/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _PNM_NETWORK_H
#define	_PNM_NETWORK_H

#pragma ident	"@(#)pnmd_network.h	1.5	08/05/20 SMI"

/*
 * pnmd_network.h - PNM networking module header file.
 *
 * This module aims to provide an abstraction layer between the PNM and the
 * kernel/networking/group component it is running on. This includes the hide
 * of all * SIOC ioctls that may be specfic to a given operating system. This
 * covers also all the logical addresses management functions needed by the
 * public network group management module. This gives also an abstraction layer
 * between the generic public network group and the actual network interface
 * group implementation that is dependent on the underlying operating system.
 *
 */

#ifdef __cplusplus
extern "C" {
#endif

#include <pnm/pnm_head.h>

typedef struct log_addr {
	struct in6_addr		ipaddr;
	char			*name; /* adp name eg: hme0:1 */
	uint64_t		flags; /* flags of the logical Ip */
	struct log_addr		*next; /* next l_addr pointer */
}	l_addr;

typedef struct log_net {
	struct in6_addr		ipnet;
	struct in6_addr		ipmask;
	struct in6_addr		ipaddr;
	struct log_net		*next; /* next l_nets pointer */
}	l_nets;

typedef struct log_netlist {
	uint64_t	num_nets;
	l_nets		*net_list;
}	l_netlist;

extern l_addr *nw_logip_get(char *, sa_family_t);
extern void nw_logip_free(l_addr *);
extern uint_t nw_is_mac_addrs_uniq(void);
extern int nw_verify_mac_addr(void);
extern char *nw_group_name(void);
extern boolean_t nw_group_failover_config(l_addr *);
extern int nw_adapterinfo_list(nw_adapter_t *, size_t *, boolean_t);
extern int nw_group_create(char *, char *);

#ifdef __cplusplus
}
#endif

#endif /* _PNM_NETWORK_H */
