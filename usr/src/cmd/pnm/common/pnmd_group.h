/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _PNM_GROUP_H
#define	_PNM_GROUP_H

#pragma ident	"@(#)pnmd_group.h	1.6	08/05/20 SMI"

/*
 * pnmd_group.h - Public network group management header file.
 *
 * Provide the PNM groups data structure and all the functions useful by
 * probing and server modules to manage the public network groups.
 */

#ifdef __cplusplus
extern "C" {
#endif

#include "pnmd_network.h"

#define	ADP_OK		0	/* adapter is ok */
#define	ADP_FAULTY	1	/* adapter is failed, IFF_FAILED = 1 */
#define	ADP_STANDBY	2	/* adapter is standby and inactive */
#define	ADP_OFFLINE	3	/* adapter is offline, IFF_OFFLINE = 1 */
#define	ADP_NOTRUN	4	/* adapter has no carrier, IFF_RUNNING = 0 */

#define	CONF_ADD	0	/* add entries in callback file */
#define	CONF_DELETE	1	/* delete entries in callback file */

#define	CONF_LINELEN	(LINELEN + CB_LINELEN)

typedef struct adp_inst {
	boolean_t	present; /* is the adp instance still plumbed? */
}	adp_inst;

typedef struct bkg_adp {
	char		*adp; /* eg: hme0 */
	int		state; /* notrun, faulty, standby, offline or ok */
	adp_inst	*pii_v4; /* pointer to v4 instance */
	adp_inst	*pii_v6; /* pointer to v6 instance */
	struct bkg_adp	*next; /* pointer to next bkg_adp */
}	bkg_adp;

typedef struct callback {
	char		*name;
	char		*cmdline;
}	callb;

typedef struct bkg_status {	/* struct for PNM group */
	char			*name; /* PNM group name */
	int			status; /* status of the group  - ok, down */
	boolean_t		change_status;
	uint_t			num_adps; /* Number of adapters in group */
	uint_t			cind; /* Number of registered callbacks */
	callb			*callback; /* Dynamic callback array */
	bkg_adp			*head; /* pointer to the adapter list */
	struct bkg_status	*next; /* pointer to next bkg_status group */
}	bkg_status;

extern bkg_status *obkg;
extern bkg_status *nbkg;

extern bkg_status *	group_add(bkg_status **);
extern void		group_delete(bkg_status *, bkg_status **);
extern bkg_status *	group_find(char *, bkg_status *);
extern void		group_free(bkg_status *);
extern void		group_append(bkg_status **, bkg_status *);
extern void		group_state_transition(bkg_status *, int);
extern void		group_updated(bkg_status *);
extern void		group_down(bkg_status *);
extern void		group_up(bkg_status *);
extern int		group_status(bkg_status *);
extern l_addr *		group_logip_get(bkg_status *, sa_family_t);

extern void		group_add_file_callbacks(void);
extern void		group_delete_file_callbacks(void);
extern int		group_add_callback(bkg_status *, char *);
extern void		group_delete_callbacks(bkg_status *);
extern int		group_run_callbacks(bkg_status *,
				const char *, boolean_t);
extern int 		group_conf_update(bkg_status *, int, int);

extern int		group_add_adp(bkg_status *, bkg_adp *);
extern void		group_delete_adp(bkg_status *, bkg_adp *);
extern bkg_adp *	group_find_adp(char *, bkg_status *);
extern void		group_free_adp(bkg_adp *);
extern void		group_free_adp_inst(adp_inst *);
extern void		group_append_adp(bkg_status *, bkg_adp *);
extern bkg_adp *	group_alloc_adp(void);
extern uint_t		group_numadp_get(bkg_status *);
extern void		group_check_and_print_mesg(bkg_adp *, char *);
extern int		group_instances(bkg_status *, int *);
extern int 		group_verify_vlan_id(bkg_status *);

#ifdef __cplusplus
}
#endif

#endif /* _PNM_GROUP_H */
