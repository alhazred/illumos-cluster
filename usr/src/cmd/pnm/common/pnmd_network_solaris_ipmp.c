/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)pnmd_network_solaris_ipmp.c	1.10	09/04/03 SMI"

/*
 * pnmd_network_solaris_ipmp.c - PNM networking group module.
 *
 * Solaris/IPMP implementation
 *
 * This module aims to provide an abstraction layer between the PNM and the
 * kernel/networking/group component it is running on. This includes the hide
 * of all * SIOC ioctls that may be specfic to a given operating system. This
 * covers also all the logical addresses management functions needed by the
 * public network group management module. This gives also an abstraction layer
 * between the generic public network group and the actual network interface
 * group implementation that is dependent on the underlying operating system.
 *
 */

#include "pnmd_network.h"

#define	IPMP_GROUPNAME "sc_ipmp"
#define	IFCONFIG	"/sbin/ifconfig"
#define	GROUP_CMD	"group"

static l_nets * nw_lognet_alloc(char *name, int so, sa_family_t af, int *r);
static void nw_copy_subnets(l_nets *ptr, nw_adapter_t *adprp, boolean_t
    alladdrs);
static boolean_t nw_subnet_present(l_nets *ptr, uint_t k, nw_adapter_t *adprp);
static int nw_update_hostname_file(int, char *, FILE *, FILE *, char *);

static uint_t mac_addrs_uniq = 1; /* 0 = not unique; 1 = unique */

static l_netlist * nw_lognet_get(char *, sa_family_t, int, struct lifconf *,
    int *);
static void nw_lognet_free(l_nets *list); /* free the subnets */
static void nw_lognetlist_free(l_netlist *list); /* free the subnet list */

/* Allocate l_nets structure and fill it */
static l_nets *
nw_lognet_alloc(char *name, int so, sa_family_t af, int *r)
{
	struct lifreq		lifraddr;
	l_nets			*ptr;
	struct sockaddr_in6	*sin6;
	struct sockaddr_in	*sin;
	struct in6_addr		addr;

	*r = -1;	/* assume error */
	ptr = (l_nets *) malloc(sizeof (l_nets));
	if (ptr == NULL) {
		log_syserr("out of memory.");
		return (NULL);
	}
	bzero(ptr, sizeof (l_nets));

	/* Retrieve the subnet */
	(void) memset(&lifraddr, 0, sizeof (lifraddr));
	(void) strncpy(lifraddr.lifr_name, name, sizeof (lifraddr.lifr_name));
	if (ioctl(so, SIOCGLIFSUBNET, (caddr_t)&lifraddr) < 0) {
		/*
		 * If the interface list has changed we return 1 so that
		 * we are called again with the new interface list
		 */
		if (errno == ENXIO) {
			*r = 1;
			free(ptr);
			return (NULL);
		}
		log_syserr("SIOCGLIFSUBNET failed.");
		free(ptr);
		return (NULL);
	}
	if (af == AF_INET) {	/* v4 */
		sin = (struct sockaddr_in *)&lifraddr.lifr_addr;
		IN6_INADDR_TO_V4MAPPED(&sin->sin_addr, &(ptr->ipnet));
	} else {	/* v6 */
		sin6 = (struct sockaddr_in6 *)&lifraddr.lifr_addr;
		addr = sin6->sin6_addr;	/* structure copy */
		bcopy(&addr, &(ptr->ipnet), CL_IPV6_ADDR_LEN);
	}

	/* Retrieve the netmask */
	(void) memset(&lifraddr, 0, sizeof (lifraddr));
	(void) strncpy(lifraddr.lifr_name, name, sizeof (lifraddr.lifr_name));
	if (ioctl(so, SIOCGLIFNETMASK, (caddr_t)&lifraddr) < 0) {
		/*
		 * If the interface list has changed we return 1 so that
		 * we are called again with the new interface list
		 */
		if (errno == ENXIO) {
			*r = 1;
			free(ptr);
			return (NULL);
		}
		log_syserr("SIOCGLIFNETMASK failed.");
		free(ptr);
		return (NULL);
	}
	if (af == AF_INET) {
		sin = (struct sockaddr_in *)&lifraddr.lifr_addr;
		IN6_INADDR_TO_V4MAPPED(&sin->sin_addr, &(ptr->ipmask));
	} else {
		sin6 = (struct sockaddr_in6 *)&lifraddr.lifr_addr;
		addr = sin6->sin6_addr;	/* structure copy */
		bcopy(&addr, &(ptr->ipmask), CL_IPV6_ADDR_LEN);
	}

	/* Retrieve the ip addr */
	(void) memset(&lifraddr, 0, sizeof (lifraddr));
	(void) strncpy(lifraddr.lifr_name, name, sizeof (lifraddr.lifr_name));
	if (ioctl(so, SIOCGLIFADDR, (caddr_t)&lifraddr) < 0) {
		/*
		 * If the interface list has changed we return 1 so that
		 * we are called again with the new interface list
		 */
		if (errno == ENXIO) {
			*r = 1;
			free(ptr);
			return (NULL);
		}
		log_syserr("SIOCGLIFADDR failed.");
		free(ptr);
		return (NULL);
	}
	if (af == AF_INET) {
		sin = (struct sockaddr_in *)&lifraddr.lifr_addr;
		IN6_INADDR_TO_V4MAPPED(&sin->sin_addr, &(ptr->ipaddr));
	} else {
		sin6 = (struct sockaddr_in6 *)&lifraddr.lifr_addr;
		addr = sin6->sin6_addr;	/* structure copy */
		bcopy(&addr, &(ptr->ipaddr), CL_IPV6_ADDR_LEN);
	}

	*r = 0; /* success */
	return (ptr);
}

/*
 * Get all IP addresses hosted on this adapter instance - both physical and
 * logical IP addresses. If the adapter instance is NULL then get all the
 * logical IP addresses (not loopback) on this node. Input is physical adp
 * name i.e. hme0. Returns a list of l_addrs. On error returns NULL. Get the IP
 * addresses for the given family.
 * INPUT: pointer to adp name or NULL, family(v4/v6)
 * RETURN: pointer to a list of l_addrs structures, NULL on error.
 */
l_addr *
nw_logip_get(char *adp, sa_family_t af)
{
	uint_t			n, lif_cnt, phys_name_len = 0;
	int			so, try = 0;
	char			*name, *buf = NULL;
	struct lifconf		lifc;
	struct lifreq		*lifr;
	l_addr			*ptr, *last_ptr, *head_ptr = NULL;
	struct lifnum		lifn;
	struct in6_addr		addr;
	struct sockaddr_in6	*sin6;
	struct sockaddr_in	*sin;
	struct lifreq		lifrflag;

	DEBUGP((stdout, "nw_logip_get: enter, adp = %s\n", adp));

	if ((so = socket(af, SOCK_DGRAM, 0)) < 0) {
		log_syserr("socket failed.");
		goto error_gli_exit;
	}

retry:
	ptr = last_ptr = head_ptr = NULL;
	/*
	 * We use SIOCGLIFNUM here. Since new interfaces may be
	 * configured after SIOCGLIFNUM, and we would get an error from
	 * SIOCGLIFCONF (EINVAL) we will try this twice. Initially we will
	 * allocate some extra buffer space.
	 */
	try++;
	lifn.lifn_family = af;

	/*
	 * LIFC_UNDER_IPMP flag is used to retrieve the interfaces under the
	 * psuedo-ipmp interface. This flag is available in Solaris release
	 * with Clearview project.
	 */
#ifdef	LIFC_UNDER_IPMP
	lifn.lifn_flags = LIFC_UNDER_IPMP;
#else
	lifn.lifn_flags = 0;
#endif
	if (ioctl(so, SIOCGLIFNUM, (char *)&lifn) < 0) {
		log_syserr("SIOCGLIFNUM failed.");
		goto error_gli_exit;
	}
	lif_cnt = (uint_t)(lifn.lifn_count + 1);

	/*
	 * Retrieve i/f configuration of machine i.e. all the networks
	 * it is connected to. Free buf if required.
	 */
	if (buf != NULL)
		free(buf);
	buf = (char *)malloc(lif_cnt * sizeof (struct lifreq));
	if (buf == NULL) {
		log_syserr("out of memory.");
		goto error_gli_exit;
	}

	(void) memset(&lifc, 0, sizeof (lifc));
	lifc.lifc_len = (int)lif_cnt * (int)sizeof (struct lifreq);
	lifc.lifc_buf = buf;

	/*
	 * LIFC_UNDER_IPMP flag is used to retrieve the interfaces under the
	 * psuedo-ipmp interface. This flag is available in Solaris release
	 * with Clearview project.
	 */
#ifdef	LIFC_UNDER_IPMP
	lifc.lifc_flags = LIFC_UNDER_IPMP;
#endif
	lifc.lifc_family = af; /* Get it only for this family */
	if (ioctl(so, SIOCGLIFCONF, (char *)&lifc) < 0) {
		/*
		 * EINVAL is commonly encountered, when things change
		 * underneath us rapidly, (eg. at boot, when new interfaces
		 * are plumbed successively) and the kernel finds the buffer
		 * size we passed as too small. We will retry twice.
		 */
		/* CSTYLED */
		if (errno == EINVAL && try < 2) /*lint !e746 */
			goto retry;
		log_syserr("SIOCGLIFCONF failed.");
		goto error_gli_exit;
	}

	/* For each network i/f on machine ... */
	lifr = (struct lifreq *)lifc.lifc_req;
	for (n = 0; n < (uint_t)lifc.lifc_len / sizeof (struct lifreq); n++,
	    lifr++) {
		name = lifr->lifr_name;
		assert(af == lifr->lifr_addr.ss_family);

		/*
		 * Get the length of the physical adapter
		 * eg for hme0 it will be 3
		 */
		phys_name_len = strcspn(name, ":");
		assert(phys_name_len != 0);

		/*
		 * If adp is NULL then we will get all the logical IPs.
		 * If the name of the i/f is not the same as the name of
		 * adp i.e. hme0 = hme0. So we are only concerned with i/f
		 * which have the same name as adp.
		 */
		if ((adp) &&
		    (phys_name_len != strlen(adp) ||
		    strncmp(adp, name, phys_name_len) != 0))
			continue;

		/* Also if the name of the adp is lo0:x then we do not care. */
		if (strncmp(name, "lo0", phys_name_len) == 0)
			continue;

		DEBUGP((stdout, "nw_logip_get: adp = %s\n", name));

		/*
		 * Retrieve the flags set on this IP address
		 * (physical or logical).
		 */
		(void) memset(&lifrflag, 0, sizeof (lifrflag));
		(void) strncpy(lifrflag.lifr_name, name,
			sizeof (lifrflag.lifr_name));

		if (ioctl(so, SIOCGLIFFLAGS, (char *)&lifrflag) < 0) {
			/*
			 * An interface might disappear after we have got the
			 * list of interfaces. In that case we will get the
			 * list of interfaces once again.
			 */
			if (errno == ENXIO) {
				try = 0;
				nw_logip_free(head_ptr);
				goto retry;
			}
			log_syserr("SIOCGLIFFLAGS failed.");
			goto error_gli_exit;
		}

		/*
		 * If the interface is an IPMP psuedo-interface,
		 * ignore it and continue to get flags for other
		 * interfaces.
		 */
#ifdef	IFF_IPMP
		if (lifrflag.lifr_flags & IFF_IPMP)
			continue;
#endif

		ptr = (l_addr *) malloc(sizeof (l_addr));
		if (ptr == NULL) {
			log_syserr("out of memory.");
			goto error_gli_exit;
		}
		bzero(ptr, sizeof (l_addr));

		/* Append l_addr structure to list */
		if (head_ptr == NULL)
			head_ptr = ptr;
		if (last_ptr != NULL)
			last_ptr->next = ptr;
		last_ptr = ptr;

		if (af == AF_INET) {	/* v4 */
			sin = (struct sockaddr_in *)&lifr->lifr_addr;
			IN6_INADDR_TO_V4MAPPED(&sin->sin_addr, &(ptr->ipaddr));
		} else {	/* v6 */
			sin6 = (struct sockaddr_in6 *)&lifr->lifr_addr;
			addr = sin6->sin6_addr;	/* structure copy */
			bcopy(&addr, &(ptr->ipaddr), CL_IPV6_ADDR_LEN);
		}

		/* Save the flags of the logical IP address */
		ptr->flags = lifrflag.lifr_flags;

		/* Store the name of the adp also */
		ptr->name = strdup(name);
		if (ptr->name == NULL) {
			log_syserr("out of memory.");
			goto error_gli_exit;
		}

	}

	(void) close(so);
	free(buf);

	DEBUGP((stdout, "nw_logip_get: exit\n"));

	return (head_ptr);

error_gli_exit:
	/* Error cleanup: free logic_ip list and return NULL */
	DEBUGP((stdout, "nw_logip_get: error\n"));

	if (so > 0)
		(void) close(so);

	if (buf != NULL)
		free(buf);

	nw_logip_free(head_ptr);

	return (NULL);
}

/*
 * Get all IP subnets, netmasks and IP addresses hosted on this adapter
 * instance - both physical and logical. Input is physical adp name
 * i.e. hme0. Returns a list of l_netlist. On error returns NULL.
 * Get the IP subnets, netmasks and IP addresses for the
 * given family.
 * INPUT: pointer to adp name, family(v4/v6), socket id, lifconf buffer pointer.
 * RETURN: pointer to a l_netlist structure, NULL on error.
 */
l_netlist *
nw_lognet_get(char *adp, sa_family_t af, int so, struct lifconf *lifcp, int *r)
{
	uint_t			n, phys_name_len = 0;
	char			*name;
	struct lifreq		*lifr, lifrflag;
	l_nets			*ptr, *last_ptr, *head_ptr;
	l_netlist		*list;

	DEBUGP((stdout, "nw_lognet_get: enter, adp = %s\n", adp));

	*r = -1;	/* assume error */
	ptr = last_ptr = head_ptr = NULL;

	list = (l_netlist *) malloc(sizeof (l_netlist));
	if (list == NULL)
		return (NULL);
	bzero(list, sizeof (l_netlist));

	/* For each network i/f on machine ... */
	lifr = (struct lifreq *)lifcp->lifc_req;
	for (n = 0; n < (uint_t)lifcp->lifc_len / sizeof (struct lifreq); n++,
	    lifr++) {

		/* Continue if it is not the same family */
		if (af != lifr->lifr_addr.ss_family)
			continue;

		name = lifr->lifr_name;

		/*
		 * Get the length of the physical adapter
		 * eg for hme0 it will be 3
		 */
		phys_name_len = strcspn(name, ":");
		assert(phys_name_len != 0);

		/*
		 * If the name of the i/f is not the same as the name of
		 * adp i.e. hme0 = hme0, then continue. So we are only
		 * concerned with i/f which have the same name as adp.
		 */
		if ((phys_name_len != strlen(adp) ||
		    strncmp(adp, name, phys_name_len) != 0))
			continue;

		/*
		 * Retrieve the flags set on this I/F
		 */
		(void) memset(&lifrflag, 0, sizeof (lifrflag));
		(void) strncpy(lifrflag.lifr_name, name,
			sizeof (lifrflag.lifr_name));

		/*
		 * Ignore this interface on ENXIO and continue getting
		 * the flags for the rest of the interfaces. Its possible
		 * the interface got removed after we got the list of
		 * interfaces - so we just ignore this interface silently.
		 */
		if (ioctl(so, SIOCGLIFFLAGS, (char *)&lifrflag) < 0) {
			/* CSTYLED */
			if (errno == ENXIO) /*lint !e746 */
				continue; /* ignore this interface */
			nw_lognetlist_free(list);
			return (NULL);
		}

		/* Allocate l_nets structure */
		ptr = nw_lognet_alloc(name, so, af, r);
		if (*r == -1 || *r == 1 || ptr == NULL) {
			nw_lognetlist_free(list);
			return (NULL);
		}

		list->num_nets++;

		/* Append l_nets structure to list */
		if (head_ptr == NULL)
			head_ptr = ptr;
		if (last_ptr != NULL)
			last_ptr->next = ptr;
		last_ptr = ptr;
		list->net_list = head_ptr;
	}

	DEBUGP((stdout, "nw_lognet_get: exit\n"));
	return (list);
}

/*
 * Free the l_addr structures list.
 * INPUT: pointer to l_addr list
 * RETURN: void
 */
void
nw_logip_free(l_addr *list)
{
	l_addr	*lp = NULL, *lp_next = NULL;

	for (lp = list; lp; lp = lp_next) {
		lp_next = lp->next;
		free(lp->name);
		free(lp);
	}
}

/*
 * Free the l_netlist structure.
 * INPUT: pointer to l_netlist
 * RETURN: void
 */
void
nw_lognetlist_free(l_netlist *list)
{
	nw_lognet_free(list->net_list);
	list = NULL;
}

/*
 * Free the l_netlist structures list.
 * INPUT: pointer to l_nets list
 * RETURN: void
 */
void
nw_lognet_free(l_nets *list)
{
	l_nets	*lp = NULL, *lp_next = NULL;

	for (lp = list; lp; lp = lp_next) {
		lp_next = lp->next;
		free(lp);
	}
}

/*
 * Return 1 if MAC addresses are unique, 0 otherwise.
 */
uint_t
nw_is_mac_addrs_uniq()
{
	return (mac_addrs_uniq);
}

/*
 * Verify that the adapters on this node have different MAC addresses.
 * If they don't then exit. This works only for v4 since there is no ARP for
 * v6. Also we should keep in mind that this only checks for the adapters that
 * are plumbed. So its best to run this check whenever an adapter is plumbed.
 * INPUT: void
 * RETURN: 0 on success and -1 on error.
 */
int
nw_verify_mac_addr()
{
	uint_t			n, j, i = 0;
	uint_t			lif_cnt;
	int			so = 0;
	uint_t			inst;	/* ordinal value of IP */
	char			*name, *buf = NULL;
	struct lifconf		lifc;
	struct sockaddr_in	*sin, *sin1;
	struct lifreq		lifrmask, lifrflag, *lifr;
#ifdef	SIOCGXARP
	struct xarpreq		xarpreq;
	struct xarpreq		ar[MAX_NUMADPS];
#else
	struct lifreq		lifrmac;
	struct arpreq		ar[MAX_NUMADPS];
#endif /* SIOCGXARP */
	struct lifnum		lifn;
	int			try = 0;

	DEBUGP((stdout, "verify_mac_addr: enter\n"));
	if ((so = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
		log_syserr("socket failed.");
		goto error_vma_exit;
	}

retry:
	/*
	 * We use SIOCGLIFNUM here. Since new interfaces may be
	 * configured after SIOCGLIFNUM, and we would get an error from
	 * SIOCGLIFCONF (EINVAL) we will try this twice. Initially we will
	 * allocate some extra buffer space.
	 */
	try++;
	lifn.lifn_family = AF_INET;	/* v4 only */

	/*
	 * We are only interested in the physical interfaces here and
	 * the information about logical instances is not needed.
	 * Since, physical interfaces can't be configured in the local zones,
	 * no need to pass the LIFC_ALLZONES flag in the following ioctl call.
	 * LIFC_UNDER_IPMP flag is used to retrieve the interfaces under the
	 * psuedo-ipmp interface. This flag is available in Solaris release
	 * with Clearview project.
	 */
#ifdef	LIFC_UNDER_IPMP
	lifn.lifn_flags = LIFC_UNDER_IPMP;
#else
	lifn.lifn_flags = 0;
#endif
	if (ioctl(so, SIOCGLIFNUM, (char *)&lifn) < 0) {
		log_syserr("SIOCGLIFNUM failed.");
		goto error_vma_exit;
	}
	lif_cnt = (uint_t)(lifn.lifn_count + 1);

	/*
	 * Retrieve i/f configuration of machine i.e. all the networks
	 * it is connected to. Free buf if required.
	 */
	if (buf != NULL)
		free(buf);
	buf = (char *)malloc(lif_cnt  * sizeof (struct lifreq));
	if (buf == NULL) {
		log_syserr("out of memory.");
		goto error_vma_exit;
	}

	(void) memset(&lifc, 0, sizeof (lifc));
	lifc.lifc_len = (int)(lif_cnt * sizeof (struct lifreq));
	lifc.lifc_buf = buf;

	/*
	 * LIFC_UNDER_IPMP flag is used to retrieve the interfaces under the
	 * psuedo-ipmp interface. This flag is available in Solaris release
	 * with Clearview project.
	 */
#ifdef	LIFC_UNDER_IPMP
	lifc.lifc_flags = LIFC_UNDER_IPMP;
#endif
	lifc.lifc_family = AF_INET; /* v4 only */
	if (ioctl(so, SIOCGLIFCONF, (char *)&lifc) < 0) {
		/*
		 * EINVAL is commonly encountered, when things change
		 * underneath us rapidly, (eg. at boot, when new interfaces
		 * are plumbed successively) and the kernel finds the buffer
		 * size we passed as too small. We will retry twice.
		 */
		if (errno == EINVAL && try < 2)
			goto retry;
		log_syserr("SIOCGLIFCONF failed.");
		goto error_vma_exit;
	}

	/*
	 * For each network i/f on machine ...
	 */

	lifr = (struct lifreq *)lifc.lifc_req;
	for (n = 0; mac_addrs_uniq &&
	    n < (uint_t)lifc.lifc_len / sizeof (struct lifreq);
	    n++, lifr++) {
		name = lifr->lifr_name;
		/* We do not want to check the mac-address for loopback */
		if (strcmp(name, "lo0") == 0)
			continue;
		assert(lifr->lifr_addr.ss_family == AF_INET);

		/*
		 * Returns 0 for physical adp, otherwise returns the
		 * ordinal value of the logical IP. eg. for
		 * tr2, it will return 0, whilst for tr2:3, it
		 * will return 3. get_logical_inst is defined in pnmd_util.c.
		 */
		inst = get_logical_inst(name);

		/* We only want to check the physical adapters */
		if (inst != 0)
			continue;

		DEBUGP((stdout, "verify_mac_addr: adp %s \n", name));

		/* Retrieve the flags set on this IP address (physical). */
		(void) memset(&lifrflag, 0, sizeof (lifrflag));
		(void) strncpy(lifrflag.lifr_name, name,
		    sizeof (lifrflag.lifr_name));
		if (ioctl(so, SIOCGLIFFLAGS, (char *)&lifrflag) < 0) {
			log_syserr("SIOCGLIFFLAGS failed.");
			goto error_vma_exit;
		}
		/* If IP is not UP then we can't get MAC address for this */
		if ((lifrflag.lifr_flags & IPADDR_UP) != IPADDR_UP)
			continue;

		/*
		 * If the interface is an IPMP psuedo-interface,
		 * ignore it and continue to get flags for other
		 * interfaces.
		 */
#ifdef	IFF_IPMP
		if (lifrflag.lifr_flags & IFF_IPMP)
			continue;
#endif

		sin = (struct sockaddr_in *)&lifr->lifr_addr;

		/*
		 * Retrieve the ARP entry for this IP address. Set the
		 * adp name and the IP address and we will get the MAC
		 */
#ifdef SIOCGXARP
		(void) memset(&xarpreq, 0, sizeof (xarpreq));
		/*
		 * fill the xarp_pa with IP protocol address info
		 */
		bcopy(sin, &xarpreq.xarp_pa, sizeof (*sin));
		xarpreq.xarp_ha.sdl_family = AF_LINK;

		if (ioctl(so, SIOCGXARP, (caddr_t)&xarpreq) < 0) {
			/*
			 * The arp entry would always be present since
			 * this is its own interface. (marked as S). However
			 * sometimes this call might fail if the driver is
			 * weird etc - so we don't want to error out in such
			 * a case - instead we will log a message and
			 * continue on as though nothing happened.
			 */
			log_syserr("%s SIOCGXARP failed.", name);
		}
		ar[i] = xarpreq;
#else
		(void) memset(&lifrmac, 0, sizeof (lifrmac));
		(void) strncpy(lifrmac.lifr_name, name,
		    sizeof (lifrmac.lifr_name));
		((struct sockaddr_in *)&lifrmac.lifr_addr)->sin_addr.s_addr =
		    sin->sin_addr.s_addr;
		if (ioctl(so, SIOCGIFARP, (caddr_t)&lifrmac) < 0) {
			/*
			 * The arp entry would always be present since
			 * this is its own interface. (marked as S). However
			 * sometimes this call might fail if the driver is
			 * weird etc - so we don't want to error out in such
			 * a case - instead we will log a message and
			 * continue on as though nothing happened.
			 */
			log_syserr("%s SIOCGIFARP failed.", name);
		}

		(void) memset(&ar[i], 0, sizeof (struct arpreq));
		(void) memcpy((uchar_t *)ar[i].arp_ha.sa_data,
		    (uchar_t *)lifrmac.lifr_nd.lnr_hdw_addr, CL_ETHERADDRL);
#endif /* SIOCGXARP */

		/* Retrieve the netmask address */
		(void) memset(&lifrmask, 0, sizeof (lifrmask));
		(void) strncpy(lifrmask.lifr_name, name,
		    sizeof (lifrmask.lifr_name));
		if (ioctl(so, SIOCGLIFNETMASK, (caddr_t)&lifrmask) < 0) {
			log_syserr("SIOCGLIFNETMASK failed.");
			goto error_vma_exit;
		}
		sin1 = (struct sockaddr_in *)(void *) &lifrmask.lifr_addr;

		/* Store the subnet : IP & MASK */
		sin->sin_addr.s_addr = sin1->sin_addr.s_addr &
		    sin->sin_addr.s_addr;
#ifdef SIOCGXARP
		sin1 = (struct sockaddr_in *)&ar[i].xarp_pa;
#else
		sin1 = (struct sockaddr_in *)&ar[i].arp_pa;
#endif
		sin1->sin_family = AF_INET;
		sin1->sin_addr.s_addr = sin->sin_addr.s_addr;

		/*
		 * Check if the MAC addrs is equal to any of the previous
		 * MAC addresses retrieved. If the MAC addresses match then
		 * check to see if they are on the same subnet
		 * then return -1.
		 */
		for (j = 0; i > 0 && j < i; j++) {
#ifdef SIOCGXARP
			if ((ar[i].xarp_ha.sdl_alen ==
			    ar[j].xarp_ha.sdl_alen) &&
			    charcmp((uchar_t *)LLADDR(&ar[i].xarp_ha),
			    (uchar_t *)LLADDR(&ar[j].xarp_ha),
			    ar[j].xarp_ha.sdl_alen) == 0) {
				sin1 = (struct sockaddr_in *)&ar[j].xarp_pa;
				/* Now verify if they are on the same subnet */
				if (sin1->sin_addr.s_addr ==
				    sin->sin_addr.s_addr) {
					mac_addrs_uniq = 0;
					break;
				}
			}
#else
			if (charcmp((uchar_t *)ar[i].arp_ha.sa_data,
			    (uchar_t *)ar[j].arp_ha.sa_data,
			    CL_ETHERADDRL) == 0) {
				sin1 = (struct sockaddr_in *)&ar[j].arp_pa;
				/* Now verify if they are on the same subnet */
				if (sin1->sin_addr.s_addr ==
				    sin->sin_addr.s_addr) {
					mac_addrs_uniq = 0;
					break;
				}
			}
#endif /* SIOCGXARP */
		}
		i++;
	}

	(void) close(so);
	free(buf);

	if (mac_addrs_uniq != 0) {
		log_debug("MAC addresses are unique per subnet");
	} else {
		log_debug("MAC addrs are not unique per subnet");
		/*
		 * SCMSGS
		 * @explanation
		 * What this means is that there are at least two adapters on
		 * a subnet which have the same MAC address. IPMP makes the
		 * assumption that all adapters have unique MAC addresses.
		 * @user_action
		 * Look at the ifconfig man page on how to set MAC addresses
		 * manually. This is however, a temporary fix and the real fix
		 * is to upgrade the hardware so that the adapters have unique
		 * MAC addresses.
		 */
		(void) sc_syslog_msg_log(msg_pnm, SC_SYSLOG_ERROR, MESSAGE,
		    "MAC addresses are not unique per subnet.");
	}

	DEBUGP((stdout, "verify_mac_addr: exit\n"));
	return (0);

error_vma_exit:
	if (so > 0)
		(void) close(so);
	if (buf != NULL)
		free(buf);
	return (-1);
}

/*
 * nw_copy_subnets() - This call is used by nw_adapterinfo_list() to copy
 * the subnets, netmasks and ipaddrs on this adapter into the network adapter
 * structure. It first checks to see that the subnet has not already been
 * copied before copying it. This way we are able to send only the unique
 * subnet and subnet mask combination hosted on this adp.
 * INPUT: ptr to l_nets - list of subnets and subnet masks,
 *        ptr to nw_adapter_t - the response that we have to fill.
 *        alladdrs - if TRUE copy all IP addresses, else only copy uniq subnets
 * RETURN: void - copies subnet, subnet masks and IP addrs from l_nets to the
 *         response.
 */
static void
nw_copy_subnets(l_nets *ptr, nw_adapter_t *adprp, boolean_t alladdrs)
{
	uint_t	k = 0;

	for (; ptr; ptr = ptr->next) {
		if (!alladdrs && nw_subnet_present(ptr, k, adprp)) {
			adprp->num_addrs--;
			continue;
		}
		/* CSTYLED */
		/*lint -e662 */
		bcopy(&ptr->ipnet, ((char *)&adprp->adp_net +
		    (k++ * CL_IPV6_ADDR_LEN)), CL_IPV6_ADDR_LEN);
		bcopy(&ptr->ipmask, ((char *)&adprp->adp_net +
		    (k++ * CL_IPV6_ADDR_LEN)), CL_IPV6_ADDR_LEN);
		bcopy(&ptr->ipaddr, ((char *)&adprp->adp_net +
		    (k++ * CL_IPV6_ADDR_LEN)), CL_IPV6_ADDR_LEN);
		/* CSTYLED */
		/*lint +e662 */
	}
}

/*
 * nw_subnet_present() - see if the subnet is already present. This call is used
 * nw_subnet_present() to see if the subnet is already present in the
 * response.
 * INPUT: ptr to l_nets - subnet and subnet mask,
 *        ptr to nw_adapter_t - the response.
 *        int k - the number of subnet + subnet masks already present in the
 *        response.
 * RETURN: true if the given subnet/mask is already present else false.
 */
static boolean_t
nw_subnet_present(l_nets *ptr, uint_t k, nw_adapter_t *adprp)
{
	uint_t	i = 0;

	for (i = 0; i < k; i += 2) {
		if (IN6_ARE_ADDR_EQUAL((in6_addr_t *)&ptr->ipnet,
		    (in6_addr_t *)((char *)&adprp->adp_net +
		    (i * CL_IPV6_ADDR_LEN))))
			return (B_TRUE);
	}

	return (B_FALSE);
}

/*
 * This function is called by nw_group_create to update the hostname files of
 * the v4 and v6 instance of the adapter.
 * INPUT: int i - 0 for v4 and 1 for v6.
 *        char *gname - group name.
 *        FILE *tfp - open file pointer for the file which we have to write to.
 *        FILE *fp - open file pointer for the file which we have to read from.
 *        char *fname - name of the original hostname file.
 * RETURN: 0 for success or -1 for error.
 */
static int
nw_update_hostname_file(int i, char *gname, FILE *tfp, FILE *fp, char *fname)
{
	char line[LINELEN];
	char line1[LINELEN];
	uint_t linelen;
	char *line_grp = NULL;
	char *grpname = NULL;
	char *line_tmp = NULL;
	boolean_t firstline;

	firstline = B_TRUE;
	/*
	 * Some assumptions about the /etc/hostname.<adp> file:
	 * With an empty /etc/hostname.* file this only sets up the
	 * streams plumbing, allowing the ifconfig auto-dhcp /
	 * auto-revarp command to attempt to set the address. If there
	 * is only one line in an /etc/hostname.* file we assume it
	 * contains the old style address which results in the
	 * interface being brought up and the netmask and broadcast
	 * address being set. If there are multiple lines we assume
	 * the file contains a list of commands to ifconfig with
	 * neither the implied bringing up of the interface nor the
	 * setting of the default netmask and broadcast address.
	 * However, in auto-create we set the first IP address
	 * as the non-failover address. So ideally, we need the file
	 * to have only one line, which should have the IP address.
	 * The main motivation for auto-create is that some cluster
	 * admins don't know anything about IPMP and do not care
	 * about having multi-adapter IPMP groups. Such admins do
	 * not have to read up on IPMP and setup IPMP on their nodes.
	 * They can just have their old style hostname.<adp> files
	 * and SC will do auto-create for them when they try to create
	 * a LH/SA resource. This will help a lot in upgrade from
	 * SC3.0 on clusters which have only a singleton adapter.
	 * For v6 also we do not expect to do auto-create unless the
	 * /etc/hostname6.<adp> file is blank. However, we will try to
	 * work with multi-line hostname6.<adp> files also.
	 * get_line() returns the next input line from a file stream
	 * every time it is called. It looks for a non blank character
	 * and returns a line uptil the newline character \n. Hence,
	 * all the initial white spaces are removed, and blank lines
	 * are not copied over.
	 */
	while (get_line(fp, line) > 0) {
		/* Process the first line */
		if (firstline == B_TRUE) {
			line_tmp = strdup(line);
			if (line_tmp == NULL) {
				log_syserr("out of memory.");
				return (-1);
			}
			/*
			 * If the keyword "group" is present then
			 * check the groupname. The keyword "group"
			 * should not be present because if it was
			 * then the IPMP group should have been set -
			 * anyways check and log an error. There could
			 * always be cases in which auto-create is
			 * called and the file has multiple lines
			 * with only one IP address - in actuality we
			 * should support such a file but hey, we can't
			 * worry about all the different formats.
			 * Generally /etc/hostname.* files which have
			 * only one IP address hosted have only one
			 * line. Do we want to take care of tabs here?
			 */
			while ((line_grp = strtok(line_tmp, " \t")) != NULL) {
				if (strcmp(line_grp, "group") == 0) {
					grpname = strtok(NULL, " \t");
					if (!grpname)
						log_conferr("wrong format for "
						    "%s", fname);
					DEBUGP((stderr, "%s has the keyword "
					    " group so group_create "
					    "should not be called.\n", fname));
					/*
					 * SCMSGS
					 * @explanation
					 * This means that auto-create was
					 * called even though the
					 * /etc/hostname.adp file has the
					 * keyword "group". Someone might have
					 * hand edited that file. This could
					 * also happen if someone deletes an
					 * IPMP group - A notification should
					 * have been provided for this.
					 * @user_action
					 * Change the file back to its
					 * original contents. Try the
					 * clreslogicalhostname or
					 * clressharedaddress command again.
					 * We do not allow IPMP groups to be
					 * deleted once they are configured.
					 * If the problem persists contact
					 * your authorized Sun service
					 * provider for suggestions.
					 */
					(void) sc_syslog_msg_log(msg_pnm,
					    SC_SYSLOG_ERROR, MESSAGE, "%s has "
					    "the keyword group already.",
					    fname);
					return (-1);
				}
				line_tmp = NULL;
			}
			/*
			 * If the keyword "group" is not present
			 * then add a group name to the line.
			 * We don't want to set the deprecated flag
			 * for auto-create, since we know that auto-create
			 * will be called only when there is a singleton group
			 * for which we do not need to set the
			 * DEPRECATED flag. Also add "-failover" flag only
			 * for non-clearview releases because with Clearview
			 * project the data address can't be overloaded as
			 * test address.
			 *
			 */
#ifdef	IFF_IPMP
			(void) sprintf(line1, " group %s", gname);
#else
			(void) sprintf(line1, " -failover group %s", gname);
#endif

			if (i == 0)	/* v4 processing */
				(void) strcat(line, line1);
			else {		/* v6 processing */
				linelen = strlen(line1);
				line1[linelen] = '\n';
				(void) fwrite(line1, 1, linelen + 1, tfp);
			}
			firstline = B_FALSE;
			free(line_tmp);
		}	/* end of first line processing */

		/* now copy all the lines */
		linelen = strlen(line);
		line[linelen] = '\n';
		(void) fwrite(line, 1, linelen + 1, tfp);
	}	/* end of while  - get_line */

	/*
	 * We could have blank files for v6 so we must take care of
	 * those too. For blank files get_line() will not return
	 * anything. So we will go ahead and append the groupname etc.
	 * Also we must remember that for v6 the link-local address
	 * will always be the test address. Hence we can write in the
	 * hostname6.<adp> file: -failover group groupname. We take
	 * care of multiple lines also.
	 * We could have blank files for v4 so we must take care of
	 * those too - error out for blank v4 files.
	 */
	if (firstline == B_TRUE) {
		if (i == 0) {	/* v4 processing */
			log_conferr("%s is blank.", fname);
			return (-1);
		} else {	/* v6 processing */
#ifdef	IFF_IPMP
			(void) sprintf(line, "group %s", gname);
#else
			(void) sprintf(line, "-failover group %s", gname);
#endif

			linelen = strlen(line);
			line[linelen] = '\n';
			(void) fwrite(line, 1, linelen + 1, tfp);
		}
	}
	(void) fclose(fp);
	(void) fclose(tfp);
	return (0);
}

/*
 * Return the group name prefix to be used
 * for group creation.
 */
char *
nw_group_name()
{
	return (IPMP_GROUPNAME);
}

/*
 * Return true if one of the adapter in the list has a failover (data)
 * address hosted/configured. False otherwise.
 */
boolean_t
nw_group_failover_config(l_addr *list)
{
	boolean_t data_ip = B_FALSE; /* Assume that data IP is not present */
	l_addr *lp = NULL;

	for (lp = list; lp; lp = lp->next) {
		if ((lp->flags & IFF_NOFAILOVER) != IFF_NOFAILOVER) {
			data_ip = B_TRUE;
			break;
		}
	}

	return (data_ip);
}

/*
 * This function returns a list of adapter instance structures that represents
 * all configured physical adapter instances on this node.
 * It is the responsibility of the caller to provide an output buffer that is
 * large enough to contain all the adapter structures.
 * INPUT: pointer to a pre-allocated buffer
 * OUTPUT: length of output buffer.
 * RETURN: number of adapters on success and -1 on error.
 */
int
nw_adapterinfo_list(nw_adapter_t *adp_list, size_t *len, boolean_t alladdrs)
{
	uint_t i, j;
	int so_v4 = 0, so_v6 = 0, so;
	uint_t lif_cnt;
	struct lifreq lifr, *lifrp = NULL;
	struct lifconf lifc;
	struct lifnum lifn;
	int try = 0;
	char *buf = NULL;
	l_nets *ptr = NULL;
	sa_family_t family;
	l_netlist *alist = NULL;
	nw_adapter_t *adprp = NULL;
	size_t offset;
	int ret;
	uint64_t adprp_size;

	DEBUGP((stdout, "nw_adapterinfo_list: enter\n"));

	/*
	 * Build a list of nw_adapter_t structures that
	 * represents all configured physical adapters on this
	 * node. This information includes adapter flags, netmasks, subnet
	 * addresses, and what group the adapter belongs to.
	 */

	/* Create a v4 socket for ioctl calls */
	if ((so_v4 = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
		log_syserr("socket failed.");
		goto al_error;
	}

	/* Create a v6 socket for ioctl calls */
	if ((so_v6 = socket(AF_INET6, SOCK_DGRAM, 0)) < 0) {
		log_syserr("socket failed.");
		goto al_error;
	}

retry:
	/*
	 * We use SIOCGLIFNUM here. Since new interfaces may be
	 * configured after SIOCGLIFNUM, and we would get an error from
	 * SIOCGLIFCONF (EINVAL) we will try this twice. Initially we will
	 * allocate some extra buffer space.
	 */
	try++;
	lifn.lifn_family = AF_UNSPEC;
	lifn.lifn_flags = 0;

	/*
	 * Use LIFC_ALLZONES flag in the ioctl to retrieve the
	 * adapter instances that are plumbed for zones. This flag is
	 * available from Solaris 10 release. This flag is used only when
	 * alladdrs = B_TRUE as we need logical instances also. If
	 * alladdrs = B_FALSE no need to pass the LIFC_ALLZONES flag in the
	 * following ioctl call since physical interfaces can't be configured
	 * in the local zones.
	 */

#ifdef LIFC_ALLZONES
	if (alladdrs)
		lifn.lifn_flags |= LIFC_ALLZONES;
#endif

	if (ioctl(so_v4, SIOCGLIFNUM, (char *)&lifn) < 0) {
		log_syserr("SIOCGLIFNUM failed.");
		goto al_error;
	}

	lif_cnt = (uint_t)(lifn.lifn_count + 1);

	/* Allocate enough space for all interfaces */
	(void) memset(&lifc, 0, sizeof (lifc));
	lifc.lifc_len = (int)(lif_cnt * sizeof (struct lifreq));
	if (buf != NULL)
		free(buf);
	buf = (char *)malloc((uint_t)lifc.lifc_len);
	if (buf == NULL) {
		log_syserr("out of memory.");
		goto al_error;
	}
	lifc.lifc_buf = buf;
	lifc.lifc_family = AF_UNSPEC; /* to get v4 and v6 instances */

	/* Fill up the interface buffer we just allocated */
	if (ioctl(so_v4, SIOCGLIFCONF, (char *)&lifc) < 0) {
		/*
		 * EINVAL is commonly encountered, when things change
		 * underneath us rapidly, (eg. at boot, when new interfaces
		 * are plumbed successively) and the kernel finds the buffer
		 * size we passed as too small. We will retry twice.
		 */
		if (errno == EINVAL && try < 2)
			goto retry;
		log_syserr("SIOCGLIFCONF failed.");
		goto al_error;
	}

	for (i = 0, j = 0, offset = 0;
	    i < ((uint_t)lifc.lifc_len / sizeof (struct lifreq)); i++) {
		lifrp = &lifc.lifc_req[i];
		family = lifrp->lifr_addr.ss_family;
		so = (family == AF_INET) ? so_v4 : so_v6;

		/* Continue if not physical adapter (inst != 0) */
		if (get_logical_inst(lifrp->lifr_name) != 0) {
			continue;
		}

		bzero(&lifr, sizeof (lifr));
		bcopy(lifrp->lifr_name, lifr.lifr_name,
		    sizeof (lifrp->lifr_name));

		/* Get flags */
		if (ioctl(so, SIOCGLIFFLAGS, (char *)&lifr) != 0) {
			/*
			 * If an interface has been removed after we got the
			 * list of interfaces then get the list again
			 */
			if (errno == ENXIO) {
				try = 0;
				goto retry;
			}
			log_syserr("SIOCGLIFFLAGS failed.");
			goto al_error;
		}

		/* If it is !alladdrs and (loopback or private adp) continue */
		if (!alladdrs && (lifr.lifr_flags & (IFF_LOOPBACK |
		    IFF_PRIVATE)))
			continue;

		/*
		 * We don't expect to have so many adapters and
		 * subnets so as to fill up ADPLIST_BUF bytes.
		 * Check with ipmp_adapter size only, later
		 * we will recheck for the lenght of adp_net
		 * after knowing the num_addr
		 */
		if ((offset + sizeof (nw_adapter_t)) > ADPLIST_BUF) {
			log_syserr("Too many adapters.");
			goto al_error;
		}

		adprp = (nw_adapter_t *)((char *)adp_list  +
		    offset);

		bzero(adprp, sizeof (nw_adapter_t));

		adprp->adp_flags = htonll(lifr.lifr_flags); /*lint !e661 */

		/* Get groupname for this adapter */
		(void) memset(lifr.lifr_groupname, 0,
		    sizeof (lifr.lifr_groupname));
		if (ioctl(so, SIOCGLIFGROUPNAME, (caddr_t)&lifr) < 0) {
			if (errno == ENXIO) {
				try = 0;
				goto retry;
			}
			log_syserr("SIOCGLIFGROUPNAME failed.");
			goto al_error;
		}
		if (strlen(lifr.lifr_groupname) > 0) {
			(void) strncpy(adprp->adp_group, lifr.lifr_groupname,
			    sizeof (lifr.lifr_groupname));
			DEBUGP((stdout, "nw_adapterinfo_list: group =  %s\n",
			    adprp->adp_group));
		}

		(void) strncpy(adprp->adp_name, lifr.lifr_name,
		    sizeof (lifr.lifr_name));
		DEBUGP((stdout, "nw_adapterinfo_list: adp =  %s, family = %d\n",
		    adprp->adp_name, family));

		/*
		 * Get the various subnets(along with IP addresses) on this
		 * adapter instance
		 */
		alist = nw_lognet_get(lifr.lifr_name, family, so, &lifc, &ret);
		if (ret == -1) {
			goto al_error;
		}
		if (ret == 1) { /* retry because interface list changed */
			try = 0;
			goto retry;
		}

		/* Everything is stable */
		ptr = alist->net_list;
		adprp->num_addrs = alist->num_nets;

		/*
		 * We don't expect to have so many adapters and
		 * subnets so as to fill up ADPLIST_BUF bytes.
		 * NOTE: unique adprp->num_addrs might be less than what we
		 * see here. The actual value is obtained after calling
		 * cp_subnets_into_rsp() which removes the duplicates. But
		 * we cannot run cp_subnets_into_rsp() before the check.
		 * Since, ADPLIST_BUF is too large, we can safely
		 * continue using current adprp->num_addrs.
		 */
		adprp_size = 2 * (sizeof (uint64_t) + NAMELEN) +
		    3 * ((uint64_t)adprp->num_addrs * CL_IPV6_ADDR_LEN);
		if ((offset + adprp_size) > ADPLIST_BUF) {
			nw_lognetlist_free(alist);
			goto al_error;
		}

		nw_copy_subnets(ptr, adprp, alladdrs);
		nw_lognetlist_free(alist);
		j++; /* For the next adapter */
		/* Actual adprp_size */
		adprp_size = 2 * (sizeof (uint64_t) + NAMELEN) +
		    3 * ((uint64_t)adprp->num_addrs * CL_IPV6_ADDR_LEN);
		offset += (size_t)adprp_size;
		adprp->num_addrs = htonll(adprp->num_addrs);
	}	/* End of for loop */

	free(buf);
	(void) close(so_v4);
	(void) close(so_v6);

	DEBUGP((stdout, "nw_adapterinfo_list: num adps = %d\n", j));
	*len = offset;

	DEBUGP((stdout, "nw_adapterinfo_list: exit\n"));
	return ((int)j);

al_error:
	DEBUGP((stdout, "nw_adapterinfo_list: error\n"));

	if (so_v4 > 0)
		(void) close(so_v4);
	if (so_v6 > 0)
		(void) close(so_v6);
	if (buf != NULL)
		free(buf);
	return (-1);
}

/*
 * This auto-creates an IPMP group on this node by configuring an IPMP group
 * for the given adapter instance.
 * It updates the /etc/hostname(6).<adp> file.
 * INPUT: group name and adapter name.
 * RETURN: 0 on success and -1 on error.
 */
int
nw_group_create(char *gname, char *adp)
{
	int family = 0, k = 0;
	int so_v4 = 0, so_v6 = 0, so = 0;
	struct lifreq lifr;
	char fname[2][FILENAMELEN] = {0}; /* v4 and v6 */
	char *file_tmp[2] = {NULL};	/* tmp files */
	char *undo_file[2] = {NULL};	/* undo changes */
	char fnamebuf[4][FILENAMELEN + 6] = {0};
	FILE *fp = NULL;
	FILE *tfp = NULL;
	int file_updated = 0, group_set = 0, flag_set = 0;
	struct stat st;
	char	command[MAXPATHLEN];

	DEBUGP((stdout, "nw_group_create: enter, gname = %s, adp = %s\n",
		gname, adp));

	bzero(&lifr, sizeof (struct lifreq));

	/*
	 * Update the hostname.<adp> file. Goto the file
	 * hostname.<adp> and update the file by adding the group
	 * name, and the -failover flag. This means that the
	 * first IP address/hostname in the file will be the
	 * non-failover address for the adapter.
	 * In the case of v6 we must update the etc/hostname6.
	 * <adp> file. In fact we must update both the files if
	 * they are present. If any of the files is present
	 * then we are fine.
	 */
	(void) sprintf(fname[family++], "/etc/hostname.%s", adp);
	(void) sprintf(fname[family], "/etc/hostname6.%s", adp);

	/* family = 0 is for v4 and family = 1 is for v6 */
	for (family = 0, k = 0; family < 2; family++) {

		/* skip if the instance file is not present */
		if (stat(fname[family], &st) != 0)
			continue;

		/* Open the file for reading */
		if ((fp = fopen(fname[family], "r")) == NULL) {
			log_syserr("can't open file %s.", fname[family]);
			goto gc_error;
		}

		/* Make a file for undoing changes */
		(void) sprintf(fnamebuf[k], "%sXXXXXX", fname[family]);
		undo_file[family] = mktemp(fnamebuf[k++]);

		/* Open a tmp file for writing */
		(void) sprintf(fnamebuf[k], "%sXXXXXX", fname[family]);
		file_tmp[family] = mktemp(fnamebuf[k++]);
		if ((tfp = fopen(file_tmp[family], "w")) == NULL) {
			log_syserr("can't open file %s.", file_tmp[family]);
			(void) fclose(fp);
			goto gc_error;
		}

		/* Update the hostname file */
		if (nw_update_hostname_file(family, gname, tfp, fp,
		    fname[family])) {
			(void) fclose(fp);
			(void) fclose(tfp);
			(void) unlink(file_tmp[family]);
			goto gc_error;
		}

		/* Rename the original file for undoing later */
		if (rename(fname[family], undo_file[family]) != 0) {
			log_syserr("rename failed.");
			(void) unlink(file_tmp[family]);
			goto gc_error;
		}

		/* Rename the tmp file to the original name */
		if (rename(file_tmp[family], fname[family]) != 0) {
			log_syserr("rename failed.");
			(void) unlink(file_tmp[family]);
			goto gc_error;
		}
		file_updated++;

#ifndef	LIFC_UNDER_IPMP
		/*
		 * Now set the adapter's groupname to the given groupname.
		 * We do this so that if there is an error in updating the
		 * the /etc/hostname.<adp> file then we do not have to set
		 * the adp's groupname.
		 * We also have to set this address to be IFF_NOFAILOVER since
		 * we want this adapter to be probed for failures using this
		 * address. Also setting the groupname for v4 implies that the
		 * groupname for v6 will also be set.
		 */
		if (family == 0) {
			/* Open v4 socket for ioctls */
			if ((so_v4 = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
				log_syserr("socket failed.");
				goto gc_error;
			}
			so = so_v4;
		} else {
			/* Open v6 socket for ioctls */
			if ((so_v6 = socket(AF_INET6, SOCK_DGRAM, 0)) < 0) {
				log_syserr("socket failed.");
				goto gc_error;
			}
			so = so_v6;
		}

		(void) strncpy(lifr.lifr_name, adp, sizeof (lifr.lifr_name));
		if (!group_set) {
			(void) strncpy(lifr.lifr_groupname, gname,
			    sizeof (lifr.lifr_groupname));
			/* CSTYLED */
			/*lint -e737 */
			if (ioctl(so, SIOCSLIFGROUPNAME,
				(caddr_t)&lifr) < 0) {
				log_syserr("SIOCSLIFGROUPNAME failed.");
				goto gc_error;
			}
			/* CSTYLED */
			/*lint +e737 */
			group_set++;
		}

		/* Set IFF_NOFAILOVER */
		if (ioctl(so, SIOCGLIFFLAGS, (char *)&lifr) < 0) {
			log_syserr("SIOCGLIFFLAGS failed.");
			goto gc_error;
		}
		lifr.lifr_flags = lifr.lifr_flags | IFF_NOFAILOVER;
		/* CSTYLED */
		/*lint -e737 */
		if (ioctl(so, SIOCSLIFFLAGS,
			(char *)&lifr) < 0) {
			log_syserr("SIOCSLIFFLAGS failed.");
			goto gc_error;
		}
		/* CSTYLED */
		/*lint +e737 */
		flag_set++;

#else
		/*
		 * Clearview integration: SIOCSLIFGROUPNAME ioctl sets
		 * several restrictions in the clearview IPMP model.
		 * IFF_NOFAILOVER flag does no longer mean both test
		 * address and data address. It is hence recommended to
		 * use ifconfig(1M) command to set the group name.
		 */
		(void) strncpy(lifr.lifr_name, adp, sizeof (lifr.lifr_name));
		if (!group_set) {
			(void) strncpy(lifr.lifr_groupname, gname,
			    sizeof (lifr.lifr_groupname));
			(void) sprintf(command, "%s %s %s %s > /dev/null 2>&1",
					IFCONFIG, lifr.lifr_name,
					GROUP_CMD, lifr.lifr_groupname);
				DEBUGP((stdout, "The command is %s"
				    ".\n", command));
			if (WEXITSTATUS(system(command)) != 0) {
				DEBUGP((stderr, "Can not place %s"
				    " interface in to the group %s"
				    ".\n", lifr.lifr_name,
				    lifr.lifr_groupname));
				/*
				 * SCMSGS
				 * @explanation
				 * Auto-create of IPMP group for this
				 * interface has failed. Some error
				 * would have occured while executing
				 * the "ifconfig" command. A notification
				 * has been provided for this.
				 * @user_action
				 * Look at the ifconfig man page on how
				 * to set the group name manually.
				 * If the problem persists contact
				 * your authorized Sun service
				 * provider for suggestions.
				 */
				(void) sc_syslog_msg_log(msg_pnm,
				    SC_SYSLOG_ERROR, MESSAGE, "Can not place "
				    "%s interface in to the group %s.\n",
				    lifr.lifr_name, lifr.lifr_groupname);
				goto gc_error;
			}
			group_set++;
		}
#endif
	} /* end of for */

	/* Unlink all the undo files */
	for (k = 0; k < family; k++)
		(void) unlink(undo_file[k]);

	if (so_v4)
		(void) close(so_v4);
	if (so_v6)
		(void) close(so_v6);

	DEBUGP((stdout, "nw_group_create: exit\n"));
	return (0);

gc_error:
	DEBUGP((stdout, "nw_group_create: error\n"));

	/* Undo the changes made to the files and the group names */
	if (file_updated) {
		for (family = 0; family < 2; family++) {
			/* skip if the instance file is not present */
			if (stat(fname[family], &st) != 0)
				continue;

			/* Undo the file change */
			if (file_updated > 0) {
				(void) rename(undo_file[family], fname[family]);
				(void) unlink(file_tmp[family]);
				--file_updated;
			}
#ifndef	LIFC_UNDER_IPMP
			so = (family == 0) ? so_v4 : so_v6;
			/* Set group name to "" */
			if (group_set > 0 && adp) {
				bzero(&lifr, sizeof (struct lifreq));
				(void) strncpy(lifr.lifr_name, adp,
				    sizeof (lifr.lifr_name));
				/* CSTYLED */
				/*lint -e737 */
				(void) ioctl(so, SIOCSLIFGROUPNAME,
					(caddr_t)&lifr);
				/* CSTYLED */
				/*lint +e737 */
				--group_set;
			}

			/* Unset the IFF_NOFAILOVER flag */
			if (flag_set > 0 && adp) {
				bzero(&lifr, sizeof (struct lifreq));
				(void) strncpy(lifr.lifr_name, adp,
				    sizeof (lifr.lifr_name));
				(void) ioctl(so, SIOCGLIFFLAGS, (char *)&lifr);
				lifr.lifr_flags = lifr.lifr_flags &
				    (~IFF_NOFAILOVER);
				/* CSTYLED */
				/*lint -e737 */
				(void) ioctl(so, SIOCSLIFFLAGS, (char *)&lifr);
				/* CSTYLED */
				/*lint +e737 */
				--flag_set;
			}
#else
			if (group_set > 0 && adp) {
				bzero(&lifr, sizeof (struct lifreq));
				(void) strncpy(lifr.lifr_name, adp,
				    sizeof (lifr.lifr_name));
				(void) sprintf(command, "%s %s %s %s", IFCONFIG,
						lifr.lifr_name, GROUP_CMD,
						lifr.lifr_groupname);
				if (WEXITSTATUS(system(command)) != 0) {
					DEBUGP((stderr, "Can not place %s"
					    " interface in to the group %s"
					    ".\n", lifr.lifr_name,
					    lifr.lifr_groupname));
					goto gc_error;
				}
				--group_set;
			}
#endif
		}	/* end of for */
	}	/* end of if */
	if (so_v4)
		(void) close(so_v4);
	if (so_v6)
		(void) close(so_v6);

	return (-1);
}
