/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)pnmd_group.c	1.4	08/05/20 SMI"

/*
 * pnmd_group.c - Public network group management.
 *
 * Provide the PNM generic groups data structure and all the functions needed by
 * probing and server modules to manage the public network groups.
 */

#include <pnm/pnm_ld.h>
#include "pnmd_group.h"

bkg_status	*nbkg = NULL, *obkg = NULL; /* group list head */

/*
 * Allocate a group struct and add it to the grouplist grp_list.
 * INPUT:  pointer to pointer to bkg_status struct(pointer to grouplist in
 *         which to add the given group, i.e. pointer to nbkg/obkg),
 * RETURN: pointer to group added or NULL on error.
 */
bkg_status *
group_add(bkg_status **grp_list)
{
	bkg_status	*ptr;

	ptr = (bkg_status *) malloc(sizeof (bkg_status));
	if (ptr == NULL) {
		log_syserr("out of memory.");
		return (NULL);
	}
	bzero((char *)ptr, sizeof (bkg_status));
	group_append(grp_list, ptr);
	return (ptr);
}

/*
 * Free the given group and remove it from the given grouplist.
 * INPUT : pointer to bkg_status struct(group to be deleted),
 *	   pointer to pointer to bkg_status struct(grouplist, i.e.
 *         pointer to obkg/nbkg)
 * RETURN :void
 */
void
group_delete(bkg_status *grp, bkg_status **grp_list)
{
	bkg_status *prev;
	bkg_status *current;

	/* Remove this group from grp_list grouplist */
	if (*grp_list == grp) {
		if ((*grp_list)->next)
			*grp_list = (*grp_list)->next;
		else
			*grp_list = NULL;
	} else {
		for (prev = *grp_list, current = (*grp_list)->next;
		    current != NULL; prev = current, current = current->next) {
			if (current == grp) {
				prev->next = current->next;
				break;
			}
		}
	}
	group_free(grp);
}

/*
 * Find the group corresponding to the group->name eg: group1.
 * INPUT: char pointer to group name, and pointer to grouplist.
 * RETURN: pointer to struct bkg_status, NULL if not found.
 */
struct bkg_status *
group_find(char *gname, bkg_status *grp_list)
{
	bkg_status	*bkg;

	for (bkg = grp_list; bkg != NULL; bkg = bkg->next) {
		if (strcmp(bkg->name, gname) == 0)
			return (bkg);
	}
	return (NULL);
}

/*
 * Free this group structure and the pointer also.
 * INPUT: pointer to a bkg_status struct (bkg which needs to be removed )
 * RETURN: void
 */
void
group_free(bkg_status *grp)
{
	bkg_adp *adp;
	bkg_adp *tmp;

	/* Remove the adp list in group */
	for (adp = grp->head; adp != NULL; adp = tmp) {
		tmp = adp->next;
		group_delete_adp(grp, adp);
	}
	free(grp->name);
	free(grp);
}

/*
 * Append the given group struct to the grouplist grp_list.
 * INPUT: pointer to pointer to bkg_status struct(pointer to grouplist in
 *        which to append)
 *        pointer to bkg_status struct (the group to append),
 * RETURN: void
 */
void
group_append(bkg_status **grp_list, bkg_status *grp)
{
	grp->next = *grp_list;
	*grp_list = grp;
}

/*
 * Sets backup group state and syslog the transition. No action taken other
 * than setting the state and syslog. Idempotent.
 * Any callback must be run by the caller.
 * INPUT: pointer to group and new state
 * RETURN: void
 */
void
group_state_transition(bkg_status *grp, int nst)
{
	int	ost;
	const char * const Status_grp[CODES] = {
		"OK",
		"DOWN",
	};
	/*
	 * If the status of the group is same as the status that we want
	 * then return - hence idempotent.
	 */
	if ((ost = grp->status) == nst) {
		log_debug("%s: state_transition: %s -> %s",
		    grp->name, Status_grp[ost], Status_grp[nst]);
		return;
	}

	/* This writes to stderr if debug is on */
	DEBUGP((stderr, "%s: state transition from %s to %s.",
	    grp->name, Status_grp[ost], Status_grp[nst]));

	/* Syslog a message saying that the status of the group has changed */
	/*
	 * SCMSGS
	 * @explanation
	 * A state transition has happened for the IPMP group. Transition to
	 * DOWN happens when all adapters in an IPMP group are determined to
	 * be faulty.
	 * @user_action
	 * If an IPMP group transitions to DOWN state, check for error
	 * messages about adapters being faulty and take suggested user
	 * actions accordingly. No user user action is needed for other state
	 * transitions.
	 */
	(void) sc_syslog_msg_log(msg_pnm, SC_SYSLOG_NOTICE, MESSAGE,
	    "%s: state transition from %s to %s.",
	    grp->name, Status_grp[ost], Status_grp[nst]);

	/* Change the status of the group */
	grp->status = nst;
}

/*
 * A group has been updated so run the callbacks.
 * INPUT: pointer to group.
 * RETURN: void
 */
void
group_updated(bkg_status *grp)
{
	if (grp) {
		/* Verify that all adapters have separate MAC addresses */
		(void) nw_verify_mac_addr();
		/* Verify that all adapters have the same vlan id. */
		(void) group_verify_vlan_id(grp);
		(void) group_run_callbacks(grp, PNM_EVENT_UPDATED, B_FALSE);
	}
}    

/*
 * This function is called only when we determine that the group is down.
 * INPUT: pointer to group
 * RETURN: void
 */
void
group_down(bkg_status *grp)
{
	struct bkg_adp	*adp;
	boolean_t	inst_failover = B_TRUE;

	/*
	 * At this stage, we know the backup group is DOWN.
	 */
	if (grp->status != PNM_STAT_DOWN) {
		group_state_transition(grp, PNM_STAT_DOWN);

		/*
		 * Find out the type of failure and run the callbacks
		 * accordingly
		 */
		for (adp = grp->head; adp; adp = adp->next) {
			if (adp->state != ADP_NOTRUN) {
				inst_failover = B_FALSE;
				break;
			}
		}
		(void) group_run_callbacks(grp, PNM_EVENT_FAILED,
				inst_failover);
	}
}

/*
 * This function is called only when we determine that the group is up.
 * INPUT: pointer to group
 * RETURN: void
 */
void
group_up(bkg_status *grp)
{
	/*
	 * At this stage, we know the backup group is UP.
	 */
	if (grp->status != PNM_STAT_OK) {
		group_state_transition(grp, PNM_STAT_OK);
		(void) group_run_callbacks(grp, PNM_EVENT_REPAIRED, B_FALSE);
	}
}


/*
 * Return the status of the group.
 * INPUT: pointer to the group.
 * RETURN: status of the group.
 */
int
group_status(bkg_status *grp)
{
	int status = PNM_STAT_DOWN; /* Assume group is down */
	bkg_adp *adp;

	for (adp = grp->head; adp != NULL; adp = adp->next) {
		if (adp->state == ADP_OK) {
			status = PNM_STAT_OK;
			break;
		}
	}
	return (status);
}

/*
 * Get all IP addresses hosted on all the adapter instances - both physical
 * and logical IP addresses in this group. If group name is NULL then get all
 * the logical IP addresses (not loopback) hosted on this node. Returns a list
 * of l_addrs. On error returns NULL. Get the IP addresses for the given
 * family.
 * INPUT: pointer to group or NULL, family(v4/v6)
 * RETURN: pointer to a list of l_addrs structures, NULL on error.
 */
l_addr *
group_logip_get(bkg_status *grp, sa_family_t af)
{
	l_addr			*ptr, *last_ptr, *head_ptr;
	bkg_adp			*adp;

	ptr = last_ptr = head_ptr = NULL;

	/* If the group is NULL get all logical IPs on this node */
	if (!grp)
		return (nw_logip_get(NULL, af));

	/* For all the adapters in this group */
	for (adp = grp->head; adp; adp = adp->next) {
		head_ptr = nw_logip_get(adp->adp, af);
		if (ptr) {
			/* Goto the last laddr struct in this list */
			for (last_ptr = ptr; last_ptr->next; ) {
				last_ptr = last_ptr->next;
			}
			last_ptr->next = head_ptr;
		}
		else
			ptr = head_ptr;
	}
	return (ptr);
}

/*
 * Add callbacks from the file "CALLFILE". This is called by pnmd_init().
 * Useful when we have to restart the daemon after a daemon crash. In such a
 * case the callbacks will be present in the CALLFILE and will be added.
 * Thus RGM will not have to reregister the callbacks again. In the case of a
 * clean shutdown (sigterm) we remove this file so when restarting (rebooting)
 * we do not add any previous callbacks and let RGM register all the callbacks.
 * INPUT: void
 * RETURN: void
 */
void
group_add_file_callbacks()
{
	FILE		*fp;
	char		line[CONF_LINELEN];
	char		*key, *cb = NULL;
	struct stat	st;
	bkg_status	*grp;

	DEBUGP((stdout, "group_add_file_callbacks: enter\n"));

	/* Check to see if there are any groups - return if there are none */
	if (!nbkg)
		return;

	/* If CALLFILE is not present or can't open CALLFILE return */
	if (stat(CALLFILE, &st) != 0 || (fp = fopen(CALLFILE, "r")) == NULL)
		return;

	/*
	 * get_line() is defined in str_opt.c.
	 * key = group name. " \t" takes care of " " || "\t".
	 * Callback is in the form:
	 * groupname \t id \t command
	 */
	while (get_line(fp, line) > 0) {
		/* Get the group name in key */
		key = (char *)strtok(line, " \t");
		cb = (char *)strtok(NULL, "");
		if (key == NULL || cb == NULL || strlen(cb) == 0)
			continue;
		/*
		 * Goto the particular group. If the group can't be
		 * found try the next callback.
		 */
		if ((grp = group_find(key, nbkg)) == NULL) {
			log_conferr("Group %s not found.", key);
			continue;
		}
		if (group_add_callback(grp, cb) >= 0) {
			log_debug("%s: picked up callback %s",
			    grp->name, cb);
		}
		/* If we can't pick up a callback we just try the next one */
	}
	(void) fclose(fp);
	DEBUGP((stdout, "group_add_file_callbacks: exit\n"));
}

/*
 * Delete the callback file, if exists.
 */
void
group_delete_file_callbacks()
{
	struct stat	st;

	DEBUGP((stdout, "group_delete_file_callbacks: enter\n"));

	/*
	 * Should not leave the callback file hanging around, since subsequent
	 * pnmd will pick it up and install it. On a clean shutdown we will
	 * receive a sigterm so we should not leave the callback file here.
	 * On a unclean shutdown (kill -9) we will leave the callback file so
	 * that the callbacks can be registered when we start pnmd again
	 * (without having RGM reregister the callbacks).
	 */
	/* If the callback file is present then remove it */
	if (stat(CALLFILE, &st) == 0)
		(void) unlink(CALLFILE);

	DEBUGP((stdout, "group_delete_file_callbacks: exit\n"));
}

/*
 * Add the given callback to the given group and return the index of the
 * callback added in the callback array. This index can then be used to
 * update the callback file. Call is idempotent - if the given callback has
 * already been added we return success.
 * INPUT: pointer to group and pointer to callback structure.
 * RETURN: index of the callback added in the callback array, -1 on error
 */
int
group_add_callback(bkg_status *ptr, char *cb)
{
	uint_t		k;
	int		ind;
	char		*callname, *callcmd;

	/*
	 * When we break a string into tokens separated by " \t" then we are
	 * actually tokenising according to " " or "\t". So if a " " or "\t"
	 * is present in a string then it will be broken into tokens in the
	 * following strtok()
	 */
	/* Get the callback id in callname */
	callname = (char *)strtok(cb, " \t");
	/* In this we could also use "\0" instead of "\n" */
	callcmd = (char *)strtok(NULL, "\n");
	if (callname == NULL || callcmd == NULL ||
	    *callname == 0 || *callcmd == 0)
		return (-1);
	ind = -1;
	/*
	 * Check to see if the callback has been registered already. This is
	 * idempotent in the sense that if the callback has already been
	 * registered then the registered callback is freed and a new copy
	 * of the registered callback is registered so it returns success.
	 */
	for (k = 0; k < ptr->cind; k++) {
		if (strcmp(ptr->callback[k].name, callname) == 0) {
			log_debug(
			    "Re-registering group callback %s", callname);
			free(ptr->callback[k].name);
			free(ptr->callback[k].cmdline);
			ind = (int)k;
			break;
		}
	}

	/*
	 * Allocate storage for the callback and fix the index in the
	 * array where the callback is to be added. Increment ptr->cind also.
	 */
	if (ind == -1) {	/* Callback not already registered */
		if (ptr->cind == 0) {
			ptr->callback = (callb *)
				malloc(sizeof (callb) * CALLB_CHUNK);
		} else if (ptr->cind % CALLB_CHUNK == 0) {
			ptr->callback = (callb *) realloc(ptr->callback,
			    (ptr->cind + CALLB_CHUNK) * sizeof (callb));
		}
		if (ptr->callback == NULL) {
			log_syserr("out of memory.");
			ptr->cind = 0;
			return (-1);
		}
		ind = (int)ptr->cind++;
	}

	/* Now add the callback to the array at the index ind */
	ptr->callback[ind].name = (char *)strdup(callname);
	ptr->callback[ind].cmdline = (char *)strdup(callcmd);

	log_debug("%s: callback registered %s: %s", ptr->name,
	    ptr->callback[ind].name, ptr->callback[ind].cmdline);

	return (ind);
}

/*
 * Remove all the callbacks from the group and also update the callback file.
 * Call is idempotent.
 * INPUT: pointer to the group.
 * RETURN: void
 */
void
group_delete_callbacks(bkg_status *grp)
{
	uint_t k;

	/*
	 * Check if all callbacks have already been removed - in this case
	 * we return which actually means success. Hence this is idempotent.
	 */
	if (!(grp->cind > 0))
		return;

	/*
	 * Free the callbacks. Remove the freed callbacks from the callback
	 * file also, i.e. update the callback file. If the callback
	 * file has already been updated then this call returns success
	 * so this call is idempotent.
	 */
	/* Delete from callback file. */
	(void) group_conf_update(grp, CONF_DELETE, -1);

	for (k = 0; k < grp->cind; k++) {
		free(grp->callback[k].name);
		free(grp->callback[k].cmdline);
	}
	log_debug("%s: callbacks unregistered", grp->name);
	free(grp->callback);
	grp->callback = NULL;
}

/*
 * Routines to execute callbacks in the following 'event's:
 *	PNM_EVENT_FAILED:	A backup group has transitioned
 *				to DOWN state
 *	PNM_EVENT_UPDATED:	An adapter has been added to a backup
 *				group
 *
 * WARNING:
 * The callback command MUST fork() first, and do all processing in the
 * child process. This is to prevent deadlocks.
 * Return the number of callbacks which failed to execute.
 * INPUT: pointer to group and pointer to event
 * RETURN: number of callbacks which failed to execute, -1 on error.
 */
int
group_run_callbacks(bkg_status *grp, const char *event, boolean_t flag)
{
	uint_t		k;
	int		rc, error = 0;
	callb		*cp = NULL;
	char		buf[CONF_LINELEN];


	/* If no registered callbacks or no event occurred return */
	if (grp->callback == NULL || !(grp->cind > 0)) {
		log_debug("%s: run_callback for %s event: none registered",
		    grp->name, event);
		return (0);
	}
	if (event == NULL || strlen(event) == 0) {
		DEBUGP((stdout, "%s: run callback had a NULL event",
		    grp->name));
		/*
		 * SCMSGS
		 * @explanation
		 * The run_callback() routine is called only when an IPMP
		 * group's state changes from OK to DOWN and also when an IPMP
		 * group is updated (adapter added to the group).
		 * @user_action
		 * Save a copy of the /var/adm/messages files on the node.
		 * Contact your authorized Sun service provider for assistance
		 * in diagnosing the problem.
		 */
		(void) sc_syslog_msg_log(msg_pnm, SC_SYSLOG_ERROR, MESSAGE,
		    "%s: run callback had a NULL event", grp->name);
		return (-1);
	}

	/*
	 * Run all the registered callbacks for any event that occurs.
	 * The event and flag is just passed as an argument to the callback
	 * when it is executed. If the flag is B_TRUE we pass nodelay and
	 * if it is B_FALSE we pass delay
	 */
	for (k = 0; k < grp->cind; k++) {
		cp = &(grp->callback[k]);
		(void) snprintf(buf, sizeof (buf),
		    "%s %s %s %s", cp->cmdline, event, grp->name,
		    (flag) ? "nodelay" : "delay");
		log_debug("%s: run_callback: %s", grp->name, buf);
		rc = execute(buf);
		if (rc != 0) {
			/*
			 * Continue even if some callbacks fail to
			 * execute. Inter-callback dependency or
			 * synchronization should be handled by the
			 * callbacks themselves.
			 */
			log_debug("%s: run_callback: failed",
			    grp->name);
			error++;
		}
	}

	return (error);
}

/*
 * This is idempotent in the sense that if something is already deleted then we
 * just return success. If an entry is already updated then also we just
 * return success. Return non-zero on failure.
 * INPUT: group, cmd = ADD/DELETE, index = callback array index or -1.
 * RETURN: 0 on success or -1 on error
 */
int
group_conf_update(bkg_status *grp, int cmd, int ind)
{
	int	fd;
	FILE	*fp = NULL, *tfp = NULL;
	uint_t	keylen;
	uint_t	linelen;
	int	fnd;
	char	key[CONF_LINELEN];
	char	line[CONF_LINELEN];
	char	newline[CONF_LINELEN] = { 0 };
	char	file_tmp[FILENAMELEN];
	char	*file;

	DEBUGP((stdout, "group_conf_update: enter\n"));

	/* CALLFILE is defined in pnm_head.h */
	file = CALLFILE;

	if (ind >= (int)grp->cind)
		return (-1);
	if (ind < 0 && cmd != CONF_DELETE)
		return (-1);

	/*
	 * The key for the callback file normally consists of both
	 * the group name and the callback tag. But
	 * ind == -1 indicates all callbacks need to be removed,
	 * and we only use the group name as the search key.
	 */
	if (cmd == CONF_DELETE && ind == -1) {
		(void) snprintf(key, CONF_LINELEN, "%s", grp->name);
	} else {
		(void) snprintf(key, CONF_LINELEN, "%s\t%s",
		    grp->name, grp->callback[ind].name);
	}
	keylen = strlen(key);

	/* Fill up newline for conf_add */
	if (cmd == CONF_ADD) {
		(void) snprintf(newline, CONF_LINELEN, "%s\t%s\t%s\n",
		    grp->name, grp->callback[ind].name,
		    grp->callback[ind].cmdline);
	}

	/* Open the callback file or create it if it is not open. */
	if ((fd = open(file, O_CREAT|O_RDONLY, 0644)) < 0) {
		log_syserr("can't open file %s.", file);
		return (-1);
	}
	(void) close(fd);

	/* Open the callback file for reading */
	if ((fp = fopen(file, "r")) == NULL) {
		log_syserr("can't open file %s.", file);
		return (-1);
	}

	/* Open a file.tmp file for writing */
	(void) sprintf(file_tmp, "%s.tmp", file);
	if ((tfp = fopen(file_tmp, "w")) == NULL) {
		log_syserr("can't open file %s.", file);
		(void) fclose(fp);
		return (-1);
	}

	fnd = 0;
	if (fp) {
		while (get_line(fp, line) > 0) {
			/*
			 * Check to see if key is present in line eg:
			 * key = A; line = AB, In this example key is not
			 * present in line.
			 */
			/*
			 * If key is not present in line then write the line
			 * to the new file tfp and check the next line.
			 */
			if (strncmp(line, key, keylen) != 0 ||
			    !isspace(line[keylen])) {
				linelen = strlen(line);
				line[linelen] = '\n';
				(void) fwrite(line, 1, linelen + 1, tfp);
				continue;
			}

			/*
			 * This line has the key so if cmd = delete then do
			 * nothing i.e. do not copy this line.
			 */
			if (cmd == CONF_DELETE) {
				continue;
			}

			/*
			 * This line has the key so if cmd = add then write
			 * the newline formed before instead of this line.
			 */
			if (cmd == CONF_ADD && fnd == 0) {
				(void) fwrite(newline, 1,
				    strlen(newline), tfp);
				fnd++;
			}
		}
		(void) fclose(fp);
	}

	/* This takes care of the case if there was no file previously */
	if (cmd == CONF_ADD && fnd == 0)
		(void) fwrite(newline, 1, strlen(newline), tfp);

	/* Close the tmp file */
	(void) fclose(tfp);

	/* Rename the .tmp file to have the name given by file */
	if (rename(file_tmp, file) != 0) {
		log_syserr("rename failed.");
		/* We can also do remove here */
		(void) unlink(file_tmp);
		return (-1);
	}

	DEBUGP((stdout, "group_conf_update: exit\n"));
	return (0);
}

/*
 * Allocate memory for adp and add it to the given group. This adp should
 * be the same as match_adp.
 * INPUT: pointer to bkg_status struct, pointer to bkg_adp struct,
 * RETURN: 0 on success or -1 on error.
 */
int
group_add_adp(bkg_status *grp, bkg_adp *match_adp)
{
	bkg_adp		*ptr;
	adp_inst	*adpi;

	if ((ptr = group_alloc_adp()) == NULL)
		return (-1);
	ptr->adp = (char *)strdup(match_adp->adp);
	ptr->state =  match_adp->state;
	if (match_adp->pii_v4) {
		adpi = (adp_inst *) malloc(sizeof (adp_inst));
		if (adpi == NULL) {
			log_syserr("out of memory.");
			group_free_adp(ptr);
			return (-1);
		}
		bzero(adpi, sizeof (adp_inst));
		ptr->pii_v4 = adpi;
		adpi->present = match_adp->pii_v4->present;
	}
	if (match_adp->pii_v6) {
		adpi = (adp_inst *) malloc(sizeof (adp_inst));
		if (adpi == NULL) {
			log_syserr("out of memory.");
			group_free_adp(ptr);
			return (-1);
		}
		bzero(adpi, sizeof (adp_inst));
		ptr->pii_v6 = adpi;
		adpi->present = match_adp->pii_v6->present;
	}
	group_append_adp(grp, ptr);
	return (0);
}

/*
 * Free this adp and remove it from the given group's adplist.
 * If this is the last adp in the group then make group->head = NULL;
 * INPUT: pointer to a bkg_adp struct (adp which needs to be removed )
 *      : pointer to a group structure
 * RETURN: void
 */
void
group_delete_adp(bkg_status *grp, bkg_adp *adp)
{
	bkg_adp	*prev;
	bkg_adp *current;

	/* Remove this adp from grp->head adplist */
	if (grp->head == adp) {
		if (grp->head->next)
			grp->head = grp->head->next;
		else
			grp->head = NULL;
	} else {
		for (prev = grp->head, current = prev->next;
		    current != NULL;
		    prev = current, current = current->next) {
			if (current == adp) {
				prev->next = current->next;
				break;
			}
		}
	}
	/* Free the memory associated with this adp structure */
	group_free_adp(adp);
}

/*
 * Find the adp corresponding to the adp->adp eg: hme0.
 * INPUT: char pointer to adp name, and pointer to group.
 * RETURN: pointer to struct bkg_adp, NULL if not found.
 */
bkg_adp *
group_find_adp(char *name, bkg_status *grp)
{
	bkg_adp *adp;

	for (adp = grp->head; adp != NULL; adp = adp->next) {
		if (strcmp(adp->adp, name) == 0)
			return (adp);
	}
	return (NULL);
}

/*
 * Free this adp structure and the pointer also.
 * INPUT: pointer to a bkg_adp struct (adp which needs to be removed )
 * RETURN: void
 */
void
group_free_adp(bkg_adp *adp)
{
	/* Free the memory associated with this adp structure */
	free(adp->adp);
	group_free_adp_inst(adp->pii_v4);
	group_free_adp_inst(adp->pii_v6);
	free(adp);
}

/*
 * Free this adp_inst structure.
 * INPUT: pointer to a adp_inst struct (adp_inst which needs to be removed )
 * RETURN: void
 */
void
group_free_adp_inst(adp_inst *adpi)
{
	if (adpi)
		free(adpi);
}

/*
 * Append the given adp to the given group.
 * INPUT: pointer to bkg_adp struct, pointer to bkg_status struct
 * RETURN: void
 */
void
group_append_adp(bkg_status *grp, bkg_adp *adp)
{
	if (grp->head != NULL)
		adp->next = grp->head;
	grp->head = adp;
	log_debug("%s has been added to %s", adp->adp, grp->name);
}

/*
 * Allocate memory for adp and zero it.
 * INPUT: NULL
 * RETURN: pointer to bkg_adp or NULL on error.
 */
bkg_adp *
group_alloc_adp()
{
	bkg_adp	*ptr;

	ptr = (bkg_adp *) malloc(sizeof (bkg_adp));
	if (ptr == NULL) {
		log_syserr("out of memory.");
		return (NULL);
	}
	bzero(ptr, sizeof (bkg_adp));
	return (ptr);
}

/* Get the number of adps in this group */
uint_t
group_numadp_get(bkg_status *grp)
{
	bkg_adp *adp;
	uint_t count = 0;

	for (adp = grp->head; adp; adp = adp->next) {
		count++;
	}

	return (count);
}	

void
group_check_and_print_mesg(bkg_adp *adp, char *name)
{
	/*
	 * We know that if the state of the adp is OK then it could be
	 * hosting HA IP addresses on it so we should warn the user.
	 * If if_mpadm (1M) was run then the adp state would be OFFLINE
	 * instead of OK and we would not warn the user. If the adp state
	 * is NOTRUN, FAULTY OR STANDBY then we know that there can't be
	 * any HA IP addresses hosted on the adp so we do not need to warn
	 * user.
	 */
	if (adp->state == ADP_OK) {
		DEBUGP((stdout, "%s has been removed from %s.\n"
		    "Make sure that all HA IP addresses hosted on %s"
		    " are moved.",
		    adp->adp, name, adp->adp));
		/*
		 * SCMSGS
		 * @explanation
		 * We do not allow removing of an adapter from an IPMP group.
		 * The correct way to DR an adapter is to use if_mpadm(1M).
		 * Therefore we notify the user of the potential error.
		 * @user_action
		 * This message is informational; no user action is needed if
		 * the DR was done properly (using if_mpadm).
		 */
		(void) sc_syslog_msg_log(msg_pnm,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "%s has been removed from %s.\n"
		    "Make sure that all HA IP addresses hosted on %s"
		    " are moved.",
		    adp->adp, name, adp->adp);
	}
}

/*
 * Returns 0 on success and on error it returns
 * PNM_ENONHOMOGENOUS - all adps in the group do not have similar instances
 * (IPv4/IPv6) plumbed, hence the group is said to be non-homogenous.
 */
int
group_instances(bkg_status *group, int *resultp)
{
	int result = 0;
	bkg_adp *adp;
	boolean_t v4 = B_FALSE;
	boolean_t v6 = B_FALSE;

	for (adp = group->head; adp; adp = adp->next) {
		if (adp->pii_v4)
			v4 = B_TRUE;
		if (adp->pii_v6)
			v6 = B_TRUE;
	}

	if (v4) {
		for (adp = group->head; adp; adp = adp->next) {
			if (!adp->pii_v4) {
				log_conferr("All adps in %s do not have similar"
				    " instances (IPv4/IPv6) plumbed.", group);
				return (PNM_ENONHOMOGENOUS);
			}
		}
		result |= PNMV4MASK;
		DEBUGP((stdout, "group_instances: IPv4 found\n"));
	}

	if (v6) {
		for (adp = group->head; adp; adp = adp->next) {
			if (!adp->pii_v6) {
				log_conferr("All adps in %s do not have similar"
				    " instances (IPv4/IPv6) plumbed.", group);
				return (PNM_ENONHOMOGENOUS);
			}
		}
		result |= PNMV6MASK;
		DEBUGP((stdout, "group_instances: IPv6 found\n"));
	}

	if (resultp != NULL)
		*resultp = result;

	return (0);
}

/*
 * Verifies that all adapters belonging to the specified IPMP group belong
 * to the same (tagged) VLAN. We allow configurations where some adapters
 * have (the same) VLAN tags while others have no VLAN tags with the
 * assumption that the untagged adapters are connected to switch ports that
 * assign then the same tag implictly. The VLAN tags are computed the same
 * way as in the rest of Solaris - vlan_tag = instance_number / 1000.
 */
#ifdef linux
int
group_verify_vlan_id(struct bkg_status *bgp)
{
	return (0);
}
#else
int
group_verify_vlan_id(struct bkg_status *bgp)
{
	bkg_adp	*bap;
	uint_t	adp_vid;
	uint_t	group_vid = 0;
	int	tag_cnt = 0;
	int	untag_cnt = 0;

	for (bap = bgp->head; bap != NULL; bap = bap->next) {
		adp_vid = get_physical_inst(bap->adp) / 1000;
		if (adp_vid == 0) {
			untag_cnt++;
			continue;
		} else {
			tag_cnt++;
		}
		if (group_vid == 0) {
			group_vid = adp_vid;
			continue;
		}
		if (group_vid != adp_vid) {
			log_debug("IPMP group %s has adapters that do not"
			    " belong to the same VLAN.", bgp->name);
			/*
			 * SCMSGS
			 * @explanation
			 * Sun Cluster has detected that the named IPMP group
			 * has adapters that belong to different VLANs. Since
			 * all adapters that participate in an IPMP group must
			 * host IP addresses from the same IP subnet, and an
			 * IP subnet cannot span multiple VLANs, this is not a
			 * legal configuration.
			 * @user_action
			 * Fix the IPMP configuration by updating the
			 * appropriate /etc/hostname&star; file(s) so that
			 * each IPMP group spans only the adapters that host
			 * IP addresses from the same IP subnet.
			 */
			(void) sc_syslog_msg_log(msg_pnm, SC_SYSLOG_ERROR,
			    MESSAGE, "IPMP group %s has adapters that do"
			    " not belong to the same VLAN.", bgp->name);
			return (-1);
		}
	}
	if (tag_cnt != 0 && untag_cnt != 0) {
		log_debug("IPMP group %s has tagged VLAN adapter(s) as"
		    " well as untagged adapter(s). Can not verify if they"
		    " belong to the same VLAN.", bgp->name);
		/*
		 * SCMSGS
		 * @explanation
		 * All adapters that participate in an IPMP group must host IP
		 * addresses from the same subnet. Sun Cluster has detected
		 * that the named IPMP group has both tagged VLAN adapters and
		 * untagged adapters that participate in the IPMP group. Sun
		 * Cluster can not determine if the are in the same VLAN.
		 * @user_action
		 * Make sure that the untagged adapters participate in the
		 * same VLAN as the tagged VLAN adapters.
		 */
		(void) sc_syslog_msg_log(msg_pnm, SC_SYSLOG_WARNING,
		    MESSAGE, "IPMP group %s has tagged VLAN adapter(s) as"
		    " well as untagged adapter(s). Can not verify if they"
		    " belong to the same VLAN.", bgp->name);
	}
	return (0);
}
#endif
