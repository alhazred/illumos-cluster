/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)pnmd_probe_solaris_ipmp.c	1.7	09/02/18 SMI"

/*
 * pnmd_probe_solaris_ipmp.c - PNM probing module for Solaris/IPMP groups.
 *
 * This module aims to implement a monitoring of all the public network groups
 * currently defined in the cluster. This module provide a (optional) commu-
 * -nication channel between the kernel/networking entity and the pnm daemon
 * to receive notifications from the kernel when an change occurs on a public
 * network group of adapters. This channel when exists offers an event driven
 * mechanism (e.g. the solaris version uses the routing socket as communication
 * channel). If no such channel are available (e.g. Linux AS 3.0), the backup
 * solution is to use the active polling method.
 * When a notification is received (or when it's time to have a look to the
 * current kernel networking configuration), all the public network groups
 * information are loaded into PNM group data structures. Then a check procedure
 * is triggered to detect all the potential changes. A group updated or a group
 * failed callback information is sent to all the PNM clients previously regis-
 * -tered with the daemon.
 *
 * This module relies on a generic group management module (pnmd_group.c) but
 * depends on the OS specificities in term of networking interfaces.
 *
 */

#include <pnm/pnm_ld.h>
#include <sys/cl_events.h>
#include "pnmd_group.h"


/*
 * The interval between two public network groups monitoring.
 * Default value is 60 seconds.
 * However, when using a probing module that is not event design oriented
 * the suggestion is to configure this value to 1-5 seconds, depending on
 * the level of availability expected.
 */
int probing_timeout = 60000;

/*
 * The communication channel handle.
 */
static int probe_handle = -1;

static boolean_t probe_init_done = B_FALSE;
static boolean_t need_if_scan = B_FALSE;

static int probe_snapshot_int(bkg_status **grp_list);

/*
 * Open the probing communication channel.
 * Create a routing socket for receiving RTM_IFINFO messages.
 * Make the flags non blocking.
 * INPUT: void
 * RETURN: 0 on success or -1 on error
 */
int
probe_open()
{
	int s;
	int flags;

	DEBUGP((stdout, "probe_open: enter\n"));

	/* For v4 we can have the family as AF_INET instead of AF_UNSPEC */
	/* The domain is PF_ROUTE and only SOCK_RAW is supported */
	s = socket(PF_ROUTE, SOCK_RAW, AF_UNSPEC);
	if (s == -1) {
		log_syserr("socket failed.");
		return (-1);
	}

	/* Set the socket as non-blocking */
	if ((flags = fcntl(s, F_GETFL, 0)) < 0) {
		log_syserr("fcntl failed.");
		(void) close(s);
		return (-1);
	}
	if ((fcntl(s, F_SETFL, flags | O_NONBLOCK)) < 0) {
		log_syserr("fcntl failed.");
		(void) close(s);
		return (-1);
	}

	/* Add the socket to the pollfds */
	if (poll_add(s) == -1) {
		(void) close(s);
		return (-1);
	}

	probe_handle = s;

	DEBUGP((stdout, "probe_open: exit, probe_handle = %d\n", probe_handle));

	return (0);
}

/*
 * Close the probing communication channel.
 * INPUT: void
 * RETURN: void
 */
void
probe_close()
{
	DEBUGP((stdout, "probe_close: enter\n"));
	(void) close(probe_handle);
	DEBUGP((stdout, "probe_close: exit\n"));
}

/*
 * Check if the communication handle given in argument is the probing
 * handle. In such a case, read a check the message received on the channel
 * and call probe_check() if needed.
 * INPUT: a communication channel
 * RETURN: 0 on success or -1 otherwise
 */
int
probe_process(int handle)
{
	int 	nbytes;
	int64_t msg[256];
	struct rt_msghdr *rtm;

	if (handle != probe_handle)
		return (-1);

	/*
	 * Retrieve as many routing socket messages as possible, and try to
	 * empty the routing socket. Initiate full scan of interfaces if needed.
	 */
	DEBUGP((stdout, "probe_process: enter\n"));

	/*
	 * Read as many messages as possible and try to empty
	 * the routing socket.
	 */
	for (; pnmd_exiting == 0; ) {
		/*
		 * If need_if_scan = TRUE then we should just empty the rtsock
		 * as soon as possible and then go ahead and check the adps -
		 * rather than keep reading the messages from the rtsock and
		 * scanning them one by one to see if we need to check all
		 * the adps. Since it is already TRUE we might as well just
		 * empty the socket.
		 */
		do
			nbytes = read(handle, msg, sizeof (msg));
		while (need_if_scan == B_TRUE && nbytes > 0);
		/* end of do - while */
		if (need_if_scan == B_TRUE || nbytes <= 0)
			break; /* out of for loop */

		/* Scan the message */
		rtm = (struct rt_msghdr *)msg;
		if (rtm->rtm_version != RTM_VERSION) {
			log_debug("probe_process: version %d "
			    "not understood", rtm->rtm_version);
			break; /* out of the for loop - version no good */
		}

		/*
		 * A routing socket message is not generated when a Logical
		 * IP address is just added to the interface - it has to be
		 * brought up for the message to be generated.
		 */
		switch (rtm->rtm_type) {
		case RTM_NEWADDR:  /* Address being added to iface */
		case RTM_DELADDR:  /* Address being removed from iface */
		case RTM_IFINFO:   /* iface going up/down etc. */

			/* Now we want to scan all the adapters */
			need_if_scan = B_TRUE;
			break; /* out of switch */

		case RTM_ADD:		/* Add route */
		case RTM_DELETE:	/* Delete route */
		case RTM_CHANGE:	/* Change gateway, metrics or flags */
		case RTM_OLDADD:   	/* Caused by SIOCADDRT */
		case RTM_OLDDEL:   	/* Caused by SIOCDELRT */
		case RTM_GET:		/* Report Metrics */
		case RTM_LOSING:	/* Kernel Suspects Partitioning */
		case RTM_REDIRECT:	/* Told to use different route */
		case RTM_MISS:		/* Lookup failed on this address */
		case RTM_LOCK:		/* Fix specified metrics */
		case RTM_RESOLVE:	/* Req to resolve dst to LL addr */
		default:		/* Not interesting */
			log_debug("Uninteresting routing socket message");
			break; /* out of switch */
		}

	} /* end of for */

	/* If we got an interesting message check all the adapters */
	if (need_if_scan)
		probe_check();

	DEBUGP((stdout, "probe_process: exit\n"));

	return (0);
}

/*
 * Takes a snapshot of the kernel adapters and form an ipmp group list
 * with nbkg at the head. First sync nbkg grouplist from obkg grouplist.
 * At the end update the grp_list to point to the new grouplist head.
 * Remember that *grp_list = nbkg or obkg.
 * INPUT: void
 * RETURN: 0 on success or -1 on error.
 */
int
probe_snapshot()
{
	DEBUGP((stdout, "probe_snapshot: enter\n"));

	if (probe_init_done == B_FALSE) {
		/* Called from main: initialize both obkg and nbkg */
			if (probe_snapshot_int(&obkg) != 0)
				return (-1);
			probe_init_done = B_TRUE;
	}

	if (probe_snapshot_int(&nbkg) != 0)
		return (-1);

	DEBUGP((stdout, "probe_snapshot: exit\n"));

	return (0);
}

static int
probe_snapshot_int(bkg_status **grp_list)
{
	uint_t			n;
	uint_t			lif_cnt; /* no. of ifs */
	int			so_v4 = 0; /* socket descriptor */
	int			so_v6 = 0;
	int			so;
	uint_t			inst;	/* ordinal value of IP */
	char			*name; /* name of adp i.e. hme0 */
	char			*buf = NULL;
	struct lifconf		lifc;
	struct lifreq		lifr, lifrflag, *lifrp = NULL;
	bkg_status		*match, *bgp, *tmp;
	bkg_status		*exist_group = NULL;
	sa_family_t		af; /* family, v4/v6 */
	bkg_adp			*match_adp, *adp, *ptr, *tmp_adp;
	adp_inst		*adpi; /* adp instance */
	int 			same_group, group_exists, in_a_group;
	int			try = 0;
	struct lifnum		lifn;

	DEBUGP((stdout, "probe_snapshot_int: enter, group list = %p\n",
		grp_list));

	/* Remove stuff from *grp_list, considering obkg as the true copy */
	/* Sync *grp_list with obkg - delete extra structs, and callbacks */
	for (bgp = *grp_list; bgp; bgp = tmp) {
		tmp = bgp->next;
		match = group_find(bgp->name, obkg);
		if (!match) {
			/* Delete this group and the callbacks */
			log_debug("probe_snapshot(): deleting group\n");
			group_delete_callbacks(bgp);
			group_delete(bgp, grp_list);
			continue; /* Next group - bgp */
		}

		/* Group matches */
		for (adp = bgp->head; adp; adp = tmp_adp) {
			tmp_adp = adp->next;
			match_adp = group_find_adp(adp->adp, match);
			if (!match_adp) {
				group_delete_adp(bgp, adp);
				if (bgp->head == NULL) {
					group_delete_callbacks(bgp);
					group_delete(bgp, grp_list);
				}
				continue; /* Next adp */
			}

			/* Adapter is found - Check for instances */
			if (!match_adp->pii_v4) {
				group_free_adp_inst(adp->pii_v4);
				adp->pii_v4 = NULL;
			}
			if (!match_adp->pii_v6) {
				group_free_adp_inst(adp->pii_v6);
				adp->pii_v6 = NULL;
			}
			/* Delete adp if both instances are NULL */
			if (!adp->pii_v4 && !adp->pii_v6) {
				group_delete_adp(bgp, adp);
				if (bgp->head == NULL) {
					group_delete_callbacks(bgp);
					group_delete(bgp, grp_list);
				}
			}
		}
	}

	/*
	 * Here we should remember that we need two sockets for doing ioctls
	 * v4 and v6. First we do a SIOCGLIFCONF with AF_UNSPEC on a v4 socket
	 * and then for every instance (v4/v6) we must do the ioctls on the
	 * particular socket..either v4 or v6.
	 */
	/* Get kernel state into nbkg */
	if ((so_v4 = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
		log_syserr("socket failed.");
		goto probe_snapshot_error;
	}
	if ((so_v6 = socket(AF_INET6, SOCK_DGRAM, 0)) < 0) {
		log_syserr("socket failed.");
		goto probe_snapshot_error;
	}
retry:
	/*
	 * We use SIOCGLIFNUM here. Since new interfaces may be
	 * configured after SIOCGLIFNUM, and we would get an error from
	 * SIOCGLIFCONF (EINVAL) we will try this twice. Initially we will
	 * allocate some extra buffer space.
	 */
	try++;
	lifn.lifn_family = AF_UNSPEC;

	/*
	 * We are only interested in the physical interfaces here and
	 * the information about logical instances is not needed.
	 * Since, physical interfaces can't be configured in the local zones,
	 * no need to pass the LIFC_ALLZONES flag in the following ioctl call.
	 * LIFC_UNDER_IPMP flag is used to retrieve the interfaces under the
	 * psuedo-ipmp interface. This flag is available in Solaris
	 * release with Clearview project.
	 */
#ifdef	LIFC_UNDER_IPMP
	lifn.lifn_flags = LIFC_UNDER_IPMP;
#else
	lifn.lifn_flags = 0;
#endif

	if (ioctl(so_v4, SIOCGLIFNUM, (char *)&lifn) < 0) {
		log_syserr("SIOCGLIFNUM failed.");
		goto probe_snapshot_error;
	}
	lif_cnt = (uint_t)(lifn.lifn_count + 1);

	/*
	 * Retrieve i/f configuration of machine i.e. all the networks
	 * it is connected to. Free buf if required.
	 */
	if (buf != NULL)
		free(buf);
	buf = (char *)malloc(lif_cnt * sizeof (struct lifreq));
	if (buf == NULL) {
		log_syserr("out of memory.");
		goto probe_snapshot_error;
	}

	(void) memset(&lifc, 0, sizeof (lifc));
	lifc.lifc_family = AF_UNSPEC; /* to get v4 and v6 instances */
	lifc.lifc_len = (int)(lif_cnt * sizeof (struct lifreq));
	/*
	 * LIFC_UNDER_IPMP flag is used to retrieve the interfaces under the
	 * psuedo-ipmp interface. This flag is available in Solaris release
	 * with Clearview project.
	 */
#ifdef	LIFC_UNDER_IPMP
	lifc.lifc_flags = LIFC_UNDER_IPMP;
#endif
	lifc.lifc_buf = buf;
	if (ioctl(so_v4, SIOCGLIFCONF, (char *)&lifc) < 0) {
		/*
		 * EINVAL is commonly encountered, when things change
		 * underneath us rapidly, (eg. at boot, when new interfaces
		 * are plumbed successively) and the kernel finds the buffer
		 * size we passed as too small. We will retry twice.
		 */
		/* CSTYLED */
		if (errno == EINVAL && try < 2) /*lint !e746 */
			goto retry;
		log_syserr("SIOCGLIFCONF failed.");
		goto probe_snapshot_error;
	}

	/* For each i/f on machine */
	lifrp = (struct lifreq *)lifc.lifc_req;
	for (n = 0; n < (uint_t)lifc.lifc_len / sizeof (struct lifreq);
	    n++, lifrp++) {
		af = lifrp->lifr_addr.ss_family;
		so = (af == AF_INET) ? so_v4 : so_v6;
		name = lifrp->lifr_name;
		DEBUGP((stdout, "probe_snapshot: adp = %s family = %d\n",
		    name, af));
		/*
		 * Returns 0 for physical adp, otherwise returns the
		 * ordinal value of the logical IP. eg. for
		 * tr2, it will return 0, whilst for tr2:3, it
		 * will return 3. get_logical_inst is defined in pnmd_util.c.
		 */
		inst = get_logical_inst(name);

		/* We only want to check the physical adapters */
		if (inst != 0)
			continue;

		/* If the adapter is clprivnet or lo0 don't do anything */
		if (strncmp(name, "clprivnet", strlen("clprivnet")) == 0 ||
		    strncmp(name, "lo0", strlen("lo0")) == 0)
			continue;

		/* Assume that the adapter is not in an ipmp group */
		in_a_group = 0;

		bzero(&lifrflag, sizeof (lifrflag));
		bcopy(name, lifrflag.lifr_name, strlen(name) + 1);

		/* Get flags - state of adp */
		if (ioctl(so, SIOCGLIFFLAGS, (char *)&lifrflag) < 0) {
			/*
			 * If the interface has been removed continue since we
			 * will get a new routing socket message anyway
			 */
			if (errno == ENXIO) {
				group_delete_adp(bgp, ptr);
				if (bgp->head == NULL)
					group_delete(bgp, grp_list);
				continue; /* ignore this interface */
			}
			log_syserr("SIOCGLIFFLAGS failed.");
			goto probe_snapshot_error;
		}

		/*
		 * If the interface is an IPMP psuedo-interface,
		 * ignore it and continue to get flags for other
		 * interfaces.
		 */
#ifdef	IFF_IPMP
		if (lifrflag.lifr_flags & IFF_IPMP)
			continue;
#endif

		bzero(&lifr, sizeof (lifr));
		bcopy(name, lifr.lifr_name, strlen(name) + 1);
		/*
		 * Get group name of adapter instance. Set in_a_group
		 * accordingly.
		 */
		(void) memset(lifr.lifr_groupname, 0,
		    sizeof (lifr.lifr_groupname));
		if (ioctl(so, SIOCGLIFGROUPNAME, (caddr_t)&lifr) < 0) {
			/*
			 * if the interface is not there anymore ignore it and
			 * goto the next one since we'll get a routing socket
			 * message due to the removal of the interface.
			 */
			if (errno == ENXIO)
				continue;	/* next interface */
			log_syserr("SIOCGLIFGROUPNAME failed.");
			goto probe_snapshot_error;
		}
		if (strlen(lifr.lifr_groupname) > 0)
			in_a_group = 1;

		DEBUGP((stdout, "probe_snapshot:%s, %s\n", name,
		    lifr.lifr_groupname));

		/*
		 * See if this adp is already there in the present *grp_list.
		 * match will be the group it is present in and match_adp
		 * will be the adapter itself.
		 */
		match_adp = NULL;
		for (match = *grp_list; match; match = match->next) {
			match_adp = group_find_adp(name, match);
			if (match_adp)
				break;
		}

		/*
		 * If adapter is already present then we need to see if it is
		 * in the same group. We are only concerned with adapters that
		 * are in an ipmp group.
		 */
		same_group = 0;
		if (match_adp && in_a_group) {
			if (strcmp(lifr.lifr_groupname, match->name) == 0)
				same_group = 1;
		}

		/*
		 * If the adapter is not found at all or not in the same
		 * group then allocate a new adp. We are only concerned with
		 * adapters that are in an ipmp group.
		 */
		ptr = NULL;
		if ((in_a_group) && (!match_adp || !same_group)) {
			/* Allocate adp structure */
			log_debug("probe_snapshot: new adp");
			if ((ptr = group_alloc_adp()) == NULL)
				goto probe_snapshot_error;
			ptr->adp = strdup(name);
		}

		/*
		 * Check to see if the adp's current group already exists in
		 * the grouplist.
		 */
		group_exists = 0;
		if (in_a_group) {
			for (bgp = *grp_list; bgp; bgp = bgp->next) {
				if (strcmp(lifr.lifr_groupname, bgp->name)
				    == 0) {
					group_exists = 1;
					break;
				}
			}
		}
		DEBUGP((stdout, "probe_snapshot:%s, %s, sg = %d, ge = %d\n",
		    name, lifr.lifr_groupname, same_group, group_exists));

		/*
		 * If the adp's current group does not exist then
		 * we have to allocate a completely new group - bgp.
		 */
		if (!group_exists && in_a_group) {
			/* Create a new group */
			log_debug("probe_snapshot: new group");
			if ((bgp = group_add(grp_list)) == NULL)
				goto probe_snapshot_error;
			bgp->name = strdup(lifr.lifr_groupname);
			group_append_adp(bgp, ptr);
			bgp->status = PNM_STAT_OK;
		}

		/*
		 * If the adp is not in the same group now then we need to
		 * remove and free it from its previous group (match). If the
		 * group becomes empty in the process then remove the group
		 * from the grouplist.
		 */
		if (!same_group && match_adp) {
			group_delete_adp(match, match_adp);
			if (match->head == NULL)
				group_delete(match, grp_list);
		}

		/*
		 * Now if the adapter is not in a group then we can ignore it
		 * completely and not get its status etc.
		 */
		if (!in_a_group)
			continue;

		/*
		 * Now find the group in the grouplist which matches this
		 * adp's group.
		 */
		for (bgp = *grp_list; bgp; bgp = bgp->next) {
			if (strcmp(lifr.lifr_groupname, bgp->name) == 0) {
				exist_group = bgp;
				break;
			}
		}

		/*
		 * If the adapter has been allocated and the group exists then
		 * we need to append it to the group.
		 */
		if (ptr && group_exists)
			group_append_adp(exist_group, ptr);

		/*
		 * If a new adp was not allocated then get new state for
		 * the old adp (match_adp).
		 */
		if (!ptr)
			ptr = match_adp;

		/*
		 * Now get the data from the kernel for this adp instance.
		 * Remember that if an adapter instance changes its flags it
		 * is still present in the group. i.e. if an adapter instance
		 * has failed it is not removed from the list of adapters in
		 * the group. Set the present flag to 1 - which means that the
		 * adp instance was not unplumbed, and is still present.
		 */
		switch (af) {
		case AF_INET:
			if (!ptr->pii_v4) {
				adpi = (adp_inst *) malloc(sizeof (adp_inst));
				if (adpi == NULL) {
					log_syserr("out of memory.");
					goto probe_snapshot_error;
				}
				ptr->pii_v4 = adpi;
			}
			ptr->pii_v4->present = B_TRUE;
			break;

		case AF_INET6:
			if (!ptr->pii_v6) {
				adpi = (adp_inst *) malloc(sizeof (adp_inst));
				if (adpi == NULL) {
					log_syserr("out of memory.");
					goto probe_snapshot_error;
				}
				ptr->pii_v6 = adpi;
			}
			ptr->pii_v6->present = B_TRUE;
			break;

		default:
			/* We could do an assert here */
			log_syserr("wrong address family.");
			goto probe_snapshot_error;
		}

		/*
		 * If the IFF_RUNNING flag is off then the adp has no carrier
		 * and has failed - so we mark it NOTRUN.
		 * If the IFF_FAILED flag is on then the adp is faulty.
		 * If the IFF_STANDBY flag is on then the kernel sets the
		 * read only flag IFF_INACTIVE on - and an adp is considered
		 * to be standby only if both the flags are on. If the adp
		 * is standby and the IFF_INACTIVE flag is off then the
		 * adp is actually ok since it can be used for traffic.
		 * The IFF_INACTIVE flag is turned off when the other active
		 * adapters in the IPMP group have failed and this standby
		 * adp is used for traffic. Previously, The INACTIVE flag
		 * was never used without the STANDBY flag also on.
		 * With the fix of 4796820/5084073 INACTIVE flag can be on
		 * alone when initially active adapter in an active/standby
		 * IPMP group gets repaired after failure with no failback
		 * option in /etc/default/mpathd file.
		 * If the IFF_OFFLINE flag is on then the adp is offline.
		 */
		if ((lifrflag.lifr_flags & IFF_RUNNING) == 0)
			ptr->state = ADP_NOTRUN;
		else if (lifrflag.lifr_flags & IFF_FAILED)
			ptr->state = ADP_FAULTY;
		else if (lifrflag.lifr_flags & IFF_INACTIVE)
			ptr->state = ADP_STANDBY;
		else if (lifrflag.lifr_flags & (uint64_t)IFF_OFFLINE)
			ptr->state = ADP_OFFLINE;
		else
			ptr->state = ADP_OK;

		DEBUGP((stdout, "probe_snapshot: state = %d\n", ptr->state));
	}

	/*
	 * Now loop through all the adapter instances and see if the present
	 * flag is on - if not then we have to remove this instance since it
	 * means that the instance has been unplumbed. Also set all the other
	 * present flags to 0 - so that we can recheck next time.
	 */
	for (bgp = *grp_list; bgp; bgp = tmp) {
		tmp = bgp->next;
		for (adp = bgp->head; adp; adp = tmp_adp) {
			tmp_adp = adp->next;
			if (adp->pii_v4) {
				if (!adp->pii_v4->present) {
					group_free_adp_inst(adp->pii_v4);
					adp->pii_v4 = NULL;
				}
				else
					adp->pii_v4->present = B_FALSE;
			}
			if (adp->pii_v6) {
				if (!adp->pii_v6->present) {
					group_free_adp_inst(adp->pii_v6);
					adp->pii_v6 = NULL;
				}
				else
					adp->pii_v6->present = B_FALSE;
			}
			/* Delete the adapter if both instances are NULL */
			if (!adp->pii_v4 && !adp->pii_v6) {
				group_delete_adp(bgp, adp);
				if (bgp->head == NULL) {
					group_delete_callbacks(bgp);
					group_delete(bgp, grp_list);
				}
				continue; /* Next adp */
			}
		}
	}

	/* This is for debugging purposes */
	DEBUGP((stderr, "probe_snapshot: dump nbkg and obkg\n"));
	for (bgp = nbkg; bgp; bgp = bgp->next) {
		DEBUGP((stdout, "probe_snapshot: nbkg name = %s\n", bgp->name));
	}
	for (bgp = obkg; bgp; bgp = bgp->next) {
		DEBUGP((stdout, "probe_snapshot: obkg name = %s\n", bgp->name));
	}

	(void) close(so_v4);
	(void) close(so_v6);
	free(buf);

	DEBUGP((stdout, "probe_snapshot_int: exit\n"));

	return (0);

probe_snapshot_error:

	DEBUGP((stdout, "probe_snapshot_int: error\n"));

	/* Free the group list */
	for (; *grp_list; *grp_list = bgp) {
		bgp = (*grp_list)->next;
		group_free(*grp_list);
	}
	if (so_v4 > 0)
		(void) close(so_v4);
	if (so_v6 > 0)
		(void) close(so_v6);
	if (buf != NULL)
		free(buf);
	return (-1);
}

/*
 * Check to see if any of the ipmp groups have failed or have been updated.
 * In either case run the appropriate callbacks. This supports both v4 and v6.
 * Check all the adapters to see if there are any ipmp group down - or group
 * updated (an adapter has been added to an ipmp group), if so run the
 * callbacks registered for the group. Also update the obkg grouplist
 * with the current information from the nbkg grouplist (got from the
 * kernel by ioctls - probe_snapshot()), considering the nbkg grouplist to have
 * the true grouplist state. This routine will post sysevents also.
 * INPUT: NULL.
 */
void
probe_check()
{
	struct bkg_adp		*adp, *match_adp, *tmp_adp;
	struct bkg_status	*bgp, *match, *tmp;
	int			grp_status; /* group status */
	int			error = 0;
	char			tmp_name[NAMELEN];
	uint_t			count = 0;

	DEBUGP((stdout, "probe_check(): enter\n"));
	need_if_scan = B_FALSE;

	/* Get an updated set of data structures - nbkg from the kernel */
	if ((probe_snapshot_int(&nbkg)) != 0)
		return;

	/* Now compare the new data structures with the old copy */
	for (bgp = nbkg; bgp; bgp = bgp->next) {
		match = group_find(bgp->name, obkg);

		if (match) { /* not a new group */
			grp_status = PNM_STAT_OK; /* Assume UP */
			count = 0;	/* initialize count */

			for (adp = bgp->head; adp; adp = adp->next) {

				/* Check state of adp - update count */
				if (adp->state == ADP_FAULTY ||
				    adp->state == ADP_OFFLINE ||
				    adp->state == ADP_NOTRUN)
					count++;

				/* See if adp is present in the old copy */
				match_adp = group_find_adp(adp->adp, match);

				if (!match_adp) { /* new adp in this bgp */
					group_updated(bgp);

					/* Add a new adapter to match */
					if (group_add_adp(match, adp) != 0)
						return;

					/*
					 * Send a GROUP_MEMBER_CHANGE
					 * adp added event
					 */
					error = sc_publish_event(
					ESC_CLUSTER_IPMP_GROUP_MEMBER_CHANGE,
					CL_EVENT_PUB_NET,
					CL_EVENT_SEV_INFO,
					CL_EVENT_INIT_OPERATOR, 0, 0, 0,
					IPMP_GROUP_NAME,
					SE_DATA_TYPE_STRING, bgp->name,
					IPMP_GROUP_OPERATION,
					SE_DATA_TYPE_UINT32, IPMP_IF_ADD,
					IPMP_IF_NAME, SE_DATA_TYPE_STRING,
					adp->adp, NULL);
					if (error)
						log_syserr("posting of an IPMP"
						    " event failed");
					error = 0;

					continue; /* Next adp in this group */
				}

				/*
				 * Compare state of match_adp with adp and if
				 * different then send a ESC_IPMP_IF_CHANGE
				 * event. Update state of match_adp.
				 */
				if (match_adp->state != adp->state) {
					match_adp->state = adp->state;
					error = sc_publish_event(
					    ESC_CLUSTER_IPMP_IF_CHANGE,
					    CL_EVENT_PUB_NET,
					    (adp->state == ADP_OK) ?
					    CL_EVENT_SEV_INFO :
					    CL_EVENT_SEV_WARNING,
					    CL_EVENT_INIT_SYSTEM, 0, 0, 0,
					    IPMP_GROUP_NAME,
					    SE_DATA_TYPE_STRING, bgp->name,
					    IPMP_IF_NAME, SE_DATA_TYPE_STRING,
					    adp->adp, IPMP_IF_STATE,
					    SE_DATA_TYPE_UINT32,
					    (adp->state == ADP_OK) ?
					    IPMP_IF_OK : IPMP_IF_FAILED, NULL);
					if (error)
						log_syserr("posting of an IPMP"
						    " event failed");
					error = 0;
				}

			} /* Next adp in this group */

			if (count == group_numadp_get(bgp))
				grp_status = PNM_STAT_DOWN;

			if (match->status == PNM_STAT_OK &&
			    grp_status == PNM_STAT_DOWN) {
				/*
				 * Run the callbacks and change the status
				 * also.
				 */
				group_down(bgp);

				/* Change the status of the old copy also */
				match->status = PNM_STAT_DOWN;

				/* Send a ESC_IPMP_GROUP_STATE = down event */
				error = sc_publish_event(
				    ESC_CLUSTER_IPMP_GROUP_STATE,
				    CL_EVENT_PUB_NET, CL_EVENT_SEV_ERROR,
				    CL_EVENT_INIT_SYSTEM, 0, 0, 0,
				    IPMP_GROUP_NAME, SE_DATA_TYPE_STRING,
				    bgp->name, IPMP_GROUP_STATE,
				    SE_DATA_TYPE_UINT32, IPMP_GROUP_FAILED,
				    NULL);
				if (error)
					log_syserr("posting of an IPMP"
					    " event failed");
				error = 0;
			}

			if (grp_status == PNM_STAT_OK &&
			    match->status == PNM_STAT_DOWN) {
				/*
				 * Run the callbacks and change the status
				 * also.
				 */
				group_up(bgp);
				match->status = PNM_STAT_OK;

				/* Send a ESC_IPMP_GROUP_STATE = up event */
				error = sc_publish_event(
				    ESC_CLUSTER_IPMP_GROUP_STATE,
				    CL_EVENT_PUB_NET, CL_EVENT_SEV_INFO,
				    CL_EVENT_INIT_SYSTEM, 0, 0, 0,
				    IPMP_GROUP_NAME, SE_DATA_TYPE_STRING,
				    bgp->name, IPMP_GROUP_STATE,
				    SE_DATA_TYPE_UINT32, IPMP_GROUP_OK, NULL);
				if (error)
					log_syserr("posting of an IPMP"
					    " event failed");
				error = 0;

			}

			continue; /* Next group */

		} /* End of - if (match) */

		/* This is a new group - add it to obkg grouplist */
		if ((match = group_add(&obkg)) == NULL) {
			return;
		}

		/* Send a  ESC_IPMP_GROUP_CHANGE - grp add event */
		error = sc_publish_event(ESC_CLUSTER_IPMP_GROUP_CHANGE,
		    CL_EVENT_PUB_NET, CL_EVENT_SEV_INFO,
		    CL_EVENT_INIT_OPERATOR, 0, 0, 0, IPMP_GROUP_NAME,
		    SE_DATA_TYPE_STRING, bgp->name, IPMP_GROUP_OPERATION,
		    SE_DATA_TYPE_UINT32, IPMP_GROUP_ADD, NULL);
		if (error)
			log_syserr("posting of an IPMP event failed");
		error = 0;

		/* Update the group structure */
		match->status = bgp->status;
		match->name = strdup(bgp->name);

		/*
		 * Now add the adapters to this group. We do not
		 * check the state of the adps since these have just
		 * been added (the whole group has just been added)
		 * and even if they are down (i.e. group is down) we
		 * don't want to run the group down callbacks.
		 */
		for (adp = bgp->head; adp; adp = adp->next) {

			/* Add a new adapter to match */
			if (group_add_adp(match, adp) != 0)
				return;

			/* Send a ESC_IPMP_GROUP_MEMBER_CHANGE - adp add */
			error = sc_publish_event(
			    ESC_CLUSTER_IPMP_GROUP_MEMBER_CHANGE,
			    CL_EVENT_PUB_NET, CL_EVENT_SEV_INFO,
			    CL_EVENT_INIT_OPERATOR, 0, 0, 0, IPMP_GROUP_NAME,
			    SE_DATA_TYPE_STRING, bgp->name,
			    IPMP_GROUP_OPERATION, SE_DATA_TYPE_UINT32,
			    IPMP_IF_ADD, IPMP_IF_NAME, SE_DATA_TYPE_STRING,
			    adp->adp, NULL);
			if (error)
				log_syserr("posting of an IPMP event failed");
			error = 0;
		}

		/* Also run group_updated() after the adapters are added */
		group_updated(bgp);
	}

	/* Sync obkg from nbkg - delete extra structs from obkg */
	for (bgp = obkg; bgp; bgp = tmp) {
		tmp = bgp->next;
		match = group_find(bgp->name, nbkg);

		if (!match) { /* group is not present */

			/*
			 * Store the name of the grp temporarily
			 * which we are going to delete
			 */
			(void) strcpy(tmp_name, bgp->name);

			/*
			 * This should be a notice rather than an error message.
			 * However, since customer might not have syslog notices
			 * show up on the console we decided to make this an
			 * error message instead so that customers can see it.
			 * Also since the callbacks are associated only with
			 * the actual group list we do not need to remove them
			 * when we delete groups from the backup grouplist.
			 */
			DEBUGP((stdout, "%s has been deleted."
			    "\nIf %s was hosting any HA IP"
			    " addresses then these should be"
			    " restarted.\n", bgp->name, bgp->name));
			(void) sc_syslog_msg_log(msg_pnm, SC_SYSLOG_ERROR,
			    MESSAGE, "%s has been deleted."
			    "\nIf %s was hosting any HA IP"
			    " addresses then these should be"
			    " restarted.", bgp->name, bgp->name);
			group_delete(bgp, &obkg);

			/* Send a ESC_IPMP_GROUP_CHANGE - delete event */
			error = sc_publish_event(ESC_CLUSTER_IPMP_GROUP_CHANGE,
			    CL_EVENT_PUB_NET, CL_EVENT_SEV_INFO,
			    CL_EVENT_INIT_OPERATOR, 0, 0, 0, IPMP_GROUP_NAME,
			    SE_DATA_TYPE_STRING, tmp_name,
			    IPMP_GROUP_OPERATION, SE_DATA_TYPE_UINT32,
			    IPMP_GROUP_REMOVE, NULL);
			if (error)
				log_syserr("posting of an IPMP event failed");
			error = 0;

			continue; /* Next group */
		}

		for (adp = bgp->head; adp; adp = tmp_adp) {
			tmp_adp = adp->next;
			match_adp = group_find_adp(adp->adp, match);

			if (!match_adp) { /* adp not present */

				/* Store the name of the adapter */
				(void) strcpy(tmp_name, adp->adp);

				/*
				 * Print an error message if the adp was
				 * removed from the IPMP group without doing
				 * an if_mpadm (1M) first - which will move
				 * the IP addresses on the adp to any other
				 * adp in the IPMP group
				 */
				group_check_and_print_mesg(adp, bgp->name);
				group_delete_adp(bgp, adp);

				/*
				 * Send a ESC_IPMP_GROUP_MEMBER_CHANGE
				 * - delete event
				 */
				error = sc_publish_event(
				    ESC_CLUSTER_IPMP_GROUP_MEMBER_CHANGE,
				    CL_EVENT_PUB_NET, CL_EVENT_SEV_INFO,
				    CL_EVENT_INIT_OPERATOR, 0, 0, 0,
				    IPMP_GROUP_NAME, SE_DATA_TYPE_STRING,
				    bgp->name, IPMP_GROUP_OPERATION,
				    SE_DATA_TYPE_UINT32, IPMP_IF_REMOVE,
				    IPMP_IF_NAME, SE_DATA_TYPE_STRING,
				    tmp_name, NULL);
				if (error)
					log_syserr("posting of an IPMP event"
					    " failed");
				error = 0;

				/*
				 * If this is the last adapter of the group
				 * then delete the group.
				 */
				if (bgp->head == NULL) {
					(void) strcpy(tmp_name, bgp->name);
					group_delete(bgp, &obkg);

					/*
					 * Send a ESC_IPMP_GROUP_CHANGE
					 * delete event
					 */
					error = sc_publish_event(
					    ESC_CLUSTER_IPMP_GROUP_CHANGE,
					    CL_EVENT_PUB_NET,
					    CL_EVENT_SEV_INFO,
					    CL_EVENT_INIT_OPERATOR, 0, 0, 0,
					    IPMP_GROUP_NAME,
					    SE_DATA_TYPE_STRING, tmp_name,
					    IPMP_GROUP_OPERATION,
					    SE_DATA_TYPE_UINT32,
					    IPMP_GROUP_REMOVE, NULL);
					if (error)
						log_syserr("posting of an IPMP"
						    " event failed");
					error = 0;
				}

				continue; /* Next adp in this group */
			}

			if (!match_adp->pii_v4) {
				group_free_adp_inst(adp->pii_v4);
				adp->pii_v4 = NULL;
			}
			if (!match_adp->pii_v6) {
				group_free_adp_inst(adp->pii_v6);
				adp->pii_v6 = NULL;
			}

			/* Delete adp if both instances are NULL */
			if (!adp->pii_v4 && !adp->pii_v6) {
				group_check_and_print_mesg(adp, bgp->name);
				group_delete_adp(bgp, adp);
				if (bgp->head == NULL)
					group_delete(bgp, &obkg);
			}
		} /* Next adp in this group */
	} /* Next group */

	/* This is for debugging purposes */
	DEBUGP((stdout, "probe_check: dump nbkg and obkg\n"));
	for (bgp = nbkg; bgp; bgp = bgp->next) {
		DEBUGP((stdout, "probe_check: nbkg name = %s\n", bgp->name));
	}
	for (bgp = obkg; bgp; bgp = bgp->next) {
		DEBUGP((stdout, "probe_check: obkg name = %s\n", bgp->name));
	}

	DEBUGP((stdout, "probe_check: exit\n"));
	/* Here we would have the grouplists obkg and nbkg as equal */
}
