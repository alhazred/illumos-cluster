/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)pnmd_main.c	1.6	08/05/20 SMI"

/*
 * pnmd_main.c - Main for the pnm daemon.
 *
 * Initialize all the PNM groups data structure.
 * The main loop of the daemon blocks in a call to poll,
 * which only returns when there is a some work to do, either
 * some group changes or a client request to handle.
 */

#include <dlfcn.h>
#include <pnm/pnm_ld.h>
#include "pnmd_group.h"
#ifndef linux
#include <rgm/sczones.h>
#else
#include <sys/clconf.h>
#endif

#include <sys/sol_version.h>

#if SOL_VERSION >= __s10
#include <libzonecfg.h>
#endif /* SOL_VERSION >= __s10 */

static int		lockfd = -1; /* allow only one pnmd */
static uint_t		pollfd_num = 0; /* Num. of poll descriptors */
static struct pollfd	*pollfds = NULL; /* Array of poll descriptors */

int pnmd_exiting = 0; /* 1 if daemon is exiting */
static int traceflag = 0;
int debug = 0;

static void usage(void);
static int redirect_output(const char *tracefile);
static int pnmd_lock(void); /* lock the PNM daemon */
static void pnmd_sigterm(int sig_in); /* set flag for clean shutdown */
static void pnmd_cleanup(void); /* do a clean shutdown */
static void pnmd_init(void);

#define	LIBCLCONF	"/usr/cluster/lib/libclconf.so.1"
#define	FRGMD		"/usr/cluster/lib/sc/frgmd"

#define	CLCONF_LIB_INIT		"clconf_lib_init"
static int (*dl_clconf_lib_init)();
#define	CLCONF_IS_FARM_MGT_ENABLED	"clconf_is_farm_mgt_enabled"
static boolean_t (*dl_clconf_is_farm_mgt_enabled)();

#define	CLCONF_CLUSTER_GET_NODEID_FOR_PRIVATE_IPADDR	\
	"clconf_cluster_get_nodeid_for_private_ipaddr"
nodeid_t (*dl_clconf_cluster_get_nodeid_for_private_ipaddr)(in_addr_t);

#if SOL_VERSION >= __s10

#define	LIBZONECFG	"/usr/lib/libzonecfg.so.1"

#define	ZONECFG_INIT_HANDLE	"zonecfg_init_handle"
zone_dochandle_t (*dl_zonecfg_init_handle)();

#define	ZONECFG_FINI_HANDLE	"zonecfg_fini_handle"
void (*dl_zonecfg_fini_handle)(zone_dochandle_t);

#define	ZONECFG_GET_HANDLE	"zonecfg_get_handle"
int (*dl_zonecfg_get_handle)(const char *, zone_dochandle_t);

#define	ZONECFG_GET_IPTYPE	"zonecfg_get_iptype"
int (*dl_zonecfg_get_iptype)(zone_dochandle_t, zone_iptype_t *);

/*
 * Zone IP types were introduced in S10U4. If we are running a Solaris
 * 10 update version that is less than U4, we just assume that the zone IP type
 * is default, shared IP zones.
 */
int iptype_exists = 1;

#endif /* SOL_VERSION >= _s10 */

/* Europa configured on this node ? */
boolean_t pnmd_iseuropa = B_FALSE;

/*
 * Prints usage on stderr and then exits.
 * INPUT: void
 * RETURN: void (exits).
 */
void
usage()
{
	(void) fprintf(stderr, "usage: pnmd [-d [-t]] filename (opt.)\n");
	exit(PNM_EUSAGE);
}

/*
 * Associate files for debug and error messages. Optional
 * Tracefile should begin with '/'. If we want to trace the execution of the
 * daemon we can set the trace flag and give an optional filename in which all
 * the daemon execution trace messages will be recorded. If we do not give the
 * -t option then we will be able to see the trace messages on the console.
 * INPUT: pointer to tracefile.
 * RETURN: 0 on success or -1 on error.
 */
int
redirect_output(const char *tracefile)
{
	if (traceflag) {	/* traceflag is on */
		if (tracefile == NULL || strlen(tracefile) == 0)
			tracefile = DAEMONLOG;

		/* Associate stdout with tracefile - after flushing first */
		if (freopen(tracefile, "a", stdout) == NULL) {
			(void) fprintf(stderr,
			    "can't open %s for writing\n", DAEMONLOG);
			return (-1);
		}

		/* Associate stderr with tracefile - after flushing first */
		if (freopen(tracefile, "a", stderr) == NULL) {
			(void) fprintf(stderr,
			    "can't open %s for writing\n", DAEMONLOG);
			return (-1);
		}

		DEBUGP((stdout, "\nNEW RUN (pid = %d)\n", getpid()));
	} else {
		/*
		 * Note that, all the err_XXX() routines use syslog(3)
		 * to report error on the server.  The LOG_CONS
		 * option in openlog(3) ensures that the error
		 * messages will show up on the console.
		 */
		/* Discard non-error messages in non-trace mode */
		/* Associate /dev/null with stdout - after flushing first */
		if (freopen("/dev/null", "a", stdout) == NULL) {
			(void) fprintf(stderr,
			    "can't open /dev/null for writing\n");
			return (-1);
		}
		/* Associate /dev/null with stderr - after flushing first */
		if (freopen("/dev/null", "a", stderr) == NULL) {
			(void) fprintf(stderr,
			    "can't open /dev/null for writing\n");
			return (-1);
		}
	}
	return (0);
}

/*
 * Lock the PNM daemon. Return 0 for success and non-zero for failure. Called
 * by pnmd_init(). This means that only one instance of the daemon can run at
 * a time.
 * INPUT: void
 * RETURN: 0 on success or -1 on error
 */
int
pnmd_lock()
{
	char		msg[LINELEN];
	struct flock	fl;	/* file lock type */

	/* Open the lock file - create it if not present */
	lockfd = open(LOCKFILE, O_RDWR|O_CREAT, 0755);
	if (lockfd < 0) {
		log_syserr("can't open file %s.", LOCKFILE);
		return (-1);
	}

	/*
	 * Set exclusive write lock for this file descriptor - will fail if
	 * someone already has the lock.
	 */
	(void) memset((char *)&fl, 0, sizeof (fl));
	fl.l_type = F_WRLCK;
	if (fcntl(lockfd, F_SETLK, &fl) != 0) { /* CSTYLED */
		if (errno != EAGAIN) {	/*lint !e746 */
			log_syserr("fcntl error.");
		}
		(void) close(lockfd);
		lockfd = -1;
		return (-1);
	}

	/* Truncate the file to 0 length */
	(void) ftruncate(lockfd, (off_t)0);

	(void) sprintf(msg, "PNM daemon lock: %d\n", (int)getpid());

	/* Now write to the file */
	(void) write(lockfd, msg, strlen(msg));

	/*
	 * We have the lock till lockfd is open - we don't need to close it
	 * if we close lockfd we will lose the lock.
	 */
	return (0);
}

/*
 * Add fd to the set being polled.
 * INPUT: socket identifier to be added
 * RETURN: 0 on success or -1 on error.
 */
int
poll_add(int fd)
{
	uint_t i;
	uint_t new_num;
	struct pollfd *newfds;

poll_add_retry:
	/* Check if already present */
	for (i = 0; i < pollfd_num; i++) {
		if (pollfds[i].fd == fd)
			return (0);
	}
	/* Check for empty spot already present */
	for (i = 0; i < pollfd_num; i++) {
		if (pollfds[i].fd == -1) {
			pollfds[i].fd = fd;
			pollfds[i].events = POLLIN;
			return (0);
		}
	}

	/* Allocate space for 1 more fds and initialize to -1 */
	new_num = pollfd_num + 1;
	newfds = realloc(pollfds, new_num * sizeof (struct pollfd));
	if (newfds == NULL) {
		log_syserr("out of memory.");
		return (-1);
	}
	newfds[pollfd_num].fd = -1;
	pollfd_num = new_num;
	pollfds = newfds;
	DEBUGP((stdout, "poll_add: num_fd = %d\n", pollfd_num));
	goto poll_add_retry;
}

/*
 * Remove fd from the set being polled. We don't need to free it since we
 * set the identifier to -1. Idempotent.
 * INPUT: socket identifier
 * RETURN: void
 */
void
poll_remove(int fd)
{
	uint_t i;

	/* Check if present - remove it if present */
	for (i = 0; i < pollfd_num; i++) {
		if (pollfds[i].fd == fd) {
			pollfds[i].fd = -1;
			return;
		}
	}
}

/*
 * SIGTERM handler. Do "graceful" shutdown. To avoid deadlock, we simply
 * set pnmd_exiting here.
 * INPUT: signal
 * RETURN: void
 */
/*ARGSUSED*/
void
pnmd_sigterm(int sig)
{
	DEBUGP((stdout, "PNM daemon exiting.\n"));
	/*
	 * SCMSGS
	 * @explanation
	 * The PNM daemon (cl_pnmd) is shutting down.
	 * @user_action
	 * This message is informational; no user action is needed.
	 */
	(void) sc_syslog_msg_log(msg_pnm, SC_SYSLOG_NOTICE, MESSAGE,
	    "PNM daemon exiting.");
	pnmd_exiting = 1;
}

/*
 * We need to do a graceful shutdown of the daemon.
 * INPUT: void
 * RETURN: void (exits)
 */
void
pnmd_cleanup() {

	/*
	 * Should not leave the callback file hanging around, since subsequent
	 * pnmd will pick it up and install it. On a clean shutdown we will
	 * receive a sigterm so we should not leave the callback file here.
	 * On a unclean shutdown (kill -9) we will leave the callback file so
	 * that the callbacks can be registered when we start pnmd again
	 * (without having RGM reregister the callbacks).
	 */
	/* If the callback file is present then remove it */
	group_delete_file_callbacks();

	/* Close the probing communication channel */
	probe_close();

	/* Close all client connections as well as TCP listener */
	sv_close();

	free(pollfds);
}

/*
 * Exits on error. If pnmd is already running it exits. Exits if it can't
 * install a sigterm handler. Returns 0 on success.
 * INPUT: void
 * RETURN: 0 on success (exits on error).
 */
void
pnmd_init()
{
	struct bkg_status *group;
	struct stat filestat;
	void *dlhandle;
#if SOL_VERSION >= __s10
	void *dlzonehandle;
#endif /* SOL_VERSION >= __s10 */

	DEBUGP((stdout, "pnmd_init(): enter\n"));

	/*
	 * Initialize sc_syslog handles. These calls will invoke
	 * openlog(), and we shouldn't be doing openlog() ourselves.
	 */
	(void) sc_syslog_msg_initialize(&msg_pnm, SC_SYSLOG_PNM_TAG, "");

	/* Make sure no other pnmd is already running */
	if (pnmd_lock() != 0) {
		DEBUGP((stderr, "can't start pnmd due to lock.\n"));
		/*
		 * SCMSGS
		 * @explanation
		 * An attempt was made to start multiple instances of the PNM
		 * daemon cl_pnmd(1M), or cl_pnmd(1M) has problem acquiring a
		 * lock on the file (/var/cluster/run/pnm_lock).
		 * @user_action
		 * Check if another instance of cl_pnmd is already running. If
		 * not, remove the lock file (/var/cluster/run/pnm_lock) and
		 * start cl_pnmd by sending KILL (9) signal to cl_pnmd. PMF will
		 * restart cl_pnmd automatically.
		 */
		(void) sc_syslog_msg_log(msg_pnm, SC_SYSLOG_ERROR, MESSAGE,
		    "can't start cl_pnmd due to lock.");
		exit(PNM_EPROG);
	}

	/*
	 * Verify that all adapters/subnet have unique MAC addresses.
	 * If we can't verify this then print a message and continue
	 * since this means that either the MAC addresses are really not
	 * unique or that we were not able to get all the MAC addresses from
	 * the interface card-drivers.
	 */
	(void) nw_verify_mac_addr();

	/*
	 * This is to setup a signal handler for sigterm which will be called
	 * to do a clean shutdown of the daemon. We exit and let pmf restart
	 * the daemon again.
	 */
	if ((void(*)())sigset(SIGTERM, pnmd_sigterm) == SIG_ERR) {
		log_syserr("can't install SIGTERM handler.");
		exit(PNM_EPROG);
	}

	/*
	 * Take a snapshot of all public network groups.
	 */
	if (probe_snapshot() != 0)
		exit(PNM_EPROG);

	/* Verify that all adapters in a group have the same vlan id. */
	for (group = nbkg; group != NULL; group = group->next) {
		(void) group_verify_vlan_id(group);
	}

	/* Add the callbacks from the file if the file is present */
	group_add_file_callbacks();

	if (stat(FRGMD, &filestat) < 0) {
		/*
		 * Open the libclconf on the Sun Cluster nodes
		 * We can't use the dynamic linking of libclconf as
		 * libclconf is not available on the Europa farm nodes.
		 * If Europa is enabled, dlopen the Europa
		 * configuration
		 */
		/*
		 * lint has 'Suspicious cast' issues with dlsym function
		 * ptrs (e611)
		 */
		/* CSTYLED */
		/*lint -e611 */
		if ((dlhandle = dlopen(LIBCLCONF, RTLD_LAZY))
		    == NULL) {
			log_syserr("Cannot open libclconf: %s.", dlerror());
			exit(PNM_EPROG);
		}

		if ((dl_clconf_lib_init = (int(*)())
		    dlsym(dlhandle, CLCONF_LIB_INIT))
		    == NULL) {
			log_syserr("Cannot access libclconf function: %s.",
			    dlerror());
			exit(PNM_EPROG);
		}

		if ((dl_clconf_is_farm_mgt_enabled = (boolean_t(*)())
		    dlsym(dlhandle, CLCONF_IS_FARM_MGT_ENABLED))
		    == NULL) {
			log_syserr("Cannot access libclconf function: %s.",
			    dlerror());
			exit(PNM_EPROG);
		}

		if ((dl_clconf_cluster_get_nodeid_for_private_ipaddr =
		    (nodeid_t(*)()) dlsym(dlhandle,
		    CLCONF_CLUSTER_GET_NODEID_FOR_PRIVATE_IPADDR))
		    == NULL) {
			log_syserr("Cannot access libclconf function: %s.",
			    dlerror());
			exit(PNM_EPROG);
		}

#if SOL_VERSION >= __s10
		/*
		 * Open the libzonecfg library on the cluster nodes.
		 */
		if ((dlzonehandle = dlopen(LIBZONECFG, RTLD_LAZY))
		    == NULL) {
			log_syserr("Cannot open libzonecfg: %s.", dlerror());
			exit(PNM_EPROG);
		}

		if ((dl_zonecfg_init_handle = (zone_dochandle_t(*)())
		    dlsym(dlzonehandle, ZONECFG_INIT_HANDLE))
		    == NULL) {
			log_syserr("Cannot access libzonecfg function: %s.",
			    dlerror());
			exit(PNM_EPROG);
		}

		if ((dl_zonecfg_fini_handle = (void(*)(
		    zone_dochandle_t)) dlsym(dlzonehandle, ZONECFG_FINI_HANDLE))
		    == NULL) {
			log_syserr("Cannot access libzonecfg function: %s.",
			    dlerror());
			exit(PNM_EPROG);
		}
		if ((dl_zonecfg_get_handle = (int(*)(const char *,
		    zone_dochandle_t))dlsym(dlzonehandle, ZONECFG_GET_HANDLE))
		    == NULL) {
			log_syserr("Cannot access libzonecfg function: %s.",
			    dlerror());
			exit(PNM_EPROG);
		}

		if ((dl_zonecfg_get_iptype = (int(*)(zone_dochandle_t,
		    zone_iptype_t *)) dlsym(dlzonehandle,
		    ZONECFG_GET_IPTYPE)) == NULL) {
			/*
			 * This is not an error as the cluster may be running
			 * on S10U3.
			 */
			iptype_exists = 0;
		}
#endif /* SOL_VERSION >= __s10 */
		/* CSTYLED */
		/*lint +e611 */

		/* initializes ORB */
		if (dl_clconf_lib_init() != 0) {
			/*
			 * SCMSGS
			 * @explanation
			 * could not initialize ORB.
			 * @user_action
			 * Please make sure the nodes are booted in cluster
			 * mode.
			 */
			(void) sc_syslog_msg_log(msg_pnm, SC_SYSLOG_ERROR,
			    MESSAGE, "couldn't initialize ORB, possibly "
			    "because machine is booted in non-cluster mode");
			exit(PNM_EPROG);
		}

		/* Check if farm mgmt enabled */
		pnmd_iseuropa = dl_clconf_is_farm_mgt_enabled();
	} else {
		pnmd_iseuropa = B_TRUE;
	}
	DEBUGP((stdout, "pnmd_init(): exit\n"));
}

int
main(int argc, char *argv[])
{
	int	c;
	uint_t	i;
	int	pid;
	char	*tracefile = NULL;
	int	poll_ret = 0;
	uint_t	size;
	struct	rlimit rl;
	int	l;

	/* If not root return error and exit */
	if (getuid() != 0) {
		(void) fprintf(stderr, "Must be root to run pnmd\n");
		exit(PNM_EUSAGE);
	}

	/* Ignore all interrupts */
	(void) sigset(SIGINT, SIG_IGN);	/* ignore ctrl-C */
	(void) sigset(SIGQUIT, SIG_IGN); /* ignore quit signal */
	(void) sigset(SIGHUP, SIG_IGN); /* ignore hangup */
	(void) sigset(SIGPIPE, SIG_IGN); /* broken pipe - ignore */

	while ((c = getopt(argc, argv, "zdt")) != EOF) {
		switch (c) {
		case 'z':
			/* PNMD run in exclusive-ip zone */
			optind++;
			break;
		case 'd':
			/* Print debug messages */
			debug = 1;
			break;
		case 't':
			/* trace/debug messages to log file */
			traceflag = 1;
			/* Optional tracefile must start with '/' */
			if (argv[optind] && *argv[optind] == '/')
				tracefile = argv[optind++];
			break;
		default:
			usage();
		}
	}

	/* Check if we have trace on and debug off - not allowed - exit */
	if (optind != argc || (!debug && traceflag)) {
		usage();
	}

	/*
	 * If only debug flag is on, we want interactive debug output.
	 * Don't make pnmd a daemon.  However, if debug and traceflag
	 * are on simultaneously, we need all debug/trace output to go
	 * to syslog (after making ourselves a daemon).
	 */
	if (debug && !traceflag) {
		pnmd_init();
	} else {
		pid = fork();
		if (pid < 0) {
			perror("cannot fork\n");
			exit(PNM_EPROG);
		}

		/* The parent exits immediately */
		if (pid)
			exit(0);

		/* Child continues - get the max number of open files */
		rl.rlim_max = 0;
		(void) getrlimit(RLIMIT_NOFILE, &rl);
		if ((size = rl.rlim_max) == 0) {
			(void) fprintf(stderr, "cannot close open files from "
			    "parent\n maximum number of open files is 0\n");
			exit(PNM_EPROG);
		}

		/* Close all files in the child */
		for (i = 0; i < size; i++) {
			if (close((int)i) != 0 && errno == EBADF)
				break;
		}

		/* Duplicate and reserve stdout and stderr file descriptors */
		l = open("/dev/null", O_RDWR);
		if (l < 0) {
			(void) fprintf(stderr, "cannot open /dev/null\n");
			exit(PNM_EPROG);
		}
		(void) dup2(l, 1);
		(void) dup2(l, 2);
		(void) setsid();

		/*
		 * Attach files to capture stdout/stderr output from DEBUGP()
		 * and printf etc. when we are in trace/debug mode.
		 */
		if (redirect_output(tracefile) < 0)
			exit(PNM_EPROG);

		pnmd_init();
	}

	/*
	 * Loop over the probing notification handler (if exist) and the
	 * socket listener for any changes in the adapters underneath or
	 * any calls form the library libpnm.
	 */

	/*
	 * Setup the TCP listener. If we can't setup the TCP listening
	 * socket then its no use trying to continue. Exit on error
	 */
	if (sv_listener() < 0)
		exit(PNM_EPROG);

	/*
	 * Open the probing notification channel.
	 * This channel is optional. Some probing modules are not able to
	 * implement an event driven mechanism. In such cases, a polling
	 * mechanism will be used.
	 */
	if (probe_open() < 0)
		exit(PNM_EPROG);

	/*
	 * Main body. Keep listening for activity on any of the sockets
	 * that we are monitoring and take appropriate action as necessary.
	 */

	while (pnmd_exiting == 0) {
	/*
	 * This is a non blocking call to poll which will only return when
	 * there is some message available in one of the pollfds we are
	 * monitoring or after probing_timeout if there is no message
	 * available in any of the pollfds. A EINTR means that an interrupt
	 * occurred - so, since this is a slow system call we will restart
	 * the system call on an interrupt - after checking that the interrupt
	 * was not for terminating the daemon - i.e. pnmd_exiting == 0.
	 */
		DEBUGP((stdout, "main(): Monitoring Public Network groups \n"));
		DEBUGP((stdout, "main(): Waiting for PNM clients requests\n"));
		if ((poll_ret = poll(pollfds, pollfd_num,
		    probing_timeout)) < 0) { /* CSTYLED */
			if (errno == EINTR) /*lint !e746 */
				continue; /* call poll again */
			log_syserr("poll failed.");
			exit(PNM_EPROG);
		}
		DEBUGP((stdout, "main(): Some events occured\n"));

		/*
		 * If poll returned and none of the fds were activated then
		 * we should get the kernel adp state just to make sure that we
		 * did not lose any routing socket message and got out of
		 * whack.
		 */
		if (poll_ret == 0) {
			DEBUGP((stdout, "main(): Timeout %d expired\n",
				probing_timeout));
			probe_check();
		}

		/*
		 * Check whether it is a probing event, a connection request
		 * to server event or a client request to server event
		 */
		for (i = 0; i < pollfd_num && poll_ret > 0; i++) {
			if (pollfds[i].fd == -1 || !(pollfds[i].revents &
			    POLLIN))
				continue;
			if (pollfds[i].revents & (POLLERR|POLLHUP|POLLNVAL))
				continue;

			/*
			 * Check if a notification has been received
			 * on probing channel
			 */
			DEBUGP((stdout, "main(): Is it a probing event ?\n"));
			if (probe_process(pollfds[i].fd) == 0) {
				poll_ret--;
				continue; /* next i */
			}

			/*
			 * Check if a connection request to pnm server
			 * is pending
			 */
			DEBUGP((stdout, "main(): Is it a connect request ?\n"));
			if (sv_accept(pollfds[i].fd) == 0) {
				poll_ret--;
				continue; /* next i */
			}

			/*
			 * Check if a client request on a opened connection
			 * is pending
			 */
			DEBUGP((stdout, "main(): Is it a client request ?\n"));
			if (sv_process(pollfds[i].fd) == 0) {
				poll_ret--;
				continue; /* next i - outer loop */
			}
		}
	}

	DEBUGP((stdout, "main(): Stopping PNMD\n"));

	/* We received a sigterm and so we have to do a cleanup and return */
	pnmd_cleanup();
	exit(0);
}
