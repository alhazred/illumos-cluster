/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)pnmd_log.c	1.4	08/05/20 SMI"

/*
 * pnmd_log.c - Logging module.
 */

#include <pnm/pnm_head.h>

#define	PNM_SYSLOG_TAG		"PNM"

/* Message handles for the sc_syslog facility */
sc_syslog_msg_handle_t	msg_pnm;

/*
 * This is used for all the system errors. The error will be written to stderr
 * if debug is on and also will be written to the syslog as an error message.
 * errno will be intrepreted in the error message. This will generate a single
 * message id and so we have to write only one message explanation. However,
 * this is not such a big problem because a system error will occur for the
 * following types of errors:
 * ioctl error, out of memory, can't open a file descriptor etc.
 * These types of errors can be classified into one type and have the same type
 * of user action. The user action can be specified to be different also in the
 * explanation.
 */
void
log_syserr(char *fmt, ...)
{
	static char	buf[LINELEN];
	va_list		ap;		/* CSTYLED */
	int		err = errno;	 /*lint !e746 */

	/* CSTYLED */
	va_start(ap, fmt);	/*lint !e40 */
	(void) vsnprintf(buf, LINELEN, fmt, ap);
	va_end(ap);

	if (err != 0) {
		(void) strcat(buf, ": ");
		(void) strcat(buf, (char *)strerror(err));
	}

	/* This writes to stderr if debug is on */
	DEBUGP((stderr, "%s\n", buf));

	/* This uses the SC way of reporting syslog messages */
	/*
	 * SCMSGS
	 * @explanation
	 * A system error has occured in the PNM daemon. This could be because
	 * of the resources on the system being very low. eg: low memory.
	 * @user_action
	 * If the message is:
	 *
	 * out of memory - increase the swap space, install more memory or
	 * reduce peak memory consumption. Otherwise the error is
	 * unrecovarable, and the node needs to be rebooted.
	 *
	 * can't open file - check the "open" man page for possible error.
	 *
	 * fcntl error - check the "fcntl" man page for possible errors.
	 *
	 * poll failed - check the "poll" man page for possible errors.
	 *
	 * socket failed - check the "socket" man page for possible errors.
	 *
	 * SIOCGLIFNUM failed - check the "ioctl" man page for possible
	 * errors.
	 *
	 * SIOCGLIFCONF failed - check the "ioctl" man page for possible
	 * errors.
	 *
	 * wrong address family - check the "ioctl" man page for possible
	 * errors.
	 *
	 * SIOCGLIFFLAGS failed - check the "ioctl" man page for possible
	 * errors.
	 *
	 * SIOCGLIFADDR failed - check the "ioctl" man page for possible
	 * errors.
	 *
	 * rename failed - check the "rename" man page for possible errors.
	 *
	 * SIOCGLIFGROUPNAME failed - check the "ioctl" man page for possible
	 * errors.
	 *
	 * setsockopt (SO_REUSEADDR) failed - check the "setsockopt" man page
	 * for possible errors.
	 *
	 * bind failed - check the "bind" man page for possible errors.
	 *
	 * listen failed - check the "listen" man page for possible errors.
	 *
	 * read error - check the "read" man page for possible errors.
	 *
	 * SIOCSLIFGROUPNAME failed - check the "ioctl" man page for possible
	 * errors.
	 *
	 * SIOCSLIFFLAGS failed - check the "ioctl" man page for possible
	 * errors.
	 *
	 * SIOCGLIFNETMASK failed - check the "ioctl" man page for possible
	 * errors.
	 *
	 * SIOCGLIFSUBNET failed - check the "ioctl" man page for possible
	 * errors.
	 *
	 * write error - check the "write" man page for possible errors.
	 *
	 * accept failed - check the "accept" man page for possible errors.
	 *
	 * wrong peerlen %d - check the "accept" man page for possible errors.
	 *
	 * gethostbyname failed %s - make sure entries in /etc/hosts,
	 * /etc/nsswitch.conf and /etc/netconfig are correct to get
	 * information about this host.
	 *
	 * SIOCGIFARP failed - check the "ioctl" man page for possible errors.
	 * Check the arp cache to see if all the adapters in the node have
	 * their entries.
	 *
	 * can't install SIGTERM handler - check the man page for possible
	 * errors.
	 *
	 * posting of an IPMP event failed - the system is out of resources
	 * and hence sysevents cannot be posted.
	 */
	(void) sc_syslog_msg_log(msg_pnm, SC_SYSLOG_ERROR, MESSAGE,
	    "PNM daemon system error: %s", buf);
}

/*
 * This is used for all the user configuration errors. The error will be
 * written to stderr if debug is on and also will be written to the syslog as
 * an error message. This will generate a single
 * message id and so we have to write only one message explanation. However,
 * this is not such a big problem because a config error will occur for the
 * following types of errors:
 * wrong format for /etc/hostname.<adp> file, wrong callback format etc.
 * These types of errors can be classified into one type and have the same type
 * of user action. The user action can be specified to be different also in the
 * explanation.
 */
void
log_conferr(char *fmt, ...)
{
	static char	buf[LINELEN];
	va_list		ap;

	/* CSTYLED */
	va_start(ap, fmt);	/*lint !e40 */
	(void) vsnprintf(buf, LINELEN, fmt, ap);
	va_end(ap);

	/* This writes to stderr if debug is on */
	DEBUGP((stderr, "%s\n", buf));

	/* This uses the SC way of reporting syslog messages */
	/*
	 * SCMSGS
	 * @explanation
	 * A configuration error has occured in the PNM daemon. This could be
	 * because of wrong configuration/format etc.
	 * @user_action
	 * If the message is:
	 *
	 * IPMP group %s not found - either an IPMP group name has been
	 * changed or all the adapters in the IPMP group have been unplumbed.
	 * There would have been an earlier NOTICE which said that a
	 * particular IPMP group has been removed. The cl_pnmd has to be
	 * restarted. Send a KILL (9) signal to the PNM daemon. Because cl_pnmd
	 * is under PMF control, it will be restarted automatically. If the
	 * problem persists, restart the node with clnode evacuate and
	 * shutdown.
	 *
	 * IPMP group %s already exists - the user of libpnm is trying to
	 * auto-create an IPMP group with a groupname that is already being
	 * used. Typically, this should not happen so contact your authorized
	 * Sun service provider to determine whether a workaround or patch or
	 * suggestion is available. Make a note of all the IPMP group names on
	 * your cluster.
	 *
	 * wrong format for /etc/hostname.%s - the format of
	 * /etc/hostname.<adp> file is wrong. Either it has the keyword group
	 * but no group name following it or the file has multiple lines.
	 * Correct the format of the file after going through the IPMP Admin
	 * Guide. The cl_pnmd has to be restarted. Send a KILL (9) signal to
	 * cl_pnmd. Because cl_pnmd is under PMF control, it will be restarted
	 * automatically. If the problem persists, restart the node by using
	 * clnode evacuate and shutdown. We do not support multi-line
	 * /etc/hostname.adp file in auto-create since it becomes difficult to
	 * figure out which IP address the user wanted to use as the
	 * non-failover IP address. All adps in %s do not have similar
	 * instances (IPv4/IPv6)
	 *
	 * plumbed - An IPMP group is supposed to be homogenous. If any adp in
	 * an IPMP group has a certain instance (IPv4 or IPv6) then all the
	 * adps in that IPMP group must have that particular instance plumbed
	 * in order to facilitate failover. Please see the Solaris IPMP docs
	 * for more details.
	 *
	 * %s is blank - this means that the /etc/hostname.<adp> file is
	 * blank. We do not support auto-create for blank
	 *
	 * v4 hostname files - since this will mean that 0.0.0.0 will be
	 * plumbed on the v4 address.
	 */
	(void) sc_syslog_msg_log(msg_pnm, SC_SYSLOG_ERROR, MESSAGE,
	"PNM daemon config error: %s", buf);
}

/*
 * Note: openlog() shouldn't be called, since it'll interfere with the
 * sc_syslog facility, such as resetting the id string. User of these
 * functions should ensure sc_syslog_msg_initialize() be used to
 * initialize syslog. Here we use syslog instead of sc_syslog since sc_syslog
 * does not have a "DEBUG" option.
 */

/*
 * General syslog routine for debug messages. System call error reporting
 * should use log_syserr instead.
 */
void
log_debug(char *fmt, ...)
{
	static char	buf[LINELEN];
	va_list		ap;

	/* CSTYLED */
	va_start(ap, fmt);	/*lint !e40 */
	(void) vsnprintf(buf, LINELEN, fmt, ap);
	va_end(ap);

	/* This writes to stderr if debug is on */
	DEBUGP((stderr, "%s\n", buf));

	/* This will syslog a message if debug is on */
	if (debug) {
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		syslog(LOG_DEBUG, "%s[%d]: %s",
		    PNM_SYSLOG_TAG, getpid(), buf);
	}
}
