/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)pnmd_server.c	1.10	09/03/17 SMI"

/*
 * pnmd_server.c - PNM server module.
 *
 * This module aims to implement all the client requests available through
 * libpnm. The requests are received by the daemon through a socket connection
 * previously established by the client.
 */

#include <pnm/pnm_ld.h>
#include "pnmd_group.h"
#include <rgm/libdsdev.h>
#include <sys/sol_version.h>

#if SOL_VERSION >= __s10
#include <libzonecfg.h>
#endif /* SOL_VERSION >= __s10 */

#ifdef linux
#include <sys/clconf.h>
#endif

static int *csocks = NULL; /* Array of connected sockets */
static uint_t num_csock = 0; /* Num. of connected sockets */

static int sv_handle = -1;

static int csock_add(int fd);
static void csock_remove(int fd);

static void close_connection(int fd);

static void send_result(int fd, responses *rsp);

static void sv_process_cmd(int fd);
static void sv_group_list(int fd, commands *cmdi);
static void sv_group_status(int fd, commands *cmdi);
static void sv_group_create(int fd, commands *cmdi);
static void sv_callback_register(int fd, commands *cmdi);
static void sv_callback_unregister(int fd, commands *cmdi);
static void sv_callback_list(int fd, commands *cmdi);
static void sv_adapterinfo_list(int fd, commands *cmdi);
static void sv_map_adapter(int fd, commands *cmdi);
static void sv_mac_address(int fd, commands *cmdi);
static void sv_group_adp_status(int fd, commands *cmdi);
static void sv_ipaddress_check(int fd, commands *cmdi);
static void sv_get_instances(int fd, commands *cmdi);

#if SOL_VERSION >= __s10
static void sv_get_zone_iptype(int fd, commands *cmdi);

extern  zone_dochandle_t (*dl_zonecfg_init_handle)(void);
extern  void (*dl_zonecfg_fini_handle)(zone_dochandle_t);
extern int (*dl_zonecfg_get_handle)(const char *, zone_dochandle_t);
extern int (*dl_zonecfg_get_iptype)(zone_dochandle_t, zone_iptype_t *);

extern int iptype_exists;

#endif /* SOL_VERSION >= __s10 */

extern nodeid_t (*dl_clconf_cluster_get_nodeid_for_private_ipaddr)(in_addr_t);

/* Europa configured on this node ? */
extern boolean_t pnmd_iseuropa;


/*
 * Add fd to the set of connected sockets.
 * INPUT: socket identifier
 * RETURN: 0 on sucess or -1 on error.
 */
int
csock_add(int fd)
{
	uint_t i;
	uint_t new_num;
	int *newcsocks;

csock_add_retry:
	/* Check if already present */
	for (i = 0; i < num_csock; i++) {
		if (csocks[i] == fd)
			return (0);
	}
	/* Check for empty spot already present */
	for (i = 0; i < num_csock; i++) {
		if (csocks[i] == -1) {
			csocks[i] = fd;
			return (0);
		}
	}

	/* Allocate space for 1 more fd and initialize to -1 */
	new_num = num_csock + 1;
	newcsocks = realloc(csocks, new_num * sizeof (int));
	if (newcsocks == NULL) {
		log_syserr("out of memory.");
		return (-1);
	}
	newcsocks[num_csock] = -1;
	num_csock = new_num;
	csocks = newcsocks;
	DEBUGP((stdout, "csock_add: sucessful\n"));
	goto csock_add_retry;
}

/*
 * Remove fd from the set of connected sockets. We don't need to free it
 * since we just set the identifier to -1. Idempotent.
 * INPUT: socket identifier
 * RETURN: void
 */
void
csock_remove(int fd)
{
	uint_t i;

	/* Check if present - remove it if present */
	for (i = 0; i < num_csock; i++) {
		if (csocks[i] == fd) {
			csocks[i] = -1;
			return;
		}
	}
}

/*
 * Sets up the TCP listener. Has to be changed for v6. First uses the port from
 * /etc/services if available else uses the port from PNMD_PORT.
 * INPUT: void
 * RETURN: returns 0 on success or -1 on error.
 */
int
sv_listener()
{
	int sock;
	int on;
	size_t len = sizeof (struct sockaddr_in);
	int ret;
	struct sockaddr_in  sin;
	struct servent *sp = NULL;

	DEBUGP((stdout, "sv_listener: enter\n"));

	sock = socket(AF_INET, SOCK_STREAM, 0);
	if (sock < 0) {
		log_syserr("socket failed.");
		return (-1);
	}

	/* Set REUSEADDR */
	on = 1;
	if (setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, (char *)&on,
	    sizeof (on)) < 0) {
		log_syserr("setsockopt (SO_REUSEADDR) failed.");
		return (-1);
	}

	bzero(&sin, len);
	sin.sin_family = AF_INET;
	/* We are going to listen on all interfaces */
	sin.sin_addr.s_addr = htonl(INADDR_ANY);

	/* First get the port number from /etc/services if possible */
	if ((sp = getservbyname("pnmd", "tcp")) == NULL) {
		log_debug("sv_listener: no pnmd entry in /etc/services");
		sin.sin_port = htons(PNMD_PORT);
	}
	else
		sin.sin_port = (in_port_t)sp->s_port; /* in network order */

	ret = bind(sock, (struct sockaddr *)&sin, len);

	/*
	 * If bind fails then try to bind to the hard coded port number next
	 * - if it has not been tried already. Here sp != NULL means that we
	 * had an entry for pnmd in the file /etc/services
	 */
	/* CSTYLED */
	if (ret < 0 && errno == EADDRINUSE && sp) { /*lint !e746 */
		sin.sin_port = htons(PNMD_PORT);
		ret = bind(sock, (struct sockaddr *)&sin, len);
	}

	/* If bind fails again then error out */
	if (ret < 0) {
		log_syserr("bind failed.");
		return (-1);
	}

	if (listen(sock, 30) < 0) { /* 30 should be a big enough number */
		log_syserr("listen failed.");
		return (-1);
	}

	/* Add the socket to the pollfds */
	if (poll_add(sock) == -1) {
		(void) close(sock);
		return (-1);
	}

	sv_handle = sock;

	DEBUGP((stdout, "sv_listener: exit, sv_handle %d\n", sv_handle));

	return (0);
}

/*
 * This will accept a connection and then add the accepted socket to the poll
 * data structure and connected sockets data structure and poll on it.
 * INPUT: TCP listening socket
 * RETURN: 0 on success or -1 otherwise
 */
int
sv_accept(int sock)
{
	struct sockaddr_in peer_sin;
	uint_t peerlen;
	int newfd;
#ifndef PNM_STANDALONE
	scha_err_t err;
	scha_str_array_t *all_nodenames;
	scha_cluster_t handle;
	uint_t ix;
	struct hostent *hp;
	struct in_addr **pptr;
#endif
	char *version = PNMD_VERSION;
	char nop;
	int match = 0;
	char abuf[INET_ADDRSTRLEN];
	int flags;
	in_addr_t ipaddr;

	if (sock != sv_handle)
		return (-1);

	DEBUGP((stdout, "sv_accept: enter\n"));

	peerlen = sizeof (struct sockaddr_in);

	/* Check for ECONNABORTED */
accept_again:
	newfd = accept(sock, (struct sockaddr *)&peer_sin, &peerlen);
	if (newfd < 0) {
		if (errno == ECONNABORTED)
			goto accept_again;
		log_syserr("accept failed.");
		return (-1);
	}

	/*
	 * Validate the port to make sure that non priviledged processes
	 * don't connect and start talking to us. We also have to make sure
	 * that the client IP connecting to us is one of the cluster nodes
	 * for security reasons.
	 */
	if (peerlen != sizeof (struct sockaddr_in)) {
		log_syserr("wrong peerlen %d.", peerlen);
		(void) close(newfd);
		return (-1);
	}

	if (ntohs(peer_sin.sin_port) >= IPPORT_RESERVED) {
		(void) inet_ntop(AF_INET, &peer_sin.sin_addr.s_addr,
		    abuf, sizeof (abuf));
		DEBUGP((stdout, "Attempt to connect from addr %s port %d.",
		    abuf, ntohs(peer_sin.sin_port)));
		/*
		 * SCMSGS
		 * @explanation
		 * There was a connection from the named IP address and port
		 * number (> 1024) which means that a non-priviledged process
		 * is trying to talk to the PNM daemon.
		 * @user_action
		 * This message is informational; no user action is needed.
		 * However, it would be a good idea to see which
		 * non-priviledged process is trying to talk to the PNM daemon
		 * and why?
		 */
		(void) sc_syslog_msg_log(msg_pnm, SC_SYSLOG_NOTICE, MESSAGE,
		    "Attempt to connect from addr %s port %d.",
		    abuf, ntohs(peer_sin.sin_port));
		(void) close(newfd);
		return (-1);
	}
	DEBUGP((stdout, "sv_accept: port verified\n"));

#ifdef PNM_STANDALONE
	match = 1;
#else
	if (pnmd_iseuropa) {
retry_cluster_open:
		/*
		 * Verify that the src addr belongs to one of the cluster
		 * nodes' private link names.
		 */
		err = scha_cluster_open(&handle);
		if (err != SCHA_ERR_NOERR) {
			DEBUGP((stderr, "scha_cluster_open failed.\n"));
			/*
			 * SCMSGS
			 * @explanation
			 * Call to initialize a handle to get cluster
			 * information failed. This means that the incoming
			 * connection to the PNM daemon will not be accepted.
			 * @user_action
			 * There could be other related error messages which
			 * might be helpful. Contact your authorized Sun
			 * service provider to determine whether a workaround
			 * or patch is available.
			 */
			(void) sc_syslog_msg_log(msg_pnm, SC_SYSLOG_ERROR,
			MESSAGE, "scha_cluster_open failed.");
			(void) close(newfd);
			return (-1);
		}

		err = scha_cluster_get(handle, SCHA_ALL_PRIVATELINK_HOSTNAMES,
		    &all_nodenames);
		if (err == SCHA_ERR_SEQID) {
			(void) scha_cluster_close(handle);
			handle = NULL;
			(void) sleep(1);
			goto retry_cluster_open;
		} else
		if (err != SCHA_ERR_NOERR) {
			(void) scha_cluster_close(handle);
			(void) close(newfd);
			DEBUGP((stderr, "scha_cluster_get failed.\n"));
			/*
			 * SCMSGS
			 * @explanation
			 * Need explanation of this message!
			 * @user_action
			 * Need a user action for this message.
			 */
			(void) sc_syslog_msg_log(msg_pnm, SC_SYSLOG_ERROR,
			    MESSAGE, "scha_cluster_get failed retrieving "
			    "the property %s: %s",
			    SCHA_ALL_PRIVATELINK_HOSTNAMES,
			    scds_error_string(err));
			return (-1);
		}

		match = 0;
		for (ix = 0; ix < all_nodenames->array_cnt && !match; ix++) {
			if ((hp = gethostbyname(all_nodenames->str_array[ix]))
			    == NULL) {
				log_syserr("gethostbyname failed %s.",
				    all_nodenames->str_array[ix]);
				continue;
			}
			pptr = (struct in_addr **)hp->h_addr_list;
			for (; *pptr != NULL; pptr++) {
				/*
				 * **pptr is the 32bit address in
				 * network order and so it can be
				 * compared directly to in_addr.
				 */
				if ((**pptr).s_addr ==
				    peer_sin.sin_addr.s_addr) {
					match = 1;
					break;
				}
			}
		}
		(void) scha_cluster_close(handle);
	} else {
		/*
		 * Verify that the src addr belongs to one of the cluster
		 * nodes' private link names.
		 */
		ipaddr = inet_addr(inet_ntoa(peer_sin.sin_addr));
		if (dl_clconf_cluster_get_nodeid_for_private_ipaddr(ipaddr)
		    != NODEID_UNKNOWN) {
			match = 1;
		}
	}

	/*
	 * Check for local call if we did not found the address yet
	 */
	if (match != 1) {
		if ((hp = gethostbyname("localhost")) == NULL) {
			log_syserr("gethostbyname failed for localhost.");
		} else {
			pptr = (struct in_addr **)hp->h_addr_list;
			for (; *pptr != NULL; pptr++) {
				/*
				 * **pptr is the 32bit address in
				 * network order and so it can be
				 * compared directly to in_addr.
				 */
				if ((**pptr).s_addr ==
				    peer_sin.sin_addr.s_addr) {
					match = 1;
					break;
				}
			}
		}
	}

	DEBUGP((stdout, "accept_connection: source address verified\n"));

#endif /* PNM_STANDALONE */

	if (!match) {
		DEBUGP((stdout, "connection from outside the cluster"
		    " - rejected.\n"));
		/*
		 * SCMSGS
		 * @explanation
		 * There was a connection from an IP address which does not
		 * belong to the cluster. This is not allowed so the PNM
		 * daemon rejected the connection.
		 * @user_action
		 * This message is informational; no user action is needed.
		 * However, it would be a good idea to see who is trying to
		 * talk to the PNM daemon and why?
		 */
		(void) sc_syslog_msg_log(msg_pnm, SC_SYSLOG_NOTICE, MESSAGE,
		    "connection from outside the cluster - rejected.");
		(void) close(newfd);
		return (-1);
	}
	DEBUGP((stdout, "sv_accept: source address verified\n"));

	/* First read a nop byte and send the version number */
	if ((read(newfd, &nop, 1) <= 0) || nop != 0) {
		log_syserr("read error.");
		(void) close(newfd);
		return (-1);
	}

	/* Got the first nop so write the version number */
	if ((write(newfd, version, NAMELEN))
	    != NAMELEN) {
		log_syserr("write error.");
		(void) close(newfd);
		return (-1);
	}

	/* Make the socket non-blocking */
	if ((flags = fcntl(newfd, F_GETFL, 0)) < 0) {
		log_syserr("fcntl failed.");
		(void) close(newfd);
		return (-1);
	}
	if ((fcntl(newfd, F_SETFL, flags | O_NONBLOCK)) < 0) {
		log_syserr("fcntl failed.");
		(void) close(newfd);
		return (-1);
	}

	/* Add it to the pollfds structure */
	if (poll_add(newfd) == -1) {
		(void) close(newfd);
		return (-1);
	}

	/* Update the csock array with this socket */
	if (csock_add(newfd) == -1) {
		close_connection(newfd);
		return (-1);
	}

	DEBUGP((stdout, "sv_accept: exit, new fd %d\n", newfd));
	return (0);
}

/*
 * Commands received from libpnm come here. Currently the agents that send
 * commands are libpnm. Different commands will be sent by libpnm - and all
 * these commands will have their own data structures to pass back and forth.
 * INPUT: socket descriptor.
 * RETURN: void
 */
static void
sv_process_cmd(int fd)
{
	int n = 0;
	uint_t len = 0;
	commands cmdi;
	command_type_t cmd;
	char buff[MAX_CMD_SIZE];
	commands *tmp;
	int try = 0;	/* no. of times we try to read from the socket */

	DEBUGP((stdout, "sv_process_cmd: enter, fd = %d\n", fd));

	/* If we get a sigterm we should return immediately */
	if (pnmd_exiting)
		return;

	/*
	 * Here we should also check for 0 bytes on reset from the client.
	 * We must take care of the case in which there is too much to read
	 * one read is not sufficient to read all the data in the read buffer
	 * - in such a case read will return with less than the total data
	 * and so we should try to read again from that point on. However
	 * we should not try to read indefinitely since we do not want to
	 * block forever in trying to read data which is not available. So
	 * we try for 3 times and then error out saying that we were not able
	 * to read the data completely.
	 */

read_again:
	if ((n = read(fd, &buff[len], MAX_CMD_SIZE)) <= 0) {
		/*
		 * If we receive -1 it means that an error has occured on the
		 * connection so we should close the connection. If we receive
		 * a 0 it means that the client has sent a FIN and we should
		 * close the connection. It is always safe to check for
		 * EWOULDBLOCK since we would not want to error out in that
		 * case. We could also check for ECONNRESET.
		 */
		if (n == -1 && errno != EWOULDBLOCK)
			log_syserr("read error.");
		DEBUGP((stdout, "sv_process_cmd: n = %d\n", n));
		close_connection(fd);
		return;
	}
	len += (uint_t)n;
	tmp = (commands *) buff;
	tmp->cmd_len = ntohl(tmp->cmd_len);
	tmp->command = ntohl(tmp->command);

	DEBUGP((stdout, "sv_process_cmd: n = %d\n", n));
	if (len < tmp->cmd_len && try++ < 3) {
		(void) sleep(1);	/* sleep for one second */
		goto read_again;
	} else
	if (try == 3 && len < tmp->cmd_len) {
		DEBUGP((stderr, "network is very slow.\n"));
		/*
		 * SCMSGS
		 * @explanation
		 * This means that the PNM daemon was not able to read data
		 * from the network - either the network is very slow or the
		 * resources on the node are dangerously low.
		 * @user_action
		 * It is best to restart the PNM daemon. Send KILL (9) signal
		 * to cl_pnmd. PMF will restart it automatically. If the
		 * problem persists, restart the node with clnode evacuate and
		 * shutdown.
		 */
		(void) sc_syslog_msg_log(msg_pnm, SC_SYSLOG_ERROR, MESSAGE,
		    "network is very slow.");
		close_connection(fd);
		return;
	}
	bcopy(buff, &cmdi, tmp->cmd_len);
	cmd = cmdi.command;

	switch (cmd) {
	case GROUP_LIST:
		sv_group_list(fd, &cmdi);
		break;

	case GROUP_STATUS:
		sv_group_status(fd, &cmdi);
		break;

	case CALLBACK_REG:
		sv_callback_register(fd, &cmdi);
		break;

	case CALLBACK_UNREG:
		sv_callback_unregister(fd, &cmdi);
		break;

	case CALLBACK_LIST:
		sv_callback_list(fd, &cmdi);
		break;

	case ADAPTERINFO_LIST:
	case ADDRINFO_LIST:
		sv_adapterinfo_list(fd, &cmdi);
		break;

	case MAP_ADAPTER:
		sv_map_adapter(fd, &cmdi);
		break;

	case MAC_ADDRESS:
		sv_mac_address(fd, &cmdi);
		break;

	case GRP_ADP_STATUS:
		sv_group_adp_status(fd, &cmdi);
		break;

	case IP_CHECK:
		sv_ipaddress_check(fd, &cmdi);
		break;

	case GROUP_CREATE:
		sv_group_create(fd, &cmdi);
		break;

	case GET_INSTANCES:
		sv_get_instances(fd, &cmdi);
		break;

#if SOL_VERSION >= __s10
	case GET_ZONE_IPTYPE:
		sv_get_zone_iptype(fd, &cmdi);
		break;
#endif /* SOL_VERSION >= __s10 */
	/*
	 * We've added PNM_IFCONFIG only for Rolling Upgrade purposes. To
	 * support Rolling Upgrade we should not remove any of the existing
	 * PNM commands.
	 */
	case PNM_IFCONFIG:
	default:
		log_debug("sv_process_cmd: bad cmd %d", cmd);
	}

	DEBUGP((stdout, "sv_process_cmd: exit\n"));
	return;
	/* do not close the connection */
}

/*
 * Check if the communication handle given in argument correspond to one of
 * the current client open connections.
 * In such a case, process the pnm command requested by the client.
 * INPUT: a communication handle
 * RETURN: 0 on success or -1 otherwise
 */
int
sv_process(int handle)
{
	uint_t i;

	DEBUGP((stdout, "sv_process: enter, handle = %d\n", handle));

	for (i = 0; i < num_csock; i++) {
		if (handle == csocks[i]) {
			sv_process_cmd(csocks[i]);
			return (0);
		}
	}

	return (-1);
}

/*
 * Remove the socket from the csock array, from the pollfds and also close
 * the socket.
 * INPUT: socket identifier
 * RETURN: void
 */
void
close_connection(int fd)
{
	DEBUGP((stdout, "close_connection: enter\n"));

	/* We have to remove the socket from the csock array also */
	(void) close(fd);
	csock_remove(fd);
	poll_remove(fd);

	DEBUGP((stdout, "close_connection: exit\n"));
}

/*
 * Close all opened connections as well as TCP listener.
 * INPUT: void
 * RETURN: void
 */
void
sv_close()
{
	uint_t i;

	DEBUGP((stdout, "sv_close: enter\n"));

	for (i = 0; i < num_csock; i++)
		close_connection((int)i);

	free(csocks);

	(void) close(sv_handle);

	DEBUGP((stdout, "sv_close: exit\n"));
}

/*
 * This lists all the PNM groups on this node. It first checks for the
 * anticipated command size received and then gets the latest state of the
 * adapters and lists all the PNM groups presently configured on this node.
 * It sends the list of PNM groups to the TCP socket. On error it sends an
 * error to the TCP socket.
 */
static void
sv_group_list(int fd, commands *cmdi)
{
	responses rspi;
	int error = 0;
	bkg_status *bkg;
	char grp[NAMELEN];

	DEBUGP((stdout, "sv_group_list : enter\n"));

	if (cmdi->cmd_len != sizeof (uint32_t) + sizeof (size_t)) {
		DEBUGP((stderr, "sv_group_list: wrong command length "
			"received %d.\n", cmdi->cmd_len));
		/*
		 * SCMSGS
		 * @explanation
		 * This means that the PNM daemon received a command from
		 * libpnm, but all the bytes were not received.
		 * @user_action
		 * This is not a serious error. It could be happening due to
		 * some network problems. If the error persists send KILL (9)
		 * signal to cl_pnmd. PMF will restart cl_pnmd automatically.
		 * If the problem persists, restart the node with clnode
		 * evacuate and shutdown.
		 */
		(void) sc_syslog_msg_log(msg_pnm, SC_SYSLOG_ERROR, MESSAGE,
		    "wrong command length received %d.", cmdi->cmd_len);
		error = PNM_EPROG;
		goto gl_error;
	}

	/* Get current group information */
	if (probe_snapshot() != 0) {
		log_debug("sv_group_list: probe_snapshot error");
		error = PNM_EPROG;
		goto gl_error;
	}
	bzero(&rspi, sizeof (rspi));

	/*
	 * Create a data structure with all the group names separated
	 * with ":"
	 */
	for (bkg = nbkg; bkg; bkg = bkg->next) {
		(void) sprintf(grp, "%s", bkg->name);
		(void) strcat(rspi.u.rsp_glrsp.gl, grp);
		if (bkg->next)
			(void) strcat(rspi.u.rsp_glrsp.gl, ":");
	}
	rspi.u.rsp_glrsp.error = htonl(error);
	rspi.rsp_size = sizeof (rsp_group_list) + sizeof (size_t);
	send_result(fd, &rspi);

	DEBUGP((stdout, "sv_group_list: exit\n"));
	return;

gl_error:
	rspi.u.rsp_glrsp.error = htonl(error);
	rspi.rsp_size = sizeof (int) + sizeof (size_t);
	send_result(fd, &rspi);

	DEBUGP((stdout, "sv_group_list: error\n"));
}

/*
 * This gives the status of the given PNM group. It first checks for the
 * anticipated command size received and then gets the latest state of the
 * adapters and then gets the status of the given PNM group. The status also
 * includes a list of all the adapters in this PNM group.
 * It sends the status of the PNM group to the TCP socket. On error it sends
 * an error to the TCP socket.
 */
static void
sv_group_status(int fd, commands *cmdi)
{
	responses rspi;
	int error = 0;
	bkg_status *bkg;
	bkg_adp *adp;
	char listbuf[LINELEN];

	DEBUGP((stdout, "sv_group_status : enter\n"));

	if (cmdi->cmd_len != sizeof (struct cmd_group_status)
	    + sizeof (uint32_t) + sizeof (size_t)) {
		DEBUGP((stderr, "sv_group_status : wrong command length "
			"received %d.\n", cmdi->cmd_len));
		(void) sc_syslog_msg_log(msg_pnm, SC_SYSLOG_ERROR, MESSAGE,
		    "wrong command length received %d.", cmdi->cmd_len);
		error = PNM_EPROG;
		goto gs_error;
	}

	/* Get current group information */
	if (probe_snapshot() != 0) {
		log_debug("sv_group_status: probe_snapshot error");
		error = PNM_EPROG;
		goto gs_error;
	}
	bzero(&rspi, sizeof (rspi));

	/*
	 * Create the backups list, which is a list of all the adps in
	 * this group separated by ":"
	 */
	if ((bkg = group_find(cmdi->u.cmd_gscmd.gname, nbkg)) == NULL) {
		log_conferr("Public Network Group %s not found.",
		    cmdi->u.cmd_gscmd.gname);
		error = PNM_ENOBKG;
		goto gs_error;
	}

	DEBUGP((stdout, "sv_group_status: group = %s\n", bkg->name));

	for (adp = bkg->head; adp; adp = adp->next) {
		(void) sprintf(listbuf, "%s", adp->adp);
		(void) strcat(rspi.u.rsp_gsrsp.adplist, listbuf);
		if (adp->next)
			(void) strcat(rspi.u.rsp_gsrsp.adplist, ":");
	}

	/* Fill the structure with all the fields */
	rspi.u.rsp_gsrsp.status = htonl(group_status(bkg));
	rspi.u.rsp_gsrsp.error = htonl(error);
	rspi.rsp_size = sizeof (rsp_group_status) + sizeof (size_t);
	send_result(fd, &rspi);
	DEBUGP((stdout, "sv_group_status: exit\n"));
	return;

gs_error:
	rspi.u.rsp_gsrsp.error = htonl(error);
	rspi.rsp_size = sizeof (int) + sizeof (size_t);
	send_result(fd, &rspi);

	DEBUGP((stdout, "sv_group_status: error\n"));
}

/*
 * This auto-creates an public network group on this node.
 * It first checks for the anticipated command size received. It then
 * configures an public network group for the given adapter instance.
 * Do we want to check if the adapter instance already has a group configured
 * - we are not doing this because if the adapter instance already has a group
 * configured then this auto-create should not be called by scrgadm in the
 * first place. It chooses its own group name.
 * On error it sends an error to the TCP socket.
 *
 * NOTE: This functionality is not supported on Linux.
 *
 */
static void
sv_group_create(int fd, commands *cmdi)
{
	int i = 0;
	responses rspi;
	int error = 0;
	bkg_status *grp;
	char *adpa = NULL;
	char gname[NAMELEN];

	DEBUGP((stdout, "sv_group_create: enter, adapter = %s\n",
			cmdi->u.cmd_gccmd.adp));

	if (cmdi->cmd_len != sizeof (struct cmd_group_create)
	    + sizeof (uint32_t) + sizeof (size_t)) {
		DEBUGP((stderr, "sv_group_create : wrong command length "
			"received %d.\n", cmdi->cmd_len));
		(void) sc_syslog_msg_log(msg_pnm, SC_SYSLOG_ERROR, MESSAGE,
		    "wrong command length received %d.", cmdi->cmd_len);
		goto gc_error;
	}

	bzero(&rspi, sizeof (rspi));

	/*
	 * Make it a groupname of group  prefix returned by nw_group_name()
	 * +XX, where XX is the least unused number.
	 * The group numbering starts from 0.
	 */
	do
		(void) snprintf(gname, NAMELEN, "%s%d",
			nw_group_name(), i++);
	while (group_find(gname, nbkg));	/* end of do-while */

	/*
	 * Create the PNM group.
	 */
	adpa = cmdi->u.cmd_gccmd.adp;
	error = nw_group_create(gname, adpa);
	if (error == -1) {
		log_debug("sv_group_create: nw_group_create error");
		goto gc_error;
	}

	/*
	 * Check that the given adp is the only one in the group.
	 * This will insure that no one was creating an PNM group
	 * while we were trying to create an PNM group
	 */
	if (probe_snapshot() != 0) {
		log_debug("sv_group_create: probe_snapshot error");
		goto gc_error;
	}
	/*
	 * If the group does not exists or has multiple adps or the only
	 * adp is not the same as the one given to us then error out
	 */
	grp = group_find(gname, nbkg);
	if (!grp || (grp->head && grp->head->next) ||
		(grp->head && strcmp(grp->head->adp, adpa))) {
		goto gc_error;
	}

	rspi.u.rsp_gcrsp.error = htonl(0);
	rspi.rsp_size = sizeof (rsp_group_create) + sizeof (size_t);
	send_result(fd, &rspi);

	DEBUGP((stdout, "sv_group_create: exit\n"));
	return;

gc_error:
	rspi.u.rsp_gcrsp.error = htonl(PNM_EPROG);
	rspi.rsp_size = sizeof (int) + sizeof (size_t);
	send_result(fd, &rspi);

	DEBUGP((stdout, "sv_group_create: error\n"));
}

/*
 * This registers a callback with the given PNM group. It first checks for the
 * anticipated command size received and then gets the latest state of the
 * adapters. Then the given callback is registered with the given PNM group
 * and the callback file is updated accordingly. On error it sends an
 * error to the TCP socket.
 */
static void
sv_callback_register(int fd, commands *cmdi)
{
	responses rspi;
	bkg_status *bkg;
	int error = 0;
	int ind;

	DEBUGP((stdout, "sv_callback_register: enter, group name = %s "
			", callback = %s\n",
			cmdi->u.cmd_crcmd.gname, cmdi->u.cmd_crcmd.buf));

	if (cmdi->cmd_len != sizeof (struct cmd_callback_reg)
	    + sizeof (uint32_t) + sizeof (size_t)) {
		DEBUGP((stderr, "sv_callback_register : wrong command length "
			"received %d.\n", cmdi->cmd_len));
		(void) sc_syslog_msg_log(msg_pnm, SC_SYSLOG_ERROR, MESSAGE,
		    "wrong command length received %d.", cmdi->cmd_len);
		error = PNM_EPROG;
		goto cr_error;
	}

	/* Get current group information */
	if (probe_snapshot() != 0) {
		error = PNM_EPROG;
		log_debug("sv_callback_register: probe_snapshot error");
		goto cr_error;
	}
	/* Check if group present */
	if ((bkg = group_find(cmdi->u.cmd_crcmd.gname, nbkg)) == NULL) {
		error = PNM_ENOBKG;
		log_debug("sv_callback_register: group_find error");
		goto cr_error;
	}

	bzero(&rspi, sizeof (rspi));

	/* Add the callback */
	if ((ind = group_add_callback(bkg, cmdi->u.cmd_crcmd.buf)) < 0) {
		error = PNM_ECALLBACK;
		log_debug("sv_callback_register: group_add_callback error");
		goto cr_error;
	}

	/* Update the callback file */
	(void) group_conf_update(bkg, CONF_ADD, ind);

	rspi.u.rsp_crrsp.error = htonl(error);
	rspi.rsp_size = sizeof (rsp_callback_reg) + sizeof (size_t);
	send_result(fd, &rspi);

	DEBUGP((stdout, "sv_callback_register: exit, cind = %d\n", bkg->cind));
	return;

cr_error:
	rspi.u.rsp_crrsp.error = htonl(error);
	rspi.rsp_size = sizeof (int) + sizeof (size_t);
	send_result(fd, &rspi);

	DEBUGP((stdout, "sv_callback_register: error\n"));
}

/*
 * This unregisters a callback with the given PNM group. It first checks for
 * the anticipated command size received and then gets the latest state of the
 * adapters. Then the given callback is unregistered with the given PNM group
 * and the callback file is updated accordingly. On error it sends an
 * error to the TCP socket.
 */
static void
sv_callback_unregister(int fd, commands *cmdi)
{
	responses rspi;
	int error = 0;
	int ind;
	bkg_status *bkg;
	uint_t k;

	DEBUGP((stdout, "sv_callback_unregister: enter, group name = %s "
			", callback id = %s\n",
			cmdi->u.cmd_cucmd.gname, cmdi->u.cmd_cucmd.id));

	if (cmdi->cmd_len != sizeof (struct cmd_callback_unreg)
	    + sizeof (uint32_t) + sizeof (size_t)) {
		DEBUGP((stderr, "sv_callback_unregister : wrong command length "
			"received %d.\n", cmdi->cmd_len));
		(void) sc_syslog_msg_log(msg_pnm, SC_SYSLOG_ERROR, MESSAGE,
		    "wrong command length received %d.", cmdi->cmd_len);
		error = PNM_EPROG;
		goto cu_error;
	}

	/* Get current group information */
	if (probe_snapshot() != 0) {
		error = PNM_EPROG;
		log_debug("sv_callback_unregister: probe_snapshot error");
		goto cu_error;
	}

	/* Check if group present */
	if ((bkg = group_find(cmdi->u.cmd_cucmd.gname, nbkg)) == NULL) {
		error = PNM_ENOBKG;
		log_debug("sv_callback_unregister: group_find_error error");
		goto cu_error;
	}
	bzero(&rspi, sizeof (rspi));

	/*
	 * Check if all callbacks have already been removed - in this
	 * we return an error value of 0 which actually means success.
	 * Hence this is idempotent.
	 */
	if (!(bkg->cind > 0)) {
		error = 0;
		DEBUGP((stdout, "sv_callback_unregister: callback already "
			"unregistered\n"));
		goto cu_error;
	}

	if (cmdi->u.cmd_cucmd.id == NULL || *cmdi->u.cmd_cucmd.id == 0) {
		error = PNM_EUSAGE;
		log_debug("sv_callback_unregister: bad callback id");
		goto cu_error;
	}

	/*
	 * Compare the callback id and free the callback
	 * if the callback ids match. ind is the index of the
	 * callback freed. Remove the freed callback from the callback
	 * file also, i.e. update the callback file. If the callback
	 * file has already been updated then this call returns success
	 * so this call is idempotent.
	 */
	ind = -1;
	for (k = 0; k < bkg->cind; k++) {
		if (strcmp(bkg->callback[k].name, cmdi->u.cmd_cucmd.id) == 0) {
			/* Delete from callback file. */
			(void) group_conf_update(bkg, CONF_DELETE, (int)k);
			log_debug("sv_callback_unregistered: "
				"group %s: callback unregistered %s: %s",
			    bkg->name, bkg->callback[k].name,
			    bkg->callback[k].cmdline);
			free(bkg->callback[k].name);
			free(bkg->callback[k].cmdline);
			ind = (int)k;
			break;
		}
	}

	/*
	 * If a callback has been freed, a hole has been created in
	 * the callback array, so fill this hole by shifting all the
	 * callbacks after this hole one index earlier. Decrement
	 * the callback count. Free the memory also if required. As a
	 * result this callback is unregistered with the group.
	 */
	if (ind != -1) {
		for (k = (uint_t)ind + 1; k < bkg->cind; k++) {
			bkg->callback[k - 1] = bkg->callback[k];
		}
		bkg->cind--;
		if (bkg->cind == 0) {
			free(bkg->callback);
			bkg->callback = NULL;
		}
	}

	rspi.u.rsp_cursp.error = htonl(error);
	rspi.rsp_size = sizeof (rsp_callback_unreg) + sizeof (size_t);
	send_result(fd, &rspi);

	DEBUGP((stdout, "sv_callback_unregistered: exit\n"));
	return;

cu_error:
	rspi.u.rsp_cursp.error = htonl(error);
	rspi.rsp_size = sizeof (int) + sizeof (size_t);
	send_result(fd, &rspi);

	DEBUGP((stdout, "sv_callback_unregistered: error\n"));
}

/*
 * This lists all the callbacks registered with the given PNM group. It first
 * checks for the anticipated command size received and then gets the latest
 * state of the adapters. Then the callbacks registered with the given PNM
 * group are listed and sent to the TCP socket. On error it sends an error to
 * the TCP socket.
 */
static void
sv_callback_list(int fd, commands *cmdi)
{
	responses rspi;
	int error = 0;
	uint_t i;
	uint_t len, n = 0;
	char *buf = NULL;
	bkg_status *bkg;

	DEBUGP((stdout, "sv_callback_list: enter, group name = %s\n",
			cmdi->u.cmd_clcmd.gname));

	if (cmdi->cmd_len != sizeof (struct cmd_callback_list)
	    + sizeof (uint32_t) + sizeof (size_t)) {
		DEBUGP((stderr, "sv_callback_list : wrong command length "
			"received %d.\n", cmdi->cmd_len));
		(void) sc_syslog_msg_log(msg_pnm, SC_SYSLOG_ERROR, MESSAGE,
		    "wrong command length received %d.", cmdi->cmd_len);
		error = PNM_EPROG;
		goto cl_error;
	}

	/* Get current group information */
	if (probe_snapshot() != 0) {
		error = PNM_EPROG;
		log_debug("sv_callback_list: probe_snapshot error");
		goto cl_error;
	}
	/* Check if group present */
	if ((bkg = group_find(cmdi->u.cmd_clcmd.gname, nbkg)) == NULL) {
		error = PNM_ENOBKG;
		log_debug("sv_callback_list: group_find error");
		goto cl_error;
	}
	bzero(&rspi, sizeof (rspi));

	buf = (char *)rspi.u.rsp_clrsp.cb_buf;
	len = NUM_CBS * CB_LINELEN;

	(void) memset(buf, 0, len);
	for (i = 0; i < bkg->cind; i++) {
		(void) snprintf(&buf[n], len - n, "%s %s:",
		    bkg->callback[i].name, bkg->callback[i].cmdline);
		n += strlen(&buf[n]);
	}
	if (n > 0)
		buf[n - 1] = '\0';

	rspi.u.rsp_clrsp.error = htonl(error);

	/*
	 * Here we want to send exactly just the number of bytes that
	 * we have to - so we will form the rsp_size
	 * accordingly - n is the number of bytes in
	 * rspi.u.rsp_clrsu.cb_buf that we just filled. sizeof (int)
	 * is required for the error field.
	 */
	rspi.rsp_size = sizeof (size_t) + n + sizeof (int);
	send_result(fd, &rspi);

	DEBUGP((stdout, "sv_callback_list: exit\n"));
	return;

cl_error:
	rspi.u.rsp_clrsp.error = htonl(error);
	rspi.rsp_size = sizeof (int) + sizeof (size_t);
	send_result(fd, &rspi);

	DEBUGP((stdout, "sv_callback_list: error\n"));
}

/*
 * This sends a list of adapter instance structures that represents all
 * configured physical adapter instances on this node. It first checks for the
 * anticipated command size received. Then it sends the list of all the public
 * adapter instances on this node along with the PNM group, its subnets and
 * subnet masks, flags etc. On error it sends an error to the TCP socket.
 */
static void
sv_adapterinfo_list(int fd, commands *cmdi)
{
	responses *rspi = NULL;
	int num_adps = 0;
	char buff[ADPLIST_BUF];	/* this should be large enough */
	size_t offset = 0;
	boolean_t alladdrs = B_FALSE;

	DEBUGP((stdout, "sv_adapterinfo_list: enter\n"));

	bzero(buff, sizeof (buff));
	rspi = (responses *) buff;

	if (cmdi->cmd_len != sizeof (uint32_t) + sizeof (size_t)) {
		DEBUGP((stderr, "sv_adapterinfo_list: wrong command length "
			"received %d.\n", cmdi->cmd_len));
		(void) sc_syslog_msg_log(msg_pnm, SC_SYSLOG_ERROR, MESSAGE,
		    "wrong command length received %d.", cmdi->cmd_len);
		goto al_error;
	}
	if (cmdi->command == ADDRINFO_LIST)
		alladdrs = B_TRUE;

	/*
	 * Sends a list of nw_adapter_t structures that
	 * represents all configured physical adapters on this
	 * node. This information includes adapter flags, netmasks, subnet
	 * addresses, and what group the adapter belongs to.
	 * This is used currently by scrgadm(1M) when figuring out
	 * what adapters/pnm group to use for a given
	 * LogicalHostname or SharedAddress resource. It calls
	 * pnm_adapterinfo() for this info.
	 */
	num_adps = nw_adapterinfo_list((nw_adapter_t *)&rspi->u.rsp_alrsp.adpr,
			&offset, alladdrs);
	DEBUGP((stdout, "sv_adapterinfo_list: num_adps = %d\n", num_adps));
	if (num_adps == -1) {
		log_debug("sv_adapterinfo_list: nw_adapterinfo_list error");
		goto al_error;
	} else {
		rspi->u.rsp_alrsp.num_adps = (uint_t)htonl(num_adps);
		rspi->u.rsp_alrsp.error = htonl(0);

		/* We will send exactly the number of bytes we need to send */
		rspi->rsp_size = sizeof (int) + sizeof (uint_t) +
			sizeof (size_t) + offset;
	}
	send_result(fd, rspi);
	DEBUGP((stdout, "sv_adapterinfo_list: exit\n"));
	return;

al_error:
	rspi->u.rsp_alrsp.error = htonl(PNM_EPROG);
	rspi->rsp_size = sizeof (int) + sizeof (size_t);
	send_result(fd, rspi);
	DEBUGP((stdout, "sv_adapterinfo_list: error\n"));
}

/*
 * This maps an adapter to an PNM group. It first checks for the
 * anticipated command size received and then gets the latest state of the
 * adapters. Then the given adapter is mapped to its PNM group and this
 * group's name is sent to the TCP socket. On error it sends an
 * error to the TCP socket.
 */
static void
sv_map_adapter(int fd, commands *cmdi)
{
	responses rspi;
	int error = 0;
	bkg_status *bkg;
	bkg_adp *adp;
	int match = 0;

	DEBUGP((stdout, "sv_map_adapter: enter, adp = %s\n",
			cmdi->u.cmd_macmd.adp));

	if (cmdi->cmd_len != sizeof (struct cmd_map_adapter)
	    + sizeof (uint32_t) + sizeof (size_t)) {
		DEBUGP((stderr, "sv_map_adapter: wrong command length "
			"received %d.\n", cmdi->cmd_len));
		(void) sc_syslog_msg_log(msg_pnm, SC_SYSLOG_ERROR, MESSAGE,
		    "wrong command length received %d.", cmdi->cmd_len);
		error = PNM_EPROG;
		goto ma_error;
	}

	/* Get current group information */
	if (probe_snapshot() != 0) {
		error = PNM_EPROG;
		log_debug("sv_map_adapter: probe_snapshot");
		goto ma_error;
	}
	bzero(&rspi, sizeof (rspi));

	/*
	 * Find the group this adapter is in.
	 */
	for (bkg = nbkg; bkg && !match; bkg = bkg->next) {
		for (adp = bkg->head; adp; adp = adp->next) {
			if (strcmp(adp->adp, cmdi->u.cmd_macmd.adp) == 0) {
				(void) strcpy(rspi.u.rsp_marsp.gname,
				    bkg->name);
				match = 1;
				break;
			}
		}
	}
	if (!match) {
		error = PNM_ENOADP;
		log_debug("sv_map_adapter: probe_snapshot");
		goto ma_error;
	}
	rspi.u.rsp_marsp.error = htonl(error);
	rspi.rsp_size = sizeof (rsp_map_adapter) + sizeof (size_t);
	send_result(fd, &rspi);

	DEBUGP((stdout, "sv_map_adapter: exit\n"));
	return;

ma_error:
	rspi.u.rsp_marsp.error = htonl(error);
	rspi.rsp_size = sizeof (int) + sizeof (size_t);
	send_result(fd, &rspi);

	DEBUGP((stdout, "sv_map_adapter: error\n"));
}

/*
 * This gets the mac_addrs_uniq variable. It first checks for the
 * anticipated command size received.Then the variable is sent to the TCP
 * socket. On error it sends an error to the TCP socket.
 */
static void
sv_mac_address(int fd, commands *cmdi)
{
	responses rspi;
	int error = 0;

	DEBUGP((stdout, "sv_mac_address: enter\n"));

	if (cmdi->cmd_len != sizeof (uint32_t) + sizeof (size_t)) {
		DEBUGP((stderr, "sv_mac_address: wrong command length "
			"received %d.\n", cmdi->cmd_len));
		(void) sc_syslog_msg_log(msg_pnm, SC_SYSLOG_ERROR, MESSAGE,
		    "wrong command length received %d.", cmdi->cmd_len);
		error = PNM_EPROG;
		goto md_error;
	}

	bzero(&rspi, sizeof (rspi));

	/* Check if mac addresses are unique per subnet */
	(void) nw_verify_mac_addr();

	rspi.u.rsp_mdrsp.mac_addrs_uniq = htonl(nw_is_mac_addrs_uniq());
	rspi.u.rsp_mdrsp.error = htonl(error);
	rspi.rsp_size = sizeof (rsp_mac_address) + sizeof (size_t);
	send_result(fd, &rspi);

	DEBUGP((stdout, "sv_mac_address: exit\n"));
	return;

md_error:
	rspi.u.rsp_mdrsp.error = htonl(error);
	rspi.rsp_size = sizeof (int) + sizeof (size_t);
	send_result(fd, &rspi);

	DEBUGP((stdout, "sv_mac_address: error\n"));
}

/*
 * This gives the detailed status of the given PNM group. It first checks
 * for the anticipated command size received and then gets the latest state
 * of the adapters and then gets the status of the given PNM group.
 * The status includes a list of all the adapters in this PNM group along
 * with their status. It sends the status of the PNM group to the TCP socket.
 * On error it sends an error to the TCP socket.
 */
static void
sv_group_adp_status(int fd, commands *cmdi)
{
	responses rspi;
	int error = 0;
	bkg_status *bkg;
	bkg_adp *adp;
	uint32_t num_adps = 0;

	DEBUGP((stdout, "sv_group_adp_status: enter, group name = %s\n",
			cmdi->u.cmd_gdcmd.gname));

	if (cmdi->cmd_len != (sizeof (struct cmd_grp_adp_status)
	    + sizeof (uint32_t) + sizeof (size_t))) {
		DEBUGP((stderr, "sv_group_adp_status: wrong command length "
			"received %d.\n", cmdi->cmd_len));
		(void) sc_syslog_msg_log(msg_pnm, SC_SYSLOG_ERROR, MESSAGE,
		    "wrong command length received %d.", cmdi->cmd_len);
		error = PNM_EPROG;
		goto gd_error;
	}

	/* Get current group information */
	if (probe_snapshot() != 0) {
		error = PNM_EPROG;
		log_debug("sv_group_adp_status: probe_snapshot");
		goto gd_error;
	}
	bzero(&rspi, sizeof (rspi));

	/* See if the group is present */
	if ((bkg = group_find(cmdi->u.cmd_gdcmd.gname, nbkg)) == NULL) {
		error = PNM_ENOBKG;
		log_conferr("Group %s not found.", cmdi->u.cmd_gdcmd.gname);
		goto gd_error;
	}

	for (adp = bkg->head; adp; adp = adp->next) {
		(void) strcpy(rspi.u.rsp_gdrsp.adp_stat[num_adps].adp_name,
		    adp->adp);

		/* Since adp_notrun is the same as faulty for status */
		if (adp->state != ADP_NOTRUN)
			rspi.u.rsp_gdrsp.adp_stat[num_adps].status =
				htonl(adp->state);
		else
			rspi.u.rsp_gdrsp.adp_stat[num_adps].status =
				htonl(ADP_FAULTY);
		num_adps++;
	}

	/* Fill the structure with all the fields */
	rspi.u.rsp_gdrsp.num_adps = htonl(num_adps);
	rspi.u.rsp_gdrsp.error = htonl(error);

	/* We will send exactly the number of bytes we need to send */
	rspi.rsp_size = sizeof (int) + sizeof (uint_t)
	    + (num_adps * sizeof (adp_status))
	    + sizeof (size_t);
	send_result(fd, &rspi);

	DEBUGP((stdout, "sv_group_adp_status: exit\n"));
	return;

gd_error:
	rspi.u.rsp_gdrsp.error = htonl(error);
	rspi.rsp_size = sizeof (int) + sizeof (size_t);
	send_result(fd, &rspi);

	DEBUGP((stdout, "sv_group_adp_status: error\n"));
}

/*
 * This gives the list of PNM groups which do not have a failover (data)
 * address hosted/configured. It first checks for the anticipated command size
 * received and then gets the latest state of the adapters and then gets the
 * list of PNM groups which do not have a failover (data) address.
 * It sends this list of PNM groups to the TCP socket along with the number
 * of groups.
 * On error it sends an error to the TCP socket.
 */
static void
sv_ipaddress_check(int fd, commands *cmdi)
{
	responses rspi;
	int error = 0;
	bkg_status *bkg;
	uint32_t num_grps = 0;
	l_addr *alist = NULL;
	boolean_t data_ip;

	DEBUGP((stdout, "sv_ipaddress_check: enter\n"));

	if (cmdi->cmd_len != (sizeof (uint32_t) + sizeof (size_t))) {
		DEBUGP((stderr, "sv_ipaddress_check: wrong command length "
			"received %d.\n", cmdi->cmd_len));
		(void) sc_syslog_msg_log(msg_pnm, SC_SYSLOG_ERROR, MESSAGE,
		    "wrong command length received %d.", cmdi->cmd_len);
		error = PNM_EPROG;
		goto ic_error;
	}

	/* Get current group information */
	if (probe_snapshot() != 0) {
		error = PNM_EPROG;
		log_debug("sv_ipaddress_check: probe_snapshot");
		goto ic_error;
	}
	bzero(&rspi, sizeof (rspi));

	for (bkg = nbkg; bkg; bkg = bkg->next) {
		/*
		 * If its a singleton group then we do not need
		 * a data address.
		 */
		if (bkg->head && !bkg->head->next)
			continue;

		/* Get all the Logical Ip addresses hosted on this group */
		alist = group_logip_get(bkg, AF_INET);

		/*
		 * Check if the group contains a failover data address
		 * configured.
		 */
		data_ip = nw_group_failover_config(alist);
		if (!data_ip) {
			(void) strcpy(rspi.u.rsp_icrsp.grp[num_grps].grp_name,
			    bkg->name);
			DEBUGP((stdout, "sv_ipaddress_check: group = %s\n",
			    rspi.u.rsp_icrsp.grp[num_grps].grp_name));
			num_grps++;
		}
		nw_logip_free(alist);
	}

	/* Fill the structure with all the fields */
	DEBUGP((stdout, "sv_ipaddress_check: num_grps = %d\n", num_grps));
	rspi.u.rsp_icrsp.num_grps = htonl(num_grps);
	rspi.u.rsp_icrsp.error = htonl(error);

	/* We will send exactly the number of bytes we need to send */
	rspi.rsp_size = sizeof (int) + sizeof (uint_t)
	    + (num_grps * sizeof (grp_name))
	    + sizeof (size_t);

	DEBUGP((stdout, "sv_ipaddress_check: response size = %d\n",
			rspi.rsp_size));

	send_result(fd, &rspi);

	DEBUGP((stdout, "sv_ipaddress_check: exit\n"));
	return;

ic_error:
	rspi.u.rsp_icrsp.error = htonl(error);
	rspi.rsp_size = sizeof (int) + sizeof (size_t);
	send_result(fd, &rspi);

	DEBUGP((stdout, "sv_ipaddress_check: error\n"));
}

/*
 * This gives the instances of the given PNM group. It first checks for the
 * anticipated command size received and then gets the latest state of the
 * adapters and then gets the instances of the given PNM group. It sends the
 * instances of the PNM group to the TCP socket. On error it sends
 * an error to the TCP socket.
 */
static void
sv_get_instances(int fd, commands *cmdi)
{
	responses rspi;
	int error = 0, result = 0;
	bkg_status *bkg;

	DEBUGP((stdout, "sv_get_instances: enter, group name = %s\n",
		cmdi->u.cmd_gicmd.gname));

	if (cmdi->cmd_len != sizeof (struct cmd_get_instances)
	    + sizeof (uint32_t) + sizeof (size_t)) {
		DEBUGP((stderr, "sv_get_instances: wrong command length "
			"received %d.\n", cmdi->cmd_len));
		(void) sc_syslog_msg_log(msg_pnm, SC_SYSLOG_ERROR, MESSAGE,
		    "wrong command length received %d.", cmdi->cmd_len);
		error = PNM_EPROG;
		goto gi_error;
	}

	/* Get current group information */
	if (probe_snapshot() != 0) {
		error = PNM_EPROG;
		log_debug("sv_get_instances: probe_snapshot");
		goto gi_error;
	}
	bzero(&rspi, sizeof (rspi));

	if ((bkg = group_find(cmdi->u.cmd_gicmd.gname, nbkg)) == NULL) {
		log_conferr("Group %s not found.",
		    cmdi->u.cmd_gicmd.gname);
		error = PNM_ENOBKG;
		goto gi_error;
	}

	/*
	 * Get the instances from the adps in the group. Form the result
	 * based on all the adps in the group.
	 */
	error = group_instances(bkg, &result);

	/* Fill the structure with all the fields */
	rspi.u.rsp_girsp.result = htonl(result);
	rspi.u.rsp_girsp.error = htonl(error);
	rspi.rsp_size = sizeof (rsp_get_instances) + sizeof (size_t);
	send_result(fd, &rspi);

	DEBUGP((stdout, "sv_get_instances: exit\n"));
	return;

gi_error:
	rspi.u.rsp_girsp.error = htonl(error);
	rspi.rsp_size = sizeof (int) + sizeof (size_t);
	send_result(fd, &rspi);

	DEBUGP((stdout, "sv_get_instances: error\n"));
}

#if SOL_VERSION >= __s10
static void
sv_get_zone_iptype(int fd, commands *cmdi)
{
	responses	rspi;
	int		error = 0;
	zone_dochandle_t	handle;
	zone_iptype_t		iptype;

	DEBUGP((stdout, "sv_get_zone_iptype: enter, zonename = %s\n",
		cmdi->u.cmd_ztcmd.zonename));

	if (cmdi->cmd_len != sizeof (struct cmd_zone_iptype)
	    + sizeof (uint32_t) + sizeof (size_t)) {
		DEBUGP((stderr, "sv_get_zone_iptype: wrong command length "
			"received %d.\n", cmdi->cmd_len));
		(void) sc_syslog_msg_log(msg_pnm, SC_SYSLOG_ERROR, MESSAGE,
		    "wrong command length received %d.", cmdi->cmd_len);
		error = PNM_EPROG;
		goto gi_error;
	}

	bzero(&rspi, sizeof (rspi));

	if (iptype_exists == 0) {
		/*
		 * cluster is running a Solaris version that is less
		 * than S10U4.
		 */
		rspi.u.rsp_ziptype.type = htonl(SHARED_IP);
		rspi.u.rsp_ziptype.error = htonl(error);
		rspi.rsp_size = sizeof (rsp_zone_iptype) + sizeof (size_t);
		send_result(fd, &rspi);

		DEBUGP((stdout, "sv_get_zone_iptype: exit\n"));
		return;
	}
	/*
	 * Get the zone IP-type, given the zone name.
	 */
	if ((handle = (*dl_zonecfg_init_handle)()) == NULL) {
		DEBUGP((stderr, "sv_get_zone_iptype: Could not init handle\n"));
		/*
		 * SCMSGS
		 * @explanation
		 * During initialization, there was a failure to get
		 * information about configured zones in the system.
		 * @user_action
		 * There might be other related error messages which
		 * might be helpful. Contact your authorized Sun
		 * service provider to determine whether a workaround
		 * or patch is available.
		 */
		(void) sc_syslog_msg_log(msg_pnm, SC_SYSLOG_ERROR, MESSAGE,
		    "sv_get_zone_iptype: Could not init zone handle");
		error = PNM_EPROG;
	} else if ((error =
	    (*dl_zonecfg_get_handle)(cmdi->u.cmd_ztcmd.zonename, handle))
	    != Z_OK) {
		DEBUGP((stderr, "sv_get_zone_iptype: Could not get handle for"
		    "zone %s, error = %d\n", cmdi->u.cmd_ztcmd.zonename,
		    error));
		/*
		 * SCMSGS
		 * @explanation
		 * During initialization, there was a failure to get
		 * information about a specific zone in the system.
		 * @user_action
		 * This is an informational message. No user action is
		 * required.
		 */
		(void) sc_syslog_msg_log(msg_pnm, SC_SYSLOG_NOTICE, MESSAGE,
		    "sv_get_zone_iptype: Could not get zone handle for zone %s,"
		    "error = %d", cmdi->u.cmd_ztcmd.zonename, error);
		(*dl_zonecfg_fini_handle)(handle);
		error = PNM_EPROG;
	} else if ((error = (*dl_zonecfg_get_iptype)(handle, &iptype))
	    != Z_OK) {
		DEBUGP((stderr, "sv_get_zone_iptype: Could not get iptype for"
		    "zone %s, error = %d\n", cmdi->u.cmd_ztcmd.zonename,
		    error));
		/*
		 * SCMSGS
		 * @explanation
		 * There was a failure to get information about the IP type
		 * of a configured zone in the system.
		 * @user_action
		 * There might be other related error messages which
		 * might be helpful. Contact your authorized Sun
		 * service provider to determine whether a workaround
		 * or patch is available.
		 */
		(void) sc_syslog_msg_log(msg_pnm, SC_SYSLOG_ERROR, MESSAGE,
		    "sv_get_zone_iptype: Could not get iptype for zone %s,"
		    "error = %d", cmdi->u.cmd_ztcmd.zonename, error);
		(*dl_zonecfg_fini_handle)(handle);
		error = PNM_EPROG;
	} else {
		/* Fill the structure with all the fields */
		if (iptype == ZS_SHARED) {
			rspi.u.rsp_ziptype.type = htonl(SHARED_IP);
		} else {
			/* Exclusive-IP zone */
			rspi.u.rsp_ziptype.type = htonl(EXCLUSIVE_IP);
		}
		rspi.u.rsp_ziptype.error = htonl(error);
		rspi.rsp_size = sizeof (rsp_zone_iptype) + sizeof (size_t);
		send_result(fd, &rspi);
		(*dl_zonecfg_fini_handle)(handle);

		DEBUGP((stdout, "sv_get_zone_iptype: exit\n"));
		return;
	}
gi_error:
	rspi.u.rsp_ziptype.error = htonl(error);
	rspi.rsp_size = sizeof (int) + sizeof (size_t);
	send_result(fd, &rspi);

	DEBUGP((stdout, "sv_get_instances: error\n"));
}
#endif /* SOL_VERSION >= __s10 */

static void
send_result(int fd, responses *rsp)
{
	int	n;
	uint_t	len = 0;
	uint_t	rsp_size = 0;

	/*
	 * Write the response. If we are not able to write without getting
	 * error then we know that the client would block, so we should close.
	 * Here we try and write the data more than once if we are not
	 * able to write the data all at once - since this is a non-blocking
	 * socket.
	 */
	rsp_size = rsp->rsp_size;
	rsp->rsp_size = htonl(rsp->rsp_size);
	while ((n = write(fd, &rsp[len], rsp_size)) !=
	    (int)rsp_size) {
		if (n < 0) {
			log_syserr("write error.");
			close_connection(fd);
			break;
		}
		DEBUGP((stdout, "send_result: n = %d\n", n));
		len += (uint_t)n;
	}
	DEBUGP((stdout, "send_result: n = %d\n", n));
}
