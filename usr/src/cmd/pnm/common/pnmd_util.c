/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)pnmd_util.c	1.3	08/05/20 SMI"

/*
 * pnmd_util.c - Workhorse routines for the pnm daemon.
 */

#include <pnm/pnm_head.h>

#ifndef linux
#define	BINSH	"/sbin/sh"
#else
#define	BINSH	"/bin/sh"
#endif

static int run(const char *, int *);

/*
 * This compares the two strings given a character at a time till the given
 * length. It returns 0 if the two strings match else it returns -1.
 */
int
charcmp(uchar_t *ha1, uchar_t *ha2, size_t len)
{
	uchar_t *ptr1;
	uchar_t *ptr2;
	uint_t i;

	ptr1 = &ha1[0];
	ptr2 = &ha2[0];
	for (i = 0; i < len; i++) {
	if (*(ptr1 + i) != *(ptr2 + i))
		return (-1);
	}
	return (0);
}

/*
 * Returns the next input line from a file stream every time it is called. I
 * It looks for a non blank character and returns a line uptil the newline
 * character and returns a line uptil the newline character \n. Hence, all the
 * initial white spaces are removed.
 * INPUT: file pointer, char pointer to a line.
 * RETURN: -1 on error and 1 on success.(line has the newline from the file
 */
int
get_line(FILE *fp, char *line)
{
	int	i = 0;
	int	c;

	while ((c = getc(fp)) == ' ' || c == '\t' || c == '\n')
		;
	if (c == EOF)
		return (-1);
	line[i++] = (char)c;
	while ((c = getc(fp)) != EOF && c != '\n') {
		line[i++] = (char)c;
	}
	line[i] = '\0';
	if (c == EOF)
		(void) putc(c, fp);
	return (1);
}

/*
 * Return the logical instance of the adapter eg: hme0:2 - > 2.
 * INPUT: pointer to the adapter name
 * RETURN: the logical instance of the adapter.
 */
uint_t
get_logical_inst(char *name)
{
	int	i = 0;
	char	c;

	while ((c = name[i++]) != ':' && c != '\0')
		;
	if (c == '\0')
		return (0);

	return ((uint_t)atoi(&name[i]));
}

/*
 * Return the physical instance of the adapter eg: hme3:2 - > 3.
 * INPUT: pointer to the adapter name
 * RETURN: the physical instance of the adapter.
 */
uint_t
get_physical_inst(char *name)
{
	char	*p = name;
	if (p == NULL)
		return ((uint_t)-1);
	while (*p != (char)NULL)
		p++;
	while (p != name && (isdigit(*(p-1)) || *(p-1) == ':'))
		p--;
	if (!isdigit(*p))
		return ((uint_t)-1);
	return ((uint_t)atoi(p));
}

/*
 * Following code implements the execute routine which is used by the
 * PNM service daemon which uses the routine to run the callbacks.
 */

int
execute(char *cmd)
{
	int	run_stat = 0;

	if (cmd == NULL || *cmd == 0)
		return (-1);

	if (run(cmd, &run_stat) != 0) {
		DEBUGP((stderr, "can't exec: %s\n", cmd));
		return (-1);
	}
	return (WIFEXITED((uint_t)run_stat)?
	    WEXITSTATUS((uint_t)run_stat): -1);
}

int
run(const char *cmd, int *statp)
{
	pid_t		pid;
	int		status = 0;

#ifndef linux
	if ((pid = fork1()) < 0) {
#else
	if ((pid = fork()) < 0) {
#endif
		log_syserr("fork failed.");
		DEBUGP((stderr, "execute(): fork1() failed\n"));
		status = -1;
	} else if (pid == 0) {

		if (execl(BINSH, "sh", "-c", cmd, (char *)0) == -1) {
			log_syserr("exec failed.");
			exit(-1);
		}
	} else {
		while ((status = waitpid(pid, statp, 0)) != pid) { /* CSTYLED */
			if (status == -1 && errno == EINTR) /*lint !e746 */
				continue;
			else {
				status = -1;
				break;
			}
		}
		if (status == pid)
			status = 0;
	}

	return (status);
}
