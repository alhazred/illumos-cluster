/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)pnmd_probe_linux_bonding.c	1.3	08/05/20 SMI"

/*
 * pnmd_probe_linux_bonding.c - PNM probing module for Linux/bonding groups.
 *
 * This module aims to implement a monitoring of all the public network groups
 * currently defined in the cluster. This module provide a (optional) commu-
 * -nication channel between the kernel/networking entity and the pnm daemon
 * to receive notifications from the kernel when an change occurs on a public
 * network group of adapters. This channel when exists offers an event driven
 * mechanism (e.g. the solaris version uses the routing socket as communication
 * channel). If no such channel are available (e.g. Linux AS 3.0), the backup
 * solution is to use the active polling method.
 * When a notification is received (or when it's time to have a look to the
 * current kernel networking configuration), all the public network groups
 * information are loaded into PNM group data structures. Then a check procedure
 * is triggered to detect all the potential changes. A group updated or a group
 * failed callback information is sent to all the PNM clients previously regis-
 * -tered with the daemon.
 *
 * This module relies on a generic group management module (pnmd_group.c) but
 * depends on the OS specificities in term of networking interfaces.
 */

#include <pnm/pnm_ld.h>
#include "pnmd_group.h"
#include <op_types.h>
#include <linux/if_bonding.h>
#include <linux/mii.h>

/*
 * The interval between two public network groups monitoring.
 * The system default value is 60 seconds.
 * However, when using a probing module that is not event design oriented
 * the suggestion is to configure this value to 1-5 seconds, depending on
 * the level of availability expected.
 * The probing timeout is set to 1 sec
 */
int probing_timeout = 1000;

/*
 * The communication channel handle.
 * static int probe_handle = -1;
 */

static int probe_init_done = 0;

static int probe_snapshot_int(bkg_status **grp_list);

/*
 * Open the probing communication channel.
 *
 * For Linux kernels not able to generate netlink broadcast in case of a
 * media link state change notification, this function will be a no-op, the
 * implementation in this case is polling oriented. (see probing_timeout).
 *
 * For Linux kernels supporting the LINKWATCH feature,
 * subscribe to a NETLINK broadcast socket. By a process known as,
 * kernel netlink reflection, the userspace information is updated with
 * changes to internal data structures. LinkWatch provides hooks in
 * netif_carrier_on() and netif_carrier_off() to queue NIC link events.
 * Then a timer thread will run the queue to generate netlink broadcast events.
 * When NETLINK broaddcast events are received, the IFF_RUNNING flag reflects
 * media link status.
 *
 * INPUT: void
 * RETURN: 0 on success or -1 on error
 */
int
probe_open()
{
	DEBUGP((stdout, "probe_open: no operation (polling design)\n"));

	return (0);
}

/*
 * Close the probing communication channel.
 * INPUT: void
 * RETURN: void
 */
void
probe_close()
{
	DEBUGP((stdout, "probe_close: no operation (polling design)\n"));
}

/*
 * Check if the communication handle given in argument is the probing
 * handle. In such a case, read a check the message received on the channel
 * and call probe_check() if needed.
 * This function is a no-op on Linux
 * INPUT: a communication channel
 * RETURN: 0 on success or -1 otherwise
 */
int
probe_process(int handle)
{
	DEBUGP((stdout, "probe_process: handle = %d\n", handle));
	DEBUGP((stdout, "probe_process: no operation (polling design)\n"));
	return (-1);
}

/*
 * Takes a snapshot of the kernel adapters and form an bonding group list
 * with nbkg at the head. First sync nbkg grouplist from obkg grouplist.
 * At the end update the grp_list to point to the new grouplist head.
 * Remember that *grp_list = nbkg or obkg.
 * INPUT: void
 * RETURN: 0 on success or -1 on error.
 */
int
probe_snapshot()
{
	DEBUGP((stdout, "probe_snapshot: enter\n"));

	if (!probe_init_done) {
		/* Called from main: initialize both obkg and nbkg */
			if (probe_snapshot_int(&obkg) != 0)
				return (-1);
			probe_init_done = 1;
	}

	if (probe_snapshot_int(&nbkg) != 0)
		return (-1);

	DEBUGP((stdout, "probe_snapshot: exit\n"));

	return (0);
}

/*
 * Get bonding information form kernel and form a bonding interface
 * list. A pseudo algorithm is given below:
 *
 * 1. Synchronize grp_list with obkg.
 * 2. Read interface networking configuration from the kernel.
 * 3. For all bonding master interface
 *	- if new one, create a new bonding group, with name, number of slaves
 *	- if existing one, check if the bonding status has changed.
 *		(bond link up to down or down to up).
 *		in this case, mark change_status true.
 *		this is used by probe_check to trigger callback events.
 *	- Find the current active slave and check its state. This is necessary
 *	   to handle the following case: bond0(eth0, eth1), eth0 active from
 *	   the bonding driver but down from an IP point of view. In this case
 *	   the bonding driver is not able to detect anything.
 *	   If we find an active slave with IFF flags DOWN, then mark
 *	   change_active to true.
 *	- For all slaves
 *		- add adapter in bonding group
 *		- if change_active and slave is "link up"
 *			Change active slave to this one.
 *		- set up the adp state depending on slave link status.
 *
 * Note: the change active is only applicable for active-backup bonding confs.
 */
static int
probe_snapshot_int(bkg_status **grp_list)
{
	int			so_v4 = 0; /* socket descriptor */
	int			so_v6 = 0;
	int			so;
	uint_t			inst;	/* ordinal value of IP */
	char			*name;	/* name of adp */
	int			numreqs;
	struct ifconf		ifc;
	struct ifreq		*ifr;
	struct ifreq		ifrslave;
	int n;
	bkg_status		*match, *bgp, *tmp;
	sa_family_t		af; /* family, v4/v6 */
	int 			bonding;
	bkg_adp			*match_adp, *adp, *ptr, *tmp_adp;
	int 			same_group;
	ifbond			bond;
	ifslave			slave;
	int			change_active;
	struct mii_ioctl_data 	*mii;

	DEBUGP((stdout, "probe_snapshot_int: enter, group list = %p\n",
		grp_list));

	/* Remove stuff from *grp_list, considering obkg as the true copy */
	/* Sync *grp_list with obkg - delete extra structs, and callbacks */
	for (bgp = *grp_list; bgp; bgp = tmp) {
		tmp = bgp->next;
		match = group_find(bgp->name, obkg);
		if (!match) {
			/*
			 * Delete all the callbacks associated with this group
			 * also.
			 */
			log_debug("probe_snapshot(): deleting group\n");
			group_delete_callbacks(bgp);
			group_delete(bgp, grp_list);
			continue; /* Next group - bgp */
		}

		/* Group matches */
		for (adp = bgp->head; adp; adp = tmp_adp) {
			tmp_adp = adp->next;
			match_adp = group_find_adp(adp->adp, match);
			if (!match_adp) {
				group_delete_adp(bgp, adp);
				if (bgp->head == NULL)
					group_delete(bgp, grp_list);
				continue; /* Next adp */
			}
		}
	}

	ifc.ifc_buf = NULL;

	/*
	 * Here we should remember that we need two sockets for doing ioctls
	 * v4 and v6. First we do a SIOCGLIFCONF with AF_UNSPEC on a v4 socket
	 * and then for every instance (v4/v6) we must do the ioctls on the
	 * particular socket..either v4 or v6.
	 */
	/* Get kernel state into nbkg */
	if ((so_v4 = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
		log_syserr("socket failed.");
		goto probe_snapshot_error;
	}
	if ((so_v6 = socket(AF_INET6, SOCK_DGRAM, 0)) < 0) {
		if (errno != EAFNOSUPPORT) {
			log_syserr("socket failed.");
			goto probe_snapshot_error;
		}
	}

	/* Get the number of interfaces */
	ifc.ifc_len = 0;
	if (ioctl(so_v4, SIOCGIFCONF, &ifc) < 0) {
		log_syserr("SIOCGIFCONF failed.");
		goto probe_snapshot_error;
	}
	numreqs = ifc.ifc_len / sizeof (struct ifreq);
	numreqs++;	/* assume 1 extra interface */

	ifc.ifc_len = sizeof (struct ifreq) * numreqs;
	ifc.ifc_buf = (char *)malloc(ifc.ifc_len);
	if (!ifc.ifc_buf) {
		log_syserr("out of virtual memory.");
		goto probe_snapshot_error;
	}

	if (ioctl(so_v4, SIOCGIFCONF, &ifc) < 0) {
		log_syserr("SIOCGIFCONF failed.");
		goto probe_snapshot_error;
	}

	ifr = ifc.ifc_req;
	for (n = 0; n < ifc.ifc_len; n += sizeof (struct ifreq), ifr++) {

		af = ifr->ifr_addr.sa_family;
		so = (af == AF_INET) ? so_v4 : so_v6;
		name = ifr->ifr_name;
		DEBUGP((stdout, "probe_snapshot: adp = %s family = %d\n",
		    name, af));
		/*
		 * Returns 0 for physical adp, otherwise returns the
		 * ordinal value of the logical IP. eg. for
		 * tr2, it will return 0, whilst for tr2:3, it
		 * will return 3. get_logical_inst is defined in pnmd_util.c.
		 */
		inst = get_logical_inst(name);

		/* We only want to check the physical adapters */
		if (inst != 0)
			continue;

		/*
		 * Check to see if this interface is a bonding one (bondx).
		 */
		bonding = 1;
		ifr->ifr_data = (char *)&bond;
		if (ioctl(so, SIOCBONDINFOQUERY, ifr) < 0) {
			if (errno != EOPNOTSUPP) {
				log_syserr("SIOCBONDINFOQUERY on %s "
					" failed: %s\n",
					ifr->ifr_name, strerror(errno));
				goto probe_snapshot_error;
			} else {
				/* not a bonding interface */
				bonding = 0;
			}
		}

		/* Ignore interfaces other than bonding ones */
		if (!bonding)
			continue;

		/* bonding interface found */
		DEBUGP((stdout, "probe_snapshot: ifname = %s, "
			"num_slaves = %d\n",
			name, bond.num_slaves));

		/* Ignore bonding interface with no slaves configured */
		if (bond.num_slaves == 0)
			continue;

		/*
		 * Find group for this bonding interface
		 * If not exist, then create it.
		 */
		bgp = group_find(name, *grp_list);
		if (!bgp) {
			/* Create a new group */
			log_debug("probe_snapshot: new group (%s)", name);
			if ((bgp = group_add(grp_list)) == NULL)
				goto probe_snapshot_error;
			bgp->name = (char *)strdup(name);
			bgp->status = PNM_STAT_OK;
			bgp->change_status = 0;
		}

		bgp->num_adps = bond.num_slaves;

		/* Check MII word status for this bonding group */
		mii = (struct mii_ioctl_data *)&(ifr->ifr_data);
		mii->reg_num = MII_BMSR;
		if (ioctl(so, SIOCGMIIREG, ifr) < 0) {
			log_syserr("SIOCGMIIREG on %s failed: %s\n",
				ifr->ifr_name, strerror(errno));
				goto probe_snapshot_error;
		}

		/*
		 * Check if there is a bond status change to signal
		 * to probe_check ...
		 */
		switch (bgp->status) {
		case PNM_STAT_OK:
			if (!(mii->val_out & BMSR_LSTATUS)) {
				bgp->change_status = 1;
			}
			break;
		case PNM_STAT_DOWN:
			if (mii->val_out & BMSR_LSTATUS) {
				bgp->change_status = 1;
			}
			break;
		default:
			bgp->change_status = 0;
		}

		change_active = 0;
		ifr->ifr_data = (char *)&slave;
		for (slave.slave_id = 0; slave.slave_id < bond.num_slaves;
			slave.slave_id ++) {
			if (ioctl(so, SIOCBONDSLAVEINFOQUERY, ifr) < 0) {
				log_syserr("SIOCBONDSLAVEINFOQUERY, "
					" on %s failed: %s\n",
					ifr->ifr_name, strerror(errno));
				goto probe_snapshot_error;
			}
			DEBUGP((stdout, "probe_snapshot: ifname = %s, "
				"slave_name = %s, link = %d, state = %d\n",
				name, slave.slave_name,
				slave.link, slave.state));

			/* Get adapter flags */
			strcpy(ifrslave.ifr_name, slave.slave_name);
			if (ioctl(so, SIOCGIFFLAGS, &ifrslave) < 0) {
				log_syserr("SIOCGIFFLAGS, "
					" on %s failed: %s\n",
					ifrslave.ifr_name, strerror(errno));
				goto probe_snapshot_error;
			}

			DEBUGP((stdout, "probe_snapshot: slave name = %s "
				" ifr_flags = %x\n", slave.slave_name,
				ifrslave.ifr_flags));

			/*
			 * If the interface is down and currently the
			 * active interface for bonding driver, then try
			 * to find another active interface
			 */
			if (!(ifrslave.ifr_flags & IFF_UP) &&
				(slave.state == BOND_STATE_ACTIVE)) {

				change_active = 1;
				DEBUGP((stdout, "probe_snapshot: slave name = "
					"%s: change active requested\n",
					slave.slave_name));
				/*
				 * SCMSGS
				 * @explanation
				 * Need explanation of this message!
				 * @user_action
				 * Need a user action for this message.
				 */
				(void) sc_syslog_msg_log(msg_pnm,
					SC_SYSLOG_NOTICE, MESSAGE,
					"%s: change active requested %s.",
					ifr->ifr_name, slave.slave_name);
			}

		}

		for (slave.slave_id = 0; slave.slave_id < bond.num_slaves;
			slave.slave_id ++) {
			ifr->ifr_data = (char *)&slave;
			if (ioctl(so, SIOCBONDSLAVEINFOQUERY, ifr) < 0) {
				log_syserr("SIOCBONDSLAVEINFOQUERY, "
					" on %s failed: %s\n",
					ifr->ifr_name, strerror(errno));
				goto probe_snapshot_error;
			}
			DEBUGP((stdout, "probe_snapshot: ifname = %s, "
				"slave_name = %s, link = %d, state = %d\n",
				name, slave.slave_name,
				slave.link, slave.state));

			/*
			 * See if this adp is already there in the present
			 * *grp_list. match will be the group it is present in
			 * and match_adp will be the adapter itself.
			 */
			match_adp = NULL;
			for (match = *grp_list; match; match = match->next) {
				match_adp = group_find_adp(slave.slave_name,
						match);
				if (match_adp)
					break;
			}

			/*
			 * If adapter is already present then we need to see if
			 * it is in the same group. We are only concerned
			 * with adapters that are in a group.
			 */
			same_group = 0;
			if (match_adp) {
				if (strcmp(name, match->name) == 0)
					same_group = 1;
			}

			/*
			 * If the adapter is not found at all or not in the
			 * same group then allocate a new adp.
			 */
			ptr = NULL;
			if (!match_adp || !same_group) {
				/* Allocate adp structure */
				log_debug("probe_snapshot: new adp");
				if ((ptr = group_alloc_adp()) == NULL)
					goto probe_snapshot_error;
				ptr->adp = (char *)strdup(slave.slave_name);
			}

			/*
			 * If the adp is not in the same group now then we need
			 * to remove and free it from its previous group (match)
			 * If the group becomes empty in the process then
			 * remove the group from the grouplist.
			 */
			if (!same_group && match_adp) {
				group_delete_adp(match, match_adp);
				if (match->head == NULL)
					group_delete(match, grp_list);
			}

			/* Get adapter flags */
			strcpy(ifrslave.ifr_name, slave.slave_name);
			if (ioctl(so, SIOCGIFFLAGS, &ifrslave) < 0) {
				log_syserr("SIOCGIFFLAGS, "
					" on %s failed: %s\n",
					ifrslave.ifr_name, strerror(errno));
				goto probe_snapshot_error;
			}

			DEBUGP((stdout, "probe_snapshot: slave name = %s "
				" ifr_flags = %x\n", slave.slave_name,
				ifrslave.ifr_flags));

			/*
			 * If a request to change active slave has been
			 * expressed, then try with this slave.
			 */
			if ((bond.bond_mode == BOND_MODE_ACTIVEBACKUP) &&
			    (change_active) && (ifrslave.ifr_flags & IFF_UP)) {
				strcpy(ifr->ifr_slave, slave.slave_name);
				if (ioctl(so, SIOCBONDCHANGEACTIVE, ifr) < 0) {
					log_syserr("SIOCBONDCHANGEACTIVE, "
						" on %s failed: %s\n",
						ifr->ifr_name, strerror(errno));
					goto probe_snapshot_error;
				}
				change_active = 0;
				DEBUGP((stdout, "probe_snapshot: slave name = "
					"%s: change active done\n",
					slave.slave_name));
				/*
				 * SCMSGS
				 * @explanation
				 * Need explanation of this message!
				 * @user_action
				 * Need a user action for this message.
				 */
				(void) sc_syslog_msg_log(msg_pnm,
					SC_SYSLOG_NOTICE, MESSAGE,
					"%s: change active to %s.",
					ifr->ifr_name, slave.slave_name);
			}

			/*
			 * If the adapter has been allocated then we need to
			 * append it to the group, otherwise get new state for
			 * the old adp. (match_adp).
			 */
			if (ptr)
				group_append_adp(bgp, ptr);
			else
				ptr = match_adp;

			switch (slave.link) {
			case BOND_LINK_UP:
			case BOND_LINK_BACK:
				ptr->state = ADP_OK;
				break;
			case BOND_LINK_FAIL:
			case BOND_LINK_DOWN:
				/*
				 * Check if bond monitoring of slaves is done
				 * either by using MII interface or ARP.
				 * In case of MII monitoring, we mark it
				 * NOTRUN, it means request an immediate
				 * failover.
				 * In case of ARP monitoring, we mark it as
				 * FAULTY, means that a delay should be
				 * observed in the hope that NIC will repair.
				 */
				if (bond.miimon != 0)
					ptr->state = ADP_NOTRUN;
				else
					ptr->state = ADP_FAULTY;
				break;
			}

			DEBUGP((stdout, "probe_snapshot: slave name = %s "
				" pnm state = %d\n", slave.slave_name,
				ptr->state));

			/*
			 * Information of V4/V6 addresses hosted by bond
			 * stored in all adapters.
			 */
			if (af == AF_INET) {
				if (!ptr->pii_v4) {
					ptr->pii_v4 = (adp_inst *) malloc(
						sizeof (adp_inst));
					if (ptr->pii_v4 == NULL) {
						log_syserr("out of memory.");
						goto probe_snapshot_error;
					}
				}
				ptr->pii_v4->present = B_TRUE;
			} else { /* AF_INET6 */
				if (!ptr->pii_v6) {
					ptr->pii_v6 = (adp_inst *) malloc(
						sizeof (adp_inst));
					if (ptr->pii_v6 == NULL) {
						log_syserr("out of memory.");
						goto probe_snapshot_error;
					}
				}
				ptr->pii_v6->present = B_TRUE;
			}
		}
	}

	(void) close(so_v4);
	(void) close(so_v6);
	free(ifc.ifc_buf);

	DEBUGP((stdout, "probe_snapshot_int: exit\n"));

	return (0);

probe_snapshot_error:

	DEBUGP((stdout, "probe_snapshot_int: error\n"));

	/* Free the group list */
	for (; *grp_list; *grp_list = bgp) {
		bgp = (*grp_list)->next;
		group_free(*grp_list);
	}
	if (so_v4 > 0)
		(void) close(so_v4);
	if (so_v6 > 0)
		(void) close(so_v6);

	if (ifc.ifc_buf)
		free(ifc.ifc_buf);
	return (-1);
}

/*
 * Check to see if any of the bonding groups have failed or have been updated.
 * In either case run the appropriate callbacks. This supports both v4 and v6.
 * Check all the adapters to see if there are any bonding group down - or group
 * updated (an adapter has been added to an bonding group), if so run the
 * callbacks registered for the group. Also update the obkg grouplist
 * with the current information from the nbkg grouplist (got from the
 * kernel by ioctls - probe_snapshot()), considering the nbkg grouplist to have
 * the true grouplist state. This routine will post sysevents also.
 * INPUT: NULL.
 */
void
probe_check()
{
	struct bkg_status	*bgp, *match, *tmp;
	char			tmp_name[NAMELEN];
	int			error = 0;

	DEBUGP((stdout, "probe_check: entry\n"));

	/* Get an updated set of data structures - nbkg from the kernel */
	if ((probe_snapshot_int(&nbkg)) != 0)
		return;

	/* Now compare the new data structures with the old copy */
	for (bgp = nbkg; bgp; bgp = bgp->next) {
		match = group_find(bgp->name, obkg);

		if (match) { /* not a new group */

			/*
			 * If a change status has been detected, then
			 * perform actual transition and according actions.
			 */
			if (bgp->change_status) {
				switch (match->status) {
				case PNM_STAT_OK:
					group_down(bgp);
					match->status = PNM_STAT_DOWN;

					/*
					 * Send a ESC_IPMP_GROUP_STATE =
					 * down event.
					 */
					error = sc_publish_event(
						ESC_CLUSTER_IPMP_GROUP_STATE,
						CL_EVENT_PUB_NET,
						CL_EVENT_SEV_ERROR,
						CL_EVENT_INIT_SYSTEM, 0, 0, 0,
						IPMP_GROUP_NAME,
						SE_DATA_TYPE_STRING,
						bgp->name, IPMP_GROUP_STATE,
						SE_DATA_TYPE_UINT32,
						IPMP_GROUP_FAILED,
						NULL);
					if (error)
						log_syserr("posting of an IPMP"
							" event failed");
					break;
				case PNM_STAT_DOWN:
					group_up(bgp);
					match->status = PNM_STAT_OK;

					/*
					 * Send a ESC_IPMP_GROUP_STATE =
					 * up event.
					 */
					error = sc_publish_event(
						ESC_CLUSTER_IPMP_GROUP_STATE,
						CL_EVENT_PUB_NET,
						CL_EVENT_SEV_INFO,
						CL_EVENT_INIT_SYSTEM, 0, 0, 0,
						IPMP_GROUP_NAME,
						SE_DATA_TYPE_STRING,
						bgp->name, IPMP_GROUP_STATE,
						SE_DATA_TYPE_UINT32,
						IPMP_GROUP_OK, NULL);
					if (error)
						log_syserr("posting of an IPMP"
							" event failed");
					break;
				}
				bgp->change_status = 0;
			}

			/* Run group_updated if number of slaves increased. */
			if (bgp->num_adps != match->num_adps) {
				if (bgp->num_adps > match->num_adps)
					group_updated(bgp);
				match->num_adps = bgp->num_adps;

				/*
				 * Send a GROUP_MEMBER_CHANGE
				 * adp added event
				 */
				error = sc_publish_event(
					ESC_CLUSTER_IPMP_GROUP_MEMBER_CHANGE,
					CL_EVENT_PUB_NET, CL_EVENT_SEV_INFO,
					CL_EVENT_INIT_OPERATOR, 0, 0, 0,
					IPMP_GROUP_NAME,
					SE_DATA_TYPE_STRING, bgp->name,
					IPMP_GROUP_OPERATION,
					SE_DATA_TYPE_UINT32,
					IPMP_IF_ADD, IPMP_IF_NAME,
					SE_DATA_TYPE_STRING,
					adp->adp, NULL);
				if (error)
					log_syserr("posting of an IPMP"
						" event failed");
			}

			continue; /* Next group */

		} /* End of - if (match) */

		/* This is a new group - add it to obkg grouplist */
		if ((match = group_add(&obkg)) == NULL) {
			return;
		}

		/* Send a  ESC_IPMP_GROUP_CHANGE - grp add event */
		error = sc_publish_event(ESC_CLUSTER_IPMP_GROUP_CHANGE,
		    CL_EVENT_PUB_NET, CL_EVENT_SEV_INFO,
		    CL_EVENT_INIT_OPERATOR, 0, 0, 0, IPMP_GROUP_NAME,
		    SE_DATA_TYPE_STRING, bgp->name, IPMP_GROUP_OPERATION,
		    SE_DATA_TYPE_UINT32, IPMP_GROUP_ADD, NULL);
		if (error)
			log_syserr("posting of an IPMP event failed");

		/* Update the group structure */
		match->status = bgp->status;
		match->name = (char *)strdup(bgp->name);

		/* Also run group_updated() after the adapters are added */
		group_updated(bgp);
	}

	/* Sync obkg from nbkg - delete extra structs from obkg */
	for (bgp = obkg; bgp; bgp = tmp) {
		tmp = bgp->next;
		match = group_find(bgp->name, nbkg);

		if (!match) { /* group is not present */

			/*
			 * Store the name of the grp temporarily
			 * which we are going to delete
			 */
			(void) strcpy(tmp_name, bgp->name);

			/*
			 * Here we don't need to delete the callbacks since the
			 * callbacks will only be associated with the true copy
			 * i.e. nbkg rather than obkg. This should be a notice
			 * rather than an error message.
			 */
			DEBUGP((stdout, "%s has been deleted."
			    "\nIf %s was hosting any HA IP"
			    " addresses then these should be"
			    " restarted.", bgp->name, bgp->name));
			/*
			 * SCMSGS
			 * @explanation
			 * We do not allow deleting of an IPMP group which is
			 * hosting Logical IP addresses registered by RGM.
			 * Therefore we notify the user of the possible error.
			 * @user_action
			 * This message is informational; no user action is
			 * needed.
			 */
			(void) sc_syslog_msg_log(msg_pnm, SC_SYSLOG_ERROR,
			    MESSAGE, "%s has been deleted."
			    "\nIf %s was hosting any HA IP"
			    " addresses then these should be"
			    " restarted.", bgp->name, bgp->name);
			group_delete(bgp, &obkg);

			/* Send a ESC_IPMP_GROUP_CHANGE - delete event */
			error = sc_publish_event(ESC_CLUSTER_IPMP_GROUP_CHANGE,
			    CL_EVENT_PUB_NET, CL_EVENT_SEV_INFO,
			    CL_EVENT_INIT_OPERATOR, 0, 0, 0, IPMP_GROUP_NAME,
			    SE_DATA_TYPE_STRING, tmp_name,
			    IPMP_GROUP_OPERATION, SE_DATA_TYPE_UINT32,
			    IPMP_GROUP_REMOVE, NULL);
			if (error)
				log_syserr("posting of an IPMP event failed");

			continue; /* Next group */
		}

	} /* Next group */

	/* This is for debugging purposes */
	DEBUGP((stdout, "probe_check: dump nbkg and obkg\n"));
	for (bgp = nbkg; bgp; bgp = bgp->next) {
		DEBUGP((stdout, "probe_check: nbkg name = %s, status = %s, "
			"num_slaves = %d\n",
			bgp->name,
			(bgp->status == 0 ? "up" : "down"),
			bgp->num_adps));
	}
	for (bgp = obkg; bgp; bgp = bgp->next) {
		DEBUGP((stdout, "probe_check: obkg name = %s, status = %s, "
			"num_slaves = %d\n",
			bgp->name,
			(bgp->status == 0 ? "up" : "down"),
			bgp->num_adps));
	}

	DEBUGP((stdout, "probe_check: exit\n"));
}
