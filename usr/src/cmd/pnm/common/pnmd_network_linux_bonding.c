/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)pnmd_network_linux_bonding.c	1.5	08/05/20 SMI"

/*
 * pnmd_network_linux_bonding.c - PNM networking group module.
 *
 * Linux/bonding implementation
 *
 * This module aims to provide an abstraction layer between the PNM and the
 * kernel/networking/group component it is running on. This includes the hide
 * of all * SIOC ioctls that may be specfic to a given operating system. This
 * covers also all the logical addresses management functions needed by the
 * public network group management module. This gives also an abstraction layer
 * between the generic public network group and the actual network interface
 * group implementation that is dependent on the underlying operating system.
 *
 */

#include "pnmd_network.h"
#include <linux/if_bonding.h>
#include <byteswap.h>
#include <rgm/pnm.h>

#define	BONDING_GROUPNAME	"bond"

static l_nets * nw_lognet_alloc(char *name, int so, sa_family_t af, int *r);
static void nw_copy_subnets(l_nets *ptr, nw_adapter_t *adprp, boolean_t
    alladdrs);
static boolean_t nw_subnet_present(l_nets *ptr, uint_t k, nw_adapter_t *adprp);

static uint_t mac_addrs_uniq = 1; /* 0 = not unique; 1 = unique */

static l_netlist * nw_lognet_get(char *, sa_family_t, int *);
static void nw_lognet_free(l_nets *list); /* free the subnets */
static void nw_lognetlist_free(l_netlist *list); /* free the subnet list */

/*
 * Build a subnet from an address and a mask.
 *
 * void IN6_BUILD_SUBNET(struct in6_addr *addr,
 *                       struct in6_addr *mask,
 *                       struct in6_addr *net);
 */
#define	IN6_BUILD_SUBNET(addr, mask, net) \
	(net)->s6_addr32[0] = (addr)->s6_addr32[0] & (mask)->s6_addr32[0], \
	(net)->s6_addr32[1] = (addr)->s6_addr32[1] & (mask)->s6_addr32[1], \
	(net)->s6_addr32[2] = (addr)->s6_addr32[2] & (mask)->s6_addr32[2], \
	(net)->s6_addr32[3] = (addr)->s6_addr32[3] & (mask)->s6_addr32[3]

/* Allocate l_nets structure and fill it */
static l_nets *
nw_lognet_alloc(char *name, int so, sa_family_t af, int *r)
{
	struct ifreq		ifr;
	l_nets			*ptr;
	struct sockaddr_in6	*sin6;
	struct sockaddr_in	*sin;
	struct in6_addr		ipaddr6;
	struct in6_addr		ipmask6;

	*r = -1;	/* assume error */
	ptr = (l_nets *) malloc(sizeof (l_nets));
	if (ptr == NULL) {
		log_syserr("out of memory.");
		return (NULL);
	}
	bzero(ptr, sizeof (l_nets));

	/* Retrieve the address */
	(void) memset(&ifr, 0, sizeof (ifr));
	(void) strncpy(ifr.ifr_name, name, sizeof (ifr.ifr_name));
	if (ioctl(so, SIOCGIFADDR, (caddr_t)&ifr) < 0) {
		/*
		 * If the interface list has changed we return 1 so that
		 * we are called again with the new interface list
		 */
		if (errno == ENXIO) {
			*r = 1;
			free(ptr);
			return (NULL);
		}
		log_syserr("SIOCGIFADDR failed.\n");
		free(ptr);
		return (NULL);
	}
	if (af == AF_INET) {    /* v4 */
		sin = (struct sockaddr_in *)&ifr.ifr_addr;
		IN6_INADDR_TO_V4MAPPED(&sin->sin_addr, &(ipaddr6));
	} else {	/* v6 */
		sin6 = (struct sockaddr_in6 *)&ifr.ifr_addr;
		ipaddr6 = sin6->sin6_addr; /* structure copy */
	}

	bcopy(&ipaddr6, &(ptr->ipaddr), CL_IPV6_ADDR_LEN);

	/* Retrieve the netmask */
	(void) memset(&ifr, 0, sizeof (ifr));
	(void) strncpy(ifr.ifr_name, name, sizeof (ifr.ifr_name));
	if (ioctl(so, SIOCGIFNETMASK, (caddr_t)&ifr) < 0) {
		/*
		 * If the interface list has changed we return 1 so that
		 * we are called again with the new interface list
		 */
		if (errno == ENXIO) {
			*r = 1;
			free(ptr);
			return (NULL);
		}
		log_syserr("SIOCGIFNETMASK failed.\n");
		free(ptr);
		return (NULL);
	}
	if (af == AF_INET) {
		sin = (struct sockaddr_in *)&ifr.ifr_addr;
		IN6_INADDR_TO_V4MAPPED(&sin->sin_addr, &(ipmask6));
	} else {
		sin6 = (struct sockaddr_in6 *)&ifr.ifr_addr;
		ipmask6 = sin6->sin6_addr; /* structure copy */
	}
	bcopy(&ipmask6, &(ptr->ipmask), CL_IPV6_ADDR_LEN);

	IN6_BUILD_SUBNET(&ipaddr6, &ipmask6, &(ptr->ipnet));

	*r = 0; /* success */
	return (ptr);
}

/*
 * Get all IP addresses hosted on this adapter instance - both physical and
 * logical IP addresses. If the adapter instance is NULL then get all the
 * logical IP addresses (not loopback) on this node. Input is physical adp
 * name i.e. eth0. Returns a list of l_addrs. On error returns NULL. Get the IP
 * addresses for the given family.
 * INPUT: pointer to adp name or NULL, family(v4/v6)
 * RETURN: pointer to a list of l_addrs structures, NULL on error.
 */
l_addr *
nw_logip_get(char *adp, sa_family_t af)
{
	uint_t			n;
	uint_t			phys_name_len;
	int			so = 0;
	char			*name;
	int			numreqs;
	l_addr			*ptr, *last_ptr, *head_ptr;
	struct ifconf		ifc;
	struct ifreq		*ifr;
	struct sockaddr_in	*sin;
	struct ifreq		ifrflag;
	struct in6_addr		addr;
	struct sockaddr_in6	*sin6;

	DEBUGP((stdout, "nw_logip_get: enter, adp = %s\n", adp));

	ifc.ifc_buf = NULL;
	ptr = last_ptr = head_ptr = NULL;

	if ((so = socket(af, SOCK_DGRAM, 0)) < 0) {
		log_syserr("socket failed.");
		goto error_gli_exit;
	}

	/* Get the number of interfaces */
	ifc.ifc_len = 0;
	if (ioctl(so, SIOCGIFCONF, &ifc) < 0) {
		log_syserr("SIOCGIFCONF failed.");
		goto error_gli_exit;
	}
	numreqs = ifc.ifc_len / sizeof (struct ifreq);
	numreqs++;	/* assume 1 extra interface */

	ifc.ifc_len = sizeof (struct ifreq) * numreqs;
	ifc.ifc_buf = (char *)malloc(ifc.ifc_len);
	if (!ifc.ifc_buf) {
		log_syserr("out of virtual memory.");
		goto error_gli_exit;
	}

	if (ioctl(so, SIOCGIFCONF, &ifc) < 0) {
		log_syserr("SIOCGIFCONF failed.");
		goto error_gli_exit;
	}

	ifr = ifc.ifc_req;
	for (n = 0; n < ifc.ifc_len; n += sizeof (struct ifreq), ifr++) {
		name = ifr->ifr_name;
		if (af != ifr->ifr_addr.sa_family)
			continue;

		/*
		 * Get the length of the physical adapter.
		 */
		phys_name_len = strcspn(name, ":");
		assert(phys_name_len != 0);

		/*
		 * If adp is NULL then we will get all the logical IPs.
		 * If the name of the i/f is not the same as the name of
		 * adp i.e. eth0 = eth0. So we are only concerned with i/f
		 * which have the same name as adp.
		 */
		if ((adp) &&
			(phys_name_len != strlen(adp) ||
			strncmp(adp, name, phys_name_len) != 0))
			continue;

		/* Also if the name of the adp is lo:x then we do not care. */
		if (strncmp(name, "lo", phys_name_len) == 0)
			continue;

		DEBUGP((stdout, "nw_logip_get: adp = %s\n", name));

		ptr = (l_addr *) malloc(sizeof (l_addr));
		if (ptr == NULL) {
			log_syserr("out of memory.");
			goto error_gli_exit;
		}
		bzero(ptr, sizeof (l_addr));

		/* Append l_addr structure to list */
		if (head_ptr == NULL)
			head_ptr = ptr;
		if (last_ptr != NULL)
			last_ptr->next = ptr;
		last_ptr = ptr;

		if (af == AF_INET) {	/* v4 */
			sin = (struct sockaddr_in *)&ifr->ifr_addr;
			IN6_INADDR_TO_V4MAPPED(&sin->sin_addr, &(ptr->ipaddr));
		} else {	/* v6 */
			sin6 = (struct sockaddr_in6 *)&ifr->ifr_addr;
			addr = sin6->sin6_addr;	/* structure copy */
			bcopy(&addr, &(ptr->ipaddr), CL_IPV6_ADDR_LEN);
		}

		/*
		 * Retrieve the flags set on this IP address
		 * (physical or logical).
		 */
		(void) memset(&ifrflag, 0, sizeof (ifrflag));
		(void) strncpy(ifrflag.ifr_name, name,
		    sizeof (ifrflag.ifr_name));

		if (ioctl(so, SIOCGIFFLAGS, (char *)&ifrflag) < 0) {
			log_syserr("SIOCGIFFLAGS failed.");
			goto error_gli_exit;
		}

		/* Save the flags of the logical IP address */
		ptr->flags = ifrflag.ifr_flags;

		/* Store the name of the adp also */
		ptr->name = (char *)strdup(name);
		if (ptr->name == NULL) {
			log_syserr("out of memory.");
			goto error_gli_exit;
		}
	}

	(void) close(so);
	free(ifc.ifc_buf);

	DEBUGP((stdout, "nw_logip_get: exit\n"));

	return (head_ptr);

error_gli_exit:

	/* Error cleanup */
	DEBUGP((stdout, "nw_logip_get: error\n"));

	if (so > 0)
		(void) close(so);

	if (ifc.ifc_buf)
		free(ifc.ifc_buf);

	nw_logip_free(head_ptr);

	return (NULL);
}

/*
 * Get all IP subnets, netmasks, ipaddrs hosted on this adapter instance - both
 * physical and logical. Input is physical adp name i.e. eth0. Returns a list
 * of l_netlist. On error returns NULL. Get the IP subnets, netmasks
 * and IP addresses for the given family.
 * INPUT: pointer to adp name, family(v4/v6), socket id, ifconf buffer pointer.
 * RETURN: pointer to a l_netlist structure, NULL on error.
 */
l_netlist *
nw_lognet_get(char *adp, sa_family_t af, int *r)
{
	uint_t			n, phys_name_len = 0;
	int			so = 0;
	char			*name;
	int			numreqs;
	struct ifconf		ifc;
	struct ifreq		*ifr;
	l_nets			*ptr, *last_ptr, *head_ptr;
	l_netlist		*list;

	DEBUGP((stdout, "nw_lognet_get: enter, adp = %s\n", adp));

	*r = -1;	/* assume error */
	ifc.ifc_buf = NULL;
	ptr = last_ptr = head_ptr = NULL;

	list = (l_netlist *) malloc(sizeof (l_netlist));
	if (list == NULL)
		return (NULL);
	bzero(list, sizeof (l_netlist));

	if ((so = socket(af, SOCK_DGRAM, 0)) < 0) {
		log_syserr("socket failed.");
		goto error_gln_exit;
	}

	/* Get the number of interfaces */
	ifc.ifc_len = 0;
	if (ioctl(so, SIOCGIFCONF, &ifc) < 0) {
		log_syserr("SIOCGIFCONF failed.");
		goto error_gln_exit;
	}
	numreqs = ifc.ifc_len / sizeof (struct ifreq);
	numreqs++;	/* assume 1 extra interface */

	ifc.ifc_len = sizeof (struct ifreq) * numreqs;
	ifc.ifc_buf = (char *)malloc(ifc.ifc_len);
	if (!ifc.ifc_buf) {
		log_syserr("out of virtual memory.");
		goto error_gln_exit;
	}

	if (ioctl(so, SIOCGIFCONF, &ifc) < 0) {
		log_syserr("SIOCGIFCONF failed.");
		goto error_gln_exit;
	}

	ifr = ifc.ifc_req;
	for (n = 0; n < ifc.ifc_len; n += sizeof (struct ifreq), ifr++) {

		/* Continue if it is not the same family */
		if (af != ifr->ifr_addr.sa_family)
			continue;

		name = ifr->ifr_name;
		DEBUGP((stdout, "nw_lognet_get: name = %s\n", name));

		/*
		 * Get the length of the physical adapter
		 * eg for eth0 it will be 3
		 */
		phys_name_len = strcspn(name, ":");
		assert(phys_name_len != 0);

		/*
		 * If the name of the i/f is not the same as the name of
		 * adp i.e. eth0 = eth0, then continue. So we are only
		 * concerned with i/f which have the same name as adp.
		 */
		if ((phys_name_len != strlen(adp) ||
		    strncmp(adp, name, phys_name_len) != 0))
			continue;

		/* Allocate l_nets structure */
		ptr = nw_lognet_alloc(name, so, af, r);
		if (*r == -1 || *r == 1 || ptr == NULL) {
			nw_lognetlist_free(list);
			goto error_gln_exit;
		}

		list->num_nets++;

		/* Append l_nets structure to list */
		if (head_ptr == NULL)
			head_ptr = ptr;
		if (last_ptr != NULL)
			last_ptr->next = ptr;
		last_ptr = ptr;
		list->net_list = head_ptr;
	}

	(void) close(so);
	free(ifc.ifc_buf);

	DEBUGP((stdout, "nw_lognet_get: exit\n"));
	return (list);

error_gln_exit:

	/* Error cleanup */
	DEBUGP((stdout, "nw_lognet_get: error\n"));

	if (so > 0)
		(void) close(so);

	if (ifc.ifc_buf)
		free(ifc.ifc_buf);

	nw_lognetlist_free(list);

	return (NULL);

}

/*
 * Free the l_addr structures list.
 * INPUT: pointer to l_addr list
 * RETURN: void
 */
void
nw_logip_free(l_addr *list)
{
	l_addr	*lp = NULL, *lp_next = NULL;

	for (lp = list; lp; lp = lp_next) {
		lp_next = lp->next;
		free(lp->name);
		free(lp);
	}
}

/*
 * Free the l_netlist structure.
 * INPUT: pointer to l_netlist
 * RETURN: void
 */
void
nw_lognetlist_free(l_netlist *list)
{
	nw_lognet_free(list->net_list);
	list = NULL;
}

/*
 * Free the l_netlist structures list.
 * INPUT: pointer to l_nets list
 * RETURN: void
 */
void
nw_lognet_free(l_nets *list)
{
	l_nets	*lp = NULL, *lp_next = NULL;

	for (lp = list; lp; lp = lp_next) {
		lp_next = lp->next;
		free(lp);
	}
}

/*
 * Return 1 if MAC addresses are unique, 0 otherwise.
 */
uint_t
nw_is_mac_addrs_uniq()
{
	return (mac_addrs_uniq);
}

/*
 * Verify that the adapters on this node have different MAC addresses.
 * This is not relevant for linux/bonding implementation ...
 * INPUT: void
 * RETURN: 0 on success and -1 on error.
 */
int
nw_verify_mac_addr()
{
	return (0);
}

/*
 * nw_copy_subnets() - This call is used by nw_adapterinfo_list() to copy
 * the subnets, netmasks and ipaddrs on this adapter into the network adapter
 * structure. It first checks to see that the subnet has not already been
 * copied before copying it. This way we are able to send only the unique
 * subnet and subnet mask combination hosted on this adp.
 * INPUT: ptr to l_nets - list of subnets and subnet masks,
 *        ptr to nw_adapter_t - the response that we have to fill.
 *        alladdrs - if TRUE copy all IP addresses, else only copy uniq subnets
 * RETURN: void - copies subnet, subnet masks and IP addrs from l_nets to the
 *         response.
 */
static void
nw_copy_subnets(l_nets *ptr, nw_adapter_t *adprp, boolean_t alladdrs)
{
	uint_t	k = 0;

	for (; ptr; ptr = ptr->next) {
		if (!alladdrs && nw_subnet_present(ptr, k, adprp)) {
			adprp->num_addrs--;
			continue;
		}
		bcopy(&ptr->ipnet, ((char *)&adprp->adp_net +
		    (k++ * CL_IPV6_ADDR_LEN)), CL_IPV6_ADDR_LEN);
		bcopy(&ptr->ipmask, ((char *)&adprp->adp_net +
		    (k++ * CL_IPV6_ADDR_LEN)), CL_IPV6_ADDR_LEN);
		bcopy(&ptr->ipaddr, ((char *)&adprp->adp_net +
		    (k++ * CL_IPV6_ADDR_LEN)), CL_IPV6_ADDR_LEN);
	}
}

/*
 * nw_subnet_present() - see if the subnet is already present. This call is used
 * nw_copy_subnets() to see if the subnet is already present in the
 * response.
 * INPUT: ptr to l_nets - subnet and subnet mask,
 *        ptr to nw_adapter_t - the response.
 *        int k - the number of subnet + subnet masks already present in the
 *        response.
 * RETURN: true if the given subnet/mask is already present else false.
 */
static boolean_t
nw_subnet_present(l_nets *ptr, uint_t k, nw_adapter_t *adprp)
{
	uint_t	i = 0;

	for (i = 0; i < k; i += 2) {
		if (IN6_ARE_ADDR_EQUAL((in6_addr_t *)&ptr->ipnet,
		    (in6_addr_t *)((char *)&adprp->adp_net +
		    (i * CL_IPV6_ADDR_LEN))))
			return (B_TRUE);
	}

	return (B_FALSE);
}

/*
 * Return the group name prefix to be used
 * for group creation.
 */
char *
nw_group_name()
{
	return (BONDING_GROUPNAME);
}

/*
 * Return true if one of the adapter in the list has a failover (data)
 * address hosted/configured. False otherwise.
 */
boolean_t
nw_group_failover_config(l_addr *list)
{
	boolean_t data_ip = B_FALSE; /* Assume that data IP is not present */

	if (list != NULL)
		data_ip = B_TRUE;

	return (data_ip);
}

/*
 * This function returns a list of adapter instance structures that represents
 * all configured physical adapter instances on this node.
 * It is the responsibility of the caller to provide an output buffer that is
 * large enough to contain all the adapter structures.
 * INPUT: pointer to a pre-allocated buffer
 * OUTPUT: length of output buffer.
 * RETURN: number of adapters on success and -1 on error.
 */
int
nw_adapterinfo_list(nw_adapter_t *adp_list, size_t *len, boolean_t alladdrs)
{
	int i, j, n;
	int so_v4 = 0, so_v6 = 0, so;
	int numreqs;
	struct ifreq *ifr = NULL;
	struct ifconf ifc;
	ifbond bond;
	struct ifreq ifrbond;
	sa_family_t family;
	l_nets *ptr = NULL;
	l_netlist *alist = NULL;
	nw_adapter_t *adprp = NULL;
	size_t offset = 0;
	int ret;

	DEBUGP((stdout, "nw_adapterinfo_list: enter\n"));

	ifc.ifc_buf = NULL;

	/*
	 * Build a list of nw_adapter_t structures that
	 * represents all configured physical adapters on this
	 * node. This information includes adapter flags, netmasks, subnet
	 * addresses, and what group the adapter belongs to.
	 */

	/* Create a v4 socket for ioctl calls */
	if ((so_v4 = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
		log_syserr("socket failed.");
		goto al_error;
	}

	/* Create a v6 socket for ioctl calls */
	if ((so_v6 = socket(AF_INET6, SOCK_DGRAM, 0)) < 0) {
		if (errno != EAFNOSUPPORT) {
			log_syserr("socket failed.");
			goto al_error;
		}
	}

retry:

	/* Get the number of interfaces */
	ifc.ifc_len = 0;
	if (ioctl(so_v4, SIOCGIFCONF, &ifc) < 0) {
		log_syserr("SIOCGIFCONF failed.");
		goto al_error;
	}
	numreqs = ifc.ifc_len / sizeof (struct ifreq);
	numreqs++;	/* assume 1 extra interface */

	ifc.ifc_len = sizeof (struct ifreq) * numreqs;
	if (ifc.ifc_buf)
		free(ifc.ifc_buf);
	ifc.ifc_buf = (char *)malloc(ifc.ifc_len);
	if (!ifc.ifc_buf) {
		log_syserr("out of virtual memory.");
		goto al_error;
	}

	/* Get all NIC's information */
	if (ioctl(so_v4, SIOCGIFCONF, &ifc) < 0) {
		log_syserr("SIOCGIFCONF failed.");
		goto al_error;
	}

	ifr = ifc.ifc_req;
	for (i = 0, j = 0, n = 0; n < ifc.ifc_len;
			n += sizeof (struct ifreq), ifr++, i++) {

		/* Continue if not physical adapter (inst != 0) */
		if (get_logical_inst(ifr->ifr_name) != 0)
			continue;

		family = ifr->ifr_addr.sa_family;
		so = (family == AF_INET) ? so_v4 : so_v6;

		/* Get flags (UP, BROADCAST, etc) */
		if (ioctl(so_v4, SIOCGIFFLAGS, (char *)ifr) != 0) {
			/*
			 * If an interface has been removed after we got the
			 * list of interfaces then get the list again
			 */
			if (errno == ENXIO) {
				goto retry;
			}
			log_syserr("SIOCGIFFLAGS failed.");
			goto al_error;
		}

		/*
		 * If it is loopback or private adp continue (if !alladdrs)
		 * Don't know how to check IFF_PRIVATE !!!
		 */
		if (!alladdrs && (ifr->ifr_flags & IFF_LOOPBACK))
			continue;

		/*
		 * Check to see if this interface is a bonding slave.
		 * If it is the case, forget it for the moment ...
		 */
		if (ifr->ifr_flags & IFF_SLAVE)
			continue;

		/*
		 * Check to see if this interface is a bonding master
		 * with no slaves attached. In this case forget it ...
		 */
		if (ifr->ifr_flags & IFF_MASTER) {
			/* bonding master interface */
			strcpy(ifrbond.ifr_name, ifr->ifr_name);
			ifrbond.ifr_data = (char *)&bond;
			if (ioctl(so, SIOCBONDINFOQUERY, &ifrbond) < 0) {
				log_syserr("SIOCBONDINFOQUERY on %s "
					" failed: %s\n",
					ifrbond.ifr_name, strerror(errno));
				goto al_error;
			}

			if (bond.num_slaves == 0)
				continue;
		}

		/*
		 * We don't expect to have so many adapters and
		 * subnets so as to fill up ADPLIST_BUF bytes.
		 */
		if (offset > ADPLIST_BUF) {
			log_syserr("Too many adapters.");
			goto al_error;
		}

		adprp = (nw_adapter_t *)((char *)adp_list  +
				offset);
		bzero(adprp, sizeof (nw_adapter_t));

		if (family == AF_INET)
			adprp->adp_flags = bswap_64(IFF_IPV4);
		else /* AF_INET6 */
			adprp->adp_flags = bswap_64(IFF_IPV6);

		(void) strncpy(adprp->adp_name, ifr->ifr_name,
			sizeof (ifr->ifr_name));

		/*
		 * Check to see if this interface is a bonding master.
		 * We have to return all master bonding interfaces, because
		 * there are the visible interface with the user. The slaves
		 * interfaces are managed by the bonding driver and are not
		 * visible to the user.
		 */
		if (ifr->ifr_flags & IFF_MASTER) {
			/* bonding master interface */
			/* bond structure hase been previously filled */
			DEBUGP((stdout, "nw_adapterinfo_list: "
				"ifname = %s, numslaves = %d\n",
				ifrbond.ifr_name, bond.num_slaves));

			(void) strncpy(adprp->adp_group, ifr->ifr_name,
				sizeof (ifr->ifr_name));
		} else {
			/* It is an orphan i/f (not in bonding group) */
			strcpy(adprp->adp_group, "\0");
		}

		DEBUGP((stdout, "nw_adapterinfo_list: adp =  %s, family = %d\n",
			adprp->adp_name, family));

		/* Get the various subnets on this adapter instance */
		alist = nw_lognet_get(ifr->ifr_name, family, &ret);
		if (ret == -1) {
			goto al_error;
		}
		if (ret == 1) { /* retry because interface list changed */
			goto retry;
		}

		ptr = alist->net_list;
		adprp->num_addrs = alist->num_nets;
		nw_copy_subnets(ptr, adprp, alladdrs);
		nw_lognetlist_free(alist);
		j++; /* For the next adapter */
		offset += 2 * (sizeof (uint64_t) + NAMELEN) +
		    3 * ((uint_t)adprp->num_addrs * CL_IPV6_ADDR_LEN);
		adprp->num_addrs = bswap_64(adprp->num_addrs);
	}

	free(ifc.ifc_buf);
	(void) close(so_v4);
	if (so_v6 > 0)
		(void) close(so_v6);

	DEBUGP((stdout, "nw_adapterinfo_list: num adps = %d\n", j));
	*len = offset;

	DEBUGP((stdout, "nw_adapterinfo_list: exit\n"));
	return (j);

al_error:
	DEBUGP((stdout, "nw_adapterinfo_list: error\n"));

	if (so_v4 > 0)
		(void) close(so_v4);
	if (so_v6 > 0)
		(void) close(so_v6);
	if (ifc.ifc_buf)
		free(ifc.ifc_buf);
	return (-1);
}

/*
 * This auto-creates an bonding group on this node by configuring an bond
 * interface for the given adapter instance.
 * This functionality will not be supported on Linux.
 * INPUT: group name and adapter name.
 * RETURN: 0 on success and -1 on error.
 */
int
nw_group_create(char *gname, char *adp)
{
	DEBUGP((stdout, "nw_group_create: enter, gname = %s, adp = %s\n",
		gname, adp));

	log_syserr("Group create not supported for Linux/bonding.");

	DEBUGP((stdout, "nw_group_create: exit\n"));
	return (0);
}
