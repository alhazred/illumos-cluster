/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)string_list.cc	1.2	08/05/20 SMI"

#include <stdio.h>		// for printf
#include <stdlib.h>		// for free
#include <string.h>		// for strdup
#include <assert.h>		// for assert
#include "string_list.h"

/*
 * ********************************************************************
 * fully free a string_list
 *
 * arg:
 *   o double pointer on the string_list
 *
 * returned value
 *   o void
 * ********************************************************************
 */
void
free_strl(string_list_t **P_strl)
{
	string_list_t *previous = NULL;
	string_list_t *l;

	assert(P_strl != NULL);
	l = *P_strl;

	while (l != NULL) {
		previous = l;
		l = l-> next;

		free(previous->string); previous->string = NULL;
		free(previous); previous = NULL;
	}
	*P_strl = NULL;
}

/*
 * ********************************************************************
 * insert a string in the string_list after having checked it is not
 * yet present.
 *
 * arg:
 *   o double pointer on the string_list (as the string_list head may
 *     have to be updated.
 *   o a string to add
 *
 * returned value
 *   o 0 in case of success (i.e the string is in the list after the call)
 *   o -1 otherwise (allocation problem, ...)
 * ********************************************************************
 */
int
insert_uniquely_in_strl(string_list_t **P_strl, const char *P_str)
{
	string_list_t *elem;
	int err = 0;

	assert(P_str != NULL);
	assert(P_strl != NULL);

	for (elem = *P_strl; elem != NULL; elem = elem->next)
		if (strcmp(elem->string, P_str) == 0) //lint !e668
			goto end;

	elem = (string_list_t *)
	    malloc(sizeof (string_list_t));

	if (elem == NULL) {
		err = -1;
		goto end;
	}

	elem->next = *P_strl;
	*P_strl = elem;
	elem->string = strdup(P_str);

	if (elem->string == NULL) {
		err = -1;
		goto end;
	}

end:
	return (err);


}

void
print_strl(const string_list_t *P_strl)
{
	while (P_strl != NULL) {
		(void) puts(P_strl->string);
		P_strl = P_strl->next;
	}
}
