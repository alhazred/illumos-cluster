/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scslmadm.cc	1.14	08/05/20 SMI"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <libgen.h>
#include <locale.h>		// for locale
#include <sys/types.h>		// boolean_t
#include <string.h>
#include <strings.h>		// for index
#include <assert.h>		// for assert
#include <ctype.h>		// for isdigit
#include <libintl.h>		// for gettext

// cluster
#include <scha.h>		// to check node
#include <rgm/sczones.h>

// Command line utilities
#include <sys/cl_auth.h>
#include <sys/cl_cmd_event.h>
#include <sys/cl_events.h>
#include <sys/cl_eventdefs.h>

// Ganymede
#include <scslmadm.h>
#include <scslm.h>		// for error management
#include <scslm_sensor.h>
#include <scslm_ccr.h>
#include <scslm_cli.h>
#include <scslm_cacao.h>

#include "string_list.h"


// size of buffer receiving the command sent to cacao
#define	COMMAND_BUFFER_SIZE  8192

#define	EXIT() { if (scslm_errno != SCSLM_NOERR)	\
			scslm_perror(NULL); exit(scslm_errno); }

/* options from the command line */
static int G_opt_greenwich = 0;
static int G_opt_average = 0;
static sensor_t *G_sensor_list = NULL; // read from CCR table

/*
 * ********************************************************************
 *
 * Note: current implementation of lists is reverting the content of the table
 * ********************************************************************
 */


/*
 * ********************************************************************
 * print usage of this command
 *
 * arg: "name" is argv[0]
 * ********************************************************************
 */
static void
usage(char *name)
{
	scslm_perror(NULL);
	(void) printf(
	    "\n%s: %s -e|-n|-l|-s|-p [options]\n"
	    "\n"
	    "Enable (-e)/ Disable (-n) options are:\n"
	    "\t-k <" STR_KINAME ">, ... to specify " STR_KINAME " \n"
	    "\t-t <" STR_MOTYPE "> to filter on a given " STR_MOTYPE "\n"
	    "\n"
	    "List objects (-l) options are:\n"
	    "\t" STR_MOTYPE "\n"
	    "\t" STR_KINAME " [-t <" STR_MOTYPE ">]\n"
	    "\n"
	    "Print telemetry values (-p) options are:\n"
	    "\t-g print time in UTC (also known as GMT) time\n"
	    "\t-k <" STR_KINAME ">,...\n"
	    "\t-m (" STR_MOTYPE "|" STR_MONAME "|"
	    STR_NODENAME ")=<value>,...\n"
	    "\t   determine what is to be printed\n"
	    "\t-a print data over a time interval instead of printing "
	    "detailed data\n"
	    "\t-d <date-range>\n"
	    "\t    date-range uses the format:\n"
	    "\t\tdate,date\n"
	    "\t\tdate+\n"
	    "\t\tdate-\n"
	    "\t    where date is of the ISO form\n"
	    "\t\tYYYY-MM-DD, YYYY-MM-DD\"T\"HH:MM or YYYY-MM-DD\"T\"HH:MM:SS\n"
	    "\n"
	    "List status (-s)\n\n",
	    gettext("usage"), basename(name));

	/*
	 * We do not use EXIT macro here as this macro is
	 * displaying the error string that is already displayed
	 * before the usage
	 */

	exit(scslm_errno);
}

/*
 * ********************************************************************
 * read the table containing the current configuration of the
 * sensors and fill a global variable
 *
 * ********************************************************************
 */
static scslm_error_t
init_sensor_config()
{
	if (G_sensor_list != NULL)
		return (SCSLM_NOERR);

	return (scslm_read_table_ccr(SCSLMADM_TABLE,
		store_in_sensor_list,
		(void *)&G_sensor_list,
		0));
}

static void
free_sensor_config()
{
	free_sensor_list(&G_sensor_list);
}


/*
 * ********************************************************************
 *
 * call ScslmadmCommand via cacaocsc.
 *
 * Params:
 *   o P_args: command to call
 *
 * Returned value:
 *   o status
 *
 * ********************************************************************
 */
static scslm_error_t
call_ScslmadmCommand(const char *P_argv)
{
	FILE *L_fp = NULL;
	char L_cmd[COMMAND_BUFFER_SIZE + 8] = "";
	char *L_tmp;		// temp pointer
	char L_buf[BUFSIZ];
	char L_date_buf[128];
	long L_date_value;
	char *L_date;
	char *L_node;
	char *L_motype;
	char *L_kiname;
	char *L_instance;
	char *L_value;
	const char *L_short_motype;
	const char *L_short_kiname;
	double L_convert_value;
	char	*L_timezone = NULL;
	ki_t *L_associated_ki;


	if (init_sensor_config() != SCSLM_NOERR)
		goto end;

	/* change timezone if CLI options request it */
	if (G_opt_greenwich == 1) {
		/* Get current timezone */
		L_timezone = getenv("TZ");

		if (putenv("TZ=GMT") != 0) {
			(void) scslm_seterror(SCSLM_ENOMEM,
			    gettext("Unable to allocate memory.\n"));
			goto end;
		}
	}

	(void) strcat(strcat(L_cmd, P_argv), " 2>&1");

	if ((L_fp = popen(L_cmd, "r")) == NULL) {
		(void) scslm_seterror(SCSLM_EINTERNAL,
		    gettext("Failed to execute \"%s\".\n"), L_cmd);
		goto end;
	}

	while (fgets(L_buf, BUFSIZ, L_fp) != NULL) {
		/*
		 * In the following we try to catch different kind of errors:
		 *
		 *  o cacao is not started
		 *  o ganymede module is not enabled in cacao
		 *  o an internal error has been raised in ganymede modue
		 *  o a known (i.e protocol) error occured
		 *
		 *  Note: the first two errors can be trapped via the
		 *  pclose returned value, however it is a little bit
		 *  too late. A prefix for valid data should be used
		 *  to differentiate valid returned value and error.
		 */
		if (strncmp(L_buf,
			"cacaocsc: unable to connect", 27) == 0) {
			(void) scslm_seterror(SCSLM_EINTERNAL,
			    gettext("Failed to connect to SLM Cacao "
				"Module. Please, check that cacao is "
				"currently running.\n"));
			goto end;
		}

		// Case where cacao module is disabled (error == 1)
		if (strncmp(L_buf,
			"java.lang.Exception: command not found", 38) == 0) {
			(void) scslm_seterror(SCSLM_EINTERNAL,
			    gettext("SLM Cacao Module is not reachable."
				" Please, check your configuration.\n"));
			goto end;
		}

		if (strncmp(L_buf, "java.lang.Exception", 19) == 0) {
			(void) scslm_seterror(SCSLM_EINTERNAL,
			    gettext("A problem occured with SLM "
				"Cacao Module.\n"));
			goto end;
		}

		if (strncmp("cacaocsc:", L_buf, 9) == 0) {
			(void) scslm_seterror(SCSLM_EINTERNAL,
			    gettext("A problem occured with %s.\n"), L_buf);
			goto end;
		}


		if (strncmp("ERROR: ", L_buf, 6) == 0) {
			int L_error_code;
			/* extract error code */
			(void) sscanf(L_buf, "ERROR: %ld", &L_error_code);

			switch (L_error_code) {
			case 1:
				(void) scslm_seterror(SCSLM_EINTERNAL,
				    gettext("A protocol error occured"
					" with SLM Cacao module.\n"));
				break;
			case 2:
				(void) scslm_seterror(SCSLM_EINTERNAL,
				    gettext("SLM Cacao module had an "
					"internal error.\n"));
				break;
			default:
				(void) scslm_seterror(SCSLM_EINTERNAL,
				    gettext("Unexpected error returned"
					" by SLM Cacao module (error = %d).\n"),
				    L_error_code);
			}
			goto end;
		}

		/* remove Cariage return at the end */
		L_tmp = index(L_buf, '\n');
		if (L_tmp != NULL)
			*L_tmp = '\0';

		/* the last line is containing a "NL" (ascii=10) only */
		if (L_tmp == L_buf)
			break;

		/* get timestamp and convert it */
		L_date = strtok(L_buf, " ");
		if (L_date == NULL) {
			(void) scslm_seterror(SCSLM_EINTERNAL,
			    gettext("Invalid line returned by SLM "
				"Cacao Module.\n"));
			goto end;
		}
		/* extract timestamp from buffer */
		(void) sscanf(L_date, "%ld", &L_date_value);

		/*
		 * returned value is error code. they are trapped
		 * before with the several strncmp
		 */
		(void) cftime(L_date_buf, NULL, &L_date_value);

		/* get node */
		L_node = strtok(NULL, " ");
		if (L_node == NULL) {
			(void) scslm_seterror(SCSLM_EINTERNAL,
			    gettext("Invalid line returned by SLM "
				"Cacao Module.\n"));
			goto end;
		}

		/* get motype and get short notation */
		L_motype = strtok(NULL, " ");
		L_short_motype = make_short_mot(L_motype);
		if (L_motype == NULL) {
			(void) scslm_seterror(SCSLM_EINTERNAL,
			    gettext("Invalid line returned by SLM "
				"Cacao Module.\n"));
			goto end;
		}

		/* get telemetry attribute and make it short */
		L_kiname = strtok(NULL, " ");
		L_short_kiname = make_short_ki(L_kiname);

		if (L_kiname == NULL) {
			(void) scslm_seterror(SCSLM_EINTERNAL,
			    gettext("Invalid line returned by SLM "
				"Cacao Module.\n"));
			goto end;
		}

		/* get instance */
		L_instance = strtok(NULL, " ");
		if (L_instance == NULL) {
			(void) scslm_seterror(SCSLM_EINTERNAL,
			    gettext("Invalid line returned by SLM "
				"Cacao Module.\n"));
			goto end;
		}

		/* Finally get value */
		L_value = strtok(NULL, " ");
		if (L_value == NULL) {
			(void) scslm_seterror(SCSLM_EINTERNAL,
			    gettext("Invalid line returned by SLM "
				"Cacao Module.\n"));
			goto end;
		}

		(void) setlocale(LC_ALL, "C");
		(void) sscanf(L_value, "%lf", &L_convert_value);
		(void) setlocale(LC_ALL, "");

		L_associated_ki = fetch_ki_from_sensor_list(G_sensor_list,
		    L_kiname, L_motype);

		(void) printf("%ld %s %s %s %s %s %lf %s\n",
		    L_date_value,
		    L_date_buf,
		    L_node,
		    ((L_short_motype != NULL) ? L_short_motype : L_motype),
		    ((L_short_kiname != NULL) ? L_short_kiname : L_kiname),
		    L_instance,
		    L_convert_value,
		    ((L_associated_ki == NULL) ? "" :
			gettext(L_associated_ki->unit)));
	}

end:
	if (L_fp != NULL)
		/*
		 * We could test the pclose status instead of parsing
		 * string returned by the popen
		 *
		 * in cacao 1.0b25:
		 *  65280 ... when ganymede module is not started
		 *  256 ..... when cacao is not started
		 */
		(void) pclose(L_fp);


	/* restore the old timezone */
	if ((G_opt_greenwich == 1) &&
	    (L_timezone != NULL)) {
		char *L_set_tz;

		/*
		 * Assign the correct size for the string: It equals
		 * the size of the prefix ("TZ=") plus the size of the
		 * assigned value (strlen(L_timezone)) plus 1 for the
		 * '\0'
		 */
		L_set_tz = (char *)malloc(
		    (strlen(L_timezone) + 4) * sizeof (char*));
		if (L_set_tz == NULL)
			(void) scslm_seterror(SCSLM_ENOMEM,
			    gettext("Unable to allocate memory.\n"));
		else {
			(void) strcpy(L_set_tz, "TZ=");
			(void) strcat(L_set_tz, L_timezone);

			if (putenv(L_set_tz) != 0)
				(void) scslm_seterror(SCSLM_ENOMEM,
				    gettext("Unable to allocate memory.\n"));
		}
	}
	free_sensor_config();

	return (scslm_errno);
}

/*
 * ********************************************************************
 *
 * verify the nodename
 *
 * Params:
 *   o P_host: host to check (or id coded as a string)
 *
 * Returned value:
 *   o status
 *
 * ********************************************************************
 */
static scslm_error_t
check_nodename(const char *P_host) {

	int is_string = 0;
	scha_cluster_t cl_handle;
	int scha_is_open = 0;
	scha_err_t scha_err;
	int id = -1;
	const char *L_ptr;
	scslm_error_t err;

	assert(P_host != NULL);

	for (L_ptr = P_host; *L_ptr; L_ptr++)
		if (!isdigit(*L_ptr) && (*L_ptr != '.')) {
			is_string = 1;
			break;
		}


	if ((scha_err = scha_cluster_open(&cl_handle)) != SCHA_ERR_NOERR) {
		err = scslm_seterror(SCSLM_EINTERNAL,
		    gettext("scha_cluster_open failed with %d.\n"),
		    scha_err);
		goto end;
	}

	scha_is_open = 1;

	if (is_string) {
		scha_err = scha_cluster_get(cl_handle,
		    SCHA_NODEID_NODENAME, P_host, &id);
		if (scha_err != SCHA_ERR_NOERR) {
			err = scslm_seterror(SCSLM_EINVAL,
			    gettext("\"%s\" is not a configured \"%s\".\n"),
			    P_host, STR_NODENAME);
			goto end;
		}
	} else {
		char *tmp;

		id = atoi(P_host); //lint !e668
		scha_err = scha_cluster_get(cl_handle, SCHA_NODENAME_NODEID,
		    id, &tmp);
		if (scha_err != SCHA_ERR_NOERR) {
			err = scslm_seterror(SCSLM_EINVAL,
			    gettext("The nodeid \"%s\" is not a "
				"valid nodeid.\n"),
			    P_host);
			goto end;
		}
	}

	err = SCSLM_NOERR;
end:
	if (scha_is_open)
		(void) scha_cluster_close(cl_handle); //lint !e644

	return (err);
}



/*
 * ********************************************************************
 * This function is called by qsort(3C). It is used to sort displayed
 * strings (-l)
 * ********************************************************************
 */
static int
qsort_compare_strings(const void *p1, const void *p2)
{
	return (strcmp(*(char **)p1, *(char **)p2));
}

/*
 * ********************************************************************
 * Display the sensor status (enabled/disabled)
 *
 * Params:
 *
 * Returned code:
 *    o status
 *
 * ********************************************************************
 */
static scslm_error_t
display_sensor_config()
{
	sensor_t *ptr = G_sensor_list;
	ki_t *ki_ptr = NULL;

	unsigned int count = get_nb_ki_in_sensor_list(G_sensor_list);
	unsigned int line_len = 0;
	char **table = NULL;
	unsigned int ii;
	scslm_error_t err;
	const char *format = "%-*s %-*s %-*s\n";

	unsigned int mot_name_len;
	unsigned int ki_name_len = strlen(STR_KINAME);
	unsigned int enabled_len = strlen(gettext("Status"));

	/* compute mot_name width */
	mot_name_len = strlen(STR_MOTYPE);

	if (enabled_len < strlen(STR_ENABLED))
		enabled_len = strlen(STR_ENABLED);
	if (enabled_len < strlen(STR_DISABLED))
		enabled_len = strlen(STR_DISABLED);

	// Retrieve the size of each field
	for (ptr = G_sensor_list; ptr != NULL; ptr = ptr->next) {
		ki_ptr = ptr->ki_list;
		while (ki_ptr != NULL) {
			char *L_temp;
			const char *L_short;
			/* make short MOtype */
			L_temp = ki_ptr->motype;
			L_short = make_short_mot(L_temp);

			/* motype must be known as read from CCR */
			assert(L_short != NULL);

			ki_ptr->motype = strdup(L_short);
			/* check of NOMEM is below */
			free(L_temp);

			/* make short KI */
			L_temp = ki_ptr->name;
			L_short = make_short_ki(L_temp);

			/* motype must be known as read from CCR */
			assert(L_short != NULL);

			ki_ptr->name = strdup(L_short);
			free(L_temp);

			if ((ki_ptr->motype == NULL) ||
			    (ki_ptr->name == NULL)) {
				err = scslm_seterror(SCSLM_ENOMEM,
				    gettext("Unable to allocate memory.\n"));
				goto end;
			}

			if (strlen(ki_ptr->motype) > mot_name_len)
				mot_name_len = strlen(ki_ptr->motype);
			if (strlen(ki_ptr->name) > ki_name_len)
				ki_name_len = strlen(ki_ptr->name);
			ki_ptr = ki_ptr->next;
		}
	}
	// Retrieve the size of each field
	ptr = G_sensor_list;
	while (ptr != NULL) {
		ki_ptr = ptr->ki_list;
		while (ki_ptr != NULL) {
			ki_ptr = ki_ptr->next;
		}
		ptr = ptr->next;
	}

	/* + 4 corresponds to 2 spaces + '.\n' + '\0' */
	line_len = mot_name_len + ki_name_len + enabled_len + 4;


	table = (char **)malloc((unsigned int) count * sizeof (char*));
	if (table == NULL) {
		err = scslm_seterror(SCSLM_ENOMEM,
		    gettext("Unable to allocate memory.\n"));
		goto end;
	}
	(void) memset(table, 0, count * sizeof (char*));
	for (ii = 0; ii < count; ii++) {
		table[ii] = (char*)malloc(line_len);
		if (table[ii] == NULL) {
			err = scslm_seterror(SCSLM_ENOMEM,
			    gettext("Unable to allocate memory.\n"));
			goto end;
		}
	}

	ptr	   = G_sensor_list;
	ki_ptr = NULL;
	ii	   = 0;
	while (ptr != NULL) {
		ki_ptr = ptr->ki_list;
		while (ki_ptr != NULL) {
			(void) snprintf(table[ii++], line_len, format,
			    mot_name_len, ki_ptr->motype,
			    ki_name_len, ki_ptr->name,
			    enabled_len,
			    ((strcmp(ki_ptr->enabled, "true") == 0)
				? STR_ENABLED : STR_DISABLED));
			ki_ptr = ki_ptr->next;
		}
		ptr = ptr->next;
	}

	qsort(table, (unsigned int) count, sizeof (char *),
	    qsort_compare_strings);


	// print header
	(void) fprintf(stdout, format,
	    mot_name_len, STR_MOTYPE,
	    ki_name_len, STR_KINAME,
	    enabled_len, gettext("Status"));


	/*
	 * print RULE with different precision. Here the arguments are
	 * fetched with there index. We have three groups for the
	 * format. Each one will print the argument 1 -- string RULE
	 * -- with different precision. For instance, %1$.2$s will print
	 * arg[1] with the precision given in argv[2].
	 */

	/* LINTED */
	(void) fprintf(stdout,
	    "%1$.*2$s %1$.*3$s %1$.*4$s\n", //lint !e557
	    RULE,
	    mot_name_len, ki_name_len, enabled_len);

	// print real lines
	ii = 0;
	while (ii < count)
		(void) fprintf(stdout, "%s", table[ii++]);

	err = SCSLM_NOERR;
end:
	// free
	for (ii = 0; ii < count; ii++) {
		if (table[ii]) free(table[ii]);
	}
	if (table) free(table);

	return (err);
}


/*
 * ********************************************************************
 *
 * Display the list of motypes. the method consists in getting sensor
 * information, extract ki, then their types. keep only one instance of
 * each. then display them
 *
 * Params:
 *
 * Returned code:
 *    o status
 *
 * ********************************************************************
 */
static scslm_error_t
list_motypes_from_sensor_config()
{
	string_list_t *string_list = NULL;
	sensor_t *ptr = NULL;
	ki_t *ki_ptr = NULL;
	unsigned int longest = strlen(STR_MOTYPE); // longest string

	ptr = G_sensor_list;
	while (ptr != NULL) {

		ki_ptr = ptr->ki_list;
		while (ki_ptr != NULL) {
			const char *short_mot = make_short_mot(ki_ptr->motype);

			assert(short_mot != NULL);

			if (insert_uniquely_in_strl(&string_list,
				short_mot) != 0) {
				(void) scslm_seterror(SCSLM_ENOMEM,
				    gettext("Unable to allocate memory.\n"));
				goto end;
			}

			if (strlen(short_mot) > longest) //lint !e668
				longest = strlen(short_mot); //lint !e668
			ki_ptr = ki_ptr->next;
		}
		ptr = ptr->next;
	}

	/* LINTED */
	(void) fprintf(stdout,
	    STR_MOTYPE "\n" "%1$.*2$s\n", //lint !e557
	    RULE, longest);
	print_strl(string_list);
end:
	free_strl(&string_list);
	return (scslm_errno);
}

/*
 * ********************************************************************
 *
 * Display the list of KI. An mot can be given to filter the resulting
 * list. If this filter (a string) is NULL, no filter is applied
 *
 * Params:
 *    o P_mot: MOType used as filter
 *
 * Returned code:
 *    o status
 *
 * ********************************************************************
 */
static scslm_error_t
list_ki_from_sensor_config(char *P_mot)
{
	/* keep a list of unique motype */
	string_list_t *string_list = NULL;
	sensor_t *ptr	 = NULL;
	ki_t	 *ki_ptr = NULL;
	unsigned int longest = strlen(STR_KINAME); // longest string
	const char	 *long_mot = NULL;
	const char	 *short_ki;

	if (P_mot != NULL) {
		long_mot = make_long_mot(P_mot);
		if (long_mot == NULL) {
			(void) scslm_seterror(SCSLM_EINVAL,
			    gettext("\"%s\" is not a valid \"%s\".\n"),
			    P_mot, STR_MOTYPE);
			goto end;
		}
	}

	ptr = G_sensor_list;
	while (ptr != NULL) {
		ki_ptr = ptr->ki_list;
		while (ki_ptr != NULL) {
			if ((long_mot == NULL) ||
			    strcmp(long_mot, ki_ptr->motype) == 0) {
				short_ki = make_short_ki(ki_ptr->name);
				if (insert_uniquely_in_strl(&string_list,
					short_ki) != 0)
					(void) scslm_seterror(SCSLM_ENOMEM,
					    gettext("Unable to "
						"allocate memory.\n"));

				if (strlen(short_ki) > longest)
					longest = strlen(short_ki);
			}
			ki_ptr = ki_ptr->next;
		}
		ptr = ptr->next;
	}

	(void) fprintf(stdout,
	    STR_KINAME "\n" "%1$.*2$s\n",  //lint !e557
	    RULE, longest);
	print_strl(string_list);
	free_strl(&string_list);
end:
	return (scslm_errno);
}

/*
 * ********************************************************************
 *
 * Management of the -l option. this function calls other functions
 * specific to each representation
 *
 * Params:
 *    o P_object_to_list: The list of object to disply
 *    o P_mot: MOType used as filter
 *
 * Returned code:
 *    o status
 *
 * ********************************************************************
 */
static scslm_error_t
list(char *P_object_to_list, char *P_motype)
{

	/*
	 * WARNING !!! P_motype can be null...then all objects are
	 * listed */

	if ((P_motype) &&
	    (make_long_mot(P_motype) == NULL)) {
		(void) scslm_seterror(SCSLM_EINVAL,
		    gettext("\"%s\" is not a valid \"%s\".\n"),
		    P_motype, STR_MOTYPE);
		goto end;
	}

	/* cannot be null as filled by getopt "l:" */
	assert(P_object_to_list != NULL);

	if (init_sensor_config() != SCSLM_NOERR)
		goto end;

	/*
	 * the lint warning is related to first arg that may be null,
	 * whereas structuraly it cannot
	 */
	if ((strcmp(P_object_to_list, STR_MOTYPE) == 0) || //lint -e668
	    (strcmp(P_object_to_list, STR_MOTYPE_SHORT) == 0))
		(void) list_motypes_from_sensor_config();
	else if ((strcmp(P_object_to_list, STR_KINAME) == 0) ||
	    (strcmp(P_object_to_list, STR_KINAME_SHORT) == 0))
		(void) list_ki_from_sensor_config(P_motype);
	else
		(void) scslm_seterror(SCSLM_EINVAL,
		    gettext("\"%s\" is not valid.\n"),
		    P_object_to_list);

end:
	free_sensor_config();
	return (scslm_errno);

}

/*
 * ********************************************************************
 *
 * Management of the -s option. read info from CCR then display it.
 *
 * Params:
 *    N/A
 *
 * Returned code:
 *    o status

 * ********************************************************************
 */
static scslm_error_t
status()
{
	if (init_sensor_config() != SCSLM_NOERR)
		goto end;

	/* below we rely on scslm_error to get a status of calls */
	(void) display_sensor_config();
end:
	free_sensor_config();
	return (scslm_errno);
}


/*
 * ********************************************************************
 *
 * split a string containing a list of "short notation KIs" into a
 * linked list of "long notation" KI data.
 *
 * Params:
 *    o P_input: comma separated list of KIs (i.e telemetry attributes)
 *    o P_KIlist [output]: result of the split
 *
 * Returned code:
 *    o status
 *
 *  ********************************************************************
 */
static scslm_error_t
parse_input_list(char *P_input, ki_t **P_KIList)
{
	int L_ki_index = 1;
	char *L_token;
	const char *L_long_ki;

	L_token = strtok(P_input, ",");
	while (L_token != NULL) {
		// create KI in list (with index)
		ki_t *L_ki_tmp = get_ki(P_KIList, L_ki_index++);

		if ((L_long_ki = make_long_ki(L_token)) == NULL) {
			(void) scslm_seterror(SCSLM_EINVAL,
			    gettext("\"%s\" is not a valid \"%s\".\n"),
			    L_token, STR_KINAME);
			goto end;
		}


		// Set the name of the new ki
		if (set_ki_property(L_ki_tmp, KEY_KI_NAME, L_long_ki)
		    != SCSLM_NOERR)
			goto end;
		L_token = strtok(NULL, ",");
	}
end:
	return (scslm_errno);
}


/*
 * ********************************************************************
 * change the activation state of a (KI*, MOtype)
 *
 * Note: only (ki, MOtype) is unique. the same kiname may exist
 *       for several motypes.
 *
 * Params:
 *    o P_motype: the selected MOtype
 *    o P_KIlist: the ki list to change
 *    o P_state: the new activation state
 *
 * Returned code:
 *    o status
 *
 * ********************************************************************
 */
static scslm_error_t
scslmadm_set_enable_state(char *P_motype, ki_t *P_kilist, char *P_state)
{
	sensor_t *L_sensor_list = NULL;
	scslm_error_t err;
	ki_t *ki_ptr = NULL;
	const char *L_long_mot;

	assert(P_motype != NULL);

	if ((L_long_mot = make_long_mot(P_motype)) == NULL) {
		err = scslm_seterror(SCSLM_EINVAL,
		    gettext("\"%s\" is not a valid \"%s\".\n"),
		    P_motype, STR_MOTYPE);
		goto end;
	}


	err = scslm_read_table_ccr(SCSLMADM_TABLE,
	    store_in_sensor_list,
	    &L_sensor_list,
	    1 /* keep_alive */);

	if (err != SCSLM_NOERR)
		goto end;

	while (P_kilist != NULL) {
		ki_ptr = fetch_ki_from_sensor_list(
		    L_sensor_list,
		    P_kilist->name,
		    L_long_mot);


		if (ki_ptr == NULL) {
			err = scslm_seterror(SCSLM_EINVAL,
			    gettext("There is no \"%s\" with name \"%s\""
				" and type \"%s\".\n"),
			    STR_KINAME,
			    make_short_ki(P_kilist->name), P_motype);
			goto end;
		}

		if (strcmp(ki_ptr->enabled, P_state) != 0) {
			err = set_ki_property(ki_ptr,
			    KEY_KI_ENABLED, P_state);

			if (err != SCSLM_NOERR) {
				goto end;
			}
		}
		// goto to next element in input list
		P_kilist = P_kilist->next;
	}

	err = scslmadm_update_table_ccr(SCSLMADM_TABLE,
	    L_sensor_list);

	if (err == SCSLM_NOERR) {
		/* post an event */
		(void) sc_publish_event(ESC_CLUSTER_SLM_CONFIG_CHANGE,
		    CL_EVENT_PUB_SLM,
		    CL_EVENT_SEV_INFO,
		    CL_EVENT_INIT_SYSTEM, 0, 0, 0,
		    CL_CONFIG_ACTION,
		    SE_DATA_TYPE_UINT32, CL_EVENT_CONFIG_PROP_CHANGED,
		    CL_CCR_TABLE_NAME, SE_DATA_TYPE_STRING, SCSLMADM_TABLE,
		    NULL);
	}
end:
	if (L_sensor_list != NULL)
		free_sensor_list(&L_sensor_list);
	return (err);
}



/*
 * *********************************************************************
 *
 * print the instrumentation values
 *
 * This calls connects to the CACAO module of Ganymede using
 * 'cacaocsc' CLI.
 *
 * Params:
 *    o P_kilist: the list of kis user is interested in
 *    o P_modesc: the fields to select a managod object (for instance
 *                a node name, ...)
 *    o P_date_range: the range of date the user is intersted in.
 *
 * Returned value:
 *    o status

 * *********************************************************************
 */

static scslm_error_t
print(
    char *P_kilist,
    char *P_modesc,
    char *P_date_range)
{
	char *long_ki_list = NULL; // list of ki (long notation)
	int port = -1;
	const char *cacaocsc_path;
	char cacaocsc_port[128];

	// TODO [CODE REVIEW]: manage nicely this buffer. The solution
	// to have a very big buffer is not nice
	static char command[COMMAND_BUFFER_SIZE] = "";
	char *cmd_ptr = command;

	cacaocsc_path = get_cacaocsc_path();
	if (cacaocsc_path == NULL)
		goto end;

	cmd_ptr = strcat(cmd_ptr, cacaocsc_path);

	if (scslmadm_get_command_stream_adapter_port(&port) != SCSLM_NOERR)
		goto end;

	(void) sprintf(cacaocsc_port, " -p %d", port);
	cmd_ptr = strcat(cmd_ptr, cacaocsc_port);
	cmd_ptr = strcat(cmd_ptr, CACAOCSC_SCSLMADM_CMD);

	if (P_kilist != NULL) {
		if (make_long_ki_list(P_kilist, &long_ki_list)
		    != SCSLM_NOERR) {
			(void) scslm_seterror(SCSLM_EINVAL,
			    gettext("\"%s\" is not valid for \"%s\".\n"),
			    long_ki_list, STR_KINAME);
			goto end;
		}

		cmd_ptr = strcat(cmd_ptr, " -k ");
		cmd_ptr = strcat(cmd_ptr, long_ki_list);
	}

	if (P_modesc != NULL) {
		char *token;
		char *pos;
		char *key;
		char *value;
		const char *L_motype = NULL;

		cmd_ptr = strcat(cmd_ptr, " -m ");

		token = strtok(P_modesc, ",");
		while (token != NULL) {

			pos = strchr(token, '=');
			if (pos == NULL) {
				(void) scslm_seterror(SCSLM_EINVAL,
				    gettext("\"%s\" is not valid.\n"),
				    token);
				goto end;
			}
			*pos = '\0';

			key = strdup(token);
			value = strdup(pos + 1);
			*pos = '=';

			cmd_ptr = strcat(cmd_ptr, key);
			cmd_ptr = strcat(cmd_ptr, "=");

			if (strcmp(key, STR_MOTYPE) == 0) {
				if (L_motype != NULL) {
					(void) scslm_seterror(SCSLM_EINVAL,
					    gettext("Only one \"%s\" can be "
						"specified.\n"),
					    STR_MOTYPE);
					goto end;
				}

				L_motype = make_long_mot(value);

				if (L_motype == NULL) {
					(void) scslm_seterror(
					    SCSLM_EINVAL,
					    gettext("\"%s\" is not valid for"
						" \"%s\".\n"),
					    value, STR_MOTYPE);
					goto end;
				}


				cmd_ptr = strcat(cmd_ptr, L_motype);
			} else if (strcmp(key, STR_NODENAME) == 0) {
				if (check_nodename(value) != SCSLM_NOERR)
					goto end;
				cmd_ptr = strcat(cmd_ptr, value);
			} else if (strcmp(key, STR_MONAME) != 0) {
				(void) scslm_seterror(SCSLM_EINVAL,
				    gettext("\"%s\" is not a valid key.\n"),
				    key);
				goto end;
			} else
				cmd_ptr = strcat(cmd_ptr, value);

			token = strtok(NULL, ",");

			if (token != NULL) {
				cmd_ptr = strcat(cmd_ptr, ",");
			}
		}
	}


	if (G_opt_average == 1) {
		cmd_ptr = strcat(cmd_ptr, " -y average");
	} else {
		cmd_ptr = strcat(cmd_ptr, " -y latest");
	}


	if (P_date_range != NULL) {
		long L_start_time = 0;
		long L_stop_time = 0;
		char L_buf[128] = "";

		if (convert_date(P_date_range, &L_start_time, &L_stop_time)
		    != SCSLM_NOERR)
			goto end;

		if (L_start_time != 0) {
			(void) sprintf(L_buf, " -start %ld", L_start_time);
			cmd_ptr = strcat(cmd_ptr, L_buf);
		}

		if (L_stop_time != 0) {
			(void) sprintf(L_buf, " -stop %ld", L_stop_time);
			cmd_ptr = strcat(cmd_ptr, L_buf);
		}
	}

	cmd_ptr = strcat(cmd_ptr, "'");
	/* result of this command is also in scsclm_errno */
	(void) call_ScslmadmCommand(command);
end:
	if (long_ki_list != NULL)
		free(long_ki_list), long_ki_list = NULL;

	return (scslm_errno);
}


/*
 * MAIN
 */
int
main(int argc, char **argv)
{
	action_t action = ACTION_UNKNOWN;
	int c;
	char *input_motype = NULL;
	char *input_mo = NULL;
	char *input_ki_list = NULL;
	char *input_date_range = NULL;
	char *l_arg	  = NULL;	// arg of -l opt
	ki_t *ki_list = NULL;

	/* Initialize, demote to normal uid, check */
	/* authorization and then promote to euid=0 */
	cl_auth_init();
	cl_auth_demote();

	// I18N housekeeping
	(void) setlocale(LC_ALL, "");
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);
	(void) textdomain(TEXT_DOMAIN);

	// parse options
	while ((c = getopt(argc, argv, "nepsl:t:m:d:k:ag")) != EOF) {
		switch (c) {
		case 'n':
			if (action != ACTION_UNKNOWN) {
				(void) scslm_seterror(SCSLM_EINVAL,
				    gettext("Only one action can be "
					"specified"));
				usage(argv[0]);
			}
			action = ACTION_DISABLE;
			break;
		case 'e':
			if (action != ACTION_UNKNOWN) {
				(void) scslm_seterror(SCSLM_EINVAL,
				    gettext("Only one action can be "
					"specified"));
				usage(argv[0]);
			}
			action = ACTION_ENABLE;
			break;
		case 'p':
			if (action != ACTION_UNKNOWN) {
				(void) scslm_seterror(SCSLM_EINVAL,
				    gettext("Only one action can be "
					"specified"));
				usage(argv[0]);
			}
			action = ACTION_PRINT;
			break;
		case 's':
			if (action != ACTION_UNKNOWN) {
				(void) scslm_seterror(SCSLM_EINVAL,
				    gettext("Only one action can be "
					"specified"));
				usage(argv[0]);
			}
			action = ACTION_STATUS;
			break;
		case 'l':
			if (action != ACTION_UNKNOWN) {
				(void) scslm_seterror(SCSLM_EINVAL,
				    gettext("Only one action can be "
					"specified"));
				usage(argv[0]);
			}
			action = ACTION_LIST;
			l_arg = strdup(optarg);
			if (l_arg == NULL) {
				(void) scslm_seterror(SCSLM_EINVAL,
				    gettext(
					"Unable to allocate memory.\n"));
				EXIT(); //lint !e527
			}

			break;
		case 't':
			input_motype = strdup(optarg);
			if (input_motype == NULL) {
				(void) scslm_seterror(SCSLM_EINVAL,
				    gettext(
					"Unable to allocate memory.\n"));
				EXIT(); //lint !e527
			}
			break;
		case 'm':
			input_mo = strdup(optarg);
			if (input_mo == NULL) {
				(void) scslm_seterror(SCSLM_EINVAL,
				    gettext(
					"Unable to allocate memory.\n"));
				EXIT(); //lint !e527
			}
			break;
		case 'd':
			input_date_range = strdup(optarg);
			if (input_date_range == NULL) {
				(void) scslm_seterror(SCSLM_EINVAL,
				    gettext(
					"Unable to allocate memory.\n"));
				EXIT(); //lint !e527
			}
			break;
		case 'k' :
			input_ki_list = strdup(optarg);
			if (input_ki_list == NULL) {
				(void) scslm_seterror(SCSLM_EINVAL,
				    gettext(
					"Unable to allocate memory.\n"));
				EXIT(); //lint !e527
			}
			break;
		case 'a' :
			G_opt_average = 1;
			break;
		case 'g' :
			G_opt_greenwich = 1;
			break;
		default:
			(void) scslm_seterror(SCSLM_EINVAL,
			gettext(
			    "Option \"%c\" is not supported.\n"),
				    optopt);
			usage(argv[0]);
			break;	// UNREACHABLE usage is exiting
		}
	}


	if (optind < argc) {
		(void) scslm_seterror(SCSLM_EINVAL,
		    gettext("Unknown argument \"%s\".\n"),
		    argv[optind]);
		usage(argv[0]);
	}

	/* This command can only be called from global zone */
	if (sc_zonescheck() != 0) {
		(void) scslm_seterror(SCSLM_EACCESS,
		    gettext("Current user is not allowed to "
			"execute this command.\n"));
		EXIT();		//lint !e527
	}

	// basic checks
	switch (action) {
	case ACTION_LIST:
		if (input_mo != NULL) {
			(void) scslm_seterror(SCSLM_EINVAL,
			    gettext("You cannot use the -m option with the "
			    "-l option.\n"));
			usage(argv[0]);
		}

		if (input_ki_list != NULL) {
			(void) scslm_seterror(SCSLM_EINVAL,
			    gettext("You cannot use the -k option with the "
			    "-l option.\n"));
			usage(argv[0]);
		}

		cl_auth_check_command_opt_exit(*argv, NULL,
		    CL_AUTH_SYSTEM_READ, SCSLM_EACCESS);
		break;
	case ACTION_ENABLE:
	case ACTION_DISABLE:
		if (input_mo != NULL) {
			(void) scslm_seterror(SCSLM_EINVAL,
			    gettext("You cannot use the -m option with the "
			    "-e/-n option.\n"));
			usage(argv[0]);
		}

		cl_auth_check_command_opt_exit(*argv, NULL,
		    CL_AUTH_SYSTEM_MODIFY, SCSLM_EACCESS);
		if (input_motype == NULL || input_ki_list == NULL)  {
			(void) scslm_seterror(SCSLM_EINVAL,
			    gettext("Missing \"%s\" or \"%s\".\n"),
			    STR_MOTYPE, STR_KINAME);
			usage(argv[0]);
			// not return ... usage is exit()ing
		}
		break;

	case ACTION_STATUS:
		if (input_mo != NULL) {
			(void) scslm_seterror(SCSLM_EINVAL,
			    gettext("You cannot use the -m option with the "
			    "-e/-n option.\n"));
			usage(argv[0]);
		}

		if (input_motype != NULL) {
			(void) scslm_seterror(SCSLM_EINVAL,
			    gettext("You cannot use the -t option with the "
			    "-s option.\n"));
			usage(argv[0]);
		}

		if (input_ki_list != NULL) {
			(void) scslm_seterror(SCSLM_EINVAL,
			    gettext("You cannot use the -k option with the "
			    "-s option.\n"));
			usage(argv[0]);
		}

		cl_auth_check_command_opt_exit(*argv, NULL,
		    CL_AUTH_SYSTEM_READ, SCSLM_EACCESS);
		break;
	case ACTION_PRINT:
		if (input_motype != NULL) {
			(void) scslm_seterror(SCSLM_EINVAL,
			    gettext("You cannot use the -t option with the "
				"-p option.\n"));
			usage(argv[0]);
		}

		cl_auth_check_command_opt_exit(*argv, NULL,
		    CL_AUTH_SYSTEM_READ, SCSLM_EACCESS);
		break;

	case ACTION_UNKNOWN:
		(void) scslm_seterror(SCSLM_EINVAL,
		    gettext("Invalid operation.\n"));
		usage(argv[0]);
		// not return ... usage is exit()ing
		break;
	}

	if (action != ACTION_PRINT) {
		if (G_opt_greenwich > 0) {
			(void) scslm_seterror(SCSLM_EINVAL,
			    gettext("You cannot use the -g option without the"
				" -p option.\n"));
			usage(argv[0]);
		}

		if (input_date_range != NULL) {
			(void) scslm_seterror(SCSLM_EINVAL,
			    gettext("You cannot use the -d option without the"
				" -p option.\n"));
			usage(argv[0]);
		}

		if (G_opt_average > 0) {
			(void) scslm_seterror(SCSLM_EINVAL,
			    gettext("You cannot use the -a option without the"
			    " -p option.\n"));
			usage(argv[0]);
		}
	}

	cl_auth_promote();

	/* Generate event */
	cl_cmd_event_init(argc, argv, (int *)&scslm_errno);
	(void) cl_cmd_event_gen_start_event();
	(void) atexit(cl_cmd_event_gen_end_event);

	/* Initialize long names <=> short names conversion */
	if (glossary_initialize() != SCSLM_NOERR)
		EXIT();

	// execute actions
	switch (action) {
	case ACTION_LIST:
		/* status is also in scslm_errno */
		(void) list(l_arg, input_motype);
		EXIT();		//lint !e527

	case ACTION_STATUS:
		/* status is also in scslm_errno */
		(void) status();
		EXIT();		//lint !e527

	case ACTION_PRINT:
		/* status is also in scslm_errno */
		(void) print(input_ki_list, input_mo,
		    input_date_range);
		EXIT();		//lint !e527

	case ACTION_ENABLE:
		if (parse_input_list(input_ki_list, &ki_list)
		    != SCSLM_NOERR) {
			usage(argv[0]);
		}

		/* status is also in scslm_errno */
		(void) scslmadm_set_enable_state(
		    input_motype, ki_list, SCSLMADM_TRUE);
		EXIT();		//lint !e527

	case ACTION_DISABLE:
		if (parse_input_list(input_ki_list, &ki_list)
		    != SCSLM_NOERR) {
			usage(argv[0]);
		}

		/* status is also in scslm_errno */
		(void) scslmadm_set_enable_state(
		    input_motype, ki_list, SCSLMADM_FALSE);
		EXIT();		//lint !e527

	case ACTION_UNKNOWN:
	default:
		(void) scslm_seterror(SCSLM_EINVAL,
		    gettext("Unknown action.\n"));
		usage(argv[0]);
	}

	/* without the following line, "make lint" complains */
	return (SCSLM_NOERR);
}
