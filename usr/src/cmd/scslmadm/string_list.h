/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */


#ifndef	_STRING_LIST_H
#define	_STRING_LIST_H

#pragma ident	"@(#)string_list.h	1.2	08/05/20 SMI"

typedef struct string_list_t {
	char *string;
	struct string_list_t *next;
} string_list_t;

extern void free_strl(string_list_t **P_strl);
extern int insert_uniquely_in_strl(string_list_t **P_strl, const char *str);
extern void print_strl(const string_list_t *P_strl);

#endif /* _STRING_LIST_H */
