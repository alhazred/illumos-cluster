//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// ccradm.cc - utility for ccr tables
//

#pragma ident	"@(#)ccradm.cc	1.24	08/12/17 SMI"

#include <stdlib.h>		// getopt
#include <stdio.h>
#include <errno.h>
#include <dlfcn.h>
#include <libgen.h>
#include <sys/os.h>
#include <sys/param.h>
#include <sys/cladm.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/clconf_int.h>
#include <h/naming.h>
#include <h/ccr.h>
#include <orb/infrastructure/orb.h>
#include <orb/invo/common.h>
#include <nslib/ns.h>
#include <ccr/persistent_table.h>
#include <ccr/ccr_ds.h>
#include <rgm/sczones.h>
#include <sys/sol_version.h>

//
// Print tool usage
// Arguments:
//	The name of this binary
//
void
usage(const char *bin_name)
{
	os::printf("Usage: \t%s -i <ccr_table_file> [-o]\n", bin_name);
#if SOL_VERSION >= __s10
	os::printf("       \t%s [-z <zone_cluster_name>] -r "
	    "<ccr_table_file> -f <input file>\n", bin_name);
#ifdef WEAK_MEMBERSHIP
	os::printf("       \t%s -m <winner | loser>\n", bin_name);
	os::printf("       \t%s -q\n", bin_name);
#endif // WEAK_MEMBERSHIP
#else
	os::printf("       \t%s -r <ccr_table_file> -f <input file>\n",
	    bin_name);
#endif // SOL_VERSION >= __s10
}

//
// Parse the user input ccrfile_name to get the ccr table name and ccr table
// path, also verifying that the table_name refers to a file under the CCR
// directory - /etc/cluster/ccr.
//
char *
get_ccrtable(char *ccrfile_name)
{
	char		cur_path[PATH_MAX + 1] = "";
	char		*ccrfile_name_copy, *table_path;
	struct stat	table_stat, ccr_stat;

	// Check if the ccrfile_name is a regular file.
	if (stat(ccrfile_name, &ccr_stat) < 0) {
		//
		// It's ok if the specified ccr table file doesn't exist since
		// we are going to recover it with another copy anyway.
		//
		if (errno != ENOENT) {
			perror(ccrfile_name);
			return (NULL);
		}
	} else if (!S_ISREG(ccr_stat.st_mode)) {
		os::printf("ERROR: %s is not a regular file.\n", ccrfile_name);
		return (NULL);
	}

	//
	// Make a copy of the ccrfile_name to be used by dirname() which
	// modifies its string argument.
	//
	if ((ccrfile_name_copy = (char *)strdup(ccrfile_name)) == NULL) {
		perror("strdup");
		return (NULL);
	}

	//
	// parse the ccrfile_name to get the actual table_path
	// if there is any.
	// For example: full path is "/etc/cluster/ccr/global/infrastructure"
	//		table_path is "/etc/cluster/ccr/global"
	//
	table_path = dirname(ccrfile_name_copy);

	//
	// Verifying if the table_path falls within the CCR directory
	// /etc/cluster/ccr/. To do so, we can first chdir to table_path
	// then check the result of getcwd.
	//
	// If user gives a filename only, the current directory needs to be
	// verified since we can only assume user refers to a file under
	// the current directory.
	//

	// change to the table_path first.
	if (chdir(table_path) != 0) {
		perror(table_path);
		delete [] ccrfile_name_copy;
		return (NULL);
	}

	// Now we need to get the current directory after chdir.
	if (getcwd(cur_path, sizeof (cur_path)) == (char *)0) {
		perror("getcwd");
		delete [] ccrfile_name_copy;
		return (NULL);
	}

	// verify if the cur_path is within /etc/cluster/ccr.
	if (strstr(cur_path, CCR_DATA_PATH) == NULL) {
		//
		// check if it is the case that if the /etc/cluster/ccr is
		// a symlink instead. If so, we need to handle this case
		// as well.
		//
		if (stat(cur_path, &table_stat) < 0) {
			perror(cur_path);
			delete [] ccrfile_name_copy;
			return (NULL);
		}
		if (stat(CCR_DATA_PATH, &ccr_stat) < 0) {
			perror(CCR_DATA_PATH);
			delete [] ccrfile_name_copy;
			return (NULL);
		}
		if (table_stat.st_ino != ccr_stat.st_ino ||
		    table_stat.st_dev != ccr_stat.st_dev) {
			//
			// The symbolic path could be /foo/global where
			// foo is a symbolic link to /etc/cluster/ccr
			// So we go back one directory level and check
			// if the inodes match.
			//
			char *tmp_path = dirname(cur_path);
			if (stat(tmp_path, &table_stat) < 0) {
				perror(tmp_path);
				delete [] ccrfile_name_copy;
				return (NULL);
			}
			if (table_stat.st_ino != ccr_stat.st_ino ||
			    table_stat.st_dev != ccr_stat.st_dev) {

				os::printf("ERROR: %s does not refer to a CCR "
				    "table under the CCR directory"
				    "(%s)!\n", ccrfile_name, CCR_DATA_PATH);
				delete [] ccrfile_name_copy;
				return (NULL);
			}
		}
	}
	delete [] ccrfile_name_copy;

	return (basename((char *)ccrfile_name));
}

//
// this function is called when -r and -f options are used to recover
// a table. The table with tname is looked up in the ccr directory and
// if found, will be replaced with the file with fname. The table is
// replaced on all the nodes in the cluster. Only invalid tables can
// be restored.
// Returns different error codes like 1,2,3,4 when same message is printed
// because it would be useful in debugging
//
int
ccr_update(const char *zcname, const char *tname, const char *fname)
{
	Environment e;
	ccr::updatable_table_var upd_ptr;
	ccr::readonly_table_var rd_ptr;
	readonly_persistent_table *read_tab;
	table_element_t *elem;
	CORBA::Exception *ex;
	ccr::element_seq *seq;
	int ret;
	uint_t len = 0;

	if ((ret = ORB::initialize()) != 0) {
		os::printf("ERROR : can't initialize ORB\n");
		return (1);
	}
	naming::naming_context_var ctxp = ns::local_nameserver();
	CORBA::Object_var obj = ctxp->resolve("ccr_directory", e);
	if (e.exception()) {
		os::printf("ccr_directory not found in the name server\n");
		return (1);
	}
	ccr::directory_var ccr_dir = ccr::directory::_narrow(obj);

#if SOL_VERSION >= __s10
	upd_ptr = ccr_dir->begin_transaction_invalid_ok_zc(zcname, tname, e);
#else
	upd_ptr = ccr_dir->begin_transaction_invalid_ok(tname, e);
#endif // SOL_VERSION >= __s10
	if ((ex = e.exception()) != NULL) {
		if (ccr::no_such_table::_exnarrow(ex) != NULL) {
			os::printf("No such table : %s:%s\n", zcname, tname);
			return (1);
		} else  {
			os::printf("Error in restoring table %s:%s\n", zcname,
			    tname);
			return (1);
		}
	}

	//
	// check to see if the table is invalid or not.
	// If the table is valid return after aborting the transaction
	//
#if SOL_VERSION >= __s10
	rd_ptr = ccr_dir->lookup_zc(zcname, tname, e);
#else
	rd_ptr = ccr_dir->lookup(tname, e);
#endif // SOL_VERSION >= __s10

	if ((ex = e.exception()) == NULL) {
		os::printf("table %s is valid and cannot be restored\n", tname);
		upd_ptr->abort_transaction(e);
		e.clear();	// clear any exceptions that occurred
		return (1);
	} else if (ccr::table_invalid::_exnarrow(ex) == NULL) {
		os::printf("Error in restoring table %s\n", tname);
		upd_ptr->abort_transaction(e);
		e.clear();
		return (2);
	}
	e.clear();	// clear the invalid exception

	read_tab = new readonly_persistent_table;
	if (read_tab->initialize(fname) != 0) {
		perror(fname);
		read_tab->close();
		delete read_tab;
		upd_ptr->abort_transaction(e);
		e.clear();	// clear any exceptions that occurred
		return (errno);
	}

	upd_ptr->remove_all_elements(e);
	if (e.exception()) {
		os::printf("Error deleting the old file entries\n");
		read_tab->close();
		delete read_tab;
		upd_ptr->abort_transaction(e);
		e.clear();	// clear any exceptions that occurred
		return (1);
	}

	read_tab->atfirst();
	seq = new ccr::element_seq(len);
	while ((elem = read_tab->next_element(ret)) != NULL) {
		if (!is_meta_row(elem->key)) {
			seq->length(len+1);
			(*seq)[len].key = elem->key;
			(*seq)[len].data = elem->data;
			len++;
		}
	}

	upd_ptr->add_elements(*seq, e);
	read_tab->close();
	delete read_tab;
	delete(ccr::element_seq *)seq;
	if (e.exception()) {		// exception from add_elements
		os::printf("Error in restoring table %s\n", tname);
		upd_ptr->abort_transaction(e);
		e.clear();	// clear any exceptions that occurred.
		return (3);
	}

	upd_ptr->commit_transaction(e);
	if (e.exception()) {
		os::printf("Error in restoring table %s\n", tname);
		upd_ptr->abort_transaction(e);
		e.clear();	// clear any exceptions that occurred
		return (4);
	}
	return (0);
}

int
main(int argc, char **argv)
{
	int arg, bootflags = 0, rc = 0;
	int iflag = 0, oflag = 0, rflag = 0, fflag = 0, Fflag = 0;
	char *ver, *ccrfile_name = NULL, *file_name = NULL, *table_name = NULL;
#ifdef WEAK_MEMBERSHIP
	int mflag = 0, qflag = 0;
	char *winner_loser = NULL;
#endif
	int zcflag = 0;
	char *zc_name = NULL;

	if (sc_zonescheck() != 0)
		return (1);
	// Get the boot flags
	if (os::cladm(CL_INITIALIZE, CL_GET_BOOTFLAG, &bootflags) != 0) {
		perror("_cladm(CL_INITIALIZE, CL_GET_BOOTFLAG)");
		return (1);
	}

	//
	// Get command line options
	//
	// They are:
	//	-i	- init table
	//	-o	- override flag to override the table on all
	//		  data servers.
	//	-z	- zone cluster name.
	//	-r	- recover table
	//	-f	- file to be used for recovering a table
	//	-F	- allow -i to be used when booted as a cluster
	//	-m	- mark this node's CCR winner | loser after split-brain
	//	-q	- query for presence of unresolved split-brain change
	//
	optind = 1;
	while ((arg = getopt(argc, argv, "oFqi:z:r:f:m:?")) != EOF) {
		// Switch on the option
		switch (arg) {
		case 'i' :
			// Initialize table
			iflag = 1;
			ccrfile_name = optarg;
			break;
		case 'z' :
			// cluster name
#if SOL_VERSION >= __s10
			zcflag = 1;
			zc_name = optarg;
#else
			usage(argv[0]);
			return (1);
#endif // SOL_VERSION >= __s10
			break;
		case 'o' :
			oflag = 1;
			break;
		case 'r' :
			rflag = 1;
			ccrfile_name = optarg;
			break;
		case 'F' :
			Fflag = 1;
			break;
		case 'f' :
			fflag = 1;
			file_name = optarg;
			break;
#ifdef WEAK_MEMBERSHIP
		case 'm' :
			mflag = 1;
			winner_loser = optarg;
			break;
		case 'q' :
			qflag = 1;
			break;
#endif // WEAK_MEMBERSHIP
		case '?' :
			usage(argv[0]);
			return (1);
		default :
			return (1);
		} // switch(arg)
	}

	if (((iflag == 1) || (oflag == 1)) && ((rflag == 1) || (fflag == 1))) {
		os::printf(" -i/-o options cannot be used with"
		    " -r/-f options.\n");

		usage(argv[0]);
		return (1);
	}

	if (iflag == 1) {
		if ((Fflag == 0) && (bootflags & CLUSTER_BOOTED)) {
			os::printf("The -i option cannot be used in cluster "
			    "mode\n");
			return (1);
		}

		ASSERT(ccrfile_name);
		ver = (oflag == 1) ? OVRD_VERSION : INIT_VERSION;
		if ((rc = ccrlib::initialize_ccr_table(ccrfile_name, ver)) != 0)
			perror(ccrfile_name);
		return (rc);

	} else if ((rflag != 0) && (fflag != 0)) {
		if ((bootflags & CLUSTER_BOOTED) == 0) {
			os::printf("The -r option cannot be used in"
			    " non-cluster mode\n");
			return (1);
		}

		ASSERT(ccrfile_name);
		ASSERT(file_name);
		if ((table_name = get_ccrtable(ccrfile_name)) != NULL) {
			if (zcflag) {
				rc = ccr_update(zc_name, table_name, file_name);
			} else {
				rc = ccr_update("global", table_name,
				    file_name);
			}
			return (rc);
		} else {
			return (1);
		}
#ifdef WEAK_MEMBERSHIP
	} else if (mflag == 1) {
		if (strcmp(optarg, "winner") == 0) {
			return (ccrlib::mark_ccr_as_winner());
		} else if (strcmp(optarg, "loser") == 0) {
			return (ccrlib::mark_ccr_as_loser());
		} else {
			usage(argv[0]);
		}
	} else if (qflag == 1) {
		if (ccrlib::split_brain_ccr_change_exists()) {
			(void) os::printf("This node has unresolved CCR "
			    "changes after split brain.\n");
		} else {
			(void) os::printf("This node has no unresolved CCR "
			    "changes after split brain.\n");
		}
#endif // WEAK_MEMBERSHIP
	} else {
		usage(argv[0]);
	}
	return (1);
}
