/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the "License").
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

/*	Copyright (c) 1984, 1986, 1987, 1988, 1989 AT&T */
/*	  All Rights Reserved   */

/*
 * University Copyright- Copyright (c) 1982, 1986, 1988
 * The Regents of the University of California
 * All Rights Reserved
 *
 * University Acknowledgment- Portions of this document are derived from
 * software developed by the University of California, Berkeley, and its
 * contributors.
 */

#pragma ident	"@(#)scds_syslog.c	1.8	08/05/20 SMI"

/*
 * This code was taken from the ON consolidation gate as of Solaris 8
 * Beta Refresh:
 *
 *	usr/src/cmd/logger/logger.c	1.12	97/03/31
 *
 * The code was changed to fit the specific needs of the Sun
 * Cluster Data Services and catalog generation requirements.
 * This is a private interface for Sun Cluster 3.0.
 *
 * This script is intended to be used by shell scripts that need
 * to log messages to syslog, but also need those messages to have
 * a unique message id.  This is not currently possible with logger(1)
 * because every message logged via logger has the same message id.
 *
 * The syntax is
 *
 *	scds_syslog [-p priority] [-t tag] -m message_format [arg [...]]
 *
 * The assumptions and restrictions are as follows:
 *
 * - The -m argument is mandatory.
 *
 * - Only the %s type is explicitly supported. No type conversion is
 * attempted, so if a number needs to be formated in a specific way,
 * you should do:
 *
 * 	scds_syslog -m "Here is the hex representation of 1234: %s" \
 * 		`printf "%x" 1234`
 *
 * - Only 20 operands are passed on to syslog. If you have more than 20
 * %s's in your format string, the results are undefined. Specifically,
 * it is likely that a core dump will happen.
 *
 * - The default priority/facility is user.notice.
 *
 * - The default tag is "Cluster.DataService" which should be overwritten
 * with the -t option.
 */

#include <stdio.h>
#include <stdlib.h>
#include <syslog.h>
#include <ctype.h>
#include <string.h>
#include <rgm/scha_strings.h>

struct code {
	char	*c_name;
	int	c_val;
};

static struct code	PriNames[] = {
	"panic",	LOG_EMERG,
	"emerg",	LOG_EMERG,
	"alert",	LOG_ALERT,
	"crit",		LOG_CRIT,
	"err",		LOG_ERR,
	"error",	LOG_ERR,
	"warn",		LOG_WARNING,
	"warning", 	LOG_WARNING,
	"notice",	LOG_NOTICE,
	"info",		LOG_INFO,
	"debug",	LOG_DEBUG,
	NULL,		-1
};

static struct code	FacNames[] = {
	"kern",		LOG_KERN,
	"user",		LOG_USER,
	"mail",		LOG_MAIL,
	"daemon",	LOG_DAEMON,
	"auth",		LOG_AUTH,
	"security",	LOG_AUTH,
	"syslog",	LOG_SYSLOG,
	"lpr",		LOG_LPR,
	"news",		LOG_NEWS,
	"uucp",		LOG_UUCP,
	"cron",		LOG_CRON,
	"local0",	LOG_LOCAL0,
	"local1",	LOG_LOCAL1,
	"local2",	LOG_LOCAL2,
	"local3",	LOG_LOCAL3,
	"local4",	LOG_LOCAL4,
	"local5",	LOG_LOCAL5,
	"local6",	LOG_LOCAL6,
	"local7",	LOG_LOCAL7,
	NULL,		-1
};

static int	pencode(register char *);
static int	decode(char *, struct code *);
static void	usage(void);

/*
 * scds_syslog -- syslog utility for Sun Cluster Data Service shell scripts
 *
 * This command takes a format string that can be used for catalog
 * generation.
 */

#define	SCDS_SYSLOG_MAXARGS	20
#define	SCDS_SYSLOG_TAG		"Cluster.DataService"

int
main(int argc, char *argv[])
{
	char *tag = SCDS_SYSLOG_TAG;
	char *syslog_args[SCDS_SYSLOG_MAXARGS];
	char *syslog_message_format = NULL;
	int pri = LOG_USER|LOG_NOTICE;
	int logflags = 0;
	int opt;
	int i;

	while ((opt = getopt(argc, argv, "t:p:m:")) != EOF) {
		switch (opt) {

		    case 't':		/* tag */
			tag = optarg;
			break;

		    case 'p':		/* priority */
			pri = pencode(optarg);
			break;

		    case 'm':		/* message format string */
			syslog_message_format = optarg;
			break;

		    default:
			usage();
		}
	}

	if (syslog_message_format != NULL) {
		openlog(tag, logflags, SCHA_CLUSTER_LOGFACILITY);

		argc -= optind;
		argv = &argv[optind];

		/*
		 * Build up an array of fixed size to use for the syslog()
		 * call. Each entry in the array will either be one of the
		 * args corresponding to a %s placeholder in the format
		 * string or the empty string if there are no more args
		 * from the user.
		 */
		for (i = 0; i < SCDS_SYSLOG_MAXARGS; i++) {
			if (argc-- > 0)
				syslog_args[i] = argv[i];
			else
				syslog_args[i] = "";
		}

		/*
		 * There is no good way to make this call to syslog
		 * generically using some varargs-like mechanism.
		 */
		syslog(pri, syslog_message_format,
		    syslog_args[0],
		    syslog_args[1],
		    syslog_args[2],
		    syslog_args[3],
		    syslog_args[4],
		    syslog_args[5],
		    syslog_args[6],
		    syslog_args[7],
		    syslog_args[8],
		    syslog_args[9],
		    syslog_args[10],
		    syslog_args[11],
		    syslog_args[12],
		    syslog_args[13],
		    syslog_args[14],
		    syslog_args[15],
		    syslog_args[16],
		    syslog_args[17],
		    syslog_args[18],
		    syslog_args[19]);
	} else {
		usage();
	}

	return (0);
}


/*
 *  Decode a symbolic name to a numeric value
 */

static int
pencode(register char *s)
{
	register char *p;
	int lev;
	int fac;
	char buf[100];

	for (p = buf; *s && *s != '.'; )
		*p++ = *s++;
	*p = '\0';
	if (*s++) {
		fac = decode(buf, FacNames);
		if (fac < 0) {
			(void) fprintf(stderr,
			    "unknown facility name: %s\n", buf);
			fac = LOG_USER;
		}
		for (p = buf; (*p++ = *s++) != NULL; )
			continue;
	} else
		fac = 0;
	lev = decode(buf, PriNames);
	if (lev < 0) {
		(void) fprintf(stderr, "unknown priority name: %s\n", buf);
		lev = LOG_NOTICE;
	}

	return ((lev & LOG_PRIMASK) | (fac & LOG_FACMASK));
}


static int
decode(char *name, struct code *codetab)
{
	register struct code *c;
	register char *p;
	char buf[40];

	if (isdigit(*name))
		return (atoi(name));

	(void) strcpy(buf, name);
	for (p = buf; *p; p++)
		if (isupper(*p))
			*p = (char)tolower(*p);
	for (c = codetab; c->c_name; c++)
		if (strcmp(buf, c->c_name) == 0)
			return (c->c_val);

	return (-1);
}

static void
usage(void)
{
	(void) fprintf(stderr,
	    "Usage:\tscds_syslog [-p priority] [-t tag] "
	    "-m message_format [arg [...]]\n");
	exit(1);
}
