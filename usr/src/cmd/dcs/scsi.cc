//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma	ident	"@(#)scsi.cc 1.17	08/05/20 SMI"

#include <strings.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stropts.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <limits.h>
#include <sys/mhd.h>
#include <sys/clconf_int.h>
#include <sys/quorum_int.h>
#include <orb/infrastructure/orb_conf.h>
#include <orb/object/adapter.h>
#include <dc/libdcs/libdcs.h>
#include <sys/didio.h>
#include <sys/sysmacros.h>
#include <nslib/ns.h>
#include <h/repl_pxfs.h>
#include <errno.h>
#include <h/dc.h>
#include <h/cmm.h>
#include <cmm/cmm_ns.h>
#include <rgm/sczones.h>


#define	INIT_KEYS NODEID_MAX		// initial number of reservation keys
#define	INIT_RESVS NODEID_MAX		// initial number of disk reservations
#define	SCSI_CMD "/usr/cluster/lib/sc/scsi"


// print the contents of a scsi-3 key
static void
print_scsi3_key(const mhioc_resv_key_t &key)
{
	unsigned int i;
	uint64_t keyval = 0;

	for (i = 0; i < MHIOC_RESV_KEY_SIZE; i++)
		keyval = (keyval << 8) + key.key[i];
	(void) printf("0x%llx\n", keyval);
}

// forward declaration
static int do_scsi3_register(int fd, mhioc_register_t r);
static int do_scsi3_registerandignorekey(int fd,
    mhioc_registerandignorekey_t r);

static mhioc_resv_key_t
get_my_key(int fd, const mhioc_inkeys_t &keylist)
{
	unsigned int i;
	mhioc_register_t reg;

	reg.aptpl = B_TRUE;
	for (i = 0; i < keylist.li->listlen; i++) {
		reg.oldkey = keylist.li->list[i];
		reg.newkey = keylist.li->list[i];
		if (do_scsi3_register(fd, reg) == 0) {
			return (reg.newkey);
		}
	}

	// register failed, not yet registered?
	bzero(reg.oldkey.key, (size_t)MHIOC_RESV_KEY_SIZE);
	// XXX ugly, but works, for now
	(void) memcpy(reg.newkey.key, "XXXXXXXXXXXXXXXX",
	    (size_t)MHIOC_RESV_KEY_SIZE);
	if (do_scsi3_register(fd, reg) == 0) {
			return (reg.newkey);
		}

	(void) printf("Scsi: unable to register with device\n");
	exit(1);
}


// issue MHIOCSTATUS ioctl
static int
do_status(int fd)
{
	return (ioctl(fd, MHIOCSTATUS));

}

// issue MHIOCENABLEFAILFAST ioctl
static int
do_enfailfast(int fd, int interval)
{
	return (ioctl(fd, MHIOCENFAILFAST, &interval));
}

// issue MHIOCRELEASE ioctl
static int
do_scsi2_release(int fd)
{
	return (ioctl(fd, MHIOCRELEASE));
}

// issue MHIOCTKOWN ioctl
static int
do_scsi2_tkown(int fd)
{
	return (ioctl(fd, MHIOCTKOWN, 0));
}

// issue MHIOCGRP_REGISTERANDIGNOREKEY ioctl
static int
do_scsi3_registerandignorekey(int fd, mhioc_registerandignorekey_t r)
{
	return (ioctl(fd, MHIOCGRP_REGISTERANDIGNOREKEY, &r));
}


// issue MHIOCGRP_REGISTER ioctl
static int
do_scsi3_register(int fd, mhioc_register_t r)
{
	return (ioctl(fd, MHIOCGRP_REGISTER, &r));
}

// issue MHIOCGRP_RESERVE ioctl
static int
do_scsi3_reserve(int fd, mhioc_resv_desc_t r)
{
	return (ioctl(fd, MHIOCGRP_RESERVE, &r));
}

// issue MHIOCGRP_PREEMPTANDABORTE ioctl
static int
do_scsi3_preemptandabort(int fd, mhioc_preemptandabort_t r)
{
	return (ioctl(fd, MHIOCGRP_PREEMPTANDABORT, &r));
}

// issue MHIOCGRP_INKEYS ioctl
static int
do_scsi3_inkeys(int fd, mhioc_inkeys_t keylist)
{
	int i;

	// CXX do we really need 'i' here?
	if ((i = ioctl(fd, MHIOCGRP_INKEYS, &keylist)) == -1)
		return (i);

	// make sure our key list was big enough
	if (keylist.li->listlen > keylist.li->listsize) {
		if ((keylist.li->list = (mhioc_resv_key *)
		    realloc(keylist.li->list, keylist.li->listlen *
		    sizeof (mhioc_resv_key_t))) == NULL) {
			(void) printf("Memory allocation failure\n");
			exit(1);
		}
		keylist.li->listsize = keylist.li->listlen;
		i = ioctl(fd, MHIOCGRP_INKEYS, &keylist);

	}
	return (i);
}

// issue MHIOCGRP_INRESV ioctl
static int
do_scsi3_inresv(int fd, mhioc_inresvs_t reslist)
{
	int i;

	// do we really need 'i' here?
	if ((i = ioctl(fd, MHIOCGRP_INRESV, &reslist)) == -1)
		return (i);

	// make sure our reservation list was big enough
	if (reslist.li->listlen > reslist.li->listsize) {
		if ((reslist.li->list = (mhioc_resv_desc *)
		    realloc(reslist.li->list, reslist.li->listlen *
		    sizeof (mhioc_resv_desc_t))) == NULL) {
			(void) printf("Memory allocation failure\n");
			exit(1);
		}
		reslist.li->listsize = reslist.li->listlen;
		i = ioctl(fd, MHIOCGRP_INRESV, &reslist);
	}
	return (i);
}

// prints out usage message for debug code paths
static void
print_usage()
{
	// only provide usage for commands to inquire and clean
	(void) printf("\nUsage:\n"
	    "scsi -c [status | release | scrub | inkeys | inresv] "
	    "-d disk_path\n");
}

//
// Removes scsi-3 keys from the specified disk.  Returns -1 if unable to get the
// list of keys from the disk or there are no keys, -2 if unable to remove the
// keys, 0 otherwise.
//
static int
do_scrub(int fd, const mhioc_inkeys_t &keylist, mhioc_register_t reg,
    const mhioc_registerandignorekey_t &regign, mhioc_preemptandabort_t pre)
{
	unsigned int i;
	int ret = 0;

	if (keylist.li->listlen == 0) {
		(void) printf("There are no keys on the disk to remove\n");
		return (-1);
	}

	// print out keys
	(void) printf("Reservation keys currently on disk:\n");
	for (i = 0; i < keylist.li->listlen; i++)
		print_scsi3_key(keylist.li->list[i]);

	(void) printf("Attempting to remove all keys from the disk...\n");

	// make sure this node is registered with the disk
	if (do_scsi3_registerandignorekey(fd, regign) == -1) {
		(void) printf("MHIOCGRP_REGISTERANDIGNOREKEY failed, attempting"
		    " MHIOCGRP_REGISTER\n");

		if (do_scsi3_register(fd, reg) == -1) {
			// register failed, not yet registered?
			bzero(reg.oldkey.key, (size_t)MHIOC_RESV_KEY_SIZE);
			if (do_scsi3_register(fd, reg) == -1) {
				// reg failed again, registered w/diff key?
				for (i = 0; i < keylist.li->listlen; i++) {
					reg.oldkey = keylist.li->list[i];
					if (do_scsi3_register(fd, reg) == 0) {
						break;
					}
				}
			}
		}
	}

	// If APTPL flag is false, return immediately.
	if (reg.aptpl == B_FALSE)
		return (ret);

	// preempt everyone else's keys
	for (i = 0; i < keylist.li->listlen; i++) {
		if (memcmp((void *)reg.newkey.key,
		    (void *)keylist.li->list[i].key,
		    (size_t)MHIOC_RESV_KEY_SIZE) == 0)
			continue;
		pre.victim_key = keylist.li->list[i];
		if (do_scsi3_preemptandabort(fd, pre) == - 1) {
			(void) printf("Unable to remove key : ");
			print_scsi3_key(pre.victim_key);
			ret = -2;
		}
	}

	// remove my key
	reg.oldkey = reg.newkey;
	bzero(reg.newkey.key, (size_t)MHIOC_RESV_KEY_SIZE);
	(void) do_scsi3_register(fd, reg);

	return (ret);
}






main(int argc, char **argv)
{
	char *command = NULL;		// fencing command to execute
	int c;
	unsigned int i;

	char *zdisk = NULL;
	int zd = -1;
	mhioc_resv_key_t mykey;
	mhioc_inkeys_t keylist;
	mhioc_key_list_t klist;
	mhioc_register_t reg;
	mhioc_registerandignorekey_t regign;
	mhioc_inresvs_t reslist;
	mhioc_resv_desc_list_t rlist;
	mhioc_resv_desc_t res;
	mhioc_preemptandabort_t pre;
	mhioc_resv_key_t vickey;
	unsigned int j;
	bool scsi3_only = false;
	bool vickey_set = false;
	bool mykey_set = false;
	int ret = 0;

	if (sc_zonescheck() != 0)
		return (1);

	// get args
	while ((c = getopt(argc, argv, "c:d:v:k:")) != EOF) {
		switch (c) {
		case 'c' :
			command = optarg;
			break;
		case 'k' :
			bzero(mykey.key, (size_t)MHIOC_RESV_KEY_SIZE);
			(void) memcpy((char *)mykey.key, optarg,
			    (size_t)MHIOC_RESV_KEY_SIZE);
			mykey_set = true;
			break;
		case 'v' :
			bzero(vickey.key, (size_t)MHIOC_RESV_KEY_SIZE);
			(void) memcpy((char *)vickey.key, optarg,
			    (size_t)MHIOC_RESV_KEY_SIZE);
			vickey_set = true;
			break;
		case 'd' :
			zdisk = optarg;
			break;
		default :
			(void) printf("Illegal option '%c'\n", optopt);
			print_usage();
			exit(1);
		}
	}

	// check for required args
	if (command == NULL) {
		(void) printf("Scsi: command not specified\n");
		print_usage();
		exit(1);
	}
	if (zdisk == NULL) {
		(void) printf("Scsi: device not specified\n");
		print_usage();
		exit(1);
	}
	if ((strcmp(command, "preempt") == 0) && (!vickey_set)) {
		(void) printf("Scsi: must specify key to preempt\n");
		print_usage();
		exit(1);
	}
	if ((strcmp(command, "register") == 0) && (!mykey_set)) {
		(void) printf("Scsi: must specify key to register with\n");
		print_usage();
		exit(1);
	}

	// open device to send ioctls to
	if ((zd = open(zdisk, O_RDONLY|O_NDELAY)) == -1) {
		(void) printf("Could not open device '%s', "
		    "errno %d\n", zdisk, errno);
		exit(1);
	}

	// are we issuing a command that can only work on a scsi-3 device?
	if ((strcmp(command, "tkown") != 0) &&
	    (strcmp(command, "release") != 0) &&
	    (strcmp(command, "status") != 0) &&
	    (strcmp(command, "enfailfast") != 0) &&
	    (strcmp(command, "disfailfast") != 0))
		scsi3_only = true;

	// setup scsi3 variables if we're issuing a scsi-3 only command
	if (scsi3_only) {
		keylist.li = &klist;
		keylist.li->list = (mhioc_resv_key *)
		    malloc(INIT_KEYS * sizeof (mhioc_resv_key_t));
		keylist.li->listsize = INIT_KEYS;
		keylist.li->listlen = 0;
		// go ahead and get the keylist, since we'll probably need it
		if (do_scsi3_inkeys(zd, keylist) != 0)
			(void) printf("do_scsi3_inkeys failed\n");

		if ((strcmp(command, "scrub") == 0) &&
		    (keylist.li->listlen == 0)) {
			(void) printf("There are no keys on the disk to "
			    "remove\n");
			exit(0);
		}

		reslist.li = &rlist;
		reslist.li->list = (mhioc_resv_desc *)
		    malloc(INIT_RESVS * sizeof (mhioc_resv_desc_t));
		reslist.li->listsize = INIT_RESVS;
		reslist.li->listlen = 0;
		if ((keylist.li->list == NULL) || (reslist.li->list == NULL)) {
			(void) printf("Memory allocation failure\n");
			exit(1);
		}

		if (strcmp(command, "scrub") == 0)
			reg.oldkey = get_my_key(zd, keylist);
		else
			//lint -e644 mykey may not have been initialized
			reg.oldkey = mykey;
			//lint +e644
		reg.newkey = reg.oldkey;
		reg.aptpl = B_TRUE;
		regign.newkey = reg.oldkey;
		regign.aptpl = B_TRUE;

		if ((strcmp(command, "preempt") == 0) ||
		    (strcmp(command, "reserve") == 0) ||
		    (strcmp(command, "scrub") == 0)) {
			res.key = reg.oldkey;
			res.type = SCSI3_RESV_WRITEEXCLUSIVEREGISTRANTSONLY;
			res.scope = SCSI3_SCOPE_LOGICALUNIT;
			res.scope_specific_addr = 0;
			pre.resvdesc = res;
		}
	}

	if (strcmp(command, "scrub") == 0) {
		//lint -e645 keylist may not have been initialized
		//lint -e644 reg may not have been initialized
		if ((ret = do_scrub(zd, keylist, reg, regign, pre)) == 0)
			(void) printf("Scrubbing complete, use "
			    "'%s -c inkeys -d %s' "
			    "to verify success\n", SCSI_CMD, zdisk);
		// Try with APTPL flag set to false
		else if (ret == -2) {
			reg.aptpl = B_FALSE;
			regign.aptpl = B_FALSE;
			if (do_scrub(zd, keylist, reg, regign, pre) == 0)
				(void) printf("Powercycle the storage box "
				    "containing the disk %s, use "
				    "'%s -c inkeys -d %s' to verify "
				    "success\n", zdisk, SCSI_CMD, zdisk);
		}
		//lint +e644
		//lint +e645
	} else if (strcmp(command, "status") == 0) {
		(void) printf("status...%d\n", do_status(zd));
	} else if (strcmp(command, "enfailfast") == 0) {
		(void) printf("do_enfailfast returned %d\n",
		    do_enfailfast(zd, INT_MAX));
	} else if (strcmp(command, "disfailfast") == 0) {
		(void) printf("do_enfailfast returned %d\n",
		    do_enfailfast(zd, 0));
	} else if (strcmp(command, "release") == 0) {
		(void) printf("do_scsi2_release returned %d\n",
		    do_scsi2_release(zd));
	} else if (strcmp(command, "tkown") == 0) {
		(void) printf("do_scsi2_tkown returned %d \n",
		    do_scsi2_tkown(zd));
	} else if (strcmp(command, "unregister") == 0) {
		bzero(reg.newkey.key, (size_t)MHIOC_RESV_KEY_SIZE);
		if (do_scsi3_register(zd, reg) == -1) {
			// register failed, not yet registered?
			bzero(reg.oldkey.key, (size_t)MHIOC_RESV_KEY_SIZE);
			if (do_scsi3_register(zd, reg) == -1) {
				// reg failed again, registered w/diff key?
				for (j = 0; j < keylist.li->listlen; j++) {
					reg.oldkey = keylist.li->list[j];
					if (do_scsi3_register(zd, reg) == 0) {
						break;
					}
				}
			}
		}
	} else if (strcmp(command, "register") == 0) {
		if (do_scsi3_registerandignorekey(zd, regign) == -1) {
			(void) printf("MHIOCGRP_REGISTERANDIGNOREKEY failed, "
			    "attempting MHIOCGRP_REGISTER\n");
			if (do_scsi3_register(zd, reg) == -1) {
				// register failed, not yet registered?
				bzero(reg.oldkey.key,
				    (size_t)MHIOC_RESV_KEY_SIZE);
				if (do_scsi3_register(zd, reg) == -1) {
					//
					// reg failed again,
					// registered w/diff key
					//
					for (j = 0; j < keylist.li->listlen;
					    j++) {
						reg.oldkey =
						    keylist.li->list[j];
						if (do_scsi3_register(zd, reg)
						    == 0) {
							break;
						}
					}
				}
			}
		}
	} else if (strcmp(command, "reserve") == 0) {
		//lint -e644 res may not have been initialized
		(void) printf("do_scsi3_reserve returned %d\n",
		    do_scsi3_reserve(zd, res));
		//lint +e644
	} else if (strcmp(command, "inkeys") == 0) {
		// print out keys
		(void) printf("Reservation keys(%d):\n",
		    keylist.li->listlen);
		for (i = 0; i < keylist.li->listlen; i++)
			print_scsi3_key(keylist.li->list[i]);
	} else if (strcmp(command, "inresv") == 0) {
		//lint -e644 reslist may not have been initialized
		if (do_scsi3_inresv(zd, reslist) != 0)
		//lint +e644
			(void) printf("do_scsi-3_inresv failed\n");
		else {
			// print out reservations
			(void) printf("Reservations(%d):\n",
			    reslist.li->listlen);
			for (i = 0; i < reslist.li->listlen; i++) {
				print_scsi3_key(reslist.li->list[i].key);
				(void) printf("type ---> %d\n",
				    reslist.li->list[i].type);
			}
		}
	} else if (strcmp(command, "preempt") == 0) {
		//lint -e644  vickey (line 1047) may not have been initialized
		pre.victim_key = vickey;
		//lint +e644
		(void) printf("do_scsi3_preemptandabort returned %d\n",
		    do_scsi3_preemptandabort(zd, pre));
	} else {
		(void) printf("Scsi: illegal command '%s'\n", command);
		print_usage();
		exit(1);
	}

	// clean up
	if (zdisk != NULL)
		(void) close(zd);
	if (scsi3_only) {
		free(keylist.li->list);
		free(reslist.li->list);
	}
	exit(0);
}
