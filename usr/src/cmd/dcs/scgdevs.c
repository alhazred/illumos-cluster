/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scgdevs.c	1.71	09/01/29 SMI"

#include <sys/clconf.h>
#include <sys/cladm_int.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <sys/vfstab.h>
#include <sys/param.h>
#include <sys/cladm.h>
#include <sys/mnttab.h>
#include <devid.h>
#include <sys/didio.h>
#include <libdid.h>
#include <sys/types.h>
#include <fcntl.h>
#include <dirent.h>
#include <libintl.h>
#include <locale.h>
#include <libgen.h>
#include <sys/stat.h>
#include <sys/ddi.h>
#include <dc/libdcs/libdcs.h>
#include <sys/systeminfo.h>
#include <sys/clconf_int.h>
#include <stdarg.h>
#include <wait.h>
#include <sys/stropts.h>
#include <sys/sc_syslog_msg.h>
#include <errno.h>
#include <signal.h>
#include <sys/cl_auth.h>
#include <sys/cl_cmd_event.h>
#include <sys/sol_version.h>
#include <sys/cl_efi.h>
#ifdef SC_EFI_SUPPORT
#include <ftw.h>
#include <sys/dkio.h>
#endif /* SC_EFI_SUPPORT */
#include <rgm/sczones.h>
#include <meta.h>

#define	GETROOT	1
#define	GETUSR	2
#define	DIDPROG "/usr/cluster/bin/scdidadm -X"
#define	LOG_REDIRECT "2>&1 | { while read xx;do echo $xx; echo $xx " \
		"| /usr/cluster/lib/sc/scds_syslog -p daemon.notice " \
		"-t Cluster.devices.did -m \"`cat -`\" 2>&1; done }"
#define	RESERVE_CMD	"/usr/cluster/lib/sc/run_reserve -c node_join -x"
#define	GDEVS_NOTROOT -1
#define	GDEVS_NOTCLUSTER -2
#define	GDEVS_NOTMOUNTED -3
#define	GDEVS_NOTMOUNTED_GLOBAL -4
#define	SERVICE_FOUND	1
#define	NODEID_FOUND	2
#define	GMIN_FOUND	4
#define	OTHER_ROOTDEV	8

#define	DEV_GLOBAL	"/dev/global/"
#define	S2_STRLEN	2	/* strlen of "s2" */
#define	DEV_GLOBAL_STRLEN	12	/* strlen of "/dev/global/" */

#define	IDENTICAL(A, B)	(A.st_dev == B.st_dev && A.st_ino == B.st_ino)
#define	ISDIR(A)	((A.st_mode & S_IFMT) == S_IFDIR)
#define	ISDEV(A)	((A.st_mode & S_IFMT) == S_IFCHR || \
			(A.st_mode & S_IFMT) == S_IFBLK || \
			(A.st_mode & S_IFMT) == S_IFIFO)

void usage();
nodeid_t getnodeid();
int safemkdir(char *);
int make_directories();
int basic_links();
int move_md();
int reconfig_did();
int upload_did_config();
int reconfig_did_driver();
int chkfiles(char *, char **, struct stat *, struct stat *);
int copyspecial(char *, char *, struct stat *, struct stat *);
int cpymve(char *, char *, struct stat *);
int move(char *, char *, struct stat *);
int rcopy(char *, char *, struct stat *);
int copydir(char *, char *, struct stat *);
int deletedir(char *);
int copy_physical_devices();
int populate_logical_devices();
int validate_mode();
int validate_mounts();
char *get_rootdev(int);
char *get_hostname();
int get_service_by_diskname(char *, char **);
int find_device_service(char *, char *, char *, int, char **);
int register_service(int, char *, int, char **, char *, char *, char *);
int remove_remote_nodes(char *);
int set_local_only(char *);
int set_disk_prop(char *);
int register_disk(char *, char *, char *, int, char **);
int register_tape(char *, int, char **);
int register_devices(int, char **, int, char **);
int mysystem(const char *);
static void log_message(int priority, int errnum, const char *msg, ...);
static void handle_sigint(int);

/* XXX - Do we need to copy ACLs? */
nodeid_t nodenum;
char progname[MAXNAMELEN];
char gdevprefix[MAXPATHLEN];
int op_no_remote = 0;

static int exit_code;

/*
 * usage()
 * Print the command usage message.
 */
void
usage()
{
	(void) fprintf(stdout, gettext("usage: %s\n"), progname);
}

#ifdef SC_EFI_SUPPORT

/*ARGSUSED*/
static int
efi_disk_path(
	const char		*path,
	const struct stat	*statp,
	int			type)
{
	int error;
	char mpath[MAXNAMELEN];

	/*
	 * Ignore non-files (e.g., directories)
	 */
	if (type != FTW_F) {
		return (0);
	}

	/*
	 * SPARC and i386:
	 * For an EFI disk, there is no more a cXtYdZs7 name in /dev,
	 * there is cXtYdZ instead. So, for the DID namespace,
	 * we simply create a hard link from dS to dSs7 as, the
	 * EFI 'whole disk' slice uses the same minor as slice 7
	 * of a VTOC disk. Here we create dS and unlink dSs7
	 * if disk is EFI.
	 */
	if (strcmp(&path[strlen(path) - 2], "s7") == 0) {
		(void) strcpy(mpath, path);
		mpath[strlen(mpath) - 2] = '\0';

		/*
		 * The integration of Devname project PSARC/2003/246 introduced
		 * a dedicated file system for /dev namespace. This file system
		 * disallows hard links but accepts symbolic links.
		 */
#if SOL_VERSION >= __s11
		error = symlink(path, mpath);
#else
		error = link(path, mpath);
#endif
		/* link is interruptible */
		/*lint -e746 */
		while (error != 0 && errno == EINTR) {
			error = link(path, mpath);
		}
		/*lint +e746 */
		if (error != 0 && errno != EEXIST) {
			perror("efi_disk_path:link");
			return (1);
		}
		/* Link dS to dSs7 is now created */
	}
	return (0);
}

static int
add_logical_efi_devices(char *dir)
{
	if (ftw(dir, efi_disk_path, 1)) {
		perror("add_logical_efi_devices:ftw");
		return (1);
	}
	return (0);
}

#endif /* SC_EFI_SUPPORT */

int
mysystem(const char *command)
{
	int ret;
	char prog[MAXPATHLEN];

	if ((strlen(LOG_REDIRECT) + strlen(command)) > sizeof (prog)) {
		log_message(LOG_ERR, 0,
		    "%s: Buffer too small for: %s %s",
		    progname, command, LOG_REDIRECT);
		return (1);
	}
	(void) sprintf(prog, "%s %s", command, LOG_REDIRECT);
	(void) sighold(SIGINT);
	ret = system(prog);
	if (ret == -1) {
		/*lint -e746 */
		log_message(LOG_ERR, errno, "%s: Cannot invoke %s ",
			progname, prog);
		/*lint +e746 */
	} else if (ret != 0) {
		log_message(LOG_ERR, errno, "%s: %s Error code %d ",
			progname, prog, ret);
	}
	(void) sigrelse(SIGINT);

	return (ret);
}

/*
 * getnodeid()
 * Get the invoking host's node id in the cluster.
 */
nodeid_t
getnodeid()
{
	nodeid_t nodeid;

	if (cladm(CL_CONFIG, CL_NODEID, &nodeid) != 0) {
		log_message(LOG_ERR, 0, "%s: Cannot determine nodeid.",
			progname);
		return (NULL);
	}

	return (nodeid);
}

/*
 * safemkdir()
 */
int
safemkdir(char *dir_name)
{
	struct stat statbuf;
	mode_t mode = (S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);

	if (lstat(dir_name, &statbuf) < 0) {
		/* Path doesn't exist. Create the directory. */
		if (mkdir(dir_name, mode)) {
			/*
			 * Race conditions with multiple scgdevs running can
			 * result in the directory already existing at
			 * this point.  Only worry about other errors.
			 */
			if (errno != EEXIST) {
				log_message(LOG_ERR, errno,
				    "%s: Cannot make directory %s: ",
				    progname, dir_name);
				return (1);
			}
		}
	} else {
		if (!S_ISDIR(statbuf.st_mode)) {
			/*
			 * If the path exists, but is not a directory,
			 * remove it.
			 */
			if (remove(dir_name)) {
				/*
				 * Potential race.
				 * See comment above.
				 */
				if (errno != ENOENT) {
					log_message(LOG_ERR, errno,
					    "%s: Cannot remove %s: ",
					    progname, dir_name);
					return (1);
				}
			}
			if (mkdir(dir_name, mode)) {
				/*
				 * Potential race.
				 * See comment above.
				 */
				if (errno != EEXIST) {
					log_message(LOG_ERR, errno,
					    "%s: Cannot make directory %s: ",
					    progname, dir_name);
					return (1);
				}
			}
		} else {
			/* It is a directory. Return success. */
			return (0);
		}
	}
	return (0);
}

/*
 * make_directories()
 * Make all the directories we'll need for the global namespace.
 */
int
make_directories()
{
	char dir_name[MAXPATHLEN];
	uint_t strsize = sizeof (dir_name);

	if ((strlen(gdevprefix) + 4 + 1) > strsize) {
		log_message(LOG_ERR, 0,
		    "%s: Buffer too small for: %s%s",
		    progname, gdevprefix, "dev");
		return (1);
	}
	(void) sprintf(dir_name, "%sdev", gdevprefix);
	if (safemkdir(dir_name))
		return (1);

	if ((strlen(gdevprefix) + 8 + 1) > strsize) {
		log_message(LOG_ERR, 0,
		    "%s: Buffer too small for: %s%s",
		    progname, gdevprefix, "devices");
		return (1);
	}
	(void) sprintf(dir_name, "%sdevices", gdevprefix);
	if (safemkdir(dir_name))
		return (1);

	if ((strlen(gdevprefix) + 7 + 1)  > strsize) {
		log_message(LOG_ERR, 0,
		    "%s: Buffer too small for: %s%s",
		    progname, gdevprefix, "dev/md");
		return (1);
	}
	(void) sprintf(dir_name, "%sdev/md", gdevprefix);
	if (safemkdir(dir_name))
		return (1);

	if ((strlen(gdevprefix) + 14 + 1) > strsize) {
		log_message(LOG_ERR, 0,
		    "%s: Buffer too small for: %s%s",
		    progname, gdevprefix, "dev/md/shared");
		return (1);
	}
	(void) sprintf(dir_name, "%sdev/md/shared", gdevprefix);
	if (safemkdir(dir_name))
		return (1);

	if ((strlen(gdevprefix) + 8 + 1) > strsize) {
		log_message(LOG_ERR, 0,
		    "%s: Buffer too small for: %s%s",
		    progname, gdevprefix, "dev/did");
		return (1);
	}
	(void) sprintf(dir_name, "%sdev/did", gdevprefix);
	if (safemkdir(dir_name))
		return (1);

	return (0);
}

/*
 * basic_links()
 * Build the basic links necessary for the global namespace.
 */
int
basic_links()
{
	char linkname[MAXPATHLEN];
	char pathname[MAXPATHLEN];
	struct stat statbuf;

	if ((strlen(gdevprefix) + 11 + 1) >
	    sizeof (linkname)) {
		log_message(LOG_ERR, 0,
		    "%s: Buffer too small for: %s%s",
		    progname, gdevprefix, "dev/global");
		return (1);
	}
	(void) sprintf(linkname, "%sdev/global", gdevprefix);
	if ((lstat(linkname, &statbuf) < 0) ||
	    (!(statbuf.st_mode & S_IFLNK))) {
		if ((strlen(gdevprefix) + 8 + 1) >
		    sizeof (pathname)) {
			log_message(LOG_ERR, 0,
			    "%s: Buffer too small for : %s%s",
			    progname, gdevprefix, "dev/did");
			return (1);
		}
		(void) sprintf(pathname, "%sdev/did", gdevprefix);
		if (symlink(pathname, linkname)) {
			/*
			 * Race conditions with multiple scgdevs running can
			 * result in the link already existing at
			 * this point.  Only worry about other errors.
			 */
			if (errno != EEXIST) {
				log_message(LOG_ERR, errno,
				    "%s: Cannot symlink %s, %s: ", progname,
				    pathname, linkname);
				return (1);
			}
		}
	}

	if (lstat("/dev/global", &statbuf) == 0) {
		if (remove("/dev/global")) {
			/*
			 * Potential race.
			 * See comment above.
			 */
			if (errno != ENOENT) {
				log_message(LOG_ERR, errno,
				    "%s: Cannot remove %s: ",
				    progname, "/dev/global");
				return (1);
			}
		}
	}

	if ((strlen(gdevprefix) + 11 + 1) >
	    sizeof (pathname)) {
		log_message(LOG_ERR, 0,
		    "%s: Buffer too small for: %s%s",
		    progname, gdevprefix, "dev/global");
		return (1);
	}
	(void) sprintf(pathname, "%sdev/global", gdevprefix);
	if (symlink(pathname, "/dev/global")) {
		/*
		 * Potential race.
		 * See comment above.
		 */
		if (errno != EEXIST) {
			log_message(LOG_ERR, errno,
			    "%s: Cannot symlink %s, %s: ", progname,
			    pathname, "/dev/global");
			return (1);
		}
	}

	return (0);
}

/*
 * move_md()
 * Move any pre-cluster md configuration to the global space.
 */
int
move_md()
{
	struct stat statbuf, targetbuf;
	char md_directory[MAXPATHLEN];

	if ((strlen(gdevprefix) + 14 + 1) >
	    sizeof (md_directory)) {
		log_message(LOG_ERR, 0,
		    "%s: Buffer too small for: %s%s",
		    progname, gdevprefix, "dev/md/shared");
		return (1);
	}
	(void) sprintf(md_directory, "%sdev/md/shared", gdevprefix);

	if (lstat("/dev/md/shared", &statbuf) == 0) {
		if (S_ISLNK(statbuf.st_mode)) {
			return (0);
		}
		if (S_ISDIR(statbuf.st_mode)) {
			if (stat(md_directory, &targetbuf) < 0) {
				if (mkdir(md_directory,
				    (statbuf.st_mode)) < 0) {
					/*
					 * Race conditions with multiple
					 * scgdevs running can
					 * result in the directory already
					 * existing at this point.
					 * Only worry about other errors.
					 */
					if (errno != EEXIST) {
						log_message(LOG_ERR, errno,
						    "%s: Cannot mkdir %s: ",
						    progname, md_directory);
						return (1);
					}
				}
			}
			if (deletedir("/dev/md/shared")) {
				log_message(LOG_ERR, 0,
				    "%s: Cannot delete %s",
				    progname, "/dev/md/shared");
				return (1);
			}
		}
	}
	if (safemkdir("/dev/md")) {
		log_message(LOG_ERR, 0,
		    "%s: Cannot make %s directory.",
		    progname, "/dev/md");
		return (1);
	}
	if (symlink(md_directory, "/dev/md/shared")) {
		/*
		 * Potential race.
		 * See comment above.
		 */
		if (errno != EEXIST) {
			log_message(LOG_ERR, errno,
			    "%s: Cannot symlink %s, %s: ",
			    progname, md_directory, "/dev/md/shared");
			return (1);
		}
	}

	return (0);
}

/*
 * reconfig_did()
 * Run the did reconfiguration command to discover new devices.
 */
int
reconfig_did()
{
	char prog[MAXPATHLEN];

	if ((strlen(DIDPROG) + 3 + 1) > sizeof (prog)) {
		log_message(LOG_ERR, 0,
		    "%s: Buffer too small for: %s %s",
		    progname, DIDPROG, "-r");
		return (1);
	}
	(void) sprintf(prog, "%s -r", DIDPROG);

	if (mysystem(prog)) {
		return (1);
	}

	return (0);
}

/*
 * upload_did_config()
 * Upload existing did config into the kernel. This is necessary
 * before we ask the Solaris to reconfig the devices.
 */
int
upload_did_config()
{
	char prog[MAXPATHLEN];

	if ((strlen(DIDPROG) + 4 + 1) > sizeof (prog)) {
		log_message(LOG_ERR, 0,
		    "%s: Buffer too small for: %s %s",
		    progname, DIDPROG, "-ui");
		return (1);
	}
	(void) snprintf(prog, MAXPATHLEN, "%s -uij", DIDPROG);
	if (mysystem(prog)) {
		return (1);
	}
	return (0);
}

/*
 * reconfig_did_driver()
 * Populate Solaris namespace with new DID devices
 */
int
reconfig_did_driver()
{
	if (mysystem("/usr/sbin/devfsadm -i did")) {
		return (1);
	}
#ifdef SC_EFI_SUPPORT
	if (add_logical_efi_devices("/dev/did/dsk")) {
		return (1);
	}
	if (add_logical_efi_devices("/dev/did/rdsk")) {
		return (1);
	}
#endif /* SC_EFI_SUPPORT */
	return (0);
}

/*
 * chkfiles()
 * This function does two things:
 * 	1) If *to is a pointer to a directory, modify it to point at
 *	   the basename(source) filename under that directory. An
 *	   attempt to mknod on the existing directory is bound to fail.
 *	2) Checks that we're not trying to copy to identical files.
 *	3) A return value of zero indicates the target file exists and the
 *	   st_rdev fields should be compared by the caller.
 *	   A return value of 1 indicates an error occurred.
 *	   A return value of 2 indicates target is a directory which does
 *	   not yet exist, hence no st_rdev check should be performed.
 */
int
chkfiles(char *source, char **to, struct stat *source_stat,
    struct stat *to_stat)
{
	char *buf = (char *)NULL;
	char *source_copy;

	if (source == NULL) {
		return (1);
	}

	if (ISDIR((*to_stat))) {
		/*
		 * Make a copy of source so we can use the basename(3C)
		 * function, which threatens to modify its arguments, on
		 * the string.
		 */
		if ((source_copy = strdup(source)) == NULL) {
			log_message(LOG_ERR, errno,
			    "%s: Cannot allocate memory: ", progname);
			return (1);
		}
		if ((buf = (char *)malloc(strlen(*to) +
		    strlen(basename(source_copy)) + 4)) == NULL) {
			log_message(LOG_ERR, errno,
			    "%s: Memory allocation failed: ", progname);
			free(source_copy);
			return (1);
		}
		(void) sprintf(buf, "%s/%s", *to, basename(source_copy));
		*to = buf;
		if (stat(*to, to_stat) != 0) {
			free(source_copy);
			return (2);
		}
		free(source_copy);
	}

	/*
	 * If the file names for source and target are
	 * the same, it is an error.
	 */
	if (IDENTICAL((*source_stat), (*to_stat))) {
		if (buf != NULL)
			free(buf);
		log_message(LOG_ERR, 0,
		    "%s: %s and %s are identical.",
		    progname, source, *to);
		return (1);
	}
	return (0);
}

int
copyspecial(char *source, char *target, struct stat *source_stat,
    struct stat *org_targ_stat)
{
	struct stat target_stat = *org_targ_stat;

	switch (chkfiles(source, &target, source_stat, &target_stat)) {
		case 0:
			if (source_stat->st_rdev == target_stat.st_rdev) {
				return (0);
			} else {
				if (remove(target)) {
					/*
					 * Race conditions with multiple
					 * scgdevs running can
					 * result in the target already
					 * removed at this point.
					 * Only worry about other errors.
					 */
					if (errno != ENOENT) {
						log_message(LOG_ERR, errno,
						    "%s: Cannot remove %s: ",
						    progname, target);
						return (1);
					}
				}
			}
			break;

		case 1:
			return (1);

		case 2:
		default:
			break;
	}

	if (mknod(target, source_stat->st_mode, source_stat->st_rdev) != 0) {
		if (errno != EEXIST) {
			/*
			 * Race conditions with multiple scgdevs running can
			 * result in the special file already existing at
			 * this point.  Only worry about other errors.
			 */
			log_message(LOG_ERR, errno,
			    "%s: cannot create special file %s: ", progname,
			    target);
			return (1);
		}
	}
	return (0);
}

int
cpymve(char *source, char *target, struct stat *target_stat)
{
	struct stat source_stat;

	if (stat(source, &source_stat) < 0) {
		/*
		 * Due to race conditions resulting from mulitple scgdevs
		 * running simultaneously on the same node, it is possible
		 * this source object has already been moved, hence
		 * doesn't exist. In that case simply return success.
		 */
		if (errno != ENOENT) {
			log_message(LOG_ERR, errno,
			    "%s: Cannot stat %s: ", progname, source);
			return (1);
		}
		return (0);
	}
	if (ISDIR(source_stat))
		return (copydir(source, target, target_stat));
	else if (ISDEV(source_stat))
		return (copyspecial(source, target, &source_stat, target_stat));
	else {
		return (0);
	}
}

int
move(char *source, char *target, struct stat *target_stat)
{
	size_t last;

	/* While source has trailing "/", remove them. */
	while (((last = strlen(source)) > 1) && (source[last-1] == '/'))
		source[last-1] = NULL;

	return (cpymve(source, target, target_stat));
}

int
rcopy(char *from, char *to, struct stat *to_stat)
{
	struct dirent *dp;
	int errs = 0;
	char fromname[MAXPATHLEN + 1];
	DIR *fold = opendir(from);

	if (fold == NULL) {
		/*
		 * Due to race conditions resulting from mulitple scgdevs
		 * running simultaneously on the same node, it is possible
		 * the "from" directory no longer exists, since this
		 * subroutine can be part of the move_md() call stack.
		 * If errno == ENOENT, just return 0.
		 */
		if (errno != ENOENT) {
			log_message(LOG_ERR, errno,
			    "%s: Cannot open directory %s: ", progname, from);
			return (1);
		}
		return (0);
	}
	for (;;) {
		dp = readdir(fold);
		if (dp == NULL) {
			(void) closedir(fold);
			return (errs);
		}
		if (dp->d_ino == 0)
			continue;
		if ((strcmp(dp->d_name, ".") == 0) ||
		    (strcmp(dp->d_name, "..") == 0))
			continue;
		if (strlen(from)+1+strlen(dp->d_name) >=
		    sizeof (fromname) - 1) {
			log_message(LOG_ERR, 0,
			    "%s : %s/%s: Name too long",
			    progname, from, dp->d_name);
			errs++;
			continue;
		}
		(void) sprintf(fromname, "%s/%s", from, dp->d_name);
		errs += move(fromname, to, to_stat);
	}
}

int
copydir(char *source, char *target, struct stat *target_stat)
{

	if (!(ISDIR((*target_stat)))) {
		log_message(LOG_ERR, 0,
		    "%s: %s is not a directory. Cannot copy.",
		    progname, target);
		return (1);
	}

	return (rcopy(source, target, target_stat));
}

int
deletedir(char *dir_name)
{
	struct stat dir_stat;
	DIR *fdir;
	struct dirent *dp;
	int errs = 0;
	char filename[MAXPATHLEN + 1];

	if (stat(dir_name, &dir_stat) < 0) {
		/*
		 * Due to race conditions resulting from mulitple scgdevs
		 * running simultaneously on the same node, it is possible
		 * this directory has already been deleted hence
		 * doesn't exist. In that case simply return success.
		 */
		if (errno != ENOENT) {
			log_message(LOG_ERR, errno,
			    "%s: cannot stat %s: ", progname, dir_name);
			return (1);
		}
		return (0);
	}

	if (ISDIR(dir_stat)) {
		fdir = opendir(dir_name);
		if (fdir == 0) {
			/*
			 * Potential race.
			 * See comment above.
			 */
			if (errno != ENOENT) {
				log_message(LOG_ERR, errno,
				    "%s cannot open %s: ", progname, dir_name);
				errs++;
				return (1);
			}
			return (0);
		}

		for (;;) {
			dp = readdir(fdir);
			if (dp == 0) {
				(void) closedir(fdir);
				if (rmdir(dir_name)) {
					/*
					 * Potential race.
					 * See comment above.
					 */
					if (errno != ENOENT) {
						log_message(LOG_ERR, errno,
						    "%s: Could not remove "
						    "directory %s",
						    progname, dir_name);
						errs++;
					}
				}
				return (errs);
			}
			if (dp->d_ino == 0)
				continue;
			if ((strcmp(dp->d_name, ".") == 0) ||
			    (strcmp(dp->d_name, "..") == 0))
				continue;
			if (strlen(dir_name)+1+strlen(dp->d_name) >=
			    sizeof (filename) - 1) {
				log_message(LOG_ERR, 0,
				    "%s : %s/%s: Name too long",
				    progname, dir_name, dp->d_name);
				errs++;
				continue;
			}
			(void) sprintf(filename, "%s/%s", dir_name, dp->d_name);
				errs += deletedir(filename);
		}
	} else if (unlink(dir_name) < 0) {
		/*
		 * Potential race.
		 * See comment above.
		 */
		if (errno != ENOENT) {
			log_message(LOG_ERR, errno, "%s: cannot unlink %s: ",
			    progname, dir_name);
			errs++;
			return (1);
		}
	}
	return (0);
}

/*
 * copy_physical_devices()
 * Populate the physical device space in /global
 */
int
copy_physical_devices()
{
	char target[MAXPATHLEN + 1];
	struct stat source_stat, target_stat;
	char *source = "/devices/pseudo";

	if ((strlen(gdevprefix) + 15 + 1) > sizeof (target)) {
		log_message(LOG_ERR, 0,
		    "%s: Buffer too small for: %s%s",
		    progname, gdevprefix, "devices/pseudo");
		return (1);
	}
	(void) sprintf(target, "%sdevices/pseudo", gdevprefix);

	if (stat(source, &source_stat) < 0) {
		log_message(LOG_ERR, errno,
		    "%s: Cannot stat %s: ", progname, source);
		return (1);
	}
	if (stat(target, &target_stat) < 0) {
		if (mkdir(target, (source_stat.st_mode)) < 0) {
			/*
			 * Race conditions resulting from mulitple scgdevs
			 * running simultaneously on the same node can
			 * result in this directory already existing at this
			 * point. Only return an error if errno != EEXIST
			 */
			if (errno != EEXIST) {
				log_message(LOG_ERR, errno,
				    "%s: Cannot mkdir %s: ", progname, target);
				return (1);
			}
		}
		/*
		 * Need to re-stat here to get a valid target_stat struct.
		 */
		if (stat(target, &target_stat) < 0) {
			log_message(LOG_ERR, errno,
			    "%s: Cannot stat  %s: ", progname, target);
			return (1);
		}
	}
	return (copydir(source, target, &target_stat));
}

/*
 * populate_logical_devices()
 * Populate the logical device space in /global
 */
int
populate_logical_devices()
{
	char command[MAXPATHLEN];

	if ((22 + strlen(gdevprefix) + 1) > sizeof (command)) {
		log_message(LOG_ERR, 0,
		    "%s: Buffer too small for: %s %s",
		    progname, "/usr/sbin/devfsadm -n -y -r", gdevprefix);
		return (1);
	}
#if SOL_VERSION >= __s10
	(void) sprintf(command, "/usr/sbin/devfsadm -n -r %s", gdevprefix);
#else
	(void) sprintf(command, "/usr/sbin/devfsadm -n -y -r %s", gdevprefix);
#endif
	if (mysystem(command)) {
		return (1);
	}
#ifdef SC_EFI_SUPPORT
	(void) sprintf(command, "%sdev/did/dsk", gdevprefix);
	if (add_logical_efi_devices(command)) {
		return (1);
	}
	(void) sprintf(command, "%sdev/did/rdsk", gdevprefix);
	if (add_logical_efi_devices(command)) {
		return (1);
	}
#endif /* SC_EFI_SUPPORT */
	return (0);
}

/*
 * validate_mode()
 * Check that we're running as root in clustered mode.
 */
int
validate_mode()
{
	int		bootflags;

	/* Must be root */
	if (getuid() != 0) {
		log_message(LOG_ERR, 0,
		    "%s: You must be root.", progname);
		(void) printf(gettext("%s: You must be root.\n"), progname);
		return (GDEVS_NOTROOT);
	}

	/* Make sure we are a cluster member */
	if ((cladm(CL_INITIALIZE, CL_GET_BOOTFLAG, &bootflags) != 0) ||
		!(bootflags & CLUSTER_BOOTED)) {
		log_message(LOG_ERR, 0,
		    "%s: Not in cluster mode. Exiting...", progname);
		(void) printf(
		    gettext("%s: Not in cluster mode. Exiting...\n"), progname);
		return (GDEVS_NOTCLUSTER);
	}

	return (0);
}

/*
 * Check that the /global/.devices/node@ partition is mounted.
 * Check that the /global/.devices/node@ partition has the global opt set.
 */
int
validate_mounts()
{
	struct mnttab	mp;
	FILE		*fp;
	char		mountpoint[MAXPATHLEN];
	int		mounted = 0;
	int		global_opt = 0;
	int		global_any = 0;
	uint_t		len;
	int		ret;

	/* Make sure /global/.devices/node@N filesystem is mounted */
	fp = fopen("/etc/mnttab", "r");
	if (fp == NULL) {
		log_message(LOG_ERR, errno,
		    "%s: Cannot open %s to determine devices mount point.",
		    progname, "/etc/mnttab");
		return (GDEVS_NOTMOUNTED);
	}

	/* Strip trailing "/" off of gdevprefix. */
	(void) strcpy(mountpoint, gdevprefix);
	len = strlen(mountpoint);
	if ((len > 0) && (mountpoint[len - 1] == '/'))
		mountpoint[len - 1] = '\0';

	while ((ret = getmntent(fp, &mp)) != -1) {
		if (ret != 0) {
			log_message(LOG_ERR, errno,
			    "%s: Cannot read mnttab entry.", progname);
			continue;
		}
		if (mp.mnt_mountp == NULL)
			continue;
		if (strcmp(mp.mnt_mountp, mountpoint) == 0) {
			mounted = 1;
			if (strstr(mp.mnt_mntopts, "global") != NULL)
				global_opt = 1;
		} else {
			/*
			 * It's not the global devices filesystem but check to
			 * see if it's global anyway. Remotely invoked
			 * scgdevs can use this info to infer whether
			 * not finding global devices in /etc/mnttab
			 * is really a problem or not. See bugId 4475987
			 */
			if (strstr(mp.mnt_mntopts, "global") != NULL)
				global_any++;
		}
	}
	(void) fclose(fp);
	if (mounted != 1) {
		if (!op_no_remote ||
		    (op_no_remote && global_any)) {
			log_message(LOG_ERR, 0,
			    "%s: Filesystem %s is not available in %s.",
			    progname, mountpoint, "/etc/mnttab");
		}
		return (GDEVS_NOTMOUNTED);
	}
	if (global_opt != 1) {
		log_message(LOG_ERR, 0,
		    "%s: Filesystem %s is not "
		    "mounted with the \"global\" mount option.",
		    progname, mountpoint);
		return (GDEVS_NOTMOUNTED_GLOBAL);
	}
	return (0);
}

/*
 * get_rootdev()
 * Get the root/usr device's DID name for this node.
 */
char *
get_rootdev(int type)
{
	FILE *fp;
	struct vfstab *vp_out = (struct vfstab *)malloc(sizeof (struct vfstab));
	struct vfstab *vp_in = (struct vfstab *)malloc(sizeof (struct vfstab));
	char *devname, *fullname;
	did_device_list_t *did_struct;
	char *didpath;
	char *didname;
	char dev_type[10];
	char *slicep;

	/* Determine the type, so that we can print out the correct error. */
	if (type == GETROOT)
		(void) strcpy(dev_type, "root");
	else if (type == GETUSR)
		(void) strcpy(dev_type, "usr");
	else
		(void) strcpy(dev_type, "unknown");

	didname = (char *)malloc(MAXNAMELEN);
	if (didname == NULL) {
		log_message(LOG_ERR, errno,
		    "%s: Cannot allocate memory: ", progname);
		return (NULL);
	}

	fp = fopen("/etc/vfstab", "r");

	bzero(vp_in, sizeof (*vp_in));
	vp_in->vfs_mountp = (char *)malloc(sizeof (char) * 3);
	if (vp_in->vfs_mountp == NULL) {
		log_message(LOG_ERR, errno,
		    "%s: Cannot allocate memory: ", progname);
		return (NULL);
	}
	if (type == GETROOT)
		(void) strcpy(vp_in->vfs_mountp, "/");
	else if (type == GETUSR)
		(void) strcpy(vp_in->vfs_mountp, "/usr");
	else {
		log_message(LOG_ERR, 0,
		    "%s: Cannot determine %s type to query: %i",
		    progname, dev_type, type);
		return (NULL);
	}


	if (getvfsany(fp, vp_out, vp_in)) {
		/*
		 * None found. We may not have a device defined for
		 * this partition.
		 */
		return (NULL);
	}

	fullname = (char *)malloc(strlen(vp_out->vfs_special) + 1);
	if (fullname == NULL) {
		log_message(LOG_ERR, errno,
		    "%s: Cannot allocate memory: ", progname);
		return (NULL);
	}
	if (vp_out->vfs_special == NULL) {
		log_message(LOG_ERR, errno,
		"%s: Cannot determine %s device name.", progname, dev_type);
		return (NULL);
	}
	(void) strcpy(fullname, vp_out->vfs_special);

	/* Point to the string after the last / */
	if (fullname == NULL) {
		log_message(LOG_ERR, 0,
		    "%s: Cannot determine %s device name.", progname, dev_type);
		return (NULL);
	}
	devname = strrchr(fullname, '/');
	devname++;

	/* Strip out the slice information. */
	if ((slicep = strrchr(devname, 's')) != NULL)
		*slicep = NULL;
	else
		return (NULL);

	did_struct = map_to_did_device(devname, NULL);
	if (did_struct == NULL) {
		/* Can't determine the DID name. It might be an md device */
		return (NULL);
	}

	didpath = did_get_did_path(did_struct);
	if (didpath == NULL) {
		log_message(LOG_ERR, 0,
		    "%s: Could not get DID path for %s", progname, devname);
		return (NULL);
	}
	(void) strcpy(didname, didpath);
	free(didpath);

	return (didname);
}

char *
get_hostname()
{
	char *hostname = (char *)malloc(sizeof (char) * MAXNAMELEN);

	if (gethostname(hostname, MAXNAMELEN - 1)) {
		log_message(LOG_ERR, errno,
		    "%s: Cannot get hostname: ", progname);
		return (NULL);
	}

	return (hostname);
}

/*
 * This function takes a dsk/dx name as argument, checks if there is a service
 * using the dev_t for /dev/global/dev/dxs2.
 * Return values:
 * 		2 in case of a newly found service name. The function stores the
 *			newly found service name in variable other_service_name.
 * 		1 in case of finding the default service name.
 *		0 if nothing is found.
 *		-1 if an error has occurred.
 */
int
get_service_by_diskname(char *disk_name, char **other_service_name)
{
	struct stat file_stat;
	char minor_name[MAXPATHLEN];
	major_t dev_maj;
	minor_t dev_min;

	if ((DEV_GLOBAL_STRLEN + strlen(disk_name) + S2_STRLEN + 1) >
	    sizeof (minor_name)) {
		log_message(LOG_ERR, 0, "%s: Buffer too small "
		    "for: %s%s%s", progname, DEV_GLOBAL,
		    disk_name, "s2");
		return (-1);
	}

	(void) sprintf(minor_name, "%s%s%s", DEV_GLOBAL,
	    disk_name, "s2");

	if (stat(minor_name, &file_stat) < 0) {
		log_message(LOG_ERR, errno, "%s: Could not stat %s: ",
		    progname, minor_name);
		return (-1);
	}

	/* Get the major and minor numbers for this disk */
	dev_maj = getemajor(file_stat.st_rdev);
	dev_min = geteminor(file_stat.st_rdev);

	/* Get the service name using the dev_t */
	if (dcs_get_service_name_by_dev_t(dev_maj,
	    dev_min, other_service_name) != DCS_SUCCESS)
		/*
		 * There is no service name registered with this dev_t.
		 * Not an error.
		 */
		return (0);

	/*
	 * Check if the service name obtained is different from the disk name.
	 * Return 2 only if it's different from disk_name.
	 */
	if (strcmp((const char *)disk_name, (const char *)(*other_service_name))
	    != 0)
		return (2);
	else
	/* Default service name found. Return 1 to indicate this. */
		return (1);
}


int
find_device_service(char *root_service_name, char *usr_service_name,
    char *service_name, int minorlist_size, char *minorlist[])
{
	dc_error_t dc_err = 0;
	dc_replica_seq_t *nodes_seq;
	dc_gdev_range_seq_t *gdevs_range_seq;
	dc_property_seq_t *property_seq;
	int i, minorcount;
	char minor_name[MAXPATHLEN];
	struct stat file_stat;
	major_t dev_maj;
	minor_t dev_min;
	int return_val = 0;
	char value[MAXNAMELEN];
	char stat_service_name[MAXPATHLEN];
	char *other_service_name;

	/* Save the service name to be used for stat */
	(void) strcpy(stat_service_name, service_name);

	/*
	 * For a disk, first we check to see if this is already registered under
	 * a different name. If it's registered under a different name, we
	 * validate the registration and try to register again if the
	 * registration is incomplete.
	 */
	if ((service_name != NULL) && (strstr(service_name, "dsk/d") != NULL)) {
		return_val =
		    get_service_by_diskname(service_name, &other_service_name);
		/*
		 * If we have another service registered with this disk name,
		 * we replace service_name with the registered name.
		 */
		switch (return_val) {
			case 2:	/* Different service name */
				/*
				 * Reinitialize return val to 0 as it will be
				 * used subsequently.
				 */
				return_val = 0;
				/*
				 * service_name size needs to be changed if the
				 * other service name is of greater length than
				 * service name can accomodate.
				 */
				if (strlen(other_service_name) >= MAXPATHLEN) {
					free(service_name);
					service_name =
					    strdup((const char *)\
						    other_service_name);
				} else {
					(void) strcpy(service_name,
					    (const char *)other_service_name);
				}
				dcs_free_string(other_service_name);
				break;
			case 1: /* Default service name */
				break;
			default:
				return (return_val);
		}
	}

	/* Get the properties for this service */
	dc_err = dcs_get_service_parameters(service_name,
	    NULL, NULL, &nodes_seq, NULL,
	    &gdevs_range_seq, &property_seq);

	/* Checking service_name != NULL just to keep lint happy */
	if ((dc_err == DCS_SUCCESS) && (service_name != NULL)) {
		return_val = return_val | SERVICE_FOUND;

		/* Check if the proper range is registered. */
		for (i = 0; i < (int)gdevs_range_seq->count; i++) {
			for (minorcount = 0; minorcount < minorlist_size;
			    minorcount++) {
				if ((DEV_GLOBAL_STRLEN
				    + strlen(stat_service_name) +
				    strlen(minorlist[minorcount] + 1)) >
				    sizeof (minor_name)) {
					log_message(LOG_ERR, 0,
					    "%s: Buffer too small "
					    "for: %s%s%s", progname,
					    DEV_GLOBAL, stat_service_name,
					    minorlist[minorcount]);
					return (-1);
				}
				(void) sprintf(minor_name, "%s%s%s", DEV_GLOBAL,
				    stat_service_name, minorlist[minorcount]);
				if (stat(minor_name, &file_stat) < 0) {
					log_message(LOG_ERR, errno,
					    "%s: Could not stat %s: ",
					    progname, minor_name);
					return (-1);
				}
				if (!S_ISCHR(file_stat.st_mode) &&
				    !S_ISBLK(file_stat.st_mode)) {
					log_message(LOG_ERR, 0,
					    "%s: %s Not a device special file",
					    progname, minor_name);
					return (-1);
				}
				/*lint -e620 */
				dev_maj = getemajor(file_stat.st_rdev);
				dev_min = geteminor(file_stat.st_rdev);
				if (dev_maj !=
				    (gdevs_range_seq->ranges)[i].maj)
					continue;
				if ((dev_min <
				    (gdevs_range_seq->ranges)[i].end_gmin) &&
				    (dev_min >
				    (gdevs_range_seq->ranges)[i].start_gmin)) {
					return_val = return_val | GMIN_FOUND;
					break;
				}

			}
		}

		/* Check if this node is registered. */
		for (i = 0; i < (int)nodes_seq->count; i++) {
			if ((nodes_seq->replicas)[i].id == nodenum) {
				return_val = return_val | NODEID_FOUND;
				break;
			}
		}

		/* Check if we match a root device. */
		if ((root_service_name != NULL) && (usr_service_name != NULL)) {
			for (i = 0; i < (int)property_seq->count; i++) {
				if (strcmp((property_seq->properties)[i].name,
					"local_only") == 0) {
					if ((sizeof (nodenum) + 1) >
					    sizeof (value)) {
						log_message(LOG_ERR, 0,
						    "%s: Buffer too small "
						    "for: %d", progname,
						    nodenum);
						return_val = 0;
						break;
					}
					(void) sprintf(value, "%d", nodenum);
					if (strcmp(root_service_name,
					    service_name) != 0) {
						return_val = OTHER_ROOTDEV;
						break;
					}
					if (strcmp(usr_service_name,
					    service_name) != 0) {
						return_val = OTHER_ROOTDEV;
						break;
					}
				}
			}
		}

		dcs_free_dc_replica_seq(nodes_seq);
		dcs_free_dc_gdev_range_seq(gdevs_range_seq);
		dcs_free_properties(property_seq);
	}

	return (return_val);
}

int
register_service(int actions, char *service_name, int minorlist_size,
    char *minorlist[], char *service_class, char *root_service_name,
    char *usr_service_name)
{
	dc_replica_t *nodes;
	dc_gdev_range_seq_t *gdevs_range_seq;
	int minorcount, i;
	char slice_name[MAXPATHLEN];
#ifdef SC_EFI_SUPPORT
	char eslice_name[MAXPATHLEN];
#endif
	major_t dev_maj;
	minor_t dev_min;
	dc_gdev_range_t gdev_ranges[DCS_GDEVS_MAX];
	dc_replica_seq_t *nodes_seq;
	struct stat file_stat;
	dc_property_seq_t *property_seq;
	dc_error_t dc_err = 0;

	nodes = malloc(sizeof (dc_replica_t));
	if (nodes == NULL) {
		log_message(LOG_ERR, errno,
		    "%s: Cannot allocate memory.", progname);
		return (1);
	}
	nodes[0].preference = 0;
	nodes[0].id = nodenum;

	gdevs_range_seq = (dc_gdev_range_seq_t *)
	    malloc(sizeof (dc_gdev_range_seq_t));
	if (gdevs_range_seq == NULL) {
		log_message(LOG_ERR, errno,
		    "%s: Cannot allocate memory.", progname);
		free(nodes);
		return (1);
	}
	gdevs_range_seq->count = (uint_t)minorlist_size;
	for (minorcount = 0; minorcount < minorlist_size; minorcount++) {
		if ((DEV_GLOBAL_STRLEN + strlen(service_name) +
		    strlen(minorlist[minorcount]) + 1) > sizeof (slice_name)) {
			log_message(LOG_ERR, 0,
			    "%s: Buffer too small for %s%s%s",
			    progname, DEV_GLOBAL,
			    service_name, minorlist[minorcount]);
			return (1);
		}
		(void) sprintf(slice_name, "%s%s%s", DEV_GLOBAL, service_name,
		    minorlist[minorcount]);

#ifdef SC_EFI_SUPPORT
		eslice_name[0] = '\0';
		if (strcmp(&slice_name[strlen(slice_name) - 2], "s7") == 0) {
			(void) strcpy(eslice_name, slice_name);
			eslice_name[strlen(eslice_name) - 2] = '\0';
		}
#endif
		if (stat(slice_name, &file_stat) < 0) {
#ifdef SC_EFI_SUPPORT
			if (eslice_name[0] != '\0') {
				if (stat(eslice_name, &file_stat) < 0) {
					log_message(LOG_ERR, errno,
					    "%s: Cannot stat %s: ",
					    progname, eslice_name);
					return (1);
				}
			} else {
				log_message(LOG_ERR, errno,
				    "%s: Cannot stat %s: ",
				    progname, slice_name);
				return (1);
			}
#else
			log_message(LOG_ERR, errno,
			    "%s: Cannot stat %s: ", progname, slice_name);
			return (1);
#endif
		}
		if (!S_ISCHR(file_stat.st_mode) &&
		    !S_ISBLK(file_stat.st_mode)) {
			log_message(LOG_ERR, 0,
			    "%s: %s not a device special file.",
			    progname, slice_name);
			return (1);
		}
		/*lint -e620 */
		dev_maj = getemajor(file_stat.st_rdev);
		dev_min = geteminor(file_stat.st_rdev);

		gdev_ranges[minorcount].maj = dev_maj;
		gdev_ranges[minorcount].end_gmin = dev_min;
		gdev_ranges[minorcount].start_gmin = dev_min;
	}
	gdevs_range_seq->ranges = gdev_ranges;

	/*
	 * If we already have registered the service, we just need to
	 * add our node id and our list of minors.
	 */
	if (actions & SERVICE_FOUND) {
retry:
		/* Check whether local_only is set */
		if (!(actions & NODEID_FOUND)) {
			boolean_t prop_found = B_FALSE;
			dc_err = dcs_get_service_parameters(service_name,
				NULL, NULL, NULL, NULL, NULL, &property_seq);
			if (dc_err == DCS_SUCCESS) {
				for (i = 0; i < (int)property_seq->count;
									i++) {
					if (strcmp((property_seq->properties)
							[i].name, "local_only")
									== 0) {
						/*
						* We dont need to check the
						* value of this property, as
						* whichever node set it, will
						* also have added itself to the
						* node_list.
						*/
						prop_found = B_TRUE;
						break;
					}
				}
				dcs_free_properties(property_seq);
			} else {
				log_message(LOG_ERR, errno,
					"%s: dcs_get_service_parameters failed"
					" for service %s, returned: %s",
					progname, service_name,
					dcs_error_to_string(dc_err));
				free(nodes);
				free(gdevs_range_seq);
				return (1);
			}

			/* Add our nodeid, only if local_only is NOT set */
			if (!prop_found) {
				dc_err = dcs_add_node(service_name, &nodes[0]);
				if (dc_err != DCS_SUCCESS) {
					log_message(LOG_ERR, 0, "%s: Cannot add"
						"node to device service %s: %s",
						progname, service_name,
						dcs_error_to_string(dc_err));
					free(nodes);
					free(gdevs_range_seq);
					return (1);
				}
			}
		}

		/* Add our list of minors. */
		if (!(actions & GMIN_FOUND)) {
			dc_err = dcs_add_devices(service_name, gdevs_range_seq);
			if (dc_err != DCS_SUCCESS) {
				log_message(LOG_ERR, 0,
				    "%s: Cannot add device to device service "
				    "%s: %s", progname, service_name,
				    dcs_error_to_string(dc_err));
				free(nodes);
				free(gdevs_range_seq);
				return (1);
			}
		}
	} else {
		/* Need to create the service from scratch. */
		nodes_seq = (dc_replica_seq_t *)
		    malloc(sizeof (dc_replica_seq_t));
		if (nodes_seq == NULL) {
			log_message(LOG_ERR, errno,
			    "%s: Cannot allocate memory.", progname);
			free(nodes);
			free(gdevs_range_seq);
			return (1);
		}
		nodes_seq->replicas = nodes;
		nodes_seq->count = 1;

		property_seq = (dc_property_seq_t *)
		    malloc(sizeof (dc_property_seq_t));
		if (property_seq == NULL) {
			log_message(LOG_ERR, errno,
			    "%s: Cannot allocate memory.", progname);
			free(nodes);
			free(gdevs_range_seq);
			free(nodes_seq);
			return (1);
		}
		property_seq->count = 0;
		property_seq->properties = NULL;

		dc_err = dcs_create_service(service_name, service_class, 0,
		    nodes_seq, 0, gdevs_range_seq, property_seq);
		if (dc_err != DCS_SUCCESS) {
			free(nodes_seq);
			free(property_seq);
			/*
			 * This may have failed due to a race with other
			 * scgdevs instance(s) already having registered the
			 * service. Try one more time.
			 * See BugId 4451786.
			 */
			actions = find_device_service(root_service_name,
			    usr_service_name, service_name,
			    minorlist_size, minorlist);
			if (!(actions < 0)) {
				if (actions & OTHER_ROOTDEV) {
					return (OTHER_ROOTDEV);
				}
				if (actions & SERVICE_FOUND) {
					goto retry;
				}
			}
			log_message(LOG_ERR, 0,
			    "%s: Cannot create device service %s: %s", progname,
			    service_name, dcs_error_to_string(dc_err));
			return (1);
		}

		free(nodes_seq);

		if (strcmp(service_class, "DISK") == 0)
			if (set_disk_prop(service_name))
				return (1);
	}

	free(nodes);

	return (0);

}

int
remove_remote_nodes(char *service_name)
{
	int ret_val = 0;
	dc_error_t dc_err = 0;
	dc_replica_seq_t *nodes_seq;
	int i;

	dc_err = dcs_get_service_parameters(service_name,
	    NULL, NULL, &nodes_seq, NULL,
	    NULL, NULL);
	if (dc_err == DCS_SUCCESS) {
		for (i = 0; i < (int)nodes_seq->count; i++) {
			if ((nodes_seq->replicas)[i].id != nodenum) {
				dc_err = dcs_remove_node(service_name,
				    (nodes_seq->replicas)[i].id);
				if (dc_err != DCS_SUCCESS) {
					log_message(LOG_ERR, errno,
					    "%s: Cannot remove node from "
					    "device service %s: %s", progname,
					    service_name,
					    dcs_error_to_string(dc_err));
					ret_val = 1;
				}
			}
		}
		dcs_free_dc_replica_seq(nodes_seq);
	}

	return (ret_val);
}

int
set_local_only(char *service_name)
{
	int ret_val = 0;
	dc_error_t dc_err = 0;
	dc_property_seq_t *property_seq;
	int i;
	int prop_found = 0;
	char value[MAXNAMELEN];

	dc_err = dcs_get_service_parameters(service_name,
	    NULL, NULL, NULL, NULL, NULL, &property_seq);
	if (dc_err == DCS_SUCCESS) {
		for (i = 0; i < (int)property_seq->count; i++) {
			if (strcmp((property_seq->properties)[i].name,
			    "local_only") == 0) {
				/* We can return if the property is there. */
				if ((sizeof (nodenum) + 1) > sizeof (value)) {
					log_message(LOG_ERR, 0,
					    "%s: Buffer too small for: %d",
					    progname, nodenum);
					ret_val = 1;
				}
				(void) sprintf(value, "%d", nodenum);
				if (strcmp((property_seq->properties)[i].value,
				    value) == 0) {
					prop_found = 1;
				}
			}
		}
		dcs_free_properties(property_seq);

		if (ret_val) {
			return (ret_val);
		}
	}

	/* Didn't find it. Should add it. */
	if (prop_found == 0) {
		property_seq = (dc_property_seq_t *)
		    malloc(sizeof (dc_property_seq_t));
		if (property_seq == NULL) {
			log_message(LOG_ERR, errno,
			    "%s: Cannot allocate memory: ", progname);
			return (1);
		}
		property_seq->count = 1;
		property_seq->properties =
		    malloc(sizeof (dc_property_t));
		if (property_seq->properties == NULL) {
			log_message(LOG_ERR, errno,
			    "%s: Cannot allocate memory: ", progname);
			return (1);
		}
		(property_seq->properties)[0].name = (char *)malloc(
		    strlen("local_only") + 1);
		if ((property_seq->properties)[0].name == NULL) {
			log_message(LOG_ERR, errno,
			    "%s: Cannot allocate memory: ", progname);
			return (1);
		}
		(void) strcpy((property_seq->properties)[0].name, "local_only");

		(property_seq->properties)[0].value = (char *)malloc(
		    MAXNAMELEN);
		if ((property_seq->properties)[0].value == NULL) {
			log_message(LOG_ERR, errno,
			    "%s: Cannot allocate memory: ", progname);
			return (1);
		}
		if ((sizeof (nodenum) + 1) > sizeof (value)) {
			log_message(LOG_ERR, 0,
			    "%s: Buffer too small for %i", progname, nodenum);
			return (1);
		}
		(void) sprintf(value, "%i", nodenum);
		(void) strcpy((property_seq->properties)[0].value, value);

		dc_err = dcs_set_properties(service_name, property_seq);
		if (dc_err != DCS_SUCCESS) {
			log_message(LOG_ERR, 0,
			    "%s: Cannot set "
			    "device service %s property %s to %s: %s",
			    progname, service_name,
			    (property_seq->properties)[0].name,
			    (property_seq->properties)[0].value,
			    dcs_error_to_string(dc_err));
			return (1);
		}
	}
	return (0);
}

int
set_disk_prop(char *service_name)
{
	dc_error_t dc_err;
	dc_property_seq_t *property_seq;
	int i;
	unsigned int gdev_needed = 1;
	unsigned int auto_needed = 1;
	unsigned int total_needed;
	char *value;

	if ((value = strstr(service_name, "/d")) == NULL) {
		log_message(LOG_ERR, 0,
		    "set_disk_prop() called with invalid service_name %s",
		    service_name);
		return (1);
	}

	value++;

	dc_err = dcs_get_service_parameters(service_name,
	    NULL, NULL, NULL, NULL, NULL, &property_seq);
	if (dc_err == DCS_SUCCESS) {
		for (i = 0; i < (int)property_seq->count; i++) {
			if (strcmp((property_seq->properties)[i].name,
			    "gdev") == 0) {
				if (strcmp((property_seq->properties)[i].value,
				    value) == 0)
					gdev_needed = 0;
			}
			if (strcmp((property_seq->properties)[i].name,
			    "autogenerated") == 0)
				auto_needed = 0;
		}
		dcs_free_properties(property_seq);
	} else {
		log_message(LOG_ERR, errno,
		    "%s: dcs_get_service_parameters failed, returned %d",
		    progname, dc_err);
		return (1);
	}

	/* return if we don't have to add a property */
	if ((gdev_needed == 0) && (auto_needed == 0))
		return (0);

	total_needed = gdev_needed + auto_needed;

	property_seq = (dc_property_seq_t *)
	    malloc((sizeof (dc_property_seq_t)) * total_needed);
	if (property_seq == NULL) {
		log_message(LOG_ERR, errno,
		    "%s: Cannot allocate memory: ", progname);
		return (1);
	}
	property_seq->properties =
		    malloc((sizeof (dc_property_t)) * total_needed);
	if (property_seq->properties == NULL) {
		log_message(LOG_ERR, errno,
		    "%s: Cannot allocate memory: ", progname);
		return (1);
	}
	property_seq->count = total_needed;

	if (gdev_needed == 1) {
		(property_seq->properties)[0].name = (char *)malloc(
		    strlen("gdev") + 1);
		if ((property_seq->properties)[0].name == NULL) {
			log_message(LOG_ERR, errno,
			    "%s: Cannot allocate memory: ", progname);
			return (1);
		}
		(void) strcpy((property_seq->properties)[0].name, "gdev");

		(property_seq->properties)[0].value = (char *)malloc(
		    strlen(value) + 1);
		if ((property_seq->properties)[0].value == NULL) {
			log_message(LOG_ERR, errno,
			    "%s: Cannot allocate memory: ", progname);
			return (1);
		}
		(void) strcpy((property_seq->properties)[0].value, value);
	}

	if (auto_needed == 1) {
		(property_seq->properties)[total_needed - 1].name =
		    (char *)malloc(strlen("autogenerated") + 1);
		if ((property_seq->properties)[total_needed - 1].name == NULL) {
			log_message(LOG_ERR, errno,
			    "%s: Cannot allocate memory: ", progname);
			return (1);
		}
		(void) strcpy((property_seq->properties)[total_needed - 1].name,
		    "autogenerated");

		/* Set autogenerated property value to 1 */
		(property_seq->properties)[total_needed - 1].value =
		    (char *)malloc(2);
		if ((property_seq->properties)[total_needed - 1].value ==
		    NULL) {
			log_message(LOG_ERR, errno,
			    "%s: Cannot allocate memory: ", progname);
			return (1);
		}
		(void) strcpy
		    ((property_seq->properties)[total_needed - 1].value, "1");
	}

	dc_err = dcs_set_properties(service_name, property_seq);
	if (dc_err != DCS_SUCCESS) {
		log_message(LOG_ERR, 0,
		    "%s: Cannot set device service %s property %s: %s",
		    progname, service_name, (property_seq->properties)[0].name,
		    dcs_error_to_string(dc_err));
		return (1);
	}

	return (0);
}

int
register_disk(char *did_name, char *rootname, char *usrname, int slicecount,
    char **slicelist)
{
	char *service_name = NULL;
	char *root_service_name = NULL;
	char *usr_service_name = NULL;
	int ret;

	service_name = strchr(did_name, 'r');
	service_name++;

	if (rootname != NULL)
		root_service_name = (strchr(rootname, 'r') + 1);
	else
		root_service_name = NULL;

	if (usrname != NULL)
		usr_service_name = (strchr(usrname, 'r') + 1);
	else
		usr_service_name = NULL;

	ret = find_device_service(root_service_name, usr_service_name,
	    service_name, slicecount, slicelist);

	if (ret < 0)
		return (1);

	/* If it is a root device for another node, return. */
	if (ret & OTHER_ROOTDEV)
		return (0);

	/* If we didn't find the service, register it. */
	if (ret != (SERVICE_FOUND | NODEID_FOUND | GMIN_FOUND)) {
		switch (register_service(ret, service_name, slicecount,
		    slicelist, "DISK", root_service_name, usr_service_name)) {
			case 0:
				break;
			case OTHER_ROOTDEV:
				return (0);
			default:
				return (1);
		}
	}

	if (root_service_name != NULL) {
		if (strcmp(service_name, root_service_name) == 0) {
			/* Add the local only property. */
			if (set_local_only(service_name))
				return (1);
			/*
			 * Remove any nodes which have added themselves
			 * as replicas.
			 */
			if (remove_remote_nodes(service_name))
				return (1);
		}
	}
	if (usr_service_name != NULL) {
		if (strcmp(service_name, usr_service_name) == 0) {
			/* Add the local only property. */
			if (set_local_only(service_name))
				return (1);
			/*
			 * Remove any nodes which have added themselves
			 * as replicas.
			 */
			if (remove_remote_nodes(service_name))
				return (1);
		}
	}

	return (0);
}

int
register_tape(char *did_name, int tapecount, char **tapelist)
{
	int ret;

	ret = find_device_service(NULL, NULL, did_name, tapecount, tapelist);

	/* If we found, service, nodeid, and all minors, return. */
	if (ret == (SERVICE_FOUND | NODEID_FOUND | GMIN_FOUND))
		return (0);

	if (register_service(ret, did_name, tapecount, tapelist, "TAPE",
	    NULL, NULL))
		return (1);

	return (0);

}

int
register_devices(int slicecount, char **slicelist, int tapecount,
	char **tapelist)
{
	char *rootdid, *usrdid;
	did_device_list_t *devlist;
	char *pathlist, *didname;
	char *path;
	char *hostname;
	char *didret;

	pathlist = (char *)malloc(MAXPATHLEN * NODEID_MAX);
	if (pathlist == NULL) {
		log_message(LOG_ERR, errno,
		    "%s: Cannot allocate memory: ", progname);
		return (1);
	}
	didname = (char *)malloc(MAXPATHLEN);
	if (didname == NULL) {
		log_message(LOG_ERR, errno,
		    "%s: Cannot allocate memory: ", progname);
		return (1);
	}

	rootdid = get_rootdev(GETROOT);
	usrdid  = get_rootdev(GETUSR);
	hostname = get_hostname();

	if (hostname == NULL) {
		return (1);
	}
	(void) sprintf(hostname, "%s:", hostname);

	devlist = list_all_devices(NULL);
	if (devlist == NULL) {
		return (1);
	}

	while (devlist != NULL) {
		didret = did_get_path(devlist);
		if (didret == NULL) {
			log_message(LOG_ERR, 0,
			    "%s: Cannot find path for a DID device. Check DID "
			    "configuration.", progname);
			return (1);
		}
		(void) strcpy(pathlist, didret);
		free(didret);

		didret = did_get_did_path(devlist);
		if (didret == NULL) {
			log_message(LOG_ERR, 0,
			    "%s: Cannot find "
			    "DID path for a DID device. Check DID "
			    "configuration.", progname);
			return (1);
		}
		(void) strcpy(didname, didret);
		free(didret);

		path = strtok(pathlist, " ");
		while (path != NULL) {
			if (strstr(path, hostname)) {
				if (strstr(path, "dev/rdsk")) {
					if (register_disk(didname, rootdid,
					    usrdid, slicecount, slicelist))
						log_message(LOG_ERR, 0,
						    "%s: Could not "
						    "register disk %s",
						    progname, devlist);
				} else if (strstr(path, "dev/rmt")) {
					if (register_tape(didname,
					    tapecount, tapelist))
						log_message(LOG_ERR, 0,
						    "%s: Could not "
						    "register tape %s",
						    progname, devlist);
				} else {
					log_message(LOG_ERR, 0,
					    "%s: Unknown device type: %s",
					    progname, path);
					return (1);
				}
			}
			path = strtok(NULL, " ");
		}

		devlist = devlist->next;
	}

	return (0);
}

/* XXX - This should really be multithreaded. */
int
do_verify()
{
	did_device_list_t *devlist = NULL, *devp;
	struct stat statbuf, md_stat;
	char *didret, *did_name, *did_diskname, *did_tapename;
	dc_error_t dc_err = 0;
	dc_replica_seq_t *nodes_seq;
	dc_property_seq_t *property_seq;
	char *service_name;
	int is_tape, node_found, i, is_iscsi;
	char *did_subpath_list;
	char *hostname;
	char *tmp_path_name;

	hostname = get_hostname();
	if (hostname == NULL) {
		return (1);
	}
	(void) sprintf(hostname, "%s:", hostname);

	/* Is /dev/global a valid link? */
	if (stat("/dev/global", &statbuf) != 0) {
		return (1);
	}
	if (S_ISLNK(statbuf.st_mode)) {
		return (1);
	}

	/* Is /dev/md/shared a link? */
	if (stat("/dev/md/shared", &statbuf) != 0) {
		return (1);
	}
	if (S_ISLNK(statbuf.st_mode)) {
		return (1);
	}

	/* DID namespace verify */
	devlist = list_all_devices(NULL);

	if (devlist == NULL) {
		/* No DID devices. Return success. */
		return (0);
	}

	/*
	 * Populate the DID instance cache and scsi3 default fencing cache
	 * when the system reboots.
	 */
	if (did_populate_list_cache(devlist) == -1) {
		log_message(LOG_NOTICE, 0, "%s: DID instance cache was "
		    "not initialized, failover performance may be impacted.",
		    progname);
	}

	devp = devlist;

	did_name = (char *)malloc(MAXPATHLEN);
	if (did_name == NULL) {
		return (1);
	}
	did_diskname = (char *)malloc(MAXPATHLEN);
	if (did_diskname == NULL) {
		return (1);
	}
	did_tapename = (char *)malloc(MAXPATHLEN);
	if (did_tapename == NULL) {
		return (1);
	}

	while (devp != NULL) {
		node_found = 0;
		is_tape = 0;
		is_iscsi = 0;

		didret = did_get_did_path(devp);
		if (didret == NULL) {
			return (1);
		}
		if ((devp->flags & ISCSI_T_PROTOCOL) == ISCSI_T_PROTOCOL) {
		    is_iscsi = 1;
		}
		(void) strcpy(did_name, didret);
		free(didret);
		(void) sprintf(did_diskname, "%s%s%s", "/dev/did/",
		    did_name, "s2");
		(void) sprintf(did_tapename, "%s%s%s", "/dev/did/",
		    did_name, "n");

		/* Tapes are not supported as iSCSI Devices */
		if (!is_iscsi) {
		    /* Confirm DID device shows up in /dev/did */
		    if (stat(did_diskname, &statbuf) != 0) {
			    if (stat(did_tapename, &statbuf) != 0) {
				    return (1);
			    }
			    is_tape = 1;
		    }

		    (void) sprintf(did_diskname, "%s%s%s", DEV_GLOBAL,
			did_name, "s2");
		    (void) sprintf(did_tapename, "%s%s%s", DEV_GLOBAL,
			did_name, "n");

		    /* Confirm DID device shows up in /dev/global */
		    if (stat(did_diskname, &statbuf) != 0) {
			    if (stat(did_tapename, &statbuf) != 0) {
				    return (1);
			    }
			    is_tape = 1;
		    }
		}

		/* Confirm DID device is registered with DCS */
		if (is_tape == 0) {
			service_name = strchr(did_name, 'r');
			service_name++;
		} else {
			service_name = did_name;
		}
		dc_err = dcs_get_service_parameters(service_name,
		    NULL, NULL, &nodes_seq, NULL, NULL, &property_seq);
		if (dc_err != DCS_SUCCESS) {
			return (1);
		}

		/* Check if this node has a path to the DID device */
		did_subpath_list = did_get_path(devp);
		if (did_subpath_list == NULL)
			return (1);
		if (strstr(did_subpath_list, hostname) != NULL) {
			/* Determine if we're registered with DCS. */
			for (i = 0; i < (int)nodes_seq->count; i++) {
				if ((nodes_seq->replicas)[i].id == nodenum) {
					node_found = 1;
					break;
				}
			}

			if (node_found == 0) {
				for (i = 0; i < (int)property_seq->count; i++) {
					if (strcmp((
					    property_seq->properties)[i].name,
					    "local_only") == 0) {
						node_found = 1;
					}
				}
			}

			if (node_found == 0) {
				free(did_subpath_list);
				return (1);
			}
		}
		free(did_subpath_list);

		devp = devp->next;
	}

	/*
	 * Fix for bugid 4314698. First stat checks if SDS is installed or not.
	 * If SDS is not installed then we don't need to do anything. If it is
	 * installed, we construct the pathname for the md admin device node
	 * (md@0:admin) in this node's global device namespace. The second stat
	 * checks if the admin node is present or not. If it is not present, the
	 * SDS device nodes have not been copied from the /devices/pseudo
	 * directory. In that case, we return 1, which triggers global device
	 * namespace reconfiguration (scgdevs -g) at boot -r time, and the SDS
	 * global device namespace gets populated correctly.
	 */

	if (stat("/usr/sbin/metaset", &md_stat) == 0) {
		tmp_path_name = (char *)malloc(sizeof (char) *
		    (MAXPATHLEN + 1));
		if (tmp_path_name == NULL) {
			log_message(LOG_ERR, errno,
			    "%s: Cannot allocate memory: ", progname);
			return (1);
		}
		(void) sprintf(tmp_path_name, "%s%s", gdevprefix,
		    "devices/pseudo/md@0:admin");
		if (stat(tmp_path_name, &md_stat) != 0) {
			free(tmp_path_name);
			return (1);
		}

#if (SOL_VERSION <= __s9)
		/*
		 * SVM creates a device node for every metadevice of every
		 * diskset the configuration can support. The number of
		 * metdevices (nmd) and disksets (md_nsets) are defined
		 * in /kernel/drv/md.conf and read by SVM at startup after
		 * a reconfiguration reboot.
		 *
		 * This behavior applies to S9 only.
		 *
		 * Every device node in the SVM namespace under
		 * /devices/pseudo/ must be populated in the global device
		 * namespace under /global/.devices/node@X/devices/pseudo/.
		 *
		 * A problem arises when the user increases either parameter
		 * in /kernel/drv/md.conf, followed by a reconfiguration
		 * reboot. This causes new device nodes to be created in
		 * the SVM namespace, but not populated in the global device
		 * namespace. To correct this problem, the user must run
		 * "scgdevs" or "cldevice populate" manually.
		 *
		 * The fix is to retrieve the number of metdevices (nmd)
		 * and disksets (md_nsets) from SVM. We then check the
		 * existence of a metadevice with name started with
		 * md@0:(md_nsets-1),(nmd-1) under /devices/pseudo/ and
		 * the global namespace.
		 *
		 * This function returns 0 if the device is found under
		 * /devices/pseudo/, but not in the global namespace.
		 * Otherwise, it returns 1.
		 *
		 * Here we query the md admin device directly for the
		 * nmd and md_nsets parameters. The md admin device and
		 * the associated ioctls are SVM private.
		 *
		 * The alternative is to walk the devinfo node list until
		 * the md node is found and retrieve the properties then. A
		 * public interface is available to walk the devinfo list. But
		 * there is concern about the performance as the average time
		 * it takes to search for the md node is linear to the number
		 * of devices on the system.
		 *
		 * So we choose to use the ioctl approach here for better
		 * performance, plus this fix is targeted for s9 only and
		 * it is not likely the private interface would evolve in the
		 * future.
		 *
		 * Note in theory we should check the global namespace for
		 * every metadevice in the SVM namespace. This will be very
		 * time consuming since the number of such devices could be
		 * huge. Therefore we only check the last device for the last
		 * diskset here.
		 */
		int fd;
		if ((fd = open(ADMSPECIAL, O_RDWR, 0)) < 0) {
			log_message(LOG_ERR, errno,
			    "%s: Cannot open device %s", progname, ADMSPECIAL);
			free(tmp_path_name);
			return (1);
		}

		int md_nsets = 0;
		int nmd = -1;

		if (ioctl(fd, MD_IOCGETNSET, &md_nsets) == -1) {
			log_message(LOG_ERR, errno,
			    "%s: Failed to retrieve value md_nsets from %s",
			    progname, ADMSPECIAL);
			close(fd);
			free(tmp_path_name);
			return (1);
		}

		if (ioctl(fd, MD_IOCGETNUNITS, &nmd) == -1) {
			log_message(LOG_ERR, errno,
			    "%s: Failed to retrieve value nmd from %s",
			    progname, ADMSPECIAL);
			close(fd);
			free(tmp_path_name);
			return (1);
		}

		close(fd);

		/*
		 * Construct the path of the last metadevice for
		 * the last diskset under /devices/pseudo/.
		 */
		if (snprintf(tmp_path_name, MAXPATHLEN + 1,
		    "%s%d,%d,blk", "/devices/pseudo/md@0:",
		    md_nsets - 1, nmd - 1) < 0) {
			/*
			 * Failed to construct the device path
			 */
			log_message(LOG_ERR, errno,
			    "%s: Failed to construct the md device path",
			    progname);
			close(fd);
			free(tmp_path_name);
			return (1);
		}

		/*
		 * Check if the md device exists
		 */
		if (stat(tmp_path_name, &md_stat) == 0) {
			/*
			 * Device found under /devices/pseudo/. Check
			 * if it is also in the global namespace.
			 *
			 * Construct the global device path.
			 */
			if (snprintf(tmp_path_name, MAXPATHLEN + 1,
			    "%s%s%d,%d,blk", gdevprefix,
			    "devices/pseudo/md@0:", md_nsets - 1,
			    nmd - 1) < 0) {
				/*
				 * Failed to construct the device path
				 */
				log_message(LOG_ERR, errno,
				    "%s: Failed to construct the global "
				    "device path",
				    progname);
				free(tmp_path_name);
				return (1);
			}

			/*
			 * Check if the global device exists
			 */
			if (stat(tmp_path_name, &md_stat) != 0) {
				/*
				 * Device not found in the global namespace
				 */
				log_message(LOG_ERR, errno,
				    "%s: Device is not found in the global "
				    "name space",
				    progname);
				free(tmp_path_name);
				return (1);
			}
		}
#endif
		free(tmp_path_name);
	}

	return (0);

}

void
do_local_config()
{
	if (reconfig_did()) {
		log_message(LOG_ERR, 0,
		    "Cannot reconfig DID driver.");
		exit_code = 1;
		exit(1);
	}
	if (upload_did_config()) {
		log_message(LOG_ERR, 0,
		    "Cannot upload new paths to DID driver.");
		exit_code = 1;
		exit(1);
	}
	if (reconfig_did_driver()) {
		log_message(LOG_ERR, 0,
		    "Cannot reconfig Solaris namespace.");
		exit_code = 1;
		exit(1);
	}
}

void
do_global_config()
{
	char *sparc_slicelist[] = { "s0", "s1", "s2", "s3", "s4", "s5", "s6",
	    "s7" };
	char *intel_slicelist[] = { "s0", "s1", "s2", "s3", "s4", "s5",
	    "s6", "s7", "s8", "s9", "s10", "s11", "s12", "s13", "s14", "s15" };
	char *tapelist[] = { "", "b", "bn", "c", "cb", "cbn", "cn", "h", "hb",
	    "hbn", "hn", "l", "lb", "lbn", "ln", "m", "mb", "mbn", "mn", "n",
	    "u", "ub", "ubn", "un" };
	char **slicelist;
	int slicecount;
	int tapecount = sizeof (tapelist) / sizeof (char *);
	char arch[MAXNAMELEN];
	long ret;
	mode_t default_mask;

	ret = sysinfo(SI_ARCHITECTURE, arch, MAXNAMELEN);
	if (ret > MAXNAMELEN) {
		log_message(LOG_ERR, 0,
		    "%s: Insufficient space to determine architecture.",
		    progname);
		exit_code = 1;
		exit(1);
	} else if (ret < 0) {
		log_message(LOG_ERR, 0,
		    "%s: Cannot get system architecture: ", progname);
		perror("");
		exit_code = 1;
		exit(1);
	}

	if (strcmp(arch, "sparc") == 0) {
		slicelist = sparc_slicelist;
		slicecount = sizeof (sparc_slicelist) / sizeof (char *);
	} else if (strcmp(arch, "i386") == 0) {
		slicelist = intel_slicelist;
		slicecount = sizeof (intel_slicelist) / sizeof (char *);
	} else {
		log_message(LOG_ERR, 0,
		    "%s: Architecture %s does not match known system arch.",
		    progname, arch);
		exit_code = 1;
		exit(1);
	}
	/*
	 * Before creating/copying device files clear the umask.
	 * Typically this will be 022 by default, meaning we cannot
	 * create a device file writable by group and other - as is
	 * the requirement for tape devices - among others.
	 * Save the original mask so we can restore it later on.
	 */
	default_mask = umask(0);

	if (make_directories()) {
		log_message(LOG_ERR, 0, "%s: Directory creation failed.",
		    progname);
		exit_code = 1;
		exit(1);
	}
	if (basic_links()) {
		log_message(LOG_ERR, 0, "%s: Link creation failed.", progname);
		exit_code = 1;
		exit(1);
	}
	if (move_md()) {
		log_message(LOG_ERR, 0, "%s: Cannot move /dev/md devices.",
		    progname);
		exit_code = 1;
		exit(1);
	}
	if (copy_physical_devices()) {
		log_message(LOG_ERR, 0,
		    "%s: Cannot populate global physical namespace.",
		    progname);
		exit_code = 1;
		exit(1);
	}
	if (populate_logical_devices()) {
		log_message(LOG_ERR, 0,
		    "%s: Cannot reconfig global logical namespace.", progname);
		exit_code = 1;
		exit(1);
	}

	/*lint -e746 */
	if (dcs_initialize()) {
		log_message(LOG_ERR, 0,
		    "%s: Could not initialize dcs library", progname);
		exit_code = 1;
		exit(1);
	}
	if (register_devices(slicecount, slicelist, tapecount, tapelist)) {
		log_message(LOG_ERR, 0,
		"%s: Cannot register devices as HA.", progname);
		exit_code = 1;
		exit(1);
	}
	/* Restore the original umask */
	(void) umask(default_mask);
}

int
main(int argc, char *argv[])
{
	int c;
	int op_local_only = 0;
	int op_global_only = 0;
	int op_verify = 0;
	int op_booting = 0;
	char prog[MAXPATHLEN];
	int validate_mode_ret = 0;

	/* Initialize and demote to normal uid */
	cl_auth_init();
	cl_auth_demote();

	(void) strcpy(progname, argv[0]);

	(void) setlocale(LC_ALL, "");
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);
	(void) textdomain(TEXT_DOMAIN);

	if (sc_zonescheck() != 0)
		return (1);

	/* Check authorization */
	cl_auth_check_command_opt_exit(argv[0], NULL,
	    CL_AUTH_SYSTEM_MODIFY, GDEVS_NOTROOT);

	while ((c = getopt(argc, argv, "SLlgv")) != EOF) {
		switch (c) {
		case 'S':
			/*
			 * Detach from tty.
			 * TODO: these closes may not be necessary with the
			 * setsid() call below.
			 */
			(void) close(0);
			(void) close(1);
			(void) close(2);
			switch (fork1()) {
			case (pid_t)-1:
				log_message(LOG_ERR, errno,
				    "%s:  fork1() error.  Exiting.", progname);
				exit(1);
				break;
			case 0:
				/* child */
				(void) setsid();
				break;
			default:
				/*
				 * Parent - don't wait for child - 4285210.
				 */
				exit(0);
				break;
			}
			op_no_remote++;
			break;
		case 'L':
			op_no_remote++;
			break;
		case 'l':
			op_local_only++;
			op_booting++;
			break;
		case 'g':
			op_global_only++;
			op_booting++;
			break;
		case 'v':
			op_verify++;
			break;
		case '?':
		default:
			usage();
			exit(2);
		}
	}
	if (op_local_only && op_global_only) {
		log_message(LOG_ERR, 0,
		    "%s: Cannot combine -l and -g options.", progname);
		exit(1);
	}

	if ((op_local_only || op_global_only) && op_verify) {
		log_message(LOG_ERR, 0,
		    "%s: Cannot combine -v with any other option.", progname);
		exit(1);
	}

	/* Promote to euid=0 */
	cl_auth_promote();

	validate_mode_ret = validate_mode();
	if (validate_mode_ret)
		exit(validate_mode_ret);

	/* Generate event */
	exit_code = 0;
	cl_cmd_event_init(argc, argv, &exit_code);
	(void) cl_cmd_event_gen_start_event();
	(void) atexit(cl_cmd_event_gen_end_event);

	/* Set gdevprefix */
	if (cladm(CL_CONFIG, CL_GDEV_PREFIX, gdevprefix) != 0) {
		log_message(LOG_ERR, 0,
		    "%s: _cladm failed. Please"
		    "ensure the node is in cluster mode.", progname);
		exit_code = 1;
		exit(1);
	}
	(void) strcat(gdevprefix, "/");

	/* No need to validate mounts if we're doing local only. */
	if (!op_local_only)
		if (validate_mounts()) {
			exit_code = 1;
			exit(1);
		}

	nodenum = getnodeid();
	if (nodenum == NULL)
		exit(0);

	if (op_verify) {
		if (do_verify() != 0) {
			exit_code = 1;
			exit(1);
		} else {
			exit(0);
		}
	}

	/* Handling SIGINT */
	if (signal(SIGINT, handle_sigint) == SIG_ERR)
		log_message(LOG_WARNING, errno, "%s: signal error.", progname);

	if (op_local_only || !op_global_only) {
		(void) printf(gettext("Configuring DID devices\n"));
		do_local_config();
	}

	if (op_global_only || !op_local_only) {
		(void) printf(gettext("Configuring the /dev/global directory "
		    "(global devices)\n"));
		(void) fflush(stdout);
		do_global_config();
	}

	/*
	 * If we are booting this command will be run independently
	 * of scgdevs. Otherwise it is needed when you add a disk
	 * that will be using scsi-3 (i.e. connected to
	 * more than 2 nodes) without rebooting. To use the disk,
	 * we must have registered the node's key with the disk.
	 */
	if (!op_booting)
		if (mysystem(RESERVE_CMD)) {
			exit_code = 1;
			exit(1);
		}

	/*
	 * Call scgdevs on the rest of the nodes. This will
	 * occur asynchronously . Do not call this in local only mode.
	 */
	if (!op_no_remote && !op_local_only) {
		if ((strlen(DIDPROG) + 5 + 1) > sizeof (prog)) {
			log_message(LOG_ERR, errno,
			    "%s: Buffer too small for: %s -S",
			    progname, DIDPROG);
			exit_code = 1;
			exit(1);
		}
		(void) sprintf(prog, "%s -S", DIDPROG);

		if (mysystem(prog)) {
			exit_code = 1;
			exit(1);
		}
	}

	exit(0);
}

static void
log_message(int priority, int errnum, const char *msg, ...)
{
	char *errstr = NULL;
	char buf[1024];
	va_list ap;
	sc_syslog_msg_handle_t	handle;

	if (errnum != 0)
		errstr = strerror(errnum);
	va_start(ap, msg);	/*lint !e40 */
	(void) sc_syslog_msg_initialize(&handle,
	    "Cluster.devices.did", "");
	(void) vsnprintf(buf, 1024, msg, ap);
	(void) sc_syslog_msg_log(handle, priority, MESSAGE, buf);
	va_end(ap);
	if (errstr != NULL) {
		(void) sc_syslog_msg_log(handle, priority, MESSAGE, errstr);
	}
	sc_syslog_msg_done(&handle);
}

static void
handle_sigint(int sig)
{
	(void) printf(gettext("%s: cannot be interupted\n"), progname);
	(void) signal(SIGINT, handle_sigint);
} /*lint !e715 */
