#! /usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident       "@(#)truecopy.ksh 1.7     08/10/02 SMI"
#

#
# This script performs the steps required prior to failing over a device group
# which contains Truecopy replicated devices.  It:
#
# - makes sure the HORCM daemon is running
#
# - performs an horctakeover to ensure that that this node is the replica
#   master
#
# calls to this script are of the form : truecopy -s <service_name>, with
# <service_name> being the name of the device group and the Truecopy
# replication dev_group (these two things must be named the same).
#
# Returns:    0 - This node is now the replication master
#             1 - Unable to make this node the replication master, most likely
#                 due to a bad replication state, requiring user intervention
#             2 - internal error occurred, replication status unknown
#

service_name=
ASYNC_WAIT=2880


#####################################################
#
# This script depends on an ASCII collating
# sequence for checking things like legal node names,
# adapter names, and junction names.  There are no dependencies
# on the collating sequence for inspecting, sorting, or massaging any
# data which might be internationalized.  Therefore,
# the collating sequence locale is forced to the 'C' locale.
#
#####################################################
typeset -x TEXTDOMAIN=SUNW_SC_CMD
typeset -x TEXTDOMAINDIR=/usr/cluster/lib/locale

LC_COLLATE=C;  export LC_COLLATE

#####################################################
# check zone credentials

/usr/cluster/lib/sc/sc_zonescheck
if [ $? -ne 0 ]; then
	exit 1
fi

##########################
# get command line options
##########################
while getopts s: name
do

	case $name in
		s)	service_name="$OPTARG"
			;;
		?)	echo $0:  `gettext "illegal command line option"`
			exit 2
			;;
	esac
done

if [ -z $service_name ]
then
	echo $0:  `gettext "service_name not specified"`
	exit 2
fi

# make sure HORCM daemon is running
/usr/bin/horcmstart.sh

# get replication status
pairvolchk -ss -g $service_name > /dev/null 2>&1
retval=$?

# see if we are already the replication primary, 23 = PVOL_PAIR (43 async)
if [[ $retval = 23  || $retval = 43 ]]
then
	echo $service_name status is PVOL_PAIR, no takeover required
	exit 0
fi

# 33 SVOL_PAIR, 53 SVOL_PAIR (async)
if [[ $retval = 33  || $retval = 53 ]]
then
	echo $service_name status is $status, performing horctakeover
	/usr/bin/horctakeover -g $service_name -t $ASYNC_WAIT
	rval=$?
	echo horctakeover returned $rval
	if [ $rval = 233 ]
	then
	    # asychronous sync timeout, increase the value of ASYNC_WAIT
	    echo Timeout waiting for the takeover operation to synchronize the PVOL and SVOL
	fi
	exit 0
else
	echo Invalid replication state $retval for $service_name, check replication configuration
	exit 2
fi

exit 0
