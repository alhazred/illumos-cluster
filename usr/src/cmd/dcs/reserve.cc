//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)reserve.cc	1.105	08/05/21 SMI"


//
//
// the following invocations are accepted:
//
// reserve	-c node_join -h host_name [ -x | -S | -X]
//		-c release_shared_scsi2 -j joining_node -h host_name
//		-c fence_node -f fenced_node -h host_name
//		-c switch_device_scsi_protocol -d device
//			-P <scsi2|scsi3|nofence>
//		-c reset_shared_bus -h host_name
//		-c fence_all_nodes -h hostname
//		-c grab_lock -n lock_name
//		-c release_lock -n lock_name
//		-c check_lock -n lock_name [ -t timeout -r retry_interval ]
//
//
//
// This program is called by it's close, personal friend, 'run_reserve', and
// issues scsi reservations to either obtain or restrict access to the specified
// disks.  When a node is joining the cluster, this program makes sure that the
// node can access all attached disks.  This involves invoking run_reserve on
// all other cluster nodes with the transition 'release_shared_scsi2'.  Also
// failfast is enabled on all devices attached to each node.
//
// When there is a CMM reconfiguration, this reservation program is called to
// fence all non-cluster nodes from shared devices.
//
//


#include <strings.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stropts.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <limits.h>
#include <sys/mhd.h>
#include <sys/clconf_int.h>
#include <sys/quorum_int.h>
#include <orb/infrastructure/orb_conf.h>
#include <orb/object/adapter.h>
#include <dc/libdcs/libdcs.h>
#include <sys/didio.h>
#include <libdid.h>
#include <sys/sysmacros.h>
#include <sys/threadpool.h>
#include <nslib/ns.h>
#include <nslib/data_container_impl.h>
#include <h/data_container.h>
#include <h/repl_pxfs.h>
#include <errno.h>
#include <h/dc.h>
#include <h/cmm.h>
#include <cmm/cmm_ns.h>
#include <sys/scsi/scsi.h>
#include <rgm/sczones.h>

#define	MAX_STR_LEN (2*MAXPATHLEN)	// maximum size of disk name string
						// made larger to accomodate
						// additional information
						// such as node name
#define	INIT_KEYS NODEID_MAX		// initial number of reservation keys
#define	INIT_RESVS NODEID_MAX		// initial number of disk reservations
#define	SCSI2_POLL_PERIOD 2000		// poll period for failfast thread
#define	SCSI3_POLL_PERIOD INT_MAX 	// do not create failfast thread on
						// scsi-3 disks, just panic
						// when access denied
#define	RESV_PATH "/usr/cluster/lib/sc"	// path to the reservation programs

#define	NUM_RETRIES 3			// number of times to retry device
						// opens / ioctls
#define	RETRY_SLEEP 2			// seconds to sleep between retries
#define	MAX_ELEMENTS		1000	// number of objects to list from
					// from the DID instance cache with
					// each list operation


//
// This is used for the reset_shared_bus code.  If there are actually more
// than 64 controllers, the code should work fine, it will just become
// incredibly inefficient for controllers about the 64 mark.  Since it will
// only remember 64 controllers that it has reset, controllers above the 64
// mark will get reset for each device on it.  This number should be easily
// increased, with the only real penalty being extra memory use.
//
#define	MAX_CONTROLLERS_PER_NODE 64	// maximum number of controllers
						// attached to a node

// structure for storing disk info
typedef struct disk_list {
	char			*did_path;
	char			*nodes;
	unsigned short		default_fencing;
	unsigned short		detected_fencing;
	struct disk_list	*next;
} disk_list_t;
disk_list_t *my_disks = NULL;

// status of another node connected to a shared disk
enum node_status {NODE_STATUS_NODE_UP, NODE_STATUS_NODE_DOWN,
    NODE_STATUS_NO_NODE, NODE_STATUS_ERROR};

nodeid_t myid;			// node id for this node
mhioc_resv_key_t mykey;		// reservation key for this node
char *host_name = NULL;		// this node's hostname
char *release_shared_scsi2_cmd = NULL;	// command to run on remote nodes to
					    // release scsi-2 reservations
char *node_join_scrubbing_cmd =		// command to run on remote nodes to
	"/usr/cluster/lib/sc/run_reserve -c node_join -x";  // scrub scsi-3 keys
cmm::cluster_state_t clust_state;	// cluster membership from lock server
clconf_cluster_t *cl;		// passed into clconf_cluster_get_nodename...()
os::sc_syslog_msg msg_handle("", "", "");
bool run_clexecd = true;	// during normal node join we use clexecd
				// during a call from scgdevs, we do not
char *command = NULL;		// fencing command to execute
int fence_node_id;		// nodeid of node we are fencing with fence_node
quorum_status_t	*quor_table = NULL;	// list of quorum devices
bool node_join_scrubbing = false;	// is this a scrubbing node_join call?
bool scrub_no_fence = false;	// scrub disks that just had fencing turned off

int verbose = 0;		// can be turned on to generate detailed output

// global default fencing status.
unsigned short global_fencing_status = GLOBAL_FENCING_UNKNOWN;


// internal utility functions

//
// Given nodename, use clconf_cluster_get_nodename_by_nodeid() to get the nodeid
// Return -1 if not found
//
static int
get_nodeid_by_nodename(char *nodename)
{
	int i;
	const char *tempname;

	for (i = 0; i < NODEID_MAX + 1; i++) {
		tempname = clconf_cluster_get_nodename_by_nodeid(cl,
		    (nodeid_t)i);

		if (tempname == NULL)
			continue;

		if (strcmp(tempname, nodename) == 0)
			return (i);
	}

	// didn't find match
	return (-1);
}

//
// Given the /dev/did/rdsk/dX form of a device name, calculate the
// /dev/rdsk/cXtXdX form of the name (the one appropriate for the local node).
// The path is returned in 'ctd', which is set to NULL if an error occurs or
// there is no matching Solaris path from this node.
//
static void
get_ctd_from_did(char *hostname, char *did, char **ctd)
{
	did_device_list_t *dlist;
	did_subpath_t *slist;
	char search_for_hostname[MAX_STR_LEN];

	*ctd = did;
	if (*ctd == NULL)
		return;

	*ctd = strstr(did, "/dev/did/rdsk/");
	if (*ctd == NULL)
		return;

	*ctd = did + strlen("/dev/did/rdsk/");
	if (strstr(*ctd, "d") == NULL) {
		*ctd = NULL;
		return;
	}

	// XXX dlist needs to be freed using free_did_list()
	dlist = map_from_did_device(*ctd);
	if (dlist == NULL) {
		*ctd = NULL;
		return;
	}

	if (dlist->subpath_listp == NULL) {
		*ctd = NULL;
		return;
	}

	(void) sprintf(search_for_hostname, "%s:", hostname);

	*ctd = NULL;
	for (slist = dlist->subpath_listp; slist != NULL; slist = slist->next) {
		if (slist->device_path == NULL)
			continue;
		if (strstr(slist->device_path, search_for_hostname) == NULL)
			continue;
		*ctd = strstr(slist->device_path, ":") + 1;
		break;
	}
}


//
// Only fence nodes that don't have the local_only flag enabled.
// Returns true if local_only property has been set. For all other
// cases, including errors, we return false.
//
static bool
is_local_only(char *did_path)
{
	int j;
	dc_property_seq_t *property_seq;
	dc_error_t dc_err = DCS_SUCCESS;
	char *dsk_path = NULL;

	//
	// The did_path is in the form of rdsk/d<#>. We
	// move past the 'r' since the dcs look needs to be
	// done on the the default rawdisk device group (dsk/d<#>).
	//
	dsk_path = did_path + 1;
	dc_err = dcs_get_service_parameters(dsk_path,
	    NULL, NULL, NULL, NULL, NULL, &property_seq);
	if (dc_err != DCS_SUCCESS) {
		//
		// SCMSGS
		// @explanation
		// The device fencing program was unable to determine if the
		// specified device is marked as local_only. This device will
		// be treated as a non- local_only device and nodes not within
		// the cluster will be fenced from it.
		// @user_action
		// If the device in question is marked marked as local_only
		// and is being used as the boot device or the mirror of a
		// boot device for a node, then that node may be unable to
		// access this device and hence, unable to boot. Contact your
		// authorized Sun service provider to determine whether a
		// workaround or patch is available.
		//
		(void) msg_handle.log(SC_SYSLOG_WARNING,
		    MESSAGE, "reservation warning(%s) - Unable to lookup "
		    "local_only flag for device %s.", command, dsk_path);
		return (false);
	}

	//
	// Traverse the list backwards since local_only is typically
	// the last property.
	//
	for (j = (int)property_seq->count - 1; j >= 0; j--) {
		if (strcmp("local_only",
		    (property_seq->properties)[j].name) == 0) {
			dcs_free_properties(property_seq);
			return (true);
		}
	}
	dcs_free_properties(property_seq);
	return (false);
}


//
// Create a list of cluster nodes which are not active.  The list is stored in
// an array of chararcter strings, with each element of length MAX_STR_LEN.  A
// pointer to the beginning of the array is returned and num_deads_nodes is set
// to the number of entries.  If an error occurs, NULL is returned and
// num_dead_nodes is set to 0.
//
static char*
show_dead_nodes(unsigned int *num_dead_nodes)
{
	unsigned int i;
	Environment ee;
	cmm::control_var ctrlp;
	cmm::cluster_state_t cluster_state;
	const char *nodename;
	char *name_array;
	unsigned int name_index = 0;

	*num_dead_nodes = 0;

	// get cluster membership
	ctrlp = cmm_ns::get_control();
	if (CORBA::is_nil(ctrlp)) {
		//
		// SCMSGS
		// @explanation
		// The device fencing program has suffered an internal error.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available. Copies of
		// /var/adm/messages from all nodes should be provided for
		// diagnosis. It may be possible to retry the failed
		// operation, depending on the nature of the error. If the
		// message specifies the 'node_join' transition, then this
		// node may be unable to access shared devices. If the failure
		// occurred during the 'release_shared_scsi2' transition, then
		// a node which was joining the cluster may be unable to
		// access shared devices. In either case, it may be possible
		// to reacquire access to shared devices by executing
		// '/usr/cluster/lib/sc/run_reserve -c node_join' on all
		// cluster nodes. If the failure occurred during the
		// 'make_primary' transition, then a device group has failed
		// to start on this node. If another node was available to
		// host the device group, then it should have been started on
		// that node. If desired, it might be possible to switch the
		// device group to this node by using the cldevicegroup
		// command. If no other node was available, then the device
		// group will not have been started. You can use the
		// cldevicegroup command to retry the attempt to start the
		// device group. If the failure occurred during the
		// 'primary_to_secondary' transition, then the shutdown or
		// switchover of a device group has failed. The desired action
		// may be retried.
		//
		(void) msg_handle.log(SC_SYSLOG_WARNING, MESSAGE,
		    "reservation fatal error(%s) - get_control() "
		    "failure", command);
		return (NULL);
		}
	ctrlp->get_cluster_state(cluster_state, ee);
	if (ee.exception()) {
		ee.clear();
		//
		// SCMSGS
		// @explanation
		// The device fencing program has suffered an internal error.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available. Copies of
		// /var/adm/messages from all nodes should be provided for
		// diagnosis. It may be possible to retry the failed
		// operation, depending on the nature of the error. If the
		// message specifies the 'node_join' transition, then this
		// node may be unable to access shared devices. If the failure
		// occurred during the 'release_shared_scsi2' transition, then
		// a node which was joining the cluster may be unable to
		// access shared devices. In either case, it may be possible
		// to reacquire access to shared devices by executing
		// '/usr/cluster/lib/sc/run_reserve -c node_join' on all
		// cluster nodes. If the failure occurred during the
		// 'make_primary' transition, then a device group has failed
		// to start on this node. If another node was available to
		// host the device group, then it should have been started on
		// that node. If desired, it might be possible to switch the
		// device group to this node by using the cldevicegroup
		// command. If no other node was available, then the device
		// group will not have been started. You can use the
		// cldevicegroup command to retry the attempt to start the
		// device group. If the failure occurred during the
		// 'primary_to_secondary' transition, then the shutdown or
		// switchover of a device group has failed. The desired action
		// may be retried.
		//
		(void) msg_handle.log(SC_SYSLOG_WARNING, MESSAGE,
		    "reservation fatal error(%s) - get_cluster_state() "
		    "exception", command);
		exit(1);
	}

	name_array = (char *)malloc(MAX_STR_LEN * NODEID_MAX);
	if (name_array == NULL)
		return (NULL);

	for (i = 0; i < NODEID_MAX + 1; i++) {
		if ((nodename = clconf_cluster_get_nodename_by_nodeid(cl,
		    (nodeid_t)i)) == NULL)
			// not a valid nodeid
			continue;
		if (cluster_state.members.members[i] == INCN_UNKNOWN) {
			(void) sprintf(&name_array[name_index * MAX_STR_LEN],
			    "%s:", nodename);
			name_index++;
		}
	}

	*num_dead_nodes = name_index;
	return (name_array);
}


// print the contents of a scsi-3 key
static void
print_scsi3_key(const mhioc_resv_key_t &key)
{
	unsigned int i;
	uint64_t keyval = 0;

	for (i = 0; i < MHIOC_RESV_KEY_SIZE; i++)
		keyval = (keyval << 8) + key.key[i];
	(void) printf("0x%llx\n", keyval);
}

// compare 2 scsi-3 keys, return 1 if equal, 0 if not
static bool
equal_scsi3_keys(const mhioc_resv_key_t &key1, const mhioc_resv_key_t &key2)
{
	unsigned int i;

	for (i = 0; i < MHIOC_RESV_KEY_SIZE; i++)
		if (key1.key[i] != key2.key[i])
			return (0);

	return (1);
}

//
// Return scsi-3 key which corresponds to specified node id.  Return -1 if
// unable to determine key.
//
static int
get_scsi3_key_by_nodeid(nodeid_t nodeid, mhioc_resv_key_t &resv_key)
{
	unsigned int i;

	for (i = 0; i < quor_table->num_nodes; i++)
		if (quor_table->nodelist[i].nid == nodeid) {
			resv_key = (*(mhioc_resv_key *)
			    (&(quor_table->nodelist[i].reservation_key)));
			return (0);
		}

	return (-1);
}


// scsi ioctl function prototypes
static int do_status(int);
static int do_enfailfast(int, int);
static int do_scsi2_release(int fd);
static int do_scsi2_tkown(int);
static int do_scsi3_registerandignorekey(int,
    const mhioc_registerandignorekey_t &);
static int do_scsi3_register(int, const mhioc_register_t &,
    const mhioc_inkeys_t &);
static int do_scsi3_reserve(int, mhioc_resv_desc_t);
static int do_scsi3_preemptandabort(int, mhioc_preemptandabort_t);
static int do_scsi3_inkeys(int, mhioc_inkeys_t, int);
static int do_scsi3_inresv(int, mhioc_inresvs_t);
static int do_scsi3_reserve_qualified_retry(int, mhioc_resv_desc_t,
    const mhioc_inresvs_t &);


// issue MHIOCSTATUS ioctl
static int
do_status(int fd)
{
	int i, error;
	int rt = NUM_RETRIES;

	i = ioctl(fd, MHIOCSTATUS);
	error = errno;
	while ((i == -1) && (rt--)) {
		//
		// SCMSGS
		// @explanation
		// The device fencing program has encountered errors while
		// trying to access a device. The failed operation will be
		// retried
		// @user_action
		// This is an informational message, no user action is needed.
		//
		(void) msg_handle.log(SC_SYSLOG_WARNING, MESSAGE,
		    "reservation warning(%s) - MHIOCSTATUS error(%d) will "
		    "retry in %d seconds", command, error, RETRY_SLEEP);
		(void) sleep(RETRY_SLEEP);
		i = ioctl(fd, MHIOCSTATUS);
		if (i != -1) {
			//
			// SCMSGS
			// @explanation
			// Informational message from reserve on ioctl success
			// during retry.
			// @user_action
			// No user action required.
			//
			(void) msg_handle.log(SC_SYSLOG_NOTICE, MESSAGE,
			    "reservation notice(%s) - MHIOCSTATUS success "
			    "during retry attempt: %d", command,
			    NUM_RETRIES - rt);
		}
		error = errno;
	}
	return (i);

}

// issue MHIOCENABLEFAILFAST ioctl
static int
do_enfailfast(int fd, int interval)
{
	int i, error;
	int rt = NUM_RETRIES;

	i = ioctl(fd, MHIOCENFAILFAST, &interval);
	error = errno;
	while ((i == -1) && (rt--)) {
		//
		// SCMSGS
		// @explanation
		// The device fencing program has encountered errors while
		// trying to access a device. The failed operation will be
		// retried
		// @user_action
		// This is an informational message, no user action is needed.
		//
		(void) msg_handle.log(SC_SYSLOG_WARNING, MESSAGE,
		    "reservation warning(%s) - MHIOCENFAILFAST error(%d) will "
		    "retry in %d seconds", command, error, RETRY_SLEEP);
		(void) sleep(RETRY_SLEEP);
		i = ioctl(fd, MHIOCENFAILFAST, &interval);
		if (i != -1) {
			//
			// SCMSGS
			// @explanation
			// Informational message from reserve on ioctl success
			// during retry.
			// @user_action
			// No user action required.
			//
			(void) msg_handle.log(SC_SYSLOG_NOTICE, MESSAGE,
			    "reservation notice(%s) - MHIOCENABLEFAILFAST "
			    "success during retry attempt: %d", command,
			    NUM_RETRIES - rt);
		}
		error = errno;
	}
	return (i);
}

// issue MHIOCRELEASE ioctl
static int
do_scsi2_release(int fd)
{
	int i, error;
	int rt = NUM_RETRIES;

	i = ioctl(fd, MHIOCRELEASE);
	error = errno;
	while ((i == -1) && (rt--)) {
		//
		// SCMSGS
		// @explanation
		// The device fencing program has encountered errors while
		// trying to access a device. The failed operation will be
		// retried
		// @user_action
		// This is an informational message, no user action is needed.
		//
		(void) msg_handle.log(SC_SYSLOG_WARNING, MESSAGE,
		    "reservation warning(%s) - MHIOCRELEASE error(%d) will "
		    "retry in %d seconds", command, error, RETRY_SLEEP);
		(void) sleep(RETRY_SLEEP);
		i = ioctl(fd, MHIOCRELEASE);
		if (i != -1) {
			//
			// SCMSGS
			// @explanation
			// Informational message from reserve on ioctl success
			// during retry.
			// @user_action
			// No user action required.
			//
			(void) msg_handle.log(SC_SYSLOG_NOTICE, MESSAGE,
			    "reservation notice(%s) - MHIOCRELEASE success "
			    "during retry attempt: %d", command,
			    NUM_RETRIES - rt);
		}
		error = errno;
	}
	return (i);
}

// issue MHIOCTKOWN ioctl
static int
do_scsi2_tkown(int fd)
{
	int i, error;
	int rt = NUM_RETRIES;

	i = ioctl(fd, MHIOCTKOWN, 0);
	error = errno;
	while ((i == -1) && (rt--)) {
		//
		// SCMSGS
		// @explanation
		// The device fencing program has encountered errors while
		// trying to access a device. The failed operation will be
		// retried
		// @user_action
		// This is an informational message, no user action is needed.
		//
		(void) msg_handle.log(SC_SYSLOG_WARNING, MESSAGE,
		    "reservation warning(%s) - MHIOCTKOWN error(%d) will "
		    "retry in %d seconds", command, error, RETRY_SLEEP);
		(void) sleep(RETRY_SLEEP);
		i = ioctl(fd, MHIOCTKOWN, 0);
		if (i != -1) {
			//
			// SCMSGS
			// @explanation
			// Informational message from reserve on ioctl success
			// during retry.
			// @user_action
			// No user action required.
			//
			(void) msg_handle.log(SC_SYSLOG_NOTICE, MESSAGE,
			    "reservation notice(%s) - MHIOCTKOWN success "
			    "during retry attempt: %d", command,
			    NUM_RETRIES - rt);
		}
		error = errno;
	}
	return (i);
}

// Issue MHIOCGRP_REGISTERANDIGNOREKEY ioctl
static int
do_scsi3_registerandignorekey(int fd, const mhioc_registerandignorekey_t &r)
{
	int i, error;
	int rt = NUM_RETRIES;

	i = ioctl(fd, MHIOCGRP_REGISTERANDIGNOREKEY, &r);
	error = errno;
	while ((i == -1) && (rt--)) {
		//
		// SCMSGS
		// @explanation
		// The device fencing program has encountered errors while
		// trying to access a device. The failed operation will be
		// retried
		// @user_action
		// This is an informational message, no user action is needed.
		//
		(void) msg_handle.log(SC_SYSLOG_WARNING, MESSAGE,
		    "reservation warning(%s) - MHIOCGRP_REGISTERANDIGNOREKEY "
		    " error(%d) will retry in %d seconds", command,
		    error, RETRY_SLEEP);
		(void) sleep(RETRY_SLEEP);
		i = ioctl(fd, MHIOCGRP_REGISTERANDIGNOREKEY, &r);
		if (i != -1) {
			//
			// SCMSGS
			// @explanation
			// Informational message from reserve on ioctl success
			// during retry.
			// @user_action
			// No user action required.
			//
			(void) msg_handle.log(SC_SYSLOG_NOTICE, MESSAGE,
			    "reservation notice(%s) - "
			    "MHIOCGRP_REGISTERANDIGNOREKEY success during "
			    "retry attempt: %d", command, NUM_RETRIES - rt);
		}
		error = errno;
	}
	return (i);
}

//
// Issue MHIOCGRP_REGISTER ioctl, trying all three versions of old key, namely
// valid SC3.0 key, null key, existing initiator keys.
//
static int
issue_scsi3_register(int fd, mhioc_register_t r, const mhioc_inkeys_t &keylist,
    int &error)
{
	int i;
	unsigned int j;

	i = ioctl(fd, MHIOCGRP_REGISTER, &r);
	if (i == -1) {
		// register w/mykey failed, not yet registered?
		bzero(r.oldkey.key, (size_t)MHIOC_RESV_KEY_SIZE);
		i = ioctl(fd, MHIOCGRP_REGISTER, &r);
		if (i == -1) {
			//
			// Register failed again, must be reg w/diff key
			// try to reg with each registered key.
			//
			if (do_scsi3_inkeys(fd, keylist, NUM_RETRIES) == -1)
				return (-1);
			for (j = 0; j < keylist.li->listlen; j++) {
				r.oldkey = keylist.li->list[j];
				i = ioctl(fd, MHIOCGRP_REGISTER, &r);
				error = errno;
				if (i == 0)
					break;
			}
		}
	}

	return (i);
}


// issue MHIOCGRP_REGISTER ioctl
static int
do_scsi3_register(int fd, const mhioc_register_t &r,
    const mhioc_inkeys_t &keylist)
{
	int i, error;
	int rt = NUM_RETRIES;

	i = issue_scsi3_register(fd, r, keylist, error);
	while ((i == -1) && (rt--)) {
		//
		// SCMSGS
		// @explanation
		// The device fencing program has encountered errors while
		// trying to access a device. The failed operation will be
		// retried
		// @user_action
		// This is an informational message, no user action is needed.
		//
		(void) msg_handle.log(SC_SYSLOG_WARNING, MESSAGE,
		    "reservation warning(%s) - MHIOCGRP_REGISTER error(%d) "
		    "will retry in %d seconds", command, error, RETRY_SLEEP);
		(void) sleep(RETRY_SLEEP);
		i = issue_scsi3_register(fd, r, keylist, error);
		if (i != -1) {
			//
			// SCMSGS
			// @explanation
			// Informational message from reserve on ioctl success
			// during retry.
			// @user_action
			// No user action required.
			//
			(void) msg_handle.log(SC_SYSLOG_NOTICE, MESSAGE,
			    "reservation notice(%s) - MHIOCGRP_REGISTER "
			    "success during retry attempt: %d", command,
			    NUM_RETRIES - rt);
		}
	}
	return (i);
}

// issue MHIOCGRP_RESERVE ioctl
static int
do_scsi3_reserve(int fd, mhioc_resv_desc_t r)
{
	int i, error;
	int rt = NUM_RETRIES;

	i = ioctl(fd, MHIOCGRP_RESERVE, &r);
	error = errno;
	while ((i == -1) && (rt--)) {
		//
		// SCMSGS
		// @explanation
		// The device fencing program has encountered errors while
		// trying to access a device. The failed operation will be
		// retried
		// @user_action
		// This is an informational message, no user action is needed.
		//
		(void) msg_handle.log(SC_SYSLOG_WARNING, MESSAGE,
		    "reservation warning(%s) - MHIOCGRP_RESERVE error(%d) will "
		    "retry in %d seconds", command, error, RETRY_SLEEP);
		(void) sleep(RETRY_SLEEP);
		i = ioctl(fd, MHIOCGRP_RESERVE, &r);
		if (i != -1) {
			//
			// SCMSGS
			// @explanation
			// Informational message from reserve on ioctl success
			// during retry.
			// @user_action
			// No user action required.
			//
			(void) msg_handle.log(SC_SYSLOG_NOTICE, MESSAGE,
			    "reservation notice(%s) - MHIOCGRP_RESERVE success "
			    "during retry attempt: %d", command,
			    NUM_RETRIES - rt);
		}
		error = errno;
	}
	return (i);
}

// issue MHIOCGRP_PREEMPTANDABORTE ioctl
static int
do_scsi3_preemptandabort(int fd, mhioc_preemptandabort_t r)
{
	int i, error;
	int rt = NUM_RETRIES;
	unsigned int j;

	i = ioctl(fd, MHIOCGRP_PREEMPTANDABORT, &r);
	error = errno;
	while ((i == -1) && (rt--)) {

		//
		// Check if retcode is Reservation Conflict (EACCES),
		// this could be returned from the device if we are
		// attempting to preempt a victimkey who has already
		// been preempted by an earlier call.
		//
		// If we get an EACCES, check the keys on the disk,
		// if the victim key is not there, return success.
		//
		if (error == EACCES) {
			mhioc_inkeys_t		inkeys;
			mhioc_key_list_t	keylist;
			int			found_my_key = 0;
			int			found_victim_key = 0;
			int			new_error;

			inkeys.li = (mhioc_key_list_t *)&keylist;
			keylist.listsize = NODEID_MAX;
			keylist.list = (mhioc_resv_key_t *)malloc(
				sizeof (mhioc_resv_key_t) * keylist.listsize);

			if ((new_error =
				do_scsi3_inkeys(fd, inkeys, NUM_RETRIES))
					!= 0) {
				//
				// SCMSGS
				// @explanation
				// The device fencing program has encountered
				// errors while trying to access a device. All
				// retry attempts have failed.
				// @user_action
				// This may be indicative of a hardware
				// problem, which should be resolved as soon
				// as possible. Once the problem has been
				// resolved, the following actions may be
				// necessary: If the message specifies the
				// 'node_join' transition, then this node may
				// be unable to access the specified device.
				// If the failure occurred during the
				// 'release_shared_scsi2' transition, then a
				// node which was joining the cluster may be
				// unable to access the device. In either
				// case, access can be reacquired by executing
				// '/usr/cluster/lib/sc/run_reserve -c
				// node_join' on all cluster nodes. If the
				// failure occurred during the 'make_primary'
				// transition, then a device group might have
				// failed to start on this node. If the device
				// group was started on another node, move it
				// to this node by using the cldevicegroup
				// command. If the device group was not
				// started, you can start it by using the
				// cldevicegroup command. If the failure
				// occurred during the 'primary_to_secondary'
				// transition, then the shutdown or switchover
				// of a device group might have failed. If so,
				// the desired action may be retried.
				//
				(void) msg_handle.log(SC_SYSLOG_WARNING,
				    MESSAGE, "reservation warning(%s) - "
				    "MHIOCGRP_PREEMPTANDABORT error in "
				    "MHIOCGRP_INKEYS, error %d ",
				    command, new_error);
			} else {
				//
				// Check if my key is present AND victim's key
				// is absent. If not, the preempt has failed.
				//
				for (j = 0; j < keylist.listlen; j++) {
					if (equal_scsi3_keys(keylist.list[j],
					    r.victim_key)) {
						found_victim_key = 1;
						// no need to search further
						break;
					} else if (equal_scsi3_keys(
					    keylist.list[j], r.resvdesc.key))
							found_my_key = 1;
				}
			}

			free(keylist.list);

			if (found_my_key && !found_victim_key)
				//
				// The command worked. Ignore the error.
				//
				return (0);

		} // if EACCES

		//
		// SCMSGS
		// @explanation
		// The device fencing program has encountered errors while
		// trying to access a device. The failed operation will be
		// retried
		// @user_action
		// This is an informational message, no user action is needed.
		//
		(void) msg_handle.log(SC_SYSLOG_WARNING, MESSAGE,
		    "reservation warning(%s) - MHIOCGRP_PREEMPTANDABORT error"
		    "(%d) will retry in %d seconds", command, error,
		    RETRY_SLEEP);
		(void) sleep(RETRY_SLEEP);
		i = ioctl(fd, MHIOCGRP_PREEMPTANDABORT, &r);
		if (i != -1) {
			//
			// SCMSGS
			// @explanation
			// Informational message from reserve on ioctl success
			// during retry.
			// @user_action
			// No user action required.
			//
			(void) msg_handle.log(SC_SYSLOG_NOTICE, MESSAGE,
			    "reservation notice(%s) - MHIOCGRP_PREEMPTANDABORT "
			    "success during retry attempt: %d", command,
			    NUM_RETRIES - rt);
		}
		error = errno;
	}
	return (i);
}

// issue MHIOCGRP_INKEYS ioctl
static int
do_scsi3_inkeys(int fd, mhioc_inkeys_t keylist, int retries)
{
	int i, error;
	int rt = retries;

	i = ioctl(fd, MHIOCGRP_INKEYS, &keylist);
	error = errno;
	while ((i == -1) && (rt--)) {
		//
		// SCMSGS
		// @explanation
		// The device fencing program has encountered errors while
		// trying to access a device. The failed operation will be
		// retried
		// @user_action
		// This is an informational message, no user action is needed.
		//
		(void) msg_handle.log(SC_SYSLOG_WARNING, MESSAGE,
		    "reservation warning(%s) - MHIOCGRP_INKEYS error(%d) will "
		    "retry in %d seconds", command, error, RETRY_SLEEP);
		(void) sleep(RETRY_SLEEP);
		i = ioctl(fd, MHIOCGRP_INKEYS, &keylist);
		if (i != -1) {
			//
			// SCMSGS
			// @explanation
			// Informational message from reserve on ioctl success
			// during retry.
			// @user_action
			// No user action required.
			//
			(void) msg_handle.log(SC_SYSLOG_NOTICE, MESSAGE,
			    "reservation notice(%s) - MHIOCGRP_INKEYS success "
			    "during retry attempt: %d", command, retries - rt);
		}
		error = errno;
	}

	if (i == -1)
		return (i);

	rt = retries;
	// make sure our key list was big enough
	if (keylist.li->listlen > keylist.li->listsize) {
		if ((keylist.li->list = (mhioc_resv_key *)
		    realloc(keylist.li->list, keylist.li->listlen *
		    sizeof (mhioc_resv_key_t))) == NULL) {
			//
			// SCMSGS
			// @explanation
			// The device fencing program has been unable to
			// allocate required memory.
			// @user_action
			// Memory usage should be monitored on this node and
			// steps taken to provide more available memory if
			// problems persist. Once memory has been made
			// available, the following steps may need to taken:
			// If the message specifies the 'node_join'
			// transition, then this node may be unable to access
			// shared devices. If the failure occurred during the
			// 'release_shared_scsi2' transition, then a node
			// which was joining the cluster may be unable to
			// access shared devices. In either case, access to
			// shared devices can be reacquired by executing
			// '/usr/cluster/lib/sc/run_reserve -c node_join' on
			// all cluster nodes. If the failure occurred during
			// the 'make_primary' transition, then a device group
			// has failed to start on this node. If another node
			// was available to host the device group, then it
			// should have been started on that node. The device
			// group can be switched back to this node if desired
			// by using the cldevicegroup command. If no other
			// node was available, then the device group will not
			// have been started. Use the cldevicegroup command to
			// start the device group. If the failure occurred
			// during the 'primary_to_secondary' transition, then
			// the shutdown or switchover of a device group has
			// failed. You can retry the desired action.
			//
			(void) msg_handle.log(SC_SYSLOG_WARNING, MESSAGE,
			    "reservation fatal error(%s) - realloc() error, "
			    "errno %d", command, errno);
			exit(1);
		}
		keylist.li->listsize = keylist.li->listlen;
		i = ioctl(fd, MHIOCGRP_INKEYS, &keylist);
		error = errno;
		while ((i == -1) && (rt--)) {
			(void) msg_handle.log(SC_SYSLOG_WARNING, MESSAGE,
			    "reservation warning(%s) - MHIOCGRP_INKEYS error"
			    "(%d) will retry in %d seconds", command, error,
			    RETRY_SLEEP);
			(void) sleep(RETRY_SLEEP);
			i = ioctl(fd, MHIOCGRP_INKEYS, &keylist);
			if (i != -1) {
				(void) msg_handle.log(SC_SYSLOG_NOTICE, MESSAGE,
				    "reservation notice(%s) - MHIOCGRP_INKEYS "
				    "success during retry attempt: %d", command,
				    retries - rt);
			}
			error = errno;
		}
	}
	return (i);
}

// issue MHIOCGRP_INRESV ioctl
static int
do_scsi3_inresv(int fd, mhioc_inresvs_t reslist)
{
	int i, error;
	int rt = NUM_RETRIES;

	i = ioctl(fd, MHIOCGRP_INRESV, &reslist);
	error = errno;
	while ((i == -1) && (rt--)) {
		//
		// SCMSGS
		// @explanation
		// The device fencing program has encountered errors while
		// trying to access a device. The failed operation will be
		// retried
		// @user_action
		// This is an informational message, no user action is needed.
		//
		(void) msg_handle.log(SC_SYSLOG_WARNING, MESSAGE,
		    "reservation warning(%s) - MHIOCGRP_INRESV error(%d) will "
		    "retry in %d seconds", command, error, RETRY_SLEEP);
		(void) sleep(RETRY_SLEEP);
		i = ioctl(fd, MHIOCGRP_INRESV, &reslist);
		if (i != -1) {
			//
			// SCMSGS
			// @explanation
			// Informational message from reserve on ioctl success
			// during retry.
			// @user_action
			// No user action required.
			//
			(void) msg_handle.log(SC_SYSLOG_NOTICE, MESSAGE,
			    "reservation notice(%s) - MHIOCGRP_INRESV success "
			    "during retry attempt: %d", command,
			    NUM_RETRIES - rt);
		}
		error = errno;
	}

	if (i == -1)
		return (i);

	rt = NUM_RETRIES;
	// make sure our reservation list was big enough
	if (reslist.li->listlen > reslist.li->listsize) {
		if ((reslist.li->list = (mhioc_resv_desc *)
		    realloc(reslist.li->list, reslist.li->listlen *
		    sizeof (mhioc_resv_desc_t))) == NULL) {
			(void) msg_handle.log(SC_SYSLOG_WARNING, MESSAGE,
			    "reservation fatal error(%s) - realloc() error, "
			    "errno %d", command, errno);
			exit(1);
		}
		reslist.li->listsize = reslist.li->listlen;
		i = ioctl(fd, MHIOCGRP_INRESV, &reslist);
		error = errno;
		while ((i == -1) && (rt--)) {
			(void) msg_handle.log(SC_SYSLOG_WARNING, MESSAGE,
			    "reservation warning(%s) - MHIOCGRP_INRESV error"
			    "(%d) will retry in %d seconds", command, error,
			    RETRY_SLEEP);
			(void) sleep(RETRY_SLEEP);
			i = ioctl(fd, MHIOCGRP_INRESV, &reslist);
			if (i != -1) {
				(void) msg_handle.log(SC_SYSLOG_NOTICE, MESSAGE,
				    "reservation notice(%s) - MHIOCGRP_INRESV "
				    "success during retry attempt: %d", command,
				    NUM_RETRIES - rt);
			}
			error = errno;
		}
	}

	return (i);
}

//
// Return 1 if MHIOCGRP_INRESV shows a reservation with this node's key,
// 0 if not, -1 if error.
//
static int
scsi3_resv_exists(int fd, const mhioc_inresvs_t &reslist)
{
	unsigned int i;

	if (do_scsi3_inresv(fd, reslist) != 0)
		return (-1);

	if (reslist.li->listlen == 0)
		return (0);

	for (i = 0; i < reslist.li->listlen; i++)
		if (equal_scsi3_keys(mykey, reslist.li->list[i].key) == 1)
			return (1);

	return (0);
}

//
// Issue MHIOCGRP_RESERVE ioctl, retry only if there is no reservation in place.
// Returns success even if ioctl fails, if a reservation appears on the device
// using this node's key.
// This is done to handle the multi-pathing case in which the RESERVE is
// placed on the disk, but we get back a failure for the ioctl (perhaps because
// a path failed at just the right time).  If the mp driver has failed us over
// to an alternate path, the retrying the RESERVE ioclt should fail (unless the
// device supports the older spec which allows multiple reservations).  But, we
// don't care as long as we have placed the reservation on the disk.
//
static int
do_scsi3_reserve_qualified_retry(int fd, mhioc_resv_desc_t r,
    const mhioc_inresvs_t &reslist)
{
	int i, error;
	int rt = NUM_RETRIES;

	i = ioctl(fd, MHIOCGRP_RESERVE, &r);
	error = errno;
	while ((i == -1) && (rt--)) {
		if (scsi3_resv_exists(fd, reslist) == 1)
			return (0);
		(void) msg_handle.log(SC_SYSLOG_WARNING, MESSAGE,
		    "reservation warning(%s) - MHIOCGRP_RESERVE error(%d) will "
		    "retry in %d seconds", command, error, RETRY_SLEEP);
		(void) sleep(RETRY_SLEEP);
		i = ioctl(fd, MHIOCGRP_RESERVE, &r);
		if (i != -1) {
			(void) msg_handle.log(SC_SYSLOG_NOTICE, MESSAGE,
			    "reservation notice(%s) - MHIOCGRP_RESERVE success "
			    "during retry attempt: %d", command,
			    NUM_RETRIES - rt);
		}
		error = errno;
	}
	return (i);
}




// prints out usage message for debug code paths
static void
print_usage()
{
	(void) printf("Usage:\n"
	    "reserve -c [status | enfailfast | disfailfast | "
	    "tkown | release | scrub | inkeys | inresv | register | reserve | "
	    "unregister | preempt -v victim_key] -z disk_path\n");
	(void) printf("reserve -c [grab_lock | release_lock | check_lock] "
	    "-n lock_name\n");
}


// check quorum table to see if a disk is a quorum device
static bool
is_quorum(char *disk)
{
	unsigned int j;

	for (j = 0; j < quor_table->num_quorum_devices; j++) {
		if (strcmp(disk,
		    quor_table->quorum_device_list[j].gdevname) == 0)
			return (true);
	}

	return (false);
}






//
// Checks to see if the cluster id portions of two scsi-3 keys match.  It is
// assumed the cluster id is the first half of the scsi-3 key.
// Returns true if they match, false if they do not.
//
static bool
correct_cluster_key_id(const mhioc_resv_key_t &key1,
    const mhioc_resv_key_t &key2)
{
	bool match = true;
	int i;

	// cxx can we use memcmp here?
	for (i = 0; i < MHIOC_RESV_KEY_SIZE / 2; i++)
		if (key1.key[i] != key2.key[i]) {
			match = false;
			break;
		}

	return (match);
}


//
// Convert a reservation key to a nodeid.
// Requires that the second half of the reservation key contain the nodeid.
//
static nodeid_t
key_to_nodeid(const mhioc_resv_key_t &key)
{
	nodeid_t keynode = 0;
	int i;

	// the second half of the key is the node id
	for (i = (MHIOC_RESV_KEY_SIZE / 2); i < MHIOC_RESV_KEY_SIZE; i++)
		keynode = (keynode << 8) + key.key[i];

	return (keynode);
}


//
// Determine if a disk should be sent scsi-2 or scsi-3 ioctls based on the
// per disk fencing setting and the global setting. The per disk setting takes
// precedence if it has been set.
//
static bool
is_scsi3_disk(disk_list_t *diskp)
{
	int num_paths = 0;
	int cur_setting;
	int i;
	bool isscsi3disk = false;

	ASSERT(global_fencing_status != GLOBAL_FENCING_UNKNOWN);

	//
	// The global values map to the per disk value so they are
	// interchangable.
	//
	if (diskp->default_fencing == FENCING_USE_GLOBAL) {
		cur_setting = global_fencing_status;
	} else {
		cur_setting = diskp->default_fencing;
	}

	switch (cur_setting) {
	case GLOBAL_FENCING_PREFER3:
		if (diskp->detected_fencing == SCSI2_FENCING) {
			isscsi3disk = false;
		} else {
			/*
			 * XXX Currently, detected_fencing can only
			 * be set to SCSI2_FENCING or FENCING_NA
			 * because we have no good way of telling
			 * if a device supports scsi3 or not.
			 * So if the global or per disk setting
			 * is prefer3 or scsi3 and the detected_fencing
			 * is not SCSI2_FENCING, we are leaving it
			 * to users' best judgement and honor their
			 * scsi3 override request.
			 *
			 * Once we have a way of detecting scsi3
			 * device, we will differentiate
			 * SCSI3_FENCING and FENCNG_NA. Most likely
			 * use pathcount if the detected_fencing
			 * is FENCING_NA.
			 */
			ASSERT(diskp->detected_fencing == FENCING_NA);
			isscsi3disk = true;
		}
		break;
	case GLOBAL_FENCING_PATHCOUNT:
		//
		// We will use the old SCSI2-default way to determine the
		// fencing protocols by counting the number of attached
		// DID paths.
		//
		for (i = 0; i < (int)strlen(diskp->nodes); i++) {
			if (diskp->nodes[i] == ':') {
				num_paths++;
			}
		}

		if (num_paths < 3) {
			isscsi3disk = false;
		} else {
			isscsi3disk = true;
		}
		break;
	default:
		ASSERT(0);
	}
	return (isscsi3disk);
}

//
// Given a disk name, determines the status of the other node (if there
// is one) attached to the disk.  Should only be used for disks which are
// attached to at most 2 nodes.
// Return values: NODE_STATUS_NODE_UP	other node is part of the cluster
//		NODE_STATUS_NODE_DOWN	other node is not part of the cluster
//		NODE_STATUS_NO_NODE	no other node is attached to the disk
//		NODE_STATUS_ERROR	error occurred, node status undetermined
//
static node_status
other_node_status(disk_list_t *diskp)
{
	const char *nodename;
	char looking_for[2 * MAX_STR_LEN];
	unsigned int i;
	unsigned int nodeid_found = 0;

	for (i = 0; i < NODEID_MAX + 1; i++) {
		if (i == myid)
			continue;
		if ((nodename = clconf_cluster_get_nodename_by_nodeid(cl,
		    (nodeid_t)i)) == NULL)
			// not a valid nodeid
			continue;

		(void) sprintf(looking_for, "%s:", nodename);
		if (strstr(diskp->nodes, looking_for) != NULL) {
			//
			// Found a connected node. Save the
			// nodeid value so that we can return
			// its status.
			//
			nodeid_found = i;
			break;
		}
	}

	//
	// If nodeid_found is set then we found a node that shares
	// our device.  The value indicates the nodeid number that
	// we found and we simply return its status. Otherwise, no
	// other node is attached to this device and we return
	// back NODE_STATUS_NO_NODE.
	//
	if (nodeid_found) {
		if (clust_state.members.members[nodeid_found] == INCN_UNKNOWN)
			return (NODE_STATUS_NODE_DOWN);
		else
			return (NODE_STATUS_NODE_UP);
	}

	// there was no matching name in the disk list
	return (NODE_STATUS_NO_NODE);
}


//
// Removes scsi-3 keys from the specified disk.  Returns -1 if unable to get the
// list of keys from the disk or there are no keys, 0 otherwise.
//
static int
do_scrub(int fd, const mhioc_inkeys_t &keylist, mhioc_register_t reg,
    const mhioc_registerandignorekey_t &regign, mhioc_preemptandabort_t pre,
    bool interactive)
{
	unsigned int i;

	// get list of keys
	if (do_scsi3_inkeys(fd, keylist, 0) == -1) {
		if (interactive)
			(void) printf("Unable to get list of keys from disk\n");
		return (-1);
	}

	if (keylist.li->listlen == 0) {
		if (interactive)
			(void) printf("There are no keys on the disk to "
			    "remove\n");
		return (-1);
	}

	if (interactive) {
		// print out keys
		(void) printf("Reservation keys currently on disk:\n");
		for (i = 0; i < keylist.li->listlen; i++)
			print_scsi3_key(keylist.li->list[i]);
		(void) printf("Attempting to remove all keys from the "
		    "disk...\n");
	}

	// make sure this node is registered with the disk
	if (do_scsi3_registerandignorekey(fd, regign) == -1) {
		if (interactive)
			(void) printf("MHIOCGRP_REGISTERANDIGNOREKEY failed, "
			    "attempting MHIOCGRP_REGISTER\n");
		(void) do_scsi3_register(fd, reg, keylist);
	}

	// preempt everyone else's keys
	for (i = 0; i < keylist.li->listlen; i++) {
		if (memcmp((void *)reg.newkey.key,
		    (void *)keylist.li->list[i].key,
		    (size_t)MHIOC_RESV_KEY_SIZE) == 0)
			continue;
		pre.victim_key = keylist.li->list[i];
		(void) do_scsi3_preemptandabort(fd, pre);
	}

	// remove my key
	reg.oldkey = reg.newkey;
	bzero(reg.newkey.key, (size_t)MHIOC_RESV_KEY_SIZE);
	(void) do_scsi3_register(fd, reg, keylist);

	return (0);
}


// issue 'release_shared_scsi2' or 'node_join -x'  on all other cluster nodes
static void
remote_cluster_invocation(void *node_num)
{
	char *cmd;
	clconf_errnum_t err;

	if (node_join_scrubbing)
		cmd = node_join_scrubbing_cmd;
	else
		cmd = release_shared_scsi2_cmd;

	if ((err = clconf_do_execution(cmd, (nodeid_t)node_num, NULL, B_TRUE,
	    B_TRUE, B_TRUE)) != CL_NOERROR) {
		if (err == CL_NO_CLUSTER)
			//
			// SCMSGS
			// @explanation
			// A node which the device fencing program was
			// communicating with has left the cluster.
			// @user_action
			// This is an informational message, no user action is
			// needed.
			//
			(void) msg_handle.log(SC_SYSLOG_WARNING, MESSAGE,
			    "reservation error(%s) error. Node %d is not"
			    " in the cluster", command, node_num);
		else if (err == CL_INVALID_OBJ)
			//
			// SCMSGS
			// @explanation
			// The device fencing code was unable to cimmunicate
			// with another cluster node.
			// @user_action
			// Contact your authorized Sun service provider to
			// determine whether a workaround or patch is
			// available. Copies of /var/adm/messages from all
			// nodes should be provided for diagnosis. It may be
			// possible to retry the failed operation, depending
			// on the nature of the error. If the message
			// specifies the 'node_join' transition, then this
			// node may be unable to access shared devices. If the
			// failure occurred during the 'release_shared_scsi2'
			// transition, then a node which was joining the
			// cluster may be unable to access shared devices. In
			// either case, it may be possible to reacquire access
			// to shared devices by executing
			// '/usr/cluster/lib/sc/run_reserve -c node_join' on
			// all cluster nodes. If the failure occurred during
			// the 'make_primary' transition, then a device group
			// has failed to start on this node. If another node
			// was available to host the device group, then it
			// should have been started on that node. If desired,
			// it might be possible to switch the device group to
			// this node by using the cldevicegroup command. If no
			// other node was available, then the device group
			// will not have been started. You can use the
			// cldevicegroup command to retry the attempt to start
			// the device group. If the failure occurred during
			// the 'primary_to_secondary' transition, then the
			// shutdown or switchover of a device group has
			// failed. The desired action may be retried.
			//
			(void) msg_handle.log(SC_SYSLOG_WARNING, MESSAGE,
			    "reservation error(%s) error. Not found clexecd"
			    " on node %d.", command, node_num);
		else
			//
			// SCMSGS
			// @explanation
			// The device fencing program has suffered an internal
			// error.
			// @user_action
			// Contact your authorized Sun service provider to
			// determine whether a workaround or patch is
			// available. Copies of /var/adm/messages from all
			// nodes should be provided for diagnosis. It may be
			// possible to retry the failed operation, depending
			// on the nature of the error. If the message
			// specifies the 'node_join' transition, then this
			// node may be unable to access shared devices. If the
			// failure occurred during the 'release_shared_scsi2'
			// transition, then a node which was joining the
			// cluster may be unable to access shared devices. In
			// either case, it may be possible to reacquire access
			// to shared devices by executing
			// '/usr/cluster/lib/sc/run_reserve -c node_join' on
			// all cluster nodes. If the failure occurred during
			// the 'make_primary' transition, then a device group
			// has failed to start on this node. If another node
			// was available to host the device group, then it
			// should have been started on that node. If desired,
			// it might be possible to switch the device group to
			// this node by using the cldevicegroup command. If no
			// other node was available, then the device group
			// will not have been started. You can use the
			// cldevicegroup command to retry the attempt to start
			// the device group. If the failure occurred during
			// the 'primary_to_secondary' transition, then the
			// shutdown or switchover of a device group has
			// failed. The desired action may be retried.
			//
			(void) msg_handle.log(SC_SYSLOG_WARNING, MESSAGE,
			    "reservation error(%s) error. Unknown internal"
			    " error returned from clconf_do_execution().",
			    command, node_num);
	}
}



//
// Release any scsi-2 reservations held on these disks. Since scsi-2 is
// restricted to 2 node access and the joining node is attached to them, if we
// are also attached to a disk, we know that no third-party node is attached.
// The memory for the disk argument must be freed in this routinue.
//
static void
release_shared_scsi2_doit(void *diskp)
{
	int fd;
	int rt = NUM_RETRIES;
	char *disk;

	disk = ((disk_list_t *)diskp)->did_path;

	fd = open((char *)disk, O_RDONLY|O_NDELAY);
	while ((fd == -1) && (rt--)) {
		//
		// SCMSGS
		// @explanation
		// The device fencing program has encountered errors while
		// trying to access a device. The failed operation will be
		// retried
		// @user_action
		// This is an informational message, no user action is needed.
		//
		(void) msg_handle.log(SC_SYSLOG_WARNING, MESSAGE,
		    "reservation warning(%s) - Unable to open device %s, will "
		    "retry in %d seconds", command, (char *)disk, RETRY_SLEEP);
		(void) sleep(RETRY_SLEEP);
		fd = open((char *)disk, O_RDONLY|O_NDELAY);
	}

	if (fd == -1) {
		//
		// SCMSGS
		// @explanation
		// The device fencing program has encountered errors while
		// trying to access a device. All retry attempts have failed.
		// @user_action
		// This may be indicative of a hardware problem, which should
		// be resolved as soon as possible. Once the problem has been
		// resolved, the following actions may be necessary: If the
		// message specifies the 'node_join' transition, then this
		// node may be unable to access the specified device. If the
		// failure occurred during the 'release_shared_scsi2'
		// transition, then a node which was joining the cluster may
		// be unable to access the device. In either case, access can
		// be reacquired by executing '/usr/cluster/lib/sc/run_reserve
		// -c node_join' on all cluster nodes. If the failure occurred
		// during the 'make_primary' transition, then a device group
		// might have failed to start on this node. If the device
		// group was started on another node, move it to this node by
		// using the cldevicegroup command. If the device group was
		// not started, you can start it by using the cldevicegroup
		// command. If the failure occurred during the
		// 'primary_to_secondary' transition, then the shutdown or
		// switchover of a device group might have failed. If so, the
		// desired action may be retried.
		//
		(void) msg_handle.log(SC_SYSLOG_WARNING,
		    MESSAGE, "reservation error(%s) - Unable to open "
		    "device %s", command, (char *)disk);
		free(disk);
		return;
	}

	// XXX this check could be moved to main()
	if (!is_scsi3_disk((disk_list_t *)diskp)) {
	// scsi-2
		if (verbose)
			(void) printf("reservation - release_shared_scsi2 "
			"disk is scsi2...%s.\n", (char *)disk);

		if (do_scsi2_release(fd) == -1)
			//
			// SCMSGS
			// @explanation
			// The device fencing program has encountered errors
			// while trying to access a device. All retry attempts
			// have failed.
			// @user_action
			// The action which failed is a scsi-2 ioctl. These
			// can fail if there are scsi-3 keys on the disk. To
			// remove invalid scsi-3 keys from a device, use
			// 'scdidadm -R' to repair the disk (see scdidadm man
			// page for details). If there were no scsi-3 keys
			// present on the device, then this error is
			// indicative of a hardware problem, which should be
			// resolved as soon as possible. Once the problem has
			// been resolved, the following actions may be
			// necessary: If the message specifies the 'node_join'
			// transition, then this node may be unable to access
			// the specified device. If the failure occurred
			// during the 'release_shared_scsi2' transition, then
			// a node which was joining the cluster may be unable
			// to access the device. In either case, access can be
			// reacquired by executing
			// '/usr/cluster/lib/sc/run_reserve -c node_join' on
			// all cluster nodes. If the failure occurred during
			// the 'make_primary' transition, then a device group
			// might have failed to start on this node. If the
			// device group was started on another node, move it
			// to this node by using the cldevicegroup command. If
			// the device group was not started, Start it by using
			// the cldevicegroup command. If the failure occurred
			// during the 'primary_to_secondary' transition, then
			// the shutdown or switchover of a device group might
			// have failed. If so, you can retry the desired
			// action.
			//
			(void) msg_handle.log(SC_SYSLOG_WARNING,
			    MESSAGE, "reservation error(%s) - "
			    "do_scsi2_release() error for disk %s",
			    command, (char *)disk);
	}

	(void) close(fd);
	free(disk);
}

//
// Called by node_join_doit and reserve -c switch_device_scsi_protocol to
// scrub scsi3 key from the disk and setup the polling thread.
//
static void
setup_scsi2_disk(disk_list_t *diskp, int scrubonly)
{
	char *disk = diskp->did_path;
	int fd;
	mhioc_inkeys_t keylist;
	mhioc_key_list_t klist;
	mhioc_register_t reg;
	mhioc_registerandignorekey_t regign;
	mhioc_resv_desc_t res;
	mhioc_preemptandabort pre;
	int retval;
	int rt = NUM_RETRIES;
	int poll_period = SCSI2_POLL_PERIOD;

	fd = open(disk, O_RDONLY|O_NDELAY);
	while ((fd == -1) && (rt--)) {
		(void) msg_handle.log(SC_SYSLOG_WARNING, MESSAGE,
		    "reservation warning(%s) - Unable to open device %s, will "
		    "retry in %d seconds", command, disk, RETRY_SLEEP);
		(void) sleep(RETRY_SLEEP);
		fd = open(disk, O_RDONLY|O_NDELAY);
	}

	if (fd == -1) {
		(void) msg_handle.log(SC_SYSLOG_WARNING,
		    MESSAGE, "reservation error(%s) - Unable to open device %s",
		    command, disk);
		return;
	}

	if (rt < NUM_RETRIES) {
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		(void) msg_handle.log(SC_SYSLOG_WARNING, MESSAGE,
		    "reservation info(%s) - Successfully opened device %s after"
		    " retry", command, disk);
	}

	// setup scsi-3 variables
	keylist.li = &klist;
	keylist.li->list = (mhioc_resv_key *)
	    malloc(INIT_KEYS * sizeof (mhioc_resv_key_t));
	if (keylist.li->list == NULL) {
		//
		// SCMSGS
		// @explanation
		// The device fencing program has been unable to allocate
		// required memory.
		// @user_action
		// Memory usage should be monitored on this node and steps
		// taken to provide more available memory if problems persist.
		// Once memory has been made available, the following steps
		// may need to taken: If the message specifies the 'node_join'
		// transition, then this node may be unable to access shared
		// devices. If the failure occurred during the
		// 'release_shared_scsi2' transition, then a node which was
		// joining the cluster may be unable to access shared devices.
		// In either case, access to shared devices can be reacquired
		// by executing '/usr/cluster/lib/sc/run_reserve -c node_join'
		// on all cluster nodes. If the failure occurred during the
		// 'make_primary' transition, then a device group has failed
		// to start on this node. If another node was available to
		// host the device group, then it should have been started on
		// that node. The device group can be switched back to this
		// node if desired by using the cldevicegroup command. If no
		// other node was available, then the device group will not
		// have been started. Use the cldevicegroup command to start
		// the device group. If the failure occurred during the
		// 'primary_to_secondary' transition, then the shutdown or
		// switchover of a device group has failed. You can retry the
		// desired action.
		//
		(void) msg_handle.log(SC_SYSLOG_WARNING,
		    MESSAGE, "reservation fatal error(%s) - malloc() error, "
		    "errno %d", command, errno);
		exit(1);
	}
	keylist.li->listsize = INIT_KEYS;
	reg.oldkey = mykey;
	reg.newkey = mykey;
	reg.aptpl  = B_TRUE;
	regign.newkey = mykey;
	regign.aptpl = B_TRUE;
	res.key = mykey;
	res.type = SCSI3_RESV_WRITEEXCLUSIVEREGISTRANTSONLY;
	res.scope = SCSI3_SCOPE_LOGICALUNIT;
	res.scope_specific_addr = 0;
	pre.resvdesc = res;

	// scsi-2
	// check for, and remove, scsi-3 keys
	if (do_scrub(fd, keylist, reg, regign, pre, 0) == 0)
		//
		// SCMSGS
		// @explanation
		// The device fencing program has detected scsi-3 registration
		// keys on a a device which is not configured for scsi-3 PGR
		// use. The keys have been removed.
		// @user_action
		// This is an informational message, no user action is needed.
		//
		(void) msg_handle.log(SC_SYSLOG_WARNING, MESSAGE,
		    "reservation message(%s) - attempted removal of "
		    " scsi-3 keys from non-scsi-3 device %s", command, disk);

	if (scrubonly == 1)
		return;

	if ((retval = do_status(fd)) == -1) {
		//
		// SCMSGS
		// @explanation
		// The device fencing program has encountered errors while
		// trying to access a device. All retry attempts have failed.
		// @user_action
		// The action which failed is a scsi-2 ioctl. These can fail
		// if there are scsi-3 keys on the disk. To remove invalid
		// scsi-3 keys from a device, use 'scdidadm -R' to repair the
		// disk (see scdidadm man page for details). If there were no
		// scsi-3 keys present on the device, then this error is
		// indicative of a hardware problem, which should be resolved
		// as soon as possible. Once the problem has been resolved,
		// the following actions may be necessary: If the message
		// specifies the 'node_join' transition, then this node may be
		// unable to access the specified device. If the failure
		// occurred during the 'release_shared_scsi2' transition, then
		// a node which was joining the cluster may be unable to
		// access the device. In either case, access can be reacquired
		// by executing '/usr/cluster/lib/sc/run_reserve -c node_join'
		// on all cluster nodes. If the failure occurred during the
		// 'make_primary' transition, then a device group might have
		// failed to start on this node. If the device group was
		// started on another node, move it to this node by using the
		// cldevicegroup command. If the device group was not started,
		// Start it by using the cldevicegroup command. If the failure
		// occurred during the 'primary_to_secondary' transition, then
		// the shutdown or switchover of a device group might have
		// failed. If so, you can retry the desired action.
		//
		(void) msg_handle.log(SC_SYSLOG_WARNING,
		    MESSAGE, "reservation error(%s) - do_status() "
		    "error for disk %s",
		    command, disk);
		// Disable failfast
		poll_period = 0;
	} else if (retval == 1) {
		// don't have access, see if other node is in cluster
		if (verbose)
			(void) printf("reservation node_join status 1"
			    "...%s.\n", disk);

		if (other_node_status(diskp)
		    == NODE_STATUS_NODE_DOWN) {
			//
			// SCMSGS
			// @explanation
			// The device fencing program is taking access to the
			// specified device away from a non-cluster node.
			// @user_action
			// This is an informational message, no user action is
			// needed.
			//
			(void) msg_handle.log(
			    SC_SYSLOG_WARNING, MESSAGE,
			    "reservation message(%s) - Taking ownership"
			    " of disk %s away from non-cluster node",
			    command, disk);
			if (do_scsi2_tkown(fd) == -1) {
				//
				// SCMSGS
				// @explanation
				// The device fencing program has encountered
				// errors while trying to access a device. All
				// retry attempts have failed.
				// @user_action
				// The action which failed is a scsi-2 ioctl.
				// These can fail if there are scsi-3 keys on
				// the disk. To remove invalid scsi-3 keys
				// from a device, use 'scdidadm -R' to repair
				// the disk (see scdidadm man page for
				// details). If there were no scsi-3 keys
				// present on the device, then this error is
				// indicative of a hardware problem, which
				// should be resolved as soon as possible.
				// Once the problem has been resolved, the
				// following actions may be necessary: If the
				// message specifies the 'node_join'
				// transition, then this node may be unable to
				// access the specified device. If the failure
				// occurred during the 'release_shared_scsi2'
				// transition, then a node which was joining
				// the cluster may be unable to access the
				// device. In either case, access can be
				// reacquired by executing
				// '/usr/cluster/lib/sc/run_reserve -c
				// node_join' on all cluster nodes. If the
				// failure occurred during the 'make_primary'
				// transition, then a device group might have
				// failed to start on this node. If the device
				// group was started on another node, move it
				// to this node by using the cldevicegroup
				// command. If the device group was not
				// started, Start it by using the
				// cldevicegroup command. If the failure
				// occurred during the 'primary_to_secondary'
				// transition, then the shutdown or switchover
				// of a device group might have failed. If so,
				// you can retry the desired action.
				//
				(void) msg_handle.log(
				    SC_SYSLOG_WARNING, MESSAGE,
				    "reservation error(%s) - "
				    "do_scsi2_tkown() error for "
				    "disk %s", command, disk);
				// Disable failfast
				poll_period = 0;
			}
		} else
			//
			// SCMSGS
			// @explanation
			// The device fencing program has encountered errors
			// while trying to access a device.
			// @user_action
			// Another cluster node has fenced this node from the
			// specified device, preventing this node from
			// accessing that device. Access should have been
			// reacquired when this node joined the cluster, but
			// this must have experienced problems. If the message
			// specifies the 'node_join' transition, this node
			// will be unable to access the specified device. If
			// the failure occurred during the 'make_primary'
			// transition, then this will be unable to access the
			// specified device and a device group that contains
			// the specified device might have failed to start on
			// this node. An attempt can be made to acquire access
			// to the device by executing
			// '/usr/cluster/lib/sc/run_reserve -c node_join' on
			// this node. If a device group failed to start on
			// this node, the cldevicegroup command can be used to
			// start the device group on this node if access can
			// be reacquired. If the problem persists, contact
			// your authorized Sun service provider to determine
			// whether a workaround or patch is available.
			//
			(void) msg_handle.log(SC_SYSLOG_WARNING,
			    MESSAGE, "reservation error(%s) - Unable "
			    "to gain access to device '%s', errno %d",
			    command, disk, errno);
			// Disable failfast
			poll_period = 0;
	}

	if (do_enfailfast(fd, poll_period) == -1)
		//
		// SCMSGS
		// @explanation
		// The device fencing program has encountered errors while
		// trying to access a device. All retry attempts have failed.
		// @user_action
		// This may be indicative of a hardware problem, which should
		// be resolved as soon as possible. Once the problem has been
		// resolved, the following actions may be necessary: If the
		// message specifies the 'node_join' transition, then this
		// node may be unable to access the specified device. If the
		// failure occurred during the 'release_shared_scsi2'
		// transition, then a node which was joining the cluster may
		// be unable to access the device. In either case, access can
		// be reacquired by executing '/usr/cluster/lib/sc/run_reserve
		// -c node_join' on all cluster nodes. If the failure occurred
		// during the 'make_primary' transition, then a device group
		// might have failed to start on this node. If the device
		// group was started on another node, move it to this node by
		// using the cldevicegroup command. If the device group was
		// not started, you can start it by using the cldevicegroup
		// command. If the failure occurred during the
		// 'primary_to_secondary' transition, then the shutdown or
		// switchover of a device group might have failed. If so, the
		// desired action may be retried.
		//
		(void) msg_handle.log(SC_SYSLOG_WARNING,
		    MESSAGE, "reservation error(%s) - do_enfailfast() "
		    "error for disk %s", command, disk);

	// cleanup
	(void) close(fd);
	free(keylist.li->list);
}

//
// Called by node_join_doit and reserve -c switch_device_scsi_protocol
// to register scsi3 key on the disk and setup the polling thread.
//
static void
setup_scsi3_disk(disk_list_t *diskp)
{
	char *disk = diskp->did_path;
	int fd;
	mhioc_inkeys_t keylist;
	mhioc_key_list_t klist;
	mhioc_register_t reg;
	mhioc_registerandignorekey_t regign;
	int rt = NUM_RETRIES;
	int poll_period = SCSI3_POLL_PERIOD;

	fd = open(disk, O_RDONLY|O_NDELAY);
	while ((fd == -1) && (rt--)) {
		(void) msg_handle.log(SC_SYSLOG_WARNING, MESSAGE,
		    "reservation warning(%s) - Unable to open device %s, will "
		    "retry in %d seconds", command, disk, RETRY_SLEEP);
		(void) sleep(RETRY_SLEEP);
		fd = open(disk, O_RDONLY|O_NDELAY);
	}

	if (fd == -1) {
		(void) msg_handle.log(SC_SYSLOG_WARNING,
		    MESSAGE, "reservation error(%s) - Unable to open device %s",
		    command, disk);
		return;
	}

	if (rt < NUM_RETRIES) {
		(void) msg_handle.log(SC_SYSLOG_WARNING, MESSAGE,
		    "reservation info(%s) - Successfully opened device %s after"
		    " retry", command, disk);
	}

	// setup scsi-3 variables
	keylist.li = &klist;
	keylist.li->list = (mhioc_resv_key *)
	    malloc(INIT_KEYS * sizeof (mhioc_resv_key_t));
	if (keylist.li->list == NULL) {
		(void) msg_handle.log(SC_SYSLOG_WARNING,
		    MESSAGE, "reservation fatal error(%s) - malloc() error, "
		    "errno %d", command, errno);
		exit(1);
	}

	keylist.li->listsize = INIT_KEYS;
	reg.oldkey = mykey;
	reg.newkey = mykey;
	reg.aptpl  = B_TRUE;
	regign.newkey = mykey;
	regign.aptpl = B_TRUE;

	// scsi-3
	if (do_scsi3_inkeys(fd, keylist, NUM_RETRIES) == -1) {
		//
		// SCMSGS
		// @explanation
		// The device fencing program has encountered errors while
		// trying to access a device. All retry attempts have failed.
		// @user_action
		// This may be indicative of a hardware problem, which should
		// be resolved as soon as possible. Once the problem has been
		// resolved, the following actions may be necessary: If the
		// message specifies the 'node_join' transition, then this
		// node may be unable to access the specified device. If the
		// failure occurred during the 'release_shared_scsi2'
		// transition, then a node which was joining the cluster may
		// be unable to access the device. In either case, access can
		// be reacquired by executing '/usr/cluster/lib/sc/run_reserve
		// -c node_join' on all cluster nodes. If the failure occurred
		// during the 'make_primary' transition, then a device group
		// might have failed to start on this node. If the device
		// group was started on another node, move it to this node by
		// using the cldevicegroup command. If the device group was
		// not started, you can start it by using the cldevicegroup
		// command. If the failure occurred during the
		// 'primary_to_secondary' transition, then the shutdown or
		// switchover of a device group might have failed. If so, the
		// desired action may be retried.
		//
		(void) msg_handle.log(SC_SYSLOG_WARNING,
		    MESSAGE, "reservation error(%s) - "
		    "do_scsi3_inkeys() error "
		    "for disk %s", command, disk);
		// Disable failfast
		poll_period = 0;
		goto outt;
	}

	if (do_scsi3_registerandignorekey(fd, regign) == -1) {
		//
		// SCMSGS
		// @explanation
		// The device fencing program has encountered errors while
		// trying to access a device. Now trying to run
		// do_scsi3_register() This is an informational message, no
		// user action is needed
		// @user_action
		// This is an informational message, no user action is needed.
		//
		(void) msg_handle.log(SC_SYSLOG_WARNING, MESSAGE,
		    "reservation warning(%s) - do_scsi3_registerandignorekey() "
		    "error for disk %s, attempting do_scsi3_register()",
		    command, disk);
		if (do_scsi3_register(fd, reg, keylist) == -1) {
			//
			// SCMSGS
			// @explanation
			// The device fencing program has encountered errors
			// while trying to access a device. All retry attempts
			// have failed.
			// @user_action
			// This may be indicative of a hardware problem, which
			// should be resolved as soon as possible. Once the
			// problem has been resolved, the following actions
			// may be necessary: If the message specifies the
			// 'node_join' transition, then this node may be
			// unable to access the specified device. If the
			// failure occurred during the 'release_shared_scsi2'
			// transition, then a node which was joining the
			// cluster may be unable to access the device. In
			// either case, access can be reacquired by executing
			// '/usr/cluster/lib/sc/run_reserve -c node_join' on
			// all cluster nodes. If the failure occurred during
			// the 'make_primary' transition, then a device group
			// might have failed to start on this node. If the
			// device group was started on another node, move it
			// to this node by using the cldevicegroup command. If
			// the device group was not started, you can start it
			// by using the cldevicegroup command. If the failure
			// occurred during the 'primary_to_secondary'
			// transition, then the shutdown or switchover of a
			// device group might have failed. If so, the desired
			// action may be retried.
			//
			(void) msg_handle.log(SC_SYSLOG_WARNING, MESSAGE,
			    "reservation error(%s) - do_scsi3_register() error "
			    "for disk %s", command, disk);
			// Disable failfast
			poll_period = 0;
		}
	}

outt:

	if (do_enfailfast(fd, poll_period) == -1)
		(void) msg_handle.log(SC_SYSLOG_WARNING,
		    MESSAGE, "reservation error(%s) - do_enfailfast() "
		    "error for disk %s", command, disk);

	// cleanup
	(void) close(fd);
	free(keylist.li->list);
}

//
// Make sure this node can access all attached disks.  For scsi-3 disks, this
// simply means registering with the disk.  For scsi-2 disks, all cluster nodes
// have already been asked to release their scsi-2 reservations for these disk.
// If we still cannot access the scsi-2 disk, we check to see if the other node
// attached to the disk is in the cluster, if not we take the owenership away.
//
static void
node_join_doit(void *diskp)
{
	if (is_scsi3_disk((disk_list_t *)diskp)) {
		if (verbose) {
			(void) printf("reservation - node_join disk is "
			    "scsi3...%s.\n", ((disk_list_t *)diskp)->did_path);
		}

		setup_scsi3_disk((disk_list_t *)diskp);
	} else {
		if (verbose) {
			(void) printf("reservation - node_join disk is "
			    "scsi2...%s.\n", ((disk_list_t *)diskp)->did_path);
		}

		setup_scsi2_disk((disk_list_t *)diskp, 0);
	}
}

//
// Restrict access to disks in to cluster nodes only.  For scsi-3 disks,
// see which nodes are registered with the disk and preempt non-cluster
// nodes.  For scsi-2 disks, see if the other node attached to the disk (if
// there is one) is a member of the cluster.  If it is not, take ownership of
// the disk.  The memory for the disk argumenet should not be freed in this
// routinue.
//
static void
fence_all_nodes_doit(void *diskp)
{
	int fd;
	unsigned int j;
	node_status status;
	nodeid_t key_to_node;
	bool preempt;
	bool found_my_key = false;	// is my key on the (scsi-3) disk?
	mhioc_inkeys_t keylist;
	mhioc_key_list_t klist;
	mhioc_inresvs_t reslist;
	mhioc_resv_desc_list_t rlist;
	mhioc_resv_desc_t res;
	mhioc_preemptandabort pre;
	mhioc_resv_key_t victkey;
	int rt = NUM_RETRIES;
	char *disk;

	disk = ((disk_list_t *)diskp)->did_path;

	// setup scsi-3 variables
	keylist.li = &klist;
	keylist.li->list = (mhioc_resv_key *)
	    malloc(INIT_KEYS * sizeof (mhioc_resv_key_t));
	keylist.li->listsize = INIT_KEYS;
	reslist.li = &rlist;
	reslist.li->list = (mhioc_resv_desc *)
	    malloc(INIT_RESVS * sizeof (mhioc_resv_desc_t));
	reslist.li->listsize = INIT_RESVS;
	res.key = mykey;
	res.type = SCSI3_RESV_WRITEEXCLUSIVEREGISTRANTSONLY;
	res.scope = SCSI3_SCOPE_LOGICALUNIT;
	res.scope_specific_addr = 0;
	pre.resvdesc = res;
	if ((keylist.li->list == NULL) || (reslist.li->list == NULL)) {
		(void) msg_handle.log(SC_SYSLOG_WARNING,
		    MESSAGE, "reservation fatal error(%s) - malloc() error, "
		    "errno %d", command, errno);
		exit(1);
	}

	fd = open((char *)disk, O_RDONLY|O_NDELAY);
	while ((fd == -1) && (rt--)) {
		(void) msg_handle.log(SC_SYSLOG_WARNING, MESSAGE,
		    "reservation warning(%s) - Unable to open device %s, will "
		    "retry in %d seconds", command, (char *)disk, RETRY_SLEEP);
		(void) sleep(RETRY_SLEEP);
		fd = open((char *)disk, O_RDONLY|O_NDELAY);
	}

	if (fd == -1) {
		(void) msg_handle.log(SC_SYSLOG_WARNING,
		    MESSAGE, "reservation error(%s) - Unable to open "
		    "device %s", command, (char *)disk);
		free(disk);
		goto open_outt;
	}

	if (!is_scsi3_disk((disk_list_t *)diskp)) {
	// scsi-2
		if (verbose)
			(void) printf("reservation - fence_all_nodes disk is "
			    "scsi2...%s.\n", (char *)disk);

		if (do_status(fd) != 0) {
			//
			// SCMSGS
			// @explanation
			// The device fencing program has encountered errors
			// while trying to access a device.
			// @user_action
			// Another cluster node has fenced this node from the
			// specified device, preventing this node from
			// accessing that device. Access should have been
			// reacquired when this node joined the cluster, but
			// this must have experienced problems. If the message
			// specifies the 'node_join' transition, this node
			// will be unable to access the specified device. If
			// the failure occurred during the 'make_primary'
			// transition, then this will be unable to access the
			// specified device and a device group that contains
			// the specified device might have failed to start on
			// this node. An attempt can be made to acquire access
			// to the device by executing
			// '/usr/cluster/lib/sc/run_reserve -c node_join' on
			// this node. If a device group failed to start on
			// this node, the cldevicegroup command can be used to
			// start the device group on this node if access can
			// be reacquired. If the problem persists, contact
			// your authorized Sun service provider to determine
			// whether a workaround or patch is available.
			//
			(void) msg_handle.log(SC_SYSLOG_WARNING, MESSAGE,
			    "reservation error(%s) - Unable to gain access to "
			    "device '%s'", command, (char *)disk);
			goto outt;
		}

		//
		// If we are unable to determine the other node's status
		// we should fence, better safe than sorry.
		//
		status = other_node_status((disk_list_t *)diskp);
		if ((status == NODE_STATUS_NODE_DOWN) ||
		    (status == NODE_STATUS_ERROR)) {
			// other node is not up, take ownership of the disk

			//
			// SCMSGS
			// @explanation
			// The device fencing program is taking access to the
			// specified device away from a non-cluster node.
			// @user_action
			// This is an informational message, no user action is
			// needed.
			//
			(void) msg_handle.log(SC_SYSLOG_WARNING, MESSAGE,
			    "reservation message(%s) - Fencing other node "
			    "from disk %s", command, (char *)disk);
			if (do_scsi2_tkown(fd) == -1) {
				(void) msg_handle.log(
				    SC_SYSLOG_WARNING, MESSAGE,
				    "reservation error(%s) - do_scsi2_tkown() "
				    "error for disk %s",
				    command, (char *)disk);
				goto outt;
			}
		}
		goto outt;
	}

	// scsi-3
	if (verbose)
		(void) printf("reservation - fence_all_nodes disk is  scsi3"
		    "...%s.\n", (char *)disk);

	if (do_scsi3_inkeys(fd, keylist, NUM_RETRIES) == -1) {
		(void) msg_handle.log(SC_SYSLOG_WARNING, MESSAGE,
		    "reservation error(%s) - do_scsi3_inkeys() error "
		    "for disk %s", command, (char *)disk);
		goto outt;
	}

	for (j = 0; j < keylist.li->listlen; j++)
		if (memcmp(keylist.li->list[j].key, mykey.key,
		    (size_t)MHIOC_RESV_KEY_SIZE) == 0) {
			found_my_key = true;
			break;
		}
	if (!found_my_key) {
		(void) msg_handle.log(SC_SYSLOG_WARNING, MESSAGE,
		    "reservation error(%s) - Unable to gain access to "
		    "device '%s'", command, (char *)disk);
		goto outt;
	}

	if (do_scsi3_inresv(fd, reslist) == -1) {
		//
		// SCMSGS
		// @explanation
		// The device fencing program has encountered errors while
		// trying to access a device. All retry attempts have failed.
		// @user_action
		// This may be indicative of a hardware problem, which should
		// be resolved as soon as possible. Once the problem has been
		// resolved, the following actions may be necessary: If the
		// message specifies the 'node_join' transition, then this
		// node may be unable to access the specified device. If the
		// failure occurred during the 'release_shared_scsi2'
		// transition, then a node which was joining the cluster may
		// be unable to access the device. In either case, access can
		// be reacquired by executing '/usr/cluster/lib/sc/run_reserve
		// -c node_join' on all cluster nodes. If the failure occurred
		// during the 'make_primary' transition, then a device group
		// might have failed to start on this node. If the device
		// group was started on another node, move it to this node by
		// using the cldevicegroup command. If the device group was
		// not started, you can start it by using the cldevicegroup
		// command. If the failure occurred during the
		// 'primary_to_secondary' transition, then the shutdown or
		// switchover of a device group might have failed. If so, the
		// desired action may be retried.
		//
		(void) msg_handle.log(SC_SYSLOG_WARNING, MESSAGE,
		    "reservation error(%s) - do_scsi3_inresv() error "
		    "for disk %s", command, (char *)disk);
		goto outt;
	}
	if (reslist.li->listlen == 0) {
		// no-one has a reservation, issue one
		if (verbose)
			(void) printf("reservation - fence_all_nodes no scsi-3 "
			    "resvervation in place...%s.\n", (char *)disk);

		if (do_scsi3_reserve_qualified_retry(fd, res, reslist) == -1) {
			//
			// SCMSGS
			// @explanation
			// The device fencing program has encountered errors
			// while trying to access a device. All retry attempts
			// have failed.
			// @user_action
			// This may be indicative of a hardware problem, which
			// should be resolved as soon as possible. Once the
			// problem has been resolved, the following actions
			// may be necessary: If the message specifies the
			// 'node_join' transition, then this node may be
			// unable to access the specified device. If the
			// failure occurred during the 'release_shared_scsi2'
			// transition, then a node which was joining the
			// cluster may be unable to access the device. In
			// either case, access can be reacquired by executing
			// '/usr/cluster/lib/sc/run_reserve -c node_join' on
			// all cluster nodes. If the failure occurred during
			// the 'make_primary' transition, then a device group
			// might have failed to start on this node. If the
			// device group was started on another node, move it
			// to this node by using the cldevicegroup command. If
			// the device group was not started, you can start it
			// by using the cldevicegroup command. If the failure
			// occurred during the 'primary_to_secondary'
			// transition, then the shutdown or switchover of a
			// device group might have failed. If so, the desired
			// action may be retried.
			//
			(void) msg_handle.log(
			    SC_SYSLOG_WARNING, MESSAGE,
			    "reservation error(%s) - do_scsi3_reserve() error "
			    "for disk %s", command, (char *)disk);
			goto outt;
		}
	}

	// look for our key
	for (j = 0; j < keylist.li->listlen; j++) {
		//
		// Preempt keys with invalid cluster id and keys belonging to
		// non-cluster nodes.  Preempting keys with invalid cluster ids
		// is done to handle unused initiators with invalid keys.
		// Used initiators will have any invalid keys replaced with
		// valid keys when the node using that initiator joins the
		// cluster.
		//
		preempt = 0;
		key_to_node = key_to_nodeid(keylist.li->list[j]);

		if ((key_to_node > NODEID_MAX) ||
		    (!correct_cluster_key_id(mykey, keylist.li->list[j]))) {
			// this is a bad key, go ahead and preempt it

			//
			// SCMSGS
			// @explanation
			// The device fencing program has discovered an
			// invalid scsi-3 key on the specified device and is
			// removing it.
			// @user_action
			// This is an informational message, no user action is
			// needed.
			//
			(void) msg_handle.log(SC_SYSLOG_WARNING, MESSAGE,
			    "reservation warning(%s) - Found invalid key, "
			    "preempting", command);
			preempt = 1;
		} else if (clust_state.members.members[key_to_node] ==
		    INCN_UNKNOWN)
			// node is not in the cluster, preempt it
			preempt = 1;

		if (preempt) {
			// registered node is not in the cluster
			bzero(victkey.key, (size_t)MHIOC_RESV_KEY_SIZE);
			(void) memcpy((char *)victkey.key,
			    (char *)keylist.li->list[j].key,
			    (size_t)MHIOC_RESV_KEY_SIZE);
			//
			// SCMSGS
			// @explanation
			// The device fencing program is taking access to the
			// specified device away from a non-cluster node.
			// @user_action
			// This is an informational message, no user action is
			// needed.
			//
			(void) msg_handle.log(SC_SYSLOG_WARNING, MESSAGE,
			    "reservation message(%s) - Fencing node %d from "
			    "disk %s", command, key_to_node, (char *)disk);
			pre.victim_key = victkey;

			if (verbose) {
				(void) printf("preempting key from disk %s - ",
				    (char *)disk);
				print_scsi3_key(victkey);
			}

			if (do_scsi3_preemptandabort(fd, pre) == -1) {
				//
				// SCMSGS
				// @explanation
				// The device fencing program has encountered
				// errors while trying to access a device. All
				// retry attempts have failed.
				// @user_action
				// This may be indicative of a hardware
				// problem, which should be resolved as soon
				// as possible. Once the problem has been
				// resolved, the following actions may be
				// necessary: If the message specifies the
				// 'node_join' transition, then this node may
				// be unable to access the specified device.
				// If the failure occurred during the
				// 'release_shared_scsi2' transition, then a
				// node which was joining the cluster may be
				// unable to access the device. In either
				// case, access can be reacquired by executing
				// '/usr/cluster/lib/sc/run_reserve -c
				// node_join' on all cluster nodes. If the
				// failure occurred during the 'make_primary'
				// transition, then a device group might have
				// failed to start on this node. If the device
				// group was started on another node, move it
				// to this node by using the cldevicegroup
				// command. If the device group was not
				// started, you can start it by using the
				// cldevicegroup command. If the failure
				// occurred during the 'primary_to_secondary'
				// transition, then the shutdown or switchover
				// of a device group might have failed. If so,
				// the desired action may be retried.
				//
				(void) msg_handle.log(SC_SYSLOG_WARNING,
				    MESSAGE, "reservation error(%s) - "
				    "do_scsi3_preemptandabort() error "
				    "for disk %s", command, (char *)disk);
				goto outt;
			}
		}
	}

outt:

	(void) close(fd);

open_outt:

	// cleanup
	free(keylist.li->list);
	free(reslist.li->list);
}



//
// Fence a specific node from the specified device.
// The memory for the disk argument must be freed in this routinue.
//
static void
fence_node_doit(void *diskp)
{
	unsigned int j;
	bool found_my_key = false;	// is my key on the (scsi-3) disk?
	int fd;
	int rt = NUM_RETRIES;
	mhioc_inkeys_t keylist;
	mhioc_key_list_t klist;
	mhioc_inresvs_t reslist;
	mhioc_resv_desc_list_t rlist;
	mhioc_resv_desc_t res;
	mhioc_preemptandabort pre;
	mhioc_resv_key_t victkey;
	char *disk;

	disk = ((disk_list_t *)diskp)->did_path;

	(void) msg_handle.log(SC_SYSLOG_WARNING, MESSAGE,
	    "reservation message(%s) - Fencing node %d from disk %s",
	    command, fence_node_id, (char *)disk);

	// setup scsi-3 variables
	keylist.li = &klist;
	keylist.li->list = (mhioc_resv_key *)
	    malloc(INIT_KEYS * sizeof (mhioc_resv_key_t));
	keylist.li->listsize = INIT_KEYS;
	reslist.li = &rlist;
	reslist.li->list = (mhioc_resv_desc *)
	    malloc(INIT_RESVS * sizeof (mhioc_resv_desc_t));
	reslist.li->listsize = INIT_RESVS;
	res.key = mykey;
	res.type = SCSI3_RESV_WRITEEXCLUSIVEREGISTRANTSONLY;
	res.scope = SCSI3_SCOPE_LOGICALUNIT;
	res.scope_specific_addr = 0;
	pre.resvdesc = res;
	if ((keylist.li->list == NULL) || (reslist.li->list == NULL)) {
		(void) msg_handle.log(SC_SYSLOG_WARNING,
		    MESSAGE, "reservation fatal error(%s) - malloc() error, "
		    "errno %d", command, errno);
		exit(1);
	}

	fd = open((char *)disk, O_RDONLY|O_NDELAY);
	while ((fd == -1) && (rt--)) {
		(void) msg_handle.log(SC_SYSLOG_WARNING, MESSAGE,
		    "reservation warning(%s) - Unable to open device %s, will "
		    "retry in %d seconds", command, (char *)disk, RETRY_SLEEP);
		(void) sleep(RETRY_SLEEP);
		fd = open((char *)disk, O_RDONLY|O_NDELAY);
	}

	if (fd == -1) {
		(void) msg_handle.log(SC_SYSLOG_WARNING,
		    MESSAGE, "reservation error(%s) - Unable to open "
		    "device %s", command, (char *)disk);
		free(disk);
		return;
	}

	if (!is_scsi3_disk((disk_list_t *)diskp)) {
		// scsi-2
		if (verbose)
			(void) printf("reservation - fence_node "
			"disk is scsi2...%s.\n", (char *)disk);

		if (do_scsi2_tkown(fd) == -1)
			(void) msg_handle.log(SC_SYSLOG_WARNING,
			    MESSAGE, "reservation error(%s) - do_scsi2_tkown() "
			    "error for disk %s",
			    command, (char *)disk);
	} else {
		// scsi-3 device
		if (verbose)
			(void) printf("reservation - fence_node "
			"disk is scsi3...%s.\n", (char *)disk);

		if (do_scsi3_inkeys(fd, keylist, NUM_RETRIES) == -1) {
			(void) msg_handle.log(SC_SYSLOG_WARNING, MESSAGE,
			    "reservation error(%s) - do_scsi3_inkeys() error "
			    "for disk %s", command, (char *)disk);
			goto outt;
		}

		// look for our key
		for (j = 0; j < keylist.li->listlen; j++)
			if (memcmp(keylist.li->list[j].key, mykey.key,
			    (size_t)MHIOC_RESV_KEY_SIZE) == 0) {
				found_my_key = true;
				break;
			}
		if (!found_my_key) {
			(void) msg_handle.log(SC_SYSLOG_WARNING, MESSAGE,
			    "reservation error(%s) - Unable to gain access to "
			    "device '%s'", command, (char *)disk);
			goto outt;
		}

		// make sure there is a reservation in place
		if (do_scsi3_inresv(fd, reslist) == -1) {
			(void) msg_handle.log(SC_SYSLOG_WARNING, MESSAGE,
			    "reservation error(%s) - do_scsi3_inresv() error "
			    "for disk %s", command, (char *)disk);
			goto outt;
		}
		if (reslist.li->listlen == 0) {
			// no-one has a reservation, issue one
			if (verbose)
				(void) printf("reservation - fence_node no "
				    "scsi-3 resvervation in place...%s.\n",
				    (char *)disk);

			if (do_scsi3_reserve_qualified_retry(fd, res, reslist)
			    == -1) {
				(void) msg_handle.log(
				    SC_SYSLOG_WARNING, MESSAGE,
				    "reservation error(%s) - do_scsi3_reserve()"
				    " error for disk %s", command,
				    (char *)disk);
				goto outt;
			}
		}

		// get the key for the node we are fencing
		if (get_scsi3_key_by_nodeid((nodeid_t)fence_node_id, victkey)
		    != 0) {
			//
			// SCMSGS
			// @explanation
			// The device fencing program has suffered an internal
			// error.
			// @user_action
			// Contact your authorized Sun service provider to
			// determine whether a workaround or patch is
			// available. Copies of /var/adm/messages from all
			// nodes should be provided for diagnosis. It may be
			// possible to retry the failed operation, depending
			// on the nature of the error. If the message
			// specifies the 'node_join' transition, then this
			// node may be unable to access shared devices. If the
			// failure occurred during the 'release_shared_scsi2'
			// transition, then a node which was joining the
			// cluster may be unable to access shared devices. In
			// either case, it may be possible to reacquire access
			// to shared devices by executing
			// '/usr/cluster/lib/sc/run_reserve -c node_join' on
			// all cluster nodes. If the failure occurred during
			// the 'make_primary' transition, then a device group
			// has failed to start on this node. If another node
			// was available to host the device group, then it
			// should have been started on that node. If desired,
			// it might be possible to switch the device group to
			// this node by using the cldevicegroup command. If no
			// other node was available, then the device group
			// will not have been started. You can use the
			// cldevicegroup command to retry the attempt to start
			// the device group. If the failure occurred during
			// the 'primary_to_secondary' transition, then the
			// shutdown or switchover of a device group has
			// failed. The desired action may be retried.
			//
			(void) msg_handle.log(SC_SYSLOG_WARNING, MESSAGE,
			    "reservation fatal error(%s) - unable to get local "
			    "node id", command);
			exit(1);
		}
		pre.victim_key = victkey;

		// preempt the node's key
		if (verbose) {
			(void) printf("preempting key from disk %s - ",
			    (char *)disk);
			print_scsi3_key(victkey);
		}
		if (do_scsi3_preemptandabort(fd, pre) == -1) {
			(void) msg_handle.log(SC_SYSLOG_WARNING, MESSAGE,
			    "reservation error(%s) - do_scsi3_preemptandabort()"
			    " error for disk %s", command, (char *)disk);
			goto outt;
		}
	}

outt:
	(void) close(fd);
	free(disk);
	free(keylist.li->list);
	free(reslist.li->list);
}

//
// Get disk related info.
//
// Search for the specified nodeid in the passed nodeid_list.
// Convert all nodeids to node names and store them in the
// path. We only use the path information if the specified
// nodeid is found in the list.
//
// Get default fencing protocol.
//
// The nodelist and default fencing are separated by " 0 ".
//
bool
get_disk_info(char *nsdata, int nodeid, char **path,
    unsigned short &default_fencing, unsigned short &detected_fencing)
{
	bool retval = false;
	const char *nodename;
	char *tok;
	int i;
	char *lasts;

	*path[0] = '\0';
	tok = strtok_r(nsdata, " ", &lasts);
	while (tok != NULL) {
		if ((i = atoi(tok)) < 1) {
			// End of node list.
			break;
		}

		if (i == nodeid) {
			retval = true;
		}

		// Convert the nodeid
		if ((nodename = clconf_cluster_get_nodename_by_nodeid(
		    cl, (nodeid_t)i)) != NULL) {
			(void) sprintf(*path, "%s%s:", *path, nodename);
		}
		tok = strtok_r(NULL, " ", &lasts);
	}

	//
	// If we get the node id correctly. we will preceed to get the fencing
	// info.
	//
	if (retval) {
		// Get the default fencing.
		tok = strtok_r(NULL, "|", &lasts);
		if (tok != NULL) {
			default_fencing = (unsigned short) atoi(tok);
			// Get the detected fencing.
			tok = strtok_r(NULL, "|", &lasts);
			if (tok != NULL) {
				detected_fencing = (unsigned short) atoi(tok);
				return (true);
			}
		}
	}

	return (false);
}

int
list_map(naming::naming_context_ptr ctxp, naming::binding_list_var bl, int nid)
{
	unsigned int len;
	unsigned int i;
	char *didname;
	CORBA::Object_var obj_v;
	Environment e;
	data_container::data_var dobj_v;
	char *nsdata;
	char *full_did_path, *path;
	disk_list_t *temp;
	char did_path[MAX_STR_LEN];
	unsigned short	default_fencing = FENCING_USE_GLOBAL;
	unsigned short	detected_fencing = FENCING_NA;

	len = bl->length();
	for (i = 0; i < len; i++) {
		//
		// We should only have object to list but skip over any
		// contexts just in case
		//
		if (bl[i].bind_type == naming::ncontext)
			continue;

		// lookup the object associated with the entry
		didname = bl[i].binding_name;
		obj_v = ctxp->resolve(didname, e);
		if (e.exception() != NULL) {
			return (-1);
		}

		dobj_v = data_container::data::_narrow(obj_v);
		if (CORBA::is_nil(dobj_v)) {
			return (-1);
		}

		// get the contents stored in the entry
		if ((nsdata = dobj_v->obtain_data(e)) == NULL) {
			return (-1);
		}
		ASSERT(!e.exception());

		if ((path = (char *)malloc(MAX_STR_LEN * sizeof (char))) ==
		    NULL) {
			return (-1);
		}

		if (!get_disk_info(nsdata, nid, &path, default_fencing,
		    detected_fencing)) {
			free(path);
			continue;
		}

		if ((full_did_path = (char *)malloc(MAX_STR_LEN)) == NULL) {
			free(path);
			return (-1);
		}

		(void) sprintf(full_did_path, "/dev/did/rdsk/%ss2", didname);

		// see if this is being used as a quorum device
		if (is_quorum(full_did_path)) {
			free(path);
			free(full_did_path);
			continue;
		}

		(void) sprintf(did_path, "rdsk/%s", didname);
		//
		// The DCS local_only property is set or fencing is turned off
		// for this device so we should not perform fencing.
		//
		if (is_local_only(did_path) ||
		    (default_fencing == NO_FENCING)) {
			free(path);
			free(full_did_path);
			continue;
		}

		if ((temp = new disk_list_t) == NULL) {
			//
			// SCMSGS
			// @explanation
			// The device fencing program has been unable to
			// allocate required memory.
			// @user_action
			// Memory usage should be monitored on this node and
			// steps taken to provide more available memory if
			// problems persist.
			//
			(void) msg_handle.log(SC_SYSLOG_WARNING, MESSAGE,
			    "reservation fatal error(%s) - malloc() error",
			    command);
			free(path);
			free(full_did_path);
			exit(1);
		}
		temp->did_path = full_did_path;
		temp->nodes = path;
		temp->default_fencing = default_fencing;
		temp->detected_fencing = detected_fencing;

		//
		// Global fencing is turned off.  If this is a '-X' call,
		// then fencing was just turned off and we need to make
		// sure there are no PGR keys on the disk.
		//
		if ((default_fencing == FENCING_USE_GLOBAL) &&
		    (global_fencing_status == GLOBAL_NO_FENCING)) {
			if (scrub_no_fence)
				setup_scsi2_disk(temp, 1);
			free(path);
			free(full_did_path);
			continue;
		}

		// add disk to the list
		temp->next = my_disks;
		my_disks = temp;
	}

	return (0);
}

//
// Retrieve global fencing status from the cache or the CCR if the cache
// is not available.
//
int
get_global_fencing_status()
{
	naming::naming_context_var	ctx_v;
	CORBA::Object_var		obj_v;
	data_container::data_var	dobj_v;
	Environment			e;
	char				*nsdata;

	// First try to look it up in the ns server.
	// lookup the GLOBAL_FENCING_MAP
	ctx_v = ns::root_nameserver();
	obj_v = ctx_v->resolve(GLOBAL_FENCING_MAP, e);
	if (e.exception() != NULL) {
		e.clear();
	} else {
		dobj_v = data_container::data::_narrow(obj_v);
		if (!CORBA::is_nil(dobj_v)) {
			if ((nsdata = dobj_v->obtain_data(e)) != NULL) {
				global_fencing_status =
				    (unsigned short) atoi(nsdata);
				return (0);
			}
		}
	}

	//
	// For some reason, the status is not availabe in ns server. Log
	// a warning and try to look it up from the CCR.
	//

	//
	// SCMSGS
	// @explanation
	// An error was encountered when run_reserve was retrieving the global
	// fencing status from the global name server. It will then try to
	// read it from the CCR directly.
	// @user_action
	// This is an informational message, no user action is needed.
	//
	(void) msg_handle.log(SC_SYSLOG_WARNING, MESSAGE, "Failed to retrieve "
	    "global fencing status from the global name server\n");

	return (read_global_fencing_status_from_ccr(&global_fencing_status));
}

int
ns_get_my_disks()
{
	naming::naming_context_ptr ctxp;
	naming::naming_context_ptr dctxp;
	naming::binding_iterator_var iterv;
	naming::binding_list_var bl;
	CORBA::Object_ptr obj;
	Environment e;
	int nid;

	// Get the global fencing status
	if (get_global_fencing_status() != 0) {
		//
		// SCMSGS
		// @explanation
		// Failed to retrieve the global fencing status from both the
		// global name server and the CCR.
		// @user_action
		// Check whether the root file system is having access
		// problems.
		//
		(void) msg_handle.log(SC_SYSLOG_WARNING,
		    MESSAGE, "reservation fatal error(%s) - "
		    "get_global_fencing_status error, errno %d",
		    command, errno);
		exit(1);
	}

	// lookup the DID_MAP
	ctxp = ns::root_nameserver();
	obj = ctxp->resolve(DID_MAP, e);
	if (e.exception() != NULL)
		return (-1);

	dctxp = naming::naming_context::_narrow(obj);
	CORBA::release(obj);

	// list out the first MAX_ELEMENTS entries
	dctxp->list(MAX_ELEMENTS, bl, iterv, e);
	if (e.exception() != NULL)
		return (-1);

	if (bl->length() == 0)
		return (0);

	if ((nid = get_nodeid_by_nodename(host_name)) == -1)
		return (-1);

	if (list_map(dctxp, bl, nid) == -1)
		return (-1);

	// Check to see if there are more items in the DID instance cache
	while (!CORBA::is_nil(iterv)) {
		iterv->next_n(MAX_ELEMENTS, bl, e);
		if (e.exception() != NULL)
			return (-1);

		if (bl->length() == 0)
			break;

		if (list_map(dctxp, bl, nid) == -1)
			return (-1);
	}
	return (0);
}

// Get the list of non-local_only, non-quorum disks attached to this node
void
get_my_disks()
{
	did_device_list_t *did_dev_list, *trans;
	char *did_path, *path, *full_did_path;
	char *search_for_host_name = NULL;
	disk_list_t *temp;

	// Get the global fencing status
	if (get_global_fencing_status() != 0) {
		(void) msg_handle.log(SC_SYSLOG_WARNING,
		    MESSAGE, "reservation fatal error(%s) - "
		    "get_global_fencing_status error, errno %d",
		    command, errno);
		exit(1);
	}

	// lookup the disks from the DID instance cache first
	if (ns_get_my_disks() == 0) {
		return;
	}

	my_disks = NULL;

	// setup search string
	//lint -e668 Possibly passing a null pointer to strlen
	if ((search_for_host_name =
	    (char *)malloc(strlen(host_name) + 2)) == NULL) {
		(void) msg_handle.log(SC_SYSLOG_WARNING,
		    MESSAGE, "reservation fatal error(%s) - "
		    "malloc() error, errno %d", command, errno);
		exit(1);
	}
	(void) sprintf(search_for_host_name, "%s:", host_name);
	//lint +e668

	did_dev_list = list_all_devices("disk");

	for (trans = did_dev_list; trans != NULL; trans = trans->next) {
		if ((path = did_get_path(trans)) == NULL) {
			//
			// SCMSGS
			// @explanation
			// The device fencing program has suffered an internal
			// error.
			// @user_action
			// Contact your authorized Sun service provider to
			// determine whether a workaround or patch is
			// available. Copies of /var/adm/messages from all
			// nodes should be provided for diagnosis. It may be
			// possible to retry the failed operation, depending
			// on the nature of the error. If the message
			// specifies the 'node_join' transition, then this
			// node may be unable to access shared devices. If the
			// failure occurred during the 'release_shared_scsi2'
			// transition, then a node which was joining the
			// cluster may be unable to access shared devices. In
			// either case, it may be possible to reacquire access
			// to shared devices by executing
			// '/usr/cluster/lib/sc/run_reserve -c node_join' on
			// all cluster nodes. If the failure occurred during
			// the 'make_primary' transition, then a device group
			// has failed to start on this node. If another node
			// was available to host the device group, then it
			// should have been started on that node. If desired,
			// it might be possible to switch the device group to
			// this node by using the cldevicegroup command. If no
			// other node was available, then the device group
			// will not have been started. You can use the
			// cldevicegroup command to retry the attempt to start
			// the device group. If the failure occurred during
			// the 'primary_to_secondary' transition, then the
			// shutdown or switchover of a device group has
			// failed. The desired action may be retried.
			//
			(void) msg_handle.log(SC_SYSLOG_WARNING, MESSAGE,
			    "reservation error(%s) - did_get_path() error",
			    command);
			continue;
		}

		//lint -e668 Possibly passing a null pointer to strstr
		if (strstr(path, search_for_host_name) == NULL) {
		//lint +e668
			// disk not connected to this node, ignore it
			free(path);
			continue;
		}

		if (did_get_devid_type(trans) == DEVID_NONE) {
			// not a disk, ignore it
			free(path);
			continue;
		}

		if ((did_path = did_get_did_path(trans)) == NULL) {
			//
			// SCMSGS
			// @explanation
			// The device fencing program has suffered an internal
			// error.
			// @user_action
			// Contact your authorized Sun service provider to
			// determine whether a workaround or patch is
			// available. Copies of /var/adm/messages from all
			// nodes should be provided for diagnosis. It may be
			// possible to retry the failed operation, depending
			// on the nature of the error. If the message
			// specifies the 'node_join' transition, then this
			// node may be unable to access shared devices. If the
			// failure occurred during the 'release_shared_scsi2'
			// transition, then a node which was joining the
			// cluster may be unable to access shared devices. In
			// either case, it may be possible to reacquire access
			// to shared devices by executing
			// '/usr/cluster/lib/sc/run_reserve -c node_join' on
			// all cluster nodes. If the failure occurred during
			// the 'make_primary' transition, then a device group
			// has failed to start on this node. If another node
			// was available to host the device group, then it
			// should have been started on that node. If desired,
			// it might be possible to switch the device group to
			// this node by using the cldevicegroup command. If no
			// other node was available, then the device group
			// will not have been started. You can use the
			// cldevicegroup command to retry the attempt to start
			// the device group. If the failure occurred during
			// the 'primary_to_secondary' transition, then the
			// shutdown or switchover of a device group has
			// failed. The desired action may be retried.
			//
			(void) msg_handle.log(SC_SYSLOG_WARNING, MESSAGE,
			    "reservation error(%s) - did_get_did_path() error",
			    command);
			free(path);
			continue;
		}
		if ((full_did_path = (char *)malloc(MAX_STR_LEN)) == NULL) {
			(void) msg_handle.log(SC_SYSLOG_WARNING, MESSAGE,
			    "reservation fatal error(%s) - malloc() error, "
			    "errno %d", command, errno);
			exit(1);
		}
		(void) sprintf(full_did_path, "/dev/did/%ss2", did_path);

		// see if this is being used as a quorum device
		if (is_quorum(full_did_path)) {
			free(full_did_path);
			free(did_path);
			free(path);
			continue;
		}

		//
		// The DCS local_only property is set or fencing is turned off
		// for this device so we should not perform fencing.
		//
		if (is_local_only(did_path) ||
		    (trans->default_fencing == NO_FENCING)) {
			free(full_did_path);
			free(did_path);
			free(path);
			continue;
		}

		if ((temp = new disk_list_t) == NULL) {
			(void) msg_handle.log(SC_SYSLOG_WARNING, MESSAGE,
			    "reservation fatal error(%s) - malloc() error",
			    command);
			exit(1);
		}
		temp->did_path = full_did_path;
		temp->nodes = path;
		temp->default_fencing = trans->default_fencing;
		temp->detected_fencing = trans->detected_fencing;

		//
		// Global fencing is turned off.  If this is a '-X' call,
		// then fencing was just turned off and we need to make
		// sure there are no PGR keys on the disk.
		//
		if ((trans->default_fencing == FENCING_USE_GLOBAL) &&
		    (global_fencing_status == GLOBAL_NO_FENCING)) {
			if (scrub_no_fence)
				setup_scsi2_disk(temp, 1);
			free(full_did_path);
			free(did_path);
			free(path);
			free(temp);
			continue;
		}

		// add disk to the list
		temp->next = my_disks;
		my_disks = temp;
		free(did_path);
	}
}

main(int argc, char **argv)
{
	char *joining_node = NULL;	// a node joining the cluster and asking
						// this node to release scsi-2
	char *fenced_node = NULL;	// fence devices shared with fenced_node
	char *search_for_node = NULL;
	char *search_for_host = NULL;
	threadpool *reservation_threadpool;	// threads for accessing disks
	cmm::control_var ctrlp;			// to get cluster state
	Environment ee;
	disk_list_t *diskp, *tempp;
	int c, error;
	unsigned int i;
	char *fence_lock = NULL;		// lock passed in
	unsigned int fence_lock_retry = 2;	// seconds between lock checks
	int fence_lock_timeout = -2;		// use default timeout value
	char *device_name = NULL;	// name of the device that has a
					// switch of scsi protocol
	bool switch_to_scsi3 = false;
	bool switch_to_scsi2 = false;

	// these are for debug uses of this code
	char *zdisk = NULL;
	int zd = -1;
	mhioc_inkeys_t keylist;
	mhioc_key_list_t klist;
	mhioc_register_t reg;
	mhioc_registerandignorekey_t regign;
	mhioc_inresvs_t reslist;
	mhioc_resv_desc_list_t rlist;
	mhioc_resv_desc_t res;
	mhioc_preemptandabort_t pre;
	mhioc_resv_key_t vickey;
	int exit_status = 0;
	int libinit;
	did_repl_list_t replt;
	did_repl_path_t *trans;
	int is_repl_host, is_repl_fenced;

	if (sc_zonescheck() != 0)
		return (1);
	// set up threadpool
	reservation_threadpool =
	    new threadpool(true, 1, "Reservation threadpool");

	// initializes ORB
	if ((error = clconf_lib_init()) != 0) {
		//
		// SCMSGS
		// @explanation
		// The device fencing program has suffered an internal error.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available. Copies of
		// /var/adm/messages from all nodes should be provided for
		// diagnosis. It may be possible to retry the failed
		// operation, depending on the nature of the error. If the
		// message specifies the 'node_join' transition, then this
		// node may be unable to access shared devices. If the failure
		// occurred during the 'release_shared_scsi2' transition, then
		// a node which was joining the cluster may be unable to
		// access shared devices. In either case, it may be possible
		// to reacquire access to shared devices by executing
		// '/usr/cluster/lib/sc/run_reserve -c node_join' on all
		// cluster nodes. If the failure occurred during the
		// 'make_primary' transition, then a device group has failed
		// to start on this node. If another node was available to
		// host the device group, then it should have been started on
		// that node. If desired, it might be possible to switch the
		// device group to this node by using the cldevicegroup
		// command. If no other node was available, then the device
		// group will not have been started. You can use the
		// cldevicegroup command to retry the attempt to start the
		// device group. If the failure occurred during the
		// 'primary_to_secondary' transition, then the shutdown or
		// switchover of a device group has failed. The desired action
		// may be retried.
		//
		(void) msg_handle.log(SC_SYSLOG_WARNING,
		    MESSAGE, "reservation fatal error(UNKNOWN) - "
		    "clconf_lib_init() error, returned %d", error);
		exit(1);
	}

	//
	// Quorum table is used to find out which devices are quorum devices
	// (reserve.cc should ignore quorum devices, the quorum algorithm will
	// handle them) and to get this node's scsi-3 reservation key.
	//
	if ((error = cluster_get_quorum_status(&quor_table)) != 0) {
		//
		// SCMSGS
		// @explanation
		// The device fencing program has suffered an internal error.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available. Copies of
		// /var/adm/messages from all nodes should be provided for
		// diagnosis. It may be possible to retry the failed
		// operation, depending on the nature of the error. If the
		// message specifies the 'node_join' transition, then this
		// node may be unable to access shared devices. If the failure
		// occurred during the 'release_shared_scsi2' transition, then
		// a node which was joining the cluster may be unable to
		// access shared devices. In either case, it may be possible
		// to reacquire access to shared devices by executing
		// '/usr/cluster/lib/sc/run_reserve -c node_join' on all
		// cluster nodes. If the failure occurred during the
		// 'make_primary' transition, then a device group has failed
		// to start on this node. If another node was available to
		// host the device group, then it should have been started on
		// that node. If desired, it might be possible to switch the
		// device group to this node by using the cldevicegroup
		// command. If no other node was available, then the device
		// group will not have been started. You can use the
		// cldevicegroup command to retry the attempt to start the
		// device group. If the failure occurred during the
		// 'primary_to_secondary' transition, then the shutdown or
		// switchover of a device group has failed. The desired action
		// may be retried.
		//
		(void) msg_handle.log(SC_SYSLOG_WARNING, MESSAGE,
		    "reservation fatal error(UNKNOWN) - "
		    "cluster_get_quorum_status() error, returned %d", error);
		exit(1);
	}

	// cxx check for failures???
	cl = clconf_cluster_get_current();
	myid = orb_conf::node_number();

	// get cluster membership
	ctrlp = cmm_ns::get_control();
	if (CORBA::is_nil(ctrlp)) {
		//
		// SCMSGS
		// @explanation
		// The device fencing program has suffered an internal error.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available. Copies of
		// /var/adm/messages from all nodes should be provided for
		// diagnosis.
		//
		(void) msg_handle.log(SC_SYSLOG_WARNING, MESSAGE,
		    "reservation fatal error(UNKNOWN) - get_control() "
		    "failure");
		exit(1);
	}
	ctrlp->get_cluster_state(clust_state, ee);
	if (ee.exception()) {
		ee.clear();
		(void) msg_handle.log(SC_SYSLOG_WARNING, MESSAGE,
		    "reservation fatal error(%s) - get_cluster_state() "
		    "exception", command);
		exit(1);
	}

	/* Init libdid. */
	libinit = did_initlibrary(1, DID_NONE, NULL);
	if (libinit != DID_INIT_SUCCESS) {
		//
		// SCMSGS
		// @explanation
		// The device fencing program has suffered an internal error.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available. Copies of
		// /var/adm/messages from all nodes should be provided for
		// diagnosis. It may be possible to retry the failed
		// operation, depending on the nature of the error. If the
		// message specifies the 'node_join' transition, then this
		// node may be unable to access shared devices. If the failure
		// occurred during the 'release_shared_scsi2' transition, then
		// a node which was joining the cluster may be unable to
		// access shared devices. In either case, it may be possible
		// to reacquire access to shared devices by executing
		// '/usr/cluster/lib/sc/run_reserve -c node_join' on all
		// cluster nodes. If the failure occurred during the
		// 'make_primary' transition, then a device group has failed
		// to start on this node. If another node was available to
		// host the device group, then it should have been started on
		// that node. If desired, it might be possible to switch the
		// device group to this node by using the cldevicegroup
		// command. If no other node was available, then the device
		// group will not have been started. You can use the
		// cldevicegroup command to retry the attempt to start the
		// device group. If the failure occurred during the
		// 'primary_to_secondary' transition, then the shutdown or
		// switchover of a device group has failed. The desired action
		// may be retried.
		//
		(void) msg_handle.log(SC_SYSLOG_WARNING, MESSAGE,
		    "reservation fatal error(%s) - did_initlibrary() failure",
		    command);
		exit(1);
	}

	// get args
	while ((c = getopt(argc, argv, "c:j:h:f:z:v:xmSlt:r:n:d:P:X")) != EOF) {
		switch (c) {
		case 'c' :
			command = optarg;
			break;
		case 'd' :
			device_name = optarg;
			break;
		case 'j' :
			joining_node = optarg;
			break;
		case 'h' :
			host_name = optarg;
			break;
		case 'f' :
			fenced_node = optarg;
			break;
		case 'r' :
			fence_lock_retry = (unsigned int)atoi(optarg);
			break;
		case 't' :
			fence_lock_timeout = atoi(optarg);
			break;
		case 'n' :
			fence_lock = optarg;
			break;
		case 'x' :
			//
			// Used to supress running release_shared_scsi-2
			// during calls from scgdevs.
			//
			run_clexecd = false;
			break;
		// debug options
		case 'm' :
			// turn on debug messages
			verbose = 1;
			break;
		case 'v' :
			bzero(vickey.key, (size_t)MHIOC_RESV_KEY_SIZE);
			(void) memcpy((char *)vickey.key, optarg,
			    (size_t)MHIOC_RESV_KEY_SIZE);
			break;
		case 'z' :
			zdisk = optarg;
			break;
		case 'S' :
			node_join_scrubbing = true;
			break;
		case 'X' :
			scrub_no_fence = true;
			break;
		case 'P' :
			if (strcmp("scsi3", optarg) == 0) {
				switch_to_scsi3 = true;
			} else if (strcmp("scsi2", optarg) == 0) {
				switch_to_scsi2 = true;
			} else {
				if (strcmp("nofence", optarg) != 0) {
					//
					// SCMSGS
					// @explanation
					// The device fencing program has
					// suffered an internal error.
					// @user_action
					// Contact your authorized Sun service
					// provider to determine whether a
					// workaround or patch is available.
					// Copies of /var/adm/messages from
					// all nodes should be provided for
					// diagnosis. It may be possible to
					// retry the failed operation,
					// depending on the nature of the
					// error. If the message specifies the
					// 'node_join' transition, then this
					// node may be unable to access shared
					// devices. If the failure occurred
					// during the 'release_shared_scsi2'
					// transition, then a node which was
					// joining the cluster may be unable
					// to access shared devices. In either
					// case, it may be possible to
					// reacquire access to shared devices
					// by executing
					// '/usr/cluster/lib/sc/run_reserve -c
					// node_join' on all cluster nodes. If
					// the failure occurred during the
					// 'make_primary' transition, then a
					// device group has failed to start on
					// this node. If another node was
					// available to host the device group,
					// then it should have been started on
					// that node. If desired, it might be
					// possible to switch the device group
					// to this node by using the
					// cldevicegroup command. If no other
					// node was available, then the device
					// group will not have been started.
					// You can use the cldevicegroup
					// command to retry the attempt to
					// start the device group. If the
					// failure occurred during the
					// 'primary_to_secondary' transition,
					// then the shutdown or switchover of
					// a device group has failed. The
					// desired action may be retried.
					//
					(void) msg_handle.log(SC_SYSLOG_WARNING,
					    MESSAGE, "reservation fatal "
					    "error(UNKNOWN) - Illegal command "
					    "line option");
					exit(1);
				}
			}
			break;
		case 'l' :
			//
			// used by ucmm scripts, which should mnove to the new
			// interface once the old one cannot be intalled
			//
			int retval;
			retval = check_for_fencing(COARSE_GRAIN_FENCING_LOCK,
			    600, 2);
			exit(retval);
		default :
			(void) msg_handle.log(SC_SYSLOG_WARNING,
			    MESSAGE, "reservation fatal error(UNKNOWN) - "
			    "Illegal command line option");
			exit(1);
		}
	}

	// check for required args
	if (command == NULL) {
		//
		// SCMSGS
		// @explanation
		// The device fencing program has suffered an internal error.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available. Copies of
		// /var/adm/messages from all nodes should be provided for
		// diagnosis. It may be possible to retry the failed
		// operation, depending on the nature of the error. If the
		// message specifies the 'node_join' transition, then this
		// node may be unable to access shared devices. If the failure
		// occurred during the 'release_shared_scsi2' transition, then
		// a node which was joining the cluster may be unable to
		// access shared devices. In either case, it may be possible
		// to reacquire access to shared devices by executing
		// '/usr/cluster/lib/sc/run_reserve -c node_join' on all
		// cluster nodes. If the failure occurred during the
		// 'make_primary' transition, then a device group has failed
		// to start on this node. If another node was available to
		// host the device group, then it should have been started on
		// that node. If desired, it might be possible to switch the
		// device group to this node by using the cldevicegroup
		// command. If no other node was available, then the device
		// group will not have been started. You can use the
		// cldevicegroup command to retry the attempt to start the
		// device group. If the failure occurred during the
		// 'primary_to_secondary' transition, then the shutdown or
		// switchover of a device group has failed. The desired action
		// may be retried.
		//
		(void) msg_handle.log(SC_SYSLOG_WARNING,
		    MESSAGE, "reservation fatal error(UNKNOWN) - "
		    "Command not specified");
		(void) printf("Reserve: command not specified\n");
		print_usage();
		exit(1);
	}
	if ((strcmp(command, "release_shared_scsi2") == 0) ||
	    (strcmp(command, "node_join") == 0) ||
	    (strcmp(command, "fence_node") == 0) ||
	    (strcmp(command, "fence_all_nodes") == 0) ||
	    (strcmp(command, "reset_shared_bus") == 0)) {
		if (host_name == NULL) {
			//
			// SCMSGS
			// @explanation
			// The device fencing program has suffered an internal
			// error.
			// @user_action
			// Contact your authorized Sun service provider to
			// determine whether a workaround or patch is
			// available. Copies of /var/adm/messages from all
			// nodes should be provided for diagnosis. It may be
			// possible to retry the failed operation, depending
			// on the nature of the error. If the message
			// specifies the 'node_join' transition, then this
			// node may be unable to access shared devices. If the
			// failure occurred during the 'release_shared_scsi2'
			// transition, then a node which was joining the
			// cluster may be unable to access shared devices. In
			// either case, it may be possible to reacquire access
			// to shared devices by executing
			// '/usr/cluster/lib/sc/run_reserve -c node_join' on
			// all cluster nodes. If the failure occurred during
			// the 'make_primary' transition, then a device group
			// has failed to start on this node. If another node
			// was available to host the device group, then it
			// should have been started on that node. If desired,
			// it might be possible to switch the device group to
			// this node by using the cldevicegroup command. If no
			// other node was available, then the device group
			// will not have been started. You can use the
			// cldevicegroup command to retry the attempt to start
			// the device group. If the failure occurred during
			// the 'primary_to_secondary' transition, then the
			// shutdown or switchover of a device group has
			// failed. The desired action may be retried.
			//
			(void) msg_handle.log(SC_SYSLOG_WARNING,
			    MESSAGE, "reservation fatal error(%s) - "
			    "host_name not specified", command);
			exit(1);
		}
	}

	if (strcmp(command, "switch_device_scsi_protocol") == 0) {
		if (device_name == NULL) {
			//
			// SCMSGS
			// @explanation
			// Need explanation of this message!
			// @user_action
			// Need a user action for this message.
			//
			(void) msg_handle.log(SC_SYSLOG_WARNING,
			    MESSAGE, "reservation fatal error(%s) - "
			    "device_name not specified", command);
			exit(1);
		}
	}
	if (strcmp(command, "release_shared_scsi2") == 0)
		if (joining_node == NULL) {
			//
			// SCMSGS
			// @explanation
			// The device fencing program has suffered an internal
			// error.
			// @user_action
			// Contact your authorized Sun service provider to
			// determine whether a workaround or patch is
			// available. Copies of /var/adm/messages from all
			// nodes should be provided for diagnosis. It may be
			// possible to retry the failed operation, depending
			// on the nature of the error. If the message
			// specifies the 'node_join' transition, then this
			// node may be unable to access shared devices. If the
			// failure occurred during the 'release_shared_scsi2'
			// transition, then a node which was joining the
			// cluster may be unable to access shared devices. In
			// either case, it may be possible to reacquire access
			// to shared devices by executing
			// '/usr/cluster/lib/sc/run_reserve -c node_join' on
			// all cluster nodes. If the failure occurred during
			// the 'make_primary' transition, then a device group
			// has failed to start on this node. If another node
			// was available to host the device group, then it
			// should have been started on that node. If desired,
			// it might be possible to switch the device group to
			// this node by using the cldevicegroup command. If no
			// other node was available, then the device group
			// will not have been started. You can use the
			// cldevicegroup command to retry the attempt to start
			// the device group. If the failure occurred during
			// the 'primary_to_secondary' transition, then the
			// shutdown or switchover of a device group has
			// failed. The desired action may be retried.
			//
			(void) msg_handle.log(SC_SYSLOG_WARNING,
			    MESSAGE, "reservation fatal error(%s) - "
			    "joining_node not specified", command);
			exit(1);
		}
	if (strcmp(command, "fence_node") == 0)
		if (fenced_node == NULL) {
			//
			// SCMSGS
			// @explanation
			// The device fencing program has suffered an internal
			// error.
			// @user_action
			// Contact your authorized Sun service provider to
			// determine whether a workaround or patch is
			// available. Copies of /var/adm/messages from all
			// nodes should be provided for diagnosis. It may be
			// possible to retry the failed operation, depending
			// on the nature of the error. If the message
			// specifies the 'node_join' transition, then this
			// node may be unable to access shared devices. If the
			// failure occurred during the 'release_shared_scsi2'
			// transition, then a node which was joining the
			// cluster may be unable to access shared devices. In
			// either case, it may be possible to reacquire access
			// to shared devices by executing
			// '/usr/cluster/lib/sc/run_reserve -c node_join' on
			// all cluster nodes. If the failure occurred during
			// the 'make_primary' transition, then a device group
			// has failed to start on this node. If another node
			// was available to host the device group, then it
			// should have been started on that node. If desired,
			// it might be possible to switch the device group to
			// this node by using the cldevicegroup command. If no
			// other node was available, then the device group
			// will not have been started. You can use the
			// cldevicegroup command to retry the attempt to start
			// the device group. If the failure occurred during
			// the 'primary_to_secondary' transition, then the
			// shutdown or switchover of a device group has
			// failed. The desired action may be retried.
			//
			(void) msg_handle.log(SC_SYSLOG_WARNING,
			    MESSAGE, "reservation fatal error(%s) - "
			    "fenced_node not specified", command);
			exit(1);
		}

	if ((strcmp(command, "grab_lock") == 0) ||
	    (strcmp(command, "release_lock") == 0) ||
	    (strcmp(command, "check_lock") == 0))
		if (fence_lock == NULL) {
			//
			// SCMSGS
			// @explanation
			// The device fencing program has suffered an internal
			// error.
			// @user_action
			// Contact your authorized Sun service provider to
			// determine whether a workaround or patch is
			// available. Copies of /var/adm/messages from all
			// nodes should be provided for diagnosis.
			//
			(void) msg_handle.log(SC_SYSLOG_WARNING,
			    MESSAGE, "reservation fatal error(%s) - "
			    "lock not specified", command);
			exit(1);
		}

	// setup scsi-3 ioctl variables
	if (get_scsi3_key_by_nodeid(myid, mykey) != 0) {
		(void) msg_handle.log(SC_SYSLOG_WARNING, MESSAGE,
		    "reservation fatal error(%s) - unable to get local node id",
		    command);
		exit(1);
	}

	// used for debug routines
	keylist.li = &klist;
	keylist.li->list = (mhioc_resv_key *)
	    malloc(INIT_KEYS * sizeof (mhioc_resv_key_t));
	keylist.li->listsize = INIT_KEYS;
	keylist.li->listlen = 0;
	reslist.li = &rlist;
	reslist.li->list = (mhioc_resv_desc *)
	    malloc(INIT_RESVS * sizeof (mhioc_resv_desc_t));
	reslist.li->listsize = INIT_RESVS;
	reslist.li->listlen = 0;
	if ((keylist.li->list == NULL) || (reslist.li->list == NULL)) {
		(void) msg_handle.log(SC_SYSLOG_WARNING,
		    MESSAGE, "reservation fatal error(%s) - malloc() error, "
		    "errno %d", command, errno);
		exit(1);
	}
	res.key = mykey;
	res.type = SCSI3_RESV_WRITEEXCLUSIVEREGISTRANTSONLY;
	res.scope = SCSI3_SCOPE_LOGICALUNIT;
	res.scope_specific_addr = 0;
	reg.oldkey = mykey;
	reg.newkey = mykey;
	reg.aptpl = B_TRUE;
	regign.newkey = mykey;
	regign.aptpl = B_TRUE;
	pre.resvdesc = res;

	if (zdisk != NULL)
		if ((zd = open(zdisk, O_RDONLY|O_NDELAY)) == -1) {
			(void) printf("Could not open device '%s', "
			    "errno %d\n", zdisk, errno);
			exit(1);
		}

	if ((strcmp(command, "release_shared_scsi2") == 0) ||
	    (strcmp(command, "node_join") == 0) ||
	    (strcmp(command, "fence_node") == 0) ||
	    (strcmp(command, "fence_all_nodes") == 0))
		get_my_disks();


	if (strcmp(command, "release_shared_scsi2") == 0) {
		//
		// Determine which disks are shared with the joining node and
		// make sure the joining node is no longer fenced off from them.
		//

		//lint -e668
		if ((search_for_node =
		    (char *)malloc(strlen(joining_node) + 2)) == NULL) {
			(void) msg_handle.log(SC_SYSLOG_WARNING,
			    MESSAGE, "reservation fatal error(%s) - "
			    "malloc() error, errno %d", command, errno);
			exit(1);
		}
		(void) sprintf(search_for_node, "%s:", joining_node);
		//lint +e668

		for (diskp = my_disks; diskp != NULL; diskp = diskp->next) {
			//lint -e668 Possibly passing a null pointer to strstr
			if (strstr(diskp->nodes, search_for_node) == NULL)
			//lint +e668
				// disk not connected to joining node, ignore it
				continue;

			// release any scsi-2 reservations held on these disks
			reservation_threadpool->defer_processing(new
			    work_task(release_shared_scsi2_doit,
			    (void *) (diskp)));
		}

		// wait for threads to finish
		reservation_threadpool->quiesce(threadpool::QEMPTY);
	} else if (strcmp(command, "node_join") == 0) {
		//
		// We first ask all other cluster nodes to release any scsi-2
		// reservations they have on any disks shared with us, then we
		// make sure we have access to all the disks.
		//
		// XXX Remove this secondary release of scsi-2 reservations
		// once rolling upgrade from SC3.1U2 is no longer supported.
		//
		if (run_clexecd) {
			if ((release_shared_scsi2_cmd =
			    (char *)malloc(45 + strlen(RESV_PATH) +
			    strlen(host_name))) == NULL) {
				(void) msg_handle.log(SC_SYSLOG_WARNING,
				    MESSAGE, "reservation fatal error(%s) - "
				    "malloc() error, errno %d", command,
				    errno);
				exit(1);
			}
			(void) sprintf(release_shared_scsi2_cmd,
			    "%s/run_reserve -c release_shared_scsi2 -n %s",
			    RESV_PATH, host_name);
			for (i = 0; i < NODEID_MAX + 1; i++) {
				// don't run it on non-cluster nodes
				if (clust_state.members.members[i] ==
				    INCN_UNKNOWN)
					continue;
				// don't run it on myself
				if (i == myid)
					continue;
				if (verbose)
					(void) printf("reservation - issuing "
					    "release_scsi2 on node %d\n", i);
				reservation_threadpool->defer_processing(new
				    work_task(remote_cluster_invocation,
				    (void *) i));
			}
			// wait for thread to finish
			reservation_threadpool->quiesce(threadpool::QEMPTY);
		}

		for (diskp = my_disks; diskp != NULL; diskp = diskp->next) {
			// make sure we have access to all disks
			reservation_threadpool->defer_processing(new
			    work_task(node_join_doit,
				(void *) (diskp)));
		}

		// wait for threads to finish
		reservation_threadpool->quiesce(threadpool::QEMPTY);
	} else if (strcmp(command, "switch_device_scsi_protocol") == 0) {
		if (!is_quorum(device_name)) {
			did_device_list_t *dlist;
			char *switch_path, *d_name;
			disk_list_t switch_did;

			if ((switch_did.did_path =
			    (char *)malloc(strlen(device_name) + 1)) == NULL) {
				(void) msg_handle.log(SC_SYSLOG_WARNING,
				    MESSAGE, "reservation fatal error(%s) - "
				    "malloc() error", command);
				exit(1);
			}
			(void) strcpy(switch_did.did_path, device_name);

			/* trim /dev/rdsk/dXs2 down to dX */
			if ((d_name = strrchr(device_name, '/')) != NULL)
				device_name = d_name + 1;
			if ((d_name = strstr(device_name, "s2")) != NULL)
				*d_name = '\0';

			if ((dlist = map_from_did_device(device_name))
			    == NULL) {
				//
				// SCMSGS
				// @explanation
				// The device fencing program has suffered an
				// internal error.
				// @user_action
				// Contact your authorized Sun service provider
				// to determine whether a workaround or patch is
				// available.
				//
				(void) msg_handle.log(SC_SYSLOG_WARNING,
				    MESSAGE, "reservation fatal error(%s) - "
				    "Unable to retrieve DID device information",
				    command);
				free(switch_did.did_path);
				exit(1);
			}
			if ((switch_path = did_get_path(dlist)) == NULL) {
				(void) msg_handle.log(SC_SYSLOG_WARNING,
				    MESSAGE, "reservation fatal error(%s) - "
				    "Unable to retrieve DID device information",
				    command);
				free(switch_did.did_path);
				exit(1);
			}
			switch_did.nodes = switch_path;
			if (switch_to_scsi3) {
				setup_scsi3_disk(&switch_did);
			} else if (switch_to_scsi2) {
				setup_scsi2_disk(&switch_did, 0);
			} else {
				setup_scsi2_disk(&switch_did, 1);
			}
			free(switch_did.did_path);
		}
	} else if (strcmp(command, "fence_all_nodes") == 0) {
		// fence all non-cluster nodes from devices we are attached to

		for (diskp = my_disks; diskp != NULL; diskp = diskp->next) {
			// fence node from this shared disk
			reservation_threadpool->defer_processing(new
			    work_task(fence_all_nodes_doit,
				(void *) (diskp)));
		}

		// wait for threads to finish
		reservation_threadpool->quiesce(threadpool::QEMPTY);
	} else if (strcmp(command, "fence_node") == 0) {
		// fence a specifc node from all devices shared with this node

		// get nodeid for the node we are fencing
		fence_node_id = get_nodeid_by_nodename(fenced_node);
		if (fence_node_id == -1) {
			//
			// SCMSGS
			// @explanation
			// The device fencing program has suffered an internal
			// error.
			// @user_action
			// Contact your authorized Sun service provider to
			// determine whether a workaround or patch is
			// available. Copies of /var/adm/messages from all
			// nodes should be provided for diagnosis. It may be
			// possible to retry the failed operation, depending
			// on the nature of the error. If the message
			// specifies the 'node_join' transition, then this
			// node may be unable to access shared devices. If the
			// failure occurred during the 'release_shared_scsi2'
			// transition, then a node which was joining the
			// cluster may be unable to access shared devices. In
			// either case, it may be possible to reacquire access
			// to shared devices by executing
			// '/usr/cluster/lib/sc/run_reserve -c node_join' on
			// all cluster nodes. If the failure occurred during
			// the 'make_primary' transition, then a device group
			// has failed to start on this node. If another node
			// was available to host the device group, then it
			// should have been started on that node. If desired,
			// it might be possible to switch the device group to
			// this node by using the cldevicegroup command. If no
			// other node was available, then the device group
			// will not have been started. You can use the
			// cldevicegroup command to retry the attempt to start
			// the device group. If the failure occurred during
			// the 'primary_to_secondary' transition, then the
			// shutdown or switchover of a device group has
			// failed. The desired action may be retried.
			//
			(void) msg_handle.log(SC_SYSLOG_WARNING, MESSAGE,
			    "reservation fatal error(%s) - unable to determine "
			    "node id for node %s", command, fenced_node);
			exit(1);
		}
		//
		// Doublecheck that this node hasn't become a member since this
		// command was issued.
		//
		if (clust_state.members.members[fence_node_id] != INCN_UNKNOWN)
			goto main_outt;

		// setup search strings
		//lint -e668 Possibly passing a null pointer to strlen
		if ((search_for_node =
		    (char *)malloc(strlen(fenced_node) + 2)) == NULL) {
			(void) msg_handle.log(SC_SYSLOG_WARNING,
			    MESSAGE, "reservation fatal error(%s) - "
			    "malloc() error, errno %d", command, errno);
			exit(1);
		}
		(void) sprintf(search_for_node, "%s:", fenced_node);
		//lint +e668
		//lint -e668 Possibly passing a null pointer to strlen
		if ((search_for_host =
		    (char *)malloc(strlen(host_name) + 2)) == NULL) {
			(void) msg_handle.log(SC_SYSLOG_WARNING,
			    MESSAGE, "reservation fatal error(%s) - "
			    "malloc() error, errno %d", command, errno);
			exit(1);
		}
		(void) sprintf(search_for_host, "%s:", host_name);
		//lint +e668

		for (diskp = my_disks; diskp != NULL; diskp = diskp->next) {
			//lint -e668 Possibly passing a null pointer to strstr
			if (strstr(diskp->nodes, search_for_node) == NULL)
			//lint +e668
				// disk not connected to fenced node, ignore it
				continue;
			//lint -e668 Possibly passing a null pointer to strstr

			//
			// Do not fence replicated devices unless we are
			// attached to the same replica as the fenced node.
			//
			is_repl_host = 0;
			is_repl_fenced = 0;
			if (check_for_repl_device(diskp->did_path, &replt)
			    == DID_REPL_TRUE) {
				for (trans = replt.device_path_list; trans;
				    trans = trans->next) {
					if (strncmp(search_for_node,
					    trans->device_path,
					    strlen(search_for_node)) == 0)
						is_repl_fenced = 1;
					if (strncmp(search_for_host,
					    trans->device_path,
					    strlen(search_for_host)) == 0)
						is_repl_host = 1;
				}
				if (is_repl_host != is_repl_fenced)
					continue;
			}

			// fence node from this shared disk
			reservation_threadpool->defer_processing(new
			    work_task(fence_node_doit,
				(void *) (diskp)));
		}

		// wait for threads to finish
		reservation_threadpool->quiesce(threadpool::QEMPTY);
	} else if (strcmp(command, "reset_shared_bus") == 0) {
		//
		// Reset all scsi buses shared with non-cluster nodes.
		// If we ever support non-fibre devices connected to more than
		// 2 nodes, this code should be changed to prevent multiple
		// nodes from resetting the same bus.
		//
		char *temp, *temp2, *dead_nodes;
		int controllers_reset[MAX_CONTROLLERS_PER_NODE];
		int controller_num;
		bool already_reset, connected_to_dead_node;
		did_device_list_t *did_dev_list, *transp;
		char *did_path, *path = NULL;
		char comp_did_path[MAX_STR_LEN];
		char *search_for_host_name = NULL;
		unsigned int num_dead_nodes;
		int fd, err, errnum, rt, indx;
		struct uscsi_cmd ucmd;
		bool displayed_msg = false;

		// init controllers_reset
		for (i = 0; i < MAX_CONTROLLERS_PER_NODE; i++)
			controllers_reset[i] = -1;

		did_dev_list = list_all_devices("disk");

		// get list of dead nodes
		dead_nodes = show_dead_nodes(&num_dead_nodes);
		if (verbose) {
			(void) printf("there are %d dead nodes\n",
			    num_dead_nodes);
			if (dead_nodes != NULL)
				for (i = 0; i < num_dead_nodes; i++)
					(void) printf("--%s--\n",
					    &dead_nodes[i * MAX_STR_LEN]);
		}
		//
		// Quorum code is responsible for resetting buses w/quorum
		// device, so mark those controllers as already reset.
		// XXX This could add the same controller to controllers_reset
		// multiple times if there are more than one quorum device on
		// the same controller.
		//
		indx = 0;
		for (i = 0; i < quor_table->num_quorum_devices; i++) {
			get_ctd_from_did(host_name,
			    quor_table->quorum_device_list[i].gdevname, &temp);
			if (temp == NULL)
				continue;
			temp2 = strstr(temp, "/rdsk/c");
			if (temp2 == NULL)
				continue;
			if (verbose)
				(void) printf("Ignoring quorum device %s ",
				    temp);
			controller_num = atoi(temp2 + strlen("/rdsk/c"));
			if (verbose)
				(void) printf("on controller %d.\n",
				    controller_num);
			controllers_reset[indx++] = controller_num;
		}

		// setup hostname search string
		//lint -e668 Possibly passing a null pointer to strlen
		if ((search_for_host_name =
		    (char *)malloc(strlen(host_name) + 2)) == NULL) {
			(void) msg_handle.log(SC_SYSLOG_WARNING,
			    MESSAGE, "reservation fatal error(%s) - "
			    "malloc() error, errno %d", command, errno);
			exit(1);
		}
		(void) sprintf(search_for_host_name, "%s:", host_name);
		//lint +e668

		//
		// find devices that :
		//	are disks
		//	are not being used as a quorum device
		//	are not fiber-channel
		//	are connected to this node
		//	are also connected to a node that is not in the cluster
		//
		for (transp = did_dev_list; transp != NULL;
		    transp = transp->next) {
			if (did_get_num_paths(transp) < 2)
				// device is not multiported, ignore
				continue;

			if (path != NULL) {
				free(path);
			}
			if ((path = did_get_path(transp)) == NULL) {
				(void) msg_handle.log(SC_SYSLOG_WARNING,
				    MESSAGE, "reservation error(%s) - "
				    "did_get_path() error", command);
				continue;
			}

			// get the controller number from the local Solaris name
			//lint -e668 Possibly passing a null pointer to strstr
			temp = strstr(path, search_for_host_name);
			if (temp == NULL) {
			//lint +e668
				// disk not connected to this node, ignore it
				continue;
			}
			temp2 = strstr(temp, "/rdsk/c");
			if (temp2 == NULL) {
				//
				// SCMSGS
				// @explanation
				// The device fencing program has suffered an
				// internal error.
				// @user_action
				// Contact your authorized Sun service
				// provider to determine whether a workaround
				// or patch is available. Copies of
				// /var/adm/messages from all nodes should be
				// provided for diagnosis. It may be possible
				// to retry the failed operation, depending on
				// the nature of the error. If the message
				// specifies the 'node_join' transition, then
				// this node may be unable to access shared
				// devices. If the failure occurred during the
				// 'release_shared_scsi2' transition, then a
				// node which was joining the cluster may be
				// unable to access shared devices. In either
				// case, it may be possible to reacquire
				// access to shared devices by executing
				// '/usr/cluster/lib/sc/run_reserve -c
				// node_join' on all cluster nodes. If the
				// failure occurred during the 'make_primary'
				// transition, then a device group has failed
				// to start on this node. If another node was
				// available to host the device group, then it
				// should have been started on that node. If
				// desired, it might be possible to switch the
				// device group to this node by using the
				// cldevicegroup command. If no other node was
				// available, then the device group will not
				// have been started. You can use the
				// cldevicegroup command to retry the attempt
				// to start the device group. If the failure
				// occurred during the 'primary_to_secondary'
				// transition, then the shutdown or switchover
				// of a device group has failed. The desired
				// action may be retried.
				//
				(void) msg_handle.log(SC_SYSLOG_WARNING,
				    MESSAGE, "reservation error(%s) - "
				    "Could not determine controller number for "
				    "device %s", command, path);
				continue;
			}
			controller_num = atoi(temp2 + strlen("/rdsk/c"));
			if (controller_num < 0) {
				(void) msg_handle.log(SC_SYSLOG_WARNING,
				    MESSAGE, "reservation error(%s) - "
				    "Could not determine controller number for "
				    "device %s", command, path);
				continue;
			}

			// has this controller already been reset?
			already_reset = false;
			for (i = 0; i < MAX_CONTROLLERS_PER_NODE; i++) {
				if (controllers_reset[i] == -1)
					break;
				else if (controllers_reset[i] ==
				    controller_num) {
					already_reset = true;
					break;
				}
			}
			if (already_reset) {
				continue;
			}

			if (did_get_devid_type(transp) == DEVID_NONE) {
				// not a disk, ignore it
				continue;
			}

			if ((did_path = did_get_did_path(transp)) == NULL) {
				(void) msg_handle.log(SC_SYSLOG_WARNING,
				    MESSAGE, "reservation error(%s) - "
				    "did_get_did_path() error", command);
				continue;
			}
			(void) sprintf(comp_did_path, "/dev/did/%ss2",
			    did_path);
			free(did_path);

			// is it connected to a dead node?
			connected_to_dead_node = false;
			for (i = 0; i < num_dead_nodes; i++)
				if (strstr(path, &dead_nodes[MAX_STR_LEN * i])
				    != NULL) {
					connected_to_dead_node = true;
					break;
				}
			if (!connected_to_dead_node) {
				continue;
			}

			rt = NUM_RETRIES;
			fd = open(comp_did_path, O_RDONLY|O_NDELAY);
			errnum = errno;
			while ((fd == -1) && (rt--)) {
				//
				// SCMSGS
				// @explanation
				// The device fencing program has encountered
				// errors while trying to access a device. The
				// failed operation will be retried
				// @user_action
				// This is an informational message, no user
				// action is needed.
				//
				(void) msg_handle.log(SC_SYSLOG_WARNING,
				    MESSAGE, "reservation warning(%s) - "
				    "Unable to open device %s, errno %d, will "
				    "retry in %d seconds", command,
				    comp_did_path, errnum, RETRY_SLEEP);
				(void) sleep(RETRY_SLEEP);
				fd = open(comp_did_path, O_RDONLY|O_NDELAY);
				errnum = errno;
			}
			if (fd == -1) {
				//
				// SCMSGS
				// @explanation
				// The device fencing program has encountered
				// errors while trying to access a device. All
				// retry attempts have failed.
				// @user_action
				// This may be indicative of a hardware
				// problem, which should be resolved as soon
				// as possible. Once the problem has been
				// resolved, the following actions may be
				// necessary: If the message specifies the
				// 'node_join' transition, then this node may
				// be unable to access the specified device.
				// If the failure occurred during the
				// 'release_shared_scsi2' transition, then a
				// node which was joining the cluster may be
				// unable to access the device. In either
				// case, access can be reacquired by executing
				// '/usr/cluster/lib/sc/run_reserve -c
				// node_join' on all cluster nodes. If the
				// failure occurred during the 'make_primary'
				// transition, then a device group might have
				// failed to start on this node. If the device
				// group was started on another node, move it
				// to this node by using the cldevicegroup
				// command. If the device group was not
				// started, you can start it by using the
				// cldevicegroup command. If the failure
				// occurred during the 'primary_to_secondary'
				// transition, then the shutdown or switchover
				// of a device group might have failed. If so,
				// the desired action may be retried.
				//
				(void) msg_handle.log(SC_SYSLOG_WARNING,
				    MESSAGE, "reservation error(%s) - Unable to"
				    " open device %s, errno %d", command,
				    comp_did_path, errnum);
				continue;
			}

			// ignore this device if it is fibre-channel
			err = ioctl(fd, IOCDID_ISFIBRE, 0);
			errnum = errno;
			if ((err == 0) || (errnum != ENOENT)) {
				if (err != 0)
					//
					// SCMSGS
					// @explanation
					// The device fencing program has
					// suffered an internal error.
					// @user_action
					// Contact your authorized Sun service
					// provider to determine whether a
					// workaround or patch is available.
					// Copies of /var/adm/messages from
					// all nodes should be provided for
					// diagnosis. It may be possible to
					// retry the failed operation,
					// depending on the nature of the
					// error. If the message specifies the
					// 'node_join' transition, then this
					// node may be unable to access shared
					// devices. If the failure occurred
					// during the 'release_shared_scsi2'
					// transition, then a node which was
					// joining the cluster may be unable
					// to access shared devices. In either
					// case, it may be possible to
					// reacquire access to shared devices
					// by executing
					// '/usr/cluster/lib/sc/run_reserve -c
					// node_join' on all cluster nodes. If
					// the failure occurred during the
					// 'make_primary' transition, then a
					// device group has failed to start on
					// this node. If another node was
					// available to host the device group,
					// then it should have been started on
					// that node. If desired, it might be
					// possible to switch the device group
					// to this node by using the
					// cldevicegroup command. If no other
					// node was available, then the device
					// group will not have been started.
					// You can use the cldevicegroup
					// command to retry the attempt to
					// start the device group. If the
					// failure occurred during the
					// 'primary_to_secondary' transition,
					// then the shutdown or switchover of
					// a device group has failed. The
					// desired action may be retried.
					//
					(void) msg_handle.log(SC_SYSLOG_WARNING,
					    MESSAGE, "reservation error(%s) - "
					    "IOCDID_ISFIBRE failed for device "
					    "%s, errno %d", command,
					    comp_did_path, errnum);
				else {
					//
					// This is a fibre-channel device.  Go
					// ahead and mark the controller since
					// you can't mix fibre and scsi devices
					//  on the same bus.
					//
					for (i = 0;
					    i < MAX_CONTROLLERS_PER_NODE; i++)
						if (controllers_reset[i]
						    == -1) {
							controllers_reset[i] =
							    controller_num;
							break;
						}
				}
				(void) close(fd);
				continue;
			}

			if (verbose) {
				(void) printf("candidate disk `%s`\n",
				    comp_did_path);
				(void) printf("controller = `%d`\n",
				    controller_num);
			}

			// all criteria met, reset this bus
			if (!displayed_msg) {
				//
				// SCMSGS
				// @explanation
				// This is informational message.
				// @user_action
				// No user action required.
				//
				(void) msg_handle.log(SC_SYSLOG_WARNING,
				    MESSAGE, "resetting scsi buses shared with "
				    "non-cluster nodes\n");
				displayed_msg = true;
			}

			rt = NUM_RETRIES;
			bzero((caddr_t)&ucmd, sizeof (ucmd));
			ucmd.uscsi_flags = USCSI_RESET_ALL;
			err = ioctl(fd, USCSICMD, &ucmd);
			while ((err != 0) && (rt--)) {
				//
				// SCMSGS
				// @explanation
				// The device fencing program has encountered
				// errors while trying to access a device. The
				// failed operation will be retried
				// @user_action
				// This is an informational message, no user
				// action is needed.
				//
				(void) msg_handle.log(SC_SYSLOG_WARNING,
				    MESSAGE, "reservation warning(%s) - "
				    "USCSI_RESET failed for device %s, returned"
				    " %d, will retry in %d seconds", command,
				    comp_did_path, err, RETRY_SLEEP);
				(void) sleep(RETRY_SLEEP);
				bzero((caddr_t)&ucmd, sizeof (ucmd));
				ucmd.uscsi_flags = USCSI_RESET_ALL;
				err = ioctl(fd, USCSICMD, &ucmd);
				if (err == 0) {
					//
					// SCMSGS
					// @explanation
					// Informational message from reserve
					// on ioctl success during retry.
					// @user_action
					// No user action required.
					//
					(void) msg_handle.log(SC_SYSLOG_NOTICE,
					    MESSAGE, "reservation notice(%s) - "
					    "USCSI_RESET success for device %s,"
					    " during retry attempt: %d",
					    command, comp_did_path,
					    NUM_RETRIES - rt);
				}
			}
			if (err != 0) {
				//
				// SCMSGS
				// @explanation
				// The device fencing program has encountered
				// errors while trying to access a device. All
				// retry attempts have failed.
				// @user_action
				// This may be indicative of a hardware
				// problem, which should be resolved as soon
				// as possible. Once the problem has been
				// resolved, the following actions may be
				// necessary: If the message specifies the
				// 'node_join' transition, then this node may
				// be unable to access the specified device.
				// If the failure occurred during the
				// 'release_shared_scsi2' transition, then a
				// node which was joining the cluster may be
				// unable to access the device. In either
				// case, access can be reacquired by executing
				// '/usr/cluster/lib/sc/run_reserve -c
				// node_join' on all cluster nodes. If the
				// failure occurred during the 'make_primary'
				// transition, then a device group might have
				// failed to start on this node. If the device
				// group was started on another node, move it
				// to this node by using the cldevicegroup
				// command. If the device group was not
				// started, you can start it by using the
				// cldevicegroup command. If the failure
				// occurred during the 'primary_to_secondary'
				// transition, then the shutdown or switchover
				// of a device group might have failed. If so,
				// the desired action may be retried.
				//
				(void) msg_handle.log(SC_SYSLOG_WARNING,
				    MESSAGE, "reservation error(%s) - "
				    "USCSI_RESET failed for device %s, returned"
				    "%d", command, comp_did_path, err);
				//
				// We'll go ahead and mark this controller as
				// reset so we don't waste time on it with
				// other devices.
				//
			}

			(void) close(fd);

			// mark this controller as having been reset
			for (i = 0; i < MAX_CONTROLLERS_PER_NODE; i++)
				if (controllers_reset[i] == -1) {
					controllers_reset[i] = controller_num;
					break;
				}

		}
		if (path != NULL) {
			free(path);
		}
		free(search_for_host_name);
	} else if (strcmp(command, "grab_lock") == 0) {
		exit_status = grab_fencing_lock(fence_lock);
	} else if (strcmp(command, "release_lock") == 0) {
		exit_status = release_fencing_lock(fence_lock);
	} else if (strcmp(command, "check_lock") == 0) {
		exit_status = check_for_fencing(fence_lock, fence_lock_timeout,
		    fence_lock_retry);

	//
	// The following should never be called when a node is joining
	// or fencing, they are here purely for debugging. They allow
	// specific ioctls to manually be sent to specific disks.
	//
	} else if (strcmp(command, "scrub") == 0) {
		if (zd == -1) {
			(void) printf("Reserve: disk_path not specified\n");
			print_usage();
			goto main_outt;
		}
		if (do_scrub(zd, keylist, reg, regign, pre, 1) != -1)
			(void) printf("Scrubbing complete, use "
			    "'reserve -c inkeys -z %s' "
			    "to verify success\n", zdisk);
	} else if (strcmp(command, "status") == 0) {
		if (zd == -1) {
			(void) printf("Reserve: disk_path not specified\n");
			print_usage();
			goto main_outt;
		}
		(void) printf("status...%d\n", do_status(zd));
	} else if (strcmp(command, "enfailfast") == 0) {
		if (zd == -1) {
			(void) printf("Reserve: disk_path not specified\n");
			print_usage();
			goto main_outt;
		}
		(void) printf("do_enfailfast returned %d\n",
		    do_enfailfast(zd, INT_MAX));
	} else if (strcmp(command, "disfailfast") == 0) {
		if (zd == -1) {
			(void) printf("Reserve: disk_path not specified\n");
			print_usage();
			goto main_outt;
		}
		(void) printf("do_enfailfast returned %d\n",
		    do_enfailfast(zd, 0));
	} else if (strcmp(command, "release") == 0) {
		if (zd == -1) {
			(void) printf("Reserve: disk_path not specified\n");
			print_usage();
			goto main_outt;
		}
		(void) printf("do_scsi2_release %d\n", do_scsi2_release(zd));
	} else if (strcmp(command, "tkown") == 0) {
		if (zd == -1) {
			(void) printf("Reserve: disk_path not specified\n");
			print_usage();
			goto main_outt;
		}
		(void) printf("do_scsi2_tkown returned %d \n",
		    do_scsi2_tkown(zd));
	} else if (strcmp(command, "unregister") == 0) {
		if (zd == -1) {
			(void) printf("Reserve: disk_path not specified\n");
			print_usage();
			goto main_outt;
		}
		bzero(reg.newkey.key, (size_t)MHIOC_RESV_KEY_SIZE);
		(void) do_scsi3_register(zd, reg, keylist);
	} else if (strcmp(command, "register") == 0) {
		if (zd == -1) {
			(void) printf("Reserve: disk_path not specified\n");
			print_usage();
			goto main_outt;
		}
		if (do_scsi3_registerandignorekey(zd, regign) == -1) {
			(void) printf("MHIOCGRP_REGISTERANDIGNOREKEY failed, "
			    "attempting MHIOCGRP_REGISTER\n");
			(void) do_scsi3_register(zd, reg, keylist);
		}
	} else if (strcmp(command, "reserve") == 0) {
		if (zd == -1) {
			(void) printf("Reserve: disk_path not specified\n");
			print_usage();
			goto main_outt;
		}
		(void) printf("do_scsi3_reserve returned %d\n",
		    do_scsi3_reserve(zd, res));
	} else if (strcmp(command, "inkeys") == 0) {
		if (zd == -1) {
			(void) printf("Reserve: disk_path not specified\n");
			print_usage();
			goto main_outt;
		}
		if (do_scsi3_inkeys(zd, keylist, 0) != 0)
			(void) printf("do_scsi3_inkeys error\n");
		else {
			// print out keys
			(void) printf("Reservation keys(%d):\n",
			    keylist.li->listlen);
			for (i = 0; i < keylist.li->listlen; i++)
				print_scsi3_key(keylist.li->list[i]);
		}
	} else if (strcmp(command, "inresv") == 0) {
		if (zd == -1) {
			(void) printf("Reserve: disk_path not specified\n");
			print_usage();
			goto main_outt;
		}
		if (do_scsi3_inresv(zd, reslist) != 0)
			(void) printf("do_scsi-3_inresv error\n");
		else {
			// print out reservations
			(void) printf("Reservations(%d):\n",
			    reslist.li->listlen);
			for (i = 0; i < reslist.li->listlen; i++) {
				print_scsi3_key(reslist.li->list[i].key);
				(void) printf("---> %d\n",
				    reslist.li->list[i].type);
			}
		}
	} else if (strcmp(command, "preempt") == 0) {
		if (zd == -1) {
			(void) printf("Reserve: disk_path not specified\n");
			print_usage();
			goto main_outt;
		}
		//lint -e644  vickey (line 1047) may not have been initialized
		pre.victim_key = vickey;
		//lint +e644
		(void) printf("do_scsi3_preemptandabort returned %d\n",
		    do_scsi3_preemptandabort(zd, pre));
	} else {
		//
		// SCMSGS
		// @explanation
		// The device fencing program has suffered an internal error.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available. Copies of
		// /var/adm/messages from all nodes should be provided for
		// diagnosis. It may be possible to retry the failed
		// operation, depending on the nature of the error. If the
		// message specifies the 'node_join' transition, then this
		// node may be unable to access shared devices. If the failure
		// occurred during the 'release_shared_scsi2' transition, then
		// a node which was joining the cluster may be unable to
		// access shared devices. In either case, it may be possible
		// to reacquire access to shared devices by executing
		// '/usr/cluster/lib/sc/run_reserve -c node_join' on all
		// cluster nodes. If the failure occurred during the
		// 'make_primary' transition, then a device group has failed
		// to start on this node. If another node was available to
		// host the device group, then it should have been started on
		// that node. If desired, it might be possible to switch the
		// device group to this node by using the cldevicegroup
		// command. If no other node was available, then the device
		// group will not have been started. You can use the
		// cldevicegroup command to retry the attempt to start the
		// device group. If the failure occurred during the
		// 'primary_to_secondary' transition, then the shutdown or
		// switchover of a device group has failed. The desired action
		// may be retried.
		//
		(void) msg_handle.log(SC_SYSLOG_WARNING,
		    MESSAGE, "reservation fatal error(%s) - "
		    "Illegal command", command);
		(void) printf("Reserve: illegal command '%s'\n", command);
		print_usage();
		exit(1);
	}

main_outt:

	// clean up
	if (zdisk != NULL)
		(void) close(zd);
	if (release_shared_scsi2_cmd != NULL)
		free(release_shared_scsi2_cmd);
	free(keylist.li->list);
	free(reslist.li->list);

	for (diskp = my_disks; diskp != NULL; ) {
		tempp = diskp->next;
		free(diskp->did_path);
		free(diskp->nodes);
		free(diskp);
		diskp = tempp;
	}

	clconf_obj_release((clconf_obj_t *)cl);
	if (quor_table != NULL)
		cluster_release_quorum_status(quor_table);

	exit(exit_status);
}
