#! /usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident "@(#)sc-stnas-ctrl.ksh 1.2     08/05/20 SMI"
#
# This script is called by run_reserve when fencing or joining nodes with a 
# Sun StoredgeTek NAS. It performs the following actions:
#
# - calls sc-netapp-cfg to create the config file 
# - issues the fence cli command via rsh/ssh
#     fence -c {join | fence} -h node -d /export ... -d /export
# - removes the config file 
#
# The following calls into sc-stnas-ctrl are supported:
#
# sc-stnas-ctrl -c fence_node -f <fenced_node> -h <host_node>
# sc-stnas-ctrl -c node_join -h <host_node>
#
# 

config_prog=/usr/cluster/lib/sc/sc-netapp-cfg
config_file=/var/run/STCF_$$
stnas_cmd="fence"
stnas_fence="$stnas_cmd -c fence"
stnas_join="$stnas_cmd -c join"

##############
# script start
#####################################################
#
# This script depends on an ASCII collating
# sequence for checking things like legal node names,
# adapter names, and junction names.  There are no dependencies
# on the collating sequence for inspecting, sorting, or massaging any
# data which might be internationalized.  Therefore,
# the collating sequence locale is forced to the 'C' locale.
#
#####################################################
typeset -x TEXTDOMAIN=SUNW_SC_CMD
typeset -x TEXTDOMAINDIR=/usr/cluster/lib/locale

LC_COLLATE=C;  export LC_COLLATE


/usr/cluster/lib/sc/sc_zonescheck
if [ $? -ne 0 ]; then
	exit 1
fi

##########################
# get command line options
##########################
while getopts c:h:f: name
do

	case $name in
		c)	command="$OPTARG"
			;;
		h)	host_node="$OPTARG"
			;;
		f)	fenced_node="$OPTARG"
			;;
		?)	echo $0:  `gettext "illegal command line option"`
			exit 1
			;;
	esac
done

############################
# check command line options
############################
if [ -z $command ]
then
	echo $0:  `gettext "command not specified"`
	exit 1
fi

if [ $command == fence_node ]
then
	if [ -z $fenced_node ]
	then
		echo $0:  `gettext "node to fence not specified"`
		exit 1
	fi
fi

if [ $command == node_join ]
then
	if [ -z $host_node ]
	then
		echo $0:  `gettext "joining node not specified"`
		exit 1
	fi
fi

# setup config file - also checks to see if we have StoredgeTek storage configured
$config_prog -i $config_file -t sun
retval=$?
if [ $retval = 2 ]
then
	# no filers or no exports to fence
	exit 0
fi
if [ $retval != 0 ]
then
	echo $0: $config_prog `gettext "failure, returned"` $retval
	rm -f $config_file
	exit $retval
fi

# set up nas command 
if [ $command = fence_node ]
then
	echo `gettext "fencing node $fenced_node from shared Sun NAS devices"`
	
	stnas_cmd="$stnas_fence -h $fenced_node"
elif [ $command = node_join ]
then
	echo `gettext "obtaining access to shared Sun NAS devices"`
	stnas_cmd="$stnas_join -h $host_node"
else
	echo $0:  `gettext "illegal command specification:"` -c $command
	rm -f $config_file
	exit 1
fi

#
# make sure config file exists and is readable
if [ ! -f $config_file ]
then 
	echo `gettext "Configuration file $config_file does not exist"`
	exit 1
elif [ ! -r $config_file ]
then
	echo `gettext "Cannot read configuration file $config_file"`
  	exit 1
fi

#
# read config file and issue command once per nas
# this assumes entries are grouped by filer
#
exports=""
this_nas=""

exec 3< $config_file
until [ $last_filer ]
do
	read <&3 stnas export
	if [ $? != 0 ]
	then
		# end of file, issue command
		if [ $this_nas ]
		then
			rsh $this_nas $stnas_cmd $exports
		fi
		last_filer=1
		continue
	fi

	if [ -z $stnas ] | [ -z $export ]
	then
		continue
	fi

	if [ "$this_nas" != "$stnas" ]
	then
		# new nas server 
		# if not first time, notify nas
		if [ $this_nas ]
		then
			rsh $this_nas $stnas_cmd $exports
		fi
		this_nas=$stnas			
		exports="-d $export "
	else
		exports="$exports -d $export "
	fi
done

# remove config file
rm -f $config_file

exit 0
