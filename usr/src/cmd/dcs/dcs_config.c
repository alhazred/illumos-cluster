/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)dcs_config.c	1.32	08/05/20 SMI"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ddi.h>
#include <dc/libdcs/libdcs.h>
#include <sys/sol_version.h>


void
usage(char *pgm) {
	(void) printf("Usage:\n"
	    "%s	-c [info]\n"
	    "		[-s service_name]\n"
	    "		[-C service_class]\n"
	    "		[-d device_path]\n"
	    "%s	-c [create | config | remove | status | startup | switchover | "
	    "shutdown | suspend | resume]\n"
	    "		-s service_name\n"
	    "		[-C service_class]\n"
	    "		[-f failback_enabled]\n"
	    "		[-n number_of_secondaries]\n"
	    "		[-i nodeid [-p preference]]\n"
	    "		[-d device_path]\n"
	    "		[-N property_name -V property_value]\n"
	    "%s	-c [create | config | remove | info ]\n"
	    "		-C service_class_name\n"
	    "		-u takeover_program\n"
	    "		-t [transparent | eio]\n"
	    "%s -c mknodes -m dev -F friendly_name\n",
	    pgm, pgm, pgm, pgm);
}

#define	CREATE 1
#define	CONFIGURE 2
#define	REMOVE 3
#define	INFO 4
#define	STATUS 5
#define	SWITCHOVER 6
#define	SHUTDOWN 7
#define	SUSPEND 8
#define	RESUME 9
#define	STARTUP 10
#define	MKNODES 11

#define	PRIMARY_STATE "Primary"
#define	SECONDARY_STATE "Secondary"
#define	SPARE_STATE "Spare"
#define	INACTIVE_STATE "Inactive"
#define	TRANSITION_STATE "Transition"

#define	DBG_PRT(a)	if (debug_mode)			\
					(void) printf a

#define	MAX_PROPERTIES	10


int
main(int argc, char *argv[]) {

	int error = 0;
	int c;
	char *service_name = NULL;
	char *user_program = NULL;
	char *service_class = NULL;
	unsigned int num_secs = 0;
	int failback_enabled = 0;
	dc_replica_t *nodes;
	dc_gdev_range_t gdev_ranges[DCS_GDEVS_MAX];
	int curr_node_num = -1;
	int curr_gdev_range_num = -1;
	dc_ha_type_t ha_type;
	int command;
	struct stat file_stat;
	dc_replica_seq_t *nodes_seq;
	dc_gdev_range_seq_t *gdevs_range_seq;
	unsigned int i = 0;
	dc_replica_status_seq_t *active_replicas;
	int is_suspended;
	dc_error_t dc_err = 0;
	dev_t curr_dev = (dev_t)0;
	major_t maj;
	minor_t start_min, end_min;
	int debug_mode = 0;
	unsigned int nodes_max = 64;
	int tmp_int;
	sc_state_code_t state;
	int command_specified = 0;
	int service_specified = 0;
	int user_program_specified = 0;
	int service_class_specified = 0;
	int ha_type_specified = 0;
	int num_secs_specified = 0;
	int failback_mode_specified = 0;
	char *property_names[MAX_PROPERTIES];
	char *property_values[MAX_PROPERTIES];
	unsigned int property_count = 0;
	int property_name_specified = 0;
	dc_property_seq_t *property_seq;
	char *service_state;
	dc_string_seq_t *service_names;
	int major_specified = 0;
	dev_t major;
	char *device_name = NULL;
	int device_name_specified = 0;

	/*lint -e746 dcs_initialize() not made in the presence of a prototype */
	if ((error = dcs_initialize()) != 0) {
	/*lint +e746 */
#ifdef	DEBUG
		(void) printf("Could not initialize library\n");
#endif
		return (error);
	}

	nodes = malloc(sizeof (dc_replica_t) * nodes_max);

	while ((c = getopt(argc, argv, "?DC:N:V:c:d:e:f:i:n:p:s:t:u:m:q:F:"))
	    != EOF) {
		switch (c) {
		case 'D':
			debug_mode = 1;
			break;

		case 'c':
			/* Command */
			DBG_PRT(("Command --> %s\n", optarg));
			if (strncmp(optarg, "create", 2) == 0) {
				command = CREATE;
				command_specified = 1;
			} else if (strncmp(optarg, "config", 2) == 0) {
				command = CONFIGURE;
				command_specified = 1;
			} else if (strncmp(optarg, "remove", 3) == 0) {
				command = REMOVE;
				command_specified = 1;
			} else if (strncmp(optarg, "info", 1) == 0) {
				command = INFO;
				command_specified = 1;
			} else if (strncmp(optarg, "status", 4) == 0) {
				command = STATUS;
				command_specified = 1;
			} else if (strncmp(optarg, "switchover", 2) == 0) {
				command = SWITCHOVER;
				command_specified = 1;
			} else if (strncmp(optarg, "shutdown", 2) == 0) {
				command = SHUTDOWN;
				command_specified = 1;
			} else if (strncmp(optarg, "suspend", 2) == 0) {
				command = SUSPEND;
				command_specified = 1;
			} else if (strncmp(optarg, "resume", 3) == 0) {
				command = RESUME;
				command_specified = 1;
			} else if (strncmp(optarg, "startup", 4) == 0) {
				command = STARTUP;
				command_specified = 1;
			} else if (strncmp(optarg, "mknodes", 2) == 0) {
				command = MKNODES;
				command_specified = 1;
			} else {
				usage(argv[0]);
				return (1);
			}
			break;

		case 'N':
			/* Property name */
			if (property_name_specified != 0) {
				usage(argv[0]);
				return (1);
			}
			if (property_count >= MAX_PROPERTIES) {
				(void) printf("Too many properties "
				    "specified!\n");
				return (1);
			}
			property_name_specified = 1;
			property_names[property_count] = optarg;
			break;

		case 'V':
			/* Property value */
			if (property_name_specified == 0) {
				usage(argv[0]);
				return (1);
			}
			property_name_specified = 0;
			property_values[property_count] = optarg;
			property_count++;
			break;

		case 's':
			/* Service name */
			DBG_PRT(("Service name --> %s\n", optarg));
			service_name = optarg;
			service_specified = 1;
			break;

		case 'u':
			/* User program */
			DBG_PRT(("User Program --> %s\n", optarg));
			user_program = optarg;
			user_program_specified = 1;
			break;

		case 'C':
			/* Service class name */
			DBG_PRT(("Service Class --> %s\n", optarg));
			service_class = optarg;
			service_class_specified = 1;
			break;

		case 'f':
			/* failback mode */

			/*lint -e746 */
			errno = 0;
			/*lint +e746 */
			DBG_PRT(("Failback mode --> %s\n", optarg));
			failback_enabled = atoi(optarg);
			failback_mode_specified = 1;
			if (errno != 0) {
				usage(argv[0]);
				return (1);
			}
			break;

		case 'n':
			/* Number of secondaries */
			errno = 0;
			tmp_int = atoi(optarg);
			num_secs_specified = 1;
			if (errno != 0) {
				usage(argv[0]);
				return (1);
			}
			if (tmp_int < 0) {
				usage(argv[0]);
				return (1);
			}
			num_secs = (unsigned int) tmp_int;
			break;

		case 'i':
			/* Node id */
			curr_node_num++;
			if ((unsigned int) curr_node_num >= nodes_max) {
				(void) printf("Exceeded maximum number of "
				    "nodes!\n");
				return (1);
			}
			errno = 0;
			tmp_int = atoi(optarg);
			nodes[curr_node_num].preference = 0;
			if (errno != 0) {
				usage(argv[0]);
				return (1);
			}
			if (tmp_int < 0) {
				usage(argv[0]);
				return (1);
			}
			nodes[curr_node_num].id = (nodeid_t)tmp_int;
			break;

		case 'p':
			/* Node preference */
			if (curr_node_num < 0) {
				usage(argv[0]);
				return (1);
			}
			errno = 0;
			tmp_int = atoi(optarg);
			if (errno != 0) {
				usage(argv[0]);
				return (1);
			}
			if (tmp_int < 0) {
				usage(argv[0]);
				return (1);
			}
			nodes[curr_node_num].preference =
			    (unsigned int) tmp_int;
			break;

		case 'd':
			/* Device path */
			error = stat(optarg, &file_stat);
			if (error != 0) {
				(void) printf("Could not stat (%s)\n", optarg);
				return (DCS_ERR_DEVICE_INVAL);
			}
			if (((file_stat.st_mode & S_IFMT) != S_IFCHR) &&
			    ((file_stat.st_mode & S_IFMT) != S_IFBLK)) {
				(void) printf("(%s) is not a device special "
				    "file\n", optarg);
				return (DCS_ERR_DEVICE_INVAL);
			}
			if (curr_dev != (dev_t)0) {
				maj = getemajor(curr_dev);
				start_min = geteminor(curr_dev);
				curr_gdev_range_num++;
				gdev_ranges[curr_gdev_range_num].maj = maj;
				gdev_ranges[curr_gdev_range_num].end_gmin =
				    start_min;
				gdev_ranges[curr_gdev_range_num].start_gmin =
				    start_min;
			}
			curr_dev = file_stat.st_rdev;
			break;

		case 'e':
			/* Device path */
			error = stat(optarg, &file_stat);
			if (error != 0) {
				(void) printf("Could not stat (%s)\n", optarg);
				return (DCS_ERR_DEVICE_INVAL);
			}
			if (((file_stat.st_mode & S_IFMT) != S_IFCHR) &&
			    ((file_stat.st_mode & S_IFMT) != S_IFBLK)) {
				(void) printf("(%s) is not a device special "
				    "file\n", optarg);
				return (DCS_ERR_DEVICE_INVAL);
			}
			if (curr_dev == (dev_t)0) {
				(void) printf("End of range specified before "
				    "start\n");
				return (DCS_ERR_DEVICE_INVAL);
			}
			maj = getemajor(curr_dev);
			start_min = geteminor(curr_dev);
			end_min = geteminor(file_stat.st_rdev);
			if ((maj != getemajor(file_stat.st_rdev)) ||
			    (start_min > end_min)) {
				(void) printf("Invalid range specified\n");
				return (DCS_ERR_DEVICE_INVAL);
			}
			curr_gdev_range_num++;
			gdev_ranges[curr_gdev_range_num].maj = maj;
			gdev_ranges[curr_gdev_range_num].start_gmin = start_min;
			gdev_ranges[curr_gdev_range_num].end_gmin = end_min;
			curr_dev = (dev_t)0;
			break;

		case 't':
			/* HA type */
			if (strcmp(optarg, "transparent") == 0) {
				ha_type = TRANSPARENT_FAILOVER;
				ha_type_specified = 1;
			} else if (strcmp(optarg, "eio") == 0) {
				ha_type = EIO_FAILOVER;
				ha_type_specified = 1;
			} else {
				usage(argv[0]);
				return (1);
			}
			break;

		case 'm':
			/* Major device number */
			errno = 0;
			tmp_int = atoi(optarg);
			major_specified = 1;
			if (errno != 0) {
				usage(argv[0]);
				return (1);
			}
			if (tmp_int < 0) {
				usage(argv[0]);
				return (1);
			}
			major = (dev_t)tmp_int;
			break;

		case 'F':
			/* Friendly name */
			device_name = optarg;
			device_name_specified = 1;


		case '?':
		default:
			usage(argv[0]);
			return (1);
		}
	}

	if (property_name_specified != 0) {
		/* User specified the property name but not the value */
		usage(argv[0]);
		return (1);
	}

	if (!command_specified) {
		usage(argv[0]);
		return (1);
	}

	if (curr_dev != (dev_t)0) {
		maj = getemajor(curr_dev);
		start_min = geteminor(curr_dev);
		curr_gdev_range_num++;
		gdev_ranges[curr_gdev_range_num].maj = maj;
		gdev_ranges[curr_gdev_range_num].end_gmin = start_min;
		gdev_ranges[curr_gdev_range_num].start_gmin = start_min;
	}

	/*lint -e644 command may not have been initialized */
	if (command == CREATE) {
	/*lint +e644 */
		DBG_PRT(("command == CREATE\n"));

		if (!service_specified) {
			if ((!service_class_specified) ||
			    (!ha_type_specified) || (!user_program_specified)) {
				usage(argv[0]);
				return (1);
			}
			/*lint -e644 ha_type may not have been initialized */
			dc_err = dcs_create_service_class(service_class,
			    user_program, ha_type);
			/*lint +e644 */
			if (dc_err != DCS_SUCCESS) {
				(void) printf("Error: %s\n",
				    dcs_error_to_string(dc_err));
			}
			return (dc_err);
		}

		if (!service_class_specified) {
			if ((ha_type_specified) && (ha_type == EIO_FAILOVER)) {
				service_class = "TAPE";
				service_class_specified = 1;
			} else if (user_program_specified) {
				/*lint -e668 Possibly passing a null pointer */
				if (strcmp(user_program, "/bin/true") == 0) {
					service_class = "DISK";
					service_class_specified = 1;
				} else if (strcmp(user_program,
				    "/usr/cluster/lib/devsrvcs/sds_ownership")
				    == 0) {
					service_class = "SUNWmd";
					service_class_specified = 1;
				}
				/*lint +e668 */
			}
		}

		if (!service_class_specified) {
			usage(argv[0]);
			return (1);
		}

		nodes_seq = (dc_replica_seq_t *)
		    malloc(sizeof (dc_replica_seq_t));
		nodes_seq->replicas = nodes;
		nodes_seq->count = (unsigned int) curr_node_num+1;

		gdevs_range_seq = (dc_gdev_range_seq_t *)
		    malloc(sizeof (dc_gdev_range_seq_t));
		gdevs_range_seq->ranges = gdev_ranges;
		gdevs_range_seq->count = (unsigned int) curr_gdev_range_num+1;

		property_seq = (dc_property_seq_t *)
		    malloc(sizeof (dc_property_seq_t));
		property_seq->count = property_count;
		if (property_count > 0) {
			/*
			 * Properties were specified - place them in the
			 * structure.
			 */
			property_seq->properties =
			    malloc(sizeof (dc_property_t) * property_count);
			for (i = 0; i < property_count; i++) {
				/*lint -e644 */
				(property_seq->properties)[i].name =
				    property_names[i];
				(property_seq->properties)[i].value =
				    property_values[i];
				/*lint +e644 */
			}
		} else {
			property_seq->properties = NULL;
		}

		DBG_PRT(("Calling dcs_create_service\n"));
		dc_err = dcs_create_service(service_name, service_class,
		    failback_enabled, nodes_seq, num_secs, gdevs_range_seq,
		    property_seq);

		free(nodes_seq);
		free(gdevs_range_seq);
		if (property_seq->properties != NULL) {
			free(property_seq->properties);
		}
		free(property_seq);

		if (dc_err != DCS_SUCCESS) {
			(void) printf("Error: %s\n",
			    dcs_error_to_string(dc_err));
			return (dc_err);
		}
		return (0);
	}

	if (command == STATUS) {
		unsigned int service_cnt;
		if (!service_specified) {
			dc_err = dcs_get_service_names(&service_names);
			if (dc_err != DCS_SUCCESS) {
				(void) printf("Error from "
				    "dcs_get_service_names: %s\n",
				    dcs_error_to_string(dc_err));
				return (dc_err);
			}
			if (service_names->count == 0) {
				(void) printf("There are no configured "
				    "services.");
				service_name = NULL;
			} else {
				service_cnt = 0;
				service_name =
				    (service_names->strings)[service_cnt];
			}
		}

		for (; service_name != NULL; ) {
			dc_err = dcs_get_service_status(service_name,
			    &is_suspended, &active_replicas, &state, NULL);
			if (dc_err != DCS_SUCCESS) {
				(void) printf("Error from "
				    "dcs_get_service_status: "
				    "%s\n", dcs_error_to_string(dc_err));
				return (dc_err);
			}

			if (service_specified) {
				(void) printf("Service Name: ");
			}
			(void) printf("%s\n", service_name);
			if (!service_specified) {
				(void) printf("\t");
			}
			(void) printf("Active replicas: ");
			for (i = 0; i < active_replicas->count; i++) {
				switch ((active_replicas->replicas)[i].state) {
				case PRIMARY:
					service_state = PRIMARY_STATE;
					break;
				case SECONDARY:
					service_state = SECONDARY_STATE;
					break;
				case SPARE:
					service_state = SPARE_STATE;
					break;
				case INACTIVE:
					service_state = INACTIVE_STATE;
					break;
				case TRANSITION:
					service_state = TRANSITION_STATE;
					break;
				default:
					service_state = "Unknown";
					break;
				}
				(void) printf("(%d. State - %s)",
				    (active_replicas->replicas)[i].id,
				    service_state);
			}
			(void) printf("\n");
			if (!service_specified) {
				(void) printf("\t");
			}
			(void) printf("Service state: ");
			switch (state) {
			case SC_STATE_ONLINE:
				if (is_suspended)
					(void) printf(
					    "SC_STATE_ONLINE (SUSPENDED)");
				else
					(void) printf("SC_STATE_ONLINE");
				break;
			case SC_STATE_OFFLINE:
				if (is_suspended)
					(void) printf(
					    "SC_STATE_OFFLINE (SUSPENDED)");
				else
					(void) printf("SC_STATE_OFFLINE");
				break;
			case SC_STATE_FAULTED:
				(void) printf("SC_STATE_FAULTED");
				break;
			case SC_STATE_DEGRADED:
				(void) printf("SC_STATE_DEGRADED");
				break;
			case SC_STATE_WAIT:
				(void) printf("SC_STATE_WAIT");
				break;
			case SC_STATE_UNKNOWN:
				(void) printf("SC_STATE_UNKNOWN");
				break;
			case SC_STATE_NOT_MONITORED:
				(void) printf("Unexpected state(%d)", state);
				break;
			default:
				(void) printf("Unknown state(%d)?", state);
			}
			(void) printf("\n\n");

			dcs_free_dc_replica_status_seq(active_replicas);
			if (service_specified) {
				break;
			} else {
				/*lint -e644 */
				service_cnt++;
				if (service_names->count <= service_cnt) {
				/*lint +e644 */
					service_name = NULL;
				} else {
					service_name = (service_names->strings)
					    [service_cnt];
				}
			}
		}

		if (!service_specified) {
			dcs_free_string_seq(service_names);
		}
		return (0);
	}

	if (command == INFO) {
		if (curr_dev != (dev_t)0) {
			dc_err = dcs_get_service_name_by_dev_t(
			    getemajor(curr_dev), geteminor(curr_dev),
			    &service_name);
			if (dc_err == DCS_SUCCESS) {
				(void) printf("Device (%d, %d) belongs to "
				    "service %s\n", getemajor(curr_dev),
				    geteminor(curr_dev), service_name);
				dcs_free_string(service_name);
			} else {
				(void) printf("Error: %s\n",
				    dcs_error_to_string(dc_err));
			}
			return (dc_err);
		}
		if (!service_specified) {
			dc_string_seq_t *service_class_names;
			if (!service_class_specified) {
				dc_err = dcs_get_service_names(&service_names);
				if (dc_err == DCS_SUCCESS) {
					dc_err = dcs_get_service_class_names(
					    &service_class_names);
				}
				if (dc_err != DCS_SUCCESS) {
					(void) printf("Error: %s\n",
					    dcs_error_to_string(dc_err));
					return (dc_err);
				}
				(void) printf("Services classes are :\n");
				/*lint -e644 */
				for (i = 0; i < service_class_names->count;
				/*lint +e644 */
				    i++) {
					(void) printf("    %s\n",
					    (service_class_names->strings)[i]);
				}
				(void) printf("Services are :\n");
				for (i = 0; i < service_names->count; i++) {
					(void) printf("    %s\n",
					    (service_names->strings)[i]);
				}
				dcs_free_string_seq(service_class_names);
				dcs_free_string_seq(service_names);
			} else {
				DBG_PRT(("Before call to "
				    "dcs_get_service_class_parameters\n"));
				dc_err = dcs_get_service_class_parameters(
				    service_class, &user_program, &ha_type);
				if (dc_err == DCS_SUCCESS) {
					DBG_PRT(("Before call to dcs_get"
					    "_service_names_of_class\n"));
					dc_err =
					    dcs_get_service_names_of_class(
					    service_class, &service_names);
					DBG_PRT(("After call to dcs_get"
					    "_service_names_of_class\n"));
				}
				if (dc_err != DCS_SUCCESS) {
					(void) printf("Error: %s\n",
					    dcs_error_to_string(dc_err));
					return (dc_err);
				}
				(void) printf("Service class\t--> %s\n",
				    service_class);
				(void) printf("Takeover pgm\t--> %s\n",
				    user_program);
				(void) printf("Failover type\t--> %s\n",
				    ((ha_type == 0) ? "transparent" : "eio"));
				(void) printf("Services :\n");
				for (i = 0; i < service_names->count; i++) {
					(void) printf("    %s\n",
					    (service_names->strings)[i]);
				}
				dcs_free_string(user_program);
				dcs_free_string_seq(service_names);
			}
			return (0);
		}

		dc_err = dcs_get_service_parameters(service_name,
		    &service_class, &failback_enabled, &nodes_seq, &num_secs,
		    &gdevs_range_seq, &property_seq);

		if (dc_err != DCS_SUCCESS) {
			(void) printf("Error: %s\n",
			    dcs_error_to_string(dc_err));
			return (dc_err);
		}
		(void) printf("Service name: %s\n", service_name);
		(void) printf("Service class: %s\n", service_class);
		(void) printf("Switchback Enabled: %s\n",
		    ((failback_enabled == 0) ? "False" : "True"));
		if (num_secs == 0) {
			(void) printf("Number of secondaries: All\n");
		} else {
			(void) printf("Number of secondaries: %d\n", num_secs);
		}
		(void) printf("Replicas: ");
		for (i = 0; i < nodes_seq->count; i++) {
			(void) printf("(Node id --> %d, Preference --> %d)",
			    (nodes_seq->replicas)[i].id,
			    (nodes_seq->replicas)[i].preference);
		}
		(void) printf("\n");
		(void) printf("Devices: ");
		for (i = 0; i < gdevs_range_seq->count; i++) {
			(void) printf("(%d, %d-%d) ",
			    (gdevs_range_seq->ranges)[i].maj,
			    (gdevs_range_seq->ranges)[i].start_gmin,
			    (gdevs_range_seq->ranges)[i].end_gmin);
		}
		(void) printf("\n");
		(void) printf("Properties:\n");
		for (i = 0; i < property_seq->count; i++) {
			(void) printf("	%s --> %s\n",
			    (property_seq->properties)[i].name,
			    (property_seq->properties)[i].value);
		}
		dcs_free_string(service_class);
		dcs_free_dc_replica_seq(nodes_seq);
		dcs_free_dc_gdev_range_seq(gdevs_range_seq);
		dcs_free_properties(property_seq);
		return (0);
	}

	if (command == SWITCHOVER) {
		if (curr_node_num < 0) {
			nodes[0].id = 0;
		}
		dc_err = dcs_switchover_service(service_name, nodes[0].id);
		if (dc_err != DCS_SUCCESS) {
			(void) printf("Error: %s\n",
			    dcs_error_to_string(dc_err));
			return (dc_err);
		}
		return (0);
	}

	if (command == STARTUP) {
		if (curr_node_num < 0) {
			nodes[0].id = 0;
		}
		dc_err = dcs_startup_service(service_name, nodes[0].id);
		if (dc_err != DCS_SUCCESS) {
			(void) printf("Error: %s\n",
			    dcs_error_to_string(dc_err));
		}
		return (dc_err);
	}

	if (command == SHUTDOWN) {
		dc_err = dcs_shutdown_service(service_name, 0);
		if (dc_err != DCS_SUCCESS) {
			(void) printf("Error: %s\n",
			    dcs_error_to_string(dc_err));
		}
		return (dc_err);
	}

	if (command == SUSPEND) {
		dc_err = dcs_shutdown_service(service_name, 1);
		if (dc_err != DCS_SUCCESS) {
			(void) printf("Error: %s\n",
			    dcs_error_to_string(dc_err));
		}
		return (dc_err);
	}

	if (command == RESUME) {
		dc_err = dcs_resume_service(service_name);
		if (dc_err != DCS_SUCCESS) {
			(void) printf("Error: %s\n",
			    dcs_error_to_string(dc_err));
		}
		return (dc_err);
	}

	if (command == CONFIGURE) {

		if (!service_specified) {
			if (!service_class_specified) {
				usage(argv[0]);
				return (1);
			}
			if (user_program_specified) {
				dc_err = dcs_set_service_class_user_program(
				    service_class, user_program);
				if (dc_err != DCS_SUCCESS) {
					(void) printf("Error: %s\n",
					    dcs_error_to_string(dc_err));
					return (dc_err);
				}
			}
			if (ha_type_specified) {
				dc_err = dcs_set_service_class_ha_type(
				    service_class, ha_type);
				if (dc_err != DCS_SUCCESS) {
					(void) printf("Error: %s\n",
					    dcs_error_to_string(dc_err));
					return (dc_err);
				}
			}
			return (dc_err);
		}

		/*
		 * The user may be trying to add nodes, add devices, set the
		 * failback mode, or set the number of secondaries.
		 */
		if (failback_mode_specified) {
			dc_err = dcs_set_failback_mode(service_name,
			    failback_enabled);
			if (dc_err != DCS_SUCCESS) {
				(void) printf("Error: %s\n",
				    dcs_error_to_string(dc_err));
				return (dc_err);
			}
		}

		if (num_secs_specified) {
			dc_err = dcs_set_active_secondaries(service_name,
			    num_secs);
			if (dc_err != DCS_SUCCESS) {
				(void) printf("Error: %s\n",
				    dcs_error_to_string(dc_err));
				return (dc_err);
			}
		}

		if (curr_node_num >= 0) {
			for (i = 0; i <= (unsigned int) curr_node_num; i++) {
				dc_err = dcs_add_node(service_name,
				    &(nodes[i]));
				if (dc_err != DCS_SUCCESS) {
					(void) printf("Error: %s\n",
					    dcs_error_to_string(dc_err));
					return (dc_err);
				}
			}
		}

		if (curr_gdev_range_num >= 0) {
			gdevs_range_seq = (dc_gdev_range_seq_t *)
			    malloc(sizeof (dc_gdev_range_seq_t));
			gdevs_range_seq->ranges = gdev_ranges;
			gdevs_range_seq->count =
			    (unsigned int) curr_gdev_range_num+1;

			dc_err = dcs_add_devices(service_name, gdevs_range_seq);

			free(gdevs_range_seq);

			if (dc_err != DCS_SUCCESS) {
				(void) printf("Error: %s\n",
				    dcs_error_to_string(dc_err));
				return (dc_err);
			}
		}

		if (property_count > 0) {
			/*
			 * Properties were specified - place them in the
			 * structure.
			 */
			property_seq = (dc_property_seq_t *)
			    malloc(sizeof (dc_property_seq_t));
			property_seq->count = property_count;
			property_seq->properties =
			    malloc(sizeof (dc_property_t) * property_count);
			for (i = 0; i < property_count; i++) {
				(property_seq->properties)[i].name =
				    property_names[i];
				(property_seq->properties)[i].value =
				    property_values[i];
			}

			dc_err = dcs_set_properties(service_name, property_seq);

			free(property_seq->properties);
			free(property_seq);

			if (dc_err != DCS_SUCCESS) {
				(void) printf("Error: %s\n",
				    dcs_error_to_string(dc_err));
				return (dc_err);
			}
		}

		return (dc_err);
	}

	if (command == REMOVE) {
		/*
		 * You can remove a service class, a service, a device, a
		 * node, or a property.
		 */
		if ((!service_specified) && (!service_class_specified)) {
			usage(argv[0]);
			return (1);
		}
		if (service_class_specified) {
			dc_err = dcs_remove_service_class(service_class);
			if (dc_err != DCS_SUCCESS) {
				(void) printf("Error: %s\n",
				    dcs_error_to_string(dc_err));
				return (dc_err);
			}
		}
		if (!service_specified) {
			return (0);
		}
		if ((curr_node_num < 0) && (curr_gdev_range_num < 0) &&
		    (property_count == 0)) {
			/* Remove the service */
			dc_err = dcs_remove_service(service_name);
			if (dc_err != DCS_SUCCESS) {
				(void) printf("Error: %s\n",
				    dcs_error_to_string(dc_err));
				return (dc_err);
			}
		}
		if (curr_node_num >= 0) {
			for (i = 0; i <= (unsigned int) curr_node_num; i++) {
				dc_err = dcs_remove_node(service_name,
				    nodes[i].id);
				if (dc_err != DCS_SUCCESS) {
					(void) printf("Error: %s\n",
					    dcs_error_to_string(dc_err));
					return (dc_err);
				}
			}
		}

		if (curr_gdev_range_num >= 0) {
			gdevs_range_seq = (dc_gdev_range_seq_t *)
			    malloc(sizeof (dc_gdev_range_seq_t));
			gdevs_range_seq->ranges = gdev_ranges;
			gdevs_range_seq->count =
			    (unsigned int) curr_gdev_range_num+1;

			dc_err = dcs_remove_devices(service_name,
			    gdevs_range_seq);

			free(gdevs_range_seq);

			if (dc_err != DCS_SUCCESS) {
				(void) printf("Error: %s\n",
				    dcs_error_to_string(dc_err));
				return (dc_err);
			}
		}

		if (property_count > 0) {
			/*
			 * Properties were specified - place them in the
			 * structure.
			 */
			dc_string_seq_t string_seq;
			char **curr_string;

			string_seq.count = property_count;
			string_seq.strings = (char **)
			    malloc(sizeof (char *) * property_count);

			curr_string = string_seq.strings;
			for (i = 0; i < property_count; i++, curr_string++) {
				*curr_string = property_names[i];
			}

			dc_err = dcs_remove_properties(service_name,
			    &string_seq);

			free(string_seq.strings);

			if (dc_err != DCS_SUCCESS) {
				(void) printf("Error: %s\n",
				    dcs_error_to_string(dc_err));
				return (dc_err);
			}
		}
		return (0);
	}

#if SOL_VERSION == __s10
	if (command == MKNODES) {
		dc_err = dcs_create_device_nodes(major, &property_seq);
		if (dc_err != DCS_SUCCESS) {
			(void) printf("Error: %s\n",
			    dcs_error_to_string(dc_err));
			return (dc_err);
		}
		return (0);
	}
#endif /* SOL_VERSION == __s10 */
#if SOL_VERSION > __s10
	if (command == MKNODES) {
		dc_err = dcs_create_device_nodes(major, &property_seq,
								device_name);
		if (dc_err != DCS_SUCCESS) {
			(void) printf("Error: %s\n",
			    dcs_error_to_string(dc_err));
			return (dc_err);
		}
		return (0);
	}
#endif /* SOL_VERSION > __s10 */

	(void) printf("Not implemented\n");

	return (0);
}
