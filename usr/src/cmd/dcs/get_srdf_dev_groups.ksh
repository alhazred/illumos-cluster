#! /usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident "@(#)get_srdf_dev_groups.ksh 1.3     08/05/20 SMI"
#
#
# This script is a no-op until we can get auto-merging working for SRDF, which
# requires an interface from EMC which will allow us to get the device path for
# both device replicas from a single host.
#
# This script may currently be called from 'scdidadm -T' (and therefor 'cldevice
# replicate') even though that is not currently supported for srdf (if the user
# tries, no SRDF devices will be returned and nothing will be merged).  It is
# also called via 'scdidadm -t' (cldevice combine) which is the supported way
# for setting up SRDF devices, but this script needs to do nothing extra, since
# the replica device group for SRDF must be provided via a command line option
# (again due to SRDF CLI limitations).

##########################
# get command line options
##########################
while getopts f:r: name
do

	case $name in
		f)	outfile="$OPTARG"
			;;
		r)	remote_host="$OPTARG"
			;;
		?)	echo $0:  `gettext "illegal command line option"`
			exit 1
			;;
	esac
done

if [ -z $outfile ]
then
	echo "$(gettext 'Output file not specified.')\n"
	exit 1
fi

rm -f $outfile
touch $outfile

exit 0
