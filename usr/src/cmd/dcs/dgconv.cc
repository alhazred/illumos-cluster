//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma	ident	"@(#)dgconv.cc 1.6	08/05/20 SMI"

#include <stdlib.h>
#include <stdio.h>
#include <strings.h>
#include <sys/clconf_int.h>
#include <dc/libdcs/libdcs.h>

//
// This utility take a did instance as an argument and displays the name of the
// rawdisk device group that did instance belongs to.  Optionally, the key word
// 'all' may replace the did instance, in which case a list of all did instances
// and their corresponding rawdisk device groups is returned.  If no rawdisk
// device group using this device is found nothing is displayed.  If an
// unexpected error is encountered, an appropriate error message is displayed.
// This utilty exits with value of 0 if no errors were encountered, 1 if an
// error was encountered.  If there are no rawdisk device groups in the cluster
// no output is generated and the utilty returns a value of 0.
//
// Examples :
//
// # dgconv -d d2
// dsk/d2
//
// # dgconv -d all
// d1 dsk/d1
// d2 dsk/d2
// d3 dsk/d3
//   .
//   .
//   .
//

int
main(int argc, char **argv)
{
	int		c;
	unsigned int	i;
	char		*did_instance = NULL;
	char		*rawdisk_class = "DISK";
	dc_string_seq_t *services;
	char		*prop;
	char		*gdev;

	while ((c = getopt(argc, argv, "d:")) != EOF) {
		switch (c) {
		case 'd' :
			did_instance = optarg;
			break;
		default :
			(void) printf("Illegal command line option\n");
			exit(1);
		}
	}

	if (did_instance == NULL) {
		(void) printf("No command line option given\n");
		exit(1);
	}

	// initializes ORB
	if (clconf_lib_init() != 0) {
		(void) printf("Could not init orb\n");
		exit(1);
	}

	if (dcs_get_service_names_of_class(rawdisk_class, &services) !=
	    DCS_SUCCESS) {
		(void) printf("Could not get rawdisk device groups\n");
		exit(1);
	}

	for (i = 0; i < services->count; i++) {
		if (dcs_get_property(services->strings[i], "gdev", &prop) !=
		    DCS_SUCCESS) {
			(void) printf("Could not get device group gdev "
			    "property\n");
			exit(1);
		} else if (strcmp(did_instance, "all") == 0) {
			//
			// gdev property is list of did instances seperated by
			// ':', i.e.   d1:d2:d3
			//
			gdev = strtok(prop, ":");
			if (gdev == NULL) {
				(void) printf("Could not parse device group "
				    "gdev property\n");
				exit(1);

			} else {
				(void) printf("%s %s\n", gdev,
				    services->strings[i]);
				while ((gdev = strtok(NULL, ":")) != NULL) {
					(void) printf("%s %s\n", gdev,
					    services->strings[i]);
				}
			}
		//lint -e668 Possibly passing a null pointer to function strcmp
		} else if (strcmp(did_instance, prop) == 0) {
		//lint +e668
			(void) printf("%s\n", services->strings[i]);
			exit(0);
		}
	}

	return (0);
}
