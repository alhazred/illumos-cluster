#! /usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)sc-netapp-ctrl.ksh	1.9	08/05/20 SMI"
#
# This script acts as an intermediate script between run_reserve and NTAPFence
# setting up things for NTAPFence to run and mapping between the run_reserve
# interface and the NTAPFence one.  It performs the following actions:
#
# - calls sc-netapp-cfg to create the config file used by NTAPFence
# - calls NTAPFence
# - removes the config file used by NTAPFence
#
# The following calls into sc-netapp-ctrl are supported:
#
# sc-netapp-ctrl -c fence_node -f <fenced_node> -h <host_node>
# sc-netapp-ctrl -c node_join -h <host_node>

config_prog=/usr/cluster/lib/sc/sc-netapp-cfg
config_file=/var/run/NACF_$$
netapp_cmd=/usr/sbin/NTAPfence
netapp_fence="$netapp_cmd -i $config_file -D "
netapp_join="$netapp_cmd -i $config_file -A "

##############
# script start
#####################################################
#
# This script depends on an ASCII collating
# sequence for checking things like legal node names,
# adapter names, and junction names.  There are no dependencies
# on the collating sequence for inspecting, sorting, or massaging any
# data which might be internationalized.  Therefore,
# the collating sequence locale is forced to the 'C' locale.
#
#####################################################
typeset -x TEXTDOMAIN=SUNW_SC_CMD
typeset -x TEXTDOMAINDIR=/usr/cluster/lib/locale

LC_COLLATE=C;  export LC_COLLATE


/usr/cluster/lib/sc/sc_zonescheck
if [ $? -ne 0 ]; then
	exit 1
fi



##########################
# get command line options
##########################
while getopts c:h:f: name
do

	case $name in
		c)	command="$OPTARG"
			;;
		h)	host_node="$OPTARG"
			;;
		f)	fenced_node="$OPTARG"
			;;
		?)	echo $0:  `gettext "illegal command line option"`
			exit 1
			;;
	esac
done

############################
# check command line options
############################
if [ -z $command ]
then
	echo $0:  `gettext "command not specified"`
	exit 1
fi

if [ $command == fence_node ]
then
	if [ -z $fenced_node ]
	then
		echo $0:  `gettext "node to fence not specified"`
		exit 1
	fi
fi

if [ $command == node_join ]
then
	if [ -z $host_node ]
	then
		echo $0:  `gettext "joining node not specified"`
		exit 1
	fi
fi

# setup config file - also checks to see if we have NetApp storage configured
$config_prog -i $config_file -t netapp
retval=$?
if [ $retval = 2 ]
then
	# no filers or no exports to fence
	exit 0
fi
if [ $retval != 0 ]
then
	echo $0: $config_prog `gettext "failure, returned"` $retval
	rm -f $config_file
	exit $retval
fi

# make sure netapp fencing command is installed
if [ ! -x $netapp_cmd ]
then
	echo `gettext "Network Appliance NAS support files missing"`
	rm -f $config_file
	exit 1
fi

if [ $command = fence_node ]
then
	echo `gettext "fencing node $fenced_node from shared NetApp devices"`
	netapp_cmd="$netapp_fence $fenced_node"
elif [ $command = node_join ]
then
	echo `gettext "obtaining access to shared NetApp devices"`
	netapp_cmd="$netapp_join $host_node"
else
	echo $0:  `gettext "illegal command specification:"` -c $command
	rm -f $config_file
	exit 1
fi

# call netapp fencing
$netapp_cmd
retval=$?
if [ $retval != 0 ]
then
	echo $0: $netapp_cmd `gettext "failure, returned"` $retval
	rm -f $config_file
	exit $retval
fi

# remove config file
rm -f $config_file

exit 0
