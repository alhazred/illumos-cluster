#! /usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)srdf.ksh	1.7	09/01/29 SMI"
#

#
# This script performs the steps required prior to failing over a device group
# which contains SRDF replicated devices.  It:
#
# - determines what state the replication is in
#
# - takes the appropriate actiosn to make this node the replication master
#
# When performing a full transition from RDF2 to RDF1, the following actions
# are needed:
#
#	- /usr/symcli/bin/symrdf -g $service_name failover
#	- /usr/symcli/bin/symrdf -g $service_name swap -refresh R1
#	- /usr/symcli/bin/symrdf -g $service_name establish
#
# Since this is a multi-step process, we have to handle failures between any of
# the steps, performing partial transitionns if some of the work appears to
# already have been done.
#
# Calls to this script are of the form : srdf -s <service_name>, with
# <service_name> being the name of the device group and the srdf
# replication dev_group (these two things must be named the same).
#
# Returns:    0 - This node is now the replication master
#             1 - Unable to make this node the replication master, most likely
#                 due to a bad replication state, requiring user intervention
#             2 - internal error occurred, replication status unknown
#

service_name=
group_status=
retry_interval=5
retry_attempts=36


#####################################################
#
# This script depends on an ASCII collating
# sequence for checking things like legal node names,
# adapter names, and junction names.  There are no dependencies
# on the collating sequence for inspecting, sorting, or massaging any
# data which might be internationalized.  Therefore,
# the collating sequence locale is forced to the 'C' locale.
#
#####################################################
typeset -x TEXTDOMAIN=SUNW_SC_CMD
typeset -x TEXTDOMAINDIR=/usr/cluster/lib/locale

LC_COLLATE=C;  export LC_COLLATE

#####################################################
# check zone credentials

/usr/cluster/lib/sc/sc_zonescheck
if [ $? -ne 0 ]; then
	exit 2
fi

if [ ! -x /usr/symcli/bin/symrdf ]
then
	echo `gettext "Unable to locate SRDF command /usr/symcli/bin/symrdf."`
	exit 2
fi

is_rdf1()
{
	/usr/symcli/bin/symrdf -g $service_name query | grep Type | grep RDF1 > /dev/null 2>&1
	if [ $? = 0 ]
	then
		return 1
	fi

	/usr/symcli/bin/symrdf -g $service_name query | grep Type | grep RDF2 > /dev/null 2>&1
	if [ $? = 0 ]
	then
		return 0
	fi

	echo `gettext "Unable to determine the RDF status for $service_name."`
	exit 2
}


##########################
# get command line options
##########################
while getopts s: name
do

	case $name in
		s)	service_name="$OPTARG"
			;;
		?)	echo $0:  `gettext "illegal command line option."`
			exit 2
			;;
	esac
done

if [ -z $service_name ]
then
	echo $0:  `gettext "service_name is not specified."`
	exit 2
fi

# see if we have an RDF1 or an RDF2 device
is_rdf1
retval=$?

#
#
# RDF1
#
#
if [ $retval = 1 ]
then
	# make sure device group is synchronized
	/usr/symcli/bin/symrdf -g $service_name verify -synchronized
	retval=$?
	if [ $retval = 0 ]
	then
		# rdf1 and synchronized, nothing to do
		exit 0
	fi

	# see if we are failedover
	/usr/symcli/bin/symrdf -g $service_name verify -failedover
	retval=$?
	if [ $retval = 0 ]
	then
		echo `gettext "$service_name status failedover, performing failback."`

		/usr/symcli/bin/symrdf -g $service_name -noprompt -force failback
		retval=$?

		tries=1
		while [ $retval = 2 ]
		do
			echo `gettext "Database locked, retrying symrdf -g $service_name failback in $retry_interval seconds"`
			sleep $retry_interval

			/usr/symcli/bin/symrdf -g $service_name -noprompt -force failback
			retval=$?
			
			tries=$(($tries + 1))
			if  [ $tries = $retry_attempts ]
			then
				echo `gettext "SRDF is still locked failing symrdf -g $service_name failback"`
				break
			fi
		done

		if [ $retval != 0 ]
		then
			echo `gettext "symrdf -g $service_name failback returned $retval, check device group status."`
			exit 1
		fi

		# make sure we are now synchronized
		/usr/symcli/bin/symrdf -g $service_name verify -synchronized
		retval=$?
		if [ $retval != 0 ]
		then
			echo `gettext "$service_name devices are not synchronized after failback, check device group status."`
			exit 1
		fi
		exit 0
	fi

	# neither synchronized nor failedover
	echo `gettext "$service_name state is invalid, check device group status."`
	exit 1
fi


#
#
# RDF2
#
#
# if a sync is in progress, wait for it to complete
#
tries=1
/usr/symcli/bin/symrdf -g $service_name verify -syncinprog
retval=$?
while [ $retval = 0 ]
do
	echo `gettext "Replication synchronization in progress, sleeping for $retry_interval seconds"`
	sleep $retry_interval

	/usr/symcli/bin/symrdf -g $service_name verify -syncinprog
	retval=$?

	tries=$(($tries + 1))
		if  [ $tries = $retry_attempts ]
		then
			break
		fi
done

# see if device group is synchronized
/usr/symcli/bin/symrdf -g $service_name verify -synchronized
retval=$?

if [ $retval = 0 ]
then
	# rdf2 and synchronized, do full failover
	# enable RDF2
	/usr/symcli/bin/symrdf -g $service_name -noprompt -force failover
	retval=$?

	tries=1
	while [ $retval = 2 ]
	do
		echo `gettext "Database locked, retrying symrdf -g $service_name failover in $retry_interval seconds"`
		sleep $retry_interval

		/usr/symcli/bin/symrdf -g $service_name -noprompt -force failover
		retval=$?
			
		tries=$(($tries + 1))
		if  [ $tries = $retry_attempts ]
		then
			echo `gettext "SRDF is still locked failing symrdf -g $service_name failover"`
			break
		fi
	done

	if [ $retval != 0 ]
	then
		echo `gettext "symrdf -g $service_name failover returned $retval, check device group status."`
		exit 1
	fi

	# swap RDF1 and RDF2
	/usr/symcli/bin/symrdf -g $service_name -noprompt -force swap -refresh R1
	retval=$?

	tries=1
	while [ $retval = 2 ]
	do
		echo `gettext "Database locked, retrying symrdf -g $service_name swap in $retry_interval seconds"`
		sleep $retry_interval

		/usr/symcli/bin/symrdf -g $service_name -noprompt -force swap -refresh R1
		retval=$?
			
		tries=$(($tries + 1))
		if  [ $tries = $retry_attempts ]
		then
			echo `gettext "SRDF is still locked failing symrdf -g $service_name swap"`
			break
		fi
	done

	if [ $retval != 0 ]
	then
		echo `gettext "symrdf -g $service_name swap returned $retval, check device group status."`
		exit 1
	fi

	# sync and enable new RDF1
	/usr/symcli/bin/symrdf -g $service_name -noprompt establish
	retval=$?

	tries=1
	while [ $retval = 2 ]
	do
		echo `gettext "Database locked, retrying symrdf -g $service_name establish in $retry_interval seconds"`
		sleep $retry_interval

		/usr/symcli/bin/symrdf -g $service_name -noprompt establish
		retval=$?
			
		tries=$(($tries + 1))
		if  [ $tries = $retry_attempts ]
		then
			echo `gettext "SRDF is still locked failing symrdf -g $service_name establish"`
			break
		fi
	done

	if [ $retval != 0 ]
	then
		echo `gettext "symrdf -g $service_name establish returned $retval, check device group status."`
		exit 1
	fi

	# make sure we are now synchronized
	/usr/symcli/bin/symrdf -g $service_name verify -synchronized
	retval=$?
	if [ $retval != 0 ]
	then
		echo `gettext "$service_name devices are not synchronized, check device group status."`
		exit 1
	fi

	exit 0
fi

# ok, see if the group has been failedover
/usr/symcli/bin/symrdf -g $service_name verify -failedover
retval=$?
if [ $retval = 0 ]
then
	# rdf2 and failedover, swap and establish
	# swap RDF1 and RDF2
	/usr/symcli/bin/symrdf -g $service_name -noprompt -force swap -refresh R1
	retval=$?

	tries=1
	while [ $retval = 2 ]
	do
		echo `gettext "Database locked, retrying symrdf -g $service_name swap in $retry_interval seconds"`
		sleep $retry_interval

		/usr/symcli/bin/symrdf -g $service_name -noprompt -force swap -refresh R1
		retval=$?
			
		tries=$(($tries + 1))
		if  [ $tries = $retry_attempts ]
		then
			break
		fi
	done

	if [ $retval != 0 ]
	then
		echo `gettext "symrdf -g $service_name swap returned $retval, check device group status."`
		exit 1
	fi

	# sync and enable new RDF1
	/usr/symcli/bin/symrdf -g $service_name -noprompt establish
	retval=$?

	tries=1
	while [ $retval = 2 ]
	do
		echo `gettext "Database locked, retrying symrdf -g $service_name establish in $retry_interval seconds"`
		sleep $retry_interval

		/usr/symcli/bin/symrdf -g $service_name -noprompt establish
		retval=$?
			
		tries=$(($tries + 1))
		if  [ $tries = $retry_attempts ]
		then
			break
		fi
	done

	if [ $retval != 0 ]
	then
		echo `gettext "symrdf -g $service_name establish returned $retval, check device group status."`
		exit 1
	fi

	# make sure we are now synchronized
	/usr/symcli/bin/symrdf -g $service_name verify -synchronized
	retval=$?
	if [ $retval != 0 ]
	then
		echo `gettext "$service_name devices are not synchronized, check device group status."`
		exit 1
	fi

	exit 0
fi

# see if the pair is partitioned
/usr/symcli/bin/symrdf -g $service_name verify -partitioned
retval=$?
if [ $retval = 0 ]
then
	echo PAIR IS PARTITIONED

	# perform failover
	/usr/symcli/bin/symrdf -g $service_name -noprompt -force failover
	retval=$?

	tries=1
	while [ $retval = 2 ]
	do
		echo `gettext "Database locked, retrying symrdf -g $service_name failover in $retry_interval seconds"`
		sleep $retry_interval

		/usr/symcli/bin/symrdf -g $service_name -noprompt -force failover
		retval=$?
			
		tries=$(($tries + 1))
		if  [ $tries = $retry_attempts ]
		then
			echo `gettext "SRDF is still locked failing symrdf -g $service_name failover"`
			break
		fi
	done

	if [ $retval != 0 ]
	then
		echo `gettext "symrdf -g $service_name failover returned $retval, check device group status."`
		exit 1
	fi

	exit 0
fi


# not synchronized, failedover or partitioned
echo `gettext "$service_name state is invalid, check device group status."`
exit 1
