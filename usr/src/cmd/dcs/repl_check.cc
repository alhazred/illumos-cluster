//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)repl_check.cc	1.6	08/05/20 SMI"

//
// This code checks to see if the specified device group has the
// 'replicated_device' property set.  If so, the program writes the replication
// type to stdout and exits with 2, if not, the program checks to see if there
// is a replication group configured with the same name as this device group.
// This handles the case in which the user forgets to set the replication
// property for an SVM device group.  Since replication group names have to
// match their device group name, if there is a replication group with the
// same name as our service, then it must belong to this service.  If there
// is no match, then this program writes to stdout "NOT_REPLICATED" and exits
// with 0.  Exit of 1 indicates an error.
//
// Legal call:
//
// repl_check -s service_name
//

#include <stdio.h>
#include <stdlib.h>
#include <nslib/ns.h>
#include <nslib/data_container_impl.h>
#include <h/naming.h>
#include <h/ccr.h>
#include <dc/libdcs/libdcs.h>
#include <libdid.h>
#include <libintl.h>
#include <locale.h>
#include <rgm/sczones.h>

#define	REPL_TAG "replicated_device"

os::sc_syslog_msg msg_handle("", "", "");

main(int argc, char **argv)
{
	int c, i;
	char *service_name = NULL;
	dc_error_t dc_err;
	dc_property_seq_t *property_seq;
	did_repl_list_t	*repllist = NULL;
	did_repl_list_t	*trans;
	int libinit;

	/* I18N housekeeping */
	(void) setlocale(LC_ALL, "");
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);
	(void) textdomain(TEXT_DOMAIN);

	if (sc_zonescheck() != 0)
		return (1);

	// get args
	while ((c = getopt(argc, argv, "s:")) != EOF) {
		switch (c) {
		case 's' :
			service_name = optarg;
			break;
		default :
			//
			// SCMSGS
			// @explanation
			// An error was encountered while checking for
			// replicated device groups.
			// @user_action
			// Replicated device groups may not have properly
			// started on this node. If so, you may manually
			// configure this node as the replication master and
			// attempt to restart the device group on this node.
			// Contact your authorized Sun service provider to
			// determine whether a workaround or patch is
			// available.
			//
			(void) msg_handle.log(SC_SYSLOG_WARNING,
			    MESSAGE, "repl_check fatal error - "
			    "Illegal command line option");
			exit(1);
		}
	}

	// check for required args
	if (service_name == NULL) {
		//
		// SCMSGS
		// @explanation
		// An error was encountered while checking for replicated
		// device groups.
		// @user_action
		// Replicated device groups may not have properly started on
		// this node. If so, you may manually configure this node as
		// the replication master and attempt to restart the device
		// group on this node. Contact your authorized Sun service
		// provider to determine whether a workaround or patch is
		// available.
		//
		(void) msg_handle.log(SC_SYSLOG_WARNING,
		    MESSAGE, "repl_check fatal error - "
		    "Device group not specified");
		exit(1);
	}

	// initializes ORB
	if (clconf_lib_init() != 0) {
		//
		// SCMSGS
		// @explanation
		// An error was encountered while checking for replicated
		// device groups.
		// @user_action
		// Replicated device groups may not have properly started on
		// this node. If so, you may manually configure this node as
		// the replication master and attempt to restart the device
		// group on this node. Contact your authorized Sun service
		// provider to determine whether a workaround or patch is
		// available.
		//
		(void) msg_handle.log(SC_SYSLOG_WARNING, MESSAGE,
		    "repl_check fatal error - clconf_lib_init() error");
		exit(1);
	}

	dc_err = dcs_get_service_parameters(service_name,
	    NULL, NULL, NULL, NULL, NULL, &property_seq);
	if (dc_err == DCS_SUCCESS) {
		for (i = 0; i < (int)property_seq->count; i++) {
			if (strcmp((property_seq->properties)[i].name,
			    REPL_TAG) == 0) {
				(void) printf("%s\n",
				    property_seq->properties[i].value);
				    dcs_free_properties(property_seq);
				exit(2);
			}
		}
	} else {
		//
		// SCMSGS
		// @explanation
		// An error was encountered while checking for replicated
		// device groups.
		// @user_action
		// Replicated device groups may not have properly started on
		// this node. If so, you may manually configure this node as
		// the replication master and attempt to restart the device
		// group on this node. Contact your authorized Sun service
		// provider to determine whether a workaround or patch is
		// available.
		//
		(void) msg_handle.log(SC_SYSLOG_WARNING,
		    MESSAGE, "repl_check fatal error - "
		    "dcs_get_service_parameters() error, dc_err %d", dc_err);
		exit(1);
	}

	dcs_free_properties(property_seq);

	//
	// we didn't find the "replicated_device" property, but it's possible
	// (in the case of SVM) that the user forgot to set it, so double
	// check by looking for a replication dev_group with a matching name
	//
	/* Init libdid. */
	libinit = did_initlibrary(1, DID_NONE, NULL);
	if (libinit != DID_INIT_SUCCESS) {
		//
		// SCMSGS
		// @explanation
		// An error was encountered while checking for replicated
		// device groups.
		// @user_action
		// Replicated device groups may not have properly started on
		// this node. If so, you may manually configure this node as
		// the replication master and attempt to restart the device
		// group on this node. Contact your authorized Sun service
		// provider to determine whether a workaround or patch is
		// available.
		//
		(void) msg_handle.log(SC_SYSLOG_WARNING, MESSAGE,
		    "repl_check fatal error - did_initlibrary() failure");
		exit(1);
	}

	if (did_getrepllist(&repllist) < 0) {
		//
		// SCMSGS
		// @explanation
		// An error was encountered while checking for replicated
		// device groups.
		// @user_action
		// Replicated device groups may not have properly started on
		// this node. If so, you may manually configure this node as
		// the replication master and attempt to restart the device
		// group on this node. Contact your authorized Sun service
		// provider to determine whether a workaround or patch is
		// available.
		//
		(void) msg_handle.log(SC_SYSLOG_WARNING, MESSAGE,
		    "repl_check fatal error - could not load DID repl list");
		exit(1);
	}

	for (trans = repllist; trans; trans = trans->next)
		if (strcmp(service_name, trans->dev_group) == 0) {
			(void) printf("%s\n", trans->repl_type);
			exit(2);
		}

	(void) printf(gettext("NOT_REPLICATED\n"));
	exit(0);
}
