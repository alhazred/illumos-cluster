#! /usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)get_truecopy_dev_groups.ksh	1.5	08/05/20 SMI"
#
# This code determines what Truecopy replication dev_groups exist on the local
# node and determines what solaris devices make up the dev_groups.  It does
# this by first parsing the output from 'raidscan' to determine what dev_groups
# are configured and then by calling 'pairdisplay' for each dev_group to get
# a list of the disks in the dev_group.  'raidscan' and 'pairdisplay' output
# are expected to be stable enough to reply upon within the following bounds:
#
# # ls /dev/rdsk/*s2 | raidscan -find verify - it is expected that dev_group
# names are provided in the second column of output.
#
# # ls /dev/rdsk/*s2 | raidscan -find verify
# DEVICE_FILE                                        Group
# /dev/rdsk/c5t50060E800000000000004E6000000005d0s2  -
# /dev/rdsk/c5t50060E800000000000004E6000000013d0s2  -
# /dev/rdsk/c5t50060E800000000000004E600000003Ad0s2  VG01
# /dev/rdsk/c5t50060E800000000000004E600000003Bd0s2  VG02
# /dev/rdsk/c5t50060E800000000000004E600000003Cd0s2  VG03
# /dev/rdsk/c5t50060E800000000000004E600000003Dd0s2  -
#
# /usr/bin/pairdisplay -g dev_group -fd -CLI - it is expected that device
# information will be given for one device per line, and that the first word
# on each line is the dev_group name, the second word is the replication pair,
# the third word is either 'L' or 'R' for local / remote and the fourth word
# on each line is the Solaris device name.  Header information is skipped
# because the first word does not match the dev_group name we are looking for.
#
#
# #  pairdisplay -g VG01 -fd -CLI
# Group   PairVol L/R  Device_File
# VG01    pair1 L      c6t500060E8000000000000EEBA0000001Dd0s2
# VG01    pair1 R      c5t50060E800000000000004E600000003Ad0s2
#
# legal calls are as follows:
#
# get_truecopy_dev_groups -f outfile -r remote_node
#	called from 'scdidadm -T' which automatically remaps all pairs of
#	replicated device to have the same DID instance
#
# get_truecopy_dev_groups -f outfile -r NULL
#	called from 'scdidadm -t' to manually remap a pair of replicated
#	devices to have the same DID instance
#
# get_truecopy_dev_groups -s service
#	initially used from run_reserve during make_primary, but no longer
#	used.
#
# Return values:
#       0       -       no error
#       1       -       error
#       2       -       replicated
#       3       -       no replication software and/or daemon
#


outfile=
local_host=`hostname`
remote_host=
service=
pairdisplay_cmd=/usr/bin/pairdisplay
raidscan_cmd=/usr/bin/raidscan

# cleanup temp files before exiting
do_exit()
{
rm -f "$outfile"_*
}


typeset -x TEXTDOMAIN=SUNW_SC_CMD
typeset -x TEXTDOMAINDIR=/usr/cluster/lib/locale

# Set the program name
typeset PROG=${CLCMD_COMMAND_EXEC:-${0##*/}}
typeset USE_MSGID=${CLCMD_COMMAND_EXEC:-}

LC_COLLATE=C;  export LC_COLLATE

trap 'do_exit' 0

#########################
# check if in global zone
/usr/cluster/lib/sc/sc_zonescheck
if [ $? -ne 0 ]; then
	exit 1
fi

################################################
#
# printerr()
#
# This function prints error messages to stderr
# from stdin, but first it generates a msgid for
# the text and inserts that into the error msg.
################################################
printerr()
{
	# Save the message
	typeset msg=$(cat)

	if [[ -z ${msg} ]]; then
		return
	fi

	# Don't generate the msgid if we don't have to
	if [[ -z ${USE_MSGID} ]]; then
		echo ${msg} | sed "s/^/${PROG}: /" >&2
		return
	fi

        # Create the message ID
	msgid=$(echo ${msg} | msgid | awk '{print $1}')
	msgid=$(printf "C%6.6d" ${msgid})

	# Print the message
	echo ${msg} | sed "s/^/${PROG}:  \(${msgid}\) /" >&2
}

##########################
# get command line options
##########################
while getopts f:r:s: name
do

	case $name in
		f)	outfile="$OPTARG"
			;;
		r)	remote_host="$OPTARG"
			;;
		s)	service="$OPTARG"
			;;
		?)	echo $0:  `gettext "illegal command line option"`
			exit 1
			;;
	esac
done

if [ -z $outfile ]
then
	printf "$(gettext 'Output file not specified.')\n" | printerr
	exit 1
fi

# create an empty output file
touch $outfile

if [ ! -z $service ]
then
	# make sure horcm is running if this is a call from run_reserve
	/usr/bin/horcmstart.sh > /dev/null 2>&1
fi

# make sure truecopy is installed (the commands we need at least)
if [[ ! -x $pairdisplay_cmd || ! -x $raidscan_cmd ]]
then
	if [ ! -z $remote_host ]
	then
		printf "$(gettext 'No TrueCopy dev_groups configured on this node.')\n" | printerr
	fi
	exit 3
fi

# get the list of dev_groups configured for this node
ls /dev/rdsk/*s2 | $raidscan_cmd -find verify | awk ' NR > 1 { if ($2 != "-") print $2 }' > "$outfile"_rs

if [ ! -s "$outfile"_rs ]
then
	# must not be any dev_groups on this node
	printf "$(gettext 'No TrueCopy dev_groups configured on this node or the horcm daemon is not running.')\n" | printerr
	exit 3
fi

# if $service is defined, see if we have a dev_group that matches $service
if [ ! -z $service ]
then
	while read dgroup
	do
		if [ $dgroup = $service ]
		then
			# found a match, it is replicated
			echo truecopy
			exit 2
		fi
	done < "$outfile"_rs
	echo NOT_REPLICATED
	exit 0
fi

# get dev_groups, removing redundant entries
dev_groups=`sort -u "$outfile"_rs`
if [ -z $dev_groups ]
then
	printf "$(gettext 'Failed to sort dev_groups.')\n" | printerr
	exit 1
fi

# get the replica devices in each dev_group
for group in $dev_groups
do
	$pairdisplay_cmd -g $group -fd -CLI | awk '{ if ($1==devg) print $1, $2, $3, $4 }' devg=$group >>  "$outfile"_pd
done

if [ ! -s "$outfile"_pd ]
then
	# most likely failure is horcm not running
	printf "$(gettext 'Unable to obtain replication information, use /usr/bin/horcmstart.sh to ensure horcm daemon is running on all nodes.')\n" | printerr
	exit 1
fi

# insert host names into device path, for use by scdidadm
while read group pair lorr device
do
	#
	# Configuration errors can cause pairdisplay to return a device of
	# "Unknown"
	#
	if [ $device = "Unknown" ]
	then
		printf "$(gettext 'TrueCopy configuration error encountered - unknown device for group %s.')\n" ${group}
		$pairdisplay_cmd -g $group -fd -CLI
		exit 1
	fi

	if [ $remote_host = NULL ]
	then
		device="/dev/rdsk/"$device
	elif [ $lorr = L ]
	then
		device=$local_host":/dev/rdsk/"$device
	else
		device=$remote_host":/dev/rdsk/"$device
	fi

	echo $group $pair $lorr $device >> "$outfile"_did
done < "$outfile"_pd

# make sure output is sorted by dev_group - pair - L/R
sort "$outfile"_did > $outfile

exit 0
