/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)print-list.cc 1.4	08/05/20 SMI"
 *
 * print-list.cc
 *
 * The print-list command provides a command line interface to
 * the ClList objects printing functionality.  This command is
 * used to provide a consistent look across the command set,
 * particularly those which are non-binary commands.
 *
 * The command accepts the following format:
 * 	$ print-list [<verbose>]
 *
 * Where <verbose> may be either "true" or "false".  If <verbose>
 * is not given, then "false" is assumed.
 *
 * The command will then read consecutive lines of data from
 * stdin until either EOF or a blank line is reached.  Each line
 * may contain up to MAX_COLS (defined in Rows.h) data columns,
 * separated by a tab ('\t') delimeter.  Any data after the
 * MAX_COLS of data will be ingnored.  If <verbose> is "true",
 * then the first line of data read will be treated as the headings
 * and every following line will be treated as data lines,
 * otherwise, all lines will be considered data.
 */

#include "ClList.h"

/*
 * Read values from a line of data, populating value[].
 * The data can be any legal string separated by tabs as
 * delimeters.  After MAX_COLS (defined in Rows.h) has been
 * reached, or a newline, the function returns 0.  If the
 * function encounters EOF or a blank line, it will return 1,
 * indicating to the caller that it is the end of the data.
 */
int
readValues(char *value[])
{
	int err = 0;
	int count = 0;
	int i = 0;
	char temp[100];

	// clear out values
	for (int j = 0; j < MAX_COLS; j++)
		value[j] = NULL;

	char c;
	while ((c = getchar()) != EOF) {

		// Only read in MAX_COLS from each line
		if (count >= MAX_COLS) {
			while ((c = getchar()) != EOF) {
				if (c == '\n')
					break;
			}
			break;
		}

		// If a new line is reached, dump data and return
		if (c == '\n') {
			// handle empty line
			if (count == 0 && i == 0) {
				return (1);
			}
			temp[i] = '\0';
			value[count] = new char [strlen(temp) + 1];
			sprintf(value[count++], "%s", temp);
			break;
		}

		// Tab is the delimeter, record string and move on
		if (c == '\t') {
			temp[i] = '\0';
			value[count] = new char [strlen(temp) + 1];
			sprintf(value[count++], "%s", temp);
			i = 0;
		} else {
			temp[i++] = c;
		}
	}

	// Return 1 to tell caller it's the end of the data
	if (c == EOF)
		return (1);

	return (err);
}

/*
 * Reads data from stdin, populating a ClList object, then
 * prints the data when all of it has been read.  If an error
 * has occured, it will return 1, otherwise it returns 0.
 */
int
main(int argc, char **argv)
{
	if (argc > 2)
		exit(1);

	bool verbose = false;
	ClList *obj = NULL;
	if (argc == 1)
		obj = new ClList();
	else {
		char *arg = argv[1];
		if (strcmp(arg, "true") == 0)
			verbose = true;
		else if (strcmp(arg, "false") == 0)
			verbose = false;
		else
			exit(1);

		obj = new ClList(verbose);
	}

	char *values[MAX_COLS];
	int err = 0;
	if (verbose) {
		err = readValues(values);
		if (err != 0)
			return (1);

		obj->setHeadings(
		    values[0],
		    values[1],
		    values[2],
		    values[3],
		    values[4]);
	}

	while ((err = readValues(values)) == 0) {
		obj->addRow(
		    values[0],
		    values[1],
		    values[2],
		    values[3],
		    values[4]);
	}

	obj->print();
	return (0);
}
