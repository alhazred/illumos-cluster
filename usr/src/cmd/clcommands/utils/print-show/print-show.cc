/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)print-show.cc 1.3	08/05/20 SMI"
 *
 * print-show.cc
 *
 * The print-show command provides a command line interface to
 * the ClShow objects printing functionality.  This command is
 * used to provide a consitent look across the command set,
 * particularly those which are non-binary commands.
 *
 * The command line interface to the ClShow command does not
 * support child ClShow objects, however, it does support a
 * manual setting of the indentation level, so that when being
 * called from a parent command (as the cluster command does with
 * all the other commands) the proper indentation can be maintained.
 *
 * The command accepts the following format:
 *	showcmd <level> [<heading> [<h_arg>]]
 *
 * <level> is an integer between 1 and 10, which is the level
 * of indenting to perform on the object when printed.
 *
 * <heading> is an integer between 1 and NUM_HEADINGS.	This is
 * optional, and corresponds to the headings predefined in the
 * show_heading_t enum.
 *
 * <h_arg> is a string which is to be inserted into the heading.
 * Not all headings require such a string, so it is optional, too.
 *
 * The command will then read data on stdin until either EOF
 * or an empty line is encountered.  The first line of data
 * will represent the object, and every additional input line
 * will be that objects properties.  Every line should be format-
 * ted in the following way:
 *
 * <name>\t<value>\n
 */

#include "ClShow.h"

/*
 * Read values from a line of data, creating a new NameValue
 * object, or NULL, if EOF or a blank line has been reached.
 * The lines of data should contain one or more strings
 * representing the "name", followed by a tab, and one or more
 * strings representing the "value", terminated by a newline.
 */
NameValue *
readProps(char *name, char *value)
{

	NameValue *nv = NULL;
	bool isName = true;
	int i = 0;

	char c;
	while ((c = getchar()) != EOF) {

		if (c == '\n') {
			if (isName && i == 0)
				return (NULL);

			value[i] = '\0';
			nv = new NameValue(name, value);
			break;
		}

		if (isName) {
			// End the name and reset counter
			if (c == '\t') {
				name[i] = '\0';
				isName = false;
				i = 0;
			} else
				name[i++] = c;
		} else
			value[i++] = c;

	}

	return (nv);
}

/*
 * Reads data from stdin, populating a ClShow object, then
 * prints the data when all of it has been read.  If an error
 * has occured, it will return 1, otherwise it returns 0.
 */
int
main(int argc, char *argv[])
{

	if (argc < 2 || argc > 4)
		return (1);

	int level = atoi(argv[1]);
	if (level > 10 || level < 1)
		return (1);

	ClShow *obj = NULL;
	if (argc == 2)
		obj = new ClShow();
	else {
		int heading = atoi(argv[2]);
		if (heading == 0 || heading > NUM_HEADINGS)
			return (1);

		if (argv[3] == NULL)
			obj = new ClShow((show_heading_t)(heading - 1));
		else
			obj = new ClShow((show_heading_t)(heading - 1),
			    argv[3]);
	}

	char name[100];
	char value[1024];
	NameValue *nv = readProps(name, value);

	if (nv == NULL)
		return (1);

	while (nv != NULL) {
		obj->addObject(nv);
		char *object = strdup(nv->getvalue());
		while ((nv = readProps(name, value)) != NULL) {
			obj->addProperty(object, nv);
		}
		nv = readProps(name, value);
	}

	obj->print(obj, level - 1);
	printf("\n");

	delete [] name;
	delete [] value;

	return (0);

}
