#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#ident	"@(#)Makefile.com	1.2	08/05/20 SMI"
#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# cmd/clcommands/lib/libclutils/Makefile.com
#
#
LIBRARYCCC= libclutils.a
VERS=.1

OBJECTS= \
	GetText.o \
	NameValue.o \
	NameValueList.o \
	Properties.o \
	PropertyResourceBundle.o \
	TextMessage.o \
	ValueList.o \
	clmessages.o

include $(SRC)/lib/Makefile.lib

MAPFILE=	../common/mapfile-vers
SRCS=		$(OBJECTS:%.o=../common/%.cc)

LIBS=		$(DYNLIBCCC)

MTFLAG=		-mt

CPPFLAGS +=	-I$(SRC)/cmd/clcommands/getopt_clip
CPPFLAGS +=	-I$(SRC)/cmd/clcommands/include

PMAP =  -M $(MAPFILE)

LINTFLAGS +=
LINTFILES +=	$(OBJECTS:%.o=%.ln)

$(S9_BUILD)LDLIBS += $(SPRO_LIBS)/libCstd.a $(SPRO_LIBS)/libCrun.a
LDLIBS +=	-lc

.KEEP_STATE:

objs/%.o pics/%.o: ../common/%.cc
	$(COMPILE.cc) -o $@ $<

include $(SRC)/lib/Makefile.targ
