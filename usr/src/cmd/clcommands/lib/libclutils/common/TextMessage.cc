//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)TextMessage.cc	1.3	08/05/20 SMI"

//
// This is the TextMessage class.
//

#include "TextMessage.h"

#undef	gettext
#define	gettext		getText.getText

static void print_lines(FILE *fp, int margin, const char *lines);

// Basic constructor
TextMessage::TextMessage()
{
	init();
}

// Constructor with command name and resource bundle
TextMessage::TextMessage(char *commandname_in,
    PropertyResourceBundle &bundle_in)
{
	init();
	setCommandName(commandname_in);
	bindPropertyResourceBundle(bundle_in);
}

// Constructor with command name, resource bundle, and flags
TextMessage::TextMessage(char *commandname_in,
    PropertyResourceBundle &bundle_in, unsigned int flags_in)
{
	init();
	setCommandName(commandname_in);
	bindPropertyResourceBundle(bundle_in);
	setFlags(flags_in);
}

//
// Constructor with command name, resource bundle, flags, and
// alternate file pointer.
//
TextMessage::TextMessage(char *commandname_in,
    PropertyResourceBundle &bundle_in, unsigned int flags_in, FILE *fp_in)
{
	init();
	setCommandName(commandname_in);
	bindPropertyResourceBundle(bundle_in);
	setFlags(flags_in);
	setFilePointer(fp_in);
}

// Destructor
TextMessage::~TextMessage()
{
}

// Set the command name
void
TextMessage::setCommandName(char *commandname_in)
{
	commandname = commandname_in;
}

// Get the command name associated with this instance
char *
TextMessage::getCommandName(void)
{
	return (commandname);
}

// Bind to a resource bundle
void
TextMessage::bindPropertyResourceBundle(PropertyResourceBundle &bundle_in)
{
	pbundle = &bundle_in;
	getText.bindPropertyResourceBundle(bundle_in);
}

// Get the resource bundle
PropertyResourceBundle
TextMessage::getPropertyResourceBundle(void)
{
	return (*pbundle);
}

// Set the flags
void
TextMessage::setFlags(unsigned int flags_in)
{
	flags = flags_in;
}

// Get the flags
unsigned int
TextMessage::getFlags(void)
{
	return (flags);
}

// Set the error flags
void
TextMessage::setErrorFlags(unsigned int flags_in)
{
	errorflags = flags_in;
}

// Get the error flags
unsigned int
TextMessage::getErrorFlags(void)
{
	return (errorflags);
}

// Set the warning flags
void
TextMessage::setWarningFlags(unsigned int flags_in)
{
	warningflags = flags_in;
}

// Get the warning flags
unsigned int
TextMessage::getWarningFlags(void)
{
	return (warningflags);
}

// Set the message flags
void
TextMessage::setMessageFlags(unsigned int flags_in)
{
	messageflags = flags_in;
}

// Get the message flags
unsigned int
TextMessage::getMessageFlags(void)
{
	return (messageflags);
}

// Set the file pointer
void
TextMessage::setFilePointer(FILE *fp_in)
{
	if (fp_in)
		fp = fp_in;
}

// Get the file pointer
FILE *
TextMessage::getFilePointer(void)
{
	return (fp);
}

// Shared function to print message
// Print error message
void
TextMessage::printError(char *message_in, ...)
{
	va_list ap;

	va_start(ap, message_in);
	printstring(0, errorflags, stderr, message_in, ap);
	va_end(ap);
}

void
TextMessage::va_printError(char *message_in, va_list ap)
{
	printstring(0, errorflags, stderr, message_in, ap);
}

// Print warning message
void
TextMessage::printWarning(char *message_in, ...)
{
	va_list ap;

	va_start(ap, message_in);
	printstring(0, warningflags, stderr, message_in, ap);
	va_end(ap);
}

void
TextMessage::va_printWarning(char *message_in, va_list ap)
{
	printstring(0, warningflags, stderr, message_in, ap);
}

// Print info message
void
TextMessage::printMessage(char *message_in, ...)
{
	va_list ap;

	va_start(ap, message_in);
	printstring(0, messageflags, stdout, message_in, ap);
	va_end(ap);
}

void
TextMessage::va_printMessage(char *message_in, va_list ap)
{
	printstring(0, messageflags, stdout, message_in, ap);
}

// Print a message
void
TextMessage::printString(char *message_in, ...)
{
	va_list ap;

	va_start(ap, message_in);
	printstring(0, flags, fp, message_in, ap);
	va_end(ap);
}

// Print a message to designated file pointer
void
TextMessage::printString(FILE *fp_in, char *message_in, ...)
{
	va_list ap;

	va_start(ap, message_in);
	printstring(0, flags, fp_in, message_in, ap);
	va_end(ap);
}

// Print a message to designated file pointer with given flags
void
TextMessage::printString(unsigned int flags_in, FILE *fp_in,
    char *message_in, ...)
{
	va_list ap;

	va_start(ap, message_in);
	printstring(0, flags_in, fp_in, message_in, ap);
	va_end(ap);
}

//
// Print a message, from it's ID.
// Return zero only if successful.
//
int
TextMessage::printStringId(char *msgid_in, ...)
{
	va_list ap;
	int status;

	va_start(ap, msgid_in);
	status = printstringid(flags, fp, msgid_in, ap);
	va_end(ap);

	return (status);
}

//
// Print a message, from it's ID, to designated file pointer
// Return zero only if successful.
//
int
TextMessage::printStringId(FILE *fp_in, char *msgid_in, ...)
{
	va_list ap;
	int status;

	va_start(ap, msgid_in);
	status = printstringid(flags, fp_in, msgid_in, ap);
	va_end(ap);

	return (status);
}

//
// Print a message, from it's ID, to designated file pointer with given flags
// Return zero only if successful.
//
int
TextMessage::printStringId(unsigned int flags_in, FILE *fp_in,
    char *msgid_in, ...)
{
	va_list ap;
	int status;

	va_start(ap, msgid_in);
	status = printstringid(flags_in, fp_in, msgid_in, ap);
	va_end(ap);

	return (status);
}

void
TextMessage::printstring(char *msgid_in, unsigned int flags_in, FILE *fp_in,
    char *message_in, va_list ap)
{
	char *new_msgid = (char *)0;
	const char *message;

	// Make sure we have a message to print
	if (!message_in)
		return;

	// Command name prefix
	if (commandname && (flags_in & TM_FLAG_PRINTCMD))
		(void) fprintf(fp_in, "%s:  ", commandname);

	// Prefix for warning messages
	if ((flags_in & TM_FLAG_PRINTWARNSTR))
		(void) fprintf(fp_in, "%s: ", gettext(STR_WARNING));

	// Get the message ID, if we need it
	if (flags_in & (TM_FLAG_PRINTID | TM_FLAG_VERBOSE) && !msgid_in) {
		msgid_in = new_msgid = getText.getMessageId(message_in);
	}

	// Include message ID
	if ((flags_in & (TM_FLAG_PRINTID | TM_FLAG_VERBOSE)) && msgid_in) {
		(void) fprintf(fp_in, "(%s) ", msgid_in);
	}

	// Get the localized message
	message = getText.getText(message_in);

	// Print the message
	(void) vfprintf(fp_in, message, ap);

	// Print verbose
	if ((flags_in & TM_FLAG_VERBOSE) && msgid_in) {
		const char *details = getText.getDetailsFromId(msgid_in);
		const char *action = getText.getActionFromId(msgid_in);

		// Only print verbose, if details are provided
		if (details && details[0]) {
			(void) fprintf(fp_in, "    %s: %s\n",
			    gettext("Message ID"), msgid_in);

			(void) fprintf(fp_in, "    %s:\n",
			    gettext("Message Details"));
			print_lines(fp_in, 8, details);
			(void) putc('\n', fp_in);

			(void) fprintf(fp_in, "    %s:\n",
			    gettext("Suggested Course of Action"));
			if (action && action[0]) {
				print_lines(fp_in, 8, action);
			} else {
				print_lines(fp_in, 8,
				    "Contact your service provider.\n");
			}
			(void) putc('\n', fp_in);
		}
	}
	delete new_msgid;
}

//
// Shared function to print message from it's ID.
// Return zero only if successful.
//
int
TextMessage::printstringid(unsigned int flags_in, FILE *fp_in,
    char *msgid_in, va_list ap)
{
	char *message;

	// Make sure we have a message ID
	if (!msgid_in)
		return (-1);

	// Get the message from the ID
	message = (char *)getText.getString(msgid_in);

	// Make sure we have a message to print
	if (!message || !message[0])
		return (-1);

	// Print the message
	printstring(msgid_in, flags, fp, message, ap);

	return (0);
}

//
// Initialize to default values
//
void
TextMessage::init()
{
	commandname = 0;
	pbundle = 0;
	fp = stderr;
	flags = TM_DEFAULT_FLAGS;
}

//
// Print lines with a left margin
//
static void
print_lines(FILE *fp, int margin, const char *lines)
{
	char *buffer;
	char *line;
	int count;

	if (!fp || !lines)
		return;

	buffer = new char[strlen(lines) + 1];
	(void) strcpy(buffer, lines);
	line = strtok(buffer, "\n");
	while (line) {
		(void) fprintf(fp, "%*s%s\n", margin, "", line);
		line = strtok(0, "\n");
	}
	delete buffer;
}
