//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident   "@(#)GetText.cc	1.4	08/05/20 SMI"

//
// This is the GetText class.
//

#include "GetText.h"

#include <sys/strlog.h>


// Basic constructor
GetText::GetText()
{
	pbundle = 0;
}

// Constructor with resource bundle
GetText::GetText(PropertyResourceBundle &bundle_in)
{
	pbundle = &bundle_in;
}

// Destructor
GetText::~GetText()
{
}

// Bind to a resource bundle
void
GetText::bindPropertyResourceBundle(PropertyResourceBundle &bundle_in)
{
	pbundle = &bundle_in;
}

// Get the resource bundle
PropertyResourceBundle
GetText::getPropertyResourceBundle(void)
{
	return (*pbundle);
}

// Get localized text
const char *
GetText::getText(char *text)
{
	const char *newtext;
	char *msgid;

	// See if there is anything to localize
	if (!pbundle || !pbundle->getLocale())
		return (text);

	// Attempt to get localized text
	msgid = getMessageId(text);
	newtext = getTextFromId(msgid);
	if (!newtext || !newtext[0])
		newtext = text;

	delete msgid;
	return (newtext);
}

// Get localized text by it's message ID
const char *
GetText::getTextFromId(char *msgid)
{
	char key[sizeof ("clcommands.Cnnnnnn")];

	(void) sprintf(key, "clcommands.%7.7s", msgid);
	return (getString(key));
}

// Get detailed description associated with message ID
const char *
GetText::getDetailsFromId(char *msgid)
{
	char key[sizeof ("clcommands.Cnnnnnn.details")];

	(void) sprintf(key, "clcommands.%7.7s.details", msgid);
	return (getString(key));
}

// Get suggested course of action associated with message ID
const char *
GetText::getActionFromId(char *msgid)
{
	char key[sizeof ("clcommands.Cnnnnnn.action")];

	(void) sprintf(key, "clcommands.%7.7s.action", msgid);
	return (getString(key));
}

//
// Get text using a resource property key
// Returns a null string, rather than a null pointer,
// on failure.
//
const char *
GetText::getString(char *key)
{
	const char *newtext = (char *)0;

	// Get the string
	if (pbundle)
		newtext = pbundle->getString(key);

	return ((newtext != (char *)0) ? newtext : "");
}

// Generate a message ID
char *
GetText::getMessageId(char *string)
{
	char *msgid;
	char *tempstr;
	unsigned int id;
	int count = 0;
	int i = 0;
	char c;

	// Make sure we have something to do
	if (!string)
		return ((char *)0);

	//
	// Search the string for:
	// 	'\' and remove it
	//	'\n' and replace with '\''n'
	//
	// The '\' won't get here when coming from a gettext() call
	// during runtime, so it must be removed when coming from
	// the messages.po file used to create the property files.
	//
	// The '\n' does not get included when making the msgid, so
	// strings which are the same except for extra '\n' will not
	// be added to the property file.  For example, the string
	// "Fail." versus "Fail.\n" will get the same msgid, and one
	// of them wouldn't be added to the property file.  At runtime
	// printing the string might get the wrong one and the formatting
	// of the text will look bad.  By replacing the '\n' char with
	// '\' and 'n', both of those characters are used when making the
	// msgid, so the strings would no longer have the same msgid.
	//

	// Adding an extra 20 to the size of the string in case
	// there are a lot of '\n' chars which we must replace
	tempstr = new char[strlen(string) + 20];
	c = string[0];
	while (c != '\0') {
		switch (c) {

		case '\\':
			// Don't add this char if not followed by 'n'
			if (string[i+1] != 'n') {
				break;
			} else {
				tempstr[count++] = c;
				break;
			}
		case '\n':
			// Replace with '\' and 'n'
			tempstr[count++] = '\\';
			tempstr[count++] = 'n';
			break;
		default:
			tempstr[count++] = c;
			break;
		}

		// Next char
		c = string[++i];
	}

	// Terminate the new string
	tempstr[count] = '\0';

	// Allocate new memory for the ID
	msgid = new char[sizeof ("Cnnnnnn")];

	// Generate the ID
	STRLOG_MAKE_MSGID(tempstr, id);
	(void) sprintf(msgid, "C%6.6d", id);

	// Delete the tempstr
	delete [] tempstr;

	// Done
	return (msgid);
}
