//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)ValueList.cc	1.8	08/07/24 SMI"

//
// This is the ValueList class.
//
// This class manages a collection of "string" values.
//

#include "ValueList.h"

#include <string.h>

// Construct an empty value list.
ValueList::ValueList(bool isSet)
{
	set = isSet;
}

// Construct a value list with it's first "string" element.
ValueList::ValueList(char *string, bool isSet)
{
	set = isSet;
	add(string);
	li = l.begin();
}

// Construct a value list.   If flagged, split the "string".
ValueList::ValueList(int splitflag, char *string, bool isSet)
{
	set = isSet;
	add(splitflag, string);
	li = l.begin();
}

// Descructor
ValueList::~ValueList()
{
	std::list<char *>::iterator myli;

	for (myli = l.begin();  myli != l.end();  ++myli)
		delete *myli;
}

// Add a value to the list.
void
ValueList::add(char *string)
{
	if (set)
		if (isValue(string) == 1)
			return;

	char *buffer;

	if (string) {
		buffer = new char[strlen(string) + 1];
		(void) strcpy(buffer, string);
		l.push_back(buffer);
	}
}

// Add value(s) to the list.   If flagged, split the "string".
void
ValueList::add(int splitflag, char *string)
{
	if (!string)
		return;

	if (splitflag) {
		split(" ,", string);
	} else {
		add(string);
	}
}

// Append a string to the end of the current element
void
ValueList::append(char *string, char separator)
{
	char *buffer;
	char sep_str[2] = {0, 0};

	if (string) {
		buffer = new char[strlen(string) + strlen(*li) + 2];
		(void) strcpy(buffer, *li);
		if (separator != 0) {
			sep_str[0] = separator;
			(void) strcat(buffer, sep_str);
		}
		(void) strcat(buffer, string);
		delete *li;
		*li = buffer;
	}
}

// Prepend a string to the end of the current element
void
ValueList::prepend(char *string, char separator)
{
	char *buffer;
	char sep_str[2] = {0, 0};

	if (string) {
		buffer = new char[strlen(string) + strlen(*li) + 2];
		(void) strcpy(buffer, string);
		if (separator != 0) {
			sep_str[0] = separator;
			(void) strcat(buffer, sep_str);
		}
		(void) strcat(buffer, *li);
		delete *li;
		*li = buffer;
	}
}

// Clear the list.
void
ValueList::clear()
{
	l.clear();
}

// Reset the iterator to the beginning of the list.
void
ValueList::start()
{
	li = l.begin();
}

// Increment the iterator.
void
ValueList::next()
{
	if (li != l.end())
		++li;
}

// Return 1 when the iterator is at the end.
int
ValueList::end()
{
	return (li == l.end() ? 1 : 0);
}

// Get the current value.
char *
ValueList::getValue()
{
	return (*li);
}

// Return 1 when the value is in the list.
int
ValueList::isValue(char *value)
{
	std::list<char *>::iterator myli;

	if (!value)
		return (0);

	for (myli = l.begin();  myli != l.end();  ++myli) {
		if (strcasecmp(value, *myli) == 0)
			return (1);
	}

	return (0);
}

// Return 1 when the value is in the list.
int
ValueList::isValue(char *value, bool doCaseCompare)
{
	std::list<char *>::iterator myli;
	int (*stringCompare)(const char *, const char *);

	if (!value)
		return (0);
	//
	// If doCaseCompare is set, perform case sensitive comparison
	// else do case insensitive comparison.
	//
	if (doCaseCompare) {
		stringCompare = strcmp;
	} else {
		stringCompare = strcasecmp;
	}

	for (myli = l.begin();  myli != l.end();  ++myli) {
		if (stringCompare(value, *myli) == 0)
			return (1);
	}

	return (0);
}

// Get the number of elements in the list.
int
ValueList::getSize()
{
	return (l.size());
}

//
// Split the string, using only the first "token" found in the "string".
// Then, add each sub-string to the list.
//
void
ValueList::split(char *tokens, char *string)
{
	char *ptoken;
	char token[2];
	char *pstring;
	char *buffer;
	int found = 0;

	if (!string)
		return;

	if (!tokens) {
		add(string);
		return;
	}

	// See if we need to split the string
	for (ptoken = tokens;  *ptoken;  ++ptoken) {
		if (strchr(string, *ptoken)) {
			token[0] = *ptoken;
			token[1] = 0;
			++found;
			break;
		}
	}

	// No tokens found;   add what we have
	if (!found) {
		add(string);
		return;
	}

	// Make copy of the string
	buffer = new char[strlen(string) + 1];
	(void) strcpy(buffer, string);

	// Split it up
	for (pstring = strtok(buffer, token); pstring;
	    pstring = strtok(NULL, token))
		add(pstring);

	//
	// If we didn't add anything, it means the string is all tokens!
	// Add a null string so that the caller atleast can detect an error.
	//
	if (getSize() == 0) {
		add("");
	}

	// Delete the copy
	delete buffer;
}
