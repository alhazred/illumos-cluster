//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident   "@(#)Properties.cc	1.10	09/03/05 SMI"

//
// This is the source for the PropertyValue and Properties classes.
//

#include "Properties.h"

#include <errno.h>
//
// PropertyValue class
//

// Constructor for PropertyValue
PropertyValue::PropertyValue() {}

// Destructor for PropertyValue
PropertyValue::~PropertyValue() {}

//
// Set the property value.
// If a value is already set, overwrite it.
//
void
PropertyValue::setPropertyValue(char *value_in)
{
	const char *value_out;

	// If a value is not given, just return
	if (!value_in)
		return;

	// Set the value
	value = value_in;
}

//
// Add a comment.
//
void
PropertyValue::addComment(char *comment_in, int unique)
{
	// If a comment is not given, just return
	if (!comment_in)
		return;

	// If the unique flag is set, punt if it already exists.
	if (unique && comments.isValue(comment_in))
		return;

	// Add the comment
	comments.add(comment_in);
}

// Get the property value.
const char *
PropertyValue::getPropertyValue()
{
	return (value.data());
}

// Get the comments ValueList
const ValueList *
PropertyValue::getComments()
{
	return (&comments);
}

//
// Properties class
//

// Constructor for Properties
Properties::Properties()
{
	propertiesFile = (char *)0;
	isLoaded = 0;
	includeComments = 0;
	iconv_cd1 = 0;
	iconv_cd2 = 0;
}

// Desctructor for Properties
Properties::~Properties()
{
	// Properties file
	if (propertiesFile)
		delete propertiesFile;

	// Property values
	for (mi = m.begin();  mi != m.end();  ++mi)
		delete mi->second;
}

// Set the name of the properties file.
void
Properties::setPropertiesFile(char *filename)
{
	if (!filename)
		return;

	if (propertiesFile)
		delete propertiesFile;

	propertiesFile = new char[strlen(filename) + 1];
	(void) strcpy(propertiesFile, filename);
}

// Return the name of the default properties file
const char *
Properties::getPropertiesFile(void)
{
	return (propertiesFile);
}

// Load the current properties file on the next reference.
void
Properties::setLoadOnReference()
{
	isLoaded = 0;
}

// Include comments in load and store.
void
Properties::setIncludeComments()
{
	includeComments = 1;
}

//
// Set the property value for the given key.
// Return null if successful.  If a key
// already exists, return it's value.  Other
// errors will return the null string.
//
const char *
Properties::setProperty(char *key_in, char *value_in)
{
	const char *value_out;
	std::string key_string;
	PropertyValue *pvalue;

	// If either key or value are not given, return the null string
	if (!key_in || !value_in)
		return ("");

	// If there is already a key, return it's value.
	if ((value_out = getProperty(key_in)) != (char *)0)
		return (value_out);

	// Add the key-value pair to the map
	key_string = key_in;
	pvalue = new PropertyValue;
	pvalue->setPropertyValue(value_in);
	m[key_string] = pvalue;

	// Success
	return ((char *)0);
}

//
// Add a comment for the given key.
// If the "unique" flag is set, it is only added if it is unique.
// Return zero on success.
//
const int
Properties::addComment(char *key_in, char *comment_in, int unique)
{
	std::string key_string;
	const char *value_out;
	PropertyValue *pvalue;

	// If either key or commant are not given, just return
	if (!key_in || ! comment_in)
		return (1);

	// Find the PropertyValue from the key
	key_string = key_in;
	mi = m.find(key_string);
	pvalue = mi->second;
	if (!pvalue)
		return (1);

	// Add the comment
	pvalue->addComment(comment_in, unique);

	// Success
	return (0);
}

//
// Get the property value from the key.
// Return null if the key is not found.
//
const char *
Properties::getProperty(char *key_in)
{
	std::string key_string;
	PropertyValue *pvalue = (PropertyValue *)0;

	// Make sure that we have a key
	if (!key_in)
		return ((char *)0);

	// If the properties file is not yet loaded, load it now.
	if (propertiesFile && !isLoaded)
		(void) load();

	// Find the value from the key
	key_string = key_in;
	mi = m.find(key_string);
	if (mi != m.end())
		pvalue = mi->second;

	return (pvalue ? pvalue->getPropertyValue() : (char *)0);
}

//
// Get the property comment list from the key.
// Return null if the key is not found.
//
const ValueList *
Properties::getComments(char *key_in)
{
	static const ValueList emptylist;
	std::string key_string;
	PropertyValue *pvalue = (PropertyValue *)0;

	// Make sure that we have a key
	if (!key_in)
		return ((ValueList *)0);

	// If the properties file is not yet loaded, load it now.
	if (propertiesFile && !isLoaded)
		(void) load();

	// Find the value from the key
	key_string = key_in;
	mi = m.find(key_string);
	if (mi != m.end())
		pvalue = mi->second;

	return (pvalue ? pvalue->getComments() : (ValueList *)0);
}

//
// Load the properties from the given file.
//
// See the "load" method description on
//
// http:// java.sun.com/j2se/1.4.2/docs/api/java/util/Properties.html
// for file requirements.
//
// If the "preserve" flag is set to non-zero, value strings are
// preserved with their escape sequences in tact, rather than mapped
// as would be done by the compiler.
//
// If successful, this function returns zero.  Otherwise,
// it returns "errno" (/usr/include/sys/errno.h).
//
int
Properties::load()
{
	return (load(propertiesFile, 0));
}

int
Properties::load(char *filename)
{
	return (load(filename, 0));
}

int
Properties::load(char *filename, int preserve)
{
	FILE *fp;
	std::string key_string;
	std::string value_string;
	std::string comment_string;
	ValueList comments;
	char *key;
	char *value;
	char *temp_str = NULL;
	int status;
	int c;
	int escapes;
	int escapeit;
	int breakloop;
	enum properties_format format = format_unknown;
	int start_size = m.size();
	enum state {
		state_scanfor_key,
		state_comment,
		state_key,
		state_scanfor_value,
		state_value,
		state_scanfor_cont_value
	} state;
	uint_t equal_count;
	uint_t column_count;

	if (!filename)
		return (EINVAL);

	// Do not re-try the load on file or other errors
	isLoaded = 1;

	// Open the properties file
	if ((fp = fopen(filename, "r")) == (FILE *)0)
		return (errno);

	// Get the locale conversion string and initialize iconv
	// conversion descriptors
	char *encoding = nl_langinfo(CODESET);
	if (encoding && (strcmp(encoding, "C") == 0)) {
		encoding = NULL;
	}
	if (encoding) {
		if ((iconv_cd1 = iconv_open("UTF-8", "UCS-2BE")) ==
			(iconv_t)-1) {
			encoding = NULL;
		}

		if (encoding && (strcmp(encoding, "UTF-8") != 0)) {
			if ((iconv_cd2 = iconv_open(encoding, "UTF-8")) ==
				(iconv_t)-1) {
				encoding = NULL;
			}
		}
	}

	// Initialize for read loop
	(void) key_string.erase();
	(void) value_string.erase();
	(void) comment_string.erase();
	(void) comments.clear();
	state = state_scanfor_key;
	breakloop = 0;
	equal_count = 0;
	column_count = 0;

	// Read loop
	while ((c = fgetc(fp)) != EOF) {
		switch (state) {

		//
		// Looking for the next key.
		// Skip comments, blank lines, and white space.
		//
		case state_scanfor_key:
			switch (c) {

			//
			// Comment - change state
			//
			case '#':
			case '!':
				state = state_comment;
				break;

			//
			// End of a blank line
			//

			// "\r", "\r\n", and "\n" are all EOL
			case '\r':
				if ((c = fgetc(fp)) != '\n') {
					if (c == EOF) {
						++breakloop;
					} else {
						(void) ungetc(c, fp);
					}
				}
				/* FALLTHROUGH */

			case '\n':
				/* FALLTHROUGH */

			//
			// White space
			//
			case ' ':
			case '\t':
			case '\f':
				break;

			// New key - change state
			default:
				(void) ungetc(c, fp);
				state = state_key;
				break;
			}
			break;

		//
		// Processing a Comment.
		//
		// The comment ends at the end of the line
		//
		case state_comment:
			switch (c) {

			//
			// End of a comment line - change state
			//

			// "\r", "\r\n", and "\n" are all EOL
			case '\r':
				if ((c = fgetc(fp)) != '\n') {
					if (c == EOF) {
						++breakloop;
					} else {
						(void) ungetc(c, fp);
					}
				}
				/* FALLTHROUGH */

			case '\n':
				if (includeComments && comment_string.data()) {
					comments.add(
					    (char *)comment_string.data());
					comment_string.erase();
				}
				state = state_scanfor_key;
				break;

			default:
				if (includeComments)
					comment_string += c;
				break;
			}
			break;

		//
		// Saving a key
		//
		// The key ends with an unescaped seperator
		// of space, tab, ':', or '='.
		//
		case state_key:
			switch (c) {

			//
			// Escape
			//
			// If we are escaping a space, tab, ':', or '=',
			// add the character to the key, without the
			// escape character itself.   Anything else is
			// added to the key as-is.
			//
			case '\\':
				escapes = 1;

				// Get the remainder of consecutive escapes
				while ((c = fgetc(fp)) == '\\') {
					++escapes;
				}

				// Only an odd number of escapes can escape
				escapeit = ((escapes % 2) == 1) ? 1 : 0;

				//
				// If this is EOF, store whatever we
				// have and break out of the read loop.
				//
				if (c == EOF) {
					if (escapeit)
						--escapes;
					while (escapes--)
						key_string += '\\';
					add_key_value(key_string, value_string,
					    comments, &format);
					++breakloop;
					break;
				}

				// We really only want to escape seperators
				if (escapeit) {
					switch (c) {
					case ' ':
					case '\t':
					case ':':
					case '=':
						--escapes;
						break;

					default:
						escapeit = 0;
					}
				}

				// Add any remaining escapes to the key
				while (escapes--)
					key_string += '\\';

				// If the seperator was escaped, add to key
				if (escapeit) {
					key_string += c;

				// Otherwise, look at it again next time
				} else {
					(void) ungetc(c, fp);
				}

				break;

			//
			// Unescaped seperator (space, tab, ':', or '=').
			// Next state is to scan for the value.
			//
			case ' ':
			case '\t':
			case ':':
			case '=':
				state = state_scanfor_value;
				break;

			//
			// End of line without a value means that
			// we have a key with no value.   Store the
			// key without the value.   The next state
			// is to go back and scan for the next key.
			//

			// "\r", "\r\n", and "\n" are all EOL
			case '\r':
				if ((c = fgetc(fp)) != '\n') {
					if (c == EOF) {
						++breakloop;
					} else {
						(void) ungetc(c, fp);
					}
				}
				/* FALLTHROUGH */

			case '\n':
				add_key_value(key_string, value_string,
				    comments, &format);
				(void) key_string.erase();
				(void) value_string.erase();
				(void) comments.clear();
				state = state_scanfor_key;
				equal_count = 0;
				column_count = 0;
				break;

			//
			// Add to the key
			//
			default:
				key_string += c;
				break;
			}
			break;

		//
		// Looking for the start of the next value.
		// Skip any additional seperators (space,
		// tab, ':', or '='), as well as any
		// escape sequences.
		//
		case state_scanfor_value:

			//
			// Skip seperators
			//
			switch (c) {
			case ' ':
			case '\t':
			case ':':
				if (!column_count) {
					column_count++;
					break;
				} else {
					// Treat it as value
					(void) ungetc(c, fp);
					state = state_value;
					break;
				}

			case '=':
				if (!equal_count) {
					equal_count++;
					break;
				} else {
					// Treat it as value
					(void) ungetc(c, fp);
					state = state_value;
					break;
				}
			//
			// End of line without a value means that
			// we have a key with no value.   Store the
			// key without the value.   The next state
			// is to go back and scan for the next key.
			//

			// "\r", "\r\n", and "\n" are all EOL
			case '\r':
				if ((c = fgetc(fp)) != '\n') {
					if (c == EOF) {
						++breakloop;
					} else {
						(void) ungetc(c, fp);
					}
				}
				/* FALLTHROUGH */

			case '\n':
				add_key_value(key_string, value_string,
				    comments, &format);
				(void) key_string.erase();
				(void) value_string.erase();
				(void) comments.clear();
				state = state_scanfor_key;
				equal_count = 0;
				column_count = 0;
				break;

			//
			// We found the first character of our value.
			// Put it back, and look at it next time.
			// Our next state will process the value.
			//
			default:
				(void) ungetc(c, fp);
				state = state_value;
				break;
			}
			break;

		//
		// Saving a value
		//
		// The value ends with an unescaped end-of-line (EOL).
		//
		case state_value:
			switch (c) {

			//
			// Escape
			//
			// If this escapes an end-of-line sequence,
			// we are ready to look for a continuation
			// of the value on the next line.
			//
			// Unless the "preserve" flag is set, special
			// characters (except '\ddd') in the value
			// are recognized and mapped as would be done
			// by the compiler.
			//
			// Anything else is added to the value as-is.
			//
			case '\\':
				c = fgetc(fp);

				if (preserve &&
				    c != '\n' &&
				    c != '\r' &&
				    c != EOF) {
					value_string += '\\';
					value_string += c;
					break;
				}

				switch (c) {

				//
				// Escaped end of line (EOL)
				//

				// "\r", "\r\n", and "\n" are all EOL
				case '\r':
					if ((c = fgetc(fp)) != '\n') {
						if (c == EOF) {
							add_key_value(
							    key_string,
							    value_string,
							    comments,
							    &format);
							++breakloop;
						} else {
							(void) ungetc(
							    c, fp);
						}
					}
					/* FALLTHROUGH */

				case '\n':
					state = state_scanfor_cont_value;
					break;

				//
				// Special characters in the value
				//

				// Newline
				case 'n':
					value_string += '\n';
					break;

				// Tab
				case 't':
					value_string += '\t';
					break;

				// Vertical tab
				case 'v':
					value_string += '\v';
					break;

				// Backspace
				case 'b':
					value_string += '\b';
					break;

				// Carriage Return
				case 'r':
					value_string += '\r';
					break;

				// Formfeed
				case 'f':
					value_string += '\f';
					break;

				//
				// Unicode escape sequence
				//
				// Unicode escape sequences are of the form
				// \uxxxx where xxxx is in hex.  This needs
				// to be translated into a two byte UCS-2
				// string, which may then be converted into
				// the target locale string
				//
				case 'u':
					// Got the converted string, save it
					// in the value string and delete it
					if ((temp_str = convertUnicode(fp,
					    encoding)) == NULL) {
						// Couldn't get the sting
						break;
					}

					// Copy each char to value_string
					value_string += temp_str;

					temp_str = NULL;

					break;

				//
				// End of File
				//
				case EOF:
					add_key_value(key_string, value_string,
					    comments, &format);
					++breakloop;
					break;

				//
				// Other characters are added to the value
				//
				default:
					value_string += c;
					break;
				}
				break;

			//
			// Unescaped end of line (EOL)
			//
			// This indicates that we have found the
			// end of our value.  The next state is
			// to start over again looking for
			// another key.
			//

			// "\r", "\r\n", and "\n" are all EOL
			case '\r':
				if ((c = fgetc(fp)) != '\n') {
					if (c == EOF) {
						add_key_value(key_string,
						    value_string, comments,
						    &format);
						++breakloop;
					} else {
						(void) ungetc(c, fp);
					}
				}
				/* FALLTHROUGH */

			case '\n':
				add_key_value(key_string, value_string,
				    comments, &format);
				(void) key_string.erase();
				(void) value_string.erase();
				(void) comments.clear();
				state = state_scanfor_key;
				equal_count = 0;
				column_count = 0;
				break;

			//
			// Anything else is added to the value.
			//
			default:
				value_string += c;
				break;
			}
			break;

		//
		// Looking for the continuation of a value
		// from a previous line ending in a value
		// and an escaped end-of-line sequence.
		//
		// Just skip over all leading whitespace.
		//
		case state_scanfor_cont_value:
			switch (c) {

			//
			// Skip whitespace
			//
			case ' ':
			case '\t':
				break;

			//
			// Anything else should be processed as a value.
			//
			default:
				(void) ungetc(c, fp);
				state = state_value;
			}
			break;
		}

		// Break out of the read loop?
		if (breakloop)
			break;
	}
	// Close iconv descriptors
	if (encoding) {
		(void) iconv_close(iconv_cd1);
		if ((strcmp(encoding, "UTF-8") != 0)) {
			(void) iconv_close(iconv_cd2);
		}
	}

	// Close the file
	(void) fclose(fp);
	if (ferror(fp))
		return (errno);

	return (0);
}

//
// convertUnicode
//
// Converts the next four characters from the FILE *fp
// into UCS-2 characters, then into UTF-8 and finally into
// the locale encoding specified.  The characters from
// the file must be valid hexadecimal characters.
//
// Returns a char pointer to a converted string.  Caller
// is responsible for freeing the memory.
//
// If the conversion fails for any reason, then NULL
// is returned
//
char *
Properties::convertUnicode(FILE *fp, char *encoding)
{
	char byte0[3];
	char byte1[3];
	char ucs[3];

	if (!fp || !encoding)
		return (NULL);

	// Get the next four chars from the file
	byte0[0] = fgetc(fp);
	byte0[1] = fgetc(fp);
	byte0[2] = '\0';

	byte1[0] = fgetc(fp);
	byte1[1] = fgetc(fp);
	byte1[2] = '\0';

	// Verify the input to make sure it is actually hex!
	if (!isxdigit(byte0[0]) || !isxdigit(byte0[1]) ||
	    !isxdigit(byte1[0]) || !isxdigit(byte1[1])) {
		return (NULL);
	}

	// Convert each byte into a UCS-2 char
	ucs[0] = (char)strtol(byte0, NULL, 16);
	ucs[1] = (char)strtol(byte1, NULL, 16);
	ucs[2] = '\0';

	// Retun the converted string
	return (convertUCS2String(ucs, encoding));
}

//
// convertUCS2String
//
// Converts a string from UCS-2 encoding to the locale encoding
// specified.
//
// Returns a new char * containing the converted string.  Caller is
// responsible for deleting the new char *.
//
char *
Properties::convertUCS2String(const char *src_str, char *encoding)
{
	int error = 0;
	char *dest_str = NULL;
	char *temp_str = NULL;
	char *temp_ptr = NULL;
	size_t inlen1, outlen1, inlen2, outlen2;

	// Check for NULL or empty input
	if ((src_str == NULL)) {
		return (NULL);
	}
	if ((encoding == NULL) || (*encoding == '\0')) {
		return (NULL);
	}

	//
	// Do the UCS-2 to UTF-8 conversion...
	//

	inlen1 = 2;

	// Calculate the max output string length
	outlen1 = inlen1 * UTF_8_FACTOR;

	temp_str = new char[BUFSIZ];

	// Save the start of temp_str
	char *temp_str_start = temp_str;

	// Perform the iconv to UTF-8
	if (iconv(iconv_cd1, &src_str, &inlen1, &temp_str, &outlen1)
	    == (size_t)-1) {
		// An error occurred, or not all characters were converted
		goto cleanup;
		error++;
	}
	// null-terminate the new string
	*temp_str = '\0';
	temp_str = temp_str_start;


	//
	// Do the UTF-8 to locale conversion...
	//

	// Get the temp string length
	inlen2 = strlen(temp_str);

	// Calculate the max output string length
	outlen2 = inlen2 * UTF_8_FACTOR;

	dest_str = new char[BUFSIZ];

	// Save the start of dest_str
	temp_ptr = dest_str;

	if (strcmp(encoding, "UTF-8") != 0) {
		// Perform the iconv to current locale
		if (iconv(iconv_cd2, (const char **)&temp_str, &inlen2,
				&dest_str, &outlen2) == (size_t)-1) {
			// An error occurred, or not all characters
			// were converted
			goto cleanup;
			error++;
		}
		// null-terminate the new string
		*dest_str = '\0';
		dest_str = temp_ptr;
	}

cleanup:
	temp_str = temp_str_start;

	if (error)
		return (NULL);

	if (strcmp(encoding, "UTF-8") != 0) {
		delete temp_str;
		return (dest_str);
	} else {
		delete dest_str;
		return (temp_str);
	}
}

//
// Add a key-value pair to our map.
//
// If the key is "PRINTF_STYLE", the key is not added to the
// map.  Rather, we update the "format" to indicate whether
// or not the format is "printf" or "unknown".
//
// Values, or elements, found in Java properties files identify their
// substitution tokens with '{<N>}'.  But, the printf style of functions
// use "string" substitution tokens of the format '%<N>$s'.
//
// If the "format" style is known to be a "printf" style, no processing
// of '{<N>}' strings is performed.   But, if the "format" is "unknown",
// all '{<N>}' sequences are converted to '%<N>$s' sequences.
//
void
Properties::add_key_value(std::string &key_string, std::string &value_string,
    ValueList &comments, enum properties_format *pformat)
{
	const char *key = key_string.data();
	const char *value = value_string.data();
	PropertyValue *pvalue;

	// Make sure that we have something to do
	if (!key)
		return;

	//
	// "PRINTF_STYLE" keys do not get added to our map.
	// However, they change the "format" setting of the caller.
	//
	if (value && (strcmp(key, "PRINTF_STYLE") == 0)) {
		if (pformat) {
			if (strcmp(value, "true") == 0) {
				*pformat = format_printf;
			} else {
				*pformat = format_unknown;
			}
		}

		// Done
		return;
	}

	//
	// If the value format is not already "printf style", we
	// need to convert it.
	//
	if (value && (!pformat || (*pformat != format_printf)) &&
	    (strchr(value, '{') != (char *)0)) {
		std::string new_value;
		char *number = new char[strlen(value) + 1];
		char *v;
		char *v2;
		char *n;

		new_value.erase();
		v = (char *)value;

		while (*v) {
			switch (*v) {

			// Substitution sequence
			case '{':
				// Record our starting point and increment
				v2 = v++;

				// Eliminate white space
				while (isspace(*v))
					v++;

				// Get the number
				*number = 0;
				n = number;
				while (isdigit(*v))
					*n++ = *v++;

				// If there is no number, we are done
				if (*number == 0) {
					v = v2;
					new_value += v;
					break;
				}

				// Eliminate white space
				while (isspace(*v))
					v++;

				// If no closing '}', we are done
				if (*v != '}') {
					v = v2;
					new_value += v;
					break;
				}

				// {<number>} is %<number>$s
				new_value += '%';
				new_value += number;
				new_value += "$s";

				break;

			// Other characters
			default:
				new_value += *v;
				break;
			}

			// Next
			++v;
		}
		// Done with our buffers
		delete number;

		// We have a new value string
		value = new_value.data();
	}

	// Update the map
	mi = m.find(key_string);
	if (mi != m.end()) {
		pvalue = mi->second;
		pvalue->value = value;
	} else {
		pvalue = new PropertyValue;
		pvalue->value = value;
		m[key_string] = pvalue;
	}

	// Comments
	if (includeComments) {
		char *comment;

		for (comments.start();  comments.end() != 1;
		    comments.next()) {
			comment = comments.getValue();
			if (!comment)
				continue;

			addComment((char *)key, comment, 1);
		}
	}
}

//
// Store the properties to the given file.
//
// The generated file will be sorted by key.
//
// If filename is a dash (-), print to stdout.
//
// The new file will conform to requirements specified by "load" method
// description found on
// http:// java.sun.com/j2se/1.4.2/docs/api/java/util/Properties.html
//
// If successful, this function returns zero.  Otherwise,
// it returns "errno" (/usr/include/sys/errno.h).
int
Properties::store()
{
	return (store(propertiesFile));
}

int
Properties::store(char *filename)
{
	FILE *fp;
	char *key;
	char *value;
	ValueList *comments;
	char *comment;

	char *buffer;
	char *v;
	PropertyValue *pvalue;

	if (!filename)
		return (EINVAL);

	// Open the properties file
	if (strcmp(filename, "-") == 0) {
		fp = stdout;
	} else {
		if ((fp = fopen(filename, "a")) == (FILE *)0)
			return (errno);
	}

	// Iterate through the map
	for (mi = m.begin();  mi != m.end();  ++mi) {

		// Get the key and PropertyValue
		key = (char *)mi->first.data();
		pvalue = mi->second;
		if (!key || !pvalue)
			continue;

		// Get the value
		value = (char *)pvalue->getPropertyValue();
		if (!value)
			continue;

		// Get the list of comments, if any
		comments = (ValueList *)pvalue->getComments();

		// Print a blank line
		(void) putc('\n', fp);

		// Print comments
		if (includeComments && comments) {
			for (comments->start();  comments->end() != 1;
			    comments->next()) {
				comment = comments->getValue();
				if (!comment)
					continue;

				(void) putc('#', fp);
				(void) fputs(comment, fp);
				if (comment[strlen(comment) - 1] != '\n')
					(void) putc('\n', fp);
			}
		}

		// Print key
		(void) fprintf(fp, "%s = ", key);

		// Print value
		if (!strchr(value, '\n')) {
			(void) fputs(value, fp);

		// Multi-line value
		} else {
			buffer = new char[strlen(value) + 1];
			(void) strcpy(buffer, value);
			v = strtok(buffer, "\n");
			while (v) {
				(void) fputs(v, fp);
				v = strtok(0, "\n");
				if (v)
					(void) fputs("\\\n\t", fp);
			}
		}
		(void) putc('\n', fp);
	}

	// Close the file, unless stdout.
	if (fp != stdout)
		(void) fclose(fp);

	// Done
	return (0);
}
