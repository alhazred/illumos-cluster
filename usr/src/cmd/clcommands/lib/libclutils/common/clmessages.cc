//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clmessages.cc	1.6	08/05/20 SMI"

//
// Provides support for all of the messaging interfaces
//

#include "clmsg_int.h"
#include "clmessages.h"
#include "cl_errno.h"

// Global variables
TextMessage clerrorMessage;		// for clerror()
TextMessage cltextMessage;		// for clwarning(), clmessage()
GetText gettextGetText;			// for gettext()
GetText getstringGetText;		// for getstring()

static TextMessage *clerrMsg = NULL;	// for clerr() C-Wrapper interface
static TextMessage *cltxtMsg = NULL;	// for clwarn(), clmsg() C-Wrapper

void
init_clmessages(char *cmd_name)
{
	char *locale;
	unsigned int alloc_flag = 0;
	char *new_locale = (char *)NULL;

	// Should we use a "test" locale?
	locale = getenv(CLCOMMANDS_VAR_TEST_LOCALE);
	if (locale && !locale[0])
		locale = (char *)0;

	//
	// I18N housekeeping
	//
	if (!locale) {
		locale = setlocale(LC_ALL, "");
		if (locale && (strcmp(locale, "C") == 0))
			locale = (char *)0;
	}

	// If locale is "zh", default to "zh_CN".
	if (locale && (strcmp(locale, "zh") == 0)) {
		new_locale = (char *)calloc(6, sizeof (char));
		// if can't allocate memory then just continue
		// no way to return error condition from here
		if (new_locale != NULL) {
			alloc_flag = 1;
			(void) strcpy(new_locale, "zh_CN");
			locale = new_locale;
		}
	}

	// Bind to our resource property bundles
	clerrorMessage.bindPropertyResourceBundle(
	    *(create_error_bundle(locale)));
	cltextMessage.bindPropertyResourceBundle(
	    *(create_text_bundle(locale)));
	gettextGetText.bindPropertyResourceBundle(
	    *(create_text_bundle(locale)));
	getstringGetText.bindPropertyResourceBundle(
	    *(create_shared_bundle(locale)));

	// Set the command names for clerror(), clwarning(), clmessage()
	clerrorMessage.setCommandName(cmd_name);
	cltextMessage.setCommandName(cmd_name);

	// Set the other options for clerror()
	clerrorMessage.setErrorFlags(get_default_error_flags());

	// Set the other options for clwarning()
	cltextMessage.setWarningFlags(get_default_warning_flags());

	// Set the other options for clmessage()
	cltextMessage.setMessageFlags(get_default_message_flags());

	if (alloc_flag == 1) {
		free(new_locale);
	}
}

//
// Get the resource bundle for errors
//
PropertyResourceBundle *
create_error_bundle(char *locale)
{
	PropertyResourceBundle *bundle = new PropertyResourceBundle();
	char *dirname;
	char *basename;

	// Initialize resource bundle for error messages (clerror)
	if ((dirname = getenv(CLCOMMANDS_VAR_PROPS1_DIRNAME)) == (char *)0)
		dirname = CLCOMMANDS_DFLT_PROPS1_DIRNAME;
	if ((basename = getenv(CLCOMMANDS_VAR_PROPS1_BASENAME)) == (char *)0)
		basename = CLCOMMANDS_DFLT_PROPS1_BASENAME;
	(void) bundle->setFamily(dirname, basename);
	bundle->setLocale(locale);

	return (bundle);
}

//
// Get the resource bundle for other text
//
PropertyResourceBundle *
create_text_bundle(char *locale)
{
	PropertyResourceBundle *bundle = new PropertyResourceBundle();
	char *dirname;
	char *basename;

	// Initialize resource bundle for other text (gettext)
	if ((dirname = getenv(CLCOMMANDS_VAR_PROPS2_DIRNAME)) == (char *)0)
		dirname = CLCOMMANDS_DFLT_PROPS2_DIRNAME;
	if ((basename = getenv(CLCOMMANDS_VAR_PROPS2_BASENAME)) == (char *)0)
		basename = CLCOMMANDS_DFLT_PROPS2_BASENAME;
	(void) bundle->setFamily(dirname, basename);
	bundle->setLocale(locale);

	return (bundle);
}

//
// Get the resource bundle for shared text
//
PropertyResourceBundle *
create_shared_bundle(char *locale)
{
	PropertyResourceBundle *bundle = new PropertyResourceBundle();
	char *dirname;
	char *basename;

	// Initialize resource bundle for messages shared with SPM (getstring)
	if ((dirname = getenv(CLCOMMANDS_VAR_PROPS3_DIRNAME)) == (char *)0)
		dirname = CLCOMMANDS_DFLT_PROPS3_DIRNAME;
	if ((basename = getenv(CLCOMMANDS_VAR_PROPS3_BASENAME)) == (char *)0)
		basename = CLCOMMANDS_DFLT_PROPS3_BASENAME;
	(void) bundle->setFamily(dirname, basename);
	bundle->setLocale(locale);

	return (bundle);
}

//
// Get the error format flags
//
unsigned int
get_default_error_flags()
{
	unsigned int flags = TM_DEFAULT_FLAGS;
	if (getenv(CLCOMMANDS_VAR_VERBOSE_ERRORS)) {
		flags |= (TM_FLAG_VERBOSE | TM_FLAG_PRINTID);
	} else if (getenv(CLCOMMANDS_VAR_NO_MESSAGE_IDS)) {
		flags &= ~TM_FLAG_PRINTID;
	}
	return (flags);
}

//
// Get the warning format flags
//
unsigned int
get_default_warning_flags()
{
	return (TM_FLAG_PRINTWARNSTR);
}

//
// Get the message format flags
//
unsigned int
get_default_message_flags()
{
	return (0);
}

//
// Initializes the static clerrMsg and cltxtMsg objects for use
// by outside programs, especially those written in C.
//
int
init_clmsg(char *cmd_name)
{
	char *locale;
	unsigned int alloc_flag = 0;
	char *new_locale = (char *)NULL;

	// Should we use a "test" locale?
	locale = getenv(CLCOMMANDS_VAR_TEST_LOCALE);
	if (locale && !locale[0])
		locale = (char *)0;

	//
	// I18N housekeeping
	//
	if (!locale) {
		locale = setlocale(LC_ALL, "");
		if (locale && (strcmp(locale, "C") == 0))
			locale = (char *)0;

	}

	// If locale is "zh", default to "zh_CN".
	if (locale && (strcmp(locale, "zh") == 0)) {
		new_locale = (char *)calloc(6, sizeof (char));
		if (new_locale == NULL) {
			return (CL_ENOMEM);
		}
		alloc_flag = 1;
		(void) strcpy(new_locale, "zh_CN");
		locale = new_locale;
	}

	// Initialize the static variables
	clerrMsg = new TextMessage();
	cltxtMsg = new TextMessage();

	if (clerrMsg == NULL || cltxtMsg == NULL) {
		delete clerrMsg;
		delete cltxtMsg;

		return (1);
	}

	// Bind to our resource property bundles
	clerrMsg->bindPropertyResourceBundle(*(create_error_bundle(locale)));
	cltxtMsg->bindPropertyResourceBundle(*(create_text_bundle(locale)));

	// Set the other options for clerror()
	clerrMsg->setErrorFlags(get_default_error_flags());

	// Set the other options for clwarning()
	cltxtMsg->setWarningFlags(get_default_warning_flags());

	// Set the other options for clmessage()
	cltxtMsg->setMessageFlags(get_default_message_flags());

	// Set the command name
	clerrMsg->setCommandName(cmd_name);
	cltxtMsg->setCommandName(cmd_name);

	if (alloc_flag == 1) {
		free(new_locale);
	}

	return (0);
}

//
// Call printError
//
void
clerror(char *message_in, ...)
{
	va_list ap;

	va_start(ap, message_in);
	clerrMsg->va_printError(message_in, ap);
	va_end(ap);
}

//
// Call printError w/ va_list
//
void
vclerror(char *message_in, va_list ap)
{
	clerrMsg->va_printError(message_in, ap);
}

//
// Call printWarning
//
void
clwarning(char *message_in, ...)
{
	va_list ap;

	va_start(ap, message_in);
	cltxtMsg->va_printWarning(message_in, ap);
	va_end(ap);
}

//
// Call printMessage
//
void
clmessage(char *message_in, ...)
{
	va_list ap;

	va_start(ap, message_in);
	cltxtMsg->va_printMessage(message_in, ap);
	va_end(ap);
}
