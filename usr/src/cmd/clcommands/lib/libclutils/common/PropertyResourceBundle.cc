//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident   "@(#)PropertyResourceBundle.cc	1.5	08/05/20 SMI"

//
// This is the PropertyResourceBundle class.
//

#include "PropertyResourceBundle.h"

#include <stdio.h>
#include <unistd.h>
#include <errno.h>

#include <sys/debug.h>

// Basic constructor
PropertyResourceBundle::PropertyResourceBundle()
{
	init();
}

// Constructor with family directory and base name of properties files
PropertyResourceBundle::PropertyResourceBundle(char *directory_in,
    char *basename_in)
{
	init();
	(void) setFamily(directory_in, basename_in);
}

// Constructor with family directory, base name, and current locale
PropertyResourceBundle::PropertyResourceBundle(char *directory_in,
    char *basename_in, char *locale_in)
{
	init();
	setFamily(directory_in, basename_in);
	setLocale(locale_in);
}

// Destructor
PropertyResourceBundle::~PropertyResourceBundle()
{
	delete directory;
	delete basename;
	delete locale;
}

//
// Set the family directory and base name of properties files.
// Return zero only on success.   Otherwise, return errno as
// described for access(2). We use the following fallback policy
// as described for Java here
// http:// java.sun.com/j2se/1.4.2/docs/api/java/util/ResourceBundle.html
//
// baseName + "_" + language1 + "_" + country1 + "." + variant1
// baseName + "_" + language1 + "_" + country1
// baseName + "_" + language1
// baseName
//
// Note that "." separates variant1 from country1 because the formal
// syntax for locales is language[_country][.codeset][@modifier]
//
// See http:// www.debian.org/doc/manuals/intro-i18n/ch-locale.en.html
//
int
PropertyResourceBundle::setFamily(char *directory_in, char *basename_in)
{
	char *mydir;
	char *mybase;
	char *mypath;
	int status;

	// Always get rid of the old settings, even if the new ones fail
	delete directory;
	delete basename;
	directory = 0;
	basename = 0;

	// Make sure that we are being called correctly
	if (!directory_in || !basename_in) {
		return (EFAULT);
	}

	// Copy new directory name
	mydir = new char[strlen(directory_in) + 1];
	(void) strcpy(mydir, directory_in);

	// Copy new base name
	mybase = new char[strlen(basename_in) + 1];
	(void) strcpy(mybase, basename_in);

	// Construct complete path for default properties
	mypath = new char[strlen(directory_in) + 1 + strlen(basename_in) +
	    sizeof (".properties")];
	(void) sprintf(mypath, "%s/%s%s", directory_in, basename_in,
	    ".properties");

	// Make sure that the properties file exists
	if (access(mypath, R_OK) < 0) {
		status = errno;
		delete mydir;
		delete mybase;
		delete mypath;

		return (status);
	}

	// Initialize our variables
	directory = mydir;
	basename = mybase;
	properties2.setPropertiesFile(mypath);
	properties2.setLoadOnReference();
	delete mypath;

	// If the locale is already set, try to construct localized properties
	if (locale) {
		// First try with the full locale string.
		mypath = new char[strlen(directory) + 1 + strlen(basename) +
		    1 + strlen(locale) + 1 + sizeof (".properties")];
		(void) sprintf(mypath, "%s/%s_%s%s", directory, basename,
		    locale, ".properties");

		// If this new file exists, set the current file.
		if (access(mypath, R_OK) == 0) {
			properties1.setPropertiesFile(mypath);
			properties1.setLoadOnReference();
			delete mypath;
			return (0);
		}

		char *tokens[3] = {"@", ".", "_"};

		// Truncate locale at "@", ".", and "_" in turn.
		for (int i = 0; i < 3; i++) {
			(void) sprintf(mypath, "%s/%s_%s%s", directory,
			    basename, strtok(locale, tokens[i]),
			    ".properties");
			if (access(mypath, R_OK) == 0) {
				properties1.setPropertiesFile(mypath);
				properties1.setLoadOnReference();
				break;
			}
		}
		delete mypath;
	}

	return (0);
}

//
// Set the locale to use. See block comment above setFamily() regarding
// localized properties.
//
void
PropertyResourceBundle::setLocale(char *locale_in)
{
	char *mylocale;
	char *mypath;

	delete locale;
	locale = 0;

	if (!locale_in)
		return;

	// Copy locale
	locale = new char[strlen(locale_in) + 1];
	(void) strcpy(locale, locale_in);

	// If we have a base name and directory, try to construct new file
	if (basename && directory) {

		// Copy complete path using the full locale string
		mypath = new char[strlen(directory) + 1 + strlen(basename) +
		    1 + strlen(locale) + 1 + sizeof (".properties")];
		(void) sprintf(mypath, "%s/%s_%s%s", directory, basename,
		    locale, ".properties");

		// If this new file exists, set the current file.
		if (access(mypath, R_OK) == 0) {
			properties1.setPropertiesFile(mypath);
			properties1.setLoadOnReference();
			delete mypath;
			return;
		}

		char *tokens[3] = {"@", ".", "_"};

		// Truncate locale at "@", ".", and "_" in turn.
		for (int i = 0; i < 3; i++) {
			(void) sprintf(mypath, "%s/%s_%s%s", directory,
			    basename, strtok(locale, tokens[i]),
			    ".properties");
			if (access(mypath, R_OK) == 0) {
				properties1.setPropertiesFile(mypath);
				properties1.setLoadOnReference();
				break;
			}
		}
		delete mypath;
	}
}

// Get the current locale setting used by this bundle.
char *
PropertyResourceBundle::getLocale()
{
	return (locale);
}

// Get a string for the given key from this resource bundle.
const char *
PropertyResourceBundle::getString(char *key)
{
	const char *value = (char *)0;

	// If locale is set, try localized properties first
	if (locale)
		value = properties1.getProperty(key);

	// Next, try default properties
	if (!value)
		value = properties2.getProperty(key);

	return (value);
}

void
PropertyResourceBundle::init(void)
{
	directory = 0;
	basename = 0;
	locale = 0;
}
