//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident   "@(#)NameValue.cc	1.3	08/05/20 SMI"

//
// This is the NameValue class for "string" name-value pairs.
//

#include "NameValue.h"

#include <string.h>

// Construct a null name-value pair
NameValue::NameValue()
{
	init();
}

// Construct a name-value pair with a null value
NameValue::NameValue(char *name_in)
{
	init();

	setname(name_in);
}

// Construct a name-value pair
NameValue::NameValue(char *name_in, char *value_in)
{
	init();

	setname(name_in);
	setvalue(value_in);
}

// Destructor
NameValue::~NameValue()
{
	delete name;
	delete value;

	init();
}

// Change the name
void
NameValue::setname(char *name_in)
{
	char *buffer;

	delete name;

	if (!name_in)
		return;

	// Make a copy
	buffer = new char[strlen(name_in) + 1];
	(void) strcpy(buffer, name_in);		//lint !e668

	// Set it to the copy
	name = buffer;
}

// Change the value
void
NameValue::setvalue(char *value_in)
{
	char *buffer;

	delete value;

	if (!value_in)
		return;

	// Make a copy
	buffer = new char[strlen(value_in) + 1];
	(void) strcpy(buffer, value_in);	//lint !e668

	// Set it to the copy
	value = buffer;
}

// Get the name
char *
NameValue::getname()
{
	return (name);
}

// Get the value
char *
NameValue::getvalue()
{
	return (value);
}

// Initialize
void
NameValue::init()
{
	name = (char *)0;
	value = (char *)0;
}
