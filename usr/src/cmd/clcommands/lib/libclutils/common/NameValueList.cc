//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)NameValueList.cc	1.3	08/05/20 SMI"

//
// This is the NameValueList class.
//
// This class manages a collection of "string" name-value pairs.
//

#include "NameValueList.h"

#include <string.h>

// Construct an empty name-value list.
NameValueList::NameValueList(bool isSet)
{
	set = isSet;
}

// Construct an name-value list with it's first pair.
NameValueList::NameValueList(char *name, char *value, bool isSet)
{
	set = isSet;
	add(name, value);
	li = l.begin();
}

//
// Construct a name-value list with it's first pair.  Name-values
// are seperated by "=" in the "string".
//
NameValueList::NameValueList(char *string, bool isSet)
{
	set = isSet;
	add(string);
	li = l.begin();
}

// Construct a name-value list.   If flagged, split the "string".
NameValueList::NameValueList(int splitflag, char *string, bool isSet)
{
	set = isSet;
	add(splitflag, string);
	li = l.begin();
}

// Destructor
NameValueList::~NameValueList()
{
	std::list<NameValue *>::iterator myli;

	for (myli = l.begin();  myli != l.end();  ++myli)
		delete *myli;
}

// Add a name-value pair to the list.
int
NameValueList::add(char *name, char *value)
{
	NameValue *nv;

	if (set && (getValue(name) != NULL)) {
		return (1);
	}

	nv = new NameValue();

	// There must always be a name in order to add it to the list.
	if (name) {
		nv->setname(name);
		nv->setvalue(value);
		l.push_back(nv);

		return (0);
	}

	return (1);
}

// Add a name-value pair to the front of the list.
int
NameValueList::addFirst(char *name, char *value)
{
	NameValue *nv;

	if (set && (getValue(name) != NULL)) {
		return (1);
	}

	nv = new NameValue();

	// There must always be a name in order to add it to the list.
	if (name) {
		nv->setname(name);
		nv->setvalue(value);
		l.push_front(nv);

		return (0);
	}

	return (1);
}

// Add a name-value to the list.  Name-values are seperated by "=".
int
NameValueList::add(char *string)
{
	NameValue nv;
	char *name;
	char *value;

	if (!string)
		return (1);

	// Make copy of the string
	name = new char[strlen(string) + 1];
	(void) strcpy(name, string);

	// Split it
	if ((value = strchr(name, '=')) != (char *)0)
		*value++ = 0;

	// Add it
	int err = add(name, value);

	// Delete the copy
	delete name;

	return (err);
}

// Add a name-value pair to the list.   If flagged, split the "string".
int
NameValueList::add(int splitflag, char *string)
{
	if (!string)
		return (1);

	if (splitflag) {
		return (split(" ,", string));
	} else {
		return (add(string));
	}
}

// Clear the list.
void
NameValueList::clear()
{
	l.clear();
}

// Reset the iterator to the beginning of the list.
void
NameValueList::start()
{
	li = l.begin();
}

// Increment the iterator.
void
NameValueList::next()
{
	if (li != l.end())
		++li;
}

// Return 1 when the iterator is at the end.
int
NameValueList::end()
{
	return (li == l.end() ? 1 : 0);
}

// Get the current name.
char *
NameValueList::getName()
{
	return ((*li)->getname());
}

// Get the current value.
char *
NameValueList::getValue()
{
	return ((*li)->getvalue());
}

// Get the value from the name
char *
NameValueList::getValue(char *name)
{
	std::list<NameValue *>::iterator myli;

	if (!name)
		return ((char *)0);

	for (myli = l.begin();  myli != l.end();  ++myli) {
		if (strcmp(name, (*myli)->getname()) == 0)
			return ((*myli)->getvalue());
	}

	return ((char *)0);
}

// Get the number of elements in the list.
int
NameValueList::getSize()
{
	return (l.size());
}

//
// Put the name/value into one string separated by the separator.
// Caller needs to free the memory allocated to the converted string.
//
char *
NameValueList::getString(char *separator)
{
	char *nv_string = (char *)0;
	char tmp_buf[BUFSIZ];
	int count, length = 0;
	std::list<NameValue *>::iterator myli;

	// Check inputs
	if (!separator || (l.size() == 0)) {
		return (nv_string);
	}

	// Count the length
	if (l.size() > 1) {
		length += (l.size() - 1) * strlen(separator);
	}
	for (myli = l.begin();  myli != l.end();  ++myli) {
		char *this_name = (*myli)->getname();
		char *this_val = (*myli)->getvalue();
		if (!this_name)
			continue;

		// Count the length
		length += strlen(this_name) + 1;
		if (this_val) {
			length +=  strlen(this_val);
		}
	}

	// Convert NameValueList to a string.
	nv_string = new char[length + 1];
	*nv_string = '\0';
	count = 0;
	for (myli = l.begin();  myli != l.end();  ++myli) {
		char *this_name = (*myli)->getname();
		char *this_val = (*myli)->getvalue();

		if (!this_name)
			continue;

		// Add the separator
		if (count) {
			(void) strcat(nv_string, separator);
		}

		// Add the name and value
		*tmp_buf = '\0';
		if (this_val) {
			(void) sprintf(tmp_buf, "%s=%s", this_name, this_val);
		} else {
			(void) sprintf(tmp_buf, "%s=", this_name);
		}

		(void) strcat(nv_string, tmp_buf);
		count++;
	}

	return (nv_string);
}

//
// Split the string, using only the first "token" found in the "string".
// Then, add each sub-string to the list.
//
int
NameValueList::split(char *tokens, char *string)
{
	char *ptoken;
	char token[2];
	char *pstring;
	char *buffer;
	int found = 0;

	if (!string)
		return (1);

	if (!tokens) {
		return (add(string));
	}

	// See if we need to split the string
	for (ptoken = tokens;  *ptoken;  ++ptoken) {
		if (strchr(string, *ptoken)) {
			token[0] = *ptoken;
			token[1] = 0;
			++found;
			break;
		}
	}

	// No tokens found;   add what we have
	if (!found) {
		return (add(string));
	}

	// Make copy of the string
	buffer = new char[strlen(string) + 1];
	(void) strcpy(buffer, string);

	int err = 0;
	// Split it up
	for (pstring = strtok(buffer, token); pstring;
	    pstring = strtok(NULL, token)) {
		int e = add(pstring);
		if (err == 0)
			err = e;
	}

	// Delete the copy
	delete buffer;

	return (err);
}
