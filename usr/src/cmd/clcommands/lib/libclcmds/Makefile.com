#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#ident	"@(#)Makefile.com	1.12	09/04/20 SMI"
#
# Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# cmd/clcommands/lib/libclcmds/Makefile.com
#
#
include $(SRC)/cmd/Makefile.cmd

LIBRARYCCC= libclcmds.a
VERS=.1

OBJECTS_CLACCESS= \
	claccess_allow.o \
	claccess_allow_all.o \
	claccess_deny.o \
	claccess_deny_all.o \
	claccess_list.o \
	claccess_misc.o \
	claccess_set.o \
	claccess_show.o

SRCS_CLACCESS= $(OBJECTS_CLACCESS:%.o=../common/claccess/%.cc)

OBJECTS_CLDEVICE= \
	cldevice_check.o \
	cldevice_clear.o \
	cldevice_combine.o \
	cldevice_common.o \
	cldevice_export.o \
	cldevice_list.o \
	cldevice_monitor.o \
	cldevice_populate.o \
	cldevice_refresh.o \
	cldevice_rename.o \
	cldevice_repair.o \
	cldevice_replicate.o \
	cldevice_set.o \
	cldevice_show.o \
	cldevice_status.o \
	cldevice_unmonitor.o

SRCS_CLDEVICE= $(OBJECTS_CLDEVICE:%.o=../common/cldevice/%.cc)

OBJECTS_CLDEVICEGROUP= \
	cldevicegroup_adddevice.o \
	cldevicegroup_addnode.o \
	cldevicegroup_common.o \
	cldevicegroup_create.o \
	cldevicegroup_delete.o \
	cldevicegroup_enable.o \
	cldevicegroup_export.o \
	cldevicegroup_list.o \
	cldevicegroup_offline.o \
	cldevicegroup_online.o \
	cldevicegroup_removedevice.o \
	cldevicegroup_removenode.o \
	cldevicegroup_set.o \
	cldevicegroup_show.o \
	cldevicegroup_status.o \
	cldevicegroup_sync.o

SRCS_CLDEVICEGROUP= $(OBJECTS_CLDEVICEGROUP:%.o=../common/cldevicegroup/%.cc)

OBJECTS_CLINTERCONNECT= \
	clinterconnect_add.o \
	clinterconnect_disable.o \
	clinterconnect_enable.o \
	clinterconnect_export.o \
	clinterconnect_misc.o \
	clinterconnect_remove.o \
	clinterconnect_show.o \
	clinterconnect_status.o

SRCS_CLINTERCONNECT= $(OBJECTS_CLINTERCONNECT:%.o=../common/clinterconnect/%.cc)

OBJECTS_CLNASDEVICE= \
	clnasdevice_add.o \
	clnasdevice_add_dir.o \
	clnasdevice_export.o \
	clnasdevice_list.o \
	clnasdevice_misc.o \
	clnasdevice_remove.o \
	clnasdevice_remove_dir.o \
	clnasdevice_set.o \
	clnasdevice_show.o

SRCS_CLNASDEVICE= $(OBJECTS_CLNASDEVICE:%.o=../common/clnasdevice/%.cc)

OBJECTS_CLNODE= \
	clnode_add.o \
	clnode_add_farm.o \
	clnode_clear.o \
	clnode_evacuate.o \
	clnode_export.o \
	clnode_list.o \
	clnode_misc.o \
	clnode_monitor_farm.o \
	clnode_remove.o \
	clnode_remove_farm.o \
	clnode_set.o \
	clnode_show.o \
	clnode_show_rev.o \
	clnode_status.o \
	clnode_unmonitor_farm.o 

$(WEAK_MEMBERSHIP)OBJECTS_CLNODE += clnode_resolve_config.o
SRCS_CLNODE= $(OBJECTS_CLNODE:%.o=../common/clnode/%.cc)

OBJECTS_CLQUORUM= \
	clquorum_add.o \
	clquorum_disable.o \
	clquorum_enable.o \
	clquorum_export.o \
	clquorum_list.o \
	clquorum_misc.o \
	clquorum_remove.o \
	clquorum_reset.o \
	clquorum_set.o \
	clquorum_show.o \
	clquorum_status.o \
	clquorum_votecount.o

SRCS_CLQUORUM= $(OBJECTS_CLQUORUM:%.o=../common/clquorum/%.cc)

OBJECTS_CLRESOURCE= \
	clresource_clear.o \
	clresource_create.o \
	clresource_delete.o \
	clresource_disable.o \
	clresource_enable.o \
	clresource_export.o \
	clresource_list.o \
	clresource_list_props.o \
	clresource_monitor.o \
	clresource_set.o \
	clresource_show.o \
	clresource_status.o \
	clresource_unmonitor.o

SRCS_CLRESOURCE= $(OBJECTS_CLRESOURCE:%.o=../common/clresource/%.cc)

OBJECTS_CLRESOURCEGROUP= \
	clrg_add_node.o \
	clrg_create.o \
	clrg_delete.o \
	clrg_evacuate.o \
	clrg_export.o \
	clrg_list.o \
	clrg_manage.o \
	clrg_offline.o \
	clrg_online.o \
	clrg_quiesce.o \
	clrg_remove_node.o \
	clrg_restart.o \
	clrg_resume.o \
	clrg_set.o \
	clrg_show.o \
	clrg_status.o \
	clrg_suspend.o \
	clrg_switch.o \
	clrg_unmanage.o \
	clrg_remaster.o

SRCS_CLRESOURCEGROUP= $(OBJECTS_CLRESOURCEGROUP:%.o=../common/clresourcegroup/%.cc)

OBJECTS_CLRESOURCETYPE= \
	clrt_add_node.o \
	clrt_export.o \
	clrt_list.o \
	clrt_list_props.o \
	clrt_register.o \
	clrt_remove_node.o \
	clrt_set.o \
	clrt_show.o \
	clrt_unregister.o

SRCS_CLRESOURCETYPE= $(OBJECTS_CLRESOURCETYPE:%.o=../common/clresourcetype/%.cc)

OBJECTS_CLSNMPHOST= \
	clsnmphost_add.o \
	clsnmphost_export.o \
	clsnmphost_list.o \
	clsnmphost_remove.o \
	clsnmphost_show.o

SRCS_CLSNMPHOST= $(OBJECTS_CLSNMPHOST:%.o=../common/clsnmphost/%.cc)

OBJECTS_CLSNMPMIB= \
	clsnmp_common.o \
	clsnmpmib_disable.o \
	clsnmpmib_enable.o \
	clsnmpmib_export.o \
	clsnmpmib_list.o \
	clsnmpmib_set.o \
	clsnmpmib_show.o

SRCS_CLSNMPMIB=	 $(OBJECTS_CLSNMPMIB:%.o=../common/clsnmpmib/%.cc)

OBJECTS_CLSNMPUSER= \
	clsnmpuser_create.o \
	clsnmpuser_delete.o \
	clsnmpuser_export.o \
	clsnmpuser_list.o \
	clsnmpuser_set.o \
	clsnmpuser_set_default.o \
	clsnmpuser_show.o

SRCS_CLSNMPUSER= $(OBJECTS_CLSNMPUSER:%.o=../common/clsnmpuser/%.cc)

OBJECTS_CLUSTER= \
	cluster_create.o \
	cluster_check.o \
	cluster_export.o \
	cluster_list.o \
	cluster_list_cmds.o \
	cluster_misc.o \
	cluster_rename.o \
	cluster_restore_netprops.o \
	cluster_set.o \
	cluster_set_netprops.o \
	cluster_show.o \
	cluster_show_netprops.o \
	cluster_shutdown.o \
	cluster_status.o

SRCS_CLUSTER= $(OBJECTS_CLUSTER:%.o=../common/cluster/%.cc)

OBJECTS_CLTA= \
	clta_disable.o \
	clta_enable.o \
	clta_export.o \
	clta_list.o \
	clta_misc.o \
	clta_print.o \
	clta_set_threshold.o \
	clta_show.o \
	clta_status.o

SRCS_CLTA= $(OBJECTS_CLTA:%.o=../common/cltelemetryattribute/%.cc)

OBJECTS_CLZONE= \
	clzonecluster_boot.o \
	clzonecluster_configure.o \
	clzonecluster_halt.o \
	clzonecluster_install.o \
	clzonecluster_list.o \
	clzonecluster_reboot.o \
	clzonecluster_show.o \
	clzonecluster_status.o \
	clzonecluster_common.o \
	clzonecluster_uninstall.o \
        clzonecluster_verify.o \
        clzonecluster_ready.o \
        clzonecluster_move.o \
        clzonecluster_export.o \
        clzonecluster_clone.o \
        clzonecluster_delete.o

SRCS_CLZONE= $(OBJECTS_CLZONE:%.o=../common/clzonecluster/%.cc)

OBJECTS_MISC= \
	ClCommand.o \
	ClList.o \
	ClShow.o \
	ClStatus.o \
	Row.o \
	clcommand_command.o \
	clcommand_help.o \
	clcommand_file.o \
	common.o

SRCS_MISC= $(OBJECTS_MISC:%.o=../common/misc/%.cc)

OBJECTS_GETOPT_CLIP= \
	getopt_long.o

SRCS_GETOPT_CLIP= $(OBJECTS_GETOPT_CLIP:%.o=../common/getopt_clip/%.c)

OBJECTS_RGM_SUPPORT= \
	rgm_support.o

SRCS_RGM_SUPPORT= $(OBJECTS_RGM_SUPPORT:%.o=../common/rgm_support/%.cc)

OBJECTS_XML= \
	ClXml.o \
	ClcmdExport.o \
	CldeviceExport.o \
	CldevicegroupExport.o \
	ClinterconnectExport.o \
	ClnasExport.o \
	ClnodeExport.o \
	ClquorumExport.o \
	ClresourceExport.o \
	ClresourcegroupExport.o \
	ClresourcetypeExport.o \
	ClsnmphostExport.o \
	ClsnmpmibExport.o \
	ClsnmpuserExport.o \
	CltelemetryattributeExport.o \
	ClusterExport.o \
	OptionValues.o \
	cldevice_xml.o \
	cldevicegroup_xml.o \
	clinterconnect_xml.o \
	clnas_xml.o \
	clnode_xml.o \
	clquorum_xml.o \
	clresource_xml.o \
	clresourcegroup_xml.o \
	clresourcetype_xml.o \
	clsnmphost_xml.o \
	clsnmpuser_xml.o \
	cltelemetryattribute_xml.o \
	cluster_xml.o

SRCS_XML= $(OBJECTS_XML:%.o=../common/xml/%.cc)

OBJECTS= \
	$(OBJECTS_CLACCESS) \
	$(OBJECTS_CLDEVICE) \
	$(OBJECTS_CLDEVICEGROUP) \
	$(OBJECTS_CLINTERCONNECT) \
	$(OBJECTS_CLNASDEVICE) \
	$(OBJECTS_CLNODE) \
	$(OBJECTS_CLQUORUM) \
	$(OBJECTS_CLRESOURCE) \
	$(OBJECTS_CLRESOURCEGROUP) \
	$(OBJECTS_CLRESOURCETYPE) \
	$(OBJECTS_CLSNMPHOST) \
	$(OBJECTS_CLSNMPMIB) \
	$(OBJECTS_CLSNMPUSER) \
	$(OBJECTS_CLTA) \
	$(OBJECTS_CLUSTER) \
	$(OBJECTS_MISC) \
	$(OBJECTS_GETOPT_CLIP) \
	$(OBJECTS_RGM_SUPPORT) \
	$(OBJECTS_XML)

$(POST_S9_BUILD)OBJECTS += $(OBJECTS_CLZONE)

include $(SRC)/lib/Makefile.lib

CHECKHDRS= \
	../common/rgm_support/rgm_support_internal.h \
	../common/cldevicegroup/cldevicegroup_private.h \
	../common/clzonecluster/clzonecluster_private.h

MAPFILE=	../common/mapfile-vers
$(POST_S9_BUILD)MAPFILE=	../common/mapfile-vers.s10
$(WEAK_MEMBERSHIP)MAPFILE=	../common/mapfile-vers.s11

SRCS= \
	$(SRCS_CLACCESS) \
	$(SRCS_CLDEVICE) \
	$(SRCS_CLDEVICEGROUP) \
	$(SRCS_CLINTERCONNECT) \
	$(SRCS_CLNASDEVICE) \
	$(SRCS_CLNODE) \
	$(SRCS_CLQUORUM) \
	$(SRCS_CLRESOURCE) \
	$(SRCS_CLRESOURCEGROUP) \
	$(SRCS_CLRESOURCETYPE) \
	$(SRCS_CLSNMPHOST) \
	$(SRCS_CLSNMPMIB) \
	$(SRCS_CLSNMPUSER) \
	$(SRCS_CLUSTER) \
	$(SRCS_CLTA) \
	$(SRCS_MISC) \
	$(SRCS_GETOPT_CLIP) \
	$(SRCS_RGM_SUPPORT) \
	$(SRCS_XML)
	
$(POST_S9_BUILD)SRCS += $(SRCS_CLZONE)

LIBS=		$(DYNLIBCCC)

MTFLAG=		-mt

# Standard header files for clcommands
CPPFLAGS +=	-I$(SRC)/cmd/clcommands/include
CPPFLAGS +=	-I$(REF_PROTO)/usr/include/libxml2

# Header files included for clcommands due to poor declarations
# XXX - this can be fixed by using proper declarations.
CPPFLAGS +=	-I$(SRC)/cmd/clcommands/include/xml
CPPFLAGS +=	-I$(ROOTCLUSTINC)/scadmin
CPPFLAGS +=	-I$(ROOTCLUSTINC)/scadmin/rpc
CPPFLAGS +=	-I$(ROOTCLUSTINC)/cl_query
$(WEAK_MEMBERSHIP)CPPFLAGS +=	-I$(ROOTCLUSTINC)/rgm

# Header files included for clcommands due to dependencies on private headers
CPPFLAGS +=	-I$(SRC)/cmd/did
CPPFLAGS +=	-I$(SRC)/lib/libdid/include
CPPFLAGS +=	-I$(SRC)/lib/libclcomm/common
CPPFLAGS +=	-I$(SRC)/lib/libscnas
CPPFLAGS +=	-I$(SRC)/lib/libscconf
CPPFLAGS +=	-I$(SRC)/lib/libscdpm
CPPFLAGS +=	-I$(SRC)/lib/libscdpm/include
CPPFLAGS +=	-I$(SRC)/lib/libscslm/common
CPPFLAGS +=	-I$(SRC)/common/cl
CPPFLAGS +=	-I$(SRC)/common/cl/dc/libdcs
CPPFLAGS += 	-I$(SRC)/common/cl/interfaces/$(CLASS)
CPPFLAGS += 	-I$(SRC)/cmd/rtreg/
$(WEAK_MEMBERSHIP)CPPFLAGS +=     -I$(SRC)/lib/libqlccr/common
$(WEAK_MEMBERSHIP)CPPFLAGS +=     -I$(SRC)/lib/libqllog/common
$(WEAK_MEMBERSHIP)CPPFLAGS += 	 -I$(SRC)/common/cl/ccr
$(WEAK_MEMBERSHIP)CPPFLAGS +=	-DWEAK_MEMBERSHIP

LIBCCRIO = $(SRC)/lib/libccrio/$(MACH)/libccrio.a

PMAP =	-M $(MAPFILE)

LINTFLAGS +=
LINTFILES +=	$(OBJECTS:%.o=%.ln)

$(S9_BUILD)LDLIBS += $(SPRO_LIBS)/libCstd.a $(SPRO_LIBS)/libCrun.a
LDLIBS +=	-lc -lxml2 -lclos -lclevent -lscconf \
		-lclconf -lscnas -lclccrad -lscrgadm \
		-lscswitch -lxnet -lpnm -lrgm -lclcomm \
		-lscstat -lgen -lrt -lnsl -ldl -ldid -lscha \
		-lscdpm -lclutils -lsecdb -lsczones -lscslm -lclevent \
		-lmd5
$(POST_S9_BUILD)LDLIBS += -lscprivip -lzccfg
$(WEAK_MEMBERSHIP)LDLIBS += -lqlccr $(LIBCCRIO) -lresolv -lhaip -ldsdev

$(PICS) := CCFLAGS += -KPIC

#
# Check targets
#
%.c_check: ../common/claccess/%.cc
	$(CSTYLE) $< > $@

%.c_check: ../common/cldevice/%.cc
	$(CSTYLE) $< > $@

%.c_check: ../common/cldevicegroup/%.cc
	$(CSTYLE) $< > $@

%.c_check: ../common/clinterconnect/%.cc
	$(CSTYLE) $< > $@

%.c_check: ../common/clnasdevice/%.cc
	$(CSTYLE) $< > $@

%.c_check: ../common/clnode/%.cc
	$(CSTYLE) $< > $@

%.c_check: ../common/clquorum/%.cc
	$(CSTYLE) $< > $@

%.c_check: ../common/clresource/%.cc
	$(CSTYLE) $< > $@

%.c_check: ../common/clresourcegroup/%.cc
	$(CSTYLE) $< > $@

%.c_check: ../common/clresourcetype/%.cc
	$(CSTYLE) $< > $@

%.c_check: ../common/clsnmphost/%.cc
	$(CSTYLE) $< > $@

%.c_check: ../common/clsnmpmib/%.cc
	$(CSTYLE) $< > $@

%.c_check: ../common/clsnmpuser/%.cc
	$(CSTYLE) $< > $@

%.c_check: ../common/cltelemetryattribute/%.cc
	$(CSTYLE) $< > $@

%.c_check: ../common/cluster/%.cc
	$(CSTYLE) $< > $@

$(POST_S9_BUILD) %.c_check: ../common/clzonecluster/%.cc
	$(CSTYLE) $< > $@

%.c_check: ../common/misc/%.cc
	$(CSTYLE) $< > $@

%.c_check: ../common/xml/%.cc
	$(CSTYLE) $< > $@

%.c_check: ../common/rgm_support/%.cc
	$(CSTYLE) $< > $@

%.c_check: ../common/getopt_clip/%.c
	touch $@


#
# Lint targets
#
%.ln: ../common/claccess/%.cc
	$(LINT.cc) $< > $@
	$(CAT) $@

%.ln: ../common/cldevice/%.cc
	$(LINT.cc) $< > $@
	$(CAT) $@

%.ln: ../common/cldevicegroup/%.cc
	$(LINT.cc) $< > $@
	$(CAT) $@

%.ln: ../common/clinterconnect/%.cc
	$(LINT.cc) $< > $@
	$(CAT) $@

%.ln: ../common/clnasdevice/%.cc
	$(LINT.cc) $< > $@
	$(CAT) $@

%.ln: ../common/clnode/%.cc
	$(LINT.cc) $< > $@
	$(CAT) $@

%.ln: ../common/clquorum/%.cc
	$(LINT.cc) $< > $@
	$(CAT) $@

%.ln: ../common/clresource/%.cc
	$(LINT.cc) $< > $@
	$(CAT) $@

%.ln: ../common/clresourcegroup/%.cc
	$(LINT.cc) $< > $@
	$(CAT) $@

%.ln: ../common/clresourcetype/%.cc
	$(LINT.cc) $< > $@
	$(CAT) $@

%.ln: ../common/clsnmphost/%.cc
	$(LINT.cc) $< > $@
	$(CAT) $@

%.ln: ../common/clsnmpmib/%.cc
	$(LINT.cc) $< > $@
	$(CAT) $@

%.ln: ../common/clsnmpuser/%.cc
	$(LINT.cc) $< > $@
	$(CAT) $@

%.ln: ../common/cltelemetryattribute/%.cc
	$(LINT.cc) $< > $@
	$(CAT) $@

%.ln: ../common/cluster/%.cc
	$(LINT.cc) $< > $@
	$(CAT) $@

$(POST_S9_BUILD) %.ln: ../common/clzonecluster/%.cc
	$(LINT.cc) $< > $@
	$(CAT) $@

%.ln: ../common/misc/%.cc
	$(LINT.cc) $< > $@
	$(CAT) $@

%.ln: ../common/xml/%.cc
	$(LINT.cc) $< > $@
	$(CAT) $@

%.ln: ../common/rgm_support/%.cc
	$(LINT.cc) $< > $@
	$(CAT) $@

%.ln: ../common/getopt_clip/%.c


#
# PI file targets
#
%.pi: ../common/claccess/%.cc
	$(CCOMPILE.CPP) $< > $@

%.pi: ../common/cldevice/%.cc
	$(CCOMPILE.CPP) $< > $@

%.pi: ../common/cldevicegroup/%.cc
	$(CCOMPILE.CPP) $< > $@

%.pi: ../common/clinterconnect/%.cc
	$(CCOMPILE.CPP) $< > $@

%.pi: ../common/clnasdevice/%.cc
	$(CCOMPILE.CPP) $< > $@

%.pi: ../common/clnode/%.cc
	$(CCOMPILE.CPP) $< > $@

%.pi: ../common/clquorum/%.cc
	$(CCOMPILE.CPP) $< > $@

%.pi: ../common/clresource/%.cc
	$(CCOMPILE.CPP) $< > $@

%.pi: ../common/clresourcegroup/%.cc
	$(CCOMPILE.CPP) $< > $@

%.pi: ../common/clresourcetype/%.cc
	$(CCOMPILE.CPP) $< > $@

%.pi: ../common/clsnmphost/%.cc
	$(CCOMPILE.CPP) $< > $@

%.pi: ../common/clsnmpmib/%.cc
	$(CCOMPILE.CPP) $< > $@

%.pi: ../common/clsnmpuser/%.cc
	$(CCOMPILE.CPP) $< > $@

%.pi: ../common/cltelemetryattribute/%.cc
	$(CCOMPILE.CPP) $< > $@

%.pi: ../common/cluster/%.cc
	$(CCOMPILE.CPP) $< > $@

$(POST_S9_BUILD) %.pi: ../common/clzonecluster/%.cc
	$(CCOMPILE.CPP) $< > $@

%.pi: ../common/misc/%.cc
	$(CCOMPILE.CPP) $< > $@

%.pi: ../common/xml/%.cc
	$(CCOMPILE.CPP) $< > $@

%.pi: ../common/rgm_support/%.cc
	$(CCOMPILE.CPP) $< > $@

%.pi: ../common/getopt_clip/%.c
	$(COMPILE.CPP) $< > $@


.KEEP_STATE:

objs/%.o pics/%.o: ../common/claccess/%.cc
	$(COMPILE.cc) -o $@ $<

objs/%.o pics/%.o: ../common/cldevice/%.cc
	$(COMPILE.cc) -o $@ $<

objs/%.o pics/%.o: ../common/cldevicegroup/%.cc
	$(COMPILE.cc) -o $@ $<

objs/%.o pics/%.o: ../common/clinterconnect/%.cc
	$(COMPILE.cc) -o $@ $<

objs/%.o pics/%.o: ../common/clnasdevice/%.cc
	$(COMPILE.cc) -o $@ $<

objs/%.o pics/%.o: ../common/clnode/%.cc
	$(COMPILE.cc) -o $@ $<

objs/%.o pics/%.o: ../common/clquorum/%.cc
	$(COMPILE.cc) -o $@ $<

objs/%.o pics/%.o: ../common/clresource/%.cc
	$(COMPILE.cc) -o $@ $<

objs/%.o pics/%.o: ../common/clresourcegroup/%.cc
	$(COMPILE.cc) -o $@ $<

objs/%.o pics/%.o: ../common/clresourcetype/%.cc
	$(COMPILE.cc) -o $@ $<

objs/%.o pics/%.o: ../common/clsnmphost/%.cc
	$(COMPILE.cc) -o $@ $<

objs/%.o pics/%.o: ../common/clsnmpmib/%.cc
	$(COMPILE.cc) -o $@ $<

objs/%.o pics/%.o: ../common/clsnmpuser/%.cc
	$(COMPILE.cc) -o $@ $<

objs/%.o pics/%.o: ../common/cluster/%.cc
	$(COMPILE.cc) -o $@ $<

objs/%.o pics/%.o: ../common/cltelemetryattribute/%.cc
	$(COMPILE.cc) -o $@ $<

$(POST_S9_BUILD) objs/%.o pics/%.o: ../common/clzonecluster/%.cc
	$(COMPILE.cc) -o $@ $<

objs/%.o pics/%.o: ../common/misc/%.cc
	$(COMPILE.cc) -o $@ $<

objs/%.o pics/%.o: ../common/getopt_clip/%.c
	$(COMPILE.c) -o $@ $<

objs/%.o pics/%.o: ../common/rgm_support/%.cc
	$(COMPILE.cc) -o $@ $<

objs/%.o pics/%.o: ../common/xml/%.cc
	$(COMPILE.cc) -o $@ $<

include $(SRC)/lib/Makefile.targ
include $(SRC)/cmd/Makefile.cmd.clprops
include $(SRC)/cmd/Makefile.targ.clprops
