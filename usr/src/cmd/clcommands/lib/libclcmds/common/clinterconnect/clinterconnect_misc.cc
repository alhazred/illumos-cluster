//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clinterconnect_misc.cc	1.7	08/05/20 SMI"

#include "clinterconnect.h"
#include "clcomm_cluster_vm.h"

//
// Print a clintr error from the CL error number.
//
void
clintr_perror(clerrno_t clerrno)
{
	switch (clerrno) {
	case CL_EEXIST:
	case CL_ENOENT:
	case CL_EOP:
	case CL_EBUSY:
		return;

	case CL_EACCESS:
	case CL_EINVAL:
	case CL_ENOMEM:
	case CL_EINTERNAL:
	default:
		clcommand_perror(clerrno);
		return;
	}
}

//
// Convert scconf error to clinterconnect error.
//
clerrno_t
clintr_conv_scconf_err(scconf_errno_t scconf_err)
{
	// Map the error
	switch (scconf_err) {
	case SCCONF_NOERR:
		return (CL_NOERR);

	case SCCONF_EPERM:
		return (CL_EACCESS);

	case SCCONF_EINUSE:
	case SCCONF_EBUSY:
		return (CL_EBUSY);

	case SCCONF_EEXIST:
		return (CL_EEXIST);

	case SCCONF_EUNKNOWN:
	case SCCONF_ENOEXIST:
		return (CL_ENOENT);

	case SCCONF_EINVAL:
	case SCCONF_EUSAGE:
	case SCCONF_ERANGE:
		return (CL_EINVAL);

	case SCCONF_ENOMEM:
	case SCCONF_EOVERFLOW:
		return (CL_ENOMEM);

	case SCCONF_ENOCLUSTER:
		return (CL_ENOTCLMODE);

	case SCCONF_ESETUP:
	case SCCONF_EUNEXPECTED:
	default:
		return (CL_EINTERNAL);
	}
}

//
// Convert scstat error to clinterconnect error.
//
clerrno_t
clintr_conv_scstat_err(scstat_errno_t scstat_err)
{
	// Map the error
	switch (scstat_err) {
	case SCSTAT_ENOERR:
		return (CL_NOERR);

	case SCSTAT_EPERM:
		return (CL_EACCESS);

	case SCSTAT_EUSAGE:
		return (CL_EINVAL);

	case SCSTAT_EINVAL:
		return (CL_EINVAL);

	case SCSTAT_ENOMEM:
		return (CL_ENOMEM);

	case SCSTAT_EUNEXPECTED:
	default:
		return (CL_EINTERNAL);
	}
}

int
initialize_endpoint(scconf_cltr_epoint_t *epoint, char *endpoint)
{
	char *node;
	char *name;
	char *port;

	if (epoint == NULL) {
		return (1);
	}

	bzero(epoint, sizeof (scconf_cltr_epoint_t));

	// Isolate the node name from the device name
	name = strchr(endpoint, ':');
	if (name != NULL) {
		node = endpoint;
		*name++ = '\0';
		if (*name == '\0' || strchr(name, ':')) {
			return (1);
		}
	} else {
		node = NULL;
		name = endpoint;
	}

	// Isolate the device name from the port
	port = strchr(name, '@');
	if (port) {
		*port++ = '\0';
		if (*port == '\0' || strchr(port, '@')) {
			return (1);
		}
	}

	epoint->scconf_cltr_epoint_type = (node == NULL) ?
	    SCCONF_CLTR_EPOINT_TYPE_CPOINT :
	    SCCONF_CLTR_EPOINT_TYPE_ADAPTER;
	epoint->scconf_cltr_epoint_nodename = node;
	epoint->scconf_cltr_epoint_devicename = name;
	epoint->scconf_cltr_epoint_portname = port;

	return (0);
}

//
// Get cable endpoint. Append portname if noport is zero.
//
void
get_cable_epoint(scconf_cltr_epoint_t *epoint, char *buffer, int noport)
{
	// initialize the buffer
	*buffer = '\0';

	// if adapter, pre-pend the hostname
	if (epoint->scconf_cltr_epoint_type ==
	    SCCONF_CLTR_EPOINT_TYPE_ADAPTER &&
	    epoint->scconf_cltr_epoint_nodename != NULL) {
		(void) sprintf(buffer, "%s:",
		    epoint->scconf_cltr_epoint_nodename);
	}

	// add the device name
	if (epoint->scconf_cltr_epoint_devicename == NULL) {
		return;
	}

	(void) strcat(buffer, epoint->scconf_cltr_epoint_devicename);

	// if switch and port requested, append the portname
	if (!noport && epoint->scconf_cltr_epoint_type ==
	    SCCONF_CLTR_EPOINT_TYPE_CPOINT &&
	    epoint->scconf_cltr_epoint_portname != NULL) {
		(void) strcat(buffer, "@");
		(void) strcat(buffer, epoint->scconf_cltr_epoint_portname);
	}
}

//
// Get endpoint portname
//
void
get_ep_port(scconf_cltr_epoint_t *epoint, char *port)
{
	// initialize the buffer
	*port = '\0';

	// if switch, append the portname
	if (epoint->scconf_cltr_epoint_type ==
	    SCCONF_CLTR_EPOINT_TYPE_CPOINT &&
		epoint->scconf_cltr_epoint_portname != NULL) {
		(void) strcat(port, epoint->scconf_cltr_epoint_portname);
	}
}

//
// Get adapter endpoint nodename
//
void
get_ep_node(scconf_cltr_epoint_t *epoint, char *node)
{
	// initialize the buffer
	*node = '\0';

	// if adapter, pre-pend the hostname
	if (epoint->scconf_cltr_epoint_type ==
	    SCCONF_CLTR_EPOINT_TYPE_ADAPTER &&
	    epoint->scconf_cltr_epoint_nodename != NULL) {
		strcpy(node, epoint->scconf_cltr_epoint_nodename);
	}
}

//
// Get endpoint device name
//
void
get_ep_name(scconf_cltr_epoint_t *epoint, char *name)
{
	// initialize the buffer
	*name = '\0';

	// add the device name
	if (epoint->scconf_cltr_epoint_devicename == NULL) {
		return;
	}

	strcpy(name, epoint->scconf_cltr_epoint_devicename);
}

//
// Get endpoint type
//
void
get_ep_type(scconf_cltr_epoint_t *epoint, cable_t *type)
{
	if (epoint->scconf_cltr_epoint_type ==
	    SCCONF_CLTR_EPOINT_TYPE_ADAPTER) {
		*type = ADAPTER;
	} else {
		*type = SWITCH;
	}
}

//
// Get all the cluster endpoints.
//
clerrno_t
clintr_get_cluster_endpoints(ValueList &endpoints)
{
	scconf_errno_t scconferr = SCCONF_NOERR;
	clerrno_t clerr = CL_NOERR;
	scconf_cfg_cluster_t *clconfig = NULL;
	scconf_cfg_cable_t *cltr_cables;
	scconf_cfg_cable_t *cltr_cable;
	scconf_cfg_node_t *cluster_nodes;
	scconf_cfg_node_t *cluster_node;
	scconf_cfg_cltr_adap_t *cltr_adapters;
	scconf_cfg_cltr_adap_t *cltr_adapter;
	scconf_cfg_cpoint_t *cltr_switches;
	scconf_cfg_cpoint_t *cltr_switch;
	char errbuf[SCCONF_MAXSTRINGLEN];
	char ep1[SCCONF_MAXSTRINGLEN];
	char ep2[SCCONF_MAXSTRINGLEN];
	char cablename[SCCONF_MAXSTRINGLEN];
	char adaptername[SCCONF_MAXSTRINGLEN];

	// Get cluster configuration
	scconferr = scconf_get_clusterconfig(&clconfig);
	if (scconferr != SCCONF_NOERR) {
		clerror("Failed to get the cluster configuration.\n");
		clerr = clintr_conv_scconf_err(scconferr);
		return (clerr);
	}

	// Set the cable list
	cltr_cables = clconfig->scconf_cluster_cablelist;

	// Add the cables to the list
	for (cltr_cable = cltr_cables; cltr_cable;
	    cltr_cable = cltr_cable->scconf_cable_next) {

		// Get endpoints
		get_cable_epoint(&cltr_cable->scconf_cable_epoint1, ep1, 0);
		get_cable_epoint(&cltr_cable->scconf_cable_epoint2, ep2, 0);

		// Build comma seperated pair of (cable) endpoints
		sprintf(cablename, "%s,%s", ep1, ep2);
		endpoints.add(cablename);
	}

	// Set the cluster nodelist
	cluster_nodes = clconfig->scconf_cluster_nodelist;
	if (cluster_nodes) {
		for (cluster_node = cluster_nodes; cluster_node;
		    cluster_node = cluster_node->scconf_node_next) {
			// Set the adapters list
			cltr_adapters =
			    cluster_node->scconf_node_adapterlist;

			// Add the adapters to the list
			for (cltr_adapter = cltr_adapters; cltr_adapter;
			    cltr_adapter = cltr_adapter->scconf_adap_next) {
				sprintf(adaptername, "%s:%s",
				    cluster_node->scconf_node_nodename,
				    cltr_adapter->scconf_adap_adaptername);
				endpoints.add(adaptername);
			}
		}
	}

	// Set the switches list
	cltr_switches = clconfig->scconf_cluster_cpointlist;

	// Add the switches to the list
	for (cltr_switch = cltr_switches; cltr_switch;
	    cltr_switch = cltr_switch->scconf_cpoint_next) {
		endpoints.add(cltr_switch->scconf_cpoint_cpointname);
	}

	return (clerr);
}

//
// clintr_verify_nodes
//
// Check if the nodes in the list are valid cluster nodes.
//
// Return Values :
//	CL_NOERR	- All nodes in the list are part of the cluster.
//	CL_ENOENT	- One or more nodes not part of the cluster.
//
clerrno_t
clintr_verify_nodes(ValueList &nodes)
{
	scconf_errno_t scconferr = SCCONF_NOERR;
	clerrno_t clerr = CL_NOERR;
	char *cur_node = NULL;
	unsigned int cur_node_id;
	int nomatch = 0;

	for (nodes.start(); !nodes.end(); nodes.next()) {
		cur_node = nodes.getValue();
		if (strlen(cur_node) == 0) {
			clerror("Invalid node specified.\n");
			nomatch++;
			continue;
		}
		scconferr = scconf_get_nodeid(cur_node, &cur_node_id);
		if (scconferr == SCCONF_ENOEXIST) {
			clerror("\"%s\" is not a valid cluster node.\n",
			    cur_node);
			nomatch++;
		} else {
			clerr = clintr_conv_scconf_err(scconferr);
		}
	}

	if (nomatch) {
		clerr = CL_ENOENT;
	}

	return (clerr);
}

//
// clintr_verify_operands
//
// Check if the operands in the list are valid cluster endpoints.
//
// Return Values :
//	CL_NOERR	- All operands in the list are valid cluster endpoints
//	CL_ENOENT	- One or more endpoints does not exist in the cluster
//
clerrno_t
clintr_verify_operands(ValueList &operands)
{
	scconf_errno_t scconferr = SCCONF_NOERR;
	clerrno_t clerr = CL_NOERR;
	char *cur_operand = NULL;
	int nomatch = 0;
	ValueList endpoints = ValueList(true);

	// Get all the cluster endpoints
	clerr = clintr_get_cluster_endpoints(endpoints);
	if (clerr != CL_NOERR) {
		return (clerr);
	}

	// Check if the specified endpoint(s) is a valid cluster endpoint
	for (operands.start(); !operands.end(); operands.next()) {
		cur_operand = operands.getValue();
		if (strcmp(cur_operand, CLCOMMANDS_WILD_OPERAND) == 0) {
			break;
		}

		// For switch ep, ignore @port if switch@port specified
		if (strchr(cur_operand, ',') == NULL &&
		    strchr(cur_operand, ':') == NULL &&
		    strchr(cur_operand, '@') != NULL) {
			strtok(cur_operand, "@");
		}

		if (!endpoints.isValue(cur_operand)) {
			clerror("Endpoint \"%s\" does not exist.\n",
			    cur_operand);
			nomatch++;
		}
	}

	if (nomatch) {
		clerr = CL_ENOENT;
	}

	return (clerr);
}

//
// clintr_is_allports_disabled
//
// Check if all of the switch ports are in disabled state
//
// Return Values :
//	CL_NOERR	- No Error
//
clerrno_t
clintr_is_allports_disabled(const char *switchname, bool *is_alldisabled)
{
	scconf_errno_t scconferr = SCCONF_NOERR;
	clerrno_t clerr = CL_NOERR;
	scconf_cfg_cluster_t *clconfig = NULL;
	scconf_cfg_cpoint_t *cltr_switches = NULL;
	scconf_cfg_cpoint_t *cltr_switch = NULL;
	scconf_cfg_port_t *cltr_switch_ports = NULL;
	scconf_cfg_port_t *cltr_switch_port = NULL;
	char switch_name[SCCONF_MAXSTRINGLEN] = "";

	// Check inputs
	if (switchname == NULL) {
		return (CL_EINTERNAL);
	}

	// Get cluster configuration
	scconferr = scconf_get_clusterconfig(&clconfig);
	if (scconferr != SCCONF_NOERR) {
		clerror("Failed to get the cluster configuration.\n");
		clerr = clintr_conv_scconf_err(scconferr);
		return (clerr);
	}

	// Set the switches list
	cltr_switches = clconfig->scconf_cluster_cpointlist;
	if (cltr_switches == NULL) {
		return (clerr);
	}

	// If switch@port, then strip off @port
	strcpy(switch_name, switchname);
	strtok(switch_name, "@");

	// Add the switches to the list
	for (cltr_switch = cltr_switches; cltr_switch;
	    cltr_switch = cltr_switch->scconf_cpoint_next) {
		if (strcmp(switch_name,
		    cltr_switch->scconf_cpoint_cpointname) == 0) {
			break;
		}
	}

	// Set the switch ports list
	cltr_switch_ports = cltr_switch->scconf_cpoint_portlist;

	for (cltr_switch_port = cltr_switch_ports; cltr_switch_port;
	    cltr_switch_port = cltr_switch_port->scconf_port_next) {
		if (cltr_switch_port->scconf_port_portstate ==
		    SCCONF_STATE_ENABLED) {
			*is_alldisabled = false;
			break;
		}
	}

	return (clerr);
}
