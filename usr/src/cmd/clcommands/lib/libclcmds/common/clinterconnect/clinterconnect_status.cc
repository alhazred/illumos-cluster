//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clinterconnect_status.cc	1.3	08/05/20 SMI"

//
// Process clinterconnect "status"
//

#include "clinterconnect.h"
#include "ClStatus.h"
#include "scadmin/scstat.h"

static bool
is_node_in_endpoint(char *node, char *endpoint);

static bool
is_node_match(ValueList &nodes, char *ep1, char *ep2);

static bool
is_operand_match(ValueList &operands, char *ep1, char *ep2);

static clerrno_t
get_path_endpoints(char *pathname, char *ep1, char *ep2);

static clerrno_t
get_filtered_trpaths_status(ValueList &operands, ValueList &nodes,
    ClStatus *trpaths_status);

clerrno_t
clinterconnect_status(ClCommand &cmd, ValueList &operands, ValueList &nodes)
{
	clerrno_t clerr = CL_NOERR;
	clerrno_t first_err = CL_NOERR;
	ClStatus *trpaths_status = new ClStatus(cmd.isVerbose ? true : false);

	// Check for valid nodes
	clerr = clintr_verify_nodes(nodes);
	if (clerr != CL_NOERR) {
		if (first_err == CL_NOERR) {
			first_err = clerr;
		}
		if (clerr != CL_ENOENT) {
			goto cleanup;
		} else {
			if (nodes.getSize() == 1) {
				goto cleanup;
			}
		}
	}

	// Check for valid operands
	clerr = clintr_verify_operands(operands);
	if (clerr != CL_NOERR) {
		if (first_err == CL_NOERR) {
			first_err = clerr;
		}
		if (clerr != CL_ENOENT) {
			goto cleanup;
		} else {
			if (operands.getSize() == 1) {
				goto cleanup;
			}
		}
	}

	clerr = get_filtered_trpaths_status(operands, nodes, trpaths_status);
	if (clerr != CL_NOERR) {
		if (first_err == CL_NOERR) {
			first_err = clerr;
		}
		goto cleanup;
	}

	// Print the transports paths status
	trpaths_status->setHeadingTitle(HEAD_TRANSPORT);
	trpaths_status->setHeadings((char *)gettext("Endpoint1"),
	    (char *)gettext("Endpoint2"), (char *)gettext("Status"));
	trpaths_status->print();

cleanup:

	// Delete the transport paths object
	delete trpaths_status;

	// Return first error
	if (first_err != CL_NOERR) {
		clerr = first_err;
	}

	return (clerr);
}

//
// get_clintr_status_obj
//
//	Get the interconnect status. The ClStatus object must be created
//	before calling, and caller needs to free it.
//
clerrno_t
get_clintr_status_obj(optflgs_t optflgs, ClStatus *clintr_cstatus)
{
	ValueList empty_list;
	clerrno_t clerrno = CL_NOERR;

	clerrno = get_filtered_trpaths_status(empty_list, empty_list,
	    clintr_cstatus);
	if ((clerrno == CL_NOERR) && clintr_cstatus) {
		// Set title
		clintr_cstatus->setHeadingTitle(HEAD_TRANSPORT);
		clintr_cstatus->setHeadings((char *)gettext("Endpoint1"),
		    (char *)gettext("Endpoint2"), (char *)gettext("Status"));
	}
	return (clerrno);
}

//
// Get transport paths status for the specified nodes
// If operands or nodes specified, filter out the endpoints
// that doesn't match with the operands or nodes in the list.
//
static clerrno_t
get_filtered_trpaths_status(ValueList &operands, ValueList &nodes,
    ClStatus *trpaths_status)
{
	scstat_errno_t error = SCSTAT_ENOERR;
	scstat_transport_t *transport = NULL;
	clerrno_t clerr = CL_NOERR;
	char errbuf[SCSTAT_MAX_STRING_LEN];
	int filt_by_operand = 1;
	int filt_by_node = 0;
	scstat_path_t *trpaths;
	scstat_path_t *trpath;
	char ep1[SCSTAT_MAX_STRING_LEN];
	char ep2[SCSTAT_MAX_STRING_LEN];

	// If no operands specified, consider it as wildcard
	if (operands.getSize() == 0) {
		filt_by_operand = 0;
	}

	// Check for wildcard "+" operand and set filt_by_operand
	for (operands.start(); operands.end() != 1; operands.next()) {
		if (strcmp(operands.getValue(),
		    CLCOMMANDS_WILD_OPERAND) == 0) {
			filt_by_operand = 0;
			break;
		}
	}

	// Check for nodes and set filt_by_node
	if (nodes.getSize()) {
		filt_by_node = 1;
	}

	// get transport status
	error = scstat_get_transport(&transport);
	if (error != SCSTAT_ENOERR) {
		clerr = clintr_conv_scstat_err(error);
		return (clerr);
	}
	if (transport == NULL) {
		goto cleanup;
	}

	// Set the pathlist
	trpaths = transport->scstat_path_list;
	if (trpaths == NULL) {
		goto cleanup;
	}

	// Get status for each path
	for (trpath = trpaths; trpath; trpath = trpath->scstat_path_next) {
		// Get transport path endpoints
		clerr = get_path_endpoints(trpath->scstat_path_name, ep1, ep2);
		if (clerr != CL_NOERR) {
			goto cleanup;
		}

		// Filter by nodes
		if (filt_by_node && !is_node_match(nodes, ep1, ep2)) {
			continue;
		}

		// Filter by operands
		if (filt_by_operand && !is_operand_match(operands, ep1, ep2)) {
			continue;
		}

		// Add transport path (ep1, ep2) and its status
		trpaths_status->addRow(ep1, ep2, trpath->scstat_path_statstr);
	}

cleanup:

	// Free transport
	if (transport) {
		scstat_free_transport(transport);
	}

	return (clerr);
}

//
// get_path_endpoints
//
//	Get the two endpoints of pathname.
//
static clerrno_t
get_path_endpoints(char *pathname, char *ep1, char *ep2)
{
	clerrno_t clerr = CL_NOERR;
	scconf_errno_t scconferr = SCCONF_NOERR;
	scstat_errno_t	error = SCSTAT_ENOERR;
	scconf_cfg_cluster_t *clconfig = NULL;
	char errbuf[SCSTAT_MAX_STRING_LEN];
	char *adapter_name1 = NULL;
	char *adapter_name2 = NULL;

	scconferr = scconf_get_clusterconfig(&clconfig);
	error = scstat_convert_scconf_error_code(scconferr);
	if (error != SCSTAT_ENOERR) {
		clerror("Failed to get the cluster configuration.\n");
		clerr = clintr_conv_scstat_err(error);
		goto cleanup;
	}

	error = scstat_get_path_endpoints(pathname, &adapter_name1,
	    &adapter_name2);
	if (error != SCSTAT_ENOERR) {
		clerr = clintr_conv_scstat_err(error);
		goto cleanup;
	}

	error = scstat_get_cached_transport_adapter_name(clconfig,
	    adapter_name1, ep1);
	if (error != SCSTAT_ENOERR) {
		clerr = clintr_conv_scstat_err(error);
		goto cleanup;
	}

	error = scstat_get_cached_transport_adapter_name(clconfig,
	    adapter_name2, ep2);
	if (error != SCSTAT_ENOERR) {
		clerr = clintr_conv_scstat_err(error);
	}

cleanup:

	if (clconfig) {
		scconf_free_clusterconfig(clconfig);
	}

	if (adapter_name1 != NULL) {
		free(adapter_name1);
	}

	if (adapter_name2 != NULL) {
		free(adapter_name2);
	}

	return (clerr);
}

//
// is_node_match
//
//	Return true, if nodename matches with any of the nodes in the list.
//	Otherwise, return false.
//
static bool
is_node_match(ValueList &nodes, char *ep1, char *ep2)
{
	bool ret_val = false;
	char *cur_node = NULL;

	for (nodes.start(); nodes.end() != 1; nodes.next()) {
		cur_node = nodes.getValue();
		if (cur_node) {
			if (is_node_in_endpoint(cur_node, ep1) ||
			    is_node_in_endpoint(cur_node, ep2)) {
				ret_val = true;
				break;
			}
		}
	}

	return (ret_val);
}

//
// is_operand_match
//
//	Return true, if ep1 or ep2 matches with any of the operands
//	in the list. Otherwise, return false.
//
static bool
is_operand_match(ValueList &operands, char *ep1, char *ep2)
{
	bool ret_val = false;
	char *cur_ep = NULL;

	for (operands.start(); operands.end() != 1; operands.next()) {
		cur_ep = operands.getValue();
		if (cur_ep) {
			if (strcmp(cur_ep, ep1) == 0 ||
			    strcmp(cur_ep, ep2) == 0) {
				ret_val = true;
				break;
			}
		}
	}

	return (ret_val);
}

//
// is_node_in_endpoint
//
//	Return true, if the node name is found in the endpoint.
//	Otherwise, return false.
//
static bool
is_node_in_endpoint(char *node, char *endpoint)
{
	char *ptr;
	int len;

	/* Check arguments */
	if (node == NULL || endpoint == NULL)
		return (false);

	/* Make sure the endpoint has a hostname */
	if ((ptr = strchr(endpoint, ':')) == NULL)
		return (false);
	len = ptr - endpoint;

	if (strlen(node) != (size_t)len ||
	    strncmp(node, endpoint, (size_t)len) != 0)
		return (false);

	return (true);
}
