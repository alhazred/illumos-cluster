//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clinterconnect_remove.cc	1.5	08/05/20 SMI"

//
// Process clinterconnect "remove"
//

#include "clinterconnect.h"
#include "clcomm_cluster_vm.h"

static clerrno_t
clintr_remove_adapter(char *endpoint);

static clerrno_t
clintr_remove_switch(char *endpoint, int rm_cable);

static clerrno_t
clintr_remove_cable(char *endpoint1, char *endpoint2);

clerrno_t
clinterconnect_remove(ClCommand &cmd, ValueList &operands, optflgs_t optflgs)
{
	scconf_errno_t scconferr = SCCONF_NOERR;
	scconf_cltr_handle_t handle = (scconf_cltr_handle_t)0;
	clerrno_t clerr = CL_NOERR;
	clerrno_t first_err = CL_NOERR;
	char errbuf[SCCONF_MAXSTRINGLEN];
	char *endpoint1 = NULL;
	char *endpoint2 = NULL;
	int limitflg = 0;
	int rm_cable = 0;

	// Check inputs
	if (operands.getSize() == 0) {
		clintr_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}

	// Check for lflg
	if (optflgs & lflg) {
		// rm only specified endpoint
		limitflg = 1;
	}

	// Remove endpoint(s)
	for (operands.start(); operands.end() != 1; operands.next()) {
		endpoint1 = operands.getValue();
		if (endpoint1 == NULL) {
			continue;
		}

		// Check for pair of endpoints (cable)
		endpoint2 = strchr(endpoint1, ',');
		if (endpoint2) {
			*endpoint2++ = '\0';

			// Check for endpoint2
			if (strlen(endpoint2) == 0) {
				clintr_perror(CL_EINVAL);
				return (CL_EINVAL);
			}

			// Remove cable
			rm_cable = 1;
			clerr = clintr_remove_cable(endpoint1, endpoint2);

			// If failed, move on to the next endpoint
			if (clerr != CL_NOERR) {
				clerror("Failed to remove cable \"%s, %s\".\n",
				    endpoint1, endpoint2);
				if (first_err == CL_NOERR) {
					first_err = clerr;
				}
				continue;
			} else if (cmd.isVerbose) {
				clmessage("Removed cable \"%s, %s\".\n",
				    endpoint1, endpoint2);
			}

			// Don't remove endpoints if limitflg is set
			if (limitflg) {
				continue;
			}

			// Remove cable's associated ep2 (adapter or switch).
			if (strchr(endpoint2, ':')) {
				clerr = clintr_remove_adapter(endpoint2);
			} else {
				clerr = clintr_remove_switch(endpoint2,
				    rm_cable);
			}
			if (clerr != CL_NOERR) {
				clerror("Failed to remove endpoint \"%s\".\n",
				    endpoint2);
				if (first_err == CL_NOERR) {
					first_err = clerr;
				}
			}
		}

		// Remove endpoint1 (adapter or switch)
		if (strchr(endpoint1, ':')) {
			clerr = clintr_remove_adapter(endpoint1);
		} else {
			clerr = clintr_remove_switch(endpoint1, rm_cable);
		}
		if (clerr != CL_NOERR) {
			clerror("Failed to remove endpoint \"%s\".\n",
			    endpoint1);
			if (first_err == CL_NOERR) {
				first_err = clerr;
			}
		} else if (!rm_cable && cmd.isVerbose) {
			clmessage("Removed endpoint \"%s\".\n", endpoint1);
		}
	}

cleanup:

	if (first_err != CL_NOERR) {
		clerr = first_err;
	}

	return (clerr);
}

static clerrno_t
clintr_remove_adapter(char *endpoint)
{
	scconf_errno_t scconferr = SCCONF_NOERR;
	clerrno_t clerr = CL_NOERR;
	char errbuf[SCCONF_MAXSTRINGLEN];
	char *nodename = NULL;
	char *adaptername = NULL;
	char *messages = (char *)0;
	int vlanid = 0;

	// Check for inputs
	if (endpoint == NULL) {
		clerr = CL_EINTERNAL;
		clintr_perror(clerr);
		goto cleanup;
	}

	// Get node and adapter name from adapter endpoint
	nodename = strdup(endpoint);
	if (nodename == NULL) {
		clerr = CL_ENOMEM;
		clintr_perror(clerr);
		goto cleanup;
	}
	adaptername = strchr(nodename, ':');
	if (adaptername) {
		*adaptername++ = '\0';

		// Check for adaptername
		if (strlen(adaptername) == 0 || strchr(adaptername, ':') ||
		    strchr(adaptername, '@')) {
			clerr = CL_EINVAL;
			clintr_perror(clerr);
			goto cleanup;
		}
	}

	// Check for nodename
	if (strlen(nodename) == 0) {
		clerr = CL_EINVAL;
		clintr_perror(clerr);
		goto cleanup;
	}

	// Remove adapter endpoint
	scconferr = scconf_rm_cltr_adapter(NULL, nodename, adaptername,
	    vlanid, &messages);
	if (scconferr != SCCONF_NOERR) {
		// Print detailed messages
		if (messages != (char *)0) {
			clcommand_dumpmessages(messages);
			free(messages);
		}

		switch (scconferr) {
		case SCCONF_EINUSE:
			clerror("Adapter \"%s\" is still cabled "
			    "or otherwise in use.\n", adaptername);
			break;

		case SCCONF_ENOEXIST:
			clerror("Adapter \"%s\" does not exist\n",
			    adaptername);
			break;

		default:
			break;
		}

		clerr = clintr_conv_scconf_err(scconferr);
	}

cleanup:
	if (clerr != CL_NOERR) {
		clintr_perror(clerr);
	}

	if (nodename)
		free(nodename);

	return (clerr);
}

static clerrno_t
clintr_remove_switch(char *endpoint, int rm_cable)
{
	scconf_errno_t scconferr = SCCONF_NOERR;
	clerrno_t clerr = CL_NOERR;
	char errbuf[SCCONF_MAXSTRINGLEN];
	char *messages = (char *)0;

	// Check for inputs
	if (endpoint == NULL) {
		return (CL_EINTERNAL);
	}

	// Donot allow switch@port while removing a switch(not part of cable)
	if (!rm_cable && strchr(endpoint, '@')) {
		clerror("To remove a switch, specify the switchname without "
		    "the portname.\n");
		clerr = CL_EINVAL;
		goto cleanup;
	}

	// Extract the switch name from endpoint (switch@port)
	strtok(endpoint, "@");

	// Remove switch endpoint
	scconferr = scconf_rm_cltr_cpoint(NULL, endpoint, &messages);
	if (scconferr != SCCONF_NOERR) {
		// Print detailed messages
		if (messages != (char *)0) {
			clcommand_dumpmessages(messages);
			free(messages);
		}

		switch (scconferr) {
		case SCCONF_EINUSE:
			//
			// while removing a cable's associated ep (switch),
			// return NOERR if any of its ports is in use.
			//
			if (rm_cable) {
				scconferr = SCCONF_NOERR;
			} else {
				clerror("Switch \"%s\" is still cabled "
				    "or otherwise in use.\n", endpoint);
			}
			break;

		case SCCONF_ENOEXIST:
			clerror("Switch \"%s\" does not exist\n",
			    endpoint);
			break;

		default:
			break;
		}

		clerr = clintr_conv_scconf_err(scconferr);
	}

cleanup:
	if (clerr != CL_NOERR) {
		clintr_perror(clerr);
	}

	return (clerr);
}

static clerrno_t
clintr_remove_cable(char *endpoint1, char *endpoint2)
{
	scconf_errno_t scconferr;
	clerrno_t clerr = CL_NOERR;
	scconf_cltr_epoint_t epoint1, epoint2;
	char errbuf[SCCONF_MAXSTRINGLEN];
	char *messages = (char *)0;
	char *ep1 = (char *)0;
	char *ep2 = (char *)0;
	bool init_epoint1 = false;
	bool init_epoint2 = false;
	int chk_state = 1;

	// Check for inputs
	if (endpoint1 == NULL || endpoint2 == NULL) {
		clerr = CL_EINTERNAL;
		goto cleanup;
	}

	// Save a copy of endpoint1, endpoint2
	ep1 = strdup(endpoint1);
	ep2 = strdup(endpoint2);
	if (ep1 == NULL || ep2 == NULL) {
		clerr = CL_ENOMEM;
		goto cleanup;
	}

	//
	// Initialize endpoints
	// scconf_rm_cltr_cable() works with just one endpoint
	// (node:adap or switch@port). Passing the second endpoint
	// is optional and it must be a switch@port, not just switch.
	// If switch@port not specified, do not initialize epoint2
	// and pass NULL in place of epoint2.
	//
	if (strchr(ep1, ':') != NULL ||
	    strchr(ep1, '@') != NULL) {
		if (initialize_endpoint(&epoint1, ep1)) {
			clerr = CL_EINVAL;
			goto cleanup;
		}
		init_epoint1 = true;
	}

	if (strchr(ep2, ':') != NULL ||
	    strchr(ep2, '@') != NULL) {
		if (init_epoint1) {
			clerr = initialize_endpoint(&epoint2, ep2);
			init_epoint2 = true;
		} else {
			clerr = initialize_endpoint(&epoint1, ep2);
		}
		if (clerr != CL_NOERR) {
			clerr = CL_EINVAL;
			goto cleanup;
		}
	}

	// Remove cable
	if (init_epoint2) {
		scconferr = scconf_rm_cltr_cable(NULL, &epoint1, &epoint2,
		    chk_state, &messages);
	} else {
		scconferr = scconf_rm_cltr_cable(NULL, &epoint1, NULL,
		    chk_state, &messages);
	}

	if (scconferr != SCCONF_NOERR) {
		// Print detailed messages
		if (messages != (char *)0) {
			clcommand_dumpmessages(messages);
			free(messages);
		}

		switch (scconferr) {
		case SCCONF_EBUSY:
			clerror("Cable \"%s, %s\" is busy.\n",
			    endpoint1, endpoint2);
			clerror(" Removing this cable could result in the "
			    "isolation of a cluster member.\n");
			clerror(" Nodes should be shutdown before they are "
			    "isolated.\n");
			break;

		case SCCONF_EINUSE:
			if (chk_state) {
				clerror("You cannot remove the cable that is "
				    "in enabled state.\n");
				clmessage("You need to disable the cable "
				    "before attempting to remove it.\n");
			}
			break;

		case SCCONF_ENOEXIST:
			clerror("Cable \"%s, %s\" does not exist\n",
			    endpoint1, endpoint2);
			break;

		default:
			break;
		}

		clerr = clintr_conv_scconf_err(scconferr);
	}

cleanup:
	if (ep1) {
		free(ep1);
	}

	if (ep2) {
		free(ep2);
	}

	if (clerr != CL_NOERR) {
		clintr_perror(clerr);
	}

	return (clerr);
}
