//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clinterconnect_show.cc	1.3	08/05/20 SMI"

//
// Process clinterconnect "show"
//

#include "clinterconnect.h"
#include "ClShow.h"

// Get the filtered cluster cable interconnects (endpoints)
void
get_filtered_cable_show(scconf_cfg_cluster_t *clconfig,
    ValueList &operands, ValueList &nodes, ClShow *clintr_cable_show);

// Get the filtered cluster switch interconnects (endpoints)
void
get_filtered_switch_show(scconf_cfg_cluster_t *clconfig,
    ValueList &operands, ValueList &nodes, ClShow *clintr_switch_show);

// Get the filtered node adapter interconnects (endpoints)
ClShow *
get_filtered_adapter_show(scconf_cfg_node_t *cluster_node,
    ValueList &operands, ValueList &nodes);

//
// clinterconnect "show"
//
clerrno_t
clinterconnect_show(ClCommand &cmd, ValueList &operands, ValueList &nodes)
{
	scconf_errno_t scconferr = SCCONF_NOERR;
	clerrno_t clerr = CL_NOERR;
	clerrno_t first_err = CL_NOERR;
	char errbuf[SCCONF_MAXSTRINGLEN];
	ClShow *clintr_cable_show = new ClShow(HEAD_CABLES);
	ClShow *clintr_switch_show = new ClShow(HEAD_SWITCHES);
	scconf_cfg_cluster_t *clconfig = NULL;
	scconf_cfg_node_t *cluster_nodes;
	scconf_cfg_node_t *cluster_node;

	// Check for valid nodes
	clerr = clintr_verify_nodes(nodes);
	if (clerr != CL_NOERR) {
		if (first_err == CL_NOERR) {
			first_err = clerr;
		}
		if (clerr != CL_ENOENT) {
			goto cleanup;
		} else {
			if (nodes.getSize() == 1) {
				goto cleanup;
			}
		}
	}

	// Check for valid operands
	clerr = clintr_verify_operands(operands);
	if (clerr != CL_NOERR) {
		if (first_err == CL_NOERR) {
			first_err = clerr;
		}
		if (clerr != CL_ENOENT) {
			goto cleanup;
		} else {
			if (operands.getSize() == 1) {
				goto cleanup;
			}
		}
	}

	// Get the cluster configuration
	scconferr = scconf_get_clusterconfig(&clconfig);
	if (scconferr != SCCONF_NOERR) {
		clerror("Failed to get the cluster configuration.\n");
		clerr = clintr_conv_scconf_err(scconferr);
		if (first_err == CL_NOERR) {
			first_err = clerr;
		}
		goto cleanup;
	}

	//
	// Get the ClShow object for the cluster cables.
	// If operands or nodes specified, then filter out
	// the endpoints that don't match with any of the
	// operands or nodes in the nodelist.
	//
	get_filtered_cable_show(clconfig, operands, nodes, clintr_cable_show);

	// print the cluster cables
	if (clintr_cable_show->getObjectSize()) {
		(void) clintr_cable_show->print();
	}

	//
	// Get the ClShow object for the cluster switches.
	// If operands or nodes specified, then filter out
	// the endpoints that don't match with any of the
	// operands or nodes in the nodelist.
	//
	get_filtered_switch_show(clconfig, operands, nodes,
	    clintr_switch_show);

	// print the cluster switches
	if (clintr_switch_show->getObjectSize()) {
		(void) clintr_switch_show->print();
	}

	//
	// Get the ClShow object for the adapters on every
	// node in the cluster. If operands or nodes specified,
	// then filter out the endpoints that don't match with
	// any of the operands or nodes in the nodelist.
	//

	// Set the cluster nodelist
	cluster_nodes = clconfig->scconf_cluster_nodelist;
	if (cluster_nodes == NULL) {
		goto cleanup;
	}

	for (cluster_node = cluster_nodes; cluster_node;
	    cluster_node = cluster_node->scconf_node_next) {

		ClShow *clintr_adapter_show = get_filtered_adapter_show(
		    cluster_node, operands, nodes);

		if (clintr_adapter_show == (ClShow *)0) {
			continue;
		}

		// print the cluster node adapters
		if (clintr_adapter_show->getObjectSize()) {
			(void) clintr_adapter_show->print();
		}

		// Delete the clintr_adapter_show object
		delete clintr_adapter_show;
	}

cleanup:

	// Free cluster config
	scconf_free_clusterconfig(clconfig);

	// Delete the clintr_cable_show object
	delete clintr_cable_show;

	// Delete the clintr_switch_show object
	delete clintr_switch_show;

	// Return first error
	if (first_err != CL_NOERR) {
		clerr = first_err;
	}

	return (clerr);
}

//
// get_clintr_show_obj
//
//	Get the ClShow objects for cables and switches. The ClShow
//	objects must be created before calling, and the caller
//	needs to delete these objects. Adapter show object is
//	handled differently as they are children of node.
//
clerrno_t
get_clintr_show_obj(optflgs_t optflgs, ClShow *clintr_cable_show,
    ClShow *clintr_switch_show)
{
	clerrno_t clerrno = CL_NOERR;
	scconf_errno_t scconferr = SCCONF_NOERR;
	scconf_cfg_cluster_t *clconfig = NULL;
	ValueList empty_list;

	// Check inputs
	if (!clintr_cable_show || !clintr_switch_show) {
		clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}

	// Get the cluster configuration
	scconferr = scconf_get_clusterconfig(&clconfig);
	if (scconferr != SCCONF_NOERR) {
		clerror("Failed to get the cluster configuration.\n");
		clerrno = clintr_conv_scconf_err(scconferr);
		return (clerrno);
	}

	// Get the cable ClShow object
	get_filtered_cable_show(clconfig, empty_list, empty_list,
	    clintr_cable_show);

	// Get the switch ClShow object
	get_filtered_switch_show(clconfig, empty_list, empty_list,
	    clintr_switch_show);

	// Free cluster config
	scconf_free_clusterconfig(clconfig);

	return (clerrno);
}

//
// get_clintr_adapter_show
//
//	Get the adapter ClShow object for a node.
//
ClShow *
get_clintr_adapter_show(char *node_name)
{
	scconf_errno_t scconferr = SCCONF_NOERR;
	ClShow *clintr_adapter_show = NULL;
	ValueList empty_list;
	ValueList node_list;
	scconf_cfg_cluster_t *clconfig = NULL;
	scconf_cfg_node_t *cluster_node;

	// Check input
	if (!node_name) {
		clcommand_perror(CL_EINTERNAL);
		return (clintr_adapter_show);
	}

	// Get the cluster configuration
	scconferr = scconf_get_clusterconfig(&clconfig);
	if ((scconferr != SCCONF_NOERR) || !clconfig) {
		clerror("Failed to get the cluster configuration.\n");
		return (clintr_adapter_show);
	}

	for (cluster_node = clconfig->scconf_cluster_nodelist; cluster_node;
	    cluster_node = cluster_node->scconf_node_next) {
		// Skip other nodes
		if (!cluster_node->scconf_node_nodename ||
		    (strcmp(cluster_node->scconf_node_nodename,
		    node_name) != 0)) {
			continue;
		}

		// Add the node to node_list
		node_list.add(node_name);
		break;
	}

	if (cluster_node && node_list.getSize()) {
		clintr_adapter_show = get_filtered_adapter_show(
		    cluster_node, empty_list, node_list);
	}

	// Free cluster config
	scconf_free_clusterconfig(clconfig);

	return (clintr_adapter_show);
}

void
get_filtered_cable_show(scconf_cfg_cluster_t *clconfig,
    ValueList &operands, ValueList &nodes, ClShow *clintr_cable_show)
{
	int wildcard = 0;
	int filt_by_operand = 1;
	int filt_by_node = 0;
	scconf_cfg_cable_t *cltr_cables;
	scconf_cfg_cable_t *cltr_cable;
	char ep1[SCCONF_MAXSTRINGLEN];
	char ep2[SCCONF_MAXSTRINGLEN];
	char cablename[SCCONF_MAXSTRINGLEN];
	char nodename[SCCONF_MAXSTRINGLEN];
	cable_t ep_type;

	// If no operands specified, consider it as wildcard
	if (operands.getSize() == 0) {
		filt_by_operand = 0;
	}

	// Check for wildcard "+" operand and set filt_by_operand
	for (operands.start(); operands.end() != 1; operands.next()) {
		if (strcmp(operands.getValue(),
		    CLCOMMANDS_WILD_OPERAND) == 0) {
			filt_by_operand = 0;
			break;
		}
	}

	// Check for nodes and set filt_by_node
	if (nodes.getSize()) {
		filt_by_node = 1;
	}

	// Set the cable list
	cltr_cables = clconfig->scconf_cluster_cablelist;

	// Add the cables
	for (cltr_cable = cltr_cables; cltr_cable;
	    cltr_cable = cltr_cable->scconf_cable_next) {

		// Get endpoints
		get_cable_epoint(&cltr_cable->scconf_cable_epoint1, ep1, 0);
		get_cable_epoint(&cltr_cable->scconf_cable_epoint2, ep2, 0);

		// Build comma seperated pair of (cable) endpoints
		sprintf(cablename, "%s,%s", ep1, ep2);

		// Filter by operands
		if (filt_by_operand) {
			if (!operands.isValue(cablename) &&
			    !operands.isValue(ep1) && !operands.isValue(ep2)) {
				continue;
			}
		}

		// Filter by nodes
		if (filt_by_node) {
			get_ep_type(&cltr_cable->scconf_cable_epoint1,
			    &ep_type);
			if (ep_type == ADAPTER) {
				get_ep_node(&cltr_cable->scconf_cable_epoint1,
				    nodename);
				if (!nodes.isValue(nodename)) {
					continue;
				}
			}
			get_ep_type(&cltr_cable->scconf_cable_epoint2,
			    &ep_type);
			if (ep_type == ADAPTER) {
				get_ep_node(&cltr_cable->scconf_cable_epoint2,
				    nodename);
				if (!nodes.isValue(nodename)) {
					continue;
				}
			}
		}

		// Add cablename
		clintr_cable_show->addObject(TRANS_CABLE, cablename);
		// Add cable ep1
		clintr_cable_show->addProperty(cablename, ENDPOINT1, ep1);
		// Add cable ep2
		clintr_cable_show->addProperty(cablename, ENDPOINT2, ep2);
		// Add cable state
		if (cltr_cable->scconf_cable_cablestate ==
		    SCCONF_STATE_ENABLED) {
			clintr_cable_show->addProperty(cablename,
			    STATE, STR_ENABLED);
		} else {
			clintr_cable_show->addProperty(cablename,
			    STATE, STR_DISABLED);
		}
	}
}

void
get_filtered_switch_show(scconf_cfg_cluster_t *clconfig,
    ValueList &operands, ValueList &nodes, ClShow *clintr_switch_show)
{
	int filt_by_operand = 1;
	int filt_by_node = 0;
	scconf_cfg_cpoint_t *cltr_switches;
	scconf_cfg_cpoint_t *cltr_switch;
	scconf_cfg_prop_t *prop;
	scconf_cfg_port_t *cltr_switch_ports;
	scconf_cfg_port_t *cltr_switch_port;
	char switchname[SCCONF_MAXSTRINGLEN];
	char switch_propval[SCCONF_MAXSTRINGLEN];
	char switch_ports[SCCONF_MAXSTRINGLEN];
	int i;

	// If no operands specified, consider it as wildcard
	if (operands.getSize() == 0) {
		filt_by_operand = 0;
	}

	// Check for wildcard "+" operand and set filt_by_operand
	for (operands.start(); operands.end() != 1; operands.next()) {
		if (strcmp(operands.getValue(),
		    CLCOMMANDS_WILD_OPERAND) == 0) {
			filt_by_operand = 0;
			break;
		}
	}

	// Set the switch list
	cltr_switches = clconfig->scconf_cluster_cpointlist;

	// Add the switches
	for (cltr_switch = cltr_switches; cltr_switch;
	    cltr_switch = cltr_switch->scconf_cpoint_next) {
		i = 0;
		// Get the switchname
		if (cltr_switch->scconf_cpoint_cpointname == NULL) {
			continue;
		}
		strcpy(switchname, cltr_switch->scconf_cpoint_cpointname);

		// Filter by operands
		if (filt_by_operand) {
			if (!operands.isValue(switchname)) {
				continue;
			}
		}

		// Add switch name
		clintr_switch_show->addObject(TRANS_SWITCH, switchname);

		// Add switch state
		if (cltr_switch->scconf_cpoint_cpointstate ==
		    SCCONF_STATE_ENABLED) {
			clintr_switch_show->addProperty(switchname,
			    STATE, STR_ENABLED);
		} else {
			clintr_switch_show->addProperty(switchname,
			    STATE, STR_DISABLED);
		}

		// Add switch type
		clintr_switch_show->addProperty(switchname, TYPE,
		    cltr_switch->scconf_cpoint_type);

		// Add switch properties
		for (prop = cltr_switch->scconf_cpoint_propertylist;
		    prop;  prop = prop->scconf_prop_next) {
			if (prop->scconf_prop_key == NULL) {
				continue;
			}

			(void) sprintf(switch_propval, "%s",
			    prop->scconf_prop_value ?
			    prop->scconf_prop_value : "<NULL>");
			clintr_switch_show->addProperty(switchname,
			    prop->scconf_prop_key, switch_propval);
		}

		// Set the ports list
		cltr_switch_ports = cltr_switch->scconf_cpoint_portlist;

		// Add switch ports
		for (cltr_switch_port = cltr_switch_ports; cltr_switch_port;
		    cltr_switch_port = cltr_switch_port->scconf_port_next) {
			if (cltr_switch_port->scconf_port_portname == NULL) {
				continue;
			}

			if (!i) {
				(void) sprintf(switch_ports, "%s",
				    cltr_switch_port->scconf_port_portname);
				i++;
				continue;
			}
			(void) sprintf(switch_ports, "%s %s", switch_ports,
			    cltr_switch_port->scconf_port_portname);
			i++;
		}
		if (!i) {
			continue;
		}
		clintr_switch_show->addProperty(switchname,
		    PORTS, switch_ports);

		// Add switch port state
		for (cltr_switch_port = cltr_switch_ports; cltr_switch_port;
		    cltr_switch_port = cltr_switch_port->scconf_port_next) {
			if (cltr_switch_port->scconf_port_portname == NULL) {
				continue;
			}

			(void) sprintf(switch_ports, "%s(%s)", PSTATE,
			    cltr_switch_port->scconf_port_portname);

			if (cltr_switch_port->scconf_port_portstate ==
			    SCCONF_STATE_ENABLED) {
				clintr_switch_show->addProperty(switchname,
				    switch_ports, STR_ENABLED);
			} else {
				clintr_switch_show->addProperty(switchname,
				    switch_ports, STR_DISABLED);
			}
		}
	}
}

ClShow *
get_filtered_adapter_show(scconf_cfg_node_t *cluster_node,
    ValueList &operands, ValueList &nodes)
{
	int filt_by_operand = 1;
	int filt_by_node = 0;
	scconf_cfg_cltr_adap_t *cltr_adapters;
	scconf_cfg_cltr_adap_t *cltr_adapter;
	scconf_cfg_prop_t *prop;
	scconf_cfg_port_t *cltr_adap_ports;
	scconf_cfg_port_t *cltr_adap_port;
	char nodename[SCCONF_MAXSTRINGLEN];
	char adaptername[SCCONF_MAXSTRINGLEN];
	char endpoint[SCCONF_MAXSTRINGLEN];
	char adapter_propval[SCCONF_MAXSTRINGLEN];
	char adapter_ports[SCCONF_MAXSTRINGLEN];
	int i;

	// If no operands specified, consider it as wildcard
	if (operands.getSize() == 0) {
		filt_by_operand = 0;
	}

	// Check for wildcard "+" operand and set filt_by_operand
	for (operands.start(); operands.end() != 1; operands.next()) {
		if (strcmp(operands.getValue(),
		    CLCOMMANDS_WILD_OPERAND) == 0) {
			filt_by_operand = 0;
			break;
		}
	}

	// Check for nodes and set filt_by_node
	if (nodes.getSize()) {
		filt_by_node = 1;
	}

	// Get the nodename
	strcpy(nodename, cluster_node->scconf_node_nodename);

	// Filter by nodes
	if (filt_by_node) {
		if (!nodes.isValue(nodename)) {
			return ((ClShow *)0);
		}
	}

	ClShow *cltr_adap_show = new ClShow(SUBHEAD_NODETRANSPORT,
	    nodename);

	// Set the adapter list
	cltr_adapters = cluster_node->scconf_node_adapterlist;

	// Add the adapters
	for (cltr_adapter = cltr_adapters; cltr_adapter;
	    cltr_adapter = cltr_adapter->scconf_adap_next) {
		i = 0;
		if (cltr_adapter->scconf_adap_adaptername == NULL) {
			continue;
		}

		// Filter by operands
		if (filt_by_operand) {
			sprintf(endpoint, "%s:%s", nodename,
			    cltr_adapter->scconf_adap_adaptername);
			if (!operands.isValue(endpoint)) {
				continue;
			}
		}

		// Add adapter name
		strcpy(adaptername, cltr_adapter->scconf_adap_adaptername);
		cltr_adap_show->addObject(TRANS_ADAPTER, adaptername);

		// Add adapter state
		if (cltr_adapter->scconf_adap_adapterstate ==
		    SCCONF_STATE_ENABLED) {
			cltr_adap_show->addProperty(adaptername,
			    STATE, STR_ENABLED);
		} else {
			cltr_adap_show->addProperty(adaptername,
			    STATE, STR_DISABLED);
		}

		// Add adapter transport type
		cltr_adap_show->addProperty(adaptername, TRANS_TYPE,
		    cltr_adapter->scconf_adap_cltrtype);

		// Add adapter properties
		for (prop = cltr_adapter->scconf_adap_propertylist;
		    prop;  prop = prop->scconf_prop_next) {
			if (prop->scconf_prop_key == NULL) {
				continue;
			}

			(void) sprintf(adapter_propval, "%s",
			    prop->scconf_prop_value ?
			    prop->scconf_prop_value : "<NULL>");
			cltr_adap_show->addProperty(adaptername,
			    prop->scconf_prop_key, adapter_propval);
		}

		// Set the ports list
		cltr_adap_ports = cltr_adapter->scconf_adap_portlist;

		// Add adapter ports
		for (cltr_adap_port = cltr_adap_ports; cltr_adap_port;
		    cltr_adap_port = cltr_adap_port->scconf_port_next) {

			if (cltr_adap_port->scconf_port_portname == NULL) {
				continue;
			}

			if (!i) {
				(void) sprintf(adapter_ports, "%s",
				    cltr_adap_port->scconf_port_portname);
				i++;
				continue;
			}
			(void) sprintf(adapter_ports, "%s %s", adapter_ports,
			    cltr_adap_port->scconf_port_portname);
			i++;
		}
		if (!i) {
			continue;
		}
		cltr_adap_show->addProperty(adaptername,
		    PORTS, adapter_ports);

		// Add adapter ports
		for (cltr_adap_port = cltr_adap_ports; cltr_adap_port;
		    cltr_adap_port = cltr_adap_port->scconf_port_next) {

			if (cltr_adap_port->scconf_port_portname == NULL) {
				continue;
			}

			(void) sprintf(adapter_ports, "%s(%s)", PSTATE,
			    cltr_adap_port->scconf_port_portname);

			if (cltr_adap_port->scconf_port_portstate ==
			    SCCONF_STATE_ENABLED) {
				cltr_adap_show->addProperty(adaptername,
				    adapter_ports, STR_ENABLED);
			} else {
				cltr_adap_show->addProperty(adaptername,
				    adapter_ports, STR_DISABLED);
			}
		}
	}

	return (cltr_adap_show);
}
