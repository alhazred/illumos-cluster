//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clinterconnect_enable.cc	1.5	08/05/20 SMI"

//
// Process clinterconnect "enable"
//

#include "clinterconnect.h"
#include "clcomm_cluster_vm.h"

static clerrno_t
clintr_enable_adapter(char *endpoint, ValueList &nodes);

static clerrno_t
clintr_enable_switch(char *endpoint, ValueList &nodes, int enab_cable);

static clerrno_t
clintr_enable_cable(char *endpoint1, char *endpoint2, ValueList &nodes);

clerrno_t
clinterconnect_enable(ClCommand &cmd, ValueList &operands, ValueList &nodes)
{
	scconf_errno_t scconferr = SCCONF_NOERR;
	clerrno_t clerr = CL_NOERR;
	clerrno_t first_err = CL_NOERR;
	char errbuf[SCCONF_MAXSTRINGLEN];
	char *endpoint1 = NULL;
	char *endpoint2 = NULL;
	char *cables = NULL;
	ValueList endpoints = ValueList(true);
	int wildcard_operand = 0;
	int enab_cable = 0;

	// Check inputs
	if (operands.getSize() == 0) {
		clintr_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}

	// Check for valid nodes
	clerr = clintr_verify_nodes(nodes);
	if (clerr != CL_NOERR) {
		return (clerr);
	}

	// Check for "+" operand
	for (operands.start(); operands.end() != 1; operands.next()) {
		if (strcmp(operands.getValue(),
		    CLCOMMANDS_WILD_OPERAND) == 0) {
			wildcard_operand = 1;
			break;
		}
	}

	//
	// If "+" is specified as an operand, get the list of
	// cluster endpoints and enable all the cable, adapter
	// and switch endpoints.
	//
	if (wildcard_operand) {
		// Get all the cluster endpoints
		clerr = clintr_get_cluster_endpoints(endpoints);
		if (clerr != CL_NOERR) {
			return (clerr);
		}

		// Assign cluster endpoints to operands
		operands = endpoints;
	}

	// Enable endpoint(s)
	for (operands.start(); operands.end() != 1; operands.next()) {
		endpoint1 = operands.getValue();
		if (endpoint1 == NULL) {
			continue;
		}

		//
		// If cable specified, enable cable first and then
		// the associated endpoints. (re)set enab_cable.
		//
		enab_cable = 0;

		// Check for pair of endpoints (cable)
		endpoint2 = strchr(endpoint1, ',');
		if (endpoint2) {
			*endpoint2++ = '\0';

			// Check for endpoint2
			if (strlen(endpoint2) == 0) {
				clintr_perror(CL_EINVAL);
				return (CL_EINVAL);
			}

			enab_cable = 1;

			// Enable cable and associated endpoints
			clerr = clintr_enable_cable(endpoint1, endpoint2,
			    nodes);
			if (clerr != CL_NOERR) {
				if (clerr == CL_EOP) {
					if (wildcard_operand) {
						clerr = CL_NOERR;
					} else {
						clerr = CL_ENOENT;
						clerror("The cable does not "
						    "exist on any of the "
						    "nodes in the nodelist.\n");
						clerror("Failed to enable "
						    "cable \"%s, %s\".\n",
						    endpoint1, endpoint2);
					}
				} else {
					clerror("Failed to enable cable "
					    "\"%s, %s\".\n",
					    endpoint1, endpoint2);
				}
				if (first_err == CL_NOERR) {
					first_err = clerr;
				}
				continue;
			} else if (cmd.isVerbose) {
				clmessage("Enabled cable \"%s, %s\".\n",
				    endpoint1, endpoint2);
			}

			// Enable "node:adapter" or "switch[@port]" endpoint2
			if (strchr(endpoint2, ':')) {
				clerr = clintr_enable_adapter(endpoint2,
				    nodes);
			} else {
				clerr = clintr_enable_switch(endpoint2,
				    nodes, enab_cable);
			}
			if (clerr != CL_NOERR) {
				clerror("Failed to enable endpoint \"%s\".\n",
				    endpoint2);
				if (first_err == CL_NOERR) {
					first_err = clerr;
				}
			}
		}

		// Enable "node:adapter" or "switch[@port]" endpoint1
		if (strchr(endpoint1, ':')) {
			clerr = clintr_enable_adapter(endpoint1, nodes);
		} else {
			clerr = clintr_enable_switch(endpoint1, nodes,
			    enab_cable);
		}
		if (clerr != CL_NOERR) {
			if (clerr == CL_EOP) {
				if (wildcard_operand) {
					clerr = CL_NOERR;
				} else {
					clerr = CL_ENOENT;
				}

				if (!wildcard_operand && !enab_cable) {
					clerror("The endpoint does not exist "
					    "on any of the nodes in the "
					    "nodelist.\n");
					clerror("Failed to enable endpoint "
					    "\"%s\".\n", endpoint1);
				}
			} else {
				clerror("Failed to enable endpoint \"%s\".\n",
				    endpoint1);
			}
			if (first_err == CL_NOERR) {
				first_err = clerr;
			}
		} else if (!enab_cable && cmd.isVerbose) {
			clmessage("Enabled endpoint \"%s\".\n", endpoint1);
		}
	}

cleanup:

	if (first_err != CL_NOERR) {
		clerr = first_err;
	}

	return (clerr);
}

static clerrno_t
clintr_enable_adapter(char *endpoint, ValueList &nodes)
{
	scconf_errno_t scconferr = SCCONF_NOERR;
	clerrno_t clerr = CL_NOERR;
	char errbuf[SCCONF_MAXSTRINGLEN];
	char *nodename = NULL;
	char *adaptername = NULL;
	char *messages = (char *)0;
	int vlanid = 0;
	uint_t cablechk = 0;

	// Check for inputs
	if (endpoint == NULL) {
		clerr = CL_EINTERNAL;
		goto cleanup;
	}

	// Get node and adapter name from adapter endpoint
	nodename = strdup(endpoint);
	if (nodename == NULL) {
		clerr = CL_ENOMEM;
		goto cleanup;
	}
	adaptername = strchr(nodename, ':');
	if (adaptername) {
		*adaptername++ = '\0';

		// Check for adaptername
		if (strlen(adaptername) == 0 || strchr(adaptername, ':') ||
		    strchr(adaptername, '@')) {
			clerr = CL_EINVAL;
			goto cleanup;
		}
	}

	// Check for nodename
	if (strlen(nodename) == 0) {
		clerr = CL_EINVAL;
		goto cleanup;
	}

	//
	// If nodelist is specified, then check if the nodename
	// matches with any of the nodes in the list. If present,
	// go ahead and enable. Otherwise, return CL_NOERR.
	//
	if (nodes.getSize()) {
		if (!nodes.isValue(nodename)) {
			clerr = CL_EOP;
			goto cleanup;
		}
	}

	// Enable adapter endpoint
	scconferr = scconf_change_cltr_adapter(NULL, nodename, adaptername,
	    vlanid, NULL, SCCONF_STATE_ENABLED, cablechk, &messages);
	if (scconferr != SCCONF_NOERR) {
		// Print detailed messages
		if (messages != (char *)0) {
			clcommand_dumpmessages(messages);
			free(messages);
		}

		if (scconferr == SCCONF_ENOEXIST) {
			if (vlanid) {
				clerror("Adapter \"%s\" on node \"%s\" "
				    "with vlanid \"%d\" is not found or is "
				    "unknown.\n", adaptername, nodename,
				    vlanid);
			} else {
				clerror("Adapter \"%s\" on node \"%s\" "
				    "is not found or is unknown.\n",
				    adaptername, nodename);
			}
		}

		clerr = clintr_conv_scconf_err(scconferr);
	}

cleanup:
	if (clerr != CL_NOERR) {
		clintr_perror(clerr);
	}

	if (nodename)
		free(nodename);

	return (clerr);
}

static clerrno_t
clintr_enable_switch(char *endpoint, ValueList &nodes, int enab_cable)
{
	scconf_errno_t scconferr = SCCONF_NOERR;
	clerrno_t clerr = CL_NOERR;
	char errbuf[SCCONF_MAXSTRINGLEN];
	char *messages = (char *)0;
	uint_t cablechk = 0;

	// Check for inputs
	if (endpoint == NULL) {
		return (CL_EINTERNAL);
	}

	// Don't allow switch@port while enabling a switch(not part of cable)
	if (!enab_cable && strchr(endpoint, '@')) {
		clerror("To enable a switch, specify the switchname without "
		    "the portname.\n");
		clerr = CL_EINVAL;
		goto cleanup;
	}

	// Extract the switch name from endpoint (switch@port)
	strtok(endpoint, "@");

	// Enable switch endpoint
	scconferr = scconf_change_cltr_cpoint(NULL, endpoint,
	    NULL, SCCONF_STATE_ENABLED, cablechk, &messages);
	if (scconferr != SCCONF_NOERR) {
		// Print detailed messages
		if (messages != (char *)0) {
			clcommand_dumpmessages(messages);
			free(messages);
		}

		if (scconferr == SCCONF_ENOEXIST) {
			clerror("Switch \"%s\" is not found or is "
			    "unknown.\n", endpoint);
		}

		clerr = clintr_conv_scconf_err(scconferr);
	}

cleanup:
	if (clerr != CL_NOERR) {
		clintr_perror(clerr);
	}

	return (clerr);
}

static clerrno_t
clintr_enable_cable(char *endpoint1, char *endpoint2, ValueList &nodes)
{
	scconf_errno_t scconferr;
	clerrno_t clerr = CL_NOERR;
	scconf_cltr_epoint_t epoint1, epoint2;
	char errbuf[SCCONF_MAXSTRINGLEN];
	char node[SCCONF_MAXSTRINGLEN];
	char *messages = (char *)0;
	char *ep1 = (char *)0;
	char *ep2 = (char *)0;
	bool init_epoint1 = false;
	bool init_epoint2 = false;

	// Check for inputs
	if (endpoint1 == NULL || endpoint2 == NULL) {
		clerr = CL_EINTERNAL;
		goto cleanup;
	}

	// Save a copy of endpoint1, endpoint2
	ep1 = strdup(endpoint1);
	ep2 = strdup(endpoint2);
	if (ep1 == NULL || ep2 == NULL) {
		clerr = CL_ENOMEM;
		goto cleanup;
	}

	//
	// scconf_change_cltr_cable() works with just one endpoint
	// (node:adap or switch@port). Passing the second endpoint is
	// optional and it must be a switch@port, not just switch.
	// If switch@port not specified, do not initialize epoint2
	// and pass NULL in place of epoint2.
	//
	if (strchr(ep1, ':') != NULL ||
	    strchr(ep1, '@') != NULL) {
		if (initialize_endpoint(&epoint1, ep1)) {
			clerr = CL_EINVAL;
			goto cleanup;
		}
		init_epoint1 = true;
	}

	if (strchr(ep2, ':') != NULL ||
	    strchr(ep2, '@') != NULL) {
		if (init_epoint1) {
			clerr = initialize_endpoint(&epoint2, ep2);
			init_epoint2 = true;
		} else {
			clerr = initialize_endpoint(&epoint1, ep2);
		}
		if (clerr != CL_NOERR) {
			clerr = CL_EINVAL;
			goto cleanup;
		}
	}

	//
	// If nodelist is specified, then check if the nodename
	// matches with any of the nodes in the list. If present,
	// go ahead and enable. Otherwise, return CL_NOERR.
	//
	if (nodes.getSize()) {
		if (epoint1.scconf_cltr_epoint_nodename) {
			strcpy(node, epoint1.scconf_cltr_epoint_nodename);
			if (!nodes.isValue(node)) {
				clerr = CL_EOP;
				goto cleanup;
			}
		}

		if (epoint2.scconf_cltr_epoint_nodename) {
			strcpy(node, epoint1.scconf_cltr_epoint_nodename);
			if (!nodes.isValue(node)) {
				clerr = CL_EOP;
				goto cleanup;
			}
		}
	}

	// Enable cable
	if (init_epoint2) {
		scconferr = scconf_change_cltr_cable(NULL, &epoint1, &epoint2,
		    SCCONF_STATE_ENABLED, &messages);
	} else {
		scconferr = scconf_change_cltr_cable(NULL, &epoint1, NULL,
		    SCCONF_STATE_ENABLED, &messages);
	}
	if (scconferr != SCCONF_NOERR) {
		// Print detailed messages
		if (messages != (char *)0) {
			clcommand_dumpmessages(messages);
			free(messages);
		}

		if (scconferr == SCCONF_ENOEXIST) {
			clerror("Cable \"%s, %s\" is not found or is "
			    "unknown.\n", endpoint1, endpoint2);
		}

		clerr = clintr_conv_scconf_err(scconferr);
	}

cleanup:
	if (ep1) {
		free(ep1);
	}

	if (ep2) {
		free(ep2);
	}

	if (clerr != CL_NOERR) {
		clintr_perror(clerr);
	}

	return (clerr);
}
