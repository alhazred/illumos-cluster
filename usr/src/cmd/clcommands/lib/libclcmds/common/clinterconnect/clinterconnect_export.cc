//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clinterconnect_export.cc	1.3	08/05/20 SMI"

//
// Process clinterconnect "export"
//

#include "clinterconnect.h"

static void
get_filtered_cable_export(scconf_cfg_cluster_t *clconfig,
    ValueList &operands, ValueList &nodes,
    ClinterconnectExport *clintr_export);

static void
get_filtered_switch_export(scconf_cfg_cluster_t *clconfig,
    ValueList &operands, ValueList &nodes,
    ClinterconnectExport *clintr_export);

static void
get_filtered_adapter_export(scconf_cfg_cluster_t *clconfig,
    ValueList &operands, ValueList &nodes,
    ClinterconnectExport *clintr_export);

static clerrno_t
clintr_get_filtered_export(ValueList &operands, ValueList &nodes,
    ClinterconnectExport *clintr_export);

//
// This function returns a ClinterconnectExport object populated with
// all of the interconnect information from the cluster.  Caller is
// responsible for releasing the ClcmdExport object.
//
ClcmdExport *
get_clinterconnect_xml_elements()
{
	clerrno_t clerr = CL_NOERR;
	ClinterconnectExport *clintr_export = new ClinterconnectExport();
	ValueList empty_list;
	ValueList wild_operand;

	// Add the wild card into operands
	wild_operand.add(CLCOMMANDS_WILD_OPERAND);

	clerr = clintr_get_filtered_export(wild_operand, empty_list,
	    clintr_export);

	return (clintr_export);
}

//
// clinterconnect_export
//
// Exports the interconnect configuration information to
// stdout or a xml file specified. If operands or nodes
// specified, then exports the filtered interconnects.
//
// Return Values :
//	CL_NOERR	-	Success
//	CL_ENOMEM	-	No memory
//	CL_EINTERNAL	-	Internal error
//	CL_EINVAL	-	Bad options
//
clerrno_t
clinterconnect_export(ClCommand &cmd, ValueList &operands,
    char *clconfiguration, ValueList &nodes)
{
	clerrno_t clerr = CL_NOERR;
	clerrno_t first_err = CL_NOERR;
	char errbuf[SCCONF_MAXSTRINGLEN];
	char *out_file = NULL;

	ClinterconnectExport *clintr_export = new ClinterconnectExport();
	ClXml Clintr_xml;

	// Check for valid nodes
	clerr = clintr_verify_nodes(nodes);
	if (clerr != CL_NOERR) {
		if (first_err == CL_NOERR) {
			first_err = clerr;
		}
		if (clerr != CL_ENOENT) {
			goto cleanup;
		} else {
			if (nodes.getSize() == 1) {
				goto cleanup;
			}
		}
	}

	// Check for valid operands
	clerr = clintr_verify_operands(operands);
	if (clerr != CL_NOERR) {
		if (first_err == CL_NOERR) {
			first_err = clerr;
		}
		if (clerr != CL_ENOENT) {
			goto cleanup;
		} else {
			if (operands.getSize() == 1) {
				goto cleanup;
			}
		}
	}

	// Set the output file
	if (clconfiguration) {
		out_file = strdup(clconfiguration);
	} else {
		out_file = strdup("-");
	}
	if (out_file == NULL) {
		clerr = CL_ENOMEM;
		if (first_err == CL_NOERR) {
			first_err = clerr;
		}
		goto cleanup;
	}

	clerr = clintr_get_filtered_export(operands, nodes,
	    clintr_export);
	if (clerr != CL_NOERR) {
		if (first_err == CL_NOERR) {
			first_err = clerr;
		}
		goto cleanup;
	}

	// Create xml
	clerr = Clintr_xml.createExportFile(CLINTERCONNECT_CMD, out_file,
	    clintr_export);

cleanup:

	if (out_file) {
		free(out_file);
	}

	delete clintr_export;

	// Return first error
	if (first_err != CL_NOERR) {
		clerr = first_err;
	}

	return (clerr);
}

static clerrno_t
clintr_get_filtered_export(ValueList &operands, ValueList &nodes,
    ClinterconnectExport *clintr_export)
{
	scconf_errno_t scconferr = SCCONF_NOERR;
	scconf_cfg_cluster_t *clconfig = NULL;
	clerrno_t clerr = CL_NOERR;
	char errbuf[SCCONF_MAXSTRINGLEN];

	// Get the cluster configuration
	scconferr = scconf_get_clusterconfig(&clconfig);
	if (scconferr != SCCONF_NOERR) {
		clerror("Failed to get the cluster configuration.\n");
		clerr = clintr_conv_scconf_err(scconferr);
		return (clerr);
	}

	// Get the filtered export object for cluster cables.
	get_filtered_cable_export(clconfig, operands, nodes, clintr_export);

	// Get the filtered export object for cluster switches.
	get_filtered_switch_export(clconfig, operands, nodes, clintr_export);

	// Get the filtered export object for cluster node adapters.
	get_filtered_adapter_export(clconfig, operands, nodes, clintr_export);

	// Free cluster config
	scconf_free_clusterconfig(clconfig);

	return (clerr);
}

static void
get_filtered_cable_export(scconf_cfg_cluster_t *clconfig,
    ValueList &operands, ValueList &nodes,
    ClinterconnectExport *clintr_export)
{
	int filt_by_operand = 1;
	int filt_by_node = 0;
	scconf_cfg_cable_t *cltr_cables;
	scconf_cfg_cable_t *cltr_cable;
	char ep1[SCCONF_MAXSTRINGLEN];
	char ep2[SCCONF_MAXSTRINGLEN];
	char cablename[SCCONF_MAXSTRINGLEN];
	char ep1_node[SCCONF_MAXSTRINGLEN];
	char ep2_node[SCCONF_MAXSTRINGLEN];
	char port[SCCONF_MAXSTRINGLEN];
	endpoint_t cable_end1, cable_end2;
	cable_t ep1_type, ep2_type;
	state_t cable_state;

	// Check for wildcard "+" operand and set filt_by_operand
	for (operands.start(); operands.end() != 1; operands.next()) {
		if (strcmp(operands.getValue(),
		    CLCOMMANDS_WILD_OPERAND) == 0) {
			filt_by_operand = 0;
			break;
		}
	}

	// Check for operands and set filt_by_operand
	if (operands.getSize() == 0) {
		filt_by_operand = 0;
	}

	// Check for nodes and set filt_by_node
	if (nodes.getSize()) {
		filt_by_node = 1;
	}

	// Set the cable list
	cltr_cables = clconfig->scconf_cluster_cablelist;
	if (cltr_cables == NULL) {
		return;
	}

	// Add the cables
	for (cltr_cable = cltr_cables; cltr_cable;
		cltr_cable = cltr_cable->scconf_cable_next) {

		// Get cable endpoints
		get_cable_epoint(&cltr_cable->scconf_cable_epoint1, ep1, 1);
		get_cable_epoint(&cltr_cable->scconf_cable_epoint2, ep2, 1);

		// Get endpoint type
		get_ep_type(&cltr_cable->scconf_cable_epoint1, &ep1_type);
		get_ep_type(&cltr_cable->scconf_cable_epoint2, &ep2_type);

		// May have to get switch ep with port for operand check

		// Filter by operands
		if (filt_by_operand) {
			// Build comma seperated pair of (cable) endpoints
			sprintf(cablename, "%s,%s", ep1, ep2);

			if (!operands.isValue(cablename)) {
				continue;
			}
		}

		// Get endpoint nodename
		get_ep_node(&cltr_cable->scconf_cable_epoint1, ep1_node);
		get_ep_node(&cltr_cable->scconf_cable_epoint2, ep2_node);

		// Filter by nodes
		if (filt_by_node) {
			if (!nodes.isValue(ep1_node) &&
			    !nodes.isValue(ep2_node)) {
				continue;
			}
		}

		//
		// Cable can have adapters as both the endpoints or
		// one adapter and one switch as the endpoints.
		// It can never have two switches as the endpoints, so
		// the possibility of getting a switch port is only once.
		//

		// Get endpoint devicename
		get_ep_name(&cltr_cable->scconf_cable_epoint1, ep1);
		get_ep_name(&cltr_cable->scconf_cable_epoint2, ep2);

		// Add cable end1
		cable_end1.name = ep1;
		cable_end1.type = ep1_type;
		if (ep1_type == ADAPTER) {
			cable_end1.node = ep1_node;
			cable_end1.port = NULL;
		} else {
			cable_end1.node = NULL;
			get_ep_port(&cltr_cable->scconf_cable_epoint1, port);
			cable_end1.port = port;
		}

		// Add cable end2
		cable_end2.name = ep2;
		cable_end2.type = ep2_type;
		if (ep2_type == ADAPTER) {
			cable_end2.node = ep2_node;
			cable_end2.port = NULL;
		} else {
			cable_end2.node = NULL;
			get_ep_port(&cltr_cable->scconf_cable_epoint2, port);
			cable_end2.port = port;
		}

		// Get cable_state
		if (cltr_cable->scconf_cable_cablestate ==
		    SCCONF_STATE_ENABLED) {
			cable_state = ENABLED;
		} else {
			cable_state = DISABLED;
		}

		// Add cable export object
		clintr_export->addTransportCable(cable_end1, cable_end2,
		    cable_state);
	}
}

static void
get_filtered_switch_export(scconf_cfg_cluster_t *clconfig,
    ValueList &operands, ValueList &nodes,
    ClinterconnectExport *clintr_export)
{
	int filt_by_operand = 1;
	scconf_cfg_cpoint_t *cltr_switches;
	scconf_cfg_cpoint_t *cltr_switch;
	char switchname[SCCONF_MAXSTRINGLEN];
	state_t switch_state;

	// Check for wildcard "+" operand and set filt_by_operand
	for (operands.start(); operands.end() != 1; operands.next()) {
		if (strcmp(operands.getValue(),
		    CLCOMMANDS_WILD_OPERAND) == 0) {
			filt_by_operand = 0;
			break;
		}
	}

	// Check for operands and set filt_by_operand
	if (operands.getSize() == 0) {
		filt_by_operand = 0;
	}

	// Set the switch list
	cltr_switches = clconfig->scconf_cluster_cpointlist;
	if (cltr_switches == NULL) {
		return;
	}

	// Add the switches
	for (cltr_switch = cltr_switches; cltr_switch;
	    cltr_switch = cltr_switch->scconf_cpoint_next) {

		// Get the switchname
		strcpy(switchname, cltr_switch->scconf_cpoint_cpointname);
		strtok(switchname, "@");

		// Filter by operands
		if (filt_by_operand) {
			if (!operands.isValue(switchname)) {
				continue;
			}
		}

		// Set the switch state
		if (cltr_switch->scconf_cpoint_cpointstate ==
		    SCCONF_STATE_ENABLED) {
			switch_state = ENABLED;
		} else {
			switch_state = DISABLED;
		}

		clintr_export->addTransportSwitch(switchname, switch_state);
	}
}

static void
get_filtered_adapter_export(scconf_cfg_cluster_t *clconfig,
    ValueList &operands, ValueList &nodes,
    ClinterconnectExport *clintr_export)
{
	int filt_by_operand = 1;
	int filt_by_node = 0;
	scconf_cfg_node_t *cluster_nodes;
	scconf_cfg_node_t *cluster_node;
	scconf_cfg_cltr_adap_t *cltr_adapters;
	scconf_cfg_cltr_adap_t *cltr_adapter;
	char nodename[SCCONF_MAXSTRINGLEN];
	char node_adapter_name[SCCONF_MAXSTRINGLEN];
	char adapter_name[SCCONF_MAXSTRINGLEN];
	state_t adapter_state;
	adap_type_t adapter_trtype;

	// Check for wildcard "+" operand and set filt_by_operand
	for (operands.start(); operands.end() != 1; operands.next()) {
		if (strcmp(operands.getValue(),
		    CLCOMMANDS_WILD_OPERAND) == 0) {
			filt_by_operand = 0;
			break;
		}
	}

	// Check for operands and set filt_by_operand
	if (operands.getSize() == 0) {
		filt_by_operand = 0;
	}

	// Check for nodes and set filt_by_node
	if (nodes.getSize()) {
		filt_by_node = 1;
	}

	// Set the cluster nodelist
	cluster_nodes = clconfig->scconf_cluster_nodelist;
	if (cluster_nodes == NULL) {
		return;
	}

	for (cluster_node = cluster_nodes; cluster_node;
	    cluster_node = cluster_node->scconf_node_next) {

		// Get the nodename, adaptername
		strcpy(nodename, cluster_node->scconf_node_nodename);

		// Filter by nodes
		if (filt_by_node) {
			if (!nodes.isValue(nodename)) {
				continue;
			}
		}

		// Set the adapter list
		cltr_adapters = cluster_node->scconf_node_adapterlist;
		if (cltr_adapters == NULL) {
			continue;
		}

		// Add the adapters
		for (cltr_adapter = cltr_adapters; cltr_adapter;
		    cltr_adapter = cltr_adapter->scconf_adap_next) {

			sprintf(node_adapter_name, "%s:%s", nodename,
			    cltr_adapter->scconf_adap_adaptername);

			// Filter by operands
			if (filt_by_operand) {
				if (!operands.isValue(node_adapter_name)) {
					continue;
				}
			}

			// Set the adapter state
			if (cltr_adapter->scconf_adap_adapterstate ==
			    SCCONF_STATE_ENABLED) {
				adapter_state = ENABLED;
			} else {
				adapter_state = DISABLED;
			}

			// Set the adapter transport type
			if (strcmp(cltr_adapter->scconf_adap_cltrtype,
			    STR_RSM) == 0) {
				adapter_trtype = RSM;
			} else {
				adapter_trtype = DLPI;
			}

			// Get adapter_name
			strcpy(adapter_name,
			    cltr_adapter->scconf_adap_adaptername);

			// Add adapter name, node, state and transport type
			clintr_export->addTransportAdapter(adapter_name,
			    nodename, adapter_state, adapter_trtype);
		}
	}
}
