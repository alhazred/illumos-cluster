//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clinterconnect_add.cc	1.6	08/05/20 SMI"

//
// Process clinterconnect "add"
//

#include "clinterconnect.h"
#include "clcomm_cluster_vm.h"

#define	DFLT_TRANS_TYPE "dlpi"
#define	DFLT_SWITCH_TYPE "switch"

static clerrno_t
clintr_add_adapter(char *endpoint, uint_t noenable, uint_t cable_add);

static clerrno_t
clintr_add_switch(char *endpoint, uint_t noenable, uint_t cable_add);

static clerrno_t
clintr_add_cable(char *endpoint1, char *endpoint2, uint_t noenable);

clerrno_t
clinterconnect_add(ClCommand &cmd, ValueList &operands, uint_t noenable)
{
	scconf_errno_t scconferr = SCCONF_NOERR;
	clerrno_t clerr = CL_NOERR;
	clerrno_t first_err = CL_NOERR;
	char errbuf[SCCONF_MAXSTRINGLEN];
	char *endpoint1 = NULL;
	char *endpoint2 = NULL;
	uint_t cable_add;

	// Check inputs
	if (operands.getSize() == 0) {
		clintr_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}

	// Add endpoint(s)
	for (operands.start(); operands.end() != 1; operands.next()) {
		endpoint1 = operands.getValue();
		if (endpoint1 == NULL) {
			continue;
		}

		// (re)set cable_add
		cable_add = 0;

		// Check for pair of endpoints (cable)
		endpoint2 = strchr(endpoint1, ',');
		if (endpoint2) {
			*endpoint2++ = '\0';

			// Check for endpoint2
			if (strlen(endpoint2) == 0) {
				clintr_perror(CL_EINVAL);
				return (CL_EINVAL);
			}
			cable_add = 1;
		}

		// Add adapter(node:adapter) or switch(switch[@port]) endpoint1
		if (strchr(endpoint1, ':')) {
			clerr = clintr_add_adapter(endpoint1, noenable,
			    cable_add);
		} else {
			clerr = clintr_add_switch(endpoint1, noenable,
			    cable_add);
		}
		if (cable_add && clerr == CL_EEXIST) {
			clerr = CL_NOERR;
		}
		if (clerr != CL_NOERR) {
			clerror("Failed to add endpoint \"%s\".\n", endpoint1);
			if (first_err == CL_NOERR) {
				first_err = clerr;
			}
		} else if (!cable_add && cmd.isVerbose) {
			clmessage("Added endpoint \"%s\".\n", endpoint1);
		}

		//
		// If endpoint2 specified, add adapter/switch endpoint2.
		// Then add a cable using endpoint1 and endpoint2.
		//
		if (endpoint2) {
			if (strchr(endpoint2, ':')) {
				clerr = clintr_add_adapter(endpoint2,
				    noenable, cable_add);
			} else {
				clerr = clintr_add_switch(endpoint2,
				    noenable, cable_add);
			}
			if (cable_add && clerr == CL_EEXIST) {
				clerr = CL_NOERR;
			}
			if (clerr != CL_NOERR) {
				clerror("Failed to add endpoint \"%s\".\n",
				    endpoint2);
				if (first_err == CL_NOERR) {
					first_err = clerr;
				}
			}

			clerr = clintr_add_cable(endpoint1, endpoint2,
			    noenable);
			if (clerr != CL_NOERR) {
				clerror("Failed to add cable \"%s, %s\".\n",
				    endpoint1, endpoint2);
				if (first_err == CL_NOERR) {
					first_err = clerr;
				}
			} else if (cmd.isVerbose) {
				clmessage("Added cable \"%s, %s\".\n",
				    endpoint1, endpoint2);
			}
		}
	}

cleanup:

	if (first_err != CL_NOERR) {
		clerr = first_err;
	}

	return (clerr);
}

//
// clinterconnect "add -i"
//
clerrno_t
clinterconnect_add(ClCommand &cmd, ValueList &operands,
    char *clconfiguration, ValueList &nodes, uint_t noenable)
{
	scconf_errno_t scconferr = SCCONF_NOERR;
	clerrno_t clerr = CL_NOERR;
	clerrno_t first_err = CL_NOERR;
	char errbuf[SCCONF_MAXSTRINGLEN];
	ValueList clintr_xml_objs;

	// Check inputs
	if (operands.getSize() == 0 || clconfiguration == NULL) {
		clintr_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}

	// Check for valid nodes
	clerr = clintr_verify_nodes(nodes);
	if (clerr != CL_NOERR) {
		return (clerr);
	}

	OptionValues *clintr_ov = new OptionValues((char *)"add",
	    cmd.argc, cmd.argv);
	ClXml clintr_xml;

	// Add nodes to OptionValues
	clintr_ov->addOptions(CLINTER_NODE, &nodes);

	// Set verbose flag
	clintr_ov->optflgs = 0;
	if (cmd.isVerbose) {
		clintr_ov->addOption(VERBOSE);
	}

	// Set disable flag
	if (noenable) {
		clintr_ov->addOption(CLINTER_DISABLE);
	}

	// Get all objects in the xml file
	clerr = clintr_xml.getAllObjectNames(CLINTERCONNECT_CMD,
	    clconfiguration, &clintr_xml_objs);
	if (clerr != CL_NOERR) {
		return (clerr);
	}
	if (clintr_xml_objs.getSize() == 0) {
		clerror("Cannot find any endpoint in the "
		    "configuration file \"%s\".\n",
		    clconfiguration);
		return (CL_EINVAL);
	}

	//
	// Loop through the operands list and create an object for
	// the operand, if a match is found in the given xml file.
	//
	for (operands.start(); operands.end() != 1; operands.next()) {
		char *current_operand = operands.getValue();
		if (current_operand == NULL) {
			continue;
		}

		if (strcmp(current_operand, CLCOMMANDS_WILD_OPERAND) != 0) {
			int match_found = 0;

			// Strip off the @port from current_operand
			strtok(current_operand, "@");

			for (clintr_xml_objs.start(); !clintr_xml_objs.end();
			    clintr_xml_objs.next()) {
				char *curr_obj = clintr_xml_objs.getValue();
				if (curr_obj == NULL) {
					continue;
				}
				if (strcmp(curr_obj, current_operand) == 0) {
					match_found++;
					break;
				}
			}

			if (!match_found) {
				clerror("Cannot find endpoint \"%s\" in "
				    "the configuration file \"%s\".\n",
				    current_operand, clconfiguration);
				if (first_err == CL_NOERR) {
					first_err = CL_ENOENT;
				}
				continue;
			}
		}

		clintr_ov->setOperand(current_operand);
		clerr = clintr_xml.applyConfig(CLINTERCONNECT_CMD,
		    clconfiguration, clintr_ov);
		if (clerr != CL_NOERR) {
			first_err = clerr;
			continue;
		}
	}

	if (first_err != CL_NOERR) {
		clerr = first_err;
	}

	return (clerr);
}

static clerrno_t
clintr_add_adapter(char *endpoint, uint_t noenable, uint_t cable_add)
{
	scconf_errno_t scconferr = SCCONF_NOERR;
	clerrno_t clerr = CL_NOERR;
	char errbuf[SCCONF_MAXSTRINGLEN];
	char *nodename = (char *)0;
	char *adaptername = (char *)0;
	char *messages = (char *)0;
	int vlanid = 0;

	// Check for inputs
	if (endpoint == NULL) {
		clerr = CL_EINTERNAL;
		goto cleanup;
	}

	// Get node and adapter name from adapter endpoint
	nodename = strdup(endpoint);
	if (nodename == NULL) {
		clerr = CL_ENOMEM;
		goto cleanup;
	}
	adaptername = strchr(nodename, ':');
	if (adaptername) {
		*adaptername++ = '\0';

		// Check for adaptername
		if (strlen(adaptername) == 0 || strchr(adaptername, ':') ||
		    strchr(adaptername, '@')) {
			clerr = CL_EINVAL;
			goto cleanup;
		}
	}

	// Check for nodename
	if (strlen(nodename) == 0) {
		clerr = CL_EINVAL;
		goto cleanup;
	}

	// Add adapter endpoint
	scconferr = scconf_add_cltr_adapter(NULL, noenable, nodename,
	    DFLT_TRANS_TYPE, adaptername, vlanid, NULL, &messages);

	// Print detailed messages
	if (messages != (char *)0) {
		clcommand_dumpmessages(messages);
		free(messages);
	}

	if (scconferr != SCCONF_NOERR) {

		switch (scconferr) {
		case SCCONF_EUNKNOWN:
			clerror("Unknown adapter: %s\n", adaptername);
			break;

		case SCCONF_ENOEXIST:
			clerror("Unknown node: %s\n", nodename);
			break;

		case SCCONF_EEXIST:
			if (!cable_add) {
				clerror("Adapter \"%s\" is already "
				    "configured.\n", adaptername);
			}
			break;

		default:
			break;
		}

		clerr = clintr_conv_scconf_err(scconferr);
	}

cleanup:
	if (clerr != CL_NOERR) {
		clintr_perror(clerr);
	}

	if (nodename)
		free(nodename);

	return (clerr);
}

static clerrno_t
clintr_add_switch(char *endpoint, uint_t noenable, uint_t cable_add)
{
	scconf_errno_t scconferr = SCCONF_NOERR;
	clerrno_t clerr = CL_NOERR;
	char errbuf[SCCONF_MAXSTRINGLEN];
	char *messages = (char *)0;

	// Check for inputs
	if (endpoint == NULL) {
		clerr = CL_EINTERNAL;
		goto cleanup;
	}

	// Don't allow switch@port while adding a switch(not part of cable)
	if (strchr(endpoint, '@')) {
		if (!cable_add) {
			clerror("To add a switch, specify the switchname "
			    "without the portname.\n");
			clerr = CL_EINVAL;
		} else {
			// If switch is part of a cable, silently ignore.
			clerr = CL_NOERR;
		}
		goto cleanup;
	}

	// Add switch endpoint
	scconferr = scconf_add_cltr_cpoint(NULL, noenable, DFLT_SWITCH_TYPE,
	    endpoint, NULL, &messages);
	if (scconferr != SCCONF_NOERR) {
		// Print detailed messages
		if (messages != (char *)0) {
			clcommand_dumpmessages(messages);
			free(messages);
		}

		switch (scconferr) {
		case SCCONF_EUNKNOWN:
			clerror("Unknown switch: %s\n", endpoint);
			break;

		case SCCONF_EEXIST:
			if (!cable_add) {
				clerror("Switch \"%s\" is already "
				    "configured.\n", endpoint);
			}
			break;

		default:
			break;
		}

		clerr = clintr_conv_scconf_err(scconferr);
	}

cleanup:
	if (clerr != CL_NOERR) {
		clintr_perror(clerr);
	}

	return (clerr);
}

static clerrno_t
clintr_add_cable(char *endpoint1, char *endpoint2, uint_t noenable)
{
	scconf_errno_t scconferr;
	clerrno_t clerr = CL_NOERR;
	scconf_cltr_epoint_t epoint1, epoint2;
	char errbuf[SCCONF_MAXSTRINGLEN];
	char *messages = (char *)0;
	char *ep1 = (char *)0;
	char *ep2 = (char *)0;

	// Check for inputs
	if (endpoint1 == NULL || endpoint2 == NULL) {
		clerr = CL_EINTERNAL;
		goto cleanup;
	}

	// Save a copy of endpoint1, endpoint2
	ep1 = strdup(endpoint1);
	ep2 = strdup(endpoint2);
	if (ep1 == NULL || ep2 == NULL) {
		clerr = CL_ENOMEM;
		goto cleanup;
	}

	// Initialize endpoints
	if (initialize_endpoint(&epoint1, ep1)) {
		clerr = CL_EINVAL;
		goto cleanup;
	}
	if (initialize_endpoint(&epoint2, ep2)) {
		clerr = CL_EINVAL;
		goto cleanup;
	}

	// Add cable
	scconferr = scconf_add_cltr_cable(NULL, noenable, &epoint1, &epoint2,
	    &messages);
	if (scconferr != SCCONF_NOERR) {
		// Print detailed messages
		if (messages != (char *)0) {
			clcommand_dumpmessages(messages);
			free(messages);
		}

		switch (scconferr) {
		case SCCONF_EINUSE:
			clerror("One of the cable endpoints is already "
			    "in use.\n");
			scconferr = SCCONF_EEXIST;
			break;

		case SCCONF_ENOEXIST:
			clerror("A node, adapter, or switch on one of the "
			    "cable endpoints is not found or is unknown.\n");
			break;

		case SCCONF_EEXIST:
			clerror("Cable \"%s, %s\" is already "
			    "configured.\n", endpoint1, endpoint2);
			break;

		default:
			break;
		}
		clerr = clintr_conv_scconf_err(scconferr);
	}

cleanup:

	if (ep1) {
		free(ep1);
	}

	if (ep2) {
		free(ep2);
	}

	if (clerr != CL_NOERR) {
		clintr_perror(clerr);
	}

	return (clerr);
}
