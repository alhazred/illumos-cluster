//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef	_CLDEVICEGROUP_PRIVATE_H
#define	_CLDEVICEGROUP_PRIVATE_H

#pragma ident	"@(#)cldevicegroup_private.h	1.9	08/09/22 SMI"


// Private header for cldevicegroup
// Not intended for clients of cldevicegroup library.

#include <cldevicegroup.h>

#include <scadmin/scconf.h>
#include <scadmin/scstat.h>
#include <libdcs.h>

// Filtering related definitions
#define	CLDG_FOUND_MATCH	1
#define	CLDG_FOUND_NOMATCH	0
#define	CLDG_NOTFOUND		-1
#define	CLDG_NOTONLINE		-2
#define	CLDG_INTERR		-3
#define	CLDG_INVAL_TYPE		-4

#define	CLDG_INCLUDE_AUTOGEN_DGS	1
#define	CLDG_EXCLUDE_AUTOGEN_DGS	0

#define	CLDG_DCS_PROP_SUSPENDED "DCS_Suspended"

// dg state related defns
#define	CLDG_STATE_ONLINE	0

// cldg levels
#define	CLDG_LEVEL_PRIM		0
#define	CLDG_LEVEL_SEC		1
#define	CLDG_LEVEL_SPARE	2
#define	CLDG_LEVEL_INACTIVE	3
#define	CLDG_LEVEL_TRANSIT	4

#define	BUFSIZE			2048

// Internationalised Strings for display
#define	X_CLDG_NAME			GETTEXT("Device Group Name")
#define	X_CLDG_TYPE			GETTEXT("Type")
#define	X_CLDG_NODELIST			GETTEXT("Node List")
#define	X_CLDG_NODE_NAME		GETTEXT("Node Name")
#define	X_CLDG_PROP_REPL		GETTEXT("Replication Type")
#define	X_TRUE				GETTEXT("true")
#define	X_FALSE				GETTEXT("false")
#define	X_CLDG_PRIMARY			GETTEXT("Primary")
#define	X_CLDG_SECONDARY		GETTEXT("Secondary")
#define	X_CLDG_SPARE			GETTEXT("Spare Nodes")
#define	X_CLDG_INACTIVE			GETTEXT("Inactive Nodes")
#define	X_CLDG_TRANSIT			GETTEXT("In Transistion Nodes")
#define	X_CLDG_STATUS			GETTEXT("Status")
#define	X_CLDG_ONLINE_STATUS		GETTEXT("Online Status")

//
// cldevicegroup_get_dglist
//
// Fills the dglist object with list of current device groups
// and thier types. dglist object should be a instance of
// NameValue class. This function wraps over scconf_get_ds_config
// api available in libscconf to retrieve the the device group
// objects and thier respective types.
//
// return values :
//	CL_NOERR	- Success
//	CL_EINTERNAL	- Internal error.
//
clerrno_t cldevicegroup_get_dglist(NameValueList &current_dg_list, int verbose,
    int wildcard);

//
// cldevicegroup_conv_scconf_params
//
// Fills all the parameters required for scconf_add_ds_vers2
// and scconf_change_ds_vers2. The input values are provided
// in various Namevalue and NameValueList objects. can be
// commonly used by create and modify subcommands. dstype and
// dsname are not filled here as they are single values incase
// of "create" subcommand and multiple values in "modify" subcommand
// If an error occurs memory allocated is freed else it is the callers
// responsibility to free.
//
// Return values:
//	CL_NOERR	: Success
//	CL_EINVAL	: invalid input from user
//	CL_ENOMEM	: No memory
//
clerrno_t cldevicegroup_conv_scconf_params(char *dstype, ValueList nodes,
    ValueList devices, NameValueList &properties, char ***dsnodes,
    scconf_state_t *dspreference, scconf_state_t *dsfailback,
    char **dsoptions, unsigned int *dsnumsecondaries);

//
// cldevicegroup_conv_to_scswitch_params
//
// This function converts the new-cli operands and other
// options to scswitch compatible data structures. new memmory
// is allocated for these parameters. If an error occurs, memory
// is freed and an error code is returned else it is the callers
// responsibility to use these new parameters and release the
// memory.
//
// Return values :
//	CL_NOERR		- Success
//	CL_ENOMEM		- Memory allocation failed
//
clerrno_t cldevicegroup_conv_to_scswitch_params(char *node, char *dsname,
    char **scswitch_node_list, char **scswitch_service_list);

//
// cldevicegroup_filter_operands
//
// This function filters out only those operands (device groups) that
// are of particular devicegroup type and exist on particular nodes.
// The devicegroup types and nodes can be optional (NULL). The device
// groups that match the filter are filled into devicegroups list.
// As all the subcommand implementations need to filter user provide
// operands, this is heavily used.
//
// Return values :
//
// 	CL_NOERR		- Success
//	CL_EINTERNAL		- Internal error
//
clerrno_t cldevicegroup_filter_operands(ValueList &operands,
    ValueList &devicegroups, ValueList &dgtypes, ValueList nodes, int verbose);

//
// cldevicegroup_filter_operands_by_type
//
// Finds out if a particular devicegroup is currently in configuration
// and whether it's type matches with any of the types in the list - dgtypes.
// Used by most of the subcommand implementations.
//
// Return values :
//
//	CLDG_FOUND_MATCH	- Found in the config and type matches
//	CLDG_FOUND_NOMATCH	- Found in the config but type mismatch
//	CLDG_NOTFOUND		- Not found in the config
//
int cldevicegroup_filter_operands_by_type(ValueList &dgtypes,
    NameValueList &dglist, char *dgname);

//
// cldevicegroup_get_dgtype
//
// Returns the type of a devicegroup. If not found in
// current configuration, a NULL will be returned.
//
char *cldevicegroup_get_dgtype(char *dsname);

//
// cldevicegroup_map_type_name
//
// Maps the dgtype returned from scconf_get_ds_config
// to a string that is equivalent to user entered type
// Caller has to free the returned buffer
//
char *cldevicegroup_map_type_name(char *scconf_dg_type);

//
// cldevicegroup_map_scconf_to_cldg
// Maps the error codes from scconf to cl error codes
//
// Return values :
//
//	CL_NOERR		- Success
//	CL_EEXIST		- Device group is already configured.
//	CL_ENOEXIST		- Device group does not exist
//	CL_EACCESS		- Already in use
//	CL_EUSAGE		- Operation not supported
//	CL_EINVAL		- Invalid argument
//	CL_ENOMEM		- Insufficient memory
//	CL_ENOCLUSTER		- Not active in cluster
//	CL_EUNKNOWN_TYPE	- Unknown device group type
//	CL_EINTERNAL		- Internal error
//	CL_ERANGE		- Out of range value for dsnumsecondaries
//	CL_DS_EINVAL		- Invalid device configurations
//	CL_ESETUP		- Setup error
//	CL_EBUSY		- Device group is busy
//
clerrno_t cldevicegroup_map_scconf_to_cldg(scconf_errno_t scconferr);

//
// cldevicegroup_filter_dg_by_node
//
// Verifies if a given a device group is online on a node
// whose name matches one of the nodes in the nodes parameter.
//
// switch_val values :
//	CLDG_FOUND_MATCH	- match
//	CLDG_FOUND_NOMATCH	- mismatch
//	CLDG_NOTFOUND		- Not found in ccr
//	CLDG_NOTONLINE		- dg is not in online state
//	CLDG_INTERR		- Internal Error
//
// Return a valid cldg error code on error
//
int cldevicegroup_filter_dg_by_node(ValueList &nodes, NameValueList &dglist,
	char *dsname, int *switch_val);

//
// cldevicegroup_get_dg_primary
//
// Gets the primary of a device group.
// The value is set to null if dg is not in online state.
//
// Return values
//	CL_NOERR	- Success
//	CL_ENOMEM	- No memory
//	CL_EINTERNAL	- Unable to retrieve the state
//
clerrno_t cldevicegroup_get_dg_primary(char *dsname, char **online_on_node);

//
// cldevicegroup_is_suspended
//
// Determines if the device group is in suspended state
// or not.
//
cldg_suspended_state_t
cldevicegroup_is_suspended(char *dsname);


//
// cldevicegroup_get_nodelist
//
// Gets the  prefernced nodelist into string format.
// Returns NULL if list is empty
//
char *
cldevicegroup_get_nodelist(scconf_nodeid_t *nodelist);

//
// cldevicegroup_verify_nodes
//
// Verifies if the nodes provided are in cluster or
// not.
//
// Return Values :
//	CL_NOERR	- All nodes in the list are valid
//	CL_EINVAL	- One or more nodes are not found or
//			  not in cluster
//
clerrno_t cldevicegroup_verify_nodes(ValueList &nodes);

//
// cldevicegroup_verify_svm
//
// Verifies and display errors incase a dg is of svm type.
// Used for operations like add-device, add-node which cannot
// be operated on svm type dgs.
//
// Return Values :
//	CL_NOERR	- The given dg is not of svm type
//	CL_ENOENT	- dg not found in configuration
//	CL_ENOMEM	- No memory
//	CL_EINTERNAL	- Bad call
//
clerrno_t cldevicegroup_verify_svm(char *dsname);

//
// cldevicegroup_get_devlist_vl
//
// Gets the list of devices in a rawdisk dg
//
void cldevicegroup_get_devlist_vl(scconf_namelist_t *dev_list,
    ValueList *device_vl);

//
// cldevicegroup_get_nodelist_vl
//
// Gets the  prefernced nodelist into valueList format.
//
//	CL_EINTERNAL	- Bad call or internal error
//	CL_NOERR	- Success
//
clerrno_t cldevicegroup_get_nodelist_vl(scconf_nodeid_t *nodelist,
    ValueList *node_vl);

//
// cldevicegroup_get_all_dg_props
//
// Get the list of all properties present in the devicegroup.
//
void cldevicegroup_get_all_dg_props(ValueList &properties);

#endif /* _CLDEVICEGROUP_PRIVATE_H */
