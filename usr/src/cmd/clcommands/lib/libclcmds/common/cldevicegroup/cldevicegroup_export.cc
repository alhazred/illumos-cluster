//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
#pragma ident	"@(#)cldevicegroup_export.cc	1.5	08/09/17 SMI"

//
// Process cldevicegroup "export"
//

#include "cldevicegroup_private.h"
#include <libdcs.h>

clerrno_t
cldevicegroup_get_filtered_exportobj(ValueList operands, ValueList nodes,
    ValueList dgtypes, ValueList devicegroups, optflgs_t optflags,
    CldevicegroupExport *cldg_export_obj);

//
// This function returns a CldevicegroupExport object populated with
// all of the devicegroup information from the cluster.  Caller is
// responsible for releasing the ClcmdExport object.
//
// NOTE TO DEVS:  You probably want to have this function call
// another of a similar fashion which takes options to filter
// the output based off of the command line args.  This function
// is required by the ClXml class in order to get all of the
// devicegroup information without calling the devicegroup command.
//
ClcmdExport *
get_cldevicegroup_xml_elements()
{
	CldevicegroupExport *cldg_export_obj = new CldevicegroupExport();
	ValueList empty_list;
	optflgs_t optflags = 0;
	clerrno_t ret_code;

	ret_code = cldevicegroup_get_filtered_exportobj(empty_list, empty_list,
	    empty_list, empty_list, optflags, cldg_export_obj);
	return (cldg_export_obj);
}

//
// cldevicegroup_export
//
// Exports the device group configuration information to
// a xml file for reusuability. User can filter out dg list
// using node and type filters.
//
// Return Values :
//	CL_NOERR	-	Success
//	CL_ENOMEM	-	No memory
//	CL_EINTERNAL	-	Internal error
//	CL_EINVAL	-	Bad options
//	CL_ETYPE	- Invalid device group type
//	CL_ENOENT	- Invalid device group
//
clerrno_t
cldevicegroup_export(ValueList dgtypes, ValueList nodes, ValueList operands,
    optflgs_t optflags, char *clconfiguration)
{
	CldevicegroupExport *cldg_export_obj = new CldevicegroupExport();
	ClXml cldg_cl_xml;

	ValueList devicegroups;
	clerrno_t ret_code = CL_NOERR;
	clerrno_t first_err = CL_NOERR;
	scconf_errno_t scconferr = SCCONF_NOERR;
	unsigned int ismember;
	int exitstatus;

	// Args check
	if (!clconfiguration) {
		clconfiguration = strdup("-");
		if (!clconfiguration) {
			ret_code = first_err = CL_ENOMEM;
			clcommand_perror(first_err);
			goto cleanup;
		}
	}

	// Valid nodes ?
	ret_code = cldevicegroup_verify_nodes(nodes);
	if (ret_code != CL_NOERR) {
		if (first_err == CL_NOERR) {
			first_err = ret_code;
		}
		return (first_err);
	}

	//
	// Get the list of devicegroup that need to be exported.
	// Not all operands will necessary be exported as user
	// can specify type filter with -t and -n options.
	//
	// Autogenerated diskgroup should be included
	//

	ret_code = cldevicegroup_filter_operands(operands, devicegroups,
	dgtypes, nodes, CLDG_INCLUDE_AUTOGEN_DGS);
	if (ret_code != CL_NOERR) {
		if (first_err == CL_NOERR) {
			first_err = ret_code;
		}
	}

	if (devicegroups.getSize() == 0) {
		goto cleanup;   // Nothing to export
	}

	// Get the export object with all device groups info
	ret_code = cldevicegroup_get_filtered_exportobj(operands, nodes,
		dgtypes, devicegroups, optflags, cldg_export_obj);
	if (ret_code != CL_NOERR) {
		if (first_err == CL_NOERR) {
			first_err = ret_code;
		}
	}

	// Export the device group configuration information
	ret_code = cldg_cl_xml.createExportFile(CLDEVICEGROUP_CMD,
	    clconfiguration, cldg_export_obj);

	if (ret_code != CL_NOERR) {
		if (first_err == CL_NOERR) {
			first_err = ret_code;
		}
	}

cleanup:
	delete cldg_export_obj;

	if ((optflags & vflg) && ret_code == CL_NOERR &&
	    clconfiguration != NULL && strcmp(clconfiguration, "-") != 0) {
		clmessage("Successfully exported device group "
		    "configuration.\n");
	}
	return (first_err);
}

//
// cldevicegroup_get_filtered_exportobj
//
// Returns the Export object consisting of all device groups
// information after applying "type" and "node" filters.
//
// Possible error values:
//
//	CL_NOERR	: Success
//	CL_ENOMEM	: No memory
//	CL_EINTERNAL	: Internal error
//
clerrno_t
cldevicegroup_get_filtered_exportobj(ValueList operands,
	ValueList nodes, ValueList dgtypes, ValueList devicegroups,
	optflgs_t optflags, CldevicegroupExport *cldg_export_obj)
{

	clerrno_t ret_code = CL_NOERR;
	clerrno_t first_err = CL_NOERR;
	scconf_errno_t scconferr = SCCONF_NOERR;
	scconf_cfg_ds_t *dsconfig = NULL;
	scconf_cfg_ds_t *device_group = NULL;
	char *current_dg_name = NULL;
	char err_buf[SCCONF_MAXSTRINGLEN];
	char prop_val[BUFSIZE];
	dg_type_t dg_type;

	// Get the copy of device group configuration
	scconferr = scconf_get_ds_config(NULL, SCCONF_BY_NAME_FLAG, &dsconfig);
	if (scconferr != SCCONF_NOERR || dsconfig == NULL) {
	// unable to retrieve configuration
		ret_code = cldevicegroup_map_scconf_to_cldg(scconferr);
		if (first_err == CL_NOERR) {
			first_err = ret_code;
		}
		goto cleanup;
	}

	// Add individual device group objects
	for (device_group = dsconfig; device_group;
	    device_group = device_group->scconf_ds_next) {
		ValueList *node_vl = new ValueList();
		ValueList *devices_vl = new ValueList();
		if (!devicegroups.isValue(device_group->scconf_ds_name))
			continue;

		// Get the type
		if (strcmp(device_group->scconf_ds_type,
		    CLDG_SCCONF_RAWDISK) == 0 || strcmp(
		    device_group->scconf_ds_type, CLDG_SCCONF_LOCALDISK) == 0) {
			dg_type = RAWDISK;
		}
		if (strcmp(device_group->scconf_ds_type,
		    CLDG_SCCONF_SDS) == 0) {
			dg_type = SVM;
		}
		if (strcmp(device_group->scconf_ds_type,
		    CLDG_SCCONF_SVM) == 0) {
			dg_type = SVM;
		}

		// Create the dg node
		ret_code = cldg_export_obj->createDevicegroup(
		    device_group->scconf_ds_name, dg_type);
		if (ret_code != CL_NOERR) {
			if (first_err = CL_NOERR) {
				first_err = ret_code;
			}
			goto cleanup;
		}

		// Add nodes
		ret_code = cldevicegroup_get_nodelist_vl(
		    device_group->scconf_ds_nodelist, node_vl);
		if (ret_code != CL_NOERR) {
			// We will return this error. however we
			// will continue with other operands
			if (first_err == CL_NOERR)
				first_err = ret_code;
			continue;
		}
		for (node_vl->start(); !node_vl->end(); node_vl->next()) {
			ret_code = cldg_export_obj->addDevicegroupNode(
			    device_group->scconf_ds_name, node_vl->getValue());
			if (ret_code != CL_NOERR) {
				if (first_err == CL_NOERR) {
					first_err = ret_code;
				}
				goto cleanup;
			}
		}

		// Add devices if the type is rawdisk
		if (strcmp(device_group->scconf_ds_type,
		    CLDG_SCCONF_RAWDISK) == 0 || strcmp(
		    device_group->scconf_ds_type, CLDG_SCCONF_LOCALDISK) == 0) {
			cldevicegroup_get_devlist_vl(
			    device_group->scconf_ds_devvalues, devices_vl);
			for (devices_vl->start(); !devices_vl->end();
			    devices_vl->next()) {
				ret_code = cldg_export_obj->addMember(
				    device_group->scconf_ds_name,
				    devices_vl->getValue());
				if (ret_code != CL_NOERR) {
					if (first_err == CL_NOERR) {
						first_err = ret_code;
					}
					goto cleanup;
				}
			}
		}

		// Add individual properties
		// numsecondaries, preferenced, failback

		// Failback
		if (device_group->scconf_ds_failback == 1)
			strcpy(prop_val, STR_TRUE);
		else
			strcpy(prop_val, STR_FALSE);

		ret_code = cldg_export_obj->addProperty(
		    device_group->scconf_ds_name, CLDG_PROP_FAILBACK, prop_val,
		    false);
		if (ret_code != CL_NOERR) {
			if (first_err == CL_NOERR) {
				first_err = ret_code;
			}
			goto cleanup;
		}

		// Preferenced
		if (device_group->scconf_ds_preference == SCCONF_STATE_ENABLED)
			strcpy(prop_val, STR_TRUE);
		else
			strcpy(prop_val, STR_FALSE);

		ret_code = cldg_export_obj->addProperty(
		    device_group->scconf_ds_name, CLDG_PROP_PREFERENCED,
		    prop_val, false);
		if (ret_code != CL_NOERR) {
			if (first_err == CL_NOERR) {
				first_err = ret_code;
			}
			goto cleanup;
		}


		// Number of secondaries
		if (device_group->scconf_ds_num_secs == 0)
			device_group->scconf_ds_num_secs =
			    SCCONF_NUMSECONDARIES_UNSET;
		(void) sprintf(prop_val, "%d",
		    device_group->scconf_ds_num_secs);

		ret_code = cldg_export_obj->addProperty(
		    device_group->scconf_ds_name, CLDG_PROP_NUMSECONDARIES,
		    prop_val, false);
		if (ret_code != CL_NOERR) {
			if (first_err == CL_NOERR) {
				first_err = ret_code;
			}
			goto cleanup;
		}
	}

cleanup:
	scconf_free_ds_config(dsconfig);
	return (first_err);
}
