//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
#pragma ident	"@(#)cldevicegroup_show.cc	1.6	08/09/17 SMI"

#include "cldevicegroup_private.h"
#include <libdid.h>
#include <libdcs.h>

void cldevicegroup_get_dg_props(scconf_cfg_ds_t *device_group,
    NameValueList &prop_list);
clerrno_t cldevicegroup_get_filtered_showobj(ValueList operands,
    ValueList nodes, ValueList dgtypes, optflgs_t optflags,
    ClShow *cldg_show_obj);

//
// show subcommand implementation.
//
// Possible return values :
//
//	CL_EINTERNAL	- Bad call or internal error
//	CL_ENOMEM	- Insufficient memory or other os resources
//	CL_EINVAL	- Invalid input
//	CL_ETYPE	- Invalid device group type
//	CL_ENOENT	- Invalid device group
//
clerrno_t
cldevicegroup_show(ValueList dgtypes, ValueList nodes,
    ValueList operands, optflgs_t optflags)
{
	ClShow *cldg_show_obj = new ClShow(HEAD_DEVICEGROUPS);
	clerrno_t ret_code = CL_NOERR;
	clerrno_t first_err = CL_NOERR;
	scconf_errno_t scconferr = SCCONF_NOERR;
	unsigned int ismember;
	int exitstatus;

	// Valid nodes ?
	ret_code = cldevicegroup_verify_nodes(nodes);
	if (ret_code != CL_NOERR) {
		if (first_err == CL_NOERR) {
			first_err = ret_code;
		}
	}

	// Get the show object with all device groups info
	ret_code = cldevicegroup_get_filtered_showobj(operands, nodes,
	    dgtypes, optflags, cldg_show_obj);

	if (ret_code != CL_NOERR) {
		if (first_err == CL_NOERR) {
			first_err = ret_code;
		}
	}

	// print the device group configuration information
	if (cldg_show_obj->getObjectSize() != 0)
		cldg_show_obj->print();

cleanup:
	delete cldg_show_obj;

	return (first_err);
}

//
// get_cldevicegroup_show_obj
//
// Gets the cldevicegroup show object. Written
// primarily for cluster show use. The object
// must be preallocated else CL_EINTERNAL will
// be returned.
//
clerrno_t
get_cldevicegroup_show_obj(optflgs_t optflgs, ClShow *cldg_show_obj)
{
	ValueList empty_list;

	// Args check
	if (cldg_show_obj == NULL) {
		clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}

	// Return the objects
	return (cldevicegroup_get_filtered_showobj(empty_list, empty_list,
	    empty_list, optflgs, cldg_show_obj));
}

//
// cldevicegroup_get_filtered_showobj
//
// Returns the Show object consisting of all device groups
// information after applying "type" and "node" filters.
//
// Possible error values:
//
//	CL_NOERR	: Success
//	CL_ENOMEM	: No memory
//	CL_EINTERNAL	: Internal error
//
clerrno_t
cldevicegroup_get_filtered_showobj(ValueList operands,
    ValueList nodes, ValueList dgtypes, optflgs_t optflags,
    ClShow *cldg_show_obj)
{

	clerrno_t ret_code = CL_NOERR;
	clerrno_t first_err = CL_NOERR;
	ValueList devicegroups;
	scconf_errno_t scconferr = SCCONF_NOERR;
	scconf_cfg_ds_t *dsconfig = NULL;
	scconf_cfg_ds_t *device_group = NULL;
	char *current_dg_name = NULL;

	//
	// Get the list of devicegroup that need to be show.
	// Not all operands will necessary be shown as user
	// can specify type filter with -t and -n options.
	//
	ret_code = cldevicegroup_filter_operands(operands, devicegroups,
	    dgtypes, nodes, optflags & vflg);
	if (ret_code != CL_NOERR) {
		if (first_err == CL_NOERR) {
			first_err = ret_code;
		}
	}

	// Get the copy of device group configuration
	scconferr = scconf_get_ds_config(NULL, SCCONF_BY_NAME_FLAG, &dsconfig);
	if (scconferr != SCCONF_NOERR || dsconfig == NULL) {
		// unable to retrieve configuration
		ret_code = cldevicegroup_map_scconf_to_cldg(scconferr);
		if (ret_code != CL_NOERR) {
			if (first_err == CL_NOERR) {
				first_err = ret_code;
			}
			goto cleanup;
		}
	}

	// Add individual device group objects
	for (device_group = dsconfig; device_group;
	    device_group = device_group->scconf_ds_next) {
		NameValueList prop_list;
		if (!devicegroups.isValue(device_group->scconf_ds_name))
			continue;
		current_dg_name = device_group->scconf_ds_name;
		(void) cldevicegroup_get_dg_props(device_group, prop_list);
		if (current_dg_name == NULL ||
		    prop_list.getSize() == 0) {
			if (first_err == CL_NOERR) {
				clcommand_perror(CL_EINTERNAL);
				first_err = CL_EINTERNAL;
			}
			goto cleanup;
		}

		// Add the object and properties
		cldg_show_obj->addObject(X_CLDG_NAME, current_dg_name);
		cldg_show_obj->addProperties(current_dg_name, &prop_list);
	}

cleanup:
	scconf_free_ds_config(dsconfig);
	return (first_err);
}

//
// cldevicegroup_get_dg_props
//
// Returns the configuration information of a devicegroup
// in the valuelist object specfied by "prop_list"
//

void
cldevicegroup_get_dg_props(scconf_cfg_ds_t *device_group,
    NameValueList &prop_list)
{
	char *nodelist = NULL;
	char *repl_property = NULL;
	char devices_buffer[BUFSIZE];
	scconf_namelist_t *namelist;
	char buffer[25];
	scconf_cfg_prop_t *prop = NULL;
	uint_t autogen = 0;

	// Args check
	if (!device_group)
		return;

	// Type
	if (device_group->scconf_ds_type) {
		prop_list.add(X_CLDG_TYPE,
		    device_group->scconf_ds_type);
	} else {
		prop_list.add(X_CLDG_TYPE, "<NULL>");
	}

	// Failback
	if (device_group->scconf_ds_failback == 1)
		prop_list.add(CLDG_PROP_FAILBACK, X_TRUE);
	else
		prop_list.add(CLDG_PROP_FAILBACK, X_FALSE);

	// Nodelist
	nodelist = cldevicegroup_get_nodelist(device_group->scconf_ds_nodelist);
	if (nodelist)
		prop_list.add(X_CLDG_NODELIST, nodelist);
	else
		prop_list.add(X_CLDG_NODELIST, "<NULL>");

	// Preferenced
	if (device_group->scconf_ds_preference == SCCONF_STATE_ENABLED)
		prop_list.add(CLDG_PROP_PREFERENCED, X_TRUE);
	else
		prop_list.add(CLDG_PROP_PREFERENCED, X_FALSE);

	//
	// Local only ?
	// The scconf_get_ds_config api from did module
	// returns ds type as "Local_Disk" if the localonly property
	// is true.
	//
	if (strcmp(device_group->scconf_ds_type, "Local_Disk") == 0) {
		prop_list.add(CLDG_PROP_LOCALONLY, X_TRUE);
	} else {
		if (strcmp(device_group->scconf_ds_type, "Disk") == 0)
			prop_list.add(CLDG_PROP_LOCALONLY, X_FALSE);
	}

	// Autogenerated
	if (strcmp(device_group->scconf_ds_type, "Local_Disk") == 0 ||
	    strcmp(device_group->scconf_ds_type, "Disk") == 0) {
		for (prop = device_group->scconf_ds_propertylist;  prop;
			    prop = prop->scconf_prop_next) {
				if (prop->scconf_prop_key &&
				    strcmp(SCCONF_DS_PROP_AUTOGENERATED,
				    prop->scconf_prop_key) == 0)
					autogen++;
		}

		if (autogen)
			prop_list.add(CLDG_PROP_AUTOGEN, X_TRUE);
		else
			prop_list.add(CLDG_PROP_AUTOGEN, X_FALSE);
	}

	// Num of secondaries
	if (device_group->scconf_ds_num_secs == 0 &&
	    !scconf_is_local_device_service(
	    device_group->scconf_ds_name)) {
		(void) sprintf(buffer, "%d",
		    DCS_DEFAULT_DESIRED_NUM_SECONDARIES);
	} else {
		(void) sprintf(buffer, "%d",
		    device_group->scconf_ds_num_secs);
	}
	prop_list.add(CLDG_PROP_NUMSECONDARIES, buffer);

	// Devices group name
	devices_buffer[0] = '\0';
	for (namelist = device_group->scconf_ds_devvalues;
	    namelist; namelist = namelist->scconf_namelist_next) {
		if (!namelist->scconf_namelist_name)
			continue;
		/* add it to the list (devnamebuf) */
		if (strlen(devices_buffer) != 0)
			(void) strcat(devices_buffer, " ");
		(void) strcat(devices_buffer,
		    namelist->scconf_namelist_name);
	}
	if (devices_buffer[0] != '\0') {
		prop_list.add((char *)gettext(device_group->scconf_ds_label),
		    devices_buffer);
	} else {
		prop_list.add((char *)gettext(device_group->scconf_ds_label),
		    "<NULL>");
	}

	// Replication property - truecopy support
	// Will be displayed only if property is set
	repl_property = scconf_get_repl_property(
	    device_group->scconf_ds_propertylist);
	if (repl_property)
		prop_list.add(X_CLDG_PROP_REPL, repl_property);

	// Cleanup
	if (nodelist)
		free(nodelist);

}
