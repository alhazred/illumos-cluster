//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
#pragma ident	"@(#)cldevicegroup_create.cc	1.7	08/05/20 SMI"

#include "cldevicegroup_private.h"

clerrno_t cldevicegroup_print_create_errors(scconf_errno_t scconf_err,
    char *dsname, int verbose);

//
// "Create" sub command implementation for cldevicegroup command
// Expects that all validations are already properly done.
// Relies on scconf_add_ds_vers2 api from libscconf to create the
// device group in CCR.
//
// Possible return values :
//
//	CL_EINTERNAL	- Bad call or internal error
//	CL_ENOMEM	- Insufficient memory or other os resources
//	CL_ENOCLUSTER	- Not in cluster
//	CL_EINVAL	- Invalid input
//
clerrno_t
cldevicegroup_create(ValueList &dgtypes, ValueList &nodes,
    ValueList &devices, NameValueList &properties, ValueList &operands,
    optflgs_t optflags)
{
	// Args required for scconf_add_ds_vers2
	char *dstype = NULL;
	char *dsname = NULL;
	char **dsnodes = NULL;
	char *dsoptions = NULL;
	char *messages = NULL;
	scconf_state_t dspreference = SCCONF_STATE_UNCHANGED;
	scconf_state_t dsfailback = SCCONF_STATE_DISABLED;
	unsigned int dsnumsecondaries = SCCONF_NUMSECONDARIES_UNSET;
	scconf_errno_t scconferr = SCCONF_NOERR;
	NameValueList current_dg_list;
	int nodecheck = 1;
	char *dg_type = NULL;
	char *localprop = NULL;

	int i = 0, j = 0;
	clerrno_t ret_code = CL_NOERR;
	int exitstatus;
	unsigned int ismember = 0;

	// Valid call ?
	if (dgtypes.getSize() != 1 ||
	    operands.getSize() != 1) {
		clcommand_perror(CL_EINTERNAL);
		ret_code = CL_EINTERNAL;
		goto cleanup;
	}

	//
	// Check if atleast one node is provided
	//
	if (dgtypes.getSize() == 1) {
		dgtypes.start();
		dg_type = dgtypes.getValue();
	}

	if (nodecheck && !nodes.getSize()) {
		clcommand_perror(CL_EINTERNAL);
		ret_code = CL_EINTERNAL;
		goto cleanup;
	}

	//
	// Convert all the passed arguments to new arguments
	// as required by scconf_add_ds_vers2
	//
	dgtypes.start();
	dstype = dgtypes.getValue();

	// device group name
	operands.start();
	dsname = operands.getValue();

	// Valid nodes ?
	ret_code = cldevicegroup_verify_nodes(nodes);
	if (ret_code != CL_NOERR) {
		goto cleanup;
	}

	//
	// SVM type devicegroups cannot be added through
	// SC commands. metaset should be used instead
	//
	if (dstype && strcmp(dstype, CLDG_SDS_TYPE) == 0) {
		ret_code = CL_EOP;
		clerror("Solaris Volume Manager device groups cannot be "
		    "created using this command.\n");
		clerror("Use the metaset(1M) or metassist(1M) commands "
		    "to create Solaris Volume Manager device groups.\n");
		goto cleanup;
	}

	// Verify if this is already configured in cluster.
	ret_code = cldevicegroup_get_dglist(current_dg_list, 1, 0);
	if (ret_code != CL_NOERR) {
		return (ret_code);
	}
	if (current_dg_list.getValue(dsname)) {
		clerror("Device group \"%s\" is already configured.\n", dsname);
		ret_code = CL_EEXIST;
		goto cleanup;
	}

	// For all other arguments, use common function
	ret_code = cldevicegroup_conv_scconf_params(dstype, nodes,
	    devices, properties, &dsnodes, &dspreference,
	    &dsfailback, &dsoptions, &dsnumsecondaries);
	if (ret_code != CL_NOERR)
		goto cleanup;

	// Call the service to create the device group in cluster
	scconferr = scconf_add_ds_vers2(dstype, dsname, dsnodes, dspreference,
	    NULL, dsfailback, NULL, dsoptions, dsnumsecondaries, &messages);

	// Print any errors and messages from the buffer
	(void) clcommand_dumpmessages(messages);
	ret_code = cldevicegroup_print_create_errors(scconferr, dsname,
	    optflags & vflg);

cleanup:
	if (dsnodes) {
		for (j = 0; dsnodes[j] != NULL; j++) {
			free(dsnodes[j]);
		}
		free(dsnodes);
	}
	if (dsoptions)
		free(dsoptions);

	// Print failure message incase of error
	if (ret_code != CL_NOERR && dsname)
		clerror("Creation of device group \"%s\" failed.\n", dsname);

	return (ret_code);
}

//
// cldevicegroup_create - with -i option
// Create the device group objects based on the information
// provided in a configuration file. The values provided in
// the command line will be written over the ones in the file.
//
// Possible return values :
//
//	CL_EINTERNAL	- Bad call or internal error
//	CL_ENOMEM	- Insufficient memory or other os resources
//	CL_ENOCLUSTER   - Not in cluster
//	CL_EINVAL	- Invalid input
//
clerrno_t
cldevicegroup_create(ClCommand &cmd, ValueList &dgtypes, ValueList &nodes,
    ValueList &devices, NameValueList &properties, ValueList &operands,
    optflgs_t optflags, char *clconfiguration)
{
	clerrno_t ret_code = CL_NOERR;
	clerrno_t first_err = CL_NOERR;
	OptionValues *cldg_ov = new OptionValues(
	    (char *)"create", cmd.argc, cmd.argv);
	ClXml cldg_xml;
	char *current_op_name = NULL;
	ClXml xml;
	ValueList xml_objects;

	// Args check
	if (operands.getSize() == 0 || !clconfiguration) {
		// bad call
		clcommand_perror(CL_EINTERNAL);
		ret_code = CL_EINTERNAL;
		goto cleanup;
	}

	// Fill the object with types and properties
	cldg_ov->addOptions(CLDG_TYPE, &dgtypes);
	cldg_ov->addOptions(CLDG_NODE, &nodes);
	cldg_ov->addOptions(CLDG_MEMBER, &devices);
	cldg_ov->addNVOptions(CLDG_PROP, &properties);

	cldg_ov->optflgs = 0;
	if (optflags & vflg) {
		cldg_ov->optflgs |= vflg;
	}

	// Get all the objects in the xml file
	ret_code = xml.getAllObjectNames(CLDEVICEGROUP_CMD, clconfiguration,
	    &xml_objects);
	if (ret_code != CL_NOERR) {
		return (ret_code);
	}

	if (xml_objects.getSize() == 0) {
		// Nothing to add. Just report a message and return
		clmessage("Cannot find any device groups in file \"%s\".\n",
		    clconfiguration);
		return (CL_NOERR);
	}

	// For each operand, apply the configuration and create
	// the object
	for (operands.start(); !operands.end(); operands.next()) {
		current_op_name = operands.getValue();
		if (strcmp(current_op_name, CLCOMMANDS_WILD_OPERAND) != 0 &&
		    (!xml_objects.isValue(current_op_name))) {
			clerror("Cannot find device group \"%s\" in file "
			    "\"%s\".\n", current_op_name, clconfiguration);
			if (first_err == CL_NOERR) {
				first_err = CL_ENOENT;
			}
			continue;
		}
		cldg_ov->setOperand(current_op_name);
		ret_code = cldg_xml.applyConfig(CLDEVICEGROUP_CMD,
		    clconfiguration, cldg_ov);
		if (ret_code != CL_NOERR && first_err == CL_NOERR) {
			// We will return this error, however we will
			// continue with other operands.
			first_err = ret_code;
			continue;
		}
	}
	ret_code = first_err;

cleanup:
	return (ret_code);
}

//
// cldevicgroup_print_create_errors
//
// Prints the error messages specific to create subcommand.
// for generic error messages, relies on clcommand_perror function.
// maps the scconf error to new cli error code and returns the same.
//
clerrno_t
cldevicegroup_print_create_errors(scconf_errno_t scconf_err, char *dsname,
    int verbose)
{
	char err_buffer[BUFSIZ];
	clerrno_t cldg_code;

	switch (scconf_err) {
	case SCCONF_NOERR:
		cldg_code = CL_NOERR;
		break;

	case SCCONF_EEXIST:
		// Device group is already configured
		clerror("Device group is already configured.\n");
		cldg_code = CL_EEXIST;
		break;

	case SCCONF_ENOEXIST:
		// Some component is not found or unknown
		clerror("A disk set, disk group or a device is "
		    "not found or is unknown.\n");
		cldg_code = CL_ENOENT;
		break;

	case SCCONF_EINUSE:
		// Already in use
		clerror("A disk set, disk group or other device is already "
		    "in use in another device group.\n");
		cldg_code = CL_EACCESS;
		break;

	case SCCONF_EUSAGE:
		// Create operation is not allowed on this device group type
		clerror("Create operation is not supported for this device "
		    "group type.\n");
		cldg_code = CL_EOP;
		break;

	case SCCONF_EBUSY:
		// Device group is busy
		clerror("Device group is in a busy state; Try again later.\n");
		cldg_code = CL_EBUSY;
		break;

	case SCCONF_EINVAL:
		// Invalid option or input given
		clerror("Bad options or input is provided.\n");
		cldg_code = CL_EINVAL;
		break;

	case SCCONF_EUNKNOWN:
		// Device group type is unknown
		clerror("Unknown device group type is given.\n");
		cldg_code = CL_ETYPE;
		break;

	case SCCONF_DS_EINVAL:
		// Inconsistencies in device configuration
		clerror("Inconsistencies are detected in the device group "
		    "configuration.\n");
		cldg_code = CL_ESTATE;
		break;

	case SCCONF_ESETUP:
		// Setup problem
		clerror("The volume manager or other device management "
		    "software was not installed or set up properly.\n");
		cldg_code = CL_ESTATE;
		break;

	case SCCONF_ERANGE:
		// num_secondaries out of range
		clerror("The value specified for \"%s\" property is too "
		    "large for the device group configuration.\n");
		cldg_code = CL_EPROP;
		break;

	default:
		// Generic error messages
		cldg_code = cldevicegroup_map_scconf_to_cldg(scconf_err);
		break;
	}

	if (cldg_code == CL_NOERR && verbose) {
		clmessage("Successfully created device group \"%s\".\n",
		    dsname);
	}

	return (cldg_code);
}
