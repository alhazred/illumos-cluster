//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
#pragma ident	"@(#)cldevicegroup_status.cc	1.4	08/09/17 SMI"

#include "cldevicegroup_private.h"
#include <libdid.h>
#include <libdcs.h>

clerrno_t cldevicegroup_get_filtered_statusobjs(ValueList operands,
    ValueList nodes, ValueList dgtypes, optflgs_t optflags,
    ClStatus *cldg_status_obj, ClStatus *cldg_addnl_obj,
    ClStatus *cldg_multidg_obj);
char *cldevicegroup_get_owners(scstat_ds_t *dgstat, int level);
clerrno_t cldevicegroup_get_multi_svm_status(scconf_cfg_ds_t *dgp,
    ClStatus *cldg_multidg_obj, char *dsname);

//
// global data
//
bool displayMultiSvm = false;

//
// "status" subcommand implementation.
//
// Displays the status information for the device groups
// in various tables. The following tables will be shown
// In non-verbose mode :
//	Servers (primary and secondary), Status
// In verbose mode :
//	Servers, Spare Nodes, Inactive nodes, In Transistion nodes,
//	Status
//
// Possible return values :
//
//	CL_EINTERNAL	- Bad call or internal error
//	CL_ENOMEM	- Insufficient memory or other os resources
//	CL_EINVAL	- Invalid input
//	CL_ETYPE	- Invalid device group type
//	CL_ENOENT	- Invalid device group
//
clerrno_t
cldevicegroup_status(ValueList dgtypes, ValueList nodes,
    ValueList operands, optflgs_t optflags)
{
	//
	// Status objects for various tables.
	//
	ClStatus *status_obj = new ClStatus(optflags & vflg ? true : false);
	ClStatus *addnl_status_obj = new ClStatus(
	    optflags & vflg ? true : false);
	ClStatus *multidg_status_obj = new ClStatus(
	    optflags & vflg ? true : false);

	clerrno_t ret_code = CL_NOERR;
	clerrno_t first_err = CL_NOERR;
	scconf_errno_t scconferr = SCCONF_NOERR;
	int exitstatus;

	// Valid nodes ?
	ret_code = cldevicegroup_verify_nodes(nodes);
	if (ret_code != CL_NOERR) {
		if (first_err == CL_NOERR) {
			first_err = ret_code;
		}
	}

	// Get the status objects
	ret_code = cldevicegroup_get_filtered_statusobjs(operands, nodes,
	    dgtypes, optflags, status_obj, addnl_status_obj,
	    multidg_status_obj);

	if (ret_code != CL_NOERR) {
		if (first_err == CL_NOERR) {
			first_err = ret_code;
		}
	}

	// print the device group status information
	status_obj->print();

	if (displayMultiSvm)
		multidg_status_obj->print();
	if (optflags & vflg)
		addnl_status_obj->print();

cleanup:
	delete status_obj;
	delete addnl_status_obj;
	delete multidg_status_obj;

	return (first_err);
}

//
// get_cldevicegroup_status_obj
//
// Gets the status object containing the status
// of all device groups. Both Standard status info
// like primary server/ secondary server and additional
// status info like Spares nodes/ Transistion nodes etc
// is filled in respective objects. The memory for these
// objects should be pre-allocated.
//
clerrno_t
get_cldevicegroup_status_obj(optflgs_t optflgs, ClStatus *basic_status_obj,
    ClStatus *addnl_status_obj, ClStatus *multidg_status_obj)
{
	ValueList empty_list;

	if (basic_status_obj == NULL ||
	    addnl_status_obj == NULL ||
	    multidg_status_obj == NULL) {
		clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}

	// Return the objects
	return (cldevicegroup_get_filtered_statusobjs(empty_list, empty_list,
	    empty_list, optflgs, basic_status_obj, addnl_status_obj,
	    multidg_status_obj));
}

//
// cldevicegroup_get_filtered_statusobjs
//
// Returns the various status objects corresponding to
// different status tables that need to be displayed
//
// Return Values
//	CL_NOERR	- Success
//	CL_ENOMEM	- Memory allocation failed
//	CL_EINVAL	- Invalid options
//	CL_EINTERNAL	- Internal error
//
clerrno_t
cldevicegroup_get_filtered_statusobjs(ValueList operands,
    ValueList nodes, ValueList dgtypes, optflgs_t optflags,
    ClStatus *cldg_status_obj, ClStatus *cldg_addnl_obj,
    ClStatus *cldg_multidg_obj)
{

	clerrno_t ret_code = CL_NOERR;
	clerrno_t first_err = CL_NOERR;
	ValueList devicegroups;
	scconf_errno_t scconferr = SCCONF_NOERR;
	scconf_cfg_ds_t *dsconfig = NULL;
	scconf_cfg_ds_t *device_group = NULL;
	char *current_dg_name = NULL;
	static scconf_cfg_cluster_t *pcluster_cfg = NULL;
	scstat_ds_t *ldgstat = NULL;
	scstat_ds_t *dgstat;
	scstat_errno_t error = SCSTAT_ENOERR;
	char *prim_buffer = NULL;
	char *sec_buffer = NULL;
	char *spares_buffer = NULL;
	char *inactive_buffer = NULL;
	char *intransit_buffer = NULL;


	//
	// Get the list of devicegroup that need to be show.
	// Not all operands will necessary be shown as user
	// can specify type filter with -t and -n options.
	//
	ret_code = cldevicegroup_filter_operands(operands, devicegroups,
	    dgtypes, nodes, optflags & vflg);
	if (ret_code != CL_NOERR) {
		if (first_err == CL_NOERR) {
			first_err = ret_code;
		}
	}

	// Get the copy of device group configuration
	scconferr = scconf_get_ds_config(NULL, SCCONF_BY_NAME_FLAG, &dsconfig);
	if (scconferr != SCCONF_NOERR || dsconfig == NULL) {
		// unable to retrieve configuration
		ret_code = cldevicegroup_map_scconf_to_cldg(scconferr);
		if (first_err == CL_NOERR) {
			first_err = ret_code;
		}
		goto cleanup;
	}

	// Get the cluster config
	scconferr = scconf_get_clusterconfig(&pcluster_cfg);
	if (scconferr != SCCONF_NOERR) {
		ret_code = cldevicegroup_map_scconf_to_cldg(scconferr);
		goto cleanup;
	}

	/* get status for device groups in the cluster */
	error = scstat_get_ds_status_with_cached_config(pcluster_cfg, NULL,
	    &ldgstat);
	if (error != SCSTAT_ENOERR) {
		clcommand_perror(CL_EINTERNAL);
		if (first_err == CL_NOERR) {
			first_err = CL_EINTERNAL;
		}
		goto cleanup;
	}

	/* Servers and status table */
	cldg_status_obj->setHeadingTitle(HEAD_DEVICEGROUP);
	cldg_status_obj->setSectionTitle(SECTION_DEVICEGROUPS);
	cldg_status_obj->setHeadings(X_CLDG_NAME, X_CLDG_PRIMARY,
	    X_CLDG_SECONDARY, X_CLDG_STATUS);

	for (dgstat = ldgstat;  dgstat;  dgstat = dgstat->scstat_ds_next) {
		// Ignore the device groups that do not pass filter
		if (!devicegroups.isValue(dgstat->scstat_ds_name))
			continue;

		// Ignore multi-owner. will be listed in seperate table
		if (scconf_is_local_device_service(dgstat->scstat_ds_name))
			continue;

		// Get the list of primaries
		prim_buffer = cldevicegroup_get_owners(dgstat,
		    CLDG_LEVEL_PRIM);

		// Get the list of secondaries
		sec_buffer = cldevicegroup_get_owners(dgstat,
		    CLDG_LEVEL_SEC);

		// Print a "-" if these values are empty
		if (prim_buffer && *prim_buffer == '\0')
			prim_buffer = strdup("-");
		if (sec_buffer && *sec_buffer == '\0')
			sec_buffer = strdup("-");

		if (prim_buffer == NULL || sec_buffer == NULL) {
			clcommand_perror(CL_ENOMEM);
			if (first_err == CL_NOERR) {
				first_err = CL_ENOMEM;
			}
			goto cleanup;
		}

		// Add this row to table
		cldg_status_obj->addRow(dgstat->scstat_ds_name, prim_buffer,
		    sec_buffer, dgstat->scstat_ds_statstr);

		// Free buffer for next dg
		if (prim_buffer) {
			free(prim_buffer);
			prim_buffer = NULL;
		}
		if (sec_buffer) {
			free(sec_buffer);
			sec_buffer = NULL;
		}
	}

	/* Status information about multi-owner dg */
	cldg_multidg_obj->setSectionTitle(SECTION_DG_MULTI_SVM);
	cldg_multidg_obj->setHeadings(X_CLDG_NAME, X_CLDG_NODE_NAME,
	    X_CLDG_STATUS);
	cldg_multidg_obj->enableRowGrouping(true);

	for (dgstat = ldgstat;  dgstat;  dgstat = dgstat->scstat_ds_next) {
		// Ignore the device groups that do not pass filter
		if (!devicegroups.isValue(dgstat->scstat_ds_name))
			continue;

		// Ignore if this is non multi-owner dg
		if (!scconf_is_local_device_service(dgstat->scstat_ds_name))
			continue;

		// We atleast found one multi-owner dg. The table can
		// be displayed
		displayMultiSvm = true;

		// Fill online status for this dg
		ret_code = cldevicegroup_get_multi_svm_status(dsconfig,
		    cldg_multidg_obj, dgstat->scstat_ds_name);

		if (ret_code != CL_NOERR) {
			if (first_err == CL_NOERR) {
				first_err = ret_code;
			}
			goto cleanup;
		}
	}

	/* Additional Status information. displayed incase of verbose mode */
	cldg_addnl_obj->setSectionTitle(SECTION_DG_S_I_ITN);
	cldg_addnl_obj->setHeadings(X_CLDG_NAME, X_CLDG_SPARE,
	    X_CLDG_INACTIVE, X_CLDG_TRANSIT);

	for (dgstat = ldgstat;  dgstat;  dgstat = dgstat->scstat_ds_next) {

		// Ignore the device groups that do not pass filter
		if (!devicegroups.isValue(dgstat->scstat_ds_name))
			continue;

		// Ignore multi-owner.
		if (scconf_is_local_device_service(dgstat->scstat_ds_name))
			continue;

		// Get the list of Spares
		spares_buffer = cldevicegroup_get_owners(dgstat,
		    CLDG_LEVEL_SPARE);

		// Get the list of Inactive nodes
		inactive_buffer = cldevicegroup_get_owners(dgstat,
		    CLDG_LEVEL_INACTIVE);

		// Get the list of In transistion nodes
		intransit_buffer = cldevicegroup_get_owners(dgstat,
		    CLDG_LEVEL_TRANSIT);

		// Display "-" if this values are empty
		if (spares_buffer && *spares_buffer == '\0')
			spares_buffer = strdup("-");
		if (inactive_buffer && *inactive_buffer == '\0')
			inactive_buffer = strdup("-");
		if (intransit_buffer && *intransit_buffer == '\0')
			intransit_buffer = strdup("-");

		if (spares_buffer == NULL ||
		    inactive_buffer  == NULL ||
		    intransit_buffer == NULL) {
			clcommand_perror(CL_ENOMEM);
			if (first_err == CL_NOERR) {
				first_err = CL_ENOMEM;
			}
			goto cleanup;
		}

		cldg_addnl_obj->addRow(dgstat->scstat_ds_name, spares_buffer,
		    inactive_buffer, intransit_buffer);

		// Free buffers for next dg
		if (spares_buffer)
			free(spares_buffer);
		if (inactive_buffer)
			free(inactive_buffer);
		if (intransit_buffer)
			free(intransit_buffer);
	}

cleanup:
	scconf_free_ds_config(dsconfig);

	/* Temporary buffers */
	if (prim_buffer)
		free(prim_buffer);
	if (sec_buffer)
		free(sec_buffer);
	if (spares_buffer)
		free(spares_buffer);
	if (inactive_buffer)
		free(inactive_buffer);
	if (intransit_buffer)
		free(intransit_buffer);

	if (ldgstat)
		scstat_free_ds_status(ldgstat);
	return (ret_code);
}

//
// cldevicegroup_get_owners
//
// Returns the string that contains the list
// of owners (primaries/secondaries/...).
// The level parameter determines the kind of
// owners that caller is looking for
//
//	CLDG_LEVEL_PRIM		- Primary level
//	CLDG_LEVEL_SEC		- Secondary
//	CLDG_LEVEL_SPARE	- Spare
//	CLDG_LEVEL_INACTIVE	- Inactive
//	CLDG_LEVEL_TRANSIT	- In transistion
//
// Returns empty string if the list is empty
//
char *
cldevicegroup_get_owners(scstat_ds_t *dgstat, int level)
{
	scstat_ds_node_state_t *node_state;
	char buffer[BUFSIZE];

	if (dgstat == NULL)
		return (NULL);

	bzero(buffer, sizeof (buffer));
	/* Add matching nodes to buffer */
	for (node_state = dgstat->scstat_node_state_list;  node_state;
	    node_state = node_state->scstat_node_next) {

		/* Matcing levels? */
		if (level == node_state->scstat_node_state) {
			/* check name */
			if (node_state->scstat_node_name == NULL)
				continue;
			if (*buffer != '\0')
				strcat(buffer, ",");
			strcat(buffer, node_state->scstat_node_name);
		}
	}

	return (strdup(buffer));
}

//
// cldevicegroup_get_multi_svm_status
//
// Adds rows to cldg_multidg_obj status object
// with status on each configured node.
//
// Return values
//	CL_NOERR	- Success
//	CL_EINTERNAL	- Unexpected
//
clerrno_t cldevicegroup_get_multi_svm_status(scconf_cfg_ds_t *dgp,
    ClStatus *cldg_multidg_obj, char *dsname)
{
	scconf_nodeid_t *nodeidp;
	uint_t member = 0;
	dc_error_t dc_err = DCS_SUCCESS;
	scconf_errno_t rval = SCCONF_NOERR;
	char *namep = NULL;
	int first_node = 1;

	// valid call ?
	if (dgp == NULL || dsname == NULL) {
		return (CL_EINTERNAL);
	}

	// Jump to interested dg
	while (dgp) {
		if (dgp->scconf_ds_name &&
		    strcmp(dgp->scconf_ds_name, dsname) == 0)
			break;

		dgp = dgp->scconf_ds_next;
	}

	if (!dgp)
		return (CL_EINTERNAL);	// Not found

	for (nodeidp = dgp->scconf_ds_nodelist; *nodeidp; nodeidp++) {

		if (nodeidp == NULL)
			return (CL_EINTERNAL);

		dc_err = dcs_get_multiowner_status(*nodeidp, &member);

		if (dc_err != DCS_SUCCESS)
			return (CL_EINTERNAL);

		rval = scconf_get_nodename(*nodeidp, &namep);
		if (rval != SCCONF_NOERR || namep == NULL)
			return (CL_EINTERNAL);

		/* If it is a member online else offline */
		if (!member) {
			if (first_node == 1) {
				cldg_multidg_obj->addRow(dsname,
				    namep, "Offline");
				first_node = 0;
			} else {
				cldg_multidg_obj->addRow("", namep, "Offline");
			}
		} else {
			if (first_node == 1) {
				cldg_multidg_obj->addRow(dsname,
				    namep, "Online");
				first_node = 0;
			} else {
				cldg_multidg_obj->addRow("", namep, "Online");
			}
		}

		if (namep)
			free(namep);

		member = 0;
	}

	return (CL_NOERR);
}
