//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
#pragma ident	"@(#)cldevicegroup_list.cc	1.4	08/09/17 SMI"

#include "cldevicegroup_private.h"
#include <libdid.h>

//
// "list" subcommand implementation.
//
// Possible return values :
//
//	CL_EINTERNAL	- Bad call or internal error
//	CL_ENOMEM	- Insufficient memory or other os resources
//	CL_EINVAL	- Invalid input
//	CL_ETYPE	- Invalid device group type
//	CL_ENOENT	- Invalid device group
//
clerrno_t
cldevicegroup_list(ValueList dgtypes, ValueList nodes,
    ValueList operands, optflgs_t optflags)
{
	ClList print_list = ClList(optflags & vflg ? true : false);
	clerrno_t ret_code = CL_NOERR;
	clerrno_t first_err = CL_NOERR;
	ValueList devicegroups;
	scconf_errno_t scconferr = SCCONF_NOERR;
	unsigned int ismember;
	int libinit, exitstatus;
	scconf_cfg_ds_t *dsconfig = NULL;
	scconf_cfg_ds_t *device_group = NULL;
	char *current_dg_name = NULL;
	char *current_dg_type = NULL;
	char *current_dg_nlist = NULL;

	// Valid nodes ?
	if (nodes.getSize() != 0) {
		ret_code = cldevicegroup_verify_nodes(nodes);
		if (ret_code != CL_NOERR) {
			if (first_err == CL_NOERR) {
				first_err = ret_code;
			}
		}
	}

	/* Libdid intialisation */
	libinit = did_initlibrary(1, DID_NONE, NULL);
	if (libinit != DID_INIT_SUCCESS) {
		if (first_err == CL_NOERR) {
			first_err = CL_EINTERNAL;
		}
		goto cleanup;
	}

	//
	// Get the list of devicegroup that need to be listed.
	// Not all operands will necessary be listed as user
	// can specify type filter with -t and -n options.
	//
	ret_code = cldevicegroup_filter_operands(operands, devicegroups,
	    dgtypes, nodes, optflags & vflg);
	if (ret_code != CL_NOERR) {
		if (first_err == CL_NOERR) {
			first_err = ret_code;
		}
	}


	// Get the copy of device group configuration
	scconferr = scconf_get_ds_config(NULL, SCCONF_BY_NAME_FLAG, &dsconfig);
	if (scconferr != SCCONF_NOERR || dsconfig == NULL) {
		// unable to retrieve configuration
		ret_code = cldevicegroup_map_scconf_to_cldg(scconferr);
		if (first_err == CL_NOERR) {
			first_err = ret_code;
		}
		goto cleanup;
	}

	// Add header if verbose mode
	if (optflags & vflg) {
		print_list.setHeadings((char *)gettext("Device group"),
		    (char *)gettext("Type"), (char *)gettext("Node list"));
	}

	// Add rows to list
	for (device_group = dsconfig; device_group;
	    device_group = device_group->scconf_ds_next) {
		if (!devicegroups.isValue(device_group->scconf_ds_name))
			continue;
		current_dg_name = device_group->scconf_ds_name;
		current_dg_type = device_group->scconf_ds_type;
		current_dg_nlist = cldevicegroup_get_nodelist(
		    device_group->scconf_ds_nodelist);
		if (!current_dg_name || !current_dg_type ||
		    !current_dg_nlist) {
			ret_code = CL_EINTERNAL;
			if (first_err == CL_NOERR) {
				first_err = ret_code;
			}

			goto cleanup;
		}

		// In verbose mode, print name,type and nodelist
		// else, just list the name
		if (optflags & vflg) {
			print_list.addRow(current_dg_name, current_dg_type,
			    current_dg_nlist);
		} else {
			print_list.addRow(current_dg_name);
		}

		// Free the nodelist buffer
		if (current_dg_nlist) {
			free(current_dg_nlist);
			current_dg_nlist = NULL;
		}
	}

	// Print
	print_list.print();

cleanup:
	scconf_free_ds_config(dsconfig);
	return (first_err);
}
