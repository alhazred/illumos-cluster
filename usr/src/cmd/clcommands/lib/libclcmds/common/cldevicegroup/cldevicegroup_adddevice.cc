//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)cldevicegroup_adddevice.cc	1.4	08/05/20 SMI"

#include "cldevicegroup_private.h"

clerrno_t cldevicegroup_print_adddevice_errors(scconf_errno_t scconf_err,
    char *dsname, int verbose);

//
// "add-device" sub command implementation for cldevicegroup command
// Expects that all syntax validations are already properly done.
// Relies on scconf_add_ds_vers2 api from libscconf to add devices to
// the device group.
//
// Possible return values :
//
//	CL_EINTERNAL	- Bad call or internal error
//	CL_ENOMEM	- Insufficient memory or other os resources
//	CL_ENOCLUSTER	- Not in cluster
//	CL_EINVAL	- Invalid input
//
clerrno_t
cldevicegroup_adddevice(ValueList devices, ValueList operands,
    optflgs_t optflgs)
{
	// Args required for scconf_add_ds_vers2
	char *dsname = NULL;
	char **dsnodes = NULL;
	char *dsoptions = NULL;
	char *messages = NULL;
	scconf_state_t dspreference = SCCONF_STATE_UNCHANGED;
	scconf_state_t dsfailback = SCCONF_STATE_DISABLED;
	unsigned int dsnumsecondaries = SCCONF_NUMSECONDARIES_UNSET;

	scconf_errno_t scconferr = SCCONF_NOERR;
	clerrno_t ret_code = CL_NOERR;
	unsigned int ismember;
	ValueList nodes;
	NameValueList properties;
	int exitstatus;

	// Add-device can be called to add atleast one device to only
	// one device group.
	if (devices.getSize() < 1 || operands.getSize() != 1) {
		clcommand_perror(CL_EINTERNAL);
		ret_code = CL_EINTERNAL;
		goto cleanup;
	}

	// Convert parameters
	operands.start();
	dsname = operands.getValue();

	// Operation not supported for svm dg
	ret_code = cldevicegroup_verify_svm(dsname);
	if (ret_code != CL_NOERR) {
		clerror("Addition of specified devices to "
		    "device group \"%s\" failed.\n", dsname);
		goto cleanup;
	}

	ret_code = cldevicegroup_conv_scconf_params(CLDG_RAWDISK_TYPE,
	    nodes, devices, properties, NULL, NULL, NULL, &dsoptions, NULL);
	if (ret_code != CL_NOERR)
		goto cleanup;

	// Call the scconf api
	scconferr = scconf_add_ds_vers2(CLDG_RAWDISK_TYPE, dsname, dsnodes,
	    dspreference, NULL, dsfailback, NULL, dsoptions, dsnumsecondaries,
	    &messages);

	(void) clcommand_dumpmessages(messages);
	ret_code = cldevicegroup_print_adddevice_errors(scconferr, dsname,
	    optflgs & vflg);

cleanup:
	if (dsoptions)
		free(dsoptions);

	return (ret_code);
}

//
// cldevicgroup_print_adddevice_errors
//
// Prints the error messages specific to "add-device" subcommand.
// for generic error messages, relies on clcommand_strerr function.
// maps the scconf error to new cli error code and returns the same.
//
clerrno_t
cldevicegroup_print_adddevice_errors(scconf_errno_t scconf_err, char *dsname,
    int verbose)
{
	char err_buffer[BUFSIZ];
	clerrno_t cldg_code;

	switch (scconf_err) {
	case SCCONF_ENOEXIST:
		// Some component is not found or unknown
		clerror("The device you are trying to add is not found "
		    "or is unknown.\n");
		cldg_code = CL_ENOENT;
		break;

	case SCCONF_EINUSE:
		// Already in use
		clerror("The specified device is already in use in some other "
		    "device group.\n");
		cldg_code = CL_EACCESS;
		break;

	case SCCONF_EBUSY:
		// Device group is busy
		clerror("The device group is in a busy state.\n");
		cldg_code = CL_EBUSY;
		break;

	case SCCONF_EINVAL:
		// Invalid option or input given
		clerror("Bad options or input is provided.\n");
		cldg_code = CL_EINVAL;
		break;

	case SCCONF_EUNKNOWN:
		// Device group type is unknown
		clerror("Unknown device group type is given.\n");
		cldg_code = CL_ETYPE;
		break;

	case SCCONF_DS_EINVAL:
		// Inconsistencies in device configuration
		clerror("Inconsistencies are detected in device "
		    "configuration\n");
		cldg_code = CL_ESTATE;
		break;

	case SCCONF_ESETUP:
		// Setup problem
		clerror("The volume manager or other device management "
		    "software was not installed or setup properly.\n");
		cldg_code = CL_ESTATE;
		break;

	default:
		// Generic error messages
		cldg_code = cldevicegroup_map_scconf_to_cldg(scconf_err);
		break;
	}
	if (cldg_code != CL_NOERR) {
		clerror("Addition of specified devices to "
		    "device group \"%s\" failed.\n", dsname);
	}

	if (cldg_code == CL_NOERR && verbose) {
		clmessage("Successfully added devices to the device group "
		    "\"%s\".\n", dsname);
	}

	return (cldg_code);
}
