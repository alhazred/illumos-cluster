//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)cldevice_show.cc	1.4	08/05/21 SMI"

//
// Process cldevice "show"
//

#include "cldevice.h"

clerrno_t
get_cldevice_show_obj(optflgs_t optflgs, ClShow *show_obj)
{
	ValueList empty;
	return (get_cldevice_filtered_show_obj(
	    show_obj, empty, empty, false));
}

clerrno_t
get_cldevice_filtered_show_obj(ClShow *show_obj,
    ValueList &devlist, ValueList &nodelist, bool verbose)
{
	int err = 0;
	int first_err = 0;

	if (show_obj == NULL)
		return (CL_EINTERNAL);

	show_obj->propSet = false;

	// Validate the nodelist passed in
	if (nodelist.getSize() != 0) {
		err = verify_nodelist(nodelist);
		if (err) {
			first_err = err;
		}

		if (nodelist.getSize() == 0)
			return (first_err);
	}

	// Validate the device list
	if (devlist.getSize() != 0) {
		err = convert_operands_to_instances(devlist);
		if (err && !first_err) {
			first_err = err;
		}

		if (devlist.getSize() == 0)
			return (first_err);
	}

	// build the command string
	char cmdbuf[BUFSIZ];
	(void) sprintf(cmdbuf, "/usr/sbin/didadm -L -o fullname,fullpath,"
	    "replication,defaultfencing");
	if (verbose) {
		(void) sprintf(cmdbuf, "%s,diskid,asciidiskid", cmdbuf);
	}

	// Add the operands to the list
	for (devlist.start();
	    !devlist.end();
	    devlist.next()) {
		(void) sprintf(cmdbuf, "%s %s", cmdbuf,
		    devlist.getValue());
	}

	// Add a sed filter for the nodes
	if (nodelist.getSize() > 0) {
		(void) sprintf(cmdbuf, "%s | sed ", cmdbuf);
		for (nodelist.start(); !nodelist.end(); nodelist.next()) {
			(void) sprintf(cmdbuf, "%s -e '/%s/b'",
			    cmdbuf, nodelist.getValue());
		}
		(void) sprintf(cmdbuf, "%s -e d", cmdbuf);
	}

	FILE *file = NULL;
	err = pipe_cmd(&file, cmdbuf);

	if (err) {
		if (!first_err)
			first_err = err;
		return (first_err);
	}

	err = parse_didadm_output(show_obj, file, verbose);
	if (err && !first_err)
		first_err = err;

	return (CL_NOERR);
}

clerrno_t
parse_didadm_output(ClShow *show_obj, FILE *file, bool verbose)
{
	if (!show_obj || !file)
		return (CL_EINTERNAL);

	int err = CL_NOERR;

	// Got the pipe with all the devices which were found,
	// now parse the output and put in a ClShow object
	char name[128];
	name[0] = '\0';
	char temp[256];
	temp[0] = '\0';

	int path = 0;
	int repl = 0;
	int fencing = 0;
	int did = 0;

	bool is_dup = false;

	while (fscanf(file, "%s", temp) != EOF) {
		if (strcmp(name, "") == 0) {
			(void) sprintf(name, temp);
			if (!show_obj->objectExists(name)) {
				show_obj->addObject(
				    (char *)gettext("DID Device Name"),
				    (char *)name);
			} else
				is_dup = true;

		} else if (!path) {
			show_obj->addPropertyFirst((char *)name,
			    (char *)gettext("Full Device Path"),
			    (char *)temp);
			path = 1;

		} else if (!repl) {
			if (!is_dup) {
				show_obj->addProperty((char *)name,
				    (char *)gettext("Replication"),
				    (char *)temp);
			}
			repl = 1;

		} else if (!fencing) {
			if (strcasecmp(temp, "use_pathcount") == 0)
				(void) sprintf(temp, "pathcount");
			else if (strcasecmp(temp, "use_scsi3") == 0)
				(void) sprintf(temp, "scsi3");
			else if (strcasecmp(temp, "use_global") == 0)
				(void) sprintf(temp, "global");
			else if (strcasecmp(temp, "no_fencing") == 0)
				(void) sprintf(temp, "nofencing");
			else {
				clerror("Unknown default_fencing value: "
				    "\"%s\".\n", temp);
				err = CL_EINTERNAL;
			}

			if (!is_dup) {
				show_obj->addProperty((char *)name,
				    (char *)"default_fencing",
				    (char *)temp);
			}
			fencing = 1;

			if (!verbose) {
				name[0] = '\0';
				path = 0;
				repl = 0;
				fencing = 0;
				is_dup = false;
			}

		} else if (verbose && !did) {
			if (!is_dup) {
				show_obj->addProperty((char *)name,
				    (char *)gettext("Disk ID"),
				    (char *)temp);
			}
			did = 1;

		} else if (verbose) {
			// Ascii Disk ID can be more than one string,
			// read in until new line found.
			char c;
			int count = strlen(temp);
			while ((c = fgetc(file)) != EOF) {
				if (c == '\n') {
					temp[count] = '\0';
					break;
				}
				temp[count++] = c;
			}

			if (feof(file)) {
				clerror("Unexpected end of file.\n");
				err = CL_EINTERNAL;
			}

			if (!is_dup) {
				show_obj->addProperty((char *)name,
				    (char *)gettext("Ascii Disk ID"),
				    (char *)temp);
			}

			name[0] = '\0';
			path = 0;
			repl = 0;
			fencing = 0;
			did = 0;
			is_dup = false;
		}

		temp[0] = '\0';
	}

	return (err);
}

clerrno_t
cldevice_show(ClCommand &cmd, ValueList &operands, ValueList &nodes)
{
	int err = CL_NOERR;

	ClShow *show = new ClShow(HEAD_DID);
	err = get_cldevice_filtered_show_obj(show, operands, nodes,
	    cmd.isVerbose);

	// Print only if there is something to print
	if (show->getObjectSize()) {
		// print it
		show->print();
	}

	return (err);
}
