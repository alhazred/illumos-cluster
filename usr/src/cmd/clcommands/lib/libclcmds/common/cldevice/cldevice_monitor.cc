//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)cldevice_monitor.cc	1.2	08/05/20 SMI"

//
// Process cldevice "monitor"
//

#include "cldevice.h"

clerrno_t
cldevice_monitor(ClCommand &cmd, ValueList &operands, ValueList &nodes)
{
	int err = CL_NOERR;
	int first_err = err;

	// Validate the nodelist passed in
	if (nodes.getSize() != 0) {
		err = verify_nodelist(nodes);
		if (err) {
			// Set first error
			first_err = err;
		}

		if (nodes.getSize() == 0) {
			return (first_err);
		}
	}

	// Set the special env variable so the command
	// knows to use newcli style error codes and
	// messages
	err = set_cldevice_env();
	if (err)
		return (err);

	// build the command string
	char cmdstr[] = "/usr/cluster/bin/scdpm -m";

	if (nodes.getSize() > 0) {
		for (nodes.start(); !nodes.end(); nodes.next()) {
			char args[BUFSIZ];
			(void) sprintf(args, "%s %s", cmdstr, nodes.getValue());

			// Wildcard
			if (operands.getSize() == 0) {
				(void) sprintf(args, "%s:all", args);
				err = exec_cmd(args);

				if (!err && cmd.isVerbose) {
					clmessage("Successfully monitored all "
					    "disk paths on node %s.\n",
					    nodes.getValue());
				}

				if (err && !first_err)
					first_err = err;
			}

			// Operand list
			for (operands.start();
			    !operands.end();
			    operands.next()) {
				char opargs[BUFSIZ];
				(void) sprintf(opargs, "%s:%s", args,
				    operands.getValue());

				err = exec_cmd(opargs);

				if (!err && cmd.isVerbose) {
					clmessage("Successfully monitored "
					    "disk path \"%s:%s\".\n",
					    nodes.getValue(),
					    operands.getValue());
				}

				if (err && !first_err) {
					first_err = err;
				}
			}
		}
	} else {

		// Wildcard
		if (operands.getSize() == 0) {
			char args[64];
			(void) sprintf(args, "%s all", cmdstr);

			err = exec_cmd(args);

			if (!err && cmd.isVerbose) {
				clmessage("Successfully monitored all "
				    "disk paths.\n");
			}
			if (err && !first_err) {
				first_err = err;
			}
		}

		// Operand list
		for (operands.start();
		    !operands.end();
		    operands.next()) {

			char opargs[BUFSIZ];
			(void) sprintf(opargs, "%s %s", cmdstr,
			    operands.getValue());

			err = exec_cmd(opargs);

			if (!err && cmd.isVerbose) {
				clmessage("Successfully monitored disk "
				    "path \"%s\".\n", operands.getValue());
			}
			if (err && !first_err) {
				first_err = err;
			}
		}
	}

	return (first_err);
}

clerrno_t
cldevice_monitor(ClCommand &cmd, ValueList &operands,
    ValueList &nodes, char *clconfig)
{
	int err = CL_NOERR;
	int first_err = err;
	ClXml xml;

	OptionValues *ov = new OptionValues("monitor");
	if (ov == NULL) {
		first_err = CL_ENOMEM;
		goto cleanup;
	}

	// No XML file
	if (!clconfig) {
		first_err = CL_EINTERNAL;
		goto cleanup;
	}

	// Nodes
	ov->addOptions(CLDEVICE_NODE, &nodes);

	// Verbose?
	if (cmd.isVerbose)
		ov->addOption(VERBOSE);

	// Wildcard operand
	if (operands.getSize() == 0)
		operands.add((char *)CLCOMMANDS_WILD_OPERAND);

	// Apply config for each operand
	for (operands.start();
	    !operands.end();
	    operands.next()) {

		ov->setOperand(operands.getValue());
		err = xml.applyConfig(CLDEVICE_CMD, clconfig, ov);
		if (err && !first_err)
			first_err = err;
	}

cleanup:
	delete ov;

	return (first_err);
}
