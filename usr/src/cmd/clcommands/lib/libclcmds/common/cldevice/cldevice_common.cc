//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)cldevice_common.cc	1.5	08/05/20 SMI"

//
// Common functions for the cldevice command
//

#include "cldevice.h"

#define	SCRCMD		"/usr/cluster/lib/sc/scrcmd"

//
// set_cldevice_env()
//
// Sets an environment variable which commands such as
// scdidadm and scdpm check to determine which type of
// error printing and exit values to return.
//
// The name of the enviorment variable is defined by
// CLCMD_ENV in didadm.h.  The value of the variable is
// used by those commands to set the command name when
// printing errors, otherwise it will use argv[0].
//
// If putenv fails, it can only be because it ran out of
// memory.
//
clerrno_t
set_cldevice_env()
{
	char *env = new char[128];
	(void) sprintf(env, "%s=%s", CLCMD_ENV,
	    commandInstance.getCommandName());
	if (putenv(env) != 0)
		return (CL_ENOMEM);
	else
		return (CL_NOERR);
}

//
// run_cmd_on_node(char *cmd, char *args, char *node)
//
// Run a command given by "cmd" with "args" on node "node".
// Check whether the command is to be executed in the local-node
// or remote-node. If it is a local node call the scrcmd command
// without -N option. This causes the the command is executed on
// the local node itself. The command is executed with scrcmd.
// The command must be defined in scrcmd.h, or else it will fail.
//
clerrno_t
run_cmd_on_node(char *cmd, char *args, char *node)
{
	scconf_nodeid_t local_nodeid;
	scconf_nodeid_t nodeid;
	char *nodename = NULL;

	if (!cmd || !args || !node) {
		return (CL_EINTERNAL);
	}

	// Set the special env variable so the command
	// knows to use newcli style error codes and
	// messages
	int err = set_cldevice_env();
	if (err)
		return (err);

	//
	// Check Whether the node on which the command is to be run
	// is a local node or remote node.
	//
	if (scconf_get_nodeid(NULL, &local_nodeid))
		return (CL_EINTERNAL);
	if (scconf_get_nodeid(node, &nodeid))
		return (CL_EINTERNAL);
	if (local_nodeid != nodeid) {
		nodename = node;
	}

	if (nodename) {
		char *argv[6];
		argv[0] = SCRCMD;
		argv[1] = "-N";
		argv[2] = node;
		argv[3] = cmd;
		argv[4] = args;
		argv[5] = NULL;

		err = exec_cmd(argv);
	} else {
		char *argv[5];
		argv[0] = SCRCMD;
		argv[1] = "-S";
		argv[2] = cmd;
		argv[3] = args;
		argv[4] = NULL;

		err = exec_cmd(argv);
	}

	return (err);
}

//
// run_scgdevs()
//
// Executes the scgdevs command with the "-g" option.
// Returns the exit code from the scgdevs command.
//
clerrno_t
run_scgdevs()
{
	// Set the special env variable so the command
	// knows to use newcli style error codes and
	// messages
	int err = set_cldevice_env();
	if (err)
		return (err);

	char *argv[2];
	argv[0] = SCGDEVS;
	argv[1] = NULL;

	err = exec_cmd(argv);

	return (err);
}

//
// exec_cmd(char **argv)
//
// Exec a command using the argument array **argv passed in.
// The exec() function requires a NULL terminated string array,
// If your command is one string, try using the other exec_cmd
// function which converts the string into a NULL terminated
// string array.
//
// Returns the exit value from the exec'd command.
//
clerrno_t
exec_cmd(char **argv)
{
	int stat = 0;

	pid_t pid = fork();
	if (pid == 0) {
		if (execv(argv[0], argv) == -1) {
			clerror("Cannot execute \"%s\" - %s.\n",
			    argv[0], strerror(errno));
			exit(1);
		}
		exit(0);
	} else {
		int status = 0;
		while ((status = waitpid(pid, &stat, 0)) != pid) {
			if (status == -1 && errno == EINTR)
				continue;
			else {
				status = -1;
				break;
			}
		}
		if (status != pid) {
			clerror("Error waiting for command result - %s\n",
			    strerror(errno));
			return (CL_EINTERNAL);
		}
	}

	// Return the exit code
	return (WEXITSTATUS(stat));
}

//
// exec_cmd(char *cmd)
//
// Execs a command string, "cmd".  Returns the exit value
// of the command which was exec'd, or CL_ENOMEM if it could
// allocate the memory to create a string array.
//
clerrno_t
exec_cmd(char *cmd)
{
	if (!cmd)
		return (NULL);

	char **argv = conv_string_to_array(cmd);
	if (!argv)
		return (CL_ENOMEM);

	return (exec_cmd(argv));
}

//
// conv_string_to_array(char *string)
//
// Converts a string into a NULL terminated string array.
// Returns NULL if the memory could not be allocated or
// if a NULL string was passed in.  Otherwise, the new
// string array is returned.
//
char **
conv_string_to_array(char *string)
{
	if (!string)
		return (NULL);

	// Tokenize on spaces
	char *pch = strtok(string, " ");

	// Add each string to the list until there are none left
	ValueList list;
	while (pch != NULL) {
		list.add(pch);
		pch = strtok(NULL, " ");
	}

	int count = 0;
	char **strarray = new char *[list.getSize() + 1];

	// Add each value in the list to the strarray
	for (list.start(); !list.end(); list.next()) {
		strarray[count++] = list.getValue();
	}
	// NULL terminate the last one
	strarray[count] = NULL;

	return (strarray);
}

//
// pipe_cmd(FILE **file, char *cmd)
//
// Opens a pipe for the exec'd command represented by "cmd".
// Caller is responsible for closing the pipe, eg, pclose();
//
// Returns CL_EIO or CL_EINTERNAL on any error, otherwise
// CL_NOERR
//
clerrno_t
pipe_cmd(FILE **file, char *cmd)
{
	if (!cmd) {
		return (CL_EINTERNAL);
	}

	// Set the special env variable so the command
	// knows to use newcli style error codes and
	// messages
	int err = set_cldevice_env();
	if (err)
		return (err);

	*file = NULL;
	*file = popen(cmd, "r");
	if (file == NULL) {
		clerror("Cannot open pipe - %s.\n", strerror(errno));
		switch (errno) {

		case EMFILE:
			return (CL_EIO);
		default:
			return (CL_EINTERNAL);
		}
	} else {
		return (CL_NOERR);
	}
}

//
// device_exists(char *dev, int global, int &err)
//
// Executes the scdidadm command using either;
//	-L if global
//	-l if not global
//
// and the device name, "dev".  If there is no output,
// excluding errors, then the device does not exist.
//
// Returns:
//
//	0:  Device does not exist
//	1:  Device does exist
//	-1: Error
//
int
device_exists(char *dev, int global, int &err)
{
	char cmd[BUFSIZ];
	(void) sprintf(cmd, "/usr/cluster/bin/scdidadm ");
	if (global)
		(void) sprintf(cmd, "%s -L %s ", cmd, dev);
	else
		(void) sprintf(cmd, "%s -l %s ", cmd, dev);

	// filter out errors
	(void) sprintf(cmd, "%s 2>/dev/null", cmd);

	FILE *file = NULL;
	err = pipe_cmd(&file, cmd);

	if (err) {
		clerror("Could not get device information.\n");
		return (-1);
	}

	char c = fgetc(file);

	// If EOF, then it does not exist
	if (c == EOF) {
		(void) pclose(file);
		return (0);
	} else {
		(void) pclose(file);
		return (1);
	}
}

//
// device_exists(char *dev, char *node, int &err)
//
// Executes the scdidadm command plus -L -o host.
// Then greps for the node name which is passed in.
// If there is no output, excluding errors,
// then the device does not exist.
//
// Returns:
//
//	0:  Device does not exist
//	1:  Device does exist
//	-1: Error
//
int
device_exists_on_node(char *dev, char *node, int &err)
{
	if (!dev || !node) {
		err = CL_EINTERNAL;
		clcommand_perror(err);
		return (-1);
	}

	// build the command string
	char cmd[BUFSIZ];
	(void) sprintf(cmd, "/usr/cluster/bin/scdidadm -L -o host %s "
	    "2>/dev/null | grep %s", dev, node);

	FILE *file = NULL;
	err = pipe_cmd(&file, cmd);

	if (err) {
		clerror("Could not get device information.\n");
		return (-1);
	}

	char c = fgetc(file);

	// If EOF, then it does not exist
	if (c == EOF) {
		(void) pclose(file);
		return (0);
	} else {
		(void) pclose(file);
		return (1);
	}
}

//
// get_all_devices(ValueList &devs)
//
// Returns all the devices known to the system by executing
// the scdidadm -L command.  The "devs" ValueList is populated
// with the list of devices retrieved from the command output.
//
clerrno_t
get_all_devices(ValueList &devs)
{
	char cmd[] = "/usr/cluster/bin/scdidadm -L -o name";

	FILE *file = NULL;
	int err = pipe_cmd(&file, cmd);

	if (err) {
		return (err);
	}

	// clear the list
	devs.clear();

	char temp[32];
	temp[0] = '\0';
	while (fscanf(file, "%s", temp) != EOF) {
		devs.add(temp);
		temp[0] = '\0';
	}

	(void) pclose(file);
	return (CL_NOERR);
}

//
// Converts all the operands listed to instance names (eg, d<n>)
// Any invalid operands will be taken care of by didadm itself.
//
// On exit, the operand list will contain only the remaining valid
// instances.
//
clerrno_t
convert_operands_to_instances(ValueList &operands)
{
	int err = CL_NOERR;

	if (operands.getSize() == 0)
		return (err);

	// Build command string
	char cmd[BUFSIZ];
	(void) sprintf(cmd, "/usr/cluster/bin/scdidadm -z -L -o name");

	// Append each operand to the command
	for (operands.start(); !operands.end(); operands.next()) {
		(void) sprintf(cmd, "%s %s", cmd, operands.getValue());
	}

	// Run the command
	FILE *file = NULL;
	err = pipe_cmd(&file, cmd);
	if (err)
		return (err);

	// Create a new list
	ValueList new_ops = ValueList(true);

	// Put the values into the new list
	char temp[32];
	temp[0] = '\0';
	while (fscanf(file, "%s", temp) != EOF) {
		if (strcmp(EXIT_MESSAGE, temp) == 0) {
			fscanf(file, "%d", &err);
		} else {
			new_ops.add(temp);
			temp[0] = '\0';
		}
	}

	// Close the pipe
	(void) pclose(file);

	// Transfer the values from the new list to the old
	operands.clear();
	for (new_ops.start(); !new_ops.end(); new_ops.next()) {
		operands.add(new_ops.getValue());
	}

	return (err);
}

//
// is_device_instance_num(char *name)
//
// Determines if the given string is device instance.
// In order to be a device instance number, it must be of the
// form "n*" or "dn.*".  Any other formats will return an error
//
// Returns:
//
//	-1:	An error
//	0:	Device is not an instance num
//	1:	Device is an instance num
//
int
is_device_instance_num(char *name)
{
	if (!name)
		return (-1);

	// Name begins with a d, strip that off and check the rest.
	char *dev = new char[strlen(name)];
	if (name[0] == 'd') {
		int i;
		for (i = 0; i < strlen(name) - 1; i++)
			dev[i] = name[i+1];
		dev[i] = '\0';
	} else {
		(void) sprintf(dev, name);
	}

	// Verify that there are only number left in the device string
	char nums[] = "0123456789";
	if (strspn(dev, nums) == strlen(dev)) {
		return (1);
	} else
		return (0);
}
