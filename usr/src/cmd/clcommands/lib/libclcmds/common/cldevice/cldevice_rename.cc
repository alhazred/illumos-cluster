//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)cldevice_rename.cc	1.3	08/05/20 SMI"

//
// Process cldevice "rename"
//

#include "cldevice.h"

clerrno_t
cldevice_rename(ClCommand &cmd, char *dev, char *destdev)
{
	if (!dev || !destdev)
		return (CL_EINTERNAL);

	int err = CL_NOERR;
	int first_err = err;

	// Verify that the destdev is of proper format
	switch (is_device_instance_num(destdev)) {
	case 0:
		// Device is not an instance num, didadm will fail.
		clerror("Destination device must be an instance number.\n");
		first_err = CL_EINVAL;
		goto exit;
	case 1:
		break;
	default:
		// Error
		first_err = CL_EINTERNAL;
		clcommand_perror(first_err);
		goto exit;
	}

	if (first_err)
		goto exit;

	// call didadm and let it do the work...
	char cmdbuf[BUFSIZ];
	(void) sprintf(cmdbuf, "/usr/cluster/bin/scdidadm -t %s:%s",
	    dev, destdev);

	err = exec_cmd(cmdbuf);

	if (err && !first_err) {
		first_err = err;
	} else if (cmd.isVerbose) {
		// Print success
		clmessage("Successfully renamed device \"%s\" to \"%s\".\n",
		    dev, destdev);
	}

exit:
	return (first_err);
}
