//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)cldevice_status.cc	1.4	08/05/20 SMI"

//
// Process cldevice "status"
//

#include "cldevice.h"
#include <string>
#include <algorithm>

clerrno_t
get_cldevice_status_obj(optflgs_t opts, ClStatus *status)
{
	ValueList empty;
	return (get_cldevice_filtered_status_obj(
	    status, empty, empty, empty, opts & vflg));
}

clerrno_t
get_cldevice_filtered_status_obj(ClStatus *status, ValueList &operands,
    ValueList &nodes, ValueList &states, bool verbose)
{
	int err = CL_NOERR;
	int first_err = err;
	char errbuf[BUFSIZ];
	char cmdbuf[2*BUFSIZ];
	FILE *file = NULL;
	const int maxbuf = sizeof (cmdbuf);
	int len;		// string length

	//
	// XXX - The states in "dpmStates" below must be synchronized
	//	 with those accepted and displayed by the "scdpm"
	//	 command.
	//
	ValueList dpmStates(true, "ok,fail,unknown,unmonitored", true);
	ValueList filterStates(true);

	status->enableVerbose(verbose);
	status->enableRowGrouping(true);

	// Validate the nodelist passed in
	if (nodes.getSize() != 0) {
		err = verify_nodelist(nodes);
		if (err)
			return (err);
	}

	// Convert all operands to device numbers
	if (operands.getSize() != 0) {
		err = convert_operands_to_instances(operands);
		if (err && !first_err)
			first_err = err;

		if (operands.getSize() == 0)
			goto exit;
	}

	// build the command string
	(void) snprintf(cmdbuf, maxbuf, "/usr/cluster/bin/scdpm -p");

	//
	// Add to command string to limit output for states.
	// We let scdpm do the filtering because it can emit the
	// correct localization for the device states. Need to
	// accept CLI mixed case version of strings for compatibility.
	//
	// Duplicate states are coalesced.
	//
	for (states.start(); !states.end(); states.next()) {
		if (dpmStates.isValue(states.getValue())) {
			std::string s(states.getValue());
			(void) std::transform(s.begin(), s.end(), s.begin(),
			    tolower);
			filterStates.add((char *)s.c_str());
		} else {
			clerror("Unknown state %s specified.\n",
			    states.getValue());
			err = CL_EINVAL;
			break;
		}
	}

	// If there was an error with the states, return now
	if (err) {
		if (!first_err) {
			first_err = err;
		}
		goto exit;
	}

	for (filterStates.start(); !filterStates.end(); filterStates.next()) {
		(void) snprintf(cmdbuf, maxbuf, "%s -s %s", cmdbuf,
		    filterStates.getValue());
	}

	(void) snprintf(cmdbuf, maxbuf, "%s all", cmdbuf);

	// Add a sed filter for the operands
	if (operands.getSize() > 0) {
		(void) sprintf(cmdbuf, "%s | sed ", cmdbuf);
		for (operands.start(); !operands.end(); operands.next()) {
			(void) sprintf(cmdbuf, "%s -e '/%s /b'",
			    cmdbuf, operands.getValue());
		}
		(void) sprintf(cmdbuf, "%s -e d", cmdbuf);
	}

	// Add a sed filter for the nodes
	if (nodes.getSize() > 0) {
		(void) sprintf(cmdbuf, "%s | sed ", cmdbuf);
		for (nodes.start(); !nodes.end(); nodes.next()) {
			(void) sprintf(cmdbuf, "%s -e '/%s/b'",
			    cmdbuf, nodes.getValue());
		}
		(void) sprintf(cmdbuf, "%s -e d", cmdbuf);
	}

	// Apply format changes to sort by device number
	len = snprintf(cmdbuf, maxbuf, "%s | awk -F: '{ print $2\" \"$1 }' | "
	    "awk '{ print $1\" \"$3\" \"$2 }' | "
	    "sed -e '/reboot_on_path_failure/d' | "
	    "sort", cmdbuf);

	if (len > maxbuf) {
		// string too large for allotted space
		err = CL_EINTERNAL;
	} else {
		err = pipe_cmd(&file, cmdbuf);
	}

	if (err) {
		// Set first error
		if (!first_err)
			first_err = err;
	} else {
		// Got the pipe with all the devices which were found,
		// now parse the output and put in a ClStatus object
		char name[32];
		char lastdev[32];
		char node[128];
		char temp[256];
		name[0] = ' ';
		name[1] = '\0';
		lastdev[0] = '\0';
		node[0] = '\0';
		temp[0] = '\0';
		while (fscanf(file, "%s", temp) != EOF) {
			if (strcmp(name, " ") == 0) {
				if (strcmp(lastdev, "") == 0) {
					// Start
					(void) sprintf(lastdev, temp);
					(void) sprintf(name, temp);

				} else if (strcmp(lastdev, temp) != 0) {
					// lastdev != next dev
					(void) sprintf(lastdev, temp);
					(void) sprintf(name, temp);

				} else {
					// lastdev == next dev
					(void) sprintf(name, "");
				}
			} else if (strcmp(node, "") == 0) {
				(void) sprintf(node, temp);

			} else {
				status->addRow(name, node, temp);
				name[0] = ' ';
				name[1] = '\0';
				node[0] = '\0';
				temp[0] = '\0';
			}

			temp[0] = '\0';
		}
	}

exit:
	// Set the headings
	status->setHeadingTitle(HEAD_DEVICE);
	status->setHeadings(
	    (char *)gettext("Device Instance"),
	    (char *)gettext("Node"),
	    (char *)gettext("Status"));

	return (first_err);
}

clerrno_t
cldevice_status(ClCommand &cmd, ValueList &operands, ValueList &nodes,
    ValueList &states)
{
	ClStatus *status = new ClStatus();
	int err = get_cldevice_filtered_status_obj(status, operands,
	    nodes, states, cmd.isVerbose);

	// print it
	status->print();

	return (err);
}
