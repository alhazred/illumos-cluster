//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident   "@(#)cldevice_export.cc	1.3	08/05/20 SMI"

//
// Process cldevice "export"
//

#include "cldevice.h"

#include "ClCommand.h"
#include "ClXml.h"

//
// This function returns a CldeviceExport object populated with
// all of the device information from the cluster.  Caller is
// responsible for releasing the ClcmdExport object.
//
ClcmdExport *
get_cldevice_xml_elements()
{
	ValueList empty;
	CldeviceExport *device = new CldeviceExport();
	(void) get_export_obj(*device, empty, empty);
	return (device);
}

clerrno_t
get_export_obj(CldeviceExport &export_obj, ValueList &operands,
    ValueList &nodes)
{
	int err = CL_NOERR;
	int first_err = err;

	// Validate the nodelist passed in
	if (nodes.getSize() != 0) {
		err = verify_nodelist(nodes);
		if (nodes.getSize() == 0) {
			return (err);
		}
	}

	// Get the device instances
	err = get_device_instances(export_obj, operands, nodes);
	if (err && !first_err)
		first_err = err;

	// Add the device paths
	err = get_device_paths(export_obj, operands, nodes);
	if (err && !first_err)
		first_err = err;

	return (first_err);
}

clerrno_t
get_device_instances(CldeviceExport &export_obj, ValueList &operands,
    ValueList &nodes)
{
	int err = CL_NOERR;
	int first_err = err;
	ValueList new_ops;
	int cmd_length = 0;
	char *cmd = (char *)0;

	//
	// Set the length of the command string. This length
	// comes from string "/usr/sbin/didadm -L -o name,path,host"
	// and the filter string "awk... | uniq" used below.
	//
	cmd_length = 96;

	// Count the length of the instances for the command string
	for (operands.start(); !operands.end(); operands.next()) {
		cmd_length += strlen(operands.getValue()) + 1;
	}

	// Count the length of the nodes
	if (nodes.getSize() > 0) {
		// The length 7 comes from " | sed "
		cmd_length += 7;

		// The length 9 comes from " -e '//b'"
		for (nodes.start(); !nodes.end(); nodes.next()) {
			cmd_length += strlen(nodes.getValue()) + 9;
		}
		// The length 5 comes from " -e d"
		cmd_length += 5;
	}

	// Allocate space
	cmd = new char[cmd_length + 1];

	// Add the command
	(void) sprintf(cmd, "/usr/sbin/didadm -L -o name,path,host");

	// Add the operands
	for (operands.start(); !operands.end(); operands.next()) {
		(void) sprintf(cmd, "%s %s", cmd, operands.getValue());
	}

	// Add a sed filter for the nodes
	if (nodes.getSize() > 0) {
		(void) sprintf(cmd, "%s | sed ", cmd);
		for (nodes.start(); !nodes.end(); nodes.next()) {
			(void) sprintf(cmd, "%s -e '/%s/b'",
			    cmd, nodes.getValue());
		}
		(void) sprintf(cmd, "%s -e d", cmd);
	}

	// Add a filter to get rid of the duplicates and only print
	// the name and ctd
	(void) sprintf(cmd, "%s | sed -e 's,/dev/rdsk/,,' | "
	    "awk '{ print $1\" \"$2 }' | uniq", cmd);

	FILE *file = NULL;
	err = pipe_cmd(&file, cmd);

	// Free memory
	delete [] cmd;

	// Check error
	if (err) {
		if (file) {
			(void) pclose(file);
		}
		return (err);
	}

	char name[16];
	char temp[64];
	name[0] = '\0';
	temp[0] = '\0';

	while (fscanf(file, "%s", temp) != EOF) {
		if (strcmp(name, "") == 0) {
			(void) sprintf(name, temp);
			new_ops.add(name);
		} else {
			err = export_obj.createDevice(name, temp);
			if (err && !first_err)
				first_err = err;
			name[0] = '\0';
		}
		temp[0] = '\0';
	}
	(void) pclose(file);

	// Get an accurate device listing for passing on to the scdpm cmd filter
	operands.clear();
	for (new_ops.start(); !new_ops.end(); new_ops.next()) {
		operands.add(new_ops.getValue());
	}

	return (first_err);
}

clerrno_t
get_device_paths(CldeviceExport &export_obj, ValueList &operands,
    ValueList &nodes)
{
	int err = CL_NOERR;
	int first_err = err;
	int cmd_length = 0;
	char *cmd = (char *)0;

	//
	// Set the length of the command string. This length
	// comes from string "/usr/cluster/bin/scdpm ... " and
	// and the sorting string "awk... | sort" used below.
	//
	cmd_length = 170;

	// Count the length of the filter for the operands
	if (operands.getSize() > 0) {
		// The length 7 comes from " | sed "
		cmd_length += 7;
		for (operands.start(); !operands.end(); operands.next()) {
			// The length 10 comes from " -e '/ /b'"
			cmd_length += strlen(operands.getValue()) + 10;
		}

		// The length 5 comes from " -e d"
		cmd_length += 5;
	}

	// Count the length of the filter for the nodes
	if (nodes.getSize() > 0) {
		// The length 7 comes from " | sed "
		cmd_length += 7;

		for (nodes.start(); !nodes.end(); nodes.next()) {
			// The length 9 comes from " -e '//b'"
			cmd_length += strlen(nodes.getValue()) + 9;
		}

		// The length 5 comes from " -e d"
		cmd_length += 5;
	}

	// Allocate space
	cmd = new char[cmd_length + 1];

	// Build the command
	(void) sprintf(cmd, "/usr/cluster/bin/scdpm -p all | grep did ");

	// Add a sed filter for the operands
	if (operands.getSize() > 0) {
		(void) sprintf(cmd, "%s | sed ", cmd);
		for (operands.start(); !operands.end(); operands.next()) {
			(void) sprintf(cmd, "%s -e '/%s /b'",
			    cmd, operands.getValue());
		}
		(void) sprintf(cmd, "%s -e d", cmd);
	}

	// Add a sed filter for the nodes
	if (nodes.getSize() > 0) {
		(void) sprintf(cmd, "%s | sed ", cmd);
		for (nodes.start(); !nodes.end(); nodes.next()) {
			(void) sprintf(cmd, "%s -e '/%s/b'",
			    cmd, nodes.getValue());
		}
		(void) sprintf(cmd, "%s -e d", cmd);
	}

	// Apply format changes to sort by device number
	(void) sprintf(cmd, "%s | awk -F: '{ print $2\" \"$1 }' | "
	    "awk '{ print $1\" \"$3\" \"$2 }' | "
	    "sed -e '/reboot_on_path_failure/d' -e 's,/dev/did/rdsk/,,' | "
	    "sort", cmd);

	FILE *file = NULL;
	err = pipe_cmd(&file, cmd);

	// Free memory
	delete [] cmd;

	// Check error
	if (err) {
		if (file) {
			(void) pclose(file);
		}
		return (err);
	}

	char name[128];
	char node[128];
	char temp[128];
	name[0] = '\0';
	node[0] = '\0';
	temp[0] = '\0';

	while (fscanf(file, "%s", temp) != EOF) {
		if (strcmp(name, "") == 0) {
			(void) sprintf(name, temp);
		} else if (strcmp(node, "") == 0) {
			(void) sprintf(node, temp);
		} else {
			bool monitored = true;
			if (strcasecmp(temp, "Unmonitored") == 0)
				monitored = false;

			err = export_obj.addDevicePath(name, node, monitored);
			if (err && !first_err)
				first_err = err;

			name[0] = '\0';
			node[0] = '\0';
		}

		temp[0] = '\0';
	}
	(void) pclose(file);

	return (first_err);
}

clerrno_t
cldevice_export(ClCommand &cmd, ValueList &operands, ValueList &nodes,
    char *clconfiguration)
{
	int err = CL_NOERR;
	int first_err = err;

	// Set the doc to be stdout
	if (!clconfiguration)
		clconfiguration = strdup("-");

	// Validate the nodelist passed in
	if (nodes.getSize() != 0) {
		err = verify_nodelist(nodes);
		if (err && !first_err) {
			// Set first error
			first_err = err;
		}
	}

	// Validate the operands
	if (operands.getSize() != 0) {
		err = convert_operands_to_instances(operands);
		if (err && !first_err) {
			first_err = err;
		}

		if (operands.getSize() == 0)
			return (first_err);
	}

	ClXml xml;
	CldeviceExport export_obj;
	err = get_export_obj(export_obj, operands, nodes);
	if (err && !first_err) {
		first_err = err;
	}

	// Export to XML
	err = xml.createExportFile(CLDEVICE_CMD, clconfiguration, &export_obj);
	if (err && !first_err)
		first_err = err;

	return (first_err);
}
