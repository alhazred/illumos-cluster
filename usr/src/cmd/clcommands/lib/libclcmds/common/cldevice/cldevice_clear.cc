//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)cldevice_clear.cc	1.2	08/05/20 SMI"

//
// Process cldevice "clear"
//

#include "cldevice.h"

clerrno_t
cldevice_clear(ClCommand &cmd, ValueList &nodes)
{
	int err = CL_NOERR;
	int first_err = err;

	// The command args
	char args[] = "-C";

	// Get the nodes to run the command on...
	if (nodes.getSize() == 0) {
		// Add this node to the nodelist
		err = add_this_node(nodes);

		if (err) {
			// Print an error
			clerror("Error adding this node to nodelist.\n");
		}

		if (err && !first_err) {
			// Set first error
			first_err = err;
		}
	} else {
		// Validate the nodelist passed in
		err = verify_nodelist(nodes);

		if (err && !first_err) {
			// Set first error
			first_err = err;
		}
	}

	for (nodes.start();
	    !nodes.end();
	    nodes.next()) {

		// Run the command on the node
		err = run_cmd_on_node(SCDIDADM, args,
		    nodes.getValue());

		if (!err && cmd.isVerbose) {
			// Print success
			clmessage("Successfully cleared DID devices on node "
			    "%s.\n", nodes.getValue());
		}

		if (err && !first_err) {
			// Set first error
			first_err = err;
		}
	}

	return (first_err);
}
