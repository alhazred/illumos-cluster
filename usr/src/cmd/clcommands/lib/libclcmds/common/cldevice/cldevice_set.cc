//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)cldevice_set.cc	1.6	08/05/21 SMI"

//
// Process cldevice "set"
//

#include "cldevice.h"

clerrno_t
cldevice_set(ClCommand &cmd, ValueList &operands, NameValueList &props)
{
	int err = CL_NOERR;
	int first_err = err;
	char *propval = NULL;

	// There is only one valid property - make sure this is it...
	for (props.start(); !props.end(); props.next()) {
		if ((strcasecmp(props.getName(), "default_fencing") == 0) ||
			(strcasecmp(props.getName(), "defaultfencing") == 0)) {
			propval = props.getValue();
		} else {
			clerror("Invalid property name - \"%s\".\n",
			    props.getName());
			err = CL_EPROP;
		}
	}

	if (err)
		return (err);
	else if (!propval)
		return (CL_EINTERNAL);

	// Now make sure the property value is valid...
	if (strcasecmp(propval, "pathcount") == 0)
		propval = strdup("pathcount");
	else if (strcasecmp(propval, "scsi3") == 0)
		propval = strdup("scsi3");
	else if (strcasecmp(propval, "global") == 0)
		propval = strdup("useglobal");
	else if (strcasecmp(propval, "nofencing") == 0)
		propval = strdup("nofencing");
	else if (strcasecmp(propval, "nofencing-noscrub") == 0)
		propval = strdup("nofencing-noscrub");
	else {
		clerror("Invalid property value - \"%s\".\n", propval);
		return (CL_EINVAL);
	}

	// build the command string
	char cmdbuf[BUFSIZ];
	(void) sprintf(cmdbuf, "/usr/cluster/bin/scdidadm -F %s", propval);

	// Verbose?
	if (cmd.isVerbose)
		(void) sprintf(cmdbuf, "%s -d 1 ", cmdbuf);

	// Check to see if it a '+'
	if (operands.getSize() == 0) {
		if (get_all_devices(operands)) {
			clerror("Error getting all device instances.\n");
		}
	}

	// Add the operands to the list
	for (operands.start();
	    !operands.end();
	    operands.next()) {
		(void) sprintf(cmdbuf, "%s %s", cmdbuf,
		    operands.getValue());
	}

	// Set the special environment variable
	if (set_cldevice_env())
		return (CL_ENOMEM);

	err = exec_cmd(cmdbuf);

	if (err) {
		// Print error
		first_err = err;
	}

	if (!err && cmd.isVerbose) {
		// Print success
		clmessage("Successfully set DID device property \"%s\".\n",
		    props.getName());
	}

	return (first_err);
}
