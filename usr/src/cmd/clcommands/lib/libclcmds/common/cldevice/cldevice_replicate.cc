//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)cldevice_replicate.cc	1.4	08/05/20 SMI"

//
// Process cldevice "replicate"
//

#include "cldevice.h"

clerrno_t
cldevice_replicate(ClCommand &cmd, char *dest_node,
    char *repl_type, char *src_node)
{
	if (!dest_node)
		return (CL_EINTERNAL);

	int err = CL_NOERR;
	int first_err = err;

	// if no src_node, then use the localhost
	if (!src_node) {
		src_node = new char[BUFSIZ];
		err = gethostname(src_node, BUFSIZ);
		if (err) {
			clerror("Unable to get localhost name.\n");
			return (CL_EINTERNAL);
		}
	} else {
		// Verify the nodelist
		ValueList nodes;
		nodes.add(src_node);
		if ((err = verify_nodelist(nodes)) != CL_NOERR) {
			return (err);
		}
	}

	// build the command string, taking care that repl_type is optional
	char args[BUFSIZ];
	if (!repl_type) {
		(void) sprintf(args, "-T %s", dest_node);
	} else {
		(void) sprintf(args, "-T %s -e %s", dest_node, repl_type);
	}

	// Verbose?
	if (cmd.isVerbose)
		(void) sprintf(args, "%s -d 1", args);

	err = run_cmd_on_node(SCDIDADM, args, src_node);

	if (!err && cmd.isVerbose) {
		// Print success
		clmessage("Successfully replicated DID devices to node %s.\n",
		    dest_node);
	}

	if (err && !first_err) {
		// Set first error
		first_err = err;
	}

	return (first_err);
}
