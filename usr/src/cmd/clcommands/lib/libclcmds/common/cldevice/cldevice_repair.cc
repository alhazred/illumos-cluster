//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)cldevice_repair.cc	1.3	08/05/20 SMI"

//
// Process cldevice "repair"
//

#include "cldevice.h"

clerrno_t
cldevice_repair(ClCommand &cmd, ValueList &operands, ValueList &nodes)
{
	int err = CL_NOERR;
	int first_err = err;

	// Get the nodes
	if (nodes.getSize() == 0) {
		// Add this node to the nodelist
		err = add_this_node(nodes);

		if (err) {
			// Print an error
			clerror("Error adding this node to nodelist.\n");

			// Set first error
			first_err = err;
		}
	} else {
		// Validate the nodelist passed in
		err = verify_nodelist(nodes);

		if (err) {
			// Set first error
			first_err = err;
		}
	}

	// Run the command on each node
	for (nodes.start();
	    !nodes.end();
	    nodes.next()) {

		// build the command string
		char args[BUFSIZ];

		// Verbose?
		if (cmd.isVerbose) {
			(void) sprintf(args, "-d 1 -R");
		} else {
			(void) sprintf(args, "-R ");
		}

		if (operands.getSize() == 0) {
			// If no operands, then it is a '+'
			(void) sprintf(args, "%s all", args);
		} else {

			// Add the operands to the list
			char devs[BUFSIZ];
			devs[0] = '\0';
			for (operands.start();
			    !operands.end();
			    operands.next()) {

				// Ensure that the operands given are local
				// to the node which the command is to be run.
				if (device_exists_on_node(operands.getValue(),
				    nodes.getValue(), err)) {
					(void) sprintf(devs, "%s %s", devs,
					    operands.getValue());
				} else {
					clerror("Device \"%s\" could not be "
					    "found  or is not local to node "
					    "\"%s\".\n", operands.getValue(),
					    nodes.getValue());
					if (!first_err)
						first_err = CL_ENOENT;
				}
			}

			// Check to see if any of the devices listed were
			// valid for this node
			if (strcmp(devs, "") == 0) {
				continue;
			} else {
				(void) sprintf(args, "%s %s", args, devs);
			}
		}

		int err = run_cmd_on_node(SCDIDADM, args, nodes.getValue());

		if (err && !first_err) {
			// Set first error
			first_err = err;
		}
	}

	return (first_err);
}
