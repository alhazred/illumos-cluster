//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident   "@(#)cldevice_list.cc	1.4	08/05/20 SMI"

//
// Process cldevice "list"
//

#include "cldevice.h"

clerrno_t
cldevice_list(ClCommand &cmd, ValueList &operands, ValueList &nodes)
{
	int err = CL_NOERR;
	int first_err = err;
	char errbuf[BUFSIZ];
	char cmdbuf[BUFSIZ];
	FILE *file = NULL;
	ClList list = ClList(cmd.isVerbose ? true : false);

	// Validate the nodelist passed in
	if (nodes.getSize() != 0) {
		err = verify_nodelist(nodes);
		if (err && !first_err) {
			// Set first error
			first_err = err;
		}

		if (nodes.getSize() == 0)
			goto exit;
	}

	// Validate the operands
	if (operands.getSize() != 0) {
		err = convert_operands_to_instances(operands);
		if (err && !first_err) {
			// Set first error
			first_err = err;
		}

		if (operands.getSize() == 0)
			goto exit;
	}

	// Set the headings
	list.setHeadings(
	    (char *)gettext("DID Device"),
	    (char *)gettext("Full Device Path"));

	// build the command string
	(void) sprintf(cmdbuf, "/usr/sbin/didadm -L -o name,fullpath");

	// Add the operands to the list
	for (operands.start();
	    !operands.end();
	    operands.next()) {
		(void) sprintf(cmdbuf, "%s %s", cmdbuf,
		    operands.getValue());
	}

	// Add a sed filter for the nodes
	if (nodes.getSize() > 0) {
		(void) sprintf(cmdbuf, "%s | sed ", cmdbuf);
		for (nodes.start(); !nodes.end(); nodes.next()) {
			(void) sprintf(cmdbuf, "%s -e '/%s/b'",
			    cmdbuf, nodes.getValue());
		}
		(void) sprintf(cmdbuf, "%s -e d", cmdbuf);
	}

	// If not verbose, only add unique device instances
	if (!cmd.isVerbose) {
		(void) sprintf(cmdbuf, "%s | awk "
		    "'{ print $1\" blank\" }' | uniq",
		    cmdbuf);
	}

	err = pipe_cmd(&file, cmdbuf);

	if (err) {
		// Set first error
		if (!first_err)
			first_err = err;
	} else {
		// Got the pipe with all the devices which were found,
		// now parse the output and put in a ClList object
		char name[128];
		char temp[256];
		name[0] = '\0';
		temp[0] = '\0';
		while (fscanf(file, "%s", temp) != EOF) {
			if (strcmp(name, "") == 0) {
				(void) sprintf(name, temp);
			} else {
				list.addRow(name, temp);
				name[0] = '\0';
				temp[0] = '\0';
			}

			temp[0] = '\0';
		}
	}

exit:

	// print it
	list.print();

	return (first_err);
}
