//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clquorum_remove.cc	1.3	09/01/16 SMI"

//
// Process clquorum "remove"
//

#include "clquorum.h"

//
// clquorum "remove"
//
clerrno_t
clquorum_remove(ClCommand &cmd, ValueList &operands, optflgs_t optflgs,
    ValueList &devicetypes)
{
#ifdef WEAK_MEMBERSHIP
	char *qdev_name = NULL;

	for (operands.start(); operands.end() != 1; operands.next()) {
		qdev_name = operands.getValue();
		if (!qdev_name)
		continue;

		if (strcmp(qdev_name, SCCONF_GLOBAL_QUORUM_NAME) == 0) {
		    clerror("Cannot remove \"%s\" type quorum "
			"device \"%s\".\n",
			SCCONF_QUORUM_TYPE_SYSTEM,
			SCCONF_GLOBAL_QUORUM_NAME);
		    return (CL_EEXIST);
		}
	}
#endif
	return (clquorum_do_set_operation(cmd, operands, optflgs, devicetypes,
	    CLQ_REMOVE));
}
