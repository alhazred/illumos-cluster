//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clquorum_list.cc	1.4	09/01/16 SMI"

//
// Process clquorum "list"
//

#include "clquorum.h"
#include "ClList.h"

//
// clquorum "list"
//
clerrno_t
clquorum_list(ClCommand &cmd, ValueList &operands, optflgs_t optflgs,
    ValueList &devicetypes, ValueList &nodelist)
{
	ClList print_list = ClList(cmd.isVerbose ? true : false);
	NameValueList qdev_list = NameValueList(true);
	clerrno_t clerrno = CL_NOERR;
	char err_buf[SCCONF_MAXSTRINGLEN];
	uint_t i;
	quorum_status_t *quorum_status = (quorum_status_t *)0;
	quorum_device_status_t *qdev_status;
	int found;
	char *quorum_name;
	char *quorum_type;
	nodeid_t nodeid;
	node_bitmask_t node_bitmask;
	node_bitmask_t conf_node_bitmask = 0;
	bool check_nodes = (nodelist.getSize() > 0) ? true : false;
	int row_count = 0;
	char ccr_gdevname[SCCONF_MAXSTRINGLEN];
	clerrno_t first_err = CL_NOERR;

	// Get the valid quorum devices from the user input
	clerrno = clquorum_preprocess_read_operation(operands,
	    true, devicetypes, qdev_list);
	if (clerrno != CL_NOERR) {
		if ((clerrno != CL_ETYPE) && (clerrno != CL_ENOENT)) {
			clcommand_perror(clerrno);
			return (clerrno);
		}

		if (first_err == CL_NOERR) {
			first_err = clerrno;
		}
	}

	// Need to check the nodelist
	clerrno = clquorum_get_nodes_bitmask(nodelist, true,
	    &node_bitmask);
	if (clerrno != CL_NOERR) {
		if (first_err == CL_NOERR) {
			first_err = clerrno;
		}
		if (clerrno != CL_ENOENT) {
			goto cleanup;
		}
	}

	if (check_nodes) {
		// Get the quorum status for checking
		if (cluster_get_quorum_status(&quorum_status) != 0) {
			clerrno = CL_EINTERNAL;
			clcommand_perror(clerrno);
			if (first_err == CL_NOERR) {
				first_err = clerrno;
			}
			goto cleanup;
		}
	}

	// Process the quorum devices
	for (qdev_list.start(); qdev_list.end() != 1; qdev_list.next()) {
		quorum_name = qdev_list.getName();
		quorum_type = qdev_list.getValue();
		if (!quorum_name || !quorum_type)
			continue;

		// If no nodelist to check, just add the quorum to the row
		if (!check_nodes) {
			print_list.addRow(quorum_name, quorum_type);
			row_count++;
			continue;
		}

#ifdef WEAK_MEMBERSHIP
		//
		// if the device is a GLOBAL_QUORUM object, just add the quorum
		// to the row
		//
		if ((strcmp(quorum_name, SCCONF_GLOBAL_QUORUM_NAME) == 0) ||
			(strcmp(quorum_type, SCCONF_QUORUM_TYPE_SYSTEM) == 0)) {
			print_list.addRow(quorum_name, quorum_type);
			row_count++;
			continue;
		}
#endif
		// Find the quorum to check againt the nodelist
		found = 0;
		for (i = 0; i < quorum_status->num_quorum_devices; i++) {
			qdev_status = &quorum_status->quorum_device_list[i];
			if (qdev_status->gdevname == NULL)
				continue;

			// Get its gdevname in ccr
			*ccr_gdevname = '\0';
			(void) scconf_didname_slice(quorum_name, ccr_gdevname);

			if ((strcmp(quorum_name, qdev_status->gdevname)
			    == 0) ||
			    (strcmp(ccr_gdevname, qdev_status->gdevname)
			    == 0)) {
				conf_node_bitmask =
				    qdev_status->nodes_with_enabled_paths;
				found = 1;
				break;
			}
		}
		if (!found) {
			// The quorum could be in maintstate
			if (strcmp(quorum_type, "node") != 0)
				continue;

			// This quorum could be a node
			if (scconf_get_nodeid(quorum_name, &nodeid) !=
			    SCCONF_NOERR) {
				clerrno = CL_EINTERNAL;
				clcommand_perror(clerrno);
				if (first_err == CL_NOERR) {
					first_err = clerrno;
				}
				goto cleanup;
			}

			found = 0;
			for (i = 0; i < quorum_status->num_nodes; i++) {
				if (quorum_status->nodelist[i].nid ==
				    nodeid) {
					found = 1;
					break;
				}
			}

			if (!found)
				continue;

			conf_node_bitmask = 1LL << (nodeid-1);
		}

		// Check if the quorum connects to any one in the nodelist
		if (node_bitmask & conf_node_bitmask) {
			print_list.addRow(quorum_name, quorum_type);
			row_count++;
		}
	}

	// Add the header
	if (((first_err == CL_NOERR) || row_count) && (optflgs & vflg)) {
		print_list.setHeadings((char *)gettext("Quorum"),
		    (char *)gettext("Type"));
	}

	// Print it
	print_list.print();

cleanup:
	// Return the first error
	if (first_err != CL_NOERR) {
		clerrno = first_err;
	}

	if (check_nodes) {
		cluster_release_quorum_status(quorum_status);
	}

	return (clerrno);
}
