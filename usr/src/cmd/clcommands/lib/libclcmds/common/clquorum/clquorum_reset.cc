//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clquorum_reset.cc	1.2	08/05/20 SMI"

//
// Process clquorum "reset"
//

#include "clquorum.h"

//
// clquorum "reset"
//
clerrno_t
clquorum_reset(ClCommand &cmd, optflgs_t optflgs)
{
	scconf_errno_t scconf_err = SCCONF_NOERR;
	clerrno_t clerrno = CL_NOERR;

	// Reset all nodes, all quorum devices, and installmode.
	scconf_err = scconf_reset_quorum();
	if (scconf_err != SCCONF_NOERR) {
		if (scconf_err == SCCONF_EQUORUM) {
			clerror("Minimum node and quorum device "
			    "requirements must be met before resetting "
			    "the quorum state.\n");
			clerrno = CL_EOP;
		} else {
			clerrno = clquorum_conv_scconf_err(scconf_err);
		}
	} else if (optflgs & vflg) {
		clmessage("Quorum configuration is set to use the default "
		    "vote count.\n");
	}

	return (clerrno);
}
