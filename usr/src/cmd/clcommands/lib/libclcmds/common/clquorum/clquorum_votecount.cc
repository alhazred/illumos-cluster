//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clquorum_votecount.cc	1.2	08/05/20 SMI"

//
// Process clquorum "votecount"
//

#include "clquorum.h"

//
// clquorum "votecount"
//
clerrno_t
clquorum_votecount(ClCommand &cmd, ValueList &operands, optflgs_t optflgs,
    ValueList &nodelist)
{
	scconf_errno_t scconf_err = SCCONF_NOERR;
	clerrno_t clerrno = CL_NOERR;
	char *this_node;
	int first_err = CL_NOERR;
	uint_t vote_number = 0;
	char *endp;
	char *vote_str;

	// Check input
	if ((nodelist.getSize() == 0) || (operands.getSize() != 1)) {
		clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}

	// Get the vote number
	operands.start();
	vote_str = operands.getValue();
	if (!vote_str) {
		clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}

	vote_number = (uint_t)strtol(vote_str, &endp, 10);
	if (*endp != NULL) {
		clerror("Invalid value \"%s\" for the default vote count.\n",
		    vote_str);
		return (CL_EINVAL);
	}

	// Go through the node list
	for (nodelist.start(); nodelist.end() != 1; nodelist.next()) {
		this_node = nodelist.getValue();
		if (!this_node)
			continue;

		// Update the votecount
		scconf_err = scconf_set_node_defaultvote(this_node,
		    vote_number);
		if (scconf_err != SCCONF_NOERR) {
			switch (scconf_err) {
			case SCCONF_ENOEXIST:
				clerror("Unknown node \"%s\".\n", this_node);
				clerrno = CL_ENOENT;
				break;

			case SCCONF_EQUORUM:
				clerror("Cluster quorum could be compromised "
				    "if set vote count to \"%d\" for node "
				    "\"%s\".\n",
				    vote_number, this_node);
				clerrno = CL_ESTATE;
				break;

			case SCCONF_EINVAL:
			case SCCONF_EBADVALUE:
				clerror("Bad votecount for node \"%s\".\n",
				    this_node);
				clerrno = CL_EINVAL;
				break;

			case SCCONF_EINUSE:
				clerror("Conflicting operation to node "
				    "\"%s\".\n", this_node);
				clerrno = CL_ESTATE;
				break;

			default:
				clerrno = clquorum_conv_scconf_err(scconf_err);
				break;

			} //lint !e788

			if ((first_err == CL_NOERR) &&
			    (clerrno != CL_NOERR)) {
				first_err = clerrno;
			}
		} else if (optflgs & vflg) {
			clmessage("The default votecount is set to \"%d\" "
			    "for node \"%s\".\n",
			    vote_number, this_node);
		}
	}

	if (first_err != CL_NOERR) {
		clerrno = first_err;
	}
	return (clerrno);
}
