//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clquorum_add.cc	1.6	09/01/16 SMI"

//
// Process clquorum "add"
//

#include "clquorum.h"
#include "ClXml.h"

static clerrno_t
clquorum_autoconfig(ClCommand &cmd, ValueList &devicetypes);

//
// clquorum "add"
//
clerrno_t
clquorum_add(ClCommand &cmd, ValueList &operands, optflgs_t optflgs,
    ValueList &devicetypes, NameValueList &properties)
{
	scconf_errno_t scconf_err = SCCONF_NOERR;
	clerrno_t clerrno = CL_NOERR;
	clerrno_t first_clerrno = CL_NOERR;
	char *qdev_type;
	char *qdev_name;
	char *qdev_props = (char *)0;
#ifdef WEAK_MEMBERSHIP
	boolean_t multiple_partitions = B_FALSE;
	clconf_cluster_t *clconf = (clconf_cluster_t *)0;

	scconf_err = scconf_cltr_openhandle((scconf_cltr_handle_t *)&clconf);
	if (scconf_err != SCCONF_NOERR) {
		clerrno = clquorum_conv_scconf_err(scconf_err);
		return (clerrno);
	}

	scconf_err = scconf_get_multiple_partitions(clconf,
				&multiple_partitions);
	if (scconf_err != SCCONF_NOERR) {
		clerrno = clquorum_conv_scconf_err(scconf_err);
		return (clerrno);
	}

	if (multiple_partitions) {
		clerror("Cannot add quorum device when \"%s\" "
			"property is set to \"true\".\n",
			PROP_CLUSTER_MULTI_PARTITIONS);
		return (CL_EOP);
	}
#endif
	// Do autoconfig if -a
	if (optflgs & aflg) {
		return (clquorum_autoconfig(cmd, devicetypes));
	}

	// Check inputs
	if ((operands.getSize() == 0) || (devicetypes.getSize() != 1)) {
		clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}

	//
	// Add the quorum. Note: nodes will not be
	// specified in CLI. So always pass as NULL.
	//
	devicetypes.start();
	qdev_type = devicetypes.getValue();

	// Convert property list to a string.
	if (properties.getSize() > 0) {
		qdev_props = properties.getString(",");
		if (!qdev_props) {
			clcommand_perror(CL_EINTERNAL);
			return (CL_EINTERNAL);
		}
	}

	// Go through theoperands
	for (operands.start(); operands.end() != 1; operands.next()) {
		char *msgbuffer = (char *)0;
		qdev_name = operands.getValue();
		if (!qdev_name)
			continue;
#ifdef WEAK_MEMBERSHIP
		if (strcmp(qdev_name, SCCONF_GLOBAL_QUORUM_NAME) == 0) {
			clerror("Quorum device name \"%s\" is reserved\n",
				SCCONF_GLOBAL_QUORUM_NAME);
			return (CL_EEXIST);
		}
#endif
		scconf_err = scconf_add_quorum_device(qdev_name, NULL,
		    qdev_type, qdev_props, &msgbuffer);
		if (scconf_err != SCCONF_NOERR) {
			// Print detailed messages
			if (msgbuffer != (char *)0) {
				clcommand_dumpmessages(msgbuffer);
				free(msgbuffer);
			}

			switch (scconf_err) {
			case SCCONF_ENOEXIST:
				clerror("Cannot find device \"%s\" to add.\n",
				    qdev_name);
				clerrno = CL_ENOENT;
				break;

			case SCCONF_EEXIST:
				clerror("Quorum device \"%s\" already "
				    "exists.\n", qdev_name);
				clerrno = CL_EEXIST;
				break;

			case SCCONF_EUNKNOWN:
				clerror("Unknown type \"%s\".\n",
				    qdev_type);
				clerrno = CL_ETYPE;
				break;

			case SCCONF_EQD_SCRUB:
				clerror("Cannot communicate with quorum device "
				    "\"%s\".\n", qdev_name);
				clerrno = CL_EINVAL;
				break;

			case SCCONF_EQD_CONFIG:
				clerrno = CL_EOP;
				break;

			case SCCONF_EBADVALUE:
			case SCCONF_EUSAGE:
				clerrno = CL_EINVAL;
				break;

			case SCCONF_VP_MISMATCH:
				clerror("You cannot add a \"%s\" quorum "
				    "device when the software versions on two "
				    "or more nodes do not match.\n",
				    qdev_type);
				clerrno = CL_EOP;
				break;

			case SCCONF_EQUORUM:
				clerror("Added quorum device \"%s\". "
				    "Cannot enable quorum device at "
				    "this time. Check quorum status.\n",
				    qdev_name);
				clerrno = CL_ESTATE;
				break;

			default:
				clerrno = clquorum_conv_scconf_err(scconf_err);
				break;

			} //lint !e788

			if (first_clerrno == CL_NOERR) {
				first_clerrno = clerrno;
			}
		} else if (cmd.isVerbose) {
			clmessage("Quorum device \"%s\" is added.\n",
			    qdev_name);
		}
	}

	// Free momory
	if (qdev_props) {
		free(qdev_props);
	}

	if (first_clerrno != CL_NOERR) {
		clerrno = first_clerrno;
	}

	return (clerrno);
}

//
// clquorum "add -i"
//
clerrno_t
clquorum_add(ClCommand &cmd, ValueList &operands, optflgs_t optflgs,
    char *clconfiguration, ValueList &devicetypes,
    NameValueList &properties)
{
	clerrno_t clerrno = CL_NOERR;
	clerrno_t first_clerrno = CL_NOERR;
	ValueList xml_objects;
#ifdef WEAK_MEMBERSHIP
	boolean_t multiple_partitions = B_FALSE;
	clconf_cluster_t *clconf = (clconf_cluster_t *)0;
	scconf_errno_t scconf_err = SCCONF_NOERR;

	scconf_err = scconf_cltr_openhandle((scconf_cltr_handle_t *)&clconf);
	if (scconf_err != SCCONF_NOERR) {
		clerrno = clquorum_conv_scconf_err(scconf_err);
		return (clerrno);
	}

	scconf_err = scconf_get_multiple_partitions(clconf,
				&multiple_partitions);
	if (scconf_err != SCCONF_NOERR) {
		clerrno = clquorum_conv_scconf_err(scconf_err);
		return (clerrno);
	}

	if (multiple_partitions) {
		clerror("Cannot add quorum device when \"%s\" "
			"property is set to \"true\".\n",
			PROP_CLUSTER_MULTI_PARTITIONS);
		return (CL_EOP);
	}
#endif
	// Check for inputs
	if (!clconfiguration) {
		clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}

	OptionValues *ov = new OptionValues((char *)"add", cmd.argc, cmd.argv);
	ClXml xml;

	// Add quorum types to OptionValues
	ov->addOptions(CLQUORUM_TYPE, &devicetypes);

	// Add quorum properties to OptionValues
	ov->addNVOptions(CLQUORUM_PROP, &properties);

	// verbose?
	ov->optflgs = 0;
	if (optflgs & vflg) {
		ov->optflgs |= vflg;
	}

	// Get all the objects in the xml file
	clerrno = xml.getAllObjectNames(CLQUORUM_CMD, clconfiguration,
	    &xml_objects);
	if (clerrno != CL_NOERR) {
		return (clerrno);
	}

	if (xml_objects.getSize() == 0) {
		clerror("Cannot find any quorum devices in \"%s\".\n",
		    clconfiguration);
		return (CL_EINVAL);
	}

	// Set operands
	for (operands.start(); !operands.end(); operands.next()) {
		char *operand_name = operands.getValue();
		if (!operand_name)
			continue;

		// Is this operand found in xml file?
		if (strcmp(operand_name, CLCOMMANDS_WILD_OPERAND) != 0) {
			int found = 0;
			for (xml_objects.start(); !xml_objects.end();
			    xml_objects.next()) {
				char *obj_name = xml_objects.getValue();
				if (!obj_name)
					continue;
				if (strcmp(obj_name, operand_name) == 0) {
					found++;
					break;
				}
			}

			if (!found) {
				clerror("Cannot find quorum device \"%s\" in "
				    "file \"%s\".\n",
				    operand_name, clconfiguration);
				if (first_clerrno == CL_NOERR) {
					first_clerrno = CL_ENOENT;
					}
				continue;
			}
		}

		ov->setOperand(operand_name);
		clerrno = xml.applyConfig(CLQUORUM_CMD, clconfiguration, ov);
		if ((clerrno != CL_NOERR) && (first_clerrno == CL_NOERR)) {
			first_clerrno = clerrno;
		}
	}

	if (first_clerrno != CL_NOERR) {
		clerrno = first_clerrno;
	}

	return (clerrno);
}


//
// clquorum "add -a"
//
clerrno_t
clquorum_autoconfig(ClCommand &cmd, ValueList &devicetypes)
{
	char *msgbuffer = (char *)0;
	scconf_errno_t scconf_err = SCCONF_NOERR;
	clerrno_t clerrno = CL_NOERR;
	scconf_name_qdev_t *qd_to_add = NULL;
	scconf_name_qdev_t *curr_qd = NULL;

	// Check inputs
	if ((devicetypes.getSize() != 0) && !devicetypes.isValue("scsi") &&
	    !devicetypes.isValue("shared_disk")) {
		clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}

	// Analyse and get the suggested quorum config changes
	scconf_err = scconf_get_optimal_quorum_config(&qd_to_add,
	    (scconf_name_qdev_t **)NULL, QUORUM_ADD_OP, &msgbuffer);
	if (scconf_err != SCCONF_NOERR) {
		// Print detailed messages
		if (msgbuffer != (char *)0) {
			clcommand_dumpmessages(msgbuffer);
			free(msgbuffer);
		}

		switch (scconf_err) {
		case SCCONF_ENOEXIST:
			clerrno = CL_ENOENT;
			break;

		case SCCONF_EEXIST:
			clerrno = CL_EEXIST;
			break;

		case SCCONF_EQD_CONFIG:
			clerrno = CL_EOP;
			break;

		default:
			clerrno = clquorum_conv_scconf_err(scconf_err);
			break;

		} //lint !e788


		goto cleanup;
	}

	// Add the quorum.
	curr_qd = qd_to_add;

	if (cmd.isVerbose) {
		if (curr_qd) {
			clmessage("Will add the following quorum devices:\n");
			while (curr_qd) {
				(void) fprintf(stdout,
				    "\t/dev/did/rdsk/%.256ss2\n",
				    curr_qd->scconf_name_qdevname);
				curr_qd = curr_qd->scconf_name_qdevnext;
			}
		}
	}

	curr_qd = qd_to_add;
	while (curr_qd) {
		char *msgbuff = (char *)0;
		scconf_err = scconf_add_quorum_device(
		    curr_qd->scconf_name_qdevname,
		    (char **)0, "shared_disk", NULL, &msgbuff);

		if (scconf_err != SCCONF_NOERR) {
			// Print detailed messages
			if (msgbuff != (char *)0) {
				clcommand_dumpmessages(msgbuff);
				free(msgbuff);
			}

			switch (scconf_err) {
			case SCCONF_ENOEXIST:
				clerror("Cannot find device \"%s\" to add.\n",
				    curr_qd->scconf_name_qdevname);
				clerrno = CL_ENOENT;
				break;

			case SCCONF_EEXIST:
				clerror("Quorum device \"%s\" already "
				    "exists.\n",
				    curr_qd->scconf_name_qdevname);
				clerrno = CL_EEXIST;
				break;

			case SCCONF_EINVAL:
				clerror("Invalid property.\n");
				clerrno = CL_EPROP;
				break;

			default:
				clerrno = clquorum_conv_scconf_err(scconf_err);
				break;

			} //lint !e788

			goto cleanup;
		} else if (cmd.isVerbose) {
			clmessage("Quorum device \"%s\" is added.\n",
			    curr_qd->scconf_name_qdevname);
		}
		curr_qd = curr_qd->scconf_name_qdevnext;
	}

cleanup:
	if (qd_to_add) {
		scconf_free_name_qdevlist(qd_to_add);
	}

	return (clerrno);
}
