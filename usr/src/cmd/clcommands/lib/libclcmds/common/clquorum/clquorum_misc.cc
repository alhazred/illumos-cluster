//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clquorum_misc.cc	1.8	09/01/16 SMI"

//
// Miscellaneous functions for clquorum
//

#include "clquorum.h"

//
// clquorum_conv_scconf_err
//
//	Convert quorum related scconf_errno_t to CL error.
//
clerrno_t
clquorum_conv_scconf_err(scconf_errno_t err)
{
	// Map the error
	switch (err) {
	case SCCONF_NOERR:
		return (CL_NOERR);

	case SCCONF_EEXIST:
		clerror("Quorum device already exist.\n");
		return (CL_EEXIST);

	case SCCONF_ENOEXIST:
		clerror("Quorum device does not exist.\n");
		return (CL_ENOENT);

	case SCCONF_EINSTALLMODE:
		clerror("Cluster is in install mode.\n");
		return (CL_ESTATE);

	case SCCONF_ENOMEM:
	case SCCONF_EOVERFLOW:
		clcommand_perror(CL_ENOMEM);
		return (CL_ENOMEM);

	case SCCONF_EBADVALUE:
		clerror("Bad value in configuration database.\n");
		return (CL_EINTERNAL);

	case SCCONF_EIO:
		clcommand_perror(CL_EIO);
		return (CL_EIO);

	case SCCONF_ERECONFIG:
		clcommand_perror(CL_ERECONF);
		return (CL_ERECONF);

	case SCCONF_DUP_NASQD:
		clerror("A quorum device is already configured for this "
		    "NAS device.\n");
		return (CL_EEXIST);

	case SCCONF_DUP_QSQD:
		clerror("A quorum device is already configured for this "
		    "quorum server host.\n");
		return (CL_EEXIST);

	case SCCONF_NO_BINARY:
		clerror("Cannot find the support software from "
		    "the NAS device vendor.\n");
		return (CL_EOP);

	default:
		clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}
}

//
// clquorum_get_quorumdevice_list
//
// Get the quorum devices from the CCR by the "qd_type" or "qd_name"
// ValueList. Saved into the NameValueList where "name" is the quorum
// device name, and "value" is its type. Quorum devices are collected by
// the following:
//
//	If only pass qd_name, get only those quorum devices of that name
//	If only pass qd_type, get only those quorum devices of that type
//	If pass both, get the quorum devices whose name is any if those
//		in qd_name, and whose type is of any of those in qd_type.
//	If pass none, get all the quorum devices.
//
clerrno_t
clquorum_get_quorumdevice_list(ValueList &qd_name, ValueList &qd_Type,
    NameValueList &qd_list)
{
	char didname[SCCONF_MAXSTRINGLEN];
	char disk_ccr_name[SCCONF_MAXSTRINGLEN];
	clerrno_t clerrno = CL_NOERR;
	scconf_errno_t scconf_err = SCCONF_NOERR;
	clconf_iter_t *currqdevsi, *qdevsi = (clconf_iter_t *)0;
	clconf_obj_t *qdev;
	clconf_cluster_t *clconf = (clconf_cluster_t *)0;
	ValueList gdevname_list = ValueList(true);
	const char *conf_name;
	const char *conf_type;
	ValueList qd_type = ValueList(true);

	//
	// shared_disk quorum devices can be specified as d<n> or /dev/did/rdsk
	// or /dev/glable/rdsk, so we get any of the legal format into
	// the "name" used in CCR. The name of other types quorum devices
	// are not impacted.
	//
	for (qd_name.start(); qd_name.end() != 1; qd_name.next()) {
		conf_name = qd_name.getValue();
		if (!conf_name ||
		    scconf_didname_slice((char *)conf_name, didname))
			continue;
		if (scconf_qdevname(didname, disk_ccr_name))
			continue;

		gdevname_list.add(disk_ccr_name);
	}

	scconf_err = scconf_cltr_openhandle((scconf_cltr_handle_t *)
	    &clconf);
	if (scconf_err != SCCONF_NOERR) {
		clerrno = clquorum_conv_scconf_err(scconf_err);
		goto cleanup;
	}

	// Process the device type.
	for (qd_Type.start(); !qd_Type.end(); qd_Type.next()) {
		char *device_type = qd_Type.getValue();
		if (!device_type) {
			continue;
		}

		// Treat "scsi" as "shared_disk"
		if (strcmp(device_type, "scsi") == 0) {
			qd_type.add("shared_disk");
		} else {
			qd_type.add(device_type);
		}
	}

	qdevsi = currqdevsi = clconf_cluster_get_quorum_devices(clconf);
	while ((qdev = clconf_iter_get_current(currqdevsi)) != NULL) {
		clconf_iter_advance(currqdevsi);

		// Get the quorum device name
		conf_name = clconf_obj_get_name(qdev);
		if (!conf_name)
			continue;

		// Check if name match
		if (qd_name.getSize() > 0) {
			if (!qd_name.isValue((char *)conf_name) &&
			    !gdevname_list.isValue((char *)conf_name))
				continue;
		}

		// Get the quorum type
		conf_type = clconf_obj_get_property(qdev, "type");

		// If no type, use the access mode
		if (conf_type == NULL) {
			conf_type = clconf_obj_get_property(qdev,
			    "access_mode");
		}
		if (!conf_type)
			continue;

		// Treat "scsi" as "shared_disk"
		if (strstr(conf_type, "scsi") != 0) {
			conf_type = "shared_disk";
		}

		if ((qd_type.getSize() > 0) &&
		    (!qd_type.isValue((char *)conf_type))) {
				continue;
		}

		// Save it
		(void) qd_list.add((char *)conf_name, (char *)conf_type);
	}

cleanup:
	// Release the cluster config
	if (clconf != (clconf_cluster_t *)0) {
		clconf_obj_release((clconf_obj_t *)clconf);
	}

	if (qdevsi != (clconf_iter_t *)0)
		clconf_iter_release(qdevsi);

	return (clerrno);
}

//
// clquorum_get_quorumdevices
//
// Get the quorum devices from the CCR by the "qd_type" or "qd_name".
// Saved into the NameValueList where "name" is the quorum device
// name, and "value" is its type. If no qd_type or qd_name is given,
// find all quorum devices.
//
clerrno_t
clquorum_get_quorumdevices(char *qd_name, char *qd_Type, NameValueList &qd_list)
{
	clerrno_t clerrno = CL_NOERR;
	scconf_errno_t scconf_err = SCCONF_NOERR;
	clconf_iter_t *currqdevsi, *qdevsi = (clconf_iter_t *)0;
	clconf_obj_t *qdev;
	clconf_cluster_t *clconf = (clconf_cluster_t *)0;
	char didname[SCCONF_MAXSTRINGLEN];
	char disk_ccr_name[SCCONF_MAXSTRINGLEN];
	const char *conf_name;
	const char *conf_type;
	char *qd_type = (char *)0;

	*disk_ccr_name = '\0';
	if (qd_name) {
		// Get the quorum name that can be used in CCR
		if (scconf_didname_slice(qd_name, didname) == 0) {
			(void) scconf_qdevname(didname, disk_ccr_name);
		}
	}

	scconf_err = scconf_cltr_openhandle((scconf_cltr_handle_t *)
	    &clconf);
	if (scconf_err != SCCONF_NOERR) {
		clerrno = clquorum_conv_scconf_err(scconf_err);
		goto cleanup;
	}

	// Treat "scsi" as "shared_disk"
	qd_type = qd_Type;
	if (qd_Type && strcmp(qd_Type, "scsi") == 0) {
		qd_type = "shared_disk";
	}

	qdevsi = currqdevsi = clconf_cluster_get_quorum_devices(clconf);
	while ((qdev = clconf_iter_get_current(currqdevsi)) != NULL) {
		clconf_iter_advance(currqdevsi);

		// Get the quorum device name
		conf_name = clconf_obj_get_name(qdev);
		if (!conf_name)
			continue;

		// Check if name match
		if (qd_name && ((strcmp(conf_name, qd_name) != 0) &&
		    (strcmp(conf_name, disk_ccr_name) != 0)))
			continue;

		// Get the quorum type
		conf_type = clconf_obj_get_property(qdev, "type");

		// If no type, use the access mode
		if (conf_type == NULL) {
			conf_type = clconf_obj_get_property(qdev,
			    "access_mode");
		}

		if (!conf_type)
			continue;

		// Treat "scsi" as "shared_disk"
		if (strstr(conf_type, "scsi") != 0) {
			conf_type = "shared_disk";
		}

		// Check if type match
		if (qd_type && strcmp(qd_type, conf_type) != 0)
			continue;

		// Add into the quorum device list
		(void) qd_list.add((char *)conf_name, (char *)conf_type);
	}

cleanup:
	// Release the cluster config
	if (clconf != (clconf_cluster_t *)0) {
		clconf_obj_release((clconf_obj_t *)clconf);
	}

	if (qdevsi != (clconf_iter_t *)0)
		clconf_iter_release(qdevsi);

	return (clerrno);
}

//
// clquorum_get_nodes_list
//
// Get the nodes devices from the CCR by the "node_name", and
// saved into the NameValueList where "name" is node name, and
// the "value" is always "node". This is for the node type
// used in clquorum command.
//
clerrno_t
clquorum_get_nodes_list(ValueList &node_name, NameValueList &qd_list)
{
	clerrno_t clerrno = CL_NOERR;
	int tmp_err;
	scconf_errno_t scconf_err = SCCONF_NOERR;
	clconf_cluster_t *clconf = (clconf_cluster_t *)0;
	clconf_iter_t *currnodesi, *nodesi = (clconf_iter_t *)0;
	clconf_obj_t *node;
	scconf_nodeid_t conf_nodeid;
	const char *conf_name;
	char conf_nodeid_str[SCCONF_MAXSTRINGLEN];

	scconf_err = scconf_cltr_openhandle((scconf_cltr_handle_t *)
	    &clconf);
	if (scconf_err != SCCONF_NOERR) {
		clerrno = clquorum_conv_scconf_err(scconf_err);
		goto cleanup;
	}

	// Read the nodes
	nodesi = currnodesi = clconf_cluster_get_nodes(clconf);
	while ((node = clconf_iter_get_current(currnodesi)) != NULL) {
		// Get the node name
		conf_name = clconf_obj_get_name(node);

		if (conf_name == NULL) {
			clerrno = CL_EINTERNAL;
			goto cleanup;
		}

		// get the node id
		conf_nodeid = (scconf_nodeid_t)clconf_obj_get_id(node);
		if (conf_nodeid == (scconf_nodeid_t)-1) {
			clerrno = CL_EINTERNAL;
			goto cleanup;
		}
		// Convert the nodeid to a string
		(void) sprintf(conf_nodeid_str, "%d", conf_nodeid);

		// Check with the node_name Valuelist
		if (node_name.getSize() > 0) {
			tmp_err = 0;
			if (node_name.isValue((char *)conf_name)) {
				tmp_err = qd_list.add((char *)conf_name,
				    "node");
			}
			if (node_name.isValue((char *)conf_nodeid_str)) {
				tmp_err = qd_list.add((char *)conf_nodeid_str,
				    "node");
			}

			if (tmp_err != 0) {
				clerrno = CL_EINTERNAL;
				goto cleanup;
			}
		} else if (qd_list.add((char *)conf_name, "node") != 0) {
			clerrno = CL_EINTERNAL;
			goto cleanup;
		}

		clconf_iter_advance(currnodesi);
	}

cleanup:
	// Release the cluster config
	if (clconf != (clconf_cluster_t *)0) {
		clconf_obj_release((clconf_obj_t *)clconf);
	}

	if (nodesi != (clconf_iter_t *)0)
		clconf_iter_release(nodesi);

	return (clerrno);
}

#ifdef WEAK_MEMBERSHIP
//
// clquorum_get_global_quorum_list
//
// Get the global quorum object from the CCR and
// save into the NameValueList where "name" is always GLOBAL_QUORUM, and
// the "value" is always "system". This is for the system type
// used in clquorum command.
//
clerrno_t
clquorum_get_global_quorum_list(ValueList &global_list, NameValueList &qd_list)
{
	clerrno_t clerrno = CL_NOERR;

	if (global_list.getSize() > 0) {
		if (global_list.isValue(SCCONF_GLOBAL_QUORUM_NAME)) {
		    if (qd_list.add((char *)SCCONF_GLOBAL_QUORUM_NAME,
			SCCONF_QUORUM_TYPE_SYSTEM) != 0) {
			clerrno = CL_EINTERNAL;
		    }
		}
	} else {
		if (qd_list.add((char *)SCCONF_GLOBAL_QUORUM_NAME,
			SCCONF_QUORUM_TYPE_SYSTEM) != 0) {
			clerrno = CL_EINTERNAL;
		}
	}

	return (clerrno);
}
#endif

//
// clquorum_get_nodes
//
// Get the node from the CCR by the "node_name", and saved
// into the NameValueList where "name" is node name, and
// the "value" is always "node". If no node_name, get all
// the nodes. The nodename can either the name or nodeid.
//
clerrno_t
clquorum_get_nodes(char *node_name, NameValueList &qd_list)
{
	clerrno_t clerrno = CL_NOERR;
	scconf_errno_t scconf_err = SCCONF_NOERR;
	clconf_cluster_t *clconf = (clconf_cluster_t *)0;
	clconf_iter_t *currnodesi, *nodesi = (clconf_iter_t *)0;
	clconf_obj_t *node;
	scconf_nodeid_t node_id = 0;
	char *c = 0;

	scconf_nodeid_t conf_nodeid;
	const char *conf_name;

	// Check if node_name is a number, the node id
	if (node_name && (*node_name != '\0')) {
		for (c = node_name;  *c;  ++c) {
			if (!isdigit(*c))
				break;
		}

		if (*c == '\0') {
			node_id = (scconf_nodeid_t)atoi(node_name);
			if (node_id < 1) {
				return (CL_EINVAL);
			}
		}
	}

	scconf_err = scconf_cltr_openhandle((scconf_cltr_handle_t *)
	    &clconf);
	if (scconf_err != SCCONF_NOERR) {
		clerrno = clquorum_conv_scconf_err(scconf_err);
		goto cleanup;
	}

	// Read the nodes
	nodesi = currnodesi = clconf_cluster_get_nodes(clconf);
	while ((node = clconf_iter_get_current(currnodesi)) != NULL) {
		clconf_iter_advance(currnodesi);

		// Get the node name
		conf_name = clconf_obj_get_name(node);

		if (!conf_name)
			continue;

		if (node_name) {
			if (node_id == 0) {
				// check with node name
				if (strcmp(node_name, conf_name) != 0)
					continue;
			} else {
				// get the node id
				conf_nodeid = (scconf_nodeid_t)
				    clconf_obj_get_id(node);
				if (conf_nodeid == (scconf_nodeid_t)-1) {
					clerrno = CL_EINTERNAL;
					goto cleanup;
				}

				// Check with node id
				if (conf_nodeid != node_id)
					continue;
			}

			// Save it
			if (qd_list.add((char *)node_name, "node") != 0) {
				clerrno = CL_EINTERNAL;
				goto cleanup;
			}
			break;
		}

		// Save it
		if (qd_list.add((char *)conf_name, "node") != 0) {
			clerrno = CL_EINTERNAL;
			goto cleanup;
		}
	}

cleanup:
	// Release the cluster config
	if (clconf != (clconf_cluster_t *)0) {
		clconf_obj_release((clconf_obj_t *)clconf);
	}

	if (nodesi != (clconf_iter_t *)0)
		clconf_iter_release(nodesi);

	return (clerrno);
}

//
// clquorum_get_nodes_bitmask
//
//	Get the bitmask from the valid ndoes in the nodelist.
//	Used for the nodelist processing in list, show, status
//	subcommand.  It prints error messages for non-existing nodes.
//
clerrno_t
clquorum_get_nodes_bitmask(ValueList &nodelist, bool print_err,
    node_bitmask_t *node_bitmask)
{
	clerrno_t clerrno = CL_NOERR;
	scconf_errno_t scconf_err = SCCONF_NOERR;
	char *node_name;
	nodeid_t nodeid;

	// No nodes means all nodes
	if (nodelist.getSize() == 0) {
		*node_bitmask = ~0;
		return (clerrno);
	}

	*node_bitmask = 0;

	// Go through the node list
	for (nodelist.start(); nodelist.end() != 1; nodelist.next()) {
		node_name = nodelist.getValue();
		if (!node_name)
			continue;

		// Get the node id
		scconf_err = scconf_get_nodeid(node_name, &nodeid);
		if (scconf_err == SCCONF_ENOEXIST) {
			if (print_err) {
				clerror("\"%s\" is not a valid cluster node.\n",
				    node_name);
			}
			clerrno = CL_ENOENT;
			continue;
		} else if (scconf_err != SCCONF_NOERR) {
			clerror("Invalid node specified.\n");
			return (CL_EINVAL);
		}

		// Save the nodeid to the bitmask
		*node_bitmask |= 1LL << (nodeid-1);
	}

	return (clerrno);
}

//
// clquorum_do_set_operation
//
//	Do an operation to the quorum devices stored in operands.
//	by the specified operation flag. It can do:
//
//	CLQ_DISABLE	put the devices into maintenance state
//	CLQ_ENABLE	clear the maintenance state
//	CLQ_REMOVE	remove the devices
//
clerrno_t
clquorum_do_set_operation(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, ValueList &devicetypes, int clq_flg)
{
	scconf_errno_t scconf_err = SCCONF_NOERR;
	clerrno_t clerrno = CL_NOERR;
	clerrno_t first_err = CL_NOERR;
	NameValueList qdev_list;
	NameValueList tmp_qdev_list;
	char *this_type;
	char *this_qdev;
	char didname[SCCONF_MAXSTRINGLEN];
	char disk_ccr_name[SCCONF_MAXSTRINGLEN];
	ValueList new_devicetypes = ValueList(true);

	// Create a new device type list
	for (devicetypes.start(); devicetypes.end() != 1;
	    devicetypes.next()) {
		this_type = devicetypes.getValue();
		if (!this_type)
			continue;
		if (strcmp(this_type, "scsi") == 0) {
			new_devicetypes.add("shared_disk");
		} else {
			new_devicetypes.add(this_type);
		}
	}

	// Get the quorum list by the specified types
	for (new_devicetypes.start(); new_devicetypes.end() != 1;
	    new_devicetypes.next()) {
		this_type = new_devicetypes.getValue();
		if (!this_type)
			continue;

		// Get the quorum devices of the this type
		tmp_qdev_list.clear();
		if (strcmp(this_type, "node") != 0) {
			clerrno = clquorum_get_quorumdevices(
			    NULL, this_type, tmp_qdev_list);
		} else {
			clerrno = clquorum_get_nodes(NULL,
			    tmp_qdev_list);
		}
		if (clerrno != CL_NOERR) {
			clcommand_perror(clerrno);
			goto cleanup;
		}

		if (tmp_qdev_list.getSize() == 0) {
			clerror("No quorum devices of type \"%s\" "
			    "exist.\n", this_type);
			if (first_err == CL_NOERR) {
				first_err = CL_ETYPE;
			}
			continue;
		}

		// If wildcard operand, save all quorum devices of this type
		if (operands.isValue(CLCOMMANDS_WILD_OPERAND)) {
			for (tmp_qdev_list.start(); tmp_qdev_list.end() != 1;
			    tmp_qdev_list.next()) {
				if (qdev_list.add(tmp_qdev_list.getName(),
				    tmp_qdev_list.getValue()) != 0) {
					clerrno = CL_EINTERNAL;
					clcommand_perror(clerrno);
					goto cleanup;
				}
			}
		}
	}

	// Check out the operands for wildcard
	if (operands.isValue(CLCOMMANDS_WILD_OPERAND)) {

		if (new_devicetypes.getSize() == 0) {
			// Get quorum devices of all types
			clerrno = clquorum_get_quorumdevices(NULL, NULL,
			    qdev_list);
			if (clerrno != CL_NOERR) {
				clcommand_perror(clerrno);
				goto cleanup;
			}

			if (clq_flg != CLQ_REMOVE) {
				clerrno = clquorum_get_nodes(NULL, qdev_list);
				if (clerrno != CL_NOERR) {
					clcommand_perror(clerrno);
					goto cleanup;
				}
			}
		}
	} else {
		// Go through all the operands
		for (operands.start(); operands.end() != 1;
		    operands.next()) {
			this_qdev = operands.getValue();
			if (!this_qdev)
				continue;

			// Get the ccr name
			*disk_ccr_name = '\0';
			if (scconf_didname_slice(this_qdev, didname) == 0) {
				(void) scconf_qdevname(didname, disk_ccr_name);
			}
			if (*disk_ccr_name == '\0') {
				(void) sprintf(disk_ccr_name, this_qdev);
			}

			// Get the type of this quorum device
			tmp_qdev_list.clear();
			clerrno = clquorum_get_quorumdevices(disk_ccr_name,
			    NULL, tmp_qdev_list);
			if (clerrno != CL_NOERR) {
				clcommand_perror(clerrno);
				goto cleanup;
			}

			this_type = tmp_qdev_list.getValue(disk_ccr_name);

			// It could be a node
			if (!this_type && (clq_flg != CLQ_REMOVE)) {
				tmp_qdev_list.clear();
				clerrno = clquorum_get_nodes(this_qdev,
				    tmp_qdev_list);
				if (clerrno != CL_NOERR) {
					clcommand_perror(clerrno);
					goto cleanup;
				}
				this_type = tmp_qdev_list.getValue(this_qdev);
			}

			// If no type is found, this is an invalid device
			if (!this_type) {
				if (first_err == CL_NOERR) {
					first_err = CL_ENOENT;
				}
				clerror("Device \"%s\" does not exist.\n",
				    this_qdev);
				continue;
			}
			if (qdev_list.add(this_qdev, this_type) != 0) {
				clerrno = CL_EINTERNAL;
				clcommand_perror(clerrno);
				goto cleanup;
			}
		}
	}

	// Do the operation to the devices specified in the operands
	for (qdev_list.start(); qdev_list.end() != 1;
	    qdev_list.next()) {
		this_qdev = qdev_list.getName();
		this_type = qdev_list.getValue();

		// Skip
		if (!this_qdev || !this_type)
			continue;

		// Is this type specified in command line?
		if ((new_devicetypes.getSize() != 0) &&
		    (!new_devicetypes.isValue(this_type))) {
			if (first_err == CL_NOERR) {
				first_err = CL_ETYPE;
			}
			clerror("The type of device \"%s\" does not "
			    "match any specified type.\n",
			    this_qdev);
			continue;
		}

		// If the device is a node
		if (strcmp(this_type, "node") == 0) {
			if (clq_flg == CLQ_DISABLE) {
				scconf_err = scconf_maintstate_quorum_node(
				    this_qdev);
			} else if (clq_flg == CLQ_ENABLE) {
				scconf_err = scconf_reset_quorum_node(
				    this_qdev);
			}
		} else {
			if (clq_flg == CLQ_DISABLE) {
				scconf_err = scconf_maintstate_quorum_device(
				    this_qdev);
			} else if (clq_flg == CLQ_ENABLE) {
				scconf_err = scconf_reset_quorum_device(
				    this_qdev);
			} else if (clq_flg == CLQ_REMOVE) {
				char *msgbuffer = (char *)0;
				scconf_err = scconf_rm_quorum_device(this_qdev,
				    optflgs, &msgbuffer);
				// Print detailed error messages
				if (msgbuffer != (char *)0) {
					clcommand_dumpmessages(msgbuffer);
					free(msgbuffer);
				}
			}
		}
		if (scconf_err != SCCONF_NOERR) {
			switch (scconf_err) {
			case SCCONF_ENOEXIST:
				if (strcmp(this_type, "node") == 0) {
					clerror("\"%s\" is not a valid cluster"
					    " node.\n", this_qdev);
				} else {
					clerror("Quorum device \"%s\" does not "
					    "exist.\n", this_qdev);
				}
				clerrno = CL_ENOENT;
				break;

			case SCCONF_EBUSY:
				if ((strcmp(this_type, "node") == 0) &&
				    (clq_flg == CLQ_DISABLE)) {
					clerror("Node \"%s\" cannot be "
					    "disabled while in cluster "
					    "mode.\n",
					    this_qdev, this_qdev);
				} else {
					clerror("Quorum device \"%s\" is "
					    "busy.\n", this_qdev);
				}
				clerrno = CL_EBUSY;
				break;

			case SCCONF_EQUORUM:
				if (clq_flg == CLQ_DISABLE) {
					clerror("Cluster quorum could be "
					    "compromised if you disable "
					    "\"%s\".\n", this_qdev);
				} else if (clq_flg == CLQ_ENABLE) {
					clerror("Cluster quorum could be "
					    "compromised if you enable "
					    "\"%s\".\n", this_qdev);
				} else if (clq_flg == CLQ_REMOVE) {
					clerror("Cluster quorum could be "
					    "compromised if you remove "
					    "\"%s\".\n", this_qdev);
				}
				clerrno = CL_ESTATE;
				break;

			default:
				clerrno = clquorum_conv_scconf_err(scconf_err);
				break;

			} // lint !e788

			if (first_err == CL_NOERR) {
				first_err = clerrno;
			}
		} else if (cmd.isVerbose || (optflgs & vflg)) {
			switch (clq_flg) {
			case CLQ_ENABLE:
				clmessage("Quorum device \"%s\" is enabled.\n",
				    this_qdev);
				break;
			case CLQ_DISABLE:
				clmessage("Quorum device \"%s\" is disabled.\n",
				    this_qdev);
				break;
			case CLQ_REMOVE:
				clmessage("Quorum device \"%s\" is removed.\n",
				    this_qdev);
				break;
			default:
				clerrno = CL_EINTERNAL;
				goto cleanup;
			}
		}
	}

cleanup:
	if (first_err != CL_NOERR) {
		clerrno = first_err;
	}

	return (clerrno);
}

//
// clquorum_preprocess_read_operation
//
//	This is used to get the valid quorum devices into a NameValueList,
//	qdev_list, based on the types and quorum devices in the operands.
//	Non-existing or invalid types and quorum devices are printed in
//	messages. The caller is usually the "list", "show", and
//	"status" subcommand processing.
//
clerrno_t
clquorum_preprocess_read_operation(ValueList &operands, bool print_err,
    ValueList &devicetypes, NameValueList &qdev_list)
{
	clerrno_t clerrno = CL_NOERR;
	int found;
	clerrno_t first_err = CL_NOERR;
	char didname[SCCONF_MAXSTRINGLEN];
	char disk_ccr_name[SCCONF_MAXSTRINGLEN];
	NameValueList tmp_qdev_list;
	ValueList empty_list;
	ValueList new_devicetypes = ValueList(true);
	char *this_type;
	char *this_qdev;
	char *tmp_buff;
	bool operand_all = false;

	// If no operands, apply to all quorum devices.
	if ((operands.getSize() == 0) ||
	    operands.isValue(CLCOMMANDS_WILD_OPERAND)) {
		operand_all = true;
	}

	// Check for invalid types
	for (devicetypes.start(); devicetypes.end() != 1;
	    devicetypes.next()) {
		this_type = devicetypes.getValue();
		if (!this_type)
			continue;

		//
		// Do not error if the type specified is SYSTEM
		//
		if ((strcmp(this_type, "node") != 0) &&
#ifdef WEAK_MEMBERSHIP
			(strcmp(this_type, SCCONF_QUORUM_TYPE_SYSTEM) != 0) &&
#endif
		    !scconf_check_quorum_devicetype(this_type)) {
			clerror("Invalid type \"%s\".\n", this_type);
			if (first_err == CL_NOERR) {
				first_err = CL_ETYPE;
			}
		} else if (strstr(this_type, "scsi") != 0) {
			// Treat "scsi" as "shared_disk"
			new_devicetypes.add("shared_disk");
		} else {
			new_devicetypes.add(this_type);
		}
	}

	// Get the quorum devices by the names in operands and
	// devicetypes.
	if (new_devicetypes.getSize() != 0) {
		clerrno = clquorum_get_quorumdevice_list(operands,
		    new_devicetypes, qdev_list);
	} else {
		clerrno = clquorum_get_quorumdevice_list(operands,
		    devicetypes, qdev_list);
	}

	if (clerrno != CL_NOERR)
		goto cleanup;

	// Get the quroum nodes by the nodes specified in operands
	if ((devicetypes.getSize() == 0) || devicetypes.isValue("node")) {
		clerrno = clquorum_get_nodes_list(operands, qdev_list);
		if (clerrno != CL_NOERR)
			goto cleanup;
	}

#ifdef WEAK_MEMBERSHIP
	//
	// Check if the device type specified is default or "SYSTEM"
	// Fetch the global quorum device and add it to qdev_list
	//
	if ((devicetypes.getSize() == 0) ||
		devicetypes.isValue(SCCONF_QUORUM_TYPE_SYSTEM)) {
		clerrno = clquorum_get_global_quorum_list(operands, qdev_list);
		if (clerrno != CL_NOERR) {
			goto cleanup;
		}
	}
#endif

	// Check for invalid operands
	for (operands.start(); operands.end() != 1;
	    operands.next()) {
		this_qdev = operands.getValue();
		if (!this_qdev)
			continue;

		// Get the quorum name that can be used in CCR
		*disk_ccr_name = '\0';
		if (scconf_didname_slice(this_qdev, didname) == 0) {
			(void) scconf_qdevname(didname, disk_ccr_name);
		}

		// Check if this device is inside the list
		found = 0;
		for (qdev_list.start(); qdev_list.end() != 1;
		    qdev_list.next()) {
			tmp_buff = qdev_list.getName();
			if (!tmp_buff)
				continue;

			if ((strcmp(this_qdev, tmp_buff) == 0) ||
			    (strcmp(tmp_buff, disk_ccr_name) == 0)) {
				found = 1;
				break;
			}
		}

		// This one is valid
		if (found)
			continue;

		// See if this quorum is valid
		found = 0;
		tmp_qdev_list.clear();
		clerrno = clquorum_get_quorumdevices(this_qdev, NULL,
		    tmp_qdev_list);
		if (clerrno != CL_NOERR)
			goto cleanup;

		if (tmp_qdev_list.getSize() != 0) {
			found = 1;
		} else {
			// If not found, it could be a node
			tmp_qdev_list.clear();
			clerrno = clquorum_get_nodes(this_qdev,
			    tmp_qdev_list);
			if (clerrno != CL_NOERR)
				goto cleanup;

			if (tmp_qdev_list.getSize() != 0) {
				found = 1;
			}
		}

		// Give error messages
		if (found) {
			if (print_err) {
				clerror("The type of device \"%s\" does "
				    "not match any specified type.\n",
				    this_qdev);
			}
			if (first_err == CL_NOERR) {
				first_err = CL_ETYPE;
			}
		} else {
			if (print_err) {
				clerror("Device \"%s\" does not exist.\n",
				    this_qdev);
			}
			if (first_err == CL_NOERR) {
				first_err = CL_ENOENT;
			}
		}
	}

cleanup:
	if (first_err != CL_NOERR) {
		clerrno = first_err;
	}

	return (clerrno);
}

bool
clquorum_are_all_nodes_online() {
	int i = 0;
	quorum_status_t *quorum_status = (quorum_status_t *)0;

	if (cluster_get_quorum_status(&quorum_status) != 0) {
		return (false);
	}

	for (i = 0; i < quorum_status->num_nodes; i++) {
		if (quorum_status->nodelist[i].state !=
				QUORUM_STATE_ONLINE) {
			return (false);
		}
	}

	return (true);
}
