//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clquorum_show.cc	1.8	09/02/24 SMI"

//
// Process clquorum "show"
//

#include "clquorum.h"

// Common property names saved in ccr for quorum devices
#define	CLQ_ENABLED	gettext("Enabled")
#define	CLQ_H_ENABLED	gettext("Hosts (enabled)")
#define	CLQ_H_DISABLED	gettext("Hosts (disabled)")

static clerrno_t clquorum_get_qnode_conf(clconf_cluster_t *clconf, char *node,
    char **node_name, node_bitmask_t *conf_node_bitmask,
    NameValueList &qnode_conf);
static clerrno_t clquorum_get_qdev_conf(clconf_cluster_t *clconf,
    char *quorum_name, node_bitmask_t *conf_node_bitmask,
    NameValueList &qdev_conf);
#ifdef WEAK_MEMBERSHIP
static clerrno_t clquorum_get_qglobal_conf(clconf_cluster_t *clconf,
    char *quorum_name, NameValueList &qglobal_conf);
static clerrno_t clquorum_get_show(ValueList &quorumdevices, optflgs_t optflgs,
    ValueList &devicetypes, ValueList &nodelist,
    ClShow *clq_node_show, ClShow *clq_dev_show, ClShow *clq_global_show);
#else
static clerrno_t clquorum_get_show(ValueList &quorumdevices, optflgs_t optflgs,
    ValueList &devicetypes, ValueList &nodelist,
    ClShow *clq_node_show, ClShow *clq_dev_show);
#endif

//
// clquorum "show"
//
clerrno_t
clquorum_show(ClCommand &cmd, ValueList &operands, optflgs_t optflgs,
    ValueList &devicetypes, ValueList &nodelist)
{
	ClShow *prt_show_node = new ClShow(HEAD_NODES);
	ClShow *prt_show_dev = new ClShow(HEAD_QUORUMDEVICES);
#ifdef WEAK_MEMBERSHIP
	ClShow *prt_show_global = new ClShow(HEAD_GLOBALQUORUM);
#endif
	clerrno_t clerrno = CL_NOERR;

	//
	// Get the ClShow objects. Error messages are already
	// printed in clquorum_get_show() so we don't print
	// anything here.
	//
#ifdef WEAK_MEMBERSHIP
	clerrno = clquorum_get_show(operands, optflgs,
	    devicetypes, nodelist, prt_show_node,
	    prt_show_dev, prt_show_global);
#else
	clerrno = clquorum_get_show(operands, optflgs,
	    devicetypes, nodelist, prt_show_node, prt_show_dev);
#endif

	// Print quorum related information of the nodes
	if (prt_show_node->getObjectSize() != 0) {
		(void) prt_show_node->print();
	}

	// Print quorum devices
	if (prt_show_dev->getObjectSize() != 0) {
		(void) prt_show_dev->print();
	}

#ifdef WEAK_MEMBERSHIP
	// Print global quorum
	if (prt_show_global->getObjectSize() != 0) {
		(void) prt_show_global->print();
	}
#endif

	// Delete the show objects
	delete prt_show_node;
	delete prt_show_dev;
#ifdef WEAK_MEMBERSHIP
	delete prt_show_global;
#endif

	return (clerrno);
} //lint !e715

//
// get_clquorum_show_obj
//
//	Get the ClShow object for all the quorum devices,
//	including node inforamtion in the quorum context. The
//	ClShow object must be created before calling this
//	function.
//
#ifdef WEAK_MEMBERSHIP
clerrno_t
get_clquorum_show_obj(optflgs_t optflgs, ClShow *clq_node_show,
    ClShow *clq_dev_show, ClShow *clq_global_show)
#else
clerrno_t
get_clquorum_show_obj(optflgs_t optflgs, ClShow *clq_node_show,
    ClShow *clq_dev_show)
#endif
{
	ValueList empty_list;

	// Check inputs
#ifdef WEAK_MEMBERSHIP
	if (!clq_node_show || !clq_dev_show || !clq_global_show) {
#else
	if (!clq_node_show || !clq_dev_show) {
#endif
		clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}

	// Get all the quorum devices in the system.
#ifdef WEAK_MEMBERSHIP
	return (clquorum_get_show(empty_list, optflgs,
	    empty_list, empty_list, clq_node_show, clq_dev_show,
		clq_global_show));
#else
	return (clquorum_get_show(empty_list, optflgs,
	    empty_list, empty_list, clq_node_show, clq_dev_show));
#endif
}

//
// clquorum_get_show
//
//	Get the ClChow object for quorum devices for printing. The
//	ClShow object must be created before calling this
//	function, and the caller is responsible to free the
//	object afterwards.
//
static clerrno_t
clquorum_get_show(ValueList &quorumdevices, optflgs_t optflgs,
    ValueList &devicetypes, ValueList &nodelist,
#ifdef WEAK_MEMBERSHIP
    ClShow *clq_node_show, ClShow *clq_dev_show, ClShow *clq_global_show)
#else
    ClShow *clq_node_show, ClShow *clq_dev_show)
#endif
{
	NameValueList qdev_list = NameValueList(true);
	NameValueList *qnode_conf, *qdev_conf;
#ifdef WEAK_MEMBERSHIP
	NameValueList *qglobal_conf;
#endif
	NameValueList show_qdev_name, show_node_name;
	clerrno_t clerrno = CL_NOERR;
	int row_count = 0;
	char *quorum_name;
	char *quorum_type;
	char *node_ccrname = (char *)0;
	node_bitmask_t node_bitmask = 0;
	node_bitmask_t conf_node_bitmask = 0;
	bool check_nodes = (nodelist.getSize() > 0) ? true : false;
	clerrno_t first_err = CL_NOERR;
	clconf_cluster_t *clconf = (clconf_cluster_t *)0;
	scconf_errno_t scconf_err = SCCONF_NOERR;

	// Check inputs
#ifdef WEAK_MEMBERSHIP
	if (!clq_node_show || !clq_dev_show || !clq_global_show) {
#else
	if (!clq_node_show || !clq_dev_show) {
#endif
		clerrno = CL_EINTERNAL;
		clcommand_perror(clerrno);
		return (clerrno);
	}

	// Get the valid quorum devices from the user input
	clerrno = clquorum_preprocess_read_operation(quorumdevices,
	    true, devicetypes, qdev_list);
	if (clerrno != CL_NOERR) {
		// Errors are processed in above function.
		if ((clerrno != CL_ETYPE) && (clerrno != CL_ENOENT)) {
			clcommand_perror(clerrno);
			return (clerrno);
		}
		if (first_err == CL_NOERR) {
			first_err = clerrno;
		}
	}

	// Need to check the nodelist
	clerrno = clquorum_get_nodes_bitmask(nodelist, true,
	    &node_bitmask);
	if (clerrno != CL_NOERR) {
		if (first_err == CL_NOERR) {
			first_err = clerrno;
		}
		if (clerrno != CL_ENOENT) {
			goto cleanup;
		}
	}

	// Get the clconf hanlder
	scconf_err = scconf_cltr_openhandle((scconf_cltr_handle_t *)&clconf);
	if (scconf_err != SCCONF_NOERR) {
		clerrno = clquorum_conv_scconf_err(scconf_err);
		if (first_err == CL_NOERR) {
			first_err = clerrno;
		}
		goto cleanup;
	}

	// Process the nodes first
	for (qdev_list.start(); qdev_list.end() != 1; qdev_list.next()) {
		quorum_name = qdev_list.getName();
		quorum_type = qdev_list.getValue();
		if (!quorum_name || !quorum_type)
			continue;

		// Skip node quorum devices
		if (strcmp(quorum_type, "node") != 0)
			continue;

		qnode_conf = new NameValueList;
		clerrno = clquorum_get_qnode_conf(clconf, quorum_name,
		    &node_ccrname, &conf_node_bitmask, *qnode_conf);
		if (clerrno != CL_NOERR)
			goto cleanup;

		// Check with the nodelist
		if (!(node_bitmask & conf_node_bitmask)) {
			free(node_ccrname);
			continue;
		}

		// Set the object name
		clq_node_show->addObject((char *)gettext("Node Name"),
		    node_ccrname);

		// Set properties
		clq_node_show->addProperties(node_ccrname, qnode_conf);
		row_count++;
		free(node_ccrname);
	}

	// Process quorum devices
	for (qdev_list.start(); qdev_list.end() != 1; qdev_list.next()) {
		quorum_name = qdev_list.getName();
		quorum_type = qdev_list.getValue();
		if (!quorum_name || !quorum_type)
			continue;

		// Skip nodes and GLOBAL_QUORUM
#ifdef WEAK_MEMBERSHIP
		if ((strcmp(quorum_type, "node") == 0) ||
			(strcmp(quorum_type, SCCONF_QUORUM_TYPE_SYSTEM) == 0))
#else
		if ((strcmp(quorum_type, "node") == 0))
#endif
			continue;

		qdev_conf = new NameValueList;
		clerrno = clquorum_get_qdev_conf(clconf, quorum_name,
		    &conf_node_bitmask, *qdev_conf);
		if (clerrno != CL_NOERR)
			goto cleanup;

		if (!(node_bitmask & conf_node_bitmask))
			continue;

		// Set the ojbect name
		clq_dev_show->addObject((char *)gettext("Quorum Device Name"),
		    quorum_name);

		// Set properties
		clq_dev_show->addProperties(quorum_name, qdev_conf);
		row_count++;
	}

#ifdef WEAK_MEMBERSHIP
	// Process global quorum object
	for (qdev_list.start(); qdev_list.end() != 1; qdev_list.next()) {
		quorum_name = qdev_list.getName();
		quorum_type = qdev_list.getValue();
		if (!quorum_name || !quorum_type)
			continue;

		if (strcmp(quorum_name, SCCONF_GLOBAL_QUORUM_NAME) != 0) {
			continue;
		}

		qglobal_conf = new NameValueList;
		clerrno = clquorum_get_qglobal_conf(clconf, quorum_name,
		    *qglobal_conf);
		if (clerrno != CL_NOERR)
			goto cleanup;

		// Set the object name
		clq_global_show->addObject((char *)gettext("Name"),
		    SCCONF_GLOBAL_QUORUM_NAME);

		// Set properties
		clq_global_show->addProperties(SCCONF_GLOBAL_QUORUM_NAME,
			qglobal_conf);
		row_count++;
	}
#endif

cleanup:
	// Release the cluster config
	if (clconf != (clconf_cluster_t *)0) {
		clconf_obj_release((clconf_obj_t *)clconf);
	}

	// Return the first error
	if (first_err != CL_NOERR) {
		clerrno = first_err;
	}

	return (clerrno);
} //lint !e715

//
// clquorum_get_qnode_conf
//
//	Get the quorum configuration infor for the node. The "node"
//	can be either node name or nodeid. The confgiuration data is
//	saved into the NameValueList, and the node name is saved to
//	"node_name". Caller needs to free the memory located to
//	"node_name".
//
clerrno_t
clquorum_get_qnode_conf(clconf_cluster_t *clconf, char *nodename,
    char **node_ccrname, node_bitmask_t *conf_node_bitmask,
    NameValueList &qnode_conf)
{
	clerrno_t clerrno = CL_NOERR;
	clconf_iter_t *currnodesi, *nodesi = (clconf_iter_t *)0;
	clconf_obj_t *node;
	scconf_nodeid_t conf_nodeid;
	const char *conf_val;
	char conf_nodeid_str[SCCONF_MAXSTRINGLEN];

	// Check inputs
	if (!nodename || !clconf || (conf_node_bitmask == NULL) ||
	    (node_ccrname == (char **)0)) {
		return (CL_EINTERNAL);
	}

	*conf_node_bitmask = 0;

	// Read the nodes
	nodesi = currnodesi = clconf_cluster_get_nodes(clconf);
	while ((node = clconf_iter_get_current(currnodesi)) != NULL) {
		clconf_iter_advance(currnodesi);

		// Get the node name
		conf_val = clconf_obj_get_name(node);
		if (!conf_val)
			continue;

		// get the node id
		conf_nodeid = (scconf_nodeid_t)clconf_obj_get_id(node);
		if (conf_nodeid == (scconf_nodeid_t)-1) {
			clerrno = CL_EINTERNAL;
			goto cleanup;
		}

		// Convert the nodeid to a string
		(void) sprintf(conf_nodeid_str, "%d", conf_nodeid);

		if ((strcmp(conf_val, nodename) != 0) &&
		    (strcmp(conf_nodeid_str, nodename) != 0))
			continue;

		// Save the node bitmask
		*conf_node_bitmask = 1LL << (conf_nodeid-1);

		// Save the node name
		*node_ccrname = strdup(conf_val);
		if (*node_ccrname == NULL) {
			clerrno = CL_ENOMEM;
			goto cleanup;
		}

		// Add the node id
		if (qnode_conf.add((char *)gettext("Node ID"),
		    conf_nodeid_str) != 0) {
			clerrno = CL_EINTERNAL;
			goto cleanup;
		}

		// Add the vote count
		conf_val = clconf_obj_get_property(node, "quorum_vote");
		if (conf_val) {
			if (qnode_conf.add((char *)gettext("Quorum Vote Count"),
			    (char *)conf_val) != 0) {
				clerrno = CL_EINTERNAL;
				goto cleanup;
			}
		}

		// Add the reservation key
		conf_val = clconf_obj_get_property(node, "quorum_resv_key");
		if (conf_val) {
			if (qnode_conf.add((char *)gettext("Reservation Key"),
			    (char *)conf_val) != 0) {
				clerrno = CL_EINTERNAL;
				goto cleanup;
			}
		}

		// Add the default vote
		conf_val = clconf_obj_get_property(node, "quorum_defaultvote");
		if (conf_val) {
			if (qnode_conf.add((char *)gettext(
			    "Default Vote Count"), (char *)conf_val) != 0) {
				clerrno = CL_EINTERNAL;
				goto cleanup;
			}
		}
		break;
	}

cleanup:
	if (nodesi != (clconf_iter_t *)0)
		clconf_iter_release(nodesi);

	return (clerrno);
}

//
// clquorum_get_qdev_conf
//
//	For the quorum with name quorum_name, get its configuration data
//	from clconf and save it into a NameValuelist. Also save the
//	enabled nodes into the conf_node_bitmask.
//
static clerrno_t
clquorum_get_qdev_conf(clconf_cluster_t *clconf, char *quorum_name,
    node_bitmask_t *conf_node_bitmask, NameValueList &qdev_conf)
{
	clerrno_t clerrno = CL_NOERR;
	int i;
	uint_t j;
	nodeid_t max_nodeid = 0;
	scconf_errno_t scconf_err = SCCONF_NOERR;
	clconf_iter_t *curr_devsi, *devsi = (clconf_iter_t *)0;
	clconf_obj_t *obj_dev;
	const char *conf_name;
	const char *conf_val;
	char *nodename = (char *)0;
	char tmp_buf[SCCONF_MAXSTRINGLEN];
	char enabled_h[SCCONF_MAXSTRINGLEN];
	char disabled_h[SCCONF_MAXSTRINGLEN];

	//
	// quorum properties saved in ccr. For type-specific properties, it
	// must match the name defined in scqd_netappnas.c and
	// scqd_quorum_server.c
	//
	char *CLQ_CONF_PROP[] = {
		"votecount", "gdevname", "type", "access_mode", "timeout",
		"path_",
		"filer", "lun_id",
		"qshost", "port",
		(char *)0
	};
	char *CLQ_CONF_PNAME[] = {
		(char *)gettext("Votes"), (char *)gettext("Global Name"),
		(char *)gettext("Type"), (char *)gettext("Access Mode"),
		(char *)gettext("Timeout"), "", (char *)gettext("Filer Name"),
		(char *)gettext("Lun ID"),
		(char *)gettext("Quorum Server Host"),
		(char *)gettext("Port"),
		(char *)0
	};

	// Check inputs
	if (!quorum_name || !clconf || !conf_node_bitmask) {
		return (CL_EINTERNAL);
	}

	// Get the largest nodeid in the system.
	curr_devsi = clconf_cluster_get_nodes(clconf);
	while ((obj_dev = clconf_iter_get_current(curr_devsi)) != NULL) {
		if (clconf_obj_get_id(obj_dev) > (int)max_nodeid) {
			max_nodeid = (nodeid_t)clconf_obj_get_id(obj_dev);
		}
		clconf_iter_advance(curr_devsi);
	}
	clconf_iter_release(curr_devsi);

	// Go through the quorum devices
	devsi = curr_devsi = clconf_cluster_get_quorum_devices(clconf);
	while ((obj_dev = clconf_iter_get_current(curr_devsi)) != NULL) {
		clconf_iter_advance(curr_devsi);

		// Get the quorum device name
		conf_name = clconf_obj_get_name(obj_dev);
		if (!conf_name)
			continue;

		// Check if name match
		if (strcmp(quorum_name, conf_name) != 0)
			continue;

		// Get state
		conf_val = clconf_obj_enabled(obj_dev) ?
		    gettext("yes") : gettext("no");
		if (qdev_conf.add((char *)CLQ_ENABLED,
		    (char *)conf_val) != 0) {
			clerrno = CL_EINTERNAL;
			goto cleanup;
		}

		// Get its properties
		*enabled_h = '\0';
		*disabled_h = '\0';
		for (i = 0; CLQ_CONF_PROP[i]; i++) {
			// Special process for path_<n>
			if (strcmp(CLQ_CONF_PROP[i], "path_") == 0) {
				*conf_node_bitmask = 0;
				for (j = 1; j <= max_nodeid; j++) {
					(void) sprintf(tmp_buf, "path_%u", j);
					conf_val = clconf_obj_get_property(
					    obj_dev, tmp_buf);
					if (!conf_val)
						continue;

					// Get node name
					scconf_err = scconf_get_nodename(j,
					    &nodename);
					if (scconf_err != SCCONF_NOERR) {
						clerrno =
						    clquorum_conv_scconf_err(
						    scconf_err);
						goto cleanup;
					}

					// Set the node bitmask
					if (strcmp(conf_val, "enabled") == 0) {
						*conf_node_bitmask |=
						    1LL << (j-1);
						if (strlen(enabled_h) > 1) {
							(void) strcat(
							    enabled_h, ", ");
						}
						(void) strcat(enabled_h,
						    nodename);
					} else {
						if (strlen(disabled_h) > 1) {
							(void) strcat(
							    disabled_h, ", ");
						}
						(void) strcat(disabled_h,
						    nodename);
					}
					free(nodename);
				}

				// Add the paths
				if (strlen(enabled_h) > 1) {
					if (qdev_conf.add(
					    (char *)CLQ_H_ENABLED,
					    enabled_h)
					    != 0) {
						clerrno = CL_EINTERNAL;
						goto cleanup;
					}
				}
				if (strlen(disabled_h) > 1) {
					if (qdev_conf.add(
					    (char *)CLQ_H_DISABLED,
					    disabled_h)
					    != 0) {
						clerrno = CL_EINTERNAL;
						goto cleanup;
					}
				}
				continue;
			}

			// Other properties
			conf_val = clconf_obj_get_property(obj_dev,
			    CLQ_CONF_PROP[i]);

			// Type
			if (strcmp(CLQ_CONF_PROP[i], "type") == 0) {
				if (!conf_val) {
					// No type? Use access_mode
					conf_val = clconf_obj_get_property(
					    obj_dev, "access_mode");
					if (!conf_val)
						continue;
				}

				// Treat "scsi" as "shared_disk"
				if (strstr(conf_val, "scsi") != 0) {
					conf_val = "shared_disk";
				}
				(void) qdev_conf.add(CLQ_CONF_PNAME[i],
				    (char *)conf_val);
				continue;
			}

			if (conf_val) {
				if (qdev_conf.add(CLQ_CONF_PNAME[i],
				    (char *)conf_val) != 0) {
					clerrno = CL_EINTERNAL;
					goto cleanup;
				}
			}
		}
	}

cleanup:
	if (devsi != (clconf_iter_t *)0)
		clconf_iter_release(devsi);

	return (clerrno);
}

#ifdef WEAK_MEMBERSHIP
//
// clquorum_get_qglobal_conf
//
//	For the quorum with global quorum name, get its configuration data
//	from clconf and save it into a NameValuelist.
//
static clerrno_t
clquorum_get_qglobal_conf(clconf_cluster_t *clconf, char *quorum_name,
    NameValueList &qglobal_conf) {

    clerrno_t clerrno = CL_NOERR;
    char *value = NULL;

    qglobal_conf.add((char *)gettext("Type"), SCCONF_QUORUM_TYPE_SYSTEM);

    value = (char *)clconf_obj_get_property((clconf_obj_t *)clconf,
		CLPROP_MULTIPLE_PARTITIONS);
	if (value == NULL || strlen(value) < 0) {
		qglobal_conf.add((char *)"multiple_partitions",
				(char *)SCCONF_MULTI_PARTITIONS_DFLT);
	} else {
		qglobal_conf.add((char *)"multiple_partitions",
				(char *)value);
	}

	value = (char *)clconf_obj_get_property((clconf_obj_t *)clconf,
				CLPROP_PING_TARGETS);
	if (value != NULL && strlen(value) > 0) {
		qglobal_conf.add((char *)"ping_targets",
				(char *)value);
	} else {
		qglobal_conf.add((char *)"ping_targets",
				(char *)"<NULL>");
	}

	return (clerrno);
}
#endif
