//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clquorum_status.cc	1.8	09/04/20 SMI"

//
// Process clquorum "status"
//

#include "clquorum.h"
#include "ClStatus.h"

#ifdef WEAK_MEMBERSHIP
static clerrno_t clquorum_get_status(ValueList &operands, optflgs_t optflgs,
    ValueList &devicetypes, ValueList &nodelist,
    ClStatus *status_sum, ClStatus *status_node, ClStatus *status_dev,
    ClStatus *status_global);
#else
static clerrno_t clquorum_get_status(ValueList &operands, optflgs_t optflgs,
    ValueList &devicetypes, ValueList &nodelist,
    ClStatus *status_sum, ClStatus *status_node, ClStatus *status_dev);
#endif

//
// clquorum "status"
//
clerrno_t
clquorum_status(ClCommand &cmd, ValueList &operands, optflgs_t optflgs,
    ValueList &devicetypes, ValueList &nodelist)
{
	ClStatus *status_sum = new ClStatus(cmd.isVerbose ? true : false);
	ClStatus *status_node = new ClStatus(cmd.isVerbose ? true : false);
	ClStatus *status_dev = new ClStatus(cmd.isVerbose ? true : false);
#ifdef WEAK_MEMBERSHIP
	ClStatus *status_global = new ClStatus(cmd.isVerbose ? true : false);
#endif
	clerrno_t clerrno = CL_NOERR;

#ifdef WEAK_MEMBERSHIP
	// Get quorum status
	clerrno = clquorum_get_status(operands, optflgs, devicetypes,
	    nodelist, status_sum, status_node, status_dev, status_global);
#else
	// Get quorum status
	clerrno = clquorum_get_status(operands, optflgs, devicetypes,
	    nodelist, status_sum, status_node, status_dev);
#endif
	// Print it
	status_sum->print();
	status_node->print();
	status_dev->print();
#ifdef WEAK_MEMBERSHIP
	status_global->print();
#endif

	// Delete the status object
	delete status_sum;
	delete status_node;
	delete status_dev;
#ifdef WEAK_MEMBERSHIP
	delete status_global;
#endif

	return (clerrno);
}

//
// get_clquorum_status_obj
//
//	Get the status of all quorum devices in the system.
#ifdef WEAK_MEMBERSHIP
clerrno_t
get_clquorum_status_obj(optflgs_t optflgs,
    ClStatus *status_sum, ClStatus *status_node, ClStatus *status_dev,
    ClStatus *status_global)
#else
clerrno_t
get_clquorum_status_obj(optflgs_t optflgs,
    ClStatus *status_sum, ClStatus *status_node, ClStatus *status_dev)
#endif // WEAK_MEMBERSHIP
{
	ValueList empty_list;

	// Get status of all quorum devices
#ifdef WEAK_MEMBERSHIP
	return (clquorum_get_status(empty_list, optflgs, empty_list,
	    empty_list, status_sum, status_node, status_dev, status_global));
#else
	return (clquorum_get_status(empty_list, optflgs, empty_list,
	    empty_list, status_sum, status_node, status_dev));
#endif
}

//
// clquorum_get_status
//
//	Gets the quorum status including quorum status summary,
//	node quorum status, and quorum device status. These
//	status are saved into ClStatus object for printing.
//
clerrno_t
#ifdef WEAK_MEMBERSHIP
clquorum_get_status(ValueList &operands, optflgs_t optflgs,
    ValueList &devicetypes, ValueList &nodelist,
    ClStatus *status_sum, ClStatus *status_node,
    ClStatus *status_dev, ClStatus *status_global)
#else
clquorum_get_status(ValueList &operands, optflgs_t optflgs,
    ValueList &devicetypes, ValueList &nodelist,
    ClStatus *status_sum, ClStatus *status_node,
    ClStatus *status_dev)
#endif
{
	NameValueList qdev_list = NameValueList(true);
	clerrno_t clerrno = CL_NOERR;
	uint_t i;
	quorum_status_t *quorum_status = (quorum_status_t *)0;
	quorum_device_status_t *qdev_status;
	quorum_node_status_t *node_status;
	int found;
	int node_count, qdev_count, global_count;
	char *quorum_name;
	char *quorum_type;
	char *nodename = (char *)0;
	char ccr_gdevname[SCCONF_MAXSTRINGLEN];
	nodeid_t nodeid;
	node_bitmask_t node_bitmask;
	node_bitmask_t conf_node_bitmask;
	bool check_nodes = (nodelist.getSize() > 0) ? true : false;
	char buff1[SCCONF_MAXSTRINGLEN];
	char buff2[SCCONF_MAXSTRINGLEN];
	char buff3[SCCONF_MAXSTRINGLEN];
	clerrno_t first_err = CL_NOERR;
	bool set_title = false;
	bool print_node = false;
	bool print_qdev = false;
#ifdef WEAK_MEMBERSHIP
	bool print_global = false;
	scconf_namelist_t *ping_targets_list;
	scconf_namelist_t *listptr;
	clconf_cluster_t *clconf = (clconf_cluster_t *)0;
	scconf_errno_t scconf_err = SCCONF_NOERR;
#endif

	// Check inputs
#ifdef WEAK_MEMBERSHIP
	if (!status_sum || !status_node || !status_dev || !status_global) {
#else
	if (!status_sum || !status_node || !status_dev) {
#endif
		clerrno = CL_EINTERNAL;
		clcommand_perror(clerrno);
		return (clerrno);
	}

	// Get the valid quorum devices from the user input
	clerrno = clquorum_preprocess_read_operation(operands,
	    true, devicetypes, qdev_list);
	if (clerrno != CL_NOERR) {
		if ((clerrno != CL_ETYPE) && (clerrno != CL_ENOENT)) {
			clcommand_perror(clerrno);
			return (clerrno);
		}
		if (first_err == CL_NOERR) {
			first_err = clerrno;
		}
	} else {
		// Check if need to print the headings
		for (devicetypes.start(); !devicetypes.end();
		    devicetypes.next()) {
			quorum_type = devicetypes.getValue();
			if (!quorum_type)
				continue;
#ifdef WEAK_MEMBERSHIP
			if (strcmp(quorum_type,
				SCCONF_QUORUM_TYPE_SYSTEM) == 0) {
				print_global = true;
			} else
#endif
				if (strcmp(quorum_type, "node") == 0) {
				print_node = true;
			} else {
				print_qdev = true;
			}
		}
		if (devicetypes.getSize() == 0) {
			for (qdev_list.start(); !qdev_list.end();
			    qdev_list.next()) {
				quorum_type = qdev_list.getValue();
				if (!quorum_type)
					continue;
#ifdef WEAK_MEMBERSHIP
				if (strcmp(quorum_type,
					SCCONF_QUORUM_TYPE_SYSTEM) == 0) {
					print_global = true;
				} else
#endif
					if (strcmp(quorum_type, "node") == 0) {
					print_node = true;
				} else {
					print_qdev = true;
				}
			}
		}
	}

	// Need to check the nodelist
	clerrno = clquorum_get_nodes_bitmask(nodelist, true,
	    &node_bitmask);
	if (clerrno != CL_NOERR) {
		if (first_err == CL_NOERR) {
			first_err = clerrno;
		}
		if (clerrno != CL_ENOENT) {
			goto cleanup;
		}
	}

	// Get the current/immediate quorum status.
	if (cluster_get_immediate_quorum_status(&quorum_status) != 0) {
		clerrno = CL_EINTERNAL;
		clcommand_perror(clerrno);
		if (first_err == CL_NOERR) {
			first_err = clerrno;
		}
		goto cleanup;
	}

	// Set the heading title
	if (!check_nodes && (devicetypes.getSize() == 0) &&
	    (operands.getSize() == 0)) {
		status_sum->setHeadingTitle(HEAD_QUORUM);

		// Vote summary section
		status_sum->setSectionTitle(SECTION_QUORUM_SUMMARY);
		status_sum->setHeadings((char *)gettext("Needed"),
		    (char *)gettext("Present"), (char *)gettext("Possible"));
		status_sum->setInitialIndent(12);
		status_sum->disableStatusAligning(true);
		// Add the votes
		(void) sprintf(buff1, "%d",
		    quorum_status->votes_needed_for_quorum);
		(void) sprintf(buff2, "%d", quorum_status->current_votes);
		(void) sprintf(buff3, "%d", quorum_status->configured_votes);
		status_sum->addRow(buff1, buff2, buff3);

		set_title = true;
	}

	// Process the quorum devices
	node_count = qdev_count = 0;
#ifdef WEAK_MEMBERSHIP
	global_count = 0;
#endif
	for (qdev_list.start(); qdev_list.end() != 1; qdev_list.next()) {
		quorum_name = qdev_list.getName();
		quorum_type = qdev_list.getValue();
		if (!quorum_name || !quorum_type)
			continue;

		found = 0;
#ifdef WEAK_MEMBERSHIP
		if (strcmp(quorum_type, SCCONF_QUORUM_TYPE_SYSTEM) == 0) {
			//
			// Fetch all the nodes in the cluster
			// Fetch all configured ping targets
			// For each node, ping all configured targets
			//
			// Get the clconf hanlder
			scconf_err =
				scconf_cltr_openhandle((scconf_cltr_handle_t *)
							&clconf);
			if (scconf_err != SCCONF_NOERR) {
				clerrno = clquorum_conv_scconf_err(scconf_err);
				if (first_err == CL_NOERR) {
					first_err = clerrno;
				}
				goto cleanup;
			}

			if (scconf_get_ping_targets(clconf, &ping_targets_list)
				!= SCCONF_NOERR) {
				clerrno = CL_EINTERNAL;
				if (first_err == CL_NOERR) {
					first_err = clerrno;
				}

				clcommand_perror(clerrno);
				goto cleanup;
			}

			for (i = 0; i < quorum_status->num_nodes; i++) {
				// Get the node name.
				if (scconf_get_nodename(
					quorum_status->nodelist[i].nid,
					&nodename) != SCCONF_NOERR) {
					if (first_err == CL_NOERR) {
						first_err = CL_EINTERNAL;
					}
					clcommand_perror(CL_EINTERNAL);
					goto cleanup;
				}

				//
				// Get the node status
				//
				node_status = &quorum_status->nodelist[i];

				for (listptr = ping_targets_list;  listptr;
					listptr =
					    listptr->scconf_namelist_next) {

					//
					// Check for the health of the ping
					// target only when the node is
					// an active member of the cluster
					// i.e., ONLINE state
					//
					if (node_status->state ==
						QUORUM_STATE_ONLINE) {
						scconf_err =
						scconf_health_check_status(
							nodename,
// CSTYLED
							listptr->scconf_namelist_name);
					} else {
						//
						// Node if OFFLINE
						//
						scconf_err = SCCONF_EUNKNOWN;
					}

					if (scconf_err == SCCONF_NOERR) {
					    status_global->addRow(nodename,
						(char *)gettext("Ping Targets"),
						listptr->scconf_namelist_name,
						(char *)gettext("Ok"));
					} else if (scconf_err ==
							SCCONF_EINVAL) {
					    status_global->addRow(nodename,
						(char *)gettext("Ping Targets"),
						listptr->scconf_namelist_name,
						(char *)gettext("Fail"));
					} else {
					    status_global->addRow(nodename,
						(char *)gettext("Ping Targets"),
						listptr->scconf_namelist_name,
						(char *)gettext("Unknown"));
					}
				}

				free(nodename);

				if (i != (quorum_status->num_nodes - 1))
				status_global->addRow("", "", "", "");
			}

			global_count++;
		} else
		//
		// End of special processing required for the
		// GLOBAL_QUORUM device. Proceed with the regular
		// processing if the quorum device type is not
		// SYSTEM
		//
#endif
			if (strcmp(quorum_type, "node") == 0) {
			// Find the votes from nodes
			// Get the node id
			if (scconf_get_nodeid(quorum_name, &nodeid) !=
			    SCCONF_NOERR) {
				clerrno = CL_EINTERNAL;
				if (first_err == CL_NOERR) {
					first_err = clerrno;
				}
				clcommand_perror(clerrno);
				goto cleanup;
			}

			// Find the vote counts of this node
			for (i = 0; i < quorum_status->num_nodes; i++) {
				if (quorum_status->nodelist[i].nid ==
				    nodeid) {
					found = 1;
					break;
				}
			}

			if (!found)
				continue;

			conf_node_bitmask = 1LL << (nodeid-1);
			if (!(node_bitmask & conf_node_bitmask))
				continue;

			// Get the node name.
			if (scconf_get_nodename(nodeid, &nodename) !=
			    SCCONF_NOERR) {
				if (first_err == CL_NOERR) {
					first_err = CL_EINTERNAL;
				}
				clcommand_perror(CL_EINTERNAL);
				goto cleanup;
			}

			// Get the votes to the row
			node_status = &quorum_status->nodelist[i];
			(void) sprintf(buff1, "%d",
			    (node_status->state == QUORUM_STATE_ONLINE) ?
			    node_status->votes_configured : 0);
			(void) sprintf(buff2, "%d",
			    node_status->votes_configured);
			// Get the status
			(void) sprintf(buff3, "%s",
			    (node_status->state == QUORUM_STATE_ONLINE) ?
			    "Online" : "Offline");

			// Add to the row
			status_node->addRow(nodename, buff1, buff2,
			    buff3);
			free(nodename);
			node_count++;
		} else {
			// Find the votes for the quorum device
			qdev_status = NULL;
			for (i = 0; i < quorum_status->num_quorum_devices;
			    i++) {
				qdev_status =
				    &quorum_status->quorum_device_list[i];
				if (!qdev_status ||
				    (qdev_status->gdevname == NULL)) {
					continue;
				}

				// Get its gdevname in ccr
				*ccr_gdevname = '\0';
				(void) scconf_didname_slice(quorum_name,
				    ccr_gdevname);

				if ((strcmp(quorum_name, qdev_status->gdevname)
				    == 0) ||
				    (strcmp(ccr_gdevname, qdev_status->gdevname)
				    == 0)) {
					found = 1;
					break;
				}
			}

			if (!found) {
				if (check_nodes)
					continue;

				//
				// quorum_status doesn't include quorum devices
				// with 0 vote; So if a valid quorum is
				// not included in quorum_status, its
				// vote is 0.
				//
				(void) sprintf(buff1, "0");
				(void) sprintf(buff2, "0");
				(void) sprintf(buff3, gettext("Offline"));

				status_dev->addRow(quorum_name, buff1,
				    buff2, buff3);
				qdev_count++;
				continue;
			}

			conf_node_bitmask =
			    qdev_status->nodes_with_enabled_paths;
			if (!(node_bitmask & conf_node_bitmask))
				continue;

			// Get the quorum device votes and status
			(void) sprintf(buff1, "%d",
			    (qdev_status->state == QUORUM_STATE_ONLINE) ?
			    qdev_status->votes_configured : 0);
			(void) sprintf(buff2, "%d",
			    qdev_status->votes_configured);
			// Get the status
			(void) sprintf(buff3, "%s",
			    (qdev_status->state == QUORUM_STATE_ONLINE) ?
			    gettext("Online") : gettext("Offline"));

			// Add to the row
			status_dev->addRow(quorum_name, buff1, buff2,
			    buff3);
			qdev_count++;
		}
	}

	// Votes by node: headings
	if (node_count || ((first_err == CL_NOERR) && print_node)) {
		if (!set_title) {
			status_node->setHeadingTitle(HEAD_QUORUM);
			set_title = true;
		}
		status_node->setSectionTitle(SECTION_QUORUM_NODES);
		status_node->setHeadings((char *)gettext("Node Name"),
		    (char *)gettext("Present"), (char *)gettext("Possible"),
		    (char *)gettext("Status"));
		status_node->setStatusColumn(4);
	}

	// Votes by quorum device: headings
	if (qdev_count || ((first_err == CL_NOERR) && print_qdev)) {
		if (!set_title) {
			status_dev->setHeadingTitle(HEAD_QUORUM);
			set_title = true;
		}
		status_dev->setSectionTitle(SECTION_QUORUM_DEVICES);
		status_dev->setHeadings((char *)gettext("Device Name"),
		    (char *)gettext("Present"), (char *)gettext("Possible"),
		    (char *)gettext("Status"));
		status_dev->setStatusColumn(4);
	}

#ifdef WEAK_MEMBERSHIP
	// Health Check : headings
	if (global_count || ((first_err == CL_NOERR) && print_global)) {
		if (!set_title) {
			status_global->setHeadingTitle(HEAD_QUORUM);
			set_title = true;
		}
		status_global->setSectionTitle(SECTION_GLOBAL_QUORUM);
		status_global->setHeadings((char *)gettext("Node Name"),
		    (char *)gettext("Health Check Type"),
			(char *)gettext("Entities"),
		    (char *)gettext("Status"));
		status_global->setStatusColumn(4);
	}
#endif
cleanup:
	// Return the first error
	if (first_err != CL_NOERR) {
		clerrno = first_err;
	}

	if (check_nodes) {
		cluster_release_quorum_status(quorum_status);
	}

	return (clerrno);
} //lint !e715
