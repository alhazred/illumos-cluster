//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clquorum_export.cc	1.4	09/01/16 SMI"

//
// Process clquorum "export"
//

#include "clquorum.h"
#include "ClXml.h"

static clerrno_t clquorum_get_qnode_xml(clconf_cluster_t *clconf,
    char *node_name, ClquorumExport *clq_export);
static clerrno_t clquorum_get_qdev_xml(clconf_cluster_t *clconf,
    char *quorum_name, ClquorumExport *clq_export);
#ifdef WEAK_MEMBERSHIP
static clerrno_t clquorum_get_qglobal_xml(clconf_cluster_t *clconf,
    char *global_quorum_name, ClquorumExport *clq_export);
#endif
//
// This function returns a ClquorumExport object populated with
// all of the quorum information from the cluster.  Caller is
// responsible for releasing the ClcmdExport object.
//
ClcmdExport *
get_clquorum_xml_elements()
{
	ClquorumExport *quorum = new ClquorumExport();
	NameValueList qdev_list = NameValueList(true);
	clerrno_t clerrno = CL_NOERR;
	ValueList empty_list;
	clconf_cluster_t *clconf = (clconf_cluster_t *)0;
	char *quorum_name;
	char *quorum_type;

	// Get all the quorum devices in the system
	(void) clquorum_preprocess_read_operation(empty_list,
	    true, empty_list, qdev_list);

	// If nothing to export?
	if (qdev_list.getSize() == 0) {
		return (quorum);
	}

	// Get the clconf hanlder
	if (scconf_cltr_openhandle((scconf_cltr_handle_t *)&clconf) !=
	    SCCONF_NOERR) {
		return (quorum);
	}

	// Process the quorum list
	for (qdev_list.start(); qdev_list.end() != 1; qdev_list.next()) {
		quorum_name = qdev_list.getName();
		quorum_type = qdev_list.getValue();
		if (!quorum_name || !quorum_type)
			continue;

		// Skip non-nodo quorum devices
#ifdef WEAK_MEMBERSHIP
		if (strcmp(quorum_type, SCCONF_QUORUM_TYPE_SYSTEM) == 0) {
			clerrno = quorum->addGlobalQuorum((char *)quorum_name,
				(char *)quorum_type);
		} else
#endif
			if (strcmp(quorum_type, "node") == 0) {
			clerrno = quorum->addQuorumNode((char *)quorum_name);
		} else {
			clerrno = quorum->addQuorumDevice((char *)quorum_name,
			    (char *)quorum_type);
		}
		if (clerrno != CL_NOERR)
			continue;

		// Add the node into the xml element
#ifdef WEAK_MEMBERSHIP
		if (strcmp(quorum_type, SCCONF_QUORUM_TYPE_SYSTEM) == 0) {
			(void) clquorum_get_qglobal_xml(clconf, quorum_name,
				quorum);
		} else
#endif
			if (strcmp(quorum_type, "node") == 0) {
			(void) clquorum_get_qnode_xml(clconf, quorum_name,
			    quorum);
		} else {
			(void) clquorum_get_qdev_xml(clconf, quorum_name,
			    quorum);
		}
	}

	// Release the cluster config
	if (clconf != (clconf_cluster_t *)0) {
		clconf_obj_release((clconf_obj_t *)clconf);
	}

	return (quorum);
}

clerrno_t
clquorum_export(ClCommand &cmd, ValueList &operands, optflgs_t optflgs,
    char *clconfiguration, ValueList &devicetypes)
{
	ClXml clq_clxml;
	ClquorumExport *clq_export = new ClquorumExport();
	NameValueList qdev_list = NameValueList(true);
	clerrno_t clerrno = CL_NOERR;
	clerrno_t first_err = CL_NOERR;
	clconf_cluster_t *clconf = (clconf_cluster_t *)0;
	scconf_errno_t scconf_err = SCCONF_NOERR;
	char *export_file = (char *)0;
	char *quorum_name;
	char *quorum_type;

	// Check inputs
	if (!clconfiguration) {
		export_file = strdup("-");
	} else {
		export_file = strdup(clconfiguration);
	}
	if (!export_file) {
		clerrno = first_err = CL_ENOMEM;
		clcommand_perror(clerrno);
		goto cleanup;
	}

	// Get the valid quorum devices from the user input
	clerrno = clquorum_preprocess_read_operation(operands,
	    true, devicetypes, qdev_list);
	if (clerrno != CL_NOERR) {
		if (first_err == CL_NOERR) {
			first_err = clerrno;
		}
		if ((clerrno != CL_ETYPE) && (clerrno != CL_ENOENT)) {
			clcommand_perror(clerrno);
			goto cleanup;
		}
	}

	// If nothing to export?
	if (qdev_list.getSize() == 0) {
		goto cleanup;
	}

	// Get the clconf hanlder
	scconf_err = scconf_cltr_openhandle((scconf_cltr_handle_t *)&clconf);
	if (scconf_err != SCCONF_NOERR) {
		clerrno = clquorum_conv_scconf_err(scconf_err);
		if (first_err == CL_NOERR) {
			first_err = clerrno;
		}
		goto cleanup;
	}

	// Process the quorum devices
	for (qdev_list.start(); qdev_list.end() != 1; qdev_list.next()) {
		quorum_name = qdev_list.getName();
		quorum_type = qdev_list.getValue();
		if (!quorum_name || !quorum_type)
			continue;

		// Add the quorum into the xml element
#ifdef WEAK_MEMBERSHIP
		if (strcmp(quorum_type, SCCONF_QUORUM_TYPE_SYSTEM) == 0) {
			clerrno = clq_export->addGlobalQuorum(
			    (char *)quorum_name, (char *)quorum_type);
		} else
#endif
			if (strcmp(quorum_type, "node") == 0) {
			clerrno = clq_export->addQuorumNode(
			    (char *)quorum_name);
		} else {
			clerrno = clq_export->addQuorumDevice(
			    (char *)quorum_name, (char *)quorum_type);
		}
		if (clerrno != CL_NOERR) {
			if (first_err == CL_NOERR) {
				first_err = clerrno;
			}
			clcommand_perror(clerrno);
			continue;
		}

		// Get quorum node configuration data to ClquorumExport
#ifdef WEAK_MEMBERSHIP
		if (strcmp(quorum_type, SCCONF_QUORUM_TYPE_SYSTEM) == 0) {
			clerrno = clquorum_get_qglobal_xml(clconf, quorum_name,
			    clq_export);
		} else
#endif
			if (strcmp(quorum_type, "node") == 0) {
			clerrno = clquorum_get_qnode_xml(clconf, quorum_name,
			    clq_export);
		} else {
			clerrno = clquorum_get_qdev_xml(clconf, quorum_name,
			    clq_export);
		}
		if (clerrno != CL_NOERR) {
			if (first_err == CL_NOERR) {
				first_err = clerrno;
			}
			clcommand_perror(clerrno);
		}
	}

	// Create xml
	clerrno = clq_clxml.createExportFile(CLQUORUM_CMD, export_file,
	    clq_export);
	if ((clerrno != CL_NOERR) && (first_err == CL_NOERR)) {
		first_err = clerrno;
	}

cleanup:
	// Release the cluster config
	if (clconf != (clconf_cluster_t *)0) {
		clconf_obj_release((clconf_obj_t *)clconf);
	}

	// Free memory
	if (export_file) {
		free(export_file);
	}

	delete clq_export;

	// Return the first error
	if (first_err != CL_NOERR) {
		clerrno = first_err;
	}

	return (clerrno);
} //lint !e715

//
// clquorum_get_qnode_xml
//
//	Get the node quorum configuration data, and add them into the
//	ClcmdExport object.
//
static clerrno_t
clquorum_get_qnode_xml(clconf_cluster_t *clconf, char *node_name,
    ClquorumExport *clq_export)
{
	clerrno_t clerrno = CL_NOERR;
	clconf_iter_t *currnodesi, *nodesi = (clconf_iter_t *)0;
	clconf_obj_t *node;
	scconf_nodeid_t conf_nodeid;
	const char *conf_val;
	const char *node_ccrname;
	char conf_nodeid_str[SCCONF_MAXSTRINGLEN];

	// Check inputs
	if (!node_name || !clconf || (clq_export == NULL)) {
		return (CL_EINTERNAL);
	}

	// Read the nodes
	nodesi = currnodesi = clconf_cluster_get_nodes(clconf);
	while ((node = clconf_iter_get_current(currnodesi)) != NULL) {
		clconf_iter_advance(currnodesi);

		// Get the node name
		conf_val = clconf_obj_get_name(node);
		if (!conf_val)
			continue;

		// get the node id
		conf_nodeid = (scconf_nodeid_t)clconf_obj_get_id(node);
		if (conf_nodeid == (scconf_nodeid_t)-1) {
			clerrno = CL_EINTERNAL;
			goto cleanup;
		}
		// Convert the nodeid to a string
		(void) sprintf(conf_nodeid_str, "%d", conf_nodeid);

		if ((strcmp(conf_val, node_name) != 0) &&
		    (strcmp(conf_nodeid_str, node_name) != 0))
			continue;

		// Add the node name
		node_ccrname = conf_val;
		clerrno = clq_export->addQuorumNode((char *)node_ccrname);
		if (clerrno != CL_NOERR) {
			goto cleanup;
		}

		// Add the vote count
		conf_val = clconf_obj_get_property(node, "quorum_vote");
		if (conf_val) {
			clerrno = clq_export->addQuorumNodeProperty(
			    (char *)node_ccrname, (char *)"votecount",
			    (char *)conf_val, true);
			if (clerrno != CL_NOERR) {
				goto cleanup;
			}
		}

		// Add the reservation key
		conf_val = clconf_obj_get_property(node, "quorum_resv_key");
		if (conf_val) {
			clerrno = clq_export->addQuorumNodeProperty(
			    (char *)node_ccrname, (char *)"reservation_key",
			    (char *)conf_val, true);
			if (clerrno != CL_NOERR) {
				goto cleanup;
			}
		}

		// Add the default vote
		conf_val = clconf_obj_get_property(node, "quorum_defaultvote");
		if (conf_val) {
			clerrno = clq_export->addQuorumNodeProperty(
			    (char *)node_ccrname, (char *)"default_vote",
			    (char *)conf_val, true);
			if (clerrno != CL_NOERR) {
				goto cleanup;
			}
		}
		break;
	}

cleanup:
	if (nodesi != (clconf_iter_t *)0) {
		clconf_iter_release(nodesi);
	}

	return (clerrno);
}

//
// clquorum_get_qdev_xml
//
//	Get the quorum device configuration data, and add them into the
//	ClcmdExport object.
//
static clerrno_t
clquorum_get_qdev_xml(clconf_cluster_t *clconf, char *quorum_name,
    ClquorumExport *clq_export)
{
	clerrno_t clerrno = CL_NOERR;
	int i, found;
	uint_t j;
	nodeid_t max_nodeid = 0;
	scconf_errno_t scconf_err = SCCONF_NOERR;
	clconf_iter_t *curr_devsi, *devsi = (clconf_iter_t *)0;
	clconf_obj_t *obj_dev;
	const char *conf_name;
	const char *conf_val;
	char *nodename = (char *)0;
	char tmp_buf[SCCONF_MAXSTRINGLEN];

	//
	// quorum properties saved in ccr. For type-specific properties, it
	// must match the name defined in scqd_netappnas.c and
	// scqd_quorum_server.c
	//
	char *CLQ_CONF_PROP[] = {
		"votecount", "gdevname", "type", "access_mode", "timeout",
		"filer", "lun_id",
		"qshost", "port",
		(char *)0
	};

	//
	// "true" as the corresponding CLQ_CONF_PROP can not be changed by
	// the user; and "false" as it can be modified by the user in the
	// xml confgiuration file.
	//
	bool CLQ_READ_ONLY[] = {true, true, false, true, false,
		false, false,
		false, false
	};

	// Check inputs
	if (!quorum_name || !clconf || !clq_export) {
		return (CL_EINTERNAL);
	}

	// Get the largest nodeid in the system.
	curr_devsi = clconf_cluster_get_nodes(clconf);
	while ((obj_dev = clconf_iter_get_current(curr_devsi)) != NULL) {
		if (clconf_obj_get_id(obj_dev) > (int)max_nodeid) {
			max_nodeid = (nodeid_t)clconf_obj_get_id(obj_dev);
		}
		clconf_iter_advance(curr_devsi);
	}
	clconf_iter_release(curr_devsi);

	// Go through the quorum devices
	found = 0;
	devsi = curr_devsi = clconf_cluster_get_quorum_devices(clconf);
	while ((obj_dev = clconf_iter_get_current(curr_devsi)) != NULL) {
		clconf_iter_advance(curr_devsi);

		// Get the quorum device name
		conf_name = clconf_obj_get_name(obj_dev);
		if (!conf_name)
			continue;

		// Check if name match
		if (strcmp(quorum_name, conf_name) == 0) {
			found = 1;
			break;
		}
	}

	if (!found) {
		goto cleanup;
	}

	// The xml file requires getting device path first.
	for (j = 1; j <= max_nodeid; j++) {
		(void) sprintf(tmp_buf, "path_%u", j);
		conf_val = clconf_obj_get_property(obj_dev, tmp_buf);
		if (!conf_val)
			continue;

		// Get node name
		scconf_err = scconf_get_nodename(j, &nodename);
		if (scconf_err != SCCONF_NOERR) {
			clerrno = clquorum_conv_scconf_err(scconf_err);
			goto cleanup;
		}

		// Add node path
		if (strcmp(conf_val, "enabled") == 0) {
			clerrno = clq_export->addQuorumDevicePath(
				quorum_name, nodename, ENABLED);
		} else {
			clerrno = clq_export->addQuorumDevicePath(
				quorum_name, nodename, DISABLED);
		}
		free(nodename);
		if (clerrno != CL_NOERR) {
			goto cleanup;
		}
	}

	// All other properties
	for (i = 0; CLQ_CONF_PROP[i]; i++) {
		conf_val = clconf_obj_get_property(obj_dev, CLQ_CONF_PROP[i]);

		// Type
		if (strcmp(CLQ_CONF_PROP[i], "type") == 0) {
			if (!conf_val) {
				// No type? Use access_mode
				conf_val = clconf_obj_get_property(
				    obj_dev, "access_mode");
				if (!conf_val)
					continue;
			}

			// For "scsi", we show it as "shared_disk".
			if (strstr(conf_val, "scsi") != 0) {
				conf_val = "shared_disk";
			}
			clerrno = clq_export->addQuorumDeviceProperty(
			    quorum_name, CLQ_CONF_PROP[i],
			    (char *)conf_val, CLQ_READ_ONLY[i]);
			if (clerrno != CL_NOERR) {
				goto cleanup;
			}
			continue;
		}

		// Other properties
		if (conf_val) {
			clerrno = clq_export->addQuorumDeviceProperty(
			    quorum_name, CLQ_CONF_PROP[i], (char *)conf_val,
			    CLQ_READ_ONLY[i]);
			if (clerrno != CL_NOERR) {
				goto cleanup;
			}
		}
	}

cleanup:
	if (devsi != (clconf_iter_t *)0) {
		clconf_iter_release(devsi);
	}

	return (clerrno);
}

#ifdef WEAK_MEMBERSHIP
//
// clquorum_get_qglobal_xml
//
//	Get the global quorum configuration data, and add them into the
//	ClcmdExport object.
//
static clerrno_t
clquorum_get_qglobal_xml(clconf_cluster_t *clconf, char *global_quorum_name,
    ClquorumExport *clq_export)
{
	clerrno_t clerrno = CL_NOERR;
	const char *conf_val;

	// Check inputs
	if (!global_quorum_name || !clconf || (clq_export == NULL)) {
		return (CL_EINTERNAL);
	}

	// Add the Type
	clerrno = clq_export->addGlobalQuorumProperty(
	    (char *)global_quorum_name, (char *)"type",
	    (char *)SCCONF_QUORUM_TYPE_SYSTEM, true);

	if (clerrno != CL_NOERR) {
		goto cleanup;
	}

	// Add the Ping Targets
	conf_val = clconf_obj_get_property((clconf_obj_t *)clconf,
						CLPROP_PING_TARGETS);
	if (conf_val) {
		clerrno = clq_export->addGlobalQuorumProperty(
		    (char *)global_quorum_name, (char *)CLPROP_PING_TARGETS,
		    (char *)conf_val, true);
		if (clerrno != CL_NOERR) {
			goto cleanup;
		}
	}

	// Add the Multiple Partitions
	conf_val = clconf_obj_get_property((clconf_obj_t *)clconf,
						CLPROP_MULTIPLE_PARTITIONS);
	if (conf_val) {
		clerrno = clq_export->addGlobalQuorumProperty(
		    (char *)global_quorum_name,
		    (char *)CLPROP_MULTIPLE_PARTITIONS,
		    (char *)conf_val, true);
		if (clerrno != CL_NOERR) {
			goto cleanup;
		}
	}

cleanup:
	return (clerrno);
}
#endif
