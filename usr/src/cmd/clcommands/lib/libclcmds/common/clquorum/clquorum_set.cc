//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident   "@(#)clquorum_set.cc 1.9     09/04/20 SMI"

//
// Process clquorum "set"
//

#include "clquorum.h"

#ifdef WEAK_MEMBERSHIP
#include <ccr/persistent_table.h>
#include <scadmin/scstat.h>
#include <rgm_support.h>

static int ask_confirmation();
static clerrno_t validate_switch_to_weak_membership(clconf_obj_t *clconf,
	NameValueList &properties);
boolean_t check_global_quorum_propname(char *propname);
clerrno_t parse_and_validate_ping_targets(char *propvalue,
	bool isappend);
bool hostname_present_in_local_files(char *ping_target);
#endif

//
// clquorum "set"
//
clerrno_t
clquorum_set(ClCommand &cmd, ValueList &operands, optflgs_t optflgs,
    ValueList &devicetypes, NameValueList &properties)
{
	scconf_errno_t scconf_err = SCCONF_NOERR;
	clerrno_t clerrno = CL_NOERR;
	NameValueList qdev_list;
	NameValueList tmp_list;
	char scconf_props[SCCONF_MAXSTRINGLEN];
	char *this_type;
	char *this_qdev;
	bool check_type = false;
	clerrno_t first_err = CL_NOERR;
#ifdef WEAK_MEMBERSHIP
	bool sys_type = false;
	boolean_t multiple_partitions = B_FALSE;
	clconf_cluster_t *clconf = (clconf_cluster_t *)0;
#endif
	// Check inputs
	if ((properties.getSize() == 0) || (operands.getSize() == 0)) {
		clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}

	// Get the quoroms of the specified type. Should be only one type
	if (devicetypes.getSize() == 1) {
		devicetypes.start();
		this_type = devicetypes.getValue();

		//
		// Get the configured quorum devices of this type.
		// qdev_list has <name, type> pair for all the quorum
		// devices of this_type.
		//
		if (this_type) {
#ifdef WEAK_MEMBERSHIP
			//
			// Check if "this_type" is "SYSTEM"
			//
			// If so then skip the step for fetching the
			// quorum devices
			// If not then continue processing the regular way
			//
			if (strcmp(this_type, SCCONF_QUORUM_TYPE_SYSTEM) != 0) {
#endif
				clerrno = clquorum_get_quorumdevices(NULL,
					this_type, qdev_list);
				if (clerrno != CL_NOERR) {
					clcommand_perror(clerrno);
					goto cleanup;
				}

				if (qdev_list.getSize() == 0) {
					clerror("No quorum devices of type "
					"\"%s\" exist.\n", this_type);
					clerrno = CL_ENOENT;
					goto cleanup;
				}
				check_type = true;
#ifdef WEAK_MEMBERSHIP
			} else {
				sys_type = true;
			}
#endif
		}
	}

	// Go through the operands
	for (operands.start(); operands.end() != 1; operands.next()) {
		this_qdev = operands.getValue();
		if (!this_qdev)
			continue;
#ifdef WEAK_MEMBERSHIP
		//
		// Check if the Quorum Device operand is GLOBAL_QUORUM
		// device.
		// If so then we need to do separate processing for the
		// 	1. Properties validation
		//	2. Property value validation
		// 	3. If we are moving to WEAK Membership model
		//		then ask the user confirmation
		//
		// 	conf_clear_ping_targets()
		//	conf_addto_ping_targets()
		//	conf_rmfrom_ping_targets()
		//	are private APIs available in libscconf
		//
		// 	The ping_targets property is set using a
		// 	'=' then use conf_clear_ping_targets()
		// 	then add each entry in the property value one by one
		// 	after validation using
		//	conf_addto_ping_targets()
		//
		//	If the ping_targets property is set using a '+='
		// 	operation then just add the entry using
		//	conf_addto_ping_targets()
		//
		//	If the ping_targets property is set using a '-='
		// 	operation then just remove the entry using
		//	conf_rmfrom_ping_targets()
		//
		// If we are not modifying the GLOBAL_QUORUM object
		// then just perform the usual operations
		//

		if (sys_type || strcmp(this_qdev,
				SCCONF_GLOBAL_QUORUM_NAME) == 0) {
			// Set the properties
			for (properties.start(); !properties.end();
				properties.next()) {
			    char *prop_name = properties.getName();
			    char *prop_val = properties.getValue();
			    char *msgbuffer = (char *)0;
			    if (!prop_name)
				continue;

			    if (check_global_quorum_propname(prop_name)
					== B_FALSE) {
				clerror("Invalid property name "
					"\"%s\" specified.\n", prop_name);
				continue;
			    }

			    //
			    // Initialize the clconf handle
			    //
			    scconf_err =
// CSTYLED
				scconf_cltr_openhandle((scconf_cltr_handle_t *) &clconf);

			    if (scconf_err != SCCONF_NOERR) {
				clerrno =
// CSTYLED
					clquorum_conv_scconf_err(scconf_err);
				goto cleanup;
			    }

			    //
			    // Get the current value of
			    // multiple_partitions property
			    //
			    scconf_err =
				scconf_get_multiple_partitions(clconf,
					&multiple_partitions);
			    if (scconf_err != SCCONF_NOERR) {
// CSTYLED
				clerrno = clquorum_conv_scconf_err(scconf_err);
				goto cleanup;
			    }

			    if (strcasecmp(prop_name,
					CLPROP_MULTIPLE_PARTITIONS) == 0) {
				if ((multiple_partitions == B_TRUE &&
					strcasecmp(prop_val, "true") == 0) ||
					(multiple_partitions == B_FALSE &&
					strcasecmp(prop_val, "false") == 0)) {
					//
					// Do nothing when the
					// multiple_partitions
					// property is already set to the new
					// value being set
					//
					continue;
				}

				if (strcasecmp(prop_val, "true") == 0) {
					clerrno =
// CSTYLED
						validate_switch_to_weak_membership((clconf_obj_t *)clconf, properties);

					if (clerrno != CL_NOERR) {
						clcommand_perror(clerrno);
						goto cleanup;
					}

					//
					// If we are moving
					// multiple_partitions from
					// false --> true
					// Ask confirmation if no Force flag
					// is used
					//
					if (!(optflgs & Fflg)) {
					    if (ask_confirmation() == 0) {
						clerrno = CL_NOERR;
						goto cleanup;
					    }
					}
				} else if (strcasecmp(prop_val, "false") == 0) {
					//
					// Make sure we do not have any
					// unresolved
					// CCR changes that are pending
					// If so we should not allow the switch
					// to STRONG membership
					//
// CSTYLED
					if (ccrlib::split_brain_ccr_change_exists() == true) {
// CSTYLED
						clerror("You cannot change \"%s\" to \"false\" "
// CSTYLED
							"when cluster configuration is still "
// CSTYLED
							"unresolved\n",
// CSTYLED
							CLPROP_MULTIPLE_PARTITIONS);
						clcommand_perror(CL_EOP);
						clerrno = CL_EOP;
						goto cleanup;
					}
				}
			    }

			    //
			    // Enforce the check for all the nodes to be
			    // Online only when changing multiple_partitions
			    // property or ping_targets property running
			    // under Weak membership
			    //
			    // In other words allow modification of
			    // ping_targets under Strong membership
			    //
			    if ((strcasecmp(prop_name,
					CLPROP_MULTIPLE_PARTITIONS) == 0) ||
					((strcasecmp(prop_name,
					CLPROP_PING_TARGETS) == 0) &&
					(multiple_partitions == B_TRUE))) {
				if (!clquorum_are_all_nodes_online()) {
// CSTYLED
				    clerror("You can only change the properties "
// CSTYLED
					"when all the nodes configured in the "
					"cluster are Online\n");
				    clerrno = CL_EOP;
				    goto cleanup;
				}
			    }

			    if (strncasecmp(prop_name,
				    PROP_CLUSTER_PING_TARGETS,
				    strlen(PROP_CLUSTER_PING_TARGETS))
					== 0) {
				//
				// Call parse_and_validate_ping_targets
				// with either
				// true/false for "isappend" arg based on
				// whether we are appending or
				// re-seting the prop value
				//
				if (strchr(prop_name, '+') != NULL) {
				    clerrno  =
				    parse_and_validate_ping_targets(prop_val,
							true);
				} else {
				    clerrno  =
				    parse_and_validate_ping_targets(prop_val,
							false);
				}

				if (clerrno != CL_NOERR) {
					goto cleanup;
				}
			    }

			    scconf_err =
// CSTYLED
				scconf_change_global_quorum_prop(prop_name,
						prop_val, &msgbuffer);

			    if (scconf_err != SCCONF_NOERR) {

				switch (scconf_err) {
				case SCCONF_EINVAL:
					// Print detailed messages
					if (msgbuffer != (char *)0) {
// CSTYLED
						clcommand_dumpmessages(msgbuffer);
						free(msgbuffer);
					}

					clerror("Invalid Property value "
					    "specified.\n");
					clerrno = CL_EINVAL;
					break;

				case SCCONF_EBADVALUE:
					// Print detailed messages
					if (msgbuffer != (char *)0) {
// CSTYLED
						clcommand_dumpmessages(msgbuffer);
						free(msgbuffer);
					}
					clerrno = CL_EPROP;
					break;

				default:
					// Print detailed messages
					if (msgbuffer != (char *)0) {
// CSTYLED
						clcommand_dumpmessages(msgbuffer);
						free(msgbuffer);
					}
					clerrno = clquorum_conv_scconf_err(
					    scconf_err);
					break;
				} //lint !e788

				// Return the first error ever happened
				if ((first_err == CL_NOERR) &&
				    (clerrno != CL_NOERR)) {
					first_err = clerrno;
				}
			    } else if (optflgs & vflg) {
				//
				// There were no errors
				// See if we need to print
				// verbose message to the user
				//
				if (prop_val && (strcmp(prop_val, "") != 0)) {
					clmessage("Property \"%s\" is set to "
					"\"%s\" for quorum device "
					"\"%s\".\n", prop_name, prop_val,
					this_qdev);
				}
			    }
			}

			//
			// Done with the properties. Do the cleanup and return
			//
			goto cleanup;
		}
#endif

		if (check_type) {
			// Get the quorum type in ccr
			tmp_list.clear();
			clerrno = clquorum_get_quorumdevices(this_qdev, NULL,
			    tmp_list);
			if (clerrno != CL_NOERR) {
				clcommand_perror(clerrno);
				goto cleanup;
			}

			// This quorum may not exist
			if (!(tmp_list.getValue(this_qdev))) {
				clerror("Device \"%s\" does not exist.\n",
				    this_qdev);
				clerrno = CL_ENOENT;

				// Return the first error ever happened
				if (first_err == CL_NOERR) {
					first_err = clerrno;
				}
				continue;
			}

			// If the type matches the specified type
			if (!(qdev_list.getValue(this_qdev))) {
				clerror("The type of quorum \"%s\" does not "
				    "match \"-t %s\".\n",
				    this_qdev, this_type);	//lint !e644
				clerrno = CL_ENOENT;

				// Return the first error ever happened
				if (first_err == CL_NOERR) {
					first_err = clerrno;
				}
				continue;
			}
		}

		// Set the properties
		for (properties.start(); !properties.end(); properties.next()) {
			char *prop_name = properties.getName();
			char *prop_val = properties.getValue();
			char *msgbuffer = (char *)0;
			if (!prop_name)
				continue;

			*scconf_props = '\0';
			if (prop_val) {
				(void) sprintf(scconf_props, "%s=%s",
				    prop_name, prop_val);
			} else {
				(void) sprintf(scconf_props, "%s=", prop_name);
			}

			scconf_err = scconf_set_qd_properties(this_qdev,
			    scconf_props, &msgbuffer);
			if (scconf_err != SCCONF_NOERR) {
				// Print detailed messages
				if (msgbuffer != (char *)0) {
					clcommand_dumpmessages(msgbuffer);
					free(msgbuffer);
				}
				switch (scconf_err) {
				case SCCONF_ENOEXIST:
					clerror("Device \"%s\" does not "
					    "exist.\n", this_qdev);
					clerrno = CL_ENOENT;
					break;

				case SCCONF_EINVAL:
					clerror("Property name \"%s\" is "
					    "invalid to quorum \"%s\".\n",
					    prop_name, this_qdev);
					clerrno = CL_EPROP;
					break;

				case SCCONF_EBADVALUE:
					clerrno = CL_EPROP;
					break;

				default:
					clerrno = clquorum_conv_scconf_err(
					    scconf_err);
					break;
				} //lint !e788

				// Return the first error ever happened
				if ((first_err == CL_NOERR) &&
				    (clerrno != CL_NOERR)) {
					return (clerrno);
				}
			} else if (optflgs & vflg) {
				if (prop_val && (strcmp(prop_val, "") != 0)) {
					clmessage("Property \"%s\" is set to "
					    "\"%s\" for quorum device "
					    "\"%s\".\n", prop_name, prop_val,
					    this_qdev);
				} else {
					clmessage("Property \"%s\" is set to "
					    "the default for quorum device "
					    "\"%s\".\n", prop_name, this_qdev);
				}
			}
		}
	}

cleanup:
#ifdef WEAK_MEMBERSHIP
	// Release old handle
	if (clconf != (scconf_cltr_handle_t)0) {
		scconf_cltr_releasehandle(clconf);
	}
#endif

	if (first_err != CL_NOERR) {
		clerrno = first_err;
	}

	return (clerrno);
}

#ifdef WEAK_MEMBERSHIP
int ask_confirmation() {
    char *rptr;
    char replybuf[BUFSIZ];
    bool force_flag = false;
    int str_len = 0;

	// If we are here then the -F option was not specified
	// We have to ask  for user confirmation
    fprintf(stderr, "This action might result in data corruption "
		"or loss.\n");

    if (strcmp(gettext("yes"), "yes") == 0 &&
	strcmp(gettext("no"), "no") == 0) {
		// The l10n language is english, so
		// we can stick with 'y' and 'n'.
		(void) fprintf(stderr,
			gettext("Are you sure you want to enable multiple "
				"partitions in the cluster to be operational "
				"(y/n) [n]?"));
		rptr = fgets(replybuf, sizeof (replybuf) - 1, stdin);
		if (rptr) {
			str_len = strlen(replybuf) - 1;
			if (str_len > 0 &&
				(strncasecmp(replybuf, "y", str_len) == 0 ||
				strncasecmp(replybuf,
					"yes", str_len) == 0)) {
				force_flag = true;
			}
		}
	} else {
		// The l10n language is not english, so
		// we have to stick with 'yes' and 'no'
		(void) fprintf(stderr,
			gettext("Are you sure you want to enable multiple "
				"partitions in the cluster to be operational "
				"(%s/%s) [%s]?"),
				gettext("yes"),
				gettext("no"),
				gettext("no"));
		rptr = fgets(replybuf, sizeof (replybuf) - 1, stdin);
		if (rptr) {
			while (str_len < BUFSIZ &&
				replybuf[str_len] != '\n')
				str_len++;
			if (strncasecmp(replybuf, gettext("yes"),
				str_len) == 0) {
				force_flag = true;
			}
		}

	}

    if (force_flag) {
	return (1);
	}

    return (0);
}

//
// validate_switch_to_weak_membership
//
// Validate whether the properties being modified
// can switch the membership model to WEAK
//
// Return CL_NOERR if the validation succeeds
//	  Other non-zero return values if the
//	  validation fails.
//
clerrno_t
validate_switch_to_weak_membership(clconf_obj_t *clconf,
	NameValueList &properties) {

    clerrno_t clerrno = CL_NOERR;
    char *value = NULL;
    char *value_str = NULL;
    int found = 0;
    quorum_status_t *clust_memb;
    char lasts[BUFSIZ];
    char *lasts_p = &lasts[0];
    ValueList empty_list;
    NameValueList qd_list;

	//
	// Do the following validations
	//
	// 1. Is the number of nodes exactly 2 ?
	// 2. Is ping_target already set or being
	//    set ?
	// 3. Are there any quorum devices already
	//    configured on the cluster ?

	//
	// Check #1
	//
	if (cluster_get_quorum_status(&clust_memb) != 0) {
	    clerror("Unable to determine the number of nodes "
		"that are configured in the cluster\n");
	    return (CL_EINTERNAL);
	}

	if (clust_memb->num_nodes != 2) {
	    clerror("Cannot set \"%s\" to \"true\" "
			"if the number of nodes in the "
			"cluster is not exactly two\n",
			CLPROP_MULTIPLE_PARTITIONS);
	    return (CL_EOP);
	}

	//
	// Check #2
	//

	//
	// Check if we are setting the ping_targets in this same invocation
	//
	if ((value = properties.getValue(CLPROP_PING_TARGETS)) != NULL) {
		value_str = strdup(value);

		value = (char *)strtok_r(value_str, ",", (char **)&lasts_p);

		while (value != NULL) {
		    if (scconf_health_check_status(NULL, value)
				== SCCONF_NOERR) {
			found = 1;
		    }

		    value = (char *)strtok_r(NULL, ",", (char **)&lasts_p);
		}
	}

	if (!found)  {
	    value = (char *)clconf_obj_get_property((clconf_obj_t *)clconf,
							CLPROP_PING_TARGETS);
	    if (value == NULL || strlen(value) <= 0) {
		clerror("Cannot set \"%s\" to \"true\" when \"%s\" "
			"property is not set or if the value set is "
			"invalid\n", CLPROP_MULTIPLE_PARTITIONS,
			CLPROP_PING_TARGETS);
		return (CL_EOP);
	    }
	}

	//
	// Check #3
	//
	clerrno = clquorum_get_quorumdevice_list(empty_list,
		empty_list, qd_list);

	if (qd_list.getSize() != 0)  {
	    clerror("Cannot set \"%s\" to \"true\" if there are quorum devices "
		"configured.\n", CLPROP_MULTIPLE_PARTITIONS);
	    return (CL_EOP);
	}

	return (CL_NOERR);
}

/*
 * check_global_quorum_propname
 *
 * This function checks if the given quorum property name is valid
 * or not
 *
 * Possible return values:
 *
 *      B_TRUE          The property name is valid
 *      B_FALSE         The property name is invalid
 */
boolean_t
check_global_quorum_propname(char *propname)
{
	int proplen = 0;
	char *lowercase_propname = NULL;
	char *lowercase_actual_propname = NULL;
	boolean_t return_value = B_FALSE;

	/*
	* Check if the given property name matches either
	* ping_targets or multiple_partitions property name
	*
	* If it matches return true
	* If not return false
	*/
	if (strcasecmp(propname, PROP_CLUSTER_MULTI_PARTITIONS) == 0) {
		return (B_TRUE);
	}

	/*
	* In case of ping_targets property, the propname can
	* contain an optional '+' or '-' at the end of the
	* property name. The caller will send the property
	* names with the '+' & '-'. This needs to be accounted
	* for when comparing the property names.
	*/
	convert_to_lowercase(propname, &lowercase_propname);
	convert_to_lowercase(PROP_CLUSTER_PING_TARGETS,
				&lowercase_actual_propname);

	if (strstr(lowercase_propname, lowercase_actual_propname) != NULL) {
		proplen = strlen(propname);
		if (proplen == strlen(PROP_CLUSTER_PING_TARGETS)) {
			return_value = B_TRUE;
			goto cleanup;
		} else if (proplen == (strlen(PROP_CLUSTER_PING_TARGETS) + 1)) {
			if (propname[proplen - 1] == '+' ||
				propname[proplen - 1] == '-') {
				return_value = B_TRUE;
				goto cleanup;
			}
		}
	}

cleanup:
	if (lowercase_propname != NULL) {
		free(lowercase_propname);
	}

	if (lowercase_actual_propname != NULL) {
		free(lowercase_actual_propname);
	}

	return (return_value);
}

//
// parse_and_validate_ping_targets
//
// Parse the values given to propvalue argument and validate
// if the given ping_targets are
// 1. Present in the same primary subnet as the cluster nodes
// 2. Mapping is present for the hostnames in the local files
// 3. Reachable
//
// Input:
//	  propname - Property name
//	  propvalue - Property Value (may be comma separated also)
//	  isappend - Boolean that says whether a property value is
//			is being appended to the existing values
//			or reset to a new value.
//
// Return :
// 	CL_NOERR - If the ping_targets abide by the above mentioned
//			validations
//	Other errors - If any of the validations fails
//
clerrno_t
parse_and_validate_ping_targets(char *propvalue, bool isappend) {
	scconf_errno_t scconferr = SCCONF_NOERR;
	char lasts[BUFSIZ];
	char *lasts_p = &lasts[0];
	char *value = NULL;
	char *value_str = NULL;
	char buffer[SCCONF_MAXSTRINGLEN];
	scconf_cltr_handle_t handle = (scconf_cltr_handle_t *)0;
	boolean_t multiple_partitions;
	scha_errmsg_t status = {SCHA_ERR_NOERR, NULL};

	/*
	 * Check if we are adding or re-setting the property
	 * value
	 */
	if (isappend) {
		if (propvalue == NULL ||
			strlen(propvalue) <= 0) {
			clerror("You must specify a value for "
				"the ping target.\n");
			return (CL_EINVAL);
		}

		//
		// Validate the ping_target being configured
		// for the following :
		// 1. Valid IP Address/Hostname
		// 2. Present in the same primary subnet
		//    as the cluster nodes
		//
		status = validate_ping_target(propvalue);

		if (status.err_code != SCHA_ERR_NOERR) {
			clcommand_dumpmessages(status.err_msg);
			free(status.err_msg);
			return (map_scha_error(status.err_code));
		}

		if (!hostname_present_in_local_files(propvalue)) {
			clerror("Ping target \"%s\" "
				"does not have a mapping in "
				"/etc/inet/ipnodes or "
				"/etc/inet/hosts "
				"for at least one node "
				"of the cluster.\n",
				propvalue);
			return (CL_EINVAL);
		}

		if (scconf_health_check_status(NULL,
			propvalue) != SCCONF_NOERR) {
			clerror("Ping target \"%s\" is "
				"unreachable.\n", propvalue);
			return (CL_EINVAL);
		}
	} else {
		/*
		 * Re set the value of ping_targets with the newly
		 * specified values
		 */
		value_str = strdup(propvalue);
		if (value_str == NULL) {
			clcommand_perror(CL_ENOMEM);
			return (CL_ENOMEM);
		}

		if (strlen(value_str) != 0) {
			value = (char *)strtok_r(value_str,
				",", (char **)&lasts_p);

			while (value != NULL) {
				status = validate_ping_target(value);

				if (status.err_code != SCHA_ERR_NOERR) {
					clcommand_dumpmessages(status.err_msg);
					free(status.err_msg);
// CSTYLED
					return (map_scha_error(status.err_code));
				}

				if (!hostname_present_in_local_files(value)) {
					clerror("Ping target \"%s\" "
						"does not have a mapping in "
						"/etc/inet/ipnodes or "
						"/etc/inet/hosts "
						"for at least one node "
						"of the cluster.\n",
						propvalue);
					return (CL_EINVAL);
				}

				if (scconf_health_check_status(NULL, value)
						!= SCCONF_NOERR) {
					clerror("Ping target \"%s\" "
						"is unreachable.\n",
						value);
					return (CL_EINVAL);
				}

				value = (char *)strtok_r(NULL,
					",", (char **)&lasts_p);
			}

		}
	}

	return (CL_NOERR);
}

//
// hostname_present_in_local_files
//
// Checks if there is a hostname to Ip Address mapping
// present for the given ping_target argument
// in the local files /etc/inet/ipnodes or
// /etc/inet/hosts
// on all the nodes of the cluster
//
//	input: Hostname or IP Address
//
//	return values:
//		true : if the mapping for the
//			hostname is present in
//			the local files on all
//			the nodes of the cluster
//
//		false : if the mapping for the
//			hostname is not present in
//			the local files on any of
//			the nodes of the cluster
//
bool
hostname_present_in_local_files(char *hostname) {
	quorum_status_t *quorum_status;
	int present_count = 0;
	int node_down_count = 0;
	int nodecount = 0;
	scconf_errno_t rstatus = SCCONF_NOERR;
	quorum_node_status_t *node_status;
	char *nodename = NULL;

	//
	// If the ping_target is an IP Address
	// return true
	//
	if (is_valid_ipaddress(hostname)) {
		return (true);
	}

	//
	// Get the current/immediate quorum status
	if (cluster_get_immediate_quorum_status(&quorum_status)
			!= SCCONF_NOERR) {
		clcommand_perror(CL_EINTERNAL);
		return (false);
	}

	present_count = 0;

	//
	// Test the existence of a mapping for the hostname
	// only on the nodes that are currently Online
	//
	for (nodecount = 0; nodecount < quorum_status->num_nodes; nodecount++) {
		// Get the node name
		if (scconf_get_nodename(quorum_status->nodelist[nodecount].nid,
				&nodename) != SCCONF_NOERR) {
			clcommand_perror(CL_EINTERNAL);
			return (false);
		}

		// Get the node status
		node_status = &quorum_status->nodelist[nodecount];

		if (node_status->state == QUORUM_STATE_ONLINE) {
			rstatus =
				scconf_check_hostname_in_files(nodename,
					hostname);

			if (rstatus == SCCONF_NOERR) {
				present_count++;
			}
		} else {
			node_down_count++;
		}
	}


	if ((quorum_status->num_nodes - node_down_count)  == present_count) {
		return (true);
	}

	return (false);
}
#endif // WEAK_MEMBERSHIP
