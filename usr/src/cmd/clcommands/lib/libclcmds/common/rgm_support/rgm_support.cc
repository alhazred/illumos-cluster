//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)rgm_support.cc	1.42	09/03/18 SMI"

//
// Process RGM common requests like error message and property string
// handling.
//

#include <sys/os.h>
#include <orb/infrastructure/orb.h>

#include <scadmin/scconf.h>
#include <scadmin/scrgadm.h>
#include <scadmin/scswitch.h>
#include <scadmin/scstat.h>
#include <rgm/rgm_scrgadm.h>
#include <rgm/rgm_cnfg.h>

#include "clcommands.h"

#include "ClCommand.h"
#include "ClShow.h"

#include "rgm_support.h"
#if (SOL_VERSION >= __s10)
#include "clzonecluster.h"
#endif
#include "common.h"

#if (SOL_VERSION >= __s10)
#include <libintl.h>
#include <locale.h>
#include <nslib/ns.h>
#include <h/rtreg_proxy.h>
#include <sys/vc_int.h>
#endif

#include "rgm_support_internal.h"

#define	MAX_CMD_LEN	8192
#define	RTREG_PROXY_SERVER "rtreg_proxy_server"

#if (SOL_VERSION >= __s10)
#define	EVAC_TIMEOUT	900	// 900 seconds = 15 minutes
#endif

static boolean_t is_enabled(rgm_resource_t *rs, clerrno_t *errflg,
    char *zc_name = NULL);
static namelist_t *get_rgm_arraypropval(char *name_str, char *objname,
    int *errflg, cl_objtype_t objtype);
static int get_property_type(char *prop, NameValueList &proplist, int *errflg,
    char **actual_name);
static char *names_to_str_internal(namelist_t *name_list, char *propname,
    int *errflg, char *sep);
static char *replace_nodeid(char *start_str, char *node_str,
    boolean_t iseuropa, int *errflg);
#if (SOL_VERSION >= __s10)
static namelist_t *zc_nodeidlist_to_namelist(nodeidlist_t *list, int *errflg,
	char *zc_name);
static char *zc_nodeid_to_nodename(uint_t nodeid, int *errflg, char *zcname);
#endif

static state_mapping_t rs_state_mapping[] = {
	{"Online", "Online"},
	{"Offline", "Offline"},
	{"Start failed", "Start_failed"},
	{"Stop failed", "Stop_failed"},
	{"Monitor failed", "Monitor_failed"},
	{"Online but not monitored", "Online_not_monitored"},
	{"Starting", "Starting"},
	{"Stopping", "Stopping"},
	{"Not_online", "Not_online"},
	{0, 0},
};

static state_mapping_t rg_state_mapping[] = {
	{"Online", "Online"},
	{"Offline", "Offline"},
	{"Unmanaged", "Unmanaged"},
	{"Pending online", "Pending_online"},
	{"Pending offline", "Pending_offline"},
	{"Error--stop failed", "Error_stop_failed"},
	{"Online faulted", "Online_faulted"},
	{"Pending online blocked", "Pending_online_blocked"},
	{"Not_online", "Not_online"},
	{0, 0},
};

clerrno_t
cluster_init()
{
	scconf_errno_t scconf_status;
	uint_t ismember = 0;
	char errbuff[BUFSIZ];

	scconf_status = scconf_ismember(0, &ismember);
	if (scconf_status != SCCONF_NOERR) {
		scconf_strerr(errbuff, scconf_status);
		fprintf(stderr, "%s\n", &errbuff);
		clerror("Cluster membership check failed.\n");
		return (map_scconf_error(scconf_status));
	}

	if (ORB::initialize()) {
		clerror("Can not access cluster information.\n");
		return (CL_EOP);
	}

	return (CL_NOERR);
}

clerrno_t
map_scconf_error(int error)
{
	clerrno_t clerrno = CL_NOERR;

	switch (error) {
	case SCCONF_NOERR:
		clerrno = CL_NOERR;
		break;

	case SCCONF_EUSAGE:
		clerrno = CL_EINVAL;
		break;

	case SCCONF_ENOEXIST:
		clerrno = CL_ENOENT;
		break;

	case SCCONF_EPERM:
		clerrno = CL_EACCESS;
		break;

	case SCCONF_ENOCLUSTER:
		clerrno = CL_EOP;
		break;

	case SCCONF_ENOMEM:
		clerrno = CL_ENOMEM;
		break;

	case SCCONF_EINVAL:
		clerrno = CL_EINVAL;
		break;

	case SCCONF_EUNEXPECTED:
		clerrno = CL_EINTERNAL;
		break;

	default:
		clerrno = CL_EINTERNAL;
		break;
	}
	return (clerrno);
}

clerrno_t
map_scstat_error(int error)
{
	clerrno_t clerrno = CL_NOERR;
	switch (error) {
	case SCSTAT_ENOERR:
		clerrno = CL_NOERR;
		break;
	case SCSTAT_EUSAGE:
		clerrno = CL_EINVAL;
		break;
	case SCSTAT_ENOMEM:
		clerrno = CL_ENOMEM;
		break;
	SCSTAT_ENOTCLUSTER:
		clerrno = CL_EOP;
		break;
	SCSTAT_ENOTCONFIGURED:
		clerrno = CL_ENOENT;
		break;
	SCSTAT_ESERVICENAME:
		clerrno = CL_EINVAL;
		break;
	SCSTAT_EINVAL:
		clerrno = CL_EINVAL;
		break;
	SCSTAT_EPERM:
		clerrno = CL_EACCESS;
		break;
	SCSTAT_ECLUSTERRECONFIG:
		clerrno = CL_EBUSY;
		break;
	SCSTAT_ERGRECONFIG:
		clerrno = CL_EBUSY;
		break;
	SCSTAT_EOBSOLETE:
		clerrno = CL_EBUSY;
		break;
	SCSTAT_EUNEXPECTED:
		clerrno = CL_EINTERNAL;
		break;
	default:
		clerrno = CL_EINTERNAL;
		break;
	}
	return (clerrno);
}

clerrno_t
map_scha_error(int error)
{
	clerrno_t clerrno = CL_NOERR;

	switch (error) {
	case SCHA_ERR_NOERR:
		clerrno = CL_NOERR;
		break;

	case SCHA_ERR_NOMEM:
		clerrno = CL_ENOMEM;
		break;

	case SCHA_ERR_HANDLE:
	case SCHA_ERR_TAG:
	case SCHA_ERR_SEQID:
	case SCHA_ERR_INTERNAL:
	case SCHA_ERR_ORB:
	case SCHA_ERR_CCR:
		clerrno = CL_EINTERNAL;
		break;

	case SCHA_ERR_ACCESS:
	case SCHA_ERR_AUTH:

		clerrno = CL_EACCESS;
		break;

	case SCHA_ERR_FILE:
		clerrno =  CL_ENOENT;
		break;

	case SCHA_ERR_CLUSTER:
	case SCHA_ERR_ZONE_CLUSTER:
	case SCHA_ERR_NODE:
		clerrno = CL_ENOENT;
		break;

	case SCHA_ERR_MEMBER:
		clerrno = CL_EOP;
		break;

	case SCHA_ERR_RG:
		clerrno = CL_ENOENT;
		break;

	case SCHA_ERR_RT:
		clerrno = CL_ETYPE;
		break;

	case SCHA_ERR_VALIDATE:
	case SCHA_ERR_METHODNAME:
	case SCHA_ERR_METHOD:

		clerrno = CL_EMETHOD;
		break;

	case SCHA_ERR_USAGE:
	case SCHA_ERR_PINT:
	case SCHA_ERR_PBOOL:
	case SCHA_ERR_PENUM:
	case SCHA_ERR_PSTR:
	case SCHA_ERR_PSARRAY:
	case SCHA_ERR_INVAL:
	case SCHA_ERR_NODEFAULT:
	case SCHA_ERR_DUPNODE:

		clerrno = CL_EINVAL;
		break;

	case SCHA_ERR_SWRECONF:
		clerrno = CL_EBUSY;
		break;

	case SCHA_ERR_RSRC:
		clerrno = CL_ENOENT;
		break;

	case SCHA_ERR_PROP:
	case SCHA_ERR_DUPP:
	case SCHA_ERR_UPDATE:
	case SCHA_ERR_INVALID_REMOTE_AFF:
	case SCHA_ERR_INVALID_REMOTE_DEP:

		clerrno = CL_EPROP;
		break;

	case SCHA_ERR_CHECKS:
	case SCHA_ERR_RSTATUS:
	case SCHA_ERR_TIMEOUT:
	case SCHA_ERR_FAIL:
	case SCHA_ERR_RS_VALIDATE:
	case SCHA_ERR_STARTFAILED:
	case SCHA_ERR_DELETE:
	case SCHA_ERR_STOPFAILED:
	case SCHA_ERR_NOMASTER:
	case SCHA_ERR_TIMESTAMP:
	case SCHA_ERR_MEMBERCHG:
	case SCHA_ERR_INSTLMODE:
	case SCHA_ERR_RGSTATE:
	case SCHA_ERR_DETACHED:
	case SCHA_ERR_RSTATE:
	case SCHA_ERR_PINGPONG:
	case SCHA_ERR_RGRECONF:
	case SCHA_ERR_CLRECONF:
	case SCHA_ERR_RECONF:
	case SCHA_ERR_DEPEND:
	case SCHA_ERR_STATE:
	case SCHA_ERR_ZC_DOWN:

		clerrno = CL_ESTATE;
		break;

	default:
		clerrno = CL_EINTERNAL;
		break;
	}
	return (clerrno);
}

void
print_scstat_error(scstat_errno_t error, char *operand)
{
	char *msg = NULL;
	char *fullmsg = NULL;
	char buffer[SCSTAT_MAX_STRING_LEN];

	buffer[0] = '\0';
	scstat_strerr(error, buffer);

	if (operand) {
		fullmsg = (char *)calloc(1,
		    strlen(operand) + 10 + SCSTAT_MAX_STRING_LEN);
		if (fullmsg == NULL)
			clcommand_perror(CL_ENOMEM);
		sprintf(fullmsg, "%s: %s\n", operand, buffer);
		fprintf(stderr, "%s", fullmsg);
	} else {
		fullmsg = (char *)calloc(1, 10 + strlen(buffer));
		if (fullmsg == NULL)
			clcommand_perror(CL_ENOMEM);
		sprintf(fullmsg, "%s\n", buffer);
		fprintf(stderr, "%s", fullmsg);
	}
}

void
print_rgm_error(scha_errmsg_t error, char *operand)
{
	char *msg = NULL;
	char *fullmsg = NULL;
	boolean_t cmd_needed = B_FALSE;
	boolean_t id_needed = B_FALSE;
	char *curr, *next, *bufptr;
	char *buffer, *dupmsg;
	int cplen = 0;

	if (error.err_msg) {

		if (clerrorMessage.getCommandName() &&
		    (clerrorMessage.getFlags() & TM_FLAG_PRINTCMD))
			cmd_needed = B_TRUE;
		if (clerrorMessage.getFlags() &
		    (TM_FLAG_PRINTID | TM_FLAG_VERBOSE))
			id_needed = B_TRUE;

		buffer = new char [10 + strlen(error.err_msg)];
		dupmsg = new char [10 + strlen(error.err_msg)];

		strcpy(dupmsg, error.err_msg);
		free(error.err_msg);
		error.err_msg = NULL;

		curr = dupmsg;
		while (*curr) {
			if (*curr == '\n') {
				fprintf(stderr, "\n");
				curr++;
				continue;
			}
			//
			// Print the line from here to the next newline,
			// append cmdname if needed, remove msgid if needed
			//
			next = strstr(curr, "\n");
			if (next) {
				cplen = next - curr + 1;
			} else {
				cplen = strlen(curr) + 1;
			}
			snprintf(buffer, cplen, "%s", curr);
			bufptr = buffer;
			if (strstr(buffer, "(C")) {
				if (id_needed)
					bufptr = buffer;
				else
					bufptr = buffer + 10;
			}
			if (bufptr > (buffer + cplen))
				bufptr = buffer;
			if (cmd_needed)
				fprintf(stderr, "%s:  %s",
				    clerrorMessage.getCommandName(), bufptr);
			else
				fprintf(stderr, "%s", bufptr);

			if (next) {
				curr = next;
			} else {
				fprintf(stderr, "\n");
				break;
			}
		}
		delete buffer;
		delete dupmsg;
	} else {
		// ask RGM for an error message to print for this code
		if (error.err_code != SCHA_ERR_NOERR) {
			msg = rgm_error_msg(error.err_code);
			if (msg) {
				if (operand) {
					fullmsg = (char *)calloc(1,
					    strlen(operand) + 10 + strlen(msg));
					if (fullmsg == NULL)
						clcommand_perror(CL_ENOMEM);
					sprintf(fullmsg, "%s: %s\n",
						    operand, msg);
					fprintf(stderr, "%s", fullmsg);
				} else {
					fullmsg = (char *)calloc(1,
					    10 + strlen(msg));
					if (fullmsg == NULL)
						clcommand_perror(CL_ENOMEM);
					sprintf(fullmsg, "%s\n", msg);
					fprintf(stderr, "%s", fullmsg);
				}
			}
		}
	}
}

void
free_prop_array(scha_property_t *prop_array)
{
	namelist_t *nlp;
	namelist_t *nlp_next;

	if (!prop_array)
		return;

	while (prop_array->sp_name) {
		free(prop_array->sp_name);
		nlp = prop_array->sp_value;
		while (nlp) {
			nlp_next = nlp->nl_next;
			free(nlp->nl_name);
			free(nlp);
			nlp = nlp_next;
		}
		prop_array++;
	}
}

char **
vl_to_names(ValueList &values, int *errflg)
{
	int i;
	int vcount = values.getSize();
	char **names_array = NULL;

	*errflg = 0;

	if (vcount == 0) {
		// There are no values. No need to allocate any memory.
		return (names_array);
	}

	names_array = (char **)calloc(
	    (size_t)(vcount + 1), sizeof (char *));

	if (names_array == NULL) {
		clcommand_perror(CL_ENOMEM);
		*errflg = CL_ENOMEM;
		return (NULL);
	}

	i = 0;
	for (values.start(); !values.end(); values.next()) {
		names_array[i] = strdup(values.getValue());
		if (names_array[i] == NULL) {
			clcommand_perror(CL_ENOMEM);
			*errflg = CL_ENOMEM;
			return (NULL);
		}
		i++;
	}
	names_array[i] = NULL;

	return (names_array);
}

char **
namelist_to_names(namelist_t *nl, int *errflg)
{
	int i;
	int vcount = 0;
	char **names_array;
	namelist_t *tmpnl;

	*errflg = 0;

	for (tmpnl = nl; tmpnl; tmpnl = tmpnl->nl_next)
		vcount++;

	names_array = (char **)calloc(
	    (size_t)(vcount + 1), sizeof (char *));

	if (names_array == NULL) {
		clcommand_perror(CL_ENOMEM);
		*errflg = CL_ENOMEM;
		return (NULL);
	}

	i = 0;
	for (tmpnl = nl; tmpnl; tmpnl = tmpnl->nl_next) {
		names_array[i] = strdup(tmpnl->nl_name);
		if (names_array[i] == NULL) {
			clcommand_perror(CL_ENOMEM);
			*errflg = CL_ENOMEM;
			return (NULL);
		}
		i++;
	}
	names_array[i] = NULL;

	return (names_array);
}

//
// Convert a name value list to a pointer to a scha property array.
// If any of the name value pairs are of the form name+=value or name-=value,
// use the name of the passed RG to get the value of the corresponding
// property and then add/subtract.
// This method will pick up the zone cluster context by parsing "objname".
//
scha_property_t *
nvl_to_prop_array(NameValueList &properties, char *objname, int *errflg,
    cl_objtype_t objtype, char *rtype, char *in_zc_name)
{
	int i = 0, j = 0;
	int pcount = properties.getSize();
	scha_property_t *property_array = NULL;
	namelist_t *value_buf = NULL;
	namelist_t *node_buf = NULL;
	namelist_t *value_list = NULL;
	namelist_t *nl = NULL;
	char *tmp_str = NULL;
	char *name_str;
	char *node_str;
	char *dup_str;
	char *value_str;
	int errflg1 = 0;
	boolean_t addprop;
	boolean_t rmprop;
	namelist_t *rgm_val;
	namelist_t *final_props = NULL;
	namelist_t tmp_nl;
	boolean_t is_per_node_prop;
	clerrno_t clerrno = CL_NOERR;
	scconf_errno_t scconf_err = SCCONF_NOERR;
	char *pure_name, *zc_name;
	char *name_part, *zc_part;
	char *prop_obj = NULL;
	char qualifier[4];
	char *modified_objname = NULL;
	char *current_cluster_name = NULL;

#if (SOL_VERSION >= __s10)
	zoneid_t zoneid;
#endif

	*errflg = 0;

	property_array = (scha_property_t *)calloc(
	    (size_t)(pcount + 1), sizeof (scha_property_t));

	if (property_array == NULL) {
		*errflg = CL_ENOMEM;
		clcommand_perror(CL_ENOMEM);
		return (NULL);
	}

	pure_name = zc_name = NULL;

	// Make sure we have the Zone cluster context even when we pass
	// objname as NULL for create & remove operations
	if (in_zc_name) {
		zc_name = strdup(in_zc_name);
	}

	// Parse the ZC name from objname
	if (objname) {
		scconf_err = scconf_parse_obj_name_from_cluster_scope(objname,
						&pure_name, &zc_name);
		if (scconf_err != SCCONF_NOERR) {
			clerrno = map_scconf_error(scconf_err);
			*errflg = clerrno;
			clcommand_perror(clerrno);
			return (NULL);
		}
	}

	i = 0;
	for (properties.start(); !properties.end(); properties.next()) {

		dup_str = strdup(properties.getName());
		if (dup_str == NULL) {
			*errflg = CL_ENOMEM;
			clcommand_perror(CL_ENOMEM);
			return (NULL);
		}
		addprop = B_FALSE;
		rmprop = B_FALSE;
		property_array[i].is_per_node = B_FALSE;
		if (strchr(dup_str, '{') != NULL) {
			name_str = strtok(dup_str, "{");
			node_str = strtok(NULL, "}");
			if (rtype != (char *)NULL) {
				is_per_node_prop = B_FALSE;
				clerrno = prop_is_per_node(name_str, rtype,
				    &is_per_node_prop, zc_name);
				if (clerrno != CL_NOERR) {
					*errflg = clerrno;
					return (NULL);
				}
				property_array[i].is_per_node =
				    is_per_node_prop;
			}

			node_buf = (namelist_t *)
			    calloc(1, sizeof (namelist_t));
			if (node_buf == NULL) {
				*errflg = CL_ENOMEM;
				clcommand_perror(CL_ENOMEM);
				return (NULL);
			}
			node_buf->nl_name = strdup(node_str);
			if (node_buf->nl_name == NULL) {
				*errflg = CL_ENOMEM;
				clcommand_perror(CL_ENOMEM);
				return (NULL);
			}
			node_buf->nl_next = NULL;
			property_array[i].sp_nodes = node_buf;
		} else {
			if (rtype != (char *)NULL) {
				is_per_node_prop = B_FALSE;
				clerrno = prop_is_per_node(dup_str, rtype,
				    &is_per_node_prop, zc_name);
				if (clerrno != CL_NOERR) {
					*errflg = clerrno;
					return (NULL);
				}
				property_array[i].is_per_node =
				    is_per_node_prop;
			}
			if (strchr(dup_str, '+') != NULL) {
				addprop = B_TRUE;
				name_str = strtok(dup_str, "+");
			} else if (strchr(dup_str, '-') != NULL) {
				rmprop = B_TRUE;
				name_str = strtok(dup_str, "-");
			} else {
				name_str = dup_str;
			}
		}

		property_array[i].sp_name = name_str;

		value_str = strdup(properties.getValue());
		if (value_str == NULL) {
			*errflg = CL_ENOMEM;
			clcommand_perror(CL_ENOMEM);
			return (NULL);
		}

		if (strlen(value_str) == 0) {

			if (addprop || rmprop) {
				clerror("You can not add or remove empty "
				    "property elements\n");
				*errflg = CL_EPROP;
				free_prop_array(property_array);
				return (NULL);
			}
			// Null property value
			value_buf = (namelist_t *)
			    calloc(1, sizeof (namelist_t));
			if (value_buf == NULL) {
				*errflg = CL_ENOMEM;
				clcommand_perror(CL_ENOMEM);
				return (NULL);
			}
			value_buf->nl_name = strdup("");
			if (value_buf->nl_name == NULL) {
				*errflg = CL_ENOMEM;
				clcommand_perror(CL_ENOMEM);
				return (NULL);
			}
			value_buf->nl_next = NULL;
			property_array[i].sp_value = value_buf;

		} else if (strcasecmp(name_str, SCHA_RG_DESCRIPTION)
		    == 0 || strcasecmp(name_str, SCHA_R_DESCRIPTION) == 0) {

			if (addprop || rmprop) {
				clerror("You can not add to or remove from "
				    "the properties %s and %s.\n",
				    SCHA_RG_DESCRIPTION, SCHA_R_DESCRIPTION);
				*errflg = CL_EPROP;
				free_prop_array(property_array);
				return (NULL);
			}
			// No comma separated values for these two
			value_buf = (namelist_t *)
			    calloc(1, sizeof (namelist_t));
			if (value_buf == NULL) {
				*errflg = CL_ENOMEM;
				free_prop_array(property_array);
				clcommand_perror(CL_ENOMEM);
				return (NULL);
			}
			value_buf->nl_name = value_str;
			value_buf->nl_next = NULL;
			property_array[i].sp_value = value_buf;

		} else if (strcasecmp(name_str, SCHA_RG_SLM_CPU_SHARES) == 0 ||
			strcasecmp(name_str, SCHA_RG_SLM_PSET_MIN) == 0) {

			if (addprop || rmprop) {
				clerror("You can not add to or remove from "
				    "the properties %s and %s.\n",
				    SCHA_RG_SLM_CPU_SHARES,
				    SCHA_RG_SLM_PSET_MIN);
				*errflg = CL_EPROP;
				free_prop_array(property_array);
				return (NULL);
			}
			// No comma separated values for these two
			value_buf = (namelist_t *)
			    calloc(1, sizeof (namelist_t));
			if (value_buf == NULL) {
				*errflg = CL_ENOMEM;
				free_prop_array(property_array);
				clcommand_perror(CL_ENOMEM);
				return (NULL);
			}
			value_buf->nl_name = value_str;
			value_buf->nl_next = NULL;
			property_array[i].sp_value = value_buf;
		} else if (strcasecmp(name_str, SCHA_RG_AFFINITIES) == 0 ||
				strcasecmp(name_str,
					SCHA_RESOURCE_DEPENDENCIES) == 0 ||
				strcasecmp(name_str,
					SCHA_RESOURCE_DEPENDENCIES_WEAK) == 0 ||
				strcasecmp(name_str,
					SCHA_RESOURCE_DEPENDENCIES_RESTART)
						== 0 ||
				strcasecmp(name_str,
// CSTYLED
					SCHA_RESOURCE_DEPENDENCIES_OFFLINE_RESTART)
						== 0) {
			//
			// The propery values for these could be comma separated
			//
			value_list = strip_commas_in_prop(value_str, &errflg1);

			if (errflg1) {
				*errflg = errflg1;
				free_prop_array(property_array);
				return (NULL);
			}

			//
			// Loop through each of the affinity/dependency to
			// check if we have a <cluster>:<objname> format
			//
			nl = value_list;
			while (nl) {
				//
				// Store the preceeding '+' & '-'
				// in the affinity property value
				// in qualifier
				//
				tmp_str = strdup(nl->nl_name);
				for (j = 0; j < strlen(tmp_str);
						j++) {
					qualifier[j] = tmp_str[j];
					if ((tmp_str[j] != '+') &&
						(tmp_str[j] != '-')) {
						qualifier[j] = '\0';
						break;
					}
				}

				prop_obj = strdup(tmp_str + j);
				*errflg = 0;
				name_part = zc_part = NULL;

// CSTYLED
				scconf_err = scconf_parse_obj_name_from_cluster_scope(
					prop_obj, &name_part, &zc_part);

				if (scconf_err != SCCONF_NOERR) {
					*errflg = CL_EINTERNAL;
					clerror("Internal error.\n");
					return (NULL);
				}

				if (zc_part) {
#if (SOL_VERSION >= __s10)
					if (!name_part) {
						*errflg = CL_EINVAL;
						clerror("Invalid "
						    "resource group "
						    "specified.\n");
						return (NULL);
					}

					//
					// Check the zonecluster context
					// in which we are running the
					// command
					//
					zoneid = getzoneid();
					if (zoneid != GLOBAL_ZONEID) {
					//
					// Running inside a zone cluster
					// Fetch the name of the zone
					// cluster
					//
// CSTYLED
						current_cluster_name = clconf_get_clustername();
					//
					// Check if there is a reference to
					// a object outside of the current
					// cluster
					//
						if (strcmp(current_cluster_name,
							zc_part) != 0) {
						//
						// Cannot process request
						// Intercluster dependencies
						// cannot be set from inside
						// a zonecluster
						//
// CSTYLED
							*errflg = CL_EOP;
// CSTYLED
							clerror("Cannot set inter-cluster dependencies "
// CSTYLED
								"or affinities from a zone cluster. "
// CSTYLED
								"Run the command from "
// CSTYLED
								"the global cluster to set inter-cluster "
// CSTYLED
								"dependencies or affinities.\n");
							return (NULL);
						}
					} else {
						//
						// Running inside
						// the global zone
						// Set the cluster
						// name as GLOBAL
						//
						current_cluster_name
							= GLOBAL_ZONENAME;
					}


					if (zc_name == NULL) {
// CSTYLED
						if (!(strcmp(current_cluster_name, zc_part))) {
						//
						// If the current cluster name
						// matches the zone cluster name
						// specified in the affinities,
						// dependencies then replace
						// the objname with just
						// <qualifier><objname>
						// if not it remains as
						// <qual><cluster>:<obj>
						//
// CSTYLED
							modified_objname = (char *) calloc(1, sizeof (char) * (strlen(name_part) + strlen(qualifier) + 1));
// CSTYLED
							sprintf(modified_objname, "%s%s", qualifier, name_part);
// CSTYLED
							nl->nl_name = modified_objname;
						}
					} else {
// CSTYLED
						if (!(strcmp(zc_name, zc_part))) {
						//
						// If the current cluster name
						// matches the zone cluster name
						// specified in the affinities,
						// dependencies then replace
						// the objname with just
						// <qualifier><objname>
						// if not it remains as
						// <qual><cluster>:<obj>
						//
// CSTYLED
							modified_objname = (char *) calloc(1, sizeof (char) * (strlen(name_part) + strlen(qualifier) + 1));
// CSTYLED
							sprintf(modified_objname, "%s%s", qualifier, name_part);
// CSTYLED
							nl->nl_name = modified_objname;
						}
					}
#endif
				}

				nl = nl->nl_next;
			}

			if (addprop || rmprop) {
				if (objname == NULL) {
					*errflg = CL_EINTERNAL;
					clerror("You can not add to or "
					    "remove from properties using "
					    "this subcommand.\n");
					free_prop_array(property_array);
					return (NULL);
				}
				rgm_val = get_rgm_arraypropval(name_str,
				    objname, &errflg1, objtype);
			}

			if (errflg1) {
				*errflg = errflg1;
				free_prop_array(property_array);
				return (NULL);
			}

			if (addprop) {
				final_props = add_to_prop(rgm_val, value_list,
				    &errflg1);
				rgm_free_nlist(value_list);
				rgm_free_nlist(rgm_val);
				value_list = NULL;
			} else if (rmprop) {
				final_props = rm_from_prop(rgm_val, value_list,
				    &errflg1);
				rgm_free_nlist(value_list);
				rgm_free_nlist(rgm_val);
				value_list = NULL;
			} else {
				final_props = value_list;
			}
			if (errflg1) {
				*errflg = errflg1;
				free_prop_array(property_array);
				rgm_free_nlist(value_list);
				return (NULL);
			}
			//
			// The buffer needs to be edited if the property
			// list is empty.
			//
			if (final_props == NULL) {
				final_props = (namelist_t *)
				    calloc(1, sizeof (namelist_t));
				if (final_props == NULL) {
					*errflg = CL_ENOMEM;
					free_prop_array(property_array);
					clcommand_perror(CL_ENOMEM);
					return (NULL);
				}
				final_props->nl_name = strdup("");
				if (final_props->nl_name == NULL) {
					*errflg = CL_ENOMEM;
					free_prop_array(property_array);
					clcommand_perror(CL_ENOMEM);
					return (NULL);
				}
				final_props->nl_next = NULL;
			}

			property_array[i].sp_value = final_props;
		} else {

			if (addprop || rmprop) {
				if (objname == NULL) {
					*errflg = CL_EINTERNAL;
					clerror("You can not add to or "
					    "remove from properties using "
					    "this subcommand.\n");
					free_prop_array(property_array);
					return (NULL);
				}
				rgm_val = get_rgm_arraypropval(name_str,
				    objname, &errflg1, objtype);
			}

			if (errflg1) {
				*errflg = errflg1;
				free_prop_array(property_array);
				return (NULL);
			}

			value_list = strip_commas_in_prop(value_str, &errflg1);
			if (errflg1) {
				*errflg = errflg1;
				free_prop_array(property_array);
				return (NULL);
			}

			if (addprop) {
				final_props = add_to_prop(rgm_val, value_list,
				    &errflg1);
				rgm_free_nlist(value_list);
				rgm_free_nlist(rgm_val);
				value_list = NULL;
			} else if (rmprop) {
				final_props = rm_from_prop(rgm_val, value_list,
				    &errflg1);
				rgm_free_nlist(value_list);
				rgm_free_nlist(rgm_val);
				value_list = NULL;
			} else {
				final_props = value_list;
			}
			if (errflg1) {
				*errflg = errflg1;
				free_prop_array(property_array);
				rgm_free_nlist(value_list);
				return (NULL);
			}

			//
			// The buffer needs to be edited if the property
			// list is empty.
			//
			if (final_props == NULL) {
				final_props = (namelist_t *)
				    calloc(1, sizeof (namelist_t));
				if (final_props == NULL) {
					*errflg = CL_ENOMEM;
					free_prop_array(property_array);
					clcommand_perror(CL_ENOMEM);
					return (NULL);
				}
				final_props->nl_name = strdup("");
				if (final_props->nl_name == NULL) {
					*errflg = CL_ENOMEM;
					free_prop_array(property_array);
					clcommand_perror(CL_ENOMEM);
					return (NULL);
				}
				final_props->nl_next = NULL;
			}

			property_array[i].sp_value = final_props;
		}
		i++;
	}
	// magic value that librgm looks for
	property_array[pcount].sp_name = NULL;

	if (pure_name)
		free(pure_name);

	if (zc_name)
		free(zc_name);

	return (property_array);
}
//
// For the specified object and the specified array property name, return the
// property value in namelist.
// This method will pick up the zone cluster context by parsing "objname".
//
namelist_t *
get_rgm_arraypropval(char *name_str, char *objname, int *errflg,
    cl_objtype_t objtype)
{
	rgm_rg_t *rg = (rgm_rg_t *)0;
	rgm_resource_t *rs = (rgm_resource_t *)0;
	int rv = 0;
	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};
	rgm_property_list_t *listp;
	rgm_property_t *prop;
	namelist_t *node_nl;
	int errflg1 = 0;
	namelist_t *rdep_nl;
	scconf_errno_t scconf_err = SCCONF_NOERR;
	char *name_part, *zc_part;

	*errflg = 0;
	name_part = zc_part = NULL;

	scconf_err = scconf_parse_obj_name_from_cluster_scope(
			objname, &name_part, &zc_part);

	if (scconf_err != SCCONF_NOERR) {
		clerror("Internal error.\n");
		return (NULL);
	}

	if (objtype == CL_TYPE_RG) {
		// Get the RG details
		scha_status = rgm_scrgadm_getrgconf(name_part, &rg, zc_part);
		rv = scha_status.err_code;
		if (rv != SCHA_ERR_NOERR) {
			print_rgm_error(scha_status, objname);
			*errflg = CL_ENOENT;
			return (NULL);
		}

		if (strcasecmp(name_str, "RG_dependencies") == 0) {
			return (rg->rg_dependencies);
		}

		if (strcasecmp(name_str, "Nodelist") == 0) {
			node_nl = nodeidlist_to_namelist(rg->rg_nodelist,
			    &errflg1);
			if (errflg1) {
				*errflg = errflg1;
				return (NULL);
			}
			return (node_nl);
		}

		if (strcasecmp(name_str, "RG_affinities") == 0) {
			return (rg->rg_affinities);
		}
	} else if (objtype == CL_TYPE_RS) {
		// Get the RS details
		scha_status = rgm_scrgadm_getrsrcconf(name_part, &rs, zc_part);
		rv = scha_status.err_code;
		if (rv != SCHA_ERR_NOERR) {
			print_rgm_error(scha_status, objname);
			*errflg = CL_ENOENT;
			return (NULL);
		}
		if (rs == NULL) {
			clerror("Failed to access resource \"%s\".\n", objname);
			*errflg = CL_ENOENT;
			return (NULL);
		}
		if (strcasecmp(name_str, "Resource_dependencies") == 0) {
			rdep_nl = rdep_list_to_nlist(
			    rs->r_dependencies.dp_strong, &errflg1);
			if (errflg1) {
				*errflg = errflg1;
				return (NULL);
			}
			return (rdep_nl);
		}
		if (strcasecmp(name_str, "Resource_dependencies_weak") == 0) {
			rdep_nl = rdep_list_to_nlist(
			    rs->r_dependencies.dp_weak, &errflg1);
			if (errflg1) {
				*errflg = errflg1;
				return (NULL);
			}
			return (rdep_nl);
		}
		if (strcasecmp(name_str, "Resource_dependencies_restart")
		    == 0) {
			rdep_nl = rdep_list_to_nlist(
			    rs->r_dependencies.dp_restart, &errflg1);
			if (errflg1) {
				*errflg = errflg1;
				return (NULL);
			}
			return (rdep_nl);
		}
		if (strcasecmp(name_str,
		    "Resource_dependencies_offline_restart") == 0) {
			rdep_nl = rdep_list_to_nlist(
			    rs->r_dependencies.dp_offline_restart, &errflg1);
			if (errflg1) {
				*errflg = errflg1;
				return (NULL);
			}
			return (rdep_nl);
		}
		for (listp = rs->r_properties; listp;
		    listp = listp->rpl_next) {
			prop = listp->rpl_property;
			if ((strcasecmp(prop->rp_key, name_str) == 0) &&
			    (prop->rp_type == SCHA_PTYPE_STRINGARRAY))
				return (prop->rp_array_values);
		}
		for (listp = rs->r_ext_properties; listp;
		    listp = listp->rpl_next) {
			prop = listp->rpl_property;
			if ((strcasecmp(prop->rp_key, name_str) == 0) &&
			    (prop->rp_type == SCHA_PTYPE_STRINGARRAY))
				return (prop->rp_array_values);
		}
	}

	clerror("You can not add to or remove from the property %s.\n",
	    name_str);
	*errflg = CL_EPROP;

	// Free any memory
	if (name_part) {
		free(name_part);
		name_part = NULL;
	}
	if (zc_part) {
		free(zc_part);
		zc_part = NULL;
	}
	return (NULL);
}

//
// Add new property elements to an existing RGM property setting.
// This routine allocates space for the resulting namelist.
//
namelist_t *
add_to_prop(namelist_t *rgm_list, namelist_t *new_list, int *errflg)
{
	namelist_t *nl_first = NULL;
	namelist_t *nl_last;
	namelist_t *new_elem;
	namelist_t *orig_list;
	boolean_t found;
	namelist_t *orig_rgm_list;
	namelist_t *orig_new_list;

	*errflg = 0;

	// First add everything from rgm_list
	orig_rgm_list = rgm_list;
	while (rgm_list) {
		new_elem = (namelist_t *)calloc(1, sizeof (namelist_t));
		if (new_elem == NULL) {
			*errflg = CL_ENOMEM;
			clcommand_perror(CL_ENOMEM);
			return (NULL);
		}
		new_elem->nl_name = strdup(rgm_list->nl_name);
		if (new_elem->nl_name == NULL) {
			*errflg = CL_ENOMEM;
			clcommand_perror(CL_ENOMEM);
			return (NULL);
		}
		new_elem->nl_next = NULL;
		if (nl_first == NULL) {
			nl_first = new_elem;
		} else {
			nl_last->nl_next = new_elem;
		}
		nl_last = new_elem;
		rgm_list = rgm_list->nl_next;
	}

	// For each value in the new list, add if not duplicate
	orig_new_list = new_list;
	while (new_list) {
		found = B_FALSE;
		rgm_list = orig_rgm_list;
		while (rgm_list) {
			if (strcmp(rgm_list->nl_name, new_list->nl_name)
			    == 0) {
				found = B_TRUE;
				break;
			}
			rgm_list = rgm_list->nl_next;
		}
		if (found == B_FALSE) {
			new_elem = (namelist_t *)calloc(1, sizeof (namelist_t));
			if (new_elem == NULL) {
				*errflg = CL_ENOMEM;
				clcommand_perror(CL_ENOMEM);
				return (NULL);
			}
			new_elem->nl_name = strdup(new_list->nl_name);
			if (new_elem->nl_name == NULL) {
				*errflg = CL_ENOMEM;
				return (NULL);
			}
			new_elem->nl_next = NULL;
			if (nl_first == NULL) {
				nl_first = new_elem;
			} else {
				nl_last->nl_next = new_elem;
			}
			nl_last = new_elem;
		}
		new_list = new_list->nl_next;
	}
	return (nl_first);
}

//
// Remove specified property elements from an existing RGM property setting.
// This routine allocates memory for the resulting namelist.
//
namelist_t *
rm_from_prop(namelist_t *rgm_list, namelist_t *new_list, int *errflg)
{
	namelist_t *nl_first = NULL;
	namelist_t *nl_last;
	namelist_t *new_elem;
	boolean_t found;
	namelist_t *orig_new_list;

	orig_new_list = new_list;

	// For each element in the original list, copy if not in the remove list
	while (rgm_list) {
		found = B_FALSE;
		new_list = orig_new_list;
		while (new_list) {
			if (strcmp(rgm_list->nl_name, new_list->nl_name)
			    == 0) {
				found = B_TRUE;
				break;
			}
			new_list = new_list->nl_next;
		}
		if (found == B_FALSE) {
			new_elem = (namelist_t *)calloc(1, sizeof (namelist_t));
			if (new_elem == NULL) {
				*errflg = CL_ENOMEM;
				clcommand_perror(CL_ENOMEM);
				return (NULL);
			}
			new_elem->nl_name = strdup(rgm_list->nl_name);
			if (new_elem->nl_name == NULL) {
				*errflg = CL_ENOMEM;
				return (NULL);
			}
			new_elem->nl_next = NULL;
			if (nl_first == NULL) {
				nl_first = new_elem;
			} else {
				nl_last->nl_next = new_elem;
			}
			nl_last = new_elem;
		}
		rgm_list = rgm_list->nl_next;
	}
	return (nl_first);
}

//
// Create a namelist out of comma separated string, allocate memory for
// the resulting namelist.
//
namelist_t *
strip_commas_in_prop(char *value_str, int *errflg)
{
	char lasts[BUFSIZ];
	char *lasts_p = &lasts[0];
	char *value_storage = NULL;
	char *value = NULL;
	namelist_t *value_p = NULL;
	namelist_t *current_value = NULL;
	namelist_t *first_value = NULL;

	*errflg = 0;

	value = (char *)strtok_r(value_str, ",", (char **)&lasts_p);

	// Special case where raw_value is comma(s) only
	if (value == NULL) {
		value_p = (namelist_t *)calloc(1, sizeof (namelist_t));
		if (value_p == NULL) {
			clcommand_perror(CL_ENOMEM);
			*errflg = CL_ENOMEM;
			return (NULL);
		}
		value_p->nl_name = "";
		value_p->nl_next = NULL;
		return (value_p);
	}

	while (value != NULL) {
		value_p = (namelist_t *)calloc(1, sizeof (namelist_t));
		value_storage = strdup(value);

		if (value_p && value_storage) {
			value_p->nl_name = value_storage;
			value_p->nl_next = NULL;

			if (first_value == NULL)
				first_value = value_p;

			if (current_value != NULL)
				current_value->nl_next = value_p;

			current_value = value_p;

			value = (char *)strtok_r(NULL, ",", (char **)&lasts_p);
			value_p = NULL;
			value_storage = NULL;
		} else {
			clcommand_perror(CL_ENOMEM);
			*errflg = CL_ENOMEM;
			return (NULL);
		}
	}
	return (first_value);
}

//
// Add ValueList elements to an existing namelist.
// Space is allocated for the resulting namelist.
//
namelist_t *
add_vl_to_names(namelist_t *rgm_list, ValueList &new_list, int *errflg)
{
	namelist_t *new_nodelist;
	namelist_t *nl_first = NULL;
	namelist_t *nl_last;
	namelist_t *new_elem;
	namelist_t *orig_nodelist;
	boolean_t found;
	namelist_t *orig_rgm_list;

	*errflg = 0;

	// First add everything from rgm_list
	orig_rgm_list = rgm_list;
	while (rgm_list) {
		new_elem = (namelist_t *)calloc(1, sizeof (namelist_t));
		if (new_elem == NULL) {
			clcommand_perror(CL_ENOMEM);
			*errflg = CL_ENOMEM;
			return (NULL);
		}
		new_elem->nl_name = strdup(rgm_list->nl_name);
		if (new_elem->nl_name == NULL) {
			clcommand_perror(CL_ENOMEM);
			*errflg = CL_ENOMEM;
			return (NULL);
		}
		new_elem->nl_next = NULL;
		if (nl_first == NULL) {
			nl_first = new_elem;
		} else {
			nl_last->nl_next = new_elem;
		}
		nl_last = new_elem;
		rgm_list = rgm_list->nl_next;
	}

	// For each node in the new list, add if not duplicate
	for (new_list.start(); new_list.end() != 1; new_list.next()) {
		found = B_FALSE;
		rgm_list = orig_rgm_list;
		while (rgm_list) {
			if (strcmp(rgm_list->nl_name, new_list.getValue())
			    == 0) {
				found = B_TRUE;
				break;
			}
			rgm_list = rgm_list->nl_next;
		}
		if (found == B_FALSE) {
			new_elem = (namelist_t *)calloc(1, sizeof (namelist_t));
			if (new_elem == NULL) {
				clcommand_perror(CL_ENOMEM);
				*errflg = CL_ENOMEM;
				return (NULL);
			}
			new_elem->nl_name = strdup(new_list.getValue());
			if (new_elem->nl_name == NULL) {
				clcommand_perror(CL_ENOMEM);
				*errflg = CL_ENOMEM;
				return (NULL);
			}
			new_elem->nl_next = NULL;
			if (nl_first == NULL) {
				nl_first = new_elem;
			} else {
				nl_last->nl_next = new_elem;
			}
			nl_last = new_elem;
		}
	}
	return (nl_first);
}

//
// Remove ValueList elements from an existing namelist.
// Space is allocated for the resulting namelist.
//
namelist_t *
remove_vl_from_names(namelist_t *rgm_list, ValueList &new_list, int *errflg)
{
	namelist_t *nl_first = NULL;
	namelist_t *nl_last;
	namelist_t *new_elem;
	boolean_t found;

	*errflg = 0;

	// For each node in the original list, copy if not in the remove list
	while (rgm_list) {
		found = B_FALSE;
		for (new_list.start(); new_list.end() != 1; new_list.next()) {
			if (strcmp(rgm_list->nl_name, new_list.getValue())
			    == 0) {
				found = B_TRUE;
				break;
			}
		}
		if (found == B_FALSE) {
			new_elem = (namelist_t *)calloc(1, sizeof (namelist_t));
			if (new_elem == NULL) {
				clcommand_perror(CL_ENOMEM);
				*errflg = CL_ENOMEM;
				return (NULL);
			}
			new_elem->nl_name = strdup(rgm_list->nl_name);
			if (new_elem->nl_name == NULL) {
				clcommand_perror(CL_ENOMEM);
				*errflg = CL_ENOMEM;
				return (NULL);
			}
			new_elem->nl_next = NULL;
			if (nl_first == NULL) {
				nl_first = new_elem;
			} else {
				nl_last->nl_next = new_elem;
			}
			nl_last = new_elem;
		}
		rgm_list = rgm_list->nl_next;
	}
	return (nl_first);
}

scha_errmsg_t
modify_rg(char *rg_name, scha_property_t *prop, int flag, const char **rgnames)
{
	scha_errmsg_t status = {SCHA_ERR_NOERR, NULL};
	scconf_errno_t scconf_err = SCCONF_NOERR;
	char *rg_part, *zc_part;

	if (prop) {
		rg_part = zc_part = NULL;
		// First parse the RG name and the zone cluster name.
		scconf_err = scconf_parse_obj_name_from_cluster_scope(
					rg_name, &rg_part, &zc_part);

		if (scconf_err != SCCONF_NOERR) {
			status.err_code = SCHA_ERR_NOMEM;
			return (status);
		}
		/* modify rg configuration */
		status = rgm_scrgadm_update_rg_property(
		    rg_part, &prop, flag, rgnames, zc_part);

		if (rg_part) {
			free(rg_part);
			rg_part = NULL;
		}
		if (zc_part) {
			free(zc_part);
			zc_part = NULL;
		}
	}
	return (status);
}

//
//	Return a single string for the list of names with space separator.
//	The caller is responsible for freeing the returned list.
//
char *
names_to_str(namelist_t *name_list, int *errflg)
{
	return (names_to_str_internal(name_list, NULL, errflg, " "));
}

//
//	Return a single string for the list of names with comma separator.
//	The caller is responsible for freeing the returned list.
//
char *
names_to_str_com(namelist_t *name_list, char *propname, int *errflg)
{
	return (names_to_str_internal(name_list, propname, errflg, ","));
}

//
// Use the passed separator for formulating the string equivalent of
// name list.
//
static char *
names_to_str_internal(namelist_t *name_list, char *propname, int *errflg,
    char *sep)
{
	size_t len = 0;
	namelist_t *nl;
	char *string;
	boolean_t conv_nodeid = B_FALSE;
	boolean_t id_used = B_TRUE;
	boolean_t iseuropa;
	scconf_nodeid_t nodeid;
	clerrno_t clerrno = CL_NOERR;
	char *atchr;
	char *c;
	char *nodestr;

	*errflg = 0;

	if (!sep || (*sep == '\0')) {
		*errflg = CL_EINTERNAL;
		return ((char *)0);
	}

	if (propname) {
		if (strcasecmp(propname, "NetIfList") == 0) {
			conv_nodeid = B_TRUE;
		}
	}

	// Check if europa is enabled
	clerrno = europa_open(&iseuropa);
	if (clerrno) {
		*errflg = clerrno;
		return ((char *)0);
	}

	// Get the length required for the string buffer
	for (nl = name_list;  nl;  nl = nl->nl_next) {
		if (nl->nl_name && *nl->nl_name) {
			if (conv_nodeid) {
				atchr = strchr(nl->nl_name, '@');
				if (atchr != NULL) {
					id_used = B_TRUE;
					for (c = atchr + 1; *c; ++c) {
						if (!isdigit(*c))
							id_used = B_FALSE;
					}
					if (id_used) {
						nodestr = replace_nodeid(
						    nl->nl_name, atchr + 1,
						    iseuropa, errflg);

						if (nodestr) {
							free(nl->nl_name);
							nl->nl_name =
							    nodestr;
						}
					}
				}
			}
			len += strlen(nl->nl_name) + 1;
		} else {
			len += sizeof ("<NULL>");
		}
	}

	if (iseuropa)
		europa_close();

	// Allocate the string buffer
	if ((string = (char *)calloc(1, len + 10)) == NULL) {
		*errflg = CL_ENOMEM;
		clcommand_perror(CL_ENOMEM);
		return ((char *)0);
	}

	if (len == 0) {
		(void) strcat(string, "<NULL>");
		return (string);
	}

	// Add values to the string buffer
	for (nl = name_list;  nl;  nl = nl->nl_next) {
		/* trailing space */
		if (*string != '\0')
			(void) strcat(string, sep);
		// name (or NULL)
		if (nl->nl_name && *nl->nl_name) {
			(void) strcat(string, nl->nl_name);
		} else {
			(void) strcat(string, "<NULL>");
		}
	}

	return (string);
}

//
// Replace the nodeid portion of a string with nodename. This is a fix with
// only the NetIfList in mind.
//
static char *
replace_nodeid(char *start_str, char *nodeid_str, boolean_t iseuropa,
    int *errflg)
{
	scconf_nodeid_t nodeid;
	char *nodename = NULL;
	char *new_str = NULL;
	scconf_errno_t scconf_status = SCCONF_NOERR;

	*errflg = 0;

	nodeid = (scconf_nodeid_t)atoi(nodeid_str);
	scconf_status = scconf_get_nodename(nodeid, &nodename);
	if (iseuropa && scconf_status == SCCONF_ENOEXIST) {
		scconf_status = scconf_get_farmnodename(nodeid, &nodename);
	}
	if (scconf_status != SCCONF_NOERR) {
		clerror("Warning: failed to get nodename from nodeid used "
		    "in the \"NetIfList\" property value.\n");
		*errflg = map_scconf_error(scconf_status);
		return (NULL);
	}
	new_str = (char *)calloc(strlen(nodename) + nodeid_str - start_str + 1,
	    sizeof (char));
	if (!new_str) {
		*errflg = CL_ENOMEM;
		clcommand_perror(CL_ENOMEM);
		return (NULL);
	}
	strncpy(new_str, start_str, nodeid_str - start_str);
	strcat(new_str, nodename);
	if (nodename)
		free(nodename);
	return (new_str);
}

//
// Replace multiple nodename portions of a string (netiflist value)
// with nodeids. This is a fix with only the NetIfList property in mind.
//
char *
replace_nodenames(char *ip_str, int *errflg)
{
	*errflg = 0;
	int num_ent = 1;
	boolean_t iseuropa;
	scconf_nodeid_t nodeid;
	char *dup_str = NULL;
	char *ptr;
	char *new_str = NULL;
	char *dest_ptr;
	char *c;
	char *zonep;
	char *saved_c;
	clerrno_t clerrno = CL_NOERR;
	scconf_errno_t scconf_status = SCCONF_NOERR;
	boolean_t is_nodeid;

	if (ip_str == NULL)
		goto cleanup;

	if (strlen(ip_str) == 0)
		goto cleanup;

	dup_str = strdup(ip_str);
	if (!dup_str) {
		clcommand_perror(CL_ENOMEM);
		*errflg = CL_ENOMEM;
		goto cleanup;
	}
	// First count the number of entries
	ptr = strtok(dup_str, ",");
	while (ptr) {
		num_ent++;
		if (strchr(ptr, '@') == NULL) {
			// malformed property we cannot handle
			clerror("Failed to interprete property value "
			    "\"%s\".\n", ip_str);
			*errflg = CL_EINVAL;
			goto cleanup;
		}
		ptr = strtok(NULL, ",");
	}

	new_str = (char *)calloc(strlen(ip_str) + num_ent * 16, sizeof (char));
	if (!new_str) {
		clcommand_perror(CL_ENOMEM);
		*errflg = CL_ENOMEM;
		goto cleanup;
	}

	// Check if europa is enabled
	clerrno = europa_open(&iseuropa);
	if (clerrno) {
		*errflg = clerrno;
		free(new_str);
		new_str = NULL;
		goto cleanup;
	}

	strcpy(dup_str, ip_str);
	dest_ptr = new_str;
	ptr = strtok(dup_str, ",");
	while (ptr) {
		c = ptr;
		while (c) {
			*dest_ptr = *c;
			if (*c == '@')
				break;
			dest_ptr++;
			c++;
		}
		c++;
		dest_ptr++;
		is_nodeid = B_TRUE;
		saved_c = c;
		while (*c) {
			if (!isdigit(*c)) {
				is_nodeid = B_FALSE;
				break;
			}
			c++;
		}
		if (is_nodeid) {
			sprintf(dest_ptr, "%s", saved_c);
		} else {
			zonep = strchr(saved_c, ':');
			if (zonep) {
				*zonep = '\0';
				zonep++;
				if (!*zonep) {
					clerror("Malformed \"NetIfList\" "
					    "property value.\n");
					*errflg = CL_EINVAL;
					free(new_str);
					new_str = NULL;
					goto cleanup;
				}
			}
			scconf_status = scconf_get_nodeid(saved_c, &nodeid);
			if (iseuropa && scconf_status == SCCONF_ENOEXIST) {
				scconf_status = scconf_get_farmnodeid(c,
				    &nodeid);
			}
			if (scconf_status != SCCONF_NOERR) {
				clerror("Failed to get nodeid from nodename "
				    "used in the \"NetIfList\" property "
				    "value.\n");
				*errflg = map_scconf_error(scconf_status);
				free(new_str);
				new_str = NULL;
				goto cleanup;
			}
			sprintf(dest_ptr, "%d", nodeid);
			if (zonep) {
				strcat(dest_ptr, ":");
				strcat(dest_ptr, zonep);
			}
		}
		ptr = strtok(NULL, ",");
		if (ptr) {
			dest_ptr = new_str + strlen(new_str);
			*dest_ptr++ = ',';
		}
	}

cleanup:
	if (iseuropa)
		europa_close();
	if (dup_str)
		free(dup_str);
	return (new_str);
}

//
// The following routines try to establish a relationship between the
// newcli accepted state names and scstat state names. This should be
// replaced by a static table, and a new implementation of scstat_get_rg()
// and scstat_get_rs() that returns detailed error codes without compressing
// them!
//
clerrno_t
check_state(char *state, cl_objtype_t objtype)
{
	int i;

	if (objtype == CL_TYPE_RG) {
		for (i = 0; rg_state_mapping[i].scstat_string; i++) {
			if (strcasecmp(rg_state_mapping[i].cl_string,
			    state) == 0) {
				return (CL_NOERR);
			}
		}
	} else if (objtype == CL_TYPE_RS) {
		for (i = 0; rs_state_mapping[i].scstat_string; i++) {
			if (strcasecmp(rs_state_mapping[i].cl_string,
			    state) == 0) {
				return (CL_NOERR);
			}
		}
	} else {
		clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}

	clerror("Invalid state \"%s\" specified.\n", state);
	return (CL_EINVAL);
}

char *
scstat_str_to_cl_str(char *state, cl_objtype_t objtype)
{
	int i;

	if (objtype == CL_TYPE_RG) {
		for (i = 0; rg_state_mapping[i].scstat_string; i++) {
			if (strcasecmp(rg_state_mapping[i].scstat_string,
			    state) == 0) {
				return (rg_state_mapping[i].cl_string);
			}
		}
	} else if (objtype == CL_TYPE_RS) {
		for (i = 0; rs_state_mapping[i].scstat_string; i++) {
			if (strcasecmp(rs_state_mapping[i].scstat_string,
			    state) == 0) {
				return (rs_state_mapping[i].cl_string);
			}
		}
	}

	return ((char *)NULL);
}

//
// Check each node and return the node name.
//
clerrno_t
get_good_node(char *input_node, char **node_name)
{
	scconf_nodeid_t nodeid;
	char *nodename = NULL;
	char *tmp_node = (char *)NULL;
	char *new_str = (char *)NULL;
	char *s;
	int zonelen;
	char *nodepart, *zonepart;
	scconf_errno_t scconf_status = SCCONF_NOERR;
	char errbuff[BUFSIZ];
	clerrno_t clerrno = CL_NOERR;
	boolean_t iseuropa;
	int clconf_err = 0;
	uint_t cluster_id;

	// Check if europa is enabled
	clerrno = europa_open(&iseuropa);
	if (clerrno) {
		return (clerrno);
	}

	tmp_node = strdup(input_node);
	if (!tmp_node) {
		clcommand_perror(CL_ENOMEM);
		return (CL_ENOMEM);
	}

	nodepart = strtok(tmp_node, ":");
	if (!nodepart) {
		clerror("Invalid node specified.\n");
		return (CL_EINVAL);
	}

	zonepart = strtok(NULL, ":");
	zonelen = 0;
	if (zonepart) {
		zonelen = strlen(zonepart);

#if (SOL_VERSION >= __s10)
		//
		// Check if we have a zone cluster name specified as a zone name
		// If specified we throw an appropriate error message
		// If not we allow the regular validations
		//

		// Check the cluster id
		clconf_err = clconf_get_cluster_id(zonepart, &cluster_id);
		if ((!clconf_err) && (cluster_id >= MIN_CLUSTER_ID)) {
			//
			// If we are here, then there is a match for
			// the zone name specified and the
			// zone-cluster names that are already existing
			// in the cluster
			//
			clerror("You cannot specify a zone-cluster "
				"name in a \"%s:%s\" format.\n",
				nodepart, zonepart);

			clerrno = CL_EINVAL;
			goto cleanup;
		}
#endif
	}

	// Check to see if nodename is a nodeid (number > 0)
	for (s = nodepart; *s; ++s)
		if (!isdigit(*s))
			break;
	if (*s == '\0') {
		// nodeid specified
		nodeid = (scconf_nodeid_t)atoi(nodepart);
		if (nodeid < 1) {
			clerror("\"%s\" is not a valid node ID.\n",
			    nodepart);
			clerrno = CL_EINVAL;
			goto cleanup;
		} else {
			scconf_status = scconf_get_nodename(nodeid, &nodename);
			if (iseuropa && scconf_status == SCCONF_ENOEXIST) {
				scconf_status = scconf_get_farmnodename(nodeid,
				    &nodename);
			}
			if (scconf_status != SCCONF_NOERR) {
				if (iseuropa) {
					clerror("\"%s\" is not a valid "
					    "cluster/farm node ID.\n",
					    nodepart);
				} else {
					clerror("\"%s\" is not a valid cluster "
					    "node ID.\n", nodepart);
				}
				clerrno = CL_EINVAL;
				goto cleanup;
			}
			new_str = (char *)calloc(strlen(nodename) +
			    zonelen + 3, sizeof (char));
			if (!new_str) {
				clcommand_perror(CL_ENOMEM);
				clerrno = CL_ENOMEM;
				goto cleanup;
			}
			strcat(new_str, nodename);
			if (zonepart) {
				strcat(new_str, ":");
				strcat(new_str, zonepart);
			}
			*node_name = new_str;
		}
	} else {
		// nodename specified
		scconf_status = scconf_get_nodeid(nodepart, &nodeid);
		if (iseuropa && scconf_status == SCCONF_ENOEXIST) {
			scconf_status = scconf_get_farmnodeid(nodepart,
			    &nodeid);
		}
		if (scconf_status != SCCONF_NOERR) {
			if (iseuropa) {
				clerror("\"%s\" is not a valid cluster/farm "
				    "node.\n", nodepart);
			} else {
				clerror("\"%s\" is not a valid cluster node.\n",
				    nodepart);
			}
			clerrno = CL_EINVAL;
			goto cleanup;
		}
		new_str = strdup(input_node);
		if (!new_str) {
			clcommand_perror(CL_ENOMEM);
			clerrno = CL_ENOMEM;
			goto cleanup;
		}
		*node_name = new_str;
	}
cleanup:
	if (iseuropa)
		europa_close();

	if (nodename)
		free(nodename);
	if (tmp_node)
		free(tmp_node);
	return (clerrno);
}

#if (SOL_VERSION >= __s10)
clerrno_t
get_good_node_for_zc(char *input_node, char **node_name, char *zc_name) {
	scconf_nodeid_t nodeid;
	char *nodename = NULL;
	char *new_str = (char *)NULL;
	char *s;
	char *nodepart;
	scconf_errno_t scconf_status = SCCONF_NOERR;
	clerrno_t clerrno = CL_NOERR;

	nodepart = strdup(input_node);
	if (!nodepart) {
		clcommand_perror(CL_ENOMEM);
		return (CL_ENOMEM);
	}

	// Check to see if nodename is a nodeid (number > 0)
	for (s = nodepart; *s; ++s)
		if (!isdigit(*s))
			break;
	if (*s == '\0') {
		// nodeid specified
		nodeid = (scconf_nodeid_t)atoi(nodepart);
		if (nodeid < 1) {
			clerror("\"%s\" is not a valid node ID.\n",
			    nodepart);
			clerrno = CL_EINVAL;
			goto cleanup;
		} else {

			scconf_status =
				scconf_get_zc_nodename_by_nodeid(zc_name,
				nodeid, &nodename);
			if (scconf_status != SCCONF_NOERR) {
				clerror("\"%s\" is not a valid cluster "
					"node ID.\n", nodepart);
				clerrno = CL_EINVAL;
				goto cleanup;
			}

			new_str = (char *)calloc(strlen(nodename) + 1,
				sizeof (char));
			if (!new_str) {
				clcommand_perror(CL_ENOMEM);
				clerrno = CL_ENOMEM;
				goto cleanup;
			}
			strcat(new_str, nodename);
			*node_name = new_str;
		}
	} else {
		// nodename specified
		nodeid = clconf_zc_get_nodeid_by_nodename(zc_name, nodepart);

		if (nodeid < 1) {
			clerror("\"%s\" is not a valid cluster node.\n",
				nodepart);
			clerrno = CL_EINVAL;
			goto cleanup;
		}
		new_str = strdup(input_node);
		if (!new_str) {
			clcommand_perror(CL_ENOMEM);
			clerrno = CL_ENOMEM;
			goto cleanup;
		}
		*node_name = new_str;
	}
cleanup:
	if (nodename)
		free(nodename);

	return (clerrno);
}
#endif

//
// Check each node and print any error. Leave it upto the caller
// to abort if there were errors.
// If zc_list contains a list of zone clusters, this
// method will check whether each node is a part
// of at least one zone cluster in "zc_list", else
// it will return an error. This function assumes that
// "zc_list" consists of valid zone cluster names and hence
// does not check their validity. This function performs
// according to context in which it has been called.
// eg: zone cluster context, global zone context.
// If "nodelist" is empty, this method simply returns with CL_NOERR.
//
clerrno_t
get_good_nodes(ValueList &nodelist, ValueList &good_nodes, ValueList &zc_list)
{
	clerrno_t first_err = CL_NOERR;
	clerrno_t clerrno = CL_NOERR;
	int rv;
	char *node_name = (char *)NULL;
	bool gz_flag = true;
	char *zc_name;
	ValueList all_zc_host_list;
	bool global_type_flag = false;
	ValueList nodelist_to_check;
	boolean_t zc_flag = B_FALSE;
	scconf_errno_t scconf_err = SCCONF_NOERR;

	// Was "nodelist" specified??
	if (nodelist.getSize() < 1) {
		// We do this check so that there is no need to make
		// any additional checks.
		return (CL_NOERR);
	}

	// Check whether we are in the global zone.
	if (sc_nv_zonescheck()) {
		// We are inside a zone
		gz_flag = false;

#if (SOL_VERSION >= __s10)
		// Check whether we are in a zone cluster.
		scconf_err = scconf_zone_cluster_check(&zc_flag);

		if (scconf_err != SCCONF_NOERR) {
			clerrno = map_scconf_error(scconf_err);
			clcommand_perror(clerrno);
			return (clerrno);
		}
#endif
	}

	// Let us check for usage errors. This is something which
	// should be handled in the option processing code, but
	// since this is a common usage check for anyone who is
	// using this function, we are putting it here.

	// If zone-clusters are specified, we will not support
	// <node:zone> format. Similarly, if we are inside a zone
	// cluster, <node:zone> format will not be supported.
	if ((zc_list.getSize() > 0) || (zc_flag == B_TRUE)) {
		for (nodelist.start(); !nodelist.end(); nodelist.next()) {
			if (strchr(nodelist.getValue(), ':')) {
				// This nodename is in the format
				// <node:zone> and we are not supporting
				// it here.
				clerror("You cannot specify zones as part"
					" of node names while operating on a "
					"zone cluster.\n");
				return (CL_EINVAL);
			}
		}
	}

	// If we are in the global zone and "zc_list" was specified,
	// we have to check whether the given "input_node" is
	// a valid zone-host name in one of the zone clusters
	// specified in "zc_list".

	if ((zc_list.getSize() > 0) && (gz_flag)) {
#if (SOL_VERSION >= __s10)
		// If we are here, then we have to check whether
		// each of the nodes in "nodelist" is a valid
		// zone hostname in at least one zone cluster
		// mentioned.
		// First get the entire zone hostname list.
		for (zc_list.start(); !zc_list.end(); zc_list.next()) {
			zc_name = zc_list.getValue();
			// If the user specified "global" as a zone cluster
			// name, we wont be able to fetch the hostname list.

			if (!(strcmp(zc_name, GLOBAL_ZONE_NAME))) {
				global_type_flag = true;
				continue;
			}

			clerrno = get_hostname_list_for_zc(zc_name,
							all_zc_host_list);

			if (clerrno == CL_ENOENT) {
				// Invalid zone cluster name. Ignore it.
				continue;
			} else if (clerrno != CL_NOERR) {
				// Internal error.
				clerror("Internal Error.\n");
				return (CL_EINTERNAL);
			}
		}

		// We have the entire zone hostname list across
		// all zone clusters. Now, we can check whether
		// each specified node is a valid zone cluster or not.

		for (nodelist.start(); !nodelist.end(); nodelist.next()) {
			if (all_zc_host_list.isValue(nodelist.getValue())) {
				// I am present. I passed the check.
				good_nodes.add(nodelist.getValue());
			} else {
				// I am not a zone cluster hostname.
				// If "global" was specified, then we have to
				// check whether this is a valid cluster node.
				// Adding it to nodelist_to_check.
				if (global_type_flag) {
					nodelist_to_check.add(
						nodelist.getValue());
					continue;
				}

				// I am not a valid zone hostname. Mention it
				clerror("%s is not a valid cluster node.\n",
					nodelist.getValue());

				if (first_err == CL_NOERR) {
					first_err = CL_EINVAL;
				}
			}
		}

		// If there is nothing to check further, we can return.
		if (nodelist_to_check.getSize() < 1) {
			// Free any memory.
			all_zc_host_list.clear();
			return (first_err);
		}
#endif
	} else {
		// Check the entire node list.
		nodelist_to_check = nodelist;
	}

	for (nodelist_to_check.start(); nodelist_to_check.end() != 1;
		nodelist_to_check.next()) {

		if (node_name) {
			free(node_name);
			node_name = NULL;
		}
		// Check whether this is a valid server/farm node.
		clerrno = get_good_node(nodelist_to_check.getValue(),
					&node_name);
		if (clerrno == 0) {
			good_nodes.add(node_name);
		} else {
			if (first_err == 0) {
				// first error!
				first_err = clerrno;
			}
		}
	}
	if (node_name)
		free(node_name);
	return (first_err);
}

clerrno_t
get_r_rtname(char *rtype, char **r_rtname, char *zc_name)
{
	int rv = 0;
	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};

	scha_status = rgmcnfg_get_rtrealname(rtype, r_rtname, zc_name);
	rv = scha_status.err_code;
	if (rv != SCHA_ERR_NOERR) {
		print_rgm_error(scha_status, rtype);
		return (map_scha_error(rv));
	} else {
		return (CL_NOERR);
	}
}

//
//	Returns 0 if standard property
//		1 if extension property
//		2 if both
//		-1 if not found
//	If +/- is used in the property name, return the actual prop name
//	in name_str.
//
int
get_property_type(char *prop, NameValueList &proplist, int *errflg,
    char **actual_name)
{
	int xfound = 0;
	int yfound = 0;
	char *dup_str;
	char *tmp_name;

	*errflg = 0;
	dup_str = strdup(prop);
	if (dup_str == NULL) {
		*errflg = CL_ENOMEM;
		clcommand_perror(CL_ENOMEM);
		return (-1);
	}
	if (strchr(dup_str, '{') != NULL) {
		tmp_name = strtok(dup_str, "{");
	} else {
		if (strchr(dup_str, '+') != NULL)
			tmp_name = strtok(dup_str, "+");
		else if (strchr(dup_str, '-') != NULL)
			tmp_name = strtok(dup_str, "-");
		else
			tmp_name = dup_str;
	}

	*actual_name = tmp_name;
	for (proplist.start(); proplist.end() != 1; proplist.next()) {

		if (strcasecmp(*actual_name, proplist.getName()) == 0) {
			if (strncmp(proplist.getValue(), "standard",
			    strlen("standard")) == 0) {
				yfound = 1;
			}
			if (strncmp(proplist.getValue(), "extension",
			    strlen("extension")) == 0) {
				xfound = 1;
			}
		}
	}
	if (xfound && yfound)
		return (2);
	else if (xfound)
		return (1);
	else if (yfound)
		return (0);
	else
		return (-1);
}

//
// Get property names and extension/standard attributes associated for all
// RTs registered
//
clerrno_t
get_all_r_props(NameValueList &proplist)
{
	char **rt_names = (char **)0;
	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};
	char **rt_namep;
	int rv = 0;

	scha_status = rgm_scrgadm_getrtlist(&rt_names, NULL);
	rv = scha_status.err_code;
	if (rv != SCHA_ERR_NOERR) {
		print_rgm_error(scha_status, NULL);
		return (CL_EINTERNAL);
	}
	if (rt_names == NULL)
		return (CL_NOERR);

	//
	// These are not real properties and can not be set, add them
	// here as these can still be used for filtering output.
	//
	proplist.add("Type", "standard");
	proplist.add("Monitored", "standard_pn");
	proplist.add("Monitored_switch", "standard_pn");
	proplist.add("Enabled", "standard_pn");
	proplist.add("On_off_switch", "standard_pn");
	proplist.add("Group", "standard");

	for (rt_namep = rt_names;  rt_namep && *rt_namep;  ++rt_namep)
		(void) get_r_props(*rt_namep, proplist);

	rgm_free_strarray(rt_names);
	return (CL_NOERR);
}

//
// Get property names and extension/standard attributes associated with an RT
//
clerrno_t
get_r_props(char *rt_name, NameValueList &proplist, char *zc_name)
{
	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};
	rgm_rt_t *rt = (rgm_rt_t *)0;
	rgm_param_t **tablep;
	int rv = 0;

	// These are not saved as real properties, so add them here
	proplist.add("Resource_dependencies", "standard");
	proplist.add("Resource_dependencies_restart", "standard");
	proplist.add("Resource_dependencies_offline_restart", "standard");
	proplist.add("Resource_dependencies_weak", "standard");
	proplist.add("Resource_project_name", "standard");
	proplist.add("R_description", "standard");
	proplist.add("Type_version", "standard");

	// Get configuration for this rt name
	scha_status = rgm_scrgadm_getrtconf(rt_name, &rt, zc_name);
	rv = scha_status.err_code;
	if (rv != SCHA_ERR_NOERR) {
		print_rgm_error(scha_status, rt_name);
		return (CL_EINTERNAL);
	}
	if (rt == NULL) {
		clerror("Failed to access resource type information for "
		    "\"%s\".\n", rt_name);
		return (CL_EINTERNAL);
	}

	if (!(rt->rt_paramtable && *rt->rt_paramtable))
		return (CL_NOERR);

	for (tablep = rt->rt_paramtable; *tablep && (*tablep)->p_name;
	    ++tablep) {
		if ((*tablep)->p_extension == B_FALSE) {
			proplist.add((*tablep)->p_name, "standard");
		} else {
			if ((*tablep)->p_per_node == B_TRUE) {
				proplist.add((*tablep)->p_name, "extension_pn");
			} else {
				proplist.add((*tablep)->p_name, "extension");
			}
		}
	}
	rgm_free_rt(rt);
	return (CL_NOERR);
}

clerrno_t
check_rg(char *rgname, char *zc_name)
{
	rgm_rg_t *rg = (rgm_rg_t *)0;
	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};
	int rv = 0;

	scha_status = rgm_scrgadm_getrgconf(rgname, &rg, zc_name);
	rv = scha_status.err_code;
	if (rv != SCHA_ERR_NOERR) {
		print_rgm_error(scha_status, rgname);
		return (CL_EINVAL);
	}
	if (rg)
		free(rg);
	return (CL_NOERR);
}

int
match_type(ValueList &resourcetypes, rgm_resource_t *rs)
{
	int type_found = 0;

	for (resourcetypes.start(); resourcetypes.end() != 1;
	    resourcetypes.next()) {
		if (strcmp(resourcetypes.getValue(), rs->r_type) == 0)
			type_found = 1;
	}
	return (type_found);
}

clerrno_t
match_rt_rg_in_zc(ValueList &resourceobjs, rgm_resource_t *rs,
    char *zc_name, boolean_t &type_found, int obj_type)
{
	char *zc_part, *obj_part;
	scconf_errno_t scconf_err = SCCONF_NOERR;
	clerrno_t clerrno = CL_NOERR;

	type_found = B_FALSE;
	zc_part = obj_part = NULL;
	for (resourceobjs.start(); resourceobjs.end() != 1;
	    resourceobjs.next()) {
		if (zc_part) {
			free(zc_part);
			zc_part = NULL;
		}

		if (obj_part) {
			free(obj_part);
			obj_part = NULL;
		}
		// First separate the Obj name from zone cluster scope
		// The parser is different for each type of object.
		// RTs have a different parser. RGs have a different parser.
		if (obj_type) {
			scconf_err = scconf_parse_obj_name_from_cluster_scope(
					resourceobjs.getValue(), &obj_part,
					&zc_part);
		} else {
			scconf_err = scconf_parse_rt_name_from_cluster_scope(
					resourceobjs.getValue(), &obj_part,
					&zc_part);
		}
		if (scconf_err != SCCONF_NOERR) {
			clerrno = map_scconf_error(scconf_err);
			break;
		}

		// Check the zone cluster name first.
		if ((zc_name != NULL) || (zc_part != NULL)) {
			// Either zc_name or zc_part is not null, or
			// both are non-null.
			if ((zc_name == NULL) || (zc_part == NULL)) {
				// If we are here, it means one of zc_part
				// or zc_name is null and the other is not.
				continue;
			}
			// If we are here, both zc_part and zc_name are
			// non-null
			if (strcmp(zc_name, zc_part) != 0) {
				continue;
			}
		}
		// If we are here, it means the zone cluster check passed.
		// We have to check now whether the object types match.
		// If "obj_type" was 0, it means we have to check the resource
		// types, else we have to check for resource groups.
		if ((obj_type == 0) && (strcmp(obj_part, rs->r_type) == 0)) {
			type_found = B_TRUE;
			break;
		} else if ((obj_type == 1) &&
			(strcmp(obj_part, rs->r_rgname) == 0)) {
			type_found = B_TRUE;
			break;
		}
	}
	// Free memory
	if (zc_part) {
		free(zc_part);
	}
	if (obj_part) {
		free(obj_part);
	}
	return (clerrno);
}

int
match_group(ValueList &resourcegroups, rgm_resource_t *rs)
{
	int grp_found = 0;

	for (resourcegroups.start(); resourcegroups.end() != 1;
	    resourcegroups.next()) {
		if (strcmp(resourcegroups.getValue(), rs->r_rgname) == 0)
			grp_found = 1;
	}
	return (grp_found);
}

//
// See if the provided resource is online on any node in the nodelist
//
int
match_rs_node(ValueList &nodes, scstat_rs_t *rs)
{
	scstat_rs_status_t *rsstat;
	int node_found = 0;

	for (rsstat = rs->scstat_rs_status_list;  rsstat;
	    rsstat = rsstat->scstat_rs_status_by_node_next) {
		for (nodes.start(); nodes.end() != 1; nodes.next()) {
			if ((strcmp(nodes.getValue(), rsstat->scstat_node_name)
			    == 0) &&
			    (strcmp(rsstat->scstat_rs_state_str, "Online") ==
			    0)) {
				node_found = 1;
				break;
			}
		}
	}

	return (node_found);
}

//
// See if the provided resource is in any of the specified state on any node
//
int
match_rs_state(ValueList &states, scstat_rs_t *rs)
{
	scstat_rs_status_t *rsstat;
	int state_found = 0;
	int notonline = 1;
	int offline = 1;
	char *tmp_state;

	for (rsstat = rs->scstat_rs_status_list;  rsstat;
	    rsstat = rsstat->scstat_rs_status_by_node_next) {
		tmp_state = scstat_str_to_cl_str(rsstat->scstat_rs_state_str,
		    CL_TYPE_RS);
		for (states.start(); states.end() != 1; states.next()) {
			if (strcasecmp(states.getValue(), "offline") == 0)
				continue;

			if (strcasecmp(states.getValue(), tmp_state) == 0) {
				state_found = 1;
				break;
			}
		}
		if ((strcasecmp(tmp_state, "online") == 0) ||
		    (strcasecmp(tmp_state, "online_not_monitored") == 0))
			notonline = 0;
		if (strcasecmp(tmp_state, "offline") != 0)
			offline = 0;
	}

	if (state_found)
		return (state_found);

	for (states.start(); states.end() != 1; states.next()) {
		if (notonline && (strcasecmp(states.getValue(), "not_online")
		    == 0)) {
			state_found = 1;
			break;
		}
		if (offline && (strcasecmp(states.getValue(), "offline")
		    == 0)) {
			state_found = 1;
			break;
		}
	}
	return (state_found);
}

//
// For type rtname, check if all the properties make sense, and if they do,
// move them to either xproperties or yproperties.
//
clerrno_t
set_rs_props(char *rsname, char *rtname, NameValueList &properties,
    NameValueList &xproperties, NameValueList &yproperties, char *zc_name)
{
	int rv = 0;
	clerrno_t clerrno = CL_NOERR;
	NameValueList proplist;
	int errflg = 0;
	char *actual_name = NULL;

	clerrno = get_r_props(rtname, proplist, zc_name);
	if (clerrno)
		return (clerrno);

	// Check xlist and ylist
	for (xproperties.start(); !xproperties.end(); xproperties.next()) {
		rv = get_property_type(xproperties.getName(), proplist,
		    &errflg, &actual_name);
		if (errflg)
			return (errflg);
		if (rv != 1) {
			if (rsname)
				clerror("Unknown extension property \"%s\" "
				    "specified for resource \"%s\".\n",
				    actual_name, rsname);
			else
				clerror("Unknown extension property \"%s\" "
				    "specified.\n", actual_name);
			return (CL_EPROP);
		}
		if (actual_name)
			free(actual_name);
	}
	for (yproperties.start(); !yproperties.end(); yproperties.next()) {
		rv = get_property_type(yproperties.getName(), proplist,
		    &errflg, &actual_name);
		if (errflg)
			return (errflg);
		if ((rv != 0) && (rv != 2)) {
			clerror("Invalid standard property \"%s\" specified."
				    "\n", actual_name);
			return (CL_EPROP);
		}
		if (actual_name)
			free(actual_name);
	}

	// Move properties to either xlist or ylist
	for (properties.start(); !properties.end(); properties.next()) {
		rv = get_property_type(properties.getName(), proplist, &errflg,
		    &actual_name);
		if (errflg)
			return (errflg);
		if (rv == 0) {
			if (yproperties.add(properties.getName(),
			    properties.getValue())) {
				clerror("Property \"%s\" specified multiple "
				    "times.\n", actual_name);
				return (CL_EPROP);
			}
		} else if (rv == 1) {
			if (xproperties.add(properties.getName(),
			    properties.getValue())) {
				clerror("Property \"%s\" specified multiple "
				    "times.\n", actual_name);
				return (CL_EPROP);
			}
		} else if (rv == 2) {
			clerror("Use either the \"-x\" or the \"-y\" option "
			    "to indicate if specified property \"%s\" is "
			    "a standard or an extension property.\n",
			    actual_name);
			return (CL_EPROP);
		} else {
			if (rsname)
				clerror("Invalid property \"%s\" specified "
				    "for resource \"%s\".\n",
				    actual_name, rsname);
			else
				clerror("Invalid property \"%s\" specified.\n",
				    actual_name);
			return (CL_EPROP);
		}
		if (actual_name)
			free(actual_name);
	}
	return (CL_NOERR);
}

//
// Convert nodeid, zone pair to a logical nodename.
// Caller is reponsible for freeing space.
//
char *
nodeidzone_to_nodename(uint_t nodeid, char *zonename, int *errflg,
    char *zcname)
{
	char *nodename = NULL;
	char *tmp = NULL;
	scconf_errno_t scconf_status;
	clerrno_t clerrno = CL_NOERR;
	boolean_t iseuropa;

	*errflg = 0;

#if (SOL_VERSION >= __s10)
	// If zone cluster name was specified, then check for the
	// node-name in the ZC specific function.
	if (zcname) {
		return (zc_nodeid_to_nodename(nodeid, errflg, zcname));
	}
#endif

	// If we are here, then no zone-cluster was specified.
	clerrno = europa_open(&iseuropa);
	if (clerrno) {
		*errflg = clerrno;
		return (NULL);
	}

	scconf_status = scconf_get_nodename(nodeid, &nodename);
	if (iseuropa && scconf_status == SCCONF_ENOEXIST) {
		scconf_status = scconf_get_farmnodename(nodeid,
		    &nodename);
	}
	if (iseuropa)
		europa_close();

	if (scconf_status != SCCONF_NOERR) {
		clerror("Internal error: failed to get nodename for nodeid "
		    "\"%d\".\n", nodeid);
		*errflg = map_scconf_error(scconf_status);
		free(nodename);
		return (NULL);
	}
	if (zonename == NULL)
		return (nodename);

	tmp = (char *)realloc(nodename, strlen(nodename) + strlen(zonename)
	    + 2);
	if (tmp == NULL) {
		// nodename is initialized to NULL so suppress lint warning
		free(nodename);
		clcommand_perror(CL_ENOMEM);
		*errflg = CL_ENOMEM;
		return (NULL);
	}
	nodename = tmp;
	(void) strcat(nodename,  ":");
	(void) strcat(nodename, zonename);
	return (nodename);
}

#if (SOL_VERSION >= __s10)
//
// Convert nodeid to nodename for a zone cluster node.
// Caller is reponsible for freeing space.
// This method is more like a utility function. It just calls the
// respective scconf method. The reason for declaring it as a
// separate method is to invoke it from nodeidzone_to_nodename()
// and keep the zone cluster logic separate from it.
// This method assumes that the zone cluster specified in "zcname"
// is a valid zone cluster.
static char *
zc_nodeid_to_nodename(uint_t nodeid, int *errflg, char *zcname)
{

	scconf_errno_t scconf_err = SCCONF_NOERR;
	char *nodename = NULL;

	if (zcname == NULL) {
		*errflg = CL_EINVAL;
		return (NULL);
	}

	scconf_err = scconf_get_zc_nodename_by_nodeid(zcname,
				nodeid, &nodename);
	if (scconf_err != SCCONF_NOERR) {
		*errflg = map_scconf_error(scconf_err);
		free(nodename);
		return (NULL);
	}

	return (nodename);
}
#endif

namelist_t *
nodeidlist_to_namelist(nodeidlist_t *list, int *errflg, char *zc_name)
{
	nodeidlist_t *nidl;
	char *nodename = NULL;
	namelist_t *prev_ptr;
	namelist_t *nl_entry;
	namelist_t *first_ptr = NULL;
	int errflg1 = 0;
	bool gz_flag = true;
	boolean_t zc_flag = B_FALSE;
	char *cl_name = NULL;
	scconf_errno_t scconf_err = SCCONF_NOERR;

	*errflg = 0;

	if (list == NULL)
		return (NULL);

	// Check whether we are in the global zone.
#if (SOL_VERSION >= __s10)
	if (sc_nv_zonescheck()) {
		// We are inside a zone
		gz_flag = false;

		// Check whether we are in a zone cluster.
		scconf_err = scconf_zone_cluster_check(&zc_flag);

		if (scconf_err != SCCONF_NOERR) {
			*errflg = map_scconf_error(scconf_err);
			return (NULL);
		}
	}

	// if we are in the global zone and a zone cluster name
	// was specified, we have to call the appropriate function.
	// If we are in a zone cluster and a zone cluster name was
	// specified, we have to ensure that both the names are same.
	if (zc_name) {
		if (gz_flag && (strcmp(zc_name, GLOBAL_ZONE_NAME))) {
			// call the zc specific function here.
			return (zc_nodeidlist_to_namelist(list, errflg,
							zc_name));
		}

		if (zc_flag == B_TRUE) {
			// Fetch the cluster name
			cl_name = clconf_get_clustername();

			if (cl_name == NULL) {
				*errflg = CL_EINTERNAL;
				return (NULL);
			}

			if (strcmp(cl_name, zc_name)) {
				// You can see only the current
				// zone cluster information.
				free(cl_name);
				*errflg = CL_ENOENT;
				return (NULL);
			}
		}

	}

	// Free memory
	if (cl_name)
		free(cl_name);
#endif

	prev_ptr = NULL;
	for (nidl = list; nidl; nidl = nidl->nl_next) {

		nodename = nodeidzone_to_nodename(nidl->nl_nodeid,
		    nidl->nl_zonename, &errflg1);
		if (nodename == NULL) {
			*errflg = errflg1;
			return (NULL);
		}
		nl_entry = (namelist_t *)calloc(1, sizeof (namelist_t));
		if (nl_entry == NULL) {
			clcommand_perror(CL_ENOMEM);
			*errflg = CL_ENOMEM;
			return (NULL);
		}
		nl_entry->nl_name = strdup(nodename);
		if (nl_entry->nl_name == NULL) {
			clcommand_perror(CL_ENOMEM);
			*errflg = CL_ENOMEM;
			return (NULL);
		}
		nl_entry->nl_next = NULL;
		if (prev_ptr) {
			prev_ptr->nl_next = nl_entry;
			prev_ptr = nl_entry;
		} else {
			first_ptr = prev_ptr = nl_entry;
		}

		// Free memory
		free(nodename);
	}

	return (first_ptr);
}

#if (SOL_VERSION >= __s10)
// This method could be made public, but for now as we are calling this
// only from one place, we are using it like a utility method for
// nodeiflist_to_namelist(). This method assumes that the validity of
// the zone cluster being passed has already been verified.
static namelist_t *
zc_nodeidlist_to_namelist(nodeidlist_t *list, int *errflg, char *zc_name) {

	scconf_errno_t scconf_err = SCCONF_NOERR;
	namelist_t *first_ptr = NULL;
	nodeidlist_t *nidl;
	char *nodename = NULL;
	namelist_t *prev_ptr;
	namelist_t *nl_entry;

	prev_ptr = NULL;
	for (nidl = list; nidl; nidl = nidl->nl_next) {

		scconf_err = scconf_get_zc_nodename_by_nodeid(zc_name,
					nidl->nl_nodeid, &nodename);
		if (scconf_err != SCCONF_NOERR) {
			*errflg = map_scconf_error(scconf_err);
			return (NULL);
		}
		nl_entry = (namelist_t *)calloc(1, sizeof (namelist_t));
		if (nl_entry == NULL) {
			clcommand_perror(CL_ENOMEM);
			*errflg = CL_ENOMEM;
			return (NULL);
		}
		nl_entry->nl_name = strdup(nodename);
		if (nl_entry->nl_name == NULL) {
			clcommand_perror(CL_ENOMEM);
			*errflg = CL_ENOMEM;
			return (NULL);
		}
		nl_entry->nl_next = NULL;
		if (prev_ptr) {
			prev_ptr->nl_next = nl_entry;
			prev_ptr = nl_entry;
		} else {
			first_ptr = prev_ptr = nl_entry;
		}

		if (nodename) {
			free(nodename);
			nodename = NULL;
		}
	}

	return (first_ptr);
}

#endif
//
// This is an attempt to reduce code duplication, this handles enable/disable
// monitor and unmonitor where processing is very similar.
//
clerrno_t
clresource_process_resources(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, ValueList &resourcegroups, ValueList &resourcetypes,
    ValueList &nodes, cl_optype_t op_code)
{
	int first_err = CL_NOERR;
	int rv = 0;
	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};
	char **rg_names = NULL;
	char **rs_names = (char **)0;
	char **rsnames = (char **)0;
	char *oper1;
	clerrno_t errflg = CL_NOERR;
	clerrno_t clerrno = CL_NOERR;
	uint_t i, j, k;
	ValueList rslist;
	rgm_resource_t *rs = (rgm_resource_t *)NULL;
	char *r_rtname = (char *)NULL;
	ValueList rt_list;
	ValueList good_rgs;
	ValueList good_nodes, tmp_list;
	boolean_t recursive = B_FALSE;
	boolean_t verbose;
	char **rsnodes = (char **)0;
	scconf_errno_t scconf_err = SCCONF_NOERR;
	boolean_t is_same = B_FALSE;
	char *zc_name, *rs_part, *zc_part;

	if (optflgs & vflg)
		verbose = B_TRUE;
	else
		verbose = B_FALSE;

	if ((op_code != CL_OP_ENABLE) && (op_code != CL_OP_DISABLE) &&
	    (op_code != CL_OP_MONITOR) && (op_code != CL_OP_UNMONITOR)) {
		clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}

	// Initializa ORB and other cluster checks
	clerrno = cluster_init();
	if (clerrno)
		return (clerrno);

	if (optflgs & Rflg)
		recursive = B_TRUE;

	// Check whether any zone cluster names were specified.
	// And if they were specified, there should be only one zone cluster
	// across all the Rs.
	zc_name = NULL;
	rs_part = zc_part = NULL;
	clerrno = check_single_zc_specified(operands, &zc_name, &is_same);
	if (clerrno != CL_NOERR) {
		clcommand_perror(clerrno);
		return (clerrno);
	}
	// If multiple zone clusters were specified, report an error.
	if (is_same == B_FALSE) {
		clerror("This operation can be performed on only one "
		    "zone cluster at a time.\n");
		return (clerrno);
	}
#if (SOL_VERSION >= __s10)
	if (zc_name) {
		// If zone cluster name was specified, we have to ensure
		// that it is valid.
		// We have to check whether this operation is allowed for
		// the status of the zone cluster.
		clerrno = check_zc_operation(CL_OBJ_TYPE_RS, CL_OP_TYPE_ENABLE,
						zc_name);
		if (clerrno != CL_NOERR) {
			return (clerrno);
		}
	}
#endif

	tmp_list.add(zc_name);
	clerrno = get_good_nodes(nodes, good_nodes, tmp_list);
	tmp_list.clear();

	if (clerrno)
		return (clerrno);
	if (good_nodes.getSize() > 0) {
		rsnodes = vl_to_names(good_nodes, &errflg);
		if (errflg)
			return (errflg);
	}

	// Check resourcetypes
	for (resourcetypes.start(); !resourcetypes.end();
	    resourcetypes.next()) {
		if (r_rtname) {
			free(r_rtname);
			r_rtname = 0;
		}
		clerrno = get_r_rtname(resourcetypes.getValue(), &r_rtname,
					zc_name);
		if (clerrno == 0)
			rt_list.add(r_rtname);
		else {
			if (first_err == 0) {
				// first error!
				first_err = clerrno;
			}
		}
	}
	if (r_rtname)
		free(r_rtname);

	if ((resourcetypes.getSize() > 0) && (rt_list.getSize() == 0))
		return (first_err);

	if (optflgs & LHflg)
		rt_list.add(get_latest_rtname(SCRGADM_RTLH_NAME, zc_name));

	if (optflgs & SAflg)
		rt_list.add(get_latest_rtname(SCRGADM_RTSA_NAME, zc_name));

	for (resourcegroups.start(); !resourcegroups.end();
	    resourcegroups.next()) {
		clerrno = check_rg(resourcegroups.getValue(), zc_name);
		if (clerrno == 0)
			good_rgs.add(resourcegroups.getValue());
		else {
			if (first_err == 0) {
				// first error!
				first_err = clerrno;
			}
		}
	}
	if ((resourcegroups.getSize() > 0) && (good_rgs.getSize() == 0))
		return (first_err);

	operands.start();

	// Get the zone cluster name and the RG name
	scconf_err = scconf_parse_obj_name_from_cluster_scope(
		operands.getValue(), &oper1, &zc_name);

	// Return if there was an error.
	if (scconf_err != SCCONF_NOERR) {
		first_err = CL_EINTERNAL;
		return (first_err);
	}

	// Now check whether a wildcard was specified.
	if (strcmp(oper1, CLCOMMANDS_WILD_OPERAND) == 0) {
		if (good_rgs.getSize() > 0) {
			rg_names = vl_to_names(good_rgs, &errflg);
			if (errflg)
				return (errflg);
		} else {
			clerrno = z_getrglist(&rg_names, verbose,
			    optflgs & uflg, zc_name);
			if (clerrno != CL_NOERR) {
				return (clerrno);
			}
			// Remove the zone cluster context from all these
			// names.
			clerrno = remove_zc_names_from_names(rg_names);
			if (clerrno != CL_NOERR) {
				clcommand_perror(clerrno);
				return (clerrno);
			}
		}
	}

	if ((strcmp(oper1, CLCOMMANDS_WILD_OPERAND) == 0) &&
	    (rg_names != (char **)NULL)) {

		// Get the list of resources for this group
		for (i = 0; rg_names[i] != NULL; i++) {
			if (rs_names) {
				rgm_free_strarray(rs_names);
				rs_names = (char **)NULL;
			}
			scha_status = rgm_scrgadm_getrsrclist(rg_names[i],
			    &rs_names, zc_name);
			rv = scha_status.err_code;
			if (rv != SCHA_ERR_NOERR) {
				print_rgm_error(scha_status, NULL);
				clerror("Failed to obtain list of all "
				    "resources in resource group "
				    "\"%s\".\n", rg_names[i]);
				clerror("Will not try to change "
				    "resources belonging to group "
				    "\"%s\".\n", rg_names[i]);
				if (first_err == 0) {
					// first error!
					first_err = map_scha_error(rv);
				}
				continue;
			}
			if (rs_names == NULL)
				continue;

			for (j = 0; rs_names[j]; j++) {
				if (rs != NULL) {
					free(rs);
					rs = NULL;
				}
				scha_status = rgm_scrgadm_getrsrcconf(
				    rs_names[j], &rs, zc_name);
				rv = scha_status.err_code;
				if (rv != SCHA_ERR_NOERR) {
					print_rgm_error(scha_status,
					    rs_names[j]);
					clerror("Resource \"%s\" will not be "
					    "changed.\n", rs_names[j]);
					if (first_err == 0) {
						// first error!
						first_err = map_scha_error(rv);
					}
					continue;
				}
				if (rs == NULL) {
					clerror("Failed to access resource "
					    "\"%s\", resource will not be "
					    "changed.\n", rs_names[j]);
					if (first_err == 0) {
						// first error!
						first_err = CL_ENOENT;
					}
					continue;
				}
				if (rt_list.getSize() > 0)
					if (match_type(rt_list, rs) == 0)
						continue;

				rslist.add(rs_names[j]);
			}
		}
	} else if (strcmp(oper1, CLCOMMANDS_WILD_OPERAND) != 0) {
		for (operands.start(); operands.end() != 1; operands.next()) {

			if (rs != NULL) {
				free(rs);
				rs = NULL;
			}
			if (rs_part) {
				free(rs_part);
				rs_part = NULL;
			}
			if (zc_part) {
				free(zc_part);
				zc_part = NULL;
			}
			// First get the R name without the ZC name
			scconf_err = scconf_parse_obj_name_from_cluster_scope(
					operands.getValue(), &rs_part,
					&zc_part);

			if (scconf_err != SCCONF_NOERR) {
				clerror("Internal error.\n");
				if (first_err == CL_NOERR) {
					first_err = CL_EINTERNAL;
				}
				continue;
			}
			// Now get the resource conf
			scha_status = rgm_scrgadm_getrsrcconf(
			    rs_part, &rs, zc_part);
			rv = scha_status.err_code;
			if (rv != SCHA_ERR_NOERR) {
				print_rgm_error(scha_status,
				    operands.getValue());
				clerror("Resource \"%s\" will not be changed"
				    ".\n", operands.getValue());
				if (first_err == 0) {
					// first error!
					first_err = map_scha_error(rv);
				}
				continue;
			}
			if (rs == NULL) {
				clerror("Failed to access resource \"%s\", "
				    "resource will not be changed.\n",
				    operands.getValue());
				if (first_err == 0) {
					// first error!
					first_err = CL_ENOENT;
				}
				continue;
			}
			if (rt_list.getSize() > 0) {
				if (match_type(rt_list, rs) == 0) {
					if (optflgs & LHflg)
						clerror("Specified resource "
						    "\"%s\" is not a logical "
						    "hostname resource.\n",
						    operands.getValue());
					else if (optflgs & SAflg)
						clerror("Specified resource "
						    "\"%s\" is not a shared "
						    "address resource.\n",
						    operands.getValue());
					else
						clerror("Specified resource "
						    "\"%s\" does not meet the "
						    "criteria set by the "
						    "\"-t\" option.\n",
						    operands.getValue());
					if (first_err == 0) {
						// first error!
						first_err = CL_EINVAL;
					}
					continue;
				}
			}
			if (good_rgs.getSize() > 0) {
				if (match_group(good_rgs, rs) == 0) {
					clerror("Specified resource \"%s\" "
					    "does not meet the criteria set "
					    "by the \"-g\" option.\n",
					    operands.getValue());
					if (first_err == 0) {
						// first error!
						first_err = CL_EINVAL;
					}
					continue;
				}
			}
			rslist.add(rs_part);
		}
	}

	if (rslist.getSize() == 0)
		return (first_err);

	rsnames = vl_to_names(rslist, &errflg);
	if (errflg)
		return (errflg);

	// Change all resources in the list
	switch (op_code) {
	case CL_OP_ENABLE:
		scha_status = rgm_scswitch_set_resource_switch(
		    (const char **) rsnames, SCHA_SWITCH_ENABLED, B_FALSE,
		    recursive, (const char **)rsnodes, verbose, zc_name);
		break;
	case CL_OP_DISABLE:
		scha_status = rgm_scswitch_set_resource_switch(
		    (const char **) rsnames, SCHA_SWITCH_DISABLED, B_FALSE,
		    recursive, (const char **)rsnodes, verbose, zc_name);
		break;
	case CL_OP_MONITOR:
		scha_status = rgm_scswitch_set_resource_switch(
		    (const char **) rsnames, SCHA_SWITCH_ENABLED, B_TRUE,
		    B_FALSE, (const char **)rsnodes, verbose, zc_name);
		break;
	case CL_OP_UNMONITOR:
		scha_status = rgm_scswitch_set_resource_switch(
		    (const char **) rsnames, SCHA_SWITCH_DISABLED, B_TRUE,
		    B_FALSE, (const char **)rsnodes, verbose, zc_name);
		break;
	}

	rv = scha_status.err_code;
	if (rv != SCHA_ERR_NOERR) {
		print_rgm_error(scha_status, NULL);
		if (first_err == 0) {
			// first error!
			first_err = map_scha_error(rv);
		}
	} else if (scha_status.err_msg) {
		fprintf(stdout, "%s\n", scha_status.err_msg);
	}

	if (rg_names)
		rgm_free_strarray(rg_names);
	if (rs_names)
		rgm_free_strarray(rs_names);
	if (rs)
		free(rs);
	if (rsnodes)
		free(rsnodes);
	if (rs_part) {
		free(rs_part);
	}
	if (zc_part) {
		free(zc_part);
	}
	if (zc_name) {
		free(zc_name);
	}

	return (first_err);
}

//
// This is an attempt to reduce code duplication, this handled RG online
// and switch.
//
clerrno_t
clrg_process_switch(ClCommand &cmd, ValueList &operands, optflgs_t optflgs,
    ValueList &nodes, cl_optype_t op_code)
{
	clerrno_t first_err = CL_NOERR;
	clerrno_t clerrno = CL_NOERR;
	int rv = 0;
	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};
	char **rg_names = NULL;
	char **final_rg_names = NULL;
	int *failed_rgs = NULL;
	char **rs_names = (char **)0;
	char **all_rs_names = (char **)0;
	char **tmp_rs_names;
	char **rg_nodes = NULL;
	char *oper1;
	int errflg = 0;
	int num_rgs = 0;
	int rg_index = 0;
	uint_t num_rs_names = 0;
	uint_t i, j, k;
	ValueList good_nodes;
	ValueList good_rgs, tmp_list;
	boolean_t verbose;
	char *zc_name, *rg_part, *zc_part;
	scconf_errno_t scconf_err = SCCONF_NOERR;
	boolean_t is_same = B_FALSE;
	char *orig_rg_name = NULL;
	char *orig_rg_zc = NULL;

	if (optflgs & vflg)
		verbose = B_TRUE;
	else
		verbose = B_FALSE;

	if ((op_code != CL_OP_ONLINE) && (op_code != CL_OP_SWITCH)) {
		clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}

	// Initializa ORB and other cluster checks
	rv = cluster_init();
	if (rv)
		return (rv);

	// Check whether any zone cluster names were specified.
	// And if they were specified, there should be only zone cluster
	// across all the RGs.
	zc_name = NULL;
	rg_part = zc_part = NULL;
	clerrno = check_single_zc_specified(operands, &zc_name, &is_same);
	if (clerrno != CL_NOERR) {
		clcommand_perror(clerrno);
		return (clerrno);
	}
	// If multiple zone clusters were specified, report an error.
	if (is_same == B_FALSE) {
		clerror("This operation can be performed on only one "
		    "zone cluster at a time.\n");
		return (clerrno);
	}
#if (SOL_VERSION >= __s10)
	if (zc_name) {
		// If zone cluster name was specified, we have to ensure
		// that it is valid.
		// We have to check whether this operation is allowed for
		// the status of the zone cluster.
		clerrno = check_zc_operation(CL_OBJ_TYPE_RG, CL_OP_TYPE_ONLINE,
						zc_name);
		if (clerrno != CL_NOERR) {
			return (clerrno);
		}
	}
#endif

	tmp_list.add(zc_name);
	rv = get_good_nodes(nodes, good_nodes, tmp_list);
	tmp_list.clear();
	if (rv)
		return (rv);

	rg_nodes = vl_to_names(good_nodes, &errflg);
	if (errflg)
		return (errflg);

	operands.start();
	oper1 = operands.getValue();

	scconf_err = scconf_parse_obj_name_from_cluster_scope(
			oper1, &orig_rg_name, &orig_rg_zc);

	if (scconf_err) {
		clerror("Internal Error.\n");
		return (CL_EINTERNAL);
	}

	if (strcmp(orig_rg_name, CLCOMMANDS_WILD_OPERAND)) {
		for (operands.start(); !operands.end(); operands.next()) {

			if (rg_part) {
				free(rg_part);
				rg_part = NULL;
			}
			if (zc_part) {
				free(zc_part);
				zc_part = NULL;
			}
			// First split the RG name from cluster scope
			scconf_err = scconf_parse_obj_name_from_cluster_scope(
					operands.getValue(), &rg_part,
					&zc_part);
			if (scconf_err != SCCONF_NOERR) {
				clerrno = map_scconf_error(scconf_err);
				clcommand_perror(clerrno);
				if (first_err == CL_NOERR) {
					clerrno = first_err;
				}

				continue;
			}
			// Check whether this RG exists
			clerrno = check_rg(rg_part, zc_part);

			if (clerrno == CL_NOERR) {
				good_rgs.add(rg_part);
				continue;
			}

			// If we are here, it means this RG does not exist.
			clerror("Invalid resource group \"%s\" specified.\n",
				operands.getValue());
			if (first_err == CL_NOERR) {
					// first error!
					first_err = CL_EINVAL;
			}
		}
		// Free memory
		if (rg_part) {
			free(rg_part);
			rg_part = NULL;
		}
		if (zc_part) {
			free(zc_part);
			zc_part = NULL;
		}

		rg_names = vl_to_names(good_rgs, &errflg);
		if (errflg)
			return (errflg);

	} else {
		clerrno = z_getrglist(&rg_names, verbose, optflgs & uflg,
					zc_name);
		if (clerrno != CL_NOERR) {
			return (clerrno);
		}
		// Remove the zone cluster context from all these
		// names.
		clerrno = remove_zc_names_from_names(rg_names);
		if (clerrno != CL_NOERR) {
			clcommand_perror(clerrno);
			return (clerrno);
		}
	}

	if (!rg_names)
		goto cleanup;

	num_rgs = 0;
	for (i = 0; rg_names[i] != NULL; i++) {
		num_rgs++;
	}
	failed_rgs = (int *)calloc((size_t)num_rgs, sizeof (int));
	if (failed_rgs == NULL) {
		clcommand_perror(CL_ENOMEM);
		first_err = CL_ENOMEM;
		goto cleanup;
	}

	// If -e or -m flag is provided, create a list of all resources
	// belonging to the RGs and enable/monitor them.
	if ((optflgs & eflg) || (optflgs & mflg)) {
		for (i = 0; rg_names[i] != NULL; i++) {

			/* Get the list of resources for this group */
			scha_status = rgm_scrgadm_getrsrclist(rg_names[i],
			    &rs_names, zc_name);
			rv = scha_status.err_code;
			if (rv != SCHA_ERR_NOERR) {
				print_rgm_error(scha_status, NULL);
				clerror("Failed to obtain list of all "
				    "resources in resource group \"%s\".\n",
				    rg_names[i]);
				clerror("Will not try to switch resource "
				    "group \"%s\".\n", rg_names[i]);
				if (first_err == 0) {
					// first error!
					first_err = map_scha_error(rv);
				}
				failed_rgs[i] = 1;
				continue;
			}

			// If there are no resources, go on to the next RG
			if (rs_names == NULL || *rs_names == NULL)
				continue;

			// Count number of elements in rs_names
			for (j = 0; rs_names[j]; j++)
				;
			/* k indexes the current end of the array */
			k = num_rs_names;
			num_rs_names += j;

			//
			// If all_rs_names is NULL, set all_rs_names to
			// rs_names; otherwise, realloc and copy the names in.
			//
			if (all_rs_names == NULL) {
				all_rs_names = rs_names;
			} else {
				tmp_rs_names = (char **)realloc(all_rs_names,
				    (num_rs_names + 1) * sizeof (char *));
				if (tmp_rs_names == NULL) {
					clerror("No memory\n");
					return (CL_ENOMEM);
				}
				all_rs_names = tmp_rs_names;
				/* Concatenate rs_names onto all_rs_names */
				for (j = 0; rs_names[j]; j++, k++) {
					all_rs_names[k] = rs_names[j];
				}
				all_rs_names[num_rs_names] = NULL;
				free(rs_names);
				rs_names = NULL;
			}
		}

		if (all_rs_names != NULL) {
			if (optflgs & eflg) {
				// Enable all resources
				scha_status = rgm_scswitch_set_resource_switch(
				    (const char **)all_rs_names,
				    SCHA_SWITCH_ENABLED, B_FALSE, B_FALSE,
				    (const char **)NULL, verbose, zc_name);
				rv = scha_status.err_code;
				if (rv != SCHA_ERR_NOERR) {
					print_rgm_error(scha_status, NULL);
					if (first_err == 0) {
						// first error!
						first_err = map_scha_error(rv);
					}
					if (rg_names)
						rgm_free_strarray(rg_names);

					if (all_rs_names)
						rgm_free_strarray(all_rs_names);
					return (first_err);

				} else {
					if (scha_status.err_msg) {
						// dump any warning messages
						fprintf(stdout, "%s\n",
							scha_status.err_msg);
					}

					if (optflgs & vflg) {
					    clmessage("Enabled all resources "
						"belonging to the specified "
						"resource groups.\n");
					}
				}

			}
			if (optflgs & mflg) {
				// Enable monitoring on all resources
				scha_status = rgm_scswitch_set_resource_switch(
				    (const char **)all_rs_names,
				    SCHA_SWITCH_ENABLED, B_TRUE, B_FALSE,
				    (const char **)NULL, verbose, zc_name);
				rv = scha_status.err_code;
				if (rv != SCHA_ERR_NOERR) {
					print_rgm_error(scha_status, NULL);
					if (first_err == 0) {
						// first error!
						first_err = map_scha_error(rv);
					}
					if (rg_names)
						rgm_free_strarray(rg_names);

					if (all_rs_names)
						rgm_free_strarray(all_rs_names);
					return (first_err);

				} else {
					if (scha_status.err_msg) {
						// dump any warning messages
						fprintf(stdout, "%s\n",
						    scha_status.err_msg);
					}

					if (optflgs & vflg) {
					    clmessage("Set monitoring on all "
						"resources belonging to the "
						"specified resource groups.\n");
					}
				}
			}
		}
	}

	final_rg_names = (char **)calloc((size_t)(num_rgs + 1),
	    sizeof (char *));
	if (final_rg_names == NULL) {
		clerror("No memory\n");
		return (CL_ENOMEM);
	}
	rg_index = 0;
	for (i = 0; i < num_rgs; i++) {
		if (failed_rgs[i] == 0) {
			final_rg_names[rg_index++] = rg_names[i];
		}
	}
	final_rg_names[rg_index] = NULL;

	if (rg_index == 0)
		goto cleanup;

	// Manage the RGs if necessary
	if (optflgs & Mflg) {
		scha_status = rgm_scswitch_change_rg_state(
		    (const char **)final_rg_names, SCSWITCH_OFFLINE, verbose,
			zc_name);
		rv = scha_status.err_code;
		if (rv != SCHA_ERR_NOERR) {
			print_rgm_error(scha_status, NULL);
			if (first_err == 0) {
				// first error!
				first_err = map_scha_error(rv);
			}
		} else {
			if (scha_status.err_msg) {
				// dump any warning messages
				fprintf(stdout, "%s\n",
					scha_status.err_msg);
			}

			if (optflgs & vflg) {
			    clmessage("Set all the specified "
				"resource groups to managed state.\n");
			}
		}
	}

	// Bring online all RGs in the list
	if (rg_nodes != NULL) {
		if (op_code == CL_OP_SWITCH)
			scha_status = rgm_scswitch_switch_rg(
			    (const char **) rg_nodes,
			    (const char **) final_rg_names, RGACTION_NONE, 0,
			    verbose, zc_name);
		else if (op_code == CL_OP_ONLINE)
			scha_status = rgm_scswitch_switch_rg(
			    (const char **) rg_nodes,
			    (const char **) final_rg_names, RGACTION_ONLINE, 0,
			    verbose, zc_name);
	} else {
		scha_status = rgm_scswitch_switch_rg(NULL,
		    (const char **) final_rg_names, RGACTION_REBALANCE, 0,
		    verbose, zc_name);
	}
	rv = scha_status.err_code;
	if (rv != SCHA_ERR_NOERR) {
		print_rgm_error(scha_status, NULL);
		if (first_err == 0) {
			// first error!
			first_err = map_scha_error(rv);
		}
	} else if (scha_status.err_msg) {
		fprintf(stdout, "%s\n", scha_status.err_msg);
	}

cleanup:
	if (rg_names)
		rgm_free_strarray(rg_names);

	if (all_rs_names)
		rgm_free_strarray(all_rs_names);

	if (failed_rgs)
		free(failed_rgs);

	if (final_rg_names)
		free(final_rg_names);

	if (zc_name) {
		free(zc_name);
	}
	return (first_err);
}
//
// This is an attempt to reduce code duplication. This handles the RG suspend
// and resume operation.
//
clerrno_t
clrg_suspend_resume(ClCommand &cmd, ValueList &operands, optflgs_t optflgs,
    cl_optype_t op_code)
{
	clerrno_t first_err = CL_NOERR;
	int rv = 0;
	int i;
	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};
	char **rg_names = NULL;
	char *oper1;
	rgm_rg_t *rg = (rgm_rg_t *)0;
	scha_property_t *srprop;
	clerrno_t errflg = CL_NOERR;
	clerrno_t clerrno = CL_NOERR;
	int *failed_rgs = NULL;
	int num_rgs = 0;
	int rg_index = 0;
	char **final_rg_names = NULL;
	namelist_t *nl_elem;
	boolean_t verbose;
	char *zc_name, *rg_part, *zc_part;
	char *orig_rg_name, *orig_rg_zc;
	scconf_errno_t scconf_err = SCCONF_NOERR;
	boolean_t is_same = B_FALSE;
	ValueList good_rgs;

	if (optflgs & vflg)
		verbose = B_TRUE;
	else
		verbose = B_FALSE;

	if ((op_code != CL_OP_SUSPEND) && (op_code != CL_OP_RESUME)) {
		clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}

	// Initializa ORB and other cluster checks
	clerrno = cluster_init();
	if (clerrno) {
		return (clerrno);
	}

	// Check whether any zone cluster names were specified.
	// And if they were specified, there should be only zone cluster
	// across all the RGs.
	zc_name = NULL;
	rg_part = zc_part = NULL;
	clerrno = check_single_zc_specified(operands, &zc_name, &is_same);
	if (clerrno != CL_NOERR) {
		clcommand_perror(clerrno);
		return (clerrno);
	}
	// If multiple zone clusters were specified, report an error.
	if (is_same == B_FALSE) {
		clerror("This operation can be performed on only one "
		    "zone cluster at a time.\n");
		return (clerrno);
	}
#if (SOL_VERSION >= __s10)
	if (zc_name) {
		// If zone cluster name was specified, we have to ensure
		// that it is valid.
		// We have to check whether this operation is allowed for
		// the status of the zone cluster.
		clerrno = check_zc_operation(CL_OBJ_TYPE_RG, CL_OP_TYPE_RESUME,
						zc_name);
		if (clerrno != CL_NOERR) {
			return (clerrno);
		}
	}
#endif

	operands.start();
	oper1 = operands.getValue();

	orig_rg_name = (char *)0;
	orig_rg_zc = (char *)0;

	scconf_err = scconf_parse_obj_name_from_cluster_scope(
			oper1, &orig_rg_name, &orig_rg_zc);

	if (scconf_err) {
		clerror("Internal Error.\n");
		return (CL_EINTERNAL);
	}

	if (strcmp(orig_rg_name, CLCOMMANDS_WILD_OPERAND)) {
		for (operands.start(); !operands.end(); operands.next()) {

			if (rg_part) {
				free(rg_part);
				rg_part = NULL;
			}
			if (zc_part) {
				free(zc_part);
				zc_part = NULL;
			}
			// First split the RG name from cluster scope
			scconf_err = scconf_parse_obj_name_from_cluster_scope(
					operands.getValue(), &rg_part,
					&zc_part);
			if (scconf_err != SCCONF_NOERR) {
				clerrno = map_scconf_error(scconf_err);
				clcommand_perror(clerrno);
				continue;
			}
			// Check whether this RG exists
			clerrno = check_rg(rg_part, zc_part);

			if (clerrno == CL_NOERR) {
				good_rgs.add(rg_part);
				continue;
			}

			// If we are here, it means this RG does not exist.
			clerror("Invalid resource group \"%s\" specified.\n",
				operands.getValue());
			if (clerrno != CL_NOERR) {
				// first error!
				if (first_err == CL_NOERR) {
					first_err = CL_EINVAL;
				}
			}
		}
		// Free memory
		if (rg_part) {
			free(rg_part);
			rg_part = NULL;
		}
		if (zc_part) {
			free(zc_part);
			zc_part = NULL;
		}

		rg_names = vl_to_names(good_rgs, &errflg);
		if (errflg)
			return (errflg);

	} else {
		clerrno = z_getrglist(&rg_names, verbose, B_TRUE, zc_name);
		if (clerrno != CL_NOERR) {
			return (clerrno);
		}
		// Remove the zone cluster context from all these
		// names.
		clerrno = remove_zc_names_from_names(rg_names);
		if (clerrno != CL_NOERR) {
			clcommand_perror(clerrno);
			return (clerrno);
		}
	}

	if (!rg_names)
		return (first_err);

	num_rgs = 0;
	for (i = 0; rg_names[i] != NULL; i++) {
		num_rgs++;
	}
	failed_rgs = (int *)calloc((size_t)num_rgs, sizeof (int));
	if (failed_rgs == NULL) {
		clcommand_perror(CL_ENOMEM);
		return (CL_ENOMEM);
	}

	srprop = (scha_property_t *)calloc(2, sizeof (scha_property_t));
	nl_elem = (namelist_t *)calloc(1, sizeof (namelist_t));
	if ((srprop == NULL) || (nl_elem == NULL)) {
		clcommand_perror(CL_ENOMEM);
		return (CL_ENOMEM);
	}

	// Set the suspend/resume property
	srprop[0].sp_name = strdup("Suspend_automatic_recovery");
	if (op_code == CL_OP_SUSPEND)
		nl_elem->nl_name = strdup("TRUE");
	else
		nl_elem->nl_name = strdup("FALSE");

	if ((srprop[0].sp_name == NULL) || (nl_elem->nl_name == NULL)) {
		clcommand_perror(CL_ENOMEM);
		return (CL_ENOMEM);
	}

	nl_elem->nl_next = NULL;
	srprop[0].sp_value = nl_elem;
	srprop[1].sp_name = NULL;

	for (i = 0; rg_names[i] != NULL; i++) {

		// Get the RG details
		if (rg != (rgm_rg_t *)0) {
			rgm_free_rg(rg);
			rg = (rgm_rg_t *)0;
		}
		scha_status = rgm_scrgadm_getrgconf(rg_names[i], &rg, zc_name);
		rv = scha_status.err_code;
		if (rv != SCHA_ERR_NOERR) {
			print_rgm_error(scha_status, rg_names[i]);
			if (first_err == 0) {
				// first error!
				first_err = map_scha_error(rv);
			}
			rgm_free_rg(rg);
			failed_rgs[i] = 1;
			continue;
		}
		if (rg == NULL) {
			clerror("Failed to obtain resource group "
			    "information for \"%s\".\n", rg_names[i]);
			if (first_err == 0) {
				// first error!
				first_err = CL_ENOENT;
			}
			rgm_free_rg(rg);
			failed_rgs[i] = 1;
			continue;
		}

		scha_status = rgm_scrgadm_update_rg_property(rg_names[i],
		    (scha_property_t **)&srprop, FROM_SCSWITCH, NULL, zc_name);
		rv = filter_warning(scha_status.err_code);
		// dump any warning messages
		if ((scha_status.err_msg) && (!rv))
			printf("%s", scha_status.err_msg);

		if (rv) {
			failed_rgs[i] = 1;
			print_rgm_error(scha_status, rg_names[i]);
			if (first_err == 0) {
				// first error!
				first_err = map_scha_error(rv);
			}
		} else if (optflgs & vflg) {
			if (op_code == CL_OP_SUSPEND)
				clmessage("%s suspended.\n", rg_names[i]);
			else
				clmessage("%s resumed.\n", rg_names[i]);
		}
		rgm_free_rg(rg);
	}

	if (op_code == CL_OP_SUSPEND) {
		final_rg_names = (char **)calloc((size_t)(num_rgs + 1),
		    sizeof (char *));
		if (final_rg_names == NULL) {
			clerror("No memory\n");
			return (CL_ENOMEM);
		}
		rg_index = 0;
		for (i = 0; i < num_rgs; i++) {
			if (failed_rgs[i] == 0) {
				final_rg_names[rg_index++] = rg_names[i];
			}
		}
		final_rg_names[rg_index] = NULL;
		if (rg_index) {
			scha_status = rgm_scswitch_quiesce_rgs(
			    (const char **) final_rg_names,
			    (const char **) NULL,
			    optflgs & kflg, verbose, zc_name);
			rv = scha_status.err_code;
			if (rv) {
				print_rgm_error(scha_status, NULL);
				if (first_err == 0) {
					// first error!
					first_err = map_scha_error(rv);
				}
			} else if (scha_status.err_msg) {
				fprintf(stdout, "%s\n", scha_status.err_msg);
			}
		}
	}

	rgm_free_strarray(rg_names);
	if (failed_rgs)
		free(failed_rgs);
	if (final_rg_names)
		free(final_rg_names);

	good_rgs.clear();

	if (orig_rg_name)
		free(orig_rg_name);

	if (orig_rg_zc)
		free(orig_rg_zc);

	if (zc_name) {
		free(zc_name);
	}

	return (first_err);
}

void
get_all_rg_props(ValueList &proplist)
{
	proplist.add(SCHA_RG_DESCRIPTION);
	proplist.add(SCHA_PINGPONG_INTERVAL);
	proplist.add(SCHA_NODELIST);
	proplist.add(SCHA_MAXIMUM_PRIMARIES);
	proplist.add(SCHA_DESIRED_PRIMARIES);
	proplist.add(SCHA_FAILBACK);
	proplist.add(SCHA_RESOURCE_LIST);
	proplist.add(SCHA_RG_DEPENDENCIES);
	proplist.add(SCHA_GLOBAL_RESOURCES_USED);
	proplist.add(SCHA_IMPL_NET_DEPEND);
	proplist.add(SCHA_RG_PROJECT_NAME);
	proplist.add(SCHA_RG_AFFINITIES);
	proplist.add(SCHA_RG_AUTO_START);
	proplist.add(SCHA_RG_MODE);
	proplist.add(SCHA_PATHPREFIX);
	proplist.add(SCHA_RG_STATE);
	proplist.add(SCHA_RG_SYSTEM);
	proplist.add(SCHA_RG_SUSP_AUTO_RECOVERY);
	proplist.add(SCHA_RG_SLM_TYPE);
	proplist.add("RG_SLM_projectname");
	proplist.add(SCHA_RG_SLM_PSET_TYPE);
	proplist.add(SCHA_RG_SLM_CPU_SHARES);
	proplist.add(SCHA_RG_SLM_PSET_MIN);
}

//
// rdep_list_to_nlist
//
//	Return a namelist pointer for the list of resource dependency names.
//	The caller is responsible for freeing the namelist.
//
namelist_t *
rdep_list_to_nlist(rdeplist_t *r_dependencies, int *errflg)
{
	rdeplist_t *r_dep;
	size_t len = 0;
	char *string;
	const char *local_node = gettext("{local_node}");
	const char *any_node = gettext("{any_node}");
	namelist_t *first_ptr = (namelist_t *)NULL;
	namelist_t *prev_ptr = (namelist_t *)NULL;
	namelist_t *nl_entry = (namelist_t *)NULL;

	*errflg = 0;

	// Get the length required for the string buffer
	for (r_dep = r_dependencies; r_dep; r_dep = r_dep->rl_next) {
		len = 0;
		if (r_dep->rl_name && *r_dep->rl_name) {
			len += strlen(r_dep->rl_name) + 1;
		} else {
			continue;
		}

		switch (r_dep->locality_type) {
		case LOCAL_NODE:
			len += strlen(local_node);
			break;
		case ANY_NODE:
			len += strlen(any_node);
			break;
		case FROM_RG_AFFINITIES:
		default:
			break;
		}

		/* Allocate the string buffer */
		if ((string = (char *)calloc(1, len)) == NULL) {
			clcommand_perror(CL_ENOMEM);
			*errflg = CL_ENOMEM;
			return (NULL);
		}

		//
		// Add values to the string buffer. The string buffer contains
		// the list of suffixed qualifier with the dependencies name.
		//
		(void) strcat(string, r_dep->rl_name);

		switch (r_dep->locality_type) {
		case LOCAL_NODE:
			(void) strcat(string, local_node);
			break;
		case ANY_NODE:
			(void) strcat(string, any_node);
			break;
		case FROM_RG_AFFINITIES:
		default:
			break;
		}
		nl_entry = (namelist_t *)calloc(1, sizeof (namelist_t));
		if (nl_entry == NULL) {
			clcommand_perror(CL_ENOMEM);
			*errflg = CL_ENOMEM;
			return (NULL);
		}
		nl_entry->nl_name = strdup(string);
		if (nl_entry->nl_name == NULL) {
			clcommand_perror(CL_ENOMEM);
			*errflg = CL_ENOMEM;
			return (NULL);
		}
		nl_entry->nl_next = NULL;
		if (prev_ptr) {
			prev_ptr->nl_next = nl_entry;
			prev_ptr = nl_entry;
		} else {
			first_ptr = prev_ptr = nl_entry;
		}
	}

	return (first_ptr);
}

//
// Get property names and for RTs
//
void
get_all_rt_props(ValueList &proplist)
{
	proplist.add(SCHA_RT_DESCRIPTION);
	proplist.add(SCHA_RT_BASEDIR);
	proplist.add(SCHA_SINGLE_INSTANCE);
	proplist.add(SCHA_INIT_NODES);
	proplist.add(SCHA_INSTALLED_NODES);
	proplist.add(SCHA_PROXY);
	proplist.add(SCHA_FAILOVER);
	proplist.add(SCHA_API_VERSION);
	proplist.add(SCHA_RT_VERSION);
	proplist.add(SCHA_PKGLIST);
#if SOL_VERSION >= __s10
	proplist.add(SCHA_GLOBALZONE);
#endif
	proplist.add(SCHA_RT_SYSTEM);
}

clerrno_t
append_lh_props(char *rg_name, NameValueList &xproperties, ValueList &lhosts,
    ValueList &netiflist, ValueList &auxnodes, char *zc_name)
{
	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};
	int rv = 0;
	scrgadm_hostname_list_t *host_elem, *lhnames;
	scrgadm_hostname_list_t *head_hosts = (scrgadm_hostname_list_t *)NULL;
	scrgadm_hostname_list_t *tail_hosts = (scrgadm_hostname_list_t *)NULL;
	scrgadm_node_ipmp_list_t *ipmp_elem, *lipmp;
	scrgadm_node_ipmp_list_t *head_ipmp = (scrgadm_node_ipmp_list_t *)NULL;
	scrgadm_node_ipmp_list_t *tail_ipmp = (scrgadm_node_ipmp_list_t *)NULL;
	scrgadm_adapterinfo_list_t *ailist = NULL;
	namelist_t *laux = (namelist_t *)NULL;
	namelist_t *aux_elem;
	namelist_t *head_aux = (namelist_t *)NULL;
	namelist_t *tail_aux = (namelist_t *)NULL;
	char *netif_list = NULL;
	char *hostname_list = NULL;
	char *auxnode_list = NULL;
	boolean_t iseuropa;
	clerrno_t clerrno = CL_NOERR;

	clerrno = europa_open(&iseuropa);
	if (clerrno)
		return (clerrno);

	// Fill in the old scrgadm_hostname_list_t struct from lhosts
	for (lhosts.start(); lhosts.end() != 1; lhosts.next()) {
		host_elem = new scrgadm_hostname_list_t;
		host_elem->raw_name = strdup(lhosts.getValue());
		if (host_elem->raw_name == NULL) {
			clcommand_perror(CL_ENOMEM);
			clerrno = CL_ENOMEM;
			goto cleanup;
		}
		host_elem->next = NULL;
		host_elem->ip_addr = NULL;
		host_elem->ip6_addr = NULL;
		if (head_hosts == NULL) {
			head_hosts = host_elem;
		} else {
			tail_hosts->next = host_elem;
		}
		tail_hosts = host_elem;
	}
	lhnames = head_hosts;

	// Fill in the old scrgadm_node_ipmp_list_t struct from netiflist
	for (netiflist.start(); netiflist.end() != 1; netiflist.next()) {
		ipmp_elem = (scrgadm_node_ipmp_list_t *)
		    calloc(1, sizeof (scrgadm_node_ipmp_list_t));
		if (ipmp_elem == NULL) {
			clcommand_perror(CL_ENOMEM);
			clerrno = CL_ENOMEM;
			goto cleanup;
		}
		scha_status = load_node_ipmp_elt(ipmp_elem,
		    netiflist.getValue());
		rv = scha_status.err_code;
		if (rv != SCHA_ERR_NOERR) {
			print_rgm_error(scha_status, NULL);
			clerrno = map_scha_error(rv);
			goto cleanup;
		}
		ipmp_elem->next = NULL;
		if (head_ipmp == NULL) {
			head_ipmp = ipmp_elem;
		} else {
			tail_ipmp->next = ipmp_elem;
		}
		tail_ipmp = ipmp_elem;
	}
	lipmp = head_ipmp;

	// Fill in the auxnodelist too
	for (auxnodes.start(); auxnodes.end() != 1; auxnodes.next()) {
		aux_elem = new namelist_t;
		aux_elem->nl_name = strdup(auxnodes.getValue());
		if (aux_elem->nl_name == NULL) {
			clcommand_perror(CL_ENOMEM);
			clerrno = CL_ENOMEM;
			goto cleanup;
		}
		aux_elem->nl_next = NULL;
		if (head_aux == NULL) {
			head_aux = aux_elem;
		} else {
			tail_aux->nl_next = aux_elem;
		}
		tail_aux = aux_elem;
	}
	laux = head_aux;

	scha_status = process_netadapter_list(rg_name, lhnames,
				lipmp, &ailist, zc_name);
	rv = scha_status.err_code;
	if (rv != SCHA_ERR_NOERR) {
		print_rgm_error(scha_status, NULL);
		clerrno = map_scha_error(rv);
		goto cleanup;
	}

	scha_status = convert_adapterinfo_to_ipmp(ailist, &lipmp, lhnames,
						zc_name);
	rv = scha_status.err_code;
	if (rv != SCHA_ERR_NOERR) {
		print_rgm_error(scha_status, NULL);
		clerrno = map_scha_error(rv);
		goto cleanup;
	}

	// Construct comma list of hostname_list values
	scha_status = create_hnlist_val(lhnames, &hostname_list);
	rv = scha_status.err_code;
	if (rv != SCHA_ERR_NOERR) {
		print_rgm_error(scha_status, NULL);
		clerrno = map_scha_error(rv);
		goto cleanup;
	}

	scha_status = validate_unique_haip(lhnames);
	rv = scha_status.err_code;
	if (rv != SCHA_ERR_NOERR) {
		print_rgm_error(scha_status, NULL);
		clerrno = map_scha_error(rv);
		goto cleanup;
	}

	// Construct comma list of netif_list values
	scha_status = create_netiflist_val(lipmp, &netif_list);
	rv = scha_status.err_code;
	if (rv != SCHA_ERR_NOERR) {
		print_rgm_error(scha_status, NULL);
		clerrno = map_scha_error(rv);
		goto cleanup;
	}

	if (laux) {
		scha_status = process_namelist_to_nodeids(laux);
		rv = scha_status.err_code;
		if (rv != SCHA_ERR_NOERR) {
			print_rgm_error(scha_status, NULL);
			clerrno = map_scha_error(rv);
			goto cleanup;
		}
		scha_status = validate_auxnodelist(ailist, laux);
		rv = scha_status.err_code;
		if (rv != SCHA_ERR_NOERR) {
			print_rgm_error(scha_status, NULL);
			clerrno = map_scha_error(rv);
			goto cleanup;
		}
		scha_status = create_auxnodelist_val(laux, &auxnode_list);
		rv = scha_status.err_code;
		if (rv != SCHA_ERR_NOERR) {
			print_rgm_error(scha_status, NULL);
			clerrno = map_scha_error(rv);
			goto cleanup;
		}
	}

	if (xproperties.add(SCRGADM_HOSTNAMELIST, hostname_list) != 0) {
		clerror("You cannot specify the \"%s\" property "
		    "in this form of the command.\n", SCRGADM_HOSTNAMELIST);
		clerrno = CL_EINVAL;
		goto cleanup;
	}
	if (xproperties.add(SCRGADM_NETIFLIST, netif_list) != 0) {
		clerror("You cannot specify the \"%s\" property "
		    "in this form of the command.\n", SCRGADM_NETIFLIST);
		clerrno = CL_EINVAL;
		goto cleanup;
	}
	if (auxnode_list) {
		if (xproperties.add(SCRGADM_AUXNODELIST, auxnode_list) != 0) {
			clerror("You cannot specify the \"%s\" property "
			    "in this form of the command.\n",
			    SCRGADM_AUXNODELIST);
			clerrno = CL_EINVAL;
			goto cleanup;
		}
	}

cleanup:
	if (hostname_list)
		free(hostname_list);
	if (netif_list)
		free(netif_list);
	if (auxnode_list)
		free(auxnode_list);
	if (ailist)
		free_adapterinfo_list(ailist);
	if (laux)
		rgm_free_nlist(laux);
	if (iseuropa)
		europa_close();

	return (clerrno);
}

int
verify_yprops(NameValueList &yprops)
{
	for (yprops.start(); yprops.end() != 1; yprops.next()) {
		if (strchr(yprops.getName(), '{') ||
		    strchr(yprops.getName(), '}')) {
			clerror("Invalid characters in property specification "
			    "\"%s\".\n", yprops.getName());
			clerror("You can not specify per-node standard "
			    "properties.\n");
			return (CL_EPROP);
		}
	}
	return (CL_NOERR);
}

clerrno_t
process_props(NameValueList &props, NameValueList &pn_props, char *rt_name,
    char *zc_name)
{
	char *prop_name;
	char *node_str;
	char *node_elem;
	char *tmp_name;
	char *good_node = (char *)NULL;
	int rv;
	char *lasts = NULL;
	clerrno_t clerrno = CL_NOERR;
	boolean_t is_per_node_prop = B_FALSE;

	for (props.start(); props.end() != 1; props.next()) {
		if ((strchr(props.getName(), '{') == NULL) &&
		    (strchr(props.getName(), '}') == NULL)) {
			pn_props.add(props.getName(), props.getValue());
			continue;
		}
		if ((strchr(props.getName(), '{') != NULL) &&
		    (strchr(props.getName(), '}') == NULL)) {
			clerror("Invalid property specification \"%s\".\n",
			    props.getName());
			clerrno = CL_EPROP;
			goto cleanup;
		}
		if ((strchr(props.getName(), '}') != NULL) &&
		    (strchr(props.getName(), '{') == NULL)) {
			clerror("Invalid property specification \"%s\".\n",
			    props.getName());
			clerrno = CL_EPROP;
			goto cleanup;
		}
		prop_name = strtok(props.getName(), "{");
		if (prop_name == NULL) {
			clerror("Invalid property specification \"%s\".\n",
			    props.getName());
			clerrno = CL_EPROP;
			goto cleanup;
		}
		node_str = strtok(NULL, "}");
		if (node_str == NULL) {
			clerror("Invalid property specification \"%s\".\n",
			    props.getName());
			clerrno = CL_EPROP;
			goto cleanup;
		}
		clerrno = prop_is_per_node(prop_name, rt_name,
		    &is_per_node_prop, zc_name);
		if (clerrno != CL_NOERR)
			goto cleanup;

		if (is_per_node_prop == B_FALSE) {
			clerror("The property \"%s\" can not accept per-node "
			    "values.\n", prop_name);
			clerrno = CL_EPROP;
			goto cleanup;
		}

		node_elem = strtok_r(node_str, ",", &lasts);
		while (node_elem) {
			if (good_node) {
				free(good_node);
				good_node = NULL;
			}
			if (zc_name) {
#if SOL_VERSION >= __s10
				clerrno =
					get_good_node_for_zc(node_elem,
					&good_node, zc_name);
#endif
			} else {
				clerrno = get_good_node(node_elem, &good_node);
			}

			if (clerrno != 0)
				goto cleanup;
			tmp_name = (char *)calloc(1, strlen(prop_name) +
			    strlen(good_node) + 5);
			if (tmp_name == NULL) {
				clcommand_perror(CL_ENOMEM);
				clerrno = CL_ENOMEM;
				goto cleanup;
			}
			strcat(tmp_name, prop_name);
			strcat(tmp_name, "{");
			strcat(tmp_name, good_node);
			strcat(tmp_name, "}");
			if (pn_props.add(tmp_name, props.getValue())) {
				clerror("Property \"%s\" for \"%s\" specified "
				    "multiple times.\n", prop_name, good_node);
				clerrno = CL_EPROP;
				goto cleanup;
			}
			node_elem = strtok_r(NULL, ",", &lasts);
		}
	}

cleanup:
	if (good_node)
		free(good_node);
	return (clerrno);
}

//
// prop_is_per_node()
//
// Given a valid resource property name and a rs/rt name, figure out if
// this property can be set per-node.
//
clerrno_t
prop_is_per_node(char *prop_name, char *rt_name, boolean_t *is_per_node,
    char *zc_name)
{
	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};
	rgm_property_list_t *listp, *list;
	rgm_rt_t *rt = (rgm_rt_t *)0;
	rgm_param_t **table, **tablep;
	char *res_type = NULL;
	clerrno_t clerrno = CL_NOERR;

	if (rt_name == NULL) {
		clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}
	res_type = strdup(rt_name);
	if (res_type == NULL) {
		clerrno = CL_ENOMEM;
		clcommand_perror(CL_ENOMEM);
		goto cleanup;
	}
	scha_status = rgmcnfg_get_rtrealname(res_type, &rt_name, zc_name);
	if (scha_status.err_code != SCHA_ERR_NOERR) {
		clerrno = map_scha_error(scha_status.err_code);
		print_rgm_error(scha_status, NULL);
		goto cleanup;
	}
	scha_status = rgm_scrgadm_getrtconf(rt_name, &rt, zc_name);
	if (scha_status.err_code != SCHA_ERR_NOERR) {
		clerrno = map_scha_error(scha_status.err_code);
		print_rgm_error(scha_status, NULL);
		goto cleanup;
	}
	table = rt->rt_paramtable;
	for (tablep = table; *tablep && (*tablep)->p_name; ++tablep) {
		if (strcasecmp((*tablep)->p_name, prop_name) == 0) {
			*is_per_node = (*tablep)->p_per_node;
			break;
		}
	}
cleanup:
	if (rt)
		rgm_free_rt(rt);
	if (rt_name)
		free(rt_name);
	if (res_type)
		free(res_type);

	return (clerrno);
}

clerrno_t
remove_busy_rg_from_list(char **rgnames, boolean_t *list_changed)
{
	*list_changed = B_FALSE;
	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};
	char **rs_names = (char **)0;
	boolean_t enabled_state;
	clerrno_t clerrno = CL_NOERR;
	int i;
	int rv;
	rgm_rg_t *rg = (rgm_rg_t *)0;
	char *rg_part, *zc_part;
	scconf_errno_t scconf_err = SCCONF_NOERR;

	rg_part = NULL;
	zc_part = NULL;

	for (i = 0; rgnames[i]; i++) {
		if (rs_names != NULL) {
			rgm_free_strarray(rs_names);
			rs_names = (char **)NULL;
		}
		if (rg != (rgm_rg_t *)NULL) {
			rgm_free_rg(rg);
			rg = (rgm_rg_t *)NULL;
		}
		if (rg_part != NULL) {
			free(rg_part);
			rg_part = NULL;
		}
		if (zc_part != NULL) {
			free(zc_part);
			zc_part = NULL;
		}

		// Get the rg name and the zone cluster name.
		scconf_err = scconf_parse_obj_name_from_cluster_scope(
				rgnames[i], &rg_part, &zc_part);

		if (scconf_err != SCCONF_NOERR) {
			clerror("Internal error.\n");
			clerrno = CL_EINTERNAL;
			goto cleanup;
		}

		if (strcmp(rg_part, DELETED_RG) == 0)
			continue;
		scha_status = rgm_scrgadm_getrsrclist(rg_part, &rs_names,
							zc_part);
		rv = scha_status.err_code;
		if (rv != SCHA_ERR_NOERR) {
			print_rgm_error(scha_status, NULL);
			clerrno = map_scha_error(rv);
			goto cleanup;
		}
		if (rs_names != NULL) {
			clerror("Resource group \"%s\" contains resources, "
			    "will not try to delete it.\n", rgnames[i]);
			*list_changed = B_TRUE;
			free(rgnames[i]);
			rgnames[i] = (char *)calloc(strlen(DELETED_RG) + 3,
			    sizeof (char));
			if (rgnames[i] == NULL) {
				clcommand_perror(CL_ENOMEM);
				clerrno = CL_ENOMEM;
				goto cleanup;
			}
			sprintf(rgnames[i], DELETED_RG);
			continue;
		}
		// check for system RG here too
		scha_status = rgm_scrgadm_getrgconf(rg_part, &rg, zc_part);
		rv = scha_status.err_code;
		if (rv != SCHA_ERR_NOERR) {
			print_rgm_error(scha_status, NULL);
			clerrno = map_scha_error(rv);
			goto cleanup;
		}
		if (rg->rg_system == B_TRUE) {
			clerror("Resource group \"%s\" is a system resource "
			    "group, will not try to delete it.\n", rgnames[i]);
			*list_changed = B_TRUE;
			free(rgnames[i]);
			rgnames[i] = (char *)calloc(strlen(DELETED_RG) + 3,
			    sizeof (char));
			if (rgnames[i] == NULL) {
				clcommand_perror(CL_ENOMEM);
				clerrno = CL_ENOMEM;
				goto cleanup;
			}
			sprintf(rgnames[i], DELETED_RG);
		}
	}

cleanup:
	if (rs_names != NULL)
		rgm_free_strarray(rs_names);
	if (rg)
		rgm_free_rg(rg);
	return (clerrno);
}

clerrno_t
remove_enabled_rs_from_list(char **rsnames, boolean_t *list_changed)
{
	*list_changed = B_FALSE;
	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};
	rgm_resource_t *rs = (rgm_resource_t *)NULL;
	boolean_t enabled_state;
	clerrno_t errflg = CL_NOERR;
	int i;
	int rv;
	char *rs_name, *zc_name;
	scconf_errno_t scconf_err = SCCONF_NOERR;

	rs_name = zc_name = NULL;

	for (i = 0; rsnames[i]; i++) {
		if (rs != NULL) {
			rgm_free_resource(rs);
			rs = NULL;
		}
		if (rs_name) {
			free(rs_name);
			rs_name = NULL;
		}
		if (zc_name) {
			free(zc_name);
			zc_name = NULL;
		}

		scconf_err = scconf_parse_obj_name_from_cluster_scope(
				rsnames[i], &rs_name, &zc_name);

		if (scconf_err != SCCONF_NOERR) {
			clerror("Internal error.\n");
			return (CL_EINTERNAL);
		}

		if (strcmp(rs_name, DELETED_RS) == 0)
			continue;
		scha_status = rgm_scrgadm_getrsrcconf(rs_name, &rs, zc_name);
		rv = scha_status.err_code;
		if (rv != SCHA_ERR_NOERR) {
			print_rgm_error(scha_status, NULL);
			return (map_scha_error(rv));
		}
		if (rs == NULL) {
			clerror("Failed to obtain information on resource "
			    "\"%s\".\n", rsnames[i]);
			return (CL_ENOENT);
		}
		enabled_state = is_enabled(rs, &errflg, zc_name);
		if (errflg)
			return (errflg);
		if (enabled_state == B_TRUE) {
			*list_changed = B_TRUE;
			clerror("Resource \"%s\" is enabled, will not try to "
			    "delete it.\n", rsnames[i]);
			free(rsnames[i]);
			rsnames[i] = (char *)calloc(strlen(DELETED_RS) + 3,
			    sizeof (char));
			if (rsnames[i] == NULL) {
				clcommand_perror(CL_ENOMEM);
				return (CL_ENOMEM);
			}
			sprintf(rsnames[i], DELETED_RS);
		}
	}
	if (rs)
		rgm_free_resource(rs);
	if (rs_name) {
		free(rs_name);
	}
	if (zc_name) {
		free(zc_name);
	}
}

boolean_t
is_enabled(rgm_resource_t *rs, clerrno_t *errflg, char *zc_name)
{
	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};
	*errflg = CL_NOERR;
	rgm_rg_t *rg = (rgm_rg_t *)NULL;
	boolean_t res = B_FALSE;
	nodeidlist_t *nl;
	scha_switch_t swtch;
	char *nodename = (char *)NULL;
	int rv;
	int errflg1 = 0;

	scha_status = rgm_scrgadm_getrgconf(rs->r_rgname, &rg, zc_name);
	rv = scha_status.err_code;
	if (rv != SCHA_ERR_NOERR) {
		print_rgm_error(scha_status, NULL);
		clerror("Failed to obtain resource group configuration for "
		    "\"%s\".\n", rs->r_rgname);
		*errflg = CL_ENOENT;
		goto cleanup;
	}
	for (nl = rg->rg_nodelist; nl; nl = nl->nl_next) {
		nodename = nodeidzone_to_nodename(nl->nl_nodeid,
		    nl->nl_zonename, &errflg1, zc_name);
		if (errflg1) {
			*errflg = errflg1;
			goto cleanup;
		}
#if (SOL_VERSION >= __s10)
		scha_status = get_swtch_zc(rs->r_onoff_switch, nodename,
						&swtch, zc_name);
#else
		scha_status = get_swtch(rs->r_onoff_switch, nodename,
						&swtch);
#endif
		rv = scha_status.err_code;
		if (rv != SCHA_ERR_NOERR) {
			print_rgm_error(scha_status, NULL);
			*errflg = map_scha_error(rv);
			goto cleanup;
		}
		switch (swtch) {
		case SCHA_SWITCH_ENABLED:
			res = B_TRUE;
			goto cleanup;
		default:
			break;
		}
	}

cleanup:
	if (rg)
		rgm_free_rg(rg);
	if (nodename)
		free(nodename);
	return (res);
}

//
// Return true if origrs has any kind of dependency on destrs.
//
boolean_t
anydep_rs(char *origrs, char *destrs, clerrno_t *errflg, rdep_locality_t *loc,
    int *nru_supp)
{
	*errflg = CL_NOERR;
	rgm_resource_t *rs = (rgm_resource_t *)NULL;
	boolean_t res = B_FALSE;
	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};
	int rv;
	rdeplist_t *rdep;
	rgm_property_list_t *listp;
	rgm_property_t *rp;
	namelist_t *nl;
	char *rs_name, *zc_name;
	scconf_errno_t scconf_err = SCCONF_NOERR;

	*loc = FROM_RG_AFFINITIES;
	*nru_supp = 0;
	rs_name = zc_name = NULL;

	if ((origrs == (char *)NULL) || (destrs == (char *)NULL)) {
		clcommand_perror(CL_EINTERNAL);
		*errflg = CL_EINTERNAL;
		goto cleanup;
	}

	scconf_err = scconf_parse_obj_name_from_cluster_scope(origrs,
					&rs_name, &zc_name);

	if (scconf_err != SCCONF_NOERR) {
		*errflg = CL_EINTERNAL;
		goto cleanup;
	}

	scha_status = rgm_scrgadm_getrsrcconf(rs_name, &rs, zc_name);
	rv = scha_status.err_code;
	if (rv != SCHA_ERR_NOERR) {
		print_rgm_error(scha_status, NULL);
		*errflg = CL_ENOENT;
		goto cleanup;
	}
	if (rs == NULL) {
		clerror("Failed to access resource information.\n");
		*errflg = CL_ENOENT;
		goto cleanup;
	}
	for (listp = rs->r_properties; listp;  listp = listp->rpl_next) {
		rp = listp->rpl_property;
		if (rp->rp_key) {
			if (strcasecmp(SCHA_NETWORK_RESOURCES_USED,
			    rp->rp_key) == 0) {
				*nru_supp = 1;
				for (nl = rp->rp_array_values; nl;
				    nl = nl->nl_next) {
					if (strcmp(nl->nl_name, destrs) == 0) {
						res = B_TRUE;
						goto cleanup;
					}
				}
			}
		}
	}
	rdep = rs->r_dependencies.dp_strong;
	while (rdep) {
		if (strcmp(rdep->rl_name, destrs) == 0) {
			*loc = rdep->locality_type;
			res = B_TRUE;
			goto cleanup;
		}
		rdep = rdep->rl_next;
	}
	rdep = rs->r_dependencies.dp_weak;
	while (rdep) {
		if (strcmp(rdep->rl_name, destrs) == 0) {
			*loc = rdep->locality_type;
			res = B_TRUE;
			goto cleanup;
		}
		rdep = rdep->rl_next;
	}
	rdep = rs->r_dependencies.dp_restart;
	while (rdep) {
		if (strcmp(rdep->rl_name, destrs) == 0) {
			*loc = rdep->locality_type;
			res = B_TRUE;
			goto cleanup;
		}
		rdep = rdep->rl_next;
	}
	rdep = rs->r_dependencies.dp_offline_restart;
	while (rdep) {
		if (strcmp(rdep->rl_name, destrs) == 0) {
			*loc = rdep->locality_type;
			res = B_TRUE;
			goto cleanup;
		}
		rdep = rdep->rl_next;
	}
cleanup:
	if (rs)
		rgm_free_resource(rs);
	if (rs_name) {
		free(rs_name);
	}
	if (zc_name) {
		free(zc_name);
	}
	return (res);
}

//
// Return true if origrg has any reference to destrg.
// If "destrg" is not scoped with a cluster name, this function
// will assume the current cluster to be cluster scoping. The
// cluster context is very important for this method since it
// is possible to have inter cluster RG affinities.
//
boolean_t
anydep_rg(char *origrg, char *destrg, clerrno_t *errflg, char *dep_str,
    char *aff_str)
{
	*errflg = CL_NOERR;
	rgm_rg_t *rg = (rgm_rg_t *)NULL;
	boolean_t res = B_FALSE;
	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};
	int rv;
	rdeplist_t *rdep;
	char tmp_str[MAXCCRTBLNMLEN];
	int tmp_len;
	namelist_t *nl;
	int i;
	char *orig_rg_name, *orig_rg_zc;
	scconf_errno_t scconf_err = SCCONF_NOERR;
	char *cur_cluster_name, *full_dest_rg_name, *tmp_rg_name;
	boolean_t zc_flag = B_FALSE;

	// First get the rg name and the zone cluster name.
	orig_rg_name = (char *)NULL;
	orig_rg_zc = (char *)NULL;
	cur_cluster_name = full_dest_rg_name = tmp_rg_name = NULL;

	// Initialize the clconf library
	if (clconf_lib_init() != 0) {
		clerror("Cannot initialize clconf library.\n");
		*errflg = CL_EINTERNAL;
		goto cleanup;
	}

#if (SOL_VERSION >= __s10)
	// Check whether we are inside a zone cluster or not
	scconf_err = scconf_zone_cluster_check(&zc_flag);
	if (scconf_err != SCCONF_NOERR) {
		clerror("Internal error.\n");
		*errflg = CL_EINTERNAL;
		goto cleanup;
	}
#endif
	// First fetch the cluster name
	if (zc_flag) {
		// We are in a zone cluster
		cur_cluster_name = clconf_get_clustername();
		if (!cur_cluster_name) {
			clerror("Cannot get the cluster name.\n");
			*errflg = CL_EINTERNAL;
			goto cleanup;
		}
	} else {
		// If we are not in a zone cluster, we should be in
		// the global zone
		cur_cluster_name = strdup(GLOBAL_CLUSTER_NAME);
	}

	// If cluster scoping was not provided, we need to pre-pend
	// the RG name with cluster name
	if (!strchr(destrg, ZC_AND_OBJ_NAME_SEPARATER_CHAR)) {
		scconf_err = scconf_add_cluster_scope_to_obj_name(destrg,
				cur_cluster_name, &full_dest_rg_name);
		if (scconf_err) {
			clerror("Internal Error.\n");
			*errflg = CL_EINTERNAL;
			goto cleanup;
		}
	} else {
		full_dest_rg_name = strdup(destrg);
	}

	scconf_err = scconf_parse_obj_name_from_cluster_scope(
			origrg, &orig_rg_name, &orig_rg_zc);

	if (scconf_err) {
		clerror("Internal Error.\n");
		*errflg = CL_EINTERNAL;
		goto cleanup;
	}

	scha_status = rgm_scrgadm_getrgconf(orig_rg_name, &rg, orig_rg_zc);
	rv = scha_status.err_code;
	if (rv != SCHA_ERR_NOERR) {
		print_rgm_error(scha_status, NULL);
		*errflg = CL_ENOENT;
		goto cleanup;
	}
	if (rg == NULL) {
		clerror("Failed to access resource group information.\n");
		*errflg = CL_ENOENT;
		goto cleanup;
	}

	// RG dependencies can be set only within a cluster. So, there is no
	// need to check for cluster scoping when checking for dependencies.
	nl = rg->rg_dependencies;
	while (nl) {
		if (strcmp(nl->nl_name, destrg) == 0) {
			strcpy(dep_str, nl->nl_name);
			res = B_TRUE;
			break;
		}
		nl = nl->nl_next;
	}

	// RG affinities can be set across clusters. So, we need to be careful
	// while checking for RG affinities. We have to take into considaration
	// the cluster scoping while checking them.
	nl = rg->rg_affinities;
	while (nl) {
		tmp_rg_name = full_dest_rg_name;

		// Now, check for the affinities.
		if (strcmp(nl->nl_name, tmp_rg_name) == 0) {
			res = B_TRUE;
			strcpy(aff_str, nl->nl_name);
			goto cleanup;
		}
		strcpy(tmp_str, nl->nl_name);
		tmp_len = strlen(tmp_str);
		for (i = 0; i < tmp_len; i++) {
			if ((tmp_str[i] != '+') && (tmp_str[i] != '-'))
				break;
		}
		if (strcmp(tmp_str + i, tmp_rg_name) == 0) {
			res = B_TRUE;
			strcpy(aff_str, nl->nl_name);
			goto cleanup;
		}
		nl = nl->nl_next;
	}
cleanup:
	if (rg)
		rgm_free_rg(rg);
	if (orig_rg_name)
		free(orig_rg_name);
	if (orig_rg_zc)
		free(orig_rg_zc);
	if (cur_cluster_name)
		free(cur_cluster_name);
	if (full_dest_rg_name)
		free(full_dest_rg_name);

	return (res);
}

//
// If any rg in allrg has dependency on one in rgnames and iorig_rg_zct
// a part of rgnames, wipe out that rg from the rgnames list. If
// this happens, set the list_changed flag and print an error message.
clerrno_t
remove_rg_referred_from_list(char **rgnames, ValueList &allrg,
    boolean_t *list_changed)
{
	boolean_t internal_rg;
	int i;
	boolean_t res;
	clerrno_t errflg = CL_NOERR;
	char aff_str[MAXCCRTBLNMLEN];
	char dep_str[MAXCCRTBLNMLEN];

	for (allrg.start(); allrg.end() != 1; allrg.next()) {
		internal_rg = B_FALSE;
		for (i = 0; rgnames[i]; i++) {
			if (strcmp(rgnames[i], allrg.getValue()) == 0) {
				internal_rg = B_TRUE;
				break;
			}
		}
		if (internal_rg)
			continue;
		for (i = 0; rgnames[i]; i++) {
			if (strcmp(rgnames[i], DELETED_RG) == 0)
				continue;
			res = anydep_rg(allrg.getValue(), rgnames[i], &errflg,
			    dep_str, aff_str);
			if (errflg) {
				return (errflg);
			}
			if (res) {
				clerror("Will not attempt to delete \"%s\" "
				    "as another resource group in the system "
				    "has a reference to it.\n",
				    rgnames[i]);
				rgnames[i] = strdup(DELETED_RG);
				if (rgnames[i] == NULL) {
					clcommand_perror(CL_ENOMEM);
					return (CL_ENOMEM);
				}
				*list_changed = B_TRUE;
			}
		}
	}
	return (CL_NOERR);
}

//
// If any resource in allrs has dependency on one in rsnames and is not
// a part of rsnames, wipe out that resource from the rsnames list. If
// this happens, set the list_changed flag and print an error message.
clerrno_t
remove_rs_dependees_from_list(char **rsnames, ValueList &allrs,
    boolean_t *list_changed)
{
	boolean_t internal_rs;
	int i;
	boolean_t res;
	clerrno_t errflg = CL_NOERR;
	rdep_locality_t loc;
	int nru_supp = 0;

	for (allrs.start(); allrs.end() != 1; allrs.next()) {
		internal_rs = B_FALSE;
		for (i = 0; rsnames[i]; i++) {
			if (strcmp(rsnames[i], allrs.getValue()) == 0) {
				internal_rs = B_TRUE;
				break;
			}
		}
		if (internal_rs)
			continue;
		for (i = 0; rsnames[i]; i++) {
			if (strcmp(rsnames[i], DELETED_RS) == 0)
				continue;
			res = anydep_rs(allrs.getValue(), rsnames[i], &errflg,
			    &loc, &nru_supp);
			if (errflg) {
				return (errflg);
			}
			if (res) {
				clerror("Will not attempt to delete \"%s\" "
				    "as another resource in the system has "
				    "dependency on it.\n",
				    rsnames[i]);
				rsnames[i] = strdup(DELETED_RS);
				if (rsnames[i] == NULL) {
					clcommand_perror(CL_ENOMEM);
					return (CL_ENOMEM);
				}
				*list_changed = B_TRUE;
			}
		}
	}
	return (CL_NOERR);
}

clerrno_t
eliminate_sysprop(char **rgnames, boolean_t verbose)
{
	clerrno_t first_err = CL_NOERR;
	int rv;
	int i;
	scha_property_t *srprop;
	namelist_t *nl_elem = (namelist_t *)NULL;
	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};
	boolean_t sysrg;
	clerrno_t clerrno = CL_NOERR;
	char *rg_name, *zc_name;
	scconf_errno_t scconf_err = SCCONF_NOERR;

	// Reset the system property
	srprop = (scha_property_t *)calloc(2, sizeof (scha_property_t));
	nl_elem = (namelist_t *)calloc(1, sizeof (namelist_t));
	if ((srprop == NULL) || (nl_elem == NULL)) {
		clcommand_perror(CL_ENOMEM);
		return (CL_ENOMEM);
	}
	srprop[0].sp_name = strdup("RG_system");
	nl_elem->nl_name = strdup("False");

	if ((srprop[0].sp_name == NULL) || (nl_elem->nl_name == NULL)) {
		clcommand_perror(CL_ENOMEM);
		first_err = CL_ENOMEM;
		goto cleanup;
	}

	nl_elem->nl_next = NULL;
	srprop[0].sp_value = nl_elem;
	srprop[1].sp_name = NULL;
	rg_name = zc_name = NULL;

	for (i = 0; rgnames[i] != NULL; i++) {

		if (rg_name) {
			free(rg_name);
			rg_name = NULL;
		}
		if (zc_name) {
			free(zc_name);
			zc_name = NULL;
		}

		// First get the RG name and the zone cluster name.
		scconf_err = scconf_parse_obj_name_from_cluster_scope(
					rgnames[i], &rg_name, &zc_name);
		if (scconf_err != SCCONF_NOERR) {
			if (first_err == CL_NOERR) {
				first_err = CL_EINTERNAL;
			}
			continue;
		}

		if (strcmp(rg_name, DELETED_RG) == 0)
			continue;

		// Check if this RG is system RG
		sysrg = B_FALSE;
		clerrno = check_system_rg(rgnames[i], &sysrg);
		if (first_err == 0) {
			// first error!
			first_err = clerrno;
		}
		if (sysrg == B_FALSE) {
			continue;
		}
		scha_status = rgm_scrgadm_update_rg_prop_no_validate(rg_name,
		    (scha_property_t **)&srprop, 0, zc_name);
		rv = filter_warning(scha_status.err_code);
		if (rv != SCHA_ERR_NOERR) {
			print_rgm_error(scha_status, NULL);
			if (first_err == 0) {
				// first error!
				first_err = map_scha_error(rv);
			}
		} else {
			if (scha_status.err_msg) {
				fprintf(stdout, "%s\n", scha_status.err_msg);
			}
			if (verbose) {
				clmessage("Reset system property for "
				    "resource group \"%s\".\n",
				    rgnames[i]);
			}
		}
	}
cleanup:
	if (srprop)
		free_prop_array(srprop);
	if (rg_name) {
		free(rg_name);
	}
	if (zc_name) {
		free(zc_name);
	}
	return (first_err);
}

//
// Reset the system property of the RG of the specified RS.
//
clerrno_t
eliminate_rg_sysprop(char *rsname, boolean_t verbose)
{
	char *rg_nosys;
	char *rglist_nosys[2];
	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};
	rgm_resource_t *rs = (rgm_resource_t *)NULL;
	char *rs_part, *zc_part;
	scconf_errno_t scconf_err = SCCONF_NOERR;
	int str_len;

	rs_part = zc_part = NULL;
	scconf_err = scconf_parse_obj_name_from_cluster_scope(rsname,
						&rs_part, &zc_part);

	if (scconf_err != SCCONF_NOERR) {
		return (CL_EINTERNAL);
	}

	scha_status = rgm_scrgadm_getrsrcconf(rs_part, &rs, zc_part);
	if ((scha_status.err_code != SCHA_ERR_NOERR) || (rs == NULL)) {
		clerror("Failed to access resource \"%s\".\n", rsname);
		return (CL_ENOENT);
	}

	str_len = strlen(rs->r_rgname);
	if (zc_part) {
		str_len += strlen(zc_part) + 1; // 1 for ':'
	}

	str_len += 1; // Fir NULL
	rg_nosys = (char *)calloc(str_len, 1);
	if (zc_part) {
		strcat(rg_nosys, zc_part);
		strcat(rg_nosys, ":");
	}

	strcat(rg_nosys, rs->r_rgname);
	rgm_free_resource(rs);
	rglist_nosys[0] = rg_nosys;
	rglist_nosys[1] = NULL;
	return (eliminate_sysprop(rglist_nosys, verbose));
}

//
// Remove any dependency/affinity properties pointing to rgs in rgnames,
// from rgs in allrg. If allrg is null, remove internal affinities/
// dependencies within rgnames.
//
clerrno_t
eliminate_rg_references(char **rgnames, ValueList &allrg, boolean_t force,
    boolean_t verbose)
{
	scha_property_t *proplist = NULL;
	clerrno_t first_err = CL_NOERR;
	clerrno_t clerrno = CL_NOERR;
	int rv = 0;
	int i;
	int j;
	clerrno_t errflg = CL_NOERR;
	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};
	boolean_t internal_rg = B_FALSE;
	boolean_t res = B_FALSE;
	char aff_str[MAXCCRTBLNMLEN];
	char dep_str[MAXCCRTBLNMLEN];
	NameValueList properties;
	char *rglist_nosys[2];
	char rg_nosys[MAXCCRTBLNMLEN];
	boolean_t int_dep = B_FALSE;
	scconf_errno_t scconf_err = SCCONF_NOERR;
	char *rg_name, *zc_name;

	rg_name = zc_name = NULL;

	if (allrg.getSize() == 0) {
		// Just remove dependencies/affinities of the rgs in rgnames
		properties.add("RG_dependencies", "");
		properties.add("RG_affinities", "");
		errflg = 0;
		proplist = nvl_to_prop_array(properties, NULL, &errflg,
		    CL_TYPE_RG, (char *)NULL);
		if (errflg) {
			first_err = errflg;
			goto cleanup;
		}
		for (i = 0; rgnames[i]; i++) {

			if (rg_name) {
				free(rg_name);
				rg_name = NULL;
			}
			if (zc_name) {
				free(zc_name);
				zc_name = NULL;
			}

			if (strcmp(rgnames[i], DELETED_RG) == 0)
				continue;

			// Parse the RG name from the ZC name
			scconf_err = scconf_parse_obj_name_from_cluster_scope(
						rgnames[i], &rg_name, &zc_name);
			if (scconf_err != SCCONF_NOERR) {
				errflg = map_scconf_error(scconf_err);
				clcommand_perror(errflg);
				if (first_err == CL_NOERR) {
					first_err = errflg;
				}
				if (!force) {
					goto cleanup;
				}
				continue;
			}

			//
			// Reset references only if this resource has
			// some reference to another in the operand set.
			//
			int_dep = B_FALSE;
			for (j = 0; rgnames[j]; j++) {
				res = anydep_rg(rgnames[i], rgnames[j],
				    &errflg, dep_str, aff_str);
				if (errflg) {
					first_err = errflg;
					goto cleanup;
				}
				if (res) {
					int_dep = B_TRUE;
					break;
				}
			}
			if (!int_dep)
				continue;

			scha_status = rgm_scrgadm_update_rg_prop_no_validate(
			    rg_name, &proplist, 0, zc_name);
			rv = filter_warning(scha_status.err_code);
			if (rv) {
				print_rgm_error(scha_status, NULL);
				clerror("Failed to eliminate dependencies or "
				    "affinities, try to delete one resource "
				    "group at a time.\n");
				clerror("One or more specified resource groups "
				    "may have been modified.\n");
				if (first_err == 0) {
					// first error!
					first_err = map_scha_error(rv);
				}
				if (!force)
					goto cleanup;
			} else {
				if (scha_status.err_msg) {
					fprintf(stdout, "%s\n",
					    scha_status.err_msg);
				}
				if (verbose) {
					clmessage("Eliminated any "
					    "resource "
					    "group dependencies or affinities "
					    "of \"%s\".\n", rgnames[i]);
				}
			}
		}

	} else {
		// Eliminate dependencies from rgs in allrg on rgnames
		for (allrg.start(); allrg.end() != 1; allrg.next()) {
			internal_rg = B_FALSE;
			if (rg_name) {
				free(rg_name);
				rg_name = NULL;
			}
			if (zc_name) {
				free(zc_name);
				zc_name = NULL;
			}
			// Parse the RG name from the ZC name
			scconf_err = scconf_parse_obj_name_from_cluster_scope(
					allrg.getValue(), &rg_name, &zc_name);
			if (scconf_err != SCCONF_NOERR) {
				errflg = map_scconf_error(scconf_err);
				clcommand_perror(errflg);
				if (first_err == CL_NOERR) {
					first_err = errflg;
				}
				if (!force) {
					goto cleanup;
				}
				continue;
			}
			// Finished parsing the RG name

			for (i = 0; rgnames[i]; i++) {
				if (strcmp(rgnames[i], allrg.getValue()) == 0) {
					internal_rg = B_TRUE;
					break;
				}
			}
			if (internal_rg)
				continue;
			for (i = 0; rgnames[i]; i++) {

				strcpy(dep_str, "");
				strcpy(aff_str, "");
				res = anydep_rg(allrg.getValue(), rgnames[i],
				    &errflg, dep_str, aff_str);
				if (errflg) {
					first_err = errflg;
					goto cleanup;
				}
				if (res) {
					sprintf(rg_nosys, allrg.getValue());
					rglist_nosys[0] = rg_nosys;
					rglist_nosys[1] = NULL;
					clerrno = eliminate_sysprop(
					    rglist_nosys, verbose);
					if (first_err == 0) {
						// first error!
						first_err = clerrno;
					}

					if (strlen(dep_str) > 0)
						properties.add(
						    "RG_dependencies-",
						    dep_str);
					if (strlen(aff_str) > 0)
						properties.add(
						    "RG_affinities-", aff_str);
					errflg = 0;
					if (proplist) {
						free_prop_array(proplist);
						proplist = NULL;
					}
					proplist = nvl_to_prop_array(
					    properties, allrg.getValue(),
					    &errflg, CL_TYPE_RG, (char *)NULL);
					if (errflg) {
						first_err = errflg;
						goto cleanup;
					}
					scha_status =
rgm_scrgadm_update_rg_prop_no_validate(rg_name, &proplist, 0, zc_name);
					rv = filter_warning(
					    scha_status.err_code);
					if (rv) {
						print_rgm_error(scha_status,
						    NULL);
						clerror("Failed to eliminate "
						    "dependencies or "
						    "affinities "
						    "from \"%s\" on \"%s\".\n",
						    allrg.getValue(),
						    rgnames[i]);
						if (first_err == 0) {
							// first error!
							first_err =
							    map_scha_error(rv);
						}
					} else {
						if (scha_status.err_msg) {
							fprintf(stdout, "%s\n",
							    scha_status.
							    err_msg);
						}
						if (verbose) {
							fprintf(stdout,
							    "Eliminated "
							    "dependencies or "
							    "affinities "
							    "of \"%s\" "
							    "referencing "
							    "\"%s\".\n",
							    allrg.getValue(),
							    rgnames[i]);
						}
					}
				}
			}
		}
	}
cleanup:
	if (proplist)
		free_prop_array(proplist);
	if (rg_name) {
		free(rg_name);
	}
	if (zc_name) {
		free(zc_name);
	}
	return (first_err);
}

//
// Remove any dependency properties pointing to resources in rsnames,
// from resources in allrsnames. If allrsnames is null, remove internal
// dependencies within rsnames.
// A zone cluster context is understood from the naming format
// <zone cluster name>:<rs name>, by this function.
//
clerrno_t
eliminate_rs_dependencies(char **rsnames, ValueList &allrs, boolean_t force,
    boolean_t verbose)
{
	scha_property_t *xproplist = NULL;
	scha_property_t *yproplist = NULL;
	NameValueList yproperties;
	NameValueList xproperties;
	clerrno_t first_err = CL_NOERR;
	scha_resource_properties_t rsprops;
	int rv = 0;
	int i;
	int j;
	int errflg;
	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};
	boolean_t internal_rs = B_FALSE;
	boolean_t res = B_FALSE;
	boolean_t int_dep = B_FALSE;
	rdep_locality_t loc;
	char dep_str[MAXCCRTBLNMLEN];
	clerrno_t clerrno = CL_NOERR;
	int nru_supp = 0;
	char *rs_name, *zc_name;
	scconf_errno_t scconf_err = SCCONF_NOERR;

	rs_name = zc_name = NULL;

	if (allrs.getSize() == 0) {
		for (i = 0; rsnames[i]; i++) {

			if (rs_name) {
				free(rs_name);
				rs_name = NULL;
			}
			if (zc_name) {
				free(zc_name);
				zc_name = NULL;
			}

			// First get the RS name and the zone cluster name
			scconf_err = scconf_parse_obj_name_from_cluster_scope(
					rsnames[i], &rs_name, &zc_name);

			if (scconf_err != SCCONF_NOERR) {

				if (first_err == CL_NOERR) {
					first_err = CL_EINTERNAL;
				}

				continue;
			}

			if (strcmp(rs_name, DELETED_RS) == 0)
				continue;

			//
			// Reset dependencies only if this resource has
			// some dependency on another in the operand set.
			//
			int_dep = B_FALSE;
			for (j = 0; rsnames[j]; j++) {
				res = anydep_rs(rsnames[i], rsnames[j],
				    &errflg, &loc, &nru_supp);
				if (errflg) {
					first_err = errflg;
					goto cleanup;
				}
				if (res) {
					int_dep = B_TRUE;
					break;
				}
			}
			if (!int_dep)
				continue;

			// Just remove dependencies of the resources in rsnames
			yproperties.clear();
			xproperties.clear();
			if (xproplist) {
				free_prop_array(xproplist);
				xproplist = NULL;
			}
			if (yproplist) {
				free_prop_array(yproplist);
				yproplist = NULL;
			}
			yproperties.add("Resource_dependencies_restart", "");
			yproperties.add("Resource_dependencies_weak", "");
			yproperties.add("Resource_dependencies", "");
			yproperties.add(
			    "Resource_dependencies_offline_restart", "");
			if (nru_supp) {
				yproperties.add("network_resources_used", "");
			}

			errflg = 0;
			yproplist = nvl_to_prop_array(yproperties, NULL,
			    &errflg, CL_TYPE_RS, (char *)NULL);
			if (errflg) {
				first_err = errflg;
				goto cleanup;
			}
			xproplist = nvl_to_prop_array(xproperties, NULL,
			    &errflg, CL_TYPE_RS, (char *)NULL);
			if (errflg) {
				first_err = errflg;
				goto cleanup;
			}
			rsprops.srp_predefined = &yproplist;
			rsprops.srp_extension = &xproplist;

			if (force) {
				clerrno = eliminate_rg_sysprop(rsnames[i],
				    verbose);
				if (clerrno != CL_NOERR) {
					if (first_err == 0) {
						// first error!
						first_err = clerrno;
					}
					continue;
				}
			}

			scha_status = rgm_scrgadm_update_rs_prop_no_validate(
			    rs_name, &rsprops, zc_name);
			rv = filter_warning(scha_status.err_code);
			if (rv) {
				print_rgm_error(scha_status, NULL);
				clerror("Failed to eliminate resource "
				    "dependencies, try to delete one resource "
				    "at a time.\n");
				clerror("One or more specified resources may "
				    "have been modified.\n");
				if (first_err == 0) {
					// first error!
					first_err = map_scha_error(rv);
				}
				if (!force)
					goto cleanup;
			} else {
				if (scha_status.err_msg &&
				    !nru_supp) {
					fprintf(stdout, "%s\n",
					    scha_status.err_msg);
				}
				if (verbose) {
					clmessage("Eliminated any "
					    "resource "
					    "dependencies of \"%s\".\n",
					    rsnames[i]);
				}
			}
		}

	} else {
		// Eliminate dependencies from resources in allrs on rsnames
		for (allrs.start(); allrs.end() != 1; allrs.next()) {
			internal_rs = B_FALSE;
			if (rs_name) {
				free(rs_name);
				rs_name = NULL;
			}
			if (zc_name) {
				free(zc_name);
				zc_name = NULL;
			}

			// First get the RS name and the zone cluster name
			scconf_err = scconf_parse_obj_name_from_cluster_scope(
					allrs.getValue(), &rs_name, &zc_name);

			if (scconf_err != SCCONF_NOERR) {

				if (first_err == CL_NOERR) {
					first_err = CL_EINTERNAL;
				}

				continue;
			}
			for (i = 0; rsnames[i]; i++) {
				if (strcmp(rsnames[i], allrs.getValue()) == 0) {
					internal_rs = B_TRUE;
					break;
				}
			}
			if (internal_rs)
				continue;
			for (i = 0; rsnames[i]; i++) {
				res = anydep_rs(allrs.getValue(), rsnames[i],
				    &errflg, &loc, &nru_supp);
				if (errflg) {
					first_err = errflg;
					goto cleanup;
				}
				if (res) {
					yproperties.clear();
					xproperties.clear();
					if (xproplist) {
						free_prop_array(xproplist);
						xproplist = NULL;
					}
					if (yproplist) {
						free_prop_array(yproplist);
						yproplist = NULL;
					}

					clerrno = eliminate_rg_sysprop(
					    allrs.getValue(), verbose);
					if (clerrno != CL_NOERR) {
						if (first_err == 0) {
							// first error!
							first_err = clerrno;
						}
						continue;
					}
					strcpy(dep_str, rsnames[i]);
					if (loc == LOCAL_NODE) {
						strcat(dep_str,
						    "{local_node}");
					} else if (loc == ANY_NODE) {
						strcat(dep_str,
						    "{any_node}");
					}
					yproperties.add(
					    "Resource_dependencies_restart-",
					    dep_str);
					yproperties.add(
					    "Resource_dependencies_weak-",
					    dep_str);
					yproperties.add(
					    "Resource_dependencies-",
					    dep_str);
					yproperties.add(
"Resource_dependencies_offline_restart-", dep_str);
					if (nru_supp) {
						yproperties.add(
						    "network_resources_used-",
						    dep_str);
					}
					errflg = 0;
					yproplist = nvl_to_prop_array(
					    yproperties, allrs.getValue(),
					    &errflg, CL_TYPE_RS, (char *)NULL);
					if (errflg) {
						first_err = errflg;
						goto cleanup;
					}
					xproplist = nvl_to_prop_array(
					    xproperties, allrs.getValue(),
					    &errflg, CL_TYPE_RS, (char *)NULL);
					if (errflg) {
						first_err = errflg;
						goto cleanup;
					}
					rsprops.srp_predefined = &yproplist;
					rsprops.srp_extension = &xproplist;

					scha_status =
rgm_scrgadm_update_rs_prop_no_validate(rs_name, &rsprops, zc_name);
					rv = filter_warning(
					    scha_status.err_code);
					if (rv) {
						print_rgm_error(scha_status,
						    NULL);
						clerror("Failed to eliminate "
						    "resource dependencies "
						    "from \"%s\" on \"%s\".\n",
						    allrs.getValue(),
						    rsnames[i]);
						if (first_err == 0) {
							// first error!
							first_err =
							    map_scha_error(rv);
						}
					} else {
						if (scha_status.err_msg &&
						    !nru_supp) {
							fprintf(stdout, "%s\n",
							    scha_status.
							    err_msg);
						}
						if (verbose) {
							fprintf(stdout,
							    "Eliminated "
							    "resource "
							    "dependencies "
							    "from \"%s\" on "
							    "\"%s\".\n",
							    allrs.getValue(),
							    rsnames[i]);
						}
					}
				}
			}
		}
	}
cleanup:
	if (xproplist)
		free_prop_array(xproplist);
	if (yproplist)
		free_prop_array(yproplist);
	if (rs_name) {
		free(rs_name);
	}
	if (zc_name) {
		free(zc_name);
	}
	return (first_err);
}

clerrno_t
disable_rs(ValueList &rslist, boolean_t verbose,
	boolean_t force, char *zc_name)
{
	char **rsnames = (char **)0;
	clerrno_t errflg = CL_NOERR;
	int rv = 0;
	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};

	rsnames = vl_to_names(rslist, &errflg);
	if (errflg)
		return (errflg);

	scha_status = rgm_scswitch_set_resource_switch((const char **) rsnames,
	    SCHA_SWITCH_DISABLED, B_FALSE, B_FALSE, (const char **)NULL,
	    verbose, zc_name);
	rv = scha_status.err_code;
	if (rv != SCHA_ERR_NOERR && !(force)) {
		print_rgm_error(scha_status, NULL);
		errflg = map_scha_error(rv);
	} else if (verbose && scha_status.err_msg) {
		fprintf(stdout, "%s\n", scha_status.err_msg);
	}
	return (errflg);
}

//
// This method will fetch all the known resources in
// the specified RGs. RGs should be scoped with their
// zone cluster names, to indicate an RG in a particular
// zone cluster.

clerrno_t
get_all_rs(ValueList &allrs, char **rg_names)
{
	clerrno_t clerrno = CL_NOERR;
	char **rs_names = (char **)NULL;
	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};
	int i, j, str_len;
	int rv = 0;
	ValueList zc_names_list, all_rg;
	char *rg_name, *zc_name;
	scconf_errno_t scconf_err = SCCONF_NOERR;
	char *rs_name;

	// If rg_names was not specified, then fetch all the
	// known resource groups.
	if (rg_names == (char **)NULL) {
		clerrno = get_all_rg(all_rg);
		if (clerrno != CL_NOERR) {
			// If there was an error, then return.
			// get_all_rg() reports any errors that occur.
			// There is not to report errors here.
			all_rg.clear();
			return (clerrno);
		}

		// Convert the ValueList to char**
		rg_names = vl_to_names(all_rg, &clerrno);
		all_rg.clear();
		if (clerrno != CL_NOERR) {
			return (clerrno);
		}
	}

	// Iterate through all the RGs and fetch the resource list for
	// each RG.
	rg_name = NULL;
	zc_name = NULL;
	for (i = 0; rg_names[i] != NULL; i++) {
		if (rs_names) {
			rgm_free_strarray(rs_names);
			rs_names = (char **)NULL;
		}

		if (rg_name) {
			free(rg_name);
		}
		if (zc_name) {
			free(zc_name);
		}

		scconf_err = scconf_parse_obj_name_from_cluster_scope(
				rg_names[i], &rg_name, &zc_name);

		if (scconf_err != SCCONF_NOERR) {
			clcommand_perror(CL_EINTERNAL);
			clerrno = CL_EINTERNAL;
			goto cleanup;
		}

		scha_status = rgm_scrgadm_getrsrclist(rg_name, &rs_names,
				zc_name);
		rv = scha_status.err_code;
		if (rv != SCHA_ERR_NOERR) {
			print_rgm_error(scha_status, NULL);
			clerror("Failed to obtain list of all "
			    "resources in resource group "
			    "\"%s\".\n", rg_names[i]);
			clerrno = map_scha_error(rv);
			goto cleanup;
		}
		if (rs_names == NULL)
			continue;

		for (j = 0; rs_names[j]; j++) {
			str_len = strlen(rs_names[j]) + 1; // 1 for NULL
			if (zc_name) {
				str_len += strlen(zc_name) + 1; // 1 for ':'
			}
			rs_name = (char *)calloc(str_len, 1);
			if (zc_name) {
				strcat(rs_name, zc_name);
				strcat(rs_name, ":");
			}
			strcat(rs_name, rs_names[j]);
			allrs.add(rs_name);
			free(rs_name);
		}
	}

cleanup:
	if (rs_names)
		rgm_free_strarray(rs_names);

	if (rg_name) {
		free(rg_name);
	}
	if (zc_name) {
		free(zc_name);
	}

	return (clerrno);
}

//
// This method will fetch all resource groups, including
// the ones inside zone clusters.
//
clerrno_t
get_all_rg(ValueList &allrg)
{
	clerrno_t clerrno, first_err = CL_NOERR;
	char **rg_names = (char **)NULL;
	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};
	int i, j, str_len;
	int rv;
	ValueList zc_names_list;
	char *rg_name;

	// Irrespective of whether there are any zone clusters or not
	// we have to get the RGs from the current cluster.
	// Add the RGs from the current cluster.
	zc_names_list.add(NULL);

	scha_status = scswitch_getrglist(&rg_names, B_FALSE, NULL);
	rv = scha_status.err_code;
	if (rv != SCHA_ERR_NOERR) {
		clerror("Failed to obtain list of all resource groups.\n");
		print_rgm_error(scha_status, NULL);
		first_err = map_scha_error(rv);
		goto cleanup;
	}

	if (rg_names != (char **)NULL) {
		for (i = 0; rg_names[i] != NULL; i++) {
			allrg.add(rg_names[i]);
		}
	}

#if (SOL_VERSION >= __s10)
	// Now get all the known zone cluster names
	// known to us.
	clerrno = get_zone_cluster_names(zc_names_list);

	if (clerrno != CL_NOERR) {
		clerror("Internal Error.\n");
		first_err = CL_EINTERNAL;
		goto cleanup;
	}
#endif

	// If there are no known zone clusters, then we can stop here.
	if (zc_names_list.getSize() < 1) {
		goto cleanup;
	}

	// If we are here, then that means there are zone clusters.
	if (rg_names == (char **)NULL) {
		rgm_free_strarray(rg_names);
	}

	// Add the RGs from known zone clusters.
	for (zc_names_list.start(); !zc_names_list.end();
		zc_names_list.next()) {
		scha_status = scswitch_getrglist(&rg_names, B_FALSE,
					zc_names_list.getValue());
		rv = scha_status.err_code;
		// If there is an error, break out of the loop.
		if (rv != SCHA_ERR_NOERR) {
			clerror("Failed to obtain list of all resource"
				"groups.\n");
			print_rgm_error(scha_status, NULL);
			clerrno = map_scha_error(rv);

			if (first_err == CL_NOERR) {
				first_err = clerrno;
			}

			break;
		}

		if (rg_names == (char **)NULL) {
			continue;
		}

		// Now add the RGs to the list
		for (i = 0; rg_names[i] != NULL; i++) {
			allrg.add(rg_names[i]);
			// We don't have to prepend the zone cluster
			// name to the RG name as that is done by
			// scswitch_getrglist() itself.
		}

		// Free the rg_names array.
		rgm_free_strarray(rg_names);
		rg_names = (char **)NULL;
	}

cleanup:
	if (rg_names)
		rgm_free_strarray(rg_names);

	return (first_err);
}

int
get_cluster_nodecount(int *errflg)
{
	scstat_errno_t scstat_err;
	scstat_node_t *cl_nodes;
	scstat_node_t *tmp_nodes;
	int num_nodes = 0;

	*errflg = 0;

	scstat_err = scstat_get_nodes(&cl_nodes);
	if (scstat_err != SCSTAT_ENOERR) {
		clerror("Failed to obtain cluster node information.\n");
		*errflg = CL_EINTERNAL;
		goto cleanup;
	}
	for (tmp_nodes = cl_nodes; tmp_nodes;
	    tmp_nodes = tmp_nodes->scstat_node_next) {
		if (cl_nodes->scstat_node_name) {
			num_nodes++;
		}
	}
cleanup:
	if (cl_nodes)
		scstat_free_nodes(cl_nodes);
	return (num_nodes);
}

//
// Returns error if the nodelist contains a mix of server and farm nodes, or
// if fails to do the check.
//
clerrno_t
check_server_farm_mix(ValueList &nodelist, char *objname)
{
	boolean_t iseuropa;
	scconf_errno_t scconf_status = SCCONF_NOERR;
	char errbuff[BUFSIZ];
	uint_t ismix;
	boolean_t first_node;
	char *nodes = (char *)NULL;
	clerrno_t clerrno = CL_NOERR;
	int space_needed;

	if (nodelist.getSize() == 0)
		return (CL_NOERR);

	clerrno = europa_open(&iseuropa);
	if (clerrno)
		return (clerrno);

	if (!iseuropa)
		return (CL_NOERR);

	space_needed = nodelist.getSize() + 1;
	for (nodelist.start(); nodelist.end() != 1; nodelist.next()) {
		space_needed += strlen(nodelist.getValue());
	}

	nodes = (char *)calloc(space_needed, sizeof (char));
	if (nodes == NULL) {
		clcommand_perror(CL_ENOMEM);
		clerrno = CL_ENOMEM;
		goto cleanup;
	}
	first_node = B_TRUE;
	for (nodelist.start(); nodelist.end() != 1; nodelist.next()) {
		if (first_node) {
			(void) strcat(nodes, nodelist.getValue());
			first_node = B_FALSE;
		} else {
			(void) strcat(nodes, ",");
			(void) strcat(nodes, nodelist.getValue());
		}
	}
	scconf_status = scconf_isserverfarm_mix(nodes, &ismix);
	if (scconf_status != SCCONF_NOERR) {
		scconf_strerr(errbuff, scconf_status);
		fprintf(stderr, "%s\n", errbuff);
		clerror("Failed to get cluster node information.\n");
		clerrno = map_scconf_error(scconf_status);
		goto cleanup;
	}
	if (ismix) {
		if (objname) {
			clerror("The resulting nodelist for \"%s\" may not "
			    "contain a mix of server and farm nodes.\n",
			    objname);
		} else {
			clerror("You cannot specify a mix of server and farm "
			    "nodes in the nodelist.\n");
		}
		clerrno = CL_EINVAL;
		goto cleanup;
	}

cleanup:
	if (nodes)
		free(nodes);
	europa_close();
	return (clerrno);
}

void
namelist_to_vl(namelist_t *new_nodelist, ValueList &new_nl_vl)
{
	while (new_nodelist) {
		new_nl_vl.add(new_nodelist->nl_name);
		new_nodelist = new_nodelist->nl_next;
	}
}

clerrno_t
europa_open(boolean_t *iseuropa)
{
	scconf_errno_t scconf_status = SCCONF_NOERR;
	clerrno_t clerrno = CL_NOERR;
	char errbuff[BUFSIZ];

	// Check if europa is enabled
	scconf_status = scconf_iseuropa(iseuropa);
	if (scconf_status != SCCONF_NOERR) {
		scconf_strerr(errbuff, scconf_status);
		fprintf(stderr, "%s\n", errbuff);
		clerror("Failed to get cluster configuration information.\n");
		clerrno = map_scconf_error(scconf_status);
		return (clerrno);
	}

	if (*iseuropa == B_FALSE)
		return (CL_NOERR);

	// dlopen libscxcfg
	scconf_status = scconf_libscxcfg_open();
	if (scconf_status != SCCONF_NOERR) {
		clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}
	scconf_status = scconf_libfmm_open();
	if (scconf_status != SCCONF_NOERR) {
		clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}
	return (CL_NOERR);
}

void
europa_close()
{
	(void) scconf_libscxcfg_close();
	(void) scconf_libfmm_close();
}

scha_err_t
filter_warning(scha_err_t scha_err)
{
	if (scha_err >= SCHA_ERR_RS_VALIDATE) {
		return (SCHA_ERR_NOERR);
	} else {
		return (scha_err);
	}
}

//
// Return all the rg names in the system.
// If inside a non-global zone, return only the ones with a nodename entry
// containing this zone. If there are rgs that don't include this zone,
// print a warning in verbose mode.
//
clerrno_t
z_getrglist(char ***rg_names, boolean_t verbose, optflgs_t susp_included,
    char *zc_name)
{
	clerrno_t clerrno = CL_NOERR;
	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};
	char **global_rg_names = NULL;
	char **active_rg_names = NULL;
	char **local_rg_names = NULL;
	char *rg_part = NULL, *zc_part = NULL;
	int local_rg_count = 0;
	int global_rg_count = 0;
	int rv = 0;
	int i = 0;
	int j = 0;
	rgm_rg_t *rgconf = (rgm_rg_t *)0;
	scconf_errno_t scconf_err = SCCONF_NOERR;
	boolean_t zc_flag = B_FALSE;

#if (SOL_VERSION >= __s10)
	// We will ignore scconf_err since we might get an error while
	// running inside a 1334 zone. This function has to work in a
	// 1334 zone scenario as well.
	scconf_err = scconf_zone_cluster_check(&zc_flag);
#endif

	if ((sc_nv_zonescheck() == 0) || (zc_flag == B_TRUE)) {
		scha_status = scswitch_getrglist(&local_rg_names, B_FALSE,
						zc_name);
	} else {
		scha_status = scswitch_getrglist(&local_rg_names, B_TRUE,
						NULL);
	}
	rv = scha_status.err_code;
	if (rv != SCHA_ERR_NOERR) {
		print_rgm_error(scha_status, NULL);
		clerror("Failed to obtain list of all resource "
		    "groups.\n");
		clerrno = map_scha_error(rv);
		goto cleanup;
	}
	if (local_rg_names) {
		for (i = 0; local_rg_names[i] != NULL; i++) {
			local_rg_count++;
		}
	}
	if ((!susp_included) && (local_rg_names)) {
		active_rg_names = ((char **)calloc(
		    (size_t)(local_rg_count + 1), sizeof (char *)));
		if (!active_rg_names) {
			clcommand_perror(CL_ENOMEM);
			clerrno = CL_ENOMEM;
			goto cleanup;
		}

		rg_part = zc_part = NULL;
		for (i = 0, j = 0; local_rg_names[i] != NULL; i++) {
			if (rgconf) {
				rgm_free_rg(rgconf);
				rgconf = (rgm_rg_t *)NULL;
			}
			if (rg_part) {
				free(rg_part);
				rg_part = NULL;
			}
			if (zc_part) {
				free(zc_part);
				zc_part = NULL;
			}
			// First parse to get the RG name and ZC name.
			// scswitch_getrglist() returns RGs which have the
			// zone cluster name prepended.
			scconf_err = scconf_parse_obj_name_from_cluster_scope(
					local_rg_names[i], &rg_part, &zc_part);

			if (scconf_err != SCCONF_NOERR) {
				clerror("Internal Error.\n");
				clerrno = CL_EINTERNAL;
				goto cleanup;
			}

			scha_status = rgm_scrgadm_getrgconf(rg_part,
			    &rgconf, zc_part);
			rv = scha_status.err_code;
			if ((rv != SCHA_ERR_NOERR) || (!rgconf)) {
				print_rgm_error(scha_status, NULL);
				clerror("Failed to obtain resource group "
				    "information\n");
				clerrno = map_scha_error(rv);
				goto cleanup;
			}
			if (rgconf->rg_suspended) {
				if (verbose) {
					clwarning("Suspended resource group "
					    "\"%s\" and resources in it will "
					    "not be impacted.\n",
					    rgconf->rg_name);
				}
				continue;
			}

			// Copy to active RGs
			active_rg_names[j++] = local_rg_names[i];
		}
		if (j > 0) {
			*rg_names = active_rg_names;
		} else {
			*rg_names = NULL;
		}
		if (local_rg_names)
			free(local_rg_names);
	} else {
		*rg_names = local_rg_names;
	}

	if ((sc_nv_zonescheck() == 0) || !verbose) {
		goto cleanup;
	}

	scha_status = scswitch_getrglist(&global_rg_names, B_FALSE, NULL);
	rv = scha_status.err_code;
	if (rv != SCHA_ERR_NOERR) {
		print_rgm_error(scha_status, NULL);
		clerror("Failed to obtain list of all resource "
		    "groups.\n");
		clerrno = map_scha_error(rv);
		goto cleanup;
	}

	if (global_rg_names) {
		for (i = 0; global_rg_names[i] != NULL; i++) {
			global_rg_count++;
		}
	}
	if (local_rg_count < global_rg_count) {
		clwarning("Operation will be restricted within resource groups "
		    "having this zone as part of its nodelist.\n");
	}
cleanup:
	rgm_free_strarray(global_rg_names);
	if (rgconf)
		free(rgconf);
	if (rg_part) {
		free(rg_part);
		rg_part = NULL;
	}
	if (zc_part) {
		free(zc_part);
		zc_part = NULL;
	}
	return (clerrno);
}

//
// Return the propname and nodename part of specified property names. Also
// set pn_specified if the user made an attempt to specify per-node names.
//
clerrno_t
tok_prop(char *src_prop, boolean_t *pn_specified, char **proppart,
    char **nodepart)
{
	char *str1 = (char *)NULL;
	char *str2 = (char *)NULL;
	char *t1 = (char *)NULL;
	char *t2 = (char *)NULL;
	char *t2_save = (char *)NULL;
	clerrno_t clerrno = CL_NOERR;
	clerrno_t first_err = CL_NOERR;
	char *good_node = (char *)NULL;
	char *lasts = NULL;
	int nodecount;
	char *node_elem;
	char *br1 = (char *)NULL;
	char *br2 = (char *)NULL;

	*pn_specified = B_FALSE;

	str1 = strdup(src_prop);
	if (str1 == NULL) {
		clcommand_perror(CL_ENOMEM);
		return (CL_ENOMEM);
	}

	br1 = strchr(src_prop, '{');
	br2 = strchr(src_prop, '}');
	if ((br1 != NULL) && (br2 == NULL)) {
		clwarning("Invalid node specification \"%s\".\n", src_prop);
	} else if ((br2 != NULL) && (br1 == NULL)) {
		clwarning("Invalid node specification \"%s\".\n", src_prop);
	}

	if (br1 && br2) {
		if ((br2 != strrchr(src_prop, '}')) ||
		    (br1 != strrchr(src_prop, '{'))) {
			clwarning("Invalid node specification \"%s\".\n",
			    src_prop);
		} else if ((src_prop + strlen(src_prop)) != (br2 + 1)) {
			clwarning("Invalid node specification \"%s\".\n",
			    src_prop);
		}
	}

	if (br1 != NULL) {
		*pn_specified = B_TRUE;
		t1 = strtok(src_prop, "{}");
		if (t1) {
			t2 = strtok(NULL, "{}");
		}
		if (t1) {
			strcpy(str1, t1);
		}
		if (t2) {
			t2_save = t2;
			nodecount = 1;
			while (*t2) {
				if (*t2 == ',')
					nodecount++;
				t2++;
			}
			t2 = t2_save;
			str2 = (char *)calloc(1, strlen(t2) + (nodecount + 1) *
			    MAXHOSTNAMELEN);
			if (str2 == NULL) {
				clcommand_perror(CL_ENOMEM);
				return (CL_ENOMEM);
			}
			node_elem = strtok_r(t2, ",", &lasts);
			while (node_elem) {
				if (good_node) {
					free(good_node);
					good_node = NULL;
				}
				clerrno = get_good_node(node_elem, &good_node);
				if (clerrno) {
					if (first_err == 0) {
						// first error
						first_err = clerrno;
					}
					node_elem = strtok_r(NULL, ",", &lasts);
					continue;
				}
				strcat(str2, good_node);
				strcat(str2, ",");
				node_elem = strtok_r(NULL, ",", &lasts);
			}
		}
	}

cleanup:
	*nodepart = str2;
	*proppart = str1;
	if (good_node)
		free(good_node);
	return (first_err);
}

//
// Given a resource structure, detect if an error is set for it on any of
// the nodes.
//
clerrno_t
check_rs_error(rgm_resource_t *rs, boolean_t *rs_err, char *zc_name)
{
	scstat_rs_status_t *rsnode;
	scstat_errno_t scstat_status;
	scstat_rs_t *rsstat = (scstat_rs_t *)NULL;
	char *rg_name;
	int len_count = 1;

	*rs_err = B_FALSE;
	// If zone cluster was specified, we have to scope the RG name
	// with the zone cluster name
	len_count += strlen(rs->r_rgname);

	if (zc_name) {
		// Add one space for the separator
		len_count += strlen(zc_name) + 1;
	}

	rg_name = (char *)calloc(len_count, 1);

	if (zc_name) {
		strcat(rg_name, zc_name);
		strcat(rg_name, ":");
	}

	strcat(rg_name, rs->r_rgname);
	// Now, get the resource status
	scstat_status = scstat_get_rs(rg_name, rs->r_name, &rsstat,
	    B_TRUE);
	// Free memory
	free(rg_name);
	rg_name = NULL;

	if (scstat_status != SCSTAT_ENOERR) {
		print_scstat_error(scstat_status, rs->r_name);
		return (map_scstat_error(scstat_status));
	}

	for (rsnode = rsstat->scstat_rs_status_list; rsnode;
	    rsnode = rsnode->scstat_rs_status_by_node_next) {
		if (strcasecmp(rsnode->scstat_rs_state_str, "Stop failed")
		    == 0) {
			*rs_err = B_TRUE;
			return (CL_NOERR);
		}
	}
	return (CL_NOERR);
}

//
// Check if the specified resource group is a valid one.
// If the resource group is scoped to a zone cluster, this
// function will also check the validity of the zone cluster.
// If print_err is set to true, this function will print errors
// including reporting that the resource group was invalid, if
// that were the case.
//
boolean_t
is_good_rg(char *rgname, boolean_t print_err)
{
	rgm_rg_t *rgconf = (rgm_rg_t *)0;
	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};
	char *zc_name = NULL;
	char *rg_part = NULL;
	scconf_errno_t scconf_err = SCCONF_NOERR;
	boolean_t gz_flag = B_TRUE;
	boolean_t is_valid_rg = B_FALSE;
	int clconf_err;
	uint_t cluster_id;

	// Fetch the zone cluster name if the rg name was scoped.
	scconf_err = scconf_parse_obj_name_from_cluster_scope(rgname,
				&rg_part, &zc_name);
	if (scconf_err != SCCONF_NOERR) {
		if (print_err) {
			clerror("Internal error.\n");
			goto cleanup;
		}
	}

	// If zone cluster name was specifed, ensure that rg part was
	// also specified. Check whether the zone cluster name is valid
	// or not.
	if (zc_name) {
		// Was rg part specified???
		if (!rg_part) {
			if (print_err) {
				clerror("Invalid resource group \"%s\" "
				    "specified.\n", rgname);
			}
			goto cleanup;
		}

		// Is zone cluster valid???
		// Check whether we are in a global zone first.
		if (sc_nv_zonescheck()) {
			gz_flag = B_FALSE;
		}

#if (SOL_VERSION >= __s10)
		// If we are in the global zone and the user specified
		// "global", then we dont have to check the validity
		// of the zone cluster. For anything else, we have to check
		// the validity. The following conditional check evaluates
		// this condition.
		if (!(gz_flag && (!(strcmp(zc_name,
					GLOBAL_ZONE_NAME))))) {
			// We have to check the validity of the
			// zone cluster.
			// Check the cluster id.
			clconf_err = clconf_get_cluster_id(zc_name,
						&cluster_id);

			if (clconf_err || (cluster_id < MIN_CLUSTER_ID)) {
				// If we are here, then there was an error.
				clerror("%s is not a valid zone cluster.\n",
					zc_name);
				goto cleanup;
			}
		}
#endif
	}

#if (SOL_VERSION >= __s10)
	// If we are here, that means any zone cluster check has been passed.
	// If the user specified "global" as the zone cluster name in the
	// global zone, we will have to set zc_name to NULL as rgm does not
	// understand "global".
	if (zc_name && (!(strcmp(zc_name, GLOBAL_ZONE_NAME)))) {
		free(zc_name);
		zc_name = NULL;
	}
#endif

	scha_status = rgm_scrgadm_getrgconf(rg_part, &rgconf, zc_name);
	if (scha_status.err_code != SCHA_ERR_NOERR) {
		if (print_err) {
			clerror("%s: Invalid resource group\n", rgname);
		}
		is_valid_rg = B_FALSE;
	} else {
		is_valid_rg = B_TRUE;
	}
cleanup:
	if (rgconf) {
		rgm_free_rg(rgconf);
	}

	if (zc_name) {
		free(zc_name);
	}

	return (is_valid_rg);
}

//
// Check if the specified resource group is defined as system resource group.
//
clerrno_t
check_system_rg(char *rgname, boolean_t *sysrg)
{
	rgm_rg_t *rgconf = (rgm_rg_t *)0;
	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};
	char *rg_part, *zc_part;
	scconf_errno_t scconf_err = SCCONF_NOERR;

	*sysrg = B_FALSE;
	rg_part = zc_part = NULL;
	// First get the RG name and the zone cluster name.
	scconf_err = scconf_parse_obj_name_from_cluster_scope(
				rgname, &rg_part, &zc_part);
	if (scconf_err != SCCONF_NOERR) {
		clerror("Internal error.\n");
		return (CL_ENOMEM);
	}

	scha_status = rgm_scrgadm_getrgconf(rg_part, &rgconf, zc_part);
	if (scha_status.err_code != SCHA_ERR_NOERR) {
		print_rgm_error(scha_status, NULL);
		return (CL_ENOENT);
	}
	*sysrg = rgconf->rg_system;
	if (rgconf)
		rgm_free_rg(rgconf);
	if (rg_part) {
		free(rg_part);
	}
	if (zc_part) {
		free(zc_part);
	}
	return (CL_NOERR);
}

#if SOL_VERSION >= __s10
// Check if the nodelist contains a mix of physical node names and
// virtual node names. This function assumes that all the nodes
// are valid node names (i.e: before calling this function call
// get_good_nodes();
// If there was such a mix found then flagp will be set to 1,
// else flagp will be set to 0.

clerrno_t
check_phys_virt_nodes_mix(ValueList &nodelist, uint_t *flagp)
{
	boolean_t found_virt = B_FALSE;
	boolean_t found_phys = B_FALSE;
	uint_t cl_id = 0;
	scconf_errno_t scconf_err = SCCONF_NOERR;
	scconf_nodeid_t nodeid;
	clerrno_t clerrno = CL_NOERR;
	ValueList zc_names;
	ValueList zc_node_names;

	// Get the list of known zone clusters
	clerrno = get_zone_cluster_names(zc_names);

	if (clerrno != CL_NOERR) {
		clerror("Internal error.\n");
		return (CL_EINTERNAL);
	}

	// Now get all the zone cluster host names
	for (zc_names.start(); !zc_names.end(); zc_names.next()) {
		clerrno = get_hostname_list_for_zc(zc_names.getValue(),
						zc_node_names);

		if (clerrno != CL_NOERR) {
			clerror("Internal error.\n");
			zc_names.clear();
			zc_node_names.clear();
			return (CL_EINTERNAL);
		}
	}

	for (nodelist.start(); nodelist.end() != 1; nodelist.next()) {

		// Let us check whether both the flags are true.
		// If both are true, we can break out of this loop.
		if (found_phys && found_phys) {
			break;
		}

		scconf_err = SCCONF_NOERR;
		scconf_err = scconf_get_nodeid(nodelist.getValue(),
						&nodeid);
		if (scconf_err != SCCONF_ENOEXIST) {
			// This is a physical node.
			found_phys = B_TRUE;
			continue;
		}

		// This is not a physical cluster node. Let us check
		// whether it is a zone cluster node
		if (zc_node_names.isValue(nodelist.getValue())) {
			found_virt = B_TRUE;
		}

	}

	if (found_virt && found_phys) {
		// If we are here then there was a mix of node zones
		// and zones clusters
		*flagp = 1;
	} else {
		*flagp = 0;
	}

	// Free any memory
	zc_node_names.clear();
	zc_names.clear();
	return (CL_NOERR);
}

//
// get_zone_clusters_with_rs(char *rsname, ValueList &zc_names,
//	ValueList &zc_check_list);
// This method will add to "zc_names" the list of zone clusters
// which have the resource "rsname". If "zc_check_list" is
// specified, this method will check only the list of zone
// clusters specified in "zc_check_list" to see whether they
// contain the resource "rsname". If "zc_check_list" is not
// specified, this method will look-up all the known zone
// clusters. This method will not check the current cluster.
// The caller of this method is responsible to free the memory
// in "zc_names". This method assumes that the zone clusters
// specified in "zc_check_list" are valid and does not check
// their validity.
//
// Return Values :
//	CL_NOERR	- No error.
//	CL_EINVAL	- Invalid resource name
//	CL_ENOENT	- Resource does not exist in any zone cluster.
//	CL_EINTERNAL	- Internal Error.

clerrno_t
get_zone_clusters_with_rs(char *rsname, ValueList &zc_names,
    ValueList &zc_check_list) {

	clerrno_t clerrno = CL_NOERR;
	rgm_resource_t *rs = (rgm_resource_t *)0;
	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};
	boolean_t rs_exists = B_FALSE;
	ValueList zc_list;

	if (!rsname) {
		return (CL_EINVAL);
	}

	// If zone cluster check list was not specified, then fetch
	// the list of known zone clusters.
	if (zc_check_list.getSize() < 1) {
		clerrno = get_zone_cluster_names(zc_list);

		if (clerrno != CL_NOERR) {
			return (clerrno);
		}
	} else {
		for (zc_check_list.start(); !zc_check_list.end();
				zc_check_list.next()) {
			zc_list.add(zc_check_list.getValue());
		}
	}

	// Now, we have the list of zone clusters where we have to
	// check the resource name.
	for (zc_list.start(); !zc_list.end();
			zc_list.next()) {

		// Check whether the RS exists in this zone cluster.
		scha_status = rgm_scrgadm_getrsrcconf(rsname, &rs,
					zc_list.getValue());
		// Free any memory
		if (rs) {
			rgm_free_resource(rs);
			rs = (rgm_resource_t *)NULL;
		}

		// Check for errors.
		if (scha_status.err_code != SCHA_ERR_NOERR) {
			// Resource does not exist in this zone cluster.
			continue;
		}

		// If we are here, it means the resource exists in this
		// zone cluster.
		zc_names.add(zc_list.getValue());
		rs_exists = B_TRUE;
	}

	// Free any memory
	if (rs) {
		rgm_free_resource(rs);
		rs = (rgm_resource_t *)NULL;
	}

	if (zc_check_list.getSize() < 1) {
		zc_list.clear();
	}

	// If the resource was present in at least one zone cluster,
	// then we will not report an error.
	if (rs_exists) {
		return (CL_NOERR);
	}

	// If we are here, it means the resource was not found in any
	// zone cluster.
	return (CL_ENOENT);
}

//
// get_zone_clusters_with_rt(char *rtname, ValueList &zc_names,
//	ValueList &zc_check_list);
// This method will add to "zc_names" the list of zone clusters
// which have the resourcetype "rtname". If "zc_check_list" is
// specified, this method will check only the list of zone
// clusters specified in "zc_check_list" to see whether they
// contain the resourcetype "rtname". If "zc_check_list" is not
// specified, this method will look-up all the known zone
// clusters. This method will not check the current cluster.
// The caller of this method is responsible to free the memory
// in "zc_names". This method assumes that the zone clusters
// specified in "zc_check_list" are valid and does not check
// their validity.
//
// Return Values :
//	CL_NOERR	- No error.
//	CL_EINVAL	- Invalid resource type
//	CL_ENOENT	- ResourceType is not registered in any zone cluster.
//	CL_EINTERNAL	- Internal Error.

clerrno_t
get_zone_clusters_with_rt(char *rtname, ValueList &zc_names,
    ValueList &zc_check_list) {

	clerrno_t clerrno = CL_NOERR;
	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};
	boolean_t rt_exists = B_FALSE;
	char *rt_realname = NULL;
	ValueList zc_list;

	if (!rtname) {
		return (CL_EINVAL);
	}

	// If zone cluster check list was not specified, then fetch
	// the list of known zone clusters.
	if (zc_check_list.getSize() < 1) {
		clerrno = get_zone_cluster_names(zc_list);

		if (clerrno != CL_NOERR) {
			return (clerrno);
		}
	} else {
		for (zc_check_list.start(); !zc_check_list.end();
				zc_check_list.next()) {
			zc_list.add(zc_check_list.getValue());
		}
	}

	// Now, we have the list of zone clusters where we have to
	// check the resource type.
	for (zc_list.start(); !zc_list.end();
			zc_list.next()) {

		// Check whether the RT exists in this zone cluster.
		scha_status = rgmcnfg_get_rtrealname(rtname, &rt_realname,
					zc_list.getValue());
		// Free any memory
		if (rt_realname) {
			free(rt_realname);
			rt_realname = NULL;
		}

		// Check for errors.
		if (scha_status.err_code != SCHA_ERR_NOERR) {
			// RT does not exist in this zone cluster.
			continue;
		}

		// If we are here, it means the RT exists in this
		// zone cluster.
		zc_names.add(zc_list.getValue());
		rt_exists = B_TRUE;
	}

	// Free any memory
	if (rt_realname) {
		free(rt_realname);
		rt_realname = NULL;
	}
	if (zc_check_list.getSize() < 1) {
		zc_list.clear();
	}

	// If the RT was present in at least one zone cluster,
	// then we will not report an error.
	if (rt_exists) {
		return (CL_NOERR);
	}

	// If we are here, it means the RT was not found in any
	// zone cluster.
	return (CL_ENOENT);
}
//
// get_zone_clusters_with_rg(char *rgname, ValueList &zc_names,
//	ValueList &zc_check_list);
// This method will add to "zc_names" the list of zone clusters
// which have the resourcegroup "rgname". If "zc_check_list" is
// specified, this method will check only the list of zone
// clusters specified in "zc_check_list" to see whether they
// contain the resourcegroup "rgname". If "zc_check_list" is not
// specified, this method will look-up all the known zone
// clusters. This method will not check the current cluster.
// The caller of this method is responsible to free the memory
// in "zc_names". This method assumes that the zone clusters
// specified in "zc_check_list" are valid and does not check
// their validity.
//
// Return Values :
//	CL_NOERR	- No error.
//	CL_EINVAL	- Invalid resource type
//	CL_ENOENT	- ResourceGroup absent in any zone cluster.
//	CL_EINTERNAL	- Internal Error.

clerrno_t
get_zone_clusters_with_rg(char *rgname, ValueList &zc_names,
    ValueList &zc_check_list) {

	clerrno_t clerrno = CL_NOERR;
	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};
	boolean_t rg_exists = B_FALSE;
	ValueList zc_list;

	if (!rgname) {
		return (CL_EINVAL);
	}

	// If zone cluster check list was not specified, then fetch
	// the list of known zone clusters.
	if (zc_check_list.getSize() < 1) {
		clerrno = get_zone_cluster_names(zc_list);

		if (clerrno != CL_NOERR) {
			return (clerrno);
		}
	} else {
		for (zc_check_list.start(); !zc_check_list.end();
				zc_check_list.next()) {
			zc_list.add(zc_check_list.getValue());
		}
	}

	// Now, we have the list of zone clusters where we have to
	// check the resource group.
	for (zc_list.start(); !zc_list.end();
			zc_list.next()) {

		// Check whether the RG exists in this zone cluster.
		clerrno = check_rg(rgname, zc_list.getValue());

		// Does RG exist?
		if (clerrno != CL_NOERR) {
			// RG does not exist in this zone cluster.
			continue;
		}

		// If we are here, it means the RG exists in this
		// zone cluster.
		zc_names.add(zc_list.getValue());
		rg_exists = B_TRUE;
	}

	// Free any memory
	if (zc_check_list.getSize() < 1) {
		zc_list.clear();
	}

	// If the RG was present in at least one zone cluster,
	// then we will not report an error.
	if (rg_exists) {
		return (CL_NOERR);
	}

	// If we are here, it means the RG was not found in any
	// zone cluster.
	return (CL_ENOENT);
}
#endif

// This function will fetch all the RTs from the specified
// zone cluster. If "zc_name" is NULL, this function will
// fetch RTs from the current cluster. If "zc_name" is specified,
// The RT name will be scoped with the "zc_name" in the format
// "<ZC name>:<RT name>".

clerrno_t
get_all_rt_from_zc(ValueList &allrt, char *zc_name,
    boolean_t print_err) {

	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};
	char *zone_cluster = NULL;
	char **rt_names = NULL;
	clerrno_t clerrno = CL_NOERR;
	int count, str_len;
	char *tmp_str = NULL;

	// If zone cluster name was specified then make a
	// copy of the name.
	if (zc_name) {
		zone_cluster = strdup(zc_name);
	}

	scha_status = rgm_scrgadm_getrtlist(&rt_names, zone_cluster);
	if (scha_status.err_code != SCHA_ERR_NOERR) {
		if (print_err == B_TRUE) {
			print_rgm_error(scha_status, NULL);
		}
		free(zone_cluster);
		return (map_scha_error(scha_status.err_code));
	}

	// If we did not get any RTs, we will return.
	if (!rt_names) {
		// Free any memory before returning.
		if (zone_cluster) {
			free(zone_cluster);
		}
		return (CL_NOERR);
	}

	// If no zone cluster was specified, we just have to
	// add the RT names to the list and return.
	if (zone_cluster == NULL) {
		clerrno = names_to_valuelist(rt_names, allrt);
		// Print an error if required
		if ((clerrno != CL_NOERR) && (print_err == B_TRUE)) {
			clcommand_perror(clerrno);
		}
		// Free any memory
		rgm_free_strarray(rt_names);
		return (clerrno);
	}

	// If we are here, it means zone cluster name was
	// specified. We have to prepend each RT name with
	// the zone cluster name and the separator.
	for (count = 0; rt_names[count] != NULL; count++) {
		str_len = strlen(zone_cluster);
		str_len += strlen(rt_names[count]) + 1;
		tmp_str = (char *)calloc(str_len, 1);
		strcat(tmp_str, zone_cluster);
		strcat(tmp_str, ":");
		strcat(tmp_str, rt_names[count]);
		allrt.add(tmp_str);
		free(tmp_str);
	}
	// Free memory
	free(zone_cluster);
	rgm_free_strarray(rt_names);
	return (clerrno);
}

// This function will add to "rt_list" the list of available of
// logical host and shared address resources types. This function
// checks for LHflg and SAflg flags in "optflgs" to determine the
// resource types to add. If "zc_names" is specified, this method will add
// all LH and SA RTs from these zone clusters. Please note that all
// the names, in such a case, will be scoped with the zone cluster name.
// It is the callers responsibility to free the memory in "rt_list". This
// method does not check for the validity of the zone cluster names.
clerrno_t
add_lh_or_sa_rts_to_list(ValueList &rt_list, ValueList &zc_names,
    optflgs_t optflgs) {

	char *r_rtname, *full_name;
	scconf_errno_t scconf_err = SCCONF_NOERR;
	clerrno_t clerrno, first_err;
	ValueList rt_types;

	r_rtname = full_name = NULL;
	clerrno = first_err = CL_NOERR;

	// Check whether zone clusters were specified.
	if (zc_names.getSize() == 0) {
		if (optflgs & LHflg) {
			rt_list.add(get_latest_rtname(SCRGADM_RTLH_NAME,
					NULL));
		}

		if (optflgs & SAflg) {
			rt_list.add(get_latest_rtname(SCRGADM_RTSA_NAME,
					NULL));
		}
		// We have finished adding. We can return.
		return (clerrno);
	}

	// If we are here, then zone cluster names were specified.
	if (optflgs & LHflg) {
		rt_types.add(SCRGADM_RTLH_NAME);
	}
	if (optflgs & SAflg) {
		rt_types.add(SCRGADM_RTSA_NAME);
	}
	// We will add one RT type at a time.
	for (rt_types.start(); !rt_types.end(); rt_types.next()) {

		for (zc_names.start(); !zc_names.end();	zc_names.next()) {

			if (r_rtname) {
				r_rtname = NULL;
			}
			if (full_name) {
				free(full_name);
				full_name = NULL;
			}
			// Get the RT name.
			r_rtname = get_latest_rtname(
					rt_types.getValue(),
					zc_names.getValue());
			// Append the cluster scope to this RT name.
			scconf_err = scconf_add_cluster_scope_to_obj_name(
					r_rtname, zc_names.getValue(),
					&full_name);
			if (scconf_err != SCCONF_NOERR) {
				clerrno = map_scconf_error(scconf_err);
				clcommand_perror(clerrno);
				if (first_err == CL_NOERR) {
					first_err = CL_EINTERNAL;
				}
				continue;
			}
			// Add this RT.
			rt_list.add(full_name);
		}
		// Free any memory
		if (r_rtname) {
			r_rtname = NULL;
		}
		if (full_name) {
			free(full_name);
			full_name = NULL;
		}
	}

	// Free memory
	rt_types.clear();
	return (first_err);
}

#if SOL_VERSION >= __s10
clerrno_t
call_rtreg_proxy_register_cmd(ClCommand &cmd) {
	char	*cmd_str;
	int 	indx;
	bool isalive;
	int ret_val;
	int command_length = 0;
	char *stdout_str = NULL;
	char *stderr_str = NULL;
	int optval;
	char *absolute_path;

	rtreg_proxy::rtreg_server_var	rtreg_server_v;
	CORBA::Object_var		obj_v;
	Environment			env;
	CORBA::Exception		*exp;
	naming::naming_context_var	ctx_v;

	//
	// Construct the clrt register command to be executed
	// in the global zone from the arguments passed
	//

	//
	// We know that the second argument to the command will be
	// register. Just to make sure we have this check.
	//

	if (strncmp(cmd.argv[1], "register", strlen(cmd.argv[1]))) {
		clerror("rtreg proxy service can be used only to "
			"register resource types\n");
		return (CL_EOP);
	}

	//
	// Count the total number of characters in the given command
	// Also keep adding a space between each string in the array
	//
	for (indx = 2; indx < cmd.argc; indx++) {
		command_length += strlen(cmd.argv[indx]);
		command_length++;
	}

	//
	// Now allocate memory in cmd_str to accomadate the given
	// command
	//
	cmd_str = (char *)calloc(command_length * sizeof (char), 1);

	if (cmd.argc < 2) {
		free(cmd_str);
		return (CL_EINVAL);
	}

	for (indx = 2; indx < cmd.argc; ++indx) {
		if (indx == (cmd.argc - 1)) {
			strcat(cmd_str, cmd.argv[indx]);
		} else {
			if (cmd.argv[indx][0] == '-') {
				if (cmd.argv[indx][1] == 'f' ||
					cmd.argv[indx][1] == 'i') {
					//
					// Get the realpath here
					// for the option value
					// of either -i or -f
					// option and resolve
					// the absolute path for
					// the file and append
					// it to the command
					//
					absolute_path =
						(char *)
						malloc(sizeof (char)
						* MAXPATHLEN);
					realpath(cmd.argv[indx+1],
						absolute_path);
					if (absolute_path != NULL) {
						strcat(cmd_str, cmd.argv[indx]);
						strcat(cmd_str, " ");
						indx++;
						strcat(cmd_str, absolute_path);
						strcat(cmd_str, " ");
					} else {
						(void) fprintf(stderr, "%s\n",
							gettext("Failed to "
							"obtain the absolute "
							"path of the resource "
							"type registration "
							"file."));
						return (CL_EINVAL);
					}
				} else {
					strcat(cmd_str, cmd.argv[indx]);
					strcat(cmd_str, " ");
				}
			} else {
				strcat(cmd_str, cmd.argv[indx]);
				strcat(cmd_str, " ");
			}
		}
	}

	ctx_v = ns::local_nameserver();

	if (CORBA::is_nil(ctx_v)) {
		//
		// Did not get the local nameserver
		// context. Cannot proceed so return
		// Internal error.
		//
		return (CL_EINTERNAL);
	}

	//
	// The rtreg server object is bound to the local
	// name server so get the object reference from
	// the local name server.
	//
	obj_v = ctx_v->resolve(RTREG_PROXY_SERVER, env);

	//
	// If we get any exception while resolving the object
	// throw an appropriate error message and return
	//
	if ((exp = env.exception()) != NULL) {
		if (naming::not_found::_exnarrow(exp) == NULL) {
			(void) fprintf(stderr, "%s\n", gettext("Failed to "
				"look up the rtreg proxy service"));
			free(cmd_str);
			return (CL_EINTERNAL);
		}

		env.clear();
	}

	//
	// Make sure the reference we got is not NULL
	//
	rtreg_server_v = rtreg_proxy::rtreg_server::_narrow(obj_v);
	ASSERT(!CORBA::is_nil(rtreg_server_v));

	//
	// Make sure the reference we got from the nameserver
	// is still valid and not stale by executing the
	// is_alive() method on the object which just returns
	// a TRUE doing nothing
	//
	isalive = rtreg_server_v->is_alive(env);

	//
	// If we get an exception here or if the isalive is set to
	// FALSE we exit throwing an appropriate error.
	//
	if (env.exception() != NULL || !isalive) {
		env.clear();
		(void) fprintf(stderr, "%s\n", gettext("Failed to access the "
			"rtreg proxy service"));
		free(cmd_str);
		return (CL_EINTERNAL);
	}

	//
	// Invoke the rtreg server to handle the clrt register command in
	// the global zone
	//

	optval = rtreg_server_v->handle_rtreg_cmd(ret_val, stdout_str,
		stderr_str, cmd_str, env);

	if ((exp = env.exception()) != NULL) {
		(void) fprintf(stderr, gettext("Exception occurred during "
		    "remote execution, %s\n"), exp->_name());
		env.clear();
		free(cmd_str);
		return (CL_EINTERNAL);
	}
	if (optval != 0) {
		if (stderr_str != NULL) {
			(void) fprintf(stderr, "%s", stderr_str);
		} else {
			(void) fprintf(stderr, gettext("Failed to execute "
			    "the proxy rtreg command retval = %d\n"),
			    optval);
		}

		free(cmd_str);
		return (CL_EINTERNAL);
	}
	if (stdout_str != NULL) {
		(void) fprintf(stdout, "%s", stdout_str);
	}
	if (stderr_str != NULL) {
		(void) fprintf(stderr, "%s", stderr_str);
	}

	free(cmd_str);
	return (ret_val);
}

//
// convert_nodename_to_zone_hostname
//
// If the specified zone cluster exists on the given base cluster node,
// this function retrieves the hostname of the zone belonging to
// the zone cluster hosted on the given base cluster node.
//
// Return Values :
//	CL_NOERR	- No Error.
//	CL_ENOENT	- Zone cluster does not exist; or
//			- zone cluster does not exist on given base cluster node
//	CL_EINVAL	- Invalid arguments
//	CL_ENOMEM	- Short of memory
//	CL_EINTERNAL	- Internal Error.
//
clerrno_t
convert_nodename_to_zone_hostname(
    char *base_nodenamep, char *zc_namep, char **zc_hostnamep) {
	zc_dochandle_t zone_handle;
	int lib_clzone_err = 0;
	struct zc_nodetab nodetab;

	if (zc_hostnamep == NULL) {
		return (CL_EINVAL);
	}
	// Initialize hostname to NULL
	*zc_hostnamep = NULL;

	if (strlen(zc_namep) == 0) {
		return (CL_EINVAL);
	}

	// Open the zonecfg handle
	zone_handle = zccfg_init_handle();
	lib_clzone_err = zccfg_get_handle(zc_namep, zone_handle);

	if (lib_clzone_err != ZC_OK) {
		return (CL_ENOENT);
	}

	lib_clzone_err = zccfg_setnodeent(zone_handle);

	if (lib_clzone_err != ZC_OK) {
		zccfg_fini_handle(zone_handle);
		return (CL_EINTERNAL);
	}

	// Get the nodelist for this zone cluster
	while (zccfg_getnodeent(zone_handle, &nodetab) == ZC_OK) {
		if (strcmp(nodetab.zc_nodename, base_nodenamep) == 0) {
			// Found zone hostname
			*zc_hostnamep = strdup(nodetab.zc_hostname);
			if (*zc_hostnamep == NULL) {
				(void) zccfg_endnodeent(zone_handle);
				// Close the zonecfg handle
				(void) zccfg_fini_handle(zone_handle);
				return (CL_ENOMEM);
			}
			break;
		}
	}

	(void) zccfg_endnodeent(zone_handle);
	// Close the zonecfg handle
	(void) zccfg_fini_handle(zone_handle);

	if (*zc_hostnamep == NULL) {
		// Zone cluster is not hosted on the given base cluster node
		return (CL_ENOENT);
	}

	return (CL_NOERR);
}

//
// Asks RGMD to evacuate all resource groups hosted
// on the specified nodes of a zone cluster,
// or for the entire zone cluster.
//
// The node list argument passed in is assumed to contain
// the base cluster nodenames.
//
// Possible return values:
//
// 	CL_NOERR		- success
//	CL_EINVAL		- invalid argument
//	any other CL_* error	- mapping for a scha error
//
clerrno_t evacuate_rgs_on_zc_nodes(ValueList &nodes, char *zc_namep) {
	clerrno_t retval = CL_NOERR;
	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};

	// Initializa ORB and other cluster checks
	retval = cluster_init();
	if (retval != CL_NOERR) {
		return (retval);
	}

	if (nodes.getSize() < 0) {
		return (CL_EINVAL);
	}

	if (zc_namep == NULL) {
		return (CL_EINVAL);
	}

	if (nodes.getSize() == 0) {
		//
		// Evacuate the resource groups for the entire zone cluster
		// Set the verbose_flag to B_FALSE.
		//
		scha_status = scswitch_evacuate_rgs(
			NULL,		// node for which evacuation
					// is to be done;
					// NULL means entire cluster
			EVAC_TIMEOUT,	// timeout of evacuation
			B_FALSE,	// verbose flag
			zc_namep);	// cluster name
		if (scha_status.err_code != SCHA_ERR_NOERR) {
			uint_t length =
			    strlen("Resource-group evacuation failed "
			    "for zone cluster ")
			    + strlen(zc_namep)
			    + 2		// double quotes for enclosing zc name
			    + 1;	// NULL terminator
			char *error_tagp = new char[length];
			if (error_tagp == NULL) {
				// Print rgm error message before returning
				print_rgm_error(scha_status,
				    "Resource-group evacuation failed");
				return (CL_ENOMEM);
			}
			sprintf(error_tagp,
			    "Resource-group evacuation failed "
			    "for zone cluster \"%s\"",
			    zc_namep);
			print_rgm_error(scha_status, error_tagp);
			retval = map_scha_error(scha_status.err_code);
			delete [] error_tagp;
		}

	} else {
		//
		// Evacuate the resource groups for each of the nodes
		// specified for the zone cluster
		// Set the verbose_flag to B_FALSE.
		//
		for (nodes.start(); !nodes.end(); nodes.next()) {
			char *nodenamep = NULL;	// zone hostname
			retval = convert_nodename_to_zone_hostname(
			    nodes.getValue(), zc_namep, &nodenamep);
			if ((retval != CL_NOERR) && (retval != CL_ENOENT)) {
				return (retval);
			}

			if (retval == CL_ENOENT) {
				continue;
			}

			scha_status = scswitch_evacuate_rgs(
				nodenamep,	// node for which evacuation
						// is to be done
				EVAC_TIMEOUT,	// timeout of evacuation
				B_FALSE,	// verbose flag
				zc_namep);	// cluster name
			if (scha_status.err_code != SCHA_ERR_NOERR) {
				uint_t length =
				    strlen("Resource-group evacuation "
				    "failed for node ")
				    + strlen(nodenamep)
				    + 2	// double quotes enclosing nodename
				    + strlen(" of zone cluster ")
				    + strlen(zc_namep)
				    + 2	// double quotes enclosing zc name
				    + 1; // NULL terminator
				char *error_tagp = new char[length];
				if (error_tagp == NULL) {
					//
					// Print rgm error message
					// before returning
					//
					print_rgm_error(scha_status,
					    "Resource-group evacuation failed");
					delete [] nodenamep;
					return (CL_ENOMEM);
				}
				sprintf(error_tagp,
				    "Resource-group evacuation "
				    "failed for node \"%s\" "
				    "of zone cluster \"%s\"",
				    nodenamep, zc_namep);
				print_rgm_error(scha_status, error_tagp);

				delete [] error_tagp;
				delete [] nodenamep;

				// We return at first error
				retval = map_scha_error(scha_status.err_code);
				return (retval);
			}

			// No error; evacuation succeeded for the node
			delete [] nodenamep;
		}
	}

	return (retval);
}
#endif
