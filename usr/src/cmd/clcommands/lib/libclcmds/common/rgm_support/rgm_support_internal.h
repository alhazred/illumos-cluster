/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_RGM_SUPPORT_INTERNAL_H
#define	_RGM_SUPPORT_INTERNAL_H

#pragma ident	"@(#)rgm_support_internal.h	1.4	08/05/20 SMI"

#ifdef __cplusplus
extern "C" {
#endif

struct state_mapping {
	char *scstat_string;
	char *cl_string;
};
typedef struct state_mapping state_mapping_t;

// Definition of status codes

#define	CL_ONLINE 		0	// RS/RG is running
#define	CL_OFFLINE  		1	// Stopped due to user action
#define	CL_FAULTED  		2	// Stopped due to failure
#define	CL_DEGRADED		3	// Minor problem
#define	CL_WAIT 		4	// In transition
#define	CL_UNMONITORED		5	// No monitor
#define	CL_UNMANAGED		6	// Unmanaged
#define	CL_PONLINE		7	// Pending online
#define	CL_POFFLINE		8	// Pending offline
#define	CL_PONLINEB		9	// Pending online blocked
#define	CL_ONLINEF		10	// Online faulted
#define	CL_ERGSTOPF		11	// Stop failed
#define	CL_NOTONLINE		12	// Not online
#define	CL_DETACHED		13	// Detached
#define	CL_ESTARTF		14	// Start failed
#define	CL_MONFAIL		15	// Monitor failed
#define	CL_ONLINENOTMON		16	// Online but not monitored
#define	CL_STARTING		17	// Starting
#define	CL_STOPPING		18	// Stopping
#define	CL_ERSSTOPF		19	// Stop failed
#define	CL_UNKNOWN  		20	// Unknown

// Status code strings
#define	CLM_ONLINE		\
	GETTEXT("online")
#define	CLM_OFFLINE		\
	GETTEXT("offline")
#define	CLM_FAULTED		\
	GETTEXT("faulted")
#define	CLM_DEGRADED		\
	GETTEXT("degraded")
#define	CLM_WAIT		\
	GETTEXT("wait")
#define	CLM_UNMONITORED		\
	GETTEXT("unmonitored")
#define	CLM_UNMANAGED		\
	GETTEXT("unmanaged")
#define	CLM_PONLINE		\
	GETTEXT("pending_online")
#define	CLM_POFFLINE		\
	GETTEXT("pending_offline")
#define	CLM_PONLINEB		\
	GETTEXT("pending_online_blocked")
#define	CLM_ONLINEF		\
	GETTEXT("online_faulted")
#define	CLM_ERGSTOPF		\
	GETTEXT("error_stop_failed")
#define	CLM_ERSSTOPF		\
	GETTEXT("stop_failed")
#define	CLM_NOTONLINE		\
	GETTEXT("not_online")
#define	CLM_DETACHED		\
	GETTEXT("detached")
#define	CLM_ESTARTF		\
	GETTEXT("start_failed")
#define	CLM_MONFAIL		\
	GETTEXT("monitor_failed")
#define	CLM_ONLINENOTMON	\
	GETTEXT("online_not_monitored")
#define	CLM_STARTING		\
	GETTEXT("starting")
#define	CLM_STOPPING		\
	GETTEXT("stopping")
#define	CLM_UNKNOWN		\
	GETTEXT("unknown")

// scstat code strings
#define	SCM_ONLINE		\
	GETTEXT("Online")
#define	SCM_OFFLINE		\
	GETTEXT("Offline")
#define	SCM_FAULTED		\
	GETTEXT("Faulted")
#define	SCM_DEGRADED		\
	GETTEXT("Degraded")
#define	SCM_WAIT		\
	GETTEXT("Wait")
#define	SCM_UNMONITORED		\
	GETTEXT("Unmonitored")
#define	SCM_UNMANAGED		\
	GETTEXT("Unmanaged")
#define	SCM_PONLINE		\
	GETTEXT("Pending online")
#define	SCM_POFFLINE		\
	GETTEXT("Pending offline")
#define	SCM_PONLINEB		\
	GETTEXT("Pending online blocked")
#define	SCM_ONLINEF		\
	GETTEXT("Online faulted")
#define	SCM_ERGSTOPF		\
	GETTEXT("Error--stop failed")
#define	SCM_ERSSTOPF		\
	GETTEXT("Stop failed")
#define	SCM_DETACHED		\
	GETTEXT("Detached")
#define	SCM_ESTARTF		\
	GETTEXT("Start failed")
#define	SCM_MONFAIL		\
	GETTEXT("Monitor failed")
#define	SCM_ONLINENOTMON	\
	GETTEXT("Online but not monitored")
#define	SCM_STARTING		\
	GETTEXT("Starting")
#define	SCM_STOPPING		\
	GETTEXT("Stopping")
#define	SCM_UNKNOWN		\
	GETTEXT("Unknown")
#ifdef __cplusplus
}
#endif

#endif	/* _RGM_SUPPORT_INTERNAL_H */
