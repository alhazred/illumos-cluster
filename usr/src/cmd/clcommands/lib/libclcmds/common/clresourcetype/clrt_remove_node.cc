//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clrt_remove_node.cc	1.6	08/07/24 SMI"

//
// Process clrt "remove-node"
//

#include <sys/os.h>
#include <scadmin/scconf.h>
#include <scadmin/scswitch.h>
#include <scadmin/scstat.h>
#include <rgm/rgm_scrgadm.h>
#include <rgm/rgm_cnfg.h>

#include "clcommands.h"

#include "ClCommand.h"

#include "rgm_support.h"
#include "common.h"

//
// clrt "remove-node"
//
clerrno_t
clrt_remove_node(ClCommand &cmd, ValueList &operands, optflgs_t optflgs,
    ValueList &nodes)
{
	clerrno_t first_err = CL_NOERR;
	int rv = 0;
	int i;
	int num_nodes, num_old_nodes;
	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};
	char **rt_names = NULL;
	char *oper1;
	rgm_rg_t *rg = (rgm_rg_t *)0;
	namelist_t *new_nodelist = (namelist_t *)NULL;
	namelist_t *nl;
	namelist_t *nl1;
	clerrno_t errflg = CL_NOERR;
	clerrno_t clerrno = CL_NOERR;
	ValueList good_nodes;
	ValueList good_rts;
	namelist_t *node_nl = (namelist_t *)NULL;
	namelist_t *nl2;
	char *r_rtname = (char *)NULL;
	rgm_rt_t *rt = (rgm_rt_t *)NULL;
	char **rtnodes = (char **)NULL;
	boolean_t is_same = B_FALSE;
	ValueList tmp_list;
	scconf_errno_t scconf_err = SCCONF_NOERR;
	char *rt_part, *zc_part, *zc_name;

	// Initializa ORB and other cluster checks
	clerrno = cluster_init();
	if (clerrno) {
		return (clerrno);
	}

	// Check whether any zone cluster names were specified.
	// And if they were specified, there should be only zone cluster
	// across all the RGs.
	rt_part = zc_part = zc_name = NULL;
	clerrno = check_single_zc_specified(operands, &zc_name, &is_same);
	if (clerrno != CL_NOERR) {
		clcommand_perror(clerrno);
		return (clerrno);
	}
	// If multiple zone clusters were specified, report an error.
	if (is_same == B_FALSE) {
		clerror("This operation can be performed on only one "
		    "zone cluster at a time.\n");
		return (clerrno);
	}
#if (SOL_VERSION >= __s10)
	if (zc_name) {
		// If zone cluster name was specified, we have to ensure
		// that it is valid.
		// We have to check whether this operation is allowed for
		// the status of the zone cluster.
		clerrno = check_zc_operation(CL_OBJ_TYPE_RT,
						CL_OP_TYPE_REMOVE_NODE,
						zc_name);
		if (clerrno != CL_NOERR) {
			return (clerrno);
		}
	}
#endif

	tmp_list.add(zc_name);
	clerrno = get_good_nodes(nodes, good_nodes, tmp_list);
	tmp_list.clear();
	if (clerrno)
		return (clerrno);

	operands.start();
	oper1 = operands.getValue();
	if (strcmp(oper1, CLCOMMANDS_WILD_OPERAND)) {
		for (operands.start(); operands.end() != 1; operands.next()) {

			if (rt_part) {
				free(rt_part);
				rt_part = NULL;
			}
			if (zc_part) {
				free(zc_part);
				zc_part = NULL;
			}

			// First parse the RT name
			scconf_err = scconf_parse_rt_name_from_cluster_scope(
						operands.getValue(),
						&rt_part, &zc_part);
			if (scconf_err != SCCONF_NOERR) {
				clerrno = map_scconf_error(scconf_err);
				clcommand_perror(clerrno);
				if (first_err == CL_NOERR) {
					first_err = clerrno;
				}
				continue;
			}
			scha_status = rgmcnfg_get_rtrealname(rt_part,
						&r_rtname, zc_part);
			rv = scha_status.err_code;
			if (rv != SCHA_ERR_NOERR) {
				print_rgm_error(scha_status,
				    operands.getValue());
				clerror("Skipping resource type \"%s\".\n",
				    operands.getValue());
				if (first_err == 0) {
					// first error!
					first_err = map_scha_error(rv);
				}
			} else {
				good_rts.add(r_rtname);
			}
			if (r_rtname)
				free(r_rtname);
		}
		rt_names = vl_to_names(good_rts, &errflg);
		if (errflg)
			return (errflg);
	} else {
		scha_status = rgm_scrgadm_getrtlist(&rt_names, zc_name);
		rv = scha_status.err_code;
		if (rv != SCHA_ERR_NOERR) {
			print_rgm_error(scha_status, NULL);
			clerror("Failed to obtain list of all registered "
			    "resource types.\n");
			return (map_scha_error(rv));
		}
	}

	// Free memory
	if (rt_part) {
		free(rt_part);
		rt_part = NULL;
	}
	if (zc_part) {
		free(zc_part);
		zc_part = NULL;
	}

	if (rt_names == NULL)
		return (CL_NOERR);

	for (i = 0; rt_names[i] != NULL; i++) {

		if (rtnodes) {
			free(rtnodes);
			rtnodes = NULL;
		}
		if (node_nl) {
			rgm_free_nlist(node_nl);
			node_nl = NULL;
		}
		if (new_nodelist) {
			rgm_free_nlist(new_nodelist);
			new_nodelist = NULL;
		}

		// Get the RT details
		if (rt != (rgm_rt_t *)0) {
			rgm_free_rt(rt);
			rt = (rgm_rt_t *)0;
		}
		scha_status = rgm_scrgadm_getrtconf(rt_names[i], &rt, zc_name);
		rv = scha_status.err_code;
		if (rv != SCHA_ERR_NOERR) {
			print_rgm_error(scha_status, rt_names[i]);
			clerror("Skipping resource type \"%s\".\n",
			    rt_names[i]);
			if (first_err == 0) {
				// first error!
				first_err = map_scha_error(rv);
			}
			continue;
		}
		if (rt == NULL) {
			clerror("Failed to obtain resource type information "
			    "for \"%s\".\n", rt_names[i]);
			clerror("Skipping resource type \"%s\".\n",
			    rt_names[i]);
			if (first_err == 0) {
				// first error!
				first_err = CL_ENOENT;
			}
			continue;
		}

		//
		// If all nodes, this is not supported as RGM doesn't support
		// the semantics of all future nodes at the same time not all
		// current nodes.
		//
		if (rt->rt_instl_nodes.is_ALL_value) {
			clerror("Resource type \"%s\" is available on all "
			    "nodes of the cluster, including nodes that might "
			    "be added in future; use the \"set\" "
			    "subcommand to alter the node list for this "
			    "resource type.\n",
			    rt_names[i]);

			if (first_err == 0) {
				// first error!
				first_err = CL_EOP;
			}
			continue;
		}

		// convert rt nodeidlist to namelist
		node_nl = nodeidlist_to_namelist(rt->rt_instl_nodes.nodeids,
			    &errflg, zc_name);
		if (errflg) {
			rgm_free_rt(rt);
			return (errflg);
		}
		// Add new nodes to the nodelist
		new_nodelist = remove_vl_from_names(node_nl, good_nodes,
		    &errflg);
		if (errflg) {
			rgm_free_rt(rt);
			return (errflg);
		}

		rtnodes = namelist_to_names(new_nodelist, &errflg);
		if (errflg)
			return (errflg);

		// Figure out the number of nodes in the nodelists
		nl1 = new_nodelist;
		num_nodes = 0;
		while (nl1) {
			num_nodes++;
			nl1 = nl1->nl_next;
		}
		nl2 = node_nl;
		num_old_nodes = 0;
		while (nl2) {
			num_old_nodes++;
			nl2 = nl2->nl_next;
		}

		if (num_nodes == 0) {
			clerror("Nodelist can not be empty.\n");
			clerror("Operation not allowed, skipping "
			    "resource type \"%s\".\n", rt_names[i]);
			if (first_err == 0) {
				// first error!
				first_err = CL_EOP;
			}
			continue;
		}
		if (num_nodes !=
		    (num_old_nodes - good_nodes.getSize())) {
			clerror("One or more of the specified nodes "
			    "are not part of the node list of resource type "
			    "\"%s\", skipping \"%s\".\n", rt_names[i],
			    rt_names[i]);
			if (first_err == 0) {
				// first error!
				first_err = CL_EOP;
			}
			continue;
		}
		scha_status = rgm_scrgadm_update_rt_instnodes(rt_names[i],
		    rtnodes, zc_name);
		rv = scha_status.err_code;
		if (rv) {
			print_rgm_error(scha_status, rt_names[i]);
			if (first_err == 0) {
				// first error!
				first_err = map_scha_error(rv);
			}
		} else if (optflgs & vflg) {
			nl1 = new_nodelist;
			clmessage("New nodelist set for resource type "
			    "\"%s\": ", rt_names[i]);
			nl1 = new_nodelist;
			while (nl1) {
				fprintf(stdout, "%s ", nl1->nl_name);
				nl1 = nl1->nl_next;
			}
			fprintf(stdout, "\n");
		}
	}

	if (rt)
		rgm_free_rt(rt);
	if (new_nodelist)
		rgm_free_nlist(new_nodelist);
	if (node_nl)
		rgm_free_nlist(node_nl);
	rgm_free_strarray(rt_names);
	if (rtnodes)
		free(rtnodes);
	if (zc_name)
		free(zc_name);
	return (first_err);
}
