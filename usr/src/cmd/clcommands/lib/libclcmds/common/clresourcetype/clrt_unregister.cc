//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clrt_unregister.cc	1.8	08/08/11 SMI"

//
// Process clrt "unregister"
//

#include <sys/os.h>
#include <scadmin/scconf.h>
#include <scadmin/scswitch.h>
#include <scadmin/scstat.h>
#include <rgm/rgm_scrgadm.h>
#include <rgm/rgm_cnfg.h>

#include "clcommands.h"

#include "ClCommand.h"

#include "rgm_support.h"
#include "common.h"

//
// clrt "unregister"
//
clerrno_t
clrt_unregister(ClCommand &cmd, ValueList &operands, optflgs_t optflgs)
{
	clerrno_t first_err = CL_NOERR;
	int rv = 0;
	int i;
	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};
	char **rt_names = NULL;
	char *oper1;
	rgm_rg_t *rg = (rgm_rg_t *)0;
	namelist_t *new_nodelist;
	namelist_t *nl;
	namelist_t *nl1;
	namelist_t desired_nl;
	namelist_t maximum_nl;
	clerrno_t errflg = CL_NOERR;
	clerrno_t clerrno = CL_NOERR;
	ValueList good_nodes;
	ValueList good_rts;
	namelist_t *node_nl;
	char *r_rtname = (char *)NULL;
	rgm_rt_t *rt = (rgm_rt_t *)NULL;
	scconf_errno_t scconf_err = SCCONF_NOERR;
	boolean_t is_same = B_FALSE;
	char *zc_name, *rt_name, *tmpStr;
	char *tmp_operands = NULL, *tmp_zcname = NULL;

	// Initializa ORB and other cluster checks
	clerrno = cluster_init();
	if (clerrno) {
		return (clerrno);
	}
	zc_name = rt_name = tmpStr = NULL;

#if (SOL_VERSION >= __s10)
	if (optflgs & Zflg) {
		operands.start();
		tmp_operands = strdup(operands.getValue());
		tmp_zcname = strtok(tmp_operands, ":");
		clerrno = check_zone_cluster_exists(tmp_zcname);

		if (clerrno != CL_NOERR) {
			clerror("%s is not a valid zone cluster.\n",
				tmp_zcname);
			return (clerrno);
		}
	}
#endif


	// Check whether any zone cluster names were specified.
	// And if they were specified, there should be only zone cluster
	// across all the RGs.
	clerrno = check_single_zc_specified(operands, &zc_name, &is_same,
				scconf_parse_rt_name_from_cluster_scope);
	if (clerrno != CL_NOERR) {
		clcommand_perror(clerrno);
		return (clerrno);
	}
	// If multiple zone clusters were specified, report an error.
	if (is_same == B_FALSE) {
		clerror("This operation can be performed on only one "
		    "zone cluster at a time.\n");
		return (clerrno);
	}
#if (SOL_VERSION >= __s10)
	if (zc_name) {
		// If zone cluster name was specified, we have to ensure
		// that it is valid.
		// We have to check whether this operation is allowed for
		// the status of the zone cluster.
		clerrno = check_zc_operation(CL_OBJ_TYPE_RT,
						CL_OP_TYPE_UNREGISTER,
						zc_name);
		if (clerrno != CL_NOERR) {
			return (clerrno);
		}
	}
#endif

	operands.start();
	oper1 = operands.getValue();
	if (strcmp(oper1, CLCOMMANDS_WILD_OPERAND)) {
		for (operands.start(); operands.end() != 1; operands.next()) {
			// First get the RT name without the ZC name
			scconf_err = scconf_parse_rt_name_from_cluster_scope(
					operands.getValue(), &rt_name,
								&tmpStr);

			if (scconf_err != SCCONF_NOERR) {
				clerror("Internal error.\n");
				clerrno = CL_EINTERNAL;
				return (clerrno);
			}

			scha_status = rgmcnfg_get_rtrealname(rt_name,
						&r_rtname, zc_name);
			rv = scha_status.err_code;
			if (rv != SCHA_ERR_NOERR) {
				print_rgm_error(scha_status,
				    operands.getValue());
				clerror("Skipping resource type \"%s\".\n",
				    operands.getValue());
				if (first_err == 0) {
					// first error!
					first_err = map_scha_error(rv);
				}
			} else {
				good_rts.add(r_rtname);
			}
			// Free any memory
			if (r_rtname) {
				free(r_rtname);
				r_rtname = NULL;
			}
			if (rt_name) {
				free(rt_name);
				rt_name = NULL;
			}
			if (tmpStr) {
				free(tmpStr);
				tmpStr = NULL;
			}
		}
		rt_names = vl_to_names(good_rts, &errflg);
		if (errflg)
			return (errflg);
	} else {
		scha_status = rgm_scrgadm_getrtlist(&rt_names, zc_name);
		rv = scha_status.err_code;
		if (rv != SCHA_ERR_NOERR) {
			print_rgm_error(scha_status, NULL);
			clerror("Failed to obtain list of all registered "
			    "resource types.\n");
			return (map_scha_error(rv));
		}
	}

	if (rt_names == NULL)
		return (first_err);

	// All the RT names we have here are not scoped with the zone cluster
	// name.

	for (i = 0; rt_names[i] != NULL; i++) {

		scha_status = rgm_scrgadm_unregister_rt(rt_names[i], zc_name);
		rv = scha_status.err_code;
		if (rv != SCHA_ERR_NOERR) {
			print_rgm_error(scha_status, rt_names[i]);
			if (first_err == 0) {
				// first error!
				first_err = map_scha_error(rv);
			}
			continue;
		} else if (optflgs & vflg)
			clmessage("Unregistered \"%s\".\n", rt_names[i]);
	}

	rgm_free_strarray(rt_names);
	return (first_err);
}
