//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clrt_show.cc	1.15	08/10/13 SMI"

//
// Process clrt "show"
//

#include <sys/os.h>
#include <scadmin/scconf.h>
#include <scadmin/scswitch.h>
#include <scadmin/scstat.h>
#include <rgm/rgm_scrgadm.h>
#include <rgm/rgm_cnfg.h>

#include "clcommands.h"

#include "ClCommand.h"
#include "ClShow.h"

#include "rgm_support.h"
#include "common.h"

static clerrno_t
get_clrt_show_obj_select(ValueList &operands, optflgs_t optflgs,
    ValueList &nodes, ClShow *rt_show, ValueList &zc_names = ValueList());

static char *
get_upg_tunability(rgm_tune_t rtu_tunability);

static char *
get_fom_name_list(void);

static void
add_param_objs(rgm_param_t *entry, ClShow *param_show);

//
// clrt "show"
//
clerrno_t
clrt_show(ClCommand &cmd, ValueList &operands, optflgs_t optflgs,
    ValueList &nodes, ValueList &zc_names)
{
	clerrno_t clerrno = CL_NOERR;
	ClShow *rt_show = new ClShow(HEAD_RESTYPES);

	// Initializa ORB and other cluster check
	clerrno = cluster_init();
	if (clerrno)
		return (clerrno);

	clerrno = get_clrt_show_obj_select(operands, optflgs, nodes, rt_show,
						zc_names);
	(void) rt_show->print();
	return (clerrno);
}

//
// clrt show for cluster command
//
clerrno_t
get_clrt_show_obj(optflgs_t optflgs, ClShow *rt_show)
{
	clerrno_t clerrno = CL_NOERR;
	ValueList rt_list = ValueList(true);
	ValueList empty_list;

	rt_list.add(CLCOMMANDS_WILD_OPERAND);
	clerrno = get_clrt_show_obj_select(rt_list, optflgs, empty_list,
	    rt_show);
	return (clerrno);
}

//
// Fetch the show buffer according to options/operands.
//
static clerrno_t
get_clrt_show_obj_select(ValueList &operands, optflgs_t optflgs,
    ValueList &nodes, ClShow *rt_show, ValueList &zc_names)
{
	clerrno_t first_err = CL_NOERR;
	int rv = 0;
	int i, k;
	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};
	char **rt_names = NULL;
	char *oper1;
	rgm_rg_t *rg = (rgm_rg_t *)0;
	namelist_t *new_nodelist;
	namelist_t *nl;
	namelist_t *nl1;
	namelist_t desired_nl;
	namelist_t maximum_nl;
	clerrno_t errflg = CL_NOERR;
	clerrno_t clerrno = CL_NOERR;
	ValueList good_nodes, valid_zc_list, tmp_list;
	ValueList good_rts;
	namelist_t *node_nl;
	namelist_t *tmp_nl;
	int node_found;
	char *r_rtname = (char *)NULL;
	rgm_rt_t *rt = (rgm_rt_t *)NULL;
	int verbose = 0;
	int num_rts = 0;
	NameValueList showdata;
	ClShow **meth_ptr = (ClShow **)NULL;
	ClShow **upg_ptr = (ClShow **)NULL;
	ClShow **param_ptr = (ClShow **)NULL;
	char *new_rt, *new_entry;
	int wildcard = 0;
	char *ptr;
	char value[BUFSIZ];
	char label[BUFSIZ];
	char *node_str = (char *)NULL;
	char *pkg_str = (char *)NULL;
	rgm_rt_upgrade_t *upgp;
	char *version;
	rgm_methods_t meth;
	rgm_param_t **tablep;
	scconf_errno_t scconf_err = SCCONF_NOERR;
	char *rt_name, *zc_name, *fullname;

	// Initializa ORB and other cluster checks
	clerrno = cluster_init();
	if (clerrno) {
		return (clerrno);
	}

	if (optflgs & vflg)
		verbose = 1;

	rt_name = zc_name = fullname = NULL;
	// Zone cluster names can be specified either with the -Z
	// option or in the form of <zonecluster>:<RT name> format.
	// But, both the ways cannot be used simultaneously.
	// If zone clusters were not specified with the -Z option, we
	// have to check whether they were specified in the <zc>:<rt>
	// format.
	for (operands.start(); !operands.end(); operands.next()) {
		scconf_err = scconf_parse_rt_name_from_cluster_scope(
				operands.getValue(), &rt_name, &zc_name);

		if (scconf_err != SCCONF_NOERR) {
			return (CL_ENOMEM);
		}

		if (zc_name && zc_names.getSize()) {
			// -Z flag was specified and this RT was scoped
			// with a zone cluster name as well. Report an
			// error.
			clerror("You cannot use the "
				"\"<zonecluster>:<rtname>\" "
				"form of the resource-type name with the "
				"\"-Z\" option.");
			free(rt_name);
			free(zc_name);
			return (CL_EINVAL);
		}

		// If this zone cluster name does not exist in tmp_list,
		// then we will add it here.
		if (!tmp_list.isValue(zc_name)) {
			tmp_list.add(zc_name);
		}

		if (rt_name) {
			free(rt_name);
			rt_name = NULL;
		}
		if (zc_name) {
			free(zc_name);
			zc_name = NULL;
		}
	}
#if (SOL_VERSION >= __s10)
	// By the time we are here, we have added any zone cluster name in the
	// RT names to tmp_list.
	if (zc_names.getSize()) {
		// Zc names were specified. We have to check whether "all"
		// was specified.
		zc_names.start();

		if (strcmp(zc_names.getValue(), ALL_CLUSTERS_STR) == 0) {
			// Get all known zone clusters.
			clerrno = get_all_clusters(valid_zc_list);

			if (clerrno != CL_NOERR) {
				return (clerrno);
			}
		} else {
			tmp_list = zc_names;
		}
	}

	// valid_zc_list will contain at least one zone cluster name
	// if "all" was specified. If it is empty, we have to check
	// the validity of the zone clusters specified by the user.
	if (valid_zc_list.getSize() < 1) {
		clerrno = get_valid_zone_clusters(tmp_list, valid_zc_list);
	}
	// Clear the memory.
	tmp_list.clear();

	if (clerrno) {
		// There was at least one invalid zone cluster.
		// We have to return.
		return (clerrno);
	}
#endif

	clerrno = get_good_nodes(nodes, good_nodes, valid_zc_list);
	if (clerrno) {
		if (first_err == 0) {
			// first error!
			first_err = clerrno;
		}
	}
	if ((nodes.getSize() > 0) && (good_nodes.getSize() == 0)) {
		return (first_err);
	}


	if (operands.getSize() > 0) {
		operands.start();
		oper1 = operands.getValue();
		if (strcmp(oper1, CLCOMMANDS_WILD_OPERAND) == 0)
			wildcard = 1;
	} else
		wildcard = 1;

	if (!wildcard) {
		for (operands.start(); operands.end() != 1; operands.next()) {

			// First parse the RT name from zone cluster scope
			if (rt_name) {
				free(rt_name);
				rt_name = NULL;
			}
			if (zc_name) {
				free(zc_name);
				zc_name = NULL;
			}
			if (r_rtname)
				free(r_rtname);

			scconf_err = scconf_parse_rt_name_from_cluster_scope(
				operands.getValue(), &rt_name, &zc_name);

			if (scconf_err != SCCONF_NOERR) {
				if (first_err == CL_NOERR) {
					first_err = map_scconf_error(
							scconf_err);
				}
				continue;
			}

			// If zone cluster names were not specified
			// using the -Z flag, they could have been specified
			// as part of the RT name also.
			if (zc_names.getSize() == 0) {
				// -Z flag was not used.
				scha_status = rgmcnfg_get_rtrealname(
				    rt_name, &r_rtname, zc_name);
				rv = scha_status.err_code;
				if (rv != SCHA_ERR_NOERR) {
					print_rgm_error(scha_status,
					    operands.getValue());
					clerror("Skipping resource type "
					    "\"%s\".\n", operands.getValue());
					if (first_err == 0) {
						// first error!
						first_err = map_scha_error(rv);
					}
				} else {
					// Now after getting the real rtname
					// append the zone cluster name to the
					// real name and add to the good_rts
					scconf_err =
					scconf_add_cluster_scope_to_obj_name(
						r_rtname, zc_name,
						&fullname);
					if (scconf_err != SCCONF_NOERR) {
						clerrno = map_scconf_error
							(scconf_err);
						clcommand_perror(clerrno);

						if (first_err == CL_NOERR) {
							first_err = clerrno;
						}

						continue;
					}

					good_rts.add(fullname);
				}

				continue;
			}

#if (SOL_VERSION >= __s10)
			// If we are here, it means zone clusters were
			// specified using the -Z flag.
			tmp_list.clear();
			clerrno = get_zone_clusters_with_rt(rt_name, tmp_list,
								valid_zc_list);
			if (clerrno != CL_NOERR) {

				if (clerrno == CL_ENOENT) {
					clerror("Skipping resource type "
					    "\"%s\".\n", operands.getValue());
				} else {
					clcommand_perror(clerrno);
				}

				if (first_err == CL_NOERR) {
					first_err = clerrno;
				}
				continue;
			}
#endif

			// We have to scope this RT name with the
			// zone cluster name
			for (tmp_list.start(); !tmp_list.end();
				tmp_list.next()) {

				if (fullname) {
					free(fullname);
					fullname = NULL;
				}

				scha_status = rgmcnfg_get_rtrealname(
				    rt_name, &r_rtname, tmp_list.getValue());
				rv = scha_status.err_code;
				if (rv != SCHA_ERR_NOERR) {
					print_rgm_error(scha_status,
					    operands.getValue());
					clerror("Skipping resource type "
					    "\"%s\".\n", operands.getValue());
					if (first_err == 0) {
						// first error!
						first_err = map_scha_error(rv);
					}
				} else {
					// Now after getting the real rtname
					// append the zone cluster name to the
					// real name and add to the good_rts
					scconf_err =
					scconf_add_cluster_scope_to_obj_name(
						r_rtname, tmp_list.getValue(),
						&fullname);
					if (scconf_err != SCCONF_NOERR) {
						clerrno = map_scconf_error
							(scconf_err);
						clcommand_perror(clerrno);

						if (first_err == CL_NOERR) {
							first_err = clerrno;
						}

						continue;
					}

					good_rts.add(fullname);
				}
			}

			if (fullname) {
				free(fullname);
				fullname = NULL;
			}

		}

		rt_names = vl_to_names(good_rts, &errflg);
		if (errflg)
			return (errflg);
	} else {

		if (zc_names.getSize() == 0) {
			// Zone cluster names were not specified. Get the
			// RT names from the current cluster itself.
			scha_status = rgm_scrgadm_getrtlist(&rt_names, NULL);
			rv = scha_status.err_code;
			if (rv != SCHA_ERR_NOERR) {
				print_rgm_error(scha_status, NULL);
				clerror("Failed to obtain list of all "
				    "registered resource types.\n");
				return (map_scha_error(rv));
			}
		} else {
			// Zone cluster names were specified. We have to fetch
			// RT names from all the zone clusters.
			tmp_list.clear();
			for (valid_zc_list.start(); !valid_zc_list.end();
				valid_zc_list.next()) {
				clerrno = get_all_rt_from_zc(tmp_list,
						valid_zc_list.getValue(),
						B_TRUE);
				if (clerrno != CL_NOERR) {
					if (first_err == CL_NOERR) {
						first_err = clerrno;
					}
					continue;
				}
			}

			// Now, get back all the RT names from the valuelist
			rt_names = vl_to_names(tmp_list, &errflg);
			// Free memory
			tmp_list.clear();
			if (errflg)
				return (errflg);

		}
	}

	// Free memory
	if (fullname) {
		free(fullname);
			fullname = NULL;
	}
	if (rt_name) {
		free(rt_name);
		rt_name = NULL;
	}
	if (zc_name) {
		free(zc_name);
		zc_name = NULL;
	}
	if (r_rtname)
		free(r_rtname);

	if (rt_names == NULL)
		return (CL_NOERR);

	// By the time we are here, we have verified all the RT names specified
	// and all the RT names are in the format <ZC name>:<RT name>

	for (i = 0; rt_names[i] != NULL; i++)
		num_rts++;

	if (verbose) {
		meth_ptr = (ClShow **) calloc(num_rts, sizeof (ClShow *));
		if (meth_ptr == (ClShow **) NULL) {
			clcommand_perror(CL_ENOMEM);
			return (CL_ENOMEM);
		}
		upg_ptr = (ClShow **) calloc(num_rts, sizeof (ClShow *));
		if (upg_ptr == (ClShow **) NULL) {
			clcommand_perror(CL_ENOMEM);
			return (CL_ENOMEM);
		}
		param_ptr = (ClShow **) calloc(num_rts, sizeof (ClShow *));
		if (param_ptr == (ClShow **) NULL) {
			clcommand_perror(CL_ENOMEM);
			return (CL_ENOMEM);
		}
	}

	k = 0;
	for (i = 0; rt_names[i] != NULL; i++) {

		if (rt != (rgm_rt_t *)0) {
			rgm_free_rt(rt);
			rt = (rgm_rt_t *)0;
		}
		if (node_str != (char *)NULL) {
			free(node_str);
			node_str = (char *)NULL;
		}
		if (pkg_str != (char *)NULL) {
			free(pkg_str);
			pkg_str = (char *)NULL;
		}
		if (rt_name) {
			free(rt_name);
			rt_name = NULL;
		}
		if (zc_name) {
			free(zc_name);
			zc_name = NULL;
		}
		// First parse the RT name from cluster scope
		scconf_err = scconf_parse_rt_name_from_cluster_scope(
				rt_names[i], &rt_name, &zc_name);

		if (scconf_err != SCCONF_NOERR) {
			if (first_err == CL_NOERR) {
				first_err = map_scconf_error(scconf_err);
			}
			continue;
		}
		// Get the RT details
		scha_status = rgm_scrgadm_getrtconf(rt_name, &rt, zc_name);
		rv = scha_status.err_code;
		if (rv != SCHA_ERR_NOERR) {
			print_rgm_error(scha_status, rt_names[i]);
			clerror("Skipping resource type \"%s\".\n",
			    rt_names[i]);
			if (first_err == 0) {
				// first error!
				first_err = map_scha_error(rv);
			}
			continue;
		}
		if (rt == NULL) {
			clerror("Failed to obtain resource type information "
			    "for \"%s\".\n", rt_names[i]);
			clerror("Skipping resource type \"%s\".\n",
			    rt_names[i]);
			if (first_err == 0) {
				// first error!
				first_err = CL_ENOENT;
			}
			continue;
		}
		if (!rt->rt_instl_nodes.is_ALL_value) {
			node_nl = nodeidlist_to_namelist(
			    rt->rt_instl_nodes.nodeids, &errflg, zc_name);
			if (errflg) {
				clerror("Failed to access information for "
				    "resource type \"%s\".\n", rt_names[i]);
				if (first_err == 0) {
					// first error!
					first_err = errflg;
				}
				continue;
			}
			node_str = names_to_str(node_nl, &errflg);
			if (errflg) {
				clerror("Failed to access information for "
				    "resource type \"%s\".\n", rt_names[i]);
				if (first_err == 0) {
					// first error!
					first_err = errflg;
				}
				continue;
			}
			// Do the filtering too
			if (good_nodes.getSize() > 0) {
				node_found = 0;
				for (good_nodes.start(); good_nodes.end() != 1;
				    good_nodes.next()) {
					tmp_nl = node_nl;
					while (tmp_nl) {
						if (strcmp(tmp_nl->nl_name,
						    good_nodes.getValue())
						    == 0) {
							node_found = 1;
							break;
						}
						tmp_nl = tmp_nl->nl_next;
					}
				}
				if (node_found == 0) {
					if (wildcard)
						continue;
					if (first_err == 0) {
						// first error!
						first_err = CL_EINVAL;
					}
					clerror("Specified resource type "
					    "\"%s\" does not meet the criteria "
					    "set by the \"-n\" option.\n");
					continue;
				}
			}
			rgm_free_nlist(node_nl);
		}
		if (rt->rt_pkglist) {
			pkg_str = names_to_str(rt->rt_pkglist, &errflg);
			if (errflg) {
				clerror("Failed to access information for "
				    "resource type \"%s\".\n", rt_names[i]);
				if (first_err == 0) {
					// first error!
					first_err = errflg;
				}
				continue;
			}
		}

		showdata.add("rt_name", rt_names[i]);

		ptr = (rt->rt_description && *rt->rt_description) ?
		    rt->rt_description : VALUE_STR_NULL;
		showdata.add("RT_description", ptr);

		ptr = (rt->rt_version && *rt->rt_version)
		    ? rt->rt_version : VALUE_STR_NULL;
		showdata.add("RT_version", ptr);

		(void) sprintf(value, "%d", rt->rt_api_version);
		showdata.add("API_version", value);

		ptr = (rt->rt_basedir && rt->rt_basedir)
		    ? rt->rt_basedir : VALUE_STR_NULL;
		showdata.add("RT_basedir", ptr);

		ptr = (rt->rt_single_inst == B_FALSE)
		    ? VALUE_STR_FALSE : VALUE_STR_TRUE;
		showdata.add("Single_instance", ptr);

		ptr = (rt->rt_proxy == B_FALSE)
		    ? VALUE_STR_FALSE : VALUE_STR_TRUE;
		showdata.add("Proxy", ptr);

		switch (rt->rt_init_nodes) {
		case SCHA_INFLAG_RG_PRIMARIES:
			ptr = "All potential masters";
			break;
		case SCHA_INFLAG_RT_INSTALLED_NODES:
			ptr = "All nodes";
		default:
			ptr = VALUE_STR_UNKNOWN;
			break;
		}
		showdata.add("Init_nodes", ptr);

		if (rt->rt_instl_nodes.is_ALL_value)
			ptr = VALUE_STR_ALL;
		else
			ptr = node_str;
		showdata.add("Installed_nodes", ptr);

		ptr = (rt->rt_failover == B_FALSE)
		    ? VALUE_STR_FALSE : VALUE_STR_TRUE;
		showdata.add("Failover", ptr);

		if (rt->rt_pkglist) {
			if (pkg_str)
				ptr = pkg_str;
			else
				ptr = VALUE_STR_NULL;
		} else
			ptr = VALUE_STR_NULL;
		showdata.add("Pkglist", ptr);

		ptr = (rt->rt_system == B_TRUE)
		    ? VALUE_STR_TRUE : VALUE_STR_FALSE;
		showdata.add("RT_system", ptr);

#if SOL_VERSION >= __s10
		ptr = (rt->rt_globalzone == B_TRUE)
		    ? VALUE_STR_TRUE : VALUE_STR_FALSE;
		showdata.add("Global_zone", ptr);
#endif

		if (!verbose)
			continue;

		ClShow *upg_show = (ClShow *)NULL;
		if (!rt->rt_sc30) {
			upg_show = new ClShow(SUBHEAD_UPGTUN, rt_names[i]);
			if (rt->rt_upgrade_from) {
				for (upgp = rt->rt_upgrade_from; upgp;
				    upgp = upgp->rtu_next) {
					if (strlen(upgp->rtu_version) == 0)
						version = "<unset_version>";
					else
						version = upgp->rtu_version;
					(void) sprintf(label, "%s %s",
					    gettext("Upgrade from"), version);

					upg_show->addNumberedObject(label,
					    get_upg_tunability(
					    upgp->rtu_tunability), 0);
				}
			}
			if (rt->rt_downgrade_to) {
				for (upgp = rt->rt_downgrade_to; upgp;
				    upgp = upgp->rtu_next) {
					if (strlen(upgp->rtu_version) == 0)
						version = "<unset_version>";
					else
						version = upgp->rtu_version;
					(void) sprintf(label, "%s %s",
					    gettext("Downgrade to"), version);

					upg_show->addNumberedObject(label,
					    get_upg_tunability(
					    upgp->rtu_tunability), 0);
				}
			}
		}
		upg_ptr[k] = upg_show;

		ClShow *meth_show = new ClShow(SUBHEAD_RTMETHODS, rt_names[i]);
		meth = rt->rt_methods;

		if (meth.m_start && *meth.m_start)
			meth_show->addNumberedObject("START", meth.m_start, 0);

		if (meth.m_stop && *meth.m_stop)
			meth_show->addNumberedObject("STOP", meth.m_stop, 0);

		if (meth.m_validate && *meth.m_validate)
			meth_show->addNumberedObject("VALIDATE",
			    meth.m_validate, 0);

		if (meth.m_update && *meth.m_update)
			meth_show->addNumberedObject("UPDATE", meth.m_update,
			    0);

		if (meth.m_init && *meth.m_init)
			meth_show->addNumberedObject("INIT", meth.m_init, 0);

		if (meth.m_fini && *meth.m_fini)
			meth_show->addNumberedObject("FINI", meth.m_fini, 0);

		if (meth.m_boot && *meth.m_boot)
			meth_show->addNumberedObject("BOOT", meth.m_boot, 0);

		if (meth.m_monitor_start && *meth.m_monitor_start)
			meth_show->addNumberedObject("MONITOR START",
			    meth.m_monitor_start, 0);

		if (meth.m_monitor_stop && *meth.m_monitor_stop)
			meth_show->addNumberedObject("MONITOR STOP",
			    meth.m_monitor_stop, 0);

		if (meth.m_monitor_check && *meth.m_monitor_check)
			meth_show->addNumberedObject("MONITOR CHECK",
			    meth.m_monitor_check, 0);

		if (meth.m_prenet_start && *meth.m_prenet_start)
			meth_show->addNumberedObject("PRENET START",
			    meth.m_prenet_start, 0);

		if (meth.m_postnet_stop && *meth.m_postnet_stop)
			meth_show->addNumberedObject("POSTNET STOP",
			    meth.m_postnet_stop, 0);

		meth_ptr[k] = meth_show;

		ClShow *param_show = new ClShow(SUBHEAD_RTPARAMS, rt_names[i]);
		for (tablep = rt->rt_paramtable; *tablep && (*tablep)->p_name;
		    ++tablep)
			add_param_objs(*tablep, param_show);

		param_ptr[k] = param_show;

		k++;
		showdata.add("rt_end", rt_names[i]);
	}
	if (showdata.getSize() > 0) {
		k = 0;
		for (showdata.start(); showdata.end() != 1; showdata.next()) {
			new_entry = showdata.getName();
			if (strcmp(new_entry, "rt_name") == 0) {
				new_rt = showdata.getValue();
				rt_show->addObject(
				    (char *)gettext("Resource Type"),
				    new_rt);
			} else if (strcmp(new_entry, "rt_end") == 0) {
				if (upg_ptr[k] != (void *) NULL)
					rt_show->addChild(new_rt, (ClShow *)
					    upg_ptr[k]);
				if (meth_ptr[k] != (void *) NULL)
					rt_show->addChild(new_rt, (ClShow *)
					    meth_ptr[k]);
				if (param_ptr[k] != (void *) NULL)
					rt_show->addChild(new_rt, (ClShow *)
					    param_ptr[k]);
					k++;
			} else {
				rt_show->addProperty(new_rt, showdata.getName(),
				    showdata.getValue());
			}
		}
	}

	if (meth_ptr)
		free(meth_ptr);
	if (upg_ptr)
		free(upg_ptr);
	if (param_ptr)
		free(param_ptr);
	if (rt)
		rgm_free_rt(rt);
	if (node_str)
		free(node_str);
	if (pkg_str)
		free(pkg_str);
	if (rt_name) {
		free(rt_name);
		rt_name = NULL;
	}
	if (zc_name) {
		free(zc_name);
		zc_name = NULL;
	}
	rgm_free_strarray(rt_names);
	return (first_err);
}

static char *
get_upg_tunability(rgm_tune_t rtu_tunability)
{
	char *ptr;

	switch (rtu_tunability) {
	case TUNE_ANYTIME:
		ptr = "Anytime";
		break;

	case TUNE_AT_CREATION:
		ptr = "At creation";
		break;

	case TUNE_WHEN_DISABLED:
		ptr = "When disabled";
		break;

	case TUNE_WHEN_OFFLINE:
		ptr = "When offline";
		break;

	case TUNE_WHEN_UNMANAGED:
		ptr = "When unmanaged";
		break;

	case TUNE_WHEN_UNMONITORED:
		ptr = "When unmonitored";
		break;

	case TUNE_NONE:
	default:
		ptr = VALUE_STR_UNKNOWN;
		break;
	}
	return (ptr);
}

static void
add_param_objs(rgm_param_t *entry, ClShow *param_show)
{
	char *ptr;
	char *param_name;
	char label[BUFSIZ];
	char value[BUFSIZ];
	int errflg = 0;

	ptr = (entry->p_name && *entry->p_name)
	    ? entry->p_name : VALUE_STR_NULL;

	param_name = ptr;

	param_show->addObject((char *)gettext("Param name"), param_name);

	/* Param table extension */
	ptr = (entry->p_extension == B_FALSE)
	    ? VALUE_STR_FALSE : VALUE_STR_TRUE;
	param_show->addProperty(param_name, (char *)gettext("Extension"), ptr);

	/* Per-node */
	ptr = (entry->p_per_node == B_FALSE)
	    ? VALUE_STR_FALSE : VALUE_STR_TRUE;
	param_show->addProperty(param_name,
	    (char *)gettext("Per-node"), ptr);

	/* Param table description */
	ptr = (entry->p_description && *entry->p_description)
	    ? entry->p_description : VALUE_STR_NULL;
	param_show->addProperty(param_name,
	    (char *)gettext("Description"), ptr);

	/* Param table tunability */
	switch (entry->p_tunable) {
	case TUNE_NONE:
		ptr = "Not tunable";
		break;

	case TUNE_AT_CREATION:
		ptr = "At creation";
		break;

	case TUNE_ANYTIME:
		ptr = "Anytime";
		break;

	case TUNE_WHEN_DISABLED:
		ptr = "When disabled";
		break;

	case TUNE_WHEN_OFFLINE:
	case TUNE_WHEN_UNMANAGED:
	case TUNE_WHEN_UNMONITORED:
	default:
		ptr = VALUE_STR_UNKNOWN;
		break;

	}
	param_show->addProperty(param_name,
	    (char *)gettext("Tunability"), ptr);

	/* Param table type */
	switch (entry->p_type) {
	case SCHA_PTYPE_STRING:

		/* type string */
		param_show->addProperty(param_name,
		    (char *)gettext("Type"), "String");

		/* print min length */
		if (entry->p_min_isset) {
			(void) sprintf(value, "%d", entry->p_min);
			param_show->addProperty(param_name,
			    (char *)gettext("Min length"), value);
		} else {
			param_show->addProperty(param_name,
			    (char *)gettext("Min length"),
			    VALUE_STR_UNSET);
		}

		/* print max length */
		if (entry->p_max_isset && entry->p_max < MAXRGMVALUESLEN) {
			(void) sprintf(value, "%d", entry->p_max);
			param_show->addProperty(param_name,
			    (char *)gettext("Max length"), value);
		} else {
			param_show->addProperty(param_name,
			    (char *)gettext("Max length"),
			    VALUE_STR_UNSET);
		}

		/* print default */
		if (entry->p_default_isset) {
			ptr = (entry->p_defaultstr && *entry->p_defaultstr)
			    ? entry->p_defaultstr : VALUE_STR_NULL;
		} else {
			ptr = VALUE_STR_UNSET;
		}
		param_show->addProperty(param_name,
		    (char *)gettext("Default"), ptr);

		break;

	case SCHA_PTYPE_INT:

		/* type int */
		param_show->addProperty(param_name,
		    (char *)gettext("Type"), "Int");

		/* print min int type */
		if (entry->p_min_isset) {
			(void) sprintf(value, "%d", entry->p_min);
			param_show->addProperty(param_name,
			    (char *)gettext("Min int value"), value);
		} else {
			param_show->addProperty(param_name,
			    (char *)gettext("Min int value"),
			    VALUE_STR_UNSET);
		}

		/* print max int type */
		if (entry->p_max_isset && entry->p_max < MAXRGMVALUESLEN) {
			(void) sprintf(value, "%d", entry->p_max);
			param_show->addProperty(param_name,
			    (char *)gettext("Max int value"), value);
		} else {
			param_show->addProperty(param_name,
			    (char *)gettext("Max int value"),
			    VALUE_STR_UNSET);
		}

		/* print default */
		if (entry->p_default_isset) {
			(void) sprintf(value, "%d", entry->p_defaultint);
			param_show->addProperty(param_name,
			    (char *)gettext("Default"),
			    value);
		} else {
			param_show->addProperty(param_name,
			    (char *)gettext("Default"),
			    VALUE_STR_UNSET);
		}

		break;

	case SCHA_PTYPE_BOOLEAN:

		/* type boolean */
		param_show->addProperty(param_name,
		    (char *)gettext("Type"), "Boolean");

		/* print default */
		if (entry->p_default_isset) {
			ptr = (entry->p_defaultbool == B_FALSE)
			    ? VALUE_STR_FALSE : VALUE_STR_TRUE;
		} else {
			ptr = VALUE_STR_UNSET;
		}
		param_show->addProperty(param_name,
		    (char *)gettext("Default"), ptr);

		break;

	case SCHA_PTYPE_ENUM:

		/* type enum */
		param_show->addProperty(param_name,
		    (char *)gettext("Type"), "Enum");

		/* print enum list */
		if (entry->p_name &&
		    (strcmp(SCHA_FAILOVER_MODE, entry->p_name) == 0)) {
			/*
			 * the failover_mode system property doesn't
			 * use the enumlist any more.
			 * we fake it.
			 */
			ptr = get_fom_name_list();
		} else {
			ptr = names_to_str(entry->p_enumlist, &errflg);
		}
		if (ptr) {
			param_show->addProperty(param_name,
			    (char *)gettext("Enum list"), ptr);
			free(ptr);
		}

		/* print default */
		if (entry->p_default_isset) {
			ptr = entry->p_defaultstr;
			if (ptr) {
				param_show->addProperty(param_name,
				    (char *)gettext("Default"),
				    entry->p_defaultstr);
			} else {
				param_show->addProperty(param_name,
				    (char *)gettext("Default"),
				    VALUE_STR_NULL);
			}
		} else {
			param_show->addProperty(param_name,
			    (char *)gettext("Default"),
			    VALUE_STR_UNSET);
		}

		break;

	case SCHA_PTYPE_STRINGARRAY:

		/* type stringarray */
		param_show->addProperty(param_name,
		    (char *)gettext("Type"),
		    "Stringarray");

		/* print min length */
		if (entry->p_min_isset) {
			(void) sprintf(value, "%d", entry->p_min);
			param_show->addProperty(param_name,
			    (char *)gettext("Min length"), value);
		} else {
			param_show->addProperty(param_name,
			    (char *)gettext("Min length"),
			    VALUE_STR_UNSET);
		}

		/* print max length */
		if (entry->p_max_isset && entry->p_max < MAXRGMVALUESLEN) {
			(void) sprintf(value, "%d", entry->p_max);
			param_show->addProperty(param_name,
			    (char *)gettext("Max length"), value);
		} else {
			param_show->addProperty(param_name,
			    (char *)gettext("Max length"),
			    VALUE_STR_UNSET);
		}

		/* print min array length */
		if (entry->p_arraymin_isset) {
			(void) sprintf(value, "%d", entry->p_arraymin);
			param_show->addProperty(param_name,
			    (char *)gettext("Min array length"), value);
		} else {
			param_show->addProperty(param_name,
			    (char *)gettext("Min array length"),
			    VALUE_STR_UNSET);
		}

		/* print max array length */
		if (entry->p_arraymax_isset) {
			(void) sprintf(value, "%d", entry->p_arraymax);
			param_show->addProperty(param_name,
			    (char *)gettext("Max array length"), value);
		} else {
			param_show->addProperty(param_name,
			    (char *)gettext("Max array length"),
			    VALUE_STR_UNSET);
		}

		/* print default */
		if (entry->p_default_isset) {
			ptr = names_to_str(entry->p_defaultarray, &errflg);
			if (ptr) {
				param_show->addProperty(param_name,
				    (char *)gettext("Default"), ptr);
				free(ptr);
			} else {
				param_show->addProperty(param_name,
				    (char *)gettext("Default"),
				    VALUE_STR_NULL);
			}
		} else {
			param_show->addProperty(param_name,
			    (char *)gettext("Default"),
			    VALUE_STR_UNSET);
		}
		break;

	default:

		// type unknown
		param_show->addProperty(param_name,
		    (char *)gettext("Type"),
		    VALUE_STR_UNKNOWN);

		break;
	}
}

//
// get_fom_name_list: This should be added to the rgm_scrgadm library.
//
// return a single string with all the possible values for
// Failover_mode
// Since the introduction of the RESTART_ONLY and LOG_ONLY values
// for Failover_mode, the enumlist is no longer used
//
static char *
get_fom_name_list(void)
{
	int	n, i;
	size_t	len = 0;
	char	*str;
	char	*fom_values[] = { SCHA_NONE, SCHA_HARD, SCHA_SOFT,
	    SCHA_RESTART_ONLY, SCHA_LOG_ONLY };

	n = sizeof (fom_values) / sizeof (char *);

	for (i = 0; i < n; i++) {
		len += strlen(fom_values[i]) + 1;
	}

	/* allocate the buffer. calloc() zeroes everything */
	if ((str = (char *)calloc(1, len + 1)) == NULL) {
		return (NULL);
	}

	for (i = 0; i < n; i++) {
		/* add space sepator, except for the first iteration */
		if (*str != '\0') {
			(void) strcat(str, " ");
		}

		(void) strcat(str, fom_values[i]);
	}

	return (str);
}
