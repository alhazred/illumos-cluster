//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clrt_set.cc	1.7	08/07/24 SMI"

//
// Process clrt "set"
//

#include <sys/os.h>
#include <scadmin/scconf.h>
#include <scadmin/scswitch.h>
#include <scadmin/scstat.h>
#include <rgm/rgm_scrgadm.h>
#include <rgm/rgm_cnfg.h>

#include "clcommands.h"

#include "ClCommand.h"

#include "rgm_support.h"
#include "common.h"

//
// clrt "set"
//
clerrno_t
clrt_set(ClCommand &cmd, ValueList &operands, optflgs_t optflgs,
    NameValueList &properties, ValueList &nodes)
{
	clerrno_t first_err = 0;
	int rv = 0;
	int i;
	int prop_len = 0;
	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};
	char **rt_names = NULL;
	char *oper1;
	namelist_t *new_nl = (namelist_t *)NULL;
	namelist_t *nl_elem;
	clerrno_t errflg = CL_NOERR;
	clerrno_t clerrno = CL_NOERR;
	ValueList good_nodes;
	ValueList good_rts;
	namelist_t *node_nl = (namelist_t *)NULL;
	char *r_rtname = (char *)NULL;
	rgm_rt_t *rt = (rgm_rt_t *)NULL;
	char **rtnodes = (char **)NULL;
	int wildcard = 0;
	boolean_t system_specified = B_FALSE;
	boolean_t nodes_prop_specified = B_FALSE;
	boolean_t system_val, is_same;
	namelist_t *nl_nodes = (namelist_t *)NULL;
	namelist_t *tmp_nl_nodes = (namelist_t *)NULL;
	ValueList nodes_prop, tmp_list;
	boolean_t add_form = B_FALSE, rm_form = B_FALSE;
	boolean_t node_incr = B_FALSE, node_decr = B_FALSE;
	namelist_t *nl_start = (namelist_t *)NULL;
	namelist_t *nl_post, *nl_prev;
	int num_nodes, num_old_nodes;
	char *rt_part, *zc_part, *zc_name;
	scconf_errno_t scconf_err = SCCONF_NOERR;

	// Initializa ORB and other cluster checks

	// Initializa ORB and other cluster checks
	clerrno = cluster_init();
	if (clerrno) {
		return (clerrno);
	}

	for (properties.start(); properties.end() != 1; properties.next()) {
		add_form = B_FALSE;
		rm_form = B_FALSE;
		if (strchr(properties.getName(), '+') != NULL)
			add_form = B_TRUE;
		if (strchr(properties.getName(), '-') != NULL)
			rm_form = B_TRUE;
		if (add_form || rm_form)
			prop_len = strlen(properties.getName()) - 1;
		else
			prop_len = strlen(properties.getName());

		if (strncasecmp(properties.getName(), SCHA_RT_SYSTEM,
		    prop_len) == 0) {
			if (add_form || rm_form) {
				clerror("You can not add to or remove from "
				    "the \"%s\" property.\n", SCHA_RT_SYSTEM);
				return (CL_EINVAL);
			}
			system_specified = B_TRUE;
			if (strcasecmp(properties.getValue(), SCHA_TRUE) == 0) {
				system_val = B_TRUE;
			} else if (strcasecmp(properties.getValue(),
			    SCHA_FALSE) == 0) {
				system_val = B_FALSE;
			} else {
				clerror("Invalid value specified for Property "
				    "\"%s\".\n", SCHA_RT_SYSTEM);
				return (CL_EINVAL);
			}
		} else if (strncasecmp(properties.getName(),
		    SCHA_INSTALLED_NODES, prop_len) == 0) {
			if ((nodes.getSize() > 0) || (optflgs & Nflg)) {
				clerror("You can not specify the \"%s\" "
				    "property if \"-n\" or \"-N\" option is "
				    "also specified.\n", SCHA_INSTALLED_NODES);
				return (CL_EINVAL);
			}
			nl_nodes = strip_commas_in_prop(
			    properties.getValue(), &errflg);
			if (errflg)
				return (errflg);
			nodes_prop_specified = B_TRUE;
			tmp_nl_nodes = nl_nodes;
			while (nl_nodes) {
				nodes_prop.add(nl_nodes->nl_name);
				nl_nodes = nl_nodes->nl_next;
			}
			if (tmp_nl_nodes)
				rgm_free_nlist(tmp_nl_nodes);
			if (add_form)
				node_incr = B_TRUE;
			if (rm_form)
				node_decr = B_TRUE;
		} else {
			clerror("\"%s\" is not an acceptable property for "
			    "this operation.\n", properties.getName());
			return (CL_EPROP);
		}
	}

	// Check whether any zone cluster names were specified.
	// And if they were specified, there should be only one zone cluster
	// across all the RTs.
	rt_part = zc_part = zc_name = NULL;
	is_same = B_FALSE;
	clerrno = check_single_zc_specified(operands, &zc_name, &is_same,
				scconf_parse_rt_name_from_cluster_scope);
	if (clerrno != CL_NOERR) {
		clcommand_perror(clerrno);
		return (clerrno);
	}
	// If multiple zone clusters were specified, report an error.
	if (is_same == B_FALSE) {
		clerror("This operation can be performed on only one "
		    "zone cluster at a time.\n");
		return (clerrno);
	}
#if (SOL_VERSION >= __s10)
	if (zc_name) {
		// If zone cluster name was specified, we have to ensure
		// that it is valid.
		// We have to check whether this operation is allowed for
		// the status of the zone cluster.
		clerrno = check_zc_operation(CL_OBJ_TYPE_RT,
						CL_OP_TYPE_SET,
						zc_name);
		if (clerrno != CL_NOERR) {
			return (clerrno);
		}
	}
#endif

	tmp_list.add(zc_name);
	if (nodes_prop_specified)
		clerrno = get_good_nodes(nodes_prop, good_nodes, tmp_list);
	else
		clerrno = get_good_nodes(nodes, good_nodes, tmp_list);

	tmp_list.clear();

	if (clerrno)
		return (clerrno);

	operands.start();
	oper1 = operands.getValue();
	if (strcmp(oper1, CLCOMMANDS_WILD_OPERAND)) {
		for (operands.start(); operands.end() != 1; operands.next()) {
			if (rt_part) {
				free(rt_part);
				rt_part = NULL;
			}
			if (zc_part) {
				free(zc_part);
				zc_part = NULL;
			}
			// First we have to parse the RT name from
			// cluster scope.
			scconf_err = scconf_parse_rt_name_from_cluster_scope(
					operands.getValue(), &rt_part,
					&zc_part);

			if (scconf_err != SCCONF_NOERR) {
				clerrno = map_scconf_error(scconf_err);
				clcommand_perror(clerrno);
				if (first_err == CL_NOERR) {
					first_err = clerrno;
				}
				continue;
			}
			// Now, check whether this RT exists
			scha_status = rgmcnfg_get_rtrealname(
			    rt_part, &r_rtname, zc_part);
			rv = scha_status.err_code;
			if (rv != SCHA_ERR_NOERR) {
				print_rgm_error(scha_status,
				    operands.getValue());
				clerror("Skipping resource type \"%s\".\n",
				    operands.getValue());
				if (first_err == 0) {
					// first error!
					first_err = map_scha_error(rv);
				}
			} else {
				good_rts.add(r_rtname);
			}
			if (r_rtname)
				free(r_rtname);
		}
		rt_names = vl_to_names(good_rts, &errflg);
		// Free memory
		if (rt_part) {
			free(rt_part);
			rt_part = NULL;
		}
		if (zc_part) {
			free(zc_part);
			zc_part = NULL;
		}
		if (errflg)
			return (errflg);
	} else {
		scha_status = rgm_scrgadm_getrtlist(&rt_names, zc_name);
		rv = scha_status.err_code;
		if (rv != SCHA_ERR_NOERR) {
			print_rgm_error(scha_status, NULL);
			clerror("Failed to obtain list of all registered "
			    "resource types.\n");
			return (map_scha_error(rv));
		}
	}

	if (rt_names == NULL)
		return (CL_NOERR);

	// By the time we are here, we have all the RT names without the zone
	// cluster scoping. We also have the ZC context in "zc_name"

	for (i = 0; rt_names[i] != NULL; i++) {

		if (rtnodes) {
			rgm_free_strarray(rtnodes);
			rtnodes = (char **)0;
		}
		if (node_nl) {
			rgm_free_nlist(node_nl);
			node_nl = (namelist_t *)NULL;
		}
		if (new_nl) {
			rgm_free_nlist(new_nl);
			new_nl = (namelist_t *)NULL;
		}
		if (nl_start) {
			rgm_free_nlist(nl_start);
			nl_start = (namelist_t *)NULL;
		}

		// Get the RT details
		if (rt != (rgm_rt_t *)NULL) {
			rgm_free_rt(rt);
			rt = (rgm_rt_t *)NULL;
		}

		scha_status = rgm_scrgadm_getrtconf(rt_names[i], &rt, zc_name);
		rv = scha_status.err_code;
		if (rv != SCHA_ERR_NOERR) {
			print_rgm_error(scha_status, rt_names[i]);
			clerror("Skipping resource type \"%s\".\n",
			    rt_names[i]);
			if (first_err == 0) {
				// first error!
				first_err = map_scha_error(rv);
			}
			continue;
		}
		if (rt == NULL) {
			clerror("Failed to obtain resource type information "
			    "for \"%s\".\n", rt_names[i]);
			clerror("Skipping resource type \"%s\".\n",
			    rt_names[i]);
			if (first_err == 0) {
				// first error!
				first_err = CL_ENOENT;
			}
			continue;
		}

		// If all nodes, can't add or remove nodes
		if (rt->rt_instl_nodes.is_ALL_value &&
		    (node_incr || node_decr)) {
			clerror("You can not add nodes to or remove nodes "
			    "from \"%s\" as this resource type is available "
			    "on all cluster nodes including future nodes.\n",
			    rt_names[i]);
			clerror("Skipping \"%s\".\n", rt_names[i]);
			continue;
		}

		//
		// Take care of possible four updates:
		// 	-N
		//	-n nodelist
		//	-p Installed_Nodes[+/-]=
		//	-p RT_system=
		//
		if (system_specified) {
			nl_elem = (namelist_t *)calloc(1, sizeof (namelist_t));
			if (!nl_elem) {
				clcommand_perror(CL_ENOMEM);
				return (CL_ENOMEM);
			}
			nl_elem->nl_next = NULL;
			if (system_val)
				nl_elem->nl_name = strdup(SCHA_RTSYS_TRUE);
			else
				nl_elem->nl_name = strdup(SCHA_RTSYS_FALSE);
			if (!nl_elem->nl_name) {
				clcommand_perror(CL_ENOMEM);
				return (CL_ENOMEM);
			}
			nl_start = nl_elem;
		}

		if (optflgs & Nflg) {
			nl_elem = (namelist_t *)calloc(1, sizeof (namelist_t));
			if (!nl_elem) {
				clcommand_perror(CL_ENOMEM);
				return (CL_ENOMEM);
			}
			nl_elem->nl_next = NULL;
			nl_elem->nl_name = strdup(SCHA_ALL_SPECIAL_VALUE);
			if (!nl_elem->nl_name) {
				clcommand_perror(CL_ENOMEM);
				return (CL_ENOMEM);
			}
			if (!nl_start)
				nl_start = nl_elem;
			else
				nl_start->nl_next = nl_elem;

		} else if (node_incr) {
			// convert rt nodeidlist to namelist
			node_nl = nodeidlist_to_namelist(
			    rt->rt_instl_nodes.nodeids, &errflg);
			if (errflg)
				return (errflg);
			new_nl = add_vl_to_names(node_nl, good_nodes,
			    &errflg);
			if (errflg)
				return (errflg);
			if (node_nl) {
				rgm_free_nlist(node_nl);
				node_nl = NULL;
			}
			if (!nl_start)
				nl_start = new_nl;
			else
				nl_start->nl_next = new_nl;

		} else if (node_decr) {
			// convert rt nodeidlist to namelist
			node_nl = nodeidlist_to_namelist(
			    rt->rt_instl_nodes.nodeids, &errflg);
			if (errflg)
				return (errflg);
			new_nl = remove_vl_from_names(node_nl, good_nodes,
			    &errflg);
			if (errflg)
				return (errflg);

			// Make some validity checks on nodelist
			nl_prev = node_nl;
			nl_post = new_nl;
			num_old_nodes = 0;
			num_nodes = 0;
			while (nl_post) {
				num_nodes++;
				nl_post = nl_post->nl_next;
			}
			while (nl_prev) {
				num_old_nodes++;
				nl_prev = nl_prev->nl_next;
			}
			if (node_nl) {
				rgm_free_nlist(node_nl);
				node_nl = NULL;
			}

			if (num_nodes == 0) {
				clerror("Nodelist can not be empty.\n");
				clerror("Operation not allowed, skipping "
				    "resource type \"%s\".\n",
				    rt_names[i]);
				if (first_err == 0) {
					// first error!
					first_err = CL_EOP;
				}
			}

			if (num_nodes !=
			    (num_old_nodes - good_nodes.getSize())) {
				clerror("Resource type \"%s\" is not installed "
				    "on one or more of the specified nodes.\n",
				    rt_names[i]);
				clerror("Skipping resource type \"%s\".\n",
				    rt_names[i]);
				if (first_err == 0) {
					// first error!
					first_err = CL_EOP;
				}
			}

			if (!nl_start)
				nl_start = new_nl;
			else
				nl_start->nl_next = new_nl;

		} else if ((nodes.getSize() > 0) || (nodes_prop_specified)) {
			new_nl = add_vl_to_names(nl_start, good_nodes,
			    &errflg);
			if (errflg)
				return (errflg);
			if (nl_start) {
				rgm_free_nlist(nl_start);
				nl_start = NULL;
			}
			nl_start = new_nl;
		}

		rtnodes = namelist_to_names(nl_start, &errflg);
		if (errflg)
			return (errflg);

		scha_status = rgm_scrgadm_update_rt_instnodes(rt_names[i],
		    rtnodes, zc_name);
		rv = scha_status.err_code;
		if (rv) {
			print_rgm_error(scha_status, rt_names[i]);
			if (first_err == 0) {
				// first error!
				first_err = map_scha_error(rv);
			}
		} else if (optflgs & vflg)
			clmessage("Resource type \"%s\" modified.\n",
			    rt_names[i]);
	}

	if (rt)
		rgm_free_rt(rt);
	rgm_free_strarray(rt_names);
	if (rtnodes)
		rgm_free_strarray(rtnodes);
	if (new_nl)
		rgm_free_nlist(new_nl);
	if (node_nl)
		rgm_free_nlist(node_nl);
	if (zc_name) {
		free(zc_name);
	}

	return (first_err);
}
