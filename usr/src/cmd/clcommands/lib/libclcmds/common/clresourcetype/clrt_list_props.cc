//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clrt_list_props.cc	1.12	08/07/25 SMI"

//
// Process clrt "list-props"
//

#include <sys/os.h>
#include <scadmin/scconf.h>
#include <scadmin/scswitch.h>
#include <scadmin/scstat.h>
#include <rgm/rgm_scrgadm.h>
#include <rgm/rgm_cnfg.h>

#include "clcommands.h"

#include "ClCommand.h"
#include "ClList.h"

#include "rgm_support.h"
#include "common.h"

static char *
print_prop_details(char *propname);

//
// clrt "list-props"
//
clerrno_t
clrt_list_props(ClCommand &cmd, ValueList &operands, optflgs_t optflgs,
    ValueList &properties, ValueList &zc_names)
{
	clerrno_t first_err = CL_NOERR;
	int rv = 0;
	int i;
	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};
	char **rt_names = NULL;
	char *oper1, *fullname;
	namelist_t *new_nodelist;
	clerrno_t errflg = CL_NOERR;
	clerrno_t clerrno = CL_NOERR;
	ValueList good_rts;
	char *r_rtname = (char *)NULL;
	ValueList proplist, tmp_list, valid_zc_list;
	ValueList props_needed;
	ClList *print_list = new ClList(cmd.isVerbose ? true : false);
	int prop_found = 0;
	int wildcard = 0;
	scconf_errno_t scconf_err = SCCONF_NOERR;
	char *zc_name, *rt_name;

	// Initializa ORB and other cluster checks
	clerrno = cluster_init();
	if (clerrno) {
		return (clerrno);
	}

	rt_name = zc_name = NULL;
	// Zone cluster names can be specified either with the -Z
	// option or in the form or <zonecluster>:<RT name> format.
	// But, both the ways cannot be used simultaneously.
	// If zone clusters were not specified with the -Z option, we
	// have to check whether they were specified in the <zc>:<rt>
	// format.
	for (operands.start(); !operands.end(); operands.next()) {
		scconf_err = scconf_parse_rt_name_from_cluster_scope(
				operands.getValue(), &rt_name, &zc_name);

		if (scconf_err != SCCONF_NOERR) {
			return (CL_ENOMEM);
		}

		if (zc_name && zc_names.getSize()) {
			// -Z flag was specified and this RT was scoped
			// with a zone cluster name as well. Report an
			// error.
			clerror("You cannot use the "
				"\"<zonecluster>:<rtname>\" "
				"form of the resource-type name with the "
				"\"-Z\" option.");
			free(rt_name);
			free(zc_name);
			return (CL_EINVAL);
		}

		// If this zone cluster name does not exist in tmp_list,
		// then we wil add it here.
		if (!tmp_list.isValue(zc_name)) {
			tmp_list.add(zc_name);
		}

		if (rt_name) {
			free(rt_name);
			rt_name = NULL;
		}
		if (zc_name) {
			free(zc_name);
			zc_name = NULL;
		}
	}

#if (SOL_VERSION >= __s10)
	// By the time we are here, we have added any zone cluster name in the
	// RT names to tmp_list.
	if (zc_names.getSize()) {
		// Zc names were specified. We have to check whether "all"
		// was specified.
		zc_names.start();

		if (strcmp(zc_names.getValue(), ALL_CLUSTERS_STR) == 0) {
			// Get all known zone clusters.
			clerrno = get_all_clusters(valid_zc_list);

			if (clerrno != CL_NOERR) {
				return (clerrno);
			}
		} else {
			tmp_list = zc_names;
		}
	}

	// valid_zc_list will contain at least one zone cluster name
	// if "all" was specified. If it is empty, we have to check
	// the validity of the zone clusters specified by the user.
	if (valid_zc_list.getSize() < 1) {
		clerrno = get_valid_zone_clusters(tmp_list, valid_zc_list);
	}
	// Clear the memory.
	tmp_list.clear();

	if (clerrno) {
		// There was at least one invalid zone cluster.
		// We have to return.
		return (clerrno);
	}
#endif

	get_all_rt_props(proplist);
	for (properties.start(); properties.end() != 1; properties.next()) {
		prop_found = 0;
		for (proplist.start(); proplist.end() != 1; proplist.next()) {
			if (strcasecmp(proplist.getValue(),
			    properties.getValue()) == 0) {
				prop_found = 1;
				break;
			}
		}
		if (!prop_found) {
			if (first_err == 0) {
				// first error!
				first_err = CL_EPROP;
			}
			clerror("Invalid property \"%s\" specified.\n",
			    properties.getValue());
		} else
			props_needed.add(properties.getValue());
	}

	if ((properties.getSize() > 0) && (props_needed.getSize() == 0))
		return (first_err);

	if (operands.getSize() > 0) {
		operands.start();
		oper1 = operands.getValue();
		if (strcmp(oper1, CLCOMMANDS_WILD_OPERAND) == 0)
			wildcard = 1;
	} else
		wildcard = 1;

	if (!wildcard) {
		for (operands.start(); operands.end() != 1; operands.next()) {
			// Get the zone cluster name and the RT name
			scconf_err = scconf_parse_rt_name_from_cluster_scope(
						operands.getValue(), &rt_name,
						&zc_name);
			// Return if there was an error.
			if (scconf_err != SCCONF_NOERR) {
				clerror("Internal error.\n");
				return (CL_EINTERNAL);
			}

			scha_status = rgmcnfg_get_rtrealname(
			    rt_name, &r_rtname, zc_name);
			rv = scha_status.err_code;
			if (rv != SCHA_ERR_NOERR) {
				print_rgm_error(scha_status,
				    operands.getValue());
				clerror("Skipping resource type %s.\n",
				    operands.getValue());
				if (first_err == 0) {
					// first error!
					first_err = map_scha_error(rv);
				}
			} else {
				// Now after getting the real rtname append the
				// zone cluster name to the real name and add
				// to the good_rts
				scconf_err =
					scconf_add_cluster_scope_to_obj_name(
					r_rtname, zc_name,
					&fullname);
				if (scconf_err != SCCONF_NOERR) {
					clerrno = map_scconf_error(scconf_err);
					clcommand_perror(clerrno);

					if (first_err == CL_NOERR) {
						first_err = clerrno;
					}

					continue;
				}

				good_rts.add(fullname);
			}

			if (r_rtname) {
				free(r_rtname);
				r_rtname = NULL;
			}
			if (rt_name) {
				free(rt_name);
				rt_name = NULL;
			}
			if (zc_name) {
				free(zc_name);
				zc_name = NULL;
			}
		}
		rt_names = vl_to_names(good_rts, &errflg);
		if (errflg)
			return (errflg);
	} else {
		// Wildcard was specified.
		// If no zone cluster names were specified, we have to fetch
		// the RTs from the current cluster.
		if (zc_names.getSize() == 0) {
			clerrno = get_all_rt_from_zc(tmp_list, NULL, B_TRUE);
			if (clerrno != CL_NOERR) {
				clerror("Failed to obtain list of all "
				    "registered resource types.\n");
				return (clerrno);
			}
		} else {
			// Zone cluster names were specified. We have to
			// fetch RTs from all the zone clusters.
			for (valid_zc_list.start(); !valid_zc_list.end();
				valid_zc_list.next()) {
				// Add all the RTs to tmp_list.
				clerrno = get_all_rt_from_zc(tmp_list,
						valid_zc_list.getValue(),
						B_TRUE);
				if (clerrno != CL_NOERR) {
					clerror("Failed to obtain list of all "
					    "registered resource types.\n");
					return (clerrno);
				}
			}
		}

		rt_names = vl_to_names(tmp_list, &errflg);
		tmp_list.clear();
		if (errflg) {
			return (errflg);
		}
	}

	if (rt_names == NULL)
		return (CL_NOERR);

	print_list->setHeadings((char *)gettext("Property Name"),
	    (char *)gettext("Description"));

	// Print the properties as long as some operands were valid
	for (proplist.start(); proplist.end() != 1; proplist.next()) {
		if (props_needed.getSize() > 0) {
			prop_found = 0;
			for (props_needed.start(); props_needed.end() != 1;
			    props_needed.next()) {
				if (strcasecmp(props_needed.getValue(),
				    proplist.getValue()) == 0) {
					prop_found = 1;
					break;
				}
			}
			if (prop_found == 0)
				continue;
		}
		print_list->addRow(proplist.getValue(),
		    print_prop_details(proplist.getValue()));
	}
	print_list->print();

	rgm_free_strarray(rt_names);
	return (first_err);
}

static char *
print_prop_details(char *propname)
{
	char *ptr;

	if (strcasecmp(propname, SCHA_RT_DESCRIPTION) == 0)
		ptr = "A brief description of the resource type";

	else if (strcasecmp(propname, SCHA_RT_BASEDIR) == 0)
		ptr = "The directory path that is used to complete relative "
		    "paths for callback methods";

	else if (strcasecmp(propname, SCHA_SINGLE_INSTANCE) == 0)
		ptr = "TRUE indicates that only one resource of this type can "
		    "exist in the cluster";

	else if (strcasecmp(propname, SCHA_INIT_NODES) == 0)
		ptr = "Indicates the nodes on which the RGM is to call the "
		    "Init, Fini, Boot, and Validate methods";

	else if (strcasecmp(propname, SCHA_INSTALLED_NODES) == 0)
#if SOL_VERSION >= __s10
		ptr = "A list of nodes or zones the resource type is allowed "
		    "to run";
#else
		ptr = "A list of nodes the resource type is allowed to run";
#endif

	else if (strcasecmp(propname, SCHA_FAILOVER) == 0)
		ptr = "TRUE indicates that resources of this type cannot be "
		    "configured in any group that can be online on multiple "
		    "nodes at once";

	else if (strcasecmp(propname, SCHA_PROXY) == 0)
		ptr = "TRUE indicates a proxy resource type that tracks "
		    "the state of an external resource";

	else if (strcasecmp(propname, SCHA_API_VERSION) == 0)
		ptr = "The version of the resource management API that is "
		    "used by this resource type implementation";

	else if (strcasecmp(propname, SCHA_RT_VERSION) == 0)
		ptr = "Version string for the resource type";

	else if (strcasecmp(propname, SCHA_PKGLIST) == 0)
		ptr = "Optional list of packages that are included in the"
		    "resource type installation";

	else if (strcasecmp(propname, SCHA_RT_SYSTEM) == 0)
		ptr = "TRUE indicates that you can not delete the resource "
		    "type or modify its properties";

#if SOL_VERSION >= __s10
	else if (strcasecmp(propname, SCHA_GLOBALZONE) == 0)
		ptr = "TRUE for a resource type indicates that its method "
		    "executes in the global zone under all circumstances";
#endif

	return (ptr);
}
