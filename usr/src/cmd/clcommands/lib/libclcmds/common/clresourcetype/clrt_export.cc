//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clrt_export.cc	1.6	08/07/24 SMI"

//
// Process clresourcetype "export"
//

#include <sys/os.h>
#include <scadmin/scconf.h>
#include <scadmin/scswitch.h>
#include <scadmin/scstat.h>
#include <rgm/rgm_scrgadm.h>
#include <rgm/rgm_cnfg.h>

#include "clresourcetype.h"
#include "clcommands.h"
#include "ClCommand.h"
#include "ClresourcetypeExport.h"
#include "ClXml.h"
#include "rgm_support.h"


static clerrno_t add_param_objs(rgm_param_t *entry, char *rt_name,
    ClresourcetypeExport *rt_export);
static char *get_fom_name_list(void);

//
// Fetch the export object according to options/operands.
//
static clerrno_t
get_clrt_export_obj_select(ValueList &operands, ClresourcetypeExport *rt_export,
    int &no_rts)
{
	clerrno_t first_err = CL_NOERR;
	clerrno_t clerrno = CL_NOERR;
	int rv = 0;
	int i, k;
	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};
	char **rt_names = NULL;
	char *oper1;
	rgm_rg_t *rg = (rgm_rg_t *)0;
	namelist_t *new_nodelist;
	namelist_t *nl;
	namelist_t *nl1;
	namelist_t desired_nl;
	namelist_t maximum_nl;
	clerrno_t errflg = CL_NOERR;
	ValueList good_nodes;
	ValueList good_rts;
	namelist_t *node_nl;
	namelist_t *tmp_nl;
	int node_found;
	char *r_rtname = (char *)NULL;
	rgm_rt_t *rt = (rgm_rt_t *)NULL;
	int num_rts = 0;
	NameValueList showdata;
	char *new_rt, *new_entry;
	int wildcard = 0;
	char *ptr;
	char value[BUFSIZ];
	char label[BUFSIZ];
	char *node_str = (char *)NULL;
	char *pkg_str = (char *)NULL;
	rgm_rt_upgrade_t *upgp;
	char *version;
	rgm_methods_t meth;
	rgm_param_t **tablep;

	// Initialize ORB and other cluster checks
	clerrno = cluster_init();
	if (clerrno) {
		return (clerrno);
	}

	if (operands.getSize() > 0) {
		operands.start();
		oper1 = operands.getValue();
		if (strcmp(oper1, CLCOMMANDS_WILD_OPERAND) == 0)
			wildcard = 1;
	} else
		wildcard = 1;

	if (!wildcard) {
		for (operands.start(); operands.end() != 1; operands.next()) {
			scha_status = rgmcnfg_get_rtrealname(
			    operands.getValue(), &r_rtname, NULL);
			rv = scha_status.err_code;
			if (rv != SCHA_ERR_NOERR) {
				print_rgm_error(scha_status,
				    operands.getValue());
				clerror("Skipping resource type \"%s\".\n",
				    operands.getValue());
				if (first_err == 0) {
					// first error!
					first_err = map_scha_error(rv);
				}
			} else {
				good_rts.add(r_rtname);
			}
			if (r_rtname)
				free(r_rtname);
		}
		rt_names = vl_to_names(good_rts, &errflg);
		if (errflg)
			return (errflg);
	} else {
		scha_status = rgm_scrgadm_getrtlist(&rt_names, NULL);
		rv = scha_status.err_code;
		if (rv != SCHA_ERR_NOERR) {
			print_rgm_error(scha_status, NULL);
			clerror("Failed to obtain list of all registered "
			    "resource types.\n");
			return (map_scha_error(rv));
		}
	}

	if (rt_names == NULL)
		return (CL_NOERR);

	for (i = 0; rt_names[i] != NULL; i++)
		num_rts++;

	k = 0;
	for (i = 0; rt_names[i] != NULL; i++) {

		if (rt != (rgm_rt_t *)0) {
			rgm_free_rt(rt);
			rt = (rgm_rt_t *)0;
		}
		if (node_str != (char *)NULL) {
			free(node_str);
			node_str = (char *)NULL;
		}
		if (pkg_str != (char *)NULL) {
			free(pkg_str);
			pkg_str = (char *)NULL;
		}
		// Get the RT details
		scha_status = rgm_scrgadm_getrtconf(rt_names[i], &rt, NULL);
		rv = scha_status.err_code;
		if (rv != SCHA_ERR_NOERR) {
			print_rgm_error(scha_status, rt_names[i]);
			clerror("Skipping resource type \"%s\".\n",
			    rt_names[i]);
			if (first_err == 0) {
				// first error!
				first_err = map_scha_error(rv);
			}
			continue;
		}
		if (rt == NULL) {
			clerror("Failed to obtain resource type information "
			    "for \"%s\".\n", rt_names[i]);
			clerror("Skipping resource type \"%s\".\n",
			    rt_names[i]);
			if (first_err == 0) {
				// first error!
				first_err = CL_ENOENT;
			}
			continue;
		}
		if (!rt->rt_instl_nodes.is_ALL_value) {
			node_nl = nodeidlist_to_namelist(
			    rt->rt_instl_nodes.nodeids, &errflg);
			if (errflg) {
				clerror("Failed to access information for "
				    "resource type \"%s\".\n", rt_names[i]);
				if (first_err == 0) {
					// first error!
					first_err = errflg;
				}
				continue;
			}
			node_str = names_to_str(node_nl, &errflg);
			if (errflg) {
				clerror("Failed to access information for "
				    "resource type \"%s\".\n", rt_names[i]);
				if (first_err == 0) {
					// first error!
					first_err = errflg;
				}
				continue;
			}
			rgm_free_nlist(node_nl);
		}
		if (rt->rt_pkglist) {
			pkg_str = names_to_str(rt->rt_pkglist, &errflg);
			if (errflg) {
				clerror("Failed to access information for "
				    "resource type \"%s\".\n", rt_names[i]);
				if (first_err == 0) {
					// first error!
					first_err = errflg;
				}
				continue;
			}
		}

		no_rts++;

		// Create an xml object
		// XXX - need the RTR file!
		rt_export->addResourcetype(rt_names[i], (char *)"");

		// Add the nodes...
		if (rt->rt_instl_nodes.is_ALL_value) {
			rt_export->setAllNodes(rt_names[i]);
		} else {
			// node_str list is ' ' separated
			char *node;
			node = strtok(node_str, " ");
			while (node != NULL) {
				rt_export->addResourcetypeNode(
				    rt_names[i], node);
				node = strtok(NULL, " ");
			}
		}

		// Properties...
		if (rt->rt_description && *rt->rt_description) {
			rt_export->addResourcetypeProperty(rt_names[i],
			    (char *)"RT_description", rt->rt_description);
		}

		if (rt->rt_version && *rt->rt_version) {
			rt_export->addResourcetypeProperty(rt_names[i],
			    (char *)"RT_version", rt->rt_version);
		}

		(void) sprintf(value, "%d", rt->rt_api_version);
		rt_export->addResourcetypeProperty(rt_names[i],
		    (char *)"API_version", value);

		if (rt->rt_basedir && rt->rt_basedir) {
			rt_export->addResourcetypeProperty(rt_names[i],
			    (char *)"RT_basedir", rt->rt_basedir);
		}

		ptr = (rt->rt_single_inst == B_FALSE)
		    ? VALUE_STR_FALSE : VALUE_STR_TRUE;
		rt_export->addResourcetypeProperty(rt_names[i],
		    (char *)"Single_instance", ptr);

		ptr = (rt->rt_proxy == B_FALSE)
		    ? VALUE_STR_FALSE : VALUE_STR_TRUE;
		rt_export->addResourcetypeProperty(rt_names[i],
		    (char *)"Proxy", ptr);

		switch (rt->rt_init_nodes) {
		case SCHA_INFLAG_RG_PRIMARIES:
			ptr = "All potential masters";
			break;
		case SCHA_INFLAG_RT_INSTALLED_NODES:
			ptr = "All nodes";
		default:
			ptr = VALUE_STR_UNKNOWN;
			break;
		}
		rt_export->addResourcetypeProperty(rt_names[i],
		    (char *)"Init_nodes", ptr);

		ptr = (rt->rt_failover == B_FALSE)
		    ? VALUE_STR_FALSE : VALUE_STR_TRUE;
		rt_export->addResourcetypeProperty(rt_names[i],
		    (char *)"Failover", ptr);

		if (rt->rt_pkglist && pkg_str) {
			rt_export->addResourcetypeProperty(rt_names[i],
			    (char *)"Pkglist", pkg_str);
		}

		ptr = (rt->rt_system == B_TRUE)
		    ? VALUE_STR_TRUE : VALUE_STR_FALSE;
		rt_export->addResourcetypeProperty(rt_names[i],
		    (char *)"RT_system", ptr);

#if SOL_VERSION >= __s10
		ptr = (rt->rt_globalzone == B_TRUE)
		    ? VALUE_STR_TRUE : VALUE_STR_FALSE;
		rt_export->addResourcetypeProperty(rt_names[i],
		    (char *)"Global_zone", ptr);
#endif

		// Methods...
		meth = rt->rt_methods;

		if (meth.m_start && *meth.m_start)
			rt_export->addMethod(rt_names[i],
			    (char *)"START", meth.m_start);

		if (meth.m_stop && *meth.m_stop)
			rt_export->addMethod(rt_names[i],
			    (char *)"STOP", meth.m_stop);

		if (meth.m_validate && *meth.m_validate)
			rt_export->addMethod(rt_names[i],
			    (char *)"VALIDATE", meth.m_validate);

		if (meth.m_update && *meth.m_update)
			rt_export->addMethod(rt_names[i],
			    (char *)"UPDATE", meth.m_update);

		if (meth.m_init && *meth.m_init)
			rt_export->addMethod(rt_names[i],
			    (char *)"INIT", meth.m_init);

		if (meth.m_fini && *meth.m_fini)
			rt_export->addMethod(rt_names[i],
			    (char *)"FINI", meth.m_fini);

		if (meth.m_boot && *meth.m_boot)
			rt_export->addMethod(rt_names[i],
			    (char *)"BOOT", meth.m_boot);

		if (meth.m_monitor_start && *meth.m_monitor_start)
			rt_export->addMethod(rt_names[i],
			    (char *)"MONITOR START",
			    meth.m_monitor_start);

		if (meth.m_monitor_stop && *meth.m_monitor_stop)
			rt_export->addMethod(rt_names[i],
			    (char *)"MONITOR STOP",
			    meth.m_monitor_stop);

		if (meth.m_monitor_check && *meth.m_monitor_check)
			rt_export->addMethod(rt_names[i],
			    (char *)"MONITOR CHECK",
			    meth.m_monitor_check);

		if (meth.m_prenet_start && *meth.m_prenet_start)
			rt_export->addMethod(rt_names[i],
			    (char *)"PRENET START",
			    meth.m_prenet_start);

		if (meth.m_postnet_stop && *meth.m_postnet_stop)
			rt_export->addMethod(rt_names[i],
			    (char *)"POSTNET STOP",
			    meth.m_postnet_stop);


		// Parameters...
		for (tablep = rt->rt_paramtable; *tablep && (*tablep)->p_name;
		    ++tablep) {
			errflg = add_param_objs(*tablep, rt_names[i],
			    rt_export);
			if (errflg && !first_err)
				first_err = errflg;
		}

	}
	if (rt)
		rgm_free_rt(rt);
	if (node_str)
		free(node_str);
	if (pkg_str)
		free(pkg_str);
	rgm_free_strarray(rt_names);
	return (first_err);
}

static clerrno_t
add_param_objs(rgm_param_t *entry, char *rt_name,
    ClresourcetypeExport *rt_export)
{
	char *ptr;
	char *param_name = NULL;
	bool extension = false;
	bool per_node = false;
	char *desc = NULL;
	tunability_t tunability;
	param_type_t type;
	char *enumList = NULL;
	char *minLength = NULL;
	char *maxLength = NULL;
	char *minArrayLength = NULL;
	char *maxArrayLength = NULL;
	char *def = NULL;
	clerrno_t errflg = 0;
	clerrno_t clerrno;

	if (!entry->p_name && !*entry->p_name) {
		return (errflg);
	} else
		param_name = entry->p_name;

	/* Param table extension */
	extension = (entry->p_extension == B_FALSE)
	    ? false : true;

	/* Per-node */
	per_node = (entry->p_per_node == B_FALSE)
	    ? false : true;

	/* Param table description */
	desc = (entry->p_description && *entry->p_description)
	    ? entry->p_description : (char *)"";

	/* Param table tunability */
	switch (entry->p_tunable) {

	case TUNE_AT_CREATION:
		tunability = AT_CREATION;
		break;

	case TUNE_ANYTIME:
		tunability = ANY_TIME;
		break;

	case TUNE_WHEN_DISABLED:
		tunability = WHEN_DISABLED;
		break;

	case TUNE_NONE:
		tunability = NONE;
		break;

	case TUNE_WHEN_OFFLINE:
	case TUNE_WHEN_UNMANAGED:
	case TUNE_WHEN_UNMONITORED:
	default:
		tunability = UNKNOWN_TYPE;
		break;
	}

	/* Param table type */
	switch (entry->p_type) {
	case SCHA_PTYPE_STRING:

		/* type string */
		type = STRING;

		/* print min length */
		if (entry->p_min_isset) {
			minLength = new char[20];
			(void) sprintf(minLength, "%d", entry->p_min);
		}

		/* print max length */
		if (entry->p_max_isset && entry->p_max < MAXRGMVALUESLEN) {
			maxLength = new char[20];
			(void) sprintf(maxLength, "%d", entry->p_max);
		}

		/* print default */
		if (entry->p_default_isset) {
			if (entry->p_defaultstr && *entry->p_defaultstr)
				def = entry->p_defaultstr;
		}
		break;

	case SCHA_PTYPE_INT:

		/* type int */
		type = INTEGER;

		/* print min int type */
		if (entry->p_min_isset) {
			minLength = new char[20];
			(void) sprintf(minLength, "%d", entry->p_min);
		}

		/* print max int type */
		if (entry->p_max_isset && entry->p_max < MAXRGMVALUESLEN) {
			maxLength = new char[20];
			(void) sprintf(maxLength, "%d", entry->p_max);
		}

		/* print default */
		if (entry->p_default_isset) {
			def = new char[20];
			(void) sprintf(def, "%d", entry->p_defaultint);
		}
		break;

	case SCHA_PTYPE_BOOLEAN:

		/* type boolean */
		type = BOOLEAN;

		/* print default */
		if (entry->p_default_isset) {
			def = (entry->p_defaultbool == B_FALSE)
			    ? VALUE_STR_FALSE : VALUE_STR_TRUE;
		}
		break;

	case SCHA_PTYPE_ENUM:

		/* type enum */
		type = ENUM;

		/* print enum list */
		if (entry->p_name &&
		    (strcmp(SCHA_FAILOVER_MODE, entry->p_name) == 0)) {
			/*
			 * the failover_mode system property doesn't
			 * use the enumlist any more.
			 * we fake it.
			 */
			ptr = get_fom_name_list();
		} else {
			ptr = names_to_str(entry->p_enumlist, &errflg);

			// If set to <NULL>, might as well not set at all
			if (strcmp(ptr, VALUE_STR_NULL) == 0)
				ptr = NULL;
		}
		if (ptr) {
			enumList = strdup(ptr);
			free(ptr);
		}

		/* print default */
		if (entry->p_default_isset) {
			def = entry->p_defaultstr;
		}

		break;

	case SCHA_PTYPE_STRINGARRAY:

		/* type stringarray */
		type = STRING_ARRAY;

		/* print min length */
		if (entry->p_min_isset) {
			minLength = new char[20];
			(void) sprintf(minLength, "%d", entry->p_min);
		}

		/* print max length */
		if (entry->p_max_isset && entry->p_max < MAXRGMVALUESLEN) {
			maxLength = new char[20];
			(void) sprintf(maxLength, "%d", entry->p_max);
		}

		/* print min array length */
		if (entry->p_arraymin_isset) {
			minArrayLength = new char[20];
			(void) sprintf(minArrayLength, "%d", entry->p_arraymin);
		}

		/* print max array length */
		if (entry->p_arraymax_isset) {
			maxArrayLength = new char[20];
			(void) sprintf(maxArrayLength, "%d", entry->p_arraymax);
		}

		/* print default */
		if (entry->p_default_isset) {
			def = names_to_str(entry->p_defaultarray, &errflg);

			// If set to <NULL>, might as well not set at all
			if (strcmp(def, VALUE_STR_NULL) == 0)
				def = NULL;
		}
		break;

	default:
		// type unknown
		errflg = CL_EINTERNAL;
		break;
	}

	clerrno = errflg;
	errflg = rt_export->addParameter(rt_name,
	    param_name,
	    extension,
	    per_node,
	    desc,
	    tunability,
	    type,
	    enumList,
	    minLength,
	    maxLength,
	    minArrayLength,
	    maxArrayLength,
	    def);

	if (errflg) {
		if (!clerrno)
			clerrno = errflg;

		clerror("Internal error encountered while adding parameter "
		    "\"%s\" to XML document.\n", param_name);
	}

	return (clerrno);
}

//
// get_fom_name_list: This should be added to the rgm_scrgadm library.
//
// return a single string with all the possible values for
// Failover_mode
// Since the introduction of the RESTART_ONLY and LOG_ONLY values
// for Failover_mode, the enumlist is no longer used
//
static char *
get_fom_name_list(void)
{
	int	n, i;
	size_t	len = 0;
	char	*str;
	char	*fom_values[] = { SCHA_NONE, SCHA_HARD, SCHA_SOFT,
	    SCHA_RESTART_ONLY, SCHA_LOG_ONLY };

	n = sizeof (fom_values) / sizeof (char *);

	for (i = 0; i < n; i++) {
		len += strlen(fom_values[i]) + 1;
	}

	/* allocate the buffer. calloc() zeroes everything */
	if ((str = (char *)calloc(1, len + 1)) == NULL) {
		return (NULL);
	}

	for (i = 0; i < n; i++) {
		/* add space sepator, except for the first iteration */
		if (*str != '\0') {
			(void) strcat(str, " ");
		}

		(void) strcat(str, fom_values[i]);
	}

	return (str);
}

//
// This function returns a ClresourcetypeExport object populated with
// all of the resourcetype information from the cluster.  Caller is
// responsible for releasing the ClcmdExport object.
//
ClcmdExport *
get_clresourcetype_xml_elements()
{
	ValueList empty;
	int no_rts = 0;
	ClresourcetypeExport *clrt_exp = new ClresourcetypeExport();

	(void) get_clrt_export_obj_select(empty, clrt_exp, no_rts);
	return (clrt_exp);
}


//
// clrt "export"
//
clerrno_t
clrt_export(ClCommand &cmd, ValueList &operands, char *clconfiguration)
{
	clerrno_t first_err = CL_NOERR;
	int clerrno = CL_NOERR;
	int no_rts = 0;

	ClresourcetypeExport *rt_export = new ClresourcetypeExport();
	clerrno = get_clrt_export_obj_select(operands, rt_export, no_rts);
	if (clerrno) {
		first_err = clerrno;
	}

	// Check for no rt's
	if (!no_rts) {
		return (first_err);
	}

	ClXml xml;
	clerrno = xml.createExportFile(CLRESOURCETYPE_CMD,
	    clconfiguration, rt_export);

	if (clerrno && !first_err)
		first_err = clerrno;

	return (first_err);
}
