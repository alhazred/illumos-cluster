//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clrt_list.cc	1.12	08/10/14 SMI"

//
// Process clrt "list"
//

#include <sys/os.h>
#include <scadmin/scconf.h>
#include <scadmin/scswitch.h>
#include <scadmin/scstat.h>
#include <rgm/rgm_scrgadm.h>
#include <rgm/rgm_cnfg.h>

#include "clcommands.h"

#include "ClCommand.h"
#include "ClList.h"

#include "rgm_support.h"
#include "common.h"

//
// clrt "list"
//
clerrno_t
clrt_list(ClCommand &cmd, ValueList &operands, optflgs_t optflgs,
    ValueList &nodes, ValueList &zc_names)
{
	clerrno_t first_err = CL_NOERR;
	int rv = 0;
	int i;
	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};
	char **rt_names = NULL;
	char *oper1;
	rgm_rg_t *rg = (rgm_rg_t *)0;
	namelist_t *nl;
	namelist_t *nl1;
	clerrno_t errflg = CL_NOERR;
	clerrno_t clerrno = CL_NOERR;
	ValueList good_nodes;
	ValueList good_rts;
	namelist_t *node_nl = (namelist_t *)NULL;
	char *r_rtname = (char *)NULL;
	rgm_rt_t *rt = (rgm_rt_t *)NULL;
	char *node_str = (char *)NULL;
	int wildcard = 0;
	ClList print_list = ClList(cmd.isVerbose ? true : false);
	ValueList col1, col2, tmp_list, valid_zc_list;
	int node_found;
	scconf_errno_t scconf_err = SCCONF_NOERR;
	char *zc_name, *rt_name;
	char *fullname = NULL;
	char **tmp_rt_list = NULL;

	// Initializa ORB and other cluster checks
	clerrno = cluster_init();
	if (clerrno) {
		return (clerrno);
	}

	rt_name = zc_name = NULL;
	// Zone cluster names can be specified either with the -Z
	// option or in the form or <zonecluster>:<RT name> format.
	// But, both the ways cannot be used simultaneously.
	// If zone clusters were not specified with the -Z option, we
	// have to check whether they were specified in the <zc>:<rt>
	// format.
	for (operands.start(); !operands.end(); operands.next()) {
		scconf_err = scconf_parse_rt_name_from_cluster_scope(
				operands.getValue(), &rt_name, &zc_name);

		if (scconf_err != SCCONF_NOERR) {
			return (CL_ENOMEM);
		}

		if (zc_name && zc_names.getSize()) {
			// -Z flag was specified and this RT was scoped
			// with a zone cluster name as well. Report an
			// error.
			clerror("You cannot use the "
				"\"<zonecluster>:<rtname>\" "
				"form of the resource-type name with the "
				"\"-Z\" option.");
			free(rt_name);
			free(zc_name);
			return (CL_EINVAL);
		}

		// If this zone cluster name does not exist in tmp_list,
		// then we wil add it here.
		if (!tmp_list.isValue(zc_name)) {
			tmp_list.add(zc_name);
		}

		if (rt_name) {
			free(rt_name);
			rt_name = NULL;
		}
		if (zc_name) {
			free(zc_name);
			zc_name = NULL;
		}
	}

#if (SOL_VERSION >= __s10)
	// By the time we are here, we have added any zone cluster name in the
	// RT names to tmp_list.
	if (zc_names.getSize()) {
		// Zc names were specified. We have to check whether "all"
		// was specified.
		zc_names.start();

		if (strcmp(zc_names.getValue(), ALL_CLUSTERS_STR) == 0) {
			// Get all known zone clusters.
			clerrno = get_all_clusters(valid_zc_list);

			if (clerrno != CL_NOERR) {
				return (clerrno);
			}
		} else {
			tmp_list = zc_names;
		}
	}

	// valid_zc_list will contain at least one zone cluster name
	// if "all" was specified. If it is empty, we have to check
	// the validity of the zone clusters specified by the user.
	if (valid_zc_list.getSize() < 1) {
		clerrno = get_valid_zone_clusters(tmp_list, valid_zc_list);
	}
	// Clear the memory.
	tmp_list.clear();

	if (clerrno) {
		// There was at least one invalid zone cluster.
		// We have to return.
		return (clerrno);
	}
#endif

	// We have to check the validity of the nodes, in case
	// they were specified. By now, we know that all the
	// zone clusters are valid.
	clerrno = get_good_nodes(nodes, good_nodes, valid_zc_list);
	if (clerrno) {
		if (first_err == 0) {
			// first error!
			// Invalid nodes were specified. We have
			// to return.
			first_err = clerrno;
			good_nodes.clear();
			return (first_err);
		}
	}
	if ((nodes.getSize() > 0) && (good_nodes.getSize() == 0)) {
		return (first_err);
	}

	if (operands.getSize() > 0) {
		operands.start();
		oper1 = operands.getValue();
		if (strcmp(oper1, CLCOMMANDS_WILD_OPERAND) == 0)
			wildcard = 1;
	} else {
		wildcard = 1;
	}

	if (!wildcard) {
		for (operands.start(); operands.end() != 1; operands.next()) {

			// First parse the RT name from zone cluster scope
			if (rt_name) {
				free(rt_name);
				rt_name = NULL;
			}
			if (zc_name) {
				free(zc_name);
				zc_name = NULL;
			}
			if (r_rtname)
				free(r_rtname);

			scconf_err = scconf_parse_rt_name_from_cluster_scope(
				operands.getValue(), &rt_name, &zc_name);

			if (scconf_err != SCCONF_NOERR) {
				if (first_err == CL_NOERR) {
					first_err = map_scconf_error(
							scconf_err);
				}
				continue;
			}

			// If zone cluster names were not specified
			// using the -Z flag, they could have been specified
			// as part of the RT name also.
			if (zc_names.getSize() == 0) {
				// -Z flag was not used.
				scha_status = rgmcnfg_get_rtrealname(
				    rt_name, &r_rtname, zc_name);
				rv = scha_status.err_code;
				if (rv != SCHA_ERR_NOERR) {
					print_rgm_error(scha_status,
					    operands.getValue());
					clerror("Skipping resource type "
					    "\"%s\".\n", operands.getValue());
					if (first_err == 0) {
						// first error!
						first_err = map_scha_error(rv);
					}
				} else {
					// Now after getting the real rtname
					// append the zone cluster name to the
					// real name and add to the good_rts
					scconf_err =
					scconf_add_cluster_scope_to_obj_name(
						r_rtname, zc_name,
						&fullname);
					if (scconf_err != SCCONF_NOERR) {
						clerrno = map_scconf_error
							(scconf_err);
						clcommand_perror(clerrno);

						if (first_err == CL_NOERR) {
							first_err = clerrno;
						}

						continue;
					}

					good_rts.add(fullname);
				}

				continue;
			}

#if (SOL_VERSION >= __s10)
			// If we are here, it means zone clusters were
			// specified using the -Z flag.
			tmp_list.clear();
			clerrno = get_zone_clusters_with_rt(rt_name, tmp_list,
								valid_zc_list);
			if (clerrno != CL_NOERR) {

				if (clerrno == CL_ENOENT) {
					clerror("Skipping resource type "
					    "\"%s\".\n", operands.getValue());
				} else {
					clcommand_perror(clerrno);
				}

				if (first_err == CL_NOERR) {
					first_err = clerrno;
				}
				continue;
			}
#endif

			// We have to scope this RT name with the
			// zone cluster name
			for (tmp_list.start(); !tmp_list.end();
				tmp_list.next()) {

				if (fullname) {
					free(fullname);
					fullname = NULL;
				}

				scha_status = rgmcnfg_get_rtrealname(
				    rt_name, &r_rtname, tmp_list.getValue());
				rv = scha_status.err_code;
				if (rv != SCHA_ERR_NOERR) {
					print_rgm_error(scha_status,
					    operands.getValue());
					clerror("Skipping resource type "
					    "\"%s\".\n", operands.getValue());
					if (first_err == 0) {
						// first error!
						first_err = map_scha_error(rv);
					}
				} else {
					// Now after getting the real rtname
					// append the zone cluster name to the
					// real name and add to the good_rts
					scconf_err =
					scconf_add_cluster_scope_to_obj_name(
						r_rtname, tmp_list.getValue(),
						&fullname);
					if (scconf_err != SCCONF_NOERR) {
						clerrno = map_scconf_error
							(scconf_err);
						clcommand_perror(clerrno);

						if (first_err == CL_NOERR) {
							first_err = clerrno;
						}

						continue;
					}

					good_rts.add(fullname);
				}
			}

			if (fullname) {
				free(fullname);
				fullname = NULL;
			}

		}

		rt_names = vl_to_names(good_rts, &errflg);
		if (errflg)
			return (errflg);
	} else {
		// Wildcard was specified.
		// If no zone cluster names were specified, we have to fetch
		// the RTs from the current cluster.
		if (zc_names.getSize() == 0) {
			clerrno = get_all_rt_from_zc(tmp_list, NULL, B_TRUE);
			if (clerrno != CL_NOERR) {
				clerror("Failed to obtain list of all "
				    "registered resource types.\n");
				return (clerrno);
			}
		} else {
			// Zone cluster names were specified. We have to
			// fetch RTs from all the zone clusters.
			for (valid_zc_list.start(); !valid_zc_list.end();
				valid_zc_list.next()) {
				// Add all the RTs to tmp_list.
				clerrno = get_all_rt_from_zc(tmp_list,
						valid_zc_list.getValue(),
						B_TRUE);
				if (clerrno != CL_NOERR) {
					clerror("Failed to obtain list of all "
					    "registered resource types.\n");
					return (clerrno);
				}
			}
		}

		rt_names = vl_to_names(tmp_list, &errflg);
		tmp_list.clear();
		if (errflg) {
			return (errflg);
		}
	}

	if (rt_names == NULL)
		return (first_err);

	// By the time we are here, all the RTs have been validated
	// and are in the format <ZC name>:<RT name>
	rt_name = zc_name = NULL;
	for (i = 0; rt_names[i] != NULL; i++) {

		if (node_str)
			free(node_str);

		if (node_nl)
			rgm_free_nlist(node_nl);

		if (rt != (rgm_rt_t *)0) {
			rgm_free_rt(rt);
			rt = (rgm_rt_t *)0;
		}
		if (rt_name) {
			free(rt_name);
			rt_name = NULL;
		}
		if (zc_name) {
			free(zc_name);
			zc_name = NULL;
		}
		// Get the zone cluster name and the RT name
		scconf_err = scconf_parse_rt_name_from_cluster_scope(
					rt_names[i], &rt_name, &zc_name);
		// Return if there was an error.
		if (scconf_err != SCCONF_NOERR) {
			clerror("Internal error.\n");
			return (CL_EINTERNAL);
		}

		// Get the RT details
		scha_status = rgm_scrgadm_getrtconf(rt_name, &rt, zc_name);
		rv = scha_status.err_code;
		if (rv != SCHA_ERR_NOERR) {
			print_rgm_error(scha_status, rt_names[i]);
			clerror("Skipping resource type \"%s\".\n",
			    rt_names[i]);
			if (first_err == 0) {
				// first error!
				first_err = map_scha_error(rv);
			}
			continue;
		}
		if (rt == NULL) {
			clerror("Failed to obtain resource type information "
			    "for \"%s\".\n", rt_names[i]);
			clerror("Skipping resource type \"%s\".\n",
			    rt_names[i]);
			if (first_err == 0) {
				// first error!
				first_err = CL_ENOENT;
			}
			continue;
		}

		// convert rt nodeidlist to namelist
		node_nl = nodeidlist_to_namelist(rt->rt_instl_nodes.nodeids,
			    &errflg);
		if (errflg) {
			rgm_free_rt(rt);
			return (errflg);
		}

		node_str = names_to_str(node_nl, &errflg);
		if (errflg)
			return (errflg);

		node_found = 0;
		if (rt->rt_instl_nodes.is_ALL_value)
			node_found = 1;

		if ((good_nodes.getSize() > 0) && (node_found == 0)) {
			for (nl = node_nl; nl; nl = nl->nl_next) {
				for (good_nodes.start(); !good_nodes.end();
				    good_nodes.next()) {
					if (strcmp(good_nodes.getValue(),
					    nl->nl_name) == 0)
						node_found = 1;
				}
			}
			if (node_found == 0) {
				if (!wildcard) {
					if (first_err == 0) {
						// first error!
						first_err = CL_EINVAL;
					}
					clerror("Specified resource type "
					    "\"%s\" does not meet the criteria "
					    "set by the \"-n\" option.\n",
					    rt_names[i]);
					continue;
				} else {
					continue;
				}
			}
		}
		col1.add(rt_names[i]);
		if (rt->rt_instl_nodes.is_ALL_value)
			col2.add("<All>");
		else
			col2.add(node_str);
	}

	print_list.setHeadings(
	    (char *)gettext("Resource Type"),
	    (char *)gettext("Node List"));
	for (col1.start(), col2.start();
	    col1.end() != 1;
	    col1.next(), col2.next())
		print_list.addRow(col1.getValue(), col2.getValue());
	print_list.print();

	if (node_nl)
		rgm_free_nlist(node_nl);
	if (node_str)
		free(node_str);

	if (rt)
		rgm_free_rt(rt);

	if (rt_name) {
		free(rt_name);
	}
	if (zc_name) {
		free(zc_name);
	}
	rgm_free_strarray(rt_names);
	return (first_err);
}
