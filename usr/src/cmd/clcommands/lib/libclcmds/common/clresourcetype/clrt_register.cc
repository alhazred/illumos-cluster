//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clrt_register.cc	1.18	08/10/06 SMI"

//
// Process clrt "register"
//

#include <sys/os.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <scadmin/scconf.h>
#include <scadmin/scswitch.h>
#include <scadmin/scrgadm.h>
#include <scadmin/scstat.h>
#include <rgm/rgm_scrgadm.h>
#include <rgm/rgm_cnfg.h>

#include "clcommands.h"
#include "common.h"

#include "ClCommand.h"
#include "ClXml.h"

#include "clresourcetype.h"
#include "rgm_support.h"

static clerrno_t
get_type_in_file(char *rtrfile, char *full_rtname, int wildcard);

static clerrno_t
get_type_file_pair(char *rtrfile, NameValueList &type_file_pair, int *isfile,
    int wildcard);

#define	FULLRT_MAXNAMELEN (3 * MAXCCRTBLNMLEN + 3)

//
// clrt "register"
//
clerrno_t
clrt_register(ClCommand &cmd, ValueList &operands, optflgs_t optflgs,
    ValueList &nodes, char *rtrfile, NameValueList &properties)
{
	clerrno_t first_err = CL_NOERR;
	int rv = 0;
	int i;
	boolean_t bypass_install_mode = B_FALSE; /* private for upgrade */
	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};
	char **rt_names = NULL;
	char *oper1;
	rgm_rg_t *rg = (rgm_rg_t *)0;
	namelist_t *new_nodelist;
	namelist_t *nl;
	namelist_t *nl1;
	clerrno_t errflg = CL_NOERR;
	clerrno_t clerrno = CL_NOERR;
	ValueList good_nodes;
	ValueList uniq_rtlist;
	namelist_t *node_nl;
	rgm_rt_t *rt = (rgm_rt_t *)NULL;
	namelist_t *rtnodes = (namelist_t *)NULL;
	NameValueList type_file_pair;
	int isfile = 0;
	int wildcard = 0;
	RtrFileResult *rtr_results = (RtrFileResult *)NULL;
	char *tmp_rtf;
	char name_buf[PATH_MAX];
	nodeidlist_t *nodeidlist = (nodeidlist_t *)NULL;
	boolean_t system_specified = B_FALSE;
	boolean_t nodes_prop_specified = B_FALSE;
	boolean_t system_val;
	namelist_t *nl_nodes = (namelist_t *)NULL;
	namelist_t *tmp_nl_nodes = (namelist_t *)NULL;
	ValueList nodes_prop;
	char full_rtname_buf[MAXCCRTBLNMLEN];
	ValueList tmp_list;
	scconf_errno_t scconf_err = SCCONF_NOERR;
	boolean_t is_same = B_FALSE, iszonecluster = B_FALSE;
	char *zc_name, *rt_name = NULL;
	char *tmp_operands = NULL, *tmp_zcname = NULL;
	char *orig_rt_name = NULL, *orig_rt_zc = NULL;

	// Initializa ORB and other cluster checks
	clerrno = cluster_init();
	if (clerrno) {
		return (clerrno);
	}

#if (SOL_VERSION >= __s10)
	scconf_err = scconf_zone_cluster_check(&iszonecluster);

	if (scconf_err != SCCONF_NOERR) {
		clerrno = CL_EINTERNAL;
		clcommand_perror(clerrno);
		return (clerrno);
	}

	if (iszonecluster) {
		//
		// We are executing inside a zone cluster if "iszonecluster" is
		// true. Zone Cluster cannot register a RT directly hence we
		// need to contact the rtproxy_server running on the global zone
		// to register the RT.
		//
		clerrno = call_rtreg_proxy_register_cmd(cmd);

		return (clerrno);
	}

	//
	// We are running in the global zone if we are here.
	//

	//
	// Check if we have a -Z flag specified if so then validate the
	// Zone cluster name
	//

	if (optflgs & Zflg) {
		operands.start();
		tmp_operands = strdup(operands.getValue());
		tmp_zcname = strtok(tmp_operands, ":");
		clerrno = check_zone_cluster_exists(tmp_zcname);

		if (clerrno != CL_NOERR) {
			clerror("%s is not a valid zone cluster.\n",
				tmp_zcname);
			return (clerrno);
		}
	}
#endif

	// Check whether any zone cluster names were specified.
	// And if they were specified, there should be only zone cluster
	// across all the RGs.
	clerrno = check_single_zc_specified(operands, &zc_name, &is_same,
				scconf_parse_rt_name_from_cluster_scope);
	if (clerrno != CL_NOERR) {
		clcommand_perror(clerrno);
		return (clerrno);
	}
	// If multiple zone clusters were specified, report an error.
	if (is_same == B_FALSE) {
		clerror("This operation can be performed on only one "
		    "zone cluster at a time.\n");
		return (clerrno);
	}
#if (SOL_VERSION >= __s10)
	if (zc_name) {
		// If zone cluster name was specified, we have to ensure
		// that it is valid.
		// We have to check whether this operation is allowed for
		// the status of the zone cluster.

		//
		// This check is disabled for allowing the clrt register
		// command to be able to run even when the zone cluster
		// node is not completely booted up. This is to allow
		// Zone Cluster Boot handlers to execute clrt register
		// for SUNW.LogicalHostname & SUNW.SharedAddress
		//

		// clerrno = check_zc_operation(CL_OBJ_TYPE_RT,
		//				CL_OP_TYPE_REGISTER,
		//				zc_name);
		// if (clerrno != CL_NOERR) {
		//	return (clerrno);
		// }
	}
#endif

	// Check the properties.
	for (properties.start(); properties.end() != 1; properties.next()) {
		if (strcasecmp(properties.getName(), SCHA_RT_SYSTEM) == 0) {
			system_specified = B_TRUE;
			if (strcasecmp(properties.getValue(), SCHA_TRUE) == 0) {
				system_val = B_TRUE;
			} else if (strcasecmp(properties.getValue(),
			    SCHA_FALSE) == 0) {
				system_val = B_FALSE;
			} else {
				clerror("Invalid value specified for Property "
				    "\"%s\".\n", SCHA_RT_SYSTEM);
				return (CL_EINVAL);
			}
		} else if (strcasecmp(properties.getName(),
		    SCHA_INSTALLED_NODES) == 0) {
			if ((nodes.getSize() > 0) || (optflgs & Nflg)) {
				clerror("You can not specify the \"%s\" "
				    "property if \"-n\" or \"-N\" option is "
				    "also specified.\n", SCHA_INSTALLED_NODES);
				return (CL_EINVAL);
			}
			nl_nodes = strip_commas_in_prop(
			    properties.getValue(), &errflg);
			if (errflg)
				return (errflg);
			nodes_prop_specified = B_TRUE;
			tmp_nl_nodes = nl_nodes;
			while (nl_nodes) {
				nodes_prop.add(nl_nodes->nl_name);
				nl_nodes = nl_nodes->nl_next;
			}
			if (tmp_nl_nodes)
				rgm_free_nlist(tmp_nl_nodes);
		} else {
			clerror("\"%s\" is not an acceptable property "
			    "for this operation.\n", properties.getName());
			return (CL_EPROP);
		}
	}

	tmp_list.add(zc_name);
	if (nodes_prop_specified) {
		// If nodes were specified as a property, we have to ensure
		// that they were not specified in the "node:zone" format
		// when zone cluster was specified.
		if (zc_name) {
			for (nodes_prop.start(); !nodes_prop.end();
					nodes_prop.next()) {
				if (!strchr(nodes_prop.getValue(), ':')) {
					continue;
				}

				// If we are here, it means this node is
				// in the format "node:zone".
				clerror("You cannot specify zones as"
				    " part of node names while"
				    "specifying the\"-Z\" option.\n");
				clerrno = CL_EINVAL;
				break;
			}
		}
		clerrno = get_good_nodes(nodes_prop, good_nodes, tmp_list);
	} else {
		clerrno = get_good_nodes(nodes, good_nodes, tmp_list);
	}
	// Free memory
	tmp_list.clear();

	if (clerrno)
		return (clerrno);

	if (good_nodes.getSize() > 0) {
		rtnodes = add_vl_to_names(rtnodes, good_nodes, &errflg);
		if (errflg)
			return (errflg);

		if (zc_name) {
#if SOL_VERSION >= __s10
			scha_status =
			    rgm_scrgadm_convert_zc_namelist_to_nodeidlist(
				rtnodes, &nodeidlist, zc_name);
#endif
		} else {
			scha_status =
			    rgm_scrgadm_convert_namelist_to_nodeidlist(
				rtnodes, &nodeidlist);
		}
		rv = scha_status.err_code;
		if (rv != SCHA_ERR_NOERR) {
			print_rgm_error(scha_status, NULL);
			return (map_scha_error(rv));
		}

	}

	// Take care of the undocumented force flag
	if (optflgs & Fflg)
		bypass_install_mode = B_TRUE;

	//
	// If there is an rtr file specified,
	//	For a single type, use that name and the rtr file
	//	For others , issue an error
	// Else if there is an rtr dir specified
	//	Make a list of typename-filenames in the directory
	//	For eact typename specified, register it using that typename
	//	and a matching filename from the list
	//	If + is specified, register using the typename-filename list
	// Else if no -f
	//	If + is specified, register known SUNW rts using default rtrdir
	//	Else register provided rts with default rtrdir
	//

	operands.start();
	oper1 = operands.getValue();

	scconf_err = scconf_parse_rt_name_from_cluster_scope(
			oper1, &orig_rt_name, &orig_rt_zc);

	if (scconf_err) {
		clerror("Internal Error.\n");
		return (CL_EINTERNAL);
	}

	if (strcmp(orig_rt_name, CLCOMMANDS_WILD_OPERAND) == 0)
		wildcard = 1;

	if (rtrfile) {
		clerrno = get_type_file_pair(rtrfile, type_file_pair, &isfile,
		    wildcard);
	} else if (wildcard) {
		clerrno = get_type_file_pair(SCRGADM_RTR_DIR,
		    type_file_pair, &isfile, wildcard);
		if (clerrno)
			return (clerrno);
		//
		// Need to check for RT in SCRGADM_RTR_OPT_DIR only if the
		// operation is done for a base cluster. If the registration
		// is done for a zone cluster skip this step
		//
		if (!zc_name) {
			clerrno = get_type_file_pair(SCRGADM_RTR_OPT_DIR,
			    type_file_pair, &isfile, wildcard);
		}
	}

	if (clerrno)
		return (clerrno);

	if (isfile && ((wildcard) || (operands.getSize() > 1))) {
		clerror("You can not specify more than one operand or "
		    "\"%s\" when an RTR file is also specified.\n",
		    CLCOMMANDS_WILD_OPERAND);
		return (CL_EINVAL);
	}

	// Create a list of operands for wildcard case
	if (wildcard) {
		for (type_file_pair.start(); type_file_pair.end() != 1;
		    type_file_pair.next()) {
			if (uniq_rtlist.isValue(type_file_pair.getName())
			    == 0) {
				uniq_rtlist.add(type_file_pair.getName());
			} else {
				clerror("Multiple RTR files defining \"%s\" "
				    "found, aborting.\n",
				    type_file_pair.getName());
				return (CL_EINVAL);
			}
		}

		for (uniq_rtlist.start(); uniq_rtlist.end() != 1;
			uniq_rtlist.next()) {
			// If we are here, we have to prepend the RT name with
			// the zone cluster name
			uniq_rtlist.prepend(zc_name,
				ZC_AND_OBJ_NAME_SEPARATER_CHAR);

		}
	} else {
		for (operands.start(); operands.end() != 1; operands.next()) {
			uniq_rtlist.add(operands.getValue());
		}
	}

	for (uniq_rtlist.start(); uniq_rtlist.end() != 1; uniq_rtlist.next()) {
		if (rtr_results) {
			scrgadm_free_rtrresult(rtr_results);
			rtr_results = NULL;
		}
		if (rt) {
			rgm_free_rt(rt);
			rt = NULL;
		}

		if (rt_name) {
			free(rt_name);
			rt_name = NULL;
		}
		if (zc_name) {
			free(zc_name);
			zc_name = NULL;
		}

		// First get the RT name without the ZC name
		scconf_err = scconf_parse_rt_name_from_cluster_scope(
					uniq_rtlist.getValue(), &rt_name,
								&zc_name);

		if (scconf_err != SCCONF_NOERR) {
			clerror("Internal error.\n");
			clerrno = CL_EINTERNAL;
			break;
		}

		tmp_rtf = NULL;
		memset(name_buf, 0, PATH_MAX);
		if (rtrfile) {
			rv = find_rt_in_list(type_file_pair,
			    rt_name, name_buf, full_rtname_buf,
			    &errflg);
			if (errflg)
				return (errflg);

			if (rv > 1) {
				clerror("Multiple RTR files matching \"%s\" "
				    "found, cannot register \"%s\".\n",
				    uniq_rtlist.getValue(),
				    uniq_rtlist.getValue());
				if (first_err == 0) {
					// first error!
					first_err = CL_EINVAL;
				}
				continue;
			}
			if (rv == 0) {
				if (isfile) {
					clerror("Failed to find \"%s\" in RTR "
					    "file \"%s\".\n",
					    uniq_rtlist.getValue(), rtrfile);
				} else {
					clerror("Failed to find \"%s\" in RTR "
					    "directory \"%s\".\n",
					    uniq_rtlist.getValue(), rtrfile);
				}
				if (first_err == 0) {
					// first error!
					first_err = CL_EINVAL;
				}
				continue;
			}
			tmp_rtf = name_buf;
		}

		//
		// Need to send the zc_name to scrgadm_process_rtreg
		// to be able to scan through the <zoneroot>/SCRGADM_RTR_DIR
		// and <zoneroot>/SCRGADM_RTR_OPT_DIR. Also we need to skip
		// checking for RTR files inside SCRGADM_RTR_OPT_DIR
		//
		scha_status = scrgadm_process_rtreg(rt_name,
		    tmp_rtf, &rtr_results, zc_name);
		rv = scha_status.err_code;
		if (rv != SCHA_ERR_NOERR) {
			print_rgm_error(scha_status, uniq_rtlist.getValue());
			if (first_err == 0) {
				// first error!
				first_err = map_scha_error(rv);
			}
			continue;
		} else if (scha_status.err_msg != NULL) {
			//
			// print warning message if set.
			// The change in NRU tunablity might have generated
			// warning message.
			//
			print_rgm_error(scha_status, uniq_rtlist.getValue());
		}

		scha_status = rgm_scrgadm_parse2rt(rtr_results, &rt, zc_name);
		rv = scha_status.err_code;
		if ((rv != SCHA_ERR_NOERR) || (!rt)) {
			print_rgm_error(scha_status, uniq_rtlist.getValue());
			if (first_err == 0) {
				// first error!
				first_err = map_scha_error(rv);
			}
			continue;
		}
		if ((optflgs & Nflg) || (good_nodes.getSize() == 0)) {
			rt->rt_instl_nodes.is_ALL_value = B_TRUE;
			rt->rt_instl_nodes.nodeids = NULL;
		} else {
			rt->rt_instl_nodes.nodeids = nodeidlist;
			rt->rt_instl_nodes.is_ALL_value = B_FALSE;
		}

		if (system_specified)
			rt->rt_system = system_val;

		scha_status = rgm_scrgadm_register_rt(rt, bypass_install_mode,
							zc_name);
		rv = scha_status.err_code;
		if (rv != SCHA_ERR_NOERR) {
			print_rgm_error(scha_status, uniq_rtlist.getValue());
			if (first_err == 0) {
				// first error!
				first_err = map_scha_error(rv);
			}
			continue;
		} else if (optflgs & vflg) {
			clmessage("Resource type \"%s\" registered.\n",
			    uniq_rtlist.getValue());
		}
	}
	if (rtr_results)
		scrgadm_free_rtrresult(rtr_results);
	if (rt)
		rgm_free_rt(rt);
	if (nodeidlist)
		rgm_free_nodeid_list(nodeidlist);
	if (zc_name)
		free(zc_name);
	if (rt_name)
		free(rt_name);

	return (first_err);
}

int
clrt_register(ClCommand &cmd, ValueList &operands, optflgs_t optflgs,
    char *clconfiguration, ValueList &nodes, char *rtrfile,
    NameValueList &properties)
{
	clerrno_t clerrno = CL_NOERR;
	clerrno_t first_err = CL_NOERR;
	int rv = 0;
	ClXml xml;
	ValueList allproplist;
	ValueList good_nodes;
	ValueList *expanded_list = new ValueList();
	int prop_found = 0;
	char *oper1 = (char *)NULL;
	ValueList rt_list;
	ValueList rtropt;
	NameValueList type_file_pair;
	char name_buf[PATH_MAX];
	char full_rtname_buf[MAXCCRTBLNMLEN];
	clerrno_t errflg = CL_NOERR;

	if (!clconfiguration) {
		clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}

	//
	// If convenience options are used, nodelist property should not be
	// used.
	//
	for (properties.start(); properties.end() != 1; properties.next()) {
		if ((strcasecmp(properties.getName(), SCHA_INSTALLED_NODES)
		    != 0) &&
		    (strcasecmp(properties.getName(), SCHA_RT_SYSTEM) != 0)) {
			clerror("\"%s\" is not an acceptable property for "
			    "this operation.\n", properties.getName());
			return (CL_EPROP);
		}
		if (strcasecmp(properties.getName(), SCHA_INSTALLED_NODES)
		    == 0) {
			if ((nodes.getSize() > 0) || (optflgs & Nflg)) {
				clerror("You cannot specify the \"-n\" or "
				    "\"-N\" option when the \"%s\" property "
				    "is also specified.\n", SCHA_NODELIST);
				return (CL_EINVAL);
			} else {
				nodes.add(properties.getValue());
			}
		}
	}

	clerrno = get_good_nodes(nodes, good_nodes);
	if (clerrno)
		return (clerrno);

	OptionValues *ov = new OptionValues((char *)"add", cmd.argc, cmd.argv);

	// Nodelist
	ov->addOptions(CLRT_NODE, &good_nodes);

	// Properties
	ov->addNVOptions(CLRT_PROP, &properties);

	// RTR file
	rtropt.add(rtrfile);
	ov->addOptions(CLRT_RTRFILE, &rtropt);

	// verbose?
	ov->optflgs = 0;
	if (optflgs & vflg) {
		ov->optflgs |= vflg;
	}

	if (operands.getSize() == 0)
		operands.add((char *)CLCOMMANDS_WILD_OPERAND);

	clerrno = xml.getAllObjectNames(CLRESOURCETYPE_CMD, clconfiguration,
	    expanded_list);
	if (clerrno != CL_NOERR) {
		return (clerrno);
	}
	if (expanded_list->getSize() == 0) {
		clerror("Cannot find any resource type in \"%s\".\n",
		    clconfiguration);
		return (CL_EINVAL);
	}

	for (expanded_list->start(); expanded_list->end() != 1;
	    expanded_list->next()) {
		type_file_pair.add(expanded_list->getValue(), "xmlfile");
	}

	operands.start();
	oper1 = operands.getValue();
	if (strcmp(oper1, (char *)CLCOMMANDS_WILD_OPERAND) == 0) {
		for (expanded_list->start(); expanded_list->end() != 1;
		    expanded_list->next()) {
			rt_list.add(expanded_list->getValue());
		}
	} else {
		for (operands.start(); operands.end() != 1; operands.next()) {
			rv = find_rt_in_list(type_file_pair,
			    operands.getValue(), name_buf, full_rtname_buf,
			    &errflg);
			if (errflg)
				return (errflg);

			if (rv > 1) {
				clerror("Multiple instances of \"%s\" found in "
				    "\"%s\", can not register \"%s\".\n",
				    operands.getValue(),
				    clconfiguration,
				    operands.getValue());
				if (first_err == 0) {
					// first error!
					first_err = CL_EINVAL;
				}
				continue;
			}
			if (rv == 0) {
				clerror("Cannot find \"%s\" in \"%s\".\n",
				    operands.getValue(), clconfiguration);
				if (first_err == 0) {
					// first error!
					first_err = CL_ENOENT;
				}
				continue;
			}
			rt_list.add(full_rtname_buf);
		}
	}

	for (rt_list.start(); !rt_list.end(); rt_list.next()) {
		ov->setOperand(rt_list.getValue());
		clerrno = xml.applyConfig(CLRESOURCETYPE_CMD, clconfiguration,
		    ov);
		if (clerrno) {
			if (first_err == 0) {
				// first error!
				first_err = clerrno;
			}
		}
	}

cleanup:
	delete ov;
	delete expanded_list;

	return (first_err);
}

//
// Given a rtrfile, return a list of RT-RTRFILE list. This is useful if
// rtrfile is a directory. If it is a regular file, there is just one element
// in the list. Also set reg_file if rtrfile is a regular file.
//
static clerrno_t
get_type_file_pair(char *rtrfile, NameValueList &type_file_pair, int *isfile,
    int wildcard)
{
	struct stat sb;
	int reg_file = 0;
	int reg_dir = 0;
	char full_rtname[FULLRT_MAXNAMELEN];
	char full_path[PATH_MAX];
	int rv = 0;
	clerrno_t clerrno = CL_NOERR;
	DIR *dirp;
	struct dirent *dp;

	*isfile = 0;

	rv = stat(rtrfile, &sb);
	if (rv) {
		clerror("Can not access file \"%s\" - %s.\n", rtrfile,
		    strerror(errno));
		return (CL_EINVAL);
	}

	reg_file = sb.st_mode & S_IFREG;
	if (reg_file)
		*isfile = 1;

	reg_dir = sb.st_mode & S_IFDIR;
	if (!reg_file && !reg_dir) {
		clerror("Specified file \"%s\" is not an ordinary file or "
		    "a directory.\n");
		return (CL_EINVAL);
	}
	if (reg_file) {
		memset(full_rtname, 0, sizeof (full_rtname));
		clerrno = get_type_in_file(rtrfile, full_rtname, wildcard);
		if (clerrno) {
			clerror("Failed to use the file \"%s\".\n", rtrfile);
			return (clerrno);
		}
		type_file_pair.add(full_rtname, rtrfile);
		return (CL_NOERR);
	}

	dirp = opendir(rtrfile);
	if (!dirp) {
		clerror("Failed to access \"%s\" - %s.\n", rtrfile,
		    strerror(errno));
		return (CL_EINVAL);
	}

	while ((dp = readdir(dirp)) != NULL) {
		// slot not in use
		if (dp->d_ino == 0)
			continue;
		// ignore '.' and '..'
		if ((strcmp(dp->d_name, ".") == 0) ||
		    (strcmp(dp->d_name, "..") == 0))
			continue;

		sprintf(full_path, "%s/%s", rtrfile, dp->d_name);
		if (stat(full_path, &sb) != 0) {
			if (wildcard)
				clwarning("Warning: failed to access file  "
				    "\"%s\".\n", full_path);
			continue;
		}
		reg_file = sb.st_mode & S_IFREG;
		if (!(sb.st_mode & S_IFREG)) {
			if (wildcard)
				clerror("Warning: file \"%s\" is not an "
				    "ordinary file.\n", full_path);
			continue;
		}

		memset(full_rtname, 0, sizeof (full_rtname));
		clerrno = get_type_in_file(full_path, full_rtname, wildcard);
		if (clerrno == 0)
			type_file_pair.add(full_rtname, full_path);
	}
	(void) closedir(dirp);

	if (type_file_pair.getSize() == 0) {
		clerror("Failed to find any resource type information in the "
		    "directory \"%s\".\n", rtrfile);
		return (CL_EINVAL);
	}
	return (CL_NOERR);
}

//
// Fetch type name by looking into the rtr file. This is not a full-proof
// method and the user could easily fool the system by having bad content.
// But those things will be caught by RGM anyway later.
//
static clerrno_t
get_type_in_file(char *rtrfile, char *full_rtname, int wildcard)
{
	int rv = 0;
	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};

	scha_status = scrgadm_get_type_in_file(rtrfile, full_rtname);
	rv = scha_status.err_code;
	if (rv != SCHA_ERR_NOERR) {
		print_rgm_error(scha_status, NULL);
		if (wildcard)
			clerror("Warning: failed to open or parse RTR file "
			    "\"%s\".\n", rtrfile);
		return (map_scha_error(rv));
	}

	return (CL_NOERR);
}

//
// Return the number of matches found in the list, and the matching file.
//
int
find_rt_in_list(NameValueList &type_file_pair, char *rtname, char *name_buf,
    char *full_rtname, int *errflg)
{
	int num_matches = 0;
	char *tmp_typename = (char *)NULL;
	char *poss1 = (char *)NULL;
	char *poss2 = (char *)NULL;
	char *poss3 = (char *)NULL;
	char *first_dot, *last_colon;
	char *main_name, *vers, *prefix;

	*errflg = 0;

	//
	// If the stored rt is v.n:v, possible matches are v.n, n, n:v and
	// v.n:v
	//
	for (type_file_pair.start(); type_file_pair.end() != 1;
	    type_file_pair.next()) {
		if (tmp_typename) {
			free(tmp_typename);
			tmp_typename = NULL;
		}
		if (poss1) {
			free(poss1);
			poss1 = NULL;
		}
		if (poss2) {
			free(poss2);
			poss2 = NULL;
		}
		if (strcmp(type_file_pair.getName(), rtname) == 0) {
			sprintf(name_buf, "%s", type_file_pair.getValue());
			sprintf(full_rtname, "%s", type_file_pair.getName());
			num_matches++;
			continue;
		}
		tmp_typename = strdup(type_file_pair.getName());
		if (tmp_typename == NULL) {
			clcommand_perror(CL_ENOMEM);
			*errflg = CL_ENOMEM;
			return (0);
		}
		vers = "";
		prefix = "";
		last_colon = strrchr(tmp_typename, ':');
		if (last_colon) {
			vers = last_colon + 1;
			*last_colon = NULL;
		}
		first_dot = strchr(tmp_typename, '.');
		if (first_dot) {
			main_name = first_dot + 1;
			*first_dot = NULL;
			prefix = tmp_typename;
		} else {
			main_name = tmp_typename;
		}
		poss1 = (char *)calloc(strlen(prefix) + strlen(main_name) + 3,
		    sizeof (char));
		if (poss1 == NULL) {
			clcommand_perror(CL_ENOMEM);
			*errflg = CL_ENOMEM;
			return (0);
		}
		strcat(poss1, prefix);
		strcat(poss1, ".");
		strcat(poss1, main_name);
		if (strcmp(poss1, rtname) == 0) {
			sprintf(name_buf, "%s", type_file_pair.getValue());
			sprintf(full_rtname, "%s", type_file_pair.getName());
			num_matches++;
			continue;
		}
		poss2 = (char *)calloc(strlen(vers) + strlen(main_name) + 3,
		    sizeof (char));
		if (poss2 == NULL) {
			clcommand_perror(CL_ENOMEM);
			*errflg = CL_ENOMEM;
			return (0);
		}
		strcat(poss2, main_name);
		strcat(poss2, ":");
		strcat(poss2, vers);
		if (strcmp(poss2, rtname) == 0) {
			sprintf(name_buf, "%s", type_file_pair.getValue());
			sprintf(full_rtname, "%s", type_file_pair.getName());
			num_matches++;
			continue;
		}
		poss3 = main_name;
		if (strcmp(poss3, rtname) == 0) {
			sprintf(name_buf, "%s", type_file_pair.getValue());
			sprintf(full_rtname, "%s", type_file_pair.getName());
			num_matches++;
			continue;
		}
	}

	if (tmp_typename)
		free(tmp_typename);
	if (poss1)
		free(poss1);
	if (poss2)
		free(poss2);

	return (num_matches);
}
