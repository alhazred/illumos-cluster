//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clsnmphost_add.cc	1.4	08/10/13 SMI"

//
// Process clsnmphost "add"
//

#include "clsnmphost.h"

#include "ClCommand.h"
#include "ClXml.h"

//
// Adds one or more SNMP hosts to the configuration.
//
// Return values:
//
//	CL_NOERR
//	CL_EINTERNAL
//	CL_ENOENT
//
clerrno_t
clsnmphost_add(
    ClCommand &cmd,
    ValueList &operands,
    ValueList &nodes,
    ValueList &communities)
{
	clerrno_t err = CL_NOERR;
	int outofcluster = 0;

	// Verify the nodelist
	if (nodes.getSize() == 0) {
		set_error(err, add_this_node(nodes));
	}
	set_error(err, verify_nodelist(nodes));

	// Verify if the nodes in the nodelist are active cluster members.
	set_error(err, verify_nodes_clustmode(nodes, outofcluster));

	if (outofcluster)
		clerror("SNMP hosts cannot be added to offline nodes.\n");

	// Add the default community
	if (communities.getSize() == 0)
		communities.add("public");

	// Call the sceventmib shell script to add the host(s)
	//lint -e(1550)
	char **argv = new char *[8];
	argv[0] = strdup("sceventmib");	// The command
	argv[1] = strdup("-x");		// CLCMD flag
	argv[2] = strdup("-a");		// The add flag
	argv[3] = strdup("-h");		// The host flag
	argv[4] = NULL;			// The host
	argv[5] = strdup("-c");		// The community flag
	argv[6] = NULL;			// The community
	argv[7] = NULL;

	for (operands.start();
	    !operands.end();
	    operands.next()) {

		argv[4] = strdup(operands.getValue());
		for (communities.start();
		    !communities.end();
		    communities.next()) {

			argv[6] = strdup(communities.getValue());
			for (nodes.start();
			    !nodes.end();
			    nodes.next()) {

				// Run the command
				clerrno_t status = run_sceventmib_cmd_on_node(
				    argv, strdup(nodes.getValue()));

				set_error(err, status);
				if (!status && cmd.isVerbose) {
					clmessage("Added SNMP host "
					    "\"%s\" to community \"%s\" on "
					    "node \"%s\".\n",
					    operands.getValue(),
					    communities.getValue(),
					    nodes.getValue());
				}
			}
		}
	}

	delete [] argv[7];
	delete [] argv[6];
	delete [] argv[5];
	delete [] argv[4];
	delete [] argv[3];
	delete [] argv[2];
	delete [] argv[1];
	delete [] argv[0];
	delete [] argv;

	return (err);
}

//
// Adds one or more SNMP hosts to the configuration using the information
// contained in the clconfig XML file.
//
// Return values:
//
//	CL_NOERR
//	CL_EINTERNAL
//	CL_ENOENT
//	CL_ENOMEM
//
clerrno_t
clsnmphost_add(
    ClCommand &cmd,
    ValueList &operands,
    ValueList &nodes,
    ValueList &communities,
    char *clconfig)
{
	clerrno_t err = CL_NOERR;
	ClXml xml;

	//lint -e(1550)
	OptionValues *ov = new OptionValues("add");

	// No xml file
	if (!clconfig) {
		err = CL_EINTERNAL;
		goto cleanup;
	}

	// Cannot allocate mem for obj
	if (ov == NULL) {
		err = CL_ENOMEM;
		goto cleanup;
	}

	// Communities
	ov->addOptions(CLSNMP_COMMUNITY, &communities);

	// If no nodes are specified, we must then specify this node
	if (nodes.getSize() == 0) {
		set_error(err, add_this_node(nodes));
	}
	ov->addOptions(CLSNMP_NODE, &nodes);

	// verbose?
	if (cmd.isVerbose) {
		ov->addOption(VERBOSE);
	}

	// Add wildcard
	if (operands.getSize() == 0)
		operands.add((char *)CLCOMMANDS_WILD_OPERAND);

	// Apply the config for each operand
	for (operands.start();
	    !operands.end();
	    operands.next()) {

		ov->setOperand(operands.getValue());
		set_error(err, xml.applyConfig(CLSNMPHOST_CMD, clconfig, ov));
	}

cleanup:
	delete ov;

	return (err);
}
