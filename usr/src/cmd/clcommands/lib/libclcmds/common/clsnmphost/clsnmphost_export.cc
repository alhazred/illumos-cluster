//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clsnmphost_export.cc	1.3	08/05/20 SMI"

//
// Process clsnmphost "export"
//

#include "clsnmphost.h"

#include "ClCommand.h"
#include "ClXml.h"

clerrno_t get_export_obj(ClsnmphostExport &export_obj, ValueList &operands,
    ValueList &nodes, ValueList &communities, int &found_host);

clerrno_t parse_stdout_to_snmp_host_xml(ClsnmphostExport &export_obj,
    FILE *file, char *node, int &found_host);

//
// This function returns a ClsnmphostExport object populated with
// all of the snmphost information from the cluster.  Caller is
// responsible for releasing the ClcmdExport object.
//
ClcmdExport *
get_clsnmphost_xml_elements()
{
	// Get the cluster nodes
	ValueList nodes;
	(void) get_all_cluster_nodes(nodes);
	ValueList operands;
	ValueList communities;
	int fh = 0;

	//lint -e(1550)
	ClsnmphostExport *host = new ClsnmphostExport();
	(void) get_export_obj(*host, operands, nodes, communities, fh);
	return (host);
}

//
// Populates export_obj with the SNMP host configuration information
// based on the operands, nodes, and communities specified.
//
// Return values:
//
//	CL_NOERR
//	CL_EINTERNAL
//	CL_ENOENT
//
clerrno_t
get_export_obj(
    ClsnmphostExport &export_obj,
    ValueList &operands,
    ValueList &nodes,
    ValueList &communities,
    int &found_host)
{
	clerrno_t err = CL_NOERR;
	nodeid_t nodeid;
	uint_t ismember;

	// create the command to get the hosts
	char args[BUFSIZ];
	args[0] = '\0';
	(void) strcat(args, "-x -X -p host-list -v ");

	// Operand list
	if (operands.getSize() != 0)
		(void) strcat(args, "-z ");

	// Add the operands
	bool first = true;
	for (operands.start();
	    !operands.end();
	    operands.next()) {

		if (!first)
			(void) strcat(args, ",");

		(void) strcat(args, operands.getValue());
		first = false;
	}

	// Communities
	if (communities.getSize() != 0)
		(void) strcat(args, " -c ");

	// Add the communities
	first = true;
	for (communities.start();
	    !communities.end();
	    communities.next()) {

		if (!first)
			(void) strcat(args, ",");

		(void) strcat(args, communities.getValue());
		first = false;
	}

	// Do it on each node
	for (nodes.start();
	    !nodes.end();
	    nodes.next()) {

		//
		// Check to see if the node is an active cluster member.
		// For cases like cluster export, we may reach here with
		// the nodelist having nodes which are out of the cluster
		// at that point of time...
		//
		set_error(err, scconf_get_nodeid(nodes.getValue(), &nodeid));

		(void) scconf_ismember(nodeid, &ismember);

		if (!ismember) {
			clerror("SNMP host information cannot be retrieved "
			    "from offline node \"%s\".\n", nodes.getValue());
			continue;
		}

		// Get a FILE * with the stdout from the cmd
		FILE *file = NULL;
		set_error(err, pipe_sceventmib_cmd_on_node(
		    &file, args, nodes.getValue()));

		// Parse the data from stdout and add it to the
		// ClsnmphostExport object.
		set_error(err, parse_stdout_to_snmp_host_xml(
		    export_obj, file, nodes.getValue(), found_host));

		// Close the pipe
		(void) pclose(file);
	}

	return (err);
}

//
// Parses the data from a stdout pipe, file, into the SNMP host XML
// configuration information.
//
// Return values:
//
//	CL_NOERR
//	CL_EINTERNAL
//
clerrno_t
parse_stdout_to_snmp_host_xml(
    ClsnmphostExport &export_obj,
    FILE *file,
    char *node,
    int &found_host)
{
	if (!node)
		return (CL_EINTERNAL);

	clerrno_t err = strip_list_headings(file);

	char name[128];
	name[0] = '\0';
	char temp[128];
	temp[0] = '\0';
	while (fscanf(file, "%s", temp) != EOF) {
		// Now we are at the data that we want.  It is of the form:
		// <name> <community>\n
		if (strcmp(temp, EXIT_MESSAGE) == 0) {
			fscanf(file, "%d", &err);
			return (conv_err_from_sceventmib(err));
		} else if (strcmp(name, "") == 0) {
			(void) sprintf(name, temp);
			found_host++;
		} else {
			(void) export_obj.createSNMPHost(
			    name,
			    node,
			    strdup(temp));
			name[0] = '\0';
		}

		temp[0] = '\0';
	}

	// shouldn't get here
	return (CL_EINTERNAL);
}

//
// Exports the SNMP host information in xml format to either stdout or a file,
// depending on the value of clconfiguration.
//
// Return values:
//
//	CL_NOERR
//	CL_EINTERNAL
//	CL_ENOENT
//
clerrno_t
clsnmphost_export(ValueList &operands,
    ValueList &nodes, ValueList &communities,
    char *clconfiguration)
{
	clerrno_t err = CL_NOERR;
	int outofcluster = 0;

	// Set the doc to be stdout
	if (!clconfiguration)
		clconfiguration = strdup("-");

	// Verify the nodelist
	if (nodes.getSize() == 0) {
		set_error(err, add_this_node(nodes));
	} else {
		set_error(err, verify_nodelist(nodes));
	}

	// Verify if the nodes in the nodelist are active cluster members.
	set_error(err, verify_nodes_clustmode(nodes, outofcluster));

	if (outofcluster)
		clerror("SNMP host information cannot be retrieved from "
		    "offline nodes.\n");

	// Get the ClsnmphostExport object
	ClXml xml;
	int found_host = 0;
	ClsnmphostExport export_obj;
	set_error(err, get_export_obj(export_obj, operands,
	    nodes, communities, found_host));

	if (!found_host)
		return (err);

	// Create the file and print it to either stdout or a file
	set_error(err, xml.createExportFile(
	    CLSNMPHOST_CMD,
	    clconfiguration,
	    &export_obj));

	return (err);
}
