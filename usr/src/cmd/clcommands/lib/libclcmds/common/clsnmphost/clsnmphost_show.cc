//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clsnmphost_show.cc	1.4	08/09/22 SMI"

//
// Process clsnmphost "show"
//

#include "clsnmphost.h"

#include "ClCommand.h"

//
// Prints the SNMP host configuration information using the ClShow
// class to format and print the data.
//
// Return values:
//
//	CL_NOERR
//	CL_EINTERNAL
//	CL_ENOENT
//
clerrno_t
clsnmphost_show(
    ValueList &operands,
    ValueList &nodes,
    ValueList &communities)
{
	clerrno_t err = CL_NOERR;
	int outofcluster = 0;

	// Verify the nodelist
	if (nodes.getSize() == 0) {
		set_error(err, add_this_node(nodes));
	}
	set_error(err, verify_nodelist(nodes));

	// Verify if the nodes in the nodelist are active cluster members.
	set_error(err, verify_nodes_clustmode(nodes, outofcluster));

	if (outofcluster)
		clerror("SNMP host information cannot be retrieved from "
		    "offline nodes.\n");

	// Do it on each node
	for (nodes.start();
	    !nodes.end();
	    nodes.next()) {

		clerrno_t error = CL_NOERR;
		ClShow *show_obj = get_filtered_clsnmphost_show_obj(
		    operands, communities, nodes.getValue(), error);

		show_obj->print();

		set_error(err, error);
	}

	return (err);
}

//
// Returns a ClShow object containing the SNMP hosts on a node.
// The argument, error, will be set to an error, if one was encountered.
//
ClShow *
get_filtered_clsnmphost_show_obj(
    ValueList &operands,
    ValueList &communities,
    char *node,
    clerrno_t &error)
{
	// Call the sceventmib shell script to show the host(s)
	char args[BUFSIZ];
	args[0] = '\0';
	(void) sprintf(args, "-x -X -v -p host-list ");

	// Operand list
	if (operands.getSize() != 0)
		(void) sprintf(args, "%s -z ", args);

	// Add the operands
	bool first = true;
	for (operands.start();
	    !operands.end();
	    operands.next()) {

		if (!first)
			(void) strcat(args, ",");

		(void) strcat(args, operands.getValue());
		first = false;
	}

	// Communities
	if (communities.getSize() != 0)
		(void) strcat(args, " -c ");

	// Add the communities
	first = true;
	for (communities.start();
	    !communities.end();
	    communities.next()) {

		if (!first)
			(void) strcat(args, ",");

		(void) strcat(args, communities.getValue());
		first = false;
	}

	// Get a FILE * with the stdout from the cmd
	FILE *file = NULL;
	set_error(error, pipe_sceventmib_cmd_on_node(
	    &file, args, node));

	// Parse the data from stdout and add it to the
	// ClShow object
	ClShow *show_obj = new ClShow(HEAD_SNMPHOST, node);
	set_error(error, parse_stdout_to_snmp_host_show(show_obj, file));

	// Close the pipe
	(void) pclose(file);

	return (show_obj);
}

//
// Returns a ClShow object for the SNMP host information on a node.
// No filtering of the data is performed.
//
ClShow *
get_clsnmphost_show_obj(char *node)
{
	ValueList empty;
	clerrno_t value;
	return (get_filtered_clsnmphost_show_obj(empty, empty, node, value));
}

//
// Parses the output from the FILE * into a ClShow object.  Returns an
// error encountered during parsing, or from the EXIT_MESSAGE value
// from the cmd output.
//
clerrno_t
parse_stdout_to_snmp_host_show(ClShow *&show_obj, FILE *file)
{
	clerrno_t err = strip_list_headings(file);

	char name[128];
	name[0] = '\0';
	char temp[128];
	temp[0] = '\0';
	while (fscanf(file, "%s", temp) != EOF) {
		// Now we are at the data that we want.  It is of the form:
		// <name> <community>\n

		// Get the exit status from the piped cmd
		if (strcmp(temp, EXIT_MESSAGE) == 0) {
			fscanf(file, "%d", &err);
			return (conv_err_from_sceventmib(err));

		// Get the hostname
		} else if (strcmp(name, "") == 0) {
			(void) sprintf(name, temp);
			show_obj->addObject(
			    (char *)gettext("SNMP Host Name"),
			    name);

		// Get the community
		} else {
			show_obj->addProperty(name,
			    (char *)gettext("Community"),
			    temp);
			name[0] = '\0';
		}

		temp[0] = '\0';
	}

	// shouldn't get here
	return (CL_EINTERNAL);
}
