//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clsnmphost_remove.cc	1.4	08/10/13 SMI"

//
// Process clsnmphost "remove"
//

#include "clsnmphost.h"

#include "ClCommand.h"

//
// Removes one or more SNMP hosts from one or more nodes, depending on the value
// of operands, nodes, and communities.
//
// Return values:
//
//	CL_NOERR
//	CL_ENOENT
//	CL_EINTERNAL
//
clerrno_t
clsnmphost_remove(
    ClCommand &cmd,
    ValueList &operands,
    ValueList &nodes,
    ValueList &communities)
{
	clerrno_t err = CL_NOERR;
	int outofcluster = 0;

	// Verify the nodelist
	if (nodes.getSize() == 0) {
		set_error(err, add_this_node(nodes));
	}
	set_error(err, verify_nodelist(nodes));

	// Verify if the nodes in the nodelist are active cluster members.
	set_error(err, verify_nodes_clustmode(nodes, outofcluster));

	if (outofcluster)
		clerror("SNMP hosts cannot be removed from offline nodes.\n");

	// Call the sceventmib shell script to remove the host(s)
	//lint -e(1550)
	char **argv = new char *[8];
	argv[0] = strdup("sceventmib");	// The command
	argv[1] = strdup("-x");		// CLCMD flag
	argv[2] = strdup("-r");		// The remove flag
	argv[3] = strdup("-h");		// The host flag
	argv[4] = NULL;			// The host
	argv[5] = strdup("-c");		// The community flag
	argv[6] = NULL;			// The community
	argv[7] = NULL;

	for (nodes.start();
	    !nodes.end();
	    nodes.next()) {

		// Get the communities on this node
		bool all_comms = false;
		if (communities.getSize() == 0) {
			set_error(err, get_all_snmp_communities(
			    communities, nodes.getValue()));
			all_comms = true;
		}

		int num_comms = communities.getSize();
		int count = 0;
		bool host_found = false;

		// No communities, eg, no hosts either
		if (num_comms == 0) {
			clerror("No hosts are configured on node \"%s\".\n",
			    nodes.getValue());

			set_error(err, CL_ENOENT);
			continue;
		}

		for (communities.start();
		    !communities.end();
		    communities.next()) {

			count++;

			// Operands
			bool all_oper = false;
			if (operands.getSize() == 0) {
				set_error(err, get_all_snmp_hosts(operands,
				    communities.getValue(), nodes.getValue()));
				all_oper = true;
			}

			for (operands.start();
			    !operands.end();
			    operands.next()) {

				// set the hostname
				argv[4] = operands.getValue();

				// set the community
				argv[6] = communities.getValue();

				// run the command
				clerrno_t status = run_sceventmib_cmd_on_node(
				    argv, strdup(nodes.getValue()));

				// host was found
				if (!status)
					host_found = true;

				// Check to see if the host was found in
				// any community
				if ((count == num_comms) &&
				    (status == CL_ENOENT) && !host_found) {

					clerror("Host \"%s\" not found.\n",
					    operands.getValue());
					set_error(err, CL_ENOENT);
				}

				// Set the error only if it was not an error
				// finding the host
				if (status != CL_ENOENT)
					set_error(err, status);

				if (!status && cmd.isVerbose) {
					clmessage("Removed SNMP host "
					    "\"%s\" from community \"%s\" "
					    "on node \"%s\".\n",
					    operands.getValue(),
					    communities.getValue(),
					    nodes.getValue());
				}
			}

			if (all_oper)
				operands.clear();
		}

		if (all_comms)
			communities.clear();
	}

	delete [] argv[7];
	delete [] argv[6];
	delete [] argv[5];
	delete [] argv[4];
	delete [] argv[3];
	delete [] argv[2];
	delete [] argv[1];
	delete [] argv[0];
	delete [] argv;

	return (err);
}

//
// Populates the communities ValueList with all the communities on the
// specified node.  The contents of the communities ValueList is first cleared.
//
// Return values:
//
//	CL_NOERR
//	CL_EINTERNAL
//
clerrno_t
get_all_snmp_communities(ValueList &communities, char *node)
{
	clerrno_t err = CL_NOERR;

	if (!node)
		return (CL_EINTERNAL);

	// command arg string
	char args[] = "-x -v -p host-list | sed 1,5d | awk '{ print $2 }'";

	// run the command and open a pipe
	FILE *file = NULL;
	set_error(err, pipe_sceventmib_cmd_on_node(&file, args, node));

	communities.clear();

	// Get the communities and populate the ValueList
	char c;
	int count = 0;
	char temp[128];
	temp[0] = '\0';
	while ((c = (char)fgetc(file)) != EOF) {

		if (c == '\n') {
			temp[count] = '\0';
			communities.add(temp);
			count = 0;
			temp[count] = '\0';
		} else {
			temp[count++] = c;
		}
	}

	// close the pipe
	(void) pclose(file);

	return (err);
}

//
// Populates the hosts ValueList with all the hosts which are in a specific
// community, on a specific node.  The contents of the hosts ValueList are
// first deleted.
//
// Return values:
//
//	CL_NOERR
//	CL_EINTERNAL
//
clerrno_t
get_all_snmp_hosts(
    ValueList &hosts,
    char *community,
    char *node)
{
	clerrno_t err = CL_NOERR;

	if (!node || !community)
		return (CL_EINTERNAL);

	// build the command arg string
	char args[256];
	(void) sprintf(args, "-x -p host-list -c %s", community);

	// run the command and open a pipe
	FILE *file = NULL;
	set_error(err, pipe_sceventmib_cmd_on_node(&file, args, node));

	hosts.clear();

	// Get the hosts and populate the ValueList
	char c;
	int count = 0;
	char temp[128];
	temp[0] = '\0';
	while ((c = (char)fgetc(file)) != EOF) {

		if (c == '\n') {
			temp[count] = '\0';
			hosts.add(temp);
			count = 0;
			temp[count] = '\0';
		} else {
			temp[count++] = c;
		}
	}

	// close the pipe
	(void) pclose(file);

	return (err);
}
