//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clsnmphost_list.cc	1.3	08/05/20 SMI"

//
// Process clsnmphost "list"
//

#include "clsnmphost.h"

#include "ClCommand.h"

//
// Lists the SNMP host information on one or many nodes.  If any node was not
// found then it is an error.  The output can be filtered by specifying one or
// more community names in which to search.
//
// Return values:
//
//	CL_NOERR
//	CL_EINTERNAL
//	CL_ENOENT
//
clerrno_t
clsnmphost_list(
    ClCommand &cmd,
    ValueList &operands,
    ValueList &nodes,
    ValueList &communities)
{
	clerrno_t err = CL_NOERR;
	int outofcluster = 0;

	// Verify the nodelist
	if (nodes.getSize() == 0) {
		set_error(err, add_this_node(nodes));
	}
	set_error(err, verify_nodelist(nodes));

	// Verify if the nodes in the nodelist are active cluster members.
	set_error(err, verify_nodes_clustmode(nodes, outofcluster));

	if (outofcluster)
		clerror("SNMP host information cannot be retrieved from "
		    "offline nodes.\n");

	// Call the sceventmib shell script to list the host(s)
	char args[BUFSIZ];
	(void) sprintf(args, "-x -p host-list ");

	// Verbose?
	if (cmd.isVerbose)
		(void) strcat(args, "-v ");

	// Operand list
	if (operands.getSize() != 0)
		(void) strcat(args, "-z ");

	// Add operands
	bool first = true;
	for (operands.start();
	    !operands.end();
	    operands.next()) {

		if (!first)
			(void) strcat(args, ",");

		(void) strcat(args, operands.getValue());
		first = false;
	}

	// Communities
	if (communities.getSize() != 0)
		(void) strcat(args, " -c ");

	// Add communities
	first = true;
	for (communities.start();
	    !communities.end();
	    communities.next()) {

		if (!first)
			(void) strcat(args, ",");

		(void) strcat(args, communities.getValue());
		first = false;
	}

	// Do it for each node
	for (nodes.start();
	    !nodes.end();
	    nodes.next()) {

		// execute the command
		clerrno_t status = run_sceventmib_cmd_on_node(
		    args, nodes.getValue());

		set_error(err, status);
	}

	return (err);
}
