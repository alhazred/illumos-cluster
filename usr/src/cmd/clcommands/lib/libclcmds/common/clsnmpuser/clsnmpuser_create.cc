//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clsnmpuser_create.cc	1.5	08/10/13 SMI"

//
// Process clsnmpuser "create"
//

#include "clsnmpuser.h"

#include "ClCommand.h"
#include "ClXml.h"

#include <termio.h>

//
// Creates one or more SNMP users to the configuration on one or more nodes.
//
// Return values:
//
//	CL_NOERR
//	CL_EINTERNAL
//	CL_EIO
//	CL_EEXIST
//
clerrno_t
clsnmpuser_create(
    ClCommand &cmd,
    ValueList &operands,
    ValueList &nodes,
    char *auth,
    char *passfile)
{
	clerrno_t err = CL_NOERR;
	int outofcluster = 0;

	// Verify the nodelist
	if (nodes.getSize() == 0) {
		set_error(err, add_this_node(nodes));
	}
	set_error(err, verify_nodelist(nodes));

	// Verify if the nodes in the nodelist are active cluster members.
	set_error(err, verify_nodes_clustmode(nodes, outofcluster));

	if (outofcluster)
		clerror("SNMP users cannot be created on offline nodes.\n");

	// Get the passwords if not already set
	bool delete_file = false;
	if (!passfile) {
		set_error(err, get_passwords_from_stdin(operands, passfile));
		delete_file = true;
	}

	// Do it on each node
	for (nodes.start();
	    !nodes.end();
	    nodes.next()) {

		for (operands.start();
		    !operands.end();
		    operands.next()) {

			// Call the sceventmib shell script to add the user(s)
			char args[BUFSIZ];
			args[0] = '\0';

			// This is the command
			(void) sprintf(args, "-x -a -u %s -t %s",
			    operands.getValue(), auth);

			// Add the -f flag for scrcmd to append a filename to
			// The scrcmd cmd will append the filename
			(void) sprintf(args, "%s -f", args);

			// run the command
			clerrno_t status = run_sceventmib_cmd_on_node_with_file(
			    args, strdup(nodes.getValue()), strdup(passfile));

			set_error(err, status);
			if (!status && cmd.isVerbose) {
				clmessage("Added SNMP user "
				    "\"%s\" on node \"%s\".\n",
				    operands.getValue(),
				    nodes.getValue());
			}
		}
	}

	// Delete the temporary file, if there is one
	if (delete_file && remove(passfile) != 0) {
		clwarning("Unable to delete file %s - %s.\n", passfile,
		    strerror(errno));
	}

	return (err);
}

//
// Creates one or more SNMP users to the configuration using the information
// contained in the clconfig XML file.
//
// Return values:
//
//	CL_NOERR
//	CL_ENOENT
//	CL_EEXIST
//	CL_ENOMEM
//
clerrno_t
clsnmpuser_create(
    ClCommand &cmd,
    ValueList &operands,
    ValueList &nodes,
    ValueList &auths,
    char *passfile,
    char *clconfig)
{
	if (!clconfig || !passfile)
		return (CL_EINTERNAL);

	clerrno_t err = CL_NOERR;

	OptionValues *ov = new OptionValues((char *)"create");
	ClXml xml;
	if (ov == NULL) {
		err = CL_ENOMEM;
		goto cleanup;
	}

	// Auth types
	ov->addOptions(CLSNMP_AUTH, &auths);

	// If no nodes are specified, we must then specify this node
	if (nodes.getSize() == 0) {
		set_error(err, add_this_node(nodes));
	}
	ov->addOptions(CLSNMP_NODE, &nodes);

	// verbose?
	if (cmd.isVerbose) {
		ov->addOption(VERBOSE);
	}

	// passfile
	ov->addOption(CLSNMP_PASSFILE, passfile);

	if (operands.getSize() == 0)
		operands.add((char *)"+");

	for (operands.start();
	    !operands.end();
	    operands.next()) {

		ov->setOperand(operands.getValue());
		set_error(err, xml.applyConfig(CLSNMPUSER_CMD, clconfig, ov));
	}

cleanup:
	delete ov;

	return (err);
}

//
// Reads a password for each user via stdin.  The echo is turned off so that
// passwords are not visible as they are entered.  The passwords are stored in
// a temp file, whose name is stored in passfile.  The file is saved in the form
// <user>:<password>
//
// Return values:
//
//	CL_NOERR
//	CL_EIO
//
clerrno_t
get_passwords_from_stdin(ValueList &users, char *&passfile)
{
	// set the filename, if not set
	if (!passfile) {
		passfile = new char[BUFSIZ];
		(void) sprintf(passfile, "/var/cluster/run/clsnmpuser.%d",
		    getpid());
	}

	// Open the file for writing
	FILE *fp = fopen(passfile, "w");
	if (fp == NULL) {
		clerror("Cannot open password file for writing - %s.\n",
		    strerror(errno));

		return (CL_EIO);
	}

	// For each user in the ValueList, ask the user to enter a password
	// Write the password to the temp file of name passfile
	for (users.start();
	    !users.end();
	    users.next()) {

		// Read the password from stdin
		char inputbuff[BUFSIZ];
		char *passwrd = (char *)0;

		(void) sprintf(inputbuff, "%s \"%s\": ",
		    gettext("Enter password for user"),
		    users.getValue());
		passwrd = getpassphrase(inputbuff);

		// Write the password and the username to the tempfile stream
		(void) fprintf(fp, "%s:%s\n", users.getValue(), passwrd);
	}

	// Close the file
	(void) fclose(fp);

	return (CL_NOERR);
}
