//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clsnmpuser_set_default.cc	1.4	08/10/13 SMI"

//
// Process clsnmpuser "set-default"
//

#include "clsnmpuser.h"

#include "ClCommand.h"

//
// Modifies one or more SNMP users on one or more nodes.
//
// Return values:
//
//	CL_NOERR
//	CL_EINTERNAL
//	CL_ENOENT
//
clerrno_t
clsnmpuser_set_default(
    ClCommand &cmd,
    ValueList &operands,
    ValueList &nodes,
    char *seclevel)
{
	clerrno_t err = CL_NOERR;
	int outofcluster = 0;

	// Verify the nodelist
	if (nodes.getSize() == 0) {
		set_error(err, add_this_node(nodes));
	}
	set_error(err, verify_nodelist(nodes));

	// Verify if the nodes in the nodelist are active cluster members.
	set_error(err, verify_nodes_clustmode(nodes, outofcluster));

	if (outofcluster)
		clerror("SNMP user default properties cannot be changed on "
		    "offline nodes.\n");

	// Call the sceventmib shell script to modify each user
	// on the nodes listed
	for (nodes.start();
	    !nodes.end();
	    nodes.next()) {

		// There will only be one, but loop through anyway...
		for (operands.start();
		    !operands.end();
		    operands.next()) {

			// Create the cmd to run
			char args[BUFSIZ];
			(void) sprintf(args, "-x -d -u %s -s %s",
			    operands.getValue(), seclevel);

			clerrno_t status = run_sceventmib_cmd_on_node(
			    args, nodes.getValue());

			set_error(err, status);
			if (!status && cmd.isVerbose) {
				clmessage("Set default SNMP user "
				    "\"%s\" on node \"%s\".\n",
				    operands.getValue(),
				    nodes.getValue());
			}
		}
	}

	return (err);
}
