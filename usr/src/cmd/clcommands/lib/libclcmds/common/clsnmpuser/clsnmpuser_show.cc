//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clsnmpuser_show.cc	1.9	08/09/22 SMI"

//
// Process clsnmpuser "show"
//

#include "clsnmpuser.h"

#include "ClCommand.h"

//
// Prints the SNMP user configuration information using the ClShow
// class to format and print the data.
//
// Return values:
//
//	CL_NOERR
//	CL_EINTERNAL
//	CL_ENOENT
//
clerrno_t
clsnmpuser_show(
    ValueList &operands,
    ValueList &nodes,
    ValueList &auths,
    bool default_only)
{
	clerrno_t err = CL_NOERR;
	int outofcluster = 0;

	// Verify the nodelist
	if (nodes.getSize() == 0) {
		set_error(err, add_this_node(nodes));
	}
	set_error(err, verify_nodelist(nodes));

	// Verify if the nodes in the nodelist are active cluster members.
	set_error(err, verify_nodes_clustmode(nodes, outofcluster));

	if (outofcluster)
		clerror("SNMP user information cannot be retrieved from "
		    "offline nodes.\n");

	// Do it on each node
	for (nodes.start();
	    !nodes.end();
	    nodes.next()) {

		clerrno_t error = CL_NOERR;
		ClShow *show_obj = get_filtered_clsnmpuser_show_obj(
		    operands, auths, default_only, nodes.getValue(), error);

		show_obj->print();

		set_error(err, error);
	}

	return (err);
}

//
// Returns a ClShow object containing the SNMP user data from one node.
//
ClShow *
get_filtered_clsnmpuser_show_obj(
    ValueList &operands,
    ValueList &auths,
    bool default_only,
    char *node,
    clerrno_t &error)
{
	// Call the sceventmib shell script to show the user(s)
	char args[BUFSIZ];
	args[0] = '\0';
	(void) sprintf(args, "-x -X -v -p user-list ");

	// Default only?
	if (default_only) {
		(void) strcat(args, "-d ");
	}

	// Operand list
	if (operands.getSize() != 0)
		(void) strcat(args, "-z ");

	bool first = true;
	for (operands.start();
	    !operands.end();
	    operands.next()) {

		if (!first)
			(void) strcat(args, ",");

		(void) strcat(args, operands.getValue());
		first = false;
	}

	// Authentication protocols
	if (auths.getSize() != 0)
		(void) strcat(args, " -t ");

	first = true;
	for (auths.start();
	    !auths.end();
	    auths.next()) {

		if (!first)
			(void) strcat(args, ",");

		(void) strcat(args, auths.getValue());
		first = false;
	}

	// Creating a ClShow object with a header
	ClShow *show_obj = new ClShow(HEAD_SNMPUSER, node);

	// Printing out the header for the node first
	printf("%s\n", show_obj->getHeading());
	delete (show_obj);

	// Get a FILE * with the stdout from the cmd
	FILE *file = NULL;
	set_error(error, pipe_sceventmib_cmd_on_node(
	    &file, args, node));

	// Creating a ClShow object without a header
	show_obj = new ClShow;

	// Parse the data from stdout and add it to the
	// ClShow object
	set_error(error, parse_stdout_to_snmp_user_show(show_obj, file, node));

	// Close the pipe
	(void) pclose(file);

	return (show_obj);
}

//
// Returns a ClShow object containing the SNMP user data from one node.
// No filtering of the data is performed.
//
ClShow *
get_clsnmpuser_show_obj(char *node)
{
	ValueList empty;
	clerrno_t err;
	return (get_filtered_clsnmpuser_show_obj(
	    empty, empty, false, node, err));
}

//
// Parses the FILE * output into the ClShow object.  If any error was found
// either during processing or from the EXIT_MESSAGE, then it is returned.
//
clerrno_t
parse_stdout_to_snmp_user_show(ClShow *&show_obj, FILE *file, char *node)
{
	clerrno_t err = strip_list_headings(file);

	char default_user[128];
	char default_seclevel[128];
	err = get_default_user_and_seclevel(default_user,
	    default_seclevel, node);

	if (err) {
		clerror("Error while getting default user "
		    "and security level.\n");
		return (err);
	}

	int is_default = 0;
	char name[128];
	char auth[128];
	char temp[128];
	name[0] = '\0';
	auth[0] = '\0';
	temp[0] = '\0';
	while (fscanf(file, "%s", temp) != EOF) {
		// Now we are at the data that we want.  It is of the form:
		// <name> <auth>\n

		// Get the exit status from the piped cmd
		if (strcmp(temp, EXIT_MESSAGE) == 0) {
			fscanf(file, "%d", &err);
			return (conv_err_from_sceventmib(err));

		// Get the user name
		} else if (strcmp(name, "") == 0) {
			(void) sprintf(name, temp);

			if (strcmp(name, default_user) == 0) {
				is_default = 1;
			}
			show_obj->addObject(
			    (char *)gettext("SNMP User Name"),
			    name);

		// Get the auth
		} else if (strcmp(auth, "") == 0) {
			(void) sprintf(auth, temp);
			show_obj->addProperty(name,
			    (char *)gettext("Authentication Protocol"),
			    auth);

			// Default user?
			if (is_default) {
				show_obj->addProperty(name,
				    (char *)gettext("Default User"),
				    (char *)gettext("Yes"));
				show_obj->addProperty(name,
				    (char *)gettext("Default Security Level"),
				    default_seclevel);

				is_default = 0;
				name[0] = '\0';
				auth[0] = '\0';
			} else {
				show_obj->addProperty(name,
				    (char *)gettext("Default User"),
				    (char *)gettext("No"));

				name[0] = '\0';
				auth[0] = '\0';
			}
		}

		temp[0] = '\0';
	}

	// shouldn't get here
	return (CL_EINTERNAL);
}

clerrno_t
get_default_user_and_seclevel(char *user, char *seclevel, char *node)
{
	clerrno_t err = CL_NOERR;
	char hostname[MAXHOSTNAMELEN];

	gethostname(hostname, sizeof (hostname));

	if (user == NULL || seclevel == NULL || node == NULL)
		return (CL_EINTERNAL);

	// Build the command string
	char cmd[BUFSIZ];

	/* BEGIN CSTYLED */
	// The following command prints the SNMP users on the given node,
	// stripping off all of the extra formatting lines surrounding the
	// output, grepping for * (which is the default user) and then
	// printing the username and seclevel separated by a tab.
       
        if (strcmp(hostname, node) == 0) {
	    (void) sprintf(cmd, "/usr/cluster/lib/sc/scrcmd snmp -p users | "
		"sed -n -e \"/^-.*$/,/^-.*-$/p\" | sed -e '1d' -e '$d' | "
		"grep '*' | awk '{ print $2\"\t\"$3 }'");
        } else {
	    (void) sprintf(cmd, "/usr/cluster/lib/sc/scrcmd -N %s snmp -p users | "
		"sed -n -e \"/^-.*$/,/^-.*-$/p\" | sed -e '1d' -e '$d' | "
		"grep '*' | awk '{ print $2\"\t\"$3 }'", node);
        }
	/* END CSTYLED */

	// Get a FILE * with the stdout from the cmd
	FILE *file = popen(cmd, "r");
	if (file == NULL) {
		clerror("Cannot open pipe - %s\n", strerror(errno));
		switch (errno) {

		case EMFILE:
			return (CL_EIO);
		default:
			return (CL_EINTERNAL);
		}
	}

	// Parse the output
	char temp[128];
	user[0] = '\0';
	seclevel[0] = '\0';

	while (fscanf(file, "%s", temp) != EOF) {

		// Set the username
		if (strcmp(user, "") == 0) {
			(void) sprintf(user, temp);

		// Set the seclevel
		} else if (strcmp(seclevel, "") == 0) {
			// Seclevel looks like (<seclevel>),
			// so we need to strip off the ( and )
			int count = 0;
			for (int i = 1; i < strlen(temp) - 1; i++) {
				seclevel[count++] = temp[i];
			}
			// Null terminate the string...
			seclevel[count] = '\0';
		} else {
			err = CL_EINTERNAL;
		}
	}

	return (err);
}
