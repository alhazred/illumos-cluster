//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clsnmpuser_list.cc	1.5	08/05/20 SMI"

//
// Process clsnmpuser "list"
//

#include "clsnmpuser.h"

#include "ClCommand.h"

//
// Prints the SNMP user information based on the operands, nodes, and
// auths specified.  If default_only, it prints the default user on the node.
//
// Return values:
//
//	CL_NOERR
//	CL_EINTERNAL
//	CL_ENOENT
//	CL_EIO
//
clerrno_t
clsnmpuser_list(
    ClCommand &cmd,
    ValueList &operands,
    ValueList &nodes,
    ValueList &auths,
    bool default_only)
{
	clerrno_t err = CL_NOERR;
	int outofcluster = 0;

	// For storing the final list output
	ClList print_list;

	// For storing the list output from a node
	char this_node_out [BUFSIZ];
	this_node_out [0] = '\0';

	// To keep a track of the no. of nodes on which a user was not found.
	int notfoundcount = 0;

	bool first = true;

	// Verify the nodelist
	if (nodes.getSize() == 0) {
		set_error(err, add_this_node(nodes));
	}
	set_error(err, verify_nodelist(nodes));

	// Verify if the nodes in the nodelist are active cluster members.
	set_error(err, verify_nodes_clustmode(nodes, outofcluster));

	if (outofcluster)
		clerror("SNMP user information cannot be retrieved from "
		    "offline nodes.\n");

	// Call the sceventmib shell script to list the user(s)
	char args[BUFSIZ];
	args[0] = '\0';
	(void) strcat(args, " -x -p user-list ");

	// As the list subcommand prints each username only once even if
	// it exists on multiple nodes, the verbose mode for the subcommand
	// is being made to behave the same way as the non-verbose one. User
	// names are not being printed on a per node basis.

	// Default only?
	if (default_only) {
		(void) strcat(args, "-d ");
	}

	// Authentication protocols
	if (auths.getSize() != 0)
		(void) strcat(args, " -t ");

	first = true;
	for (auths.start();
	    !auths.end();
	    auths.next()) {

		if (!first)
			(void) strcat(args, ",");

		(void) strcat(args, auths.getValue());
		first = false;
	}

	if (operands.getSize() == 0) {
		// No operands or a '+' was specified to the command.
		// Get the list of the snmp users from all the nodes.
		// Populate print_list with the usernames.

		set_error(err, iterate_thru_nodelist(nodes, args, print_list));

	} else {
		// Operands were specified to the command.

		(void) strcat(args, "-z ");

		for (operands.start();
		!operands.end();
		operands.next()) {

			//
			// If a snmp user is not present on a cluster node
			// then increment notfoundcount by 1. When listing
			// users with a node list to the -n option, we do not
			// print the user name if the user doesn't exist on
			// all the nodes specified. Also we print the user name
			// only once even if it exists on more than one node.
			//
			notfoundcount = 0;

			set_error(err, iterate_thru_nodelist(nodes, args,
			    operands.getValue(), this_node_out,
			    &notfoundcount));

			if (notfoundcount == 0) {
				// The user is present on all the nodes in the
				// nodelist. Add it to print_list for printing.
				print_list.addRow(this_node_out);

			} else if ((notfoundcount == 1) &&
			    (nodes.getSize() == 1)) {
				// No user on the single node, implied either
				// through the value to the -n option or the
				// node on which the command runs.
				clerror("User \"%s\" not found.\n",
				    operands.getValue());
				set_error(err, CL_ENOENT);

			} else if (notfoundcount == nodes.getSize()) {
				// User not present on any of the nodes.
				clerror("User \"%s\" is not configured on any "
				    "of the specified nodes.\n",
				    operands.getValue());
				set_error(err, CL_ENOENT);

			} else if (notfoundcount < nodes.getSize()) {
				// User not present on one or more nodes.
				clerror("User \"%s\" is not configured on all "
				    "of the specified nodes.\n",
				    operands.getValue());
				set_error(err, CL_ENOENT);
			}
		}
	}

	// At this point the list of users, with or without operands specified
	// to the command is available. Print the list now.
	print_list.print();

	return (err);
}


//
// Iterates through cluster nodes running the scrcmd command without any
// operands and adds the user name to the list display if the user exists.
// It also checks for uniqueness of the user name in the final command
// output. So if a single user exists on multiple nodes of the cluster,
// then the user name is printed only once.
//
// Return values:
//
//	CL_NOERR
//	CL_EINTERNAL
//	CL_EIO
//
clerrno_t
iterate_thru_nodelist(ValueList &nodes, char *args, ClList &print_list_output)
{

	// File pointer for use with the pipe to the scrcmd command
	FILE *file = NULL;

	// For storing the user name retreived from the pipe to scrcmd
	char temp[BUFSIZ];
	temp[0] = '\0';

	// For storing the temporary list output
	char list_output[BUFSIZ];
	list_output[0] = '\0';

	// For traversing the formed command output to check if the
	// username from a node is already present in it.
	char *temp_output = NULL, *temppos = NULL;

	clerrno_t status = CL_NOERR;

	// For checking if the username has already been added to the
	// command output
	bool present;

	for (nodes.start(); !nodes.end(); nodes.next()) {

		set_error(status, run_scrcmd_for_list_on_node(args,
		    nodes.getValue(), &file));

		// At this point we have the snmp user list from the
		// current node in the file pointer.
		// No operand or a '+' was supplied to the command
		// Extracting the command output.

		while (fscanf(file, "%s", temp) != EOF) {
			present = false;

			if (*list_output == '\0') {
				// Add the user name to the list output
				strcpy(list_output, temp);
				strcat(list_output, "\n");
				print_list_output.addRow(temp);

			} else {

				// Checking to see if the user name has already
				// been added to the command output. We pick up
				// user names from the list output and compare
				// it with the user on the current node.

				temp_output = list_output;

				while (temppos = strchr(temp_output, '\n')) {
					*temppos = '\0';
					if (strcmp(temp_output, temp) == 0) {
						present = true;
						*temppos = '\n';
						break;
					}
					temp_output = temppos + 1;
					*temppos = '\n';
				}

				if (!present) {
					// The user name has to be
					// added to the list output
					print_list_output.addRow(temp);
					strcat(list_output, temp);
					strcat(list_output, "\n");
				}
			}
		}
	}

	list_output[strlen(list_output)-1] = '\0';
	return (status);

}


//
// Iterates through cluster nodes running the scrcmd command with an
// operand and adds the user name to the to be displayed list if the user
// exists. If the SNMP user is absent on any one of the nodes, then a count
// is incremented. The count is later used to issue proper error messages.
//
// Return values:
//
//	CL_NOERR
//	CL_EINTERNAL
//	CL_EIO
//
clerrno_t
iterate_thru_nodelist(ValueList &nodes, char *args, char *operand,
	char *node_list_output, int *notfndcnt)
{

	// For storing the scrcmd arguments including the operand supplied
	char temp_args [BUFSIZ];
	temp_args[0] = '\0';

	// File pointer for use with the pipe to the scrcmd command
	FILE *file = NULL;

	// For storing the user name retreived from the pipe to scrcmd
	char temp[BUFSIZ];
	temp[0] = '\0';

	clerrno_t status = CL_NOERR;

	for (nodes.start(); !nodes.end(); nodes.next()) {

		// Forming the arguments for scrcmd
		(void) strcpy(temp_args, args);
		(void) strcat(temp_args, operand);

		// Masking the error messages from sceventmib to
		// issue appropriate error messages from clsnmpuser
		(void) strcat(temp_args, " 2> /dev/null");

		set_error(status, run_scrcmd_for_list_on_node(temp_args,
		    nodes.getValue(), &file));

		// At this point we have the snmp user list from the current
		// node in the file pointer. Extracting the user name.

		if ((fscanf(file, "%s", temp) != EOF) &&
		    (strcmp(temp, operand) == 0)) {

			strcpy(node_list_output, temp);
		} else {
			// User was not found on this node. Increment the
			// notfndcnt variable.
			(*notfndcnt)++;
		}
	}

	return (status);

}
