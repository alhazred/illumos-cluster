//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clsnmpuser_delete.cc	1.6	08/10/13 SMI"

//
// Process clsnmpuser "delete"
//

#include "clsnmpuser.h"

#include "ClCommand.h"

clerrno_t get_all_snmp_users_in_auth(ValueList &users, char *auth, char *node);
clerrno_t get_snmp_users_in_auth(ValueList &users, char *auth, char *node);

//
// Deletes one or more SNMP users from one or more nodes, depending on the value
// of operands, nodes, and seclevels.
//
// Return values:
//
//	CL_NOERR
//	CL_ENOENT
//	CL_EINTERNAL
//
clerrno_t
clsnmpuser_delete(
    ClCommand &cmd,
    ValueList &operands,
    ValueList &auths,
    ValueList &nodes)
{
	clerrno_t err = CL_NOERR;
	int outofcluster = 0;

	// Verify the nodelist
	if (nodes.getSize() == 0) {
		set_error(err, add_this_node(nodes));
	}
	set_error(err, verify_nodelist(nodes));

	// Verify if the nodes in the nodelist are active cluster members.
	set_error(err, verify_nodes_clustmode(nodes, outofcluster));

	if (outofcluster)
		clerror("SNMP users cannot be deleted from offline nodes.\n");

	// Call the sceventmib shell script to remove the user(s)
	char *argv[6];
	argv[0] = "sceventmib";	// The command
	argv[1] = "-x";		// CLCMD flag
	argv[2] = "-r";		// The remove flag
	argv[3] = "-u";		// The user flag
	argv[4] = NULL;		// The user
	argv[5] = NULL;

	for (nodes.start();
	    !nodes.end();
	    nodes.next()) {

		int count = 0;

		// Get the operands.

		// There are only two auth types, so if none or both are
		// specified, then they are same.
		bool all_oper = false;
		if (operands.getSize() == 0 &&
		    (auths.getSize() == 0 || auths.getSize() == 2)) {
			set_error(err, get_all_snmp_users(operands,
			    nodes.getValue()));
			all_oper = true;

		// The user specified one auth type, and all users.
		} else if (operands.getSize() == 0) {
			auths.start();
			set_error(err, get_all_snmp_users_in_auth(operands,
			    auths.getValue(), nodes.getValue()));

		// The user specified one auth type and set of specific
		// users, we need to determine which users are valid.
		} else if (auths.getSize() == 1) {
			auths.start();
			set_error(err, get_snmp_users_in_auth(operands,
			    auths.getValue(), nodes.getValue()));
		}

		for (operands.start();
		    !operands.end();
		    operands.next()) {

			// set the hostname
			argv[4] = operands.getValue();

			// run the command
			clerrno_t status = run_sceventmib_cmd_on_node(
			    argv, strdup(nodes.getValue()));

			// Set the error
			if (status == CL_ENOENT) {
				clerror("SNMP User \"%s\" does not exist on "
				    "node \"%s\".\n",
				    operands.getValue(),
				    nodes.getValue());
			}

			set_error(err, status);

			if (!status && cmd.isVerbose) {
				clmessage("Removed SNMP user "
				    "\"%s\" on node \"%s\".\n",
				    operands.getValue(),
				    nodes.getValue());
			}
		}

		if (all_oper)
			operands.clear();
	}

	return (err);
}


clerrno_t
get_all_snmp_users_in_auth(ValueList &users, char *auth, char *node)
{
	clerrno_t err = CL_NOERR;

	if (!node || !auth)
		return (CL_EINTERNAL);

	// Delete the list, if there are any in it.
	users.clear();

	// Build the command string
	char args[256];
	(void) sprintf(args, "-x -p user-list -t %s", auth);

	// run the command and open a pipe
	FILE *file = NULL;
	set_error(err, pipe_sceventmib_cmd_on_node(&file, args, node));

	// Get the users and populate the ValueList
	char c;
	int count = 0;
	char temp[128];
	temp[0] = '\0';
	while ((c = (char)fgetc(file)) != EOF) {
		if (c == '\n') {
			temp[count] = '\0';
			users.add(temp);
			count = 0;
			temp[count] = '\0';
		} else {
			temp[count++] = c;
		}
	}

	// close the pipe
	(void) pclose(file);

	return (err);
}

clerrno_t
get_snmp_users_in_auth(ValueList &users, char *auth, char *node)
{
	clerrno_t err = CL_NOERR;
	ValueList all_users;
	ValueList valid_users = ValueList(true);

	err = get_all_snmp_users_in_auth(all_users, auth, node);
	if (err == CL_EINTERNAL)
		return (err);

	// Iterate through the list of users.  For each user, if
	// it is not in the all_users list, then it is invalid,
	// so print an error, otherwise add it to the valid_users
	// list.
	for (users.start(); !users.end(); users.next()) {
		if (!all_users.isValue(users.getValue())) {
			clerror("User \"%s\" does not match the "
			    "authentication protocol specified.\n",
			    users.getValue());
			if (!err)
				err = CL_EINVAL;
		} else {
			valid_users.add(users.getValue());
		}
	}

	// Replace the users list with the valid users
	users.clear();
	for (valid_users.start(); !valid_users.end(); valid_users.next()) {
		users.add(valid_users.getValue());
	}

	return (err);
}
