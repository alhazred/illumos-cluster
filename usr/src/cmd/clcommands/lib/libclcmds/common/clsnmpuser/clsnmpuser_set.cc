//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clsnmpuser_set.cc	1.5	08/10/13 SMI"

//
// Process clsnmpuser "set"
//

#include "clsnmpuser.h"

#include "ClCommand.h"

//
// Modifies one or more SNMP users on one or more nodes.
//
// Return values:
//
//	CL_NOERR
//	CL_EINTERNAL
//	CL_ENOENT
//
clerrno_t
clsnmpuser_set(
    ClCommand &cmd,
    ValueList &operands,
    ValueList &nodes,
    char *auth)
{
	clerrno_t err = CL_NOERR;
	int outofcluster = 0;

	// Verify the nodelist
	if (nodes.getSize() == 0) {
		set_error(err, add_this_node(nodes));
	}
	set_error(err, verify_nodelist(nodes));

	// Verify if the nodes in the nodelist are active cluster members.
	set_error(err, verify_nodes_clustmode(nodes, outofcluster));

	if (outofcluster)
		clerror("SNMP user properties cannot be changed on offline "
		    "nodes.\n");

	// Call the sceventmib shell script to modify each user
	// on the nodes listed
	for (nodes.start();
	    !nodes.end();
	    nodes.next()) {

		// Get the users, if none specified
		bool all_oper = false;
		if (operands.getSize() == 0) {
			set_error(err, get_all_snmp_users(
			    operands, nodes.getValue()));
			all_oper = true;
		}

		for (operands.start();
		    !operands.end();
		    operands.next()) {

			// Create the cmd to run
			char args[BUFSIZ];
			(void) sprintf(args, "-x -m -u %s -t %s",
			    operands.getValue(), auth);

			clerrno_t status = run_sceventmib_cmd_on_node(
			    args, nodes.getValue());

			set_error(err, status);
			if (!status && cmd.isVerbose) {
				clmessage("Modified SNMP user "
				    "\"%s\" on node \"%s\".\n",
				    operands.getValue(),
				    nodes.getValue());
			}
		}

		if (all_oper)
			operands.clear();
	}

	return (err);
}

//
// Populates the users ValueList with all the SNMP users on the node
// specified.  The contents of the users ValueList is deleted first.
//
// Return values:
//
//	CL_NOERR
//	CL_EINTERNAL
//
clerrno_t
get_all_snmp_users(ValueList &users, char *node)
{
	if (!node)
		return (CL_EINTERNAL);

	clerrno_t err = CL_NOERR;

	// Delete the list, if there are any in it.
	users.clear();

	// Build the command string
	char args[256];
	(void) sprintf(args, "-x -p user-list");

	// run the command and open a pipe
	FILE *file = NULL;
	set_error(err, pipe_sceventmib_cmd_on_node(&file, args, node));

	// Get the users and populate the ValueList
	char c;
	int count = 0;
	char temp[128];
	temp[0] = '\0';
	while ((c = (char)fgetc(file)) != EOF) {
		if (c == '\n') {
			temp[count] = '\0';
			users.add(temp);
			count = 0;
			temp[count] = '\0';
		} else {
			temp[count++] = c;
		}
	}

	// close the pipe
	(void) pclose(file);

	return (err);
}
