//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clsnmpuser_export.cc	1.4	08/05/20 SMI"

//
// Process clsnmpuser "export"
//

#include "clsnmpuser.h"

#include "ClCommand.h"
#include "ClXml.h"

clerrno_t get_export_obj(ClsnmpuserExport &export_obj, ValueList &operands,
    ValueList &nodes, ValueList &auths,
    int &user_found);

clerrno_t parse_stdout_to_snmp_user_xml(ClsnmpuserExport &export_obj,
    FILE *file, char *node, int &user_found);

auth_t get_auth(char *auth);

//
// This function returns a ClsnmpuserExport object populated with
// all of the snmpuser information from the cluster.  Caller is
// responsible for releasing the ClcmdExport object.
//
ClcmdExport *
get_clsnmpuser_xml_elements()
{
	// Get the cluster nodes
	ValueList nodes;
	(void) get_all_cluster_nodes(nodes);
	ValueList operands;
	ValueList auths;
	int uf = 0;

	//lint -e(1550) -e(578)
	ClsnmpuserExport *user = new ClsnmpuserExport();
	(void) get_export_obj(*user, operands, nodes, auths, uf);

	return (user);
}

//
// Populates export_obj with the SNMP user configuration information
// based on the operands, nodes, and auths specified.
//
// Return values:
//
//	CL_NOERR
//	CL_EINTNERAL
//	CL_ENOENT
//
clerrno_t
get_export_obj(ClsnmpuserExport &export_obj,
    ValueList &operands, ValueList &nodes,
    ValueList &auths, int &user_found)
{
	clerrno_t err = CL_NOERR;
	nodeid_t nodeid;
	uint_t ismember;

	// Create the command to get the users
	char args[BUFSIZ];
	args[0] = '\0';
	(void) strcat(args, "-x -X -p user-list -v ");

	// Operand list
	if (operands.getSize() != 0)
		(void) strcat(args, "-z ");

	// Add the operands
	bool first = true;
	for (operands.start();
	    !operands.end();
	    operands.next()) {

		if (!first)
			(void) strcat(args, ",");

		(void) strcat(args, operands.getValue());
		first = false;
	}

	// Authentication protocols
	if (auths.getSize() != 0)
		(void) strcat(args, " -t ");

	// Add the auth protocols
	first = true;
	for (auths.start();
	    !auths.end();
	    auths.next()) {

		if (!first)
			(void) strcat(args, ",");

		(void) strcat(args, auths.getValue());
		first = false;
	}

	// Do it on each node
	for (nodes.start();
	    !nodes.end();
	    nodes.next()) {

		//
		// Check to see if the node is an active cluster member.
		// For cases like cluster export, we may reach here with
		// the nodelist having nodes which are out of the cluster
		// at that point of time...
		//
		set_error(err, scconf_get_nodeid(nodes.getValue(), &nodeid));

		(void) scconf_ismember(nodeid, &ismember);

		if (!ismember) {
			clerror("SNMP user information cannot be retrieved "
			    "from offline node \"%s\".\n", nodes.getValue());
			continue;
		}

		// Get a FILE * with the stdout from the cmd
		FILE *file = NULL;
		set_error(err, pipe_sceventmib_cmd_on_node(
		    &file, args, nodes.getValue()));

		// Parse the data from stdout and add it to the
		// ClsnmphostExport object.
		set_error(err, parse_stdout_to_snmp_user_xml(
		    export_obj, file, nodes.getValue(), user_found));

		// Close the pipe
		(void) pclose(file);
	}
	return (err);
}

//
// Parses the data from a stdout pipe, file, into the SNMP user XML
// configuration information.
//
// Return values:
//
//	CL_NOERR
//	CL_EINTERNAL
//
clerrno_t
parse_stdout_to_snmp_user_xml(
    ClsnmpuserExport &export_obj,
    FILE *file,
    char *node,
    int &user_found)
{
	if (!node)
		return (CL_EINTERNAL);

	(void) strip_list_headings(file);

	char default_user[128];
	char default_seclevel[128];
	clerrno_t err = get_default_user_and_seclevel(default_user,
	    default_seclevel, node);

	if (err) {
		clerror("Error while getting default user "
		    "and security level.\n");
		return (err);
	}

	int is_default = 0;
	char name[128];
	name[0] = '\0';
	char auth[16];
	auth[0] = '\0';

	char temp[128];
	temp[0] = '\0';
	while (fscanf(file, "%s", temp) != EOF) {
		// Now we are at the data that we want. It is of the form:
		// <name> <auth>\n
		if (strcmp(temp, EXIT_MESSAGE) == 0) {
			fscanf(file, "%d", &err);
			return (conv_err_from_sceventmib(err));
		} else if (strcmp(name, "") == 0) {
			(void) sprintf(name, temp);
			if (strcmp(name, default_user) == 0) {
				is_default = 1;
			}
			user_found++;
		} else if (strcmp(auth, "") == 0) {
			(void) sprintf(auth, temp);

			if (is_default) {
				(void) export_obj.createSNMPUser(
				    name,
				    node,
				    get_auth(auth),
				    true,
				    default_seclevel);

				is_default = 0;
			} else {
				(void) export_obj.createSNMPUser(
				    name,
				    node,
				    get_auth(auth),
				    false);
			}

			name[0] = '\0';
			auth[0] = '\0';
		}
		temp[0] = '\0';
	}

	return (CL_EINTERNAL);
}

//
// Exports the SNMP user information in XML format to either stdout or a file,
// depending on the value of clconfiguration.
//
// Return values:
//
//	CL_NOERR
//	CL_EINTERNAL
//	CL_ENOENT
//
clerrno_t
clsnmpuser_export(ValueList &operands, ValueList &nodes,
    ValueList &auths, char *clconfiguration)
{
	clerrno_t err = CL_NOERR;
	int outofcluster = 0;

	// Set the doc to be stdout
	if (!clconfiguration)
		clconfiguration = strdup("-");

	// Verify the nodelist
	if (nodes.getSize() == 0) {
		set_error(err, add_this_node(nodes));
	}
	set_error(err, verify_nodelist(nodes));

	// Verify if the nodes in the nodelist are active cluster members.
	set_error(err, verify_nodes_clustmode(nodes, outofcluster));

	if (outofcluster)
		clerror("SNMP user information cannot be retrieved from "
		    "offline nodes.\n");

	// Get the ClsnmpuserExport object
	ClXml xml;
	int user_found = 0;
	ClsnmpuserExport export_obj;
	set_error(err, get_export_obj(export_obj, operands,
	    nodes, auths, user_found));

	if (!user_found)
		return (err);

	// Create the xml file and print it to stdout or a file
	set_error(err, xml.createExportFile(
	    CLSNMPUSER_CMD,
	    clconfiguration,
	    &export_obj));

	return (err);
}

//
// Returns a auth_t value based on a string, or NULL
//
auth_t
get_auth(char *auth)
{
	if (strcasecmp(auth, "MD5") == 0) {
		return (MD5);
	} else if (strcasecmp(auth, "SHA") == 0) {
		return (SHA);
	} else
		return ((auth_t)NULL);
}
