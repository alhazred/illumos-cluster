//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clrg_restart.cc	1.13	08/07/25 SMI"

//
// Process clrg "restart"
//
#include <sys/os.h>
#include <scadmin/scconf.h>
#include <scadmin/scswitch.h>
#include <scadmin/scstat.h>
#include <rgm/rgm_scrgadm.h>

#include "clcommands.h"

#include "ClCommand.h"

#include "rgm_support.h"
#include "common.h"

//
// clrg "restart"
//
clerrno_t
clrg_restart(ClCommand &cmd, ValueList &operands, optflgs_t optflgs,
    ValueList &nodes)
{
	clerrno_t first_err = CL_NOERR;
	int rv = 0;
	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};
	char **rg_names = NULL;
	char **rg_nodes = NULL;
	char *oper1;
	clerrno_t errflg = CL_NOERR;
	clerrno_t clerrno = CL_NOERR;
	int num_rgs = 0;
	uint_t i, j, k;
	ValueList good_nodes, good_rgs, tmp_list;
	boolean_t verbose;
	char *zc_name, *rg_part, *zc_part;
	scconf_errno_t scconf_err = SCCONF_NOERR;
	uint_t zc_id;
	boolean_t is_same = B_FALSE;
	char *orig_rg_name = NULL;
	char *orig_rg_zc = NULL;

	if (optflgs & vflg)
		verbose = B_TRUE;
	else
		verbose = B_FALSE;

	// Initializa ORB and other cluster checks
	clerrno = cluster_init();
	if (clerrno)
		return (clerrno);

	// Check whether any zone cluster names were specified.
	// And if they were specified, there should be only zone cluster
	// across all the RGs.
	zc_name = NULL;
	rg_part = zc_part = NULL;
	clerrno = check_single_zc_specified(operands, &zc_name, &is_same);
	if (clerrno != CL_NOERR) {
		clcommand_perror(clerrno);
		return (clerrno);
	}
	// If multiple zone clusters were specified, report an error.
	if (is_same == B_FALSE) {
		clerror("This operation can be performed on only one "
		    "zone cluster at a time.\n");
		return (clerrno);
	}
#if (SOL_VERSION >= __s10)
	if (zc_name) {
		// If zone cluster name was specified, we have to ensure
		// that it is valid.
		// We have to check whether this operation is allowed for
		// the status of the zone cluster.
		clerrno = check_zc_operation(CL_OBJ_TYPE_RG, CL_OP_TYPE_RESTART,
						zc_name);
		if (clerrno != CL_NOERR) {
			return (clerrno);
		}
	}
#endif

	// Check the validity of nodes
	tmp_list.add(zc_name);
	clerrno = get_good_nodes(nodes, good_nodes, tmp_list);
	tmp_list.clear();

	if (clerrno)
		return (clerrno);

	rg_nodes = vl_to_names(good_nodes, &errflg);
	if (errflg)
		return (errflg);

	operands.start();
	oper1 = operands.getValue();

	scconf_err = scconf_parse_obj_name_from_cluster_scope(
			oper1, &orig_rg_name, &orig_rg_zc);

	if (scconf_err) {
		clerror("Internal Error.\n");
		return (CL_EINTERNAL);
	}

	if (strcmp(orig_rg_name, CLCOMMANDS_WILD_OPERAND)) {
		for (operands.start(); !operands.end(); operands.next()) {

			if (rg_part) {
				free(rg_part);
				rg_part = NULL;
			}
			if (zc_part) {
				free(zc_part);
				zc_part = NULL;
			}
			// First split the RG name from cluster scope
			scconf_err = scconf_parse_obj_name_from_cluster_scope(
					operands.getValue(), &rg_part,
					&zc_part);
			if (scconf_err != SCCONF_NOERR) {
				clerrno = map_scconf_error(scconf_err);
				clcommand_perror(clerrno);
				if (first_err == CL_NOERR) {
					clerrno = first_err;
				}

				continue;
			}
			// Check whether this RG exists
			clerrno = check_rg(rg_part, zc_part);

			if (clerrno == CL_NOERR) {
				good_rgs.add(rg_part);
				continue;
			}

			// If we are here, it means this RG does not exist.
			clerror("Invalid resource group \"%s\" specified.\n",
				operands.getValue());
			if (first_err == CL_NOERR) {
					// first error!
					first_err = CL_EINVAL;
			}
		}
		// Free memory
		if (rg_part) {
			free(rg_part);
			rg_part = NULL;
		}
		if (zc_part) {
			free(zc_part);
			zc_part = NULL;
		}

		rg_names = vl_to_names(good_rgs, &errflg);
		if (errflg)
			return (errflg);

	} else {
		clerrno = z_getrglist(&rg_names, verbose, optflgs & uflg,
					zc_name);
		if (clerrno != CL_NOERR) {
			return (clerrno);
		}
		// Remove the zone cluster context from all these
		// names.
		clerrno = remove_zc_names_from_names(rg_names);
		if (clerrno != CL_NOERR) {
			clcommand_perror(clerrno);
			return (clerrno);
		}
	}

	if (!rg_names)
		return (CL_NOERR);

	num_rgs = 0;
	for (i = 0; rg_names[i] != NULL; i++) {
		num_rgs++;
	}

	//
	// Restart all RGs in the list on the specified nodes, in the more
	// common case where the RGs need to be restarted on their currently
	// online nodes, nodelist passed is NULL.
	//
	if (rg_nodes != NULL) {
		scha_status = rgm_scswitch_restart_rg((const char **) rg_nodes,
		    (const char **) rg_names, verbose, zc_name);
	} else {
		scha_status = rgm_scswitch_restart_rg(NULL,
		    (const char **) rg_names, verbose, zc_name);
	}
	rv = scha_status.err_code;
	if (rv != SCHA_ERR_NOERR) {
		print_rgm_error(scha_status, NULL);
		if (first_err == 0) {
			// first error!
			first_err = map_scha_error(rv);
		}
	} else if (scha_status.err_msg) {
		fprintf(stdout, "%s\n", scha_status.err_msg);
	}

	// Free memory
	if (rg_names)
		rgm_free_strarray(rg_names);

	if (zc_name) {
		free(zc_name);
	}
	good_rgs.clear();

	if (orig_rg_name)
		free(orig_rg_name);

	if (orig_rg_zc)
		free(orig_rg_zc);

	return (first_err);
}
