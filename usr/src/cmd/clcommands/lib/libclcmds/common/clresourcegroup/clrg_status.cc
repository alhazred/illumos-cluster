//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clrg_status.cc	1.13	08/07/25 SMI"

//
// Process clrg "status"
//

#include <sys/os.h>
#include <scadmin/scconf.h>
#include <scadmin/scswitch.h>
#include <scadmin/scstat.h>
#include <rgm/rgm_scrgadm.h>
#include <rgm/rgm_cnfg.h>

#include "clcommands.h"

#include "ClCommand.h"
#include "ClStatus.h"

#include "rgm_support.h"
#include "common.h"

static clerrno_t
get_clrg_status_obj_select(ValueList &operands, optflgs_t optflgs,
    ValueList &resources, ValueList &resourcetypes, ValueList &states,
    ValueList &nodes, ClStatus *rg_status, ValueList &zc_names = ValueList());

//
// clrg "status"
//
clerrno_t
clrg_status(ClCommand &cmd, ValueList &operands, optflgs_t optflgs,
    ValueList &resources, ValueList &resourcetypes, ValueList &states,
    ValueList &nodes, ValueList &zc_names = ValueList())
{
	clerrno_t clerrno = CL_NOERR;
	ClStatus *rg_status = new ClStatus(cmd.isVerbose ? true : false);

	clerrno = get_clrg_status_obj_select(operands, optflgs, resources,
	    resourcetypes, states, nodes, rg_status, zc_names);

	rg_status->enableRowGrouping(true);
	rg_status->setHeadingTitle(HEAD_RESOURCEGROUP);
	rg_status->setHeadings(
	    (char *)gettext("Group Name"),
	    (char *)gettext("Node Name"),
	    (char *)gettext("Suspended"),
	    (char *)gettext("Status"));
	rg_status->print();

	return (clerrno);
}

//
// clrg "status" for cluster
//
clerrno_t
get_clrg_status_obj(optflgs_t optflgs, ClStatus *rg_status)
{
	clerrno_t clerrno = CL_NOERR;
	ValueList rg_list = ValueList(true);
	ValueList empty_list;

	rg_list.add(CLCOMMANDS_WILD_OPERAND);

	rg_status->enableRowGrouping(true);
	rg_status->setHeadingTitle(HEAD_RESOURCEGROUP);
	rg_status->setHeadings(
	    (char *)gettext("Group Name"),
	    (char *)gettext("Node Name"),
	    (char *)gettext("Suspended"),
	    (char *)gettext("State"));
	clerrno = get_clrg_status_obj_select(rg_list, optflgs, empty_list,
	    empty_list, empty_list, empty_list, rg_status);

	return (clerrno);
}

static clerrno_t
get_clrg_status_obj_select(ValueList &operands, optflgs_t optflgs,
    ValueList &resources, ValueList &resourcetypes, ValueList &states,
    ValueList &nodes, ClStatus *rg_status, ValueList &zc_names)
{
	clerrno_t first_err = CL_NOERR;
	int rv = 0;
	int i, j;
	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};
	char **rg_names = NULL;
	char **rs_names = (char **)0;
	char *oper1, *rg_name, *zc_name, *fullname;
	clerrno_t errflg = CL_NOERR;
	clerrno_t clerrno = CL_NOERR;
	int wildcard = 0;
	char *rgstatus;
	rgm_rg_t *rgconf = (rgm_rg_t *)0;
	int rs_found, rt_found, node_found, state_found;
	int notonline, offline;
	ValueList sys_rts;
	ValueList good_states;
	rgm_rt_t *rt = (rgm_rt_t *)0;
	rgm_resource_t *rs = (rgm_resource_t *)0;
	char *r_rtname = NULL;
	ValueList col1, col2, col3, col4;
	scstat_errno_t scstat_status;
	scstat_rg_t *rgstat = (scstat_rg_t *)NULL;
	scstat_rg_status_t *rgnode;
	int first_node;
	ValueList good_nodes, tmp_list, valid_zc_list, good_rgs;
	scconf_errno_t scconf_err = SCCONF_NOERR;

	// Initializa ORB and other cluster checks
	clerrno = cluster_init();
	if (clerrno) {
		return (clerrno);
	}

	rg_name = zc_name = NULL;
	// Zone cluster names can be specified either with the -Z
	// option or in the form of <zonecluster>:<RG name> format.
	// But, both the ways cannot be used simultaneously.
	// If zone clusters were not specified with the -Z option, we
	// have to check whether they were specified in the <zc>:<rg>
	// format.
	for (operands.start(); !operands.end(); operands.next()) {
		scconf_err = scconf_parse_obj_name_from_cluster_scope(
				operands.getValue(), &rg_name, &zc_name);

		if (scconf_err != SCCONF_NOERR) {
			return (CL_ENOMEM);
		}

		if (zc_name && zc_names.getSize()) {
			// -Z flag was specified and this RG was scoped
			// with a zone cluster name as well. Report an
			// error.
			clerror("You cannot use the "
				"\"<zonecluster>:<rgname>\" "
				"form of the resource-group name with the "
				"\"-Z\" option.");
			free(rg_name);
			free(zc_name);
			return (CL_EINVAL);
		}

		// If this zone cluster name does not exist in tmp_list,
		// then we wil add it here.
		if (!tmp_list.isValue(zc_name)) {
			tmp_list.add(zc_name);
		}

		if (rg_name) {
			free(rg_name);
			rg_name = NULL;
		}
		if (zc_name) {
			free(zc_name);
			zc_name = NULL;
		}
	}
#if (SOL_VERSION >= __s10)
	// By the time we are here, we have added any zone cluster name in the
	// ResourceGroup names to tmp_list.
	if (zc_names.getSize()) {
		// Zc names were specified. We have to check whether "all"
		// was specified.
		zc_names.start();

		if (strcmp(zc_names.getValue(), ALL_CLUSTERS_STR) == 0) {
			// Get all known zone clusters.
			clerrno = get_all_clusters(valid_zc_list);

			if (clerrno != CL_NOERR) {
				return (clerrno);
			}
		} else {
			tmp_list = zc_names;
		}
	}

	// valid_zc_list will contain at least one zone cluster name
	// if "all" was specified. If it is empty, we have to check
	// the validity of the zone clusters specified by the user.
	if (valid_zc_list.getSize() < 1) {
		clerrno = get_valid_zone_clusters(tmp_list, valid_zc_list);
	}
	// Clear the memory.
	tmp_list.clear();

	if (clerrno) {
		// There was at least one invalid zone cluster.
		// We have to return.
		return (clerrno);
	}
#endif

	if (operands.getSize() > 0) {
		operands.start();
		oper1 = operands.getValue();
		if (!(strcmp(oper1, CLCOMMANDS_WILD_OPERAND))) {
			wildcard = 1;
		}
	} else {
		wildcard = 1;
	}

	if (wildcard) {
		if (zc_names.getSize() == 0) {
			scha_status = scswitch_getrglist(&rg_names, B_FALSE,
							NULL);
			rv = scha_status.err_code;
			if (rv != SCHA_ERR_NOERR) {
				clerror("Failed to obtain list of all resource"
					" groups.\n");
				print_rgm_error(scha_status, NULL);
				return (map_scha_error(rv));
			}
		} else {
			// If  we are here, then we have to fetch RGs from all
			// the zone clusters.
			for (valid_zc_list.start(); !valid_zc_list.end();
				valid_zc_list.next()) {
				scha_status = scswitch_getrglist(&rg_names,
						B_FALSE,
						valid_zc_list.getValue());
				rv = scha_status.err_code;
				clerrno = names_to_valuelist(rg_names,
						tmp_list);
				rgm_free_strarray(rg_names);

				if ((rv != SCHA_ERR_NOERR) ||
				    (clerrno != CL_NOERR)) {
					clerror("Failed to obtain list of "
					"all resource groups.\n");
					// Free memory.
					tmp_list.clear();
					print_rgm_error(scha_status, NULL);
					return (map_scha_error(rv));
				}
			}

			// Get all the RG names
			rg_names = vl_to_names(tmp_list, &clerrno);
			tmp_list.clear();
			if (clerrno != CL_NOERR) {
				return (clerrno);
			}
		}
	} else {
		// RG names were specified. We have to verify the validity
		// of RG names. RG names could have been specified using
		// the <ZC name>:<RG name> format, or the ZC name could
		// have been specified using the -Z flag. We have to handle
		// both the cases here.
		for (operands.start(); operands.end() != 1; operands.next()) {

			// First parse the RG name from zone cluster scope
			if (rgconf) {
				rgm_free_rg(rgconf);
				rgconf == (rgm_rg_t *)NULL;
			}
			if (rg_name) {
				free(rg_name);
				rg_name = NULL;
			}
			if (zc_name) {
				free(zc_name);
				zc_name = NULL;
			}

			scconf_err = scconf_parse_obj_name_from_cluster_scope(
				operands.getValue(), &rg_name, &zc_name);

			if (scconf_err != SCCONF_NOERR) {
				if (first_err == CL_NOERR) {
					first_err = map_scconf_error(
							scconf_err);
				}
				continue;
			}

			// If zone cluster names were not specified
			// using the -Z flag, they could have been specified
			// as part of the RG name also.
			if (zc_names.getSize() == 0) {
				// -Z flag was not used.
				scha_status = rgm_scrgadm_getrgconf(
						rg_name, &rgconf, zc_name);
				rv = scha_status.err_code;
				if (rv != SCHA_ERR_NOERR) {
					print_rgm_error(scha_status,
					    operands.getValue());
					clerror("Failed to access information "
					    "for resource group \"%s\".\n",
					    operands.getValue());
					if (first_err == CL_NOERR) {
						// first error!
						first_err = map_scha_error(rv);
					}
				} else {
					good_rgs.add(operands.getValue());
				}

				continue;
			}
#if (SOL_VERSION >= __s10)
			// If we are here, it means zone clusters were
			// specified using the -Z flag.
			tmp_list.clear();
			clerrno = get_zone_clusters_with_rg(rg_name, tmp_list,
								valid_zc_list);
			if (clerrno != CL_NOERR) {

				if (clerrno == CL_ENOENT) {
					clerror("Failed to access information "
					    "for resource group \"%s\".\n",
					    operands.getValue());
				} else {
					clcommand_perror(clerrno);
				}

				if (first_err == CL_NOERR) {
					first_err = clerrno;
				}
				continue;
			}
#endif

			// We have to scope this RG name with the
			// zone cluster name
			for (tmp_list.start(); !tmp_list.end();
				tmp_list.next()) {

				if (fullname) {
					free(fullname);
					fullname = NULL;
				}

				scconf_err =
					scconf_add_cluster_scope_to_obj_name(
						rg_name, tmp_list.getValue(),
						&fullname);
				if (scconf_err != SCCONF_NOERR) {
					clerrno = map_scconf_error(scconf_err);
					clcommand_perror(clerrno);

					if (first_err == CL_NOERR) {
						first_err = clerrno;
					}

					continue;
				}

				good_rgs.add(fullname);
			}

			if (fullname) {
				free(fullname);
				fullname = NULL;
			}
		}

		// Get all the RG names
		rg_names = vl_to_names(good_rgs, &clerrno);
		good_rgs.clear();
		tmp_list.clear();
		if (clerrno != CL_NOERR) {
			return (clerrno);
		}
	}

	// By the time we are here, we have all the RG names with their
	// zone cluster names scoped as well.

	clerrno = get_good_nodes(nodes, good_nodes, valid_zc_list);
	if (first_err == 0) {
		// first error!
		first_err = clerrno;
	}

	// If resources were specified, we have to check whether they
	// are valid resources or not.
	for (resources.start(); !resources.end(); resources.next()) {

#if SOL_VERSION >= __s10
		// If zone clusters were specified, we have to check whether
		// this resource is valid in at least one of the zone
		// clusters.
		if (valid_zc_list.getSize() > 0) {
			clerrno = get_zone_clusters_with_rs(
					resources.getValue(), tmp_list,
					valid_zc_list);
			tmp_list.clear();
			if (clerrno != CL_NOERR) {
				// This resource was not present in any
				// of the zone clusters.
				clerror("%s: Invalid resource name\n",
					resources.getValue());
				if (first_err == CL_NOERR) {
					first_err = clerrno;
				}
			}

			continue;
		}
#endif

		// If we are here, it means no zone cluster names were
		// specified. We have to check the validity of the resource
		// in the current cluster itself.
		if (rs) {
			rgm_free_resource(rs);
			rs = (rgm_resource_t *)NULL;
		}

		scha_status = rgm_scrgadm_getrsrcconf(resources.getValue(),
		    &rs, NULL);
		rv = scha_status.err_code;
		if (rv != SCHA_ERR_NOERR) {
			clerrno = map_scha_error(rv);
			clerror("%s: Invalid resource name\n",
				resources.getValue());
			if (first_err == CL_NOERR) {
				// first error!
				first_err = clerrno;
			}
		}
	}

	if (clerrno != CL_NOERR) {
		return (clerrno);
	}

	// We have to check whether the states are valid
	for (states.start(); !states.end(); states.next()) {
		clerrno = check_state(states.getValue(), CL_TYPE_RG);
		if (clerrno) {
			if (first_err == 0) {
				// first error!
				first_err = clerrno;
			}
		} else {
			good_states.add(states.getValue());
		}
	}

	// If resource types were specified, we have to check their validity.
	for (resourcetypes.start(); !resourcetypes.end();
	    resourcetypes.next()) {
#if SOL_VERSION >= __s10
		// If zone clusters were specified, we have to check whether
		// this RT is valid in at least one of the zone
		// clusters.
		if (valid_zc_list.getSize() > 0) {
			clerrno = get_zone_clusters_with_rt(
					resourcetypes.getValue(), tmp_list,
					valid_zc_list);
			tmp_list.clear();
			if (clerrno != CL_NOERR) {
				// This RT was not present in any
				// of the zone clusters.
				clerror("%s: Invalid resource type name\n",
					resourcetypes.getValue());
				if (first_err == CL_NOERR) {
					first_err = clerrno;
				}
			} else {
				sys_rts.add(r_rtname);
			}

			continue;
		}
#endif

		// If we are here, it means no zone cluster names were
		// specified. We have to check the validity of the RT
		// in the current cluster itself.
		if (r_rtname) {
			free(r_rtname);
			r_rtname = (char *)NULL;
		}
		scha_status = rgmcnfg_get_rtrealname(resourcetypes.getValue(),
		    &r_rtname, NULL);
		rv = scha_status.err_code;
		if (rv != SCHA_ERR_NOERR) {
			clerror("%s: Invalid resource type name\n",
				resourcetypes.getValue());
			if (first_err == CL_NOERR) {
				// first error!
				first_err = map_scha_error(rv);
			}
		} else {
			sys_rts.add(r_rtname);
		}
	}

	if ((resourcetypes.getSize() > 0) && (sys_rts.getSize() == 0))
		return (first_err);

	if ((states.getSize() > 0) && (good_states.getSize() == 0))
		return (first_err);

	if ((nodes.getSize() > 0) && (good_nodes.getSize() == 0))
		return (first_err);

	if (!rg_names)
		return (first_err);

	for (i = 0; rg_names[i] != NULL; i++) {

		if (rgconf) {
			rgm_free_rg(rgconf);
			rgconf = (rgm_rg_t *)NULL;
		}
		if (rs_names) {
			rgm_free_strarray(rs_names);
			rs_names = (char **)NULL;
		}
		if (rg_name) {
			free(rg_name);
		}
		if (zc_name) {
			free(zc_name);
		}

		rg_name = zc_name = NULL;
		// Split the RG name and zone cluster name
		scconf_err = scconf_parse_obj_name_from_cluster_scope(
					rg_names[i], &rg_name, &zc_name);
		if (scconf_err != SCCONF_NOERR) {
			// We should not be coming in here actually.
			continue;
		}

		// Get configuration information
		scha_status = rgm_scrgadm_getrgconf(rg_name, &rgconf, zc_name);
		rv = scha_status.err_code;
		if (rv != SCHA_ERR_NOERR) {
			print_rgm_error(scha_status, NULL);
			clerror("Failed to access information for "
			    "resource group \"%s\".\n", rg_names[i]);
			if (first_err == 0) {
				// first error!
				first_err = map_scha_error(rv);
			}
			continue;
		}

		// Get the list of resources for this group
		scha_status = rgm_scrgadm_getrsrclist(rg_name, &rs_names,
							zc_name);
		rv = scha_status.err_code;
		if (rv != SCHA_ERR_NOERR) {
			print_rgm_error(scha_status, NULL);
			clerror("Failed to access information for "
			    "resource group \"%s\".\n", rg_names[i]);
			if (first_err == 0) {
				// first error!
				first_err = map_scha_error(rv);
			}
			continue;
		}

		if (rgstat) {
			scstat_free_rg(rgstat);
			rgstat = NULL;
		}

		// Get the status information
		scstat_status = scstat_get_rg(rg_names[i], &rgstat, B_TRUE);
		if (scstat_status != SCSTAT_ENOERR) {
			print_scstat_error(scstat_status, NULL);
			clerror("Failed to access information for "
			    "resource group \"%s\".\n", rg_names[i]);
			if (first_err == 0) {
				// first error!
				first_err = map_scstat_error(rv);
			}
			continue;
		}

		// Do the node filtering and status collection
		notonline = 1;
		offline = 1;
		node_found = 0;
		state_found = 0;
		for (rgnode = rgstat->scstat_rg_status_list; rgnode;
		    rgnode = rgnode->scstat_rg_status_by_node_next) {
			if (strcasecmp(rgnode->scstat_rg_statstr, "Online")
			    == 0) {
				notonline = 0;
				for (good_nodes.start(); !good_nodes.end();
				    good_nodes.next()) {
					if (strcmp(good_nodes.getValue(),
					    rgnode->scstat_node_name) == 0) {
						node_found = 1;
					}
				}
			}
			if (strcasecmp(rgnode->scstat_rg_statstr, "Offline")
			    != 0) {
				for (good_states.start(); !good_states.end();
				    good_states.next()) {
					if (strcasecmp(good_states.getValue(),
					    scstat_str_to_cl_str(
					    rgnode->scstat_rg_statstr,
					    CL_TYPE_RG)) == 0) {
						state_found = 1;
					}
				}
				offline = 0;
			}
		}
		for (good_states.start(); !good_states.end();
		    good_states.next()) {
			if (((strcasecmp(good_states.getValue(), "not_online")
			    == 0) && notonline) ||
			    ((strcasecmp(good_states.getValue(), "offline")
			    == 0) && offline)) {
				state_found = 1;
			}
		}

		// Do the resource and resource type filtering
		rs_found = 0;
		rt_found = 0;
		if ((rs_names != NULL) && (*rs_names != NULL)) {
			for (j = 0; rs_names[j] != NULL; j++) {
				if (rs) {
					rgm_free_resource(rs);
					rs = (rgm_resource_t *)NULL;
				}
				scha_status = rgm_scrgadm_getrsrcconf(
				    rs_names[j], &rs, zc_name);
				rv = scha_status.err_code;
				if (rv != SCHA_ERR_NOERR) {
					print_rgm_error(scha_status,
					    rs_names[j]);
					if (first_err == 0) {
						// first error!
						first_err = map_scha_error(rv);
					}
					continue;
				}
				if (resources.getSize() > 0) {
					for (resources.start();
					    !resources.end();
					    resources.next()) {
						if (strcmp(resources.getValue(),
						    rs_names[j]) == 0) {
							rs_found = 1;
							break;
						}
					}
					if (rs_found)
						break;
				}
				if (sys_rts.getSize() > 0) {
					for (sys_rts.start(); !sys_rts.end();
					    sys_rts.next()) {
						if (strcmp(sys_rts.getValue(),
						    rs->r_type) == 0) {
							rt_found = 1;
							break;
						}
					}
					if (rt_found)
						break;
				}
			}
		}

		if ((resources.getSize() > 0) ||
		    (resourcetypes.getSize() > 0) ||
		    (nodes.getSize() > 0) ||
		    (states.getSize() > 0)) {
			if ((!rs_found && (resources.getSize() > 0)) ||
			    (!rt_found && (resourcetypes.getSize() > 0)) ||
			    (!node_found && (nodes.getSize() > 0)) ||
			    (!state_found && (states.getSize() > 0))) {
				if (wildcard) {
					continue;
				} else {
					if (first_err == 0) {
						// first error!
						first_err = CL_EINVAL;
						clerror("Specified resource "
						    "group \"%s\" does not "
						    "meet the criteria set by "
						    "the \"-t\", \"-r\", "
						    "\"-n\", and \"-s\" "
						    "options.\n", rg_names[i]);
					}
					continue;
				}
			}
		}

		first_node = 1;

		for (rgnode = rgstat->scstat_rg_status_list; rgnode;
		    rgnode = rgnode->scstat_rg_status_by_node_next) {

			if (first_node) {
				col1.add(rg_names[i]);
				first_node = 0;
			} else {
				col1.add("");
			}
			col2.add(rgnode->scstat_node_name);
			if (rgconf->rg_suspended == B_FALSE)
				col3.add("No");
			else
				col3.add("Yes");
			col4.add(rgnode->scstat_rg_statstr);
		}
	}

	if (col1.getSize() > 0) {
		for (col1.start(), col2.start(), col3.start(), col4.start();
		    col1.end() != 1;
		    col1.next(), col2.next(), col3.next(), col4.next()) {

			rg_status->addRow(col1.getValue(), col2.getValue(),
			    col3.getValue(), col4.getValue());
		}
	}

	if (rs_names)
		rgm_free_strarray(rs_names);

	if (rgconf)
		rgm_free_rg(rgconf);

	if (rg_names)
		rgm_free_strarray(rg_names);

	if (rgstat)
		scstat_free_rg(rgstat);

	if (rg_name) {
		free(rg_name);
	}
	if (zc_name) {
		free(zc_name);
	}
	if (r_rtname) {
		free(r_rtname);
	}
	tmp_list.clear();

	return (first_err);
}
