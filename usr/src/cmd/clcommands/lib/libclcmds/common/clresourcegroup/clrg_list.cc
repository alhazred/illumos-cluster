//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clrg_list.cc	1.18	09/03/05 SMI"

//
// Process clrg "list"
//

#include <sys/os.h>
#include <scadmin/scconf.h>
#include <scadmin/scswitch.h>
#include <scadmin/scstat.h>
#include <rgm/rgm_scrgadm.h>
#include <rgm/rgm_cnfg.h>

#include "clcommands.h"

#include "ClCommand.h"
#include "ClList.h"

#include "rgm_support.h"
#if (SOL_VERSION >= __s10)
#include "clzonecluster.h"
#endif
#include "common.h"

//
// clrg "list"
// "operands" must either contain only one item (wildcard),
// or must contain the list of RG names.
//
clerrno_t
clrg_list(ClCommand &cmd, ValueList &operands, optflgs_t optflgs,
    ValueList &resources, ValueList &resourcetypes, ValueList &nodes,
    ValueList &zc_names)
{
	clerrno_t first_err = CL_NOERR;
	int rv = 0;
	int i, j, str_len;
	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};
	char **rg_names = NULL;
	char **rs_names = (char **)0;
	char *oper1, *zc_name, *rg_name;
	clerrno_t errflg = CL_NOERR;
	clerrno_t clerrno = CL_NOERR;
	ClList print_list = ClList(cmd.isVerbose ? true : false);
	int wildcard = 0;
	char *rgmode;
	char *rgstatus;
	rgm_rg_t *rgconf = (rgm_rg_t *)0;
	int rs_found, rt_found, node_found;
	rgm_rt_t *rt = (rgm_rt_t *)0;
	rgm_resource_t *rs = (rgm_resource_t *)0;
	char *r_rtname = NULL;
	ValueList col1, col2, col3;
	scstat_errno_t scstat_status;
	scstat_rg_t *rgstat;
	scstat_rg_status_t *rgnode;
	ValueList good_nodes;
	ValueList tmp_list, valid_zc_list;
	scconf_errno_t scconf_err = SCCONF_NOERR;
	boolean_t found_rg, has_rg;

	rg_name = zc_name = NULL;
	// Initializa ORB and other cluster checks
	clerrno = cluster_init();
	if (clerrno)
		return (clerrno);

	// Zone cluster names can be specified either with the -Z
	// option or in the form of <zonecluster>:<RG name> format.
	// But, both the ways cannot be used simultaneously.
	// If zone clusters were not specified with the -Z option, we
	// have to check whether they were specified in the <zc>:<rg>
	// format.
	for (operands.start(); !operands.end(); operands.next()) {
		scconf_err = scconf_parse_obj_name_from_cluster_scope(
				operands.getValue(), &rg_name, &zc_name);

		if (scconf_err != SCCONF_NOERR) {
			return (CL_ENOMEM);
		}

		if (zc_name && zc_names.getSize()) {
			// -Z flag was specified and this RG was scoped
			// with a zone cluster name as well. Report an
			// error.
			clerror("You cannot use the "
				"\"<zonecluster>:<rgname>\" "
				"form of the resource-group name with the "
				"\"-Z\" option.");
			free(rg_name);
			free(zc_name);
			return (CL_EINVAL);
		}

		// If this zone cluster name does not exist in tmp_list,
		// then we wil add it here.
		if (!tmp_list.isValue(zc_name)) {
			tmp_list.add(zc_name);
		}

		if (rg_name) {
			free(rg_name);
			rg_name = NULL;
		}
		if (zc_name) {
			free(zc_name);
			zc_name = NULL;
		}
	}
#if (SOL_VERSION >= __s10)
	// By the time we are here, we have added any zone cluster name in the
	// ResourceGroup names to tmp_list.
	if (zc_names.getSize()) {
		// Zc names were specified. We have to check whether "all"
		// was specified.
		zc_names.start();

		if (strcmp(zc_names.getValue(), ALL_CLUSTERS_STR) == 0) {
			// Get all known zone clusters.
			clerrno = get_all_clusters(valid_zc_list);

			if (clerrno != CL_NOERR) {
				return (clerrno);
			}
		} else {
			tmp_list = zc_names;
		}
	}

	// valid_zc_list will contain at least one zone cluster name
	// if "all" was specified. If it is empty, we have to check
	// the validity of the zone clusters specified by the user.
	if (valid_zc_list.getSize() < 1) {
		clerrno = get_valid_zone_clusters(tmp_list, valid_zc_list);
	}
	// Clear the memory.
	tmp_list.clear();

	if (clerrno) {
		// There was at least one invalid zone cluster.
		// We have to return.
		return (clerrno);
	}
#endif

	// If we are here, then it means the specified zone clusters
	// are valid, if they were specified. We have to check the
	// validity of the nodes, in case they were specified.

	clerrno = get_good_nodes(nodes, good_nodes, valid_zc_list);
	if (clerrno) {
		// One or more invalid node names. We
		// have to return.
		good_nodes.clear();
		return (clerrno);
	}

	// Check whether wildcard was specified.
	if (operands.getSize() > 0) {
		operands.start();
		// Get the zone cluster name and the RG name
		scconf_err = scconf_parse_obj_name_from_cluster_scope(
			operands.getValue(), &oper1, &zc_name);

		// Return if there was an error.
		if (scconf_err != SCCONF_NOERR) {
			clerror("Internal error.\n");
			return (CL_EINTERNAL);
		}

		// Now check whether a wildcard was specified.
		if (strcmp(oper1, CLCOMMANDS_WILD_OPERAND)) {
			// RG names were specified, we have to check
			// whether the names are valid or not.
			// Add all the valid RG names to tmp_list.
			for (operands.start(); !operands.end();
						operands.next()) {

				// If zone cluster names were not specified
				// using the -Z flag, we can check their
				// validity right here.
				if (zc_names.getSize() < 1)  {
					if (!is_good_rg(operands.getValue(),
							B_TRUE)) {
						// No need to print errors.
						if (first_err == CL_NOERR) {
							first_err = CL_ENOENT;
						}
						continue;
					}
					// This is a valid RG. Add it
					// to tmp_list.
					tmp_list.add(operands.getValue());
					continue;
				}

#if (SOL_VERSION >= __s10)
				// If we are here, then it means that zone
				// cluster names were specified using the
				// -Z flag.
				found_rg = B_FALSE;
				// Now check this RG in each zone cluster
				for (zc_names.start(); !zc_names.end();
					zc_names.next()) {
					has_rg = B_FALSE;
					scconf_err = scconf_does_zc_have_rg(
							zc_names.getValue(),
							operands.getValue(),
							&has_rg);

					if (!has_rg) {
						continue;
					}

					// This zone cluster has the RG
					found_rg = B_TRUE;
					// Account chars for ':' and NULL
					str_len = strlen(zc_names.getValue())
							+ 2;
					str_len += strlen(operands.getValue());
					rg_name = (char *)calloc(str_len, 1);
					strcat(rg_name, zc_names.getValue());
					strcat(rg_name, ":");
					strcat(rg_name, operands.getValue());
					tmp_list.add(rg_name);
					free(rg_name);
					rg_name = NULL;
				}

				if (found_rg) {
					// Valid resource group.
					continue;
				}
#endif

				// If we are here then it means that his RG
				// does not exist across any of the specified
				// zone clusters. Report an error now and
				// return.
				clerror("%s: Invalid resource group\n",
					operands.getValue());
				if (first_err == CL_NOERR) {
					first_err = CL_ENOENT;
				}
			}

			// Any errors would have been reported by now.
			// Get the RGnames as char** from tmp_list.
			rg_names = vl_to_names(tmp_list, &errflg);
			tmp_list.clear();
			if (errflg)
				return (errflg);
		} else {
			wildcard = 1;
		}
	} else {
		wildcard = 1;
	}

	if (wildcard) {

		// If zone cluster names were not specified, fetch the
		// default set of RG names.
		if (valid_zc_list.getSize() == 0) {
			scha_status = scswitch_getrglist(&rg_names,
				B_FALSE, NULL);
			rv = scha_status.err_code;
			if (rv != SCHA_ERR_NOERR) {
				clerror("Failed to obtain list of all resource "
				    "groups.\n");
				print_rgm_error(scha_status, NULL);
				return (map_scha_error(rv));
			}
		} else {
			// Zone clusters were specified.
			for (valid_zc_list.start(); !valid_zc_list.end();
				valid_zc_list.next()) {
				scha_status = scswitch_getrglist(&rg_names,
						B_FALSE,
						valid_zc_list.getValue());
				rv = scha_status.err_code;
				clerrno = names_to_valuelist(rg_names,
						tmp_list);
				rgm_free_strarray(rg_names);

				if ((rv != SCHA_ERR_NOERR) ||
				    (clerrno != CL_NOERR)) {
					clerror("Failed to obtain list of "
					"all resource groups.\n");
					// Free memory.
					tmp_list.clear();
					print_rgm_error(scha_status, NULL);
					return (map_scha_error(rv));
				}
			}

			// Get all the RG names
			rg_names = vl_to_names(tmp_list, &clerrno);
			tmp_list.clear();
			if (clerrno != CL_NOERR) {
				return (clerrno);
			}
		}
	}

	// By now, we should have fetched all the RGs. If there is nothing
	// to process, then just return
	if (!rg_names) {
		// Free any memory
		if (zc_names.getSize()) {
			zc_names.clear();
		}
		return (first_err);
	}

	// If resources were specified, we have to check whether they
	// are valid resources or not.
	for (resources.start(); !resources.end(); resources.next()) {

#if SOL_VERSION >= __s10
		// If zone clusters were specified, we have to check whether
		// this resource is valid in at least one of the zone
		// clusters.
		if (valid_zc_list.getSize() > 0) {
			clerrno = get_zone_clusters_with_rs(
					resources.getValue(), tmp_list,
					valid_zc_list);
			tmp_list.clear();
			if (clerrno != CL_NOERR) {
				// This resource was not present in any
				// of the zone clusters.
				clerror("%s: Invalid resource name\n",
					resources.getValue());
				if (first_err == CL_NOERR) {
					first_err = clerrno;
				}
			}

			continue;
		}
#endif

		// If we are here, it means no zone cluster names were
		// specified. We have to check the validity of the resource
		// in the current cluster itself.
		if (rs) {
			rgm_free_resource(rs);
			rs = (rgm_resource_t *)NULL;
		}

		scha_status = rgm_scrgadm_getrsrcconf(resources.getValue(),
		    &rs, NULL);
		rv = scha_status.err_code;
		if (rv != SCHA_ERR_NOERR) {
			clerror("%s: Invalid resource name\n",
				resources.getValue());
			if (first_err == CL_NOERR) {
				// first error!
				first_err = map_scha_error(rv);
			}
		}
	}

	// If resource types were specified, we have to check their validity.

	for (resourcetypes.start(); !resourcetypes.end();
	    resourcetypes.next()) {
#if SOL_VERSION >= __s10
		// If zone clusters were specified, we have to check whether
		// this RT is valid in at least one of the zone
		// clusters.
		if (valid_zc_list.getSize() > 0) {
			clerrno = get_zone_clusters_with_rt(
					resourcetypes.getValue(), tmp_list,
					valid_zc_list);
			tmp_list.clear();
			if (clerrno != CL_NOERR) {
				// This RT was not present in any
				// of the zone clusters.
				clerror("%s: Invalid resource type name\n",
					resourcetypes.getValue());
				if (first_err == CL_NOERR) {
					first_err = clerrno;
				}
			}

			continue;
		}
#endif

		// If we are here, it means no zone cluster names were
		// specified. We have to check the validity of the RT
		// in the current cluster itself.
		if (r_rtname) {
			free(r_rtname);
			r_rtname = (char *)NULL;
		}
		scha_status = rgmcnfg_get_rtrealname(resourcetypes.getValue(),
		    &r_rtname, NULL);
		rv = scha_status.err_code;
		if (rv != SCHA_ERR_NOERR) {
			clerror("%s: Invalid resource type name\n",
				resourcetypes.getValue());
			if (first_err == CL_NOERR) {
				// first error!
				first_err = map_scha_error(rv);
			}
		}
	}

	// By now we have verified all valid nodes, valid resources,
	// valid resource types, valid zone clusters and valid RGs.
	// Each RG should match at least one of the following criteria:
	// (a) should have at least one node from "good_nodes" in its
	// nodelist.
	// (b) should have at least one resource from "resources" in
	// its list of resources.
	// (c) should have at least one registered RT from "resourcetypes".
	// We have to check that each RG matches at least one of the above
	// conditions if they were specified.

	for (i = 0; rg_names[i] != NULL; i++) {

		node_found = rs_found = rt_found = 0;

		if (rgconf) {
			rgm_free_rg(rgconf);
			rgconf = (rgm_rg_t *)NULL;
		}

		if (rs_names) {
			rgm_free_strarray(rs_names);
			rs_names = (char **)NULL;
		}

		if (rg_name) {
			free(rg_name);
		}
		if (zc_name) {
			free(zc_name);
		}

		rg_name = zc_name = NULL;
		// Split the RG name and zone cluster name
		scconf_err = scconf_parse_obj_name_from_cluster_scope(
					rg_names[i], &rg_name, &zc_name);
		if (scconf_err != SCCONF_NOERR) {
			// We should not be coming in here actually.
			continue;
		}

		// Get configuration information
		scha_status = rgm_scrgadm_getrgconf(rg_name, &rgconf, zc_name);
		rv = scha_status.err_code;
		if (rv != SCHA_ERR_NOERR) {
			// We should ideally not get in here, since we have
			// already verified all the RGs by now.
			print_rgm_error(scha_status, NULL);
			clerror("Failed to access information for "
			    "resource group \"%s\".\n", rg_names[i]);
			if (first_err == 0) {
				// first error!
				first_err = map_scha_error(rv);
			}
			continue;
		}
		if (rgconf->rg_mode == RGMODE_FAILOVER)
			rgmode = "Failover";
		else
			rgmode  = "Scalable";

		// Get the list of resources for this group
		scha_status = rgm_scrgadm_getrsrclist(rg_name, &rs_names,
							zc_name);
		rv = scha_status.err_code;
		if (rv != SCHA_ERR_NOERR) {
			print_rgm_error(scha_status, NULL);
			clerror("Failed to access information for "
			    "resource group \"%s\".\n", rg_names[i]);
			if (first_err == 0) {
				// first error!
				first_err = map_scha_error(rv);
			}
			continue;
		}

		// Get the status information
		scstat_status = scstat_get_rg(rg_names[i], &rgstat, B_TRUE);
		if (scstat_status != SCSTAT_ENOERR) {
			print_scstat_error(scstat_status, NULL);
			clerror("Failed to access information for "
			    "resource group \"%s\".\n", rg_names[i]);
			if (first_err == 0) {
				// first error!
				first_err = map_scstat_error(rv);
			}
			continue;
		}

		// Do the node filtering and status collection
		rgstatus = "not_online";
		node_found = 0;
		for (rgnode = rgstat->scstat_rg_status_list; rgnode;
		    rgnode = rgnode->scstat_rg_status_by_node_next) {
			if (strcmp(rgnode->scstat_rg_statstr, "Online") == 0) {
				rgstatus = "online";
				for (good_nodes.start(); !good_nodes.end();
				    good_nodes.next()) {
					if (strcmp(good_nodes.getValue(),
					    rgnode->scstat_node_name) == 0) {
						node_found = 1;
					}
				}
			}
		}

		// If "node_found" was set, then this RG has satisfied the
		// the node filter. No need to check the rest of the filters.
		if (node_found) {
			col1.add(rg_names[i]);
			col2.add(rgmode);
			col3.add(rgstatus);
			continue;
		}

		// If we are here, this RG did not satisfy the node filter.
		// However, it is possible that none of the filters was
		// specified. If that were the case, then there is no need
		// to check this RG for the remaining filters. We have to make
		// this check here, since we need the previous code to fetch
		// RG status.
		if ((resourcetypes.getSize() < 1) &&
		    (resources.getSize() < 1) && (good_nodes.getSize() < 1)) {
			// No filter was specified
			col1.add(rg_names[i]);
			col2.add(rgmode);
			col3.add(rgstatus);
			continue;
		}

		// If we are here, then it means at least one filter was
		// specified.

		// If we are here, this RG did not satisfy the node filter.
		// We have to check the resource and RT filtering.
		// First, check for resource filter.
		if ((rs_names != NULL) && (*rs_names != NULL) &&
			(resources.getSize() > 0)) {
			for (j = 0; rs_names[j] != NULL; j++) {
				if (resources.isValue(rs_names[j])) {
					// Found the resource.
					rs_found = 1;
					break;
				}
			}
		}

		// If the resource filter passed, no need to check the
		// RT filter.
		if (rs_found) {
			col1.add(rg_names[i]);
			col2.add(rgmode);
			col3.add(rgstatus);
			continue;
		}

		// If we are here, then we have to check the RT filter.
		// If there are no resources, or if the RT type filter
		// was not specified, then we dont have to add this RG.
		if ((rs_names == NULL) || (*rs_names == NULL) ||
			(resourcetypes.getSize() < 1)) {
			continue;
		}

		for (j = 0; rs_names[j] != NULL; j++) {
			if (rs) {
				rgm_free_resource(rs);
				rs = (rgm_resource_t *)NULL;
			}

			scha_status = rgm_scrgadm_getrsrcconf(
				rs_names[j], &rs, zc_name);
			rv = scha_status.err_code;
			if (rv != SCHA_ERR_NOERR) {
				// We should not be coming in here since
				// all the resources have been verified.
				clerror("Internal error\n");
				if (first_err == CL_NOERR) {
					// first error!
					first_err = map_scha_error(rv);
				}
				continue;
			}
			// Check whether the RT of this resource matches with
			// at least one of the RTs specified.
			if (resourcetypes.isValue(rs->r_type)) {
				rt_found = 1;
				break;
			}
		}

		// Check whether this RG satisfied the RT filter
		if (rt_found) {
			col1.add(rg_names[i]);
			col2.add(rgmode);
			col3.add(rgstatus);
			continue;
		}

		// If we are here, it means this RG did not satisfy any of the
		// filters. We have to report an error if RGs were specified
		// as operands.
		if (operands.getSize() > 0) {
			clerror("Specified resource group "
				    "\"%s\" does not meet the criteria "
				    "set by the \"-t\", \"-r\", or "
				    "\"-n\" options.\n",
				    rg_names[i]);
		}
	}

	if (col1.getSize() > 0) {
		print_list.setHeadings(
		    (char *)gettext("Resource Group"),
		    (char *)gettext("Mode"),
		    (char *)gettext("Overall status"));
		for (col1.start(), col2.start(), col3.start();
		    col1.end() != 1;
		    col1.next(), col2.next(), col3.next()) {

			print_list.addRow(col1.getValue(), col2.getValue(),
			    col3.getValue());
		}
		print_list.print();

	}

	if (rs_names)
		rgm_free_strarray(rs_names);

	if (rgconf)
		rgm_free_rg(rgconf);

	if (rg_names)
		rgm_free_strarray(rg_names);

	return (first_err);
}
