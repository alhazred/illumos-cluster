//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clrg_add_node.cc	1.12	08/06/06 SMI"

//
// Process clrg "add-node"
//

#include <sys/os.h>
#include <scadmin/scconf.h>
#include <scadmin/scswitch.h>
#include <scadmin/scstat.h>
#include <rgm/rgm_scrgadm.h>

#include "clcommands.h"

#include "ClCommand.h"

#include "rgm_support.h"
#include "common.h"

//
// clrg "add-node"
//
clerrno_t
clrg_add_node(ClCommand &cmd, ValueList &operands, optflgs_t optflgs,
    ValueList &nodes)
{
	clerrno_t first_err = CL_NOERR;
	int rv = 0;
	int i;
	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};
	char **rg_names = NULL;
	char *oper1;
	rgm_rg_t *rg = (rgm_rg_t *)0;
	namelist_t *new_nodelist = (namelist_t *)NULL;
	namelist_t *nl1;
	scha_property_t nodeprop[4];
	namelist_t desired_nl;
	namelist_t maximum_nl;
	char str_nodes[10];
	int num_nodes;
	clerrno_t errflg = CL_NOERR;
	clerrno_t clerrno = CL_NOERR;
	ValueList good_nodes, tmp_list;
	namelist_t *node_nl;
	ValueList new_nl_vl;
	boolean_t verbose = B_FALSE;
	scconf_errno_t scconf_err = SCCONF_NOERR;
	boolean_t is_same = B_FALSE;
	char *rg_part, *zc_part, *zc_name;

	// Initializa ORB and other cluster checks
	clerrno = cluster_init();
	if (clerrno) {
		return (clerrno);
	}

	// Check whether any zone cluster names were specified.
	// And if they were specified, there should be only zone cluster
	// across all the RGs.
	rg_part = zc_part = zc_name = NULL;
	clerrno = check_single_zc_specified(operands, &zc_name, &is_same);
	if (clerrno != CL_NOERR) {
		clcommand_perror(clerrno);
		return (clerrno);
	}
	// If multiple zone clusters were specified, report an error.
	if (is_same == B_FALSE) {
		clerror("This operation can be performed on only one "
		    "zone cluster at a time.\n");
		return (clerrno);
	}
#if (SOL_VERSION >= __s10)
	if (zc_name) {
		// If zone cluster name was specified, we have to ensure
		// that it is valid.
		// We have to check whether this operation is allowed for
		// the status of the zone cluster.
		clerrno = check_zc_operation(CL_OBJ_TYPE_RG,
						CL_OP_TYPE_ADD_NODE,
						zc_name);
		if (clerrno != CL_NOERR) {
			return (clerrno);
		}
	}
#endif

	if (optflgs & vflg)
		verbose = B_TRUE;

	tmp_list.add(zc_name);
	clerrno = get_good_nodes(nodes, good_nodes, tmp_list);
	tmp_list.clear();

	if (clerrno)
		return (clerrno);

	operands.start();
	oper1 = operands.getValue();
	if (strcmp(oper1, CLCOMMANDS_WILD_OPERAND)) {
		rg_names = vl_to_names(operands, &errflg);
		if (errflg)
			return (errflg);
	} else {
		clerrno = z_getrglist(&rg_names, verbose, optflgs & uflg,
					zc_name);
		if (clerrno != CL_NOERR) {
			return (clerrno);
		}
	}

	if (rg_names) {
		for (i = 0; rg_names[i] != NULL; i++) {

			memset(nodeprop, 0, 4 * (sizeof (scha_property_t)));

			if (rg != (rgm_rg_t *)0) {
				rgm_free_rg(rg);
				rg = (rgm_rg_t *)0;
			}
			if (new_nodelist) {
				rgm_free_nlist(new_nodelist);
				new_nodelist = (namelist_t *)NULL;
			}
			// First parse the RG name and the cluster name.
			if (rg_part) {
				free(rg_part);
				rg_part = NULL;
			}
			if (zc_part) {
				free(zc_part);
				zc_part = NULL;
			}

			// Split the RG name and zone cluster name
			scconf_err = scconf_parse_obj_name_from_cluster_scope(
					rg_names[i], &rg_part, &zc_part);
			if (scconf_err != SCCONF_NOERR) {
				// We should not be coming in here actually.
				clerrno = map_scconf_error(scconf_err);
				clcommand_perror(clerrno);
				if (first_err == CL_NOERR) {
					first_err = clerrno;
				}
				continue;
			}
			// Get the RG details
			scha_status = rgm_scrgadm_getrgconf(rg_part, &rg,
								zc_part);
			rv = scha_status.err_code;
			if (rv != SCHA_ERR_NOERR) {
				print_rgm_error(scha_status, rg_names[i]);
				if (first_err == 0) {
					// first error!
					first_err = map_scha_error(rv);
				}
				rgm_free_rg(rg);
				continue;
			}
			if (rg == NULL) {
				clerror("Failed to obtain resource group "
				    "information for \"%s\".\n", rg_names[i]);
				if (first_err == 0) {
					// first error!
					first_err = CL_ENOENT;
				}
				rgm_free_rg(rg);
				continue;
			}

			// If it is not a scalable RG, -S can't be provided
			if (optflgs & Sflg) {
				if (rg->rg_mode != RGMODE_SCALABLE) {
					clerror("Resource group \"%s\" is not "
					    "a scalable resource group, \"-S\" "
					    "is not applicable. Skipping "
					    "\"%s\".\n",
					    rg_names[i], rg_names[i]);
					if (first_err == 0) {
						// first error!
						first_err = CL_EINVAL;
					}
					rgm_free_rg(rg);
					continue;
				}
			}

			// convert rg nodeidlist to namelist
			node_nl = nodeidlist_to_namelist(rg->rg_nodelist,
			    &errflg, zc_part);
			if (errflg) {
				rgm_free_rg(rg);
				return (errflg);
			}
			// Add new nodes to the nodelist
			new_nodelist = add_vl_to_names(node_nl, good_nodes,
			    &errflg);
			if (node_nl)
				rgm_free_nlist(node_nl);
			if (errflg) {
				rgm_free_rg(rg);
				return (errflg);
			}

			// Check for server-farm mix for this object
			new_nl_vl.clear();
			namelist_to_vl(new_nodelist, new_nl_vl);
			clerrno = check_server_farm_mix(new_nl_vl, rg_names[i]);
			if (clerrno != CL_NOERR) {
				if (first_err == 0) {
					// first error!
					first_err = clerrno;
				}
				rgm_free_rg(rg);
				rgm_free_nlist(new_nodelist);
				continue;
			}

			// Figure out the number of nodes in the new nodelist
			nl1 = new_nodelist;
			num_nodes = 0;
			while (nl1) {
				num_nodes++;
				nl1 = nl1->nl_next;
			}
			sprintf(str_nodes, "%d", num_nodes);
			maximum_nl.nl_name = str_nodes;
			maximum_nl.nl_next = NULL;
			desired_nl.nl_name = str_nodes;
			desired_nl.nl_next = NULL;

			// Set the new nodelist and associated properties
			nodeprop[0].sp_name = "Nodelist";
			nodeprop[0].sp_value = new_nodelist;
			if (optflgs & Sflg) {
				nodeprop[1].sp_name = "Maximum_primaries";
				nodeprop[1].sp_value = &maximum_nl;
				nodeprop[2].sp_name = "Desired_primaries";
				nodeprop[2].sp_value = &desired_nl;
				nodeprop[3].sp_name = NULL;
			} else {
				nodeprop[1].sp_name = NULL;
			}

			scha_status = modify_rg(rg_names[i], nodeprop,
			    FROM_SCRGADM, (const char **)rg_names);
			rv = filter_warning(scha_status.err_code);
			if (rv) {
				print_rgm_error(scha_status, rg_names[i]);
				if (first_err == 0) {
					// first error!
					first_err = map_scha_error(rv);
				}
			} else {
				if (scha_status.err_msg) {
					fprintf(stdout, "%s\n",
					    scha_status.err_msg);
				}
				if (optflgs & vflg) {
					nl1 = new_nodelist;
					clmessage("New nodelist set for "
					    "resource group \"%s\": ",
					    rg_names[i]);
					while (nl1) {
						fprintf(stdout, "%s ",
						    nl1->nl_name);
						nl1 = nl1->nl_next;
					}
					fprintf(stdout, "\n");
				}
			}
			rgm_free_nlist(new_nodelist);
			rgm_free_rg(rg);
		}
	}

	rgm_free_strarray(rg_names);

	if (zc_name)
		free(zc_name);
	if (rg_part)
		free(rg_part);
	if (zc_part)
		free(zc_part);

	return (first_err);
}
