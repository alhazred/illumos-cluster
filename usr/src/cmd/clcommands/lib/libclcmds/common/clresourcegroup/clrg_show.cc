//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clrg_show.cc	1.16	08/10/13 SMI"

//
// Process clrg "show"
//

#include <sys/os.h>
#include <scadmin/scconf.h>
#include <scadmin/scswitch.h>
#include <scadmin/scstat.h>
#include <scadmin/scrgadm.h>
#include <rgm/rgm_scrgadm.h>
#include <rgm/rgm_cnfg.h>

#include "clcommands.h"

#include "ClCommand.h"
#include "ClShow.h"

#include "clresource.h"
#include "rgm_support.h"
#include "common.h"

static clerrno_t
get_clrg_show_obj_select(ValueList &operands, optflgs_t optflgs,
    ValueList &resources, ValueList &resourcetypes, ValueList &properties,
    ValueList &nodes, ClShow *rg_show, ValueList &zc_names = ValueList());

//
// clrg "show"
//
clerrno_t
clrg_show(ClCommand &cmd, ValueList &operands, optflgs_t optflgs,
    ValueList &resources, ValueList &resourcetypes, ValueList &properties,
    ValueList &nodes, ValueList &zc_names)
{
	clerrno_t clerrno = CL_NOERR;
	ClShow *rg_show = new ClShow(HEAD_RESGROUPS);

	// Initializa ORB and other cluster checks
	clerrno = cluster_init();
	if (clerrno)
		return (clerrno);

	clerrno = get_clrg_show_obj_select(operands, optflgs, resources,
	    resourcetypes, properties, nodes, rg_show, zc_names);
	(void) rg_show->print();
	return (clerrno);
}

//
// clrg "show" for cluster command
//
int
get_clrg_show_obj(optflgs_t optflgs, ClShow *rg_show)
{
	clerrno_t clerrno = CL_NOERR;
	ValueList rg_list = ValueList(true);
	ValueList empty_list;

	rg_list.add(CLCOMMANDS_WILD_OPERAND);
	clerrno = get_clrg_show_obj_select(rg_list, optflgs, empty_list,
	    empty_list, empty_list, empty_list, rg_show);
	return (clerrno);
}

//
// Fetch the show buffer according to options/operands.
//
clerrno_t
get_clrg_show_obj_select(ValueList &operands, optflgs_t optflgs,
    ValueList &resources, ValueList &resourcetypes, ValueList &properties,
    ValueList &nodes, ClShow *rg_show, ValueList &zc_names)
{
	clerrno_t first_err = CL_NOERR;
	int rv = 0;
	int i, j;
	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};
	char **rg_names = NULL;
	char **rs_names = (char **)0;
	char *oper1;
	clerrno_t errflg = CL_NOERR;
	clerrno_t clerrno = CL_NOERR;
	ClShow **show_ptr;
	int wildcard = 0;
	char *rgstatus;
	rgm_rg_t *rgconf = (rgm_rg_t *)0;
	int rs_found, rt_found, node_found;
	ValueList sys_rts;
	rgm_rt_t *rt = (rgm_rt_t *)0;
	rgm_resource_t *rs = (rgm_resource_t *)0;
	char *r_rtname = NULL;
	NameValueList showdata;
	scstat_errno_t scstat_status;
	scstat_rg_t *rgstat;
	scstat_rg_status_t *rgnode;
	char *new_rg, *new_entry;
	int props_specified = 0;
	char *proj_ptr = (char *)NULL;
	char *gru_ptr = (char *)NULL;
	char *aff_str = (char *)NULL;
	char *node_str = (char *)NULL;
	char *rgdep_str = (char *)NULL;
	char *proj_ptr2 = (char *)NULL;
	char *slm_type = (char *)NULL;
	char *slm_type2 = (char *)NULL;
	char *slm_proj = (char *)NULL;
	char *slm_pset = (char *)NULL;
	char *slm_pset2 = (char *)NULL;
	char *gru_ptr2 = (char *)NULL;
	int verbose = 0;
	char vstr[BUFSIZ];
	int k = 0;
	int num_rgs = 0;
	ValueList good_nodes;
	ValueList rslist;
	ValueList empty_list;
	namelist_t *node_nl;
	ValueList props_needed;
	int prop_found = 0;
	ValueList allproplist, valid_zc_list, tmp_list, good_rgs;
	scconf_errno_t scconf_err = SCCONF_NOERR;
	char *rg_name, *zc_name, *fullname;

	rg_name = zc_name = fullname = NULL;
	// Zone cluster names can be specified either with the -Z
	// option or in the form of <zonecluster>:<RG name> format.
	// But, both the ways cannot be used simultaneously.
	// If zone clusters were not specified with the -Z option, we
	// have to check whether they were specified in the <zc>:<rg>
	// format.
	for (operands.start(); !operands.end(); operands.next()) {
		scconf_err = scconf_parse_obj_name_from_cluster_scope(
				operands.getValue(), &rg_name, &zc_name);

		if (scconf_err != SCCONF_NOERR) {
			return (CL_ENOMEM);
		}

		if (zc_name && zc_names.getSize()) {
			// -Z flag was specified and this RG was scoped
			// with a zone cluster name as well. Report an
			// error.
			clerror("You cannot use the "
				"\"<zonecluster>:<rgname>\" "
				"form of the resource-group name with the "
				"\"-Z\" option.");
			free(rg_name);
			free(zc_name);
			return (CL_EINVAL);
		}

		// If this zone cluster name does not exist in tmp_list,
		// then we wil add it here.
		if (!tmp_list.isValue(zc_name)) {
			tmp_list.add(zc_name);
		}

		if (rg_name) {
			free(rg_name);
			rg_name = NULL;
		}
		if (zc_name) {
			free(zc_name);
			zc_name = NULL;
		}
	}
#if (SOL_VERSION >= __s10)
	// By the time we are here, we have added any zone cluster name in the
	// ResourceGroup names to tmp_list.
	if (zc_names.getSize()) {
		// Zc names were specified. We have to check whether "all"
		// was specified.
		zc_names.start();

		if (strcmp(zc_names.getValue(), ALL_CLUSTERS_STR) == 0) {
			// Get all known zone clusters.
			clerrno = get_all_clusters(valid_zc_list);

			if (clerrno != CL_NOERR) {
				return (clerrno);
			}
		} else {
			tmp_list = zc_names;
		}
	}

	// valid_zc_list will contain at least one zone cluster name
	// if "all" was specified. If it is empty, we have to check
	// the validity of the zone clusters specified by the user.
	if (valid_zc_list.getSize() < 1) {
		clerrno = get_valid_zone_clusters(tmp_list, valid_zc_list);
	}
	// Clear the memory.
	tmp_list.clear();

	if (clerrno) {
		// There was at least one invalid zone cluster.
		// We have to return.
		return (clerrno);
	}
#endif

	clerrno = get_good_nodes(nodes, good_nodes, valid_zc_list);
	if (clerrno) {
		if (first_err == 0) {
			// first error!
			first_err = clerrno;
		}
	}

	if (operands.getSize() > 0) {
		operands.start();
		oper1 = operands.getValue();
		if (!(strcmp(oper1, CLCOMMANDS_WILD_OPERAND))) {
			wildcard = 1;
		}
	} else {
		wildcard = 1;
	}

	if (wildcard) {
		if (zc_names.getSize() == 0) {
			scha_status = scswitch_getrglist(&rg_names, B_FALSE,
							NULL);
			rv = scha_status.err_code;
			if (rv != SCHA_ERR_NOERR) {
				clerror("Failed to obtain list of all resource"
					" groups.\n");
				print_rgm_error(scha_status, NULL);
				return (map_scha_error(rv));
			}
		} else {
			// If  we are here, then we have to fetch RGs from all
			// the zone clusters.
			for (valid_zc_list.start(); !valid_zc_list.end();
				valid_zc_list.next()) {
				scha_status = scswitch_getrglist(&rg_names,
						B_FALSE,
						valid_zc_list.getValue());
				rv = scha_status.err_code;
				clerrno = names_to_valuelist(rg_names,
						tmp_list);
				rgm_free_strarray(rg_names);

				if ((rv != SCHA_ERR_NOERR) ||
				    (clerrno != CL_NOERR)) {
					clerror("Failed to obtain list of "
					"all resource groups.\n");
					// Free memory.
					tmp_list.clear();
					print_rgm_error(scha_status, NULL);
					return (map_scha_error(rv));
				}
			}

			// Get all the RG names
			rg_names = vl_to_names(tmp_list, &clerrno);
			tmp_list.clear();
			if (clerrno != CL_NOERR) {
				return (clerrno);
			}
		}
	} else {
		// RG names were specified. We have to verify the validity
		// of RG names. RG names could have been specified using
		// the <ZC name>:<RG name> format, or the ZC name could
		// have been specified using the -Z flag. We have to handle
		// both the cases here.
		for (operands.start(); operands.end() != 1; operands.next()) {

			// First parse the RG name from zone cluster scope
			if (rgconf) {
				rgm_free_rg(rgconf);
				rgconf = (rgm_rg_t *)NULL;
			}
			if (rg_name) {
				free(rg_name);
				rg_name = NULL;
			}
			if (zc_name) {
				free(zc_name);
				zc_name = NULL;
			}

			scconf_err = scconf_parse_obj_name_from_cluster_scope(
				operands.getValue(), &rg_name, &zc_name);

			if (scconf_err != SCCONF_NOERR) {
				if (first_err == CL_NOERR) {
					first_err = map_scconf_error(
							scconf_err);
				}
				continue;
			}

			// If zone cluster names were not specified
			// using the -Z flag, they could have been specified
			// as part of the RG name also.
			if (zc_names.getSize() == 0) {
				// -Z flag was not used.
				scha_status = rgm_scrgadm_getrgconf(
						rg_name, &rgconf, zc_name);
				rv = scha_status.err_code;
				if (rv != SCHA_ERR_NOERR) {
					print_rgm_error(scha_status,
					    operands.getValue());
					clerror("Failed to access information "
					    "for resource group \"%s\".\n",
					    operands.getValue());
					if (first_err == CL_NOERR) {
						// first error!
						first_err = map_scha_error(rv);
					}
				} else {
					good_rgs.add(operands.getValue());
				}

				continue;
			}
#if (SOL_VERSION >= __s10)
			// If we are here, it means zone clusters were
			// specified using the -Z flag.
			tmp_list.clear();
			clerrno = get_zone_clusters_with_rg(rg_name, tmp_list,
								valid_zc_list);
			if (clerrno != CL_NOERR) {

				if (clerrno == CL_ENOENT) {
					clerror("Failed to access information "
					    "for resource group \"%s\".\n",
					    operands.getValue());
				} else {
					clcommand_perror(clerrno);
				}

				if (first_err == CL_NOERR) {
					first_err = clerrno;
				}
				continue;
			}
#endif

			// We have to scope this RG name with the
			// zone cluster name
			for (tmp_list.start(); !tmp_list.end();
				tmp_list.next()) {

				if (fullname) {
					free(fullname);
					fullname = NULL;
				}

				scconf_err =
					scconf_add_cluster_scope_to_obj_name(
						rg_name, tmp_list.getValue(),
						&fullname);
				if (scconf_err != SCCONF_NOERR) {
					clerrno = map_scconf_error(scconf_err);
					clcommand_perror(clerrno);

					if (first_err == CL_NOERR) {
						first_err = clerrno;
					}

					continue;
				}

				good_rgs.add(fullname);
			}

			if (fullname) {
				free(fullname);
				fullname = NULL;
			}
		}

		// Get all the RG names
		rg_names = vl_to_names(good_rgs, &clerrno);
		good_rgs.clear();
		tmp_list.clear();
		if (clerrno != CL_NOERR) {
			return (clerrno);
		}
	}

	// If resources were specified, we have to check whether they
	// are valid resources or not.
	for (resources.start(); !resources.end(); resources.next()) {
#if SOL_VERSION >= __s10
		// If zone clusters were specified, we have to check whether
		// this resource is valid in at least one of the zone
		// clusters.
		if (valid_zc_list.getSize() > 0) {
			clerrno = get_zone_clusters_with_rs(
					resources.getValue(), tmp_list,
					valid_zc_list);
			tmp_list.clear();
			if (clerrno != CL_NOERR) {
				// This resource was not present in any
				// of the zone clusters.
				clerror("%s: Invalid resource name\n",
					resources.getValue());
				if (first_err == CL_NOERR) {
					first_err = clerrno;
				}
			}

			continue;
		}
#endif
		// If we are here, then it means zone clusters were not
		// specified.
		if (rs) {
			rgm_free_resource(rs);
			rs = (rgm_resource_t *)NULL;
		}
		scha_status = rgm_scrgadm_getrsrcconf(resources.getValue(),
		    &rs, NULL);
		rv = scha_status.err_code;
		if (rv != SCHA_ERR_NOERR) {
			print_rgm_error(scha_status, resources.getValue());
			if (first_err == 0) {
				// first error!
				first_err = map_scha_error(rv);
			}
		}
	}

	// If resource types were specified, we have to check their validity.
	for (resourcetypes.start(); !resourcetypes.end();
	    resourcetypes.next()) {
#if SOL_VERSION >= __s10
		// If zone clusters were specified, we have to check whether
		// this RT is valid in at least one of the zone
		// clusters.
		if (valid_zc_list.getSize() > 0) {
			clerrno = get_zone_clusters_with_rt(
					resourcetypes.getValue(), tmp_list,
					valid_zc_list);
			tmp_list.clear();
			if (clerrno != CL_NOERR) {
				// This RT was not present in any
				// of the zone clusters.
				clerror("%s: Invalid resource type name\n",
					resourcetypes.getValue());
				if (first_err == CL_NOERR) {
					first_err = clerrno;
				}
			} else {
				sys_rts.add(r_rtname);
			}

			continue;
		}
#endif
		// If we are here, then no zone clusters were specified.
		if (r_rtname) {
			free(r_rtname);
			r_rtname = (char *)NULL;
		}
		scha_status = rgmcnfg_get_rtrealname(resourcetypes.getValue(),
		    &r_rtname, NULL);
		rv = scha_status.err_code;
		if (rv != SCHA_ERR_NOERR) {
			print_rgm_error(scha_status, resourcetypes.getValue());
			if (first_err == 0) {
				// first error!
				first_err = map_scha_error(rv);
			}
		} else {
			sys_rts.add(r_rtname);
		}
	}

	if (!rg_names)
		return (first_err);

	if (properties.getSize() > 0) {
		get_all_rg_props(allproplist);
		for (properties.start(); properties.end() != 1;
		    properties.next()) {
			prop_found = 0;
			for (allproplist.start(); allproplist.end() != 1;
			    allproplist.next()) {
				if (strcasecmp(allproplist.getValue(),
				    properties.getValue()) == 0) {
					prop_found = 1;
					break;
				}
			}
			if (!prop_found) {
				if (first_err == 0) {
					// first error!
					first_err = CL_EPROP;
				}
				clerror("Invalid property \"%s\" specified.\n",
				    properties.getValue());
			} else
				props_needed.add(properties.getValue());
		}

		if (props_needed.getSize() == 0)
			return (first_err);

		props_specified = 1;
	}

	if (optflgs & vflg)
		verbose = 1;

	for (i = 0; rg_names[i] != NULL; i++)
		num_rgs++;

	show_ptr = (ClShow **) calloc(num_rgs, sizeof (ClShow *));
	if (show_ptr == (ClShow **) NULL) {
		clerror("No memory.\n");
		return (CL_ENOMEM);
	}

	for (i = 0; rg_names[i] != NULL; i++) {

		if (rgconf) {
			rgm_free_rg(rgconf);
			rgconf = (rgm_rg_t *)NULL;
		}
		if (rs_names) {
			rgm_free_strarray(rs_names);
			rs_names = (char **)NULL;
		}
		if (proj_ptr2) {
			free(proj_ptr2);
			proj_ptr2 = (char *)NULL;
		}
		if (slm_type2) {
			free(slm_type2);
			slm_type2 = (char *)NULL;
		}
		if (slm_pset2) {
			free(slm_pset2);
			slm_pset2 = (char *)NULL;
		}
		if (slm_proj) {
			free(slm_proj);
			slm_proj = (char *)NULL;
		}
		if (gru_ptr2) {
			free(gru_ptr2);
			gru_ptr2 = (char *)NULL;
		}
		if (aff_str) {
			free(aff_str);
			aff_str = (char *)NULL;
		}
		if (node_str) {
			free(node_str);
			node_str = (char *)NULL;
		}
		if (rgdep_str) {
			free(rgdep_str);
			rgdep_str = (char *)NULL;
		}
		if (rg_name) {
			free(rg_name);
		}
		if (zc_name) {
			free(zc_name);
		}

		rg_name = zc_name = NULL;
		// Split the RG name and zone cluster name
		scconf_err = scconf_parse_obj_name_from_cluster_scope(
					rg_names[i], &rg_name, &zc_name);
		if (scconf_err != SCCONF_NOERR) {
			// We should not be coming in here actually.
			continue;
		}

		// Get configuration information
		scha_status = rgm_scrgadm_getrgconf(rg_name, &rgconf, zc_name);
		rv = scha_status.err_code;
		if (rv != SCHA_ERR_NOERR) {
			print_rgm_error(scha_status, NULL);
			clerror("Failed to access information for "
			    "resource group \"%s\".\n", rg_names[i]);
			if (first_err == 0) {
				// first error!
				first_err = map_scha_error(rv);
			}
			continue;
		}

		// Get the list of resources for this group
		scha_status = rgm_scrgadm_getrsrclist(rg_name, &rs_names,
							zc_name);
		rv = scha_status.err_code;
		if (rv != SCHA_ERR_NOERR) {
			print_rgm_error(scha_status, NULL);
			clerror("Failed to access information for "
			    "resource group \"%s\".\n", rg_names[i]);
			if (first_err == 0) {
				// first error!
				first_err = map_scha_error(rv);
			}
			continue;
		}

		// Get the status information
		scstat_status = scstat_get_rg(rg_names[i], &rgstat, B_TRUE);
		if (scstat_status != SCSTAT_ENOERR) {
			print_scstat_error(scstat_status, NULL);
			clerror("Failed to access information for "
			    "resource group \"%s\".\n", rg_names[i]);
			if (first_err == 0) {
				// first error!
				first_err = map_scstat_error(rv);
			}
			continue;
		}

		// Do the node filtering and status collection
		rgstatus = "Not Online";
		node_found = 0;
		for (rgnode = rgstat->scstat_rg_status_list; rgnode;
		    rgnode = rgnode->scstat_rg_status_by_node_next) {
			if (strcmp(rgnode->scstat_rg_statstr, "Online") == 0) {
				rgstatus = "Online";
				for (good_nodes.start(); !good_nodes.end();
				    good_nodes.next()) {
					if (strcmp(good_nodes.getValue(),
					    rgnode->scstat_node_name) == 0) {
						node_found = 1;
					}
				}
			}
		}

		// Do the resource and resource type filtering
		rs_found = 0;
		rt_found = 0;
		if ((rs_names != NULL) && (*rs_names != NULL)) {
			for (j = 0; rs_names[j] != NULL; j++) {
				if (rs) {
					rgm_free_resource(rs);
					rs = (rgm_resource_t *)NULL;
				}
				scha_status = rgm_scrgadm_getrsrcconf(
				    rs_names[j], &rs, zc_name);
				rv = scha_status.err_code;
				if (rv != SCHA_ERR_NOERR) {
					print_rgm_error(scha_status,
					    rs_names[j]);
					if (first_err == 0) {
						// first error!
						first_err = map_scha_error(rv);
					}
					continue;
				}
				if (resources.getSize() > 0) {
					for (resources.start();
					    !resources.end();
					    resources.next()) {
						if (strcmp(resources.getValue(),
						    rs_names[j]) == 0) {
							rs_found = 1;
							break;
						}
					}
					if (rs_found)
						break;
				}
				if (sys_rts.getSize() > 0) {
					for (sys_rts.start(); !sys_rts.end();
					    sys_rts.next()) {
						if (strcmp(sys_rts.getValue(),
						    rs->r_type) == 0) {
							rt_found = 1;
							break;
						}
					}
					if (rt_found)
						break;
				}
			}
		}

		if ((resources.getSize() > 0) ||
		    (resourcetypes.getSize() > 0) ||
		    (nodes.getSize() > 0)) {
			if ((!rs_found && (resources.getSize() > 0)) ||
			    (!rt_found && (resourcetypes.getSize() > 0)) ||
			    (!node_found && (nodes.getSize() > 0))) {
				if (wildcard) {
					continue;
				} else {
					if (first_err == 0) {
						// first error!
						first_err = CL_EINVAL;
					}
					clerror("Specified resource "
					    "group \"%s\" does not "
					    "meet the criteria set by "
					    "the \"-t\", \"-r\", or "
					    "\"-n\" options.\n",
					    rg_names[i]);
					continue;
				}
			}
		}

		// Make sure we have access to all information about this RG
		proj_ptr = (char *)NULL;
		if (rgconf->rg_project_name &&
		    *(rgconf->rg_project_name) != '\0') {
		    proj_ptr = rgconf->rg_project_name;
		} else {
			//
			// RG project name is not set; print the
			// system default project name.
			//
			scha_status = rgm_scrgadm_get_default_proj(
			    NULL, &proj_ptr2, NULL);
			if (scha_status.err_code != SCHA_ERR_NOERR) {
				print_rgm_error(scha_status, NULL);
				clerror("Failed to access information for "
				    "resource group \"%s\".\n", rg_names[i]);
				if (first_err == 0) {
					// first error!
					first_err = map_scha_error(rv);
				}
				continue;
			}
			proj_ptr = proj_ptr2;
		}

		slm_type2 = (char *)NULL;
		slm_type = (char *)NULL;
		if (rgconf->rg_slm_type && *(rgconf->rg_slm_type) != '\0') {
			slm_type = rgconf->rg_slm_type;
		} else {
			//
			// RG SLM type is not set; print the system
			// default : "manual"
			//
			scha_status = rgm_scrgadm_get_rg_slm_type(
			    rgconf->rg_name, &slm_type2, zc_name);
			if (scha_status.err_code != SCHA_ERR_NOERR) {
				print_rgm_error(scha_status, NULL);
				clerror("Failed to access information for "
				    "resource group \"%s\".\n", rg_names[i]);
				if (first_err == 0) {
					// first error!
					first_err = map_scha_error(rv);
				}
				continue;
			}
			slm_type = slm_type2;
		}

		if (strcmp(slm_type, SCHA_SLM_TYPE_AUTOMATED) == 0) {

			// SLM project name
			slm_proj = (char *)calloc(1, strlen(rgconf->rg_name)
			    + strlen("SCSLM_"));

			if (slm_proj == NULL) {
				clcommand_perror(CL_ENOMEM);
				if (first_err == 0) {
					// first error!
					first_err = CL_ENOMEM;
				}
				continue;
			}
			sprintf(slm_proj, "SCSLM_%s", rgconf->rg_name);
			slm_replace_dash_in_str(slm_proj);

			// SLM pset type
			slm_pset2 = (char *)NULL;
			slm_pset = (char *)NULL;
			if (rgconf->rg_slm_pset_type &&
			    *(rgconf->rg_slm_pset_type) != '\0') {
				slm_pset = rgconf->rg_slm_pset_type;
			} else {
				//
				// RG SLM pset type is not set; print the system
				// default : "default"
				//
				scha_status = rgm_scrgadm_get_rg_slm_pset_type(
				    rgconf->rg_name, &slm_pset2, zc_name);
				if (scha_status.err_code != SCHA_ERR_NOERR) {
					print_rgm_error(scha_status, NULL);
					clerror("Failed to access information "
					    "for resource group \"%s\".\n",
					    rg_names[i]);
					if (first_err == 0) {
						// first error!
						first_err = map_scha_error(rv);
					}
					continue;
				}
				slm_pset = slm_pset2;
			}
		}

		aff_str = names_to_str(rgconf->rg_affinities, &errflg);
		if (errflg) {
			clerror("Failed to access information for "
			    "resource group \"%s\".\n", rg_names[i]);
			if (first_err == 0) {
				// first error!
				first_err = errflg;
			}
			continue;
		}
		node_nl = nodeidlist_to_namelist(rgconf->rg_nodelist, &errflg,
						zc_name);
		if (errflg) {
			clerror("Failed to access information for "
			    "resource group \"%s\".\n", rg_names[i]);
			if (first_err == 0) {
				// first error!
				first_err = errflg;
			}
			continue;
		}
		node_str = names_to_str(node_nl, &errflg);
		if (errflg) {
			clerror("Failed to access information for "
			    "resource group \"%s\".\n", rg_names[i]);
			if (first_err == 0) {
				// first error!
				first_err = errflg;
			}
			continue;
		}
		if (node_nl)
			rgm_free_nlist(node_nl);

		rgdep_str = names_to_str(rgconf->rg_dependencies, &errflg);
		if (errflg) {
			clerror("Failed to access information for "
			    "resource group \"%s\".\n", rg_names[i]);
			if (first_err == 0) {
				// first error!
				first_err = errflg;
			}
			continue;
		}
		if (rgconf->rg_glb_rsrcused.is_ALL_value) {
			gru_ptr = "<All>";
		} else {
			gru_ptr2 = names_to_str(rgconf->rg_glb_rsrcused.names,
			    &errflg);
			if (errflg) {
				clerror("Failed to access information for "
				    "resource group \"%s\".\n", rg_names[i]);
				if (first_err == 0) {
					// first error!
					first_err = errflg;
				}
				continue;
			}
			gru_ptr = gru_ptr2;
		}

		showdata.add("rg_name", rg_names[i]);
		if ((props_specified &&
		    (props_needed.isValue(SCHA_RG_DESCRIPTION) == 1)) ||
		    (!props_specified)) {
			if (rgconf->rg_description && *rgconf->rg_description) {
				showdata.add("RG_Description",
				    rgconf->rg_description);
			} else {
				showdata.add(SCHA_RG_DESCRIPTION, "<NULL>");
			}
		}
		if ((props_specified && (props_needed.isValue(SCHA_RG_MODE) ==
		    1)) || (!props_specified)) {
			if (rgconf->rg_mode == RGMODE_FAILOVER)
				showdata.add(SCHA_RG_MODE, "Failover");
			else
				showdata.add(SCHA_RG_MODE, "Scalable");
		}
		if ((props_specified && (props_needed.isValue(SCHA_RG_STATE) ==
		    1)) || (!props_specified)) {
			if (rgconf->rg_unmanaged == B_FALSE)
				showdata.add(SCHA_RG_STATE, "Managed");
			else
				showdata.add(SCHA_RG_STATE, "Unmanaged");
		}
		if ((props_specified &&
		    (props_needed.isValue(SCHA_RG_PROJECT_NAME) == 1)) ||
		    (verbose && !props_specified)) {
			showdata.add(SCHA_RG_PROJECT_NAME, proj_ptr);
		}
		if ((props_specified &&
		    (props_needed.isValue(SCHA_RG_AFFINITIES) == 1)) ||
		    (verbose && !props_specified)) {
			showdata.add(SCHA_RG_AFFINITIES, aff_str);
		}
		if ((props_specified &&
		    (props_needed.isValue(SCHA_RG_SLM_TYPE) == 1)) ||
		    (verbose && !props_specified)) {
			showdata.add(SCHA_RG_SLM_TYPE, slm_type);
		}
		if (strcmp(slm_type, SCHA_SLM_TYPE_AUTOMATED) == 0) {
			if ((props_specified &&
			    (props_needed.isValue("RG_SLM_projectname")
			    == 1)) ||
			    (verbose && !props_specified)) {
				showdata.add("RG_SLM_projectname", slm_proj);
			}
			if ((props_specified &&
			    (props_needed.isValue(
			    SCHA_RG_SLM_PSET_TYPE) == 1)) ||
			    (verbose && !props_specified)) {
				showdata.add(SCHA_RG_SLM_PSET_TYPE, slm_pset);
			}
			if ((props_specified &&
			    (props_needed.isValue(
			    SCHA_RG_SLM_CPU_SHARES) == 1)) ||
			    (verbose && !props_specified)) {
				sprintf(vstr, "%d", rgconf->rg_slm_cpu);
				showdata.add(SCHA_RG_SLM_CPU_SHARES, vstr);
			}
			if ((props_specified &&
			    (props_needed.isValue(
			    SCHA_RG_SLM_PSET_MIN) == 1)) ||
			    (verbose && !props_specified)) {
				sprintf(vstr, "%d", rgconf->rg_slm_cpu_min);
				showdata.add(SCHA_RG_SLM_PSET_MIN, vstr);
			}
		}

		if ((props_specified &&
		    (props_needed.isValue(SCHA_RG_SLM_TYPE) == 1)) ||
		    (verbose && !props_specified)) {
			showdata.add(SCHA_RG_SLM_TYPE, slm_type);
		}
		if ((props_specified && (props_needed.isValue(
		    SCHA_RG_AUTO_START) == 1)) ||
		    (verbose && !props_specified)) {
			if (rgconf->rg_auto_start == B_FALSE)
				showdata.add(SCHA_RG_AUTO_START, "False");
			else
				showdata.add(SCHA_RG_AUTO_START, "True");
		}
		if ((props_specified && (props_needed.isValue(
		    SCHA_FAILBACK) == 1)) || (!props_specified)) {
			if (rgconf->rg_failback == B_FALSE)
				showdata.add(SCHA_FAILBACK, "False");
			else
				showdata.add(SCHA_FAILBACK, "True");
		}
		if ((props_specified && (props_needed.isValue(SCHA_NODELIST)
		    == 1)) || (!props_specified)) {
			showdata.add(SCHA_NODELIST, node_str);
		}
		if ((props_specified && (props_needed.isValue(
		    SCHA_MAXIMUM_PRIMARIES) == 1)) ||
		    (verbose && !props_specified)) {
			sprintf(vstr, "%d", rgconf->rg_max_primaries);
			showdata.add(SCHA_MAXIMUM_PRIMARIES, vstr);
		}
		if ((props_specified && (props_needed.isValue(
		    SCHA_DESIRED_PRIMARIES) == 1)) ||
		    (verbose && !props_specified)) {
			sprintf(vstr, "%d", rgconf->rg_desired_primaries);
			showdata.add(SCHA_DESIRED_PRIMARIES, vstr);
		}
		if ((props_specified &&
		    (props_needed.isValue(SCHA_RG_DEPENDENCIES) == 1)) ||
		    (verbose && !props_specified)) {
			showdata.add(SCHA_RG_DEPENDENCIES, rgdep_str);
		}
		if ((props_specified &&
		    (props_needed.isValue(SCHA_IMPL_NET_DEPEND) == 1)) ||
		    (verbose && !props_specified)) {
			if (rgconf->rg_impl_net_depend == B_FALSE)
				showdata.add(SCHA_IMPL_NET_DEPEND, "False");
			else
				showdata.add(SCHA_IMPL_NET_DEPEND, "True");
		}
		if ((props_specified && (props_needed.isValue(
		    SCHA_GLOBAL_RESOURCES_USED) == 1)) ||
		    (verbose && !props_specified)) {
			showdata.add(SCHA_GLOBAL_RESOURCES_USED, gru_ptr);
		}
		if ((props_specified && (props_needed.isValue(
		    SCHA_PINGPONG_INTERVAL) == 1)) ||
		    (verbose && !props_specified)) {
			sprintf(vstr, "%d", rgconf->rg_ppinterval);
			showdata.add(SCHA_PINGPONG_INTERVAL, vstr);
		}
		if ((props_specified &&
		    (props_needed.isValue(SCHA_PATHPREFIX) == 1)) ||
		    (verbose && !props_specified)) {
			if (rgconf->rg_pathprefix && *rgconf->rg_pathprefix) {
				showdata.add(SCHA_PATHPREFIX,
				    rgconf->rg_pathprefix);
			} else {
				showdata.add(SCHA_PATHPREFIX, "<NULL>");
			}
		}
		if ((props_specified && (props_needed.isValue(SCHA_RG_SYSTEM)
		    == 1)) || (verbose && !props_specified)) {
			if (rgconf->rg_system == B_FALSE)
				showdata.add(SCHA_RG_SYSTEM, "False");
			else
				showdata.add(SCHA_RG_SYSTEM, "True");
		}
		if ((props_specified && (props_needed.isValue(
		    SCHA_RG_SUSP_AUTO_RECOVERY) == 1)) ||
		    (verbose && !props_specified)) {
			if (rgconf->rg_suspended == B_FALSE)
				showdata.add(SCHA_RG_SUSP_AUTO_RECOVERY,
				    "False");
			else
				showdata.add(SCHA_RG_SUSP_AUTO_RECOVERY,
				    "True");
		}

		rslist.clear();
		ClShow *rs_show = new ClShow(SUBHEAD_RGRESOURCES, rg_names[i]);
		if ((rs_names != NULL) && (*rs_names != NULL)) {
			for (j = 0; rs_names[j] != NULL; j++) {
				rslist.add(rs_names[j]);
			}
		}
		//
		// We don't want the resource objects in case any -p
		// was specified as -p's are meant to be RG props.
		//
		tmp_list.add(zc_name);
		if ((rslist.getSize() > 0) && (!props_specified)) {
			(void) get_clresource_show_obj_select(rslist, optflgs,
			    empty_list, empty_list, empty_list, empty_list,
			    empty_list, rs_show, tmp_list);
			show_ptr[k] = rs_show;
		}

		tmp_list.clear();
		rslist.clear();
		k++;

		showdata.add("rg_end", rg_names[i]);
	}

	if (showdata.getSize() > 0) {
		k = 0;
		for (showdata.start(); showdata.end() != 1; showdata.next()) {
			new_entry = showdata.getName();
			if (strcmp(new_entry, "rg_name") == 0) {
				new_rg = showdata.getValue();
				rg_show->addObject(
				    (char *)gettext("Resource Group"),
				    new_rg);
			} else if (strcmp(new_entry, "rg_end") == 0) {
				if (show_ptr[k] != (void *) NULL)
					rg_show->addChild(new_rg, (ClShow *)
					    show_ptr[k]);
				k++;
			} else {
				rg_show->addProperty(new_rg,
				    showdata.getName(),
				    showdata.getValue());
			}
		}
	}

	if (rs_names)
		rgm_free_strarray(rs_names);

	if (rgconf)
		rgm_free_rg(rgconf);

	if (rg_names)
		rgm_free_strarray(rg_names);

	if (proj_ptr2)
		free(proj_ptr2);

	if (slm_proj)
		free(slm_proj);

	if (slm_type2)
		free(slm_type2);

	if (slm_pset2)
		free(slm_pset2);

	if (gru_ptr2)
		free(gru_ptr2);

	if (aff_str)
		free(aff_str);

	if (node_str)
		free(node_str);

	if (rgdep_str)
		free(rgdep_str);

	tmp_list.clear();
	valid_zc_list.clear();

	if (rg_name) {
		free(rg_name);
	}
	if (zc_name) {
		free(zc_name);
	}

	return (first_err);
}
