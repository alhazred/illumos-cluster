//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clrg_export.cc	1.13	08/09/15 SMI"

//
// Process clresourcegroup "export"
//

#include <sys/os.h>
#include <scadmin/scconf.h>
#include <scadmin/scswitch.h>
#include <scadmin/scstat.h>
#include <scadmin/scrgadm.h>
#include <rgm/rgm_scrgadm.h>
#include <rgm/rgm_cnfg.h>

#include "clcommands.h"

#include "ClCommand.h"
#include "ClresourcegroupExport.h"
#include "ClXml.h"

#include "clresource.h"
#include "clresourcegroup.h"
#include "rgm_support.h"

//
// Fetch the export object according to options/operands.
//
clerrno_t
get_clrg_export_obj_select(ValueList &operands,
    ClresourcegroupExport *rg_export, bool res_too, int &no_rgs)
{
	clerrno_t first_err = CL_NOERR;
	int rv = 0;
	int i, j;
	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};
	char **rg_names = NULL;
	char **rs_names = (char **)0;
	char **rg_nodes = NULL;
	char *oper1;
	clerrno_t errflg = CL_NOERR;
	clerrno_t clerrno = CL_NOERR;
	int wildcard = 0;
	char *rgstatus;
	rgm_rg_t *rgconf = (rgm_rg_t *)0;
	int rs_found, rt_found, node_found;
	ValueList sys_rts;
	rgm_rt_t *rt = (rgm_rt_t *)0;
	rgm_resource_t *rs = (rgm_resource_t *)0;
	char *r_rtname = NULL;
	scstat_errno_t scstat_status;
	scstat_rg_t *rgstat;
	scstat_rg_status_t *rgnode;
	char *new_rg, *new_entry;
	int props_specified = 0;
	char *proj_ptr = (char *)NULL;
	char *gru_ptr = (char *)NULL;
	char *aff_str = (char *)NULL;
	char *node_str = (char *)NULL;
	char *rgdep_str = (char *)NULL;
	char *proj_ptr2 = (char *)NULL;
	char *gru_ptr2 = (char *)NULL;
	char vstr[BUFSIZ];
	int k = 0;
	namelist_t *node_nl;
	char *slm_type = (char *)NULL;
	char *slm_type2 = (char *)NULL;
	char *slm_proj = (char *)NULL;
	char *slm_pset = (char *)NULL;
	char *slm_pset2 = (char *)NULL;

	// Initialize ORB and other cluster checks
	clerrno = cluster_init();
	if (clerrno) {
		return (clerrno);
	}

	if (operands.getSize() > 0) {
		operands.start();
		oper1 = operands.getValue();
		if (strcmp(oper1, CLCOMMANDS_WILD_OPERAND)) {
			rg_names = vl_to_names(operands, &errflg);
			if (errflg)
				return (errflg);
		} else {
			wildcard = 1;
		}
	} else {
		wildcard = 1;
	}

	if (wildcard) {
		scha_status = scswitch_getrglist(&rg_names, B_FALSE, NULL);
		rv = scha_status.err_code;
		if (rv != SCHA_ERR_NOERR) {
			clerror("Failed to obtain list of all resource "
			    "groups.\n");
			print_rgm_error(scha_status, NULL);
			return (map_scha_error(rv));
		}
	}

	if (!rg_names)
		return (CL_NOERR);

	for (i = 0; rg_names[i] != NULL; i++) {

		if (rgconf) {
			rgm_free_rg(rgconf);
			rgconf = (rgm_rg_t *)NULL;
		}
		if (rs_names) {
			rgm_free_strarray(rs_names);
			rs_names = (char **)NULL;
		}
		if (proj_ptr2) {
			free(proj_ptr2);
			proj_ptr2 = (char *)NULL;
		}
		if (gru_ptr2) {
			free(gru_ptr2);
			gru_ptr2 = (char *)NULL;
		}
		if (aff_str) {
			free(aff_str);
			aff_str = (char *)NULL;
		}
		if (node_str) {
			free(node_str);
			node_str = (char *)NULL;
		}
		if (rgdep_str) {
			free(rgdep_str);
			rgdep_str = (char *)NULL;
		}
		if (slm_type2) {
			free(slm_type2);
			slm_type2 = (char *)NULL;
		}
		if (slm_pset2) {
			free(slm_pset2);
			slm_pset2 = (char *)NULL;
		}
		if (slm_proj) {
			free(slm_proj);
			slm_proj = (char *)NULL;
		}

		// Get configuration information
		scha_status = rgm_scrgadm_getrgconf(rg_names[i], &rgconf, NULL);
		rv = scha_status.err_code;
		if (rv != SCHA_ERR_NOERR) {
			print_rgm_error(scha_status, NULL);
			clerror("Failed to access information for "
			    "resource group \"%s\".\n", rg_names[i]);
			if (first_err == 0) {
				// first error!
				first_err = map_scha_error(rv);
			}
			continue;
		}

		// Get the list of resources for this group
		scha_status = rgm_scrgadm_getrsrclist(rg_names[i],
			&rs_names, NULL);
		rv = scha_status.err_code;
		if (rv != SCHA_ERR_NOERR) {
			print_rgm_error(scha_status, NULL);
			clerror("Failed to access information for "
			    "resource group \"%s\".\n", rg_names[i]);
			if (first_err == 0) {
				// first error!
				first_err = map_scha_error(rv);
			}
			continue;
		}

		// Get the status information
		scstat_status = scstat_get_rg(rg_names[i], &rgstat, B_TRUE);
		if (scstat_status != SCSTAT_ENOERR) {
			print_scstat_error(scstat_status, NULL);
			clerror("Failed to access information for "
			    "resource group \"%s\".\n", rg_names[i]);
			if (first_err == 0) {
				// first error!
				first_err = map_scstat_error(rv);
			}
			continue;
		}

		// Make sure we have access to all information about this RG
		proj_ptr = (char *)NULL;
		if (rgconf->rg_project_name &&
		    *(rgconf->rg_project_name) != '\0') {
		    proj_ptr = rgconf->rg_project_name;
		} else {
			//
			// RG project name is not set; print the
			// system default project name.
			//
			scha_status = rgm_scrgadm_get_default_proj(
			    NULL, &proj_ptr2, NULL);
			if (scha_status.err_code != SCHA_ERR_NOERR) {
				print_rgm_error(scha_status, NULL);
				clerror("Failed to access information for "
				    "resource group \"%s\".\n", rg_names[i]);
				if (first_err == 0) {
					// first error!
					first_err = map_scha_error(rv);
				}
				continue;
			}
			proj_ptr = proj_ptr2;
		}
		aff_str = names_to_str_com(rgconf->rg_affinities, NULL,
		    &errflg);
		// if set to <NULL>, might as well not be set
		if (strcmp(aff_str, VALUE_STR_NULL) == 0) {
			aff_str = NULL;
		}
		if (errflg) {
			clerror("Failed to access information for "
			    "resource group \"%s\".\n", rg_names[i]);
			if (first_err == 0) {
				// first error!
				first_err = errflg;
			}
			continue;
		}
		node_nl = nodeidlist_to_namelist(rgconf->rg_nodelist, &errflg);
		if (errflg) {
			clerror("Failed to access information for "
			    "resource group \"%s\".\n", rg_names[i]);
			if (first_err == 0) {
				// first error!
				first_err = errflg;
			}
			continue;
		}
		node_str = names_to_str(node_nl, &errflg);
		if (errflg) {
			clerror("Failed to access information for "
			    "resource group \"%s\".\n", rg_names[i]);
			if (first_err == 0) {
				// first error!
				first_err = errflg;
			}
			continue;
		}
		if (node_nl)
			rgm_free_nlist(node_nl);

		rgdep_str = names_to_str_com(rgconf->rg_dependencies, NULL,
		    &errflg);
		// if set to <NULL>, might as well not be set
		if (strcmp(rgdep_str, VALUE_STR_NULL) == 0) {
			rgdep_str = NULL;
		}
		if (errflg) {
			clerror("Failed to access information for "
			    "resource group \"%s\".\n", rg_names[i]);
			if (first_err == 0) {
				// first error!
				first_err = errflg;
			}
			continue;
		}
		if (rgconf->rg_glb_rsrcused.is_ALL_value) {
			gru_ptr = "all";
		} else {
			gru_ptr2 = names_to_str_com(
			    rgconf->rg_glb_rsrcused.names, NULL,
			    &errflg);
			if (errflg) {
				clerror("Failed to access information for "
				    "resource group \"%s\".\n", rg_names[i]);
				if (first_err == 0) {
					// first error!
					first_err = errflg;
				}
				continue;
			}
			gru_ptr = gru_ptr2;
		}

		slm_type2 = (char *)NULL;
		slm_type = (char *)NULL;
		if (rgconf->rg_slm_type && *(rgconf->rg_slm_type) != '\0') {
			slm_type = rgconf->rg_slm_type;
		} else {
			//
			// RG SLM type is not set; print the system
			// default : "manual"
			//
			scha_status = rgm_scrgadm_get_rg_slm_type(
			    rgconf->rg_name, &slm_type2, NULL);
			if (scha_status.err_code != SCHA_ERR_NOERR) {
				print_rgm_error(scha_status, NULL);
				clerror("Failed to access information for "
				    "resource group \"%s\".\n", rg_names[i]);
				if (first_err == 0) {
					// first error!
					first_err = map_scha_error(rv);
				}
				continue;
			}
			slm_type = slm_type2;
		}

		if (strcmp(slm_type, SCHA_SLM_TYPE_AUTOMATED) == 0) {

			// SLM project name
			slm_proj = (char *)calloc(1, strlen(rgconf->rg_name)
			    + strlen("SCSLM_"));

			if (slm_proj == NULL) {
				clcommand_perror(CL_ENOMEM);
				if (first_err == 0) {
					// first error!
					first_err = CL_ENOMEM;
				}
				continue;
			}
			sprintf(slm_proj, "SCSLM_%s", rgconf->rg_name);
			slm_replace_dash_in_str(slm_proj);

			// SLM pset type
			slm_pset2 = (char *)NULL;
			slm_pset = (char *)NULL;
			if (rgconf->rg_slm_pset_type &&
			    *(rgconf->rg_slm_pset_type) != '\0') {
				slm_pset = rgconf->rg_slm_pset_type;
			} else {
				//
				// RG SLM pset type is not set; print the system
				// default : "default"
				//
				scha_status = rgm_scrgadm_get_rg_slm_pset_type(
				    rgconf->rg_name, &slm_pset2, NULL);
				if (scha_status.err_code != SCHA_ERR_NOERR) {
					print_rgm_error(scha_status, NULL);
					clerror("Failed to access information "
					    "for resource group \"%s\".\n",
					    rg_names[i]);
					if (first_err == 0) {
						// first error!
						first_err = map_scha_error(rv);
					}
					continue;
				}
				slm_pset = slm_pset2;
			}
		}

		no_rgs++;

		// Add the rg
		rg_export->addResourcegroup(rg_names[i],
		    (rgconf->rg_mode == RGMODE_FAILOVER) ? FAILOVER : SCALABLE,
		    (rgconf->rg_unmanaged) ? false : true);

		// Add the rg node list
		char *node = strtok(node_str, " ");
		while (node != NULL) {
			// Check for a node:zone name
			char *zone = NULL;
			if (strchr(node, ':')) {
				char *pch = strchr(node, ':');

				// Set the zone name
				zone = new char[strlen(node)-(pch-node+1)+1];
				for (int i = 1; i < strlen(pch); i++) {
					zone[i-1] = pch[i];
					zone[i] = '\0';
				}

				// Truncate the node name
				node[pch-node] = '\0';
			}
			rg_export->addResourcegroupNode(rg_names[i],
			    node, zone);

			zone = NULL;
			delete zone;

			node = strtok(NULL, " ");
		}

		// Properties...
		if (rgconf->rg_description && *rgconf->rg_description) {
			rg_export->addResourcegroupProperty(rg_names[i],
			    "RG_Description", rgconf->rg_description);
		}

		rg_export->addResourcegroupProperty(rg_names[i],
		    SCHA_RG_PROJECT_NAME, proj_ptr);

		rg_export->addResourcegroupProperty(rg_names[i],
		    SCHA_RG_AFFINITIES, aff_str);

		rg_export->addResourcegroupProperty(rg_names[i],
		    SCHA_RG_AUTO_START,
		    (rgconf->rg_auto_start == B_TRUE) ?
		    "true" : "false");

		rg_export->addResourcegroupProperty(rg_names[i],
		    SCHA_FAILBACK,
		    (rgconf->rg_failback == B_TRUE) ?
		    "true" : "false");

		(void) sprintf(vstr, "%d", rgconf->rg_max_primaries);
		rg_export->addResourcegroupProperty(rg_names[i],
		    SCHA_MAXIMUM_PRIMARIES, vstr);

		(void) sprintf(vstr, "%d", rgconf->rg_desired_primaries);
		rg_export->addResourcegroupProperty(rg_names[i],
		    SCHA_DESIRED_PRIMARIES, vstr);

		rg_export->addResourcegroupProperty(rg_names[i],
		    SCHA_RG_DEPENDENCIES, rgdep_str);

		rg_export->addResourcegroupProperty(rg_names[i],
		    SCHA_IMPL_NET_DEPEND,
		    (rgconf->rg_impl_net_depend == B_TRUE) ?
		    "true" : "false");

		rg_export->addResourcegroupProperty(rg_names[i],
		    SCHA_GLOBAL_RESOURCES_USED, gru_ptr);

		(void) sprintf(vstr, "%d", rgconf->rg_ppinterval);
		rg_export->addResourcegroupProperty(rg_names[i],
		    SCHA_PINGPONG_INTERVAL, vstr);

		if (rgconf->rg_pathprefix && *rgconf->rg_pathprefix) {
			rg_export->addResourcegroupProperty(rg_names[i],
			    SCHA_PATHPREFIX, rgconf->rg_pathprefix);
		} else {
			rg_export->addResourcegroupProperty(rg_names[i],
			    SCHA_PATHPREFIX, (char *)NULL);
		}

		rg_export->addResourcegroupProperty(rg_names[i],
		    SCHA_RG_SYSTEM,
		    (rgconf->rg_system == B_TRUE) ?
		    "true" : "false");

		rg_export->addResourcegroupProperty(rg_names[i],
		    SCHA_RG_SUSP_AUTO_RECOVERY,
		    (rgconf->rg_suspended == B_TRUE) ?
		    "true" : "false");

		rg_export->addResourcegroupProperty(rg_names[i],
		    SCHA_RG_SLM_TYPE, slm_type);

		if (strcmp(slm_type, SCHA_SLM_TYPE_AUTOMATED) == 0) {
			rg_export->addResourcegroupProperty(rg_names[i],
			    "RG_SLM_projectname", slm_proj);
			rg_export->addResourcegroupProperty(rg_names[i],
			    SCHA_RG_SLM_PSET_TYPE, slm_pset);
			(void) sprintf(vstr, "%d", rgconf->rg_slm_cpu);
			rg_export->addResourcegroupProperty(rg_names[i],
			    SCHA_RG_SLM_CPU_SHARES, vstr);
			(void) sprintf(vstr, "%d", rgconf->rg_slm_cpu_min);
			rg_export->addResourcegroupProperty(rg_names[i],
			    SCHA_RG_SLM_PSET_MIN, vstr);
		}

		// Add the resources
		if (res_too && (rs_names != NULL) && (*rs_names != NULL)) {
			for (j = 0; rs_names[j] != NULL; j++) {
				rg_export->addResourcegroupResource(
				    rg_names[i], rs_names[j]);
			}
		}
	}

	if (rgconf)
		rgm_free_rg(rgconf);

	if (rg_names)
		rgm_free_strarray(rg_names);

	if (rs_names)
		rgm_free_strarray(rs_names);

	if (proj_ptr2)
		free(proj_ptr2);

	if (gru_ptr2)
		free(gru_ptr2);

	if (aff_str)
		free(aff_str);

	if (node_str)
		free(node_str);

	if (rgdep_str)
		free(rgdep_str);

	if (slm_proj)
		free(slm_proj);

	if (slm_type2)
		free(slm_type2);

	if (slm_pset2)
		free(slm_pset2);

	return (first_err);
}

//
// This function returns a ClresourcegroupExport object populated with
// all of the resourcegroup information from the cluster.  Caller is
// responsible for releasing the ClcmdExport object.
//
//	include_resources:	Determines whether or not this
//				function is to include the resource
//				information or not.
//
ClcmdExport *
get_clresourcegroup_xml_elements(bool include_resources)
{
	ValueList empty;
	int no_rgs = 0;
	ClresourcegroupExport *clrg_exp = new ClresourcegroupExport();

	(void) get_clrg_export_obj_select(empty, clrg_exp,
	    include_resources, no_rgs);

	// Check for no rg's.  If the object is NULL, then just realloc
	// a new one so it will be empty.
	if (!clrg_exp)
		clrg_exp = new ClresourcegroupExport();

	return (clrg_exp);
}

//
// clrg "export"
//
int
clrg_export(ClCommand &cmd, ValueList &operands, char *clconfiguration)
{
	int first_err = 0;
	int err = 0;
	int no_rgs = 0;

	ClresourcegroupExport *rg_export = new ClresourcegroupExport();
	err = get_clrg_export_obj_select(operands, rg_export, false, no_rgs);
	if (err)
		first_err = err;

	// No rg's met the criteria
	if (!no_rgs) {
		return (first_err);
	}

	ClXml xml;
	err = xml.createExportFile(CLRESOURCEGROUP_CMD,
	    clconfiguration, rg_export);

	if (err && !first_err)
		first_err = err;

	return (first_err);
}
