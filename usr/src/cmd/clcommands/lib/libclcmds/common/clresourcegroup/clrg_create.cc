//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clrg_create.cc	1.13	08/11/14 SMI"

//
// Process clrg "create"
//

#include "clcommands.h"
#include "common.h"

#include <sys/os.h>

#include "ClCommand.h"
#include "ClXml.h"

#include <scadmin/scrgadm.h>
#include <scadmin/scconf.h>
#include <scadmin/scstat.h>

#include "rgm_support.h"

//
// clrg "create"
// This method is aware about zone clusters. This method
// will make checks of valid zone clusters specified in
// "operands". This method expects that zone cluster names
// be specified in the format <zonecluster name>:<rg name>
//
clerrno_t
clrg_create(ClCommand &cmd, ValueList &operands, optflgs_t optflgs,
    ValueList &nodes, NameValueList &properties)
{
	clerrno_t first_err = CL_NOERR;
	int rv = 0;
	int len = 0;
	int first_val = 1;
	char *nodestr = NULL;
	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};
	scha_property_t *proplist = NULL;
	char scalbuf1[40];
	char scalbuf2[40];
	int num_nodes = 0;
	boolean_t bypass_install_mode = B_FALSE; /* private for upgrade */
	clerrno_t errflg = CL_NOERR;
	clerrno_t clerrno = CL_NOERR;
	ValueList good_nodes, tmp_list;
	ValueList allproplist;
	int prop_found = 0;
	rgm_rg_t *rg = (rgm_rg_t *)NULL;
	char *zc_name, *rg_name;
	boolean_t is_same = B_FALSE;
	scconf_errno_t scconf_err = SCCONF_NOERR;

	rg_name = zc_name = NULL;
	// Check whether any zone cluster names were specified.
	// And if they were specified, there should be only zone cluster
	// across all the RGs.
	clerrno = check_single_zc_specified(operands, &zc_name, &is_same);
	if (clerrno != CL_NOERR) {
		clcommand_perror(clerrno);
		return (clerrno);
	}
	// If multiple zone clusters were specified, report an error.
	if (is_same == B_FALSE) {
		clerror("This operation can be performed on only one "
		    "zone cluster at a time.\n");
		return (clerrno);
	}

#if (SOL_VERSION >= __s10)
	if (zc_name) {
		// If zone cluster name was specified, we have to ensure
		// that it is valid.
		// We have to check whether this operation is allowed for
		// the status of the zone cluster.
		clerrno = check_zc_operation(CL_OBJ_TYPE_RG, CL_OP_TYPE_CREATE,
						zc_name);
		if (clerrno != CL_NOERR) {
			return (clerrno);
		}
	}
#endif

	//
	// If convenience option is used, nodelist property should not be used,
	// also, the RG_mode property can not be specified if -S is also
	// specified.
	//
	for (properties.start(); properties.end() != 1; properties.next()) {
		if ((strcasecmp(properties.getName(), SCHA_NODELIST) == 0) &&
		    (nodes.getSize() > 0)) {
			clerror("You cannot specify the \"-n\" option "
			    "and the \"%s\" property at the same "
			    "time.\n", SCHA_NODELIST);
			return (CL_EINVAL);
		}
		if ((strcasecmp(properties.getName(), SCHA_RG_MODE) == 0) &&
		    (optflgs & Sflg)) {
			clerror("You cannot specify the \"-S\" option "
			    "and the \"%s\" property at the same "
			    "time.\n", SCHA_RG_MODE);
			return (CL_EINVAL);
		}
	}

	tmp_list.add(zc_name);
	clerrno = get_good_nodes(nodes, good_nodes, tmp_list);
	tmp_list.clear();
	if (clerrno)
		return (clerrno);

	// Check for server/farm mix
	clerrno = check_server_farm_mix(good_nodes, NULL);
	if (clerrno)
		return (clerrno);

	get_all_rg_props(allproplist);
	for (properties.start(); properties.end() != 1; properties.next()) {
		prop_found = 0;
		for (allproplist.start(); allproplist.end() != 1;
		    allproplist.next()) {
			if (strcasecmp(allproplist.getValue(),
			    properties.getName()) == 0) {
				prop_found = 1;
				break;
			}
		}
		if (!prop_found) {
			clerror("Invalid property \"%s\" specified.\n",
			    properties.getName());
			return (CL_EPROP);
		}
	}

	// Create the nodelist property
	for (good_nodes.start(); !good_nodes.end(); good_nodes.next()) {
		len += (strlen(good_nodes.getValue()) + 1);
	}
	if (len) {
		nodestr = (char *)calloc(1, len + 20);
		strcat(nodestr, "Nodelist=");
		for (good_nodes.start(); !good_nodes.end(); good_nodes.next()) {
			if (first_val) {
				strcat(nodestr, good_nodes.getValue());
			} else {
				strcat(nodestr, ",");
				strcat(nodestr, good_nodes.getValue());
			}
			first_val = 0;
		}
		properties.add(0, nodestr);
	}

	// Take care of the -S flag
	if (optflgs & Sflg) {
		properties.add(0, "RG_Mode=Scalable");
		num_nodes = good_nodes.getSize();
		if (num_nodes == 0) {
			// No explicit nodelist, get the cluster nodecount
			num_nodes = get_cluster_nodecount(&errflg);
			if (errflg) {
				return (errflg);
			}
		}
		sprintf(scalbuf1, "Maximum_primaries=%d", num_nodes);
		sprintf(scalbuf2, "Desired_primaries=%d", num_nodes);
		properties.add(0, scalbuf1);
		properties.add(0, scalbuf2);
	}

	// Take care of the undocumented force flag
	if (optflgs & Fflg)
		bypass_install_mode = B_TRUE;

	proplist = nvl_to_prop_array(properties, NULL, &errflg, CL_TYPE_NONE,
	    NULL, zc_name);
	if (errflg) {
		return (errflg);
	}

	for (operands.start(); !operands.end(); operands.next()) {
		if (rg_name) {
			free(rg_name);
			rg_name = NULL;
		}
		if (zc_name) {
			free(zc_name);
			zc_name = NULL;
		}
		// First get the RG name and the ZC name
		scconf_err = scconf_parse_obj_name_from_cluster_scope(
				operands.getValue(), &rg_name, &zc_name);
		if (scconf_err != SCCONF_NOERR) {
			clerrno = map_scconf_error(scconf_err);
			clcommand_perror(clerrno);

			if (first_err == CL_NOERR) {
				first_err = clerrno;
			}
			continue;
		}
		// Check that the rg doesn't exist already
		scha_status = rgm_scrgadm_getrgconf(rg_name, &rg, zc_name);
		rgm_free_rg(rg);
		if (scha_status.err_code == SCHA_ERR_NOERR) {
			clerror("Failed to create \"%s\", the resource group "
			    "already exists.\n", operands.getValue());
			if (first_err == 0) {
				// first error!
				first_err = CL_EEXIST;
			}
			continue;
		}

		scha_status = rgm_scrgadm_create_rg(rg_name,
		    &proplist, bypass_install_mode, zc_name);
		rv = filter_warning(scha_status.err_code);
		if (rv) {
			print_rgm_error(scha_status, operands.getValue());
			if (first_err == 0) {
				// first error!
				first_err = map_scha_error(rv);
			}
		} else {
			if (scha_status.err_msg) {
				fprintf(stdout, "%s\n", scha_status.err_msg);
			}
			if (optflgs & vflg) {
				clmessage("Resource group \"%s\" created.\n",
				    operands.getValue());
			}
		}
	}

	if (nodestr)
		free(nodestr);
	free_prop_array(proplist);
	return (first_err);
}

clerrno_t
clrg_create(ClCommand &cmd, ValueList &operands, optflgs_t optflgs,
    char *clconfiguration, ValueList &nodes, NameValueList &properties)
{
	clerrno_t clerrno = CL_NOERR;
	clerrno_t first_err = CL_NOERR;
	ClXml xml;
	ValueList allproplist;
	ValueList created_rgs;
	ValueList good_nodes;
	ValueList *expanded_list = new ValueList();
	ValueList rg_list;
	int prop_found = 0;
	char *oper1 = (char *)NULL;
	boolean_t rg_found;

	if (!clconfiguration) {
		clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}

	//
	// If convenience option is used, nodelist property should not be used,
	// Also check for RG_mode property and valid properties
	//
	get_all_rg_props(allproplist);
	for (properties.start(); properties.end() != 1; properties.next()) {
		prop_found = 0;
		for (allproplist.start(); allproplist.end() != 1;
		    allproplist.next()) {
			if (strcasecmp(allproplist.getValue(),
			    properties.getName()) == 0) {
				prop_found = 1;
				break;
			}
		}
		if (!prop_found) {
			clerror("Invalid property \"%s\" specified.\n",
			    properties.getName());
			return (CL_EPROP);
		}
		if (strcasecmp(properties.getName(), SCHA_RG_MODE) == 0) {
			clerror("You cannot specify the \"-i\" option "
			    "and the \"%s\" property at the same "
			    "time.\n", SCHA_RG_MODE);
			return (CL_EINVAL);
		}
		if (strcasecmp(properties.getName(), SCHA_RG_STATE) == 0) {
			clerror("You cannot specify the \"-i\" option "
			    "and the \"%s\" property at the same "
			    "time.\n", SCHA_RG_STATE);
			return (CL_EINVAL);
		}
		if (strcasecmp(properties.getName(), SCHA_NODELIST) == 0) {
			if (nodes.getSize() > 0) {
				clerror("You cannot specify the \"-n\" option "
				    "and the \"%s\" property at the same "
				    "time.\n", SCHA_NODELIST);
				return (CL_EINVAL);
			} else {
				nodes.add(properties.getValue());
			}
		}
	}

	clerrno = get_good_nodes(nodes, good_nodes);
	if (clerrno)
		return (clerrno);

	OptionValues *ov = new OptionValues((char *)"add", cmd.argc, cmd.argv);

	// Nodelist
	ov->addOptions(CLRG_NODE, &good_nodes);

	// Properties
	ov->addNVOptions(CLRG_PROP, &properties);

	// verbose?
	ov->optflgs = 0;
	if (optflgs & vflg) {
		ov->optflgs |= vflg;
	}

	if (operands.getSize() == 0)
		operands.add((char *)CLCOMMANDS_WILD_OPERAND);

	clerrno = xml.getAllObjectNames(CLRESOURCEGROUP_CMD, clconfiguration,
	    expanded_list);
	if (clerrno != CL_NOERR) {
		return (clerrno);
	}
	if (expanded_list->getSize() == 0) {
		clerror("Cannot find any resource group in \"%s\".\n",
		    clconfiguration);
		return (CL_EINVAL);
	}

	operands.start();
	oper1 = operands.getValue();
	if (strcmp(oper1, (char *)CLCOMMANDS_WILD_OPERAND) == 0) {
		for (expanded_list->start(); expanded_list->end() != 1;
		    expanded_list->next()) {
			rg_list.add(expanded_list->getValue());
		}
	} else {
		for (operands.start(); operands.end() != 1; operands.next()) {
			rg_found = B_FALSE;
			for (expanded_list->start(); expanded_list->end() != 1;
			    expanded_list->next()) {
				if (strcmp(expanded_list->getValue(),
				    operands.getValue()) == 0) {
					rg_found = B_TRUE;
					break;
				}
			}
			if (rg_found) {
				rg_list.add(operands.getValue());
			} else {
				clerror("Cannot find \"%s\" in \"%s\".\n",
				    operands.getValue(), clconfiguration);
				if (first_err == 0) {
					// first error!
					first_err = CL_ENOENT;
				}
			}
		}
	}

	for (rg_list.start(); !rg_list.end(); rg_list.next()) {
		ov->setOperand(rg_list.getValue());
		xml.reset_error();
		clerrno = xml.applyConfig(CLRESOURCEGROUP_CMD, clconfiguration,
		    ov);
		if (clerrno) {
			if (first_err == 0) {
				// first error!
				first_err = clerrno;
			}
		} else {
			created_rgs.add(rg_list.getValue());
		}
	}

	for (created_rgs.start(); !created_rgs.end(); created_rgs.next()) {
		clerrno = xml.RGPostProcessor(created_rgs.getValue(),
		    properties, optflgs);
		if (clerrno) {
			if (first_err == 0) {
				// first error!
				first_err = clerrno;
			}
		}
	}

cleanup:
	delete ov;
	delete expanded_list;

	return (first_err);
}
