//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clrg_remove_node.cc	1.13	08/07/25 SMI"

//
// Process clrg "remove-node"
//

#include <sys/os.h>
#include <scadmin/scconf.h>
#include <scadmin/scswitch.h>
#include <scadmin/scstat.h>
#include <rgm/rgm_scrgadm.h>

#include "clcommands.h"

#include "ClCommand.h"

#include "rgm_support.h"
#include "common.h"

//
// clrg "remove-node"
//
clerrno_t
clrg_remove_node(ClCommand &cmd, ValueList &operands, optflgs_t optflgs,
    ValueList &nodes, uint_t flg)
{
	clerrno_t first_err = CL_NOERR;
	int rv = 0;
	int i;
	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};
	char **rg_names = NULL;
	char *oper1;
	rgm_rg_t *rg = (rgm_rg_t *)0;
	namelist_t *new_nodelist;
	namelist_t *nl1;
	nodeidlist_t *nl2;
	scha_property_t nodeprop[4];
	namelist_t desired_nl;
	namelist_t maximum_nl;
	char str_nodes[10];
	int num_nodes;
	int num_old_nodes;
	clerrno_t errflg = CL_NOERR;
	clerrno_t clerrno = CL_NOERR;
	int prop_index;
	ValueList good_nodes, tmp_list;
	namelist_t *node_nl;
	boolean_t verbose = B_FALSE;
	scconf_errno_t scconf_err = SCCONF_NOERR;
	boolean_t is_same = B_FALSE;
	char *rg_part, *zc_part, *zc_name;

	// Initializa ORB and other cluster checks
	clerrno = cluster_init();
	if (clerrno)
		return (clerrno);

	if (optflgs & vflg)
		verbose = B_TRUE;

	// Check whether any zone cluster names were specified.
	// And if they were specified, there should be only zone cluster
	// across all the RGs.
	rg_part = zc_part = zc_name = NULL;
	clerrno = check_single_zc_specified(operands, &zc_name, &is_same);
	if (clerrno != CL_NOERR) {
		clcommand_perror(clerrno);
		return (clerrno);
	}
	// If multiple zone clusters were specified, report an error.
	if (is_same == B_FALSE) {
		clerror("This operation can be performed on only one "
		    "zone cluster at a time.\n");
		return (clerrno);
	}
#if (SOL_VERSION >= __s10)
	if (zc_name) {
		// If zone cluster name was specified, we have to ensure
		// that it is valid.
		// We have to check whether this operation is allowed for
		// the status of the zone cluster.
		clerrno = check_zc_operation(CL_OBJ_TYPE_RG, CL_OP_TYPE_CREATE,
						zc_name);
		if (clerrno != CL_NOERR) {
			return (clerrno);
		}
	}
#endif

	tmp_list.add(zc_name);
	clerrno = get_good_nodes(nodes, good_nodes, tmp_list);
	tmp_list.clear();
	if (clerrno)
		return (clerrno);

	operands.start();
	oper1 = operands.getValue();
	if (strcmp(oper1, CLCOMMANDS_WILD_OPERAND)) {
		rg_names = vl_to_names(operands, &errflg);
		if (errflg)
			return (errflg);

	} else {
		clerrno = z_getrglist(&rg_names, verbose, optflgs & uflg,
					zc_name);
		if (clerrno != CL_NOERR) {
			return (clerrno);
		}
	}

	if (rg_names) {
		for (i = 0; rg_names[i] != NULL; i++) {

			memset(nodeprop, 0, 4 * (sizeof (scha_property_t)));

			if (rg != (rgm_rg_t *)0) {
				rgm_free_rg(rg);
				rg = (rgm_rg_t *)0;
			}
			// First parse the RG name and the cluster name.
			if (rg_part) {
				free(rg_part);
				rg_part = NULL;
			}
			if (zc_part) {
				free(zc_part);
				zc_part = NULL;
			}
			// Split the RG name and zone cluster name
			scconf_err = scconf_parse_obj_name_from_cluster_scope(
					rg_names[i], &rg_part, &zc_part);
			if (scconf_err != SCCONF_NOERR) {
				// We should not be coming in here actually.
				clerrno = map_scconf_error(scconf_err);
				clcommand_perror(clerrno);
				if (first_err == CL_NOERR) {
					first_err = clerrno;
				}
				continue;
			}

			// Get the RG details
			scha_status = rgm_scrgadm_getrgconf(rg_part, &rg,
								zc_part);
			rv = scha_status.err_code;
			if (rv != SCHA_ERR_NOERR) {
				print_rgm_error(scha_status, rg_names[i]);
				clerror("Failed to obtain resource group "
				    "information, skipping \"%s\".\n",
				    rg_names[i]);
				if (first_err == 0) {
					// first error!
					first_err = map_scha_error(rv);
				}
				rgm_free_rg(rg);
				continue;
			}
			if (rg == NULL) {
				clerror("Failed to obtain resource group "
				    "information, skipping \"%s\".\n",
				    rg_names[i]);
				if (first_err == 0) {
					// first error!
					first_err = CL_ENOENT;
				}
				rgm_free_rg(rg);
				continue;
			}

			// convert nodeidlist to namelist
			node_nl = nodeidlist_to_namelist(rg->rg_nodelist,
			    &errflg, zc_part);
			if (errflg) {
				rgm_free_rg(rg);
				return (errflg);
			}

			// Remove nodes from the nodelist
			new_nodelist = remove_vl_from_names(node_nl,
			    good_nodes, &errflg);
			rgm_free_nlist(node_nl);

			if (errflg) {
				rgm_free_rg(rg);
				return (errflg);
			}

			// Figure out the number of nodes in the nodelists
			nl1 = new_nodelist;
			num_nodes = 0;
			while (nl1) {
				num_nodes++;
				nl1 = nl1->nl_next;
			}
			nl2 = rg->rg_nodelist;
			num_old_nodes = 0;
			while (nl2) {
				num_old_nodes++;
				nl2 = nl2->nl_next;
			}

			if (num_nodes == 0) {
				if (!flg) {
					clerror("Nodelist can not be empty.\n");
					clerror("Operation not allowed, "
					    "skipping resource group \"%s\".\n",
					    rg_names[i]);
					if (first_err == 0) {
						// first error!
						first_err = CL_EOP;
					}
				}
				rgm_free_rg(rg);
				rgm_free_nlist(new_nodelist);
				continue;
			}
			if (num_nodes !=
			    (num_old_nodes - good_nodes.getSize())) {
				if (!flg) {
					clerror("One or more of the specified "
					    "nodes are not part of resource "
					    "group \"%s\", "
					    "skipping \"%s\".\n", rg_names[i],
					    rg_names[i]);
					if (first_err == 0) {
						// first error!
						first_err = CL_EOP;
					}
				}
				rgm_free_rg(rg);
				rgm_free_nlist(new_nodelist);
				continue;
			}
			sprintf(str_nodes, "%d", num_nodes);
			maximum_nl.nl_name = str_nodes;
			maximum_nl.nl_next = NULL;
			desired_nl.nl_name = str_nodes;
			desired_nl.nl_next = NULL;

			//
			// Set the new nodelist and associated properties.
			// If the node number is falling below max or
			// desired primaries, we don't want to fail the
			// operation, i.e., the add-node -S behavior is
			// implicit here.
			//
			prop_index = 0;
			nodeprop[prop_index].sp_name = "Nodelist";
			nodeprop[prop_index++].sp_value = new_nodelist;
			if (num_nodes < rg->rg_max_primaries) {
				nodeprop[prop_index].sp_name =
				    "Maximum_primaries";
				nodeprop[prop_index++].sp_value = &maximum_nl;
			}
			if (num_nodes < rg->rg_desired_primaries) {
				nodeprop[prop_index].sp_name =
				    "Desired_primaries";
				nodeprop[prop_index++].sp_value = &desired_nl;
			}
			nodeprop[prop_index].sp_name = NULL;
			scha_status = modify_rg(rg_names[i], nodeprop,
			    FROM_SCRGADM, (const char **) rg_names);
			rv = filter_warning(scha_status.err_code);
			if (rv) {
				print_rgm_error(scha_status, rg_names[i]);
				clerror("Failed to remove nodes from resource "
				    "group \"%s\"\n", rg_names[i]);
				if (first_err == 0) {
					// first error!
					first_err = map_scha_error(rv);
				}
			} else {
				if (scha_status.err_msg) {
					fprintf(stdout, "%s\n",
					    scha_status.err_msg);
				}
				if (optflgs & vflg) {
					nl1 = new_nodelist;
					clmessage("New nodelist set for "
					    "resource group \"%s\": ",
					    rg_names[i]);
					while (nl1) {
						fprintf(stdout, "%s ",
						    nl1->nl_name);
						nl1 = nl1->nl_next;
					}
					fprintf(stdout, "\n");
				}
			}
			rgm_free_nlist(new_nodelist);
			rgm_free_rg(rg);
		}
	}

	rgm_free_strarray(rg_names);

	if (zc_name)
		free(zc_name);
	if (rg_part)
		free(rg_part);
	if (zc_part)
		free(zc_part);

	return (first_err);
}
