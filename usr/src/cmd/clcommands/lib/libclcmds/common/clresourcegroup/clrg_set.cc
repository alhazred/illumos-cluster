//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clrg_set.cc	1.14	08/11/14 SMI"

//
// Process clrg "set"
//

#include <sys/os.h>
#include <scadmin/scconf.h>
#include <scadmin/scswitch.h>
#include <scadmin/scstat.h>
#include <rgm/rgm_scrgadm.h>

#include "clcommands.h"

#include "ClCommand.h"

#include "rgm_support.h"
#include "common.h"

//
// clrg "set"
//
clerrno_t
clrg_set(ClCommand &cmd, ValueList &operands, optflgs_t optflgs,
    NameValueList &properties, ValueList &nodes)
{
	int errcount = 0;
	clerrno_t first_err = CL_NOERR;
	int i;
	int rv = 0;
	int len = 0;
	int first_val = 1;
	char *nodestr = NULL;
	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};
	scha_property_t *proplist = NULL;
	int num_nodes;
	char **rg_names = NULL;
	char *oper1;
	clerrno_t errflg = CL_NOERR;
	clerrno_t clerrno = CL_NOERR;
	ValueList good_nodes, tmp_list;
	ValueList allproplist;
	int prop_found = 0;
	char tmp_prop[MAXCCRTBLNMLEN];
	int tmp_len1, tmp_len2;
	boolean_t verbose = B_FALSE;
	char *zc_name, *full_name;
	boolean_t is_same = B_FALSE;
	scconf_errno_t scconf_err = SCCONF_NOERR;

	// Initializa ORB and other cluster checks
	clerrno = cluster_init();
	if (clerrno)
		return (clerrno);

	if (optflgs & vflg)
		verbose = B_TRUE;

	zc_name = full_name = NULL;
	// Check whether any zone cluster names were specified.
	// And if they were specified, there should be only zone cluster
	// across all the RGs.
	clerrno = check_single_zc_specified(operands, &zc_name, &is_same);
	if (clerrno != CL_NOERR) {
		clcommand_perror(clerrno);
		return (clerrno);
	}
	// If multiple zone clusters were specified, report an error.
	if (is_same == B_FALSE) {
		clerror("This operation can be performed on only one "
		    "zone cluster at a time.\n");
		return (clerrno);
	}

#if (SOL_VERSION >= __s10)
	if (zc_name) {
		// If zone cluster name was specified, we have to ensure
		// that it is valid.
		// We have to check whether this operation is allowed for
		// the status of the zone cluster.
		clerrno = check_zc_operation(CL_OBJ_TYPE_RG, CL_OP_TYPE_CREATE,
						zc_name);
		if (clerrno != CL_NOERR) {
			return (clerrno);
		}
	}
#endif

	// If convenience option is used, nodelist property should not be used
	if (nodes.getSize() > 0) {
		for (properties.start(); properties.end() != 1;
		    properties.next()) {
			if (strcasecmp(properties.getName(), SCHA_NODELIST)
			    == 0) {
				clerror("You cannot specify the \"-n\" option "
				    "and the \"%s\" property at the same "
				    "time.\n", SCHA_NODELIST);
				return (CL_EINVAL);
			}
		}
	}

	tmp_list.add(zc_name);
	clerrno = get_good_nodes(nodes, good_nodes, tmp_list);
	tmp_list.clear();
	if (clerrno)
		return (clerrno);

	// Check for server/farm mix
	clerrno = check_server_farm_mix(good_nodes, NULL);
	if (clerrno)
		return (clerrno);

	get_all_rg_props(allproplist);
	for (properties.start(); properties.end() != 1; properties.next()) {
		prop_found = 0;
		for (allproplist.start(); allproplist.end() != 1;
		    allproplist.next()) {
			if (strcasecmp(allproplist.getValue(),
			    properties.getName()) == 0) {
				prop_found = 1;
				break;
			}
			strcpy(tmp_prop, properties.getName());
			tmp_len1 = strlen(allproplist.getValue());
			tmp_len2 = strlen(properties.getName());
			if ((strncasecmp(allproplist.getValue(),
			    properties.getName(), tmp_len1) == 0) &&
			    (tmp_len1 + 1 == tmp_len2) &&
			    ((tmp_prop[tmp_len2 - 1] == '+') ||
			    (tmp_prop[tmp_len2 - 1] == '-'))) {
				prop_found = 1;
				break;
			}
		}
		if (!prop_found) {
			clerror("Invalid property \"%s\" specified.\n",
			    properties.getName());
			return (CL_EPROP);
		}
	}

	operands.start();
	oper1 = operands.getValue();
	if (strcmp(oper1, CLCOMMANDS_WILD_OPERAND)) {
		rg_names = vl_to_names(operands, &errflg);
		if (errflg)
			return (errflg);
			// Remove the zone cluster context from all these
			// names.
			clerrno = remove_zc_names_from_names(rg_names);
			if (clerrno != CL_NOERR) {
				clcommand_perror(clerrno);
				if (first_err == CL_NOERR) {
					first_err = clerrno;
				}
				return (clerrno);
			}

	} else {
		clerrno = z_getrglist(&rg_names, verbose, optflgs & uflg,
					zc_name);
		if (clerrno != CL_NOERR) {
			return (clerrno);
		}
		// Remove the zone cluster context from all these names
		clerrno = remove_zc_names_from_names(rg_names);
		if (clerrno != CL_NOERR) {
			clcommand_perror(clerrno);
			if (first_err == CL_NOERR) {
				first_err = clerrno;
			}
			return (clerrno);
		}
	}

	// By now, we have all the RG names without the zone cluster scoping.

	// Create the nodelist property
	for (good_nodes.start(); !good_nodes.end(); good_nodes.next()) {
		len += (strlen(good_nodes.getValue()) + 1);
	}
	if (len) {
		nodestr = (char *)calloc(1, len + 20);
		strcat(nodestr, "Nodelist=");
		for (good_nodes.start(); !good_nodes.end(); good_nodes.next()) {
			if (first_val) {
				strcat(nodestr, good_nodes.getValue());
			} else {
				strcat(nodestr, ",");
				strcat(nodestr, good_nodes.getValue());
			}
			first_val = 0;
		}
		properties.add(0, nodestr);
	}

	if (rg_names) {
		for (i = 0; rg_names[i] != NULL; i++) {
			if (full_name) {
				free(full_name);
				full_name = NULL;
			}

			scconf_err = scconf_add_cluster_scope_to_obj_name(
					rg_names[i], zc_name, &full_name);
			if (scconf_err != SCCONF_NOERR) {
				clerrno = map_scconf_error(scconf_err);
				clcommand_perror(clerrno);

				if (first_err == CL_NOERR) {
					first_err = clerrno;
				}
				continue;
			}

			proplist = nvl_to_prop_array(properties, full_name,
			    &errflg, CL_TYPE_RG, NULL, zc_name);
			if (errflg) {
				if ((errflg == CL_ENOMEM) ||
				    (errflg == CL_EINVAL)) {
					return (errflg);
				} else {
					if (first_err == 0) {
						// first error!
						first_err = errflg;
					}
					continue;
				}
			}

			scha_status = modify_rg(full_name, proplist,
			    FROM_SCRGADM, (const char **)rg_names);
			rv = filter_warning(scha_status.err_code);
			if (rv) {
				print_rgm_error(scha_status, rg_names[i]);
				errcount++;
				if (first_err == 0) {
					// first error!
					first_err = map_scha_error(rv);
				}
			} else {
				if (scha_status.err_msg) {
					fprintf(stdout, "%s\n",
					    scha_status.err_msg);
				}
				if (optflgs & vflg) {
					clmessage("Resource group \"%s\" "
					    "modified.\n", rg_names[i]);
				}
			}
		}
	}

	if (nodestr)
		free(nodestr);
	free_prop_array(proplist);
	if (zc_name) {
		free(zc_name);
	}
	if (full_name) {
		free(full_name);
	}
	return (first_err);
}
