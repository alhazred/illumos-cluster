//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clrg_evacuate.cc	1.5	08/07/24 SMI"

//
// Process clrg "evacuate"
//

#include <sys/os.h>
#include <scadmin/scconf.h>
#include <scadmin/scswitch.h>
#include <scadmin/scstat.h>
#include <rgm/rgm_scrgadm.h>

#include "clcommands.h"

#include "ClCommand.h"

#include "rgm_support.h"
#include "common.h"

//
// clrg "evacuate"
//
clerrno_t
clrg_evacuate(ClCommand &cmd, optflgs_t optflgs, int itimeout,
    ValueList &nodes, char *zc_name)
{
	char *nodename;
	int rv = 0;
	clerrno_t clerrno = CL_NOERR;
	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};
	ValueList good_nodes;
	boolean_t verbose;
	ValueList tmp_list;

	if (optflgs & vflg)
		verbose = B_TRUE;
	else
		verbose = B_FALSE;

#if (SOL_VERSION >= __s10)
	if (zc_name) {
		// If zone cluster name was specified, we have to ensure
		// that it is valid.
		// We have to check whether this operation is allowed for
		// the status of the zone cluster.
		clerrno = check_zc_operation(CL_OBJ_TYPE_RG,
						CL_OP_TYPE_EVACUATE,
						zc_name);
		if (clerrno != CL_NOERR) {
			return (clerrno);
		}
	}
#endif

	tmp_list.add(zc_name);
	clerrno = get_good_nodes(nodes, good_nodes, tmp_list);
	tmp_list.clear();

	if (clerrno)
		return (clerrno);

	good_nodes.start();
	nodename = strdup(good_nodes.getValue());

	scha_status = scswitch_evacuate_rgs(nodename, (ushort_t)itimeout,
	    verbose, zc_name);
	rv = scha_status.err_code;
	if (rv) {
		print_rgm_error(scha_status, NULL);
	} else if (scha_status.err_msg) {
		fprintf(stdout, "%s\n", scha_status.err_msg);
	}

	return (map_scha_error(rv));
}
