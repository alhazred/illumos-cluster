//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clrg_delete.cc	1.10	08/07/25 SMI"

//
// Process clrg "delete"
//

#include <sys/os.h>
#include <scadmin/scconf.h>
#include <scadmin/scswitch.h>
#include <scadmin/scstat.h>
#include <rgm/rgm_scrgadm.h>

#include "clcommands.h"

#include "ClCommand.h"

#include "clresource.h"
#include "clresourcegroup.h"
#include "rgm_support.h"
#include "common.h"

//
// clrg "delete"
//
clerrno_t
clrg_delete(ClCommand &cmd, ValueList &operands, optflgs_t optflgs)
{
	int errcount = 0;
	clerrno_t first_err = CL_NOERR;
	int i;
	int rv = 0;
	int len = 0;
	int first_val = 1;
	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};
	char **rg_names = NULL;
	char *oper1;
	clerrno_t errflg = CL_NOERR;
	clerrno_t clerrno = CL_NOERR;
	boolean_t verbose = B_FALSE;
	boolean_t force = B_FALSE;
	ValueList allrg;
	boolean_t list_changed = B_FALSE;
	ValueList empty_list;
	ValueList rslist;
	ValueList rg_names_vlist, rg_full_names_vlist;
	ValueList rs_names_vlist;
	ValueList good_rgs;
	char *zc_name, *rg_name, *zc_part;
	scconf_errno_t scconf_err = SCCONF_NOERR;
	boolean_t is_same = B_FALSE;

	if (optflgs & vflg)
		verbose = B_TRUE;

	if (optflgs & Fflg)
		force = B_TRUE;

	// Initializa ORB and other cluster checks
	clerrno = cluster_init();
	if (clerrno)
		return (clerrno);

	zc_name = rg_name = zc_part = NULL;
	// Check whether any zone cluster names were specified.
	// And if they were specified, there should be only zone cluster
	// across all the RGs.
	clerrno = check_single_zc_specified(operands, &zc_name, &is_same);
	if (clerrno != CL_NOERR) {
		clcommand_perror(clerrno);
		return (clerrno);
	}
	// If multiple zone clusters were specified, report an error.
	if (is_same == B_FALSE) {
		clerror("This operation can be performed on only one "
		    "zone cluster at a time.\n");
		return (clerrno);
	}

#if (SOL_VERSION >= __s10)
	if (zc_name) {
		// If zone cluster name was specified, we have to ensure
		// that it is valid.
		// We have to check whether this operation is allowed for
		// the status of the zone cluster.
		clerrno = check_zc_operation(CL_OBJ_TYPE_RG, CL_OP_TYPE_DELETE,
						zc_name);
		if (clerrno != CL_NOERR) {
			return (clerrno);
		}
	}
#endif

	operands.start();
	// Get the zone cluster name and the RG name
	scconf_err = scconf_parse_obj_name_from_cluster_scope(
			operands.getValue(), &oper1, &zc_part);
	// Free memory
	if (zc_part) {
		free(zc_part);
		zc_part = NULL;
	}

	// Return if there was an error.
	if (scconf_err != SCCONF_NOERR) {
		clerror("Internal error.\n");
		return (CL_EINTERNAL);
	}

	if (strcmp(oper1, CLCOMMANDS_WILD_OPERAND)) {
		for (operands.start(); !operands.end(); operands.next()) {
			if (is_good_rg(operands.getValue(), B_TRUE)) {
				good_rgs.add(operands.getValue());
			} else {
				// No need to print an error reporting an
				// invalid resource group since is_good_rg()
				// takes care of reporting the error.
				if (first_err == 0) {
					// first error!
					first_err = CL_EINVAL;
				}
			}
		}
		rg_names = vl_to_names(good_rgs, &errflg);
		if (errflg)
			return (errflg);

	} else {
		scha_status = scswitch_getrglist(&rg_names, B_FALSE, zc_name);
		rv = scha_status.err_code;
		if (rv != SCHA_ERR_NOERR) {
			clerror("Failed to obtain list of all resource "
			    "groups.\n");
			print_rgm_error(scha_status, NULL);
			return (map_scha_error(rv));
		}
	}

	// Free memory
	if (oper1) {
		free(oper1);
		oper1 = NULL;
	}

	if ((rg_names == (char **)NULL) || (*rg_names == (char *)NULL)) {
		goto cleanup;
	}

	clerrno = get_all_rg(allrg);
	if (clerrno) {
		first_err = clerrno;
		goto cleanup;
	}

	//
	// If no force flag, remove all non-empty rgs from the
	// list. Then remove all rgs that have dependents/affinities
	// outside the list. Then remove all dependency/affinity properties
	// of the surviving rgs in the list.
	//
	if (!force) {
		list_changed = B_FALSE;
		clerrno = remove_busy_rg_from_list(rg_names, &list_changed);
		if (clerrno) {
			if (first_err == 0) {
				// first error!
				first_err = clerrno;
			}
			goto cleanup;
		}
		if (list_changed == B_TRUE) {
			if (first_err == 0) {
				// first error!
				first_err = CL_EOP;
			}
		}
		list_changed = B_TRUE;
		while (list_changed == B_TRUE) {
			list_changed = B_FALSE;
			clerrno = remove_rg_referred_from_list(rg_names, allrg,
			    &list_changed);
			if (clerrno) {
				if (first_err == 0) {
					// first error!
					first_err = clerrno;
				}
				goto cleanup;
			}
			if (list_changed == B_TRUE) {
				if (first_err == 0) {
					// first error!
					first_err = CL_EOP;
				}
			}

		}

		// At this point we can destroy the dependencies/affinities
		clerrno = eliminate_rg_references(rg_names, empty_list,
		    B_FALSE, verbose);
		if (clerrno) {
			if (first_err == 0) {
				// first error!
				first_err = CL_EOP;
			}
			goto cleanup;
		}
	}

	//
	// If force option is specified, delete all resources in the rgs
	// and then cut down all dependency links from outside this set.
	// Then remove all dependency/affinity properties of the surviving
	// rgs in the list.
	//
	if (force) {
		// First quiesce and clear error
		for (i = 0; rg_names[i] != NULL; i++) {
			rg_full_names_vlist.add(rg_names[i]);
		}
		if (verbose) {
			clmessage("Attempting to quiesce resource groups...\n");
		}
		clerrno = clrg_quiesce(cmd, rg_full_names_vlist, optflgs);
		if (first_err == 0) {
			// first error!
			first_err = clerrno;
		}
		clerrno = remove_zc_names_from_valuelist(rg_full_names_vlist,
							rg_names_vlist);
		if (first_err == CL_NOERR) {
			first_err = clerrno;
		}
		rs_names_vlist.add("+");
		if (zc_name) {
			// We have to prepend the ZC name
			rs_names_vlist.start();
			rs_names_vlist.prepend(zc_name,
					ZC_AND_OBJ_NAME_SEPARATER_CHAR);
		}
		clerrno = clresource_clear(cmd, rs_names_vlist, optflgs & vflg,
		    rg_names_vlist, empty_list, ERRFLAG_STOP_FAILED,
		    empty_list);
		if (first_err == 0) {
			// first error!
			first_err = clerrno;
		}
		clerrno = eliminate_sysprop(rg_names, verbose);
		if (first_err == 0) {
			// first error!
			first_err = clerrno;
		}
		clerrno = get_all_rs(rslist, rg_names);
		if (first_err == 0) {
			// first error!
			first_err = clerrno;
		}
		if (rslist.getSize() > 0) {
			clerrno = clresource_delete(cmd, rslist, optflgs,
			    empty_list, empty_list);
			if (first_err == 0) {
				// first error!
				first_err = clerrno;
			}
		}
		clerrno = eliminate_rg_references(rg_names, allrg, B_TRUE,
		    verbose);
		if (first_err == 0) {
			// first error!
			first_err = clerrno;
		}
		clerrno = eliminate_rg_references(rg_names, empty_list,
		    B_TRUE, verbose);
		if (first_err == 0) {
			// first error!
			first_err = clerrno;
		}
	}

	if (rg_names) {
		for (i = 0; rg_names[i] != NULL; i++) {

			if (rg_name) {
				free(rg_name);
				rg_name = NULL;
			}
			if (zc_name) {
				free(zc_name);
				zc_name = NULL;
			}

			scconf_err = scconf_parse_obj_name_from_cluster_scope(
					rg_names[i], &rg_name, &zc_name);

			if (scconf_err != SCCONF_NOERR) {
				if (first_err == CL_NOERR) {
					first_err = CL_EINTERNAL;
				}
				break;
			}

			if (strcmp(rg_name, DELETED_RG) == 0)
				continue;

			scha_status = rgm_scrgadm_remove_rg(rg_name,
			    verbose, zc_name);
			rv = scha_status.err_code;
			if (rv) {
				print_rgm_error(scha_status, NULL);
				errcount++;
				if (first_err == 0) {
					// first error!
					first_err = map_scha_error(rv);
				}
			} else if (scha_status.err_msg) {
				fprintf(stdout, "%s\n", scha_status.err_msg);
			}
		}
	}

cleanup:
	if (rg_names)
		rgm_free_strarray(rg_names);

	if (rg_name) {
		free(rg_name);
	}
	if (zc_name) {
		free(zc_name);
	}
	return (first_err);
}
