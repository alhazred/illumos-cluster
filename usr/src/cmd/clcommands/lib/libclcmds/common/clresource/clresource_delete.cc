//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clresource_delete.cc	1.12	08/07/25 SMI"

//
// Process clresource "delete"
//

#ifdef __cplusplus
#include <sys/os.h>
#endif

#include <scadmin/scswitch.h>
#include <scadmin/scrgadm.h>
#include <scadmin/scconf.h>
#include <scadmin/scstat.h>
#include "clcommands.h"
#include "ClCommand.h"

#include "rgm_support.h"
#include "clresource.h"
#include "common.h"

//
// clresource "delete"
//
clerrno_t
clresource_delete(ClCommand &cmd, ValueList &operands, optflgs_t optflgs,
    ValueList &resourcegroups, ValueList &resourcetypes)
{
	clerrno_t first_err = CL_NOERR;
	int rv = 0;
	clerrno_t clerrno = CL_NOERR;
	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};
	char **rg_names = NULL;
	char **rs_names = (char **)0;
	char *oper1;
	clerrno_t errflg = CL_NOERR;
	uint_t i, j, k;
	ValueList rslist;
	rgm_resource_t *rs = (rgm_resource_t *)NULL;
	char *r_rtname = (char *)NULL;
	ValueList good_rts;
	ValueList good_rgs;
	boolean_t verbose = B_FALSE;
	boolean_t force = B_FALSE;
	boolean_t list_changed;
	ValueList allrs;
	ValueList empty_list, tmp_list;
	char **sel_rsnames = (char **)NULL;
	scconf_errno_t scconf_err = SCCONF_NOERR;
	boolean_t is_same = B_FALSE;
	char *zc_name;
	char *rs_part, *zc_part, *tmpStr;
	int str_len;

	// Initializa ORB and other cluster checks
	clerrno = cluster_init();
	if (clerrno)
		return (clerrno);

	if (optflgs & vflg)
		verbose = B_TRUE;

	if (optflgs & Fflg)
		force = B_TRUE;

	// Check whether any zone cluster names were specified.
	// And if they were specified, there should be only zone cluster
	// across all the RGs.
	zc_name = NULL;
	rs_part = zc_part = NULL;
	clerrno = check_single_zc_specified(operands, &zc_name, &is_same);
	if (clerrno != CL_NOERR) {
		clcommand_perror(clerrno);
		return (clerrno);
	}
	// If multiple zone clusters were specified, report an error.
	if (is_same == B_FALSE) {
		clerror("This operation can be performed on only one "
		    "zone cluster at a time.\n");
		return (clerrno);
	}
#if (SOL_VERSION >= __s10)
	if (zc_name) {
		// If zone cluster name was specified, we have to ensure
		// that it is valid.
		// We have to check whether this operation is allowed for
		// the status of the zone cluster.
		clerrno = check_zc_operation(CL_OBJ_TYPE_RS, CL_OP_TYPE_DELETE,
						zc_name);
		if (clerrno != CL_NOERR) {
			return (clerrno);
		}
	}
#endif

	// Check resourcetypes
	for (resourcetypes.start(); !resourcetypes.end();
	    resourcetypes.next()) {
		if (r_rtname) {
			free(r_rtname);
			r_rtname = (char *)NULL;
		}
		clerrno = get_r_rtname(resourcetypes.getValue(), &r_rtname,
					zc_name);
		if (clerrno == 0)
			good_rts.add(r_rtname);
		else {
			if (first_err == 0) {
				// first error!
				first_err = clerrno;
			}
		}
	}
	if (r_rtname)
		free(r_rtname);

	if ((resourcetypes.getSize() > 0) && (good_rts.getSize() == 0))
		return (first_err);

	if (optflgs & LHflg)
		good_rts.add(get_latest_rtname(SCRGADM_RTLH_NAME, zc_name));

	if (optflgs & SAflg)
		good_rts.add(get_latest_rtname(SCRGADM_RTSA_NAME, zc_name));

	for (resourcegroups.start(); !resourcegroups.end();
	    resourcegroups.next()) {
		clerrno = check_rg(resourcegroups.getValue(), zc_name);
		if (clerrno == CL_NOERR)
			good_rgs.add(resourcegroups.getValue());
		else {
			if (first_err == 0) {
				// first error!
				first_err = clerrno;
			}
		}
	}
	if ((resourcegroups.getSize() > 0) && (good_rgs.getSize() == 0))
		return (first_err);

	operands.start();
	oper1 = operands.getValue();
	if (strcmp(oper1, CLCOMMANDS_WILD_OPERAND) == 0) {
		if (good_rgs.getSize() > 0) {
			rg_names = vl_to_names(good_rgs, &errflg);
			if (errflg)
				return (errflg);
		} else {
			clerrno = z_getrglist(&rg_names, verbose, B_TRUE,
						zc_name);
			if (clerrno != CL_NOERR) {
				if (first_err == CL_NOERR) {
					first_err = clerrno;
				}
				goto cleanup;
			}
			// Remove the zone cluster context from all these
			// names.
			clerrno = remove_zc_names_from_names(rg_names);
			if (clerrno != CL_NOERR) {
				clcommand_perror(clerrno);
				if (first_err == CL_NOERR) {
					first_err = clerrno;
				}
				goto cleanup;
			}
		}
	}

	// By the time we are here, RT names and RG names do not have
	// the zone cluster scope in their names. Only the resource names
	// have the zone cluster scope in their names.

	if ((strcmp(oper1, CLCOMMANDS_WILD_OPERAND) == 0) &&
	    (rg_names != (char **)NULL)) {

		// Get the list of resources for this group
		for (i = 0; rg_names[i] != NULL; i++) {
			if (rs_names) {
				rgm_free_strarray(rs_names);
				rs_names = (char **)NULL;
			}
			scha_status = rgm_scrgadm_getrsrclist(rg_names[i],
			    &rs_names, zc_name);
			rv = scha_status.err_code;
			if (rv != SCHA_ERR_NOERR) {
				print_rgm_error(scha_status, NULL);
				clerror("Failed to obtain list of all "
				    "resources in resource group "
				    "\"%s\".\n", rg_names[i]);
				clerror("Will not try to delete "
				    "resources belonging to group "
				    "\"%s\".\n", rg_names[i]);
				if (first_err == 0) {
					// first error!
					first_err = map_scha_error(rv);
				}
				continue;
			}
			if (rs_names == NULL)
				continue;

			for (j = 0; rs_names[j]; j++) {
				if (rs != NULL) {
					free(rs);
					rs = NULL;
				}

				scha_status = rgm_scrgadm_getrsrcconf(
				    rs_names[j], &rs, zc_name);
				rv = scha_status.err_code;
				if (rv != SCHA_ERR_NOERR) {
					print_rgm_error(scha_status, NULL);
					clerror("Will not attempt to delete "
					    "resource \"%s\".\n",
					    rs_names[j]);
					if (first_err == 0) {
						// first error!
						first_err = map_scha_error(rv);
					}
					continue;
				}
				if (rs == NULL) {
					clerror("Failed to access "
					    "\"%s\".\n", rs_names[j]);
					clerror("Will not attempt to "
					    "delete \"%s\".\n", rs_names[j]);
					if (first_err == 0) {
						// first error!
						first_err = CL_ENOENT;
					}
					continue;
				}
				if (good_rts.getSize() > 0)
					if (match_type(good_rts, rs) == 0)
						continue;

				// Now we can add.
				if (!zc_name) {
					rslist.add(rs->r_name);
					continue;
				}
				// Zone cluster was specified.
				str_len = strlen(rs->r_name);
				str_len += strlen(zc_name);
				str_len += 2; // 1 for ':' and one for NULL.
				tmpStr = (char *)calloc(str_len, 1);
				strcat(tmpStr, zc_name);
				strcat(tmpStr, ":");
				strcat(tmpStr, rs->r_name);
				rslist.add(rs->r_name);
				free(tmpStr);
			}
		}
	} else if (strcmp(oper1, CLCOMMANDS_WILD_OPERAND) != 0) {
		for (operands.start(); operands.end() != 1; operands.next()) {

			if (rs != NULL) {
				free(rs);
				rs = NULL;
			}
			if (rs_part) {
				free(rs_part);
				rs_part = NULL;
			}
			if (zc_part) {
				free(zc_part);
				zc_part = NULL;
			}
			// First get the RT name without the ZC name
			scconf_err = scconf_parse_obj_name_from_cluster_scope(
					operands.getValue(), &rs_part,
					&zc_part);

			if (scconf_err != SCCONF_NOERR) {
				clerror("Internal error.\n");
				if (first_err == CL_NOERR) {
					first_err = CL_EINTERNAL;
				}
				continue;
			}

			scha_status = rgm_scrgadm_getrsrcconf(
			    rs_part, &rs, zc_part);
			rv = scha_status.err_code;
			if (rv != SCHA_ERR_NOERR) {
				print_rgm_error(scha_status, NULL);
				clerror("Will not attempt to delete resource "
				    "\"%s\".\n", operands.getValue());
				if (first_err == 0) {
					// first error!
					first_err = map_scha_error(rv);
				}
				continue;
			}
			if (rs == NULL) {
				clerror("Failed to access \"%s\".\n",
				    operands.getValue());
				clerror("will not attempt to delete \"%s\".\n",
				    operands.getValue());
				if (first_err == 0) {
					// first error!
					first_err = CL_ENOENT;
				}
				continue;
			}
			if (good_rts.getSize() > 0) {
				if (match_type(good_rts, rs) == 0) {
					if (optflgs & LHflg)
						clerror("Specified resource "
						    "\"%s\" is not a logical "
						    "hostname resource.\n",
						    operands.getValue());
					if (optflgs & SAflg)
						clerror("Specified resource "
						    "\"%s\" is not a shared "
						    "address resource.\n",
						    operands.getValue());
					else
						clerror("Specified resource "
						    "\"%s\" does not meet the "
						    "criteria set by the "
						    "\"-t\" option.\n",
						    operands.getValue());
					if (first_err == 0) {
						// first error!
						first_err = CL_EINVAL;
					}
					continue;
				}
			}
			if (good_rgs.getSize() > 0) {
				if (match_group(good_rgs, rs) == 0) {
					clerror("Specified resource \"%s\" "
					    "does not meet the criteria set "
					    "by the \"-g\" option.\n",
					    operands.getValue());
					if (first_err == 0) {
						// first error!
						first_err = CL_EINVAL;
					}
					continue;
				}
			}
			rslist.add(operands.getValue());
		}
	}

	if (rslist.getSize() == 0) {
		goto cleanup;
	}

	sel_rsnames = vl_to_names(rslist, &errflg);
	if (errflg) {
		first_err = errflg;
		goto cleanup;
	}
	clerrno = get_all_rs(allrs, (char **)NULL);
	if (clerrno) {
		first_err = clerrno;
		goto cleanup;
	}

	//
	// If no force flag, remove all enabled resource from the
	// list. Then remove all resources that have dependents
	// outside the list. Then remove all dependency properties
	// of the surviving resources in the list.
	//
	if (!force) {
		list_changed = B_FALSE;
		remove_enabled_rs_from_list(sel_rsnames, &list_changed);
		if (list_changed == B_TRUE) {
			if (first_err == 0) {
				// first error!
				first_err = CL_EOP;
			}
		}
		list_changed = B_TRUE;
		while (list_changed == B_TRUE) {
			list_changed = B_FALSE;
			remove_rs_dependees_from_list(sel_rsnames, allrs,
			    &list_changed);
			if (list_changed == B_TRUE) {
				if (first_err == 0) {
					// first error!
					first_err = CL_EOP;
				}
			}

		}
		// Add a check, if network rs and enabled rs in
		// group, remove that rs from list

		// At this point we can destroy the resources/properties
		clerrno = eliminate_rs_dependencies(sel_rsnames, empty_list,
		    B_FALSE, verbose);
		if (clerrno) {
			if (first_err == 0) {
				// first error!
				first_err = CL_EOP;
			}
			goto cleanup;
		}
	}

	//
	// If force option is specified, disable all resources in the list
	// and then cut down all dependency links from outside this set.
	// Then remove all dependency properties of the surviving resources
	// in the list.
	//
	if (force) {
		clerrno = clresource_clear(cmd, rslist, optflgs & vflg,
		    empty_list, empty_list, ERRFLAG_STOP_FAILED, empty_list);
		if (first_err == 0) {
			// first error!
			first_err = clerrno;
		}
		for (rslist.start(); !rslist.end(); rslist.next()) {
			clerrno = eliminate_rg_sysprop(rslist.getValue(),
			    verbose);
			if (first_err == 0) {
				// first error!
				first_err = clerrno;
			}
		}

		// To disable the resources, we need their name without
		// the zone cluster scoping. First, let us remove the
		// zone cluster name from the names of resources.
		clerrno = remove_zc_names_from_valuelist(rslist, tmp_list);
		if (clerrno != CL_NOERR) {
			if (first_err == CL_NOERR) {
				first_err = clerrno;
			}
		}

		clerrno = disable_rs(tmp_list, verbose, force, zc_name);
		tmp_list.clear();
		if (first_err == 0) {
			// first error!
			first_err = clerrno;
		}

		// The disable might leave resources in a stop-failed state
		clerrno = clresource_clear(cmd, rslist, optflgs & vflg,
		    empty_list, empty_list, ERRFLAG_STOP_FAILED, empty_list);
		if (first_err == 0) {
			// first error!
			first_err = clerrno;
		}

		clerrno = eliminate_rs_dependencies(sel_rsnames, allrs, B_TRUE,
		    verbose);
		if (first_err == 0) {
			// first error!
			first_err = clerrno;
		}
		clerrno = eliminate_rs_dependencies(sel_rsnames, empty_list,
		    B_TRUE, verbose);
		if (first_err == 0) {
			// first error!
			first_err = clerrno;
		}
	}

	for (k = 0; sel_rsnames[k]; k++) {
		if (rs_part) {
			free(rs_part);
			rs_part = NULL;
		}
		if (zc_part) {
			free(zc_part);
			zc_part = NULL;
		}
		// First get the R name without the ZC name
		scconf_err = scconf_parse_obj_name_from_cluster_scope(
				sel_rsnames[k], &rs_part,
				&zc_part);

		if (scconf_err != SCCONF_NOERR) {
			clerror("Internal error.\n");
			if (first_err == CL_NOERR) {
				first_err = CL_EINTERNAL;
			}
			continue;
		}

		if (strcmp(sel_rsnames[k], DELETED_RS) == 0)
			continue;

		scha_status = rgm_scrgadm_delete_resource(rs_part,
						verbose, zc_part);
		rv = scha_status.err_code;
		if (rv != CL_NOERR) {
			print_rgm_error(scha_status, NULL);
			if (first_err == 0) {
				// first error!
				first_err = map_scha_error(rv);
			}
		} else if (scha_status.err_msg)
			fprintf(stdout, "%s\n", scha_status.err_msg);
	}

cleanup:
	if (rg_names)
		rgm_free_strarray(rg_names);
	if (rs_names)
		rgm_free_strarray(rs_names);
	if (rs)
		free(rs);
	if (sel_rsnames)
		free(sel_rsnames);
	if (rs_part) {
		free(rs_part);
	}
	if (zc_part) {
		free(zc_part);
	}
	if (zc_name) {
		free(zc_name);
	}

	return (first_err);
}
