//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clresource_show.cc	1.17	08/10/13 SMI"

//
// Process clresource "show"
//

#ifdef __cplusplus
#include <sys/os.h>
#endif

#include <scadmin/scswitch.h>
#include <scadmin/scrgadm.h>
#include <scadmin/scconf.h>
#include <scadmin/scstat.h>
#include "clcommands.h"
#include "ClCommand.h"
#include "ClShow.h"

#include "clresource.h"
#include "rgm_support.h"
#include "common.h"

static clerrno_t
show_rs(ClShow *rs_show, rgm_resource_t *rs, rgm_rg_t *rg, ValueList *rg_nl,
    int props_specified, NameValueList &properties, int verbose, char *zc_name);
static clerrno_t
show_rs_property(ClShow *rs_show, rgm_resource_t *rs, rgm_rg_t *rg,
    ValueList *rg_nl, rgm_property_t *rp, int props_specified,
    NameValueList &properties, int verbose, boolean_t extension);
static clerrno_t
merge_prop(NameValueList &props_needed, NameValueList &merged_props,
    NameValueList &proplist);
static int prop_present(NameValueList &properties, char *prop);
static clerrno_t
filter_rs_and_show_it(ClShow *rs_show, char *rs_name, char *zc_name,
    ValueList &good_rts, ValueList &good_rgs, int props_specified,
    NameValueList &merged_props, int verbose, optflgs_t optflgs);

//
// clrs show
//
clerrno_t
clresource_show(ClCommand &cmd, ValueList &operands, optflgs_t optflgs,
    ValueList &resourcegroups, ValueList &resourcetypes,
    ValueList &properties, ValueList &xproperties, ValueList &yproperties,
    ValueList &zc_names)
{
	clerrno_t clerrno = CL_NOERR;
	int rv = 0;
	ClShow *rs_show = new ClShow(HEAD_RESOURCES);

	// Initializa ORB and other cluster checks
	clerrno = cluster_init();
	if (clerrno)
		return (clerrno);

	clerrno = get_clresource_show_obj_select(operands, optflgs,
	    resourcegroups,
	    resourcetypes, properties, xproperties, yproperties, rs_show,
	    zc_names);
	(void) rs_show->print();
	return (clerrno);
}

//
// clrs show for cluster command
//
clerrno_t
get_clresource_show_obj(optflgs_t optflgs, ClShow *rs_show)
{
	clerrno_t clerrno = CL_NOERR;
	ValueList rs_list = ValueList(true);
	ValueList empty_list;

	rs_list.add(CLCOMMANDS_WILD_OPERAND);
	clerrno = get_clresource_show_obj_select(rs_list, optflgs, empty_list,
	    empty_list, empty_list, empty_list, empty_list, rs_show);
	return (clerrno);
}

//
// Fetch the show buffer according to options/operands.
//
clerrno_t
get_clresource_show_obj_select(ValueList &operands, optflgs_t optflgs,
    ValueList &resourcegroups, ValueList &resourcetypes,
    ValueList &properties, ValueList &xproperties, ValueList &yproperties,
    ClShow *rs_show, ValueList &zc_names)
{
	clerrno_t first_err = CL_NOERR;
	int rv = 0;
	int prop_found = 0;
	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};
	char **rg_names = NULL;
	char **rs_names = (char **)0;
	char *oper1;
	clerrno_t errflg = CL_NOERR;
	clerrno_t clerrno = CL_NOERR;
	uint_t i, j, k;
	ValueList rslist;
	rgm_resource_t *rs = (rgm_resource_t *)NULL;
	char *r_rtname = (char *)NULL;
	ValueList good_rts;
	ValueList good_rgs;
	scstat_errno_t scstat_status;
	int props_specified = 0;
	int verbose = 0;
	NameValueList proplist;
	NameValueList props_needed;
	NameValueList merged_props;
	rgm_rg_t *rg = (rgm_rg_t *)NULL;
	nodeidlist_t *nl;
	char *nodename = NULL;
	ValueList *rg_nl = (ValueList *)NULL;
	boolean_t pn_matched;
	boolean_t pn_specified;
	char *nodepart = (char *)NULL;
	char *proppart = (char *)NULL;
	scconf_errno_t scconf_err = SCCONF_NOERR;
	char *zc_name, *full_name, *r_name, *obj_name;
	ValueList tmp_list, valid_zc_list;
	boolean_t has_obj, found_rs;

	zc_name = full_name = r_name = obj_name = NULL;
	has_obj = found_rs = B_FALSE;
	// Zone cluster names can be specified either with the -Z
	// option or in the form of <zonecluster>:<R name> format.
	// But, both the ways cannot be used simultaneously.
	// If zone clusters were not specified with the -Z option, we
	// have to check whether they were specified in the <zc>:<r>
	// format.
	for (operands.start(); !operands.end(); operands.next()) {
		scconf_err = scconf_parse_obj_name_from_cluster_scope(
				operands.getValue(), &r_name, &zc_name);

		if (scconf_err != SCCONF_NOERR) {
			return (CL_ENOMEM);
		}

		if (zc_name && zc_names.getSize()) {
			// -Z flag was specified and this R was scoped
			// with a zone cluster name as well. Report an
			// error.
			clerror("You cannot use the "
			    "\"<zonecluster>:<resourcename>\" "
			    "form of the resource name with the "
			    "\"-Z\" option.");
			free(r_name);
			free(zc_name);
			return (CL_EINVAL);
		}

		// If this zone cluster name does not exist in tmp_list,
		// then we wil add it here.
		if (!tmp_list.isValue(zc_name)) {
			tmp_list.add(zc_name);
		}

		if (r_name) {
			free(r_name);
			r_name = NULL;
		}
		if (zc_name) {
			free(zc_name);
			zc_name = NULL;
		}
	}
#if (SOL_VERSION >= __s10)
	// By the time we are here, we have added any zone cluster name in the
	// Resource names to tmp_list.
	if (zc_names.getSize()) {
		// Zc names were specified. We have to check whether "all"
		// was specified.
		zc_names.start();

		if (strcmp(zc_names.getValue(), ALL_CLUSTERS_STR) == 0) {
			// Get all known zone clusters.
			clerrno = get_all_clusters(valid_zc_list);

			if (clerrno != CL_NOERR) {
				return (clerrno);
			}
		} else {
			tmp_list = zc_names;
		}
	}

	// valid_zc_list will contain at least one zone cluster name
	// if "all" was specified. If it is empty, we have to check
	// the validity of the zone clusters specified by the user.
	if (valid_zc_list.getSize() < 1) {
		clerrno = get_valid_zone_clusters(tmp_list, valid_zc_list);
	}
	// Clear the memory.
	// tmp_list.clear();

	if (clerrno) {
		// There was at least one invalid zone cluster.
		// We have to return.
		return (clerrno);
	}
#endif

	// Now, we have to check the validity of properties.
	if ((properties.getSize() > 0) || (xproperties.getSize() > 0) ||
	    (yproperties.getSize() > 0))
		props_specified = 1;

	if ((optflgs & vflg) || (properties.getSize() > 0) ||
	    (xproperties.getSize() > 0) || (yproperties.getSize() > 0)) {
		verbose = 1;
	}

	// Check properties
	clerrno = get_all_r_props(proplist);
	if (clerrno)
		return (clerrno);

	for (properties.start(); properties.end() != 1; properties.next()) {
		prop_found = B_FALSE;
		pn_matched = B_FALSE;
		clerrno = tok_prop(properties.getValue(), &pn_specified,
		    &proppart, &nodepart);
		if (clerrno) {
			if (clerrno == CL_ENOMEM) {
				return (CL_ENOMEM);
			}
			if (first_err == 0) {
				// first error!
				first_err = clerrno;
			}
		}

		for (proplist.start(); proplist.end() != 1; proplist.next()) {
			if (strcasecmp(proplist.getName(), proppart) == 0) {
				prop_found = B_TRUE;
				if (pn_specified) {
					if (strstr(proplist.getValue(), "pn")) {
						pn_matched = B_TRUE;
						break;
					}
				} else {
					break;
				}
			}
		}
		if (pn_specified && !pn_matched && prop_found) {
			clwarning("A node-specifier is not appropriate for "
			    "the specified property \"%s\".\n", proppart);
		}

		if (!prop_found) {
			clerror("Specified property \"%s\" is not valid for "
			    "any of the registered resource types.\n",
			    properties.getValue());
			if (first_err == 0) {
				// first error!
				first_err = CL_EPROP;
			}
		} else {
			if (nodepart && *nodepart) {
				props_needed.add(proppart, nodepart);
			} else {
				props_needed.add(proppart, "");
			}
		}
		if (proppart) {
			free(proppart);
			proppart = NULL;
		}
		if (nodepart) {
			free(nodepart);
			nodepart = NULL;
		}
	}

	for (xproperties.start(); xproperties.end() != 1; xproperties.next()) {
		prop_found = B_FALSE;
		pn_matched = B_FALSE;
		clerrno = tok_prop(xproperties.getValue(), &pn_specified,
		    &proppart, &nodepart);
		if (clerrno) {
			if (clerrno == CL_ENOMEM) {
				return (CL_ENOMEM);
			}
			if (first_err == 0) {
				// first error!
				first_err = clerrno;
			}
		}

		for (proplist.start(); proplist.end() != 1; proplist.next()) {
			if (strncasecmp(proplist.getValue(), "standard",
			    strlen("standard")) == 0)
				continue;
			if (strcasecmp(proplist.getName(), proppart) == 0) {
				prop_found = B_TRUE;
				if (pn_specified) {
					if (strstr(proplist.getValue(), "pn")) {
						pn_matched = B_TRUE;
						break;
					}
				} else {
					break;
				}
			}
		}
		if (pn_specified && !pn_matched && prop_found) {
			clwarning("A node-specifier is not appropriate for "
			    "the specified property \"%s\".\n", proppart);
		}

		if (!prop_found) {
			clerror("Specified extension property \"%s\" is not "
			    "valid for any of the registered resource types.\n",
			    xproperties.getValue());
			if (first_err == 0) {
				// first error!
				first_err = CL_EPROP;
			}
		} else {
			if (nodepart && *nodepart) {
				props_needed.add(proppart, nodepart);
			} else {
				props_needed.add(proppart, "");
			}
		}
		if (proppart) {
			free(proppart);
			proppart = NULL;
		}
		if (nodepart) {
			free(nodepart);
			nodepart = NULL;
		}
	}

	for (yproperties.start(); yproperties.end() != 1; yproperties.next()) {
		prop_found = B_FALSE;
		pn_matched = B_FALSE;
		clerrno = tok_prop(yproperties.getValue(), &pn_specified,
		    &proppart, &nodepart);
		if (clerrno) {
			if (clerrno == CL_ENOMEM) {
				return (CL_ENOMEM);
			}
			if (first_err == 0) {
				// first error!
				first_err = clerrno;
			}
		}

		for (proplist.start(); proplist.end() != 1; proplist.next()) {
			if (strncasecmp(proplist.getValue(), "extension",
			    strlen("extension")) == 0)
				continue;
			if (strcasecmp(proplist.getName(), proppart) == 0) {
				prop_found = B_TRUE;
				if (pn_specified) {
					if (strstr(proplist.getValue(), "pn")) {
						pn_matched = B_TRUE;
						break;
					}
				} else {
					break;
				}
			}
		}
		if (pn_specified && !pn_matched && prop_found) {
			clwarning("A node-specifier is not appropriate for "
			    "the specified property \"%s\".\n", proppart);
		}

		if (!prop_found) {
			clerror("Specified standard property \"%s\" is not "
			    "valid for any of the registered resource types.\n",
			    yproperties.getValue());
			if (first_err == 0) {
				// first error!
				first_err = CL_EPROP;
			}
		} else {
			if (nodepart && *nodepart) {
				props_needed.add(proppart, nodepart);
			} else {
				props_needed.add(proppart, "");
			}
		}
		if (proppart) {
			free(proppart);
			proppart = NULL;
		}
		if (nodepart) {
			free(nodepart);
			nodepart = NULL;
		}
	}

	clerrno = merge_prop(props_needed, merged_props, proplist);
	if (clerrno)
		return (clerrno);

	// Check resourcetypes.
	// If zone clusters were specified, we have to check whether
	// the specified RTs are part of at least one zone cluster.
	// If an RT is not present in at least one zone cluster, then
	// we will not include it in the list of good RTs (good_rts).
	for (resourcetypes.start(); !resourcetypes.end();
	    resourcetypes.next()) {
		if (valid_zc_list.getSize() == 0) {
			// If we are here, then no zone cluster was
			// specified. We just have to check whether this
			// RT is valid in the current cluster.
			clerrno = get_r_rtname(resourcetypes.getValue(),
						&r_rtname, NULL);
			if (clerrno == 0) {
				good_rts.add(r_rtname);
			} else {
				if (first_err == 0) {
					// first error!
					first_err = clerrno;
				}
			}

			if (r_rtname) {
				free(r_rtname);
				r_rtname = 0;
			}

			continue;
		}
		// If we are here, it means zone clusters were specified.
		// We have to check whether this RT is valid across at least
		// one zone cluster.
		has_obj = B_FALSE;
		for (valid_zc_list.start(); !valid_zc_list.end();
				valid_zc_list.next()) {

			if (r_rtname) {
				free(r_rtname);
				r_rtname = 0;
			}
			if (full_name) {
				free(full_name);
				full_name = NULL;
			}

			clerrno = get_r_rtname(resourcetypes.getValue(),
						&r_rtname,
						valid_zc_list.getValue());
			if (clerrno != CL_NOERR) {
				// This RT is not present in this cluster.
				continue;
			}
			// This RT is present in this cluster. We have to
			// add this RT to "good_rts" with cluster scoping.
			scconf_err = scconf_add_cluster_scope_to_obj_name(
					r_rtname, valid_zc_list.getValue(),
					&full_name);
			if (scconf_err != SCCONF_NOERR) {
				if (first_err == CL_NOERR) {
					first_err = CL_EINTERNAL;
				}
				continue;
			}

			good_rts.add(full_name);
			has_obj = B_TRUE;
		}

		// Check whether this RT was present in any zone cluster
		// or not.
		if (has_obj == B_FALSE) {
			clerror("Unknown resource type %s\n",
				resourcetypes.getValue());
			if (first_err == CL_NOERR) {
				first_err = CL_ENOENT;
			}
		}
		// Free any memory
		if (r_rtname) {
			free(r_rtname);
			r_rtname = 0;
		}
		if (full_name) {
			free(full_name);
			full_name = NULL;
		}
	}

	if ((resourcetypes.getSize() > 0) && (good_rts.getSize() == 0))
		return (first_err);

	if ((merged_props.getSize() == 0) &&
	    ((properties.getSize() > 0) || (xproperties.getSize() > 0) ||
	    (yproperties.getSize() > 0))) {
		return (first_err);
	}

	// Add Logical Host or Shared Address resource types if requird.
	clerrno = add_lh_or_sa_rts_to_list(good_rts, valid_zc_list, optflgs);
	if (clerrno != CL_NOERR) {
		return (clerrno);
	}

	// Check the validity of RGs
	// If zone clusters were specified, we have to check whether
	// the specified RGs are part of at least one zone cluster.
	// If an RG is not present in at least one zone cluster, then
	// we will not include it in the list of good RGs (good_rgs).
	if (valid_zc_list.getSize() == 0) {
		for (resourcegroups.start(); !resourcegroups.end();
		    resourcegroups.next()) {
			clerrno = check_rg(resourcegroups.getValue(), NULL);
			if (clerrno == 0)
				good_rgs.add(resourcegroups.getValue());
			else {
				if (first_err == 0) {
					// first error!
					first_err = clerrno;
				}
			}
		}
	} else {
		// Zone cluster names were specified.
		for (resourcegroups.start(); !resourcegroups.end();
		    resourcegroups.next()) {
			tmp_list.clear();
#if (SOL_VERSION >= __s10)
			clerrno = get_zone_clusters_with_rg(
					resourcegroups.getValue(), tmp_list,
					valid_zc_list);
			if (clerrno == CL_ENOENT) {
				clerror("Resource group %s does not exist"
					" in any of the specified"
					" zone clusters",
					resourcegroups.getValue());
			}
			if (clerrno != CL_NOERR) {
				if (first_err == CL_NOERR) {
					first_err = clerrno;
				}
				continue;
			}
#endif
			// If we are here, then this RG existed in at least
			// one zone cluster
			for (tmp_list.start(); !tmp_list.end();
				tmp_list.next()) {
				scconf_err =
					scconf_add_cluster_scope_to_obj_name(
					    resourcegroups.getValue(),
					    tmp_list.getValue(),
					    &full_name);
				if (scconf_err != SCCONF_NOERR) {
					if (first_err == CL_NOERR) {
						first_err = CL_EINTERNAL;
					}
				} else {
					good_rgs.add(full_name);
				}
				// Free memory
				if (full_name) {
					free(full_name);
					full_name = NULL;
				}
			}
		}
		// Free memory
		tmp_list.clear();
	}

	if ((resourcegroups.getSize() > 0) && (good_rgs.getSize() == 0))
		return (first_err);

	// By the time we are here, we have verified all the RGs specified
	// by the user.

	if (operands.getSize() > 0) {
		operands.start();
		oper1 = operands.getValue();
	} else
		oper1 = CLCOMMANDS_WILD_OPERAND;

	if (strcmp(oper1, CLCOMMANDS_WILD_OPERAND) == 0) {
		if (good_rgs.getSize() > 0) {
			rg_names = vl_to_names(good_rgs, &errflg);
			if (errflg)
				return (errflg);
		} else {
			if (valid_zc_list.getSize() == 0) {
				// No zone clusters were specified.
				// Get the list of RGs from the current cluster
				scha_status = scswitch_getrglist(&rg_names,
						B_FALSE, NULL);
				rv = scha_status.err_code;
				if (rv != SCHA_ERR_NOERR) {
					clerror("Failed to obtain list of "
					    "all resource groups.\n");
					print_rgm_error(scha_status, NULL);
					return (map_scha_error(rv));
				}
			} else {
				// Zone clusters were specified.
				// Fetch RGs from all the zone clusters.
				for (valid_zc_list.start();
					!valid_zc_list.end();
					valid_zc_list.next()) {
					scha_status =
						scswitch_getrglist(&rg_names,
						    B_FALSE,
						    valid_zc_list.getValue());
					rv = scha_status.err_code;
					if (rv != SCHA_ERR_NOERR) {
						clerror("Failed to obtain list"
						" of all resource groups.\n");
						print_rgm_error(scha_status,
							NULL);
						return (map_scha_error(rv));
					}
					clerrno = names_to_valuelist(rg_names,
								tmp_list);
					if ((clerrno != CL_NOERR) &&
						(first_err == CL_NOERR)) {
						first_err = clerrno;
					}
					// Free memory
					rgm_free_strarray(rg_names);
				}
				// Now, convert to names
				rg_names = vl_to_names(tmp_list, &errflg);
				// Free memory
				tmp_list.clear();
				if (errflg) {
					return (errflg);
				}
			}
		}
	}

	// By now, we definitely should have RG names (whether the user
	// specified them or not). And if there are no RGs we just
	// dont do anything.
	if ((strcmp(oper1, CLCOMMANDS_WILD_OPERAND) == 0) &&
	    (rg_names != (char **)NULL)) {

		// Get the list of resources for this group
		for (i = 0; rg_names[i] != NULL; i++) {
			if (rs_names) {
				rgm_free_strarray(rs_names);
				rs_names = (char **)NULL;
			}
			if (rg) {
				rgm_free_rg(rg);
				rg = (rgm_rg_t *)NULL;
			}
			if (obj_name) {
				free(obj_name);
				obj_name = NULL;
			}
			if (zc_name) {
				free(zc_name);
				zc_name = NULL;
			}
			// First get the RG name and the ZC name
			scconf_err = scconf_parse_obj_name_from_cluster_scope(
					rg_names[i], &obj_name, &zc_name);
			if (scconf_err != SCCONF_NOERR) {
				if (first_err == CL_NOERR) {
					first_err = map_scconf_error(
							scconf_err);
				}
				clcommand_perror(first_err);
				continue;
			}
			// Get the RG conf now.
			scha_status = rgm_scrgadm_getrgconf(obj_name, &rg,
								zc_name);
			rv = scha_status.err_code;
			if (rv != SCHA_ERR_NOERR) {
				print_rgm_error(scha_status, NULL);
				clerror("Failed to obtain resource group "
				    "configuration for \"%s\".\n",
				    rg_names[i]);
				if (first_err == 0) {
					// first error!
					first_err = map_scha_error(rv);
				}
				continue;
			}
			if (rg_nl) {
				delete rg_nl;
				rg_nl = NULL;
			}
			rg_nl = new ValueList;
			for (nl = rg->rg_nodelist; nl; nl = nl->nl_next) {
				nodename = nodeidzone_to_nodename(
				    nl->nl_nodeid, nl->nl_zonename, &errflg,
				    zc_name);
				if (errflg)
					return (errflg);
				rg_nl->add(nodename);
				free(nodename);
			}

			scha_status = rgm_scrgadm_getrsrclist(obj_name,
			    &rs_names, zc_name);
			rv = scha_status.err_code;
			if (rv != SCHA_ERR_NOERR) {
				print_rgm_error(scha_status, NULL);
				clerror("Failed to obtain list of all "
				    "resources in resource group "
				    "\"%s\".\n", rg_names[i]);
				if (first_err == 0) {
					// first error!
					first_err = map_scha_error(rv);
				}
				continue;
			}
			if (rs_names == NULL)
				continue;

			for (j = 0; rs_names[j]; j++) {
				if (rs != NULL) {
					free(rs);
					rs = NULL;
				}
				scha_status = rgm_scrgadm_getrsrcconf(
				    rs_names[j], &rs, zc_name);
				rv = scha_status.err_code;
				if (rv != SCHA_ERR_NOERR) {
					print_rgm_error(scha_status,
					    rs_names[j]);
					if (first_err == 0) {
						// first error!
						first_err = map_scha_error(rv);
					}
					continue;
				}
				if (rs == NULL) {
					clerror("Failed to access resource "
					    "\"%s\".\n", rs_names[j]);
					if (first_err == 0) {
						// first error!
						first_err = CL_ENOENT;
					}
					continue;
				}
				if (good_rts.getSize() > 0)
					if (match_type(good_rts, rs) == 0)
						continue;

				clerrno = show_rs(rs_show, rs, rg, rg_nl,
				    props_specified,
				    merged_props, verbose, zc_name);
				if (clerrno) {
					if (first_err == 0) {
						// first error!
						first_err = clerrno;
					}
					break;
				}
			}
		}
	} else if (strcmp(oper1, CLCOMMANDS_WILD_OPERAND) != 0) {
		// Operands were specified. Zone cluster names can be specified
		// either in "zc_names", or as part of the resource names
		// itself.
		for (operands.start(); operands.end() != 1; operands.next()) {

			if (r_name) {
				free(r_name);
				r_name = NULL;
			}
			if (zc_name) {
				free(zc_name);
				zc_name = NULL;
			}
			tmp_list.clear();

			// We have to check whether zone cluster name
			// was specified in the resource name.
			scconf_err = scconf_parse_obj_name_from_cluster_scope(
					operands.getValue(), &r_name,
					&zc_name);
			if (scconf_err != SCCONF_NOERR) {
				clerrno = map_scconf_error(scconf_err);
				clcommand_perror(clerrno);
				if (first_err == CL_NOERR) {
					first_err = clerrno;
				}
				continue;
			}

			if (zc_names.getSize() > 0) {
#if (SOL_VERSION >= __s10)
				// -Z flag was specified.
				clerrno = get_zone_clusters_with_rs(
						operands.getValue(), tmp_list,
						valid_zc_list);

				if (clerrno != CL_NOERR) {
					clcommand_perror(clerrno);
					if (first_err == CL_NOERR) {
						first_err = clerrno;
					}
					continue;
				}
#endif
			} else {
				// ZC name could have been specified in the
				// resource name.
				tmp_list.add(zc_name);
			}

			// Now filter and print the status of this resource
			// if it satisfies all the filters.
			if (tmp_list.getSize() == 0) {
				// There was no zone cluster specified
				// anywhere.
				clerrno = filter_rs_and_show_it(rs_show,
					r_name, NULL,
					good_rts, good_rgs, props_specified,
					merged_props, verbose, optflgs);
				if (clerrno != CL_NOERR) {
					if (first_err == CL_NOERR) {
						first_err = clerrno;
					}
				}
				continue;
			}
			// If we are here, it means zone clusters were
			// specified.
			for (tmp_list.start(); !tmp_list.end();
				tmp_list.next()) {
				clerrno = filter_rs_and_show_it(rs_show,
					r_name, tmp_list.getValue(),
					good_rts, good_rgs, props_specified,
					merged_props, verbose, optflgs);
				if (clerrno != CL_NOERR) {
					if (first_err == CL_NOERR) {
						first_err = clerrno;
					}
				}

			}

		}
	}

	if (rg_names)
		rgm_free_strarray(rg_names);
	if (rs_names)
		rgm_free_strarray(rs_names);
	if (rs)
		free(rs);
	if (rg_nl)
		delete rg_nl;
	return (first_err);
}

clerrno_t
filter_rs_and_show_it(ClShow *rs_show, char *rs_name, char *zc_name,
    ValueList &good_rts, ValueList &good_rgs, int props_specified,
    NameValueList &merged_props, int verbose, optflgs_t optflgs) {

	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};
	int rv = 0;
	clerrno_t clerrno = CL_NOERR;
	rgm_resource_t *rs = (rgm_resource_t *)NULL;
	ValueList *rg_nl = (ValueList *)NULL;
	rgm_rg_t *rg = (rgm_rg_t *)NULL;
	nodeidlist_t *nl;
	char *nodename = NULL;

	if (!rs_name) {
		return (CL_EINVAL);
	}

	scha_status = rgm_scrgadm_getrsrcconf(rs_name, &rs, zc_name);
	rv = scha_status.err_code;
	if (rv != SCHA_ERR_NOERR) {
		print_rgm_error(scha_status, rs_name);
		clerrno = map_scha_error(rv);
		goto cleanup;
	}
	if (rs == NULL) {
		clerror("Failed to access resource \"%s\".\n");
		clerrno = CL_ENOENT;
		goto cleanup;
	}

	scha_status = rgm_scrgadm_getrgconf(rs->r_rgname, &rg, zc_name);
	rv = scha_status.err_code;
	if (rv != SCHA_ERR_NOERR) {
		print_rgm_error(scha_status, NULL);
		clerror("Failed to obtain resource group "
		    "configuration for \"%s\".\n", rs->r_rgname);
		clerrno = map_scha_error(rv);
		goto cleanup;
	}

	rg_nl = new ValueList;
	for (nl = rg->rg_nodelist; nl; nl = nl->nl_next) {
		nodename = nodeidzone_to_nodename(nl->nl_nodeid,
				nl->nl_zonename, &clerrno, zc_name);
		if (clerrno != CL_NOERR) {
			goto cleanup;
		}

		rg_nl->add(nodename);
		free(nodename);
	}

	if (good_rts.getSize() > 0) {
		if (match_type(good_rts, rs) == 0) {
			if (optflgs & LHflg)
				clerror("Specified resource "
				    "\"%s\" is not a logical "
				    "hostname resource.\n", rs_name);
			else if (optflgs & SAflg)
				clerror("Specified resource "
				    "\"%s\" is not a shared "
				    "address resource.\n", rs_name);
			else
				clerror("Specified resource "
				    "\"%s\" does not meet the "
				    "criteria set by the "
				    "\"-t\" option.\n", rs_name);

			clerrno = CL_EINVAL;
			goto cleanup;
		}
	}

	if (good_rgs.getSize() > 0) {
		if (match_group(good_rgs, rs) == 0) {
			clerror("Specified resource \"%s\" "
			    "does not meet the criteria set "
			    "by the \"-g\" option.\n", rs_name);

			clerrno = CL_EINVAL;
		}
	}
	// Now print the properties of this resource
	clerrno = show_rs(rs_show, rs, rg, rg_nl, props_specified,
			merged_props, verbose, zc_name);

cleanup:
	if (rg_nl) {
		delete rg_nl;
	}
	if (rs) {
		rgm_free_resource(rs);
	}
	if (rg) {
		rgm_free_rg(rg);
	}

	return (clerrno);
}

static clerrno_t
show_rs(ClShow *rs_show, rgm_resource_t *rs, rgm_rg_t *rg, ValueList *rg_nl,
    int props_specified, NameValueList &properties, int verbose, char *zc_name)
{
	char *ptr, *ptr2;
	int rv = 0;
	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};
	rgm_property_list_t *listp;
	ClShow *props_show;
	int node_num;
	scha_switch_t swtch;
	char *prop_node = (char *)NULL;
	clerrno_t clerrno = CL_NOERR;
	boolean_t prop_found;
	boolean_t pn_specified;
	char *pn_string;
	char *r_name = NULL;
	scconf_errno_t scconf_err = SCCONF_NOERR;

	// First fetch the complete name of the resource
	scconf_err = scconf_add_cluster_scope_to_obj_name(rs->r_name, zc_name,
								&r_name);
	if (scconf_err != SCCONF_NOERR) {
		clerrno = map_scconf_error(scconf_err);
		clcommand_perror(clerrno);
		return (clerrno);
	}

	rs_show->addObject((char *)gettext("Resource"), r_name);

	if ((props_specified && (prop_present(properties, "Type") == 1)) ||
	    (!props_specified)) {
		if (rs->r_type) {
			rs_show->addProperty(r_name, (char *)gettext("Type"),
			    rs->r_type);
		} else {
			rs_show->addProperty(r_name, (char *)gettext("Type"),
			    "<NULL>");
		}
	}

	if ((props_specified && (prop_present(properties, "Type_version")
	    == 1)) || (!props_specified)) {
		if (rs->r_type) {
			rs_show->addProperty(r_name,
			    (char *)gettext("Type_version"),
			    rs->r_type_version);
		} else {
			rs_show->addProperty(r_name,
			    (char *)gettext("Type_version"), "<NULL>");
		}
	}

	if ((props_specified && (prop_present(properties, "Group") == 1)) ||
	    (!props_specified)) {
			rs_show->addProperty(r_name, (char *)gettext("Group"),
			    rs->r_rgname);
	}

	if ((props_specified &&
	    (prop_present(properties, "R_description") == 1)) ||
	    (!props_specified)) {
		if (rs->r_description)
			rs_show->addProperty(r_name, "R_description",
			    rs->r_description);
		else
			rs_show->addProperty(r_name, "R_description",
			    "<NULL>");
	}

	ptr2 = NULL;
	ptr = NULL;
	if (rs->r_project_name && *(rs->r_project_name) != '\0')
		ptr = rs->r_project_name;
	else {
		//
		// Resource project name is not set; get the default project
		// name of the containing RG
		//
		scha_status = rgm_scrgadm_get_default_proj(rs->r_rgname,
			&ptr2, NULL);
		if (scha_status.err_code == SCHA_ERR_NOERR)
			ptr = ptr2;
	}

	if ((props_specified &&
	    (prop_present(properties, "Resource_project_name") == 1)) ||
	    (!props_specified)) {
		if (ptr) {
			rs_show->addProperty(r_name,
			    (char *)gettext("Resource_project_name"), ptr);
		} else {
			rs_show->addProperty(r_name,
			    (char *)gettext("Resource_project_name"), "<NULL>");
		}
	}
	if (ptr2)
		free(ptr2);

	pn_specified = B_FALSE;
	prop_found = B_FALSE;
	for (properties.start(); properties.end() != 1; properties.next()) {
		if ((strncasecmp(properties.getName(), "Enabled",
		    strlen("Enabled")) == 0) ||
		    (strncasecmp(properties.getName(), "On_off_switch",
		    strlen("On_off_switch")) == 0)) {
			prop_found = B_TRUE;
			if (strlen(properties.getValue()) > 0) {
				pn_specified = B_TRUE;
				pn_string = properties.getValue();
			}
		}
	}
	if ((props_specified && prop_found) || (!props_specified)) {
		for (rg_nl->start(), node_num = 1; rg_nl->end() != 1;
		    rg_nl->next(), node_num++) {
			if (pn_specified) {
				if (strstr(pn_string, rg_nl->getValue())
				    == NULL) {
					continue;
				}
			}
#if (SOL_VERSION >= __s10)
			scha_status = get_swtch_zc(rs->r_onoff_switch,
			    rg_nl->getValue(), &swtch, zc_name);
#else
			scha_status = get_swtch(rs->r_onoff_switch,
			    rg_nl->getValue(), &swtch);
#endif
			rv = scha_status.err_code;
			if (rv != SCHA_ERR_NOERR) {
				print_rgm_error(scha_status,
				    NULL);
				clerrno = map_scha_error(rv);
				goto cleanup;
			}
			if (prop_node) {
				free(prop_node);
				prop_node = NULL;
			}
			clerrno = get_prop_node((char *)gettext("Enabled"),
			    rg_nl->getValue(), &prop_node);
			if (clerrno)
				goto cleanup;

			switch (swtch) {
			case SCHA_SWITCH_DISABLED:
				rs_show->addProperty(r_name, prop_node,
				    VALUE_STR_FALSE);
				break;

			case SCHA_SWITCH_ENABLED:
				rs_show->addProperty(r_name, prop_node,
				    VALUE_STR_TRUE);
				break;

			default:
				rs_show->addProperty(r_name, prop_node,
				    VALUE_STR_UNKNOWN);
			}
		}
	}

	pn_specified = B_FALSE;
	prop_found = B_FALSE;
	for (properties.start(); properties.end() != 1; properties.next()) {
		if ((strncasecmp(properties.getName(), "Monitored",
		    strlen("Monitored")) == 0) ||
		    (strncasecmp(properties.getName(), "Monitored_switch",
		    strlen("Monitored_switch")) == 0)) {
			prop_found = B_TRUE;
			if (strlen(properties.getValue()) > 0) {
				pn_specified = B_TRUE;
				pn_string = properties.getValue();
			}
		}
	}
	if ((props_specified && prop_found) || (!props_specified)) {
		for (rg_nl->start(), node_num = 1; rg_nl->end() != 1;
		    rg_nl->next(), node_num++) {
			if (pn_specified) {
				if (strstr(pn_string, rg_nl->getValue())
				    == NULL) {
					continue;
				}
			}
#if (SOL_VERSION >= __s10)
			scha_status = get_swtch_zc(rs->r_monitored_switch,
			    rg_nl->getValue(), &swtch, zc_name);
#else
			scha_status = get_swtch(rs->r_monitored_switch,
			    rg_nl->getValue(), &swtch);
#endif
			rv = scha_status.err_code;
			if (rv != SCHA_ERR_NOERR) {
				print_rgm_error(scha_status,
				    NULL);
				clerrno = map_scha_error(rv);
				goto cleanup;
			}
			if (prop_node) {
				free(prop_node);
				prop_node = NULL;
			}
			clerrno = get_prop_node((char *)gettext("Monitored"),
			    rg_nl->getValue(), &prop_node);
			if (clerrno)
				goto cleanup;

			switch (swtch) {
			case SCHA_SWITCH_DISABLED:
				rs_show->addProperty(r_name, prop_node,
				    VALUE_STR_FALSE);
				break;

			case SCHA_SWITCH_ENABLED:
				rs_show->addProperty(r_name, prop_node,
				    VALUE_STR_TRUE);
				break;

			default:
				rs_show->addProperty(r_name, prop_node,
				    VALUE_STR_UNKNOWN);
			}
		}
	}

	if ((props_specified &&
	    (prop_present(properties, "Resource_dependencies") == 1)) ||
	    (verbose && !props_specified)) {
		ptr = NULL;
		if (rs->r_dependencies.dp_strong) {
			ptr = scrgadm_rdep_list_to_string(
			    rs->r_dependencies.dp_strong, "<NULL>", " ");
		}
		if (ptr)
			rs_show->addProperty(r_name,
			    "Resource_dependencies", ptr);
		else
			rs_show->addProperty(r_name,
			    "Resource_dependencies", "<NULL>");
	}

	if ((props_specified && (prop_present(properties,
	    "Resource_dependencies_weak") == 1)) ||
	    (verbose && !props_specified)) {
		ptr = NULL;
		if (rs->r_dependencies.dp_weak) {
			ptr = scrgadm_rdep_list_to_string(
			    rs->r_dependencies.dp_weak, "<NULL>", " ");
		}
		if (ptr)
			rs_show->addProperty(r_name,
			    "Resource_dependencies_weak", ptr);
		else
			rs_show->addProperty(r_name,
			    "Resource_dependencies_weak", "<NULL>");
	}

	if ((props_specified && (prop_present(properties,
	    "Resource_dependencies_restart") == 1)) ||
	    (verbose && !props_specified)) {
		ptr = NULL;
		if (rs->r_dependencies.dp_restart) {
			ptr = scrgadm_rdep_list_to_string(
			    rs->r_dependencies.dp_restart, "<NULL>", " ");
		}
		if (ptr)
			rs_show->addProperty(r_name,
			    "Resource_dependencies_restart", ptr);
		else
			rs_show->addProperty(r_name,
			    "Resource_dependencies_restart", "<NULL>");
	}

	if ((props_specified && (prop_present(properties,
	    "Resource_dependencies_offline_restart") == 1)) ||
	    (verbose && !props_specified)) {
		ptr = NULL;
		if (rs->r_dependencies.dp_offline_restart) {
			ptr = scrgadm_rdep_list_to_string(
			    rs->r_dependencies.dp_offline_restart,
			    "<NULL>", " ");
		}
		if (ptr)
			rs_show->addProperty(r_name,
			    "Resource_dependencies_offline_restart", ptr);
		else
			rs_show->addProperty(r_name,
			    "Resource_dependencies_offline_restart", "<NULL>");
	}

	if (!verbose)
		goto cleanup;

	props_show = new ClShow(SUBHEAD_RSPROPS);

	for (listp = rs->r_ext_properties; listp;  listp = listp->rpl_next) {
		clerrno = show_rs_property(props_show, rs, rg, rg_nl,
		    listp->rpl_property, props_specified, properties, verbose,
		    B_TRUE);
		if (clerrno)
			goto cleanup;
	}

	for (listp = rs->r_properties; listp;  listp = listp->rpl_next) {
		clerrno = show_rs_property(props_show, rs, rg, rg_nl,
		    listp->rpl_property, props_specified, properties, verbose,
		    B_FALSE);
		if (clerrno)
			goto cleanup;
	}

	rs_show->addChild(r_name, (ClShow *)props_show);

cleanup:
	if (prop_node)
		free(prop_node);
	if (r_name)
		free(r_name);
	return (clerrno);
}

static clerrno_t
show_rs_property(ClShow *props_show, rgm_resource_t *rs, rgm_rg_t *rg,
    ValueList *rg_nl, rgm_property_t *rp, int props_specified,
    NameValueList &properties, int verbose, boolean_t extension)
{
	char *ptr, *ptr2;
	int errflg = 0;
	int rv = 0;
	char *value = NULL;
	char *typestr = (char *)NULL;
	char *prop_node = (char *)NULL;
	int node_num;
	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};
	clerrno_t clerrno = CL_NOERR;
	boolean_t prop_found;
	boolean_t pn_specified;
	char *pn_string;

	// Check arguments
	if (rp == NULL || rp->rp_key == NULL || *rp->rp_key == NULL)
		return (0);

	pn_specified = B_FALSE;
	prop_found = B_FALSE;
	for (properties.start(); properties.end() != 1; properties.next()) {
		if ((strncasecmp(properties.getName(), rp->rp_key,
		    strlen(rp->rp_key)) == 0) ||
		    (strncasecmp(properties.getName(), rp->rp_key,
		    strlen(rp->rp_key)) == 0)) {
			prop_found = B_TRUE;
			if (strlen(properties.getValue()) > 0) {
				pn_specified = B_TRUE;
				pn_string = properties.getValue();
			}
		}
	}
	if ((props_specified && prop_found) || (verbose && !props_specified)) {

		switch (rp->rp_type) {
		case SCHA_PTYPE_STRING:
			if (!typestr)
				typestr = "string";
		case SCHA_PTYPE_INT:
			if (!typestr)
				typestr = "int";
		case SCHA_PTYPE_BOOLEAN:
			if (!typestr)
				typestr = "boolean";
		case SCHA_PTYPE_ENUM:
			if (!typestr)
				typestr = "enum";

			if (rp->is_per_node) {
				for (rg_nl->start(), node_num = 1;
				    rg_nl->end() != 1;
				    rg_nl->next(), node_num++) {
					if (value) {
						free(value);
						value = NULL;
					}
					if (pn_specified) {
						if (strstr(pn_string,
						    rg_nl->getValue()) ==
						    NULL) {
							continue;
						}
					}
					scha_status = get_value(rp->rp_value,
					    rg_nl->getValue(),
					    &value, B_TRUE);
					rv = scha_status.err_code;
					if (rv != SCHA_ERR_NOERR) {
						print_rgm_error(scha_status,
						    NULL);
						clerrno = map_scha_error(rv);
						goto cleanup;
					}
					if (prop_node) {
						free(prop_node);
						prop_node = NULL;
					}
					rv = get_prop_node(rp->rp_key,
					    rg_nl->getValue(), &prop_node);
					props_show->addNumberedObject(prop_node,
					    value, node_num);
				}
			} else {
				if (value) {
					free(value);
					value = NULL;
				}
				scha_status = get_value(rp->rp_value, NULL,
				    &value, B_FALSE);
				rv = scha_status.err_code;
				if (rv != SCHA_ERR_NOERR) {
					print_rgm_error(scha_status, NULL);
					clerrno = map_scha_error(rv);
					goto cleanup;
				}
				if (prop_node) {
					free(prop_node);
					prop_node = NULL;
				}
				prop_node = strdup(rp->rp_key);
				if (prop_node == NULL) {
					clcommand_perror(CL_ENOMEM);
					clerrno = CL_ENOMEM;
					goto cleanup;
				}
				props_show->addObject(prop_node, value);
			}

			if (extension)
				props_show->addRSProperty(prop_node,
				    (char *)gettext("Class"), "extension");
			else
				props_show->addRSProperty(prop_node,
				    (char *)gettext("Class"), "standard");
			props_show->addRSProperty(prop_node,
			    (char *)gettext("Description"), rp->rp_description);
			if (extension)
				props_show->addRSProperty(prop_node,
				    (char *)gettext("Per-node"),
				    (rp->is_per_node) ?
				    VALUE_STR_TRUE : VALUE_STR_FALSE);
			props_show->addRSProperty(prop_node,
			    (char *)gettext("Type"), typestr);
			break;

		case SCHA_PTYPE_STRINGARRAY:

			typestr = "stringarray";
			ptr2 = NULL;
			//
			// If NRU is not set then compute the NRU from the
			// dependency list and display it
			//
			if ((strcasecmp(SCHA_NETWORK_RESOURCES_USED,
			    rp->rp_key) == 0) &&
			    (rp->rp_array_values == NULL))
				compute_NRUlist_frm_deplist(rs,
				    &(rp->rp_array_values), NULL);
			if (rp->rp_array_values) {
				ptr2 = names_to_str(rp->rp_array_values,
				    &errflg);
				ptr = (ptr2) ? ptr2 : "<NULL>";
			} else
				ptr = "<NULL>";
			props_show->addObject(rp->rp_key, ptr);
			if (extension)
				props_show->addRSProperty(rp->rp_key,
				    (char *)gettext("Class"), "extension");
			else
				props_show->addRSProperty(rp->rp_key,
				    (char *)gettext("Class"), "standard");
			props_show->addRSProperty(rp->rp_key,
			    (char *)gettext("Description"),
			    rp->rp_description);
			if (extension)
				props_show->addRSProperty(rp->rp_key,
				    (char *)gettext("Per-node"),
				    (rp->is_per_node) ?
				    VALUE_STR_TRUE : VALUE_STR_FALSE);
			props_show->addRSProperty(rp->rp_key,
			    (char *)gettext("Type"), typestr);
			if (ptr2)
				free(ptr2);
			break;

		default:
			props_show->addObject(rp->rp_key, "Unknown");
			break;
		}
	}

cleanup:
	if (value)
		free(value);
	if (prop_node)
		free(prop_node);
	return (clerrno);
}

//
// get_prop_node: Provided a property name and nodename, allocate space
// and create a pernode property string.
//
clerrno_t
get_prop_node(char *propname, char *nodename, char **prop_node)
{
	char *tmp_name;

	tmp_name = (char *)calloc(1, strlen(propname) +
	    strlen(nodename) + 5);
	if (tmp_name == NULL) {
		clcommand_perror(CL_ENOMEM);
		return (CL_ENOMEM);
	}
	strcat(tmp_name, propname);
	strcat(tmp_name, "{");
	strcat(tmp_name, nodename);
	strcat(tmp_name, "}");
	*prop_node = tmp_name;
	return (CL_NOERR);
}

//
// Go through all the known properties, and if it has been specified,
// create a merge list that has all the necessary nodes in the value field.
//
clerrno_t
merge_prop(NameValueList &props_needed, NameValueList &merged_props,
    NameValueList &proplist)
{
	int nodelen = 10;
	char *node_buf = (char *)NULL;
	boolean_t all_node_found;
	boolean_t prop_found;
	int clerrno = CL_NOERR;

	if (props_needed.getSize() == 0) {
		return (clerrno);
	}

	for (props_needed.start(); props_needed.end() != 1;
	    props_needed.next()) {
		nodelen += strlen(props_needed.getValue());
	}

	node_buf = (char *)calloc(1, nodelen);
	if (!node_buf) {
		clcommand_perror(CL_ENOMEM);
		return (CL_ENOMEM);
	}

	for (proplist.start(); proplist.end() != 1; proplist.next()) {
		strcpy(node_buf, "");
		all_node_found = B_FALSE;
		prop_found = B_FALSE;
		for (props_needed.start(); props_needed.end() != 1;
		    props_needed.next()) {
			if (strcasecmp(proplist.getName(),
			    props_needed.getName()) == 0) {
				prop_found = B_TRUE;
				if (strlen(props_needed.getValue()) == 0) {
					merged_props.add(props_needed.getName(),
					    "");
					all_node_found = B_TRUE;
					break;
				}
				strcat(node_buf, props_needed.getValue());
			}
		}
		if (!all_node_found && prop_found) {
			merged_props.add(proplist.getName(), node_buf);
		}
	}
	free(node_buf);
	return (clerrno);
}

int
prop_present(NameValueList &properties, char *prop)
{
	for (properties.start(); properties.end() != 1; properties.next()) {
		if (strcasecmp(properties.getName(), prop) == 0) {
			return (1);
		}
	}
	return (0);
}
