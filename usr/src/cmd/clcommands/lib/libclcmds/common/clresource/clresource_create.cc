//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clresource_create.cc	1.13	08/10/07 SMI"

//
// Process clresource "create"
//

#ifdef __cplusplus
#include <sys/os.h>
#endif

#include <scadmin/scswitch.h>
#include <scadmin/scrgadm.h>
#include <scadmin/scconf.h>
#include <scadmin/scstat.h>
#include <rgm/rgm_scrgadm.h>
#include <rgm/rgm_cnfg.h>
#include "clcommands.h"
#include "ClCommand.h"
#include "ClXml.h"

#include "clresource.h"
#include "clresourcegroup.h"
#include "clresourcetype.h"
#include "rgm_support.h"
#include "common.h"

#define	RSNEW 0
#define	RSCREATED 1
#define	RSFAILED 2

typedef struct rs_state {
	char *rsname;
	int rsstate;
} rs_state_t;

static clerrno_t
filter_ops(ClXml *xml, char *rsname, ValueList &rt_list, ValueList &rg_list);

static clerrno_t
add_dependents(ClXml *xml, ValueList &rs_list, boolean_t *list_changed,
    NameValueList &properties, NameValueList &yproperties);

static clerrno_t
get_rsnames_from_string(char *rs_string, ValueList &rsn_list);

static clerrno_t
check_lhsa_nodes(char *rgname, optflgs_t optflgs, char *zc_name);

static clerrno_t
set_readiness(ClXml *xml, rs_state_t *rs_state_list, char **curr_rs,
    int *rs_index, NameValueList &proplist1, NameValueList &proplist2);

static int
prop_defined(NameValueList &proplist, char *propname);

//
// clresource "create"
//
clerrno_t
clresource_create(ClCommand &cmd, ValueList &operands, optflgs_t optflgs,
    ValueList &resourcegroups, ValueList &resourcetypes,
    NameValueList &properties, NameValueList &xproperties,
    NameValueList &yproperties, ValueList &lhosts, ValueList &netiflist,
    ValueList &auxnodes)
{
	clerrno_t first_err = CL_NOERR;
	clerrno_t clerrno = CL_NOERR;
	int rv = 0;
	int len = 0;
	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};
	scha_property_t *xproplist = NULL;
	scha_property_t *yproplist = NULL;
	scha_resource_properties_t rsprops;
	boolean_t bypass_install_mode = B_FALSE; /* private for upgrade */
	clerrno_t errflg = CL_NOERR;
	char *r_rtname = 0;
	NameValueList proplist;
	boolean_t verbose;
	int hprop_specified = 0;
	NameValueList pn_properties = NameValueList(true);
	NameValueList pn_xproperties = NameValueList(true);
	rgm_resource_t *rs = (rgm_resource_t *)NULL;
	rg_ip_type_t iptyp;
	boolean_t gz_ov_found;
	char *zc_name = NULL;
	boolean_t is_same = B_FALSE;
	scconf_errno_t scconf_err = SCCONF_NOERR;
	char *res_part, *zc_part;

	// Initialize ORB and other cluster checks
	res_part = zc_part = NULL;
	clerrno = cluster_init();
	if (clerrno)
		return (clerrno);

	if (optflgs & vflg)
		verbose = B_TRUE;
	else
		verbose = B_FALSE;

	// Check whether any zone cluster names were specified.
	// And if they were specified, there should be only zone cluster
	// across all the RGs.
	clerrno = check_single_zc_specified(operands, &zc_name, &is_same);
	if (clerrno != CL_NOERR) {
		clcommand_perror(clerrno);
		return (clerrno);
	}
	// If multiple zone clusters were specified, report an error.
	if (is_same == B_FALSE) {
		clerror("This operation can be performed on only one "
		    "zone cluster at a time.\n");
		return (clerrno);
	}
#if (SOL_VERSION >= __s10)
	if (zc_name) {
		// If zone cluster name was specified, we have to ensure
		// that it is valid.
		// We have to check whether this operation is allowed for
		// the status of the zone cluster.
		clerrno = check_zc_operation(CL_OBJ_TYPE_RS, CL_OP_TYPE_CREATE,
						zc_name);
		if (clerrno != CL_NOERR) {
			return (clerrno);
		}
	}
#endif

	if (optflgs & LHflg)
		resourcetypes.add(get_latest_rtname(SCRGADM_RTLH_NAME,
							zc_name));

	if (optflgs & SAflg)
		resourcetypes.add(get_latest_rtname(SCRGADM_RTSA_NAME,
							zc_name));

	// Check resourcetype
	for (resourcetypes.start(); !resourcetypes.end();
	    resourcetypes.next()) {
		if (r_rtname) {
			free(r_rtname);
			r_rtname = 0;
		}

		clerrno = get_r_rtname(resourcetypes.getValue(), &r_rtname,
					zc_name);
		if (clerrno)
			return (CL_ETYPE);
	}

	for (resourcegroups.start(); !resourcegroups.end();
	    resourcegroups.next()) {
		clerrno = check_rg(resourcegroups.getValue(), zc_name);
		if (clerrno)
			return (clerrno);
	}

	// Take care of the undocumented force flag
	if (optflgs & Fflg)
		bypass_install_mode = B_TRUE;

	operands.start();
	resourcegroups.start();

	// Make sure yproperties don't have any per-node elements
	clerrno = verify_yprops(yproperties);
	if (clerrno)
		return (clerrno);

	//
	// Break-up per-node properties into prop{node} form, also detect
	// invalid nodes.
	//
	clerrno = process_props(properties, pn_properties, r_rtname, zc_name);
	if (clerrno)
		return (clerrno);
	clerrno = process_props(xproperties, pn_xproperties, r_rtname, zc_name);
	if (clerrno)
		return (clerrno);

	// Move properties to either xproperties or yproperties
	clerrno = set_rs_props(NULL, r_rtname, pn_properties, pn_xproperties,
	    yproperties, zc_name);
	if (clerrno)
		return (clerrno);

	// Get the zone cluster name and the RG name
	scconf_err = scconf_parse_obj_name_from_cluster_scope(
			operands.getValue(), &res_part, &zc_part);
	// Return if there was an error.
	if (scconf_err != SCCONF_NOERR) {
		clerror("Internal error.\n");
		return (CL_EINTERNAL);
	}

	if ((optflgs & LHflg) || (optflgs & SAflg)) {
		// Do nodename validation for historical reasons
		clerrno = check_lhsa_nodes(resourcegroups.getValue(),
		    optflgs, zc_name);
		if (clerrno) {
			return (clerrno);
		}

		// Check ip-type conflict
		if (optflgs & LHflg) {
			iptyp = get_ip_type(resourcegroups.getValue(), zc_name);
			if (iptyp == IP_TYPE_MIX) {
				clerror("The resource group nodelist for the "
				    "logical hostname resource \"%s\" cannot "
				    "contain a mix of non-global zones of "
				    "ip-type=exclusive with either non-global "
				    "zones of ip-type=shared or "
				    "global zones.\n", operands.getValue());
				return (CL_EINVAL);
			} else if (iptyp == IP_TYPE_UNKNOWN) {
				clwarning("Failed to verify that the resource "
				    "group nodelist for the logical hostname "
				    "resource \"%s\" does not contain a mix "
				    "of non-global zones of "
				    "ip-type=exclusive with either non-global "
				    "zones of ip-type=shared or global "
				    "zones.\n", operands.getValue());
			}
		} else if (optflgs & SAflg) {
			iptyp = get_ip_type(resourcegroups.getValue(), zc_name);
			if (iptyp == IP_TYPE_UNKNOWN) {
				clwarning("Failed to verify that the resource "
				    "group nodelist for the shared address "
				    "resource \"%s\" does not contain any "
				    "non-global zone of ip-type=exclusive.\n",
				    operands.getValue());
			} else if (iptyp != IP_TYPE_SHARED) {
				clerror("The resource group nodelist for the "
				    "shared address resource \"%s\" cannot "
				    "contain any non-global zone of "
				    "ip-type=exclusive.\n",
				    operands.getValue());
				return (CL_EINVAL);
			}
		}

		//
		// If ip-type exclusive, make sure global_zone_override is
		// set to false for LH resources.
		//
		if ((optflgs & LHflg) && (iptyp == IP_TYPE_EXCL)) {
			gz_ov_found = B_FALSE;
			for (yproperties.start(); !yproperties.end();
			    yproperties.next()) {
				if (strcasecmp(yproperties.getName(),
				    "Global_zone_override") == 0) {
					gz_ov_found = B_TRUE;
					break;
				}
			}
			if (!gz_ov_found)
				yproperties.add("Global_zone_override",
				    "false");
		}

		// Add lhosts and NetIfList properties
		if (lhosts.getSize() == 0) {
			hprop_specified = 0;
			for (pn_xproperties.start(); !pn_xproperties.end();
			    pn_xproperties.next()) {
				if (strcasecmp(pn_xproperties.getName(),
				    SCRGADM_HOSTNAMELIST) == 0) {
					hprop_specified = 1;
					break;
				}
			}
			if (hprop_specified == 0)
				lhosts.add(res_part);
		}
		clerrno = append_lh_props(resourcegroups.getValue(),
		    pn_xproperties,
		    lhosts, netiflist, auxnodes, zc_name);
		if (clerrno) {
			return (clerrno);
		}
	}
	xproplist = nvl_to_prop_array(pn_xproperties, NULL, &errflg,
	    CL_TYPE_RS, r_rtname, zc_name);
	if (errflg) {
		return (errflg);
	}
	yproplist = nvl_to_prop_array(yproperties, NULL, &errflg, CL_TYPE_RS,
	    r_rtname, zc_name);
	if (errflg) {
		return (errflg);
	}
	rsprops.srp_predefined = &yproplist;
	rsprops.srp_extension = &xproplist;

	// Check that the resource doesn't exist
	scha_status = rgm_scrgadm_getrsrcconf(res_part, &rs, zc_part);
	rgm_free_resource(rs);
	if (scha_status.err_code == SCHA_ERR_NOERR) {
		clerror("Failed to create \"%s\", the resource already "
		    "exists.\n", operands.getValue());
		return (CL_EEXIST);
	}

	scha_status = rgm_scrgadm_add_resource(res_part,
	    r_rtname, resourcegroups.getValue(), &rsprops,
	    bypass_install_mode, zc_part);
	rv = filter_warning(scha_status.err_code);

	if (rv) {
		print_rgm_error(scha_status, NULL);
		clerror("Failed to create resource \"%s\".\n",
		    operands.getValue());
		if (first_err == 0) {
			// first error!
			first_err = map_scha_error(rv);
		}
	} else {
		if (scha_status.err_msg) {
			fprintf(stdout, "%s\n", scha_status.err_msg);
		}
		if (optflgs & vflg) {
			clmessage("Resource \"%s\" created.\n",
			    operands.getValue());
		}
	}

	if (!(optflgs & dflg) && (!first_err)) {
		scha_status = scswitch_set_resource_switch(res_part,
		    SCHA_SWITCH_ENABLED, B_FALSE, NULL, verbose, zc_part);
		rv = scha_status.err_code;
		if (rv) {
			print_rgm_error(scha_status, operands.getValue());
			if (first_err == 0) {
				// first error!
				first_err = map_scha_error(rv);
			}
		} else if (scha_status.err_msg) {
			fprintf(stdout, "%s\n", scha_status.err_msg);
		}
	}

	if (r_rtname)
		free(r_rtname);
	if (res_part) {
		free(res_part);
		res_part = NULL;
	}
	if (zc_part) {
		free(zc_part);
		zc_part = NULL;
	}
	if (zc_name) {
		free(zc_name);
		zc_name = NULL;
	}
	free_prop_array(xproplist);
	free_prop_array(yproplist);
	return (first_err);
}

clerrno_t
clresource_create(ClCommand &cmd, ValueList &operands, optflgs_t optflgs,
    char *clconfiguration, ValueList &resourcegroups, ValueList &resourcetypes,
    NameValueList &properties, NameValueList &xproperties,
    NameValueList &yproperties, ValueList &lhosts, ValueList &netiflist,
    ValueList &auxnodes)
{
	clerrno_t clerrno = CL_NOERR;
	clerrno_t first_err = CL_NOERR;
	clerrno_t errflg = CL_NOERR;
	int rv = 0;
	ClXml xml;
	ValueList allproplist;
	ValueList created_rss;
	ValueList good_nodes;
	ValueList *expanded_list = new ValueList();
	ValueList rs_list;
	int prop_found = 0;
	char *oper1 = (char *)NULL;
	NameValueList pn_properties = NameValueList(true);
	NameValueList pn_xproperties = NameValueList(true);
	NameValueList r_yproperties = NameValueList(true);
	boolean_t rs_found;
	ValueList empty_list;
	NameValueList empty_nv_list;
	ValueList rg_list;
	ValueList rt_list;
	ValueList rg_op;
	ValueList rt_op;
	rgm_rg_t *rg = (rgm_rg_t *)0;
	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};
	char *r_rtname = (char *)NULL;
	char *rt_xml = (char *)NULL;
	char *rg_xml = (char *)NULL;
	char *check_rtname = (char *)NULL;
	char full_rtname_buf[MAXCCRTBLNMLEN];
	char name_buf[PATH_MAX];
	NameValueList type_file_pair;
	int hprop_specified = 0;
	boolean_t list_changed;
	int num_rs;
	rs_state_t *rs_state_list = NULL;
	int i;
	char *curr_rs = NULL;
	int rs_index;

	if (!clconfiguration) {
		clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}

	OptionValues *ov = new OptionValues((char *)"add", cmd.argc, cmd.argv);

	//
	// Check resourcetype, make sure user specified format matches those
	// in the xmlfile.
	//
	if (optflgs & LHflg)
		resourcetypes.add(get_latest_rtname(SCRGADM_RTLH_NAME,
							NULL));

	if (optflgs & SAflg)
		resourcetypes.add(get_latest_rtname(SCRGADM_RTSA_NAME,
							NULL));

	if (resourcetypes.getSize() > 0) {
		clerrno = xml.getAllObjectNames(CLRESOURCETYPE_CMD,
		    clconfiguration, expanded_list);
		if (clerrno != CL_NOERR) {
			return (clerrno);
		}

		for (expanded_list->start(); expanded_list->end() != 1;
		    expanded_list->next()) {
			type_file_pair.add(expanded_list->getValue(),
			    "xmlfile");
		}

		for (resourcetypes.start(); resourcetypes.end() != 1;
		    resourcetypes.next()) {
			rv = find_rt_in_list(type_file_pair,
			    resourcetypes.getValue(), name_buf, full_rtname_buf,
			    &errflg);
			if (errflg)
				return (errflg);

			if (rv > 1) {
				clerror("Multiple instances of \"%s\" found in "
				    "\"%s\", can not create resources of "
				    "type \"%s\".\n", resourcetypes.getValue(),
				    clconfiguration, resourcetypes.getValue());
				if (first_err == 0) {
					// first error!
					first_err = CL_ETYPE;
				}
				continue;
			}
			if (rv == 0) {
				clerror("Cannot find \"%s\" in \"%s\".\n",
				    resourcetypes.getValue(), clconfiguration);
				if (first_err == 0) {
					// first error!
					first_err = CL_ENOENT;
				}
				continue;
			}
			rt_list.add(full_rtname_buf);
		}
		if (rt_list.getSize() == 0) {
			return (first_err);
		}
	}

	ov->addOptions(CLRES_TYPE, &rt_list);
	ov->addOptions(CLRES_GROUP, &resourcegroups);

	// Make sure yproperties don't have any per-node elements
	clerrno = verify_yprops(yproperties);
	if (clerrno)
		return (clerrno);

	// verbose?
	ov->optflgs = 0;
	if (optflgs & vflg) {
		ov->optflgs |= vflg;
	}

	if (operands.getSize() == 0)
		operands.add((char *)CLCOMMANDS_WILD_OPERAND);

	expanded_list->clear();
	clerrno = xml.getAllObjectNames(CLRESOURCE_CMD, clconfiguration,
	    expanded_list);
	if (clerrno != CL_NOERR) {
		return (clerrno);
	}
	if (expanded_list->getSize() == 0) {
		clerror("Cannot find any resource in \"%s\".\n",
		    clconfiguration);
		return (CL_EINVAL);
	}

	operands.start();
	oper1 = operands.getValue();
	if (strcmp(oper1, (char *)CLCOMMANDS_WILD_OPERAND) == 0) {
		for (expanded_list->start(); expanded_list->end() != 1;
		    expanded_list->next()) {
			clerrno = filter_ops(&xml, expanded_list->getValue(),
			    rt_list, resourcegroups);
			if (clerrno == CL_NOERR) {
				rs_list.add(expanded_list->getValue());
			}
		}
	} else {
		for (operands.start(); operands.end() != 1; operands.next()) {
			rs_found = B_FALSE;
			for (expanded_list->start(); expanded_list->end() != 1;
			    expanded_list->next()) {
				if (strcmp(expanded_list->getValue(),
				    operands.getValue()) == 0) {
					rs_found = B_TRUE;
					break;
				}
			}
			if (rs_found) {
				clerrno = filter_ops(&xml, operands.getValue(),
				    rt_list, resourcegroups);
				if (clerrno == CL_NOERR) {
					rs_list.add(operands.getValue());
				} else {
					if (optflgs & LHflg) {
						clerror("Specified resource "
						    "\"%s\" does not meet the "
						    "criteria set by the "
						    "\"-g\" "
						    "option, or is not defined "
						    "as a logical host "
						    "resource.\n",
						    operands.getValue());
					} else if (optflgs & SAflg) {
						clerror("Specified resource "
						    "\"%s\" does not meet the "
						    "criteria set by the "
						    "\"-g\" "
						    "option, or is not defined "
						    "as a shared address "
						    "resource.\n",
						    operands.getValue());
					} else {
						clerror("Specified resource "
						    "\"%s\" does not meet the "
						    "criteria set by the "
						    "\"-t\" "
						    "or \"-g\" option.\n",
						    operands.getValue());
					}
					if (first_err == 0) {
						// first error!
						first_err = clerrno;
					}
				}
			} else {
				clerror("Cannot find \"%s\" in \"%s\".\n",
				    operands.getValue(), clconfiguration);
				if (first_err == 0) {
					// first error!
					first_err = CL_ENOENT;
				}
			}
		}
	}

	if (optflgs & aflg) {
		list_changed = B_TRUE;
		while (list_changed == B_TRUE) {
			list_changed = B_FALSE;
			clerrno = add_dependents(&xml, rs_list, &list_changed,
			    properties, yproperties);
			if (clerrno) {
				clerror("Failed to find dependendents of "
				    "resources.\n");
				return (clerrno);
			}
		}
	}

	if (optflgs & aflg) {
		clerrno = xml.getAllRGsFromResources(rs_list, rg_list);
		if (clerrno != CL_NOERR) {
			clerror("Failed to obtain the list of resource groups "
			    "from the specified configuration file.\n");
			return (CL_EINVAL);
		}
		rg_op.clear();
		for (rg_list.start(); rg_list.end() != 1; rg_list.next()) {
			if (rg != (rgm_rg_t *)0) {
				rgm_free_rg(rg);
				rg = (rgm_rg_t *)0;
			}
			scha_status = rgm_scrgadm_getrgconf(rg_list.getValue(),
			    &rg, NULL);
			if (scha_status.err_code != SCHA_ERR_NOERR) {
				rg_op.add(rg_list.getValue());
			}
		}
		if (rg_op.getSize() > 0) {
			clerrno = clrg_create(cmd, rg_op, optflgs,
			    clconfiguration, empty_list, empty_nv_list);
			if (clerrno != CL_NOERR) {
				clerror("Failed to create one or more resource "
				    "groups, will fail to create resources in "
				    "these.\n");
				if (first_err == 0) {
					// first error!
					first_err = clerrno;
				}
			}
		}
	}

	num_rs = rs_list.getSize();
	rs_state_list = (rs_state_t *)calloc(num_rs + 1, sizeof (rs_state_t));
	if (rs_state_list == NULL) {
		clcommand_perror(CL_ENOMEM);
		first_err = CL_ENOMEM;
		goto cleanup;
	}

	for (rs_list.start(), i = 0; rs_list.end() != 1; rs_list.next(), i++) {
		rs_state_list[i].rsname = strdup(rs_list.getValue());
		if (rs_state_list[i].rsname == NULL) {
			clcommand_perror(CL_ENOMEM);
			first_err = CL_ENOMEM;
			goto cleanup;
		}
		rs_state_list[i].rsstate = RSNEW;
	}

	while (num_rs > 0) {
		if (rt_xml) {
			free(rt_xml);
			rt_xml = (char *)NULL;
		}

		clerrno = set_readiness(&xml, rs_state_list, &curr_rs,
		    &rs_index, properties, yproperties);
		if (clerrno != CL_NOERR) {
			if (first_err == 0) {
				// first error!
				first_err = clerrno;
			}
			break;
		}
		num_rs --;
		clerrno = xml.getRTFromResource(curr_rs, &rt_xml);
		if (clerrno != CL_NOERR) {
			clerror("Failed to obtain the resource type for \"%s\" "
			    "from the specified configuration file.\n",
			    curr_rs);
			if (first_err == 0) {
				// first error!
				first_err = clerrno;
			}
			rs_state_list[rs_index].rsstate = RSFAILED;
			continue;
		}
		if (r_rtname) {
			free(r_rtname);
			r_rtname = 0;
		}
		scha_status = rgmcnfg_get_rtrealname(rt_xml, &r_rtname, NULL);
		if (scha_status.err_code == SCHA_ERR_RT) {
			if (optflgs & aflg) {
				rt_op.clear();
				rt_op.add(rt_xml);
				clerrno = clrt_register(cmd, rt_op, optflgs,
				    clconfiguration, empty_list, (char *)NULL,
				    empty_nv_list);
				if (clerrno != CL_NOERR) {
					clerror("Failed to register resource "
					    "type \"%s\", will not create "
					    "\"%s\".\n", rt_xml, curr_rs);
					if (first_err == 0) {
						// first error!
						first_err = clerrno;
					}
					rs_state_list[rs_index].rsstate =
					    RSFAILED;
					continue;
				}
			} else {
				clerror("Resource type \"%s\" is not "
				    "registered, "
				    "will not create \"%s\".\n",
				    rt_xml, curr_rs);
				if (first_err == 0) {
					// first error!
					first_err = clerrno;
				}
				rs_state_list[rs_index].rsstate = RSFAILED;
				continue;
			}
		}
		scha_status = rgmcnfg_get_rtrealname(rt_xml, &r_rtname, NULL);
		if (scha_status.err_code != SCHA_ERR_NOERR) {
			clerror("Resource type \"%s\" is not registered, "
			    "will not create \"%s\".\n",
			    rt_xml, curr_rs);
			if (first_err == 0) {
				// first error!
				first_err = clerrno;
			}
			rs_state_list[rs_index].rsstate = RSFAILED;
			continue;
		}
		if (rg_xml) {
			free(rg_xml);
			rg_xml = (char *)NULL;
		}
		clerrno = xml.getRGFromResource(curr_rs, &rg_xml);
		if (clerrno != CL_NOERR) {
			clerror("Failed to obtain the resource group for "
			    "\"%s\" "
			    "from the specified configuration file.\n",
			    curr_rs);
			if (first_err == 0) {
				// first error!
				first_err = clerrno;
			}
			rs_state_list[rs_index].rsstate = RSFAILED;
			continue;
		}

		// Now validate and process properties based on RT
		pn_properties.clear();
		pn_xproperties.clear();
		r_yproperties.clear();
		for (yproperties.start(); !yproperties.end();
		    yproperties.next()) {
			r_yproperties.add(yproperties.getValue());
		}
		//
		// Break-up per-node properties into prop{node} form, also
		// detect invalid nodes.
		//
		clerrno = process_props(properties, pn_properties, r_rtname);
		if (clerrno)
			return (clerrno);
		clerrno = process_props(xproperties, pn_xproperties, r_rtname);
		if (clerrno)
			return (clerrno);

		// Move properties to either xproperties or yproperties
		clerrno = set_rs_props(NULL, r_rtname, pn_properties,
		    pn_xproperties, r_yproperties);
		if (clerrno)
			return (clerrno);

		if ((optflgs & LHflg) || (optflgs & SAflg)) {
			// Do nodename validation for historical reasons
			clerrno = check_lhsa_nodes(rg_xml, optflgs, NULL);
			if (clerrno) {
				if (first_err == 0) {
					// first error!
					first_err = clerrno;
				}
				rs_state_list[rs_index].rsstate = RSFAILED;
				continue;
			}
		}

		// Properties
		ov->addNVOptions(CLRES_PROP, &r_yproperties);
		ov->addNVOptions(CLRES_EXT_PROP, &pn_xproperties);

		ov->setOperand(curr_rs);
		xml.reset_error();
		clerrno = xml.applyConfig(CLRESOURCE_CMD, clconfiguration,
		    ov);
		if (clerrno) {
			if (first_err == 0) {
				// first error!
				first_err = clerrno;
			}
			rs_state_list[rs_index].rsstate = RSFAILED;
		} else {
			created_rss.add(curr_rs);
			rs_state_list[rs_index].rsstate = RSCREATED;
		}
	}

	for (created_rss.start(); !created_rss.end(); created_rss.next()) {
		clerrno = xml.RSPostProcessor(created_rss.getValue(),
		    yproperties, optflgs);
		if (clerrno) {
			if (first_err == 0) {
				// first error!
				first_err = clerrno;
			}
		}
	}

	//
	// Enable all the resources created, we can't respect the enabled state
	// in the xml.
	//
	if (!(optflgs & dflg) && (created_rss.getSize() > 0)) {
		clerrno = clresource_enable(cmd, created_rss, optflgs,
		    empty_list, empty_list, empty_list);
		if (first_err == 0) {
			// first error!
			first_err = clerrno;
		}
	}

cleanup:
	delete ov;
	delete expanded_list;
	if (r_rtname)
		free(r_rtname);
	if (rg != (rgm_rg_t *)0)
		rgm_free_rg(rg);
	if (rt_xml)
		free(rt_xml);
	if (check_rtname)
		free(check_rtname);
	if (r_rtname)
		free(r_rtname);

	return (first_err);
}

//
// For the resources in the rs_state_list, see if there is a resource that
// is ready to be created now (has dependees already present). Return the
// information about the ready resource. If no ready resource is found, we
// abort further resource creations with a suitable message.
//
// Ignore xml dependencies if the corresponding dependency is provided to
// the command line through any of the property lists.
//
static clerrno_t
set_readiness(ClXml *xml, rs_state_t *rs_state_list, char **curr_rs,
    int *rs_index, NameValueList &proplist1, NameValueList &proplist2)
{
	NameValueList xml_deplist;
	ValueList rsn_list;
	int idx = 0;
	rs_state_t *rsptr;
	clerrno_t clerrno = CL_NOERR;
	int deps_met;
	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};
	rgm_resource_t *rs = (rgm_resource_t *)NULL;

	for (rsptr = rs_state_list, idx = 0; rsptr->rsname; rsptr++, idx++) {
		if (rsptr->rsstate == RSFAILED)
			continue;
		if (rsptr->rsstate == RSCREATED)
			continue;

		xml_deplist.clear();
		xml->getDependents(rsptr->rsname, xml_deplist);
		rsn_list.clear();
		for (xml_deplist.start(); xml_deplist.end() != 1;
		    xml_deplist.next()) {
			if (prop_defined(proplist1, xml_deplist.getName()))
				continue;
			if (prop_defined(proplist2, xml_deplist.getName()))
				continue;
			clerrno = get_rsnames_from_string(
			    xml_deplist.getValue(), rsn_list);
			if (clerrno)
				return (clerrno);
		}
		deps_met = 1;
		for (rsn_list.start(); rsn_list.end() != 1; rsn_list.next()) {
			scha_status = rgm_scrgadm_getrsrcconf(
			    rsn_list.getValue(), &rs, NULL);
			if (scha_status.err_code != SCHA_ERR_NOERR) {
				deps_met = 0;
				break;
			}
		}
		if (deps_met == 1) {
			*curr_rs = rsptr->rsname;
			*rs_index = idx;
			return (CL_NOERR);
		}
	}
	clerror("The following resources could not be created as they have "
	    "dependencies on resources that do not exist: ");
	for (rsptr = rs_state_list, idx = 0; rsptr->rsname; rsptr++, idx++) {
		if (rsptr->rsstate != RSNEW)
			continue;
		fprintf(stderr, "%s ", rsptr->rsname);
	}
	fprintf(stderr, "\n");
	return (CL_ENOENT);
}

//
// Check if propname is defined in the list. Return 0 if not, 1 otherwise.
//
static int
prop_defined(NameValueList &proplist, char *propname)
{
	if (!propname)
		return (0);

	for (proplist.start(); proplist.end() != 1; proplist.next()) {
		if (strcasecmp(proplist.getName(), propname) == 0)
			return (1);
	}
	return (0);
}

static clerrno_t
filter_ops(ClXml *xml, char *rsname, ValueList &rt_list, ValueList &rg_list)
{
	char *rt_xml = (char *)NULL;
	char *rg_xml = (char *)NULL;
	clerrno_t clerrno = CL_NOERR;
	boolean_t rt_matched;
	boolean_t rg_matched;

	if (rt_list.getSize() > 0) {
		clerrno = xml->getRTFromResource(rsname, &rt_xml);
		if (clerrno != CL_NOERR) {
			clerror("Failed to obtain the resource type for "
			    "\"%s\" from the specified configuration "
			    "file.\n", rsname);
			return (clerrno);
		}

		rt_matched = B_FALSE;
		for (rt_list.start(); rt_list.end() != 1; rt_list.next()) {
			if (strcmp(rt_list.getValue(), rt_xml) == 0) {
				rt_matched = B_TRUE;
				break;
			}
		}
		if (rt_matched == B_FALSE) {
			return (CL_ENOENT);
		}
	}

	if (rg_list.getSize() > 0) {
		clerrno = xml->getRGFromResource(rsname, &rg_xml);
		if (clerrno != CL_NOERR) {
			clerror("Failed to obtain the resource group for "
			    "\"%s\" from the specified configuration "
			    "file.\n", rsname);
			return (clerrno);
		}

		rg_matched = B_FALSE;
		for (rg_list.start(); rg_list.end() != 1; rg_list.next()) {
			if (strcmp(rg_list.getValue(), rg_xml) == 0) {
				rg_matched = B_TRUE;
				break;
			}
		}
		if (rg_matched == B_FALSE) {
			return (CL_ENOENT);
		}
	}
	if (rt_xml)
		free(rt_xml);
	if (rg_xml)
		free(rg_xml);
	return (CL_NOERR);
}

static clerrno_t
add_dependents(ClXml *xml, ValueList &rs_list, boolean_t *list_changed,
    NameValueList &properties, NameValueList &yproperties)
{
	NameValueList proplist;
	ValueList newdeps = ValueList(true);
	NameValueList xml_deplist;
	clerrno_t clerrno = CL_NOERR;
	ValueList dep_props;
	boolean_t overridden;
	ValueList rsn_list;
	rgm_resource_t *rs = (rgm_resource_t *)NULL;
	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};

	// First get one property list
	for (properties.start(); properties.end() != 1; properties.next()) {
		proplist.add(properties.getName(), properties.getValue());
	}
	for (yproperties.start(); yproperties.end() != 1; yproperties.next()) {
		proplist.add(yproperties.getName(), yproperties.getValue());
	}

	dep_props.add("Resource_dependencies");
	dep_props.add("Resource_dependencies_weak");
	dep_props.add("Resource_dependencies_restart");
	dep_props.add("Resource_dependencies_offline_restart");

	for (rs_list.start(); rs_list.end() != 1; rs_list.next()) {
		xml_deplist.clear();
		xml->getDependents(rs_list.getValue(), xml_deplist);
		for (dep_props.start(); dep_props.end() != 1;
		    dep_props.next()) {
			overridden = B_FALSE;
			for (proplist.start(); proplist.end() != 1;
			    proplist.next()) {
				if (strcasecmp(proplist.getName(),
				    dep_props.getValue()) == 0) {
					// Overridden
					overridden = B_TRUE;
					break;
				}
			}
			rsn_list.clear();
			if (overridden) {
				clerrno = get_rsnames_from_string(
				    proplist.getValue(),
				    rsn_list);
				if (clerrno) {
					return (clerrno);
				}
			} else {
				for (xml_deplist.start();
				    xml_deplist.end() != 1;
				    xml_deplist.next()) {
					if (strcasecmp(xml_deplist.getName(),
					    dep_props.getValue()) == 0) {
						clerrno =
						    get_rsnames_from_string(
						    xml_deplist.getValue(),
						    rsn_list);
						if (clerrno) {
							return (clerrno);
						}
						break;
					}
				}
			}
			for (rsn_list.start(); rsn_list.end() != 1;
			    rsn_list.next()) {
				newdeps.add(rsn_list.getValue());
			}
		}
	}
	if (newdeps.getSize() > 0) {
		for (newdeps.start(); newdeps.end() != 1; newdeps.next()) {
			if (strlen(newdeps.getValue()) == 0) {
				continue;
			}
			if (rs_list.isValue(newdeps.getValue()) == 1) {
				continue;
			}
			if (rs != NULL) {
				free(rs);
				rs = NULL;
			}
			scha_status = rgm_scrgadm_getrsrcconf(
			    newdeps.getValue(), &rs, NULL);
			if (scha_status.err_code == SCHA_ERR_NOERR) {
				// resource can be accessed, ignore it
				continue;
			}

			*list_changed = B_TRUE;
			rs_list.add(newdeps.getValue());
		}
	}
	if (rs != NULL)
		free(rs);
	return (clerrno);
}

static clerrno_t
get_rsnames_from_string(char *rs_string, ValueList &rsn_list)
{
	clerrno_t errflg = CL_NOERR;
	namelist_t *value_list;

	if (strlen(rs_string) == 0) {
		return (CL_NOERR);
	}

	value_list = strip_commas_in_prop(rs_string, &errflg);
	if (errflg) {
		return (errflg);
	}
	while (value_list) {
		rsn_list.add(strtok(value_list->nl_name, "{"));
		value_list = value_list->nl_next;
	}
	rgm_free_nlist(value_list);
	return (CL_NOERR);
}

//
// check_lhsa_nodes
// 	Perform various checks for the operation (lh/sa rs add) against the
//	RG type.
//
static clerrno_t
check_lhsa_nodes(char *rgname, optflgs_t optflgs, char *zc_name)
{
	scconf_errno_t scconf_status = SCCONF_NOERR;
	char errbuff[BUFSIZ];
	clerrno_t clerrno = CL_NOERR;
	boolean_t iseuropa;
	scconf_nodetype_t rg_nodetype = SCCONF_NODE_CLUSTER;
	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};
	int rv = 0;

	// Check if europa is enabled
	clerrno = europa_open(&iseuropa);
	if (clerrno)
		return (clerrno);

	if (iseuropa) {
		scha_status = get_rg_nodetype(rgname, &rg_nodetype,
						zc_name);
		rv = scha_status.err_code;
		if (rv != SCHA_ERR_NOERR) {
			print_rgm_error(scha_status, NULL);
			clerrno = map_scha_error(rv);
			goto cleanup;
		}
	}

	if (optflgs & SAflg) {
		if (rg_nodetype == SCCONF_NODE_FARM) {
			clerror("You cannot create a shared address resource in"
			    " a farm resource group.\n");
			clerrno = EINVAL;
		} else {
			scha_status = validate_nodelist(rgname, zc_name);
			rv = scha_status.err_code;
			if (rv) {
				print_rgm_error(scha_status, NULL);
				clerrno = map_scha_error(rv);
			}
		}
	} else {
		if (rg_nodetype == SCCONF_NODE_FARM) {
			scha_status = validate_farmnodelist(rgname);
		} else {
			scha_status = validate_nodelist(rgname, zc_name);
		}
		rv = scha_status.err_code;
		if (rv) {
			print_rgm_error(scha_status, NULL);
			clerrno = map_scha_error(rv);
		}
	}

cleanup:
	if (iseuropa)
		europa_close();

	return (clerrno);
}
