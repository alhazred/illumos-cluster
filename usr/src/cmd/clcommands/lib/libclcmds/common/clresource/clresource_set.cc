//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clresource_set.cc	1.12	08/11/14 SMI"

//
// Process clresource "set"
//

#ifdef __cplusplus
#include <sys/os.h>
#endif

#include <scadmin/scswitch.h>
#include <scadmin/scrgadm.h>
#include <scadmin/scconf.h>
#include <scadmin/scstat.h>
#include "clcommands.h"
#include "ClCommand.h"

#include "rgm_support.h"
#include "common.h"

//
// clresource "set"
//
clerrno_t
clresource_set(ClCommand &cmd, ValueList &operands, optflgs_t optflgs,
    ValueList &resourcegroups, ValueList &resourcetypes,
    NameValueList &properties, NameValueList &xproperties,
    NameValueList &yproperties)
{
	clerrno_t first_err = CL_NOERR;
	int rv = 0;
	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};
	char **rg_names = NULL;
	char **rs_names = (char **)0;
	char **rsnames = (char **)0;
	char *oper1;
	clerrno_t errflg = CL_NOERR;
	clerrno_t clerrno = CL_NOERR;
	uint_t i, j, k;
	ValueList rslist;
	rgm_resource_t *rs = (rgm_resource_t *)NULL;
	char *r_rtname = (char *)NULL;
	ValueList good_rts;
	ValueList good_rgs;
	scha_property_t *xproplist = NULL;
	scha_property_t *yproplist = NULL;
	scha_resource_properties_t rsprops;
	NameValueList pn_properties = NameValueList(true);
	NameValueList pn_xproperties = NameValueList(true);
	boolean_t verbose = B_FALSE;
	scconf_errno_t scconf_err = SCCONF_NOERR;
	boolean_t is_same = B_FALSE;
	char *rs_part, *zc_part, *zc_name;

	// Initializa ORB and other cluster checks
	clerrno = cluster_init();
	if (clerrno)
		return (clerrno);

	if (optflgs & vflg)
		verbose = B_TRUE;

	// Check whether any zone cluster names were specified.
	// And if they were specified, there should be only zone cluster
	// across all the RGs.
	rs_part = zc_part = zc_name = NULL;
	clerrno = check_single_zc_specified(operands, &zc_name, &is_same);
	if (clerrno != CL_NOERR) {
		clcommand_perror(clerrno);
		return (clerrno);
	}
	// If multiple zone clusters were specified, report an error.
	if (is_same == B_FALSE) {
		clerror("This operation can be performed on only one "
		    "zone cluster at a time.\n");
		return (clerrno);
	}
#if (SOL_VERSION >= __s10)
	if (zc_name) {
		// If zone cluster name was specified, we have to ensure
		// that it is valid.
		// We have to check whether this operation is allowed for
		// the status of the zone cluster.
		clerrno = check_zc_operation(CL_OBJ_TYPE_RS, CL_OP_TYPE_SET,
						zc_name);
		if (clerrno != CL_NOERR) {
			return (clerrno);
		}
	}
#endif

	// Check resourcetypes
	for (resourcetypes.start(); !resourcetypes.end();
	    resourcetypes.next()) {
		if (r_rtname) {
			free(r_rtname);
			r_rtname = 0;
		}
		clerrno = get_r_rtname(resourcetypes.getValue(), &r_rtname,
					zc_name);
		if (clerrno == 0)
			good_rts.add(r_rtname);
		else {
			if (first_err == 0) {
				// first error!
				first_err = clerrno;
			}
		}
	}
	if (r_rtname)
		free(r_rtname);

	if ((resourcetypes.getSize() > 0) && (good_rts.getSize() == 0))
		return (first_err);

	if (optflgs & LHflg)
		good_rts.add(get_latest_rtname(SCRGADM_RTLH_NAME, NULL));

	if (optflgs & SAflg)
		good_rts.add(get_latest_rtname(SCRGADM_RTSA_NAME, NULL));

	for (resourcegroups.start(); !resourcegroups.end();
	    resourcegroups.next()) {
		clerrno = check_rg(resourcegroups.getValue(), zc_name);
		if (clerrno == 0)
			good_rgs.add(resourcegroups.getValue());
		else {
			if (first_err == 0) {
				// first error!
				first_err = clerrno;
			}
		}
	}
	if ((resourcegroups.getSize() > 0) && (good_rgs.getSize() == 0))
		return (first_err);

	operands.start();
	oper1 = operands.getValue();
	if (strcmp(oper1, CLCOMMANDS_WILD_OPERAND) == 0) {
		if (good_rgs.getSize() > 0) {
			rg_names = vl_to_names(good_rgs, &errflg);
			if (errflg)
				return (errflg);
		} else {
			clerrno = z_getrglist(&rg_names, verbose,
			    optflgs & uflg, zc_name);
			if (clerrno != CL_NOERR) {
				return (clerrno);
			}
			// Remove the zone cluster context from all these
			// names.
			clerrno = remove_zc_names_from_names(rg_names);
			if (clerrno != CL_NOERR) {
				clcommand_perror(clerrno);
				if (first_err == CL_NOERR) {
					first_err = clerrno;
				}
				return (clerrno);
			}
		}
	}

	// By the time we are here, RT names and RG names do not have
	// the zone cluster scope in their names. Only the resource names
	// have the zone cluster scope in their names.
	if ((strcmp(oper1, CLCOMMANDS_WILD_OPERAND) == 0) &&
	    (rg_names != (char **)NULL)) {

		// Get the list of resources for this group
		for (i = 0; rg_names[i] != NULL; i++) {
			if (rs_names) {
				rgm_free_strarray(rs_names);
				rs_names = (char **)NULL;
			}
			scha_status = rgm_scrgadm_getrsrclist(rg_names[i],
			    &rs_names, zc_name);
			rv = scha_status.err_code;
			if (rv != SCHA_ERR_NOERR) {
				print_rgm_error(scha_status, NULL);
				clerror("Failed to obtain list of all "
				    "resources in resource group "
				    "\"%s\".\n", rg_names[i]);
				clerror("Will not try to modify "
				    "resources belonging to group "
				    "\"%s\".\n", rg_names[i]);
				if (first_err == 0) {
					// first error!
					first_err = map_scha_error(rv);
				}
				continue;
			}
			if (rs_names == NULL)
				continue;

			for (j = 0; rs_names[j]; j++) {
				if (rs != NULL) {
					free(rs);
					rs = NULL;
				}
				scha_status = rgm_scrgadm_getrsrcconf(
				    rs_names[j], &rs, zc_name);
				rv = scha_status.err_code;
				if (rv != SCHA_ERR_NOERR) {
					print_rgm_error(scha_status, NULL);
					clerror("Resource \"%s\" will not be "
					    "modified.\n", rs_names[j]);
					if (first_err == 0) {
						// first error!
						first_err = map_scha_error(rv);
					}
					continue;
				}
				if (rs == NULL) {
					clerror("Failed to access resource "
					    "\"%s\", resource will not be "
					    "modified.\n", rs_names[j]);
					if (first_err == 0) {
						// first error!
						first_err = CL_ENOENT;
					}
					continue;
				}
				if (good_rts.getSize() > 0)
					if (match_type(good_rts, rs) == 0)
						continue;

				//
				// Verify all the properties for per-node
				// elements.
				//
				clerrno = verify_yprops(yproperties);
				if (clerrno) {
					if (first_err == 0) {
						// first error!
						first_err = clerrno;
					}
					continue;
				}
				pn_properties.clear();
				clerrno = process_props(properties,
				    pn_properties, rs->r_type, zc_name);
				if (clerrno) {
					if (first_err == 0) {
						// first error!
						first_err = clerrno;
					}
					continue;
				}
				pn_xproperties.clear();
				clerrno = process_props(xproperties,
				    pn_xproperties, rs->r_type, zc_name);
				if (clerrno) {
					if (first_err == 0) {
						// first error!
						first_err = clerrno;
					}
					continue;
				}

				// Move properties to either x- or y-
				clerrno = set_rs_props(rs_names[j], rs->r_type,
				    pn_properties, pn_xproperties, yproperties,
				    zc_name);
				if (clerrno) {
					if (first_err == 0) {
						// first error!
						first_err = clerrno;
					}
					continue;
				}
				errflg = 0;
				xproplist = nvl_to_prop_array(pn_xproperties,
				    rs_names[j], &errflg, CL_TYPE_RS,
				    rs->r_type, zc_name);
				if (errflg) {
					if (first_err == 0) {
						// first error!
						first_err = errflg;
					}
					continue;
				}
				errflg = 0;
				yproplist = nvl_to_prop_array(yproperties,
				    rs_names[j], &errflg, CL_TYPE_RS,
				    rs->r_type, zc_name);
				if (errflg) {
					if (first_err == 0) {
						// first error!
						first_err = errflg;
					}
					continue;
				}
				rsprops.srp_predefined = &yproplist;
				rsprops.srp_extension = &xproplist;

				scha_status =
				    rgm_scrgadm_update_resource_property(
				    rs_names[j], &rsprops, zc_name);
				rv = scha_status.err_code;
				if (rv) {
					print_rgm_error(scha_status, NULL);
					clerror("Failed to update resource "
					    "properties for \"%s\".\n",
					    rs_names[j]);
					if (first_err == 0) {
						// first error!
						first_err = map_scha_error(rv);
					}
				} else if (optflgs & vflg)
					clmessage("Resource \"%s\" modified.\n",
					    rs_names[j]);
				free_prop_array(xproplist);
				free_prop_array(yproplist);
			}
		}
	} else if (strcmp(oper1, CLCOMMANDS_WILD_OPERAND) != 0) {
		for (operands.start(); operands.end() != 1; operands.next()) {

			if (rs != NULL) {
				free(rs);
				rs = NULL;
			}
			if (rs_part) {
				free(rs_part);
				rs_part = NULL;
			}
			if (zc_part) {
				free(zc_part);
				zc_part = NULL;
			}

			scconf_err = scconf_parse_obj_name_from_cluster_scope(
					operands.getValue(), &rs_part,
					&zc_part);

			if (scconf_err != SCCONF_NOERR) {
				clerrno = map_scconf_error(scconf_err);
				clcommand_perror(clerrno);
				if (first_err == CL_NOERR) {
					first_err = clerrno;
				}
				continue;
			}

			scha_status = rgm_scrgadm_getrsrcconf(
			    rs_part, &rs, zc_part);
			rv = scha_status.err_code;
			if (rv != SCHA_ERR_NOERR) {
				print_rgm_error(scha_status, NULL);
				clerror("Resource \"%s\" will not be modified"
				    ".\n", operands.getValue());
				if (first_err == 0) {
					// first error!
					first_err = map_scha_error(rv);
				}
				continue;
			}
			if (rs == NULL) {
				clerror("Failed to access resource \"%s\", "
				    "resource will not be modified.\n",
				    operands.getValue());
				if (first_err == 0) {
					// first error!
					first_err = CL_ENOENT;
				}
				continue;
			}
			if (good_rts.getSize() > 0) {
				if (match_type(good_rts, rs) == 0) {
					if (optflgs & LHflg)
						clerror("Specified resource "
						    "\"%s\" is not a logical "
						    "hostname resource.\n",
						    operands.getValue());
					else if (optflgs & SAflg)
						clerror("Specified resource "
						    "\"%s\" is not a shared "
						    "address resource.\n",
						    operands.getValue());
					else
						clerror("Specified resource "
						    "\"%s\" does not meet the "
						    "criteria set by the "
						    "\"-t\" option.\n",
						    operands.getValue());
					if (first_err == 0) {
						// first error!
						first_err = CL_EINVAL;
					}
					continue;
				}
			}
			if (good_rgs.getSize() > 0) {
				if (match_group(good_rgs, rs) == 0) {
					clerror("Specified resource \"%s\" "
					    "does not meet the criteria set "
					    "by the \"-g\" option.\n",
					    operands.getValue());
					if (first_err == 0) {
						// first error!
						first_err = CL_EINVAL;
					}
					continue;
				}
			}

			//
			// Verify all the properties for per-node
			// elements.
			//
			clerrno = verify_yprops(yproperties);
			if (clerrno) {
				if (first_err == 0) {
					// first error!
					first_err = clerrno;
				}
				continue;
			}
			pn_properties.clear();
			clerrno = process_props(properties, pn_properties,
			    rs->r_type, zc_part);
			if (clerrno) {
				if (first_err == 0) {
					// first error!
					first_err = clerrno;
				}
				continue;
			}
			pn_xproperties.clear();
			clerrno = process_props(xproperties, pn_xproperties,
			    rs->r_type, zc_part);
			if (clerrno) {
				if (first_err == 0) {
					// first error!
					first_err = clerrno;
				}
				continue;
			}

			// Move properties to either x- or y-
			clerrno = set_rs_props(operands.getValue(), rs->r_type,
			    pn_properties, pn_xproperties, yproperties,
			    zc_part);
			if (clerrno) {
				if (first_err == 0) {
					// first error!
					first_err = clerrno;
				}
				continue;
			}
			errflg = 0;
			xproplist = nvl_to_prop_array(pn_xproperties,
			    operands.getValue(), &errflg, CL_TYPE_RS,
			    rs->r_type, zc_name);
			if (errflg) {
				if (first_err == 0) {
					// first error!
					first_err = errflg;
				}
				continue;
			}
			errflg = 0;
			yproplist = nvl_to_prop_array(yproperties,
			    operands.getValue(), &errflg, CL_TYPE_RS,
			    rs->r_type, zc_name);
			if (errflg) {
				if (first_err == 0) {
					// first error!
					first_err = errflg;
				}
				continue;
			}

			rsprops.srp_predefined = &yproplist;
			rsprops.srp_extension = &xproplist;
			scha_status = rgm_scrgadm_update_resource_property(
			    rs_part, &rsprops, zc_part);
			rv = filter_warning(scha_status.err_code);
			if (rv) {
				print_rgm_error(scha_status, NULL);
				clerror("Failed to update resource properties "
				    "for \"%s\".\n",
				    operands.getValue());
				if (first_err == 0) {
					// first error!
					first_err = map_scha_error(rv);
				}
			} else {
				if (scha_status.err_msg) {
					fprintf(stdout, "%s\n",
					    scha_status.err_msg);
				}

				if (optflgs & vflg) {
					clmessage("Resource \"%s\" modified.\n",
					    operands.getValue());
				}
			}
			free_prop_array(xproplist);
			free_prop_array(yproplist);
		}
	}

	if (rg_names)
		rgm_free_strarray(rg_names);
	if (rs_names)
		rgm_free_strarray(rs_names);
	if (rs)
		free(rs);
	if (zc_name) {
		free(zc_name);
	}
	if (rs_part) {
		free(rs_part);
	}
	if (zc_part) {
		free(zc_part);
	}

	return (first_err);
}
