//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clresource_status.cc	1.15	08/07/25 SMI"

//
// Process clresource "status"
//

#ifdef __cplusplus
#include <sys/os.h>
#endif

#include <scadmin/scswitch.h>
#include <scadmin/scrgadm.h>
#include <scadmin/scconf.h>
#include <scadmin/scstat.h>
#include "clcommands.h"
#include "ClCommand.h"
#include "ClStatus.h"

#include "rgm_support.h"
#include "common.h"

static clerrno_t
filter_rs_add_to_status_lists(char *rsname, char *zc_name,
    ValueList &good_rts, ValueList &good_rgs, ValueList &good_nodes,
    ValueList &good_states, ValueList &rslist, ValueList &rsnodelist,
    ValueList &rsstatlist, ValueList &rsmsglist, optflgs_t optflgs);

static clerrno_t
get_clresource_status_obj_select(ValueList &operands, optflgs_t optflgs,
    ValueList &resourcegroups, ValueList &resourcetypes,
    ValueList &nodes, ValueList &states, ClStatus *rs_status,
    ValueList &zc_names = ValueList());

static void
update_status_lists(char *rsname, scstat_rs_t *rs, ValueList &rslist,
    ValueList &rsnodelist, ValueList &rsstatlist, ValueList &rsmsglist,
    char *zc_name);

//
// clresource "status"
//

clerrno_t
clresource_status(ClCommand &cmd, ValueList &operands, optflgs_t optflgs,
    ValueList &resourcegroups, ValueList &resourcetypes,
    ValueList &nodes, ValueList &states, ValueList &zc_names)
{
	clerrno_t clerrno = CL_NOERR;
	ClStatus *rs_status = new ClStatus(cmd.isVerbose ? true : false);

	clerrno = get_clresource_status_obj_select(operands, optflgs,
	    resourcegroups, resourcetypes, nodes, states, rs_status, zc_names);

	rs_status->enableRowGrouping(true);
	rs_status->setHeadingTitle(HEAD_RESOURCE);
	rs_status->setHeadings(
	    (char *)gettext("Resource Name"),
	    (char *)gettext("Node Name"),
	    (char *)gettext("State"),
	    (char *)gettext("Status Message"));
	rs_status->print();

	return (clerrno);
}

//
// clrs "status" for cluster
//
clerrno_t
get_clresource_status_obj(optflgs_t optflgs, ClStatus *rs_status)
{
	clerrno_t clerrno = CL_NOERR;
	ValueList rs_list = ValueList(true);
	ValueList empty_list;

	rs_list.add(CLCOMMANDS_WILD_OPERAND);

	rs_status->enableRowGrouping(true);
	rs_status->setHeadingTitle(HEAD_RESOURCE);
	rs_status->setHeadings(
	    (char *)gettext("Resource Name"),
	    (char *)gettext("Node Name"),
	    (char *)gettext("State"),
	    (char *)gettext("Status Message"));
	clerrno = get_clresource_status_obj_select(rs_list, optflgs, empty_list,
	    empty_list, empty_list, empty_list, rs_status);

	return (clerrno);

}

clerrno_t
get_clresource_status_obj_select(ValueList &operands, optflgs_t optflgs,
    ValueList &resourcegroups, ValueList &resourcetypes,
    ValueList &nodes, ValueList &states, ClStatus *rs_status,
    ValueList &zc_names)
{
	clerrno_t first_err = CL_NOERR;
	int rv = 0;
	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};
	char **rg_names = NULL;
	char **rs_names = (char **)0;
	char **rsnames = (char **)0;
	char *oper1;
	clerrno_t clerrno = CL_NOERR;
	clerrno_t errflg = CL_NOERR;
	uint_t i, j, k;
	ValueList rslist;
	ValueList rsnodelist;
	ValueList rsstatlist;
	ValueList rsmsglist;
	rgm_resource_t *rs = (rgm_resource_t *)NULL;
	char *r_rtname = (char *)NULL;
	ValueList good_rts;
	ValueList good_rgs;
	ValueList good_nodes;
	ValueList good_states, tmp_list, valid_zc_list;
	scstat_rs_t *rsstat = (scstat_rs_t *)NULL;
	scstat_errno_t scstat_status;
	char *r_name, *zc_name, *full_name, *obj_name;
	scconf_errno_t scconf_err = SCCONF_NOERR;
	boolean_t has_obj = B_FALSE;

	// Initializa ORB and other cluster checks
	clerrno = cluster_init();
	if (clerrno)
		return (clerrno);

	r_name = zc_name = full_name = obj_name = NULL;
	// Zone cluster names can be specified either with the -Z
	// option or in the form of <zonecluster>:<R name> format.
	// But, both the ways cannot be used simultaneously.
	// If zone clusters were not specified with the -Z option, we
	// have to check whether they were specified in the <zc>:<r>
	// format.
	for (operands.start(); !operands.end(); operands.next()) {
		scconf_err = scconf_parse_obj_name_from_cluster_scope(
				operands.getValue(), &r_name, &zc_name);

		if (scconf_err != SCCONF_NOERR) {
			return (CL_ENOMEM);
		}

		if (zc_name && zc_names.getSize()) {
			// -Z flag was specified and this R was scoped
			// with a zone cluster name as well. Report an
			// error.
			clerror("You cannot use the "
				"\"<zonecluster>:<resourcename>\" "
				"form of the resource name with the "
				"\"-Z\" option.");
			free(r_name);
			free(zc_name);
			return (CL_EINVAL);
		}

		// If this zone cluster name does not exist in tmp_list,
		// then we wil add it here.
		if (!tmp_list.isValue(zc_name)) {
			tmp_list.add(zc_name);
		}

		if (r_name) {
			free(r_name);
			r_name = NULL;
		}
		if (zc_name) {
			free(zc_name);
			zc_name = NULL;
		}
	}
#if (SOL_VERSION >= __s10)
	// By the time we are here, we have added any zone cluster name in the
	// Resource names to tmp_list.
	if (zc_names.getSize()) {
		// Zc names were specified. We have to check whether "all"
		// was specified.
		zc_names.start();

		if (strcmp(zc_names.getValue(), ALL_CLUSTERS_STR) == 0) {
			// Get all known zone clusters.
			clerrno = get_all_clusters(valid_zc_list);

			if (clerrno != CL_NOERR) {
				return (clerrno);
			}
		} else {
			tmp_list = zc_names;
		}
	}

	// valid_zc_list will contain at least one zone cluster name
	// if "all" was specified. If it is empty, we have to check
	// the validity of the zone clusters specified by the user.
	if (valid_zc_list.getSize() < 1) {
		clerrno = get_valid_zone_clusters(tmp_list, valid_zc_list);
	}
	// Clear the memory.
	tmp_list.clear();

	if (clerrno) {
		// There was at least one invalid zone cluster.
		// We have to return.
		return (clerrno);
	}
#endif

	// Check nodes
	clerrno = get_good_nodes(nodes, good_nodes, valid_zc_list);
	if (first_err == 0) {
		// first error!
		first_err = clerrno;
	}

	// Check states
	for (states.start(); !states.end(); states.next()) {
		clerrno = check_state(states.getValue(), CL_TYPE_RS);
		if (clerrno) {
			if (first_err == 0) {
				// first error!
				first_err = clerrno;
			}
		} else {
			good_states.add(states.getValue());
		}
	}

	// Check resourcetypes
	// If zone clusters were specified, we have to check whether
	// the specified RTs are part of at least one zone cluster.
	// If an RT is not present in at least one zone cluster, then
	// we will not include it in the list of good RTs (good_rts).
	for (resourcetypes.start(); !resourcetypes.end();
	    resourcetypes.next()) {

		if (valid_zc_list.getSize() == 0) {
			// If we are here, then no zone cluster was
			// specified. We just have to check whether this
			// RT is valid in the current cluster.
			clerrno = get_r_rtname(resourcetypes.getValue(),
						&r_rtname);
			if (clerrno == 0)
				good_rts.add(r_rtname);
			else {
				if (first_err == 0) {
					// first error!
					first_err = clerrno;
				}
			}
			if (r_rtname) {
				free(r_rtname);
				r_rtname = 0;
			}

			continue;
		}
		// If we are here, it means zone clusters were specified.
		// We have to check whether this RT is valid across at least
		// one zone cluster.
		has_obj = B_FALSE;
		for (valid_zc_list.start(); !valid_zc_list.end();
				valid_zc_list.next()) {

			if (r_rtname) {
				free(r_rtname);
				r_rtname = 0;
			}
			if (full_name) {
				free(full_name);
				full_name = NULL;
			}

			clerrno = get_r_rtname(resourcetypes.getValue(),
						&r_rtname,
						valid_zc_list.getValue());
			if (clerrno != CL_NOERR) {
				// This RT is not present in this cluster.
				continue;
			}
			// This RT is present in this cluster. We have to
			// add this RT to "good_rts" with cluster scoping.
			scconf_err = scconf_add_cluster_scope_to_obj_name(
					r_rtname, valid_zc_list.getValue(),
					&full_name);
			if (scconf_err != SCCONF_NOERR) {
				if (first_err == CL_NOERR) {
					first_err = CL_EINTERNAL;
				}
				continue;
			}

			good_rts.add(full_name);
			has_obj = B_TRUE;
		}

		// Check whether this RT was present in any zone cluster
		// or not.
		if (has_obj == B_FALSE) {
			clerror("Unknown resource type %s\n",
				resourcetypes.getValue());
			if (first_err == CL_NOERR) {
				first_err = CL_ENOENT;
			}
		}
		// Free any memory
		if (r_rtname) {
			free(r_rtname);
			r_rtname = 0;
		}
		if (full_name) {
			free(full_name);
			full_name = NULL;
		}
	}

	if ((resourcetypes.getSize() > 0) && (good_rts.getSize() == 0))
		return (first_err);
	// By now, we have verified RTs. And all the valid RTs have their
	// names scoped with zone cluster names as well.

	if ((states.getSize() > 0) && (good_states.getSize() == 0))
		return (first_err);

	// Add Logical Host or Shared Address resource types if requird.
	clerrno = add_lh_or_sa_rts_to_list(good_rts, valid_zc_list, optflgs);
	if (clerrno != CL_NOERR) {
		return (clerrno);
	}

	// Check the validity of RGs
	// If zone clusters were specified, we have to check whether
	// the specified RGs are part of at least one zone cluster.
	// If an RG is not present in at least one zone cluster, then
	// we will not include it in the list of good RGs (good_rgs).
	if (valid_zc_list.getSize() == 0) {
		for (resourcegroups.start(); !resourcegroups.end();
		    resourcegroups.next()) {
			clerrno = check_rg(resourcegroups.getValue(), NULL);
			if (clerrno == 0)
				good_rgs.add(resourcegroups.getValue());
			else {
				if (first_err == 0) {
					// first error!
					first_err = clerrno;
				}
			}
		}
	} else {
		// Zone cluster names were specified.
		for (resourcegroups.start(); !resourcegroups.end();
		    resourcegroups.next()) {
			tmp_list.clear();
#if (SOL_VERSION >= __s10)
			clerrno = get_zone_clusters_with_rg(
					resourcegroups.getValue(), tmp_list,
					valid_zc_list);
			if (clerrno == CL_ENOENT) {
				clerror("Resource group %s does not exist"
					" in any of the specified"
					" zone clusters",
					resourcegroups.getValue());
			}
			if (clerrno != CL_NOERR) {
				if (first_err == CL_NOERR) {
					first_err = clerrno;
				}
				continue;
			}
#endif
			// If we are here, then this RG existed in at least
			// one zone cluster
			for (tmp_list.start(); !tmp_list.end();
				tmp_list.next()) {
				scconf_err =
					scconf_add_cluster_scope_to_obj_name(
					    resourcegroups.getValue(),
					    tmp_list.getValue(),
					    &full_name);
				if (scconf_err != SCCONF_NOERR) {
					if (first_err == CL_NOERR) {
						first_err = CL_EINTERNAL;
					}
				} else {
					good_rgs.add(full_name);
				}
				// Free memory
				if (full_name) {
					free(full_name);
					full_name = NULL;
				}
			}
		}
		// Free memory
		tmp_list.clear();
	}

	if ((resourcegroups.getSize() > 0) && (good_rgs.getSize() == 0))
		return (first_err);

	if (operands.getSize() > 0) {
		operands.start();
		oper1 = operands.getValue();
	} else
		oper1 = CLCOMMANDS_WILD_OPERAND;

	if (strcmp(oper1, CLCOMMANDS_WILD_OPERAND) == 0) {
		if (good_rgs.getSize() > 0) {
			rg_names = vl_to_names(good_rgs, &errflg);
			if (errflg)
				return (errflg);
		} else {
			if (valid_zc_list.getSize() == 0) {
				// No zone clusters were specified.
				// Get the list of RGs from the current cluster
				scha_status = scswitch_getrglist(&rg_names,
						B_FALSE, NULL);
				rv = scha_status.err_code;
				if (rv != SCHA_ERR_NOERR) {
					clerror("Failed to obtain list of "
					    "all resource groups.\n");
					print_rgm_error(scha_status, NULL);
					return (map_scha_error(rv));
				}
			} else {
				// Zone clusters were specified.
				// Fetch RGs from all the zone clusters.
				for (valid_zc_list.start();
					!valid_zc_list.end();
					valid_zc_list.next()) {
					scha_status =
						scswitch_getrglist(&rg_names,
						    B_FALSE,
						    valid_zc_list.getValue());
					rv = scha_status.err_code;
					if (rv != SCHA_ERR_NOERR) {
						clerror("Failed to obtain list"
						" of all resource groups.\n");
						print_rgm_error(scha_status,
							NULL);
						return (map_scha_error(rv));
					}
					clerrno = names_to_valuelist(rg_names,
								tmp_list);
					if ((clerrno != CL_NOERR) &&
						(first_err == CL_NOERR)) {
						first_err = clerrno;
					}
					// Free memory
					rgm_free_strarray(rg_names);
				}
				// Now, convert to names
				rg_names = vl_to_names(tmp_list, &errflg);
				// Free memory
				tmp_list.clear();
				if (errflg) {
					return (errflg);
				}
			}
		}
	}

	// By now, we definitely should have RG names (whether the user
	// specified them or not). And if there are no RGs we just
	// dont do anything.

	if ((strcmp(oper1, CLCOMMANDS_WILD_OPERAND) == 0) &&
	    (rg_names != (char **)NULL)) {

		// Get the list of resources for this group
		for (i = 0; rg_names[i] != NULL; i++) {
			if (rs_names) {
				rgm_free_strarray(rs_names);
				rs_names = (char **)NULL;
			}
			if (obj_name) {
				free(obj_name);
				obj_name = NULL;
			}
			if (zc_name) {
				free(zc_name);
				zc_name = NULL;
			}
			scconf_err = scconf_parse_obj_name_from_cluster_scope(
					rg_names[i], &obj_name, &zc_name);
			if (scconf_err != SCCONF_NOERR) {
				if (first_err == CL_NOERR) {
					first_err = map_scconf_error(
							scconf_err);
				}
				clcommand_perror(first_err);
				continue;
			}
			scha_status = rgm_scrgadm_getrsrclist(obj_name,
			    &rs_names, zc_name);
			rv = scha_status.err_code;
			if (rv != SCHA_ERR_NOERR) {
				print_rgm_error(scha_status, NULL);
				clerror("Failed to obtain list of all "
				    "resources in resource group "
				    "\"%s\".\n", rg_names[i]);
				if (first_err == 0) {
					// first error!
					first_err = map_scha_error(rv);
				}
				continue;
			}
			if (rs_names == NULL)
				continue;

			for (j = 0; rs_names[j]; j++) {
				if (rs != NULL) {
					free(rs);
					rs = NULL;
				}
				scha_status = rgm_scrgadm_getrsrcconf(
				    rs_names[j], &rs, zc_name);
				rv = scha_status.err_code;
				if (rv != SCHA_ERR_NOERR) {
					print_rgm_error(scha_status,
					    rs_names[j]);
					if (first_err == 0) {
						// first error!
						first_err = map_scha_error(rv);
					}
					continue;
				}
				if (rs == NULL) {
					clerror("Failed to access resource "
					    "\"%s\".\n", rs_names[j]);
					if (first_err == 0) {
						// first error!
						first_err = CL_ENOENT;
					}
					continue;
				}
				if (good_rts.getSize() > 0) {
					has_obj = B_FALSE;
					clerrno = match_rt_rg_in_zc(
							good_rts,
							rs, zc_name,
							has_obj, 0);
					if (clerrno != CL_NOERR) {
						if (first_err == CL_NOERR) {
							first_err = clerrno;
						}
						clerror("Internal error.\n");
						continue;
					}

					if (has_obj == B_FALSE) {
						continue;
					}
				}
				// If we are here, it means this resource
				// passed ResourceType checks. Now, we should
				// fetch the status of this resource.

				if (rsstat) {
					scstat_free_rs(rsstat);
					rsstat = NULL;
				}

				scstat_status = scstat_get_rs(rg_names[i],
				    rs_names[j], &rsstat, B_TRUE);
				if (scstat_status != SCSTAT_ENOERR) {
					print_scstat_error(scstat_status,
					    rs_names[j]);
					if (first_err == 0) {
						// first error!
						first_err =
						    map_scstat_error(
						    scstat_status);
					}
					continue;
				}
				// We have to check whether this resource
				// matches the status filter.
				if (good_states.getSize() > 0)
					if (match_rs_state(good_states, rsstat)
					    == 0)
						continue;

				if (good_nodes.getSize() > 0)
					if (match_rs_node(good_nodes, rsstat)
					    == 0)
						continue;

				update_status_lists(rs_names[j], rsstat, rslist,
				    rsnodelist, rsstatlist, rsmsglist, zc_name);
			}
		}
	} else if (strcmp(oper1, CLCOMMANDS_WILD_OPERAND) != 0) {
		// Operands were specified. Zone cluster names can be specified
		// either in "zc_names", or as part of the resource names
		// itself.
		for (operands.start(); operands.end() != 1; operands.next()) {

			if (rs != NULL) {
				free(rs);
				rs = NULL;
			}
			if (r_name) {
				free(r_name);
				r_name = NULL;
			}
			if (zc_name) {
				free(zc_name);
				zc_name = NULL;
			}
			tmp_list.clear();

			if (zc_names.getSize() > 0) {
#if (SOL_VERSION >= __s10)
				// -Z flag was specified.
				clerrno = get_zone_clusters_with_rs(
						operands.getValue(),
						tmp_list, valid_zc_list);

				if (clerrno != CL_NOERR) {
					clcommand_perror(clerrno);
					if (first_err == CL_NOERR) {
						first_err = clerrno;
					}
					continue;
				}

				r_name = strdup(operands.getValue());
#endif
			} else {
				// We have to check whether zone cluster name
				// was specified in the resource name.
				scconf_err =
				    scconf_parse_obj_name_from_cluster_scope(
					operands.getValue(), &r_name,
								&zc_name);
				if (scconf_err != SCCONF_NOERR) {
					clerrno = map_scconf_error(scconf_err);
					clcommand_perror(clerrno);
					if (first_err == CL_NOERR) {
						first_err = clerrno;
					}
					continue;
				}
				tmp_list.add(zc_name);
			}
			// Now filter and add the resources to print list
			// if they satisfy all the filters.
			if (tmp_list.getSize() == 0) {
				// There was not zone cluster specified
				// anywhere.
				clerrno = filter_rs_add_to_status_lists(
					operands.getValue(), NULL, good_rts,
					good_rgs, good_nodes, good_states,
					rslist, rsnodelist, rsstatlist,
					rsmsglist, optflgs);
				if (clerrno != CL_NOERR) {
					if (first_err == CL_NOERR) {
						first_err = clerrno;
					}
				}
				continue;
			}
			// If we are here, it means zone clusters were
			// specified.
			for (tmp_list.start(); !tmp_list.end();
				tmp_list.next()) {
				clerrno = filter_rs_add_to_status_lists(
					r_name, tmp_list.getValue(), good_rts,
					good_rgs, good_nodes, good_states,
					rslist, rsnodelist, rsstatlist,
					rsmsglist, optflgs);
				if (clerrno != CL_NOERR) {
					if (first_err == CL_NOERR) {
						first_err = clerrno;
					}
				}
			}

		}
	}

	for (rslist.start(), rsnodelist.start(), rsstatlist.start(),
	    rsmsglist.start();
	    rslist.end() != 1;
	    rslist.next(), rsnodelist.next(), rsstatlist.next(),
	    rsmsglist.next())
		rs_status->addRow(rslist.getValue(), rsnodelist.getValue(),
		    rsstatlist.getValue(), rsmsglist.getValue());

	if (rg_names)
		rgm_free_strarray(rg_names);
	if (rs_names)
		rgm_free_strarray(rs_names);
	if (rs)
		free(rs);
	if (rsstat)
		free(rsstat);
	if (r_name) {
		free(r_name);
	}
	if (zc_name) {
		free(zc_name);
	}
	if (obj_name) {
		free(obj_name);
	}
	tmp_list.clear();

	return (first_err);
}

clerrno_t
filter_rs_add_to_status_lists(char *rsname, char *zc_name,
    ValueList &good_rts, ValueList &good_rgs, ValueList &good_nodes,
    ValueList &good_states, ValueList &rslist, ValueList &rsnodelist,
    ValueList &rsstatlist, ValueList &rsmsglist, optflgs_t optflgs) {

	scstat_rs_t *rsstat = (scstat_rs_t *)NULL;
	scstat_errno_t scstat_status;
	scconf_errno_t scconf_err = SCCONF_NOERR;
	clerrno_t clerrno = CL_NOERR;
	rgm_resource_t *rs = (rgm_resource_t *)NULL;
	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};
	boolean_t found_rs = B_FALSE;
	char *full_name = NULL;
	int rv = 0;

	if (!rsname) {
		return (CL_EINVAL);
	}

	scha_status = rgm_scrgadm_getrsrcconf(rsname, &rs, zc_name);
	rv = scha_status.err_code;
	if (rv != SCHA_ERR_NOERR) {
		print_rgm_error(scha_status, rsname);
		clerrno = map_scha_error(rv);
		clcommand_perror(clerrno);
		goto cleanup;
	}

	if (rs == NULL) {
		clerror("Failed to access resource \"%s\".\n");
		clerrno = CL_ENOENT;
		clcommand_perror(clerrno);
		goto cleanup;
	}

	if (good_rts.getSize() > 0) {
		clerrno = match_rt_rg_in_zc(good_rts, rs, zc_name,
						found_rs, 0);
		if (clerrno != CL_NOERR) {
			clcommand_perror(clerrno);
			goto cleanup;
		}
		if (found_rs == B_FALSE) {
			if (optflgs & LHflg)
				clerror("Specified resource "
					"\"%s\" is not a logical "
					"hostname resource.\n",
					rsname);
			else if (optflgs & SAflg)
				clerror("Specified resource "
					"\"%s\" is not a shared "
					"address resource.\n",
					rsname);
			else
				clerror("Specified resource "
					"\"%s\" does not meet the "
					"criteria set by the "
					"\"-t\" option.\n",
					rsname);

			clerrno = CL_EINVAL;
			goto cleanup;
		}
	}

	if (good_rgs.getSize() > 0) {
		clerrno = match_rt_rg_in_zc(good_rts, rs, zc_name,
						found_rs, 1);
		if (clerrno != CL_NOERR) {
			clcommand_perror(clerrno);
			goto cleanup;
		}
		if (found_rs == B_FALSE) {
			clerror("Specified resource \"%s\" "
				"does not meet the criteria set "
				"by the \"-g\" option.\n", rsname);
			clerrno = CL_EINVAL;
			goto cleanup;
		}
	}

	// If we are here, this resource has cleared RT and RG filters
	scconf_err = scconf_add_cluster_scope_to_obj_name(rs->r_rgname,
				zc_name, &full_name);
	if (scconf_err != SCCONF_NOERR) {
		clerrno = map_scconf_error(scconf_err);
		goto cleanup;
	}
	scstat_status = scstat_get_rs(full_name, rs->r_name, &rsstat, B_TRUE);
	if (scstat_status != SCSTAT_ENOERR) {
		print_scstat_error(scstat_status, rsname);
		clerrno = map_scstat_error(scstat_status);
		goto cleanup;
	}

	// Now check the Status filter
	if (good_states.getSize() > 0) {
		if (match_rs_state(good_states, rsstat) == 0) {
			clerror("Specified resource \"%s\" "
			    "does not meet the criteria set "
			    "by the \"-s\" option.\n", rsname);
			clerrno = CL_EINVAL;
			goto cleanup;
		}
	}

	if (good_nodes.getSize() > 0) {
		if (match_rs_node(good_nodes, rsstat) == 0) {
			clerror("Specified resource \"%s\" "
				"does not meet the criteria set "
				"by the \"-n\" or \"-z\" option.\n",
				rsname);
			clerrno = CL_EINVAL;
			goto cleanup;
		}
	}

	// If we are here, it means this resource cleared all the filters.
	update_status_lists(rs->r_name, rsstat, rslist, rsnodelist, rsstatlist,
				rsmsglist, zc_name);

cleanup:
	if (full_name) {
		free(full_name);
	}
	if (rsstat) {
		scstat_free_rs(rsstat);
	}
	return (clerrno);
}

static void
update_status_lists(char *rsname, scstat_rs_t *rs, ValueList &rslist,
    ValueList &rsnodelist, ValueList &rsstatlist, ValueList &rsmsglist,
    char *zc_name)
{
	int first_node = 1;
	scstat_rs_status_t *rsnode;

	for (rsnode = rs->scstat_rs_status_list; rsnode;
	    rsnode = rsnode->scstat_rs_status_by_node_next) {

		if (first_node) {
			rslist.add(rsname);
			first_node = 0;
		} else {
			rslist.add("");
		}
		rsnodelist.add(rsnode->scstat_node_name);
		rsstatlist.add(rsnode->scstat_rs_state_str);
		rsmsglist.add(rsnode->scstat_rs_statstr);
	}
}
