//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clresource_export.cc	1.14	08/07/25 SMI"

//
// Process clresource "export"
//

#ifdef __cplusplus
#include <sys/os.h>
#endif

#include <scadmin/scswitch.h>
#include <scadmin/scrgadm.h>
#include <scadmin/scconf.h>
#include <scadmin/scstat.h>

#include "clresource.h"
#include "rgm_support.h"
#include "clcommands.h"
#include "ClCommand.h"
#include "ClresourceExport.h"
#include "ClXml.h"

static clerrno_t
export_rs(ClresourceExport *rs_export, rgm_resource_t *rs, rgm_rg_t *rg,
    ValueList *rg_nl);

static clerrno_t
export_rs_property(ClresourceExport *rs_export, rgm_resource_t *rs,
    rgm_rg_t *rg, ValueList *rg_nl, rgm_property_t *rp,
    boolean_t extension);

//
// Fetch the export data according to operands.
//
clerrno_t
get_clresource_export_obj_select(ValueList &operands, optflgs_t optflgs,
    ClresourceExport *rs_export, int &no_res)
{
	clerrno_t first_err = CL_NOERR;
	int rv = 0;
	int prop_found = 0;
	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};
	char **rg_names = NULL;
	char **rs_names = (char **)0;
	char *oper1;
	clerrno_t errflg = CL_NOERR;
	clerrno_t clerrno = CL_NOERR;
	uint_t i, j, k;
	ValueList rslist;
	ValueList rts;
	rgm_resource_t *rs = (rgm_resource_t *)NULL;
	rgm_resource_t *rs1 = (rgm_resource_t *)NULL;
	char *r_rtname = (char *)NULL;
	scstat_errno_t scstat_status;
	int props_specified = 0;
	int verbose = 0;
	rgm_rg_t *rg = (rgm_rg_t *)NULL;
	nodeidlist_t *nl;
	char *nodename = NULL;
	ValueList *rg_nl = (ValueList *)NULL;
	ValueList rs_list;

	// Initialize ORB and other cluster checks
	clerrno = cluster_init();
	if (clerrno)
		return (clerrno);

	// Get the LH rt
	if (optflgs & LHflg)
		rts.add(get_latest_rtname(SCRGADM_RTLH_NAME, NULL));

	// Get the SA rt
	if (optflgs & SAflg)
		rts.add(get_latest_rtname(SCRGADM_RTSA_NAME, NULL));

	// Check the operands
	if (operands.getSize() > 0) {
		operands.start();
		oper1 = operands.getValue();
	} else
		oper1 = CLCOMMANDS_WILD_OPERAND;

	if (strcmp(oper1, CLCOMMANDS_WILD_OPERAND) == 0) {
		scha_status = scswitch_getrglist(&rg_names, B_FALSE, NULL);
		rv = scha_status.err_code;
		if (rv != SCHA_ERR_NOERR) {
			clerror("Failed to obtain list of all resource "
			    "groups.\n");
			print_rgm_error(scha_status, NULL);
			return (map_scha_error(rv));
		}

		// Make sure there are RG's configured.
		if (rg_names == NULL)
			return (CL_ENOENT);

		// Get the list of resources for this group
		for (i = 0; rg_names[i] != NULL; i++) {
			if (rs_names) {
				rgm_free_strarray(rs_names);
				rs_names = (char **)NULL;
			}
			if (rg) {
				rgm_free_rg(rg);
				rg = (rgm_rg_t *)NULL;
			}
			scha_status = rgm_scrgadm_getrgconf(rg_names[i],
				&rg, NULL);
			rv = scha_status.err_code;
			if (rv != SCHA_ERR_NOERR) {
				print_rgm_error(scha_status, NULL);
				clerror("Failed to obtain resource group "
				    "configuration for \"%s\".\n",
				    rg_names[i]);
				if (first_err == 0) {
					// first error!
					first_err = map_scha_error(rv);
				}
				continue;
			}
			if (rg_nl) {
				delete rg_nl;
				rg_nl = NULL;
			}
			rg_nl = new ValueList;
			for (nl = rg->rg_nodelist; nl; nl = nl->nl_next) {
				nodename = nodeidzone_to_nodename(
				    nl->nl_nodeid, nl->nl_zonename, &errflg);
				if (errflg)
					return (errflg);
				rg_nl->add(nodename);
				free(nodename);
			}

			scha_status = rgm_scrgadm_getrsrclist(rg_names[i],
			    &rs_names, NULL);
			rv = scha_status.err_code;
			if (rv != SCHA_ERR_NOERR) {
				print_rgm_error(scha_status, NULL);
				clerror("Failed to obtain list of all "
				    "resources in resource group "
				    "\"%s\".\n", rg_names[i]);
				if (first_err == 0) {
					// first error!
					first_err = map_scha_error(rv);
				}
				continue;
			}
			if (rs_names == NULL)
				continue;

			for (j = 0; rs_names[j]; j++) {
				if (rs_list.isValue(rs_names[j]) == 1)
					continue;
				if (rs != NULL) {
					free(rs);
					rs = NULL;
				}
				scha_status = rgm_scrgadm_getrsrcconf(
				    rs_names[j], &rs, NULL);
				rv = scha_status.err_code;
				if (rv != SCHA_ERR_NOERR) {
					print_rgm_error(scha_status,
					    rs_names[j]);
					if (first_err == 0) {
						// first error!
						first_err = map_scha_error(rv);
					}
					continue;
				}
				if (rs == NULL) {
					clerror("Failed to access resource "
					    "\"%s\".\n", rs_names[j]);
					if (first_err == 0) {
						// first error!
						first_err = CL_ENOENT;
					}
					continue;
				}
				if (rts.getSize() > 0)
					if (match_type(rts, rs) == 0)
						continue;

				no_res++;

				clerrno = export_rs(rs_export, rs, rg, rg_nl);
				if (clerrno) {
					if (first_err == 0) {
						// first error!
						first_err = clerrno;
					}
					break;
				}
				//
				// All resources belonging to same group
				// should be exported too.
				//
				rs_list.add(rs_names[j]);
				for (k = 0; rs_names[k]; k++) {
					if (rs_list.isValue(rs_names[k]) == 1)
						continue;
					if (rs1 != NULL) {
						free(rs1);
						rs1 = NULL;
					}
					scha_status = rgm_scrgadm_getrsrcconf(
					    rs_names[k], &rs1, NULL);
					rv = scha_status.err_code;
					if (rv != SCHA_ERR_NOERR) {
						print_rgm_error(scha_status,
						    rs_names[k]);
						clerror("Resulting "
						    "clconfiguration file will"
						    " be incomplete.\n");
						if (first_err == 0) {
							// first error!
							first_err =
							    map_scha_error(rv);
						}
						continue;
					}
					if (rs1 == NULL) {
						clerror("Failed to access "
						    "resource \"%s\".\n",
						    rs_names[k]);
						clerror("Resulting "
						    "clconfiguration file will"
						    " be incomplete.\n");
						if (first_err == 0) {
							// first error!
							first_err = CL_ENOENT;
						}
						continue;
					}
					clerrno = export_rs(rs_export, rs1,
					    rg, rg_nl);
					if (clerrno) {
						if (first_err == 0) {
							// first error!
							first_err = clerrno;
						}
						continue;
					}
					rs_list.add(rs_names[k]);
				}
			}
		}
	} else {
		for (operands.start(); operands.end() != 1; operands.next()) {

			if (rs_list.isValue(operands.getValue()) == 1)
				continue;
			if (rs != NULL) {
				rgm_free_resource(rs);
				rs = NULL;
			}
			if (rg) {
				rgm_free_rg(rg);
				rg = (rgm_rg_t *)NULL;
			}
			scha_status = rgm_scrgadm_getrsrcconf(
			    operands.getValue(), &rs, NULL);
			rv = scha_status.err_code;
			if (rv != SCHA_ERR_NOERR) {
				print_rgm_error(scha_status,
				    operands.getValue());
				if (first_err == 0) {
					// first error!
					first_err = map_scha_error(rv);
				}
				continue;
			}
			if (rs == NULL) {
				clerror("Failed to access resource \"%s\".\n",
				    operands.getValue());
				if (first_err == 0) {
					// first error!
					first_err = CL_ENOENT;
				}
				continue;
			}
			scha_status = rgm_scrgadm_getrgconf(rs->r_rgname,
				&rg, NULL);
			rv = scha_status.err_code;
			if (rv != SCHA_ERR_NOERR) {
				print_rgm_error(scha_status, NULL);
				clerror("Failed to obtain resource group "
				    "configuration for \"%s\".\n",
				    rs->r_rgname);
				if (first_err == 0) {
					// first error!
					first_err = map_scha_error(rv);
				}
				continue;
			}

			if (rg_nl) {
				delete rg_nl;
				rg_nl = NULL;
			}
			rg_nl = new ValueList;
			for (nl = rg->rg_nodelist; nl; nl = nl->nl_next) {
				nodename = nodeidzone_to_nodename(
				    nl->nl_nodeid, nl->nl_zonename, &errflg);
				if (errflg)
					return (errflg);
				rg_nl->add(nodename);
				free(nodename);
			}

			if (rts.getSize() > 0) {
				if (match_type(rts, rs) == 0) {
					if (optflgs & LHflg) {
						clerror("Specified resource "
						    "\"%s\" is not a logical "
						    "hostname resource.\n",
						    operands.getValue());
					} else if (optflgs & SAflg) {
						clerror("Specified resource "
						    "\"%s\" is not a shared "
						    "address resource.\n",
						    operands.getValue());
					} else {
						clcommand_perror(
						    CL_EINTERNAL);
						if (!first_err) {
							first_err =
							    CL_EINTERNAL;
						}
					}
					if (first_err == 0) {
						// first error!
						first_err = CL_EINVAL;
					}
					continue;
				}
			}

			no_res++;

			clerrno = export_rs(rs_export, rs, rg, rg_nl);
			if (clerrno) {
				if (first_err == 0) {
					// first error!
					first_err = clerrno;
				}
				break;
			}

			//
			// All resources belonging to same group
			// should be exported too.
			//
			rs_list.add(operands.getValue());
			if (rs_names) {
				rgm_free_strarray(rs_names);
				rs_names = (char **)NULL;
			}
			scha_status = rgm_scrgadm_getrsrclist(rs->r_rgname,
			    &rs_names, NULL);
			rv = scha_status.err_code;
			if (rv != SCHA_ERR_NOERR) {
				print_rgm_error(scha_status, NULL);
				clerror("Failed to obtain list of all "
				    "resources in resource group "
				    "\"%s\".\n", rs->r_rgname);
				if (first_err == 0) {
					// first error!
					first_err = map_scha_error(rv);
				}
				continue;
			}
			if (rs_names == NULL)
				continue;
			for (k = 0; rs_names[k]; k++) {
				if (rs_list.isValue(rs_names[k]) == 1)
					continue;
				if (rs1 != NULL) {
					free(rs1);
					rs1 = NULL;
				}
				scha_status = rgm_scrgadm_getrsrcconf(
				    rs_names[k], &rs1, NULL);
				rv = scha_status.err_code;
				if (rv != SCHA_ERR_NOERR) {
					print_rgm_error(scha_status,
					    rs_names[k]);
					clerror("Resulting "
					    "clconfiguration file will"
					    " be incomplete.\n");
					if (first_err == 0) {
						// first error!
						first_err =
						    map_scha_error(rv);
					}
					continue;
				}
				if (rs1 == NULL) {
					clerror("Failed to access "
					    "resource \"%s\".\n",
					    rs_names[k]);
					clerror("Resulting "
					    "clconfiguration file will"
					    " be incomplete.\n");
					if (first_err == 0) {
						// first error!
						first_err = CL_ENOENT;
					}
					continue;
				}
				clerrno = export_rs(rs_export, rs1,
				    rg, rg_nl);
				if (clerrno) {
					if (first_err == 0) {
						// first error!
						first_err = clerrno;
					}
					continue;
				}
				rs_list.add(rs_names[k]);
			}
		}
	}

	if (rg_names)
		rgm_free_strarray(rg_names);
	if (rs_names)
		rgm_free_strarray(rs_names);
	if (rs)
		free(rs);
	if (rs1)
		free(rs1);
	if (rg_nl)
		delete rg_nl;
	return (first_err);
}

//
// Add a resource to the export object
//
static clerrno_t
export_rs(ClresourceExport *rs_export, rgm_resource_t *rs, rgm_rg_t *rg,
    ValueList *rg_nl)
{
	char *ptr, *ptr2, *zone, *node;
	int rv = 0;
	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};
	rgm_property_list_t *listp;
	int node_num;
	scha_switch_t swtch;
	clerrno_t clerrno = CL_NOERR;

	// Create the resource
	rs_export->addResource(rs->r_name, rs->r_type, rs->r_rgname);

	// Set the standard resource properties
	rs_export->addStandardProperty(rs->r_name, "R_description",
	    rs->r_description, NULL);

	ptr2 = NULL;
	ptr = NULL;
	if (rs->r_project_name && *(rs->r_project_name) != '\0')
		ptr = rs->r_project_name;
	else {
		//
		// Resource project name is not set; get the default project
		// name of the containing RG
		//
		scha_status = rgm_scrgadm_get_default_proj(rs->r_rgname,
			&ptr2, NULL);
		if (scha_status.err_code == SCHA_ERR_NOERR)
			ptr = ptr2;
	}

	rs_export->addStandardProperty(rs->r_name,
	    "Resource_project_name", ptr, NULL);

	if (ptr2)
		free(ptr2);

	ptr = NULL;
	if (rs->r_dependencies.dp_strong) {
		ptr = scrgadm_rdep_list_to_string(
		    rs->r_dependencies.dp_strong, "<NULL>", ",");
	}
	rs_export->addStandardProperty(rs->r_name,
	    "Resource_dependencies", ptr, NULL);

	ptr = NULL;
	if (rs->r_dependencies.dp_weak) {
		ptr = scrgadm_rdep_list_to_string(
		    rs->r_dependencies.dp_weak, "<NULL>", ",");
	}
	rs_export->addStandardProperty(rs->r_name,
	    "Resource_dependencies_weak", ptr, NULL);

	ptr = NULL;
	if (rs->r_dependencies.dp_restart) {
		ptr = scrgadm_rdep_list_to_string(
		    rs->r_dependencies.dp_restart, "<NULL>", ",");
	}
	rs_export->addStandardProperty(rs->r_name,
	    "Resource_dependencies_restart", ptr, NULL);

	ptr = NULL;
	if (rs->r_dependencies.dp_offline_restart) {
		ptr = scrgadm_rdep_list_to_string(
		    rs->r_dependencies.dp_offline_restart,
		    "<NULL>", ",");
	}
	rs_export->addStandardProperty(rs->r_name,
	    "Resource_dependencies_offline_restart", ptr, NULL);

	// Set the resource node info and state
	for (rg_nl->start(), node_num = 1; rg_nl->end() != 1;
	    rg_nl->next(), node_num++) {

		node = new char[strlen(rg_nl->getValue()) + 1];
		strcpy(node, rg_nl->getValue());
		zone = NULL;
		if (strchr(node, ':')) {
			char *pch = strchr(node, ':');

			// Set the zone name
			zone = new char[strlen(node)-(pch-node+1)+1];
			for (int i = 1; i < strlen(pch); i++) {
				zone[i-1] = pch[i];
				zone[i] = '\0';
			}

			// Truncate the node name
			node[pch-node] = '\0';
		}
		state_t state = DISABLED;
		bool monitored = false;

		// state
		scha_status = get_swtch(rs->r_onoff_switch,
		    rg_nl->getValue(), &swtch);
		rv = scha_status.err_code;
		if (rv != SCHA_ERR_NOERR) {
			print_rgm_error(scha_status,
			    NULL);
			clerrno = map_scha_error(rv);
			goto cleanup;
		}

		switch (swtch) {
		case SCHA_SWITCH_ENABLED:
			state = ENABLED;
			break;

		case SCHA_SWITCH_DISABLED:
			break;

		default:
			clcommand_perror(CL_EINTERNAL);
			if (!clerrno)
				clerrno = CL_EINTERNAL;
		}

		// monitored
		scha_status = get_swtch(rs->r_monitored_switch,
		    rg_nl->getValue(), &swtch);
		rv = scha_status.err_code;
		if (rv != SCHA_ERR_NOERR) {
			print_rgm_error(scha_status,
			    NULL);
			clerrno = map_scha_error(rv);
			goto cleanup;
		}

		switch (swtch) {
		case SCHA_SWITCH_ENABLED:
			monitored = true;
			break;

		case SCHA_SWITCH_DISABLED:
			break;

		default:
			clcommand_perror(CL_EINTERNAL);
			if (!clerrno)
				clerrno = CL_EINTERNAL;
		}

		rs_export->addResourceNode(rs->r_name, node, state,
		    monitored, zone);

		node = NULL;
		zone = NULL;

		delete node;
		delete zone;
	}

	// Standard properties
	for (listp = rs->r_properties; listp;  listp = listp->rpl_next) {
		clerrno = export_rs_property(rs_export, rs, rg, rg_nl,
		    listp->rpl_property, B_FALSE);
		if (clerrno)
			goto cleanup;
	}

	// Extension properties
	for (listp = rs->r_ext_properties; listp;  listp = listp->rpl_next) {
		clerrno = export_rs_property(rs_export, rs, rg, rg_nl,
		    listp->rpl_property, B_TRUE);
		if (clerrno)
			goto cleanup;
	}

cleanup:
	node = NULL;
	zone = NULL;

	delete node;
	delete zone;

	return (clerrno);
}

//
// Handle a resource property
//
static clerrno_t
export_rs_property(ClresourceExport *rs_export, rgm_resource_t *rs,
    rgm_rg_t *rg, ValueList *rg_nl, rgm_property_t *rp, boolean_t extension)
{
	char *ptr, *ptr2;
	clerrno_t errflg = CL_NOERR;
	int rv = 0;
	char *value = NULL;
	char *typestr = (char *)NULL;
	int node_num;
	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};
	clerrno_t clerrno = CL_NOERR;
	char *prop_node = (char *)NULL;

	// Check arguments
	if (rp == NULL || rp->rp_key == NULL || *rp->rp_key == NULL)
		return (CL_NOERR);

	switch (rp->rp_type) {
	case SCHA_PTYPE_STRING:
		if (!typestr)
			typestr = "string";
	case SCHA_PTYPE_INT:
		if (!typestr)
			typestr = "int";
	case SCHA_PTYPE_BOOLEAN:
		if (!typestr)
			typestr = "boolean";
	case SCHA_PTYPE_ENUM:
		if (!typestr)
			typestr = "enum";

		if (rp->is_per_node) {
			for (rg_nl->start(), node_num = 1;
			    rg_nl->end() != 1;
			    rg_nl->next(), node_num++) {
				if (value) {
					free(value);
					value = NULL;
				}
				scha_status = get_value(rp->rp_value,
				    rg_nl->getValue(),
				    &value, B_TRUE);
				rv = scha_status.err_code;
				if (rv != SCHA_ERR_NOERR) {
					print_rgm_error(scha_status,
					    NULL);
					clerrno = map_scha_error(rv);
					goto cleanup;
				}
				if (prop_node) {
					free(prop_node);
					prop_node = NULL;
				}
				clerrno = get_prop_node(rp->rp_key,
				    rg_nl->getValue(), &prop_node);
				if (clerrno) {
					goto cleanup;
				}

				// Add the property to the node
				rs_export->addExtensionProperty(
				    rs->r_name, prop_node,
				    value, typestr, extension);
			}
		} else {
			if (value) {
				free(value);
				value = NULL;
			}
			scha_status = get_value(rp->rp_value, NULL,
			    &value, B_FALSE);
			rv = scha_status.err_code;
			if (rv != SCHA_ERR_NOERR) {
				print_rgm_error(scha_status, NULL);
				clerrno = map_scha_error(rv);
				goto cleanup;
			}

			if (extension) {
				rs_export->addExtensionProperty(
				    rs->r_name, rp->rp_key,
				    value, typestr);
			} else {
				rs_export->addStandardProperty(
				    rs->r_name, rp->rp_key,
				    value, typestr);
			}
		}
		break;

	case SCHA_PTYPE_STRINGARRAY:

		typestr = "stringarray";
		ptr2 = NULL;
		//
		// If NRU is not set then compute the NRU from the
		// dependency list and display it
		//
		if ((strcasecmp(SCHA_NETWORK_RESOURCES_USED,
		    rp->rp_key) == 0) &&
		    (rp->rp_array_values == NULL))
			compute_NRUlist_frm_deplist(rs,
			    &(rp->rp_array_values), NULL);
		if (rp->rp_array_values) {
			ptr2 = names_to_str_com(rp->rp_array_values,
			    rp->rp_key, &errflg);
			ptr = (ptr2) ? ptr2 : "";
		} else
			ptr = "";

		if (extension) {
			rs_export->addExtensionProperty(
			    rs->r_name, rp->rp_key, ptr,
			    typestr);
		} else {
			rs_export->addStandardProperty(
			    rs->r_name, rp->rp_key, ptr,
			    typestr);
		}
		if (ptr2)
			free(ptr2);

		break;

	default:
		clerror("Could not get information for resource "
		    "property \"%s\".\n", rp->rp_key);
		clerrno = CL_EINTERNAL;
		break;
	}

cleanup:
	if (value)
		free(value);
	if (prop_node)
		free(prop_node);
	return (clerrno);
}

//
// This function returns a ClresourceExport object populated with
// all of the resource information from the cluster.  Caller is
// responsible for deleting the ClcmdExport object.
//
// The value of rs_types depends on the type of resources wanted.
//
//	All resources		- 0
//	Shared Address only	- 1
//	Logical Hostname only	- 2
//
ClcmdExport *
get_clresource_xml_elements(int rs_type)
{
	ValueList empty;
	int err;

	optflgs_t flags;
	switch (rs_type) {

	case 1:
		flags |= SAflg;
		break;
	case 2:
		flags |= LHflg;
		break;
	default:
		flags = 0;
	}

	ClresourceExport *rs_export = new ClresourceExport();
	(void) get_clresource_export_obj_select(empty, flags, rs_export, err);

	return (rs_export);
}

//
// clrs export
//
clerrno_t
clresource_export(ClCommand &cmd, ValueList &operands, optflgs_t optflgs,
    char *clconfig)
{
	int rv = 0;
	int err = 0;
	int no_res = 0;

	ClresourceExport *rs_export = new ClresourceExport();

	err = get_clresource_export_obj_select(operands, optflgs,
	    rs_export, no_res);
	if (err) {
		rv = err;
	}

	// Check for no res's
	if (!no_res) {
		return (rv);
	}

	ClXml xml;
	err = xml.createExportFile(CLRESOURCE_CMD, clconfig,
	    rs_export);

	if (err && !rv)
		rv = err;

	return (rv);
}
