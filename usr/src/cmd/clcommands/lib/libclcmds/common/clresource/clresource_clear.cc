//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clresource_clear.cc	1.16	08/07/24 SMI"

//
// Process clresource "clear"
//

#ifdef __cplusplus
#include <sys/os.h>
#endif

#include <scadmin/scswitch.h>
#include <scadmin/scrgadm.h>
#include <scadmin/scconf.h>
#include <scadmin/scstat.h>
#include "clcommands.h"
#include "ClCommand.h"

#include "clresource.h"
#include "rgm_support.h"
#include "common.h"

static int
reset_error(ClCommand &cmd, char *rsname, rgm_resource_t *rs, ValueList &nodes,
    boolean_t verbose, int last, char *zc_name);

//
// clresource "clear"
//
clerrno_t
clresource_clear(ClCommand &cmd, ValueList &operands, optflgs_t optflgs,
    ValueList &resourcegroups, ValueList &resourcetypes,
    resource_errflag_t errflag, ValueList &nodes)
{
	clerrno_t first_err = CL_NOERR;
	clerrno_t clerrno = CL_NOERR;
	int rv = 0;
	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};
	char **rg_names = NULL;
	char **rs_names = (char **)0;
	char *oper1;
	clerrno_t errflg = CL_NOERR;
	uint_t i, j, k;
	ValueList rslist;
	rgm_resource_t *rs = (rgm_resource_t *)NULL;
	char *r_rtname = (char *)NULL;
	ValueList good_rts;
	ValueList good_rgs;
	ValueList good_nodes, tmp_list;
	boolean_t verbose = B_FALSE;
	char **allrs_names = (char **)0;
	uint_t num_rs = 0;
	uint_t rs_idx = 0;
	rgm_resource_t **rsarray  = (rgm_resource_t **)0;
	int last;
	scconf_errno_t scconf_err = SCCONF_NOERR;
	boolean_t is_same = B_FALSE;
	char *zc_name, *rs_part, *zc_part;

	// Initializa ORB and other cluster checks
	clerrno = cluster_init();
	if (clerrno)
		return (clerrno);

	if (optflgs & vflg)
		verbose = B_TRUE;

	// Check whether any zone cluster names were specified.
	// And if they were specified, there should be only one zone cluster
	// across all the Rs.
	zc_name = NULL;
	rs_part = zc_part = NULL;
	clerrno = check_single_zc_specified(operands, &zc_name, &is_same);
	if (clerrno != CL_NOERR) {
		clcommand_perror(clerrno);
		return (clerrno);
	}
	// If multiple zone clusters were specified, report an error.
	if (is_same == B_FALSE) {
		clerror("This operation can be performed on only one "
		    "zone cluster at a time.\n");
		return (clerrno);
	}
#if (SOL_VERSION >= __s10)
	if (zc_name) {
		// If zone cluster name was specified, we have to ensure
		// that it is valid.
		// We have to check whether this operation is allowed for
		// the status of the zone cluster.
		clerrno = check_zc_operation(CL_OBJ_TYPE_RS, CL_OP_TYPE_CLEAR,
						zc_name);
		if (clerrno != CL_NOERR) {
			return (clerrno);
		}
	}
#endif

	// Check validity of nodes
	tmp_list.add(zc_name);
	clerrno = get_good_nodes(nodes, good_nodes, tmp_list);
	tmp_list.clear();
	if (clerrno)
		return (clerrno);

	// Check resourcetypes
	for (resourcetypes.start(); !resourcetypes.end();
	    resourcetypes.next()) {
		if (r_rtname) {
			free(r_rtname);
			r_rtname = 0;
		}
		clerrno = get_r_rtname(resourcetypes.getValue(), &r_rtname,
					zc_name);
		if (clerrno == 0)
			good_rts.add(r_rtname);
		else {
			if (first_err == 0) {
				// first error!
				first_err = clerrno;
			}
		}
	}
	if (r_rtname)
		free(r_rtname);

	if ((resourcetypes.getSize() > 0) && (good_rts.getSize() == 0))
		return (first_err);

	if (optflgs & LHflg)
		good_rts.add(get_latest_rtname(SCRGADM_RTLH_NAME, zc_name));

	if (optflgs & SAflg)
		good_rts.add(get_latest_rtname(SCRGADM_RTSA_NAME, zc_name));

	for (resourcegroups.start(); !resourcegroups.end();
	    resourcegroups.next()) {
		clerrno = check_rg(resourcegroups.getValue(), zc_name);
		if (clerrno == 0)
			good_rgs.add(resourcegroups.getValue());
		else {
			if (first_err == 0) {
				// first error!
				first_err = clerrno;
			}
		}
	}
	if ((resourcegroups.getSize() > 0) && (good_rgs.getSize() == 0))
		return (first_err);

	operands.start();
	// Have to parse for the ZC name and RS name
	scconf_err = scconf_parse_obj_name_from_cluster_scope(
			operands.getValue(), &oper1, &zc_part);
	// Free memory
	if (zc_part) {
		free(zc_part);
		zc_part = NULL;
	}
	if (scconf_err != SCCONF_NOERR) {
		clerrno = map_scconf_error(scconf_err);
		clcommand_perror(clerrno);
		return (clerrno);
	}
	if (strcmp(oper1, CLCOMMANDS_WILD_OPERAND) == 0) {
		if (good_rgs.getSize() > 0) {
			rg_names = vl_to_names(good_rgs, &errflg);
			if (errflg)
				return (errflg);
		} else {
			clerrno = z_getrglist(&rg_names, verbose,
			    optflgs & uflg, zc_name);
			if (clerrno != CL_NOERR) {
				return (clerrno);
			}
			// Remove the zone cluster context from all these
			// names.
			clerrno = remove_zc_names_from_names(rg_names);
			if (clerrno != CL_NOERR) {
				clcommand_perror(clerrno);
				return (clerrno);
			}
		}
	}

	// For accounting purpose, count the number of resources
	scha_status = rgm_scrgadm_getrsrclist((char *)NULL, &allrs_names,
						zc_name);
	rv = scha_status.err_code;
	if (rv != SCHA_ERR_NOERR) {
		print_rgm_error(scha_status, NULL);
		clerror("Failed to accesss resources in the cluster.\n");
		return (map_scha_error(rv));
	}
	j = 0;
	if (allrs_names != NULL) {
		for (j = 0; allrs_names[j]; j++);
	}
	num_rs = j + 1;
	rsarray = (rgm_resource_t **)calloc(num_rs, sizeof (rgm_resource_t *));
	if (rsarray == NULL) {
		clcommand_perror(CL_ENOMEM);
		return (CL_ENOMEM);
	}

	if ((strcmp(oper1, CLCOMMANDS_WILD_OPERAND) == 0) &&
	    (rg_names != (char **)NULL)) {

		// Get the list of resources for this group
		for (i = 0; rg_names[i] != NULL; i++) {
			if (rs_names) {
				rgm_free_strarray(rs_names);
				rs_names = (char **)NULL;
			}
			scha_status = rgm_scrgadm_getrsrclist(rg_names[i],
			    &rs_names, zc_name);
			rv = scha_status.err_code;
			if (rv != SCHA_ERR_NOERR) {
				print_rgm_error(scha_status, NULL);
				clerror("Failed to obtain list of all "
				    "resources in resource group "
				    "\"%s\".\n", rg_names[i]);
				clerror("Will not try to clear errors on "
				    "resources belonging to group "
				    "\"%s\".\n", rg_names[i]);
				if (first_err == 0) {
					// first error!
					first_err = map_scha_error(rv);
				}
				continue;
			}
			if (rs_names == NULL)
				continue;

			for (j = 0; rs_names[j]; j++) {
				rs = NULL;
				scha_status = rgm_scrgadm_getrsrcconf(
				    rs_names[j], &rs, zc_name);
				rv = scha_status.err_code;
				if (rv != SCHA_ERR_NOERR) {
					print_rgm_error(scha_status,
					    rs_names[j]);
					clerror("Error on resource \"%s\" "
					    "will not be cleared.\n",
					    rs_names[j]);
					if (first_err == 0) {
						// first error!
						first_err = map_scha_error(rv);
					}
					continue;
				}
				if (rs == NULL) {
					clerror("Failed to access resource "
					    "\"%s\", errors on resource will "
					    "not be cleared.\n", rs_names[j]);
					if (first_err == 0) {
						// first error!
						first_err = CL_ENOENT;
					}
					continue;
				}
				if (good_rts.getSize() > 0)
					if (match_type(good_rts, rs) == 0)
						continue;

				rsarray[rs_idx] = rs;
				rs_idx++;
			}
		}
	} else if (strcmp(oper1, CLCOMMANDS_WILD_OPERAND) != 0) {
		for (operands.start(); operands.end() != 1; operands.next()) {

			if (rs_part) {
				free(rs_part);
				rs_part = NULL;
			}
			if (zc_part) {
				free(zc_part);
				zc_part = NULL;
			}
			// First get the R name without the ZC name
			scconf_err = scconf_parse_obj_name_from_cluster_scope(
					operands.getValue(), &rs_part,
					&zc_part);

			if (scconf_err != SCCONF_NOERR) {
				clerror("Internal error.\n");
				if (first_err == CL_NOERR) {
					first_err = CL_EINTERNAL;
				}
				continue;
			}

			scha_status = rgm_scrgadm_getrsrcconf(
			    rs_part, &rs, zc_part);
			rv = scha_status.err_code;
			if (rv != SCHA_ERR_NOERR) {
				print_rgm_error(scha_status,
				    operands.getValue());
				clerror("Errors on resource \"%s\" will not "
				    "be cleared.\n", operands.getValue());
				if (first_err == 0) {
					// first error!
					first_err = map_scha_error(rv);
				}
				continue;
			}
			if (rs == NULL) {
				clerror("Failed to access resource \"%s\", "
				    "errors on resource will not be cleared.\n",
				    operands.getValue());
				if (first_err == 0) {
					// first error!
					first_err = CL_ENOENT;
				}
				continue;
			}
			if (good_rts.getSize() > 0) {
				if (match_type(good_rts, rs) == 0) {
					if (optflgs & LHflg)
						clerror("Specified resource "
						    "\"%s\" is not a logical "
						    "hostname resource.\n",
						    operands.getValue());
					if (optflgs & SAflg)
						clerror("Specified resource "
						    "\"%s\" is not a shared "
						    "address resource.\n",
						    operands.getValue());
					else
						clerror("Specified resource "
						    "\"%s\" does not meet the "
						    "criteria set by the "
						    "\"-t\" option.\n",
						    operands.getValue());
					if (first_err == 0) {
						// first error!
						first_err = CL_EINVAL;
					}
					continue;
				}
			}
			if (good_rgs.getSize() > 0) {
				if (match_group(good_rgs, rs) == 0) {
					clerror("Specified resource \"%s\" "
					    "does not meet the criteria set "
					    "by the \"-g\" option.\n",
					    operands.getValue());
					if (first_err == 0) {
						// first error!
						first_err = CL_EINVAL;
					}
					continue;
				}
			}
			rsarray[rs_idx] = rs;
			rs_idx++;
		}
	}

	for (i = 0; i < rs_idx; i++) {
		last = 1;
		for (j = i + 1; j < rs_idx; j++) {
			if (strcmp(rsarray[i]->r_rgname,
			    rsarray[j]->r_rgname) == 0) {
				last = 0;
			}
		}
		clerrno = reset_error(cmd, rsarray[i]->r_name, rsarray[i],
		    good_nodes, verbose, last, zc_name);
		if (clerrno != CL_NOERR) {
			if (first_err == 0) {
				// first error!
				first_err = clerrno;
			}
		}
	}

	if (rg_names)
		rgm_free_strarray(rg_names);
	if (rs_names)
		rgm_free_strarray(rs_names);
	for (i = 0; i < rs_idx; i++) {
		rgm_free_resource(rsarray[i]);
	}
	if (rs_part) {
		free(rs_part);
	}
	if (zc_part) {
		free(zc_part);
	}
	if (zc_name) {
		free(zc_name);
	}
	if (oper1) {
		free(oper1);
	}

	return (first_err);
}

static clerrno_t
reset_error(ClCommand &cmd, char *rsname, rgm_resource_t *rs, ValueList &nodes,
    boolean_t verbose, int last, char *zc_name)
{
	char *rsname_ptr[2];
	char **rsnodes = (char **)0;
	rgm_rg_t *rg = (rgm_rg_t *)0;
	clerrno_t errflg = CL_NOERR;
	int rv = 0;
	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};
	namelist_t *node_nl;
	boolean_t rs_err;

	if (strcmp(cmd.getSubCommandName(), "delete") == 0) {
		//
		// We might get called more than once during delete, we
		// don't want to repeat the operation if error is not set.
		//
		errflg = check_rs_error(rs, &rs_err, zc_name);
		if (errflg)
			return (errflg);
		if (rs_err == B_FALSE)
			return (CL_NOERR);
	}

	rsname_ptr[0] = rsname;
	rsname_ptr[1] = NULL;

	if (nodes.getSize() > 0) {
		rsnodes = vl_to_names(nodes, &errflg);
		if (errflg)
			return (errflg);
	} else {
		scha_status = rgm_scrgadm_getrgconf(rs->r_rgname, &rg,
							zc_name);
		rv = scha_status.err_code;
		if (rv != SCHA_ERR_NOERR) {
			print_rgm_error(scha_status, NULL);
			return (CL_ENOENT);
		}
		if (rg == NULL) {
			clerror("Failed to access resource group containing "
			    "\"%s\".\n", rsname);
			return (CL_ENOENT);
		}
		// convert rg nodeidlist to namelist
		node_nl = nodeidlist_to_namelist(rg->rg_nodelist,
		    &errflg, zc_name);
		if (errflg)
			return (errflg);

		rsnodes = namelist_to_names(node_nl, &errflg);
		if (errflg)
			return (errflg);

		if (node_nl)
			rgm_free_nlist(node_nl);
	}

	// Reset error
	scha_status = rgm_scswitch_clear((const char **) rsnodes,
	    (const char **) rsname_ptr, STOP_FAILED, zc_name);
	rv = scha_status.err_code;
	if (rv != SCHA_ERR_NOERR) {
		print_rgm_error(scha_status, NULL);
		// Free any memory
		if (scha_status.err_msg != NULL) {
			free(scha_status.err_msg);
		}
		return (map_scha_error(rv));
	} else {
		//
		// The msg on success is not interesting for delete users,
		// also, print the standard warnings only if this resource
		// is the last one belonging to the rg.
		//
		if ((scha_status.err_msg) &&
		    (strcmp(cmd.getSubCommandName(), "delete")) &&
		    (last == 1))
			fprintf(stderr, "%s\n",  scha_status.err_msg);
		if (verbose)
			clmessage("Attempt to clear error on \"%s\" "
			    "succeeded.\n", rsname);
	}
	if (rg)
		rgm_free_rg(rg);
	if (rsnodes)
		rgm_free_strarray(rsnodes);
	return (map_scha_error(rv));
}
