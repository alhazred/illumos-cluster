//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clresource_list_props.cc	1.13	08/09/22 SMI"

//
// Process clresource "list-props"
//

#ifdef __cplusplus
#include <sys/os.h>
#endif

#include <scadmin/scswitch.h>
#include <scadmin/scrgadm.h>
#include <scadmin/scconf.h>
#include <scadmin/scstat.h>
#include "clcommands.h"
#include "ClCommand.h"
#include "ClList.h"

#include "clresource.h"
#include "rgm_support.h"

static clerrno_t
get_prop_details(char *rtname, listtype_t listtype, int optflgs,
    ValueList &props_needed, ClList *print_list);

static boolean_t
composite_prop(char *prop);

//
// clresource "list-props"
//
clerrno_t
clresource_list_props(ClCommand &cmd, ValueList &operands, optflgs_t optflgs,
    ValueList &resourcegroups, ValueList &resourcetypes, listtype_t listtype,
    ValueList &properties, ValueList &xproperties, ValueList &yproperties)
{
	clerrno_t first_err = CL_NOERR;
	int first_rs = 0;
	int rv = 0;
	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};
	char **rg_names = NULL;
	char **rs_names = (char **)0;
	char **rsnames = (char **)0;
	char *oper1;
	clerrno_t errflg = CL_NOERR;
	clerrno_t clerrno = CL_NOERR;
	uint_t i, j, k;
	NameValueList rslist;
	rgm_resource_t *rs = (rgm_resource_t *)NULL;
	char *r_rtname = (char *)NULL;
	ValueList good_rts;
	ValueList good_rgs;
	ClList **list_ptr;
	char **rs_ptr;
	char *rsstring;
	int rslen = 0;
	ValueList uniq_rtlist;
	NameValueList proplist;
	ValueList props_needed;
	boolean_t prop_found = B_FALSE;
	boolean_t single_op = B_FALSE;
	char *nodepart = (char *)NULL;
	char *proppart = (char *)NULL;
	boolean_t pn_specified;
	boolean_t pn_matched;

	// Initializa ORB and other cluster checks
	clerrno = cluster_init();
	if (clerrno)
		return (clerrno);

	// Check properties
	clerrno = get_all_r_props(proplist);
	if (clerrno)
		return (clerrno);

	for (properties.start(); properties.end() != 1; properties.next()) {

		prop_found = B_FALSE;
		pn_matched = B_FALSE;
		clerrno = tok_prop(properties.getValue(), &pn_specified,
		    &proppart, &nodepart);
		if (clerrno) {
			if (clerrno == CL_ENOMEM) {
				return (CL_ENOMEM);
			}
			if (first_err == 0) {
				// first error!
				first_err = clerrno;
			}
		}
		if (composite_prop(proppart)) {
			clerror("The specified property \"%s\" is invalid "
			    "for this operation.\n", proppart);
			if (first_err == 0) {
				// first error!
				first_err = CL_EPROP;
			}
			if (proppart) {
				free(proppart);
				proppart = NULL;
			}
			if (nodepart) {
				free(nodepart);
				nodepart = NULL;
			}
			continue;
		}

		for (proplist.start(); proplist.end() != 1; proplist.next()) {
			if (strcasecmp(proplist.getName(), proppart) == 0) {
				prop_found = B_TRUE;
				if (pn_specified) {
					if (strstr(proplist.getValue(), "pn")) {
						pn_matched = B_TRUE;
						break;
					}
				} else {
					break;
				}
			}
		}
		if (pn_specified && !pn_matched && prop_found) {
			clwarning("A node-specifier is not appropriate for "
			    "the specified property \"%s\".\n", proppart);
		}
		if (!prop_found) {
			clerror("Specified property \"%s\" is not valid for "
			    "any of the registered resource types.\n",
			    properties.getValue());
			if (first_err == 0) {
				// first error!
				first_err = CL_EPROP;
			}
		} else {
			props_needed.add(proppart);
		}
		if (proppart) {
			free(proppart);
			proppart = NULL;
		}
		if (nodepart) {
			free(nodepart);
			nodepart = NULL;
		}
	}

	for (xproperties.start(); xproperties.end() != 1; xproperties.next()) {
		prop_found = B_FALSE;
		pn_matched = B_FALSE;
		clerrno = tok_prop(xproperties.getValue(), &pn_specified,
		    &proppart, &nodepart);
		if (clerrno) {
			if (clerrno == CL_ENOMEM) {
				return (CL_ENOMEM);
			}
			if (first_err == 0) {
				// first error!
				first_err = clerrno;
			}
		}
		for (proplist.start(); proplist.end() != 1; proplist.next()) {
			if (strncasecmp(proplist.getValue(), "standard",
			    strlen("standard")) == 0)
				continue;

			if (strcasecmp(proplist.getName(), proppart) == 0) {
				prop_found = B_TRUE;
				if (pn_specified) {
					if (strstr(proplist.getValue(), "pn")) {
						pn_matched = B_TRUE;
						break;
					}
				} else {
					break;
				}
			}
		}
		if (pn_specified && !pn_matched && prop_found) {
			clwarning("A node-specifier is not appropriate for "
			    "the specified property \"%s\".\n", proppart);
		}
		if (!prop_found) {
			clerror("Specified extension property \"%s\" "
			    "is not valid for any of the registered resource "
			    "types.\n", xproperties.getValue());
			if (first_err == 0) {
				// first error!
				first_err = CL_EPROP;
			}
		} else {
			props_needed.add(proppart);
		}
		if (proppart) {
			free(proppart);
			proppart = NULL;
		}
		if (nodepart) {
			free(nodepart);
			nodepart = NULL;
		}
	}

	for (yproperties.start(); yproperties.end() != 1; yproperties.next()) {
		prop_found = B_FALSE;
		clerrno = tok_prop(yproperties.getValue(), &pn_specified,
		    &proppart, &nodepart);
		if (clerrno) {
			if (clerrno == CL_ENOMEM) {
				return (CL_ENOMEM);
			}
			if (first_err == 0) {
				// first error!
				first_err = clerrno;
			}
		}
		if (composite_prop(proppart)) {
			clerror("The specified property \"%s\" is invalid "
			    "for this operation.\n", proppart);
			if (first_err == 0) {
				// first error!
				first_err = CL_EPROP;
			}
			if (proppart) {
				free(proppart);
				proppart = NULL;
			}
			if (nodepart) {
				free(nodepart);
				nodepart = NULL;
			}
			continue;
		}
		for (proplist.start(); proplist.end() != 1; proplist.next()) {
			if (strncmp(proplist.getValue(), "extension",
			    strlen("extension")) == 0)
				continue;

			if (strcasecmp(proplist.getName(), proppart) == 0) {
				prop_found = B_TRUE;
				if (pn_specified) {
					if (strstr(proplist.getValue(), "pn")) {
						pn_matched = B_TRUE;
						break;
					}
				} else {
					break;
				}
			}
		}
		if (pn_specified && !pn_matched && prop_found) {
			clwarning("The specified property \"%s\" may not "
			    "accept per-node values.\n", proppart);
		}
		if (!prop_found) {
			clerror("Specified standard property \"%s\" "
			    "is not valid for any of the registered resource "
			    "types.\n", yproperties.getValue());
			if (first_err == 0) {
				// first error!
				first_err = CL_EPROP;
			}
		} else {
			props_needed.add(proppart);
		}
		if (proppart) {
			free(proppart);
			proppart = NULL;
		}
		if (nodepart) {
			free(nodepart);
			nodepart = NULL;
		}
	}

	if ((properties.getSize() > 0) || (xproperties.getSize() > 0) ||
	    (yproperties.getSize() > 0)) {
		if (props_needed.getSize() == 0) {
			return (first_err);
		}
	}

	// Check resourcetypes
	for (resourcetypes.start(); !resourcetypes.end();
	    resourcetypes.next()) {
		if (r_rtname) {
			free(r_rtname);
			r_rtname = 0;
		}
		clerrno = get_r_rtname(resourcetypes.getValue(), &r_rtname);
		if (clerrno == 0)
			good_rts.add(r_rtname);
		else {
			if (first_err == 0) {
				// first error!
				first_err = clerrno;
			}
		}
	}
	if (r_rtname)
		free(r_rtname);

	if ((resourcetypes.getSize() > 0) && (good_rts.getSize() == 0))
		return (first_err);

	if (optflgs & LHflg)
		good_rts.add(get_latest_rtname(SCRGADM_RTLH_NAME, NULL));

	if (optflgs & SAflg)
		good_rts.add(get_latest_rtname(SCRGADM_RTSA_NAME, NULL));

	for (resourcegroups.start(); !resourcegroups.end();
	    resourcegroups.next()) {
		clerrno = check_rg(resourcegroups.getValue(), NULL);
		if (clerrno == 0)
			good_rgs.add(resourcegroups.getValue());
		else {
			if (first_err == 0) {
				// first error!
				first_err = clerrno;
			}
		}
	}
	if ((resourcegroups.getSize() > 0) && (good_rgs.getSize() == 0))
		return (first_err);

	if (operands.getSize() > 0) {
		operands.start();
		oper1 = operands.getValue();
		if ((operands.getSize() == 1) &&
		    (strcmp(oper1, CLCOMMANDS_WILD_OPERAND) != 0)) {
			single_op = B_TRUE;
		}
	} else
		oper1 = CLCOMMANDS_WILD_OPERAND;

	if (strcmp(oper1, CLCOMMANDS_WILD_OPERAND) == 0) {
		if (good_rgs.getSize() > 0) {
			rg_names = vl_to_names(good_rgs, &errflg);
			if (errflg)
				return (errflg);
		} else {
			scha_status = scswitch_getrglist(&rg_names,
				B_FALSE, NULL);
			rv = scha_status.err_code;
			if (rv != SCHA_ERR_NOERR) {
				clerror("Failed to obtain list of all resource "
				    "groups.\n");
				print_rgm_error(scha_status, NULL);
				return (map_scha_error(rv));
			}
		}
	}

	if ((strcmp(oper1, CLCOMMANDS_WILD_OPERAND) == 0) &&
	    (rg_names != (char **)NULL)) {
		// Get the list of resources for this group
		for (i = 0; rg_names[i] != NULL; i++) {
			if (rs_names) {
				rgm_free_strarray(rs_names);
				rs_names = (char **)NULL;
			}
			scha_status = rgm_scrgadm_getrsrclist(rg_names[i],
			    &rs_names, NULL);
			rv = scha_status.err_code;
			if (rv != SCHA_ERR_NOERR) {
				print_rgm_error(scha_status, NULL);
				clerror("Failed to obtain list of all "
				    "resources in resource group "
				    "\"%s\".\n", rg_names[i]);
				if (first_err == 0) {
					// first error!
					first_err = map_scha_error(rv);
				}
				continue;
			}
			if (rs_names == NULL)
				continue;
			for (j = 0; rs_names[j]; j++) {
				if (rs != NULL) {
					free(rs);
					rs = NULL;
				}
				scha_status = rgm_scrgadm_getrsrcconf(
				    rs_names[j], &rs, NULL);
				rv = scha_status.err_code;
				if (rv != SCHA_ERR_NOERR) {
					print_rgm_error(scha_status,
					    rs_names[j]);
					if (first_err == 0) {
						// first error!
						first_err = map_scha_error(rv);
					}
					continue;
				}
				if (rs == NULL) {
					clerror("Failed to access resource "
					    "\"%s\".\n", rs_names[j]);
					if (first_err == 0) {
						// first error!
						first_err = CL_ENOENT;
					}
					continue;
				}
				if (good_rts.getSize() > 0)
					if (match_type(good_rts, rs) == 0)
						continue;

				rslist.add(rs_names[j], rs->r_type);
			}
		}
	} else if (strcmp(oper1, CLCOMMANDS_WILD_OPERAND) != 0) {
		for (operands.start(); operands.end() != 1; operands.next()) {

			if (rs != NULL) {
				free(rs);
				rs = NULL;
			}
			scha_status = rgm_scrgadm_getrsrcconf(
			    operands.getValue(), &rs, NULL);
			rv = scha_status.err_code;
			if (rv != SCHA_ERR_NOERR) {
				print_rgm_error(scha_status,
				    operands.getValue());
				if (first_err == 0) {
					// first error!
					first_err = map_scha_error(rv);
				}
				continue;
			}
			if (rs == NULL) {
				clerror("Failed to access resource \"%s\".\n");
				if (first_err == 0) {
					// first error!
					first_err = CL_ENOENT;
				}
				continue;
			}
			if (good_rts.getSize() > 0) {
				if (match_type(good_rts, rs) == 0) {
					if (optflgs & LHflg)
						clerror("Specified resource "
						    "\"%s\" is not a logical "
						    "hostname resource.\n",
						    operands.getValue());
					else if (optflgs & SAflg)
						clerror("Specified resource "
						    "\"%s\" is not a shared "
						    "address resource.\n",
						    operands.getValue());
					else
						clerror("Specified resource "
						    "\"%s\" does not meet the "
						    "criteria set by the "
						    "\"-t\" option.\n",
						    operands.getValue());
					if (first_err == 0) {
						// first error!
						first_err = CL_EINVAL;
					}
					continue;
				}
			}
			if (good_rgs.getSize() > 0) {
				if (match_group(good_rgs, rs) == 0) {
					clerror("Specified resource \"%s\" "
					    "does not meet the criteria set "
					    "by the \"-g\" option.\n",
					    operands.getValue());
					if (first_err == 0) {
						// first error!
						first_err = CL_EINVAL;
					}
					continue;
				}
			}
			rslist.add(operands.getValue(), rs->r_type);
		}
	}

	rslen = 0;
	for (rslist.start(); rslist.end() != 1; rslist.next()) {
		if (uniq_rtlist.isValue(rslist.getValue()) == 0)
			uniq_rtlist.add(rslist.getValue());
		//
		// Save the max size of all rs names, with some buffer for
		// separators (at present comma and space).
		//
		rslen += (strlen(rslist.getName()) + 4);
	}

	list_ptr = (ClList **) calloc(uniq_rtlist.getSize(), sizeof (ClList *));
	if (list_ptr == (ClList **) NULL) {
		clcommand_perror(CL_ENOMEM);
		return (CL_ENOMEM);
	}

	rs_ptr = (char **)calloc(uniq_rtlist.getSize(), sizeof (char *));
	if (rs_ptr == (char **)NULL) {
		clcommand_perror(CL_ENOMEM);
		return (CL_ENOMEM);
	}

	for (k = 0, uniq_rtlist.start(); uniq_rtlist.end() != 1;
	    uniq_rtlist.next(), k++) {
		ClList *print_list = new ClList(cmd.isVerbose ? true : false);

		rsstring = (char *)calloc(rslen, sizeof (char));
		if (rsstring == (char *)NULL) {
			clcommand_perror(CL_ENOMEM);
			return (CL_ENOMEM);
		}
		first_rs = 1;
		for (rslist.start(); rslist.end() != 1; rslist.next()) {
			if (strcmp(uniq_rtlist.getValue(), rslist.getValue())
			    == 0) {
				if (first_rs) {
					strcat(rsstring, rslist.getName());
					first_rs = 0;
				} else {
					strcat(rsstring, ", ");
					strcat(rsstring, rslist.getName());
				}
			}
		}
		print_list->setHeadings(
		    (char *)gettext("Property Name"),
		    (char *)gettext("Description"));
		(void) get_prop_details(uniq_rtlist.getValue(), listtype,
		    optflgs, props_needed, print_list);

		list_ptr[k] = print_list;
		rs_ptr[k] = rsstring;
	}

	for (k = 0, uniq_rtlist.start(); uniq_rtlist.end() != 1;
	    uniq_rtlist.next(), k++) {
		if (!(optflgs & LHflg) &&
		    !(optflgs & SAflg) &&
		    (single_op == B_FALSE)) {
			clmessage("\n=== Properties for resource %s ===\n",
			    rs_ptr[k]);
		}
		list_ptr[k]->print();
	}

	for (k = 0, uniq_rtlist.start(); uniq_rtlist.end() != 1;
	    uniq_rtlist.next(), k++) {
		delete list_ptr[k];
		free(rs_ptr[k]);
	}

	if (rg_names)
		rgm_free_strarray(rg_names);
	if (rs_names)
		rgm_free_strarray(rs_names);
	if (rs)
		free(rs);

	return (first_err);
}

static clerrno_t
get_prop_details(char *rtname, listtype_t listtype, int optflgs,
    ValueList &props_needed, ClList *print_list)
{
	rgm_rt_t *rt = (rgm_rt_t *)0;
	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};
	rgm_param_t **tablep;
	int rv = 0;

	// Get configuration for this rt name
	scha_status = rgm_scrgadm_getrtconf(rtname, &rt, NULL);
	rv = scha_status.err_code;
	if (rv != SCHA_ERR_NOERR) {
		print_rgm_error(scha_status, rtname);
		return (CL_EINTERNAL);
	}
	if (rt == NULL) {
		clerror("Failed to access resource type information for "
		    "\"%s\".\n", rtname);
		return (CL_EINTERNAL);
	}

	if (!(rt->rt_paramtable && *rt->rt_paramtable))
		return (CL_NOERR);

	// First add the extension properties
	if (listtype != LISTTYPE_STANDARD) {
		for (tablep = rt->rt_paramtable; *tablep && (*tablep)->p_name;
		    ++tablep) {
			if ((*tablep)->p_extension == B_FALSE) {
				continue;
			}

			// If specified by user, just add it
			if (props_needed.isValue((*tablep)->p_name) == 1) {
				print_list->addRow((*tablep)->p_name,
				    (*tablep)->p_description);
			} else {
				if (props_needed.getSize() == 0) {
					print_list->addRow((*tablep)->p_name,
					    (*tablep)->p_description);
				}
			}
		}
	}

	// Then the standard properties
	if ((listtype != LISTTYPE_EXTENSION) ||
	    (props_needed.isValue("Type_version") == 1))
		print_list->addRow("Type_version",
		    (char *)gettext(
		    "The version of the resource type currently "
		    "associated with this resource"));

	if ((listtype != LISTTYPE_EXTENSION) ||
	    (props_needed.isValue("R_description") == 1))
		print_list->addRow("R_description",
		    (char *)gettext("A brief description of the resource"));

	if ((listtype != LISTTYPE_EXTENSION) ||
	    (props_needed.isValue("Resource_project_name") == 1))
		print_list->addRow("Resource_project_name",
		    (char *)gettext(
		    "The Solaris project name associated with the "
		    "resource"));

	if ((listtype != LISTTYPE_EXTENSION) ||
	    (props_needed.isValue("Resource_dependencies") == 1))
		print_list->addRow("Resource_dependencies",
		    (char *)gettext(
		    "A list of resources upon which this resource has "
		    "a strong dependency"));

	if ((listtype != LISTTYPE_EXTENSION) ||
	    (props_needed.isValue("Resource_dependencies_restart") == 1))
		print_list->addRow("Resource_dependencies_restart",
		    (char *)gettext(
		    "A list of resources upon which this resource has "
		    "a restart dependency"));

	if ((listtype != LISTTYPE_EXTENSION) ||
	    (props_needed.isValue("Resource_dependencies_weak") == 1))
		print_list->addRow("Resource_dependencies_weak",
		    (char *)gettext(
		    "A list of resources upon which this resource has "
		    "a weak dependency"));

	if ((listtype != LISTTYPE_EXTENSION) ||
	    (props_needed.isValue("Resource_dependencies_offline_restart")
	    == 1))
		print_list->addRow("Resource_dependencies_offline_restart",
		    (char *)gettext(
		    "A list of resources upon which this resource has "
		    "a offline-restart dependency"));

	if (listtype != LISTTYPE_EXTENSION) {
		for (tablep = rt->rt_paramtable; *tablep && (*tablep)->p_name;
		    ++tablep) {
			if ((*tablep)->p_extension == B_TRUE) {
				continue;
			}

			// If specified by user, just add it
			if (props_needed.isValue((*tablep)->p_name) == 1) {
				print_list->addRow((*tablep)->p_name,
				    (*tablep)->p_description);
			} else {
				if (props_needed.getSize() == 0) {
					print_list->addRow((*tablep)->p_name,
					    (*tablep)->p_description);
				}
			}
		}
	}

	rgm_free_rt(rt);
	return (CL_NOERR);
}

boolean_t
composite_prop(char *prop)
{
	if ((strcasecmp(prop, "enabled") == 0) ||
	    (strcasecmp(prop, "on_off_switch") == 0) ||
	    (strcasecmp(prop, "monitored") == 0) ||
	    (strcasecmp(prop, "monitored_switch") == 0) ||
	    (strcasecmp(prop, "group") == 0) ||
	    (strcasecmp(prop, "type") == 0)) {
		return (B_TRUE);
	} else {
		return (B_FALSE);
	}
}
