//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// ident	"@(#)clnode_xml.cc	1.3	08/05/20 SMI"
//

#include "ClXml.h"
#include "clnode.h"

char *getCableEndpoints(xmlNode *cable);

//
// Returns a ValueList of xpath statements specific to cluster nodes
//
ValueList *
ClXml::clnodeFilter(OptionValues *opts)
{
	ValueList *xpath = new ValueList();

	// Number of xpath statements is always one
	char *buffer = new char [BUFSIZ];

	sprintf(buffer, "//nodeList/*[@name=\"%s\"]",
	    opts->getOperand());

	xpath->add(buffer);

	return (xpath);
}

//
// Processes the node information from the XML file and the command
// line options, and calls the function to add the node.
//
int
ClXml::clnodeProcessor(xmlNode *node, OptionValues *opts)
{
	ClCommand cmd;
	ValueList operands;
	char *node_name = (char *)0;
	char *tmp_buf;
	cltr_cable_t *cable_props = NULL;
	int ep_length = 0;

	// Set verbose
	if (opts->optflgs & vflg) {
		cmd.isVerbose = 1;
	}

	// Set command arguments
	cmd.argc = opts->argc;
	cmd.argv = opts->argv;

	// Set verbose mode
	if (opts->optflgs & vflg) {
		cmd.isVerbose = 1;
	}

	// Set the name of the node
	node_name = (char *)xmlGetProp(node, (unsigned char *)"name");
	if (node_name == NULL) {
		clerror("Cannot find the node object.\n");
		return (CL_EINTERNAL);
	}
	operands.add(node_name);

	// Get node type
	char *node_type = opts->getOption(CLNODE_TYPE);
	if (node_type && (strcmp(node_type, NODETYPE_FARM) == 0)) {
		// Check if this is a farm node
		if (node && (strcmp((char *)node->name,	"node") == 0)) {
			// This is a cluster node. Just ignore it.
			return (CL_NOERR);
		}

		// Process farm node
		NameValueList *NVprop = opts->getNVOptions(CLNODE_PROP);
		int found = 0;

		// Check the adapterlist
		if (NVprop) {
			for (NVprop->start(); !NVprop->end(); NVprop->next()) {
				char *prop_name = NVprop->getName();

				if (prop_name &&
				    (strcmp(prop_name,
				    NODEPROP_ADAPTERS) == 0)) {
					found++;
					break;
				}
			}
		}
		if (!found) {
			// Get it from the xml node
			xmlNode *tempNode = NULL;
			char xml_buf[BUFSIZ];
			int count = 0;

			*xml_buf = '\0';
			for (tempNode = node->children; tempNode;
			    tempNode = tempNode->next) {
				if (!tempNode->name ||
				    (strcmp((char *)tempNode->name,
				    "transportAdapter") != 0)) {
					continue;
				}

				// Get this adapter
				tmp_buf = (char *)xmlGetProp(tempNode,
				    (unsigned char *)"name");
				if (!tmp_buf || (*tmp_buf == '\0')) {
					continue;
				}

				if (count != 0) {
					(void) strcat(xml_buf, ",");
				}
				(void) strcat(xml_buf, tmp_buf);
				xmlFree(tmp_buf);
				tmp_buf = NULL;
				count++;
				if (count == 2) {
					break;
				}
			}

			// Adapters are required
			if (*xml_buf == '\0') {
				clerror("No \"%s\" is specified for "
				    "node \"%s\".\n",
				    NODEPROP_ADAPTERS, node_name);
				return (CL_EINVAL);
			} else {
				NVprop->add(NODEPROP_ADAPTERS, xml_buf);
			}
		}

		// Check the monitoring
		found = 0;
		if (NVprop) {
			for (NVprop->start(); !NVprop->end(); NVprop->next()) {
				char *prop_name = NVprop->getName();

				if (prop_name &&
				    (strcmp(prop_name,
				    NODEPROP_MONITOR) == 0)) {
					found++;
					break;
				}
			}
		}
		if (!found) {
			// Get it from the xml node
			xmlNode *tempNode = NULL;
			char *temp;

			for (tempNode = node->children; tempNode;
			    tempNode = tempNode->next) {
				if (!tempNode->name ||
				    (strcmp((char *)tempNode->name,
				    "monitoredState") != 0)) {
					continue;
				}

				// Get the value
				temp = (char *)xmlGetProp(tempNode,
				    (unsigned char *)"value");
				if (temp && (strcasecmp(temp, "true") == 0)) {
					NVprop->add(NODEPROP_MONITOR,
					    MONITOR_ENABLED);
				} else {
					NVprop->add(NODEPROP_MONITOR,
					    MONITOR_DISABLED);
				}
				xmlFree(temp);
			}
		}

		return (clnode_add_farm(cmd, operands, opts->optflgs,
		    *NVprop));
	}

	// Check if this is a cluster node
	if (node && (strcmp((char *)node->name,	"farmNode") == 0)) {
		// This is a farm node. Just ignore it.
		return (CL_NOERR);
	}

	// Get the sponsor node
	char *sponsornode = opts->getOption(CLNODE_SPONSOR);
	if (sponsornode == NULL) {
		clerror("Attempting to add a node without sponsor node.\n");
		xmlFree(node_name);
		delete [] node_name;
		return (CL_EINTERNAL);
	}

	// Get the global devices mount point
	char *gdev = opts->getOption(CLNODE_GDEV);
	if (gdev == NULL) {
		gdev = (char *)xmlGetProp(node, (unsigned char *)"globaldevfs");
	}

	// Get the cluster name
	char *cluster = opts->getOption(CLNODE_CLUSTER);

	// Get the endpoints
	ValueList *endpoints = opts->getOptions(CLNODE_ENDPOINT);
	if (endpoints->getSize() == 0) {
		// Get the cable properties
		if ((getEndpointsProps(node_name, &cable_props) == CL_NOERR) &&
		    cable_props) {

			cltr_cable_t *cable_p;
			for (cable_p = cable_props; cable_p;
			    cable_p = cable_p->cable_next) {
				if (cable_p->epoint1.node &&
				    (strcmp(cable_p->epoint1.node, node_name)
				    != 0)) {
					continue;
				}

				// Must have both endpoints
				if (!cable_p->epoint1.name ||
				    !cable_p->epoint2.name) {
					continue;
				}

				// Add endpoints.
				ep_length = strlen(node_name) +
				    strlen(cable_p->epoint1.name) +
				    strlen(cable_p->epoint2.name) + 3;
				if (cable_p->epoint2.port) {
					ep_length += 1 +
					    strlen(cable_p->epoint2.port);
				}

				tmp_buf = new char[ep_length];
				*tmp_buf = '\0';
				(void) sprintf(tmp_buf, "%s:%s,%s",
				    node_name,
				    cable_p->epoint1.name,
				    cable_p->epoint2.name);
				if (cable_p->epoint2.port) {
					(void) strcat(tmp_buf, "@");
					(void) strcat(tmp_buf,
					    cable_p->epoint2.port);
				}
				endpoints->add(tmp_buf);
				delete [] tmp_buf;
			}
		}

		if (cable_props) {
			delete cable_props;
		}
	}

	// No properties can be added at this time.
	NameValueList empty_list;

	return (clnode_add(cmd, operands, opts->optflgs, empty_list,
	    sponsornode, cluster, gdev, *endpoints));
}
