//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// ident	"@(#)clnas_xml.cc	1.4	08/05/20 SMI"
//

#include "ClXml.h"
#include "clnasdevice.h"
#include "scnas.h"

//
// Returns a ValueList of xpath statements specific to the NAS
// device section, based off of the type filter
//
ValueList *
ClXml::clnasFilter(OptionValues *opts)
{
	ValueList *xpath = new ValueList();
	char buffer[BUFSIZ];

	// Number of xpath statements depends on the number of
	// types specified in the options.
	ValueList *types = opts->getOptions(CLNAS_TYPE);

	// If no type is specifed, then construct default
	// xpath based on name of nas device
	if (types->getSize() == 0) {
		*buffer = '\0';
		(void) sprintf(buffer,
		    "//nasdevice[@name=\"%s\"]",
		    opts->getOperand());

		xpath->add(buffer);
		return (xpath);
	}

	// Otherwise, create an xpath statement for each type listed.
	for (types->start(); !types->end(); types->next()) {
		*buffer = '\0';

		// create default statement with nas device name
		(void) sprintf(buffer,
		    "//nasdevice[@name=\"%s\"]",
		    opts->getOperand());

		// append the device type
		(void) sprintf(buffer, "%s[@type=\"%s\"]",
		    buffer, types->getValue());

		xpath->add(buffer);
	}

	return (xpath);
}

//
// Processes the NAS specific information from the XML file and the command
// line options, and calls the function to create either the NAS device, or
// the NAS dirs, or both
//
int
ClXml::clnasProcessor(xmlNode *node, OptionValues *opts)
{
	NameValueList *NVprop;
	ValueList operands, devicetypes;
	NameValueList *new_properties = (NameValueList *)0;
	int first_err = CL_NOERR;
	int clerrno = CL_NOERR;
	ClCommand cmd;
	int found;
	char *temp, *nas_name;
	char *sub_cmd;
	char *nas_type;
	boolean_t prop_required = B_FALSE;

	// Set the NAS device name to operands
	nas_name = (char *)xmlGetProp(node, (unsigned char *)"name");
	operands.add(0, nas_name);

	// Get the subcommand.
	sub_cmd = opts->getSubcommand();

	if (!sub_cmd) {
		if (first_err == CL_NOERR) {
			first_err = CL_EINTERNAL;
		}
		goto cleanup;
	}

	// Set verbose mode
	if (opts->optflgs & vflg) {
		cmd.isVerbose = 1;
	}

	// Set command arguments
	cmd.argc = opts->argc;
	cmd.argv = opts->argv;

	// Set the type
	nas_type = (char *)xmlGetProp(node, (unsigned char *)"type");
	if (!nas_type) {
		nas_type = (char *)xmlGetProp(node,
		    (unsigned char *)"type");
		if (!nas_type) {
			clerror("Cannot find what type the NAS device "
			    "\"%s\" is.\n", nas_name);
			if (first_err == CL_NOERR) {
				first_err = CL_EINVAL;
			}
			goto cleanup;
		}
	}

	// Check the type and if it require any properties.
	if (!scnas_check_nas_devicetype(nas_type, &prop_required)) {
		clerror("Invalid type \"%s\".\n", nas_type);
		if (first_err == CL_NOERR) {
			first_err = CL_ETYPE;
		}
		goto cleanup;
	}
	devicetypes.add(nas_type);

	new_properties = opts->getNVOptions(CLNAS_PROP);
	if (!new_properties) {
		new_properties = new NameValueList(true);
	}

	// Check if the required property "userid" is specified.
	found = 0;
	if (prop_required && (strcmp(sub_cmd, "add") == 0)) {
		NVprop = opts->getNVOptions(CLNAS_PROP);
		if (NVprop) {
			for (NVprop->start(); !NVprop->end(); NVprop->next()) {
				char *prop_name = NVprop->getName();
				if (!prop_name)
					continue;

				if (strcmp(prop_name, "userid") == 0) {
					found = 1;
					break;
				}
			}
		}
	}

	// Set the username from xml?
	if (prop_required && !found) {
		temp = (char *)xmlGetProp(node,
		    (unsigned char *)"userid");
		// No required property
		if (!temp) {
			clerror("Cannot find the required \"userid\" "
			    "for NAS device \"%s\".\n", nas_name);
			if (first_err == CL_NOERR) {
				first_err = CL_EINVAL;
			}
			goto cleanup;
		}
		(void) new_properties->add(NAS_USERID, temp);
		xmlFree(temp);
	}

	// Check for subcommand "add"
	if (strcmp(sub_cmd, "add") == 0) {
		clerrno = clnasdevice_add(cmd, operands, opts->optflgs,
		    devicetypes, *new_properties);
	} else if (strcmp(sub_cmd, "add-dir") == 0) {
		// Get the user specified directories
		NameValueList *dirs = opts->getNVOptions(CLNAS_DIR);
		char *filer_table_name = (char *)0;

		// Get all directories in xml file
		xmlNode *tempNode = NULL;
		for (tempNode = node->children; tempNode;
		    tempNode = tempNode->next) {
			if (strcmp((char *)tempNode->name, "nasdir") != 0)
				continue;
			// Get the directory
			temp = (char *)xmlGetProp(tempNode,
			    (unsigned char *)"path");
			if (!temp)
				continue;

			// Valid directory?
			if (*temp != '/') {
				clerror("Directory \"%s\" does not "
				    "start with \"/\". Ignored.\n",
				    temp);
				if (first_err == CL_NOERR) {
					first_err = CL_EINVAL;
				}
				continue;
			} else if (strlen(temp) == 1) {
				clerror("Cannot add the root "
				    "directory. Ignored.\n");
				if (first_err == CL_NOERR) {
					first_err = CL_EINVAL;
				}
				continue;
			}

			// Add this directory
			(void) dirs->add(NAS_DIRECTORIES, temp);
			xmlFree(temp);
		}
		xmlFree(tempNode);

		// Nothing to add
		if (dirs->getSize() == 0) {
			clerror("No directories are found for NAS "
			    "device \"%s\".\n", nas_name);
			if (first_err == CL_NOERR) {
				first_err = CL_EINVAL;
			}
			goto cleanup;
		}

		// Check if the filer exist
		filer_table_name = scnas_get_filer_tablename(nas_name);
		if (!filer_table_name) {
			if (prop_required) {
				// Must have specified the password
				found = 0;

				for (new_properties->start();
				    !new_properties->end();
				    new_properties->next()) {
					if (strcasecmp(
					    new_properties->getName(),
					    NAS_PASSWD) == 0) {
						found = 1;
						break;
					}
				}

				if (!found) {
					clerror("NAS device \"%s\" does not "
					    "exist.\n", nas_name);
					clerror("You must specify a file that "
					    "contains the password to access "
					    "NAS device \"%s\".\n", nas_name);
					if (first_err == CL_NOERR) {
						first_err = CL_ENOENT;
					}
					goto cleanup;
				}
			}

			// NAS filer not exist yet. Add it
			clerrno = clnasdevice_add(cmd, operands, opts->optflgs,
			    devicetypes, *new_properties);
			if (clerrno != CL_NOERR) {
				if (first_err == CL_NOERR) {
					first_err = clerrno;
				}
				goto cleanup;
			}
		} else {
			free(filer_table_name);
		}

		// Call the function to do add_dir
		clerrno = clnasdevice_add_dir(cmd, operands, opts->optflgs,
		    *dirs);
	} else {
		if (first_err == CL_NOERR) {
			first_err = CL_EINTERNAL;
		}
	}

	if ((clerrno != CL_NOERR) && (first_err == CL_NOERR)) {
		first_err = clerrno;
	}

cleanup:
	xmlFree(nas_name);
	xmlFree(nas_type);
	if (first_err != CL_NOERR) {
		clerrno = first_err;
	}

	return (clerrno);
}
