//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)OptionValues.cc	1.6	08/05/20 SMI"

#include "OptionValues.h"

//
// OptionValues Class definitions
//

OptionValues::OptionValues(char *subcmd) :
    subcommand(subcmd),
    operand(NULL)
{

}

OptionValues::OptionValues(char *subcmd, int argc_in, char *argv_in[]) :
    subcommand(subcmd),
    operand(NULL)
{
	// Command arguments
	argc = argc_in;
	argv = argv_in;
}

OptionValues::~OptionValues()
{
	subcommand = NULL;
	options.clear();
	nvoptions.clear();
}

//
// Dumps all the data in the object to stdout
//
void
OptionValues::dumpData()
{
	char temp[50];
	char temp2[50];

	printf("---------------------------------\n\n");
	printf("Subcommand:  %s\n", subcommand);
	printf("Operand:    \"%s\"\n\n", operand);
	printf("Regular Options:\n");
	for (optionStart();
	    !optionEnd();
	    optionNext()) {

		sprintf(temp, "OptionID: %s",
		    optionCurrent()->getname());
		printf("%-*s value: \"%s\"\n",
		    30,
		    temp,
		    optionCurrent()->getvalue());
	}
	printf("\nNV-pair Options:\n");
	for (nvoptionStart();
	    !nvoptionEnd();
	    nvoptionNext()) {

		sprintf(temp, "OptionID: %s",
		    getOptionName(nvoptionCurrent().name));
		sprintf(temp2, "name: \"%s\"",
		    nvoptionCurrent().nv->getname());
		printf("%-*s %-*s value: \"%s\"\n",
		    30,
		    temp,
		    30,
		    temp2,
		    nvoptionCurrent().nv->getvalue());
	}
}

//
// Returns the option name for a specific ID
char *
OptionValues::getOptionName(int id)
{
	switch (id) {

	case CLUSTER_AUTOQUORUM:
		return ("CLUSTER_AUTOQUORUM");

	case CLUSTER_PROP:
		return ("CLUSTER_PROP");

	case CLDEVICE_NODE:
		return ("CLDEVICE_NODE");

	case CLDG_PROP:
		return ("CLDG_PROP");

	case CLDG_NODE:
		return ("CLDG_NODE");

	case CLDG_MEMBER:
		return ("CLDG_MEMBER");

	case CLDG_TYPE:
		return ("CLDG_TYPE");

	case CLINTER_NODE:
		return ("CLINTER_NODE");

	case CLINTER_ADAP_PROP:
		return ("CLINTER_ADAP_PROP");

	case CLINTER_ADAP_TT_PROP:
		return ("CLINTER_ADAP_TT_PROP");

	case CLINTER_DISABLE:
		return ("CLINTER_DISABLE");

	case CLNAS_TYPE:
		return ("CLNAS_TYPE");

	case CLNAS_PROP:
		return ("CLNAS_PROP");

	case CLNAS_USER:
		return ("CLNAS_USER");

	case CLNAS_HOSTNAME:
		return ("CLNAS_HOSTNAME");

	case CLNAS_PASSFILE:
		return ("CLNAS_PASSFILE");

	case CLNAS_DIR:
		return ("CLNAS_DIR");

	case CLNODE_CLUSTER:
		return ("CLNODE_CLUSTER");

	case CLNODE_SPONSOR:
		return ("CLNODE_SPONSOR");

	case CLNODE_ENDPOINT:
		return ("CLNODE_ENDPOINT");

	case CLNODE_GDEV:
		return ("CLNODE_GDEV");

	case CLNODE_PROP:
		return ("CLNODE_PROP");

	case CLNODE_TYPE:
		return ("CLNODE_TYPE");

	case CLQUORUM_TYPE:
		return ("CLQUORUM_TYPE");

	case CLQUORUM_PROP:
		return ("CLQUORUM_PROP");

	case CLRLH_GROUP:
		return ("CLRLH_GROUP");

	case CLRLH_PROP:
		return ("CLRLH_PROP");

	case CLRLH_AUTO:
		return ("CLRLH_AUTO");

	case CLRLH_DISABLE:
		return ("CLRLH_DISABLE");

	case CLRES_PROP:
		return ("CLRES_PROP");

	case CLRES_EXT_PROP:
		return ("CLRES_EXT_PROP");

	case CLRES_GROUP:
		return ("CLRES_GROUP");

	case CLRES_AUTO:
		return ("CLRES_AUTO");

	case CLRES_XPROP:
		return ("CLRES_XPROP");

	case CLRES_YPROP:
		return ("CLRES_YPROP");

	case CLRES_DISABLE:
		return ("CLRES_DISABLE");

	case CLRES_TYPE:
		return ("CLRES_TYPE");

	case CLRG_NODE:
		return ("CLRG_NODE");

	case CLRG_PROP:
		return ("CLRG_PROP");

	case CLRG_SCALABLE:
		return ("CLRG_SCALABLE");

	case CLRG_ZONE:
		return ("CLRG_ZONE");

	case CLRT_ZONE:
		return ("CLRT_ZONE");

	case CLRT_NODE:
		return ("CLRT_NODE");

	case CLRT_RTRFILE:
		return ("CLRT_RTRFILE");

	case CLRT_ALLNODES:
		return ("CLRT_ALLNODES");

	case CLRT_PROP:
		return ("CLRT_PROP");

	case CLRSA_GROUP:
		return ("CLRSA_GROUP");

	case CLRSA_PROP:
		return ("CLRSA_PROP");

	case CLRSA_AUXNODE:
		return ("CLRSA_AUXNODE");

	case CLRSA_AUTO:
		return ("CLRSA_AUTO");

	case CLRSA_DISABLE:
		return ("CLRSA_DISABLE");

	case CLSNMP_DEFAULT:
		return ("CLSNMP_DEFAULT");

	case CLSNMP_PASSFILE:
		return ("CLSNMP_PASSFILE");

	case CLSNMP_SECLEVEL:
		return ("CLSNMP_SECLEVEL");

	case CLSNMP_AUTH:
		return ("CLSNMP_AUTH");

	case CLSNMP_COMMUNITY:
		return ("CLSNMP_COMMUNITY");

	case CLSNMP_NODE:
		return ("CLSNMP_NODE");

	case CLTA_TYPE:
		return ("CLTA_TYPE");

	case VERBOSE:
		return ("VERBOSE");

	default:
		return (NULL);

	}
}

//
// Returns a ValueList of all the options for a particular ID
//
ValueList *
OptionValues::getOptions(int id)
{
	ValueList *v = new ValueList();

	char *name = getOptionName(id);
	for (optionStart();
	    !optionEnd();
	    optionNext()) {

		if (strcmp(optionCurrent()->getname(), name) == 0)
			v->add(optionCurrent()->getvalue());
	}

	return (v);
}

//
// Returns a NameValueList of all the options for a particular ID
//
NameValueList *
OptionValues::getNVOptions(int id)
{
	NameValueList *nvl = new NameValueList();

	for (nvoptionStart();
	    !nvoptionEnd();
	    nvoptionNext()) {

		if (nvoptionCurrent().name == id)
			nvl->add(nvoptionCurrent().nv->getname(),
			    nvoptionCurrent().nv->getvalue());
	}

	return (nvl);
}

//
// Returns the first value for an option of specific ID, or NULL.
//
char *
OptionValues::getOption(int id)
{
	char *name = getOptionName(id);
	for (optionStart();
	    !optionEnd();
	    optionNext()) {

		if (strcmp(optionCurrent()->getname(), name) == 0)
			return (optionCurrent()->getvalue());
	}

	return (NULL);
}

//
// Adds the value to the list of values for option id.
//
void
OptionValues::addOption(int id, char *value)
{
	if (value == NULL)
		value = ".";

	NameValue *nv = new NameValue(getOptionName(id), value);

	options.push_back(nv);
}

//
// Adds the list of values in list to the option id.
//
void
OptionValues::addOptions(int id, ValueList *list)
{
	for (list->start();
	    !list->end();
	    list->next()) {

		addOption(id, list->getValue());
	}
}

//
// Adds a NV-pair option to the list of option id
//
void
OptionValues::addNVOption(int id, char *name, char *value)
{
	NameValue *nv = new NameValue(name, value);
	nv_opt_t nvot;
	nvot.name = id;
	nvot.nv = nv;

	nvoptions.push_back(nvot);
}

//
// Adds a NV-pair option to the list of option id
//
void
OptionValues::addNVOption(int id, NameValue *nv)
{
	NameValue *new_nv = new NameValue(
	    nv->getname(),
	    nv->getvalue());

	nv_opt_t nvot;
	nvot.name = id;
	nvot.nv = new_nv;

	nvoptions.push_back(nvot);
}

//
// Adds all of the NV-pair options in list to the list of option id
//
void
OptionValues::addNVOptions(int id, NameValueList *list)
{
	for (list->start();
	    !list->end();
	    list->next()) {

		addNVOption(id,
		    new NameValue(list->getName(), list->getValue()));
	}
}

//
// Sets the operand -- there may be only one
//
void
OptionValues::setOperand(char *value)
{
	operand = strdup(value);
}

//
// Returns the operand name
//
char *
OptionValues::getOperand()
{
	return (operand);
}

//
// Returns the subcommand name
//
char *
OptionValues::getSubcommand()
{
	return (subcommand);
}

//
// Sets the subcommand name
//
void
OptionValues::setSubcommand(char *subcmd)
{
	subcommand = strdup(subcmd);
}

//
// Start the option iterator
//
void
OptionValues::optionStart()
{
	opt_iter = options.begin();
}

//
// Advance the option iterator
//
void
OptionValues::optionNext()
{
	if (opt_iter != options.end())
		++opt_iter;
}

//
// Return 1 if the option list is at the end
//
int
OptionValues::optionEnd()
{
	return (opt_iter == options.end() ? 1 : 0);
}

//
// Return the value of the option iterator
//
NameValue *
OptionValues::optionCurrent()
{
	return ((*opt_iter));
}

//
// Start the NV-paiar option iterator
//
void
OptionValues::nvoptionStart()
{
	nvopt_iter = nvoptions.begin();
}

//
// Advance the NV-paiar option iterator
//
void
OptionValues::nvoptionNext()
{
	if (nvopt_iter != nvoptions.end())
		++nvopt_iter;
}

//
// Return 1 if the NV-pair option list is at the end
//
int
OptionValues::nvoptionEnd()
{
	return (nvopt_iter == nvoptions.end() ? 1 : 0);
}

//
// Return the value of the NV-pair option iterator
//
OptionValues::nv_opt_t
OptionValues::nvoptionCurrent()
{
	return ((*nvopt_iter));
}
