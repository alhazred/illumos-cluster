//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// ident	"@(#)CltelemetryattributeExport.cc 1.2	08/05/20 SMI"
//

#include "CltelemetryattributeExport.h"

#define	ENABLED_STATE	(char *)"enabled"
#define	DISABLED_STATE	(char *)"disabled"

//
// Create the root element for the telemetry attributes
//
CltelemetryattributeExport::CltelemetryattributeExport()
{
	root = newXmlNode("telemetrics");
}

//
// Create a "telemetryObjectType" xmlNode with the given name.
//
int
CltelemetryattributeExport::createTelemetryObjectType(char *name)
{
	CHECKVALUE(name);

	xmlNode *tot = newXmlNode("telemetryObjectType");

	// add the attribute
	newXmlAttr(tot, "name", name);

	if (!xmlAddChild(root, tot)) {
		errorAddingElement("telemetryObjectType", "telemetrics");
		return (CL_EINTERNAL);
	} else
		return (CL_NOERR);
}

//
// Add a "telemetryAttribute" to the "telemetryObjectType" designated.
// The object type must have already been created.
//
int
CltelemetryattributeExport::addTelemetryAttribute(
    char *telemetryObjectTypeName,
    char *name,
    bool enabled)
{
	CHECKVALUE(telemetryObjectTypeName);
	CHECKVALUE(name);

	// Get the telemetryObjectType element
	xmlNode *tot = NULL;
	for (xmlNode *temp = root->children; temp; temp = temp->next) {
		if (strcmp((char *)temp->name,
		    (char *)"telemetryObjectType") == 0) {
			char *name = (char *)xmlGetProp(temp,
			    (unsigned char *)"name");
			if (strcmp(name, telemetryObjectTypeName) == 0) {
				xmlFree(name);
				tot = temp;
				break;
			}
			xmlFree(name);
		}
	}

	if (tot == NULL) {
		clerror("No such telemetry object type exists in the "
		    "configuration - %s.\n", tot);
		return (CL_EINTERNAL);
	}

	// Create the telemetryAttribute element
	xmlNode *telemetryAttribute = newXmlNode("telemetryAttribute");
	newXmlAttr(telemetryAttribute, "name", name);

	// Create the state element
	xmlNode *state = newXmlNode("state");
	if (enabled) {
		newXmlAttr(state, "value", ENABLED_STATE);
	} else {
		newXmlAttr(state, "value", DISABLED_STATE);
	}

	if (!xmlAddChild(telemetryAttribute, state)) {
		errorAddingElement("state", "telemetryAttribute");
		return (CL_EINTERNAL);
	}
	if (!xmlAddChild(tot, telemetryAttribute)) {
		errorAddingElement("telemetryAttribute", "telemetryObjectType");
		return (CL_EINTERNAL);
	} else
		return (CL_NOERR);
}
