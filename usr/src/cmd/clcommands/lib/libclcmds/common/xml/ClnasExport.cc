//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// ident	"@(#)ClnasExport.cc 1.3	08/05/20 SMI"
//

#include "ClnasExport.h"

//
// Creates the nasdeviceList element and sets it to root
//
ClnasExport::ClnasExport()
{
	root = newXmlNode("nasdeviceList");
}

//
// Creates a nasdevice element and adds it to the nasdeviceList element.
//
//	name:	The name of the nas device
//	type:	The type of the nas device (optional)
//	userid:	The userid used to access the nas device (optional)
//
// Returns CL_EINTERNAL on any error, CL_NOERR otherwise.
//
int
ClnasExport::createNasDevice(char *name, char *type, char *userid)
{
	CHECKVALUE(name);

	xmlNode *nas = newXmlNode("nasdevice");

	// add the attributes
	newXmlAttr(nas, "name", name);
	if (type)
		newXmlAttr(nas, "type", type);
	if (userid)
		newXmlAttr(nas, "userid", userid);

	if (!xmlAddChild(root, nas)) {
		errorAddingElement("nasdevice", "nasdeviceList");
		return (CL_EINTERNAL);
	} else
		return (CL_NOERR);
}

//
// Creates a nasdir element and adds it to the nasdevice element
// attributed by nasDeviceName
//
//	nasDeviceName:	the name of the nas device
//	path:		the nas dir path
//
// Returns CL_EINTERNAL on any error, CL_NOERR otherwise.
//
int
ClnasExport::addNasDir(char *nasDeviceName, char *path)
{
	CHECKVALUE(nasDeviceName);
	CHECKVALUE(path);

	// Get the nas element
	xmlNode *nasDevice = NULL;
	for (xmlNode *temp = root->children; temp; temp = temp->next) {
		if (strcmp((char *)temp->name, (char *)"nasdevice") == 0) {
			char *name = (char *)xmlGetProp(temp,
			    (unsigned char *)"name");
			if (strcmp(name, nasDeviceName) == 0) {
				xmlFree(name);
				nasDevice = temp;
				break;
			}
			xmlFree(name);
		}
	}

	if (nasDevice == NULL) {
		clerror("No such NAS device exists in the configuration - %s\n",
		    nasDeviceName);
		return (CL_EINTERNAL);
	}

	// Create the NAS dir element
	xmlNode *nasdir = newXmlNode("nasdir");
	newXmlAttr(nasdir, "path", path);

	if (!xmlAddChild(nasDevice, nasdir)) {
		errorAddingElement("nasdir", "nasdevice");
		return (CL_EINTERNAL);
	} else
		return (CL_NOERR);
}
