//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// ident	"@(#)cldevice_xml.cc	1.2	08/05/20 SMI"
//

#include "ClXml.h"
#include "cldevice.h"

//
// Returns a ValueList of xpath statements specific to the cluster
// device name
//
ValueList *
ClXml::cldeviceFilter(OptionValues *opts)
{
	ValueList *xpath = new ValueList();

	// If the subcommand is monitored, then get only those set
	// to be monitored, otherwise get only those set to be
	// unmonitored.
	char monitored[6];
	if (strcmp(opts->getSubcommand(), "monitor") == 0)
		sprintf(monitored, "true");
	else if (strcmp(opts->getSubcommand(), "unmonitor") == 0)
		sprintf(monitored, "false");
	else {
		set_error(CL_EINTERNAL);
		return (xpath);
	}

	// If no nodes are specified, add all of them
	ValueList *nodes = opts->getOptions(CLDEVICE_NODE);
	if (nodes->getSize() == 0) {

		// We need to get all the node names specified in the xml
		// and create an xpath statement for each one.  We need to
		// do this because a check later will fail if the xpath
		// generates a hit for more than xmlNode per statement.

		(void) getAllObjectNames(CLNODE_CMD, NULL, nodes);
	}

	// Add each node to the xpath statement
	for (nodes->start(); !nodes->end(); nodes->next()) {
		// Since the device can be either the ctd or the name
		// construct one for each.
		char *buffer = new char [BUFSIZ];
		sprintf(buffer, "//device[@name=\"%s\"]/"
		    "devicePath[@nodeRef=\"%s\"][@monitored=\"%s\"]",
		    opts->getOperand(), nodes->getValue(), monitored);
		xpath->add(buffer);

		sprintf(buffer, "//device[@ctd=\"%s\"]/"
		    "devicePath[@nodeRef=\"%s\"][@monitored=\"%s\"]",
		    opts->getOperand(), nodes->getValue(), monitored);
		xpath->add(buffer);
	}

	return (xpath);
}

//
// Processes the device information from the XML file and the command line
// options, and calls the function to (un)monitor the device.
//
int
ClXml::cldeviceProcessor(xmlNode *node, OptionValues *opts)
{
	ClCommand cmd;
	ValueList dev;
	ValueList nodelist;
	int err = CL_NOERR;

	// In order to get the device name, we must get the parent xmlNode of
	// this one, and get that node's "name" prop.
	char *temp = (char *)xmlGetProp(node->parent, (unsigned char *)"name");
	dev.add(temp);
	xmlFree(temp);

	// Set verbose
	if (opts->getOption(VERBOSE))
		cmd.isVerbose = 1;

	// There will only be one node, which can be obtained by the getting the
	// nodeRef property.
	temp = (char *)xmlGetProp(node, (unsigned char *)"nodeRef");
	nodelist.add(temp);
	xmlFree(temp);

	if (strcmp(opts->getSubcommand(), "monitor") == 0)
		err = cldevice_monitor(cmd, dev, nodelist);
	else
		err = cldevice_unmonitor(cmd, dev, nodelist);

	if (err && !error)
		error = err;

	return (error);
}
