//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// ident	"@(#)ClnodeExport.cc 1.3	08/05/20 SMI"
//

#include "ClnodeExport.h"

//
// Creates the nodeList elements and sets it as the root
//
ClnodeExport::ClnodeExport()
{
	root = xmlNewNode(NULL, (xmlChar *)"nodeList");
}

//
// Creates a node element and adds it to the nodeList root
// element.
//
//	name:		The name of the node
//	id:		The node id
//
// Returns CL_EINTERNAL on any error, CL_NOERR otherwise
//
int
ClnodeExport::createNode(char *name, char *id)
{
	xmlNode *node = newXmlNode("node");

	// add the attributes
	newXmlAttr(node, "name", name);
	if (id)
		newXmlAttr(node, "id", id);

	if (!xmlAddChild(root, node)) {
		errorAddingElement("node", "nodeList");
		return (CL_EINTERNAL);
	} else
		return (CL_NOERR);
}

//
// Creates a farmNode element and adds it to the nodeList root
// element.
//
//	name:		The name of the farmNode
//	id:		The node id
//	monitored:	Is this farm node monitored?
//	adap1:		The name of the 1st transport adapter
//	adap2:		The name of the 2nd transport adapter
//
// Returns CL_EINTERNAL on any error, CL_NOERR otherwise
//
int
ClnodeExport::createFarmNode(char *name, char *id, bool monitored,
    char *adap1, char *adap2)
{
	xmlNode *node = newXmlNode("farmNode");

	// add the attributes
	newXmlAttr(node, "name", name);
	if (id)
		newXmlAttr(node, "id", id);

	// monitored state
	xmlNode *mon = newXmlNode("monitoredState");
	newXmlAttr(mon, "value", (monitored) ? "true" : "false");
	if (!xmlAddChild(node, mon)) {
		errorAddingElement("monitoredState", "node");
		return (CL_EINTERNAL);
	}

	// adapter 1
	if (adap1) {
		xmlNode *adapter = newXmlNode("transportAdapter");
		newXmlAttr(adapter, "name", adap1);
		if (!xmlAddChild(node, adapter)) {
			errorAddingElement("transportAdapter", "node");
			return (CL_EINTERNAL);
		}
	}

	// adapter 2
	if (adap2) {
		xmlNode *adapter = newXmlNode("transportAdapter");
		newXmlAttr(adapter, "name", adap2);
		if (!xmlAddChild(node, adapter)) {
			errorAddingElement("transportAdapter", "node");
			return (CL_EINTERNAL);
		}
	}

	if (!xmlAddChild(root, node)) {
		errorAddingElement("node", "nodeList");
		return (CL_EINTERNAL);
	} else
		return (CL_NOERR);
}

//
// Adds a property element as a child of the node element referenced
// by nodeName
//
//	nodeName:	The name of the node
//	name:		The property name
//	value:		The property value
//	readonly:	Is this property readonly?
//
// Returns CL_EINTERNAL on any error, CL_NOERR otherwise.
//
int
ClnodeExport::addProperty(
    char *nodeName,
    char *name,
    char *value,
    bool readonly)
{
	return (ClcmdExport::addProperty(
	    getNode(nodeName),
	    name,
	    value,
	    NULL, // type
	    readonly));
}

//
// Adds a property element as a child of the node element referenced
// by nodeName
//
//	nodeName:	The name of the node
//	nv:		The property name-value
//	readonly:	Is this property readonly?
//
// Returns CL_EINTERNAL on any error, CL_NOERR otherwise.
//
int
ClnodeExport::addProperty(
    char *nodeName,
    NameValue *nv,
    bool readonly)
{
	return (ClcmdExport::addProperty(
	    getNode(nodeName),
	    nv,
	    NULL, // type
	    readonly));
}

//
// Add property elements as children of the node element referenced
// by nodeName
//
//	nodeName:	The name of the node
//	nvl:		A list of property name-values
//	readonly:	Are these properties readonly?
//
// Returns CL_EINTERNAL on any error, CL_NOERR otherwise.
//
int
ClnodeExport::addProperties(
    char *nodeName,
    NameValueList *nvl,
    bool readonly)
{
	return (ClcmdExport::addProperties(
	    getNode(nodeName),
	    nvl,
	    NULL, // type
	    readonly));
}

//
// Returns the node element referenced by nodeName.  If not found,
// then NULL is returned.
//
xmlNode *
ClnodeExport::getNode(char *nodeName)
{
	if (!nodeName)
		return (NULL);

	xmlNode *node = NULL;
	for (xmlNode *temp = root->children; temp; temp = temp->next) {
		if (strcmp((char *)temp->name, (char *)"node") == 0 ||
		    strcmp((char *)temp->name, (char *)"farmNode") == 0) {
			char *name = (char *)xmlGetProp(temp,
			    (unsigned char *)"name");
			if (strcmp(name, nodeName) == 0) {
				xmlFree(name);
				node = temp;
				break;
			}
			xmlFree(name);
		}
	}
	return (node);
}
