//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// ident	"@(#)ClusterExport.cc 1.2	08/05/20 SMI"
//

#include "ClusterExport.h"

//
// Creates the cluster element and sets it as the root element.
// If name is NULL, then an empty string is used instead.
//
//	name:	The name of the cluster
//
ClusterExport::ClusterExport(char *name)
{
	root = xmlNewNode(NULL, (xmlChar *)"cluster");

	if (!name)
		name = (char *)"";

	// Set the cluster name
	xmlNewProp(root, (xmlChar *)"name", (xmlChar *)strdup(name));
}

//
// Creates a property element as a child of the cluster element.
//
//	name:		The property name
//	value:		The property value
//	readonly:	Is this property readonly?
//
// Returns CL_EINTERNAL on any error, CL_NOERR otherwise.
//
int
ClusterExport::addProperty(
    char *name,
    char *value,
    bool readonly)
{
	return (ClcmdExport::addProperty(
	    root,
	    name,
	    value,
	    NULL, // type
	    readonly));
}

//
// Creates a property element as a child of the cluster element.
//
//	nv:		The property name-value
//	readonly:	Is this property readonly?
//
// Returns CL_EINTERNAL on any error, CL_NOERR otherwise.
//
int
ClusterExport::addProperty(
    NameValue *nv,
    bool readonly)
{
	return (ClcmdExport::addProperty(
	    root,
	    nv,
	    NULL, // type
	    readonly));
}

//
// Creates property elements as children of the cluster element.
//
//	nvl:		A list of property name-values
//	readonly:	Are these properties readonly?
//
// Returns CL_EINTERNAL on any error, CL_NOERR otherwise.
//
int
ClusterExport::addProperties(
    NameValueList *nvl,
    bool readonly)
{
	return (ClcmdExport::addProperties(
	    root,
	    nvl,
	    NULL, // type
	    readonly));
}
