//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// ident	"@(#)ClresourcegroupExport.cc 1.3	08/05/20 SMI"
//

#include "ClresourcegroupExport.h"

#define	RG_FAILOVER	(char *)"failover"
#define	RG_SCALABLE	(char *)"scalable"

#define	TRUE_STRING	(char *)"true"
#define	FALSE_STRING	(char *)"false"

//
// Creates the resourcegroupList element and sets it as the root
//
ClresourcegroupExport::ClresourcegroupExport()
{
	root = xmlNewNode(NULL, (xmlChar *)"resourcegroupList");
}

//
// Creates the resourcegroup element and adds it to the
// resourcegroupList element.
//
//	rgName:		The name of the resource group
//	failover:	The failover type (FAILVOER or
//			SCALABLE)
//	managed:	Is the rg managed?
//
// Returns CL_EINTERNAL on any error, CL_NOERR otherwise.
//
int
ClresourcegroupExport::addResourcegroup(
    char *rgName,
    failover_t failover,
    bool managed)
{
	CHECKVALUE(rgName);

	xmlNode *rg = newXmlNode("resourcegroup");

	// add the attribute
	newXmlAttr(rg, "name", rgName);

	// Create the failoverMode element
	xmlNode *failoverElement = newXmlNode("failoverMode");
	if (failover == FAILOVER)
		newXmlAttr(failoverElement, "value", RG_FAILOVER);
	else
		newXmlAttr(failoverElement, "value", RG_SCALABLE);

	// Create the managedState element
	xmlNode *managedElement = newXmlNode("managedState");
	if (managed)
		newXmlAttr(managedElement, "value", TRUE_STRING);
	else
		newXmlAttr(managedElement, "value", FALSE_STRING);

	if (!xmlAddChild(rg, failoverElement)) {
		errorAddingElement("failoverMode", "resourcegroup");
		return (CL_EINTERNAL);
	}
	if (!xmlAddChild(rg, managedElement)) {
		errorAddingElement("managedState", "resourcegroup");
		return (CL_EINTERNAL);
	}

	// Add the rg to the list
	if (!xmlAddChild(root, rg)) {
		errorAddingElement("resourcegroup", "resourcegroupList");
		return (CL_EINTERNAL);
	} else
		return (CL_NOERR);
}

//
// Creates property element as a child of the resourcegroup element
// referenced by rgName.  The resourcegroup element must already exist.
//
//	rgName:		The name of the resourcegroup
//	name:		The property name
//	value:		The property value
//	readonly:	Is the property readonly?
//
// Returns CL_EINTERNAL on any error, CL_NOERR otherwise.
//
int
ClresourcegroupExport::addResourcegroupProperty(
    char *rgName,
    char *name,
    char *value,
    bool readonly)
{
	return (ClcmdExport::addProperty(
	    getResourcegroup(rgName),
	    name,
	    value,
	    NULL, // type
	    readonly));
}

//
// Creates property element as a child of the resourcegroup element
// referenced by rgName.  The resourcegroup element must already exist.
//
//	rgName:		The name of the resourcegroup
//	nv:		The property name-value
//	readonly:	Is the property readonly?
//
// Returns CL_EINTERNAL on any error, CL_NOERR otherwise.
//
int
ClresourcegroupExport::addResourcegroupProperty(
    char *rgName,
    NameValue *nv,
    bool readonly)
{
	return (ClcmdExport::addProperty(
	    getResourcegroup(rgName),
	    nv,
	    NULL, // type
	    readonly));
}

//
// Creates property element as a child of the resourcegroup element
// referenced by rgName.  The resourcegroup element must already exist.
//
//	rgName:		The name of the resourcegroup
//	nv:		The property name-value
//	readonly:	Is the property readonly?
//
// Returns CL_EINTERNAL on any error, CL_NOERR otherwise.
//
int
ClresourcegroupExport::addResourcegroupProperties(
    char *rgName,
    NameValueList *nvl,
    bool readonly)
{
	return (ClcmdExport::addProperties(
	    getResourcegroup(rgName),
	    nvl,
	    NULL, // type
	    readonly));
}

//
// Creates a resourcegroupNode element as a child of the
// resourcegroup element referenced by rgName. The resource
// group must already exist.
//
//	rgName:	The name of the resource group
//	node:	The node name
//
// Returns CL_EINTERNAL on any error, CL_NOERR otherwise.
//
int
ClresourcegroupExport::addResourcegroupNode(char *rgName,
    char *node, char *zone)
{
	CHECKVALUE(rgName);
	CHECKVALUE(node);

	xmlNode *rg = getResourcegroup(rgName);
	if (rg == NULL) {
		clerror("Cannot find resourcegroup element - %s\n", rgName);
		return (CL_EINTERNAL);
	}

	// Get the resourcegroupNodeList
	xmlNode *rgnList = NULL;
	for (xmlNode *temp = rg->children; temp; temp = temp->next) {
		if (strcmp((char *)temp->name,
		    (char *)"resourcegroupNodeList") == 0) {
			rgnList = temp;
			break;
		}
	}
	if (rgnList == NULL) {
		// Node list doesn't exist.  We can't just add it to the rg,
		// beacuse the resourcegroupResourceList element might exist
		// already and that would create a non-valid document.  We
		// must add it as the next sibling of the managedState element
		// instead.
		xmlNode *ms = NULL;
		for (xmlNode *temp = rg->children; temp; temp = temp->next) {
			if (strcmp((char *)temp->name,
			    (char *)"managedState") == 0) {
				ms = temp;
				break;
			}
		}
		if (ms == NULL) {
			clerror("Cannot find managedState element\n");
			return (CL_EINTERNAL);
		}
		rgnList = newXmlNode("resourcegroupNodeList");
		if (!xmlAddNextSibling(ms, rgnList)) {
			errorAddingElement("resourcegroupNodeList",
			    "resourcegroup");
			return (CL_EINTERNAL);
		}
	}

	// Create the resourcegroupNode element
	xmlNode *rgn = newXmlNode("resourcegroupNode");
	newXmlAttr(rgn, "nodeRef", node);
	if (zone) {
		newXmlAttr(rgn, "zone", zone);
	}

	if (!xmlAddChild(rgnList, rgn)) {
		errorAddingElement("resourcegroupNode",
		    "resourcegroupNodeList");
		return (CL_EINTERNAL);
	} else
		return (CL_NOERR);
}

//
// Creates a resourcegroupResource element as the child of the
// resourcegroup element referenced by rgName.  The resource
// group must already exist.
//
//	rgName:		The name of the resource group
//	resName:	The name of the resource
//
// Returns CL_EINTERNAL on any error, CL_NOERR otherwise.
//
int
ClresourcegroupExport::addResourcegroupResource(char *rgName, char *resName)
{
	CHECKVALUE(rgName);
	CHECKVALUE(resName);

	// Get the resourcegroup element
	xmlNode *rg = getResourcegroup(rgName);
	if (rg == NULL) {
		clerror("Cannot find resourcegroup - %s\n", rgName);
		return (CL_EINTERNAL);
	}

	// Make sure the resourcegroupResourceList exists
	xmlNode *rgrList = NULL;
	for (xmlNode *temp = rg->children; temp; temp = temp->next) {
		if (strcmp((char *)temp->name,
		    (char *)"resourcegroupResourceList") == 0) {
			rgrList = temp;
			break;
		}
	}
	if (rgrList == NULL) {
		rgrList = newXmlNode("resourcegroupResourceList");
		if (!xmlAddChild(rg, rgrList)) {
			errorAddingElement("resourcegroupResourceList",
			    "resourcegroup");
			return (CL_EINTERNAL);
		}
	}

	// Create the resourcegroupResource element
	xmlNode *rgr = newXmlNode("resourcegroupResource");
	newXmlAttr(rgr, "resourceRef", resName);

	if (!xmlAddChild(rgrList, rgr)) {
		errorAddingElement("resourcegroupResource",
		    "resourcegroupResourceList");
		return (CL_EINTERNAL);
	} else
		return (CL_NOERR);
}

//
// Returns a pointer to the resourcegroup xmlNode referenced by rgName.
// If it is not found, then it returns NULL.
//
xmlNode *
ClresourcegroupExport::getResourcegroup(char *rgName)
{
	if (!rgName)
		return (NULL);

	xmlNode *rg = NULL;
	for (xmlNode *temp = root->children; temp; temp = temp->next) {
		if (strcmp((char *)temp->name, (char *)"resourcegroup") == 0) {
			char *name = (char *)xmlGetProp(temp,
			    (unsigned char *)"name");
			if (strcmp(name, rgName) == 0) {
				xmlFree(name);
				rg = temp;
				break;
			}
			xmlFree(name);
		}
	}

	if (rg == NULL) {
		clerror("Cannot find resourcegroup - %s\n", rgName);
		return (NULL);
	} else
		return (rg);
}
