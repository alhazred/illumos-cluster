//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// ident	"@(#)clresource_xml.cc	1.6	08/05/20 SMI"
//

#include <sys/os.h>
#include <scadmin/scswitch.h>
#include <scadmin/scrgadm.h>
#include <scadmin/scconf.h>
#include <scadmin/scstat.h>
#include "clcommands.h"
#include "ClCommand.h"

#include "ClXml.h"
#include "clresource.h"
#include "rgm_support.h"

//
// Returns a ValueList of xpath statements specific to the cluster
// resource information
//
ValueList *
ClXml::clresourceFilter(OptionValues *opts)
{
	ValueList *xpath = new ValueList();

	// Number of xpath statements depends on the number of
	// resourcegroups specified and the number of resource
	// types specified, whichever is more.
	ValueList *rgs = opts->getOptions(CLRES_GROUP);
	ValueList *types = opts->getOptions(CLRES_TYPE);

	// If no resource groups or types were specifed, then
	// construct default xpath based on the name
	if (rgs->getSize() == 0 && types->getSize() == 0) {
		char *buffer = new char [BUFSIZ];
		sprintf(buffer,
		    "//resource[@name=\"%s\"]",
		    opts->getOperand());

		xpath->add(buffer);
		return (xpath);
	}

	// Otherwise, create an XPath statement for each resource group
	// and type listed and the operand supplied.  In practice, this should
	// only result in one match.  If the result of this XPath statement
	// is greater than one xmlNode, then it is an error and will be caught.
	int max = max(rgs->getSize(), types->getSize());
	rgs->start();
	types->start();
	for (int i = 0; i < max; i++) {

		char *buffer = new char [BUFSIZ];

		// Initial xpath statement
		sprintf(buffer, "//resource");

		// Add resource group
		if (!rgs->end()) {
			sprintf(buffer, "%s[@resourcegroupRef=\"%s\"]",
			    buffer, rgs->getValue());
			rgs->next();
		}

		// Add resource type
		if (!types->end()) {
			sprintf(buffer, "%s[@resourcetypeRef=\"%s\"]",
			    buffer, types->getValue());
			types->next();
		}

		// resource name
		sprintf(buffer, "%s[@name=\"%s\"]", buffer, opts->getOperand());

		xpath->add(buffer);
	}

	return (xpath);
}

//
// Processes the resource information from the XML file and the command line
// options, and calls the function to create the resource
//
clerrno_t
ClXml::clresourceProcessor(xmlNode *node, OptionValues *opts)
{
	NameValueList new_properties;
	NameValueList new_x_properties;
	NameValueList *new_NVprop;
	NameValueList *new_x_NVprop;
	NameValueList *old_x_NVprop;
	OptionValues *new_opts = new OptionValues(opts->getSubcommand());
	ValueList rtlist;
	ValueList rglist;
	ValueList empty_list;
	NameValueList empty_nv_list;
	ValueList operand;
	char *tmp_str = (char *)NULL;
	xmlNode *tempNode = NULL;
	xmlNode *proplist = NULL;
	char *rgname = (char *)NULL;
	char *rstype = (char *)NULL;
	char *dup_prop = (char *)NULL;
	char *prop_part = (char *)NULL;
	boolean_t prop_not_needed;
	int errflg = 0;

	// Set the resource name
	tmp_str = (char *)xmlGetProp(node, (unsigned char *)"name");
	new_opts->setOperand(tmp_str);
	xmlFree(tmp_str);

	// Set the resource type
	rstype = (char *)xmlGetProp(node, (unsigned char *)"resourcetypeRef");
	new_opts->addOption(CLRES_TYPE, rstype);

	// Set the resource group
	rgname = (char *)xmlGetProp(node, (unsigned char *)"resourcegroupRef");
	new_opts->addOption(CLRES_GROUP, rgname);

	// Now get all the properties
	for (proplist = node->children; proplist; proplist = proplist->next) {
		if (strcmp((char *)proplist->name, "propertyList") == 0)
			break;
	}

	// Call generic property processing routine
	genericPropertyOverrides(
	    proplist,
	    opts->getNVOptions(CLRES_PROP),
	    CLRES_PROP,
	    new_opts);

	// Do the same for extension properties
	for (proplist = node->children; proplist; proplist = proplist->next) {
		if (strcmp((char *)proplist->name, "propertyList") == 0) {
			tmp_str = (char *)xmlGetProp(proplist,
			    (unsigned char *)"extension");
			if (strcasecmp(tmp_str, "true") == 0) {
				xmlFree(tmp_str);
				break;
			} else {
				xmlFree(tmp_str);
			}
		}
	}

	// Call generic property processing routine
	genericPropertyOverrides(
	    proplist,
	    opts->getNVOptions(CLRES_EXT_PROP),
	    CLRES_EXT_PROP,
	    new_opts);

	ClCommand cmd;
	// verbose?
	if (opts->getOption(VERBOSE) != NULL) {
		opts->optflgs |= vflg;
	}
	// Force the disabled flag during creation
	opts->optflgs |= dflg;

	// Set command arguments
	cmd.argc = opts->argc;
	cmd.argv = opts->argv;

	new_NVprop = new_opts->getNVOptions(CLRES_PROP);
	for (new_NVprop->start(); !new_NVprop->end(); new_NVprop->next()) {
		char *prop_name = new_NVprop->getName();
		if (!prop_name)
			continue;
		(void) new_properties.add(prop_name, new_NVprop->getValue());
	}

	new_x_NVprop = new_opts->getNVOptions(CLRES_EXT_PROP);
	old_x_NVprop = opts->getNVOptions(CLRES_EXT_PROP);
	for (new_x_NVprop->start(); !new_x_NVprop->end();
	    new_x_NVprop->next()) {
		char *prop_name = new_x_NVprop->getName();
		if (!prop_name)
			continue;
		//
		// If this is a per-node property, we might also have the
		// same property in -p specified non-per-node. In that case
		// ignore the per-node from xml.
		//
		if (dup_prop) {
			free(dup_prop);
			dup_prop = NULL;
		}
		dup_prop = strdup(prop_name);
		if (dup_prop == (char *)NULL) {
			clcommand_perror(CL_ENOMEM);
			return (CL_ENOMEM);
		}
		prop_not_needed = B_FALSE;
		if (strchr(dup_prop, '{') != NULL) {
			prop_part = strtok(dup_prop, "{");
			for (old_x_NVprop->start(); !old_x_NVprop->end();
			    old_x_NVprop->next()) {
				if (strcasecmp(prop_part,
				    old_x_NVprop->getName()) == 0) {
					prop_not_needed = B_TRUE;
					break;
				}
			}
			if (prop_not_needed) {
				continue;
			}
		}
		if (strcasecmp(new_x_NVprop->getName(), "NetIfList") == 0) {
			if (dup_prop) {
				free(dup_prop);
				dup_prop = NULL;
			}
			dup_prop = replace_nodenames(new_x_NVprop->getValue(),
			    &errflg);
			if (errflg) {
				return (errflg);
			}
			if (dup_prop) {
				(void) new_x_properties.add(
				    new_x_NVprop->getName(), dup_prop);
			}
		} else {
			(void) new_x_properties.add(new_x_NVprop->getName(),
			    new_x_NVprop->getValue());
		}
	}

	if (dup_prop)
		free(dup_prop);
	// XXX - dependent rg's and rt's
	if (opts->getOption(CLRES_AUTO) != NULL) {
		// XXX - Verify that the RG exists, if not create it
		if (false) {
			OptionValues *rg_opts = new OptionValues(
			    (char *)"create");
			rg_opts->setOperand(rgname);
			applyConfig(CLRESOURCEGROUP_CMD, NULL, rg_opts);
		}
		// XXX - Verify that the RT exists, if not create it
		if (false) {
			OptionValues *rt_opts = new OptionValues(
			    (char *)"register");
			rt_opts->setOperand(rstype);
			applyConfig(CLRESOURCETYPE_CMD, NULL, rt_opts);
		}
	}

	operand.add(new_opts->getOperand());
	rglist.add(new_opts->getOption(CLRES_GROUP));
	rtlist.add(new_opts->getOption(CLRES_TYPE));
	return (clresource_create(cmd, operand, opts->optflgs, rglist,
	    rtlist, empty_nv_list, new_x_properties, new_properties, empty_list,
	    empty_list, empty_list));
}

//
// Applies special resource property settings from the XML
// for each RS specified, in order.  This is to be executed after all the
// RSs have been created. The rs list must NOT contain a '+' sign.
// Also, the XML document must have already
// been loaded into memory, as this function has no access to the
// xml config filename.  Returns the first error from clresource_set()
// or CL_EINTERNAL.
//
clerrno_t
ClXml::RSPostProcessor(char *rsname, NameValueList &props, optflgs_t optflgs)
{
	int clerrno = CL_NOERR;
	int first_err = CL_NOERR;
	xmlNodeSet *nodes = NULL;
	NameValueList *temp_props = NULL;
	NameValueList new_props;
	ValueList oper_list;
	ValueList empty;
	NameValueList empty_nv_list;
	OptionValues *ov = new OptionValues(NULL);
	xmlNode *proplist = NULL;
	xmlNode *rnlist = NULL;
	char *tmp_str = (char *)NULL;
	char *node_str = (char *)NULL;
	char *zone_str = (char *)NULL;
	boolean_t all_monitored;
	xmlNode *rn;
	xmlNode *rn_attr;
	ValueList unmon_list;
	char *nodezone = NULL;
	int zonelen = 0;

	// If no rs, do nothing
	if (rsname == (char *)NULL) {
		return (CL_EINTERNAL);
	}

	oper_list.add(rsname);

	// Ensure that the XML doc exists
	if (doc == NULL) {
		return (CL_EINTERNAL);
	}

	// Create a generic ClCommand object
	ClCommand cmd;

	// Create an Xpath statement
	char xpath[BUFSIZ];
	xpath[0] = '\0';
	(void) sprintf(xpath, "//resource[@name=\"%s\"]", rsname);

	// Evaluate the xpath
	clerrno = evaluateXpath(xpath);
	if (clerrno) {
		clerror("Failed to update \"%s\".\n", rsname);
		return (CL_EINTERNAL);
	}

	// Check for empty nodeset
	if (xpathObj->nodesetval == NULL) {
		clerror("Failed to update \"%s\".\n", rsname);
		return (CL_EINTERNAL);
	}

	nodes = xpathObj->nodesetval;

	// Make sure that something matched the xpath
	if (nodes->nodeNr != 1) {
		clerror("Failed to update \"%s\".\n", rsname);
		return (CL_EINTERNAL);
	}

	//
	// See if the RS needs to be unmonitored, assume all nodes
	// are monitored. Resources were created monitored.
	//
	all_monitored = B_TRUE;
	for (rnlist = nodes->nodeTab[0]->children; rnlist;
	    rnlist = rnlist->next) {
		if (strcmp((char *)rnlist->name, "resourceNodeList") == 0) {
			for (rn = rnlist->children; rn; rn = rn->next) {
				if (strcmp((char *)rn->name, "resourceNode")
				    != 0)
					continue;
				node_str = (char *)xmlGetProp(rn,
				    (unsigned char *)"nodeRef");
				zone_str = (char *)xmlGetProp(rn,
				    (unsigned char *)"zone");
				if (zone_str) {
					zonelen = strlen(zone_str);
				} else {
					zonelen = 0;
				}
				nodezone = new char[strlen(node_str) +
				    zonelen + 3];
				if (zonelen > 0) {
					(void) sprintf(nodezone, "%s:%s",
					    node_str, zone_str);
				} else {
					sprintf(nodezone, "%s", node_str);
				}
				for (rn_attr = rn->children; rn_attr;
				    rn_attr = rn_attr->next) {
					if (strcmp((char *)rn_attr->name,
					    "monitoredState") == 0) {
						tmp_str = (char *)
						    xmlGetProp(rn_attr,
						    (unsigned char *)
						    "value");
						if (strcasecmp(tmp_str, "false")
						    == 0) {
							all_monitored = B_FALSE;
							unmon_list.add(
							    nodezone);
						}
						xmlFree(tmp_str);
					}
				}
				xmlFree(node_str);
				if (zone_str)
					xmlFree(zone_str);
				delete nodezone;
			}
		}
	}

	if (all_monitored == B_TRUE) {
		goto cleanup;
	}

	clerrno = clresource_unmonitor(cmd, oper_list, optflgs, empty,
	    empty, unmon_list);
	if (clerrno) {
		clerror("Failed to update the monitored state "
		    "of \"%s\".\n", rsname);
		if (first_err == 0) {
			// first error!
			first_err = clerrno;
		}
	}

cleanup:
	// delete the temp_props
	delete temp_props;
	delete ov;

	return (first_err);
}

//
// Populates the ValueList, rgs, with the name of a resource
// group.  The resource group name is the name as specified by
// each resource specified in the ValueList res.
//
// The rgs ValueList will not contain duplicates, but will be
// sorted in order of the resources specified in the res ValueList.
//
clerrno_t
ClXml::getAllRGsFromResources(ValueList &res, ValueList &rgs)
{
	clerrno_t clerrno = CL_NOERR;
	ValueList real_rgs = ValueList(true);
	xmlNodeSet *nodes = NULL;
	char xpath[BUFSIZ];

	// if no resources, do nothing
	if (res.getSize() == 0) {
		return (clerrno);
	}

	// Check for '+' operand
	if (res.isValue((char *)CLCOMMANDS_WILD_OPERAND)) {
		clerror("Wildcard operands are not permitted in the "
		    "getAllRGsFromResources() function.\n");
		return (CL_EINTERNAL);
	}

	// Ensure that the XML doc exists
	if (doc == NULL) {
		return (CL_EINTERNAL);
	}

	// Iterate through the resources
	for (res.start(); !res.end(); res.next()) {

		// Create an xpath statement
		xpath[0] = '\0';
		(void) sprintf(xpath, "//resource[@name=\"%s\"]",
		    res.getValue());

		// Evaluate the xpath
		clerrno = evaluateXpath(xpath);
		if (clerrno)
			return (clerrno);

		// Check for empty nodeset
		if (xpathObj->nodesetval == NULL) {
			continue;
		}

		nodes = xpathObj->nodesetval;

		// Make sure that something matched the xpath
		if (nodes->nodeNr != 1) {
			continue;
		}

		// Get the rg name and add it to the rg VL
		real_rgs.add(
		    (char *)xmlGetProp(nodes->nodeTab[0],
			(unsigned char *)"resourcegroupRef"));
	}

	// Copy the real rg's into the rg list
	rgs.clear();
	for (real_rgs.start(); !real_rgs.end(); real_rgs.next()) {
		rgs.add(real_rgs.getValue());
	}

	return (clerrno);
}

//
// Figure out the RT name of the specified RS in the xml
//
clerrno_t
ClXml::getRTFromResource(char *rsname, char **rt_xml)
{
	clerrno_t clerrno = CL_NOERR;
	xmlNodeSet *nodes = NULL;
	char xpath[BUFSIZ];
	char *rtname;

	// if no resources, do nothing
	if (rsname == (char *)NULL) {
		return (clerrno);
	}

	// Ensure that the XML doc exists
	if (doc == NULL) {
		return (CL_EINTERNAL);
	}

	// Create an xpath statement
	xpath[0] = '\0';
	(void) sprintf(xpath, "//resource[@name=\"%s\"]", rsname);

	// Evaluate the xpath
	clerrno = evaluateXpath(xpath);
	if (clerrno)
		return (clerrno);

	// Check for empty nodeset
	if (xpathObj->nodesetval == NULL) {
		return (CL_EINVAL);
	}

	nodes = xpathObj->nodesetval;

	// Make sure that something matched the xpath
	if (nodes->nodeNr != 1) {
		return (CL_EINVAL);
	}

	// Get the rt name and add it to the rt VL
	rtname = strdup((char *)xmlGetProp(nodes->nodeTab[0],
	    (unsigned char *)"resourcetypeRef"));
	if (rtname == NULL) {
		clcommand_perror(CL_ENOMEM);
		return (CL_ENOMEM);
	}
	*rt_xml = rtname;

	return (clerrno);
}

//
// Figure out the RG name of the specified RS in the xml
//
clerrno_t
ClXml::getRGFromResource(char *rsname, char **rg_xml)
{
	clerrno_t clerrno = CL_NOERR;
	xmlNodeSet *nodes = NULL;
	char xpath[BUFSIZ];
	char *rgname;

	// if no resources, do nothing
	if (rsname == (char *)NULL) {
		return (clerrno);
	}

	// Ensure that the XML doc exists
	if (doc == NULL) {
		return (CL_EINTERNAL);
	}

	// Create an xpath statement
	xpath[0] = '\0';
	(void) sprintf(xpath, "//resource[@name=\"%s\"]", rsname);

	// Evaluate the xpath
	clerrno = evaluateXpath(xpath);
	if (clerrno)
		return (clerrno);

	// Check for empty nodeset
	if (xpathObj->nodesetval == NULL) {
		return (CL_EINVAL);
	}

	nodes = xpathObj->nodesetval;

	// Make sure that something matched the xpath
	if (nodes->nodeNr != 1) {
		return (CL_EINVAL);
	}

	// Get the rt name and add it to the rt VL
	rgname = strdup((char *)xmlGetProp(nodes->nodeTab[0],
	    (unsigned char *)"resourcegroupRef"));
	if (rgname == NULL) {
		clcommand_perror(CL_ENOMEM);
		return (CL_ENOMEM);
	}
	*rg_xml = rgname;

	return (clerrno);
}

//
// Go through rs_list and add dependents, set the list_changed flag if
// dependets found.
//
clerrno_t
ClXml::getDependents(char *rsname, NameValueList &deps)
{
	int clerrno = CL_NOERR;
	int first_err = CL_NOERR;
	xmlNode *proplist = NULL;
	xmlNodeSet *nodes = NULL;
	xmlNode *prop = NULL;

	// Ensure that the XML doc exists
	if (doc == NULL) {
		return (CL_EINTERNAL);
	}

	// Create an Xpath statement
	char xpath[BUFSIZ];
	xpath[0] = '\0';
	(void) sprintf(xpath, "//resource[@name=\"%s\"]", rsname);

	// Evaluate the xpath
	clerrno = evaluateXpath(xpath);
	if (clerrno) {
		clerror("Failed to access \"%s\".\n", rsname);
		return (CL_EINTERNAL);
	}

	// Check for empty nodeset
	if (xpathObj->nodesetval == NULL) {
		clerror("Failed to access \"%s\".\n", rsname);
		return (CL_EINTERNAL);
	}

	nodes = xpathObj->nodesetval;

	// Make sure that something matched the xpath
	if (nodes->nodeNr != 1) {
		clerror("Failed to access \"%s\".\n", rsname);
		return (CL_EINTERNAL);
	}

	// Get the RS properties, if it exists...
	for (proplist = nodes->nodeTab[0]->children; proplist;
	    proplist = proplist->next) {
		if (strcmp((char *)proplist->name, "propertyList") == 0) {
			for (prop = proplist->children; prop;
			    prop = prop->next) {
				if (strcmp((char *)prop->name,
				    "property") == 0) {
					char *name =
					    (char *)xmlGetProp(prop,
					    (unsigned char *)"name");
					char *value =
					    (char *)xmlGetProp(prop,
					    (unsigned char *)"value");
					if ((strcasecmp(name,
					    "Resource_dependencies") == 0) ||
					    (strcasecmp(name,
					    "Resource_dependencies_weak")
					    == 0) ||
					    (strcasecmp(name,
					    "Resource_dependencies_restart")
					    == 0) ||
					    (strcasecmp(name,
"Resource_dependencies_offline_restart") == 0)) {
						if (strlen(value) > 0)
							deps.add(name, value);
					}
					xmlFree(name);
					xmlFree(value);
				}
			}
		}
	}
}
