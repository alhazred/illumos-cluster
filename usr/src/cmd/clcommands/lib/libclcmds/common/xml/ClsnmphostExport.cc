//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// ident	"@(#)ClsnmphostExport.cc 1.3	08/05/20 SMI"
//

#include "ClsnmphostExport.h"

//
// Creates the snmphostList element and sets it as the root element
//
ClsnmphostExport::ClsnmphostExport()
{
	root = xmlNewNode(NULL, (xmlChar *)"snmphostList");
}

//
// Creates a snmphost element and adds it to the snmphostList element.
//
//	name:		The host name
//	node:		The node on which the host is configured
//	community:	The community name
//
// Returns CL_EINTERNAL on any error, CL_NOERR othwerwise.
//
int
ClsnmphostExport::createSNMPHost(char *name, char *node, char *community)
{
	CHECKVALUE(name);
	CHECKVALUE(node);
	CHECKVALUE(community);

	xmlNode *snmphost = newXmlNode("snmphost");

	// add the attributes
	newXmlAttr(snmphost, "nodeRef", node);
	newXmlAttr(snmphost, "name", name);
	newXmlAttr(snmphost, "community", community);

	if (!xmlAddChild(root, snmphost)) {
		errorAddingElement("snmphost", "snmphostList");
		return (CL_EINTERNAL);
	} else
		return (CL_NOERR);
}
