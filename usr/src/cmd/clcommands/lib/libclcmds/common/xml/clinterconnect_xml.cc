//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clinterconnect_xml.cc	1.8	08/05/20 SMI"

#include "ClXml.h"
#include "clinterconnect.h"

#define	INTR_CABLE	0
#define	INTR_ADAPTER	1
#define	INTR_SWITCH	2

//
// Returns the interconnect type specified by the operand
//
int
getInterconnectType(char *operand)
{
	if (strchr(operand, ',') != NULL)
		return (INTR_CABLE);
	else if (strchr(operand, ':') != NULL)
		return (INTR_ADAPTER);
	else
		return (INTR_SWITCH);
}

//
// Returns a comma separated cable description, ie, endpoint,endpoint
// The information is taken from the xmlNode cable
//
char *
ClXml::getCableEndpoints(xmlNode *cable)
{
	char *buffer = new char [BUFSIZ];
	bool done = false;
	bool ep1_set = false;

	// Iterate through the cable elements children.  The DTD
	// mandates that there must be two endpoint elements.
	for (cable = cable->children; cable; cable = cable->next) {
		if (strcmp((char *)cable->name, "endpoint") == 0) {

			// Get the type
			char *type = (char *)xmlGetProp(cable,
			    (unsigned char *)"type");

			if (strcmp(type, (char *)"adapter") == 0) {
				// Adapter
				char *node = (char *)xmlGetProp(cable,
				    (unsigned char *)"nodeRef");

				if (node == NULL) {
					clerror("Endpoint adapter missing "
					    "nodeRef data.\n");
					set_error(CL_EINVAL);
					continue;
				}

				char *adapter = (char *)xmlGetProp(cable,
				    (unsigned char *)"name");

				// Direct connect cable has both eps as adapters
				if (!ep1_set) {
					sprintf(buffer, "%s:%s", node, adapter);
					ep1_set = true;
				} else {
					sprintf(buffer, "%s,%s:%s", buffer,
					    node, adapter);
				}

				xmlFree(node);
				xmlFree(adapter);
			} else {
				// Switch
				char *name = (char *)xmlGetProp(cable,
				    (unsigned char *)"name");

				// switch can be the first ep of a cable
				if (!ep1_set) {
					sprintf(buffer, "%s", name);
					ep1_set = true;
				} else {
					sprintf(buffer, "%s,%s", buffer, name);
				}
				xmlFree(name);
			}

			if (done) {
				// Reset ep1_set for next cable
				ep1_set = false;
				break;
			}

			done = true;
		}
	}

	return (buffer);
}

//
// Returns a ValueList of xpath statements specific to the interconnect
//
ValueList *
ClXml::clinterconnectFilter(OptionValues *opts)
{
	ValueList *list = new ValueList();
	ValueList *nodeList = opts->getOptions(CLINTER_NODE);
	char *operand = opts->getOperand();
	char *ampersand = strchr(operand, '@');
	char *colon = strchr(operand, ':');
	char *comma = strchr(operand, ',');
	int com, col, amp, count;
	char temp[120];

	switch (getInterconnectType(operand)) {

	case INTR_CABLE:

		// Get the first endpoint from the cable
		com = comma - operand;
		for (int i = 0; i < com; i++) {
			temp[i] = operand[i];
		}
		temp[com] = '\0';

		// If the first endpoint is not an adapter type, the
		// second one must be
		if (getInterconnectType(temp) != INTR_ADAPTER) {
			count = 0;
			for (int j = com + 1; j < strlen(operand); j++)
				temp[count++] = operand[j];
			temp[count] = '\0';
		}

		if (getInterconnectType(temp) != INTR_ADAPTER) {
			clerror("Cable contains no adapter endpoints - %s\n",
			    operand);
			set_error(CL_EINVAL);
		}

		char node[100];
		char adap[20];

		col = colon - operand;
		for (int i = 0; i < col; i++) {
			node[i] = temp[i];
		}
		node[col] = '\0';

		count = 0;
		for (int i = col + 1; i < strlen(temp); i++) {
			adap[count++] = temp[i];
		}
		adap[count] = '\0';

		if (nodeList->getSize() != 0) {
			for (nodeList->start();
			    !nodeList->end();
			    nodeList->next()) {

				//
				// Add to list only if operand(node) matches
				// with any of the nodes in the nodelist
				//
				if (strcmp(nodeList->getValue(), node) != 0) {
					continue;
				}

				char buffer[BUFSIZ];

				sprintf(buffer,
				    "//endpoint[@name=\"%s\"][@nodeRef=\"%s\"]",
				    adap,
				    nodeList->getValue());

				list->add(buffer);
			}
		} else {
			char buffer[BUFSIZ];

			sprintf(buffer,
			    "//endpoint[@name=\"%s\"][@nodeRef=\"%s\"]",
			    adap,
			    node);

			list->add(buffer);
		}

		break;

	case INTR_ADAPTER:
		// Iterate through the nodes specified as filters.
		// For each node given, create an xpath statement
		// which will get the transport info data for that node
		if (nodeList->getSize() != 0) {
			for (nodeList->start();
			    !nodeList->end();
			    nodeList->next()) {

				// Get nodename from operand
				col = colon - operand;
				for (int i = 0; i < col; i++) {
					temp[i] = operand[i];
				}
				temp[col] = '\0';

				//
				// Add to list only if operand(node) matches
				// with any of the nodes in the nodelist
				//
				if (strcmp(nodeList->getValue(), temp) != 0) {
					continue;
				}

				char buffer[BUFSIZ];
				sprintf(buffer,
				    "//transportNode[@nodeRef=\"%s\"]",
				    nodeList->getValue());

				// Get adaptername from operand
				count = 0;
				for (int i = col + 1;
				    i < strlen(operand); i++) {
					temp[count++] = operand[i];
				}
				temp[count] = '\0';

				sprintf(buffer,
				    "%s/transportAdapter[@name=\"%s\"]",
				    buffer, temp);

				list->add(buffer);
			}
		} else {
			// No nodes filtering needs to be done, so just create
			// an xpath statement which reflects the adapter
			// information given as the operand.
			char buffer[BUFSIZ];
			col = colon - operand;
			for (int i = 0; i < col; i++) {
				temp[i] = operand[i];
			}
			temp[col] = '\0';
			sprintf(buffer, "//transportNode[@nodeRef=\"%s\"]",
			    temp);

			count = 0;
			for (int i = col + 1; i < strlen(operand); i++) {
				temp[count++] = operand[i];
			}
			temp[count] = '\0';
			sprintf(buffer, "%s/transportAdapter[@name=\"%s\"]",
			    buffer, temp);

			list->add(buffer);
		}

		break;

	case INTR_SWITCH:
		char buffer[BUFSIZ];

		// Check for a port
		if (ampersand != NULL) {
			amp = ampersand - operand;
			for (int i = 0; i < amp; i++) {
				temp[i] = operand[i];
			}
			temp[amp] = '\0';
			sprintf(buffer, "//transportSwitch[@name=\"%s\"]",
			    temp);

			count = 0;
			for (int i = amp + 1; i < strlen(operand); i++) {
				temp[count++] = operand[i];
			}
			temp[count] = '\0';
			sprintf(buffer, "%s[@port=\"%s\"]",
			    buffer, temp);
		} else {
			sprintf(buffer, "//transportSwitch[@name=\"%s\"]",
			    operand);
		}

		list->add(buffer);
		break;

	default:
		error = CL_EINTERNAL;
		return (NULL);
	}

	return (list);
}

//
// Processes the interconnect information from the XML file and the command
// line options, and calls the function the create the transport item.
//
int
ClXml::clinterconnectProcessor(xmlNode *node, OptionValues *opts)
{
	ClCommand cmd;
	int noenable = 0;

	// Set the operand
	ValueList operands;
	operands.add(opts->getOperand());

	// Verbose?
	if (opts->getOption(VERBOSE)) {
		cmd.isVerbose = 1;
	}

	// Disable?
	if (opts->getOption(CLINTER_DISABLE)) {
		noenable = 1;
	}

	return (clinterconnect_add(cmd, operands, noenable));
}

//
// Populates the ValueList with the names of all the transport
// items in the XML file.  Returns a CL error, if one was
// encountered.
//
int
ClXml::getAllInterconnectNames(ValueList *list)
{
	xpathCtx = NULL;
	xpathObj = NULL;
	xmlNodeSet *nodes = NULL;
	int num_nodes;

	// first get the node endpoints
	error = evaluateXpath((char *)"//transportNode");
	if (error != CL_NOERR)
		return (error);

	// If the xpath returned results, get them here.
	if (xpathObj->nodesetval != NULL) {
		nodes = xpathObj->nodesetval;
		num_nodes = nodes->nodeNr;
		for (int i = 0; i < num_nodes; i++) {
			xmlNode *children = nodes->nodeTab[i]->children;
			for (children; children; children = children->next) {
				if (strcmp((char *)children->name,
				    (char *)"transportAdapter") == 0) {
					char *buffer = new char [BUFSIZ];

					sprintf(buffer, "%s:%s",
					    (char *)xmlGetProp(
						nodes->nodeTab[i],
						(unsigned char *)"nodeRef"),
					    (char *)xmlGetProp(
						children,
						(unsigned char *)"name"));

					list->add(buffer);
				}
			}
		}
	}

	// second, all the switches
	error = evaluateXpath((char *)"//transportSwitch");
	if (error != CL_NOERR)
		return (error);

	// If the xpath returned results, get them here.
	if (xpathObj->nodesetval != NULL) {
		nodes = xpathObj->nodesetval;
		num_nodes = nodes->nodeNr;
		for (int i = 0; i < num_nodes; i++) {
			char *buffer = new char[BUFSIZ];

			char *name = (char *)xmlGetProp(nodes->nodeTab[i],
			    (unsigned char *)"name");
			char *port = (char *)xmlGetProp(nodes->nodeTab[i],
			    (unsigned char *)"port");

			if (port == NULL)
				sprintf(buffer, "%s", name);
			else
				sprintf(buffer, "%s@%s", name, port);

			xmlFree(name);
			xmlFree(port);
			list->add(buffer);
		}
	}

	// lastly, all the cables
	error = evaluateXpath((char *)"//transportCable");
	if (error != CL_NOERR)
		return (error);

	// If the xpath returned results, get them here.
	if (xpathObj->nodesetval != NULL) {
		nodes = xpathObj->nodesetval;
		num_nodes = nodes->nodeNr;
		for (int i = 0; i < num_nodes; i++) {
			char *buffer = getCableEndpoints(nodes->nodeTab[i]);

			if (endpointCableComponentsDefined(list, buffer))
				list->add(buffer);
		}
	}

	return (error);
}

//
// Returns true if both endpoints in the cable are in the list of
// components given by the ValueList comps
//
bool
ClXml::endpointCableComponentsDefined(ValueList *comps, char *cable)
{
	ValueList *list = new ValueList(1, cable);

	bool found = true;
	for (list->start(); !list->end(); list->next()) {
		if (comps->isValue(list->getValue()) != 1) {
			clerror("Cable endpoint \"%s\" not found in "
			    "configuration.\n", list->getValue());
			found = false;
			set_error(CL_EINVAL);
		}
	}

	return (found);
}

//
// Populate the endpoints and their properties. Caller is responsible
// to free the memories allocated to cltr_cable_t.
//
int
ClXml::getEndpointsProps(char *node_name, cltr_cable_t **cable_props)
{
	int clerrno = CL_NOERR;
	char err_buf[BUFSIZ];
	xpathObj = NULL;
	xmlNodeSet *nodes = NULL;
	int num_nodes;
	char *prop_name = (char *)0;
	xmlNode *children;
	bool found;
	int cable_count;
	cltr_cable_t *cable_props_p = (cltr_cable_t *)0;
	char *node_ref;
	char *adp_name;
	char *tmp_buf;
	int first_prop = 1;

	// Check inputs
	if (!node_name || !cable_props) {
		clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}

	clerrno = evaluateXpath((char *)"//transportCable");
	if (clerrno != CL_NOERR) {
		return (clerrno);
	}

	// Found endpoints?
	if (xpathObj->nodesetval == NULL) {
		return (clerrno);
	}

	nodes = xpathObj->nodesetval;
	num_nodes = nodes->nodeNr;
	cable_count = 1;
	for (int i = 0; i < num_nodes; i++) {
		//
		// Go through its children. Two children define
		// one cable. The 1st loop is to check if
		// the adapter nodeRef is the given node; The 2nd
		// loop is to store this cable.
		//
		found = false;
		for (children = nodes->nodeTab[i]->children;
		    children; children = children->next) {
			// Find endpoint
			if (!children->name ||
			    (strcmp((char *)children->name,
			    "endpoint") != 0)) {
				continue;
			}

			// Get endpoint type
			prop_name = (char *)xmlGetProp(children,
			    (unsigned char *)"type");
			if (!prop_name ||
			    (strcmp(prop_name, "adapter") != 0)) {
				continue;
			}

			// Check the node name for adapter
			node_ref = (char *)xmlGetProp(children,
			    (unsigned char *)"nodeRef");

			// Get the adapter name
			prop_name = (char *)xmlGetProp(children,
			    (unsigned char *)"name");
			if (!node_ref || !prop_name ||
			    (strlen(prop_name) <= 1)) {
				continue;
			}
			tmp_buf = new char[strlen(prop_name) + 1];
			(void) strcpy(tmp_buf, prop_name);

			// In case the adapter contains node name
			adp_name = strchr(prop_name, ':');
			if (adp_name) {
				*adp_name++ = '\0';
				if (!adp_name ||
				    (strlen(adp_name) == 0)) {
					clerror("Invalid adapter "
					    "\"%s\" in configuration "
					    "file.\n", tmp_buf);
					if (clerrno == CL_NOERR) {
						clerrno = CL_EINVAL;
					}
					delete [] tmp_buf;
					continue;
				}

				// Is the node part same as nodeRef?
				if (strcmp(node_ref, prop_name) != 0) {
					clerror("The node name in adapter "
					    "\"%s\" is not same as its "
					    "nodeRef.\n",
					    tmp_buf);
					if (clerrno == CL_NOERR) {
						clerrno = CL_EINVAL;
					}
					delete [] tmp_buf;
					continue;
				}
			}
			delete [] tmp_buf;

			// Is the nodeRef the one we looking for?
			if (strcmp(node_ref, node_name) != 0) {
				continue;
			}

			if (!found) {
				found = true;
				// Add this adapter as the 1st endpoint
				cable_props_p = new cltr_cable_t;
				cable_props_p->epoint1.name = (char *)0;
				cable_props_p->epoint1.node = (char *)0;
				cable_props_p->epoint1.port = (char *)0;
				cable_props_p->epoint2.name = (char *)0;
				cable_props_p->epoint2.node = (char *)0;
				cable_props_p->epoint2.port = (char *)0;
				cable_props_p->cable_next = NULL;

				// Set the type, name, and node
				cable_props_p->epoint1.type = ADAPTER;
				if (adp_name) {
					cable_props_p->epoint1.name =
					    new char[strlen(adp_name) + 1];
					(void) strcpy(
					    cable_props_p->epoint1.name,
					    adp_name);
				} else {
					cable_props_p->epoint1.name =
					    new char[strlen(prop_name) + 1];
					(void) strcpy(
					    cable_props_p->epoint1.name,
					    prop_name);
				}
				cable_props_p->epoint1.node =
				    new char[strlen(node_ref) + 1];
				(void) strcpy(cable_props_p->epoint1.node,
				    node_ref);
			}
		}

		// Skip if not found
		if (!found) {
			continue;
		}

		//
		// The 2nd end point could be listed before the 1st one in
		// the xml So we loop again.
		//
		for (children = nodes->nodeTab[i]->children;
		    children; children = children->next) {
			// Find endpoint
			if (!children->name ||
			    (strcmp((char *)children->name,
			    "endpoint") != 0)) {
				continue;
			}

			// Get endpoint type
			prop_name = (char *)xmlGetProp(children,
			    (unsigned char *)"type");
			if ((strcmp(prop_name, (char *)"adapter") != 0) &&
			    (strcmp(prop_name, (char *)"switch") != 0)) {
				continue;
			}
			cable_props_p->epoint2.type = SWITCH;
			if (strcmp(prop_name, (char *)"adapter") == 0) {
				cable_props_p->epoint2.type = ADAPTER;
			}

			// Get the endpoint name
			prop_name = (char *)xmlGetProp(children,
			    (unsigned char *)"name");
			if (!prop_name || (strlen(prop_name) <= 1)) {
				continue;
			}

			// Adapter?
			if (cable_props_p->epoint2.type == ADAPTER) {
				// Check the node name for adapter
				node_ref = (char *)xmlGetProp(children,
				    (unsigned char *)"nodeRef");
				if (!node_ref ||
				    (strcmp(node_ref, node_name) == 0)) {
					// This is the 1st end point. Skip
					continue;
				}

				// In case the adapter contains node name
				adp_name = strchr(prop_name, ':');
				if (adp_name) {
					*adp_name++ = '\0';
					if (!adp_name ||
					    (strlen(adp_name) == 0)) {
						continue;
					}

					// Is the node part same as nodeRef?
					if (strcmp(node_ref, prop_name)
					    != 0) {
						continue;
					}
					cable_props_p->epoint2.name =
					    new char[strlen(adp_name) + 1];
					(void) strcpy(
					    cable_props_p->epoint2.name,
					    adp_name);
				} else {
					cable_props_p->epoint2.name =
					    new char[strlen(prop_name) + 1];
					(void) strcpy(
					    cable_props_p->epoint2.name,
					    prop_name);
				}

				// Add node reference
				cable_props_p->epoint2.node =
				    new char[strlen(node_ref) + 1];
				(void) strcpy(cable_props_p->epoint2.node,
				    node_ref);
			} else {
				// Add the switch name
				cable_props_p->epoint2.name =
				    new char[strlen(prop_name) + 1];
				(void) strcpy(
				    cable_props_p->epoint2.name, prop_name);

				// Switch type end point. Get the port
				prop_name = (char *)xmlGetProp(children,
				    (unsigned char *)"port");
				if (prop_name) {
					cable_props_p->epoint2.port =
					    new char[strlen(prop_name) + 1];
					(void) strcpy(
					    cable_props_p->epoint2.port,
					    prop_name);
				}
			}

			// We get both endpoints, done
			break;
		}

		// Got both endpoints?
		if (cable_props_p->epoint1.name &&
		    cable_props_p->epoint2.name) {

			if (first_prop) {
				cable_props_p->cable_next =
				    (cltr_cable_t *)NULL;
				first_prop = 0;
			} else {
				cable_props_p->cable_next = *cable_props;
			}
			*cable_props = cable_props_p;

			cable_count++;
		} else {
			// Not a valid cable.
			if (!cable_props_p->epoint1.name) {
				delete cable_props_p->epoint1.name;
				delete cable_props_p->epoint1.node;
				delete cable_props_p->epoint1.port;
			}
			if (!cable_props_p->epoint2.name) {
				delete cable_props_p->epoint2.name;
				delete cable_props_p->epoint2.node;
				delete cable_props_p->epoint2.port;
			}
			delete cable_props_p;
		}

		// We only count up to two cables
		if (cable_count == 3) {
			break;
		}
	}

	return (clerrno);
}
