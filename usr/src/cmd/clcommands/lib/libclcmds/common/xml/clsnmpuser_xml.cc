//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// ident	"@(#)clsnmpuser_xml.cc	1.3	08/05/20 SMI"
//

#include "ClXml.h"
#include "clsnmpuser.h"

//
// Returns a ValueList of xpath statements specific to the snmp user
// information
//
ValueList *
ClXml::clsnmpuserFilter(OptionValues *opts)
{
	ValueList *xpath = new ValueList();

	// Number of xpath statements depends on the number of
	// authentication protocols and nodes
	ValueList *auth = opts->getOptions(CLSNMP_AUTH);
	ValueList *nodes = opts->getOptions(CLSNMP_NODE);

	// There will always be at least one node
	for (nodes->start();
	    !nodes->end();
	    nodes->next()) {

		// First, start with the default
		char *buffer = new char [BUFSIZ];
		char *buffer2 = new char [BUFSIZ];
		sprintf(buffer, "//snmpuser[@name=\"%s\"]",
		    opts->getOperand());

		// Add the node
		sprintf(buffer, "%s[@nodeRef=\"%s\"]",
		    buffer, nodes->getValue());

		if (auth->getSize() == 0) {
			xpath->add(buffer);
		}

		for (auth->start();
		    !auth->end();
		    auth->next()) {

			// add the auth
			sprintf(buffer2, "%s[@auth=\"%s\"]",
			    buffer,
			    auth->getValue());

			xpath->add(buffer2);
			buffer2[0] = '\0';
		}
	}

	return (xpath);
}

//
// Processes the snmp user specific information from the XML file and the
// command line options, and calls the function to add the snmp user.
//
int
ClXml::clsnmpuserProcessor(xmlNode *node, OptionValues *opts)
{
	ValueList operands;
	ValueList nodes;
	char *auth = (char *)0;
	char *passfile = (char *)0;
	bool is_default = false;

	// Get the username
	char *temp = (char *)xmlGetProp(node, (unsigned char *)"name");
	operands.add(temp);
	xmlFree(temp);

	// Get the authentication type
	temp = (char *)xmlGetProp(node, (unsigned char *)"auth");
	auth = strdup(temp);
	xmlFree(temp);

	// Get the node name
	temp = (char *)xmlGetProp(node, (unsigned char *)"nodeRef");
	nodes.add(temp);
	xmlFree(temp);

	// Get the password file
	passfile = opts->getOption(CLSNMP_PASSFILE);

	// default flag?
	temp = (char *)xmlGetProp(node, (unsigned char *)"defaultUser");
	if (temp != NULL)
		is_default = true;
	xmlFree(temp);

	ClCommand cmd;
	// verbose?
	if (opts->getOption(VERBOSE) != NULL) {
		cmd.isVerbose = 1;
	} else {
		cmd.isVerbose = 0;
	}

	clerrno_t err = clsnmpuser_create(cmd, operands, nodes, auth, passfile);
	if (!err && is_default) {
		// get the sec level
		temp = (char *)xmlGetProp(node,
		    (unsigned char *)"defaultSecurityLevel");
		char *seclevel = strdup(temp);
		xmlFree(temp);

		err = clsnmpuser_set_default(cmd, operands, nodes, seclevel);
	}

	return (err);
}
