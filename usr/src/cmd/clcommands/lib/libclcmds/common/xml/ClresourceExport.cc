//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// ident	"@(#)ClresourceExport.cc 1.3	08/05/20 SMI"
//

#include "ClresourceExport.h"

#define	RESOURCE_STATE_ENABLED	(char *)"enabled"
#define	RESOURCE_STATE_DISABLED	(char *)"disabled"

#define	TRUE_STRING	(char *)"true"
#define	FALSE_STRING	(char *)"false"

//
// Creates the resourceList element and sets it as the root
//
ClresourceExport::ClresourceExport()
{
	root = newXmlNode("resourceList");
}

//
// Creates a resource element and adds it as a child of the
// resourceList element.
//
//	rName:		The name of the resource
//	rt:		The resource type
//	rg:		The resource group
//
// Returns CL_EINTERNAL on any error, CL_NOERR otherwise.
//
int
ClresourceExport::addResource(
    char *rName,
    char *rt,
    char *rg)
{
	CHECKVALUE(rName);
	CHECKVALUE(rt);
	CHECKVALUE(rg);

	xmlNode *res = newXmlNode("resource");

	// add the attributes
	newXmlAttr(res, "name", rName);
	newXmlAttr(res, "resourcetypeRef", rt);
	newXmlAttr(res, "resourcegroupRef", rg);

	// Create the resourceNodeList element
	xmlNode *rnl = newXmlNode("resourceNodeList");

	// add it to the resource
	if (!xmlAddChild(res, rnl)) {
		errorAddingElement("resourceNodeList", "resource");
		return (CL_EINTERNAL);
	}

	// Add the resource to the list
	if (!xmlAddChild(root, res)) {
		errorAddingElement("resource", "resourceList");
		return (CL_EINTERNAL);
	} else
		return (CL_NOERR);
}

int
ClresourceExport::addResourceNode(
    char *rName,
    char *nodeRef,
    state_t state,
    bool monitored,
    char *zone)
{
	CHECKVALUE(rName);
	CHECKVALUE(nodeRef);

	// Get the resource xml node
	xmlNode *res = getResource(rName);
	if (res == NULL)
		return (CL_EINTERNAL);

	// Get the resourceNodeList xml node
	xmlNode *rnl = NULL;
	for (xmlNode *temp = res->children; temp; temp = temp->next) {
		if (strcmp((char *)temp->name,
		    (char *)"resourceNodeList") == 0) {
			rnl = temp;
			break;
		}
	}

	if (rnl == NULL)
		return (CL_EINTERNAL);

	// Create the resourceNode element
	xmlNode *rNode = newXmlNode("resourceNode");
	newXmlAttr(rNode, "nodeRef", nodeRef);
	if (zone) {
		newXmlAttr(rNode, "zone", zone);
	}

	// Create the state element
	xmlNode *stateElem = newXmlNode("state");
	if (state == ENABLED)
		newXmlAttr(stateElem, "value", RESOURCE_STATE_ENABLED);
	else
		newXmlAttr(stateElem, "value", RESOURCE_STATE_DISABLED);

	// Create the monitoredState element
	xmlNode *ms = newXmlNode("monitoredState");
	if (monitored)
		newXmlAttr(ms, "value", TRUE_STRING);
	else
		newXmlAttr(ms, "value", FALSE_STRING);

	if (!xmlAddChild(rNode, stateElem)) {
		errorAddingElement("state", "resourceNode");
		return (CL_EINTERNAL);
	}
	if (!xmlAddChild(rNode, ms)) {
		errorAddingElement("monitoredState", "resourceNode");
		return (CL_EINTERNAL);
	}


	if (!xmlAddChild(rnl, rNode)) {
		errorAddingElement("resourceNode", "resourceNodeList");
		return (CL_EINTERNAL);
	} else {
		return (CL_NOERR);
	}
}

//
//
//
int
ClresourceExport::addResourceNodeProperty(
    char *rName,
    char *nodeRef,
    char *name,
    char *value,
    char *type,
    bool extension,
    bool readonly)
{
	return (ClcmdExport::addProperty(
	    getResourceNode(rName, nodeRef),
	    name,
	    value,
	    type,
	    readonly,
	    false,
	    (extension) ? TRUE : FALSE));
}

//
// Creates a property element as a child of the resource element
// referenced by rName.  The resource must already exist. The
// propertyList parent of the created element will be of type
// standard.
//
//	rName:		The name of the resource
//	name:		The property name
//	value:		The property value
//	readonly:	Is the property readonly?
//
// Returns CL_EINTERNAL on any error, CL_NOERR othwerwise.
//
int
ClresourceExport::addStandardProperty(
    char *rName,
    char *name,
    char *value,
    char *type,
    bool readonly)
{
	return (ClcmdExport::addProperty(
	    getResource(rName),
	    name,
	    value,
	    type,
	    readonly,
	    false,
	    FALSE));
}

//
// Creates a property element as a child of the resource element
// referenced by rName.  The resource must already exist.  The
// propertyList parent of the created element will be of type
// extension.
//
//	rName:		The name of the resource
//	name:		The property name
//	value:		The property value
//	readonly:	Is the property readonly?
//
// Returns CL_EINTERNAL on any error, CL_NOERR othwerwise.
//
int
ClresourceExport::addExtensionProperty(
    char *rName,
    char *name,
    char *value,
    char *type,
    bool readonly)
{
	return (ClcmdExport::addProperty(
	    getResource(rName),
	    name,
	    value,
	    type,
	    readonly,
	    false,
	    TRUE));
}

//
// Creates a property element as a child of the resource element
// referenced by rName.  The resource must already exist.  The
// propertyList parent of the created element will be of type
// standard.
//
//	rName:		The name of the resource
//	nv:		The property name-value
//	readonly:	Is the property readonly?
//
// Returns CL_EINTERNAL on any error, CL_NOERR othwerwise.
//
int
ClresourceExport::addStandardProperty(
    char *rName,
    NameValue *nv,
    char *type,
    bool readonly)
{
	return (ClcmdExport::addProperty(
	    getResource(rName),
	    nv,
	    type,
	    readonly,
	    false,
	    FALSE));
}

//
// Creates a property element as a child of the resource element
// referenced by rName.  The resource must already exist.  The
// propertyList parent of the created element will be of type
// extension.
//
//	rName:		The name of the resource
//	nv:		The property name-value
//	readonly:	Is the property readonly?
//
// Returns CL_EINTERNAL on any error, CL_NOERR othwerwise.
//
int
ClresourceExport::addExtensionProperty(
    char *rName,
    NameValue *nv,
    char *type,
    bool readonly)
{
	return (ClcmdExport::addProperty(
	    getResource(rName),
	    nv,
	    type,
	    readonly,
	    false,
	    TRUE));
}

//
// Create property elements as children of the resource element
// referenced by rName.  The resource must already exist.  The
// propertyList parent of the created elements will be of type
// standard.
//
//	rName:		The name of the resource
//	nvl:		List of property name-value
//	readonly:	Are the properties readonly?
//
// Returns CL_EINTERNAL on any error, CL_NOERR othwerwise.
//
int
ClresourceExport::addStandardProperties(
    char *rName,
    NameValueList *nvl,
    char *type,
    bool readonly)
{
	return (ClcmdExport::addProperties(
	    getResource(rName),
	    nvl,
	    type,
	    readonly,
	    false,
	    FALSE));
}

//
// Create property elements as children of the resource element
// referenced by rName.  The resource must already exist.  The
// propertyList parent of the created elements will be of type
// extension.
//
//	rName:		The name of the resource
//	nvl:		List of property name-value
//	readonly:	Are the properties readonly?
//
// Returns CL_EINTERNAL on any error, CL_NOERR othwerwise.
//
int
ClresourceExport::addExtensionProperties(
    char *rName,
    NameValueList *nvl,
    char *type,
    bool readonly)
{
	return (ClcmdExport::addProperties(
	    getResource(rName),
	    nvl,
	    type,
	    readonly,
	    false,
	    TRUE));
}

//
// Returns the resource xmlNode referenced by the resource name
// rName.  If the element is not found, then NULL is returned.
//
xmlNode *
ClresourceExport::getResource(char *rName)
{
	if (!rName)
		return (NULL);

	xmlNode *res = NULL;
	for (xmlNode *temp = root->children; temp; temp = temp->next) {
		if (strcmp((char *)temp->name, (char *)"resource") == 0) {
			char *name = (char *)xmlGetProp(temp,
			    (unsigned char *)"name");
			if (strcmp(name, rName) == 0) {
				xmlFree(name);
				res = temp;
				break;
			}
			xmlFree(name);
		}
	}

	if (res == NULL) {
		clerror("Cannot find resource - %s\n", rName);
		return (NULL);
	} else
		return (res);
}

xmlNode *
ClresourceExport::getResourceNode(char *rName, char *nodeRef)
{
	if (!rName || !nodeRef)
		return (NULL);

	xmlNode *res = getResource(rName);
	if (res == NULL)
		return (NULL);

	xmlNode *temp;
	xmlNode *rnl = NULL;
	for (temp = res->children; temp; temp = temp->next) {
		if (strcmp((char *)temp->name,
		    (char *)"resourceNodeList") == 0) {
			rnl = temp;
			break;
		}
	}

	if (rnl == NULL) {
		clerror("Cannot find resourceNodeList.\n");
		return (NULL);
	}

	xmlNode *rNode = NULL;
	for (temp = rnl->children; temp; temp = temp->next) {
		if (strcmp((char *)temp->name,
		    (char *)"resourceNode") == 0) {
			char *name = (char *)xmlGetProp(temp,
			    (unsigned char *)"nodeRef");
			if (strcmp(name, nodeRef) == 0) {
				xmlFree(name);
				rNode = temp;
				break;
			}
			xmlFree(name);
		}
	}
	xmlFree(temp);

	if (rNode == NULL) {
		clerror("Cannot find resourceNode - %s\n", nodeRef);
		return (NULL);
	} else
		return (rNode);
}
