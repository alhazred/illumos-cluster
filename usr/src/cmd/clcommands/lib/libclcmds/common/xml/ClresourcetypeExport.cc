//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// ident	"@(#)ClresourcetypeExport.cc 1.2	08/05/20 SMI"
//

#include "ClresourcetypeExport.h"

#define	TRUE_STRING	(char *)"true"
#define	FALSE_STRING	(char *)"false"

//
// Creates the resourcetypeList element and sets it as the root.
//
ClresourcetypeExport::ClresourcetypeExport()
{
	root = xmlNewNode(NULL, (xmlChar *)"resourcetypeList");
}

//
// Creates a resourcetype element and adds it to the
// resourcetypeList element.
//
//	rtName:		The resource type name
//	RTRfile:	The name of the RTR file
//
// Returns CL_EINTERNAL on any error, CL_NOERR otherwise.
//
int
ClresourcetypeExport::addResourcetype(
    char *rtName,
    char *RTRfile)
{
	CHECKVALUE(rtName);
	CHECKVALUE(RTRfile);

	xmlNode *rt = newXmlNode("resourcetype");

	// add the attribute
	newXmlAttr(rt, "name", rtName);

	// Create the RTR element
	xmlNode *rtrElem = newXmlNode("resourcetypeRTRFile");
	newXmlAttr(rtrElem, "name", RTRfile);

	// Create the resourcetypeNodeList
	xmlNode *rtnList = newXmlNode("resourcetypeNodeList");

	// Create the methodList
	xmlNode *mList = newXmlNode("methodList");
	newXmlAttr(mList, "readonly", TRUE_STRING);

	// Create the parameterList
	xmlNode *pList = newXmlNode("parameterList");
	newXmlAttr(pList, "readonly", TRUE_STRING);

	if (!xmlAddChild(rt, rtrElem)) {
		errorAddingElement("resourcetypeRTRFile", "resourcetype");
		return (CL_EINTERNAL);
	}
	if (!xmlAddChild(rt, rtnList)) {
		errorAddingElement("resourcetypeNodeList", "resourcetype");
		return (CL_EINTERNAL);
	}
	if (!xmlAddChild(rt, mList)) {
		errorAddingElement("methodList", "resourcetype");
		return (CL_EINTERNAL);
	}
	if (!xmlAddChild(rt, pList)) {
		errorAddingElement("parameterList", "resourcetype");
		return (CL_EINTERNAL);
	}

	// Add the rt to the list
	if (!xmlAddChild(root, rt)) {
		errorAddingElement("resourcetype", "resourcetypeList");
		return (CL_EINTERNAL);
	} else
		return (CL_NOERR);
}

//
// Creates a property element as a child of the resourcetype element
// referenced by rtName.  The resourcetype must already exist.
//
//	rtName:		The name of the resource type
//	name:		The property name
//	value:		The property value
//	readonly:	Is the property readonly?
//
// Returns CL_EINTERNAL on any error, CL_NOERR otherwise.
//
int
ClresourcetypeExport::addResourcetypeProperty(
    char *rtName,
    char *name,
    char *value,
    bool readonly)
{
	return (ClcmdExport::addProperty(
	    getResourcetype(rtName),
	    name,
	    value,
	    NULL, // type
	    readonly,
	    true));
}

//
// Creates a property element as a child of the resourcetype element
// referenced by rtName.  The resourcetype must already exist.
//
//	rtName:		The name of the resource type
//	nv:		The property name-value
//	readonly:	Is the property readonly?
//
// Returns CL_EINTERNAL on any error, CL_NOERR otherwise.
//
int
ClresourcetypeExport::addResourcetypeProperty(
    char *rtName,
    NameValue *nv,
    bool readonly)
{
	return (ClcmdExport::addProperty(
	    getResourcetype(rtName),
	    nv,
	    NULL, // type
	    readonly,
	    true));
}

//
// Creates property elements as children of the resourcetype element
// referenced by rtName.  The resourcetype must already exist.
//
//	rtName:		The name of the resource type
//	nvl:		A list of property name-values
//	readonly:	Are the properties readonly?
//
// Returns CL_EINTERNAL on any error, CL_NOERR otherwise.
//
int
ClresourcetypeExport::addResourcetypeProperties(
    char *rtName,
    NameValueList *nvl,
    bool readonly)
{
	return (ClcmdExport::addProperties(
	    getResourcetype(rtName),
	    nvl,
	    NULL, // type
	    readonly,
	    true));
}

//
// Creates a method element as a child of the resourcetype element
// listed.  The resource type must have already been created.
//
//	rtName:	The name of the resource type
//	name:	The method name
//	type:	The type of method
//
// Returns CL_EINTERNAL on any error, CL_NOERR otherwise.
//
int
ClresourcetypeExport::addMethod(char *rtName, char *name, char *type)
{
	CHECKVALUE(rtName);
	CHECKVALUE(name);
	CHECKVALUE(type);

	xmlNode *rt = getResourcetype(rtName);
	if (rt == NULL) {
		clerror("Cannot find resourcetype element - %s\n", rtName);
		return (CL_EINTERNAL);
	}

	// Get the methodList
	xmlNode *mList = NULL;
	for (xmlNode *temp = rt->children; temp; temp = temp->next) {
		if (strcmp((char *)temp->name,
		    (char *)"methodList") == 0) {
			mList = temp;
			break;
		}
	}
	if (mList == NULL) {
		clerror("Cannot find methodList element\n");
		return (CL_EINTERNAL);
	}

	// Create the method element
	xmlNode *method = newXmlNode("method");
	newXmlAttr(method, "name", name);
	newXmlAttr(method, "type", type);

	if (!xmlAddChild(mList, method)) {
		errorAddingElement("method",
		    "methodList");
		return (CL_EINTERNAL);
	} else
		return (CL_NOERR);
}

//
// Creates a parameter element and adds it to the resourcetype element
// referenced by rtName.  The resource type element must already exist.
//
//	rtName:		The name of the resource type
//	name:		The parameter name
//	extension:	Is this an extension parameter?
//	per_node:	Is this a per node parameter?
//	description:	The parameter description
//	tunability:	The tunability value
//	type:		The parameter type
//	enumList:	The enumList string (optional)
//	minLength:	minLength value (optional)
//	maxLength:	maxLength value (optional)
//	minArrayLength:	minArrayLength value (optional)
//	maxArrayLength:	maxArrayLength value (optional)
//	def:		The default value (optional)
//
// Returns CL_EINTERNAL on any error, CL_NOERR otherwise.
//
int
ClresourcetypeExport::addParameter(
    char *rtName,
    char *name,
    bool extension,
    bool per_node,
    char *description,
    tunability_t tunability,
    param_type_t type,
    char *enumList,
    char *minLength,
    char *maxLength,
    char *minArrayLength,
    char *maxArrayLength,
    char *def)
{
	CHECKVALUE(rtName);
	CHECKVALUE(name);
	CHECKVALUE(description);

	xmlNode *rt = getResourcetype(rtName);
	if (rt == NULL) {
		clerror("Cannot find resourcetype element - %s\n", rtName);
		return (CL_EINTERNAL);
	}

	// Get the parameterList
	xmlNode *pList = NULL;
	for (xmlNode *temp = rt->children; temp; temp = temp->next) {
		if (strcmp((char *)temp->name,
		    (char *)"parameterList") == 0) {
			pList = temp;
			break;
		}
	}
	if (pList == NULL) {
		clerror("Cannot find parameterList element\n");
		return (CL_EINTERNAL);
	}

	xmlNode *para = newXmlNode("parameter");
	newXmlAttr(para, "name", name);
	if (extension)
		newXmlAttr(para, "extension", TRUE_STRING);
	else
		newXmlAttr(para, "extension", FALSE_STRING);

	if (per_node)
		newXmlAttr(para, "per-node", TRUE_STRING);
	else
		newXmlAttr(para, "per-node", FALSE_STRING);

	newXmlAttr(para, "description", description);

	switch (tunability) {

	case AT_CREATION:
		newXmlAttr(para, "tunability", (char *)"atCreation");
		break;
	case WHEN_DISABLED:
		newXmlAttr(para, "tunability", (char *)"whenDisabled");
		break;
	case ANY_TIME:
		newXmlAttr(para, "tunability", (char *)"anyTime");
		break;
	case NONE:
		newXmlAttr(para, "tunability", (char *)"none");
		break;
	case UNKNOWN_TYPE:
		newXmlAttr(para, "tunability", (char *)"unknown");
		break;
	default:
		return (CL_EINTERNAL);
	}

	switch (type) {

	case INTEGER:
		newXmlAttr(para, "type", (char *)"int");
		break;
	case BOOLEAN:
		newXmlAttr(para, "type", (char *)"boolean");
		break;
	case ENUM:
		newXmlAttr(para, "type", (char *)"enum");
		break;
	case STRING:
		newXmlAttr(para, "type", (char *)"string");
		break;
	case STRING_ARRAY:
		newXmlAttr(para, "type", (char *)"stringarray");
		break;
	default:
		return (CL_EINTERNAL);
	}

	if (enumList)
		newXmlAttr(para, "enumList", enumList);
	if (minLength)
		newXmlAttr(para, "minLength", minLength);
	if (maxLength)
		newXmlAttr(para, "maxLength", maxLength);
	if (minArrayLength)
		newXmlAttr(para, "minArrayLength", minArrayLength);
	if (maxArrayLength)
		newXmlAttr(para, "maxArrayLength", maxArrayLength);
	if (def)
		newXmlAttr(para, "default", def);

	if (!xmlAddChild(pList, para)) {
		errorAddingElement("parameter", "parameterList");
		return (CL_EINTERNAL);
	} else
		return (CL_NOERR);
}

//
// Creates a resourcetypeNode element and adds it to the resourcetype
// element.  The resource type must already exist.  The resourcetypeNode
// element and the allNodes elements are mutually exclusive.
//
//	rtName:		The name of the resource
//	nodeName:	The name of the node
//
// Returns CL_EINTERNAL on any error, CL_NOERR otherwise.
//
int
ClresourcetypeExport::addResourcetypeNode(char *rtName, char *nodeName)
{
	CHECKVALUE(rtName);
	CHECKVALUE(nodeName);

	// Get the resourcetype element
	xmlNode *rt = getResourcetype(rtName);
	if (rt == NULL) {
		clerror("Cannot find resourcetype - %s\n", rtName);
		return (CL_EINTERNAL);
	}

	// Make sure the resourcetypeNodeList exists
	xmlNode *rtnList = NULL;
	for (xmlNode *temp = rt->children; temp; temp = temp->next) {
		if (strcmp((char *)temp->name,
		    (char *)"resourcetypeNodeList") == 0) {
			rtnList = temp;
			break;
		}
	}
	if (rtnList == NULL) {
		clerror("Cannot find resourcetypeNodeList element\n");
		return (CL_EINTERNAL);
	}

	// Create the resourcetypeNode element
	xmlNode *rtn = newXmlNode("resourcetypeNode");
	newXmlAttr(rtn, "nodeRef", nodeName);

	if (!xmlAddChild(rtnList, rtn)) {
		errorAddingElement("resourcetypeNode",
		    "resourcetypeNodeList");
		return (CL_EINTERNAL);
	} else
		return (CL_NOERR);
}

//
// Creates the allNodes element as a child of the resourcetype
// element.  The resource type must already exist.
//
//	rtName:	The name of the resource type
//
// Returns CL_EINTERNAL on any error, CL_NOERR otherwise.
//
int
ClresourcetypeExport::setAllNodes(char *rtName)
{
	CHECKVALUE(rtName);

	// Get the resourcetype element
	xmlNode *rt = getResourcetype(rtName);
	if (rt == NULL) {
		clerror("Cannot find resourcetype - %s\n", rtName);
		return (CL_EINTERNAL);
	}

	// delete the resourcetypeNodeList children if they exist
	xmlNode *rtnList = NULL;
	for (xmlNode *temp = rt->children; temp; temp = temp->next) {
		if (strcmp((char *)temp->name,
		    (char *)"resourcetypeNodeList") == 0) {
			xmlUnlinkNode(temp->children);
			xmlFreeNode(temp->children);
			rtnList = temp;
			break;
		}
	}
	if (rtnList == NULL)
		return (CL_EINTERNAL);

	// Create the allNodes element
	xmlNode *an = newXmlNode("allNodes");

	if (!xmlAddChild(rtnList, an)) {
		errorAddingElement("allNodes",
		    "resourcetypeNodeList");
		return (CL_EINTERNAL);
	} else
		return (CL_NOERR);
}

//
// Returns an xmlNode resourcetype element referenced by the rtName.
// If the element is not found, NULL is returned.
//
xmlNode *
ClresourcetypeExport::getResourcetype(char *rtName)
{
	if (!rtName)
		return (NULL);

	xmlNode *rt = NULL;
	for (xmlNode *temp = root->children; temp; temp = temp->next) {
		if (strcmp((char *)temp->name, (char *)"resourcetype") == 0) {
			char *name = (char *)xmlGetProp(temp,
			    (unsigned char *)"name");
			if (strcmp(name, rtName) == 0) {
				xmlFree(name);
				rt = temp;
				break;
			}
			xmlFree(name);
		}
	}
	if (rt == NULL) {
		clerror("Cannot find resourcetype - %s\n", rtName);
		return (NULL);
	} else
		return (rt);
}
