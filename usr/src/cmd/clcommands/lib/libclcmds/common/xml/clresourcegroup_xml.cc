//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// ident	"@(#)clresourcegroup_xml.cc	1.8	08/05/20 SMI"
//

#include <sys/os.h>
#include <scadmin/scconf.h>
#include <scadmin/scswitch.h>
#include <scadmin/scstat.h>
#include <rgm/rgm_scrgadm.h>
#include <rgm/rgm_cnfg.h>

#include "ClXml.h"
#include "clresourcegroup.h"

//
// Returns a ValueList of xpath statements specific to the
// resource group info
//
ValueList *
ClXml::clresourcegroupFilter(OptionValues *opts)
{
	ValueList *xpath = new ValueList();

	// No options will make the number of XPath statements
	// more than one.  So create the default xpath expression
	// and append the name of the rg
	char *buffer = new char [BUFSIZ];

	sprintf(buffer,
	    "//resourcegroup[@name=\"%s\"]",
	    opts->getOperand());

	xpath->add(buffer);

	return (xpath);
}

//
// Processes the resource group specific information from the XML
// file and the command line options, and calls the function to create
// the resource groups.
//
clerrno_t
ClXml::clresourcegroupProcessor(xmlNode *node, OptionValues *opts)
{
	NameValueList new_properties;
	NameValueList *new_NVprop;
	OptionValues *new_opts = new OptionValues(opts->getSubcommand());
	ValueList *nodelist;
	ValueList operand;
	char *tmp_str = (char *)NULL;
	xmlNode *tempNode = NULL;
	xmlNode *proplist = NULL;
	boolean_t is_scalable = B_FALSE;

	// Set the resourcegroup name
	tmp_str = (char *)xmlGetProp(node, (unsigned char *)"name");
	new_opts->setOperand(tmp_str);
	xmlFree(tmp_str);

	// Check for Scalable flag in the XML
	for (tempNode = node->children; tempNode; tempNode = tempNode->next) {
		if (strcmp((char *)tempNode->name, "failoverMode") == 0) {
			tmp_str = (char *)xmlGetProp(tempNode,
			    (unsigned char *)"value");
			if (strcasecmp(tmp_str, "scalable") == 0) {
				is_scalable = B_TRUE;
				break;
			}
			xmlFree(tmp_str);
			break;
		}
	}

	// Check for node list from command line, if present
	// pass it along, otherwise, create one from the data
	// in the XML.
	ValueList *cli_nodelist = opts->getOptions(CLRG_NODE);
	if (cli_nodelist->getSize() == 0) {
		// Nodelist not set, get it from the XML
		for (tempNode = node->children;
		    tempNode;
		    tempNode = tempNode->next) {
			if (strcmp((char *)tempNode->name,
			    "resourcegroupNodeList") == 0) {
				for (tempNode = tempNode->children;
				    tempNode;
				    tempNode = tempNode->next) {

					if (strcmp(
					    (char *)tempNode->name,
					    "resourcegroupNode") == 0) {

						// Get the node name
						char nodename[BUFSIZ];
						tmp_str = (char *)xmlGetProp(
						    tempNode,
						    (unsigned char *)"nodeRef");
						(void) sprintf(nodename,
						    tmp_str);
						xmlFree(tmp_str);

						// Get the zone, if present
						tmp_str = (char *)xmlGetProp(
						    tempNode,
						    (unsigned char *)"zone");
						if (tmp_str != NULL) {
							(void) sprintf(nodename,
							    "%s:%s", nodename,
							    tmp_str);
							xmlFree(tmp_str);
						}

						new_opts->addOption(
						    CLRG_NODE, nodename);
					}
				}
				break;
			}
		}
	} else {
		// Node list set on cmd line, pass it along
		for (cli_nodelist->start(); !cli_nodelist->end();
		    cli_nodelist->next()) {
			new_opts->addOption(CLRG_NODE,
			    cli_nodelist->getValue());
		}
	}

	// Now get all the properties
	for (proplist = node->children; proplist; proplist = proplist->next) {
		if (strcmp((char *)proplist->name, "propertyList") == 0)
			break;
	}

	// Call generic property processing routine
	genericPropertyOverrides(
	    proplist,
	    opts->getNVOptions(CLRG_PROP),
	    CLRG_PROP,
	    new_opts);

	ClCommand cmd;
	// verbose?
	if (opts->getOption(VERBOSE) != NULL) {
		cmd.isVerbose = 1;
	} else {
		cmd.isVerbose = 0;
	}

	// Set command arguments
	cmd.argc = opts->argc;
	cmd.argv = opts->argv;

	new_NVprop = new_opts->getNVOptions(CLRG_PROP);
	for (new_NVprop->start(); !new_NVprop->end(); new_NVprop->next()) {
		char *prop_name = new_NVprop->getName();
		if (!prop_name)
			continue;
		if ((strcasecmp(prop_name, SCHA_RG_SUSP_AUTO_RECOVERY) == 0) ||
		    (strcasecmp(prop_name, "RG_SLM_projectname") == 0)) {
			// These can't be set like a regular property
			continue;
		}
		if (strcasecmp(prop_name, SCHA_NODELIST) == 0) {
			//
			// The nodelist property has alreday been converted
			// to the -n option.
			//
			continue;
		}
		if ((strcasecmp(prop_name, SCHA_RG_DEPENDENCIES) == 0) ||
		    (strcasecmp(prop_name, SCHA_RG_AFFINITIES) == 0)) {
			//
			// We want to set this two after all the RGs are
			// created.
			//
			continue;
		}
		(void) new_properties.add(prop_name, new_NVprop->getValue());
	}

	if (is_scalable) {
		(void) new_properties.add(SCHA_RG_MODE, "scalable");
	}

	if (new_opts->getOption(CLRG_SCALABLE) != NULL) {
		opts->optflgs |= Sflg;
	}

	operand.add(new_opts->getOperand());
	nodelist = new_opts->getOptions(CLRG_NODE);

	return (clrg_create(cmd, operand, opts->optflgs, *nodelist,
	    new_properties));
}

//
// Applies special resource group property settings from the XML
// for each RG specified, in order.  This is to be executed after all the
// RGs have been created. The rgs list must NOT contain a '+' sign.
// Also, the XML document must have already
// been loaded into memory, as this function has no access to the
// xml config filename.  Returns the first error from clrg_set()/clrg_suspend()
// or CL_EINTERNAL.
//
clerrno_t
ClXml::RGPostProcessor(char *rgname, NameValueList &props, optflgs_t optflgs)
{
	int clerrno = CL_NOERR;
	int first_err = CL_NOERR;
	xmlNodeSet *nodes = NULL;
	NameValueList *temp_props = NULL;
	NameValueList new_props;
	ValueList oper_list;
	boolean_t is_suspended = B_FALSE;
	boolean_t is_managed = B_FALSE;
	ValueList empty;
	OptionValues *ov = new OptionValues(NULL);
	xmlNode *proplist = NULL;
	char *tmp_str = (char *)NULL;

	// If no rgs, do nothing
	if (rgname == (char *)NULL) {
		return (CL_EINTERNAL);
	}

	oper_list.add(rgname);

	// Ensure that the XML doc exists
	if (doc == NULL) {
		return (CL_EINTERNAL);
	}

	// Create a generic ClCommand object
	ClCommand cmd;

	// Create an Xpath statement
	char xpath[BUFSIZ];
	xpath[0] = '\0';
	(void) sprintf(xpath, "//resourcegroup[@name=\"%s\"]", rgname);

	// Evaluate the xpath
	clerrno = evaluateXpath(xpath);
	if (clerrno) {
		clerror("Failed to update \"%s\".\n", rgname);
		return (CL_EINTERNAL);
	}

	// Check for empty nodeset
	if (xpathObj->nodesetval == NULL) {
		clerror("Failed to update \"%s\".\n", rgname);
		return (CL_EINTERNAL);
	}

	nodes = xpathObj->nodesetval;

	// Make sure that something matched the xpath
	if (nodes->nodeNr != 1) {
		clerror("Failed to update \"%s\".\n", rgname);
		return (CL_EINTERNAL);
	}

	// See if the RG is managed
	for (proplist = nodes->nodeTab[0]->children; proplist;
	    proplist = proplist->next) {
		if (strcmp((char *)proplist->name, "managedState") == 0) {
			tmp_str = (char *)xmlGetProp(proplist,
			    (unsigned char *)"value");
			if (strcasecmp(tmp_str, "true") == 0) {
				is_managed = B_TRUE;
			}
			xmlFree(tmp_str);
		}
	}

	// Get the RG properties, if it exists...
	for (proplist = nodes->nodeTab[0]->children; proplist;
	    proplist = proplist->next) {
		if (strcmp((char *)proplist->name, "propertyList") == 0)
			break;
	}
	if (proplist) {

		// Call generic property processing routine
		genericPropertyOverrides(
		    proplist,
		    &props,
		    CLRG_PROP,
		    ov);

		// Call the set method to apply the properties
		temp_props = ov->getNVOptions(CLRG_PROP);
		for (temp_props->start(); temp_props->end() != 1;
		    temp_props->next()) {
			if ((strcasecmp(SCHA_RG_AFFINITIES,
			    temp_props->getName()) == 0) ||
			    (strcasecmp(SCHA_RG_DEPENDENCIES,
			    temp_props->getName()) == 0)) {
				if (strlen(temp_props->getValue()) > 0) {
					new_props.add(temp_props->getName(),
					    temp_props->getValue());
				}
			}
			if ((strcasecmp(SCHA_RG_SUSP_AUTO_RECOVERY,
			    temp_props->getName()) == 0) &&
			    (strcasecmp(temp_props->getValue(), "true") == 0)) {
				is_suspended = B_TRUE;
			}
		}
	}

	if ((new_props.getSize() == 0) && (!is_suspended) && (!is_managed)) {
		goto cleanup;
	}

	if (new_props.getSize() > 0) {
		clerrno = clrg_set(cmd, oper_list, optflgs, new_props, empty);
		if (clerrno) {
			clerror("Failed to update the \"%s\" or \"%s\" "
			    "property of \"%s\".\n", SCHA_RG_DEPENDENCIES,
			    SCHA_RG_AFFINITIES, rgname);
			if (first_err == 0) {
				// first error!
				first_err = clerrno;
			}
		}
	}
	if (is_suspended) {
		clerrno = clrg_suspend(cmd, oper_list, optflgs);
		if (clerrno) {
			clerror("Failed to suspend \"%s\".\n", rgname);
			if (first_err == 0) {
				// first error!
				first_err = clerrno;
			}
		}
	}
	if (is_managed) {
		clerrno = clrg_manage(cmd, oper_list, optflgs);
		if (clerrno) {
			clerror("Failed to manage \"%s\".\n", rgname);
			if (first_err == 0) {
				// first error!
				first_err = clerrno;
			}
		}
	}

cleanup:
	// delete the temp_props
	delete temp_props;
	delete ov;

	return (first_err);
}
