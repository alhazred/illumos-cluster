//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// ident	"@(#)CldeviceExport.cc 1.3	08/05/20 SMI"
//

#include "CldeviceExport.h"

#define	TRUE_STRING	(char *)"true"
#define	FALSE_STRING	(char *)"false"

//
// Instantiate the CldeviceExport class.  Creates a deviceList element
// as the root node.  The deviceList elements are always readonly, so
// that attribute is set as well.
//
CldeviceExport::CldeviceExport()
{
	root = newXmlNode("deviceList");
	newXmlAttr(root, "readonly", TRUE_STRING);
}

//
// Creates a device element and adds it to the root node.
//
//	name:		The name of the device
//	ctd:		The ctd (unix disk name) for the device
//	monitored:	Is this device monitored?
//
// Returns CL_EINTERNAL on any error, CL_NOERR otherwise.
//
int
CldeviceExport::createDevice(char *name, char *ctd)
{
	CHECKVALUE(name);
	CHECKVALUE(ctd);

	xmlNode *device = newXmlNode("device");

	// add the attributes
	newXmlAttr(device, "name", name);
	newXmlAttr(device, "ctd", ctd);

	if (!xmlAddChild(root, device)) {
		errorAddingElement("device", "deviceList");
		return (CL_EINTERNAL);
	} else
		return (CL_NOERR);
}

//
// Creates a devicePath element as a child of the device element
// referenced by deviceName.  The device must have already been
// created.
//
//	deviceName:	The name of the device
//	node:		The name of the node
//
// Returns CL_EINTERNAL on any error, CL_NOERR otherwise.
//
int
CldeviceExport::addDevicePath(char *deviceName, char *node, bool monitored)
{
	CHECKVALUE(deviceName);
	CHECKVALUE(node);

	// Get the device element
	xmlNode *device = NULL;
	for (xmlNode *temp = root->children; temp; temp = temp->next) {
		if (strcmp((char *)temp->name, (char *)"device") == 0) {
			char *name = (char *)xmlGetProp(temp,
			    (unsigned char *)"name");
			if (strcmp(name, deviceName) == 0) {
				xmlFree(name);
				device = temp;
				break;
			}
			xmlFree(name);
		}
	}

	if (device == NULL) {
		clerror("No such device exists in the configuration - %s\n",
		    deviceName);
		return (CL_EINTERNAL);
	}

	// Create the device node element
	xmlNode *devicePath = newXmlNode("devicePath");
	newXmlAttr(devicePath, "nodeRef", node);
	if (monitored)
		newXmlAttr(devicePath, "monitored", TRUE_STRING);
	else
		newXmlAttr(devicePath, "monitored", FALSE_STRING);


	if (!xmlAddChild(device, devicePath)) {
		errorAddingElement("devicePath", "device");
		return (CL_EINTERNAL);
	} else
		return (CL_NOERR);
}
