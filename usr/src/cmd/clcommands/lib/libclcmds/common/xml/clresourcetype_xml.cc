//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// ident	"@(#)clresourcetype_xml.cc	1.4	08/05/20 SMI"
//

#include <sys/os.h>
#include <scadmin/scconf.h>
#include <scadmin/scswitch.h>
#include <scadmin/scstat.h>
#include <rgm/rgm_scrgadm.h>
#include <rgm/rgm_cnfg.h>

#include "ClXml.h"
#include "clresourcetype.h"

//
// Returns a ValueList of xpath statements specific to the cluster
// resource type given
//
ValueList *
ClXml::clresourcetypeFilter(OptionValues *opts)
{
	ValueList *xpath = new ValueList();

	// No options will make the number of XPath statements
	// more than one.  So create the default xpath expression
	// and append the name of the rt
	char *buffer = new char [BUFSIZ];

	sprintf(buffer,
	    "//resourcetype[@name=\"%s\"]",
	    opts->getOperand());

	xpath->add(buffer);

	return (xpath);
}

//
// Processes the resource type information from the XML file and the command
// line options, and calls the function to add the resource type.
//
int
ClXml::clresourcetypeProcessor(xmlNode *node, OptionValues *opts)
{
	NameValueList new_properties;
	NameValueList *new_NVprop;
	OptionValues *new_opts = new OptionValues(opts->getSubcommand());
	ValueList *nodelist;
	ValueList operand;
	char *tmp_str = (char *)NULL;
	xmlNode *tempNode = NULL;
	xmlNode *proplist = NULL;
	char *rtrfile = (char *)NULL;

	// Set the resourcetype name
	tmp_str = (char *)xmlGetProp(node, (unsigned char *)"name");
	new_opts->setOperand(tmp_str);
	xmlFree(tmp_str);

	// Get the RTR file mode
	tmp_str = opts->getOption(CLRT_RTRFILE);
	if (tmp_str == NULL) {
		// RTR file not set, grab from XML file.
		xmlNode *tempNode = NULL;
		for (tempNode = node->children;
		    tempNode;
		    tempNode = tempNode->next) {
			if (strcmp((char *)tempNode->name,
			    "resourcetypeRTRFile") == 0) {
				tmp_str = (char *)xmlGetProp(tempNode,
				    (unsigned char *)"name");
				if (strlen(tmp_str) != 0) {
					new_opts->addOption(
					    CLRT_RTRFILE,
					    tmp_str);
				}
				xmlFree(tmp_str);
				break;
			}
		}
	} else {
		// RTR file set, pass it along
		new_opts->addOption(CLRT_RTRFILE,
		    tmp_str);
	}

	//
	// Check for node list from command line, if present
	// pass it along, otherwise, create one from the data
	// in the XML.
	ValueList *cli_nodelist = opts->getOptions(CLRT_NODE);
	if ((cli_nodelist->getSize() == 0) &&
	    (opts->getOption(CLRT_ALLNODES) == NULL)) {
		// Nodelist not set, get it from the XML
		for (tempNode = node->children;
		    tempNode;
		    tempNode = tempNode->next) {
			if (strcmp((char *)tempNode->name,
			    "resourcetypeNodeList") == 0) {
				for (tempNode = tempNode->children;
				    tempNode;
				    tempNode = tempNode->next) {

					if (strcmp(
					    (char *)tempNode->name,
					    "resourcetypeNode") == 0) {

						// Check for 'allNodes'
						if (strcmp(
						    (char *)tempNode->name,
						    "allNodes") == 0) {

							new_opts->addOption(
							    CLRT_ALLNODES);
							break;
						}

						// Check for node names
						if (strcmp(
						    (char *)tempNode->name,
						    "resouretypeNode") == 0) {

							tmp_str = (char *)
							    xmlGetProp(
							    tempNode,
							    (unsigned char *)
							    "nodeRef");

							new_opts->addOption(
							    CLRT_NODE,
							    tmp_str);
							xmlFree(tmp_str);
						}
					}
				}
				break;
			}
		}
	} else if (opts->getOption(CLRT_ALLNODES) != NULL) {
		new_opts->addOption(CLRT_ALLNODES);
	} else {
		// Node list set on cmd line, pass it along
		for (cli_nodelist->start(); !cli_nodelist->end();
		    cli_nodelist->next()) {
			new_opts->addOption(CLRT_NODE,
			    cli_nodelist->getValue());
		}
	}

	// Now get all the properties
	for (proplist = node->children; proplist; proplist = proplist->next) {
		if (strcmp((char *)proplist->name, "propertyList") == 0)
			break;
	}

	// Call generic property processing routine
	genericPropertyOverrides(
	    proplist,
	    opts->getNVOptions(CLRT_PROP),
	    CLRT_PROP,
	    new_opts);

	ClCommand cmd;
	// verbose?
	if (opts->getOption(VERBOSE) != NULL) {
		cmd.isVerbose = 1;
	} else {
		cmd.isVerbose = 0;
	}

	// Set command arguments
	cmd.argc = opts->argc;
	cmd.argv = opts->argv;

	new_NVprop = new_opts->getNVOptions(CLRT_PROP);
	for (new_NVprop->start(); !new_NVprop->end(); new_NVprop->next()) {
		char *prop_name = new_NVprop->getName();
		if (!prop_name)
			continue;
		if (strcasecmp(prop_name, SCHA_RT_SYSTEM) != 0) {
			// These can't be set!
			continue;
		}
		(void) new_properties.add(prop_name, new_NVprop->getValue());
	}

	operand.add(new_opts->getOperand());
	nodelist = new_opts->getOptions(CLRT_NODE);
	rtrfile = new_opts->getOption(CLRT_RTRFILE);

	return (clrt_register(cmd, operand, opts->optflgs, *nodelist,
	    rtrfile, new_properties));
}
