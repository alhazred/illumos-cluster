//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// ident	"@(#)ClcmdExport.cc 1.2	08/05/20 SMI"
//

#include "ClcmdExport.h"

#define	PROP_EXTENSION	(char *)"true"
#define	PROP_STANDARD	(char *)"false"
#define	PROP_MIXED	(char *)"mixed"
#define	PROP_DNA	(char *)"doesNotApply"

#define	TRUE_STRING	(char *)"true"
#define	FALSE_STRING	(char *)"false"

//
// Helper function which creates an xmlNode with the specified name
//
// Returns the new xmlNode *
//
xmlNode *
ClcmdExport::newXmlNode(const char *name)
{
	return (xmlNewNode(NULL, (xmlChar *)name));
}

//
// Helper function which adds an attribute to an xmlNode
//
//	node:	The xmlNode to add the attribute to
//	name:	The name of the attribute
//	value:	The attribute value
//
void
ClcmdExport::newXmlAttr(xmlNode *node, const char *name, char *value)
{
	xmlNewProp(node, (xmlChar *)name, (xmlChar *)value);
}

//
// Creates the propertyList element as a child of the xmlNode, parent.
//
//	parent:		The xmlNode to add the propertyList as a child of
//	readonly:	Is this a readonly propertyList?
//	ext:		The extension type (TRUE, FALSE, MIXED, or DOESNOTAPPLY)
//
// Returns CL_EINTERNAL on any error, CL_NOERR otherwise
//
int
ClcmdExport::beginPropertyList(
    xmlNode *parent,
    bool readonly,
    extension_t ext)
{
	// Ensure parent is not NULL
	CHECKVALUE(parent);

	xmlNode *proplist = newXmlNode("propertyList");

	// Only add the readonly property if set to true
	if (readonly)
		newXmlAttr(proplist, "readonly", TRUE_STRING);

	switch (ext) {

	case TRUE:
		newXmlAttr(proplist, "extension", PROP_EXTENSION);
		break;
	case FALSE:
		newXmlAttr(proplist, "extension", PROP_STANDARD);
		break;
	case MIXED:
		newXmlAttr(proplist, "extension", PROP_MIXED);
		break;
	case DOESNOTAPPLY:
		// Don't add the extension type if it does not apply
		break;
	}

	// Add propertyList to object
	if (!xmlAddChild(parent, proplist)) {
		errorAddingElement("propertyList", (const char *)parent->name);
		return (CL_EINTERNAL);
	} else
		return (CL_NOERR);
}

//
// Adds a property to the given parent node.  A new property element
// is created as a child of the propertyList element.  If the propertyList
// element does not yet exist, then the beginPropertyList method is called
// with the ro_propList and ext_propList parameters.
//
//	parent:		The parent xmlNode
//	name:		The name of the property
//	value:		The property value
//	readonly:	Is this a readonly property?
//	ro_propList:	Is the propertyList readonly?
//	ext_propList:	The extenstion type of the propertyList
//
// Returns CL_EINTERNAL on any error, CL_NOERR otherwise.
//
int
ClcmdExport::addProperty(
    xmlNode *parent,
    char *name,
    char *value,
    char *type,
    bool readonly,
    bool ro_propList,
    extension_t ext_propList)
{
	// Verify parent is not NULL
	CHECKVALUE(parent);

	// Create the property
	xmlNode *prop = newXmlNode("property");
	newXmlAttr(prop, "name", name);
	newXmlAttr(prop, "value", value);

	// Add the type, if it is not NULL
	if (type != NULL)
		newXmlAttr(prop, "type", type);

	// Only add the readonly attribute if it is 'true'
	if (readonly)
		newXmlAttr(prop, "readonly", TRUE_STRING);

	// Check if the PropertyList element exists yet
	xmlNode *proplist = NULL;
	for (xmlNode *tempNode = parent->children;
	    tempNode;
	    tempNode = tempNode->next) {

		if (strcmp((char *)tempNode->name,
		    (char *)"propertyList") == 0) {

			if (ext_propList == DOESNOTAPPLY) {
				proplist = tempNode;
				break;
			}

			char *type = (char *)xmlGetProp(tempNode,
			    (unsigned char *)"extension");
			char *extension;
			switch (ext_propList) {

			case TRUE:
				extension = PROP_EXTENSION;
				break;
			case FALSE:
				extension = PROP_STANDARD;
				break;
			case MIXED:
				extension = PROP_MIXED;
				break;
			default:
				extension = PROP_DNA;
			}
			if (type && strcmp(type, extension) == 0) {
				proplist = tempNode;
				break;
			}
		}
	}
	if (proplist == NULL) {
		if (beginPropertyList(parent, ro_propList, ext_propList) == 0)
			return (addProperty(
			    parent,
			    name,
			    value,
			    type,
			    readonly,
			    ro_propList,
			    ext_propList));
		else
			return (CL_EINTERNAL);
	}

	// Add property to property list
	if (!xmlAddChild(proplist, prop)) {
		errorAddingElement("property", "propertyList");
		return (CL_EINTERNAL);
	} else
		return (CL_NOERR);
}

//
// Adds a property to the given parent node.  Directly calls the
// addProperty() function above using the name and value found in nv.
//
//	parent:		The parent xmlNode
//	nv:		The name-value of the property
//	readonly:	Is this a readonly property?
//	ro_propList:	Is the propertyList readonly?
//	ext_propList:	The extenstion type of the propertyList
//
// Returns CL_EINTERNAL on any error, CL_NOERR otherwise.
//
int
ClcmdExport::addProperty(
    xmlNode *parent,
    NameValue *nv,
    char *type,
    bool readonly,
    bool ro_propList,
    extension_t ext_propList)
{
	CHECKVALUE(nv);

	return (addProperty(
	    parent,
	    nv->getname(),
	    nv->getvalue(),
	    type,
	    readonly,
	    ro_propList,
	    ext_propList));
}

//
// Adds a property to the given parent node.  Directly calls the
// addProperty() function above using the name and value found in
// the NameValueList for each item in the list.
//
//	parent:		The parent xmlNode
//	nvl:		A list of property name-values
//	readonly:	Is this a readonly property?
//	ro_propList:	Is the propertyList readonly?
//	ext_propList:	The extenstion type of the propertyList
//
// Returns CL_EINTERNAL on any error, CL_NOERR otherwise.
//
int
ClcmdExport::addProperties(
    xmlNode *parent,
    NameValueList *nvl,
    char *type,
    bool readonly,
    bool ro_propList,
    extension_t ext_propList)
{
	CHECKVALUE(nvl);

	for (nvl->start(); !nvl->end(); nvl->next()) {
		int err = addProperty(
		    parent,
		    nvl->getName(),
		    nvl->getValue(),
		    type,
		    readonly,
		    ro_propList,
		    ext_propList);
		if (err != 0)
			return (err);
	}

	return (CL_NOERR);
}

//
// Returns the root xmlNode for the class.
// Caller is responsible for freeing the xmlNode memory
//
xmlNode *
ClcmdExport::getRootNode()
{
	return (root);
}

//
// DEBUG only: Prints the XML data to stdout.
//
void
ClcmdExport::dumpXML()
{
	xmlDoc *doc = xmlNewDoc(BAD_CAST "1.0");
	xmlDocSetRootElement(doc, root);

	xmlChar *xmlbuff;
	int buffersize;
	xmlDocDumpFormatMemory(doc, &xmlbuff, &buffersize, 1);
	printf((char *)xmlbuff);

	xmlFree(xmlbuff);
	xmlFree(doc);
}

//
// Generic method to print a consistent error message when adding an
// xmlNode child fails.
//
void
ClcmdExport::errorAddingElement(const char *child, const char *parent)
{
	clerror("Error while adding element %s to element %s\n", child, parent);
}
