//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// ident	"@(#)ClsnmpmibExport.cc 1.3	08/05/20 SMI"
//

#include "ClsnmpmibExport.h"

#define	MIB_PROTO_V2	(char *)"SNMPv2"
#define	MIB_PROTO_V3	(char *)"SNMPv3"

//
// Creates the snmpmibList element and sets it as the root element.
//
ClsnmpmibExport::ClsnmpmibExport()
{
	root = xmlNewNode(NULL, (xmlChar *)"snmpmibList");
}

//
// Creates the snmpmib element and adds it as a child of the
// snmpmibList element.
//
//	name:	The MIB name
//	node:	The node on which the MIB is configured
//	proto:	The protocol type (SNMPv2 or SNMPv3)
//
// Returns CL_EINTERNAL on any error, CL_NOERR otherwise.
//
int
ClsnmpmibExport::createSNMPMib(char *name, char *node, protocol_t proto)
{
	CHECKVALUE(name);
	CHECKVALUE(node);

	xmlNode *snmpmib = newXmlNode("snmpmib");

	// add the attributes
	newXmlAttr(snmpmib, "nodeRef", node);
	newXmlAttr(snmpmib, "name", name);
	if (proto == SNMPv2)
		newXmlAttr(snmpmib, "protocol", MIB_PROTO_V2);
	else
		newXmlAttr(snmpmib, "protocol", MIB_PROTO_V3);

	if (!xmlAddChild(root, snmpmib)) {
		errorAddingElement("snmpmib", "snmpmibList");
		return (CL_EINTERNAL);
	} else
		return (CL_NOERR);
}
