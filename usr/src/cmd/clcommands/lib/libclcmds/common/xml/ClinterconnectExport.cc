//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// ident	"@(#)ClinterconnectExport.cc 1.2	08/05/20 SMI"
//

#include "ClinterconnectExport.h"

#define	TRANSPORT_STATE_ENABLED		(char *)"enabled"
#define	TRANSPORT_STATE_DISABLED	(char *)"disabled"
#define	ADAP_TYPE_DLPI			(char *)"dlpi"
#define	ADAP_TYPE_RSM			(char *)"rsm"
#define	ENDP_TYPE_ADAP			(char *)"adapter"
#define	ENDP_TYPE_SWITCH		(char *)"switch"

//
// Creates the transportNodeList, transportSwitchList and transportCableList
// elements, and adds them to the root element, clusterTransport
//
ClinterconnectExport::ClinterconnectExport()
{
	root = xmlNewNode(NULL, (xmlChar *)"clusterTransport");

	xmlNode *tnl = newXmlNode("transportNodeList");
	xmlNode *tsl = newXmlNode("transportSwitchList");
	xmlNode *tcl = newXmlNode("transportCableList");

	if (!xmlAddChild(root, tnl) ||
	    !xmlAddChild(root, tsl) ||
	    !xmlAddChild(root, tcl)) {

		errorAddingElement("transport list(s)", "clusterTransport");
	}
}

//
// Creates a transportAdapter element and adds it to the transportAdapterList
// element.
//
//	name:	The name of the adapter
//	node:	The name of the node which the adapter is on
//	state:	The state of the adapter (ENABLED or DISABLED)
//	type:	The adapter type
//
// Returns CL_EINTERNAL on any error, CL_NOERR otherwise
//
int
ClinterconnectExport::addTransportAdapter(
    char *name,
    char *node,
    state_t state,
    adap_type_t type)
{
	CHECKVALUE(name);
	CHECKVALUE(node);

	// Get the node list element
	xmlNode *tnl = NULL;
	for (xmlNode *temp = root->children; temp; temp = temp->next) {
		if (strcmp((char *)temp->name,
		    (char *)"transportNodeList") == 0) {
			tnl = temp;
			break;
		}
	}

	if (tnl == NULL)
		return (CL_EINTERNAL);

	// Try to get the specific node
	xmlNode *tn = NULL;
	for (xmlNode *temp = tnl->children; temp; temp = temp->next) {
		if (strcmp((char *)temp->name,
		    (char *)"transportNode") == 0) {

			char *nodeName = (char *)xmlGetProp(
			    temp,
			    (unsigned char *)"nodeRef");
			if (strcmp(nodeName, node) == 0) {
				tn = temp;
				break;
			}
			xmlFree(nodeName);
		}
	}

	// If not found, then create one and add it to the list
	if (tn == NULL) {
		tn = newXmlNode("transportNode");
		newXmlAttr(tn, "nodeRef", node);
		if (!xmlAddChild(tnl, tn)) {
			errorAddingElement("transportNode",
			    "transportNodeList");
			return (CL_EINTERNAL);
		}
	}

	xmlNode *ta = newXmlNode("transportAdapter");
	newXmlAttr(ta, "name", name);

	xmlNode *stateNode = newXmlNode("state");
	if (state == ENABLED)
		newXmlAttr(stateNode, "value", TRANSPORT_STATE_ENABLED);
	else
		newXmlAttr(stateNode, "value", TRANSPORT_STATE_DISABLED);
	if (!xmlAddChild(ta, stateNode)) {
		errorAddingElement("state", "transportAdapter");
		return (CL_EINTERNAL);
	}

	xmlNode *tt = newXmlNode("transportType");
	if (type == DLPI)
		newXmlAttr(tt, "value", ADAP_TYPE_DLPI);
	else
		newXmlAttr(tt, "value", ADAP_TYPE_RSM);
	if (!xmlAddChild(ta, tt)) {
		errorAddingElement("transportType", "transportAdapter");
		return (CL_EINTERNAL);
	}

	if (!xmlAddChild(tn, ta)) {
		errorAddingElement("transportAdapter", "transportNode");
		return (CL_EINTERNAL);
	} else
		return (CL_NOERR);
}

//
// Adds a property element as a child of an adapter:node element
// The adapter:node element must already exist.
//
//	nodeName:	The name of the node the adapter is on
//	adapterName:	The name of the adapter
//	name:		The property name
//	value:		The property value
//	readonly:	Is this property readonly?
//
// Returns CL_EINTERNAL on any error, CL_NOERR otherwise.
//
int
ClinterconnectExport::addTransportAdapterProperty(
    char *nodeName,
    char *adapterName,
    char *name,
    char *value,
    bool readonly)
{
	return (ClcmdExport::addProperty(
	    getAdapter(nodeName, adapterName),
	    name,
	    value,
	    NULL, // type
	    readonly));
}

//
// Adds a property element as a child of an adapter:node element
// The adapter:node element must already exist.
//
//	nodeName:	The name of the node the adapter is on
//	adapterName:	The name of the adapter
//	nv:		The property name-value
//	readonly:	Is this property readonly?
//
// Returns CL_EINTERNAL on any error, CL_NOERR otherwise.
//
int
ClinterconnectExport::addTransportAdapterProperty(
    char *nodeName,
    char *adapterName,
    NameValue *nv,
    bool readonly)
{
	return (ClcmdExport::addProperty(
	    getAdapter(nodeName, adapterName),
	    nv,
	    NULL, // type
	    readonly));
}

//
// Adds property elements as a children of an adapter:node element
// The adapter:node element must already exist.
//
//	nodeName:	The name of the node the adapter is on
//	adapterName:	The name of the adapter
//	nvl:		The list of property name-values
//	readonly:	Is this property readonly?
//
// Returns CL_EINTERNAL on any error, CL_NOERR otherwise.
//
int
ClinterconnectExport::addTransportAdapterProperties(
    char *nodeName,
    char *adapterName,
    NameValueList *nvl,
    bool readonly)
{
	return (ClcmdExport::addProperties(
	    getAdapter(nodeName, adapterName),
	    nvl,
	    NULL, // type
	    readonly));
}

//
// Adds a property element as a child of an adapter:node transport
// type element.  The adapter:node transport type element must
// already exist.
//
//	nodeName:	The name of the node the adapter is on
//	adapterName:	The name of the adapter
//	name:		The property name
//	value:		The property value
//	readonly:	Is this property readonly?
//
// Returns CL_EINTERNAL on any error, CL_NOERR otherwise.
//
int
ClinterconnectExport::addTransportTypeProperty(
    char *nodeName,
    char *adapterName,
    char *name,
    char *value,
    bool readonly)
{
	return (ClcmdExport::addProperty(
	    getTransportType(nodeName, adapterName),
	    name,
	    value,
	    NULL, // type
	    readonly));
}

//
// Adds a property element as a child of an adapter:node transport
// type element.  The adapter:node transport type element must
// already exist.
//
//	nodeName:	The name of the node the adapter is on
//	adapterName:	The name of the adapter
//	nv:		The property name-value
//	readonly:	Is this property readonly?
//
// Returns CL_EINTERNAL on any error, CL_NOERR otherwise.
//
int
ClinterconnectExport::addTransportTypeProperty(
    char *nodeName,
    char *adapterName,
    NameValue *nv,
    bool readonly)
{
	return (ClcmdExport::addProperty(
	    getTransportType(nodeName, adapterName),
	    nv,
	    NULL, // type
	    readonly));
}

//
// Adds property elements as children of an adapter:node transport
// type element.  The adapter:node transport type element must
// already exist.
//
//	nodeName:	The name of the node the adapter is on
//	adapterName:	The name of the adapter
//	nvl:		A list of property name-values
//	readonly:	Is this property readonly?
//
// Returns CL_EINTERNAL on any error, CL_NOERR otherwise.
//
int
ClinterconnectExport::addTransportTypeProperties(
    char *nodeName,
    char *adapterName,
    NameValueList *nvl,
    bool readonly)
{
	return (ClcmdExport::addProperties(
	    getTransportType(nodeName, adapterName),
	    nvl,
	    NULL, // type
	    readonly));
}

//
// Creates a transportSwitch element and adds it as a child of the
// transportSwitchList element.
//
//	name:	The name of the switch
//	state:	The state of the switch (ENABLED or DISABLED)
//	port:	The port on the switch (this is optional)
//
// Returns CL_EINTERNAL on any error, CL_NOERR otherwise.
//
int
ClinterconnectExport::addTransportSwitch(
    char *name,
    state_t state,
    char *port)
{
	CHECKVALUE(name);

	xmlNode *ts = newXmlNode("transportSwitch");

	// add the attributes
	newXmlAttr(ts, "name", name);
	if (port)
		newXmlAttr(ts, "port", port);

	// Get the switch list
	xmlNode *tsl = NULL;
	for (xmlNode *tempNode = root->children;
	    tempNode;
	    tempNode = tempNode->next) {

		if (strcmp((char *)tempNode->name,
		    (char *)"transportSwitchList") == 0) {
			tsl = tempNode;
			break;
		}
	}
	if (tsl == NULL) {
		clerror("Cannot find transportSwitchList element\n");
		return (CL_EINTERNAL);
	}

	xmlNode *stateNode = newXmlNode("state");
	if (state == ENABLED)
		newXmlAttr(stateNode, "value", TRANSPORT_STATE_ENABLED);
	else
		newXmlAttr(stateNode, "value", TRANSPORT_STATE_DISABLED);
	if (!xmlAddChild(ts, stateNode)) {
		errorAddingElement("state", "transportSwitch");
		return (CL_EINTERNAL);
	}

	if (!xmlAddChild(tsl, ts)) {
		errorAddingElement("transportSwitch", "transportSwitchList");
		return (CL_EINTERNAL);
	} else
		return (CL_NOERR);
}

//
// Creates a transportCable element as a child of the transportCableList
// element.
//
//	end_1:	The first transport endpoint
//	end_2:	The second transport endpoint
//	state:	The state of the cable (ENABLED or DISABLE)
//
// Returns CL_EINTERNAL on any error, CL_NOERR otherwise.
//
int
ClinterconnectExport::addTransportCable(
    endpoint_t end_1,
    endpoint_t end_2,
    state_t state)
{
	xmlNode *tc = newXmlNode("transportCable");

	// Get the cable list
	xmlNode *tcl = NULL;
	for (xmlNode *tempNode = root->children;
	    tempNode;
	    tempNode = tempNode->next) {

		if (strcmp((char *)tempNode->name,
		    (char *)"transportCableList") == 0) {
			tcl = tempNode;
			break;
		}
	}
	if (tcl == NULL) {
		clerror("Cannot find transportCableList element\n");
		return (CL_EINTERNAL);
	}

	// Create the state node
	xmlNode *stateNode = newXmlNode("state");
	if (state == ENABLED)
		newXmlAttr(stateNode, "value", TRANSPORT_STATE_ENABLED);
	else
		newXmlAttr(stateNode, "value", TRANSPORT_STATE_DISABLED);
	if (!xmlAddChild(tc, stateNode)) {
		errorAddingElement("state", "transportCable");
		return (CL_EINTERNAL);
	}

	// Create the endpoints
	if (!xmlAddChild(tc, createEndpointElement(end_1)) ||
	    !xmlAddChild(tc, createEndpointElement(end_2))) {
		errorAddingElement("endpoint", "transportCable");
		return (CL_EINTERNAL);
	}

	// Add the cable
	if (!xmlAddChild(tcl, tc)) {
		errorAddingElement("transportCable", "transportCableList");
		return (CL_EINTERNAL);
	} else
		return (CL_NOERR);
}

//
// Returns an endpoint element based off the information in the endpoint_t
// struct, end.  If the endpoint is not valid, NULL is returned.
//
//	end:	The endpoint
//
xmlNode *
ClinterconnectExport::createEndpointElement(endpoint_t end)
{
	if (!end.name)
		return (NULL);

	if (end.type == ADAPTER && end.node == NULL) {
		clerror("Endpoint adapters must have a node name\n");
		return (NULL);
	}
	xmlNode *endp = newXmlNode("endpoint");

	newXmlAttr(endp, "name", end.name);
	if (end.type == SWITCH)
		newXmlAttr(endp, "type", ENDP_TYPE_SWITCH);
	else
		newXmlAttr(endp, "type", ENDP_TYPE_ADAP);
	if (end.node)
		newXmlAttr(endp, "nodeRef", end.node);
	if (end.port)
		newXmlAttr(endp, "port", end.port);

	return (endp);
}

//
// Returns the adapter element referenced by node and adapter.
// If not found, then NULL is returned.
//
xmlNode *
ClinterconnectExport::getAdapter(char *node, char *adapter)
{
	xmlNode *adap = NULL;
	xmlNode *nodeElem = NULL;
	for (xmlNode *temp = root->children; temp; temp = temp->next) {
		if (strcmp((char *)temp->name,
		    (char *)"transportNodeList") == 0) {

			for (xmlNode *temp2 = temp->children;
			    temp2;
			    temp2 = temp2->next) {

				if (strcmp((char *)temp2->name,
				    (char *)"transportNode") == 0) {

					char *name = (char *)xmlGetProp(temp2,
					    (unsigned char *)"nodeRef");
					if (strcmp(name, node) == 0) {
						nodeElem = temp2;
						xmlFree(name);
						break;
					}
					xmlFree(name);
				}
			}
		}
	}
	if (nodeElem == NULL) {
		clerror("Cannot find transportNode element - %s\n", node);
		return (NULL);
	}

	for (xmlNode *temp = nodeElem->children; temp; temp = temp->next) {
		if (strcmp((char *)temp->name,
		    (char *)"transportAdapter") == 0) {

			char *name = (char *)xmlGetProp(temp,
			    (unsigned char *)"name");
			if (strcmp(name, adapter) == 0) {
				adap = temp;
				xmlFree(name);
				break;
			}
			xmlFree(name);
		}
	}
	if (adap == NULL) {
		clerror("Cannot find transportAdapter element - %s\n", adapter);
		return (NULL);
	} else
		return (adap);
}

//
// Returns the transportType element referenced by node and adapter.
// If not found, then NULL is returned.
//
xmlNode *
ClinterconnectExport::getTransportType(char *node, char *adapter)
{
	xmlNode *adap = getAdapter(node, adapter);
	if (adap == NULL)
		return (NULL);

	for (xmlNode *temp = adap->children; temp; temp = temp->next) {
		if (strcmp((char *)temp->name, (char *)"transportType") == 0) {
			return (temp);
		}
	}

	clerror("Cannot find transportType element\n");
	return (NULL);
}
