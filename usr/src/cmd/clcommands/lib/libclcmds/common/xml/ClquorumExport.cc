//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// ident	"@(#)ClquorumExport.cc 1.3	09/01/16 SMI"
//

#include "ClquorumExport.h"

#define	QD_PATHLIST_READONLY	(char *)"true"
#define	QD_STATE_ENABLED	(char *)"enabled"
#define	QD_STATE_DISABLED	(char *)"disabled"

#define	TRUE_STRING	(char *)"true"

//
// Creates the clusterQuorum element and sets it as the root node.
//
ClquorumExport::ClquorumExport()
{
	root = xmlNewNode(NULL, (xmlChar *)"clusterQuorum");
}

//
// Creates a quorumNode element and adds it to the quorumNodeList
// element.  If the quorumNodeList element does not exist, then it
// is created as a child of the clusterQuorum element.
//
//	nodeName:	The node name for the quorumNode
//
// Returns CL_EINTERNAL on any error, CL_NOERR otherwise.
//
int
ClquorumExport::addQuorumNode(char *nodeName)
{
	CHECKVALUE(nodeName);

	xmlNode *qn = newXmlNode("quorumNode");

	// add the attributes
	newXmlAttr(qn, "nodeRef", nodeName);

	// Get the quorumNodeList
	xmlNode *qnList = NULL;
	for (xmlNode *tempNode = root->children;
	    tempNode;
	    tempNode = tempNode->next) {

		if (strcmp((char *)tempNode->name,
		    (char *)"quorumNodeList") == 0) {
			qnList = tempNode;
			break;
		}
	}
	if (qnList == NULL) {
		// Create the quorumNodeList element
		qnList = newXmlNode("quorumNodeList");
		newXmlAttr(qnList, "readonly", TRUE_STRING);
		if (!xmlAddChild(root, qnList)) {
			errorAddingElement("quorumNodeList", "clusterQuorum");
			return (CL_EINTERNAL);
		}
	}

	if (!xmlAddChild(qnList, qn)) {
		errorAddingElement("quorumNode", "quorumNodeList");
		return (CL_EINTERNAL);
	} else
		return (CL_NOERR);
}

//
// Creates a property element as a child of the quorumNode element
// referenced by nodeName.  The quorumNode element must already exist.
//
//	nodeName:	The quorum node name
//	name:		The property name
//	value:		The property value
//	readonly:	Is this property readonly?
//
// Returns CL_EINTERNAL on any error, CL_NOERR otherwise.
//
int
ClquorumExport::addQuorumNodeProperty(
    char *nodeName,
    char *name,
    char *value,
    bool readonly)
{
	return (ClcmdExport::addProperty(
	    getQuorumNode(nodeName),
	    name,
	    value,
	    NULL, // type
	    readonly));
}

//
// Creates a property element as a child of the quorumNode element
// referenced by nodeName.  The quorumNode element must already exist.
//
//	nodeName:	The quorum node name
//	nv:		The property name-value
//	readonly:	Is this property readonly?
//
// Returns CL_EINTERNAL on any error, CL_NOERR otherwise.
//
int
ClquorumExport::addQuorumNodeProperty(
    char *nodeName,
    NameValue *nv,
    bool readonly)
{
	return (ClcmdExport::addProperty(
	    getQuorumNode(nodeName),
	    nv,
	    NULL, // type
	    readonly));
}

//
// Creates property elements as children of the quorumNode element
// referenced by nodeName.  The quorumNode element must already exist.
//
//	nodeName:	The quorum node name
//	nvl:		List of property name-values
//	readonly:	Is this property readonly?
//
// Returns CL_EINTERNAL on any error, CL_NOERR otherwise.
//
int
ClquorumExport::addQuorumNodeProperties(
    char *nodeName,
    NameValueList *nvl,
    bool readonly)
{
	return (ClcmdExport::addProperties(
	    getQuorumNode(nodeName),
	    nvl,
	    NULL, // type
	    readonly));
}

//
// Creates a property element as a child of the quorumDevice element
// referenced by deviceName. The quorumDevice element must already exist.
//
//	deviceName:	The quorum device name
//	name:		The property name
//	value:		The property value
//	readonly:	Is this property readonly?
//
// Returns CL_EINTERNAL on any error, CL_NOERR otherwise.
//
int
ClquorumExport::addQuorumDeviceProperty(
    char *deviceName,
    char *name,
    char *value,
    bool readonly)
{
	return (ClcmdExport::addProperty(
	    getQuorumDevice(deviceName),
	    name,
	    value,
	    NULL, // type
	    readonly));
}

//
// Creates a property element as a child of the quorumDevice element
// referenced by deviceName. The quorumDevice element must already exist.
//
//	deviceName:	The quorum node name
//	name:		The property name
//	value:		The property value
//	readonly:	Is this property readonly?
//
// Returns CL_EINTERNAL on any error, CL_NOERR otherwise.
//
int
ClquorumExport::addQuorumDeviceProperty(
    char *deviceName,
    NameValue *nv,
    bool readonly)
{
	return (ClcmdExport::addProperty(
	    getQuorumDevice(deviceName),
	    nv,
	    NULL, // type
	    readonly));
}

//
// Creates property elements as children of the quorumDevice element
// referenced by deviceName. The quorumDevice element must already exist.
//
//	deviceName:	The quorum device name
//	nvl:		List of property name-values
//	readonly:	Is this property readonly?
//
// Returns CL_EINTERNAL on any error, CL_NOERR otherwise.
//
int
ClquorumExport::addQuorumDeviceProperties(
    char *deviceName,
    NameValueList *nvl,
    bool readonly)
{
	return (ClcmdExport::addProperties(
	    getQuorumDevice(deviceName),
	    nvl,
	    NULL, // type
	    readonly));
}

//
// Creates a quorumDevice element and adds it to the quorumDeviceList
// element.  If the quorumDeviceList element does not exist, then it
// is created as a child of the clusterQuorum element.
//
//	name:	The device name for the quorumDevice
//	type:	The quorum device type
//
// Returns CL_EINTERNAL on any error, CL_NOERR otherwise.
//
int
ClquorumExport::addQuorumDevice(char *name, char *type)
{
	CHECKVALUE(name);

	xmlNode *qd = newXmlNode("quorumDevice");

	// add the attributes
	newXmlAttr(qd, "name", name);
	newXmlAttr(qd, "type", type);

	// Get the quorumDeviceList
	xmlNode *qdList = NULL;
	for (xmlNode *tempNode = root->children;
	    tempNode;
	    tempNode = tempNode->next) {

		if (strcmp((char *)tempNode->name,
		    (char *)"quorumDeviceList") == 0) {
			qdList = tempNode;
			break;
		}
	}
	if (qdList == NULL) {
		// Create the quorumDeviceList element

		qdList = newXmlNode("quorumDeviceList");
		if (!xmlAddChild(root, qdList)) {
			errorAddingElement("quorumDeviceList", "clusterQuorum");
			return (CL_EINTERNAL);
		}
	}

	if (!xmlAddChild(qdList, qd)) {
		errorAddingElement("quorumDevice", "quorumDeviceList");
		return (CL_EINTERNAL);
	} else
		return (CL_NOERR);
}

//
// Creates a quorumDevicePath element as a child of the quorumDeviceList
// element.  If the quorumDeviceList element does not exist, it is
// created as a child of the clusterQuorum element.
//
//	quorumDeviceName:	The quorum device name
//	nodeName:		The name of the node
//	state:			The state of the quorum device
//
// Returns CL_EINTERNAL on any error, CL_NOERR otherwise.
//
int
ClquorumExport::addQuorumDevicePath(
    char *quorumDeviceName,
    char *nodeName,
    state_t state)
{
	CHECKVALUE(quorumDeviceName);
	CHECKVALUE(nodeName);

	// Get the quorumDevice element
	xmlNode *qd = NULL;
	xmlNode *temp = NULL;
	for (temp = root->children; temp; temp = temp->next) {
		if (strcmp((char *)temp->name, (char *)"quorumDeviceList") == 0)
			break;
	}
	if (temp == NULL) {
		clerror("No quorum devices exist in configuration\n");
		return (CL_EINTERNAL);
	}

	for (temp = temp->children; temp; temp = temp->next) {
		if (strcmp((char *)temp->name, (char *)"quorumDevice") == 0) {
			char *name = (char *)xmlGetProp(temp,
			    (unsigned char *)"name");
			if (strcmp(name, quorumDeviceName) == 0) {
				xmlFree(name);
				qd = temp;
				break;
			}
			xmlFree(name);
		}
	}
	if (qd == NULL) {
		clerror("No such quorumDevice exists in configuration - %s\n",
		    quorumDeviceName);
		return (CL_EINTERNAL);
	}

	// Make sure the qdPathList exists
	xmlNode *qdpList = NULL;
	for (temp = qd->children; temp; temp = temp->next) {
		if (strcmp((char *)temp->name,
		    (char *)"quorumDevicePathList") == 0) {
			qdpList = temp;
			break;
		}
	}
	if (qdpList == NULL) {
		// Create the quorumDevicePathList element

		qdpList = newXmlNode("quorumDevicePathList");
		newXmlAttr(qdpList, "readonly", QD_PATHLIST_READONLY);
		if (!xmlAddChild(qd, qdpList)) {
			errorAddingElement("quorumDevicePathList",
			    "quorumDevice");
			return (CL_EINTERNAL);
		}
	}

	// Create the quorumDevicePath element
	xmlNode *qdp = newXmlNode("quorumDevicePath");
	newXmlAttr(qdp, "nodeRef", nodeName);
	xmlNode *stateNode = newXmlNode("state");
	if (state == ENABLED)
		newXmlAttr(stateNode, "value", QD_STATE_ENABLED);
	else
		newXmlAttr(stateNode, "value", QD_STATE_DISABLED);
	if (!xmlAddChild(qdp, stateNode)) {
		errorAddingElement("state", "quorumDevicePath");
		return (CL_EINTERNAL);
	}

	if (!xmlAddChild(qdpList, qdp)) {
		errorAddingElement("quorumDevicePath", "quorumDevicePathList");
		return (CL_EINTERNAL);
	} else
		return (CL_NOERR);
}

//
// Returns the quorumNode element referenced by nodeName, or NULL
// if the element cannot be found.
//
xmlNode *
ClquorumExport::getQuorumNode(char *nodeName)
{
	if (!nodeName)
		return (NULL);

	xmlNode *node = NULL;
	xmlNode *temp = NULL;
	for (temp = root->children; temp; temp = temp->next) {
		if (strcmp((char *)temp->name, (char *)"quorumNodeList") == 0)
			break;
	}
	if (temp == NULL)
		return (NULL);

	for (temp = temp->children; temp; temp = temp->next) {
		if (strcmp((char *)temp->name, (char *)"quorumNode") == 0) {
			char *name = (char *)xmlGetProp(temp,
			    (unsigned char *)"nodeRef");
			if (strcmp(name, nodeName) == 0) {
				xmlFree(name);
				node = temp;
				break;
			}
			xmlFree(name);
		}
	}

	return (node);
}

//
// Returns the quorumDevice element referenced by deviceName, or NULL
// if the element cannot be found.
//
xmlNode *
ClquorumExport::getQuorumDevice(char *deviceName)
{
	if (!deviceName)
		return (NULL);

	xmlNode *device = NULL;
	xmlNode *temp = NULL;
	for (temp = root->children; temp; temp = temp->next) {
		if (strcmp((char *)temp->name, (char *)"quorumDeviceList") == 0)
			break;
	}
	if (temp == NULL)
		return (NULL);

	for (temp = temp->children; temp; temp = temp->next) {
		if (strcmp((char *)temp->name, (char *)"quorumDevice") == 0) {
			char *name = (char *)xmlGetProp(temp,
			    (unsigned char *)"name");
			if (strcmp(name, deviceName) == 0) {
				xmlFree(name);
				device = temp;
				break;
			}
			xmlFree(name);
		}
	}

	return (device);
}

#ifdef WEAK_MEMBERSHIP
//
// Creates a globalQuorum element and adds it to the globalQuorumList
// element.  If the globalQuorumList element does not exist, then it
// is created as a child of the clusterQuorum element.
//
//	name:	The device name for the quorumDevice
//	type:	The quorum device type
//
// Returns CL_EINTERNAL on any error, CL_NOERR otherwise.
//
int
ClquorumExport::addGlobalQuorum(char *name, char *type)
{
	CHECKVALUE(name);

	xmlNode *qd = newXmlNode("globalQuorum");

	// add the attributes
	newXmlAttr(qd, "name", name);
	newXmlAttr(qd, "type", type);

	// Get the quorumDeviceList
	xmlNode *qdList = NULL;
	for (xmlNode *tempNode = root->children;
	    tempNode;
	    tempNode = tempNode->next) {

		if (strcmp((char *)tempNode->name,
		    (char *)"globalQuorumList") == 0) {
			qdList = tempNode;
			break;
		}
	}
	if (qdList == NULL) {
		// Create the quorumDeviceList element

		qdList = newXmlNode("globalQuorumList");
		if (!xmlAddChild(root, qdList)) {
			errorAddingElement("globalQuorumList", "clusterQuorum");
			return (CL_EINTERNAL);
		}
	}

	if (!xmlAddChild(qdList, qd)) {
		errorAddingElement("globalQuorum", "globalQuorumList");
		return (CL_EINTERNAL);
	} else
		return (CL_NOERR);
}

//
// Creates a property element as a child of the globalQuorum element
// referenced by deviceName. The globalQuorum element must already exist.
//
//	deviceName:	The quorum device name
//	name:		The property name
//	value:		The property value
//	readonly:	Is this property readonly?
//
// Returns CL_EINTERNAL on any error, CL_NOERR otherwise.
//
int
ClquorumExport::addGlobalQuorumProperty(
    char *deviceName,
    char *name,
    char *value,
    bool readonly)
{
	return (ClcmdExport::addProperty(
	    getGlobalQuorum(deviceName),
	    name,
	    value,
	    NULL, // type
	    readonly));
}

//
// Creates a property element as a child of the globalQuorum element
// referenced by deviceName. The globalQuorum element must already exist.
//
//	deviceName:	The quorum node name
//	name:		The property name
//	value:		The property value
//	readonly:	Is this property readonly?
//
// Returns CL_EINTERNAL on any error, CL_NOERR otherwise.
//
int
ClquorumExport::addGlobalQuorumProperty(
    char *deviceName,
    NameValue *nv,
    bool readonly)
{
	return (ClcmdExport::addProperty(
	    getGlobalQuorum(deviceName),
	    nv,
	    NULL, // type
	    readonly));
}

//
// Creates property elements as children of the globalQuorum element
// referenced by deviceName. The globalQuorum element must already exist.
//
//	deviceName:	The quorum device name
//	nvl:		List of property name-values
//	readonly:	Is this property readonly?
//
// Returns CL_EINTERNAL on any error, CL_NOERR otherwise.
//
int
ClquorumExport::addGlobalQuorumProperties(
    char *deviceName,
    NameValueList *nvl,
    bool readonly)
{
	return (ClcmdExport::addProperties(
	    getGlobalQuorum(deviceName),
	    nvl,
	    NULL, // type
	    readonly));
}

//
// Returns the globalQuorum element referenced by deviceName, or NULL
// if the element cannot be found.
//
xmlNode *
ClquorumExport::getGlobalQuorum(char *deviceName)
{
	if (!deviceName)
		return (NULL);

	xmlNode *device = NULL;
	xmlNode *temp = NULL;
	for (temp = root->children; temp; temp = temp->next) {
		if (strcmp((char *)temp->name, (char *)"globalQuorumList") == 0)
			break;
	}
	if (temp == NULL)
		return (NULL);

	for (temp = temp->children; temp; temp = temp->next) {
		if (strcmp((char *)temp->name, (char *)"globalQuorum") == 0) {
			char *name = (char *)xmlGetProp(temp,
			    (unsigned char *)"name");
			if (strcmp(name, deviceName) == 0) {
				xmlFree(name);
				device = temp;
				break;
			}
			xmlFree(name);
		}
	}

	return (device);
}
#endif
