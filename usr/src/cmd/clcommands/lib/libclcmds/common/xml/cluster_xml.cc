//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// ident	"@(#)cluster_xml.cc	1.3	08/05/20 SMI"
//

#include "ClXml.h"

//
// Returns a ValueList of cluster xpath statements.
//
ValueList *
ClXml::clusterFilter(OptionValues *opts)
{
	ValueList *xpath = new ValueList();

	// The number of xpath statements is always one.
	xpath->add((char *)"//cluster");

	return (xpath);
}

//
// Processes specific aspects of the cluster information contained in the
// XML file.  These aspects are:  cluster nodes, transports, devices, and
// optionally the quorum.
//
int
ClXml::clusterProcessor(xmlNode *node, OptionValues *opts)
{
	// Get the cluster name
	char *cluster_name = opts->getOperand();
	if (cluster_name == NULL) {
		char *temp = (char *)xmlGetProp(node,
		    (unsigned char *)"name");
		cluster_name = strdup(temp);
		xmlFree(temp);
	}

	// Get the cluster props
	xmlNode *proplist = NULL;
	for (proplist = node->children; proplist; proplist = proplist->next) {
		if (strcmp((char *)proplist->name, "propertyList") == 0)
			break;
	}

	OptionValues *new_opts = new OptionValues((char *)"create");
	// Call generic property processing routine
	genericPropertyOverrides(
	    proplist,
	    opts->getNVOptions(CLUSTER_PROP),
	    CLUSTER_PROP,
	    new_opts);

	new_opts->setOperand(cluster_name);
	new_opts->dumpData();

	// Create generic OptionValues object
	OptionValues *temp_opts = new OptionValues((char *)"add");
	temp_opts->setOperand(CLCOMMANDS_WILD_OPERAND);

	// Do the cluster nodes
	error = applyConfig(CLNODE_CMD, NULL, temp_opts);

	// Do the transport
	temp_opts->setSubcommand("create");
	temp_opts->setOperand(CLCOMMANDS_WILD_OPERAND);
	error = applyConfig(CLINTERCONNECT_CMD, NULL, temp_opts);

	// Do the devices
	temp_opts->setOperand(CLCOMMANDS_WILD_OPERAND);
	temp_opts->setSubcommand("monitor");
	error = applyConfig(CLDEVICE_CMD, NULL, temp_opts);
	temp_opts->setOperand(CLCOMMANDS_WILD_OPERAND);
	temp_opts->setSubcommand("unmonitor");
	error = applyConfig(CLDEVICE_CMD, NULL, temp_opts);

	// Do the quorum (if flag set)
	temp_opts->setOperand(CLCOMMANDS_WILD_OPERAND);
	if (opts->getOption(CLUSTER_AUTOQUORUM) != NULL) {
		temp_opts->setOperand(CLCOMMANDS_WILD_OPERAND);
		temp_opts->setSubcommand("add");
		error = applyConfig(CLQUORUM_CMD, NULL, temp_opts);
	}

	return (error);
}
