//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
//
#pragma ident	"@(#)CldevicegroupExport.cc	1.3	08/05/20 SMI"


#include "CldevicegroupExport.h"

#define	DG_TYPE_SVM		(char *)"sds"
#define	DG_TYPE_RAWDISK		(char *)"rawdisk"
#define	DG_TYPE_MULTI_SVM	(char *)"local"

//
// Creates the devicegroupList element as the root node
//
CldevicegroupExport::CldevicegroupExport()
{
	root = xmlNewNode(NULL, (xmlChar *)"devicegroupList");
}

//
// Creates a devicegroup element with the specified name and type
//
//	name:	The name of the devicegroup
//	type:	The type of the devicegroup
//
// Returns CL_EINTERNAL on any error, CL_NOERR otherwise.
//
int
CldevicegroupExport::createDevicegroup(char *name, dg_type_t type)
{
	CHECKVALUE(name);

	xmlNode *devicegroup = newXmlNode("devicegroup");

	// add the attributes
	newXmlAttr(devicegroup, "name", name);
	if (type == SVM)
		newXmlAttr(devicegroup, "type", DG_TYPE_SVM);
	else
		newXmlAttr(devicegroup, "type", DG_TYPE_RAWDISK);

	if (!xmlAddChild(root, devicegroup)) {
		errorAddingElement("devicegroup", "devicegroupList");
		return (CL_EINTERNAL);
	} else
		return (CL_NOERR);
}

//
// Creates a devicegroupNode element as a child of the devicegroup element
// referenced by devicegroupName.  The devicegroup element must have been
// created previously.
//
//	devicegroupName:	The name of the devicegroup
//	node:			The name of the node
//
// Returns CL_EINTERNAL on any error, CL_NOERR otherwise.
//
int
CldevicegroupExport::addDevicegroupNode(char *devicegroupName, char *node)
{
	CHECKVALUE(devicegroupName);
	CHECKVALUE(node);

	// Get the devicegroup element
	xmlNode *devicegroup = NULL;
	for (xmlNode *temp = root->children; temp; temp = temp->next) {
		if (strcmp((char *)temp->name, (char *)"devicegroup") == 0) {
			char *name = (char *)xmlGetProp(temp,
			    (unsigned char *)"name");
			if (strcmp(name, devicegroupName) == 0) {
				xmlFree(name);
				devicegroup = temp;
				break;
			}
			xmlFree(name);
		}
	}

	if (devicegroup == NULL) {
		clerror("No such devicegroup exists in the configuration "
		    " - %s\n", devicegroupName);
		return (CL_EINTERNAL);
	}

	// Make sure the devicegroupNodeList exists
	xmlNode *dgnList = NULL;
	for (xmlNode *temp = devicegroup->children; temp; temp = temp->next) {
		if (strcmp((char *)temp->name,
		    (char *)"devicegroupNodeList") == 0) {
			dgnList = temp;
			break;
		}
	}

	// If it doesn't exist, create it now
	if (dgnList == NULL) {
		dgnList = newXmlNode("devicegroupNodeList");
		if (!xmlAddChild(devicegroup, dgnList)) {
			errorAddingElement("devicegroupNodeList",
			    "devicegroup");
			return (CL_EINTERNAL);
		}
	}

	// Create the devicegroup node element
	xmlNode *devicegroupNode = newXmlNode("devicegroupNode");
	newXmlAttr(devicegroupNode, "nodeRef", node);

	if (!xmlAddChild(dgnList, devicegroupNode)) {
		errorAddingElement("devicegroupNode", "devicegroupNodeList");
		return (CL_EINTERNAL);
	} else
		return (CL_NOERR);
}

//
// Creates a member element as a child of the devicegroup element
// referenced by devicegroupName.  The devicegroup element must have
// already been created.
//
//	devicegroupName:	The name of the devicegroup
//	memberName:		The name of the member
//
// Returns CL_EINTERNAL on any error, CL_NOERR otherwise.
//
int
CldevicegroupExport::addMember(char *devicegroupName, char *memberName)
{
	CHECKVALUE(devicegroupName);
	CHECKVALUE(memberName);

	// Get the devicegroup element
	xmlNode *devicegroup = NULL;
	for (xmlNode *temp = root->children; temp; temp = temp->next) {
		if (strcmp((char *)temp->name, (char *)"devicegroup") == 0) {
			char *name = (char *)xmlGetProp(temp,
			    (unsigned char *)"name");
			if (strcmp(name, devicegroupName) == 0) {
				xmlFree(name);
				devicegroup = temp;
				break;
			}
			xmlFree(name);
		}
	}

	if (devicegroup == NULL) {
		clerror("No such devicegroup exists in the configuration "
		    " - %s\n", devicegroupName);
		return (CL_EINTERNAL);
	}

	// Make sure the memberList exists
	xmlNode *memList = NULL;
	for (xmlNode *temp = devicegroup->children; temp; temp = temp->next) {
		if (strcmp((char *)temp->name,
		    (char *)"memberDeviceList") == 0) {
			memList = temp;
			break;
		}
	}

	// If it doesn't exist, create it now
	if (memList == NULL) {
		memList = newXmlNode("memberDeviceList");
		if (!xmlAddChild(devicegroup, memList)) {
			errorAddingElement("memberDeviceList", "devicegroup");
			return (CL_EINTERNAL);
		}
	}

	// Create the member element
	xmlNode *member = newXmlNode("memberDevice");
	newXmlAttr(member, "name", memberName);

	if (!xmlAddChild(memList, member)) {
		errorAddingElement("memberDevice", "memberDeviceList");
		return (CL_EINTERNAL);
	} else
		return (CL_NOERR);
}

//
// Creates a property element as a child of the devicegroup element
// referenced by devicegroupName.  The devicegroup must have been
// already created.
//
//	devicegroupName:	The name of the devivegroup
//	name:			The property name
//	value:			The property value
//	readonly:		Is this property readonly?
//
// Returns CL_EINVALID on any error, CL_NOERR otherwise.
//
int
CldevicegroupExport::addProperty(
    char *devicegroupName,
    char *name,
    char *value,
    bool readonly)
{
	return (ClcmdExport::addProperty(
	    getDevicegroup(devicegroupName),
	    name,
	    value,
	    NULL, // type
	    readonly));
}

//
// Creates a property element as a child of the devicegroup element
// referenced by devicegroupName.  The devicegroup must have been
// already created.
//
//	devicegroupName:	The name of the devivegroup
//	nv:			The property name and value
//	readonly:		Is this property readonly?
//
// Returns CL_EINVALID on any error, CL_NOERR otherwise.
//
int
CldevicegroupExport::addProperty(
    char *devicegroupName,
    NameValue *nv,
    bool readonly)
{
	return (ClcmdExport::addProperty(
	    getDevicegroup(devicegroupName),
	    nv,
	    NULL, // type
	    readonly));
}

//
// Creates property elements as children of the devicegroup element
// referenced by devicegroupName.  The devicegroup must have been
// already created.
//
//	devicegroupName:	The name of the devivegroup
//	nvl:			The properties list
//	readonly:		Are these properties readonly?
//
// Returns CL_EINVALID on any error, CL_NOERR otherwise.
//
int
CldevicegroupExport::addProperties(
    char *devicegroupName,
    NameValueList *nvl,
    bool readonly)
{
	return (ClcmdExport::addProperties(
	    getDevicegroup(devicegroupName),
	    nvl,
	    NULL, // type
	    readonly));
}

//
// Returns the devicegroup xmlNode referenced by name dgname.  If the element
// does not exist, then NULL is returned.
//
xmlNode *
CldevicegroupExport::getDevicegroup(char *dgname)
{
	if (!dgname)
		return (NULL);

	xmlNode *dg = NULL;
	for (xmlNode *temp = root->children; temp; temp = temp->next) {
		if (strcmp((char *)temp->name, (char *)"devicegroup") == 0) {
			char *name = (char *)xmlGetProp(temp,
			    (unsigned char *)"name");
			if (strcmp(name, dgname) == 0) {
				xmlFree(name);
				dg = temp;
				break;
			}
			xmlFree(name);
		}
	}

	return (dg);
}
