//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// ident	"@(#)cltelemetryattribute_xml.cc	1.2	08/05/20 SMI"
//

#include "ClXml.h"
#include "cltelemetryattribute.h"

//
// Returns a ValueList of xpath statements specific to the
// telemetry attributes
//
ValueList *
ClXml::cltelemetryattributeFilter(OptionValues *opts)
{
	ValueList *xpath = new ValueList();

	// If the subcommand is 'enable', then get only those set
	// to be enabled, otherwise get only those set to be
	// disabled.
	char state[10];
	if (strcmp(opts->getSubcommand(), "enable") == 0)
		(void) sprintf(state, "enabled");
	else if (strcmp(opts->getSubcommand(), "disable") == 0)
		(void) sprintf(state, "disabled");
	else {
		set_error(CL_EINTERNAL);
		return (xpath);
	}

	// If an object type is specifed, run through the list of
	// object types and create an xpath for each one.  Otherwise,
	// just create an xpath for the names
	ValueList *types = opts->getOptions(CLTA_TYPE);
	if ((types == NULL) || (types->getSize() == 0)) {
		char *buffer = new char [BUFSIZ];
		sprintf(buffer, "//telemetryAttribute[@name=\"%s\"]"
		    "/state[@value=\"%s\"]", opts->getOperand(),
		    state);

		xpath->add(buffer);
	} else {
		for (types->start(); !types->end(); types->next()) {
			char *buffer = new char [BUFSIZ];
			sprintf(buffer, "//telemetryObjectType[@name="
			    "\"%s\"]/telemetryAttribute[@name=\"%s\"]"
			    "/state[@value=\"%s\"]", types->getValue(),
			    opts->getOperand(), state);

			xpath->add(buffer);
		}
	}

	return (xpath);
}

//
// Processes the telemetry attribute information from the XML file and
// the command line options, and calls the function to enable/disable
// the attribute
//
int
ClXml::cltelemetryattributeProcessor(xmlNode *node, OptionValues *opts)
{
	ClCommand cmd;
	ValueList operands;
	ValueList types;
	int err = CL_NOERR;

	// In order to get the operand name, we must get the parent xmlNode of
	// this one, and get that node's "name" prop.  Since this node is the
	// 'state' node.
	char *temp = (char *)xmlGetProp(node->parent, (unsigned char *)"name");
	operands.add(temp);
	xmlFree(temp);

	// In order to get the type, we must get the parent xmlNode of
	// node->parent. In otherwords, the grandparent, and get that node's
	// "name" prop.
	temp = (char *)xmlGetProp(node->parent->parent,
	    (unsigned char *)"name");
	types.add(temp);
	xmlFree(temp);

	// Set verbose
	if (opts->optflgs & vflg) {
		cmd.isVerbose = 1;
	}

	if (strcmp(opts->getSubcommand(), "enable") == 0) {
		err = clta_do_set_talist(cmd, operands, types,
		    opts->optflgs, DO_ENABLE);
	} else  {
		err = clta_do_set_talist(cmd, operands, types,
		    opts->optflgs, DO_DISABLE);
	}

	if (err && !error)
		error = err;

	return (error);
}
