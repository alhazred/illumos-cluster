//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// ident	"@(#)clsnmphost_xml.cc	1.3	08/05/20 SMI"
//

#include "ClXml.h"
#include "clsnmphost.h"

//
// Returns a ValueList of xpath statements specific to the cluster
// snmp host information
//
ValueList *
ClXml::clsnmphostFilter(OptionValues *opts)
{
	ValueList *xpath = new ValueList();

	// Number of xpath statements depends on the number of
	// communities defined and the number of nodes
	ValueList *comm = opts->getOptions(CLSNMP_COMMUNITY);
	ValueList *nodes = opts->getOptions(CLSNMP_NODE);

	// If there are no communities specified, then there might be a case
	// there would be the same host in more than one community, which the
	// XML processing won't allow.  To get around this, we need to get all
	// the community names which are in the xml file, and add them to the
	// each xpath statement
	if (comm->getSize() == 0) {
		comm = getSNMPHostCommunityNames();
		if (comm == NULL)
			return (xpath);
	}

	char *buffer = new char [BUFSIZ];
	char *buffer2 = new char [BUFSIZ];

	// There will always be at least one node
	for (nodes->start();
	    !nodes->end();
	    nodes->next()) {

		// First, start with the default
		sprintf(buffer, "//snmphost[@name=\"%s\"]",
		    opts->getOperand());

		// Add the node
		sprintf(buffer, "%s[@nodeRef=\"%s\"]",
		    buffer, nodes->getValue());

		for (comm->start();
		    !comm->end();
		    comm->next()) {

			(void) sprintf(buffer2, "%s[@community=\"%s\"]",
			    buffer, comm->getValue());

			xpath->add(buffer2);
			buffer2[0] = '\0';
		}
		buffer[0] = '\0';
	}

	delete buffer;
	delete buffer2;

	return (xpath);
}

//
// Returns a ValueList of the community names in the XML file.
// NULL is returned on any error, and the global err is set, too.
//
ValueList *
ClXml::getSNMPHostCommunityNames()
{
	ValueList *comms = new ValueList(true);
	if (comms == NULL) {
		set_error(CL_ENOMEM);
		return (NULL);
	}

	// Doc should be here, double check
	if (doc == NULL) {
		set_error(CL_EINTERNAL);
		clerror("Unable to get XML document\n");
		return (NULL);
	}

	xpathCtx = NULL;
	xpathObj = NULL;
	xmlNodeSet *nodes = NULL;
	int num_nodes;

	int err = evaluateXpath(cmd_xpath[CLSNMPHOST_CMD]);
	if (err != CL_NOERR) {
		set_error(err);
		return (NULL);
	}

	// Check for empty nodeset
	if (xpathObj->nodesetval == NULL) {
		clerror("No clconfiguration data exists for this type.\n");
		set_error(CL_EINVAL);
		return (NULL);
	}

	nodes = xpathObj->nodesetval;
	num_nodes = nodes->nodeNr;

	// iterate through the nodes and copy the names into the ValueList
	for (int i = 0; i < num_nodes; i++) {

		char *name = (char *)xmlGetProp(nodes->nodeTab[i],
		    (unsigned char *)"community");

		if (name)
			comms->add(name);
	}

	return (comms);
}

//
// Processes the snmp host specific information from the XML file and the
// command line options, and calls the function to add the host
//
int
ClXml::clsnmphostProcessor(xmlNode *node, OptionValues *opts)
{
	ValueList operands;
	ValueList nodes;
	ValueList communities;

	// Set the host name
	char *temp = (char *)xmlGetProp(node, (unsigned char *)"name");
	operands.add(temp);
	xmlFree(temp);

	// Get the community name
	temp = (char *)xmlGetProp(node, (unsigned char *)"community");
	communities.add(temp);
	xmlFree(temp);

	// Get the node name
	temp = (char *)xmlGetProp(node, (unsigned char *)"nodeRef");
	nodes.add(temp);
	xmlFree(temp);

	ClCommand cmd;

	// verbose?
	if (opts->getOption(VERBOSE) != NULL) {
		cmd.isVerbose = 1;
	} else {
		cmd.isVerbose = 0;
	}

	return (clsnmphost_add(cmd, operands, nodes, communities));
}
