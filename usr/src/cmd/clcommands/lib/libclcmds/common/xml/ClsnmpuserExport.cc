//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// ident	"@(#)ClsnmpuserExport.cc 1.3	08/05/20 SMI"
//

#include "ClsnmpuserExport.h"

#define	AUTH_TYPE_MD5		(char *)"MD5"
#define	AUTH_TYPE_SHA		(char *)"SHA"
#define	IS_DEFAULT		(char *)"yes"

//
// Creates the snmpuserList element and sets it as the root element
//
ClsnmpuserExport::ClsnmpuserExport()
{
	root = xmlNewNode(NULL, (xmlChar *)"snmpuserList");
}

//
// Creates the snmpuser element and adds it to the snmpuserList
// element.
//
//	name:	The SNMP user name
//	node:	The node on which the user is configured
//	auth:	The authentication method (MD5 or SHA)
//	def:	Is this the default user
//	sec:	The security level of the default user
//
// Returns CL_EINTERNAL on any error, CL_NOERR otherwise.
//
int
ClsnmpuserExport::createSNMPUser(
    char *name,
    char *node,
    auth_t auth,
    bool def,
    char *sec)
{
	CHECKVALUE(name);
	CHECKVALUE(node);

	xmlNode *snmpuser = newXmlNode("snmpuser");

	// add the attributes
	newXmlAttr(snmpuser, "nodeRef", node);
	newXmlAttr(snmpuser, "name", name);

	if (auth == MD5)
		newXmlAttr(snmpuser, "auth", AUTH_TYPE_MD5);
	else
		newXmlAttr(snmpuser, "auth", AUTH_TYPE_SHA);

	if (def)
		newXmlAttr(snmpuser, "defaultUser", IS_DEFAULT);
	if (sec)
		newXmlAttr(snmpuser, "defaultSecurityLevel", sec);

	if (!xmlAddChild(root, snmpuser)) {
		errorAddingElement("snmpuser", "snmpuserList");
		return (CL_EINTERNAL);
	} else
		return (CL_NOERR);
}
