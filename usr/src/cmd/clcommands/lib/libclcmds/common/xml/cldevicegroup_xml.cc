//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// ident	"@(#)cldevicegroup_xml.cc	1.3	08/05/20 SMI"
//

#include "ClXml.h"
#include "cldevicegroup.h"

//
// Returns a ValueList of xpath statements specific to the
// device group name
//
ValueList *
ClXml::cldevicegroupFilter(OptionValues *opts)
{
	ValueList *xpath = new ValueList();

	// Number of xpath statements depends on the number
	// of type options given.
	ValueList *types = opts->getOptions(CLDG_TYPE);

	// If no type specified, then construct default xpath
	// based on the name of the device group
	if (types->getSize() == 0) {
		char *buffer  = new char [BUFSIZ];
		sprintf(buffer,
		    "//devicegroup[@name=\"%s\"]",
		    opts->getOperand());

		xpath->add(buffer);
		return (xpath);
	}

	// Otherwise, create an xpath statement for each type listed
	for (types->start();
	    !types->end();
	    types->next()) {

		char *buffer = new char [BUFSIZ];

		// create default statement with quorum device name
		sprintf(buffer,
		    "//devicegroup[@name=\"%s\"]",
		    opts->getOperand());

		// Append the type name
		sprintf(buffer, "%s[@type=\"%s\"]",
		    buffer,
		    types->getValue());

		xpath->add(buffer);
	}

	return (xpath);
}

//
// Processes the device group information from the XML file and the command
// line options, can calls the function to create the device group.
//
int
ClXml::cldevicegroupProcessor(xmlNode *node, OptionValues *opts)
{
	OptionValues *new_opts = new OptionValues(opts->getSubcommand());
	ValueList operands, con_nodes, devices, dgtypes;
	NameValueList *new_props;
	optflgs_t optflags = 0;
	char *char_ptr = NULL;
	char *char_ptr2 = NULL;


	// Set the device group name
	char *temp = (char *)xmlGetProp(node, (unsigned char *)"name");
	operands.add(temp);
	xmlFree(temp);

	// Set the type
	temp = (char *)xmlGetProp(node, (unsigned char *)"type");
	new_opts->addOption(CLDG_TYPE, temp);
	dgtypes.add(temp);
	xmlFree(temp);

	// See if a nodelist was passed in
	ValueList *nodes = opts->getOptions(CLDG_NODE);
	if (nodes->getSize() == 0) {
		// No nodelist, so use the one from the XML file
		xmlNode *tempNode = NULL;

		// Cycle to the "devicegroupNodeList" element
		for (tempNode = node->children;
		    tempNode;
		    tempNode = tempNode->next) {
			if (strcmp((char *)tempNode->name,
			    "devicegroupNodeList") == 0)
				break;
		}

		// For every devicegroupNode element, add the nodeRef name to
		// the opts list
		for (tempNode = tempNode->children;
		    tempNode;
		    tempNode = tempNode->next) {
			if (strcmp((char *)tempNode->name,
			    "devicegroupNode") == 0) {
				temp = (char *)xmlGetProp(tempNode,
				    (unsigned char *)"nodeRef");
				con_nodes.add(temp);
				xmlFree(temp);
			}
		}
	} else {
		// Use the node name(s) passed in
		for (nodes->start(); !nodes->end(); nodes->next()) {
			con_nodes.add(nodes->getValue());
		}
	}

	// If this is a type "rawdisk", then check the member list
	if (strcmp(new_opts->getOption(CLDG_TYPE), "rawdisk") == 0) {
		ValueList *member = opts->getOptions(CLDG_MEMBER);
		if (member->getSize() == 0) {
			// No member list, get it from the XML
			xmlNode *tempNode = NULL;

			// Cycle to the memberDeviceList element
			for (tempNode = node->children;
			    tempNode;
			    tempNode = tempNode->next) {
				if (strcmp((char *)tempNode->name,
				    "memberDeviceList") == 0)
					break;
			}

			// For every element of type memberDevice, add the name
			// of the member to the member list
			for (tempNode = tempNode->children;
			    tempNode;
			    tempNode = tempNode->next) {
				if (strcmp((char *)tempNode->name,
				    "memberDevice") == 0) {

					temp = (char *)xmlGetProp(tempNode,
					    (unsigned char *)"name");
					// get the name in d<N> format
					char_ptr = strrchr(temp, '/');
					if (char_ptr != NULL) {
						char_ptr2 = strchr(++char_ptr,
						    's');
						if (char_ptr2) {
							*char_ptr2 = '\0';
						}
						devices.add(char_ptr);
					} else {
						devices.add(temp);
					}
					xmlFree(temp);
				}
			}
			if (devices.getSize() == 0) {

				clerror("No member device information found "
				    "for device group \"%s\".\n",
				    new_opts->getOperand());
				error = CL_EINVAL;
			}
		} else {
			for (member->start(); !member->end(); member->next()) {
				devices.add(member->getValue());
			}
		}
	}


	// Now get all the properties
	xmlNode *proplist = NULL;
	for (proplist = node->children; proplist; proplist = proplist->next) {
		if (strcmp((char *)proplist->name, "propertyList") == 0)
			break;
	}

	// Properties defined on the command line are considered overrides
	// get all of them.
	genericPropertyOverrides(
	    proplist,
	    opts->getNVOptions(CLDG_PROP),
	    CLDG_PROP,
	    new_opts);

	delete [] temp;

	// Get the properties
	new_props = new_opts->getNVOptions(CLDG_PROP);

	if (opts->optflgs & vflg)
		optflags |= vflg;

	// Create the device group
	error = cldevicegroup_create(dgtypes, con_nodes, devices, *new_props,
	    operands, optflags);

	return (error);
}
