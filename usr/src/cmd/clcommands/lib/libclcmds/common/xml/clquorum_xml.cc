//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// ident	"@(#)clquorum_xml.cc	1.3	08/05/20 SMI"
//

#include "ClXml.h"
#include "clquorum.h"

//
// Returns a ValueList of xpath statements specific to the cluster quorum
//
ValueList *
ClXml::clquorumFilter(OptionValues *opts)
{
	ValueList *xpath = new ValueList();
	char buffer[BUFSIZ];

	// Number of xpath statements depends on the number of
	// types specified in the options.
	ValueList *types = opts->getOptions(CLQUORUM_TYPE);

	// If no type is specified, then construct default
	// xpath based on name of quorum device
	if (types->getSize() == 0) {
		*buffer = '\0';
		(void) sprintf(buffer,
		    "//quorumDevice[@name=\"%s\"]",
		    opts->getOperand());

		xpath->add(buffer);
		return (xpath);
	}

	// Otherwise, create an xpath statement for each type listed.
	for (types->start(); !types->end(); types->next()) {
		*buffer = '\0';

		// create default statement with quorum device name
		(void) sprintf(buffer,
		    "//quorumDevice[@name=\"%s\"]",
		    opts->getOperand());

		// append the device type
		(void) sprintf(buffer, "%s[@type=\"%s\"]",
		    buffer, types->getValue());

		xpath->add(buffer);
	}

	return (xpath);
}

//
// Processes the quorum information from the XML file and the command line
// options, and calls the function to add the quorum devices.
//
int
ClXml::clquorumProcessor(xmlNode *node, OptionValues *opts)
{
	OptionValues *new_opts = new OptionValues(opts->getSubcommand());
	ValueList properties;
	ValueList operands, devicetypes;
	NameValueList new_properties;
	ClCommand cmd;
	NameValueList *new_NVprop;
	int i, found = 0;
	char *read_only_prop[] = {
		"votecount", "gdevname", "type", "access_mode", "timeout",
		(char *)0
	};

	// Set the quorum device name
	char *temp = (char *)xmlGetProp(node, (unsigned char *)"name");
	operands.add(0, temp);
	xmlFree(temp);

	// Set the device type
	temp = (char *)xmlGetProp(node, (unsigned char *)"type");
	devicetypes.add(temp);
	xmlFree(temp);

	// Now get all the properties
	xmlNode *proplist = NULL;
	for (proplist = node->children; proplist; proplist = proplist->next) {
		if (strcmp((char *)proplist->name, "propertyList") == 0)
			break;
	}

	// Call generic property processor
	genericPropertyOverrides(
	    proplist,
	    opts->getNVOptions(CLQUORUM_PROP),
	    CLQUORUM_PROP,
	    new_opts);

	delete [] temp;

	// Set verbose
	if (opts->optflgs & vflg) {
		cmd.isVerbose = 1;
	}

	// Set command arguments
	cmd.argc = opts->argc;
	cmd.argv = opts->argv;

	//
	// We need to remove the common properties exported from ccr. These
	// properties cannot be specified when add a quorum.
	//
	new_NVprop = new_opts->getNVOptions(CLQUORUM_PROP);
	for (new_NVprop->start(); !new_NVprop->end(); new_NVprop->next()) {
		char *prop_name = new_NVprop->getName();
		if (!prop_name)
			continue;

		found = 0;
		for (i = 0; read_only_prop[i]; i++) {
			if (strcmp(prop_name, read_only_prop[i]) == 0) {
				found = 1;
				break;
			}
		}

		// We don't want to add the read-only properties
		if (found)
			continue;

		(void) new_properties.add(prop_name, new_NVprop->getValue());
	}

	return (clquorum_add(cmd, operands, opts->optflgs, devicetypes,
	    new_properties));
}
