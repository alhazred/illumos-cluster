//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// ident	"@(#)ClXml.cc	1.12	08/05/20 SMI"
//

#include "ClXml.h"
#include "cluster.h"
#include "clnode.h"

//
// Begin ClXml Class definitions
//

//
// Simple method to not clobber the error variable, if it
// is already set
//
void
ClXml::set_error(int err)
{
	if (error == CL_NOERR)
		error = err;
}

//
// Reset error in case a caller is not interested in past errors
//
void
ClXml::reset_error()
{
	error = 0;
}

//
// Constructor
//
ClXml::ClXml()
{
	// Populate the array of xpath statements
	cmd_xpath[CLUSTER_CMD] = (char *)"//cluster";
	cmd_xpath[CLDEVICE_CMD] = (char *)"//device";
	cmd_xpath[CLDEVICEGROUP_CMD] = (char *)"//devicegroup";
	cmd_xpath[CLINTERCONNECT_CMD] = (char *)"//clusterTransport";
	cmd_xpath[CLNAS_CMD] = (char *)"//nasdevice";
	cmd_xpath[CLNODE_CMD] = (char *)"//nodeList/*";
	cmd_xpath[CLQUORUM_CMD] = (char *)"//quorumDevice";
	cmd_xpath[CLRESOURCE_CMD] = (char *)"//resource";
	cmd_xpath[CLRESLOGICALHOSTNAME_CMD] = (char *)"//resource";
	cmd_xpath[CLRESSHAREDADDRESS_CMD] = (char *)"//resource";
	cmd_xpath[CLRESOURCEGROUP_CMD] = (char *)"//resourcegroup";
	cmd_xpath[CLRESOURCETYPE_CMD] = (char *)"//resourcetype";
	cmd_xpath[CLSNMPHOST_CMD] = (char *)"//snmphost";
	cmd_xpath[CLSNMPUSER_CMD] = (char *)"//snmpuser";
	cmd_xpath[CLTELEMETRYATTRIBUTE_CMD] = (char *)"//telemetryAttribute";

	// Populate the array of filter function pointers
	filter_method[CLUSTER_CMD] = &clusterFilter;
	filter_method[CLDEVICE_CMD] = &cldeviceFilter;
	filter_method[CLDEVICEGROUP_CMD] = &cldevicegroupFilter;
	filter_method[CLINTERCONNECT_CMD] = &clinterconnectFilter;
	filter_method[CLNAS_CMD] = &clnasFilter;
	filter_method[CLNODE_CMD] = &clnodeFilter;
	filter_method[CLQUORUM_CMD] = &clquorumFilter;
	filter_method[CLRESOURCE_CMD] = &clresourceFilter;
	filter_method[CLRESLOGICALHOSTNAME_CMD] = &clresourceFilter;
	filter_method[CLRESSHAREDADDRESS_CMD] = &clresourceFilter;
	filter_method[CLRESOURCEGROUP_CMD] = &clresourcegroupFilter;
	filter_method[CLRESOURCETYPE_CMD] = &clresourcetypeFilter;
	filter_method[CLSNMPHOST_CMD] = &clsnmphostFilter;
	filter_method[CLSNMPUSER_CMD] = &clsnmpuserFilter;
	filter_method[CLTELEMETRYATTRIBUTE_CMD] = &cltelemetryattributeFilter;

	// Populate the array of procesor function pointers
	processor_method[CLUSTER_CMD] = &clusterProcessor;
	processor_method[CLDEVICE_CMD] = &cldeviceProcessor;
	processor_method[CLDEVICEGROUP_CMD] = &cldevicegroupProcessor;
	processor_method[CLINTERCONNECT_CMD] = &clinterconnectProcessor;
	processor_method[CLNAS_CMD] = &clnasProcessor;
	processor_method[CLNODE_CMD] = &clnodeProcessor;
	processor_method[CLQUORUM_CMD] = &clquorumProcessor;
	processor_method[CLRESOURCE_CMD] = &clresourceProcessor;
	processor_method[CLRESLOGICALHOSTNAME_CMD] = &clresourceProcessor;
	processor_method[CLRESSHAREDADDRESS_CMD] = &clresourceProcessor;
	processor_method[CLRESOURCEGROUP_CMD] = &clresourcegroupProcessor;
	processor_method[CLRESOURCETYPE_CMD] = &clresourcetypeProcessor;
	processor_method[CLSNMPHOST_CMD] = &clsnmphostProcessor;
	processor_method[CLSNMPUSER_CMD] = &clsnmpuserProcessor;
	processor_method[CLTELEMETRYATTRIBUTE_CMD] =
	    &cltelemetryattributeProcessor;

	/*
	 * this initializes the library and check potential ABI mismatches
	 * between the version it was compiled for and the actual shared
	 * library used.
	 */
	LIBXML_TEST_VERSION

	// Send error messages from libxml2 to /dev/null
	FILE *out = fopen("/dev/null", "w");
	xmlSetGenericErrorFunc(out, NULL);

	error = 0;
	doc = NULL;
	xpathCtx = NULL;
	xpathObj = NULL;
}

//
// Deconstructor
//
ClXml::~ClXml()
{
	// free the XPath object
	xmlXPathFreeObject(xpathObj);

	// free the XPath context
	xmlXPathFreeContext(xpathCtx);

	// free the doc
	xmlFreeDoc(doc);

	// free parser related memory
	xmlCleanupParser();
}

//
// Evaluate the xpath statement given.  Once the expression has
// been evaluated, the xpathObj global variable may be used to
// access the nodeList.
//
// Returns a CL error, if one was encountered
//
int
ClXml::evaluateXpath(char *statement)
{
	// create an XPath evaluation context for the doc
	if (xpathCtx) {
		xmlXPathFreeContext(xpathCtx);
		xpathCtx = NULL;
	}
	xpathCtx = xmlXPathNewContext(doc);
	if (xpathCtx == NULL) {
		clerror("Unable to create new XPath context.\n");
		return (CL_EINTERNAL);
	}

	// Evalute the xpath expression
	xpathObj = xmlXPathEvalExpression(
	    (unsigned char *)statement,
	    xpathCtx);

	if (xpathCtx) {
		xmlXPathFreeContext(xpathCtx);
		xpathCtx = NULL;
	}

	if (xpathObj == NULL) {
		clerror("Unable to evaluate xpath expression:  \"%s\".\n",
		    statement);
		return (CL_EINTERNAL);
	}

	return (CL_NOERR);
}

//
// Validate the list of operands for the specified command.
// For each operand in the list, it will verify that it exists
// in the XML config, if not, it will issue an error and remove
// it from the list.
//
// Returns a CL error, if one was encountered
//
int
ClXml::validateOperands(
    command_t	command,
    char	*input_file,
    ValueList	*list)
{
	int err = CL_NOERR;

	if (list == NULL)
		return (CL_EINTERNAL);

	if (list->getSize() == 0)
		return (CL_EINTERNAL);

	ValueList *operands = new ValueList();
	ValueList *newList = new ValueList();

	err = getAllObjectNames(command, input_file, operands);

	if (err != CL_NOERR)
		return (err);

	for (list->start();
	    !list->end();
	    list->next()) {

		if (operands->isValue(list->getValue()) == 0) {
			clerror("Specified operand value not found "
			    "in configuration file - \"%s\".\n",
			    list->getValue());

			err = CL_EINVAL;
		} else {
			newList->add(list->getValue());
		}
	}

	list->clear();
	for (newList->start();
	    !newList->end();
	    newList->next()) {

		list->add(newList->getValue());
	}
	newList = NULL;

	return (err);
}

//
// Populates the ValueList object with the names of the objects which
// are contained in the xml file.  The objects collected depends on the
// command.
//
// Returns a CL error, if one was encountered
//
int
ClXml::getAllObjectNames(
    command_t	command,
    char	*input_file,
    ValueList	*list)
{
	int err = CL_NOERR;

	if (list == NULL)
		list = new ValueList(true);

	// remove all items from the list just to be sure
	list->clear();

	// Get the doc
	if (doc == NULL)
		if (getDoc(input_file) != 0)
			return (CL_EINVAL);

	// If clinterconnect, then go to custom method
	if (command == CLINTERCONNECT_CMD)
		return (getAllInterconnectNames(list));

	xpathCtx = NULL;
	xpathObj = NULL;
	xmlNodeSet *nodes = NULL;
	int num_nodes;

	err = evaluateXpath(cmd_xpath[command]);
	if (err != CL_NOERR)
		return (err);

	// Check for empty nodeset
	if (xpathObj->nodesetval == NULL) {
		clerror("No clconfiguration data exists for this type.\n");
		return (CL_EINVAL);
	}

	nodes = xpathObj->nodesetval;
	num_nodes = nodes->nodeNr;

	// iterate through the nodes and copy the names into the ValueList
	for (int i = 0; i < num_nodes; i++) {
		char *name = (char *)xmlGetProp(nodes->nodeTab[i],
		    (unsigned char *)"name");

		if (name) {
			if (strcmp(name,
			    (char *)CLCOMMANDS_WILD_OPERAND) == 0) {
				clerror("Invalid object name encountered - "
				    "\"%s\".\n",
				    (char *)CLCOMMANDS_WILD_OPERAND);
				set_error(CL_EINVAL);
			} else {
				list->add(name);
			}
		}
	}

	return (err);
}

//
// The applyConfig() method reads in the values from the XML file and
// applies those values to the cluster configuration.  For each object
// to be created, the opts argument should contain the name of the
// object, provided either from the command line or through the use of
// the getAllObjectNames() method.
//
int
ClXml::applyConfig(
    command_t	command,
    char	*input_file,
    OptionValues *opts)
{
	xpathCtx = NULL;
	xpathObj = NULL;
	xmlNodeSet *nodes = NULL;
	int num_nodes = 0;

	if (doc == NULL) {
		if (getDoc(input_file) != 0) {
			return (CL_EINVAL);
		}
	}

	// The cluster command doesn't need an operand, but the XML
	// infrastruture does, so we'll call it "+"
	if (command == CLUSTER_CMD && opts->getOperand() == NULL)
		opts->setOperand((char *)CLCOMMANDS_WILD_OPERAND);

	// get all the object names, in case of '+' sign.
	if (opts->getOperand() == NULL) {
		clerror("No operand found in OptionValues object.\n");
		return (CL_EINTERNAL);
	} else if (strcmp(opts->getOperand(),
	    (char *)CLCOMMANDS_WILD_OPERAND) == 0) {
		ValueList *list = new ValueList(true);
		set_error(getAllObjectNames(command, input_file, list));
		if (error != 0) {
			list = NULL;
			return (error);
		}

		if (list->getSize() == 0) {
			clerror("No configuration information found for "
			    "this object type.\n");
			return (CL_EINVAL);
		}

		// For each object, make a call to applyConfig again
		// substituting the correct operand name each time.
		for (list->start();
		    !list->end();
		    list->next()) {

			opts->setOperand(list->getValue());
			set_error(applyConfig(command, input_file, opts));
		}

		return (error);
	}

	ValueList *xpath = getXpathExpressions(command, opts);

	if (xpath->getSize() == 0) {
		clerror("No applicable configuration data found.\n");
		set_error(CL_EINVAL);
		goto cleanup;
	}

	// Evaluate xpath expression
	for (xpath->start();
	    !xpath->end();
	    xpath->next()) {

		set_error(evaluateXpath(xpath->getValue()));

		// Check for empty nodeset
		if (xpathObj->nodesetval == NULL) {
			continue;
		}

		nodes = xpathObj->nodesetval;
		num_nodes = nodes->nodeNr;

		// The DTD cannot inforce duplicate names for some cases, but
		// the XML is laid out such that the xPath statement should only
		// allow for zero or one matching nodes.  If this is not the
		// case then we have competing data, and will abort.
		if ((num_nodes > 1) && (command != CLTELEMETRYATTRIBUTE_CMD)) {
			clerror("Invalid clconfiguration document.\n");
			clerror("There are multiple objects which match "
			    "the object name \"%s\".\n", opts->getOperand());
			set_error(CL_EINVAL);
			goto cleanup;
		}

		// Process the XML information, and apply the specific changes
		for (int i = 0; i < num_nodes; i++) {
			set_error((*this.*processor_method[command])
			    (nodes->nodeTab[i], opts));
		}
	}

cleanup:
	// delete the xpath buffer
	xpath->clear();
	delete xpath;

	return (error);
}

//
// Creates an XML file specified by output_file, containing the data
// from the ClcmdExport object, export_obj.
//
// Returns a CL error, if one was encountered.
//
int
ClXml::createExportFile(
    command_t	command,
    char 	*output_file,
    ClcmdExport	*export_obj,
    ValueList	*obj_types)
{
	int ret_clerr = CL_NOERR;

	// Get the root node
	xmlNode *root = NULL;
	if (command != CLUSTER_CMD)
		root = getClusterElement();
	else
		root = export_obj->getRootNode();

	if (root == NULL) {
		clerror("Unable to get XML data for export.\n");
		return (CL_EINTERNAL);
	}

	// These commands depend on the node info...
	if (command == CLDEVICE_CMD ||
	    command == CLDEVICEGROUP_CMD ||
	    command == CLINTERCONNECT_CMD ||
	    command == CLQUORUM_CMD ||
	    command == CLRESOURCE_CMD ||
	    command == CLRESOURCEGROUP_CMD ||
	    command == CLRESOURCETYPE_CMD ||
	    command == CLSNMPMIB_CMD ||
	    command == CLSNMPUSER_CMD ||
	    command == CLSNMPHOST_CMD) {

		xmlNode *nodeRoot = get_clnode_xml_elements()->getRootNode();
		if (!xmlAddChild(root, nodeRoot)) {
			clerror("Error while adding node elements to root.\n");
			xmlFree(nodeRoot);
			xmlFree(root);
			return (CL_EIO);
		}
	}

	// resources depend on resource types and groups
	if (command == CLRESOURCE_CMD ||
	    command == CLRESLOGICALHOSTNAME_CMD ||
	    command == CLRESSHAREDADDRESS_CMD) {

		xmlNode *rtRoot =
		    get_clresourcetype_xml_elements()->getRootNode();
		if (rtRoot == NULL) {
			clerror("Unable to get resource type elements.\n");
			xmlFree(root);
			return (CL_EINTERNAL);
		} else if (!xmlAddChild(root, rtRoot)) {
			clerror("Error while adding resource type "
			    "elements to root.\n");
			xmlFree(rtRoot);
			xmlFree(root);
			return (CL_EIO);
		}

		xmlNode *rgRoot =
		    get_clresourcegroup_xml_elements(true)->getRootNode();
		if (rgRoot == NULL) {
			clerror("Unable to get resource group elements.\n");
			xmlFree(rtRoot);
			xmlFree(root);
			return (CL_EINTERNAL);
		} else if (!xmlAddChild(root, rgRoot)) {
			clerror("Error while adding resource group "
			    "elements to root.\n");
			xmlFree(rgRoot);
			xmlFree(root);
			return (CL_EIO);
		}
	}

	// Cluster depends on all other commands
	if (command == CLUSTER_CMD) {
		bool all_types = (!obj_types || (obj_types->getSize() == 0)) ?
		    true : false;

		// Add node dependency
		if (all_types || obj_types->isValue(TYPE_NODE) ||
		    obj_types->isValue(TYPE_DEVICE) ||
		    obj_types->isValue(TYPE_DEVGRP) ||
		    obj_types->isValue(TYPE_INTR) ||
		    obj_types->isValue(TYPE_QUORUM) ||
		    obj_types->isValue(TYPE_RT) ||
		    obj_types->isValue(TYPE_RG) ||
		    obj_types->isValue(TYPE_RS) ||
		    obj_types->isValue(TYPE_RSLH) ||
		    obj_types->isValue(TYPE_RSSA) ||
		    obj_types->isValue(TYPE_SNMPMIB) ||
		    obj_types->isValue(TYPE_SNMPHOST) ||
		    obj_types->isValue(TYPE_SNMPUSER)) {
			xmlNode *nodes =
			    get_clnode_xml_elements()->getRootNode();
			ret_clerr = addElementToRoot(nodes, root,
			    (char *)"node");
		}

		if (all_types || obj_types->isValue(TYPE_INTR) ||
		    obj_types->isValue(TYPE_NODE)) {
			xmlNode *trans =
			    get_clinterconnect_xml_elements()->getRootNode();
			ret_clerr = addElementToRoot(trans, root,
			    (char *)"transport");
		}
		if (all_types || obj_types->isValue(TYPE_DEVICE)) {
			xmlNode *device =
			    get_cldevice_xml_elements()->getRootNode();
			ret_clerr = addElementToRoot(device, root,
			    (char *)"device");
		}
		if (all_types || obj_types->isValue(TYPE_QUORUM)) {
			xmlNode *quorum =
			    get_clquorum_xml_elements()->getRootNode();
			ret_clerr = addElementToRoot(quorum, root,
			    (char *)"quorum");
		}
		if (all_types || obj_types->isValue(TYPE_DEVGRP)) {
			xmlNode *dg =
			    get_cldevicegroup_xml_elements()->getRootNode();
			ret_clerr = addElementToRoot(dg, root,
			    (char *)"device group");
		}
		if (all_types || obj_types->isValue(TYPE_RT) ||
		    obj_types->isValue(TYPE_RS) ||
		    obj_types->isValue(TYPE_RSLH) ||
		    obj_types->isValue(TYPE_RSSA)) {
			xmlNode *rt =
			    get_clresourcetype_xml_elements()->getRootNode();
			ret_clerr = addElementToRoot(rt, root,
			    (char *)"resource type");
		}
		if (all_types || obj_types->isValue(TYPE_RG) ||
		    obj_types->isValue(TYPE_RS) ||
		    obj_types->isValue(TYPE_RSLH) ||
		    obj_types->isValue(TYPE_RSSA)) {
			xmlNode *rg = get_clresourcegroup_xml_elements
			    (all_types ? true : false)->getRootNode();
			ret_clerr = addElementToRoot(rg, root,
			    (char *)"resource group");
		}
		if (all_types || obj_types->isValue(TYPE_RS)) {
			xmlNode *res =
			    get_clresource_xml_elements(0)->getRootNode();
			ret_clerr = addElementToRoot(res, root,
			    (char *)"resource");
		}
		if (all_types || obj_types->isValue(TYPE_RSSA)) {
			// RS contains RSSA
			if (!all_types && !obj_types->isValue(TYPE_RS)) {
				xmlNode *res =
				get_clresource_xml_elements(1)->getRootNode();
				ret_clerr = addElementToRoot(res, root,
				    (char *)"resource");
			}
		}
		if (all_types || obj_types->isValue(TYPE_RSLH)) {
			// RS contains RSLH
			if (!all_types && !obj_types->isValue(TYPE_RS)) {
				xmlNode *res =
				get_clresource_xml_elements(2)->getRootNode();
				ret_clerr = addElementToRoot(res, root,
				    (char *)"resource");
			}
		}
		if (all_types || obj_types->isValue(TYPE_NAS)) {
			xmlNode *nas =
			    get_clnas_xml_elements()->getRootNode();
			ret_clerr = addElementToRoot(nas, root,
			    (char *)"NAS device");
		}
		if (all_types || obj_types->isValue(TYPE_SNMPMIB)) {
			xmlNode *snmpmib =
			    get_clsnmpmib_xml_elements()->getRootNode();
			addElementToRoot(snmpmib, root, (char *)"SNMP mib");
		}
		if (all_types || obj_types->isValue(TYPE_SNMPHOST)) {
			xmlNode *snmphost =
			    get_clsnmphost_xml_elements()->getRootNode();
			addElementToRoot(snmphost, root, (char *)"SNMP host");
		}
		if (all_types || obj_types->isValue(TYPE_SNMPUSER)) {
			xmlNode *snmpuser =
			    get_clsnmpuser_xml_elements()->getRootNode();
			addElementToRoot(snmpuser, root, (char *)"SNMP user");
		}
		if (all_types || obj_types->isValue(TYPE_TELATTR)) {
			xmlNode *tele_attr =
			    get_clta_xml_elements()->getRootNode();
			addElementToRoot(tele_attr, root,
			    (char *)"Telemetry Attribute");
		}

		if (ret_clerr != CL_NOERR) {
			return (CL_EINTERNAL);
		}
	} else {
		// Add the object root to the root element
		xmlNode *objRoot = export_obj->getRootNode();
		if (!xmlAddChild(root, objRoot)) {
			clerror("Error while adding object element to root.\n");
			xmlFree(objRoot);
			xmlFree(root);
			return (CL_EIO);
		}
	}

	// These commands depend on interconnect info...
	if (command == CLNODE_CMD) {
		// Only get the interconnect data if it is cluster node
		if (obj_types && obj_types->isValue(NODETYPE_SERVER)) {
			xmlNode *interconnectRoot =
			    get_clinterconnect_xml_elements()->getRootNode();
			if (!xmlAddChild(root, interconnectRoot)) {
				clerror("Error while adding interconnect "
				    "elements to root.\n");
				xmlFree(interconnectRoot);
				xmlFree(root);
				return (CL_EIO);
			}
		}
	}

	// Create the doc
	xmlDoc *document = xmlNewDoc(BAD_CAST "1.0");
	xmlDocSetRootElement(document, root);

	// Set the DTD info
	xmlCreateIntSubset(document, (xmlChar *)"cluster",
	    NULL, (xmlChar *)DTD_PATH);

	// Fill a buffer with the doc
	xmlChar *xmlbuff;
	int buffersize;
	xmlDocDumpFormatMemory(document, &xmlbuff, &buffersize, 1);

	// Write doc to output file
	if (!output_file || strcmp(output_file, "-") == 0)
		fprintf(stdout, "%s", (char *)xmlbuff);
	else {
		FILE *fp = fopen(output_file, "r");
		if (fp != NULL) {
			clerror("Output file already exists - \"%s\".\n",
			    output_file);
			xmlFree(xmlbuff);
			xmlFreeDoc(document);
			return (CL_EINVAL);
		}

		fp = clcommand_fopen(output_file, "w");
		if (fp == NULL) {
			clerror("Failed to create file \"%s\" "
			    "- %s.\n",
			    output_file, strerror(errno));
			ret_clerr = CL_EIO;
			xmlFree(xmlbuff);
			xmlFreeDoc(document);
			return (ret_clerr);
		}
		fprintf(fp, "%s", (char *)xmlbuff);
		fclose(fp);
	}

	xmlFree(xmlbuff);
	xmlFreeDoc(document);
	return (CL_NOERR);
}

//
// Creates the "cluster" XML element and returns it.
//
xmlNode *
ClXml::getClusterElement()
{
	xmlNode *cluster = xmlNewNode(NULL, (xmlChar *)"cluster");

	// Set the cluster name
	if (clconf_lib_init() != 0) {
		clerror("Error while initializing clconf library.\n");
		set_error(CL_EINTERNAL);
		return (cluster);
	}

	char *name = clconf_get_clustername();
	if (name == NULL) {
		clerror("Unable to get cluster name.\n");
		set_error(CL_EINTERNAL);
		return (cluster);
	}
	xmlNewProp(cluster, (xmlChar *)"name", (xmlChar *)name);

	return (cluster);
}

//
// Adds the xmlNode element to root.  Name is the name of the element.
// Returns CL_EINTERNAL if there was an error.
//
int
ClXml::addElementToRoot(xmlNode *element, xmlNode *root, char *name)
{
	if (element == NULL) {
		clerror("Unable to get %s elements.\n", name);
		return (CL_EINTERNAL);
	} else if (!xmlAddChild(root, element)) {
		clerror("Error while adding %s element to root.\n",
		    name);
		xmlFree(element);
		return (CL_EINTERNAL);
	} else
		return (CL_NOERR);
}


//
// Creates a temporary file, TMPFILE.<pid>, and copies all of the
// data from stdin to this file.
//
// Returns the name of the file
//
char *
ClXml::stdinToFile()
{
	FILE *fp;
	char filename[1024];
	char buffer[BUFSIZ];
	size_t rcount;
	size_t wcount;

	(void) (sprintf(filename, "TMPFILE.%d", getpid()));

	if ((fp = (fopen(filename, "w+"))) == NULL) {
		(void) clerror("Failed to open file (%s) - %s.\n",
		    filename, strerror(errno));
		return (NULL);
	}

	while ((rcount = fread(buffer, 1, sizeof (buffer), stdin)) != 0) {
		if ((wcount = fwrite(buffer, 1, rcount, fp)) == 0) {
			(void) clerror("Writing to file failed -"
			    "filename: %s.\n", filename);
			return (NULL);
		}
	}
	if (!feof(stdin)) {
		(void) clerror("Reading of stdin failed.\n");
		return (NULL);
	}

	return (filename);
}

//
// Creates an in memory version of the XML doc
// If the filename is "-", then it will direct stdin to a temporary
// file, read in that data, and then delete the temporary file.
//
// Returns a CL error, if one was encountered.
//
int
ClXml::getDoc(char *filename)
{
	// get an in memory version of the xml file and validate it
	if (filename == NULL) {
		return (CL_EINTERNAL);

	} else if (strcmp(filename, "-") == 0) {
		char *temp = stdinToFile();
		if (temp == NULL) {
			return (CL_EINTERNAL);
		}
		int err = parseDoc(temp);
		if (remove(temp) != 0) {
			clerror("Unable to delete temporary file %s.\n",
			    tmpfile);
			if (err == 0)
				return (CL_EIO);
		}

		return (err);
	} else {
		return (parseDoc(filename));
	}
}

//
// Parses and validates the xml file
//
// Returns a CL error, if one was encountered.
//
int
ClXml::parseDoc(char *filename)
{
	// Now Parse and validate the file to an xml tree
	xmlParserCtxtPtr ctxt = NULL;
	xmlValidCtxtPtr v_ctxt = NULL;
	int valid = 0;

	ctxt = xmlNewParserCtxt();
	if (ctxt == NULL) {
		clerror("Failed to allocate parser context.\n");
		set_error(CL_ENOMEM);
		goto cleanup;
	}

	doc = xmlParseFile(filename);
	if (doc == NULL) {
		clerror("Failed to parse clconfiguration document.\n");
		set_error(CL_EINTERNAL);
		goto cleanup;
	}

	v_ctxt = (xmlValidCtxtPtr) xmlMalloc(sizeof (xmlValidCtxt));
	if (v_ctxt == NULL) {
		clerror("Failed to allocate validity context.\n");
		set_error(CL_ENOMEM);
		goto cleanup;
	} else {
		(void) memset(v_ctxt, 0, sizeof (xmlValidCtxt));
	}

	valid = xmlValidateDocument(v_ctxt, doc);
	if (valid == 0) {
		clerror("Failed to validate clconfiguration document.\n");
		xmlFreeDoc(doc);
		doc = NULL;
		set_error(CL_EINVAL);
	}


cleanup:
	if (ctxt != NULL) {
		xmlFreeParserCtxt(ctxt);
	}

	if (v_ctxt != NULL) {
		xmlFree(v_ctxt);
	}

	return (error);
}


//
// Recursively prints all the element names contained in the
// xmlNode -- Debug purposes only
//
void
ClXml::printElementNames(xmlNode *node)
{
	xmlNode *cur_node = NULL;

	for (cur_node = node; cur_node; cur_node = cur_node->next) {
		if (cur_node->type == XML_ELEMENT_NODE) {
			printf("Element name: %s\n", cur_node->name);
		}

		printElementNames(cur_node->children);
	}
}

//
// Calls the specific command function to which will return a
// ValueList of xpath statements.
//
ValueList *
ClXml::getXpathExpressions(command_t cmd, OptionValues *opts)
{
	// return function pointer to appropriate cmd parser
	return ((*this.*filter_method[cmd])(opts));
}

//
// Many of the commands have properties which are structured in
// the XML in a similar fashion.  This is a generic function to
// get those properties.
//
void
ClXml::genericPropertyOverrides(
    xmlNode *proplist,
    NameValueList *overrides,
    int propid,
    OptionValues *new_opts)
{
	// Properties defined on the command line are considered overrides
	// get all of them.
	xmlNode *prop = NULL;
	char buffer[BUFSIZ];

	// Add the original user specified properties
	new_opts->addNVOptions(propid, overrides);

	if (!proplist) {
		return;
	}

	// Add the properties in xml file.
	for (prop = proplist->children; prop; prop = prop->next) {
		if (strcmp((char *)prop->name, "property") == 0) {
			char *name = (char *)xmlGetProp(prop,
			    (unsigned char *)"name");
			char *value = (char *)xmlGetProp(prop,
			    (unsigned char *)"value");

			char *or_name = NULL;
			bool override = true;
			for (overrides->start();
			    !overrides->end();
			    overrides->next()) {

				or_name = overrides->getName();
				if ((or_name != NULL) &&
				    (strcmp(name, or_name) == 0)) {
					override = false;
					break;
				}
			}
			if (override) {
				new_opts->addNVOption(propid, name, value);
			}

			name = NULL;
			or_name = NULL;
			value = NULL;

			delete [] or_name;
			xmlFree(name);
			xmlFree(value);
		}
	}
}

//
// Get the properties of an XML object of the given name.
// The properies are saved in the NameValueList. The list must be
// already created.
//
int
ClXml::getObjProps(command_t command, char *obj_name, char *input_file,
    NameValueList *obj_props)
{
	int found;
	int err = CL_NOERR;
	xmlNode *root_element = NULL;
	xmlNode *xml_node = NULL;
	xmlAttr *xml_attr = NULL;
	char *name, *value;
	int num_nodes;
	xmlNodeSet *nodes = NULL;
	int i;

	if (!obj_name || !input_file || !obj_props) {
		err = CL_EINTERNAL;
		goto cleanup;
	}

	// Get the doc
	if (doc == NULL) {
		if (getDoc(input_file) != 0) {
			clerror("Invalid file \"%s\".\n",
			    input_file);
			return (CL_EINVAL);
		}
	}

	// Evaluate xpath
	err = evaluateXpath(cmd_xpath[command]);
	if (err != CL_NOERR) {
		return (err);
	}

	// Check for empty nodeset
	if (xpathObj->nodesetval == NULL) {
		goto cleanup;
	}

	nodes = xpathObj->nodesetval;
	num_nodes = nodes->nodeNr;

	// Go through the elements
	found = 0;
	for (i = 0; i < num_nodes; i++) {
		// Name match?
		name = (char *)xmlGetProp(nodes->nodeTab[i],
		    (unsigned char *)"name");
		if (name && (strcmp((const char *)name, obj_name) == 0)) {
			found = 1;
			break;
		}
	}

	// Not found?
	if (!found) {
		goto cleanup;
	}

	// Get the properties
	found = 0;
	for (xml_node = nodes->nodeTab[i]->children; xml_node;
	    xml_node = xml_node->next) {
		// Skip
		if (!xml_node->name ||
		    (strcmp((const char *)xml_node->name, "propertyList") !=
		    0)) {
			continue;
		}
		found = 1;
		break;
	}

	if (!found) {
		goto cleanup;
	}

	// Add the properties
	for (xml_node = xml_node->children; xml_node;
	    xml_node = xml_node->next) {
		// Skip
		if (xml_node->type != XML_ELEMENT_NODE) {
			continue;
		}
		if (!xml_node->name ||
		    (strcmp((const char *)xml_node->name, "property") != 0)) {
			continue;
		}

		name = (char *)xmlGetProp(xml_node, (unsigned char *)"name");
		if (!name) {
			continue;
		}

		value = (char *)xmlGetProp(xml_node, (unsigned char *)"value");
		(void) obj_props->add(name, value);
	}

cleanup:
	if (err != CL_NOERR) {
		clcommand_perror(err);
	}

	return (err);
}

//
// Get the cables and the properties in xml file.
//
int
ClXml::getCableProps(char *obj_name, char *input_file,
    cltr_cable_t **cable_props)
{
	int err = CL_NOERR;

	if (!obj_name || !input_file || !cable_props) {
		clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}

	// Get the doc
	if (doc == NULL) {
		if (getDoc(input_file) != 0) {
			clerror("Invalid file \"%s\".\n",
			    input_file);
			return (CL_EINVAL);
		}
	}

	// Get the cables
	err = getEndpointsProps(obj_name, cable_props);

	return (err);
}
