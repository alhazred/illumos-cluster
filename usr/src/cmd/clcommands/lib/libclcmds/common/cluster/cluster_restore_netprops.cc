//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)cluster_restore_netprops.cc	1.2	08/05/20 SMI"

//
// Process cluster "restore-netprops"
//

#include "cluster.h"
#include "clnode.h"

//
// cluster "restore-netprops"
//
clerrno_t
cluster_restore_netprops(ClCommand &cmd, ValueList &operands, optflgs_t optflgs)
{
	clerrno_t clerrno = CL_NOERR;
	clerrno_t first_err = CL_NOERR;
	char clcmd_buf[BUFSIZ];
	int tmp_err = SCIP_NOERR;
	char **cli_envs = new char *[3];
	char **cli_args = new char *[3];
	int arg_count = 0;
	int env_count = 0;

	// Check inputs
	if (operands.getSize() > 1) {
		clcommand_perror(CL_EINTERNAL);
		first_err = CL_EINTERNAL;
		goto cleanup;
	}

	// The operand must be this cluster name
	if (operands.getSize() == 1) {
		char *operand_name = (char *)0;
		FILE *clconf_fp;
		char line_buf[SCCONF_MAXSTRINGLEN];
		char val_buf[SCCONF_MAXSTRINGLEN];

		operands.start();
		operand_name = operands.getValue();

		// Open the cluster configuration file.
		clconf_fp = fopen(CCR_CONF_FILE, "r");
		if (clconf_fp == NULL) {
			clerror("Cannot open file \"%s\".\n", CCR_CONF_FILE);
			if (first_err == CL_NOERR) {
				first_err = CL_EINTERNAL;
			}
			goto cleanup;
		}

		// Check cluster name
		while (fgets(line_buf, BUFSIZ, clconf_fp) != NULL) {
			if (sscanf(line_buf, "cluster.name %s", val_buf) != 1)
				continue;

			if (strcmp(operand_name, val_buf) != 0) {
				clerror("\"%s\" is not the name of this "
				    "cluster.\n", operand_name);
				if (first_err == CL_NOERR) {
					first_err = CL_ENOENT;
				}
				(void) fclose(clconf_fp);
				goto cleanup;
			}
			break;
		}
		(void) fclose(clconf_fp);
	}

	// Set the environement variables to CLI_SCPRIVIPADM
	cli_envs[env_count] = new char [strlen(cmd.commandname) + 6];
	(void) sprintf(cli_envs[env_count++], "PROG=%s",
	    cmd.commandname);
	if (cmd.isVerbose || (optflgs & vflg)) {
		cli_envs[env_count] =
		    new char [strlen("CL_VBOSE=y") + 1];
		(void) strcpy(cli_envs[env_count++], "CL_VBOSE=y");
	}
	cli_envs[env_count] = NULL;

	// Set the argvs
	cli_args[0] = new char [strlen(CLI_SCPRIVIPADM) + 1];
	cli_args[1] = new char [strlen("-R") + 1];
	(void) strcpy(cli_args[0], CLI_SCPRIVIPADM);
	(void) strcpy(cli_args[1], "-R");
	cli_args[2] = NULL;
	arg_count = 2;

	tmp_err = execve_command(CLI_SCPRIVIPADM, cli_args, cli_envs);
	if (tmp_err != SCIP_NOERR) {
		switch (tmp_err) {
		case SCIP_EOP:
			clerrno = CL_EOP;
			break;
		case SCIP_EUSAGE:
		case SCIP_EINVAL:
			clerrno = CL_EINVAL;
			break;
		case SCIP_EINTERNAL:
		default:
			clerrno = CL_EINTERNAL;
			break;
		}

		// Save the first error
		if (first_err == CL_NOERR) {
			first_err = clerrno;
		}
	}

cleanup:
	for (int i = 0; i < arg_count; i++) {
		delete [] cli_args[i];
	}
	delete [] cli_args;

	for (int i = 0; i < env_count; i++) {
		delete [] cli_envs[i];
	}
	delete [] cli_envs;

	return (first_err);
}
