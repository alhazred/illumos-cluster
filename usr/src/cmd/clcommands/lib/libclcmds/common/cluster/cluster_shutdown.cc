//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)cluster_shutdown.cc	1.12	09/02/08 SMI"

//
// Process cluster "shutdown"
//

#ifdef __cplusplus
#include <sys/os.h>
#endif

#include <sys/mnttab.h>
#include <sys/mount.h>
#include <sys/mntent.h>
#include <dc/libdcs/libdcs.h>
#include <scadmin/scswitch.h>
#include <scadmin/scconf.h>
#include <scadmin/scstat.h>
#include <sys/sc_syslog_msg.h>

#include "cluster.h"

#define	MNTOPT_SEPARATOR	','
#define	GDEV_PATH		"/global/.devices/node@"
#define	SHUTDOWN_USER_CMD	"/usr/sbin/init 0"
#define	WALL_CMD		"/usr/sbin/wall"
#define	RWALL_CMD		"/usr/sbin/rwall"

#if SOL_VERSION >= __s10
#include <zone.h>
#include <sys/cladm_int.h>
#include <sys/quorum_int.h>
#include <sys/clconf_int.h>
#include <orb/infrastructure/orb_conf.h>

#define	SHUTDOWN_ZC_CMD		"/usr/cluster/bin/clzonecluster halt +"
#endif // SOL_VERSION >= __s10

//
// The number of times we retry scswitch_evacuate_devices(), and how long
// (in seconds) we sleep in between each attempt. Refer to the comments below
// in main() for scswitch_evacuate_devices().
//
#define	EVACUATE_DEVICES_RETRY_LIMIT	5
#define	EVACUATE_DEVICES_RETRY_DELAY	5

//
// cl_shutdown_node_t holds the list of all cluster nodes and its status.
//
typedef struct cl_shutdown_node_struct {
	char				*nodename;
	nodeid_t			nodeid;
	int				online;
	struct cl_shutdown_node_struct	*next;
} cl_shutdown_node_t;

// Cluster name
static char *cluster_name = (char *)0;

// Variables that store the list of mountpoints from /etc/mnttab.
static char *mp_buf = NULL;
static int *mount_points = NULL;
static size_t curr_mp_pos = 0;

static clerrno_t cluster_get_shutdown_node(cl_shutdown_node_t **node_info);
static clerrno_t cluster_get_shutdown_nodes(cl_shutdown_node_t **nodes_info);
static void cluster_process_shutdown_grace(cl_shutdown_node_t *nodes_info,
    int grace_period, optflgs_t optflgs, char *std_message,
    bool *shutdown);
static clerrno_t cluster_shutdown_umountall(ClCommand &cmd);
static int cluster_appendtolist(const char *mount_point);
static clerrno_t cluster_shutdown_dotdevices_umountall(
    cl_shutdown_node_t *nodes_info, nodeid_t nodeid);
static clerrno_t cluster_shutdown_dsm(cl_shutdown_node_t *nodes_info);
static clerrno_t cluster_shutdown_set_shutdown(void);
static clerrno_t cluster_shutdown_nodes(cl_shutdown_node_t *my_node,
    cl_shutdown_node_t *nodes_info);
// static void cluster_shutdown_node(void *arg);
static void cluster_shutdown_free_node(cl_shutdown_node_t *node_info);

static void cluster_shutdown_send_msg(cl_shutdown_node_t *nodes_info,
    char *send_msg);
static int cluster_shutdown_send_wall_msg(char *msgbuf, nodeid_t nodeid);
static int cluster_shutdown_send_rwall_msg(char *msgbuf, nodeid_t nodeid);

#if SOL_VERSION >= __s10
static clerrno_t cluster_shutdown_zc(nodeid_t nodeid);
#endif // SOL_VERSION >= __s10

//
// cluster "shutdown"
//
clerrno_t
cluster_shutdown(ClCommand &cmd, ValueList &operands, optflgs_t optflgs,
    ushort_t int_grace_time, char *std_message)
{
	clerrno_t clerrno = CL_NOERR;
	char err_buf[SCCONF_MAXSTRINGLEN];
	char *operand_name = (char *)0;
	uint_t ismember;
	bool shutdown = false;
	int i, count;
	cl_shutdown_node_t *node_info = (cl_shutdown_node_t *)0;
	cl_shutdown_node_t *nodes_info = (cl_shutdown_node_t *)0;
	scha_errmsg_t scha_err = { SCHA_ERR_NOERR, NULL };
	scswitch_errbuff_t scswitch_err = { SCSWITCH_NOERR, NULL };
	boolean_t verbose;
#if SOL_VERSION >= __s10
	zoneid_t zoneid = getzoneid();
#endif

	if (optflgs & vflg)
		verbose = B_TRUE;
	else
		verbose = B_FALSE;

	// Check input
	if (operands.getSize() > 1) {
		clerrno = CL_EINTERNAL;
		clcommand_perror(clerrno);
		return (clerrno);
	}

	// Get cluster name
	if (clconf_lib_init() != 0) {
		clerror("Cannot initialize clconf library.\n");
		return (CL_EINTERNAL);
	}
	cluster_name = clconf_get_clustername();
	if (!cluster_name) {
		clerror("Cannot get the cluster name.\n");
		return (CL_EINTERNAL);
	}

	// If operand is specified, it must be the cluster name
	if (operands.getSize() == 1) {
		char *operand_name = (char *)0;
		operands.start();
		operand_name = operands.getValue();
		if (operand_name && (strcmp(operand_name, cluster_name) != 0)) {
			clerror("\"%s\" is not the name of this cluster.\n",
			    operand_name);
			return (CL_ENOENT);
		}
	}


	//  Get info of this node
	clerrno = cluster_get_shutdown_node(&node_info);
	if (clerrno != CL_NOERR) {
		clcommand_perror(clerrno);
		goto cleanup;
	}

	// Get info of all nodes
	clerrno = cluster_get_shutdown_nodes(&nodes_info);
	if (clerrno != CL_NOERR) {
		clcommand_perror(clerrno);
		goto cleanup;
	}

	// Sends shutdown messages to cluster nodes
	cluster_process_shutdown_grace(nodes_info, int_grace_time, optflgs,
	    std_message, &shutdown);

	// See if the user answered "yes" to the shutdown question.
	if (!shutdown) {
		clerrno = CL_EOP;
		goto cleanup;
	}

#if (SOL_VERSION >= __s10)
	//
	// Before we shut down the base cluster, we will gracefully shut
	// down all the zone clusters if there are any.
	//
	if (zoneid == GLOBAL_ZONEID) {
		clerrno = cluster_shutdown_zc(node_info->nodeid);
		if (clerrno != CL_NOERR) {
			clcommand_perror(clerrno);
			goto cleanup;
		}
	}
#endif // SOL_VERSION >= __s10

	// Shutdown all the RGs in the cluster.
	scha_err = scswitch_evacuate_rgs(NULL, 0, verbose, NULL);
	if (scha_err.err_code != SCSWITCH_NOERR) {
		char *errormessage = scha_err.err_msg ? scha_err.err_msg :
		    scha_strerror(scha_err.err_code);

		(void) fprintf(stderr, "%s:  %s\n", cmd.getCommandName(),
		    errormessage);
		clerror("Could not shutdown all resource groups in "
		    "the cluster.\n");

		if (scha_err.err_msg) {
			free(scha_err.err_msg);
		}

		clerrno = CL_EINTERNAL;
		goto cleanup;
	}

#if (SOL_VERSION >= __s10)
	if (zoneid != GLOBAL_ZONEID) {
		goto skip_global;
	}
#endif
	// Unmount all global filesystems in the cluster.
	clerrno = cluster_shutdown_umountall(cmd);
	if (clerrno != 0) {
		goto cleanup;
	}

	//
	// Evacuate all the dcs device groups in the cluster.
	//
	// It can actually take some time (several seconds) for a device group
	// to become idle even after all processes that were using it have
	// exited. This is attributed to the deferred processing of inactive,
	// checkpointing and unreference requests in the kernel. Therefore the
	// scswitch_evacuate_devices() function can fail (SCSWITCH_EBUSY),
	// even though nothing is using a device service anymore. We have no
	// way of telling (from up here in userland) if a busy device service
	// will soon be inactive, all we know is that it's currently busy. So
	// we retry scswitch_evacuate_devices() in a delay loop. Note that the
	// message printed when a device service is still busy is appended
	// with a dot on each retry so that the admin knows the command is
	// still processing (in case they're tempted to ctrl+C it).
	//
	for (count = 1; count <= EVACUATE_DEVICES_RETRY_LIMIT; count++) {
		scswitch_err = scswitch_evacuate_devices(NODEID_UNKNOWN);
		if (scswitch_err.err_code != SCSWITCH_EBUSY) {
			break;
		} else {
			if (count == 1) {
				(void) fprintf(stderr, gettext("Waiting "
				    "for devices services to become idle."));
			} else if (count == EVACUATE_DEVICES_RETRY_LIMIT) {
				(void) fprintf(stderr, ".\n");
				break;
			} else {
				(void) fprintf(stderr, ".");
			}

			free(scswitch_err.err_msg);
			(void) sleep((uint_t)EVACUATE_DEVICES_RETRY_DELAY);
		}
	}

	if (scswitch_err.err_code != SCSWITCH_NOERR) {
		if (scswitch_err.err_msg) {
			(void) fprintf(stderr, "%s:  %s\n",
			    cmd.getCommandName(), scswitch_err.err_msg);
			free(scswitch_err.err_msg);
			clerror("Could not shutdown device service in the "
			    "cluster.\n");
		} else {
			// Get the error messages for the error code
			*err_buf = '\0';
			scswitch_strerr(NULL, err_buf, scswitch_err.err_code);
			if (err_buf && *err_buf) {
				(void) fprintf(stderr, "%s:  %s\n",
				    cmd.getCommandName(), err_buf);
				clerror("Could not shutdown device service in "
				    "in the cluster.\n");
			}
		}
		clerrno = CL_EINTERNAL;
		goto cleanup;
	}

	// Umount other file systems
	clerrno = cluster_shutdown_dotdevices_umountall(nodes_info,
	    node_info->nodeid);
	if (clerrno != 0) {
		goto cleanup;
	}

	// Shutdown the DSM on all cluster nodes.
	clerrno = cluster_shutdown_dsm(nodes_info);
	if (clerrno != CL_NOERR) {
		goto cleanup;
	}

skip_global:
	// Signal cluster shutdown on all nodes
	clerrno = cluster_shutdown_set_shutdown();
	if (clerrno != 0) {
		goto cleanup;
	}

	// Execute "init 0" on all cluster nodes.
	clerrno = cluster_shutdown_nodes(node_info, nodes_info);
	if (clerrno != CL_NOERR) {
		goto cleanup;
	}

cleanup:
	cluster_shutdown_free_node(node_info);
	cluster_shutdown_free_node(nodes_info);

	return (clerrno);
}

//
// cluster_shutdown_dsm
//
//	Disable the DSMs on all cluster nodes.
//
clerrno_t
cluster_shutdown_dsm(cl_shutdown_node_t *nodes_info)
{
	clerrno_t clerrno = CL_NOERR;
	cl_shutdown_node_t *node = NULL;
	cl_shutdown_node_t *node2 = NULL;
	nodeid_t tmpid;

	if (!nodes_info) {
		clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}

	// Walk the list of active nodes, disabling the DSM on each one.
	node = nodes_info;
	while (node != NULL) {
		if (node->online == 0) {
			node = node->next;
			continue;
		}
		if (dcs_disable_dsm(node->nodeid) != DCS_SUCCESS) {
			clerror("Cannot disable device service on node "
			    "\"%s\".\n", node->nodename);
			if (clerrno == CL_NOERR) {
				clerrno = CL_EOP;
			}
			//
			// Failure to disable one of the DSMs.  Enable all
			// the ones we've disabled so far, and then return with
			// the error.
			//
			node2 = nodes_info;
			while (node2 != node) {
				(void) dcs_enable_dsm(node->nodeid);
				node2 = node2->next;
			}
			break;
		}
		node = node->next;
	}

	return (clerrno);
}

#if SOL_VERSION >= __s10
//
// Gracefully halt all the zone clusters in the base cluster.
//
clerrno_t
cluster_shutdown_zc(nodeid_t nodeid)
{
	clconf_errnum_t err;
	clerrno_t clerrno = CL_NOERR;

	err = clconf_do_execution(SHUTDOWN_ZC_CMD, nodeid,
	    NULL, B_FALSE, B_TRUE, B_TRUE);
	if (err != CL_NOERROR) {
		//
		// Error executing the shutdown command.
		//
		if (err == CL_INVALID_OBJ) {
			clerror("Error halting node."
			    " Clexecd not found on node %d.\n", nodeid);
			clerrno = CL_EOP;
		} else {
			clerror("Error halting zone clusters. Unexpected "
			    "internal error %d.\n", err);
			clerrno = CL_EINTERNAL;
		}
	}
	return (clerrno);
}
#endif // SOL_VERSION >= __s10


//
// cluster_shutdown_node
//
//	 This call should be passed in a 'nodeid_t' casted as a 'void *'.
//
void *
cluster_shutdown_node(void *arg)
{
	clconf_errnum_t err;
	cl_shutdown_node_t *node = (cl_shutdown_node_t *)arg;
	clerrno_t clerrno = CL_NOERR;

	//
	// This call is made in a separate thread, so we need a different
	// buffer for the error string.
	//

	err = clconf_do_execution(SHUTDOWN_USER_CMD, node->nodeid,
	    NULL, B_FALSE, B_TRUE, B_TRUE);
	if (err != CL_NOERROR) {
		//
		// Error executing the init command on one of the nodes.
		// It's too late to stop, so print out an error message
		// and continue.
		//
		if (err == CL_NO_CLUSTER) {
			clerror("Node \"%s\" is not in cluster \"%s\".\n",
			    node->nodename, cluster_name);
			clerrno = CL_EOP;
		} else if (err == CL_INVALID_OBJ) {
			clerror("Cannot find clexecd on node \"%s\".\n",
			    node->nodename);
			clerrno = CL_EOP;
		} else {
			clerror("Unexpected error on node \"%s\".\n",
			    node->nodename);
			clerrno = CL_EINTERNAL;
		}
	}

	return ((void *)clerrno);
}

//
// cluster_shutdown_nodes
//
//	Shutdown all nodes in the cluster. The current node is always
//	shutdown last.
//
clerrno_t
cluster_shutdown_nodes(cl_shutdown_node_t *my_node,
    cl_shutdown_node_t *nodes_info)
{
	int err;
	cl_shutdown_node_t *node = NULL;
	clerrno_t clerrno = CL_NOERR;
	thread_t tid;

	// Check inputs
	if (!my_node || !nodes_info) {
		clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}

	// Walk the list of active nodes, and call "init 0" on each one.
	node = nodes_info;
	while (node != NULL) {
		if (node->online == 0) {
			node = node->next;
			continue;
		}

		// Skip the node this command is running on - do that last.
		if (node->nodeid == my_node->nodeid) {
			node = node->next;
			continue;
		}
		if (thr_create(NULL, NULL, cluster_shutdown_node, (void *)node,
		    NULL, &tid) != 0) {
			//
			// Error creating a thread to shutdown one of the
			// remote nodes.  It's too late to stop, so print out
			// an error message and continue.
			//
			clerror("Cannot create a thread to halt node \"%s\".\n",
			    node->nodename);
			if (clerrno == CL_NOERR) {
				clerrno = CL_EOP;
			}
		}
		node = node->next;
	}

	// Now its time to do me.
	err = (int)cluster_shutdown_node((void *)my_node);
	if ((err != 0) && (clerrno == CL_NOERR)) {
		clerrno = CL_EOP;
	}

	return (clerrno);
}

//
// cluster_appendtolist
//
//	Helper function used to to build a list of mountpoints to unmount.
//	Returns 0 if successful, and 1 if out of memory.
//
static int
cluster_appendtolist(const char *mount_point)
{
	static size_t mp_buf_increment = 1024;
	static size_t curr_mp_buf_pos = 0;
	static size_t max_mp_buf_pos = 0;
	static size_t mp_increment = 50;
	static size_t max_mp_pos = 0;

	size_t len;
	void *tmp;

	ASSERT(curr_mp_buf_pos <= max_mp_buf_pos);
	ASSERT(curr_mp_pos <= max_mp_pos);

	len = strlen(mount_point) + 1;

	// Confirm that we have enough memory in 'mp_buf'.
	if ((curr_mp_buf_pos + len) > max_mp_buf_pos) {
		if (len > mp_buf_increment) {
			max_mp_buf_pos += len;
		} else {
			max_mp_buf_pos += mp_buf_increment;
		}
		tmp = realloc((void *)mp_buf, max_mp_buf_pos * sizeof (char));
		if (tmp == NULL) {
			goto nomem;
		}
		mp_buf = (char *)tmp;
	}

	// Confirm that we have enough memory in 'mount_points'.
	if (curr_mp_pos >= max_mp_pos) {
		max_mp_pos += mp_increment;
		tmp = realloc((void *)mount_points, max_mp_pos * sizeof (int));
		if (tmp == NULL) {
			goto nomem;
		}
		mount_points = (int *)tmp;
	}

	// Store the new mount point at the end
	(void) strcpy(mp_buf + curr_mp_buf_pos, mount_point);
	mount_points[curr_mp_pos++] = (int)curr_mp_buf_pos;
	curr_mp_buf_pos += len;

	return (0);

nomem:
	// Error case - clean up and return.
	free(mp_buf);
	free(mount_points);
	mp_buf = NULL;
	mount_points = NULL;
	curr_mp_buf_pos = max_mp_buf_pos = curr_mp_pos = max_mp_pos = 0;
	return (1);
}

//
// cluster_shutdown_umountall
//
//	Unmount all global filesystems.
//
clerrno_t
cluster_shutdown_umountall(ClCommand &cmd)
{
	FILE *fp;
	struct mnttab mnt;
	clerrno_t clerrno = CL_NOERR;
	int tmp_err;
	int error_code;
	char *tmp;
	int i;
	size_t len = strlen(GDEV_PATH);

	// Open /etc/mnttab in read-only mode.
	fp = fopen("/etc/mnttab", "r");
	if (fp == NULL) {
		clerror("Cannot open \"/etc/mnttab\".\n");
		clerrno = CL_EIO;
		goto cleanup;
	}

	// Iterate through entries in mnttab.
	for (;;) {
		tmp_err = getmntent(fp, &mnt);
		if (tmp_err != 0) {
			if (tmp_err != -1) {
				clerror("Cannot open \"/etc/mnttab\".\n");
				clerrno = CL_EIO;
				goto cleanup;
			}
			break;
		}

		//
		// Look for the occurence of "global" in the mount
		// option string.
		//
		tmp = mnt.mnt_mntopts;
		while ((tmp = strstr(tmp, MNTOPT_GLOBAL)) != NULL) {
			//
			// Is this the "global" mount option or some
			// other mount option that contains "global" as
			// a substring?
			//
			if ((tmp != mnt.mnt_mntopts) &&
			    (*(tmp - 1) != MNTOPT_SEPARATOR)) {
				tmp += (sizeof (MNTOPT_GLOBAL) - 1);
				continue;
			}

			tmp += (sizeof (MNTOPT_GLOBAL) - 1);
			if ((*tmp != MNTOPT_SEPARATOR) && (*tmp != 0)) {
				continue;
			}

			if (strncmp(GDEV_PATH, mnt.mnt_mountp, len) == 0) {
				//
				// Skip all the /global/.devices/node@ mounts
				// so that retries of 'scshutdown' will succeed.
				//
				break;
			}

			if (cluster_appendtolist(mnt.mnt_special) != 0) {
				clcommand_perror(CL_ENOMEM);
				goto cleanup;
			};
		}
	}

	//
	// Now that all the entries are in 'mount_points', we unmount them in
	// reverse order - this way, we can handle nested mounts. If an
	// unmount fails because the filesystem is busy (EBUSY), then the
	// the unmount is retried using the force flag (MS_FORCE).
	//
	for (i = (int)curr_mp_pos-1; i >= 0; i--) {
		tmp_err = umount(mp_buf + mount_points[i]);
		if (tmp_err == -1) {
			switch (errno) {
			case EINVAL:
				// Skip invalid entry in /etc/mnttab.
				continue;
			case EBUSY:
				clwarning("Using force option to unmount "
				    "\"%s\".\n",
				    mp_buf + mount_points[i]);
				tmp_err = umount2(mp_buf + mount_points[i],
				    MS_FORCE);
				if (tmp_err == 0) {
					continue;
				}
				// fall through
			default:
				if (tmp_err < 0) {
					clerrno = CL_EOP,
					clerror("Cannot umount\"%s\".\n",
					    mp_buf + mount_points[i]);
					goto cleanup;
				}
				break;
			}
		}
	}

cleanup:
	free(mp_buf);
	free(mount_points);

	return (clerrno);
}

//
// cluster_shutdown_dotdevices_umountall
//
//	Umount all the /global/.devices filesystems in the cluster.
//
clerrno_t
cluster_shutdown_dotdevices_umountall(cl_shutdown_node_t *nodes_info,
    nodeid_t nodeid)
{
	clerrno_t clerrno = CL_NOERR;
	clerrno_t first_err = CL_NOERR;
	int tmp_err;
	char gdevices_mnt[100];
	cl_shutdown_node_t *node = NULL;

	//
	// The other unmounts succeeded - try unmounting all the
	// /global/.devices filesystems in the cluster.  We do this step as
	// stage 2 because these filesystems need to exist for retries
	// of 'scshutdown' from other nodes to work.
	//
	node = nodes_info;
	while (node != NULL) {
		// Skip dead nodes, and the node we are on.
		if ((node->online == 0) || (node->nodeid == nodeid)) {
			node = node->next;
			continue;
		}
		(void) sprintf(gdevices_mnt, "%s%d", GDEV_PATH, node->nodeid);
		tmp_err = umount(gdevices_mnt);
		if (tmp_err == -1) {
			switch (errno) {
				case EINVAL:
					// Invalid entry in /etc/mnttab. Go on.
					node = node->next;

					continue;
				case EBUSY:
					clwarning("Using force option "
					    "to unmount \"%s\".\n",
					    gdevices_mnt);
					tmp_err = umount2(gdevices_mnt,
					    MS_FORCE);
					// fall through
				default:
					if (tmp_err < 0) {
						first_err = CL_EOP;
						clerror("Cannot umount "
						    "\"%s\" - %s.\n",
						    gdevices_mnt,
						    strerror(errno));
						goto cleanup;
					}
					break;
			}
		}
		node = node->next;
	}

	//
	// Unmount the local /global/.devices filesystem - this has to be last
	// because we need the device nodes to exist for the other unmounts to
	// succeed.
	//
	(void) sprintf(gdevices_mnt, "%s%d", GDEV_PATH, nodeid);
	tmp_err = umount(gdevices_mnt);
	if (tmp_err == -1) {
		switch (errno) {
			case EINVAL:
				// Invalid entry in /etc/mnttab. Return normally
				return (0);
			case EBUSY:
				clwarning(
				    "Using force option to unmount \"%s\".\n",
				    gdevices_mnt);
				tmp_err = umount2(gdevices_mnt, MS_FORCE);
				// fall through
			default:
				if (tmp_err < 0) {
					first_err = CL_EOP;
					clerror("Cannot umount \"%s\" - %s.\n",
					    gdevices_mnt,
					    strerror(errno));
					goto cleanup;
				}
				break;
		}
	}

cleanup:
	if (first_err != CL_NOERR) {
		clerrno = first_err;
	}

	return (clerrno);
}

//
// cluster_process_shutdown_grace
//
//	Sends shutdown related messages after a regular interval, and asks
//	for approval to continue with the shutdown.
//
void
cluster_process_shutdown_grace(cl_shutdown_node_t *nodes_info,
    int grace_period, optflgs_t optflgs, char *std_message,
    bool *shutdown)
{
	int gracetable[] = {
		7200, 3600, 1800, 1200, 600, 300, 120, 60, 30, 10};
	char buftime[BUFSIZ];
	int hours, minutes, seconds, ti, i;
	char *rptr;
	char replybuf[BUFSIZ];
	size_t sz = 0;
	char send_msg[BUFSIZ];

	hours = minutes = seconds = ti = i = 0;
	for (ti = gracetable[i]; i < 10; i++, ti = gracetable[i]) {
		if (grace_period > ti) {
			hours = grace_period / 3600;
			minutes = grace_period % 3600 / 60;
			seconds = grace_period % 60;
			buftime[0] = '\0';

			if (hours > 1) {
				(void) sprintf(buftime, "%d hours", hours);
			} else if (hours == 1) {
				(void) sprintf(buftime, "%d hour", hours);
			}

			if (minutes > 1) {
				(void) sprintf(buftime, "%s %d minutes",
				    buftime, minutes);
			} else if (minutes == 1) {
				(void) sprintf(buftime, "%s %d minute",
				    buftime, minutes);
			}

			if ((hours == 0) && (seconds > 0)) {
				if (seconds == 1) {
					(void) sprintf(buftime, "%s %d second",
					    buftime, seconds);
				} else {
					(void) sprintf(buftime,
					    "%s %d seconds", buftime, seconds);
				}
			}

			(void) sprintf(send_msg,
			    "Cluster \"%s\" will be shutdown in %s.\n",
			    cluster_name, buftime);
			if (std_message) {
				(void) strcat(send_msg, std_message);
				(void) strcat(send_msg, "\n\n");
			}

			cluster_shutdown_send_msg(nodes_info, send_msg);
			(void) sleep((uint_t)(grace_period - ti));
			grace_period = ti;
		}
	}

	// Set the default
	*shutdown = false;
	if (!(optflgs & yflg)) {
		// Mimic interface look of the Solaris shutdown(1m) command
		if (strcmp(gettext("yes"), "yes") == 0 &&
		    strcmp(gettext("no"), "no") == 0) {
			(void) fprintf(stderr,
			    gettext("Do you want to continue? (y or n):   "));
			rptr = fgets(replybuf, sizeof (replybuf) - 1, stdin);
			if (rptr) {
				sz = strlen(replybuf) - 1;
				if (strncasecmp(replybuf, "y", sz) == 0 ||
				    strncasecmp(replybuf, "yes", sz) == 0) {
					*shutdown = true;
				}
			}
		} else {
			(void) fprintf(stderr,
			    gettext("Do you want to continue? (%s or %s):   "),
			    gettext("yes"), gettext("no"));
			rptr = fgets(replybuf, sizeof (replybuf) - 1, stdin);
			if (rptr) {
				while (sz < BUFSIZ && replybuf[sz] != '\n')
					sz++;
				if (strncasecmp(replybuf, gettext("yes"), sz)
				    == 0) {
					*shutdown = true;
				}
			}
		}
		if (*shutdown == false) {
			(void) sprintf(send_msg, "Shutdown aborted.\n");
			cluster_shutdown_send_msg(nodes_info, send_msg);
		}
	} else {
		*shutdown = true;
	}
}

//
// cluster_get_shutdown_node
//
//	Upon success, an object of the cl_shutdown_node_t is returned.
//	It is the caller's  responsibility to free it.
//
clerrno_t
cluster_get_shutdown_node(cl_shutdown_node_t **ppnodeinfo)
{
	clerrno_t clerrno = CL_NOERR;
	scconf_errno_t scconf_err = SCCONF_NOERR;
	cl_shutdown_node_t *pnodeinfo = NULL;
	scconf_nodeid_t nodeid;
	char *nodename = NULL;

	// Check inputs
	if (ppnodeinfo == NULL) {
		clerrno = CL_EINTERNAL;
		goto cleanup;
	}

	// Allocate memory
	pnodeinfo = (cl_shutdown_node_t *)calloc(1,
	    sizeof (cl_shutdown_node_t));
	if (pnodeinfo == NULL) {
		clerrno = CL_ENOMEM;
		goto cleanup;
	}
	pnodeinfo->next = NULL;
	pnodeinfo->online = 1;

	// Get node
	scconf_err = scconf_get_nodeid(NULL, &nodeid);
	if (scconf_err != SCCONF_NOERR) {
		clerrno = cluster_conv_scconf_err(0, scconf_err);
		goto cleanup;
	}
	pnodeinfo->nodeid = nodeid;

	// Get node name
	scconf_err = scconf_get_nodename(nodeid, &nodename);
	if (scconf_err != SCCONF_NOERR) {
		clerrno = cluster_conv_scconf_err(0, scconf_err);
		goto cleanup;
	}
	pnodeinfo->nodename = strdup(nodename);
	if (pnodeinfo->nodename == NULL) {
		clerrno = CL_ENOMEM;
		goto cleanup;
	}

	*ppnodeinfo = pnodeinfo;

cleanup:
	if (nodename)
		free(nodename);

	return (clerrno);
}

//
// cluster_get_shutdown_nodes
//
//	Upon success, an object of cl_shutdown_node_t is returned.
//	The caller is responsible for freeing the space.
//
clerrno_t
cluster_get_shutdown_nodes(cl_shutdown_node_t **cl_shutdown_nodes)
{
	quorum_status_t *quorum_status = (quorum_status_t *)0;
	clerrno_t clerrno = CL_NOERR;
	int i;
	char *node_name = (char *)0;
	clconf_cluster_t *clconf = (clconf_cluster_t *)0;
	scconf_errno_t scconf_err = SCCONF_NOERR;
	cl_shutdown_node_t *pnode = NULL;
	cl_shutdown_node_t *pnode_current = NULL;

#if (SOL_VERSION >= __s10)
	int ret_val;
	uint_t clid;
	char cl_name[ZONENAME_MAX];
	quorum_status_t *clust_memb = NULL;
	char *nodenamep = NULL;
	zoneid_t zoneid;
	clconf_cluster_t *clp = NULL;
#endif

	// Check inputs
	if (cl_shutdown_nodes == NULL) {
		clerrno = CL_EINTERNAL;
		goto cleanup;
	}

// Code for zone support
#if (SOL_VERSION >= __s10)
	zoneid = getzoneid();
	if (zoneid != GLOBAL_ZONEID) {
		if (clconf_lib_init() != 0) {
			(void) fprintf(stderr, "Failed to initialize clconf\n");
			clerrno = SCSWITCH_EUNEXPECTED;
			goto cleanup;
		}

		if (getzonenamebyid(zoneid, cl_name, ZONENAME_MAX) == -1) {
			(void) fprintf(stderr, "Failed to get cluster name\n");
			clerrno = SCSWITCH_EUNEXPECTED;
			goto cleanup;
		}

		ret_val = clconf_get_cluster_id(cl_name, &clid);
		if (ret_val == ENOENT) {
			/*
			 * For a non-cluster-wide zone, we exit much earlier.
			 */
			ASSERT(0);
			clerrno = SCSWITCH_ENOCLUSTER;
			goto cleanup;
		} else if (ret_val == EACCES) {
			/*
			 * We should be able to get the current cluster id.
			 */
			ASSERT(0);
			clerrno = SCSWITCH_EUNEXPECTED;
			goto cleanup;
		} else if (ret_val != 0) {
			/* Unexpected error */
			ASSERT(0);
			clerrno = SCSWITCH_EUNEXPECTED;
			goto cleanup;
		}

		/* get the clconf object */
		clp = clconf_cluster_get_current();
		if (clp == NULL) {
			clerrno = SCSWITCH_EUNEXPECTED;
			goto cleanup;
		}

		if (cluster_get_quorum_status(&clust_memb) != 0) {
			clerrno = SCSWITCH_EUNEXPECTED;
			goto cleanup;
		}

		for (i = 0; i < clust_memb->num_nodes; i++) {
			pnode = (cl_shutdown_node_t *)calloc(1,
				sizeof (cl_shutdown_node_t));
			if (pnode == NULL) {
				clerrno = CL_ENOMEM;
				goto cleanup;
			}

			/* Get nodename. */
			nodenamep =
				(char *)clconf_cluster_get_nodename_by_nodeid(
				clp, clust_memb->nodelist[i].nid);
			if (nodenamep == NULL) {
				clerrno = SCSWITCH_EUNEXPECTED;
				goto cleanup;
			}

			pnode->nodename = strdup(nodenamep);
			if (pnode->nodename == NULL) {
				clerrno = CL_ENOMEM;
				goto cleanup;
			}

			pnode->nodeid = clust_memb->nodelist[i].nid;
			if (clust_memb->nodelist[i].state
				== QUORUM_STATE_ONLINE) {
				pnode->online = 1;
			} else {
				pnode->online = 0;
			}

			/* append it to the list */
			if (pnode_current != NULL) {
				pnode_current->next = pnode;
			} else { /* first element */
				*cl_shutdown_nodes = pnode;
			}
			pnode_current = pnode;
			pnode = NULL; /* so we don't accidentally free it */
		}
		cluster_release_quorum_status(clust_memb);
		goto cleanup;
	}
#endif
// end of code for zone support

	// Get the clconf
	if (scconf_cltr_openhandle((scconf_cltr_handle_t *)&clconf) !=
	    SCCONF_NOERR) {
		clerrno = CL_EINTERNAL;
		goto cleanup;
	}

	// Get the quorum status
	if (cluster_get_quorum_status(&quorum_status) != 0) {
		clerrno = CL_EINTERNAL;
		goto cleanup;
	}

	// Go through the nodes in quorum status
	for (i = 0; i < quorum_status->num_nodes; i++) {
		// Get node name
		scconf_err = scconf_get_nodename(
		    quorum_status->nodelist[i].nid, &node_name);
		if (scconf_err != SCCONF_NOERR) {
			clerrno = cluster_conv_scconf_err(0, scconf_err);
			goto cleanup;
		}

		// Allocate space
		pnode = (cl_shutdown_node_t *)calloc(1,
		    sizeof (cl_shutdown_node_t));
		if (pnode == NULL) {
			clerrno = CL_ENOMEM;
			goto cleanup;
		}

		// Save the nodename, node id, and status
		pnode->nodename = strdup(node_name);
		if (pnode->nodename == NULL) {
			clerrno = CL_ENOMEM;
			goto cleanup;
		}
		pnode->nodeid = quorum_status->nodelist[i].nid;
		if (quorum_status->nodelist[i].state == QUORUM_STATE_ONLINE) {
			pnode->online = 1;
		} else {
			pnode->online = 0;
		}

		// Append it to the list
		if (pnode_current != NULL) {
			pnode_current->next = pnode;
		} else {
			*cl_shutdown_nodes = pnode;
		}
		pnode_current = pnode;
	}

cleanup:
	/* free space */
#if (SOL_VERSION >= __s10)
	if (clp != NULL) {
		clconf_obj_release((clconf_obj_t *)clp);
	}
#endif
	if (quorum_status) {
		cluster_release_quorum_status(quorum_status);
	}
	return (clerrno);
}

//
// cluster_shutdown_free_node
//
//	Free cl_shutdown_node_t.
//
void
cluster_shutdown_free_node(cl_shutdown_node_t *node_info)
{
	cl_shutdown_node_t *pnode = node_info;
	cl_shutdown_node_t *pnode_next;

	if (pnode == NULL) {
		return;
	}

	while (pnode) {
		pnode_next = pnode->next;
		free(pnode->nodename);
		free(pnode);
		pnode = pnode_next;
	}
}

//
// cluster_shutdown_send_msg
//
//	Send shutdown start message at regular interval to all the nodes
//	in the cluster.
//
//	gettext on message string sent to all the nodes in cluster is not being
//	done here, because other machines outside the cluster can nfs mount and
//	if locale is set on the cluster node then the same message will be sent
//	out to all the machines.
//
void
cluster_shutdown_send_msg(cl_shutdown_node_t *nodes_info, char *send_msg)
{
	cl_shutdown_node_t *pnode = NULL;

	if (!nodes_info || !send_msg) {
		return;
	}

	// Go through the nodes
	pnode = nodes_info;
	while (pnode != NULL) {
		// Skip node that is already offline.
		if (pnode->online == 0) {
			pnode = pnode->next;
			continue;
		}
		if (cluster_shutdown_send_wall_msg(send_msg,
		    pnode->nodeid) != 0) {
			clerror("Cannot execute \"%s\" on node \"%s\".\n",
			    WALL_CMD, pnode->nodename);
		}
		pnode = pnode->next;
	}

	pnode = nodes_info;
	while (pnode != NULL) {
		if (cluster_shutdown_send_rwall_msg(send_msg,
		    pnode->nodeid) != 0) {
			clerror("Cannot execute \"%s\" on node \"%s\".\n",
			    RWALL_CMD, pnode->nodename);
		}
		pnode = pnode->next;
	}
}

//
// cluster_shutdown_send_wall_msg
//
//	Build message to be sent to all the m/c's which are nfs mounted on the
//	cluster node and send the messages.
//
int
cluster_shutdown_send_wall_msg(char *msgbuf, nodeid_t nodeid)
{
	char cmdbuf[BUFSIZ];

	(void) sprintf(cmdbuf, "%s -a <<-!\n %s", WALL_CMD, msgbuf);

	// Send message to the node
	return (clconf_do_execution(cmdbuf, nodeid, NULL, B_FALSE,
	    B_TRUE, B_TRUE));
}

//
// cluster_shutdown_send_rwall_msg
//
//	Build message to be sent to all the m/c's which are nfs mounted on the
//	cluster node and send the messages.
//
int
cluster_shutdown_send_rwall_msg(char *msgbuf, nodeid_t nodeid)
{
	char cmdbuf[BUFSIZ];

	(void) sprintf(cmdbuf,
	    "if [ -x /usr/sbin/showmount -a -x /usr/sbin/rwall ]\n"
	    "then\n"
	    "	remotes=`/usr/sbin/showmount`\n"
	    "	if [ -n \"$remotes\" ]\n"
	    "	then\n"
	    "		/usr/sbin/rwall ${remotes} <<-!\n%s\n!\n"
	    "	fi\n"
	    "fi\n",
	    msgbuf);

	// Send message to the node
	return (clconf_do_execution(cmdbuf, nodeid, NULL, B_FALSE,
	    B_TRUE, B_TRUE));
}

//
// cluster_shutdown_set_shutdown
//
//	If running in global zone, then set the cluster shutdown flag
//	on the CMM of every node.
//	If running in a zone cluster, request the local node's ZCMM
//	to start the zone cluster's membership shutdown.
//
clerrno_t
cluster_shutdown_set_shutdown(void)
{
	nodeid_t i;
	clerrno_t clerrno = CL_NOERR;
	int error;

#if (SOL_VERSION >= __s10)
	if (getzoneid() != GLOBAL_ZONEID) {
		//
		// If we are running in a native non-global zone,
		// then we would have exited much earlier
		// as 'cluster' command is not allowed to run
		// in a native non-global zone.
		// So we can assume that we are in a zone cluster
		// if we reach here.
		// Tell the local node's ZCMM to start the zone cluster's
		// membership shutdown.
		// If called in a zone cluster, clconf_cluster_shutdown()
		// talks to the local node's ZCMM, and hence ignores
		// any nodeid argument passed to it.
		// So we pass a 0 as the nodeid.
		//
		error = clconf_cluster_shutdown(0);

		//
		// Since we are talking to the local node's ZCMM,
		// we do not expect an ENOENT or EACCES error;
		// so we regard those errors as internal errors.
		//
		if (error != 0) {
			switch (error) {
			case EEXIST:
				clerror("Cluster shutdown is already in "
				    "process.\n");
				clerrno = CL_EOP;
				break;
			case EIO:
				clerror("Exception  raised while communicating "
				    "with the node %d.\n",
				    orb_conf::local_nodeid());
				clerrno = CL_EINTERNAL;
				break;
			default:
				clcommand_perror(CL_EINTERNAL);
				clerrno = CL_EINTERNAL;
				break;
			}
		}

		return (clerrno);
	}
#endif	// (SOL_VERSION >= __s10)

	// Signal cluster shutdown on all nodes
	for (i = 1; i <= NODEID_MAX; i++) {
		error = clconf_cluster_shutdown(i);
		if ((error != 0) && (error != ENOENT)) {
			switch (error) {
			case EEXIST:
				clerror("Cluster shutdown is already in "
				    "process.\n");
				clerrno = CL_EOP;
				break;
			case EIO:
				clerror("Exception  raised while communicating "
				    "with the node %d.\n", i);
				clerrno = CL_EINTERNAL;
				break;
			case EACCES:
				clerror("Cluster upgrade is in process.\n");
				clerrno = CL_EOP;
				break;
			default:
				clcommand_perror(CL_EINTERNAL);
				clerrno = CL_EINTERNAL;
				break;
			}

			break;
		}
	}

	return (clerrno);
}
