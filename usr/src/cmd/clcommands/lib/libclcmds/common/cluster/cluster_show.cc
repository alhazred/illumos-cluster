//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)cluster_show.cc	1.13	09/01/16 SMI"

//
// Process cluster "show"
//

#include "cluster.h"
#include "claccess.h"
#include "clnode.h"
#include "clquorum.h"
#include "clnasdevice.h"
#include "clsnmpmib.h"
#include "clsnmphost.h"
#include "clsnmpuser.h"
#include "clinterconnect.h"
#include "libdid.h"

#if SOL_VERSION >= __s10
#include <zone.h>
#include <sys/cladm_int.h>
#include <sys/quorum_int.h>
#include <sys/clconf_int.h>
#endif

static bool
add_valid_cltype(char *cl_type, ValueList &valid_cltypes);
static clerrno_t
cluster_add_node_child(ClShow *clshow_parent, char *cl_type);
static clerrno_t
cluster_add_node_subtype(ClShow *clshow_parent, char *cl_type);

//
// cluster "show"
//
clerrno_t
cluster_show(ClCommand &cmd, ValueList &operands, optflgs_t optflgs,
    ValueList &cltypes)
{
	clerrno_t clerrno = CL_NOERR;
	clerrno_t first_err = CL_NOERR;
	int i;
	bool do_all = false;
	bool has_nodetype = false;
	bool zone_support = false;
	ValueList valid_cltypes = ValueList(true);
	ClShow *lev1_parent = (ClShow *)0;
	ClShow *lev2_parent = (ClShow *)0;
	ClShow *obj_show1 = (ClShow *)0;
	ClShow *obj_show2 = (ClShow *)0;
#ifdef WEAK_MEMBERSHIP
	ClShow *obj_show3 = (ClShow *)0;
#endif
	ClShow *lev1_sibling = (ClShow *)0;
	char *lev1_parent_name = (char *)0;
	int child_num = 0;
	int count = 0;

	// Initialize clconf.
	if (clconf_lib_init() != 0) {
		clerror("Cannot initialize clconf library.\n");
		clerrno = CL_EINTERNAL;
	}

	// Check cluster name
	// The operand must be this cluster name
	if (operands.getSize() == 1) {
		char *operand_name = (char *)0;

		operands.start();
		operand_name = operands.getValue();
		clerrno = cluster_name_compare(operand_name);
		if (clerrno != CL_NOERR) {
			goto cleanup;
		}
	}

#if (SOL_VERSION >= __s10)
	if (getzoneid() != GLOBAL_ZONEID) {
		zone_support = true;
	}
#endif
	// Show all components?
	if (cltypes.getSize() == 0) {
		do_all = true;
		has_nodetype = true;
	} else {
		// Check for valid types
		for (cltypes.start(); !cltypes.end(); cltypes.next()) {
			char *cl_type = cltypes.getValue();

			if (!cl_type ||
			    (strcasecmp(cl_type, TYPE_RS) == 0) ||
			    (strcasecmp(cl_type, STYPE_RS) == 0) ||
			    (strcasecmp(cl_type, TYPE_RSLH) == 0) ||
			    (strcasecmp(cl_type, STYPE_RSLH) == 0) ||
			    (strcasecmp(cl_type, TYPE_RSSA) == 0) ||
			    (strcasecmp(cl_type, STYPE_RSSA) == 0))
				continue;

			if (!add_valid_cltype(cl_type, valid_cltypes)) {
				clerror("Unknown object type \"%s\".\n",
		cl_type);
				if (first_err == CL_NOERR) {
					first_err = CL_ETYPE;
				}
			}
		}
		// "rg" contains "rs", and "rs" contains rsls and rssa
		if (cltypes.isValue(TYPE_RS) ||
		    cltypes.isValue(STYPE_RS)) {
			if (!cltypes.isValue(TYPE_RG) &&
			    !cltypes.isValue(STYPE_RG)) {
				(void) add_valid_cltype(TYPE_RS,
				    valid_cltypes);
			}
		}
		if (cltypes.isValue(TYPE_RSSA) ||
		    cltypes.isValue(STYPE_RSSA)) {
			if (!cltypes.isValue(TYPE_RG) &&
			    !cltypes.isValue(STYPE_RG) &&
			    !cltypes.isValue(TYPE_RS) &&
			    !cltypes.isValue(STYPE_RS)) {
				(void) add_valid_cltype(TYPE_RSSA,
				    valid_cltypes);
			}
		}
		if (cltypes.isValue(TYPE_RSLH) ||
		    cltypes.isValue(STYPE_RSLH)) {
			if (!cltypes.isValue(TYPE_RG) &&
			    !cltypes.isValue(STYPE_RG) &&
			    !cltypes.isValue(TYPE_RS) &&
			    !cltypes.isValue(STYPE_RS)) {
				(void) add_valid_cltype(TYPE_RSLH,
				    valid_cltypes);
			}
		}

		// If nothing to show?
		if (valid_cltypes.getSize() == 0) {
			goto cleanup;
		}

		// Is type "node" specified? Needs special treatment for node.
		if (valid_cltypes.isValue(TYPE_NODE)) {
			has_nodetype = true;
		}

		// Special processing for adapter ClShow
		if (valid_cltypes.isValue(TYPE_INTR)) {
			valid_cltypes.add(TYPE_INTRADAPTER);
		}
	}

	// Count the number of children if node is specified.
	if (has_nodetype && !do_all) {
		if (!zone_support) {
			for (i = 0; cl_objtypes[i].obj_name; i++) {
				if ((cl_objtypes[i].obj_level == 2) &&
					valid_cltypes.isValue(
					cl_objtypes[i].obj_name)) {
					child_num++;
				}
			}
		} else {
			for (i = 0; zc_objtypes[i].obj_name; i++) {
				if ((zc_objtypes[i].obj_level == 2) &&
					valid_cltypes.isValue(
					zc_objtypes[i].obj_name)) {
					child_num++;
				}
			}
		}
	}

	// Go through all cluster components
	for (i = 0; !zone_support && cl_objtypes[i].obj_name; i++) {
		int found = 0;

		// Is this object type specified?
		if (do_all) {
			// Skip rs, rslh, and rssa
			if ((strcmp(cl_objtypes[i].obj_name, TYPE_RSLH) != 0) &&
			    (strcmp(cl_objtypes[i].obj_name, TYPE_RSSA) != 0) &&
			    (strcmp(cl_objtypes[i].obj_name, TYPE_RS) != 0)) {
				found++;
			}
		} else if (valid_cltypes.isValue(cl_objtypes[i].obj_name)) {
			found++;
		}

		// If not found, skip
		if (!found)
			continue;

		// Get the corresponding ClShow object.
		if (cl_objtypes[i].do_clshow1) {
			obj_show1 = new ClShow(cl_objtypes[i].show_head1);

			// Need to pass a special flag for rslh and rssa
			if (strcmp(cl_objtypes[i].obj_name, TYPE_RSLH) == 0) {
				clerrno = cl_objtypes[i].do_clshow1(
				    optflgs | LHflg, obj_show1);
			} else if (strcmp(cl_objtypes[i].obj_name,
			    TYPE_RSSA) == 0) {
				clerrno = cl_objtypes[i].do_clshow1(
				    optflgs | SAflg, obj_show1);
			} else if (strcmp(cl_objtypes[i].obj_name,
			    TYPE_TELATTR) == 0) {
				// Use the Fflg to print error
				// if SLM is not configured
				if (!do_all) {
					clerrno = cl_objtypes[i].do_clshow1(
					    optflgs | Fflg, obj_show1);
				} else {
					clerrno = cl_objtypes[i].do_clshow1(
					    optflgs, obj_show1);
				}
				if (clerrno == CL_EOP) {
					// SLM is not configured
					if (do_all) {
						clerrno = CL_NOERR;
					}
					delete obj_show1;
					continue;
				}
			} else {
				clerrno = cl_objtypes[i].do_clshow1(optflgs,
				    obj_show1);
			}
		} else if (cl_objtypes[i].do_clshow2) {
			obj_show1 = new ClShow(cl_objtypes[i].show_head1);
			obj_show2 = new ClShow(cl_objtypes[i].show_head2);

			clerrno = cl_objtypes[i].do_clshow2(optflgs,
			    obj_show1, obj_show2);
#ifdef WEAK_MEMBERSHIP
		} else if (cl_objtypes[i].do_clshow3) {
			obj_show1 = new ClShow(cl_objtypes[i].show_head1);
			obj_show2 = new ClShow(cl_objtypes[i].show_head2);
			obj_show3 = new ClShow(cl_objtypes[i].show_head3);

			clerrno = cl_objtypes[i].do_clshow3(optflgs,
			    obj_show1, obj_show2, obj_show3);
#endif
		}

		if ((clerrno != CL_NOERR) && (first_err == CL_NOERR)) {
			first_err = clerrno;
		}

		// Add into cluster clshow object.
		if (cl_objtypes[i].obj_level == 0) {
			// Add all objects in obj_show1
			lev1_parent = obj_show1;

			// Get level1 parent object value
			for (lev1_parent->startObjects();
			    !lev1_parent->endObjects();
			    lev1_parent->nextObjects()) {
				lev1_parent_name =
				    lev1_parent->currentObject()->value;
				break;
			}
		} else if (cl_objtypes[i].obj_level == 1) {
			// Add all the objects into level1_parent as children
			if (lev1_parent_name && lev1_parent) {
				if ((strcmp(cl_objtypes[i].obj_name,
				    TYPE_QUORUM) != 0) || !has_nodetype) {
					(void) lev1_parent->addChild(
					    lev1_parent_name, obj_show1);
				}
#ifdef WEAK_MEMBERSHIP
				// "intr" has two ClShow objects
				if ((strcmp(cl_objtypes[i].obj_name,
				    TYPE_INTR) == 0)) {
					(void) lev1_parent->addChild(
					    lev1_parent_name, obj_show2);
				}

				// "quorum" has three ClShow objects
				if ((strcmp(cl_objtypes[i].obj_name,
				    TYPE_QUORUM) == 0)) {
					(void) lev1_parent->addChild(
					    lev1_parent_name, obj_show3);
				}
#else
				// "quorum" and intr" has two ClShow objects
				if ((strcmp(cl_objtypes[i].obj_name,
				    TYPE_QUORUM) == 0) ||
					(strcmp(cl_objtypes[i].obj_name,
				    TYPE_INTR) == 0)) {
					(void) lev1_parent->addChild(
					    lev1_parent_name, obj_show2);
				}
#endif // WEAK_MEMBERSHIP
			} else if (!do_all) {
				bool print_show1 = false;

				//
				// No need to print obj_show1 for quorum
				// if node is specified.
				//
				if (strcmp(cl_objtypes[i].obj_name,
				    TYPE_QUORUM) == 0) {
					if (!has_nodetype) {
						print_show1 = true;
					}
				} else if (strcmp(cl_objtypes[i].obj_name,
				    TYPE_NODE) == 0) {
					if (!child_num) {
						print_show1 = true;
					}
				} else {
					// Print all other types
					print_show1 = true;
				}

				// Print the ClShow object.
				if (print_show1) {
					(void) obj_show1->print();
					delete obj_show1;
				}
#ifdef WEAK_MEMBERSHIP
				// "intr" has two ClShow objects.
				if ((strcmp(cl_objtypes[i].obj_name,
				    TYPE_INTR) == 0)) {
					(void) obj_show2->print();
					delete obj_show2;
				}

				// "quorum" has three ClShow objects.
				if ((strcmp(cl_objtypes[i].obj_name,
				    TYPE_QUORUM) == 0)) {
					(void) obj_show3->print();
					delete obj_show3;
				}
#else
				// "quorum" and intr" has two ClShow objects.
				if ((strcmp(cl_objtypes[i].obj_name,
				    TYPE_QUORUM) == 0) ||
					(strcmp(cl_objtypes[i].obj_name,
				    TYPE_INTR) == 0)) {
					(void) obj_show2->print();
					delete obj_show2;
				}
#endif // WEAK_MEMBERSHIP
			}

			// Special process for the 2nd ClShow object of node
			if (strcmp(cl_objtypes[i].obj_name, TYPE_NODE) == 0) {
				// Check if Europa is enabled
				boolean_t iseuropa = B_FALSE;
				if ((scconf_iseuropa(&iseuropa) ==
				    SCCONF_NOERR) && iseuropa) {
					lev1_sibling = obj_show2;
				}
			}
			lev2_parent = obj_show1;
		} else if (cl_objtypes[i].obj_level == 2) {
			if (has_nodetype) {
				clerrno = cluster_add_node_child(obj_show1,
				    cl_objtypes[i].obj_name);
				count++;
				if (!lev1_parent && (count == child_num)) {
					// Print the ClShow object.
					(void) obj_show1->print();
					delete obj_show1;
				}
			} else {
				clerrno = cluster_add_node_subtype(
				    lev1_parent, cl_objtypes[i].obj_name);
			}

			// Record the first error
			if ((clerrno != CL_NOERR) &&
			    (first_err == CL_NOERR)) {
				first_err = clerrno;
			}
		}

		// Processing siblings after processed all children
		if (lev1_sibling && (count == child_num)) {
			if (lev1_parent_name && lev1_parent) {
				(void) lev1_parent->addChild(lev1_parent_name,
				    lev1_sibling);
			} else if (!do_all) {
				(void) lev1_sibling->print();
				delete lev1_sibling;
			}
			lev1_sibling = NULL;
		}
	}

#if (SOL_VERSION >= __s10)
	// Go through all zone cluster components
	for (i = 0; zone_support && zc_objtypes[i].obj_name; i++) {
		int found = 0;

		// Is this object type specified?
		if (do_all) {
			// Skip rs, rslh, and rssa
			if ((strcmp(zc_objtypes[i].obj_name, TYPE_RSLH) != 0) &&
			    (strcmp(zc_objtypes[i].obj_name, TYPE_RSSA) != 0) &&
			    (strcmp(zc_objtypes[i].obj_name, TYPE_RS) != 0)) {
				found++;
			}
		} else if (valid_cltypes.isValue(zc_objtypes[i].obj_name)) {
			found++;
		}

		// If not found, skip
		if (!found)
			continue;

		// Get the corresponding ClShow object.
		if (zc_objtypes[i].do_clshow1) {
			obj_show1 = new ClShow(zc_objtypes[i].show_head1);

			// Need to pass a special flag for rslh and rssa
			if (strcmp(zc_objtypes[i].obj_name, TYPE_RSLH) == 0) {
				clerrno = zc_objtypes[i].do_clshow1(
				    optflgs | LHflg, obj_show1);
			} else if (strcmp(zc_objtypes[i].obj_name,
			    TYPE_RSSA) == 0) {
				clerrno = zc_objtypes[i].do_clshow1(
				    optflgs | SAflg, obj_show1);
			} else if (strcmp(zc_objtypes[i].obj_name,
			    TYPE_TELATTR) == 0) {
				// Use the Fflg to print error
				// if SLM is not configured
				if (!do_all) {
					clerrno = zc_objtypes[i].do_clshow1(
					    optflgs | Fflg, obj_show1);
				} else {
					clerrno = zc_objtypes[i].do_clshow1(
					    optflgs, obj_show1);
				}
				if (clerrno == CL_EOP) {
					// SLM is not configured
					if (do_all) {
						clerrno = CL_NOERR;
					}
					delete obj_show1;
					continue;
				}
			} else {
				clerrno = zc_objtypes[i].do_clshow1(optflgs,
				    obj_show1);
			}
		} else if (zc_objtypes[i].do_clshow2) {
			obj_show1 = new ClShow(zc_objtypes[i].show_head1);
			obj_show2 = new ClShow(zc_objtypes[i].show_head2);

			clerrno = zc_objtypes[i].do_clshow2(optflgs,
			    obj_show1, obj_show2);
		}

		if ((clerrno != CL_NOERR) && (first_err == CL_NOERR)) {
			first_err = clerrno;
		}

		// Add into cluster clshow object.
		if (zc_objtypes[i].obj_level == 0) {
			// Add all objects in obj_show1
			lev1_parent = obj_show1;

			// Get level1 parent object value
			for (lev1_parent->startObjects();
			    !lev1_parent->endObjects();
			    lev1_parent->nextObjects()) {
				lev1_parent_name =
				    lev1_parent->currentObject()->value;
				break;
			}
		} else if (zc_objtypes[i].obj_level == 1) {
			// Add all the objects into level1_parent as children
			if (lev1_parent_name && lev1_parent) {
				if ((strcmp(zc_objtypes[i].obj_name,
				    TYPE_QUORUM) != 0) || !has_nodetype) {
					(void) lev1_parent->addChild(
					    lev1_parent_name, obj_show1);
				}
#ifdef WEAK_MEMBERSHIP
				// "intr" has two ClShow objects
				if ((strcmp(zc_objtypes[i].obj_name,
				    TYPE_INTR) == 0)) {
					(void) lev1_parent->addChild(
					    lev1_parent_name, obj_show2);
				}

				// "quorum" has three ClShow objects
				if ((strcmp(zc_objtypes[i].obj_name,
				    TYPE_QUORUM) == 0)) {
					(void) lev1_parent->addChild(
					    lev1_parent_name, obj_show3);
				}
#else
				// "quorum" and "intr" has two ClShow objects
				if ((strcmp(zc_objtypes[i].obj_name,
				    TYPE_QUORUM) == 0) ||
					(strcmp(zc_objtypes[i].obj_name,
				    TYPE_INTR) == 0)) {
					(void) lev1_parent->addChild(
					    lev1_parent_name, obj_show2);
				}
#endif // WEAK_MEMBERSHIP
			} else if (!do_all) {
				bool print_show1 = false;

				//
				// No need to print obj_show1 for quorum
				// if node is specified.
				//
				if (strcmp(zc_objtypes[i].obj_name,
				    TYPE_QUORUM) == 0) {
					if (!has_nodetype) {
						print_show1 = true;
					}
				} else if (strcmp(zc_objtypes[i].obj_name,
				    TYPE_NODE) == 0) {
					if (!child_num) {
						print_show1 = true;
					}
				} else {
					// Print all other types
					print_show1 = true;
				}

				// Print the ClShow object.
				if (print_show1) {
					(void) obj_show1->print();
					delete obj_show1;
				}

#ifdef WEAK_MEMBERSHIP
				// "intr" has two ClShow objects.
				if ((strcmp(zc_objtypes[i].obj_name,
				    TYPE_INTR) == 0)) {
					(void) obj_show2->print();
					delete obj_show2;
				}

				// "quorum" has three ClShow objects.
				if ((strcmp(zc_objtypes[i].obj_name,
				    TYPE_QUORUM) == 0)) {
					(void) obj_show3->print();
					delete obj_show3;
				}
#else
				// "quorum" and "intr" has two ClShow objects.
				if ((strcmp(zc_objtypes[i].obj_name,
				    TYPE_QUORUM) == 0) ||
					(strcmp(zc_objtypes[i].obj_name,
				    TYPE_INTR) == 0)) {
					(void) obj_show2->print();
					delete obj_show2;
				}
#endif
			}

			// Special process for the 2nd ClShow object of node
			if (strcmp(zc_objtypes[i].obj_name, TYPE_NODE) == 0) {
				// Check if Europa is enabled
				boolean_t iseuropa = B_FALSE;
				if ((scconf_iseuropa(&iseuropa) ==
				    SCCONF_NOERR) && iseuropa) {
					lev1_sibling = obj_show2;
				}
			}
			lev2_parent = obj_show1;
		} else if (zc_objtypes[i].obj_level == 2) {
			if (has_nodetype) {
				clerrno = cluster_add_node_child(obj_show1,
				    zc_objtypes[i].obj_name);
				count++;
				if (!lev1_parent && (count == child_num)) {
					// Print the ClShow object.
					(void) obj_show1->print();
					delete obj_show1;
				}
			} else {
				clerrno = cluster_add_node_subtype(
				    lev1_parent, zc_objtypes[i].obj_name);
			}

			// Record the first error
			if ((clerrno != CL_NOERR) &&
			    (first_err == CL_NOERR)) {
				first_err = clerrno;
			}
		}

		// Processing siblings after processed all children
		if (lev1_sibling && (count == child_num)) {
			if (lev1_parent_name && lev1_parent) {
				(void) lev1_parent->addChild(lev1_parent_name,
				    lev1_sibling);
			} else if (!do_all) {
				(void) lev1_sibling->print();
				delete lev1_sibling;
			}
			lev1_sibling = NULL;
		}
	}
#endif

	if (lev1_parent) {
		(void) lev1_parent->print();
		delete lev1_parent;
	}

cleanup:
	if (first_err != CL_NOERR) {
		clerrno = first_err;
	}

	return (clerrno);
}

//
// get_global_show_obj
//
//	Get the global cluster properties and saved in ClShow object.
//	The ClShow object must be created before calling.
//
clerrno_t
get_global_show_obj(optflgs_t optflgs, ClShow *global_clshow)
{
	int i;
	clerrno_t clerrno = CL_NOERR;
	clconf_cluster_t *clconf = (clconf_cluster_t *)0;
	scconf_errno_t scconf_err = SCCONF_NOERR;
	const char *cluster_name = (char *)0;
	const char *conf_val = (char *)0;
	clconf_iter_t *currnodesi, *nodesi = (clconf_iter_t *)0;
	clconf_obj_t *node;
	char node_list[SCCONF_MAXSTRINGLEN];
	bool is_first = true;
	unsigned short globalfencing = GLOBAL_FENCING_UNKNOWN;
	char udp_dflt_str[16];

	// Check inputs
	if (!global_clshow) {
		clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}

	// Get cluster clconf handle
	scconf_err = scconf_cltr_openhandle((scconf_cltr_handle_t *)&clconf);
	if (scconf_err != SCCONF_NOERR) {
		clerrno = cluster_conv_scconf_err(1, scconf_err);
		return (clerrno);
	}

	// Get cluster name
	cluster_name = clconf_obj_get_name((clconf_obj_t *)clconf);
	if (!cluster_name) {
		clerrno = CL_EINTERNAL;
		clcommand_perror(CL_EINTERNAL);
		goto cleanup;
	}

	// Add cluster name to ClShow.
	global_clshow->addObject((char *)gettext("Cluster Name"),
	    (char *)cluster_name);

	// Add other properties
	for (i = 0; cluster_props[i].prop_ccr_name; i++) {
		conf_val = clconf_obj_get_property((clconf_obj_t *)clconf,
		    cluster_props[i].prop_ccr_name);

		//
		// We should not get a value of "default" but if we do,
		// convert it.
		//
		if ((strcmp(cluster_props[i].prop_ccr_name,
		    CLPROP_UDP_SESSION_TIMEOUT) == 0) &&
		    (!conf_val || strcasecmp(conf_val, "default") == 0)) {
		    // we do not display udp timeout inside a zc.
#if (SOL_VERSION >= __s10)
		    if (getzoneid() != GLOBAL_ZONEID) {
			    continue;
		    }
#endif
			sprintf(udp_dflt_str, "%d", UDP_SESSION_TIMEOUT_DFLT);
			global_clshow->addProperty((char *)cluster_name,
			    cluster_props[i].prop_cli_name, udp_dflt_str);
		} else if (strcmp(cluster_props[i].prop_ccr_name,
			CLPROP_PRIV_NETMASK) == 0) {
			// The netmask is stored as "cluster_netmask"
			conf_val =  clconf_obj_get_property(
			    (clconf_obj_t *)clconf, "cluster_netmask");
			if (conf_val == NULL) {
				// If the cluster_netmask is not there,
				// Display the private_netmask value
				conf_val = clconf_obj_get_property(
				    (clconf_obj_t *)clconf,
				    cluster_props[i].prop_ccr_name);
			}
			global_clshow->addProperty((char *)cluster_name,
			    cluster_props[i].prop_cli_name, (char *)conf_val);
		} else {
			global_clshow->addProperty((char *)cluster_name,
			    cluster_props[i].prop_cli_name, (char *)conf_val);
		}
	}

#if (SOL_VERSION >= __s10)
	if (getzoneid() != GLOBAL_ZONEID) {
		goto skip_global;
	}
#endif

	// Add global fencing setting
	// If inside a zone cluster we do not need to fetch the
	// global fencing status.
	if (did_initlibrary(1, DID_NONE, NULL) == DID_INIT_SUCCESS) {
		(void) did_getglobalfencingstatus(&globalfencing);
		switch (globalfencing) {
		case GLOBAL_FENCING_PATHCOUNT:
			global_clshow->addProperty((char *)cluster_name,
			    CLPROP_FENCING, (char *)"pathcount");
			break;
		case GLOBAL_FENCING_PREFER3:
			global_clshow->addProperty((char *)cluster_name,
			    CLPROP_FENCING, (char *)"prefer3");
			break;
		case GLOBAL_NO_FENCING:
			global_clshow->addProperty((char *)cluster_name,
			    CLPROP_FENCING, (char *)"nofencing");
			break;
		case GLOBAL_FENCING_UNKNOWN:
		default:
			global_clshow->addProperty((char *)cluster_name,
			    CLPROP_FENCING, (char *)gettext("Unknown"));
			break;
		}
	}

skip_global:
	/* Add cluster node list */
	*node_list = '\0';
	nodesi = currnodesi = clconf_cluster_get_nodes(clconf);
	while ((node = clconf_iter_get_current(currnodesi)) != NULL) {
		conf_val = clconf_obj_get_name(node);
		if (!conf_val)
			continue;

		// Add into the nodelist buffer
		if (is_first) {
			is_first = false;
		} else {
			(void) strcat(node_list, ", ");
		}
		(void) strcat(node_list, conf_val);

		// Go to next node
		clconf_iter_advance(currnodesi);
	}
	global_clshow->addProperty((char *)cluster_name,
	    (char *)gettext("Node List"), node_list);

cleanup:
	if (nodesi != (clconf_iter_t *)0) {
		clconf_iter_release(nodesi);
	}

	// Release the cluster config
	if (clconf != (clconf_cluster_t *)0) {
		clconf_obj_release((clconf_obj_t *)clconf);
	}

	return (clerrno);
}

//
// is_valid_cltype
//
//	Check if the given cluster object type is valid. If valid,
//	add into the ValueList and return true; O.w. return false.
//
bool
add_valid_cltype(char *cl_type, ValueList &valid_cltypes)
{
	int i;

	if (!cl_type || (strcasecmp(cl_type, TYPE_INTRADAPTER) == 0))
		return (false);

	for (i = 0; cl_objtypes[i].obj_name; i++) {
		if ((strcasecmp(cl_type, cl_objtypes[i].obj_name) == 0) ||
		    (strcasecmp(cl_type, cl_objtypes[i].obj_shortname) == 0)) {
			valid_cltypes.add(cl_objtypes[i].obj_name);
			return (true);
		}
	}

	return (false);
}	

//
// cluster_add_node_child
//
//	Get the ClShow objects of cl_type, and add it to clshow_parent
//	as child.
//
clerrno_t
cluster_add_node_child(ClShow *clshow_parent, char *cl_type)
{
	clerrno_t clerrno = CL_NOERR;
	ClShow *clshow_obj;
	char *parent_name = (char *)0;
	NameValueList *prop_list = (NameValueList *)0;
	bool found;

	// Check inputs
	if (!clshow_parent || !cl_type) {
		return (CL_EINTERNAL);
	}

	// Go through the clshow objects
	for (clshow_parent->startObjects(); !clshow_parent->endObjects();
	    clshow_parent->nextObjects()) {
		parent_name = clshow_parent->currentObject()->value;
		if (!parent_name) {
			if (clerrno == CL_NOERR) {
				clerrno = CL_EINTERNAL;
			}
			continue;
		}

		// Check the type. Skip farm nodes
		prop_list = clshow_parent->currentObject()->properties;
		if (!prop_list) {
			if (clerrno == CL_NOERR) {
				clerrno = CL_EINTERNAL;
			}
			continue;
		}

		// Is it farm node?
		found = false;
		for (prop_list->start(); !prop_list->end(); prop_list->next()) {
			char *prop_name = prop_list->getName();
			char *prop_val = prop_list->getValue();
			if (!prop_name || !prop_val ||
			    (strcasecmp(prop_name, "type") != 0)) {
				continue;
			}

			if (strcasecmp(prop_val, NODETYPE_SERVER) == 0) {
				found = true;
				break;
			}
		}
		if (!found) {
			continue;
		}

		// Get the ClShow object of cl_type
		if (strcmp(cl_type, TYPE_SNMPMIB) == 0) {
			clshow_obj =
			    get_clsnmpmib_show_obj((char *)parent_name);
		} else if (strcmp(cl_type, TYPE_SNMPHOST) == 0) {
			clshow_obj =
			    get_clsnmphost_show_obj((char *)parent_name);
		} else if (strcmp(cl_type, TYPE_SNMPUSER) == 0) {
			clshow_obj =
			    get_clsnmpuser_show_obj((char *)parent_name);
		} else if (strcmp(cl_type, TYPE_INTRADAPTER) == 0) {
			clshow_obj =
			    get_clintr_adapter_show((char *)parent_name);
		}

		if (!clshow_obj) {
			continue;
		}

		// Add into clshow_parent as a child
		(void) clshow_parent->addChild(parent_name, clshow_obj);
	}

	return (clerrno);
}

//
// cluster_add_node_subtype
//
//	Get the ClShow objects of cl_type. If clshow_parent is NULL, print
//	them; If clshow_parent is not NULL, add them to clshow_parent as
//	child. For now the "type" is snmp and interconnect.
//
clerrno_t
cluster_add_node_subtype(ClShow *clshow_parent, char *cl_type)
{
	clerrno_t clerrno = CL_NOERR;
	clconf_cluster_t *clconf = (clconf_cluster_t *)0;
	clconf_iter_t *currnodesi, *nodesi = (clconf_iter_t *)0;
	clconf_obj_t *node;
	const char *conf_name;
	ClShow *clshow_obj;
	char *parent_name = (char *)0;

	// Check inputs
	if (!cl_type) {
		return (CL_EINTERNAL);
	}

	// Get the parent name
	if (clshow_parent) {
		for (clshow_parent->startObjects();
		    !clshow_parent->endObjects();
		    clshow_parent->nextObjects()) {
			parent_name = clshow_parent->currentObject()->value;
			if (!parent_name) {
				return (CL_EINTERNAL);
			}
			break;
		}
	}

	// Get the handler
	if (scconf_cltr_openhandle((scconf_cltr_handle_t *)
	    &clconf) != SCCONF_NOERR) {
		return (CL_EINTERNAL);
	}

	// Go through all nodes.
	nodesi = currnodesi = clconf_cluster_get_nodes(clconf);
	while ((node = clconf_iter_get_current(currnodesi)) != NULL) {
		// Get the node name
		conf_name = clconf_obj_get_name(node);
		if (!conf_name)
			continue;

		// Get the ClShow object of "cl_type" on each node
		if (strcmp(cl_type, TYPE_SNMPMIB) == 0) {
			clshow_obj = get_clsnmpmib_show_obj((char *)conf_name);
		} else if (strcmp(cl_type, TYPE_SNMPHOST) == 0) {
			clshow_obj = get_clsnmphost_show_obj((char *)conf_name);
		} else if (strcmp(cl_type, TYPE_SNMPUSER) == 0) {
			clshow_obj = get_clsnmpuser_show_obj((char *)conf_name);
		} else if (strcmp(cl_type, TYPE_INTRADAPTER) == 0) {
			clshow_obj = get_clintr_adapter_show((char *)conf_name);
		}

		if (!clshow_obj) {
			continue;
		}

		if (!clshow_parent) {
			// Print it.
			(void) clshow_obj->print();
		} else {
			// Add it as a child
			(void) clshow_parent->addChild(parent_name, clshow_obj);
		}

		// Next node
		clconf_iter_advance(currnodesi);
	}

	// Release the cluster config
	if (clconf != (clconf_cluster_t *)0) {
		clconf_obj_release((clconf_obj_t *)clconf);
	}

	if (nodesi != (clconf_iter_t *)0) {
		clconf_iter_release(nodesi);
	}

	return (clerrno);
}
