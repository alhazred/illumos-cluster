//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)cluster_check.cc	1.4	08/09/09 SMI"

//
// Process cluster "check" subcommand
//

#include "cluster.h"
#include "clnode.h"

#define	CMD_CL_CHECK "/usr/cluster/lib/cfgchk/cfgchk"

//
// "cluster check" and "cluster list-checks"
//
//	Call for configuration check or check listing
//
clerrno_t
cluster_check(ClCommand &cmd, ValueList &args)
{

	//
	//	create command string to invoke:
	//		configuration checker engine + array of args
	//

	clerrno_t clerrno = CL_NOERR;
	int i;
	int ret_code = 0;
	char **cli_envs = new char *[128]; // safe # elements
	char **cli_args;
	int arg_count = 0;
	int env_count = 0;
	char *progname = (char *)0;
	char *subcmdname = (char *)0;
	int len;
	char *curr_locale = (char *)0;

	// set calling program name in environment for use by ksh
	if ((progname = strrchr(cmd.argv[0], '/')) == NULL) {
		progname = cmd.argv[0];
	} else {
		++progname;
	}
	subcmdname = cmd.argv[1];

	len = strlen(progname) + 7 + strlen(subcmdname);
	cli_envs[env_count] = new char [len];

	(void) sprintf(cli_envs[env_count++],
	    "PROG=%s %s", progname, subcmdname);

	// set user's locale in environment
	len = 128; // safe capacity
	curr_locale = setlocale(LC_ALL, NULL);
	cli_envs[env_count] = new char [len];
	(void) sprintf(cli_envs[env_count++],
	    "LC_ALL=%s", curr_locale);

	cli_envs[env_count] = NULL;

	arg_count = args.getSize();

	// create array of cmd + args to invoke
	cli_args = new char *[arg_count + 2];
	arg_count = 0;

	// put cmd itself in as $0 to ksh
	cli_args[arg_count] = new char [strlen(CMD_CL_CHECK) + 1];
	(void) strcpy(cli_args[arg_count++], CMD_CL_CHECK);

	// now put in user args: $1,... to ksh
	for (args.start(); !args.end(); args.next()) {
		char *value = args.getValue();
		if (!value) {
			continue;
		}
		cli_args[arg_count] = new char [strlen(value) + 1];
		(void) strcpy(cli_args[arg_count++], value);
	}
	cli_args[arg_count] = NULL; // terminate


	// Call the actual command
	//   (reaching into clnode_misc.cc for execve code like other
	//    cluster subcommands do)
	ret_code = execve_command(CMD_CL_CHECK, cli_args, cli_envs);
	clerrno = ret_code;

	// cleanup
	// Free memory
	for (i = 0; i < arg_count; i++) {
		if (cli_args && cli_args[i]) {
			delete [] cli_args[i++];
		}
	}
	if (arg_count && cli_args) {
		delete [] cli_args;
	}

	for (i = 0; i < env_count; i++) {
		if (cli_envs[i]) {
			delete [] cli_envs[i];
		}
	}
	delete [] cli_envs;

	return (clerrno);
}
