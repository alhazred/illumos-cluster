//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)cluster_set_netprops.cc	1.8	08/07/25 SMI"

//
// Process cluster "set-netprops"
//

#include "cluster.h"
#include "clnode.h"

//
// cluster "set-netprops"
//
clerrno_t
cluster_set_netprops(ClCommand &cmd, ValueList &operands, optflgs_t optflgs,
    NameValueList &properties)
{
	clerrno_t clerrno = CL_NOERR;
	NameValueList privatenet_list = NameValueList(true);
	char *prop_name, *prop_val;
	char **cli_envs = new char *[3];
	char **cli_args = new char *[4];
	int env_count = 0;
	int arg_count = 0;
	int count_max = 0;
	int addr_count = 0;
	int invalid = 0;
	uint_t ismember;
	int ret = 0;

	// Check inputs
	if ((properties.getSize() == 0) || (operands.getSize() > 1)) {
		clerrno = CL_EINTERNAL;
		clcommand_perror(clerrno);
		goto cleanup;
	}

	// The operand must be this cluster name
	if (operands.getSize() == 1) {
		char *operand_name = (char *)0;
		FILE *clconf_fp;
		char line_buf[BUFSIZ];
		char val_buf[BUFSIZ];

		operands.start();
		operand_name = operands.getValue();

		// Open the cluster configuration file.
		clconf_fp = fopen(CCR_CONF_FILE, "r");
		if (clconf_fp == NULL) {
			clerror("Cannot open file \"%s\".\n", CCR_CONF_FILE);
			clerrno = CL_EINTERNAL;
			goto cleanup;
		}

		// Check cluster name
		while (fgets(line_buf, BUFSIZ, clconf_fp) != NULL) {
			if (sscanf(line_buf, "cluster.name %s", val_buf) != 1)
				continue;

			if (strcmp(operand_name, val_buf) != 0) {
				clerror("\"%s\" is not the name of this "
				    "cluster.\n", operand_name);
				clerrno = CL_ENOENT;
			}
			break;
		}
		(void) fclose(clconf_fp);
	}

	// Go through the properties
	for (properties.start(); !properties.end(); properties.next()) {
		// Get property name and value
		prop_name = properties.getName();
		prop_val = properties.getValue();
		if (!prop_name)
			continue;

		ismember = 0;
		(void) scconf_ismember(0, &ismember);

		if ((strcmp(prop_name, CLPROP_PRIV_NETADDR) == 0) ||
		    (strcmp(prop_name, CLPROP_PRIV_NETMASK) == 0) ||
		    (strcmp(prop_name, CLPROP_PRIV_MAXNODE) == 0) ||
		    (strcmp(prop_name, CLPROP_PRIV_MAXPRIV) == 0)) {
			// These properties can be set only
			// in the non-cluster mode
			if (ismember) {
				clerror("Property \"%s\" can be set only "
				    "in non-cluster mode.\n", prop_name);
				if (clerrno == CL_NOERR) {
					clerrno = CL_EPROP;
				}
				continue;
			}

			if (!prop_val || (strlen(prop_val) == 0)) {
				clerror("You must specify a value for "
				    "\"%s\".\n", prop_name);
				if (clerrno == CL_NOERR) {
					clerrno = CL_EPROP;
				}
				continue;
			}
			// For scprivipadm, no netmask means use the default.
			if (strcmp(prop_name, CLPROP_PRIV_NETMASK) == 0) {
				(void) privatenet_list.add(
				    SCPRIV_NETMASK, prop_val);
			} else if (strcmp(prop_name, CLPROP_PRIV_NETADDR)
			    == 0) {
				(void) privatenet_list.add(SCPRIV_NETADDR,
				    prop_val);
				addr_count++;
			} else if (strcmp(prop_name, CLPROP_PRIV_MAXNODE)
			    == 0) {
				(void) privatenet_list.add(SCPRIV_MAXNODE,
				    prop_val);
				count_max++;
			} else if (strcmp(prop_name, CLPROP_PRIV_MAXPRIV)
			    == 0) {
				(void) privatenet_list.add(SCPRIV_MAXPRIV,
				    prop_val);
				count_max++;
			}
#if (SOL_VERSION >= __s10)
		} else if (strcmp(prop_name, CLPROP_NUM_ZCLUSTERS) == 0) {
			// Check whether this is in cluster-mode
			if (!ismember) {
				clerror("Property \"%s\" can be set only "
				    "in cluster mode.\n", prop_name);
				if (clerrno == CL_NOERR) {
					clerrno = CL_EPROP;
				}
				continue;
			}
			ret = zccfg_set_clusternetmask(prop_val);
			if (ret != ZC_OK) {
				clerror("Failed to create the new netmask "
				    "needed for zone clusters.\n");
				if (clerrno == CL_NOERR) {
					clerrno = CL_EPROP;
				}
				continue;
			}
#endif
		} else {
			// Can this property be set in non-cluster mode?
			switch (cluster_check_property(prop_name)) {
			case CLPROP_READONLY:
				clerror("Cannot set read-only property "
				    "\"%s\".\n", prop_name);
				if (clerrno == CL_NOERR) {
					clerrno = CL_EOP;
				}
				break;
			case CLPROP_CLUSTER:
				clerror("User the \"set\" subcommand "
				    "in cluster mode to set \"%s\".\n",
				    prop_name);
				if (clerrno == CL_NOERR) {
					clerrno = CL_EOP;
				}
				break;
			case CLPROP_NONCLUSTER:
			case CLPROP_BOTH:
				break;
			case CLPROP_UNKNOWN:
			default:
				// Invalid node property
				clerror("Invalid property \"%s\".\n",
				    prop_name);
				if (clerrno == CL_NOERR) {
					clerrno = CL_EPROP;
				}
				break;
			}
		}
	}

	if (privatenet_list.getSize() == 0) {
		goto cleanup;
	}

	// Network address must be specified
	if (!addr_count) {
		clerror("You must specify the \"%s\".\n", CLPROP_PRIV_NETADDR);
		if (clerrno == CL_NOERR) {
			clerrno = CL_EPROP;
		}
		invalid++;
	}

	// SCPRIV_MAXNODE and SCPRIV_MAXPRIV must be specified together
	if (count_max != 0 && count_max != 2) {
		clerror("You must specify both \"%s\" and \"%s\".\n",
		    CLPROP_PRIV_MAXNODE, CLPROP_PRIV_MAXPRIV);
		if (clerrno == CL_NOERR) {
			clerrno = CL_EPROP;
		}
		invalid++;
	}

	// No need to go further
	if (invalid) {
		goto cleanup;
	}

	// Private network changes
	if (privatenet_list.getSize() > 0) {
		int tmp_err = SCIP_NOERR;
		char *private_net_params = (char *)0;
		char clcmd_buf[BUFSIZ];

		// Get the private IP properties.
		private_net_params = privatenet_list.getString(",");
		if (!private_net_params) {
			clerrno = CL_EINTERNAL;
			clcommand_perror(clerrno);
			goto cleanup;
		}

		// Set the environement variables to CLI_SCPRIVIPADM
		env_count = arg_count = 0;
		cli_envs[env_count] = new char [strlen(cmd.commandname) + 6];
		(void) sprintf(cli_envs[env_count++], "PROG=%s",
		    cmd.commandname);
		if (cmd.isVerbose || (optflgs & vflg)) {
			cli_envs[env_count] =
			    new char [strlen("CL_VBOSE=y") + 1];
			(void) strcpy(cli_envs[env_count++], "CL_VBOSE=y");
		}
		cli_envs[env_count] = NULL;

		// Set the argvs for calling CLI_SCPRIVIPADM
		cli_args[arg_count] = new char [strlen(CLI_SCPRIVIPADM) + 1];
		(void) strcpy(cli_args[arg_count++], CLI_SCPRIVIPADM);
		cli_args[arg_count] = new char [strlen("-c") + 1];
		(void) strcpy(cli_args[arg_count++], "-c");
		cli_args[arg_count] = new char [strlen(private_net_params) + 1];
		(void) strcpy(cli_args[arg_count++], private_net_params);

		// Execute CLI_SCPRIVIPADM
		tmp_err = execve_command(CLI_SCPRIVIPADM, cli_args, cli_envs);

		if (tmp_err != SCIP_NOERR) {
			switch (tmp_err) {
			case SCIP_EOP:
				clerrno = CL_EOP;
				break;
			case SCIP_EUSAGE:
			case SCIP_EINVAL:
				clerrno = CL_EINVAL;
				break;
			case SCIP_EINTERNAL:
			default:
				clerrno = CL_EINTERNAL;
				break;
			}
		}
	}

cleanup:
	for (int i = 0; i < arg_count; i++) {
		delete [] cli_args[i];
	}
	delete [] cli_args;

	for (int i = 0; i < env_count; i++) {
		delete [] cli_envs[i];
	}
	delete [] cli_envs;

	return (clerrno);
}
