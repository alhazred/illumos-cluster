//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)cluster_show_netprops.cc	1.7	08/07/25 SMI"

//
// Process cluster "show-netprops"
//

#include "cluster.h"
#include "ClShow.h"

// Private net property names saved in CCR
#define	CONF_NETADDR	"net_number"
#define	CONF_NETMASK	"netmask"
#define	CONF_MAXNODE	"maxnodes"
#define	CONF_MAXPRIV	"maxprivnets"
#define	CONF_ZCLUSTERS	"zoneclusters"

//
// cluster "show-netprops"
//
//	This subcommand can run on both cluster and non-cluster mode.
//	It prints the private network configuration on the node where
//	this subcommand is being run.
//
clerrno_t
cluster_show_netprops(ClCommand &cmd, ValueList &operands, optflgs_t optflgs)
{
	clerrno_t clerrno = CL_NOERR;
	clerrno_t first_err = CL_NOERR;
	FILE *clconf_fp;
	char line_buf[SCCONF_MAXSTRINGLEN];
	char prop_buf[SCCONF_MAXSTRINGLEN];
	char val_buf[SCCONF_MAXSTRINGLEN];
	char cnetmask[SCCONF_MAXSTRINGLEN];
	ClShow *prt_show = new ClShow(HEAD_PRIVNET);
	char *priv_netaddr = (char *)0;

	// Check inputs
	if (operands.getSize() > 1) {
		clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}

	// Open the cluster configuration file.
	clconf_fp = fopen(CCR_CONF_FILE, "r");
	if (clconf_fp == NULL) {
		clerror("Cannot open file \"%s\".\n", CCR_CONF_FILE);
		return (CL_EINTERNAL);
	}

	// The operand must be this cluster name
	if (operands.getSize() == 1) {
		char *operand_name = (char *)0;

		operands.start();
		operand_name = operands.getValue();

		// Check cluster name
		while (fgets(line_buf, BUFSIZ, clconf_fp) != NULL) {
			if (sscanf(line_buf, "cluster.name %s", val_buf) != 1)
				continue;

			if (strcmp(operand_name, val_buf) != 0) {
				clerror("\"%s\" is not the name of this "
				    "cluster.\n", operand_name);
				if (first_err == CL_NOERR) {
					first_err = CL_ENOENT;
				}
				goto cleanup;
			}
			break;
		}
		rewind(clconf_fp);
	}

	// Read the configuration file
	while (fgets(line_buf, BUFSIZ, clconf_fp) != NULL) {
		if (sscanf(line_buf, "cluster.properties.private_%s %s",
		    prop_buf, val_buf) != 2)
			continue;

		if (strcmp(prop_buf, CONF_NETADDR) == 0) {
			prt_show->addObject(CLPROP_PRIV_NETADDR, val_buf);
			priv_netaddr = strdup(val_buf);
			if (!priv_netaddr) {
				clcommand_perror(clerrno);
				if (first_err == CL_NOERR) {
					first_err = CL_ENOMEM;
				}
				goto cleanup;
			}
			break;
		}
	}

	// Nothing to print.
	if (!priv_netaddr || (prt_show->getObjectSize() == 0)) {
		goto cleanup;
	}

	// Rewind the file and read other private net properties
	rewind(clconf_fp);
	*cnetmask = '\0';
	while (fgets(line_buf, BUFSIZ, clconf_fp) != NULL) {
		if (sscanf(line_buf, "cluster.properties.cluster_netmask %s",
		    cnetmask) != 1) {
			continue;
		}
	}
	rewind(clconf_fp);
	while (fgets(line_buf, BUFSIZ, clconf_fp) != NULL) {
		if (sscanf(line_buf, "cluster.properties.private_%s %s",
		    prop_buf, val_buf) != 2) {
			continue;
		}

		if (strcmp(prop_buf, CONF_NETMASK) == 0) {
			if (strlen(cnetmask) > 0) {
				prt_show->addProperty(priv_netaddr,
				    CLPROP_PRIV_NETMASK, cnetmask);
			} else {
				prt_show->addProperty(priv_netaddr,
				    CLPROP_PRIV_NETMASK, val_buf);
			}
		} else if (strcmp(prop_buf, CONF_MAXNODE) == 0) {
			prt_show->addProperty(priv_netaddr,
			    CLPROP_PRIV_MAXNODE, val_buf);
		} else if (strcmp(prop_buf, CONF_MAXPRIV) == 0) {
			prt_show->addProperty(priv_netaddr,
			    CLPROP_PRIV_MAXPRIV, val_buf);
		}

	}

#if (SOL_VERSION >= __s10)
	rewind(clconf_fp);
	while (fgets(line_buf, BUFSIZ, clconf_fp) != NULL) {
		if (sscanf(line_buf, "cluster.properties.zoneclusters %s",
		    val_buf) != 1) {
			continue;
		}
		prt_show->addProperty(priv_netaddr,
		    CLPROP_NUM_ZCLUSTERS, val_buf);
	}
#endif

	// Print the ClShow
	(void) prt_show->print();

cleanup:
	// Close the file
	(void) fclose(clconf_fp);

	delete prt_show;

	if (first_err != CL_NOERR) {
		clerrno = first_err;
	}

	return (clerrno);
} //lint !e715
