//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident   "@(#)cluster_misc.cc 1.16     09/01/16 SMI"

//
// Miscellaneous functions for cluster
//

#include "cluster.h"
#include "clnode.h"
#include "claccess.h"
#include "clquorum.h"
#include "clnasdevice.h"
#include "clresourcegroup.h"
#include "clresourcetype.h"
#include "clresource.h"
#include "cldevicegroup.h"
#include "cldevice.h"
#include "clinterconnect.h"
#include "clsnmpmib.h"
#include "clsnmphost.h"
#include "clsnmpuser.h"
#include "cltelemetryattribute.h"

//
// Initialization cluster object types
//	If type B can be a child of type A, list type B immediately after
//	type A in this table.
//
#ifdef WEAK_MEMBERSHIP
//
// When running under Weak Membership model Quorum object
// needs to pring an extra HEADING for the GLOBAL_QUORUM
// object instance. Adding the new function pointer
// argument to the cluster_object_type_t structure
//
cluster_object_type_t cl_objtypes[] = {
	{
		TYPE_GLOBAL, TYPE_GLOBAL, 0, true,
		HEAD_CLUSTER, (show_heading_t)0, (show_heading_t)0,
		get_global_show_obj, 0, 0,
		0, 0, 0, 0
	},
	{
		TYPE_ACCESS, TYPE_ACCESS, 1, false,
		HEAD_HOSTACCESS, (show_heading_t)0, (show_heading_t)0,
		get_access_show_obj, 0, 0,
		0, 0, 0, 0
	},
	{
		TYPE_NODE, TYPE_NODE, 1, true,
		HEAD_NODES, HEAD_FNODES, (show_heading_t)0,
		0, get_clnode_show_obj, 0,
		0, 0, 0, get_clnode_status_obj
	},
	{
		TYPE_INTRADAPTER, TYPE_INTRADAPTER, 2, false,
		(show_heading_t)0, (show_heading_t)0, (show_heading_t)0,
		0, 0, 0,
		0, 0, 0, 0
	},
	{
		TYPE_SNMPMIB, TYPE_SNMPMIB, 2, true,
		(show_heading_t)0, (show_heading_t)0, (show_heading_t)0,
		0, 0, 0,
		0, 0, 0, 0
	},
	{
		TYPE_SNMPHOST, TYPE_SNMPHOST, 2, true,
		(show_heading_t)0, (show_heading_t)0, (show_heading_t)0,
		0, 0, 0,
		0, 0, 0, 0
	},
	{
		TYPE_SNMPUSER, TYPE_SNMPUSER, 2, true,
		(show_heading_t)0, (show_heading_t)0, (show_heading_t)0,
		0, 0, 0,
		0, 0, 0, 0
	},
	{
		TYPE_INTR, STYPE_INTR, 1, true,
		HEAD_CABLES, HEAD_SWITCHES, (show_heading_t)0,
		0, get_clintr_show_obj, 0,
		get_clintr_status_obj, 0, 0, 0
	},
	{
		TYPE_QUORUM, TYPE_QUORUM, 1, true,
		HEAD_NODES, HEAD_QUORUMDEVICES, HEAD_GLOBALQUORUM,
		0, 0, get_clquorum_show_obj,
		0, 0, 0, get_clquorum_status_obj
	},
	{
		TYPE_DEVGRP, STYPE_DEVGRP, 1, true,
		HEAD_DEVICEGROUPS, (show_heading_t)0, (show_heading_t)0,
		get_cldevicegroup_show_obj, 0, 0,
		0, 0, get_cldevicegroup_status_obj, 0
	},
	{
		TYPE_RT, STYPE_RT, 1, true,
		(show_heading_t)HEAD_RESTYPES, (show_heading_t)0,
			(show_heading_t)0,
		get_clrt_show_obj, 0, 0,
		0, 0, 0, 0
	},
	{
		TYPE_RG, STYPE_RG, 1, true,
		HEAD_RESGROUPS, (show_heading_t)0, (show_heading_t)0,
		get_clrg_show_obj, 0, 0,
		get_clrg_status_obj, 0, 0, 0
	},
	{
		TYPE_RS, STYPE_RS, 1, true,
		HEAD_RESOURCES, (show_heading_t)0, (show_heading_t)0,
		get_clresource_show_obj, 0, 0,
		get_clresource_status_obj, 0, 0, 0
	},
	{
		TYPE_RSLH, STYPE_RSLH, 1, true,
		HEAD_RESOURCES, (show_heading_t)0, (show_heading_t)0,
		get_clresource_show_obj, 0, 0,
		get_clresource_status_obj, 0, 0, 0
	},
	{
		TYPE_RSSA, STYPE_RSSA, 1, true,
		HEAD_RESOURCES, (show_heading_t)0, (show_heading_t)0,
		get_clresource_show_obj, 0, 0,
		get_clresource_status_obj, 0, 0, 0
	},
	{
		TYPE_DEVICE, STYPE_DEVICE, 1, true,
		HEAD_DID, (show_heading_t)0, (show_heading_t)0,
		get_cldevice_show_obj, 0, 0,
		get_cldevice_status_obj, 0, 0, 0
	},
	{
		TYPE_NAS, STYPE_NAS, 1, true,
		HEAD_NASDEVICE, (show_heading_t)0, (show_heading_t)0,
		get_clnasdevice_show_obj, 0, 0,
		0, 0, 0, 0
	},
	{
		TYPE_TELATTR, STYPE_TELATTR, 1, true,
		HEAD_TELEATTR, (show_heading_t)0, (show_heading_t)0,
		get_clta_show_obj, 0, 0,
		get_clta_status_obj, 0, 0, 0
	},
#if (SOL_VERSION >= __s10)
	{
		TYPE_ZONECLUSTER, TYPE_ZONECLUSTER, 1, false,
		HEAD_ZONECLUSTER, (show_heading_t)0, (show_heading_t)0,
		get_zonecluster_show_obj, 0, 0,
		get_zonecluster_status_obj, 0, 0
	},
#endif
	{0, 0, 0, false, (show_heading_t)0, (show_heading_t)0,
			(show_heading_t)0, 0, 0, 0, 0, 0, 0, 0}
};
#else // WEAK_MEMBERSHIP
cluster_object_type_t cl_objtypes[] = {
	{
		TYPE_GLOBAL, TYPE_GLOBAL, 0, true,
		HEAD_CLUSTER, (show_heading_t)0,
		get_global_show_obj, 0,
		0, 0, 0, 0
	},
	{
		TYPE_ACCESS, TYPE_ACCESS, 1, false,
		HEAD_HOSTACCESS, (show_heading_t)0,
		get_access_show_obj, 0,
		0, 0, 0, 0
	},
	{
		TYPE_NODE, TYPE_NODE, 1, true,
		HEAD_NODES, HEAD_FNODES,
		0, get_clnode_show_obj,
		0, 0, 0, get_clnode_status_obj
	},
	{
		TYPE_INTRADAPTER, TYPE_INTRADAPTER, 2, false,
		(show_heading_t)0, (show_heading_t)0,
		0, 0,
		0, 0, 0, 0
	},
	{
		TYPE_SNMPMIB, TYPE_SNMPMIB, 2, true,
		(show_heading_t)0, (show_heading_t)0,
		0, 0,
		0, 0, 0, 0
	},
	{
		TYPE_SNMPHOST, TYPE_SNMPHOST, 2, true,
		(show_heading_t)0, (show_heading_t)0,
		0, 0,
		0, 0, 0, 0
	},
	{
		TYPE_SNMPUSER, TYPE_SNMPUSER, 2, true,
		(show_heading_t)0, (show_heading_t)0,
		0, 0,
		0, 0, 0, 0
	},
	{
		TYPE_INTR, STYPE_INTR, 1, true,
		HEAD_CABLES, HEAD_SWITCHES,
		0, get_clintr_show_obj,
		get_clintr_status_obj, 0, 0, 0
	},
	{
		TYPE_QUORUM, TYPE_QUORUM, 1, true,
		HEAD_NODES, HEAD_QUORUMDEVICES,
		0, get_clquorum_show_obj,
		0, 0, get_clquorum_status_obj, 0
	},
	{
		TYPE_DEVGRP, STYPE_DEVGRP, 1, true,
		HEAD_DEVICEGROUPS, (show_heading_t)0,
		get_cldevicegroup_show_obj, 0,
		0, 0, get_cldevicegroup_status_obj, 0
	},
	{
		TYPE_RT, STYPE_RT, 1, true,
		(show_heading_t)HEAD_RESTYPES, (show_heading_t)0,
		get_clrt_show_obj, 0,
		0, 0, 0, 0
	},
	{
		TYPE_RG, STYPE_RG, 1, true,
		HEAD_RESGROUPS, (show_heading_t)0,
		get_clrg_show_obj, 0,
		get_clrg_status_obj, 0, 0, 0
	},
	{
		TYPE_RS, STYPE_RS, 1, true,
		HEAD_RESOURCES, (show_heading_t)0,
		get_clresource_show_obj, 0,
		get_clresource_status_obj, 0, 0, 0
	},
	{
		TYPE_RSLH, STYPE_RSLH, 1, true,
		HEAD_RESOURCES, (show_heading_t)0,
		get_clresource_show_obj, 0,
		get_clresource_status_obj, 0, 0, 0
	},
	{
		TYPE_RSSA, STYPE_RSSA, 1, true,
		HEAD_RESOURCES, (show_heading_t)0,
		get_clresource_show_obj, 0,
		get_clresource_status_obj, 0, 0, 0
	},
	{
		TYPE_DEVICE, STYPE_DEVICE, 1, true,
		HEAD_DID, (show_heading_t)0,
		get_cldevice_show_obj, 0,
		get_cldevice_status_obj, 0, 0, 0
	},
	{
		TYPE_NAS, STYPE_NAS, 1, true,
		HEAD_NASDEVICE, (show_heading_t)0,
		get_clnasdevice_show_obj, 0,
		0, 0, 0, 0
	},
	{
		TYPE_TELATTR, STYPE_TELATTR, 1, true,
		HEAD_TELEATTR, (show_heading_t)0,
		get_clta_show_obj, 0,
		get_clta_status_obj, 0, 0, 0
	},
#if (SOL_VERSION >= __s10)
	{
		TYPE_ZONECLUSTER, TYPE_ZONECLUSTER, 1, false,
		HEAD_ZONECLUSTER, (show_heading_t)0,
		get_zonecluster_show_obj, 0,
		get_zonecluster_status_obj, 0, 0
	},
#endif
	{0, 0, 0, false, (show_heading_t)0, (show_heading_t)0,
			0, 0, 0, 0, 0, 0}
};
#endif // WEAK_MEMBERSHIP

//
// Initialization cluster object types
//	If type B can be a child of type A, list type B immediately after
//	type A in this table.
//
#ifdef WEAK_MEMBERSHIP
cluster_object_type_t zc_objtypes[] = {
	{
		TYPE_GLOBAL, TYPE_GLOBAL, 0, true,
		HEAD_CLUSTER, (show_heading_t)0, (show_heading_t)0,
		get_global_show_obj, 0, 0,
		0, 0, 0, 0
	},
	{
		TYPE_NODE, TYPE_NODE, 1, true,
		HEAD_NODES, HEAD_FNODES, (show_heading_t)0,
		0, get_clnode_show_obj, 0,
		0, 0, 0, get_clnode_status_obj
	},
	{
		TYPE_RT, STYPE_RT, 1, true,
		(show_heading_t)HEAD_RESTYPES, (show_heading_t)0,
				(show_heading_t)0,
		get_clrt_show_obj, 0, 0,
		0, 0, 0, 0
	},
	{
		TYPE_RG, STYPE_RG, 1, true,
		HEAD_RESGROUPS, (show_heading_t)0, (show_heading_t)0,
		get_clrg_show_obj, 0, 0,
		get_clrg_status_obj, 0, 0, 0
	},
	{
		TYPE_RS, STYPE_RS, 1, true,
		HEAD_RESOURCES, (show_heading_t)0, (show_heading_t)0,
		get_clresource_show_obj, 0, 0,
		get_clresource_status_obj, 0, 0, 0
	},
	{
		TYPE_RSLH, STYPE_RSLH, 1, true,
		HEAD_RESOURCES, (show_heading_t)0, (show_heading_t)0,
		get_clresource_show_obj, 0, 0,
		get_clresource_status_obj, 0, 0, 0
	},
	{
		TYPE_RSSA, STYPE_RSSA, 1, true,
		HEAD_RESOURCES, (show_heading_t)0, (show_heading_t)0,
		get_clresource_show_obj, 0, 0,
		get_clresource_status_obj, 0, 0, 0
	},
	{0, 0, 0, false, (show_heading_t)0, (show_heading_t)0,
			(show_heading_t)0, 0, 0, 0, 0, 0, 0, 0}
};
#else // WEAK_MEMBERSHIP
cluster_object_type_t zc_objtypes[] = {
	{
		TYPE_GLOBAL, TYPE_GLOBAL, 0, true,
		HEAD_CLUSTER, (show_heading_t)0,
		get_global_show_obj, 0,
		0, 0, 0, 0
	},
	{
		TYPE_NODE, TYPE_NODE, 1, true,
		HEAD_NODES, HEAD_FNODES,
		0, get_clnode_show_obj,
		0, 0, 0, get_clnode_status_obj
	},
	{
		TYPE_RT, STYPE_RT, 1, true,
		(show_heading_t)HEAD_RESTYPES, (show_heading_t)0,
		get_clrt_show_obj, 0,
		0, 0, 0, 0
	},
	{
		TYPE_RG, STYPE_RG, 1, true,
		HEAD_RESGROUPS, (show_heading_t)0,
		get_clrg_show_obj, 0,
		get_clrg_status_obj, 0, 0, 0
	},
	{
		TYPE_RS, STYPE_RS, 1, true,
		HEAD_RESOURCES, (show_heading_t)0,
		get_clresource_show_obj, 0,
		get_clresource_status_obj, 0, 0, 0
	},
	{
		TYPE_RSLH, STYPE_RSLH, 1, true,
		HEAD_RESOURCES, (show_heading_t)0,
		get_clresource_show_obj, 0,
		get_clresource_status_obj, 0, 0, 0
	},
	{
		TYPE_RSSA, STYPE_RSSA, 1, true,
		HEAD_RESOURCES, (show_heading_t)0,
		get_clresource_show_obj, 0,
		get_clresource_status_obj, 0, 0, 0
	},
	{0, 0, 0, false, (show_heading_t)0, (show_heading_t)0,
			0, 0, 0, 0, 0, 0}
};
#endif // WEAK_MEMBERSHIP

// Initialization of cluster properties
//	Note "global_fencing" property is handled separately.
//
cluster_proplist_t cluster_props[] = {
	{CLPROP_CLUSTERID, "cluster_id", CLPROP_READONLY},
	{CLPROP_INSTALLMODE, CLPROP_INSTALLMODE, CLPROP_CLUSTER},
	{CLPROP_HB_TIMEOUT, "transport_heartbeat_timeout", CLPROP_CLUSTER},
	{CLPROP_HB_QUANTUM, "transport_heartbeat_quantum", CLPROP_CLUSTER},
	{CLPROP_PRIV_NETADDR, "private_net_number", CLPROP_NONCLUSTER},
	{CLPROP_PRIV_NETMASK, "cluster_netmask", CLPROP_NONCLUSTER},
	{CLPROP_PRIV_MAXNODE, "private_maxnodes", CLPROP_NONCLUSTER},
	{CLPROP_PRIV_MAXPRIV, "private_maxprivnets", CLPROP_NONCLUSTER},
	{CLPROP_NUM_ZCLUSTERS, "zoneclusters", CLPROP_CLUSTER},
	{CLPROP_UDP_SESSION_TIMEOUT, "udp_session_timeout", CLPROP_CLUSTER},
	{(char *)0, (char *)0, false}
};

//
// cluster_conv_scconf_err
//
//	Convert cluster related scconf_errno_t to CL error.
//	Print an error, if the printflag is set.
//
clerrno_t
cluster_conv_scconf_err(int printflag, scconf_errno_t err)
{
	// Map the error
	switch (err) {
	case SCCONF_NOERR:
		return (CL_NOERR);

	case SCCONF_ENOCLUSTER:
		if (printflag)
			clcommand_perror(CL_ENOENT);
		return (CL_ENOENT);

	case SCCONF_EINSTALLMODE:
		if (printflag)
			clerror("Cluster install mode is enabled.\n");
		return (CL_ESTATE);

	case SCCONF_ENOMEM:
	case SCCONF_EOVERFLOW:
		if (printflag)
			clcommand_perror(CL_ENOMEM);
		return (CL_ENOMEM);

	case SCCONF_EBADVALUE:
		if (printflag)
			clerror("Bad value in configuration database.\n");
		return (CL_EINTERNAL);

	case SCCONF_ERECONFIG:
		if (printflag)
			clcommand_perror(CL_ERECONF);
		return (CL_ERECONF);

	case SCCONF_TM_EBADOPTS:
		if (printflag)
			clerror("Invalid transport property name.\n");
		return (CL_EPROP);

	case SCCONF_TM_EINVAL:
		if (printflag)
			clerror("Invalid transport property value.\n");
		return (CL_EPROP);

	default:
		if (printflag)
			clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}
}

//
// cluster_name_compare
//
//	Compare the givien name with the cluster name. If they
//	are same, return CL_NOERR; Otherwise return an error.
//
clerrno_t
cluster_name_compare(char *cl_name)
{
	char *ccr_clname = (char *)0;
	clerrno_t clerrno = CL_NOERR;

	// Check input
	if (!cl_name) {
		clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}

	// Initialize clconf
	if (clconf_lib_init() != 0) {
		clerror("Cannot initialize clconf library.\n");
		return (CL_EINTERNAL);
	}

	// Get cluster name
	ccr_clname = clconf_get_clustername();
	if (!ccr_clname) {
		clerror("Cannot get the cluster name.\n");
		return (CL_EINTERNAL);
	}

	// The operand must be this cluster name
	if (strcmp(ccr_clname, cl_name) != 0) {
		clerror("\"%s\" is not the name of this cluster.\n",
		    cl_name);
		clerrno = CL_ENOENT;
	}

	return (clerrno);
}

//
// cluster_check_property
//
//	Check if the given cluster property is valid, can
//	be modified in cluster mode or not.
//
uint_t
cluster_check_property(char *cl_prop_name)
{
	int i;

	// Check input
	if (!cl_prop_name) {
		return (CLPROP_UNKNOWN);
	}

	// Check the property
	for (i = 0; cluster_props[i].prop_ccr_name; i++) {
		// Special process for global_fencing property
		if (strcmp(cl_prop_name, CLPROP_FENCING) == 0) {
			return (CLPROP_CLUSTER);
		}

		if ((strcmp(cl_prop_name,
		    cluster_props[i].prop_ccr_name) == 0) ||
		    (strcmp(cl_prop_name,
		    cluster_props[i].prop_cli_name) == 0)) {
			return (cluster_props[i].prop_attr);
		}
	}

	// Not found?
	return (CLPROP_UNKNOWN);
}
