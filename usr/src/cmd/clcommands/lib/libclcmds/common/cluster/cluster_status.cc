//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)cluster_status.cc	1.7	08/10/09 SMI"

//
// Process cluster "status"
//

#include "cluster.h"

#if SOL_VERSION >= __s10
#include <zone.h>
#include <sys/cladm_int.h>
#include <sys/quorum_int.h>
#include <sys/clconf_int.h>
#endif

static bool
add_valid_cltype(char *cl_type, ValueList &valid_cltypes);

//
// cluster "status"
//
clerrno_t
cluster_status(ClCommand &cmd, ValueList &operands, optflgs_t optflgs,
    ValueList &cltypes)
{
	clerrno_t clerrno = CL_NOERR;
	clerrno_t first_err = CL_NOERR;
	int i;
	bool do_all = false;
	bool zone_support = false;
	ValueList valid_cltypes = ValueList(true);
	ClStatus *obj_status1 = (ClStatus *)0;
	ClStatus *obj_status2 = (ClStatus *)0;
	ClStatus *obj_status3 = (ClStatus *)0;
	ClStatus *obj_status4 = (ClStatus *)0;

	// Initialize clconf.
	if (clconf_lib_init() != 0) {
		clerror("Cannot initialize clconf library.\n");
		clerrno = CL_EINTERNAL;
		goto cleanup;
	}

	// The operand must be this cluster name
	if (operands.getSize() == 1) {
		char *operand_name = (char *)0;

		operands.start();
		operand_name = operands.getValue();
		clerrno = cluster_name_compare(operand_name);
		if (clerrno != CL_NOERR) {
			goto cleanup;
		}
	}

	// Print all components?
	if (cltypes.getSize() == 0) {
		do_all = true;
	} else {
		// Check for valid types
		for (cltypes.start(); !cltypes.end(); cltypes.next()) {
			char *cl_type = cltypes.getValue();

			if (!cl_type ||
			    (strcasecmp(cl_type, TYPE_RSLH) == 0) ||
			    (strcasecmp(cl_type, STYPE_RSLH) == 0) ||
			    (strcasecmp(cl_type, TYPE_RSSA) == 0) ||
			    (strcasecmp(cl_type, STYPE_RSSA) == 0))
				continue;

			if (sc_nv_zonescheck()) {
				if (strcasecmp(cl_type, TYPE_DEVICE) == 0) {
					clerror("You cannot view the status of "
					    "object type \"%s\" "
					    "from a non-global zone.\n",
					    cl_type);
					if (first_err == CL_NOERR) {
						first_err = CL_EOP;
					}
					continue;
				}
			}

			if (!add_valid_cltype(cl_type, valid_cltypes)) {
				if (first_err == CL_NOERR) {
					first_err = CL_ETYPE;
				}
			}
		}
		// "rs" contains rsls and rssa
		if (cltypes.isValue(TYPE_RSSA) ||
		    cltypes.isValue(STYPE_RSSA)) {
			if (!cltypes.isValue(TYPE_RS) &&
			    !cltypes.isValue(STYPE_RS)) {
				(void) add_valid_cltype(TYPE_RSSA,
				    valid_cltypes);
			}
		}
		if (cltypes.isValue(TYPE_RSLH) ||
		    cltypes.isValue(STYPE_RSLH)) {
			if (!cltypes.isValue(TYPE_RS) &&
			    !cltypes.isValue(STYPE_RS)) {
				(void) add_valid_cltype(TYPE_RSLH,
				    valid_cltypes);
			}
		}

		// If nothing to show?
		if (valid_cltypes.getSize() == 0) {
			goto cleanup;
		}
	}

#if (SOL_VERSION >= __s10)
	if (getzoneid() != GLOBAL_ZONEID) {
		zone_support = true;
	}
#endif
	// Go through all cluster components
	for (i = 0; !zone_support && cl_objtypes[i].obj_name; i++) {
		int found = 0;

		// Is this object type specified?
		if (do_all) {
			//
			// Skip rslh and rssa and if inside non-global zone,
			// device.
			//
			if (strcmp(cl_objtypes[i].obj_name, TYPE_RSLH) &&
			    strcmp(cl_objtypes[i].obj_name, TYPE_RSSA) &&
			    strcmp(cl_objtypes[i].obj_name, TYPE_DEVICE)) {
				found++;
			} else if (strcmp(cl_objtypes[i].obj_name, TYPE_DEVICE)
			    == 0) {
				if (sc_nv_zonescheck() == 0) {
					found++;
				}
			}
		} else if (valid_cltypes.isValue(cl_objtypes[i].obj_name)) {
			found++;
		}

		// If not found, skip
		if (!found)
			continue;

		// Get the corresponding ClShow object.
		if (cl_objtypes[i].do_clstatus1) {
			obj_status1 =
			    new ClStatus(cmd.isVerbose ? true : false);

			// Need to pass a special flag for rslh and rssa
			if (strcmp(cl_objtypes[i].obj_name, TYPE_RSLH) == 0) {
				clerrno = cl_objtypes[i].do_clstatus1(
				    optflgs | LHflg, obj_status1);
			} else if (strcmp(cl_objtypes[i].obj_name,
			    TYPE_RSSA) == 0) {
				clerrno = cl_objtypes[i].do_clstatus1(
				    optflgs | SAflg, obj_status1);
			} else if (strcmp(cl_objtypes[i].obj_name,
			    TYPE_TELATTR) == 0) {
				// Use the Fflg to print error
				// if SLM is not configured
				if (!do_all) {
					clerrno = cl_objtypes[i].do_clstatus1(
					    optflgs | Fflg, obj_status1);
				} else {
					clerrno = cl_objtypes[i].do_clstatus1(
					    optflgs, obj_status1);
				}
				if (clerrno == CL_EOP) {
					// SLM is not configured
					if (do_all) {
						clerrno = CL_NOERR;
					}
					delete obj_status1;
					continue;
				}
			} else {
				clerrno = cl_objtypes[i].do_clstatus1(optflgs,
				    obj_status1);
			}
		} else if (cl_objtypes[i].do_clstatus2) {
			obj_status1 =
			    new ClStatus(cmd.isVerbose ? true : false);
			obj_status2 =
			    new ClStatus(cmd.isVerbose ? true : false);

			clerrno = cl_objtypes[i].do_clstatus2(optflgs,
			    obj_status1, obj_status2);
		} else if (cl_objtypes[i].do_clstatus3) {
			obj_status1 =
			    new ClStatus(cmd.isVerbose ? true : false);
			obj_status2 =
			    new ClStatus(cmd.isVerbose ? true : false);
			obj_status3 =
			    new ClStatus(cmd.isVerbose ? true : false);

			clerrno = cl_objtypes[i].do_clstatus3(optflgs,
				obj_status1, obj_status2, obj_status3);
		} else if (cl_objtypes[i].do_clstatus4) {
			obj_status1 =
			    new ClStatus(cmd.isVerbose ? true : false);
			obj_status2 =
			    new ClStatus(cmd.isVerbose ? true : false);
			obj_status3 =
			    new ClStatus(cmd.isVerbose ? true : false);
			obj_status4 =
			    new ClStatus(cmd.isVerbose ? true : false);

			clerrno = cl_objtypes[i].do_clstatus4(optflgs,
			    obj_status1, obj_status2, obj_status3, obj_status4);
		}

		// Print it
		if (obj_status1) {
			obj_status1->print();
			delete obj_status1;
			obj_status1 = (ClStatus *)0;
		}
		if (obj_status2) {
			obj_status2->print();
			delete obj_status2;
			obj_status2 = (ClStatus *)0;
		}
		if (obj_status3) {
			// Special process for "node"
			if (strcmp(cl_objtypes[i].obj_name, TYPE_NODE) == 0) {
				// Check if Europa is confgiured
				boolean_t iseuropa = B_FALSE;
				if ((scconf_iseuropa(&iseuropa) ==
				    SCCONF_NOERR) && iseuropa) {
					obj_status3->print();
				}
			} else {
				obj_status3->print();
			}
			delete obj_status3;
			obj_status3 = (ClStatus *)0;
		}
		if (obj_status4) {
			obj_status4->print();
			delete obj_status4;
			obj_status4 = (ClStatus *)0;
		}

		if ((clerrno != CL_NOERR) && (first_err == CL_NOERR)) {
			first_err = clerrno;
		}
	}

#if (SOL_VERSION >= __s10)
	// Go through all zone cluster components
	for (i = 0; zone_support && zc_objtypes[i].obj_name; i++) {
		int found = 0;

		// Is this object type specified?
		if (do_all) {
			//
			// Skip rslh and rssa and if inside non-global zone,
			// device.
			//
			if (strcmp(zc_objtypes[i].obj_name, TYPE_RSLH) &&
			    strcmp(zc_objtypes[i].obj_name, TYPE_RSSA) &&
			    strcmp(zc_objtypes[i].obj_name, TYPE_DEVICE)) {
				found++;
			} else if (strcmp(zc_objtypes[i].obj_name, TYPE_DEVICE)
			    == 0) {
				if (sc_nv_zonescheck() == 0) {
					found++;
				}
			}
		} else if (valid_cltypes.isValue(zc_objtypes[i].obj_name)) {
			found++;
		}

		// If not found, skip
		if (!found)
			continue;

		// Get the corresponding ClShow object.
		if (zc_objtypes[i].do_clstatus1) {
			obj_status1 =
			    new ClStatus(cmd.isVerbose ? true : false);

			// Need to pass a special flag for rslh and rssa
			if (strcmp(zc_objtypes[i].obj_name, TYPE_RSLH) == 0) {
				clerrno = zc_objtypes[i].do_clstatus1(
				    optflgs | LHflg, obj_status1);
			} else if (strcmp(zc_objtypes[i].obj_name,
			    TYPE_RSSA) == 0) {
				clerrno = zc_objtypes[i].do_clstatus1(
				    optflgs | SAflg, obj_status1);
			} else if (strcmp(zc_objtypes[i].obj_name,
			    TYPE_TELATTR) == 0) {
				// Use the Fflg to print error
				// if SLM is not configured
				if (!do_all) {
					clerrno = zc_objtypes[i].do_clstatus1(
					    optflgs | Fflg, obj_status1);
				} else {
					clerrno = zc_objtypes[i].do_clstatus1(
					    optflgs, obj_status1);
				}
				if (clerrno == CL_EOP) {
					// SLM is not configured
					if (do_all) {
						clerrno = CL_NOERR;
					}
					delete obj_status1;
					continue;
				}
			} else {
				clerrno = zc_objtypes[i].do_clstatus1(optflgs,
				    obj_status1);
			}
		} else if (zc_objtypes[i].do_clstatus2) {
			obj_status1 =
			    new ClStatus(cmd.isVerbose ? true : false);
			obj_status2 =
			    new ClStatus(cmd.isVerbose ? true : false);

			clerrno = zc_objtypes[i].do_clstatus2(optflgs,
			    obj_status1, obj_status2);
		} else if (zc_objtypes[i].do_clstatus3) {
			obj_status1 =
			    new ClStatus(cmd.isVerbose ? true : false);
			obj_status2 =
			    new ClStatus(cmd.isVerbose ? true : false);
			obj_status3 =
			    new ClStatus(cmd.isVerbose ? true : false);

			clerrno = zc_objtypes[i].do_clstatus3(optflgs,
				obj_status1, obj_status2, obj_status3);
		} else if (zc_objtypes[i].do_clstatus4) {
			obj_status1 =
			    new ClStatus(cmd.isVerbose ? true : false);
			obj_status2 =
			    new ClStatus(cmd.isVerbose ? true : false);
			obj_status3 =
			    new ClStatus(cmd.isVerbose ? true : false);
			obj_status4 =
			    new ClStatus(cmd.isVerbose ? true : false);

			clerrno = zc_objtypes[i].do_clstatus4(optflgs,
			    obj_status1, obj_status2, obj_status3, obj_status4);
		}

		// Print it
		if (obj_status1) {
			obj_status1->print();
			delete obj_status1;
			obj_status1 = (ClStatus *)0;
		}
		if (obj_status2) {
			obj_status2->print();
			delete obj_status2;
			obj_status2 = (ClStatus *)0;
		}
		if (obj_status3) {
			// Special process for "node"
			if (strcmp(zc_objtypes[i].obj_name, TYPE_NODE) == 0) {
				// Check if Europa is confgiured
				boolean_t iseuropa = B_FALSE;
				if ((scconf_iseuropa(&iseuropa) ==
				    SCCONF_NOERR) && iseuropa) {
					obj_status3->print();
				}
			} else {
				obj_status3->print();
			}
			delete obj_status3;
			obj_status3 = (ClStatus *)0;
		}
		if (obj_status4) {
			obj_status4->print();
			delete obj_status4;
			obj_status4 = (ClStatus *)0;
		}

		if ((clerrno != CL_NOERR) && (first_err == CL_NOERR)) {
			first_err = clerrno;
		}
	}
#endif

cleanup:
	if (first_err != CL_NOERR) {
		clerrno = first_err;
	}

	return (clerrno);
}

//
// add_valid_cltype
//
//	Check if the given cluster object type is known and has
//	status. Returns true if it has; O.w. return false.
//
bool
add_valid_cltype(char *cl_type, ValueList &valid_cltypes)
{
	int i;

	if (!cl_type)
		return (false);

	for (i = 0; cl_objtypes[i].obj_name; i++) {
		if ((strcasecmp(cl_type, cl_objtypes[i].obj_name) == 0) ||
		    (strcasecmp(cl_type, cl_objtypes[i].obj_shortname) == 0)) {
			if (cl_objtypes[i].do_clstatus1 ||
			    cl_objtypes[i].do_clstatus2 ||
			    cl_objtypes[i].do_clstatus3 ||
			    cl_objtypes[i].do_clstatus4) {
				valid_cltypes.add(cl_objtypes[i].obj_name);
				return (true);
			} else {
				clerror("Object type \"%s\" does not have "
				    "status.\n", cl_type);
				return (false);
			}
		}
	}

	clerror("Unknown object type \"%s\".\n", cl_type);
	return (false);
}	
