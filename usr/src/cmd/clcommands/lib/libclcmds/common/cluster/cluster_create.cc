//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)cluster_create.cc	1.6	08/05/21 SMI"

//
// Process cluster "create"
//

#include "cluster.h"
#include "ClXml.h"
#include "clnode.h"

#define	CMD_SCINSTALL	"/usr/cluster/bin/scinstall"

//
// cluster "create"
//
clerrno_t
cluster_create(ClCommand &cmd, ValueList &operands, optflgs_t optflgs,
    char *clconfiguration)
{
	clerrno_t clerrno = CL_NOERR;
	clerrno_t first_err = CL_NOERR;
	char input_fname[SCCONF_MAXSTRINGLEN];
	ValueList xml_objects;
	char *operand_name = (char *)0;
	ClXml cl_xml;
	FILE *input_fp = 0;
	char *cluster_name = (char *)0;
	int counter;
	int node_num;
	char **cli_envs = new char *[2];
	char **cli_args = new char *[5];
	int arg_count = 0;
	int env_count = 0;
	NameValueList *cl_props = new NameValueList(true);
	char switches[BUFSIZ];
	char switch_types[BUFSIZ];
	char local_node[BUFSIZ];
	NameValueList *cl_objprops = new NameValueList;
	char adapters[SCCONF_MAXSTRINGLEN];
	char adapter_types[SCCONF_MAXSTRINGLEN];
	char ports[SCCONF_MAXSTRINGLEN];
	char pp_cable[SCCONF_MAXSTRINGLEN];
	char *sponsor_node = (char *)0;
	cltr_cable_t *cable_p;
	cltr_cable_t *cable_props;
	int cable_count;
	int direct_connect = 0;

	// Check for inputs
	if (!clconfiguration || (operands.getSize() > 1)) {
		clcommand_perror(CL_EINTERNAL);
		first_err = CL_EINTERNAL;
		goto cleanup;
	}

	// Get the cluster objects in the xml file
	clerrno = cl_xml.getAllObjectNames(CLUSTER_CMD, clconfiguration,
		&xml_objects);
	if (clerrno != CL_NOERR) {
		if (first_err == CL_NOERR) {
			first_err = clerrno;
		}
		goto cleanup;
	}

	// Check operand. There is only one operand.
	if (operands.getSize() > 0) {
		operands.start();
		operand_name = operands.getValue();

		// Is this operand found in xml file?
		counter = 0;
		for (xml_objects.start(); !xml_objects.end();
		    xml_objects.next()) {
			cluster_name = xml_objects.getValue();
			if (!cluster_name) {
				continue;
			}

			if (strcmp(cluster_name, operand_name) == 0) {
				counter++;
				break;
			}
		}
		if (!counter) {
			clerror("Cannot find cluster \"%s\" in file \"%s\".\n",
			    operand_name, clconfiguration);
			if (first_err == CL_NOERR) {
				first_err = CL_ENOENT;
			}
			goto cleanup;
		}
	} else {
		counter = xml_objects.getSize();

		if (counter == 0) {
			// "counter > 1" will be from an invalid doc, in which
			// case error clready printed in getAllObjects().
			clerror("Cannot find any cluster in \"%s\".\n",
			    clconfiguration);
			if (first_err == CL_NOERR) {
				first_err = CL_EINVAL;
			}
			goto cleanup;
		}

		// Save the cluster name
		xml_objects.start();
		cluster_name = xml_objects.getValue();
	}

	// Get cluster properties
	clerrno = cl_xml.getObjProps(CLUSTER_CMD, cluster_name,
	    clconfiguration, cl_props);
	if (clerrno != CL_NOERR) {
		if (first_err == CL_NOERR) {
			first_err = clerrno;
		}
		goto cleanup;
	}

	// Get local node name
	local_node[0] = '\0';
	(void) gethostname(local_node, sizeof (local_node));

	// Open the input file for scinstall
	(void) sprintf(input_fname, "/tmp/%s.%d", cmd.commandname, getpid());
	input_fp = fopen(input_fname, "w");
	if (input_fp == NULL) {
		clerror("Cannot create file \"%s\".\n", CCR_CONF_FILE);
		if (first_err == CL_NOERR) {
			first_err = CL_EIO;
		}
		goto cleanup;
	}

	// Read all the nodes configured in the xml file
	xml_objects.clear();
	clerrno = cl_xml.getAllObjectNames(CLNODE_CMD, clconfiguration,
	    &xml_objects);
	if (clerrno != CL_NOERR) {
		if (first_err == CL_NOERR) {
			first_err = clerrno;
		}
		goto cleanup;
	}

	// No nodes specified?
	if (xml_objects.getSize() == 0) {
		clerror("No nodes are specified to create a cluster.\n");
		if (first_err == CL_NOERR) {
			first_err = CL_EINVAL;
		}
		goto cleanup;
	}

	// Go over the nodes
	counter = 1;
	node_num = 0;
	for (xml_objects.start(); !xml_objects.end(); xml_objects.next()) {
		char *node_name = xml_objects.getValue();
		if (!node_name) {
			continue;
		}

		// Write the node name
		if (strcmp(node_name, local_node) == 0) {
			node_num = 0;
		} else {
			sponsor_node = node_name;
			node_num = counter++;
		}
		(void) fprintf(input_fp,
		    "SC_ARGVAR_NODENAME[%d]=\"%s\"\n",
		    node_num, node_name);

		// State
		(void) fprintf(input_fp,
		    "SC_ARGVAR_STATE[%d]=\"1\"\n", node_num);

		// Add globale dev file system mount point
		cl_objprops->clear();
		clerrno = cl_xml.getObjProps(CLNODE_CMD, node_name,
		    clconfiguration, cl_objprops);
		if (clerrno != CL_NOERR) {
			if (first_err == CL_NOERR) {
				first_err = clerrno;
			}
			goto cleanup;
		}
		for (cl_objprops->start(); !cl_objprops->end();
		    cl_objprops->next()) {
			char *name = cl_objprops->getName();
			if (!name || (strcmp(name, "globaldevfs") != 0)) {
				continue;
			}

			// Add global dev fs
			name = cl_objprops->getValue();
			if (name && (strlen(name) != 0)) {
				(void) fprintf(input_fp,
				    "SC_ARGVAR_GDIR[%d]=\"%s\"\n",
				    node_num, name);
				break;
			}
			break;
		}

		// Get endpoints
		clerrno = cl_xml.getCableProps(node_name,
		    clconfiguration, &cable_props);
		if (clerrno != CL_NOERR) {
			delete cable_props;
			cable_props = NULL;
			if (first_err == CL_NOERR) {
				first_err = clerrno;
			}
			goto cleanup;
		}

		// go through the endpoints
		cable_count = 0;
		*switches = '\0';
		*switch_types = '\0';
		*adapters = '\0';
		*adapter_types = '\0';
		*ports = '\0';
		*pp_cable = '\0';

		// Check if it is direct connection
		direct_connect = 0;
		for (cable_p = cable_props; cable_p;
		    cable_p = cable_p->cable_next) {
			// Direct connection?
			if ((cable_p->epoint1.type == ADAPTER) &&
			    (cable_p->epoint2.type == ADAPTER)) {
				direct_connect++;
			}
			cable_count++;
		}

		if ((cable_count == 2) && (direct_connect == 1)) {
			clerror("The cables defined in file \"%s\" "
			    "for node \"%s\" has both direct-connect "
			    "and switches.\n",
			    clconfiguration, node_name);
			if (first_err == CL_NOERR) {
				first_err = CL_EINVAL;
			}
			goto cleanup;
		}

		// Go through the cables
		cable_count = 0;
		for (cable_p = cable_props; cable_p;
		    cable_p = cable_p->cable_next) {
			// Skip
			if (!cable_p->epoint1.name ||
			    !cable_p->epoint2.name) {
				continue;
			}
			if (direct_connect) {
				if (!cable_p->epoint1.node &&
				    !cable_p->epoint2.node) {
					continue;
				}
				if ((strcmp(cable_p->epoint1.node,
				    node_name) != 0) &&
				    (strcmp(cable_p->epoint2.node,
				    node_name) != 0)) {
					continue;
				}
			} else if (cable_p->epoint1.node &&
			    (strcmp(cable_p->epoint1.node,
			    node_name) != 0)) {
				continue;
			}

			if (!direct_connect) {
				// Add switch
				(void) strcat(switches, cable_p->epoint2.name);
				(void) strcat(switch_types, "switch");
				if (cable_p->epoint2.port) {
					(void) strcat(ports,
					    cable_p->epoint2.port);
				} else {
					(void) strcat(ports, "@");
				}
				(void) strcat(switches, " ");
				(void) strcat(switch_types, " ");
				(void) strcat(ports, " ");

				// Add adapters
				(void) strcat(adapters, cable_p->epoint1.name);
			} else {
				// Add adapters and the direct connection
				if (strcmp(cable_p->epoint1.node,
				    node_name) == 0) {
					(void) strcat(adapters,
					    cable_p->epoint1.name);
					(void) strcat(switches,
					    cable_p->epoint2.node);
					(void) strcat(switches, ":");
					(void) strcat(switches,
					    cable_p->epoint1.name);
				} else {
					(void) strcat(adapters,
					    cable_p->epoint2.name);
					(void) strcat(switches,
					    cable_p->epoint1.node);
					(void) strcat(switches, ":");
					(void) strcat(switches,
					    cable_p->epoint2.name);
				}
				(void) strcat(switches, " ");
			}
			(void) strcat(adapter_types, "dlpi");
			(void) strcat(adapters, " ");
			(void) strcat(adapter_types, " ");

			cable_count++;
			if (cable_count == 2) {
				break;
			}
		}

		// Must have endpoints specified
		if (!cable_count) {
			clerror("Cannot find one pair of endpoints "
			    "for node \"%s\".\n", node_name);
			if (first_err == CL_NOERR) {
				first_err = CL_EINVAL;
			}
			delete cable_props;
			cable_props = NULL;
		}

		// Add adapters, cables, and ports from endpoints
		(void) fprintf(input_fp, "SC_ARGVAR_ADAPTERS[%d]=\"%s\"\n",
		    node_num, adapters);
		(void) fprintf(input_fp, "SC_ARGVAR_TRTYPES[%d]=\"%s\"\n",
		    node_num, adapter_types);
		if (direct_connect) {
			(void) fprintf(input_fp,
			    "SC_ARGVAR_JUNCTIONS[%d]=\"\"\n", node_num);
		} else {
			(void) fprintf(input_fp,
			    "SC_ARGVAR_JUNCTIONS[%d]=\"%s\"\n",
			    node_num, switches);
		}
		(void) fprintf(input_fp, "SC_ARGVAR_JUNCTYPES[%d]=\"%s\"\n",
		    node_num, switch_types);

		(void) fprintf(input_fp, "SC_ARGVAR_E2CABLES[%d]=\"%s\"\n",
		    node_num, switches);
		(void) fprintf(input_fp, "SC_ARGVAR_E2PORTS[%d]=\"%s\"\n",
		    node_num, ports);

		if (direct_connect) {
			(void) fprintf(input_fp, "SC_ARGVAR_DIRECT=\"1\"\n");
		}

		delete cable_props;
		cable_props = NULL;
	}

	// Direct connect only applies to 2-node
	if ((counter != 2) && direct_connect) {
		clerror("Direct-connect is not allowed on a %d-node "
		    "cluster.\n", counter);
		if (first_err == CL_NOERR) {
			first_err  = CL_EINVAL;
		}
	}

	// Sponsor node?
	if (!sponsor_node) {
		if (counter > 1) {
			// Should not get here.
			clcommand_perror(CL_EINTERNAL);
			if (first_err == CL_NOERR) {
				first_err  = CL_EINTERNAL;
			}
		}
	}

	// Any errors?
	if (first_err != CL_NOERR) {
		goto cleanup;
	}

	// Write the sponsor node.
	node_num = 0;
	for (xml_objects.start(); !xml_objects.end(); xml_objects.next()) {
		char *node_name = xml_objects.getValue();
		if (!node_name) {
			continue;
		}

		// Write the node name
		(void) fprintf(input_fp, "SC_ARGVAR_SPONSORNODE[%d]=\"%s\"\n",
		    node_num++, sponsor_node);
	}

	// Write common setting: cluster name
	(void) fprintf(input_fp, "SC_ARGVAR_CLUSTERNAME=\"%s\"\n",
	    cluster_name);

	// Add privatenet and global_fencing properties etc
	for (cl_props->start(); !cl_props->end(); cl_props->next()) {
		char *prop_name = cl_props->getName();
		char *prop_value = cl_props->getValue();
		if (!prop_name || !prop_value) {
			continue;
		}

		if (strcmp(prop_name, CLPROP_PRIV_NETADDR) == 0) {
			(void) fprintf(input_fp,
			    "SC_ARGVAR_NETADDR=\"%s\"\n", prop_value);
		} else if (strcmp(prop_name,
		    CLPROP_PRIV_NETMASK) == 0) {
			(void) fprintf(input_fp,
			    "SC_ARGVAR_NETMASK=\"%s\"\n", prop_value);
		} else if (strcmp(prop_name,
		    CLPROP_PRIV_MAXNODE) == 0) {
			(void) fprintf(input_fp,
			    "SC_ARGVAR_MAXNODES=\"%s\"\n", prop_value);
		} else if (strcmp(prop_name,
		    CLPROP_PRIV_MAXPRIV) == 0) {
			(void) fprintf(input_fp,
			    "SC_ARGVAR_MAXPRIVATENETS=\"%s\"\n", prop_value);
		} else if (strcmp(prop_name,
		    CL_AUTHPROTOCOL) == 0) {
			(void) fprintf(input_fp,
			    "SC_ARGVAR_AUTHTYPE=\"%s\"\n", prop_value);
		} else if (strcmp(prop_name, CLPROP_FENCING) == 0) {
			(void) fprintf(input_fp,
			    "SC_ARGVAR_GLOBAL_FENCING=\"%s\"\n", prop_value);
		}
	}

	// No direct connection.
	if (!direct_connect) {
		(void) fprintf(input_fp, "SC_ARGVAR_TWONODES=\"1\"\n");
	}

	// no quorum autoconfig
	if (node_num != 2) {
		// For >2 and 1 node clusters, it's basic mandatory
		// things like resetting
		(void) fprintf(input_fp,
		    "SC_ARGVAR_AUTOQUORUMCONFIG_TASKNAME=\"quorum\"\n"
		    "SC_ARGVAR_AUTOQUORUMCONFIG_STATE=\"INIT\"\n");
	}

	(void) fclose(input_fp);
	input_fp = 0;

	// Set the arguments to scinstall
	cli_args[arg_count] = new char [strlen(CMD_SCINSTALL) + 1];
	(void) strcpy(cli_args[arg_count++], CMD_SCINSTALL);
	cli_args[arg_count] = new char [strlen("-i") + 1];
	(void) strcpy(cli_args[arg_count++], "-i");
	cli_args[arg_count] = new char [strlen("-I") + 1];
	(void) strcpy(cli_args[arg_count++], "-I");
	cli_args[arg_count] = new char [strlen("-f") + 1];
	(void) strcpy(cli_args[arg_count++], "-f");
	cli_args[arg_count] = new char [strlen(input_fname) + 1];
	(void) strcpy(cli_args[arg_count++], input_fname);
	cli_args[arg_count] = NULL;

	// Set the environement variables to scinstall
	cli_envs[env_count] = new char [strlen(cmd.commandname) + 6];
	(void) sprintf(cli_envs[env_count++], "PROG=%s", cmd.commandname);
	cli_envs[env_count] = NULL;

	clerrno = execve_command(CMD_SCINSTALL, cli_args, cli_envs);
	if (clerrno != CL_NOERR) {
		if ((clerrno == 1) && (first_err == CL_NOERR)) {
			first_err = CL_EINVAL;
		}
	}

cleanup:
	if (input_fp) {
		(void) fclose(input_fp);
	}

	(void) unlink(input_fname);
	delete cl_props;
	delete cl_objprops;

	for (int i = 0; i < arg_count; i++) {
		delete [] cli_args[i];
	}
	delete [] cli_args;

	for (int i = 0; i < env_count; i++) {
		delete [] cli_envs[i];
	}
	delete [] cli_envs;

	return (first_err);
}
