//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)cluster_list_cmds.cc	1.5	08/10/07 SMI"

//
// Process cluster "list"
//

#include "cluster.h"
#include "ClList.h"

#if SOL_VERSION >= __s10
#include <zone.h>
#include <sys/cladm_int.h>
#include <sys/quorum_int.h>
#include <sys/clconf_int.h>
#endif

typedef struct {
	char *cmd_name;
	char *cmd_shortname;
	char *cmd_description;
} cmd_list_t;

//
// cluster "list"
//
clerrno_t
cluster_list_cmds(ClCommand &cmd, ValueList &operands, optflgs_t optflgs)
{
	ClList print_list = ClList(cmd.isVerbose ? true : false);
	int i;
	clerrno_t clerrno = CL_NOERR;
	char *operand_name = (char *)0;
	cmd_list_t clcmd_list[] = {
		{"claccess", "claccess",
		(char *)gettext("Manage Sun Cluster access policies")},
		{"cldevice", "cldev",
		(char *)gettext("Manage Sun Cluster devices")},
		{"cldevicegroup", "cldg",
		(char *)gettext("Manage Sun Cluster device groups")},
		{"clinterconnect", "clintr",
		(char *)gettext("Manage Sun Cluster interconnects")},
		{"clnasdevice", "clnas",
		(char *)gettext(
		"Manage access to NAS device for Sun Cluster ")},
		{"clnode", "clnode",
		(char *)gettext("Manage Sun Cluster nodes")},
		{"clquorum", "clq",
		(char *)gettext("Manage Sun Cluster quorums")},
		{"clreslogicalhostname", "clrslh",
		(char *)gettext(
		"Manage Sun Cluster resources for logical host names")},
		{"clresource", "clrs",
		(char *)gettext(
		"Manage resources for Sun Cluster data services")},
		{"clresourcegroup", "clrg",
		(char *)gettext(
		"Manage resource groups for Sun Cluster data services")},
		{"clresourcetype", "clrt",
		(char *)gettext(
		"Manage resource types for Sun Cluster data services")},
		{"clressharedaddress", "clrssa",
		(char *)gettext(
		"Manage Sun Cluster resources for shared addresses")},
		{"clsetup", "clsetup",
		(char *)gettext("Configure Sun Cluster interactively")},
		{"clsnmphost", "clsnmphost",
		(char *)gettext("Administer Sun Cluster SNMP hosts")},
		{"clsnmpmib", "clmib",
		(char *)gettext("Administer Sun Cluster SNMP MIB")},
		{"clsnmpuser", "clsnmpuser",
		(char *)gettext("Administer Sun Cluster SNMP users")},
		{"cltelemetryattribute", "clta",
		(char *)gettext("Manage telemetry attributes monitoring")},
		{"cluster", "cluster",
		(char *)gettext(
		"Manage Sun Cluster global configuration and status")},
		{"clzonecluster", "clzc",
		(char *)gettext("Manage zone clusters")},
		{0, 0, 0}
	};

	// List of commands to be displayed from inside a zc
	cmd_list_t zccmd_list[] = {
		{"clnode", "clnode",
		(char *)gettext("Manage Sun Cluster nodes")},
		{"clreslogicalhostname", "clrslh",
		(char *)gettext(
		"Manage Sun Cluster resources for logical host names")},
		{"clresource", "clrs",
		(char *)gettext(
		"Manage resources for Sun Cluster data services")},
		{"clresourcegroup", "clrg",
		(char *)gettext(
		"Manage resource groups for Sun Cluster data services")},
		{"clresourcetype", "clrt",
		(char *)gettext(
		"Manage resource types for Sun Cluster data services")},
		{"clressharedaddress", "clrssa",
		(char *)gettext(
		"Manage Sun Cluster resources for shared addresses")},
		{"cluster", "cluster",
		(char *)gettext(
		"Manage Sun Cluster global configuration and status")},
		{0, 0, 0}
	};

	// Get the current cluster name, if specified
	if (operands.getSize() > 1) {
		clerrno = CL_EINTERNAL;
		clcommand_perror(clerrno);
		goto cleanup;

	} else if (operands.getSize() == 1) {
		char *cl_name = (char *)0;

		// Initialize clconf
		if (clconf_lib_init() != 0) {
			clerror("Error while initializing clconf library\n");
			clerrno = CL_EINTERNAL;
			goto cleanup;
		}

		// Get the cluster name
		cl_name = clconf_get_clustername();
		if (!cl_name) {
			clerrno = CL_EINTERNAL;
			clcommand_perror(clerrno);
			goto cleanup;
		}

		// The operand must be this cluster name
		operands.start();
		operand_name = operands.getValue();
		if (strcmp(operand_name, cl_name) != 0) {
			clerror("\"%s\" is not the name of this cluster.\n",
			    operand_name);
			clerrno = CL_ENOENT;
			goto cleanup;
		}
	}

	// If verbose, add the header
	if (optflgs & vflg) {
		print_list.setHeadings((char *)gettext("Command"),
		    (char *)gettext("Short name"),
		    (char *)gettext("Description"));
	}

#if (SOL_VERSION >= __s10)
	if (getzoneid() != GLOBAL_ZONEID) {
		// Add the command to ZcList object.
		for (i = 0; zccmd_list[i].cmd_name; i++) {
			print_list.addRow(zccmd_list[i].cmd_name,
				zccmd_list[i].cmd_shortname,
				zccmd_list[i].cmd_description);
		}
		goto skip_global;
	}
#endif

	// Add the commands to the ClList object.
	for (i = 0; clcmd_list[i].cmd_name; i++) {
		print_list.addRow(clcmd_list[i].cmd_name,
		    clcmd_list[i].cmd_shortname,
		    clcmd_list[i].cmd_description);
	}

skip_global:
	// Print it
	print_list.print();

cleanup:
	return (clerrno);
}
