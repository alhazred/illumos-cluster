//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)cluster_list.cc	1.2	08/05/20 SMI"

//
// Process cluster "list"
//

#include "cluster.h"
#include "ClList.h"

//
// cluster "list"
//
clerrno_t
cluster_list(ClCommand &cmd, ValueList &operands, optflgs_t optflgs)
{
	ClList print_list = ClList(cmd.isVerbose ? true : false);
	clerrno_t clerrno = CL_NOERR;
	char *cl_name = (char *)0;
	char *operand_name = (char *)0;

	// Check input
	if (operands.getSize() > 1) {
		clerrno = CL_EINTERNAL;
		clcommand_perror(clerrno);
		return (clerrno);
	}

	// Get cluster name
	if (clconf_lib_init() != 0) {
		clerror("Cannot initialize clconf library.\n");
		return (CL_EINTERNAL);
	}
	cl_name = clconf_get_clustername();
	if (!cl_name) {
		clerror("Cannot get the cluster name.\n");
		return (CL_EINTERNAL);
	}

	// The operand, if specified, must be this cluster name
	if (operands.getSize() == 1) {
		operands.start();
		operand_name = operands.getValue();
		if (operand_name && (strcmp(operand_name, cl_name) != 0)) {
			clerror("\"%s\" is not the name of this cluster.\n",
			    operand_name);
			return (CL_ENOENT);
		}
	}

	// Set headings
	if (cmd.isVerbose || (optflgs & vflg)) {
		print_list.setHeadings((char *)gettext("Cluster"));
	}

	// Add the cluster name to ClList
	print_list.addRow(cl_name);

	// Print it
	print_list.print();

	return (clerrno);
}
