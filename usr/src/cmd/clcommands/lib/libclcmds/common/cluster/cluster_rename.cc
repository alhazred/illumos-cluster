//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)cluster_rename.cc	1.2	08/05/20 SMI"

//
// Process cluster "rename"
//

#include "cluster.h"
#include "scadmin/scconf.h"

//
// cluster "rename"
//
clerrno_t
cluster_rename(ClCommand &cmd, ValueList &operands, optflgs_t optflgs,
    char *new_clname)
{
	clerrno_t clerrno = CL_NOERR;
	char *cl_name = (char *)0;
	scconf_errno_t scconf_err = SCCONF_NOERR;
	char *msgbuffer = (char *)0;

	// Check input
	if (!new_clname || (operands.getSize() > 1)) {
		clerrno = CL_EINTERNAL;
		clcommand_perror(clerrno);
		return (clerrno);
	}

	// Get the current cluster name
	if (clconf_lib_init() != 0) {
		clerror("Cannot initialize clconf library.\n");
		return (CL_EINTERNAL);
	}
	cl_name = clconf_get_clustername();
	if (!cl_name) {
		clerror("Cannot get the cluster name.\n");
		return (CL_EINTERNAL);
	}

	// The operand must be this cluster name
	if (operands.getSize() == 1) {
		char *operand_name = (char *)0;

		operands.start();
		operand_name = operands.getValue();
		if (strcmp(operand_name, cl_name) != 0) {
			clerror("\"%s\" is not the name of this cluster.\n",
			    operand_name);
			clerrno = CL_ENOENT;
			goto cleanup;
		}
	}

	// Check with the new cluster name
	if (strcmp(new_clname, cl_name) == 0) {
		if (cmd.isVerbose) {
			clmessage("Cluster name remains to be \"%s\".\n",
			    new_clname);
		}
		goto cleanup;
	}

	// Update the cluster name
	scconf_err = scconf_change_clustername(new_clname, &msgbuffer);
	if (scconf_err != SCCONF_NOERR) {
		// Print detailed error messages
		if (msgbuffer != (char *)0) {
			clcommand_dumpmessages(msgbuffer);
		}

		clerrno = cluster_conv_scconf_err(1, scconf_err);
	} else if (optflgs & vflg) {
		clmessage("Cluster name is changed to \"%s\".\n",
		    new_clname);
	}

cleanup:
	return (clerrno);
}
