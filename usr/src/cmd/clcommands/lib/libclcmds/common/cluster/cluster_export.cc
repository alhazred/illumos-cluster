//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident   "@(#)cluster_export.cc	1.6	08/05/21 SMI"

//
// Process cluster "export"
//

#include "cluster.h"
#include "ClXml.h"
#include "libdid.h"

#include "ClCommand.h"

#define	GET_PROP	1
#define	GET_CLNAME	2

static bool
add_valid_cltype(char *cl_type, ValueList &valid_cltypes);

//
// This function returns a ClusterExport object populated with
// all of the cluster information from the cluster.  Caller is
// responsible for releasing the ClcmdExport object.
//
ClcmdExport *
get_cluster_xml_elements(uint_t get_flg)
{
	ClusterExport *cluster = NULL;
	clconf_cluster_t *clconf = (clconf_cluster_t *)0;
	scconf_errno_t scconf_err = SCCONF_NOERR;
	const char *cluster_name = (char *)0;
	const char *conf_val = (char *)0;
	int i;
	bool readonly_prop;
	char udp_dflt_str[16];
	unsigned short globalfencing = GLOBAL_FENCING_UNKNOWN;

	// Get cluster clconf handle
	scconf_err = scconf_cltr_openhandle((scconf_cltr_handle_t *)&clconf);
	if (scconf_err != SCCONF_NOERR) {
		(void) cluster_conv_scconf_err(1, scconf_err);
		return (cluster);
	}

	// Get cluster name
	cluster_name = clconf_obj_get_name((clconf_obj_t *)clconf);
	if (!cluster_name) {
		clcommand_perror(CL_EINTERNAL);
		goto cleanup;
	}


	// Add cluster name to ClusterExport object.
	cluster = new ClusterExport((char *)cluster_name);

	if (get_flg == GET_CLNAME) {
		// Done.
		goto cleanup;
	} else if (get_flg != GET_PROP) {
		clcommand_perror(CL_EINTERNAL);
		goto cleanup;
	}

	// Get the cluster properties.
	for (i = 0; cluster_props[i].prop_cli_name; i++) {
		conf_val = clconf_obj_get_property((clconf_obj_t *)clconf,
		    cluster_props[i].prop_ccr_name);
		if (cluster_props[i].prop_attr == CLPROP_READONLY) {
			readonly_prop = true;
		} else {
			readonly_prop = false;
		}
		//
		// We should not get a value of "default" but if we do,
		// convert it.
		//
		if ((strcmp(cluster_props[i].prop_ccr_name,
		    CLPROP_UDP_SESSION_TIMEOUT) == 0) &&
		    (!conf_val || strcasecmp(conf_val, "default") == 0)) {
			sprintf(udp_dflt_str, "%d", UDP_SESSION_TIMEOUT_DFLT);
			(void) cluster->addProperty(
			    (char *)cluster_props[i].prop_cli_name,
			    udp_dflt_str, readonly_prop);
		} else {
			(void) cluster->addProperty(
			    (char *)cluster_props[i].prop_cli_name,
			    (char *)conf_val, readonly_prop);
		}
	}

	// Get the global_fencing property
	if (did_initlibrary(1, DID_NONE, NULL) == DID_INIT_SUCCESS) {
		(void) did_getglobalfencingstatus(&globalfencing);
		switch (globalfencing) {
		case GLOBAL_FENCING_PATHCOUNT:
			(void) cluster->addProperty(
			    CLPROP_FENCING, (char *)"pathcount",
			    false);
			break;
		case GLOBAL_FENCING_PREFER3:
			(void) cluster->addProperty(
			    CLPROP_FENCING, (char *)"prefer3", false);
			break;
		case GLOBAL_NO_FENCING:
			(void) cluster->addProperty(
			    CLPROP_FENCING, (char *)"nofencing",
			    false);
			break;
		case GLOBAL_FENCING_UNKNOWN:
		default:
			(void) cluster->addProperty(
			    CLPROP_FENCING, (char *)gettext("Unknown"),
			    false);
			    break;
		}
	}

	// Add nodes authentication type
	scconf_authtype_t authtype;
	if (scconf_get_secure_authtype(&authtype) != CL_NOERR) {
		clcommand_perror(CL_EINTERNAL);
	}
	switch (authtype) {
	case SCCONF_AUTH_SYS:
		(void) cluster->addProperty(
		    (char *)CL_AUTHPROTOCOL, (char *)"sys");
		break;
	case SCCONF_AUTH_DH:
		(void) cluster->addProperty(
		    (char *)CL_AUTHPROTOCOL, (char *)"des");
		break;
	default:
		(void) cluster->addProperty(
		    (char *)CL_AUTHPROTOCOL, (char *)"unknown");
		break;
	}

cleanup:
	// Release the cluster config
	if (clconf != (clconf_cluster_t *)0) {
		clconf_obj_release((clconf_obj_t *)clconf);
	}

	return (cluster);
}

clerrno_t
cluster_export(ClCommand &cmd, ValueList &operands, optflgs_t optflgs,
    ValueList &cltypes, char *clconfiguration)
{
	ClXml cluster_clxml;
	ClcmdExport *cl_cluster_export = NULL;
	clerrno_t clerrno = CL_NOERR;
	clerrno_t first_err = CL_NOERR;
	char *export_file = (char *)0;
	ValueList valid_cltypes = ValueList(true);

	// Check inputs
	if (!clconfiguration) {
		export_file = strdup("-");
	} else {
		export_file = strdup(clconfiguration);
	}
	if (!export_file) {
		clerrno = first_err = CL_ENOMEM;
		clcommand_perror(clerrno);
		goto cleanup;
	}

	// The operand must be this cluster name
	if (operands.getSize() == 1) {
		char *operand_name = (char *)0;

		operands.start();
		operand_name = operands.getValue();
		clerrno = cluster_name_compare(operand_name);
		if (clerrno != CL_NOERR) {
			goto cleanup;
		}
	}

	// Go through the object types
	if (cltypes.getSize() != 0) {
		// Check for valid types
		for (cltypes.start(); !cltypes.end(); cltypes.next()) {
			char *cl_type = cltypes.getValue();

			if (!cl_type)
				continue;

			if (!add_valid_cltype(cl_type, valid_cltypes)) {
				if (first_err == CL_NOERR) {
					first_err = CL_ETYPE;
				}
			}
		}

		// If nothing to show?
		if (valid_cltypes.getSize() == 0) {
			goto cleanup;
		}
	}

	// Get the cluster xml elements
	if ((cltypes.getSize() == 0) || valid_cltypes.isValue(TYPE_GLOBAL)) {
		cl_cluster_export = get_cluster_xml_elements(GET_PROP);
	} else {
		cl_cluster_export = get_cluster_xml_elements(GET_CLNAME);
	}

	// Create xml
	clerrno = cluster_clxml.createExportFile(CLUSTER_CMD, export_file,
	    cl_cluster_export, &valid_cltypes);
	if ((clerrno != CL_NOERR) && (first_err == CL_NOERR)) {
		first_err = clerrno;
	}

cleanup:
	// Free memory
	if (export_file) {
		free(export_file);
	}

	delete cl_cluster_export;

	// Return the first error
	if (first_err != CL_NOERR) {
		clerrno = first_err;
	}

	return (clerrno);
} //lint !e715

//
// add_valid_cltype
//
//	Check if the given cluster object type is known and has
//	xml related subcommands. Returns true if it has; O.w.
//	return false.
//
bool
add_valid_cltype(char *cl_type, ValueList &valid_cltypes)
{
	int i;

	if (!cl_type) {
		return (false);
	}

	for (i = 0; cl_objtypes[i].obj_name; i++) {
		if ((strcasecmp(cl_type, cl_objtypes[i].obj_name) == 0) ||
		    (strcasecmp(cl_type, cl_objtypes[i].obj_shortname) == 0)) {
			if (cl_objtypes[i].has_xml) {
				valid_cltypes.add(cl_objtypes[i].obj_name);
				return (true);
			} else {
				clerror("Object type \"%s\" cannot be "
				    "exported.\n", cl_type);
				return (false);
			}
		}
	}

	clerror("Unknown object type \"%s\".\n", cl_type);
	return (false);
}
