//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)cluster_set.cc	1.7	08/05/21 SMI"

//
// Process cluster "set"
//

#include "cluster.h"
#include "clnode.h"
#include "didadm.h"

#define	CMD_SCDIDADM   "/usr/cluster/bin/scdidadm"

static clerrno_t
cluster_set_installmode(char *prop_name, char *prop_val, ClCommand &cmd);
static clerrno_t
cluster_set_heartbeat(char *hb_name, char *hb_val, optflgs_t optflgs);
static clerrno_t
cluster_set_globalfencing(ClCommand &cmd, char *prop_name, char *prop_val,
    optflgs_t optflgs);
static clerrno_t
cluster_set_udp_session_timeout(char *udp_session_timeout, optflgs_t optflgs);

//
// cluster "set"
//
clerrno_t
cluster_set(ClCommand &cmd, ValueList &operands, optflgs_t optflgs,
    NameValueList &properties)
{
	scconf_errno_t scconf_err = SCCONF_NOERR;
	clerrno_t clerrno = CL_NOERR;
	clerrno_t first_err = CL_NOERR;
	char *prop_name, *prop_val;
	char *hb_timeout = (char *)0;
	char *hb_quantum = (char *)0;
	char *udp_session_timeout = (char *)0;
	int quantum_int = 0;
	int timeout_int = 0;
	char *endp;
	char *old_timeout = (char *)0;
	char *old_quantum = (char *)0;
	long udp_int = 0;
	int udp_dflt_used = 0;
	char udp_dflt_str[16];

	// Check inputs
	if ((properties.getSize() == 0) || (operands.getSize() > 1)) {
		clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}

	// The operand must be this cluster name
	if (operands.getSize() == 1) {
		char *operand_name = (char *)0;

		operands.start();
		operand_name = operands.getValue();
		clerrno = cluster_name_compare(operand_name);
		if (clerrno != CL_NOERR) {
			goto cleanup;
		}
	}

	// Go through the properties
	for (properties.start(); !properties.end(); properties.next()) {
		// Get property name and value
		prop_name = properties.getName();
		prop_val = properties.getValue();
		if (!prop_name)
			continue;

		// Call different functions per the property name
		if (strcmp(prop_name, CLPROP_INSTALLMODE) == 0) {
			clerrno = cluster_set_installmode(
			    prop_name, prop_val, cmd);
			if ((clerrno != CL_NOERR) &&
			    (first_err == CL_NOERR)) {
				first_err = clerrno;
			}
			continue;
		} else if (strcmp(prop_name, CLPROP_HB_TIMEOUT) == 0) {
			// Check the property value
			hb_timeout = properties.getValue();
			if (!hb_timeout || (strcmp(hb_timeout, "") == 0)) {
				// This is using the default setting.
				char *tmp_quantum;

				// Get default timeout
				scconf_err = scconf_get_heartbeat(
				    &hb_timeout, B_TRUE,
				    &tmp_quantum, B_FALSE);
				if (tmp_quantum) {
					free(tmp_quantum);
				}
				if (scconf_err != SCCONF_NOERR) {
					clerrno = cluster_conv_scconf_err(
					    1, scconf_err);
					if (first_err == CL_NOERR) {
						first_err = clerrno;
					}
					continue;
				}
			}

			// Convert to a number.
			timeout_int = strtol(hb_timeout, &endp, 10);
			if (*endp != NULL) {
				clerror("The \"%s\" value \"%s\" is not "
				    "a number.\n",
				    CLPROP_HB_TIMEOUT, hb_timeout);
				if (first_err == CL_NOERR) {
					first_err = CL_EINVAL;
				}
				timeout_int = -1;
				hb_timeout = (char *)0;
			}
			continue;
		} else if (strcmp(prop_name, CLPROP_HB_QUANTUM) == 0) {
			// Check the property value
			hb_quantum = properties.getValue();
			if (!hb_quantum || (strcmp(hb_quantum, "") == 0)) {
				// This is using the default setting.
				char *tmp_timeout;

				scconf_err = scconf_get_heartbeat(
				    &tmp_timeout, B_FALSE,
				    &hb_quantum, B_TRUE);
				if (tmp_timeout) {
					free(tmp_timeout);
				}
				if (scconf_err != SCCONF_NOERR) {
					clerrno = cluster_conv_scconf_err(
					    1, scconf_err);
					if (first_err == CL_NOERR) {
						first_err = clerrno;
					}
					continue;
				}
			}

			// Convert to a number.
			quantum_int = strtol(hb_quantum, &endp, 10);
			if (*endp != NULL) {
				clerror("The \"%s\" value \"%s\" is not a "
				    "number.\n",
				    CLPROP_HB_QUANTUM, hb_quantum);
				if (first_err == CL_NOERR) {
					first_err = CL_EINVAL;
				}
				quantum_int = -1;
				hb_quantum = (char *)0;
			}
			continue;
		} else if (strcmp(prop_name, CLPROP_FENCING) == 0) {
			// Set global fencing property
			clerrno = cluster_set_globalfencing(cmd,
			    prop_name, prop_val, optflgs);
			if ((clerrno != CL_NOERR) &&
			    (first_err == CL_NOERR)) {
				first_err = clerrno;
			}
			continue;
		} else if (strcmp(prop_name, CLPROP_UDP_SESSION_TIMEOUT) == 0) {
			// Check the property value
			udp_session_timeout = properties.getValue();
			if (!udp_session_timeout ||
			    (strcmp(udp_session_timeout, "") == 0)) {
				// This is using the default setting.
				udp_int = UDP_SESSION_TIMEOUT_DFLT;
				udp_dflt_used = 1;
			} else {
				// Convert to a number.
				udp_int = strtol(udp_session_timeout, &endp,
				    10);
				if (*endp != NULL) {
					clerror("The \"%s\" value \"%s\" is "
					    "not a number.\n",
					    CLPROP_UDP_SESSION_TIMEOUT,
					    udp_session_timeout);
					if (first_err == CL_NOERR) {
						first_err = CL_EINVAL;
					}
					udp_int = -1;
				}
			}

			if (udp_int != -1) {
				if (udp_dflt_used == 1) {
					sprintf(udp_dflt_str, "%d",
					    UDP_SESSION_TIMEOUT_DFLT);
					clerrno =
					    cluster_set_udp_session_timeout(
					    udp_dflt_str, optflgs);
				} else {
					clerrno =
					    cluster_set_udp_session_timeout(
					    udp_session_timeout, optflgs);
				}
				if ((clerrno != CL_NOERR) &&
				    (first_err == CL_NOERR)) {
					first_err = clerrno;
				}
			}

			continue;
		} else {
			// Can this property be changed in cluster mode?
			switch (cluster_check_property(prop_name)) {
			case CLPROP_READONLY:
				clerror("Cannot set read-only property "
				    "\"%s\".\n", prop_name);
				if (first_err == CL_NOERR) {
					first_err = CL_EOP;
				}
				break;
			case CLPROP_NONCLUSTER:
				//
				// For now, only set-nerprops is used
				// to modify noncluster mode properties.
				// So give the subcommand name to better
				// help the user. But this won't be proper
				// if more subcommands are added to set
				// properties in noncluster mode.
				//
				clerror("User the \"set-netprops\" "
				    "subcommand in noncluster mode "
				    "to set \"%s\".\n", prop_name);
				if (first_err == CL_NOERR) {
					first_err = CL_EOP;
				}
				break;
			case CLPROP_CLUSTER:
			case CLPROP_BOTH:
				break;
			case CLPROP_UNKNOWN:
			default:
				clerror("Invalid property \"%s\".\n",
				    prop_name);
				if (first_err == CL_NOERR) {
					first_err = CL_EPROP;
				}
				break;
			}
		}
	}

	// Heartbeat changes
	if (hb_timeout || hb_quantum) {
		//
		// If both are specified to change, need to find the right order
		// to set them. We can only change it one by one if both are set
		// to different values, due to some issues in transport.
		//
		if (hb_timeout && hb_quantum) {
			int old_timeout_int, old_quantum_int;

			// Get the current heartbeat values.
			scconf_err = scconf_get_heartbeat(&old_timeout,
			    B_FALSE, &old_quantum, B_FALSE);
			if (scconf_err != SCCONF_NOERR) {
				clerrno = cluster_conv_scconf_err(
				    1, scconf_err);

				if (first_err == CL_NOERR) {
					first_err = clerrno;
				}
				goto cleanup;
			}

			// Convert current heartbeats to numeric values.
			old_timeout_int = old_quantum_int = 0;

			old_timeout_int = strtol(old_timeout, &endp, 10);
			if (*endp != NULL) {
				if (first_err == CL_NOERR) {
					first_err = CL_EINTERNAL;
				}
				old_timeout_int = -1;
			}

			old_quantum_int = strtol(old_quantum, &endp, 10);
			if (*endp != NULL) {
				if (first_err == CL_NOERR) {
					first_err = CL_EINTERNAL;
				}
				old_quantum_int = -1;
			}

			//
			// Due to constraints in the lower transport code, when
			// decrease both timeout and quantum, set quantum first;
			// For all other cases, set timeout first.
			//
			if ((timeout_int != -1) && (quantum_int != -1) &&
			    (timeout_int < old_timeout_int) &&
			    (quantum_int < old_quantum_int)) {
				// Update quantum first.
				clerrno = cluster_set_heartbeat(
				    CLPROP_HB_QUANTUM, hb_quantum, optflgs);
				if ((clerrno != CL_NOERR) &&
				    (first_err == CL_NOERR)) {
					first_err = clerrno;
				}

				// Then update timeout
				clerrno = cluster_set_heartbeat(
				    CLPROP_HB_TIMEOUT, hb_timeout, optflgs);
				if ((clerrno != CL_NOERR) &&
				    (first_err == CL_NOERR)) {
					first_err = clerrno;
				}
			} else {
				// Update timeout first
				if ((timeout_int != -1) &&
				    (old_timeout_int != -1)) {
					clerrno = cluster_set_heartbeat(
					    CLPROP_HB_TIMEOUT, hb_timeout,
					    optflgs);
					if ((clerrno != CL_NOERR) &&
					    (first_err == CL_NOERR)) {
						first_err = clerrno;
					}
				}

				// Then update quantum
				if ((quantum_int != -1) &&
				    (old_quantum_int != -1)) {
					clerrno = cluster_set_heartbeat(
					    CLPROP_HB_QUANTUM, hb_quantum,
					    optflgs);
					if ((clerrno != CL_NOERR) &&
					    (first_err == CL_NOERR)) {
						first_err = clerrno;
					}
				}
			}
			free(hb_timeout);
			free(hb_quantum);
		} else if (hb_timeout) {
			clerrno = cluster_set_heartbeat(CLPROP_HB_TIMEOUT,
			    hb_timeout, optflgs);
			if ((clerrno != CL_NOERR) &&
			    (first_err == CL_NOERR)) {
				first_err = clerrno;
			}
			free(hb_timeout);
		} else if (hb_quantum) {
			clerrno = cluster_set_heartbeat(CLPROP_HB_QUANTUM,
			    hb_quantum, optflgs);
			if ((clerrno != CL_NOERR) &&
			    (first_err == CL_NOERR)) {
				first_err = clerrno;
			}
			free(hb_quantum);
		}
	}

cleanup:
	if (first_err != CL_NOERR) {
		clerrno = first_err;
	}

	if (old_timeout) {
		free(old_timeout);
	}

	if (old_quantum) {
		free(old_quantum);
	}

	return (clerrno);
}

//
// cluster_set_installmode
//
//	Modify the cluster installmode property
//
clerrno_t
cluster_set_installmode(char *prop_name, char *prop_val, ClCommand &cmd)
{
	clerrno_t clerrno = CL_NOERR;
	scconf_errno_t scconf_err = SCCONF_NOERR;

	// Check inputs
	if (!prop_name) {
		clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}

	// Check the property value
	if (!prop_val || (strcmp(prop_val, "") == 0)) {
		clerror("You must specify the value of "
		    "\"%s\".\n", prop_name);
		return (CL_EINVAL);
	}

	// Enable install mode?
	if (strcasecmp(prop_val, "enabled") == 0) {
		// Set installmode
		scconf_err = scconf_set_quorum_installflag();

		if (scconf_err != SCCONF_NOERR) {
			clerror("Cannot enable \"%s\".\n", prop_name);
			clerrno = cluster_conv_scconf_err(
			    1, scconf_err);
		} else if (cmd.isVerbose) {
			clmessage("\"%s\" is set to "
			    "\"enabled\".\n", prop_name);
		}
	} else if (strcasecmp(prop_val, "disabled") == 0) {
		// Disable installmode
		scconf_err = scconf_clear_quorum_installflag();

		if (scconf_err != SCCONF_NOERR) {
			clerror("Cannot disable \"%s\".\n", prop_name);
			clerrno = cluster_conv_scconf_err(
			    1, scconf_err);
		} else if (cmd.isVerbose) {
			clmessage("\"%s\" is set to "
			    "\"disabled\".\n", prop_name);
		}
	} else {
		clerror("\"%s\" is invalid value for \"%s\".\n",
		    prop_val, prop_name);
		clerrno = CL_EINVAL;
	}

	return (clerrno);
}


//
// cluster_set_heartbeat
//
//	Update the heartbeat parameter, one at time.
//
clerrno_t
cluster_set_heartbeat(char *hb_name, char *hb_val, optflgs_t optflgs)
{
	char hb_props[SCCONF_MAXSTRINGLEN];
	clerrno_t clerrno = CL_NOERR;
	scconf_errno_t scconf_err = SCCONF_NOERR;
	char *msgbuffer = (char *)0;

	// Check inputs
	if (!hb_name || !hb_val) {
		clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}

	// Call scconf_change_heartbeat().
	(void) sprintf(hb_props, "%s=%s", hb_name, hb_val);
	scconf_err = scconf_change_heartbeat(hb_props, &msgbuffer);

	if (scconf_err != SCCONF_NOERR) {
		// Print detailed messages
		if (msgbuffer != (char *)0) {
			clcommand_dumpmessages(msgbuffer);
			free(msgbuffer);
		}

		switch (scconf_err) {
		case SCCONF_TM_EBADOPTS:
			clerrno = CL_EPROP;
			clerror("Invalid property name \"%s\".\n", hb_name);
			break;
		case SCCONF_ESETUP:
			clerrno = CL_EINVAL;
			break;
		case SCCONF_ERANGE:
			clerrno = CL_EINVAL;
			clerror("The \"%s\" value \"%s\" is out of range.\n",
			    hb_name, hb_val);
			break;
		default:
			clerrno = cluster_conv_scconf_err(1,
			    scconf_err);
			break;
		} //lint !e788

	} else if (optflgs & vflg) {
		clmessage("\"%s\" for cluster interconnect is set to "
		    "\"%s\".\n", hb_name, hb_val);
	}

	return (clerrno);
}

//
// cluster_set_globalfencing
//
//	Set the global_fencing property
//
clerrno_t
cluster_set_globalfencing(ClCommand &cmd, char *prop_name, char *prop_val,
    optflgs_t optflgs)
{
	clerrno_t clerrno = CL_NOERR;
	char *gfencing = (char *)0;

	// Check input
	if (!prop_name || !prop_val ||
	    (strcmp(prop_name, CLPROP_FENCING) != 0)) {
		clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}

	// Check the property value
	if (*prop_val == '\0') {
		clerror("You must specify a value to "
		    "property \"%s\".\n",
		    prop_name);
		return (CL_EINVAL);
	} else if (strcasecmp(prop_val, "prefer3") == 0) {
		gfencing = new char[strlen("prefer3") + 1];
		(void) strcpy(gfencing, "prefer3");
	} else if (strcasecmp(prop_val, "pathcount") == 0) {
		gfencing = new char[strlen("pathcount") + 1];
		(void) strcpy(gfencing, "pathcount");
	} else if (strcasecmp(prop_val, "nofencing") == 0) {
		gfencing = new char[strlen("nofencing") + 1];
		(void) strcpy(gfencing, "nofencing");
	} else if (strcasecmp(prop_val, "nofencing-noscrub") == 0) {
		gfencing = new char[strlen("nofencing-noscrub") + 1];
		(void) strcpy(gfencing, "nofencing-noscrub");
	} else {
		clerror("Invalid value \"%s\" for property \"%s\".\n",
		    prop_val, prop_name);
		return (CL_EINVAL);
	}

	// Set the property
	char env[BUFSIZ];
	char **cli_args = new char *[4];
	int arg_count = 0;

	// Set the envs
	(void) sprintf(env, "%s=%s", CLCMD_ENV,
	    cmd.commandname);
	if (putenv(env) != 0) {
		clcommand_perror(CL_ENOMEM);
		return (CL_ENOMEM);
	}

	// Set the arguments
	cli_args[arg_count] = new char [strlen(CMD_SCDIDADM) + 1];
	(void) strcpy(cli_args[arg_count++], CMD_SCDIDADM);
	cli_args[arg_count] = new char [strlen("-G") + 1];
	(void) strcpy(cli_args[arg_count++], "-G");
	cli_args[arg_count] = new char [strlen(gfencing) + 1];
	(void) strcpy(cli_args[arg_count++], gfencing);
	cli_args[arg_count] = NULL;

	// Call CMD_SCDIDAM
	clerrno = execve_command(CMD_SCDIDADM, cli_args, NULL);
	if ((clerrno == CL_NOERR) && (optflgs & vflg)) {
		clmessage("\"%s\" is set to \"%s\".\n",
		    prop_name, gfencing);
	}

	// Free memory
	delete [] gfencing;
	for (int i = 0; i < arg_count; i++) {
		delete [] cli_args[i];
	}
	delete [] cli_args;

	return (clerrno);
}

//
// Set udp_session_timeout
//
static clerrno_t
cluster_set_udp_session_timeout(char *udp_session_timeout, optflgs_t optflgs)
{
	char *msgbuffer = (char *)0;
	char *udp_props = (char *)0;
	clerrno_t clerrno = CL_NOERR;
	scconf_errno_t scconf_err = SCCONF_NOERR;

	udp_props = new char[strlen(CLPROP_UDP_SESSION_TIMEOUT) +
	    strlen(udp_session_timeout) + 10];

	// Call scconf_change_udp_timeout().
	(void) sprintf(udp_props, "%s=%s", CLPROP_UDP_SESSION_TIMEOUT,
	    udp_session_timeout);
	scconf_err = scconf_change_udp_timeout(udp_props, &msgbuffer);
	if (scconf_err != SCCONF_NOERR) {
		// Print detailed messages
		if (msgbuffer != (char *)0) {
			clcommand_dumpmessages(msgbuffer);
			free(msgbuffer);
		}

		switch (scconf_err) {
		case SCCONF_ESETUP:
			clerrno = CL_EINVAL;
			break;
		case SCCONF_ERANGE:
			clerrno = CL_EINVAL;
			clerror("The \"%s\" value \"%s\" is out of range.\n",
			    CLPROP_UDP_SESSION_TIMEOUT, udp_session_timeout);
			break;
		default:
			clerrno = cluster_conv_scconf_err(1,
			    scconf_err);
			break;
		} //lint !e788

	} else if (optflgs & vflg) {
		clmessage("\"%s\" for cluster is set to \"%s\".\n",
		    CLPROP_UDP_SESSION_TIMEOUT, udp_session_timeout);
	}
	delete [] udp_props;
	return (clerrno);
}
