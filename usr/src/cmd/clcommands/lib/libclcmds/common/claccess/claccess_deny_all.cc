//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)claccess_deny_all.cc	1.3	08/05/20 SMI"

//
// Process claccess "deny_all"
//

#include "claccess.h"

//
// claccess "deny_all"
//
clerrno_t
claccess_deny_all(ClCommand &cmd)
{
	scconf_errno_t scconf_err = SCCONF_NOERR;
	clerrno_t clerrno = CL_NOERR;
	char *host = ".";

	// If the special "hostname" of "." is added, all other names are
	// cleared. This would deny all the hosts from joining the cluster.
	scconf_err = scconf_addto_secure_joinlist(host);
	if (scconf_err != SCCONF_NOERR) {
		clerrno = claccess_conv_scconf_err(scconf_err);
		claccess_perror(clerrno);
	} else if (cmd.isVerbose) {
		clmessage("All hosts are denied access "
		    "to the cluster services.\n");
	}

	return (clerrno);
}
