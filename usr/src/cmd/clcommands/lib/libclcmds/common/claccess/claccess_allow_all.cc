//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)claccess_allow_all.cc	1.3	08/05/20 SMI"

//
// Process claccess "allow_all"
//

#include "claccess.h"

//
// claccess "allow_all"
//
clerrno_t
claccess_allow_all(ClCommand &cmd)
{
	scconf_errno_t scconf_err = SCCONF_NOERR;
	clerrno_t clerrno = CL_NOERR;

	// Clear the list of hosts able to add themselves to the cluster.
	// Once the list is cleared, any host may add itself to the cluster.
	scconf_err = scconf_clear_secure_joinlist();
	if (scconf_err != SCCONF_NOERR) {
		clerrno = claccess_conv_scconf_err(scconf_err);
		claccess_perror(clerrno);
	} else if (cmd.isVerbose) {
		clmessage("All hosts are allowed to access particular "
		    "cluster services.\n");
	}

	return (clerrno);
}
