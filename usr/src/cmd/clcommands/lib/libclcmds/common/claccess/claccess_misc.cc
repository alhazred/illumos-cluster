//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)claccess_misc.cc	1.3	08/05/20 SMI"

#include "claccess.h"

//
// Print a claccess error from the CL error number.
//
void
claccess_perror(clerrno_t clerrno)
{
	switch (clerrno) {
	case CL_EEXIST:
		clerror("Host already exists in the authentication list.\n");
		return;

	case CL_ENOENT:
		clerror("Host does not exist in the authentication list.\n");
		return;

	case CL_EINVAL:
		clcommand_perror(clerrno);
		return;

	case CL_ENOMEM:
		clcommand_perror(clerrno);
		return;

	case CL_EINTERNAL:
		clcommand_perror(clerrno);
		return;
	}
}

//
// Convert scconf error to claccess error.
//
clerrno_t
claccess_conv_scconf_err(scconf_errno_t scconf_err)
{
	// Map the error
	switch (scconf_err) {
	case SCCONF_NOERR:
		return (CL_NOERR);

	case SCCONF_EEXIST:
		return (CL_EEXIST);

	case SCCONF_ENOEXIST:
		return (CL_ENOENT);

	case SCCONF_EUSAGE:
	case SCCONF_EINVAL:
		return (CL_EINVAL);

	case SCCONF_ENOMEM:
	case SCCONF_EOVERFLOW:
		return (CL_ENOMEM);

	case SCCONF_EUNEXPECTED:
	default:
		return (CL_EINTERNAL);
	}
}
