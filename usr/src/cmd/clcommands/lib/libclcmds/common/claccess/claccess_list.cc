//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)claccess_list.cc	1.3	08/05/20 SMI"

//
// Process claccess "list"
//

#include "claccess.h"
#include "ClList.h"

//
// claccess "list"
//
clerrno_t
claccess_list(ClCommand &cmd)
{
	scconf_errno_t scconf_err = SCCONF_NOERR;
	clerrno_t clerrno = CL_NOERR;
	scconf_namelist_t *joinlist = (scconf_namelist_t *)0;
	ClList print_list = ClList(cmd.isVerbose ? true : false);

	// Get the list of allowed hosts
	scconf_err = scconf_get_secure_joinlist(&joinlist);
	if (scconf_err != SCCONF_NOERR) {
		clerrno = claccess_conv_scconf_err(scconf_err);
		goto cleanup;
	}

	// If verbose, add header
	if (cmd.isVerbose) {
		print_list.setHeadings(CLA_ALLOWED);
	}

	// Add hosts to the print list
	if (joinlist == NULL) {
		// Any node can join
		print_list.addRow(CLA_ANY);
	}

	while (joinlist) {
		if (joinlist->scconf_namelist_name &&
		    (strcmp(joinlist->scconf_namelist_name, ".") == 0) &&
		    joinlist->scconf_namelist_next == NULL) {
			// No node can join
			print_list.addRow(CLA_NONE);
			break;
		} else {
			// List of hosts that can join
			if (joinlist->scconf_namelist_name) {
				print_list.addRow(
				    joinlist->scconf_namelist_name);
			}
			joinlist = joinlist->scconf_namelist_next;
		}
	}

	// print the host list
	print_list.print();

cleanup:
	if (clerrno != CL_NOERR) {
		clcommand_perror(clerrno);
	}

	if (joinlist)
		scconf_free_namelist(joinlist);

	return (clerrno);
}
