//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)claccess_allow.cc	1.3	08/05/20 SMI"

//
// Process claccess "allow"
//

#include "claccess.h"

//
// claccess "allow"
//
clerrno_t
claccess_allow(ClCommand &cmd, ValueList &hosts)
{
	scconf_errno_t scconf_err = SCCONF_NOERR;
	clerrno_t clerrno = CL_NOERR;
	bool first_err = true;
	clerrno_t first_clerrno = CL_NOERR;
	char *host = NULL;

	// Check inputs
	if (hosts.getSize() == 0) {
		clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}

	// Add host to the join list
	for (hosts.start(); hosts.end() != 1; hosts.next()) {
		host = hosts.getValue();
		if (host == NULL)
			continue;

		scconf_err = scconf_addto_secure_joinlist(host);
		if (scconf_err != SCCONF_NOERR) {
			clerrno = claccess_conv_scconf_err(scconf_err);
			switch (clerrno) {
			case CL_EEXIST:
				clerror("Host \"%s\" already "
				    "exists in the authentication "
				    "list.\n", host);
				break;

			default:
				claccess_perror(clerrno);
				break;
			}

			if (first_err) {
				first_clerrno = clerrno;
				first_err = false;
			}
		} else if (cmd.isVerbose) {
			clmessage("Host \"%s\" is allowed to access "
			    "particular cluster services.\n", host);
		}
	}

	if (!first_err) {
		clerrno = first_clerrno;
	}

	return (clerrno);
}
