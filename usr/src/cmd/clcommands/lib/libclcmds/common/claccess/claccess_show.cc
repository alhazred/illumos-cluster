//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)claccess_show.cc	1.2	08/05/20 SMI"

//
// Process claccess "show"
//

#include "claccess.h"
#include "ClShow.h"

// Get the host access information for claccess_show.
clerrno_t
claccess_get_show(ClShow *claccess_get_show);

//
// claccess "show"
//
clerrno_t
claccess_show(ClCommand &cmd)
{
	clerrno_t clerrno = CL_NOERR;
	ClShow *claccess_show = new ClShow(HEAD_HOSTACCESS);

	//
	// Get the ClShow object for the allowed hosts and their
	// authentication protocol.
	//
	clerrno = claccess_get_show(claccess_show);

	// print the host and authprotocol
	if (claccess_show->getObjectSize() != 0) {
		(void) claccess_show->print();
	}

	// Delete the claccess_show object
	delete claccess_show;

	return (clerrno);
}

//
// get_access_show_obj
//
//	Get access clshow object. The ClShow object must be created
//	before calling.
//
clerrno_t
get_access_show_obj(optflgs_t optflgs, ClShow *access_clshow)
{
	int clerr = CL_NOERR;

	// Check input
	if (!access_clshow) {
		clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}

	return (claccess_get_show(access_clshow));
}


//
// claccess_get_show
//
//	Get the host access information and save it in claccess_show object.
//
clerrno_t
claccess_get_show(ClShow *claccess_show)
{
	scconf_errno_t scconf_err = SCCONF_NOERR;
	clerrno_t clerrno = CL_NOERR;

	char *clustername = NULL;
	scconf_namelist_t *joinlist = (scconf_namelist_t *)0;
	scconf_authtype_t authtype;
	char authprotocol[SCCONF_MAXSTRINGLEN];
	char hostbuf[SCCONF_MAXSTRINGLEN];

	// Get the list of allowed hosts
	scconf_err = scconf_get_secure_joinlist(&joinlist);
	if (scconf_err != SCCONF_NOERR) {
		clerrno = claccess_conv_scconf_err(scconf_err);
		goto cleanup;
	}

	*hostbuf = '\0';

	if (joinlist == NULL) {
		// Any node can join
		strcpy(hostbuf, CLA_ANY);
	}

	while (joinlist) {
		if (joinlist->scconf_namelist_name &&
		    (strcmp(joinlist->scconf_namelist_name, ".") == 0) &&
		    joinlist->scconf_namelist_next == NULL) {
			// No node can join
			strcpy(hostbuf, CLA_NONE);
			break;
		} else {
			// List of hosts that can join
			if (joinlist->scconf_namelist_name) {
				if (strlen(hostbuf) > 1) {
					(void) strcat(hostbuf, ", ");
				}
				(void) strcat(hostbuf,
				    joinlist->scconf_namelist_name);
			}
			joinlist = joinlist->scconf_namelist_next;
		}
	}

	// Get the authentication type
	scconf_err = scconf_get_secure_authtype(&authtype);
	if (scconf_err != SCCONF_NOERR) {
		goto cleanup;
	}
	switch (authtype) {
	case SCCONF_AUTH_SYS:
		strcpy(authprotocol, CLA_SYS);
		break;

	case SCCONF_AUTH_DH:
		strcpy(authprotocol, CLA_DES);
		break;

	default:
		strcpy(authprotocol, CLA_UNKNOWN);
		break;
	}

	// Initialize clconf library (returns zero on success)
	if (clconf_lib_init()) {
		clerrno = CL_EINTERNAL;
		goto cleanup;
	}

	clustername = clconf_get_clustername();

	// Add cluster name to the print list
	claccess_show->addObject(CLA_CLNAME, clustername);

	// Add hosts to the print list
	claccess_show->addProperty(clustername, CLA_ALLOWED, hostbuf);

	// Add authprotocol to the print list
	claccess_show->addProperty(clustername, CLA_AUTHPROTO, authprotocol);

cleanup:
	if (clerrno != CL_NOERR) {
		clcommand_perror(clerrno);
	}

	if (joinlist)
		scconf_free_namelist(joinlist);

	return (clerrno);
}
