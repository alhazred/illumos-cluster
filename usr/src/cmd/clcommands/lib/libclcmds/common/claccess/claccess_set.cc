//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)claccess_set.cc	1.2	08/05/20 SMI"

//
// Process claccess "set"
//

#include "claccess.h"

//
// claccess "set"
//
clerrno_t
claccess_set(ClCommand &cmd, char *authprotocol)
{
	scconf_errno_t scconf_err = SCCONF_NOERR;
	clerrno_t clerrno = CL_NOERR;
	scconf_authtype_t authtype;

	// Check inputs
	if (authprotocol == (char *)0) {
		clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}

	// Check for valid authtypes.
	if (strcmp(authprotocol, CLA_SYS) == 0)
		authtype = SCCONF_AUTH_SYS;
	else if (strcmp(authprotocol, CLA_UNIX) == 0)
		authtype = SCCONF_AUTH_SYS;
	else if (strcmp(authprotocol, CLA_DES) == 0)
		authtype = SCCONF_AUTH_DH;
	else {
		clerror("Failed to update authentication "
		    "- unknown authentication protocol.\n");
		return (CL_EINVAL);
	}

	// Set authentication protocol
	scconf_err = scconf_set_secure_authtype(authtype);
	if (scconf_err != SCCONF_NOERR) {
		clerrno = claccess_conv_scconf_err(scconf_err);
		claccess_perror(clerrno);
	} else if (cmd.isVerbose) {
		clmessage("Authentication protocol "
		    "is set to \"%s\".\n", authprotocol);
	}

	return (clerrno);
}
