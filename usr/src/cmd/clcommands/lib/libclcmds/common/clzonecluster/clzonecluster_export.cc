//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
#pragma ident	"@(#)clzonecluster_export.cc	1.7	08/07/24 SMI"

//
// Process clzonecluster "export"
//

#include "clzonecluster.h"
#include "clzonecluster_private.h"

//
// clzonecluster "boot"
//
clerrno_t
clzonecluster_export(ClCommand &cmd, char *zone_cluster,
	optflgs_t optflgs, char *command_file)
{
	clerrno_t first_err = CL_NOERR;
	clerrno_t zc_err = CL_NOERR;
	char *export_cmd_str;
	int len_count = 0;

	// Check if we are running on a platform that supports
	// zone cluster feature
	if ((zc_err = validate_zonecluster_support())
		!= CL_NOERR) {
		return (zc_err);
	}

	// If we are here it means that we are in a global zone
	// If no operands were specified then we should return
	if (zone_cluster != NULL && strlen(zone_cluster) < 1) {
		clerror("You must give the name of the zone cluster you want "
			"to export.\n");
		clcommand_usage(cmd, cmd.getSubCommandName(), 1);
		return (CL_EINVAL);
	}

	/* Check whether it is a valid zone cluster */
	zc_err = check_zone_cluster_exists(zone_cluster);

	if (zc_err != CL_NOERR) {
		// If we are here then it's not a valid zone
		// cluster
		clerror("Zone cluster %s does not exist.\n",
			zone_cluster);

		if (first_err == CL_NOERR) {
			first_err = CL_ENOENT;
		}

		// Nothing to export just exit with the error message
		return (first_err);
	}

	// Export SubCommand
	len_count = strlen(ZC_CFG_CMD);
	len_count += strlen(ZONE_ADM_ZONE_OPTION) + 2;
	len_count += strlen(zone_cluster) + 1;
	len_count += strlen(ZC_CFG_EXPORT_SUBCMD);

	if (optflgs & fflg) {
		len_count += strlen(ZONE_ADM_ZONE_OPTION) + 2;
		len_count += strlen(command_file) + 1;
	}

	export_cmd_str = new char[len_count];

	// Set the command to execute
	strcpy(export_cmd_str, ZC_CFG_CMD);
	strcat(export_cmd_str, " -z ");
	strcat(export_cmd_str, zone_cluster);
	strcat(export_cmd_str, " ");
	strcat(export_cmd_str, ZC_CFG_EXPORT_SUBCMD);

	// If the -f flag was specified set it for the command too
	if (optflgs & fflg) {
		strcat(export_cmd_str, " ");
		strcat(export_cmd_str, ZC_CFG_FILE_OPTION);
		strcat(export_cmd_str, " ");
		strcat(export_cmd_str, command_file);
	}

	zc_err = exec_command(export_cmd_str);
	delete[] export_cmd_str;

	if (first_err == CL_NOERR) {
		first_err = zc_err;
	}

	return (first_err);
}
