//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
#pragma ident	"@(#)clzonecluster_clone.cc	1.10	08/10/14 SMI"

//
// Process clzonecluster "clone"
//

#include "clzonecluster.h"
#include "clzonecluster_private.h"

//
// clzonecluster "clone"
//

// Global error variable to set the first error that occurs which will be set
// by any of the threads invoking the operation

// Global variable declared in clzonecluster_common.cc which is used to populate
// the list of zone cluster nodes where there was an error running the zoneadm
// command.
extern char **error_in_nodes;

// Count for the number of nodes where there was an error running the zoneadm
// commands
extern int error_nodes_count;

clerrno_t
clzonecluster_clone(ClCommand &cmd, char *source_zc,
	optflgs_t optflgs, ValueList &nodes, char *target_zc, char *copymethod)
{
	clerrno_t first_err = CL_NOERR;
	clerrno_t zc_err = CL_NOERR;
	ValueList valid_nodes;
	ValueList node_list;
	ValueList target_zc_node_list, source_zc_node_list, new_node_list,
		filtered_nodelist;
	cl_cmd_info_t clone_cmd_info;
	char *clone_cmd_str;
	char *t_zc_node = NULL;
	char *s_zc_node = NULL;
	int flag = 0;
	int len_count = 0;
	int i = 0;

	// Check if we are running on a platform that supports
	// zone cluster feature
	if ((zc_err = validate_zonecluster_support())
		!= CL_NOERR) {
		return (zc_err);
	}

	// If we are here it means that we are in a global zone
	/* Check whether it is a valid zone cluster */
	zc_err = check_zone_cluster_exists(target_zc);

	if (zc_err != CL_NOERR) {
		// If we are here then it's not a valid zone
		// cluster
		clerror("Target zone cluster %s does not exist.\n",
			target_zc);

		if (first_err == CL_NOERR) {
			first_err = CL_ENOENT;
		}

		// Nothing to export just exit with the error message
		return (first_err);
	}

	/* Check whether it is a valid zone cluster */
	zc_err = check_zone_cluster_exists(source_zc);

	if (zc_err != CL_NOERR) {
		// If we are here then it's not a valid zone
		// cluster
		clerror("Source zone cluster %s does not exist.\n",
			source_zc);

		if (first_err == CL_NOERR) {
			first_err = CL_ENOENT;
		}

		// Nothing to export just exit with the error message
		return (first_err);
	}

	//
	// If the user specified "nodes" then we have to
	// get only the set of nodes which are part of the nodelist
	// for the zone cluster.
	//
	if (nodes.getSize() > 0) {
		zc_err = filter_nodelist_for_zc(target_zc,
			nodes, target_zc_node_list);
		// Set first_err
		if ((first_err == CL_NOERR) &&
			(zc_err != CL_NOERR)) {
			first_err = zc_err;
		}

		// If the user specified "nodes", but they are not part
		// of this zone cluster's nodelist, then we have to skip
		if (target_zc_node_list.getSize() < 1) {
			// The user-specified nodelist was invalid.
			// No need to print an error here since
			// filter_nodelist_for_zc() takes care.
			target_zc_node_list.clear();
		}
	} else {
		// get the nodelist of the target zone cluster
		zc_err = get_nodelist_for_zc(target_zc, target_zc_node_list);

		if (zc_err != CL_NOERR) {
			// Set first_Err
			if (first_err == CL_NOERR) {
				first_err = zc_err;
			}
			// print an internal error here
			clcommand_perror(CL_EINTERNAL);
			// Skip this zone cluster and continue
			target_zc_node_list.clear();
			return (CL_EINTERNAL);
		}
	}

	// get the nodelist of the source zone cluster
	zc_err = get_nodelist_for_zc(source_zc, source_zc_node_list);

	if (zc_err != CL_NOERR) {
		// Set first_Err
		if (first_err == CL_NOERR) {
			first_err = zc_err;
		}
		// print an internal error here
		clcommand_perror(CL_EINTERNAL);
		// Skip this zone cluster and continue
		source_zc_node_list.clear();
		return (CL_EINTERNAL);
	}

	for (target_zc_node_list.start(); !target_zc_node_list.end();
	target_zc_node_list.next()) {
		flag = 0;
		t_zc_node = strdup(target_zc_node_list.getValue());

		for (source_zc_node_list.start();
		!source_zc_node_list.end(); source_zc_node_list.next()) {
			s_zc_node = strdup(source_zc_node_list.getValue());
			if (strncmp(t_zc_node, s_zc_node,
				strlen(t_zc_node)) == 0) {
				flag = 1;
			}

			free(s_zc_node);
		}

		if (flag == 0) {
			clerror("Target zone cluster \(\"%s\"\) nodelist "
				"must be a subset of the source zone "
				"cluster \(\"%s\"\) nodelist\n",
				target_zc, source_zc);
			return (CL_EINVAL);
		}

		free(t_zc_node);
	}

	// Filter out only the valid nodes
	valid_nodes.clear();
	zc_err = get_valid_nodes(target_zc_node_list, valid_nodes);

	if (zc_err != CL_NOERR) {
		// Set first_Err
		if (first_err == CL_NOERR) {
			first_err = zc_err;
		}
	}

	node_list = valid_nodes;

	// Show the waiting message
	show_waiting_message(target_zc, ZONE_ADM_CLONE_SUBCMD);

	// Now we can clone the zone cluster
	// First form the zoneadm command
	// 2 spaces + "-z"
	len_count = strlen(ZONE_ADM_ZONE_OPTION) + 2;
	len_count += strlen(target_zc);

	// Space + Clone SubCommand
	len_count += strlen(ZONE_ADM_CLONE_SUBCMD) + 1;

	// Space + source ZC
	len_count += strlen(source_zc) + 1;

	if (copymethod != NULL) {
		// 2 Spaces + "-m"
		len_count += strlen(ZONE_ADM_COPY_OPTION) + 2;
		len_count += strlen(copymethod);
	}

	// '\0'
	len_count += 1;

	clone_cmd_str = new char[len_count];

	// -z
	strcpy(clone_cmd_str, ZONE_ADM_ZONE_OPTION);
	strcat(clone_cmd_str, " ");
	// target zc
	strcat(clone_cmd_str, target_zc);
	strcat(clone_cmd_str, " ");
	// clone
	strcat(clone_cmd_str, ZONE_ADM_CLONE_SUBCMD);

	if (copymethod != NULL) {
		strcat(clone_cmd_str, " ");
		strcat(clone_cmd_str, ZONE_ADM_COPY_OPTION);
		strcat(clone_cmd_str, " ");
		strcat(clone_cmd_str, copymethod);
	}

	// source zc
	strcat(clone_cmd_str, " ");
	strcat(clone_cmd_str, source_zc);

	clone_cmd_info.cmd_str = clone_cmd_str;
	clone_cmd_info.redirect_output = 1;

	zc_err = exec_zone_adm_cmd_on_nodes(node_list, clone_cmd_info);

	delete[] clone_cmd_str;

	// Check for any errors
	if (zc_err != CL_NOERR) {
		//
		// Filter the nodes on which the clone command succeeded
		// and call sysidcfg on only those set of nodes
		// flag to detect if the given node is in the error node list
		//
		int flag = 0;

		//
		// Loop through the node_list of the zone cluster and remove
		// all the nodes where there was an error.
		// Resulting new_node_list ValueList will contain only those
		// nodes where clone completed successfully
		//

		for (filtered_nodelist.start(); !filtered_nodelist.end();
			filtered_nodelist.next()) {
			flag = 0;
			for (i = 0; i < error_nodes_count; i++) {
				if (strncmp(filtered_nodelist.getValue(),
					error_in_nodes[i],
					strlen(error_in_nodes[i])) == 0) {
					flag = 1;
					break;
				}
			}

			if (flag == 0) {
				new_node_list.add(node_list.getValue());
			}
		}
	} else {
		for (filtered_nodelist.start(); !filtered_nodelist.end();
			filtered_nodelist.next()) {
			new_node_list.add(filtered_nodelist.getValue());
		}
	}

	//
	// If we are here, then the "zoneadm clone"  was a success.
	// We have to perform a sysidcfg now.
	// The command that needs to be formed here is of the format
	// "<zonename> sysidcfg".
	//
	// We are adding 1 for the space after <zonename>
	//
	len_count = strlen(target_zc) + 1;
	// Added +1 for '\0'
	len_count = strlen(CLZ_HELPER_SYSIDCFG_SUBCMD) + 1;
	clone_cmd_str = new char[len_count];
	strcpy(clone_cmd_str, target_zc);
	strcat(clone_cmd_str, " ");
	strcat(clone_cmd_str, CLZ_HELPER_SYSIDCFG_SUBCMD);
	clone_cmd_info.cmd_str = clone_cmd_str;
	// Execute the "clzone_helper <zonename> sysidcfg"
	zc_err = exec_zone_adm_cmd_on_nodes(new_node_list, clone_cmd_info,
				SCRCMD_CLZONE_HELPER_SUBCMD);

	if (zc_err == CL_NOERR) {
		(void) sc_publish_zc_event(
			CL_GLOBAL_CLUSTER_NAME, ESC_CLUSTER_ZC_STATE_CHANGE,
			CL_EVENT_PUB_CMD, CL_EVENT_SEV_INFO,
			CL_EVENT_INIT_SYSTEM, 0, 0, 0,
			CL_ZC_NAME, SE_DATA_TYPE_STRING, target_zc,
			CL_REASON_CODE, SE_DATA_TYPE_UINT32,
			CL_REASON_ZC_INSTALLED, NULL);
	} else {
		(void) sc_publish_zc_event(
			CL_GLOBAL_CLUSTER_NAME, ESC_CLUSTER_ZC_STATE_CHANGE,
			CL_EVENT_PUB_CMD, CL_EVENT_SEV_INFO,
			CL_EVENT_INIT_SYSTEM, 0, 0, 0,
			CL_ZC_NAME, SE_DATA_TYPE_STRING, target_zc,
			CL_REASON_CODE, SE_DATA_TYPE_UINT32,
			CL_REASON_ZC_INSTALL_FAILED, NULL);
	}


	// Set first_err
	if ((first_err == CL_NOERR) &&
		(zc_err != CL_NOERR)) {
		first_err = zc_err;
	}

	delete[] clone_cmd_str;

	node_list.clear();

	return (first_err);
}
