//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
#pragma ident	"@(#)clzonecluster_common.cc	1.14	08/07/25 SMI"

//
// Process clzonecluster common functions
//

#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include "clzonecluster.h"
#include "clzonecluster_private.h"
#include "ClList.h"
#include <libzccfg/libzccfg.h>

#define	SCRCMD		"/usr/cluster/lib/sc/scrcmd"

//
// Global variables used in individual command file clzonecluster_<operation>.cc
// to undo any operation done of partial set of nodes when the operation fails
// on any of the nodes in the zone cluster.
// error_in_nodes stores the node names where the command failed
// error_nodes_count stores the count of the number of nodes where the command
// failed
//
char **error_in_nodes;
int error_nodes_count = 0;

//
// clzonecluster common functions
//

/*
 * Executes a given zoneadm command("cmd") on all the
 * specified nodes. This method will execute the command
 * on each node in a separate thread so that the execution
 * is parallel on each node. This method will prepend
 * "scrcmd -N <node> adminsterzone" before the command
 * string and xecute it. This function uses the struct
 * clzc_cmd_info to fetch the required information. In
 * the case where the command being executed causes an error,
 * this function will invoke the error handled specified.
 * This function assumes that the nodes specified in the
 * nodelist are valid and does not perform any checks
 * thereof. This function will return the first error
 * that occurs while executing any of the commands.
 *
 * Possible return values:
 *
 * 	CL_NOERR		- success
 *	CL_EINTERNAL		- internal error
 *	CL_EINVAL		- invalid argument
 */
clerrno_t exec_zone_adm_cmd_on_nodes(ValueList &nodes,
	cl_cmd_info_t cmd_info, char *scrcmd_sub_cmd) {

	pthread_t *threads;
	int nodeCount;
	clerrno_t first_err = CL_NOERR;
	clerrno_t err_result = CL_NOERR;
	void *statusp;
	int thread_ret, i = 0;
	char *cmd_buffer, *log_file;
	cl_cmd_info_t *cmd_info_list;

	if (nodes.getSize() < 1) {
		return (CL_EINVAL);
	}

	// First prepend "adminsterzone" to the command
	// Space + Added 1 for '\0'
	cmd_buffer = (char *)calloc(1, strlen(cmd_info.cmd_str) +
		strlen(scrcmd_sub_cmd) + 2 + 1);
	// for start double quote and end double quote
	strcat(cmd_buffer, scrcmd_sub_cmd);
	if (cmd_info.redirect_output != 0) {
		strcat(cmd_buffer, " \"");
	} else {
		strcat(cmd_buffer, " ");
	}
	strcat(cmd_buffer, cmd_info.cmd_str);

	// Now, allocate space for the number of threads reqd
	threads = (pthread_t *)calloc(sizeof (pthread_t),
		nodes.getSize());

	// Allocate memory for storing the nodes where there was an error
	if (error_nodes_count) {
		for (i = 0; i < error_nodes_count; i++) {
			if (error_in_nodes[i])
				free(error_in_nodes[i]);
		}

		free(error_in_nodes);
	}

	error_in_nodes = (char **) malloc(sizeof (char *) * nodes.getSize());
	error_nodes_count = 0;

	cmd_info_list = (cl_cmd_info_t *)calloc(sizeof (cl_cmd_info_t),
				nodes.getSize());

	// Execute the command on each node
	nodeCount = 0;
	for (nodes.start(); !nodes.end(); nodes.next()) {
		cmd_info_list[nodeCount].nodename = strdup(nodes.getValue());
		cmd_info_list[nodeCount].cmd_str = cmd_buffer;
		cmd_info_list[nodeCount].command_error_str = NULL;
		if (cmd_info.redirect_output != 0) {
			cmd_info_list[nodeCount].redirect_output =
				cmd_info.redirect_output;
		}

		err_result = execute_cmd_at_node_in_new_thread(
			&(threads[nodeCount]),
			&cmd_info_list[nodeCount]);

		if ((err_result != CL_NOERR) &&
			(first_err == CL_NOERR)) {

			first_err = err_result;
		}
		nodeCount++;
	}

	// Wait for each thread now
	for (nodeCount = 0; nodeCount < nodes.getSize(); nodeCount++) {
		thread_ret = pthread_join(threads[nodeCount],
			&statusp);

		// Sync all the threads here and get the first error exit value
		if (cmd_info_list[nodeCount].cmd_exit_status != 0) {
			// Add the nodename to the list of nodes which needs to
			// performt he undo operation
			error_in_nodes[error_nodes_count] =
				(char *)malloc(sizeof (char *) *
					strlen(
					cmd_info_list[nodeCount].nodename));
			error_in_nodes[error_nodes_count++] =
				strdup(cmd_info_list[nodeCount].nodename);

			if (first_err == CL_NOERR) {
				// Map the exit code to the clzonecluster
				// exit codes
				if (cmd_info_list[nodeCount].cmd_exit_status
					>= 100) {
					// All scrcmd exit codes are between
					// 100 - 109
					// (usr/src/head/scadmin/rpc/
					// rpc_scrcmd.x)
					// Set exit code to CL_EINTERNAL
					first_err = CL_EINTERNAL;
				} else {
					// Command execution failed.
					// Set the exit code to CL_EINVAL
					first_err = CL_EINVAL;
				}
			}

			if (cmd_info_list[nodeCount].command_error_str
				!= NULL && strlen(
				cmd_info_list[nodeCount].command_error_str)
					!= 0) {
				clerror("Command execution failed on node %s."
					"\n%s\n",
					cmd_info_list[nodeCount].nodename,
					/*CSTYLED */
					cmd_info_list[nodeCount].command_error_str);
			} else {
				clerror("Command execution failed on node "
					"%s. Please refer to the console for "
					"more information\n",
					cmd_info_list[nodeCount].nodename);
			}
		}
	}

	// Now free all the memory allocated
	free(threads);
	free(cmd_info_list);

	// Free the command buffer
	free(cmd_buffer);

	return (first_err);
}

//
// filter_nodelist_for_zc
//
// Verifies whether the given nodelist is a sub-set of
// the nodelist of zone cluster. If "nodes" contains nodes which
// do not form part of the zone cluster, then they are stipped off. If the
// zone cluster spans the nodelist then this function returns CL_NOERR, and
// if it doesn't then the function returns CL_EINVAL.
//
// Return Values :
//	CL_NOERR	- All nodes in the list are part of zone cluster
//	CL_EINVAL	- One or more nodes are not part of zone cluster
//	CL_ENOENT	- No such zone cluster exists
//	CL_EINTERNAL	- Internal Error
//
clerrno_t
filter_nodelist_for_zc(char *zonename, ValueList &nodes,
	ValueList &valid_nodes) {
	int node_found = 0;
	char *node_name;
	clerrno_t cz_err = CL_NOERR;
	// To store the nodelist of the zone cluster
	ValueList node_list;

	// zonename and nodes specified??
	if ((strlen(zonename) == 0) ||
		(nodes.getSize() < 1)) {
		return (CL_ENOENT);
	}

	// Get the nodelist for this zone cluster
	cz_err = get_nodelist_for_zc(zonename, node_list);

	// return if there was an error in fetcing the nodelist
	if (cz_err != CL_NOERR) {
		return (cz_err);
	}

	// Now check whether the nodes are present in the nodelist
	for (nodes.start(); !nodes.end(); nodes.next()) {
		node_name = nodes.getValue();
		node_found = node_list.isValue(node_name, 1);

		if (node_found != 0) {
			// Node is present in the nodelist
			valid_nodes.add(node_name);
			continue;
		}

		// If we are here then the node wasn't present in the nodelist
		clerror("Zone cluster %s is not configured on node %s.\n",
			zonename, node_name);
		cz_err = CL_EINVAL;
	}

	return (cz_err);
}

//
// get_nodelist_for_zc
//
// Fetches the nodelist for a specified zone cluster. This function
// appends to "node_list" the list of nodes across which the zone
// cluster spans.
//
// Return Values :
//	CL_NOERR	- No Error.
//	CL_ENOENT	- No such zone cluster exists.
//	CL_EINTERNAL	- Internal Error.
//
clerrno_t
get_nodelist_for_zc(char *zonename, ValueList &node_list) {
	zc_dochandle_t zone_handle;
	int lib_clzone_err = 0;
	struct zc_nodetab nodetab;

	if (strlen(zonename) == 0) {
		return (CL_ENOENT);
	}

	// Open the zonecfg handle
	zone_handle = zccfg_init_handle();
	lib_clzone_err = zccfg_get_handle(zonename, zone_handle);

	if (lib_clzone_err != ZC_OK) {
		return (CL_ENOENT);
	}

	lib_clzone_err = zccfg_setnodeent(zone_handle);

	if (lib_clzone_err != ZC_OK) {
		zccfg_fini_handle(zone_handle);
		return (CL_EINTERNAL);
	}

	// Get the nodelist for this zone cluster
	while (zccfg_getnodeent(zone_handle, &nodetab)
		== ZC_OK) {
		node_list.add(nodetab.zc_nodename);
	}

	(void) zccfg_endnodeent(zone_handle);
	// Close the zonecfg handle
	(void) zccfg_fini_handle(zone_handle);

	return (CL_NOERR);
}

//
// get_hostname_list_for_zc
//
// Fetches the list of zone hostnames for a specified zone cluster.
// This functions appends to "host_list" the hostnames of the zone
// on nodes across which the zone cluster spans.
// This function is very similar to get_nodelist_for_zc(), but we
// can have them as separate functions there are places which
// require only nodelist and there are places which require only
// zone-hostname list.
//
// Return Values :
//	CL_NOERR	- No Error.
//	CL_ENOENT	- No such zone cluster exists.
//	CL_EINTERNAL	- Internal Error.
//
extern clerrno_t
get_hostname_list_for_zc(char *zonename, ValueList &node_list) {
	zc_dochandle_t zone_handle;
	int lib_clzone_err = 0;
	struct zc_nodetab nodetab;

	if (strlen(zonename) == 0) {
		return (CL_ENOENT);
	}

	// Open the zonecfg handle
	zone_handle = zccfg_init_handle();
	lib_clzone_err = zccfg_get_handle(zonename, zone_handle);

	if (lib_clzone_err != ZC_OK) {
		return (CL_ENOENT);
	}

	lib_clzone_err = zccfg_setnodeent(zone_handle);

	if (lib_clzone_err != ZC_OK) {
		zccfg_fini_handle(zone_handle);
		return (CL_EINTERNAL);
	}

	// Get the nodelist for this zone cluster
	while (zccfg_getnodeent(zone_handle, &nodetab)
		== ZC_OK) {
		node_list.add(nodetab.zc_hostname);
	}

	(void) zccfg_endnodeent(zone_handle);
	// Close the zonecfg handle
	(void) zccfg_fini_handle(zone_handle);

	return (CL_NOERR);
}

//
// check_zone_cluster_exists
//
// This function checks whether the specified zone cluster exists or not.
// If the zone cluster exists, then this function CL_ENOERR. If the
// zone cluster does not exist, this function returns CL_ENOENT.
// If there is any error determining the validity of the zone cluster
// this function returns CL_EINTERNAL.
//
// Return Values :
//	CL_NOERR	- Zone cluster exists
//	CL_EINVAL	- Zone cluster was not specified
//	CL_ENOENT	- Zone cluster does not exist
//	CL_EINTERNAL	- Internal error
clerrno_t
check_zone_cluster_exists(char *zonename) {
	uint_t cl_id = 0;
	scconf_errno_t scconf_err = SCCONF_NOERR;

	if (strlen(zonename) == 0) {
		return (CL_EINVAL);
	}

	// Get the cluster id for the zone cluster.
	// If there is an error in fetching the cluster id
	// then it is an invalid zone cluster.
	scconf_err = scconf_get_zone_cluster_id(zonename,
		&cl_id);

	if (scconf_err == SCCONF_ENOEXIST) {
		/* If we are here then it's not a valid zone cluster */
		return (CL_ENOENT);
	} else if (scconf_err != SCCONF_NOERR) {
		/* There was some other error */
		return (CL_EINTERNAL);
	}

	// If we are here then the zone cluster is valid
	return (CL_NOERR);
}

//
// get_zone_cluster_names
//
// This method fetches the list of names of zone clusters
// configured in the cluster. The caller is responsible for
// freeing the memory in the list. If this method is called
// inside a zone cluster then the list will be empty. Note
// that in such a case this method will not return an error.
// Also note that this method will not return "global" in the
// list, when called from a global zone.
//
// Return Values :
//	CL_NOERR	- No Error.
//	CL_EINTERNAL	- Internal Error.
//
clerrno_t
get_zone_cluster_names(ValueList &zc_list) {
	char **zc_names;
	int zc_count = 0;
	int clconf_err;

	clconf_err = clconf_lib_init();

	if (clconf_err != 0) {
		return (CL_EINTERNAL);
	}

	zc_names = clconf_cluster_get_cluster_names();

	if (zc_names == NULL) {
		return (CL_NOERR);
	}

	for (; zc_names[zc_count] != NULL; zc_count++) {
		zc_list.add(zc_names[zc_count]);
	}

	// we have to free the memory for "zc_names", but
	// since we are returning the zone cluster names to
	// the caller and allowing him to delete the memory,
	// we are not doing it here.
	return (CL_NOERR);
}

void show_waiting_message(char *zcname, char *operation) {
	clmessage("Waiting for zone %s commands to complete on all the "
		"nodes of the zone cluster \"%s\"...\n", operation, zcname);
}

//
// get_zc_node_status
//
// This function fetches the status of the node in the zone cluster.
// The nodename expected here is the hostname of the zone, which is
// part of the zone cluster. If the zone cluster or the zone-cluster
// hostname does not exist, this function returns CL_ENOENT. Please
// note that this method will function properly only when executing
// in the global zone.
//
// Return Values :
//	CL_NOERR	- No error.
//	CL_ENOENT	- Zone cluster/ZC hostname does not exist
//	CL_EINVAL	- Invalid input
//	CL_EINTERNAL	- Internal error
extern clerrno_t
get_zc_node_status(char *zcname, char *zone_hostname,
    zone_cluster_state_t &zc_node_state) {

	int zc_cfg_err = ZC_OK;
	zone_cluster_state_t zc_state = ZC_STATE_UNKNOWN;
	struct zc_nodetab nodetab;
	zc_dochandle_t zone_handle;
	boolean_t found_zone = B_FALSE;
	clerrno_t cl_err = CL_NOERR;
	scconf_nodeid_t node_id;
	scconf_errno_t scconf_err = SCCONF_NOERR;

	if (zcname == NULL) {
		// Nothing to check
		return (CL_NOERR);
	}

	if (zone_hostname == NULL) {
		// Invalid input
		return (CL_EINVAL);
	}

	// First we have to find the node-id of the zone-hostname
	// and then invoke the corresponding libzccfg method to
	// fetch the status of a zone cluster node.
	// First open the zonecfg handle
	zone_handle = zccfg_init_handle();
	zc_cfg_err = zccfg_get_handle(zcname, zone_handle);

	if (zc_cfg_err != ZC_OK) {
		return (CL_ENOENT);
	}

	// Go to the nodetab
	zc_cfg_err = zccfg_setnodeent(zone_handle);

	if (zc_cfg_err != ZC_OK) {
		zccfg_fini_handle(zone_handle);
		return (CL_EINTERNAL);
	}

	// Get the nodelist for this zone cluster
	while (zccfg_getnodeent(zone_handle, &nodetab)
			== ZC_OK) {
		if (strcmp(zone_hostname, nodetab.zc_hostname)) {
			continue;
		}

		// If we are here, it means this is the zone cluster-node
		// we were looking for.
		found_zone = B_TRUE;

		// Now, get the nodeid of the physical nodename where this
		// zone is hosted.
		scconf_err = scconf_get_nodeid(nodetab.zc_nodename,
						&node_id);
		if (scconf_err != SCCONF_NOERR) {
			// We should not be here ideally. The zone is up, but
			// the node is not up.
			cl_err = CL_EINTERNAL;
			break;
		}

		// Now, fetch the state of the zone cluster node
		zc_cfg_err = get_zone_state_on_node(zcname,
					(nodeid_t)node_id,
					&zc_node_state);
		if (zc_cfg_err != ZC_OK) {
			cl_err = CL_EINTERNAL;
		}

		// No need to loop further
		break;
	}

	if (found_zone == B_FALSE) {
		cl_err = CL_ENOENT;
	}

	(void) zccfg_endnodeent(zone_handle);
	// Close the zonecfg handle
	(void) zccfg_fini_handle(zone_handle);

	return (cl_err);
}

//
// validate_zonecluster_support
//
// This function validates whether the platform/OS that the
// command is being run is capable of supporting zoneclusters
//
// Return Values :
//	CL_NOERR	- No error.
//	CL_EOP		- Unsupported OS

extern clerrno_t
validate_zonecluster_support() {
	if (zccfg_validate_brand_definition() != ZC_OK) {
		clerror("Current combination of Solaris and Solaris"
			"Cluster Software does not support zone"
			"cluster feature\n");
		return (CL_EOP);
	}

	return (CL_NOERR);
}

//
// validate_zonecluster_name
//
// This function validates whether the given zone cluster
// name doesnt not conflict with any of the reserved zone cluster
// names "all", "global" or "base"
//
// Return Values :
//	CL_NOERR	- No error.
//	CL_EINVAL	- Invalid Zone Cluster name due to conflict

extern clerrno_t
validate_zonecluster_name(char *zcname) {
	if ((strncmp(zcname, GLOBAL_CLUSTER_NAME, strlen(zcname)) == 0) ||
		(strncmp(zcname, ALL_CLUSTERS_STR, strlen(zcname)) == 0) ||
		(strncmp(zcname, BASE_CLUSTER_STR, strlen(zcname))) == 0) {
		clerror("Zone cluster name \"%s\" conflicts with the "
			"reserved zonecluster names : "
			"\"%s\", \"%s\" and \"%s\"\n",
			zcname, GLOBAL_CLUSTER_NAME, ALL_CLUSTERS_STR,
			BASE_CLUSTER_STR);
		return (CL_EINVAL);
	}

	return (CL_NOERR);
}
