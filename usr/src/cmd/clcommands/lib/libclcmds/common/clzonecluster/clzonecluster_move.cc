//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
#pragma ident	"@(#)clzonecluster_move.cc	1.11	08/07/24 SMI"

//
// Process clzonecluster "move"
//

#include "clzonecluster.h"
#include "clzonecluster_private.h"
#include <sys/os.h>

//
// clzonecluster "move"
//

// Global error variable to set the first error that occurs which will be set
// by any of the threads invoking the operation

// Global variable declared in clzonecluster_common.cc which is used to populate
// the list of zone cluster nodes where there was an error running the zoneadm
// command.
extern char **error_in_nodes;

// Count for the number of nodes where there was an error running the zoneadm
// commands
extern int error_nodes_count;

clerrno_t
clzonecluster_move(ClCommand &cmd, ValueList &operands, char *newzonepath) {
	clerrno_t zc_err = CL_NOERR;
	char *zone_cluster = NULL;
	char **zone_cluster_arr;
	ValueList valid_nodes;
	ValueList node_list, new_node_list;
	ValueList zone_clusters;
	cl_cmd_info_t move_cmd_info;
	char *move_cmd_str;
	int len_count = 0;
	int lib_clzone_err = 0;
	zc_dochandle_t zone_handle;
	char oldzonepath[MAXPATHLEN];
	clerrno_t first_err = CL_NOERR;
	zone_cluster_state_t state;
	nodeid_t nodeid;
	char *nodename;
	int i = 0;

	// Check if we are running on a platform that supports
	// zone cluster feature
	if ((zc_err = validate_zonecluster_support())
		!= CL_NOERR) {
		return (zc_err);
	}

	// If we are here we are in the global zone
	if ((newzonepath == NULL) || (strlen(newzonepath) <= 0)) {
		clerror("No zonepath specified for the zone cluster\n");
		return (CL_EINVAL);
	}

	// Check the validity of each specified name
	operands.start();
	zone_cluster = operands.getValue();

	// Check whether it is a valid zone cluster
	zc_err = check_zone_cluster_exists(zone_cluster);

	if (zc_err == CL_ENOENT) {
		// If we are here then it's not a valid zone cluster
		clerror("Zone cluster \"%s\" does not exist.\n",
			zone_cluster);

		if (first_err == CL_NOERR) {
			first_err = CL_ENOENT;
		}
	}

	// Get the nodelist of the zone cluster
	zc_err = get_nodelist_for_zc(zone_cluster, node_list);

	if (zc_err != CL_NOERR) {
		// Set first_Err
		if (first_err == CL_NOERR) {
			first_err = zc_err;
		}

		// print an internal error here
		clcommand_perror(CL_EINTERNAL);
		clerror("Unable to get nodelist"
			"for the zone cluster\n");

		// Skip this zone cluster and continue
		node_list.clear();
	}

	// Open the zonecfg handle
	zone_handle = zccfg_init_handle();
	lib_clzone_err = zccfg_get_handle(zone_cluster, zone_handle);

	if (lib_clzone_err != ZC_OK) {
		clcommand_perror(CL_EINTERNAL);
		clerror("Unable to get handle to the libzccfg\n");
		return (CL_ENOENT);
	}

	// Get valid set of nodes
	// Filter out only the valid nodes
	valid_nodes.clear();
	zc_err = get_valid_nodes(node_list, valid_nodes);

	if (zc_err != CL_NOERR) {
		// Set first_Err
		if (first_err == CL_NOERR) {
			first_err = zc_err;
		}

		clerror("Move operation cannot be performed if the zone "
			"cluster is not completely up.\n");
		return (CL_EINVAL);
	}

	node_list = valid_nodes;

	//
	// Loop through all the nodes and get the individual status
	// We allow the move operation only of all the zone cluster nodes
	// are in Installed state else return error
	//
	for (node_list.start(); !node_list.end(); node_list.next()) {
		// Get the nodename
		nodename = node_list.getValue();
		nodeid = clconf_cluster_get_nodeid_by_nodename(nodename);
		(void) get_zone_state_on_node(zone_cluster, nodeid, &state);

		if (state != ZC_STATE_INSTALLED) {
			clerror("Move operation is invalid for %s "
				"zone cluster.\n",
				zone_cluster_state_str(state));
			return (CL_EINVAL);
		}
	}

	if (zccfg_get_zonepath(zone_handle, oldzonepath,
		sizeof (oldzonepath))) {
		if (oldzonepath != NULL &&
			strlen(oldzonepath) == strlen(newzonepath)) {
			if (strncmp(oldzonepath, newzonepath,
				sizeof (oldzonepath))) {
				clerror("Identical zonepaths. "
					"No changes made to zonepath\n");
				return (CL_EINVAL);
			}
		}
	}

	// Set Zone Path
	if (zccfg_set_zonepath(zone_handle, newzonepath) != ZC_OK) {
		clcommand_perror(CL_EINTERNAL);
		clerror("Unable to update zonepath attribute for zone "
			"cluster \"%s\"\n", zone_cluster);
		return (CL_EINTERNAL);
	}

	// Commit the changes
	zc_err = zccfg_save(zone_handle);
	if (zc_err != ZC_OK) {
		clcommand_perror(CL_EINTERNAL);
		clerror("Unable to save configuration information to CCR\n");
		return (CL_EINTERNAL);
	}

	show_waiting_message(zone_cluster, ZONE_ADM_MOVE_SUBCMD);

	// Now we can move the zone cluster properties
	// First form the zoneadm command
	len_count = strlen(ZONE_ADM_MOVE_SUBCMD);
	len_count += strlen(ZONE_ADM_ZONE_OPTION) + 3; // 3 Spaces
	len_count += strlen(zone_cluster);
	len_count += strlen(newzonepath) + 1; // Added +1 for '\0'
	move_cmd_str = new char[len_count];
	strcpy(move_cmd_str, ZONE_ADM_ZONE_OPTION); // -z
	strcat(move_cmd_str, " ");
	strcat(move_cmd_str, zone_cluster); // zonename
	strcat(move_cmd_str, " ");
	strcat(move_cmd_str, ZONE_ADM_MOVE_SUBCMD); // move
	strcat(move_cmd_str, " ");
	strcat(move_cmd_str, newzonepath); // newzonepath
	move_cmd_info.cmd_str = move_cmd_str;
	move_cmd_info.redirect_output = 0;
	zc_err = exec_zone_adm_cmd_on_nodes(node_list, move_cmd_info);
	delete[] move_cmd_str;

	// Set first_err
	if ((first_err == CL_NOERR) &&
		(zc_err != CL_NOERR)) {
		first_err = zc_err;
		// If setting the new zonepath fails then reset the zonepath to
		// the old value of zonepath stored in oldzonepath
		// flag to detect if the given node is in the error node list
		int flag = 0;

		// Loop through the node_list of the zone cluster and remove
		// all the nodes where there was an error.
		// Resulting new_node_list ValueList will contain only those
		// nodes where there was a change in zonepath to the
		// new zonepath

		// This list is required to reset the zonepath to the old value
		// only of these nodes.
		for (node_list.start(); !node_list.end(); node_list.next()) {
			flag = 0;
			for (i = 0; i < error_nodes_count; i++) {
				if (strncmp(node_list.getValue(),
					error_in_nodes[i],
					strlen(error_in_nodes[i])) == 0) {
					flag = 1;
					break;
				}
			}

			if (flag == 0) {
				new_node_list.add(node_list.getValue());
			}
		}

		// Set Zone Path to the old value
		if (zccfg_set_zonepath(zone_handle, oldzonepath) != ZC_OK) {
			clcommand_perror(CL_EINTERNAL);
			clerror("Unable to update zonepath attribute for "
				"zone cluster \"%s\"\n", zone_cluster);
			return (CL_EINTERNAL);
		}

		// Commit the changes
		zc_err = zccfg_save(zone_handle);
		if (zc_err != ZC_OK) {
			clcommand_perror(CL_EINTERNAL);
			clerror("Unable to save configuration information "
				"to CCR\n");
			return (CL_EINTERNAL);
		}

		// Run the zoneadm commands only on those nodes where
		// the zonepath was changed to the new zonepath value.
		len_count = strlen(ZONE_ADM_MOVE_SUBCMD);
		len_count += strlen(ZONE_ADM_ZONE_OPTION) + 3; // 3 Spaces
		len_count += strlen(zone_cluster);
		len_count += strlen(newzonepath) + 1; // Added +1 for '\0'
		move_cmd_str = new char[len_count];
		strcpy(move_cmd_str, ZONE_ADM_ZONE_OPTION); // -z
		strcat(move_cmd_str, " ");
		strcat(move_cmd_str, zone_cluster); // zonename
		strcat(move_cmd_str, " ");
		strcat(move_cmd_str, ZONE_ADM_MOVE_SUBCMD); // move
		strcat(move_cmd_str, " ");
		strcat(move_cmd_str, oldzonepath); // newzonepath
		move_cmd_info.cmd_str = move_cmd_str;
		zc_err = exec_zone_adm_cmd_on_nodes(new_node_list,
				move_cmd_info);
		delete[] move_cmd_str;
	}

	node_list.clear();

	for (i = 0; i < error_nodes_count; i++) {
		delete error_in_nodes[i];
	}

	free(error_in_nodes);
	zccfg_fini_handle(zone_handle);

	return (first_err);
}
