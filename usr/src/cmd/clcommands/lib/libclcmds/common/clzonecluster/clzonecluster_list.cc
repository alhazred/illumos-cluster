//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
#pragma ident	"@(#)clzonecluster_list.cc	1.9	08/07/25 SMI"

//
// Process clzonecluster "list"
//

#include "clzonecluster.h"
#include "ClList.h"
#include <sys/clconf_int.h>

//
// clzonecluster "list"
//

clerrno_t
clzonecluster_list(ClCommand &cmd, optflgs_t optflgs)
{
	ClList print_list = ClList(cmd.isVerbose ? true : false);
	NameValueList node_list = NameValueList(true);
	clerrno_t clerrno = CL_NOERR;
	char **zone_cluster_arr;
	int zc_count = 0;
	char *cl_name;
	boolean_t zc_flag;
	scconf_errno_t scconf_err = SCCONF_NOERR;

	// Check if we are running on a platform that supports
	// zone cluster feature
	if ((clerrno = validate_zonecluster_support())
		!= CL_NOERR) {
		return (clerrno);
	}

	// If verbose, add the header
	if (optflgs & vflg) {
		print_list.setHeadings((char *)gettext("Zone Cluster"));
	}

	// Get virtual cluster names
	zone_cluster_arr = clconf_cluster_get_cluster_names();

	if (zone_cluster_arr == NULL) {
		// If we are here then we are in a non-global zone.
		// If are inside a zone cluster, then we will print
		// the zone cluster name, else we will report an error
		scconf_err = scconf_zone_cluster_check(&zc_flag);

		if (!zc_flag) {
			clerror("This command is supported only in "
				"the global zone and zone clusters.\n");
			clcommand_usage(cmd, cmd.getSubCommandName(), 1);
			return (CL_EOP);
		}

		// If we are here, then we are inside a zone cluster.
		// Hence, we should print the zone cluster name
		cl_name = clconf_get_clustername();
		if (!cl_name) {
			clerror("Cannot get the cluster name.\n");
			return (CL_EINTERNAL);
		}

		print_list.addRow(cl_name);
		print_list.print();
		return (clerrno);
	}

	// We are inside global zone here.
	// First print "global"
	// print_list.addRow(GLOBAL_ZONE_NAME);
	for (; zone_cluster_arr[zc_count] != NULL; zc_count++) {
		print_list.addRow(zone_cluster_arr[zc_count]);
	}

	// Print it
	print_list.print();

	return (clerrno);
}
