//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
#pragma ident	"@(#)clzonecluster_delete.cc	1.10	08/07/25 SMI"

//
// Process clzonecluster "delete"
//

#include "clzonecluster.h"
#include "clzonecluster_private.h"

//
// clzonecluster "delete"
//

clerrno_t clzonecluster_delete(ClCommand &cmd, ValueList operands,
	optflgs_t optflgs)
{
	clerrno_t clerrno = CL_NOERR;
	clerrno_t first_err = CL_NOERR;
	char *cmd_str = NULL;
	int cmd_length_count = 0;
	char *zonecluster_name = NULL;

	// Check if we are running on a platform that supports
	// zone cluster feature
	if ((clerrno = validate_zonecluster_support())
		!= CL_NOERR) {
		return (clerrno);
	}

	if (operands.getSize() < 1) {
		clerror("You must specify at least one zone cluster "
			"to delete\n");
		return (CL_EINVAL);
	}

	// Check the validity of each specified name
	operands.start();
	zonecluster_name = operands.getValue();

	// We need to validate the zone_name specified does not
	// conflict with any of the reserver zone cluster names
	if ((clerrno = validate_zonecluster_name(zonecluster_name))
		!= CL_NOERR) {
		return (clerrno);
	}

	// Check whether it is a valid zone cluster
	clerrno = check_zone_cluster_exists(zonecluster_name);

	if (clerrno == CL_ENOENT) {
		/* If we are here then it's not a valid zone cluster */
		clerror("Zone cluster \"%s\" does not exist.\n",
			zonecluster_name);

		if (first_err == CL_NOERR) {
			first_err = CL_ENOENT;
		}

		return (first_err);
	}

	cmd_length_count = 5; // " -z " + '\0'
	// Set the length of the cmd_str for the command
	cmd_length_count += strlen(zonecluster_name);
	// Get the length of the command
	cmd_length_count += strlen(ZC_CFG_CMD);
	cmd_length_count += strlen(ZC_CFG_DELETE_SUBCMD) + 1;

	if (optflgs & Fflg) {
		// Add space for " -F"
		cmd_length_count += 3;
	}

	// Allocate the memory for the cmd string
	cmd_str = new char[cmd_length_count];

	strcpy(cmd_str, ZC_CFG_CMD);
	strcat(cmd_str, " ");
	strcat(cmd_str, ZONE_ADM_ZONE_OPTION);
	strcat(cmd_str, " ");
	strcat(cmd_str, zonecluster_name);
	strcat(cmd_str, " ");
	strcat(cmd_str, ZC_CFG_DELETE_SUBCMD);

	if (optflgs & Fflg) {
		// Add the Force option if specified
		strcat(cmd_str, " ");
		strcat(cmd_str, ZONE_ADM_FORCE_OPTION);
	}

	clerrno = exec_command(cmd_str);

	if (first_err == CL_NOERR) {
		first_err = clerrno;
	}

	if (first_err == CL_NOERR) {
		(void) sc_publish_zc_event(
			CL_GLOBAL_CLUSTER_NAME, ESC_CLUSTER_ZC_STATE_CHANGE,
			CL_EVENT_PUB_CMD, CL_EVENT_SEV_INFO,
			CL_EVENT_INIT_SYSTEM, 0, 0, 0,
			CL_ZC_NAME, SE_DATA_TYPE_STRING, zonecluster_name,
			CL_REASON_CODE, SE_DATA_TYPE_UINT32,
			CL_REASON_ZC_DELETED, NULL);
	} else {
		(void) sc_publish_zc_event(
			CL_GLOBAL_CLUSTER_NAME, ESC_CLUSTER_ZC_STATE_CHANGE,
			CL_EVENT_PUB_CMD, CL_EVENT_SEV_INFO,
			CL_EVENT_INIT_SYSTEM, 0, 0, 0,
			CL_ZC_NAME, SE_DATA_TYPE_STRING, zonecluster_name,
			CL_REASON_CODE, SE_DATA_TYPE_UINT32,
			CL_REASON_ZC_DELETE_FAILED, NULL);
	}

	delete[] cmd_str;

	return (first_err);
}
