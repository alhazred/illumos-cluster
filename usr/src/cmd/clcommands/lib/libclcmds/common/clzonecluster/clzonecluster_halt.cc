//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
#pragma ident	"@(#)clzonecluster_halt.cc	1.13	08/08/07 SMI"

//
// Process clzonecluster "halt"
//

#include "clzonecluster.h"
#include "clzonecluster_private.h"

#include <rgm/scha_priv.h>
#include <rgm/rgm_common.h>
#include <scadmin/scstat.h>
#include "rgm_support.h"

//
// clzonecluster "halt"
// This file is very similar to clzonecluster_boot.cc
// But we need to maintain different files as the
// option processing will be different down the line.
//
clerrno_t
clzonecluster_halt(ClCommand &cmd, ValueList &operands,
	ValueList &nodes)
{
	clerrno_t first_err = CL_NOERR;
	clerrno_t zc_err = CL_NOERR;
	char *zone_cluster = NULL;
	ValueList valid_nodes;
	ValueList node_list;
	ValueList zone_clusters;
	cl_cmd_info_t halt_cmd_info;
	char *halt_cmd_str;
	int len_count = 0;

	// Check if we are running on a platform that supports
	// zone cluster feature
	if ((zc_err = validate_zonecluster_support())
		!= CL_NOERR) {
		return (zc_err);
	}

	// If no operands were specified then we should return
	if (operands.getSize() < 1) {
		clerror("You must give the name of the zone cluster you want "
			"to halt.\n");
		clcommand_usage(cmd, cmd.getSubCommandName(), 1);
		return (CL_EINVAL);
	}

	// Check the validity of the nodes specified.
	if (nodes.getSize() > 0) {
		// We have to check whether these nodes are valid
		first_err = get_valid_nodes(nodes, valid_nodes);

		if (valid_nodes.getSize() <= 0) {
			// None of the nodes were valid
			// We don't have to print an error message here
			// since all the invalid nodes are reported by
			// get_valid_nodes()
			return (CL_ENOENT);
		}
	}

	// If the wild-card operand was specified then we have to
	// fetch all the zone clusters configured.
	operands.start();
	if (strcmp(operands.getValue(), CLCOMMANDS_WILD_OPERAND)) {
		// Wild card was not specified.
		// Check the validity of each specified name
		for (operands.start(); !operands.end(); operands.next()) {
			zone_cluster = operands.getValue();

			/* Check whether it is a valid zone cluster */
			zc_err = check_zone_cluster_exists(zone_cluster);

			if (zc_err == CL_ENOENT) {
				// If we are here then it's not a valid
				// zone cluster
				clerror("Zone cluster %s does not exist.\n",
					zone_cluster);

				if (first_err == CL_NOERR) {
					first_err = CL_ENOENT;
				}

				continue;
			}
			// If we are here then this was a valid zone cluster
			zone_clusters.add(zone_cluster);
		}

	} else {
		// Wild card was specified
		zc_err = get_zone_cluster_names(zone_clusters);
		if (zc_err != CL_NOERR) {
			// Internal error
			clcommand_perror(CL_EINTERNAL);
			return (zc_err);
		}
	}

	// Iterate through the zone clusters
	for (zone_clusters.start(); !zone_clusters.end();
		zone_clusters.next()) {
		zone_cluster = zone_clusters.getValue();

		// If we are here then it is a valid zone cluster

		// Do the necessary evacuation of RGs
		zc_err = evacuate_rgs_on_zc_nodes(valid_nodes, zone_cluster);

		// Set first_err
		if ((first_err == CL_NOERR) && (zc_err != CL_NOERR)) {
			first_err = zc_err;
			continue;
		}

		// If the user specified "nodes" then we have to
		// get only the set of nodes which are part of the nodelist
		// for the zone cluster.

		if (nodes.getSize() > 0) {
			zc_err = filter_nodelist_for_zc(zone_cluster,
					valid_nodes, node_list);
			// Set first_err
			if ((first_err == CL_NOERR) &&
				(zc_err != CL_NOERR)) {
				first_err = zc_err;
			}

			// If the user specified "nodes", but they are
			// not part of this zone cluster's nodelist,
			// then we have to skip
			if (node_list.getSize() < 1) {
				// The user-specified nodelist was invalid.
				node_list.clear();
				continue;
			}
		} else {
			// get the nodelist of the zone cluster
			zc_err = get_nodelist_for_zc(zone_cluster, node_list);

			if (zc_err != CL_NOERR) {
				// Set first_Err
				if (first_err == CL_NOERR) {
					first_err = zc_err;
				}
				// print an internal error here
				clcommand_perror(CL_EINTERNAL);
				// Skip this zone cluster and continue
				node_list.clear();
				continue;
			}

			// Filter out only the valid nodes
			valid_nodes.clear();
			zc_err = get_valid_nodes(node_list, valid_nodes);

			if (zc_err != CL_NOERR) {
				// Set first_Err
				if (first_err == CL_NOERR) {
					first_err = zc_err;
				}
			}

			node_list = valid_nodes;
		}

		show_waiting_message(zone_cluster, ZONE_ADM_HALT_SUBCMD);

		// Now we can halt the cluster
		// First form the zoneadm command
		len_count = strlen(ZONE_ADM_HALT_SUBCMD);
		// 2 spaces + '\0'
		len_count += strlen(ZONE_ADM_ZONE_OPTION) + 3;
		len_count += strlen(zone_cluster);
		halt_cmd_str = new char[len_count];
		strcpy(halt_cmd_str, ZONE_ADM_ZONE_OPTION); // -z
		strcat(halt_cmd_str, " ");
		strcat(halt_cmd_str, zone_cluster); // zonename
		strcat(halt_cmd_str, " ");
		strcat(halt_cmd_str, ZONE_ADM_HALT_SUBCMD); // halt
		halt_cmd_info.cmd_str = halt_cmd_str;
		halt_cmd_info.redirect_output = 0;
		zc_err = exec_zone_adm_cmd_on_nodes(node_list, halt_cmd_info);
		delete[] halt_cmd_str;

		// Set first_err
		if ((first_err == CL_NOERR) &&
			(zc_err != CL_NOERR)) {
			first_err = zc_err;
		}

		node_list.clear();
	}

	return (first_err);
}
