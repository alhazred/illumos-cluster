//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
#pragma ident	"@(#)clzonecluster_show.cc	1.13	09/04/02 SMI"

//
// Process clzonecluster "show"
//

#include "clzonecluster.h"

#include <unistd.h>
#include <sys/types.h>
#include <assert.h>
#include <auth_attr.h>
#include <sys/wait.h>
#include "clzonecluster.h"
#include "clzonecluster_private.h"
#include <libzccfg/libzccfg.h>
#include <zonecluster_prop/zonecluster_prop.h>
#include <sys/clconf_int.h>

/*
 * Wrapper function to loop through all the available or given zone clusters
 * and verify if such a zone cluster exists and show their properties
 *
 */
static clerrno_t clzonecluster_get_show(ValueList &zoneclusters,
    optflgs_t optflgs, ClShow *clzc_show);

/*
 * Fetch all the properties and their values for a given zone cluster
 *
 */
static clerrno_t get_zc_properties(char *zcnamem, ClShow *clzc_show,
	optflgs_t optflgs);

/*
 * Helper utility copied from sczonecfg.c for converting a long value to units
 * of [BKMG] Bytes
 */
static void bytes_to_units(char *, char *, int);

//
// clzonecluster "show"
//

clerrno_t
clzonecluster_show(ClCommand &cmd, ValueList zcnames,
	optflgs_t optflgs)
{
	clerrno_t clerrno = CL_NOERR;
	char **zone_cluster_arr;
	ClShow *clzc_show = new ClShow(HEAD_ZONECLUSTER);
	ValueList zc_filtered;
	char *cl_name;
	boolean_t zc_flag;
	int zc_count = 0;
	scconf_errno_t scconf_err = SCCONF_NOERR;

	// Check if we are running on a platform that supports
	// zone cluster feature
	if ((clerrno = validate_zonecluster_support())
		!= CL_NOERR) {
		return (clerrno);
	}

	// Check if a WILDCARD was specified if so get the list of all the
	// zone cluster present and add them to the list. If not just use
	// the list of zone clusters given as operands
	if (zcnames.getSize() > 0) {
		zcnames.start(); // Position the ValueList
		if (strcmp(zcnames.getValue(),
			CLCOMMANDS_WILD_OPERAND) == 0) {
			clerrno = get_zone_cluster_names(zc_filtered);

			if (clerrno != CL_NOERR) {
				clerror("Unable to get zone cluster "
					"information\n");
				goto cleanup;
			}
		} else {
			zc_filtered = zcnames;
		}
	}

	// fill the clzc_show object
	clerrno = clzonecluster_get_show(zc_filtered, optflgs, clzc_show);

	// Print it
	if ((clerrno == CL_NOERR) ||
		(clzc_show->getObjectSize() != 0)) {
		(void) clzc_show->print();
	}

cleanup:
	// Delete the show object
	delete clzc_show;

	return (clerrno);
}

clerrno_t
get_zonecluster_show_obj(optflgs_t optflgs, ClShow *clzc_show) {
	ValueList zc_list;
	clerrno_t clerrno = CL_NOERR;

	clerrno = get_zone_cluster_names(zc_list);
	if (clerrno != CL_NOERR) {
		clerror("Unable to get list of zone clusters\n");
		return (CL_EINTERNAL);
	}

	// Get the zc show
	clerrno = clzonecluster_get_show(zc_list, optflgs, clzc_show);
	if (clerrno != CL_NOERR) {
		clerror("Unable to get properties of Zone Clusters\n");
	}

	// Free memory
	zc_list.clear();
	return (clerrno);
}

//
// clzonecluster_get_show
//
//	Get the zone cluster configuration data and save into the ClShow
//	object for printing.
//
clerrno_t
clzonecluster_get_show(ValueList &zoneclusters, optflgs_t optflgs,
	ClShow *clzc_show) {
	clerrno_t clerrno = CL_NOERR;
	NameValueList zc_list;
	int zc_count = 0;
	int found;
	clerrno_t first_err = CL_NOERR;

	for (zoneclusters.start(); !zoneclusters.end();
		zoneclusters.next()) {

		// Check if the given zone cluster is valid
		clerrno = check_zone_cluster_exists(zoneclusters.getValue());

		if (clerrno != CL_NOERR) {
			clerror("Zone cluster \"%s\" does not exist\n",
				zoneclusters.getValue());
			first_err = CL_ENOENT;
			continue;
		}

		// Add the header for zone cluters and add the current
		// zone cluster name as well in the header
		clzc_show->addObject(CLZC_CLUSTER_NAME,
			zoneclusters.getValue());

		// Add all the properties of the zone clusters to the
		// clzc_show obj
		clerrno = get_zc_properties(zoneclusters.getValue(),
				clzc_show, optflgs);

		if (clerrno != CL_NOERR && first_err == CL_NOERR) {
			clerror("Unable to get Zone cluster \"%s\" "
				"properties\n", zoneclusters.getValue());
			first_err = CL_EINTERNAL;
		}
	}

	return (first_err);
}

/*
 * Helper function to convert from bytes to the SI units. str has the input
 * byte string and buf contains the conversion result.
 */
void
bytes_to_units(char *str, char *buf, int bufsize)
{
	unsigned long long num;
	unsigned long long save = 0;
	char *units = "BKMGT";
	char *up = units;

	num = strtoll(str, NULL, 10);

	// less than 1K, so no prefix
	if (num < 1024) {
		(void) snprintf(buf, bufsize, "%llu%c", num, *up);
		return;
	}

	// From 'Kilo' to 'Tera'
	while ((num >= 1024) && (*up != 'T')) {
		up++; /* next unit of measurement */
		save = num;
		num = (num + 512) >> 10;
	}

	// check if we should output a fraction.
	// snprintf will round for us
	if (save % 1024 != 0 && ((save >> 10) < 10)) {
		(void) snprintf(buf, bufsize, "%2.1f%c",
		((float)save / 1024), *up);
	} else {
		(void) snprintf(buf, bufsize, "%llu%c", num, *up);
	}
}

static
clerrno_t get_zc_properties(char *zcname, ClShow *clzc_show,
	optflgs_t optflgs) {
	zc_dochandle_t zone_handle;
	int lib_clzone_err = 0;
	int err = 0;
	int count = 0;
	int first = 1;
	char buf[128];
	struct zc_nodetab nodetab;
	ClShow *clzc_resource_show;
	ClShow *zc_node_show;
	ClShow *zc_net_node_show;
	ClShow *clzc_rctl_show;
	char *ipd_entry;
	char options[256];

	// Structures and variables for fetching zone cluster property values
	char zonename[ZONENAME_MAX];
	char zonepath[MAXPATHLEN];
	bool autoboot;
	char brand[MAXNAMELEN];
	char bootargs[BOOTARGS_MAX];
	char pool[MAXNAMELEN];
	char *limitpriv;
	char sched[MAXNAMELEN];
	zc_iptype_t iptype;
	uint64_t limit;
	bool privateip;
	struct zc_nwiftab nwif_lookup;
	struct zc_nwiftab net_lookup;
	struct zc_nodetab node_lookup;
	struct zc_fstab ipd_lookup;
	struct zc_fstab fs_lookup;
	struct zc_mcaptab mcap_lookup;
	struct zc_rctltab rctl_lookup;
	struct zc_sysidtab sysid_lookup;
	struct zc_devtab dev_lookup;
	struct zc_dstab ds_lookup;
	struct zc_psettab pset_lookup;
	struct zc_fsopt *fs_options;
	uint64_t cap;
	bool defrouter_support;

	if (strlen(zcname) == 0) {
		return (CL_ENOENT);
	}

	// Open the zonecfg handle
	zone_handle = zccfg_init_handle();
	lib_clzone_err = zccfg_get_handle(zcname, zone_handle);

	if (lib_clzone_err != ZC_OK) {
		clerror("Unable to get handle to the libzccfg\n");
		return (CL_ENOENT);
	}

	// Get Zone Name
	if (zccfg_get_name(zone_handle, zonename,
		sizeof (zonename)) == ZC_OK) {
		clzc_show->addProperty(zcname,
			prop_types[PT_ZONENAME], zonename);
	} else {
		return (CL_EINTERNAL);
	}

	// Get Zone Path
	if (zccfg_get_zonepath(zone_handle, zonepath,
		sizeof (zonepath)) == ZC_OK) {
		clzc_show->addProperty(zcname,
			prop_types[PT_ZONEPATH], zonepath);
	} else {
		return (CL_EINTERNAL);
	}

	// Get autoboot
	if ((err = zccfg_get_autoboot(zone_handle, &autoboot)) == ZC_OK) {
		if (autoboot) {
			clzc_show->addProperty(zcname,
				prop_types[PT_AUTOBOOT], "TRUE");
		} else {
			clzc_show->addProperty(zcname,
				prop_types[PT_AUTOBOOT], "FALSE");
		}
	} else {
		return (CL_EINTERNAL);
	}

	// If verbose
	if (optflgs & vflg) {
		// Get brand
		if (zccfg_get_brand(zone_handle, brand,
			sizeof (brand)) == ZC_OK) {
			clzc_show->addProperty(zcname, prop_types[PT_BRAND],
				brand);
		} else {
			return (CL_EINTERNAL);
	}

	// Get bootargs
	if ((err = zccfg_get_bootargs(zone_handle,
		bootargs, sizeof (bootargs))) == ZC_OK) {
		if (strlen(bootargs) != 0)  {
			clzc_show->addProperty(zcname,
				prop_types[PT_BOOTARGS], bootargs);
		} else {
			clzc_show->addProperty(zcname,
				prop_types[PT_BOOTARGS], CLZC_NOVALUE);
		}
	} else {
		return (CL_EINTERNAL);
	}

		// Get pool
		if ((err = zccfg_get_pool(zone_handle, pool,
			sizeof (pool))) == ZC_OK) {
		if (strlen(pool) != 0) {
			clzc_show->addProperty(zcname,
				prop_types[PT_POOL], pool);
		} else {
			clzc_show->addProperty(zcname,
				prop_types[PT_POOL], CLZC_NOVALUE);
		}

		} else {
		return (CL_EINTERNAL);
		}

		// Get limitpriv
		if ((err = zccfg_get_limitpriv(zone_handle,
			&limitpriv)) == ZC_OK) {
		if (strlen(limitpriv) != 0) {
			clzc_show->addProperty(zcname, prop_types[PT_LIMITPRIV],
				limitpriv);
		} else {
			clzc_show->addProperty(zcname, prop_types[PT_LIMITPRIV],
				CLZC_NOVALUE);
		}
		} else {
		return (CL_EINTERNAL);
		}

		// Get sched
		if ((err = zccfg_get_sched_class(zone_handle, sched,
			sizeof (sched))) == ZC_OK) {
		if (strlen(sched) != 0) {
			clzc_show->addProperty(zcname,
				prop_types[PT_SCHED], sched);
		} else {
			clzc_show->addProperty(zcname, prop_types[PT_SCHED],
				CLZC_NOVALUE);
		}
		} else {
		return (CL_EINTERNAL);
		}
	}

	// Get iptype
	if ((err = zccfg_get_iptype(zone_handle, &iptype)) == ZC_OK) {
		switch (iptype) {
		case CZS_SHARED:
			clzc_show->addProperty(zcname,
				prop_types[PT_IPTYPE], "shared");
			break;

		case CZS_EXCLUSIVE:
			clzc_show->addProperty(zcname,
				prop_types[PT_IPTYPE], "exclusive");
			break;
		}
	} else {
		return (CL_EINTERNAL);
	}

	if (optflgs & vflg) {
		// Get aliased rctl - ALIAS_MAXLWPS
		if (zccfg_get_aliased_rctl(zone_handle,
			ALIAS_MAXLWPS, &limit) == ZC_OK) {
		char buf[128];
		(void) snprintf(buf, sizeof (buf), "%llu", limit);
		clzc_show->addProperty(zcname, prop_types[PT_MAXLWPS], buf);
		}

		// Get aliased rctl - ALIAS_MAXSHMMEM
		if (zccfg_get_aliased_rctl(zone_handle, ALIAS_MAXSHMMEM,
			&limit) == ZC_OK) {
		char buf[128];
		(void) snprintf(buf, sizeof (buf), "%llu", limit);
		bytes_to_units(buf, buf, sizeof (buf));
		clzc_show->addProperty(zcname, prop_types[PT_MAXSHMMEM], buf);
		}

		// Get aliased rctl - ALIAS_MAXSHMIDS
		if (zccfg_get_aliased_rctl(zone_handle, ALIAS_MAXSHMIDS,
			&limit) == ZC_OK) {
		char buf[128];
		(void) snprintf(buf, sizeof (buf), "%llu", limit);
		clzc_show->addProperty(zcname, prop_types[PT_MAXSHMIDS], buf);
		}

		// Get aliased rctl - ALIAS_MAXMSGIDS
		if (zccfg_get_aliased_rctl(zone_handle, ALIAS_MAXMSGIDS,
			&limit) == ZC_OK) {
		char buf[128];
		(void) snprintf(buf, sizeof (buf), "%llu", limit);
		clzc_show->addProperty(zcname, prop_types[PT_MAXMSGIDS], buf);
		}

		// Get aliased rctl - ALIAS_MAXSEMIDS
		if (zccfg_get_aliased_rctl(zone_handle, ALIAS_MAXSEMIDS,
			&limit) == ZC_OK) {
		char buf[128];
		(void) snprintf(buf, sizeof (buf), "%llu", limit);
		clzc_show->addProperty(zcname, prop_types[PT_MAXSEMIDS], buf);
		}

		// Get aliased rctl - ALIAS_SHARES
		if (zccfg_get_aliased_rctl(zone_handle, ALIAS_SHARES,
			&limit) == ZC_OK) {
		char buf[128];
		(void) snprintf(buf, sizeof (buf), "%llu", limit);
		clzc_show->addProperty(zcname, prop_types[PT_SHARES], buf);
		}
	}
	// Get clprivnet
	if ((err = zccfg_get_clprivnet(zone_handle, &privateip)) == ZC_OK) {
		if (privateip) {
			clzc_show->addProperty(zcname,
				prop_types[PT_CLPRIVNET], "TRUE");
		} else {
			clzc_show->addProperty(zcname,
				prop_types[PT_CLPRIVNET], "FALSE");
		}
	}

	// ADD RESOURCE SUB-HEAD for Resources
	clzc_resource_show = new ClShow(SUBHEAD_ZC_RESOURCES, zcname);

	// Get net
	first = 1;

	if (zccfg_setnwifent(zone_handle) == ZC_OK) {
		while (zccfg_getnwifent(zone_handle, &net_lookup) == ZC_OK) {
		clzc_resource_show->addObject(CLZC_PROP_NAME,
			res_types[RT_NET]);

		if (strlen(net_lookup.zc_nwif_address) != 0) {
			clzc_resource_show->addProperty(res_types[RT_NET],
				prop_types[PT_ADDRESS],
				net_lookup.zc_nwif_address);
		} else {
			clzc_resource_show->addProperty(res_types[RT_NET],
				prop_types[PT_ADDRESS], CLZC_NOVALUE);
		}

		if (strlen(net_lookup.zc_nwif_physical) != 0)  {
			clzc_resource_show->addProperty(res_types[RT_NET],
				prop_types[PT_PHYSICAL],
				net_lookup.zc_nwif_physical);
		} else {
			clzc_resource_show->addProperty(res_types[RT_NET],
				prop_types[PT_PHYSICAL], CLZC_NOVALUE);
		}
		}
	}

	// Get fs
	first = 1;
	if (zccfg_setfsent(zone_handle) == ZC_OK) {
		while (zccfg_getfsent(zone_handle, &fs_lookup) == ZC_OK) {
		clzc_resource_show->addObject(CLZC_PROP_NAME, CLZC_FS);

		clzc_resource_show->addProperty(CLZC_FS, prop_types[PT_DIR],
			fs_lookup.zc_fs_dir);
		clzc_resource_show->addProperty(CLZC_FS, prop_types[PT_SPECIAL],
			fs_lookup.zc_fs_special);
		clzc_resource_show->addProperty(CLZC_FS, prop_types[PT_RAW],
			fs_lookup.zc_fs_raw);
		clzc_resource_show->addProperty(CLZC_FS, prop_types[PT_TYPE],
			fs_lookup.zc_fs_type);


		(void) snprintf(options, sizeof (options), "[");

		/* TODO: Add comment explaining fs_options loop */
		for (fs_options = fs_lookup.zc_fs_options; fs_options != NULL;
		fs_options = fs_options->zc_fsopt_next) {
			if (strchr(fs_options->zc_fsopt_opt, '=')) {
				(void) snprintf(options,
					sizeof (options), "%s\"%s\"", options,
					fs_options->zc_fsopt_opt);
			} else {
				(void) snprintf(options,
					sizeof (options), "%s%s",
					options, fs_options->zc_fsopt_opt);
			}

			if (fs_options->zc_fsopt_next != NULL) {
				(void) snprintf(options,
					sizeof (options), "%s,", options);
			}
		}

		(void) snprintf(options, sizeof (options), "%s]\n", options);

		clzc_resource_show->addProperty(CLZC_FS,
			prop_types[PT_OPTIONS], options);
		}

		(void) zccfg_endfsent(zone_handle);
	}

	// Get dev
	first = 1;
	if (zccfg_setdevent(zone_handle) == ZC_OK) {
		while (zccfg_getdevent(zone_handle, &dev_lookup) == ZC_OK) {
			clzc_resource_show->addObject(CLZC_PROP_NAME,
				res_types[RT_DEVICE]);
			clzc_resource_show->addProperty(res_types[RT_DEVICE],
				prop_types[PT_MATCH], dev_lookup.zc_dev_match);
		}
	}

	// Get dataset
	first = 1;
	if (zccfg_setdsent(zone_handle) == ZC_OK) {
		while (zccfg_getdsent(zone_handle, &ds_lookup) == ZC_OK) {
		clzc_resource_show->addObject(CLZC_PROP_NAME,
			res_types[RT_DATASET]);

		clzc_resource_show->addProperty(res_types[RT_DATASET],
			prop_types[PT_NAME], ds_lookup.zc_dataset_name);
		}
	}

	if (optflgs & vflg) {
		// Get all sysid
		first = 1;
		if (zccfg_setsysident(zone_handle) == ZC_OK) {
		while (zccfg_getsysident(zone_handle, &sysid_lookup) == ZC_OK) {
			clzc_resource_show->addObject(CLZC_PROP_NAME,
				CLZC_SYSID);

			//
			// Commenting out the display of root password for
			// the zone cluster show output. For more information
			// refer to CR 6740154
			//

			/*
			 * struct passwd *pwp = getpwuid(getuid());
			 * assert(pwp);
			 */

			/*
			 * Show the encrypted root password only if the user has
			 * the solaris.cluster.admin RBAC profile.
			 */

			/*
			 * if (chkauthattr("solaris.cluster.admin",
			 *	(const char *)pwp->pw_name) == 1) {
			 *	clzc_resource_show->addProperty(CLZC_SYSID,
			 *		prop_types[PT_ROOTPASSWD],
			 *		sysid_lookup.zc_root_password);
			 * }
			 */

			clzc_resource_show->addProperty(CLZC_SYSID,
				prop_types[PT_NAMESERVICE],
				sysid_lookup.zc_name_service);
			clzc_resource_show->addProperty(CLZC_SYSID,
				prop_types[PT_NFS4DOMAIN],
				sysid_lookup.zc_nfs4_domain);
			clzc_resource_show->addProperty(CLZC_SYSID,
				prop_types[PT_SECPOLICY],
				sysid_lookup.zc_sec_policy);
			clzc_resource_show->addProperty(CLZC_SYSID,
				prop_types[PT_SYSLOCALE],
				sysid_lookup.zc_sys_locale);
			clzc_resource_show->addProperty(CLZC_SYSID,
				prop_types[PT_TERMINAL],
				sysid_lookup.zc_terminal);
			clzc_resource_show->addProperty(CLZC_SYSID,
				prop_types[PT_TIMEZONE],
				sysid_lookup.zc_timezone);
		}

		(void) zccfg_endsysident(zone_handle);
		}

		first = 1;
		// Get capped-memory

		if (zccfg_getmcapent(zone_handle, &mcap_lookup) == ZC_OK) {
		char buf[128];
		if (mcap_lookup.zc_physmem_cap[0] != '\0') {
			clzc_resource_show->addObject(CLZC_PROP_NAME,
				CLZC_CAPPED_MEM);

			bytes_to_units(mcap_lookup.zc_physmem_cap,
				buf, sizeof (buf));
			clzc_resource_show->addProperty(CLZC_CAPPED_MEM,
				prop_types[PT_PHYSICAL], buf);
		}
		}

		uint64_t swap_limit;
		uint64_t locked_limit;

		// Get aliased rctl - ALIAS_MAXSWAP
		if (zccfg_get_aliased_rctl(zone_handle,
			ALIAS_MAXSWAP, &swap_limit) == ZC_OK) {
		char buf[128];
		clzc_resource_show->addObject(CLZC_PROP_NAME,
			CLZC_CAPPED_MEM);

		(void) snprintf(buf, sizeof (buf), "%llu", swap_limit);
		bytes_to_units(buf, buf, sizeof (buf));
		clzc_resource_show->addProperty(CLZC_CAPPED_MEM,
			prop_types[PT_SWAP], buf);
		}

		// Get aliased rctl - ALIAS_MAXLOCKEDMEM
		if (zccfg_get_aliased_rctl(zone_handle,
			ALIAS_MAXLOCKEDMEM, &locked_limit) == ZC_OK) {
		char buf[128];
		clzc_resource_show->addObject(CLZC_PROP_NAME, CLZC_CAPPED_MEM);

		(void) snprintf(buf, sizeof (buf), "%llu", locked_limit);
		bytes_to_units(buf, buf, sizeof (buf));
		clzc_resource_show->addProperty(CLZC_CAPPED_MEM,
			prop_types[PT_LOCKED], buf);
		}

		// Get ipd
		first = 1;
		count = 0;
		char buf[128];
		if (zccfg_setipdent(zone_handle) == ZC_OK) {
		while (zccfg_getipdent(zone_handle, &ipd_lookup) == ZC_OK) {
			clzc_resource_show->addObject(CLZC_PROP_NAME, CLZC_IPD);

			(void) snprintf(buf, sizeof (buf),
				"%s (%d)", prop_types[PT_DIR], count);
			clzc_resource_show->addProperty(CLZC_IPD, buf,
				ipd_lookup.zc_fs_dir);
			count++;
		}

		(void) zccfg_endipdent(zone_handle);
		}

		// Get pset
		first = 1;
		if (zccfg_getpsetent(zone_handle, &pset_lookup) == ZC_OK) {
		char buf[128];
		if (strcmp(pset_lookup.zc_ncpu_min,
			pset_lookup.zc_ncpu_max) == 0) {
			clzc_resource_show->addObject(CLZC_PROP_NAME,
				res_types[RT_DCPU]);

			clzc_resource_show->addProperty(res_types[RT_DCPU],
				prop_types[PT_NCPUS], pset_lookup.zc_ncpu_max);
		} else {
			clzc_resource_show->addObject(CLZC_PROP_NAME,
				res_types[RT_DCPU]);

			snprintf(buf, sizeof (buf), "%s-%s",
				pset_lookup.zc_ncpu_min,
				pset_lookup.zc_ncpu_max);
			clzc_resource_show->addProperty(res_types[RT_DCPU],
				prop_types[PT_NCPUS], buf);
		}

		if (pset_lookup.zc_importance[0] != '\0') {
			clzc_resource_show->addObject(CLZC_PROP_NAME,
				res_types[RT_DCPU]);

			clzc_resource_show->addProperty(res_types[RT_DCPU],
				prop_types[PT_IMPORTANCE],
				pset_lookup.zc_importance);
		}
		}

		// Get pcap
		if (zccfg_get_aliased_rctl(zone_handle,
			ALIAS_CPUCAP, &cap) == ZC_OK) {
		float scaled = cap/100;
		char buf[128];
		clzc_resource_show->addObject(CLZC_PROP_NAME,
			res_types[RT_PCAP]);

		(void) snprintf(buf, sizeof (buf), "%llu", scaled);
		clzc_resource_show->addProperty(res_types[RT_PCAP],
			prop_types[PT_NCPUS], buf);
		}

		// Get rctl
		if (zccfg_setrctlent(zone_handle) == ZC_OK) {
		while (zccfg_getrctlent(zone_handle, &rctl_lookup) == ZC_OK) {
			clzc_resource_show->addObject(CLZC_PROP_NAME,
				CLZC_RCTL);

			struct zc_rctlvaltab *valp;
			clzc_resource_show->addProperty(CLZC_RCTL,
				prop_types[PT_NAME], rctl_lookup.zc_rctl_name);

			for (valp = rctl_lookup.zc_rctl_valp; valp != NULL;
			valp = valp->zc_rctlval_next) {
			clzc_resource_show->addProperty(CLZC_RCTL,
				prop_types[PT_PRIV], valp->zc_rctlval_priv);
			clzc_resource_show->addProperty(CLZC_RCTL,
				prop_types[PT_LIMIT], valp->zc_rctlval_limit);
			clzc_resource_show->addProperty(CLZC_RCTL,
				prop_types[PT_ACTION], valp->zc_rctlval_action);
			}
		}

		(void) zccfg_endrctlent(zone_handle);
		} // end if
	} // end verbose

	clzc_show->addChild(zcname, clzc_resource_show);

	//
	// Determine the underlying Solaris version. If the attribute
	// 'defrouter' is supported, we record the information here.
	//
	defrouter_support = zccfg_attr_exists_in_zones_dtd("defrouter");

	// Get node
	// Create a new ClShow object for all the node specific information
	// TODO: Add other properties that are different from each node in a zc
	zc_node_show = new ClShow(SUBHEAD_ZC_NODES, zcname);
	if (zccfg_setnodeent(zone_handle) == ZC_OK) {
		zc_nwifelem_t *nwif_listp;
		while (zccfg_getnodeent(zone_handle, &node_lookup) == ZC_OK) {
			zc_node_show->addObject(CLZC_NODE_NAME,
				node_lookup.zc_nodename);
			zc_node_show->addProperty(node_lookup.zc_nodename,
				prop_types[PT_PHYSICALHOST],
				node_lookup.zc_nodename);
			zc_node_show->addProperty(node_lookup.zc_nodename,
				prop_types[PT_HOSTNAME],
				node_lookup.zc_hostname);
			zc_net_node_show = new ClShow(SUBHEAD_ZC_RESOURCES,
				node_lookup.zc_nodename);

			if (optflgs & vflg) {
				// Get net
				first = 1;
				nwif_listp = node_lookup.zc_nwif_listp;
				while (nwif_listp != NULL) {
				zc_net_node_show->addObject(CLZC_PROP_NAME,
					res_types[RT_NET]);

				zc_net_node_show->addProperty(res_types[RT_NET],
					prop_types[PT_ADDRESS],
					nwif_listp->elem.zc_nwif_address);
				zc_net_node_show->addProperty(res_types[RT_NET],
					prop_types[PT_PHYSICAL],
					nwif_listp->elem.zc_nwif_physical);
				if (defrouter_support) {
					if (strlen(
					    nwif_listp->elem.zc_nwif_defrouter)
					    > 0) {
						zc_net_node_show->addProperty(
						    res_types[RT_NET],
						    prop_types[PT_DEFROUTER],
						    nwif_listp->elem.
						    zc_nwif_defrouter);
					} else {
						zc_net_node_show->addProperty(
						    res_types[RT_NET],
						    prop_types[PT_DEFROUTER],
						    CLZC_NOVALUE);
					}
				}
				nwif_listp = nwif_listp->next;
				}

				// TODO: Add fs, dev, dataset
				// No implementation available in
				// sczonecfg.c currently

				// Add Resources to the Node Parent
				zc_node_show->addChild(node_lookup.zc_nodename,
					zc_net_node_show);
			}
		}

		// Add Nodes to the Parent Show
		clzc_show->addChild(zcname, zc_node_show);

		(void) zccfg_endnodeent(zone_handle);
	}

	// Close the zonecfg handle
	(void) zccfg_fini_handle(zone_handle);

	return (CL_NOERR);
}
