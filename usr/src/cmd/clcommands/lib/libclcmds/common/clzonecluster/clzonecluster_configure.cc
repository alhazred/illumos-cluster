//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
#pragma ident	"@(#)clzonecluster_configure.cc	1.13	08/07/25 SMI"

//
// Process clzonecluster "configure"
//

#include "clzonecluster.h"
#include "clzonecluster_private.h"

//
// clzonecluster "configure"
//

clerrno_t clzonecluster_configure(ClCommand &cmd, char *zone_name,
	optflgs_t optflgs, char *command_file)
{
	clerrno_t clerrno = CL_NOERR;
	clerrno_t first_err = CL_NOERR;
	char *cmd_str;
	// For the index -z
	int cmd_length_count = 5;

	// Check if we are running on a platform that supports
	// zone cluster feature
	if ((clerrno = validate_zonecluster_support())
		!= CL_NOERR) {
		return (clerrno);
	}

	if (strlen(zone_name) == 0) {
		return (CL_EINVAL);
	}

	// We need to validate the zone_name specified does not
	// conflict with any of the reserver zone cluster names
	if ((clerrno = validate_zonecluster_name(zone_name))
		!= CL_NOERR) {
		return (clerrno);
	}

	// We will not check whether the zone cluster exists
	// since the command supports creation of a new
	// zone cluster. The user may want to run
	// "zonecfg -z <zonename>", hence we will not check
	// whether the zone cluster is valid or not here.

	// If the command file was specified then we have to check
	// whether the file exists and is readable or not
	if (optflgs & fflg) {
		// command file was specified
		// the framework option processing will ensure that
		// if "-f" was specified, so was a command file.

		clerrno = check_file_exists(command_file);

		if (clerrno != CL_NOERR) {
			clerror("Command file %s does not exist or is "
				"not readable.\n", command_file);
			return (CL_ENOENT);
		}

		// If we here then the command file exists
		cmd_length_count += 4; // For -f
		cmd_length_count += strlen(command_file);
	}

	// Set the length of the cmd_str for the command
	cmd_length_count += strlen(zone_name);
	// Get the length of the command
	cmd_length_count += strlen(ZC_CFG_CMD) + 1; // Added for '\0'

	// Allocate the memory for the cmd string
	cmd_str = new char[cmd_length_count];
	// Set the command to execute
	strcpy(cmd_str, ZC_CFG_CMD);
	strcat(cmd_str, " -z ");
	strcat(cmd_str, zone_name);

	// If the -f flag was specified set it for the command too
	if (optflgs & fflg) {
		strcat(cmd_str, " -f ");
		strcat(cmd_str, command_file);
	}

	clerrno = exec_command(cmd_str);

	if (clerrno == CL_EINTERNAL) {
		clerror("Error executing zone configure command - %s\n"
			"Please refer to the console for more "
			"detailed error information.\n", cmd_str);
	}

	if (clerrno == CL_NOERR) {
		(void) sc_publish_zc_event(
			CL_GLOBAL_CLUSTER_NAME, ESC_CLUSTER_ZC_STATE_CHANGE,
			CL_EVENT_PUB_CMD, CL_EVENT_SEV_INFO,
			CL_EVENT_INIT_SYSTEM, 0, 0, 0,
			CL_ZC_NAME, SE_DATA_TYPE_STRING, zone_name,
			CL_REASON_CODE, SE_DATA_TYPE_UINT32,
			CL_REASON_ZC_CONFIGURED, NULL);
	} else {
		(void) sc_publish_zc_event(
			CL_GLOBAL_CLUSTER_NAME, ESC_CLUSTER_ZC_STATE_CHANGE,
			CL_EVENT_PUB_CMD, CL_EVENT_SEV_INFO,
			CL_EVENT_INIT_SYSTEM, 0, 0, 0,
			CL_ZC_NAME, SE_DATA_TYPE_STRING, zone_name,
			CL_REASON_CODE, SE_DATA_TYPE_UINT32,
			CL_REASON_ZC_CONFIGURE_FAILED, NULL);
	}

	delete[] cmd_str;
	return (clerrno);
}
