//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
#pragma ident	"@(#)clzonecluster_install.cc	1.17	09/04/02 SMI"

//
// Process clzonecluster "install"
//

#include "clzonecluster.h"
#include "clzonecluster_private.h"

//
// clzonecluster "install"
//

// Global error variable to set the first error that occurs which will be set
// by any of the threads invoking the operation

// Global variable declared in clzonecluster_common.cc which is used to populate
// the list of zone cluster nodes where there was an error running the zoneadm
// command.
extern char **error_in_nodes;

// Count for the number of nodes where there was an error running the zoneadm
// commands
extern int error_nodes_count;

clerrno_t
clzonecluster_install(ClCommand &cmd, ValueList &nodes,
			char *zone_name) {
	clerrno_t clerrno = CL_NOERR;
	clerrno_t first_err = CL_NOERR;
	char *install_cmd_str = NULL;
	int len_count;
	ValueList node_list, filtered_nodelist, new_node_list;
	cl_cmd_info_t install_cmd_info;
	char *nodename = NULL;
	zone_cluster_state_t state = ZC_STATE_UNKNOWN;
	nodeid_t nodeid;
	int i = 0;
	char *status_str = NULL;

	// Check if we are running on a platform that supports
	// zone cluster feature
	if ((clerrno = validate_zonecluster_support())
		!= CL_NOERR) {
		return (clerrno);
	}

	if (strlen(zone_name) == 0) {
		return (CL_EINVAL);
	}

	/* Check whether it is a valid zone cluster */
	clerrno = check_zone_cluster_exists(zone_name);

	if (clerrno == CL_ENOENT) {
		/* If we are here then it's not a valid zone cluster */
		clerror("Zone cluster %s does not exist.\n",
			zone_name);
		return (clerrno);
	}

	// If there is some other error then we exit.
	if (clerrno != CL_NOERR) {
		clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}

	// Check the validity of the nodes specified.
	if (nodes.getSize() > 0) {
		// We have to check whether these nodes are valid
		first_err = get_valid_nodes(nodes, node_list);

		if (node_list.getSize() <= 0) {
			// None of the nodes were valid
			// We don't have to print an error message here
			// since all the invalid nodes are reported by
			// get_valid_nodes()
			return (CL_ENOENT);
		}
	} else {
		// get the nodelist of the zone cluster
		clerrno = get_nodelist_for_zc(zone_name, node_list);

		// If there is some other error then we exit.
		if (clerrno != CL_NOERR) {
			clcommand_perror(CL_EINTERNAL);
			return (CL_EINTERNAL);
		}
	}

	show_waiting_message(zone_name, ZONE_ADM_INSTALL_SUBCMD);

	// Now we can boot the cluster
	// First form the zoneadm command
	// The command that needs to be formed here is
	// of the format "-z <zonename> install"
	len_count = strlen(ZONE_ADM_INSTALL_SUBCMD);
	// We are adding 2 for the spaces before & after "-z"
	len_count += strlen(ZONE_ADM_ZONE_OPTION) + 2;
	len_count += strlen(zone_name) + 1; // Added +1 for '\0'
	install_cmd_str = new char[len_count];

	strcpy(install_cmd_str, ZONE_ADM_ZONE_OPTION); // -z
	strcat(install_cmd_str, " ");
	strcat(install_cmd_str, zone_name); // zonename
	strcat(install_cmd_str, " ");
	strcat(install_cmd_str, ZONE_ADM_INSTALL_SUBCMD); // install
	install_cmd_info.cmd_str = install_cmd_str;
	install_cmd_info.redirect_output = 1;

	for (node_list.start(); !node_list.end();
			node_list.next()) {
		nodename = node_list.getValue();
		// get the individual status
		nodeid = clconf_cluster_get_nodeid_by_nodename(nodename);

		if ((clerrno = get_zone_cluster_state_on_node(zone_name,
			nodeid,	&status_str)) != CL_NOERR) {
			clerror("Failed to get the node status\n");
		}

		(void) get_zone_state_on_node(zone_name, nodeid, &state);
		if (state < ZC_STATE_INSTALLED) {
			filtered_nodelist.add(nodename);
		}
	}

	// Execute the "zoneadm install" command
	clerrno = exec_zone_adm_cmd_on_nodes(filtered_nodelist,
					install_cmd_info);
	delete[] install_cmd_str;

	// Check for any errors
	if (clerrno != CL_NOERR) {
		//
		// Filter the nodes on which the install command succeeded
		// and call sysidcfg on only those set of nodes
		// flag to detect if the given node is in the error node list
		//
		int flag = 0;

		//
		// Loop through the node_list of the zone cluster and remove
		// all the nodes where there was an error.
		// Resulting new_node_list ValueList will contain only those
		// nodes where install completed successfully
		//

		for (filtered_nodelist.start(); !filtered_nodelist.end();
			filtered_nodelist.next()) {
			flag = 0;
			for (i = 0; i < error_nodes_count; i++) {
				if (strncmp(filtered_nodelist.getValue(),
					error_in_nodes[i],
					strlen(error_in_nodes[i])) == 0) {
					flag = 1;
					break;
				}
			}

			if (flag == 0) {
				new_node_list.add(node_list.getValue());
			}
		}
	} else {
		for (filtered_nodelist.start(); !filtered_nodelist.end();
			filtered_nodelist.next()) {
			new_node_list.add(filtered_nodelist.getValue());
		}
	}

	//
	// If we are here, then the "zoneadm install" was a success.
	// We have to perform a sysidcfg now.
	// The command that needs to be formed here is of the format
	// "<zonename> sysidcfg".
	//
	// We are adding 1 for the space after <zonename>
	//
	len_count = strlen(zone_name) + 1;
	// Added +1 for '\0'
	len_count += strlen(CLZ_HELPER_SYSIDCFG_SUBCMD) + 1;
	install_cmd_str = new char[len_count];
	strcpy(install_cmd_str, zone_name);
	strcat(install_cmd_str, " ");
	strcat(install_cmd_str, CLZ_HELPER_SYSIDCFG_SUBCMD);
	install_cmd_info.cmd_str = install_cmd_str;
	// Execute the "clzone_helper <zonename> sysidcfg"
	clerrno = exec_zone_adm_cmd_on_nodes(new_node_list, install_cmd_info,
				SCRCMD_CLZONE_HELPER_SUBCMD);

	if (clerrno == CL_NOERR) {
		(void) sc_publish_zc_event(
			CL_GLOBAL_CLUSTER_NAME, ESC_CLUSTER_ZC_STATE_CHANGE,
			CL_EVENT_PUB_CMD, CL_EVENT_SEV_INFO,
			CL_EVENT_INIT_SYSTEM, 0, 0, 0,
			CL_ZC_NAME, SE_DATA_TYPE_STRING, zone_name,
			CL_REASON_CODE, SE_DATA_TYPE_UINT32,
			CL_REASON_ZC_INSTALLED, NULL);
	} else {
		(void) sc_publish_zc_event(
			CL_GLOBAL_CLUSTER_NAME, ESC_CLUSTER_ZC_STATE_CHANGE,
			CL_EVENT_PUB_CMD, CL_EVENT_SEV_INFO,
			CL_EVENT_INIT_SYSTEM, 0, 0, 0,
			CL_ZC_NAME, SE_DATA_TYPE_STRING, zone_name,
			CL_REASON_CODE, SE_DATA_TYPE_UINT32,
			CL_REASON_ZC_INSTALL_FAILED, NULL);
	}

	delete[] install_cmd_str;
	return (clerrno);
}
