//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
#pragma ident	"@(#)clzonecluster_status.cc	1.13	08/07/25 SMI"

//
// Process clzonecluster "status"
//

#include "clzonecluster.h"
#include "ClStatus.h"
#include <sys/clconf_int.h>

#include "clzonecluster_private.h"
#include <libzccfg/libzccfg.h>

static clerrno_t
clzc_get_status(ValueList &zcnames, optflgs_t optflgs, ClStatus *clzc_status);

//
// clzonecluster "status"
//

clerrno_t
clzonecluster_status(ClCommand &cmd, ValueList operands, optflgs_t optflgs) {
	ClStatus *clzc_status = new ClStatus(cmd.isVerbose ? true : false);
	ValueList filtered_zcs;
	clerrno_t clerrno = CL_NOERR;

	// Check if we are running on a platform that supports
	// zone cluster feature
	if ((clerrno = validate_zonecluster_support())
		!= CL_NOERR) {
		return (clerrno);
	}

	filtered_zcs.clear();
	operands.start();

	if (!(strcmp(operands.getValue(), CLCOMMANDS_WILD_OPERAND))) {
		if ((clerrno = get_zone_cluster_names(filtered_zcs)) !=
			CL_NOERR) {
			clerror("Unable to get list of zone clusters\n");
			return (CL_EINTERNAL);
		}
	} else {
		for (operands.start(); !operands.end(); operands.next()) {
			if (check_zone_cluster_exists(operands.getValue()) ==
				CL_NOERR) {
				filtered_zcs.add(operands.getValue());
			} else {
				clerror("Zone cluster %s does not exist.\n",
					operands.getValue());
			}
		}
	}


	if (filtered_zcs.getSize() <= 0) {
		return (CL_ENOENT);
	}

	// Get the zc status
	clerrno = clzc_get_status(filtered_zcs, optflgs, clzc_status);

	clzc_status->setHeadingTitle(HEAD_ZC);
	clzc_status->setSectionTitle(SECTION_ZC_NODES);


	clzc_status->enableRowGrouping(true);
	clzc_status->setHeadings((char *)gettext("Name"),
		(char *)gettext("Node Name"), (char *)gettext("Zone HostName"),
		(char *)gettext("Status"), (char *)gettext("Zone Status"));

	// Print the Zone Cluster Status
	clzc_status->print();


	delete clzc_status;

	return (clerrno);
}

clerrno_t
get_zonecluster_status_obj(optflgs_t optflgs, ClStatus *clzc_status) {
	ValueList zc_list;
	clerrno_t clerrno = CL_NOERR;

	clerrno = get_zone_cluster_names(zc_list);
	if (clerrno != CL_NOERR) {
		clerror("Unable to get list of Zone Clusters\n");
		return (CL_EINTERNAL);
	}

	// Get the zc status
	clerrno = clzc_get_status(zc_list, optflgs, clzc_status);
	if (clerrno != CL_NOERR) {
		clerror("Unable to get status of Zone Clusters\n");
		// Free memory before returning
		zc_list.clear();
		return (CL_EINTERNAL);
	}
	// Set the headings
	clzc_status->setHeadingTitle(HEAD_ZC);
	clzc_status->setSectionTitle(SECTION_ZC_NODES);

	clzc_status->enableRowGrouping(true);
	clzc_status->setHeadings((char *)gettext("Name"),
		(char *)gettext("Node Name"), (char *)gettext("Zone HostName"),
		(char *)gettext("Status"), (char *)gettext("Zone Status"));

	zc_list.clear();
	return (clerrno);
}

clerrno_t
clzc_get_status(ValueList &zcnames, optflgs_t optflgs, ClStatus *clzc_status) {

	ValueList col1, col2, col3, col4, col5;
	ValueList node_list, hostname_list;
	int err = 0;
	int first = 0;
	clerrno_t clerr;
	char *zcname;
	char *nodename;
	char *status_str = "Unknown";
	char zc_status[128];
	char node_status[128];
	zone_cluster_state_t	state = ZC_STATE_UNKNOWN;
	zone_cluster_state_t    max_state = ZC_STATE_UNKNOWN;
	nodeid_t nodeid;


	for (zcnames.start(); !zcnames.end(); zcnames.next()) {
		// Check if the zcname is valid
		zcname = zcnames.getValue();

		// Clear all the existing values
		node_list.clear();
		hostname_list.clear();

		if ((clerr = check_zone_cluster_exists(zcname)) != CL_NOERR) {
			clerror("Zone cluster %s does not exist\n", zcname);
			return (clerr);
		}

		// Add the Zone Cluster Name
		col1.add(zcname);

		node_list.clear();
		// Get all the nodes of the zone cluster
		if ((clerr = get_nodelist_for_zc(zcname,
			node_list)) != CL_NOERR) {
			clerror("Failed to get the nodelist for zone "
				"cluster \"%s\"\n", zcname);
			return (clerr);
		}

		// Get all the nodes of the zone cluster
		if ((clerr = get_hostname_list_for_zc(zcname,
			hostname_list)) != CL_NOERR) {
			clerror("Failed to get the nodelist for zone "
				"cluster \"%s\"\n", zcname);
			return (clerr);
		}

		first = 1;
		for (node_list.start(), hostname_list.start(); !node_list.end();
		    node_list.next(), hostname_list.next()) {
			if (first == 1) {
				first = 0;
			} else {
				// Add dummy col data for row grouping
				col1.add("");
			}

			// Add the nodename
			nodename = node_list.getValue();
			col2.add(nodename);

			// Add the hostname
			col3.add(hostname_list.getValue());

			// Loop through all the nodes and
			// get the individual status
			nodeid = clconf_cluster_get_nodeid_by_nodename
				(nodename);

			if ((clerr = get_zone_cluster_state_on_node(zcname,
				nodeid,	&status_str)) != CL_NOERR) {
				clerror("Failed to get the node status\n");
			}

			col4.add((char *)gettext(status_str));

			(void) get_zone_state_on_node(zcname, nodeid, &state);
			(void) snprintf(node_status,
				sizeof (node_status), "%s",
				zone_cluster_state_str(state));

			col5.add(node_status);
		}
	}

	if (col1.getSize() > 0) {
		for (col1.start(), col2.start(), col3.start(), col4.start(),
			col5.start();
		col1.end() != 1;
		col1.next(), col2.next(), col3.next(), col4.next(),
			col5.next()) {

			clzc_status->addRow(col1.getValue(), col2.getValue(),
				col3.getValue(), col4.getValue(),
				col5.getValue());
		}
	}

	return (CL_NOERR);
}
