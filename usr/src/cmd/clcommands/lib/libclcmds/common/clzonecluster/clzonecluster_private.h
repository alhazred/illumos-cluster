//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef	_CLZONECLUSTER_PRIVATE_H
#define	_CLZONECLUSTER_PRIVATE_H

#pragma ident	"@(#)clzonecluster_private.h	1.13	08/07/24 SMI"

//
// Private header for clzonecluster
// Not intended for clients of clzonecluster library.
//

#include <clzonecluster.h>
#include <common.h>
#include <libzccfg/libzccfg.h>

#define	ZC_CFG_CMD			"/usr/cluster/lib/sc/sczonecfg"
#define	ZONE_ADM_CMD			"/usr/sbin/zoneadm"
#define	ZONE_ADM_ZONE_OPTION		"-z"
#define	ZONE_ADM_FORCE_OPTION		"-F"
#define	ZONE_ADM_COPY_OPTION		"-m"
#define	ZONE_ADM_BOOT_SUBCMD		"boot"
#define	ZONE_ADM_HALT_SUBCMD		"halt"
#define	ZONE_ADM_INSTALL_SUBCMD		"install"
#define	ZONE_ADM_REBOOT_SUBCMD		"reboot"
#define	ZONE_ADM_UNINSTALL_SUBCMD	"uninstall"
#define	ZONE_ADM_VERIFY_SUBCMD		"verify"
#define	ZONE_ADM_READY_SUBCMD		"ready"
#define	ZONE_ADM_MOVE_SUBCMD		"move"
#define	ZONE_ADM_CLONE_SUBCMD		"clone"
#define	SCRCMD_ZONE_ADM_SUBCMD		"administerzone"
#define	SCRCMD_CLZONE_HELPER_SUBCMD	"clzonehelper"
#define	CLZ_HELPER_SYSIDCFG_SUBCMD	"sysidcfg"
#define	ZC_CFG_EXPORT_SUBCMD		"export"
#define	ZC_CFG_DELETE_SUBCMD		"delete"
#define	ZC_CFG_FILE_OPTION		"-f"
#define	ZC_LOG_FILE			"/var/cluster/logs/clzonecluster.log."
/*
 * This is an error handler to handle error cases
 * while executing "zoneadm" commands
 */

void clzonecluster_cmd_errhandler(cl_cmd_info_t cmd_info);

/*
 * Executes a given zoneadm command("cmd") on all the
 * specified nodes. This method will execute the command
 * on each node in a separate thread so that the execution
 * is parallel on each node. This method will prepend
 * "scrcmd <scrcmd_sub_cmd>" before the command string and
 * execute it. This function uses the struct
 * cl_cmd_info to fetch the required information. In
 * the case where the command being executed causes an error,
 * this function will invoke the error handled specified.
 * This function assumes that the nodes specified in the
 * nodelist are valid and does not perform any checks
 * thereof. This function will return the first error
 * that occurs while executing any of the commands.
 * If "scrcmd_sub_cmd" is not specified, then the default
 * sub-command is "administerzone".
 *
 * Possible return values:
 *
 * 	CL_NOERR		- success
 *	CL_EINTERNAL		- internal error
 *	CL_EINVAL		- invalid argument
 */
clerrno_t exec_zone_adm_cmd_on_nodes(ValueList &nodes,
	cl_cmd_info_t cmd_info,
	char *scrcmd_sub_cmd = SCRCMD_ZONE_ADM_SUBCMD);


//
// filter_nodelist_for_zc
//
// Verifies whether the given nodelist is a sub-set of
// the nodelist of zone cluster. If "nodes" contains nodes which
// do not form part of the zone cluster, then they are stipped off. If the
// zone cluster spans the nodelist then this function returns CL_NOERR, and
// if it doesn't then the function returns CL_EINVAL.
//
// Return Values :
//	CL_NOERR	- All nodes in the list are part of zone cluster
//	CL_EINVAL	- One or more nodes are not part of zone cluster
//	CL_ENOENT	- No such zone cluster exists
//
clerrno_t
filter_nodelist_for_zc(char *zonename, ValueList &nodes,
	ValueList &valid_nodes);

void show_waiting_message(char *zcname, char *operation);

#endif /* _CLZONECLUSTER_PRIVATE_H */
