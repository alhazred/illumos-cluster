//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
#pragma ident	"@(#)clzonecluster_uninstall.cc	1.17	08/10/14 SMI"

//
// Process clzonecluster "uninstall"
//

#include "clzonecluster.h"
#include "clzonecluster_private.h"

#include <scadmin/scstat.h>
#include <rgm/rgm_scrgadm.h>
#include "rgm_support.h"
#include "clresourcegroup.h"
#include "clresourcetype.h"

//
// clzonecluster "uninstall"
// This file is similar to clzonecluster_install,
// but we need to keep the two files separate due
// to difference in the option processing
//

clerrno_t clzonecluster_uninstall(ClCommand &cmd, ValueList &nodes,
	char *zone_name, optflgs_t optflgs) {

	clerrno_t clerrno = CL_NOERR;
	clerrno_t first_err = CL_NOERR;
	char *uninstall_cmd_str = NULL;
	int len_count, str_len;
	ValueList node_list, filtered_nodelist;
	cl_cmd_info_t uninstall_cmd_info;
	char *rptr;
	char replybuf[BUFSIZ];
	bool force_flag = false;
	char **rg_names = NULL;
	boolean_t verbose;
	char *nodename = NULL;
	zone_cluster_state_t state = ZC_STATE_UNKNOWN;
	nodeid_t nodeid;
	char *status_str = NULL;


	// Check if we are running on a platform that supports
	// zone cluster feature
	if ((clerrno = validate_zonecluster_support())
		!= CL_NOERR) {
		return (clerrno);
	}

	if (strlen(zone_name) == 0) {
		return (CL_EINVAL);
	}

	/* Check whether it is a valid zone cluster */
	clerrno = check_zone_cluster_exists(zone_name);

	if (clerrno == CL_ENOENT) {
		/* If we are here then it's not a valid zone cluster */
		clerror("Zone cluster %s does not exist.\n",
			zone_name);
		return (clerrno);
	}

	// If there is some other error then we exit.
	if (clerrno != CL_NOERR) {
		clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}

	// Check the validity of the nodes specified.
	if (nodes.getSize() > 0) {
		// We have to check whether these nodes are valid
		clerrno = get_valid_nodes(nodes, node_list);

		if (node_list.getSize() <= 0) {
			// None of the nodes were valid
			// We don't have to print an error message here
			// since all the invalid nodes are reported by
			// get_valid_nodes()
			return (CL_ENOENT);
		}
	} else {
		// get the nodelist of the zone cluster
		clerrno = get_nodelist_for_zc(zone_name, node_list);

		// If there is some other error then we exit.
		if (clerrno != CL_NOERR) {
			clcommand_perror(CL_EINTERNAL);
			return (CL_EINTERNAL);
		}
	}

	// If there is some other error then we exit.
	if (clerrno != CL_NOERR) {
		clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}

	// If the -F option was not specified then we need to
	// confirm with the admin whether he wants to uninstall
	// the zone cluster.
	if (!(optflgs & Fflg)) {

		// If we are here then the -F option was not specified
		// We have to check with the admin

		if (strcmp(gettext("yes"), "yes") == 0 &&
		    strcmp(gettext("no"), "no") == 0) {
			// The l10n language is english, so
			// we can stick with 'y' and 'n'.
			(void) fprintf(stderr,
				gettext("Are you sure you want to uninstall "
					"zone cluster %s (y/[n])?"),
					zone_name);
			rptr = fgets(replybuf, sizeof (replybuf) - 1, stdin);
			if (rptr) {
				str_len = strlen(replybuf) - 1;
				if (strncasecmp(replybuf, "y", str_len) == 0 ||
				    strncasecmp(replybuf, "yes", str_len) == 0)
				{
					force_flag = true;
				}
			}
		} else {
			// The l10n language is not english, so
			// we have to stick with 'yes' and 'no'
			(void) fprintf(stderr,
				gettext("Are you sure you want to uninstall "
					"zone cluster %s (%s/[%s])?"),
					zone_name, gettext("yes"),
					gettext("no"));
			rptr = fgets(replybuf, sizeof (replybuf) - 1, stdin);
			if (rptr) {
				while (str_len < BUFSIZ &&
					replybuf[str_len] != '\n')
					str_len++;
				if (strncasecmp(replybuf, gettext("yes"),
					str_len) == 0) {
					force_flag = true;
				}
			}

		}
	} else {
		force_flag = true;
	}

	// If the force flag was not specified or if the
	// admin did not confirm the operation, then we return
	if (force_flag == false) {
		return (CL_NOERR);
	}

	if (optflgs & vflg)
		verbose = B_TRUE;
	else
		verbose = B_FALSE;

	// Get RGs in the given zone cluster
	clerrno = z_getrglist(&rg_names, verbose, 1,
		zone_name);

	if (clerrno != CL_NOERR) {
		return (clerrno);
	}

	if (rg_names != NULL) {
		clerror("Remove all resource groups from zone cluster (\"%s\") "
			"before uninstalling.\n", zone_name);
		return (CL_EOP);
	}

	show_waiting_message(zone_name, ZONE_ADM_UNINSTALL_SUBCMD);

	// Now we can uninstall the cluster
	// First form the zoneadm command
	len_count = strlen(ZONE_ADM_UNINSTALL_SUBCMD);
	len_count += strlen(ZONE_ADM_ZONE_OPTION) + 2;
	len_count += strlen(ZONE_ADM_FORCE_OPTION) + 1;
	len_count += strlen(zone_name) + 1; // Added for '\0'
	// uninstall_cmd_str = (char *) calloc(1, len_count);
	uninstall_cmd_str = new char[len_count];
	strcpy(uninstall_cmd_str, ZONE_ADM_ZONE_OPTION); // -z
	strcat(uninstall_cmd_str, " ");
	strcat(uninstall_cmd_str, zone_name); // zone-name
	strcat(uninstall_cmd_str, " ");
	strcat(uninstall_cmd_str, ZONE_ADM_UNINSTALL_SUBCMD); // uninstall
	strcat(uninstall_cmd_str, " ");
	strcat(uninstall_cmd_str, ZONE_ADM_FORCE_OPTION); // -F
	uninstall_cmd_info.cmd_str = uninstall_cmd_str;
	uninstall_cmd_info.redirect_output = 0;

	for (node_list.start(); !node_list.end();
			node_list.next()) {
		nodename = node_list.getValue();
		// get the individual status
		nodeid = clconf_cluster_get_nodeid_by_nodename(nodename);

		if ((clerrno = get_zone_cluster_state_on_node(zone_name,
			nodeid,	&status_str)) != CL_NOERR) {
			clerror("Failed to get the node status\n");
			continue;
		}

		(void) get_zone_state_on_node(zone_name, nodeid, &state);
		if (state >= ZC_STATE_INSTALLED) {
			filtered_nodelist.add(nodename);
		}
	}

	// Execute the command
	clerrno = exec_zone_adm_cmd_on_nodes(filtered_nodelist,
				uninstall_cmd_info);

	if (clerrno == CL_NOERR) {
		(void) sc_publish_zc_event(
			CL_GLOBAL_CLUSTER_NAME, ESC_CLUSTER_ZC_STATE_CHANGE,
			CL_EVENT_PUB_CMD, CL_EVENT_SEV_INFO,
			CL_EVENT_INIT_SYSTEM, 0, 0, 0,
			CL_ZC_NAME, SE_DATA_TYPE_STRING, zone_name,
			CL_REASON_CODE, SE_DATA_TYPE_UINT32,
			CL_REASON_ZC_UNINSTALLED, NULL);
	} else {
		(void) sc_publish_zc_event(
			CL_GLOBAL_CLUSTER_NAME, ESC_CLUSTER_ZC_STATE_CHANGE,
			CL_EVENT_PUB_CMD, CL_EVENT_SEV_INFO,
			CL_EVENT_INIT_SYSTEM, 0, 0, 0,
			CL_ZC_NAME, SE_DATA_TYPE_STRING, zone_name,
			CL_REASON_CODE, SE_DATA_TYPE_UINT32,
			CL_REASON_ZC_UNINSTALL_FAILED, NULL);
	}

	delete[] uninstall_cmd_str;
	return (clerrno);
}
