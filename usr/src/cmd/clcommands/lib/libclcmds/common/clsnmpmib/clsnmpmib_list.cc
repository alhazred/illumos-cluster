//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clsnmpmib_list.cc	1.3	08/05/20 SMI"

//
// Process clsnmpmib "list"
//

#include "clsnmpmib.h"
#include "ClList.h"

#include "ClCommand.h"

//
// The the MIBs, and possibly their config info.
//
// Return values:
//
//	CL_NOERR
//	CL_EINTERNAL
//	CL_ENOENT
//
clerrno_t
clsnmpmib_list(
    ClCommand &cmd,
    ValueList &operands,
    ValueList &nodes)
{
	int outofcluster = 0;

	// Validate the operands which are passed in
	ValueList new_operands;
	clerrno_t err = get_mib_operands(operands, new_operands);

	// Verify nodelist
	if (nodes.getSize() == 0) {
		set_error(err, add_this_node(nodes));
	}
	set_error(err, verify_nodelist(nodes));

	// Verify if the nodes in the nodelist are active cluster members.
	set_error(err, verify_nodes_clustmode(nodes, outofcluster));

	if (outofcluster)
		clerror("SNMP MIB information cannot be retrieved from "
		    "offline nodes.\n");

	// See if there is anything to print
	if (new_operands.getSize() == 0)
		goto cleanup;

	// If command is not verbose, then we can simply print the
	// list of operands for each node instance.
	if (!cmd.isVerbose) {
		ClList list;
		for (new_operands.start();
		    !new_operands.end();
		    new_operands.next()) {

			list.addRow(new_operands.getValue());
		}

		for (int i = 0; i < nodes.getSize(); i++)
			list.print();
	} else {
		// Call the sceventmib shell script to print verbose
		//lint -e(1550)
		char **argv = new char *[5];
		argv[0] = strdup("sceventmib");	// The command
		argv[1] = strdup("-x");		// CLCMD flag
		argv[2] = strdup("-p");		// Print flag
		argv[3] = strdup("mib-list");	// Print mibs
		argv[4] = NULL;

		for (nodes.start();
		    !nodes.end();
		    nodes.next()) {

			set_error(err,
			    run_sceventmib_cmd_on_node(argv, nodes.getValue()));
		}

		delete [] argv[4];
		delete [] argv[3];
		delete [] argv[2];
		delete [] argv[1];
		delete [] argv[0];
		delete [] argv;
	}

cleanup:

	return (err);
}
