//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clsnmpmib_export.cc	1.3	08/05/20 SMI"

//
// Process clsnmpmib "export"
//

#include "clsnmpmib.h"

#include "ClCommand.h"
#include "ClXml.h"

//
// This function returns a ClsnmpmibExport object populated with
// all of the snmpmib information from the cluster.  Caller is
// responsible for releasing the ClcmdExport object.
//
ClcmdExport *
get_clsnmpmib_xml_elements()
{
	// Get the cluster nodes
	ValueList nodes;
	(void) get_all_cluster_nodes(nodes);
	ValueList operands;

	//lint -e(1550)
	ClsnmpmibExport *mib = new ClsnmpmibExport();
	(void) get_export_obj(*mib, operands, nodes);
	return (mib);
}

//
// Populates export_obj with the MIB configuration information
// based on the operands and nodes in the lists.
//
// Return values:
//
//	CL_NOERR
//	CL_EINTERNAL
//	CL_ENOENT
//
clerrno_t
get_export_obj(
    ClsnmpmibExport &export_obj,
    ValueList &operands,
    ValueList &nodes)
{
	// Get the right mibs
	ValueList new_operands;
	clerrno_t err = get_mib_operands(operands, new_operands);
	nodeid_t nodeid;
	uint_t ismember;

	// If there were no matching operands, don't do anything
	if (new_operands.getSize() == 0) {
		return (err);
	}

	for (nodes.start();
	    !nodes.end();
	    nodes.next()) {

		//
		// Check to see if the node is an active cluster member.
		// For cases like cluster export, we may reach here with
		// the nodelist having nodes which are out of the cluster
		// at that point of time...
		//
		set_error(err, scconf_get_nodeid(nodes.getValue(), &nodeid));

		(void) scconf_ismember(nodeid, &ismember);

		if (!ismember) {
			clerror("SNMP MIB information cannot be retrieved "
			    "from offline node \"%s\".\n", nodes.getValue());
			continue;
		}

		// The rpc program already knows the command name,
		// so we only need to pass the arguments
		char *cmd = strdup("-x -p mib-list");

		// Get a FILE * with the stdout from the cmd
		FILE *file = NULL;
		set_error(err, pipe_sceventmib_cmd_on_node(
		    &file, cmd, nodes.getValue()));

		// Parse the data from stdout and add it to the
		// ClsnmpmibExport object.
		set_error(err, parse_stdout_to_snmp_mib_xml(
		    export_obj, file, nodes.getValue()));

		// Close the pipe
		(void) pclose(file);
	}

	return (err);
}

//
// Parses the data from a stdout pipe, file, into the MIB xml
// configuration information.
//
// Return values:
//
//	CL_NOERR
//	CL_EINTERNAL
//
clerrno_t
parse_stdout_to_snmp_mib_xml(
    ClsnmpmibExport &export_obj,
    FILE *file,
    char *node)
{
	if (!node)
		return (CL_EINTERNAL);

	clerrno_t err = strip_list_headings(file);

	char c;
	int count = 0;
	int datum = 1;
	char temp[128];
	temp[0] = '\0';
	char *name = NULL;
	while ((c = (char)fgetc(file)) != EOF) {
		// Now we are at the data that we want.  It is of the form:
		//
		// <mib name>  <state>  <protocol>\n

		// End of line, finish off the word and add the MIB info
		// to the export_obj
		if (c == '\n') {
			if (datum != 3)
				return (CL_EINTERNAL);

			temp[count] = '\0';
			if (strcasecmp("SNMPv2", temp) == 0) {
				err = export_obj.createSNMPMib(
				    name,
				    node,
				    SNMPv2);
			} else if (strcasecmp("SNMPv3", temp) == 0) {
				err = export_obj.createSNMPMib(
				    name,
				    node,
				    SNMPv3);
			} else
				return (CL_EINTERNAL);

			datum = 1;
			count = 0;
			temp[count] = '\0';
			name = NULL;

		// End of one value
		} else if (c == ' ') {
			// Cycle to the next word
			do { c = (char)fgetc(file); } while (c == ' ');
			(void) ungetc(c, file);

			switch (datum) {

			case 1:
				temp[count] = '\0';
				name = strdup(temp);
				count = 0;
				temp[count] = '\0';
				++datum;
				break;
			case 2:
				count = 0;
				temp[count] = '\0';
				++datum;
				break;
			default:
				return (CL_EINTERNAL);
			}
		} else {
			temp[count++] = c;
		}
	}

	return (err);
}

//
// Exports the MIB information in xml format to either stdout or a file,
// depending on the value of clconfiguration.
//
// Return values:
//
// 	CL_NOERR
//	CL_EINTERNAL
//	CL_EINVAL
//	CL_ENOENT
//
clerrno_t
clsnmpmib_export(
    ValueList &operands,
    ValueList &nodes,
    char *clconfiguration)
{
	clerrno_t err = CL_NOERR;
	int outofcluster = 0;

	// Set the doc to be stdout
	if (!clconfiguration)
		clconfiguration = strdup("-");

	// verify the nodelist
	if (nodes.getSize() == 0) {
		set_error(err, add_this_node(nodes));
	}
	set_error(err, verify_nodelist(nodes));

	// Verify if the nodes in the nodelist are active cluster members.
	set_error(err, verify_nodes_clustmode(nodes, outofcluster));

	if (outofcluster)
		clerror("SNMP MIB information cannot be retrieved from "
		    "offline nodes.\n");

	// Get the ClsnmpmibExport object
	ClXml xml;
	ClsnmpmibExport export_obj;
	set_error(err, get_export_obj(export_obj, operands, nodes));

	if (err != CL_ENOENT) {
		// Create the file and print it to either stdout or a file
		set_error(err, xml.createExportFile(
		    CLSNMPMIB_CMD,
		    clconfiguration,
		    &export_obj));
	}

	return (err);
}
