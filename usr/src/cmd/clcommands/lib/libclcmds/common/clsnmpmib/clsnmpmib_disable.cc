//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clsnmpmib_disable.cc	1.4	08/10/13 SMI"

//
// Process clsnmpmib "disable"
//

#include "clsnmpmib.h"

#include "ClCommand.h"

//
// Disable one or more MIBs on one or more nodes.
//
// Return values:
//
//	CL_NOERR
//	CL_EINTERNAL
//	CL_ENOENT
//
clerrno_t
clsnmpmib_disable(
    ClCommand &cmd,
    ValueList &operands,
    ValueList &nodes)
{
	int outofcluster = 0;

	// Validate the operands which are passed in
	ValueList new_operands;
	clerrno_t err = get_mib_operands(operands, new_operands);

	// Verify the nodelist
	if (nodes.getSize() == 0) {
		set_error(err, add_this_node(nodes));
	}
	set_error(err, verify_nodelist(nodes));

	// Verify if the nodes in the nodelist are active cluster members.
	set_error(err, verify_nodes_clustmode(nodes, outofcluster));

	if (outofcluster)
		clerror("SNMP MIBs cannot be disabled on offline nodes.\n");

	// Call the sceventmib shell script to disable each MIB
	//lint -e(1550)
	char **argv = new char *[4];
	argv[0] = strdup("sceventmib");	// The command
	argv[1] = strdup("-x");		// CLCMD flag
	argv[2] = strdup("-n");		// The disable flag
	argv[3]	= NULL;

	for (new_operands.start();
	    !new_operands.end();
	    new_operands.next()) {

		for (nodes.start();
		    !nodes.end();
		    nodes.next()) {

			// run the command
			clerrno_t status = run_sceventmib_cmd_on_node(
			    argv, strdup(nodes.getValue()));

			set_error(err, status);
			if (!status && cmd.isVerbose) {
				clmessage("Disabled \"%s\" "
				    "MIB on node \"%s\".\n",
				    new_operands.getValue(),
				    nodes.getValue());
			}
		}
	}

	delete [] argv[3];
	delete [] argv[2];
	delete [] argv[1];
	delete [] argv[0];
	delete [] argv;

	return (err);
}
