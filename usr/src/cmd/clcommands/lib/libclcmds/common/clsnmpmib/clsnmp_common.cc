//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clsnmp_common.cc	1.7	08/05/20 SMI"

//
// Common functions for clsnmp family of commands
//

#include "clsnmpmib.h"
#include "rpc_scrcmd.h"

// The list of mibs on the cluster
char *CLUSTER_MIBS[] = { "event", NULL };

// The scrcmd binary to make rpc calls remotely
#define	SCRCMD	"/usr/cluster/lib/sc/scrcmd"

//
// This function will execute the scrcmd command on the node specified,
// calling the 'snmp' scrcmd command, and passing the arguments listed
// in argv.  It also uses the '-f' option to pass the name of a file
// to scrcmd, which will then read the contents of the file and pass it
// along to the remote node, eventually creating a temporary file with
// the contents anew, and launching sceventmib with the filename appended
// at the end.  When using this function, be sure have a '-f' at the end
// of the argv string so that the sceventmib script will not give a usage
// error.
//
// Return values:
//
//	CL_NOERR
//	CL_EINTERNAL
//	CL_EEXIST
//	CL_ENOENT
//
clerrno_t
run_sceventmib_cmd_on_node_with_file(char *argv, char *node, char *file)
{
	char hostname[MAXHOSTNAMELEN];
	clerrno_t err = CL_NOERR;

	if (!argv || !node || !file)
		return (CL_EINTERNAL);

	(void) gethostname(hostname, sizeof (hostname));

	// lint -e(1550)
	char **args = new char *[8];

	if (strcmp(hostname, node) == 0) {
		args[0] = "scrcmd";
		args[1] = "-S";
		args[2] = "-f";
		args[3] = file;
		args[4] = "snmp";
		args[5] = argv;
		args[6] = NULL;
		err = execute_scrcmd_command(args);
	} else {
		args[0] = "scrcmd";
		args[1] = "-N";
		args[2] = node;
		args[3] = "-f";
		args[4] = file;
		args[5] = "snmp";
		args[6] = argv;
		args[7] = NULL;
		err = execute_scrcmd_command(args);
	}

	delete [] args;
	return (err);
}

//
// This function will execute the scrcmd command on the node specified,
// calling the 'snmp' scrcmd command, and passing the arguments listed
// in argv.
//
// Return values:
//
//	CL_NOERR
//	CL_EINTERNAL
//	CL_EEXIST
//	CL_ENOENT
//
clerrno_t
run_sceventmib_cmd_on_node(char *argv, char *node)
{
	char hostname[MAXHOSTNAMELEN];

	if (!argv || !node)
		return (CL_EINTERNAL);

	(void) gethostname(hostname, sizeof (hostname));

	char *args[6];

	if (strcmp(hostname, node) == 0) {
		args[0] = "scrcmd";
		args[1] = "-S";
		args[2] = "snmp";
		args[3] = argv;
		args[4] = NULL;
	} else {
		args[0] = "scrcmd";
		args[1] = "-N";
		args[2] = node;
		args[3] = "snmp";
		args[4] = argv;
		args[5] = NULL;
	}

	clerrno_t err = execute_scrcmd_command(args);

	return (err);
}


//
// This function will execute the scrcmd command on the node specified,
// calling the 'snmp' scrcmd command, and passing the arguments listed
// in argv.
//
// Return values:
//
//	CL_NOERR
//	CL_EINTERNAL
//	CL_EEXIST
//	CL_ENOENT
//
clerrno_t
run_sceventmib_cmd_on_node(char *argv[], char *node)
{
	char hostname[MAXHOSTNAMELEN];
	clerrno_t err = CL_NOERR;

	if (!argv || !node)
		return (CL_EINTERNAL);

	(void) gethostname(hostname, sizeof (hostname));

	// lint -e(1550)
	char *args[6];
	int count = 1;

	if (strcmp(hostname, node) == 0) {
		args[0] = "scrcmd";
		args[1] = "-S";
		args[2] = "snmp";
		args[3] = new char [BUFSIZ];
		bzero(args[3], BUFSIZ);
		args[4] = NULL;
		while (argv[count]) {
			(void) sprintf(args[3], "%s%s", args[3], argv[count++]);
			(void) sprintf(args[3], "%s ", args[3]);
		}
		err = execute_scrcmd_command(args);
		delete [] args[3];
	} else {
		args[0] = "scrcmd";
		args[1] = "-N";
		args[2] = node;
		args[3] = "snmp";
		args[4] = new char [BUFSIZ];
		bzero(args[4], BUFSIZ);
		args[5] = NULL;
		while (argv[count]) {
			(void) sprintf(args[4], "%s%s", args[4], argv[count++]);
			(void) sprintf(args[4], "%s ", args[4]);
		}
		err = execute_scrcmd_command(args);
		delete [] args[4];
	}

	return (err);
}

//
// This function will open a pipe to the scrcmd command on the node
// specified calling the 'snmp' command and passing the arguments listed
// in args and capture the stdout of scrcmd in a file pointer.
//
// Return values:
//
//	CL_NOERR
//	CL_EIO
//	CL_EINTERNAL
//
clerrno_t
run_scrcmd_for_list_on_node(char *args, char *node, FILE **fp)
{
	char hostname[MAXHOSTNAMELEN];

	if (!args || !node) {
		return (CL_EINTERNAL);
	}

	(void) gethostname(hostname, sizeof (hostname));

	// For storing the scrcmd command to be executed on the node.
	char the_cmd[BUFSIZ];
	the_cmd[0] = '\0';

	// Forming the scrcmd command.
	if (strcmp(hostname, node) == 0) {
		(void) sprintf(the_cmd, "%s -S snmp %s", (char *)SCRCMD, args);
	} else {
		(void) sprintf(the_cmd, "%s -N %s snmp %s", (char *)SCRCMD,
		    node, args);
	}

	// Opening the pipe to the scrcmd command.
	*fp = popen(the_cmd, "r");
	if (*fp == NULL) {
		clerror("Cannot open pipe - %s\n", strerror(errno));
		switch (errno) {

		case EMFILE:
			return (CL_EIO);
		default:
			return (CL_EINTERNAL);
		}
	}

	// If we are here, we have the output from scrcmd in the file pointer
	return (CL_NOERR);

}

//
// Executes scrcmd with the given args.
//
// Return values:
//
//	CL_NOERR
//	CL_EINTERNAL
//	CL_ENOENT
//	CL_EEXIST
//
clerrno_t
execute_scrcmd_command(char **args)
{
	int stat = 0;

	// lint -e(578)
	pid_t pid = fork();
	if (pid == 0) {
		if (execv((char *)SCRCMD, args) == -1) {
			clerror("Cannot execute scrcmd - %s.\n",
			    strerror(errno));
			exit(1);
		}
		exit(0);
	} else {
		int status = 0;
		while ((status = waitpid(pid, &stat, 0)) != pid) {
			if (status == -1 && errno == EINTR)
				continue;
			else {
				status = -1;
				break;
			}
		}
		if (status != pid) {
			clerror("Error waiting for command result - %s\n",
			    strerror(errno));
			return (CL_EINTERNAL);
		}
	}

	// lint -e(702)
	return (conv_err_from_sceventmib(WEXITSTATUS(stat)));
}

//
// Converts errors returned from sceventmib to CL errors
//
clerrno_t
conv_err_from_sceventmib(int error)
{
	switch (error) {

	case 0:
		return (CL_NOERR);
	case 2:
		return (CL_EEXIST);
	case 3:
		return (CL_ENOENT);
	case 4:
		return (CL_EINVAL);

	case SCRCMD_ENOMEM:
		return (CL_ENOMEM);

	case SCRCMD_EPROTONOSUPPORT:
	case SCRCMD_ETIMEDOUT:
	case SCRCMD_ECONNREFUSED:
	case SCRCMD_ECONNABORTED:
	case SCRCMD_ENOEXEC:
		return (CL_EIO);

	default:
		return (CL_EINTERNAL);
	}
}

//
// This function will execute the scrcmd command on the node specified
// and call the 'snmp' scrcmd command passing the args along.  It will
// open a pipe for the output of that command and set assign it to the
// FILE * which was passed in.
//
// Return values:
//
//	CL_NOERR
//	CL_EINTERNAL
//	CL_ENOMEM
//	CL_EIO
//
clerrno_t
pipe_sceventmib_cmd_on_node(FILE **file, char *args, char *node)
{
	char hostname[MAXHOSTNAMELEN];
	*file = NULL;
	if ((!args) || (!node))
		return (CL_EINTERNAL);

	char the_cmd[BUFSIZ];

	(void) gethostname(hostname, sizeof (hostname));

	// Build the command string
	if (strcmp(hostname, node) == 0) {
		(void) sprintf(the_cmd, "%s -S snmp %s", SCRCMD, args);
	} else {
		(void) sprintf(the_cmd, "%s -N %s snmp %s", SCRCMD, node, args);
	}

	*file = popen(the_cmd, "r");
	if (file == NULL) {
		clerror("Cannot open pipe - %s\n", strerror(errno));
		switch (errno) {

		case EMFILE:
			return (CL_EIO);
		default:
			return (CL_EINTERNAL);
		}
	} else {
		return (CL_NOERR);
	}
}

//
// Goes through the operands ValueList, eliminating those which are not
// known MIBs.  Each valid operand is added to the new_operands ValueList.
//
// Return values:
//
//	CL_NOERR
//	CL_ENOENT
//
clerrno_t
get_mib_operands(ValueList &operands, ValueList &new_operands)
{
	clerrno_t err = CL_NOERR;

	// Check for wildcard
	if (operands.getSize() == 0) {
		int count = 0;
		while (CLUSTER_MIBS[count]) {
			new_operands.add(CLUSTER_MIBS[count++]);
		}
	} else {
		// Go through list of operands removing
		// those which are not known MIBs
		for (operands.start();
		    !operands.end();
		    operands.next()) {

			bool found = false;
			int count = 0;
			while (CLUSTER_MIBS[count]) {
				if (strcasecmp(operands.getValue(),
				    CLUSTER_MIBS[count++]) == 0) {

					found = true;
				}
			}

			// If not found, print error and remove from list
			if (!found) {
				clerror("Cluster MIB \"%s\" does not exist.\n",
				    operands.getValue());
				set_error(err, CL_ENOENT);
			} else {
				new_operands.add(operands.getValue());
			}
		}
	}

	return (err);
}

//
// Replaces the value of error if it has not already been set
//
void
set_error(clerrno_t &error, clerrno_t value)
{
	if (error == CL_NOERR)
		error = value;
}

//
// Populates the nodes ValueList with the list of cluster
// nodes on the system.
//
// Return values:
//
//	CL_NOERR
//	CL_EINTERNAL
//
clerrno_t
get_all_cluster_nodes(ValueList &nodes)
{
	// Initialize the orb
	if (clconf_lib_init() != 0) {
		clerror("Error while initializing ORB\n");
		return (CL_EINTERNAL);
	}
	clconf_cluster_t *cl = clconf_cluster_get_current();
	clconf_iter_t *iter = clconf_cluster_get_nodes(cl);
	clconf_obj_t *obj;

	nodes.clear();
	while ((obj = clconf_iter_get_current(iter)) != NULL) {
		nodes.add((char *)clconf_obj_get_name(obj));
		clconf_iter_advance(iter);
	}
	clconf_iter_release(iter);

	return (CL_NOERR);
}

//
// Validates the list of nodes in the ValueList 'list' against
// a list of all the known cluster nodes.  The 'list' ValueList
// is returned with only the valid cluster nodes which it originally
// contained.
//
// Return values:
//
//	CL_NOERR
//	CL_EINVAL
//	CL_EINTERNAL
//
clerrno_t
verify_nodelist(ValueList &list)
{
	ValueList nodes;
	clerrno_t err = get_all_cluster_nodes(nodes);

	ValueList real_list;
	for (list.start();
	    !list.end();
	    list.next()) {
		if (strlen(list.getValue()) == 0) {
			clerror("Invalid node specified.\n");
			err = CL_EINVAL;
			continue;
		}

		if (!nodes.isValue(list.getValue())) {
			clerror("\"%s\" is not a valid cluster node.\n",
			    list.getValue());
			err = CL_EINVAL;
		} else {
			real_list.add(list.getValue());
		}
	}

	// Replace the existing list with the list of real nodes
	list.clear();
	for (real_list.start();
	    !real_list.end();
	    real_list.next()) {
		list.add(real_list.getValue());
	}

	return (err);
}

//
// Adds the name of the local node to the nodes ValueList.
//
// Return values:
//
//	CL_NOERR
//	CL_EINTERNAL
//
clerrno_t
add_this_node(ValueList &nodes)
{
	char hostname[256];
	clerrno_t err = gethostname(hostname, 256);
	nodes.add(hostname);

	if (err == 0)
		return (CL_NOERR);
	else
		return (CL_EINTERNAL);
}

//
// Strips off the headings and section title from the verbose list output
//
clerrno_t
strip_list_headings(FILE *fp)
{
	if (!fp)
		return (CL_EINTERNAL);

	char c;
	int count = 0;
	while ((c = (char)fgetc(fp)) != EOF) {
		// The data from the command is of the following form:
		// \n
		// --- SNMP ...--- \n
		// \n
		// <heading>...\n
		// ---------...\n
		// <data>\n
		//
		// After the fifth newline (\n), we want to break
		if (c == '\n')
			count++;

		if (count == 5)
			break;
		else
			continue;
	}

	if (count != 5)
		return (CL_EINTERNAL);
	else
		return (CL_NOERR);
}

//
// Validates the list of nodes in the ValueList 'list' checking
// to see if individual nodes are active members of the cluster.
// The original list ValueList is returned with only the nodes
// which are in cluster mode.
//
// Return values:
//
//	CL_NOERR
//	CL_ENOTCLMODE
//
clerrno_t
verify_nodes_clustmode(ValueList &list, int &outofcluster)
{
	nodeid_t nodeid;
	clerrno_t err = CL_NOERR;
	uint_t ismember;
	ValueList real_list;
	outofcluster = 0;

	for (list.start();
	    !list.end();
	    list.next()) {

		if (scconf_get_nodeid(list.getValue(), &nodeid)
		    == SCCONF_NOERR) {

			(void) scconf_ismember(nodeid, &ismember);

			if (!ismember) {
				clerror("Node \"%s\" is not an active cluster "
				    "member.\n", list.getValue());
				set_error(err, CL_ENOTCLMODE);
				outofcluster = 1;
			} else {
				real_list.add(list.getValue());
			}
		} else {
			set_error(err, CL_EINTERNAL);
		}
	}

	// Replace the existing list with the list of nodes in cluster.
	list.clear();
	for (real_list.start();
	    !real_list.end();
	    real_list.next()) {
		list.add(real_list.getValue());
	}

	return (err);
}
