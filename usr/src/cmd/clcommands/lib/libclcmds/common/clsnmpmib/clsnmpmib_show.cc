//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clsnmpmib_show.cc	1.4	08/09/22 SMI"

//
// Process clsnmpmib "show"
//

#include "clsnmpmib.h"

#include "ClCommand.h"

//
// Prints the MIB configuration information using the ClShow
// class to format and print the data.
//
// Return values:
//
//	CL_NOERR
//	CL_EINTERNAL
//	CL_ENOENT
//
clerrno_t
clsnmpmib_show(
    ValueList &operands,
    ValueList &nodes)
{
	ValueList new_operands;
	int outofcluster = 0;

	clerrno_t err = get_mib_operands(operands, new_operands);

	// Verify the nodelist
	if (nodes.getSize() == 0) {
		set_error(err, add_this_node(nodes));
	}
	set_error(err, verify_nodelist(nodes));

	// Verify if the nodes in the nodelist are active cluster members.
	set_error(err, verify_nodes_clustmode(nodes, outofcluster));

	if (outofcluster)
		clerror("SNMP MIB information cannot be retrieved from "
		    "offline nodes.\n");

	// Do it on each node
	for (nodes.start();
	    !nodes.end();
	    nodes.next()) {

		clerrno_t error = CL_NOERR;
		ClShow *show_obj = get_filtered_clsnmpmib_show_obj(
		    new_operands, nodes.getValue(), error);

		show_obj->print();

		set_error(err, error);
	}

	return (err);
}

//
// Returns a ClShow object containing the SNMP mib data from a node.  The
// value error will be set if any error was encountered.
//
ClShow *
get_filtered_clsnmpmib_show_obj(ValueList &operands, char *node,
    clerrno_t &error)
{
	ClShow *show_obj = new ClShow(HEAD_SNMPMIB, node);

	// Check for empty operand list
	if (operands.getSize() == 0)
		return (show_obj);

	// Call the sceventmib shell script to print
	char args[BUFSIZ];
	args[0] = '\0';
	(void) sprintf(args, "-x -X -p mib-list");

	// Get a FILE * with the stdout from the cmd
	FILE *file = NULL;
	set_error(error, pipe_sceventmib_cmd_on_node(
	    &file, args, node));

	// Parse the data from stdout and add it to the
	// ClShow object
	set_error(error, parse_stdout_to_snmp_mib_show(show_obj, file));

	// Close the pipe
	(void) pclose(file);

	return (show_obj);
}

//
// Returns a ClShow object containing the SNMP mib data from one node.
// No filtering of the data is done.
//
ClShow *
get_clsnmpmib_show_obj(char *node)
{
	ValueList operands;
	ValueList empty;
	clerrno_t error;
	(void) get_mib_operands(empty, operands);
	return (get_filtered_clsnmpmib_show_obj(operands, node, error));
}

//
// Parses the data from the FILE * into the ClShow object.  If any error
// was encountered during processing or via the EXIT_MESSAGE, then it will
// be returned.
//
clerrno_t
parse_stdout_to_snmp_mib_show(ClShow *&show_obj, FILE *file)
{
	clerrno_t err = strip_list_headings(file);

	char name[128];
	name[0] = '\0';
	char state[128];
	state[0] = '\0';
	char temp[128];
	temp[0] = '\0';

	while (fscanf(file, "%s", temp) != EOF) {

		// Get the exit status from the piped cmd
		if (strcmp(temp, EXIT_MESSAGE) == 0) {
			fscanf(file, "%d", &err);
			return (conv_err_from_sceventmib(err));

		// Get the name
		} else if (strcmp(name, "") == 0) {
			(void) sprintf(name, temp);
			show_obj->addObject(
			    (char *)gettext("SNMP MIB Name"),
			    name);

		// Get the state
		} else if (strcmp(state, "") == 0) {
			(void) sprintf(state, temp);
			show_obj->addProperty(name,
			    (char *)gettext("State"),
			    state);

		// Get the protocol
		} else {
			show_obj->addProperty(name,
			    (char *)gettext("Protocol"),
			    temp);

			state[0] = '\0';
		}

		temp[0] = '\0';
	}

	return (CL_EINTERNAL);
}
