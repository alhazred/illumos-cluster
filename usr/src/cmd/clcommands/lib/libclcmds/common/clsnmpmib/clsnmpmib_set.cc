//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clsnmpmib_set.cc	1.4	08/10/13 SMI"

//
// Process clsnmpmib "set"
//

#include "clsnmpmib.h"

#include "ClCommand.h"

//
// Modify one or more MIB's configuration on one or more nodes.
//
// Return values:
//
//	CL_NOERR
//	CL_EINTERNAL
//	CL_ENOENT
//
clerrno_t
clsnmpmib_set(
    ClCommand &cmd,
    ValueList &operands,
    ValueList &nodes,
    NameValueList &props)
{
	clerrno_t err = CL_NOERR;
	char *propval;
	ValueList new_operands;
	char *argv[5];
	int outofcluster = 0;

	// There is only one valid property - make sure this is it...
	for (props.start(); !props.end(); props.next()) {
		if (strcasecmp(props.getName(), "version") == 0) {
			propval = props.getValue();
		} else {
			clerror("Invalid property name - \"%s\".\n",
			    props.getName());
			err = CL_EPROP;
		}
	}

	if (err)
		return (err);
	else if (!propval)
		return (CL_EINTERNAL);

	// Verify the property value
	if ((strcasecmp(propval, "2") != 0) &&
	    (strcasecmp(propval, "3") != 0) &&
	    (strcasecmp(propval, "snmpv2") != 0) &&
	    (strcasecmp(propval, "snmpv3") != 0)) {

		clerror("Invalid property value - \"%s\".\n", propval);
		return (CL_EINVAL);
	}

	err = get_mib_operands(operands, new_operands);

	// Verify the nodelist
	if (nodes.getSize() == 0) {
		set_error(err, add_this_node(nodes));
	}
	set_error(err, verify_nodelist(nodes));

	// Verify if the nodes in the nodelist are active cluster members.
	set_error(err, verify_nodes_clustmode(nodes, outofcluster));

	if (outofcluster)
		clerror("SNMP MIB properties cannot be changed on offline "
		    "nodes.\n");

	// Call the sceventmib shell script to change the protocol on each MIB
	argv[0] = "sceventmib"; // The command
	argv[1] = "-x";		// CLCMD flag
	argv[2] = "-l";		// The protocol flag
	argv[3] = propval;	// The protocol value
	argv[4] = NULL;

	for (new_operands.start();
	    !new_operands.end();
	    new_operands.next()) {

		for (nodes.start();
		    !nodes.end();
		    nodes.next()) {

			clerrno_t status = run_sceventmib_cmd_on_node(
			    argv, strdup(nodes.getValue()));

			set_error(err, status);
			if (!status && cmd.isVerbose) {
				clmessage("Modified \"%s\" "
				    "MIB on node \"%s\".\n",
				    new_operands.getValue(),
				    nodes.getValue());
			}
		}
	}

exit:

	return (err);
}
