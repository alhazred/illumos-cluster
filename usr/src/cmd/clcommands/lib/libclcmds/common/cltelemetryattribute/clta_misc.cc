//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clta_misc.cc	1.6	08/05/20 SMI"

#include "cltelemetryattribute.h"
#include "scslm_cacao.h"
#include "clnode.h"
#include "ClXml.h"

static const char *clta_check_objtype(char *obj_type,
    sensor_t *ccr_telattr_list);
static clerrno_t clta_add_ki_list(ki_t **valid_clta_list, ki_t *ki_ptr,
    const char *ki_name, const char *ki_type);
static scslm_error_t clta_save_threshold_list(const char *thresh_key,
    char *thresh_value, void *ccr_thresholds);
static scslm_error_t clta_threshold_set_field(threshold_t *ccr_threshold,
    uint_t ccr_field, const char *thresh_value);

//
// clta_preprocess_talist
//
//	Read the SCSLMADM_TABLE, and create the valid_clta_list. Caller will
//	need to free it.
//
clerrno_t
clta_preprocess_talist(ValueList &clta_attributes,
    ValueList &clta_types, ki_t **valid_clta_list, bool print_err)
{
	clerrno_t clerrno = CL_NOERR;
	sensor_t *ccr_telattr_list = NULL;
	scslm_error_t scslm_errno = SCSLM_OK;
	char *this_val;
	ValueList valid_type = ValueList(true);
	ValueList valid_ta = ValueList(true);
	sensor_t *s_ptr = NULL;
	ki_t *ki_ptr = NULL;
	const char *short_ki;
	const char *short_motype;
	clerrno_t tmp_err = CL_NOERR;
	const char *short_form;

	// Read all the attributes from ccr
	scslm_errno = scslm_read_table_ccr(SCSLMADM_TABLE,
	    store_in_sensor_list, (void *)&ccr_telattr_list, 0);
	if (scslm_errno != SCSLM_OK) {
		if (scslm_errno != SCSLM_ENOENT) {
			clcommand_perror(CL_EINTERNAL);
			clerrno = CL_EINTERNAL;
			goto cleanup;
		} else {
			// SLM not configured yet.
			if (print_err) {
				clerror("Service Level Manageability is not "
				    "configured.\n");
			}
			clerrno = CL_EOP;
			goto cleanup;
		}
	}

	// Check the user input object types
	for (clta_types.start(); !clta_types.end(); clta_types.next()) {
		this_val = clta_types.getValue();

		// Is the type valid?
		short_form = clta_check_objtype(this_val, ccr_telattr_list);
		if (!short_form) {
			clerror("Unknown object type \"%s\".\n",
			    this_val);
			if (clerrno == CL_NOERR) {
				clerrno = CL_ETYPE;
			}
		} else {
			valid_type.add((char *)short_form);
		}
	}

	// Go through the ccr_telattr_list
	for (s_ptr = ccr_telattr_list; s_ptr; s_ptr = s_ptr->next) {
		if (!s_ptr->ki_list) {
			continue;
		}

		// Go through the ki list
		for (ki_ptr = s_ptr->ki_list; ki_ptr; ki_ptr = ki_ptr->next) {
			short_motype = make_short_mot(ki_ptr->motype);
			short_ki = make_short_ki(ki_ptr->name);

			if (!short_motype || !short_ki) {
				continue;
			}

			// Check the ki name and type
			if ((clta_types.getSize() == 0) ||
			    valid_type.isValue((char *)short_motype)) {
				if ((clta_attributes.getSize() == 0) ||
				    clta_attributes.isValue((char *)short_ki) ||
				    clta_attributes.isValue(ki_ptr->name)) {
					// Add into valid_clta_list
					tmp_err = clta_add_ki_list(
					    valid_clta_list,
					    ki_ptr, short_ki, short_motype);
					if (tmp_err != CL_NOERR) {
						if (clerrno == CL_NOERR) {
							clerrno = tmp_err;
						}
						goto cleanup;
					}
				}
			}

			// Attribute valid?
			if (clta_attributes.isValue((char *)short_ki) ||
			    clta_attributes.isValue(ki_ptr->name)) {
				valid_ta.add((char *)short_ki);
			}
		}
	}

	// Check agains the clta_attributes list
	for (clta_attributes.start(); !clta_attributes.end();
	    clta_attributes.next()) {
		this_val = clta_attributes.getValue();

		// Convert to short form if it is long form
		if (strstr(this_val, "ki.sunw.")) {
			short_ki = make_short_ki(this_val);
		} else {
			short_ki = this_val;
		}

		// Is it in the valid ki list valid_clta_list?
		if (!clta_isvalue_in_kilist(*valid_clta_list, short_ki, NULL)) {
			if (!valid_ta.isValue((char *)short_ki)) {
				clerror("Invalid telemetry attribute "
				    "\"%s\" .\n", this_val);
				if (clerrno == CL_NOERR) {
					clerrno = CL_ENOENT;
				}
			} else {
				clerror("Attribute \"%s\" does not "
				    "apply to the specified type.\n",
				    this_val);
				if (clerrno == CL_NOERR) {
					clerrno = CL_ETYPE;
				}
			}
		}
	}

cleanup:
	// Free the atribute list
	if (ccr_telattr_list) {
		free_sensor_list(&ccr_telattr_list);
	}

	return (clerrno);
}

//
// clta_do_set_talist
//
//	Modify the attrute list by the operation flag, which is either
//	DO_ENABLE or DO_DISABLE.
//
clerrno_t clta_do_set_talist(ClCommand &cmd, ValueList &clta_attributes,
    ValueList &clta_types, optflgs_t optflgs, uint_t set_flg)
{
	clerrno_t clerrno = CL_NOERR;
	ki_t *valid_clta_list = NULL;
	ki_t *ki_ptr;
	ValueList clta_motype = ValueList(true);
	scslm_error_t scslm_errno = SCSLM_OK;
	sensor_t *ccr_telattr_list = NULL;
	ki_t *ki_in_sensorlist;
	const char *long_motype;
	const char *long_ki;

	// Get the valid telemetry attributes
	clerrno = clta_preprocess_talist(clta_attributes, clta_types,
	    &valid_clta_list, true);

	// No valid attributes are found
	if (!valid_clta_list) {
		return (clerrno);
	}

	// Read the ccr table for update
	scslm_errno = scslm_read_table_ccr(SCSLMADM_TABLE,
	    store_in_sensor_list, (void *)&ccr_telattr_list, 1);
	if (scslm_errno != SCSLM_OK) {
		clcommand_perror(CL_EINTERNAL);
		if (clerrno == CL_NOERR) {
			clerrno = CL_EINTERNAL;
		}
		goto cleanup;
	}

	// Go through the ki list to set the enabled state
	for (ki_ptr = valid_clta_list; ki_ptr; ki_ptr = ki_ptr->next) {
		long_motype = make_long_mot(ki_ptr->motype);
		long_ki = make_long_ki(ki_ptr->name);
		if (!long_motype || !long_ki) {
			clcommand_perror(CL_EINTERNAL);
			if (clerrno == CL_NOERR) {
				clerrno = CL_EINTERNAL;
			}
			continue;
		}

		// Get this KI from the sensor list
		ki_in_sensorlist = fetch_ki_from_sensor_list(ccr_telattr_list,
		    long_ki, long_motype);
		if (!ki_in_sensorlist) {
			clcommand_perror(CL_EINTERNAL);
			if (clerrno == CL_NOERR) {
				clerrno = CL_EINTERNAL;
			}
			continue;
		}

		// Set the enabled state
		scslm_errno = SCSLM_OK;
		if (set_flg == DO_ENABLE) {
			if (strcmp(ki_in_sensorlist->enabled,
			    SCSLMADM_TRUE) != 0) {
				scslm_errno = set_ki_property(ki_in_sensorlist,
				    KEY_KI_ENABLED, SCSLMADM_TRUE);
			}
		} else if (set_flg == DO_DISABLE) {
			if (strcmp(ki_in_sensorlist->enabled,
			    SCSLMADM_FALSE) != 0) {
				scslm_errno = set_ki_property(ki_in_sensorlist,
				    KEY_KI_ENABLED, SCSLMADM_FALSE);
			}
		}
		if (scslm_errno != SCSLM_OK) {
			switch (scslm_errno) {
			case SCSLM_ENOMEM:
				clcommand_perror(CL_ENOMEM);
				if (clerrno == CL_NOERR) {
					clerrno = CL_ENOMEM;
				}
				break;
			default:
				clcommand_perror(CL_EINTERNAL);
				if (clerrno == CL_NOERR) {
					clerrno = CL_EINTERNAL;
				}
				break;
			}
			goto cleanup;
		}
	}

	// Update the ccr table
	scslm_errno = scslmadm_update_table_ccr(SCSLMADM_TABLE,
	    ccr_telattr_list);
	if (scslm_errno != SCSLM_OK) {
		switch (scslm_errno) {
		case SCSLM_ENOMEM:
			clcommand_perror(CL_ENOMEM);
			if (clerrno == CL_NOERR) {
				clerrno = CL_ENOMEM;
			}
			break;
		case SCSLM_EBUSY:
			clerror("Table \"%s\" has been updated "
			    "concurently.\n", SCSLMADM_TABLE);
			if (clerrno == CL_NOERR) {
				clerrno = CL_EBUSY;
			}
			break;
		default:
			clcommand_perror(CL_EINTERNAL);
			if (clerrno == CL_NOERR) {
				clerrno = CL_EINTERNAL;
			}
			break;
		}
	} else {
		// post an event
		(void) sc_publish_event(ESC_CLUSTER_SLM_CONFIG_CHANGE,
		    CL_EVENT_PUB_SLM,
		    CL_EVENT_SEV_INFO,
		    CL_EVENT_INIT_SYSTEM, 0, 0, 0,
		    CL_CONFIG_ACTION,
		    SE_DATA_TYPE_UINT32, CL_EVENT_CONFIG_PROP_CHANGED,
		    CL_CCR_TABLE_NAME, SE_DATA_TYPE_STRING, SCSLMADM_TABLE,
		    NULL);
	}

cleanup:
	// Free the telemetry attribute list
	free_sensor_list(&ccr_telattr_list);
	clta_free_ta_list(valid_clta_list);

	return (clerrno);
}

//
// clta_do_set_talist_xml
//
//	Modify the attrute list by the operation flag, which is either
//	DO_ENABLE or DO_DISABLE, using xml configuration file.
//
clerrno_t clta_do_set_talist_xml(ClCommand &cmd, ValueList &clta_attributes,
    ValueList &clta_types, char *clconfiguration, optflgs_t optflgs,
    uint_t set_flg)
{
	clerrno_t clerrno = CL_NOERR;
	clerrno_t first_err = CL_NOERR;
	ValueList xml_objects;

	// Check for inputs
	if (!clconfiguration) {
		clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}

	OptionValues *ov;
	ClXml xml;

	if (set_flg == DO_ENABLE) {
		ov = new OptionValues((char *)"enable", cmd.argc, cmd.argv);
	} else {
		ov = new OptionValues((char *)"disable", cmd.argc, cmd.argv);
	}

	// Add quorum types to OptionValues
	ov->addOptions(CLTA_TYPE, &clta_types);

	// verbose?
	if (optflgs & vflg) {
		ov->optflgs |= vflg;
	}

	// Get all the objects in the xml file
	clerrno = xml.getAllObjectNames(CLTELEMETRYATTRIBUTE_CMD,
	    clconfiguration, &xml_objects);
	if (clerrno != CL_NOERR) {
		delete ov;
		return (clerrno);
	}

	if (xml_objects.getSize() == 0) {
		clerror("Cannot find any telemetry attributes in \"%s\".\n",
		    clconfiguration);
		delete ov;
		return (CL_EINVAL);
	}

	if (clta_attributes.getSize() == 0) {
		clta_attributes.add(CLCOMMANDS_WILD_OPERAND);
	}

	// Set the attributes
	for (clta_attributes.start(); !clta_attributes.end();
	    clta_attributes.next()) {
		char *attribute = clta_attributes.getValue();
		if (!attribute)
			continue;

		// Is this attribute found in xml file?
		if (strcmp(attribute, CLCOMMANDS_WILD_OPERAND) != 0) {
			int found = 0;
			for (xml_objects.start(); !xml_objects.end();
			    xml_objects.next()) {
				char *obj_name = xml_objects.getValue();
				if (obj_name &&
				    (strcmp(obj_name, attribute) == 0)) {
					found++;
					break;
				}
			}

			if (!found) {
				clerror("Cannot find telemetry attribute "
				    "\"%s\" in file \"%s\".\n",
				    attribute, clconfiguration);
				if (first_err == CL_NOERR) {
					first_err = CL_ENOENT;
				}
				continue;
			}
		}

		ov->setOperand(attribute);
		clerrno = xml.applyConfig(CLTELEMETRYATTRIBUTE_CMD,
		    clconfiguration, ov);
		if ((clerrno != CL_NOERR) && (first_err == CL_NOERR)) {
			first_err = clerrno;
		}
	}

	return (first_err);
}

//
// clta_preprocess_thresh
//
//	Get the valid threshold list that match the given obj_instance,
//	nodes, and the ki_t list. Caller needs to free the memory allocated
//	to valid_thresholds.
//
clerrno_t
clta_preprocess_thresh(ValueList &obj_instances, ValueList &nodes,
    ki_t *valid_clta_list, threshold_list_t **valid_thresholds)
{
	clerrno_t clerrno = CL_NOERR;
	clerrno_t first_err = CL_NOERR;
	scslm_error_t scslm_errno = SCSLM_OK;
	threshold_list_t *ccr_thresholds = NULL;
	threshold_t *p_thresh;
	char *this_val;
	threshold_list_t *th_ptr;
	int found = 0;
	ValueList valid_instances = ValueList(true);
	NameValueList valid_nodes = NameValueList(true);
	NameValueList cluster_nodes = NameValueList(true);
	ki_t *ki_ptr;
	ValueList node_type;

	// Check inputs
	if (!valid_clta_list) {
		clerrno = CL_EINTERNAL;
		clcommand_perror(clerrno);
		return (clerrno);
	}

	// Get all cluster nodes
	node_type.add(NODETYPE_SERVER);

	clerrno = clnode_preprocess_nodelist(nodes, node_type,
	    valid_nodes, true);
	if (clerrno != CL_NOERR) {
		if (first_err == CL_NOERR) {
			first_err = clerrno;
		}
	}

	// No valid nodes
	if (nodes.getSize() != 0 && valid_nodes.getSize() == 0) {
		goto cleanup;
	}

	// Read the CCR thresholds
	scslm_errno = scslm_read_table_ccr(SCSLMTHRESH_TABLE,
	    clta_save_threshold_list, (void *)&ccr_thresholds, 0);
	if (scslm_errno != SCSLM_OK) {
		if (scslm_errno != SCSLM_ENOENT) {
			clerrno = CL_EINTERNAL;
			clcommand_perror(clerrno);
		}
		return (clerrno);
	}

	// Check the user input instance
	for (obj_instances.start(); !obj_instances.end();
	    obj_instances.next()) {
		char *node_name = (char *)0;
		char *zone_name = (char *)0;
		this_val = obj_instances.getValue();

		// If it is a node:zone instance
		if (strchr(this_val, ':')) {
			node_name = new char[strlen(this_val) + 1];
			(void) strcpy(node_name, this_val);

			// Get the zone name and node name
			zone_name = strchr(node_name, ':');
			*zone_name++ = '\0';
			if ((strlen(zone_name) == 0) ||
			    (strlen(node_name) == 0)) {
				clerror("Invalid instance \"%s\".\n",
				    this_val);
				if (first_err == CL_NOERR) {
					first_err = CL_EINVAL;
				}
				delete [] node_name;
				continue;
			}
		}

		// Is this instance in the ccr_thresholds list?
		found = 0;
		for (th_ptr = ccr_thresholds; th_ptr; th_ptr = th_ptr->next) {
			p_thresh = th_ptr->threshold;
			if (strchr(this_val, ':')) {
				if (!(*p_thresh)[THRESH_MONAME] ||
				    (strcmp((*p_thresh)[THRESH_MONAME],
				    zone_name) != 0)) {
					continue;
				}
			} else if (!(*p_thresh)[THRESH_MONAME] ||
			    (strcmp((*p_thresh)[THRESH_MONAME],
			    this_val) != 0)) {
				continue;
			}
			found = 1;

			// Is this in the valid TA list?
			for (ki_ptr = valid_clta_list; ki_ptr;
			    ki_ptr = ki_ptr->next) {
				if (!ki_ptr->name || !ki_ptr->motype)
					continue;

				if ((strcmp(ki_ptr->name,
				    (*p_thresh)[THRESH_KINAME]) == 0) &&
				    (strcmp(ki_ptr->motype,
				    (*p_thresh)[THRESH_MOTYPE]) == 0)) {
					if (strchr(this_val, ':')) {
						if (strcmp(node_name,
						(*p_thresh)[THRESH_NODENAME])
						    == 0) {
							found = 2;
							break;
						}
					} else {
						found = 2;
						break;
					}
				}
			}

			if (found == 2) {
				break;
			}
		}

		if (!found) {
			clerror("Unknown object instance \"%s\".\n",
				    this_val);
			if (first_err == CL_NOERR) {
				first_err = CL_ENOENT;
			}
		} else {
			if (strchr(this_val, ':')) {
				valid_instances.add(zone_name);
				valid_nodes.add(node_name, NODETYPE_SERVER);
				delete [] node_name;
			} else {
				valid_instances.add(this_val);
			}
		}
	}

	// Go through the threshold list
	for (th_ptr = ccr_thresholds; th_ptr; th_ptr = th_ptr->next) {
		p_thresh = th_ptr->threshold;

		// Is the instance specified?
		if ((obj_instances.getSize() == 0 ||
		    valid_instances.isValue((*p_thresh)[THRESH_MONAME])) &&
		    (((strcmp((*p_thresh)[THRESH_MOTYPE], "node") != 0) &&
		    (nodes.getSize() == 0 ||
		    strcmp((*p_thresh)[THRESH_NODENAME], EMPTY_VALUE) == 0 ||
		    clta_is_node(valid_nodes, (*p_thresh)[THRESH_NODENAME]))) ||
		    ((strcmp((*p_thresh)[THRESH_MOTYPE], "node") == 0) &&
		    (nodes.getSize() == 0 ||
		    clta_is_node(valid_nodes, (*p_thresh)[THRESH_MONAME]))))) {
			// Is it in the valid TA list?
			found = 0;
			for (ki_ptr = valid_clta_list; ki_ptr;
			    ki_ptr = ki_ptr->next) {
				if (!ki_ptr->name || !ki_ptr->motype)
					continue;

				if ((strcmp(ki_ptr->name,
				    (*p_thresh)[THRESH_KINAME]) == 0) &&
				    (strcmp(ki_ptr->motype,
				    (*p_thresh)[THRESH_MOTYPE]) == 0)) {
					found = 1;
					break;
				}
			}

			if (found) {
				// Add into the valid threshold list
				scslm_error_t scslm_errno =
				    threshold_list_insert(valid_thresholds,
				    th_ptr->id_in_ccr, p_thresh);
				if (scslm_errno != SCSLM_OK) {
					if (scslm_errno == SCSLM_ENOMEM) {
						clcommand_perror(CL_ENOMEM);
						if (first_err == CL_NOERR) {
							first_err = CL_ENOMEM;
						}
					} else {
						clcommand_perror(CL_EINTERNAL);
						if (first_err == CL_NOERR) {
							first_err =
							    CL_EINTERNAL;
						}
					}
				}
			}
		}
	}

cleanup:
	return (first_err);
}

//
// clta_get_cacao_cmd
//
//	Get the cacao command for the given cmd_name.
//
char *
clta_get_cacao_cmd(uint_t cmd_type)
{
	char command[COMMAND_BUFFER_SIZE] = "";
	char *cmd_ptr = command;
	int port = -1;
	char cacaocsc_port[128];

	// Check inputs
	if ((cmd_type != CMD_PRINT) && (cmd_type != CMD_STATUS)) {
		goto cleanup;
	}

	// Get cacao path
	cmd_ptr = strcat(cmd_ptr, get_cacaocsc_path());
	if (strlen(cmd_ptr) == 0) {
		goto cleanup;
	}

	// Get cacao port
	if (scslmadm_get_command_stream_adapter_port(&port) != SCSLM_NOERR) {
		goto cleanup;
	}

	(void) sprintf(cacaocsc_port, " -p %d", port);
	cmd_ptr = strcat(cmd_ptr, cacaocsc_port);
	if (cmd_type == CMD_PRINT) {
		cmd_ptr = strcat(cmd_ptr, CACAOCSC_SCSLMADM_CMD);
	} else {
		cmd_ptr = strcat(cmd_ptr, CACAOCSC_SCSLMTHRESH_CMD);
	}

	return (cmd_ptr);
cleanup:
	clcommand_perror(CL_EINTERNAL);
	return (NULL);
}

//
// clta_get_ki_from_ki_list
//
ki_t *
clta_get_ki_from_ki_list(ki_t *ki_list, const char *ki_name,
    const char *mo_type)
{
	ki_t *ki_ptr;

	if (!ki_list || !ki_name || !mo_type) {
		return (NULL);
	}

	for (ki_ptr = ki_list; ki_ptr; ki_ptr = ki_ptr->next) {
		if ((strcmp(ki_ptr->name, ki_name) == 0) &&
		    (strcmp(ki_ptr->motype, mo_type) == 0)) {
			return (ki_ptr);
		}
	}

	return (NULL);
}

//
// clta_convert_valuelist_str
//
//	For the given ValueList, convert the items in the list to a string,
//	which starts with the *starter, and each item is separated by the
//	*separator. Caller needs to free the allocaed memory
//
char *clta_convert_valuelist_str(ValueList &value_list, char *starter,
    char *separator, uint_t str_flg)
{
	char *convert_str = (char *)0;
	int length = 0;
	int sep_length = 0;
	const char *long_form;
	int count = 0;
	int i = 0;

	if (starter) {
		length += strlen(starter);
	}
	if (separator) {
		sep_length = strlen(separator);
	}

	// Count the length
	for (value_list.start(); !value_list.end(); value_list.next()) {
		char *this_val = value_list.getValue();
		if (!this_val) {
			continue;
		}

		count++;

		if (str_flg == LONG_KI) {
			long_form = make_long_ki(this_val);
			if (long_form) {
				length += strlen(long_form);
			}
		} else {
			length += strlen(this_val);
		}
		if (separator) {
			length += sep_length;
		}
	}

	// Convert each item
	if (length) {
		convert_str = new char[length + 1];
		*convert_str = '\0';

		// Add the starter
		if (starter) {
			(void) strcat(convert_str, starter);
		}
		for (value_list.start(); !value_list.end(); value_list.next()) {
			char *this_val = value_list.getValue();
			if (!this_val) {
				continue;
			}

			i++;

			// Get the long form
			if (str_flg == LONG_KI) {
				long_form = make_long_ki(this_val);
				if (long_form) {
					(void) strcat(convert_str, long_form);
				}
			} else {
				(void) strcat(convert_str, this_val);
			}

			// Add the separator, but not the last one
			if (separator && (i < count)) {
				(void) strcat(convert_str, separator);
			}
		}
	}

	return (convert_str);
}

//
// clta_check_objtype
//
//	Check if the given object type is valid among the *sensor_t.
//	Return true if it is; return false if invalid.
//
const char *
clta_check_objtype(char *obj_type, sensor_t *ccr_telattr_list)
{
	sensor_t *s_ptr = NULL;
	ki_t *ki_ptr = NULL;
	const char *short_type;

	if (!ccr_telattr_list || !obj_type) {
		return (NULL);
	}

	// Go through the list
	for (s_ptr = ccr_telattr_list; s_ptr; s_ptr = s_ptr->next) {
		// Go through the ki_list
		for (ki_ptr = s_ptr->ki_list; ki_ptr; ki_ptr = ki_ptr->next) {
			short_type = make_short_mot(ki_ptr->motype);
			if (!short_type) {
				continue;
			}

			if ((strcasecmp(obj_type, ki_ptr->motype) == 0) ||
			    (strcasecmp(obj_type, short_type) == 0)) {
				return (short_type);
			}
		}
	}

	return (NULL);
}

//
// clta_add_ki_list
//
//	Add the given ki_t into the list.
//
clerrno_t
clta_add_ki_list(ki_t **valid_clta_list, ki_t *ki_ptr,
    const char *ki_name, const char *ki_type)
{
	ki_t *new_ki;

	// Check input
	if (!ki_name || !ki_type || !ki_ptr || !valid_clta_list) {
		clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}

	new_ki = (ki_t *)calloc(1, sizeof (ki_t));
	if (!new_ki) {
		clcommand_perror(CL_ENOMEM);
		return (CL_ENOMEM);
	}

	// Duplicate the ki
	new_ki->id = ki_ptr->id;
	new_ki->name = strdup(ki_name);
	new_ki->unit = strdup(ki_ptr->unit);
	new_ki->enabled = strdup(ki_ptr->enabled);
	new_ki->motype = strdup(ki_type);
	new_ki->divisor = strdup(ki_ptr->divisor);
	if (!new_ki->name || !new_ki->unit || !new_ki->enabled ||
	    !new_ki->motype || !new_ki->divisor) {
		clcommand_perror(CL_ENOMEM);
		return (CL_ENOMEM);
	}

	new_ki->next = *valid_clta_list;
	*valid_clta_list = new_ki;

	return (CL_NOERR);
}

//
// clta_free_ta_list
//
//	Free the memory allocated to the ki_t list.
//
void
clta_free_ta_list(ki_t *clta_ki_list)
{
	ki_t *ki_ptr;
	ki_t **ki_list;

	if (!clta_ki_list) {
		return;
	}

	// Go through the list
	ki_list = &clta_ki_list;
	while (*ki_list) {
		if ((*ki_list)->name) {
			free((*ki_list)->name);
		}
		if ((*ki_list)->unit) {
			free((*ki_list)->unit);
		}
		if ((*ki_list)->enabled) {
			free((*ki_list)->enabled);
		}
		if ((*ki_list)->motype) {
			free((*ki_list)->motype);
		}
		if ((*ki_list)->divisor) {
			free((*ki_list)->divisor);
		}

		ki_ptr = *ki_list;
		*ki_list = (*ki_list)->next;
		free(ki_ptr);
	}
}

//
// clta_isvalue_in_kilist
//
//	Check if the given ki name or type is inside the ki list. Both the
//	list and names use short format.
//
bool
clta_isvalue_in_kilist(ki_t *clta_ki_list, const char *ki_name,
    const char *ki_motype)
{
	bool found = false;
	ki_t *ki_ptr = NULL;

	if (!clta_ki_list || (!ki_name && !ki_motype)) {
		return (found);
	}

	// Go through the ki_list
	for (ki_ptr = clta_ki_list; ki_ptr; ki_ptr = ki_ptr->next) {
		if (!ki_ptr->motype || !ki_ptr->name) {
			continue;
		}

		if (ki_name && (strcasecmp(ki_ptr->name, ki_name) != 0)) {
			continue;
		}

		if (ki_motype && (strcasecmp(ki_ptr->motype, ki_motype) != 0)) {
			continue;
		}

		found = true;
		break;
	}

	return (found);
}

//
// clta_save_threshold_list
//
//	Called when read the threshold CCR table to save the thesholds
//	into ccr_thresholds.
//
scslm_error_t
clta_save_threshold_list(const char *thresh_key,
    char *thresh_value, void *ccr_thresholds)
{
	uint_t id_in_ccr = 0;
	uint_t ccr_field;
	scslm_error_t scslm_errno = SCSLM_OK;
	threshold_t *ccr_threshold = NULL;
	threshold_list_t **test_ptr = (threshold_list_t **)ccr_thresholds;

	// extarct the threshold id and the field
	scslm_errno = threshold_parse_ccr_key(thresh_key, &id_in_ccr,
	    &ccr_field);
	if (scslm_errno != SCSLM_OK) {
		goto cleanup;
	}

	// Use the id to get the threshold data
	ccr_threshold = threshold_list_extract(*test_ptr, id_in_ccr);
	if (ccr_threshold == NULL) {
		ccr_threshold = threshold_allocate();
		if (!ccr_threshold) {
			scslm_errno = SCSLM_ENOMEM;
			goto cleanup;
		}

		// Insert this threshold
		scslm_errno = threshold_list_insert(
		    (threshold_list_t **)ccr_thresholds,
		    id_in_ccr, ccr_threshold);
		if (scslm_errno != SCSLM_OK) {
			goto cleanup;
		}
	}

	// Set the threshold field
	scslm_errno = clta_threshold_set_field(ccr_threshold,
	    ccr_field, thresh_value);

cleanup:

	return (scslm_errno);
}

//
// clta_is_node
//
//	Check is the given node is in the valid node list. Return true
//	if it is; return false if not.
//
bool
clta_is_node(NameValueList &valid_nodes, const char *node_name)
{
	if (!node_name || (valid_nodes.getSize() == 0)) {
		return (false);
	}

	for (valid_nodes.start(); !valid_nodes.end(); valid_nodes.next()) {
		if (strcmp(node_name, valid_nodes.getName()) == 0) {
			return (true);
		}
	}

	return (false);
}

//
// clta_threshold_set_field
//
//	Set the value of a field of a threshold using the short format
//	of motype and TA names.
//
scslm_error_t
clta_threshold_set_field(threshold_t *ccr_threshold,
    uint_t ccr_field, const char *thresh_value)
{
	const char *short_name;

	if (!ccr_threshold || (ccr_field >= THRESH_NB_FIELDS)) {
		return (SCSLM_EINTERNAL);
	}

	free((*ccr_threshold)[ccr_field]);

	// if the given string is empty, store the EMPTY_STRING instead
	if (!thresh_value || (*thresh_value == '\0')) {
		(*ccr_threshold)[ccr_field] = strdup(EMPTY_VALUE);
	} else {
		if (ccr_field == THRESH_MOTYPE) {
			short_name = make_short_mot(thresh_value);
			(*ccr_threshold)[ccr_field] = strdup(short_name);
		} else if (ccr_field == THRESH_KINAME) {
			short_name = make_short_ki(thresh_value);
			(*ccr_threshold)[ccr_field] = strdup(short_name);
		} else {
			(*ccr_threshold)[ccr_field] = strdup(thresh_value);
		}
	}

	if ((*ccr_threshold)[ccr_field] == NULL) {
		return (SCSLM_ENOMEM);
	}

	return (SCSLM_OK);
}
