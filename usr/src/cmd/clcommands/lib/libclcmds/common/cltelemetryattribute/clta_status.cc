//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clta_status.cc	1.4	08/05/20 SMI"

#include "cltelemetryattribute.h"
#include "ClStatus.h"
#include "clnode.h"

static clerrno_t clta_get_status(ValueList &operands, optflgs_t optflgs,
    ValueList &obj_types, ValueList &obj_instances, ValueList &nodes,
    ClStatus *prt_output, bool *print_it, bool print_err);
static clerrno_t clta_call_printcmd(char *full_cmd, ki_t *ki_list,
    ClStatus *prt_output, bool *print_it);

//
// clta "status"
//
clerrno_t
clta_status(ClCommand &cmd, ValueList &operands, optflgs_t optflgs,
    ValueList &obj_types, ValueList &obj_instances, ValueList &nodes)
{
	ClStatus *prt_output = new ClStatus(cmd.isVerbose ? true : false);
	bool print_it = false;
	clerrno_t clerrno = CL_NOERR;

	// Get the status
	clerrno = clta_get_status(operands, optflgs, obj_types, obj_instances,
	    nodes, prt_output, &print_it, true);
	if ((clerrno == CL_NOERR) || print_it) {
		prt_output->print();
	}

	delete prt_output;
	return (clerrno);
}

//
// get_clta_status_obj
//
//	Get the status of all telemetry instances.
//
clerrno_t
get_clta_status_obj(optflgs_t optflgs, ClStatus *clta_clstatus)
{
	ValueList empty_list;
	bool print_it = false;
	clerrno_t clerrno = CL_NOERR;
	bool print_err = (optflgs & Fflg) ? true : false;

	if (!clta_status) {
		clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}

	// Set the title
	clta_clstatus->setHeadingTitle(HEAD_TELEATTRIBUTE);

	clerrno = clta_get_status(empty_list, optflgs, empty_list, empty_list,
	    empty_list, clta_clstatus, &print_it, print_err);

	return (clerrno);
}

//
// clta_get_status
//
//	Get the ClStatus data.
//
clerrno_t
clta_get_status(ValueList &operands, optflgs_t optflgs,
    ValueList &obj_types, ValueList &obj_instances, ValueList &nodes,
    ClStatus *prt_output, bool *print_it, bool print_err)
{
	clerrno_t clerrno = CL_NOERR;
	clerrno_t first_err = CL_NOERR;
	ki_t *valid_clta_list = NULL;
	ki_t *ki_ptr, *n_ki_ptr;
	ValueList process_types = ValueList(true);
	ValueList process_kis = ValueList(true);
	char *instance_str = NULL;
	char *ki_str = NULL;
	char *motype_str = NULL;
	char *node_str = NULL;
	char full_cmd[COMMAND_BUFFER_SIZE];
	char *cacao_cmd = NULL;
	const char *long_motype;
	const char *long_kiname;
	NameValueList valid_nodes = NameValueList(true);
	ValueList new_instances = ValueList(true);
	ValueList new_nodes = ValueList(true);

	// Check input
	if (!prt_output || !print_it) {
		clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}

	// Get the valid telemetry attributes
	clerrno = clta_preprocess_talist(operands, obj_types,
	    &valid_clta_list, print_err);
	if ((clerrno != CL_NOERR) && (first_err == CL_NOERR)) {
		first_err = clerrno;
	}

	// Check the nodes
	if (nodes.getSize() != 0) {
		// Get all cluster nodes
		ValueList node_type;
		node_type.add(NODETYPE_SERVER);

		clerrno = clnode_preprocess_nodelist(nodes, node_type,
		    valid_nodes, true);
		if (clerrno != CL_NOERR) {
			if (first_err == CL_NOERR) {
				first_err = clerrno;
			}
		}

		// No valid nodes?
		if (valid_nodes.getSize() == 0) {
			goto cleanup;
		}

		for (valid_nodes.start(); !valid_nodes.end();
		    valid_nodes.next()) {
			new_nodes.add(valid_nodes.getName());
		}
	}

	// If nothing
	if (!valid_clta_list) {
		goto cleanup;
	}

	// Process the instance list
	for (obj_instances.start(); !obj_instances.end();
	    obj_instances.next()) {
		char *this_val = obj_instances.getValue();
		char *node_name = (char *)0;
		char *zone_name = (char *)0;

		if (strchr(this_val, ':')) {
			node_name = new char[strlen(this_val) + 1];
			(void) strcpy(node_name, this_val);

			// Get the zone name and node name
			zone_name = strchr(node_name, ':');
			*zone_name++ = '\0';
			if ((strlen(zone_name) == 0) ||
			    (strlen(node_name) == 0)) {
				clerror("Invalid instance \"%s\".\n",
				    this_val);
				if (first_err == CL_NOERR) {
					first_err = CL_EINVAL;
				}
				delete [] node_name;
				continue;
			}
			new_instances.add(zone_name);
			new_nodes.add(node_name);
			delete [] node_name;
		} else {
			new_instances.add(this_val);
		}
	}

	// For instances, just pass to cacao
	if (new_instances.getSize() != 0) {
		instance_str = clta_convert_valuelist_str(
		    new_instances, "-moname ", " -moname ", 0);
		if (!instance_str || (*instance_str == '\0')) {
			clcommand_perror(CL_EINTERNAL);
			if (first_err == CL_NOERR) {
				first_err = CL_EINTERNAL;
			}
			goto cleanup;
		}
	}

	// Get the node string
	if (new_nodes.getSize() != 0) {
		node_str = clta_convert_valuelist_str(new_nodes,
		    "-nodename ", " -nodename ", 0);
		if (!node_str || (*node_str == '\0')) {
			clcommand_perror(CL_EINTERNAL);
			if (first_err == CL_NOERR) {
				first_err = CL_EINTERNAL;
			}
			goto cleanup;
		}
	}

	// Get the base cacao command
	cacao_cmd = clta_get_cacao_cmd(CMD_STATUS);
	if (cacao_cmd == NULL) {
		clcommand_perror(CL_EINTERNAL);
		if (first_err == CL_NOERR) {
			first_err = CL_EINTERNAL;
		}
		goto cleanup;
	}

	// Set the header
	prt_output->setHeadings(
	    (char *)gettext("Instance"),
	    (char *)gettext("Type"),
	    (char *)gettext("Attribute"),
	    (char *)gettext("Node"),
	    (char *)gettext("Value"),
	    (char *)gettext("Status"));

	// Go through the attributes
	for (ki_ptr = valid_clta_list; ki_ptr; ki_ptr = ki_ptr->next) {
		// Get the long type
		long_motype = make_long_mot(ki_ptr->motype);
		long_kiname = make_long_ki(ki_ptr->name);
		if (!long_motype || !long_kiname) {
			continue;
		}

		// Add type and KI name
		process_types.add((char *)long_motype);
		process_kis.add((char *)long_kiname);
	}

	// Conver the motype and KIs into the cmass scslmthreshCmdStream format
	motype_str = clta_convert_valuelist_str(process_types, "-motype ",
	    " -motype ", 0);
	ki_str = clta_convert_valuelist_str(process_kis, "-kiname ",
	    " -kiname ", 0);
	if (!ki_str || *ki_str == '\0' || !motype_str || *motype_str == '\0') {
		clcommand_perror(CL_EINTERNAL);
		if (first_err == CL_NOERR) {
			first_err = CL_EINTERNAL;
		}
		goto cleanup;
	}

	// Get the full cacao command
	*full_cmd = '\0';
	if (instance_str) {
		(void) sprintf(full_cmd, "%s %s %s %s ",
		    cacao_cmd, instance_str, motype_str, ki_str);
	} else {
		(void) sprintf(full_cmd, "%s %s %s ",
		    cacao_cmd, motype_str, ki_str);
	}
	if (node_str) {
		(void) strcat(full_cmd, node_str);
	}
	(void) strcat(full_cmd, " -all' 2>&1");

	// Execute the cacao command
	clerrno = clta_call_printcmd(full_cmd, valid_clta_list,
	    prt_output, print_it);
	if (clerrno != CL_NOERR) {
		if (first_err == CL_NOERR) {
			first_err = clerrno;
		}
	}

cleanup:
	clta_free_ta_list(valid_clta_list);
	if (instance_str) {
		delete [] instance_str;
	}
	if (ki_str) {
		delete [] ki_str;
	}
	if (motype_str) {
		delete [] motype_str;
	}
	if (node_str) {
		delete [] node_str;
	}

	return (first_err);
}

//
// clta_call_printcmd
//
//	Execute the given full_cmd, and put the ouput from that command to
//	*prt_output.
//
clerrno_t
clta_call_printcmd(char *full_cmd, ki_t *ki_list, ClStatus *prt_output,
    bool *print_it)
{
	clerrno_t clerrno = CL_NOERR;
	FILE *fp = NULL;
	char line_buf[BUFSIZ];
	double convert_value;
	char *tmp_ptr;
	char *instance;
	char *mo_type, *ki_name;
	char *status;
	const char *short_motype, *short_kiname;
	char *node;
	char *utilization;
	char convert_utilization[128];
	ki_t *associated_ki;

	// Check input
	if (!full_cmd || !ki_list || !prt_output || !print_it) {
		clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}

	// Execute the command
	if ((fp = popen(full_cmd, "r")) == NULL) {
		clerror("Failed to execute \"%s\".\n", full_cmd);
		clerrno = CL_EINTERNAL;
		goto cleanup;
	}

	// Read the output
	while (fgets(line_buf, BUFSIZ, fp) != NULL) {
		// Check for cacao errors
		if (strncmp(line_buf, "cacaocsc: unable to connect", 27) == 0) {
			clerror("Cannot connect to SLM Cacao Module.\n");
			clerror("cacao may not be running.\n");
			if (clerrno == CL_NOERR) {
				clerrno = CL_EOP;
			}
			goto cleanup;
		}
		if (strncmp(line_buf,
		    "java.lang.Exception: command not found", 38) == 0) {
			clerror("SLM Cacao Module is not running.\n");
			if (clerrno == CL_NOERR) {
				clerrno = CL_EOP;
			}
			goto cleanup;
		}
		if (strncmp(line_buf, "cacaocsc:", 9) == 0) {
			clerror("A problem occured with \"cacaocsc\".\n");
			if (clerrno == CL_NOERR) {
				clerrno = CL_EOP;
			}
			goto cleanup;
		}
		if (strncmp(line_buf, "ERROR: ", 7) == 0) {
			int error_code;
			(void) sscanf(line_buf, "ERROR: %ld", &error_code);

			// Check the error code
			switch (error_code) {
			case 1:
				clerror("A protocol error occured "
				    "with SLM Cacao module.\n");
				break;
			default:
				clerror("SLM Cacao module had an "
				    "internal error.\n");
				break;
			}
			if (clerrno == CL_NOERR) {
				clerrno = CL_EINTERNAL;
			}
			goto cleanup;
		}

		// Remove Cariage return at the end
		tmp_ptr = index(line_buf, '\n');
		if (tmp_ptr != NULL) {
			*tmp_ptr = '\0';
		}

		// the last line is containing a "NL" (ascii=10) only
		if (tmp_ptr == line_buf) {
			break;
		}

		// Get motype and its short notation
		mo_type = strtok(line_buf, " ");
		short_motype = make_short_mot(mo_type);

		// Get KI name and its short notation
		ki_name = strtok(NULL, " ");
		short_kiname = make_short_ki(ki_name);

		// Get node
		node = strtok(NULL, " ");

		// Get status
		status = strtok(NULL, " ");

		// Get the utilization value
		utilization = strtok(NULL, " ");

		// Get instance
		instance = strtok(NULL, " ");

		if (!node || !short_motype || !short_kiname ||
		    !instance || !status || !utilization) {
			clerror("Invalid line return by SLM cacao module.\n");
			if (clerrno == CL_NOERR) {
				clerrno = CL_EINTERNAL;
			}
			goto cleanup;
		}

		// Get converted utilization value
		(void) setlocale(LC_ALL, "C");
		(void) sscanf(utilization, "%lf", &convert_value);
		(void) setlocale(LC_ALL, "");

		// Get the unit
		associated_ki = clta_get_ki_from_ki_list(ki_list, short_kiname,
		    short_motype);
		if (associated_ki && associated_ki->unit) {
			(void) sprintf(convert_utilization,
			    "%lf %s", convert_value, associated_ki->unit);
		} else {
			(void) sprintf(convert_utilization, "%lf",
			    convert_value);
		}

		// Add it
		prt_output->addRow(instance, (char *)short_motype,
		    (char *)short_kiname, node, convert_utilization, status);
		*print_it = true;
	}

cleanup:
	if (fp != NULL) {
		(void) pclose(fp);
	}

	return (clerrno);
}
