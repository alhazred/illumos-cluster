//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clta_print.cc	1.6	08/05/20 SMI"

#include "cltelemetryattribute.h"
#include "ClStatus.h"
#include "clnode.h"

static clerrno_t clta_call_printcmd(char *full_cmd, optflgs_t optflgs,
    ki_t *ki_list, ClStatus *prt_ouput, bool *print_it);

//
// clta "print"
//
clerrno_t
clta_print(ClCommand &cmd, ValueList &operands, optflgs_t optflgs,
    char *date_range, ValueList &obj_types, ValueList &obj_instances,
    ValueList &nodes)
{
	clerrno_t clerrno = CL_NOERR;
	clerrno_t first_err = CL_NOERR;
	ki_t *valid_clta_list = NULL;
	ki_t *ki_ptr, *n_ki_ptr;
	ValueList processed_type = ValueList(true);
	ValueList onetype_kis = ValueList(true);
	char *instance_str = NULL;
	char *ki_str = NULL;
	char *node_str = NULL;
	char full_cmd[COMMAND_BUFFER_SIZE];
	char *cacao_cmd = NULL;
	char *instance_buf = NULL;
	const char *long_motype;
	char date_buf[BUFSIZ];
	ClStatus *prt_output = new ClStatus(cmd.isVerbose ? true : false);
	NameValueList valid_nodes = NameValueList(true);
	ValueList new_instances = ValueList(true);
	ValueList new_nodes = ValueList(true);
	bool print_it = false;
	int instance_len = 0;

	// Get the date range
	*date_buf = '\0';
	if (date_range) {
		long start_time = 0;
		long stop_time = 0;
		scslm_error_t scslm_err = SCSLM_NOERR;
		char tmp_buf[BUFSIZ];

		scslm_err = convert_date(date_range, &start_time, &stop_time);
		if (scslm_err != SCSLM_NOERR) {
			switch (scslm_err) {
			case SCSLM_ENOMEM:
				clcommand_perror(CL_ENOMEM);
				clerrno = CL_ENOMEM;
				break;
			case SCSLM_EINVAL:
				clerror("Invalid date \"%s\".\n",
				    date_range);
				clerrno = CL_EINVAL;
				break;
			default:
				clcommand_perror(CL_EINTERNAL);
				clerrno = CL_EINTERNAL;
				break;
			}
			if (first_err == CL_NOERR) {
				first_err = clerrno;
			}
			goto cleanup;
		} else {
			if (start_time != 0) {
				(void) sprintf(tmp_buf, " -start %ld",
				    start_time);
				(void) strcat(date_buf, tmp_buf);
			}
			if (stop_time != 0) {
				(void) sprintf(tmp_buf, " -stop %ld",
				    stop_time);
				(void) strcat(date_buf, tmp_buf);
			}
		}
	}

	// Get the valid telemetry attributes
	clerrno = clta_preprocess_talist(operands, obj_types,
	    &valid_clta_list, true);
	if ((clerrno != CL_NOERR) && (first_err == CL_NOERR)) {
		first_err = clerrno;
	}

	// Check the nodes
	if (nodes.getSize() != 0) {
		// Get all cluster nodes
		ValueList node_type;
		node_type.add(NODETYPE_SERVER);

		clerrno = clnode_preprocess_nodelist(nodes, node_type,
		    valid_nodes, true);
		if (clerrno != CL_NOERR) {
			if (first_err == CL_NOERR) {
				first_err = clerrno;
			}
		}

		// No valid nodes?
		if (valid_nodes.getSize() == 0) {
			goto cleanup;
		}

		for (valid_nodes.start(); !valid_nodes.end();
		    valid_nodes.next()) {
			new_nodes.add(valid_nodes.getName());
		}
	}

	// If nothing
	if (!valid_clta_list) {
		goto cleanup;
	}

	// Process the instance list
	for (obj_instances.start(); !obj_instances.end();
	    obj_instances.next()) {
		char *this_val = obj_instances.getValue();
		char *node_name = (char *)0;
		char *zone_name = (char *)0;

		if (strchr(this_val, ':')) {
			node_name = new char[strlen(this_val) + 1];
			(void) strcpy(node_name, this_val);

			// Get the zone name and node name
			zone_name = strchr(node_name, ':');
			*zone_name++ = '\0';
			if ((strlen(zone_name) == 0) ||
			    (strlen(node_name) == 0)) {
				clerror("Invalid instance \"%s\".\n",
				    this_val);
				if (first_err == CL_NOERR) {
					first_err = CL_EINVAL;
				}
				delete [] node_name;
				continue;
			}
			new_instances.add(zone_name);
			new_nodes.add(node_name);
			delete [] node_name;
		} else {
			new_instances.add(this_val);
		}
	}

	// For instances, just pass to cacao
	if (new_instances.getSize() != 0) {
		instance_str = clta_convert_valuelist_str(
		    new_instances, ",object-instance=", ",object-instance=", 0);
		if (!instance_str || (*instance_str == '\0')) {
			clcommand_perror(CL_EINTERNAL);
			if (first_err == CL_NOERR) {
				first_err = CL_EINTERNAL;
			}
			goto cleanup;
		}
	}

	// Get the node string
	if (new_nodes.getSize() != 0) {
		node_str = clta_convert_valuelist_str(new_nodes,
		    ",node=", " ,node=", 0);
		if (!node_str || (*node_str == '\0')) {
			clcommand_perror(CL_EINTERNAL);
			if (first_err == CL_NOERR) {
				first_err = CL_EINTERNAL;
			}
			goto cleanup;
		}
	}

	// Get the base cacao command
	cacao_cmd = clta_get_cacao_cmd(CMD_PRINT);
	if (cacao_cmd == NULL) {
		clcommand_perror(CL_EINTERNAL);
		if (first_err == CL_NOERR) {
			first_err = CL_EINTERNAL;
		}
		goto cleanup;
	}

	// Set the header
	prt_output->setHeadings(
	    (char *)gettext("Date"),
	    (char *)gettext("Instance"),
	    (char *)gettext("Type"),
	    (char *)gettext("Attribute"),
	    (char *)gettext("Node"),
	    (char *)gettext("Value"));

	// Go through the attributes
	for (ki_ptr = valid_clta_list; ki_ptr; ki_ptr = ki_ptr->next) {
		if (!ki_ptr->name || !ki_ptr->motype)
			continue;

		// Skip if already processed
		if (processed_type.isValue(ki_ptr->motype)) {
			continue;
		}

		// Find all the KIs of this type
		onetype_kis.clear();
		for (n_ki_ptr = valid_clta_list; n_ki_ptr;
		    n_ki_ptr = n_ki_ptr->next) {
			if (!n_ki_ptr->name || !n_ki_ptr->motype) {
				continue;
			}
			if (strcmp(n_ki_ptr->motype, ki_ptr->motype) == 0) {
				onetype_kis.add(n_ki_ptr->name);
			}
		}

		// Conver the KIs
		if (onetype_kis.getSize() == 0) {
			continue;
		}

		ki_str = clta_convert_valuelist_str(onetype_kis, "-k ",
		    ",", LONG_KI);
		if (!ki_str || *ki_str == '\0') {
			clcommand_perror(CL_EINTERNAL);
			if (first_err == CL_NOERR) {
				first_err = CL_EINTERNAL;
			}
			goto cleanup;
		}

		// Get the long type
		long_motype = make_long_mot(ki_ptr->motype);
		if (!long_motype) {
			continue;
		}

		// Make the "-m" string
		instance_len = strlen(long_motype) +
		    strlen("-m object-type=") + 1;
		if (instance_str) {
			instance_len += strlen(instance_str);
		}
		if (node_str) {
			instance_len += strlen(node_str);
		}
		instance_buf = new char[instance_len];

		*instance_buf = '\0';
		(void) strcat(instance_buf, "-m object-type=");
		(void) strcat(instance_buf, long_motype);
		if (instance_str) {
			(void) strcat(instance_buf, instance_str);
		}
		if (node_str) {
			(void) strcat(instance_buf, node_str);
		}

		// Add this type to the processed list
		processed_type.add(ki_ptr->motype);

		// Get the full cacao command
		*full_cmd = '\0';
		(void) sprintf(full_cmd, "%s %s %s",
		    cacao_cmd, ki_str, instance_buf);
		if (optflgs & aflg) {
			(void) strcat(full_cmd, " -y average");
		} else {
			(void) strcat(full_cmd, " -y latest");
		}
		if (*date_buf != '\0') {
			(void) strcat(full_cmd, date_buf);
		}
		(void) strcat(full_cmd, "' 2>&1");

		// Execute the cacao command
		clerrno = clta_call_printcmd(full_cmd, optflgs, valid_clta_list,
		    prt_output, &print_it);
		if (clerrno != CL_NOERR) {
			if (first_err == CL_NOERR) {
				first_err = clerrno;
			}
			if (clerrno == CL_EOP) {
				// No need to try further
				goto cleanup;
			}

			// Indicate which attribute is failed to process
			clerror("Failed to print the usage of \"%s\" for "
			    "object type \"%s\".\n",
			    ki_ptr->name, ki_ptr->motype);
		}

		if (ki_str) {
			delete [] ki_str;
			ki_str = NULL;
		}
	}

	// Print it
	if ((first_err == CL_NOERR) || print_it) {
		prt_output->print();
	}

cleanup:
	clta_free_ta_list(valid_clta_list);
	if (instance_str) {
		delete [] instance_str;
	}
	if (ki_str) {
		delete [] ki_str;
	}
	if (node_str) {
		delete [] node_str;
	}
	delete prt_output;

	return (first_err);
}

//
// clta_call_printcmd
//
//	Execute the given full_cmd, and put the ouput from that command to
//	*prt_output.
//
clerrno_t
clta_call_printcmd(char *full_cmd, optflgs_t optflgs, ki_t *ki_list,
    ClStatus *prt_output, bool *print_it)
{
	clerrno_t clerrno = CL_NOERR;
	char *timezone = NULL;
	FILE *fp = NULL;
	char line_buf[BUFSIZ];
	char date_buf[128];
	long date_value;
	double convert_value;
	char *tmp_ptr;
	char *date;
	char *instance;
	char *mo_type, *ki_name;
	const char *short_motype, *short_kiname;
	char *node;
	char *utilization;
	char convert_utilization[128];
	ki_t *associated_ki;

	// Check input
	if (!full_cmd || !ki_list || !prt_output || !print_it) {
		clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}

	// Change time zone. "Mflg" for GMT.
	if (optflgs & Mflg) {
		// Get current timezone
		timezone = getenv("TZ");
		if (putenv("TZ=GMT") != 0) {
			clerror("Cannot change the timezone.\n");
			clerrno = CL_EINTERNAL;
			goto cleanup;
		}
	}

	// Execute the command
	if ((fp = popen(full_cmd, "r")) == NULL) {
		clerror("Failed to execute \"%s\".\n", full_cmd);
		clerrno = CL_EINTERNAL;
		goto cleanup;
	}

	// Read the output
	while (fgets(line_buf, BUFSIZ, fp) != NULL) {
		// Check for cacao errors
		if (strncmp(line_buf, "cacaocsc: unable to connect", 27) == 0) {
			clerror("Cannot connect to SLM Cacao Module.\n");
			clerror("cacao may not be running.\n");
			if (clerrno == CL_NOERR) {
				clerrno = CL_EOP;
			}
			goto cleanup;
		}
		if (strncmp(line_buf,
		    "java.lang.Exception: command not found", 38) == 0) {
			clerror("SLM Cacao Module is not running.\n");
			if (clerrno == CL_NOERR) {
				clerrno = CL_EOP;
			}
			goto cleanup;
		}
		if (strncmp(line_buf, "cacaocsc:", 9) == 0) {
			clerror("A problem occured with \"cacaocsc\".\n");
			if (clerrno == CL_NOERR) {
				clerrno = CL_EOP;
			}
			goto cleanup;
		}
		if (strncmp(line_buf, "ERROR: ", 7) == 0) {
			int error_code;
			(void) sscanf(line_buf, "ERROR: %ld", &error_code);

			// Check the error code
			switch (error_code) {
			case 1:
				clerror("A protocol error occured "
				    "with SLM Cacao module.\n");
				break;
			default:
				clerror("SLM Cacao module had an "
				    "internal error.\n");
				break;
			}
			if (clerrno == CL_NOERR) {
				clerrno = CL_EOP;
			}
			goto cleanup;
		}

		// Remove Cariage return at the end
		tmp_ptr = index(line_buf, '\n');
		if (tmp_ptr != NULL) {
			*tmp_ptr = '\0';
		}

		// the last line is containing a "NL" (ascii=10) only
		if (tmp_ptr == line_buf) {
			break;
		}

		// Get the time and convert it
		date = strtok(line_buf, " ");
		if (!date) {
			clerror("Invalid line return by SLM cacao module.\n");
			if (clerrno == CL_NOERR) {
				clerrno = CL_EINTERNAL;
			}
			goto cleanup;
		}
		// extract timestamp from buffer
		(void) sscanf(date, "%ld", &date_value);
		(void) cftime(date_buf, NULL, &date_value);

		// Get node
		node = strtok(NULL, " ");

		// Get motype and its short notation
		mo_type = strtok(NULL, " ");
		short_motype = make_short_mot(mo_type);

		// Get KI name and its short notation
		ki_name = strtok(NULL, " ");
		short_kiname = make_short_ki(ki_name);

		// Get instance
		instance = strtok(NULL, " ");

		// Get the utilization value
		utilization = strtok(NULL, " ");

		if (!node || !short_motype || !short_kiname ||
		    !instance || !utilization) {
			clerror("Invalid line return by SLM cacao module.\n");
			if (clerrno == CL_NOERR) {
				clerrno = CL_EINTERNAL;
			}
			goto cleanup;
		}

		// Get converted utilization value
		(void) setlocale(LC_ALL, "C");
		(void) sscanf(utilization, "%lf", &convert_value);
		(void) setlocale(LC_ALL, "");

		// Get the unit
		associated_ki = clta_get_ki_from_ki_list(ki_list, short_kiname,
		    short_motype);
		if (associated_ki && associated_ki->unit) {
			(void) sprintf(convert_utilization,
			    "%lf %s", convert_value, associated_ki->unit);
		} else {
			(void) sprintf(convert_utilization, "%lf",
			    convert_value);
		}

		// Add it
		prt_output->addRow(date_buf, instance, (char *)short_motype,
		    (char *)short_kiname, node, convert_utilization);
		*print_it = true;
	}

cleanup:
	if (fp != NULL) {
		(void) pclose(fp);
	}

	// Restore to the old timezone
	if ((optflgs & Mflg) && timezone) {
		// It's size if the prefix ("TZ=") plus the size of the
		// assigned value + 1.
		int no_mem = 0;
		char *set_tz = (char *)malloc(
		    (strlen(timezone) + 4) * sizeof (char*));
		if (!set_tz) {
			no_mem == 1;
		} else {
			(void) strcpy(set_tz, "TZ=");
			(void) strcat(set_tz, timezone);
			if (putenv(set_tz) != 0) {
				no_mem == 1;
			}
		}
		if (no_mem) {
			clerror("Cannot change the timezone due to "
			    "no memory.\n");
			if (clerrno == CL_NOERR) {
				clerrno = CL_ENOMEM;
			}
		}
	}

	return (clerrno);
}
