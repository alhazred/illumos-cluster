//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clta_list.cc	1.3	08/05/20 SMI"

#include "cltelemetryattribute.h"
#include "ClList.h"

//
// clta "list"
//
clerrno_t
clta_list(ClCommand &cmd, ValueList &operands, optflgs_t optflgs,
    ValueList &obj_types)
{
	clerrno_t clerrno = CL_NOERR;
	ki_t *valid_clta_list = NULL;
	ki_t *ki_ptr;
	ClList print_list = ClList(cmd.isVerbose ? true : false);
	NameValueList clta_name_type_list = NameValueList(true);

	// Get the valid telemetry attributes
	clerrno = clta_preprocess_talist(operands, obj_types,
	    &valid_clta_list, true);
	// If nothing to print
	if (!valid_clta_list) {
		return (clerrno);
	}

	// If verbose, add the header
	if ((optflgs & vflg) || cmd.isVerbose) {
		print_list.setHeadings((char *)gettext("Telemetry Attribute"),
		    (char *)gettext("Object Type"));
	}

	// List the attributes
	for (ki_ptr = valid_clta_list; ki_ptr; ki_ptr = ki_ptr->next) {
		if (!ki_ptr->name || !ki_ptr->motype)
			continue;

		if (cmd.isVerbose) {
			print_list.addRow(ki_ptr->name, ki_ptr->motype);
		} else {
			//
			// Add as Name Value pair into clta_name_type_list
			// to filter out the duplicate names
			//
			clta_name_type_list.add(ki_ptr->name, ki_ptr->motype);
		}
	}

	// Add into ClList for non-verbose
	if (!cmd.isVerbose) {
		for (clta_name_type_list.start(); !clta_name_type_list.end();
		    clta_name_type_list.next()) {
			print_list.addRow(clta_name_type_list.getName(),
			    clta_name_type_list.getValue());
		}
	}

	// Print it
	print_list.print();

	// Free the telemetry attribute list
	clta_free_ta_list(valid_clta_list);

	return (clerrno);
}
