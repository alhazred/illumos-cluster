//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clta_show.cc	1.4	08/05/20 SMI"

#include "cltelemetryattribute.h"

#define	STR_THRESH_HEAD		"<Direction, Severity, Value, Rearm>"

static clerrno_t clta_get_show(ValueList &clta_attributes, optflgs_t optflgs,
    ValueList &obj_types, ValueList &obj_instances, ValueList &nodes,
    ClShow *clta_clshow, bool print_err);
static clerrno_t clta_add_ClShow_child(const char *mo_type, const char *ki_name,
    threshold_list_t *valid_thresholds, ClShow *clta_instance);

//
// clta "show"
//
clerrno_t
clta_show(ClCommand &cmd, ValueList &operands, optflgs_t optflgs,
    ValueList &obj_types, ValueList &obj_instances, ValueList &nodes)
{
	clerrno_t clerrno = CL_NOERR;
	ClShow *print_show = new ClShow(HEAD_TELEATTR);

	// Get the valid telemetry attributes
	clerrno = clta_get_show(operands, optflgs, obj_types, obj_instances,
	    nodes, print_show, true);

	// Print it
	if ((clerrno == CL_NOERR) ||
	    (print_show->getObjectSize() != 0)) {
		print_show->print();
	}

	// Free the object
	delete print_show;

	return (clerrno);
}

//
// get_clta_show_obj
//
//	Get the clta show object for all the telemetry attribtues. The ClShow
//	object must be created before calling.
//
clerrno_t
get_clta_show_obj(optflgs_t optflgs, ClShow *clta_clshow)
{
	ValueList empty_list;
	clerrno_t clerrno = CL_NOERR;
	bool print_err = (optflgs & Fflg) ? true : false;

	// Check inputs
	if (!clta_show) {
		clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}

	// Get the ClShow object for all telemetry attribtues
	clerrno = clta_get_show(empty_list, optflgs, empty_list, empty_list,
	    empty_list, clta_clshow, print_err);

	return (clerrno);
}

//
// clta_get_show
//
//	Get the ClShow object for the telemetry attributes by the given
//	inputs, including specified attributes, object types and instances,
//	and node list. The ClShow object must be created before calling,
//	and the caller is responsible to free the object afterwards.
//
clerrno_t
clta_get_show(ValueList &clta_attributes, optflgs_t optflgs,
    ValueList &obj_types, ValueList &obj_instances, ValueList &nodes,
    ClShow *clta_clshow, bool print_err)
{
	clerrno_t clerrno = CL_NOERR;
	clerrno_t first_err = CL_NOERR;
	ki_t *valid_clta_list = NULL;
	ki_t *ki_ptr, *n_ki_ptr;
	ValueList processed_ki = ValueList(true);
	ValueList ki_motypes = ValueList(true);
	NameValueList clta_name_type_list = NameValueList(true);
	const char *last_type = NULL;
	threshold_list_t *valid_thresholds = NULL;
	threshold_list_t *th_ptr;
	threshold_t *p_thresh;
	int found;
	bool print_ki = true;
	char enabled_buf[BUFSIZ];
	char disabled_buf[BUFSIZ];

	// Check inputs
	if (!clta_show) {
		clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}

	// Get the valid telemetry attributes
	clerrno = clta_preprocess_talist(clta_attributes, obj_types,
	    &valid_clta_list, print_err);
	// If nothing
	if (!valid_clta_list) {
		return (clerrno);
	}

	if (clerrno != CL_NOERR) {
		first_err = clerrno;
	}

	// If verbose, process the object instances.
	if ((obj_instances.getSize() != 0) || (nodes.getSize() != 0)) {
		print_ki = false;
	}
	if ((optflgs & vflg) ||
	    (obj_instances.getSize() != 0) || (nodes.getSize() != 0)) {
		// Get all the instance with nodes
		clerrno = clta_preprocess_thresh(obj_instances, nodes,
		    valid_clta_list, &valid_thresholds);
		if (clerrno != CL_NOERR) {
			if (first_err == CL_NOERR) {
				first_err = clerrno;
			}
			if (!valid_thresholds) {
				goto cleanup;
			}
		}
	}

	clta_clshow->propSet = false;

	// Go through the attributes
	for (ki_ptr = valid_clta_list; ki_ptr; ki_ptr = ki_ptr->next) {
		if (!ki_ptr->name || !ki_ptr->motype)
			continue;

		// Skip if already processed
		if (processed_ki.isValue(ki_ptr->name)) {
			continue;
		}

		// Is it in the instance thresholds list?
		if (!print_ki) {
			if (!valid_thresholds) {
				continue;
			}

			found = 0;
			for (th_ptr = valid_thresholds; th_ptr;
			    th_ptr = th_ptr->next) {
				p_thresh = th_ptr->threshold;
				if ((strcmp((*p_thresh)[THRESH_MOTYPE],
				    ki_ptr->motype) != 0) ||
				    (strcmp((*p_thresh)[THRESH_KINAME],
				    ki_ptr->name) != 0)) {
					continue;
				}
				found = 1;
				break;
			}
			if (!found) {
				continue;
			}
		}

		processed_ki.add(ki_ptr->name);

		// Add the KI to ClShow
		clta_clshow->addObject(
		    (char *)gettext("Telemetry Attribute"),
		    ki_ptr->name);
		// Add its configuration data
		if (ki_ptr->unit && (strlen(ki_ptr->unit) != 0)) {
			clta_clshow->addProperty(ki_ptr->name,
			    (char *)gettext("Unit"), ki_ptr->unit);
		}

		// Initialize
		ki_motypes.clear();
		*enabled_buf = '\0';
		*disabled_buf = '\0';

		// There might be multple object types share the same attribute
		for (n_ki_ptr = valid_clta_list; n_ki_ptr;
		    n_ki_ptr = n_ki_ptr->next) {
			if (!n_ki_ptr->name || !n_ki_ptr->motype ||
			    (strcmp(ki_ptr->name, n_ki_ptr->name) != 0)) {
				continue;
			}

			// Skip if the type is already added
			if (ki_motypes.isValue(n_ki_ptr->motype)) {
				continue;
			}

			// Is it enabled?
			if (n_ki_ptr->enabled &&
			    (strcmp(n_ki_ptr->enabled, SCSLMADM_TRUE) == 0)) {
				if (*enabled_buf != '\0') {
					(void) strcat(enabled_buf, ", ");
				}
				(void) strcat(enabled_buf, n_ki_ptr->motype);
				ki_motypes.add(n_ki_ptr->motype);
			}

			// Is it disabled?
			if (!n_ki_ptr->enabled ||
			    (strcmp(n_ki_ptr->enabled, SCSLMADM_FALSE) == 0)) {
				if (*disabled_buf != '\0') {
					(void) strcat(disabled_buf, ", ");
				}
				(void) strcat(disabled_buf, n_ki_ptr->motype);
				ki_motypes.add(n_ki_ptr->motype);
			}
		}

		// Add the enabled types
		if (*enabled_buf != '\0') {
			clta_clshow->addProperty(ki_ptr->name,
			    (char *)gettext("Enabled Object Types"),
			    enabled_buf);
		}

		// Add the disabled types
		if (*disabled_buf != '\0') {
			clta_clshow->addProperty(ki_ptr->name,
			    (char *)gettext("Disabled Object Types"),
			    disabled_buf);
		}

		// Add object instances and thresholds
		if (valid_thresholds) {
			// Add all the instance configuration for each of
			// of the object types of this KI
			for (ki_motypes.start(); !ki_motypes.end();
			    ki_motypes.next()) {
				char *mo_type = ki_motypes.getValue();

				ClShow *clta_instance =
				    new ClShow(SUBHEAD_TAINST, mo_type);
				// Add the configuration of the instances
				clerrno = clta_add_ClShow_child(mo_type,
				    ki_ptr->name, valid_thresholds,
				    clta_instance);
				if (clerrno != CL_NOERR) {
					if (first_err == CL_NOERR) {
						first_err = clerrno;
					}
				}
				if (clerrno == CL_NOERR &&
				    clta_instance->getObjectSize() != 0) {
					clta_clshow->addChild(ki_ptr->name,
					    clta_instance);
				}
			}
		}
	}

cleanup:
	if (valid_thresholds) {
		(void) threshold_list_free(&valid_thresholds);
	}
	clta_free_ta_list(valid_clta_list);

	return (first_err);
}

//
// clta_add_ClShow_child
//
//	Add the object instances of the given type and attributes,
//	also add its thresholds confgiurations.
//
clerrno_t
clta_add_ClShow_child(const char *mo_type, const char *ki_name,
    threshold_list_t *valid_thresholds, ClShow *clta_instance)
{
	clerrno_t clerrno = CL_NOERR;
	threshold_list_t *th_ptr;
	threshold_t *p_thresh;
	ValueList added_instances = ValueList(true);

	if (!mo_type || !ki_name || !valid_thresholds ||
	    !clta_instance) {
		clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}

	// Go through the thresholds
	for (th_ptr = valid_thresholds; th_ptr;
	    th_ptr = th_ptr->next) {
		p_thresh = th_ptr->threshold;

		if ((strcmp((*p_thresh)[THRESH_MOTYPE], mo_type) != 0) ||
		    (strcmp((*p_thresh)[THRESH_KINAME], ki_name) != 0)) {
			continue;
		}

		// Add instance object
		if ((*p_thresh)[THRESH_MONAME] &&
		    (strcmp((*p_thresh)[THRESH_MONAME],
		    EMPTY_VALUE) != 0) &&
		    !added_instances.isValue((*p_thresh)[THRESH_MONAME])) {
			clta_instance->addObject(
			    (char *)gettext("Object Instance"),
			    (*p_thresh)[THRESH_MONAME]);
			added_instances.add((*p_thresh)[THRESH_MONAME]);
		}

		// Add node
		if ((*p_thresh)[THRESH_NODENAME] &&
		    (strcmp((*p_thresh)[THRESH_NODENAME],
		    EMPTY_VALUE) != 0)) {
			ClShow *clta_node = new ClShow;
			clta_node->print_NL = false;
			clta_node->addObject((char *)gettext("Node"),
			    (*p_thresh)[THRESH_NODENAME]);
			clta_instance->addChild(
			    (*p_thresh)[THRESH_MONAME], clta_node);
		}

		// Add threshold as children
		if ((strcmp((*p_thresh)[THRESH_RISING_WARNING_VALUE],
		    EMPTY_VALUE) != 0) ||
		    (strcmp((*p_thresh)[THRESH_RISING_FATAL_VALUE],
		    EMPTY_VALUE) != 0) ||
		    (strcmp((*p_thresh)[THRESH_FALLING_WARNING_VALUE],
		    EMPTY_VALUE) != 0) ||
		    (strcmp((*p_thresh)[THRESH_FALLING_FATAL_VALUE],
		    EMPTY_VALUE) != 0)) {
			char th_buf[BUFSIZ];
			char thval_buf[BUFSIZ];
			int i = 1;

			ClShow *clta_threshs = new ClShow;
			clta_threshs->print_NL = false;
			clta_threshs->addObject((char *)gettext("Thresholds"),
			    (char *)STR_THRESH_HEAD);

			// The rising - warning threshold
			if (strcmp((*p_thresh)[THRESH_RISING_WARNING_VALUE],
			    EMPTY_VALUE) != 0) {
				(void) sprintf(th_buf, gettext("Threshold"));
				(void) sprintf(th_buf, "%s %d", th_buf, i);

				// Get the threshold value
				(void) sprintf(thval_buf,
				    "<rising, warning, %s, %s>",
				    (*p_thresh)[THRESH_RISING_WARNING_VALUE],
				    (*p_thresh)[THRESH_RISING_WARNING_REARM]);
				clta_threshs->addProperty(
				    (char *)STR_THRESH_HEAD,
				    th_buf, thval_buf);
				i++;
			}

			// The rising - fatal threshold
			if (strcmp((*p_thresh)[THRESH_RISING_FATAL_VALUE],
			    EMPTY_VALUE) != 0) {
				(void) sprintf(th_buf, gettext("Threshold"));
				(void) sprintf(th_buf, "%s %d", th_buf, i);

				// Get the threshold value
				(void) sprintf(thval_buf,
				    "<rising, fatal, %s, %s>",
				    (*p_thresh)[THRESH_RISING_FATAL_VALUE],
				    (*p_thresh)[THRESH_RISING_FATAL_REARM]);
				clta_threshs->addProperty(
				    (char *)STR_THRESH_HEAD,
				    th_buf, thval_buf);
				i++;
			}

			// The falling - warning threshold
			if (strcmp((*p_thresh)[THRESH_FALLING_WARNING_VALUE],
			    EMPTY_VALUE) != 0) {
				(void) sprintf(th_buf, gettext("Threshold"));
				(void) sprintf(th_buf, "%s %d", th_buf, i);

				// Get the threshold value
				(void) sprintf(thval_buf,
				    "<falling, warning, %s, %s>",
				    (*p_thresh)[THRESH_FALLING_WARNING_VALUE],
				    (*p_thresh)[THRESH_FALLING_WARNING_REARM]);
				clta_threshs->addProperty(
				    (char *)STR_THRESH_HEAD,
				    th_buf, thval_buf);
				i++;
			}

			// The falling - fatal threshold
			if (strcmp((*p_thresh)[THRESH_FALLING_FATAL_VALUE],
			    EMPTY_VALUE) != 0) {
				(void) sprintf(th_buf, gettext("Threshold"));
				(void) sprintf(th_buf, "%s %d", th_buf, i);

				// Get the threshold value
				(void) sprintf(thval_buf,
				    "<falling, fatal, %s, %s>",
				    (*p_thresh)[THRESH_FALLING_FATAL_VALUE],
				    (*p_thresh)[THRESH_FALLING_FATAL_REARM]);
				clta_threshs->addProperty(
				    (char *)STR_THRESH_HEAD,
				    th_buf, thval_buf);
			}

			clta_instance->addChild((*p_thresh)[THRESH_MONAME],
			    clta_threshs);
		}
	}

	return (clerrno);
}
