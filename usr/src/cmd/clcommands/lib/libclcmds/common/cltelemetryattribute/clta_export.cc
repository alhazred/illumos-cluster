//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clta_export.cc	1.4	08/05/20 SMI"

#include "cltelemetryattribute.h"
#include "ClXml.h"

static clerrno_t clta_get_ta_xml(ValueList &clta_attributes,
    ValueList &obj_types, CltelemetryattributeExport *clta_export,
    bool print_err, int *ta_count);

//
// clta "export"
//
clerrno_t
clta_export(ClCommand &cmd, ValueList &operands, optflgs_t optflgs,
    char *clconfiguration, ValueList &obj_types)
{
	clerrno_t clerrno = CL_NOERR;
	ClXml clta_clxml;
	CltelemetryattributeExport *clta_export =
	    new CltelemetryattributeExport();
	clerrno_t first_err = CL_NOERR;
	char *export_file = (char *)0;
	NameValueList clta_name_type_list = NameValueList(true);
	int ta_count = 0;

	// Check inputs
	if (!clconfiguration) {
		export_file = strdup("-");
	} else {
		export_file = strdup(clconfiguration);
	}
	if (!export_file) {
		clerrno = first_err = CL_ENOMEM;
		clcommand_perror(clerrno);
		goto cleanup;
	}

	// Get the xml elements
	clerrno = clta_get_ta_xml(operands, obj_types, clta_export, true,
	    &ta_count);
	if ((clerrno != CL_NOERR) && (first_err == CL_NOERR)) {
		first_err = clerrno;
	}

	if (ta_count != 0) {
		// Create xml
		clerrno = clta_clxml.createExportFile(CLTELEMETRYATTRIBUTE_CMD,
		    export_file, clta_export);
		if ((clerrno != CL_NOERR) && (first_err == CL_NOERR)) {
			first_err = clerrno;
		}
	}

cleanup:
	if (export_file) {
		free(export_file);
	}
	delete clta_export;

	return (first_err);
}

//
// get_cltelemetryattribute_xml_elements
//
//	Get all the telemetry attributes.
//
ClcmdExport *
get_clta_xml_elements()
{
	CltelemetryattributeExport *exp_clta = new CltelemetryattributeExport();
	ValueList empty_list;
	int ta_count = 0;

	// Add configuration of all the nas devices to ClnasExport object.
	(void) clta_get_ta_xml(empty_list, empty_list, exp_clta, false,
	    &ta_count);

	return (exp_clta);
}

//
// clta_get_ta_xml
//
//	Get all the telemetry attributes and add into
//	CltelemetryattributeExport, which must be created before calling.
//
clerrno_t
clta_get_ta_xml(ValueList &clta_attributes, ValueList &obj_types,
    CltelemetryattributeExport *clta_export, bool print_err, int *ta_count)
{
	clerrno_t clerrno = CL_NOERR;
	clerrno_t first_err = CL_NOERR;
	ki_t *valid_clta_list = NULL;
	ki_t *ki_ptr, *ki_nptr;
	ValueList processed_types = ValueList(true);

	// Check inputs
	if (!clta_export || !ta_count) {
		clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}

	*ta_count = 0;

	// Get the valid telemetry attributes
	clerrno = clta_preprocess_talist(clta_attributes, obj_types,
	    &valid_clta_list, print_err);
	if ((clerrno != CL_NOERR) && (first_err == CL_NOERR)) {
		first_err = clerrno;
	}

	// If nothing to export
	if (!valid_clta_list) {
		return (first_err);
	}

	// List the attributes
	for (ki_ptr = valid_clta_list; ki_ptr; ki_ptr = ki_ptr->next) {
		if (!ki_ptr->name || !ki_ptr->motype)
			continue;

		// Skip if already processed
		if (processed_types.isValue(ki_ptr->motype)) {
			continue;
		}

		// Add the onject type
		processed_types.add(ki_ptr->motype);
		clerrno = clta_export->createTelemetryObjectType(
		    ki_ptr->motype);
		if (clerrno != CL_NOERR) {
			if (first_err == CL_NOERR) {
				first_err = clerrno;
			}
			continue;
		} else {
			(*ta_count)++;
		}

		// Add the attribute of this type
		for (ki_nptr = valid_clta_list; ki_nptr;
		    ki_nptr = ki_nptr->next) {
			if (!ki_nptr->name || !ki_nptr->motype ||
			    !ki_nptr->enabled ||
			    (strcmp(ki_nptr->motype, ki_ptr->motype) != 0)) {
				continue;
			}

			// Add the attribute
			if (strcmp(ki_nptr->enabled, "true") == 0) {
				clerrno = clta_export->addTelemetryAttribute(
				    ki_ptr->motype,
				    ki_nptr->name, true);
			} else {
				clerrno = clta_export->addTelemetryAttribute(
				    ki_ptr->motype,
				    ki_nptr->name, false);
			}
			if (clerrno != CL_NOERR) {
				if (first_err == CL_NOERR) {
					first_err = clerrno;
				}
			} else {
				(*ta_count)++;
			}
		}
	}

	// Free the telemetry attribute list
	clta_free_ta_list(valid_clta_list);

	return (clerrno);
}
