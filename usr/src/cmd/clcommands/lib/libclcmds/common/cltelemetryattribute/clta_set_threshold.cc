//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clta_set_threshold.cc	1.7	08/06/23 SMI"

#include "cltelemetryattribute.h"
#include "clnode.h"

static scslm_error_t clta_save_threshold_list(const char *thresh_key,
    char *thresh_value, void *ccr_thresholds);
static clerrno_t clta_update_threshold_ccr(const char *long_kiname,
    const char *ki_name, const char *long_motype, const char *mo_type,
    const char *instance, char *node, NameValueList &threshold,
    optflgs_t optflgs);
static bool is_empty(const char *p_str);

//
// clta "set-threshold"
//
clerrno_t
clta_set_threshold(ClCommand &cmd, ValueList &operands, optflgs_t optflgs,
    ValueList &obj_types, ValueList &obj_instances, ValueList &nodes,
    NameValueList &threshold)
{
	clerrno_t clerrno = CL_NOERR;
	clerrno_t first_err = CL_NOERR;
	ki_t *valid_clta_list = NULL;
	scslm_error_t scslm_errno = SCSLM_OK;
	const char *mo_type, *long_motype;
	const char *ki_name, *long_kiname;
	bool is_zone = false;
	bool is_node = false;
	bool is_rg = false;
	NameValueList valid_nodes = NameValueList(true);
	NameValueList cluster_nodes = NameValueList(true);

	// Check inputs
	if ((obj_types.getSize() != 1) ||
	    (operands.getSize() != 1)) {
		clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}

	// Get the valid telemetry attributes
	clerrno = clta_preprocess_talist(operands, obj_types,
	    &valid_clta_list, true);
	// If nothing
	if (!valid_clta_list) {
		return (clerrno);
	}

	if (clerrno != CL_NOERR) {
		first_err = clerrno;
	}

	// Get the object type
	obj_types.start();
	mo_type = make_short_mot(obj_types.getValue());
	if (!mo_type) {
		// The obj_types is the short form.
		mo_type = obj_types.getValue();
	}
	if (!mo_type) {
		clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}
	if (strcasecmp(mo_type, "zone") == 0) {
		is_zone = true;
	} else if (strcasecmp(mo_type, "node") == 0) {
		is_node = true;
	} else if (strcasecmp(mo_type, "resourcegroup") == 0) {
		is_rg = true;
	}

	// Get all the cluster nodes for checking
	if (is_zone || is_node) {
		ValueList empty;
		ValueList node_type;
		node_type.add(NODETYPE_SERVER);


		// Get all the cluster nodes
		clerrno = clnode_preprocess_nodelist(empty, node_type,
		    cluster_nodes, true);
		if (clerrno != CL_NOERR) {
			if (first_err == CL_NOERR) {
				first_err = clerrno;
			}
		}
	}

	// Get the user specified cluster nodes
	if (!is_rg && !is_zone) {
		ValueList node_type;
		node_type.add(NODETYPE_SERVER);

		clerrno = clnode_preprocess_nodelist(nodes, node_type,
		    valid_nodes, true);
		if (clerrno != CL_NOERR) {
			if (first_err == CL_NOERR) {
				first_err = clerrno;
			}
		}

		//
		// No valid nodes? node for zone is already in the
		// instance.
		//
		if (valid_nodes.getSize() == 0) {
			goto cleanup;
		}
	}

	// Get the attribute name
	operands.start();
	ki_name = operands.getValue();
	long_motype = make_long_mot(mo_type);
	long_kiname = make_long_ki(ki_name);

	// Go through the object instance and the nodes
	for (obj_instances.start(); !obj_instances.end();
	    obj_instances.next()) {
		char *this_instance = obj_instances.getValue();

		// If the instance is a zone instance.
		if (is_zone) {
			char *zone_name = (char *)0;
			char *node_name = (char *)0;

			node_name = new char[strlen(this_instance) + 1];
			(void) strcpy(node_name, this_instance);

			// The instance is as "zone:node"
			if ((zone_name = strchr(node_name, ':')) != NULL) {
				*zone_name++ = '\0';
				if ((strlen(zone_name) == 0) ||
				    (strlen(node_name) == 0)) {
					clcommand_perror(CL_EINTERNAL);
					if (first_err == CL_NOERR) {
						first_err = CL_EINTERNAL;
					}
					delete [] node_name;
					continue;
				}

				// Check the node
				if (!clta_is_node(cluster_nodes,
				    node_name)) {
					clerror("The node in instance \"%s\" "
					    "does not exist.\n",
					    this_instance);
					if (first_err == CL_NOERR) {
						first_err = CL_ENOENT;
					}
					delete [] node_name;
					continue;
				}

				// Set the state
				clerrno = clta_update_threshold_ccr(
				    long_kiname, ki_name, long_motype,
				    mo_type, zone_name, node_name,
				    threshold, optflgs);
				if ((clerrno != CL_NOERR) &&
				    (first_err == CL_NOERR)) {
					first_err = clerrno;
				}
			}
			delete [] node_name;
			continue;
		}

		// Cluster-wide threshold: only for resourcegroup
		if (is_rg) {
			clerrno = clta_update_threshold_ccr(long_kiname,
			    ki_name, long_motype, mo_type, this_instance,
			    NULL, threshold, optflgs);
			if ((clerrno != CL_NOERR) && (first_err == CL_NOERR)) {
				first_err = clerrno;
			}

			continue;
		}

		// Special process for "node".
		if (is_node) {
			// Is the instance a valid node?
			if (!clta_is_node(cluster_nodes, this_instance)) {
				clerror("Node instance \"%s\" does not "
				    "exist.\n",
				    this_instance);
				if (first_err == CL_NOERR) {
					first_err = CL_ENOENT;
				}
				continue;
			}

			// Check against "-n"
			if ((nodes.getSize() != 0) &&
			    !clta_is_node(valid_nodes, this_instance)) {
				clerror("Node instance \"%s\" does not "
				    "match any nodes specified by \"-n\".\n",
				    this_instance);
				if (first_err == CL_NOERR) {
					first_err = CL_EINVAL;
				}
				continue;
			}

			clerrno = clta_update_threshold_ccr(long_kiname,
			    ki_name, long_motype, mo_type, this_instance,
			    NULL, threshold, optflgs);
			if ((clerrno != CL_NOERR) && (first_err == CL_NOERR)) {
				first_err = clerrno;
			}
			continue;
		}

		// Per-node threshold
		for (valid_nodes.start(); !valid_nodes.end();
		    valid_nodes.next()) {
			char *this_node = valid_nodes.getName();

			clerrno = clta_update_threshold_ccr(long_kiname,
			    ki_name, long_motype, mo_type, this_instance,
			    this_node, threshold, optflgs);
			if ((clerrno != CL_NOERR) && (first_err == CL_NOERR)) {
				first_err = clerrno;
			}
		}
	}

cleanup:
	clta_free_ta_list(valid_clta_list);

	return (first_err);
}


//
// clta_update_threshold_ccr
//
//	Update the threshold ccr table with the given parameters to
//	set or remove a threshold.
//
clerrno_t
clta_update_threshold_ccr(const char *long_kiname,
    const char *ki_name, const char *long_motype, const char *mo_type,
    const char *instance, char *node, NameValueList &threshold,
    optflgs_t optflgs)
{
	threshold_t *new_thresh = NULL;
	uint_t thresh_id;
	char *direction = NULL;
	char *severity = NULL;
	char *value = NULL;
	char *rearm = NULL;
	threshold_list_t *ccr_thresholds = NULL;
	uint_t value_id, rearm_id;
	scslm_error_t scslm_err = SCSLM_NOERR;
	clerrno_t clerrno = CL_NOERR;
	bool create_table = false;
	bool abort = true;
	bool need_free = true;

	// Check inputs
	if (!long_kiname || !ki_name || !long_motype || !instance) {
		clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}

	// Read the threshold ccr table for update
	scslm_errno = scslm_read_table_ccr(SCSLMTHRESH_TABLE,
	    clta_save_threshold_list, (void *)&ccr_thresholds, 1);
	if (scslm_errno != SCSLM_OK) {
		create_table = true;
	}

	// Allocate the threshold
	if ((new_thresh = threshold_allocate()) == NULL) {
		clcommand_perror(CL_ENOMEM);
		clerrno = CL_ENOMEM;
		goto cleanup;
	}

	// Set the mo type field for this threshold
	if (threshold_set_field(new_thresh, THRESH_MOTYPE,
	    long_motype) != SCSLM_NOERR) {
		clcommand_perror(CL_ENOMEM);
		clerrno = CL_ENOMEM;
		goto cleanup;
	}

	// Set the ki name for this threshold
	if (threshold_set_field(new_thresh, THRESH_KINAME,
	    long_kiname) != SCSLM_NOERR) {
		clcommand_perror(CL_ENOMEM);
		clerrno = CL_ENOMEM;
		goto cleanup;
	}

	// Set instance
	if (threshold_set_field(new_thresh, THRESH_MONAME,
	    instance) != SCSLM_NOERR) {
		clcommand_perror(CL_ENOMEM);
		clerrno = CL_ENOMEM;
		goto cleanup;
	}

	// Set node
	if (node) {
		if (threshold_set_field(new_thresh, THRESH_NODENAME,
		    node) != SCSLM_NOERR) {
			clcommand_perror(CL_ENOMEM);
			clerrno = CL_ENOMEM;
			goto cleanup;
		}
	}

	// Find a matched threshold from existing list
	thresh_id = threshold_list_get_matching_id(
	    ccr_thresholds, new_thresh);
	// If no threshold are found, insert the created one
	if (thresh_id == INVALID_THRESH_ID) {
		thresh_id = threshold_list_get_new_id(
		    ccr_thresholds);
		if (threshold_list_insert(&ccr_thresholds,
		    thresh_id, new_thresh) != SCSLM_NOERR) {
			clcommand_perror(CL_EINTERNAL);
			clerrno = CL_EINTERNAL;
			goto cleanup;
		}
	} else {
		threshold_t *found_thresh = NULL;
		found_thresh = threshold_list_extract(
		    ccr_thresholds, thresh_id);

		if (new_thresh && *new_thresh) {
			threshold_free(&new_thresh);
		}
		new_thresh = found_thresh;
		need_free = false;
	}

	// Assign the threshold values
	for (threshold.start(); !threshold.end(); threshold.next()) {
		char *t_name = threshold.getName();

		if (!t_name) {
			continue;
		}

		if (strcasecmp(t_name, STR_DIRECTION) == 0) {
			direction = threshold.getValue();
		} else if (strcasecmp(t_name, STR_SEVERITY) == 0) {
			severity = threshold.getValue();
		} else if (strcasecmp(t_name, STR_VALUELIMIT) == 0) {
			value = threshold.getValue();
		} else if (strcasecmp(t_name, STR_RESETVALUE) == 0) {
			rearm = threshold.getValue();
		} else {
			clcommand_perror(CL_EINTERNAL);
			clerrno = CL_EINTERNAL;
			goto cleanup;
		}
	}

	// Required fields
	if (!direction || !severity || (!value && !rearm)) {
		clcommand_perror(CL_EINTERNAL);
		clerrno = CL_EINTERNAL;
		goto cleanup;
	}

	if (strcasecmp(direction, STR_RISING) == 0) {
		if (strcasecmp(severity, STR_WARNING) == 0) {
			value_id = THRESH_RISING_WARNING_VALUE;
			rearm_id = THRESH_RISING_WARNING_REARM;
		} else {
			value_id = THRESH_RISING_FATAL_VALUE;
			rearm_id = THRESH_RISING_FATAL_REARM;
		}
	} else {
		if (strcasecmp(severity, STR_WARNING) == 0) {
			value_id = THRESH_FALLING_WARNING_VALUE;
			rearm_id = THRESH_FALLING_WARNING_REARM;
		} else {
			value_id = THRESH_FALLING_FATAL_VALUE;
			rearm_id = THRESH_FALLING_FATAL_REARM;
		}
	}

	// Set the value and rearm
	if (!(*new_thresh)[value_id] ||
	    (strcmp((*new_thresh)[value_id], EMPTY_VALUE) == 0)) {
		if (!value && !is_empty(rearm)) {
			clerror("You must specify the \"%s\" of the "
			    "threshold.\n", STR_VALUELIMIT);
			if (clerrno == CL_NOERR) {
				clerrno = CL_EPROP;
			}
		}
	} else if (value && (*value == '\0')) {
		if (threshold_set_field(new_thresh, value_id,
		    EMPTY_VALUE) != SCSLM_NOERR) {
			clcommand_perror(CL_ENOMEM);
			if (clerrno == CL_NOERR) {
				clerrno = CL_ENOMEM;
			}
		}

		if (threshold_set_field(new_thresh, rearm_id,
		    EMPTY_VALUE) != SCSLM_NOERR) {
			clcommand_perror(CL_ENOMEM);
			if (clerrno == CL_NOERR) {
				clerrno = CL_ENOMEM;
			}
		}
	}
	if (value && (*value != '\0')) {
		if (threshold_set_field(new_thresh, value_id,
		    value) != SCSLM_NOERR) {
			clcommand_perror(CL_ENOMEM);
			if (clerrno == CL_NOERR) {
				clerrno = CL_ENOMEM;
			}
		}
	}

	if (!is_empty(rearm)) {
		scslm_err = threshold_set_field(new_thresh, rearm_id,
		    rearm);
	} else if (rearm) {
		scslm_err = threshold_set_field(new_thresh, rearm_id,
		    EMPTY_VALUE);
	}
	if (scslm_err != SCSLM_NOERR) {
		clcommand_perror(CL_ENOMEM);
		if (clerrno == CL_NOERR) {
			clerrno = CL_ENOMEM;
		}
	}

	//
	// Validate the value that is set. Other test has been validated
	// in option processing part.
	//
	if (!is_empty(value) && !is_empty(rearm)) {
		if (((value_id == THRESH_RISING_WARNING_VALUE) ||
		    (value_id == THRESH_RISING_FATAL_VALUE)) &&
		    (atof(value) < atof(rearm))) {
			if (node) {
				clerror("The %s \"%s\" of this \"%s\" "
				    "threshold of \"%s\" "
				    "is smaller than the %s value \"%s\" "
				    "on node %s.\n",
				    STR_VALUELIMIT, value, STR_RISING,
				    instance, STR_RESETVALUE, rearm, node);
			} else {
				clerror("The %s \"%s\" of this \"%s\" "
				    "threshold of \"%s\" "
				    "is smaller than the %s value \"%s\".\n",
				    STR_VALUELIMIT, value, STR_RISING,
				    instance, STR_RESETVALUE, rearm);
			}
			if (clerrno == CL_NOERR) {
				clerrno = CL_EPROP;
			}
		}

		if (((value_id == THRESH_FALLING_FATAL_VALUE) ||
		    (value_id == THRESH_FALLING_WARNING_VALUE)) &&
		    (atof(value) > atof(rearm))) {
			if (node) {
				clerror("The %s \"%s\" of this \"%s\" "
				    "threshold of \"%s\" "
				    "is larger than the %s value \"%s\" "
				    "on node %s.\n",
				    STR_VALUELIMIT, value, STR_FALLING,
				    instance, STR_RESETVALUE, rearm, node);
			} else {
				clerror("The %s \"%s\" of this \"%s\" "
				    "threshold of \"%s\" "
				    "is larger than the %s value \"%s\".\n",
				    STR_VALUELIMIT, value, STR_FALLING,
				    instance, STR_RESETVALUE, rearm);
			}
			if (clerrno == CL_NOERR) {
				clerrno = CL_EPROP;
			}
		}
	}

	// Check for the ordering againt the existing thresholds
	if (!is_empty((*new_thresh)[THRESH_RISING_WARNING_VALUE]) &&
	    !is_empty((*new_thresh)[THRESH_RISING_FATAL_VALUE]) &&
	    (atof((*new_thresh)[THRESH_RISING_WARNING_VALUE]) >
	    atof((*new_thresh)[THRESH_RISING_FATAL_VALUE]))) {
		if (node) {
			clerror("The %s \"%s\" of the \"%s - %s\" threshold "
			    "of \"%s\" "
			    "is larger than the %s \"%s\" of the \"%s - %s\" "
			    "threshold on node %s.\n",
			    STR_VALUELIMIT,
			    (*new_thresh)[THRESH_RISING_WARNING_VALUE],
			    STR_RISING, STR_WARNING,
			    instance, STR_VALUELIMIT,
			    (*new_thresh)[THRESH_RISING_FATAL_VALUE],
			    STR_RISING, STR_FATAL, node);
		} else {
			clerror("The %s \"%s\" of the \"%s - %s\" threshold "
			    "of \"%s\" "
			    "is larger than the %s \"%s\" of the \"%s - %s\" "
			    "threshold.\n",
			    STR_VALUELIMIT,
			    (*new_thresh)[THRESH_RISING_WARNING_VALUE],
			    STR_RISING, STR_WARNING,
			    instance, STR_VALUELIMIT,
			    (*new_thresh)[THRESH_RISING_FATAL_VALUE],
			    STR_RISING, STR_FATAL);
		}
		if (clerrno == CL_NOERR) {
			clerrno = CL_EPROP;
		}
	}
	if (!is_empty((*new_thresh)[THRESH_RISING_WARNING_VALUE]) &&
	    !is_empty((*new_thresh)[THRESH_FALLING_WARNING_VALUE]) &&
	    (atof((*new_thresh)[THRESH_FALLING_WARNING_VALUE]) >
	    atof((*new_thresh)[THRESH_RISING_WARNING_VALUE]))) {
		if (node) {
			clerror("The %s \"%s\" of the \"%s - %s\" threshold "
			    "of \"%s\" "
			    "is larger than the %s \"%s\" of the \"%s - %s\" "
			    "threshold on node %s.\n",
			    STR_VALUELIMIT,
			    (*new_thresh)[THRESH_FALLING_WARNING_VALUE],
			    STR_FALLING, STR_WARNING,
			    instance, STR_VALUELIMIT,
			    (*new_thresh)[THRESH_RISING_WARNING_VALUE],
			    STR_RISING, STR_WARNING, node);
		} else {
			clerror("The %s \"%s\" of the \"%s - %s\" threshold "
			    "of \"%s\" "
			    "is larger than the %s \"%s\" of the \"%s - %s\" "
			    "threshold.\n",
			    STR_VALUELIMIT,
			    (*new_thresh)[THRESH_FALLING_WARNING_VALUE],
			    STR_FALLING, STR_WARNING,
			    instance, STR_VALUELIMIT,
			    (*new_thresh)[THRESH_RISING_WARNING_VALUE],
			    STR_RISING, STR_WARNING);
		}
		if (clerrno == CL_NOERR) {
			clerrno = CL_EPROP;
		}
	}
	if (!is_empty((*new_thresh)[THRESH_FALLING_WARNING_VALUE]) &&
	    !is_empty((*new_thresh)[THRESH_FALLING_FATAL_VALUE]) &&
	    (atof((*new_thresh)[THRESH_FALLING_FATAL_VALUE]) >
	    atof((*new_thresh)[THRESH_FALLING_WARNING_VALUE]))) {
		if (node) {
			clerror("The %s \"%s\" of the \"%s - %s\" threshold "
			    "of \"%s\" "
			    "is larger than the %s \"%s\" of the \"%s - %s\" "
			    "threshold on node %s.\n",
			    STR_VALUELIMIT,
			    (*new_thresh)[THRESH_FALLING_WARNING_VALUE],
			    STR_FALLING, STR_WARNING,
			    instance, STR_VALUELIMIT,
			    (*new_thresh)[THRESH_FALLING_FATAL_VALUE],
			    STR_FALLING, STR_FATAL, node);
		} else {
			clerror("The %s \"%s\" of the \"%s - %s\" threshold "
			    "of \"%s\" "
			    "is larger than the %s \"%s\" of the \"%s - %s\" "
			    "threshold.\n",
			    STR_VALUELIMIT,
			    (*new_thresh)[THRESH_FALLING_WARNING_VALUE],
			    STR_FALLING, STR_WARNING,
			    instance, STR_VALUELIMIT,
			    (*new_thresh)[THRESH_FALLING_FATAL_VALUE],
			    STR_FALLING, STR_FATAL);
		}
		if (clerrno == CL_NOERR) {
			clerrno = CL_EPROP;
		}
	}

	if (clerrno != CL_NOERR) {
		goto cleanup;
	}

	//
	// scslm_read_table_ccr() has started the transaction
	// to update the CCR table. If something goes wrong,
	// we need to abort the transaction so updating the
	// ccr table for the next instance will succeed.
	// However if it gets here, nothing is wrong and
	// it should  not abort.
	//
	abort = false;

	// Update the ccr
	scslm_err = scslmthresh_update_table_ccr(SCSLMTHRESH_TABLE,
	    ccr_thresholds, create_table);
	if (scslm_err != SCSLM_NOERR) {
		clcommand_perror(CL_EINTERNAL);
		clerrno = CL_EINTERNAL;
		goto cleanup;
	}

	if (optflgs & vflg) {
		if (is_empty(value)) {
			if (node) {
				clmessage("Threshold <%s, %s> was removed "
				    "for attribute \"%s\" on instance "
				    "\"%s\" on node \"%s\".\n",
				    direction, severity, ki_name,
				    instance, node);
			} else {
				clmessage("Threshold <%s, %s> was removed "
				    "for attribute \"%s\" on instance "
				    "\"%s\".\n",
				    direction, severity, ki_name,
				    instance);
			}
		} else if (is_empty(rearm)) {
			if (node) {
				clmessage("Threshold <%s, %s, %s> was set "
				    "for attribute \"%s\" on instance "
				    "\"%s\" on node \"%s\".\n",
				    direction, severity, value,
				    ki_name, instance, node);
			} else {
				clmessage("Threshold <%s, %s, %s> was set "
				    "for attribute \"%s\" on instance "
				    "\"%s\".\n",
				    direction, severity, value,
				    ki_name, instance);
			}
		} else if (node) {
			clmessage("Threshold <%s, %s, %s, %s> was set "
			    "for attribute \"%s\" on instance "
			    "\"%s\" on node \"%s\".\n",
			    direction, severity, value, rearm,
			    ki_name, instance, node);
		} else {
			clmessage("Threshold <%s, %s, %s, %s> was set "
			    "for instance \"%s\" on attribute "
			    "\"%s\".\n",
			    direction, severity, value, rearm,
			    instance, ki_name);
		}
	}

	// Post an event
	(void) sc_publish_event(ESC_CLUSTER_SLM_CONFIG_CHANGE,
	    CL_EVENT_PUB_SLM,
	    CL_EVENT_SEV_INFO,
	    CL_EVENT_INIT_SYSTEM, 0, 0, 0,
	    CL_CONFIG_ACTION,
	    SE_DATA_TYPE_UINT32, CL_EVENT_CONFIG_PROP_CHANGED,
	    CL_CCR_TABLE_NAME, SE_DATA_TYPE_STRING, SCSLMTHRESH_TABLE,
	    NULL);

cleanup:
	if (ccr_thresholds) {
		(void) threshold_list_free(&ccr_thresholds);
	}
	if (new_thresh && *new_thresh && need_free) {
		threshold_free(&new_thresh);
	}

	// Abort the transaction
	if (abort) {
		scslm_abort_update_table_ccr(SCSLMTHRESH_TABLE);
	}

	return (clerrno);
}

//
// is_empty
//
//	Return true is the given string is empty, false if it is set.
//
bool
is_empty(const char *p_str)
{
	if (!p_str || (*p_str == '\0')) {
		return (true);
	}

	return (false);
}

//
// clta_save_threshold_list
//
//	Called when read the threshold CCR table to save the thesholds
//	into ccr_thresholds.
//
scslm_error_t
clta_save_threshold_list(const char *thresh_key,
    char *thresh_value, void *ccr_thresholds)
{
	uint_t id_in_ccr = 0;
	uint_t ccr_field;
	scslm_error_t scslm_errno = SCSLM_OK;
	threshold_t *ccr_threshold = NULL;
	threshold_list_t **test_ptr = (threshold_list_t **)ccr_thresholds;

	// extarct the threshold id and the field
	scslm_errno = threshold_parse_ccr_key(thresh_key, &id_in_ccr,
	    &ccr_field);
	if (scslm_errno != SCSLM_OK) {
		return (scslm_errno);
	}

	// Use the id to get the threshold data
	ccr_threshold = threshold_list_extract(*test_ptr, id_in_ccr);
	if (ccr_threshold == NULL) {
		ccr_threshold = threshold_allocate();
		if (!ccr_threshold) {
			return (SCSLM_ENOMEM);
		}

		// Insert this threshold
		scslm_errno = threshold_list_insert(
		    (threshold_list_t **)ccr_thresholds,
		    id_in_ccr, ccr_threshold);
		if (scslm_errno != SCSLM_OK) {
			return (scslm_errno);
		}
	}

	// Set the threshold field
	scslm_errno = threshold_set_field(ccr_threshold, ccr_field,
	    thresh_value);
	return (scslm_errno);
}

//
// node_is_valid
//
//	Check is the given node is in the valid node list. Return true
//	if it is; return false if not.
//
bool
node_is_valid(NameValueList &valid_nodes, const char *node_name)
{
	if (!node_name || (valid_nodes.getSize() == 0)) {
		return (false);
	}

	for (valid_nodes.start(); !valid_nodes.end(); valid_nodes.next()) {
		if (strcmp(node_name, valid_nodes.getName()) == 0) {
			return (true);
		}
	}

	return (false);
}

//
// clta_threshold_set_field
//
//	Set the value of a field of a threshold using the short format
//	of motype and KI names.
//
scslm_error_t
clta_threshold_set_field(threshold_t *ccr_threshold,
    uint_t ccr_field, const char *thresh_value)
{
	const char *short_name;

	if (!ccr_threshold || (ccr_field >= THRESH_NB_FIELDS)) {
		return (SCSLM_EINTERNAL);
	}

	free((*ccr_threshold)[ccr_field]);

	// if the given string is empty, store the EMPTY_STRING instead
	if (!thresh_value || (*thresh_value == '\0')) {
		(*ccr_threshold)[ccr_field] = strdup(EMPTY_VALUE);
	} else {
		if (ccr_field == THRESH_MOTYPE) {
			short_name = make_short_mot(thresh_value);
			(*ccr_threshold)[ccr_field] = strdup(short_name);
		} else if (ccr_field == THRESH_KINAME) {
			short_name = make_short_ki(thresh_value);
			(*ccr_threshold)[ccr_field] = strdup(short_name);
		} else {
			(*ccr_threshold)[ccr_field] = strdup(thresh_value);
		}
	}

	if ((*ccr_threshold)[ccr_field] == NULL) {
		return (SCSLM_ENOMEM);
	}

	return (SCSLM_OK);
}
