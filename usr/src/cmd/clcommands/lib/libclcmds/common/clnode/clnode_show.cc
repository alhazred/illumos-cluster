//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clnode_show.cc	1.9	08/07/25 SMI"

//
// Process clnode "show"
//

#include "clnode.h"
#include "ClShow.h"
#include "scconf_private.h"
#include "libscdpm.h"
#if (SOL_VERSION >= __s10)
#include <libzccfg/libzccfg.h>
#endif

static clerrno_t
clnode_get_server_conf(char *nodename, scconf_cfg_cluster_t *clconf,
    ValueList &prop_list, NameValueList &node_conf,
    ClShow *clnode_show_child, optflgs_t optflgs, bool *found_zones);
static clerrno_t
clnode_get_farm_conf(char *nodename, scconf_farm_nodelist_t *farm_nodelist,
    ValueList &prop_list, NameValueList &node_conf, optflgs_t optflgs);
static clerrno_t
clnode_get_show(ValueList &nodenames, optflgs_t optflgs, ValueList &nodetypes,
    ValueList &prop_list, ClShow *clnode_cshow, ClShow *clnode_fshow,
    bool *print_cshow, bool *print_fshow, ValueList &zc_names,
    ClShow *clnode_zcshow, bool *print_zcshow);
static clerrno_t
clnode_get_dpm_autoreboot(scconf_nodeid_t nodeid, boolean_t *auto_reboot);
#if (SOL_VERSION >= __s10)
static clerrno_t
clnode_get_zc_node_conf(char *nodename, ValueList &prop_list,
    NameValueList &node_conf, optflgs_t optflgs);
#endif

//
// clnode "show"
//
clerrno_t
clnode_show(ClCommand &cmd, ValueList &operands, optflgs_t optflgs,
    ValueList &nodetypes, ValueList &prop_list, ValueList &zc_names)
{
	ClShow *prt_show_cnode = new ClShow(HEAD_NODES);
	ClShow *prt_show_fnode = new ClShow(HEAD_FNODES);
	ClShow *prt_show_zc_node = new ClShow(HEAD_ZC_NODES);
	clerrno_t clerrno = CL_NOERR;
	bool print_cshow, print_fshow, print_zcshow;

	//
	// Get the ClShow objects. Error messages are already printed
	// in clnode_get_show().
	//
	clerrno = clnode_get_show(operands, optflgs,
	    nodetypes, prop_list, prt_show_cnode, prt_show_fnode,
	    &print_cshow, &print_fshow, zc_names, prt_show_zc_node,
	    &print_zcshow);

	// Print cluster node
	if (print_cshow) {
		(void) prt_show_cnode->print();
	}

	// Print farm node
	if (print_fshow) {
		(void) prt_show_fnode->print();
	}

	// Print zone cluster node
	if (print_zcshow) {
		(void) prt_show_zc_node->print();
	}
	// Deletet the object
	delete prt_show_cnode;
	delete prt_show_fnode;
	delete prt_show_zc_node;

	return (clerrno);
} //lint !e715

//
// get_clnode_show_obj
//
//	Get the ClChow object for all the cluster nodes for printing. The
//	ClShow object must be created before calling this function.
//
clerrno_t
get_clnode_show_obj(optflgs_t optflgs, ClShow *clnode_cshow,
    ClShow *clnode_fshow)
{
	ValueList empty_list;
	bool print_cshow, print_fshow, print_zcshow;
	ClShow *prt_show_zc_node = new ClShow(HEAD_ZC_NODES);
	clerrno_t cl_errno = CL_NOERR;
	ValueList zc_names = ValueList();

	cl_errno = clnode_get_show(empty_list, optflgs, empty_list, empty_list,
	    clnode_cshow, clnode_fshow, &print_cshow, &print_fshow, zc_names,
	    prt_show_zc_node, &print_zcshow);

	delete prt_show_zc_node;
	return (cl_errno);
}

//
// clnode_get_show
//
//	Get the ClShow objects based on the nodenames and nodetype.
//	If these lists are empty, get all the cluster nodes.
//	ClShow object must be created before calling this
//	function, and the caller is responsible to free the
//	object afterwards.
//
clerrno_t
clnode_get_show(ValueList &nodenames, optflgs_t optflgs, ValueList &nodetypes,
    ValueList &prop_list, ClShow *clnode_cshow, ClShow *clnode_fshow,
    bool *print_cshow, bool *print_fshow, ValueList &zc_names,
    ClShow *clnode_zcshow, bool *print_zcshow)
{
	NameValueList node_list = NameValueList(true);
	ValueList vprop_list = ValueList(true);
	NameValueList *node_conf;
	NameValueList show_qdev_name, show_node_name;
	clerrno_t clerrno = CL_NOERR;
	clerrno_t first_err = CL_NOERR;
	scconf_errno_t scconf_err = SCCONF_NOERR;
	scconf_cfg_cluster_t *clconf = (scconf_cfg_cluster_t *)0;
	scconf_cfg_node_t *servernodes = (scconf_cfg_node_t *)0;
	char *node_name;
	char *node_type;
	boolean_t iseuropa = B_FALSE;
	scconf_farm_nodelist_t *farm_nodelist = (scconf_farm_nodelist_t *)0;

	// Check inputs
	if (!clnode_cshow || !clnode_fshow) {
		clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}

	*print_cshow = *print_fshow = *print_zcshow = false;

	// Get the valid nodes from the user inputs
	clerrno = clnode_preprocess_nodelist(nodenames, nodetypes,
	    node_list, true, zc_names);
	if (clerrno != CL_NOERR) {
		if (first_err == CL_NOERR) {
			first_err = clerrno;
		}
	} else {
		// Is Europa configured?
		if ((clnode_check_europa(&iseuropa) == CL_NOERR) &&
		    iseuropa) {
			if (nodetypes.isValue(NODETYPE_FARM)) {
				*print_fshow = true;
			}
		}

		// Is cluster node specified?
		if (nodetypes.isValue(NODETYPE_SERVER)) {
			*print_cshow = true;
		}
		if (nodetypes.isValue(NODETYPE_ZONE_CLUSTER)) {
			*print_zcshow = true;
		}
	}

	// Nothing to show
	if (node_list.getSize() == 0) {
		goto cleanup;
	}

	// Filer out by the user specified properties
	for (prop_list.start(); !prop_list.end(); prop_list.next()) {
		// Check with the node type
		int found = 0;
		char *prop_name = prop_list.getValue();

		if (!prop_name)
			continue;

		// Go through the node list.
		for (node_list.start(); !node_list.end(); node_list.next()) {
			node_type = node_list.getValue();
			if (!node_type)
				continue;

			if (clnode_valid_prop(prop_name, node_type)) {
				(void) vprop_list.add(prop_name);
				found = 1;
				break;
			}
		}

		// Not found?
		if (!found) {
			clerror("Invalid property \"%s\".\n",
			    prop_name);

			if (first_err == CL_NOERR) {
				first_err = CL_EPROP;
			}
		}
	}

	// No valid properties?
	if ((prop_list.getSize() != 0) && (vprop_list.getSize() == 0)) {
		*print_cshow = *print_fshow = false;
		goto cleanup;
	}

	// Get cluster configuration for server nodes
	scconf_err = scconf_get_clusterconfig(&clconf);
	if (scconf_err != SCCONF_NOERR) {
		clerrno = clnode_conv_scconf_err(1, scconf_err);
		if (first_err == CL_NOERR) {
			first_err = clerrno;
		}
		goto cleanup;
	}

	// Get farm nodes
	if (iseuropa) {
		clerrno = clnode_open_europa();
		if (clerrno != CL_NOERR) {
			if (first_err == CL_NOERR) {
				first_err = clerrno;
			}
		} else {
			// Get all the farm nodes
			scconf_err = scconf_get_farmnodes(&farm_nodelist);
			if (scconf_err != SCCONF_NOERR) {
				clerrno = clnode_conv_scconf_err(1, scconf_err);
				if (first_err == CL_NOERR) {
					first_err = clerrno;
				}
			}

			(void) clnode_close_europa();
		}
	}

	// Process the nodes
	for (node_list.start(); node_list.end() != 1; node_list.next()) {
		ClShow *clnode_show_child = (ClShow *)0;
		bool found_zone = false;
		node_name = node_list.getName();
		node_type = node_list.getValue();
		if (!node_name || !node_type)
			continue;

		// These NameValueList is deleted by "delete ClShow"
		node_conf = new NameValueList;

		if (strcmp(node_type, NODETYPE_SERVER) == 0) {
			clnode_show_child = new ClShow(SUBHEAD_ZONES,
			    node_name);
			clerrno = clnode_get_server_conf(node_name,
			    clconf, vprop_list, *node_conf, clnode_show_child,
			    optflgs, &found_zone);
		} else if ((strcmp(node_type, NODETYPE_FARM) == 0) &&
		    iseuropa && farm_nodelist) {
			clerrno = clnode_get_farm_conf(node_name,
			    farm_nodelist, vprop_list, *node_conf,
			    optflgs);
#if (SOL_VERSION >= __s10)
		} else if (strcmp(node_type, NODETYPE_ZONE_CLUSTER) == 0) {
			clerrno = clnode_get_zc_node_conf(node_name,
					vprop_list, *node_conf, optflgs);
#endif
		} else {
			clerrno = CL_EINTERNAL;
		}
		if (clerrno != CL_NOERR) {
			clcommand_perror(clerrno);
			if (first_err == CL_NOERR) {
				first_err = clerrno;
			}
			continue;
		}

		// Set the node as an object to print
		if (strcmp(node_type, NODETYPE_SERVER) == 0) {
			if (node_conf->getSize() > 0) {
				clnode_cshow->addObject(
				    (char *)gettext("Node Name"),
				    node_name);
				*print_cshow = true;
				clnode_cshow->addProperties(node_name,
				    node_conf);
			}
#if (SOL_VERSION >= __s10)
		} else if (strcmp(node_type, NODETYPE_ZONE_CLUSTER) == 0) {
			if (node_conf->getSize() > 0) {
				clnode_zcshow->addObject(
				    (char *)gettext("Node Name"),
				    node_name);
				*print_zcshow = true;
				clnode_zcshow->addProperties(node_name,
				    node_conf);
			}
#endif
		} else {
			if (node_conf->getSize() > 0) {
				clnode_fshow->addObject(
				    (char *)gettext("Node Name"),
				    node_name);
				*print_fshow = true;
				clnode_fshow->addProperties(node_name,
				    node_conf);
			}
		}

		// Add children
		if (found_zone &&
		    (strcmp(node_type, NODETYPE_SERVER) == 0)) {
			(void) clnode_cshow->addChild(node_name,
			    clnode_show_child);
		}
	}

cleanup:
	// Release the cluster config
	scconf_free_clusterconfig(clconf);

	if (farm_nodelist) {
		scconf_free_farm_nodelist(farm_nodelist);
	}

	if (servernodes) {
		conf_free_clusternodes(servernodes);
	}

	// Return the first error
	return (first_err);
}

//
// clnode_get_server_conf
//
//	Get the configuration data of the node. The confgiuration
//	data is saved into the NameValueList.
//
clerrno_t
clnode_get_server_conf(char *nodename, scconf_cfg_cluster_t *clconf,
    ValueList &prop_list, NameValueList &node_conf,
    ClShow *clnode_show_child, optflgs_t optflgs, bool *found_zone)
{
	scconf_errno_t scconf_err = SCCONF_NOERR;
	clerrno_t clerrno = CL_NOERR;
	clerrno_t first_err = CL_NOERR;
	scconf_cfg_node_t *cluster_nodes;
	scconf_cfg_node_t *cluster_node;
	char conf_buf[SCCONF_MAXSTRINGLEN];
	scconf_cfg_cltr_adap_t *cltr_adapters;
	scconf_cfg_cltr_adap_t *cltr_adapter;
	const char *conf_name;
	bool all_prop = (prop_list.getSize() == 0) ? true : false;
	int i;
	bool found;
	char *zcname, *node_name;
	boolean_t zc_flag = B_FALSE;

	// Check inputs
	if ((clconf == NULL) || !nodename || !found_zone)
		return (CL_EINTERNAL);

	zcname = node_name = NULL;
#if (SOL_VERSION >= __s10)
	// We will use "zc_flag" to determine whether we are inside
	// a zone cluster or not.
	scconf_err = scconf_zone_cluster_check(&zc_flag);
	if (scconf_err != SCCONF_NOERR) {
		return (CL_EINTERNAL);
	}
#endif
	// Go through the nodes
	cluster_nodes = clconf->scconf_cluster_nodelist;
	for (cluster_node = cluster_nodes; cluster_node;
	    cluster_node = cluster_node->scconf_node_next) {
		// First we need to parse the node name from cluster
		// scope
		if (zcname) {
			free(zcname);
			zcname = NULL;
		}
		if (node_name) {
			free(node_name);
			node_name = NULL;
		}

		scconf_err = scconf_parse_obj_name_from_cluster_scope(
					nodename, &node_name, &zcname);

		if (scconf_err != SCCONF_NOERR) {
			if (first_err == CL_NOERR) {
				first_err = CL_EINTERNAL;
			}
			continue;
		}

		// Check the node name
		if (!cluster_node->scconf_node_nodename ||
		    (strcmp(cluster_node->scconf_node_nodename,
		    node_name) != 0)) {
			continue;
		}

		if (all_prop) {
			// Add the node id
			(void) sprintf(conf_buf, "%d",
			    cluster_node->scconf_node_nodeid);
			(void) node_conf.add((char *)gettext("Node ID"),
			    conf_buf);

			// Add the state
			(void) node_conf.add((char *)gettext("Enabled"),
			    ((cluster_node->scconf_node_nodestate ==
			    SCCONF_STATE_ENABLED) ?
			    (char *)gettext("yes") : (char *)gettext("no")));
		}

		if (zc_flag == B_TRUE) {
			// If we inside a zone cluster, then none of the
			// following properties make sense.
			break;
		}
		// Add other properties
		found = false;

		// Go through the properties
		for (i = 0; clnode_props[i].cli_name; i++) {
			// Skip farm nodes
			if (strcmp(clnode_props[i].node_type,
			    NODETYPE_FARM) == 0) {
				continue;
			}

			// Is it specified?
			if (!all_prop &&
			    !prop_list.isValue(clnode_props[i].cli_name)) {
				continue;
			}

			// Get the property
			if (strcmp(clnode_props[i].cli_name,
			    NODEPROP_PRIVHOST) == 0) {
				(void) node_conf.add(
				    clnode_props[i].cli_name,
				    cluster_node->scconf_node_privatehostname);
				found = true;
			} else if ((strcmp(clnode_props[i].cli_name,
			    NODEPROP_ZONESHARE) == 0) &&
			    cluster_node->scslm_global_zone_shares) {
				(void) sprintf(conf_buf, "%d",
				    cluster_node->scslm_global_zone_shares);
				(void) node_conf.add(
				    clnode_props[i].cli_name, conf_buf);
				found = true;
			} else if ((strcmp(clnode_props[i].cli_name,
			    NODEPROP_PSETMIN) == 0) &&
			    cluster_node->scslm_default_pset_min) {
				(void) sprintf(conf_buf, "%d",
				    cluster_node->scslm_default_pset_min);
				(void) node_conf.add(
				    clnode_props[i].cli_name, conf_buf);
				found = true;
			} else if (strcmp(clnode_props[i].cli_name,
			    PROP_NODE_QUORUM_VOTE) == 0) {
				(void) sprintf(conf_buf, "%d",
				    cluster_node->scconf_node_qvotes);
				(void) node_conf.add(
				    clnode_props[i].cli_name, conf_buf);
				found = true;
			} else if (strcmp(clnode_props[i].cli_name,
			    PROP_NODE_QUORUM_DEFAULTVOTE) == 0) {
				(void) sprintf(conf_buf, "%d",
				    cluster_node->scconf_node_qdefaultvotes);
				(void) node_conf.add(
				    clnode_props[i].cli_name, conf_buf);
				found = true;
			} else if ((strcmp(clnode_props[i].cli_name,
			    PROP_NODE_QUORUM_RESV_KEY) == 0) &&
			    cluster_node->scconf_node_qreskey) {
				(void) node_conf.add(
				    clnode_props[i].cli_name,
				    cluster_node->scconf_node_qreskey);
				found = true;
			} else if (strcmp(clnode_props[i].cli_name,
			    NODEPROP_FAILOVER_DPM) == 0) {
				boolean_t auto_reboot;

				// Special processing
				clerrno = clnode_get_dpm_autoreboot(
				    cluster_node->scconf_node_nodeid,
				    &auto_reboot);
				if (clerrno == CL_NOERR) {
					(void) node_conf.add(
					clnode_props[i].cli_name,
					auto_reboot ?
					(char *)gettext("enabled") :
					(char *)gettext("disabled"));
					found = true;
				} else if (first_err == CL_NOERR) {
					// Errors are already printed
					first_err = clerrno;
				}
			}
		}

		if (!found && !all_prop) {
			// Don't add any properties
			node_conf.clear();
			break;
		}

		if (!all_prop) {
			break;
		}

		// Add the adapters
		cltr_adapters = cluster_node->scconf_node_adapterlist;
		if (cltr_adapters) {
			bool is_first = true;
			*conf_buf = '\0';
			for (cltr_adapter = cltr_adapters;  cltr_adapter;
			    cltr_adapter = cltr_adapter->scconf_adap_next) {
				if (!cltr_adapter->scconf_adap_adaptername) {
					continue;
				}
				if (is_first) {
					is_first = false;
				} else {
					(void) strcat(conf_buf, ", ");
				}
				(void) strcat(conf_buf,
				    cltr_adapter->scconf_adap_adaptername);
			}
			(void) node_conf.add(
			    (char *)gettext("Transport Adapter List"),
			    conf_buf);
		}

		// Get zones
#if SOL_VERSION >= __s10
		scconf_zonelist_t *zonelist = (scconf_zonelist_t *)0;
		scconf_zonelist_t *zones;

		scconf_err = scconf_get_node_zones(
		    cluster_node->scconf_node_nodeid, &zonelist);
		if (scconf_err == SCCONF_NOERR) {
			clerrno = clnode_conv_scconf_err(NULL, scconf_err);
			if (first_err == CL_NOERR) {
				first_err = clerrno;
			}
		}

		if (zonelist) {
			bool is_first = true;
			*conf_buf = '\0';

			for (zones = zonelist; zones;
			    zones = zones->scconf_zonelist_next) {
				if (!zones->scconf_zone_name) {
					continue;
				}
				if (is_first) {
					is_first = false;
				} else {
					(void) strcat(conf_buf, ", ");
				}
				(void) strcat(conf_buf,
				    zones->scconf_zone_name);

				// Compose the child clshow
				clnode_show_child->addObject((char *)gettext(
				    "Zone Name"), zones->scconf_zone_name);
				*found_zone = true;
				if (zones->scconf_zone_privhostname) {
					clnode_show_child->addProperty(
					    zones->scconf_zone_name,
					    NODEPROP_ZPRIVHOST,
					    zones->scconf_zone_privhostname);
				} else {
					clnode_show_child->addProperty(
					    zones->scconf_zone_name,
					    NODEPROP_ZPRIVHOST,
					    "<NULL>");
				}
			}
			(void) node_conf.add(
			    (char *)gettext("Node Zones"), conf_buf);

			scconf_free_zonelist(zonelist);
		}
#endif
		break;
	}

	if (first_err != CL_NOERR) {
		clerrno = first_err;
	}
	if (zcname) {
		free(zcname);
	}
	if (node_name) {
		free(node_name);
	}

	return (clerrno);
}

//
// clnode_get_dpm_autoreboot
//
//	Get the setting of reboot_on_path_failure on the given nodeid.
//
clerrno_t
clnode_get_dpm_autoreboot(scconf_nodeid_t nodeid, boolean_t *auto_reboot)
{
	clerrno_t clerrno = CL_NOERR;
	boolean_t autorebootlist[NODEID_MAX];

	// Check input
	if ((nodeid == 0) || (nodeid >= NODEID_MAX)) {
		clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}

	// Initialize
	if (dpm_initialize() != 0) {
		clerror("Cannot initialize ORB.\n");
		return (CL_EINTERNAL);
	}
	if (dpmccr_initialize(NULL) != 0) {
		clerror("Cannot initialize CCR.\n");
		return (CL_EINTERNAL);
	}

	// Get the auto reboot state from the CCR
	if (libdpm_get_all_auto_reboot_state(autorebootlist) != 0) {
		clerror("Failed to read \"%s\" from the cluster "
		    "configuration.\n", NODEPROP_FAILOVER_DPM);
		return (CL_EINTERNAL);
	}

	*auto_reboot = autorebootlist[nodeid - 1];

	return (clerrno);
}

//
// clnode_get_farm_conf
//
//	Get the configuration of the given farm node, and save it
//	to the NameValueList.
//
clerrno_t
clnode_get_farm_conf(char *nodename, scconf_farm_nodelist_t *farm_nodelist,
    ValueList &prop_list, NameValueList &node_conf, optflgs_t optflgs)
{
	scconf_farm_nodelist_t *farm_node;
	clerrno_t clerrno = CL_NOERR;
	bool all_prop = (prop_list.getSize() == 0) ? true : false;
	char conf_buf[SCCONF_MAXSTRINGLEN];
	int i;

	// Check input
	if (!nodename || !farm_nodelist) {
		return (CL_EINTERNAL);
	}

	// Go through the farm nodes
	for (farm_node = farm_nodelist; farm_node;
	    farm_node = farm_node->scconf_node_next) {
		// Check the node name
		if (!farm_node->scconf_node_nodename ||
		    (strcmp(farm_node->scconf_node_nodename, nodename) != 0)) {
			continue;
		}

		// Add properties
		if (all_prop) {
			// Add the node id
			(void) sprintf(conf_buf, "%d",
			    farm_node->scconf_node_nodeid);
			(void) node_conf.add((char *)gettext("Node ID"),
			    conf_buf);
		}

		// Add other properties
		for (i = 0; clnode_props[i].cli_name; i++) {
			// Skip server nodes
			if (strcmp(clnode_props[i].node_type,
			    NODETYPE_SERVER) == 0) {
				continue;
			}

			// Is it specified?
			if (!all_prop &&
			    !prop_list.isValue(clnode_props[i].cli_name)) {
				continue;
			}

			// Add the property
			if (strcmp(clnode_props[i].cli_name,
			    NODEPROP_MONITOR) == 0) {
				if (farm_node->scconf_node_monstate ==
				    SCCONF_STATE_ENABLED) {
					(void) node_conf.add(
					    clnode_props[i].cli_name,
					    "enabled");
				} else {
					(void) node_conf.add(
					    clnode_props[i].cli_name,
					    "disabled");
				}
			}
		}

		// Add adapters
		if (all_prop) {
			if (farm_node->scconf_node_adapters) {
				(void) node_conf.add(
				    (char *)gettext("Transport Adapter List"),
				    farm_node->scconf_node_adapters);
			}
		}
		break;
	}

	return (clerrno);
}

#if (SOL_VERSION >= __s10)
//
// clnode_get_zc_node_conf
//
//	Get the configuration of the given ZC node, and save it
//	to the NameValueList.
//
clerrno_t
clnode_get_zc_node_conf(char *nodename, ValueList &prop_list,
    NameValueList &node_conf, optflgs_t optflgs)
{

	scconf_errno_t scconf_err = SCCONF_NOERR;
	char *zcname, *zone_hostname;
	clerrno_t cl_errno = CL_NOERR;
	int zc_cfg_err = ZC_OK;
	struct zc_nodetab nodetab;
	zc_dochandle_t zone_handle;
	boolean_t found_zone = B_FALSE;

	zcname = zone_hostname = NULL;

	if (!nodename) {
		return (CL_EINTERNAL);
	}

	// First parse the ZC name from cluster scoping
	zcname = zone_hostname = NULL;
	scconf_err = scconf_parse_obj_name_from_cluster_scope(nodename,
				&zone_hostname, &zcname);
	if (scconf_err != SCCONF_NOERR) {
		cl_errno = CL_EINTERNAL;
		goto finish_zc_node;
	}

	// First we have to find the node-id of the zone-hostname
	// and then invoke the corresponding libzccfg method to
	// fetch the properties of a zone cluster node.
	// First open the zonecfg handle
	zone_handle = zccfg_init_handle();
	zc_cfg_err = zccfg_get_handle(zcname, zone_handle);

	if (zc_cfg_err != ZC_OK) {
		cl_errno = CL_ENOENT;
		goto finish_zc_node;
	}

	// Go to the nodetab
	zc_cfg_err = zccfg_setnodeent(zone_handle);

	if (zc_cfg_err != ZC_OK) {
		zccfg_fini_handle(zone_handle);
		cl_errno = CL_EINTERNAL;
		goto finish_zc_node;
	}

	// Get the nodelist for this zone cluster
	while (zccfg_getnodeent(zone_handle, &nodetab)
			== ZC_OK) {
		if (strcmp(zone_hostname, nodetab.zc_hostname)) {
			continue;
		}

		// If we are here, it means this is the zone cluster-node
		// we were looking for.
		found_zone = B_TRUE;
		// Add physical host if it was specified in the prop list
		if ((prop_list.getSize() < 1) ||
			(prop_list.isValue(NODEPROP_PHYS_HOST))) {

			(void) node_conf.add(NODEPROP_PHYS_HOST,
						nodetab.zc_nodename);
		}
	}

	if (found_zone == B_FALSE) {
		cl_errno = CL_ENOENT;
	}

	(void) zccfg_endnodeent(zone_handle);
	// Close the zonecfg handle
	(void) zccfg_fini_handle(zone_handle);

finish_zc_node:
	if (zcname) {
		free(zcname);
	}
	if (zone_hostname) {
		free(zone_hostname);
	}

	return (cl_errno);
}
#endif
