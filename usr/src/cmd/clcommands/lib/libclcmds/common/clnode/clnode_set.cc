//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clnode_set.cc	1.12	08/05/20 SMI"

//
// Process clnode "set"
//

#include "clnode.h"
#include <sys/fss.h>
#ifndef FSS_MAXSHARES
#define	BUG_6541238
#define	FSS_MAXSHARES   65535 /* XXX from sys/fss.h */
#endif
#if SOL_VERSION >= __s10
#include "privip/privip_map.h"
#endif
#include "libscdpm.h"

#define	FLG_READ	1
#define	FLG_WRITE	2
#define	FLG_INVALID	3

// The default value of NODEPROP_PSETMIN
#define	DFT_PSETMIN	1

static int
clnode_get_prop_attr(char *prop_name, char *conf_pname);
static clerrno_t
clnode_set_privatehost(char *node_name, char *priv_host, optflgs_t optflgs);
static clerrno_t
clnode_set_zprivatehost(char *node_name, char *zpriv_host, optflgs_t optflgs);
static clerrno_t
clnode_set_dpm_autoreboot(char *node_name, char *prop_val, optflgs_t optflgs);
static clerrno_t
clnode_set_scslm_props(char *node_name, char *prop_name,
    char *prop_val, optflgs_t optflgs);
static clerrno_t clnode_scslm_check_values(char **argv);

//
// clnode "set"
//
clerrno_t
clnode_set(ClCommand &cmd, ValueList &operands, optflgs_t optflgs,
    ValueList &nodetypes, NameValueList &properties)
{
	scconf_errno_t scconf_err = SCCONF_NOERR;
	clerrno_t clerrno = CL_NOERR;
	clerrno_t first_err = CL_NOERR;
	NameValueList node_list;
	char *this_node, *this_type;
	char *prop_name, *prop_val;
	ValueList node_operands = ValueList(true);
	ValueList zone_operands = ValueList(true);
	ValueList valid_zone_list = ValueList(true);
	NameValueList valid_props = NameValueList(true);
	char *zone_name = (char *)0;
	char *node_name = (char *)0;
	boolean_t iseuropa = B_FALSE;
	NameValueList farmnodes = NameValueList(true);
	uint_t farm_state;
	bool europa_err = false;

	// Check inputs
	if (properties.getSize() == 0) {
		clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}

	// Get the proper operands
	for (operands.start(); !operands.end(); operands.next()) {
		this_node = operands.getValue();
		if (!this_node) {
			continue;
		}

		node_name = strdup(this_node);
		if (!node_name) {
			clcommand_perror(CL_ENOMEM);
			if (first_err == CL_NOERR) {
				first_err = CL_ENOMEM;
			}
			goto cleanup;
		}

		// Check the zone name
		zone_name = strchr(node_name, ':');
		if (zone_name) {
			*zone_name++ = '\0';
			if ((strlen(zone_name) == 0) ||
			    (strlen(node_name) == 0)) {
				clerror("Invalid node \"%s\".\n",
				    this_node);
				if (first_err == CL_NOERR) {
					first_err = CL_EINVAL;
				}
				free(node_name);
				continue;
			}
			zone_operands.add(this_node);
		} else {
			node_operands.add(this_node);
		}
		free(node_name);
	}

	// Get the valid non-zone nodes
	if ((operands.getSize() == 0) || (node_operands.getSize() != 0)) {
		clerrno = clnode_preprocess_nodelist(node_operands, nodetypes,
		    node_list, true);
		if ((clerrno != CL_NOERR) && (first_err == CL_NOERR)) {
			first_err = clerrno;
		}
	}

	// Get the valid zone nodes
	if (zone_operands.getSize() > 0) {
		ValueList tmp_input_list;
		NameValueList tmp_node_list;

		// Go through the zone nodes
		for (zone_operands.start(); !zone_operands.end();
		    zone_operands.next()) {
			this_node = zone_operands.getValue();
			if (!this_node)
				continue;

			// Get the node name by removing the zone name.
			zone_name = strdup(this_node);
			if (!zone_name) {
				clcommand_perror(CL_ENOMEM);
				if (first_err == CL_NOERR) {
					first_err = CL_ENOMEM;
				}
				goto cleanup;
			}

			node_name = strtok(zone_name, ":");
			if (!node_name) {
				free(zone_name);
				continue;
			}

			tmp_input_list.add(node_name);
			clerrno = clnode_preprocess_nodelist(tmp_input_list,
			    nodetypes, tmp_node_list, true);
			if ((clerrno != CL_NOERR) &&
			    (first_err == CL_NOERR)) {
				first_err = clerrno;
			}

			if (tmp_node_list.getSize() != 0) {
				valid_zone_list.add(this_node);
			}
			tmp_input_list.clear();
			tmp_node_list.clear();
		}
	}

	// Save the property list for zone operands
	if (node_list.getSize() == 0) {
		// Go through the properties
		for (properties.start(); !properties.end(); properties.next()) {
			// Get property name and value
			prop_name = properties.getName();
			prop_val = properties.getValue();
			if (!prop_name)
				continue;

			(void) valid_props.add(prop_name,
			    (prop_val) ? prop_val : "");
		}
	}

	// Go through the nodes
	for (node_list.start(); !node_list.end(); node_list.next()) {
		this_node = node_list.getName();
		this_type = node_list.getValue();
		if (!this_node || !this_type)
			continue;

		// Go through the properties
		for (properties.start(); !properties.end(); properties.next()) {
			// Get property name and value
			prop_name = properties.getName();
			prop_val = properties.getValue();
			if (!prop_name)
				continue;

			// Check the property with the node type
			if (strcmp(prop_name, NODEPROP_MONITOR) == 0) {
				if (strcmp(this_type, NODETYPE_FARM) != 0) {
					clerror("Property \"%s\" does not "
					    "apply to \"%s\" node \"%s\".\n",
					    prop_name, this_type, this_node);
					if (first_err == CL_NOERR) {
						first_err = CL_EOP;
					}
					continue;
				}

				// Check the property value
				if (!prop_val || (*prop_val == '\0')) {
					if (!europa_err) {
						europa_err = true;
						clerror("You must specify a "
						    "value for \"%s\".\n",
						    NODEPROP_MONITOR);
					}
					if (first_err == CL_NOERR) {
						first_err = CL_EPROP;
					}
					continue;
				}

				if (strcasecmp(prop_val, MONITOR_DISABLED)
				    == 0) {
					farm_state = SET_UNMONITOR;
				} else if (strcasecmp(prop_val,
				    MONITOR_ENABLED) == 0) {
					farm_state = SET_MONITOR;
				} else if (!europa_err) {
					europa_err = true;
					clerror("Invalid value \"%s\" "
					    "for \"%s\".\n",
					    prop_val, NODEPROP_MONITOR);
					if (first_err == CL_NOERR) {
						first_err = CL_EPROP;
					}
					continue;
				}
				farmnodes.add(this_node, NODETYPE_FARM);
				(void) valid_props.add(prop_name,
				    (prop_val) ? prop_val : "");
			} else if (strcmp(this_type, NODETYPE_SERVER) != 0) {
				clerror("Property \"%s\" does not "
				    "apply to \"%s\" node \"%s\".\n",
				    prop_name, this_type, this_node);
				if (first_err == CL_NOERR) {
					first_err = CL_EOP;
				}
				continue;
			}

			// Call different functions per the property name
			if (strcmp(prop_name, NODEPROP_PRIVHOST) == 0) {
				clerrno = clnode_set_privatehost(this_node,
				    prop_val, optflgs);
				if ((clerrno != CL_NOERR) &&
				    (first_err == CL_NOERR)) {
					first_err = clerrno;
				}
				(void) valid_props.add(prop_name,
				    (prop_val) ? prop_val : "");
			} else if (strcmp(prop_name, NODEPROP_ZPRIVHOST) == 0) {
				// Cannot apply zone property if no zone name.
				clerror("Property \"%s\" does not apply to "
				    "node \"%s\".\n",
				    prop_name, this_node);
				if (first_err == CL_NOERR) {
					first_err = CL_EOP;
				}

				(void) valid_props.add(prop_name,
				    (prop_val) ? prop_val : "");
			} else if (
			    (strcmp(prop_name, NODEPROP_ZONESHARE) == 0) ||
			    (strcmp(prop_name, NODEPROP_PSETMIN) == 0)) {
				// check all SC SLM numerical args of the
				// command line
				if ((clerrno = clnode_scslm_check_values(
				    cmd.argv)) != CL_NOERR) {
					return (clerrno);
				}
				// Set SLM properties
				clerrno = clnode_set_scslm_props(this_node,
				    prop_name, prop_val, optflgs);
				if ((clerrno != CL_NOERR) &&
				    (first_err == CL_NOERR)) {
					first_err = clerrno;
				}

				(void) valid_props.add(prop_name,
				    (prop_val) ? prop_val : "");
			} else if (strcmp(prop_name,
			    NODEPROP_FAILOVER_DPM) == 0) {

				// Set DPM property
				clerrno = clnode_set_dpm_autoreboot(this_node,
				    prop_val, optflgs);
				if ((clerrno != CL_NOERR) &&
				    (first_err == CL_NOERR)) {
					first_err = clerrno;
				}

				(void) valid_props.add(prop_name,
				    (prop_val) ? prop_val : "");
			} else {
				int prop_attr = clnode_get_prop_attr(
				    prop_name, NULL);

				if (prop_attr == FLG_READ) {
					clerror("You cannot set "
					    "property \"%s\" for node "
					    "\"%s\".\n",
					    prop_name, this_node);
					if (first_err == CL_NOERR) {
						first_err = CL_EOP;
					}
				} else if (prop_attr == FLG_INVALID) {
					// Invalid node property
					clerror("Invalid property \"%s\" "
					    "for node \"%s\".\n",
					    prop_name, this_node);
					if (first_err == CL_NOERR) {
						first_err = CL_EPROP;
					}
				}
			}
		}
	}

	// Set farm node status
	if (farmnodes.getSize() != 0) {
		boolean_t iseuropa = B_FALSE;
		clerrno = clnode_check_europa(&iseuropa);
		if (clerrno != CL_NOERR) {
			if (first_err == CL_NOERR) {
				first_err = clerrno;
			}
		} else if (!iseuropa) {
			clerror("Farm node management is not configured.\n");
			if (first_err == CL_NOERR) {
				first_err = CL_EOP;
			}
		} else {
			// open Europa libaries
			clerrno = clnode_open_europa();
			if (clerrno != CL_NOERR) {
				if (first_err == CL_NOERR) {
					first_err = clerrno;
				}
			} else {
				clerrno = clnode_set_farm_monstate(farmnodes,
				    optflgs, farm_state);
				if ((clerrno != CL_NOERR) &&
				    (first_err == CL_NOERR)) {
					first_err = clerrno;
				}
			}
			(void) clnode_close_europa();
		}
	}

	// Go through the operands that have zone name
	for (valid_zone_list.start(); !valid_zone_list.end();
	    valid_zone_list.next()) {
		this_node = valid_zone_list.getValue();
		if (!this_node) {
			continue;
		}

		// Go through the valid properties
		for (valid_props.start(); !valid_props.end();
		    valid_props.next()) {
			// Get property name and value
			prop_name = valid_props.getName();
			prop_val = valid_props.getValue();
			if (!prop_name)
				continue;

			// Check if the property can be applied to zones
			if (strcmp(prop_name, NODEPROP_ZPRIVHOST) != 0) {
				clerror("Property \"%s\" does not apply to "
				    "zone \"%s\".\n", prop_name, this_node);
				if (first_err == CL_NOERR) {
					first_err = CL_EPROP;
				}
				continue;
			}

			// Set the zprivatehostname
#if SOL_VERSION >= __s10
			clerrno = clnode_set_zprivatehost(this_node,
			    prop_val, optflgs);
#else
			clerror("Zone \"%s\" is not configured.\n",
			    this_node);
			clerrno = CL_ENOENT;
#endif
			if ((clerrno != CL_NOERR) &&
			    (first_err == CL_NOERR)) {
				first_err = clerrno;
			}
		}
	}

cleanup:
	if (first_err != CL_NOERR) {
		clerrno = first_err;
	}

	return (clerrno);
}

//
// clnode_get_prop_attr
//
//	If the prop_name is changable, return FLG_WRITE;
//	If the prop_name is valid but not changeable, return FLG_READ;
//	O.w. return FLG_INVALID.
//
int
clnode_get_prop_attr(char *prop_name, char *conf_pname)
{
	int i;

	// Check inputs
	if (!prop_name && !conf_pname) {
		return (FLG_INVALID);
	}

	for (i = 0; clnode_props[i].cli_name; i++) {
		if ((prop_name &&
		    (strcmp(prop_name, clnode_props[i].cli_name) == 0)) ||
		    (conf_pname &&
		    (strcmp(prop_name, clnode_props[i].cli_name) == 0))) {
			if (clnode_props[i].read_only) {
				return (FLG_READ);
			} else {
				return (FLG_WRITE);
			}
		}
	}
	return (FLG_INVALID);
}

//
// clnode_set_privatehost
//
//	Set the privatehostname property for a node.
//
clerrno_t
clnode_set_privatehost(char *node_name, char *priv_host, optflgs_t optflgs)
{
	scconf_errno_t scconf_err = SCCONF_NOERR;
	clerrno_t clerrno = CL_NOERR;
	char *msgbuffer = (char *)0;

	// Check inputs
	if (!node_name) {
		clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}

	// Set the privatehostname property
	if (!priv_host || (*priv_host == '\0')) {
		scconf_err = scconf_set_privatehostname(node_name, NULL,
		    0, &msgbuffer);
	} else {
		scconf_err = scconf_set_privatehostname(node_name, priv_host,
		    0, &msgbuffer);
	}

	// Print detailed messages
	if (msgbuffer != (char *)0) {
		clcommand_dumpmessages(msgbuffer);
		free(msgbuffer);
	}
	if (scconf_err != SCCONF_NOERR) {
		switch (scconf_err) {
		case SCCONF_EINUSE:
			clerrno = CL_EBUSY;
			clerror("\"%s\" is already in use by another "
			    "node or zone.\n", priv_host);
			break;
		case SCCONF_ENOEXIST:
			clerrno = CL_ENOENT;
			clerror("Node \"%s\" does not exist.\n", node_name);
			break;
		default:
			clerrno = CL_EINTERNAL;
			clerror("An unexpected error occurred; failed to "
			    "set \"%s\" for node "
			    "\"%s\".\n", NODEPROP_PRIVHOST, node_name);
			break;
		} //lint !e788
	} else if (optflgs & vflg) {
		if (!priv_host || (*priv_host == '\0')) {
			clmessage("\"%s\" is set to the default for node "
			    "\"%s\".\n", NODEPROP_PRIVHOST, node_name);
		} else {
			clmessage("\"%s\" is set to \"%s\" for node "
			    "\"%s\".\n",
			    NODEPROP_PRIVHOST, priv_host, node_name);
		}
	}

	return (clerrno);
}

//
// clnode_set_zprivatehost
//
//	Manage the zprivatehostname for node:zone. The node_name must be
//	in the format of node:zone.
//
clerrno_t
clnode_set_zprivatehost(char *node_name, char *zpriv_host, optflgs_t optflgs)
{
	scconf_errno_t scconf_err = SCCONF_NOERR;
	clerrno_t clerrno = CL_NOERR;
	clerrno_t first_err = CL_NOERR;
	char *zone_name = (char *)0;
	char *node_name_dup = (char *)0;
	int tmp_err = 0;
	char *msgbuffer = (char *)0;

	// Check for inputs
	if (!node_name) {
		clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}

#if SOL_VERSION >= __s10
	// Remove the zprivatehost?
	if (!zpriv_host || (strlen(zpriv_host) == 0)) {
		scconf_err = scconf_clear_privatehostname(node_name);

		if (scconf_err != SCCONF_NOERR) {
			switch (scconf_err) {
			case SCCONF_ENOEXIST:
				clerror("Zone \"%s\" is not configured.\n",
				    node_name);
				clerrno = CL_ENOENT;
				break;
			default:
				clerrno = CL_EINTERNAL;
				clerror("An unexpected error occurred; "
				    "failed to remove \"%s\" "
				    "for zone \"%s\".\n",
				    NODEPROP_ZPRIVHOST, node_name);
				break;
			} //lint !e788
		} else if (optflgs & vflg) {
			clmessage("\"%s\" is removed for zone \"%s\".\n",
			    NODEPROP_ZPRIVHOST, node_name);
		}
	} else {
		// First try to add the new hostname. If the hostname for
		// the non-global zone already exists, change the hostname.
		tmp_err = scconf_set_privatehostname(node_name, zpriv_host, 1,
			&msgbuffer);
		if (tmp_err == SCCONF_EEXIST) {
			// Change the property.
			tmp_err = scconf_set_privatehostname(node_name,
				zpriv_host, 0, &msgbuffer);
		}

		// Print detailed messages
		if (msgbuffer != (char *)0) {
			clcommand_dumpmessages(msgbuffer);
			free(msgbuffer);
		}

		if (tmp_err != CL_NOERR) {
			switch (tmp_err) {
			case SCCONF_EINUSE:
				clerror("\"%s\" is already in use by another "
				    "node or zone.\n", zpriv_host);
				clerrno = CL_EBUSY;
				break;
			case SCCONF_ENOEXIST:
				clerror("\"%s\" is not available for zone "
				    "\"%s\".\n", zpriv_host, node_name);
				clerrno = CL_EINVAL;
				break;
			case SCCONF_ESETUP:
			case SCCONF_EUNEXPECTED:
			default:
				clerror("An unexpected error occurred; "
				    "failed to set \"%s\" for "
				    "zone \"%s\".\n",
				    NODEPROP_ZPRIVHOST, node_name);
				clerrno = CL_EINTERNAL;
				break;
			}
		} else if (optflgs & vflg) {
			clmessage("\"%s\" is set to \"%s\" for zone \"%s\".\n",
			    NODEPROP_ZPRIVHOST, zpriv_host, node_name);
		}
	}

cleanup:
	if (node_name_dup) {
		free(node_name_dup);
	}
#endif

	return (clerrno);
}

//
// clnode_set_dpm_autoreboot
//
//	Set the reboot_on_path_failure setting of the node.
//
clerrno_t
clnode_set_dpm_autoreboot(char *node_name, char *prop_val, optflgs_t optflgs)
{
	clerrno_t clerrno = CL_NOERR;
	int dpm_err = DPM_SUCCESS;
	nodeid_t nodeid;
	scconf_errno_t scconf_err = SCCONF_NOERR;
	boolean_t auto_reboot;

	// Check for inputs
	if (!node_name) {
		clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}

	if (!prop_val || (strcmp(prop_val, "") == 0)) {
		clerror("You must specify the value of "
		"\"%s\".\n", NODEPROP_FAILOVER_DPM);
		return (CL_EINVAL);
	}

	// Invalid property value?
	if ((strcasecmp(prop_val, "enabled") != 0) &&
	    (strcasecmp(prop_val, "disabled") != 0)) {
		clerror("\"%s\" is invalid value for \"%s\".\n",
		    prop_val, NODEPROP_FAILOVER_DPM);
		return (CL_EINVAL);
	}

	// Get the node id
	scconf_err = scconf_get_nodeid(node_name, &nodeid);
	if (scconf_err != SCCONF_NOERR) {
		switch (scconf_err) {
		case SCCONF_ENOEXIST:
			clerror("Node \"%s\" does not exist.\n", node_name);
			clerrno = CL_ENOENT;
			break;
		default:
			clerrno = CL_EINTERNAL;
			clcommand_perror(CL_EINTERNAL);
			break;
		} //lint !e788
		return (clerrno);
	}

	// Initialize libdpm
	if (dpm_initialize() != 0) {
		clerror("Cannot initialize ORB.\n");
		return (CL_EINTERNAL);
	}
	if (dpmccr_initialize(NULL) != 0) {
		clerror("Cannot initialize CCR.\n");
		return (CL_EINTERNAL);
	}

	// Set the property
	if (strcasecmp(prop_val, "enabled") == 0) {
		// Enable it
		auto_reboot = B_TRUE;
	} else if (strcasecmp(prop_val, "disabled") == 0) {
		// Disable it
		auto_reboot = B_FALSE;
	}
	dpm_err = libdpm_set_auto_reboot_feature_state(nodeid,
	    auto_reboot);
	if (dpm_err != DPM_SUCCESS) {
		clerror("Failed to update \"%s\" for node \"%s\".\n",
		    NODEPROP_FAILOVER_DPM, node_name);
		clerrno = CL_EINTERNAL;
	} else if (optflgs & vflg) {
		if (auto_reboot) {
			clmessage("\"%s\" is set to \"enabled\" for "
			    "node \"%s\".\n",
			    NODEPROP_FAILOVER_DPM, node_name);
		} else {
			clmessage("\"%s\" is set to \"disabled\" for "
			    "node \"%s\".\n",
			    NODEPROP_FAILOVER_DPM, node_name);
		}
	}

	return (clerrno);
}

//
// clnode_set_scslm_props
//
//	Set the SLM properties
//
clerrno_t
clnode_set_scslm_props(char *node_name, char *prop_name,
    char *prop_val, optflgs_t optflgs)
{
	scconf_errno_t scconf_err = SCCONF_NOERR;
	int clerrno = CL_NOERR;
	char *msgbuffer = (char *)0;
	int prop_id;
	int prop_ival;

	// Check the input
	if (!node_name) {
		clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}

	// Check the property value
	if (!prop_val || (*prop_val == '\0')) {
		if (strcmp(prop_name, NODEPROP_PSETMIN) == 0) {
			prop_ival = DFT_PSETMIN;
		} else {
			clerror("You must specify "
			    "a value for property "
			    "\"%s\".\n",
			    prop_name);
			return (CL_EINTERNAL);
		}
	} else {
		prop_ival = atoi(prop_val);
	}

	// Set the property ID
	if (strcmp(prop_name, NODEPROP_ZONESHARE) == 0) {
		if ((prop_ival < 1) ||
		    (prop_ival > FSS_MAXSHARES)) {
			clerror("The value of \"%s\" must be "
			    "a number between \"1\" and "
			    "\"%d\".\n",
			    prop_name, FSS_MAXSHARES);
			return (CL_EINTERNAL);
		}
		prop_id = SCCONF_NODE_SCSLM_GLOBAL_SHARES;
	} else if (strcmp(prop_name, NODEPROP_PSETMIN) == 0) {
		if (prop_ival < 1) {
			clerror("The value of \"%s\" must be "
			    "a number greater or equal to \"1\"\n",
			    prop_name);
			return (CL_EINTERNAL);
		}
		prop_id = SCCONF_NODE_SCSLM_DEFAULT_PSET;
	}

	// Set the service level management properties
	scconf_err = scconf_set_scslm(node_name,
	    prop_id, prop_ival, prop_val, &msgbuffer);
	if (scconf_err != SCCONF_NOERR) {
		// Print detailed messages
		if (msgbuffer != (char *)0) {
			clcommand_dumpmessages(msgbuffer);
			free(msgbuffer);
		}

		switch (scconf_err) {
		case SCCONF_ENOEXIST:
			clerrno = CL_ENOENT;
			clerror("Node \"%s\" does not exist.\n", node_name);
			break;
		default:
			clerrno = CL_EINTERNAL;
			clcommand_perror(CL_EINTERNAL);
			break;
		}
	} else if (optflgs & vflg) {
		clmessage("\"%s\" is set to \"%d\" on node \"%s\".\n",
		    prop_name, prop_ival, node_name);
	}

	return (clerrno);
}


//
// check all SC SLM numerical args of the command line
//
static clerrno_t
clnode_scslm_check_values(char **argv)
{
	char		*ptr;
	char		*ptr_cpu;	/* defaultpsetmin=X */
	char		*ptr_share;	/* globalzoneshares=X */
	unsigned int	arg_value;	/* value of X above */
	boolean_t	cpu;		/* cpu or share value ? */

	if (argv == NULL) {
		return (SCCONF_NOERR);
	}
	while (*argv != NULL) {
		ptr_cpu = strstr(*argv, "defaultpsetmin=");
		ptr_share = strstr(*argv, "globalzoneshares=");
		ptr = NULL;
		if (ptr_cpu != NULL) {
			if (ptr_share != NULL) {
				if (ptr_cpu > ptr_share) {
					ptr = ptr_share;
					cpu = B_FALSE;
				} else {
					ptr = ptr_cpu;
					cpu = B_TRUE;
				}
			} else {
				ptr = ptr_cpu;
				cpu = B_TRUE;
			}
		} else {
			ptr = ptr_share;
			cpu = B_FALSE;
		}
		if (ptr == NULL) {
			argv++;
			continue;
		}
		while (*ptr++ != '=') {
		}
		arg_value = (unsigned int) atoi(ptr);
		if (cpu == B_FALSE &&
		    (arg_value < 1 || arg_value > FSS_MAXSHARES)) {
			clerror("globalzoneshares must be "
			    "a number between \"1\" and "
			    "\"%d\".\n",
			    FSS_MAXSHARES);
			return (CL_EINVAL);
		}
		if (cpu == B_TRUE && arg_value < 1) {
			clerror("defaultpsetmin must be "
			    "a number greater or equal to \"1\"\n");
			return (CL_EINVAL);
		}
		argv++;
	}
	return (CL_NOERR);
}
