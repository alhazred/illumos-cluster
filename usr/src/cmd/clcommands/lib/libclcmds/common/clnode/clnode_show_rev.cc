//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clnode_show_rev.cc	1.4	08/05/20 SMI"

//
// Process clnode "show-rev"
//

#include "clnode.h"
#include <scadmin/rpc/rpc_scrcmd.h>

#define	CMD_SCRCMD	"/usr/cluster/lib/sc/scrcmd"
#define	CMD_SCINSTALL	"/usr/cluster/bin/scinstall"

//
// clnode "show-rev"
//
clerrno_t
clnode_show_rev(ClCommand &cmd, ValueList &operands, optflgs_t optflgs)
{
	clerrno_t clerrno = CL_NOERR;
	char output_buf[BUFSIZ];
	char rev_cli[BUFSIZ];
	int err_cli = 0;
	FILE *file_ptr;
	uint_t ismember;
	char *the_node = (char *)0;
	char local_node[BUFSIZ];

	*rev_cli = '\0';

	// Get local node
	*local_node = '\0';
	(void) gethostname(local_node, sizeof (local_node));
	if (*local_node == '\0') {
		clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}

	// Check inputs
	switch (operands.getSize()) {
	case 0:
		(void) sprintf(rev_cli,
		    "%s -p", CMD_SCINSTALL);
		break;
	case 1:
		operands.start();
		the_node = operands.getValue();

		// Check cluster mode
		ismember = 0;
		(void) scconf_ismember(0, &ismember);

		if (ismember) {
			char *nodename = NULL;
			nodeid_t nodeid;
			scconf_errno_t scconf_err = SCCONF_NOERR;

			//
			// The node could be specified as nodeid or nodename.
			// But scrcmd only takes node name.
			//
			scconf_err = scconf_get_nodeid(the_node, &nodeid);
			if (scconf_err != SCCONF_NOERR) {
				if (scconf_err == SCCONF_ENOEXIST) {
					clerrno = CL_ENOENT;
					clerror("Node \"%s\" does not exist.\n",
					    the_node);
				} else {
					clerrno = CL_EINTERNAL;
					clcommand_perror(clerrno);
				}
				goto cleanup;
			}

			// Get the node name
			if (scconf_get_nodename(nodeid, &nodename) !=
			    SCCONF_NOERR) {
				clerrno = CL_EINTERNAL;
				clcommand_perror(clerrno);
				goto cleanup;
			}

			// The command to execute on the node
			if (strcmp(local_node, nodename) == 0) {
				(void) sprintf(rev_cli,
				    "%s -p", CMD_SCINSTALL);
			} else {
				(void) sprintf(rev_cli,
				    "%s -N %s showrev -p",
				    CMD_SCRCMD, nodename);
			}
			free(nodename);
		} else {
			if (strcmp(local_node, the_node) == 0) {
				(void) sprintf(rev_cli,
				    "%s -p", CMD_SCINSTALL);
			} else {
				(void) sprintf(rev_cli,
				    "%s -N %s showrev -p",
				    CMD_SCRCMD, the_node);
			}
		}
		break;
	default:
		clerrno = CL_EINTERNAL;
		clcommand_perror(clerrno);
		goto cleanup;
	}

	if (cmd.isVerbose || (optflgs & vflg)) {
		(void) strcat(rev_cli, "v");
	}

	// Discard the error from scrcmd
	(void) strcat(rev_cli, " 2>/dev/null");

	// Execute the command
	if ((file_ptr = popen(rev_cli, "r")) == NULL) {
		clerrno = CL_EINTERNAL;
		clcommand_perror(clerrno);
		goto cleanup;
	}

	// Get the putput
	*output_buf = '\0';
	while (fgets(output_buf, BUFSIZ, file_ptr) != NULL) {
		// Discard the scrcmd status output.
		if (strstr(output_buf, "SCRCMD_CMD_STATUS=")) {
			continue;
		}

		(void) fprintf(stdout, output_buf);
	}
	err_cli = pclose(file_ptr) >> 8;
	if (err_cli != 0) {
		switch (err_cli) {
		case SCRCMD_ENOMEM:
			clerrno = CL_ENOMEM;
			clcommand_perror(CL_ENOMEM);
			break;
		case SCRCMD_EPROTONOSUPPORT:
			clerrno = CL_EINTERNAL;
			clerror("Cannot create socket.\n");
			break;
		case SCRCMD_ETIMEDOUT:
			clerrno = CL_EINTERNAL;
			clerror("Connection timed out.\n");
			break;
		case SCRCMD_ECONNREFUSED:
			// This error only happens if scrcmd uses remote node
			if (operands.getSize() == 0) {
				clerrno = CL_EINTERNAL;
				clcommand_perror(CL_EINTERNAL);
			} else {
				clerrno = CL_ENOENT;
				clerror("Node \"%s\" does not exist or "
				    "connection refused.\n", the_node);
			}
			break;
		default:
			clerrno = CL_EINTERNAL;
			clcommand_perror(CL_EINTERNAL);
			break;
		}
	}

cleanup:
	return (clerrno);
}
