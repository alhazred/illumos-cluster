//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clnode_add_farm.cc	1.3	08/05/20 SMI"

//
// Process clnode "add-farm"
//

#include "clnode.h"
#include "ClXml.h"

#define	SEPARATER_S	","
#define	SEPARATER_C	','
#define	SEPARATER_OLD	":"

static clerrno_t
clnode_add_farmnode(char *node_name, char *adapters);

//
// clnode "add-farm" with no -i
//
clerrno_t
clnode_add_farm(ClCommand &cmd, ValueList &operands, optflgs_t optflgs,
    NameValueList &properties)
{
	clerrno_t clerrno = CL_NOERR;
	clerrno_t first_err = CL_NOERR;
	scconf_errno_t scconf_err = SCCONF_NOERR;
	char *adapters = (char *)0;
	bool do_disable = false;
	bool is_adapter = false;
	NameValueList farmnodes = NameValueList(true);

	// Check inputs
	if ((operands.getSize() == 0) || (properties.getSize() == 0)) {
		clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}

	// Get the properties
	for (properties.start(); !properties.end(); properties.next()) {
		char *prop_name = properties.getName();
		char *prop_val = properties.getValue();

		if (!prop_name) {
			continue;
		}

		// Check the property
		if (strcasecmp(prop_name, NODEPROP_MONITOR) == 0) {
			if (!prop_val || (*prop_val == '\0')) {
				clerror("You must specify a value "
				    "for \"%s\".\n",
				    NODEPROP_MONITOR);
				if (first_err == CL_NOERR) {
					first_err = CL_EPROP;
				}
			} else if (strcasecmp(prop_val,
			    MONITOR_DISABLED) == 0) {
				do_disable = true;
			} else if (strcasecmp(prop_val, MONITOR_ENABLED) != 0) {
				clerror("Invalid value \"%s\" for "
				    "\"%s\".\n",
				    prop_val, NODEPROP_MONITOR);
				if (first_err == CL_NOERR) {
					first_err = CL_EPROP;
				}
			}
		} else if (strcasecmp(prop_name, NODEPROP_ADAPTERS) == 0) {
			is_adapter = true;

			// Check the adapters
			if (!prop_val || (*prop_val == '\0')) {
				clerror("You must specify the adapters for "
				    "%s node to add.\n",
				    NODETYPE_FARM);
				if (first_err == CL_NOERR) {
					first_err = CL_EPROP;
				}
			} else {
				adapters = new char[strlen(prop_val) + 1];
				if (!strchr(prop_val, SEPARATER_C)) {
					(void) sprintf(adapters, prop_val);
				} else {
					char *adapter1 = (char *)0;
					char *adapter2 = (char *)0;
					*adapters = '\0';

					// Get each adapter
					adapter1 = strtok(prop_val,
					    SEPARATER_S);
					(void) sprintf(adapters, adapter1);
					adapter2 = strtok(NULL, ",");
					if (adapter2) {
						(void) strcat(adapters,
						    SEPARATER_OLD);
						(void) strcat(adapters,
						    adapter2);
					}

					// More than two adapters?
					if (strtok(NULL, SEPARATER_S)) {
						clerror("You cannot specify "
						    "more than two"
						    "adapters.\n");
						if (first_err == CL_NOERR) {
							first_err = CL_EPROP;
						}
					}
				}
			}
		} else {
			clerror("Unknown property \"%s\".\n",
			    prop_name);
			if (first_err == CL_NOERR) {
				first_err = CL_EPROP;
			}
		}
	}

	if (!is_adapter) {
		clerror("You must specify the \"%s\" for the % nodes "
		    "to add.\n",
		    NODEPROP_ADAPTERS, NODETYPE_FARM);
		if (first_err == CL_NOERR) {
			first_err = CL_EINVAL;
		}
	}
	if (first_err != CL_NOERR) {
		goto cleanup;
	}

	// Open the Europa libaries
	clerrno = clnode_open_europa();
	if (clerrno != CL_NOERR) {
		first_err = clerrno;
		goto cleanup;
	}

	// Go through the operands
	for (operands.start(); !operands.end(); operands.next()) {
		char *node_name = operands.getValue();
		if (!node_name)
			continue;

		if (strchr(node_name, ':')) {
			clerror("Invalid node name \"%s\".\n",
			    node_name);
			if (first_err == CL_NOERR) {
				first_err = CL_EINVAL;
			}
			continue;
		}

		// Add this node
		clerrno = clnode_add_farmnode(node_name, adapters);
		if (clerrno != CL_NOERR) {
			if (first_err == CL_NOERR) {
				first_err = clerrno;
			}
		} else {
			if (do_disable) {
				//
				// Collect the nodes to be disabled.
				// By default the node is enabled. So only
				// do disbaled case.
				//
				farmnodes.add(node_name, NODETYPE_FARM);
			}
			if (optflgs & vflg) {
				clmessage("Farm node \"%s\" is "
				    "added.\n", node_name);
			}
		}
	}
	(void) clnode_close_europa();

	if (do_disable && (farmnodes.getSize() != 0)) {
		clerrno = clnode_set_farm_monstate(farmnodes,
		    optflgs, SET_UNMONITOR);
		if ((clerrno != CL_NOERR) && (first_err == CL_NOERR)) {
			first_err = clerrno;
		}
	}

cleanup:
	if (adapters) {
		delete [] adapters;
	}

	return (first_err);
}

//
// clnode "add-farm" with -i
//
clerrno_t
clnode_add_farm(ClCommand &cmd, ValueList &operands, optflgs_t optflgs,
    char *clconfiguration, NameValueList &properties)
{
	clerrno_t clerrno = CL_NOERR;
	clerrno_t first_err = CL_NOERR;
	ValueList xml_objects;
	OptionValues *ov = new OptionValues((char *)"add", cmd.argc, cmd.argv);
	ClXml xml;

	// Check for inputs
	if (!clconfiguration) {
		clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}

	// Add node type
	ov->addOption(CLNODE_TYPE, NODETYPE_FARM);

	// Add command line options
	ov->addNVOptions(CLNODE_PROP, &properties);

	// verbose?
	ov->optflgs = 0;
	if (optflgs & vflg) {
		ov->optflgs |= vflg;
	}

	// Get all the objects in the xml file
	clerrno = xml.getAllObjectNames(CLNODE_CMD, clconfiguration,
	    &xml_objects);
	if (clerrno != CL_NOERR) {
		return (clerrno);
	}
	if (xml_objects.getSize() == 0) {
		clerror("Cannot find any nodes in \"%s\".\n",
		    clconfiguration);
		return (CL_EINVAL);
	}

	// Set operands
	for (operands.start(); !operands.end(); operands.next()) {
		char *operand_name = operands.getValue();
		if (!operand_name)
			continue;

		// Is this operand found in xml file?
		if (strcmp(operand_name, CLCOMMANDS_WILD_OPERAND) != 0) {
			int found = 0;
			for (xml_objects.start(); !xml_objects.end();
			    xml_objects.next()) {
				char *obj_name = xml_objects.getValue();
				if (!obj_name)
					continue;

				if (strcmp(obj_name, operand_name) == 0) {
					found++;
					break;
				}
			}

			if (!found) {
				clerror("Cannot find %s node \"%s\" "
				    "in file \"%s\".\n",
				    NODETYPE_FARM, operand_name,
				    clconfiguration);
				if (first_err == CL_NOERR) {
					first_err = CL_ENOENT;
				}
				continue;
			}
		}

		// Set this operand
		ov->setOperand(operand_name);
		clerrno = xml.applyConfig(CLNODE_CMD, clconfiguration, ov);
		if ((clerrno != CL_NOERR) && (first_err == CL_NOERR)) {
			first_err = clerrno;
		}
	}

	return (first_err);
}

//
// clnode_add_farmnode
//
//	Add the specified farm node.
//
clerrno_t
clnode_add_farmnode(char *node_name, char *adapters)
{
	clerrno_t clerrno = CL_NOERR;
	clerrno_t first_err = CL_NOERR;
	scconf_errno_t scconf_err = SCCONF_NOERR;
	scconf_cltr_handle_t handle = (scconf_cltr_handle_t)0;
	char *msgbuffer = (char *)0;

	// Check input
	if (!node_name || !adapters) {
		clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}

	// Get handle
	(void) scconf_cltr_openhandle(&handle);

	scconf_err = scconf_add_farmnode(handle,
	    node_name, adapters, &msgbuffer);
	if (scconf_err != SCCONF_NOERR) {
		if (msgbuffer) {
			clcommand_dumpmessages(msgbuffer);
			free(msgbuffer);
			msgbuffer = (char *)0;
		}

		// Check the error code
		switch (scconf_err) {
		case SCCONF_EEXIST:
			clerror("Farm node \"%s\" already "
			    "exists.\n", node_name);
			clerrno = CL_EEXIST;
			break;
		case SCCONF_ENODEID:
			// The node_name is numeric.
			clerror("Invalid node name \"%s\".\n",
			    node_name);
			clerrno = CL_EINVAL;
			break;
		case SCCONF_ENOMEM:
			clcommand_perror(CL_ENOMEM);
			clerrno = CL_ENOMEM;
			break;
		default:
			clcommand_perror(CL_EINTERNAL);
			clerrno = CL_EINTERNAL;
			break;
		}
	}

	// Release old handle
	if (handle != (scconf_cltr_handle_t)0) {
		scconf_cltr_releasehandle(handle);
	}

	return (clerrno);
}
