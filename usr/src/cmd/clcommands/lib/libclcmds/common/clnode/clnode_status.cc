//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clnode_status.cc	1.13	09/04/17 SMI"

//
// Process clnode "status"
//

#include "clnode.h"
#include "ClStatus.h"
#include "scstat.h"
#if (SOL_VERSION >= __s10)
#include "clzonecluster.h"
#include "common.h"
#endif

#ifdef WEAK_MEMBERSHIP
#include "ccr_access.h"
#include "sys/stat.h"
#define	MAXNODE 64
#define	NODEID_FILENAME "/etc/cluster/nodeid"
#endif // WEAK_MEMBERSHIP

static clerrno_t
clnode_get_status(ValueList &nodenames, optflgs_t optflgs, ValueList &nodetypes,
    ClStatus *clnode_cstatus, ClStatus *clnode_cstatus_ipmp_node,
    ClStatus *clnode_fstatus, ClStatus *clnode_cstatus_ipmp_zone,
    bool *print_cstatus, bool *print_fstatus, ValueList &zc_names,
    ClStatus *clnode_zcstatus, bool *print_zcstatus);

//
// clnode "status"
//
clerrno_t
clnode_status(ClCommand &cmd, ValueList &operands, optflgs_t optflgs,
    ValueList &nodetypes, ValueList &zc_names)
{
	ClStatus *prt_cstatus = new ClStatus(cmd.isVerbose ? true : false);
	ClStatus *prt_fstatus = new ClStatus(cmd.isVerbose ? true : false);
	ClStatus *prt_zcstatus = new ClStatus(cmd.isVerbose ? true : false);
	ClStatus *prt_cstatus_ipmp_node =
	    new ClStatus(cmd.isVerbose ? true : false);
	ClStatus *prt_cstatus_ipmp_zone =
	    new ClStatus(cmd.isVerbose ? true : false);
	clerrno_t clerrno = CL_NOERR;
	bool print_cstatus, print_fstatus, print_zcstatus;

#ifdef WEAK_MEMBERSHIP
	bool split_brain_exist = false;
	char local_node[BUFSIZ];
	ccr_access_readonly_table read_tab;
	char nkey[24] = "cluster.nodes.";
	char mkey[] = "cluster.properties.multiple_partitions";
	char *datap = NULL;
	char *p = NULL;
	int i;
	int node_count = 0;
	struct stat status_stat;

	// Check if the split brain has occured in the cluster
	split_brain_exist = split_brain_ccr_change_exists();

	if (!(cmd.isClusterMode)) {
		//
		// If the cluster is not configured, we don't proceed.
		//
		if (stat(NODEID_FILENAME, &status_stat) < 0) {
			clcommand_perror(CL_ENOTCLMODE);
			clerrno = CL_ENOTCLMODE;
			goto cleanup;
		}

		// Get the nodes other than local node in the cluster
		// Opens the infrastructure table for reading.
		// Initialization is a must.
		if (read_tab.initialize("infrastructure") != 0) {
			read_tab.close();
			clcommand_perror(CL_EINTERNAL);
			clerrno = CL_EINTERNAL;
			goto cleanup;
		}

		//
		// query for the cluster membership.
		// If node is running the strong membership then we disallow
		// to fetch the cluster status.
		//
		datap = read_tab.query_element(mkey);
		if (datap != NULL && !(strncasecmp(datap, "false", 5))) {
			read_tab.close();
			delete datap;
			clcommand_perror(CL_ENOTCLMODE);
			clerrno = CL_ENOTCLMODE;
			goto cleanup;
		}

		p = nkey + strlen(nkey);
		// Get the local node
		local_node[0] = '\0';
		(void) gethostname(local_node, sizeof (local_node));

		// Set the cluster node heading
		prt_cstatus->setHeadingTitle(HEAD_NODE);
		prt_cstatus->setSectionTitle(SECTION_NODES);
		prt_cstatus->setHeadings((char *)gettext("Node Name"),
			(char *)gettext("Status"));

		for (i = 1; i <= MAXNODE; i++) {
			(void) sprintf(p, "%d.name", i);
			//
			// Query for the element.
			// The data value for the above element is
			// returned by the function.
			//
			datap = read_tab.query_element(nkey);

			if (datap != NULL) {
			    ++node_count;
			    print_cstatus = true;
			    if (strcmp(datap, local_node) == 0) {
				// Add the status of the current node
				if (split_brain_exist) {
					prt_cstatus->addRow(datap,
					(char *)gettext("Offline "
					"- Unresolved split-brain "
					"ccr data changes "
					"present"));
				} else {
					prt_cstatus->addRow(datap,
					(char *)gettext("Offline"));
				}
			    } else {
				// Add the status of other nodes
				prt_cstatus->addRow(datap,
				(char *)gettext("Unknown"));
			    }
			}

			delete datap;
		}
		//
		// Close the table before any updates are to be done
		// on the table.
		// The close operation must happen after all
		// reads are done.
		read_tab.close();

		//
		// Show the error message if the node count
		// is not exactly two
		//
		if (node_count != 2) {
			clerror("You can run this command in "
			    "noncluster mode only for a "
			    "two-node cluster.\n");
			delete prt_cstatus;
			delete prt_cstatus_ipmp_node;
			delete prt_cstatus_ipmp_zone;
			delete prt_fstatus;
			delete prt_zcstatus;
			return (CL_EINVAL);
		}
	} else {
#endif // WEAK_MEMBERSHIP
		clerrno = clnode_get_status(operands, optflgs, nodetypes,
			prt_cstatus, prt_cstatus_ipmp_node, prt_fstatus,
			prt_cstatus_ipmp_zone, &print_cstatus, &print_fstatus,
			zc_names, prt_zcstatus, &print_zcstatus);
#ifdef WEAK_MEMBERSHIP
	}
#endif // WEAK_MEMBERSHIP

	// Print the cluster nodes
	if (print_cstatus) {
		if (!(optflgs & mflg)) {
			prt_cstatus->print();
		}

		// Print ipmp status
		prt_cstatus_ipmp_node->print();
		prt_cstatus_ipmp_zone->print();
	}

	// Print the farm nodes
	if (print_fstatus) {
		prt_fstatus->print();
	}
	// Print the zone cluster nodes
	if (print_zcstatus) {
		prt_zcstatus->print();
	}

cleanup:
	delete prt_cstatus;
	delete prt_cstatus_ipmp_node;
	delete prt_cstatus_ipmp_zone;
	delete prt_fstatus;
	delete prt_zcstatus;

	return (clerrno);
}

//
// get_clnode_status_obj
//
//	Get the status of all quorums in the system.
//
clerrno_t
get_clnode_status_obj(optflgs_t optflgs,
    ClStatus *clnode_cstatus, ClStatus *clnode_cstatus_ipmp_node,
    ClStatus *clnode_fstatus, ClStatus *clnode_cstatus_ipmp_zone)
{
	ValueList empty_list, zc_list;
	bool print_cstatus, print_fstatus, print_zcstatus;
	clerrno_t cl_errno = CL_NOERR;
	ClStatus *prt_zcstatus = new ClStatus(optflgs | vflg ? true : false);

	// Get status of all nodes
	cl_errno = clnode_get_status(empty_list, optflgs, empty_list,
	    clnode_cstatus, clnode_cstatus_ipmp_node, clnode_fstatus,
	    clnode_cstatus_ipmp_zone, &print_cstatus, &print_fstatus,
	    zc_list, prt_zcstatus, &print_zcstatus);

	delete prt_zcstatus;
	return (cl_errno);
}

//
// clnode_get_status
//
//	Get the ClStatus objects based on the nodelist and nodetype.
//	If these lists are empty, get all the cluster nodes.
//	ClStatus object must be created before calling this
//	function, and the caller is responsible to free the
//	object afterwards.
//
clerrno_t
clnode_get_status(ValueList &nodenames, optflgs_t optflgs, ValueList &nodetypes,
    ClStatus *clnode_cstatus, ClStatus *clnode_cstatus_ipmp_node,
    ClStatus *clnode_fstatus, ClStatus *clnode_cstatus_ipmp_zone,
    bool *print_cstatus, bool *print_fstatus, ValueList &zc_names,
    ClStatus *clnode_zcstatus, bool *print_zcstatus)
{
	NameValueList node_list = NameValueList(true);
	clerrno_t clerrno = CL_NOERR;
	clerrno_t first_err = CL_NOERR;
	uint_t i;
	int found;
	quorum_status_t *quorum_status = (quorum_status_t *)0;
	char *node_name;
	char *node_type;
	nodeid_t nodeid;
	scstat_ipmp_stat_list_t *ipmpstate_list;
	scstat_ipmp_stat_list_t *ipmpstate_p = NULL;
	scstat_grp_stat_list_t *grp_p = NULL;
	scstat_errno_t scstat_err = SCSTAT_ENOERR;
	scconf_errno_t scconf_err = SCCONF_NOERR;
	char adpstat[BUFSIZ];
	char grpstat[BUFSIZ];
	char *adpname;
	bool get_ipmp_node = false;
	bool get_ipmp_zone = false;
	boolean_t iseuropa = B_FALSE;
	scstat_farm_node_t *farm_states = NULL;
	scstat_farm_node_t *f_state;
	uint_t offline_count, online_count;
	bool get_farm = false;
	char *zc_name, *nodename, *zc_node_status;
#ifdef WEAK_MEMBERSHIP
	bool split_brain_exist = false;
	bool local_split_brain_exist = false;
	char local_node[BUFSIZ];
	nodeid_t local_nodeid;
#endif // WEAK_MEMBERSHIP

	zc_name = nodename = zc_node_status = NULL;
	// Check inputs
	if (!clnode_cstatus || !clnode_cstatus_ipmp_node || !clnode_fstatus ||
		!clnode_cstatus_ipmp_zone || !clnode_zcstatus) {
		clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}

	*print_cstatus = *print_fstatus = *print_zcstatus = false;

	// Get the valid nodes from the user inputs
	clerrno = clnode_preprocess_nodelist(nodenames, nodetypes,
		node_list, true, zc_names);
	if (clerrno != CL_NOERR) {
		if (first_err == CL_NOERR) {
			first_err = clerrno;
		}
	} else {
		// Is Europa configured?
		if ((clnode_check_europa(&iseuropa) == CL_NOERR) &&
		    iseuropa) {
			if (nodetypes.isValue(NODETYPE_FARM)) {
				*print_fstatus = true;
			}
		}

		// Is cluster node specified?
		if (nodetypes.isValue(NODETYPE_SERVER)) {
			*print_cstatus = true;
		}
		// Is zone cluster node specified?
		if (nodetypes.isValue(NODETYPE_ZONE_CLUSTER)) {
			*print_zcstatus = true;
		}
	}

	// Nothing to print
	if (node_list.getSize() == 0) {
		goto cleanup;
	}

	// Get the quorum status
	if (cluster_get_quorum_status(&quorum_status) != 0) {
		clerrno = CL_EINTERNAL;
		clcommand_perror(clerrno);
		if (first_err == CL_NOERR) {
			first_err = clerrno;
		}
		goto cleanup;
	}

#ifdef WEAK_MEMBERSHIP
	// Check if the split brain has occured in the cluster
	split_brain_exist = split_brain_ccr_change_exists();
#endif // WEAK_MEMBERSHIP

	// Set the cluster node heading
	clnode_cstatus->setHeadingTitle(HEAD_NODE);
	clnode_cstatus->setSectionTitle(SECTION_NODES);
	clnode_cstatus->setHeadings((char *)gettext("Node Name"),
		(char *)gettext("Status"));

	// Process the server nodes
	for (node_list.start(); !node_list.end(); node_list.next()) {
		node_name = node_list.getName();
		node_type = node_list.getValue();
		if (!node_name || !node_type)
			continue;

		if (strcmp(node_type, NODETYPE_SERVER) != 0)
			continue;

		if (zc_name) {
			free(zc_name);
			zc_name = NULL;
		}
		if (nodename) {
			free(nodename);
			nodename = NULL;
		}
		// First parse the nodename from the clustername
		scconf_err = scconf_parse_obj_name_from_cluster_scope(
					node_name, &nodename, &zc_name);
		if (scconf_err != SCCONF_NOERR) {
			if (first_err == CL_NOERR) {
				first_err = CL_EINTERNAL;
			}
			continue;
		}

		// Get the node id of this node
		scconf_err = scconf_get_nodeid(nodename, &nodeid);
		if (scconf_err != SCCONF_NOERR) {
			clerrno = clnode_conv_scconf_err(1, scconf_err);
			if (first_err == CL_NOERR) {
				first_err = clerrno;
			}
			continue;
		}

		// Find the status of this node
		found = 0;
		for (i = 0; i < quorum_status->num_nodes; i++) {
			if (quorum_status->nodelist[i].nid == nodeid) {
				found = 1;
				break;
			}
		}

		if (!found)
			continue;

		// Check the status
		*print_cstatus = true;
		switch (quorum_status->nodelist[i].state) {
		case QUORUM_STATE_ONLINE:
#ifdef WEAK_MEMBERSHIP
			//
			// Make sure we check split_brain_exist flag
			// only for the local node
			//
			// Get the local node
			local_node[0] = '\0';
			(void) gethostname(local_node, sizeof (local_node));

			scconf_err = scconf_get_nodeid(local_node,
						&local_nodeid);
			if (scconf_err != SCCONF_NOERR) {
				clerrno = clnode_conv_scconf_err(1,
						scconf_err);
				if (first_err == CL_NOERR) {
					first_err = clerrno;
				}
				continue;
			}

			if (local_nodeid == nodeid) {
				local_split_brain_exist = split_brain_exist;
			} else {
				local_split_brain_exist = false;
			}

			if (!(local_split_brain_exist)) {
#endif // WEAK_MEMBERSHIP
			    clnode_cstatus->addRow(node_name,
				(char *)gettext("Online"));
#ifdef WEAK_MEMBERSHIP
			} else {
			    clnode_cstatus->addRow(node_name,
				(char *)gettext("Online - Unresolved "
				"split-brain ccr data changes present"));
			}
#endif // WEAK_MEMBERSHIP
			break;
		case QUORUM_STATE_OFFLINE:
			clnode_cstatus->addRow(node_name,
			    (char *)gettext("Offline"));
			break;
		default:
			clnode_cstatus->addRow(node_name,
			    (char *)gettext("Unknown"));
			break;
		}
	}

	// Free any memory
	if (zc_name) {
		free(zc_name);
		zc_name = NULL;
	}
	if (nodename) {
		free(nodename);
		nodename = NULL;
	}

#if (SOL_VERSION >= __s10)
	// Set the cluster node heading
	clnode_zcstatus->setHeadingTitle(HEAD_ZC_NODE);
	clnode_zcstatus->setSectionTitle(SECTION_NODES);
	clnode_zcstatus->setHeadings((char *)gettext("Node Name"),
	    (char *)gettext("Status"));

	// Process the zonecluster nodes
	for (node_list.start(); !node_list.end(); node_list.next()) {
		node_name = node_list.getName();
		node_type = node_list.getValue();
		if (!node_name || !node_type)
			continue;

		if (strcmp(node_type, NODETYPE_ZONE_CLUSTER) != 0)
			continue;

		// First parse the ZC name and the ZC nodename.
		if (zc_name) {
			free(zc_name);
			zc_name = NULL;
		}
		if (nodename) {
			free(nodename);
			nodename = NULL;
		}
		scconf_err = scconf_parse_obj_name_from_cluster_scope(
					node_name, &nodename, &zc_name);
		if (scconf_err != SCCONF_NOERR) {
			if (first_err == CL_NOERR) {
				first_err = CL_EINTERNAL;
			}
			continue;
		}

		// Get the node id of this node
		nodeid = clconf_zc_get_nodeid_by_nodename(zc_name, nodename);

		// Get the status of the ZC nodename
		clerrno = get_zone_cluster_state_on_node(zc_name, nodeid,
						&zc_node_status);
		if (clerrno != CL_NOERR) {
			if (first_err == CL_NOERR) {
				first_err = clerrno;
			}
			continue;
		}

		clnode_zcstatus->addRow(node_name,
			    (char *)gettext(zc_node_status));
		// We will treat a zone cluster node in the same way as a
		// cluster node.
		*print_zcstatus = true;
	}

	// Free any memory
	if (zc_name) {
		free(zc_name);
		zc_name = NULL;
	}
	if (nodename) {
		free(nodename);
		nodename = NULL;
	}
#endif

	// Set the cluster node heading
	clnode_fstatus->setHeadingTitle(HEAD_FNODE);
	clnode_fstatus->setSectionTitle(SECTION_NODES);
	clnode_fstatus->setHeadings((char *)gettext("Node Name"),
	    (char *)gettext("Status"));

	// Process the farm nodes
	if (iseuropa) {
		// Open Europa lib
		clerrno = clnode_open_europa();
		if (clerrno != CL_NOERR) {
			if (first_err == CL_NOERR) {
				first_err = clerrno;
			}
			get_farm = false;
		} else {
			get_farm = true;
		}
	}

	if (get_farm) {
		// Get the farm node status
		scstat_errno_t scstat_err = scstat_get_farmnodes(
		    &farm_states, &offline_count, &online_count);
		if (scstat_err != SCSTAT_ENOERR || !farm_states) {
			clcommand_perror(CL_EINTERNAL);
			if (first_err == CL_NOERR) {
				first_err = CL_EINTERNAL;
			}
		} else {
			// Go through the farm nodes
			for (node_list.start(); !node_list.end();
			    node_list.next()) {
				node_name = node_list.getName();
				node_type = node_list.getValue();
				if (!node_name || !node_type)
					continue;

				if (strcmp(node_type, NODETYPE_FARM) != 0)
					continue;

				// Find the node status
				found = 0;
				for (f_state = farm_states; f_state;
				    f_state = f_state->scstat_node_next) {
					if (!f_state->scstat_node_name) {
						continue;
					}

					if (strcmp(f_state->scstat_node_name,
					    node_name) == 0) {
						found = 1;
						break;
					}
				}
				if (!found) {
					continue;
				}

				*print_fstatus = true;
				clnode_fstatus->addRow(node_name,
				    f_state->scstat_node_statstr);
			}
		}

		if (farm_states) {
			scstat_free_farmnodes(farm_states);
		}
		(void) clnode_close_europa();
	}

	// IPMP status is only displayed in verbose mode.
	if (!(optflgs & vflg) && !(optflgs & mflg)) {
		// No ipmp status. Done.
		goto cleanup;
	}

	// Get ipmp state from libscstat
	scstat_err = scstat_get_ipmp_state(&ipmpstate_list);
	if (scstat_err != SCSTAT_ENOERR) {
		clerrno = CL_EINTERNAL;
		clcommand_perror(clerrno);
		if (first_err == CL_NOERR) {
			first_err = clerrno;
		}
		goto cleanup;
	}

	// Go through each IPMP group
	for (ipmpstate_p = ipmpstate_list; ipmpstate_p;
	    ipmpstate_p = ipmpstate_p->next) {
		if (!ipmpstate_p->name)
			continue;

		// Check the node name
		found = 0;
		for (node_list.start(); !node_list.end();
		    node_list.next()) {
			node_name = node_list.getName();
			if (!node_name)
				continue;
			if ((strcmp(node_name, ipmpstate_p->name) == 0) ||
			    ((strlen(ipmpstate_p->name) > strlen(node_name)) &&
			    (strncmp(node_name, ipmpstate_p->name,
			    strlen(node_name)) == 0) &&
			    (ipmpstate_p->name[strlen(node_name)] == ':'))) {
				found = 1;
				break;
			}
		}

		if (!found)
			continue;

		// Print each group status
		for (grp_p = ipmpstate_p->pgslt; grp_p; grp_p = grp_p->next) {
			// Go through the adapters
			for (i = 0;  i < grp_p->pgst.num_adps; i++) {
				// Group status
				*grpstat = '\0';
				switch (grp_p->status) {
				case PNM_STAT_OK:
					(void) strcpy(grpstat,
					    gettext("Online"));
					break;
				case PNM_STAT_DOWN:
					(void) strcpy(grpstat,
					    gettext("Offline"));
					break;
				case PNM_STAT_STANDBY:
				default:
					(void) strcpy(grpstat,
					    gettext("Unknown"));
					break;
				}

				// Get the adapter status
				adpname = grp_p->pgst.pnm_adp_stat[i].adp_name;
				*adpstat = '\0';
				switch (grp_p->pgst.pnm_adp_stat[i].status) {
				case PNM_STAT_OK:
					(void) strcpy(adpstat,
					    gettext("Online"));
					break;
				case PNM_STAT_DOWN:
					(void) strcpy(adpstat,
					    gettext("Offline"));
					break;
				case PNM_STAT_STANDBY:
					(void) strcpy(adpstat,
					    gettext("Standby"));
					break;
				default:
					(void) strcpy(adpstat,
					    gettext("Unknown"));
					break;
				}

				// Add this row
				if (strstr(ipmpstate_p->name, ":")) {
					clnode_cstatus_ipmp_zone->addRow(
					    ipmpstate_p->name,
					    grp_p->grp_name,
					    grpstat,
					    adpname, adpstat);
					get_ipmp_zone = true;
				} else {
					clnode_cstatus_ipmp_node->addRow(
					    ipmpstate_p->name,
					    grp_p->grp_name,
					    grpstat,
					    adpname, adpstat);
					get_ipmp_node = true;
				}
			}
		}
	}

	// Add heading
	if (get_ipmp_node) {
		clnode_cstatus_ipmp_node->setSectionTitle(SECTION_IPMP_NODE);
		clnode_cstatus_ipmp_node->setHeadings(
		    (char *)gettext("Node Name"),
		    (char *)gettext("Group Name"),
		    (char *)gettext("Status"),
		    (char *)gettext("Adapter"),
		    (char *)gettext("Status"));
	}
	if (get_ipmp_zone) {
		clnode_cstatus_ipmp_zone->setSectionTitle(SECTION_IPMP_ZONE);
		clnode_cstatus_ipmp_zone->setHeadings(
		    (char *)gettext("Zone Name"),
		    (char *)gettext("Group Name"),
		    (char *)gettext("Status"),
		    (char *)gettext("Adapter"),
		    (char *)gettext("Status"));
	}

	scstat_ipmp_stat_list_free(ipmpstate_list);

cleanup:
	// Release quorum status
	if (quorum_status != NULL) {
		cluster_release_quorum_status(quorum_status);
	}

	if (first_err != CL_NOERR) {
		clerrno = first_err;
	}

	return (clerrno);
}
