//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clnode_list.cc	1.11	09/03/23 SMI"

//
// Process clnode "list"
//

#include "clnode.h"
#include "ClList.h"

//
// clnode "list"
//
clerrno_t
clnode_list(ClCommand &cmd, ValueList &operands, optflgs_t optflgs,
    ValueList &nodetypes, ValueList &zc_names)
{
	ClList print_list = ClList(cmd.isVerbose ? true : false);
	NameValueList node_list = NameValueList(true);
	clerrno_t clerrno = CL_NOERR;
	clerrno_t first_err = CL_NOERR;
	boolean_t zc_flag;
	scconf_errno_t scconf_err = SCCONF_NOERR;
	char *cl_name;
	ValueList zc_node_list;

	// Make sure that only one of zc_names or nodetypes is
	// specified
	if (zc_names.getSize() > 0 && nodetypes.getSize() > 0) {
		clerror("You cannot specify both nodetypes and zone cluster "
			"names as operands.\n");
		clcommand_usage(cmd, cmd.getSubCommandName(), 1);
		return (CL_EINVAL);
	}

	// Get the valid nodes from the user inputs

	// If the user specified nodetypes, then fetch all the nodes
	// of the specified type.
	if (zc_names.getSize() < 1) {
		// This is also the default behavior of the command
		clerrno = clnode_preprocess_nodelist(operands, nodetypes,
		    node_list, true);
	} else {
#if (SOL_VERSION >= __s10)
		// The -Z flag was specified.
		// First we have to check whether we are in the global zone
		// or not. If we are in a zone cluster then only the current
		// zone cluster information can be seen.
		scconf_err = scconf_zone_cluster_check(&zc_flag);

		if (scconf_err != SCCONF_NOERR) {
			clerror("Internal Error.\n");
			return (CL_EINTERNAL);
		}

		if (zc_flag) {
			// We are in the zone cluster
			cl_name = clconf_get_clustername();
			if (!cl_name) {
				clerror("Cannot get the cluster name.\n");
				return (CL_EINTERNAL);
			}

			// Check for invalid cluster names and report errors
			// if zcnames as all then add that zone to zc_names
			// as within a zone cluster only one zone exists.
			for (zc_names.start(); !zc_names.end();
				zc_names.next()) {
				if (strcmp(zc_names.getValue(),
					ALL_CLUSTERS_STR) == 0) {
					zc_names.clear();
					zc_names.add(cl_name);
					break;
				} else if (strcmp(zc_names.getValue(),
					cl_name)) {
					// Unknown cluster
					clerror("Invalid zone cluster name %s"
					" specified", zc_names.getValue());
					clerrno = CL_EINVAL;
				}
			}

			// If the current cluster name was not specified, then
			// there is nothing to print
			if (zc_names.isValue(cl_name)) {
				// Now we can print the current zone cluster
				// configuration
				clerrno = clnode_preprocess_nodelist(operands,
						nodetypes, node_list, true);
			}
		} else {
			// If we are here, then we are in the global zone since
			// clnode_list() will not be called from a native zone.
			zc_names.start();
			if ((strcmp(zc_names.getValue(),
				ALL_CLUSTERS_STR) == 0) ||
				(strcmp(zc_names.getValue(),
				GLOBAL_CLUSTER_NAME) == 0)) {
				clerrno = clnode_preprocess_nodelist(operands,
					nodetypes, node_list, true, zc_names);
			} else {
				clerrno = clnode_get_zone_cluster_nodes(
					zc_node_list, true, zc_names);
			}

			if (clerrno != CL_NOERR) {
				return (clerrno);
			}

			// Now add them to node_list
			for (zc_node_list.start(); !zc_node_list.end();
				zc_node_list.next()) {
				node_list.add(zc_node_list.getValue(),
						NODETYPE_ZONE_CLUSTER);
			}
		}
#endif
	}

	if (clerrno != CL_NOERR) {
		// If nothing to print
		if (node_list.getSize() == 0) {
			return (clerrno);
		}
	}

	// If verbose, add the header
	if (optflgs & vflg) {
		print_list.setHeadings((char *)gettext("Node"),
		    (char *)gettext("Type"));
	}

	// List the nodes
	for (node_list.start(); node_list.end() != 1; node_list.next()) {
		print_list.addRow(node_list.getName(), node_list.getValue());
	}

	// Print it
	print_list.print();

	return (clerrno);
}
