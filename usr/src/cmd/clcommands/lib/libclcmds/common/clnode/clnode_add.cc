//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clnode_add.cc	1.8	09/04/03 SMI"

//
// Process clnode "add"
//

#include "clnode.h"
#include "sys/stat.h"
#include "ClXml.h"

#define	CMD_SCINSTALL	"/usr/cluster/bin/scinstall"
#define	CMD_SCRCONF	"/usr/cluster/lib/sc/scrconf"
#define	SC_TRTYPE	"dlpi"
#define	SC_TRSWITCH	"switch"

static clerrno_t
clnode_get_ep_from_cable(char *cable, char *node_name, char **ep_adapter,
    char **adapter_node, char **ep_switch);

//
// clnode "add" without -i
//
//	The cluster packages must be already installed on the node to be
//	added.
//
clerrno_t
clnode_add(ClCommand &cmd, ValueList &operands, optflgs_t optflgs,
    NameValueList &properties, char *sponsornode,
    char *clustername, char *gmntpoint, ValueList &endpoints)
{
	clerrno_t clerrno = CL_NOERR;
	clerrno_t first_err = CL_NOERR;
	char cli_cmd[SCCONF_MAXSTRINGLEN];
	char tmp_buf[SCCONF_MAXSTRINGLEN];
	char *node_name = (char *)0;
	bool is_first = true;
	int i;
	char **cli_envs = new char *[2];
	char **cli_args;
	int arg_count = 0;
	int env_count = 0;
	char *cli_save, *chk_string;
	char *global_fs = (char *)0;
	int ep_count = 0;
	char *cbl1_adapter = (char *)0;
	char *cbl1_node = (char *)0;
	char *cbl1_switch = (char *)0;
	char *cbl2_adapter = (char *)0;
	char *cbl2_node = (char *)0;
	char *cbl2_switch = (char *)0;
	char *progname = (char *)0;
	//
	// Check for inputs. We don't take any properties for now, but
	// we will add it in the future.
	//
	if (!sponsornode || (operands.getSize() != 1) ||
	    (properties.getSize() > 0)) {
		clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}

	// Check the sponsornode
	operands.start();
	node_name = operands.getValue();
	if (!node_name) {
		clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}
	if (strcmp(sponsornode, node_name) == 0) {
		clerror("The sponsornode \"%s\" cannot be the "
		    "node to add.\n", sponsornode);
		if (first_err == CL_NOERR) {
			first_err = CL_EINVAL;
		}
	}

	// Initialize the command buffer
	(void) sprintf(cli_cmd,
	    "%s -k -i -N %s ", CMD_SCINSTALL, sponsornode);

	// Add the cluster name
	if (clustername) {
		(void) sprintf(tmp_buf, "-C %s ", clustername);
		(void) strcat(cli_cmd, tmp_buf);
	}

	// Add the global FS
	if (gmntpoint) {
		global_fs = strdup(gmntpoint);
	} else {
		global_fs = strdup(DEFAULT_GLOBALDEVFS);
	}
	if (global_fs) {
		struct stat gmnt_stat;
		if (strncmp(global_fs, LOFI_METHOD,
		    sizeof (LOFI_METHOD) - 1) != 0) {
		    if (*global_fs == '/') {
			while (*global_fs == '/') global_fs++;
			global_fs--;
		    }
		    if (stat(global_fs, &gmnt_stat) < 0) {
			clerror("Cannot access \"%s\".\n", global_fs);
			if (first_err == CL_NOERR) {
			    first_err = CL_EINVAL;
			}
		    } else if (S_ISCHR(gmnt_stat.st_mode)) {
			// Raw disk?
			if (!strstr(global_fs, "/rdsk/")) {
			    clerror("\"%s\" is not a raw disk device.\n",
				    global_fs);
			    if (first_err == CL_NOERR) {
				first_err = CL_EINVAL;
			    }
			}
		    } else if (!S_ISDIR(gmnt_stat.st_mode)) {
			// File system mount points?
			clerror("\"%s\" is not a file system mount point.\n",
				global_fs);
			if (first_err == CL_NOERR) {
			    first_err = CL_EINVAL;
			}
		    }
		}
		(void) sprintf(tmp_buf, "-G %s ", global_fs);
		(void) strcat(cli_cmd, tmp_buf);
	}

	// Must have cables
	if (endpoints.getSize() == 0) {
		clerror("You must specify the interconnect cables using "
		    "\"-e\" or in the configuration file.\n");
		if (first_err == CL_NOERR) {
			first_err = CL_EINVAL;
		}
	}

	// Go through the endpoints
	for (endpoints.start(); !endpoints.end(); endpoints.next()) {
		char *endpoint = endpoints.getValue();
		if (!endpoint) {
			continue;
		}

		// Set the counter
		if (!cbl1_adapter || (strlen(cbl1_adapter) == 0)) {
			clerrno = clnode_get_ep_from_cable(endpoint, node_name,
			    &cbl1_adapter, &cbl1_node, &cbl1_switch);
			if (clerrno != CL_NOERR) {
				if (first_err == CL_NOERR) {
					first_err = clerrno;
				}
				if (cbl1_adapter) {
					delete [] cbl1_adapter;
					*cbl1_adapter = '\0';
				}
				if (cbl1_node) {
					delete [] cbl1_node;
					*cbl1_node = '\0';
				}
				if (cbl1_switch) {
					delete [] cbl1_switch;
					*cbl1_switch = '\0';
				}
			} else {
				ep_count++;
			}
		} else if (!cbl2_adapter || (strlen(cbl2_adapter) == 0)) {
			clerrno = clnode_get_ep_from_cable(endpoint, node_name,
			    &cbl2_adapter, &cbl2_node, &cbl2_switch);
			if (clerrno != CL_NOERR) {
				if (first_err == CL_NOERR) {
					first_err = clerrno;
				}
				if (cbl2_adapter) {
					delete [] cbl2_adapter;
					*cbl2_adapter = '\0';
				}
				if (cbl2_node) {
					delete [] cbl2_node;
					*cbl2_node = '\0';
				}
				if (cbl2_switch) {
					delete [] cbl2_switch;
					*cbl2_switch = '\0';
				}
			} else {
				ep_count++;
			}
		} else {
			break;
		}
	}

	// Add the endpoints
	if (ep_count) {
		// Verify the endpoints
		if (cbl1_adapter && cbl2_adapter &&
		    (strcmp(cbl1_adapter, cbl2_adapter) == 0) &&
		    (strlen(cbl1_adapter) != 0)) {
			clerror("You cannot specify adapter \"%s\" "
			    "in more than one cable.\n", cbl1_adapter);
			if (first_err == CL_NOERR) {
				first_err = CL_EINVAL;
			}
		}
		if (cbl1_switch && cbl2_switch &&
		    (strcmp(cbl1_switch, cbl2_switch) == 0)) {
			clerror("You cannot specify switch \"%s\" "
			    "in more than one cable.\n", cbl1_adapter);
			if (first_err == CL_NOERR) {
				first_err = CL_EINVAL;
			}
		}

		// Add the adapters into the cli buffer
		if (cbl1_adapter && (strlen(cbl1_adapter) != 0)) {
			(void) sprintf(tmp_buf, "-A trtype=%s,name=%s ",
			    SC_TRTYPE, cbl1_adapter);
			(void) strcat(cli_cmd, tmp_buf);
		}
		if (cbl2_adapter && (strlen(cbl2_adapter) != 0)) {
			(void) sprintf(tmp_buf, "-A trtype=%s,name=%s ",
			    SC_TRTYPE, cbl2_adapter);
			(void) strcat(cli_cmd, tmp_buf);
		}

		// Add the cables into the cli buffer
		if (cbl1_adapter && cbl1_switch) {
			(void) sprintf(tmp_buf,
			    "-m endpoint=%s:%s,endpoint=%s ",
			    node_name, cbl1_adapter, cbl1_switch);
			(void) strcat(cli_cmd, tmp_buf);
		}
		if (cbl2_adapter && cbl2_switch) {
			(void) sprintf(tmp_buf,
			    "-m endpoint=%s:%s,endpoint=%s ",
			    node_name, cbl2_adapter, cbl2_switch);
			(void) strcat(cli_cmd, tmp_buf);
		}

		// Add the switches into the cli buffer
		if (cbl1_switch && (strlen(cbl1_switch) != 0)) {
			// No port
			char *tmp_char = strchr(cbl1_switch, '@');
			if (tmp_char) {
				*tmp_char++ = '\0';
			}

			(void) sprintf(tmp_buf, "-B type=%s,name=%s ",
			    SC_TRSWITCH, cbl1_switch);
			(void) strcat(cli_cmd, tmp_buf);
		}
		if (cbl2_switch && (strlen(cbl2_switch) != 0)) {
			// No port
			char *tmp_char = strchr(cbl2_switch, '@');
			if (tmp_char) {
				*tmp_char++ = '\0';
			}

			(void) sprintf(tmp_buf, "-B type=%s,name=%s ",
			    SC_TRSWITCH, cbl2_switch);
			(void) strcat(cli_cmd, tmp_buf);
		}
	}

	// Any errors?
	if (first_err != CL_NOERR) {
		goto cleanup;
	}

	// Set the env to call scinstall.
	if ((progname = strrchr(cmd.argv[0], '/')) == NULL) {
		progname = cmd.argv[0];
	} else {
		++progname;
	}

	cli_envs[env_count] = new char [strlen(progname) + 6];
	(void) sprintf(cli_envs[env_count++], "PROG=%s", progname);
	cli_envs[env_count] = NULL;

	// Find out how many arguments are specified
	arg_count = 0;
	cli_save = new char [strlen(cli_cmd) + 1];
	(void) strcpy(cli_save, cli_cmd);

	chk_string = strtok(cli_cmd, " ");
	while ((chk_string = strtok(NULL, " ")) != NULL) arg_count++;

	// Set the args to call scinstall.
	cli_args = new char *[arg_count + 2];

	chk_string = strtok(cli_save, " ");
	arg_count = 0;
	while (chk_string != NULL) {
		cli_args[arg_count] = new char [strlen(chk_string) + 1];
		(void) strcpy(cli_args[arg_count++], chk_string);

		chk_string = strtok(NULL, " ");
	}
	cli_args[arg_count] = NULL;

	delete [] cli_save;

	// Call the command
	clerrno = execve_command(CMD_SCINSTALL, cli_args, cli_envs);
	if ((clerrno == 1) && (first_err == CL_NOERR)) {
		first_err = CL_EINVAL;
	}

cleanup:
	// Free memory
	for (i = 0; i < arg_count; i++) {
		if (cli_args && cli_args[i]) {
			delete [] cli_args[i++];
		}
	}
	if (arg_count && cli_args) {
		delete [] cli_args;
	}

	for (i = 0; i < env_count; i++) {
		delete [] cli_envs[i];
	}
	delete [] cli_envs;

	if (global_fs)
		delete [] global_fs;

	if (cbl1_adapter)
		delete [] cbl1_adapter;
	if (cbl2_adapter)
		delete [] cbl2_adapter;
	if (cbl1_node)
		delete [] cbl1_node;
	if (cbl2_node)
		delete [] cbl2_node;
	if (cbl1_switch)
		delete [] cbl1_switch;
	if (cbl2_switch)
		delete [] cbl2_switch;

	if (first_err != CL_NOERR) {
		clerrno = first_err;
	}

	return (clerrno);
}

//
// clnode "add" with -i
//
clerrno_t
clnode_add(ClCommand &cmd, ValueList &operands, optflgs_t optflgs,
    char *clconfiguration, NameValueList &properties,
    char *sponsornode, char *clustername, char *gmntpoint,
    ValueList &endpoints)
{
	clerrno_t clerrno = CL_NOERR;
	clerrno_t first_err = CL_NOERR;
	ValueList xml_objects;
	char *operand_name = (char *)0;
	int found = 0;

	// Check for inputs
	if (!clconfiguration || !sponsornode ||
	    (operands.getSize() != 1) ||
	    (properties.getSize() > 0)) {
		clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}

	OptionValues *ov = new OptionValues((char *)"add", cmd.argc, cmd.argv);
	ClXml xml;

	// Add command line options
	ov->addOption(CLNODE_SPONSOR, sponsornode);
	if (clustername) {
		ov->addOption(CLNODE_CLUSTER, clustername);
	}
	if (gmntpoint) {
		ov->addOption(CLNODE_GDEV, gmntpoint);
	}

	// Add endpoints
	if (endpoints.getSize() > 0) {
		ov->addOptions(CLNODE_ENDPOINT, &endpoints);
	}

	// verbose?
	ov->optflgs = 0;
	if (optflgs & vflg) {
		ov->optflgs |= vflg;
	}

	// Get all the node objects in the xml file
	clerrno = xml.getAllObjectNames(CLNODE_CMD, clconfiguration,
		&xml_objects);
	if (clerrno != CL_NOERR) {
		return (clerrno);
	}

	if (xml_objects.getSize() == 0) {
		clerror("Cannot find any node in \"%s\".\n",
		    clconfiguration);
		return (CL_EINVAL);
	}

	// Set operand. There is only one operand.
	operands.start();
	operand_name = operands.getValue();
	// Is this operand found in xml file?
	for (xml_objects.start(); !xml_objects.end();
	    xml_objects.next()) {
		char *obj_name = xml_objects.getValue();
		if (!obj_name) {
			continue;
		}

		if (strcmp(obj_name, operand_name) == 0) {
			found++;
			break;
		}
	}
	if (!found) {
		clerror("Cannot find node \"%s\" in file \"%s\".\n",
		    operand_name, clconfiguration);
		clerrno = CL_ENOENT;
	} else {
		// Set the operand
		ov->setOperand(operand_name);

		// Check if there is user specified global mount point
		if (!gmntpoint) {
			NameValueList *cl_objprops = new NameValueList;
			clerrno = xml.getObjProps(CLNODE_CMD, operand_name,
			    clconfiguration, cl_objprops);
			if (clerrno != CL_NOERR) {
				delete cl_objprops;
				return (clerrno);
			}

			// Check the globaldevfs tag
			for (cl_objprops->start(); !cl_objprops->end();
			    cl_objprops->next()) {
				char *name = cl_objprops->getName();
				if (!name ||
				    (strcmp(name, "globaldevfs") != 0)) {
					continue;
				}

				// Add global dev fs
				name = cl_objprops->getValue();
				if (name && (strlen(name) != 0)) {
					ov->addOption(CLNODE_GDEV, name);
					break;
				}
			}
			delete cl_objprops;
		}

		// Add the node.
		clerrno = xml.applyConfig(CLNODE_CMD, clconfiguration, ov);
	}

	return (clerrno);
}

//
// clnode_get_ep_from_cable
//
//	Get the adapter, adapter name, and the switch from a given cable.
//	Caller need to free the memory allocated for these components.
//
clerrno_t
clnode_get_ep_from_cable(char *cable, char *node_name, char **ep_adapter,
    char **adapter_node, char **ep_switch)
{
	int clerrno = CL_NOERR;
	char *ep1 = (char *)0;
	char *ep2 = (char *)0;

	// Check inputs
	if (!cable || !node_name || !ep_adapter || !adapter_node ||
	    !ep_switch) {
		clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}

	*ep_adapter = '\0';
	*adapter_node = '\0';
	*ep_switch = '\0';

	// Get the endpoints
	ep1 = new char[strlen(cable) + 1];
	(void) strcpy(ep1, cable);
	ep2 = strchr(ep1, ',');
	if (ep2) {
		*ep2++ = '\0';

		// Cannot have two adapters specified
		if (ep2 && strchr(ep2, ':') && ep1 && strchr(ep1, ':')) {
			clerror("No switch is specified in cable \"%s\".\n",
			    cable);
			clerrno = CL_EINVAL;
			goto cleanup;
		}

		// Get the components
		if (ep2 && strchr(ep2, ':')) {
			*adapter_node = new char[strlen(ep2) + 1];
			(void) strcpy(*adapter_node, ep2);
			if (ep1) {
				*ep_switch = new char[strlen(ep1) + 1];
				(void) strcpy(*ep_switch, ep1);
			}
		} else if (ep1 && strchr(ep1, ':')) {
			*adapter_node = new char[strlen(ep1) + 1];
			(void) strcpy(*adapter_node, ep1);
			if (ep2) {
				*ep_switch = new char[strlen(ep2) + 1];
				(void) strcpy(*ep_switch, ep2);
			}
		} else {
			clerror("No \"[node]:adapter\" is specified in cable "
			    "\"%s\".\n", cable);
			clerrno = CL_EINVAL;
			goto cleanup;
		}

		// No swtch?
		if (**ep_switch == '\0') {
			clerror("No switch is specified in cable \"%s\".\n",
			    cable);
			clerrno = CL_EINVAL;
			goto cleanup;
		}

		// Separate the adpater and node
		char *tmp_adapter = strchr(*adapter_node, ':');
		if (tmp_adapter) {
			*tmp_adapter++ = '\0';
			if (tmp_adapter && (strlen(tmp_adapter) != 0)) {
				*ep_adapter = new char[strlen(tmp_adapter) + 1];
				(void) strcpy(*ep_adapter, tmp_adapter);
			} else {
				clerror("No adapter is specified in cable "
				    "\"%s\".\n", cable);
				clerrno = CL_EINVAL;
				goto cleanup;
			}
		}
	}

	// Check the node name
	if (*adapter_node && (strlen(*adapter_node) != 0) &&
	    (strcmp(*adapter_node, node_name) != 0)) {
		clerror("The node in cable \"%s\" must be same as node "
		    "\"%s\" that you are adding to the cluster.\n",
		    cable, node_name);
		clerrno = CL_EINVAL;
	}

cleanup:
	delete [] ep1;

	return (clerrno);
}
