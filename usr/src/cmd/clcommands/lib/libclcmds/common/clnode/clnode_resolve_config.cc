//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident   "@(#)clnode_resolve_config.cc 1.3     09/04/14 SMI"

//
// Process clnode "resolve-config"
//

#include "clnode.h"
#include "common.h"
#include "ccr_access.h"
#include "sys/stat.h"

#define	MAXNODE 64

clerrno_t
clnode_resolve_config(ClCommand &cmd, ValueList &operands, optflgs_t optflgs,
	char *winnernode)
{
	clerrno_t clerrno = CL_NOERR;
	clerrno_t first_err = CL_NOERR;
	char *node_name = NULL;
	char *answer = NULL;
	char replybuf[BUFSIZ];
	int err_flg = 0;
	int found = 0;
	int str_len = 0;
	nodeid_t nodeid;
	scconf_errno_t scconf_err = SCCONF_NOERR;
	ccr_access_readonly_table read_tab;
	clconf_iter_t *currnodesi = NULL;
	clconf_cluster_t *clconf = NULL;
	char *datap = NULL;
	bool force_flag_winner = false;
	bool force_flag_loser = false;
	char *p = NULL;
	int node_count = 0;
	int i = 0;
	char nkey[24] = "cluster.nodes.";
	int retval = 0;

	//
	// Check for the inputs
	//
	if (!winnernode || (operands.getSize() != 1)) {
		clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}

	//
	// Validate if there are exactly two nodes in the cluster
	// configuration before proceeding
	//
	if (cmd.isClusterMode) {
		//
		// The node is in the cluster mode.
		// Initialize ORB. If we are not able to initialize the ORB here
		// we can't proceed further.
		//
		if ((retval = clconf_lib_init()) != 0) {
			clerrno = CL_EINTERNAL;
			clerror("Failed to initialize clconf library\n");
			return (clerrno);
		}

		if ((clconf = clconf_cluster_get_current()) == NULL) {
			clerrno = CL_EINTERNAL;
			clerror("Error in clconf_cluster_get_current()\n");
			return (clerrno);
		}

		currnodesi = clconf_cluster_get_nodes(clconf);

		node_count = clconf_iter_get_count(currnodesi);
	} else {
		//
		// The node is in non cluster mode
		// Opens the infrastructure table for reading.
		// Initialization is a must.
		//
		if (read_tab.initialize("infrastructure") != 0) {
			read_tab.close();
			clcommand_perror(CL_EINTERNAL);
			return (CL_EINTERNAL);
		}

		p = nkey + strlen(nkey);

		for (i = 1; i <= MAXNODE; i++) {
			(void) sprintf(p, "%d.name", i);

			datap = read_tab.query_element(nkey);
			if (datap != NULL) {
				++node_count;
			}

			delete datap;
		}

		read_tab.close();
	}

	if (node_count != 2) {
		clcommand_perror(CL_EOP);
		clerror("Number of nodes not exactly two\n");
		return (CL_EOP);
	}

	//
	// Check for the validity of winner node
	//
	if (cmd.isClusterMode) {
		//
		// The node is in the cluster mode.
		//
		scconf_err = scconf_get_nodeid(winnernode, &nodeid);
		if (scconf_err != SCCONF_NOERR) {
			if (scconf_err == SCCONF_ENOEXIST) {
				clerrno = CL_ENOENT;
				clerror("Node \"%s\" does not exist.\n",
					winnernode);
			} else {
				clerrno = CL_EINTERNAL;
				clcommand_perror(clerrno);
			}
			goto cleanup;
		}
	} else {
		//
		// The node is in non cluster mode
		// Opens the infrastructure table for reading.
		// Initialization is a must.
		//
		if (read_tab.initialize("infrastructure") != 0) {
			read_tab.close();
			clcommand_perror(CL_EINTERNAL);
			return (CL_EINTERNAL);
		}

		//
		// Query for the elements "cluster.nodes.1.name" and
		// "cluster.nodes.2.name" and check if the winner node equals
		// to any of these elements or not. If it returns true the
		// winner node is a valid node otherwise we'll
		// abort the operation.
		//
		datap = read_tab.query_element("cluster.nodes.1.name");

		if (strcmp(datap, winnernode) == 0) {
			// winner node is a valid node.
			found++;
		}

		if (found == 0) {
			datap = read_tab.query_element("cluster.nodes.2.name");
			if (strcmp(datap, winnernode) == 0) {
				found++;
			}
		}

		if (found == 0) {
			// If we still come here, it means there the node entry
			// is not found in ccr.
			clerrno = CL_ENOENT;
			clerror("Node \"%s\" does not exist.\n", winnernode);
			delete datap;
			read_tab.close();
			goto cleanup;
		}

		//
		// Need to free up the space occupied by the data.
		//
		delete datap;

		//
		// Close the table before any updates are to be done
		// on the table.
		// The close operation must happen after all reads are done.
		//
		read_tab.close();
	}

	//
	// Retrieve the operands.
	//
	operands.start();
	node_name = operands.getValue();

	//
	// If the winner node and current nodes are the same, then mark
	// the ccr copy of current node as valid ccr.
	// If the winner node and the current nodes are not the same, then
	// mark the ccr copy of the current node as invalid ccr.
	// User needs to reboot the node if the ccr copy of the current
	// node is invalidated.
	//
	if (strcmp(winnernode, node_name) == 0) {
		if (optflgs & Fflg) {
			// -F option.
			force_flag_winner = true;
	} else {
		if (strcmp(gettext("yes"), "yes") == 0 &&
			strcmp(gettext("no"), "no") == 0) {
			// The l10n language is english, so
			// we can stick with 'y' and 'n'.
			(void) clmessage("This command accepts configuration "
				"changes on \"%s\" and enables the ability to "
				"form the 2-node cluster. Is it okay to "
				"proceed (y/n)[n]? ", winnernode);

			answer = fgets(replybuf, sizeof (replybuf) - 1,
				stdin);
			if (answer) {
			str_len = strlen(replybuf) - 1;
			if (*answer == '\n') {
				str_len = str_len + 1;
			}
			if (strncasecmp(replybuf, "y", str_len) == 0 ||
				strncasecmp(replybuf,
					"yes", str_len) == 0) {
				force_flag_winner = true;
			} else if (strncasecmp(replybuf, "n", str_len) == 0 ||
				strncasecmp(replybuf, "no", str_len) == 0 ||
				*answer == '\n') {
				// user has entered No or selected the default.
				force_flag_winner = false;
			} else {
				snprintf(replybuf, strlen(replybuf), replybuf);
				clerror("Invalid input \"%s\".\n", replybuf);
				clerrno = CL_EINVAL;
				goto cleanup;
			}
			}
		} else {
			// The l10n language is not english, so
			// we have to stick with 'yes' and 'no'
			(void) clmessage("This command accepts configuration "
				"changes on \"%s\" and enables the ability to "
				"form the 2-node cluster. Is it okay to "
				"proceed (%s/%s)[%s]? ", winnernode,
				gettext("yes"), gettext("no"), gettext("no"));

			answer = fgets(replybuf, sizeof (replybuf) - 1,
				stdin);
			if (answer) {
			while (str_len < BUFSIZ &&
				replybuf[str_len] != '\n') {
				str_len++;
			}
			if (*answer == '\n') {
				str_len = str_len + 1;
			}
			if (strncasecmp(replybuf, gettext("yes"),
				str_len) == 0) {
				force_flag_winner = true;
			} else if (strncasecmp(replybuf, gettext("no"),
				str_len) == 0 ||
				*answer == '\n') {
				// user has entered No or selected the default.
				force_flag_winner = false;
			} else {
				snprintf(replybuf, strlen(replybuf), replybuf);
				clerror("Invalid input \"%s\".\n", replybuf);
				clerrno = CL_EINVAL;
				goto cleanup;
			}
			}
		}
		}
	} else {
		if (optflgs & Fflg) {
		// -F option.
		force_flag_loser = true;
		} else {
		if (strcmp(gettext("yes"), "yes") == 0 &&
			strcmp(gettext("no"), "no") == 0) {
			// The l10n language is english, so
			// we can stick with 'y' and 'n'.
			(void) clmessage("This command discards configuration "
				"changes on \"%s\" and enables the ability to "
				"form the 2-node cluster. Is it okay to "
				"proceed (y/n)[n]? ", node_name);

			answer = fgets(replybuf, sizeof (replybuf) - 1,
				stdin);
			if (answer) {
			str_len = strlen(replybuf) - 1;
			if (*answer == '\n') {
					str_len = str_len + 1;
			}
			if (strncasecmp(replybuf, "y", str_len) == 0 ||
				strncasecmp(replybuf,
						"yes", str_len) == 0) {
				force_flag_loser = true;
			} else if (strncasecmp(replybuf, "n", str_len) == 0 ||
				strncasecmp(replybuf, "no", str_len) == 0 ||
				*answer == '\n') {
				// user has entered No or selected the default.
				force_flag_winner = false;
			} else {
				snprintf(replybuf, strlen(replybuf), replybuf);
				clerror("Invalid input \"%s\".\n", replybuf);
				clerrno = CL_EINVAL;
				goto cleanup;
			}
			}
		} else {
			// The l10n language is not english, so
			// we have to stick with 'yes' and 'no'
			(void) clmessage("This command discards configuration "
				"changes on \"%s\" and enables the ability to "
				"form the 2-node cluster. Is it okay to "
				"proceed (%s/%s)[%s]? ", node_name,
				gettext("yes"), gettext("no"), gettext("no"));

			answer = fgets(replybuf, sizeof (replybuf) - 1,
				stdin);
			if (answer) {
			while (str_len < BUFSIZ &&
				replybuf[str_len] != '\n') {
				str_len++;
			}
			if (*answer == '\n') {
					str_len = str_len + 1;
			}
			if (strncasecmp(replybuf, gettext("yes"),
				str_len) == 0) {
				force_flag_loser = true;
			} else if (strncasecmp(replybuf, gettext("no"),
				str_len) == 0 ||
				*answer == '\n') {
				// user has entered No or selected the default.
				force_flag_winner = false;
			} else {
				snprintf(replybuf, strlen(replybuf), replybuf);
				clerror("Invalid input \"%s\".\n", replybuf);
				clerrno = CL_EINVAL;
				goto cleanup;
			}
			}
		}
		}
	}

	if (force_flag_winner) {
		err_flg = mark_ccr_as_winner();
	} else if (force_flag_loser) {
		err_flg = mark_ccr_as_loser();
	}

	if (err_flg) {
		if (first_err == CL_NOERR) {
			first_err = CL_EINVAL;
		}
	}

	// Verbose option
	if ((first_err == CL_NOERR) && (optflgs & vflg)) {
		if (force_flag_winner) {
			(void) clmessage("Node \"%s\" marked as "
				"winner node.\n", node_name);
		} else if (force_flag_loser) {
			(void) clmessage("Node \"%s\" marked as "
				"joining node.\n", node_name);
		}
	}

	if (first_err != CL_NOERR) {
		clerrno = first_err;
	}

cleanup:

	return (clerrno);
}
