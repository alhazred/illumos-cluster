//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clnode_remove_farm.cc	1.3	08/05/20 SMI"

//
// Process clnode "remove-farm"
//

#include "clnode.h"

//
// clnode "remove-farm"
//
clerrno_t
clnode_remove_farm(ClCommand &cmd, ValueList &operands, optflgs_t optflgs)
{
	clerrno_t clerrno = CL_NOERR;
	NameValueList node_list = NameValueList(true);
	ValueList node_type;
	clerrno_t first_err = CL_NOERR;
	scconf_errno_t scconf_err = SCCONF_NOERR;
	char *msgbuffer = (char *)0;

	// Get the valid farm nodes from the user inputs
	if (operands.getSize() == 0) {
		node_type.add(NODETYPE_FARM);
	}
	clerrno = clnode_preprocess_nodelist(operands, node_type,
	    node_list, true);
	// Nothing to remove?
	if (node_list.getSize() == 0) {
		return (clerrno);
	}

	if (clerrno != CL_NOERR) {
		first_err = clerrno;
	}

	// Open the Europa libraries
	clerrno = clnode_open_europa();
	if (clerrno != CL_NOERR) {
		if (first_err == CL_NOERR) {
			first_err = clerrno;
		}
		goto cleanup;
	}

	// Process the nodes
	for (node_list.start(); !node_list.end(); node_list.next()) {
		char *node_name = node_list.getName();
		char *node_type = node_list.getValue();

		if (!node_name || !node_type) {
			continue;
		}

		// Check the node
		if (strcmp(node_type, NODETYPE_FARM) != 0) {
			clerror("You cannot remove %s node "
			    "\"%s\".\n",
			    node_type, node_name);
			if (first_err == CL_NOERR) {
				first_err = CL_EOP;
			}
			continue;
		}

		// Call scconf_rm_farmnode()
		scconf_err = scconf_rm_farmnode(node_name, &msgbuffer);

		// Print detailed messages
		if (msgbuffer) {
			clcommand_dumpmessages(msgbuffer);
			free(msgbuffer);
			msgbuffer = (char *)0;
		}

		if (scconf_err != SCCONF_NOERR) {
			switch (scconf_err) {
			// Error messages are printed in msgbuffer
			case SCCONF_ENOEXIST:
				clerrno = CL_ENOENT;
				break;
			case SCCONF_EINUSE:
				clerrno = CL_EBUSY;
				break;
			case SCCONF_EUNEXPECTED:
				clerrno = CL_EINTERNAL;
				break;
			default:
				clerrno = CL_EINTERNAL;
				clerror("An unexpected error occurred; "
				    "failed to remove %s node \"%s\".\n",
				    NODETYPE_FARM, node_name);
				break;
			}

			if (first_err == CL_NOERR) {
				first_err = clerrno;
			}
		} else if (cmd.isVerbose) {
			clmessage("Farm node \"%s\" is removed.\n",
			    node_name);
		}
	}

cleanup:
	(void) clnode_close_europa();

	// Return the first error
	return (first_err);
}
