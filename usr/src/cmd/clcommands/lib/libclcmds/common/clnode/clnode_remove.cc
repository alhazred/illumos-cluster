//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clnode_remove.cc	1.4	08/05/20 SMI"

//
// Process clnode "remove"
//

#include "clnode.h"
#include "sys/stat.h"

#define	CMD_SCINSTALL	"/usr/cluster/bin/scinstall"
#define	CMD_SCRCONF	"/usr/cluster/lib/sc/scrconf"

//
// clnode "remove"
//
//	Does not remove the cluster packages.
//
clerrno_t
clnode_remove(ClCommand &cmd, ValueList &operands, optflgs_t optflgs,
    char *sponsornode, char *gmntpoint)
{
	clerrno_t clerrno = CL_NOERR;
	clerrno_t first_err = CL_NOERR;
	char cli_cmd[SCCONF_MAXSTRINGLEN];
	char tmp_buf[SCCONF_MAXSTRINGLEN];
	int tmp_err = 0;
	char *node_name = (char *)0;
	char **cli_envs = new char *[3];
	char **cli_args;
	int i, arg_count, env_count;
	char *cli_save, *chk_string;
	char *global_fs = (char *)0;
	char cwd_path[PATH_MAX + 1] = "";
	char *progname = (char *)0;

	arg_count = env_count = 0;

	// Check inputs
	if (operands.getSize() != 1) {
		clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}

	// Check sponsornode
	if (sponsornode) {
		// Cannot be same as the local node
		operands.start();
		node_name = operands.getValue();
		if (strcmp(sponsornode, node_name) == 0) {
			clerror("The sponsornode \"%s\" cannot be the "
			    "node to remove.\n", sponsornode);
			if (first_err == CL_NOERR) {
				first_err = CL_EINVAL;
			}
		} else {
			FILE *fd_p;

			// Set args to call scrconf
			(void) sprintf(cli_cmd,
			    "%s -x 10 -N %s >/dev/null 2>&1",
			    CMD_SCRCONF, sponsornode);
			if ((fd_p = popen(cli_cmd, "r")) == NULL) {
				clerror("Cannot check node \"%s\".\n",
				    sponsornode);
				if (first_err == CL_NOERR) {
					first_err = CL_EINTERNAL;
				}
			} else {
				if (pclose(fd_p) != 0) {
					clerror("Cannot communicate with node "
					    "\"%s\".\n", sponsornode);
					if (first_err == CL_NOERR) {
						first_err = CL_EINVAL;
					}
				}
			}
			*cli_cmd = '\0';
		}
	}

	// The command to execute
	(void) sprintf(cli_cmd, "%s -r -k ", CMD_SCINSTALL);
	if (sponsornode && (first_err == CL_NOERR)) {
		(void) sprintf(tmp_buf, "-N %s ", sponsornode);
		(void) strcat(cli_cmd, tmp_buf);
	}

	// Add the global FS
	if (gmntpoint) {
		global_fs = new char[strlen(gmntpoint) + 1];
		(void) sprintf(global_fs, "%s", gmntpoint);
		// Remove the leading /
		if (*global_fs == '/') {
			while (*global_fs == '/') global_fs++;
			global_fs--;
		}

		if (global_fs && (strcmp(global_fs, "/globaldevices") != 0)) {
			struct stat gmnt_stat;
			if (stat(global_fs, &gmnt_stat) < 0) {
				clerror("Cannot access \"%s\".\n", global_fs);
				if (first_err == CL_NOERR) {
					first_err = CL_EINVAL;
				}
			} else if (!S_ISDIR(gmnt_stat.st_mode)) {
				clerror("\"%s\" is not a file system mount "
				    "point.\n", global_fs);
				if (first_err == CL_NOERR) {
					first_err = CL_EINVAL;
				}
			}

			// Add as CLI parameter
			if (first_err == CL_NOERR) {
				(void) sprintf(tmp_buf, "-G %s ", global_fs);
				(void) strcat(cli_cmd, tmp_buf);
			}
		}
	}

	// Check the current working directory
	if (getcwd(cwd_path, sizeof (cwd_path))) {
		if (strstr(cwd_path, "/etc")) {
			if (strcmp(cwd_path, strstr(cwd_path, "/etc")) == 0) {
				// Cannot be inside /etc direcoty
				clerror("You must not be inside directory /etc "
				    "when remove this node.\n");
				if (first_err == CL_NOERR) {
					first_err = CL_EOP;
				}
			}
		}
	}

	// Errors?
	if (first_err != CL_NOERR) {
		goto cleanup;
	}

	// Set the envs
	if ((progname = strrchr(cmd.argv[0], '/')) == NULL) {
		progname = cmd.argv[0];
	} else {
		++progname;
	}
	cli_envs[env_count] = new char [strlen(progname) + 6];
	(void) sprintf(cli_envs[env_count++], "PROG=%s", progname);
	if (optflgs & Fflg) {
		cli_envs[env_count] = new char [strlen("SC_FORCE_RM=yes") + 1];
		(void) strcpy(cli_envs[env_count++], "SC_FORCE_RM=yes");
	}
	cli_envs[env_count] = NULL;

	// Find out how many arguments are specified
	arg_count = 0;
	cli_save = new char [strlen(cli_cmd) + 1];
	(void) strcpy(cli_save, cli_cmd);

	chk_string = strtok(cli_cmd, " ");
	while ((chk_string = strtok(NULL, " ")) != NULL) arg_count++;

	// Set the args to call scinstall.
	cli_args = new char *[arg_count + 2];
	chk_string = strtok(cli_save, " ");
	arg_count = 0;
	while (chk_string != NULL) {
		cli_args[arg_count] = new char [strlen(chk_string) + 1];
		(void) strcpy(cli_args[arg_count++], chk_string);

		chk_string = strtok(NULL, " ");
	}
	cli_args[arg_count] = NULL;
	delete [] cli_save;

	// Call the command
	clerrno = execve_command(CMD_SCINSTALL, cli_args, cli_envs);
	if ((clerrno == 1) && (first_err == CL_NOERR)) {
		first_err = CL_EINVAL;
	}

cleanup:
	if (first_err != CL_NOERR) {
		clerrno = first_err;
	}

	for (i = 0; i < arg_count; i++) {
		delete [] cli_args[i];
	}
	if (arg_count && cli_args) {
		delete [] cli_args;
	}

	for (i = 0; i < env_count; i++) {
		delete [] cli_envs[i];
	}
	delete [] cli_envs;

	if (global_fs)
		delete [] global_fs;

	return (clerrno);
}
