//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clnode_evacuate.cc	1.6	08/10/07 SMI"

//
// Process clnode "evacuate"
//

#ifdef __cplusplus
#include <sys/os.h>
#endif
#include "scswitch.h"
#include "rgm/scha_priv.h"
#include "clnode.h"

#if SOL_VERSION >= __s10
#include <zone.h>
#include <sys/cladm_int.h>
#include <sys/quorum_int.h>
#include <sys/clconf_int.h>
#endif

//
// clnode "evacuate"
//
clerrno_t
clnode_evacuate(ClCommand &cmd, ValueList &operands, optflgs_t optflgs,
    ushort_t evac_timeout)
{
	clerrno_t clerrno = CL_NOERR;
	scha_errmsg_t scha_err = { SCHA_ERR_NOERR, NULL };
	scswitch_errbuff_t scswitch_err = { SCSWITCH_NOERR, NULL };
	scconf_errno_t scconf_err = SCCONF_NOERR;
	scconf_nodeid_t nodeid;
	char *nodename;
	char err_buf[SCCONF_MAXSTRINGLEN];
	boolean_t verbose;

	if (optflgs & vflg)
		verbose = B_TRUE;
	else
		verbose = B_FALSE;

	// Get the input
	if (operands.getSize() != 1) {
		clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}

	operands.start();
	nodename = operands.getValue();

	// Get the node id
	scconf_err = scconf_get_nodeid(nodename, &nodeid);
	if (scconf_err != SCCONF_NOERR) {
		switch (scconf_err) {
		case SCCONF_ENOEXIST:
			clerror("Node \"%s\" does not exist.\n", nodename);
			clerrno = CL_ENOENT;
			break;
		default:
			clerrno = CL_EINTERNAL;
			clcommand_perror(clerrno);
			break;
		} //lint !e788
		return (clerrno);
	}

	// Evacuate the resource groups
	scha_err = scswitch_evacuate_rgs(nodename, evac_timeout, verbose, NULL);
	if (scha_err.err_msg) {
		//
		// scswitch_evacuate_rgs() already compose warning messages
		// in scha_err.err_msg. The return code is normally 0.
		//
		(void) fprintf(stderr, "%s:  %s\n",
		    cmd.getCommandName(), scha_err.err_msg);
		free(scha_err.err_msg);
	} else if (scha_err.err_code != SCHA_ERR_NOERR) {
		char *scha_msg = NULL;

		//  obtain error message from rgm
		scha_msg = scha_strerror(scha_err.err_code);
		if (scha_msg) {
			(void) fprintf(stderr, "%s:  %s\n",
			    cmd.getCommandName(), scha_msg);
		}

		return (CL_EINTERNAL);
	}

	//
	// Skip the device group evacuation if inside a zc
	//
#if (SOL_VERSION >= __s10)
	if (getzoneid() != GLOBAL_ZONEID) {
		goto skip_global;
	}
#endif

	// Evacuate device groups for the node
	scswitch_err = scswitch_evacuate_devices(nodeid);
	if (scswitch_err.err_code != SCSWITCH_NOERR) {
		// Print the error messages in scswitch_err
		if (scswitch_err.err_msg) {
			(void) fprintf(stderr, "%s:  %s\n",
			    cmd.getCommandName(), scswitch_err.err_msg);
			free(scswitch_err.err_msg);
		}

		// Get the corresponding error messages for the error code
		*err_buf = '\0';
		scswitch_strerr(NULL, err_buf, scswitch_err.err_code);
		if (err_buf && *err_buf) {
			(void) fprintf(stderr, "%s:  %s\n",
			    cmd.getCommandName(), err_buf);
		}
		clerrno = CL_EINTERNAL;
	}

skip_global:
	return (clerrno);
}
