//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clnode_export.cc	1.3	08/05/20 SMI"

//
// Process clnode "export"
//

#include "clnode.h"
#include "ClXml.h"

#define	CLNODE_CLUSTER	1
#define	CLNODE_FARM	2

static clerrno_t
clnode_get_servernode_xml(clconf_cluster_t *clconf, char *node_name,
    ClnodeExport *clnode_export_obj);
static clerrno_t
clnode_get_farmnode_xml(scconf_farm_nodelist_t *farm_nodelist,
    char *node_name, ClnodeExport *clnode_export_obj);
static clerrno_t
clnode_get_node_xml(ValueList &node_names, ValueList &node_types,
    ClnodeExport *clnode_export_obj, bool *create_cnode,
    bool *create_fnode);

//
// get_clnode_xml_elements
//
// This function returns a ClnodeExport object populated with
// all of the node information from the cluster.  Caller is
// responsible for releasing the ClcmdExport object.
//
ClcmdExport *
get_clnode_xml_elements()
{
	ClnodeExport *node = new ClnodeExport();
	ValueList empty_list;
	bool create_cnode = false;
	bool create_fnode = false;

	// Get the xml configuration for all nodes
	(void) clnode_get_node_xml(empty_list, empty_list,
	    node, &create_cnode, &create_fnode);

	return (node);
}

//
// clnode_export
//
//	Export the node confifuration data to xml format, and
//	output it to clconfiguration file. The nodes are filtered
//	based on the user specified nodes and node types.
//
clerrno_t
clnode_export(ClCommand &cmd, ValueList &operands, optflgs_t optflgs,
    char *clconfiguration, ValueList &nodetypes)
{
	ClXml clnode_clxml;
	ClnodeExport *clnode_export_obj = new ClnodeExport();
	clerrno_t clerrno = CL_NOERR;
	clerrno_t first_err = CL_NOERR;
	char *export_file = (char *)0;
	bool create_cnode = false;
	bool create_fnode = false;
	ValueList create_node;

	// Check inputs
	if (!clconfiguration) {
		export_file = strdup("-");
	} else {
		export_file = strdup(clconfiguration);
	}
	if (!export_file) {
		clerrno = first_err = CL_ENOMEM;
		clcommand_perror(clerrno);
		goto cleanup;
	}

	clerrno = clnode_get_node_xml(operands, nodetypes,
	    clnode_export_obj, &create_cnode, &create_fnode);
	if (clerrno != CL_NOERR) {
		if (first_err == CL_NOERR) {
			first_err = clerrno;
		}
	}

	// Create xml
	if (create_cnode) {
		create_node.add(NODETYPE_SERVER);
	}
	if (create_fnode) {
		create_node.add(NODETYPE_FARM);
	}
	if (create_node.getSize() != 0) {
		clerrno = clnode_clxml.createExportFile(CLNODE_CMD,
		    export_file, clnode_export_obj, &create_node);
		if ((clerrno != CL_NOERR) && (first_err == CL_NOERR)) {
			first_err = clerrno;
		}
	}

cleanup:
	// Free memory
	if (export_file) {
		free(export_file);
	}

	delete clnode_export_obj;

	// Return the first error
	return (first_err);
}

//
// clnode_get_servernode_xml
//
//	Get the node configuration data, and add them into the
//	ClnodeExport object.
//
static clerrno_t
clnode_get_servernode_xml(clconf_cluster_t *clconf, char *node_name,
    ClnodeExport *clnode_export_obj)
{
	int i;
	clerrno_t clerrno = CL_NOERR;
	clconf_iter_t *currnodesi, *nodesi = (clconf_iter_t *)0;
	clconf_obj_t *node;
	scconf_nodeid_t conf_nodeid;
	const char *conf_val;
	const char *node_ccrname;
	char conf_nodeid_str[SCCONF_MAXSTRINGLEN];

	// Check inputs
	if (!node_name || !clconf || (clnode_export_obj == NULL)) {
		return (CL_EINTERNAL);
	}

	// Read the nodes
	nodesi = currnodesi = clconf_cluster_get_nodes(clconf);
	while ((node = clconf_iter_get_current(currnodesi)) != NULL) {
		clconf_iter_advance(currnodesi);

		// Get the node name
		conf_val = clconf_obj_get_name(node);
		if (!conf_val)
			continue;

		// get the node id
		conf_nodeid = (scconf_nodeid_t)clconf_obj_get_id(node);
		if (conf_nodeid == (scconf_nodeid_t)-1) {
			clerrno = CL_EINTERNAL;
			goto cleanup;
		}
		// Convert the nodeid to a string
		(void) sprintf(conf_nodeid_str, "%d", conf_nodeid);

		if ((strcmp(conf_val, node_name) != 0) &&
		    (strcmp(conf_nodeid_str, node_name) != 0))
			continue;

		// Add the node into the ClnodeExport object.
		node_ccrname = conf_val;
		clerrno = clnode_export_obj->createNode((char *)node_ccrname,
		    conf_nodeid_str);
		if (clerrno != CL_NOERR) {
			goto cleanup;
		}

		// Add all the properties
		for (i = 0; clnode_props[i].ccr_name; i++) {
			// Skip farm nodes
			if (strcmp(clnode_props[i].node_type,
			    NODETYPE_FARM) == 0) {
				continue;
			}

			conf_val = clconf_obj_get_property(node,
			    clnode_props[i].ccr_name);

			if (conf_val) {
				(void) clnode_export_obj->addProperty(
				    (char *)node_ccrname,
				    clnode_props[i].cli_name,
				    (char *)conf_val,
				    clnode_props[i].read_only);
			}
		}
		break;
	}

cleanup:
	if (nodesi != (clconf_iter_t *)0) {
		clconf_iter_release(nodesi);
	}

	return (clerrno);
}

//
// clnode_get_farmnode_xml
//
//	Get the farm node configuration data and save it into the ClnodeExport
//	object.
//
clerrno_t
clnode_get_farmnode_xml(scconf_farm_nodelist_t *farm_nodelist,
    char *node_name, ClnodeExport *clnode_export_obj)
{
	clerrno_t clerrno = CL_NOERR;
	scconf_farm_nodelist_t *farm_node;
	char node_id[BUFSIZ];
	bool monitored;
	char *adapter1 = (char *)0;
	char *adapter2 = (char *)0;

	// Check inputs
	if (!farm_nodelist || !node_name || !clnode_export_obj) {
		clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}

	// Go through the farm nodes
	for (farm_node = farm_nodelist; farm_node;
	    farm_node = farm_node->scconf_node_next) {
		// Check the node name
		if (!farm_node->scconf_node_nodename ||
		    (strcmp(farm_node->scconf_node_nodename, node_name) != 0)) {
			continue;
		}

		// Get the nodeid
		(void) sprintf(node_id, "%d",
		    farm_node->scconf_node_nodeid);

		// Monitored?
		if (farm_node->scconf_node_monstate ==
		    SCCONF_STATE_ENABLED) {
			monitored = true;
		} else {
			monitored = false;
		}

		// Get the adapters
		if (farm_node->scconf_node_adapters) {
			adapter1 = new char[strlen(
			    farm_node->scconf_node_adapters) + 1];
			(void) sprintf(adapter1,
			    farm_node->scconf_node_adapters);
			adapter2 = strchr(adapter1, ' ');
			if (adapter2) {
				*adapter2++ = '\0';
			}
		}

		// Add this node to ClnodeExport
		clerrno = clnode_export_obj->createFarmNode(node_name,
		    node_id, monitored, adapter1, adapter2);
		break;
	}

	return (clerrno);
}

//
// clnode_get_node_xml
//
//	Get the ClnodeExport object for the given node list.
//
clerrno_t
clnode_get_node_xml(ValueList &node_names, ValueList &node_types,
    ClnodeExport *clnode_export_obj, bool *create_cnode,
    bool *create_fnode)
{
	clerrno_t clerrno = CL_NOERR;
	clerrno_t first_err = CL_NOERR;
	NameValueList node_list = NameValueList(true);
	clconf_cluster_t *clconf = (clconf_cluster_t *)0;
	scconf_errno_t scconf_err = SCCONF_NOERR;
	char *node_name;
	char *node_type;
	boolean_t iseuropa = B_FALSE;
	scconf_farm_nodelist_t *farm_nodelist = (scconf_farm_nodelist_t *)0;

	// Check inputs
	if (!clnode_export_obj || !create_cnode || !create_fnode) {
		clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}

	*create_cnode = *create_fnode = false;

	// Get the valid nodes from the user inputs
	clerrno = clnode_preprocess_nodelist(node_names, node_types,
	    node_list, true);
	if (clerrno != CL_NOERR) {
		if (first_err == CL_NOERR) {
			first_err = clerrno;
		}
	}

	// If nothing to export?
	if (node_list.getSize() == 0) {
		return (first_err);
	}

	// Get the clconf hanlder
	scconf_err = scconf_cltr_openhandle(
	    (scconf_cltr_handle_t *)&clconf);
	if (scconf_err != SCCONF_NOERR) {
		clerrno = clnode_conv_scconf_err(1, scconf_err);
		if (first_err == CL_NOERR) {
			first_err = clerrno;
		}
	}

	// Is Europa configured?
	clerrno = clnode_check_europa(&iseuropa);
	if (clerrno != CL_NOERR) {
		if (first_err == CL_NOERR) {
			first_err = clerrno;
		}
	} else if (iseuropa) {
		// Open europa libraries
		clerrno = clnode_open_europa();
		if (clerrno != CL_NOERR) {
			if (first_err == CL_NOERR) {
				first_err = clerrno;
			}
		} else {
			// Get the farm nodes
			scconf_err = scconf_get_farmnodes(&farm_nodelist);
			if (scconf_err != SCCONF_NOERR) {
				clerrno = clnode_conv_scconf_err(1, scconf_err);
				if (first_err == CL_NOERR) {
					first_err = clerrno;
				}
			}
			(void) clnode_close_europa();
		}
	}

	// Process the nodes
	for (node_list.start(); node_list.end() != 1; node_list.next()) {
		node_name = node_list.getName();
		node_type = node_list.getValue();
		if (!node_name || !node_type)
			continue;

		// Get the configuration data for this node to ClnodeExport.
		if (strcmp(node_type, NODETYPE_SERVER) == 0) {
			clerrno = clnode_get_servernode_xml(clconf, node_name,
			    clnode_export_obj);
			if (clerrno == CL_NOERR) {
				*create_cnode = true;
			}
		}
		if (strcmp(node_type, NODETYPE_FARM) == 0) {
			clerrno = clnode_get_farmnode_xml(farm_nodelist,
			    node_name, clnode_export_obj);
			if (clerrno == CL_NOERR) {
				*create_fnode = true;
			}
		}
		if (clerrno != CL_NOERR) {
			if (first_err == CL_NOERR) {
				first_err = clerrno;
			}
			clcommand_perror(clerrno);
			continue;
		}
	}

cleanup:
	// Release the cluster config
	if (clconf != (clconf_cluster_t *)0) {
		clconf_obj_release((clconf_obj_t *)clconf);
	}

	if (farm_nodelist) {
		scconf_free_farm_nodelist(farm_nodelist);
	}

	return (first_err);
}
