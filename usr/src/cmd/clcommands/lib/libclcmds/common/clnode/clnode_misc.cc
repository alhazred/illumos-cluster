//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clnode_misc.cc	1.12	08/10/07 SMI"

//
// Miscellaneous functions for clnode
//

#include "clnode.h"
#include "scconf_private.h"
#include "wait.h"
#include "scha.h"
#include "scha_err.h"
#include "common.h"

#if (SOL_VERSION >= __s10)
#include <libzccfg/libzccfg.h>
#endif

static clerrno_t
clnode_get_server_nodes(NameValueList &nlist_server);
static clerrno_t
clnode_get_farm_nodes(NameValueList &nlist_farm, bool print_err);

// Initialization of node property table.
clnode_proplist_t clnode_props[] = {
	{NODEPROP_PRIVHOST, "private_hostname", NODETYPE_SERVER, false},
	{NODEPROP_ZPRIVHOST, "zprivate_hostname", NODETYPE_SERVER, false},
	{NODEPROP_FAILOVER_DPM, NODEPROP_FAILOVER_DPM, NODETYPE_SERVER, false},
	{NODEPROP_ZONESHARE, PROP_NODE_SCSLM_GLOBAL_SHARES,
		NODETYPE_SERVER, false},
	{NODEPROP_PSETMIN, PROP_NODE_SCSLM_DEFAULT_PSET,
		NODETYPE_SERVER, false},
	{PROP_NODE_QUORUM_VOTE, PROP_NODE_QUORUM_VOTE, NODETYPE_SERVER, true},
	{PROP_NODE_QUORUM_DEFAULTVOTE, PROP_NODE_QUORUM_DEFAULTVOTE,
		NODETYPE_SERVER, true},
	{PROP_NODE_QUORUM_RESV_KEY, PROP_NODE_QUORUM_RESV_KEY,
		NODETYPE_SERVER, true},
	{NODEPROP_MONITOR, NODEPROP_MONITOR, NODETYPE_FARM, false},
	{NODEPROP_PHYS_HOST, NODEPROP_PHYS_HOST, NODETYPE_ZONE_CLUSTER, false},
	{(char *)0, (char *)0, false}
};

//
// clnode_conv_scconf_err
//
//	Convert node related scconf_errno_t to CL error.   If
//	the "printflag" is set, print an error.
//
clerrno_t
clnode_conv_scconf_err(int printflag, scconf_errno_t err)
{
	// Map the error
	switch (err) {
	case SCCONF_NOERR:
		return (CL_NOERR);

	case SCCONF_EINVAL:
		if (printflag)
			clerror("Invalid value.\n");
		return (CL_EINVAL);

	case SCCONF_EINSTALLMODE:
		if (printflag)
			clerror("Cluster is in install mode.\n");
		return (CL_ESTATE);

	case SCCONF_ENOMEM:
	case SCCONF_EOVERFLOW:
		if (printflag)
			clcommand_perror(CL_ENOMEM);
		return (CL_ENOMEM);

	case SCCONF_EBADVALUE:
		if (printflag)
			clerror("Bad value in configuration database.\n");
		return (CL_EINTERNAL);

	case SCCONF_ERECONFIG:
		if (printflag)
			clerror("Cluster is reconfiguring.\n");
		return (CL_ESTATE);

	default:
		if (printflag)
			clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}
}

//
// clnode_preprocess_nodelist
//
//	This is used to get the valid nodes into a NameValueList,
//	node_list, with <name, type> pair, based on the types and
//	nodes that are passed. Non-existing or invalid types and
//	nodes are printed in messages if "print_err" is set.
//	This function is also aware of zone clusters. If this
//	function is called inside a zone cluster, then it will
//	support only the type "zonecluster" and specifying any
//	other type might report an error depending the "print_err"
//	flag.
clerrno_t
clnode_preprocess_nodelist(ValueList &nodes, ValueList &nodetypes,
    NameValueList &node_list, bool print_err, ValueList &zc_names)
{
	clerrno_t clerrno = CL_NOERR;
	NameValueList nlist_server = NameValueList(true);
	NameValueList nlist_farm = NameValueList(true);
	int found;
	clerrno_t first_err = CL_NOERR;
	char *this_val;
	char *this_name;
	char *this_node;
	bool has_server, has_farm, has_zc, valid_node;
	ValueList valid_node_types;
	ValueList zone_clusters, zc_nodes, tmp_list;
	scconf_errno_t scconf_err = SCCONF_NOERR;
	boolean_t zc_flag, gz_flag, all_flag;
	char *node_name, *zc_name, *full_name;
	char *clustername = (char *)0;

	node_name = zc_name = full_name = NULL;
	zc_flag = all_flag = B_FALSE;
	gz_flag = B_TRUE;

	// Initialize the clconf library
	if (clconf_lib_init() != 0) {
		clerror("Cannot initialize clconf library.\n");
		return (CL_EINTERNAL);
	}

	// Zone cluster names can be specified either with the -Z
	// option or in the form of <zonecluster>:<node name> format.
	// But, both the ways cannot be used simultaneously.
	// If zone clusters were not specified with the -Z option, we
	// have to check whether they were specified in the <zc>:<node>
	// format.
	for (nodes.start(); !nodes.end(); nodes.next()) {
		scconf_err = scconf_parse_obj_name_from_cluster_scope(
				nodes.getValue(), &node_name, &zc_name);

		if (scconf_err != SCCONF_NOERR) {
			return (CL_ENOMEM);
		}

		if (zc_name && zc_names.getSize()) {
			// -Z flag was specified and this node was scoped
			// with a zone cluster name as well. Report an
			// error.
			clerror("You cannot use the "
			    "\"<zonecluster>:<nodename>\" form of the "
			    "node name with the \"-Z\" option.");
			free(node_name);
			free(zc_name);
			return (CL_EINVAL);
		}

		// If this zone cluster name does not exist in tmp_list,
		// then we wil add it here.
		if (!tmp_list.isValue(zc_name)) {
			tmp_list.add(zc_name);
		}

		if (node_name) {
			free(node_name);
			node_name = NULL;
		}
		if (zc_name) {
			free(zc_name);
			zc_name = NULL;
		}
	}
#if (SOL_VERSION >= __s10)
	// By the time we are here, we have added any zone cluster name in the
	// node names to tmp_list.
	if (zc_names.getSize()) {
		// Zc names were specified. We have to check whether "all"
		// was specified.
		zc_names.start();

		if (strcmp(zc_names.getValue(), ALL_CLUSTERS_STR) == 0) {
			all_flag = B_TRUE;
			// Get all known zone clusters.
			clerrno = get_all_clusters(zone_clusters);

			// We are fetching the zone names for the current zone
			// cluster. For the global zone it will fetch all the
			// zone clusters including the global zone.
			zc_names.clear();

			for (zone_clusters.start(); !zone_clusters.end();
				zone_clusters.next()) {
				zc_names.add(zone_clusters.getValue());
			}

			if (clerrno != CL_NOERR) {
				return (clerrno);
			}
		} else {
			tmp_list = zc_names;
		}
	}

	// valid_zc_list will contain at least one zone cluster name
	// if "all" was specified. If it is empty, we have to check
	// the validity of the zone clusters specified by the user.
	if (zone_clusters.getSize() < 1) {
		clerrno = get_valid_zone_clusters(tmp_list, zone_clusters);
	}
	// Clear the memory.
	tmp_list.clear();

	if (clerrno) {
		// There was at least one invalid zone cluster.
		// We have to return.
		return (clerrno);
	}

	// Check whether we are inside a zone cluster or not
	scconf_err = scconf_zone_cluster_check(&zc_flag);
	if (scconf_err != SCCONF_NOERR) {
		// If print_err was set, we have to report an error
		if (print_err) {
			clerror("Internal error.\n");
		}

		return (CL_EINTERNAL);
	}
	if (sc_nv_zonescheck()) {
		// We are inside a zone
		gz_flag = B_FALSE;
	}
#endif
	// First fetch the cluster name
	if (zc_flag) {
		// We are in a zone cluster
		clustername = clconf_get_clustername();
		if (!clustername) {
			clerror("Cannot get the cluster name.\n");
			return (CL_EINTERNAL);
		}
	} else {
		// If we are not in a zone cluster, we should be in
		// the global zone
		clustername = strdup(GLOBAL_CLUSTER_NAME);
	}

	// Fetch valid node types
	(void) clnode_get_supported_node_types(valid_node_types);

	// Check node types
	// These checks apply only from the global zone.
	for (nodetypes.start(); !nodetypes.end(); nodetypes.next()) {
		this_val = nodetypes.getValue();
		if (!this_val)
			continue;

		// If nodetypes was specified and we are in a zone cluster,
		// we will support only the type "zonecluster"
		if (zc_flag && strcmp(this_val, NODETYPE_ZONE_CLUSTER)) {

			if (print_err) {
				clerror("Invalid node type \"%s\".\n",
					this_val);
			}

			if (first_err == CL_NOERR) {
				first_err = CL_ETYPE;
			}

			continue;
		}

		// We are in the global zone. Check the type
		if (valid_node_types.isValue(this_val)) {
			// if we are here then it is a valid node
			continue;
		}

		// if we are here then this it is an invalid node
		if (print_err) {
			clerror("Invalid node type \"%s\".\n",
				this_val);
		}

		if (first_err == CL_NOERR) {
			first_err = CL_ETYPE;
		}
	}

	// Please note that the server nodes (i.e; nodes of the physical
	// cluster) is obtained from the function clnode_get_server_nodes().
	// This function is virtualized, i.e:, this function will return
	// zone cluster nodes when called inside a zone cluster and will
	// return the physical cluster nodes when called from the global
	// zone. So, we have to handle the function call according to
	// the context from where we are calling it.

	// Set the type checking variables
	has_server = has_farm = has_zc = false;

	// If -Z flag was not specified and none of the opions was used,
	// then we will show both server and farm nodes.
	if (zc_names.getSize() == 0) {
		if ((nodetypes.getSize() == 0) ||
			nodetypes.isValue(NODETYPE_SERVER)) {
			has_server = true;
		}
		if ((nodetypes.getSize() == 0) ||
			nodetypes.isValue(NODETYPE_FARM)) {
			has_farm = true;
		}
	}
	// We will not show zone cluster nodes by default in the global zone
	if (nodetypes.isValue(NODETYPE_ZONE_CLUSTER)) {
		// The user can specify "--type zonecluster" while
		// issuing "clnode list" in a zone cluster. This has
		// to be handled here.

		if (zc_flag) {
			// Inside a zone cluster, clnode_get_server_nodes()
			// will return the correct set of nodelist. We have
			// to use that function call.
			has_server = true;
		} else {
			// We are in the global zone here.
			has_zc = true;
		}
	}
	// If the user specified "-Z all/global" in the global zone, then we
	// have to show the server nodes as well. Similarly, if the user
	// specified "-Z <zcname>" in a zone-cluster, where <zcname> is the
	// name of the zone cluster, then we have to show the server nodes
	// as well.
	if ((gz_flag == B_TRUE) && (all_flag == B_TRUE)) {
		has_server = true;
	}
	if ((zc_names.getSize() > 0) && (zc_names.isValue(clustername))) {
		has_server = true;
	}

	// By now, we have processed all the options and set values required
	// for further processing. We also have to remove the current cluster
	// name from "zone_clusters" since if the "-Z" flag was specified with
	// the current cluster name, a few of the methods used for fetching
	// zone cluster information might fail. Hence, removing the current
	// cluster name from "zone_clusters".
	if (zone_clusters.isValue(clustername)) {
		tmp_list = zone_clusters;
		zone_clusters.clear();

		for (tmp_list.start(); !tmp_list.end(); tmp_list.next()) {
			if (strcmp(clustername, tmp_list.getValue())) {
				zone_clusters.add(tmp_list.getValue());
			}
		}

		tmp_list.clear();
	}

	// Get the server nodes configured in the cluster
	clerrno = clnode_get_server_nodes(nlist_server);
	if ((clerrno != CL_NOERR) && (first_err == CL_NOERR)) {
		first_err = clerrno;
	}

	if ((clerrno != CL_NOERR) && (first_err == CL_NOERR)) {
		first_err = clerrno;
	}

	// Get the farm nodes configured in the cluster
	if (nodetypes.isValue(NODETYPE_FARM)) {
		clerrno = clnode_get_farm_nodes(nlist_farm, true);
	} else {
		clerrno = clnode_get_farm_nodes(nlist_farm, false);
	}

	// We will receive an CL_EOP when europa is not set up.
	if ((clerrno != CL_NOERR) && (first_err == CL_NOERR)) {
		first_err = clerrno;
	}

	// Go through the server nodes
	if (has_server) {
		for (nlist_server.start(); !nlist_server.end();
		    nlist_server.next()) {
			this_name = nlist_server.getName();
			this_val = nlist_server.getValue();
			if (!this_name || !this_val)
				continue;

			// Check with the user specified name and type
			if ((nodes.getSize() == 0) ||
			    nodes.isValue(this_name) ||
			    nodes.isValue(this_val)) {

				// If the user specified the "-Z" flag,
				// then we have to fully qualify the
				// nodename with the cluster name
				if (zc_names.getSize() > 0) {

					if (full_name) {
						free(full_name);
						full_name = NULL;
					}

					scconf_err =
					scconf_add_cluster_scope_to_obj_name(
						this_name, clustername,
						&full_name);
					if (scconf_err != SCCONF_NOERR) {
						if (first_err == CL_NOERR) {
							first_err =
								CL_EINTERNAL;
						}
					}
				} else {
					full_name = this_name;
				}
				// Add this name to node_list
				(void) node_list.add(full_name,
				    NODETYPE_SERVER);
			}
		}
	}

	// Go through the farm nodes
	if (has_farm) {
		for (nlist_farm.start(); !nlist_farm.end(); nlist_farm.next()) {
			this_name = nlist_farm.getName();
			this_val = nlist_farm.getValue();
			if (!this_name || !this_val)
				continue;

			// Check with the user specified name and type
			if ((nodes.getSize() == 0) ||
			    nodes.isValue(this_name) ||
			    nodes.isValue(this_val)) {
				(void) node_list.add(this_name, NODETYPE_FARM);
			}
		}
	}

#if (SOL_VERSION >= __s10)
	// Get the zone cluster nodes configured in the cluster
	if (nodetypes.isValue(NODETYPE_ZONE_CLUSTER) ||
		(zone_clusters.getSize() > 0)) {
		clerrno = clnode_get_zone_cluster_nodes(zc_nodes,
							print_err,
							zone_clusters);

		if ((clerrno != CL_NOERR) && (first_err == CL_NOERR)) {
			first_err = clerrno;
		} else {

			for (zc_nodes.start(); !zc_nodes.end();
				zc_nodes.next()) {
				this_name = zc_nodes.getValue();
				// Check with the user specified name and type
				if ((nodes.getSize() == 0) ||
				    nodes.isValue(this_name)) {
					node_list.add(this_name,
						NODETYPE_ZONE_CLUSTER);
				}
			} // for loop
		} // else
	}
#endif

	// Go through the passed "nodes" list.
	for (nodes.start(); !nodes.end(); nodes.next()) {
		this_node = nodes.getValue();
		if (!this_node)
			continue;

		found = 0;
		valid_node = false;

		// Check with server nodes
		for (nlist_server.start(); !nlist_server.end();
		    nlist_server.next()) {
			this_name = nlist_server.getName();
			this_val = nlist_server.getValue();
			if (!this_name || !this_val)
				continue;

			if ((strcmp(this_node, this_name) == 0) ||
			    (strcmp(this_node, this_val) == 0)) {
				valid_node = true;
				break;
			}
		}

		if (has_server && valid_node) {
			found = 1;
		}

		// Check with farm nodes
		if (!found && !valid_node && has_farm) {
			for (nlist_farm.start(); !nlist_farm.end();
			    nlist_farm.next()) {
				this_name = nlist_farm.getName();
				this_val = nlist_farm.getValue();
				if (!this_name || !this_val)
					continue;

				if ((strcmp(this_node, this_name) == 0) ||
				    (strcmp(this_node, this_val) == 0)) {
					found = 1;
					break;
				}
			}
		}

		// Check with zone clusters
		if (!found && has_zc) {

			if (zone_clusters.isValue(this_node)) {
				found = 1;
			}
		}

		// Invalid node
		if (!found) {
			if (print_err) {
				if (valid_node) {
					clerror("The type of node \"%s\" does "
					    "not match the specified type.\n",
					    this_node);
				} else {
					clerror("\"%s\" is not a valid "
					    "cluster node.\n",
					    this_node);
				}
			}
			if (first_err == CL_NOERR) {
				first_err = CL_ENOENT;
			}
		}
	}

cleanup:
	valid_node_types.clear();
	zone_clusters.clear();
	zc_nodes.clear();

	if (full_name) {
		free(full_name);
	}
	if (clustername) {
		free(clustername);
	}

	return (first_err);
}

//
// clnode_set_monitor
//
//	Monitoring or unmonitoring the farm nodes based on the set_flg.
//
clerrno_t
clnode_set_monitor(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, uint_t set_flg)
{
	clerrno_t clerrno = CL_NOERR;
	clerrno_t first_err = CL_NOERR;
	ValueList node_type;
	NameValueList node_list = NameValueList(true);
	NameValueList farmnodes = NameValueList(true);

	// Get all the nodes
	if (operands.getSize() == 0) {
		// Get the farm nodes
		node_type.add(NODETYPE_FARM);
	}
	clerrno = clnode_preprocess_nodelist(operands, node_type,
	    node_list, true);
	if (node_list.getSize() == 0) {
		return (clerrno);
	}
	if (clerrno != CL_NOERR) {
		first_err = clerrno;
	}

	// Check the nodes
	for (node_list.start(); !node_list.end(); node_list.next()) {
		char *node_name = node_list.getName();
		char *node_type = node_list.getValue();
		if (!node_name || !node_type)
			continue;

		// Only monitoring farm nodes
		if (strcmp(node_type, NODETYPE_FARM) != 0) {
			if (set_flg == SET_MONITOR) {
				clerror("You cannot monitor %s node "
				    "\"%s\".\n",
				    node_type, node_name);
			} else {
				clerror("You cannot unmonitor %s node "
				    "\"%s\".\n",
				    node_type, node_name);
			}
			if (first_err == CL_NOERR) {
				first_err = CL_EOP;
			}
		} else {
			farmnodes.add(node_name, NODETYPE_FARM);
		}
	}

	if (farmnodes.getSize() == 0) {
		return (first_err);
	}

	// Set farm node monitoring
	clerrno = clnode_set_farm_monstate(farmnodes, optflgs, set_flg);
	if ((clerrno != CL_NOERR) && (first_err == CL_NOERR)) {
		first_err = clerrno;
	}

	return (first_err);
}

//
// clnode_set_farm_monstate
//
//	Set the monitoring state value for the farm nodes in node_list.
//
clerrno_t
clnode_set_farm_monstate(NameValueList &node_list,
    optflgs_t optflgs, uint_t set_flg)
{
	clerrno_t clerrno = CL_NOERR;
	char monitoring[128];
	char *farmnodes = (char *)0;
	char *node_name, *node_type;
	char *msgbuffer = (char *)0;
	char *tmp_nodes = (char *)0;
	uint_t do_all = 0;
	scconf_errno_t scconf_err = SCCONF_NOERR;
	int node_number, count;

	// Monitor or unmonitor?
	if (set_flg == SET_MONITOR) {
		(void) sprintf(monitoring, MONITOR_ENABLED);
	} else if (set_flg == SET_UNMONITOR) {
		(void) sprintf(monitoring, MONITOR_DISABLED);
	} else {
		clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}

	// Get how many nodes
	node_number = node_list.getSize();
	if (node_number == 0) {
		do_all = 1;
	} else {
		tmp_nodes = node_list.getString(":");
		farmnodes = new char[strlen(tmp_nodes) + 1];
		delete [] tmp_nodes;

		// Get the farm node list
		*farmnodes = '\0';
		count = 1;
		for (node_list.start(); !node_list.end(); node_list.next()) {
			node_type = node_list.getValue();
			node_name = node_list.getName();
			if (!node_type || !node_name ||
			    (strcmp(node_type, NODETYPE_FARM) != 0)) {
				continue;
			}

			(void) strcat(farmnodes, node_name);
			if (count < node_number) {
				(void) strcat(farmnodes, ":");
			}
			count++;
		}
	}

	// Open Europa libraries
	clerrno = clnode_open_europa();
	if (clerrno != CL_NOERR) {
		goto cleanup;
	}

	// Change the monitoring state
	scconf_err = scconf_change_farm_monstate(farmnodes, monitoring,
	    do_all, &msgbuffer);

	// Most of the messages are printed to msgbuffer
	if (msgbuffer) {
		clcommand_dumpmessages(msgbuffer);
		free(msgbuffer);
		msgbuffer = (char *)0;
	}

	if (scconf_err != SCCONF_NOERR) {
		switch (scconf_err) {
		// Messages are already in msgbuffer
		case SCCONF_ENOEXIST:
			clerrno = CL_ENOENT;
			break;
		case SCCONF_EUNEXPECTED:
			clerrno = CL_EINTERNAL;
			break;
		default:
			clerrno = CL_EINTERNAL;
			if (set_flg == SET_MONITOR) {
				clerror("An unexpected error occurred; "
				    "failed to enable the %s node.\n",
				    NODETYPE_FARM);
			} else {
				clerror("An unexpected error occurred; "
				    "failed to disable the %s node.\n",
				    NODETYPE_FARM);
			}
			break;
		}
	}

cleanup:
	if (farmnodes) {
		delete [] farmnodes;
	}

	// close Europa libaries
	(void) clnode_close_europa();

	return (clerrno);
}

//
// clnode_get_server_nodes
//
//	Find all the server nodes configured in the system, and save
//	both the node name and node ID into the NameValueList.
//
clerrno_t
clnode_get_server_nodes(NameValueList &nlist_server)
{
	clerrno_t clerrno = CL_NOERR;
	clconf_cluster_t *clconf = (clconf_cluster_t *)0;
	clconf_iter_t *currnodesi, *nodesi = (clconf_iter_t *)0;
	clconf_obj_t *node;
	scconf_nodeid_t conf_nodeid;
	const char *conf_name;
	char conf_nodeid_str[SCCONF_MAXSTRINGLEN];

	// Get the handler
	if (scconf_cltr_openhandle((scconf_cltr_handle_t *)
	    &clconf) != SCCONF_NOERR) {
		clerrno = CL_EINTERNAL;
		goto cleanup;
	}

	// Read the nodes
	nodesi = currnodesi = clconf_cluster_get_nodes(clconf);
	while ((node = clconf_iter_get_current(currnodesi)) != NULL) {
		// Get the node name
		conf_name = clconf_obj_get_name(node);
		if (!conf_name) {
			clerrno = CL_EINTERNAL;
			goto cleanup;
		}

		// Get the nodeid
		conf_nodeid = (scconf_nodeid_t)clconf_obj_get_id(node);
		if (conf_nodeid == (scconf_nodeid_t)-1) {
			clerrno = CL_EINTERNAL;
			goto cleanup;
		}
		// Convert the nodeid to a string
		(void) sprintf(conf_nodeid_str, "%d", conf_nodeid);

		// Add into the name value list
		(void) nlist_server.add((char *)conf_name,
		    (char *)conf_nodeid_str);

		// Next node
		clconf_iter_advance(currnodesi);
	}

cleanup:
	// Release the cluster config
	if (clconf != (clconf_cluster_t *)0) {
		clconf_obj_release((clconf_obj_t *)clconf);
	}

	if (nodesi != (clconf_iter_t *)0) {
		clconf_iter_release(nodesi);
	}

	if (clerrno != CL_NOERR) {
		clcommand_perror(clerrno);
	}

	return (clerrno);
}

//
// clnode_check_europa
//
//	Check if europa is enabled; Set iseuropa to be
//	true if it is, and false otherwise.
//
clerrno_t
clnode_check_europa(boolean_t *iseuropa)
{
	// Check input
	if (!iseuropa) {
		clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}

	*iseuropa = B_FALSE;

	if (scconf_iseuropa(iseuropa) != SCCONF_NOERR) {
		clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}

	return (CL_NOERR);
}

//
// clnode_open_europa
//
//	dlopen europa libraries
//
clerrno_t
clnode_open_europa()
{
	clerrno_t clerrno = CL_NOERR;

	if (scconf_libscxcfg_open() != SCCONF_NOERR) {
		clerror("Failed to open LIBSCXCFG.\n");
		clerrno = CL_EINTERNAL;
	} else if (scconf_libfmm_open() != SCCONF_NOERR) {
		(void) scconf_libscxcfg_close();
		clerror("Failed to open LIBFMM.\n");
		clerrno = CL_EINTERNAL;
	}

	return (clerrno);
}

//
// clnode_close_europa
//
//	Close the Europa libraries
//
void
clnode_close_europa()
{
	(void) scconf_libfmm_close();
	(void) scconf_libscxcfg_close();
}

//
// clnode_get_farm_nodes
//
//	Find all the farm nodes configured in the system, and save
//	both the node name and node ID into the NameValueList.
//
clerrno_t
clnode_get_farm_nodes(NameValueList &nlist_farm, bool print_err)
{
	clerrno_t clerrno = CL_NOERR;
	scconf_farm_nodelist_t *farm_nodelist = (scconf_farm_nodelist_t *)0;
	scconf_farm_nodelist_t *farm_node;
	scconf_errno_t scconf_err = SCCONF_NOERR;
	char buf[128];
	boolean_t iseuropa = B_FALSE;

	// Is Euorpa configured?
	clerrno = clnode_check_europa(&iseuropa);
	if (clerrno != CL_NOERR) {
		return (clerrno);
	}
	if (!iseuropa) {
		if (print_err) {
			clerror("Farm node management is not configured.\n");
			return (CL_EOP);
		}

		return (clerrno);
	}

	// dlopen libscxcfg, libfmm
	clerrno = clnode_open_europa();
	if (clerrno != CL_NOERR) {
		goto cleanup;
	}

	// Get the farm nodes list
	scconf_err = scconf_get_farmnodes(&farm_nodelist);
	if (scconf_err != SCCONF_NOERR) {
		clerrno = clnode_conv_scconf_err(1, scconf_err);
		goto cleanup;
	}

	// Go through the node list
	for (farm_node = farm_nodelist; farm_node;
	    farm_node = farm_node->scconf_node_next) {
		if (!farm_node->scconf_node_nodename) {
			continue;
		}

		(void) sprintf(buf, "%d", farm_node->scconf_node_nodeid);
		nlist_farm.add(farm_node->scconf_node_nodename, buf);
	}

cleanup:
	scconf_free_farm_nodelist(farm_nodelist);
	(void) clnode_close_europa();

	return (clerrno);
}

//
// clnode_valid_prop
//
//	Check whether the prop_name is valid one by the node_type.
//	Return true is it is valid; and return false if not.
//
bool
clnode_valid_prop(char *prop_name, char *node_type)
{
	bool valid_prop = false;
	int i;

	// Check inputs
	if (!prop_name || !node_type) {
		return (valid_prop);
	}

	for (i = 0; clnode_props[i].cli_name; i++) {
		if (strcmp(node_type, clnode_props[i].node_type) != 0)
			continue;

		if (strcmp(prop_name, clnode_props[i].cli_name) == 0) {
			valid_prop = true;
			break;
		}
	}

	return (valid_prop);
}

//
// execute_command
//
//	Executes the given command with the given arguments.
//
clerrno_t
execve_command(char *cli_cmd, char **cli_args, char **cli_envs)
{
	pid_t cli_pid;
	int cli_stat;
	clerrno_t clerrno = CL_NOERR;

	// Check for inputs
	if (!cli_cmd || (cli_args == (char **)0)) {
		clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}

	cli_pid = fork1();
	if (cli_pid == -1) {
		clerror("Cannot fork.\n");
		return (CL_EINTERNAL);
	} else if (cli_pid == 0) {
		// Child

		if (cli_envs) {
			if (execve(cli_cmd, cli_args, cli_envs) == -1) {
				clerror("Cannot execute \"%s\" - %s.\n",
				    cli_cmd, strerror(errno));
				exit(CL_EINTERNAL);
			}
		} else {
			if (execv(cli_cmd, cli_args) == -1) {
				clerror("Cannot execute \"%s\" - %s.\n",
				    cli_cmd, strerror(errno));
				exit(CL_EINTERNAL);
			}
		}
	} else {
		// Wait for command to finish
		int status = 0;
		while ((status = waitpid(cli_pid, &cli_stat, 0)) != cli_pid) {
			if (status == -1 && errno == EINTR) {
				continue;
			} else {
				status = -1;
				break;
			}
		}
		if (status != cli_pid) {
			clerror("Error waiting for command result - %s\n",
			    strerror(errno));
			clerrno = CL_EINTERNAL;
		} else {
			clerrno = WEXITSTATUS(cli_stat);
		}
	}

	return (clerrno);
}

//
// clnode_get_ng_zones
//
//	Fetches the list of non-global zones hosted on the specified
//
clerrno_t
clnode_get_ng_zones(uint_t node_id, ValueList &zones_list)
{
	clerrno_t clerrno = CL_NOERR;
	scha_err_t rstatus = SCHA_ERR_NOERR;
	scha_cluster_t hdl;
	scha_str_array_t *all_nonglobal_node_zones;

	// Check whether the node id is valid or not
	if (node_id < 1) {
		return (CL_EINVAL);
	}

	// Get the scha handle
	rstatus = scha_cluster_open(&hdl);
	if (rstatus != SCHA_ERR_NOERR)
		return (CL_EINTERNAL);

	// Get the names of all the zones in this node
	rstatus = scha_cluster_get(hdl, SCHA_ALL_NONGLOBAL_ZONES_NODEID,
	    node_id, &all_nonglobal_node_zones);
	if (rstatus != SCHA_ERR_NOERR) {
		(void) scha_cluster_close(hdl);
		return (CL_EINTERNAL);
	}

	// Populate the zonelist with zone names and their hostnames
	for (int count = 0; count < (int)all_nonglobal_node_zones->array_cnt;
		count++) {

		zones_list.add(strdup(
			all_nonglobal_node_zones->str_array[count]));

	}

	// Free the handle
	(void) scha_cluster_close(hdl);
	return (clerrno);
}

//
// clnode_get_supported_node_types()
// This function will return the set of node types recognized
// by cluster. This function will add the names of the supported
// types to "node_types". Currently the supported types include
// "cluster, "farm" and "zonecluster". If your project is adding a new
// type of node to cluster, then you might want to add it to this
// list so that routines using this function to determine valid type
// of nodes do not reject your type as unknown.
// The caller of this function is responsible to free the memory
// of the ValueList.

clerrno_t
clnode_get_supported_node_types(ValueList &node_types)
{
	node_types.add(NODETYPE_SERVER);
	node_types.add(NODETYPE_FARM);
	node_types.add(NODETYPE_ZONE_CLUSTER);

	return (CL_NOERR);
}

#if (SOL_VERSION >= __s10)
//
// clnode_get_zone_cluster_nodes()
//
// This function will return the zones which form part
// of the zone cluster names, specified by "zc_names", in "zc_list"
// in the format "<zone cluster name>:<zone host name>".
// If "zc_names" is empty, this function will return all the zones
// across all the zone clusters. The caller of this function
// is responsible to free-up the memory of "zc_list". This
// function will not check for the validity of the zone cluster
// explicitly. Calling this function from a non-global zone will
// return an empty list of zone cluster nodes.
// Return Values :
//	CL_NOERR	- No Error.
//	CL_ENOENT	- One or more invalid zone cluster names
//	CL_EINTERNAL	- Internal Error.
//

clerrno_t
clnode_get_zone_cluster_nodes(ValueList &zc_list, bool print_err,
	ValueList zc_names)
{
	clerrno_t rstatus = CL_NOERR;
	clerrno_t first_err = CL_NOERR;
	char *zc_name;
	zc_dochandle_t zone_handle;
	int lib_clzone_err = 0;
	struct zc_nodetab nodetab;
	char *zc_node_name;
	int str_len_count;
	int vl_list_flag = 0;

	if (zc_names.getSize() == 0) {
		// No zone cluster name was specified.
		// We have to fetch all the zone cluster names
		rstatus = get_zone_cluster_names(zc_names);

		if (rstatus != CL_NOERR) {
			return (rstatus);
		}

		// Set this flag so that we can clear the memory
		// used for creating "zc_names"
		vl_list_flag = 1;
	}

	// Iterate through each zone cluster name
	for (zc_names.start(); !zc_names.end(); zc_names.next()) {
		zc_name = zc_names.getValue();

		// Now get the zone host names for this zone using
		// libzccfg
		// Open the zonecfg handle
		zone_handle = zccfg_init_handle();
		lib_clzone_err = zccfg_get_handle(zc_name, zone_handle);

		if (lib_clzone_err != ZC_OK) {

			if (first_err == CL_NOERR) {

				if (lib_clzone_err == ZC_NO_ZONE) {
					first_err = CL_ENOENT;
				} else {
					first_err = CL_EINTERNAL;
				}
			}

			if (print_err) {

				if (lib_clzone_err == ZC_NO_ZONE) {
					clerror("Invalid zone cluster name "
						"%s\n", zc_name);
				} else {
					clerror("Unable to fetch zone cluster"
						" %s information.\n", zc_name);
				}
			}

			// skip this zone cluster. might be invalid
			continue;
		}

		lib_clzone_err = zccfg_setnodeent(zone_handle);

		if (lib_clzone_err != ZC_OK) {
			zccfg_fini_handle(zone_handle);
			if (first_err == CL_NOERR) {
				first_err = CL_EINTERNAL;
			}

			// skip this zone cluster
			continue;
		}

		// Get the nodelist for this zone cluster
		while (zccfg_getnodeent(zone_handle, &nodetab)
				== ZC_OK) {
			// Add the zone cluster node name here
			zc_node_name = NULL;
			str_len_count = 1; // for NULL
			str_len_count += strlen(zc_name);
			str_len_count += strlen(ZC_NODE_NAME_SEPARATOR);
			str_len_count += strlen(nodetab.zc_hostname);
			zc_node_name = (char *)calloc(1, str_len_count);
			strcat(zc_node_name, zc_name);
			strcat(zc_node_name, ZC_NODE_NAME_SEPARATOR);
			strcat(zc_node_name, nodetab.zc_hostname);
			zc_list.add(zc_node_name);
		}

		(void) zccfg_endnodeent(zone_handle);
		// Close the zonecfg handle
		(void) zccfg_fini_handle(zone_handle);

	}

	// If we created the zone names list, we should clear the list here
	if (vl_list_flag) {
		zc_names.clear();
	}

	return (first_err);
}
#endif
