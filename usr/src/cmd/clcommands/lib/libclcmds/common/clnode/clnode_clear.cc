//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clnode_clear.cc	1.19	09/04/02 SMI"

//
// Process clnode "clear"
//

#include "clnode.h"
#include "common.h"
#include "misc.h"
#include <scadmin/scstat.h>
#include <rgm/rgm_scrgadm.h>
#include <sys/clconf_int.h>
#include "rgm_support.h"
#include "clresourcegroup.h"
#include "clresourcetype.h"
#if (SOL_VERSION >= __s10)
#include <libzccfg/libzccfg.h>
#include "clzonecluster.h"
#endif

#ifdef WEAK_MEMBERSHIP
#include "clquorum.h"
#endif

clerrno_t clnode_remove_node_and_zones_from_rts(char *nodename,
	scconf_nodeid_t nodeid, char **msg_buffer, ClCommand cmd,
	optflgs_t optflgs);
void print_err(char *err_message, char **msg_buffer);
clerrno_t clnode_check_quorum(scconf_nodeid_t nodeid);

//
// clnode "clear"
//
clerrno_t
clnode_clear(ClCommand &cmd, ValueList &operands, optflgs_t optflgs,
		char **msg_str_buffer)
{
	NameValueList node_list = NameValueList(true);
	ValueList empty_list;
	clerrno_t clerrno = CL_NOERR;
	clerrno_t first_err = CL_NOERR;
	scconf_errno_t scconf_err = SCCONF_NOERR;
	scconf_cfg_node_t *servernodes = (scconf_cfg_node_t *)0;
	char *node_name;
	char *node_type;
	char msgbuffer[SCCONF_MAXSTRINGLEN];
	char *scconf_err_msg = (char *)0;
	scconf_err  = SCCONF_NOERR;
	scconf_nodeid_t nodeid = 0;
	uint_t ismember;
	bool do_remove = true;

#ifdef WEAK_MEMBERSHIP
	boolean_t multiple_partitions = B_FALSE;
	clconf_cluster_t *clconf = (clconf_cluster_t *)0;
	int err_flg = 0;

	scconf_err = scconf_cltr_openhandle((scconf_cltr_handle_t *)&clconf);
	if (scconf_err != SCCONF_NOERR) {
		return (CL_EINTERNAL);
	}

	//
	// Get the value for the multiple_partitions
	// property
	//
	scconf_err = scconf_get_multiple_partitions(clconf,
                &multiple_partitions);

	if (scconf_err != SCCONF_NOERR) {
		clerror("Unable to determine the value configured "
			"for \"%s\".\n",
			CLPROP_MULTIPLE_PARTITIONS);
		return (CL_EINVAL);
	}

	//
	// Allow the command to continue only if Force option
	// is specified when running under weak membership
	//
	if (multiple_partitions == B_TRUE && !(optflgs & Fflg)) {
		clerror("You cannot clear a node when \"%s\" "
			"is set to \"true\"\n",
			CLPROP_MULTIPLE_PARTITIONS);

		if (first_err == CL_NOERR) {
			first_err = CL_EOP;
		}

		goto cleanup;
	}
#endif // WEAK_MEMBERSHIP

	// Get the valid nodes from the user inputs
	clerrno = clnode_preprocess_nodelist(operands, empty_list,
	    node_list, true);
	if (clerrno != CL_NOERR) {
		if (first_err == CL_NOERR) {
			first_err = clerrno;
		}
	}

	// Nothing to remove?
	if (node_list.getSize() == 0) {
		goto cleanup;
	}

	// Process the nodes
	for (node_list.start(); !node_list.end(); node_list.next()) {
		node_name = node_list.getName();
		node_type = node_list.getValue();
		if (!node_name || !node_type)
			continue;

		// Do not remove farm node
		if (strcmp(node_type, NODETYPE_FARM) == 0) {
			(void) sprintf(msgbuffer, "Cannot clear \"%s\" node "
			    "\"%s\" with this subcommand.\n",
				node_type, node_name);
			print_err(msgbuffer, msg_str_buffer);
			if (first_err == CL_NOERR) {
				first_err = CL_EOP;
			}
			continue;
		}

		// Get the node id
		scconf_err = scconf_get_nodeid(node_name, &nodeid);
		if (scconf_err != SCCONF_NOERR) {
			// We should not come here since we have already
			// checked for the validity of the node.
			return (CL_EINVAL);
		}

		do_remove = true;

		// Check if the node is in non-cluster mode
		(void) scconf_ismember(nodeid, &ismember);
		if (ismember) {
			(void) sprintf(msgbuffer,
			    "Cannot clear node \"%s\" which is still in "
			    "cluster mode.\n", node_name);
			print_err(msgbuffer, msg_str_buffer);
			if (first_err == CL_NOERR) {
				first_err = CL_EOP;
			}
			do_remove = false;
		}

		//
		// Check if any quorums connects to this node connects to
		// more than two nodes.  This check can be removed if
		// conf_cluster_commit() does not return error in this case.
		//
		clerrno = clnode_check_quorum(nodeid);
		if (clerrno != CL_NOERR) {
			if (first_err == CL_NOERR) {
				first_err = clerrno;
			}
			do_remove = false;
		}
		if (!do_remove) {
			continue;
		}

		//
		// For force clear, we need to remove the node from
		// RGs first before calling scconf_rm_node. So check
		// node removal first too.
		//
		if (optflgs & Fflg) {
			ValueList rg_objs;
			ValueList node_to_remove;
			ValueList zones_list;
			ValueList zc_list;
			ValueList zc_node_list;

			rg_objs.add(CLCOMMANDS_WILD_OPERAND);
			node_to_remove.add(node_name);
			// Get the list of zones which are hosted on this node
			clerrno = clnode_get_ng_zones(nodeid, zones_list);

			// Remove node from RGs
			clerrno = clrg_remove_node(cmd, rg_objs, optflgs,
			    node_to_remove, 1);

			if (clerrno != CL_NOERR) {
				if (first_err == CL_NOERR) {
					first_err = clerrno;
				}
				continue;
			}

			// Now remove the zones from the RGs.
			// We have to remove the zones, which are hosted
			// on this node, as well
			for (zones_list.start(); !zones_list.end();
				zones_list.next()) {
				node_to_remove.clear();
				node_to_remove.add(zones_list.getValue());
				clerrno = clrg_remove_node(cmd, rg_objs,
					optflgs, node_to_remove, 1);

				if (clerrno != CL_NOERR) {
					// If there was an error then break
					break;
				}
			}

			if (clerrno != CL_NOERR) {
				if (first_err == CL_NOERR) {
					first_err = clerrno;
				}
				continue;
			}

			// Remove node from all RT's Installed_nodes.
			clerrno = clnode_remove_node_and_zones_from_rts(
					node_name, nodeid, msg_str_buffer,
					cmd, optflgs);

			if (clerrno != CL_NOERR) {
				first_err = clerrno;
				continue;
			}

#if (SOL_VERSION >= __s10)
			// Get the list of zone clusters for this (current) node
			clerrno = get_zone_cluster_names(zc_list);

			// Removing node from zoneclusters as well
			for (zc_list.start(); !zc_list.end();
				zc_list.next()) {
				zc_node_list.clear();
				// Get the list of nodes for the given zc
				clerrno = get_nodelist_for_zc(
						zc_list.getValue(),
						zc_node_list);
				if (clerrno != CL_NOERR) {
					first_err = clerrno;
					clerror("Error occured while fetching "
					"node list for zone cluster \"%s\".\n",
					zc_list.getValue());
					continue;
				}
				for (zc_node_list.start(); !zc_node_list.end();
					zc_node_list.next()) {
					if (strcmp((char *)node_name,
						(char *)zc_node_list.getValue())
							== 0) {
						clerrno = delete_node_from_zc(
							node_name,
							zc_list.getValue());
						if (clerrno != CL_NOERR) {
							first_err = clerrno;
							clerror("Failed to "
							"remove node \"%s\" "
							"from \"%s\"\n",
							node_name,
							zc_list.getValue());
							continue;
						}
						break;
					}
				}
			}
#endif
		}

		// Call scconf_rm_node()
		scconf_err = scconf_rm_node(node_name, optflgs & Fflg,
		    &scconf_err_msg);

		// Print detailed messages
		if (scconf_err_msg) {
			if (msg_str_buffer == (char **)0) {
				clcommand_dumpmessages(scconf_err_msg);
			} else {
				// Append these messages to the message buffer
				print_err(scconf_err_msg, msg_str_buffer);
			}

			(void) free(scconf_err_msg);
		}

		if (scconf_err != SCCONF_NOERR) {
			switch (scconf_err) {
			case SCCONF_ENOEXIST:
				sprintf(msgbuffer,
				"Node \"%s\" does not exist.\n", node_name);
				print_err(msgbuffer, msg_str_buffer);
				clerrno = CL_ENOENT;
				break;
			case SCCONF_EINUSE:
			case SCCONF_EQUORUM:
				// Errors are in msgbuffer already.
				clerrno = CL_EOP;
				break;
			case SCCONF_EBUSY:
				clerrno = CL_ESTATE;
				break;
			default:
				clerrno = CL_EINTERNAL;
				sprintf(msgbuffer,
				    "An unexpected error occurred; "
				    "failed to clear "
				    "node \"%s\".\n", node_name);
				print_err(msgbuffer, msg_str_buffer);
			}

			if (first_err == CL_NOERR) {
				first_err = clerrno;
			}
		} else if (cmd.isVerbose) {
			clmessage("Node \"%s\" is cleared.\n", node_name);
		}

#ifdef WEAK_MEMBERSHIP
		//
		// Check if the cluster is running Weak Membership
		// if so then switch the membership to STRONG by
		// changing the multiple_partitions property to "false"
		//
		// if not, continue with the regular processing
		//
        	if (multiple_partitions == B_TRUE) {
			//
			// If we are here then we are in a
			// 2-node cluster running weak membership
			// and we are clearing
			// references of one of the nodes
			//
			if (optflgs & Fflg) {
				//
				// Cluster is running weak membership
				// Switch the membership to "STRONG"
				//

				scconf_err = scconf_change_global_quorum_prop(
					CLPROP_MULTIPLE_PARTITIONS,
					"false", &scconf_err_msg);

				if (scconf_err_msg  != (char *)0) {
					clcommand_dumpmessages(scconf_err_msg);
					(void) free(scconf_err_msg);
				}

				if (scconf_err != SCCONF_NOERR) {
					clerrno = CL_EINTERNAL;
					clerror("An unexpected error occurred; "
						"failed to set \"%s\" to "
						"\"false\".\n",
						CLPROP_MULTIPLE_PARTITIONS);

					if (first_err == CL_NOERR) {
						first_err = clerrno;
					}
				} else if (cmd.isVerbose) {
					clmessage("\"%s\" is set to "
						"\"false\".\n",
						CLPROP_MULTIPLE_PARTITIONS);
				}

				//
				// Mark the CCR on the current
				// cluster node as the winner.
				// This is done because
				// current node will be
				// the only surviving node
				// in the cluster
				//
				err_flg = mark_ccr_as_winner();

				if (err_flg) {
					clerrno = CL_EINTERNAL;
					clerror("An unexpected error occurred; "
						"failed to mark the CCR as "
						"winner.\n");

					if (first_err == CL_NOERR) {
						first_err = clerrno;
					}
				}
			}
		}
#endif // WEAK_MEMBERSHIP
	}

cleanup:
	// Return the first error
	if (first_err != CL_NOERR) {
		clerrno = first_err;
	}

	return (clerrno);
}

//
// clnode "clear" plain function to be used from old-cli code
//
clerrno_t
clnode_clear_node(char *nodename, uint_t force_flag, char **msg_buffer)
{

	ClCommand cmdObj;
	ValueList nodesSet;
	optflgs_t optflgs = 0;

	if (nodename == NULL) {
		return (CL_EINVAL);
	}

	nodesSet.add(nodename);

	if (force_flag) {
		optflgs = Fflg;
	}

	return (clnode_clear(cmdObj, nodesSet, optflgs, msg_buffer));
}

// This function will remove a node and the zones hosted on the node from
// the Installed_nodes property of all the RTs. This function uses
// clrt_remove_node() function to achieve it. Please note that
// clrt_remove_node() reports errors when a wild card is specified
// as the RT list. Also, clrt_remove_node() does not take a force flag as
// well. Hence, we need to pass only the set of RTs which have the nodename
// in the Installed_nodes property.

clerrno_t
clnode_remove_node_and_zones_from_rts(char *nodename, scconf_nodeid_t nodeid,
	char **msg_buffer, ClCommand cmd, optflgs_t optflgs) {

	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};
	char **rt_names = NULL;
	char buffer[SCCONF_MAXSTRINGLEN];
	rgm_rt_t *rt = (rgm_rt_t *)NULL;
	clerrno_t rstatus = CL_NOERR;
	clerrno_t first_err = CL_NOERR;
	nodeidlist_t *nodeid_list = (nodeidlist_t *)NULL;
	scconf_errno_t scconf_err = SCCONF_NOERR;
	char *zonename = NULL;

	// Check whether nodename was specified
	if (nodename == NULL) {
		return (CL_EINVAL);
	}

	// If nodeid was not specified then fetch it
	if (nodeid < 1) {
		scconf_err = scconf_get_nodeid(nodename, &nodeid);
		// Verify whether the node is valid
		if (scconf_err != SCCONF_NOERR) {
			return (CL_EINVAL);
		}
	}

	// Get the list of Resource Types
	scha_status = rgm_scrgadm_getrtlist(&rt_names, NULL);
	if (scha_status.err_code != SCHA_ERR_NOERR) {
		sprintf(buffer, "Failed to obtain list of all registered "
		    "resource types.\n");
		print_err(buffer, msg_buffer);
		rstatus = map_scha_error(scha_status.err_code);
		return (rstatus);
	}

	// If there are not RTs then return.
	if (rt_names == NULL) {
		return (CL_NOERR);
	}

	// For each RT get the Installed_nodes property
	for (int i = 0; rt_names[i] != NULL; i++) {
		ValueList nodes_to_remove;
		ValueList rt_list;

		// Free the rt structure.
		if (rt != (rgm_rt_t *)0) {
			rgm_free_rt(rt);
			rt = (rgm_rt_t *)0;
		}

		scha_status = rgm_scrgadm_getrtconf(rt_names[i], &rt, NULL);
		if (scha_status.err_code != SCHA_ERR_NOERR) {
			sprintf(buffer, "Skipping resource type \"%s\".\n",
			    rt_names[i]);
			print_err(buffer, msg_buffer);
			rstatus = map_scha_error(scha_status.err_code);

			if (first_err == CL_NOERR) {
				first_err = rstatus;
			}

			continue;
		}

		// If Installed_nodes is set to <ALL>, then we have to skip
		if (rt->rt_instl_nodes.is_ALL_value) {
			continue;
		}

		// Get the nodeids of nodes in Installed_nodes property
		nodeid_list = rt->rt_instl_nodes.nodeids;
		for (; nodeid_list; nodeid_list = nodeid_list->nl_next) {

			if (nodeid_list->nl_nodeid != nodeid) {
				continue;
			}

			// If we are here then the nodeid is present.
			// Add this node to the nodes to remove list
			if (!nodeid_list->nl_zonename) {
				nodes_to_remove.add(nodename);
			} else {
				zonename = (char *)calloc(1, strlen(nodename) +
					strlen(nodeid_list->nl_zonename) + 2);
				strcat(zonename, nodename);
				strcat(zonename, ":");
				strcat(zonename, nodeid_list->nl_zonename);
				nodes_to_remove.add(strdup(zonename));
				delete(zonename);
			}
		}

		// If node and the zones hosted on the node was not found in
		// Installed_nodes property then there is no
		// need to remove the node from the RTs nodelist
		if (nodes_to_remove.getSize() < 1) {
			continue;
		}

		// If we are here then we have to update the Installed_nodes
		// property of the RT.
		rt_list.add(rt_names[i]);
		rstatus = clrt_remove_node(cmd, rt_list, optflgs,
					nodes_to_remove);

		if (rstatus != CL_NOERR) {
			if (first_err == CL_NOERR) {
				first_err = rstatus;
			}

			if (rstatus == CL_EOP) {
				// One or more RTs have Installed_nodes set to *
				sprintf(buffer, "One or more "
					"Resource Types are available "
					"on all nodes of the cluster. "
					"Please modify the "
					"Installed_nodes property "
					"of these Resource Types "
					"and try again.\n");
				print_err(buffer, msg_buffer);
			}

			continue;
		}
	}

	// Free memory
	rgm_free_strarray(rt_names);
	if (rt != (rgm_rt_t *)0) {
		rgm_free_rt(rt);
		rt = (rgm_rt_t *)0;
	}

	return (first_err);
}

void
print_err(char *err_message, char **msg_buffer) {

	if (err_message == (char *)0) {
		return;
	}

	if (msg_buffer == (char **)0) {
		clerror(err_message);
		return;
	}

	scconf_addmessage(err_message, msg_buffer);
}

//
// clnode_check_quorum
//
//	Check if there are quorum devices connexted to more than
//	two nodes.
//
clerrno_t
clnode_check_quorum(scconf_nodeid_t nodeid)
{
	clconf_cluster_t *clconf = (clconf_cluster_t *)0;
	scconf_errno_t scconf_err = SCCONF_NOERR;
	clconf_iter_t *currqdevsi, *qdevsi = (clconf_iter_t *)0;
	clconf_iter_t *currnodesi, *nodesi;
	clconf_obj_t *qdev;
	clconf_obj_t *node;
	clerrno_t first_err = CL_NOERR;
	const char *conf_name, *conf_val;
	uint_t *index_map = NULL;
	scconf_nodeid_t conf_nodeid;
	uint_t i, count;
	char prop_buf[BUFSIZ];

	// Check input
	if (!nodeid || nodeid > NODEID_MAX) {
		clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}

	// Get the cluster config
	if (scconf_cltr_openhandle((scconf_cltr_handle_t *)
	    &clconf) != SCCONF_NOERR) {
		clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}

	// Initialzie node ID map
	index_map = new uint_t[NODEID_MAX];
	for (i = 0; i < NODEID_MAX; i++) {
		index_map[i] = 0;
	}

	// Get the node IDs
	nodesi = currnodesi = clconf_cluster_get_nodes(clconf);
	while ((node = clconf_iter_get_current(currnodesi)) != NULL) {
		// get the node id
		conf_nodeid = (scconf_nodeid_t)clconf_obj_get_id(node);
		if (conf_nodeid == (scconf_nodeid_t)-1) {
			continue;
		}

		// Set the node ID map
		index_map[conf_nodeid] = 1;

		clconf_iter_advance(currnodesi);
	}

	// Go through all the quorums
	qdevsi = currqdevsi = clconf_cluster_get_quorum_devices(clconf);
	while ((qdev = clconf_iter_get_current(currqdevsi)) != NULL) {
		clconf_iter_advance(currqdevsi);

		// Get the quorum device name
		conf_name = clconf_obj_get_name(qdev);
		if (!conf_name) {
			continue;
		}

		// Does this quorum to this node?
		(void) sprintf(prop_buf, "path_%u", nodeid);
		conf_val = clconf_obj_get_property(qdev, prop_buf);
		if (!conf_val) {
			continue;
		}

		// Get the path connected to nodes
		count = 1;
		for (i = 0; i < NODEID_MAX; i++) {
			if (!index_map[i] || (nodeid == i)) {
				continue;
			}

			// Get the path property
			(void) sprintf(prop_buf, "path_%u", i);
			conf_val = clconf_obj_get_property(qdev, prop_buf);
			if (!conf_val) {
				continue;
			}

			// This device connects to more than 3 nodes
			count++;
			if (count == 3) {
				clerror("Quorum device \"%s\" connects to "
				    "more than two nodes. Unconfigure the "
				    "quorum device before you clear "
				    "the node.\n",
				    conf_name);
				if (first_err == CL_NOERR) {
					first_err = CL_EOP;
				}
				break;
			}
		}
	}

	// Release the cluster config
	if (clconf != (clconf_cluster_t *)0) {
		clconf_obj_release((clconf_obj_t *)clconf);
	}

	delete [] index_map;

	return (first_err);
}

//
// Deletes node information from zc, by removing entries from ccr
// zccfg_delete_node is used to clear ccr entries
//
clerrno_t
delete_node_from_zc(char *nodename, char *zonename) {
	char *cl_name;

#if (SOL_VERSION >= __s10)
	zc_dochandle_t handle;
	int err;
	struct zc_nodetab nodetab;

	// Initialize the handle
	if ((handle = zccfg_init_handle()) == NULL) {
		// Insufficient memory, exiting..
		return (CL_ENOMEM);
	}

	// Get the handle
	if ((err = zccfg_get_handle(zonename, handle)) != ZC_OK) {
		// Could not read configuration of zone cluster
		zccfg_fini_handle(handle);
		return (CL_EINTERNAL);
	}

	// Delete the node from the configuration
	(void) strcpy(nodetab.zc_nodename, nodename);
	if ((err = zccfg_delete_node(handle, &nodetab)) != ZC_OK) {
		// Could not delete node from zone cluster
		zccfg_fini_handle(handle);
		return (CL_EINTERNAL);
	}

	// Commit the latest configuration
	if ((err = zccfg_save(handle)) != ZC_OK) {
		// Could not commit zone cluster configuration
		zccfg_fini_handle(handle);
		return (CL_EINTERNAL);
	}

	// Finish off handle
	zccfg_fini_handle(handle);
#endif

	return (CL_NOERR);
}
