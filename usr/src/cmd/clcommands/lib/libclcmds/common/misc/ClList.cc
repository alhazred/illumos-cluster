/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)ClList.cc 1.4	08/05/20 SMI"
 *
 * ClList.cc
 */

#include "ClList.h"

// Initialize all the values
ClList::ClList(bool v) :
    verbose(v),
    numColumns(0)
{
	for (int i = 0; i < MAX_COLS; i++) {
		longest[i] = 0;
	}

	headings[0] = NULL;
	headings[1] = NULL;
}


ClList::~ClList()
{
	rows.clear();
	delete headings[0];
	delete headings[1];
}

// Keeps track of the longest column lengths
void
ClList::setLongest(Row *row)
{
	for (int i = 0; i < numColumns - 1; i++) {
		int col_i = strlen(row->getValue(i));
		if (col_i > longest[i])
			longest[i] = col_i;
	}
}

// Add a row of up to five columns to the list of Row objects
void
ClList::addRow(
    char *v1,
    char *v2,
    char *v3,
    char *v4,
    char *v5)
{
	if (numColumns == 0) {
		numColumns = 1;
		if (v2  != NULL)
			numColumns = 2;
		if (v3  != NULL)
			numColumns = 3;
		if (v4  != NULL)
			numColumns = 4;
		if (v5  != NULL)
			numColumns = 5;
	}

	if (v1 == NULL)
		v1 = (char *)"";
	if (numColumns >= 2 && v2 == NULL)
		v2 = (char *)"";
	if (numColumns >= 3 && v3 == NULL)
		v3 = (char *)"";
	if (numColumns >= 4 && v4 == NULL)
		v4 = (char *)"";
	if (numColumns >= 5 && v5 == NULL)
		v5 = (char *)"";

	Row *a_row = new Row(v1, v2, v3, v4, v5);
	setLongest(a_row);
	rows.push_back(a_row);
}

// initialize the iterator to the beginning of the list
void
ClList::rowStart()
{
	iter = rows.begin();
}

// Go to the next value in the list
void
ClList::rowNext()
{
	if (iter != rows.end())
		++iter;
}

// Returns 1 if the end of the list has been reached
int
ClList::rowEnd()
{
	return (iter == rows.end() ? 1 : 0);
}

// Get the current row value
Row *
ClList::rowCurrent()
{
	return ((*iter));
}

// Set the headings of up to five columns
void
ClList::setHeadings(
    char *h1,
    char *h2,
    char *h3,
    char *h4,
    char *h5)
{
	if (numColumns == 0) {
		numColumns = 1;
		if (h2  != NULL)
			numColumns = 2;
		if (h3  != NULL)
			numColumns = 3;
		if (h4  != NULL)
			numColumns = 4;
		if (h5  != NULL)
			numColumns = 5;
	}

	headings[0] = new Row(h1, h2, h3, h4, h5);
	setLongest(headings[0]);
	char temp[MAX_COLS][100];
	for (int i = 0; i < MAX_COLS; i++) {
		if (headings[0]->getValue(i) == NULL)
			temp[i][0] = '\0';
		else {
			memset(temp[i], '-', strlen(headings[0]->getValue(i)));
			temp[i][strlen(headings[0]->getValue(i))] = '\0';
		}
	}
	headings[1] = new Row(
	    temp[0],
	    temp[1],
	    temp[2],
	    temp[3],
	    temp[4]);

}

// Enable verbose mode printing
void
ClList::enableVerbose(bool v)
{
	verbose = v;
}

// Print the data
void
ClList::print()
{

	// If not verbose mode, print only first column
	if (!verbose) {
		for (rowStart();
		    !rowEnd();
		    rowNext()) {

			printf("%s\n", rowCurrent()->getValue(0));

		}
		return;
	}

	// Determine columns widths
	for (int i = 0; i < numColumns - 1; i++) {
		longest[i] += MIN_SPACE;
		if (COL_WIDTH > longest[i])
			longest[i] = COL_WIDTH;
	}

	// Now print the headings
	switch (numColumns) {

	case 1:
		printf("%s\n", headings[0]->getValue(0));
		printf("%s\n", headings[1]->getValue(0));
		break;
	case 2:
		printf("%-*s %s\n",
		    longest[0] - 1,
		    headings[0]->getValue(0),
		    headings[0]->getValue(1));
		printf("%-*s %s\n",
		    longest[0] - 1,
		    headings[1]->getValue(0),
		    headings[1]->getValue(1));
		break;
	case 3:
		printf("%-*s %-*s %s\n",
		    longest[0] - 1,
		    headings[0]->getValue(0),
		    longest[1] - 1,
		    headings[0]->getValue(1),
		    headings[0]->getValue(2));
		printf("%-*s %-*s %s\n",
		    longest[0] - 1,
		    headings[1]->getValue(0),
		    longest[1] - 1,
		    headings[1]->getValue(1),
		    headings[1]->getValue(2));
		break;
	case 4:
		printf("%-*s %-*s %-*s %s\n",
		    longest[0] - 1,
		    headings[0]->getValue(0),
		    longest[1] - 1,
		    headings[0]->getValue(1),
		    longest[2] - 1,
		    headings[0]->getValue(2),
		    headings[0]->getValue(3));
		printf("%-*s %-*s %-*s %s\n",
		    longest[0] - 1,
		    headings[1]->getValue(0),
		    longest[1] - 1,
		    headings[1]->getValue(1),
		    longest[2] - 1,
		    headings[1]->getValue(2),
		    headings[1]->getValue(3));
		break;
	case 5:
		printf("%-*s %-*s %-*s %-*s %s\n",
		    longest[0] - 1,
		    headings[0]->getValue(0),
		    longest[1] - 1,
		    headings[0]->getValue(1),
		    longest[2] - 1,
		    headings[0]->getValue(2),
		    longest[3] - 1,
		    headings[0]->getValue(3),
		    headings[0]->getValue(4));
		printf("%-*s %-*s %-*s %-*s %s\n",
		    longest[0] - 1,
		    headings[1]->getValue(0),
		    longest[1] - 1,
		    headings[1]->getValue(1),
		    longest[2] - 1,
		    headings[1]->getValue(2),
		    longest[3] - 1,
		    headings[1]->getValue(3),
		    headings[1]->getValue(4));
		break;

	default:
		break;
	}

	// Print each row
	for (rowStart();
	    !rowEnd();
	    rowNext()) {

		switch (numColumns) {

		case 1:
			printf("%s\n", rowCurrent()->getValue(0));
			break;
		case 2:
			printf("%-*s %s\n",
			    longest[0] - 1,
			    rowCurrent()->getValue(0),
			    rowCurrent()->getValue(1));
			break;
		case 3:
			printf("%-*s %-*s %s\n",
			    longest[0] - 1,
			    rowCurrent()->getValue(0),
			    longest[1] - 1,
			    rowCurrent()->getValue(1),
			    rowCurrent()->getValue(2));
			break;
		case 4:
			printf("%-*s %-*s %-*s %s\n",
			    longest[0] - 1,
			    rowCurrent()->getValue(0),
			    longest[1] - 1,
			    rowCurrent()->getValue(1),
			    longest[2] - 1,
			    rowCurrent()->getValue(2),
			    rowCurrent()->getValue(3));
			break;
		case 5:
			printf("%-*s %-*s %-*s %-*s %s\n",
			    longest[0] - 1,
			    rowCurrent()->getValue(0),
			    longest[1] - 1,
			    rowCurrent()->getValue(1),
			    longest[2] - 1,
			    rowCurrent()->getValue(2),
			    longest[3] - 1,
			    rowCurrent()->getValue(3),
			    rowCurrent()->getValue(4));
			break;

		default:
			break;
		}
	}
}

// Print the raw values
void
ClList::print(
    char *v1,
    char *v2,
    char *v3,
    char *v4,
    char *v5)
{
	switch (numColumns) {

	case 1:
		printf("%s\n",
		    v1);
		return;
	case 2:
		printf("%-*s %s\n",
		    COL_WIDTH - 1,
		    v1,
		    v2);
		return;
	case 3:
		printf("%-*s %-*s %s\n",
		    COL_WIDTH - 1,
		    v1,
		    COL_WIDTH - 1,
		    v2,
		    v3);
		return;
	case 4:
		printf("%-*s %-*s %-*s %s\n",
		    COL_WIDTH - 1,
		    v1,
		    COL_WIDTH - 1,
		    v2,
		    COL_WIDTH - 1,
		    v3,
		    v4);
		return;
	case 5:
		printf("%-*s %-*s %-*s %-*s %s\n",
		    COL_WIDTH - 1,
		    v1,
		    COL_WIDTH - 1,
		    v2,
		    COL_WIDTH - 1,
		    v3,
		    COL_WIDTH - 1,
		    v4,
		    v5);
		return;
	default:
		printf("Internal error -- Invalid number of columns\n");
		return;
	}

}

// Print a Row object
void
ClList::print(Row *row)
{
	if (row) {
		print(
		    row->getValue(0),
		    row->getValue(1),
		    row->getValue(2),
		    row->getValue(3),
		    row->getValue(4));
	}
}
