//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clcommand_command.cc	1.12	08/07/25 SMI"

//
// Common control functions for the "cl" family of commands
//

#include <pwd.h>
#include <unistd.h>
#include <auth_attr.h>
#include <secdb.h>

#include <sys/types.h>

#include "clcommands.h"

#include "ClCommand.h"

static int clcommand_nosubcommand(ClCommand &cmd);
static void print_clcommand_version(ClCommand &cmd);
static void nomem_exception(void);

// Global variables
ClCommand commandInstance;

//
// This is the Common "main" for the "cl" family of commands.
//
// All commands in the command set should call this function with
// their args and statically defined properties (clcommandinit_t).
//
// This function declares and initializes an instance of the
// ClCommand class, used by each command in the command set.
//
// Among other things, this function enforces some of the common
// characteristics of each command in the "cl" family of commands.
// This includes:
//
//	- All commands must have at least one argument.
//	- If there is no subcommand, only the "--help" and "--version"
//		options are processed.
//
clerrno_t
clcommand_command(int argc, char *argv[], clcommandinit_t &init)
{
	ClCommand &cmd = commandInstance;
	unsigned int flags;
	clerrno_t clerrno = CL_NOERR;
	boolean_t cl_zone_flag;

	// setup "new" exception handler
	std::set_new_handler(&nomem_exception);

	// Make sure that the getopt_clip "opterr" variable is not set
	opterr = 0;

	// Setup the command
	cmd.initCommand(argc, argv, init);

	// Initialize the messaging
	init_clmessages(cmd.getCommandName());

	// All commands take at least one argument
	if (argc < 2) {
		clerror("Not enough arguments.\n");
		clcommand_usage(cmd);
		return (CL_EINVAL);
	}

	// If there is no subcommand, process arguments
	if (*argv[1] == '-') {
		clerrno = clcommand_nosubcommand(cmd);
		return (clerrno);
	}

	// Find the subcommand, and initialize data
	if ((clerrno = cmd.initSubCommand()) != CL_NOERR) {
		return (clerrno);
	}

	// If we just have a help option, always allow it
	if ((argc == 3) &&
	    ((strcmp(argv[2], "-?") == 0) ||
	    (strcmp(argv[2], "--help") == 0))) {
		clerrno = cmd.doSubCommand();
		return (clerrno);
	}

	// Check authorization
	if ((clerrno = clcommand_check_authorization(
	    cmd.getSubCommandRbacAuthorization())) != CL_NOERR) {
		return (clerrno);
	}

	// Now we can promote the real uid to super-user
	if ((clerrno = clcommand_promote_uid()) != CL_NOERR) {
		return (clerrno);
	}

	// Now that we are super-user, we can set the cluster mode flag
	(void) scconf_ismember(0, &cmd.isClusterMode);

	// Check cluster mode
	flags = cmd.getSubCommandFlags();
	if (cmd.isClusterMode && (flags & CLSUB_NCLMODE_REQUIRED)) {
		clerrno = CL_ECLMODE;
		clcommand_perror(clerrno);
		return (clerrno);
	} else if (!cmd.isClusterMode && !(flags & CLSUB_NCLMODE_ALLOWED) &&
	    !(flags & CLSUB_NCLMODE_REQUIRED)) {
		clerrno = CL_ENOTCLMODE;
		clcommand_perror(clerrno);
		return (clerrno);
	}

	// Check zone cluster setting
	cl_zone_flag = B_FALSE;
#if SOL_VERSION >= __s10
	(void) scconf_zone_cluster_check(&cl_zone_flag);
#endif

	if (!(flags & CLSUB_ZONE_CLUSTER_ALLOWED) &&
		(cl_zone_flag)) {
		clerror("You cannot run this command from a zone cluster.\n");
		return (CL_EOP);
	}

	// Check zones setting
	if (!(flags & CLSUB_NON_GLOBAL_ZONE_ALLOWED) &&
	    (!cl_zone_flag) &&
	    (sc_nv_zonescheck() != 0)) {
		clerror("You cannot run this command from a non-global "
		    "zone.\n");
		return (CL_EOP);
	}

	// Command logging
	if (flags & CLSUB_DO_COMMAND_LOGGING) {
		cl_cmd_event_init(cmd.argc, cmd.argv, &clerrno);
		(void) cl_cmd_event_gen_start_event();
		(void) atexit(cl_cmd_event_gen_end_event);
	}

	// Process the subcommand
	clerrno = cmd.doSubCommand();

	// Done
	return (clerrno);
}

//
// Check RBAC authorization.
//
clerrno_t
clcommand_check_authorization(char *authorization)
{
	struct passwd *pw;
	uid_t uid = getuid();

	// Check argument
	if (!authorization) {
		clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}

	// Get the password entry for this user
	if ((pw = getpwuid(uid)) == (struct passwd *)0) {
		clerror("You are an unrecognized user (%d).\n", uid);
		return (CL_EACCESS);
	}

	// Check authorization
	if (!chkauthattr(authorization, pw->pw_name)) {
		clerror("You are not authorized to run this command "
		    "with these options.\n");
		clerror("An RBAC authorization of \"%s\" is required.\n",
		    authorization);
		clerror("Refer to the rbac(5) man page for more information "
		    "about RBAC authorizations.\n");
		return (CL_EACCESS);
	}

	return (CL_NOERR);
}

//
// Promote the real user ID to super-user.
//
clerrno_t
clcommand_promote_uid(void)
{
	clerrno_t clerrno = CL_NOERR;

	if (setuid(0) < 0) {
		if (errno == EPERM && geteuid() != 0) {
			clerrno = CL_EACCESS;
			clcommand_perror(clerrno);
		} else {
			clerrno = CL_EINTERNAL;
			clcommand_perror(clerrno);
		}
	}

	return (clerrno);
}

//
// Return the list of short options for this subcommand.
//
char *
clsubcommand_get_shortoptions(ClCommand &cmd)
{
	char *shortopts;
	char *s;
	cloptions_t *subopts;
	cloptions_t *o;
	int count;

	// Get options
	subopts = cmd.getSubCommandOptions();

	// If there are no options, return null.
	if (!subopts)
		return ((char *)0);

	// Count the number of subcommand options
	count = 0;
	for (o = subopts;  o->shortoption;  ++o)
		++count;

	// If there are no options, return null.
	if (!count)
		return ((char *)0);

	//
	// Allocate the shortoptions array.  Allow for leading ':',
	// trailing null, and ':' argument indicators.
	//
	shortopts = new char[(2 * count) + 2];

	// Add sort options
	s = shortopts;
	*s++ = ':';
	for (o = subopts;  o->shortoption;  ++o) {
		*s++ = o->shortoption;
		if (o->optionarg)
			*s++ = ':';
	}
	*s++ = 0;

	// Done
	return (shortopts);
}

//
// Free memory for the list of short options for this subcommand.
//
void
clsubcommand_free_shortoptions(char *shortopts)
{
	delete shortopts;
}

//
// Return the list of long options for this subcommand.
//
struct option *
clsubcommand_get_longoptions(ClCommand &cmd)
{
	struct option *longopts;
	struct option *l;
	cloptions_t *subopts;
	cloptions_t *o;
	int count;

	// Get options
	subopts = cmd.getSubCommandOptions();

	// If there are no options, return null.
	if (!subopts)
		return ((struct option *)0);

	// Count the number of subcommand options
	count = 0;
	for (o = subopts;  o->shortoption;  ++o)
		++count;

	// If there are no options, return null.
	if (!count)
		return ((struct option *)0);

	//
	// Allocate the longoptions array.  Leave enough room for
	// long option names ending in 's'.
	//
	l = longopts = new struct option[(2 * ++count)];
	while (count--)
		bzero(l++, sizeof (struct option));

	// Add long options
	l = longopts;
	for (o = subopts;  o->shortoption;  ++o, ++l) {
		l->name = new char[strlen(o->longoption) + 1];
		(void) strcpy(l->name, o->longoption);
		l->val = o->shortoption;
		if (o->optionarg) {
			l->has_arg = required_argument;
		} else {
			l->has_arg = no_argument;
		}
	}

	// Done
	return (longopts);
}

//
// Free memory for the list of long options for this subcommand.
//
void
clsubcommand_free_longoptions(struct option *longopts)
{
	struct option *l;

	if (longopts) {
		for (l = longopts;  l->name;  ++l)
			delete l->name;

		delete longopts;
	}
}

//
// Process arguments when there is no subcommand.
//
// Only the "--help", "--version", and "--verbose" options are
// allowed by commands in the "cl" family of commands when no
// subcommand is provided.
//
static int
clcommand_nosubcommand(ClCommand &cmd)
{
	int c;
	int helpflag = 0;
	int Vflag = 0;
	int vflag = 0;
	int errcount = 0;
	char *option = 0;
	int option_index = 0;
	char *shortopts = ":?Vv";
	struct option longopts[] = {
	    {"help", no_argument, 0, '?'},
	    {"version", no_argument, 0, 'V'},
	    {"verbose", no_argument, 0, 'v'},
	    {0, 0, 0, 0}
	};

	optind = 1;
	while ((c = getopt_clip(cmd.argc, cmd.argv, shortopts, longopts,
	    &option_index)) != -1) {

		// Get the option
		delete option;
		option = clcommand_option(optopt, cmd.argv[optind - 1]);

		// Switch on option letter
		switch (c) {
		case 'V':
			// Version option
			++Vflag;
			break;

		case 'v':
			// Verbose option, accepted, but ignored
			++vflag;
			break;

		case '?':
			// Help option?
			if ((strcmp(option, "-?") == 0) ||
			    (strcmp(option, "--help") == 0)) {
				++helpflag;
				break;
			}

			// Or, unrecognized option?
			clerror("Unrecognized command option - \"%s\".\n",
			    option);
			++errcount;
			break;

		case ':':
			// Missing argument
			clerror("Option \"%s\" requires an argument.\n",
			    option);
			++errcount;
			break;

		default:
			//
			// We should never get here.
			// If we do, it means that there
			// is an option in our option list
			// for which there is no case statement.
			// We either have a bad option list or
			// a bad case statement.
			//
			clerror("Unexpected option - \"%s\".\n", option);
			++errcount;
			break;
		}
	}
	delete option;
	option = (char *)0;

	// Help and Version are not allowed together.
	if (Vflag && helpflag) {
		clerror("The \"%s\" and \"%s\" options cannot "
		    "be used together.\n", "-V (--version)", "-? (--help)");
		++errcount;
	}

	// Verbose cannot be used by itself.
	if (vflag && !errcount && !(Vflag || helpflag)) {
		clerror("\"%s\" cannot be used by itself.\n", "-v (--verbose)");
		++errcount;
	}

	// No additional operands allowed.
	while (optind < cmd.argc) {
		clerror("Unexpected operand - \"%s\".\n", cmd.argv[optind++]);
		++errcount;
	}

	// Errors?
	if (errcount) {
		clcommand_usage(cmd, errcount);
		return (CL_EINVAL);
	}

	// Print version
	if (Vflag) {
		print_clcommand_version(cmd);

	// Print help
	} else if (helpflag) {
		clcommand_help(cmd);
	}

	return (CL_NOERR);
}

//
// Return the option name, either "-<shortoption>", "--<longoption>",
// or the null string.
//
char *
clcommand_option(int optopt, char *argument)
{
	char *option;
	char *s;

	if (optopt) {
		option = new char[3];
		(void) sprintf(option, "-%c", optopt);
	} else if (argument && (argument[0] == '-') && (argument[1] == '-')) {
		option = new char[strlen(argument) + 1];
		(void) strcpy(option, argument);
		if ((s = strchr(option, '=')) != (char *)0)
			*s = 0;
	} else if (argument) {
		option = new char[strlen(argument) + 1];
		(void) strcpy(option, argument);
	} else {
		option = new char[1];
		*option = 0;
	}

	return (option);
}

//
// Print a generic error message the CL error code.
//
void
clcommand_perror(clerrno_t clerrno)
{
	switch (clerrno) {
	case CL_ENOMEM:
		clerror("Not enough swap space.\n");
		return;

	case CL_EINVAL:
		clerror("Invalid argument.\n");
		return;

	case CL_ERECONF:
		clerror("Cluster is reconfiguring.\n");
		return;

	case CL_EACCESS:
		clerror("Permission denied.\n");
		return;

	case CL_ESTATE:
		clerror("Unexpected state.\n");
		return;

	case CL_EMETHOD:
		clerror("Resource method failed.\n");
		return;

	case CL_EPROP:
		clerror("Invalid property.\n");
		return;

	case CL_EINTERNAL:
		clerror("Internal error.\n");
		return;

	case CL_EIO:
		clerror("I/O error.\n");
		return;

	case CL_ENOENT:
		clerror("No such object.\n");
		return;

	case CL_EOP:
		clerror("Operation is not allowed.\n");
		return;

	case CL_EBUSY:
		clerror("Object busy.\n");
		return;

	case CL_EEXIST:
		clerror("Object already exists.\n");
		return;

	case CL_ETYPE:
		clerror("Unknown type.\n");
		return;

	case CL_ECLMODE:
		clerror("Node is in cluster mode.\n");
		return;

	case CL_ENOTCLMODE:
		clerror("This node is not in cluster mode.\n");
		return;
	}
}

//
// Print each line in the buffer as an error message.   Each line
// is preceeded with the command name prefix and is printed to stderr.
// However, errors in this buffer will not have message IDs and
// must be already localized.
//
void
clcommand_dumpmessages(char *msgbuffer)
{
	char *commandname = commandInstance.getCommandName();
	char *last;
	char *buffer;
	char *msg;

	// Check arguments
	if (!msgbuffer || !msgbuffer[0])
		return;

	//
	// Make a copy of the message buffer, since we
	// will be modifying it.
	//
	buffer = new char[strlen(msgbuffer)+1];
	(void) strcpy(buffer, msgbuffer);

	// Print it line-by-line
	last = (char *)0;
	for (msg = strtok_r(buffer, "\n", &last);  msg != NULL;
	    msg = strtok_r(NULL, "\n", &last)) {

		// skip blank lines
		if (!msg[0])
			continue;

		// print the line to stderr with the command name
		(void) fprintf(stderr, "%s:  %s\n",
		    commandInstance.getCommandName(), msg);
	}

	// Delete our buffer
	delete buffer;
}

//
// Print the command version for any command in the "cl" family of commands.
//
static void
print_clcommand_version(ClCommand &cmd)
{
	(void) printf("%s %s\n", cmd.getCommandName(), cmd.getCommandVersion());
}

//
// Exception handler, "not enough memory", for the "cl family of commands
//
static void
nomem_exception(void)
{
	clcommand_perror(CL_ENOMEM);
	exit(CL_ENOMEM);
}
