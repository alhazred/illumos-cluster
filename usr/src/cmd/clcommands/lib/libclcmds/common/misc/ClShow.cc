/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *
 * ClShow.cc
 */

#pragma ident	"@(#)ClShow.cc	1.13	09/01/16 SMI"

#include "ClShow.h"
#include "clcommands.h"

// Return the heading, or an empty string if not valid
char *
ClShow::getHeading()
{

	char r_value[1024];

	switch (heading) {

	case HEAD_CLUSTER:
		sprintf(r_value, gettext("=== Cluster ==="));
		break;

	case HEAD_HOSTACCESS:
		sprintf(r_value, gettext("=== Host Access Control ==="));
		break;

	case HEAD_PRIVNET:
		sprintf(r_value, gettext("=== Private Network ==="));
		break;

	case HEAD_NODES:
		sprintf(r_value, gettext("=== Cluster Nodes ==="));
		break;

	case HEAD_FNODES:
		sprintf(r_value, gettext("=== Farm Nodes ==="));
		break;

	case HEAD_SWITCHES:
		sprintf(r_value, gettext("=== Transport Switches ==="));
		break;

	case HEAD_CABLES:
		sprintf(r_value, gettext("=== Transport Cables ==="));
		break;

	case HEAD_QUORUMDEVICES:
		sprintf(r_value, gettext("=== Quorum Devices ==="));
		break;

#ifdef WEAK_MEMBERSHIP
	case HEAD_GLOBALQUORUM:
		sprintf(r_value, gettext("=== Global Quorum ==="));
		break;
#endif
	case HEAD_DEVICEGROUPS:
		sprintf(r_value, gettext("=== Device Groups ==="));
		break;

	case HEAD_RESTYPES:
		sprintf(r_value, gettext("=== Registered Resource Types ==="));
		break;

	case HEAD_RESGROUPS:
		sprintf(r_value,
		    gettext("=== Resource Groups and Resources ==="));
		break;

	case HEAD_RESOURCES:
		sprintf(r_value, gettext("=== Resources ==="));
		break;

	case HEAD_DID:
		sprintf(r_value, gettext("=== DID Device Instances ==="));
		break;

	case HEAD_NASDEVICE:
		sprintf(r_value, gettext("=== NAS Devices ==="));
		break;

	case HEAD_TELEATTR:
		sprintf(r_value, gettext("=== Telemetry Attributes ==="));
		break;

	case HEAD_ZONECLUSTER:
		sprintf(r_value, gettext("=== Zone Clusters ==="));
		break;

	case HEAD_ZC_NODES:
		sprintf(r_value, gettext("=== Zone Cluster Nodes ==="));
		break;

	case SUBHEAD_NODETRANSPORT:
		if (h_var == NULL) {
			printf("Heading variable is null!\n");
			exit(1);
		}
		sprintf(r_value,
		    gettext("--- Transport Adapters for %s ---"), h_var);
		break;

	case HEAD_SNMPHOST:
		if (h_var == NULL) {
			printf("Heading variable is null!\n");
			exit(1);
		}
		sprintf(r_value,
		    gettext("--- SNMP Host Configuration on %s ---"), h_var);
		break;

	case HEAD_SNMPMIB:
		if (h_var == NULL) {
			printf("Heading variable is null!\n");
			exit(1);
		}
		sprintf(r_value,
		    gettext("--- SNMP MIB Configuration on %s ---"), h_var);
		break;

	case HEAD_SNMPUSER:
		if (h_var == NULL) {
			printf("Heading variable is null!\n");
			exit(1);
		}
		sprintf(r_value,
		    gettext("--- SNMP User Configuration on %s ---"), h_var);
		break;

	case SUBHEAD_RGRESOURCES:
		if (h_var == NULL) {
			printf("Heading variable is null!\n");
			exit(1);
		}
		sprintf(r_value, gettext("--- Resources for Group %s ---"),
		    h_var);
		break;

	case SUBHEAD_RTMETHODS:
		if (h_var == NULL) {
			printf("Heading variable is null!\n");
			exit(1);
		}
		sprintf(r_value,
		    gettext("--- Methods for Resource Type %s ---\n"), h_var);
		break;

	case SUBHEAD_UPGTUN:
		if (h_var == NULL) {
			printf("Heading variable is null!\n");
			exit(1);
		}
		sprintf(r_value,
		    gettext("--- Upgrade tunability for Resource Type "
		    "%s ---\n"), h_var);
		break;

	case SUBHEAD_RTPARAMS:
		if (h_var == NULL) {
			printf("Heading variable is null!\n");
			exit(1);
		}
		sprintf(r_value, gettext("--- Param table for Resource Type "
		    "%s ---"), h_var);
		break;

	case SUBHEAD_RSPROPS:
		sprintf(r_value,
		    gettext("--- Standard and extension properties ---"));
		break;

	case SUBHEAD_ZONES:
		if (h_var == NULL) {
			printf("Heading variable is null!\n");
			exit(1);
		}
		sprintf(r_value, gettext("--- Zones on node %s ---"), h_var);
		break;

	case SUBHEAD_TAINST:
		sprintf(r_value,
		    gettext("--- Object Instances of Type \"%s\" ---"), h_var);
		break;

	case SUBHEAD_ZC_NODES:
		sprintf(r_value,
			gettext("--- Zone Cluster Nodes for %s ---"), h_var);
		break;

	case SUBHEAD_ZC_RESOURCES:
		sprintf(r_value,
			gettext("--- Solaris Resources for %s ---"), h_var);
		break;

	default:
		sprintf(r_value, "");

	}

	return (r_value);

}

// Initialize the data
void
ClShow::init()
{
	heading = -1;
	h_var = NULL;
	propSet = true;
	print_NL = true;
}

// Constructor
ClShow::ClShow()
{
	init();
	hasHeading = false;
}

// Constructor, sets heading
ClShow::ClShow(show_heading_t head)
{
	init();
	hasHeading = true;
	heading = (int)head;
}

// Constructor, sets headings and heading variable
ClShow::ClShow(show_heading_t head, char *var)
{
	init();
	hasHeading = true;
	heading = (int)head;
	h_var = strdup(var);
}

// Deconstructor
ClShow::~ClShow()
{
	objects.clear();
	delete [] h_var;
}

// Add an object name and value
void
ClShow::addObject(char *name, char *value)
{
	if (!name || !value)
		return;

	show_object_t *a_object = new show_object_t;
	a_object->name = strdup(name);
	a_object->value = strdup(value);
	a_object->object_num = 1;
	a_object->properties = new NameValueList(propSet);

	objects.push_back(a_object);
}

// Add per-node object
void
ClShow::addNumberedObject(char *name, char *value, int obj_num)
{
	if (!name || !value)
		return;

	show_object_t *a_object = new show_object_t;
	a_object->name = strdup(name);
	a_object->value = strdup(value);
	a_object->object_num = obj_num;
	a_object->properties = new NameValueList(propSet);

	objects.push_back(a_object);
}

// Add an object as the NameValue object, nv
void
ClShow::addObject(NameValue *nv)
{
	addObject(nv->getname(), nv->getvalue());
}

// Adds a property name and value to the list
void
ClShow::addProperty(char *object, char *name, char *value)
{
	if (!object || !name || !value)
		return;

	for (startObjects();
	    !endObjects();
	    nextObjects()) {

		show_object_t *a_object = currentObject();
		if (strcmp(object, a_object->value) == 0) {
			a_object->properties->add(name, value);
		}
	}
}

//
// Adds a property name and value to the list, however, match the object
// name with the name, not value. value is an object name, and
// name is object type, in case or resource properties, we use the
// property name as the object name.
//
void
ClShow::addRSProperty(char *object, char *name, char *value)
{
	if (!object || !name || !value)
		return;

	for (startObjects(); !endObjects(); nextObjects()) {
		show_object_t *a_object = currentObject();
		if (strcmp(object, a_object->name) == 0) {
			a_object->properties->add(name, value);
		}
	}
}

void
ClShow::addPropertyFirst(char *object, char *name, char *value)
{
	if (!object || !name || !value)
		return;

	for (startObjects();
	    !endObjects();
	    nextObjects()) {

		show_object_t *a_object = currentObject();
		if (strcmp(object, a_object->value) == 0) {
			a_object->properties->addFirst(name, value);
		}
	}
}

// Adds a property name and value, using those in nv.
void
ClShow::addProperty(char *object, NameValue *nv)
{
	addProperty(object, nv->getname(), nv->getvalue());
}

// Sets the properties to be equal to the list, nvl.
void
ClShow::addProperties(char *object, NameValueList *nvl)
{
	for (nvl->start();
	    !nvl->end();
	    nvl->next()) {

		addProperty(object, nvl->getName(), nvl->getValue());
	}
}

// Add a ClShow object as a child.
// Return 1 if the child could not be added to the object,
// otherwise return 0
int
ClShow::addChild(char *object, ClShow *child)
{
	if (!object)
		return (1);

	for (startObjects();
	    !endObjects();
	    nextObjects()) {

		show_object_t *a_object = currentObject();
		if (strcmp(object, a_object->value) == 0) {
			a_object->children.push_back(child);
			return (0);
		}
	}
	return (1);
}

void
ClShow::startObjects()
{
	object_iter = objects.begin();
}

void
ClShow::nextObjects()
{
	if (object_iter != objects.end())
		++object_iter;
}

int
ClShow::endObjects()
{
	return (object_iter == objects.end() ? 1 : 0);
}

ClShow::show_object_t *
ClShow::currentObject()
{
	return ((*object_iter));
}

bool
ClShow::objectExists(char *name)
{
	for (startObjects(); !endObjects(); nextObjects()) {
		if (strcmp(name, currentObject()->value) == 0)
			return (true);
	}

	return (false);
}

int
ClShow::getObjectSize()
{
	return (objects.size());
}

// Prints the data
int
ClShow::print()
{
	int r_value = print(this, 0);
	printf("\n");

	return (r_value);
}

// Prints the data of ClShow object, obj, with the given
// indentation level, level.
int
ClShow::print(ClShow *obj, int level)
{
	int r_value = 0;
	char object_name[BUFSIZ];

	// Indent if necessary
	char indent[20];
	indent[0] = '\0';
	if (indent == NULL) {
		r_value = 1;
		goto cleanup;
	}
	for (int i = 0; i < level; i++) {
		(void) sprintf(indent, "%s  ", indent);
	}

	if (obj->print_NL) {
		printf("\n");
	}

	// Print heading if there is one
	if (obj->hasHeading) {
		char head[BUFSIZ];
		(void) sprintf(head, "%s%s", indent, obj->getHeading());
		printf("%-*s\n",
		    COL_WIDTH - 1,
		    head);
	}

	for (obj->startObjects();
	    !obj->endObjects();
	    obj->nextObjects()) {

		show_object_t *object = obj->currentObject();

		//
		// Print a newline unless it is in a series of one-line
		// objects or is numbered 2nd or more in a series of
		// pernode property objects.
		//
		if (obj->print_NL) {
			if (object->object_num == 1) {
				printf("\n");
			}
		}

		// Print object name and value
		object_name[0] = '\0';
		(void) sprintf(object_name, "%s%s:", indent, object->name);

		printf("%-*s %s\n",
		    COL_WIDTH -1,
		    object_name,
		    object->value);

		// Print the properties
		for (object->properties->start();
		    !object->properties->end();
		    object->properties->next()) {

			char propname[BUFSIZ];
			(void) sprintf(propname, "%s  %s:", indent,
			    object->properties->getName());

			char *propvalue = new char
			    [strlen(object->properties->getValue()) + 4];
			(void) sprintf(propvalue, "   %s",
			    object->properties->getValue());

			printf("%-*s %s\n",
			    COL_WIDTH - 1,
			    propname,
			    propvalue);
			delete [] propvalue;
		}

		// Handle the children
		std::list<ClShow *>::iterator iter;
		for (iter = object->children.begin();
		    iter != object->children.end();
		    iter++) {

			r_value = print(((*iter)), level + 1);
			if (r_value != 0)
				break;
		}
	}

cleanup:

	return (r_value);
}
