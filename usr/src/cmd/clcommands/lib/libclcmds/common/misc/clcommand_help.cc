//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident   "@(#)clcommand_help.cc	1.4	08/09/22 SMI"

//
// Output help and usage
//

#include "clcommands.h"

#include "ClCommand.h"

static void print_command_help(FILE *fp, ClCommand &cmd);
static void print_subcommand_help(FILE *fp, ClCommand &cmd,
    char *subcommandname);

// Print command help message
void
clcommand_help(ClCommand &cmd)
{
	print_command_help(stdout, cmd);
}

// Print subcommand help message
void
clcommand_help(ClCommand &cmd, char *subcommandname)
{
	print_subcommand_help(stdout, cmd, subcommandname);
}

// Print command usage message for one error
void
clcommand_usage(ClCommand &cmd)
{
	clerror("Usage error.\n\n");
	print_command_help(stderr, cmd);
}

// Print command usage message for some number of errors
void
clcommand_usage(ClCommand &cmd, int errcount)
{
	if (errcount > 1) {
		clerror("Usage errors.\n\n");
		print_command_help(stderr, cmd);
	} else {
		clcommand_usage(cmd);
	}
}

// Print subcommand usage message
void
clcommand_usage(ClCommand &cmd, char *subcommandname)
{
	clerror("Usage error.\n\n");
	print_subcommand_help(stderr, cmd, subcommandname);
}

// Print subcommand command usage message for some number of errors
void
clcommand_usage(ClCommand &cmd, char *subcommandname, int errcount)
{
	if (errcount > 1) {
		clerror("Usage errors.\n\n");
		print_subcommand_help(stderr, cmd, subcommandname);
	} else {
		clcommand_usage(cmd, subcommandname);
	}
}

// Print command help or usage
static void
print_command_help(FILE *fp, ClCommand &cmd)
{
	char *description;
	char **usage;
	clsubcommand_t *subcommands;
	const int usage_width = 10;		// usage col width
	const int subcmd_width = 15;		// subcommand name col width

	// Usage lines
	if (((usage = cmd.getCommandUsageLines()) != (char **)0) && *usage) {
		char buffer[CLCOMMANDS_BUFSIZ];

		// All usage strings include a single "%s" for command name
		(void) sprintf(buffer, *usage++, cmd.getCommandName());
		(void) fprintf(fp, "%-*s %s\n", usage_width - 1,
		    gettext("Usage:"), buffer);

		// Print any additional usage messages
		while (*usage) {
			(void) sprintf(buffer, *usage++, cmd.getCommandName());
			(void) fprintf(fp, "%*s %s\n", usage_width - 1,
			    "", buffer);
		}
		(void) putc('\n', fp);
	}

	// Command description
	if ((description = cmd.getCommandDescription()) != (char *)0) {
		(void) fprintf(fp, "%s\n", gettext(description));
		(void) putc('\n', fp);
	}

	// Print help line for each subcommand
	if (((subcommands = cmd.getListOfSubcommands()) != 0) &&
	    subcommands->subcommand_name) {
		// Check if Europa is configured
		boolean_t iseuropa = B_FALSE;
		scconf_errno_t scconf_err = scconf_iseuropa(&iseuropa);

		(void) fprintf(fp, "%s:\n", gettext("SUBCOMMANDS"));
		(void) putc('\n', fp);
		while (subcommands->subcommand_name) {

			// Skip undocumented subcommands
			if (subcommands->flags & CLSUB_UNDOCUMENTED) {
				++subcommands;
				continue;
			}

			// Skip europa subcommands if Europa isn't enabled
			if (subcommands->flags & CLSUB_CHECK_EUROPA) {
				if (!iseuropa ||
				    (scconf_err != SCCONF_NOERR)) {
					++subcommands;
					continue;
				}
			}

			// Print subcommand
			(void) fprintf(fp, "%-*s ", subcmd_width - 1,
			    subcommands->subcommand_name);

			// Print following subcommand description
			if (subcommands->subcommand_description) {
				(void) fputs(gettext(
				    subcommands->subcommand_description), fp);
			}
			(void) putc('\n', fp);
			++subcommands;
		}
		(void) putc('\n', fp);
	}
}

// Print subcommand help or usage
static void
print_subcommand_help(FILE *fp, ClCommand &cmd, char *subcommandname)
{
	char *description;
	char **usage;
	clsubcommand_t *subcommands;
	clsubcommand_t *s;
	cloptions_t *options;
	const int usage_width = 10;		// usage col width

	const int option_margin1 = 2;		// margin for option letter
	const int option_margin2 = 8;		// margin for description
						// (margin2 - margin1) MUST
						// always be greater than 2!

	// Find our subcommand
	if ((subcommands = cmd.getListOfSubcommands()) == 0) {
		return;
	}
	for (s = subcommands;  s->subcommand_name;  ++s) {
		if (strcmp(subcommandname, s->subcommand_name) == 0)
			break;
	}
	if (s->subcommand_name == 0) {
		return;
	}

	// Usage lines
	if (((usage = s->subcommand_usage) != (char **)0) && *usage) {
		char buffer[CLCOMMANDS_BUFSIZ];

		// All usage strings include a single "%s" for command name
		(void) sprintf(buffer, gettext(*usage++),
		    cmd.getCommandName());
		(void) fprintf(fp, "%-*s %s\n", usage_width - 1,
		    gettext("Usage:"), buffer);

		// Print any additional usage messages
		while (*usage) {
			(void) sprintf(buffer, gettext(*usage++),
			    cmd.getCommandName());
			(void) fprintf(fp, "%*s %s\n", usage_width - 1,
			    "", buffer);
		}
		(void) putc('\n', fp);
	}

	// Subcommand description
	if ((description = s->subcommand_description) != (char *)0) {
		(void) fprintf(fp, "%s\n", gettext(description));
		(void) putc('\n', fp);
	}

	// Print command options and their descriptions
	if (((options = s->subcommand_options) != 0) &&
	    options->shortoption && options->longoption) {
		(void) fprintf(fp, "%s:\n", gettext("OPTIONS"));
		(void) putc('\n', fp);
		while (options->shortoption && options->longoption) {
			const char *ldescription;	// localized

			// Skip undocumented options
			if (options->flags & CLOPT_UNDOCUMENTED) {
				++options;
				continue;
			}

			// Print option synopsis
			(void) fprintf(fp, "%-*s-%c", option_margin1, "",
			    options->shortoption);
			if (options->optionarg) {
				(void) fprintf(fp, " %s", options->optionarg);
				if (options->flags & CLOPT_ARG_LIST) {
					(void) fprintf(fp, "[,...]");
				}
				(void) putc('\n', fp);
				(void) fprintf(fp, "%-*s", option_margin2, "");
			} else {
				(void) fprintf(fp, "%-*s",
				    (option_margin2 - option_margin1 - 2), "");
			}

			//
			// Print option description
			//
			// Look for any embedded '\n' characters, and
			// split the lines using correct margins.
			//
			ldescription = gettext(options->description);
			if (strchr(ldescription, '\n') == 0) {
				(void) fprintf(fp, "%s\n", ldescription);
			} else {
				char *buffer = new char[
				    strlen(ldescription) + 1];

				(void) strcpy(buffer, ldescription);
				description = strtok(buffer, "\n");
				(void) fprintf(fp, "%s\n", description);
				description = strtok((char *)0, "\n");
				while (description != (char *)0) {
					(void) fprintf(fp, "%-*s%s\n",
					    option_margin2, "", description);
					description = strtok((char *)0, "\n");
				}
				delete buffer;
			}
			(void) putc('\n', fp);

			// Next
			++options;
		}
		(void) putc('\n', fp);
	}
}
