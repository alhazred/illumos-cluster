//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
#pragma ident	"@(#)common.cc	1.21	09/04/20 SMI"

// This file will contains functions which
// are commonly used across the new-cli library.
// The purpose of having this file is to
// avoid duplicity of code in the new-cli codebase.
// If you want to write a function which would be
// used in more places than one, then you might
// want to consider it defining here and declaring
// it in common.h

#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>
#include "ClList.h"
#include "common.h"
#if (SOL_VERSION >= __s10)
#include <rgm/sczones.h>
#include <libzccfg/libzccfg.h>
#include <sys/vc_int.h>
#endif

#ifdef WEAK_MEMBERSHIP
#include <ccr/persistent_table.h>
#include <sys/socket.h>		/* for AF_INET */
#include <arpa/inet.h>		/* for IP mappings */
#include <netinet/in.h>		/* for socket options */
#endif // WEAK_MEMBERSHIP

#define	SCRCMD		"/usr/cluster/lib/sc/scrcmd"
#define	DEV_CONSOLE	"/dev/console"

static void *start_command_exec(void *input);
static void *start_remote_command_exec(void *input);

#if (SOL_VERSION >= __s10)
//
// check_zc_operation(cluster_obj_type obj_type, cluster_op_type op_type,
//			char *zcname);
//
// This method will check whether the specified operation(op_type) is
// allowed on the cluster object (obj_type) in the zone cluster (zcname).
// This method checks for the validity of the zone cluster, zcname,
// and reports an error if it does not exist. Essentially, there are
// certain operations which can be allowed from the global zone on a zone
// cluster based on the status of the zone cluster. And there are certain
// operations which are not allowed. For example, it is not possible to
// create/delete RGs in a zone cluster, when the ZC status is less then
// "Running". This kind of check is handled here. If an operation is not
// allowed this method will return CL_EACCESS. If it is allowed this method
// will return CL_NOERR.
// Most of the commands, which can operate on a zone cluster, call this
// method before performing the actual operation. This method can serve as a
// central place where all such checks can be made.
//
// Return Values :
//	CL_NOERR	- Operation is allowed.
//	CL_EACCESS	- Operation not allowed.
//	CL_ENOENT	- No such zone cluster.
//	CL_EINTERNAL	- Internal error.

extern clerrno_t
check_zc_operation(cluster_obj_type obj_type, cluster_op_type op_type,
    char *zcname) {

	clerrno_t cl_err = CL_NOERR;
	int zc_cfg_err = ZC_OK;
	zone_cluster_state_t zc_state = ZC_STATE_UNKNOWN;

	if (zcname == NULL) {
		// Nothing to check
		return (CL_NOERR);
	}

	// Check whether this is a valid zone cluster
	cl_err = check_zone_cluster_exists(zcname);

	if (cl_err != CL_NOERR) {
		clerror("%s is not a valid zone cluster.\n", zcname);
		return (cl_err);
	}

	// Now, get the zone state.
	zc_cfg_err = get_zone_cluster_state(zcname, &zc_state);
	if (zc_cfg_err != ZC_OK) {
		clerror("Failed to determine state of zone cluster %s\n",
			zcname);
		return (CL_EINTERNAL);
	}

	if ((obj_type == CL_OBJ_TYPE_RS) ||
		(obj_type == CL_OBJ_TYPE_RG) ||
		(obj_type == CL_OBJ_TYPE_RT)) {
		// Operations on RGM objects are allowed only
		// when a zone cluster is in "Running" state.
		if (zc_state < ZC_STATE_RUNNING) {
			cl_err = CL_EACCESS;
		}
	}

	if (cl_err == CL_EACCESS) {
		clerror("This operation is not allowed on a zone cluster "
			"in \"%s\" state\n", zone_cluster_state_str(zc_state));
	}

	return (cl_err);
}
#endif

//
// names_to_valuelist(const char **names, ValueList names_list);
//
// This method adds each string in "names" to valuelist
// "names_list". Null values will not be added and
// the caller of this function is responsible to clear
// the memory in "names_list"
//
// Return Values :
//	CL_NOERR	- No error.
//	CL_ENOMEM	- Not enough memory

clerrno_t
names_to_valuelist(char **names, ValueList &names_list) {

	int count;

	if (!names) {
		return (CL_NOERR);
	}

	for (count = 0; names[count] != NULL; count++) {
		names_list.add(names[count]);
	}

	return (CL_NOERR);
}

//
// clcommand_zc_usage_check(ValueList &zc_list);
//
// This method checks basic usage of the -Z option.
// Some commands allow the -Z option to be specified
// multiple times. This method ensures that if "all"
// is specified, then other zone cluster names should
// not be specified.
//
// Return Values :
//	CL_NOERR	- No error.
//	CL_EINVAL	- Invalid usage.

clerrno_t
clcommand_zc_usage_check(ValueList &zc_list) {

	if (zc_list.getSize() < 2) {
		// Either no zone-clusters were specified,
		// or only one zone cluster was specified.
		// There is no need to check for any errors
		// in this case.
		return (CL_NOERR);
	}

	// If we are here, then multiple zone clusters were
	// specified. We have to ensure that "all" was not
	// one among them.
	if (!zc_list.isValue(ALL_CLUSTERS_STR)) {
		return (CL_NOERR);
	}

	// If we are here, it means multiple zone clusters
	// were specified, and "all" is one among them.
	clerror("You must specify either \"%s\" or a list of "
			"zone clusters.\n", ALL_CLUSTERS_STR);
	return (CL_EINVAL);
}

//
// parse_zc_names_from_names_str(char **names, ValueList zc_names);
//
// This method will read each string in "names" and parse it
// to read the zone cluster name. The format expected by this
// method is <zonecluster name>:<obj name>. The caller of this
// method is responsible for freeing the memory in "zc_names".
//
// Return Values :
//	CL_NOERR	- No error.
//	CL_ENOMEM	- Not enough memory

clerrno_t
parse_zc_names_from_names_str(char **names, ValueList zc_names) {

	char *tmpStr;
	int count;

	if (!names) {
		return (CL_NOERR);
	}

	for (count = 0; names[count] != NULL; count++) {

		if (! strchr(names[count], ':')) {
			// This string does not contain ":".
			// No zone cluster name, so check the next one.
			continue;
		}

		tmpStr = strdup(names[count]);

		if (!tmpStr) {
			return (CL_ENOMEM);
		}

		tmpStr = strtok(tmpStr, ":");

		if (!tmpStr || (!(strcmp(tmpStr, "")))) {
			continue;
		}

		// If we are here, then it means that tmpStr
		// is a valid string. Before adding it to
		// zc_names we have to ensure that it is not
		// already present in the list.
		if (zc_names.isValue(tmpStr)) {
			free(tmpStr);
			continue;
		}

		// If we are here, then we can add it.
		zc_names.add(tmpStr);
		free(tmpStr);
	}

	return (CL_NOERR);
}

//
// parse_zc_names_from_valuelist(ValueList names, ValueList zc_names);
//
// This method will read each string in "names" and parse it
// to read the zone cluster name. The format expected by this
// method is <zonecluster name>:<obj name>. The caller of this
// method is responsible for freeing the memory in "zc_names".
//
// Return Values :
//	CL_NOERR	- No error.
//	CL_ENOMEM	- Not enough memory

clerrno_t
parse_zc_names_from_valuelist(ValueList names, ValueList zc_names) {

	char *tmpStr;

	if (names.getSize() < 1) {
		return (CL_NOERR);
	}

	for (names.start(); !names.end(); names.next()) {

		if (! strchr(names.getValue(), ':')) {
			// This string does not contain ":".
			// No zone cluster name, so check the next one.
			continue;
		}

		tmpStr = strdup(names.getValue());

		if (!tmpStr) {
			return (CL_ENOMEM);
		}

		tmpStr = strtok(tmpStr, ":");

		if (!tmpStr || (!(strcmp(tmpStr, "")))) {
			continue;
		}

		// If we are here, then it means that tmpStr
		// is a valid string. Before adding it to
		// zc_names we have to ensure that it is not
		// already present in the list.
		if (zc_names.isValue(tmpStr)) {
			free(tmpStr);
			continue;
		}

		// If we are here, then we can add it.
		zc_names.add(tmpStr);
		free(tmpStr);
	}

	return (CL_NOERR);
}

//
// remove_zc_names_from_valuelist(ValueList names, ValueList obj_names);
//
// This method will read each string in "names" and parse it
// to read the zone cluster name. The format expected by this
// method is <zonecluster name>:<obj name>. This method will then
// remove the zone cluster name and add the pure object name to
// "obj_names".The caller of this method is responsible for freeing
// the memory in "obj_names".
//
// Return Values :
//	CL_NOERR	- No error.
//	CL_ENOMEM	- Not enough memory

clerrno_t
remove_zc_names_from_valuelist(ValueList &names, ValueList &obj_names) {

	char *tmp_str, *tmp_name;
	char *obj_name;

	if (names.getSize() < 1) {
		return (CL_NOERR);
	}

	for (names.start(); !names.end(); names.next()) {
		if (! strchr(names.getValue(), ':')) {
			// This string does not contain ":".
			// No zone cluster name. Just add this
			// to obj_names and return.
			obj_names.add(names.getValue());
			continue;
		}

		// If we are here, it means this string was scoped
		// with a zone cluster name.
		tmp_name = strdup(names.getValue());
		tmp_str = tmp_name;
		strtok(tmp_str, ":");
		obj_name = strtok(NULL, ":");
		obj_names.add(obj_name);
		free(tmp_name);
	}

	return (CL_NOERR);
}

//
// remove_zc_names_from_names(char** obj_names);
//
// This method will read each string in "names" and parse it
// to read the zone cluster name. The format expected by this
// method is <zonecluster name>:<obj name>. This method will then
// remove the zone cluster name and update the string with the pure
// object name to.
// Note that this method will modify the contents of "obj_names".
//
// Return Values :
//	CL_NOERR	- No error.
//	CL_ENOMEM	- Not enough memory

clerrno_t
remove_zc_names_from_names(char **obj_names) {

	clerrno_t clerrno = CL_NOERR;
	int count;
	char *obj_name, *zc_name;
	scconf_errno_t scconf_err = SCCONF_NOERR;

	if (obj_names == NULL) {
		return (CL_NOERR);
	}

	obj_name = zc_name = NULL;

	for (count = 0; obj_names[count] != NULL; count++) {

		if (obj_name) {
			free(obj_name);
			obj_name = NULL;
		}
		if (zc_name) {
			free(zc_name);
			zc_name = NULL;
		}

		scconf_err = scconf_parse_obj_name_from_cluster_scope(
				obj_names[count], &obj_name, &zc_name);

		if (scconf_err != SCCONF_NOERR) {
			clerrno = CL_ENOMEM;
			break;
		}

		if (zc_name == NULL) {
			// no need to do anything.
			continue;
		}

		// If we are here, we have to update this string.
		free(obj_names[count]);
		if (obj_name == NULL) {
			// We should not be coming here in an ideal case.
			obj_names[count] = NULL;
			continue;
		}
		obj_names[count] = new char[(strlen(obj_name) + 1)];
		strcpy(obj_names[count], obj_name);
	}
	// Free any memory
	if (obj_name) {
		free(obj_name);
		obj_name = NULL;
	}
	if (zc_name) {
		free(zc_name);
		zc_name = NULL;
	}

	return (clerrno);
}

/*
 * exec_command(char *cmd_str)
 *
 * Executes the command specified by "cmd_str".
 * This function waits for the command to finish execution
 * and then returns. Essentially invokes the "execv" call.
 * Returns the exit value from the exec'd command.
 * Possible return values:
 *
 *	CL_NOERR		- success
 *	CL_EIO			- io error
 *	CL_EINTERNAL		- internal error
 *	CL_EINVAL		- invalid argument(command)
 */

clerrno_t
exec_command(char *cmd_str)
{
	int stat = 0;
	char **strarray;
	char *pch;
	int space_count = 0;
	ValueList param_list;
	int count = 0;
	pid_t pid;
	char *tokens;

	if (strlen(cmd_str) == 0) {
		return (CL_EINVAL);
	}

	// First form the char array required to invoke execv()
	// Tokenize on spaces
	pch = strtok(cmd_str, " ");

	while (pch != NULL) {
		// Add each argument to the list
		tokens = strdup(pch);
		param_list.add(tokens);
		pch = strtok(NULL, " ");
		free(tokens);
	}

	strarray = new char *[param_list.getSize() + 1];

	// Add each value in the list to the strarray
	for (param_list.start(); !param_list.end(); param_list.next()) {
		strarray[count++] = param_list.getValue();
	}
	// NULL terminate the last one
	strarray[count] = NULL;

	// The command str has been formed, we have to execute the command.

	// Ignoring SIG_INT
	(void) sigset(SIGINT, SIG_IGN);

	// Fork
	pid = fork();
	if (pid == 0) {
		if (execv(strarray[0], strarray) == -1) {
			clerror("Cannot execute \"%s\" - %s.\n",
			    strarray[0], strerror(errno));
			exit(1);
		}
		exit(0);
	} else {
		int status = 0;
		while ((status = waitpid(pid, &stat, 0)) != pid) {
			if (status == -1 && errno == EINTR)
				continue;
			else {
				status = -1;
				break;
			}
		}
		if (status != pid) {
			clerror("Error waiting for command result - %s\n",
			    strerror(errno));
			return (CL_EINTERNAL);
		}
	}

	// free the memory
	param_list.clear();
	free(strarray);

	// Check if the child process exited normally if not return
	// Internal Error
	if (!WIFEXITED(stat)) {
		return (CL_EINTERNAL);
	}

	// Return the exit code
	return (WEXITSTATUS(stat));
}

/*
 * Executes and pipes the command .
 *
 * Caller is responsible to close the pipe.
 * "file" will be updated to be a pointer to the stream.
 * "cmd" is the command to be executed.
 *
 * Possible return values:
 *
 *	CL_NOERR		- success
 *	CL_EIO			- io error during the pipe
 *	CL_EINTERNAL		- internal error
 *	CL_EINVAL		- invalid argument(command)
 */
clerrno_t exec_and_pipe_cmd(FILE **file, char *cmd) {

	if (!cmd) {
		return (CL_EINVAL);
	}

	*file = NULL;
	*file = popen(cmd, "r");

	if (file != NULL) {
		/* If we are here then there was no erro */
		return (CL_NOERR);
	}

	/* If we are here then there was an error */
	switch (errno) {

	case EMFILE:
		return (CL_EIO);
	default:
		return (CL_EINTERNAL);
	}
}

/*
 * Executes a given command("cmd") in a new thread .
 * This function can be used to execute time consuming commands.
 * This function internally uses "exec_and_pipe_cmd" to
 * execute a command. The "thread" variable is updated with the
 * thread ID as is given by the pthread_create() function call.
 * The caller is responsible to determine when to join and wait
 * for the thread.
 *
 * Possible return values:
 *
 * 	CL_NOERR		- success
 *	CL_EIO			- io error during the pipe
 *	CL_EINTERNAL		- internal error
 *	CL_EINVAL		- invalid argument
 */
clerrno_t execute_cmd_in_new_thread(pthread_t *thread, char *cmd) {

	if (!cmd) {
		return (CL_EINVAL);
	}

	int pthread_err = 0;

	*thread = NULL;
	// create the thread
	pthread_err = pthread_create(thread, NULL,
		start_command_exec, (void *)cmd);

	if (pthread_err != 0)
		return (CL_EINTERNAL);

	// If we are here then it means the thread has been activated.
	return (CL_NOERR);
}

/*
 * Executes a given command on the specified node in a separate
 * thread. This function can be used to execute time consuming
 * commands. The "thread" variable is updated with the
 * thread ID as is given by the pthread_create() function call.
 * The caller is responsible to determine when to join and wait
 * for the thread. This function uses the "scrcmd" command to
 * execute the command on a remote node. This function uses the
 * struct cl_cmd_info to fetch the required information. In
 * the case where the command being executed causes an error,
 * this function will invoke the error handled specified.
 *
 * Possible return values:
 *
 * 	CL_NOERR		- success
 *	CL_EIO			- io error during the pipe
 *	CL_EINTERNAL		- internal error
 *	CL_EINVAL		- invalid argument
 */
clerrno_t execute_cmd_at_node_in_new_thread(pthread_t *thread,
	cl_cmd_info_t *cmd_info) {

	if (!cmd_info->cmd_str) {
		return (CL_EINVAL);
	}

	int pthread_err = 0;
	cl_cmd_info_t *cmd_ptr;

	*thread = NULL;
	cmd_ptr = cmd_info;

	// create the thread
	pthread_err = pthread_create(thread, NULL, start_remote_command_exec,
					(void *)cmd_ptr);

	if (pthread_err != 0)
		return (CL_EINTERNAL);

	// If we are here then it means the thread has been activated.
	return (CL_NOERR);
}

//
// get_valid_nodes
//
// Filters the given nodelist and returns only the set of
// valid cluster nodes.
//
// Return Values :
//	CL_NOERR	- All nodes in the list are valid
//	CL_EINVAL	- One or more nodes are not found or
//			  not in cluster
//
clerrno_t
get_valid_nodes(ValueList &nodes, ValueList &valid_nodes)
{
	scconf_errno_t scconf_err = SCCONF_NOERR;
	char *cur_node = NULL;
	unsigned int cur_node_id;
	clerrno_t err_code = CL_NOERR;
	uint_t ismember;

	for (nodes.start(); !nodes.end(); nodes.next()) {
		cur_node = nodes.getValue();

		if (strlen(cur_node) == 0) {
			/* Ignore empty strings */
			continue;
		}

		scconf_err = scconf_get_nodeid(cur_node, &cur_node_id);

		if (scconf_err != SCCONF_NOERR) {
			clerror("\"%s\" is not a valid base cluster node.\n",
				cur_node);
			err_code = CL_ENOENT;
		} else {
			scconf_err = scconf_ismember(cur_node_id, &ismember);

			if (!ismember) {
				clerror("\"%s\" is not in cluster mode.\n",
					cur_node);
				err_code = CL_ENOENT;
			} else {
				/* If we are here then it's a valid node */
				valid_nodes.add(cur_node);
			}
		}
	}

	return (err_code);
}

#if (SOL_VERSION >= __s10)
//
// get_valid_zone_clusters
//
// Filters the given zone cluster list and returns only the set of
// valid zone cluster names.
// This method functions according to the context from which it has
// been called. If called from a global zone, this function will
// check whether each zone cluster is valid or not by checking
// the zone cluster id. If called from a zone cluster, this function
// will report an error unless a zone cluster name matches with the
// name of the current zone cluster. Please note that "global"
// will be treated as a valid zone cluster name, when this method
// is called in a global zone.
//
// Return Values :
//	CL_NOERR	- All zone clusters in the list are valid
//	CL_ENOENT	- One or more zone clusters are invalid/unknown
//	CL_EINTERNAL	- Internal Error.
//
clerrno_t
get_valid_zone_clusters(ValueList &zc_names, ValueList &valid_zc_names)
{
	clerrno_t clerrno = CL_NOERR;
	int clconf_err;
	uint_t cluster_id;
	bool gz_flag = true;

	if (sc_nv_zonescheck()) {
		// We are inside a zone
		gz_flag = false;
	}

	for (zc_names.start(); !zc_names.end(); zc_names.next()) {

		// If we are inside the global zone and "global"
		// was specified, then we have to treat it as a
		// valid zone cluster name. We will hard-code the
		// string "global" here.
		if (gz_flag && (!(strcmp(zc_names.getValue(),
					"global")))) {
			valid_zc_names.add(zc_names.getValue());
			continue;
		}

		// Check the cluster id.
		clconf_err = clconf_get_cluster_id(zc_names.getValue(),
						&cluster_id);

		if ((!clconf_err) && (cluster_id >= MIN_CLUSTER_ID)) {
			// If we are here, then there was no error.
			valid_zc_names.add(zc_names.getValue());
			continue;
		}

		// If we are here, then there was either an error or the
		// cluster_id was less than MIN_CLUSTER_ID.
		// If cluster_id is less than MIN_CLUSTER_ID, then it
		// means the name specified is the
		// physical cluster name.
		clerror("%s is not a valid zone cluster.\n",
				zc_names.getValue());
		if (clerrno == CL_NOERR) {
			clerrno = CL_ENOENT;
		}
	}

	return (clerrno);
}

//
// get_all_clusters
//
// Adds all the known zone cluster names to "all_clusters". If this method
// is called in a global zone, this method will append "global" to the list.
// If this method is called in a zone cluster, this method will append the
// name of the zone cluster itself. This method internally calls
// get_zone_cluster_names() defined in clzonecluster.h
// Return Values :
//	CL_NOERR	- No Error
//	CL_EACCESS	- Permission Error
//	CL_EINTERNAL	- Internal Error.
extern clerrno_t
get_all_clusters(ValueList &all_clusters)
{
	bool gz_flag = true;
	boolean_t zc_flag = B_FALSE;
	scconf_errno_t scconf_err = SCCONF_NOERR;
	clerrno_t clerrno = CL_NOERR;
	char *cl_name = NULL;

	if (sc_nv_zonescheck()) {
		// We are inside a zone
		gz_flag = false;

		// Check whether we are in a zone cluster.
		scconf_err = scconf_zone_cluster_check(&zc_flag);

		if (scconf_err != SCCONF_NOERR) {
			return (CL_EINTERNAL);
		}
	}

	if ((gz_flag == false) && (zc_flag == B_FALSE)) {
		// If we are here, it means we are inside a
		// native zone. We are not allowed to call this
		// method.
		return (CL_EACCESS);
	}

	if (zc_flag == B_TRUE) {
		// If we are inside a zone cluster, we just
		// append the cluster name.
		cl_name = clconf_get_clustername();
		if (!cl_name) {
			clerror("Cannot get the cluster name.\n");
			return (CL_EINTERNAL);
		}

		// Add it and return.
		all_clusters.add(cl_name);
		free(cl_name);
		return (CL_NOERR);
	}

	// Ok. We are inside the global zone. So, we add the
	// Fuhrer first.
	all_clusters.add(GLOBAL_CLUSTER_NAME);
	// Now, we add the rest.
	clerrno = get_zone_cluster_names(all_clusters);
	return (clerrno);
}

#endif

//
// check_single_zc_specified(ValueList names, char **zc_name,
//	boolean_t is_name);
//
// This method parses all the object names specified in the list "names"
// for zone cluster name and ensures that each name in the list
// has been scoped to the same zone cluster name. The format
// for which this method parses is <zonecluster name>:<object name>.
// This method set the value of "is_same" to "B_TRUE" if all the names
// had the same zone cluster name (or if none of them had any zone
// cluster scoping). In any other case, this method returns false.
// If this method is called from a global zone, this method would
// treat "global" zonecluster name same as a NULL zonecluster name.
// The zonecluster name, which was found in all the object names,
// will be set as the value in "zc_name". This method will not check
// for the validity of the zone cluster. The caller of this method
// has to free the memory of "zc_name".
//
// Return Values :
//	CL_NOERR	- No error.
//	CL_EINVAL	- More than one zone cluster was specified.
//	CL_EINTERNAL	- Internal Error.
//
clerrno_t
check_single_zc_specified(ValueList &names, char **zc_name,
    boolean_t *is_same,
    scconf_errno_t (*scconf_parser)(char *fullname, char **objname,
					char **zcname)) {

	bool gz_flag = true;
	bool change_flag = false;
	char *zone_cluster = NULL;
	char *tmp_name, *tmp_zc = NULL;
	scconf_errno_t scconf_err = SCCONF_NOERR;
	clerrno_t clerrno = CL_NOERR;

	if (names.getSize() < 1) {
		return (CL_NOERR);
	}

	// Check whether we are inside a global zone.
	if (sc_nv_zonescheck()) {
		// We are inside a zone
		gz_flag = false;
	}

	// Set the default parser
	if (scconf_parser == NULL) {
		scconf_parser = scconf_parse_obj_name_from_cluster_scope;
	}

	// Get the zone cluster name from the first name
	names.start();
	scconf_err = scconf_parser(names.getValue(), &tmp_name,
				&zone_cluster);

	if (scconf_err != SCCONF_NOERR) {
		clerrno = CL_NOERR;
		goto cleanup;
	}

#if (SOL_VERSION >= __s10)
	// If we are in the global zone and "global" was specified,
	// we have to treat it as NULL
	if (gz_flag && zone_cluster &&
		(!(strcmp(zone_cluster, GLOBAL_ZONE_NAME)))) {
		free(zone_cluster);
		zone_cluster = NULL;
	}
#endif

	if (tmp_name) {
		free(tmp_name);
		tmp_name = NULL;
	}

	// Now check whether all the other zone clusters are the same.
	for (names.next(); !names.end(); names.next()) {
		if (tmp_name) {
			free(tmp_name);
			tmp_name = NULL;
		}
		if (tmp_zc) {
			free(tmp_zc);
			tmp_zc = NULL;
		}

		scconf_err = scconf_parse_obj_name_from_cluster_scope(
					names.getValue(), &tmp_name,
					&tmp_zc);
		if (scconf_err != SCCONF_NOERR) {
			clerrno = CL_NOERR;
			goto cleanup;
		}

#if (SOL_VERSION >= __s10)
		// If we are in global zone and "global" was specified,
		// we have to set it NULL
		if (gz_flag && tmp_zc &&
			(!(strcmp(tmp_zc, GLOBAL_ZONE_NAME)))) {
			free(tmp_zc);
			tmp_zc = NULL;
		}
#endif
		if ((!zone_cluster) && (!tmp_zc)) {
			// If both are null, just continue.
			continue;
		}

		if (zone_cluster && tmp_zc &&
			(!(strcmp(zone_cluster, tmp_zc)))) {
			// If both are not null and are equal, just continue.
			continue;
		}

		// If we are here, it means there was a difference.
		change_flag = true;
		break;
	}

	if (!change_flag) {
		*is_same = B_TRUE;
		if (zone_cluster) {
			*zc_name = strdup(zone_cluster);
		} else {
			*zc_name = NULL;
		}
	} else {
		*is_same = B_FALSE;
	}
cleanup:
	if (tmp_name) {
		free(tmp_name);
	}
	if (tmp_zc) {
		free(tmp_zc);
	}
	if (zone_cluster) {
		free(zone_cluster);
	}
	return (clerrno);
}

//
// check_file_exists
//
// This function checks whether the specified file exists or not.
// It also checks if the file is readable or not. If the file exists
// and is also readable, then this function returns CL_ENOERR. If
// the file does not exist or if it is not readable then this
// function returns CL_ENOENT. If the filename is not specified
// this function returns CL_EINVAL
//
// Return Values :
//	CL_NOERR	- File exists and is readable
//	CL_EINVAL	- File name was not specified
//	CL_ENOENT	- File does not exist or is not readable
clerrno_t
check_file_exists(char *filename) {

	int err_code = 0;

	if (strlen(filename) == 0) {
		return (CL_EINVAL);
	}

	err_code = access(filename, R_OK);

	if (err_code == 0) {
		return (CL_NOERR);
	} else {
		return (CL_ENOENT);
	}
}


/*
 * This is the start_routine() method for the pthread_create() function
 * to execute a command in a new thread
 */

static void *start_command_exec(void *input) {

	FILE *file = NULL;
	clerrno_t err_status = CL_NOERR;
	char *cmd = (char *)input;

	err_status = exec_and_pipe_cmd(&file, cmd);

	if (err_status != CL_NOERR) {
		clerror("There was an error while executing the "
			"command %s", cmd);
	}

	(void) pclose(file);
	return (0);
}

/*
 * This is the start_routine() method for the pthread_create() function
 * to execute a command in a remote node
 */

static void *start_remote_command_exec(void *input) {

	FILE *file = NULL;
	clerrno_t err_status = CL_NOERR;
	cl_cmd_info_t *cmd_info = NULL;
	scconf_nodeid_t local_nodeid;
	scconf_nodeid_t nodeid;
	boolean_t current_node = B_FALSE;
	char *scrcmd_str, *tmp_str;
	int cmd_str_len, cmd_err_status = 0;
	char cmd_out[1024];
	char *buf = NULL;

	cmd_info = (cl_cmd_info_t *)input;

	//
	// Check Whether the node on which the command is to be run
	// is a local node or remote node.
	//
	if (!cmd_info->nodename) {
		current_node = B_TRUE;
	}

	if (current_node == B_FALSE) {
		if (scconf_get_nodeid(NULL, &local_nodeid))
			return (0); // Call the error handler for internal error

		if (scconf_get_nodeid(cmd_info->nodename, &nodeid))
			return (0); // Call the error handler for internal error

		// If the nodeid is the same as the local node id, then
		// it is a local node
		if (local_nodeid == nodeid) {
			current_node = B_TRUE;
		}
	}

	// First form the scrcmd command
	if (current_node == B_FALSE) {
		cmd_str_len = strlen(SCRCMD) + 4; // for scrcmd & -N
		cmd_str_len += strlen(cmd_info->nodename) + 1;
		cmd_str_len += strlen(cmd_info->cmd_str) + 1; // Added for NULL

		scrcmd_str = (char *)calloc(1, cmd_str_len);
		strcat(scrcmd_str, SCRCMD);
		strcat(scrcmd_str, " -N ");
		strcat(scrcmd_str, cmd_info->nodename);
		strcat(scrcmd_str, " ");
		strcat(scrcmd_str, cmd_info->cmd_str);
	} else {
		cmd_str_len = strlen(SCRCMD) + 4; // for scrcmd & -S
		cmd_str_len += strlen(cmd_info->cmd_str) + 1; // Added for NULL
		scrcmd_str = (char *)calloc(1, cmd_str_len);
		strcat(scrcmd_str, SCRCMD);
		strcat(scrcmd_str, " -S ");
		strcat(scrcmd_str, cmd_info->cmd_str);
	}

	//
	// Check whether any file was specified where the output has
	// to be re-directed. If it was specified, then we have to
	// form the command to redirect the output.
	//
	if (cmd_info->redirect_output != 0) {
		// We have to redirect the command output.
		// The format we will use is
		// "\"<command> > <outfile> 2>1& /dev/console\""
		tmp_str = strdup(scrcmd_str);
		free(scrcmd_str);
		//
		// " > /dev/console" ==> 3 + length of the filename
		//
		cmd_str_len += 3 + strlen(DEV_CONSOLE);
		//
		// " 2>&1 \"" ==> 4 + 1 (space) + 1 for the closing quote
		//
		cmd_str_len += 5 + 1;
		//
		// String Terminator '\0'
		//
		cmd_str_len += 1;
		scrcmd_str = (char *)calloc(cmd_str_len, 1);
		strcat(scrcmd_str, tmp_str);
		strcat(scrcmd_str, " > ");
		strcat(scrcmd_str, DEV_CONSOLE);
		strcat(scrcmd_str, " 2>&1");

		//
		// Make sure we output the installation log on the respective
		// node console
		//
		strcat(scrcmd_str, "\"");
		free(tmp_str);
		tmp_str = NULL;
	} else {
		// We do not have to redirect the command output.
		// The format we will use is "<command> 2>&1"
		tmp_str = strdup(scrcmd_str);
		free(scrcmd_str);
		//
		// Adding '5' for the space after the filename and
		// the redirection of stderr to stdout part.
		// " 2>&1" ==> 5 + String Terminator
		//
		cmd_str_len += 5 + 1;
		scrcmd_str = (char *)calloc(cmd_str_len, 1);
		strcat(scrcmd_str, tmp_str);
		strcat(scrcmd_str, " 2>&1");
		free(tmp_str);
		tmp_str = NULL;
	}

	err_status = exec_and_pipe_cmd(&file, scrcmd_str);
	if (err_status != CL_NOERR) {
		clerror("There was an error while executing the "
			"command %s", scrcmd_str);
	} else {
		// clerror("Successfully executed cmd %s\n", scrcmd_str);
	}

	while (fgets(cmd_out, sizeof (cmd_out), file)) {
		if (buf == NULL) {
			buf = (char *)malloc(
				(strlen(cmd_out) + 1) * sizeof (char));
			if (buf != NULL) {
				(void) snprintf(buf,
				    strlen(cmd_out) + 1, "%s", cmd_out);
			} else {
				clcommand_perror(CL_ENOMEM);
			}
		} else {
			buf = (char *)realloc(buf,
				(strlen(buf) + strlen(cmd_out) + 1) *
				sizeof (char));
			if (buf != NULL) {
				(void) snprintf(buf,
				    strlen(buf) + strlen(cmd_out) + 1, "%s%s",
				    buf, cmd_out);
			} else {
				clcommand_perror(CL_ENOMEM);
			}
		}
	}

	cmd_err_status = pclose(file);

	// If the command execution was a failure
	// then invoke the error handler
	if (WEXITSTATUS(cmd_err_status) != 0) {
		cmd_info->cmd_exit_status = WEXITSTATUS(cmd_err_status);
		if (buf != NULL) {
			cmd_info->command_error_str = strdup(buf);
		}
	}

	return (0);
}

#ifdef WEAK_MEMBERSHIP
/*
 * mark_ccr_as_winner
 *
 * This function checks the ccr contents of the the node and assign this copy
 * of ccr as valid copy. It returns 0 on success and non-zero on failure.
 *
 * Return Values :
 *	zero	    - ccr marked valid
 *	non-zero    - ccr could not get marked valid
 */
int
mark_ccr_as_winner() {
	return (ccrlib::mark_ccr_as_winner());
}

/*
 * mark_ccr_as_loser
 *
 * This function checks the ccr contents of the the node and assign this copy
 * of ccr as invalid copy. It returns 0 on success and non-zero on failure.
 *
 * Return Values :
 *	zero	    - ccr marked invalid
 *	non-zero    - ccr could not get marked invalid
 */
int
mark_ccr_as_loser() {
	return (ccrlib::mark_ccr_as_loser());
}

/*
 * split_brain_ccr_change_exists
 *
 * This function query the cluster and finds whether a split brain has occured
 * or not. If a split brain has occured then it returns "true" else
 * it returns "false".
 *
 * Return Values :
 *	true	- Split brain has occured in the cluster
 *	false	- There is no split brain in the cluster
 */
bool
split_brain_ccr_change_exists() {
	return (ccrlib::split_brain_ccr_change_exists());
}

/*
 * convert_to_lowercase
 *
 * This function converts the given value in to its lower case equivalent
 * and return that to the caller
 *
 * Returns a NULL if the given value is invalid
 */
void convert_to_lowercase(char *value, char **lowercase_value) {
	int length = 0;
	int i = 0;
	int j = 0;
	length = strlen(value);

	if (value == NULL ||
		length <= 0) {
		return;
	}

	*lowercase_value = (char *) malloc(sizeof (char) * (length + 1));

	for (i = 0; i < length; i++) {
		*(*(lowercase_value + 0) + i) = tolower(value[i]);
	}

	*(*(lowercase_value + 0) + i) = '\0';
}

//
// is_valid_ipaddress
//
// This method checks whether a given argument is a valid
// IP Address syntactically
//
// Input : 	ipaddress
//
// Return : true - If the given input is a valid Ip Address
//	    false - If the given input is not a valid
//			IP Address
//
bool
is_valid_ipaddress(char *ipaddress) {
	char buf[BUFSIZ];

	//
	// Check that the ipaddress is a valid '.' or ':'
	// separated address.
	// inet_pton() returns 1 if the conversion was successful
	// and 0 if the address was not parsable in the specified
	// address family
	//
	if ((inet_pton(AF_INET, ipaddress, buf) == 0) &&
		(inet_pton(AF_INET6, ipaddress, buf) == 0)) {
		return (false);
	}

	return (true);
}
#endif // WEAK_MEMBERSHIP
