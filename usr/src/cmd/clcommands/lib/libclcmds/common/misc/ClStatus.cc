/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *
 * ClStatus.cc
 */
#pragma ident	"@(#)ClStatus.cc	1.14	09/01/21 SMI"

#include "ClStatus.h"
#include "clcommands.h"

// Initialize all the values
ClStatus::ClStatus(bool v) :
    verbose(v),
    hasHeading(false),
    hasSection(false),
    alignStatus(true),
    rowGroups(false),
    statusColumn(0),
    heading(-1),
    section(-1),
    indent(0),
    numColumns(0)
{
	for (int i = 0; i < MAX_COLS; i++) {
		longest[i] = 0;
	}

	headings[0] = NULL;
	headings[1] = NULL;
}

ClStatus::~ClStatus()
{
	rows.clear();
	delete headings[0];
	delete headings[1];
}


// Keeps track of the longest column lengths
void
ClStatus::setLongest(Row *row)
{
	for (int i = 0; i < numColumns; i++) {
		int col_i = strlen(row->getValue(i));
		if (col_i > longest[i])
			longest[i] = col_i;
	}
}

// Add a row of up to 7 columns to the list of Row objects
void
ClStatus::addRow(
    char *v1,
    char *v2,
    char *v3,
    char *v4,
    char *v5,
    char *v6,
    char *v7)
{
	if (numColumns == 0) {
		numColumns = 1;
		if (v2 != NULL)
			numColumns = 2;
		if (v3 != NULL)
			numColumns = 3;
		if (v4 != NULL)
			numColumns = 4;
		if (v5 != NULL)
			numColumns = 5;
		if (v6 != NULL)
			numColumns = 6;
		if (v7 != NULL)
			numColumns = 7;
	}

	if (v1 == NULL)
		v1 = (char *)"";
	if (numColumns >= 2 && v2 == NULL)
		v2 = (char *)"";
	if (numColumns >= 3 && v3 == NULL)
		v3 = (char *)"";
	if (numColumns >= 4 && v4 == NULL)
		v4 = (char *)"";
	if (numColumns >= 5 && v5 == NULL)
		v5 = (char *)"";
	if (numColumns >= 6 && v6 == NULL)
		v6 = (char *)"";
	if (numColumns >= 7 && v7 == NULL)
		v7 = (char *)"";

	Row *a_row = new Row(v1, v2, v3, v4, v5, v6, v7);
	setLongest(a_row);
	rows.push_back(a_row);
}

// Set the headings of up to 7 columns.
void
ClStatus::setHeadings(
    char *h1,
    char *h2,
    char *h3,
    char *h4,
    char *h5,
    char *h6,
    char *h7)
{
	if (numColumns == 0) {
		numColumns = 1;
		if (h2 != NULL)
			numColumns = 2;
		if (h3 != NULL)
			numColumns = 3;
		if (h4 != NULL)
			numColumns = 4;
		if (h5 != NULL)
			numColumns = 5;
		if (h6 != NULL)
			numColumns = 6;
		if (h7 != NULL)
			numColumns = 7;
	}

	headings[0] = new Row(h1, h2, h3, h4, h5, h6, h7);
	setLongest(headings[0]);
	char temp[MAX_COLS][100];
	for (int i = 0; i < MAX_COLS; i++) {
		if (headings[0]->getValue(i) == NULL)
			temp[i][0] = '\0';
		else {
			memset(temp[i], '-', strlen(headings[0]->getValue(i)));
			temp[i][strlen(headings[0]->getValue(i))] = '\0';
		}
	}
	headings[1] = new Row(
	    temp[0],
	    temp[1],
	    temp[2],
	    temp[3],
	    temp[4],
	    temp[5],
	    temp[6]);
}


// Set the status column number
void
ClStatus::setStatusColumn(int col)
{
	statusColumn = col;
}

// Disble alignment of the status column at a fixed width
void
ClStatus::disableStatusAligning(bool value)
{
	alignStatus = !value;
}

// Enable verbose mode printing
void
ClStatus::enableVerbose(bool value)
{
	verbose = value;
}

// Set the heading title
void
ClStatus::setHeadingTitle(status_heading_t value)
{
	heading = (int)value;
	hasHeading = true;
}

// Set the section title
void
ClStatus::setSectionTitle(section_t value)
{
	section = (int)value;
	hasSection = true;
}

// Set the initial number of spaces to indent the columns
void
ClStatus::setInitialIndent(int value)
{
	indent = value;
}

// Enable grouping of like rows
void
ClStatus::enableRowGrouping(bool value)
{
	rowGroups = value;
}

// Return the heading title, or an empty string
char *
ClStatus::getHeadingTitle()
{
	char r_value[1024];

	switch (heading) {

	case HEAD_NODE:
		sprintf(r_value, gettext("=== Cluster Nodes ==="));
		break;

	case HEAD_FNODE:
		sprintf(r_value, gettext("=== Farm Nodes ==="));
		break;

	case HEAD_TRANSPORT:
		sprintf(r_value, gettext("=== Cluster Transport Paths ==="));
		break;

	case HEAD_QUORUM:
		sprintf(r_value, gettext("=== Cluster Quorum ==="));
		break;

	case HEAD_DEVICEGROUP:
		sprintf(r_value, gettext("=== Cluster Device Groups ==="));
		break;

	case HEAD_RESOURCEGROUP:
		sprintf(r_value, gettext("=== Cluster Resource Groups ==="));
		break;

	case HEAD_RESOURCE:
		sprintf(r_value, gettext("=== Cluster Resources ==="));
		break;

	case HEAD_DEVICE:
		sprintf(r_value, gettext("=== Cluster DID Devices ==="));
		break;

	case HEAD_TELEATTRIBUTE:
		sprintf(r_value, gettext("=== Telemetry Attributes ==="));
		break;

	case HEAD_ZC:
		sprintf(r_value, gettext("=== Zone Clusters ==="));
		break;

	case HEAD_ZC_NODE:
		sprintf(r_value, gettext("=== Zone Cluster Nodes ==="));
		break;

	default:
		sprintf(r_value, "");
		break;
	}

	return (r_value);
}

// Return the section title, or an empty string.
char *
ClStatus::getSectionTitle()
{
	char r_value[1024];

	switch (section) {

	case SECTION_NODES:
		sprintf(r_value, gettext("--- Node Status ---"));
		break;

	case SECTION_IPMP_NODE:
		sprintf(r_value, gettext("--- Node IPMP Group Status ---"));
		break;

	case SECTION_IPMP_ZONE:
		sprintf(r_value, gettext("--- Zone IPMP Group Status ---"));
		break;

	case SECTION_QUORUM_SUMMARY:
		sprintf(r_value, gettext("--- Quorum Votes Summary from "
		    "(latest node reconfiguration) ---"));
		break;

	case SECTION_QUORUM_NODES:
		sprintf(r_value, gettext(
		    "--- Quorum Votes by Node (current status) ---"));
		break;

#ifdef WEAK_MEMBERSHIP
	case SECTION_GLOBAL_QUORUM:
		sprintf(r_value, gettext(
		    "--- Global Quorum Health Check (current status) ---"));
		break;
#endif
	case SECTION_QUORUM_DEVICES:
		sprintf(r_value, gettext(
		    "--- Quorum Votes by Device (current status) ---"));
		break;

	case SECTION_DEVICEGROUPS:
		sprintf(r_value, gettext("--- Device Group Status ---"));
		break;

	case SECTION_DG_S_I_ITN:
		sprintf(r_value, gettext(
		    "--- Spare, Inactive, and In Transition Nodes ---"));
		break;

	case SECTION_DG_MULTI_SVM:
		sprintf(r_value,
		    gettext("--- Multi-owner Device Group Status ---"));
		break;

	case SECTION_ZC_NODES:
		sprintf(r_value,
		    gettext("--- Zone Cluster Status ---"));
		break;

	default:
		sprintf(r_value, "");
		break;

	}

	return (r_value);
}

// Print the data
void
ClStatus::print()
{
	// if nothing has been set yet, don't bother
	if (numColumns == 0)
		return;

	// Set status column based on verbose mode
	int status_width = (verbose) ? V_STATUS_COL : STATUS_COL;

	// If status column number not set, assume last column
	if (statusColumn == 0)
		statusColumn = numColumns - 1;

	// Determine the column widths
	int longest_temp[MAX_COLS];
	for (int i = 0; i < numColumns - 1; i++) {
		longest_temp[i] = longest[i] + MIN_SPACE;
	}
	longest_temp[numColumns - 1] = 0;

	if (alignStatus) {
		int num_chars = 0;
		for (int i = 0; i < statusColumn; i++) {
			num_chars += longest_temp[i];
		}

		// If the number of characters are less than the set status
		// width, then we can set their positions and, otherwise we
		// just leave it.
		if (num_chars < status_width) {
			int diff = status_width - num_chars;
			for (int i = diff; i > 0; ) {
				for (int j = 0; j < statusColumn; j++) {
					longest_temp[j]++;
					if (--i == 0)
						break;
				}
			}
		}
	}

	printf("\n");

	// Print the heading title
	if (hasHeading) {
		printf("%s\n\n", getHeadingTitle());
	}

	// Print the section title
	if (hasSection) {
		printf("%s\n\n", getSectionTitle());
	}

	// Print the headings
	switch (numColumns) {

	case 2:
		printf("%*s%-*s %s\n",
		    indent,
		    "",
		    longest_temp[0] - 1,
		    headings[0]->getValue(0),
		    headings[0]->getValue(1));
		printf("%*s%-*s %s",
		    indent,
		    "",
		    longest_temp[0] - 1,
		    headings[1]->getValue(0),
		    headings[1]->getValue(1));
		break;
	case 3:
		printf("%*s%-*s %-*s %s\n",
		    indent,
		    "",
		    longest_temp[0] - 1,
		    headings[0]->getValue(0),
		    longest_temp[1] - 1,
		    headings[0]->getValue(1),
		    headings[0]->getValue(2));
		printf("%*s%-*s %-*s %s",
		    indent,
		    "",
		    longest_temp[0] - 1,
		    headings[1]->getValue(0),
		    longest_temp[1] - 1,
		    headings[1]->getValue(1),
		    headings[1]->getValue(2));
		break;
	case 4:
		printf("%*s%-*s %-*s %-*s %s\n",
		    indent,
		    "",
		    longest_temp[0] - 1,
		    headings[0]->getValue(0),
		    longest_temp[1] - 1,
		    headings[0]->getValue(1),
		    longest_temp[2] - 1,
		    headings[0]->getValue(2),
		    headings[0]->getValue(3));
		printf("%*s%-*s %-*s %-*s %s",
		    indent,
		    "",
		    longest_temp[0] - 1,
		    headings[1]->getValue(0),
		    longest_temp[1] - 1,
		    headings[1]->getValue(1),
		    longest_temp[2] - 1,
		    headings[1]->getValue(2),
		    headings[1]->getValue(3));
		break;
	case 5:
		printf("%*s%-*s %-*s %-*s %-*s %s\n",
		    indent,
		    "",
		    longest_temp[0] - 1,
		    headings[0]->getValue(0),
		    longest_temp[1] - 1,
		    headings[0]->getValue(1),
		    longest_temp[2] - 1,
		    headings[0]->getValue(2),
		    longest_temp[3] - 1,
		    headings[0]->getValue(3),
		    headings[0]->getValue(4));
		printf("%*s%-*s %-*s %-*s %-*s %s",
		    indent,
		    "",
		    longest_temp[0] - 1,
		    headings[1]->getValue(0),
		    longest_temp[1] - 1,
		    headings[1]->getValue(1),
		    longest_temp[2] - 1,
		    headings[1]->getValue(2),
		    longest_temp[3] - 1,
		    headings[1]->getValue(3),
		    headings[1]->getValue(4));
		break;
	case 6:
		printf("%*s%-*s %-*s %-*s %-*s %-*s %s\n",
		    indent,
		    "",
		    longest_temp[0] - 1,
		    headings[0]->getValue(0),
		    longest_temp[1] - 1,
		    headings[0]->getValue(1),
		    longest_temp[2] - 1,
		    headings[0]->getValue(2),
		    longest_temp[3] - 1,
		    headings[0]->getValue(3),
		    longest_temp[4] - 1,
		    headings[0]->getValue(4),
		    headings[0]->getValue(5));
		printf("%*s%-*s %-*s %-*s %-*s %-*s %s",
		    indent,
		    "",
		    longest_temp[0] - 1,
		    headings[1]->getValue(0),
		    longest_temp[1] - 1,
		    headings[1]->getValue(1),
		    longest_temp[2] - 1,
		    headings[1]->getValue(2),
		    longest_temp[3] - 1,
		    headings[1]->getValue(3),
		    longest_temp[4] - 1,
		    headings[1]->getValue(4),
		    headings[1]->getValue(5));
		break;
	case 7:
		printf("%*s%-*s %-*s %-*s %-*s %-*s %-*s %s\n",
		    indent,
		    "",
		    longest_temp[0] - 1,
		    headings[0]->getValue(0),
		    longest_temp[1] - 1,
		    headings[0]->getValue(1),
		    longest_temp[2] - 1,
		    headings[0]->getValue(2),
		    longest_temp[3] - 1,
		    headings[0]->getValue(3),
		    longest_temp[4] - 1,
		    headings[0]->getValue(4),
		    longest_temp[5] - 1,
		    headings[0]->getValue(5),
		    headings[0]->getValue(6));
		printf("%*s%-*s %-*s %-*s %-*s %-*s %-*s %s",
		    indent,
		    "",
		    longest_temp[0] - 1,
		    headings[1]->getValue(0),
		    longest_temp[1] - 1,
		    headings[1]->getValue(1),
		    longest_temp[2] - 1,
		    headings[1]->getValue(2),
		    longest_temp[3] - 1,
		    headings[1]->getValue(3),
		    longest_temp[4] - 1,
		    headings[1]->getValue(4),
		    longest_temp[5] - 1,
		    headings[1]->getValue(5),
		    headings[1]->getValue(6));
		break;

	default:
		break;
	}

	// Add an extra new-line if row grouping isn't enabled
	// or the first row will be misaligned.
	if (!rowGroups)
		printf("\n");

	bool grouped = false;
	for (rowStart();
	    !rowEnd();
	    rowNext()) {

		// Handle row grouping
		if (rowGroups) {
			if (strcmp(rowCurrent()->getValue(0), "") != 0 &&
			    !grouped)
				// singleton
				printf("\n");
			else if (strcmp(rowCurrent()->getValue(0), "") == 0)
				grouped = true;
			else if (grouped) {
				// end of grouping
				printf("\n");
				grouped = false;
			}
		}

		switch (numColumns) {

		case 2:
			printf("%*s%-*s %s\n",
			    indent,
			    "",
			    longest_temp[0] - 1,
			    rowCurrent()->getValue(0),
			    rowCurrent()->getValue(1));
			break;
		case 3:
			printf("%*s%-*s %-*s %s\n",
			    indent,
			    "",
			    longest_temp[0] - 1,
			    rowCurrent()->getValue(0),
			    longest_temp[1] - 1,
			    rowCurrent()->getValue(1),
			    rowCurrent()->getValue(2));
			break;
		case 4:
			printf("%*s%-*s %-*s %-*s %s\n",
			    indent,
			    "",
			    longest_temp[0] - 1,
			    rowCurrent()->getValue(0),
			    longest_temp[1] - 1,
			    rowCurrent()->getValue(1),
			    longest_temp[2] - 1,
			    rowCurrent()->getValue(2),
			    rowCurrent()->getValue(3));
			break;
		case 5:
			printf("%*s%-*s %-*s %-*s %-*s %s\n",
			    indent,
			    "",
			    longest_temp[0] - 1,
			    rowCurrent()->getValue(0),
			    longest_temp[1] - 1,
			    rowCurrent()->getValue(1),
			    longest_temp[2] - 1,
			    rowCurrent()->getValue(2),
			    longest_temp[3] - 1,
			    rowCurrent()->getValue(3),
			    rowCurrent()->getValue(4));
			break;
		case 6:
			printf("%*s%-*s %-*s %-*s %-*s %-*s %s\n",
			    indent,
			    "",
			    longest_temp[0] - 1,
			    rowCurrent()->getValue(0),
			    longest_temp[1] - 1,
			    rowCurrent()->getValue(1),
			    longest_temp[2] - 1,
			    rowCurrent()->getValue(2),
			    longest_temp[3] - 1,
			    rowCurrent()->getValue(3),
			    longest_temp[4] - 1,
			    rowCurrent()->getValue(4),
			    rowCurrent()->getValue(5));
			break;
		case 7:
			printf("%*s%-*s %-*s %-*s %-*s %-*s %-*s %s\n",
			    indent,
			    "",
			    longest_temp[0] - 1,
			    rowCurrent()->getValue(0),
			    longest_temp[1] - 1,
			    rowCurrent()->getValue(1),
			    longest_temp[2] - 1,
			    rowCurrent()->getValue(2),
			    longest_temp[3] - 1,
			    rowCurrent()->getValue(3),
			    longest_temp[4] - 1,
			    rowCurrent()->getValue(4),
			    longest_temp[5] - 1,
			    rowCurrent()->getValue(5),
			    rowCurrent()->getValue(6));
		default:
			break;
		}
	}

	printf("\n");
}

// initialize the iterator to the beginning of the list
void
ClStatus::rowStart()
{
	iter = rows.begin();
}

// Get the current row value
Row *
ClStatus::rowCurrent()
{
	return ((*iter));
}

// Go to the next value in the list
void
ClStatus::rowNext()
{
	if (iter != rows.end())
		++iter;
}

// Returns 1 if the end of the list has been reached
int
ClStatus::rowEnd()
{
	return (iter == rows.end() ? 1 : 0);
}
