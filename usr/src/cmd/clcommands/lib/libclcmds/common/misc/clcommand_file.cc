//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clcommand_file.cc	1.2	08/05/20 SMI"

//
// Wrapper for file related processing for "cl" commands
//

#include "clcommands.h"
#include "ClCommand.h"

// clcommand_fopen
//	Wraper for fopen(w), using the original user id instead
//	of root.
//
FILE *
clcommand_fopen(const char *filename, const char *mode)
{
	FILE *open_fp = NULL;
	uid_t current_uid, original_uid;
	gid_t current_gid, original_gid;
	int saved_errno = 0;

	// Check input
	if (!filename || !mode) {
		return (NULL);
	}

	// Get the current uid and gid
	current_uid = getuid();
	current_gid = getgid();

	// Get the original uid and gid
	original_uid = ClCommand::getOriginaluid();
	original_gid = ClCommand::getOriginalgid();

	// Check the permission as the initial user
	if ((seteuid(original_uid) < 0) ||
	    (setegid(original_gid) < 0)) {
		return (NULL);
	}

	// Open the file
	open_fp = fopen(filename, mode);
	saved_errno = errno;

	// Restore to the current user ID and group ID
	if ((setuid(current_uid) < 0) ||
	    (setgid(current_gid) < 0)) {
		if (saved_errno == 0) {
			saved_errno = errno;
		}
	}

	// Restore the errno
	errno = saved_errno;

	return (open_fp);
}
