//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident   "@(#)ClCommand.cc	1.4	08/05/28 SMI"

//
// This is the ClCommand class.
//

#include "clcommands.h"

#include "ClCommand.h"

uid_t ClCommand::original_uid;
gid_t ClCommand::original_gid;

static void sortSubcommandList(struct clsubcommand &subcommands);
static int compareSubCommands(const void *ps1, const void *ps2);
static void sortOptionsLists(struct clsubcommand &subcommands);
static int compareOptions(const void *pc1, const void *pc2);

ClCommand::ClCommand()
{
	init_to_zero();
}

ClCommand::ClCommand(int argc_in, char *argv_in[],
    struct clcommandinit &init)
{
	init_to_zero();

	initCommand(argc_in, argv_in, init);
}

ClCommand::~ClCommand()
{
}

// Initialize
void
ClCommand::initCommand(int argc_in, char *argv_in[],
    struct clcommandinit &init)
{
	// Command name
	if ((commandname = strrchr(argv_in[0], '/')) == NULL) {
		commandname = argv_in[0];
	} else {
		++commandname;
	}

	// Command arguments
	argc = argc_in;
	argv = argv_in;

	// Command type data
	command_properties = &init;

	// Sort the list of subcommands
	sortSubcommandList(*command_properties->command_subcommands);

	// Sort all of the option lists for all of the subcommands
	sortOptionsLists(*command_properties->command_subcommands);

	// Store the original uid and gid.
	original_uid = getuid();
	original_gid = getgid();
}

// Get the command name
char *
ClCommand::getCommandName()
{
	return (commandname);
}

// Get the version of this command
char *
ClCommand::getCommandVersion()
{
	if (command_properties && command_properties->command_version) {
		return (command_properties->command_version);
	} else {
		return (CLCOMMANDS_COMMON_VERSION);
	}
}

// Get command description
char *
ClCommand::getCommandDescription()
{
	if (command_properties) {
		return (command_properties->command_description);
	} else {
		return ((char *)0);
	}
}

// Get usage lines
char **
ClCommand::getCommandUsageLines()
{
	if (command_properties) {
		return (command_properties->command_usage);
	} else {
		return ((char **)0);
	}
}

// Initialize subcommand properties
clerrno_t
ClCommand::initSubCommand()
{
	char *subcommand_name = 0;
	int namelen;
	clsubcommand_t *subcommands;

	// We should never be here, if there is no subcommand.
	if ((argc < 2) || (*argv[1] == '-')) {
		clerror("Unexpected missing subcommand.\n");
		return (CL_EINVAL);
	}

	// The subcommand is always the first argument.
	subcommand_name = argv[1];
	namelen = strlen(subcommand_name);

	// Make sure subcommands are allowed with this command.
	if ((subcommands = this->getListOfSubcommands()) == 0) {
		clerror("Unrecognized subcommand - \"%s\".\n", subcommand_name);
		clcommand_usage(*this);
		return (CL_EINVAL);
	}

	// Find our subcommand in the table.
	subcommand_properties = 0;
	while (subcommands->subcommand_name) {

		// Look for an exact match
		if (strcmp(subcommand_name,
		    subcommands->subcommand_name) == 0) {
			subcommand_properties = subcommands;
			break;
		}

		// Next
		++subcommands;
	}

	// Make sure we found our subcommand.
	if (!subcommand_properties) {
		clerror("Unrecognized subcommand - \"%s\".\n", subcommand_name);
		clcommand_usage(*this);
		return (CL_EINVAL);
	}

	return (CL_NOERR);
}

// Get the list of available subcommands.
clsubcommand_t *
ClCommand::getListOfSubcommands()
{
	if (command_properties) {
		return (command_properties->command_subcommands);
	} else {
		return ((clsubcommand_t *)0);
	}
}

// Get the subcommand name
char *
ClCommand::getSubCommandName()
{
	if (subcommand_properties) {
		return (subcommand_properties->subcommand_name);
	} else {
		return ((char *)0);
	}
}

// Get subcommand flags
unsigned int
ClCommand::getSubCommandFlags()
{
	return (subcommand_properties->flags);
}

// Get the subcommand description
char *
ClCommand::getSubCommandDescription()
{
	if (subcommand_properties) {
		return (subcommand_properties->subcommand_description);
	} else {
		return ((char *)0);
	}
}

// Get RBAC authorization for the subcommand
char *
ClCommand::getSubCommandRbacAuthorization()
{
	if (subcommand_properties) {
		return (subcommand_properties->subcommand_rbac);
	} else {
		return ((char *)0);
	}
}

// Get usage lines for the subcommand
char **
ClCommand::getSubCommandUsageLines()
{
	if (subcommand_properties) {
		return (subcommand_properties->subcommand_usage);
	} else {
		return ((char **)0);
	}
}

// Get subcommand options
struct cloptions *
ClCommand::getSubCommandOptions()
{
	if (subcommand_properties) {
		return (subcommand_properties->subcommand_options);
	} else {
		return ((struct cloptions *)0);
	}
}

// Process the subcommand
clerrno_t
ClCommand::doSubCommand()
{
	clerrno_t status;
	if (subcommand_properties && subcommand_properties->dosub) {
		status = subcommand_properties->dosub(*this);
		return (status);
	} else {
		return (CL_EINTERNAL);
	}
}

// Make sure everything is initialized to zero
void
ClCommand::init_to_zero()
{
	commandname = 0;
	argc = 0;
	argv = 0;
	isClusterMode = 0;
	isVerbose = 0;
	command_properties = 0;
	subcommand_properties = 0;
}

// Get the original uid
uid_t
ClCommand::getOriginaluid()
{
	return (original_uid);
}

// Get the original gid
gid_t
ClCommand::getOriginalgid()
{
	return (original_gid);
}

// Sort the list of subcommands
static void
sortSubcommandList(struct clsubcommand &subcommands)
{
	struct clsubcommand *s;
	int count;

	count = 0;
	for (s = &subcommands;  s->subcommand_name;  ++s)
		++count;

	qsort((void *)&subcommands, count, sizeof (struct clsubcommand),
	    compareSubCommands);
}

// Compare subcommands for the sortSubCommandList
static int
compareSubCommands(const void *ps1, const void *ps2)
{
	struct clsubcommand *s1 = (struct clsubcommand *)ps1;
	struct clsubcommand *s2 = (struct clsubcommand *)ps2;

	if (!s1->subcommand_name || !s2->subcommand_name)
		return (0);

	return (strcmp(s1->subcommand_name, s2->subcommand_name));
}

// Sort the lists of options for all subcommands
static void
sortOptionsLists(struct clsubcommand &subcommands)
{
	struct clsubcommand *s;
	struct cloptions *o;
	int count;

	for (s = &subcommands;  s->subcommand_name;  ++s) {
		if ((o = s->subcommand_options) == 0)
			continue;
		count = 0;
		while (o->shortoption) {
			++count;
			++o;
		}

		qsort((void *)s->subcommand_options, count,
		    sizeof (struct cloptions), compareOptions);
	}
}

// Compare options for sortOptionsLists
static int
compareOptions(const void *po1, const void *po2)
{
	struct cloptions *o1 = (struct cloptions *)po1;
	struct cloptions *o2 = (struct cloptions *)po2;
	int l1;
	int l2;

	// If they are equal, do nothing
	if (o1->shortoption == o2->shortoption)
		return (0);

	// Convert to lower case
	l1 = tolower(o1->shortoption);
	l2 = tolower(o2->shortoption);

	// If they are the same as lower case, uppercase option sorts first
	if (l1 == l2) {
		return (isupper(o1->shortoption) ? -1 : 1);
	}

	// Otherwise, sort as though everything is in the same case
	return ((l1 > l2) ? 1 : -1);
}
