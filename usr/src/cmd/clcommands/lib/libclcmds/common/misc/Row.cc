/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)Row.cc	1.3	08/05/20 SMI"
 *
 * ClList.cc
 */

#include "Row.h"

/*
 * Initialize the values[] with the string values
 * which are passed in.
 */
Row::Row(
    char *one,
    char *two,
    char *three,
    char *four,
    char *five,
    char *six,
    char *seven)
{
	values[0] = new char[strlen(one) + 1];
	(void) strcpy(values[0], one);

	if (two != NULL) {
		values[1] = new char[strlen(two) + 1];
		(void) strcpy(values[1], two);
	} else
		values[1] = NULL;

	if (three != NULL) {
		values[2] = new char[strlen(three) + 1];
		(void) strcpy(values[2], three);
	} else
		values[2] = NULL;

	if (four != NULL) {
		values[3] = new char[strlen(four) + 1];
		(void) strcpy(values[3], four);
	} else
		values[3] = NULL;

	if (five != NULL) {
		values[4] = new char[strlen(five) + 1];
		(void) strcpy(values[4], five);
	} else
		values[4] = NULL;

	if (six != NULL) {
		values[5] = new char[strlen(six) + 1];
		(void) strcpy(values[5], six);
	} else
		values[5] = NULL;

	if (seven != NULL) {
		values[6] = new char[strlen(seven) + 1];
		(void) strcpy(values[6], seven);
	} else
		values[6] = NULL;
}

/*
 * Deconstructor
 */
Row::~Row()
{
	for (int i = 0; i < MAX_COLS; i++) {
		if (values[i]) {
			delete [] values[i];
		}
		values[i] = NULL;
	}
}

/*
 * Return the value associated with the integer num
 */
char *
Row::getValue(int num)
{
	return (values[num]);
}
