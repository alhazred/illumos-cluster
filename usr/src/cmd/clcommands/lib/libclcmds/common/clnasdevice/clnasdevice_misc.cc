//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clnasdevice_misc.cc	1.3	08/05/20 SMI"

//
// Miscellaneous functions for clnasdevice
//

#include "clnasdevice.h"

//
// clnasdevice_prop_convert
//
//	Convert the Name-Value pair of NameValueList into
//	scnas_filer_prop_t. Caller needs to free the space
//	allocated for scnas_filer_prop_t.
//
clerrno_t
clnasdevice_prop_convert(NameValueList &properties,
    scnas_filer_prop_t **scnas_props)
{
	clerrno_t clerrno = CL_NOERR;
	scnas_filer_prop_t *prop;

	if (scnas_props == NULL) {
		return (CL_EINTERNAL);
	}

	if (properties.getSize() == 0) {
		return (CL_NOERR);
	}

	// Go through the properties
	for (properties.start(); properties.end() != 1;
	    properties.next()) {
		char *prop_name = properties.getName();
		char *prop_val = properties.getValue();

		if (!prop_name)
			continue;

		prop = (scnas_filer_prop_t *)calloc(1,
		    sizeof (scnas_filer_prop_t));
		if (prop == (scnas_filer_prop_t *)0) {
			clerrno = CL_ENOMEM;
			goto cleanup;
		}

		// Get the name
		if ((prop->scnas_prop_key = strdup(prop_name)) == NULL) {
			clerrno = CL_ENOMEM;
			goto cleanup;
		}

		// Get the value
		if (prop_val) {
			if ((prop->scnas_prop_value = strdup(prop_val)) ==
			    NULL) {
				clerrno = CL_ENOMEM;
				goto cleanup;
			}
		}

		prop->scnas_prop_next = *scnas_props;
		*scnas_props = prop;
	}

	return (clerrno);

cleanup:
	// Free the property
	if (prop) {
		if (prop->scnas_prop_key)
			free(prop->scnas_prop_key);
		if (prop->scnas_prop_value)
			free(prop->scnas_prop_value);
		free(prop);
	}

	return (clerrno);
}

//
// clnasdevice_perror
//
//	Print a clnasdevice error from the CL error number.
//
void
clnasdevice_perror(clerrno_t clerrno)
{
	switch (clerrno) {
	case CL_EEXIST:
		clerror("NAS device already exists.\n");
		return;

	case CL_ENOENT:
		clerror("NAS device does not exist.\n");
		return;

	case CL_ETYPE:
		clcommand_perror(clerrno);
		return;

	case CL_ENOMEM:
		clcommand_perror(clerrno);
		return;

	case CL_EACCESS:
		clerror("Cannot access the NAS device.\n");
		return;

	case CL_EPROP:
		clerror("Invalid NAS property.\n");
		return;

	case CL_EOP:
		clerror("Cannot find NAS device support software.\n");
		return;

	case CL_ERECONF:
		clcommand_perror(clerrno);
		return;

	case CL_EINTERNAL:
		clcommand_perror(clerrno);
		return;
	}
}

//
// clnasdevice_conv_scnas_err
//
//	Convert scnas_errno_t to CL error.
//
clerrno_t
clnasdevice_conv_scnas_err(scnas_errno_t err)
{
	// Map the error
	switch (err) {
	case SCNAS_NOERR:
		return (CL_NOERR);

	case SCNAS_EEXIST:
		return (CL_EEXIST);

	case SCNAS_ENOEXIST:
		return (CL_ENOENT);

	case SCNAS_EUNKNOWN:
		return (CL_ETYPE);

	case SCNAS_EDIREXIST:
		return (CL_EEXIST);

	case SCNAS_ENOMEM:
		return (CL_ENOMEM);

	case SCNAS_EAUTH:
		return (CL_EACCESS);

	case SCNAS_EINVALPROP:
		return (CL_EPROP);

	case SCNAS_NO_BINARY:
		return (CL_EOP);

	case SCNAS_ERECONFIG:
		return (CL_ERECONF);

	// These errors are not used so simply ignore them
	case SCNAS_ENOCLUSTER:
	case SCNAS_EPERM:
	case SCNAS_ENODEID:
	case SCNAS_EINVAL:
	case SCNAS_EUSAGE:
	case SCNAS_EUNEXPECTED:
	default:
		return (CL_EINTERNAL);
	}
}

//
// clnasdevice_preprocess_naslist
//
//	Find the NAS devices and its type in CCR, and save them into
//	a NameValueList with <name, type> pair. These NAS list must
//	match the input NAS name in nasdevices, and the type in
//	nastypes. If the print_err flag is set, print error messages
//	for the invalid ones.
//
clerrno_t
clnasdevice_preprocess_naslist(ValueList &nasdevices,
    ValueList &nastypes, NameValueList &nas_list, bool print_err)
{
	clerrno_t clerrno = CL_NOERR;
	scnas_errno_t scnas_err = SCNAS_NOERR;
	cl_query_table_t index_table;
	cl_query_table_t filer_table;
	char table_name[SCNAS_MAXSTRINGLEN];
	uint_t i, j;
	int found;
	clerrno_t first_err = CL_NOERR;
	char *this_val;
	char *nas_name = (char *)0;
	char *nas_type = (char *)0;
	ValueList valid_nas = ValueList(true);
	ValueList valid_type = ValueList(true);

	// Initialize the ccr table structure
	index_table.n_rows = 0;
	filer_table.n_rows = 0;

	// Read the NAS index table
	scnas_err = scnas_read_filer_table(NAS_SERVICE_KEY_TABLE,
	    NULL, NULL, &index_table, GET_TABLE_CONTENTS);
	if ((scnas_err != SCNAS_ENOEXIST) && (scnas_err != SCNAS_NOERR)) {
		// Not an error if NAS is not configured
		clerrno = clnasdevice_conv_scnas_err(scnas_err);
		return (clerrno);
	}

	// Check the user input types
	for (nastypes.start(); !nastypes.end(); nastypes.next()) {
		this_val = nastypes.getValue();

		// Is the type valid?
		if (!scnas_check_nas_devicetype(this_val, NULL)) {
			if (print_err) {
				clerror("Invalid type \"%s\".\n",
				    this_val);
			}
			if (first_err == CL_NOERR) {
				first_err = CL_ETYPE;
			}
		} else {
			valid_type.add(this_val);
		}
	}

	// Go through the NAS devices saved by the index
	for (i = 0; i < index_table.n_rows; i++) {
		if (!(index_table.table_rows[i]->key) ||
		    !(index_table.table_rows[i]->value))
			continue;

		// Get the NAS ccr table name
		(void) sprintf(table_name, "%s_%s",
		    NAS_SERVICE_TABLE_PRE, index_table.table_rows[i]->key);

		// Read the NAS ccr table
		scnas_err = scnas_read_filer_table(table_name, NULL, NULL,
		    &filer_table, GET_TABLE_CONTENTS);
		if (scnas_err != SCNAS_NOERR) {
			clerrno = clnasdevice_conv_scnas_err(scnas_err);
			if (first_err == CL_NOERR) {
				first_err = clerrno;
			}
			goto cleanup;
		}

		// Check the filer name in the table
		found = 0;
		for (j = 0; j < filer_table.n_rows; j++) {
			if (!(filer_table.table_rows[j]->key) ||
			    !(filer_table.table_rows[j]->value))
				continue;

			// Get the NAS device name
			if (strcmp(filer_table.table_rows[j]->key,
			    PROP_FILER_NAME) == 0) {
				nas_name =
				    strdup(filer_table.table_rows[j]->value);
				if (!nas_name) {
					clerrno = CL_ENOMEM;
					goto cleanup;
				}
				found++;
				if (found == 2)
					break;
			}

			// Get the NAS device type
			if (strcmp(filer_table.table_rows[j]->key,
			    PROP_FILER_TYPE) == 0) {
				nas_type =
				    strdup(filer_table.table_rows[j]->value);
				if (!nas_type) {
					clerrno = CL_ENOMEM;
					goto cleanup;
				}
				found++;
				if (found == 2)
					break;
			}
		}

		if (found < 2) {
			if (nas_name)
				free(nas_name);
			if (nas_type)
				free(nas_type);

			continue;
		}

		// Check the name and type
		if ((nastypes.getSize() == 0) || valid_type.isValue(nas_type)) {
			if ((nasdevices.getSize() == 0) ||
			    nasdevices.isValue(nas_name)) {
				(void) nas_list.add(nas_name, nas_type);
			}
		}
		if ((nastypes.getSize() != 0) &&
		    !valid_type.isValue(nas_type) &&
		    nasdevices.isValue(nas_name)) {
			if (print_err) {
				clerror("The type of NAS device "
				    "\"%s\" does not match the "
				    "specified type.\n", nas_name);
			}
			if (first_err == CL_NOERR) {
				first_err = CL_ETYPE;
			}
			valid_nas.add(nas_name);
		}

		// Free the memory
		free(nas_name);
		free(nas_type);

		// Free the NAS ccr table
		if (cl_query_free_table(&filer_table) != CL_QUERY_OK) {
			if (first_err == CL_NOERR) {
				first_err = CL_EINTERNAL;
			}
		}
	}

	// Check against the user input filer names
	for (nasdevices.start(); !nasdevices.end(); nasdevices.next()) {
		this_val = nasdevices.getValue();
		if (!this_val || valid_nas.isValue(this_val))
			continue;

		// Search for the valid nas list for this NAS
		found = 0;
		for (nas_list.start(); !nas_list.end(); nas_list.next()) {
			if (strcmp(nas_list.getName(), this_val) == 0) {
				found++;
				break;
			}
		}

		// Valid?
		if (!found) {
			if (print_err) {
				clerror("Device \"%s\" does not exist.\n",
				    this_val);
			}
			if (first_err == CL_NOERR) {
				first_err = CL_ENOENT;
			}
		}
	}

cleanup:
	// Free the index table
	if (cl_query_free_table(&index_table) != CL_QUERY_OK) {
		if (first_err == CL_NOERR) {
			first_err = CL_EINTERNAL;
		}
	}

	if (nas_name)	//lint !e644
		free(nas_name);
	if (nas_type)	//lint !e644
		free(nas_type);

	if (first_err != CL_NOERR) {
		clerrno = first_err;
	}

	return (clerrno);
}
