//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clnasdevice_add_dir.cc	1.4	08/05/20 SMI"

//
// Process clnasdevice "add-dir"
//

#include "clnasdevice.h"
#include "ClXml.h"

//
// clnasdevice "add-dir" without -i
//
clerrno_t
clnasdevice_add_dir(ClCommand &cmd, ValueList &operands, optflgs_t optflgs,
    NameValueList &directories)
{
	char *msgbuffer = (char *)0;
	scnas_errno_t scnas_err = SCNAS_NOERR;
	scnas_filer_prop_t *proplist = (scnas_filer_prop_t *)0;
	clerrno_t clerrno = CL_NOERR;
	char *nas_name;

	// Directories specified?
	if (directories.getSize() == 0) {
		return (CL_EINVAL);
	}

	// Get the properties
	clerrno = clnasdevice_prop_convert(directories, &proplist);
	if (clerrno != SCNAS_NOERR) {
		clcommand_perror(clerrno);
		goto cleanup;
	}

	// Add the directories
	operands.start();
	nas_name = operands.getValue();
	scnas_err = scnasdir_set_filer_prop(nas_name,
	    proplist, &msgbuffer, NULL, SCNASDIR_CMD_ADD);
	if (scnas_err != SCNAS_NOERR) {
		// Print detailed messages
		if (msgbuffer != (char *)0) {
			clcommand_dumpmessages(msgbuffer);
		}

		switch (scnas_err) {
		case SCNAS_ENOEXIST:
			clerror("NAS device \"%s\" does not exist.\n",
			    nas_name);
			clerrno = CL_ENOENT;
			break;

		// For this error, the msgbuffer has detailed messages.
		case SCNAS_EINVALPROP:
			clerrno = CL_EPROP;
			break;
		case SCNAS_EEXIST:
			clerrno = CL_EEXIST;
			break;

		default:
			clerrno = clnasdevice_conv_scnas_err(scnas_err);
			clnasdevice_perror(clerrno);
			break;

		} //lint !e788

		goto cleanup;
	} else if (optflgs & vflg) {
		clmessage("Directories are added to NAS device \"%s\".\n",
		    nas_name);
	}

cleanup:
	if (msgbuffer) {
		free(msgbuffer);
	}

	if (proplist != (scnas_filer_prop_t *)0) {
		scnas_free_proplist(proplist);
	}

	return (clerrno);
}

//
// clnasdevice add-dir with -i
//
clerrno_t
clnasdevice_add_dir(ClCommand &cmd, ValueList &operands, optflgs_t optflgs,
    char *clconfiguration, NameValueList &directories,
    NameValueList &properties)
{
	clerrno_t clerrno = CL_NOERR;
	clerrno_t first_err = CL_NOERR;
	char err_buf[SCCONF_MAXSTRINGLEN];

	// Check for inputs
	if (!clconfiguration) {
		clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}

	OptionValues *ov = new OptionValues((char *)"add-dir",
	    cmd.argc, cmd.argv);
	ClXml xml;

	// Add nas properties to OptionValues
	ov->addNVOptions(CLNAS_DIR, &directories);
	ov->addNVOptions(CLNAS_PROP, &properties);

	// verbose?
	if (optflgs & vflg) {
		ov->optflgs |= vflg;
	}

	// Set operands
	for (operands.start(); !operands.end(); operands.next()) {
		ov->setOperand(operands.getValue());
		clerrno = xml.applyConfig(CLNAS_CMD, clconfiguration, ov);
		if ((clerrno != CL_NOERR) && (first_err == CL_NOERR))  {
			first_err = clerrno;
		}
	}

	if (first_err != CL_NOERR) {
		clerrno = first_err;
	}

	return (clerrno);
}
