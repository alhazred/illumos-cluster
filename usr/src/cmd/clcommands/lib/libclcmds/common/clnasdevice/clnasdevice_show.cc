//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clnasdevice_show.cc	1.4	08/09/22 SMI"

//
// Process clnasdevice "show"
//

#include "clnasdevice.h"

// Properties to print in the "show" output
#define	CLNAS_NAME	gettext("Nas Device")
#define	CLNAS_TYPE	gettext("Type")
#define	CLNAS_USER	gettext("User ID")
#define	CLNAS_DIR	gettext("Directory")

static clerrno_t clnasdevice_get_show(ValueList &nasdevices,
    optflgs_t optflgs, ValueList &nastypes, ClShow *clnas_show);

//
// clnasdevice "show"
//
clerrno_t
clnasdevice_show(ClCommand &cmd, ValueList &operands, optflgs_t optflgs,
    ValueList &nastypes)
{
	clerrno_t clerrno = CL_NOERR;
	ClShow *clnas_show = new ClShow(HEAD_NASDEVICE);

	//
	// Get the ClShow object for the nas devices. Error messages are
	// printed in clnasdevice_get_show().
	//
	clerrno = clnasdevice_get_show(operands, optflgs, nastypes,
	    clnas_show);

	// Print it
	if ((clerrno == CL_NOERR) ||
	    (clnas_show->getObjectSize() != 0)) {
		(void) clnas_show->print();
	}

	// Delete the show object
	delete clnas_show;

	return (clerrno);
} //lint !e715

//
// get_clnasdevice_show_obj
//
//	Get the ClShow object for all the nas devices in the  system.
//	The ClShow object must be created before calling this function.
//
int
get_clnasdevice_show_obj(optflgs_t optflgs, ClShow *clnas_show)
{
	ValueList empty_list;

	// Check inputs
	if (!clnas_show) {
		clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}

	// Get all the nas devices into the ClShow object
	return (clnasdevice_get_show(empty_list, optflgs,
	    empty_list, clnas_show));
}
//
// clnasdevice_get_show
//
//	Get the nasdevice configuration data and saved into the ClShow
//	object for printing.
//
clerrno_t
clnasdevice_get_show(ValueList &nasdevices, optflgs_t optflgs,
    ValueList &nastypes, ClShow *clnas_show)
{
	clerrno_t clerrno = CL_NOERR;
	scnas_errno_t scnas_err = SCNAS_NOERR;
	NameValueList nas_list;
	int found;
	clerrno_t first_err = CL_NOERR;
	cl_query_table_t index_table;
	cl_query_table_t filer_table;
	char table_name[SCNAS_MAXSTRINGLEN];
	uint_t i, j;

	// Get the valid nas device list from the user input
	clerrno = clnasdevice_preprocess_naslist(nasdevices, nastypes,
	    nas_list, true);
	if (clerrno != CL_NOERR) {
		// Errors are processed in above function.
		if ((clerrno != CL_ETYPE) && (clerrno != CL_ENOENT)) {
			clcommand_perror(clerrno);
			return (clerrno);
		}
		if (first_err == CL_NOERR) {
			first_err = clerrno;
		}
	}

	// If nothing to print, done
	if (nas_list.getSize() == 0) {
		return (clerrno);
	}

	// Initialize the ccr table structure
	index_table.n_rows = 0;
	filer_table.n_rows = 0;

	// Read the NAS index table
	scnas_err = scnas_read_filer_table(NAS_SERVICE_KEY_TABLE,
	    NULL, NULL, &index_table, GET_TABLE_CONTENTS);
	if (scnas_err != SCNAS_NOERR) {
		clerrno = clnasdevice_conv_scnas_err(scnas_err);
		clnasdevice_perror(clerrno);
		if (first_err == CL_NOERR) {
			first_err = clerrno;
		}
		goto cleanup;
	}

	// Go through the NAS devices saved by the index
	for (i = 0; i < index_table.n_rows; i++) {
		if (!(index_table.table_rows[i]->key) ||
		    !(index_table.table_rows[i]->value))
			continue;

		// Check the type
		found = 0;
		for (nas_list.start(); !nas_list.end(); nas_list.next()) {
			if (strcmp(index_table.table_rows[i]->value,
			    nas_list.getValue()) == 0) {
				found = 1;
				break;
			}
		}
		if (!found)
			continue;

		// Get the NAS ccr table name
		(void) sprintf(table_name, "%s_%s",
		    NAS_SERVICE_TABLE_PRE, index_table.table_rows[i]->key);

		// Read the NAS ccr table
		scnas_err = scnas_read_filer_table(table_name, NULL, NULL,
		    &filer_table, GET_TABLE_CONTENTS);
		if (scnas_err != SCNAS_NOERR) {
			clerrno = clnasdevice_conv_scnas_err(scnas_err);
			clnasdevice_perror(clerrno);
			if (first_err == CL_NOERR) {
				first_err = clerrno;
			}
			goto cleanup;
		}

		// Check the filer name in the table
		found = 0;
		for (j = 0; j < filer_table.n_rows; j++) {
			if (!(filer_table.table_rows[j]->key) ||
			    !(filer_table.table_rows[j]->value))
				continue;

			// Get the NAS device name
			if (strcmp(filer_table.table_rows[j]->key,
			    PROP_FILER_NAME) != 0)
				continue;

			for (nas_list.start(); !nas_list.end();
			    nas_list.next()) {
				if (strcmp(filer_table.table_rows[j]->value,
				    nas_list.getName()) == 0) {
					found = 1;
					break;
				}
			}
			if (found)
				break;
		}
		if (!found) {
			if (cl_query_free_table(&filer_table) != CL_QUERY_OK) {
				clerrno = CL_EINTERNAL;
				clcommand_perror(clerrno);
				if (first_err == CL_NOERR) {
					first_err = clerrno;
				}
				goto cleanup;
			}

			continue;
		}

		// Get the configuration of this NAS device and print it.
		char *nas_device_name = filer_table.table_rows[j]->value;
		clnas_show->propSet = false;
		clnas_show->addObject((char *)CLNAS_NAME, nas_device_name);
		for (j = 0; j < filer_table.n_rows; j++) {
			if (!(filer_table.table_rows[j]->key) ||
			    !(filer_table.table_rows[j]->value))
				continue;

			// Get type
			if (strcmp(filer_table.table_rows[j]->key,
			    PROP_FILER_TYPE) == 0) {
				clnas_show->addProperty(
				    nas_device_name,
				    (char *)CLNAS_TYPE,
				    filer_table.table_rows[j]->value);
			}
			if (strcmp(filer_table.table_rows[j]->key,
			    PROP_USERID) == 0) {
				clnas_show->addProperty(nas_device_name,
				    (char *)CLNAS_USER,
				    filer_table.table_rows[j]->value);
			}
		}

		// Get the direcoties in verbose mode
		if (optflgs & vflg) {
			for (j = 0; j < filer_table.n_rows; j++) {
				if (!(filer_table.table_rows[j]->key) ||
				    !(filer_table.table_rows[j]->value))
					continue;

				// Get direcotires
				if (strstr(filer_table.table_rows[j]->key,
				    PROP_DIRECTORY)) {
					clnas_show->addProperty(nas_device_name,
					    (char *)CLNAS_DIR,
					    filer_table.table_rows[j]->value);
				}
			}
		}

		// Free the NAS ccr table
		if (cl_query_free_table(&filer_table) != CL_QUERY_OK) {
			clerrno = CL_EINTERNAL;
			clcommand_perror(clerrno);
			if (first_err == CL_NOERR) {
				first_err = clerrno;
			}
			goto cleanup;
		}
	}

cleanup:

	// Free the index table
	if (cl_query_free_table(&index_table) != CL_QUERY_OK) {
		if (first_err == CL_NOERR) {
			first_err = CL_EINTERNAL;
		}
	}

	if (cl_query_free_table(&filer_table) != CL_QUERY_OK) {
		if (first_err == CL_NOERR) {
			first_err = CL_EINTERNAL;
		}
	}

	if (first_err != CL_NOERR) {
		clerrno = first_err;
	}

	return (clerrno);
}
