//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clnasdevice_set.cc	1.3	08/05/20 SMI"

//
// Process clnasdevice "set"
//

#include "clnasdevice.h"

//
// clnasdevice "set"
//
clerrno_t
clnasdevice_set(ClCommand &cmd, ValueList &operands, optflgs_t optflgs,
    NameValueList &properties)
{
	char *msgbuffer = (char *)0;
	scnas_errno_t scnas_err = SCNAS_NOERR;
	clerrno_t clerrno = CL_NOERR;
	scnas_filer_prop_t *proplist = (scnas_filer_prop_t *)0;

	// Get the properties
	clerrno = clnasdevice_prop_convert(properties, &proplist);
	if (clerrno != CL_NOERR) {
		clcommand_perror(clerrno);
		goto cleanup;
	}

	// Change the device.
	operands.start();
	scnas_err = scnas_change_filer(operands.getValue(), proplist,
	    &msgbuffer);
	if (scnas_err != SCNAS_NOERR) {
		// Print detailed messages
		if (msgbuffer != (char *)0) {
			clcommand_dumpmessages(msgbuffer);
		}

		switch (scnas_err) {
		case SCNAS_ENOEXIST:
			clerror("NAS device \"%s\" does not exist.\n",
			    operands.getValue());
			clerrno = CL_ENOENT;
			break;

		case SCNAS_EAUTH:
			clerrno = CL_EINVAL;
			break;

		case SCNAS_EINVALPROP:
		case SCNAS_EUSAGE:
			clerrno = CL_EPROP;
			break;

		default:
			clerrno = clnasdevice_conv_scnas_err(scnas_err);
			clnasdevice_perror(clerrno);
			break;

		} //lint !e788

		goto cleanup;
	}

cleanup:
	if (msgbuffer) {
		free(msgbuffer);
	}

	if (proplist != (scnas_filer_prop_t *)0) {
		scnas_free_proplist(proplist);
	}

	return (clerrno);
} //lint !e715
