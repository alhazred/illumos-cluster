//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clnasdevice_remove.cc	1.2	08/05/20 SMI"

//
// Process clnasdevice "remove"
//

#include "clnasdevice.h"

//
// clnasdevice "remove"
//
clerrno_t
clnasdevice_remove(ClCommand &cmd, ValueList &operands, optflgs_t optflgs,
    ValueList &nastypes)
{
	clerrno_t clerrno = CL_NOERR;
	scnas_errno_t scnas_err = SCNAS_NOERR;
	NameValueList nas_list;
	clerrno_t first_err = CL_NOERR;
	char *this_val;

	// Get the valid nas device list from the user input
	clerrno = clnasdevice_preprocess_naslist(operands, nastypes,
	    nas_list, true);
	switch (clerrno) {
	case CL_NOERR:
		if (nas_list.getSize() == 0) {
			return (clerrno);
		}
		break;

	case CL_ENOENT:
	case CL_ETYPE:
		// Errors are printed in above function
		if (nas_list.getSize() == 0)
			return (clerrno);
		break;

	default:
		clcommand_perror(clerrno);
		return (clerrno);
	}

	// Remove the NAS devices
	for (nas_list.start(); !nas_list.end(); nas_list.next()) {
		char *msgbuffer = (char *)0;
		this_val = nas_list.getName();
		if (!this_val)
			continue;

		// Call the remove function
		scnas_err = scnas_remove_filer(this_val, NULL,
		    &msgbuffer, (optflgs & Fflg));
		if (scnas_err != SCNAS_NOERR) {
			// Print extra messages
			if (msgbuffer != (char *)0) {
				clcommand_dumpmessages(msgbuffer);
				free(msgbuffer);
			}

			switch (scnas_err) {
			case SCNAS_ENOEXIST:
				clerror("NAS device \"%s\" does not exist.\n",
				    this_val);
				clerrno = CL_ENOENT;
				goto cleanup;

			case SCNAS_EDIREXIST:
				clerror("NAS device \"%s\" still contains "
				    "directories.\n", this_val);
				clerrno = CL_EEXIST;
				break;

			default:
				clerrno = clnasdevice_conv_scnas_err(
				    scnas_err);
				clnasdevice_perror(clerrno);
				break;

			} //lint !e788

			if (first_err == CL_NOERR) {
				first_err = clerrno;
			}
		} else if (cmd.isVerbose) {
			clmessage("NAS device \"%s\" is removed.\n",
			    this_val);
		}
	}

cleanup:
	if (first_err != CL_NOERR) {
		clerrno = first_err;
	}

	return (clerrno);
}
