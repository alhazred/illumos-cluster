//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clnasdevice_export.cc	1.3	08/05/20 SMI"

//
// Process clnasdevice "export"
//

#include "clnasdevice.h"
#include "ClXml.h"

static clerrno_t
clnasdevice_get_nas_xml(NameValueList &nas_list,
    NameValueList &directories, ClnasExport *clnas_export);

//
// This function returns a ClnasExport object populated with
// all of the NAS device information from the cluster.  Caller is
// responsible for releasing the ClcmdExport object.
//
// NOTE TO DEVS:  You probably want to have this function call
// another of a similar fashion which takes options to filter
// the output based off of the command line args.  This function
// is required by the ClXml class in order to get all of the
// NAS device information without calling the command itself.
//
ClcmdExport *
get_clnas_xml_elements()
{
	ClnasExport *nas = new ClnasExport();
	NameValueList nas_list = NameValueList(true);

	// Add configuration of all the nas devices to ClnasExport object.
	(void) clnasdevice_get_nas_xml(nas_list, nas_list, nas);

	return (nas);
}

clerrno_t
clnasdevice_export(ClCommand &cmd, ValueList &operands, optflgs_t optflgs,
    char *clconfiguration,
    ValueList &nasdevicetypes, NameValueList &directories)
{
	ClXml clnas_clxml;
	ClnasExport *clnas_export = new ClnasExport();
	NameValueList nas_list = NameValueList(true);
	clerrno_t clerrno = CL_NOERR;
	clerrno_t first_err = CL_NOERR;
	char *export_file = (char *)0;

	// Check inputs
	if (!clconfiguration) {
		export_file = strdup("-");
	} else {
		export_file = strdup(clconfiguration);
	}
	if (!export_file) {
		clerrno = first_err = CL_ENOMEM;
		clcommand_perror(clerrno);
		goto cleanup;
	}

	// Get the valid nas device list from the user input
	clerrno = clnasdevice_preprocess_naslist(operands, nasdevicetypes,
	    nas_list, true);
	if (clerrno != CL_NOERR) {
		if (first_err == CL_NOERR) {
			first_err = clerrno;
		}
		if ((clerrno != CL_ETYPE) && (clerrno != CL_ENOENT)) {
			clcommand_perror(clerrno);
		}
	}

	// If nothing to export?
	if (nas_list.getSize() == 0) {
		goto cleanup;
	}

	// Add the configuration data of the nas devices in nas_list.
	clerrno = clnasdevice_get_nas_xml(nas_list, directories,
	    clnas_export);
	if ((clerrno != CL_NOERR) && (first_err == CL_NOERR)) {
		// Errors if any, are already printed in above function.
		first_err = clerrno;
	}

	// Create xml
	clerrno = clnas_clxml.createExportFile(CLNAS_CMD, export_file,
	    clnas_export);
	if ((clerrno != CL_NOERR) && (first_err == CL_NOERR)) {
		first_err = clerrno;
	}

cleanup:
	// Free memory
	if (export_file) {
		free(export_file);
	}

	delete clnas_export;

	// Return the first error
	if (first_err != CL_NOERR) {
		clerrno = first_err;
	}

	return (clerrno);
} //lint !e715

//
// clnasdevice_get_nas_xml
//
//	From the nasdevice index table, read all the nas devices that match
//	the <devicename, type> pair saved in the NameValueList. If the list
//	is empty, get all the nas devices. Add those nas devices and their
//	configuration data into the ClnasExport object.
//
static clerrno_t
clnasdevice_get_nas_xml(NameValueList &nas_list, NameValueList &directories,
    ClnasExport *clnas_export)
{
	clerrno_t clerrno = CL_NOERR;
	scnas_errno_t scnas_err = SCNAS_NOERR;
	int found;
	clerrno_t first_err = CL_NOERR;
	cl_query_table_t index_table;
	cl_query_table_t filer_table;
	char table_name[SCNAS_MAXSTRINGLEN];
	uint_t i, j;
	bool get_all = false;
	bool all_dir = false;
	char *nas_name, *nas_type, *nas_user;
	char *list_name, *list_type;
	char *nas_dir;

	// If nas_list is empty, get all nas devices in the system.
	if (nas_list.getSize() == 0) {
		get_all = true;
	}

	if (directories.getSize() == 0) {
		all_dir = true;
	}

	// Initialize the ccr table structure
	index_table.n_rows = 0;
	filer_table.n_rows = 0;

	// Read the NAS index table
	scnas_err = scnas_read_filer_table(NAS_SERVICE_KEY_TABLE,
	    NULL, NULL, &index_table, GET_TABLE_CONTENTS);
	if (scnas_err != SCNAS_NOERR) {
		// It is not an error if no NAS devices configured
		if (scnas_err != SCNAS_ENOEXIST) {
			clerrno = first_err = clnasdevice_conv_scnas_err(
			    scnas_err);
			clnasdevice_perror(clerrno);
		}
		goto cleanup;
	}

	// Go through the NAS devices saved by the index
	for (i = 0; i < index_table.n_rows; i++) {
		if (!(index_table.table_rows[i]->key) ||
		    !(index_table.table_rows[i]->value))
			continue;

		nas_type = index_table.table_rows[i]->value;

		// Get the NAS ccr table name
		(void) sprintf(table_name, "%s_%s",
		    NAS_SERVICE_TABLE_PRE, index_table.table_rows[i]->key);

		// Read the NAS ccr table
		scnas_err = scnas_read_filer_table(table_name, NULL, NULL,
		    &filer_table, GET_TABLE_CONTENTS);
		if (scnas_err != SCNAS_NOERR) {
			clerrno = clnasdevice_conv_scnas_err(scnas_err);
			clnasdevice_perror(clerrno);
			if (first_err == CL_NOERR) {
				first_err = clerrno;
			}
			continue;
		}

		// Check the filer configuration in the table
		found = 0;
		nas_user = NULL;
		for (j = 0; j < filer_table.n_rows; j++) {
			if (!(filer_table.table_rows[j]->key) ||
			    !(filer_table.table_rows[j]->value))
				continue;

			// Get the NAS device name
			if (strcmp(filer_table.table_rows[j]->key,
			    PROP_FILER_NAME) == 0) {
				nas_name = filer_table.table_rows[j]->value;

				if (get_all) {
					found++;
					continue;
				}

				// Is it in nas_list?
				for (nas_list.start(); !nas_list.end();
				    nas_list.next()) {
					list_name = nas_list.getName();
					list_type = nas_list.getValue();
					if (!list_name || !list_type)
						continue;

					if ((strcmp(list_name, nas_name)
					    == 0) &&
					    (strcmp(list_type, nas_type)
					    == 0)) {
						found++;
						break;
					}
				}
			} else if (strcmp(filer_table.table_rows[j]->key,
			    PROP_USERID) == 0) {
				nas_user = filer_table.table_rows[j]->value;
			}
		}

		if (!found) {
			(void) cl_query_free_table(&filer_table);
			continue;
		}

		// Add the nas device to the nas xml object
		clerrno = clnas_export->createNasDevice(nas_name, nas_type,
		    nas_user); //lint !e644
		if (clerrno != CL_NOERR) {
			if (first_err == CL_NOERR) {
				first_err = clerrno;
			}
			clcommand_perror(clerrno);
			continue;
		}

		// Get the direcoties
		for (j = 0; j < filer_table.n_rows; j++) {
			if (!(filer_table.table_rows[j]->key) ||
			    !(filer_table.table_rows[j]->value))
				continue;

			// Skip if not directory
			if (strstr(filer_table.table_rows[j]->key,
			    PROP_DIRECTORY) == NULL)
				continue;

			// Is the directory specified by user?
			found = 0;
			if (all_dir) {
				found = 1;
			}
			for (directories.start(); !directories.end();
			    directories.next()) {
				nas_dir = directories.getValue();
				if (!nas_dir)
					continue;

				if (strcmp(nas_dir,
				    filer_table.table_rows[j]->value) == 0) {
					found = 1;
					break;
				}
			}

			// Get the directories
			if (found) {
				clerrno = clnas_export->addNasDir(
				    nas_name, filer_table.table_rows[j]->value);
				if (clerrno != CL_NOERR) {
					if (first_err == CL_NOERR) {
						first_err = clerrno;
					}
					clcommand_perror(clerrno);
				}
			}
		}

		// Free the NAS ccr table
		(void) cl_query_free_table(&filer_table);
	}

	// Free the index table
	(void) cl_query_free_table(&index_table);

cleanup:
	if (first_err != CL_NOERR) {
		clerrno = first_err;
	}

	return (clerrno);
}
