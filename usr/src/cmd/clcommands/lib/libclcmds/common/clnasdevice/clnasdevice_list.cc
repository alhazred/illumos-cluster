//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clnasdevice_list.cc	1.2	08/05/20 SMI"

//
// Process clnasdevice "list"
//

#include "clnasdevice.h"
#include "ClList.h"

//
// clnasdevice "list"
//
clerrno_t
clnasdevice_list(ClCommand &cmd, ValueList &operands, optflgs_t optflgs,
    ValueList &nastypes)
{
	clerrno_t clerrno = CL_NOERR;
	NameValueList nas_list;
	ClList print_list = ClList(cmd.isVerbose ? true : false);

	// Get the valid nas device list from the user input
	clerrno = clnasdevice_preprocess_naslist(operands, nastypes,
	    nas_list, true);
	if (clerrno != CL_NOERR) {
		if ((clerrno != CL_ETYPE) && (clerrno != CL_ENOENT)) {
			clcommand_perror(clerrno);
			return (clerrno);
		}

		// If nothing to print
		if (nas_list.getSize() == 0) {
			return (clerrno);
		}
	}

	// If verbose, add the header
	if ((optflgs & vflg) || cmd.isVerbose) {
		print_list.setHeadings((char *)gettext("NAS Device"),
		    (char *)gettext("Type"));
	}

	// List the NAS devices
	for (nas_list.start(); !nas_list.end(); nas_list.next()) {
		// Add the nas device
		print_list.addRow(nas_list.getName(), nas_list.getValue());
	}

	// Print it
	print_list.print();

	return (clerrno);
}
