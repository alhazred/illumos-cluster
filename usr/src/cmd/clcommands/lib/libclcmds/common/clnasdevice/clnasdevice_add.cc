//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clnasdevice_add.cc	1.4	08/05/20 SMI"

//
// Process clnasdevice "add"
//

#include "clnasdevice.h"
#include "ClXml.h"

//
// clnasdevice "add" without -i
//
clerrno_t
clnasdevice_add(ClCommand &cmd, ValueList &operands, optflgs_t optflgs,
    ValueList &nasdevicetypes, NameValueList &properties)
{
	char *msgbuffer = (char *)0;
	scnas_errno_t scnas_err = SCNAS_NOERR;
	clerrno_t clerrno = CL_NOERR;
	scnas_filer_prop_t *proplist = (scnas_filer_prop_t *)0;
	char *nas_name;

	// Get the properties
	clerrno = clnasdevice_prop_convert(properties, &proplist);
	if (clerrno != CL_NOERR) {
		clcommand_perror(clerrno);
		goto cleanup;
	}

	// Add the filer. It is already checked to only add one device.
	operands.start();
	nas_name = operands.getValue();
	nasdevicetypes.start();
	scnas_err = scnas_attach_filer(nas_name,
	    nasdevicetypes.getValue(), proplist, &msgbuffer);
	if (scnas_err != SCNAS_NOERR) {
		// Print detailed messages
		if (msgbuffer != (char *)0) {
			clcommand_dumpmessages(msgbuffer);
		}

		switch (scnas_err) {
		case SCNAS_EEXIST:
			clerror("NAS device \"%s\" already exists.\n",
			    nas_name);
			clerrno = CL_EEXIST;
			break;

		case SCNAS_ENOEXIST:
		case SCNAS_EAUTH:
			clerrno = CL_EINVAL;
			break;

		case SCNAS_EINVALPROP:
		case SCNAS_EUSAGE:
			clerrno = CL_EPROP;
			break;

		case SCNAS_ESETUP:
			clerrno = CL_EOP;
			break;

		case SCNAS_VP_MISMATCH:
			clerror("You cannot add a NAS device when the "
			    "software versions on two or more nodes "
			    "do not match.\n");
			clerrno = CL_EOP;
			break;

		default:
			clerrno = clnasdevice_conv_scnas_err(scnas_err);
			clnasdevice_perror(clerrno);
			break;

		} //lint !e788

		goto cleanup;
	} else if (optflgs & vflg) {
		clmessage("Nas device \"%s\" is added.\n", nas_name);
	}

cleanup:
	if (msgbuffer) {
		free(msgbuffer);
	}

	if (proplist != (scnas_filer_prop_t *)0) {
		scnas_free_proplist(proplist);
	}

	return (clerrno);
}

//
// clnasdevice "add" with -i
//
clerrno_t
clnasdevice_add(ClCommand &cmd, ValueList &operands, optflgs_t optflgs,
    char *clconfiguration, ValueList &nasdevicetypes,
    NameValueList &properties)
{
	clerrno_t clerrno = CL_NOERR;
	clerrno_t first_err = CL_NOERR;
	ValueList xml_objects;

	// Check for inputs
	if (!clconfiguration) {
		clcommand_perror(CL_EINTERNAL);
		return (CL_EINTERNAL);
	}

	OptionValues *ov = new OptionValues((char *)"add", cmd.argc, cmd.argv);
	ClXml xml;

	// Add quorum types to OptionValues
	ov->addOptions(CLNAS_TYPE, &nasdevicetypes);

	// Add quorum properties to OptionValues
	ov->addNVOptions(CLNAS_PROP, &properties);

	// verbose?
	if (optflgs & vflg) {
		ov->optflgs |= vflg;
	}

	// Get all the objects in the xml file
	clerrno = xml.getAllObjectNames(CLNAS_CMD, clconfiguration,
	    &xml_objects);
	if (clerrno != CL_NOERR) {
		clerror("Cannot read all the NAS devices in \"%s\".\n",
		    clconfiguration);
		return (CL_EINVAL);
	}

	if (xml_objects.getSize() == 0) {
		clerror("Cannot find any NAS devices in \"%s\".\n",
		    clconfiguration);
		return (CL_EINVAL);
	}

	// Set operands
	for (operands.start(); !operands.end(); operands.next()) {
		char *operand_name = operands.getValue();
		if (!operand_name)
			continue;

		// Is this operand found in xml file?
		if (strcmp(operand_name, CLCOMMANDS_WILD_OPERAND) != 0) {
			int found = 0;
			for (xml_objects.start(); !xml_objects.end();
			    xml_objects.next()) {
				char *obj_name = xml_objects.getValue();
				if (!obj_name)
					continue;
				if (strcmp(obj_name, operand_name) == 0) {
					found++;
					break;
				}
			}

			if (!found) {
				clerror("Cannot find NAS device \"%s\" in "
				    "file \"%s\".\n",
				    operand_name, clconfiguration);
				if (first_err == CL_NOERR) {
					first_err = CL_ENOENT;
				}
				continue;
			}
		}

		ov->setOperand(operand_name);
		clerrno = xml.applyConfig(CLNAS_CMD, clconfiguration, ov);
		if ((clerrno != CL_NOERR) && (first_err == CL_NOERR)) {
			first_err = clerrno;
		}
	}

	if (first_err != CL_NOERR) {
		clerrno = first_err;
	}

	return (clerrno);
}
