//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef	_CLRESOURCE_CMD_H
#define	_CLRESOURCE_CMD_H

#pragma ident	"@(#)clresource_cmd.h	1.3	08/05/20 SMI"

#include "clcommands.h"
#include "clresource.h"

//
// This is the main header file for clresource functions
//

#define	CLRS_SUBCMD_CREATE	\
	GETTEXT("Create resources")
#define	CLRS_SUBCMD_DELETE	\
	GETTEXT("Delete resources")
#define	CLRS_SUBCMD_DISABLE	\
	GETTEXT("Disable resources")
#define	CLRS_SUBCMD_ENABLE	\
	GETTEXT("Enable resources")
#define	CLRS_SUBCMD_EXPORT	\
	GETTEXT("Export resource configuration")
#define	CLRS_SUBCMD_LIST	\
	GETTEXT("List resources")
#define	CLRS_SUBCMD_LIST_PROPS	\
	GETTEXT("List resource properties")
#define	CLRS_SUBCMD_SET		\
	GETTEXT("Set resource properties")
#define	CLRS_SUBCMD_MONITOR	\
	GETTEXT("Enable monitoring of resources")
#define	CLRS_SUBCMD_CLEAR	\
	GETTEXT("Clear resource error flags")
#define	CLRS_SUBCMD_SHOW	\
	GETTEXT("Show resources and their properties")
#define	CLRS_SUBCMD_STATUS	\
	GETTEXT("Display the status of resources")
#define	CLRS_SUBCMD_UNMONITOR	\
	GETTEXT("Disable monitoring of resources")
#define	CLRS_CMD_DESC		\
	GETTEXT("Manage cluster resources")

extern clcommandinit_t clresourceinit;

// Subcommand options
extern int clresource_create_options(ClCommand &cmd);
extern int clresource_delete_options(ClCommand &cmd);
extern int clresource_disable_options(ClCommand &cmd);
extern int clresource_enable_options(ClCommand &cmd);
extern int clresource_export_options(ClCommand &cmd);
extern int clresource_list_options(ClCommand &cmd);
extern int clresource_list_props_options(ClCommand &cmd);
extern int clresource_set_options(ClCommand &cmd);
extern int clresource_monitor_options(ClCommand &cmd);
extern int clresource_clear_options(ClCommand &cmd);
extern int clresource_show_options(ClCommand &cmd);
extern int clresource_status_options(ClCommand &cmd);
extern int clresource_unmonitor_options(ClCommand &cmd);

#endif /* _CLRESOURCE_CMD_H */
