#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#ident	"@(#)Makefile.com	1.2	08/05/20 SMI"
#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# cmd/clcommands/commands/clresource/Makefile.com
#
OBJECTS = \
	clresource.o \
	clresourceinit_constants.o \
	clresource_create_options.o \
	clresource_delete_options.o \
	clresource_disable_options.o \
	clresource_enable_options.o \
	clresource_export_options.o \
	clresource_list_options.o \
	clresource_list_props_options.o \
	clresource_set_options.o \
	clresource_monitor_options.o \
	clresource_clear_options.o \
	clresource_show_options.o \
	clresource_status_options.o \
	clresource_unmonitor_options.o

CLRS_HEADER = clresource_cmd.h
