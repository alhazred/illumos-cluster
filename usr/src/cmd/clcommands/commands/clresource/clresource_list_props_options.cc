//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident   "@(#)clresource_list_props_options.cc	1.2	08/05/20 SMI"

//
// Process clresource "list-props" subcommand options
//

#include "clresource_cmd.h"

//
// clresource "list-props" options
//
int
clresource_list_props_options(ClCommand &cmd)
{
	int i;
	int c;
	int wildcard;
	int num_operands;
	int errcount = 0;
	char *option = 0;
	int option_index = 0;
	char *badoption;
	char *shortopts;
	struct option *longopts;
	ValueList operands;
	optflgs_t optflgs = 0;
	int local_lflg = 0;

	ValueList resourcegroups;
	ValueList resourcetypes;
	ValueList properties;
	ValueList xproperties;
	ValueList yproperties;
	listtype_t listtype = LISTTYPE_EXTENSION;

#ifdef RS_LH
	optflgs |= LHflg;
#endif

#ifdef RS_SA
	optflgs |= SAflg;
#endif


	// Get the short and long option lists
	shortopts = clsubcommand_get_shortoptions(cmd);
	longopts = clsubcommand_get_longoptions(cmd);

	// This subcommand should have options
	if (!shortopts || !longopts) {
		clerror("Aborting - subcommand initialization failed.\n");
		abort();
	}

	optind = 2;
	while ((c = getopt_clip(cmd.argc, cmd.argv, shortopts, longopts,
	    &option_index)) != -1) {

		// Get the option
		delete option;
		option = clcommand_option(optopt, cmd.argv[optind - 1]);

		// Switch on option letter
		switch (c) {
		case 'v':
			// Verbose option
			cmd.isVerbose = 1;
			optflgs |= vflg;
			break;

		case '?':
			// Help option?
			if ((strcmp(option, "-?") == 0) ||
			    (strcmp(option, "--help") == 0)) {
				optflgs |= hflg;
				break;
			}

			// Or, unrecognized option?
			clerror("Unrecognized option - \"%s\".\n", option);
			++errcount;
			break;

		case 'g':
			// Resource groups
			resourcegroups.add(1, optarg);
			break;

		case 'l':
			// List type
			if (strcasecmp("extension", optarg) == 0) {
				listtype = LISTTYPE_EXTENSION;
			} else if (strcasecmp("standard", optarg) == 0) {
				listtype = LISTTYPE_STANDARD;
			} else if (strcasecmp("all", optarg) == 0) {
				listtype = LISTTYPE_ALL;
			} else {
				clerror("Only \"%s\", \"%s\", and \"%s\" "
				    "can be used with \"%s\".\n",
				    "extension", "standard", "all",
				    option);
				++errcount;
			}
			local_lflg++;
			break;

		case 'p':
			// Properties
			properties.add(1, optarg);
			break;

#ifdef RS_PLAIN
		case 't':
			// Resource types
			resourcetypes.add(1, optarg);
			break;

		case 'x':
			// Extension properties
			xproperties.add(1, optarg);
			break;

		case 'y':
			// Standard properties
			yproperties.add(1, optarg);
			break;
#endif

		case ':':
			// Missing argument
			clerror("Option \"%s\" requires an argument.\n",
			    option);
			++errcount;
			break;

		default:
			//
			// We should never get here.
			// If we do, it means that there
			// is an option in our option list
			// for which there is no case statement.
			// We either have a bad option list or
			// a bad case statement.
			//
			clerror("Unexpected option - \"%s\".\n", option);
			++errcount;
			break;
		}
	}
	delete option;
	option = (char *)0;

	// Errors?
	if (errcount) {
		clcommand_usage(cmd, cmd.getSubCommandName(), errcount);
		return (CL_EINVAL);
	}

	// Just print help?
	if (optflgs & hflg) {
		clcommand_help(cmd, cmd.getSubCommandName());
		return (CL_NOERR);
	}

	if (local_lflg > 1) {
		clerror("You can specify the \"-l\" option only once.\n");
		++errcount;
	}

	// If wildcard operand specified, there should be no other operand
	wildcard = 0;
	num_operands = 0;
	for (i = optind;  i < cmd.argc;  ++i) {
		num_operands++;
		if (strcmp(cmd.argv[i], CLCOMMANDS_WILD_OPERAND) == 0) {
			++wildcard;
		}
	}
	if ((wildcard) && (num_operands > 1)) {
		++errcount;
		clerror("If you specify an operand, it should be either a "
		    "\"+\" or a list of resources.\n");
	}

	// Update the operands list
	if (wildcard) {
		operands.add(CLCOMMANDS_WILD_OPERAND);
	} else {
		for (i = optind;  i < cmd.argc;  ++i)
			operands.add(cmd.argv[i]);
	}

	// Errors?
	if (errcount) {
		clcommand_usage(cmd, cmd.getSubCommandName(), errcount);
		return (CL_EINVAL);
	}

	// Perform clresource "list-props" operation
	return (clresource_list_props(cmd, operands, optflgs, resourcegroups,
	    resourcetypes, listtype, properties, xproperties, yproperties));
}
