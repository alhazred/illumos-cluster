//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clresourceinit_constants.cc	1.16	08/10/14 SMI"

//
// Initialization data for clresource(1CL)
//

#include "clresource_cmd.h"

extern clcommandinit_t clresourceinit;

//
// Usage Messages
//
// All usage message lines should begin with a single "%s" for
// the command name.   Lines must never include more than a single "%s".
//

// List of "clresource" command usage message
static char *clresource_usage [] = {
	"%s <subcommand> [<options>] [+ | <resource> ...]",
	"%s [<subcommand>]  -? | --help",
	"%s  -V | --version",
	(char *)0
};

// List of clresource "create" subcommand usage messages
static char *clresource_create_usage[] = {
#ifdef RS_PLAIN
	"%s create -g <resourcegroup> -t <resourcetype> [<options>] "
	    "<resource>",
#else
	"%s create -g <resourcegroup> [<options>] <resource>",
#endif
	"%s create -i <configfile> [<options>] [+ | <resource> ...]",
	(char *)0
};

// List of clresource "delete" subcommand usage messages
static char *clresource_delete_usage[] = {
	"%s delete [<options>] + | <resource> ...",
	(char *)0
};

// List of clresource "disable" subcommand usage messages
static char *clresource_disable_usage[] = {
	"%s disable [<options>] + | <resource> ...",
	(char *)0
};

// List of clresource "enable" subcommand usage messages
static char *clresource_enable_usage[] = {
	"%s enable [<options>] + | <resource> ...",
	(char *)0
};

// List of clresource "export" subcommand usage messages
static char *clresource_export_usage[] = {
	"%s export [<options>] [+ | <resource> ...]",
	(char *)0
};

// List of clresource "list" subcommand usage messages
static char *clresource_list_usage[] = {
	"%s list [<options>] [+ | <resource> ...]",
	(char *)0
};

// List of clresource "list-props" subcommand usage messages
static char *clresource_list_props_usage[] = {
	"%s list-props [<options>] [+ | <resource> ...]",
	(char *)0
};

// List of clresource "set" subcommand usage messages
static char *clresource_set_usage[] = {
	"%s set [<options>] + | <resource> ...",
	(char *)0
};

// List of clresource "monitor" subcommand usage messages
static char *clresource_monitor_usage[] = {
	"%s monitor [<options>] + | <resource> ...",
	(char *)0
};

// List of clresource "clear" subcommand usage messages
static char *clresource_clear_usage[] = {
	"%s clear [<options>] + | <resource> ...",
	(char *)0
};

// List of clresource "show" subcommand usage messages
static char *clresource_show_usage[] = {
	"%s show [<options>] [+ | <resource> ...]",
	(char *)0
};

// List of clresource "status" subcommand usage messages
static char *clresource_status_usage[] = {
	"%s status [<options>] [+ | <resource> ...]",
	(char *)0
};

// List of clresource "unmonitor" subcommand usage messages
static char *clresource_unmonitor_usage[] = {
	"%s unmonitor [<options>] + | <resource> ...",
	(char *)0
};

//
// Option lists
//
// All option descriptions should be broken up into lines of no
// greater than 70 characters.  All but the trailing newline ('\n')
// itself should be included in the option descriptions found in these
// tables.
//

// List of options for the clresource "create" subcommand
static cloptions_t clresource_create_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'a', "automatic", 0, 0,
	    CLDES_OPT_AUTO
	},
	{'d', "disable", 0, 0,
	    CLDES_OPT_DISABLE
	},
	{'g', "resourcegroup", "<resourcegroup>", CLOPT_ARG_LIST,
	    CLDES_OPT_RG
	},
#if defined RS_LH || RS_SA
	{'h', "lhost", "<lhost>", CLOPT_ARG_LIST,
	    CLDES_OPT_LHOST
	},
#endif
	{'i', "input", "{- | <clconfiguration>}", 0,
	    CLDES_OPT_INPUT
	},
#if defined RS_LH || RS_SA
	{'N', "netiflist", "<netif>@<node>", CLOPT_ARG_LIST,
	    CLDES_OPT_NETIFLIST
	},
#endif
	{'p', "property", "<name>=<value>", 0,
	    CLDES_OPT_RS_PROPERTY
	},
#ifdef RS_PLAIN
	{'t', "type", "<resourcetype>", CLOPT_ARG_LIST,
	    CLDES_OPT_RT
	},
#endif
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
#ifdef RS_SA
	{'X', "auxnode", "<node>[:<zone>]", CLOPT_ARG_LIST,
	    CLDES_OPT_AUXNODES
	},
#endif
#ifdef RS_PLAIN
	{'x', "extension-property", "<name>=<value>", 0,
	    CLDES_OPT_RS_XPROPERTY
	},
	{'y', "standard-property", "<name>=<value>", 0,
	    CLDES_OPT_RS_YPROPERTY
	},
#endif
#if SOL_VERSION >= __s10
	{'Z', "zonecluster", "{global | <zone-cluster-name>}", 0,
	    CLDES_OPT_ZC_NAME
	},
#endif
	{0, 0, 0, 0, 0}
};

// List of options for the clresource "delete" subcommand
static cloptions_t clresource_delete_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'F', "force", 0, 0,
	    CLDES_OPT_FORCE
	},
	{'g', "resourcegroup", "<resourcegroup>", CLOPT_ARG_LIST,
	    CLDES_OPT_RG
	},
#ifdef RS_PLAIN
	{'t', "type", "<resourcetype>", CLOPT_ARG_LIST,
	    CLDES_OPT_RT
	},
#endif
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
#if SOL_VERSION >= __s10
	{'Z', "zonecluster", "{global | <zone-cluster-name>}", 0,
	    CLDES_OPT_ZC_NAME
	},
#endif
	{0, 0, 0, 0, 0}
};

// List of options for the clresource "disable" subcommand
static cloptions_t clresource_disable_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'g', "resourcegroup", "<resourcegroup>", CLOPT_ARG_LIST,
	    CLDES_OPT_RG
	},
#if SOL_VERSION >= __s10
	{'n', "node", "<node>[:<zone>]", CLOPT_ARG_LIST,
#else
	{'n', "node", "<node>", CLOPT_ARG_LIST,
#endif
	    CLDES_OPT_NODE
	},
	{'R', "recursive", 0, 0,
	    CLDES_OPT_RS_RECURSIVE_DISABLE
	},
#ifdef RS_PLAIN
	{'t', "type", "<resourcetype>", CLOPT_ARG_LIST,
	    CLDES_OPT_RT
	},
#endif
	{'u', "suspended", 0, 0,
		CLDES_OPT_SUSPENDED
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
#if SOL_VERSION >= __s10
	{'z', "zone", "<zone>", 0,
	    CLDES_OPT_ZONE
	},
	{'Z', "zonecluster", "{global | <zone-cluster-name>}", 0,
	    CLDES_OPT_ZC_NAME
	},
#endif
	{0, 0, 0, 0, 0}
};

// List of options for the clresource "enable" subcommand
static cloptions_t clresource_enable_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'g', "resourcegroup", "<resourcegroup>", CLOPT_ARG_LIST,
	    CLDES_OPT_RG
	},
#if SOL_VERSION >= __s10
	{'n', "node", "<node>[:<zone>]", CLOPT_ARG_LIST,
#else
	{'n', "node", "<node>", CLOPT_ARG_LIST,
#endif
	    CLDES_OPT_NODE
	},
	{'R', "recursive", 0, 0,
	    CLDES_OPT_RS_RECURSIVE_ENABLE
	},
#ifdef RS_PLAIN
	{'t', "type", "<resourcetype>", CLOPT_ARG_LIST,
	    CLDES_OPT_RT
	},
#endif
	{'u', "suspended", 0, 0,
		CLDES_OPT_SUSPENDED
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
#if SOL_VERSION >= __s10
	{'z', "zone", "<zone>", 0,
	    CLDES_OPT_ZONE
	},
	{'Z', "zonecluster", "{global | <zone-cluster-name>}", 0,
	    CLDES_OPT_ZC_NAME
	},
#endif
	{0, 0, 0, 0, 0}
};

// List of options for the clresource "export" subcommand
static cloptions_t clresource_export_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{'o', "output", "{- | <clconfiguration>}", 0,
	    CLDES_OPT_OUTPUT
	},
	{0, 0, 0, 0, 0}
};

// List of options for the clresource "list" subcommand
static cloptions_t clresource_list_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'g', "resourcegroup", "<resourcegroup>", CLOPT_ARG_LIST,
	    CLDES_OPT_RG
	},
	{'s', "state", "<state>", CLOPT_ARG_LIST,
	    CLDES_OPT_RS_STATE
	},
#ifdef RS_PLAIN
	{'t', "type", "<resourcetype>", CLOPT_ARG_LIST,
	    CLDES_OPT_RT
	},
#endif
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
#if SOL_VERSION >= __s10
	{'Z', "zonecluster", "{all | global | <zone-cluster-name>}", 0,
	    CLDES_OPT_ZC_NAME
	},
#endif
	{0, 0, 0, 0, 0}
};

// List of options for the clresource "list-props" subcommand
static cloptions_t clresource_list_props_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'g', "resourcegroup", "<resourcegroup>", CLOPT_ARG_LIST,
	    CLDES_OPT_RG
	},
	{'l', "listtype", "<listtype>", 0,
	    CLDES_OPT_RS_LISTTYPE
	},
	{'p', "property", "<name>", CLOPT_ARG_LIST,
	    CLDES_OPT_RS_PROPERTIES
	},
#ifdef RS_PLAIN
	{'t', "type", "<resourcetype>", CLOPT_ARG_LIST,
	    CLDES_OPT_RT
	},
#endif
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
#ifdef RS_PLAIN
	{'x', "extension-property", "<name>", CLOPT_ARG_LIST,
	    CLDES_OPT_RS_XPROPERTIES
	},
	{'y', "standard-property", "<name>", CLOPT_ARG_LIST,
	    CLDES_OPT_RS_YPROPERTIES
	},
#endif
	{0, 0, 0, 0, 0}
};

// List of options for the clresource "set" subcommand
static cloptions_t clresource_set_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'g', "resourcegroup", "<resourcegroup>", CLOPT_ARG_LIST,
	    CLDES_OPT_RG
	},
	{'p', "property", "<name>[+|-]=<value>", 0,
	    CLDES_OPT_RS_PROPERTY
	},
#ifdef RS_PLAIN
	{'t', "type", "<resourcetype>", CLOPT_ARG_LIST,
	    CLDES_OPT_RT
	},
#endif
	{'u', "suspended", 0, 0,
		CLDES_OPT_SUSPENDED
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
#ifdef RS_PLAIN
	{'x', "extension-property", "<name>[+|-]=<value>", 0,
	    CLDES_OPT_RS_XPROPERTY
	},
	{'y', "standard-property", "<name>[+|-]=<value>", 0,
	    CLDES_OPT_RS_YPROPERTY
	},
#endif
#if SOL_VERSION >= __s10
	{'Z', "zonecluster", "{global | <zone-cluster-name>}", 0,
	    CLDES_OPT_ZC_NAME
	},
#endif
	{0, 0, 0, 0, 0}
};

// List of options for the clresource "monitor" subcommand
static cloptions_t clresource_monitor_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'g', "resourcegroup", "<resourcegroup>", CLOPT_ARG_LIST,
	    CLDES_OPT_RG
	},
#if SOL_VERSION >= __s10
	{'n', "node", "<node>[:<zone>]", CLOPT_ARG_LIST,
#else
	{'n', "node", "<node>", CLOPT_ARG_LIST,
#endif
	    CLDES_OPT_NODE
	},
#ifdef RS_PLAIN
	{'t', "type", "<resourcetype>", CLOPT_ARG_LIST,
	    CLDES_OPT_RT
	},
#endif
	{'u', "suspended", 0, 0,
		CLDES_OPT_SUSPENDED
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
#if SOL_VERSION >= __s10
	{'z', "zone", "<zone>", 0,
	    CLDES_OPT_ZONE
	},
	{'Z', "zonecluster", "{global | <zone-cluster-name>}", 0,
	    CLDES_OPT_ZC_NAME
	},
#endif
	{0, 0, 0, 0, 0}
};

// List of options for the clresource "clear" subcommand
static cloptions_t clresource_clear_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'f', "flag", "<errorflag>", 0,
	    CLDES_OPT_RS_ERRFLAG
	},
	{'g', "resourcegroup", "<resourcegroup>", CLOPT_ARG_LIST,
	    CLDES_OPT_RG
	},
#if SOL_VERSION >= __s10
	{'n', "node", "<node>[:<zone>]", CLOPT_ARG_LIST,
#else
	{'n', "node", "<node>", CLOPT_ARG_LIST,
#endif
	    CLDES_OPT_NODE
	},
#ifdef RS_PLAIN
	{'t', "type", "<resourcetype>", CLOPT_ARG_LIST,
	    CLDES_OPT_RT
	},
#endif
	{'u', "suspended", 0, 0,
		CLDES_OPT_SUSPENDED
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
#if SOL_VERSION >= __s10
	{'z', "zone", "<zone>", 0,
	    CLDES_OPT_ZONE
	},
	{'Z', "zonecluster", "{global | <zone-cluster-name>}", 0,
	    CLDES_OPT_ZC_NAME
	},
#endif
	{0, 0, 0, 0, 0}
};

// List of options for the clresource "show" subcommand
static cloptions_t clresource_show_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'g', "resourcegroup", "<resourcegroup>", CLOPT_ARG_LIST,
	    CLDES_OPT_RG
	},
	{'p', "property", "<name>", CLOPT_ARG_LIST,
	    CLDES_OPT_RS_PROPERTIES
	},
#ifdef RS_PLAIN
	{'t', "type", "<resourcetype>", CLOPT_ARG_LIST,
	    CLDES_OPT_RT
	},
	{'x', "extension-property", "<name>", CLOPT_ARG_LIST,
	    CLDES_OPT_RS_XPROPERTIES
	},
	{'y', "standard-property", "<name>", CLOPT_ARG_LIST,
	    CLDES_OPT_RS_YPROPERTIES
	},
#endif

	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
#if SOL_VERSION >= __s10
	{'Z', "zonecluster", "{all | global | <zone-cluster-name>}", 0,
	    CLDES_OPT_ZC_NAME
	},
#endif
	{0, 0, 0, 0, 0}
};

// List of options for the clresource "status" subcommand
static cloptions_t clresource_status_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'g', "resourcegroup", "<resourcegroup>", CLOPT_ARG_LIST,
	    CLDES_OPT_RG
	},
#if SOL_VERSION >= __s10
	{'n', "node", "<node>[:<zone>]", CLOPT_ARG_LIST,
#else
	{'n', "node", "<node>", CLOPT_ARG_LIST,
#endif
	    CLDES_OPT_NODE
	},
	{'s', "state", "<state>", CLOPT_ARG_LIST,
	    CLDES_OPT_RS_STATE
	},
#ifdef RS_PLAIN
	{'t', "type", "<resourcetype>", CLOPT_ARG_LIST,
	    CLDES_OPT_RT
	},
#endif
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
#if SOL_VERSION >= __s10
	{'z', "zone", "<zone>", 0,
	    CLDES_OPT_ZONE
	},
	{'Z', "zonecluster", "{all | global | <zone-cluster-name>}", 0,
	    CLDES_OPT_ZC_NAME
	},
#endif
	{0, 0, 0, 0, 0}
};

// List of options for the clresource "unmonitor" subcommand
static cloptions_t clresource_unmonitor_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'g', "resourcegroup", "<resourcegroup>", CLOPT_ARG_LIST,
	    CLDES_OPT_RG
	},
#if SOL_VERSION >= __s10
	{'n', "node", "<node>[:<zone>]", CLOPT_ARG_LIST,
#else
	{'n', "node", "<node>", CLOPT_ARG_LIST,
#endif
	    CLDES_OPT_NODE
	},
#ifdef RS_PLAIN
	{'t', "type", "<resourcetype>", CLOPT_ARG_LIST,
	    CLDES_OPT_RT
	},
#endif
	{'u', "suspended", 0, 0,
		CLDES_OPT_SUSPENDED
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
#if SOL_VERSION >= __s10
	{'z', "zone", "<zone>", 0,
	    CLDES_OPT_ZONE
	},
	{'Z', "zonecluster", "{global | <zone-cluster-name>}", 0,
	    CLDES_OPT_ZC_NAME
	},
#endif
	{0, 0, 0, 0, 0}
};

// List of subcommands for the "clreosource" command
static clsubcommand_t clresource_subcommands[] = {
	{
		"create",
#ifdef RS_PLAIN
		CLSUB_DO_COMMAND_LOGGING | CLSUB_NON_GLOBAL_ZONE_ALLOWED |
			CLSUB_ZONE_CLUSTER_ALLOWED,
#else
		CLSUB_DO_COMMAND_LOGGING | CLSUB_ZONE_CLUSTER_ALLOWED,
#endif
		(char *)CLRS_SUBCMD_CREATE,
		CL_AUTH_CLUSTER_MODIFY,
		clresource_create_usage,
		clresource_create_optionslist,
		clresource_create_options
	},
	{
		"delete",
		CLSUB_DO_COMMAND_LOGGING | CLSUB_NON_GLOBAL_ZONE_ALLOWED |
			CLSUB_ZONE_CLUSTER_ALLOWED,
		(char *)CLRS_SUBCMD_DELETE,
		CL_AUTH_CLUSTER_MODIFY,
		clresource_delete_usage,
		clresource_delete_optionslist,
		clresource_delete_options
	},
	{
		"disable",
		CLSUB_DO_COMMAND_LOGGING | CLSUB_NON_GLOBAL_ZONE_ALLOWED |
			CLSUB_ZONE_CLUSTER_ALLOWED,
		(char *)CLRS_SUBCMD_DISABLE,
		CL_AUTH_CLUSTER_ADMIN,
		clresource_disable_usage,
		clresource_disable_optionslist,
		clresource_disable_options
	},
	{
		"enable",
		CLSUB_DO_COMMAND_LOGGING | CLSUB_NON_GLOBAL_ZONE_ALLOWED |
			CLSUB_ZONE_CLUSTER_ALLOWED,
		(char *)CLRS_SUBCMD_ENABLE,
		CL_AUTH_CLUSTER_ADMIN,
		clresource_enable_usage,
		clresource_enable_optionslist,
		clresource_enable_options
	},
	{
		"export",
		CLSUB_NON_GLOBAL_ZONE_ALLOWED,
		(char *)CLRS_SUBCMD_EXPORT,
		CL_AUTH_CLUSTER_READ,
		clresource_export_usage,
		clresource_export_optionslist,
		clresource_export_options
	},
	{
		"list",
		CLSUB_NON_GLOBAL_ZONE_ALLOWED | CLSUB_ZONE_CLUSTER_ALLOWED,
		(char *)CLRS_SUBCMD_LIST,
		CL_AUTH_CLUSTER_READ,
		clresource_list_usage,
		clresource_list_optionslist,
		clresource_list_options
	},
	{
		"list-props",
		CLSUB_NON_GLOBAL_ZONE_ALLOWED | CLSUB_ZONE_CLUSTER_ALLOWED,
		(char *)CLRS_SUBCMD_LIST_PROPS,
		CL_AUTH_CLUSTER_READ,
		clresource_list_props_usage,
		clresource_list_props_optionslist,
		clresource_list_props_options
	},
	{
		"set",
		CLSUB_DO_COMMAND_LOGGING | CLSUB_NON_GLOBAL_ZONE_ALLOWED |
			CLSUB_ZONE_CLUSTER_ALLOWED,
		(char *)CLRS_SUBCMD_SET,
		CL_AUTH_CLUSTER_MODIFY,
		clresource_set_usage,
		clresource_set_optionslist,
		clresource_set_options
	},
	{
		"monitor",
		CLSUB_DO_COMMAND_LOGGING | CLSUB_NON_GLOBAL_ZONE_ALLOWED |
			CLSUB_ZONE_CLUSTER_ALLOWED,
		(char *)CLRS_SUBCMD_MONITOR,
		CL_AUTH_CLUSTER_ADMIN,
		clresource_monitor_usage,
		clresource_monitor_optionslist,
		clresource_monitor_options
	},
	{
		"clear",
		CLSUB_DO_COMMAND_LOGGING | CLSUB_NON_GLOBAL_ZONE_ALLOWED |
			CLSUB_ZONE_CLUSTER_ALLOWED,
		(char *)CLRS_SUBCMD_CLEAR,
		CL_AUTH_CLUSTER_ADMIN,
		clresource_clear_usage,
		clresource_clear_optionslist,
		clresource_clear_options
	},
	{
		"show",
		CLSUB_NON_GLOBAL_ZONE_ALLOWED |
			CLSUB_ZONE_CLUSTER_ALLOWED,
		(char *)CLRS_SUBCMD_SHOW,
		CL_AUTH_CLUSTER_READ,
		clresource_show_usage,
		clresource_show_optionslist,
		clresource_show_options
	},
	{
		"status",
		CLSUB_NON_GLOBAL_ZONE_ALLOWED | CLSUB_ZONE_CLUSTER_ALLOWED,
		(char *)CLRS_SUBCMD_STATUS,
		CL_AUTH_CLUSTER_READ,
		clresource_status_usage,
		clresource_status_optionslist,
		clresource_status_options
	},
	{
		"unmonitor",
		CLSUB_DO_COMMAND_LOGGING | CLSUB_NON_GLOBAL_ZONE_ALLOWED |
			CLSUB_ZONE_CLUSTER_ALLOWED,
		(char *)CLRS_SUBCMD_UNMONITOR,
		CL_AUTH_CLUSTER_ADMIN,
		clresource_unmonitor_usage,
		clresource_unmonitor_optionslist,
		clresource_unmonitor_options
	},
	{0, 0, 0, 0, 0, 0, 0}
};

// Initialization properties for the "clresource" command
clcommandinit_t clresourceinit = {
	CLCOMMANDS_COMMON_VERSION,
	(char *)CLRS_CMD_DESC,
	clresource_usage,
	clresource_subcommands
};
