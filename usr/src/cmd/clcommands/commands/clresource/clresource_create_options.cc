//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clresource_create_options.cc	1.6	08/07/28 SMI"

//
// Process clresource "create" subcommand options
//

#include "clresource_cmd.h"

//
// clresource "create" options
//
int
clresource_create_options(ClCommand &cmd)
{
	int i;
	int c;
	int wildcard;
	int num_operands;
	int errcount = 0;
	char *option = 0;
	int option_index = 0;
	char *badoption;
	char *shortopts;
	struct option *longopts;
	ValueList operands;
	optflgs_t optflgs = 0;

	ValueList resourcegroups;
	ValueList resourcetypes;
	ValueList lhosts;
	ValueList netiflist;
	ValueList auxnodes;
	NameValueList properties = NameValueList(true);
	NameValueList xproperties = NameValueList(true);
	NameValueList yproperties = NameValueList(true);
	char *clconfiguration = (char *)0;
	int rv = 0;
	int zcflg = 0;
	int str_len;
	char *zc_name = (char *)0;
	char *r_name;

	int iflg = 0;

#ifdef RS_LH
	optflgs |= LHflg;
#endif

#ifdef RS_SA
	optflgs |= SAflg;
#endif
	// Get the short and long option lists
	shortopts = clsubcommand_get_shortoptions(cmd);
	longopts = clsubcommand_get_longoptions(cmd);

	// This subcommand should have options
	if (!shortopts || !longopts) {
		clerror("Aborting - subcommand initialization failed.\n");
		abort();
	}

	optind = 2;
	while ((c = getopt_clip(cmd.argc, cmd.argv, shortopts, longopts,
	    &option_index)) != -1) {

		// Get the option
		delete option;
		option = clcommand_option(optopt, cmd.argv[optind - 1]);

		// Switch on option letter
		switch (c) {
		case 'v':
			// Verbose option
			cmd.isVerbose = 1;
			optflgs |= vflg;
			break;

		case '?':
			// Help option?
			if ((strcmp(option, "-?") == 0) ||
			    (strcmp(option, "--help") == 0)) {
				optflgs |= hflg;
				break;
			}

			// Or, unrecognized option?
			clerror("Unrecognized option - \"%s\".\n", option);
			++errcount;
			break;

		case 'a':
			// Automatic flag
			optflgs |= aflg;
			break;

		case 'd':
			// Disable flag
			optflgs |= dflg;
			break;

		case 'g':
			// Resource groups
			resourcegroups.add(1, optarg);
			break;

		case 'i':
			// Input file
			clconfiguration = optarg;
			iflg++;
			break;

#if defined RS_LH || RS_SA
		case 'h':
			// lhosts
			lhosts.add(1, optarg);
			break;

		case 'N':
			// netiflists
			netiflist.add(1, optarg);
			break;
#endif

		case 'p':
			// Properties
			if (strchr(optarg, '=') == NULL) {
				clerror("Invalid property specification: "
				    "\"%s\".\n", optarg);
				++errcount;
			}
			if (properties.add(0, optarg)) {
				clerror("Multiple property specification: "
				    "\"%s\".\n", optarg);
				++errcount;
			}
			break;

#ifdef RS_PLAIN
		case 't':
			// Resource types
			resourcetypes.add(1, optarg);
			break;
#endif

#ifdef RS_SA
		case 'X':
			// AuxNodeList
			auxnodes.add(1, optarg);
			break;
#endif

#ifdef RS_PLAIN
		case 'x':
			// Extension properties
			if (strchr(optarg, '=') == NULL) {
				clerror("Invalid property specification: "
				    "\"%s\".\n", optarg);
				++errcount;
			}
			if (xproperties.add(0, optarg)) {
				clerror("Multiple property specification: "
				    "\"%s\".\n", optarg);
				++errcount;
			}
			break;

		case 'y':
			// Standard properties
			if (strchr(optarg, '=') == NULL) {
				clerror("Invalid property specification: "
				    "\"%s\".\n", optarg);
				++errcount;
			}
			if (yproperties.add(0, optarg)) {
				clerror("Multiple property specification: "
				    "\"%s\".\n", optarg);
				++errcount;
			}
			break;
#endif
#if SOL_VERSION >= __s10
		case 'Z':
			// zone cluster name
			optflgs |= Zflg;
			zc_name = optarg;
			zcflg++;
			break;
#endif

		case ':':
			// Missing argument
			clerror("Option \"%s\" requires an argument.\n",
			    option);
			++errcount;
			break;

		default:
			//
			// We should never get here.
			// If we do, it means that there
			// is an option in our option list
			// for which there is no case statement.
			// We either have a bad option list or
			// a bad case statement.
			//
			clerror("Unexpected option - \"%s\".\n", option);
			++errcount;
			break;
		}
	}
	delete option;
	option = (char *)0;

	// Errors?
	if (errcount) {
		clcommand_usage(cmd, cmd.getSubCommandName(), errcount);
		return (CL_EINVAL);
	}

	// Just print help?
	if (optflgs & hflg) {
		clcommand_help(cmd, cmd.getSubCommandName());
		return (CL_NOERR);
	}

	if (iflg > 1) {
		clerror("You can specify the \"-i\" option only once.\n");
		++errcount;
	}
	if (zcflg > 1) {
		clerror("You can specify the \"-Z\" option only once.\n");
		++errcount;
	}

	// If there is no -i, there must be one group, one type, and no -a.
	if (!clconfiguration) {
		switch (resourcegroups.getSize()) {
		case 0:
			clerror("You cannot create a resource without "
			    "specifying a resource group.\n");
			++errcount;
			break;

		case 1:
			break;

		default:
			clerror("You can specify only one resource group, "
			    "unless you use \"-i\".\n");
			++errcount;
			break;
		}

#ifdef RS_PLAIN
		switch (resourcetypes.getSize()) {
		case 0:
			clerror("You cannot create a resource without "
			    "specifying a resource type.\n");
			++errcount;
			break;

		case 1:
			break;

		default:
			clerror("You can specify only one resource type, "
			    "unless you use \"-i\".\n");
			++errcount;
			break;
		}
#endif

		if (optflgs & aflg) {
			clerror("You cannot use the \"-a\" option without "
			    "a \"-i\"option.\n");
			++errcount;
		}
	} else {
		// The convenience options are not allowed with -i
		if (lhosts.getSize() > 0) {
			clerror("You cannot use the \"-h\" option with the "
			    "\"-i\" option.\n");
			++errcount;
		}
		if (netiflist.getSize() > 0) {
			clerror("You cannot use the \"-N\" option with the "
			    "\"-i\" option.\n");
			++errcount;
		}
		if (auxnodes.getSize() > 0) {
			clerror("You cannot use the \"-X\" option with the "
			    "\"-i\" option.\n");
			++errcount;
		}
	}

	// Errors?
	if (errcount) {
		clcommand_usage(cmd, cmd.getSubCommandName(), errcount);
		return (CL_EINVAL);
	}

	//
	// If wildcard operand specified, there should be no other operand,
	// also, only one resource without "-i"
	//
	wildcard = 0;
	num_operands = 0;
	for (i = optind;  i < cmd.argc;  ++i) {
		num_operands++;
		if (strcmp(cmd.argv[i], CLCOMMANDS_WILD_OPERAND) == 0) {
			++wildcard;
		}
	}

	if (!clconfiguration) {
		if (wildcard || (num_operands > 1)) {
			++errcount;
			clerror("You can create only one resource at a time, "
			    "unless you use \"-i\".\n");
		}
	} else {
		if ((wildcard) && (num_operands > 1)) {
			++errcount;
			clerror("You can either specify a \"+\" or a list of "
			    "resources.\n");
		}
	}

	// Update the operands list
	if (wildcard) {
		operands.add(CLCOMMANDS_WILD_OPERAND);
	} else {
		for (i = optind;  i < cmd.argc;  ++i)
			operands.add(cmd.argv[i]);
	}

	// There must be at least one resource
	if (operands.getSize() == 0) {
		++errcount;
		if (!clconfiguration) {
			clerror("You must give the name of the resource "
			    "to create.\n");
		} else {
			clerror("You must specify either \"+\" or a "
			    "list of resources to create.\n");
		}
	}

	// Errors?
	if (errcount) {
		clcommand_usage(cmd, cmd.getSubCommandName(), errcount);
		return (CL_EINVAL);
	}
	// If -Z flag was specified, none of the resources
	// should be scoped.
	// Also if -Z was specified, we should scope each resource
	// with the zone cluster name in the format
	// <zone cluster name>:<r name>
	if (zcflg) {
		for (operands.start(); !operands.end(); operands.next()) {

			if (strchr(operands.getValue(), ':')) {
				++errcount;
				clerror("You cannot specify zone cluster name"
					" as part of resource name while"
					" using the \"-Z\" option.\n");
				break;
			}

			// If we are here, we have to prepend the R name with
			// the zone cluster name
			operands.prepend(zc_name,
					ZC_AND_OBJ_NAME_SEPARATER_CHAR);
		}
	}
	// Errors?
	if (errcount) {
		clcommand_usage(cmd, cmd.getSubCommandName(), errcount);
		return (CL_EINVAL);
	}

	// Perform clresource "create" operation
	if (!clconfiguration) {
		return (clresource_create(cmd, operands, optflgs,
		    resourcegroups, resourcetypes, properties,
		    xproperties, yproperties, lhosts, netiflist, auxnodes));
	} else {
		return (clresource_create(cmd, operands, optflgs,
		    clconfiguration, resourcegroups, resourcetypes,
		    properties, xproperties, yproperties, lhosts,
		    netiflist, auxnodes));
	}
}
