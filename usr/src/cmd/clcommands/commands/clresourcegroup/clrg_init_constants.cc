//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clrg_init_constants.cc	1.21	08/10/14 SMI"

//
// Initialization data for clresourcegroupgroup(1CL)
//

#include "clrg.h"

extern clcommandinit_t clrginit;

//
// Usage Messages
//
// All usage message lines should begin with a single "%s" for
// the command name.   Lines must never include more than a single "%s".
//

// List of "clresourcegroupgroup" command usage message
static char *clrg_usage [] = {
	"%s <subcommand> [<options>] [+ | <resourcegroup> ...]",
	"%s [<subcommand>]  -? | --help",
	"%s  -V | --version",
	(char *)0
};

// List of clresourcegroup "add-node" subcommand usage messages
static char *clrg_add_node_usage[] = {
	"%s add-node [<options>] + | <resourcegroup> ...",
	(char *)0
};

// List of clresourcegroup "create" subcommand usage messages
static char *clrg_create_usage[] = {
	"%s create [<options>] <resourcegroup> ...",
	"%s create -i <configfile> [<options>] [+ | <resourcegroup> ...]",
	(char *)0
};

// List of clresourcegroup "delete" subcommand usage messages
static char *clrg_delete_usage[] = {
	"%s delete [<options>] + | <resourcegroup> ...",
	(char *)0
};

// List of clresourcegroup "evacuate" subcommand usage messages
static char *clrg_evacuate_usage[] = {
#if SOL_VERSION >= __s10
	"%s evacuate [-z <zone>] -n <node>[:<zone>] [<options>] [+]",
#else
	"%s evacuate -n <node> [<options>] [+]",
#endif
	(char *)0
};

// List of clresourcegroup "export" subcommand usage messages
static char *clrg_export_usage[] = {
	"%s export [<options>] [+ | <resourcegroup> ...]",
	(char *)0
};

// List of clrg "list" subcommand usage messages
static char *clrg_list_usage[] = {
	"%s list [<options>] [+ | <resourcegroup> ...]",
	(char *)0
};

// List of clresourcegroup "manage" subcommand usage messages
static char *clrg_manage_usage[] = {
	"%s manage [<options>] + | <resourcegroup> ...",
	(char *)0
};

// List of clresourcegroup "set" subcommand usage messages
static char *clrg_set_usage[] = {
	"%s set [<options>] + | <resourcegroup> ...",
	(char *)0
};

// List of clresourcegroup "offline" subcommand usage messages
static char *clrg_offline_usage[] = {
	"%s offline [<options>] + | <resourcegroup> ...",
	(char *)0
};

// List of clresourcegroup "online" subcommand usage messages
static char *clrg_online_usage[] = {
	"%s online [<options>] + | <resourcegroup> ...",
	(char *)0
};

// List of clresourcegroup "quiesce" subcommand usage messages
static char *clrg_quiesce_usage[] = {
	"%s quiesce [<options>] + | <resourcegroup> ...",
	(char *)0
};

// List of clresourcegroup "remove-node" subcommand usage messages
static char *clrg_remove_node_usage[] = {
#if SOL_VERSION >= __s10
	"%s remove-node [-z <zone>] -n <node>[:<zone>][,...] "
	"[<options>] [+ | <resourcegroup> ...]",
#else
	"%s remove-node -n <node>[,...] [<options>] [+ | <resourcegroup> ...]",
#endif
	(char *)0
};

// List of clresourcegroup "restart" subcommand usage messages
static char *clrg_restart_usage[] = {
	"%s restart [<options>] [+ | <resourcegroup> ...]",
	(char *)0
};

// List of clresourcegroup "resume" subcommand usage messages
static char *clrg_resume_usage[] = {
	"%s resume [<options>] [+ | <resourcegroup> ...]",
	(char *)0
};

// List of clresourcegroup "show" subcommand usage messages
static char *clrg_show_usage[] = {
	"%s show [<options>] [+ | <resourcegroup> ...]",
	(char *)0
};

// List of clresourcegroup "status" subcommand usage messages
static char *clrg_status_usage[] = {
	"%s status [<options>] [+ | <resourcegroup> ...]",
	(char *)0
};

// List of clresourcegroup "suspend" subcommand usage messages
static char *clrg_suspend_usage[] = {
	"%s suspend [<options>] + | <resourcegroup> ...",
	(char *)0
};

// List of clresourcegroup "switch" subcommand usage messages
static char *clrg_switch_usage[] = {
#if SOL_VERSION >= __s10
	"%s switch [-z <zone>] -n <node>[:<zone>][,...] [<options>] "
#else
	"%s switch -n <node>[,...] [<options>] "
#endif
	"+ | <resourcegroup> ...",
	(char *)0
};

// List of clresourcegroup "unmanage" subcommand usage messages
static char *clrg_unmanage_usage[] = {
	"%s unmanage [<options>] + | <resourcegroup> ...",
	(char *)0
};

// List of clresourcegroup "remaster" subcommand usage messages
static char *clrg_remaster_usage[] = {
	"%s remaster + | <resourcegroup> ...",
	(char *)0
};
//
// Option lists
//
// All option descriptions should be broken up into lines of no
// greater than 70 characters.  All but the trailing newline ('\n')
// itself should be included in the option descriptions found in these
// tables.
//

// List of options for the clresourcegroup "add-node" subcommand
static cloptions_t clrg_add_node_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
#if SOL_VERSION >= __s10
	{'n', "node", "<node>[:<zone>][,...]", 0,
#else
	{'n', "node", "<node>[,...]", 0,
#endif
	    CLDES_OPT_NODE
	},
	{'S', "scalable", 0, 0,
	    CLDES_OPT_SCALABLE
	},
	{'u', "suspended", 0, 0,
		CLDES_OPT_SUSPENDED
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
#if SOL_VERSION >= __s10
	{'z', "zone", "<zone>", 0,
	    CLDES_OPT_ZONE
	},
	{'Z', "zonecluster", "{global | <zone-cluster-name>}", 0,
	    CLDES_OPT_ZC_NAME
	},
#endif
	{0, 0, 0, 0, 0}
};

// List of options for the clresourcegroup "create" subcommand
static cloptions_t clrg_create_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'F', "force", "<force>", CLOPT_UNDOCUMENTED,
	    CLDES_OPT_FORCE
	},
	{'i', "input", "{- | <clconfiguration>}", 0,
	    CLDES_OPT_INPUT
	},
#if SOL_VERSION >= __s10
	{'n', "node", "<node>[:<zone>][,...]", 0,
#else
	{'n', "node", "<node>[,...]", 0,
#endif
	    CLDES_OPT_NODE
	},
	{'p', "property", "<name>=<value>", 0,
	    CLDES_OPT_RG_PROPERTY
	},
	{'S', "scalable", 0, 0,
	    CLDES_OPT_SCALABLE
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
#if SOL_VERSION >= __s10
	{'z', "zone", "<zone>", 0,
	    CLDES_OPT_ZONE
	},
	{'Z', "zonecluster", "{global | <zone-cluster-name>}", 0,
	    CLDES_OPT_ZC_NAME
	},
#endif
	{0, 0, 0, 0, 0}
};

// List of options for the clresourcegroup "delete" subcommand
static cloptions_t clrg_delete_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'F', "force", 0, 0,
	    CLDES_OPT_FORCE
	},
#if SOL_VERSION >= __s10
	{'Z', "zonecluster", "{global | <zone-cluster-name>}", 0,
	    CLDES_OPT_ZC_NAME
	},
#endif
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the clresourcegroup "evacuate" subcommand
static cloptions_t clrg_evacuate_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
#if SOL_VERSION >= __s10
	{'n', "node", "<node>[:<zone>][,...]", 0,
#else
	{'n', "node", "<node>[,...]", 0,
#endif
	    CLDES_OPT_NODE
	},
	{'T', "time", "<seconds>", 0,
	    CLDES_OPT_RG_TIME
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
#if SOL_VERSION >= __s10
	{'z', "zone", "<zone>", 0,
	    CLDES_OPT_ZONE
	},
	{'Z', "zonecluster", "{global | <zone-cluster-name>}", 0,
	    CLDES_OPT_ZC_NAME
	},
#endif
	{0, 0, 0, 0, 0}
};

// List of options for the clresourcegroup "export" subcommand
static cloptions_t clrg_export_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{'o', "output", "{- | <clconfiguration>}", 0,
	    CLDES_OPT_OUTPUT
	},
	{0, 0, 0, 0, 0}
};

// List of options for the clresourcegroup "list" subcommand
static cloptions_t clrg_list_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
#if SOL_VERSION >= __s10
	{'n', "node", "<node>[:<zone>][,...]", 0,
#else
	{'n', "node", "<node>[,...]", 0,
#endif
	    CLDES_OPT_NODE
	},
	{'r', "resource", "<resource>", CLOPT_ARG_LIST,
	    CLDES_OPT_RS
	},
	{'t', "type", "<resourcetype>", CLOPT_ARG_LIST,
	    CLDES_OPT_RT
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
#if SOL_VERSION >= __s10
	{'z', "zone", "<zone>", 0,
	    CLDES_OPT_ZONE
	},
	{'Z', "zonecluster", "{all | global | <zone-cluster-name>}", 0,
	    CLDES_OPT_ZC_NAME
	},
#endif
	{0, 0, 0, 0, 0}
};

// List of options for the clresourcegroup "manage" subcommand
static cloptions_t clrg_manage_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'u', "suspended", 0, 0,
		CLDES_OPT_SUSPENDED
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
#if SOL_VERSION >= __s10
	{'Z', "zonecluster", "{global | <zone-cluster-name>}", 0,
	    CLDES_OPT_ZC_NAME
	},
#endif
	{0, 0, 0, 0, 0}
};

// List of options for the clresourcegroup "set" subcommand
static cloptions_t clrg_set_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
#if SOL_VERSION >= __s10
	{'n', "node", "<node>[:<zone>][,...]", 0,
#else
	{'n', "node", "<node>[,...]", 0,
#endif
	    CLDES_OPT_NODE
	},
	{'p', "property", "<name>[+|-]=<value>", 0,
	    CLDES_OPT_RG_PROPERTY
	},
	{'u', "suspended", 0, 0,
		CLDES_OPT_SUSPENDED
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
#if SOL_VERSION >= __s10
	{'z', "zone", "<zone>", 0,
	    CLDES_OPT_ZONE
	},
	{'Z', "zonecluster", "{global | <zone-cluster-name>}", 0,
	    CLDES_OPT_ZC_NAME
	},
#endif
	{0, 0, 0, 0, 0}
};

// List of options for the clresourcegroup "offline" subcommand
static cloptions_t clrg_offline_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
#if SOL_VERSION >= __s10
	{'n', "node", "<node>[:<zone>][,...]", 0,
#else
	{'n', "node", "<node>[,...]", 0,
#endif
	    CLDES_OPT_NODE
	},
	{'u', "suspended", 0, 0,
		CLDES_OPT_SUSPENDED
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
#if SOL_VERSION >= __s10
	{'z', "zone", "<zone>", 0,
	    CLDES_OPT_ZONE
	},
	{'Z', "zonecluster", "{global | <zone-cluster-name>}", 0,
	    CLDES_OPT_ZC_NAME
	},
#endif
	{0, 0, 0, 0, 0}
};

// List of options for the clresourcegroup "online" subcommand
static cloptions_t clrg_online_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'e', "enable", 0, 0,
	    CLDES_OPT_ENABLE
	},
	{'M', "manage", 0, 0,
	    CLDES_OPT_MANAGE
	},
	{'m', "monitor", 0, 0,
	    CLDES_OPT_MONITOR
	},
#if SOL_VERSION >= __s10
	{'n', "node", "<node>[:<zone>][,...]", 0,
#else
	{'n', "node", "<node>[,...]", 0,
#endif
	    CLDES_OPT_NODE
	},
	{'u', "suspended", 0, 0,
		CLDES_OPT_SUSPENDED
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
#if SOL_VERSION >= __s10
	{'z', "zone", "<zone>", 0,
	    CLDES_OPT_ZONE
	},
	{'Z', "zonecluster", "{global | <zone-cluster-name>}", 0,
	    CLDES_OPT_ZC_NAME
	},
#endif
	{0, 0, 0, 0, 0}
};

// List of options for the clresourcegroup "quiesce" subcommand
static cloptions_t clrg_quiesce_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'k', "kill", 0, 0,
	    CLDES_OPT_KILL
	},
	{'u', "suspended", 0, 0,
		CLDES_OPT_SUSPENDED
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
#if SOL_VERSION >= __s10
	{'Z', "zonecluster", "{global | <zone-cluster-name>}", 0,
	    CLDES_OPT_ZC_NAME
	},
#endif
	{0, 0, 0, 0, 0}
};

// List of options for the clresourcegroup "remove-node" subcommand
static cloptions_t clrg_remove_node_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
#if SOL_VERSION >= __s10
	{'n', "node", "<node>[:<zone>][,...]", 0,
#else
	{'n', "node", "<node>[,...]", 0,
#endif
	    CLDES_OPT_NODE
	},
	{'u', "suspended", 0, 0,
		CLDES_OPT_SUSPENDED
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
#if SOL_VERSION >= __s10
	{'z', "zone", "<zone>", 0,
	    CLDES_OPT_ZONE
	},
	{'Z', "zonecluster", "{global | <zone-cluster-name>}", 0,
	    CLDES_OPT_ZC_NAME
	},
#endif
	{0, 0, 0, 0, 0}
};

// List of options for the clresourcegroup "restart" subcommand
static cloptions_t clrg_restart_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
#if SOL_VERSION >= __s10
	{'n', "node", "<node>[:<zone>][,...]", 0,
#else
	{'n', "node", "<node>[,...]", 0,
#endif
	    CLDES_OPT_NODE
	},
	{'u', "suspended", 0, 0,
		CLDES_OPT_SUSPENDED
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
#if SOL_VERSION >= __s10
	{'z', "zone", "<zone>", 0,
	    CLDES_OPT_ZONE
	},
	{'Z', "zonecluster", "{global | <zone-cluster-name>}", 0,
	    CLDES_OPT_ZC_NAME
	},
#endif
	{0, 0, 0, 0, 0}
};

// List of options for the clresourcegroup "resume" subcommand
static cloptions_t clrg_resume_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
#if SOL_VERSION >= __s10
	{'Z', "zonecluster", "{global | <zone-cluster-name>}", 0,
	    CLDES_OPT_ZC_NAME
	},
#endif
	{0, 0, 0, 0, 0}
};

// List of options for the clresourcegroup "show" subcommand
static cloptions_t clrg_show_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
#if SOL_VERSION >= __s10
	{'n', "node", "<node>[:<zone>][,...]", 0,
#else
	{'n', "node", "<node>[,...]", 0,
#endif
	    CLDES_OPT_NODE
	},
	{'p', "property", "<name>", CLOPT_ARG_LIST,
	    CLDES_OPT_RG_PROPERTIES
	},
	{'r', "resource", "<resource>", CLOPT_ARG_LIST,
	    CLDES_OPT_RS
	},
	{'t', "type", "<resourcetype>", CLOPT_ARG_LIST,
	    CLDES_OPT_RT
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
#if SOL_VERSION >= __s10
	{'z', "zone", "<zone>", 0,
	    CLDES_OPT_ZONE
	},
	{'Z', "zonecluster", "{all | global | <zone-cluster-name>}", 0,
	    CLDES_OPT_ZC_NAME
	},
#endif
	{0, 0, 0, 0, 0}
};

// List of options for the clresourcegroup "status" subcommand
static cloptions_t clrg_status_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
#if SOL_VERSION >= __s10
	{'n', "node", "<node>[:<zone>]", CLOPT_ARG_LIST,
#else
	{'n', "node", "<node>", CLOPT_ARG_LIST,
#endif
	    CLDES_OPT_NODE
	},
	{'r', "resource", "<resource>", CLOPT_ARG_LIST,
	    CLDES_OPT_RS
	},
	{'s', "state", "<state>", CLOPT_ARG_LIST,
	    CLDES_OPT_RG_STATE
	},
	{'t', "type", "<resourcetype>", CLOPT_ARG_LIST,
	    CLDES_OPT_RT
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
#if SOL_VERSION >= __s10
	{'z', "zone", "<zone>", 0,
	    CLDES_OPT_ZONE
	},
	{'Z', "zonecluster", "{all | global | <zone-cluster-name>}", 0,
	    CLDES_OPT_ZC_NAME
	},
#endif
	{0, 0, 0, 0, 0}
};

// List of options for the clresourcegroup "suspend" subcommand
static cloptions_t clrg_suspend_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'k', "kill", 0, 0,
	    CLDES_OPT_KILL
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
#if SOL_VERSION >= __s10
	{'Z', "zonecluster", "{global | <zone-cluster-name>}", 0,
	    CLDES_OPT_ZC_NAME
	},
#endif
	{0, 0, 0, 0, 0}
};

// List of options for the clresourcegroup "switch" subcommand
static cloptions_t clrg_switch_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'e', "enable", 0, 0,
	    CLDES_OPT_ENABLE
	},
	{'M', "manage", 0, 0,
	    CLDES_OPT_MANAGE
	},
	{'m', "monitor", 0, 0,
	    CLDES_OPT_MONITOR
	},
#if SOL_VERSION >= __s10
	{'n', "node", "<node>[:<zone>][,...]", 0,
#else
	{'n', "node", "<node>[,...]", 0,
#endif
	    CLDES_OPT_NODE
	},
	{'u', "suspended", 0, 0,
		CLDES_OPT_SUSPENDED
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
#if SOL_VERSION >= __s10
	{'z', "zone", "<zone>", 0,
	    CLDES_OPT_ZONE
	},
	{'Z', "zonecluster", "{global | <zone-cluster-name>}", 0,
	    CLDES_OPT_ZC_NAME
	},
#endif
	{0, 0, 0, 0, 0}
};

// List of options for the clresourcegroup "unmanage" subcommand
static cloptions_t clrg_unmanage_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'u', "suspended", 0, 0,
		CLDES_OPT_SUSPENDED
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
#if SOL_VERSION >= __s10
	{'Z', "zonecluster", "{global | <zone-cluster-name>}", 0,
	    CLDES_OPT_ZC_NAME
	},
#endif
	{0, 0, 0, 0, 0}
};

// List of options for the clresourcegroup "remaster" subcommand
static cloptions_t clrg_remaster_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'u', "suspended", 0, 0,
		CLDES_OPT_SUSPENDED
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
#if SOL_VERSION >= __s10
	{'Z', "zonecluster", "{global | <zone-cluster-name>}", 0,
	    CLDES_OPT_ZC_NAME
	},
#endif
	{0, 0, 0, 0, 0}
};
// List of subcommands for the "clresourcegroup" command
static clsubcommand_t clrg_subcommands[] = {
	{
		"add-node",
		CLSUB_DO_COMMAND_LOGGING | CLSUB_ZONE_CLUSTER_ALLOWED,
		(char *)CLRG_SUBCMD_ADD_NODE,
		CL_AUTH_CLUSTER_MODIFY,
		clrg_add_node_usage,
		clrg_add_node_optionslist,
		clrg_add_node_options
	},
	{
		"create",
		CLSUB_DO_COMMAND_LOGGING | CLSUB_ZONE_CLUSTER_ALLOWED,
		(char *)CLRG_SUBCMD_CREATE,
		CL_AUTH_CLUSTER_MODIFY,
		clrg_create_usage,
		clrg_create_optionslist,
		clrg_create_options
	},
	{
		"delete",
		CLSUB_DO_COMMAND_LOGGING | CLSUB_ZONE_CLUSTER_ALLOWED,
		(char *)CLRG_SUBCMD_DELETE,
		CL_AUTH_CLUSTER_MODIFY,
		clrg_delete_usage,
		clrg_delete_optionslist,
		clrg_delete_options
	},
	{
		"evacuate",
		CLSUB_DO_COMMAND_LOGGING | CLSUB_NON_GLOBAL_ZONE_ALLOWED |
		CLSUB_ZONE_CLUSTER_ALLOWED,
		(char *)CLRG_SUBCMD_EVACUATE,
		CL_AUTH_CLUSTER_ADMIN,
		clrg_evacuate_usage,
		clrg_evacuate_optionslist,
		clrg_evacuate_options
	},
	{
		"export",
		CLSUB_NON_GLOBAL_ZONE_ALLOWED,
		(char *)CLRG_SUBCMD_EXPORT,
		CL_AUTH_CLUSTER_READ,
		clrg_export_usage,
		clrg_export_optionslist,
		clrg_export_options
	},
	{
		"list",
		CLSUB_NON_GLOBAL_ZONE_ALLOWED | CLSUB_ZONE_CLUSTER_ALLOWED,
		(char *)CLRG_SUBCMD_LIST,
		CL_AUTH_CLUSTER_READ,
		clrg_list_usage,
		clrg_list_optionslist,
		clrg_list_options
	},
	{
		"manage",
		CLSUB_DO_COMMAND_LOGGING | CLSUB_NON_GLOBAL_ZONE_ALLOWED |
		CLSUB_ZONE_CLUSTER_ALLOWED,
		(char *)CLRG_SUBCMD_MANAGE,
		CL_AUTH_CLUSTER_ADMIN,
		clrg_manage_usage,
		clrg_manage_optionslist,
		clrg_manage_options
	},
	{
		"set",
		CLSUB_DO_COMMAND_LOGGING | CLSUB_NON_GLOBAL_ZONE_ALLOWED |
		CLSUB_ZONE_CLUSTER_ALLOWED,
		(char *)CLRG_SUBCMD_SET,
		CL_AUTH_CLUSTER_MODIFY,
		clrg_set_usage,
		clrg_set_optionslist,
		clrg_set_options
	},
	{
		"offline",
		CLSUB_DO_COMMAND_LOGGING | CLSUB_NON_GLOBAL_ZONE_ALLOWED |
		CLSUB_ZONE_CLUSTER_ALLOWED,
		(char *)CLRG_SUBCMD_OFFLINE,
		CL_AUTH_CLUSTER_ADMIN,
		clrg_offline_usage,
		clrg_offline_optionslist,
		clrg_offline_options
	},
	{
		"online",
		CLSUB_DO_COMMAND_LOGGING | CLSUB_NON_GLOBAL_ZONE_ALLOWED |
		CLSUB_ZONE_CLUSTER_ALLOWED,
		(char *)CLRG_SUBCMD_ONLINE,
		CL_AUTH_CLUSTER_ADMIN,
		clrg_online_usage,
		clrg_online_optionslist,
		clrg_online_options
	},
	{
		"quiesce",
		CLSUB_DO_COMMAND_LOGGING | CLSUB_NON_GLOBAL_ZONE_ALLOWED |
		CLSUB_ZONE_CLUSTER_ALLOWED,
		(char *)CLRG_SUBCMD_QUIESCE,
		CL_AUTH_CLUSTER_ADMIN,
		clrg_quiesce_usage,
		clrg_quiesce_optionslist,
		clrg_quiesce_options
	},
	{
		"remove-node",
		CLSUB_DO_COMMAND_LOGGING | CLSUB_ZONE_CLUSTER_ALLOWED,
		(char *)CLRG_SUBCMD_REMOVE_NODE,
		CL_AUTH_CLUSTER_MODIFY,
		clrg_remove_node_usage,
		clrg_remove_node_optionslist,
		clrg_remove_node_options
	},
	{
		"restart",
		CLSUB_DO_COMMAND_LOGGING | CLSUB_NON_GLOBAL_ZONE_ALLOWED |
		CLSUB_ZONE_CLUSTER_ALLOWED,
		(char *)CLRG_SUBCMD_RESTART,
		CL_AUTH_CLUSTER_ADMIN,
		clrg_restart_usage,
		clrg_restart_optionslist,
		clrg_restart_options
	},
	{
		"resume",
		CLSUB_DO_COMMAND_LOGGING | CLSUB_NON_GLOBAL_ZONE_ALLOWED |
		CLSUB_ZONE_CLUSTER_ALLOWED,
		(char *)CLRG_SUBCMD_RESUME,
		CL_AUTH_CLUSTER_ADMIN,
		clrg_resume_usage,
		clrg_resume_optionslist,
		clrg_resume_options
	},
	{
		"show",
		CLSUB_NON_GLOBAL_ZONE_ALLOWED | CLSUB_ZONE_CLUSTER_ALLOWED,
		(char *)CLRG_SUBCMD_SHOW,
		CL_AUTH_CLUSTER_READ,
		clrg_show_usage,
		clrg_show_optionslist,
		clrg_show_options
	},
	{
		"status",
		CLSUB_NON_GLOBAL_ZONE_ALLOWED | CLSUB_ZONE_CLUSTER_ALLOWED,
		(char *)CLRG_SUBCMD_STATUS,
		CL_AUTH_CLUSTER_READ,
		clrg_status_usage,
		clrg_status_optionslist,
		clrg_status_options
	},
	{
		"suspend",
		CLSUB_DO_COMMAND_LOGGING | CLSUB_NON_GLOBAL_ZONE_ALLOWED |
		CLSUB_ZONE_CLUSTER_ALLOWED,
		(char *)CLRG_SUBCMD_SUSPEND,
		CL_AUTH_CLUSTER_ADMIN,
		clrg_suspend_usage,
		clrg_suspend_optionslist,
		clrg_suspend_options
	},
	{
		"switch",
		CLSUB_DO_COMMAND_LOGGING | CLSUB_NON_GLOBAL_ZONE_ALLOWED |
		CLSUB_ZONE_CLUSTER_ALLOWED,
		(char *)CLRG_SUBCMD_SWITCH,
		CL_AUTH_CLUSTER_ADMIN,
		clrg_switch_usage,
		clrg_switch_optionslist,
		clrg_switch_options
	},
	{
		"unmanage",
		CLSUB_DO_COMMAND_LOGGING | CLSUB_NON_GLOBAL_ZONE_ALLOWED |
		CLSUB_ZONE_CLUSTER_ALLOWED,
		(char *)CLRG_SUBCMD_UNMANAGE,
		CL_AUTH_CLUSTER_ADMIN,
		clrg_unmanage_usage,
		clrg_unmanage_optionslist,
		clrg_unmanage_options
	},
	{
		"remaster",
		CLSUB_DO_COMMAND_LOGGING | CLSUB_NON_GLOBAL_ZONE_ALLOWED |
		CLSUB_ZONE_CLUSTER_ALLOWED,
		(char *)CLRG_SUBCMD_REMASTER,
		CL_AUTH_CLUSTER_ADMIN,
		clrg_remaster_usage,
		clrg_remaster_optionslist,
		clrg_remaster_options
	},

	{0, 0, 0, 0, 0, 0, 0}
};

// Initialization properties for the "clresource" command
clcommandinit_t clrginit = {
	CLCOMMANDS_COMMON_VERSION,
	(char *)CLRG_CMD_DESC,
	clrg_usage,
	clrg_subcommands
};
