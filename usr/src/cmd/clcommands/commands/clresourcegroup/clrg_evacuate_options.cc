//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clrg_evacuate_options.cc	1.6	08/07/24 SMI"

//
// Process clresourcegroup "evacuate" subcommand options
//

#include "clrg.h"

//
// clresourcegroup "evacuate" options
//
int
clrg_evacuate_options(ClCommand &cmd)
{
	int i;
	int c;
	int wildcard;
	int num_operands;
	int errcount = 0;
	char *option = 0;
	int option_index = 0;
	char *badoption;
	char *shortopts;
	struct option *longopts;
	ValueList operands;
	optflgs_t optflgs = 0;

	ValueList nodes;
	char *buffer;
	char *zone = (char *)0;
	int zflg = 0;
	int zcflg = 0;
	char *zc_name = NULL;

	char *endp;
	int itimeout = 60;
	long ltimeout;
	char *stimeout;

	// Get the short and long option lists
	shortopts = clsubcommand_get_shortoptions(cmd);
	longopts = clsubcommand_get_longoptions(cmd);

	// This subcommand should have options
	if (!shortopts || !longopts) {
		clerror("Aborting - subcommand initialization failed.\n");
		abort();
	}

	optind = 2;
	while ((c = getopt_clip(cmd.argc, cmd.argv, shortopts, longopts,
	    &option_index)) != -1) {

		// Get the option
		delete option;
		option = clcommand_option(optopt, cmd.argv[optind - 1]);

		// Switch on option letter
		switch (c) {
		case 'v':
			// Verbose option
			cmd.isVerbose = 1;
			optflgs |= vflg;
			break;

		case '?':
			// Help option?
			if ((strcmp(option, "-?") == 0) ||
			    (strcmp(option, "--help") == 0)) {
				optflgs |= hflg;
				break;
			}

			// Or, unrecognized option?
			clerror("Unrecognized option - \"%s\".\n", option);
			++errcount;
			break;

		case 'n':
			// Nodes
			nodes.add(1, optarg);
			break;

		case 'T':
			// Timeout
			stimeout = optarg;
			ltimeout = strtol(stimeout, &endp, 10);
			if (*endp != NULL) {
				clerror("Invalid argument specified with "
				    "option \"-T\".\n");
				++errcount;
				break;
			}
			if ((ltimeout > MAX_RG_EVACUATE_TIMEOUT) ||
			    (ltimeout < MIN_RG_EVACUATE_TIMEOUT)) {
				clerror("Value specified with \"-T\" option "
				    "is out of range.\n");
				++errcount;
				break;
			}
			itimeout = (int)ltimeout;
			break;

#if SOL_VERSION >= __s10
		case 'z':
			// Zone
			zone = optarg;
			zflg++;
			break;

		case 'Z':
			// zone cluster name
			optflgs |= Zflg;
			zc_name = optarg;
			zcflg++;
			break;
#endif

		case ':':
			// Missing argument
			clerror("Option \"%s\" requires an argument.\n",
			    option);
			++errcount;
			break;

		default:
			//
			// We should never get here.
			// If we do, it means that there
			// is an option in our option list
			// for which there is no case statement.
			// We either have a bad option list or
			// a bad case statement.
			//
			clerror("Unexpected option - \"%s\".\n", option);
			++errcount;
			break;
		}
	}
	delete option;
	option = (char *)0;

	// Errors?
	if (errcount) {
		clcommand_usage(cmd, cmd.getSubCommandName(), errcount);
		return (CL_EINVAL);
	}

	// Just print help?
	if (optflgs & hflg) {
		clcommand_help(cmd, cmd.getSubCommandName());
		return (0);
	}

	// Check for multiple options
	if (zflg > 1) {
		clerror("You can specify the \"-z\" option only once.\n");
		++errcount;
	}
	if (zcflg > 1) {
		clerror("You can specify the \"-Z\" option only once.\n");
		++errcount;
	}
	if (zflg && zcflg) {
		clerror("You cannot use both \"-z\" and \"-Z\" options "
		    "together.\n");
		++errcount;
	}
	// Errors?
	if (errcount) {
		clcommand_usage(cmd, cmd.getSubCommandName(), errcount);
		return (CL_EINVAL);
	}

	// If there is no -n, there must not be a -z.
	if (!nodes.getSize() && zone) {
		clerror("You cannot use the \"-z\" option without "
		    "a \"-n\" option.\n");
		++errcount;
	}

	// evacuate operation needs atleast one node
	if (!nodes.getSize()) {
		clerror("The \"-n\" option must be specified with this "
		    "subcommand.\n");
		++errcount;
	}

	// evacuate operation accepts only one node at present
	if (nodes.getSize() != 1) {
		clerror("You can specify only one node for evacuation.\n");
		++errcount;
	}
	// Errors?
	if (errcount) {
		clcommand_usage(cmd, cmd.getSubCommandName(), errcount);
		return (CL_EINVAL);
	}

	// Only one wildcard can be our possible operand
	wildcard = 1;
	num_operands = 0;
	for (i = optind;  i < cmd.argc;  ++i) {
		num_operands++;
		if (strcmp(cmd.argv[i], CLCOMMANDS_WILD_OPERAND) != 0) {
			wildcard = 0;
			break;
		}
	}

	if ((num_operands > 1) || (wildcard == 0)) {
		++errcount;
		clerror("In this subcommand, \"%s\" is the only operand "
		    "allowed.\n", CLCOMMANDS_WILD_OPERAND);
	}

	// Create the node list, appending zone component as needed
	if (nodes.getSize()) {
		for (nodes.start(); nodes.end() != 1; nodes.next()) {
#if SOL_VERSION < __s10
			if (strchr(nodes.getValue(), ':')) {
				++errcount;
				clerror("You cannot specify zones as part "
				    "of node names in this operating system "
				    "release.\n");
				break;
			}
#endif
			// if -Z was specified <node>:<zone> should not be
			// allowed
			if (zc_name &&
				strchr(nodes.getValue(), ':')) {
				++errcount;
				clerror("You cannot specify zones as part"
					" of node names while specifying the "
					"\"-Z\" option.\n");
				break;
			}

			if (zone && !strchr(nodes.getValue(), ':')) {
				nodes.append(zone, ':');
			}
		}
	}

	// Errors?
	if (errcount) {
		clcommand_usage(cmd, cmd.getSubCommandName(), errcount);
		return (CL_EINVAL);
	}

	// Perform clresourcegroup "evacuate" operation
	return (clrg_evacuate(cmd, optflgs, itimeout, nodes, zc_name));
}
