//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clrg_list_options.cc	1.7	08/07/25 SMI"

//
// Process clresourcegroup "list" subcommand options
//

#include "clrg.h"

//
// clresourcegroup "list" options
//
int
clrg_list_options(ClCommand &cmd)
{
	int i;
	int c;
	int wildcard;
	int num_operands;
	int errcount = 0;
	char *option = 0;
	int option_index = 0;
	char *badoption;
	char *shortopts;
	struct option *longopts;
	ValueList operands;
	optflgs_t optflgs = 0;

	ValueList resources;
	ValueList resourcetypes;
	ValueList nodes;
	char *buffer;
	char *zone = (char *)0;
	int zflg = 0;
	ValueList zc_names;

	char *clconfiguration = (char *)0;

	// Get the short and long option lists
	shortopts = clsubcommand_get_shortoptions(cmd);
	longopts = clsubcommand_get_longoptions(cmd);

	// This subcommand should have options
	if (!shortopts || !longopts) {
		clerror("Aborting - subcommand initialization failed.\n");
		abort();
	}

	optind = 2;
	while ((c = getopt_clip(cmd.argc, cmd.argv, shortopts, longopts,
	    &option_index)) != -1) {

		// Get the option
		delete option;
		option = clcommand_option(optopt, cmd.argv[optind - 1]);

		// Switch on option letter
		switch (c) {
		case 'v':
			// Verbose option
			cmd.isVerbose = 1;
			optflgs |= vflg;
			break;

		case '?':
			// Help option?
			if ((strcmp(option, "-?") == 0) ||
			    (strcmp(option, "--help") == 0)) {
				optflgs |= hflg;
				break;
			}

			// Or, unrecognized option?
			clerror("Unrecognized option - \"%s\".\n", option);
			++errcount;
			break;

		case 'n':
			// Nodes
			nodes.add(1, optarg);
			break;

		case 'r':
			// Resources
			resources.add(1, optarg);
			break;

		case 't':
			// Resource types
			resourcetypes.add(1, optarg);
			break;

#if SOL_VERSION >= __s10
		case 'z':
			// Zone
			zone = optarg;
			zflg++;
			break;

		case 'Z':
			// Zone Cluster
			zc_names.add(optarg);
			break;
#endif

		case ':':
			// Missing argument
			clerror("Option \"%s\" requires an argument.\n",
			    option);
			++errcount;
			break;

		default:
			//
			// We should never get here.
			// If we do, it means that there
			// is an option in our option list
			// for which there is no case statement.
			// We either have a bad option list or
			// a bad case statement.
			//
			clerror("Unexpected option - \"%s\".\n", option);
			++errcount;
			break;
		}
	}
	delete option;
	option = (char *)0;

	// Errors?
	if (errcount) {
		clcommand_usage(cmd, cmd.getSubCommandName(), errcount);
		return (CL_EINVAL);
	}

	// Just print help?
	if (optflgs & hflg) {
		clcommand_help(cmd, cmd.getSubCommandName());
		return (0);
	}

	// Check for multiple options
	if (zflg > 1) {
		clerror("You can specify the \"-z\" option only once.\n");
		++errcount;
	}

	// If there is no -n, there must not be a -z.
	if (!nodes.getSize() && zone) {
		clerror("You cannot use the \"-z\" option without "
		    "a \"-n\" option.\n");
		++errcount;
	}

	// Errors?
	if (errcount) {
		clcommand_usage(cmd, cmd.getSubCommandName(), errcount);
		return (CL_EINVAL);
	}

	// If wildcard operand specified, there should be no other operand.
	wildcard = 0;
	num_operands = 0;
	for (i = optind;  i < cmd.argc;  ++i) {
		num_operands++;
		if (strcmp(cmd.argv[i], CLCOMMANDS_WILD_OPERAND) == 0) {
			++wildcard;
		}

	}

	if ((wildcard) && (num_operands > 1)) {
		++errcount;
		clerror("If you specify an operand, it must be either a "
		    "\"%s\" or a list of resource group names.\n",
		    CLCOMMANDS_WILD_OPERAND);
	}

	// Errors?
	if (errcount) {
		clcommand_usage(cmd, cmd.getSubCommandName(), errcount);
		return (CL_EINVAL);
	}

	// Update the operands list
	if (wildcard) {
		operands.add(CLCOMMANDS_WILD_OPERAND);
	} else {
		for (i = optind;  i < cmd.argc;  ++i) {
			operands.add(cmd.argv[i]);
		}
	}

	// Create the node list, appending zone component as needed
	if (nodes.getSize()) {
		for (nodes.start(); nodes.end() != 1; nodes.next()) {
#if SOL_VERSION < __s10
			if (strchr(nodes.getValue(), ':')) {
				++errcount;
				clerror("You cannot specify zones as part "
				    "of node names in this operating system "
				    "release.\n");
				break;
			}
#endif
			if (zone && !strchr(nodes.getValue(), ':')) {
				nodes.append(zone, ':');
			}
		}
	}

	// Errors?
	if (errcount) {
		clcommand_usage(cmd, cmd.getSubCommandName(), errcount);
		return (CL_EINVAL);
	}

	// Check multiple zc names
	if ((clcommand_zc_usage_check(zc_names)) != CL_NOERR) {
		++errcount;
		clcommand_usage(cmd, cmd.getSubCommandName(), errcount);
		return (CL_EINVAL);
	}

	// Errors?
	if (errcount) {
		clcommand_usage(cmd, cmd.getSubCommandName(), errcount);
		return (CL_EINVAL);
	}

	// Perform clresourcegroup "list" operation
	return (clrg_list(cmd, operands, optflgs, resources, resourcetypes,
	    nodes, zc_names));
}
