//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef _CLRG_H
#define	_CLRG_H

#pragma ident	"@(#)clrg.h	1.4	08/05/20 SMI"

//
// This is the header file for the clresourcegroup(1CL) command
//

#define	CLRG_SUBCMD_ADD_NODE	\
	GETTEXT("Add nodes to a resource group nodelist")
#define	CLRG_SUBCMD_CREATE	\
	GETTEXT("Create resource groups")
#define	CLRG_SUBCMD_DELETE	\
	GETTEXT("Delete resource groups")
#define	CLRG_SUBCMD_EVACUATE	\
	GETTEXT("Evacuate resource groups from nodes")
#define	CLRG_SUBCMD_EXPORT	\
	GETTEXT("Export resource group configuration")
#define	CLRG_SUBCMD_LIST	\
	GETTEXT("List resource groups")
#define	CLRG_SUBCMD_MANAGE	\
	GETTEXT("Bring resource groups to the managed state")
#define	CLRG_SUBCMD_SET		\
	GETTEXT("Set resource group properties")
#define	CLRG_SUBCMD_OFFLINE	\
	GETTEXT("Take resource groups to the offline state")
#define	CLRG_SUBCMD_ONLINE	\
	GETTEXT("Take resource groups to the online state")
#define	CLRG_SUBCMD_QUIESCE	\
	GETTEXT("Bring resource groups to the quiescent state")
#define	CLRG_SUBCMD_REMOVE_NODE	\
	GETTEXT("Remove nodes from a resource group nodelist")
#define	CLRG_SUBCMD_RESTART	\
	GETTEXT("Restart resource groups")
#define	CLRG_SUBCMD_RESUME	\
	GETTEXT("Resume suspended resource groups")
#define	CLRG_SUBCMD_SHOW	\
	GETTEXT("Display resource groups and their properties")
#define	CLRG_SUBCMD_STATUS	\
	GETTEXT("Display the status of resource groups")
#define	CLRG_SUBCMD_SUSPEND	\
	GETTEXT("Suspend resource groups")
#define	CLRG_SUBCMD_SWITCH	\
	GETTEXT("Specify nodes where resource groups are mastered")
#define	CLRG_SUBCMD_UNMANAGE	\
	GETTEXT("Bring resource groups to the unmanaged state")
#define	CLRG_CMD_DESC		\
	GETTEXT("Manage resource groups for Sun Cluster data services")
#define	CLRG_SUBCMD_REMASTER		\
	GETTEXT("Remaster the resource groups to their preferred primaries")

#include <sys/sol_version.h>

#include "clcommands.h"
#include "clresourcegroup.h"

#include "ClCommand.h"

extern clcommandinit_t clrginit;

// Subcommand options
extern int clrg_add_node_options(ClCommand &cmd);
extern int clrg_create_options(ClCommand &cmd);
extern int clrg_delete_options(ClCommand &cmd);
extern int clrg_evacuate_options(ClCommand &cmd);
extern int clrg_export_options(ClCommand &cmd);
extern int clrg_list_options(ClCommand &cmd);
extern int clrg_manage_options(ClCommand &cmd);
extern int clrg_set_options(ClCommand &cmd);
extern int clrg_offline_options(ClCommand &cmd);
extern int clrg_online_options(ClCommand &cmd);
extern int clrg_quiesce_options(ClCommand &cmd);
extern int clrg_remove_node_options(ClCommand &cmd);
extern int clrg_restart_options(ClCommand &cmd);
extern int clrg_resume_options(ClCommand &cmd);
extern int clrg_show_options(ClCommand &cmd);
extern int clrg_status_options(ClCommand &cmd);
extern int clrg_suspend_options(ClCommand &cmd);
extern int clrg_switch_options(ClCommand &cmd);
extern int clrg_unmanage_options(ClCommand &cmd);
extern int clrg_remaster_options(ClCommand &cmd);

#define	MAX_RG_EVACUATE_TIMEOUT 65535
#define	MIN_RG_EVACUATE_TIMEOUT 0
#endif /* _CLRG_H */
