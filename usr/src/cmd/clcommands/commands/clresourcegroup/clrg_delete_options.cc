//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clrg_delete_options.cc	1.7	08/07/25 SMI"

//
// Process clresourcegroup "delete" subcommand options
//

#include "clrg.h"

//
// clresourcegroup "delete" options
//
int
clrg_delete_options(ClCommand &cmd)
{
	int i;
	int c;
	int wildcard;
	int num_operands;
	int errcount = 0;
	char *option = 0;
	int option_index = 0;
	char *badoption;
	char *shortopts;
	struct option *longopts;
	ValueList operands;
	optflgs_t optflgs = 0;
	char *zc_names;
	int zcflg;
	char *zc_name = (char *)0;

	// Get the short and long option lists
	shortopts = clsubcommand_get_shortoptions(cmd);
	longopts = clsubcommand_get_longoptions(cmd);

	// This subcommand should have options
	if (!shortopts || !longopts) {
		clerror("Aborting - subcommand initialization failed.\n");
		abort();
	}

	zcflg = 0;
	optind = 2;
	while ((c = getopt_clip(cmd.argc, cmd.argv, shortopts, longopts,
	    &option_index)) != -1) {

		// Get the option
		delete option;
		option = clcommand_option(optopt, cmd.argv[optind - 1]);

		// Switch on option letter
		switch (c) {
		case 'v':
			// Verbose option
			cmd.isVerbose = 1;
			optflgs |= vflg;
			break;

		case '?':
			// Help option?
			if ((strcmp(option, "-?") == 0) ||
			    (strcmp(option, "--help") == 0)) {
				optflgs |= hflg;
				break;
			}

			// Or, unrecognized option?
			clerror("Unrecognized option - \"%s\".\n", option);
			++errcount;
			break;

		case 'F':
			// Force
			optflgs |= Fflg;
			break;

#if SOL_VERSION >= __s10
		case 'Z':
			// zone cluster name
			optflgs |= Zflg;
			zc_name = optarg;
			zcflg++;
			break;
#endif

		case ':':
			// Missing argument
			clerror("Option \"%s\" requires an argument.\n",
			    option);
			++errcount;
			break;

		default:
			//
			// We should never get here.
			// If we do, it means that there
			// is an option in our option list
			// for which there is no case statement.
			// We either have a bad option list or
			// a bad case statement.
			//
			clerror("Unexpected option - \"%s\".\n", option);
			++errcount;
			break;
		}
	}
	delete option;
	option = (char *)0;

	// Errors?
	if (errcount) {
		clcommand_usage(cmd, cmd.getSubCommandName(), errcount);
		return (CL_EINVAL);
	}

	// Just print help?
	if (optflgs & hflg) {
		clcommand_help(cmd, cmd.getSubCommandName());
		return (0);
	}

	// If wildcard operand spcified, there should be no other operand
	wildcard = 0;
	num_operands = 0;
	for (i = optind;  i < cmd.argc;  ++i) {
		num_operands++;
		if (strcmp(cmd.argv[i], CLCOMMANDS_WILD_OPERAND) == 0) {
			++wildcard;
		}
	}
	if ((wildcard) && (num_operands > 1)) {
		++errcount;
		clerror("You can either specify a \"%s\" or a list of "
		    "resource group names as operands.\n",
		    CLCOMMANDS_WILD_OPERAND);
	}

	// Update the operands list
	if (wildcard) {
		operands.add(CLCOMMANDS_WILD_OPERAND);
	} else {
		for (i = optind;  i < cmd.argc;  ++i)
			operands.add(cmd.argv[i]);
	}

	// There must be at least one resource group
	if (operands.getSize() == 0) {
		++errcount;
		clerror("You must specify either \"%s\" or a "
		    "list of resource groups to delete.\n",
		    CLCOMMANDS_WILD_OPERAND);
	}

	// -Z should be specified only once.
	if (zcflg > 1) {
		clerror("You can specify the -Z option only once.\n");
		++errcount;
	}

	// If -Z flag was specified, none of the resource groups
	// should be scoped.
	// Also if -Z was specified, we should scope each resource
	// group with the zone cluster name in the format
	// <zone cluster name>:<rg name>
	if (zcflg) {
		for (operands.start(); !operands.end(); operands.next()) {

			if (strchr(operands.getValue(), ':')) {
				++errcount;
				clerror("You cannot specify zone cluster name"
					" as part of resource group name while"
					" using the \"-Z\" option.\n");
				break;
			}

			// If we are here, we have to prepend the RG name with
			// the zone cluster name
			operands.prepend(zc_name,
					ZC_AND_OBJ_NAME_SEPARATER_CHAR);
		}
	}

	// Errors?
	if (errcount) {
		clcommand_usage(cmd, cmd.getSubCommandName(), errcount);
		return (CL_EINVAL);
	}

	// Perform clresourcegroup "delete" operation
	return (clrg_delete(cmd, operands, optflgs));
}
