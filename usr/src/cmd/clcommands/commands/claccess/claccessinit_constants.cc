/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)claccessinit_constants.cc	1.3	08/05/20 SMI"

//
// Initialization data for claccess(1CL)
//

#include "claccess_cmd.h"

//
// Usage Messages
//
// All usage message lines should begin with a single "%s" for
// the command name.   Lines must never include more than a single "%s".
//

// List of "claccess" command usage message
static char *claccess_usage [] = {
	"%s <subcommand> [<options>]",
	"%s [<subcommand>]  -? | --help",
	"%s  -V | --version",
	(char *)0
};

// List of claccess "allow" subcommand usage messages
static char *claccess_allow_usage[] = {
	"%s allow -h <hostname> ...",
	(char *)0
};

// List of claccess "allow-all" subcommand usage messages
static char *claccess_allow_all_usage[] = {
	"%s allow-all",
	(char *)0
};

// List of claccess "deny" subcommand usage messages
static char *claccess_deny_usage[] = {
	"%s deny -h <hostname> ...",
	(char *)0
};

// List of claccess "deny-all" subcommand usage messages
static char *claccess_deny_all_usage[] = {
	"%s deny-all",
	(char *)0
};

// List of claccess "list" subcommand usage messages
static char *claccess_list_usage[] = {
	"%s list",
	(char *)0
};

// List of claccess "set" subcommand usage messages
static char *claccess_set_usage[] = {
	"%s set -p <name>=<value>",
	(char *)0
};

// List of claccess "show" subcommand usage messages
static char *claccess_show_usage[] = {
	"%s show",
	(char *)0
};

//
// Option lists
//
// All option descriptions should be broken up into lines of no
// greater than 70 characters.  All but the trailing newline ('\n')
// itself should be included in the option descriptions found in these
// tables.
//

// List of options for the claccess "allow" subcommand
static cloptions_t claccess_allow_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'h', "host", "<host>", CLOPT_ARG_LIST,
	    CLDES_OPT_NODE
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the claccess "allow-all" subcommand
static cloptions_t claccess_allow_all_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the claccess "deny" subcommand
static cloptions_t claccess_deny_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'h', "host", "<host>", CLOPT_ARG_LIST,
	    CLDES_OPT_NODE
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the claccess "deny-all" subcommand
static cloptions_t claccess_deny_all_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the claccess "list" subcommand
static cloptions_t claccess_list_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the claccess "set" subcommand
static cloptions_t claccess_set_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'p', "property", "<name>=<value>", CLOPT_ARG_LIST,
	    CLDES_OPT_PROPERTY
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the claccess "show" subcommand
static cloptions_t claccess_show_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of subcommands for the "claccess" command
static clsubcommand_t claccess_subcommands[] = {
	{
		"allow",
		CLSUB_DEFAULT,
		(char *)CLA_SUBCMD_ALLOW,
		CL_AUTH_CLUSTER_MODIFY,
		claccess_allow_usage,
		claccess_allow_optionslist,
		claccess_allow_options
	},
	{
		"allow-all",
		CLSUB_DEFAULT,
		(char *)CLA_SUBCMD_ALLOW_ALL,
		CL_AUTH_CLUSTER_MODIFY,
		claccess_allow_all_usage,
		claccess_allow_all_optionslist,
		claccess_allow_all_options
	},
	{
		"deny",
		CLSUB_DEFAULT,
		(char *)CLA_SUBCMD_DENY,
		CL_AUTH_CLUSTER_MODIFY,
		claccess_deny_usage,
		claccess_deny_optionslist,
		claccess_deny_options
	},
	{
		"deny-all",
		CLSUB_DEFAULT,
		(char *)CLA_SUBCMD_DENY_ALL,
		CL_AUTH_CLUSTER_MODIFY,
		claccess_deny_all_usage,
		claccess_deny_all_optionslist,
		claccess_deny_all_options
	},
	{
		"list",
		CLSUB_DEFAULT,
		(char *)CLA_SUBCMD_LIST,
		CL_AUTH_CLUSTER_READ,
		claccess_list_usage,
		claccess_list_optionslist,
		claccess_list_options
	},
	{
		"set",
		CLSUB_DEFAULT,
		(char *)CLA_SUBCMD_SET,
		CL_AUTH_CLUSTER_MODIFY,
		claccess_set_usage,
		claccess_set_optionslist,
		claccess_set_options
	},
	{
		"show",
		CLSUB_DEFAULT,
		(char *)CLA_SUBCMD_SHOW,
		CL_AUTH_CLUSTER_READ,
		claccess_show_usage,
		claccess_show_optionslist,
		claccess_show_options
	},
	{0, 0, 0, 0, 0, 0, 0}
};

// Initialization properties for the "claccess" command
clcommandinit_t claccessinit = {
	CLCOMMANDS_COMMON_VERSION,
	(char *)CLA_CMD_DESC,
	claccess_usage,
	claccess_subcommands
};
