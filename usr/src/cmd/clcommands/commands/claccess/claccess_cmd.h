//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef	_CLACCESS_CMD_H
#define	_CLACCESS_CMD_H

#pragma ident	"@(#)claccess_cmd.h	1.3	08/05/20 SMI"

//
// Header file for command processing functions for claccess(1M) command
//

// Table text which needs to be translated
#define	CLA_OPT_AUTHPROTOCOL	\
	GETTEXT("Specify an authentication protocol.")
#define	CLA_SUBCMD_ALLOW	\
	GETTEXT("Allow specified hostnames")
#define	CLA_SUBCMD_ALLOW_ALL	\
	GETTEXT("Allow all hostnames")
#define	CLA_SUBCMD_DENY		\
	GETTEXT("Deny specified hostnames")
#define	CLA_SUBCMD_DENY_ALL	\
	GETTEXT("Deny all hostnames")
#define	CLA_SUBCMD_LIST		\
	GETTEXT("List hostnames that can join/leave the cluster")
#define	CLA_SUBCMD_SET		\
	GETTEXT("Set access properties")
#define	CLA_SUBCMD_SHOW		\
	GETTEXT("Show hostnames and their access properties")
#define	CLA_CMD_DESC		\
	GETTEXT("Manage cluster access policies")

#include <claccess.h>
extern clcommandinit_t claccessinit;

// Subcommand options
extern clerrno_t claccess_allow_options(ClCommand &cmd);
extern clerrno_t claccess_allow_all_options(ClCommand &cmd);
extern clerrno_t claccess_deny_options(ClCommand &cmd);
extern clerrno_t claccess_deny_all_options(ClCommand &cmd);
extern clerrno_t claccess_list_options(ClCommand &cmd);
extern clerrno_t claccess_set_options(ClCommand &cmd);
extern clerrno_t claccess_show_options(ClCommand &cmd);

#endif /* _CLACCESS_CMD_H */
