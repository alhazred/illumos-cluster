//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident   "@(#)clsnmpmibinit_constants.cc	1.3	08/05/20 SMI"

//
// Initialization data for clsnmpmib(1CL)
//

#include "clsnmpmib_cmd.h"

extern clcommandinit_t clsnmpmibinit;

//
// Usage Messages
//
// All usage message lines should begin with a single "%s" for
// the command name.   Lines must never include more than a single "%s".
//

// List of "clsnmpmib" command usage message
static char *clsnmpmib_usage [] = {
	"%s <subcommand> [<options>] [+ | <snmpmib> ...]",
	"%s [<subcommand>]  -? | --help",
	"%s  -V | --version",
	(char *)0
};

static char *clsnmpmib_disable_usage[] = {
	"%s disable [<options>] + | <snmpmib> ...",
	(char *)0
};

// List of clsnmpmib "enable" subcommand usage messages
static char *clsnmpmib_enable_usage[] = {
	"%s enable [<options>] + | <snmpmib> ...",
	(char *)0
};

// List of clsnmpmib "export" subcommand usage messages
static char *clsnmpmib_export_usage[] = {
	"%s export [<options>] [+ | <snmpmib> ...]",
	(char *)0
};

// List of clsnmpmib "list" subcommand usage messages
static char *clsnmpmib_list_usage[] = {
	"%s list [<options>] [+ | <snmpmib> ...]",
	(char *)0
};

// List of clsnmpmib "set" subcommand usage messages
static char *clsnmpmib_set_usage[] = {
	"%s set -p <name>=<value> [<options>] + | <snmpmib> ...",
	(char *)0
};

// List of clsnmpmib "show" subcommand usage messages
static char *clsnmpmib_show_usage[] = {
	"%s show [<options>] [+ | <snmpmib> ...]",
	(char *)0
};

//
// Option lists
//
// All option descriptions should be broken up into lines of no
// greater than 70 characters.  All but the trailing newline ('\n')
// itself should be included in the option descriptions found in these
// tables.
//

// List of options for the clsnmpmib "disable" subcommand
static cloptions_t clsnmpmib_disable_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'n', "node", "<node>", CLOPT_ARG_LIST,
	    CLDES_OPT_NODE
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the clsnmpmib "enable" subcommand
static cloptions_t clsnmpmib_enable_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'n', "node", "<node>", CLOPT_ARG_LIST,
	    CLDES_OPT_NODE
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the clsnmpmib "export" subcommand
static cloptions_t clsnmpmib_export_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'n', "node", "<node>", CLOPT_ARG_LIST,
	    CLDES_OPT_NODE
	},
	{'o', "output", "{- | <clconfiguration>}", 0,
	    CLDES_OPT_OUTPUT
	},
	{0, 0, 0, 0, 0}
};

// List of options for the clsnmpmib "list" subcommand
static cloptions_t clsnmpmib_list_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'n', "node", "<node>", CLOPT_ARG_LIST,
	    CLDES_OPT_NODE
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the clsnmpmib "set" subcommand
static cloptions_t clsnmpmib_set_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'n', "node", "<node>", CLOPT_ARG_LIST,
	    CLDES_OPT_NODE
	},
	{'p', "property", "<name>=<value>", CLOPT_ARG_LIST,
	    CLDES_OPT_PROPERTY
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the clsnmpmib "show" subcommand
static cloptions_t clsnmpmib_show_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'n', "node", "<node>", CLOPT_ARG_LIST,
	    CLDES_OPT_NODE
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of subcommands for the "clsnmpmib" command
static clsubcommand_t clsnmpmib_subcommands[] = {
	{
		"disable",
		CLSUB_DO_COMMAND_LOGGING,
		(char *)CLSNMPMIB_SUBCMD_DISABLE,
		CL_AUTH_CLUSTER_ADMIN,
		clsnmpmib_disable_usage,
		clsnmpmib_disable_optionslist,
		clsnmpmib_disable_options
	},
	{
		"enable",
		CLSUB_DO_COMMAND_LOGGING,
		(char *)CLSNMPMIB_SUBCMD_ENABLE,
		CL_AUTH_CLUSTER_ADMIN,
		clsnmpmib_enable_usage,
		clsnmpmib_enable_optionslist,
		clsnmpmib_enable_options
	},
	{
		"export",
		CLSUB_DEFAULT,
		(char *)CLSNMPMIB_SUBCMD_EXPORT,
		CL_AUTH_CLUSTER_READ,
		clsnmpmib_export_usage,
		clsnmpmib_export_optionslist,
		clsnmpmib_export_options
	},
	{
		"list",
		CLSUB_DEFAULT,
		(char *)CLSNMPMIB_SUBCMD_LIST,
		CL_AUTH_CLUSTER_READ,
		clsnmpmib_list_usage,
		clsnmpmib_list_optionslist,
		clsnmpmib_list_options
	},
	{
		"set",
		CLSUB_DO_COMMAND_LOGGING,
		(char *)CLSNMPMIB_SUBCMD_SET,
		CL_AUTH_CLUSTER_MODIFY,
		clsnmpmib_set_usage,
		clsnmpmib_set_optionslist,
		clsnmpmib_set_options
	},
	{
		"show",
		CLSUB_DEFAULT,
		(char *)CLSNMPMIB_SUBCMD_SHOW,
		CL_AUTH_CLUSTER_READ,
		clsnmpmib_show_usage,
		clsnmpmib_show_optionslist,
		clsnmpmib_show_options
	},
	{0, 0, 0, 0, 0, 0, 0}
};

// Initialization properties for the "clsnmpmib" command
clcommandinit_t clsnmpmibinit = {
	CLCOMMANDS_COMMON_VERSION,
	(char *)CLSNMPMIB_CMD_DESC,
	clsnmpmib_usage,
	clsnmpmib_subcommands
};
