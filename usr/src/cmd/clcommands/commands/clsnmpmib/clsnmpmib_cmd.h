//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef	_CLSNMPMIB_CMD_H
#define	_CLSNMPMIB_CMD_H

#pragma ident	"@(#)clsnmpmib_cmd.h	1.3	08/05/20 SMI"

#include "clcommands.h"
#include "clsnmpmib.h"

//
// This is the main header file for clsnmpmib options functions
//

#define	CLSNMPMIB_SUBCMD_DISABLE	\
	GETTEXT("Disable an SNMP MIB")
#define	CLSNMPMIB_SUBCMD_ENABLE		\
	GETTEXT("Enable an SNMP MIB")
#define	CLSNMPMIB_SUBCMD_EXPORT		\
	GETTEXT("Export SNMP MIB configuration")
#define	CLSNMPMIB_SUBCMD_LIST		\
	GETTEXT("List the SNMP MIBs")
#define	CLSNMPMIB_SUBCMD_SET		\
	GETTEXT("Set SNMP MIB properties")
#define	CLSNMPMIB_SUBCMD_SHOW		\
	GETTEXT("Display SNMP MIBs and their properties")
#define	CLSNMPMIB_CMD_DESC		\
	GETTEXT("Administer Sun Cluster SNMP MIBs")

extern clcommandinit_t clsnmpmibinit;

// Subcommand options
extern clerrno_t clsnmpmib_disable_options(ClCommand &cmd);
extern clerrno_t clsnmpmib_enable_options(ClCommand &cmd);
extern clerrno_t clsnmpmib_export_options(ClCommand &cmd);
extern clerrno_t clsnmpmib_list_options(ClCommand &cmd);
extern clerrno_t clsnmpmib_set_options(ClCommand &cmd);
extern clerrno_t clsnmpmib_show_options(ClCommand &cmd);

#endif /* _CLSNMPMIB_CMD_H */
