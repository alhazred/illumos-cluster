//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident   "@(#)clsnmpmib_set_options.cc	1.2	08/05/20 SMI"

//
// Process clsnmpmib "set" subcommand options
//

#include "clsnmpmib_cmd.h"

//
// clsnmpmib "set" options
//
clerrno_t
clsnmpmib_set_options(ClCommand &cmd)
{
	int i;
	int c;
	int wildcard;
	int errcount = 0;
	char *option = 0;
	int option_index = 0;
	char *shortopts;
	struct option *longopts;
	ValueList operands = ValueList(true);
	ValueList nodes = ValueList(true);
	NameValueList props;
	optflgs_t optflgs = 0;

	// Get the short and long option lists
	shortopts = clsubcommand_get_shortoptions(cmd);
	longopts = clsubcommand_get_longoptions(cmd);

	// This subcommand should have options
	if (!shortopts || !longopts) {
		clerror("Aborting - subcommand initialization failed.\n");
		abort();
	}

	optind = 2;
	while ((c = getopt_clip(cmd.argc, cmd.argv, shortopts, longopts,
	    &option_index)) != -1) {

		// Get the option
		delete option;
		option = clcommand_option(optopt, cmd.argv[optind - 1]);

		// Switch on option letter
		switch (c) {
		case 'v':
			// Verbose option
			cmd.isVerbose = 1;
			break;

		case '?':
			// Help option?
			if ((strcmp(option, "-?") == 0) ||
			    (strcmp(option, "--help") == 0)) {
				optflgs |= hflg;
				break;
			}

			// Or, unrecognized option?
			clerror("Unrecognized option - \"%s\".\n", option);
			++errcount;
			break;

		case 'n':
			// Nodes
			nodes.add(1, optarg);
			break;

		case 'p':
			// Properties
			if ((strchr(optarg, '=') == NULL) ||
			    (strchr(optarg, '=') != strrchr(optarg, '='))) {
				clerror("Invalid property specification: "
				    "\"%s\".\n", optarg);
				++errcount;
			}
			if (props.add(0, optarg)) {
				clerror("Multiple property specification: "
				    "\"%s\".\n", optarg);
				++errcount;
			}
			break;

		case ':':
			// Missing argument
			clerror("Option \"%s\" requires an argument.\n",
			    option);
			++errcount;
			break;

		default:
			//
			// We should never get here.
			// If we do, it means that there
			// is an option in our option list
			// for which there is no case statement.
			// We either have a bad option list or
			// a bad case statement.
			//
			clerror("Unexpected option - \"%s\".\n", option);
			++errcount;
			break;
		}
	}
	delete option;
	option = (char *)0;

	// Errors?
	if (errcount) {
		clcommand_usage(cmd, cmd.getSubCommandName(), errcount);
		return (CL_EINVAL);
	}

	// Just print help?
	if (optflgs & hflg) {
		clcommand_help(cmd, cmd.getSubCommandName());
		return (CL_NOERR);
	}

	// There must be at least one prop
	if (props.getSize() == 0) {
		++errcount;
		clerror("You must specify at least one property.\n");
	}

	// Update the operands list.  Don't add the wildcard to the operands
	wildcard = 0;
	for (i = optind;  i < cmd.argc;  ++i) {
		if (strcmp(cmd.argv[i], CLCOMMANDS_WILD_OPERAND) == 0) {
			++wildcard;
		} else {
			operands.add(cmd.argv[i]);
		}
	}

	// Update the operands list
	if (wildcard && operands.getSize() > 0) {
		++errcount;
		clerror("You cannot specify both \"%s\" and SNMP MIBs at "
		    "the same time.\n", CLCOMMANDS_WILD_OPERAND);
	}

	// There must be at least one snmpmib
	if (operands.getSize() == 0 && !wildcard) {
		++errcount;
		clerror("You must specify either \"%s\" or a "
		    "list of SNMP MIBs to modify.\n",
		    CLCOMMANDS_WILD_OPERAND);
	}

	if (errcount) {
		clcommand_usage(cmd, cmd.getSubCommandName(), errcount);
		return (CL_EINVAL);
	}

	// Perform clsnmpmib "set" operation
	return (clsnmpmib_set(cmd, operands, nodes, props));
}
