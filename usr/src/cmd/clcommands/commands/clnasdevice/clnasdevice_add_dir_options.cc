//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clnasdevice_add_dir_options.cc	1.6	08/07/24 SMI"

//
// Process clnasdevice "add-dir" subcommand options
//

#include "clnasdevice_cmd.h"

//
// clnasdevice "add-dir" options
//
clerrno_t
clnasdevice_add_dir_options(ClCommand &cmd)
{
	int i;
	int c;
	int wildcard;
	int errcount = 0;
	char *cl_option = 0;
	int option_index = 0;
	char *shortopts;
	struct option *longopts;
	ValueList operands;
	optflgs_t optflgs = 0;
	int f_flg, iflg;
	ValueList directories = ValueList(true);
	NameValueList dir_list;
	NameValueList properties = NameValueList(true);
	int all_dir = 0;
	int skipped = 0;
	char *clconfiguration = (char *)0;
	char *this_dir;
	char *passwdfile = (char *)0;
	char *passwrd = (char *)0;
	int invalid = 0;

	// Get the short and long option lists
	shortopts = clsubcommand_get_shortoptions(cmd);
	longopts = clsubcommand_get_longoptions(cmd);

	// This subcommand should have options
	if (!shortopts || !longopts) {
		clerror("Aborting - subcommand initialization failed.\n");
		abort();
	}

	optind = 2;
	f_flg = iflg = 0;
	while ((c = getopt_clip(cmd.argc, cmd.argv, shortopts, longopts,
	    &option_index)) != -1) {

		// Get the option
		delete cl_option;
		cl_option = clcommand_option(optopt, cmd.argv[optind - 1]);

		// Switch on option letter
		switch (c) {
		case 'v':
			// Verbose option
			cmd.isVerbose = 1;
			optflgs |= vflg;
			break;

		case '?':
			// Help option?
			if ((strcmp(cl_option, "-?") == 0) ||
			    (strcmp(cl_option, "--help") == 0)) {
				optflgs |= hflg;
				break;
			}

			// Or, unrecognized option?
			clerror("Unrecognized option - \"%s\".\n", cl_option);
			++errcount;
			break;

		case 'f':
			// Password file
			passwdfile = optarg;
			f_flg++;
			break;

		case 'i':
			// Input file
			clconfiguration = optarg;
			iflg++;
			break;

		case 'd':
			// Directories
			directories.add(1, optarg);
			break;

		case ':':
			// Missing argument
			clerror("Option \"%s\" requires an argument.\n",
			    cl_option);
			++errcount;
			break;

		default:
			//
			// We should never get here.
			// If we do, it means that there
			// is an option in our option list
			// for which there is no case statement.
			// We either have a bad option list or
			// a bad case statement.
			//
			clerror("Unexpected option - \"%s\".\n", cl_option);
			++errcount;
			break;
		}
	}
	delete cl_option;
	cl_option = (char *)0;

	if (iflg > 1) {
		clerror("You can only specify the \"-i\" option once.\n");
		++errcount;
	}
	if (f_flg > 1) {
		clerror("You can only specify the \"-f\" option once.\n");
		++errcount;
	}

	// Errors?
	if (errcount) {
		clcommand_usage(cmd, cmd.getSubCommandName(), errcount);
		return (CL_EINVAL);
	}

	// Just print help?
	if (optflgs & hflg) {
		clcommand_help(cmd, cmd.getSubCommandName());
		return (CL_NOERR);
	}

	// Process the directories
	for (directories.start(); !directories.end(); directories.next()) {
		this_dir = directories.getValue();
		if (strcmp(this_dir, NASDIR_ALL_DIR) == 0) {
			all_dir++;
			continue;
		}
		if (*this_dir != '/') {
			clerror("Directory \"%s\" does not start with "
			    "\"/\". Ignored.\n", this_dir);
			skipped++;
			continue;
		} else if (strlen(this_dir) == 1) {
			clerror("Cannot add the root directory. Ignored.\n");
			skipped++;
			continue;
		}
		(void) dir_list.add(NAS_DIRECTORIES, this_dir);
	}

	// Cannot specify both "all" and other directories
	if (all_dir && dir_list.getSize() > 0) {
		clerror("You cannot specify \"-d\" with both \"%s\" "
		    "and directories at the same time.\n",
		    NASDIR_ALL_DIR);
		++errcount;
	}

	// Update the operands list
	wildcard = 0;
	for (i = optind;  i < cmd.argc;  ++i) {
		operands.add(cmd.argv[i]);
		if (strcmp(cmd.argv[i], CLCOMMANDS_WILD_OPERAND) == 0) {
			++wildcard;
		}
	}

	if (!clconfiguration) {
		// If there is no -i, there must be directory.
		if (all_dir) {
			clerror("You cannot specify \"-d %s\" unless "
			    "you use \"-i\".\n", NASDIR_ALL_DIR);
			++errcount;
		}
		if (dir_list.getSize() == 0) {
			if (!skipped) {
				clerror("No directories are specified.\n");
			}
			++errcount;
		}

		// -f is not necessary if no input file
		if (passwdfile) {
			clerror("You cannot specify the \"-f\" option "
			    "unless you use \"-i\".\n");
			++errcount;
		}

		// There must be one NAS device
		if (operands.getSize() == 0) {
			clerror("You must give the name of the NAS device "
			    "for the directories.\n");
			++errcount;
		}

		// Only one NAS device without "-i"
		if (wildcard) {
			++errcount;
			clerror("Cannot use \"%s\" with this "
			    "subcommand unless you use \"-i\".\n",
			    CLCOMMANDS_WILD_OPERAND);
		}
		if (operands.getSize() > 1) {
			++errcount;
			clerror("Only add directories to one NAS device "
			    "at a time, unless you use \"-i\".\n");
		}
	} else {
		// Process the password
		if (passwdfile) {
			// Read the password file
			if (scnas_read_passwd_file(passwdfile,
			    &passwrd) != 0) {
				clerror("Cannot read the given password file "
				    "\"%s\".\n", passwdfile);
				++invalid;
			}

			if (passwrd && !invalid) {
				// Use rot13 to obfuscate the password
				scnas_rot13_alg(&passwrd);
				(void) properties.add(NAS_PASSWD, passwrd);
			}
		}

		// There must be at least one NAS device
		if (operands.getSize() == 0) {
			++errcount;
			clerror("You must specify either \"%s\" or a "
			    "list of NAS devices for the directories.\n",
			    CLCOMMANDS_WILD_OPERAND);
		}
	}

	// wildcard and other operands cannot be specified together
	if (wildcard && operands.getSize() > 1) {
		++errcount;
		clerror("You cannot specify both \"%s\" and NAS devices "
		    "at the same time.\n", CLCOMMANDS_WILD_OPERAND);
	}

	if (errcount) {
		clcommand_usage(cmd, cmd.getSubCommandName(), errcount);
		return (CL_EINVAL);
	}

	if (invalid) {
		// Invalid passwd file. Not usage error.
		return (CL_ENOENT);
	}

	// Perform clnasdevice "add-dir" operation
	if (!clconfiguration) {
		return (clnasdevice_add_dir(cmd, operands, optflgs,
		    dir_list));
	} else {
		return (clnasdevice_add_dir(cmd, operands, optflgs,
		    clconfiguration, dir_list, properties));
	}
}
