//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clnasdevice_set_options.cc	1.7	08/07/24 SMI"

//
// Process clnasdevice "set" subcommand options
//

#include "clnasdevice_cmd.h"

//
// clnasdevice "set" options
//
clerrno_t
clnasdevice_set_options(ClCommand &cmd)
{
	int i;
	int c;
	int errcount = 0;
	char *cl_option = 0;
	int option_index = 0;
	char *shortopts;
	struct option *longopts;
	ValueList operands;
	optflgs_t optflgs = 0;
	int f_flg, u_flg;
	NameValueList properties = NameValueList(true);
	char *passwdfile = (char *)0;
	char *userid = (char *)0;
	char *passwrd = (char *)0;
	char input_buf[SCNAS_MAXSTRINGLEN];
	boolean_t prop_required = B_FALSE;

	// Get the short and long option lists
	shortopts = clsubcommand_get_shortoptions(cmd);
	longopts = clsubcommand_get_longoptions(cmd);

	// This subcommand should have options
	if (!shortopts || !longopts) {
		clerror("Aborting - subcommand initialization failed.\n");
		abort();
	}

	optind = 2;
	f_flg = u_flg = 0;
	while ((c = getopt_clip(cmd.argc, cmd.argv, shortopts, longopts,
	    &option_index)) != -1) {

		// Get the option
		delete cl_option;
		cl_option = clcommand_option(optopt, cmd.argv[optind - 1]);

		// Switch on option letter
		switch (c) {
		case 'v':
			// Verbose option
			cmd.isVerbose = 1;
			optflgs |= vflg;
			break;

		case '?':
			// Help option?
			if ((strcmp(cl_option, "-?") == 0) ||
			    (strcmp(cl_option, "--help") == 0)) {
				optflgs |= hflg;
				break;
			}

			// Or, unrecognized option?
			clerror("Unrecognized option - \"%s\".\n", cl_option);
			++errcount;
			break;

		case 'f':
			// Password file
			passwdfile = optarg;
			f_flg++;
			break;

		case 'p':
			// Properties
			if (strchr(optarg, '=') == NULL) {
				clerror("No property value is specified in "
				    "\"%s\".\n", optarg);
				++errcount;
			} else if (strchr(optarg, '=') !=
			    strrchr(optarg, '=')) {
				clerror("You must use one \"-p\" for each "
				    "property in \"%s\".\n",
				    optarg);
				++errcount;
			} else if (properties.add(1, optarg) != 0) {
				clerror("The same property name is specified "
				    "multiple times in \"%s\".\n",
				    optarg);
				++errcount;
			}
			break;

		case 'u':
			// userid
			userid = optarg;
			u_flg++;
			break;

		case ':':
			// Missing argument
			clerror("Option \"%s\" requires an argument.\n",
			    cl_option);
			++errcount;
			break;

		default:
			//
			// We should never get here.
			// If we do, it means that there
			// is an option in our option list
			// for which there is no case statement.
			// We either have a bad option list or
			// a bad case statement.
			//
			clerror("Unexpected option - \"%s\".\n", cl_option);
			++errcount;
			break;
		}
	}
	delete cl_option;
	cl_option = (char *)0;

	// Update operands list first so we can check if it can be set
	for (i = optind;  i < cmd.argc;  ++i) {
		operands.add(cmd.argv[i]);
	}

	if (operands.getSize() == 1) {

		if (scnas_check_nas_proprequired(cmd.argv[optind],
		    &prop_required) && !prop_required) {
			if (errcount) {
				clcommand_usage(cmd,
				    cmd.getSubCommandName(), errcount);
			} else {
				clerror("You cannot set any properties "
				    "for NAS device \"%s\".\n",
				    cmd.argv[optind]);
			}
			return (CL_EINVAL);
		}
	}

	// Check for multiple options
	if (f_flg > 1) {
		clerror("You can only specify the \"-f\" "
		    "option once.\n");
		++errcount;
	}
	if (u_flg > 1) {
		clerror("You can only specify the \"-u\" "
		    "option once.\n");
		++errcount;
	}

	// Errors?
	if (errcount) {
		clcommand_usage(cmd, cmd.getSubCommandName(), errcount);
		return (CL_EINVAL);
	}

	// Just print help?
	if (optflgs & hflg) {
		clcommand_help(cmd, cmd.getSubCommandName());
		return (CL_NOERR);
	}

	// Password cannot be specified in command line
	for (properties.start(); !properties.end(); properties.next()) {
		if (strcmp(properties.getName(), NAS_PASSWD) == 0) {
			clerror("You cannot specify password in command "
			    "line.\n");
			++errcount;
		}
	}

	// There must be a property or userid
	if (userid && (properties.add(NAS_USERID, userid) != 0)) {
		clerror("You can only specify the user ID once.\n");
		++errcount;
	}

	if (properties.getSize() == 0) {
		++errcount;
		clerror("You must specify the properties to change.\n");
	}

	// There must be at least one NAS device
	if (operands.getSize() == 0) {
		++errcount;
		clerror("You must specify one NAS device that "
		    "you want to modify.\n");
	} else if (operands.getSize() > 1) {
		++errcount;
		clerror("You cannot specify more than one NAS "
		    "device to modify.\n");
	}

	// Process the password
	if (prop_required && passwdfile) {
		int invalid = 0;

		// Read the password file
		if (scnas_read_passwd_file(passwdfile, &passwrd) != 0) {
			++invalid;
			clerror("Cannot read the given password file "
			    "\"%s\".\n", passwdfile);
		}
		if (errcount) {
			clcommand_usage(cmd, cmd.getSubCommandName(), errcount);
			return (CL_EINVAL);
		} else if (invalid) {
			// Invalid passwd file. Not usage error.
			return (CL_ENOENT);
		}
	}

	// Errors?
	if (errcount) {
		clcommand_usage(cmd, cmd.getSubCommandName(), errcount);
		return (CL_EINVAL);
	}

	if (prop_required && !passwdfile) {
		// Prompt user to enter password
		(void) sprintf(input_buf,
		    gettext("Enter password:  "));
		passwrd = getpassphrase(input_buf);

	}
	if (passwrd) {
		// Use rot13 to obfuscate the password
		scnas_rot13_alg(&passwrd);
		(void) properties.add(NAS_PASSWD, passwrd);
	}

	//
	// Perform clnasdevice "set" operation
	//
	return (clnasdevice_set(cmd, operands, optflgs,
	    properties));
}
