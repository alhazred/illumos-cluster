//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clnasdeviceinit_constants.cc	1.7	08/07/24 SMI"

//
// Initialization data for clnasdevice(1CL)
//

#include "clnasdevice_cmd.h"

//
// Usage Messages
//
// All usage message lines should begin with a single "%s" for
// the command name. Lines must never include more than a single "%s".
//

// List of "clnasdevice" command usage message
static char *clnasdevice_usage [] = {
	"%s <subcommand> [<options>] [+ | <nasdevice> ...]",
	"%s [<subcommand>]  -? | --help",
	"%s  -V | --version",
	(char *)0
};

// List of clnasdevice "add" subcommand usage messages
static char *clnasdevice_add_usage[] = {
	"%s add -t <type> [<options>] <nasdevice>",
	"%s add -i <configfile> [<options>] + | <nasdevice> ...",
	(char *)0
};

// List of clnasdevice "add-dir" subcommand usage messages
static char *clnasdevice_add_dir_usage[] = {
	"%s add-dir -d <directory>[,...] <nasdevice>",
	"%s add-dir -i <configfile> [<options>] + | <nasdevice> ...",
	(char *)0
};

// List of clnasdevice "export" subcommand usage messages
static char *clnasdevice_export_usage[] = {
	"%s export [<options>] [+ | <nasdevice> ...]",
	(char *)0
};

// List of clnasdevice "list" subcommand usage messages
static char *clnasdevice_list_usage[] = {
	"%s list [<options>] [+ | <nasdevice> ...]",
	(char *)0
};

// List of clnasdevice "set" subcommand usage messages
static char *clnasdevice_set_usage[] = {
	"%s set -p <name>=<value> [<options>] <nasdevice>",
	(char *)0
};

// List of clnasdevice "show" subcommand usage messages
static char *clnasdevice_show_usage[] = {
	"%s show [<options>] [+ | <nasdevice> ...]",
	(char *)0
};

// List of clnasdevice "remove" subcommand usage messages
static char *clnasdevice_remove_usage[] = {
	"%s remove [<options>] + | <nasdevice> ...",
	(char *)0
};

// List of clnasdevice "remove-dir" subcommand usage messages
static char *clnasdevice_remove_dir_usage[] = {
	"%s remove-dir -d all | <directory>[,...] <nasdevice>",
	(char *)0
};

//
// Option lists
//
// All option descriptions should be broken up into lines of no
// greater than 70 characters.  All but the trailing newline ('\n')
// itself should be included in the option descriptions found in these
// tables.
//

// List of options for the clnasdevice "add" subcommand
static cloptions_t clnasdevice_add_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'f', "passwdfile", "<passwdfile>", 0,
	    CLDES_OPT_PASSWD
	},
	{'i', "input", "{- | <clconfiguration>}", 0,
	    CLDES_OPT_INPUT
	},
	{'p', "property", "<name>=<value>", 0,
	    CLDES_OPT_PROPERTY
	},
	{'t', "type", "<type>", 0,
	    CLDES_OPT_TYPE
	},
	{'u', "userid", "<userid>", 0,
	    CLDES_OPT_USERID
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the clnasdevice "add-dir" subcommand
static cloptions_t clnasdevice_add_dir_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'d', "directory", "all | <directory>", CLOPT_ARG_LIST,
	    CLDES_OPT_DIR
	},
	{'f', "passwdfile", "<passwdfile>", 0,
	    CLDES_OPT_PASSWD
	},
	{'i', "input", "{- | <clconfiguration>}", 0,
	    CLDES_OPT_INPUT
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the clnasdevice "export" subcommand
static cloptions_t clnasdevice_export_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'d', "directory", "all | <directory>", CLOPT_ARG_LIST,
	    CLDES_OPT_DIR
	},
	{'o', "output", "{- | <clconfiguration>}", 0,
	    CLDES_OPT_OUTPUT
	},
	{'t', "type", "<type>", CLOPT_ARG_LIST,
	    CLDES_OPT_TYPE
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the clnasdevice "list" subcommand
static cloptions_t clnasdevice_list_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'t', "type", "<type>", CLOPT_ARG_LIST,
	    CLDES_OPT_TYPE
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the clnasdevice "set" subcommand
static cloptions_t clnasdevice_set_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'f', "passwdfile", "<passwdfile>", 0,
	    CLDES_OPT_PASSWD
	},
	{'p', "property", "<name>=<value>", 0,
	    CLDES_OPT_PROPERTY
	},
	{'u', "userid", "<userid>", 0,
	    CLDES_OPT_USERID
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the clnasdevice "remove" subcommand
static cloptions_t clnasdevice_remove_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'F', "force", 0, 0,
	    CLDES_OPT_FORCE
	},
	{'t', "type", "<type>", CLOPT_ARG_LIST,
	    CLDES_OPT_TYPE
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the clnasdevice "remove-dir" subcommand
static cloptions_t clnasdevice_remove_dir_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'d', "directory", "all | <directory>", CLOPT_ARG_LIST,
	    CLDES_OPT_DIR
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the clnasdevice "show" subcommand
static cloptions_t clnasdevice_show_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'t', "type", "<type>", CLOPT_ARG_LIST,
	    CLDES_OPT_TYPE
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of subcommands for the "clnasdevice" command
static clsubcommand_t clnasdevice_subcommands[] = {
	{
		"add",
		CLSUB_DO_COMMAND_LOGGING | CLSUB_ZONE_CLUSTER_ALLOWED,
		(char *)CLNAS_SUBCMD_ADD,
		CL_AUTH_CLUSTER_MODIFY,
		clnasdevice_add_usage,
		clnasdevice_add_optionslist,
		clnasdevice_add_options
	},
	{
		"add-dir",
		CLSUB_DO_COMMAND_LOGGING | CLSUB_ZONE_CLUSTER_ALLOWED,
		(char *)CLNAS_SUBCMD_ADD_DIR,
		CL_AUTH_CLUSTER_MODIFY,
		clnasdevice_add_dir_usage,
		clnasdevice_add_dir_optionslist,
		clnasdevice_add_dir_options
	},
	{
		"export",
		CLSUB_DEFAULT,
		(char *)CLNAS_SUBCMD_EXPORT,
		CL_AUTH_CLUSTER_READ,
		clnasdevice_export_usage,
		clnasdevice_export_optionslist,
		clnasdevice_export_options
	},
	{
		"list",
		CLSUB_DEFAULT | CLSUB_ZONE_CLUSTER_ALLOWED,
		(char *)CLNAS_SUBCMD_LIST,
		CL_AUTH_CLUSTER_READ,
		clnasdevice_list_usage,
		clnasdevice_list_optionslist,
		clnasdevice_list_options
	},
	{
		"set",
		CLSUB_DO_COMMAND_LOGGING | CLSUB_ZONE_CLUSTER_ALLOWED,
		(char *)CLNAS_SUBCMD_SET,
		CL_AUTH_CLUSTER_MODIFY,
		clnasdevice_set_usage,
		clnasdevice_set_optionslist,
		clnasdevice_set_options
	},
	{
		"remove",
		CLSUB_DO_COMMAND_LOGGING | CLSUB_ZONE_CLUSTER_ALLOWED,
		(char *)CLNAS_SUBCMD_REMOVE,
		CL_AUTH_CLUSTER_MODIFY,
		clnasdevice_remove_usage,
		clnasdevice_remove_optionslist,
		clnasdevice_remove_options
	},
	{
		"remove-dir",
		CLSUB_DO_COMMAND_LOGGING | CLSUB_ZONE_CLUSTER_ALLOWED,
		(char *)CLNAS_SUBCMD_REMOVE_DIR,
		CL_AUTH_CLUSTER_MODIFY,
		clnasdevice_remove_dir_usage,
		clnasdevice_remove_dir_optionslist,
		clnasdevice_remove_dir_options
	},
	{
		"show",
		CLSUB_DEFAULT | CLSUB_ZONE_CLUSTER_ALLOWED,
		(char *)CLNAS_SUBCMD_SHOW,
		CL_AUTH_CLUSTER_READ,
		clnasdevice_show_usage,
		clnasdevice_show_optionslist,
		clnasdevice_show_options
	},
	{0, 0, 0, 0, 0, 0, 0}
};

// Initialization properties for the "clnasdevice" command
clcommandinit_t clnasdeviceinit = {
	CLCOMMANDS_COMMON_VERSION,
	(char *)CLNAS_CMD_DESC,
	clnasdevice_usage,
	clnasdevice_subcommands
};
