//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clnasdevice_remove_dir_options.cc	1.2	08/05/20 SMI"

//
// Process clnasdevice "remove-dir" subcommand options
//

#include "clnasdevice_cmd.h"

//
// clnasdevice "remove-dir" options
//
clerrno_t
clnasdevice_remove_dir_options(ClCommand &cmd)
{
	int i;
	int c;
	int errcount = 0;
	char *cl_option = 0;
	int option_index = 0;
	char *shortopts;
	struct option *longopts;
	ValueList operands;
	optflgs_t optflgs = 0;
	ValueList directories = ValueList(true);
	NameValueList dir_list;
	int all_dir = 0;
	int skipped = 0;
	char *this_dir;

	// Get the short and long option lists
	shortopts = clsubcommand_get_shortoptions(cmd);
	longopts = clsubcommand_get_longoptions(cmd);

	// This subcommand should have options
	if (!shortopts || !longopts) {
		clerror("Aborting - subcommand initialization failed.\n");
		abort();
	}

	optind = 2;
	while ((c = getopt_clip(cmd.argc, cmd.argv, shortopts, longopts,
	    &option_index)) != -1) {

		// Get the option
		delete cl_option;
		cl_option = clcommand_option(optopt, cmd.argv[optind - 1]);

		// Switch on option letter
		switch (c) {
		case 'v':
			// Verbose option
			cmd.isVerbose = 1;
			optflgs |= vflg;
			break;

		case '?':
			// Help option?
			if ((strcmp(cl_option, "-?") == 0) ||
			    (strcmp(cl_option, "--help") == 0)) {
				optflgs |= hflg;
				break;
			}

			// Or, unrecognized option?
			clerror("Unrecognized option - \"%s\".\n", cl_option);
			++errcount;
			break;

		case 'd':
			// Directories
			directories.add(1, optarg);
			break;

		case ':':
			// Missing argument
			clerror("Option \"%s\" requires an argument.\n",
			    cl_option);
			++errcount;
			break;

		default:
			//
			// We should never get here.
			// If we do, it means that there
			// is an option in our option list
			// for which there is no case statement.
			// We either have a bad option list or
			// a bad case statement.
			//
			clerror("Unexpected option - \"%s\".\n", cl_option);
			++errcount;
			break;
		}
	}
	delete cl_option;
	cl_option = (char *)0;

	// Errors?
	if (errcount) {
		clcommand_usage(cmd, cmd.getSubCommandName(), errcount);
		return (CL_EINVAL);
	}

	// Just print help?
	if (optflgs & hflg) {
		clcommand_help(cmd, cmd.getSubCommandName());
		return (CL_NOERR);
	}

	// Process the directories
	for (directories.start(); !directories.end(); directories.next()) {
		this_dir = directories.getValue();
		if (strcmp(this_dir, NASDIR_ALL_DIR) == 0) {
			all_dir++;
			continue;
		}

		if (*this_dir != '/') {
			clerror("Directory \"%s\" does not start with "
			    "\"/\". Ignored.\n", this_dir);
			skipped++;
			continue;
		} else if (strlen(this_dir) == 1) {
			clerror("Invalid directory \"%s\". Ignored.\n",
			    this_dir);
			skipped++;
			continue;
		}
		(void) dir_list.add(NAS_DIRECTORIES, this_dir);
	}

	// There must be at least one directory
	if (dir_list.getSize() == 0 && all_dir == 0) {
		if (!skipped) {
			clerror("No directories are specified.\n");
		}
		++errcount;
	}

	// Cannot specify both "all" and directories at same time
	if (all_dir && (dir_list.getSize() > 0)) {
		clerror("You cannot specify \"-d\" with both \"%s\" "
		    "and directories at the same time.\n",
		    NASDIR_ALL_DIR);
		++errcount;
	}

	// Update the operands list
	for (i = optind;  i < cmd.argc;  ++i) {
		operands.add(cmd.argv[i]);
	}

	// Only one NAS device
	if (operands.getSize() > 1) {
		++errcount;
		clerror("Only remove directories from one NAS "
		    "device at a time.\n");
	} else if (operands.getSize() == 0) {
		++errcount;
		clerror("You must give the name of the NAS device "
		    "to remove.\n");
	}

	if (errcount) {
		clcommand_usage(cmd, cmd.getSubCommandName(), errcount);
		return (CL_EINVAL);
	}

	// Perform clnasdevice "remove-dir" operation
	if (all_dir) {
		return (clnasdevice_remove_dir(cmd, operands, optflgs,
		    dir_list, NASDIR_ALL_DIR));
	} else {
		return (clnasdevice_remove_dir(cmd, operands, optflgs,
		    dir_list, NULL));
	}
}
