//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clnasdevice_add_options.cc	1.8	08/07/24 SMI"

//
// Process clnasdevice "add" subcommand options
//

#include "clnasdevice_cmd.h"

//
// clnasdevice "add" options
//
clerrno_t
clnasdevice_add_options(ClCommand &cmd)
{
	int i;
	int c;
	int wildcard;
	int errcount = 0;
	char *cl_option = 0;
	int option_index = 0;
	char *shortopts;
	struct option *longopts;
	ValueList operands;
	optflgs_t optflgs = 0;
	int iflg, f_flg, u_flg;
	ValueList nasdevicetypes = ValueList(true);
	ValueList xml_nasdevicetypes = ValueList(true);
	NameValueList properties = NameValueList(true);
	char *clconfiguration = (char *)0;
	char *passwdfile = (char *)0;
	char *userid = (char *)0;
	char *passwrd = (char *)0;
	char input_buf[SCNAS_MAXSTRINGLEN];
	char *nas_type;
	boolean_t prop_required = B_FALSE;
	int dup_userid = 0;
	clerrno_t clerrno = CL_NOERR;

	// Get the short and long option lists
	shortopts = clsubcommand_get_shortoptions(cmd);
	longopts = clsubcommand_get_longoptions(cmd);

	// This subcommand should have options
	if (!shortopts || !longopts) {
		clerror("Aborting - subcommand initialization failed.\n");
		abort();
	}

	optind = 2;
	iflg = f_flg = u_flg = 0;
	while ((c = getopt_clip(cmd.argc, cmd.argv, shortopts, longopts,
	    &option_index)) != -1) {

		// Get the option
		delete cl_option;
		cl_option = clcommand_option(optopt, cmd.argv[optind - 1]);

		// Switch on option letter
		switch (c) {
		case 'v':
			// Verbose option
			cmd.isVerbose = 1;
			optflgs |= vflg;
			break;

		case '?':
			// Help option?
			if ((strcmp(cl_option, "-?") == 0) ||
			    (strcmp(cl_option, "--help") == 0)) {
				optflgs |= hflg;
				break;
			}

			// Or, unrecognized option?
			clerror("Unrecognized option - \"%s\".\n", cl_option);
			++errcount;
			break;

		case 'f':
			// Password file
			passwdfile = optarg;
			f_flg++;
			break;

		case 't':
			// NAS type
			nasdevicetypes.add(1, optarg);
			break;

		case 'i':
			// Input file
			clconfiguration = optarg;
			iflg++;
			break;

		case 'p':
			// Properties
			if (strchr(optarg, '=') == NULL) {
				clerror("No property value is specified in "
				    "\"%s\".\n", optarg);
				++errcount;
			} else if (strchr(optarg, '=') !=
			    strrchr(optarg, '=')) {
				clerror("You must use one \"-p\" for each "
				    "property in \"%s\".\n",
				    optarg);
				++errcount;
			} else if (properties.add(1, optarg) != 0) {
				clerror("The same property name is specified "
				    "multiple times in \"%s\".\n",
				    optarg);
				++errcount;
			}
			break;

		case 'u':
			// userid
			userid = optarg;
			u_flg++;
			break;

		case ':':
			// Missing argument
			clerror("Option \"%s\" requires an argument.\n",
			    cl_option);
			++errcount;
			break;

		default:
			//
			// We should never get here.
			// If we do, it means that there
			// is an option in our option list
			// for which there is no case statement.
			// We either have a bad option list or
			// a bad case statement.
			//
			clerror("Unexpected option - \"%s\".\n", cl_option);
			++errcount;
			break;
		}
	}
	delete cl_option;
	cl_option = (char *)0;

	// Check for multiple options
	if (iflg > 1) {
		clerror("You can only specify the \"-i\" "
		    "option once.\n");
		++errcount;
	}
	if (f_flg > 1) {
		clerror("You can only specify the \"-f\" "
		    "option once.\n");
		++errcount;
	}
	if (u_flg > 1) {
		clerror("You can only specify the \"-u\" "
		    "option once.\n");
		++errcount;
	}

	// Errors?
	if (errcount) {
		clcommand_usage(cmd, cmd.getSubCommandName(), errcount);
		return (CL_EINVAL);
	}

	// Just print help?
	if (optflgs & hflg) {
		clcommand_help(cmd, cmd.getSubCommandName());
		return (CL_NOERR);
	}

	// Check for duplicate -u and "-p userid=<userid>"
	if (userid && (properties.add(NAS_USERID, userid) != 0)) {
		dup_userid++;
	}

	// Update the operands list
	wildcard = 0;
	for (i = optind;  i < cmd.argc;  ++i) {
		operands.add(cmd.argv[i]);
		if (strcmp(cmd.argv[i], CLCOMMANDS_WILD_OPERAND) == 0) {
			++wildcard;
		}
	}

	if (!clconfiguration) {
		// Must have one type
		if (nasdevicetypes.getSize() == 0) {
			clerror("You must specify the type of the NAS "
			    "device.\n");
			++errcount;
		} else if (nasdevicetypes.getSize() > 1) {
			clerror("You can specify only one NAS device type, "
			    "unless you use \"-i\".\n");
			++errcount;
		} else {
			// Valid type?
			nasdevicetypes.start();
			nas_type = nasdevicetypes.getValue();
			if (!scnas_check_nas_devicetype(nas_type,
			    &prop_required)) {
				clerror("Invalid type \"%s\".\n",
				    nas_type);
				if (!errcount) {
					//
					// There may be other usage errors
					// to check. So onl exit if no
					// other errors.
					//
					return (CL_ETYPE);
				}
			} else if (prop_required) {
				// Must have properties
				if (properties.getSize() == 0) {
					clerror("You must specify the required "
					    "properties \"%s\" and \"%s\".\n",
					    NAS_USERID, NAS_PASSWD);
					++errcount;
				} else {
					// Password cannot be in command line
					for (properties.start();
					    !properties.end();
					    properties.next()) {
						if (strcmp(properties.getName(),
						    NAS_PASSWD) == 0) {
							clerror("You cannot "
							    "specify password "
							    "in command "
							    "line.\n");
							++errcount;
						}
					}

					// Duplicated userid?
					if (dup_userid) {
						clerror("You can only specify "
						    "the user ID once.\n");
						++errcount;
					}
				}
			} else {
				// Should not have any properties
				if (dup_userid ||
				    (!u_flg && properties.getSize() != 0) ||
				    (u_flg && properties.getSize() > 1)) {
					clerror("You cannot specify \"-p\" "
					    "option for NAS type \"%s\".\n",
					    nas_type);
					++errcount;
				}
				if (f_flg) {
					clerror("You cannot specify \"-f\" "
					    "option for NAS type \"%s\".\n",
					    nas_type);
					++errcount;
				}
				if (u_flg) {
					clerror("You cannot specify \"-u\" "
					    "option for NAS type \"%s\".\n",
					    nas_type);
					++errcount;
				}
			}
		}

		// No wildcard
		if (wildcard) {
			++errcount;
			clerror("You cannot use \"%s\" with this subcommand.\n",
			    CLCOMMANDS_WILD_OPERAND);
		}

		// Only one NAS device without "-i"
		if (operands.getSize() > 1) {
			++errcount;
			clerror("Only one NAS device can be added "
			    "at a time, unless you use \"-i\".\n");
			if (wildcard) {
				clerror("You cannot specify both \"%s\" and "
				    "NAS devices at the same time.\n",
				    CLCOMMANDS_WILD_OPERAND);
			}
		}

		if (operands.getSize() == 0) {
			++errcount;
			clerror("You must give the name of the NAS device "
			    "that you want to add.\n");
		}

		// Set return error
		if (errcount && (clerrno == CL_NOERR)) {
			clerrno = CL_EINVAL;
		}
	} else {
		int add_this_type = 0;

		// Check if type is valid
		for (nasdevicetypes.start(); !nasdevicetypes.end();
		    nasdevicetypes.next()) {
			nas_type = nasdevicetypes.getValue();
			prop_required = B_FALSE;
			if (!scnas_check_nas_devicetype(nas_type,
			    &prop_required)) {
				clerror("Invalid type \"%s\".\n",
				    nas_type);
				if (clerrno == CL_NOERR) {
					clerrno = CL_ETYPE;
				}
				continue;
			}

			add_this_type = 1;

			// Check the properties
			if (prop_required) {
				// Password cannot be in command line
				for (properties.start(); !properties.end();
				    properties.next()) {
					if (strcmp(properties.getName(),
					    NAS_PASSWD) == 0) {
						clerror("You cannot specify "
						    "specify password in "
						    "command line.\n");
						add_this_type = 0;
					}
				}

				// Duplicated userid?
				if (dup_userid) {
					clerror("You can only specify "
					    "the user ID once.\n");
					add_this_type = 0;
				}

				// Must specify password file when use -i
				if (!passwdfile) {
					clerror("For NAS type \"%s\", you "
					    "must specify the name of a file "
					    "that contains the password for "
					    "the user ID you specified.\n",
					    nas_type);
					add_this_type = 0;
				}
			} else {
				// Should not have any properties
				if (dup_userid ||
				    (!u_flg && properties.getSize() != 0) ||
				    (u_flg && properties.getSize() > 1)) {
					clerror("You cannot specify \"-p\" "
					    "option for NAS type \"%s\".\n",
					    nas_type);
					add_this_type = 0;
				}
				if (f_flg) {
					clerror("You cannot specify \"-f\" "
					    "option for NAS type \"%s\".\n",
					    nas_type);
					add_this_type = 0;
				}
				if (u_flg) {
					clerror("You cannot specify \"-u\" "
					    "option for NAS type \"%s\".\n",
					    nas_type);
					add_this_type = 0;
				}
			}
			if (add_this_type) {
				xml_nasdevicetypes.add(nas_type);
			} else {
				// Usage errors
				if (clerrno == CL_NOERR) {
					clerrno = CL_EINVAL;
				}
				++errcount;
			}
		}

		// There must be at least one NAS device
		if (operands.getSize() == 0) {
			++errcount;
			clerror("You must specify either \"%s\" or a "
			    "list of NAS devices to add.\n",
			    CLCOMMANDS_WILD_OPERAND);
			if (clerrno == CL_NOERR) {
				clerrno = CL_EINVAL;
			}
		}

		// wildcard and other operands cannot be specified together
		if (wildcard && operands.getSize() > 1) {
			++errcount;
			clerror("You cannot specify both \"%s\" and NAS "
			    "devices at the same time.\n",
			    CLCOMMANDS_WILD_OPERAND);
			if (clerrno == CL_NOERR) {
				clerrno = CL_EINVAL;
			}
		}
	}

	// Errors?
	if (clerrno != CL_NOERR) {
		if (errcount) {
			clcommand_usage(cmd, cmd.getSubCommandName(), errcount);
		}
		return (clerrno);
	}

	// Process the password
	if (passwdfile) {
		// Read the password file
		if (scnas_read_passwd_file(passwdfile, &passwrd) != 0) {
			clerror("Cannot read the given password file "
			    "\"%s\".\n", passwdfile);
			// Invalid passwd file. Not usage error.
			return (CL_ENOENT);
		}
	} else {
		// Prompt for passwd
		if (!clconfiguration && prop_required) {
			(void) sprintf(input_buf,
			    gettext("Enter password:  "));
			passwrd = getpassphrase(input_buf);
		}
	}

	if (passwrd) {
		// Use rot13 to obfuscate the password
		scnas_rot13_alg(&passwrd);
		(void) properties.add(NAS_PASSWD, passwrd);
	}

	// Perform clnasdevice "add" operation
	if (!clconfiguration) {
		return (clnasdevice_add(cmd, operands, optflgs,
		    nasdevicetypes, properties));
	} else {
		return (clnasdevice_add(cmd, operands, optflgs,
		    clconfiguration,
		    xml_nasdevicetypes, properties));
	}
}
