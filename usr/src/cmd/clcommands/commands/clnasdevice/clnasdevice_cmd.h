//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef _CLNASDEVICE_CMD_H
#define	_CLNASDEVICE_CMD_H

#pragma ident	"@(#)clnasdevice_cmd.h	1.3	08/05/20 SMI"

//
// This is the header file for the clnasdevice(1CL) command
//

#include "clcommands.h"
#include "clnasdevice.h"

#define	CLNAS_SUBCMD_ADD		\
	GETTEXT("Add a NAS device to the cluster configuration")
#define	CLNAS_SUBCMD_ADD_DIR		\
	GETTEXT("Add NAS directories to the cluster configuration")
#define	CLNAS_SUBCMD_REMOVE		\
	GETTEXT("Remove a NAS device from the cluster configuration")
#define	CLNAS_SUBCMD_REMOVE_DIR		\
	GETTEXT("Remove NAS directories from the cluster configuration")
#define	CLNAS_SUBCMD_EXPORT		\
	GETTEXT("Export the NAS device configuration")
#define	CLNAS_SUBCMD_LIST		\
	GETTEXT("List NAS devices")
#define	CLNAS_SUBCMD_SET		\
	GETTEXT("Set NAS device properties")
#define	CLNAS_SUBCMD_SHOW		\
	GETTEXT("Show NAS devices and their properties")
#define	CLNAS_CMD_DESC			\
	GETTEXT("Manage NAS devices")

#ifdef __cplusplus
#include <sys/os.h>
#endif

#include "scnas.h"

// Subcommand options
extern clerrno_t clnasdevice_add_options(ClCommand &cmd);
extern clerrno_t clnasdevice_add_dir_options(ClCommand &cmd);
extern clerrno_t clnasdevice_remove_options(ClCommand &cmd);
extern clerrno_t clnasdevice_remove_dir_options(ClCommand &cmd);
extern clerrno_t clnasdevice_export_options(ClCommand &cmd);
extern clerrno_t clnasdevice_list_options(ClCommand &cmd);
extern clerrno_t clnasdevice_list_props_options(ClCommand &cmd);
extern clerrno_t clnasdevice_set_options(ClCommand &cmd);
extern clerrno_t clnasdevice_show_options(ClCommand &cmd);
extern clerrno_t clnasdevice_status_options(ClCommand &cmd);

#endif /* _CLNASDEVICE_CMD_H */
