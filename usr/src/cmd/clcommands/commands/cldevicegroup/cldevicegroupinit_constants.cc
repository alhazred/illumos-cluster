//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
#pragma ident	"@(#)cldevicegroupinit_constants.cc	1.7	08/05/20 SMI"

//
// Command definition table date for cldevicegroup(1CL)
//

//
// Usage Messages
//

#include "cldevicegroup_cmd.h"

// List of "cldevicegroup" command usage message
static char *cldevicegroup_usage[] = {
	"%s <subcommand> [<options>] [+ | <devicegroup>,...]",
	"%s [<subcommand>] -? | --help",
	"%s -V | --version",
	(char *)0
};

// List of cldevicegroup "add-device" subcommand usage messages
static char *cldevicegroup_adddevice_usage[] = {
	"%s add-device -d <device>[,...] <devicegroup>",
	(char *)0
};

// List of cldevicegroup "add-node" subcommand usage messages
static char *cldevicegroup_addnode_usage[] = {
	"%s add-node [-t <devicegroup-type>[,...]] -n <node>[,...] "
	    "{+ | <devicegroup> ...}",
	(char *)0
};

// List of cldevicegroup "create" subcommand usage messages
static char *cldevicegroup_create_usage[] = {
	"%s create -t <devicegroup-type> [-p <property_name>=<property_value>] "
	    "[-n <node>[,...]] [-d <device>[,...]] <devicegroup>",
	"%s create -i {- | <config_file>} [-t <devicegroup-type>[,...]] "
	    "[-p <property_name=property_value>] "
	    "[-n <node>[,...]] [-d <device>[,...]] {+ | <devicegroup> ...}",
	(char *)0
};

// List of cldevicegroup "delete" subcommand usage messages
static char *cldevicegroup_delete_usage[] = {
	"%s delete [-t <devicegroup-type>[,...]] {+ | <devicegroup> ...}",
	(char *)0
};

// List of cldevicegroup "disable" subcommand usage messages
static char *cldevicegroup_disable_usage[] = {
	"%s disable [-t <devicegroup-type>[,...]] {+ | <devicegroup> ...}",
	(char *)0
};

// List of cldevicegroup "enable" subcommand usage messages
static char *cldevicegroup_enable_usage[] = {
	"%s enable [-t <devicegroup-type>[,...]] {+ | <devicegroup> ...}",
	(char *)0
};

// List of cldevicegroup "export" subcommand usage messages
static char *cldevicegroup_export_usage[] = {
	"%s export [-o {- | <config_file>}] [-n <node>[,...]] "
	    "[-t <devicegroup-type>[,...]] {+ | <devicegroup> ...}",
	(char *)0
};

// List of cldevicegroup "list" subcommand usage messages
static char *cldevicegroup_list_usage[] = {
	"%s list [-t <devicegroup-type>[,...]] [-n <node>[,...]] "
	    "[+ | <devicegroup> ...]",
	(char *)0
};

// List of cldevicegroup "set" subcommand usage messages
static char *cldevicegroup_set_usage[] = {
	"%s [-t <devicegroup-type>[,...]] [-n <node>[,...]] "
	    "[-p <property_name>=<property_value>] [-d device[,...] "
	    "{+ | <devicegroup> ...}",
	(char *)0
};

// List of cldevicegroup "offline" subcommand usage messages
static char *cldevicegroup_offline_usage[] = {
	"%s offline [-t <devicegroup-type>[,...]] "
	    "{+ | <devicegroup> ...}",
	(char *)0

};

// List of cldevicegroup "online" subcommand usage messages
static char *cldevicegroup_online_usage[] = {
	"%s online [-t <devicegroup-type>[,...]] [-n <node>] [-e] "
	    "{+ | <devicegroup> ...}",
	(char *)0
};

// List of cldevicegroup "remove-device" subcommand usage messages
static char *cldevicegroup_removedevice_usage[] = {
	"%s remove-device -d <device>[,...] <devicegroup>",
	(char *)0
};

// List of cldevicegroup "remove-node" subcommand usage messages
static char *cldevicegroup_removenode_usage[] = {
	"%s remove-node [-t <devicegroup-type>[,...]] -n <node>[,...] "
	    "{+ | <devicegroup> ...}",
	(char *)0
};

// List of cldevicegroup "show" subcommand usage messages
static char *cldevicegroup_show_usage[] = {
	"%s show [-t <devicegroup-type>[,...]] [-n <node>[,...]] "
	    "[+ | <devicegroup> ...]",
	(char *)0
};

// List of cldevicegroup "status" subcommand usage messages
static char *cldevicegroup_status_usage[] = {
	"%s status [-t <devicegroup-type>[,...]] [-n <node>[,...]] "
	    "[+ | <devicegroup> ...]",
	(char *)0
};

// List of cldevicegroup "switch" subcommand usage messages
static char *cldevicegroup_switch_usage[] = {
	"%s switch [-t <devicegroup-type>[,...]] [-e] -n node "
	    "{+ | <devicegroup> ...}",
	(char *)0

};

// List of cldevicegroup "sync" subcommand usage messages
static char *cldevicegroup_sync_usage[] = {
	"%s sync [-t <devicegroup-type>[,...]] {+ | <devicegroup> ...}",
	(char *)0
};

// Options list

// Options for "create" subcommand
static cloptions_t cldevicegroup_create_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'t', "type", "<devicegroup-type>", CLOPT_ARG_LIST,
	    CLDES_OPT_DT
	},
	{'n', "node", "<node>", CLOPT_ARG_LIST,
	    CLDES_OPT_NODE
	},
	{'d', "device", "<device>", CLOPT_ARG_LIST,
	    CLDES_OPT_DG_DEVICE
	},
	{'p', "property", "<name>=<value>", 0,
	    CLDES_OPT_DG_PROPERTY
	},
	{'i', "input", "- | <clconfiguration>", 0,
	    CLDES_OPT_INPUT
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// Options for "add-device" subcommand
static cloptions_t cldevicegroup_adddevice_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'d', "device", "<device>", CLOPT_ARG_LIST,
	    CLDES_OPT_DG_DEVICE
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// Options for "add-node" subcommand
static cloptions_t cldevicegroup_addnode_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'t', "type", "<devicegroup-type>", CLOPT_ARG_LIST,
	    CLDES_OPT_DT
	},
	{'n', "node", "<node>", CLOPT_ARG_LIST,
	    CLDES_OPT_NODE
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// Options for "delete" subcommand
static cloptions_t cldevicegroup_delete_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'t', "type", "<devicegroup-type>", CLOPT_ARG_LIST,
	    CLDES_OPT_DT
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// Options for "disable" subcommand
static cloptions_t cldevicegroup_disable_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{'t', "type", "<devicegroup-type>", CLOPT_ARG_LIST,
	    CLDES_OPT_DT
	},
	{0, 0, 0, 0, 0}
};

// Options for "enable" subcommand
static cloptions_t cldevicegroup_enable_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{'t', "type", "<devicegroup-type>", CLOPT_ARG_LIST,
	    CLDES_OPT_DT
	},
	{0, 0, 0, 0, 0}
};

// Options for "export" subcommand
static cloptions_t cldevicegroup_export_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{'o', "output", "{- | <clconfiguration>}", 0,
	    CLDES_OPT_OUTPUT
	},
	{'t', "type", "<devicegroup-type>", CLOPT_ARG_LIST,
	    CLDES_OPT_DT
	},
	{'n', "node", "<node>", CLOPT_ARG_LIST,
	    CLDES_OPT_NODE
	},
	{0, 0, 0, 0, 0}
};

// Options for "list" subcommand
static cloptions_t cldevicegroup_list_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{'t', "type", "<devicegroup-type>", CLOPT_ARG_LIST,
	    CLDES_OPT_DT
	},
	{'n', "node", "<node>", CLOPT_ARG_LIST,
	    CLDES_OPT_NODE
	},
	{0, 0, 0, 0, 0}
};

// Options for "set" subcommand
static cloptions_t cldevicegroup_set_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{'t', "type", "<devicegroup-type>", CLOPT_ARG_LIST,
	    CLDES_OPT_DT
	},
	{'n', "node", "<node>", CLOPT_ARG_LIST,
	    CLDES_OPT_NODE
	},
	{'d', "device", "<device>", CLOPT_ARG_LIST,
	    CLDES_OPT_DG_DEVICE
	},
	{'p', "property", "<name>=<value>", 0,
	    CLDES_OPT_DG_PROPERTY
	},
	{0, 0, 0, 0, 0}
};

// Options for "offline" subcommand
static cloptions_t cldevicegroup_offline_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{'t', "type", "<devicegroup-type>", CLOPT_ARG_LIST,
	    CLDES_OPT_DT
	},
	{0, 0, 0, 0, 0}
};

// Options for "online" subcommand
static cloptions_t cldevicegroup_online_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{'t', "type", "<devicegroup-type>", CLOPT_ARG_LIST,
	    CLDES_OPT_DT
	},
	{'n', "node", "<node>", 0,
	    CLDES_OPT_SINGLE_NODE
	},
	{'e', "enable", 0, 0,
	    CLDES_OPT_ENABLE
	},
	{0, 0, 0, 0, 0}
};

// Options for "remove-device" subcommand
static cloptions_t cldevicegroup_removedevice_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'d', "device", "<device>", CLOPT_ARG_LIST,
	    CLDES_OPT_DG_DEVICE
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

//  Options for "remove-node" subcommand
static cloptions_t cldevicegroup_removenode_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'n', "node", "<node>", CLOPT_ARG_LIST,
	    CLDES_OPT_NODE
	},
	{'t', "type", "<devicegroup-type>", CLOPT_ARG_LIST,
	    CLDES_OPT_DT
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

//  Options for "show" subcommand
static cloptions_t cldevicegroup_show_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{'t', "type", "<devicegroup-type>", CLOPT_ARG_LIST,
	    CLDES_OPT_DT
	},
	{'n', "node", "<node>", CLOPT_ARG_LIST,
	    CLDES_OPT_NODE
	},
	{0, 0, 0, 0, 0}
};

//  Options for "status" subcommand
static cloptions_t cldevicegroup_status_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{'t', "type", "<devicegroup-type>", CLOPT_ARG_LIST,
	    CLDES_OPT_DT
	},
	{'n', "node", "<node>", CLOPT_ARG_LIST,
	    CLDES_OPT_NODE
	},
	{0, 0, 0, 0, 0}
};

//  Options for "switch" subcommand
static cloptions_t cldevicegroup_switch_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'t', "type", "<devicegroup-type>", CLOPT_ARG_LIST,
	    CLDES_OPT_DT
	},
	{'n', "node", "<node>", 0,
	    CLDES_OPT_SINGLE_NODE
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{'e', "enable", 0, 0,
	    CLDES_OPT_ENABLE
	},
	{0, 0, 0, 0, 0}
};

//  Options for "sync" subcommand
static cloptions_t cldevicegroup_sync_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{'t', "type", "<devicegroup-type>", CLOPT_ARG_LIST,
	    CLDES_OPT_DT
	},
	{0, 0, 0, 0, 0}
};

// List of subcommands possible for "cldevicegroup" command
static clsubcommand_t cldevicegroup_subcommands[] = {
	{
		"create",
		CLSUB_DO_COMMAND_LOGGING,
		(char *)CLDG_SUBCMD_CREATE,
		CL_AUTH_CLUSTER_MODIFY,
		cldevicegroup_create_usage,
		cldevicegroup_create_optionslist,
		cldevicegroup_create_options
	},
	{
		"delete",
		CLSUB_DO_COMMAND_LOGGING,
		(char *)CLDG_SUBCMD_DELETE,
		CL_AUTH_CLUSTER_MODIFY,
		cldevicegroup_delete_usage,
		cldevicegroup_delete_optionslist,
		cldevicegroup_delete_options
	},
	{
		"disable",
		CLSUB_DO_COMMAND_LOGGING,
		(char *)CLDG_SUBCMD_DISABLE,
		CL_AUTH_CLUSTER_ADMIN,
		cldevicegroup_disable_usage,
		cldevicegroup_disable_optionslist,
		cldevicegroup_disable_options
	},
	{
		"enable",
		CLSUB_DO_COMMAND_LOGGING,
		(char *)CLDG_SUBCMD_ENABLE,
		CL_AUTH_CLUSTER_ADMIN,
		cldevicegroup_enable_usage,
		cldevicegroup_enable_optionslist,
		cldevicegroup_enable_options
	},
	{
		"export",
		CLSUB_DEFAULT,
		(char *)CLDG_SUBCMD_EXPORT,
		CL_AUTH_CLUSTER_READ,
		cldevicegroup_export_usage,
		cldevicegroup_export_optionslist,
		cldevicegroup_export_options
	},
	{
		"list",
		CLSUB_NON_GLOBAL_ZONE_ALLOWED,
		(char *)CLDG_SUBCMD_LIST,
		CL_AUTH_CLUSTER_READ,
		cldevicegroup_list_usage,
		cldevicegroup_list_optionslist,
		cldevicegroup_list_options
	},
	{
		"set",
		CLSUB_DO_COMMAND_LOGGING,
		(char *)CLDG_SUBCMD_SET,
		CL_AUTH_CLUSTER_MODIFY,
		cldevicegroup_set_usage,
		cldevicegroup_set_optionslist,
		cldevicegroup_set_options
	},
	{
		"show",
		CLSUB_DEFAULT,
		(char *)CLDG_SUBCMD_SHOW,
		CL_AUTH_CLUSTER_READ,
		cldevicegroup_show_usage,
		cldevicegroup_show_optionslist,
		cldevicegroup_show_options
	},
	{
		"status",
		CLSUB_NON_GLOBAL_ZONE_ALLOWED,
		(char *)CLDG_SUBCMD_STATUS,
		CL_AUTH_CLUSTER_READ,
		cldevicegroup_status_usage,
		cldevicegroup_status_optionslist,
		cldevicegroup_status_options
	},
	{
		"add-device",
		CLSUB_DO_COMMAND_LOGGING,
		(char *)CLDG_SUBCMD_ADD_DEVICE,
		CL_AUTH_CLUSTER_MODIFY,
		cldevicegroup_adddevice_usage,
		cldevicegroup_adddevice_optionslist,
		cldevicegroup_adddevice_options
	},
	{
		"remove-device",
		CLSUB_DO_COMMAND_LOGGING,
		(char *)CLDG_SUBCMD_REMOVE_DEVICE,
		CL_AUTH_CLUSTER_MODIFY,
		cldevicegroup_removedevice_usage,
		cldevicegroup_removedevice_optionslist,
		cldevicegroup_removedevice_options
	},
	{
		"add-node",
		CLSUB_DO_COMMAND_LOGGING,
		(char *)CLDG_SUBCMD_ADD_NODE,
		CL_AUTH_CLUSTER_MODIFY,
		cldevicegroup_addnode_usage,
		cldevicegroup_addnode_optionslist,
		cldevicegroup_addnode_options
	},
	{
		"remove-node",
		CLSUB_DO_COMMAND_LOGGING,
		(char *)CLDG_SUBCMD_REMOVE_NODE,
		CL_AUTH_CLUSTER_MODIFY,
		cldevicegroup_removenode_usage,
		cldevicegroup_removenode_optionslist,
		cldevicegroup_removenode_options
	},
	{
		"offline",
		CLSUB_DO_COMMAND_LOGGING,
		(char *)CLDG_SUBCMD_OFFLINE,
		CL_AUTH_CLUSTER_ADMIN,
		cldevicegroup_offline_usage,
		cldevicegroup_offline_optionslist,
		cldevicegroup_offline_options
	},
	{
		"online",
		CLSUB_DO_COMMAND_LOGGING,
		(char *)CLDG_SUBCMD_ONLINE,
		CL_AUTH_CLUSTER_ADMIN,
		cldevicegroup_online_usage,
		cldevicegroup_online_optionslist,
		cldevicegroup_online_options
	},
	{
		"switch",
		CLSUB_DO_COMMAND_LOGGING,
		(char *)CLDG_SUBCMD_SWITCH,
		CL_AUTH_CLUSTER_ADMIN,
		cldevicegroup_switch_usage,
		cldevicegroup_switch_optionslist,
		cldevicegroup_switch_options
	},
	{
		"sync",
		CLSUB_DO_COMMAND_LOGGING,
		(char *)CLDG_SUBCMD_SYNC,
		CL_AUTH_CLUSTER_MODIFY,
		cldevicegroup_sync_usage,
		cldevicegroup_sync_optionslist,
		cldevicegroup_sync_options
	},
	{0, 0, 0, 0, 0, 0, 0}
};

// Initialization properties for the "cldevicegroup" command
clcommandinit_t cldevicegroupinit = {
	CLCOMMANDS_COMMON_VERSION,
	(char *)CLDG_CMD_DESC,
	cldevicegroup_usage,
	cldevicegroup_subcommands
};
