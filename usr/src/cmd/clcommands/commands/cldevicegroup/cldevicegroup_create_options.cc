//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
#pragma ident	"@(#)cldevicegroup_create_options.cc	1.6	08/05/20 SMI"

//
// Options processing function for "create" subcommand
//

#include "cldevicegroup_cmd.h"

clerrno_t
cldevicegroup_create_options(ClCommand &cmd)
{
	int i;
	int c;
	int wildcard;
	int num_operands;
	int errcount = 0;
	char *option = 0;
	int option_index = 0;
	char *shortopts;
	struct option *longopts;
	ValueList operands;
	optflgs_t optflgs = 0;

	ValueList dgtypes, nodes, devices, fixed_dgtypes;
	NameValueList properties;
	char *clconfiguration = NULL;
	char *dg_type = NULL;
	int iflg = 0;
	int nodecheck = 1;
	char *localprop = NULL;

	// Get the short and long option lists
	shortopts = clsubcommand_get_shortoptions(cmd);
	longopts = clsubcommand_get_longoptions(cmd);

	// This subcommand should have options
	if (!shortopts || !longopts) {
		clerror("Aborting - subcommand initialization failed.\n");
		abort();
	}

	optind = 2;
	while ((c = getopt_clip(cmd.argc, cmd.argv, shortopts, longopts,
	    &option_index)) != -1) {

		// Get the option
		delete option;
		option = clcommand_option(optopt, cmd.argv[optind - 1]);

		// Switch on option letter
		switch (c) {
		case 'v':
			// Verbose option
			cmd.isVerbose = 1;
			optflgs |= vflg;
			break;

		case '?':
			// Help option?
			if ((strcmp(option, "-?") == 0) ||
			    (strcmp(option, "--help") == 0)) {
				optflgs |= hflg;
				break;
			}

			// Or, unrecognized option?
			clerror("Unrecognized option - \"%s\".\n", option);
			++errcount;
			break;

		case 'i':
			// Input file
			clconfiguration = optarg;
			iflg++;
			break;

		case 't':
			// Devicegroup type(s)
			dgtypes.add(1, optarg);
			break;

		case 'n':
			// Nodes
			nodes.add(1, optarg);
			break;

		case 'd':
			// Devices /dev/did/rdsk/d<N> for rawdisk dg
			devices.add(1, optarg);
			break;

		case 'p':
			// properties
			properties.add(0, optarg);
			break;

		default:
			// we should never reach here
			clerror("Unexpected option - \"%s\".\n", option);
			++errcount;
			break;
		}
	}

	delete option;
	option = NULL;

	// Errors ?
	if (errcount) {
		clcommand_help(cmd, cmd.getSubCommandName());
		return (CL_EINVAL);
	}

	// Just print help?
	if (optflgs & hflg) {
		clcommand_help(cmd, cmd.getSubCommandName());
		return (CL_NOERR);
	}

	// Get all operands or a wildcard
	wildcard = 0;
	num_operands = 0;
	for (i = optind;  i < cmd.argc;  ++i) {
		num_operands++;
		if (strcmp(cmd.argv[i], CLCOMMANDS_WILD_OPERAND) == 0) {
			++wildcard;
		}
	}
	// Update the operands list
	if (wildcard) {
		operands.add(CLCOMMANDS_WILD_OPERAND);
	} else {
		for (i = optind;  i < cmd.argc;  ++i)
			operands.add(cmd.argv[i]);
	}
	// Operands are needed
	if (operands.getSize() == 0) {
		clerror("Provide the name of the device group to create.\n");
		errcount++;
	}

	// Validations on options, arguments and wildcard usage
	if (!clconfiguration) {

		// Only 1 dgtype can be specified
		switch (dgtypes.getSize()) {
		case 0:
			clerror("You must specify a device group type.\n");
			errcount++;
			break;

		case 1:
			break;

		default:
			clerror("You can specify multiple device-group types "
			    "with the \"-i\" option.\n");
			clerror("Specify only one device group type.\n");
			errcount++;
			break;
		}

		//
		// Atleast one node need to be specified
		//
		if (dgtypes.getSize() == 1) {
			dgtypes.start();
			dg_type = dgtypes.getValue();
		}

		if (nodecheck && !nodes.getSize()) {
			clerror("Specify at least one node.\n");
			errcount++;
		}

		// devices can be used only with "rawdisk" dgtype
		if (devices.getSize() && (dgtypes.getSize() == 1)) {
			dgtypes.start();
			dg_type = dgtypes.getValue();
			if (dg_type != NULL &&
			    strcmp(dg_type, CLDG_RAWDISK_TYPE) != 0) {
				clerror("You can only specify the "
				    "\"-d\" option with the %s device "
				    "group type.\n", CLDG_RAWDISK_TYPE);
				errcount++;
			}
		}

		//
		// <name>=<value> should be specified with the -p option
		// <value>  is optional if <property> has a default value
		//
		for (properties.start(); !properties.end(); properties.next()) {
			if (properties.getValue() == NULL) {
				++errcount;
				clerror("Property \"%s\" must be in the form"
				    " <name>=<value>.\n", properties.getName());
			}
		}

		//
		// Only one operand (device group) is allowed.
		// And it should not be wildcard
		//
		if (num_operands > 1 || wildcard) {
			clerror("You can only create multiple device groups "
			    "by using the \"-i\" option.\n");
			clerror("Specify only one device group name.\n");
			errcount++;
		}
	} else {
		// With i option, user can either specify "+" or list. Not both
		if ((wildcard) && (num_operands > 1)) {
			++errcount;
			clerror("You can either specify a \"+\" or a list of "
			    "device groups.\n");
		}
	}

	// Errors ?
	if (errcount) {
		clcommand_help(cmd, cmd.getSubCommandName());
		return (CL_EINVAL);
	}

	// svm types should be mapped to sds
	cldevicegroup_fix_dgtype(dgtypes, fixed_dgtypes);

	if (!clconfiguration) {
		return (cldevicegroup_create(fixed_dgtypes, nodes, devices,
		    properties, operands, optflgs));
	} else {
		return (cldevicegroup_create(cmd, fixed_dgtypes, nodes, devices,
		    properties, operands, optflgs, clconfiguration));
	}
}
