//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
#pragma ident	"@(#)cldevicegroup_removenode_options.cc	1.3	08/05/20 SMI"

//
// Options processing function for "remove-node" subcommand
//

#include "cldevicegroup_cmd.h"

clerrno_t
cldevicegroup_removenode_options(ClCommand &cmd)
{
	int i;
	int c;
	int wildcard;
	int num_operands;
	int errcount = 0;
	char *option = 0;
	int option_index = 0;
	char *badoption;
	char *shortopts;
	struct option *longopts;
	ValueList operands;
	optflgs_t optflgs = 0;

	ValueList dgtypes, nodes, fixed_dgtypes;
	char *dg_type = NULL;

	// Get the short and long option lists
	shortopts = clsubcommand_get_shortoptions(cmd);
	longopts = clsubcommand_get_longoptions(cmd);

	// This subcommand should have options
	if (!shortopts || !longopts) {
		clerror("Aborting - subcommand initialization failed.\n");
		abort();
	}

	optind = 2;
	while ((c = getopt_clip(cmd.argc, cmd.argv, shortopts, longopts,
	    &option_index)) != -1) {

		// Get the option
		delete option;
		option = clcommand_option(optopt, cmd.argv[optind - 1]);

		// Switch on option letter
		switch (c) {
		case 'v':
			// Verbose option
			cmd.isVerbose = 1;
			optflgs |= vflg;
			break;

		case '?':
			// Help option?
			if ((strcmp(option, "-?") == 0) ||
			    (strcmp(option, "--help") == 0)) {
				optflgs |= hflg;
				break;
			}

			// Or, unrecognized option?
			clerror("Unrecognized option - \"%s\".\n", option);
			++errcount;
			break;

		case 't':
			// Devicegroup type(s)
			dgtypes.add(1, optarg);
			break;

		case 'n':
			// Nodes
			nodes.add(1, optarg);
			break;

		default:
			// we should never reach here
			clerror("Unexpected option - \"%s\".\n", option);
			++errcount;
			break;
		}
	}

	delete option;
	option = NULL;

	// Errors ?
	if (errcount) {
		clcommand_help(cmd, cmd.getSubCommandName());
		return (CL_EINVAL);
	}

	// Just print help?
	if (optflgs & hflg) {
		clcommand_help(cmd, cmd.getSubCommandName());
		return (CL_NOERR);
	}

	// Get all operands or a wildcard
	wildcard = 0;
	num_operands = 0;
	for (i = optind;  i < cmd.argc;  ++i) {
		num_operands++;
		if (strcmp(cmd.argv[i], CLCOMMANDS_WILD_OPERAND) == 0) {
			++wildcard;
		}
	}
	// Update the operands list
	if (wildcard) {
		operands.add(CLCOMMANDS_WILD_OPERAND);
	} else {
		for (i = optind;  i < cmd.argc;  ++i)
			operands.add(cmd.argv[i]);
	}

	// Validations on options, arguments and wildcard usage
	if (num_operands == 0) {
		clerror("Specify the name of the device group to modify.\n");
		errcount++;
	}

	// wildcard or individual operands. Not both
	if ((wildcard) && (num_operands > 1)) {
		++errcount;
		clerror("You can either specify a \"+\" or a list of "
		    "device groups.\n");
	}

	// Atleast one node should be specified.
	if (nodes.getSize() < 1) {
		++errcount;
		clerror("Specify at least one node to remove from the "
		    "device group.\n");
	}

	// Errors ?
	if (errcount) {
		clcommand_help(cmd, cmd.getSubCommandName());
		return (CL_EINVAL);
	}

	// svm types should be mapped to sds
	cldevicegroup_fix_dgtype(dgtypes, fixed_dgtypes);

	return (cldevicegroup_removenode(fixed_dgtypes, nodes, operands,
	    optflgs));
}
