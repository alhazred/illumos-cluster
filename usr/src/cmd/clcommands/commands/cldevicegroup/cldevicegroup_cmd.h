//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef	_CLDEVICEGROUP_CMD_H
#define	_CLDEVICEGROUP_CMD_H

#pragma ident	"@(#)cldevicegroup_cmd.h	1.3	08/05/20 SMI"

//
// Header file for command processing functions for cldevicegroup(1M) command
//
#include <cldevicegroup.h>

#define	CLDG_SUBCMD_CREATE		\
	GETTEXT("Create device groups")
#define	CLDG_SUBCMD_DELETE		\
	GETTEXT("Delete device groups")
#define	CLDG_SUBCMD_DISABLE		\
	GETTEXT("Disable device groups")
#define	CLDG_SUBCMD_ENABLE		\
	GETTEXT("Enable device groups")
#define	CLDG_SUBCMD_EXPORT		\
	GETTEXT("Export device group configuration")
#define	CLDG_SUBCMD_LIST		\
	GETTEXT("List device groups")
#define	CLDG_SUBCMD_SET			\
	GETTEXT("Set device group properties")
#define	CLDG_SUBCMD_SHOW		\
	GETTEXT("Show device groups and their properties")
#define	CLDG_SUBCMD_STATUS		\
	GETTEXT("Display device group status")
#define	CLDG_SUBCMD_ADD_DEVICE		\
	GETTEXT("Add devices to device groups")
#define	CLDG_SUBCMD_REMOVE_DEVICE	\
	GETTEXT("Remove devices from device groups")
#define	CLDG_SUBCMD_ADD_NODE		\
	GETTEXT("Add nodes to device groups")
#define	CLDG_SUBCMD_REMOVE_NODE		\
	GETTEXT("Remove nodes from device groups")
#define	CLDG_SUBCMD_OFFLINE		\
	GETTEXT("Take device groups offline")
#define	CLDG_SUBCMD_ONLINE		\
	GETTEXT("Bring device groups online")
#define	CLDG_SUBCMD_SWITCH		\
	GETTEXT("Switch device groups to a node")
#define	CLDG_SUBCMD_SYNC		\
	GETTEXT("Synchronize the volume and replication information")
#define	CLDG_CMD_DESC			\
	GETTEXT("Manage cluster device groups")

extern clcommandinit_t cldevicegroupinit;

//
// Functions that does option processing for individual
// subcommands of cldevicegroup(1CL)
//

extern clerrno_t cldevicegroup_create_options(ClCommand &cmd);
extern clerrno_t cldevicegroup_delete_options(ClCommand &cmd);
extern clerrno_t cldevicegroup_disable_options(ClCommand &cmd);
extern clerrno_t cldevicegroup_enable_options(ClCommand &cmd);
extern clerrno_t cldevicegroup_export_options(ClCommand &cmd);
extern clerrno_t cldevicegroup_list_options(ClCommand &cmd);
extern clerrno_t cldevicegroup_set_options(ClCommand &cmd);
extern clerrno_t cldevicegroup_show_options(ClCommand &cmd);
extern clerrno_t cldevicegroup_status_options(ClCommand &cmd);
extern clerrno_t cldevicegroup_adddevice_options(ClCommand &cmd);
extern clerrno_t cldevicegroup_removedevice_options(ClCommand &cmd);
extern clerrno_t cldevicegroup_addnode_options(ClCommand &cmd);
extern clerrno_t cldevicegroup_removenode_options(ClCommand &cmd);
extern clerrno_t cldevicegroup_offline_options(ClCommand &cmd);
extern clerrno_t cldevicegroup_online_options(ClCommand &cmd);
extern clerrno_t cldevicegroup_switch_options(ClCommand &cmd);
extern clerrno_t cldevicegroup_sync_options(ClCommand &cmd);

#endif /* _CLDEVICEGROUP_CMD_H */
