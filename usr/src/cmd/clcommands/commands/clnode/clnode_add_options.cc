//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clnode_add_options.cc	1.4	08/05/20 SMI"

//
// Process clnode "add" subcommand options
//
#include "clnode_cmd.h"

#define	COMMA	","

static int count_tok(char *string, char *token);
bool is_version_s9();

//
// clnode "add" options
//
clerrno_t
clnode_add_options(ClCommand &cmd)
{
	int i, c;
	int errcount = 0;
	char *cl_option = 0;
	int option_index = 0;
	int wildcard = 0;
	char *shortopts;
	struct option *longopts;
	ValueList operands;
	optflgs_t optflgs = 0;
	int nflg, cflg, iflg, gflg;
	int edup_err;
	NameValueList properties = NameValueList(true);
	ValueList endpoints = ValueList(true);
	NameValueList chk_endpoints = NameValueList(true);
	char *clconfiguration = (char *)0;
	char *clustername = (char *)0;
	char *gmntpoint = (char *)0;
	char *sponsornode = (char *)0;
	char local_node[BUFSIZ];
	int invalid = 0;
	int cable_count = 0;
	// Get the short and long option lists
	shortopts = clsubcommand_get_shortoptions(cmd);
	longopts = clsubcommand_get_longoptions(cmd);

	// This subcommand should have options
	if (!shortopts || !longopts) {
		clerror("Aborting - subcommand initialization failed.\n");
		abort();
	}

	// Initialize
	edup_err = 0;
	nflg = cflg = iflg = gflg = 0;

	optind = 2;
	while ((c = getopt_clip(cmd.argc, cmd.argv, shortopts, longopts,
	    &option_index)) != -1) {

		// Get the option
		delete cl_option;
		cl_option = clcommand_option(optopt, cmd.argv[optind - 1]);

		// Switch on option letter
		switch (c) {
		case 'v':
			// Verbose option
			cmd.isVerbose = 1;
			optflgs |= vflg;
			break;

		case '?':
			// Help option?
			if ((strcmp(cl_option, "-?") == 0) ||
			    (strcmp(cl_option, "--help") == 0)) {
				optflgs |= hflg;
				break;
			}

			// Or, unrecognized option?
			clerror("Unrecognized option - \"%s\".\n", cl_option);
			++errcount;
			break;

		case 'c':
			// Cluster name
			clustername = optarg;
			cflg++;
			break;

		case 'e':
			// End point is always two and separated by ",".
			if (count_tok(optarg, ",") != 1) {
				clerror("Cable \"%s\" is not a pair of "
				    "endpoints separated by a comma "
				    "(\"%s\").\n", optarg, COMMA);
				++errcount;
				break;
			} else if (!strstr(optarg, ":")) {
				// Must have adapter specified in the endpoint
				clerror("No \"node:adapter\" is specified in "
				    "cable \"%s\".\n", optarg);
				++errcount;
			}
			if (chk_endpoints.add(optarg, "endpoint") != 0) {
				edup_err++;
			} else {
				endpoints.add(0, optarg);
			}
			break;
		case 'G':
			// Global device mountpoint
			gmntpoint = optarg;
			// Must start with "/" or sholuld be "lofi"
			if (!(*gmntpoint == '/' || (strlen(gmntpoint) ==
			    (sizeof (LOFI_METHOD) - 1) &&
			    strncmp(gmntpoint, LOFI_METHOD,
			    sizeof (LOFI_METHOD) - 1) == 0))) {
				clerror("The global file system \"%s\" "
				    "is not an absolute path or lofi.\n",
				    gmntpoint);
				++errcount;
			}
			// Use -G lofi if solaris version is S10 and above
			if (strncmp(gmntpoint, LOFI_METHOD,
			    sizeof (LOFI_METHOD)) == 0 && is_version_s9()) {
			    clerror("The lofi option for global devices is not "
				"supported on the Solaris 9 OS.\n");
			    ++errcount;
			}
			gflg++;
			break;

		case 'n':
			// Sponsor node
			sponsornode = optarg;
			nflg++;
			break;

		case 'i':
			// Input file
			clconfiguration = optarg;
			iflg++;
			break;

		case ':':
			// Missing argument
			clerror("Option \"%s\" requires an argument.\n",
			    cl_option);
			++errcount;
			break;

		default:
			//
			// We should never get here.
			// If we do, it means that there
			// is an option in our option list
			// for which there is no case statement.
			// We either have a bad option list or
			// a bad case statement.
			//
			clerror("Unexpected option - \"%s\".\n", cl_option);
			++errcount;
			break;
		}
	}
	delete cl_option;
	cl_option = (char *)0;

	// Check for multiple options
	if (cflg > 1) {
		clerror("You can only specify the \"-c\" option once.\n");
		++errcount;
	}
	if (gflg > 1) {
		clerror("You can only specify the \"-G\" option once.\n");
		++errcount;
	}
	if (nflg > 1) {
		clerror("You can only specify the \"-n\" option once.\n");
		++errcount;
	}
	if (iflg > 1) {
		clerror("You can only specify the \"-i\" option once.\n");
		++errcount;
	}
	if (edup_err != 0) {
		clerror("Each pair of endpionts must be different.\n");
		++errcount;
	}

	// Errors?
	if (errcount) {
		clcommand_usage(cmd, cmd.getSubCommandName(), errcount);
		return (CL_EINVAL);
	}

	// Just print help?
	if (optflgs & hflg) {
		clcommand_help(cmd, cmd.getSubCommandName());
		return (CL_NOERR);
	}

	// Check for required input
	if (!sponsornode) {
		clerror("You must specify the sponsor node.\n");
		++errcount;
	}

	// Required endpoints
	cable_count = endpoints.getSize();
	if (!clconfiguration) {
		if (cable_count == 0) {
			clerror("You must specify the "
			    "interconnect cables.\n");
			++errcount;
		}
	}
	if (cable_count > 2) {
		clerror("You cannot specify more than two cables.\n");
		++errcount;
	}

	// Update the operands list
	for (i = optind;  i < cmd.argc;  ++i) {
		operands.add(cmd.argv[i]);
		if (strcmp(cmd.argv[i], CLCOMMANDS_WILD_OPERAND) == 0) {
			++wildcard;
		}
	}

	// Cannot use wildcard
	if (wildcard) {
		clerror("You cannot use operand \"%s\" with this subcommand.\n",
		    CLCOMMANDS_WILD_OPERAND);
		++errcount;
	} else {
		// Get the local node
		local_node[0] = '\0';
		(void) gethostname(local_node, sizeof (local_node));

		switch (operands.getSize()) {
		case 0:
			// Add the local node
			operands.add(local_node);
			break;
		case 1:
			operands.start();
			if (strcmp(local_node, operands.getValue()) != 0) {
				clerror("You can only add the local node.\n");
				++invalid;
			}
			break;
		default:
			clerror("You can only add one node at a time "
			    "to the cluster.\n.");
			++errcount;
			break;
		}
	}

	if (errcount) {
		clcommand_usage(cmd, cmd.getSubCommandName(), errcount);
		return (CL_EINVAL);
	}

	if (invalid) {
		return (CL_EINVAL);
	}

	//
	// Perform clnode "add" operation. The properties are empty for now,
	// and will be added in the future.
	//
	if (!clconfiguration) {
		// "add" with no -i
		return (clnode_add(cmd, operands, optflgs, properties,
		    sponsornode, clustername, gmntpoint, endpoints));
	} else {
		// "add" mode with -i
		return (clnode_add(cmd, operands, optflgs, clconfiguration,
		    properties, sponsornode, clustername, gmntpoint,
		    endpoints));
	}
}

//
// count_tok
//
//	Return how many times the token appears in the string.
//
int
count_tok(char *string, char *token)
{
	int count = 0;
	char *str_buffer = (char *)0;

	if (!string || !token)
		return (0);

	// Save the string
	str_buffer = strdup(string);
	if (!str_buffer) {
		return (0);
	}

	// Check the token
	char *chk_string = strtok(str_buffer, token);
	while ((chk_string = strtok(NULL, token)) != NULL) count++;

	// Free memory
	free(str_buffer);

	return (count);
} //lint !e550

//
// is_version_s9
//
//	Check for Solaris version 9.
//
bool
is_version_s9()
{
	char version[128];
	(void) sysinfo(SI_RELEASE, version, 128);
	return (!(bool) strncmp(version, S9, sizeof (S9)));
}
