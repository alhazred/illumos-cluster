//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clnode_set_options.cc	1.3	08/05/20 SMI"

//
// Process clnode "set" subcommand options
//

#include "clnode_cmd.h"

//
// clnode "set" options
//
clerrno_t
clnode_set_options(ClCommand &cmd)
{
	int i;
	int c;
	int errcount = 0;
	char *cl_option = 0;
	int option_index = 0;
	int wildcard = 0;
	char *shortopts;
	struct option *longopts;
	ValueList operands;
	optflgs_t optflgs = 0;

	NameValueList properties = NameValueList(true);
	ValueList nodetypes = ValueList(true);

	// Get the short and long option lists
	shortopts = clsubcommand_get_shortoptions(cmd);
	longopts = clsubcommand_get_longoptions(cmd);

	// This subcommand should have options
	if (!shortopts || !longopts) {
		clerror("Aborting - subcommand initialization failed.\n");
		abort();
	}

	optind = 2;
	while ((c = getopt_clip(cmd.argc, cmd.argv, shortopts, longopts,
	    &option_index)) != -1) {

		// Get the option
		delete cl_option;
		cl_option = clcommand_option(optopt, cmd.argv[optind - 1]);

		// Switch on option letter
		switch (c) {
		case 'v':
			// Verbose option
			cmd.isVerbose = 1;
			optflgs |= vflg;
			break;

		case '?':
			// Help option?
			if ((strcmp(cl_option, "-?") == 0) ||
			    (strcmp(cl_option, "--help") == 0)) {
				optflgs |= hflg;
				break;
			}

			// Or, unrecognized option?
			clerror("Unrecognized option - \"%s\".\n", cl_option);
			++errcount;
			break;

		case 't':
			// node type
			nodetypes.add(1, optarg);
			break;

		case 'p':
			// Properties
			if (strchr(optarg, '=') == NULL) {
				clerror("You did not specify a property value "
				    "in \"%s\".\n", optarg);
				++errcount;
			} else if (strchr(optarg, '=') !=
			    strrchr(optarg, '=')) {
				clerror("You must specify one \"-p\" for each "
				    "property in \"%s\".\n",
				    optarg);
				++errcount;
			} else if (properties.add(1, optarg) != 0) {
				clerror("You specified the same property name "
				    "multiple times in \"%s\".\n",
				    optarg);
				++errcount;
			}
			break;

		case ':':
			// Missing argument
			clerror("Option \"%s\" requires an argument.\n",
			    cl_option);
			++errcount;
			break;

		default:
			//
			// We should never get here.
			// If we do, it means that there
			// is an option in our option list
			// for which there is no case statement.
			// We either have a bad option list or
			// a bad case statement.
			//
			clerror("Unexpected option - \"%s\".\n", cl_option);
			++errcount;
			break;
		}
	}
	delete cl_option;
	cl_option = (char *)0;

	// Errors?
	if (errcount) {
		clcommand_usage(cmd, cmd.getSubCommandName(), errcount);
		return (CL_EINVAL);
	}

	// Just print help?
	if (optflgs & hflg) {
		clcommand_help(cmd, cmd.getSubCommandName());
		return (CL_NOERR);
	}

	// If type is specified, it must be just one
	if (nodetypes.getSize() > 1) {
		++errcount;
		clerror("You can only specify one type of "
		    "node to modify.\n");
	} else if (nodetypes.getSize() == 1) {
		// Valid type?
		nodetypes.start();
		char *node_type = nodetypes.getValue();
		if ((strcmp(node_type, NODETYPE_SERVER) != 0) &&
		    (strcmp(node_type, NODETYPE_FARM) != 0)) {
			clerror("Invalid node type \"%s\".\n", node_type);
			return (CL_ETYPE);
		}
	}

	// There must be a property
	if (properties.getSize() == 0) {
		++errcount;
		clerror("You must specify the properties to modify.\n");
	}

	// Update operands list
	for (i = optind;  i < cmd.argc;  ++i) {
		if (strcmp(cmd.argv[i], CLCOMMANDS_WILD_OPERAND) == 0) {
			++wildcard;
		} else {
			operands.add(cmd.argv[i]);
		}
	}

	// Wildcard?
	if (wildcard) {
		if (operands.getSize() > 0) {
			++errcount;
			clerror("You cannot specify both \"%s\" and nodes "
			    "at the same time.\n", CLCOMMANDS_WILD_OPERAND);
		}
	} else {
		// There must be at least one node
		if (operands.getSize() == 0) {
			++errcount;
			clerror("You must specify the node to modify.\n");
		}
	}

	if (errcount) {
		clcommand_usage(cmd, cmd.getSubCommandName(), errcount);
		return (CL_EINVAL);
	}

	//
	// Perform clnode "set" operation
	//
	return (clnode_set(cmd, operands, optflgs,
	    nodetypes, properties));
}
