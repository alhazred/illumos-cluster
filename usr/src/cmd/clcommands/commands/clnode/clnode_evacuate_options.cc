//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clnode_evacuate_options.cc	1.2	08/05/20 SMI"

//
// Process clnode "evacuate" subcommand options
//

#include "clnode_cmd.h"

//
// clnode "evacuate" options
//
clerrno_t
clnode_evacuate_options(ClCommand &cmd)
{
	int i, c;
	int errcount = 0;
	char *cl_option = 0;
	int option_index = 0;
	char *shortopts;
	struct option *longopts;
	ValueList operands;
	optflgs_t optflgs = 0;
	char *timeout = (char *)0;
	int tflg = 0;
	int e_timeout = -1;
	ushort_t evac_timeout = 60;
	char junk[4];

	// Get the short and long option lists
	shortopts = clsubcommand_get_shortoptions(cmd);
	longopts = clsubcommand_get_longoptions(cmd);

	// This subcommand should have options
	if (!shortopts || !longopts) {
		clerror("Aborting - subcommand initialization failed.\n");
		abort();
	}

	optind = 2;
	while ((c = getopt_clip(cmd.argc, cmd.argv, shortopts, longopts,
	    &option_index)) != -1) {

		// Get the option
		delete cl_option;
		cl_option = clcommand_option(optopt, cmd.argv[optind - 1]);

		// Switch on option letter
		switch (c) {
		case 'v':
			// Verbose option
			cmd.isVerbose = 1;
			optflgs |= vflg;
			break;

		case '?':
			// Help option?
			if ((strcmp(cl_option, "-?") == 0) ||
			    (strcmp(cl_option, "--help") == 0)) {
				optflgs |= hflg;
				break;
			}

			// Or, unrecognized option?
			clerror("Unrecognized option - \"%s\".\n", cl_option);
			++errcount;
			break;

		case 'T':
			// Timeout
			timeout = optarg;
			tflg++;
			break;

		case ':':
			// Missing argument
			clerror("Option \"%s\" requires an argument.\n",
			    cl_option);
			++errcount;
			break;

		default:
			//
			// We should never get here.
			// If we do, it means that there
			// is an option in our option list
			// for which there is no case statement.
			// We either have a bad option list or
			// a bad case statement.
			//
			clerror("Unexpected option - \"%s\".\n", cl_option);
			++errcount;
			break;
		}
	}
	delete cl_option;
	cl_option = (char *)0;

	// Check for duplicated options
	if (tflg > 1) {
		clerror("You can only specify the \"-T\" option once.\n");
		++errcount;
	}

	// Errors?
	if (errcount) {
		clcommand_usage(cmd, cmd.getSubCommandName(), errcount);
		return (CL_EINVAL);
	}

	// Just print help?
	if (optflgs & hflg) {
		clcommand_help(cmd, cmd.getSubCommandName());
		return (CL_NOERR);
	}

	// Get the timeout
	if (timeout) {
		i = sscanf(timeout, "%d%1s", &e_timeout, junk);
		if (i != 1) {
			++errcount;
			clerror("You must specify an integer, in seconds, for "
			    "the evacuate timeout.\n");
		} else if (e_timeout < 0 || e_timeout > (int)UINT16_MAX) {
			++errcount;
			clerror("The evacuate timeout must be an integer "
			    "between 0 and %d.\n", (int)UINT16_MAX);
		} else {
			evac_timeout = (ushort_t)e_timeout;
		}
	}

	// Add the operands list
	for (i = optind;  i < cmd.argc;  ++i) {
		operands.add(cmd.argv[i]);
	}

	// Must have operands
	if (operands.getSize() == 0) {
		++errcount;
		clerror("You must specify the node to evacuate.\n");
	}

	// Cannot specify more than one operand
	if (operands.getSize() > 1) {
		++errcount;
		clerror("You cannot specify more than one node to evacuate.\n");
	}

	if (errcount) {
		clcommand_usage(cmd, cmd.getSubCommandName(), errcount);
		return (CL_EINVAL);
	}

	// Perform clnode "evacuate" operation
	return (clnode_evacuate(cmd, operands, optflgs,
	    evac_timeout));
}
