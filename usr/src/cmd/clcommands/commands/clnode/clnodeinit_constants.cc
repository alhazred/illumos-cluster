//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clnodeinit_constants.cc	1.16	09/01/16 SMI"

//
// Initialization data for clnode(1CL)
//

#include "clnode_cmd.h"
#include "sys/sol_version.h"

//
// Usage Messages
//
// All usage message lines should begin with a single "%s" for
// the command name. Lines must never include more than a single "%s".
//

// List of "clnode" command usage message
static char *clnode_usage [] = {
	"%s <subcommand> [<options>] [+ | <node> ...]",
	"%s [<subcommand>]  -? | --help",
	"%s  -V | --version",
	(char *)0
};

// List of clnode "add" subcommand usage messages
static char *clnode_add_usage[] = {
	"%s add -n <sponsornode> -e <endpoint>,<endpoint> [<options>] [<node>]",
	"%s add -i <configfile> -n <sponsornode> [<options>] [<node>]",
	(char *)0
};

// List of clnode "add_farm" subcommand usage messages
static char *clnode_add_farm_usage[] = {
	"%s add-farm -p adapterlist=<adapter>[,...] [<options>] <node> ...",
	"%s add-farm -i <configfile>  [<options>] + | <node> ...",
	(char *)0
};

// List of clnode "clear" subcommand usage messages
static char *clnode_clear_usage[] = {
	"%s clear [<options>] <node> ...",
	(char *)0
};

// List of clnode "evacuate" subcommand usage messages
static char *clnode_evacuate_usage[] = {
	"%s evacuate [<options>] <node>",
	(char *)0
};

// List of clnode "export" subcommand usage messages
static char *clnode_export_usage[] = {
	"%s export [<options>] [+ | <node> ...]",
	(char *)0
};

// List of clnode "list" subcommand usage messages
static char *clnode_list_usage[] = {
	"%s list [<options>] [+ | <node> ...]",
	(char *)0
};

// List of clnode "monitor-farm" subcommand usage messages
static char *clnode_monitor_usage[] = {
	"%s monitor-farm [<options>] {+ | <node> ...}",
	(char *)0
};

// List of clnode "show_rev" subcommand usage messages
static char *clnode_show_rev_usage[] = {
	"%s show-rev [<options>] [<node>]",
	(char *)0
};

// List of clnode "set" subcommand usage messages
static char *clnode_set_usage[] = {
#if SOL_VERSION < __s10
	"%s set -p <name>=<value> [<options>] + | <node> ...",
#else
	"%s set -p <name>=<value> [<options>] "
	"+ | <node>[:<zone>] ...",
#endif
	(char *)0
};

// List of clnode "remove" subcommand usage messages
static char *clnode_remove_usage[] = {
	"%s remove [<options>] [<node>]",
	(char *)0
};

// List of clnode "remove-farm" subcommand usage messages
static char *clnode_remove_farm_usage[] = {
	"%s remove-farm [<options>] + | <node> ...",
	(char *)0
};

// List of clnode "show" subcommand usage messages
static char *clnode_show_usage[] = {
	"%s show [<options>] [+ | <node> ...]",
	(char *)0
};

// List of clnode "status" subcommand usage messages
static char *clnode_status_usage[] = {
	"%s status [<options>] [+ | <node> ...]",
	(char *)0
};

#ifdef WEAK_MEMBERSHIP
// List of clnode "resolve_config" subcommand usage messages
static char *clnode_resolve_config_usage[] = {
	"%s resolve-config [<options>] -n <winnernode> [<localnode>]",
	(char *)0
};
#endif // WEAK_MEMBERSHIP

// List of clnode "unmonitor-farm" subcommand usage messages
static char *clnode_unmonitor_usage[] = {
	"%s unmonitor-farm [<options>] {+ | <node> ...}",
	(char *)0
};

//
// Option lists
//
// All option descriptions should be broken up into lines of no
// greater than 70 characters.  All but the trailing newline ('\n')
// itself should be included in the option descriptions found in these
// tables.
//

// List of options for the clnode "add" subcommand
static cloptions_t clnode_add_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'c', "clustername", "<clustername>", 0,
	    CLDES_OPT_CLUSTER
	},
	{'e', "endpoint", "<endpoint>,<endpoint>", 0,
	    CLDES_OPT_ENDPOINT
	},
	{'G', "globaldevfs", "<globaldevfs>", 0,
	    CLDES_OPT_GLBMNT
	},
	{'i', "input", "{- | <clconfiguration>}", 0,
	    CLDES_OPT_INPUT
	},
	{'n', "sponsornode", "<sponsornode>", 0,
	    CLDES_OPT_SNODE
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the clnode "add-farm" subcommand
static cloptions_t clnode_add_farm_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'i', "input", "{- | <clconfiguration>}", 0,
	    CLDES_OPT_INPUT
	},
	{'p', "property", "<name>=<value>", 0,
	    CLDES_OPT_PROPERTY
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the clnode "clear" subcommand
static cloptions_t clnode_clear_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'F', "force", 0, 0,
	    CLDES_OPT_FORCE
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the clnode "evacuate" subcommand
static cloptions_t clnode_evacuate_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'T', "time", "<seconds>", 0,
	    CLDES_OPT_TIME
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the clnode "export" subcommand
static cloptions_t clnode_export_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'o', "output", "{- | <clconfiguration>}", 0,
	    CLDES_OPT_OUTPUT
	},
	{'t', "type", "<type>", CLOPT_UNDOCUMENTED,
	    CLDES_OPT_NTYPE
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the clnode "list" subcommand
static cloptions_t clnode_list_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'Z', "zonecluster", "{all | global | <zone-cluster-name>}", 0,
	    CLDES_OPT_ZC_NAME
	},
	{'t', "type", "<type>", CLOPT_UNDOCUMENTED,
	    CLDES_OPT_NTYPE
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the clnode "monitor-farm" subcommand
static cloptions_t clnode_monitor_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the clnode "show-rev" subcommand
static cloptions_t clnode_show_rev_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the clnode "set" subcommand
static cloptions_t clnode_set_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'p', "property", "<name>=<value>", 0,
	    CLDES_OPT_PROPERTY
	},
	{'t', "type", "<type>", CLOPT_UNDOCUMENTED,
	    CLDES_OPT_NTYPE
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the clnode "remove" subcommand
static cloptions_t clnode_remove_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'F', "force", 0, 0,
	    CLDES_OPT_FORCE
	},
	{'G', "globaldevfs", "<globaldevfs>", 0,
	    CLDES_OPT_GLBMNT
	},
	{'n', "sponsornode", "<sponsornode>", 0,
	    CLDES_OPT_SNODE
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the clnode "remove-farm" subcommand
static cloptions_t clnode_remove_farm_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the clnode "show" subcommand
static cloptions_t clnode_show_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'Z', "zonecluster", "{all | global | <zone-cluster-name>}", 0,
	    CLDES_OPT_ZC_NAME
	},
	{'t', "type", "<type>", CLOPT_UNDOCUMENTED,
	    CLDES_OPT_NTYPE
	},
	{'p', "property", "<name>", CLOPT_ARG_LIST,
	    CLDES_OPT_PROPERTY
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the clnode "status" subcommand
static cloptions_t clnode_status_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'Z', "zonecluster", "{all | global | <zone-cluster-name>}", 0,
	    CLDES_OPT_ZC_NAME
	},
	{'m', "ipmp", 0, 0,
	    CLDES_OPT_IPMP
	},
	{'t', "type", "<type>", CLOPT_UNDOCUMENTED,
	    CLDES_OPT_NTYPE
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

#ifdef WEAK_MEMBERSHIP
// List of options for the clnode "resolve-config" subcommand
static cloptions_t clnode_resolve_config_optionlist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'n', "winnernode", "<winnernode>", 0,
	    CLDES_OPT_WNODE
	},
	{'F', "force", 0, 0,
	    CLDES_OPT_FORCE
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};
#endif // WEAK_MEMBERSHIP

// List of options for the clnode "unmonitor-farm" subcommand
static cloptions_t clnode_unmonitor_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of subcommands for the "clnode" command
static clsubcommand_t clnode_subcommands[] = {
	{
		"add-farm",
		CLSUB_DO_COMMAND_LOGGING | CLSUB_CHECK_EUROPA,
		(char *)CLNODE_SUBCMD_ADD_FARM,
		CL_AUTH_CLUSTER_MODIFY,
		clnode_add_farm_usage,
		clnode_add_farm_optionslist,
		clnode_add_farm_options
	},
	{
		"add",
		CLSUB_NCLMODE_REQUIRED,
		(char *)CLNODE_SUBCMD_ADD,
		CL_AUTH_CLUSTER_MODIFY,
		clnode_add_usage,
		clnode_add_optionslist,
		clnode_add_options
	},
	{
		"clear",
		CLSUB_DO_COMMAND_LOGGING,
		(char *)CLNODE_SUBCMD_CLEAR,
		CL_AUTH_CLUSTER_MODIFY,
		clnode_clear_usage,
		clnode_clear_optionslist,
		clnode_clear_options
	},
	{
		"evacuate",
		CLSUB_DO_COMMAND_LOGGING |
			CLSUB_ZONE_CLUSTER_ALLOWED,
		(char *)CLNODE_SUBCMD_EVACUATE,
		CL_AUTH_CLUSTER_MODIFY,
		clnode_evacuate_usage,
		clnode_evacuate_optionslist,
		clnode_evacuate_options
	},
	{
		"export",
		CLSUB_DEFAULT,
		(char *)CLNODE_SUBCMD_EXPORT,
		CL_AUTH_CLUSTER_READ,
		clnode_export_usage,
		clnode_export_optionslist,
		clnode_export_options
	},
	{
		"list",
		CLSUB_NON_GLOBAL_ZONE_ALLOWED |
		CLSUB_ZONE_CLUSTER_ALLOWED,
		(char *)CLNODE_SUBCMD_LIST,
		CL_AUTH_CLUSTER_READ,
		clnode_list_usage,
		clnode_list_optionslist,
		clnode_list_options
	},
	{
		"monitor-farm",
		CLSUB_DO_COMMAND_LOGGING | CLSUB_CHECK_EUROPA,
		(char *)CLNODE_SUBCMD_MONITOR,
		CL_AUTH_CLUSTER_MODIFY,
		clnode_monitor_usage,
		clnode_monitor_optionslist,
		clnode_monitor_options
	},
	{
		"show-rev",
		CLSUB_NCLMODE_ALLOWED | CLSUB_NON_GLOBAL_ZONE_ALLOWED,
		(char *)CLNODE_SUBCMD_SHOW_REV,
		CL_AUTH_CLUSTER_READ,
		clnode_show_rev_usage,
		clnode_show_rev_optionslist,
		clnode_show_rev_options
	},
	{
		"set",
		CLSUB_DO_COMMAND_LOGGING,
		(char *)CLNODE_SUBCMD_SET,
		CL_AUTH_CLUSTER_MODIFY,
		clnode_set_usage,
		clnode_set_optionslist,
		clnode_set_options
	},
	{
		"remove",
		CLSUB_NCLMODE_REQUIRED,
		(char *)CLNODE_SUBCMD_REMOVE,
		CL_AUTH_CLUSTER_MODIFY,
		clnode_remove_usage,
		clnode_remove_optionslist,
		clnode_remove_options
	},
	{
		"remove-farm",
		CLSUB_DO_COMMAND_LOGGING | CLSUB_CHECK_EUROPA,
		(char *)CLNODE_SUBCMD_REMOVE_FARM,
		CL_AUTH_CLUSTER_MODIFY,
		clnode_remove_farm_usage,
		clnode_remove_farm_optionslist,
		clnode_remove_farm_options
	},
	{
		"show",
		CLSUB_DEFAULT |
		CLSUB_ZONE_CLUSTER_ALLOWED,
		(char *)CLNODE_SUBCMD_SHOW,
		CL_AUTH_CLUSTER_READ,
		clnode_show_usage,
		clnode_show_optionslist,
		clnode_show_options
	},
#ifdef WEAK_MEMBERSHIP
	{
		"status",
		CLSUB_NON_GLOBAL_ZONE_ALLOWED |
		CLSUB_ZONE_CLUSTER_ALLOWED
		| CLSUB_NCLMODE_ALLOWED,
		(char *)CLNODE_SUBCMD_STATUS,
		CL_AUTH_CLUSTER_READ,
		clnode_status_usage,
		clnode_status_optionslist,
		clnode_status_options
	},
	{
		"resolve-config",
		CLSUB_NCLMODE_ALLOWED,
		(char *)CLNODE_SUBCMD_RESOLVE_CONFIG,
		CL_AUTH_CLUSTER_MODIFY,
		clnode_resolve_config_usage,
		clnode_resolve_config_optionlist,
		clnode_resolve_config_options
	},
#else
	{
		"status",
		CLSUB_NON_GLOBAL_ZONE_ALLOWED |
		CLSUB_ZONE_CLUSTER_ALLOWED,
		(char *)CLNODE_SUBCMD_STATUS,
		CL_AUTH_CLUSTER_READ,
		clnode_status_usage,
		clnode_status_optionslist,
		clnode_status_options
	},
#endif // WEAK_MEMBERSHIP
	{
		"unmonitor-farm",
		CLSUB_DO_COMMAND_LOGGING | CLSUB_CHECK_EUROPA,
		(char *)CLNODE_SUBCMD_UNMONITOR,
		CL_AUTH_CLUSTER_MODIFY,
		clnode_unmonitor_usage,
		clnode_unmonitor_optionslist,
		clnode_unmonitor_options
	},
	{0, 0, 0, 0, 0, 0, 0}
};

// Initialization properties for the "clnode" command
clcommandinit_t clnodeinit = {
	CLCOMMANDS_COMMON_VERSION,
	(char *)CLNODE_CMD_DESC,
	clnode_usage,
	clnode_subcommands
};
