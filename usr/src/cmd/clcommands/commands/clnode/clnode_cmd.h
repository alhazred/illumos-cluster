//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef _CLNODE_CMD_H
#define	_CLNODE_CMD_H

#pragma ident	"@(#)clnode_cmd.h	1.6	09/01/16 SMI"

//
// This is the header file for the clnode(1CL) command
//

#define	CLNODE_SUBCMD_ADD		\
	GETTEXT("Add a node into the cluster")
#define	CLNODE_SUBCMD_ADD_FARM		\
	GETTEXT("Add a farm node to the cluster configuration")
#define	CLNODE_SUBCMD_CHECK		\
	GETTEXT("Check and report whether the cluster is configured corretly")
#define	CLNODE_SUBCMD_CLEAR		\
	GETTEXT("Clear a node from the cluster")
#define	CLNODE_SUBCMD_EVACUATE		\
	GETTEXT("Evacuate resource groups and device groups from the node")
#define	CLNODE_SUBCMD_EXPORT		\
	GETTEXT("Export the node configuration")
#define	CLNODE_SUBCMD_LIST		\
	GETTEXT("List the cluster nodes")
#define	CLNODE_SUBCMD_MONITOR		\
	GETTEXT("Enable monitoring of farm nodes")
#define	CLNODE_SUBCMD_SHOW_REV		\
	GETTEXT("Show Sun Cluster software release and packages")
#define	CLNODE_SUBCMD_SET		\
	GETTEXT("Set node properties")
#define	CLNODE_SUBCMD_REMOVE		\
	GETTEXT("Remove a node from the cluster")
#define	CLNODE_SUBCMD_REMOVE_FARM	\
	GETTEXT("Remove a farm node from the cluster configuration")
#define	CLNODE_SUBCMD_SHOW		\
	GETTEXT("Display cluster nodes and their properties")
#define	CLNODE_SUBCMD_STATUS		\
	GETTEXT("Display the status of cluster nodes")
#define	CLNODE_SUBCMD_RESOLVE_CONFIG		\
	GETTEXT("Resolve cluster configuration conflicts")
#define	CLNODE_SUBCMD_UNMONITOR		\
	GETTEXT("Disable monitoring of farm nodes")
#define	CLNODE_CMD_DESC			\
	GETTEXT("Manage cluster nodes")

#include "clnode.h"
#include "clcommands.h"

// Subcommand options
extern clerrno_t clnode_add_options(ClCommand &cmd);
extern clerrno_t clnode_add_farm_options(ClCommand &cmd);
extern clerrno_t clnode_check_options(ClCommand &cmd);
extern clerrno_t clnode_clear_options(ClCommand &cmd);
extern clerrno_t clnode_evacuate_options(ClCommand &cmd);
extern clerrno_t clnode_export_options(ClCommand &cmd);
extern clerrno_t clnode_list_options(ClCommand &cmd);
extern clerrno_t clnode_monitor_options(ClCommand &cmd);
extern clerrno_t clnode_show_rev_options(ClCommand &cmd);
extern clerrno_t clnode_set_options(ClCommand &cmd);
extern clerrno_t clnode_remove_options(ClCommand &cmd);
extern clerrno_t clnode_remove_farm_options(ClCommand &cmd);
extern clerrno_t clnode_show_options(ClCommand &cmd);
extern clerrno_t clnode_status_options(ClCommand &cmd);
#ifdef WEAK_MEMBERSHIP
extern clerrno_t clnode_resolve_config_options(ClCommand &cmd);
#endif // WEAK_MEMBERSHIP
extern clerrno_t clnode_unmonitor_options(ClCommand &cmd);

#endif /* _CLNODE_CMD_H */
