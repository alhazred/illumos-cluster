//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clnode_remove_options.cc	1.2	08/05/20 SMI"

//
// Process clnode "remove" subcommand options
//

#include "clnode_cmd.h"

//
// clnode "remove" options
//
clerrno_t
clnode_remove_options(ClCommand &cmd)
{
	int i, c;
	int wildcard;
	int errcount = 0;
	char *cl_option = 0;
	int option_index = 0;
	char *shortopts;
	struct option *longopts;
	ValueList operands;
	optflgs_t optflgs = 0;
	char *gmntpoint = (char *)0;
	char *sponsornode = (char *)0;
	int nflg, gflg;
	char local_node[BUFSIZ];

	// Get the short and long option lists
	shortopts = clsubcommand_get_shortoptions(cmd);
	longopts = clsubcommand_get_longoptions(cmd);

	// This subcommand should have options
	if (!shortopts || !longopts) {
		clerror("Aborting - subcommand initialization failed.\n");
		abort();
	}

	// Initialize
	nflg = gflg = 0;

	optind = 2;
	while ((c = getopt_clip(cmd.argc, cmd.argv, shortopts, longopts,
	    &option_index)) != -1) {

		// Get the option
		delete cl_option;
		cl_option = clcommand_option(optopt, cmd.argv[optind - 1]);

		// Switch on option letter
		switch (c) {
		case 'v':
			// Verbose option
			cmd.isVerbose = 1;
			optflgs |= vflg;
			break;

		case '?':
			// Help option?
			if ((strcmp(cl_option, "-?") == 0) ||
			    (strcmp(cl_option, "--help") == 0)) {
				optflgs |= hflg;
				break;
			}

			// Or, unrecognized option?
			clerror("Unrecognized option - \"%s\".\n", cl_option);
			++errcount;
			break;

		case 'F':
			// Force option
			optflgs |= Fflg;
			break;

		case 'G':
			// Global device mountpoint
			gmntpoint = optarg;
			gflg++;
			break;

		case 'n':
			// Sponsor node
			sponsornode = optarg;
			nflg++;
			break;

		case ':':
			// Missing argument
			clerror("Option \"%s\" requires an argument.\n",
			    cl_option);
			++errcount;
			break;

		default:
			//
			// We should never get here.
			// If we do, it means that there
			// is an option in our option list
			// for which there is no case statement.
			// We either have a bad option list or
			// a bad case statement.
			//
			clerror("Unexpected option - \"%s\".\n", cl_option);
			++errcount;
			break;
		}
	}
	delete cl_option;
	cl_option = (char *)0;

	// Check if option is specified multiple times
	if (nflg > 1) {
		clerror("You can only specify the \"-n\" option once.\n");
		++errcount;
	}
	if (gflg > 1) {
		clerror("You can only specify the \"-G\" option once.\n");
		++errcount;
	}

	// Errors?
	if (errcount) {
		clcommand_usage(cmd, cmd.getSubCommandName(), errcount);
		return (CL_EINVAL);
	}

	// Just print help?
	if (optflgs & hflg) {
		clcommand_help(cmd, cmd.getSubCommandName());
		return (CL_NOERR);
	}

	// Check the -g
	if (gmntpoint) {
		// Must start with "/"
		if (*gmntpoint != '/') {
			clerror("The cluster file system \"%s\" is "
			    "not an absolute path.\n", gmntpoint);
			++errcount;
		}
	}

	// Update the operands list
	wildcard = 0;
	for (i = optind;  i < cmd.argc;  ++i) {
		if (strcmp(cmd.argv[i], CLCOMMANDS_WILD_OPERAND) == 0) {
			++wildcard;
		} else {
			operands.add(cmd.argv[i]);
		}
	}

	// Cannot use wildcard
	if (wildcard) {
		clerror("You cannot use operand \"%s\" with this subcommand.\n",
		    CLCOMMANDS_WILD_OPERAND);
		++errcount;
	} else {
		// Get the local host name
		local_node[0] = '\0';
		(void) gethostname(local_node, sizeof (local_node));

		// If there is an operand, it must be the local node
		switch (operands.getSize()) {
		case 0:
			operands.add(local_node);
			break;
		case 1:
			operands.start();
			if (strcmp(operands.getValue(), local_node) != 0) {
				++errcount;
				clerror("You can only remove the local "
				    "node.\n");
			}
			break;
		default:
			++errcount;
			clerror("You cannot remove more than one "
			    "node from the cluster.\n");
		}

		// No wildcard in non-cluster mode
		if (wildcard) {
			++errcount;
			clerror("You cannot use operand \"%s\" with this "
			    "subcommand.\n");
		}
	}

	if (errcount) {
		clcommand_usage(cmd, cmd.getSubCommandName(), errcount);
		return (CL_EINVAL);
	}

	// Perform clnode "remove" operation
	return (clnode_remove(cmd, operands, optflgs, sponsornode, gmntpoint));
}
