#! /usr/xpg4/bin/sh -p
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#ident	"@(#)clsetup.ksh	1.49	09/03/16 SMI"
#
# Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

#####################################################
#
# Global constants which may be set from the environment
#
# SC_DEBUG can be set to non-NULL to turn on additional debug information.
#  And, if it is set to 2, assume installmode is disabled - init cluster.
#  If set to 3, assume europa bits are available and allows the developer
#  to test farm management operations.
#
# SC_MAXNODEID is the maximum node id;  it does not necessarily indicate
#  the maximum number of nodes supported in the cluster for this release.
#
# SC_SCADMINDIR can be set to to point to an alternate lib dir
#
# CLSETUP_LIBRARY can be set to non-NULL to use clsetup functions without
#  actually running clsetup.
#
# CLSETUP_DEBUG_RGM_CACHEFILE can be set to a file containing clresource*
#  output constructed especially for debug purposes.
#
#####################################################
typeset -r SC_DEBUG=${SC_DEBUG:-}
typeset -r SC_MAXNODEID=${SC_MAXNODEID:-12}
typeset -r SC_SCADMINDIR=${SC_SCADMINDIR:-/usr/cluster/lib/scadmin}
typeset -r CLSETUP_LIBRARY=${CLSETUP_LIBRARY:-}
typeset CLSETUP_DEBUG_RGM_CACHEFILE=${CLSETUP_DEBUG_RGM_CACHEFILE:-}
typeset -r SC_CMD_VERSION="1.0"

# debugging
typeset PRINTDEBUG
if [[ -n "${SC_DEBUG}" ]]; then
	PRINTDEBUG="echo \n"
	PRINTFDEBUG="printf"
else
	PRINTDEBUG=:
	PRINTFDEBUG=:
fi

#####################################################
#
# I18N
#
#####################################################
typeset -x TEXTDOMAIN=SUNW_SC_CMD
typeset -x TEXTDOMAINDIR=/usr/cluster/lib/locale

# zone privilege check

if [[ -z "${SC_DEBUG}" ]]; then
	/usr/cluster/lib/sc/sc_zonescheck
	if [ $? -ne 0 ]; then
		exit 1
	fi
fi

#####################################################
#
# Constant globals
#
#####################################################

# Set the PATH
typeset SC_BINDIRS=/usr/cluster/bin
PATH=${SC_BINDIRS}:${PWD}:/bin:/usr/bin:/sbin:/usr/sbin:/usr/cluster/lib/sc:${PATH}; export PATH

# Program name
typeset -r PROG=${0##*/}

# Global internationalized constant strings
typeset -r YES=$(gettext 'yes')		# I18N "yes"
typeset -r NO=$(gettext 'no')		# I18N "no"

# Libs
typeset -r SC_SCADMINLIBDIR=${SC_SCADMINDIR}/lib
typeset -r SC_LIB_SC=sc_common

# Device groups
typeset -r CLSETUP_DG_VXVM_SET=vxvm
typeset -r CLSETUP_DG_VXVM_LOOKUP=VxVM

# Cluster transport
typeset -r CLSETUP_WILDCAT_ADAPTER="wrsm"
typeset -r CLSETUP_WILDCAT_JUNCTION="sw_wrsm"

# RGM
typeset -r CLSETUP_RT_NOVERSION="<unset_version>"

# Other constants
typeset -r SC_SCRGADM=scrgadm
typeset -r SC_SCSTAT=scstat
typeset -r SC_SUNOS=$(uname -r)
typeset -r SC_SUNOS_MAJ=$(echo ${SC_SUNOS} | awk -F'.' '{print $1}')
typeset -r SC_SUNOS_MIN=$(echo ${SC_SUNOS} | awk -F'.' '{print $2}')

# Sun Cluster New Commands
typeset -r CL_CMD_CLQUORUM=clquorum
typeset -r CL_CMD_CLACCESS=claccess
typeset -r CL_CMD_CLRESOURCEGROUP=clresourcegroup
typeset -r CL_CMD_CLRESOURCE=clresource
typeset -r CL_CMD_CLRESOURCETYPE=clresourcetype
typeset -r CL_CMD_CLRES_LOGICAL_HOSTNAME=clreslogicalhostname
typeset -r CL_CMD_CLRES_SHARED_ADDRESS=clressharedaddress
typeset -r CL_CMD_CLINTERCONNECT=clinterconnect
typeset -r CL_CMD_CLDEVICEGROUP=cldevicegroup
typeset -r CL_CMD_CLNODE=clnode
typeset -r CL_CMD_CLUSTER=cluster

typeset -r SCHA_CLUSTER_GET=scha_cluster_get
typeset -r SCHA_RT_GET=scha_resourcetype_get
typeset -r SCHA_RG_GET=scha_resourcegroup_get
typeset -r SCHA_RS_GET=scha_resource_get

integer -r SCHA_ERR_RSRC=14

typeset -r CLSETUP_UID=`/usr/xpg4/bin/id -u -r`
typeset -r CLSETUP_EUID=`/usr/xpg4/bin/id -u`
typeset -r CLSETUP_PID=$$
typeset -r CLSETUP_PPID=`ps -o ppid= -p $$`
typeset -r CLSETUP_LOGNAME=`logname`
typeset -r CLSETUP_TTY=`tty`
typeset -r CLSETUP_OPT="$*"
typeset -r RAC_SETUP=/usr/cluster/lib/ucmm/rac_setup

# telemetry related
typeset -r SCTELEMETRY=/usr/cluster/lib/rgm/rt/sctelemetry/sctelemetry
typeset -r SC_SLM_IPSPKG=SUNWscmaslm
typeset -r SC_DSCONFIG_IPSPKG=SUNWscdsconfig

# Macros for display Telemetry or Wizard menus
integer LIST_TELEMETRY=1
integer LIST_DSCONFIG=1

# VxVM support, either packaging system
VXVM_SO=/usr/cluster/lib/dcs/scconf_vxvm.so.1

# Are IPS packages in play?
integer IS_IPS=0  # 0 means no, 1 means yes

# Temp directory
if [[ -n "${SC_DEBUG}" ]]; then
	typeset -r CLSETUP_TMPDIR=/tmp
else
	typeset -r CLSETUP_TMPDIR=/var/cluster/run/clsetup
fi

# Temp files
typeset -r CLSETUP_TMP_RGLIST=${CLSETUP_TMPDIR}/${PROG}.rglist.$$
typeset -r CLSETUP_TMP_RSLIST=${CLSETUP_TMPDIR}/${PROG}.rslist.$$
typeset -r CLSETUP_TMP_RSLIST_SORTED=${CLSETUP_TMPDIR}/${PROG}.rslist_sorted.$$
typeset -r CLSETUP_TMP_PROPLIST=${CLSETUP_TMPDIR_TMPDIR}/${PROG}.proplist.$$
typeset -r CLSETUP_TMP_RGMSTATUS=${CLSETUP_TMPDIR}/${PROG}.rgmstatus.$$
typeset -r CLSETUP_TMP_RGSTATE=${CLSETUP_TMPDIR}/${PROG}.rgstate.$$
typeset -r CLSETUP_TMP_CMDOUT=${CLSETUP_TMPDIR}/${PROG}.cmdout.$$
typeset -r CLSETUP_TMP_SUMMARY=${CLSETUP_TMPDIR}/${PROG}.summary.$$
typeset -r CLSETUP_TMP_FARMNODE_LIST=${CLSETUP_TMPDIR}/${PROG}.farmnode_list.$$
typeset -r CLSETUP_TMP_DSWIZLIST=${CLSETUP_TMPDIR}/${PROG}.dswiz_list.$$

# RGM cache file.
#
# scrgadm -pvv output is stored in this cache file.
# In future, this need to be updated to use ouputs from
# new-cli equivalents like clrs,clrg and clrt
#
if [[ -n "${CLSETUP_DEBUG_RGM_CACHEFILE}" ]] &&
    [[ -r "${CLSETUP_DEBUG_RGM_CACHEFILE}" ]]; then
	typeset -r CLSETUP_RGM_CACHEFILE=${CLSETUP_DEBUG_RGM_CACHEFILE}
else

	typeset -r CLSETUP_RGM_CACHEFILE=${CLSETUP_TMPDIR}/${PROG}.rgm.cache.$$
    	CLSETUP_DEBUG_RGM_CACHEFILE=
fi

# clsetup process ID list
typeset -r CLSETUP_PIDLIST=${CLSETUP_TMPDIR}/${PROG}_pidlist

# Default number of desired secondaries in a device group. Note that if
# we ever want to change this value, we have to review the messages printed
# also.
integer -r SC_DEFAULT_NUMSECONDARIES=1

integer -r SC_VLAN_MULTIPLIER=1000

# Macros for including or excluding system RTs
integer -r CL_NOSYSRT=1
integer -r CL_SYSRT=2

#####################################################
#
# Variable globals
#
#####################################################
integer SC_LOADED_COMMON=0		# set to 1 when common libs loaded
integer CLSETUP_ISMEMBER=0		# set to 1, if we are in the cluster
typeset CLSETUP_CMD_LOG			# set to log file, if there is one
typeset CLSETUP_DO_CMDSET		# input to clsetup_do_cmdset()
typeset ZONES_OK=0			# set to 1, if OS is S10
integer	CLSETUP_IS_EUROPA=0		# set to 1, if europa is enabled.

typeset MYNAME=$(uname -n)

# user can override via /etc/default/sccheck config file
typeset JAVA_HOME=${JAVA_HOME:-/usr/java}

# user can override via /etc/default/sccheck config file
typeset JAVA=${JAVA:-${JAVA_HOME}/bin/java}

typeset CLASSPATH=${CLASSPATH}:/usr/cluster/lib/ds/lib/dsconfig.jar:/usr/cluster/lib/model/lib/model.jar
typeset CMASS_JAR=/usr/cluster/lib/cmass
for i in ${CMASS_JAR}/cmas_*.jar; do
	 CLASSPATH=${CLASSPATH}:${i}
done
typeset CACAO_JAR=/usr/lib/cacao/lib

for i in ${CACAO_JAR}/*.jar; do
	CLASSPATH=${CLASSPATH}:${i}
done

typeset JDMK_OLDJAR=/opt/SUNWjdmk/*/lib
typeset JDMK_NEWJAR=/usr/share/lib/jdmk
for i in ${JDMK_OLDJAR}/*.jar ${JDMK_NEWJAR}/*.jar; do
	CLASSPATH=${CLASSPATH}:${i}
done

CLASSPATH=${CLASSPATH}:${JAVA_HOME}/jre/lib/rt.jar

# RTR directories path.
typeset -r CLSETUP_RTR_DIR=/usr/cluster/lib/rgm/rtreg
typeset -r CLSETUP_OPT_RTR_DIR=/opt/cluster/lib/rgm/rtreg

# IPS package data service wizard reg files
typeset -r CLSETUP_DSWIZ_REG_DIR=/opt/cluster/lib/ds/registry
typeset -r CLSETUP_DSWIZ_REG_FILE=${CLSETUP_DSWIZ_REG_DIR}/00-reg-files.reg


####################################################
#
# Create descriptor 4 as a dup of the original stdout.
# Functions which want to print user visible output
# to the original stdout have the option of using
# descriptor 4.
#
####################################################
exec 4>&1

#####################################################
#####################################################
##
## General housekeeping and other useful functions
##
#####################################################
#####################################################

#####################################################
#
# print_usage()
#
#	Print usage message to stderr
#
#####################################################
print_usage()
{
	printf "$(gettext 'Usage:    %s [<options>]')\n" "${PROG}" >&2
	printf "          %s -? | --help\n" "${PROG}" >&2
	printf "          %s -f <logfile> | --file <logfile>\n" "${PROG}" >&2
	printf "          %s -V | --version\n" "${PROG}" >&2
	printf "\n$(gettext 'Manage cluster configuration')\n" >&2
	printf "\n$(gettext 'OPTIONS:')\n" >&2
	printf "\n$(gettext '  -?    Display this help text.')\n" >&2
	printf "\n$(gettext '  -f <file>')" >&2
	printf "\n$(gettext '        Specify the log file.')\n" >&2
	printf "\n$(gettext '  -V    Display command version.')\n\n" >&2

	return 0
}

#####################################################
#
# cleanup [exitstatus]
#
#	Remove any remaining temp files.
#
#	If an "exitstatus" is given, exit with that status.
#
#####################################################
cleanup()
{
	typeset exitstatus=$1

	typeset file
	typeset pid
	typeset foo

	rm -f ${CLSETUP_TMP_RGLIST} ${CLSETUP_TMP_RSLIST} ${CLSETUP_TMP_RSLIST_SORTED} ${CLSETUP_TMP_PROPLIST} ${CLSETUP_TMP_RGMSTATUS} ${CLSETUP_TMP_CMDOUT} ${CLSETUP_TMP_SUMMARY} ${CLSETUP_TMP_FARMNODE_LIST}

	clsetup_rm_rgmfile ${CLSETUP_RGM_CACHEFILE}
	clsetup_rm_rgstatefile ${CLSETUP_TMP_RGSTATE}

	# Let's get any leftover stuff, too
	(
		cd ${CLSETUP_TMPDIR}
		for file in *
		do
			# only interested in clsetup tmp files
			if [[ ${file} != clsetup.* ]]; then
				continue
			fi

			# Get the PID suffix from the file
			pid=$(IFS=. ; set -- ${file};  shift $(($# - 1));  echo $1)
			if [[ $(expr "${pid}" : '[0-9]*') -ne ${#pid} ]]; then
				continue
			fi

			# If the process is still running, don't touch it
			if [[ -n "$(ptree ${pid})" ]]; then
				continue
			fi

			# Zap the file
			rm -f ${file}
		done
	)

	if [[ -n "${exitstatus}" ]]; then
		exit ${exitstatus}
	fi

	return 0
}

#####################################################
#
# loadlib() libname flag
#
#	If the "flag" is not set to 1, load the
#	named include file.
#
#	Return:
#		zero		Success
#		non-zero	Failure
#
#####################################################
loadlib()
{
	typeset libname=$1
	typeset flag=$2

	# Check flag
	if [[ "${flag}" = 1 ]]; then
		return 0
	fi

	# Load the library
	. ${libname}
	if [[ $? -ne 0 ]]; then
		printf "$(gettext '%s:  Unable to load \"%s\"')\n" "${PROG}" "${libname}" >&2
		return 1
	fi
}

#####################################################
#
# clsetup_continue()
#
#	Is it okay to continue [yes]?
#
#	Return:
#		zero		continue
#		non-zero	do not continue
#
#####################################################
clsetup_continue()
{
	typeset answer

	answer=$(sc_prompt_yesno "$(gettext 'Is it okay to continue?')" "${YES}") || return 1

	[[ "${answer}" != "yes" ]] && return 1

	return 0
}

#####################################################
#
# clsetup_do_cmdset() [noconfirm] [nopause] [commands]
#
#	noconfirm	- if given, do not re-confirm the update
#			- if 1, just run command
#			- if 2, don't run command, but may ask to log
#
#	nopause		- if given, do not pause after running command
#
#	commands	- if given, and "nopause" is not, messages
#				should act as though multiple commands
#				were issued.
#
#	noncluster	- if given, it is okay to run in noncluster mode
#
#	Process the list of commands found in the
#	CLSETUP_DO_CMDSET array.
#
#	Return:
#		0		Completed
#		1		Failure occurred or user typed Ctrl-D
#		2		User decided not to update
#
#####################################################
clsetup_do_cmdset()
{
	typeset noconfirm=${1}
	typeset nopause=${2}
	typeset commands=${3}
	typeset noncluster=${4}

	sctxt_p1_cmd="$(gettext '
		DEBUG: Please consider the following command:
	')"
	sctxt_p1_cmds="$(gettext '
		DEBUG: Please consider the following commands:
	')"

	typeset tmpout=${CLSETUP_TMP_CMDOUT}

	typeset answer

	integer index
	integer logthem=0
	integer retval

	# Make sure that CLSETUP_DO_CMDSET is set
	if [[ -z "${CLSETUP_DO_CMDSET[0]}" ]]; then
		return 1
	fi

	# If DEBUG, print command list
	if [[ -n "${SC_DEBUG}" ]]; then

		# Print Messages
		if [[ -z "${CLSETUP_DO_CMDSET[1]}" ]]; then
			sc_print_para "${sctxt_p1_cmd}"
		else
			sc_print_para "${sctxt_p1_cmds}"
		fi

		# Print the command(s)
		let index=0
		while [[ -n "${CLSETUP_DO_CMDSET[index]}" ]]
		do
			printf "           %s\n" "${CLSETUP_DO_CMDSET[index]}"
			((index += 1))
		done
		echo
	fi

	# See if command(s) should be run
	if [[ ${CLSETUP_ISMEMBER} -eq 1 ]] ||
	    [[ -n "${noncluster}" ]]; then

		# Okay to run?
		if [[ -n "${noconfirm}" ]]; then
			if [[ "${noconfirm}" == 2 ]]; then
				answer="no"
			else
				answer="yes"
			fi
		else
			answer=$(sc_prompt_yesno "$(gettext 'Is it okay to proceed with the update?')" "${YES}") || return 1
		fi
		if [[ "${answer}" = "yes" ]]; then

			# Run and log them
			let index=0
			while [[ -n "${CLSETUP_DO_CMDSET[index]}" ]]
			do
				# Run command
				echo "${CLSETUP_DO_CMDSET[index]}"
				eval ${CLSETUP_DO_CMDSET[index]} >${tmpout} 2>&1
				retval=$?

				# Messages?
				if [[ -s ${tmpout} ]]; then
					more ${tmpout}
				fi
				rm -f ${tmpout}

				# Check for command success
				if [[ ${retval} -ne 0 ]]; then
					echo
					printf "$(gettext 'Command failed.')\n\n\a"
					if [[ -z "${nopause}" ]]; then
						sc_prompt_pause
					fi
					return 1
				fi

				# If it succeeded, log it, if there is a log
				if [[ -n "${CLSETUP_CMD_LOG}" ]]; then
					echo "${CLSETUP_DO_CMDSET[index]}" >>${CLSETUP_CMD_LOG}
				fi

				# Next
				((index += 1))
			done

			# If we got this far, command(s) must have succeeded
			if [[ -z "${nopause}" ]]; then
				echo
				if [[ -z "${CLSETUP_DO_CMDSET[1]}" ]] &&
				    [[ -z "${commands}" ]]; then
					sc_print_para "$(gettext 'Command completed successfully.')"
				else
					sc_print_para "$(gettext 'Commands completed successfully.')"
				fi
			fi

		# Just log it/them?
		elif [[ -n "${CLSETUP_CMD_LOG}" ]]; then
			answer=$(sc_prompt_yesno "$(gettext 'Do you just want to add a commands record to the log?')" "${YES}") || return 1

			# Just log them
			if [[ "${answer}" = "yes" ]]; then
				let logthem=1
			else
				return 2
			fi
		else
			return 2
		fi
	elif [[ -n "${CLSETUP_CMD_LOG}" ]]; then
		# Debug mode, okay to log?
		if [[ -z "${CLSETUP_DO_CMDSET[1]}" ]]; then
			answer=$(sc_prompt_yesno "$(gettext 'DEBUG:  Do you want to log this command?')" "${YES}") || return 1
		else
			answer=$(sc_prompt_yesno "$(gettext 'DEBUG:  Do you want to log these commands?')" "${YES}") || return 1
		fi
		if [[ "${answer}" = "yes" ]]; then
			let logthem=1
		fi
	fi

	# Just log commands?
	if [[ ${logthem} -eq 1 ]] && [[ -n "${CLSETUP_CMD_LOG}" ]]; then
		let index=0
		while [[ -n "${CLSETUP_DO_CMDSET[index]}" ]]
		do
			# Log command
			echo "${CLSETUP_DO_CMDSET[index]}" >>${CLSETUP_CMD_LOG}

			# Next
			((index += 1))
		done
	fi

	# If "nopause" is set, do not pause for reconfirmation
	if [[ -z "${nopause}" ]]; then
		sc_prompt_pause || return 1
	fi

	return 0
}

#####################################################
#
# clsetup_prompt_nodename() "nodelist" "[prompt]"
#
#	Prompt for a node name, then verify that it is one
#	of the names in the "nodelist".  If "prompt" is not given,
#	a default prompt is used.
#
#	The prompt is printed on file descriptor 4, and the answer
#	is printed to stdout.  File descriptor 4 should be
#	dupped to the original stdout before this function is called.
#
#	Return:
#		zero		Completed
#		non-zero	Not completed
#
#####################################################
clsetup_prompt_nodename()
{
	typeset nodelist="${1}"
	typeset prompt="${2}"

	typeset dflt_prompt="$(gettext 'Node name?')"
	integer found
	typeset answer
	typeset node

	# If no prompt is given, use the default
	if [[ -z "${prompt}" ]]; then
		prompt="${dflt_prompt}"
	fi

	#
	# The caller of this function should have already opened
	# descriptor 4 as a dup of the original stdout.
	#
	# Dup this function's stdout to descriptor 5.
	# Then, re-direct stdout to descriptor 4.
	#
	# So, the default stdout from this function will go to
	# the original stdout (probably the tty).  Descriptor 5
	# has the answer printed to it.
	#
	exec 5>&1
	exec 1>&4

	# Loop until done or Ctrl-D is typed
	while true
	do
		# Get the node name
		answer=$(sc_prompt "${prompt}") || return 1

		# Make sure it is a known node
		let found=0
		for node in ${nodelist}
		do
			if [[ "${answer}" = "${node}" ]]; then
				let found=1
				break
			fi
		done

		# If there is a nodelist, but the node is not found, print err
		if [[ -n "${nodelist}" ]] && [[ ${found} -eq 0 ]]; then
			printf "$(gettext 'Unknown node name.')\n\n\a"
			continue
		fi

		# okay
		echo ${answer} >&5
		break
	done

	return 0
}

#####################################################
#
# clsetup_prompt_zonename() "zonelist" "[prompt]"
#
#	Prompt for a zone name, then verify that it is one
#	of the names in the "zonelist".  If "prompt" is not given,
#	a default prompt is used.
#
#	The prompt is printed on file descriptor 4, and the answer
#	is printed to stdout.  File descriptor 4 should be
#	dupped to the original stdout before this function is called.
#
#	Return:
#		zero		Completed
#		non-zero	Not completed
#
#####################################################
clsetup_prompt_zonename()
{
	typeset zonelist="${1}"
	typeset prompt="${2}"

	typeset dflt_prompt="$(gettext 'Zone name?')"
	integer found
	typeset answer
	typeset zone

	# If no prompt is given, use the default
	if [[ -z "${prompt}" ]]; then
		prompt="${dflt_prompt}"
	fi

	#
	# The caller of this function should have already opened
	# descriptor 4 as a dup of the original stdout.
	#
	# Dup this function's stdout to descriptor 5.
	# Then, re-direct stdout to descriptor 4.
	#
	# So, the default stdout from this function will go to
	# the original stdout (probably the tty).  Descriptor 5
	# has the answer printed to it.
	#
	exec 5>&1
	exec 1>&4

	# Loop until done or Ctrl-D is typed
	while true
	do
		# Get the zone name
		answer=$(sc_prompt "${prompt}") || return 1

		# Make sure it is a known zone
		let found=0
		for zone in ${zonelist}
		do
			if [[ "${answer}" = "${zone}" ]]; then
				let found=1
				break
			fi
		done

		# If there is a zonelist, but the zone is not found, print err
		if [[ -n "${zonelist}" ]] && [[ ${found} -eq 0 ]]; then
			printf "$(gettext 'Unknown zone name.')\n\n\a"
			continue
		fi

		# okay
		echo ${answer} >&5
		break
	done

	return 0
}

#####################################################
#####################################################
##
## Get config data functions
##
#####################################################
#####################################################

#####################################################
#
# clsetup_get_installmode()
#
#	This function prints 1 if installmode is set and
#	zero if it is not.
#
#	This function always returns zero.
#
#####################################################
clsetup_get_installmode()
{
	typeset mode

	(
		LC_ALL=C; export LC_ALL
		mode="$(${CL_CMD_CLUSTER} show 2>/dev/null | sed -n 's/^  installmode:[   ]*\([^ ]*\).*/\1/p')"
		if [[ -n "${mode}" ]] && [[ "${mode}" = "enabled" ]]; then
			echo 1
		else
			echo 0
		fi
	)

	return 0
}

#####################################################
#
# clsetup_get_nodelist()
#
#	Print the list of cluster nodes or farm nodes.
# 	If the cluster is not a europa enabled cluster,
#	get all the nodes configured.
#
#	If the cluster is europa enabled and is_farm_rg
#	is 0, get only the cluster nodes.
#
#	If the cluster is europa enabled and is_farm_rg
#	is 1, get the farm node list alone.
#
#	This function always returns zero.
#
#####################################################
clsetup_get_nodelist()
{
	typeset farm_nodenames=
	typeset clusternode=
	typeset clusternodes=
	typeset clusternodenames=

	if [[ ${CLSETUP_IS_EUROPA} -eq 0 ]]; then
		${SCHA_CLUSTER_GET} -O all_nodenames
	else
		if [[ ${is_farm_rg} -eq 0 ]]; then
			# Retrive only the cluster node names.
			clusternodes="$(${SCHA_CLUSTER_GET} -O all_nodenames)"
			for clusternode in ${clusternodes}
			do
				clsetup_is_farm_node_present "${clusternode}"
				if [[ $? -eq 1 ]]; then
					continue
				fi

				if [[ -z ${clusternodenames} ]]; then
					clusternodenames="${clusternode}"
				else
					clusternodenames="${clusternodenames} ${clusternode}"
				fi
			done
			# Display the cluster node names alone.
			echo "${clusternodenames}"
		else
			# Retrive only the farm node names alone.
			farm_nodenames="$(clsetup_get_farmnodelist)"
			echo "${farm_nodenames}"
		fi
	fi
	return 0
}

#####################################################
#
# clsetup_get_zonelist()
#
#	Print the list of configured zones for the given node.
#
#	This function always returns zero.
#
#####################################################
clsetup_get_zonelist()
{
	${SCHA_CLUSTER_GET} -O all_nonglobal_zones

	return 0
}

#####################################################
#
# clsetup_get_adapterlist() nodename
#
#	Print the list of configured adapters for the given node.
#
#	This function always returns zero.
#
#####################################################
clsetup_get_adapterlist()
{
	typeset nodename=${1}
	typeset list
	typeset item

	if [[ -z "${nodename}" ]]; then
		return 0
	fi

	list="$(
		LC_ALL=C; export LC_ALL;
		${CL_CMD_CLINTERCONNECT} show -n ${nodename} 2>/dev/null | nawk '/^ *Transport Adapter:/ {print $3}'
	)"
	for item in ${list}
	do
		if [[ ${item} != "<NULL>" ]]; then
			echo ${item}
		fi
	done

	return 0
}

#####################################################
#
# clsetup_get_junctionlist()
#
#	Print the list of configured junctions(aka switch).
#
#	This function always returns zero.
#
#####################################################
clsetup_get_junctionlist()
{
	(
		LC_ALL=C; export LC_ALL;
		${CL_CMD_CLINTERCONNECT} show 2>/dev/null | nawk '/^ *Transport Switch:/ {print $3}'
	)

	return 0
}

#####################################################
#
# clsetup_get_trtypes()
#
#	Print the list of configured transport types.
#
#	This function always returns zero.
#
#####################################################
clsetup_get_trtypes()
{
	typeset trtypes
	typeset trtype

	# Get the types
	trtypes="$(
		LC_ALL=C; export LC_ALL;
		${CL_CMD_CLINTERCONNECT} show -v 2>/dev/null | nawk '
		/^ *Transport Type:/ {print $3}
		')"
	# List each one only once
	for trtype in ${trtypes}
	do
		echo ${trtype}
	done | sort | uniq

	return 0
}

#####################################################
#
# clsetup_get_endpoints()
#
#	Print the list of configured transport cable endpoints.
#	Each pair of endpoints in the list identifies a cable.
#	Note that the port number is printed for each endpoint.
#
#	This function always returns zero.
#
#####################################################
clsetup_get_endpoints()
{
	#
	# Read the cables and split the string to get individual endpoints
	#
	(
		LC_ALL=C; export LC_ALL;
		${CL_CMD_CLINTERCONNECT} show 2>/dev/null | nawk '/^ *Transport Cable:/ {print $3}' | \
		while read tp_cable
		do
			IFS=,; set -- ${tp_cable}; echo ${1} ${2}
		done
	)

	return 0
}

#####################################################
#
# clsetup_get_devicegroup_type() groupname
#
#	Print the list device group type for the given "groupname".
#
#	This function always returns zero.
#
#####################################################
clsetup_get_devicegroup_type()
{
	typeset groupname=${1}

	# If name is not given, simply return
	if [[ -z "${groupname}" ]]; then
		return 0
	fi

	(
		LC_ALL=C; export LC_ALL;
		${CL_CMD_CLDEVICEGROUP} show ${groupname} 2>/dev/null | nawk '{
			if ($1 == "Type:") {
			    print $2
			}
		}'
	)

	return 0
}

#####################################################
#
# clsetup_get_devicegroup_nodes() groupname
#
#	Print the list device group nodes for the given "groupname".
#
#	This function always returns zero.
#
#####################################################
clsetup_get_devicegroup_nodes()
{
	typeset groupname=${1}
	typeset groupnodes
	typeset node

	# If name is not given, simply return
	if [[ -z "${groupname}" ]]; then
		return 0
	fi

	# Get the list of nodes
	groupnodes="$(
		LC_ALL=C; export LC_ALL;
		${CL_CMD_CLDEVICEGROUP} show ${groupname} 2>/dev/null | grep Node | nawk -F: '{
			cldg_op = $2
			# Remove leading spaces
			gsub(" ", "", cldg_op)
			# Split the comma seperated node list
			gsub(",", " ", cldg_op)
			print cldg_op
		}'
	)"

	print ${groupnodes}

	return 0
}

#####################################################
#
# clsetup_get_qdevlist()
#
#	Print the list of configured quorum devices.
#
#	This function always returns zero.
#
#####################################################
clsetup_get_qdevlist()
{
	typeset list
	typeset item

	list="$(
		LC_ALL=C; export LC_ALL;
		${CL_CMD_CLQUORUM} list -t shared_disk,netapp_nas,quorum_server 2>/dev/null
	)"
	for item in ${list}
	do
		if [[ ${item} != "<NULL>" ]]; then
			echo ${item}
		fi
	done

	return 0
}

#####################################################
#
# clsetup_create_rgstatefile() file
#
#	Create the given rgstate "file".   Each line of the file
#	has the following format:
#
#		<rgname> <nodename> <state>
#
#	If there are any errors in creating the file, the file
#	will not be created at all.
#
#	This function always returns 0.
#
#####################################################
clsetup_create_rgstatefile()
{
	typeset file=${1}

	# Make sure we are a cluster member
	if [[ ${CLSETUP_ISMEMBER} -ne 1 ]];  then
		return 0
	fi

	# Remove the file, if it exists
	if [[ -f "${file}" ]]; then
		rm -f ${file}
	fi

	#
	# Use scstat to get the current state for all nodes.
	# Note that we add an underbar in the event that the state is
	# two words.
	#
	(
		LC_ALL=C; export LC_ALL;
		${SC_SCSTAT} -g | nawk '/^[ 	]*Group: / {
			if (NF > 5) {
				print $2, $3,  $4 "_" $5
			} else {
				print $2, $3,  $4
			}
		}' >${file} 2>/dev/null
		if [[ $? -ne 0 ]] || [[ ! -s ${file} ]]; then
			rm -f ${file}
		fi
	)

	return 0
}

#####################################################
#
# clsetup_rm_rgstatefile() file
#
#	Remove the the given rgstatefile "file".
#
#	This function always returns 0.
#
#####################################################
clsetup_rm_rgstatefile()
{
	typeset file=${1}

	# Make sure we are a cluster member
	if [[ ${CLSETUP_ISMEMBER} -ne 1 ]];  then
		return 0
	fi

	# Remove the file, if it exists
	if [[ -f "${file}" ]]; then
		rm -f ${file}
	fi

	return 0
}

#####################################################
#
# clsetup_create_rgmfile() file
#
#	Create the scrgadm -pvv command output cachefile
#
#	Some of the RGM get functions take an optional "cache"
#	filename and will use that file for collecting data,
#	rather than re-running clresource family of commands
#	each time.   It is the caller's responsibility to
#	manage the "files", and to remove unused and obsolete
#	files.
#
#	If there are any errors in creating the file, the file
#	will not be created at all.
#
#	This function always returns 0.
#
#####################################################
clsetup_create_rgmfile()
{
	typeset file=${1}

	# Make sure we are a cluster member
	if [[ ${CLSETUP_ISMEMBER} -ne 1 ]]; then
		return 0
	fi

	# If this is the debug file, do not alter it
	if [[ "${file}" == "${CLSETUP_DEBUG_RGM_CACHEFILE}" ]]; then
		return 0
	fi

	# Remove the file, if it exists
	if [[ -f "${file}" ]]; then
		rm -f ${file}
	fi

	# Create the "cache" file
	(
		LC_ALL=C; export LC_ALL;
		${SC_SCRGADM} -pvv >${file} 2>/dev/null
		if [[ $? -ne 0 ]] || [[ ! -s ${file} ]]; then
			rm -f ${file}
		fi

	)

	return 0
}

#####################################################
#
# clsetup_rm_rgmfile() file
#
#	Remove the the given "cache file", as long as
#	it is not the "debug" file.
#
#	This function always returns 0.
#
#####################################################
clsetup_rm_rgmfile()
{
	typeset file=${1}

	# Make sure we are a cluster member
	if [[ ${CLSETUP_ISMEMBER} -ne 1 ]]; then
		return 0
	fi

	# If this is the debug file, we are done
	if [[ "${file}" == "${CLSETUP_DEBUG_RGM_CACHEFILE}" ]]; then
		return 0
	fi

	# Remove the file, if it exists
	if [[ -f "${file}" ]]; then
		rm -f ${file}
	fi

	return 0
}

#####################################################
#
# clsetup_get_cluster_or_farm_rgs [rglist] [type]
#
# Input
#	rglist - Resource Group containing  the mix of
#		 cluster RG's and Farm RG's.
#
#	type	- Whether "cluster" or "farm" rg is
#		  to be retrieved.
#
# Output:
#	 List of cluster RG's or Farm RG's.
#
#	Always returns 0.
#
#####################################################
clsetup_get_cluster_or_farm_rgs()
{
	typeset rgnames=${1}
	typeset type=${2}

	typeset rgname
	typeset nodelist
	typeset nodename
	typeset rglist=

	# Make sure that the list is not empty.
	if [[ -z ${rgnames} ]]; then
		return 0
	fi

	for rgname in ${rgnames}
	do
		nodelist="$(${SCHA_RG_GET} -G ${rgname} -O nodelist)"
		if [[ -n ${nodelist} ]]; then
			nodename="$(echo ${nodelist} | cut -f1 -d ' ')"
			clsetup_is_farm_node_present "${nodename}"
			if [[ $? -eq 1 ]]; then
				if [[ ${type} == "farm" ]]; then
					if [[ -z ${rglist} ]]; then
						rglist=${rgname}
					else
						rglist="${rglist} ${rgname}"
					fi
				fi
			else
				if [[ ${type} == "cluster" ]]; then
					if [[ -z ${rglist} ]]; then
						rglist=${rgname}
					else
						rglist="${rglist} ${rgname}"
					fi
				fi
			fi
		fi
	done

	# Display the list of rg's.
	echo ${rglist}
	return 0
}

#####################################################
#
# clsetup_get_rglist() [rgname] [rgtype] [rstype] [cachefile]
#
#	if "rgname" is not null, only return the given "rgname",
#	if it exists and matches the optional "rgtype" and "rstype".
#
#	If "rgtype" is given, only return resource groups of the given
#	type.  And, if "rstype" is also given, only resource groups of the
#	given "rgtype" and "rstype" are returned.
#
#	If "cachefile" is given, it is searched, rather than current
#	scha output.
#
#	Print the list of configured resource groups.
#
#	This function always returns zero.
#
#####################################################
clsetup_get_rglist()
{
	typeset rgname=${1}
	typeset -l rgtype=${2}		# lower case
	typeset rstype=${3}
	typeset cachefile=${4}

	typeset list
	typeset rslist
	typeset item
	typeset -l itemtype		# lower case
	typeset itemresources
	typeset foo

	integer found
	typeset rglist

	# Get the Farm RG 's alone if farm rg mangement is to be done.
	if [[ -n "${cachefile}" ]] && [[ -r "${cachefile}" ]]; then
		list="$(sed -n 's/^ *Res Group name:[  ]*\(.*\)/\1/p' ${cachefile})"
	else
		list="$(${SCHA_CLUSTER_GET} -O all_resourcegroups)"
	fi

	# For Europa enabled cluster get the server RG's or Farm RG's depneding on the type
	# of RG needed.
	if [[ ${CLSETUP_IS_EUROPA} -eq 1 ]]; then
		if [[ ${is_farm_rg} -eq 0 ]]; then
			rglist="$(clsetup_get_cluster_or_farm_rgs "${list}" "cluster")"
		else
			rglist="$(clsetup_get_cluster_or_farm_rgs "${list}" "farm")"
		fi
		list=${rglist}
	fi

	for item in ${list}
	do
		if [[ ${item} = "<NULL>" ]]; then
			continue
		fi
		if [[ -n "${rgname}" ]] &&
		    [[ ${item} != ${rgname} ]]; then
			continue
		fi
		if [[ -n "${rgtype}" ]]; then
			if [[ -n "${cachefile}" ]] &&
			    [[ -r "${cachefile}" ]]; then
				itemtype="$(sed -n 's/^ *('${item}') Res Group mode:[ 	]*\(.*\)/\1/p' ${cachefile})"
			else
				itemtype="$(${SCHA_RG_GET} -G ${item} -O rg_mode)"
			fi
			if [[ "${itemtype}" != "${rgtype}" ]]; then
				continue
			fi
		fi
		if [[ -n "${rstype}" ]]; then
			if [[ -n "${cachefile}" ]] &&
			    [[ -r "${cachefile}" ]]; then
				itemresources="$(sed -n 's/^ *('${item}':.*) Res resource type:[ 	]*\(.*\)/\1/p' ${cachefile})"
			else
				rslist="$(${SCHA_RG_GET} -G ${item} -O resource_list)"
				itemresources=
				for foo in ${rslist}
				do
					itemresources="${itemresources} $(${SCHA_RS_GET} -G ${item} -R ${foo} -O type)"
				done
			fi
			let found=0
			for foo in ${itemresources}
			do
				if [[ ${foo} = ${rstype} ]] || [[ ${foo} = ${rstype}:* ]]; then
					let found=1
					break
				fi
			done
			if [[ ${found} -ne 0 ]]; then
				echo ${item}
			fi
		else
			echo ${item}
		fi
	done

	return 0
}

#####################################################
#
# clsetup_get_rlist_from_rtype() rstype [rg]
#
#	Print the list of configured resource of the
#	given resource type.
#
#	This function always returns zero.
#
#####################################################
clsetup_get_rlist_from_rtype()
{
	typeset rstype=${1}
	typeset rg=${2}

	typeset rglist
	typeset rslist
	typeset -l itemtype		# lower case
	typeset resource


	if [[ -z ${rg} ]]; then
	    rglist="$(${SCHA_CLUSTER_GET} -O all_resourcegroups)"
	else
	    rglist=${rg}
    	fi

	for rg in ${rglist}
	do
		rslist="$(${SCHA_RG_GET} -G ${rg} -O resource_list)"
		for resource in ${rslist}
		do
			resourcetype="$(${SCHA_RS_GET} -G ${rg} -R ${resource} -O type)"
			if [[ ${resourcetype} = ${rstype} ]] || [[ ${resourcetype} = ${rstype}:* ]]; then
			    echo ${resource}
			    break
			fi
		done
	done

	return 0
}

#####################################################
#
# clsetup_get_rgtype() rgname [cachefile]
#
#	Print the type of resource group, "failover" or "scalable".
#
#	If "cachefile" is given, it is searched, rather than current
#	scha output.
#
#	This function always returns zero.
#
#####################################################
clsetup_get_rgtype()
{
	typeset rgname=${1}
	typeset cachefile=${2}

	typeset -l rgtype		# lower case

	if [[ -n "${cachefile}" ]] && [[ -r "${cachefile}" ]]; then
		rgtype="$(sed -n 's/^ *('${rgname}') Res Group mode:[ 	]*\(.*\)/\1/p' ${cachefile})"
	else
		rgtype="$(${SCHA_RG_GET} -G ${rgname} -O rg_mode)"
	fi

	if [[ "${rgtype}" == "failover" ]] ||
	    [[ "${rgtype}" == "scalable" ]]; then
		echo ${rgtype}
	fi

	return 0
}

#####################################################
#
# clsetup_get_rgdescription() rgname [cachefile]
#
#	Print the rg description
#
#	If "cachefile" is given, it is searched, rather than current
#	scha command.
#
#	This function always returns zero.
#
#####################################################
clsetup_get_rgdescription()
{
	typeset rgname=${1}
	typeset cachefile=${2}

	typeset rgdescription

	if [[ -n "${cachefile}" ]] && [[ -r "${cachefile}" ]]; then
		rgdescription="$(sed -n 's/^ *('${rgname}') Res Group RG_description:[ 	]*\(.*\)/\1/p' ${cachefile})"
	else
		rgdescription="$(${SCHA_RG_GET} -G ${rgname} -O rg_description)"
	fi

	if [[ "${rgdescription}" != "<NULL>" ]]; then
		echo "${rgdescription}"
	fi

	return 0
}

#####################################################
#
# clsetup_get_rgpathprefix() rgname [cachefile]
#
#	Print the rg pathprefix
#
#	If "cachefile" is given, it is searched, rather than current
#	scha output.
#
#	This function always returns zero.
#
#####################################################
clsetup_get_rgpathprefix()
{
	typeset rgname=${1}
	typeset cachefile=${2}

	typeset rgpathprefix

	if [[ -n "${cachefile}" ]] && [[ -r "${cachefile}" ]]; then
		rgpathprefix="$(sed -n 's/^ *('${rgname}') Res Group Pathprefix:[ 	]*\(.*\)/\1/p' ${cachefile})"
	else
		rgpathprefix="$(${SCHA_RG_GET} -G ${rgname} -O pathprefix)"
	fi

	if [[ "${rgpathprefix}" != "<NULL>" ]]; then
		echo "${rgpathprefix}"
	fi

	return 0

}

#####################################################
#
# clsetup_get_rgstate() rgname [rgstatefile] [flag] [nodename]
#
#	Print the rg state, a list of rg states, or a list of nodes.
#
#	The "flag" argument may have the following settings:
#
#	online		- list nodenames of all nodes for "rgname" which
#			  are online or pending_online
#	offline		- list nodenames of all nodes for "rgname" which
#			  are offline or pending_offline
#	unmanaged	- list nodenames of all nodes for "rgname" which
#			  are unmanaged
#	not_offline	- list nodenames of all nodes for "rgname" which
#			  are managed and not offline
#	stop_failed	- list nodenames of all nodes for "rgname" which
#			  are stop_failed
#	allnodes	- list rgstates of all nodes
#
#	If "nodename" is given, the state for the "rgname" on the given node
#	is printed.
#
#	It is not legal to supply both "flag" and "nodename".
#
#	If both "nodename" and "flag" are NULL, the list of all rg
#	states for all nodes is printed using the format
#	"<nodename>:<state> ...".
#
#	This function always returns zero.
#
#####################################################
clsetup_get_rgstate()
{
	typeset rgname=${1}
	typeset rgstatefile=${2}
	typeset flag=${3}
	typeset nodename=${4}

	typeset rgstatelist
	typeset -l lrgstate		# lower case

	integer created_file=0
	integer i

	# If not a cluster member, return "Unknown"
	if [[ ${CLSETUP_ISMEMBER} -ne 1 ]];  then
		if [[ -n "${nodename}" ]]; then
			echo "Unknown"
		fi
		return 0
	fi

	# Create the state file, if necessary
	if [[ -z "${rgstatefile}" ]] || [[ ! -f "${rgstatefile}" ]]; then
		rgstatefile=${CLSETUP_TMP_RGSTATE}
		clsetup_create_rgstatefile ${rgstatefile}
		if [[ ! -f "${rgstatefile}" ]]; then
			clsetup_create_rgstatefile ${rgstatefile}
			if [[ -f "${rgstatefile}" ]];  then
				let created_file=1
			else
				return 0
			fi
		fi
	fi

	# Get state data for our "rgname"
	set -A rgstatelist $(nawk '/^'${rgname}' / { print $2, $3 }' <${rgstatefile})

	# If we created the rgstatefile, get rid of it now
	if [[ ${created_file} -gt 0 ]]; then
		clsetup_rm_rgstatefile ${rgstatefile}
	fi

	# If nodename is given, just print the state for the given node.
	if [[ -n "${nodename}" ]]; then
		let i=0
		while [[ -n "${rgstatelist[i]}" ]]
		do
			if [[ ${rgstatelist[i]} == ${nodename} ]]; then
				echo ${rgstatelist[i + 1]}
				break
			fi
			((i += 2))
		done

	# If a flag is given, take the indicated action
	elif [[ -n "${flag}" ]]; then
		let i=0
		while [[ -n "${rgstatelist[i]}" ]]
		do
			lrgstate=${rgstatelist[i + 1]}
			case ${flag} in
			online)
				if [[ "${lrgstate}" == "online" ]] ||
				    [[ "${lrgstate}" == "pending_online" ]]; then
					echo ${rgstatelist[i]}
				fi
				;;

			offline)
				if [[ "${lrgstate}" == "offline" ]] ||
				    [[ "${lrgstate}" == "pending_offline" ]]; then
					echo ${rgstatelist[i]}
				fi
				;;

			unmanaged)
				if [[ "${lrgstate}" == "unmanaged" ]]; then
					echo ${rgstatelist[i]}
				fi
				;;

			not_offline)
				if [[ "${lrgstate}" != "offline" ]] &&
				    [[ "${lrgstate}" != "unmanaged" ]]; then
					echo ${rgstatelist[i]}
				fi
				;;

			stop_failed)
				if [[ "${lrgstate}" == "error--stop_failed" ]]; then
					echo ${rgstatelist[i]}
				fi
				;;

			allnodes)
				echo ${rgstatelist[i + 1]}
				;;

			esac
			((i += 2))
		done

	# Neither a nodename or a flag was given, print everything
	else
		let i=0
		while [[ -n "${rgstatelist[i]}" ]]
		do
			echo "${rgstatelist[i]}:${rgstatelist[i + 1]}"
			((i += 2))
		done
	fi

	return 0
}

#####################################################
#
# clsetup_rgstate_string() rgstate
#
#	Print a one-word message string for the given "rgstate".
#
#	This function is the inverse of clsetup_string_rgstate()
#
#	This function always returns zero.
#
#####################################################
clsetup_rgstate_string()
{
	typeset lrgstate=${1}

	case ${lrgstate} in
	unmanaged)		echo "Unmanaged" ;;
	online)			echo "Online" ;;
	pending_online)		echo "Pending_Online" ;;
	offline)		echo "Offline" ;;
	pending_offline)	echo "Pending_Offline" ;;
	error--stop_failed)	echo "Stop_Failed" ;;
	*)			echo "Unknown" ;;
	esac

	return 0
}

#####################################################
#
# clsetup_string_rgstate() string
#
#	Print the rgstate from the one-word messages state string.
#
#	This function is the inverse of clsetup_rgstate_string()
#
#	This function always returns zero.
#
#####################################################
clsetup_string_rgstate()
{
	typeset string=${1}

	case ${string} in
	Unmanaged)		echo "unmanaged" ;;
	Online)			echo "online" ;;
	Pending_Online)		echo "pending_online" ;;
	Offline)		echo "offline" ;;
	Pending_Offline)	echo "pending_offline" ;;
	Stop_Failed)		echo "error--stop_failed" ;;
	*)			echo "unknown" ;;
	esac

	return 0
}

#####################################################
#
# clsetup_get_rslist() [rsname] [rstype] [cachefile]
#
#	if "rsname" is not null, only return the given "rsname",
#	if it exists and matches the optional "rstype".
#
#	If "rstype" is also given, only resources of the
#	given "rstype" are returned.
#
#	If "cachefile" is given, it is searched, rather than current
#	scha output.
#
#	Print the list of configured resource.
#
#	This function always returns zero.
#
#####################################################
clsetup_get_rslist()
{
	typeset rsname=${1}
	typeset rstype=${2}		# lower case
	typeset cachefile=${3}

	typeset list
	typeset rglist
	typeset item
	typeset itemtype
	typeset foo

	integer found

	# Get the list
	if [[ -n "${cachefile}" ]] && [[ -r "${cachefile}" ]]; then
		if [[ -n "${rsname}" ]]; then
			list="$(sed -n 's/^ *('$(rsname)') Res name:[  ]*\(.*\)/\1/p' ${cachefile})"
		else
			list="$(sed -n 's/^ *(.*) Res name:[  ]*\(.*\)/\1/p' ${cachefile})"
		fi
	else
		rglist="$(${SCHA_CLUSTER_GET} -O all_resourcegroups)"
		list=
		for item in ${rglist}
		do
			list="${list} $(${SCHA_RG_GET} -G ${item} -O resource_list)"
		done
		if [[ -n "${rsname}" ]]; then
			let found=0
			for item in ${list}
			do
				if [[ "${item}" == "${rsname}" ]]; then
					let found=1
					break
				fi
			done
			list=
			if [[ ${found} -ne 0 ]]; then
				list=${rsname}
			fi
		fi
	fi
	for item in ${list}
	do
		if [[ ${item} == "<NULL>" ]]; then
			continue
		fi
		if [[ -n "${rstype}" ]]; then
			if [[ -n "${cachefile}" ]] &&
			    [[ -r "${cachefile}" ]]; then
				itemtype="$(sed -n 's/^ *(.*:'${item}') Res resource type:[ 	]*\(.*\)/\1/p' ${cachefile})"
			else
				itemtype="$(${SCHA_RS_GET} -R ${item} -O type)"
			fi
			if [[ ${itemtype} != ${rstype} ]] && [[ ${itemtype} != ${rstype}:* ]]; then
				continue
			fi
		fi

		# Okay
		echo ${item}
	done

	return 0
}

#####################################################
#
# clsetup_get_rs_props() rsname rsflavor rsstate cachefile
#
#	rsname		- Resource name
#
#	rsflavor	- Must be either "extension" or "standard"
#
#	rsstate		- Must be either "enabled"," disabled" or "partially enabled"
#
#	cachefile	- File containing output of scrgadm -pvv
#
#
#	Get resource property attributes for the given "rsname" which
#	are settable based on the current rsstate.
#
#	The following arrays for the given resource are set:
#
#	CLSETUP_RS_PROP_NAMES		- each element contains the name of
#						a resource property.
#	CLSETUP_RS_PROP_DESCS		- each element contains the description
#						of a corresponding rs property.
#	CLSETUP_RS_PROP_TYPES		- each element contains the type of
#						rs property.
#	CLSETUP_RS_PROP_VALUES		- each element contains the current
#						value of an rs property.
#	CLSETUP_RS_IS_PER_NODE_PROP	- each element contains the type of
#						rs extension property (pernode or non-pernode)
#	CLSETUP_RS_PER_NODE_PROP_VALUES	- each element contains the value for pernode
#						extension property.
#
#	This function always returns 0.
#
#####################################################
clsetup_get_rs_props()
{
	typeset rsname=${1}
	typeset rsflavor=${2}
	typeset rsstate=${3}
	typeset cachefile=${4}

	typeset -l proptype			# lower case
	typeset value
	typeset values
	typeset foo

	integer i

	set -A CLSETUP_RS_PROP_NAMES
	set -A CLSETUP_RS_PROP_DESCS
	set -A CLSETUP_RS_PROP_TYPES
	set -A CLSETUP_RS_PROP_VALUES
	set -A CLSETUP_RS_IS_PER_NODE_PROP
	set -A CLSETUP_RS_PER_NODE_PROP_VALUES
	let i=0

	# Make sure we can read the scrgadm file
	if [[ ! -r "${cachefile}" ]]; then
		return 0
	fi

	################
	#
	# Perl script
	#
	################
	/usr/bin/perl -e '

	my $rsname=$ARGV[0];
	my $rsflavor=$ARGV[1];
	my $rsstate=$ARGV[2];
	my $filename=$ARGV[3];

	my $property;
	my $rstype;
	my $tunability;
	my $value;
	my $ispernode;
	my @pernode_props;
	my $nodevalue;
	my $node;
	my @properties;
	my %allowable;

	my %prop_names;
	my %prop_descs;
	my %prop_types;
	my %prop_values;
	my %prop_is_per_node;
	my %prop_node_values;

	# Based on state, set allowable tunability values
	$allowable{"Anytime"} = 1;
	if ($rsstate eq "disabled") {
		$allowable{"When disabled"} = 1;
	}

	# Special handling
	if ($rsflavor eq "standard") {

		# Special handling - R_description
		$property = "R_description";
		push(@properties, $property);
		$prop_names{$property} = 1;
		$prop_descs{$property} = "Description";
		$prop_types{$property} = "String";

		# Special handling - Resource_dependencies
		$property = "Resource_dependencies";
		push(@properties, $property);
		$prop_names{$property} = 1;
		$prop_descs{$property} = "Strong resource dependencies";
		$prop_types{$property} = "Stringarray";

		# Special handling - Resource_dependencies_weak
		$property = "Resource_dependencies_weak";
		push(@properties, $property);
		$prop_names{$property} = 1;
		$prop_descs{$property} = "Weak resource dependencies";
		$prop_types{$property} = "Stringarray";

		# Special handling - Resource_dependencies_restart
		$property = "Resource_dependencies_restart";
		push(@properties, $property);
		$prop_names{$property} = 1;
		$prop_descs{$property} = "Resource restart dependencies";
		$prop_types{$property} = "Stringarray";

		# Special handling - Resource_dependencies_offline
		$property = "Resource_dependencies_offline_restart";
		push(@properties, $property);
		$prop_names{$property} = 1;
		$prop_descs{$property} = "Resource offline restart dependencies";
		$prop_types{$property} = "Stringarray";
	}

	# Get all "extension" or "standard" properties for the resource
	open (FD, "<$filename") || exit(1);
	while (<FD>) {
		($property) = /^\W+\(.*:$rsname:(.*)\)\s+Res property class:\s+$rsflavor/i;
		if ($property) {
			push(@properties, $property);
			$prop_names{$property} = 1;
		}
		if (!$rstype) {
			($rstype) = /^\W+\(.*:$rsname\)\s+Res resource type:\s+(.*)/i;
		}
	}

	# Make sure we have a resource type
	if (!$rstype) {
		exit(1);
	}

	# One more pass to get descriptions, types, and values
	open (FD, "<$filename") || exit(1);
	while (<FD>) {
		# Descriptions
		($property, $value) = /^\W+\(.*:$rsname:(.*)\)\s+Res property description:\s+(.*)/i;
		if ($property && $value && $prop_names{$property}) {
			$prop_descs{$property} = $value;
		}

		# Types
		($property, $value) = /^\W+\(.*:$rsname:(.*)\)\s+Res property type:\s+(.*)/i;
		if ($property && $value && $prop_names{$property}) {
			$prop_types{$property} = $value;
		}

		# Special handling- Pernode property
		if ($rsflavor eq "extension") {
			($property, $ispernode) = /^\W+\(.*:$rsname:(.*)\)\s+Res property pernode:\s+(.*)/i;
			if ($property && $ispernode && $prop_names{$property} ) {
				$prop_is_per_node{$property} = $ispernode;
			 }
		} else {
			$prop_is_per_node{$property} = "False";
		}

		# Special handling- Pernode  extension property value
		($property, $node, $value) = /^\W+\(.*:$rsname:(.*){(.*)}\)\s+Res property value:\s+(.*)/i;
		if ($property && $value && $node && $prop_names{$property} ) {
			$nodevalue = $value."@".$node;
			$prop_node_values{$property} = $prop_node_values{$property}." ".$nodevalue;
		}

		# Values
		($property, $value) = /^\W+\(.*:$rsname:(.*)\)\s+Res property value:\s+(.*)/i;
		if ($property && $value && $prop_names{$property}) {
			$prop_values{$property} = $value;
		}

		# Special handling - R_description
		($value) = /^\W+\(.*:$rsname\)\s+Res R_description:\s+(.*)/i;
		$property = "R_description";
		if ($value && $prop_names{$property}) {
			$prop_values{$property} = $value;
			$prop_is_per_node{$property} = "False";
		}

		# Special handling - Resource_dependencies
		($value) = /^\W+\(.*:$rsname\)\s+Res strong dependencies:\s+(.*)/i;
		$property = "Resource_dependencies";
		if ($value && $prop_names{$property}) {
			$prop_values{$property} = $value;
			$prop_is_per_node{$property} = "False";
		}

		# Special handling - Resource_dependencies_weak
		($value) = /^\W+\(.*:$rsname\)\s+Res weak dependencies:\s+(.*)/i;
		$property = "Resource_dependencies_weak";
		if ($value && $prop_names{$property}) {
			$prop_values{$property} = $value;
			$prop_is_per_node{$property} = "False";
		}

		# Special handling - Resource_dependencies_restart
		($value) = /^\W+\(.*:$rsname\)\s+Res restart dependencies:\s+(.*)/i;
		$property = "Resource_dependencies_restart";
		if ($value && $prop_names{$property}) {
			$prop_values{$property} = $value;
			$prop_is_per_node{$property} = "False";
		}

		# Specialhandling - Resource_dependencies_offline_restart
		($value) = /^\W+\(.*:$rsname\)\s+Res offline restart dependencies:\s+(.*)/i;
		$property = "Resource_dependencies_offline_restart";
		if ($value && $prop_names{$property}) {
			$prop_values{$property} = $value;
			$prop_is_per_node{$property} = "False";
		}
	}

	# Weed out the properties we cannot set because of state
	open (FD, "<$filename") || exit(1);
	while (<FD>) {

		($property, $tunability) = /^\W+\($rstype:([^:]*)\)\s+Res Type param tunability:\s+(.*)/i;

		if ( $rsstate eq "partially enabled" ) {
			if ( $prop_is_per_node{$property} eq "True") {
				# Set the allowable array for a pernode property.
				$allowable{"When disabled"} = 1;
			}
			else {
				# Reset the allowable array for a non pernode property.
				$allowable{"When disabled"} = 0;
			}
		}

		if ( $property && $tunability && $prop_names{$property} && !$allowable{$tunability} ) {
			delete($prop_names{$property});
		}
	}
	close (FD);

	# Fix descriptions, types and values
	foreach $property (keys(%prop_names)) {
		if (!$prop_descs{$property}) {
			$prop_descs{$property} = "-";
		}
		if (!$prop_types{$property}) {
			$prop_types{$property} = "-";
		}
		if (!$prop_values{$property}) {
			$prop_values{$property} = "-";
		}
		if (!$prop_node_values{$property}) {
			$prop_node_values{$property} = "-";
		}
		if (!$prop_is_per_node{$property}) {
			$prop_is_per_node{$property} = "-";
		}
	}

	# Print the list of properties
	foreach $property (@properties) {
		if (!$prop_names{$property}) {
			next;
		}
		print "$property\n";
		print "$prop_types{$property}\n";
		print "$prop_values{$property}\n";
		print "$prop_descs{$property}\n";
		print "$prop_is_per_node{$property}\n";
		print "$prop_node_values{$property}\n";
	}

	################
	#
	# End of Perl script
	#
	################
' "${rsname}" "${rsflavor}" "${rsstate}" "${cachefile}" | \
	while read CLSETUP_RS_PROP_NAMES[i]
	do
		# Types
		read CLSETUP_RS_PROP_TYPES[i]
		if [[ "${CLSETUP_RS_PROP_TYPES[i]}" == "-" ]];  then
			CLSETUP_RS_PROP_TYPES[i]=
		fi
		proptype=${CLSETUP_RS_PROP_TYPES[i]}

		# Values - insert commas as needed
		read values
		if [[ -n "${values}" ]] &&
		    [[ "${values}" != "-" ]];  then
			if [[ "${proptype}" == "stringarray" ]]; then
				foo=
				for value in ${values}
				do
					if [[ -z "${foo}" ]]; then
						foo="${value}"
					else
						foo="${foo},${value}"
					fi
				done
				CLSETUP_RS_PROP_VALUES[i]="${foo}"
			else
				CLSETUP_RS_PROP_VALUES[i]="${values}"
			fi
		fi

		# Descriptions
		read CLSETUP_RS_PROP_DESCS[i]
		if [[ "${CLSETUP_RS_PROP_DESCS[i]}" == "-" ]] ||
		    [[ "${CLSETUP_RS_PROP_DESCS[i]}" == "<NULL>" ]];  then
			CLSETUP_RS_PROP_DESCS[i]=
		fi

		# Per-node property
		read CLSETUP_RS_IS_PER_NODE_PROP[i]
		if [[ "${CLSETUP_RS_IS_PER_NODE_PROP[i]}" == "-" ]] ||
		    [[ "${CLSETUP_RS_IS_PER_NODE_PROP[i]}" == "<NULL>" ]]; then
			CLSETUP_RS_IS_PER_NODE_PROP[i]=
		fi

		# Per-node extension property values.
		read CLSETUP_RS_PER_NODE_PROP_VALUES[i]
		if [[ "${CLSETUP_RS_PER_NODE_PROP_VALUES[i]}" == "-" ]] ||
		    [[ "${CLSETUP_RS_PER_NODE_PROP_VALUES[i]}" == "<NULL>" ]]; then
			CLSETUP_RS_PER_NODE_PROP_VALUES[i]=
		fi
		# Next
		((i += 1))
	done

	return 0
}

#####################################################
#
# clsetup_get_rs_prop_index() ${propname}
#
#	Print the index of the property in the CLSETUP_RS_PROP_NAMES array.
#
#	Case is ignored.
#
#	Return values:
#
#		1		Unknown property
#		0		Success
#
#####################################################
clsetup_get_rs_prop_index()
{
	typeset -l propname=${1}		# lower case

	typeset -l name				# lower case
	integer index
	integer found

	# Get the index
	let index=0
	let found=0
	for name in ${CLSETUP_RS_PROP_NAMES[*]}
	do
		if [[ "${propname}" == "${name}" ]]; then
			let found=1
			break
		fi

		((index += 1))
	done

	# If we didn't find it, return 1
	if [[ ${found} -ne 1 ]]; then
		return 1
	fi

	# Print the index
	echo ${index}

	return 0
}

#####################################################
#
# clsetup_get_rs_prop_value() ${propname}
#
#	Print the property value of the given property using
#	the following arrays:
#
#	CLSETUP_RS_PROP_NAMES		- each element contains the name of
#						a resource property.
#	CLSETUP_RS_PROP_VALUES		- each element contains the value
#						of a corresponding rs property.
#
#	Case is ignored.
#
#	Return values:
#
#		1		Unknown property
#		0		Success
#
#####################################################
clsetup_get_rs_prop_value()
{
	typeset -l propname=${1}		# lower case

	typeset index

	# Get the index
	index=$(clsetup_get_rs_prop_index ${propname})

	# Print the value
	if [[ -n "${index}" ]]; then
		echo ${CLSETUP_RS_PROP_VALUES[index]}
	else
		return 1
	fi

	return 0
}

#####################################################
#
# clsetup_get_rstype_extprop_is_per_node() propname
#
#	Print the extension property type (per-node or not) using
#	the following arrays:
#
#	CLSETUP_RT_PROP_NAMES		-each element contains the name of
#						a resource property.
#	CLSETUP_RT_PROP_IS_PER_NODE	-each element contains the extension
#						prop. type(per-node or not)
#	Case is ignored.
#
#	Return values:
#
#		1		Unknown property
#		0		Success
#
######################################################
clsetup_get_rstype_extprop_is_per_node()
{
	typeset -l propname=${1}

	typeset index

	# Get the index
	index=$(clsetup_get_rstype_extprop_index ${propname})

	# Print the extension prop. type.
	if [[ -n "${index}" ]]; then
		echo ${CLSETUP_RT_PROP_IS_PER_NODE[index]}
	else
		return 1
	fi

	return 0
}

#####################################################
#
# clsetup_get_rs_prop_is_per_node_prop() ${propname}
#
#	Print whether the given property is per-node or not using
#	the following arrays:
#
#	CLSETUP_RS_PROP_NAMES		-each element contains the name of
#						a resource property.
#	CLSETUP_RS_IS_PER_NODE_PROP	-each element contains the boolean value
#						 of a corresponding rs property.
#
#	case is ignored.
#
#	Return values:
#
#		-1			Unknown property
#		 0			Success
#####################################################
clsetup_get_rs_prop_is_per_node_prop()
{
	typeset propname=${1}

	typeset index
	# Get the index

	index=$(clsetup_get_rs_prop_index ${propname})

	# Print the extension property type.
	if [[ -n "${index}" ]]; then
		echo ${CLSETUP_RS_IS_PER_NODE_PROP[index]}
	else
		return -1
	fi

	return 0
}

#####################################################
#
# clsetup_get_rs_per_node_ext_prop_values() ${propname}
#
#	Print the per-node property of the given property using
#	the following arrays:
#
#	CLSETUP_RS_PROP_NAMES		-each element contains the name of
#						a resource property.
#	CLSETUP_RS_PER_NODE_PROP_VALUES	-each element contains the boolean value
#						of a corresponding rs property.
#
#	case is ignored.
#
#	Return values:
#
#		-1			Unknown property
#		 0			Success
#####################################################
clsetup_get_rs_per_node_ext_prop_values()
{
	typeset -l propname=${1}

	typeset index
	#Get the index
	index=$(clsetup_get_rs_prop_index ${propname})

	# Print the per-node extension property node value pair.
	if [[ -n "${index}" ]]; then
			echo ${CLSETUP_RS_PER_NODE_PROP_VALUES[index]}
	else
		return -1
	fi

	return 0
}

#####################################################
#
# clsetup_get_rs_prop_type() ${propname}
#
#	Print the property type of the given property using
#	the following arrays:
#
#	CLSETUP_RS_PROP_NAMES		- each element contains the name of
#						a resource property.
#	CLSETUP_RS_PROP_TYPES		- each element contains the type
#						of a corresponding rs property.
#
#	Case is ignored.
#
#	Return values:
#
#		-1		Unknown property
#		0		Success
#
#####################################################
clsetup_get_rs_prop_type()
{
	typeset -l propname=${1}		# lower case

	typeset index

	# Get the index
	index=$(clsetup_get_rs_prop_index ${propname})

	# Print the value
	if [[ -n "${index}" ]]; then
		echo ${CLSETUP_RS_PROP_TYPES[index]}
	else
		return -1
	fi

	return 0
}

#####################################################
#
# clsetup_get_rs_prop_description() propname
#
#	Print the property description of the given property using
#	the following arrays:
#
#	CLSETUP_RS_PROP_NAMES		- each element contains the name of
#						a resource property.
#	CLSETUP_RS_PROP_DESC		- each element contains the descript
#						of a corresponding rs property.
#
#	Case is ignored.
#
#	Return values:
#
#		-1		Unknown property
#		0		Success
#
#####################################################
clsetup_get_rs_prop_description()
{
	typeset -l propname=${1}		# lower case

	typeset index

	# Get the index
	index=$(clsetup_get_rs_prop_index ${propname})

	# Print the value
	if [[ -n "${index}" ]]; then
		echo ${CLSETUP_RS_PROP_DESCS[index]}
	else
		return 1
	fi

	return 0
}

#####################################################
#
# clsetup_get_upgradeable_resources() [cachefile]
#
#	Set the following arrays:
#
#	CLSETUP_RS_NAMES_UPGD		- each element contains the name of
#						a resource.  If a resource
#						can be upgraded to more than
#						one version, there will be
#						an element for each version.
#	CLSETUP_RS_CVERS_UPGD		- each element contains the current
#						version of a corresponding
#						RS_NAME.
#	CLSETUP_RS_TYPE_UPGD		- each element contains the resource
#						type of the corresponding
#						rs_name, without version.
#	CLSETUP_RS_PVERS_UPGD		- each element contains a possible
#						version upgrade of a
#						corresponding rs_name.
#						Items in this array may be
#						NULL.
#	CLSETUP_RS_WHEN_UPGD		- each element contains the "when"
#						restriction of a
#						version upgrade of a
#						corresponding rs_name.
#						Items in this array may be
#						NULL.
#
#	Note that there will be at least one element in each array
#	for each resource, even if the resource has no upgradable options.
#
#	Also note that there will be multiple elements in each array
#	for those resources which can be upgraded to more than one version.
#
#	If "cachefile" is given, it is searched, rather than current
#	scha calls
#
#	This function always returns zero.
#
#####################################################
clsetup_get_upgradeable_resources()
{
	typeset cachefile=${1}

	set -A CLSETUP_RS_NAMES_UPGD
	set -A CLSETUP_RS_CVERS_UPGD
	set -A CLSETUP_RS_TYPE_UPGD
	set -A CLSETUP_RS_PVERS_UPGD
	set -A CLSETUP_RS_WHEN_UPGD

	# Variables for call to clsetup_get_upgradeable_rstypes()
	typeset CLSETUP_RT_NAMES_UPGD		# RT names
	typeset CLSETUP_RT_VERS_UPGD		# RT versions
	typeset CLSETUP_RT_FROM_UPGD		# RT upgrade from versions
	typeset CLSETUP_RT_WHEN_UPGD		# RT upgrade when

	typeset rslist
	typeset rsnames
	typeset rsname
	typeset rstypes
	typeset rstype
	typeset rsversions
	typeset rsversion

	integer i1
	integer i2
	integer i3

	# If not a cluster member, we at least need a cachefile
	if [[ ${CLSETUP_ISMEMBER} -ne 1 ]] &&
	    [[ ! -r "${cachefile}" ]]; then
		return 0
	fi

	# Get the list of resources
	rslist="$(clsetup_get_rslist "" "" ${cachefile})"

	# For each resource, get the resource names, types, versions
	let i1=0
	set -A rsnames
	set -A rstypes
	set -A rsversions
	for rsname in ${rslist}
	do
		# Get the name of the resource type
		if [[ -n "${cachefile}" ]] &&
		    [[ -r "${cachefile}" ]]; then
			rstype="$(sed -n 's/^ *(.*:'${rsname}') Res resource type:[ 	]*\(.*\)/\1/p' ${cachefile})"
		else
			rstype="$(${SCHA_RS_GET} -R ${rsname} -O type)"
		fi
		if [[ -z "${rstype}" ]]; then
			continue
		fi

		# Strip the version from the rt name
		rstype=$(IFS=: ; set -- ${rstype};  echo ${1})

		# Get the rs version
		if [[ -n "${cachefile}" ]] &&
		    [[ -r "${cachefile}" ]]; then
			# If 3.1 or beyond, resource will have version
			rsversion="$(sed -n 's/^ *(.*:'${rsname}') Res type version:[ 	]*\(.*\)/\1/p' ${cachefile})"

			# Otherwise, it inherits from resource type
			if [[ -z "${rsversion}" ]]; then
				rsversion="$(sed -n 's/^ *('${rstype}') Res Type version:[ 	]*\(.*\)/\1/p' ${cachefile})"
			fi
		else
			# If 3.1 or beyond, resource will have version
			rsversion="$(${SCHA_RS_GET} -R ${rsname} -O rt_version)"

			# Otherwise, it inherits from resource type
			if [[ -z "${rsversion}" ]]; then
				rsversion="$(${SCHA_RT_GET} -T ${rstype} -O rt_version)"
			fi
		fi
		if [[ -z "${rsversion}" ]]; then
			# See if associated type has a version
			rsversion="${CLSETUP_RT_NOVERSION}"
		fi

		# Add to lists
		rsnames[i1]=${rsname}
		rstypes[i1]=${rstype}
		rsversions[i1]=${rsversion}

		# Next
		((i1 += 1))
	done

	# Get the rt upgrade data
	set -A CLSETUP_RT_NAMES_UPGD
	set -A CLSETUP_RT_VERS_UPGD
	set -A CLSETUP_RT_FROM_UPGD
	set -A CLSETUP_RT_WHEN_UPGD
	clsetup_get_upgradeable_rstypes ${cachefile}

	# Set up the exported arrays
	let i1=0
	let i2=0
	while [[ -n "${rsnames[i1]}" ]]
	do
		CLSETUP_RS_NAMES_UPGD[i2]=${rsnames[i1]}
		CLSETUP_RS_CVERS_UPGD[i2]=${rsversions[i1]}
		CLSETUP_RS_TYPE_UPGD[i2]=${rstypes[i1]}

		let i3=0
		while [[ -n "${CLSETUP_RT_NAMES_UPGD[i3]}" ]]
		do
			if [[ "${CLSETUP_RT_NAMES_UPGD[i3]}" != "${CLSETUP_RS_TYPE_UPGD[i2]}" ]] ||
			    [[ "${CLSETUP_RT_FROM_UPGD[i3]}" != "${CLSETUP_RS_CVERS_UPGD[i2]}" ]]; then
				# Next RT
				((i3 += 1))
				continue
			fi

			# Add another RS?
			if [[ -n "${CLSETUP_RS_PVERS_UPGD[i2]}" ]]; then
				((i2 += 1))
				CLSETUP_RS_NAMES_UPGD[i2]=${rsnames[i1]}
				CLSETUP_RS_CVERS_UPGD[i2]=${rsversions[i1]}
				CLSETUP_RS_TYPE_UPGD[i2]=${rstypes[i1]}
			fi

			# Add possible new version and when to RS
			CLSETUP_RS_PVERS_UPGD[i2]=${CLSETUP_RT_VERS_UPGD[i3]}
			CLSETUP_RS_WHEN_UPGD[i2]="${CLSETUP_RT_WHEN_UPGD[i3]}"

			# Next RT
			((i3 += 1))
		done

		# Next RS
		((i1 += 1))
		((i2 += 1))
	done

	return 0

}

#####################################################
#
# clsetup_get_upgradeable_rstypes() [cachefile]
#
#	Set the following arrays:
#
#	CLSETUP_RT_NAMES_UPGD		- each element contains the name of
#						an upgradeable resource type,
#						without version.
#	CLSETUP_RT_VERS_UPGD		- each element contains the version
#						of a corresponding rs type.
#	CLSETUP_RT_FROM_UPGD		- each element contains a
#						"from" version of a
#						corresponding rs type.
#	CLSETUP_RT_WHEN_UPGD		- each element contains a
#						"when" corresponding rs
#						type can be upgraded to.
#
#	If "cachefile" is given, it is searched, rather than current
#	scrgadm command.
#
#	This function always returns zero.
#
#####################################################
clsetup_get_upgradeable_rstypes()
{
	typeset cachefile=${1}

	set -A CLSETUP_RT_NAMES_UPGD
	set -A CLSETUP_RT_VERS_UPGD
	set -A CLSETUP_RT_FROM_UPGD
	set -A CLSETUP_RT_WHEN_UPGD

	typeset cmd
	typeset rstype
	typeset version
	typeset from
	typeset when

	integer index

	# Get the types
	if [[ -n "${cachefile}" ]] && [[ -r "${cachefile}" ]]; then
		cmd="cat ${cachefile}"
	else
		cmd="${SC_SCRGADM} -pvv -t ."
	fi
	let index=0
	${cmd} | sed -n 's/^ *(\(.*\)) Res Type upgrade from \(.*\):[    ]*\(.*\)/\1 \2 \3/p' | while read rstype from when
	do
		if [[ -z "${rstype}" ]] ||
		    [[ -z "${from}" ]] ||
		    [[ -z "${when}" ]]; then
			continue
		fi

		(IFS=: ; set -- ${rstype}; echo $*) | read rstype version
		if [[ -z "${version}" ]]; then
			if [[ ${CLSETUP_ISMEMBER} -eq 1 ]];  then
				version="$(${SCHA_RT_GET} -T ${rstype} -O rt_version)"
			fi
			if [[ -z "${version}" ]]; then
				version="${CLSETUP_RT_NOVERSION}"
			fi
		fi
		CLSETUP_RT_NAMES_UPGD[index]=${rstype}
		CLSETUP_RT_VERS_UPGD[index]=${version}
		CLSETUP_RT_FROM_UPGD[index]=${from}
		CLSETUP_RT_WHEN_UPGD[index]="${when}"

		# Next
		((index += 1))
	done

	return 0


	return 0
}

#####################################################
#
# clsetup_get_registered_rstypes() no_system no_nettypes [cachefile]
#
#	Set the following arrays:
#
#	CLSETUP_RT_NAMES_REG		- each element contains the name of
#						a registered resource type.
#	CLSETUP_RT_DESCS_REG		- each element contains the description
#						of a corresponding rs type.
#	CLSETUP_RT_MODE_REG		- each element contains "failover" or
#						"scalable" for a corresponding
#						resource type.
#
#	If no_system is ${CL_NOSYSRT}, do not include resources whose RT_SYSTEM property is
#	set to TRUE; if it is not, include them.
#
#	If "no_nettypes" is given, do not return network resources.
#
#	If "cachefile" is given, it is searched, rather than current
#	scha command output.
#
#	This function always returns zero.
#
#####################################################
clsetup_get_registered_rstypes()
{
	integer no_system=${1}
	typeset no_nettypes=${2}
	typeset cachefile=${3}

	set -A CLSETUP_RT_NAMES_REG
	set -A CLSETUP_RT_DESCS_REG
	set -A CLSETUP_RT_MODE_REG

	integer index
	typeset rstype
	typeset rstypes
	typeset -l lmode		# lower case

	# Get the types
	if [[ -n "${cachefile}" ]] && [[ -r "${cachefile}" ]]; then
		set -A rstypes $(sed -n 's/^ *Res Type name:[  ]*\(.*\)/\1/p' ${cachefile})
	else
		set -A rstypes $(${SCHA_CLUSTER_GET} -O all_resourcetypes)
	fi

	# Set CLSETUP_RT_NAMES_REG, filtering out net resources, if needed
	if [[ -n "${no_nettypes}" ]]; then
		set -A CLSETUP_RT_NAMES_REG
		let index=0
		for rstype in ${rstypes[*]}
		do
			clsetup_is_network_rstype ${rstype} || continue
			# Filter out resources whose RT_SYSTEM is true.
			if [[ ${no_system} -eq ${CL_NOSYSRT} ]]; then
				clsetup_is_system_rstype ${rstype} ${cachefile} || continue
			fi
			CLSETUP_RT_NAMES_REG[index]=${rstype}
			((index += 1))
		done
	else
		set -A CLSETUP_RT_NAMES_REG ${rstypes[*]}
	fi

	# And, the descriptions and types
	let index=0
	for rstype in ${CLSETUP_RT_NAMES_REG[*]}
	do
		if [[ -n "${cachefile}" ]] && [[ -r "${cachefile}" ]]; then
			CLSETUP_RT_DESCS_REG[index]="$(sed -n 's/^ *('${rstype}') Res Type description:[  ]*\(.*\)/\1/p' ${cachefile})"
			lmode="$(sed -n 's/^ *('${rstype}') Res Type failover:[ 	]*\(.*\)/\1/p' ${cachefile})"
		else
			CLSETUP_RT_DESCS_REG[index]="$(${SCHA_RT_GET} -T ${rstype} -O rt_description)"
			lmode="$(${SCHA_RT_GET} -T ${rstype} -O failover)"
		fi

		# false is "scalable", true is "failover"
		if [[ "${lmode}" == "false" ]]; then
			CLSETUP_RT_MODE_REG[index]=scalable
		else
			CLSETUP_RT_MODE_REG[index]=failover
		fi

		# Next
		((index += 1))
	done

	return 0
}

#####################################################
#
# clsetup_get_unregistered_rstypes() [no_nettypes]
#
#	Set the following arrays:
#
#	CLSETUP_RT_NAMES_UNREG		- each element contains the name of
#						an unregistered type.
#	CLSETUP_RT_DESCS_UNREG		- each element contains the description
#						of a corresponding unreg. type.
#	CLSETUP_RT_MODE_UNREG		- each element contains "failover" or
#						"scalable" for a corresponding
#						resource type.
#
#	clsetup_get_unregistered_rstypes() should always be called first
#	to setup the _REG arrays.  Otherwise, all resource types will
#	be returned.
#
#	If "no_nettypes" is given, do not return network resources.
#
#	This function always returns zero.
#
#####################################################
clsetup_get_unregistered_rstypes()
{
	typeset no_nettypes=${1}

	set -A CLSETUP_RT_NAMES_UNREG
	set -A CLSETUP_RT_DESCS_UNREG
	set -A CLSETUP_RT_MODE_UNREG

	integer index
	typeset rstype
	typeset file
	typeset key
	typeset vid
	typeset upgradeable
	typeset version
	typeset description
	typeset -l lmode		# lower case
	typeset rt_fullname
	typeset	rt_version
	typeset rt_basename
	integer found

	# Get from files
	let index=0
	for file in ${CLSETUP_RTR_DIR}/* ${CLSETUP_OPT_RTR_DIR}/*
	do
		# Use awk script to grab all variables of interest
		(
		    LC_ALL=C; export LC_ALL;
		    nawk -F= '
			BEGIN {
				len = 5;

				keys[1] = "vendor_id";
				keys[2] = "resource_type";
				keys[3] = "rt_version";
				keys[4] = "failover";
				keys[5] = "rt_description";

				values[1] = "-";
				values[2] = "-";
				values[3] = "-";
				values[4] = "-";
				values[5] = "-";

				upgrade = "-";
			}

			/#\$upgrade/ {
				upgrade = "true";
			}

			/[^#]*[=]/ {
				split($0, array, "=");
				gsub(/[	 ]*/, "", array[1]);
				key = tolower(array[1]);
				for (i = 1;  i <= len;  ++i) {
					if (key == keys[i]) {
						value=array[2];
						sub(/[ 	"]*/, "", value);
						sub(/[";].*/, "", value);
						values[i] = value;
					}
				}
			}

			END {
				printf("%s %s %s %s %s %s\n",
				    values[1], values[2], values[3],
				    values[4], upgrade, values[5]);
			}

		    ' <${file}
		) | read vid rstype version lmode upgradeable description

		# Clear any variables set to "-"
		if [[ "${vid}" == "-" ]]; then
			vid=
		fi
		if [[ "${rstype}" == "-" ]]; then
			rstype=
		fi
		if [[ "${version}" == "-" ]]; then
			version=
		fi
		if [[ "${lmode}" == "-" ]]; then
			lmode=
		fi
		if [[ "${upgradeable}" == "-" ]]; then
			upgradeable=
		fi
		if [[ "${description}" == "-" ]]; then
			description=
		fi

		# Name
		if [[ -z "${rstype}" ]]; then
			continue
		fi

		# Pre-pend venor ID
		if [[ -n "${vid}" ]]; then
			rstype=${vid}.${rstype}
		fi

		# Check for net type
		if [[ -n "${no_nettypes}" ]]; then
			clsetup_is_network_rstype ${rstype} || continue
		fi

		# If it is already registered, skip it
		let found=0
		for rt_fullname in ${CLSETUP_RT_NAMES_REG[*]}
		do
			rt_basename=`echo $rt_fullname | cut -d: -f1`
			if [[ "${rstype}" == "${rt_basename}" ]]; then
				rt_version="$(${SCHA_RT_GET} -T ${rt_fullname} -O rt_version)"
				if [[ "${version}" == "${rt_version}" ]]; then
					let found=1
					break
				fi
			fi
		done

		# Skip it
		if [[ ${found} -eq 1 ]]; then
			continue
		elif [[ -n "${version}" ]]; then
			rstype=${rstype}:${version}
		fi

		# Not registered, add to arrays
		CLSETUP_RT_NAMES_UNREG[index]=${rstype}

		# Description
		CLSETUP_RT_DESCS_UNREG[index]="${description}"

		# Failover or scalable?
		if [[ "${lmode}" == "true" ]]; then
			CLSETUP_RT_MODE_UNREG[index]="failover"
		else
			CLSETUP_RT_MODE_UNREG[index]="scalable"
		fi

		# Next
		((index += 1))
	done

	return 0
}



#####################################################
#
# clsetup_get_rstype_extprops() rstype_name
#
#	Set the following arrays for the given resource type name:
#
#	CLSETUP_RT_PROP_NAMES		- each element contains the name of
#						a resource property.
#	CLSETUP_RT_PROP_DESCS		- each element contains the description
#						of a corresponding rs property.
#	CLSETUP_RT_PROP_TYPES		- each element contains the type of
#						rs property.
#	CLSETUP_RT_PROP_DFLTS		- each element contains the default
#						value of an rs property.
#	CLSETUP_RT_PROPS_IS_PER_NODE	- each element contains the boolean
#						value whether the given property
#						is per-node or not.
#
#	We only look at registered resource types.
#
#	Note that we cannot use the scha_cmds(1ha) here, since there are
#	no options for getting resource type parameters. we use scrgadm command
#	to gather the information.
#
#	This function always returns zero.
#
#####################################################
clsetup_get_rstype_extprops()
{
	typeset rstype_name=${1}

	integer i

	set -A CLSETUP_RT_PROP_NAMES
	set -A CLSETUP_RT_PROP_DESCS
	set -A CLSETUP_RT_PROP_TYPES
	set -A CLSETUP_RT_PROP_DFLTS
	let i=0

	################
	#
	# Perl script
	#
	################
	/usr/bin/perl -e '

	my $rtname=$ARGV[0];
	my $filename=$ARGV[1];

	my $property;
	my $value;
	my @properties;

	my %prop_names;
	my %prop_descs;
	my %prop_types;
	my %prop_defaults;
	my %prop_is_per_node;

	# Get all "extension" properties for the resource type
	open (FD, "<$filename") || exit(1);
	while (<FD>) {
		($property) = /^\W+\($rtname:([^:]*)\)\s+Res Type param extension:\s+True/i;
		if ($property) {
			push(@properties, $property);
			$prop_names{$property} = 1;
		}
	}

	# One more pass to get descriptions, types, and defaults
	open (FD, "<$filename") || exit(1);
	while (<FD>) {
		# Descriptions
		($property, $value) = /^\W+\($rtname:([^:]*)\)\s+Res Type param description:\s+(.*)/i;
		if ($property && $value && $prop_names{$property}) {
			$prop_descs{$property} = $value;
		}

		# Types
		($property, $value) = /^\W+\($rtname:([^:]*)\)\s+Res Type param type:\s+(.*)/i;
		if ($property && $value && $prop_names{$property}) {
			$prop_types{$property} = $value;
		}

		# Defaults
		($property, $value) = /^\W+\($rtname:([^:]*)\)\s+Res Type param default:\s+(.*)/i;
		if ($property && $value && $prop_names{$property}) {
			$prop_defaults{$property} = $value;
		}

		# per-node
		($property, $value) = /^\W+\($rtname:([^:]*)\)\s+Res Type param per-node:\s+(.*)/i;
		if ($property && $value && $prop_names{$property}) {
			$prop_is_per_node{$property} = $value;
		}
	}
	close (FD);

	# Fix descriptions, types and values
	foreach $property (keys(%prop_names)) {
		if (!$prop_descs{$property}) {
			$prop_descs{$property} = "-";
		}
		if (!$prop_types{$property}) {
			$prop_types{$property} = "-";
		}
		if (!$prop_defaults{$property}) {
			$prop_defaults{$property} = "-";
		}
		if (!$prop_is_per_node{$property}) {
			$prop_is_per_node{$property} = "-";
		}
	}

	# Print the list of properties
	foreach $property (@properties) {
		if (!$prop_names{$property}) {
			next;
		}
		print "$property\n";
		print "$prop_types{$property}\n";
		print "$prop_descs{$property}\n";
		print "$prop_defaults{$property}\n";
		print "$prop_is_per_node{$property}\n";
	}

	################
	#
	# End of Perl script
	#
	################
' ${rstype_name} ${cachefile} | \
	while read CLSETUP_RT_PROP_NAMES[i]
	do
		# Types
		read CLSETUP_RT_PROP_TYPES[i]
		if [[ "${CLSETUP_RT_PROP_TYPES[i]}" == "-" ]] ||
		    [[ "${CLSETUP_RT_PROP_TYPES[i]}" == "<NULL>" ]];  then
			CLSETUP_RT_PROP_TYPES[i]=
		fi

		# Descriptions
		read CLSETUP_RT_PROP_DESCS[i]
		if [[ "${CLSETUP_RT_PROP_DESCS[i]}" == "-" ]] ||
		    [[ "${CLSETUP_RT_PROP_DESCS[i]}" == "<NULL>" ]];  then
			CLSETUP_RT_PROP_DESCS[i]=
		fi

		# Defaults
		read CLSETUP_RT_PROP_DFLTS[i]
		if [[ "${CLSETUP_RT_PROP_DFLTS[i]}" == "-" ]] ||
		    [[ "${CLSETUP_RT_PROP_DFLTS[i]}" == "<unset>" ]];  then
			CLSETUP_RT_PROP_DFLTS[i]=
		fi

		# Per-node
		read CLSETUP_RT_PROP_IS_PER_NODE[i]
		if [[ "${CLSETUP_RT_IS_PER_NODE[i]}" == "-" ]] ||
		    [[ "${CLSETUP_RT_PROP_IS_PER_NODE[i]}" == "<unset>" ]]; then
				CLSETUP_RT_PROP_IS_PER_NODE[i]=
		fi

		# Next
		((i += 1))
	done

	return 0
}

#####################################################
#
# clsetup_get_rstype_extprop_index() ${propname}
#
#	Prin the index of the property in the CLSETUP_RT_PROP_NAMES array.
#
#	Case is ignored.
#
#	Return values:
#
#		-1		Unknown property
#		0		Success
#
#####################################################
clsetup_get_rstype_extprop_index()
{
	typeset -l propname=${1}		# lower case

	typeset -l name				# lower case
	integer index
	integer found

	# Get the index
	let index=0
	let found=0
	for name in ${CLSETUP_RT_PROP_NAMES[*]}
	do
		if [[ "${propname}" == "${name}" ]]; then
			let found=1
			break
		fi

		((index += 1))
	done

	# If we didn't find it, return -1
	if [[ ${found} -ne 1 ]]; then
		return 1
	fi

	# Print the index
	echo ${index}

	return 0
}

#####################################################
#
# clsetup_get_rstype_extprop_type() propname
#
#	Print the extenstion property type of the given propname using
#	the following arrays:
#
#	CLSETUP_RT_PROP_NAMES		- each element contains the name of
#						a resource property.
#	CLSETUP_RT_PROP_TYPES		- each element contains the type
#						of a corresponding rs property.
#
#	Case is ignored.
#
#	Return values:
#
#		-1		Unknown property
#		0		Success
#
#####################################################
clsetup_get_rstype_extprop_type()
{
	typeset -l propname=${1}		# lower case

	typeset index

	# Get the index
	index=$(clsetup_get_rstype_extprop_index ${propname})

	# Print the value
	if [[ -n "${index}" ]]; then
		echo ${CLSETUP_RT_PROP_TYPES[index]}
	else
		return 1
	fi

	return 0
}

#####################################################
#
# clsetup_get_rstype_extprop_desc() propname
#
#	Print the extenstion property description of the given propname using
#	the following arrays:
#
#	CLSETUP_RT_PROP_NAMES		- each element contains the name of
#						a resource property.
#	CLSETUP_RT_PROP_DESCS		- each element contains the description
#						of a corresponding rs property.
#
#	Case is ignored.
#
#	Return values:
#
#		-1		Unknown property
#		0		Success
#
#####################################################
clsetup_get_rstype_extprop_desc()
{
	typeset -l propname=${1}		# lower case

	typeset index

	# Get the index
	index=$(clsetup_get_rstype_extprop_index ${propname})

	# Print the value
	if [[ -n "${index}" ]]; then
		echo ${CLSETUP_RT_PROP_DESCS[index]}
	else
		return 1
	fi

	return 0
}

#####################################################
#
# clsetup_get_rstype_extprop_dflt() propname
#
#	Print the extenstion property default of the given propname using
#	the following arrays:
#
#	CLSETUP_RT_PROP_NAMES		- each element contains the name of
#						a resource property.
#	CLSETUP_RT_PROP_DFLTS		- each element contains the description
#						of a corresponding rs property.
#
#	Case is ignored.
#
#	This function serves the same purpose as clsetup_get_prop_default(),
#	but can only be used when the indicated arrays have first been
#	properly initialized.
#
#	Return values:
#
#		-1		Unknown property
#		0		Success
#
#####################################################
clsetup_get_rstype_extprop_dflt()
{
	typeset -l propname=${1}		# lower case

	typeset index

	# Get the index
	index=$(clsetup_get_rstype_extprop_index ${propname})

	# Print the value
	if [[ -n "${index}" ]]; then
		echo ${CLSETUP_RT_PROP_DFLTS[index]}
	else
		return 1
	fi

	return 0
}

#####################################################
#
# clsetup_get_prop_default() rstype property [cachefile]
#
#	Test to see if the "property" exists for the given resource type.
#	If it does, print the default value.
#
#	Note that we cannot use the scha_cmds(1ha) here, since there are
#	no options for getting resource type parameters.
#
#	If "cachefile" is given, it is searched, rather than current
#	scha output.
#
#	Return values:
#
#		-1		Unknown property
#		0		Success
#
#####################################################
clsetup_get_prop_default()
{
	typeset rstype=${1}
	typeset property=${2}
	typeset cachefile=${3}

	typeset item
	typeset prop_val
	typeset default

	integer found

	# See if the property even exists for this resource type
	if [[ -n "${cachefile}" ]] && [[ -r "${cachefile}" ]]; then
		list="$(sed -n 's/^ *('${rstype}') Res Type param name:[ 	]*\(.*\)/\1/p' ${cachefile})"
	else
		list="$(
			LC_ALL=C; export LC_ALL;
			${SC_SCRGADM} -pvv -t ${rstype} 2>/dev/null | sed -n 's/^ *('${rstype}') Res Type param name:[ 	]*\(.*\)/\1/p'
		)"
	fi
	let found=0
	for item in ${list}
	do
		if [[ "${item}" == "${property}" ]]; then
			let found=1
			break
		fi
	done
	if [[ ${found} -eq 0 ]]; then
		return -1
	fi

	# If the property exists, print the default value
	if [[ -n "${cachefile}" ]] && [[ -r "${cachefile}" ]]; then
		default="$(sed -n 's/^ *('${rstype}:${property}') Res Type param default:[ 	]*\(.*\)/\1/p' ${cachefile})"
	else
		default="$(
			LC_ALL=C; export LC_ALL;
			${SC_SCRGADM} -pvv -t ${rstype} 2>/dev/null | sed -n 's/^ *('${rstype}:${property}') Res Type param default:[ 	]*\(.*\)/\1/p'
		)"
	fi

	if [[ -n "${default}" ]]; then
		echo "${default}"
	fi

	return 0
}

#####################################################
#
# clsetup_get_resource() rsname
#
#	Test to see if the resource exists. If it does, print the name.
#
#	This function always returns zero.
#
#####################################################
clsetup_get_resource()
{
	typeset rsname=${1}

	${SCHA_RS_GET} -R ${rsname} -O type >/dev/null 2>&1
	if [[ $? -ne ${SCHA_ERR_RSRC} ]]; then
		echo "${rsname}"
	fi

	return 0
}

#####################################################
#
# clsetup_get_rg_dependencies() rgname [flag]
#
#	Print a list of all resource groups which depend upon "rgname".
#	If "flag" is set to "offline", only list those groups which are
#	offline.   If "flag" is set to "unmanaged", only list those groups
#	which are unmanaged.
#
#	This function always returns zero.
#
#####################################################
clsetup_get_rg_dependencies()
{
	typeset rgname=${1}
	typeset flag=${2}

	typeset rgstatefile=${CLSETUP_TMP_RGSTATE}

	typeset dep
	typeset list
	typeset nodelist

	# If not a cluster member, just return
	if [[ ${CLSETUP_ISMEMBER} -ne 1 ]];  then
		return 0
	fi

	# Get the rg_dependencies
	list="$(${SCHA_RG_GET} -G ${rgname} -O rg_dependencies)"
	if [[ -z "${list}" ]]; then
		return 0
	fi

	# If no "flag" is given, list all RG_dependencies
	if [[ -z "${flag}" ]]; then
		deps="${list}"

	# Otherwise, list either "offline" or "unmanaged" dependencies
	else
		# Create the rgstate file
		clsetup_create_rgstatefile ${rgstatefile}

		# Get the dependencies list
		for dep in ${list}
		do
			# Update depended on list based on "flag"
			if [[ "${flag}" == "offline" ]]; then

				# If any node is online, do NOT add to the list
				nodelist=$(clsetup_get_rgstate ${dep} ${rgstatefile} "not_offline")
				if [[ -z "${nodelist}" ]]; then
					deps="${deps} ${dep}"
				fi
			elif [[ "${flag}" == "unmanaged" ]]; then

				# If any node is unmanaged, do add to the list
				nodelist=$(clsetup_get_rgstate ${dep} ${rgstatefile} "unmanaged")
				if [[ -n "${nodelist}" ]]; then
					deps="${deps} ${dep}"
				fi
			fi
		done

		# Done with rgstate file
		clsetup_rm_rgstatefile ${rgstatefile}
	fi

	# If there are any, print them
	if [[ -n "${deps}" ]]; then
		echo ${deps}
	fi

	return 0
}

#####################################################
#
# clsetup_get_resource_depended_ons() rsname
#
#	Print a list of all resources which depend upon "rsname".
#	This includes strong, weak, restart and offline_restart 
#	dependencies.
#
#	This function always returns zero.
#
#####################################################
clsetup_get_resource_depended_ons()
{
	typeset rsname=${1}

	typeset rgname
	typeset rglist
	typeset deps
	typeset resources
	typeset resource
	typeset list
	typeset -l lfoo			# lower case
	typeset i
	typeset j
	typeset tmprglist

	integer found

	# Get the RG name
	rgname="$(${SCHA_RS_GET} -R ${rsname} -O group)" || return 0

	# Check for all the resources in all the resource groups for dependencies
	rglist="$(clsetup_get_rglist)"
	for tmprglist in ${rglist}
	do
		resources="$(${SCHA_RG_GET} -G ${tmprglist} -O resource_list)"
		for resource in ${resources}
		do
			# Skip myself
			if [[ "${resource}" == "${rsname}" ]]; then
				continue
			fi

			# Strong
			foo="$(${SCHA_RS_GET} -R ${resource} -G ${rgname} -O resource_dependencies)"

			# Weak
			foo="${foo} $(${SCHA_RS_GET} -R ${resource} -G ${rgname} -O resource_dependencies_weak)"

			# Restart
			foo="${foo} $(${SCHA_RS_GET} -R ${resource} -G ${rgname} -O resource_dependencies_restart)"

			# Offline_restart
			foo="${foo} $(${SCHA_RS_GET} -R ${resource} -G ${rgname} -O resource_dependencies_offline_restart)"

			# See if we are in their strong, weak or restart dependency list
			for i in ${foo}
			do
				if [[ $i == ${rsname} ]]; then
					deps="${deps} ${resource}"
					break
				fi
			done
		done
	done

	# Filter out any duplicates
	list=
	for i in ${deps}
	do
		let found=0
		for j in ${list}
		do
			if [[ $i == $j ]]; then
				let found=1
				break
			fi
		done
		if [[ ${found} -eq 0 ]]; then
			list="${list} $i"
		fi
	done

	# Print the list of dependencies which depend upon me
	if [[ -n "${list}" ]]; then
		echo ${list}
	fi

	return 0
}

#####################################################
#
# clsetup_get_rg_depended_on() rgname [flag]
#
#	Print a list of all resource groups which depend upon "rgname".
#	If "flag" is set to "online", only list those groups which are online.
#	Otherwise, list all managed groups which depend on "rgname".
#
#	This function always returns zero.
#
#####################################################
clsetup_get_rg_depended_on()
{
	typeset rgname=${1}
	typeset flag=${2}

	typeset rgstatefile=${CLSETUP_TMP_RGSTATE}

	typeset rglist
	typeset rg
	typeset list
	typeset foo
	typeset deps
	typeset nodelist

	# If not a cluster member, just return
	if [[ ${CLSETUP_ISMEMBER} -ne 1 ]];  then
		return 0
	fi

	# Create the rgstate file
	clsetup_create_rgstatefile ${rgstatefile}

	# Get the list of depended ons
	deps=
	rglist="$(clsetup_get_rglist)"
	for rg in ${rglist}
	do
		# If it is us, skip it
		if [[ "${rg}" == "${rgname}" ]]; then
			continue
		fi

		# Get the rg_dependencies
		list="$(${SCHA_RG_GET} -G ${rg} -O rg_dependencies)"
		if [[ -z "${list}" ]]; then
			continue
		fi

		# See if we are in the list
		for foo in ${list}
		do
			if [[ "${foo}" == "${rgname}" ]]; then

				# Update depended on list
				if [[ "${flag}" == "online" ]]; then
					nodelist=$(clsetup_get_rgstate ${rg} ${rgstatefile} "online")
					if [[ -n "${nodelist}" ]]; then
						deps="${deps} ${rg}"
					fi
				else
					nodelist=$(clsetup_get_rgstate ${rg} ${rgstatefile} "unmanaged")
					if [[ -z "${nodelist}" ]]; then
						deps="${deps} ${rg}"
					fi
				fi

				# Next
				break
			fi
		done
	done

	# Done with rgstate file
	clsetup_rm_rgstatefile ${rgstatefile}

	# If there are any, print them
	if [[ -n "${deps}" ]]; then
		echo ${deps}
	fi

	return 0
}

#####################################################
#
# clsetup_is_resource_enabled() rsname node
#
#	Test to see if "rsname" is enabled.
#
#
#	If nodename is not null, then the resource state is
#	tested only for that node.
#	If nodename is null, then the resource state is
#	tested for the entire cluster.
#
#	Return values:
#
#		2	Yes, but partially. (used only when nodename is null)
#		1	Yes, it is.
#		0	No,  it is not.
#		-1	Error, not set.
#
##################################################################################
clsetup_is_resource_enabled()
{
	typeset rsname=${1}
	typeset nodename=${2}
	typeset rgname
	typeset nodelist
	typeset node

	let enabled_count=0
	let total_rs_count=0

	typeset -l state		# lower case

	# If not a cluster member, return not enabled
	if [[ ${CLSETUP_ISMEMBER} -ne 1 ]];  then
		return 0
	fi

	if [[ -n "${nodename}" ]]; then
		state=$(${SCHA_RS_GET} -R ${rsname} -O on_off_switch_node ${nodename})
		if [[ "${state}" == "enabled" ]]; then
			return 1
		elif [[ "${state}" == "disabled" ]]; then
			return 0
		else
			return -1
		fi
	fi

	rgname="$(${SCHA_RS_GET} -O Group -R "${rsname}")"
	nodelist="$(${SCHA_RG_GET} -G ${rgname} -O nodelist)"

	if [[ -z ${nodelist} ]]; then
		# Return Disabled if nodelist is empty.
		return 0
	fi

	for node in ${nodelist}
	do
		state=$(${SCHA_RS_GET} -R ${rsname} -O on_off_switch_node ${node})
		if [[ "${state}" == "enabled" ]]; then
			((enabled_count += 1))
		fi
		if [[ "${state}" != "enabled" ]] && [[ "${state}" != "disabled" ]]; then
			return -1
		fi
		((total_rs_count += 1))
	done

	if [[ ${enabled_count} -eq 0 ]]; then
		return 0
	elif [[ ${enabled_count} -eq ${total_rs_count} ]]; then
		return 1
	else
		return 2
	fi
}

#####################################################
#
# clsetup_enabled_resources() resource ...
#
#	Test to see if any "resources" in in the resource list
#	are enabled.   Print back the ones that are.
#
#	This function always returns zero.
#
#####################################################
clsetup_get_enabled_resources()
{
	typeset resource

	# If not a cluster member, return with nothing enabled
	if [[ ${CLSETUP_ISMEMBER} -ne 1 ]];  then
		return 0
	fi

	for resource in $*
	do
		clsetup_is_resource_enabled ${resource} ""
		# Return value:
		# 	0 - the resource is disabled on all the nodes.
		# 	1 - the resource is enabled on all the nodes.
		# 	2 - the resource is enabled on some of the nodes and disabled
		if [[ $? -eq 1 ]]; then
			echo ${resource}
		fi
	done

	return 0
}

#####################################################
#
# clsetup_is_resource_stop_failed() rsname
#
#	Test to see if "rsname" is is in the STOP_FAILED state
#	on any of the nodes.  If it is, a list of the nodes is
#	printed.
#
#	Return values:
#
#		1	Yes, it is.
#		0	No,  it is not.
#		-1	Error.
#
#####################################################
clsetup_is_resource_stop_failed()
{
	typeset rsname=${1}

	typeset rgname
	typeset nodelist
	typeset node
	typeset -l state		# lower case

	integer rstatus

	# If not a cluster member, return not enabled
	if [[ ${CLSETUP_ISMEMBER} -ne 1 ]];  then
		return 0
	fi

	# Get the nodelist for this resource
	rgname=$(${SCHA_RS_GET} -R ${rsname} -O group)
	nodelist="$(${SCHA_RG_GET} -G ${rgname} -O nodelist)"

	# Check for STOP_FAILED for each node
	let rstatus=0
	for node in ${nodelist}
	do
		state=$(${SCHA_RS_GET} -R ${rsname} -O resource_state_node ${node})
		if [[ "${state}" == "stop_failed" ]]; then
			echo ${node}
			let rstatus=1
		fi
	done

	return ${rstatus}
}

#####################################################
#
# clsetup_is_network_rstype() rstype
#
#	Test to see if this is a network resource type.
#
#	Return values:
#
#		1	Yes, it is.
#		0	No,  it is not.
#
#####################################################
clsetup_is_network_rstype()
{
	typeset rstype=${1}

	if [[ ${rstype} = SUNW.LogicalHostname ]] ||
	    [[ ${rstype} = SUNW.SharedAddress ]] ||
	    [[ ${rstype} = SUNW.LogicalHostname:* ]] ||
	    [[ ${rstype} = SUNW.SharedAddress:* ]] ||
	    [[ ${rstype} = LogicalHostname ]] ||
	    [[ ${rstype} = SharedAddress ]] ||
	    [[ ${rstype} = LogicalHostname:* ]] ||
	    [[ ${rstype} = SharedAddress:* ]]; then
		return 1
	fi

	return 0
}

#####################################################
#
# clsetup_is_system_rstype() rstype
#
#       Test to see of this is a system resource type.
#
#       Return values:
#
#               1       Yes, it is.
#               0       No, it is not.
#
#####################################################
clsetup_is_system_rstype()
{
        typeset rstype=${1}
        typeset cachefile=${2}

        typeset systemrt

        if [[ -n "${cachefile}" ]] && [[ -r "${cachefile}" ]]; then
                systemrt="$(sed -n 's/^ *('${rstype}') Res Type system:[  ]*\(.*\)/\1/p' ${cachefile})"
        else
                systemrt="$(${SCHA_RT_GET} -T ${rstype} -O RT_SYSTEM)"
        fi

        if [[ "${systemrt}" = "TRUE" ]]
        then
                return 1
        else
                return 0
        fi
}

#####################################################
#####################################################
##
## Main help text
##
#####################################################
#####################################################

#####################################################
#
# clsetup_help_main_quorum()
#
#	Print the main help message for quorum.
#
#	This function always returns zero.
#
#####################################################
clsetup_help_main_quorum()
{
	typeset sctxt_title="$(gettext '
		*** Help Screen - Quorum ***
	')"
	typeset sctxt_p1="$(gettext '
		A quorum device is a device connected to at least two
		nodes and configured to participate in computing the
		quorum for the cluster. Both nodes and quorum devices
		contribute votes to the quorum.  A simple majority of
		votes must be present in order to establish quorum
		and form a cluster.
	')"
	typeset sctxt_p2="$(gettext '
		Each cluster node has a default quorum vote count of
		one. If configured, each quorum device has a quorum
		vote count of N-1, where N is the number of
		disk-to-node paths configured for the device. For
		example, a quorum device consisting of a disk with two
		disk-to-node paths has a quorum vote count of 1.
	')"
	typeset sctxt_p3="$(gettext '
		Both nodes and quorum devices may be placed
		in a quorum maintenance state.  This is achieved by
		setting their vote counts to zero.  Resetting this
		maintenance state restores vote counts to their default
		values.  Please refer to the clquorum(1M) man page and
		other Sun Cluster documentation for information
		on changing the quorum maintenance state.
	')"
	typeset sctxt_p4="$(gettext '
		The minimum requirement for configuring quorum is
		that all two-node clusters have at least one quorum device.
		For more information regarding disk layout, quorum topologies,
		the types of quorum devices, and recommendations for
		configuring quorum, please refer to the Sun Cluster
		documentation.
	')"

	sc_print_title "${sctxt_title}"
	sc_print_para "${sctxt_p1}"
	sc_print_para "${sctxt_p2}"
	sc_print_para "${sctxt_p3}"
	sc_print_para "${sctxt_p4}"

	return 0
}

#####################################################
#
# clsetup_help_main_rgm()
#
#	Print the main help message for resource groups.
#
#	This function always returns zero.
#
#####################################################
clsetup_help_main_rgm()
{
	typeset sctxt_title="$(gettext '
		*** Help Screen - Resource Groups ***
	')"
	typeset sctxt_p1="$(gettext '
		The cluster uses resource groups to manage data service
		and network resources.   A resource group is a
		container into which resources of various types may be
		placed.   There are different types of resources,
		including the two network resources types and any
		number of data service resource types.   However, there
		are only two types of resource groups, failover and
		scalable.
	')"
	typeset sctxt_p2="$(gettext '
		The two types of network resources are LogicalHostname
		resources and SharedAddress resources.   Both of these
		network resource types are pre-registered with the
		cluster.  Each data service is also associated with its
		own data service resource type.  Data service resource
		types are not pre-registered and must be registered
		with the cluster after they have been installed.
	')"
	typeset sctxt_p3="$(gettext '
		A failover resource group typically contains one or
		more network resource type resources.  Usually, there
		is at least one network resource for each subnet.  In
		addition, each failover resource group may contain one
		or more data service resource.
	')"
	typeset sctxt_p4="$(gettext '
		A scalable resource group never contains network resources,
		but always depends upon a failover resource group that
		includes at least one SharedAddress resource.
	')"

	(
		sc_print_title "${sctxt_title}"
		sc_print_para "${sctxt_p1}"
		sc_print_para "${sctxt_p2}"
		sc_print_para "${sctxt_p3}"
		sc_print_para "${sctxt_p4}"
	) | more

	return 0
}

#####################################################
#
# clsetup_help_main_transport()
#
#	Print the main help message for cluster interconnect.
#
#	This function always returns zero.
#
#####################################################
clsetup_help_main_transport()
{
	typeset sctxt_title="$(gettext '
		*** Help Screen - Cluster Interconnect ***
	')"
	typeset sctxt_p1="$(gettext '
		The cluster interconnect facilitates high speed private
		communications between nodes in the cluster.  One of
		the more important uses of the interconnect is the
		synchronization of cluster membership information.
	')"
	typeset sctxt_p2="$(gettext '
		There must be at least two points of connection to the
		cluster interconnect for each node in the cluster.
		Each such endpoint is connected to the interconnect by
		a cable.  In two-node clusters with interconnect
		hardware which supports direct-connect cabling, both
		endpoints of a transport cable may be directly attached
		to transport adapters.  But, all configurations support
		cabling with a cluster transport switch at one
		endpoint and a cluster transport adapter at the other.
	')"

	sc_print_title "${sctxt_title}"
	sc_print_para "${sctxt_p1}"
	sc_print_para "${sctxt_p2}"

	return 0
}

#####################################################
#
# clsetup_help_main_privatehosts()
#
#	Print the main help message for private hostnames.
#
#	This function always returns zero.
#
#####################################################
clsetup_help_main_privatehosts()
{
	typeset sctxt_title="$(gettext '
		*** Help Screen - Private Hostnames ***
	')"
	typeset sctxt_p1="$(gettext '
		Nodes may contact other nodes in the cluster
		over the cluster interconnect by accessing them using
		their private hostnames.   Default private hostnames
		are assigned during cluster installation.
	')"
	typeset sctxt_p2="$(gettext '
		Note that private hostnames should never be stored
		in the hosts(4) or other naming services databases.
		A special nsswitch facility (see nsswitch.conf(4))
		performs all hostname lookups for private hostnames.
	')"
	typeset sctxt_p3="$(gettext '
		Zones may contact other zones in the cluster
		over the cluster interconnect by accessing them using
		their private hostnames.   Default private hostnames
		are not assigned during cluster installation.
	')"

	sc_print_title "${sctxt_title}"
	sc_print_para "${sctxt_p1}"
	sc_print_para "${sctxt_p2}"
	if [[ ${ZONES_OK} -eq 1 ]]; then
		sc_print_para "${sctxt_p3}"
	fi

	return 0
}

#####################################################
#
# clsetup_help_main_devicegroups()
#
#	Print the main help message for device groups.
#
#	This function always returns zero.
#
#####################################################
clsetup_help_main_devicegroups()
{
	typeset sctxt_title="$(gettext '
		*** Help Screen - Device Groups ***
	')"
	typeset sctxt_p1="$(gettext '
		A device group is a group of devices which can
		be mastered as a single set.  The most common
		types of device groups are disk device groups.
	')"
	typeset sctxt_p2_sds="$(gettext '
		Every Solstice DiskSuite diskset is automatically
		registered as a Sun Cluster device group.   VxVM shared
		disk groups should also be registered as Sun Cluster
		device groups, but it is not done automatically.  Disks
		which do not belong to either SDS disksets or VxVM disk
		groups may be grouped together into their own
		Sun Cluster rawdisk device groups.
	')"
	typeset sctxt_p2_svm="$(gettext '
		Every Solaris Volume Manager diskset is automatically
		registered as a Sun Cluster device group.   VxVM shared
		disk groups should also be registered as Sun Cluster
		device groups, but it is not done automatically.  Disks
		which do not belong to either Solaris Volume Manager
		disksets or VxVM disk groups may be grouped together
		into their own Sun Cluster rawdisk device groups.
	')"
	typeset sctxt_p3_sds="$(gettext '
		Solstice DiskSuite is Sun Cluster aware,
		and most device group administration occurs
		automatically through the Solstice DiskSuite command
		set or GUI interface for DiskSuite disksets.
		However, since VxVM is not Sun Cluster aware, VxVM disk
		groups and volumes must each be registered with the
		cluster using either clsetup(1M) or cldevicegroup(1M).
	')"
	typeset sctxt_p3_svm="$(gettext '
		Solaris Volume Manager is Sun Cluster aware,
		and most device group administration occurs
		automatically through the Solaris Volume Manager command
		set or GUI interface for disksets.
		However, since VxVM is not Sun Cluster aware, VxVM disk
		groups and volumes must each be registered with the
		cluster using either clsetup(1M) or cldevicegroup(1M).
	')"

	sc_print_title "${sctxt_title}"
	sc_print_para "${sctxt_p1}"
	if [[ "${SC_SUNOS}" == "5.8" ]]; then
		sc_print_para "${sctxt_p2_sds}"
		sc_print_para "${sctxt_p3_sds}"
	else
		sc_print_para "${sctxt_p2_svm}"
		sc_print_para "${sctxt_p3_svm}"
	fi

	return 0
}

#####################################################
#
# clsetup_help_main_auth()
#
#	Print the main help message for managing authorization
#	for nodes which want to add themselves to the cluster.
#
#	This function always returns zero.
#
#####################################################
clsetup_help_main_auth()
{
	typeset sctxt_title="$(gettext '
		*** Help Screen - New Nodes ***
	')"
	typeset sctxt_p1="$(gettext '
		To be added as a cluster node, a machine must appear in
		the list of hosts that are permitted to install themselves
		into cluster configuration.  If this list is empty, any
		machine may add itself to the cluster configuration.
	')"
	typeset sctxt_p2="$(gettext '
		The identity of machines contacting the cluster over
		the public network for installation purposes may be
		confirmed using either standard UNIX or the more secure
		Diffie-Hellman, or DES, authentication.  However, DES
		authentication is not usually considered necessary for
		this purpose, since machines which are not physically
		connected to the private cluster interconnect will
		never be able to actually join the cluster membership
		anyway.
	')"
	typeset sctxt_p3="$(gettext '
		If DES authentication is selected, you must also configure
		all necessary encryption keys before a node can join
		(see keyserv(1M), publickey(4)).
	')"

	sc_print_title "${sctxt_title}"
	sc_print_para "${sctxt_p1}"
	sc_print_para "${sctxt_p2}"
	sc_print_para "${sctxt_p3}"

	return 0
}

#####################################################
#
# clsetup_help_noncluster_mode_ip_address_range()
#
#	Print the main help message for other cluster tasks.
#
#	This function always returns zero.
#
#####################################################
clsetup_help_noncluster_mode_ip_address_range()
{
	typeset sctxt_title="$(gettext '
		*** Help Screen - Network Addressing and Ranges for the Cluster Transport ***
	')"
	typeset sctxt_p1="$(gettext '
		The cluster transport uses a default network
		address of 172.16.0.0. The default netmask used is
		255.255.240.0. You can choose to use a different
		network address. You can also choose to use a different
		netmask that is computed by Sun Cluster, based on the
		number of nodes and private networks that you
		expect to configure in the cluster. The given netmask
		must minimally mask all the bits in the given network
		address and must accommodate the existing cluster
		configuration.
	')"

	sc_print_title "${sctxt_title}"
	sc_print_para "${sctxt_p1}"

	return 0
}

#####################################################
#
# clsetup_help_main_other()
#
#	Print the main help message for other cluster tasks.
#
#	This function always returns zero.
#
#####################################################
clsetup_help_main_other()
{
	typeset sctxt_title="$(gettext '
		*** Help Screen - Other Cluster Tasks ***
	')"
	typeset sctxt_p1="$(gettext '
		To change the name of the cluster, select \"Change
		the name of the cluster.\"
	')"
	typeset sctxt_p2="$(gettext '
		To enable the collection of data about the cluster
		and the generation of graphs in Sun Cluster Manager,
		select \"Configure telemetry.\"
	')"

	sc_print_title "${sctxt_title}"
	sc_print_para "${sctxt_p1}"
	sc_print_para "${sctxt_p2}"

	return 0
}

#####################################################
#####################################################
##
## Quorum
##
#####################################################
#####################################################

#####################################################
#
# clsetup_menu_quorum_adddev() [nohelp]
#
#	nohelp		- if this flag is given, do not
#			    clear screen, print title, print help,
#			    or ask for confirmation to continue
#
#	Add a quorum device to the cluster.
#
#	Note that the clsetup_initcluster() calls this function with "nohelp".
#	If it has it's own help information to print, it should do so
#	before calling this function.
#
#	Return:
#		zero		Completed
#		non-zero	Not completed
#
#####################################################
clsetup_menu_quorum_adddev()
{
	typeset nohelp=${1}

	typeset sctxt_title="$(gettext '
		>>> Add a Quorum Device <<<
	')"
	typeset sctxt_p1="$(gettext '
		This option is used to add a quorum device to the cluster
		configuration.  Quorum devices are necessary to protect the
		cluster from split brain and amnesia situations.  Each
		quorum device must be connected to at least two nodes.
		You can use a device containing user data.
	')"
	typeset sctxt_p2="$(gettext '
		Adding a quorum device automatically configures
		node-to-device paths for the nodes attached to the device.
		Later, if you add more nodes to the cluster, you might
		need to update these paths by removing then adding back
		the quorum device.  For more information on supported
		quorum device topologies, see the Sun Cluster documentation.
	')"

	typeset dev_type
	typeset sctxt_option_001="$(gettext 'Directly attached shared disk')"
	typeset sctxt_option_002="$(gettext 'Network Attached Storage (NAS) from Network Appliance')"
	typeset sctxt_option_003="$(gettext 'Quorum Server')"
	typeset sctxt_option_return="$(gettext 'Return to the quorum menu')"

	# Unless nohelp is specified, print help and get confirmation
	if [[ -z "${nohelp}" ]]; then

		# Clear screen, print title and help
		sc_print_title "${sctxt_title}"
		sc_print_para "${sctxt_p1}"
		sc_print_para "${sctxt_p2}"

		# Okay to continue?
		clsetup_continue || return 1
	fi

	# Get the device type
	prompt="$(gettext 'What is the type of device you want to use?')"
	dev_type="$(
		sc_get_menuoption	\
		    "T2+++${prompt}" \
		    "S+0+1+${sctxt_option_001}" \
		    "S+0+2+${sctxt_option_002}" \
		    "S+0+3+${sctxt_option_003}" \
		    "R+++" \
		    "S+0+q+${sctxt_option_return}" \
	)"

	if [[ -z "${dev_type}" ]]; then
		return 1
	fi

	if [[ ${dev_type} == "1" ]]; then
		# The shared_disk quorum menu
		clsetup_menu_quorum_add_shared_disk
		return $?
	elif [[ ${dev_type} == "2" ]]; then
		# The netapp_net menu
		clsetup_menu_quorum_add_netappnas
		return $?
	elif [[ ${dev_type} == "3" ]]; then
		# The quorum_server menu
		clsetup_menu_quorum_add_qs
		return $?
	fi

	return 0
}

#####################################################
#
# clsetup_menu_quorum_add_shared_disk() [nohelp]
#
#	nohelp		if this flag is given, do not
#			clear screen, print title, print help,
#			or ask for confirmation to continue
#
#	Add a shared_disk disk quorum device to the cluster.
#
#	Return:
#		zero		Completed
#		none-zero	Not complete
#
#####################################################
clsetup_menu_quorum_add_shared_disk()
{
	typeset nohelp=${1}

	typeset sctxt_title="$(gettext '
		>>> Add a Shared Disk Quorum Device <<<
	')"

	typeset sctxt_p1="$(gettext '
		If you are using a dual-ported disk, by default, Sun
		Cluster uses SCSI-2. If you are using disks that are
		connected to more than two nodes, or if you manually
		override the protocol from SCSI-2 to SCSI-3, by default,
		Sun Cluster uses SCSI-3.
	')"
	typeset sctxt_p2="$(gettext '
		If you turn off SCSI fencing for disks, Sun Cluster
		uses software quorum, which is Sun Cluster software
		that emulates a form of SCSI Persistent Group
		Reservations (PGR).
	')"

	typeset sctxt_p3="$(gettext '
		Warning: If you are using disks that do not support
		SCSI, such as Serial Advanced Technology Attachment
		(SATA) disks, turn off SCSI fencing.
	')"

	typeset sctxt_p4="$(gettext '
		For more information about supported quorum device
		topologies, see the Sun Cluster documentation.
	')"

	typeset answer
	typeset globaldev

	# Unless nohelp is specified, print help and get confirmation
	if [[ -z "${nohelp}" ]]; then

		# Clear screen, print title and help
		sc_print_title "${sctxt_title}"
		sc_print_para "${sctxt_p1}"
		sc_print_para "${sctxt_p2}"
		sc_print_para "${sctxt_p3}"
		sc_print_para "${sctxt_p4}"

		# Okay to continue?
		clsetup_continue || return 1
	fi

	# Loop until done or Ctrl-D is typed
	while true
	do
		# Get the name of the global device
		answer=$(sc_prompt "$(gettext 'Which global device do you want to use (d<N>)?')") || return 1

		# Make sure they use the d<N> format
		if [[ $(expr "${answer}" : 'd[0-9][0-9]*') -ne ${#answer} ]]; then
			printf "$(gettext 'The global device name must be given as d<N>.')\n"
			if [[ "${answer}" = /* ]]; then
				printf "$(gettext 'It is not necessary to use the full pathname.')\n"
			fi
			printf "\n\a"

			# Retry from the top
			continue
		fi

		# Set the global device name
		globaldev=${answer}

		#
		# Set up the command set
		#

		# Initialize
		set -A CLSETUP_DO_CMDSET

		# Add the command
		CLSETUP_DO_CMDSET[0]="${CL_CMD_CLQUORUM} add ${globaldev}"

		# Attempt to issue the command
		clsetup_do_cmdset || return 1

		# Do you want to add any quorum devices?
		answer=$(sc_prompt_yesno "$(gettext 'Do you want to add another quorum device?')" "${YES}") || return 1
		if [[ "${answer}" = "no" ]]; then
			# Done
			break
		fi

	done

	return 0
}

#####################################################
#
# clsetup_menu_quorum_add_netappnas() [nohelp]
#
#	nohelp		if this flag is given, do not
#			clear screen, print title, print help,
#			or ask for confirmation to continue
#
#	Add a Netapp NAS quorum device to the cluster.
#
#	Return:
#		zero		Completed
#		none-zero	Not complete
#
#####################################################
clsetup_menu_quorum_add_netappnas()
{
	typeset nohelp=${1}

	typeset sctxt_title="$(gettext '
		>>> Add a Netapp NAS Quorum Device <<<
	')"

	typeset sctxt_p1="$(gettext '
		A Netapp NAS device can be configured as a quorum
		device for Sun Cluster.  The NAS configuration
		data includes a device name, which is given by the
		user and must be unique across all quorum devices, the
		filer name, and a LUN id, which defaults to 0
		if not specified.  Please refer to the clquorum(1M) man
		page and other Sun Cluster documentation for details.
	')"

	typeset sctxt_p2="$(gettext '
		The NAS quorum device must be setup before
		configuring it with Sun Cluster. For more
		information on setting up Netapp NAS filer,
		creating the device, and installing the license
		and the Netapp binaries, see the Sun Cluster
		documentation.
	')"

	typeset qd_name
	typeset filer_name
	typeset lun_id
	typeset answer

	# Unless nohelp is specified, print help and get confirmation
	if [[ -z "${nohelp}" ]]; then

		# Clear screen, print title and help
		sc_print_title "${sctxt_title}"
		sc_print_para "${sctxt_p1}"
		sc_print_para "${sctxt_p2}"

		# Okay to continue?
		clsetup_continue || return 1
	fi

	# Loop until done or Ctrl-D is typed
	while true
	do
		# get the NAS quorum name
		answer=$(sc_prompt "$(gettext 'What name do you want to use for this quorum device?')") || return 1

		# The name has no special characters
		if [[ $(expr "${answer}" : '[a-z_A-Z0-9]*') -ne ${#answer} ]]; then
			printf "$(gettext 'Invalid characters in naming. ')\n\n\a"
			continue
		fi

		# Set the quorum name
		qd_name=${answer}

		# Get the filer name
		while true
		do
			answer=$(sc_prompt "$(gettext 'What is the name of the filer?')" "${qd_name}") || return 1

			# Set the quorum name
			filer_name=${answer}
			break
		done

		# Get the LUN id
		while true
		do
			answer=$(sc_prompt "$(gettext 'What is the LUN id on the filer?')" "0") || return 1

			if [[ $(expr "${answer}" : '[0-9]*') -ne ${#answer} ]]; then
				printf "$(gettext 'The LUN id must be digits.')\n\n\a"
				continue
			fi

			# Set the quorum name
			lun_id=${answer}
			break
		done

		#
		# Set up the command set
		#

		# Initialize
		set -A CLSETUP_DO_CMDSET

		# Add the command
		CLSETUP_DO_CMDSET[0]="${CL_CMD_CLQUORUM} add -t netapp_nas -p filer=${filer_name} -p lun_id=${lun_id} ${qd_name}"

		# Attempt to issue the command
		clsetup_do_cmdset || return 1

		# Do you want to add any quorum devices?
		answer=$(sc_prompt_yesno "$(gettext 'Do you want to add another quorum device?')" "${YES}") || return 1
		if [[ "${answer}" = "no" ]]; then
			# Done
			break
		fi
	done

	return 0
}

#####################################################
#
# clsetup_menu_quorum_add_qs() [nohelp]
#
#	nohelp		if this flag is given, do not
#			clear screen, print title, print help,
#			or ask for confirmation to continue
#
#	Add a quorum server quorum device to the cluster.
#
#	Return:
#		zero		Completed
#		none-zero	Not complete
#
#####################################################
clsetup_menu_quorum_add_qs()
{
	typeset nohelp=${1}

	typeset sctxt_title="$(gettext '
		>>> Add a Quorum Server Quorum Device <<<
	')"

	typeset sctxt_p1="$(gettext '
		A Quorum Server process runs on a machine outside
		Sun Cluster and serves the cluster as a quorum
		device. Before configuring the quorum server as
		a quorum device into the cluster, you will need
		to setup the quorum server machine and start
		the quorum server process. For detailed information
		on setting up a quorum server, refer to Sun
		Cluster system administration guide.
	')"


	typeset sctxt_p2="$(gettext '
		You will need to specify a device name for the quorum
		server quorum device, which must be unique across all
		quorum devices, the IP address of the quorum server
		machine, or hostname if the machine is added into
		/etc/hosts, and a port number on the quorum server machine
		used to communicate with the cluster nodes.
		Please refer to the clquorum(1M) man page and other Sun
		Cluster documentation for details.
	')"

	typeset qd_name
	typeset host_name
	typeset port_num
	typeset answer

	# Unless nohelp is specified, print help and get confirmation
	if [[ -z "${nohelp}" ]]; then

		# Clear screen, print title and help
		sc_print_title "${sctxt_title}"
		sc_print_para "${sctxt_p1}"
		sc_print_para "${sctxt_p2}"

		# Okay to continue?
		clsetup_continue || return 1
	fi

	# Loop until done or Ctrl-D is typed
	while true
	do
		# get the NAS quorum name
		answer=$(sc_prompt "$(gettext 'What name do you want to use for this quorum device?')") || return 1

		# The name has no special characters
		if [[ $(expr "${answer}" : '[a-z_A-Z0-9]*') -ne ${#answer} ]]; then
			printf "$(gettext 'Invalid characters in naming. ')\n\n\a"
			continue
		fi

		# Set the quorum name
		qd_name=${answer}

		# Get the quorum server host name
		while true
		do
			answer=$(sc_prompt "$(gettext 'What is the IP address of the quorum server machine?')") || return 1

			# Set the quorum name
			host_name=${answer}
			break
		done

		# Get the Port number
		while true
		do
			answer=$(sc_prompt "$(gettext 'What is the port number on the quorum server machine?')") || return 1

			if [[ $(expr "${answer}" : '[0-9]*') -ne ${#answer} ]]; then
				printf "$(gettext 'The port number must be digits.')\n\n\a"
				continue
			fi

			if [[ "${answer}" = "0" ]]; then
				printf "$(gettext 'The port number cannot be \"0\".')\n\n\a"
				continue
			fi

			# Set the quorum name
			port_num=${answer}
			break
		done

		#
		# Set up the command set
		#

		# Initialize
		set -A CLSETUP_DO_CMDSET

		# Add the command
		CLSETUP_DO_CMDSET[0]="${CL_CMD_CLQUORUM} add -t quorum_server -p qshost=${host_name} -p port=${port_num} ${qd_name}"

		# Attempt to issue the command
		clsetup_do_cmdset || return 1

		# Do you want to add any quorum devices?
		answer=$(sc_prompt_yesno "$(gettext 'Do you want to add another quorum device?')" "${YES}") || return 1
		if [[ "${answer}" = "no" ]]; then
			# Done
			break
		fi
	done

	return 0
}

#####################################################
#
# clsetup_menu_quorum_rmdev() [nohelp]
#
#	nohelp		- if this flag is given, do not
#			    clear screen, print title, print help,
#			    or ask for confirmation to continue
#
#	Remove a quorum device from the cluster.
#
#	Return:
#		zero		Completed
#		non-zero	Not completed
#
#####################################################
clsetup_menu_quorum_rmdev()
{
	typeset nohelp=${1}

	typeset sctxt_title="$(gettext '
		>>> Remove a Quorum Device <<<
	')"
	typeset sctxt_p1="$(gettext '
		This option is used to remove a quorum device from the
		cluster configuration.  When a quorum device is removed,
		it no longer participates in the voting to establish
		quorum.
	')"
	typeset sctxt_p2="$(gettext '
		Note that all two-node clusters require that at least
		one quorum device be configured.  So, if this is
		the last quorum device on a two-node cluster, clquorum(1M)
		will fail to remove the device from the configuration.
	')"

	typeset answer
	typeset globaldev

	# Unless nohelp is specified, print help and get confirmation
	if [[ -z "${nohelp}" ]]; then

		# Clear screen, print title and help
		sc_print_title "${sctxt_title}"
		sc_print_para "${sctxt_p1}"
		sc_print_para "${sctxt_p2}"

		# Okay to continue?
		clsetup_continue || return 1
	fi

	# Loop until done or Ctrl-D is typed
	while true
	do
		# Get the name of the global device
		answer=$(sc_prompt "$(gettext 'Which quorum device do you want to remove?')") || return 1

		if [[ -z "${answer}" ]]; then
			printf "$(gettext 'A quorum device name must be given.')\n.')\n\n\a"
			# Retry from the top
			continue
		fi

		# Set the global device name
		globaldev=${answer}

		#
		# Set up the command set
		#

		# Initialize
		set -A CLSETUP_DO_CMDSET

		# Add the command
		CLSETUP_DO_CMDSET[0]="${CL_CMD_CLQUORUM} remove ${globaldev}"

		# Attempt to issue the command
		clsetup_do_cmdset || return 1

		# Done
		break
	done

	return 0
}

#####################################################
#
# clsetup_help_quorum_menu()
#
#	Print help information from the quorum menu.
#	This is the same help information as is printed
#	from the MAIN help menu.  But, we ask for confirmation
#	before returning to the previous menu.
#
#	This function always returns zero.
#
#####################################################
clsetup_help_quorum_menu()
{
	clear
	clsetup_help_main_quorum
	sc_prompt_pause

	return 0
}

#####################################################
#
# clsetup_get_quorum_menuoption()
#
#	Print the quorum menu, and return the selected option.
#
#	This function always returns zero.
#
#####################################################
clsetup_get_quorum_menuoption()
{
	typeset sctxt_title_1="$(gettext '*** Quorum Menu ***')"
	typeset sctxt_title_2="$(gettext 'Please select from one of the following options:')"
	typeset sctxt_option_001="$(gettext 'Add a quorum device')"
	typeset sctxt_option_002="$(gettext 'Remove a quorum device')"
	typeset sctxt_option_help="$(gettext 'Help')"
	typeset sctxt_option_return="$(gettext 'Return to the Main Menu')"

	typeset option

	option=$(sc_get_menuoption \
		"T1+++${sctxt_title_1}" \
		"T2+++${sctxt_title_2}" \
		"S+0+1+${sctxt_option_001}" \
		"S+0+2+${sctxt_option_002}" \
		"R+++" \
		"S+0+?+${sctxt_option_help}" \
		"S+0+q+${sctxt_option_return}" \
	)

	echo "${option}"

	return 0
}

#####################################################
#
# clsetup_menu_quorum()
#
#	Quorum menu
#
#	This function always returns zero.
#
#####################################################
clsetup_menu_quorum()
{
	# Loop around the quorum menu
	while true
	do
		case $(clsetup_get_quorum_menuoption) in
		'1')    clsetup_menu_quorum_adddev ;;
		'2')    clsetup_menu_quorum_rmdev ;;
		'?')    clsetup_help_quorum_menu ;;
		'q')    break ;;
                esac
	done

	return 0
}

#####################################################
#
# clsetup_menu_print_ip_address_range()
#
#	Print the current IP address range menu
#
#	This function always returns zero.
#
#####################################################
clsetup_menu_print_ip_address_range()
{
	typeset sctxt_title="$(gettext '
		>>> Network Addressing and Ranges for the Cluster Transport <<<
	')"

	typeset sctxt_p1="$(gettext '
		Network addressing for the cluster transport is
		configured as follows:
	')"

	# Clear the screen and print the title and help text
	sc_print_title "${sctxt_title}"
	sc_print_para "${sctxt_p1}"

	# Print the existing IP address range
	${CL_CMD_CLUSTER} show-netprops
        echo

	sc_print_prompt "$(gettext 'Type ENTER to return to the menu:')" || return 1
	read
	echo

	return 0
}

#####################################################
#
# clsetup_menu_ip_address_range()
#
#	IP address range menu
#
#	This function always returns zero.
#
#####################################################
clsetup_menu_ip_address_range()
{
	typeset sctxt_title="$(gettext '
		>>> Change Network Addressing and Ranges for the Cluster Transport <<<
	')"

	typeset sctxt_p1="$(gettext '
		Network addressing for the cluster transport is currently
		configured as follows:
	')"

	typeset sctxt_netaddr_p1="$(gettext '
		The default network address for the cluster transport is %s.
	')"
	sctxt_netaddr_p1="$(printf "${sctxt_netaddr_p1}" ${SC_PNET_DFLT_NETADDR})"
	typeset sctxt_netmask_p1="$(gettext '
		The default netmask for the cluster transport is %s.
	')"
	sctxt_netmask_p1="$(printf "${sctxt_netmask_p1}" ${SC_PNET_DFLT_NETMASK})"

	typeset sctxt_pnet_ranges_p1="$(gettext '
		Specify a netmask of %s to meet anticipated future
		requirements of %d cluster nodes and %d private networks.
	')"
	typeset sctxt_pnet_ranges_p2="$(gettext '
		To accommodate more growth, specify a netmask of %s
		to support up to %d cluster nodes and %d private
		networks.
	')"

	typeset sctxt_pnet_maxes_p1="$(gettext '
		The combination of private netmask and network address
		will dictate both the maximum number of nodes and
		private networks that can be supported by a cluster.
		Given your private network address, this program will
		generate a range of recommended private netmasks based on
		the maximum number of nodes and private networks that
		you anticipate for this cluster.
	')"
	typeset sctxt_pnet_maxes_p2="$(gettext '
		In specifying the anticipated number of maximum nodes
		and private networks for this cluster, it is important
		that you give serious consideration to future growth
		potential.   While both the private netmask and network
		address can be changed later, the tools for making such
		changes require that all nodes in the cluster be booted
		into noncluster mode.
	')"

	typeset dflt_maxnodes
	typeset dflt_maxprivatenets
	typeset dflt_zoneclusters
	typeset answer

	typeset nodes
	typeset adapters

	typeset netaddr
	integer netaddr_hex
	typeset netmask
	integer netmask_hex
	integer dflt_netmask_hex=$(sc_ipv4_dot_to_hex ${SC_PNET_DFLT_NETMASK})

	typeset maxnodes			# maximum nodes
	typeset maxprivnets			# maximum private nets
	typeset min_maxcounts_netmask		# min netmask based on counts
	integer min_maxcounts_netmask_hex

	integer double_maxnodes			# double maximum nodes
	integer double_maxprivnets		# double maximum private nets
	typeset double_maxcounts_netmask	# double mask based on counts
	integer double_maxcounts_netmask_hex

	integer min_netmask_hex			# min netmask to cover net addr
	integer min_dflt_netmask_hex		# min for default net addr

	integer current_node_count
	integer current_privnet_count
	integer current_zc_count                # Number of current zone clusters

	integer result

	# Clear the screen and print the title and help text
	sc_print_title "${sctxt_title}"
	sc_print_para "${sctxt_p1}"

	# Print the existing IP address range
	${CL_CMD_CLUSTER} show-netprops

	# Continue?
	answer=$(sc_prompt_yesno "$(gettext 'Do you want to change this configuration?')" "${YES}")
	if [[ "${answer}" != "yes" ]]; then
		return 0
	fi

	# Get our current node and private network counts
	current_node_count=$(sc_print_ccr_node_count)
	current_privnet_count=$(sc_print_ccr_privnet_count)
	current_zc_count=$(sc_print_ccr_zc_count)

	# Network address
	while true
	do
		# Use the default?
		sc_print_para "${sctxt_netaddr_p1}"
		answer=$(sc_prompt_yesno "$(gettext 'Do you want to use the default?')" "${YES}") || return 0
		if [[ "${answer}" == "yes" ]]; then
			netaddr=${SC_PNET_DFLT_NETADDR}
			let netaddr_hex=$(sc_ipv4_dot_to_hex ${netaddr})
		else
			#
			# Create a netmask for a user specified network
			# address which supports the minimum number of
			# nodes and private networks.
			#
			min_maxcounts_netmask=$(sc_netmask_generate_private ${current_node_count} ${current_privnet_count} ${current_zc_count})
			let min_netmask_hex=$(sc_ipv4_dot_to_hex ${min_maxcounts_netmask})

			#
			# Get a valid network address from the user.
			#
			while true
			do
				# Get the netaddr
				answer=$(sc_prompt "$(gettext 'What network address do you want to use?')") || return 0

				# Check it
				is_ipv4_dot_notation "${answer}"
				if [[ $? -ne 0 ]]; then
					printf "$(gettext '%s is an invalid network address.')\n" "${answer}"
					printf "$(gettext 'The network address must be provided in IPv4 dot notation.')\n\n\a"
					continue
				fi

				# Set the netaddr
				netaddr=${answer}

				# Set the netaddr_hex value
				let netaddr_hex=$(sc_ipv4_dot_to_hex ${netaddr})

				#
				# Make sure that the network address will
				# support the minimum number of nodes
				# and private networks.
				#
				if ((netaddr_hex & ~min_netmask_hex)); then
					printf "$(gettext 'This network address is too large.')\n"
					printf "$(gettext 'It must be able to support a minimum of %d nodes and %d private networks.')\n\n\a" ${current_node_count} ${current_privnet_count}
					continue
				fi

				# Done
				break
			done
		fi

		# Done
		break
	done

	# Set the minimum required netmask for this netaddr
	let min_netmask_hex=$(sc_netmask_min ${netaddr_hex})

	# Set the minimum required netmask for the default netmask
	let min_dflt_netmask_hex=$(sc_netmask_min ${dflt_netmask_hex})

	# Set defaults for prompts
	min_maxcounts_netmask=$(sc_netmask_generate_private ${SC_PNET_DFLT_MAXNODES} ${SC_PNET_DFLT_MAXPRIVATENETS} ${SC_PNET_DFLT_VIRTUALCLUSTERS})
	let min_maxcounts_netmask_hex=$(sc_ipv4_dot_to_hex ${min_maxcounts_netmask})
	if [[ ${min_maxcounts_netmask_hex} -lt ${min_netmask_hex} ]]; then
		dflt_maxnodes=${current_node_count}
		dflt_maxprivatenets=${current_privnet_count}
		dflt_zoneclusters=${current_zc_count}
	else
		dflt_maxnodes=${SC_PNET_DFLT_MAXNODES}
		dflt_maxprivatenets=${SC_PNET_DFLT_MAXPRIVATENETS}
		dflt_zoneclusters=${SC_PNET_DFLT_VIRTUALCLUSTERS}
	fi

	# Netmask
	while true
	do
		# Initialize
		netmask=
		maxnodes=
		maxprivnets=
		zoneclusters=

		# If the default netmask is okay, should we use it?
		if [[ ${min_netmask_hex} -le ${min_dflt_netmask_hex} ]]; then

			# Use the default?
			sc_print_para "${sctxt_netmask_p1}"
			answer=$(sc_prompt_yesno "$(gettext 'Do you want to use the default?')" "${YES}") || return 0
			if [[ "${answer}" == "yes" ]]; then
				netmask=${SC_PNET_DFLT_NETMASK}
				maxnodes=${SC_PNET_DFLT_MAXNODES}
				maxprivnets=${SC_PNET_DFLT_MAXPRIVATENETS}
				zoneclusters=${SC_PNET_DFLT_VIRTUALCLUSTERS}
			fi
		fi

		# If we didn't use the default, ask the user
		if [[ -z "${netmask}" ]]; then
			(
				sc_print_para "${sctxt_pnet_maxes_p1}"
				sc_print_para "${sctxt_pnet_maxes_p2}"
			) | more

			# Get the max nodes expected
			while true
			do
				answer=$(sc_prompt "$(gettext 'Maximum number of nodes anticipated for future growth?')" "${dflt_maxnodes}") || return 0
				is_numeric ${answer}
				if [[ $? -ne 0 ]]; then
					printf "$(gettext '%s is invalid.')\n\n\a" "${answer}"
					continue
				fi

				# Make sure that this is in range
				min_maxcounts_netmask=$(sc_netmask_generate_private ${answer} ${current_privnet_count} ${current_zc_count})
				let min_maxcounts_netmask_hex=$(sc_ipv4_dot_to_hex ${min_maxcounts_netmask})
				if [[ ${min_maxcounts_netmask_hex} -lt ${min_netmask_hex} ]]; then
					printf "$(gettext 'The network adddress that you selected (%s) will not support this many nodes.')\n\n\a" "${netaddr}"
					continue
				fi

				# Make sure the node count is not too small
				if [[ ${answer} -lt ${current_node_count} ]];  then
					printf "$(gettext 'The number of nodes must be at least %d.')\n\n\a" "${current_node_count}"
					continue
				fi
				# Make sure the node count is not too large
				if [[ ${answer} -gt ${SC_PNET_MAX_MAXNODES} ]];
 then
					printf "$(gettext 'The number of nodes cannot be greater than %d.')\n\n\a" "${SC_PNET_MAX_MAXNODES}"
					continue
				fi

				# Done
				break
			done
			maxnodes=${answer}

			# Get the private networks expected
			while true
			do
				answer=$(sc_prompt "$(gettext 'Maximum number of private networks anticipated for future growth?')" "${dflt_maxprivatenets}") || return 0
				is_numeric ${answer}
				if [[ $? -ne 0 ]]; then
					printf "$(gettext '%s is invalid.')\n\n\a" "${answer}"
					continue
				fi

				# Make sure that this is in range
				min_maxcounts_netmask=$(sc_netmask_generate_private ${maxnodes} ${answer} ${current_zc_count})
				let min_maxcounts_netmask_hex=$(sc_ipv4_dot_to_hex ${min_maxcounts_netmask})
				if [[ ${min_maxcounts_netmask_hex} -lt ${min_netmask_hex} ]]; then
					printf "$(gettext 'The network adddress that you selected (%s) will not support this many nodes and private networks.')\n\n\a" "${netaddr}"
					continue
				fi

				# Make sure the net count is not too small
				if [[ ${answer} -lt ${current_privnet_count} ]];  then
					printf "$(gettext 'The number of private networks must be at least %d.')\n\n\a" "${current_privnet_count}"
					continue
				fi

				# Make sure the net count is not too large
				if [[ ${answer} -gt ${SC_PNET_MAX_MAXPRIVATENETS} ]];  then
					printf "$(gettext 'The number of private networks cannot be greater than %d.')\n\n\a" "${SC_PNET_MAX_MAXPRIVATENETS}"
					continue
				fi

				# Done
				break
			done
			maxprivnets=${answer}

			# Print message regarding using these ranges
			sctxt_pnet_ranges_p1="$(printf "${sctxt_pnet_ranges_p1}" "${min_maxcounts_netmask}" "${maxnodes}" "${maxprivnets}")"
			sc_print_para "${sctxt_pnet_ranges_p1}"

			#
			# Calculate a larger recommended netmask
			# based on double the number of "maxnodes"
			# and "maxprivnets".
			#
			let double_maxnodes=$((maxnodes * 2))
			if [[ ${double_maxnodes} -gt ${SC_PNET_MAX_MAXNODES} ]]; then
				let double_maxnodes=${SC_PNET_MAX_MAXNODES}
			fi
			let double_maxprivnets=$((maxprivnets * 2))
			if [[ ${double_maxprivnets} -gt ${SC_PNET_MAX_MAXPRIVATENETS} ]]; then
				let double_maxprivnets=${SC_PNET_MAX_MAXPRIVATENETS}
			fi
			double_maxcounts_netmask=$(sc_netmask_generate_private ${double_maxnodes} ${double_maxprivnets} ${current_zc_count})

			# If we can use the larger one, print a second message
			let double_maxcounts_netmask_hex=$(sc_ipv4_dot_to_hex ${double_maxcounts_netmask})
			if [[ ${double_maxcounts_netmask_hex} -ge ${min_netmask_hex} ]]; then
				sctxt_pnet_ranges_p2="$(printf "${sctxt_pnet_ranges_p2}" "${double_maxcounts_netmask}" "${double_maxnodes}" "${double_maxprivnets}")"
				sc_print_para "${sctxt_pnet_ranges_p2}"
			fi

			# Get the netmask
			while true
			do
				answer=$(sc_prompt "$(gettext 'What netmask do you want to use?')" "${min_maxcounts_netmask}")
if 				[[ -z "${answer}" ]]; then
					echo
					continue 2
				fi

				# Check it
				is_ipv4_dot_notation "${answer}"
				if [[ $? -ne 0 ]]; then
					printf "$(gettext '%s is an invalid netmask.')\n" "${answer}"
					printf "$(gettext 'The netmask must be provided in IPv4 dot notation.')\n\n\a"
					continue
				fi

				# Set the netmask
				netmask=${answer}

				# Set the netmask_hex value
				let netmask_hex=$(sc_ipv4_dot_to_hex ${netmask})

				#
				# Make sure it has no holes and that it at least
				# covers our network address.
				#
				sc_netmask_check ${netmask_hex}
				let result=$?
				if [[ ${netmask_hex} -lt ${min_netmask_hex} ]] ||
				    [[ ${result} -ne 0 ]]; then
					printf "$(gettext '%s is an invalid netmask for network adddress %s.')\n" "${answer}" "${netaddr}"
					printf "$(gettext 'Choose a netmask that masks at least all of this private network address.')\n\n\a"
					continue
                                fi

				#
				# Make sure that the netmask is not too
				# restrictive.  It must allow for a host
				# portion which supports enough addresses
				# for the desired number of anticipated nodes
				# and private nets.
				#
				if [[ ${netmask_hex} -gt ${min_maxcounts_netmask_hex} ]]; then
					printf "$(gettext 'This netmask cannot accommodate the desired configuration.')\n"
					printf "$(gettext 'Choose a netmask that masks no more than %s.')\n\n\a" "${min_maxcounts_netmask}"
					continue
				fi

				#
				# If the netmask allows for double nodes
				# and private nets, reset "maxnodes" and
				# "maxprivnets" to their doubled values.
				#
				if [[ ${netmask_hex} -lt ${double_maxcounts_netmask_hex} ]]; then
					maxnodes=${double_maxnodes}
					maxprivnets=${double_maxprivnets}
                                fi

				# Done
				break
			done
		fi

		# Done
		break
	done

	#
	# Set up the command set
	#

	# Initialize
	set -A CLSETUP_DO_CMDSET

	# Add the command
	CLSETUP_DO_CMDSET[0]="${CL_CMD_CLUSTER} set-netprops -p private_netaddr=${netaddr} -p private_netmask=${netmask} -p max_nodes=${maxnodes} -p max_privatenets=${maxprivnets}"

	# Attempt to issue the command
	clsetup_do_cmdset "" "" "" "noncluster" || return 1

	return 0
}

#####################################################
#####################################################
##
## Resource Group Management
##
#####################################################
#####################################################

#####################################################
#
# clsetup_menu_rgm_rg_create() [nohelp]
#
#	nohelp		- if this flag is given, do not
#			    clear screen, print title, print help,
#			    or ask for confirmation to continue
#
#	Create a resource group.
#
#	Return:
#		zero		Completed
#		non-zero	Not completed
#
#####################################################
clsetup_menu_rgm_rg_create()
{
	typeset nohelp=${1}

	typeset sctxt_title="$(gettext '
		>>> Create a Resource Group <<<
	')"
	typeset sctxt_p1="$(gettext '
		Use this option to create a new resource group.  You
		can also use this option to create new resources for
		the new group.
	')"
	typeset sctxt_p2="$(gettext '
		A resource group is a container into which you can
		place resources of various types, such as network and
		data service resources.   The cluster uses resource
		groups to manage its resource types.  There are two
		types of resource groups, failover and scalable.
	')"
	typeset sctxt_p3="$(gettext '
		Only failover resource groups may contain network
		resources.  A network resource is either a
		LogicalHostname or SharedAddress resource.
	')"
	typeset sctxt_p4="$(gettext '
		It is important to remember that each scalable resource
		group depends upon a failover resource group which
		contains one or more SharedAddress network resources.
	')"

	typeset sctxt_p1_pprefix="$(gettext '
		Some types of resources (for example, HA for NFS)
		require the use of an area in a global file system for
		storing configuration data.  If any of the resources
		that will be added to this group require such support,
		you can specify the full directory path name now.
	')"
	typeset sctxt_p2_pprefix="$(gettext '
		Please remember that this directory must be located on
		a global file system.
	')"

	typeset sctxt_p1_scalable="$(gettext '
		Each scalable resource group has a tightly defined
		dependency on a failover resource group which contains
		one or more network resources of the SharedAddress resource
		type.
	')"
	typeset sctxt_p1_no_sharedaddr="$(gettext '
		However, there are no SharedAddress resources currently
		configured in the cluster.
	')"
	typeset sctxt_p2_no_sharedaddr="$(gettext '
		You must create a failover resource group with
		one or more SharedAddress network resources upon which
		a scalable resource group can depend, before you can
		create that scalable resource group.
	')"
	typeset sctxt_p1_2nodes="$(gettext '
		Because this cluster has two nodes, the new resource
		group will be configured to be hosted by both cluster
		nodes.
	')"
	typeset sctxt_p2_2nodes="$(gettext '
		At this time, you may select one node to be the
		preferred node for hosting this group.  Or, you may
		allow the system to select a preferred node on an
		arbitrary basis.
	')"
	if [[ ${ZONES_OK} -eq 1 ]]; then
	    typeset sctxt_p1_node_list="$(gettext "
		Each resource group is associated with a list of nodes/zones
		which are capable of hosting it.  Also, the order in which
		nodes/zones are found in a group's \"Nodelist\" reflects a
		preferred ordering for hosting a group.
	")"
	    typeset sctxt_p2_node_list="$(gettext '
		You may either explicitly specify an ordered list of
		nodes/zones for hosting a new group or let the system take
		the default \"Nodelist\".  The default is an
		arbitrarily ordered list of all of the nodes/zones in the
		cluster.
	')"
	else
	    typeset sctxt_p1_node_list="$(gettext "
		Each resource group is associated with a list of nodes which
		are capable of hosting it.   Also, the order in which nodes
		are found in a group's \"Nodelist\" reflects a preferred
		ordering for hosting a group.
	")"
	    typeset sctxt_p2_node_list="$(gettext '
		You may either explicitly specify an ordered list of
		nodes for hosting a new group or let the system take
		the default \"Nodelist\".  The default is an
		arbitrarily ordered list of all of the nodes in the
		cluster.
	')"
	fi
	typeset sctxt_p1_network_rsl="$(gettext '
		If a failover resource group contains LogicalHostname
		resources, the most common configuration is to have one
		LogicalHostname resource for each subnet.
	')"
	typeset sctxt_p1_network_rss="$(gettext '
		If a failover resource group contains SharedAddress
		resources, the most common configuration is to have one
		SharedAddress resource for each subnet.
	')"

	typeset cachefile=${CLSETUP_RGM_CACHEFILE}
	typeset tmpfile=${CLSETUP_TMP_RGLIST}

	typeset foo
	typeset answer
	typeset prompt
	typeset node
	typeset nodelist
	typeset rglist
	typeset rgname
	typeset rgdescription
	typeset saddr_rglist
	typeset saddr_rgname
	typeset saddr_rgdes
	typeset -l rgtype			# lower case
	typeset rgdependency
	typeset rgnodelist
	typeset net_rstype
	typeset default

	integer i
	integer pass
	integer nodecount
	integer found
	integer listcount
	integer total
	integer index

	typeset farmnode
	typeset farmnodes
	typeset farmnodelist

	# Unless nohelp is specified, print help and get confirmation
	if [[ -z "${nohelp}" ]]; then

		# Clear screen, print title and help
		(
			sc_print_title "${sctxt_title}"
			sc_print_para "${sctxt_p1}"
			sc_print_para "${sctxt_p2}"
			sc_print_para "${sctxt_p3}"
			sc_print_para "${sctxt_p4}"
		) | more

		# Okay to continue?
		clsetup_continue || return 1
	fi

	# Get the RG type for cluster RG's only.
	if [[ ${is_farm_rg} -eq 0 ]]; then
		prompt="$(gettext 'Select the type of resource group you want to add:')"
		rgtype="$(
	    	    sc_get_scrolling_menuoptions	\
			"${prompt}"			\
			"" ""				\
			1 1 1				\
			"Failover Group"		\
			"Scalable Group"		\
		)"
		if [[ -z "${rgtype}" ]]; then
			return 1
		fi
	else
		# Scalable Group cannot be created in Farm RG's.
		rgtype="failover"
	fi

	# Get the RG name
	while true
	do
		# Get the name of the resource group
		answer=$(sc_prompt "$(gettext 'What is the name of the group you want to add?')") || return 1

		# See if the group is already configured
		if [[ ${CLSETUP_ISMEMBER} -eq 1 ]] &&
		    [[ -n "$(clsetup_get_rglist ${answer})" ]]; then
			printf "$(gettext 'Resource group \"%s\" is already configured.')\n\n\a" "${answer}"
			continue
		fi

		# Okay
		rgname=${answer}

		# Done
		break
	done

	# Get the RG description
	rgdescription=
	while true
	do
		# Add a description?
		answer=$(sc_prompt_yesno "$(gettext 'Do you want to add an optional description?')" "${YES}") || return 1

		# If so, get the description
		if [[ "${answer}" == "yes" ]]; then

			sc_print_prompt "$(gettext 'Description:')"
			read rgdescription || continue
			echo
		fi

		# Done
		break
	done

	# Scalable resource groups depend upon a failover RG
	rgdependency=
	if [[ "${rgtype}" = "scalable" ]]; then
		# Print message - scalable groups depend on failover groups
		sc_print_para "${sctxt_p1_scalable}"

		# Get the complete list of configured groups w/ shared addrs
		if [[ ${CLSETUP_ISMEMBER} -eq 1 ]] ||
		    [[ -r "${cachefile}" ]];  then
			saddr_rglist="$(clsetup_get_rglist "" FAILOVER "SUNW.SharedAddress" ${cachefile})"
		else
			saddr_rglist="saddr_rg1 saddr_rg2 saddr_rg3 saddr_rg4 saddr_rg5 saddr_rg6 saddr_rg7 saddr_rg8 saddr_rg9 saddr_rg10 saddr_rg11 saddr_rg12"
		fi

		# Create a temp file which contains each RG with description
		rm -f ${tmpfile}
		for saddr_rgname in ${saddr_rglist}
		do
			if [[ ${CLSETUP_ISMEMBER} -eq 1 ]] ||
			    [[ -r "${cachefile}" ]];  then
				saddr_rgdes="$(clsetup_get_rgdescription ${saddr_rgname} ${cachefile})"
			else
				saddr_rgdes="DEBUG:  this is a description"
			fi
			printf "%-20s %-10.10s %-30.30s" ${saddr_rgname} "Failover" "${saddr_rgdes}"
			if [[ ${#saddr_rgdes} -gt 30 ]]; then
				echo " ...\c"
			fi
			echo
		done >${tmpfile}

		# If there are no groups with shared address resources, abort
		if [[ -z "${saddr_rglist}" ]]; then
			sc_print_para "${sctxt_p1_no_sharedaddr}"
			sc_print_para "${sctxt_p2_no_sharedaddr}"
			sc_prompt_pause
			rm -f ${tmpfile}
			return 1
		fi

		# Have they configured the necessary group?
		answer=$(sc_prompt_yesno "$(gettext 'Have you already configured such a group?')" "${YES}") || return 1
		if [[ $? -ne 0 ]]; then
			rm -f ${tmpfile}
			return 1
		fi
		if [[ "${answer}" != "yes" ]]; then
			sc_print_para "${sctxt_p2_no_sharedaddr}"
			sc_prompt_pause
			rm -f ${tmpfile}
			return 1
		fi

		# Get the name of the group
		prompt="$(printf "$(gettext 'Select the group upon which \"%s\" depends:')" ${rgname})"
		header1="$(printf "%-20s %-10.10s %-30.30s" "$(gettext 'Group Name')" "$(gettext 'Type')" "$(gettext 'Description')")"
		header2="$(printf "%-20s %-10.10s %-30.30s" "==========" "====" "===========")"
		rgdependency="$(
		    sc_get_scrolling_menuoptions	\
			"${prompt}"			\
			"${header1}"			\
			"${header2}"			\
			0 1 1				\
			:${tmpfile}			\
		)"
		rm -f ${tmpfile}
		if [[ -z "${rgdependency}" ]]; then
			sc_print_para "${sctxt_p2_no_sharedaddr}"
			sc_prompt_pause
			return 1
		fi
	fi

	#
	# Establish the nodelist
	#
	rgnodelist=
	let listcount=0

	# Get the configured list of cluster nodes (nodelist) and zones.
	nodelist=
	zonelist=
	node_zone_list=
	let nodecount=0
	let zonecount=0
	let node_zone_count=0
	if [[ ${CLSETUP_ISMEMBER} -eq 1 ]]; then

		# Get nodelist
		nodelist="$(clsetup_get_nodelist)"

		# Make sure we have a nodelist
		if [[ -z "${nodelist}" ]]; then
			if [[ ${is_farm_rg} -eq 1 ]]; then
				printf "$(gettext 'Unable to establish the list of farm nodes.')\n\n\a"
			else
				printf "$(gettext 'Unable to establish the list of cluster nodes.')\n\n\a"
			fi
			sc_prompt_pause
			return 1
		fi

		# Set node count
		let nodecount=$(set -- ${nodelist}; echo $#)

		if [[ ${ZONES_OK} -eq 1 ]]; then
			# Get zonelist
			zonelist="$(clsetup_get_zonelist)"

			# Set zone count
			let zonecount=$(set -- ${zonelist}; echo $#)
		fi

	# If debug mode, generate a list of nodenames based on a specified count
	else
		while [[ ${nodecount} -eq 0 ]]
		do
			answer=$(sc_prompt "$(gettext 'DEBUG:  How many nodes in this debug cluster?')" "2") || continue
			if [[ $(expr "${answer}" : '[0-9]*') -ne ${#answer} ]]; then
				printf "$(gettext '\"%s\" is not numeric.')\n\n\a" "${answer}"
				continue
			fi
			if [[ ${answer} -lt 2 ]]; then
				printf "$(gettext 'There must be at least two nodes in the debug cluster.')\n\n\a"
				continue
			fi
			let nodecount=${answer}
		done

		nodelist="node1 node2"
		let i=2
		while [[ ${i} -lt ${nodecount} ]]
		do
			((i += 1))
			nodelist="${nodelist} node${i}"
		done
	fi

	# If this is a one-node cluster with no zone, we have the nodelist
	if [[ ${ZONES_OK} -eq 1 ]]; then
		if [[ ${nodecount} -eq 1 && ${zonecount} -eq 0 ]]; then
			let listcount=1
		fi
	else
	        # If this is a one-node cluster, we have the nodelist
	        if [[ ${nodecount} -eq 1 ]]; then
	                let listcount=1
		fi
	fi

	# If this is a one-node cluster with one zone or a
	# two-node cluster with no zone, we have the nodelist
	if [[ ${ZONES_OK} -eq 1 ]]; then
		if [[ ${nodecount} -eq 1 &&  ${zonecount} -eq 1 ]] ||
		     [[ ${nodecount} -eq 2 &&  ${zonecount} -eq 0 ]]; then
			# Set the listcount
			let listcount=2
		fi
	else
	        # If this is a two-node cluster, we have the nodelist
	        if [[ ${nodecount} -eq 2 ]]; then
			# Set the listcount
			let listcount=2
		fi
	fi

	if [[ ${listcount} -eq 2 ]]; then
		# Print message - two nodes
		sc_print_para "${sctxt_p1_2nodes}"

		# If failover, do they want to specify the preferred node?
		answer=
		if [[ "${rgtype}" == "failover" ]];  then
			# reset rgnodelist
			rgnodelist=

			# Print additional message - two nodes
			sc_print_para "${sctxt_p2_2nodes}"

			# Okay to proceed?
			answer=$(sc_prompt_yesno "$(gettext 'Do you want to specify a preferred node?')" "${YES}") || return 1
		fi

		# If they want to specify preferred node, let them
		if [[ "${answer}" == "yes" ]]; then

			# Consolidate the nodes and zones list
			node_zone_list="${nodelist} ${zonelist}"
			node_zone_count=$(($nodecount + $zonecount))

			# Prompt from the list of two nodes or one node and one zone
			if [[ ${ZONES_OK} -eq 1 ]]; then
				prompt="$(gettext 'Select the preferred node or zone for hosting this group:')"
			else
				prompt="$(gettext 'Select the preferred node for hosting this group:')"
			fi
			node="$(
			    sc_get_scrolling_menuoptions	\
				"${prompt}"			\
				"" ""				\
				1 1 1				\
				${node_zone_list}		\
			)"
			if [[ -z "${node}" ]]; then
				return -1
			fi

			# Set the nodelist
			set -A foo ${node_zone_list}
			if [[ "${foo[0]}" == "${node}" ]]; then
				rgnodelist="${foo[0]} ${foo[1]}"
			elif [[ "${foo[1]}" == "${node}" ]]; then
				rgnodelist="${foo[1]} ${foo[0]}"
			fi
		fi

	# otherwise, this is a multi-node cluster
	else
		# Print message
		sc_print_para "${sctxt_p1_node_list}"
		sc_print_para "${sctxt_p2_node_list}"

		# Do they want to specify the Nodelist?
		prompt="$(printf "$(gettext 'Do you want to explicitly specify the %s?')" "Nodelist")"
		answer=$(sc_prompt_yesno "${prompt}" "${YES}") || return 1

		# Consolidate the nodes and zones list
		node_zone_list="${nodelist} ${zonelist}"
		node_zone_count=$(($nodecount + $zonecount))

		# If so, let them
		if [[ "${answer}" == "yes" ]]; then

			# Set the menu title
			if [[ ${ZONES_OK} -eq 1 ]]; then
			    prompt="$(gettext 'Select an ordered list of two or more nodes/zones:')"
			else
			    prompt="$(gettext 'Select an ordered list of two or more nodes:')"
			fi

			# Loop until list is confirmed
			rgnodelist=
			while [[ -z "${rgnodelist}" ]]
			do
				# Prompt for an ordered list of nodes
				rgnodelist="$(
				    sc_get_scrolling_menuoptions	\
				    "${prompt}"				\
				    "" ""				\
				    2 ${node_zone_count} 1		\
				    ${node_zone_list}			\
				)"
				if [[ -z "${rgnodelist}" ]]; then
					return -1
				fi

				# Print the list and verify that it is correct
				(
				    if [[ ${ZONES_OK} -eq 1 ]]; then
					echo
					sc_print_para "$(gettext 'This is the ordered list of nodes/zones you entered:')"
				    else
					echo
					sc_print_para "$(gettext 'This is the ordered list of nodes you entered:')"
				    fi
					for node in ${rgnodelist}
					do
						printf "\t${node}\n"
					done
					echo
				) | more

				# Verify
				answer=$(sc_prompt_yesno "$(gettext 'Is it correct?')" "${YES}") || return 1
				if [[ "${answer}" != "yes" ]]; then
					rgnodelist=
				fi
			done

			# Set the list count
			let listcount=$(set -- ${rgnodelist}; echo $#)

		# Otherwise, the listcount is all nodes and zones in the cluster
		else
			let listcount=${node_zone_count}
		fi
	fi

	# Get the pathprefix
	rgpathprefix=
	while true
	do
		# Print message
		sc_print_para "${sctxt_p1_pprefix}"

		# Add a pathprefix?
		answer=$(sc_prompt_yesno "$(gettext 'Do you want to specify such a directory now?')" "${NO}") || return 1

		# If not, we are done
		if [[ "${answer}" != "yes" ]]; then
			break
		fi

		# Get the directory name
		while [[ -z "${rgpathprefix}" ]]
		do
			# Set default
			default=
			if [[ ${CLSETUP_ISMEMBER} -eq 1 ]] &&
			     [[ -d /global/${rgname} ]]; then
				default=/global/${rgname}
			fi

			# Print message
			sc_print_para "${sctxt_p2_pprefix}"

			# Get the name and verify
			while true
			do
				# Get the name
				answer=$(sc_prompt "$(gettext 'What is the name of the (\"Pathprefix\") directory?')" "${default}") || continue 2

				if [[ "${answer}" != /* ]]; then
					printf "$(gettext 'The name of the directory must begin with /.')\n\n\a"
					continue
				fi

				# If not in debug mode, make sure it is there
				if [[ ${CLSETUP_ISMEMBER} -eq 1 ]] &&
				    [[ ! -d "${answer}" ]]; then
					printf "$(gettext 'Directory not found - %s.')\n\n\a" "${answer}"
					continue
				fi

				# Done
				break
			done

			rgpathprefix=${answer}
		done

		# Done
		break
	done

	#
	# Set up the command set
	#

	# Initialize
	set -A CLSETUP_DO_CMDSET

	#
	# Add the command
	#

	# Basic command
	if [[ ${is_farm_rg} -eq 0 ]]; then
		CLSETUP_DO_CMDSET[0]="${CL_CMD_CLRESOURCEGROUP} create "
	else
		farmnodes="$(clsetup_get_farmnodelist)"
		if [[ -z ${farmnodes} ]]; then
			printf "$(gettext 'There are no farmnode configured.')\n\n\a"
			return 1
		fi
		farmnodelist=
		for farmnode in ${farmnodes}
		do
			if [[ -z ${farmnodelist} ]]; then
				farmnodelist=${farmnode}
			else
				farmnodelist="${farmnodelist},${farmnode}"
			fi
		done
		if [[ -z "${rgnodelist}" ]]; then
			CLSETUP_DO_CMDSET[0]="${CL_CMD_CLRESOURCEGROUP} create -n ${farmnodelist} "
		else
			CLSETUP_DO_CMDSET[0]="${CL_CMD_CLRESOURCEGROUP} create "
		fi
	fi

	# RG nodelist
	if [[ -n "${rgnodelist}" ]]; then
		CLSETUP_DO_CMDSET[0]="${CLSETUP_DO_CMDSET[0]} -n "

		let count=0
		for node in ${rgnodelist}
		do
			if [[ ${count} -gt 0 ]]; then
				CLSETUP_DO_CMDSET[0]="${CLSETUP_DO_CMDSET[0]},"
			fi
			CLSETUP_DO_CMDSET[0]="${CLSETUP_DO_CMDSET[0]}${node}"
			((count += 1))
		done
	fi

	# RG type
	if [[ "${rgtype}" == "scalable" ]]; then
		CLSETUP_DO_CMDSET[0]="${CLSETUP_DO_CMDSET[0]} -S"
	fi

	# RG dependencies
	if [[ -n "${rgdependency}" ]]; then
		CLSETUP_DO_CMDSET[0]="${CLSETUP_DO_CMDSET[0]} -p RG_dependencies=${rgdependency}"
	fi

        CLSETUP_DO_CMDSET[0]="${CLSETUP_DO_CMDSET[0]} ${rgname}"

	# Add-on change commands
	let index=1

	# RG pathprefix
	if [[ -n "${rgpathprefix}" ]]; then
		CLSETUP_DO_CMDSET[${index}]="${CL_CMD_CLRESOURCEGROUP} set"
		CLSETUP_DO_CMDSET[${index}]="${CLSETUP_DO_CMDSET[${index}]} -p Pathprefix=${rgpathprefix} ${rgname}"
		((index += 1))
	fi

	# RG description
	if [[ -n "${rgdescription}" ]]; then
		CLSETUP_DO_CMDSET[${index}]="${CL_CMD_CLRESOURCEGROUP} set"
		CLSETUP_DO_CMDSET[${index}]="${CLSETUP_DO_CMDSET[${index}]} -p RG_description=\"${rgdescription}\" ${rgname}"
	fi

	# Attempt to issue the command
	clsetup_do_cmdset || return 1

	# If FAILOVER RG, add check for network resources
	net_rstype=
	let pass=1
	while [[ "${rgtype}" = "failover" ]]
	do
		net_rstype=

		# Add any network resources; valid for cluster RGs only
		# Only LogicalHostname can be created in Farm RG's. SharedAddress is not supported
		if [[ ${pass} -eq 1 ]]; then
			if [[ ${is_farm_rg} -eq 0 ]]; then
				answer=$(sc_prompt_yesno "$(gettext 'Do you want to add any network resources now?')" "${YES}") || return 1
			else
				answer=$(sc_prompt_yesno "$(gettext 'Do you want to add any LogicalHostname resources now?')" "${YES}") || return 1
			fi
		else
			if [[ ${is_farm_rg} -eq 0 ]]; then
				answer=$(sc_prompt_yesno "$(gettext 'Do you want to add any additional network resources?')" "${NO}") || return 1
			else
				answer=$(sc_prompt_yesno "$(gettext 'Do you want to add any additional LogicalHostname resources?')" "${NO}") || return 1
			fi
		fi

		# No, break out
		if [[ "${answer}" != "yes" ]]; then
			break
		fi

		if [[ ${is_farm_rg} -eq 0 ]]; then
			# LogicalHostname or SharedAddress?
			prompt="$(gettext 'Select the type of network resource you want to add:')"
			net_rstype="$(
		   		sc_get_scrolling_menuoptions	\
				"${prompt}"			\
				"" ""				\
				1 1 1				\
				"LogicalHostname"		\
				"SharedAddress"			\
			)"
		else
			# Shared Address is not supported in Europa.
			net_rstype="LogicalHostname"
		fi

		if [[ -z "${net_rstype}" ]]; then
			return 1
		fi

		# How many subnets/resources?
		if [[ "${net_rstype}" == "LogicalHostname" ]]; then
			sc_print_para "${sctxt_p1_network_rsl}"
		else
			sc_print_para "${sctxt_p1_network_rss}"
		fi
		answer=
		while [[ -z "${answer}" ]]
		do
			prompt="$(printf "$(gettext 'How many %s resources would you like to create?')" ${net_rstype})"
			answer="$(sc_prompt "${prompt}" "1")" || return 1
			if [[ $(expr "${answer}" : '[0-9]*') -ne ${#answer} ]]; then
				printf "$(gettext '\"%s\" is not numeric.')\n\n\a" "${answer}"
				answer=
				continue
			fi
		done
		let total=${answer}
		let count=1
		while [[ ${count} -le ${total} ]]
		do
			# Confirm, when more than one resource?
			if [[ ${total} -gt 1 ]]; then
				if [[ ${count} -eq 1 ]]; then
					foo="$(printf "$(gettext 'Okay to configure the first %s resource?')" "${net_rstype}")"
				else
					foo="$(printf "$(gettext 'Okay to configure the next %s resource?')" "${net_rstype}")"
				fi
				answer=$(sc_prompt_yesno "${foo}" "${YES}") || break
				if [[ "${answer}" != "yes" ]]; then
					answer="$(sc_prompt_yesno "$(gettext 'Are you sure?')")" || break
					if [[ "${answer}" == "yes" ]]; then
						break
					fi
				fi
			fi

			# Add resource with appropriate help text
			if [[ ${count} -eq 1 ]]; then
				foo="nohelp"
			else
				foo="NOHELP2"
			fi
			clsetup_menu_rgm_rs_addnet "${foo}" "${rgname}" "${rgtype}" "${net_rstype}"
			if [[ $? -eq 0 ]]; then
				((count += 1))
			fi
		done

		# Done
		((pass+=1))
	done

	let count=0
	while true
	do
		# Add any data service resources?
		if [[ ${count} -eq 0 ]]; then
			answer=$(sc_prompt_yesno "$(gettext 'Do you want to add any data service resources now?')" "${YES}") || return 1
		else
			answer=$(sc_prompt_yesno "$(gettext 'Do you want to add any additional data service resources?')" "${NO}") || return 1
		fi

		# No, break out
		if [[ "${answer}" != "yes" ]]; then
			break
		fi

		# Add resource
		clsetup_menu_rgm_rs_addds "nohelp" "${rgname}" "${rgtype}" "1"

		# Next
		((count += 1))
	done

	# Bring it online?
	if [[ ${CLSETUP_ISMEMBER} -eq 1 ]]; then
		# Initialize
		set -A CLSETUP_DO_CMDSET
		CLSETUP_DO_CMDSET[0]="${CL_CMD_CLRESOURCEGROUP} online -M ${rgname}"

		answer=$(sc_prompt_yesno "$(gettext 'Do you want to manage and bring this resource group online now?')" "${YES}") || return 1
		if [[ "${answer}" == "yes" ]]; then

			# Do it, without re-confirming
			clsetup_do_cmdset 1 || return 1

		elif [[ -n "${CLSETUP_CMD_LOG}" ]]; then

			# Just ask if they want to log
			clsetup_do_cmdset 2 || return 1
		fi
	fi

	return 0
}

#####################################################
#
# clsetup_menu_rgm_rs_addnet() [nohelp rgname rgtype net_rstype]
# clsetup_menu_rgm_rs_addnet() [nohelp]
#
#	nohelp		- if this flag is given, do not
#			    clear screen, print title, print help,
#			    or ask for confirmation to continue
#			- if the flag is set to "NOHELP2", do not print
#			    any help whatsoever
#
#	rgname		- the name of the resource group
#
#	rgtype		- if given, must be "failover"
#
#	net_rstype	- "LogicalHostname" or "SharedAddress"
#
#	Add a network resource to a resource group.
#
#	Return:
#		zero		Completed
#		non-zero	Not completed
#
#####################################################
clsetup_menu_rgm_rs_addnet()
{
	typeset nohelp=${1}
	typeset rgname=${2}
	typeset -l rgtype=${3}		# lower case
	typeset net_rstype=${4}

	typeset sctxt_title="$(gettext '
		>>> Add a Network Resource to a Resource Group <<<
	')"

	typeset sctxt_p1="$(gettext '
		Use this option to add a network resource to a resource
		group.
	')"
	typeset sctxt_p2="$(gettext '
		There are two types of network resources,
		LogicalHostname and SharedAddress.  Only failover
		resource groups may contain network resources.  A
		failover resource group typically contains one or more
		network resource type resources.  Usually, there is at
		least one network resource for each subnet.
	')"
	typeset sctxt_lhostname_p1="$(gettext '
		Each network resource manages a list of one or more
		logical hostnames for a single subnet.  This is true
		whether the resource is a LogicalHostname or
		SharedAddress resource type.  The most common
		configuration is to assign a single logical hostname to
		each network resource for each subnet.  Therefore,
		clsetup(1M) only supports this configuration.  If you
		need to support more than one hostname for a given
		subnet, add the additional support using clresourcegroup(1M).
	')"
	typeset sctxt_lhostname_p2="$(gettext '
		Before clsetup(1M) can create a network resource for
		any logical hostname, that hostname must be specified
		in the hosts(4) file on each node in the cluster.  In
		addition, the required network adapters must be
		actively available on each of the nodes.
	')"

	typeset cachefile=${CLSETUP_RGM_CACHEFILE}
	typeset tmpfile=${CLSETUP_TMP_RGLIST}

	typeset rsname
	typeset answer
	typeset rglist
	typeset prompt
	typeset header1
	typeset header2
	typeset rgdescription
	typeset rsdescription

	integer error
	integer count

	# Check arguments
	let error=0
	if [[ -n "${rgname}" ]]; then
		if [[ "${rgtype}" != "failover" ]]; then
			let error=1
		elif [[ "${net_rstype}" != "LogicalHostname" ]] &&
		    [[ "${net_rstype}" != "SharedAddress" ]]; then
			let error=1
		fi
		if [[ ${error} -ne 0 ]]; then
			printf "$(gettext 'Internal error.')\n\n\a"
			return -1
		fi
	fi

	# Unless nohelp is specified, print help and get confirmation
	if [[ -z "${nohelp}" ]]; then

		# Clear screen, print title and help
		sc_print_title "${sctxt_title}"
		sc_print_para "${sctxt_p1}"
		sc_print_para "${sctxt_p2}"

		# Okay to continue?
		clsetup_continue || return 1
	fi

	# Get the network resource type
	if [[ -z "${net_rstype}" ]]; then
		prompt="$(gettext 'Select the type of network resource you want to add:')"
		net_rstype="$(
		    sc_get_scrolling_menuoptions	\
			"${prompt}"			\
			"" ""				\
			1 1 1				\
			"LogicalHostname"		\
			"SharedAddress"			\
		)"
		if [[ -z "${net_rstype}" ]]; then
			return 1
		fi
	fi

	# Get the network resource name
	rsname=
	let count=0
	while [[ -z "${rsname}" ]]
	do
		if [[ ${count} -eq 0 ]] &&
		    [[ "${nohelp}" != "NOHELP2" ]]; then
			sc_print_para "${sctxt_lhostname_p1}"
			sc_print_para "${sctxt_lhostname_p2}"
			((count += 1))
		fi
		prompt="$(gettext 'What logical hostname do you want to add?')"
		answer=$(sc_prompt "${prompt}") || return 1
		if [[ ${CLSETUP_ISMEMBER} -eq 1 ]]; then
			rsname="$(clsetup_get_resource ${answer})"
			if [[ -n "${rsname}" ]]; then
				printf "$(gettext '\"%s\" already exists.')\n\n\a" "${rsname}"
				rsname=
				continue
			fi
		fi

		# Done
		rsname=${answer}
	done

	# Get the rgname
	while [[ -z "${rgname}" ]]
	do
		# Get the complete list of configured FAILOVER groups
		if [[ ${CLSETUP_ISMEMBER} -eq 1 ]] ||
		    [[ -r "${cachefile}" ]];  then
			rglist="$(clsetup_get_rglist "" FAILOVER "" ${cachefile})"
		else
			rglist="failover_rg1 failover_rg2 failover_rg3 failover_rg4 failover_rg5 failover_rg6 failover_rg7 failover_rg8 failover_rg9 failover_rg10 failover_rg11 failover_rg12"
		fi

		# Make sure we found at least one failover group
		if [[ -z "${rglist}" ]]; then
			echo
			printf "$(gettext 'There are no failover resource groups.')\n\n\a"
			sc_prompt_pause
			return -1
		fi

		# Create a temp file which contains each RG with description
		rm -f ${tmpfile}
		for rgname in ${rglist}
		do
			if [[ ${CLSETUP_ISMEMBER} -eq 1 ]] ||
			    [[ -r "${cachefile}" ]];  then
				rgdescription="$(clsetup_get_rgdescription ${rgname} ${cachefile})"
			else
				rgdescription="DEBUG:  this is a description"
			fi
			printf "%-20s %-10.10s %-30.30s" ${rgname} "Failover" "${rgdescription}"
			if [[ ${#rgdescription} -gt 30 ]]; then
				echo " ...\c"
			fi
			echo
		done >${tmpfile}

		# Get the resource group name
		prompt="$(printf "$(gettext 'Select the resource group you want to use for \"%s\":')" "${rsname}")"
		header1="$(printf "%-20s %-10.10s %-30.30s" "$(gettext 'Group Name')" "$(gettext 'Type')" "$(gettext 'Description')")"
		header2="$(printf "%-20s %-10.10s %-30.30s" "==========" "====" "===========")"
		rgname="$(
		    sc_get_scrolling_menuoptions	\
			"${prompt}"			\
			"${header1}"			\
			"${header2}"			\
			1 1 1				\
			:${tmpfile}			\
		)"
		rm -f ${tmpfile}
		if [[ -z "${rgname}" ]]; then
			return 1
		fi
	done

	# Set the rgtype to failover
	rgtype=failover

	# Initialize
	set -A CLSETUP_DO_CMDSET

	# Basic command
	if [[ "${net_rstype}" = "LogicalHostname" ]]; then
		rsdescription="LogicalHostname resource for ${rsname}"
		CLSETUP_DO_CMDSET[0]="${CL_CMD_CLRES_LOGICAL_HOSTNAME} create -g ${rgname} -p R_description=\"${rsdescription}\"  ${rsname}"
	elif [[ "${net_rstype}" = "SharedAddress" ]]; then
		rsdescription="SharedAddress resource for ${rsname}"
		CLSETUP_DO_CMDSET[0]="${CL_CMD_CLRES_SHARED_ADDRESS} create -g ${rgname} -p R_description=\"${rsdescription}\"  ${rsname}"
	fi

	# Attempt to issue the command
	clsetup_do_cmdset || return 1

	return 0
}

####################################################
#
# clsetup_menu_rgm_rs_nodelist() rgname [resource] [prompt]
#
#	rgname		- RG Name of the resource.
#
#	resource	- Resource name to be enabled/
#			disabled on the selected node(s).
#
#	prompt		- Title to be displayed in the menu.
#
#	Display and select nodelist for resource.
#
#	Return:
#		nodelist 	list of nodes selected.
#
#	Exitvalue:
#		0		on successful completion
#		1		on failure
#
####################################################
clsetup_menu_rgm_rs_nodelist()
{
	typeset rgname=${1}
	typeset resource=${2}
	typeset prompt=${3}

	typeset nodelist
	typeset node
	typeset foo
	typeset answers
	typeset answer
	typeset nodenames=
	typeset nodename
	integer count

	#
	# Use the SCHA_RS_GET infrastructure to get the RG
	# where the resource belongs.
	#
	#  Use the SCHA_RG_GET infrastrucure to get the nodelist for
	#  the RG where it is primaried.
	if [[ -z "${rgname}" ]]; then
		rgname="$(${SCHA_RS_GET} -O Group -R "${resource}")"
	fi
	nodelist="$(${SCHA_RG_GET} -G ${rgname} -O nodelist)"
	if [[ -n "${nodelist}" ]]; then
		# Get nodenames from the user
		while [[ -z ${nodenames} ]]
		do
			answers="$(
				    sc_get_scrolling_menuoptions	\
					"${prompt}"			\
					""				\
					""				\
					0 0 0				\
					${nodelist}			\
					"All"
				)"

			# Dup this function's stdout to descriptor 5.
			# Then, re-direct stdout to descriptor 4.
			#
			# So, the default stdout from this function will go to
			# the original stdout (probably the tty).  Descriptor 5
			# has the option letter printed to it.
			#

			exec 5>&1
			exec 1>&4

			# Add answers to nodelist
			for answer in ${answers}
			do
				if [[ ${answer} == "All" ]]; then
					nodenames="${nodelist}"
					break
				else
					nodenames="${nodenames} ${answer}"
				fi
			done

			# Make sure that there is at least one nodelist
			if [[ -z "${nodenames}" ]]; then
				return 1
			fi

			# verify that the list is correct
			echo
			sc_print_para "$(gettext 'This is the complete list of nodes you selected:')"

			for foo in ${nodenames}
			do
				printf "\t${foo}\n"
			done
			echo
			answer=$(sc_prompt_yesno "$(gettext 'Is it correct?')" "${YES}") || return 1
			if [[ "${answer}" != "yes" ]]; then
				nodenames=
				continue
			fi
		done
		# reformat nodelist with commas
		foo=
		let count=0
		for nodename in ${nodenames}
		do
			if [[ -z "${foo}" ]]; then
				foo=${nodename}
			else
				foo="${foo},${nodename}"
			fi
		done
		nodenames="${foo}"
	fi

	#Done

	echo "${nodenames}" >&5
	return 0

}
#######################################################################
#
# clsetup_menu_rgm_rs_set_per_node_props() rsname type property value
#
#	Set the per-node extension properties of a resource.
#
#	Return:
#		zero		Completed
#		non-zero	Not completed
########################################################################
clsetup_menu_rgm_rs_set_per_node_props()
{
	typeset rgname=${1}
	typeset proptype=${2}
	typeset property=${3}

	typeset value=
	typeset answer

	typeset nodelist=
	typeset cmd_ext_prop=

	exec 5>&1
	exec 1>&4

	echo
	answer=$(sc_prompt_yesno "$(gettext 'Do you require the same value for all nodes or zones that can master the resource?')" "${YES}")
	# Set the per-node extension property value for all nodes.
	if [[ ${answer} == "yes" ]]; then
		 value=$(clsetup_menu_rgm_rs_prompt_per_node_ext_prop_value "${proptype}" "") || return 1
		 cmd_ext_prop=${property}"="${value}
	else
		while true
		do
			prompt="$(gettext 'Select the node(s) on which the per-node value needs to be changed')"
			nodelist=$(clsetup_menu_rgm_rs_nodelist "${rgname}" "" "${prompt}") || continue
		 	value=$(clsetup_menu_rgm_rs_prompt_per_node_ext_prop_value "${proptype}" "${nodelist}") || return 1

			cmd_ext_prop="${cmd_ext_prop} \"${property}{${nodelist}}=${value}\""

			# Want to set another value for node.
			answer=$(sc_prompt_yesno "$(gettext 'Do you want to set another per-node value?')" "${YES}")
			if [[ ${answer} == "yes" ]]; then
				cmd_ext_prop="${cmd_ext_prop} "
				continue
			fi
			# Done setting the values.
			break
		done
	fi
	echo "${cmd_ext_prop}" >&5
}

#####################################################
#
# clsetup_menu_rgm_rs_addds() [nohelp rgname rgtype]
# clsetup_menu_rgm_rs_addds() [nohelp]
#
#	nohelp		- if this flag is given, do not
#			    clear screen, print title, print help,
#			    or ask for confirmation to continue
#
#	rgname		- the name of the resource group
#
#	rgtype		- if given, must be "failover" or "scalable"
#
#	Add a data service resource to a resource group.
#
#	Return:
#		zero		Completed
#		non-zero	Not completed
#
#####################################################
clsetup_menu_rgm_rs_addds()
{
	typeset nohelp=${1}
	typeset rgname=${2}
	typeset -l rgtype=${3}		# lower case

	typeset sctxt_title="$(gettext '
		>>> Add a Data Service Resource to a Resource Group <<<
	')"

	typeset sctxt_p1="$(gettext '
		This option allows you to add a data service resource
		to a resource group.  If the resource type for the data
		service is not yet registered with the cluster, you
		will have the opportunity to register that type.
	')"
	typeset sctxt_reg_p1="$(gettext '
		The type of resource you selected is not yet
		registered.  Each resource type must be registered with
		the cluster before any resources of the selected type
		can be added to a resource group.
	')"
	typeset sctxt_reg_p2="$(gettext '
		Registration of a resource type updates the
		cluster-wide configuration with the resource type
		properties found in default rt_reg files (see
		rt_reg(4)) on this node.  It is important that the same
		resource type or data service software has been
		installed on each node in the cluster prior to
		registration.

	')"
	typeset sctxt_reg_p3="$(gettext '
		If you do not register the resource type now, you will
		not be able to add this resource.
	')"
	typeset sctxt_netused_p1="$(gettext '
		For scalable resources, you must specify a list of
		SharedAddress resources upon which the new resource
		depends.  Since network resources can only belong to
		failover resource groups, the SharedAddress resources
		that you name must live in a failover group.
	')"
	typeset sctxt_netused_p2="$(gettext '
		However, there are no SharedAddress resources currently
		configured in the cluster.
	')"
	typeset sctxt_portlist_p1="$(gettext '
		This data service uses the \"Port_list\" property.  The
		default \"Port_list\" for this data service is as
		follows:
	')"
	typeset sctxt_portlist_p2="$(gettext '
		This data service requires that you explicitly set the
		\"Port_list\" property.
	')"
	typeset sctxt_portlist_p3="$(gettext '
		Please check the man page for this resource type
		for more information on how the list should be set for
		this resource.
	')"
	typeset sctxt_portlist_prompt1="$(gettext 'Port number (Ctrl-D to finish):')"
	typeset sctxt_portlist_prompt2="$(gettext 'Is this a TCP port?')"

	typeset sctxt_loadbalance_title="$(gettext '
		Configure a load balancing policy
	')"
	typeset sctxt_loadbalance_p1="$(gettext '
		Sun Cluster supports three load balancing policies.
		Each scalable resource must have a policy assigned to
		it at creation time.  The policies are weighted,
		sticky, and wild.
	')"
	typeset sctxt_loadbalance_p2="$(gettext '
		A weighted load balancing policy distributes the load
		among the various nodes according to the weights set in
		a \"Load_balancing_weights\" property.
	')"
	typeset sctxt_loadbalance_p3="$(gettext '
		A sticky load balancing policy always returns a given
		client of a scalable resource to the same node and port
		of the cluster.
	')"
	typeset sctxt_loadbalance_p4="$(gettext '
		A wild load balancing policy always returns a given
		client of a scalable resource to the same node, but to
		any port on that node.
	')"
	typeset sctxt_loadbalance_p5="$(gettext '
		Please refer to your data services documentation for
		recommendations on how the load balancing policy should
		be set for this service.
	')"
	typeset sctxt_loadbalance_prompt1="$(gettext 'Okay to use the default \"weighted\" policy?')"
	typeset sctxt_loadbalance_prompt2="$(gettext 'Select the policy you want to use:')"

	typeset sctxt_extprops_p1="$(gettext '
		Some resource types require that you set certain
		extension properties. if you require nondefault
		values for any extension properties of this resource,
		specify the values that you require.

		You must also specify values for any required extension
		properties for which no default is defined.

		Some properties accept different values for each node or
		zone that can  master the resource.

		For information about the extension properties of the
		resource that  you are adding, see the man page for the
		resource resource type.
	')"
	typeset sctxt_extprops_p2="$(gettext '
		Please check the man page for your data service to
		learn more about extension properties associated with
		the resource that you are adding.
	')"
	typeset sctxt_extprops_p3="$(gettext '
		This resource type does require that you set one or more
		extension properties.
	')"
	typeset sctxt_extprops_prompt1="    $(gettext 'Property name:       ')"
	typeset sctxt_extprops_prompt2="    $(gettext 'Property description:')"
	typeset sctxt_extprops_prompt3="    $(gettext 'Property type:       ')"
	typeset sctxt_extprops_prompt4="    $(gettext 'Property value:      ')"
	typeset sctxt_extprops_prompt5="    $(gettext 'Property per-node:   ')"

	typeset CLSETUP_RT_NAMES_REG		# registered rt names
	typeset CLSETUP_RT_MODE_REG		# "failover" or "scalable"
	typeset CLSETUP_RT_DESCS_REG		# registered rt descripts
	typeset CLSETUP_RT_NAMES_UNREG		# UNregistered rt names
	typeset CLSETUP_RT_MODE_UNREG		# "failover" or "scalable"
	typeset CLSETUP_RT_DESCS_UNREG		# UNregistered rt descripts

	typeset CLSETUP_RT_PROP_NAMES		# Property names
	typeset CLSETUP_RT_PROP_DESCS		# Property descriptions
	typeset CLSETUP_RT_PROP_TYPES		# Property types
	typeset CLSETUP_RT_PROP_DFLTS		# Property defaults
	typeset CLSETUP_RT_PROP_IS_PER_NODE		# Extension property type (per-node or not.)

	typeset rstypes

	typeset cachefile=${CLSETUP_RGM_CACHEFILE}
	typeset tmpfile1=${CLSETUP_TMP_RSLIST}
	typeset tmpfile1_sorted=${CLSETUP_TMP_RSLIST_SORTED}
	typeset tmpfile2=${CLSETUP_TMP_PROPLIST}

	typeset prompt
	typeset header1
	typeset header2
	typeset answer
	typeset name
	typeset answers
	typeset rgdescription
	typeset rsdescription
	typeset rglist
	typeset rglist_f
	typeset rglist_s
	typeset rsname
	typeset rsmode
	typeset rstype
	typeset rsdesc
	typeset rslist
	typeset rsscalable
	typeset rsloadpolicy
	typeset rsnetused
	typeset rsportlist
	typeset rsportlist_dflt
	typeset foo
	typeset foonetused
	typeset propname
	typeset propnames
	typeset -l proptype		# lower case
	typeset propvalue
	typeset -l lpropvalue		# lower case
	typeset propvalues
	typeset propdesc
	typeset proprequired
	typeset -l lprop_is_per_node
	typeset prop_is_per_node
	set cmd_ext_prop

	integer i
	integer j
	integer index
	integer index_reg
	integer index_unreg
	integer counter
	integer rstype_register
	integer found
	integer registered
	integer error
	integer saved_mustset
	integer mustset
	integer rscount
	integer count

	# Check arguments
	let error=0
	if [[ -n "${rgname}" ]]; then
		if [[ "${rgtype}" != "failover" ]] &&
		    [[ "${rgtype}" != "scalable" ]]; then
			printf "$(gettext 'Internal error.')\n\n\a"
			return -1
		fi
	fi

	# Unless nohelp is specified, print help and get confirmation
	if [[ -z "${nohelp}" ]]; then

		# Clear screen, print title and help
		sc_print_title "${sctxt_title}"
		sc_print_para "${sctxt_p1}"

		# Okay to continue?
		clsetup_continue || return 1
	fi

	#
	# Set up the following arrays:
	#
	#	CLSETUP_RT_NAMES_REG	# registered resource type names
	#	CLSETUP_RT_DESCS_REG	# registered resource descripts
	#	CLSETUP_RT_MODE_REG	# "failover" or "scalable"
	#
	#	CLSETUP_RT_NAMES_UNREG	# unregistered resource type names
	#	CLSETUP_RT_DESCS_UNREG	# unregistered resource descrips
	#	CLSETUP_RT_MODE_UNREG	# "failover" or "scalable"
	#
	if [[ ${CLSETUP_ISMEMBER} -eq 1 ]]; then
		clsetup_get_registered_rstypes ${CL_NOSYSRT} "nonet" ${cachefile}
		clsetup_get_unregistered_rstypes "nonet"
	else
		if [[ -n "${cachefile}" ]] && [[ -r "${cachefile}" ]]; then
			clsetup_get_registered_rstypes ${CL_NOSYSRT} "nonet" ${cachefile}
		fi
		if [[ -z "${CLSETUP_RT_NAMES_REG[0]}" ]]; then
			set -A CLSETUP_RT_NAMES_REG
			set -A CLSETUP_RT_DESCS_REG
			set -A CLSETUP_RT_MODE_REG

			set -A CLSETUP_RT_NAMES_UNREG
			set -A CLSETUP_RT_DESCS_UNREG
			set -A CLSETUP_RT_MODE_UNREG

			# Set some debug resource types
			CLSETUP_RT_NAMES_REG[0]=Debug1
			CLSETUP_RT_NAMES_REG[1]=Debug2
			CLSETUP_RT_DESCS_REG[0]="Failover resource type debug1"
			CLSETUP_RT_DESCS_REG[1]="Scalable resource type debug2"
			CLSETUP_RT_MODE_REG[0]="failover"
			CLSETUP_RT_MODE_REG[1]="scalable"

			CLSETUP_RT_NAMES_UNREG[0]=unreg_Debug3
			CLSETUP_RT_NAMES_UNREG[1]=unreg_Debug4
			CLSETUP_RT_DESCS_UNREG[0]="Failover resource type debug3"
			CLSETUP_RT_DESCS_UNREG[1]="Scalable resource type debug4"
			CLSETUP_RT_MODE_UNREG[0]="failover"
			CLSETUP_RT_MODE_UNREG[1]="scalable"
		fi
	fi

	# Get the resource type name
	rstype=
	while [[ -z "${rstype}" ]]
	do

		# Create a temp file which contains each res, with description
		rsdesc=
		let rstype_register=0
		let index_reg=0
		let index_unreg=0
		let counter=0
		set -A rstypes

		rm -f ${tmpfile1}
		while true
		do
			if [[ -n "${CLSETUP_RT_NAMES_REG[index_reg]}" ]]; then
				rstype=${CLSETUP_RT_NAMES_REG[index_reg]}
				rsdesc="${CLSETUP_RT_DESCS_REG[index_reg]}"
				rsmode=${CLSETUP_RT_MODE_REG[index_reg]}
				((index_reg += 1))
				if [[ "${rgtype}" == "scalable" ]] &&
				    [[ "${rsmode}" != "scalable" ]]; then
					continue
				fi
			elif [[ -n "${CLSETUP_RT_NAMES_UNREG[index_unreg]}" ]]; then
				rstype=${CLSETUP_RT_NAMES_UNREG[index_unreg]}
				rsdesc="${CLSETUP_RT_DESCS_UNREG[index_unreg]}"
				rsmode=${CLSETUP_RT_MODE_UNREG[index_unreg]}
				((index_unreg += 1))
				if [[ "${rgtype}" == "scalable" ]] &&
				    [[ "${rsmode}" != "scalable" ]]; then
					continue
				fi
			else
				# End of list
				break
			fi
			rstypes[counter]=${rstype}
			((counter += 1))
			printf "%-20s %-43.43s" "${rstype}" "${rsdesc}"
			if [[ ${#rsdesc} -gt 43 ]]; then
				echo " ...\c"
			fi
			echo
		done >${tmpfile1}

		# Sort it
		rm -f ${tmpfile1_sorted}
		sort ${tmpfile1} >${tmpfile1_sorted}
		rm -f ${tmpfile1}

		# Make sure we found something
		if [[ ${counter} -eq 0 ]]; then
			if [[ "${rgtype}" == "scalable" ]]; then
				printf "$(gettext 'Cannot find any scalable data service resource types.')\n\n\a"
			else
				printf "$(gettext 'Cannot find any data service resource types.')\n\n\a"
			fi
			rm -f ${tmpfile1_sorted}
			sc_prompt_pause
			return 1
		fi
		echo

		# Get the resource type
		prompt="$(gettext 'Select the type of resource you want to add:')"
		header1="$(printf "%-20s %-43.43s" "$(gettext 'Res Name')" "$(gettext 'Description')")"
		header2="$(printf "%-20s %-43.43s" "========" "===========")"
		rstype="$(
		    sc_get_scrolling_menuoptions	\
			"${prompt}"			\
			"${header1}"			\
			"${header2}"			\
			1 1 1				\
			:${tmpfile1_sorted}		\
		)"
		rm -f ${tmpfile1_sorted}
		if [[ -z "${rstype}" ]]; then
			return 1
		fi

		# Set the rsmode and registered flag, if it is registered
		rsmode=
		let registered=0
		let index_reg=0
		while [[ -n "${CLSETUP_RT_NAMES_REG[index_reg]}" ]]
		do
			if [[ "${CLSETUP_RT_NAMES_REG[index_reg]}" == "${rstype}" ]]; then
				rsmode=${CLSETUP_RT_MODE_REG[index_reg]}
				let registered=1
				break
			fi
			((index_reg += 1))
		done

		# If not registered, it should be unregistered
		if [[ ${registered} -eq 0 ]]; then
			let index_unreg=0
			while [[ -n "${CLSETUP_RT_NAMES_UNREG[index_unreg]}" ]]
			do
				if [[ "${CLSETUP_RT_NAMES_UNREG[index_unreg]}" == "${rstype}" ]]; then
					rsmode=${CLSETUP_RT_MODE_UNREG[index_unreg]}
					break
				fi
				((index_unreg += 1))
			done
		fi

		# If unregistered, register it now?
		while [[ ${registered} -eq 0 ]]
		do
			# Print message
			sc_print_para "${sctxt_reg_p1}"
			sc_print_para "${sctxt_reg_p2}"

			# Is the software installed on each node?
			answer=$(sc_prompt_yesno "$(gettext 'Is the software for this service installed on each node?')" "${YES}") || return 1
			if [[ "${answer}" != "yes" ]]; then
				printf "$(gettext 'Please install the software on each node, then try again.')\n\n\a"
				return 1
			fi

			# Okay to register the data service
			answer=$(sc_prompt_yesno "$(gettext 'Is it okay to register this resource type now?')" "${YES}") || return 1
			if [[ "${answer}" != "yes" ]]; then

				# Print message
				sc_print_para "${sctxt_reg_p3}"

				# Confirm
				answer=$(sc_prompt_yesno "$(gettext 'Are you sure?')" "${NO}") || return 1
				if [[ "${answer}" != "yes" ]]; then
					printf "$(gettext 'Then you must select another resource type.')\n\n\a"
					sc_prompt_pause
					return 1
				fi
			fi

			# Okay to register
			let rstype_register=1

			# Done
			break
		done

		# Done
		break
	done

	# Register the resource type, if needed
	if [[ -n "${rstype}" ]] && [[ ${rstype_register} -ne 0 ]]; then

		# Initialize
		set -A CLSETUP_DO_CMDSET

		# Add the command
		CLSETUP_DO_CMDSET[0]="${CL_CMD_CLRESOURCETYPE} register ${rstype}"

		# Attempt to issue the command, without re-confirmation
		clsetup_do_cmdset 1 || return 1
	fi

	# Get the resource name
	rsname=
	while [[ -z "${rsname}" ]]
	do
		# Get the name of the resource
		answer="$(sc_prompt "$(gettext 'What is the name of the resource you want to add?')")" || return 1

		# If the resource already exists, print error
		if [[ ${CLSETUP_ISMEMBER} -eq 1 ]]; then
			foo="$(clsetup_get_resource ${answer})"
			if [[ -n "${foo}" ]]; then
				printf "$(gettext 'Resource \"%s\" is already configured.')\n\n\a" "${answer}"
				continue
			fi
		fi

		# Done
		rsname=${answer}
	done

	# Get the rgname
	while [[ -z "${rgname}" ]]
	do
		rglist_f=
		rglist_s=

		# Get the failover and scalable RGs
		if [[ ${CLSETUP_ISMEMBER} -eq 1 ]] ||
		    [[ -r "${cachefile}" ]]; then
			rglist_f="$(clsetup_get_rglist "" Failover "" ${cachefile})"
			if [[ "${rsmode}" == "scalable" ]]; then
				rglist_s="$(clsetup_get_rglist "" scalable "" ${cachefile})"
			fi
		else
			rglist_f="failover_rg1 failover_rg2 failover_rg3 failover_rg4 failover_rg5 failover_rg6 failover_rg7 failover_rg8 failover_rg9 failover_rg10 failover_rg11 failover_rg12 failover_rg13 failover_rg14"
			if [[ "${rsmode}" == "scalable" ]]; then
				rglist_s="scalable_rg1 scalable_rg2 scalable_rg3 scalable_rg4 scalable_rg5 scalable_rg6 scalable_rg7 scalable_rg8 scalable_rg9 scalable_rg10 scalable_rg11 scalable_rg12 scalable_rg13 scalable_rg14"
			fi
		fi

		# Make sure we found at least one group
		if [[ -z "${rglist_f}" ]] && [[ -z "${rglist_s}" ]]; then
			echo
			if [[ "${rsmode}" == "failover" ]]; then
				printf "$(gettext 'There are no failover resource groups configured.')\n\n\a"
			else
				printf "$(gettext 'There are no resource groups configured.')\n\n\a"
			fi
			sc_prompt_pause
			return -1
		fi

		# Create a temp file which contains each RG with description
		rm -f ${tmpfile1}
		rglist="${rglist_f}"
		foo="Failover"
		for j in 1 2
		do
			for rgname in ${rglist}
			do
				if [[ ${CLSETUP_ISMEMBER} -eq 1 ]] ||
				    [[ -r "${cachefile}" ]];  then
					rgdescription="$(clsetup_get_rgdescription ${rgname} ${cachefile})"
				else
					rgdescription="DEBUG:  this is a description"
				fi

				printf "%-20s %-10.10s %-30.30s" ${rgname} "${foo}" "${rgdescription}"
				if [[ ${#rgdescription} -gt 30 ]]; then
					echo " ...\c"
				fi
				echo
			done
			rglist="${rglist_s}"
			foo="Scalable"
		done >${tmpfile1}

		# Get the resource group name
		prompt="$(printf "$(gettext 'Select the resource group you want to use for \"%s\":')" "${rsname}")"
		header1="$(printf "%-20s %-10.10s %-30.30s" "$(gettext 'Group Name')" "$(gettext 'Type')" "$(gettext 'Description')")"
		header2="$(printf "%-20s %-10.10s %-30.30s" "==========" "====" "===========")"
		rgname="$(
		    sc_get_scrolling_menuoptions	\
			"${prompt}"			\
			"${header1}"			\
			"${header2}"			\
			1 1 1				\
			:${tmpfile1}			\
		)"
		rm -f ${tmpfile1}
		if [[ -z "${rgname}" ]]; then
			return 1
		fi
	done

	# Get the rg type
	if [[ -z "${rgtype}" ]]; then
		if [[ ${CLSETUP_ISMEMBER} -eq 1 ]]; then
			rgtype="$(clsetup_get_rgtype ${rgname})"
			if [[ -z "${rgtype}" ]]; then
				printf "$(gettext 'Unable to discover what type of resource group this is.')\n\n\a"
				sc_prompt_pause
				return 1
			fi
		else
			# Get the RG type from DEBUG mode
			rgtype=
			while [[ "${rgtype}" != "failover" ]] &&
			    [[ "${rgtype}" != "scalable" ]]
			do
				rgtype=$(sc_prompt "DEBUG:  What type of resource group is this (failover|scalable)?" "failover")
			done
		fi
	fi

	# Cache the scrgadm output
	if [[ ${CLSETUP_ISMEMBER} -eq 1 ]]; then
		clsetup_create_rgmfile ${cachefile}
	fi

        # See if we must set the Scalable property
	rsscalable=
	if [[ ${CLSETUP_ISMEMBER} -eq 1 ]]; then
		clsetup_get_prop_default ${rstype} Scalable
		if [[ $? -eq 0 ]]; then
			rsscalable=doit
		fi
	elif [[ "${rgtype}" == "scalable" ]]; then
		rsscalable=doit
	fi
	if [[ -n "${rsscalable}" ]]; then
		if [[ "${rgtype}" == "scalable" ]]; then
			rsscalable=true
		else
			rsscalable=false
		fi
	fi

	# Get the network_resources_used list for scalable RGs, if required
	rsnetused=
	if [[ "${rgtype}" == "scalable" ]]; then

		if [[ ${CLSETUP_ISMEMBER} -eq 1 ]] ||
		    [[ -r "${cachefile}" ]];  then
			clsetup_get_prop_default ${rstype} Network_resources_used ${cachefile} 2>/dev/null 1>&2
			if [[ $? -eq 0 ]]; then
				rsnetused=doit
			fi
		else
			rsnetused=doit
		fi
	fi
	if [[ "${rsnetused}" == "doit" ]]; then
		rsnetused=

		# Print help
		sc_print_para "${sctxt_netused_p1}"

		# Get the list of SharedAddress resources
		if [[ ${CLSETUP_ISMEMBER} -eq 1 ]] ||
		    [[ -r "${cachefile}" ]];  then
			rslist="$(clsetup_get_rslist "" "SUNW.SharedAddress" ${cachefile})"
		else
			rslist="saddr_res1 saddr_res2 saddr_res3 saddr_res4 saddr_res5 saddr_res5 saddr_res6 saddr_res7 saddr_res8 saddr_res9 saddr_res10"
		fi
		if [[ -z "${rslist}" ]]; then
			sc_print_para "${sctxt_netused_p2}"
			sc_prompt_pause
			return 1
		fi

		# Get the count
		rscount=$(set -- ${rslist};  echo $#)

		# Set the prompt
		prompt="$(gettext 'Select one or more SharedAddress resources:')"
		# Get the rsnetused list
		while [[ -z "${rsnetused}" ]]
		do
			# Menu of SharedAddress resources
			rsnetused="$(
			    sc_get_scrolling_menuoptions		\
				"${prompt}"				\
				"" ""					\
				1 ${rscount} 1				\
				${rslist}				\
			)"
			if [[ -z "${rsnetused}" ]]; then
				return -1
			fi

			# Print the list and verify that the list is correct
			(
				sc_print_para "$(gettext 'This is the list you entered of SharedAddress resources used:')"
				for foo in ${rsnetused}
				do
					printf "\t${foo}\n"
				done
				echo
			) | more

			# Verify
			answer=$(sc_prompt_yesno "$(gettext 'Is it correct?')" "${YES}") || return 1
			if [[ "${answer}" != "yes" ]]; then
				rsnetused=
				continue
			fi

			# Fix up rsnetused
			foonetused=
			for foo in ${rsnetused}
			do
				if [[ -z "${foonetused}" ]]; then
					foonetused="${foo}"
				else
					foonetused="${foonetused},${foo}"
				fi
			done
			rsnetused="${foonetused}"
		done
	fi

	# See if a portlist is required
	if [[ ${CLSETUP_ISMEMBER} -eq 1 ]] || [[ -r "${cachefile}" ]];  then
		rsportlist_dflt="$(clsetup_get_prop_default ${rstype} Port_list ${cachefile})"
		# If Port_list is unknown, set dflt to "notused"
		if [[ $? -ne 0 ]]; then
			rsportlist_dflt=notused
		fi

	# If debugging (and no cachefile) and "scalable", port list is required
	elif [[ "${rgtype}" == "scalable" ]]; then
		rsportlist_dflt=

	# If debugging (and no cachefile) and not "scalable", no port list
	else
		rsportlist_dflt=notused
	fi

	# Get the rsportlist, if required
	rsportlist=
	while [[ "${rsportlist_dflt}" != "notused" ]] &&
	    [[ -z "${rsportlist}" ]]
	do
		if [[ -n "${rsportlist_dflt}" ]]; then

			# Print help
			sc_print_para "${sctxt_portlist_p1}"

			# Print default setting
			for foo in ${rsportlist_dflt}
			do
				printf "\t${foo}\n"
			done
			echo

			# Print more help
			sc_print_para "${sctxt_portlist_p3}"

			# Override the default?
			answer=$(sc_prompt_yesno "$(gettext 'Do you want to override the default?')" "${NO}") || return 1
			if [[ "${answer}" != "yes" ]]; then
				break
			fi
		else
			# Print help
			sc_print_para "${sctxt_portlist_p2}"
			sc_print_para "${sctxt_portlist_p3}"
		fi

		# Get the list of ports
		answers=
		while true
		do
			# Get the port number
			answer=$(sc_prompt "${sctxt_portlist_prompt1}" "" "nonl") || break
			if [[ "${answer}" != [0-9]* ]]; then
				echo
				printf "$(gettext '\"%s\" is not numeric.')\n\n\a" "${answer}"
				continue
			fi
			let j=${answer}
			if [[ ${j} -lt 1 ]]; then
				echo
				printf "$(gettext 'The port number must be greater than zero.')\n\n\a"
				continue
			fi

			# TCP?
			foo=$(sc_prompt_yesno "${sctxt_portlist_prompt2}" "${YES}") || continue
			if [[ "${foo}" == "yes" ]]; then
				answer="${answer}/tcp"
			else
				answer="${answer}/udp"
			fi

			# Make sure it is not a duplicate
			for foo in ${answers}
			do
				if [[ "${answer}" == "${foo}" ]]; then
					printf "$(gettext '\"%s\" already entered.')\n\n\a" "${answer}"
					continue 2
				fi
			done

			# Okay, add it to the list of answers
			answers="${answers} ${answer}"
		done

		# Did they enter anything?
		if [[ -z "${answers}" ]]; then
			continue
		fi

		# Verify that the list is correct
		echo
		sc_print_para "$(gettext 'This is the Port_list which you entered:')"
		for answer in ${answers}
		do
			printf "\t${answer}\n"
		done
		echo
		answer=$(sc_prompt_yesno "$(gettext 'Is it correct?')" "${YES}") || return 1
		if [[ "${answer}" != "yes" ]]; then
			continue
		fi

		# Okay, set rsportlist
		for answer in ${answers}
		do
			if [[ -n "${rsportlist}" ]]; then
				rsportlist="${rsportlist},"
			fi
			rsportlist="${rsportlist}${answer}"
		done
	done

	# For scalable resources, get the Load_balancing_policy.
	rsloadpolicy=
	while [[ "${rsscalable}" == "true" ]] && [[ -z "${rsloadpolicy}" ]]
	do
		# Clear screen, print title and help
		sc_print_title "${sctxt_loadbalance_title}"

		# Print help
		sc_print_para "${sctxt_loadbalance_p1}"
		sc_print_para "${sctxt_loadbalance_p2}"
		sc_print_para "${sctxt_loadbalance_p3}"
		sc_print_para "${sctxt_loadbalance_p4}"
		sc_print_para "${sctxt_loadbalance_p5}"

		# Weighted?
		answer=$(sc_prompt_yesno "${sctxt_loadbalance_prompt1}" "${YES}") || return 1
		if [[ "${answer}" == "yes" ]]; then
			rsloadpolicy="Lb_weighted"
			break
		fi

		# Otherwise, select from menu
		rsloadpolicy="$(
		    sc_get_scrolling_menuoptions		\
			"${sctxt_loadbalance_prompt2}"		\
			"" ""					\
			1 1 1					\
			"Weighted"				\
			"Sticky"				\
			"Wild"					\
		)"

		# By default, select weighted.
		if [[ -z "${rsloadpolicy}" ]]; then
			rsloadpolicy="Weighted"
		fi

		# Confirm
		foo="$(printf "$(gettext 'Are you sure you want to use the \"%s\" policy?')" "${rsloadpolicy}")"
		echo
		answer=$(sc_prompt_yesno "${foo}" "${YES}") || return 1
		if [[ "${answer}" != "yes" ]]; then
			rsloadpolicy=
			continue
		fi

		# Reset rsloadpolicy
		case ${rsloadpolicy} in
		Weighted)	rsloadpolicy="Lb_weighted" ;;
		Sticky)		rsloadpolicy="Lb_sticky" ;;
		Wild)		rsloadpolicy="Lb_wild" ;;
		*)		rsloadpolicy= ;;
		esac
	done

	#
	# Get the extension properties for this resource type
	#
	#	CLSETUP_RT_PROP_NAMES		# names of all the properties
	#	CLSETUP_RT_PROP_DESCS		# description of each property in list
	#	CLSETUP_RT_PROP_TYPES		# type of each property in list
	#	CLSETUP_RT_PROP_DFLTS		# default values for each property
	#	CLSETUP_RT_PROP_IS_PER_NODE	# boolean value to indicate whether the given extension property
	#
	set -A CLSETUP_RT_PROP_NAMES
	set -A CLSETUP_RT_PROP_DESCS
	set -A CLSETUP_RT_PROP_TYPES
	set -A CLSETUP_RT_PROP_DFLTS
	set -A CLSETUP_RT_PROP_IS_PER_NODE

	if [[ ${CLSETUP_ISMEMBER} -eq 1 ]];  then
		clsetup_get_rstype_extprops ${rstype}

	# otherwise, fabricate debug data
	else
		set -A CLSETUP_RT_PROP_NAMES Prop_int Prop_string Prop_array Prop_other
		CLSETUP_RT_PROP_DESCS[0]="Integer property"
		CLSETUP_RT_PROP_DESCS[1]="String property"
		CLSETUP_RT_PROP_DESCS[2]="String array property"
		CLSETUP_RT_PROP_DESCS[3]="Other property"

		CLSETUP_RT_PROP_TYPES[0]=Int
		CLSETUP_RT_PROP_TYPES[1]=String
		CLSETUP_RT_PROP_TYPES[2]=Stringarray
		CLSETUP_RT_PROP_TYPES[3]=

		CLSETUP_RT_PROP_DFLTS[0]=20
		CLSETUP_RT_PROP_DFLTS[1]="hello"
		CLSETUP_RT_PROP_DFLTS[2]=
		CLSETUP_RT_PROP_DFLTS[3]=

		CLSETUP_RT_PROP_IS_PER_NODE[0]="True"
		CLSETUP_RT_PROP_IS_PER_NODE[1]="False"
		CLSETUP_RT_PROP_IS_PER_NODE[2]="True"
		CLSETUP_RT_PROP_IS_PER_NODE[3]="False"
	fi

	# Done with cache file, for now
	if [[ ${CLSETUP_ISMEMBER} -eq 1 ]]; then
		clsetup_rm_rgmfile ${cachefile}
	fi

	#
	# Get the extension property settings
	#

	# Initialize menu headings
	prompt="$(gettext 'Select a property you would like to set:')"
	header1="$(printf "%-30s %-18.18s %-18.18s" "$(gettext 'Property Name')" "$(gettext 'Default Setting')" "$(gettext 'Desired Setting')")"
	header2="$(printf "%-30s %-18.18s %-18.18s" "=============" "===============" "===============")"

	# Set propnames and propvalues arrays
	set -A propnames
	set -A propvalues
	set -A cmd_ext_props
	while [[ -n "${CLSETUP_RT_PROP_NAMES[0]}" ]]
	do
		# First time through with property name selection?
		if [[ -z "${propnames[0]}" ]]; then

			# See if any properties must be set
			let i=0
			let saved_mustset=0
			for propname in ${CLSETUP_RT_PROP_NAMES[*]}
			do
				if [[ -z "${CLSETUP_RT_PROP_DFLTS[i]}" ]]; then
					((saved_mustset += 1))
				fi
				((i += 1))
			done

			# Print help
			(
				sc_print_para "${sctxt_extprops_p1}"
				sc_print_para "${sctxt_extprops_p2}"
				if [[ ${saved_mustset} -gt 0 ]]; then
					sc_print_para "${sctxt_extprops_p3}"
				fi
			) | more

		# Not the first time through
		else
			# Clear the old values?
			if [[ -n "${propvalues[*]}" ]]; then
				answer=$(sc_prompt_yesno "$(gettext 'Do you want to clear all the old property values?')" "${NO}")
				if [[ "${answer}" == "yes" ]]; then
					set -A propnames
					set -A propvalues
					set -A cmd_ext_props
				fi
			else
				set -A propnames
			fi
		fi

		# Set count of properties that must be set
		let mustset=${saved_mustset}

		# Set any properties now?
		if [[ -z "${propnames[0]}" ]] &&
		    [[ ${mustset} -eq 0 ]]; then
			answer=$(sc_prompt_yesno "$(gettext 'Any extension properties you would like to set?')" "${YES}") || return 1
			if [[ "${answer}" != "yes" ]]; then
				break
			fi
		fi

		# Loop for all properties they want to set
		while true
		do
			# Setup the menu list
			rm -f ${tmpfile2}
			let i=0
			for propname in ${CLSETUP_RT_PROP_NAMES[*]}
			do
				# Set the default value
				propdflt="${CLSETUP_RT_PROP_DFLTS[i]}"

				# Set the desired value
				let j=0
				propvalue=
				while [[ -n "${propnames[j]}" ]]
				do
					if [[ "${propname}" == "${propnames[j]}" ]]; then
						propvalue="${propvalues[j]}"
						break
					fi

					# Next
					((j += 1))
				done

				# If no default and no value, set string
				if [[ -z "${propdflt}" ]] &&
				    [[ -z "${propvalue}" ]]; then
					propdflt="$(printf "<%s>" "$(gettext 'Needs Setting')")"
				fi

				# Add the line
				printf "%-30s %-18.18s %-18.18s\n" "${propname}" "${propdflt}" "${propvalue}"

				# Next
				((i += 1))
			done >${tmpfile2}

			# Get the name of the property to set
			propname="$(
			    sc_get_scrolling_menuoptions	\
				"${prompt}"			\
				"${header1}"			\
				"${header2}"			\
				0 1 1				\
				:${tmpfile2}			\
			)"
			rm -f ${tmpfile2}

			# Done?
			if [[ -z "${propname}" ]]; then

				# See if all required properties are set
				if [[ ${mustset} -gt 0 ]]; then
					echo
					printf "$(gettext 'Any property without a default value must be explicitly set.')\n\n\a"
					sc_prompt_pause
					continue
				fi

				# Are they done?
				answer=$(sc_prompt_yesno "$(gettext 'Are you done setting properties?')" "${YES}") || return 1
				if [[ "${answer}" == "yes" ]]; then
					break
				else
					continue
				fi
			fi

			# Set the index into propnames and propvalues
			let index=0
			while [[ -n "${propnames[index]}" ]]
			do
				if [[ "${propname}" == "${propnames[index]}" ]]; then
					break
				fi
				((index += 1))
			done

			# Print the name
			sc_print_prompt "${sctxt_extprops_prompt1}" "space"
			echo "${propname}"

			# Print the description
			sc_print_prompt "${sctxt_extprops_prompt2}" "space"
			propdesc="$(clsetup_get_rstype_extprop_desc ${propname})"
			echo "${propdesc}"

			# Print the property type
			sc_print_prompt "${sctxt_extprops_prompt3}" "space"
			proptype="$(clsetup_get_rstype_extprop_type ${propname})"
			echo "${proptype}"

			# Print the extension property type
			sc_print_prompt "${sctxt_extprops_prompt5}" "space"
			lprop_is_per_node="$(clsetup_get_rstype_extprop_is_per_node ${propname})"
			case ${lprop_is_per_node} in
	                	true)   prop_is_per_node="TRUE" ;;
                                false)  prop_is_per_node="FALSE" ;;
                                *)	;;
			esac

			echo "${prop_is_per_node}"

			# See if we have to set this
			proprequired="$(clsetup_get_rstype_extprop_dflt ${propname})"
			[[ -z "${proprequired}" ]] && proprequired=1

			if [[ "${lprop_is_per_node}" == "false" ]]; then
				while true
				do
					propvalue=
					sc_print_prompt "${sctxt_extprops_prompt4}" "space"
					read propvalue

					# Ctrl-D?
					if [[ $? -ne 0 ]]; then
						echo
						continue 2
					fi

					# Compress ", " in stringarrays
					if [[ "${proptype}" == "stringarray" ]]; then
						propvalue="$(echo ${propvalue} | sed 's/, /,/g')"
					fi

					# Too many options?
					if [[ "${proptype}" != "string" ]];  then
						count=$(set -- ${propvalue};  echo $#)
						if [[ ${count} -gt 1 ]]; then
							echo "\a\c"
							continue
						fi
					fi

					# Newline
					echo

					# Nothing entered
					if [[ -z "${propvalue}" ]];  then

						# Clear the old value?
						if [[ -n "${propvalues[index]}" ]]; then
							answer=$(sc_prompt_yesno "$(gettext 'Do you want to clear the old property value?')" "${NO}")
							if [[ "${answer}" == "yes" ]]; then
								propvalues[index]=
								if [[ -n "${proprequired}" ]]; then
									((mustset += 1))
								fi
							fi
						fi
						continue 2
					fi

					#
					# Attempt to make sure the value is in some
					# kind of legal range.  Note that we currently
					# do not check for all value types.
					#
					case ${proptype} in
					int)
						if [[ $(expr "${propvalue}" : '[0-9]*') -ne ${#propvalue} ]]; then
							printf "$(gettext '\"%s\" is not numeric.')\n\n\a" "${propvalue}"
							continue
						fi
						;;

					boolean)
						lpropvalue=${propvalue}
						case ${lpropvalue} in
						true)	propvalue=TRUE ;;
						false)	propvalue=FALSE ;;
						*)
							printf "$(gettext '%s must be set to either \"%s\" or \"%s\".')\n\n\a" "Boolean" "TRUE" "FALSE"
							continue
						;;
						esac
						;;

					string|stringarray)
						;;

					*)
						if [[ "${propvalue}" == *,* ]]; then
							printf "$(gettext 'Stringarray is not expected.')\n\n\a"
							continue
						fi
						;;
					esac

					# Done
					break
				done
			else
				cmd_ext_props[index]=$(clsetup_menu_rgm_rs_set_per_node_props "${rgname}" "${proptype}" "${propname}") || return 1

			fi


			# Set the name and value
			if [[ -z "${propnames[index]}" ]]; then
				propnames[index]=${propname}
			fi
			propvalues[index]=${propvalue}

			if [[ -z "${cmd_ext_props[index]}" ]]; then
				cmd_ext_props[index]=${propnames[index]}"="${propvalues[index]}
			fi

			if [[ -n "${proprequired}" ]]; then
				((mustset -= 1))
			fi
		done

		# Verify that the list is correct
		echo
		sc_print_prompt "$(gettext 'Here is the list of extension properties you want to set:')"
		echo
		echo
		if [[ -n "${cmd_ext_props[*]}" ]]; then
			for cmd_ext_prop in ${cmd_ext_props[*]}
			do
				printf "\t${cmd_ext_prop}\n"
			done | more
		else
			printf "\t<%s>\n" "$(gettext 'none')"
		fi
		echo

		answer=$(sc_prompt_yesno "$(gettext 'Is it correct?')" "${YES}") || return 1
		if [[ "${answer}" == "yes" ]]; then
			break
		fi
	done

	# Set the default description (in English)
	if [[ "${rsscalable}" == "true" ]]; then
		rsdescription="Scalable data service resource for ${rstype}"
	else
		rsdescription="Failover data service resource for ${rstype}"
	fi

	#
	# Add the resource
	#

	# Initialize
	set -A CLSETUP_DO_CMDSET

	# Basic command
	CLSETUP_DO_CMDSET[0]="${CL_CMD_CLRESOURCE} create "
	CLSETUP_DO_CMDSET[0]="${CLSETUP_DO_CMDSET[0]} -g ${rgname}"
	CLSETUP_DO_CMDSET[0]="${CLSETUP_DO_CMDSET[0]} -t ${rstype}"

	# Add Scalable=true|false
	if [[ -n "${rsscalable}" ]]; then
		CLSETUP_DO_CMDSET[0]="${CLSETUP_DO_CMDSET[0]} -p Scalable=${rsscalable}"
	fi

	# Add Load_balancing_policy
	if [[ -n "${rsloadpolicy}" ]]; then
		CLSETUP_DO_CMDSET[0]="${CLSETUP_DO_CMDSET[0]} -p Load_balancing_policy=${rsloadpolicy}"
	fi

	# Add Network_resources_used
	if [[ -n "${rsnetused}" ]]; then
		CLSETUP_DO_CMDSET[0]="${CLSETUP_DO_CMDSET[0]} -p Resource_dependencies=${rsnetused}"
	fi

	# Add the Port_list, if set
	if [[ -n "${rsportlist}" ]]; then
		CLSETUP_DO_CMDSET[0]="${CLSETUP_DO_CMDSET[0]} -p Port_list=${rsportlist}"
	fi

	# Add extension properties, if there are any
	for cmd_ext_prop in ${cmd_ext_props[*]}
	do
		if [[ -n "${cmd_ext_prop}" ]]; then
			cmd_ext_prop=" -p ${cmd_ext_prop}"
			CLSETUP_DO_CMDSET[0]="${CLSETUP_DO_CMDSET[0]} ${cmd_ext_prop}"
		fi
	done

        CLSETUP_DO_CMDSET[0]="${CLSETUP_DO_CMDSET[0]} ${rsname}"

	# Add the default description
	CLSETUP_DO_CMDSET[1]="${CL_CMD_CLRESOURCE} set -p R_description=\"${rsdescription}\" ${rsname}"

	# Attempt to issue the command
	clsetup_do_cmdset || return 1
	case $? in
	'0')	# Update and/or logging completed
		;;
	'2')	# User decided not to update OR log
		noenable=1
		;;
	*)	# Error
		return 1
		;;
	esac

	return 0
}

#####################################################
#
# clsetup_menu_rgm_rt_registration()
#
#	Register one or more unregistered resource types found
#	on this node.   Or, unregister one or more unused resource
#	types.
#
#	This function always returns zero.
#
#####################################################
clsetup_menu_rgm_rt_registration()
{
	typeset sctxt_title="$(gettext '
		>>> Resource Type Registration <<<
	')"

	typeset sctxt_p1="$(gettext '
		This option allows you to register and unregister
		resource types.
	')"
	typeset sctxt_p2="$(gettext '
		Registration of a resource type updates the
		cluster-wide configuration with the resource type
		properties found in default rt_reg files (see
		rt_reg(4)) on this node.  It is important that the same
		resource type or data service software has been
		installed on each node in the cluster prior to
		registration.
	')"
	typeset sctxt_p3="$(gettext '
		A resource type, or version of a resource type, can only
		be unregistered as long as there are no resources still
		using it.
	')"
	typeset sctxt_p4="$(gettext '
		As a result of a data service upgrade, the versions
		of some resource types may be changed.   These resource
		types should be re-registered under their new versions.
		Resources may then be re-versioned to use the updated
		resource types.   Once an old version of a resource type
		is no longer used by any resources, that resource type
		can be unregistered.
	')"

	typeset sctxt_prompt_rtreg="$(gettext 'Make a selection:')"
	typeset sctxt_option_rtreg_001="$(gettext 'Register all resource types which are not yet registered')"
	typeset sctxt_option_rtreg_002="$(gettext 'Register selected resource types')"
	typeset sctxt_option_rtreg_003="$(gettext 'Unregister selected resource types')"
	typeset sctxt_option_rtreg_004="$(gettext 'Unregister all resource types which are not in use by resources')"
	typeset sctxt_option_return="$(gettext 'Return to the resource group menu')"

	typeset CLSETUP_RT_NAMES_REG		# registered rt names
	typeset CLSETUP_RT_MODE_REG		# "failover" or "scalable"
	typeset CLSETUP_RT_DESCS_REG		# registered rt descripts
	typeset CLSETUP_RT_NAMES_UNREG		# Unregistered rt names
	typeset CLSETUP_RT_MODE_UNREG		# "failover" or "scalable"
	typeset CLSETUP_RT_DESCS_UNREG		# UNregistered rs descripts

	typeset tmpfile1=${CLSETUP_TMP_RSLIST}
	typeset tmpfile1_sorted=${CLSETUP_TMP_RSLIST_SORTED}

	typeset option
	typeset task
	typeset line
	typeset prompt
	typeset header1
	typeset header2
	typeset rstype
	typeset inuse_rstypes
	typeset unreg_rstypes
	typeset rsname
	typeset rslist
	typeset foo

	integer index

	# Clear screen, print title and help
	sc_print_title "${sctxt_title}"
	sc_print_para "${sctxt_p1}"
	sc_print_para "${sctxt_p2}"
	sc_print_para "${sctxt_p3}"
	sc_print_para "${sctxt_p4}"

	# Okay to continue?
	clsetup_continue || return 1

	# Loop until done or Ctrl-D is typed
	while true
	do

		# Register or unregister?
		option=$(sc_get_menuoption \
			"T2+++${sctxt_prompt_rtreg}" \
			"S+0+1+${sctxt_option_rtreg_001}" \
			"S+0+2+${sctxt_option_rtreg_002}" \
			"S+0+3+${sctxt_option_rtreg_003}" \
			"S+0+4+${sctxt_option_rtreg_004}" \
			"R+++" \
			"S+0+q+${sctxt_option_return}" \
		)
		case ${option} in
		1)	task="registerall"	;;
		2)	task="register"		;;
		3)	task="unregister"	;;
		4)	task="unregisterall"	;;
		*)	return 0		;;
		esac

		# Common registration stuff
		if [[ ${task} == "register" ]] ||
		    [[ ${task} == "registerall" ]]; then
			#
			# Set up the following arrays for registration:
			#
			# CLSETUP_RT_NAMES_UNREG  # unregistered rt names
			# CLSETUP_RT_DESCS_UNREG  # unregistered rt descrips
			#
			if [[ ${CLSETUP_ISMEMBER} -eq 1 ]]; then
				clsetup_get_registered_rstypes ${CL_SYSRT} "nonet"
				clsetup_get_unregistered_rstypes "nonet"
			else
				set -A CLSETUP_RT_NAMES_UNREG
				set -A CLSETUP_RT_DESCS_UNREG

				# Set some debug resource types
				CLSETUP_RT_NAMES_UNREG[0]=SUNW.unreg_Debug1
				CLSETUP_RT_NAMES_UNREG[1]=SUNW.unreg_Debug2
				CLSETUP_RT_DESCS_UNREG[0]="unreg_Debug1"
				CLSETUP_RT_DESCS_UNREG[1]="unreg_Debug2"
			fi

			# Create a temp file which contains each unreg type
			let index=0
			rm -f ${tmpfile1}
			while [[ -n "${CLSETUP_RT_NAMES_UNREG[index]}" ]]
			do
				printf "%-20s %-43.43s" "${CLSETUP_RT_NAMES_UNREG[index]}" "${CLSETUP_RT_DESCS_UNREG[index]}"
				if [[ ${#CLSETUP_RT_DESCS_UNREG[index]} -gt 43 ]]; then
					echo " ...\c"
				fi
				echo

				((index += 1))
			done >${tmpfile1}
		elif [[ ${task} == "unregister" ]] ||
		    [[ ${task} == "unregisterall" ]]; then
			#
			# Set up the following arrays for unregistration:
			#
			# CLSETUP_RT_NAMES_REG    # registered rt names
			# CLSETUP_RT_DESCS_REG    # registered rt descrips
			#
			if [[ ${CLSETUP_ISMEMBER} -eq 1 ]]; then
				clsetup_get_registered_rstypes ${CL_NOSYSRT} "nonet"
			else
				set -A CLSETUP_RT_NAMES_REG
				set -A CLSETUP_RT_DESCS_REG

				# Set some debug resource types
				CLSETUP_RT_NAMES_REG[0]=SUNW.reg_Debug3
				CLSETUP_RT_NAMES_REG[1]=SUNW.reg_Debug4
				CLSETUP_RT_DESCS_REG[0]="reg_Debug3"
				CLSETUP_RT_DESCS_REG[1]="reg_Debug4"
			fi

			# Get the list of in-use resource types
			if [[ ${CLSETUP_ISMEMBER} -eq 1 ]]; then
				rslist="$(clsetup_get_rslist)"
				inuse_rstypes=
				for rsname in ${rslist}
				do
					rstype="$(${SCHA_RS_GET} -R ${rsname} -O type)"
					for foo in ${inuse_rstypes}
					do
						if [[ "${rstype}" == "${foo}" ]]; then
							continue 2
						fi
					done
					inuse_rstypes="${inuse_rstypes} ${rstype}"
				done
			else
				inuse_rstypes="SUNW.reg_Debug4"
			fi

			# Create a temp file which lists each unused unreg type
			let index=0
			rm -f ${tmpfile1}
			while [[ -n "${CLSETUP_RT_NAMES_REG[index]}" ]]
			do
				for foo in ${inuse_rstypes}
				do
					if [[ "${CLSETUP_RT_NAMES_REG[index]}" == "${foo}" ]]; then
						((index += 1))
						continue 2
					fi
				done
				unreg_rstypes="${unreg_rstypes} ${CLSETUP_RT_NAMES_REG[index]}"
				printf "%-20s %-43.43s" "${CLSETUP_RT_NAMES_REG[index]}" "${CLSETUP_RT_DESCS_REG[index]}"
				if [[ ${#CLSETUP_RT_DESCS_REG[index]} -gt 43 ]]; then
					echo " ...\c"
				fi
				echo

				((index += 1))
			done >${tmpfile1}

		fi

		# Sort the tmpfile created above
		rm -f ${tmpfile1_sorted}
		sort ${tmpfile1} >${tmpfile1_sorted}
		rm -f ${tmpfile1}

		# Initialize headers
		header1="$(printf "%-20s %-43.43s" "$(gettext 'Resource Type')" "$(gettext 'Description')")"
		header2="$(printf "%-20s %-43.43s" "$(gettext '=============')" "$(gettext '===========')")"

		case ${task} in
		registerall)		# Register all resource types

			# Print help
			sc_print_para "$(printf "$(gettext 'This option allows you to register all resource types which have %s files (see %s) in %s an in %s, but are not yet registered.')" "rt_reg" "rt_reg(4)" "${CLSETUP_RTR_DIR}" "${CLSETUP_OPT_RTR_DIR}")"

			# Anything to register?
			if [[ ! -s "${tmpfile1_sorted}" ]]; then
				printf "$(gettext 'There are no unregistered resource types in %s and in %s.')\n\n\a" "${CLSETUP_RTR_DIR}" "${CLSETUP_OPT_RTR_DIR}"
				sc_prompt_pause
				continue
			fi

			# Print list
			(
				sc_print_para "$(gettext 'The following resource types are unregistered:')"
				printf "%11.11s%s\n" "" "${header1}"
				printf "%11.11s%s\n" "" "${header2}"
				echo
				while read line
				do
					printf "%11.11s%s\n" "" "${line}"
				done <${tmpfile1_sorted}
				echo
			) | more -15

			# Okay to register?
			answer=$(sc_prompt_yesno "$(gettext 'Is it okay to register all of these?')" "${YES}") || continue
			if [[ "${answer}" != "yes" ]]; then
				continue
			fi

			# Set rstypes
			rstypes="${CLSETUP_RT_NAMES_UNREG[*]}"
			;;

		register)		# Register selected resource types

			# Print help
			sc_print_para "$(printf "$(gettext 'This option allows you to register resource types which are not yet registered, but which have %s files (see %s) in %s and in %s.')" "rt_reg" "rt_reg(4)" "${CLSETUP_RTR_DIR}" "${CLSETUP_OPT_RTR_DIR}")"

			# Anything to register?
			if [[ ! -s "${tmpfile1_sorted}" ]]; then
				printf "$(gettext 'There are no unregistered resource types in %s and in %s.')\n\n\a" "${CLSETUP_RTR_DIR}" "${CLSETUP_OPT_RTR_DIR}"
				sc_prompt_pause
				continue
			fi

			# Get the resource types
			prompt="$(gettext 'Select the resource type(s) you want to register:')"
			rstypes="$(
			    sc_get_scrolling_menuoptions	\
				"${prompt}"			\
				"${header1}"			\
				"${header2}"			\
				0 0 1				\
				:${tmpfile1_sorted}		\
			)"
			rm -f ${tmpfile1_sorted}
			if [[ -z "${rstypes}" ]]; then
				continue
			fi

			# Print the list and verify
			(
				sc_print_para "$(gettext 'This is the list of resource types you selected for registration:')"
				for rstype in ${rstypes}
				do
					printf "\t${rstype}\n"
				done
				echo
			) | more

			# Okay to register?
			answer=$(sc_prompt_yesno "$(gettext 'Is it okay to register these?')" "${YES}") || continue
			if [[ "${answer}" != "yes" ]]; then
				continue
			fi
			;;

		unregister)		# Unegister selected resource types

			# Print help
			sc_print_para "$(gettext 'This option allows you to unregister resource types which are not already in use by resources.')"

			# Anything to unregister?
			if [[ -z "${CLSETUP_RT_NAMES_REG[0]}" ]]; then
				printf "$(gettext 'There are no registered resource types to unregister.')\n\n\a"
				sc_prompt_pause
				continue
			fi
			if [[ ! -s "${tmpfile1_sorted}" ]]; then
				printf "$(gettext 'All registered resource types are in use by resources.')\n\n\a"
				sc_prompt_pause
				continue
			fi

			# Get the resource types
			prompt="$(gettext 'Select the resource type(s) you want to unregister:')"
			rstypes="$(
			    sc_get_scrolling_menuoptions	\
				"${prompt}"			\
				"${header1}"			\
				"${header2}"			\
				0 0 1				\
				:${tmpfile1_sorted}		\
			)"
			rm -f ${tmpfile1_sorted}
			if [[ -z "${rstypes}" ]]; then
				continue
			fi

			# Print the list and verify
			(
				sc_print_para "$(gettext 'This is the list of resource types you selected to unregister:')"
				for rstype in ${rstypes}
				do
					printf "\t${rstype}\n"
				done
				echo
			) | more

			# Okay to unregister?
			answer=$(sc_prompt_yesno "$(gettext 'Is it okay to unregister these?')" "${YES}") || continue
			if [[ "${answer}" != "yes" ]]; then
				continue
			fi
			;;

		unregisterall)		# Unregister all resource types

			# Print help
			sc_print_para "$(gettext 'This option allows you to unregister all resource types which are not already in use by resources.')"

			# Anything to unregister?
			if [[ -z "${CLSETUP_RT_NAMES_REG[0]}" ]]; then
				printf "$(gettext 'There are no registered resource types to unregister.')\n\n\a"
				sc_prompt_pause
				continue
			fi
			if [[ ! -s "${tmpfile1_sorted}" ]]; then
				printf "$(gettext 'All registered resource types are in use by resources.')\n\n\a"
				sc_prompt_pause
				continue
			fi

			# Print list
			(
				sc_print_para "$(gettext 'The following resource types are registered, but not used:')"
				printf "%11.11s%s\n" "" "${header1}"
				printf "%11.11s%s\n" "" "${header2}"
				echo
				while read line
				do
					printf "%11.11s%s\n" "" "${line}"
				done <${tmpfile1_sorted}
				echo
			) | more -15

			# Okay to unregister?
			answer=$(sc_prompt_yesno "$(gettext 'Is it okay to unregister all of these?')" "${YES}") || continue
			if [[ "${answer}" != "yes" ]]; then
				continue
			fi

			# Set rstypes
			rstypes="${unreg_rstypes}"
			;;

		esac

		# Run the commands
		if [[ ${task} == "register" ]] ||
		    [[ ${task} == "registerall" ]]; then

			# Initialize the command set
			let index=0
			set -A CLSETUP_DO_CMDSET
			for rstype in ${rstypes}
			do
				CLSETUP_DO_CMDSET[index]="${CL_CMD_CLRESOURCETYPE} register ${rstype}"
				((index += 1))
			done

			# Do it, without re-confirming
			clsetup_do_cmdset 1 || continue

		elif [[ ${task} == "unregister" ]] ||
		    [[ ${task} == "unregisterall" ]]; then

			# Initialize the command set
			let index=0
			set -A CLSETUP_DO_CMDSET
			for rstype in ${rstypes}
			do
				CLSETUP_DO_CMDSET[index]="${CL_CMD_CLRESOURCETYPE} unregister ${rstype}"
				((index += 1))
			done

			# Do it, without re-confirming
			clsetup_do_cmdset 1 || continue
		fi

		# Done
		break
	done

	return 0
}

#####################################################
#
# clsetup_menu_rgm_rg_change_nodelist () rgname [help_only] [nohelp]
#
#	rgname		- name of the resource group to change.  If
#			    "help_only" is given, the rgname may be null.
#
#	help_only	- if this flag is given, just print the
#			    help message
#
#	nohelp		- if this flag is given, do not
#			    clear screen, print title, print help,
#			    or ask for confirmation to continue
#
#	Change the Nodelist property of a resource group.
#
#	Return:
#		zero		Completed
#		non-zero	Not completed
#
#####################################################
clsetup_menu_rgm_rg_change_nodelist()
{
	typeset rgname=${1}
	typeset help_only=${2}
	typeset nohelp=${3}

	typeset sctxt_title="$(gettext '
		>>> Change the Nodelist Resource Group Property <<<
	')"
	if [[ ${ZONES_OK} -eq 1 ]]; then
	    typeset sctxt_p1="$(gettext '
		The Nodelist of a resource group is an ordered list of
		nodes/zones capable of mastering that group.  These nodes/zones
		are the potential primaries for a resource group.  You can
		add nodes/zones to a list, remove nodes/zones from a list, or
		change the order of nodes/zones in a list.
	')"
	    typeset sctxt_remove1_p1="$(gettext '
		A node/zone can only be removed from the Nodelist of a resource
		group as long as that node/zone is not online for the group.
	')"
	    typeset sctxt_remove2_p1="$(gettext '
		A node/zone can only be removed from the Nodelist of a resource
		group as long as that node/zone is not online for the group.
		Also, a node/zone cannot be removed from the Nodelist if it
		would cause the node/zone count to drop below the
		\"Desired_primaries\" setting.
	')"
	    typeset sctxt_rgnode_scalable_p1="$(gettext '
		The \"Maximum_primaries\" and \"Desired_primaries\" properties
		of a resource group control the maximum and desired number
		of nodes or zones where the resource group could be online
		at the same time. Check the rg_properties(5) man page
		for more information regarding the setting of these
		properties.
	')"
	else
	    typeset sctxt_p1="$(gettext '
		The Nodelist of a resource group is an ordered list of
		nodes capable of mastering that group.  These nodes are
		the potential primaries for a resource group.  You can
		add nodes to a list, remove nodes from a list, or
		change the order of nodes in a list.
	')"
	    typeset sctxt_remove1_p1="$(gettext '
		A node can only be removed from the Nodelist of a resource
		group as long as that node is not online for the group.
	')"
	    typeset sctxt_remove2_p1="$(gettext '
		A node can only be removed from the Nodelist of a resource
		group as long as that node is not online for the group.
		Also, a node cannot be removed from the Nodelist if it would
		cause the node count to drop below the \"Desired_primaries\"
		setting.
	')"
	    typeset sctxt_rgnode_scalable_p1="$(gettext '
		The \"Maximum_primaries\" and \"Desired_primaries\" properties
		of a resource group control the maximum and desired number
		of nodes where the resource group could be online at the
		same time. Check the rg_properties(5) man page for more
		information regarding the setting of these properties.
	')"
	fi

	typeset sctxt_prompt_operation="$(printf "$(gettext 'Select the type of change you want to make to the %s:')" "Nodelist")"

	if [[ ${ZONES_OK} -eq 1 ]]; then
	    typeset sctxt_option_operation_001="$(printf "$(gettext 'Add a node/zone to the top of the %s')" "Nodelist")"
	    typeset sctxt_option_operation_002="$(printf "$(gettext 'Add a node/zone to the bottom of the %s')" "Nodelist")"
	    typeset sctxt_option_operation_003="$(printf "$(gettext 'Move a node/zone to the top of the %s')" "Nodelist")"
	    typeset sctxt_option_operation_004="$(printf "$(gettext 'Move a node/zone to the bottom of the %s')" "Nodelist")"
	    typeset sctxt_option_operation_005="$(printf "$(gettext 'Reorder all of the nodes/zones in the %s')" "Nodelist")"
	    typeset sctxt_option_operation_006="$(printf "$(gettext 'Remove a node/zone from the %s')" "Nodelist")"
	    typeset sctxt_option_operation_007="$(printf "$(gettext 'Swap the order of the two nodes/zones in the %s')" "Nodelist")"
	else
	    typeset sctxt_option_operation_001="$(printf "$(gettext 'Add a node to the top of the %s')" "Nodelist")"
	    typeset sctxt_option_operation_002="$(printf "$(gettext 'Add a node to the bottom of the %s')" "Nodelist")"
	    typeset sctxt_option_operation_003="$(printf "$(gettext 'Move a node to the top of the %s')" "Nodelist")"
	    typeset sctxt_option_operation_004="$(printf "$(gettext 'Move a node to the bottom of the %s')" "Nodelist")"
	    typeset sctxt_option_operation_005="$(printf "$(gettext 'Reorder all of the nodes in the %s')" "Nodelist")"
	    typeset sctxt_option_operation_006="$(printf "$(gettext 'Remove a node from the %s')" "Nodelist")"
	    typeset sctxt_option_operation_007="$(printf "$(gettext 'Swap the order of the two nodes in the %s')" "Nodelist")"
	fi
	typeset sctxt_option_done="$(gettext 'Done')"

	typeset rgstatefile=${CLSETUP_TMP_RGSTATE}

	typeset prompt
	typeset answer
	typeset rgnodelist
	typeset original_rgnodelist
	typeset clnodelist
	typeset nodelist
	typeset node
	typeset -l lrgstate			# lower case
	typeset option
	typeset task
	typeset foo
	typeset foo2
	typeset rgtype

	integer rgnodelistcount
	integer clnodelistcount
	integer dprimaries
	integer changed
	integer i
	integer j
	integer set_associated_props

	# Unless nohelp is specified, print help
	if [[ -z "${nohelp}" ]] && [[ -z "${help_only}" ]]; then

		# Clear screen, print title
		sc_print_title "${sctxt_title}"
		sc_print_para "${sctxt_p1}"

		# Okay to continue?
		clsetup_continue || return 1

	elif [[ -n "${help_only}" ]]; then
		sc_print_para "${sctxt_p1}"
		return 0
	fi

	if [[ ${CLSETUP_ISMEMBER} -eq 1 ]]; then
		rgtype="$(clsetup_get_rgtype ${rgname})"
		if [[ -z "${rgtype}" ]]; then
			printf "$(gettext 'Unable to discover what type of resource group this is.')\n\n\a"
			sc_prompt_pause
			return 1
		fi
	else
		# Get the RG type from DEBUG mode
		rgtype=
		while [[ "${rgtype}" != "failover" ]] &&
		    [[ "${rgtype}" != "scalable" ]]
		do
			rgtype=$(sc_prompt "DEBUG:  What type of resource group is this (failover|scalable)?" "failover")
		done
	fi

	# Loop until done or Ctrl-D is typed
	while true
	do
		# Get and list the Nodelist
		if [[ ${CLSETUP_ISMEMBER} -eq 1 ]]; then
			rgnodelist="$(${SCHA_RG_GET} -G ${rgname} -O nodelist)"
			if [[ ${ZONES_OK} -eq 1 ]]; then
				nodelist="$(clsetup_get_nodelist)"
				zonelist="$(clsetup_get_zonelist)"
				clnodelist="${nodelist} ${zonelist}"
			else
				clnodelist="$(clsetup_get_nodelist)"
			fi
		else
			# Debug nodes in the RG nodelist
			let i=0
			let j=0
			rgnodelist=
			answer=$(sc_prompt "DEBUG:  How many nodes in the RG nodelist?")
			if [[ -n "${answer}" ]]; then
				let i=${answer}
			fi
			while [[ $j -lt $i ]]
			do
				((j += 1))
				rgnodelist="${rgnodelist} node${j}"
			done

			# Debug nodes in the cluster
			let i=0
			let j=0
			clnodelist=
			answer=$(sc_prompt "DEBUG:  How many nodes in the cluster?")
			if [[ -n "${answer}" ]]; then
				let i=${answer}
			fi
			while [[ $j -lt $i ]]
			do
				((j += 1))
				clnodelist="${clnodelist} node${j}"
			done
		fi
		rgnodelist=$(echo ${rgnodelist})
		original_rgnodelist="${rgnodelist}"
		clnodelist=$(echo ${clnodelist})
		clnodelistcount=$(set -- ${clnodelist}; echo $#)

		# Loop until we have a complete Nodelist
		while true
		do
			let changed=0
			let set_associated_props=0
			# Show the nodelist
			task=
			rgnodelistcount=$(set -- ${rgnodelist}; echo $#)
			case ${rgnodelistcount} in
			0)	# Error - No nodes/zones in the nodelist

				echo
				printf "$(gettext 'The %s for \"%s\" is unexpectedly empty.')\n" "Nodelist" "${rgname}"
				printf "$(gettext 'Use the \"%s\" command to correct the problem.')\n\n\a" "clresourcegroup"
				sc_prompt_pause
				return 1
				;;

			1)	# Just one node/zone in the nodelist

				echo
				if [[ ${ZONES_OK} -eq 1 ]]; then
				    sc_print_para "$(printf "$(gettext 'There is only one node/zone in the %s:')" "Nodelist")"
				else
				    sc_print_para "$(printf "$(gettext 'There is only one node in the %s:')" "Nodelist")"
				fi
				printf "\t${rgnodelist}\n"
				echo

				# Any nodes/zones to add?
				if [[ ${clnodelistcount} -lt 2 ]]; then
				    if [[ ${ZONES_OK} -eq 1 ]]; then
					printf "$(gettext 'And, there are no nodes/zones to add.')\n\n\a"
				    else
					printf "$(gettext 'And, there are no nodes to add.')\n\n\a"
				    fi
					sc_prompt_pause
					return 1
				fi

				# Add?
				option=$(sc_get_menuoption \
					"T2+++${sctxt_prompt_operation}" \
					"S+0+1+${sctxt_option_operation_001}" \
					"S+0+2+${sctxt_option_operation_002}" \
					"R+++" \
					"S+0+q+${sctxt_option_done}" \
				)
				case ${option} in
				1)	task="addt"	;;
				2)	task="addb"	;;
				*)	task="quit" ;;
				esac
				;;

			2)	# Just two nodes/zones in the nodelist

				echo
				if [[ ${ZONES_OK} -eq 1 ]]; then
				    sc_print_para "$(printf "$(gettext 'The following two nodes/zones are in the %s:')" "Nodelist")"
				else
				    sc_print_para "$(printf "$(gettext 'The following two nodes are in the %s:')" "Nodelist")"
				fi
				for node in ${rgnodelist}
				do
					printf "\t${node}\n"
				done
				echo

				# Add/swap/remove?
				if [[ ${clnodelistcount} -gt ${rgnodelistcount} ]]; then
					# Add/swap/remove?
					option=$(sc_get_menuoption \
					"T2+++${sctxt_prompt_operation}" \
					"S+0+1+${sctxt_option_operation_001}" \
					"S+0+2+${sctxt_option_operation_002}" \
					"S+0+3+${sctxt_option_operation_007}" \
					"S+0+4+${sctxt_option_operation_006}" \
					"R+++" \
					"S+0+q+${sctxt_option_done}" \
					)
					case ${option} in
					1)	task="addt"	;;
					2)	task="addb"	;;
					3)	task="swap"	;;
					4)	task="remove"	;;
					*)	task="quit" ;;
					esac
				else
					# Swap/remove?
					option=$(sc_get_menuoption \
					"T2+++${sctxt_prompt_operation}" \
					"S+0+1+${sctxt_option_operation_007}" \
					"S+0+2+${sctxt_option_operation_006}" \
					"R+++" \
					"S+0+q+${sctxt_option_done}" \
					)
					case ${option} in
					1)	task="swap"	;;
					2)	task="remove"	;;
					*)	task="quit" ;;
					esac
				fi
				;;

			*)	# More than two nodes/zones in rgnodelist

				# Print the nodelist, if not yet changed
				if [[ ${changed} -eq 0 ]]; then
					(
					    if [[ ${ZONES_OK} -eq 1 ]]; then
						echo
						sc_print_para "$(printf "$(gettext 'Here is the ordered list of nodes/zones in the %s for \"%s\":')" "Nodelist" "${rgname}")"
					    else
						echo
						sc_print_para "$(printf "$(gettext 'Here is the ordered list of nodes in the %s for \"%s\":')" "Nodelist" "${rgname}")"
					    fi
						for node in ${rgnodelist}
						do
							printf "\t${node}\n"
						done
						echo
					) | more
				fi

				# Add/remove/change?
				if [[ ${clnodelistcount} -gt ${rgnodelistcount} ]]; then
					# Add/remove/change?
					option=$(sc_get_menuoption \
					"T2+++${sctxt_prompt_operation}" \
					"S+0+1+${sctxt_option_operation_001}" \
					"S+0+2+${sctxt_option_operation_002}" \
					"S+0+3+${sctxt_option_operation_003}" \
					"S+0+4+${sctxt_option_operation_004}" \
					"S+0+5+${sctxt_option_operation_005}" \
					"S+0+6+${sctxt_option_operation_006}" \
					"R+++" \
					"S+0+q+${sctxt_option_done}" \
					)
					case ${option} in
					1)	task="addt"	;;
					2)	task="addb"	;;
					3)	task="movet"	;;
					4)	task="moveb"	;;
					5)	task="reorder"	;;
					6)	task="remove"	;;
					*)	task="quit" ;;
					esac
				else
					# Remove/change?
					option=$(sc_get_menuoption \
					"T2+++${sctxt_prompt_operation}" \
					"S+0+1+${sctxt_option_operation_003}" \
					"S+0+2+${sctxt_option_operation_004}" \
					"S+0+3+${sctxt_option_operation_005}" \
					"S+0+4+${sctxt_option_operation_006}" \
					"R+++" \
					"S+0+q+${sctxt_option_done}" \
					)
					case ${option} in
					1)	task="movet"	;;
					2)	task="moveb"	;;
					3)	task="reorder"	;;
					4)	task="remove"	;;
					*)	task="quit" ;;
					esac
				fi
				;;
			esac

			# Do it
			case ${task} in
			addt|addb)
				# Get the list of all cluster nodes/zones
				if [[ ${CLSETUP_ISMEMBER} -eq 1 ]]; then
				    if [[ ${ZONES_OK} -eq 1 ]]; then
					nodelist="$(clsetup_get_nodelist)"
					zonelist="$(clsetup_get_zonelist)"
					foo="${nodelist} ${zonelist}"
				    else
					foo="$(clsetup_get_nodelist)"
				    fi
				else
					foo="node1 node2 node3 node4 node5 node6"
				fi

				# Set "nodelist" to those not in "rgnodelist"
				nodelist=
				for node in ${foo}
				do
					for foo2 in ${rgnodelist}
					do
						if [[ "${node}" == "${foo2}" ]]; then
							continue 2
						fi
					done
					nodelist="${nodelist} ${node}"
				done
				nodelist=$(echo ${nodelist})

				# Available nodes/zones left?
				if [[ -z "${nodelist}" ]]; then
				    if [[ ${ZONES_OK} -eq 1 ]]; then
					echo
					printf "$(gettext 'The %s for \"%s\" already includes all cluster nodes/zones.')\n\n\a" "Nodelist" "${rgname}"
				    else
					echo
					printf "$(gettext 'The %s for \"%s\" already includes all cluster nodes.')\n\n\a" "Nodelist" "${rgname}"
				    fi
					sc_prompt_pause
					continue
				fi

				# Get the name of the node/zone to add
				if [[ ${ZONES_OK} -eq 1 ]]; then
				    prompt="$(gettext 'Select the name of the node/zone you want to add:')"
				else
				    prompt="$(gettext 'Select the name of the node you want to add:')"
				fi
				node="$(
				    sc_get_scrolling_menuoptions	\
					"${prompt}"			\
					"" ""				\
					0 1 1				\
					${nodelist}			\
				)"
				if [[ -z "${node}" ]]; then
					return 1
				fi

				# Add it to the rgnodelist
				if [[ "${task}" == "addt" ]]; then
					rgnodelist="${node} ${rgnodelist}"
				else
					rgnodelist="${rgnodelist} ${node}"
				fi

				# If it is a scalable RG, ask user if it is
				# desired to bump up the desired and max
				# primaries
				if [[ "${rgtype}" == "scalable" ]]; then
					sc_print_para "${sctxt_rgnode_scalable_p1}"
					answer=$(sc_prompt_yesno "$(gettext 'Do you want to set these properties to be equal to the number of entries in the new nodelist?')" "${YES}") || return 1
					if [[ "${answer}" == "yes" ]]; then
						set_associated_props=1
					fi
				fi

				;;

			movet|moveb)

				# Get the name of the node to move
				if [[ "${task}" == "movet" ]]; then
				    if [[ ${ZONES_OK} -eq 1 ]]; then
					prompt="$(gettext 'Select the name of the node/zone to move to the top of the list:')"
				    else
					prompt="$(gettext 'Select the name of the node to move to the top of the list:')"
				    fi
				else
				    if [[ ${ZONES_OK} -eq 1 ]]; then
					prompt="$(gettext 'Select the name of the node/zone to move to the bottom of the list:')"
				    else
					prompt="$(gettext 'Select the name of the node to move to the bottom of the list:')"
				    fi
				fi

				node="$(
				    sc_get_scrolling_menuoptions	\
					"${prompt}"			\
					"" ""				\
					0 1 1				\
					${rgnodelist}			\
				)"
				if [[ -z "${node}" ]]; then
					return 1
				fi

				# Update the rgnodelist
				nodelist=
				for foo in ${rgnodelist}
				do
					if [[ "${node}" != "${foo}" ]]; then
						nodelist="${nodelist} ${foo}"
					fi
				done
				if [[ "${task}" == "movet" ]]; then
					nodelist="${node} ${nodelist}"
				else
					nodelist="${nodelist} ${node}"
				fi
				rgnodelist=$(echo ${nodelist})
				;;

			swap)
				# Swap the two nodes in the list
				nodelist=
				for node in ${rgnodelist}
				do
					nodelist="${node} ${nodelist}"
				done
				rgnodelist=$(echo ${nodelist})
				;;

			reorder)

				# Get the new list
				if [[ ${ZONES_OK} -eq 1 ]]; then
				    prompt="$(gettext 'Select all of the nodes/zones in the desired order:')"
				else
				    prompt="$(gettext 'Select all of the nodes in the desired order:')"
				fi

				while true
				do
					nodelist="$(
					    sc_get_scrolling_menuoptions \
						"${prompt}"		\
						"" ""			\
						${rgnodelistcount}	\
						${rgnodelistcount}	\
						1			\
						${rgnodelist}		\
					)"
					if [[ -z "${nodelist}" ]]; then
						return -1
					fi

					# Print the list and verify that
					# it is correct
					(
					    if [[ ${ZONES_OK} -eq 1 ]]; then
						echo
						sc_print_para "$(gettext 'This is the ordered list of nodes/zones you entered:')"
					    else
						echo
						sc_print_para "$(gettext 'This is the ordered list of nodes you entered:')"
					    fi
						for node in ${nodelist}
						do
							printf "\t${node}\n"
						done
						echo
					) | more

					# Verify
					answer=$(sc_prompt_yesno "$(gettext 'Is it correct?')" "${YES}") || return 1
					if [[ "${answer}" == "yes" ]]; then
						break
					fi
				done

				# Update the rgnodelist
				rgnodelist=$(echo ${nodelist})
				;;

			remove)

				# Get desired primaries
				if [[ ${CLSETUP_ISMEMBER} -eq 1 ]]; then
					dprimaries="$(${SCHA_RG_GET} -G ${rgname} -O desired_primaries)"
				else
					let dprimaries=1
				fi

				# Print help
				if [[ ${dprimaries} -lt 2 ]]; then
					sc_print_para "${sctxt_remove1_p1}"
				else
					sc_print_para "${sctxt_remove2_p1}"
				fi

				# Get the list of offline nodes
				nodelist=
				clsetup_create_rgstatefile ${rgstatefile}
				for node in ${rgnodelist}
				do
					lrgstate=$(clsetup_get_rgstate ${rgname} ${rgstatefile} "" ${node})
					if [[ "${lrgstate}" == "offline" ]] ||
					    [[ "${lrgstate}" == "unmanaged" ]]; then
						nodelist="${nodelist} ${node}"
					fi
				done
				clsetup_rm_rgstatefile ${rgstatefile}

				# Make sure at least one node is offline
				if [[ -z "${nodelist}" ]]; then
				    if [[ ${ZONES_OK} -eq 1 ]]; then
					echo
					printf "$(gettext '\"%s\" is not offline on any of the nodes/zones.')\n" "${rgname}"
					printf "$(gettext 'A node/zone cannot be removed from the %s while still online for the group.')\n\n\a" "Nodelist"
				    else
					echo
					printf "$(gettext '\"%s\" is not offline on any of the nodes.')\n" "${rgname}"
					printf "$(gettext 'A node cannot be removed from the %s while still online for the group.')\n\n\a" "Nodelist"
				    fi
					sc_prompt_pause
					continue
				fi

				# If it is a scalable RG, ask user if it is
				# desired to reduce the value of desired and max
				# primaries
				if [[ "${rgtype}" == "scalable" ]]; then
					sc_print_para "${sctxt_rgnode_scalable_p1}"
					answer=$(sc_prompt_yesno "$(gettext 'Do you want to set these properties to be equal to the number of entries in the new nodelist?')" "${YES}") || return 1
					if [[ "${answer}" == "yes" ]]; then
						set_associated_props=1
					fi
				fi

				# We can't drop below desired_primaries
				if [[ ${rgnodelistcount} -le ${dprimaries} ]] && [[ ${set_associated_props} -eq 0 ]]; then
				    if [[ ${ZONES_OK} -eq 1 ]]; then
					echo
					printf "$(gettext 'The %s property is set to %d.')\n" "Desired_primaries" ${dprimaries}
					printf "$(gettext 'And, there are only %d nodes/zones in the %s'.)\n" ${rgnodelistcount} "Nodelist"
					printf "$(gettext 'Therefore, it is not possible to remove a node/zone from the %s'.)\n" "Nodelist"
				    else
					echo
					printf "$(gettext 'The %s property is set to %d.')\n" "Desired_primaries" ${dprimaries}
					printf "$(gettext 'And, there are only %d nodes in the %s'.)\n" ${rgnodelistcount} "Nodelist"
					printf "$(gettext 'Therefore, it is not possible to remove a node from the %s'.)\n" "Nodelist"
				    fi
					echo
					printf "$(gettext 'The %s resource group property can be changed using %s'.)\n\n\a" "Desired_primaries" "clresourcegroup(1M)"
					sc_prompt_pause
					continue
				fi

				# Get the name of the node/zone to remove
				if [[ ${ZONES_OK} -eq 1 ]]; then
				    prompt="$(gettext 'Select the name of the node/zone to remove from the list:')"
				else
				    prompt="$(gettext 'Select the name of the node to remove from the list:')"
				fi
				node="$(
				    sc_get_scrolling_menuoptions	\
					"${prompt}"			\
					"" ""				\
					0 1 1				\
					${nodelist}			\
				)"
				if [[ -z "${node}" ]]; then
					return 1
				fi

				# Update the rgnodelist
				nodelist=
				for foo in ${rgnodelist}
				do
					if [[ "${node}" != "${foo}" ]]; then
						nodelist="${nodelist} ${foo}"
					fi
				done
				rgnodelist=$(echo ${nodelist})
				;;

			quit)
				if [[ ${changed} -eq 0 ]]; then
					return 0
				fi
				;;

			esac

			# Print the nodelist and update, if changed
			if [[ "${rgnodelist}" != "${original_rgnodelist}" ]];  then
				((changed += 1))
			fi

			# Changes (either now or earlier), print list and update
			if [[ ${changed} -ne 0 ]]; then

				# Print the list
				(
					echo
					sc_print_para "$(printf "$(gettext 'Here is the new %s for \"%s\":')" "Nodelist" "${rgname}")"
					for node in ${rgnodelist}
					do
						printf "\t${node}\n"
					done
					echo
				) | more

				# Maybe they want to abort without a commit
				if [[ "${task}" == "quit" ]]; then
					prompt="$(printf "$(gettext 'Do you want to quit without updating the %s?')" "Nodelist")"
					answer=$(sc_prompt_yesno "${prompt}" "${NO}") || continue
					if [[ "${answer}" == "yes" ]]; then
						continue 2
					fi
				fi

				# Okay to continue?
				prompt="$(printf "$(gettext 'Are you ready to update the %s property now?')" "Nodelist")"
				answer=$(sc_prompt_yesno "${prompt}" "${YES}") || continue
				if [[ "${answer}" != "yes" ]]; then
					rgnodelist="${original_rgnodelist}"
					continue
				fi

				# Construct the new list
				nodelist=
				let entry_count=0
				for node in ${rgnodelist}
				do
					((entry_count += 1))
					if [[ -z "${nodelist}" ]]; then
						nodelist=${node}
					else
						nodelist="${nodelist},${node}"
					fi
				done

				# Initialize command set
				set -A CLSETUP_DO_CMDSET

				# Set nodelist
				if [[ ${set_associated_props} -eq 1 ]]; then
					CLSETUP_DO_CMDSET[0]="${CL_CMD_CLRESOURCEGROUP} set -n ${nodelist} -p Desired_primaries=${entry_count} -p Maximum_primaries=${entry_count} ${rgname}"
				else
					CLSETUP_DO_CMDSET[0]="${CL_CMD_CLRESOURCEGROUP} set -n ${nodelist} ${rgname}"
				fi

				# Doit
				clsetup_do_cmdset 1

				# Done with this RG
				continue 2
			fi
		done
	done

	return 0
}

#####################################################
#
# clsetup_menu_rgm_rg_change_rgdescription () rgname [help_only] [nohelp]
#
#	rgname		- name of the resource group to change.  If
#			    "help_only" is given, the rgname may be null.
#
#	help_only	- if this flag is given, just print the
#			    help message
#
#	nohelp		- if this flag is given, do not
#			    clear screen, print title, print help,
#			    or ask for confirmation to continue
#
#	Change the RG_Description property of a resource group.
#
#	Return:
#		zero		Completed
#		non-zero	Not completed
#
#####################################################
clsetup_menu_rgm_rg_change_rgdescription()
{
	typeset rgname=${1}
	typeset help_only=${2}
	typeset nohelp=${3}

	typeset sctxt_title="$(gettext '
		>>> Change the RG_description Resource Group Property <<<
	')"
	typeset sctxt_p1="$(gettext '
		The RG_description property of a resource group is a
		user-defined description of the group.  Use the
		RG_description menu option to change that description.
	')"

	typeset cachefile=${CLSETUP_RGM_CACHEFILE}

	typeset prompt
	typeset answer
	typeset rgdescription

	integer isset

	# Unless nohelp is specified, print help
	if [[ -z "${nohelp}" ]] && [[ -z "${help_only}" ]]; then

		# Clear screen, print title
		sc_print_title "${sctxt_title}"
		sc_print_para "${sctxt_p1}"

		# Okay to continue?
		clsetup_continue || return 1

	elif [[ -n "${help_only}" ]]; then
		sc_print_para "${sctxt_p1}"
		return 0
	fi


	# Loop until done or Ctrl-D is typed
	while true
	do
		let isset=0

		# Get the RG_description
		if [[ ${CLSETUP_ISMEMBER} -eq 1 ]] ||
		    [[ -r "${cachefile}" ]]; then
			rgdescription="$(clsetup_get_rgdescription "${rgname}" ${cachefile})"
		else
			rgdescription="DEBUG:  this is a description"
		fi

		# Print the current description
		echo
		if [[ -n "${rgdescription}" ]]; then
			let isset=1
			sc_print_para "$(printf "$(gettext 'The %s property for \"%s\" is currently set to the following:')" "RG_description" "${rgname}")"
			sc_print_prompt "$(gettext 'Description:')"
			echo "${rgdescription}"
			echo
		else
			sc_print_para "$(printf "$(gettext 'The %s property for \"%s\" is currently not set.')" "RG_description" "${rgname}")"
		fi

		# Get the new description
		sc_print_para "$(gettext 'Enter the new description.')"
		sc_print_prompt "$(gettext 'Description:')"
		read rgdescription || continue
		echo

		# If no description, confirm clearing it
		if [[ -z "${rgdescription}" ]]; then
			if [[ ${isset} -eq 0 ]]; then
				prompt="$(gettext 'Are you sure you do not want to set the description?')"
				answer=$(sc_prompt_yesno "${prompt}" "${YES}") || return 0
			else
				prompt="$(gettext 'Are you sure you want to clear the description?')"
				answer=$(sc_prompt_yesno "${prompt}" "${NO}") || return 0
			fi
			if [[ "${answer}" != "yes" ]]; then
				continue
			elif [[ ${isset} -eq 0 ]]; then
				return 0
			fi
		fi

		# Initialize command set
		set -A CLSETUP_DO_CMDSET

		# Set description
		CLSETUP_DO_CMDSET[0]="${CL_CMD_CLRESOURCEGROUP} set -p RG_description=\"${rgdescription}\" ${rgname}"

		# Doit
		clsetup_do_cmdset

		# Done
		break
	done

	return 0
}

#####################################################
#
# clsetup_menu_rgm_rg_change_pathprefix () rgname [help_only] [nohelp]
#
#	rgname		- name of the resource group to change.  If
#			    "help_only" is given, the rgname may be null.
#
#	help_only	- if this flag is given, just print the
#			    help message
#
#	nohelp		- if this flag is given, do not
#			    clear screen, print title, print help,
#			    or ask for confirmation to continue
#
#	Change the pathprefix property of a resource group.
#
#	Return:
#		zero		Completed
#		non-zero	Not completed
#
#####################################################
clsetup_menu_rgm_rg_change_pathprefix()
{
	typeset rgname=${1}
	typeset help_only=${2}
	typeset nohelp=${3}

	typeset sctxt_title="$(gettext '
		>>> Change the Pathprefix Resource Group Property <<<
	')"
	typeset sctxt_p1="$(gettext '
		The Pathprefix property of a resource group specifies a
		directory in a global file system that resources in the
		group can use for essential administrative files and
		other data.   Some types of resource (for example, HA
		for NFS) require a valid Pathprefix property setting.
		Use the Pathprefix menu option to change the Pathprefix
		property of a resource group.
	')"

	typeset cachefile=${CLSETUP_RGM_CACHEFILE}

	typeset prompt
	typeset answer
	typeset rgpathprefix
	typeset foo

	integer isset

	# Unless nohelp is specified, print help
	if [[ -z "${nohelp}" ]] && [[ -z "${help_only}" ]]; then

		# Clear screen, print title
		sc_print_title "${sctxt_title}"
		sc_print_para "${sctxt_p1}"

		# Okay to continue?
		clsetup_continue || return 1

	elif [[ -n "${help_only}" ]]; then
		sc_print_para "${sctxt_p1}"
		return 0
	fi

	# Loop until done or Ctrl-D is typed
	while true
	do
		let isset=0

		# Get the Pathprefix
		if [[ ${CLSETUP_ISMEMBER} -eq 1 ]] ||
		    [[ -r "${cachefile}" ]]; then
			rgpathprefix="$(clsetup_get_rgpathprefix "${rgname}" ${cachefile})"
		else
			rgpathprefix=
		fi

		# Print the current Pathprefix
		echo
		if [[ -n "${rgpathprefix}" ]]; then
			let isset=1
			sc_print_para "$(printf "$(gettext 'The %s property for \"%s\" is currently set to the following:')" "Pathprefix" "${rgname}")"
			sc_print_prompt "$(gettext 'Pathprefix:')"
			echo "${rgpathprefix}"
			echo
		else
			sc_print_para "$(printf "$(gettext 'The %s property for \"%s\" is currently not set.')" "Pathprefix" "${rgname}")"
		fi

		# Get the new Pathprefix
		while true
		do
			sc_print_prompt "$(gettext 'What is the name of the new \"Pathprefix\" directory?')"
			read rgpathprefix foo || continue 2
			echo

			# If not set, confirm clearing it
			if [[ -z "${rgpathprefix}" ]]; then
				if [[ ${isset} -eq 0 ]]; then
					prompt="$(gettext 'Are you sure you do not want to set the the \"Pathprefix\"?')"
					answer=$(sc_prompt_yesno "${prompt}" "${YES}") || return 0
				else
					prompt="$(gettext 'Are you sure you want to clear the \"Pathprefix\"?')"
					answer=$(sc_prompt_yesno "${prompt}" "${NO}") || return 0
				fi
				if [[ "${answer}" != "yes" ]]; then
					continue 2
				elif [[ ${isset} -eq 0 ]]; then
					return 0
				else
					break
				fi
			fi

			# Make sure it is just one word
			if [[ -n "${foo}" ]]; then
				echo "\a\c"
				continue
			fi

			# Make sure it starts with "/"
			if [[ "${rgpathprefix}" != /* ]]; then
				printf "$(gettext 'The name of the directory must begin with /.')\n\n\a"
				continue
			fi

			# Make sure it exists
			if [[ ${CLSETUP_ISMEMBER} -eq 1 ]] &&
			    [[ ! -d "${rgpathprefix}" ]]; then
				printf "$(gettext 'Directory not found - %s.')\n\n\a" "${rgpathprefix}"
				continue
			fi

			# Done
			break
		done

		# Initialize command set
		set -A CLSETUP_DO_CMDSET

		# Set pathprefix
		CLSETUP_DO_CMDSET[0]="${CL_CMD_CLRESOURCEGROUP} set -p Pathprefix=\"${rgpathprefix}\" ${rgname}"

		# Doit
		clsetup_do_cmdset

		# Done
		break
	done

	return 0
}

#####################################################
#
# clsetup_get_rg_changeproperties_menuoption()
#
#	Print the resource group change-properties menu, and return
#	the selected option.
#
#	This function always returns zero.
#
#####################################################
clsetup_get_rg_changeproperties_menuoption()
{
	typeset sctxt_title_2="$(gettext 'Select the property you want to change:')"
	typeset sctxt_option_001="Nodelist"
	typeset sctxt_option_002="RG_description"
	typeset sctxt_option_003="Pathprefix"
	typeset sctxt_option_help="$(gettext 'Help')"
	typeset sctxt_option_return="$(gettext 'Return to the resource group menu')"

	typeset option

	# Return
	option=$(sc_get_menuoption \
		"T2+++${sctxt_title_2}" \
		"S+0+1+${sctxt_option_001}" \
		"S+0+2+${sctxt_option_002}" \
		"S+0+3+${sctxt_option_003}" \
		"R+++" \
		"S+0+?+${sctxt_option_help}" \
		"S+0+q+${sctxt_option_return}" \
	)

	echo "${option}"

	return 0
}

#####################################################
#
# clsetup_menu_rgm_rg_changeproperties() [nohelp]
#
#	nohelp		- if this flag is given, do not
#			    clear screen, print title, print help,
#			    or ask for confirmation to continue
#
#	Change the selected properties of a resource group.
#
#	Return:
#		zero		Completed
#		non-zero	Not completed
#
#####################################################
clsetup_menu_rgm_rg_changeproperties()
{
	typeset nohelp=${1}

	typeset sctxt_title="$(gettext '
		>>> Change Properties of a Resource Group <<<
	')"
	typeset sctxt_title_help="$(gettext '
		*** Help Screen - Change Properties of a Resource Group ***
	')"
	typeset sctxt_p1="$(gettext '
		Use this option to change certain properties of a
		resource group.   These properties are the resource
		group Nodelist, RG_description, and Pathprefix.  Other
		resource group properties can be changed using clresourcegroup(1M).
	')"

	typeset rgname
	typeset rglist
	typeset prompt

	# Unless nohelp is specified, print help and get confirmation
	if [[ -z "${nohelp}" ]]; then

		# Clear screen, print title and help
		sc_print_title "${sctxt_title}"
		sc_print_para "${sctxt_p1}"

		# Okay to continue?
		clsetup_continue || return 1
	fi

	# Get the complete list of resource groups
	if [[ ${CLSETUP_ISMEMBER} -eq 1 ]]; then
		rglist="$(clsetup_get_rglist)"
	else
	    	rglist="rg1 rg2 rg3 rg4"
	fi

	# Make sure we have at least one group
	if [[ -z "${rglist}" ]]; then
		echo
		printf "$(gettext 'There are no resource groups configured.')\n\n\a"
		sc_prompt_pause
		break
	fi

	# Get the name of the group to change
	prompt="$(gettext 'Select the group whose property you want to change:')"
	rgname="$(
	    sc_get_scrolling_menuoptions	\
		"${prompt}"			\
		"" ""				\
		0 1 1				\
		${rglist}			\
	)"
	if [[ -z "${rgname}" ]]; then
		return 0
	fi

	# Loop until done or Ctrl-D is typed
	while true
	do
		# Get the menu option
		case $(clsetup_get_rg_changeproperties_menuoption) in
		'1')	clsetup_menu_rgm_rg_change_nodelist ${rgname} ;;
		'2')	clsetup_menu_rgm_rg_change_rgdescription ${rgname} ;;
		'3')	clsetup_menu_rgm_rg_change_pathprefix ${rgname} ;;

		'?')	# Help
			clear
			(
				sc_print_title "${sctxt_title_help}"
				clsetup_menu_rgm_rg_change_nodelist "" "help"
				clsetup_menu_rgm_rg_change_rgdescription "" "help"
				clsetup_menu_rgm_rg_change_pathprefix "" "help"
			) | more
			sc_prompt_pause
			continue
			;;

		'q')	break ;;
		esac

		# Done
		break
	done

	return 0
}

#####################################################
#
# clsetup_menu_rgm_rg_delete() [delete] [nohelp]
#
#	nohelp		- if this flag is given, do not
#			    clear screen, print title, print help,
#			    or ask for confirmation to continue
#
#	Delete a resource group.
#
#	Return:
#		zero		Completed
#		non-zero	Not completed
#
#####################################################
clsetup_menu_rgm_rg_delete()
{
	typeset delete=${1}
	typeset nohelp=${2}

	typeset sctxt_title="$(gettext '
		>>> Remove a Resource Group <<<
	')"
	typeset sctxt_p1="$(gettext '
		Use this option to remove a resource group from the
		cluster.
	')"
	typeset sctxt_p2="$(gettext '
		Before a resource group can be deleted, all resources
		within that group must first be disabled and removed.
		Once any resource is removed, that resource is no
		longer available to clients.  Clients who depend on
		those resources will experience a loss of service.
	')"

	typeset tmpfile=${CLSETUP_TMP_RGLIST}
	typeset cachefile=${CLSETUP_RGM_CACHEFILE}

	typeset answer
	typeset prompt
	typeset prompt2
	typeset rglist
	typeset rgname
	typeset rgdes
	typeset online_nodelist
	typeset rslist
	typeset sorted_list
	typeset resource
	typeset resource2
	typeset deps
	typeset foo
	typeset list
	typeset newlist
	typeset item

	integer commands=0
	integer index
	integer found
	integer count

	# Unless nohelp is specified, print help and get confirmation
	if [[ -z "${delete}" ]] && [[ -z "${nohelp}" ]]; then

		# Clear screen, print title and help
		sc_print_title "${sctxt_title}"
		sc_print_para "${sctxt_p1}"
		sc_print_para "${sctxt_p2}"

		# Okay to continue?
		clsetup_continue || return 1
	fi

	# Initialize
	prompt="$(gettext 'Select the resource group you want to remove:')"
	header1="$(printf "%-20s %-40.40s" "$(gettext 'Group Name')" "$(gettext 'Description')")"
	header2="$(printf "%-20s %-40.40s" "$(gettext '==========')" "$(gettext '===========')")"

	# Loop until done or Ctrl-D is typed
	while [[ -z "${delete}" ]]
	do
		# Get the list of resource groups
		if [[ ${CLSETUP_ISMEMBER} -eq 1 ]] || [[ -r "${cachefile}" ]];  then
			rglist="$(clsetup_get_rglist "" "" "" ${cachefile})"
		else
			rglist="rg1 rg2 rg3 rg4 rg5 rg6 rg7 rg8 rg9 rg10 rg11 rg12 rg13 rg14"
		fi

		# Verify that there are some resource groups
		if [[ -z "${rglist}" ]]; then
			echo
			printf "$(gettext 'There are no resource groups configured.')\n"
			printf "$(gettext 'So, there is nothing to remove.')\n\n\a"
			sc_prompt_pause
			return 1
		fi

		# Create a temp file which contains each RG with description
		rm -f ${tmpfile}
		for rgname in ${rglist}
		do
			if [[ ${CLSETUP_ISMEMBER} -eq 1 ]] ||
			    [[ -r "${cachefile}" ]];  then
				rgdes="$(clsetup_get_rgdescription ${rgname} ${cachefile})"
			else
				rgdes="DEBUG:  this is a description"
			fi
			printf "%-20s %-40.40s" ${rgname} "${rgdes}"
			if [[ ${#rgdes} -gt 40 ]]; then
				echo " ...\c"
			fi
			echo
		done >${tmpfile}

		# Get the name of the group
		rgname="$(
		    sc_get_scrolling_menuoptions	\
			"${prompt}"			\
			"${header1}"			\
			"${header2}"			\
			0 1 1				\
			:${tmpfile}			\
		)"
		rm -f ${tmpfile}
		if [[ -z "${rgname}" ]]; then
			return 0
		fi

		# Confirm
		prompt2="$(printf "$(gettext 'Are you sure you want to remove \"%s\"?')" "${rgname}")"
		answer=$(sc_prompt_yesno "${prompt2}" "${YES}") || return 1
		if [[ "${answer}" == "yes" ]]; then
			delete=${rgname}
		else
			prompt2="$(gettext 'Do you want to select a different group to remove?')"
			answer=$(sc_prompt_yesno "${prompt2}" "${NO}") || return 1
			if [[ "${answer}" != "yes" ]]; then
				return 1
			fi
		fi
	done

	# Get the resource list for this group
	if [[ ${CLSETUP_ISMEMBER} -eq 1 ]]; then
		rslist="$(${SCHA_RG_GET} -G ${delete} -O resource_list)"
	else
		rslist="${delete}res1 ${delete}res2 ${delete}res3 ${delete}res4"
	fi

	# Disable all enabled resources in this group
	if [[ ${CLSETUP_ISMEMBER} -eq 1 ]]; then
		clsetup_disable_resources ${delete}
		if [[ $? -ne 0 ]]; then
			printf "$(gettext 'All resources must be disabled before they can be removed.')\n\n\a"
			printf "$(gettext 'And all resources must be removed from \"%s\" before it can be removed.')\n\n\a" "${delete}"
			sc_prompt_pause
			return 1
		fi
		echo
	fi

	#
	# Remove all resources
	#
	for resource in ${rslist}
	do
		# Remove the resource
		clsetup_menu_rgm_rs_delete ${resource}
		if [[ $? -ne 0 ]]; then
			echo
			printf "$(gettext 'All resources must be removed from \"%s\" before it can be removed.')\n\n\a" "${delete}"
			sc_prompt_pause
			return 1
		fi
	done

	# Cleanup RG_dependencies
	set -A CLSETUP_DO_CMDSET
	let index=0
	if [[ ${CLSETUP_ISMEMBER} -eq 1 ]]; then
		for rgname in ${rglist}
		do
			# We can skip ourselves
			if [[ "${rgname}" == "${delete}" ]]; then
				continue
			fi

			# RG_dependencies
			newlist=
			list="$(${SCHA_RG_GET} -G ${rgname} -O rg_dependencies)"
			let found=0
			for item in ${list}
			do
				if [[ "${item}" == "${delete}" ]]; then
					let found=1
					continue
				fi
				if [[ -n "${newlist}" ]]; then
					newlist="${newlist},"
				fi
				newlist="${newlist}${item}"
			done
			if [[ ${found} -eq 1 ]]; then
				CLSETUP_DO_CMDSET[index]="${CL_CMD_CLRESOURCEGROUP} set -p RG_dependencies=${newlist} ${rgname}"
				((index += 1))
			fi

			# RG_affinities
			newlist=
			list="$(${SCHA_RG_GET} -G ${rgname} -O rg_affinities)"
			let found=0
			for item in ${list}
			do
				if [[ "${item}" == "+${delete}" ]]; then
					let found=1
					continue
				fi
				if [[ "${item}" == "++${delete}" ]]; then
					let found=1
					continue
				fi
				if [[ "${item}" == "+++${delete}" ]]; then
					let found=1
					continue
				fi
				if [[ "${item}" == "-${delete}" ]]; then
					let found=1
					continue
				fi
				if [[ "${item}" == "--${delete}" ]]; then
					let found=1
					continue
				fi
				if [[ -n "${newlist}" ]]; then
					newlist="${newlist},"
				fi
				newlist="${newlist}${item}"
			done
			if [[ ${found} -eq 1 ]]; then
				CLSETUP_DO_CMDSET[index]="${CL_CMD_CLRESOURCEGROUP} set -p RG_affinities=${newlist} ${rgname}"
				((index += 1))
			fi


		done
	fi
	if [[ ${index} -gt 0 ]]; then
		# increment commands counter
		((commands += index))

		# Do not re-confirm.  Also, ignore return code
		clsetup_do_cmdset 1 1

		# Re-initialize
		set -A CLSETUP_DO_CMDSET
		let index=0
	fi

	# Let's see if the group is still online
	if [[ ${CLSETUP_ISMEMBER} -eq 1 ]]; then
		online_nodelist="$(clsetup_get_rgstate ${rg} "" "not_offline")"
	else
		online_nodelist=node1
	fi

	# If it is not already offline on all nodes, attempt to bring it offline
	if [[ -n "${online_nodelist}" ]]; then
		# increment commands counter
		((commands += 1))

		# Set up the comand to bring the group offline
		set -A CLSETUP_DO_CMDSET
		CLSETUP_DO_CMDSET[0]="${CL_CMD_CLRESOURCEGROUP} offline ${delete}"

		# Do not re-confirm.  Also, ignore return code
		clsetup_do_cmdset 1 1
	fi


	# Add the command to remove the resource group
	CLSETUP_DO_CMDSET[index]="${CL_CMD_CLRESOURCEGROUP} delete ${delete}"

	# Do it!
	if [[ ${commands} -gt 0 ]]; then
		clsetup_do_cmdset 1 "" ${commands} || return 1
	else
		clsetup_do_cmdset 1 || return 1
	fi

	return 0
}

#####################################################
#
# clsetup_get_rg_online_offline_menuoption() rgname rgstate maxprimaries
#
#	rgname		- name of the resource group
#
#	rgstate		- overall state of the resource group
#
#	maxprimaries	- maximum number of primaries for the resource group
#
#	Print the resource group online-offline menu, and return the
#	selected option.
#
#	If "rgstate" is "unmanaged", the following menu is used:
#
#		1)	Put the group in the managed state
#
#	If "rgstate" is "offline", the following:
#
#		1)	Bring the group online
#		2)	Put the group into an unmanaged state
#
#	If "rgstate" is "error--stop_failed", the following:
#
#		1)	Bring the group offline from all cluster nodes
#
#	If "rgstate" is "online" & "maxprimaries" < 2
#
#		1)	Switch ownership of the group
#		2)	Bring the group offline from all cluster nodes
#		3)	Put the group into an unmanaged state
#
#	If "rgstate" is "online" & "maxprimaries" >= 2
#
#		1)	Change the set of current primaries
#		2)	Bring the group offline from all cluster nodes
#		3)	Put the group into an unmanaged state
#
#	Returned options are:
#
#		1)	Bring the group online
#		2)	Bring the group offline from all cluster nodes
#		3)	Put the group into an unmanaged state
#		4)	Switch ownership of the group
#		5)	Change the set of current primaries
#		6)	Put the group into a managed state
#
#	This function always returns zero.
#
#####################################################
clsetup_get_rg_online_offline_menuoption()
{
	typeset rgname=${1}
	typeset rgstate=${2}
	typeset maxprimaries=${3}

	typeset sctxt_title_2="$(gettext 'Please select from one of the following options:')"
	typeset sctxt_option_managed="$(gettext 'Put the group into a managed state')"
	typeset sctxt_option_unmanaged="$(gettext 'Put the group into an unmanaged state')"
	typeset sctxt_option_switch="$(gettext 'Switch ownership of the group')"
	typeset sctxt_option_change="$(gettext 'Change the set of current primaries')"
	typeset sctxt_option_offline="$(gettext 'Bring the group offline from all cluster nodes')"
	typeset sctxt_option_online="$(gettext 'Bring the group online')"

	typeset sctxt_option_quit="$(gettext 'Quit')"

	typeset option

	case ${rgstate} in
	unmanaged)
		option=$(sc_get_menuoption \
			"T2+++${sctxt_title_2}" \
			"S+0+1+${sctxt_option_managed}" \
			"R+++" \
			"S+0+q+${sctxt_option_quit}" \
		)
		case ${option} in
		1)	option=6 ;;
		esac
		;;

	offline)
		option=$(sc_get_menuoption \
			"T2+++${sctxt_title_2}" \
			"S+0+1+${sctxt_option_online}" \
			"S+0+2+${sctxt_option_unmanaged}" \
			"R+++" \
			"S+0+q+${sctxt_option_quit}" \
		)
		case ${option} in
		1)	option=1 ;;
		2)	option=3 ;;
		esac
		;;

	error--stop_failed)
		option=$(sc_get_menuoption \
			"T2+++${sctxt_title_2}" \
			"S+0+1+${sctxt_option_offline}" \
			"R+++" \
			"S+0+q+${sctxt_option_quit}" \
		)
		case ${option} in
		1)	option=2 ;;
		esac
		;;

	online)
		if [[ ${maxprimaries} -lt 2 ]]; then
			option=$(sc_get_menuoption \
				"S+0+1+${sctxt_option_switch}" \
				"S+0+2+${sctxt_option_offline}" \
				"S+0+3+${sctxt_option_unmanaged}" \
				"R+++" \
				"S+0+q+${sctxt_option_quit}" \
			)
			case ${option} in
			1)	option=4 ;;
			2)	option=2 ;;
			3)	option=3 ;;
			esac
		else
			option=$(sc_get_menuoption \
				"S+0+1+${sctxt_option_change}" \
				"S+0+2+${sctxt_option_offline}" \
				"S+0+3+${sctxt_option_unmanaged}" \
				"R+++" \
				"S+0+q+${sctxt_option_quit}" \
			)
			case ${option} in
			1)	option=5 ;;
			2)	option=2 ;;
			3)	option=3 ;;
			esac
		fi
		;;
	esac

	echo "${option}"

	return 0
}

#####################################################
#
# clsetup_menu_rgm_rg_online_offline() [nohelp]
#
#	nohelp		- if this flag is given, do not
#			    clear screen, print title, print help,
#			    or ask for confirmation to continue
#
#	Online/offline a resource group.
#
#	Return:
#		zero		Completed
#		non-zero	Not completed
#
#####################################################
clsetup_menu_rgm_rg_online_offline()
{
	typeset nohelp=${1}

	typeset sctxt_title="$(gettext '
		>>> Online/Offline or Switchover a Resource Group <<<
	')"
	typeset sctxt_p1="$(gettext '
		Use this option to bring a resource group online or
		offline on one or more cluster nodes.  For failover
		resource groups which are already online, use this
		option to switch the primary owner of a group.  For
		scalable groups which are already online, use it to
		change the set of current primaries.
	')"
	typeset sctxt_p2="$(gettext '
		This option is also used to control the managed and
		unmanaged state of a resource group.
	')"
	typeset sctxt_p3="$(gettext '
		Once a resource group is brought online, all enabled
		resources in that group become available to clients.
	')"
	typeset sctxt_p4="$(gettext '
		Once a resource group is taken offline from all cluster
		nodes, the resources in that group are no longer
		available to clients of those resources.
	')"
	typeset sctxt_managed_p1="$(gettext '
		Once a resource group is brought from an unmanaged
		state to the managed state, it can then be brought
		online.
	')"
	typeset sctxt_offline_warning_p1="$(gettext '
		Remember that once a resource group is taken offline,
		the resources in that group are no longer available to
		clients of those resources.  Clients who depend on
		those resources will experience a loss of service.
	')"
	typeset sctxt_unmanage_p1="$(gettext '
		Before putting a resource group into an unmanaged state,
		it is first taken offline from all cluster nodes.
	')"
	typeset sctxt_unmanage_p2="$(gettext '
		A resource group can also be put into an unmanaged state.
		Once a resource group is unmanaged, it must be changed
		back to the managed state before it can be brought online.
	')"
	typeset sctxt_unmanage_p3="$(gettext '
		A resource group cannot be unmanaged until all of the
		resources within that group are first disabled.
	')"
	typeset sctxt_unmanage_p4="$(gettext '
		Once a resource group is unmanaged, it must be changed
		back to the managed state before it can be brought online.
	')"
	typeset sctxt_switch_p1="$(gettext '
		Switching primary ownership of a failover resource group
		typically results in a brief outage of service for all
		resources in the group.
	')"

	typeset tmpfile=${CLSETUP_TMP_RGLIST}
	typeset cachefile=${CLSETUP_RGM_CACHEFILE}
	typeset rgstatefile=${CLSETUP_TMP_RGSTATE}

	typeset prompt
	typeset header1
	typeset header2
	typeset answer
	typeset rglist
	typeset -l lrgstatelist		# lower case
	typeset rgstatelist
	typeset rgname
	typeset rgtype
	typeset rgstate
	typeset -l lrgstate		# lower case
	typeset -l lmainstate		# lower case
	typeset -l newstate		# lower case
	typeset -l laststate		# lower case
	typeset resource
	typeset rslist
	typeset deps
	typeset deps2
	typeset dep
	typeset -l lnodestate		# lower case
	typeset node
	typeset nodelist
	typeset online_nodelist
	typeset foo
	typeset foo2
	typeset -l lsuspended		# lower case
	typeset rgsuspended

	integer found
	integer index
	integer commands
	integer maxprimaries
	integer count

	# Unless nohelp is specified, print help and get confirmation
	if [[ -z "${nohelp}" ]]; then

		# Clear screen, print title and help
		sc_print_title "${sctxt_title}"
		sc_print_para "${sctxt_p1}"
		sc_print_para "${sctxt_p2}"
		sc_print_para "${sctxt_p3}"
		sc_print_para "${sctxt_p4}"

		# Okay to continue?
		clsetup_continue || return 1
	fi

	# Loop until done or Ctrl-D is typed
	while true
	do
		# Get the list of resource groups
		if [[ ${CLSETUP_ISMEMBER} -eq 1 ]] ||
		    [[ -r "${cachefile}" ]];  then
			rglist="$(clsetup_get_rglist "" "" "" ${cachefile})"
		else
			rglist="rg1 rg2 rg3 rg4 rg5 rg6 rg7 rg8 rg9 rg10 rg11 rg12 rg13 rg14"
		fi

		# Create the rgstate file
		if [[ ${CLSETUP_ISMEMBER} -eq 1 ]]; then
			clsetup_create_rgstatefile ${rgstatefile}
		fi

		# Create the temp file
		rm -f ${tmpfile}
		let found=0
		for rgname in ${rglist}
		do
			if [[ ${CLSETUP_ISMEMBER} -eq 1 ]];  then
				lrgstate=
				lmainstate=
				lrgstatelist=" $(clsetup_get_rgstate ${rgname} "${rgstatefile}" "allnodes")"
				# Unmanaged?
				expr "${lrgstatelist}" : '.*\(unmanaged\)' >/dev/null && lmainstate="unmanaged"

				# Stop_Failed anywhere?
				if [[ -z "${lmainstate}" ]]; then
					expr "${lrgstatelist}" : '.*\(error--stop_failed\)' >/dev/null && lmainstate="error--stop_failed"
				fi

				# Online anywhere?
				if [[ -z "${lmainstate}" ]]; then
					expr "${lrgstatelist}" : '.*\([^_]online\)' >/dev/null && lmainstate="online"
				fi

				# Must be offline
				if [[ -z "${lmainstate}" ]]; then
					lmainstate=offline
				fi
				lrgstate=${lmainstate}
				rgstate=$(clsetup_rgstate_string ${lrgstate})

				# If mainstate is online, don't add more offline
				if [[ "${lmainstate}" == "online" ]]; then
					foo2=
				else
					foo2=offline
				fi

				# Add any additional state information
				for foo in online pending_online ${foo2} pending_offline unmanaged error--stop_failed
				do
					if [[ "${foo}" == "${lmainstate}" ]]; then
						continue
					fi
					lrgstate=
					if [[ "${foo}" == "online" ]] ||
					    [[ "${foo}" == "offline" ]]; then
						expr "${lrgstatelist}" : '.*\([^_]'${foo}'\)' >/dev/null && lrgstate=${foo}
					else
						expr "${lrgstatelist}" : '.*\('${foo}'\)' >/dev/null && lrgstate=${foo}
					fi
					if [[ -n "${lrgstate}" ]]; then
						rgstate="${rgstate}, $(clsetup_rgstate_string ${lrgstate})"
					fi
				done

				rgtype="$(clsetup_get_rgtype ${rgname})"

				# Suspended?
				rgsuspended="$(gettext 'No')"
				lsuspended="$(${SCHA_RG_GET} -G ${rgname} -O Suspend_automatic_recovery)"
				if [[ "${lsuspended}" == "true" ]]; then
					rgsuspended="$(gettext 'Yes')"
				fi
			else
				case ${rgname} in
				rg1)		rgstate="Unmanaged" ;;
				rg2)		rgstate="Online" ;;
				rg3)		rgstate="Pending_Online" ;;
				rg4)		rgstate="Offline" ;;
				rg5)		rgstate="Pending_Offline" ;;
				rg6)		rgstate="Stop_Failed" ;;
				*)		rgstate="Online" ;;
				esac

				rgtype="failover"
				rgsuspended="$(gettext 'No')"
			fi
			((found += 1))
			printf "%-20s %-10.10s %-10.10s %s\n" "${rgname}" "${rgtype}" "${rgsuspended}" "${rgstate}"
		done > ${tmpfile}

		# Done with rgstate file
		if [[ ${CLSETUP_ISMEMBER} -eq 1 ]]; then
			clsetup_rm_rgstatefile ${rgstatefile}
		fi

		# Are there any resource groups?
		if [[ ${found} -lt 1 ]]; then
			echo
			printf "$(gettext 'There are no resource groups configured.')\n"
			printf "$(gettext 'So, there is nothing to online/offline.')\n\n\a"
			sc_prompt_pause
			rm -f ${tmpfile}
			return 1
		fi

		# Initialize headers
		prompt="$(gettext 'Select the resource group you want to change:')"
		header1="$(printf "%-20s %-10.10s %-10.10s %s" "$(gettext 'Group Name')" "$(gettext 'Type')" "$(gettext 'Suspended')" "$(gettext 'State(s)')")"
		header2="$(printf "%-20s %-10.10s %-10.10s %s" "==========" "====" "=========" "========")"

		# Get the name of the resource group to change
		answer="$(
		    sc_get_scrolling_menuoptions	\
			"${prompt}"			\
			"${header1}"			\
			"${header2}"			\
			0 1 4				\
			:${tmpfile}			\
		)"
		rm -f ${tmpfile}

		# Done?
		if [[ -z "${answer}" ]]; then
			return 0
		fi

		# Break out rgname and rgstate
		rgname=$(IFS=: ; set -- ${answer}; echo ${1})
		rgstate=$(IFS=:, ; set -- ${answer}; echo ${4})
		lmainstate=$(clsetup_string_rgstate ${rgstate})

		# Get Maximum_primaries
		if [[ ${CLSETUP_ISMEMBER} -eq 1 ]]; then
			maxprimaries="$(${SCHA_RG_GET} -G ${rgname} -O maximum_primaries)"
		else
			maxprimaries=1
		fi

		#
		# Get Online/offline action for rg
		#
		# Returned menu options are:
		#
		# 1)	Bring the group online
		# 2)	Bring the group offline from all cluster nodes
		# 3)	Put the group into an unmanaged state
		# 4)	Switch ownership of the group
		# 5)	Change the set of current primaries
		# 6)	Put the group into a managed state
		#
		newstate=
		laststate=
		case $(clsetup_get_rg_online_offline_menuoption "${rgname}" "${lmainstate}" "${maxprimaries}") in
		1)	newstate="online" ;;
		2)	newstate="offline" ;;
		3)	newstate="unmanaged" ;;
		4)	newstate="switch" ;;
		5)	newstate="change" ;;
		6)	newstate="managed" ;;
		*)	continue ;;
		esac

		# Online/offline/manage/unmanage/switch the rg
		while [[ -n "${newstate}" ]]
		do
			case ${newstate} in
			online)
				# Reset the "newstate"
				newstate=

				# Okay to continue?
				if [[ "${laststate}" == "managed" ]]; then
					prompt="$(printf "$(gettext 'Would you also like to bring \"%s\" online?')" "${rgname}")"
				else
					prompt="$(printf "$(gettext 'Are you sure you want to bring \"%s\" online?')" "${rgname}")"
				fi
				laststate=
				answer=$(sc_prompt_yesno "${prompt}" "${YES}") || break
				if [[ "${answer}" != "yes" ]]; then
					break
				fi

				# Check RG_dependencies
				deps=
				let found=0
				if [[ ${CLSETUP_ISMEMBER} -eq 1 ]]; then
					deps="$(clsetup_get_rg_dependencies ${rgname} "offline")"
				fi
				for dep in ${deps}
				do
					deps2="$(clsetup_get_rg_dependencies ${dep} "offline")"
					if [[ -n "${deps2}" ]]; then
						printf "$(gettext 'Resource group \"%s\" depends upon \"%s\".')\n" "${rgname}" "${dep}"
						printf "$(gettext 'And \"%s\", in turn, has other dependencies.')\n" "${dep}"
						((found += 1))
					fi
				done
				if [[ ${found} -ne 0 ]]; then
					printf "$(gettext 'Cannot bring \"%s\" online.')\n\n\a" "${rgname}"
					sc_prompt_pause
					break
				fi

				# Initialize command set
				set -A CLSETUP_DO_CMDSET
				let index=0

				# Set up commands
				for dep in ${deps}
				do
					foo="$(printf "$(gettext 'Resource group \"%s\" depends upon \"%s\".')" "${rgname}" "${dep}")"
					sc_print_para "${foo}"
					prompt="$(printf "$(gettext 'Do you also want to bring \"%s\" online?')" "${dep}")"
					answer=$(sc_prompt_yesno "${prompt}" "${YES}") || break 2
					if [[ "${answer}" == "yes" ]]; then
						CLSETUP_DO_CMDSET[index]="${CL_CMD_CLRESOURCEGROUP} online -eM ${dep}"
						((index += 1))
					else
						printf "$(gettext 'Cannot bring \"%s\" online.')\n\n\a" "${rgname}"
						sc_prompt_pause
						break 2
					fi
				done

				# Online command
				CLSETUP_DO_CMDSET[index]="${CL_CMD_CLRESOURCEGROUP} online -eM ${rgname}"

				# Doit
				clsetup_do_cmdset 1
				;;

			managed)
				# Reset the "newstate"
				newstate=

				# Print help
				sc_print_para "${sctxt_managed_p1}"

				# Okay to continue?
				prompt="$(printf "$(gettext 'Are you sure you want the %s to manage \"%s\"?')" "RGM" "${rgname}")"
				answer=$(sc_prompt_yesno "${prompt}" "${YES}") || break
				if [[ "${answer}" != "yes" ]]; then
					break
				fi

				# Check RG_dependencies
				deps=
				let found=0
				if [[ ${CLSETUP_ISMEMBER} -eq 1 ]]; then
					deps="$(clsetup_get_rg_dependencies ${rgname} "unmanaged")"
				fi
				for dep in ${deps}
				do
					deps2="$(clsetup_get_rg_dependencies ${dep} "unmanaged")"
					if [[ -n "${deps2}" ]]; then
						printf "$(gettext 'Resource group \"%s\" depends upon \"%s\".')\n" "${rgname}" "${dep}"
						printf "$(gettext 'And \"%s\", in turn, has other dependencies.')\n" "${dep}"
						((found += 1))
					fi
				done
				if [[ ${found} -ne 0 ]]; then
					printf "$(gettext 'Cannot manage \"%s\".')\n\n\a" "${rgname}"
					sc_prompt_pause
					break
				fi

				# Initialize command set
				set -A CLSETUP_DO_CMDSET
				let index=0

				# Set up commands
				for dep in ${deps}
				do
					foo="$(printf "$(gettext 'Resource group \"%s\" depends upon \"%s\".')" "${rgname}" "${dep}")"
					sc_print_para "${foo}"
					prompt="$(printf "$(gettext 'Do you also want to manage \"%s\"?')" "${dep}")"
					answer=$(sc_prompt_yesno "${prompt}" "${YES}") || break 2
					if [[ "${answer}" == "yes" ]]; then
						CLSETUP_DO_CMDSET[index]="${CL_CMD_CLRESOURCEGROUP} manage ${dep}"
						((index += 1))
					else
						printf "$(gettext 'Cannot manage \"%s\".')\n\n\a" "${rgname}"
						sc_prompt_pause
						break 2
					fi
				done

				# Manage command
				CLSETUP_DO_CMDSET[index]="${CL_CMD_CLRESOURCEGROUP} manage ${rgname}"

				# Doit
				clsetup_do_cmdset 1 || break

				# See if the want to bring it online
				laststate="managed"
				newstate="online"
				continue
				;;

			offline)
				# Reset the "newstate"
				newstate=

				# Print offline warning
				sc_print_para "${sctxt_offline_warning_p1}"

				# Okay to continue?
				prompt="$(printf "$(gettext 'Are you sure you want to take \"%s\" offline?')" "${rgname}")"
				answer=$(sc_prompt_yesno "${prompt}" "${YES}") || break
				if [[ "${answer}" != "yes" ]]; then
					break
				fi

				# Look for other RG_dependencies
				deps=
				let found=0
				if [[ ${CLSETUP_ISMEMBER} -eq 1 ]]; then
					deps="$(clsetup_get_rg_depended_on ${rgname} "online")"
				fi
				for dep in ${deps}
				do
					deps2="$(clsetup_get_rg_depended_on ${dep} "online")"
					if [[ -n "${deps2}" ]]; then
						printf "$(gettext 'Resource group \"%s\" is depended upon by \"%s\".')\n" "${rgname}" "${dep}"
						printf "$(gettext 'And \"%s\", in turn, has other dependencies.')\n" "${dep}"
						((found += 1))
					fi
				done
				if [[ ${found} -ne 0 ]]; then
					printf "$(gettext 'Cannot take \"%s\" offline.')\n\n\a" "${rgname}"
					sc_prompt_pause
					break
				fi

				# Initialize command set
				set -A CLSETUP_DO_CMDSET
				let index=0

				# Set up commands
				for dep in ${deps}
				do
					foo="$(printf "$(gettext 'Resource group \"%s\" is depended upon by \"%s\".')" "${rgname}" "${dep}")"
					sc_print_para "${foo}"
					prompt="$(printf "$(gettext 'Do you also want to take \"%s\" offline?')" "${dep}")"
					answer=$(sc_prompt_yesno "${prompt}" "${YES}") || break 2
					if [[ "${answer}" == "yes" ]]; then
						CLSETUP_DO_CMDSET[index]="${CL_CMD_CLRESOURCEGROUP} offline ${dep}"
						((index += 1))
					else
						printf "$(gettext 'Cannot take \"%s\" offline.')\n\n\a" "${rgname}"
						sc_prompt_pause
						break 2
					fi
				done

				# Offline command
				CLSETUP_DO_CMDSET[index]="${CL_CMD_CLRESOURCEGROUP} offline ${rgname}"
				# Doit
				clsetup_do_cmdset 1 || break

				# Initialize command set
				set -A CLSETUP_DO_CMDSET
				let index=0

				# Unmanage?
				if [[ "${laststate}" != "unmanaged" ]]; then
					sc_print_para "${sctxt_unmanage_p2}"

					prompt="$(printf "$(gettext 'Do you want to unmanage \"%s\"?')" "${rgname}")"
					foo="${NO}"
				else
					laststate=
					prompt="$(printf "$(gettext 'Do you still want to unmanage \"%s\"?')" "${rgname}")"
					foo="${YES}"
				fi
				answer=$(sc_prompt_yesno "${prompt}" "${foo}") || break
				if [[ "${answer}" == "yes" ]]; then
					newstate="unmanaged2"
					continue
				fi
				;;

			unmanaged)
				# Reset "newstate" to the next state
				laststate=${newstate}
				if [[ "${lmainstate}" == "online" ]]; then
					sc_print_para "${sctxt_unmanage_p1}"
					newstate="offline"
					continue
				else
					newstate="unmanaged2"
					continue
				fi
				;;

			unmanaged2)
				# Reset the "newstate"
				newstate=

				# Unmanage?
				if [[ "${laststate}" == "unmanaged" ]]; then
					# Clear laststate
					laststate=

					# Print message
					sc_print_para "${sctxt_unmanage_p4}"

					# Okay to continue?
					prompt="$(printf "$(gettext 'Are you sure you want to unmanage \"%s\"?')" "${rgname}")"
					answer=$(sc_prompt_yesno "${prompt}" "${YES}") || break
					if [[ "${answer}" != "yes" ]]; then
						break
					fi
				fi

				# If anything enabled, disable them?
				rslist=
				if [[ ${CLSETUP_ISMEMBER} -eq 1 ]]; then
					rslist="$(${SCHA_RG_GET} -G ${rgname} -O resource_list)"
					rslist="$(clsetup_get_enabled_resources ${rslist})"
				fi

				# Disable enabled resources
				let commands=0
				if [[ -n "${rslist}" ]]; then

					# Print the list
					foo="$(gettext 'The following resource(s) are still enabled:')"
					(
						sc_print_para "${sctxt_unmanage_p3}"
						sc_print_para "${foo}"
						for resource in ${rslist}
						do
							printf "\t${resource}\n"
						done
						echo
					) | more

					# Okay to disable?
					prompt="$(printf "$(gettext 'Is it okay to disable these resource(s) first?')" "${rgname}")"
					answer=$(sc_prompt_yesno "${prompt}" "${YES}") || break
					if [[ "${answer}" != "yes" ]]; then
						printf "$(gettext 'These resource(s) must all be disabled before \"%s\" can be unmanaged.')\n\n\a" "${rgname}"
						break
					fi

					# Disable all resources in the group
					clsetup_disable_resources ${rgname}
					if [[ $? -ne 0 ]]; then
						printf "$(gettext 'All resources must be disabled before \"%s\" can be unmanaged.')\n\n\a" "${rgname}"
						break
					fi
					echo
					((commands += 1))
				fi

				# Look for unmanaged dependencies
				deps=
				let found=0
				if [[ ${CLSETUP_ISMEMBER} -eq 1 ]]; then
					deps="$(clsetup_get_rg_depended_on ${rgname})"
				fi
				for dep in ${deps}
				do
					deps2="$(clsetup_get_rg_depended_on ${dep})"
					if [[ -n "${deps2}" ]]; then
						printf "$(gettext 'Resource group \"%s\" depends upon \"%s\" being unmanaged.')\n" "${dep}" "${rgname}"
						printf "$(gettext 'And \"%s\", in turn, has other unmanaged dependencies.')\n" "${dep}"
						((found += 1))
					fi
				done
				if [[ ${found} -ne 0 ]]; then
					printf "$(gettext 'Cannot unmanage \"%s\".')\n\n\a" "${rgname}"
					sc_prompt_pause
					break
				fi

				# Initialize command set
				set -A CLSETUP_DO_CMDSET
				let index=0

				# Set up commands
				for dep in ${deps}
				do
					foo="$(printf "$(gettext 'Resource group \"%s\" depends upon \"%s\" being unmanaged.')" "${rgname}" "${dep}")"
					sc_print_para "${foo}"
					prompt="$(printf "$(gettext 'Do you also want to unmanage \"%s\"?')" "${dep}")"
					answer=$(sc_prompt_yesno "${prompt}" "${YES}") || break 2
					if [[ "${answer}" == "yes" ]]; then
						CLSETUP_DO_CMDSET[index]="${CL_CMD_CLRESOURCEGROUP} unmanage ${dep}"
						((index += 1))
						((commands += 1))
					else
						printf "$(gettext 'Cannot unmanage \"%s\".')\n\n\a" "${rgname}"
						sc_prompt_pause
						break 2
					fi
				done

				# Unmanage command
				CLSETUP_DO_CMDSET[index]="${CL_CMD_CLRESOURCEGROUP} unmanage ${rgname}"

				# Doit
				if [[ ${commands} -gt 0 ]]; then
					clsetup_do_cmdset 1 "" ${commands}
				else
					clsetup_do_cmdset 1
				fi
				;;

			switch)
				# Reset the "newstate"
				newstate=

				if [[ ${CLSETUP_ISMEMBER} -eq 1 ]]; then
					foo=
					online_nodelist="$(clsetup_get_rgstate ${rgname} "" "online")"
					nodelist="$(${SCHA_RG_GET} -G ${rgname} -O nodelist)"
					for node in ${nodelist}
					do
						for foo2 in ${online_nodelist}
						do
							if [[ "${foo2}" == "${node}" ]]; then
								continue 2
							fi
						done
						lnodestate="$(${SCHA_CLUSTER_GET} -O nodestate_node ${node})"
						if [[ "${lnodestate}" == "up" ]]; then
							foo="${foo} ${node}"
						fi
					done
					nodelist=$(echo ${foo})
				else
					nodelist="node2"
				fi

				# Anybody ready to take ownership?
				if [[ -z "${nodelist}" ]]; then
					echo
					printf "$(gettext 'There are no available nodes to take ownership of \"%s\".')\n\n\a" "${rgname}"
					sc_prompt_pause
					break
				fi

				# Menu of available nodes
				prompt="$(printf "$(gettext 'Select the node to take ownership of \"%s\":')" "${rgname}")"
				node="$(
					sc_get_scrolling_menuoptions	\
					"${prompt}"			\
					"" ""				\
					0 1 1				\
					${nodelist}			\
				)"
				if [[ -z "${node}" ]]; then
					break
				fi

				# Print warning message
				sc_print_para "${sctxt_switch_p1}"

				# Okay to continue?
				prompt="$(printf "$(gettext 'Are you sure you want to switch \"%s\"?')" "${rgname}")"
				answer=$(sc_prompt_yesno "${prompt}" "${YES}") || break
				if [[ "${answer}" != "yes" ]]; then
					break
				fi

				# Initialize command set
				set -A CLSETUP_DO_CMDSET
				let index=0

				# Manage command
				CLSETUP_DO_CMDSET[index]="${CL_CMD_CLRESOURCEGROUP} switch -n ${node} ${rgname}"

				# Doit
				clsetup_do_cmdset 1
				;;

			change)
				# Reset the "newstate"
				newstate=

				# Create the temp file
				rm -f ${tmpfile}
				if [[ ${CLSETUP_ISMEMBER} -eq 1 ]];  then
					rgstatelist="$(clsetup_get_rgstate ${rgname})"
				else
					rgstatelist="node2:offline node3:offline node4:online"
				fi
				for foo in ${rgstatelist}
				do
					node=$(expr "${foo}" : '\(.*\):')
					lrgstate=$(expr "${foo}" : '.*:\(.*\)')
					rgstate=$(clsetup_rgstate_string ${lrgstate})
					printf "%-25s %s\n" "${node}" "${rgstate}"
				done > ${tmpfile}

				# Initialize headers
				prompt="$(gettext 'Select the node on which you want to change the group state:')"
				header1="$(printf "%-25s %s" "$(gettext 'Node Name')" "$(gettext 'Resource Group State')")"
				header2="$(printf "%-25s %s" "=========" "====================")"

				# Get the name of the node to change
				node="$(
				    sc_get_scrolling_menuoptions	\
					"${prompt}"			\
					"${header1}"			\
					"${header2}"			\
					0 1 1				\
					:${tmpfile}			\
				)"
				rm -f ${tmpfile}

				# Done?
				if [[ -z "${node}" ]]; then
					break
				fi

				# Get the state of the node
				if [[ ${CLSETUP_ISMEMBER} -eq 1 ]];  then
					lrgstate="$(clsetup_get_rgstate ${rgname} "" "" "${node}")"
				else
					case ${node} in
					node2)	lrgstate=offline ;;
					node3)	lrgstate=offline ;;
					node4)	lrgstate=online  ;;
					*)	lrgstate=offline ;;
					esac
				fi

				# What do we want to do?
				case ${lrgstate} in
				online|pending_online|error--stop_failed)
					#
					# Take the node offline
					#

					# See if it is the last node
					if [[ ${CLSETUP_ISMEMBER} -eq 1 ]];  then
						online_nodelist="$(clsetup_get_rgstate ${rgname} "" "online")"
					else
						online_nodelist="node4"
					fi
					if [[ -n "${online_nodelist}" ]] &&
					    [[ "${node}" == "${online_nodelist}" ]];  then
						newstate="offline"
						continue
					fi

					# Okay to continue?
					prompt="$(printf "$(gettext 'Okay to offline \"%s\" from \"%s\"?')" "${rgname}" "${node}")"
					answer=$(sc_prompt_yesno "${prompt}" "${YES}") || (newstate="change"; continue)
					if [[ "${answer}" != "yes" ]]; then
						newstate="change"
						continue
					fi

					# Construct the new online nodelist
					nodelist=
					for foo in ${online_nodelist}
					do
						if [[ "${foo}" == "${node}" ]]; then
							continue
						fi
						if [[ -z "${nodelist}" ]]; then
							nodelist=${foo}
						else
							nodelist="${nodelist},${foo}"
						fi
					done

					# Initialize command set
					set -A CLSETUP_DO_CMDSET
					let index=0

					# Switch
					CLSETUP_DO_CMDSET[index]="${CL_CMD_CLRESOURCEGROUP} switch -n ${nodelist} ${rgname}"

					# Doit
					clsetup_do_cmdset 1

					# More changes?
					newstate="change"
					;;

				offline|pending_offline)
					#
					# Bring the node online
					#

					# Make sure the node is in the cluster
					if [[ ${CLSETUP_ISMEMBER} -eq 1 ]];  then
						lnodestate="$(${SCHA_CLUSTER_GET} -O nodestate_node ${node})"
					else
						lnodestate=up
					fi
					if [[ "${lnodestate}" != "up" ]]; then
						echo
						printf "$(gettext '\"%s\" is not currently a member of the cluster.')\n\n\a" "${rgname}"
						sc_prompt_pause
						newstate="change"
						continue
					fi

					# Check maxprimaries
					if [[ ${CLSETUP_ISMEMBER} -eq 1 ]];  then
						online_nodelist="$(clsetup_get_rgstate ${rgname} "" "online")"
					else
						online_nodelist="node4"
					fi
					count=$(set -- ${online_nodelist}; echo $#)
					if [[ ${count} -ge ${maxprimaries} ]]; then
						echo
						printf "$(gettext 'The max number of primaries are already online for \"%s\".')\n\n\a" "${rgname}"
						sc_prompt_pause
						newstate="change"
						continue
					fi

					# Okay to continue?
					prompt="$(printf "$(gettext 'Okay to bring \"%s\" online on \"%s\"?')" "${rgname}" "${node}")"
					answer=$(sc_prompt_yesno "${prompt}" "${YES}") || (newstate="change"; continue)
					if [[ "${answer}" != "yes" ]]; then
						newstate="change"
						continue
					fi

					# Construct the new online nodelist
					nodelist=${node}
					if [[ ${CLSETUP_ISMEMBER} -eq 1 ]];  then
						online_nodelist="$(clsetup_get_rgstate ${rgname} "" "online")"
					else
						online_nodelist=node4
					fi
					for foo in ${online_nodelist}
					do
						nodelist="${nodelist},${foo}"
					done

					# Initialize command set
					set -A CLSETUP_DO_CMDSET
					let index=0

					# Switch
					CLSETUP_DO_CMDSET[index]="${CL_CMD_CLRESOURCEGROUP} switch -n ${nodelist} ${rgname}"

					# Doit
					clsetup_do_cmdset 1

					# More changes?
					newstate="change"
					;;

				unmanaged)
					newstate="unmanaged"
					continue
					;;

				*)
					prompt="$(gettext 'Unable to online or offline a group in an \"Unkown\" state.')"
					sc_print_para "${prompt}"
					sc_prompt_pause
					;;
				esac
			esac

			# Clear laststate
			laststate=
		done
	done

	return 0
}

##################################################################
#
# clsetup_menu_rgm_rs_prompt_per_node_ext_prop_value() value  type
#
#	type		- Type of the value.
#	nodelist	- List of node where the value is to be set.
#
#	Validate the contents of the value.
#
#	Return:
#		zero		Completed.
#		non-zero	Not completed
#####################################################################
clsetup_menu_rgm_rs_prompt_per_node_ext_prop_value()
{
	typeset proptype=${1}
	typeset nodelist=${2}
	typeset value
	typeset count
	typeset answer
	typeset -l lpropvalue

	typeset sctxt_props_prompt1="	$(gettext 'New Value:')"

	exec 5>&1
	exec 1>&4

	# Get the new value
	while true
	do
		# Get user input
		value=
		if [[ -n "${nodelist}" ]]; then
			printf "\t$(gettext 'New Value{%s}:	')" "${nodelist}"
		else
			sc_print_prompt "${sctxt_props_prompt1}" "space"
		fi
		read value

		# Ctrl-D?
		if [[ $? -ne 0 ]]; then
			echo
			return 0
		fi

		# Too many options?
		if [[ "${proptype}" != "string" ]];  then
			count=$(set -- ${value};  echo $#)
			if [[ ${count} -gt 1 ]]; then
				echo "\a\c"
				continue
			fi
		fi

		# Newline
		echo

		# Nothing entered
		if [[ -z "${value}" ]];  then
			# if [[ -z "${propvalue}" ]]; then
			#		continue 2
			#	fi
			if [[ "${proptype}" == "int" ]];  then
				echo "\a\c"
				continue
			fi
			answer=$(sc_prompt_yesno "$(gettext 'Do you want to try to clear the property?')" "${NO}")
			if [[ "${answer}" != "yes" ]]; then
				continue 2
			fi
		fi

		#
		# Attempt to make sure the value is in some
		# kind of legal range.  Note that we currently
		# do not check for all value types.
		#

		case ${proptype} in
		int)
			if [[ $(expr "${value}" : '[0-9]*') -ne ${#value} ]]; then
				printf "$(gettext '\"%s\" is not numeric.')\n\n\a" "${value}"
				continue
			fi
			;;

		boolean)
			lpropvalue=${value}
			case ${lpropvalue} in
			true)	value=TRUE ;;
			false)	value=FALSE ;;
			*)
				printf "$(gettext '%s must be set to either \"%s\" or \"%s\".')\n\n\a" "Boolean" "TRUE" "FALSE"
				continue
				;;
			esac
			;;

		string)
			;;

			*)
				if [[ "${value}" == *,* ]]; then
					printf "$(gettext 'Stringarray is not expected.')\n\n\a"
					continue
				fi
				;;
		esac

		# Done
		echo "${value}" >&5
		break
	done
}
#######################################################################
#
# clsetup_menu_rgm_rs_change_per_node_props() rsname type property value
#
#	rsname		- resource name of which the per-node extension property is to be set.
#	type		- property type.
#	property	- property name
#	nodevalues	- per-node extension property values.
#
#	Change the per-node extension properties of a resource.
#
#	Return:
#		zero		Completed
#		non-zero	Not completed
########################################################################
clsetup_menu_rgm_rs_change_per_node_props()
{
	typeset rsname=${1}
	typeset proptype=${2}
	typeset property=${3}
	typeset nodevalues=${4}
	typeset nodevalue
	typeset node=
	typeset value=
	typeset answer

	typeset sctxt_props_prompt="    $(gettext 'Current value:       ')"
	let is_over=0
	typeset prompt
	typeset nodelist=
	typeset cmd_node_value=

	for nodevalue in ${nodevalues}
	do
		# Split the node value pair into value and node.
		echo "${nodevalue}" | IFS=@ read propvalue node
		printf "\t$(gettext 'Current Value{%s}:	 ')" "${node}"
		echo "${propvalue}"
	done
	echo
	answer=$(sc_prompt_yesno "$(gettext 'Do you want to set the value on all nodes?')" "${YES}")

	# Set the per-node extension property value for all nodes.
	if [[ ${answer} == "yes" ]]; then
		 value=$(clsetup_menu_rgm_rs_prompt_per_node_ext_prop_value "${proptype}" "") || return 1

		# Initialize
		set -A CLSETUP_DO_CMDSET
		CLSETUP_DO_CMDSET[0]="${CL_CMD_CLRESOURCE} set -p ${property}=\"${value}\" ${rsname}"
		# Run it
		clsetup_do_cmdset
	else
		while true
		do
			prompt="$(gettext 'Select the node(s) on which the per-node value needs to be changed')"
			nodelist=$(clsetup_menu_rgm_rs_nodelist "" "${rsname}" "${prompt}") || continue
		 	value=$(clsetup_menu_rgm_rs_prompt_per_node_ext_prop_value "${type}" "${nodelist}") || return 1

			cmd_node_value="${cmd_node_value} -p \"${property}{${nodelist}}=${value}\""

			# Want to set another value for node.
			answer=$(sc_prompt_yesno "$(gettext 'Do you want to set another per-node value?')" "${YES}")
			if [[ ${answer} == "yes" ]]; then
				cmd_node_value="${cmd_node_value} "
				continue
			fi
			# Done setting the values.
			set -A CLSETUP_DO_CMDSET
			CLSETUP_DO_CMDSET[0]="${CL_CMD_CLRESOURCE} set ${cmd_node_value} ${rsname}"
			clsetup_do_cmdset
			break
		done
	fi
}

#####################################################
#
# clsetup_menu_rgm_rs_change_props() rsflavor
#
#	rsflavor	- must be "standard" or "extension".
#
#	Change "standard" or "extension" properties of a resource.
#
#	Return:
#		zero		Completed
#		non-zero	Not completed
#
#####################################################
clsetup_menu_rgm_rs_change_props()
{
	typeset -l lrsflavor=${1}

	typeset sctxt_rsstate_enabled_ext_p1="$(gettext '
		Since this resource is enabled, only those extension
		properties which can be changed while the resource is
		in this state will be shown in the properties menu.  You
		may need to disable the resource in order to change
		certain properties.  Check the man page for your
		resource type for more information.
	')"
	typeset sctxt_rsstate_enabled_std_p1="$(gettext '
		Since this resource is enabled, only those standard
		properties which can be changed while the resource is
		in this state will be shown in the properties menu.
		You may need to disable the resource in order to change
		certain properties.  Check the r_properties(5) man page
		for more information regarding the setting of standard
		resource properties.
	')"
	typeset sctxt_rsstate_disabled_ext_p1="$(gettext '
		This resource is currently disabled.  However, not all
		resource properties can be changed even when the
		resource is in this state.  Only those extension
		properties which can be changed when the resource is in
		a disabled state will be shown in the properties
		menu.   Check the man page for your resource type for
		more information.
	')"
	typeset sctxt_rsstate_disabled_std_p1="$(gettext '
		This resource is currently disabled.  However, not all
		resource properties can be changed even when the
		resource is in this state.  Only those standard
		properties which can be changed when the resource is in
		a disabled state will be shown in the properties
		menu.   Check the r_properties(5) man page for more
		information regarding the setting of standard resource
		properties.
	')"
	typeset sctxt_rsstate_partially_enabled_ext_p1="$(gettext '
		Since this resource is partially enabled, only those
		extension properties which can be changed while resource is
		in this state will be shown in the properties menu. Check the
		man page for your resource type for more information.
        ')"

	typeset sctxt_dependencies_std_p1="$(gettext '
		Specify each resource on which this resource is to
		depend in the format:<resource-name>{<qualifier>}.

		Set <resource-name> to the name of the resource on
		which this resource is to depend.

		Set <qualifier> to one value from the following list
		to specify the extent of the dependency of this
		resource on <resource-name>.

		LOCAL_NODE specifies that this resource depends only
		on an instance of <resource-name> on the same node
		or in the same zone.

		ANY_NODE specifies that this resource depends on an
		instance of <resource-name> on any node or in any zone.

		FROM_RG_AFFINITIES specifies that extent of the
		dependency is determined by resource group affinities of
		this resource resource group. This value is the default.

		For more information about resource dependencies, see the
		r_properties(5) man page.

	')"

	typeset sctxt_props_prompt1="    $(gettext 'Property name:       ')"
	typeset sctxt_props_prompt2="    $(gettext 'Property description:')"
	typeset sctxt_props_prompt3="    $(gettext 'Property type:       ')"
	typeset sctxt_props_prompt4="    $(gettext 'Current value:       ')"
	typeset sctxt_props_prompt5="    $(gettext 'New value:           ')"
	typeset sctxt_props_prompt6="    $(gettext 'Property per-node:   ')"

	typeset tmpfile1=${CLSETUP_TMP_RSLIST}
	typeset tmpfile2=${CLSETUP_TMP_PROPLIST}
	typeset cachefile=${CLSETUP_RGM_CACHEFILE}

	typeset CLSETUP_RS_PROP_NAMES		# Property names
	typeset CLSETUP_RS_PROP_DESCS		# Property descriptions
	typeset CLSETUP_RS_PROP_TYPES		# Property types
	typeset CLSETUP_RS_PROP_VALUES		# Property values
	typeset CLSETUP_RS_IS_PER_NODE_PROP	# Extension property type.
	typeset CLSETUP_RS_PER_NODE_PROP_VALUES	# Per-node extension property value.


	typeset answer
	typeset rslist
	typeset rsname
	typeset rsstate
	typeset rstype
	typeset prompt
	typeset header1
	typeset header2
	typeset property
	typeset value
	typeset propvalue
	typeset -l lpropvalue			# lower case
	typeset -l proptype			# lower case
	typeset foo
	typeset ret

	typeset -l is_per_node="false"
	typeset nodevalues
	typeset dep="Resource_dependencies"

	integer found
	integer count
	integer boolean_is_set

	# Loop until done or Ctrl-D is typed
	while true
	do
		# If not in debug mode, use scha_ commands to get rslist
		if [[ ${CLSETUP_ISMEMBER} -eq 1 ]]; then
			clsetup_rm_rgmfile ${cachefile}
		fi

		# Get the list of resources
		if [[ ${CLSETUP_ISMEMBER} -eq 1 ]] ||
		    [[ -r "${cachefile}" ]];  then
			rslist="$(clsetup_get_rslist "" "" "")"
		else
			rslist="res1 res2 res3 res4 res5 res6 res7 res8 res9 res10"
		fi

		# Create the temp file
		rm -f ${tmpfile1}
		let found=0
		for rsname in ${rslist}
		do
			if [[ ${CLSETUP_ISMEMBER} -eq 1 ]];  then
				clsetup_is_resource_enabled ${rsname} ""
				# Return value:
				# 	0 - the resource is disabled on all the nodes.
				# 	1 - the resource is enabled on all the nodes.
				# 	2 - the resource is enabled on some of the nodes and disabled on others.
				case $? in
				2)	rsstate="Partially Enabled" ;;
				1)	rsstate="Enabled" ;;
				0)	rsstate="Disabled" ;;
				*)	rsstate=
					((found -= 1))
					continue
					;;
				esac
				rstype="$(${SCHA_RS_GET} -R ${rsname} -O type)"
			else
				rsstate="<DEBUG>"
				rstype="<DEBUG>"
			fi
			((found += 1))
			printf "%-25s %-16.16s %-22.22s\n" "${rsname}" "${rsstate}" "${rstype}"
		done > ${tmpfile1}

		# Are there any resources?
		if [[ ${found} -lt 1 ]]; then
			echo
			printf "$(gettext 'There are no resources configured.')\n\n\a"
			sc_prompt_pause
			rm -f ${tmpfile1}
			return 1
		fi

		# Initialize menu headings
		prompt="$(gettext 'Select the resource whose properties you want to change:')"
		header1="$(printf "%-25s %-16.16s %-22.22s" "$(gettext 'Resource Name')" "$(gettext 'Resource State')" "$(gettext 'Resource Type')")"
		header2="$(printf "%-25s %-16.16s %-22.22s" "=============" "==============" "=============")"

		# Get the name of the resource to change
		rsname="$(
		    sc_get_scrolling_menuoptions	\
			"${prompt}"			\
			"${header1}"			\
			"${header2}"			\
			0 1 1				\
			:${tmpfile1}			\
		)"
		rm -f ${tmpfile1}

		# Done?
		if [[ -z "${rsname}" ]]; then
			return 0
		fi

		# Enabled or disabled?
		if [[ ${CLSETUP_ISMEMBER} -eq 1 ]];  then
			clsetup_is_resource_enabled ${rsname} ""
			ret=$?
			# Return value:
			# 	0 - the resource is disabled on all the nodes.
			# 	1 - the resource is enabled on all the nodes.
			# 	2 - the resource is enabled on some of the nodes and disabled on others.
			if [[ $ret -eq 0 ]]; then
				rsstate="disabled"
			elif [[ $ret -eq 1 ]]; then
				rsstate="enabled"
			else
				rsstate="partially enabled"
			fi
		else
			rsstate="disabled"
		fi

		# Print help text

		if [[ "${lrsflavor}" == "standard" ]]; then
			if [[ "${rsstate}" == "enabled" || "${rsstate}" = "partially enabled" ]]; then
				sc_print_para "${sctxt_rsstate_enabled_std_p1}"
			else
				sc_print_para "${sctxt_rsstate_disabled_std_p1}"
			fi
		elif [[ "${lrsflavor}" == "extension" ]]; then
			if [[ "${rsstate}" == "enabled" ]]; then
				sc_print_para "${sctxt_rsstate_enabled_ext_p1}"
			elif [[ "${rsstate}" == "disabled" ]]; then
				sc_print_para "${sctxt_rsstate_disabled_ext_p1}"
			elif [[ "${rsstate}" == "partially enabled" ]]; then
				sc_print_para "${sctxt_rsstate_partially_enabled_ext_p1}"
			fi
		fi
		sc_prompt_pause

		# Initialize menu headings
		prompt="$(printf "$(gettext 'Select the %s property you want to change:')" "${lrsflavor}")"
		header1="$(printf "%-30s %-30.30s" "$(gettext 'Property Name')" "$(gettext 'Current Setting')")"
		header2="$(printf "%-30s %-30.30s" "=============" "===============")"

		# Loop until done or Ctrl-D is typed
		while true
		do
			# Clear the resource attributes arrays
			set -A CLSETUP_RS_PROP_NAMES
			set -A CLSETUP_RS_PROP_DESCS
			set -A CLSETUP_RS_PROP_TYPES
			set -A CLSETUP_RS_PROP_VALUES
			set -A CLSETUP_RS_IS_PER_NODE_PROP
			set -A CLSETUP_RS_PER_NODE_PROP_VALUES

			# Cache output
			if [[ ${CLSETUP_ISMEMBER} -eq 1 ]]; then
				clsetup_create_rgmfile ${cachefile}
			fi

			# Set the resource attributes arrays
			if [[ ${CLSETUP_ISMEMBER} -eq 1 ]] ||
			    [[ -r "${cachefile}" ]]; then
				clsetup_get_rs_props "${rsname}" "${lrsflavor}" "${rsstate}" "${cachefile}"
			# If debug, fabricate some debug data
			else
				set -A CLSETUP_RS_PROP_NAMES Prop_int Prop_string Prop_array Prop_other

				CLSETUP_RS_PROP_DESCS[0]="Integer property"
				CLSETUP_RS_PROP_DESCS[1]="String property"
				CLSETUP_RS_PROP_DESCS[2]="String array property"
				CLSETUP_RS_PROP_DESCS[3]="Other property"

				CLSETUP_RS_PROP_TYPES[0]=Int
				CLSETUP_RS_PROP_TYPES[1]=String
				CLSETUP_RS_PROP_TYPES[2]=Stringarray
				CLSETUP_RS_PROP_TYPES[3]=

				CLSETUP_RS_PROP_VALUES[0]=20
				CLSETUP_RS_PROP_VALUES[1]="hello"
				CLSETUP_RS_PROP_VALUES[2]=
				CLSETUP_RS_PROP_VALUES[3]=
			fi

			# Done with cache file
			if [[ ${CLSETUP_ISMEMBER} -eq 1 ]]; then
				clsetup_rm_rgmfile ${cachefile}
			fi

			# Are there any properties to set?
			if [[ -z "${CLSETUP_RS_PROP_NAMES[0]}" ]]; then
				echo
				if [[ "${rsstate}" == "disabled" ]]; then
					printf "$(gettext 'There are no tunable %s properties for \"%s\".')\n\n\a" "${lrsflavor}" "${rsname}"
				else
					printf "$(gettext 'There are no tunable %s properties for \"%s\" at this time.')\n" "${lrsflavor}" "${rsname}"
					printf "$(gettext 'Additional properties may be tunable in the %s state.')\n\n\a" "disabled"
				fi
				sc_prompt_pause
				continue 2
			fi

			# Create the temp file
			rm -f ${tmpfile2}
			for property in ${CLSETUP_RS_PROP_NAMES[*]}
			do
				is_per_node="$(clsetup_get_rs_prop_is_per_node_prop ${property})"
				if [[ "${is_per_node}" == "true" ]]; then
					nodevalues="$(clsetup_get_rs_per_node_ext_prop_values ${property})"
					printf "%-30s %-30.30s\n" "${property}" "${nodevalues}"
					nodevalues=
				else
					value="$(clsetup_get_rs_prop_value ${property})"
					printf "%-30s %-30.30s\n" "${property}" "${value}"
				fi
			done >${tmpfile2}

			# Get the property
			property="$(
			    sc_get_scrolling_menuoptions	\
				"${prompt}"			\
				"${header1}"			\
				"${header2}"			\
				0 1 1				\
				:${tmpfile2}			\
			)"
			rm -f ${tmpfile2}

			# Done with this resource?
			if [[ -z "${property}" ]]; then
				continue 2
			fi

			# Print the help message for resource
			# dependencies properties alone.

			if [[ $(expr "${property}" : ${dep}'*') -eq ${#dep} ]]; then
				sc_print_para "${sctxt_dependencies_std_p1}"
			fi

			# Print the name
			sc_print_prompt "${sctxt_props_prompt1}" "space"
			echo "${property}"

			# Print the description
			sc_print_prompt "${sctxt_props_prompt2}" "space"
			propvalue="$(clsetup_get_rs_prop_description ${property})"
			echo "${propvalue}"

			# Print the type
			sc_print_prompt "${sctxt_props_prompt3}" "space"
			proptype="$(clsetup_get_rs_prop_type ${property})"
			echo "${proptype}"

			# Print the property type (per-node or not).
			is_per_node="$(clsetup_get_rs_prop_is_per_node_prop ${property})"
			if [[ "${lrsflavor}" == "extension" ]]; then
				sc_print_prompt "${sctxt_props_prompt6}" "space"
				echo "${is_per_node}"
			fi
			# Print the current value
			if [[ "${is_per_node}" == "true" ]]; then
				nodevalues="$(clsetup_get_rs_per_node_ext_prop_values ${property})"
				clsetup_menu_rgm_rs_change_per_node_props "${rsname}" "${proptype}" "${property}" "${nodevalues}" || return 1
				return 0
			else
				sc_print_prompt "${sctxt_props_prompt4}" "space"
				propvalue="$(clsetup_get_rs_prop_value ${property})"
				echo "${propvalue}"
			fi

			# If proptype is boolean, switch the setting
			let boolean_is_set=0
			if [[ "${proptype}" == "boolean" ]];  then
				lpropvalue=${propvalue}
				case ${lpropvalue} in
				true)	value=FALSE
					let boolean_is_set=1
					;;

				false)	value=TRUE
					let boolean_is_set=1
					;;
				esac

				if [[ ${boolean_is_set} -eq 1 ]]; then
					sc_print_prompt "${sctxt_props_prompt5}" "space"
					echo "${value}"
					echo
				fi
			fi

			# Get the new value
			while [[ ${boolean_is_set} -eq 0 ]]
			do
				# Get user input
				value=
				sc_print_prompt "${sctxt_props_prompt5}" "space"
				read value

				# Ctrl-D?
				if [[ $? -ne 0 ]]; then
					echo
					continue 2
				fi

				# Compress ", " in stringarrays
				if [[ "${proptype}" == "stringarray" ]]; then
					value="$(echo ${value} | sed 's/, /,/g')"
				fi

				# Too many options?
				if [[ "${proptype}" != "string" ]];  then
					count=$(set -- ${value};  echo $#)
					if [[ ${count} -gt 1 ]]; then
						echo "\a\c"
						continue
					fi
				fi

				# Newline
				echo

				# Nothing entered
				if [[ -z "${value}" ]];  then
					if [[ -z "${propvalue}" ]]; then
						continue 2
					fi
					if [[ "${proptype}" == "int" ]];  then
						echo "\a\c"
						continue
					fi
					answer=$(sc_prompt_yesno "$(gettext 'Do you want to try to clear the property?')" "${NO}")
					if [[ "${answer}" != "yes" ]]; then
						continue 2
					fi
				fi

				#
				# Attempt to make sure the value is in some
				# kind of legal range.  Note that we currently
				# do not check for all value types.
				#
				case ${proptype} in
				int)
					if [[ $(expr "${value}" : '[0-9]*') -ne ${#value} ]]; then
						printf "$(gettext '\"%s\" is not numeric.')\n\n\a" "${value}"
						continue
					fi
					;;

				boolean)
					lpropvalue=${value}
					case ${lpropvalue} in
					true)	value=TRUE ;;
					false)	value=FALSE ;;
					*)
						printf "$(gettext '%s must be set to either \"%s\" or \"%s\".')\n\n\a" "Boolean" "TRUE" "FALSE"
						continue
						;;
					esac
					;;

				string|stringarray)
					;;

				*)
					if [[ "${value}" == *,* ]]; then
						printf "$(gettext 'Stringarray is not expected.')\n\n\a"
						continue
					fi
					;;
				esac

				# Done
				break
			done

			# Initialize
			set -A CLSETUP_DO_CMDSET

			CLSETUP_DO_CMDSET[0]="${CL_CMD_CLRESOURCE} set -p ${property}=\"${value}\" ${rsname}"

			# Run it
			clsetup_do_cmdset
		done
	done

	return 0
}

#####################################################
#
# clsetup_re_version_resource() index rgstatefile [summaryfile]
#
#	index		- index into an already initialized
#			    set of CLSETUP_RS_*_UPGRD arrays:
#
#				CLSETUP_RS_NAMES_UPGD
#				CLSETUP_RS_CVERS_UPGD
#				CLSETUP_RS_TYPE_UPGD
#				CLSETUP_RS_PVERS_UPGD
#				CLSETUP_RS_WHEN_UPGD
#
#	rgstatefile	- resource group statefile (see
#			    clsetup_create_rgstatefile())
#
#	summaryfile	- summarize errors in this file
#
#	This function re-versions CLSETUP_RS_NAMES_UPGD[index] to
#	CLSETUP_RS_PVERS_UPGD[index], if allowed by
#	CLSETUP_RS_WHEN_UPGD[index].
#
#	Return:
#		zero		Completed
#		non-zero	Not completed
#
#####################################################
clsetup_re_version_resource()
{
	integer index=${1}
	typeset rgstatefile=${2}
	typeset summaryfile=${3}

	typeset prompt1
	typeset prompt2
	typeset answer
	typeset errmsg
	typeset nodelist
	typeset rgname
	typeset -l lmonitored		# lower case

	integer status

	# Initialize prompts
	prompt1="$(printf "$(gettext 'The \"%s\" resource is currently running version \"%s\" of resource type \"%s\".')" "${CLSETUP_RS_NAMES_UPGD[index]}" "${CLSETUP_RS_CVERS_UPGD[index]}" "${CLSETUP_RS_TYPE_UPGD[index]}")"
	prompt2="$(printf "$(gettext 'Is it okay to upgrade to version \"%s\"?')" "${CLSETUP_RS_PVERS_UPGD[index]}")"

	# Okay to continue?
	sc_print_para "${prompt1}"
	answer=$(sc_prompt_yesno "${prompt2}" "${YES}") || return 1
	if [[ "${answer}" != "yes" ]]; then
		return 1
	fi

	# Check "when"
	case "${CLSETUP_RS_WHEN_UPGD[index]}" in
	"Anytime")
		;;

	"At creation")
		sc_print_para "$(printf "$(gettext 'Resources can only be upgrade to \"%s\" at creation time.')" "${CLSETUP_RS_TYPE_UPGD[index]}:${CLSETUP_RS_PVERS_UPGD[index]}")"
		errmsg="$(printf "$(gettext '\"%s\" must be removed and re-created to use the new version.')" "${CLSETUP_RS_NAMES_UPGD[index]}")"
		printf "%s\n\n\a" "${errmsg}"
		if [[ -n "${summaryfile}" ]]; then
			echo "${errmsg}" >> ${summaryfile}
		fi
		sc_prompt_pause
		return 1
		;;

	"When disabled")
		if [[ ${CLSETUP_ISMEMBER} -eq 1 ]]; then
			# Enabled?
			clsetup_is_resource_enabled ${CLSETUP_RS_NAMES_UPGD[index]} ""
			# Return value:
			# 	0 - the resource is disabled on all the nodes.
			# 	1 - the resource is enabled on all the nodes.
			# 	2 - the resource is enabled on some of the nodes and disabled on others.

			# Enabled or partially enabled /online?
			if [[ $? -ne 0 ]]; then
				sc_print_para "$(printf "$(gettext 'Resources can only be upgrade to \"%s\" when they are disabled.')" "${CLSETUP_RS_TYPE_UPGD[index]}:${CLSETUP_RS_PVERS_UPGD[index]}")"
				errmsg="$(printf "$(gettext '\"%s\" must be disabled before it can be upgraded.')" "${CLSETUP_RS_NAMES_UPGD[index]}")"
				printf "%s\n\n\a" "${errmsg}"
				if [[ -n "${summaryfile}" ]]; then
					echo "${errmsg}" >> ${summaryfile}
				fi
				sc_prompt_pause
				return 1
			fi
		fi
		;;

	"When offline")
		if [[ ${CLSETUP_ISMEMBER} -eq 1 ]]; then
			rgname=$(${SCHA_RS_GET} -R ${CLSETUP_RS_NAMES_UPGD[index]} -O group)
			if [[ -n "${rgname}" ]]; then
				nodelist=$(clsetup_get_rgstate ${rgname} ${rgstatefile} "not_offline")
			fi
			if [[ -z "${rgname}" ]] || [[ -n "${nodelist}" ]]; then
				sc_print_para "$(printf "$(gettext 'Resources can only be upgrade to \"%s\" when the groups to which they belong are offline.')" "${CLSETUP_RS_TYPE_UPGD[index]}:${CLSETUP_RS_PVERS_UPGD[index]}")"
				errmsg="$(printf "$(gettext '\"%s\" must be offline before \"%s\" can be upgraded.')" "${rgname}" "${CLSETUP_RS_NAMES_UPGD[index]}")"
				printf "%s\n\n\a" "${errmsg}"
				if [[ -n "${summaryfile}" ]]; then
					echo "${errmsg}" >> ${summaryfile}
				fi
				sc_prompt_pause
				return 1
			fi
		fi
		;;

	"When unmanaged")
		if [[ ${CLSETUP_ISMEMBER} -eq 1 ]]; then
			rgname=$(${SCHA_RS_GET} -R ${CLSETUP_RS_NAMES_UPGD[index]} -O group)
			if [[ -n "${rgname}" ]]; then
				nodelist=$(clsetup_get_rgstate ${rgname} ${rgstatefile} "unmanaged")
			fi
			if [[ -z "${rgname}" ]] || [[ -z "${nodelist}" ]]; then
				sc_print_para "$(printf "$(gettext 'Resources can only be upgrade to \"%s\" when the groups to which they belong are unmanaged.')" "${CLSETUP_RS_TYPE_UPGD[index]}:${CLSETUP_RS_PVERS_UPGD[index]}")"
				errmsg="$(printf "$(gettext '\"%s\" must be unmanaged before \"%s\" can be upgraded.')" "${rgname}" "${CLSETUP_RS_NAMES_UPGD[index]}")"
				printf "%s\n\n\a" "${errmsg}"
				if [[ -n "${summaryfile}" ]]; then
					echo "${errmsg}" >> ${summaryfile}
				fi
				sc_prompt_pause
				return 1
			fi
		fi
		;;

	"When unmonitored")
		if [[ ${CLSETUP_ISMEMBER} -eq 1 ]]; then
			lmonitored=$(${SCHA_RS_GET} -R ${CLSETUP_RS_NAMES_UPGD[index]} -O monitored_switch)
			if [[ "${lmonitored}" == "enabled" ]]; then
				let status=1
			else
				let status=0
			fi

			# If monitored, but not enabled, should be okay
			if [[ ${status} -ne 0 ]]; then
				# Enabled?
				clsetup_is_resource_enabled ${CLSETUP_RS_NAMES_UPGD[index]} ""
				# Return value:
				# 	0 - the resource is disabled on all the nodes.
				# 	1 - the resource is enabled on all the nodes.
				# 	2 - the resource is enabled on some of the nodes and disabled
				let status=$?
			fi

			# If monitored or monitored/enabled,
			# but group is offline, should be okay
			if [[ ${status} -ne 0 ]]; then
				rgname=$(${SCHA_RS_GET} -R ${CLSETUP_RS_NAMES_UPGD[index]} -O group)
				if [[ -n "${rgname}" ]]; then
					nodelist=$(clsetup_get_rgstate ${rgname} ${rgstatefile} "not_offline")
				fi
				if [[ -n "${rgname}" ]] && [[ -z "${nodelist}" ]]; then
					let status=0
				fi
			fi

			# Monitored or monitored/enabled or
			# monitored/enabled/online?
			if [[ ${status} -ne 0 ]]; then

				sc_print_para "$(printf "$(gettext 'Resources can only be upgrade to \"%s\" if they are unmonitored.')" "${CLSETUP_RS_TYPE_UPGD[index]}:${CLSETUP_RS_PVERS_UPGD[index]}")"
				errmsg="$(printf "$(gettext '\"%s\" must have monitoring disabled before it can be upgraded.')" "${CLSETUP_RS_NAMES_UPGD[index]}")"
				printf "%s\n\n\a" "${errmsg}"
				if [[ -n "${summaryfile}" ]]; then
					echo "${errmsg}" >> ${summaryfile}
				fi
				sc_prompt_pause
				return 1
			fi
		fi
		;;
	esac

	# Go ahead and try to re-version
	set -A CLSETUP_DO_CMDSET

	CLSETUP_DO_CMDSET[0]="${CL_CMD_CLRESOURCE} set -p Type_version=${CLSETUP_RS_PVERS_UPGD[index]} ${CLSETUP_RS_NAMES_UPGD[index]}"

	# Run it without re-confirming
	clsetup_do_cmdset 1 || return 1

	return 0
}

#####################################################
#
# clsetup_menu_rgm_rs_manage_versions() [nohelp]
#
#	nohelp		- if this flag is given, do not
#			    clear screen, print title, print help,
#			    or ask for confirmation to continue
#
#	Manage resource versioning.
#
#	This function always returns zero.
#
#####################################################
clsetup_menu_rgm_rs_manage_versions()
{
	typeset nohelp=${1}

	typeset sctxt_title="$(gettext '
		>>> Resource Versioning <<<
	')"
	typeset sctxt_p1="$(gettext '
		Use this option to manage the versioning of resources.
	')"
	typeset sctxt_p2="$(gettext '
		The \"Type_version\" property is a special standard
		property of a resource which specifies the version of a
		resource type used by that resource.  Once a new version
		of a resource type is installed and registered, the
		\"Type_version\" property of resources of that resource
		type can be changed to upgrade those resources to the
		new version.
	')"

	typeset sctxt_prompt_operation="$(gettext 'Please select from one of the following options:')"
	typeset sctxt_option_operation_001="$(gettext 'Show versioning status')"
	typeset sctxt_option_operation_002="$(gettext 'Re-version selected resources')"
	typeset sctxt_option_operation_003="$(gettext 'Re-version all eligible resources of a given resource type')"
	typeset sctxt_option_operation_004="$(gettext 'Re-version all eligible resources')"
	typeset sctxt_option_return="$(gettext 'Return to the resource properties menu')"

	typeset tmpfile1=${CLSETUP_TMP_RSLIST}
	typeset tmpfile1_sorted=${CLSETUP_TMP_RSLIST_SORTED}
	typeset tmpsummary=${CLSETUP_TMP_SUMMARY}
	typeset cachefile=${CLSETUP_RGM_CACHEFILE}
	typeset rgstatefile=${CLSETUP_TMP_RGSTATE}

	# Variables for call to clsetup_get_upgradeable_resources()
	typeset CLSETUP_RS_NAMES_UPGD		# Resource names
	typeset CLSETUP_RS_CVERS_UPGD		# Resource current versions
	typeset CLSETUP_RS_TYPE_UPGD		# Resource type
	typeset CLSETUP_RS_PVERS_UPGD		# Possible upgrade versions
	typeset CLSETUP_RS_WHEN_UPGD		# Upgrade when

	typeset prompt
	typeset header1
	typeset header2
	typeset answer
	typeset option
	typeset task
	typeset to_when
	typeset rsname
	typeset rstype
	typeset newvers
	typeset curvers
	typeset rstypes_done
	typeset foo

	integer i
	integer linecount
	integer found

	# Unless nohelp is specified, print help and get confirmation
	if [[ -z "${nohelp}" ]]; then

		# Clear screen, print title and help
		sc_print_title "${sctxt_title}"
		sc_print_para "${sctxt_p1}"
		sc_print_para "${sctxt_p2}"

		# Okay to continue?
		clsetup_continue || return 0
	fi

	# Loop until done or Ctrl-D is typed
	task=
	while true
	do
		# If we are already on the "select" or "allrt" menu, stay there
		if [[ "${task}" != "select" ]] &&
		    [[ "${task}" != "allrt" ]]; then

			# Get the menu option
			option=$(sc_get_menuoption \
			    "T2+++${sctxt_prompt_operation}" \
			    "S+0+1+${sctxt_option_operation_001}" \
			    "S+0+2+${sctxt_option_operation_002}" \
			    "S+0+3+${sctxt_option_operation_003}" \
			    "S+0+4+${sctxt_option_operation_004}" \
			    "R+++" \
			    "S+0+q+${sctxt_option_return}" \
			)

			# Set the task
			case ${option} in
			1)	task="show"	;;
			2)	task="select"	;;
			3)	task="allrt"	;;
			4)	task="all"	;;
			*)	break		;;
			esac
		fi

		# Remove the rgstatefile
		clsetup_rm_rgstatefile ${rgstatefile}

		# If not a cluster member and no cachefile, set for debug
		if [[ ${CLSETUP_ISMEMBER} -ne 1 ]] &&
		    [[ ! -r "${cachefile}" ]]; then

			set -A CLSETUP_RS_NAMES_UPGD
			set -A CLSETUP_RS_CVERS_UPGD
			set -A CLSETUP_RS_TYPE_UPGD
			set -A CLSETUP_RS_PVERS_UPGD
			set -A CLSETUP_RS_WHEN_UPGD

			CLSETUP_RS_NAMES_UPGD[0]=res1
			CLSETUP_RS_NAMES_UPGD[1]=res2

			CLSETUP_RS_CVERS_UPGD[0]=1.0
			CLSETUP_RS_CVERS_UPGD[1]=2.0

			CLSETUP_RS_TYPE_UPGD[0]=typeX
			CLSETUP_RS_TYPE_UPGD[1]=typeY

			CLSETUP_RS_PVERS_UPGD[0]=3.0
			CLSETUP_RS_PVERS_UPGD[1]=3.0

			CLSETUP_RS_WHEN_UPGD[0]=Anytime
			CLSETUP_RS_WHEN_UPGD[1]="When unmonitored"

		# Otherwise, get the upgradeable resources and rgstatefile
		else
			clsetup_get_upgradeable_resources ${cachefile}

			# Create the rgstatefile
			case ${task} in
			select|allrt|all)
				clsetup_create_rgstatefile ${rgstatefile}
				;;
			esac
		fi

		# Make sure that there is at least one resource
		if [[ -z "${CLSETUP_RS_NAMES_UPGD[0]}" ]]; then
			echo
			printf "$(gettext 'There are no resources configured.')\n\n\a"
			sc_prompt_pause
			break
		fi

		# Switch on task
		case ${task} in
		show)		# Show re-versioning status
			(
				echo
				printf "%10.10s %-17s %-27s %s\n" "" "$(gettext 'Resource Name')" "$(gettext 'Current Version')" "$(gettext 'Upgradeable To')"
				printf "%10.10s %-17s %-27s %s\n" "" "$(gettext '=============')" "$(gettext '===============')" "$(gettext '==============')"
				echo

				let i=0
				while [[ -n "${CLSETUP_RS_NAMES_UPGD[i]}" ]]
				do
					# Set "to_when"
					if [[ -n "${CLSETUP_RS_PVERS_UPGD[i]}" ]]; then
						to_when=${CLSETUP_RS_PVERS_UPGD[i]}
						if [[ -n "${CLSETUP_RS_WHEN_UPGD[i]}" ]]; then
							to_when="${to_when} - ${CLSETUP_RS_WHEN_UPGD[i]}"
						fi
					else
						to_when=
					fi

					# Print line
					printf "%10.10s %-17s %-27s %s\n" "" "${CLSETUP_RS_NAMES_UPGD[i]}" "${CLSETUP_RS_TYPE_UPGD[i]}:${CLSETUP_RS_CVERS_UPGD[i]}" "${to_when}"

					# Next
					((i += 1))
				done
			) | more

			# Done
			sc_prompt_pause
			continue
			;;

		select)		# Select individual resources for re-versioning

			# Initialize headers
			prompt="$(gettext 'Select the resource you want to upgrade:')"
			header1="$(printf "%-27s %-17s %s" "$(gettext 'New Version')" "$(gettext 'Resource Name')" "$(gettext 'Current Version')")"
			header2="$(printf "%-27s %-17s %s" "$(gettext '===========')" "$(gettext '=============')" "$(gettext '===============')")"

			# Create the temp file
			rm -f ${tmpfile1}
			let found=0
			let i=0
			while [[ -n "${CLSETUP_RS_NAMES_UPGD[i]}" ]]
			do
				# Anything to upgrade to?
				if [[ -z "${CLSETUP_RS_PVERS_UPGD[i]}" ]]; then
					((i += 1))
					continue
				fi
				let found=1

				# Print line
				printf "%-27s %-17s %s\n" "${CLSETUP_RS_TYPE_UPGD[i]}:${CLSETUP_RS_PVERS_UPGD[i]}" "${CLSETUP_RS_NAMES_UPGD[i]}" "${CLSETUP_RS_CVERS_UPGD[i]}"

				# Next
				((i += 1))
			done > ${tmpfile1}

			# If the list is empty, we are done
			if [[ ${found} -eq 0 ]]; then
				echo
				printf "$(gettext 'There are no resources ready to be re-versioned.')\n"
				sc_prompt_pause
				rm -f ${tmpfile1}
				task=
				continue
			fi

			# Sort it
			rm -f ${tmpfile1_sorted}
			sort ${tmpfile1} >${tmpfile1_sorted}
			rm -f ${tmpfile1}

			# Get the name of the resource to upgrade
			answer="$(
			    sc_get_scrolling_menuoptions	\
				"${prompt}"			\
				"${header1}"			\
				"${header2}"			\
				0 1 3				\
				:${tmpfile1_sorted}		\
			)"
			rm -f ${tmpfile1_sorted}

			# Done?
			if [[ -z "${answer}" ]]; then
				task=
				continue
			fi

			# Get resource type, resource name, and versions
			(IFS=: ; set -- ${answer};  echo ${*}) | read rstype newvers rsname curvers

			# Find it in our set of arrays
			let found=0
			let i=0
			while [[ -n "${CLSETUP_RS_NAMES_UPGD[i]}" ]]
			do
				if [[ "${rsname}" == "${CLSETUP_RS_NAMES_UPGD[i]}" ]] &&
				    [[ "${curvers}" == "${CLSETUP_RS_CVERS_UPGD[i]}" ]] &&
				    [[ "${newvers}" == "${CLSETUP_RS_PVERS_UPGD[i]}" ]]; then
					# Reversion the resource at index "i"
					clsetup_re_version_resource ${i} ${rgstatefile}

					# Done
					let found=1
					break
				fi

				# Next
				((i += 1))
			done

			# Should never happen
			if [[ ${found} -eq 0 ]]; then
				echo
				printf "$(gettext 'Unable to re-version \"%s\".')\n" "${rsname}"
				sc_prompt_pause
				continue
			fi

			# Done
			continue
			;;

		allrt)		# Re-version all resources of a given rt

			# Initialize headers
			prompt="$(gettext 'Select the resource type and version for upgrade:')"
			header1="$(printf "%s" "$(gettext 'New Version')")"
			header2="$(printf "%s" "$(gettext '===========')")"

			# Create the temp file
			rm -f ${tmpfile1}
			let found=0
			let i=0
			while [[ -n "${CLSETUP_RS_NAMES_UPGD[i]}" ]]
			do
				# Anything to upgrade to?
				if [[ -z "${CLSETUP_RS_PVERS_UPGD[i]}" ]]; then
					((i += 1))
					continue
				fi
				let found=1

				# Print line
				printf "%s\n" "${CLSETUP_RS_TYPE_UPGD[i]}:${CLSETUP_RS_PVERS_UPGD[i]}"

				# Next
				((i += 1))
			done > ${tmpfile1}

			# If the list is empty, we are done
			if [[ ${found} -eq 0 ]]; then
				echo
				printf "$(gettext 'There are no resources ready to be re-versioned.')\n"
				sc_prompt_pause
				rm -f ${tmpfile1}
				task=
				continue
			fi

			# Sort it, and weed out duplicates
			rm -f ${tmpfile1_sorted}
			sort ${tmpfile1} | uniq >${tmpfile1_sorted}
			rm -f ${tmpfile1}

			# Get the name of the resource types to upgrade
			answer="$(
			    sc_get_scrolling_menuoptions	\
				"${prompt}"			\
				"${header1}"			\
				"${header2}"			\
				0 1 1				\
				:${tmpfile1_sorted}		\
			)"
			rm -f ${tmpfile1_sorted}

			# Done?
			if [[ -z "${answer}" ]]; then
				task=
				continue
			fi

			# Get resource type and version
			(IFS=: ; set -- ${answer};  echo ${*}) | read rstype newvers

			# Re-version each resource which qualifies
			rm -f ${tmpsummary}
			rstypes_done=
			let found=0
			let i=0
			while [[ -n "${CLSETUP_RS_NAMES_UPGD[i]}" ]]
			do
				if [[ "${rstype}" == "${CLSETUP_RS_TYPE_UPGD[i]}" ]] &&
				    [[ "${newvers}" == "${CLSETUP_RS_PVERS_UPGD[i]}" ]]; then
					# Only do it once
					for foo in ${rstypes_done}
					do
						if [[ "${foo}" == "${CLSETUP_RS_NAMES_UPGD[i]}" ]]; then
							((i += 1))
							continue 2
						fi
					done

					# Reversion the resource at index "i"
					clsetup_re_version_resource ${i} ${rgstatefile} ${tmpsummary}
					if [[ $? -eq 0 ]]; then
						rstypes_done="${rstypes_done} ${CLSETUP_RS_NAMES_UPGD[i]}"
					fi

					let found=1
				fi

				# Next
				((i += 1))
			done

			# Should never happen
			if [[ ${found} -eq 0 ]]; then
				echo
				printf "$(gettext 'Unable to re-version \"%s\" resources.')\n" "${rstype}"
				sc_prompt_pause
				rm -f ${tmpsummary}
				continue
			fi

			# Re-try messages
			let linecount=0
			if [[ -s "${tmpsummary}" ]]; then
				wc -l ${tmpsummary} | read linecount foo
			fi
			if [[ ${linecount} -gt 1 ]]; then
			    (
				sc_print_para "$(gettext 'Here is a summary of messages:')"
				while read line
				do
					printf "\t${line}\n"
				done < ${tmpsummary}
				echo
			    ) | more

				sc_prompt_pause
			fi

			# Done
			rm -f ${tmpsummary}
			continue
			;;

		all)		# Re-version all resources

			# Re-version each resource which can be upgraded
			rstypes_done=
			let found=0
			let i=0
			while [[ -n "${CLSETUP_RS_NAMES_UPGD[i]}" ]]
			do
				if [[ -n "${CLSETUP_RS_PVERS_UPGD[i]}" ]]; then

					# Only do it once
					for foo in ${rstypes_done}
					do
						if [[ "${foo}" == "${CLSETUP_RS_NAMES_UPGD[i]}" ]]; then
							((i += 1))
							continue 2
						fi
					done

					# Reversion the resource at index "i"
					clsetup_re_version_resource ${i} ${rgstatefile} ${tmpsummary}
					if [[ $? -eq 0 ]]; then
						rstypes_done="${rstypes_done} ${CLSETUP_RS_NAMES_UPGD[i]}"
					fi

					let found=1
				fi

				# Next
				((i += 1))
			done

			# Anything to re-version?
			if [[ ${found} -eq 0 ]]; then
				echo
				printf "$(gettext 'There is nothing to re-version.')\n"
				rm -f ${tmpsummary}
				sc_prompt_pause
				continue
			fi

			# Re-try messages
			let linecount=0
			if [[ -s "${tmpsummary}" ]]; then
				wc -l ${tmpsummary} | read linecount foo
			fi
			if [[ ${linecount} -gt 1 ]]; then
			    (
				sc_print_para "$(gettext 'Here is a summary of messages:')"
				while read line
				do
					printf "\t${line}\n"
				done < ${tmpsummary}
				echo
			    ) | more

				sc_prompt_pause
			fi

			# Done
			rm -f ${tmpsummary}
			continue
			;;

		*)		# Done
			break
			;;

		esac
	done

	# Remove the rgstatefile
	clsetup_rm_rgstatefile ${rgstatefile}

	return 0
}

#####################################################
#
# clsetup_get_rs_changeproperties_menuoption()
#
#	Print the resource change-properties menu, and return
#	the selected option.
#
#	This function always returns zero.
#
#####################################################
clsetup_get_rs_changeproperties_menuoption()
{
	typeset sctxt_title_2="$(gettext 'Please select from one of the following options:')"
	typeset sctxt_option_001="$(gettext 'Change standard resource properties')"
	typeset sctxt_option_002="$(gettext 'Change extension resource properties')"
	typeset sctxt_option_003="$(gettext 'Manage resource versioning')"
	typeset sctxt_option_return="$(gettext 'Return to the resource group menu')"

	typeset option

	# Return
	option=$(sc_get_menuoption \
		"T2+++${sctxt_title_2}" \
		"S+0+1+${sctxt_option_001}" \
		"S+0+2+${sctxt_option_002}" \
		"S+0+3+${sctxt_option_003}" \
		"R+++" \
		"S+0+q+${sctxt_option_return}" \
	)

	echo "${option}"

	return 0
}

#####################################################
#
# clsetup_menu_rgm_rs_changeproperties() [nohelp]
#
#	nohelp		- if this flag is given, do not
#			    clear screen, print title, print help,
#			    or ask for confirmation to continue
#
#	Change properties of a resource.
#
#	This function always returns zero.
#
#####################################################
clsetup_menu_rgm_rs_changeproperties()
{
	typeset nohelp=${1}

	typeset sctxt_title="$(gettext '
		>>> Change Properties of a Resource <<
	')"
	typeset sctxt_p1="$(gettext '
		Use this option to change properties of a resource.
	')"
	typeset sctxt_p2="$(gettext '
		Resources have two types of properties, standard properties
		and extension properties.   Standard resource properties are
		described on the r_properties(5) man page.  Extended
		properties are described on the individual man pages
		for each resource type.
	')"
	typeset sctxt_p3="$(gettext '
		The \"Type_version\" property is a special standard
		property of a resource which specifies the version of a
		resource type used by that resource. The \"Type_version\"
		property of resources can sometimes be changed to
		upgrade resources to a new version.  This menu option
		includes support for the management of resource
		re-versioning.
	')"
	typeset sctxt_p4="$(gettext '
		Not all resource properties can be changed at any
		time.  For example, some properties can only be set at
		the time that the resource is created, some can only be
		changed when the resource is disabled, and some can
		never be changed.
	')"

	# Unless nohelp is specified, print help and get confirmation
	if [[ -z "${nohelp}" ]]; then

		# Clear screen, print title and help
		sc_print_title "${sctxt_title}"
		sc_print_para "${sctxt_p1}"
		sc_print_para "${sctxt_p2}"
		sc_print_para "${sctxt_p3}"
		sc_print_para "${sctxt_p4}"

		# Okay to continue?
		clsetup_continue || return 1
	fi

	# Loop until done or Ctrl-D is typed
	while true
	do
		# Get the menu option
                case $(clsetup_get_rs_changeproperties_menuoption) in
                '1')    clsetup_menu_rgm_rs_change_props "standard" ;;
                '2')    clsetup_menu_rgm_rs_change_props "extension" ;;
                '3')    clsetup_menu_rgm_rs_manage_versions ;;
                'q')    break ;;
                esac
	done

	return 0
}

#####################################################
#
# clsetup_menu_rgm_rs_delete() [delete] [nohelp]
#
#	delete		- the resource to delete
#
#	nohelp		- if this flag is given, do not
#			    clear screen, print title, print help,
#			    or ask for confirmation to continue
#
#	Remove a resource from a resource group.
#
#	Return:
#		zero		Completed
#		non-zero	Not completed
#
#####################################################
clsetup_menu_rgm_rs_delete()
{
	typeset delete=${1}
	typeset nohelp=${2}

	typeset sctxt_title="$(gettext '
		>>> Remove a Resource from a Resource Group <<<
	')"
	typeset sctxt_p1="$(gettext '
		Use this option to to remove a resource from a resource
		group.
	')"
	typeset sctxt_p2="$(gettext '
		Before a resource can be removed, it must be in the
		disabled state.   
	')"
	typeset sctxt_p3="$(gettext '
		If the resource you select for removal is not already
		disabled, you can disable it using this menu option.
	')"
	typeset sctxt_p4="$(gettext '
		Once a resource has been disabled or removed, the
		resource is no longer available to clients of that
		resource.
	')"
	typeset sctxt_remove_warning_p1="$(gettext '
		Remember that once a resource is removed, the resource
		is no longer available to clients of that resource.
		Clients which depend upon that resource will experience
		a loss of service.
	')"

	typeset tmpfile=${CLSETUP_TMP_RSLIST}
	typeset cachefile=${CLSETUP_RGM_CACHEFILE}

	typeset nopause
	typeset answer
	typeset answer2
	typeset prompt
	typeset prompt2
	typeset header1
	typeset header2
	typeset rglist
	typeset rgname
	typeset rslist
	typeset resource
	typeset dep
	typeset deps
	typeset item
	typeset item2
	typeset list
	typeset rs_dep
	typeset rs_dep_weak
	typeset rs_dep_restart
	typeset rs_dep_offline_restart
	typeset nru

	integer commands=0
	integer found
	integer index
	integer disabled

	if [[ -n "${delete}" ]]; then
		nohelp=1
		nopause=1
		resource=${delete}
	fi

	# Unless nohelp is specified, print help and get confirmation
	if [[ -z "${nohelp}" ]]; then

		# Clear screen, print title and help
		sc_print_title "${sctxt_title}"
		sc_print_para "${sctxt_p1}"
		sc_print_para "${sctxt_p2}"
		sc_print_para "${sctxt_p3}"
		sc_print_para "${sctxt_p4}"

		# Okay to continue?
		clsetup_continue || return 1
	fi

	# Initialize
	prompt="$(gettext 'Select the resource you want to remove:')"
	header1="$(printf "%-25s %s" "$(gettext 'Resource Name')" "$(gettext 'Resource Group')")"
	header2="$(printf "%-25s %s" "=============" "==============")"

	# Loop until done or Ctrl-D is typed
	while [[ -z "${delete}" ]]
	do
		# Get the rglist
		if [[ ${CLSETUP_ISMEMBER} -eq 1 ]] ||
		    [[ -r "${cachefile}" ]]; then
			rglist="$(clsetup_get_rglist "" "" "" ${cachefile})"
		else
			rglist="rg1 rg2 rg3 rg4 rg5 rg6 rg7 rg8 rg9 rg10 rg11 rg12 rg13 rg14"
		fi

		# Create the temp file
		rm -f ${tmpfile}
		let found=0
		for rgname in ${rglist}
		do
			if [[ ${CLSETUP_ISMEMBER} -eq 1 ]];  then
				rslist="$(${SCHA_RG_GET} -G ${rgname} -O resource_list)"
			else
				rslist="${rgname}res1 ${rgname}res2 ${rgname}res3 ${rgname}res4"
			fi
			for resource in ${rslist}
			do
				((found += 1))
				printf "%-25s %s\n" "${resource}" "${rgname}"
			done
		done > ${tmpfile}

		# If the list is empty, we are done
		if [[ ${found} -eq 0 ]]; then
			echo
			printf "$(gettext 'There are no resources configured.')\n"
			printf "$(gettext 'So, there is nothing to remove.')\n\n\a"
			sc_prompt_pause
			rm -f ${tmpfile}
			return 1
		fi

		# Get the name of the resource to delete
		resource="$(
		    sc_get_scrolling_menuoptions	\
			"${prompt}"			\
			"${header1}"			\
			"${header2}"			\
			0 1 1				\
			:${tmpfile}			\
		)"
		rm -f ${tmpfile}

		# Done?
		if [[ -z "${resource}" ]]; then
			return 0
		fi

		# Confirm
		sc_print_para "${sctxt_remove_warning_p1}"
		prompt2="$(printf "$(gettext 'Are you sure you want to remove \"%s\"?')" "${resource}")"
		answer=$(sc_prompt_yesno "${prompt2}" "${YES}") || return 1
		if [[ "${answer}" == "yes" ]]; then
			delete=${resource}
		else
			prompt2="$(gettext 'Do you want to select a different resource to remove?')"
			answer=$(sc_prompt_yesno "${prompt2}" "${NO}") || return 1
			if [[ "${answer}" != "yes" ]]; then
				return 1
			fi
		fi
	done

	# Cleanup explicit dependencies
	set -A CLSETUP_DO_CMDSET
	let index=0
	deps=
	if [[ ${CLSETUP_ISMEMBER} -eq 1 ]]; then
		deps="$(clsetup_get_resource_depended_ons ${resource})"
	fi

	# If the resource is enabled, it must first be disabled.
	let disabled=0
	answer=
	if [[ ${CLSETUP_ISMEMBER} -eq 1 ]]; then
		clsetup_is_resource_enabled ${delete} ""
		# Return value:
		#       0 - the resource is disabled on all the nodes.
		#       1 - the resource is enabled on all the nodes.
		#       2 - the resource is enabled on some of the nodes and disabled on others.
		if [[ $? -ne 0 ]]; then
			sc_print_para "$(printf "$(gettext '\"%s\" is still enabled.  You cannot remove a resource as long it remains enabled.')" "${delete}")"
			prompt2="$(printf "$(gettext 'Is it okay to disable \"%s\"?')" "${delete}")"
			answer=$(sc_prompt_yesno "${prompt2}" "${YES}") || return 1
		fi
	fi

	if [[ ! -z ${deps} ]]; then
		sc_print_para "$(printf "$(gettext 'One or more resources on the system have dependencies on the resource \"%s\".')" "${delete}")"
		prompt2="$(gettext 'Do you want to remove the dependencies?')"
		answer2=$(sc_prompt_yesno "${prompt2}" "${YES}") || return 1
	fi

	# Resetting the Resource_dependency{strong, weak, restart, offline_restart} properties to remove the dependency.
	if [[ ! -z ${deps} ]]; then
		if [[ "${answer2}" == "yes" ]]; then
			for dep in ${deps}
			do
				rs_dep=
				rs_dep_weak=
				rs_dep_restart=
				rs_dep_offline_restart=
				nru=

				# Resource_dependencies
				let found=0
				list="$(${SCHA_RS_GET} -R ${dep} -O resource_dependencies -Q)"
				for item in ${list}
				do
					item2=`echo ${item} | awk -F{ '{print $1}'`
					if [[ "${item2}" == "${delete}" ]]; then
						found=1
						continue
					fi
					if [[ -n "${rs_dep}" ]]; then
						rs_dep="${rs_dep},"
					fi
					rs_dep="${rs_dep}${item}"
				done
				if [[ ${found} -eq 1 ]]; then
					${SCHA_RS_GET} -R ${dep} -O Network_resources_used > /dev/null 2>&1 
					if [[ $? -eq 0 ]]; then
						list="$(${SCHA_RS_GET} -R ${dep} -O Network_resources_used)"
						for item in ${list}
						do
							if [[ "${item}" == "${delete}" ]]; then
								continue
							fi
							if [[ -n "${nru}" ]]; then
								nru="${nru}"
							fi
							nru="${nru}${item}"
						done
						CLSETUP_DO_CMDSET[index]="${CL_CMD_CLRESOURCE} set -p Resource_dependencies=${rs_dep} -p Network_resources_used=${nru} ${dep}"
					else
						CLSETUP_DO_CMDSET[index]="${CL_CMD_CLRESOURCE} set -p Resource_dependencies=${rs_dep} ${dep}"
					fi
					((index += 1))
					((commands += 1))
				fi

				# Resource_dependencies_weak
				found=0
				list="$(${SCHA_RS_GET} -R ${dep} -O resource_dependencies_weak -Q)"
				for item in ${list}
				do
					item2=`echo ${item} | awk -F{ '{print $1}'`
					if [[ "${item2}" == "${delete}" ]]; then
						found=1
						continue
					fi
					if [[ -n "${rs_dep_weak}" ]]; then
						rs_dep_weak="${rs_dep_weak},"
					fi
					rs_dep_weak="${rs_dep_weak}${item}"
				done
				if [[ ${found} -eq 1 ]]; then
					CLSETUP_DO_CMDSET[index]="${CL_CMD_CLRESOURCE} set -p Resource_dependencies_weak=${rs_dep_weak} ${dep}"
					((index += 1))
					((commands += 1))
				fi

				# Resource_dependencies_restart
				found=0
				list="$(${SCHA_RS_GET} -R ${dep} -O resource_dependencies_restart -Q)"
				for item in ${list}
				do
					item2=`echo ${item} | awk -F{ '{print $1}'`
					if [[ "${item2}" == "${delete}" ]]; then
						found=1
						continue
					fi
					if [[ -n "${rs_dep_restart}" ]]; then
						rs_dep_restart="${rs_dep_restart},"
					fi
					rs_dep_restart="${rs_dep_restart}${item}"
				done
				if [[ ${found} -eq 1 ]]; then
					CLSETUP_DO_CMDSET[index]="${CL_CMD_CLRESOURCE} set -p Resource_dependencies_restart=${rs_dep_restart} ${dep}"
					((index += 1))
					((commands += 1))
				fi

				# Resource_dependencies_offline_restart
				found=0
				list="$(${SCHA_RS_GET} -R ${dep} -O resource_dependencies_offline_restart -Q)"
				for item in ${list}
				do
					item2=`echo ${item} | awk -F{ '{print $1}'`
					if [[ "${item2}" == "${delete}" ]]; then
						found=1
						continue
					fi
					if [[ -n "${rs_dep_offline_restart}" ]]; then
						rs_dep_offline_restart="${rs_dep_offline_restart},"
					fi
					rs_dep_offline_restart="${rs_dep_offline_restart}${item}"
				done
				if [[ ${found} -eq 1 ]]; then
					CLSETUP_DO_CMDSET[index]="${CL_CMD_CLRESOURCE} set -p Resource_dependencies_offline_restart=${rs_dep_offline_restart} ${dep}"
					((index += 1))
					((commands += 1))
				fi
			done

			if [[ ${commands} -gt 1 ]]; then
				clsetup_do_cmdset 1 "${nopause}" "${commands}" || return 1
			else
				clsetup_do_cmdset 1 "${nopause}" || return 1
			fi
		else
			printf "$(gettext 'It is not possible to delete the resource without removing the dependencies.')\n\n\a"				
			sc_prompt_pause	
			return 1
		fi
	fi
	commands=0

	if [[ "${answer}" == "yes" ]]; then
		clsetup_disable_resource ${delete} ""
		if [[ $? -eq 0 ]]; then
			((commands += 1))
			let disabled=1
		fi
		if [[ ${disabled} -ne 1 ]]; then
			printf "$(gettext '\"%s\" must be disabled before it can be removed.')\n\n\a" "${delete}"
			sc_prompt_pause
			return 1
		fi
	fi

	# Re-initialize
	set -A CLSETUP_DO_CMDSET

	# Add the command to remove the resource
	CLSETUP_DO_CMDSET[0]="${CL_CMD_CLRESOURCE} delete ${delete}"

	# Do it
	if [[ ${commands} -gt 0 ]]; then	
		clsetup_do_cmdset 1 "${nopause}" "${commands}" || return 1
	else
		clsetup_do_cmdset 1 "${nopause}" || return 1
	fi

	return 0
}

#####################################################
#
# clsetup_enable_resource() resource nodelist
#
#	Enable a resource on the specified nodelist.
#	If nodelist is empty, the resource is enabled on
#	all the nodes.
#	
#	Dependencies are not required to be checked before
#	enabling a resource.
#
#	Return:
#		zero		Completed
#		non-zero	Not completed
#
#####################################################
clsetup_enable_resource()
{
	typeset resource=${1}
	typeset nodelist=${2}

	# Enable the resource on a particular nodelist or on all the nodes.
	set -A CLSETUP_DO_CMDSET
	if [[ -z ${nodelist} ]];then
		CLSETUP_DO_CMDSET[0]="${CL_CMD_CLRESOURCE} enable ${resource}"
	else
		CLSETUP_DO_CMDSET[0]="${CL_CMD_CLRESOURCE} enable -n ${nodelist} ${resource}"
	fi

	clsetup_do_cmdset 1 1 || return 1

	return 0
}

#####################################################
#
# clsetup_disable_resource() resource nodelist
#
#	Disable a resource on the specified nodelist.
#	If nodelist is empty, resource is disabled on all the nodes.
#
#	Disabling a dependant resource is not required before
#	a dependee resource is disabled. So this function 
#	does not care about dependencies before disabling.
#
#	Return:
#		zero		Completed
#		non-zero	Not completed
#
#####################################################
clsetup_disable_resource()
{
	typeset resource=${1}
	typeset nodelist=${2}

	# Disable the resource on the specified nodes. If no nodes are specified
	# disable the resource on all the nodes.
	set -A CLSETUP_DO_CMDSET
	if [[ -z ${nodelist} ]];then
		CLSETUP_DO_CMDSET[0]="${CL_CMD_CLRESOURCE} disable ${resource}"
	else
		CLSETUP_DO_CMDSET[0]="${CL_CMD_CLRESOURCE} disable -n ${nodelist} ${resource}"
	fi
	clsetup_do_cmdset 1 1 || return 1

	return 0
}

#####################################################
#
# clsetup_disable_resources() rgname
#
#	Attempt to disable all resources in a resource group.
#	
#	Dependency checks are not required when disabling a
#	resource.
#
#	Return:
#		zero		Completed
#		non-zero	Not successful
#
#####################################################
clsetup_disable_resources()
{
	typeset rgname=${1}

	typeset resource
	typeset rslist

	integer count

	# Must be a cluster member
	if [[ ${CLSETUP_ISMEMBER} -ne 1 ]];  then
		return 0
	fi

	# Get the resource list for this group
	rslist="$(${SCHA_RG_GET} -G ${rgname} -O resource_list)"

	# Disable each resource
	let count=0
	for resource in ${rslist}
	do
		# Make sure it is still disabled, since
		# clsetup_disable_resource() can give the user the
		# oportunity to disable more resources.
		clsetup_is_resource_enabled ${resource} ""
		if [[ $? -eq 0 ]]; then
			continue
		fi

		# Disable the resource
		clsetup_disable_resource ${resource} ""
		if [[ $? -ne 0 ]]; then
			printf "$(gettext 'Failed to disable \"%s\".')\n" "${resource}"
			((count += 1))
		fi
	done

	if [[ ${count} -gt 0 ]]; then
		echo "\a"
	fi

	return ${count}
}

#####################################################
#
# clsetup_menu_rgm_rs_enable_disable() [nohelp]
#
#	nohelp		- if this flag is given, do not
#			    clear screen, print title, print help,
#			    or ask for confirmation to continue
#
#	Enable/disable a resource.
#
#	Return:
#		zero		Completed
#		non-zero	Not completed
#
#####################################################
clsetup_menu_rgm_rs_enable_disable()
{
	typeset nohelp=${1}

	typeset sctxt_title="$(gettext '
		>>> Enable or Disable a Resource <<<
	')"
	typeset sctxt_p1="$(gettext '
		Use this option to either enable or disable a resource.
	')"
	typeset sctxt_p2="$(gettext '
		Once a resource is enabled in an online resource group,
		the resource becomes available to clients.
	')"
	typeset sctxt_p3="$(gettext '
		Once a resource is disabled, the resource is no longer
		available to clients of that resource.
	')"
	typeset sctxt_p4="$(gettext '
		This resource is partially enabled on some nodes. Use
		the following option to specify the type of state change
		to be performed on the resource.
	')"
	typeset sctxt_disable_warning_p1="$(gettext '
		Remember that once a resource is disabled, the resource
		is no longer available to clients of that resource.
		Clients which depend upon that resource will experience
		a loss of service.
	')"

	typeset tmpfile=${CLSETUP_TMP_RSLIST}

	typeset prompt
	typeset header1
	typeset header2

	typeset prompt2
	typeset answer
	typeset resource
	typeset state

	typeset prompt3
	typeset prompt4
	typeset clusternodes=
	typeset clusternode=
	typeset nodelist=

	integer found

	# Unless nohelp is specified, print help and get confirmation
	if [[ -z "${nohelp}" ]]; then

		# Clear screen, print title and help
		sc_print_title "${sctxt_title}"
		sc_print_para "${sctxt_p1}"
		sc_print_para "${sctxt_p2}"
		sc_print_para "${sctxt_p3}"

		# Okay to continue?
		clsetup_continue || return 1
	fi

	# Loop until done or Ctrl-D is typed
	while true
	do
		# Initialize
		prompt="$(gettext 'Select the resource for which you want to make a state change:')"
		header1="$(printf "%-25s %-16.16s" "$(gettext 'Resource Name')" "$(gettext 'Resource State')")"
		header2="$(printf "%-25s %-16.16s" "=============" "==============")"

		# Get the list of resources
		if [[ ${CLSETUP_ISMEMBER} -eq 1 ]]; then
			rslist="$(clsetup_get_rslist "" "" "")"
		else
			rslist="res1 res2 res3 res4 res5 res6 res7 res8 res9 res10"
		fi

		# Create the temp file
		rm -f ${tmpfile}
		let found=0
		for resource in ${rslist}
		do
			if [[ ${CLSETUP_ISMEMBER} -eq 1 ]];  then
				clsetup_is_resource_enabled ${resource} ""
				case $? in
				2)	state="Partially enabled" ;;
				1)	state="Enabled" ;;
				0)	state="Disabled" ;;
				*)	state=
					((found -= 1))
					continue
					;;
				esac
			else
				state="<DEBUG>"
			fi
			((found += 1))
			printf "%-25s %-20.20s\n" "${resource}" "${state}"
		done > ${tmpfile}

		# Are there any resources?
		if [[ ${found} -lt 1 ]]; then
			echo
			printf "$(gettext 'There are no resources configured.')\n"
			printf "$(gettext 'So, there is nothing to Enable/Disable.')\n\n\a"
			sc_prompt_pause
			rm -f ${tmpfile}
			return 1
		fi

		# Get the name of the resource to change
		resource="$(
		    sc_get_scrolling_menuoptions	\
			"${prompt}"			\
			"${header1}"			\
			"${header2}"			\
			0 1 1				\
			:${tmpfile}			\
		)"
		rm -f ${tmpfile}

		# Done?
		if [[ -z "${resource}" ]]; then
			return 0
		fi

		prompt="$(printf "$(gettext 'Do you require to change the state of resource \"%s\" on individual nodes or zones that can master the resource ?')" "${resource}")"
		answer=$(sc_prompt_yesno "${prompt}" "${NO}") || continue
		if [[ "${answer}" == "yes" ]]; then
			clsetup_menu_rgm_rs_pernode_enable_disable "${resource}"
			continue
		fi

		# Enable, partially enable or disable?
		if [[ ${CLSETUP_ISMEMBER} -eq 1 ]]; then
			clsetup_is_resource_enabled ${resource} ""
			case $? in
			2)	state="Partially Enabled" ;;
			1)	state="Enabled"
				# Make the state to be changed as Disabled.
				rsstate="Disable"
				;;
			0)	state="Disabled"
				# Make the state to be changed as Enabled.
				rsstate="Enable"
				;;
			*)	state= ;;
			esac
		else
			state="Debug"
		fi

		# Partially Enabled.
		if [[ "${state}" == "Partially Enabled" ]]; then
			prompt="$(gettext 'Select the type of state to be performed on the resource:')"
			rsstate="$(
		    	sc_get_scrolling_menuoptions	\
				"${prompt}"			\
				"" ""				\
				1 1 1				\
				"Enable"			\
				"Disable"			\
			)"
			if [[ -z "${rsstate}" ]]; then
				return 1
			fi
		fi
		# Enable or disable?
		if [[ "${rsstate}" == "Disable" ]] ||
		    [[ "${rsstate}" == "Debug" ]]; then
			sc_print_para "${sctxt_disable_warning_p1}"
			prompt2="$(printf "$(gettext 'Are you sure you want to disable \"%s\"?')" "${resource}")"
			answer=$(sc_prompt_yesno "${prompt2}" "${YES}") || continue
			if [[ "${answer}" == "yes" ]]; then
				clsetup_disable_resource ${resource} ""
				sc_prompt_pause
				continue
			fi
		fi
		if [[ "${rsstate}" == "Enable" ]] ||
		    [[ "${rsstate}" == "Debug" ]]; then
			prompt2="$(printf "$(gettext 'Do you want to enable \"%s\"?')" "${resource}")"
			answer=$(sc_prompt_yesno "${prompt2}" "${YES}") || continue
			if [[ "${answer}" == "yes" ]]; then
				clsetup_enable_resource ${resource} ""
				sc_prompt_pause
				continue
			fi
		fi
		prompt2="$(gettext 'Do you want to select a different resource?')"
		answer=$(sc_prompt_yesno "${prompt2}" "${NO}") || return 1
		if [[ "${answer}" != "yes" ]]; then
			return 1
		fi
	done

	return 0
}

#####################################################
#
# clsetup_menu_rgm_rs_pernode_enable_disable() rsname
#
#	rsname
#
#	Enable/disable a resource on a per node basis.
#
#	Return:
#		zero		Completed
#		non-zero	Not completed
#
#####################################################
clsetup_menu_rgm_rs_pernode_enable_disable()
{
	typeset resource=${1}
	typeset rgname
	typeset nodes
	typeset node
	typeset prompt

	let found=0

	typeset sctxt_p4="$(gettext '
		This resource is partially enabled on some nodes. Use
		the following option to specify the type of state change
		to be performed on the resource.
	')"

	typeset sctxt_title_2="$(gettext 'Select one option from the following list:')"
	typeset sctxt_option_001="$(gettext 'Show per node resource status')"
	typeset sctxt_option_002="$(gettext 'Disable the resource on a per node basis')"
	typeset sctxt_option_003="$(gettext 'Enable the resource on a per node basis')"
	typeset sctxt_option_return="$(gettext 'Return to the previous menu')"

	typeset option

	# Return
	while true
	do
		let found=0
		option=$(sc_get_menuoption \
			"T2+++${sctxt_title_2}" \
			"S+0+1+${sctxt_option_001}" \
			"S+0+2+${sctxt_option_002}" \
			"S+0+3+${sctxt_option_003}" \
			"R+++" \
			"S+0+q+${sctxt_option_return}" \
		)

		case ${option} in
		1)	printf "%4.4s$(gettext 'Resource state for \"%s\"')\n\n" "" "${resource}"
			printf "%9.9s%-25s %-16.16s\n" "" "$(gettext 'Node Name')" "$(gettext 'Resource State')"
			printf "%9.9s%-25s %-16.16s\n\n" "" "=============" "==============="

			rgname="$(${SCHA_RS_GET} -O Group -R "${resource}")"
			nodes="$(${SCHA_RG_GET} -G ${rgname} -O nodelist)"
			for node in ${nodes}
			do
				if [[ ${CLSETUP_ISMEMBER} -eq 1 ]];  then
					clsetup_is_resource_enabled "${resource}" "${node}"
					case $? in
					1)	state="Enabled" ;;
					0)	state="Disabled" ;;
					*)	state=
						((found -= 1))
						continue
						;;
					esac
				else
					state="<DEBUG>"
				fi
				printf "%7.7s%2.2s) %-25s %-16.16s\n" "" "$((found+1))" "${node}" "${state}"
				((found +=1))
			done
			sc_prompt_pause
			continue
			;;
		## Disable resource per node
		2)	prompt="$(gettext 'Select each node or zone where the resource is to be disabled')"
			nodelist=$(clsetup_menu_rgm_rs_nodelist "" "${resource}" "${prompt}") || continue

			sc_print_para "${sctxt_disable_warning_p1}"
			prompt="$(printf "$(gettext 'Are you sure you want to disable \"%s\"?')" "${resource}")"
			answer=$(sc_prompt_yesno "${prompt}" "${YES}") || continue
			if [[ "${answer}" == "yes" ]]; then
				clsetup_disable_resource "${resource}" "${nodelist}"
				nodelist=
				sc_prompt_pause
				continue
			fi
			;;
		## Enable resource per node
		3)	 prompt="$(gettext 'Select each node or zone where the resource is to be enabled')"
			nodelist=$(clsetup_menu_rgm_rs_nodelist "" "${resource}" "${prompt}") || continue

			prompt="$(printf "$(gettext 'Do you want to enable \"%s\"?')" "${resource}")"
			answer=$(sc_prompt_yesno "${prompt}" "${YES}") || continue
			if [[ "${answer}" == "yes" ]]; then
				clsetup_enable_resource "${resource}" "${nodelist}"
				nodelist=
				sc_prompt_pause
				continue
			fi
			;;
		## Return to previous menu.
		q)	return
			;;
		*)	continue
			;;
		esac
	done
}

#####################################################
#
# clsetup_menu_rgm_rs_clear() [nohelp]
#
#	nohelp		- if this flag is given, do not
#			    clear screen, print title, print help,
#			    or ask for confirmation to continue
#
#	Clear resource STOP_FAILED error flags.
#
#	Return:
#		zero		Completed
#		non-zero	Not completed
#
#####################################################
clsetup_menu_rgm_rs_clear()
{
	typeset nohelp=${1}

	typeset sctxt_title="$(gettext '
		>>> Clear the STOP_FAILED Error Flag from a Resource <<<
	')"
	typeset sctxt_p1="$(gettext '
		Use this option to clear the STOP_FAILED error flag
		from a resource.  clClearing the STOP_FAILED error flag
		places the resource into the offline state on the
		specified nodes.
	')"

	typeset tmpfile=${CLSETUP_TMP_RSLIST}
	typeset cachefile=${CLSETUP_RGM_CACHEFILE}

	typeset prompt
	typeset header1
	typeset header2
	typeset answer
	typeset rslist
	typeset resource
	typeset nodelist
	typeset nodename

	integer found

	# Unless nohelp is specified, print help and get confirmation
	if [[ -z "${nohelp}" ]]; then

		# Clear screen, print title and help
		sc_print_title "${sctxt_title}"
		sc_print_para "${sctxt_p1}"

		# Okay to continue?
		clsetup_continue || return 1
	fi

	# Initialize
	prompt="$(gettext 'Select the resource whose STOP_FAILED flag you want to clear:')"
	header1="$(printf "%-25s %-14s %-20s" "$(gettext 'Resource Name')" "$(gettext 'Error Flag')" "$(gettext 'Node')")"
	header2="$(printf "%-25s %-14s %-20s" "=============" "==========" "====")"

	# Loop until done or Ctrl-D is typed
	while true
	do
		# Get the list of resources
		if [[ ${CLSETUP_ISMEMBER} -eq 1 ]] ||
		    [[ -r "${cachefile}" ]];  then
			rslist="$(clsetup_get_rslist "" "" ${cachefile})"
		else
			rslist="res1 res2 res3 res4"
		fi

		# Create the temp file
		rm -f ${tmpfile}
		let found=0
		for resource in ${rslist}
		do
			if [[ ${CLSETUP_ISMEMBER} -eq 1 ]];  then
				nodelist="$(clsetup_is_resource_stop_failed ${resource})"
			else
				nodelist="node1 node2"
			fi
			for nodename in ${nodelist}
			do
				((found += 1))
				printf "%-25s %-14s %-20s\n" "${resource}" "STOP_FAILED" "${nodename}"
			done
		done > ${tmpfile}

		# Are there any resources?
		if [[ ${found} -lt 1 ]]; then
			echo
			printf "$(gettext 'There are no resources with the STOP_FAILED error flag set.')\n"
			printf "$(gettext 'So, there is nothing to clear.')\n\n\a"
			sc_prompt_pause
			rm -f ${tmpfile}
			return 1
		fi

		# Get the name of the resource to clear
		answer="$(
		    sc_get_scrolling_menuoptions	\
			"${prompt}"			\
			"${header1}"			\
			"${header2}"			\
			0 1 3				\
			:${tmpfile}			\
		)"
		rm -f ${tmpfile}

		# Done?
		if [[ -z "${answer}" ]]; then
			return 0
		fi
		resource=$(IFS=: ; set -- ${answer}; echo ${1})
		nodename=$(IFS=: ; set -- ${answer}; if [[ -z "${4}" ]]; then echo ${3}; else echo "${3}:${4}"; fi)

		# Clear STOP_FAILED
		set -A CLSETUP_DO_CMDSET
		CLSETUP_DO_CMDSET[0]="${CL_CMD_CLRESOURCE} clear -n ${nodename} -f STOP_FAILED ${resource}"
		clsetup_do_cmdset || return 1
		break
	done

	return 0
}

#####################################################
#
# clsetup_get_suspend_resume_menuoption rgname rgsuspeneded
#
#	rgname		- name of the resource group
#	rgsuspeneded	- "Yes"/"No". The groups is suspended or not
#
#	Print the resource group suspend-resume menu, and return the
#	selected option.
#
#	If "rgsuspended" is "Yes", the following menu is used:
#		1)	Resume automatic recovery actions for the group
#
#	If "rgsuspended" is "No", the following menu is used:
#		1)	Suspend automated recovery actions for the group
#		2)	Fast suspend the group by killing any running methods
#
#	Returned options are:
#		1)	Suspend automated recovery actions for the group
#		2)	Fast suspend the group by killing any running methods
#		3)	Resume automatic recovery actions for the group
#
#	This function always returns zero.
#
#####################################################
clsetup_get_suspend_resume_menuoption()
{
	typeset rgname=${1}
	typeset rgsuspended=${2}

	typeset sctxt_title_1="$(gettext 'Please select from one of the following options:')"
	typeset sctxt_option_suspend="$(gettext 'Suspend automated recovery actions for the group')"
	typeset sctxt_option_fastsuspend="$(gettext 'Fast suspend the group by killing running methods')"
	typeset sctxt_option_resume="$(gettext 'Resume automatic recovery actions for the group')"

	typeset sctxt_option_quit="$(gettext 'Return to the resource group menu')"

	typeset option

	case ${rgsuspended} in
	Yes)
		option=$(sc_get_menuoption \
			"T2+++${sctxt_title_1}" \
			"S+0+1+${sctxt_option_resume}" \
			"R+++" \
			"S+0+q+${sctxt_option_quit}" \
		)
		case ${option} in
		1)	option=3 ;;
		esac
		;;
	No)
		option=$(sc_get_menuoption \
			"T2+++${sctxt_title_1}" \
			"S+0+1+${sctxt_option_suspend}" \
			"S+0+2+${sctxt_option_fastsuspend}" \
			"R+++" \
			"S+0+q+${sctxt_option_quit}" \
		)
		;;
	esac

	echo "${option}"

	return 0
}

#####################################################
#
# clsetup_menu_rgm_rg_suspend_resume() [nohelp]
#
#	nohelp		- if this flag is given, do not
#			  clear screen, print title, print help,
#			  or ask for confirmation to continue
#
#	Suspend/Resume a resource group.
#
#	Return:
#		zero		Completed
#		non-zero	Not completed
#
#####################################################
clsetup_menu_rgm_rg_suspend_resume()
{
	typeset nohelp=${1}

	typeset sctxt_title="$(gettext '
		>>> Suspend/Resume Recovery for a Resource Group <<<
	')"
	typeset sctxt_p1="$(gettext '
			Use this option to suspend or resume the cluster framework
			automated recovery actions for a resource group.  Once a
			resource group is suspended, no automatic start, failover,
			or restart will be performed on the group or its resources.
	')"
	typeset sctxt_p2="$(gettext "
			Fast suspend can be performed on a resource group to quickly
			bring the group to a quiescent state. This option quickly suspends the
			automated recovery actions for the group by killing any
			methods that are currently executing on the group's resources.
			This action might leave resources in the Error_stop_failed state,
			and these resources might need manual cleanup.
	")"
	typeset sctxt_p3="$(gettext '
			Only the automated recovery actions are suppressed on a
			suspended resource group. Some cluster commands (for example,
			clresourcegroup(1M)) can still be used to manually switch the group
			or its resources online, offline, or to a different master.
	')"
	typeset sctxt_p4="$(gettext '
			A suspended resource group can be brought back under automated
			cluster control by the resume operation.
	')"
	typeset sctxt_suspend_warning_p1="$(gettext '
			Remember that once a resource group is suspended, the cluster framework
			cannot perform any automated recovery actions on the group
			and its resources. You can still use some cluster commands
			to manually manage the group or its resources, but automatic
			failover, restart, or failback of the group is no longer
			available to this group.
	')"
	typeset sctxt_fastsuspend_warning_p1="$(gettext '
			Fast suspend kills any methods that are currently executing
			on the resources in this group. This option quickly suspends
			the cluster automated recovery actions on the group. This
			action might leave resources in the Error_stop_failed state,
			and these resources might need manual cleanup. The group
			may be switched offline or restarted manually using cluster commands.
	')"
	typeset sctxt_resume_warning_p1="$(gettext '
			The resume operation brings the resource group back to the
			cluster controlled automated recovery actions.
	')"

	typeset tmpfile=${CLSETUP_TMP_RGLIST}
	typeset rgstatefile=${CLSETUP_TMP_RGSTATE}

	typeset rglist
	typeset prompt
	typeset header1
	typeset header2
	typeset answer
	typeset operation
	typeset rgname
	typeset rgtype
	typeset rgsuspended
	typeset -l lsuspended		# lower case
	typeset -l lrgstatelist		# lower case
	typeset -l lmainstate		# lower case
	typeset -l lrgstate		# lower case
	typeset rgstate
	typeset foo
	typeset foo2
	typeset enrgsuspended

	integer found
	integer index

	# Unless nohelp is specified, print help and get confirmation
	if [[ -z "${nohelp}" ]]; then

		# Clear screen, print title and help
		sc_print_title "${sctxt_title}"
		sc_print_para "${sctxt_p1}"
		sc_print_para "${sctxt_p2}"
		sc_print_para "${sctxt_p3}"
		sc_print_para "${sctxt_p4}"

		# Okay to continue?
		clsetup_continue || return 1
	fi

	# Loop until done or Ctrl-D is typed
	while true
	do
		# Get the list of resource groups
		rglist="$(clsetup_get_rglist)"

		# Create the rgstate file
		rm -f ${rgstatefile} 2>/dev/null
		clsetup_create_rgstatefile ${rgstatefile}

		# Create the temp file
		rm -f ${tmpfile}
		let found=0
		for rgname in ${rglist}
		do
			lmainstate=
			lrgstatelist=" $(clsetup_get_rgstate ${rgname} "${rgstatefile}" "allnodes")"

			# Unmanaged?
			expr "${lrgstatelist}" : '.*\(unmanaged\)' >/dev/null && lmainstate="unmanaged"

			# Stop_Failed anywhere?
			if [[ -z "${lmainstate}" ]]; then
				expr "${lrgstatelist}" : '.*\(error--stop_failed\)' >/dev/null && lmainstate="error--stop_failed"
			fi

			# Online anywhere?
			if [[ -z "${lmainstate}" ]]; then
				expr "${lrgstatelist}" : '.*\([^_]online\)' >/dev/null && lmainstate="online"
			fi

			# Must be offline
			if [[ -z "${lmainstate}" ]]; then
				lmainstate=offline
			fi
			rgstate=$(clsetup_rgstate_string ${lmainstate})

			# If mainstate is online, don't add more offline
			if [[ "${lmainstate}" == "online" ]]; then
				foo2=
			else
				foo2=offline
			fi

			# Add any additional state information
			for foo in online pending_online ${foo2} pending_offline unmanaged error--stop_failed
			do
				if [[ "${foo}" == "${lmainstate}" ]]; then
					continue
				fi
				lrgstate=
				if [[ "${foo}" == "online" ]] ||
				    [[ "${foo}" == "offline" ]]; then
					expr "${lrgstatelist}" : '.*\([^_]'${foo}'\)' >/dev/null && lrgstate=${foo}
				else
					expr "${lrgstatelist}" : '.*\('${foo}'\)' >/dev/null && lrgstate=${foo}
				fi
				if [[ -n "${lrgstate}" ]]; then
					rgstate="${rgstate}, $(clsetup_rgstate_string ${lrgstate})"
				fi
			done

			rgtype="$(clsetup_get_rgtype ${rgname})"

			# Suspended?
			rgsuspended="$(gettext 'No')"
			lsuspended="$(${SCHA_RG_GET} -G ${rgname} -O Suspend_automatic_recovery)"
			if [[ "${lsuspended}" == "true" ]]; then
				rgsuspended="$(gettext 'Yes')"
			fi

			((found += 1))
			printf "%-20s %-10.10s %-10.10s %s\n" "${rgname}" "${rgtype}" "${rgsuspended}" "${rgstate}"
		done > ${tmpfile}

		# Done with rgstate file
		clsetup_rm_rgstatefile ${rgstatefile}

		# Are there any resource groups?
		if [[ ${found} -lt 1 ]]; then
			echo
			printf "$(gettext 'There are no resource groups configured.')\n\n\a"
			sc_prompt_pause
			rm -f ${tmpfile}
			return 1
		fi

		# Initialize headers
		prompt="$(gettext 'Select the resource group you want to change:')"
		header1="$(printf "%-20s %-10.10s %-10.10s %s" "$(gettext 'Group Name')" "$(gettext 'Type')" "$(gettext 'Suspended')" "$(gettext 'State(s)')")"
		header2="$(printf "%-20s %-10.10s %-10.10s %s" "==========" "====" "=========" "==================")"

		# Get the name of the resource group to change
		answer="$(
		    sc_get_scrolling_menuoptions \
		    "${prompt}"			\
		    "${header1}"		\
		    "${header2}"		\
		    0 1 4			\
		    :${tmpfile}			\
		)"
		rm -f ${tmpfile}

		# Done?
		if [[ -z "${answer}" ]]; then
			return 0
		fi

		# Break out rgname and enrgsuspended
		enrgsuspended="No"
		rgname=$(IFS=: ; set -- ${answer}; echo ${1})
		lsuspended="$(${SCHA_RG_GET} -G ${rgname} -O Suspend_automatic_recovery)"
		if [[ "${lsuspended}" == "true" ]]; then
			enrgsuspended="Yes"
		fi 

		case $(clsetup_get_suspend_resume_menuoption "${rgname}" "${enrgsuspended}") in
		1)	operation="suspend" ;;
		2)	operation="fastsuspend" ;;
		3)	operation="resume" ;;
		q)	break ;;
		*)	continue ;;
		esac

		# Suspend/resume the rg
		while [[ -n "${operation}" ]]
		do
			case ${operation} in
			suspend)
				operation=
				# Print suspend warning
				sc_print_para "${sctxt_suspend_warning_p1}"

				# Okay to continue?
				prompt="$(printf "$(gettext 'Are you sure you want to suspend \"%s\"?')" "${rgname}")"
				answer=$(sc_prompt_yesno "${prompt}" "${YES}") || break
				if [[ "${answer}" != "yes" ]]; then
					break
				fi

			 	# Initialize command set
				set -A CLSETUP_DO_CMDSET
				let index=0

				CLSETUP_DO_CMDSET[index]="${CL_CMD_CLRESOURCEGROUP} suspend ${rgname}"
				clsetup_do_cmdset 1
				;;

			fastsuspend)
				operation=
				# Print fast suspend warning
				sc_print_para "${sctxt_fastsuspend_warning_p1}"

				# Okay to continue?
				prompt="$(printf "$(gettext 'Are you sure you want to do fast suspend on \"%s\"?')" "${rgname}")"
				answer=$(sc_prompt_yesno "${prompt}" "${YES}") || break
				if [[ "${answer}" != "yes" ]]; then
					break
				fi

			 	# Initialize command set
				set -A CLSETUP_DO_CMDSET
				let index=0

				CLSETUP_DO_CMDSET[index]="${CL_CMD_CLRESOURCEGROUP} suspend -k ${rgname}"
				clsetup_do_cmdset 1
				;;
			resume)
				operation=
				# Print resume warning
				sc_print_para "${sctxt_resume_warning_p1}"

				# Okay to continue?
				prompt="$(printf "$(gettext 'Are you sure you want to resume \"%s\"?')" "${rgname}")"
				answer=$(sc_prompt_yesno "${prompt}" "${YES}") || break
				if [[ "${answer}" != "yes" ]]; then
					break
				fi

			 	# Initialize command set
				set -A CLSETUP_DO_CMDSET
				let index=0

				CLSETUP_DO_CMDSET[index]="${CL_CMD_CLRESOURCEGROUP} resume ${rgname}"
				clsetup_do_cmdset 1
				;;
			*)
				prompt="$(gettext 'Unable to suspend or resume a group.')"
				sc_print_para "${prompt}"
				sc_prompt_pause
				;;
			esac
		done
	done

	return 0
}

#####################################################
#
# clsetup_menu_rgm_status()
#
#	Print status
#
#	This function always returns zero.
#
#####################################################
clsetup_menu_rgm_status()
{
	typeset tmpfile=${CLSETUP_TMP_RGMSTATUS}

	echo
	if [[ ${CLSETUP_ISMEMBER} -eq 1 ]]; then
		rm -f ${tmpfile}
                ${CL_CMD_CLRESOURCEGROUP} status >${tmpfile} 2>/dev/null
                ${CL_CMD_CLRESOURCE} status >>${tmpfile} 2>/dev/null
		if [[ -s "${tmpfile}" ]]; then
			more ${tmpfile}
		else
			printf "$(gettext 'There are no configured resource groups.')\n\n\a"
		fi
		rm -f ${tmpfile}
	else
		echo "DEBUG:  Not a cluster member - no status."
	fi
	sc_prompt_pause

	return 0
}

#####################################################
#
# clsetup_help_rgm_menu()
#
#	Print help information from the rgm menu.
#	This is the same help information as is printed
#	from the MAIN RGM help menu.  But, we ask for confirmation
#	before returning to the previous menu.
#
#	This function always returns zero.
#
#####################################################
clsetup_help_rgm_menu()
{
	clear
	clsetup_help_main_rgm
	sc_prompt_pause

	return 0
}

#####################################################
#
# clsetup_get_rgm_menuoption()
#
#	Print the RGM menu, and return the selected option.
#
#	This function always returns zero.
#
#####################################################
clsetup_get_rgm_menuoption()
{
	typeset sctxt_title_1="$(gettext '*** Resource Group Menu ***')"
	typeset sctxt_title_2="$(gettext 'Please select from one of the following options:')"
	typeset sctxt_option_001="$(gettext 'Create a resource group')"
	typeset sctxt_option_002="$(gettext 'Add a network resource to a resource group')"
	typeset sctxt_option_003="$(gettext 'Add a data service resource to a resource group')"
	typeset sctxt_option_004="$(gettext 'Resource type registration')"
	typeset sctxt_option_005="$(gettext 'Online/Offline or Switchover a resource group')"
	typeset sctxt_option_006="$(gettext 'Suspend/Resume recovery for a resource group')"
	typeset sctxt_option_007="$(gettext 'Enable/Disable a resource')"
	typeset sctxt_option_008="$(gettext 'Change properties of a resource group')"
	typeset sctxt_option_009="$(gettext 'Change properties of a resource')"
	typeset sctxt_option_010="$(gettext 'Remove a resource from a resource group')"
	typeset sctxt_option_011="$(gettext 'Remove a resource group')"
	typeset sctxt_option_012="$(gettext 'Clear the stop_failed error flag from a resource')"
	typeset sctxt_option_help="$(gettext 'Help')"
	typeset sctxt_option_status="$(gettext 'Show current status')"
	typeset sctxt_option_return="$(gettext 'Return to the main menu')"

	typeset option

	# Return
	option=$(sc_get_menuoption \
		"T1+++${sctxt_title_1}" \
		"T2+++${sctxt_title_2}" \
		"S+0+1+${sctxt_option_001}" \
		"S+0+2+${sctxt_option_002}" \
		"S+0+3+${sctxt_option_003}" \
		"S+0+4+${sctxt_option_004}" \
		"S+0+5+${sctxt_option_005}" \
		"S+0+6+${sctxt_option_006}" \
		"S+0+7+${sctxt_option_007}" \
		"S+0+8+${sctxt_option_008}" \
		"S+0+9+${sctxt_option_009}" \
		"S+0+10+${sctxt_option_010}" \
		"S+0+11+${sctxt_option_011}" \
		"S+0+12+${sctxt_option_012}" \
		"R+++" \
		"S+0+?+${sctxt_option_help}" \
		"S+0+s+${sctxt_option_status}" \
		"S+0+q+${sctxt_option_return}" \
	)

	echo "${option}"

	return 0
}

#####################################################
#
# clsetup_menu_rgm()
#
#	RGM menu.
#
#	This function always returns zero.
#
#####################################################
clsetup_menu_rgm()
{
	typeset cachefile=${CLSETUP_RGM_CACHEFILE}

	# Loop around the RGM RG menu
	while true
	do
		# Just to be safe, clean up any possible cachefiles
		if [[ ${CLSETUP_ISMEMBER} -eq 1 ]]; then
			clsetup_rm_rgmfile ${cachefile}
		fi

		# Do it
		case $(clsetup_get_rgm_menuoption) in
		'1')	clsetup_menu_rgm_rg_create ;;
		'2')	clsetup_menu_rgm_rs_addnet ;;
		'3')	clsetup_menu_rgm_rs_addds ;;
		'4')	clsetup_menu_rgm_rt_registration ;;
		'5')	clsetup_menu_rgm_rg_online_offline ;;
		'6')	clsetup_menu_rgm_rg_suspend_resume;;
		'7')	clsetup_menu_rgm_rs_enable_disable ;;
		'8')	clsetup_menu_rgm_rg_changeproperties ;;
		'9')	clsetup_menu_rgm_rs_changeproperties ;;
		'10')	clsetup_menu_rgm_rs_delete ;;
		'11')	clsetup_menu_rgm_rg_delete ;;
		'12')	clsetup_menu_rgm_rs_clear ;;
		'?')	clsetup_help_rgm_menu ;;
		's')	clsetup_menu_rgm_status ;;
		'q')	break ;;
                esac
	done

	return 0
}

#####################################################
#####################################################
##
## Cluster transport
##
#####################################################
#####################################################

#####################################################
#
# clsetup_prompt_transport_adapter() "[prompt]"
#
#	Prompt for the transport adapter name, then
#	perform simple verification.  If "prompt" is not given,
#	a default prompt is used.
#
#	The prompt is printed on file descriptor 4, and the answer
#	is printed to stdout.  File descriptor 4 should be
#	dupped to the original stdout before this function is called.
#
#	Return:
#		zero		Completed
#		non-zero	Not completed
#
#####################################################
clsetup_prompt_transport_adapter()
{
	typeset prompt="${1}"

	typeset dflt_prompt="$(gettext 'Name of adapter?')"
	typeset answer

	# If no prompt is given, use the default
	if [[ -z "${prompt}" ]]; then
		prompt="${dflt_prompt}"
	fi

	#
	# The caller of this function should have already opened
	# descriptor 4 as a dup of the original stdout.
	#
	# Dup this function's stdout to descriptor 5.
	# Then, re-direct stdout to descriptor 4.
	#
	# So, the default stdout from this function will go to
	# the original stdout (probably the tty).  Descriptor 5
	# has the answer printed to it.
	#
	exec 5>&1
	exec 1>&4

	# Loop until we get the value or Ctrl-D is typed
	while true
	do
		# Get the adapter name
		answer=$(sc_prompt "${prompt}") || return 1

		# Adapter name must be <device_name><device_instance>
		# where <device_instance> is numeric
		if [[ "${answer}" = *:* ]] ||
		    [[ $(expr "${answer}" : '[a-z][a-z0-9]*[0-9][0-9]*') -ne ${#answer} ]]; then
			printf "$(gettext 'Invalid adapter name.')\n\n\a"
			continue
		fi

		# okay
		echo ${answer} >&5
		break
	done

	return 0
}

#####################################################
#
# clsetup_prompt_vlanid() "[prompt]"
#
#	Prompt for the transport adapter vlanid, then
#	perform simple verification.  If "prompt" is not given,
#	a default prompt is used.
#
#	The prompt is printed on file descriptor 4, and the answer
#	is printed to stdout.  File descriptor 4 should be
#	dupped to the original stdout before this function is called.
#
#	Return:
#		zero		Completed
#		non-zero	Not completed
#
#####################################################
clsetup_prompt_vlanid()
{
	typeset prompt="${1}"

	typeset dflt_prompt="$(gettext 'What is the VLAN ID for this adapter?')"
	typeset answer

	# If no prompt is given, use the default
	if [[ -z "${prompt}" ]]; then
		prompt="${dflt_prompt}"
	fi

	#
	# The caller of this function should have already opened
	# descriptor 4 as a dup of the original stdout.
	#
	# Dup this function's stdout to descriptor 5.
	# Then, re-direct stdout to descriptor 4.
	#
	# So, the default stdout from this function will go to
	# the original stdout (probably the tty).  Descriptor 5
	# has the answer printed to it.
	#
	exec 5>&1
	exec 1>&4

	# Loop until we get the value or Ctrl-D is typed
	while true
	do
		# Get the adapter name
		answer=$(sc_prompt "${prompt}") || return 1

		# VLAN id must be numeric and between 1 and 4094
		if [[ $(expr "${answer}" : '[0-9]*') -ne ${#answer} ]]; then
			printf "$(gettext '\"%s\" is not numeric.')\n\n\a" "${answer}"
			continue
		fi
		if [[ ${answer} -lt 1 ]] || [[ ${answer} -gt 4094 ]]; then
			printf "$(gettext 'VLAN ID is out of range, valid values are between 1 and 4094.')\n\n\a"
			continue
		fi

		# okay
		echo ${answer} >&5
		break
	done

	return 0
}

#####################################################
#
# clsetup_prompt_transport_junction() "[prompt]"
#
#	Prompt for the transport junction(or switch) name, then
#	perform simple verification.  If "prompt" is not given,
#	a default prompt is used.
#
#	Note : Junction and Switch are used interchangeable in the
#	code, However switch is prefered term that is displayed to
#	users in error strings and help messages.
#
#	The prompt is printed on file descriptor 4, and the answer
#	is printed to stdout.  File descriptor 4 should be
#	dupped to the original stdout before this function is called.
#
#	Return:
#		zero		Completed
#		non-zero	Not completed
#
#####################################################
clsetup_prompt_transport_junction()
{
	typeset prompt="${1}"

	typeset dflt_prompt="$(gettext 'Name of cluster transport switch?')"
	typeset answer

	# If no prompt is given, use the default
	if [[ -z "${prompt}" ]]; then
		prompt="${dflt_prompt}"
	fi

	#
	# The caller of this function should have already opened
	# descriptor 4 as a dup of the original stdout.
	#
	# Dup this function's stdout to descriptor 5.
	# Then, re-direct stdout to descriptor 4.
	#
	# So, the default stdout from this function will go to
	# the original stdout (probably the tty).  Descriptor 5
	# has the answer printed to it.
	#
	exec 5>&1
	exec 1>&4

	# Loop until we get the value or Ctrl-D is typed
	while true
	do
		# Get the switch name
		answer=$(sc_prompt "${prompt}") || return 1

		# Switch name must begin with alpha and may not include : or @.
		if [[ "${answer}" = [!a-zA-Z]* ]] ||
		    [[ "${answer}" = *[@:]* ]]; then
			printf "$(gettext 'Invalid switch name.')\n\n\a"
			continue
		fi

		# Switch name must be <= 256 characters
		if [[ ${#answer} -gt 256 ]]; then
			printf "$(gettext 'Switch name is too long.')\n\n\a"
			continue
		fi

		# okay
		echo ${answer} >&5
		break
	done

	return 0
}

#####################################################
#
# clsetup_prompt_transport_endpoint()
#
#	Get the adapter endpoint of an existing transport cable.
#
#	The prompt is printed on file descriptor 4, and the answer
#	is printed to stdout.  File descriptor 4 should be
#	dupped to the original stdout before this function is called.
#
#	Return:
#		zero		Completed
#		non-zero	Not completed
#
#####################################################
clsetup_prompt_transport_endpoint()
{
	typeset answer

	typeset sctxt_p1="$(gettext '
		Each existing cluster transport cable can be
		identified by naming one of the cable endpoints.
		Since one of the two endpoints is always attached to a
		node, you will be asked to supply the node and adapter
		names of one of the endpoints of the cable you are
		trying to identify.
	')"

	integer found
	typeset nodelist
	typeset nodename
	typeset adapterlist
	typeset adaptername
	typeset adapter
	typeset endpointlist
	typeset endpoint
	typeset prompt
	typeset answer
	typeset curr_driver
	typeset curr_inst
 	typeset curr_physinst
	typeset curr_physadp

	#
	# The caller of this function should have already opened
	# descriptor 4 as a dup of the original stdout.
	#
	# Dup this function's stdout to descriptor 5.
	# Then, re-direct stdout to descriptor 4.
	#
	# So, the default stdout from this function will go to
	# the original stdout (probably the tty).  Descriptor 5
	# has the answer printed to it.
	#
	exec 5>&1
	exec 1>&4

	# Print help
	sc_print_para "${sctxt_p1}"

	# Loop until we get the value or Ctrl-D is typed
	while true
	do
		# Get nodelist
		nodelist=
		if [[ ${CLSETUP_ISMEMBER} -eq 1 ]]; then

			# Get nodelist
			nodelist="$(clsetup_get_nodelist)"

			# Make sure we have a nodelist
			if [[ -z "${nodelist}" ]]; then
				printf "$(gettext 'Unable to establish the list of cluster nodes.')\n\n\a"
				sc_prompt_pause
				return 1
			fi
		fi

		# Get the node name for the endpoint
		answer=$(clsetup_prompt_nodename "${nodelist}" "$(gettext 'To which node is the cable attached?')") || return 1
		nodename=${answer}

		# Get the adaptername
		prompt="$(printf "$(gettext 'Name of the adapter on \"%s\"?')" "${nodename}")"
		while true
		do
			# Prompt for adapter name
			answer=$(clsetup_prompt_transport_adapter "${prompt}") || continue 2

			# Get the list of configured adapters for this node
			adapterlist=
			if [[ ${CLSETUP_ISMEMBER} -eq 1 ]]; then
				adapterlist="$(clsetup_get_adapterlist ${nodename})"
			fi

			# See if this adapter is configured
			let found=0
			for adapter in ${adapterlist}
			do
				if [[ "${answer}" = "${adapter}" ]]; then
					let found=1
					break
				fi
				curr_driver=$(expr ${adapter} : '\([a-z0-9]*[a-z]\)')
				curr_inst=$(expr ${adapter} : '[a-z0-9]*[a-z]\([0-9]*\)')
				if [[ ${curr_inst} -ge ${SC_VLAN_MULTIPLIER} ]]; then
					curr_physinst=$(expr ${curr_inst} % ${SC_VLAN_MULTIPLIER} )
					curr_physadp=${curr_driver}${curr_physinst}
					if [[ "${answer}" = "${curr_physadp}" ]]; then
						let found=1
						break
					fi
				fi
			done

			#
			# If there is a adapterlist, but the adapter
			# is not found, print error.
			#
			if [[ -n "${adapterlist}" ]] && [[ ${found} -eq 0 ]]; then
				printf "$(gettext 'Unknown adapter.')\n\n\a"
				continue
			fi

			# Okay
			adaptername=${answer}
			break
		done

		# Get the list of configured endpoints
		endpointlist=
		if [[ ${CLSETUP_ISMEMBER} -eq 1 ]]; then
			endpointlist="$(clsetup_get_endpoints)"
		fi

		# See if this endpoint is configured
		let found=0
		answer="${nodename}:${adaptername}"
		for endpoint in ${endpointlist}
		do
			if [[ "${answer}" = "${endpoint}" ]]; then
				let found=1
				break
			fi
		done
		answer="${nodename}:${adaptername}"

		#
		# If there is an endpointlist, but the endpoint
		# is not found, print error.
		#
		if [[ -n "${endpointlist}" ]] && [[ ${found} -eq 0 ]]; then
			printf "$(gettext 'Unknown cable.')\n\n\a"
			continue
		fi

		# okay
		echo ${answer} >&5
		break
	done

	return 0
}

#####################################################
#
# clsetup_menu_transport_addcable() [nohelp]
#
#	nohelp		- if this flag is given, do not
#			    clear screen, print title, print help,
#			    or ask for confirmation to continue
#
#	Add a cluster transport cable to the cluster, along with
#	any necessary adapters and switches used to create the cable.
#
#	Return:
#		zero		Completed
#		non-zero	Not completed
#
#####################################################
clsetup_menu_transport_addcable()
{
	typeset nohelp=${1}

	typeset sctxt_title="$(gettext '
		>>> Add a Cluster Transport Cable <<<
	')"
        typeset sctxt_p1="$(gettext '
		Each cluster transport cable either directly connects
		two cluster nodes or connects a cluster node to a port
		on a transport switch.  Only two-node clusters may
		use directly-connected cables;  all cables on clusters
		with more than two nodes must be cabled to switches.
	')"
	typeset sctxt_p2="$(gettext '
		This option is used to add a new cluster transport
		cable to the cluster configuration.  If either of the
		two endpoints of the cable do not already exist,
		they will also be added to the configuration.
	')"
	typeset sctxt_p1_directconnect="$(gettext '
		Since this is a two-node cluster, direct-connect cabling
		of the transport cables might be supported.  Refer to the
		documentation for your cluster transport adapters for
		more information about whether your hardware supports
		direct-connect cabling.  Some adapter types require
		the use of switches even on two-node clusters.
	')"
	typeset sctxt_p2_directconnect="$(gettext '
		Note that direct-connect cabling can complicate the
		later addition of a third node to the cluster.
	')"
	typeset sctxt_p1_selected_direct="$(gettext '
		Since the cable you are adding is a direct-connect
		cable, each cable endpoint must be attached directly to
		an adapter on each of the two nodes in the cluster.
		You will be asked to supply the names of each of these
		two adapters.
	')"
	typeset sctxt_p1_not_directconnect="$(gettext '
		Since this is not a two-node cluster, all transport cables
		must have one end connected to a transport adapter on a node
		and the other end connected to a port on a cluster transport
		switch.  
	')"
	typeset sctxt_p1_selected_not_direct="$(gettext '
		Since the cable you are adding is not a direct-connect cable,
		one end must be attached to a node and the other attached
		to a cluster transport switch.  You will be asked to
		supply the name of the node and adapter for the first
		endpoint, as well as the name of the switch and port
		for the second endpoint.
	')"
	typeset sctxt_p1_selected_singlenode_not_direct="$(gettext '
		This is a single-node cluster.  You will be asked to supply
		the name of the adapter on this node for the first endpoint, 
		as well as the name of the junction and port for the second
		endpoint.
	')"
	typeset sctxt_p1_dfltport="$(gettext '
		Each adapter is cabled to a particular port on a
		transport switch, and each port is assigned a
		name.  You may explicitly assign a name to each port,
		or, for certain switch types, you may allow clsetup
		to assign a default name for you.   The default port
		name assignment sets the name to the node number of the
		node hosting the transport adapter at the other end of
		the cable.
	')"
        typeset sctxt_p2_dfltport="$(gettext '
		Please remember that some types of cluster transport
		switched are not compatible with the default port
		naming assignments.  For more information regarding
		port naming requirements, refer to the
		scconf_transp_jct family of man pages (for example,
		scconf_transp_jct_dolphinswitch(1M)).
	')"
	typeset sctxt_prompt_dfltport="$(gettext '
		Okay to use the default for this cable connection?
	')"

	typeset sctxt_no_wildcat="$(gettext '
		WCI transport adapter  is not supported for cluster transport
	')"
	integer twonodes
	integer singlenode
	integer directconnect
	integer index1
	integer index2
	integer found
	integer thisvlanid
	integer vlan_allowed
	integer thisadapterinst
	typeset trtype
	typeset list
	typeset nodelist
	typeset adapterlist
	typeset junctionlist
	typeset node
	typeset adapter
	typeset junction
	typeset endpoint_nodelist
	typeset endpoint_adapterlist
	typeset endpoint_junction
	typeset endpoint_junctionport
	typeset answer
	typeset prompt
	typeset thisadapterdriver
	typeset thisadapter
	typeset curr_driver
	typeset curr_inst
	typeset curr_physinst
	typeset curr_physadp
	typeset adaptername
	typeset default_clpl
	typeset driver_clpl

	#
	# Print title
	#
	if [[ -z "${nohelp}" ]]; then
		clear
		sc_print_title "${sctxt_title}"
		sc_print_para "${sctxt_p1}"
		sc_print_para "${sctxt_p2}"

		# Okay to continue?
		clsetup_continue || return 1
	fi

	# Loop until done or Ctrl-D is typed
	while true
	do
		# Initialize
		let twonodes=0
		let singlenode=0
		let directconnect=0
		trtype=
		nodelist=
		adapterlist=
		endpoint_nodelist=
		endpoint_adapterlist=

		# Get nodelist, twonodes, and trtype
		if [[ ${CLSETUP_ISMEMBER} -eq 1 ]]; then

			# Get nodelist
			nodelist="$(clsetup_get_nodelist)"

			# Make sure we have a nodelist
			if [[ -z "${nodelist}" ]]; then
				printf "$(gettext 'Unable to establish the list of cluster nodes.')\n\n\a"
				sc_prompt_pause
				return 1
			fi

			# Set "twonodes" based on number of nodes in node list
			if [[ $(set -- ${nodelist}; echo $#) -eq 2 ]]; then
				let twonodes=1
			fi

			# Set "singlenode" based on number of nodes in node list
			if [[ $(set -- ${nodelist}; echo $#) -eq 1 ]]; then
				let singlenode=1
			fi

			# Get trtype
			trtype="$(clsetup_get_trtypes)"

			# Make sure we have only one transport type
			if [[ -z "${trtype}" ]]; then
				printf "$(gettext 'Unable to determine transport type.')\n\n\a"
				sc_prompt_pause
				return 1
			fi
			if [[ $(set -- ${trtype}; echo $#) -gt 1 ]]; then
				printf "$(gettext 'clsetup does not support clusters with mixed transport types.')\n\n\a"
				sc_prompt_pause
				return 1
			fi
		else
			# We must be running in DEBUG mode
			answer=$(sc_prompt_yesno "$(gettext 'DEBUG:  Is this a two-node cluster?')" "${YES}") || return 1
			if [[ "${answer}" = "yes" ]]; then
				let twonodes=1

				#
				# Since this is just DEBUG mode, we do
				# not error-check node name responses.
				#
				answer=$(sc_prompt "$(gettext 'DEBUG:  What is the name of the first node?')" "node1") || return 1
				nodelist="${answer}"

				answer=$(sc_prompt "$(gettext 'DEBUG:  What is the name of the second node?')" "node2") || return 1
				nodelist="${nodelist} ${answer}"
			fi

			# Since this is just DEBUG mode, do not check trtype
			answer=$(sc_prompt "$(gettext 'DEBUG:  What transport type are you using?')" "dlpi") || return 1
			trtype=${answer}
		fi

		# Two node clusters may be directly connected.
		if [[ ${twonodes} -eq 1 ]]; then
			sc_print_para "${sctxt_p1_directconnect}"
			sc_print_para "${sctxt_p2_directconnect}"
			answer=$(sc_prompt_yesno "$(gettext 'Are you adding a directly-connected cable?')" "${NO}") || return 1
			if [[ "${answer}" = "yes" ]]; then
				let directconnect=1
			fi
		else
			sc_print_para "${sctxt_p1_not_directconnect}"
		fi

		# Direct connect
		if [[ ${directconnect} -eq 1 ]]; then

			# Print help
			sc_print_para "${sctxt_p1_selected_direct}"

			# Get the two adapters for the two nodes
			endpoint_nodelist="${nodelist}"
			endpoint_adapterlist=
			for node in ${endpoint_nodelist}
			do
				# Get the adapter name
				while true
				do
					prompt="$(printf "$(gettext 'Name of the adapter to use on \"%s\"?')" "${node}")"
					answer=$(clsetup_prompt_transport_adapter "${prompt}") || return 1
					thisadapterdriver=$(expr ${answer} : '\([a-z]*\)')
					if [[ "${thisadapterdriver}" = "${CLSETUP_WILDCAT_ADAPTER}" ]]; then
						printf "$(gettext 'WCI transport adapters is not supported.')\n\n\a"
						continue;
					fi
					break
				done

				# add it to the list
				endpoint_adapterlist="${endpoint_adapterlist} ${answer}"
			done

		# NOT direct connect
		else
			# Print help
			if [[ ${singlenode} -eq 1 ]]; then
				sc_print_para "${sctxt_p1_selected_singlenode_not_direct}"
				endpoint_nodelist=${nodelist}
			else	
				sc_print_para "${sctxt_p1_selected_not_direct}"

				# Get the node name for first endpoint
				answer=$(clsetup_prompt_nodename "${nodelist}" "$(gettext 'To which node do you want to add the cable?')") || return 1
				endpoint_nodelist=${answer}
			fi

			# Get the adapter name for first endpoint
			prompt="$(printf "$(gettext 'Name of the adapter to use on \"%s\"?')" "${endpoint_nodelist}")"
			answer=$(clsetup_prompt_transport_adapter "${prompt}") || return 1
			endpoint_adapterlist="${endpoint_adapterlist} ${answer}"

			# if Wildcat, we know the Junction name
			thisadapter=${answer}
			thisadapterdriver=$(expr ${answer} : '\([a-z]*\)')

			if [[ "${thisadapterdriver}" = "${CLSETUP_WILDCAT_ADAPTER}" ]]; then
				sc_print_para "${sctxt_no_wildcat}"
				continue

			# Otherwise, the user specifies the Junction/Switch name
			else
				# Get the name of the switch for the second endpoint
				answer=$(clsetup_prompt_transport_junction "$(gettext 'Name of the switch at the other end of the cable?')") || return 1
			fi

			endpoint_junction=${answer}
			# Use default junction ports?
			sc_print_para "${sctxt_p1_dfltport}"
			sc_print_para "${sctxt_p2_dfltport}"
			answer=$(sc_prompt_yesno "${sctxt_prompt_dfltport}" "${YES}") || return 1

			# If answer is yes, set port to "@"
			if [[ "${answer}" = "yes" ]]; then
				answer="@"

			# otherwise, prompt for the name
			else
				answer=$(sc_prompt "$(gettext 'What is the name of the port you want to use?')") || return 1
			fi
			endpoint_junctionport=${answer}
		fi

		#
		# Set up the command set
		#

		# Initialize
		set -A CLSETUP_DO_CMDSET
		let index1=0

		# Create any needed adapters
		set -A list ${endpoint_adapterlist}
		let index2=0
		for node in ${endpoint_nodelist}
		do
			# Get the list of configured adapters for this node
			adapterlist=
			if [[ ${CLSETUP_ISMEMBER} -eq 1 ]]; then
				adapterlist="$(clsetup_get_adapterlist ${node})"
			fi

			# See if our adapter is already configured
			let found=0
			for adapter in ${adapterlist}
			do
				if [[ "${list[index2]}" = "${adapter}" ]]; then
					let found=1
					break
				fi
				curr_driver=$(expr ${adapter} : '\([a-z0-9]*[a-z]\)')
				curr_inst=$(expr ${adapter} : '[a-z0-9]*[a-z]\([0-9]*\)')
				if [[ ${curr_inst} -ge ${SC_VLAN_MULTIPLIER} ]]; then
					curr_physinst=$(expr ${curr_inst} % ${SC_VLAN_MULTIPLIER} )
					curr_physadp=${curr_driver}${curr_physinst}
					if [[ "${list[index2]}" = "${curr_physadp}" ]]; then
						let found=1
						break
					fi
				fi
			done

			# If not, add command to create adapter
			if [[ ${found} -eq 0 ]]; then
				adaptername="${list[index2]}"
				thisadapterdriver=$(expr ${adaptername} : '\([a-z0-9]*[a-z]\)')
				thisadapterinst=$(expr ${adaptername} : '[a-z0-9]*[a-z]\([0-9]*\)')
				vlan_allowed=0
				thisvlanid=0
				default_clpl=/etc/cluster/clpl/SUNW.adapter.default.clpl
				driver_clpl=/etc/cluster/clpl/SUNW.adapter.${thisadapterdriver}.clpl
				if [[ ${thisadapterinst} -lt ${SC_VLAN_MULTIPLIER} ]]; then
					if [[ -f ${driver_clpl} ]]; then
						grep "^adapters.${thisadapterdriver}.vlan_id" ${driver_clpl} 2>&1 >/dev/null
						if [[ $? -eq 0 ]]; then
							vlan_allowed=1
						fi
					elif [[ -f ${default_clpl} ]]; then
						grep "^adapters.${thisadapterdriver}.vlan_id" ${default_clpl} 2>&1 >/dev/null
						if [[ $? -eq 0 ]]; then
							vlan_allowed=1
						fi
					fi
				fi

				if [[ ${vlan_allowed} -eq 1 ]]; then
					sc_print_line "$(printf "$(gettext 'Creating adapter \"%s\" on node \"%s\".')" "${list[index2]}" "${node}")"
					echo
					echo
					answer=$(sc_prompt_yesno "$(gettext 'Will this be a dedicated cluster transport adapter?')" "${YES}") || return 1
					if [[ "${answer}" != "yes" ]]; then
						prompt="$(printf "$(gettext 'What is the cluster transport VLAN ID for this adapter?')")"
						thisvlanid=$(clsetup_prompt_vlanid "${prompt}") || continue
					fi
				fi

				if [[ ${thisvlanid} -eq 0 ]]; then
					CLSETUP_DO_CMDSET[index1]="${CL_CMD_CLINTERCONNECT} add ${node}:${list[index2]}"
				else
					# Clinterconnect accepts as a part of adapter name
					thisvlanid=$(expr ${thisvlanid} \* ${SC_VLAN_MULTIPLIER})
					thisvlanid=$(expr ${thisvlanid} + ${thisadapterinst})

					CLSETUP_DO_CMDSET[index1]="${CL_CMD_CLINTERCONNECT} add ${node}:${thisadapterdriver}${thisvlanid}"
				fi
				((index1 += 1))
			fi

			# Next
			((index2 += 1))
		done

		# Create any needed junctions(or switches)
		if [[ -n ${endpoint_junction} ]]; then

			# Get the list of configured junctions
			junctionlist=
			if [[ ${CLSETUP_ISMEMBER} -eq 1 ]]; then
				junctionlist="$(clsetup_get_junctionlist)"
			fi

			# See if our junction is already configured
			let found=0
			for junction in ${junctionlist}
			do
				if [[ "${endpoint_junction}" = "${junction}" ]]; then
					let found=1
					break
				fi
			done

			# If not, add command to create junction
			if [[ ${found} -eq 0 ]]; then
                                # add the switch here
				CLSETUP_DO_CMDSET[index1]="${CL_CMD_CLINTERCONNECT} add ${endpoint_junction}"
				((index1 += 1))
			fi
		fi

		# Add command to create cable
		CLSETUP_DO_CMDSET[index1]="${CL_CMD_CLINTERCONNECT}  add"
		set -A list ${endpoint_adapterlist}
		let index2=0

		# Add all node endpoints
		for node in ${endpoint_nodelist}
		do
			# Add adapter endpoint to command
			if [[ index2 -eq 0 ]]; then
				CLSETUP_DO_CMDSET[index1]="${CLSETUP_DO_CMDSET[index1]} ${node}:${list[index2]}"
			else
				CLSETUP_DO_CMDSET[index1]="${CLSETUP_DO_CMDSET[index1]},${node}:${list[index2]}"
			fi
			# Next
			((index2 += 1))
		done

		# Add the junction endpoint
		if [[ -n "${endpoint_junction}" ]]; then
			CLSETUP_DO_CMDSET[index1]="${CLSETUP_DO_CMDSET[index1]},${endpoint_junction}"

			# And, the junction endpoint port
			if [[ -n "${endpoint_junctionport}" ]] &&
			    [[ "${endpoint_junctionport}" != "@" ]]; then
				CLSETUP_DO_CMDSET[index1]="${CLSETUP_DO_CMDSET[index1]}@${endpoint_junctionport}"
			fi
		fi

		# Attempt to issue the command(s)
		clsetup_do_cmdset || return 1

		# Done
		break
	done

	return 0
}

#####################################################
#
# clsetup_menu_transport_addjunction() [nohelp]
#
#	nohelp		- if this flag is given, do not
#				clear screen, print title, print help,
#				or ask for confirmation to continue.
#
#	Add a cluster transport junction to the cluster.
#
#	Return:
#		zero		Completed
#		non-zero	Not completed
#
#####################################################
clsetup_menu_transport_addjunction()
{
	typeset nohelp=${1}

	typeset sctxt_title="$(gettext '
		>>> Add a Cluster Transport Switch <<<
	')"
	typeset sctxt_p1="$(gettext '
		This option is used to add a new cluster transport
		switch to the cluster configuration.
	')"

	typeset junctionlist
	typeset junctionname
	typeset junction
	typeset answer
	typeset prompt

	#
	# Print title
	#
	if [[ -z "${nohelp}" ]]; then
		clear
		sc_print_title "${sctxt_title}"
		sc_print_para "${sctxt_p1}"

		# Okay to continue?
		clsetup_continue || return 1
	fi

	# Loop until done or Ctrl-D is typed
	while true
	do
		# Get the junctionname
		while true
		do
			# Prompt for junction name
			answer=$(clsetup_prompt_transport_junction "$(gettext 'Name of the switch to add?')") || return 1

			# Get the list of configured junctions
			junctionlist=
			if [[ ${CLSETUP_ISMEMBER} -eq 1 ]]; then
				junctionlist="$(clsetup_get_junctionlist)"
			fi

			# See if our junction is already configured
			for junction in ${junctionlist}
			do
				if [[ "${answer}" = "${junction}" ]]; then
					printf "$(gettext 'Switch \"%s\" is already configured on \"%s\".')\n\n\a" "${answer}" "${nodename}"
					continue 2
				fi
			done

			# Okay
			junctionname=${answer}
			break
		done

		#
		# Set up the command set
		#

		# Initialize
		set -A CLSETUP_DO_CMDSET

		# Add the command to add the switch here
		CLSETUP_DO_CMDSET[0]="${CL_CMD_CLINTERCONNECT} add ${junctionname}"

		# Attempt to issue the command
		clsetup_do_cmdset || return 1

		# Done
		break
	done

	return 0
}

#####################################################
#
# clsetup_menu_transport_addadapter() [nohelp]
#
#	nohelp		- if this flag is given, do not
#				clear screen, print title, print help,
#				or ask for confirmation to continue.
#
#	Add a cluster transport adapter to the cluster.
#
#	Return:
#		zero		Completed
#		non-zero	Not completed
#
#####################################################
clsetup_menu_transport_addadapter()
{
	typeset nohelp=${1}

	typeset sctxt_title="$(gettext '
		>>> Add a Cluster Transport Adapter to a Node <<<
	')"
	typeset sctxt_p1="$(gettext '
		This option is used to add a new cluster transport
		adapter to the cluster configuration.
	')"
	typeset sctxt_p1_adapterdata="$(gettext '
		You will be asked to supply both the name of the adapter
		and the name of the node on which the adapter is found.
	')"

	integer thisvlanid
	integer vlan_allowed=0
	typeset nodelist
	typeset nodename
	typeset adapterlist
	typeset adaptername
	typeset adapter
	typeset trtype
	typeset answer
	typeset prompt
	typeset default_trtypes
	typeset vlanid
	typeset adapterdriver
	typeset thisadapterdriver
	typeset thisadapterinst
	typeset default_clpl
	typeset driver_clpl
	typeset curr_driver
	typeset curr_inst
	typeset curr_vlanid
	typeset curr_physinst
	typeset curr_physadp

	nodelist=
	let nodecount=0

	#
	# Print title
	#
	if [[ -z "${nohelp}" ]]; then
		clear
		sc_print_title "${sctxt_title}"
		sc_print_para "${sctxt_p1}"

		# Okay to continue?
		clsetup_continue || return 1
	fi

	# Loop until done or Ctrl-D is typed
	while true
	do
		# Get nodelist and trtype
		if [[ ${CLSETUP_ISMEMBER} -eq 1 ]]; then

			# Get nodelist
			nodelist="$(clsetup_get_nodelist)"

			# Make sure we have a nodelist
			if [[ -z "${nodelist}" ]]; then
				printf "$(gettext 'Unable to establish the list of cluster nodes.')\n\n\a"
				sc_prompt_pause
				return 1
			fi

			# Set node count
			let nodecount=$(set -- ${nodelist}; echo $#)

			# Get trtype
			trtype="$(clsetup_get_trtypes)"

			# Make sure we have only one transport type
			if [[ -z "${trtype}" ]]; then
				# Single node cluster
				if [[ ${nodecount} -eq 1 ]]; then
					# Default trtypes
					if [[ -f "${SC_SCADMINDIR}/defaults" ]]; then
						default_trtypes=$(grep '^SC_DFLT_KNOWN_TRTYPES=' \
						    ${SC_SCADMINDIR}/defaults | \
						    nawk -F'=' '{print $2}' 2>/dev/null)
					fi

					while true
					do
						trtype=$(sc_prompt "$(gettext 'What transport type are you using?')" "dlpi") || return 1
						if [[ -n "${default_trtypes}" ]] &&
						   [[ "${default_trtypes}" != *${trtype}* ]]; then
							printf "$(gettext 'Unknown transport type.')\n\n\a"
						else
							break
						fi
					done

				else
					printf "$(gettext 'Unable to determine transport type.')\n\n\a"
					sc_prompt_pause
					return 1
				fi
			fi
			if [[ $(set -- ${trtype}; echo $#) -gt 1 ]]; then
				printf "$(gettext 'clsetup does not support clusters with mixed transport types.')\n\n\a"
				sc_prompt_pause
				return 1
			fi
		else
			# Since this is just DEBUG mode, do not check trtype
			answer=$(sc_prompt "$(gettext 'DEBUG:  What transport type are you using?')" "dlpi") || return 1
			trtype=${answer}
		fi

		# Print help
		sc_print_para "${sctxt_p1_adapterdata}"

		# Get the nodename
		answer=$(clsetup_prompt_nodename "${nodelist}" "$(gettext 'To which node do you want to add the adapter?')") || return 1
		nodename=${answer}

		# Get the adaptername
		prompt="$(printf "$(gettext 'Name of the adapter to add to \"%s\"?')" "${nodename}")"
		while true
		do
			# Prompt for adapter name
			answer=$(clsetup_prompt_transport_adapter "${prompt}") || continue 2

			# Get the list of configured adapters for this node
			adapterlist=
			if [[ ${CLSETUP_ISMEMBER} -eq 1 ]]; then
				adapterlist="$(clsetup_get_adapterlist ${nodename})"
			fi

			# See if our adapter is already configured
			for adapter in ${adapterlist}
			do
				if [[ "${answer}" = "${adapter}" ]]; then
					printf "$(gettext 'Adapter \"%s\" is already configured on \"%s\".')\n\n\a" "${answer}" "${nodename}"
					continue 2
				fi

				curr_driver=$(expr ${adapter} : '\([a-z0-9]*[a-z]\)')
				curr_inst=$(expr ${adapter} : '[a-z0-9]*[a-z]\([0-9]*\)')
				if [[ ${curr_inst} -ge ${SC_VLAN_MULTIPLIER} ]]; then
					curr_vlanid=$(expr ${curr_inst} / ${SC_VLAN_MULTIPLIER} )
					curr_physinst=$(expr ${curr_inst} % ${SC_VLAN_MULTIPLIER} )
					curr_physadp=${curr_driver}${curr_physinst}
					if [[ "${answer}" = "${curr_physadp}" ]]; then
						printf "$(gettext 'Adapter \"%s\" is already configured on \"%s\" with VLAN ID \"%d\".')\n\n\a" "${answer}" "${nodename}" "${curr_vlanid}"
						continue 2
					fi
				fi
			done

			# Okay
			adaptername=${answer}

			thisadapterdriver=$(expr ${adaptername} : '\([a-z0-9]*[a-z]\)')
			thisadapterinst=$(expr ${adaptername} : '[a-z0-9]*[a-z]\([0-9]*\)')
			vlan_allowed=0
			default_clpl=/etc/cluster/clpl/SUNW.adapter.default.clpl
			driver_clpl=/etc/cluster/clpl/SUNW.adapter.${thisadapterdriver}.clpl
			if [[ ${thisadapterinst} -lt ${SC_VLAN_MULTIPLIER} ]]; then
				if [[ -f ${driver_clpl} ]]; then
					grep "^adapters.${thisadapterdriver}.vlan_id" ${driver_clpl} 2>&1 >/dev/null
					if [[ $? -eq 0 ]]; then
						vlan_allowed=1
					fi
				elif [[ -f ${default_clpl} ]]; then
					grep "^adapters.${thisadapterdriver}.vlan_id" ${default_clpl} 2>&1 >/dev/null
					if [[ $? -eq 0 ]]; then
						vlan_allowed=1
					fi
				fi
			fi

			if [[ ${vlan_allowed} -eq 1 ]]; then
				answer=$(sc_prompt_yesno "$(gettext 'Will this be a dedicated cluster transport adapter?')" "${YES}") || return 1
				if [[ "${answer}" == "yes" ]]; then
					break;
				fi
				prompt="$(printf "$(gettext 'What is the cluster transport VLAN ID for this adapter?')")"
				thisvlanid=$(clsetup_prompt_vlanid "${prompt}") || continue
			fi

			break
		done

		#
		# Set up the command set
		#

		# Initialize
		set -A CLSETUP_DO_CMDSET

		# Add the command
		if [[ ${thisvlanid} -eq 0 ]]; then
			CLSETUP_DO_CMDSET[0]="${CL_CMD_CLINTERCONNECT} add ${nodename}:${adaptername}"
		else
			thisvlanid=$(expr ${thisvlanid} \* ${SC_VLAN_MULTIPLIER})
			thisvlanid=$(expr ${thisvlanid} + ${thisadapterinst})

			CLSETUP_DO_CMDSET[0]="${CL_CMD_CLINTERCONNECT} add ${node}:${thisadapterdriver}${thisvlanid}"
		fi

		# Attempt to issue the command
		clsetup_do_cmdset || return 1

		# Done
		break
	done

	return 0
}

#####################################################
#
# clsetup_menu_transport_rmcable() [nohelp]
#
#	nohelp		- if this flag is given, do not
#				clear screen, print title, print help,
#				or ask for confirmation to continue.
#
#	Remove a cluster transport cable.
#
#	Return:
#		zero		Completed
#		non-zero	Not completed
#
#####################################################
clsetup_menu_transport_rmcable()
{
	typeset nohelp=${1}

	typeset sctxt_title="$(gettext '
		>>> Remove a Cluster Transport Cable <<<
	')"
	typeset sctxt_p1="$(gettext '
		This option is used to remove an existing
		cluster transport cable from the cluster configuration.
		However, the two endpoints of the cable will remain
		configured.
	')"
	typeset sctxt_p2="$(gettext '
		It is very important that you do not disable or remove
		the last remaining healthy connection to an active cluster
		node.  Please check the status of other cables before
		you proceed.
	')"

	typeset endpoint
	typeset otherendpoint

	#
	# Print title
	#
	if [[ -z "${nohelp}" ]]; then
		clear
		sc_print_title "${sctxt_title}"
		sc_print_para "${sctxt_p1}"
		sc_print_para "${sctxt_p2}"

		# Okay to continue?
		clsetup_continue || return 1
	fi

	# Loop until done or Ctrl-D is typed
	while true
	do
		# Get the endpoints.
		# The first end point can be retrieved from user
		# The other one can be automaticcaly determined
		#

		endpoint=$(clsetup_prompt_transport_endpoint) || return 1
		otherendpoint=$(clsetup_get_other_endpoint ${endpoint}) || return 1

		#
		# Set up the command set
		#

		# Initialize
		set -A CLSETUP_DO_CMDSET

		# Add the command
		CLSETUP_DO_CMDSET[0]="${CL_CMD_CLINTERCONNECT} remove ${endpoint},${otherendpoint}"

		# Attempt to issue the command
		clsetup_do_cmdset || return 1

		# Done
		break
	done

	return 0
}

#####################################################
#
# clsetup_get_other_endpoint ${endpoint}
#
#	endpoint	- One endpoint of the cable
#
#	Determines the other endpoint the cable
#
#	Return:
#		zero		Success
#		non-zero	Failure
#
#####################################################
clsetup_get_other_endpoint()
{
	typeset endpoint=${1}
	typeset other_endpoint
	typeset cablelist
	typeset endpoint1
	typeset endpoint2
	integer found

	if [[ -z ${endpoint} ]]; then
		return 1
	fi

	# Determine the other cable end point
	cablelist="$(
		LC_ALL=C; export LC_ALL;
               	${CL_CMD_CLINTERCONNECT} show 2>/dev/null | nawk '/^ *Transport Cable:/ {print $3}'
		)"

	found=1
	for cable in ${cablelist}
	do
		endpoint1=$(IFS=,; set -- ${cable}; echo ${1})
		endpoint2=$(IFS=,; set -- ${cable}; echo ${2})
		if [[ "${endpoint1}" = "${endpoint}" ]]; then
			other_endpoint=${endpoint2}
			found=1
			break
		fi
		if [[ "${endpoint2}" = "${endpoint}" ]]; then
			other_endpoint=${endpoint1}
			found=1
			break
		fi
	done

	if [[ ${found} -ne 1 || -z ${other_endpoint} ]]; then
		# We should never be here
		printf "$(gettext 'Unable to determine the other endpoint.')\n\n\a"
		sc_prompt_pause
		return 1
	fi

	echo "${other_endpoint}"
	return 0
}

#####################################################
#
# clsetup_menu_transport_rmadapter() [nohelp]
#
#	nohelp		- if this flag is given, do not
#				clear screen, print title, print help,
#				or ask for confirmation to continue.
#
#	Remove a cluster transport adapter from the cluster.
#
#	Return:
#		zero		Completed
#		non-zero	Not completed
#
#####################################################
clsetup_menu_transport_rmadapter()
{
	typeset nohelp=${1}

	typeset sctxt_title="$(gettext '
		>>> Remove a Cluster Transport Adapter <<<
	')"
	typeset sctxt_p1="$(gettext '
		This option is used to remove an existing cluster transport
		adapter from the cluster configuration.  An adapter cannot
		be removed if it is still in use as an endpoint in a
		transport cable.
	')"
	typeset sctxt_p1_adapterdata="$(gettext '
		You will be asked to supply both the name of the adapter
		and the name of the node on which the adapter is found.
	')"

	integer found
	typeset nodelist
	typeset nodename
	typeset adapterlist
	typeset adaptername
	typeset adapter
	typeset answer
	typeset prompt
	typeset curr_driver
	typeset curr_inst
	typeset curr_physinst
	typeset curr_physadp

	#
	# Print title
	#
	if [[ -z "${nohelp}" ]]; then
		clear
		sc_print_title "${sctxt_title}"
		sc_print_para "${sctxt_p1}"

		# Okay to continue?
		clsetup_continue || return 1
	fi

	# Loop until done or Ctrl-D is typed
	while true
	do
		# Get nodelist
		if [[ ${CLSETUP_ISMEMBER} -eq 1 ]]; then

			# Get nodelist
			nodelist="$(clsetup_get_nodelist)"

			# Make sure we have a nodelist
			if [[ -z "${nodelist}" ]]; then
				printf "$(gettext 'Unable to establish the list of cluster nodes.')\n\n\a"
				sc_prompt_pause
				return 1
			fi
		fi

		# Print help
		sc_print_para "${sctxt_p1_adapterdata}"

		# Get the nodename
		answer=$(clsetup_prompt_nodename "${nodelist}" "$(gettext 'On which node is the adapter found?')") || return 1
		nodename=${answer}

		# Get the adaptername
		prompt="$(printf "$(gettext 'Name of the adapter to remove from \"%s\"?')" "${nodename}")"
		while true
		do
			# Prompt for adapter name
			answer=$(clsetup_prompt_transport_adapter "${prompt}") || continue 2

			# Get the list of configured adapters for this node
			adapterlist=
			if [[ ${CLSETUP_ISMEMBER} -eq 1 ]]; then
				adapterlist="$(clsetup_get_adapterlist ${nodename})"
			fi

			# See if our adapter is configured
			let found=0
			for adapter in ${adapterlist}
			do
				if [[ "${answer}" = "${adapter}" ]]; then
					let found=1
					break
				fi

				curr_driver=$(expr ${adapter} : '\([a-z0-9]*[a-z]\)')
				curr_inst=$(expr ${adapter} : '[a-z0-9]*[a-z]\([0-9]*\)')
				if [[ ${curr_inst} -ge ${SC_VLAN_MULTIPLIER} ]]; then
					curr_physinst=$(expr ${curr_inst} % ${SC_VLAN_MULTIPLIER} )
					curr_physadp=${curr_driver}${curr_physinst}
					if [[ "${answer}" = "${curr_physadp}" ]]; then
						let found=1
						break
					fi
				fi

			done

			#
			# If there is an adapterlist, but the adapter
			# is not found, print error.
			#
			if [[ -n "${adapterlist}" ]] && [[ ${found} -eq 0 ]]; then
				printf "$(gettext 'Unknown adapter.')\n\n\a"
				continue
			fi

			# Okay
			adaptername=${answer}
			break
		done

		#
		# Set up the command set
		#

		# Initialize
		set -A CLSETUP_DO_CMDSET

		# Add the command
		CLSETUP_DO_CMDSET[0]="${CL_CMD_CLINTERCONNECT} remove ${nodename}:${adaptername}"

		# Attempt to issue the command
		clsetup_do_cmdset || return 1

		# Done
		break
	done

	return 0
}

#####################################################
#
# clsetup_menu_transport_rmjunction() [nohelp]
#
#	nohelp		- if this flag is given, do not
#				clear screen, print title, print help,
#				or ask for confirmation to continue.
#
#	Remove a cluster transport junction from the cluster.
#
#	Return:
#		zero		Completed
#		non-zero	Not completed
#
#####################################################
clsetup_menu_transport_rmjunction()
{
	typeset nohelp=${1}

	typeset sctxt_title="$(gettext '
		>>> Remove a Cluster Transport Switch <<<
	')"
	typeset sctxt_p1="$(gettext '
		This option is used to remove an existing cluster transport
		switch from the cluster configuration.  A switch cannot
		be removed if any of the ports are still in use as a endpoints
		in any transport cables.
	')"

	integer found
	typeset junctionlist
	typeset junctionname
	typeset junction
	typeset answer

	#
	# Print title
	#
	if [[ -z "${nohelp}" ]]; then
		clear
		sc_print_title "${sctxt_title}"
		sc_print_para "${sctxt_p1}"

		# Okay to continue?
		clsetup_continue || return 1
	fi

	# Loop until done or Ctrl-D is typed
	while true
	do
		# Get the junctionname
		while true
		do
			# Prompt for junction name
			answer=$(clsetup_prompt_transport_junction "$(gettext 'Name of the switch to remove?')") || return 1

			# Get the list of configured junctions
			junctionlist=
			if [[ ${CLSETUP_ISMEMBER} -eq 1 ]]; then
				junctionlist="$(clsetup_get_junctionlist)"
			fi

			# See if our junction is configured
			let found=0
			for junction in ${junctionlist}
			do
				if [[ "${answer}" = "${junction}" ]]; then
					let found=1
					break
				fi
			done

			#
			# If there is a junctionlist, but the junction
			# is not found, print error.
			#
			if [[ -n "${junctionlist}" ]] && [[ ${found} -eq 0 ]]; then
				printf "$(gettext 'Unknown switch.')\n\n\a"
				continue
			fi

			# Okay
			junctionname=${answer}
			break
		done

		#
		# Set up the command set
		#

		# Initialize
		set -A CLSETUP_DO_CMDSET

		# Add the command
		CLSETUP_DO_CMDSET[0]="${CL_CMD_CLINTERCONNECT} remove ${junctionname}"

		# Attempt to issue the command
		clsetup_do_cmdset || return 1

		# Done
		break
	done

	return 0
}

#####################################################
#
# clsetup_menu_transport_cableon() [nohelp]
#
#	nohelp		- if this flag is given, do not
#				clear screen, print title, print help,
#				or ask for confirmation to continue.
#
#	Enable a cluster transport cable.
#
#	Return:
#		zero		Completed
#		non-zero	Not completed
#
#####################################################
clsetup_menu_transport_cableon()
{
	typeset nohelp=${1}

	typeset sctxt_title="$(gettext '
		>>> Enable a Cluster Transport Cable <<<
	')"
	typeset sctxt_p1="$(gettext '
		This option is used to enable an already existing
		cluster transport cable.
	')"

	typeset endpoint1
	typeset endpoint2

	#
	# Print title
	#
	if [[ -z "${nohelp}" ]]; then
		clear
		sc_print_title "${sctxt_title}"
		sc_print_para "${sctxt_p1}"

		# Okay to continue?
		clsetup_continue || return 1
	fi

	# Loop until done or Ctrl-D is typed
	while true
	do
		# Get the endpoint
		endpoint1=$(clsetup_prompt_transport_endpoint) || return 1
		endpoint2=$(clsetup_get_other_endpoint ${endpoint1}) || return 1


		#
		# Set up the command set
		#

		# Initialize
		set -A CLSETUP_DO_CMDSET

		# Add the command
		CLSETUP_DO_CMDSET[0]="${CL_CMD_CLINTERCONNECT} enable ${endpoint1},${endpoint2}"

		# Attempt to issue the command
		clsetup_do_cmdset || return 1

		# Done
		break
	done

	return 0
}

#####################################################
#
# clsetup_menu_transport_cableoff() [nohelp]
#
#	nohelp		- if this flag is given, do not
#				clear screen, print title, print help,
#				or ask for confirmation to continue.
#
#	Disable a cluster transport cable.
#
#	Return:
#		zero		Completed
#		non-zero	Not completed
#
#####################################################
clsetup_menu_transport_cableoff()
{
	typeset nohelp=${1}

	typeset sctxt_title="$(gettext '
		>>> Disable a Cluster Transport Cable <<<
	')"
	typeset sctxt_p1="$(gettext '
		This option is used to disable an existing
		cluster transport cable.
	')"
	typeset sctxt_p2="$(gettext '
		It is very important that you not disable or remove
		the last remaining healthy connection to an active cluster
		node.  Please check the status of other cables before
		you proceed.
	')"

	typeset endpoint1
	typeset endpoint2

	#
	# Print title
	#
	if [[ -z "${nohelp}" ]]; then
		clear
		sc_print_title "${sctxt_title}"
		sc_print_para "${sctxt_p1}"
		sc_print_para "${sctxt_p2}"

		# Okay to continue?
		clsetup_continue || return 1
	fi

	# Loop until done or Ctrl-D is typed
	while true
	do
		# Get the endpoints
		endpoint1=$(clsetup_prompt_transport_endpoint) || return 1
		endpoint2=$(clsetup_get_other_endpoint ${endpoint1}) || return 1

		#
		# Set up the command set
		#

		# Initialize
		set -A CLSETUP_DO_CMDSET

		# Add the command
		CLSETUP_DO_CMDSET[0]="${CL_CMD_CLINTERCONNECT} disable ${endpoint1},${endpoint2}"

		# Attempt to issue the command
		clsetup_do_cmdset || return 1

		# Done
		break
	done

	return 0
}

#####################################################
#
# clsetup_help_transport_menu()
#
#	Print help information from the transport menu.
#	This is the same help information as is printed
#	from the MAIN help menu.  But, we ask for confirmation
#	before returning to the previous menu.
#
#	This function always returns zero.
#
#####################################################
clsetup_help_transport_menu()
{
	clear
	clsetup_help_main_transport
	sc_prompt_pause

	return 0
}

#####################################################
#
# clsetup_get_transport_menuoption()
#
#	Print the cluster transport menu, and return
#	the selected option.
#
#	This function always returns zero.
#
#####################################################
clsetup_get_transport_menuoption()
{
	typeset sctxt_title_1="$(gettext '*** Cluster Interconnect Menu ***')"
	typeset sctxt_title_2="$(gettext 'Please select from one of the following options:')"
	typeset sctxt_option_001="$(gettext 'Add a transport cable')"
	typeset sctxt_option_002="$(gettext 'Add a transport adapter to a node')"
	typeset sctxt_option_003="$(gettext 'Add a transport switch')"
	typeset sctxt_option_004="$(gettext 'Remove a transport cable')"
	typeset sctxt_option_005="$(gettext 'Remove a transport adapter from a node')"
	typeset sctxt_option_006="$(gettext 'Remove a transport switch')"
	typeset sctxt_option_007="$(gettext 'Enable a transport cable')"
	typeset sctxt_option_008="$(gettext 'Disable a transport cable')"
	typeset sctxt_option_help="$(gettext 'Help')"
	typeset sctxt_option_return="$(gettext 'Return to the Main Menu')"

	typeset option

	option=$(sc_get_menuoption \
		"T1+++${sctxt_title_1}" \
		"T2+++${sctxt_title_2}" \
		"S+0+1+${sctxt_option_001}" \
		"S+0+2+${sctxt_option_002}" \
		"S+0+3+${sctxt_option_003}" \
		"S+0+4+${sctxt_option_004}" \
		"S+0+5+${sctxt_option_005}" \
		"S+0+6+${sctxt_option_006}" \
		"S+0+7+${sctxt_option_007}" \
		"S+0+8+${sctxt_option_008}" \
		"R+++" \
		"S+0+?+${sctxt_option_help}" \
		"S+0+q+${sctxt_option_return}" \
	)

	echo "${option}"

	return 0
}

#####################################################
#
# clsetup_menu_transport()
#
#	Cluster transport menu
#
#	This function always returns zero.
#
#####################################################
clsetup_menu_transport()#have to check the entire transport changes shekar
{
	# Loop around the cluster transport menu
	while true
	do
		case $(clsetup_get_transport_menuoption) in
		'1')    clsetup_menu_transport_addcable ;;
		'2')    clsetup_menu_transport_addadapter ;;
		'3')    clsetup_menu_transport_addjunction ;;
		'4')    clsetup_menu_transport_rmcable ;;
		'5')    clsetup_menu_transport_rmadapter ;;
		'6')    clsetup_menu_transport_rmjunction ;;
		'7')    clsetup_menu_transport_cableon ;;
		'8')    clsetup_menu_transport_cableoff ;;
		'?')    clsetup_help_transport_menu ;;
		'q')    break ;;
                esac
	done

	return 0
}

#####################################################
#####################################################
##
## Private hostnames
##
#####################################################
#####################################################

#####################################################
#
# clsetup_menu_privatehosts_namechange() [nohelp]
#
#	nohelp		- if this flag is given, do not
#				clear screen, print title, print help,
#				or ask for confirmation to continue.
#
#	Change the private hostname for a given node in the cluster.
#
#	Return:
#		zero		Completed
#		non-zero	Not completed
#
#####################################################
clsetup_menu_privatehosts_namechange()
{
	typeset nohelp=${1}

	if [[ ${ZONES_OK} -eq 1 ]]; then
		typeset sctxt_title="$(gettext '
			>>> Change a Node Private Hostname <<<
		')"
	else
		typeset sctxt_title="$(gettext '
			>>> Change a Private Hostname <<<
		')"
	fi
	typeset sctxt_p1="$(gettext '
		Nodes may contact other nodes in the cluster
		over the cluster interconnect by accessing them using
		their private hostnames.   Default private hostnames
		are assigned during cluster installation.
	')"
	typeset sctxt_p2="$(gettext '
		Note that private hostnames should never be stored
		in the hosts(4) or other naming services databases.
		A special nsswitch facility (see nsswitch.conf(4))
		performs all hostname lookups for private hostnames.
	')"
	typeset sctxt_p3="$(gettext '
		This option is used to change the private hostname
		for a given cluster node.   Any application or service
		which might cache private hostnames should be stopped
		before this change is made, then re-started after the
		change.  Please refer to the Sun Cluster documentation
		for additional information regarding changes to private
		hostnames before attempting to make any changes.
	')"

	typeset nodelist
	typeset nodename
	typeset privatename
	typeset answer
	typeset prompt

	#
	# Print title
	#
	if [[ -z "${nohelp}" ]]; then
		clear
		sc_print_title "${sctxt_title}"
		sc_print_para "${sctxt_p1}"
		sc_print_para "${sctxt_p2}"
		sc_print_para "${sctxt_p3}"

		# Okay to continue?
		clsetup_continue || return 1
	fi

	# Loop until done or Ctrl-D is typed
	while true
	do
		# Get nodelist
		if [[ ${CLSETUP_ISMEMBER} -eq 1 ]]; then

			# Get nodelist
			nodelist="$(clsetup_get_nodelist)"

			# Make sure we have a nodelist
			if [[ -z "${nodelist}" ]]; then
				printf "$(gettext 'Unable to establish the list of cluster nodes.')\n\n\a"
				sc_prompt_pause
				return 1
			fi
		fi

		# Get the nodename
		answer=$(clsetup_prompt_nodename "${nodelist}" "$(gettext 'Which cluster node do you want to change?')") || return 1
		nodename=${answer}

		# Get the new private hostname
		prompt="$(printf "$(gettext 'What is the new private hostname for \"%s\"?')" "${nodename}")"
		answer=$(sc_prompt "${prompt}") || return 1
		privatename=${answer}

		#
		# Set up the command set
		#

		# Initialize
		set -A CLSETUP_DO_CMDSET

		# Add the command
		CLSETUP_DO_CMDSET[0]="${CL_CMD_CLNODE} set -p privatehostname=${privatename} ${nodename}"

		# Attempt to issue the command
		clsetup_do_cmdset || return 1

		# Done
		break
	done

	return 0
}

#####################################################
#
# clsetup_menu_zone_privatehosts_nameadd() [nohelp]
#
#	nohelp		- if this flag is given, do not
#				clear screen, print title, print help,
#				or ask for confirmation to continue.
#
#	Add the private hostname for a given zone in the cluster.
#
#	Return:
#		zero		Completed
#		non-zero	Not completed
#
#####################################################
clsetup_menu_zone_privatehosts_nameadd()
{
	typeset nohelp=${1}

	typeset sctxt_title="$(gettext '
		>>> Add a Zone Private Hostname <<<
	')"
	typeset sctxt_p1="$(gettext '
		Zones may contact other local zones in the cluster
		over the cluster interconnect by accessing them using
		their private hostnames.
	')"
	typeset sctxt_p2="$(gettext '
		This option is used to add the private hostname for
		a local zone on a given cluster node.
	')"
	typeset sctxt_p3="$(gettext '
		There is no default private hostname for zone.  You must
		specify the zonename in the form of nodename:zonename.
	')"

	typeset nodelist
	typeset zonelist
	typeset nodename
	typeset zonename
	typeset privatename
	typeset answer
	typeset prompt
	typeset default

	#
	# Print title
	#
	if [[ -z "${nohelp}" ]]; then
		clear
		sc_print_title "${sctxt_title}"
		sc_print_para "${sctxt_p1}"
		sc_print_para "${sctxt_p2}"
		sc_print_para "${sctxt_p3}"

		# Okay to continue?
		clsetup_continue || return 1
	fi

	# Loop until done or Ctrl-D is typed
	while true
	do
		# Get the zonelist(nodenames:zonenames)
		if [[ ${CLSETUP_ISMEMBER} -eq 1 ]]; then

			# Get zonelist
			zonelist="$(clsetup_get_zonelist)"

			# Make sure we have a zonelist
			if [[ -z "${zonelist}" ]]; then
				printf "$(gettext 'Unable to establish the list of cluster zones.')\n\n\a"
				sc_prompt_pause
				return 1
			fi
		fi

		# Get the zonename from user
		answer=$(clsetup_prompt_zonename "${zonelist}" "$(gettext 'Specify the zone for which private IP communications are to be enabled:')") || return 1
		zonename=${answer}

		# Get the zone private hostname from user
		prompt="$(printf "$(gettext 'Specify the private hostname for \"%s\":')" "${zonename}")"
		answer=$(sc_prompt "${prompt}") || return 1
		privatename=${answer}

		#
		# Set up the command set
		#

		# Initialize
		set -A CLSETUP_DO_CMDSET

		# Add the command
		CLSETUP_DO_CMDSET[0]="${CL_CMD_CLNODE} set -p zprivatehostname=${privatename} ${zonename}"

		# Attempt to issue the command
		clsetup_do_cmdset || return 1

		# Done
		break
	done

	return 0
}

#####################################################
#
# clsetup_menu_zone_privatehosts_namechange() [nohelp]
#
#	nohelp		- if this flag is given, do not
#				clear screen, print title, print help,
#				or ask for confirmation to continue.
#
#	Change the private hostname for a given node in the cluster.
#
#	Return:
#		zero		Completed
#		non-zero	Not completed
#
#####################################################
clsetup_menu_zone_privatehosts_namechange()
{
	typeset nohelp=${1}

	typeset sctxt_title="$(gettext '
		>>> Change a Zone Private Hostname <<<
	')"
	typeset sctxt_p1="$(gettext '
		Zones may contact other local zones in the cluster
		over the cluster interconnect by accessing them using
		their private hostnames.
	')"
	typeset sctxt_p2="$(gettext '
		This option is used to change the private hostname for
		a local zone on a given cluster node.  Any application
		or service which might cache private hostnames should be
		stopped before this change is made, then re-started after
		the change.  Please refer to the Sun Cluster documentation
		for additional information regarding changes to private
		hostnames of zones before attempting to make any changes.
	')"
	typeset sctxt_p3="$(gettext '
		There is no default private hostname for zone.  You must
		specify the zonename in the form of nodename:zonename.
	')"

	typeset nodelist
	typeset zonelist
	typeset nodename
	typeset zonename
	typeset privatename
	typeset answer
	typeset prompt

	#
	# Print title
	#
	if [[ -z "${nohelp}" ]]; then
		clear
		sc_print_title "${sctxt_title}"
		sc_print_para "${sctxt_p1}"
		sc_print_para "${sctxt_p2}"
		sc_print_para "${sctxt_p3}"

		# Okay to continue?
		clsetup_continue || return 1
	fi

	# Loop until done or Ctrl-D is typed
	while true
	do
		# Get the zonelist(nodenames:zonenames)
		if [[ ${CLSETUP_ISMEMBER} -eq 1 ]]; then

			# Get zonelist
			zonelist="$(clsetup_get_zonelist)"

			# Make sure we have a zonelist
			if [[ -z "${zonelist}" ]]; then
				printf "$(gettext 'Unable to establish the list of cluster zones.')\n\n\a"
				sc_prompt_pause
				return 1
			fi
		fi

		# Get the zonename from user
		answer=$(clsetup_prompt_zonename "${zonelist}" "$(gettext 'Specify the zone whose private hostname is to be changed:')") || return 1
		zonename=${answer}

		# Get the new zone private hostname from user
		prompt="$(printf "$(gettext 'Specify the new private hostname for \"%s\":')" "${zonename}")"
		answer=$(sc_prompt "${prompt}") || return 1
		privatename=${answer}

		#
		# Set up the command set
		#

		# Initialize
		set -A CLSETUP_DO_CMDSET

		# Add the command
		CLSETUP_DO_CMDSET[0]="${CL_CMD_CLNODE} set -p zprivatehostname=${privatename} ${zonename}"

		# Attempt to issue the command
		clsetup_do_cmdset || return 1

		# Done
		break
	done

	return 0
}

#####################################################
#
# clsetup_menu_zone_privatehosts_nameremove() [nohelp]
#
#	nohelp		- if this flag is given, do not
#				clear screen, print title, print help,
#				or ask for confirmation to continue.
#
#	Remove the private hostname for a given zone in the cluster.
#
#	Return:
#		zero		Completed
#		non-zero	Not completed
#
#####################################################
clsetup_menu_zone_privatehosts_nameremove()
{
	typeset nohelp=${1}

	typeset sctxt_title="$(gettext '
		>>> Remove a Zone Private Hostname <<<
	')"
	typeset sctxt_p1="$(gettext '
		Zones may contact other local zones in the cluster
		over the cluster interconnect by accessing them using
		their private hostnames.
	')"
	typeset sctxt_p2="$(gettext '
		This option is used to remove the private hostname of
		a cluster zone.  Any application or service which might
		cache private hostnames should be stopped before this
		change is made, then re-started after the change.  Please
		refer to the Sun Cluster documentation for additional
		information regarding changes to private hostnames of
		zones before attempting to make any changes.
	')"

	typeset privatename
	typeset answer
	typeset prompt

	#
	# Print title
	#
	if [[ -z "${nohelp}" ]]; then
		clear
		sc_print_title "${sctxt_title}"
		sc_print_para "${sctxt_p1}"
		sc_print_para "${sctxt_p2}"

		# Okay to continue?
		clsetup_continue || return 1
	fi

	# Loop until done or Ctrl-D is typed
	while true
	do
		# Get the zonelist(nodenames:zonenames)
		if [[ ${CLSETUP_ISMEMBER} -eq 1 ]]; then

			# Get zonelist
			zonelist="$(clsetup_get_zonelist)"

			# Make sure we have a zonelist
			if [[ -z "${zonelist}" ]]; then
				printf "$(gettext 'Unable to establish the list of cluster zones.')\n\n\a"
				sc_prompt_pause
				return 1
			fi
		fi

		# Get the zonename from user
		answer=$(clsetup_prompt_zonename "${zonelist}" "$(gettext 'Specify the zone for which private IP communications are to be disabled:')") || return 1
		zonename=${answer}

		#
		# Set up the command set
		#

		# Initialize
		set -A CLSETUP_DO_CMDSET

		# Add the command
		CLSETUP_DO_CMDSET[0]="${CL_CMD_CLNODE} set -p zprivatehostname= ${zonename}"

		# Attempt to issue the command
		clsetup_do_cmdset || return 1

		# Done
		break
	done

	return 0
}

#####################################################
#
# clsetup_help_privatehosts_menu()
#
#	Print help information from the private hostnames menu.
#	This is the same help information as is printed
#	from the MAIN help menu.  But, we ask for confirmation
#	before returning to the previous menu.
#
#	This function always returns zero.
#
#####################################################
clsetup_help_privatehosts_menu()
{
	clear
	clsetup_help_main_privatehosts
	sc_prompt_pause

	return 0
}

#####################################################
#
# clsetup_get_privatehosts_menuoption()
#
#	Print the private hostnames menu, and return the selected option.
#
#	This function always returns zero.
#
#####################################################
clsetup_get_privatehosts_menuoption()
{
	typeset sctxt_title_1="$(gettext '*** Private Hostnames Menu ***')"
	typeset sctxt_title_2="$(gettext 'Please select from one of the following options:')"
	if [[ ${ZONES_OK} -eq 1 ]]; then
	    typeset sctxt_option_001="$(gettext 'Change a node private hostname')"
	    typeset sctxt_option_002="$(gettext 'Add a zone private hostname')"
	    typeset sctxt_option_003="$(gettext 'Remove a zone private hostname')"
	    typeset sctxt_option_004="$(gettext 'Change a zone private hostname')"
	else
	    typeset sctxt_option_001="$(gettext 'Change a private hostname')"
	fi
	typeset sctxt_option_help="$(gettext 'Help')"
	typeset sctxt_option_return="$(gettext 'Return to the Main Menu')"

	typeset option

	if [[ ${ZONES_OK} -eq 1 ]]; then
	    option=$(sc_get_menuoption \
		"T1+++${sctxt_title_1}" \
		"T2+++${sctxt_title_2}" \
		"S+0+1+${sctxt_option_001}" \
		"S+0+2+${sctxt_option_002}" \
		"S+0+3+${sctxt_option_003}" \
		"S+0+4+${sctxt_option_004}" \
		"R+++" \
		"S+0+?+${sctxt_option_help}" \
		"S+0+q+${sctxt_option_return}" \
	    )
	else
	    option=$(sc_get_menuoption \
		"T1+++${sctxt_title_1}" \
		"T2+++${sctxt_title_2}" \
		"S+0+1+${sctxt_option_001}" \
		"R+++" \
		"S+0+?+${sctxt_option_help}" \
		"S+0+q+${sctxt_option_return}" \
	    )
	fi

	echo "${option}"

	return 0
}

#####################################################
#
# clsetup_menu_privatehosts()
#
#	Private hostnames menu
#
#	This function always returns zero.
#
#####################################################
clsetup_menu_privatehosts()
{
	# Loop around the private hostnames menu
	while true
	do
	if [[ ${ZONES_OK} -eq 1 ]]; then
		case $(clsetup_get_privatehosts_menuoption) in
		'1')    clsetup_menu_privatehosts_namechange ;;
		'2')    clsetup_menu_zone_privatehosts_nameadd ;;
		'3')    clsetup_menu_zone_privatehosts_nameremove ;;
		'4')    clsetup_menu_zone_privatehosts_namechange ;;
		'?')    clsetup_help_privatehosts_menu ;;
		'q')    break ;;
                esac
	else
		case $(clsetup_get_privatehosts_menuoption) in
		'1')    clsetup_menu_privatehosts_namechange ;;
		'?')    clsetup_help_privatehosts_menu ;;
		'q')    break ;;
		esac
	fi
	done

	return 0
}

#####################################################
#####################################################
##
## Device Groups
##
#####################################################
#####################################################

#####################################################
#
# clsetup_menu_devicegroups_addvxvm() [nohelp]
#
#	nohelp		- if this flag is given, do not
#			clear screen, print title, print help,
#			or ask for confirmation to continue.
#
#	Register a VxVM disk group as a cluster device group.
#
#	Return:
#		zero		Completed
#		non-zero	Not completed
#
#####################################################
clsetup_menu_devicegroups_addvxvm()
{
	typeset nohelp=${1}

	typeset sctxt_title="$(gettext '
		>>> Register a VxVM Disk Group as a Device Group <<<
	')"
	typeset sctxt_p1="$(gettext '
		VERITAS Volume Manager disk groups are always managed by
		the cluster as cluster device groups.  This option is
		used to register a VxVM disk group with the
		cluster as a cluster device group. Make sure that the 
		disk group is imported on atleast one of the cluster 
		nodes before registering it.
	')"
	typeset sctxt_p1_preferenced="$(gettext '
		Primary ownership of a device group is determined by
		either specifying or not specifying a preferred
		ordering of the nodes that can own the device group. If
		an order is specified, this will be the order in which
		nodes will attempt to establish ownership. If an order
		is not specified, the first node that attempts to
		access a disk in the device group becomes the owner.
	')"
	typeset sctxt_p1_list1="$(gettext '
		Please list the nodes which are directly attached to
		all of the disks in this disk group and are, therefore,
		eligible to take primary ownership of the group.
	')"
	typeset sctxt_p1_list2="$(gettext '
		Please list the nodes which are directly attached to
		all of the disks in this disk group and are, therefore,
		eligible to take primary ownership of the group.  The
		order in which you list these names is important,
		as it also specifies the order in which nodes will
		attempt to establish ownership of the group.
	')"
	typeset sctxt_p1_list3="$(gettext '
		Please list the order in which nodes should attempt
		to establish ownership of the group.
	')"
	typeset sctxt_p2_list1="$(gettext '
		List one node name per line.   At least two node nodes
		must be given.  When finished, type Control-D:
	')"
	typeset sctxt_p2_list2="${sctxt_p2_list1}"
	typeset sctxt_p2_list3="$(gettext '
		List one node name per line:
	')"
	typeset sctxt_node_prompt1="$(gettext 'Node name:')"
	typeset sctxt_node_prompt2="$(gettext 'Node name (Ctrl-D to finish):')"
	typeset sctxt_p1_numsecondaries="$(gettext '
		The desired number of secondaries value can be set to any
		integer between 1 and the number of non-primary provider nodes
		in the device group. The cluster framework will use this number
		as a guide while configuring the actual secondaries for the
		device group. The default number of secondaries for a device
		group is 1. A smaller number of secondaries will typically
		result in less performance penalty, but it can reduce
		availability.
	')"

	typeset dgname
	typeset dgtype
	typeset dg_nodelist
	typeset preferenced
	typeset failback

	integer nodecount
	integer dg_nodecount
	integer getlist
	integer count
	integer found
	typeset nodelist
	typeset node
	typeset list
	typeset foo
	typeset answer
	typeset answers

	#
	# Print title
	#
	if [[ -z "${nohelp}" ]]; then
		clear
		sc_print_title "${sctxt_title}"
		sc_print_para "${sctxt_p1}"

		# Okay to continue?
		clsetup_continue || return 1
	fi

	# Loop until done or Ctrl-D is typed
	while true
	do
		# Initialize
		let nodecount=0
		nodelist=
		dg_nodelist=
		preferenced=
		failback=

		# Get nodelist and nodecount
		if [[ ${CLSETUP_ISMEMBER} -eq 1 ]]; then

			# Get nodelist
			nodelist="$(clsetup_get_nodelist)"

			# Make sure we have a nodelist
			if [[ -z "${nodelist}" ]]; then
				printf "$(gettext 'Unable to establish the list of cluster nodes.')\n\n\a"
				sc_prompt_pause
				return 1
			fi

			# Set node count
			let nodecount=$(set -- ${nodelist}; echo $#)
		else
			# We must be running in DEBUG mode
			answer=$(sc_prompt_yesno "$(gettext 'DEBUG:  Is this a two-node cluster?')" "${YES}") || return 1
			if [[ "${answer}" = "yes" ]]; then

				# Set nodecount
				let nodecount=2

				#
				# Since this is just DEBUG mode, we do
				# not error-check node name responses.
				#
				answer=$(sc_prompt "$(gettext 'DEBUG:  What is the name of the first node?')" "node1") || return 1
				nodelist="${answer}"

				answer=$(sc_prompt "$(gettext 'DEBUG:  What is the name of the second node?')" "node2") || return 1
				nodelist="${nodelist} ${answer}"
			else
				# If not two, then get the node count
				while true
				do
					answer=$(sc_prompt "$(gettext 'DEBUG:  How  many nodes are in the cluster?')") || return 1

					# In range?
					if [[ $(expr ${answer} : '[0-9]*') -ne $(expr ${answer} : '.*') ]] ||
					    [[ ${answer} -lt 2 ]] ||
					    [[ ${answer} -gt ${SC_MAXNODEID} ]]; then
						printf "\a"
					else
						break
					fi
				done

				# Set nodecount
				let nodecount=${answer}

				# Set nodelist (node1, node2, node3, ...)
				count=0
				while [[ ${count} -lt ${nodecount} ]]
				do
					((count += 1))
					nodelist="${nodelist} node${count}"
				done
			fi
		fi

		# Get the name of the device group
		answer=$(sc_prompt "$(gettext 'Name of the VxVM disk group you want to register?')") || return 1
		dgname=${answer}

		# Make sure it is not already registered
		if [[ ${CLSETUP_ISMEMBER} -eq 1 ]]; then
			dgtype=$(clsetup_get_devicegroup_type ${dgname})
			if [[ -n "${dgtype}" ]]; then
				printf "$(gettext '\"%s\" is already registered as a device group')\n\n\a" "${dgname}"
				continue
			fi
		fi

		# Preferenced?
		sc_print_para "${sctxt_p1_preferenced}"
		answer=$(sc_prompt_yesno "$(gettext 'Do you want to configure a preferred ordering?')" "${YES}") || return 1
		if [[ "${answer}" = "yes" ]]; then
			preferenced="true"
		else
			preferenced="false"
		fi

		# Two node cluster
		if [[ ${nodecount} -eq 2 ]]; then
			answer=$(sc_prompt_yesno "$(gettext 'Are both nodes attached to all disks in this group?')" "${YES}") || return 1

			# They MUST be!
			if [[ "${answer}" != "yes" ]]; then
				printf "$(gettext 'In a two-node cluster, all disks in the group must be dual-ported.')\n\n\a"
				continue
			fi

			# If preferenced is true, find out who is first
			if [[ "${preferenced}" = "true" ]]; then
				answer=$(clsetup_prompt_nodename "${nodelist}" "$(gettext 'Which node is the preferred primary for this device group?')") || return 1
				set -A list ${nodelist}
				if [[ "${answer}" = "${list[0]}" ]]; then
					dg_nodelist="${list[0]} ${list[1]}"
				elif [[ "${answer}" = "${list[1]}" ]]; then
					dg_nodelist="${list[1]} ${list[0]}"
				else
					printf "$(gettext 'Internal error.')\n\n\a"
					sc_prompt_pause
					return 1
				fi
			fi

		# More than two nodes
		else
			answer=$(sc_prompt_yesno "$(gettext 'Are all nodes attached to all disks in this group?')" "${YES}") || return 1

			# Clear flag to get node list
			let getlist=0

			# Get list of disks, no preferenced order
			if [[ "${answer}" = "no" ]] &&
			    [[ "${preferenced}" = "false" ]]; then
				let getlist=1

			# Get list of disks, preferenced ordering
			elif [[ "${answer}" = "no" ]] &&
			    [[ "${preferenced}" = "true" ]]; then
				let getlist=2

			# Get the preference list
			elif [[ "${answer}" = "yes" ]] &&
			    [[ "${preferenced}" = "true" ]]; then
				let getlist=3
			fi

			# Get the nodelist
			while [[ ${getlist} -gt 0 ]]
			do
				# Initialize
				answers=

				# Prompt based on type
				case ${getlist} in
				1)	# list of disks, no order
					sc_print_para "${sctxt_p1_list1}"
					sc_print_para "${sctxt_p2_list1}"
					;;

				2)	# list of disks, ordered
					sc_print_para "${sctxt_p1_list2}"
					sc_print_para "${sctxt_p2_list2}"
					;;

				3)	# Preference order only
					sc_print_para "${sctxt_p1_list3}"
					sc_print_para "${sctxt_p2_list3}"
					;;
				esac

				# Get the dg nodelist
				let count=0
				while [[ ${count} -lt ${nodecount} ]]
				do
					#
					# Cannot use Ctrl-D if we have not yet
					# typed in at least two node names or
					# if we are just typing preference list.
					#
					if [[ ${count} -lt 2 ]] ||
					    [[ ${getlist} -eq 3 ]]; then
						answer=$(sc_prompt "${sctxt_node_prompt1}" "" "nonl") || continue
					else
						answer=$(sc_prompt "${sctxt_node_prompt2}" "" "nonl") || break
					fi

					# must be in our nodelist
					let found=0
					for node in ${nodelist}
					do
						if [[ "${answer}" = "${node}" ]]; then
							let found=1
							break
						fi
					done

					# Make sure we found it
					if [[ -n "${nodelist}" ]] &&
					    [[ ${found} -eq 0 ]]; then
						echo
						printf "$(gettext 'Unknown node name.')\n\n\a"
						continue
					fi

					# Make sure it is not a duplicate
					for node in ${answers}
					do
						if [[ "${answer}" = "${node}" ]]; then
							echo
							printf "$(gettext '\"%s\" already entered.')\n\n\a" "${answer}"
							continue 2
						fi
					done

					# Okay, add it to list of answers
					answers="${answers} ${answer}"

					# Next
					((count += 1))
				done

				# Verify that the list is correct
				echo
				case ${getlist} in
				1)	sc_print_para "$(gettext 'This is the list of nodes:')" ;;
				2|3)
					sc_print_para "$(gettext 'This is the ordered list of nodes:')" ;;
				esac
				for node in ${answers}
				do
					printf "\t${node}\n"
				done
				echo
				answer=$(sc_prompt_yesno "$(gettext 'Is it correct?')" "${YES}") || return 1
				if [[ "${answer}" != "yes" ]]; then
					continue
				fi

				# Set dg_nodelist
				dg_nodelist="${answers}"

				# Done
				break
			done
		fi

		# If the dg_nodelist is not set, set to the default
		if [[ -z "${dg_nodelist}" ]]; then
			dg_nodelist="${nodelist}"
		fi

		# Reformat the dg_nodelist to give to cldg
		foo="${dg_nodelist}"
		dg_nodelist=
		dg_nodecount=0
		for node in ${foo}
		do
			if [[ -n "${dg_nodelist}" ]]; then
				dg_nodelist="${dg_nodelist},"
			fi
			dg_nodelist="${dg_nodelist}${node}"
			(( dg_nodecount += 1 ))
		done

		# Failback
		failback=
		if [[ "${preferenced}" = "true" ]]; then
			answer=$(sc_prompt_yesno "$(gettext 'Enable \"failback\" for this disk device group?')" "${NO}") || return 1
			if [[ "${answer}" = "yes" ]]; then
				failback="enabled"
			fi
		fi

		# numsecondaries
		numsecondaries=${SC_DEFAULT_NUMSECONDARIES}
		if [[ ${dg_nodecount} -gt 2 ]]; then
			while true
			do
				sc_print_para "${sctxt_p1_numsecondaries}"
				answer=$(sc_prompt "$(gettext 'What is the desired number of secondaries ?')" "${numsecondaries}") || return 1

				if [[ $(expr "${answer}" : '[0-9][0-9]*') -ne ${#answer} ]]; then
					echo
					printf "$(gettext 'The desired number of secondaries must be a valid integer.')\n"
					continue
				fi

				if [[ ${answer} -lt 1 ]] ||
			   	   [[ ${answer} -ge ${dg_nodecount} ]]; then
					echo
					printf "$(gettext 'Input must be between 1 and the number of non-primary nodes in the device group.')\n\n\a"
					continue
				else
					numsecondaries=${answer}
					break
				fi
			done
		fi

		#
		# Set up the command set
		#

		# Initialize
		set -A CLSETUP_DO_CMDSET

		# Add the command
		CLSETUP_DO_CMDSET[0]="${CL_CMD_CLDEVICEGROUP} create -t ${CLSETUP_DG_VXVM_SET}"

		# Nodelist?	(the default is all nodes)
		if [[ -n "${dg_nodelist}" ]]; then
			CLSETUP_DO_CMDSET[0]="${CLSETUP_DO_CMDSET[0]} -n ${dg_nodelist}"
		fi

		# Preferenced?	("false" is the default)
		if [[ "${preferenced}" = "true" ]]; then
			CLSETUP_DO_CMDSET[0]="${CLSETUP_DO_CMDSET[0]} -p preferenced=true"
		fi

		# Failback?	("false" is the default)
		if [[ "${failback}" = "enabled" ]]; then
			CLSETUP_DO_CMDSET[0]="${CLSETUP_DO_CMDSET[0]} -p failback=true"
		fi

		# numsecondaries?
		if [[ ${numsecondaries} -ne ${SC_DEFAULT_NUMSECONDARIES} ]]; then
			CLSETUP_DO_CMDSET[0]="${CLSETUP_DO_CMDSET[0]} -p numsecondaries=${numsecondaries}"
		fi

                CLSETUP_DO_CMDSET[0]="${CLSETUP_DO_CMDSET[0]} ${dgname}"

		# Attempt to issue the command
		clsetup_do_cmdset || return 1

		# Done
		break
	done

	return 0
}

#####################################################
#
# clsetup_menu_devicegroups_syncvxvm() [nohelp]
#
#	nohelp		- if this flag is given, do not
#				clear screen, print title, print help,
#				or ask for confirmation to continue.
#
#	Synchronize volume and replication information for a
#	VxVM device group.
#
#	Return:
#		zero		Completed
#		non-zero	Not completed
#
#####################################################
clsetup_menu_devicegroups_syncvxvm()
{
	typeset nohelp=${1}

	typeset sctxt_title="$(gettext '
		>>> Synchronize Volume Information for a VxVM Device Group <<<
	')"
	typeset sctxt_p1="$(gettext '
		VERITAS Volume Manager disk groups are always managed
		by the cluster as cluster device groups.  This option
		is used to synchronize volume information for a VxVM
		device group between the VxVM software and the
		clustering software.  It should be selected anytime a
		volume is either added to or removed from a VxVM disk
		group.  Otherwise, the cluster will be unaware of the
		changes. This option is also useful when you want to
		synchronize the replication information of a VxVM
		device group between the replication infrastructure(Truecopy)
		and the clustering software.
	')"
	typeset dgname
	typeset dgtype

	typeset answer

	#
	# Print title
	#
	if [[ -z "${nohelp}" ]]; then
		clear
		sc_print_title "${sctxt_title}"
		sc_print_para "${sctxt_p1}"

		# Okay to continue?
		clsetup_continue || return 1
	fi

	# Loop until done or Ctrl-D is typed
	while true
	do

		# Get the name of the device group
		answer=$(sc_prompt "$(gettext 'Name of the VxVM device group you want to synchronize?')") || return 1
		dgname=${answer}

		# Make sure it is a VxVM device group
		if [[ ${CLSETUP_ISMEMBER} -eq 1 ]]; then
			dgtype=$(clsetup_get_devicegroup_type ${dgname})
			if [[ -z "${dgtype}" ]] ||
			    [[ "${dgtype}" != "${CLSETUP_DG_VXVM_LOOKUP}" ]]; then
				printf "$(gettext '\"%s\" is not a %s device group.')\n\n\a" "${dgname}" "${CLSETUP_DG_VXVM_LOOKUP}"
				continue
			fi
		fi

		#
		# Set up the command set
		#

		# Initialize
		set -A CLSETUP_DO_CMDSET

		# Add the command
		CLSETUP_DO_CMDSET[0]="${CL_CMD_CLDEVICEGROUP} sync ${dgname}"

		# Attempt to issue the command
		clsetup_do_cmdset || return 1

		# Done
		break
	done

	return 0
}

#####################################################
#
# clsetup_menu_devicegroups_rmvxvm() [nohelp]
#
#	nohelp		- if this flag is given, do not
#				clear screen, print title, print help,
#				or ask for confirmation to continue.
#
#	Unregister a VxVM disk group as a cluster device group.
#
#	Return:
#		zero		Completed
#		non-zero	Not completed
#
#####################################################
clsetup_menu_devicegroups_rmvxvm()
{
	typeset nohelp=${1}

	typeset sctxt_title="$(gettext '
		>>> Unregister a VxVM Disk Group as a Device Group <<<
	')"
	typeset sctxt_p1="$(gettext '
		VERITAS Volume Manager disk groups are always managed
		by the cluster as cluster device groups.  This option is used
		to unregister a VxVM cluster device group.
	')"
	typeset sctxt_p2="$(gettext '
		Before you attempt to unregister this group, please be sure
		that group is offline and there are no mounts or other active 
		references to any of the devices in the group.
	')"

	typeset dgname
	typeset dgtype
	typeset answer

	#
	# Print title
	#
	if [[ -z "${nohelp}" ]]; then
		clear
		sc_print_title "${sctxt_title}"
		sc_print_para "${sctxt_p1}"
		sc_print_para "${sctxt_p2}"

		# Okay to continue?
		clsetup_continue || return 1
	fi

	# Loop until done or Ctrl-D is typed
	while true
	do

		# Get the name of the device group
		answer=$(sc_prompt "$(gettext 'Name of the VxVM device group you want to unregister?')") || return 1
		dgname=${answer}

		# Make sure it is a VxVM device group
		if [[ ${CLSETUP_ISMEMBER} -eq 1 ]]; then
			dgtype=$(clsetup_get_devicegroup_type ${dgname})
			if [[ -z "${dgtype}" ]] ||
			    [[ "${dgtype}" != "${CLSETUP_DG_VXVM_LOOKUP}" ]]; then
				printf "$(gettext '\"%s\" is not a %s device group.')\n\n\a" "${dgname}" "${CLSETUP_DG_VXVM_LOOKUP}"
				continue
			fi
		fi

		#
		# Set up the command set
		#

		# Initialize
		set -A CLSETUP_DO_CMDSET

		state="$(${CL_CMD_CLDEVICEGROUP} status ${dgname} | grep -w "${dgname}" | awk '{print $4}' | grep [Online,Offline,Degraded])"
		if [[ ${state} == "Online" ]]; then
			answer=$(sc_prompt_yesno "$(gettext 'The devicegroup is online. Do you still want to remove ?')")
			
			if [[ "${answer}" == "yes" ]]; then
				# Add the commands
				CLSETUP_DO_CMDSET[0]="${CL_CMD_CLDEVICEGROUP} offline ${dgname}"
				CLSETUP_DO_CMDSET[1]="${CL_CMD_CLDEVICEGROUP} delete ${dgname}"
			fi
		else
			# Add the command
			CLSETUP_DO_CMDSET[0]="${CL_CMD_CLDEVICEGROUP} delete ${dgname}"
		fi
		
		# Attempt to issue the command
		clsetup_do_cmdset || return 1

		# Done
		break
	done

	return 0
}

#####################################################
#
# clsetup_menu_devicegroups_addnodevxvm() [nohelp]
#
#	nohelp		- if this flag is given, do not
#				clear screen, print title, print help,
#				or ask for confirmation to continue.
#
#	Add a node to a VxVM device group.
#
#	Return:
#		zero		Completed
#		non-zero	Not completed
#
#####################################################
clsetup_menu_devicegroups_addnodevxvm()
{
	typeset nohelp=${1}

	typeset sctxt_title="$(gettext '
		>>> Add a Node to a VxVM Device Group <<<
	')"
	typeset sctxt_p1="$(gettext '
		VERITAS Volume Manager disk groups are always managed by
		the cluster as cluster device groups.  This option is
		used to add a node to an already existing VxVM cluster
		device group.
	')"

	typeset nodelist
	typeset nodename
	typeset node
	typeset list
	typeset dgname
	typeset dgtype
	typeset answer

	#
	# Print title
	#
	if [[ -z "${nohelp}" ]]; then
		clear
		sc_print_title "${sctxt_title}"
		sc_print_para "${sctxt_p1}"

		# Okay to continue?
		clsetup_continue || return 1
	fi

	# Loop until done or Ctrl-D is typed
	while true
	do
		#Initialize
		nodelist=

		# Get the name of the device group
		answer=$(sc_prompt "$(gettext 'Name of the VxVM device group to which you want to add a node?')") || return 1
		dgname=${answer}

		# Make sure it is a VxVM device group
		if [[ ${CLSETUP_ISMEMBER} -eq 1 ]]; then
			dgtype=$(clsetup_get_devicegroup_type ${dgname})
			if [[ -z "${dgtype}" ]] ||
			    [[ "${dgtype}" != "${CLSETUP_DG_VXVM_LOOKUP}" ]]; then
				printf "$(gettext '\"%s\" is not a %s device group.')\n\n\a" "${dgname}" "${CLSETUP_DG_VXVM_LOOKUP}"
				continue
			fi
		fi

		# Get the nodelist
		if [[ ${CLSETUP_ISMEMBER} -eq 1 ]]; then

			nodelist="$(clsetup_get_nodelist)"

			# Make sure we have a nodelist
			if [[ -z "${nodelist}" ]]; then
				printf "$(gettext 'Unable to establish the list of cluster nodes.')\n\n\a"
				sc_prompt_pause
				return 1
			fi
		fi

		# Get the nodename
		while true
		do
			answer=$(clsetup_prompt_nodename "${nodelist}" "$(gettext 'Name of the node to add to this group?')") || return 1

			# Make sure it is not already there
			if [[ ${CLSETUP_ISMEMBER} -eq 1 ]]; then
				list="$(clsetup_get_devicegroup_nodes ${dgname})"
				for node in ${list}
				do
					if [[ "${answer}" = "${node}" ]]; then
						printf "$(gettext '\"%s\" is already part of this group.')\n\n\a" "${answer}"
						continue 2
					fi
				done
			fi

			# Done
			break
		done

		# Okay
		nodename=${answer}

		#
		# Set up the command set
		#

		# Initialize
		set -A CLSETUP_DO_CMDSET

		# Add the command
		CLSETUP_DO_CMDSET[0]="${CL_CMD_CLDEVICEGROUP} add-node -n ${nodename} ${dgname}"

		# Attempt to issue the command
		clsetup_do_cmdset || return 1

		# Done
		break
	done

	return 0
}

#####################################################
#
# clsetup_menu_devicegroups_rmnodevxvm() [nohelp]
#
#	nohelp		- if this flag is given, do not
#				clear screen, print title, print help,
#				or ask for confirmation to continue.
#
#	Remove a node from a VxVM device group.
#
#	Return:
#		zero		Completed
#		non-zero	Not completed
#
#####################################################
clsetup_menu_devicegroups_rmnodevxvm()
{
	typeset nohelp=${1}

	typeset sctxt_title="$(gettext '
		>>> Remove a Node from a VxVM Device Group <<<
	')"
	typeset sctxt_p1="$(gettext '
		VERITAS Volume Manager disk groups are always managed by
		the cluster as cluster device groups.  This option is
		used to remove a node from an already existing
		VxVM cluster device group.
	')"

	integer found
	typeset nodelist
	typeset nodename
	typeset node
	typeset list
	typeset dgname
	typeset dgtype
	typeset answer

	#
	# Print title
	#
	if [[ -z "${nohelp}" ]]; then
		clear
		sc_print_title "${sctxt_title}"
		sc_print_para "${sctxt_p1}"

		# Okay to continue?
		clsetup_continue || return 1
	fi

	# Loop until done or Ctrl-D is typed
	while true
	do
		#Initialize
		nodelist=

		# Get the name of the device group
		answer=$(sc_prompt "$(gettext 'Name of the VxVM device group from which you want to remove a node?')") || return 1
		dgname=${answer}

		# Make sure it is a VxVM device group
		if [[ ${CLSETUP_ISMEMBER} -eq 1 ]]; then
			dgtype=$(clsetup_get_devicegroup_type ${dgname})
			if [[ -z "${dgtype}" ]] ||
			    [[ "${dgtype}" != "${CLSETUP_DG_VXVM_LOOKUP}" ]]; then
				printf "$(gettext '\"%s\" is not a %s device group.')\n\n\a" "${dgname}" "${CLSETUP_DG_VXVM_LOOKUP}"
				continue
			fi
		fi

		# Get the dg nodelist
		if [[ ${CLSETUP_ISMEMBER} -eq 1 ]]; then
			nodelist="$(clsetup_get_devicegroup_nodes ${dgname})"
		fi

		# Get the nodename
		while true
		do
			answer=$(sc_prompt "$(gettext 'Name of the node to remove from this group?')") || return 1

			# Make sure the node is in the list
			if [[ ${CLSETUP_ISMEMBER} -eq 1 ]]; then
				list="$(clsetup_get_devicegroup_nodes ${dgname})"
				let found=0
				for node in ${list}
				do
					if [[ "${answer}" = "${node}" ]]; then
						let found=1
						break
					fi
				done
				if [[ ${found} -ne 1 ]]; then
					printf "$(gettext '\"%s\" is not a part of this group.')\n\n\a" "${answer}"
					continue
				fi
			fi

			# Done
			break
		done

		# Okay
		nodename=${answer}

		#
		# Set up the command set
		#

		# Initialize
		set -A CLSETUP_DO_CMDSET

		# Add the command
		CLSETUP_DO_CMDSET[0]="${CL_CMD_CLDEVICEGROUP} remove-node -n ${nodename} ${dgname}"

		# Attempt to issue the command
		clsetup_do_cmdset || return 1

		# Done
		break
	done

	return 0
}

#####################################################
#
# clsetup_get_changeprops_menuoption()
#
#	Print the device group change properties menu, and return the
#	selected option.
#
#	This function always returns zero.
#
#####################################################
clsetup_get_changeprops_menuoption()
{
	typeset sctxt_title_1="$(gettext '*** Device Group Change properties Menu ***')"
	typeset sctxt_title_2="$(gettext 'Please select from one of the following options:')"
	typeset sctxt_option_001="$(gettext 'Change the preferenced and/or failback properties')"
	typeset sctxt_option_002="$(gettext 'Change the numsecondaries property')"
	typeset sctxt_option_help="$(gettext 'Help')"
	typeset sctxt_option_return="$(gettext 'Return to the Device Groups Menu')"

	typeset option

	option=$(sc_get_menuoption \
		"T1+++${sctxt_title_1}" \
		"T2+++${sctxt_title_2}" \
		"S+0+1+${sctxt_option_001}" \
		"S+0+2+${sctxt_option_002}" \
		"R+++" \
		"S+0+?+${sctxt_option_help}" \
		"S+0+q+${sctxt_option_return}" \
	)

	echo "${option}"

	return 0
}

#####################################################
#
# clsetup_menu_devicegroups_changeprops() [nohelp]
#
#	Device groups change properties menu.
#
#	Return:
#		Always zero
#
#####################################################
clsetup_menu_devicegroups_changeprops()
{
	# Loop around the devicegroup change properties menu
	while true
	do
		case $(clsetup_get_changeprops_menuoption) in
		'1')    clsetup_menu_changeprops_prio_failback ;;
		'2')	clsetup_menu_changeprops_numsecs ;;
		'?')    clsetup_help_devicegroups_menu ;;
		'q')    break ;;
                esac
	done

	return 0
}


#####################################################
#
# clsetup_menu_devicegroups_setlocalonly() [nohelp]
#
#	nohelp		- if this flag is given, do not
#			clear screen, print title, print help,
#			or ask for confirmation to continue.
#
#	Set a VxVM disk group as a local disk group.
#
#	Return:
#		zero		Completed
#		non-zero	Not completed
#
#####################################################
clsetup_menu_devicegroups_setlocalonly()
{
	typeset nohelp=${1}

	typeset sctxt_title="$(gettext '
		>>> Set a VxVM Disk Group as a local Disk Group <<<
	')"
	typeset sctxt_p1="$(gettext '
		A VERITAS Volume Manager disk group that you set as
		a local disk group can be used on the node where it
		is imported without any interaction with Sun Cluster
		software. This option is used to set a VxVM disk
		group as a local disk group.
	')"
	typeset sctxt_p1_list1="$(gettext '
		Specify a node that is directly attached to
		all of the disks in this disk group and which
		will be one of the nodes that can access the
		disk group.
	')"
	typeset sctxt_p2_list1="$(gettext '
		List exactly one node name.
	')"
	typeset sctxt_node_prompt1="$(gettext 'Node name:')"

	typeset dgname
	typeset dgtype
	typeset dg_nodelist

	integer nodecount
	integer dg_nodecount
	integer found
	typeset nodelist
	typeset node
	typeset foo
	typeset answer
	typeset answers

	#
	# Print title
	#
	if [[ -z "${nohelp}" ]]; then
		clear
		sc_print_title "${sctxt_title}"
		sc_print_para "${sctxt_p1}"

		# Okay to continue?
		clsetup_continue || return 1
	fi

	# Loop until done or Ctrl-D is typed
	while true
	do
		# Initialize
		let nodecount=0
		nodelist=
		dg_nodelist=

		# Get nodelist and nodecount
		if [[ ${CLSETUP_ISMEMBER} -eq 1 ]]; then

			# Get nodelist
			nodelist="$(clsetup_get_nodelist)"

			# Make sure we have a nodelist
			if [[ -z "${nodelist}" ]]; then
				printf "$(gettext 'Unable to establish the\
					list of cluster nodes.')\n\n\a"
				sc_prompt_pause
				return 1
			fi

			# Set node count
			let nodecount=$(set -- ${nodelist}; echo $#)
		else
			# We must be running in DEBUG mode
			answer=$(sc_prompt_yesno "$(gettext 'DEBUG:  Is this\
				a two-node cluster?')" "${YES}") || return 1
			if [[ "${answer}" = "yes" ]]; then

				# Set nodecount
				let nodecount=2

				#
				# Since this is just DEBUG mode, we do
				# not error-check node name responses.
				#
				answer=$(sc_prompt "$(gettext 'DEBUG:  What is\
					the name of the first node?')"\
					"node1") || return 1
				nodelist="${answer}"

				answer=$(sc_prompt "$(gettext 'DEBUG:  What is\
					the name of the second node?')"\
					"node2") || return 1
				nodelist="${nodelist} ${answer}"
			else
				# If not two, then get the node count
				while true
				do
					answer=$(sc_prompt "$(gettext 'DEBUG: \
						How  many nodes are in the\
						cluster?')") || return 1

					# In range?
					if [[ $(expr ${answer} : '[0-9]*') -ne\
						$(expr ${answer} : '.*') ]] ||
					    [[ ${answer} -lt 2 ]] ||
					    [[ ${answer} -gt ${SC_MAXNODEID}\
						]]; then
						printf "\a"
					else
						break
					fi
				done

				# Set nodecount
				let nodecount=${answer}

				# Set nodelist (node1, node2, node3, ...)
				count=0
				while [[ ${count} -lt ${nodecount} ]]
				do
					((count += 1))
					nodelist="${nodelist} node${count}"
				done
			fi
		fi

		# Get the name of the device group
		answer=$(sc_prompt "$(gettext 'Name of the VxVM disk group to \
			set as localonly?')") || return 1
		dgname=${answer}

		# Get the nodelist
		while true
		do
			sc_print_para "${sctxt_p1_list1}"
			sc_print_para "${sctxt_p2_list1}"

			# Get the dg nodelist
			answers=$(sc_prompt "${sctxt_node_prompt1}" "" "nonl") \
				|| return 1

			# must be in our nodelist
			let found=0
			for node in ${nodelist}
			do
				if [[ "${answers}" = "${node}" ]]; then
					let found=1
					break
				fi
			done

			# Make sure we found it
			if [[ -n "${nodelist}" ]] &&
			    [[ ${found} -eq 0 ]]; then
				echo
				printf "$(gettext 'Unknown node name.')\n\n\a"
				continue
			fi

			# Verify that the list is correct
			echo
			sc_print_para "$(gettext 'Node that you provided is:')"
			echo
			printf "\t${answers}\n"
			echo
			answer=$(sc_prompt_yesno "$(gettext 'Is it correct?')" \
				"${YES}") || return 1
			if [[ "${answer}" != "yes" ]]; then
				continue
			fi

			# Set dg_nodelist
			dg_nodelist="${answers}"

			# Done
			break
		done

		# Reformat the dg_nodelist to give to cldg
		foo="${dg_nodelist}"
		dg_nodelist=
		dg_nodecount=0
		for node in ${foo}
		do
			if [[ -n "${dg_nodelist}" ]]; then
				dg_nodelist="${dg_nodelist},"
			fi
			dg_nodelist="${dg_nodelist}${node}"
			(( dg_nodecount += 1 ))
		done

		#
		# Set up the command set
		#

		# Initialize
		set -A CLSETUP_DO_CMDSET

		# Add the command
		CLSETUP_DO_CMDSET[0]="${CL_CMD_CLDEVICEGROUP} create -t vxvm"

		# Add localonly property
		CLSETUP_DO_CMDSET[0]="${CLSETUP_DO_CMDSET[0]} -p localonly=true"

		# Nodelist
		CLSETUP_DO_CMDSET[0]="${CLSETUP_DO_CMDSET[0]} -n ${dg_nodelist}"

                CLSETUP_DO_CMDSET[0]="${CLSETUP_DO_CMDSET[0]} ${dgname}"

		# Attempt to issue the command
		clsetup_do_cmdset || return 1

		# Done
		break
	done

	return 0
}

#####################################################
#
# clsetup_menu_devicegroups_unsetlocalonly() [nohelp]
#
#	nohelp		- if this flag is given, do not
#			clear screen, print title, print help,
#			or ask for confirmation to continue.
#
#	Unset local flag on a VxVM disk group.
#
#	Return:
#		zero		Completed
#		non-zero	Not completed
#
#####################################################
clsetup_menu_devicegroups_unsetlocalonly()
{
	typeset nohelp=${1}

	typeset sctxt_title="$(gettext '
		>>> Reset a local disk group to a VxVM disk group <<<
	')"
	typeset sctxt_p1="$(gettext '
		This option is used to convert a local
		disk group to a VxVM disk group.
	')"

	typeset dgname

	#
	# Print title
	#
	if [[ -z "${nohelp}" ]]; then
		clear
		sc_print_title "${sctxt_title}"
		sc_print_para "${sctxt_p1}"

		# Okay to continue?
		clsetup_continue || return 1
	fi

	# Get the name of the device group
	dgname=$(sc_prompt "$(gettext 'Name of the VxVM disk group to set as \
		regular disk group?')") || return 1

	#
	# Set up the command set
	#

	# Initialize
	set -A CLSETUP_DO_CMDSET

	# Add the command
	CLSETUP_DO_CMDSET[0]="${CL_CMD_CLDEVICEGROUP} create -t vxvm"

	# Add localonly property
	CLSETUP_DO_CMDSET[0]="${CLSETUP_DO_CMDSET[0]} -p localonly=false"

	CLSETUP_DO_CMDSET[0]="${CLSETUP_DO_CMDSET[0]} ${dgname}"

	# Attempt to issue the command
	clsetup_do_cmdset || return 1

	return 0
}

####################################################
#
# clsetup_menu_changeprops_numsecs() [nohelp]
#
#	nohelp		- if this flag is given, do not
#				clear screen, print title, print help,
#				or ask for confirmation to continue.
#
#	Change the Desired number of secondaries property of a device group.
#
#	Return:
#		zero		Completed
#		non-zero	Not completed
#
#####################################################
clsetup_menu_changeprops_numsecs()
{
	typeset nohelp=${1}

	typeset sctxt_title="$(gettext '
		>>> Change numsecondaries property of a Device Group <<<
	')"
	typeset sctxt_p1="$(gettext '
		This option is used to change the \"numsecondaries\" property of a device group.
	')"
	typeset sctxt_p2="$(gettext '
		The desired number of secondaries value can be set to any
		integer between 1 and the number of non-primary provider nodes
		in the device group. The cluster framework will use this number
		as a guide while establishing the actual secondaries for the
		device group. The default number of secondaries for a device
		group is 1. A smaller number of secondaries will typically
		result in less performance penalty, but it can reduce
		availability.
	')"

	typeset dgname
	typeset answer

	#
	# Print title
	#
	if [[ -z "${nohelp}" ]]; then
		clear
		sc_print_title "${sctxt_title}"
		sc_print_para "${sctxt_p1}"
		sc_print_para "${sctxt_p2}"

		# Okay to continue?
		clsetup_continue || return 1
	fi

	# Loop until done or Ctrl-D is typed
	while true
	do
		# Get the name of the device group
		answer=$(sc_prompt "$(gettext 'Name of the device group you want to change?')") || return 1
		dgname=${answer}

		# Make sure it is a known device group
		if [[ ${CLSETUP_ISMEMBER} -eq 1 ]]; then
			dgtype=$(clsetup_get_devicegroup_type ${dgname})
			if [[ -z "${dgtype}" ]]; then
				printf "$(gettext '\"%s\" is not a known device group.')\n\n\a" "${dgname}"
				continue
			fi
		fi

		sc_print_para "$(printf "$(gettext 'If the \"numsecondaries\" property is reset, the system default value will always be used.')")"
		answer=$(sc_prompt_yesno "$(gettext 'Do you want to reset the \"numsecondaries\" property ?')") || return 1
		if [[ ${answer} = "yes" ]]; then
			#
			# Set up the command set
			#

			# Initialize
			set -A CLSETUP_DO_CMDSET

			# Add the command
			CLSETUP_DO_CMDSET[0]="${CL_CMD_CLDEVICEGROUP} set -p numsecondaries= ${dgname}"

			# Attempt to issue the command
			clsetup_do_cmdset || return 1

			# Done
			break
		fi

		numsecs=0
		while true
		do
			#Initialize
			numsecs=`${CL_CMD_CLDEVICEGROUP} show ${dgname} 2>/dev/null | grep "numsecondaries" | awk -F: '{print $2}'`

			# Change value
			answer=$(sc_prompt "$(gettext 'Please specify the desired number of secondaries.')" "current value:$numsecs") || return 1

			if [[ $(expr "${answer}" : '[0-9][0-9]*') -ne ${#answer} ]]; then
				printf "$(gettext 'The desired number of secondaries must be a valid positive integer.')\n"
				continue
			elif [[ ${answer} -eq 0 ]]; then
				printf "$(gettext 'The desired number of secondaries must be a valid positive integer.')\n"
				continue
			else
				numsecs=$answer
				break
			fi
		done

		#
		# Set up the command set
		#

		# Initialize
		set -A CLSETUP_DO_CMDSET

		# Add the command
		CLSETUP_DO_CMDSET[0]="${CL_CMD_CLDEVICEGROUP} set -p numsecondaries=${numsecs} ${dgname}"

		# Attempt to issue the command
		clsetup_do_cmdset || return 1

		# Done
		break

	done

	return 0
}

#####################################################
#
# clsetup_menu_changeprops_prio_failback() [nohelp]
#
#	nohelp		- if this flag is given, do not
#				clear screen, print title, print help,
#				or ask for confirmation to continue.
#
#	Change the preference and/or failback properties of a device group.
#
#	Return:
#		zero		Completed
#		non-zero	Not completed
#
#####################################################
clsetup_menu_changeprops_prio_failback()
{
	typeset nohelp=${1}

	typeset sctxt_title="$(gettext '
		>>> Change preferenced and/or failback properties of a Device Group <<<
	')"
	typeset sctxt_p1="$(gettext '
		This option is used to change the \"preferenced\" and/or
		\"failback\" properties of a device group.
	')"
	typeset sctxt_p2="$(gettext '
		The scheme for establishing the primary ownership of a
		device group is based on the setting of a special
		ownership preference attribute.  If the attribute is
		not set, the primary owner of an otherwise unowned
		device group is the first node which attempts to access
		a disk in that group. However, if this attribute is
		set, you must specify the preferred order in which
		nodes will attempt to establish ownership.
	')"
	typeset sctxt_p3="$(gettext '
		If you disable the \"preferenced\" attribute, then the
		\"failback\" attribute is also automatically disabled.
		However, if you attempt to enable or re-enable the
		\"preferenced\" attribute, you will have the choice
		of enabling or disabling the \"failback\" attribute.
	')"
	typeset sctxt_p4="$(gettext '
		Note that if the \"preferenced\" attribute is either
		enabled or re-enabled that you will be required to
		re-establish the order of nodes in the primary ownership
		preference list.
	')"
	sctxt_p1_list="$(gettext '
		Please list the order in which nodes should attempt
		to establish ownership of the group.
	')"
	sctxt_p2_list="$(gettext '
		List one node name per line:
	')"

	typeset dgname
	typeset dgtype
	typeset dg_nodelist
	typeset preferenced
	typeset failback

	integer found
	integer count
	integer listcount
	typeset nodename
	typeset node
	typeset list
	typeset foo
	typeset answer
	typeset answers

	#
	# Print title
	#
	if [[ -z "${nohelp}" ]]; then
		clear
		sc_print_title "${sctxt_title}"
		sc_print_para "${sctxt_p1}"
		sc_print_para "${sctxt_p2}"
		sc_print_para "${sctxt_p3}"
		sc_print_para "${sctxt_p4}"

		# Okay to continue?
		clsetup_continue || return 1
	fi

	# Loop until done or Ctrl-D is typed
	while true
	do
		#Initialize
		dg_nodelist=
		preferenced=
		failback=

		# Get the name of the device group
		answer=$(sc_prompt "$(gettext 'Name of the device group you want to change?')") || return 1
		dgname=${answer}

		# Make sure it is a known device group
		if [[ ${CLSETUP_ISMEMBER} -eq 1 ]]; then
			dgtype=$(clsetup_get_devicegroup_type ${dgname})
			if [[ -z "${dgtype}" ]]; then
				printf "$(gettext '\"%s\" is not a known device group.')\n\n\a" "${dgname}"
				continue
			fi
		fi

		# Disable "preferenced"?
		answer=$(sc_prompt_yesno "$(gettext 'Do you want to disable the \"preferenced\" attribute?')") || return 1
		if [[ "${answer}" = "yes" ]]; then
			preferenced="false"
			failback="false"
		fi

		# Disable "failback"?
		if [[ -z "${preferenced}" ]]; then
			answer=$(sc_prompt_yesno "$(gettext 'Do you just want to disable \"failback\"?')") || return 1
			if [[ "${answer}" = "yes" ]]; then
				failback="false"
			fi
		fi

		# Enable "preferenced"?
		if [[ -z "${preferenced}" ]] && [[ -z "${failback}" ]]; then
			answer=$(sc_prompt_yesno "$(gettext 'Do you want to enable the \"preferenced\" attribute?')") || return 1
			if [[ "${answer}" = "yes" ]]; then
				preferenced="true"
			else
				sc_print_prompt "\n$(gettext 'Type ENTER to return to the menu:')"
				read
				echo
				return 1
			fi
		fi

		# Establish list of nodes currently in device group
		if [[ ${CLSETUP_ISMEMBER} -eq 1 ]]; then
			list="$(clsetup_get_devicegroup_nodes ${dgname})"
			if [[ -z "${list}" ]]; then
				printf "$(gettext 'Unable to establish the list of nodes for this device group.')\n\n\a"
				sc_prompt_pause
				return 1
			fi
		fi

		# If "preferenced" is true, get the nodelist and "failback"
		if [[ "${preferenced}" = "true" ]]; then

			# Change the preference order?
			sc_print_para "$(gettext 'Consider the following node preference order for this group:')"
			dg_nodelist="${list}"
			for node in ${list}
			do
				printf "\t${node}\n"
			done
			echo
			answer=$(sc_prompt_yesno "$(gettext 'Do you want to change it?')" "${NO}") || return 1

			# Yes, change the order
			if [[ "${answer}" = "yes" ]]; then
				dg_nodelist=
			fi
			let listcount=$(set -- ${list}; echo $#)
			while [[ -z "${dg_nodelist}" ]]
			do
				# Initialize
				let count=0
				answers=

				# Prompt
				sc_print_para "${sctxt_p1_list}"
				sc_print_para "${sctxt_p2_list}"

				# Get the new order
				while [[ ${count} -lt ${listcount} ]]
				do
					answer=$(sc_prompt "$(gettext 'Node name:')" "" "nonl") || return 1

					# Must be in our list
					let found=0
					for node in ${list}
					do
						if [[ "${answer}" = "${node}" ]]; then
							let found=1
							break
						fi
					done

					# Make sure we found it
					if [[ ${found} -eq 0 ]]; then
						echo
						printf "$(gettext 'Node is not part of this device group.')\n\n\a"
						continue
					fi

					# Make sure it is not a duplicate
					for node in ${answers}
					do
						if [[ "${answer}" = "${node}" ]]; then
							echo
							printf "$(gettext '\"%s\" already entered.')\n\n\a" "${answer}"
							continue 2
						fi
					done

					# Okay, add it to list of answers
					answers="${answers} ${answer}"

					# Next
					((count += 1))
				done

				# Verify that the list is correct
				echo
				sc_print_para "$(gettext 'This is the new preference order:')"
				for node in ${answers}
				do
					printf "\t${node}\n"
				done
				echo
				answer=$(sc_prompt_yesno "$(gettext 'Is it correct?')" "${YES}") || return 1
				if [[ "${answer}" != "yes" ]]; then
					continue
				fi

				# Set the dg nodelist
				dg_nodelist="${answers}"

				# Done
				break
			done

			# Failback?
			failback=
			answer=$(sc_prompt_yesno "$(gettext 'Enable \"failback\" for this disk device group?')" "${NO}") || return 1
			if [[ "${answer}" = "yes" ]]; then
				failback="true"
			else
				failback="false"
			fi
		fi

		# If the dg_nodelist is not set, set to the default
		if [[ -z "${dg_nodelist}" ]]; then
			dg_nodelist="${list}"
		fi

		# Reformat the dg_nodelist to give to cldg
		foo="${dg_nodelist}"
		dg_nodelist=
		for node in ${foo}
		do
			if [[ -n "${dg_nodelist}" ]]; then
				dg_nodelist="${dg_nodelist},"
			fi
			dg_nodelist="${dg_nodelist}${node}"
		done

		#
		# Set up the command set
		#

		# Initialize
		set -A CLSETUP_DO_CMDSET

		# Add the command
		CLSETUP_DO_CMDSET[0]="${CL_CMD_CLDEVICEGROUP} set"

		# Nodelist?
		if [[ -n "${dg_nodelist}" ]]; then
			CLSETUP_DO_CMDSET[0]="${CLSETUP_DO_CMDSET[0]} -n ${dg_nodelist}"
		fi

		# Preferenced?
		if [[ -n "${preferenced}" ]]; then
			CLSETUP_DO_CMDSET[0]="${CLSETUP_DO_CMDSET[0]} -p preferenced=${preferenced}"
		fi

		# Failback?
		if [[ -n "${failback}" ]]; then
			CLSETUP_DO_CMDSET[0]="${CLSETUP_DO_CMDSET[0]} -p failback=${failback}"
		fi

                CLSETUP_DO_CMDSET[0]="${CLSETUP_DO_CMDSET[0]} ${dgname}"

		# Attempt to issue the command
		clsetup_do_cmdset || return 1

		# Done
		break

	done

	return 0
}

#####################################################
#
# clsetup_help_devicegroups_menu()
#
#	Print help information from the device groups menu.
#	This is the same help information as is printed
#	from the MAIN help menu.  But, we ask for confirmation
#	before returning to the previous menu.
#
#	This function always returns zero.
#
#####################################################
clsetup_help_devicegroups_menu()
{
	clear
	clsetup_help_main_devicegroups
	sc_prompt_pause

	return 0
}

#####################################################
#
# clsetup_get_devicegroups_menuoption()
#
#	Print the device groups menu, and return the selected option.
#
#	This function always returns zero.
#
#####################################################
clsetup_get_devicegroups_menuoption()
{
	typeset sctxt_title_1="$(gettext '*** Device Groups Menu ***')"
	typeset sctxt_title_2="$(gettext 'Please select from one of the following options:')"
	typeset sctxt_option_001="$(gettext 'Register a VxVM disk group as a device group')"
	typeset sctxt_option_002="$(gettext 'Synchronize volume information for a VxVM device group')"
	typeset sctxt_option_003="$(gettext 'Unregister a VxVM device group')"
	typeset sctxt_option_004="$(gettext 'Add a node to a VxVM device group')"
	typeset sctxt_option_005="$(gettext 'Remove a node from a VxVM device group')"
	typeset sctxt_option_006="$(gettext 'Change key properties of a device group')"
	typeset sctxt_option_007="$(gettext 'Set a VxVM disk group as a\
		local disk group')"
	typeset sctxt_option_008="$(gettext 'Reset a local disk group\
		to a VxVM disk group')"
	typeset sctxt_option_help="$(gettext 'Help')"
	typeset sctxt_option_return="$(gettext 'Return to the Main Menu')"

	# vxvm might not be available
	typeset select_vxvm="N"
	if [[ -f ${VXVM_SO} ]]; then
	    select_vxvm="S"
	fi

	typeset option

	# option 6 is always available; others requre vxvm
	option=$(sc_get_menuoption \
		"T1+++${sctxt_title_1}" \
		"T2+++${sctxt_title_2}" \
		"${select_vxvm}+1+1+${sctxt_option_001}" \
		"${select_vxvm}+1+2+${sctxt_option_002}" \
		"${select_vxvm}+1+3+${sctxt_option_003}" \
		"${select_vxvm}+1+4+${sctxt_option_004}" \
		"${select_vxvm}+1+5+${sctxt_option_005}" \
		"S+1+6+${sctxt_option_006}" \
		"${select_vxvm}+1+7+${sctxt_option_007}" \
		"${select_vxvm}+1+8+${sctxt_option_008}" \
		"R+++" \
		"S+1+?+${sctxt_option_help}" \
		"S+1+q+${sctxt_option_return}" \
	)

	echo "${option}"

	return 0
}

#####################################################
#
# clsetup_menu_devicegroups()
#
#	Device groups menu
#
#	This function always returns zero.
#
#####################################################
clsetup_menu_devicegroups()
{
	# Loop around the devicegroups menu
	while true
	do
		case $(clsetup_get_devicegroups_menuoption) in
		'1')    clsetup_menu_devicegroups_addvxvm ;;
		'2')	clsetup_menu_devicegroups_syncvxvm ;;
		'3')    clsetup_menu_devicegroups_rmvxvm ;;
		'4')    clsetup_menu_devicegroups_addnodevxvm ;;
		'5')    clsetup_menu_devicegroups_rmnodevxvm ;;
		'6')    clsetup_menu_devicegroups_changeprops ;;
		'7')	clsetup_menu_devicegroups_setlocalonly ;;
		'8')	clsetup_menu_devicegroups_unsetlocalonly ;;
		'?')    clsetup_help_devicegroups_menu ;;
		'q')    break ;;
                esac
	done

	return 0
}

#####################################################
#####################################################
##
## Authentication of nodes able to install into the cluster
##
#####################################################
#####################################################

#####################################################
#
# clsetup_menu_auth_alloff() [nohelp]
#
#	nohelp		- if this flag is given, do not
#				clear screen or print title.
#
#	Prevent any new machines from installing into the cluster.
#
#	Return:
#		zero		Completed
#		non-zero	Not completed
#
#####################################################
clsetup_menu_auth_alloff()
{
	typeset nohelp=${1}

	typeset sctxt_title="$(gettext '
		>>> Prevent Any New Machines from Installing into the Cluster <<<
	')"
	typeset sctxt_p1="$(gettext '
		This option is used to prevent any new machines from
		installing themselves into the cluster.  It does this
		by setting an attribute which tells the processes
		responsible for helping new machines to add themselves
		to the cluster to ignore all such add requests coming
		in over the public network.
	')"

	#
	# Clear screen and print title
	#
	if [[ -z "${nohelp}" ]]; then
		clear
		sc_print_title "${sctxt_title}"
	fi

	# Print help
	sc_print_para "${sctxt_p1}"

	#
	# Set up the command set
	#

	# Initialize
	set -A CLSETUP_DO_CMDSET

	# Add the command
	CLSETUP_DO_CMDSET[0]="${CL_CMD_CLACCESS} deny-all"

	# Attempt to issue the command
	clsetup_do_cmdset || return 1

	return 0
}

#####################################################
#
# clsetup_menu_auth_allon() [nohelp]
#
#	nohelp		- if this flag is given, do not
#				clear screen or print title.
#
#	Allow any machine to install into the cluster.
#
#	Return:
#		zero		Completed
#		non-zero	Not completed
#
#####################################################
clsetup_menu_auth_allon()
{
	typeset nohelp=${1}

	typeset sctxt_title="$(gettext '
		>>> Permit Any Machine to Install itself into the Cluster <<<
	')"
	typeset sctxt_p1="$(gettext '
		This option is used to permit any machine to install
		itself into the cluster.  It does this by completely
		clearing the list of machines recognized as being
		permitted to install themselves into the cluster.  An
		empty list means that any node can add itself.  When
		the processes responsible for helping new machines to
		add themselves to the cluster confirm that the list is
		empty, they attempt to honor all such add requests
		coming in over the public network.
	')"
	typeset sctxt_p2="$(gettext '
		Of course, any machine attempting to install itself as
		a cluster node must first have all of the necessary
		software and hardware correctly installed and configured,
		including a good physical connection to the private cluster
		interconnect.  Refer to the Sun Cluster installation
		documentation and the scinstall(1M) man page for more
		information regarding installation and adding new machines
		to the cluster.
	')"

	#
	# Clear screen and print title
	#
	if [[ -z "${nohelp}" ]]; then
		clear
		sc_print_title "${sctxt_title}"
	fi

	# Print help
	sc_print_para "${sctxt_p1}"
	sc_print_para "${sctxt_p2}"

	#
	# Set up the command set
	#

	# Initialize
	set -A CLSETUP_DO_CMDSET

	# Add the command
	CLSETUP_DO_CMDSET[0]="${CL_CMD_CLACCESS} allow-all"

	# Attempt to issue the command
	clsetup_do_cmdset || return 1

	return 0
}

#####################################################
#
# clsetup_menu_auth_addnode() [nohelp]
#
#	nohelp		- if this flag is given, do not
#				clear screen, print title, print help,
#				or ask for confirmation to continue.
#
#	Add a machine to the list of nodes permitted to install into
#	the cluster.
#
#	Return:
#		zero		Completed
#		non-zero	Not completed
#
#####################################################
clsetup_menu_auth_addnode()
{
	typeset nohelp=${1}

	typeset sctxt_title="$(gettext '
		>>> Specify a Machine which may Install itself into the Cluster <<<
	')"
	typeset sctxt_p1="$(gettext '
		This option is used to permit a given machine to
		install itself into the cluster.  It does this by
		adding the node name that you give to the list of nodes
		recognized as being permitted to install themselves
		into the cluster.
	')"
	typeset sctxt_p2="$(gettext '
		Of course, any machine attempting to install itself as
		a cluster node must first have all of the necessary
		software and hardware correctly installed and configured,
		including a good physical connection to the private cluster
		interconnect.  Refer to the Sun Cluster installation
		documentation and the scinstall(1M) man page for more
		information regarding installation and adding new machines
		to the cluster.
	')"

	typeset addhost
	typeset answer

	#
	# Clear screen and print title
	#
	if [[ -z "${nohelp}" ]]; then
		clear
		sc_print_title "${sctxt_title}"
		sc_print_para "${sctxt_p1}"
		sc_print_para "${sctxt_p2}"

		# Okay to continue?
		clsetup_continue || return 1
	fi

	# Loop until done or Ctrl-D is typed
	while true
	do
		# Get the name of the machine
		answer=$(sc_prompt "$(gettext 'Name of the host to add to the list of recognized machines?')") || return 1
		addhost=${answer}

		#
		# Set up the command set
		#

		# Initialize
		set -A CLSETUP_DO_CMDSET

		# Add the command
		CLSETUP_DO_CMDSET[0]="${CL_CMD_CLACCESS} allow -h ${addhost}"

		# Attempt to issue the command
		clsetup_do_cmdset || return 1

		# Done
		break
	done

	return 0
}

#####################################################
#
# clsetup_menu_auth_unix() [nohelp]
#
#	nohelp		- if this flag is given, do not
#				clear screen or print title.
#
#	Use standard UNIX authentication methods for install.
#
#	Return:
#		zero		Completed
#		non-zero	Not completed
#
#####################################################
clsetup_menu_auth_unix()
{
	typeset nohelp=${1}

	typeset sctxt_title="$(gettext '
		>>> Use standard UNIX Authentication <<<
	')"
	typeset sctxt_p1="$(gettext '
		To be added as a cluster node, a machine must appear in
		the list of hosts that are permitted to install themselves
		into cluster configuration.  If this list is empty, any
		machine may add itself to the cluster configuration.
	')"
	typeset sctxt_p2="$(gettext '
		The identity of machines contacting the cluster over
		the public network for installation purposes may be
		confirmed using either standard UNIX or the more secure
		Diffie-Hellman, or DES, authentication.  However, DES
		authentication is not usually considered necessary for
		this purpose, since machines which are not physically
		connected to the cluster interconnect will never be
		able to actually join the cluster membership anyway.
	')"
	typeset sctxt_p3="$(gettext '
		Standard UNIX authentication is the default.  This option
		is used to restore this default.
	')"

	#
	# Clear screen and print title
	#
	if [[ -z "${nohelp}" ]]; then
		clear
		sc_print_title "${sctxt_title}"
	fi

	# Print help
	sc_print_para "${sctxt_p1}"
	sc_print_para "${sctxt_p2}"
	sc_print_para "${sctxt_p3}"

	#
	# Set up the command set
	#

	# Initialize
	set -A CLSETUP_DO_CMDSET

	# Add the command
	CLSETUP_DO_CMDSET[0]="${CL_CMD_CLACCESS} set -p protocol=sys"

	# Attempt to issue the command
	clsetup_do_cmdset || return 1

	return 0
}


#####################################################
#
# clsetup_menu_auth_des() [nohelp]
#
#	nohelp		- if this flag is given, do not
#				clear screen or print title.
#
#	Use DES authentication methods for install.
#
#	Return:
#		zero		Completed
#		non-zero	Not completed
#
#####################################################
clsetup_menu_auth_des()
{
	typeset nohelp=${1}

	typeset sctxt_title="$(gettext '
		>>> Use Diffie-Hellman Authentication <<<
	')"
	typeset sctxt_p1="$(gettext '
		To be added as a cluster node, a machine must appear in
		the list of hosts that are permitted to install themselves
		into cluster configuration.  If this list is empty, any
		machine may add itself to the cluster configuration.
	')"
	typeset sctxt_p2="$(gettext '
		The identity of machines contacting the cluster over
		the public network for installation purposes may be
		confirmed using either standard UNIX or the more secure
		Diffie-Hellman, or DES, authentication.  However, DES
		authentication is not usually considered necessary for
		this purpose, since machines which are not physically
		connected to the private cluster interconnect will
		never be able to actually join the cluster membership
		anyway.
	')"
	typeset sctxt_p3="$(gettext '
		This option is used to select DES authentication methods
		for use during node installation.  Remember that if
		DES authentication is selected, you must also configure
		all necessary encryption keys before a node can join
		(see keyserv(1M), publickey(4)).
	')"

	#
	# Clear screen and print title
	#
	if [[ -z "${nohelp}" ]]; then
		clear
		sc_print_title "${sctxt_title}"
	fi

	# Print help
	sc_print_para "${sctxt_p1}"
	sc_print_para "${sctxt_p2}"
	sc_print_para "${sctxt_p3}"

	#
	# Set up the command set
	#

	# Initialize
	set -A CLSETUP_DO_CMDSET

	# Add the command
	CLSETUP_DO_CMDSET[0]="${CL_CMD_CLACCESS} set -p protocol=des"

	# Attempt to issue the command
	clsetup_do_cmdset || return 1

	return 0
}

#####################################################
#
# clsetup_help_auth_menu()
#
#	Print help information from the auth menu.
#	This is the same help information as is printed
#	from the MAIN help menu.  But, we ask for confirmation
#	before returning to the previous menu.
#
#	This function always returns zero.
#
#####################################################
clsetup_help_auth_menu()
{
	clear
	clsetup_help_main_auth
	sc_prompt_pause

	return 0
}

#####################################################
#
# clsetup_get_auth_menuoption()
#
#	Print the authentication menu, and return the selected option.
#
#	This function always returns zero.
#
#####################################################
clsetup_get_auth_menuoption()
{
	typeset sctxt_title_1="$(gettext '*** New Nodes Menu ***')"
	typeset sctxt_title_2="$(gettext 'Please select from one of the following options:')"
	typeset sctxt_option_001="$(gettext 'Prevent any new machines from being added to the cluster')"
	typeset sctxt_option_002="$(gettext 'Permit any machine to add itself to the cluster')"
	typeset sctxt_option_003="$(gettext 'Specify the name of a machine which may add itself')"
	typeset sctxt_option_004="$(gettext 'Use standard UNIX authentication')"
	typeset sctxt_option_005="$(gettext 'Use Diffie-Hellman authentication')"
	typeset sctxt_option_help="$(gettext 'Help')"
	typeset sctxt_option_return="$(gettext 'Return to the Main Menu')"

	typeset option

	option=$(sc_get_menuoption \
		"T1+++${sctxt_title_1}" \
		"T2+++${sctxt_title_2}" \
		"S+0+1+${sctxt_option_001}" \
		"S+0+2+${sctxt_option_002}" \
		"S+0+3+${sctxt_option_003}" \
		"S+0+4+${sctxt_option_004}" \
		"S+0+5+${sctxt_option_005}" \
		"R+++" \
		"S+0+?+${sctxt_option_help}" \
		"S+0+q+${sctxt_option_return}" \
	)

	echo "${option}"

	return 0
}

#####################################################
#
# clsetup_menu_auth()
#
#	Authentication menu
#
#	This function always returns zero.
#
#####################################################
clsetup_menu_auth()
{
	# Loop around the auth menu
	while true
	do
		case $(clsetup_get_auth_menuoption) in
		'1')    clsetup_menu_auth_alloff ;;
		'2')    clsetup_menu_auth_allon ;;
		'3')    clsetup_menu_auth_addnode ;;
		'4')    clsetup_menu_auth_unix ;;
		'5')    clsetup_menu_auth_des ;;
		'?')    clsetup_help_auth_menu ;;
		'q')    break ;;
                esac
	done

	return 0
}

#####################################################
#####################################################
##
## Other cluster-wide options
##
#####################################################
#####################################################

#####################################################
#
# clsetup_menu_other_clustername() [nohelp]
#
#	nohelp		- if this flag is given, do not
#				clear screen, print title, print help,
#				or ask for confirmation to continue.
#
#	Change the the name of the cluster.
#
#	Return:
#		zero		Completed
#		non-zero	Not completed
#
#####################################################
clsetup_menu_other_clustername()
{
	typeset nohelp=${1}

	typeset sctxt_title="$(gettext '
		>>> Changing the Name of the Cluster <<<
	')"
	typeset sctxt_p1="$(gettext '
		This option is used to change the name of the cluster.
	')"

	typeset answer
	typeset clustername

	#
	# Print help
	#
	if [[ -z "${nohelp}" ]]; then
		clear
		sc_print_title "${sctxt_title}"
		sc_print_para "${sctxt_p1}"

		# Okay to continue?
		clsetup_continue || return 1
	fi

	# Loop until done or Ctrl-D is typed
	while true
	do
		# Get the new cluster name
		answer=$(sc_prompt "$(gettext 'What name do you want to assign to this cluster?')") || return 1
		clustername=${answer}

		#
		# Set up the command set
		#

		# Initialize
		set -A CLSETUP_DO_CMDSET

		# Add the command
		CLSETUP_DO_CMDSET[0]="${CL_CMD_CLUSTER} rename -c ${clustername}"

		# Attempt to issue the command
		clsetup_do_cmdset || return 1

		# Done
		break
	done

	return 0
}

#####################################################
#
# clsetup_menu_other_telemetry() [nohelp]
#
#	nohelp		- if this flag is given, do not
#				clear screen, print title, print help,
#				or ask for confirmation to continue.
#
#	Change the the name of the cluster.
#
#	Return:
#		zero		Completed
#		non-zero	Not completed
#
#####################################################
clsetup_menu_other_telemetry()
{
	typeset nohelp=${1}

	typeset sctxt_title="$(gettext '
		>>> Configure Cluster Telemetry <<<
	')"

	typeset sctxt_prerequisite1="$(gettext '
		In order to successfully configure telemetry for this cluster, you must have already done the following:
	')"
	typeset sctxt_prerequisite2="$(gettext '
		* Configured a file system for the database.
	')"
	typeset sctxt_prerequisite3="$(gettext '
		* Created an HA Storage Plus resource for that file system.
	')"

	typeset sctxt_explanation="$(gettext '
		This wizard creates resources and resource groups required for cluster telemetry. In addition, a database used to store telemetry data is installed in the location you specify.
	')"

	typeset sctxt_property_hasprs="$(gettext 'HA Storage Plus resource')"
	typeset sctxt_property_hasprg="$(gettext 'HA Storage Plus resource group')"
	typeset sctxt_property_filesystem="$(gettext 'Mount point')"
	typeset sctxt_property_cldbrg="$(gettext 'Telemetry database resource group')"
	typeset sctxt_property_cldbrs="$(gettext 'Telemetry database resource')"
	typeset sctxt_property_cltlmtryrg="$(gettext 'Telemetry resource group')"
	typeset sctxt_property_cltlmtryrs="$(gettext 'Telemetry resource')"

	typeset sctxt_option_return="$(gettext 'Return to the Other Cluster Tasks Menu')"
	typeset sctxt_option_configure="$(gettext 'Configure with current values')"
	typeset sctxt_p1_exist="$(gettext 'This cluster is already configured for telemetry:')"

	typeset sctxt_nopkg_error="$(gettext '
		\nTo configure telemetry, you must install the IPS package \"%s\".
	')"

	typeset rlist
	typeset rglist
	typeset hasprs
	typeset hasprg
	typeset hasprglist
	typeset filesystem
	typeset tmphasprs
	typeset tmpfilesystem
	typeset tmpcltlmtryrg
	typeset prompt
	typeset mntpt
	typeset cldbrg="cl-db-rg"
	typeset cldbrs="cl-db-rs"
	typeset cltlmtryrg="cl-tlmtry-rg"
	typeset cltlmtryrs="cl-tlmtry-rs"
	typeset updating

	#
	# No IPS package installed?
	#
	if [[ ${LIST_TELEMETRY} -eq 0 ]]; then
		clear
		sc_print_para "$(printf "${sctxt_nopkg_error}" "${SC_SLM_IPSPKG}")"

		# Okay to continue?
                sc_prompt_pause
		return 1
	fi

	#
	# Print help
	#
	if [[ -z "${nohelp}" ]]; then
		clear
		sc_print_title "${sctxt_title}"

		sc_print_para "${sctxt_explanation}"
		sc_print_para "${sctxt_prerequisite1}"
		sc_print_line "${sctxt_prerequisite2}"
		echo
		sc_print_line "${sctxt_prerequisite3}"
		echo
		echo
		# Okay to continue?
		clsetup_continue || return 1
	fi

	# Retrieve the Resource Group of the Resource sctelemtry
	# the RT for SUNW.sctelemetry has single_instance set to true
	tmpcltlmtryrg="$(clsetup_get_rglist "" "" "SUNW.sctelemetry" "")"
	if [[ -n "${tmpcltlmtryrg}" ]]; then
		    # there is already a Resource of type sctelemetry
		    # Need to find out what are the other RG/R names
		    # used for the telemetry

		    cltlmtryrg=${tmpcltlmtryrg}

		    # get the Resource name
		    cltlmtryrs="$(clsetup_get_rlist_from_rtype "SUNW.sctelemetry" "${cltlmtryrg}")"

		    # Retrieve the Database Resource Group
		    cldbrg=`${SCHA_RG_GET} -O RG_DEPENDENCIES -G ${cltlmtryrg}`

		    # get the Database Resource name
		    cldbrs="$(clsetup_get_rlist_from_rtype "SUNW.derby" "${cldbrg}")"


		    property003="$(printf "%-35s %-20.20s\n" "${sctxt_property_cldbrg}" "${cldbrg}")"
		    property004="$(printf "%-35s %-20.20s\n" "${sctxt_property_cldbrs}" "${cldbrs}")"
		    property005="$(printf "%-35s %-20.20s\n" "${sctxt_property_cltlmtryrg}" "${cltlmtryrg}")"
		    property006="$(printf "%-35s %-20.20s\n" "${sctxt_property_cltlmtryrs}" "${cltlmtryrs}")"

		    # display the collected names
		    sc_print_para ${sctxt_p1_exist}
		    sc_print_line "${property003}"
		    echo

		    sc_print_line "${property004}"
		    echo

		    sc_print_line "${property005}"
		    echo

		    sc_print_line "${property006}"

		    echo
		    echo

		    sc_print_prompt "\n$(gettext 'Type ENTER to return to the menu:')"
		    read
		    echo
		    return 1

	fi

	# Loop until done or Ctrl-D is typed
	while true
	do

		hasprs="$(clsetup_menu_other_telemetry_hastorageplus)"
		if [[ -z "${hasprs}" ]]; then
			sc_print_para "$(gettext 'There is no HA Storage Plus resource configured on the cluster.')"
			sc_print_prompt "\n$(gettext 'Type ENTER to return to the menu:')"
			read
			echo
			return -1
		fi


		filesystem="$(clsetup_menu_other_telemetry_filesystem ${hasprs})"
		if [[ -z "${filesystem}" ]]; then
			echo
			sc_print_para "$(printf "$(gettext 'There are no FilesystemMountPoints configured in the resource %s.')" "${hasprs}")"
			sc_print_prompt "\n$(gettext 'Type ENTER to return to the menu:')"
			read
			echo

			return -1
		fi

		# Print summary
		header1="$(gettext ' *** Summary *** ')"
		header2="$(gettext 'The following Sun Cluster configuration will now be created. If you want to modify the configuration to be created, select the property you want to change.')"


	    while true
	    do
		    hasprg=

		    # Find the RG in which the HA Stoprage Plus resource is located
		    hasprglist="$(clsetup_get_rglist "" "" "SUNW.HAStoragePlus" "")"
		    if [[ -z ${hasprglist} ]]; then
			    return -1
		    fi

		    # temporay iterators
		    typeset rg
		    typeset r

		    for rg in ${hasprglist}
		    do
			    rlist=$(${SCHA_RG_GET} -O Resource_list -G ${rg})
			    for r in ${rlist}
			    do
				    if [[ "${r}" = "${hasprs}" ]]; then
					    hasprg=${rg}
					    break;
				    fi
			    done
			    if [[ -n "${hasprg}" ]]; then
				    break;
			    fi

		    done

		    if [[ -z "${hasprg}" ]]; then
			    return -1
		    fi

		    property001="$(printf "%-35s %-30.30s\n" "${sctxt_property_hasprs}" "${hasprs}")"
		    property001a="$(printf "%-35s %-30.30s\n" "${sctxt_property_hasprg}" "${hasprg}")"
		    property002="$(printf "%-35s %-30.30s\n" "${sctxt_property_filesystem}" "${filesystem}")"
		    property003="$(printf "%-35s %-30.30s\n" "${sctxt_property_cldbrg}" "${cldbrg}")"
		    property004="$(printf "%-35s %-30.30s\n" "${sctxt_property_cldbrs}" "${cldbrs}")"
		    property005="$(printf "%-35s %-30.30s\n" "${sctxt_property_cltlmtryrg}" "${cltlmtryrg}")"
		    property006="$(printf "%-35s %-30.30s\n" "${sctxt_property_cltlmtryrs}" "${cltlmtryrs}")"

		    updating=$(sc_get_menuoption \
			"T1+++${header1}" \
			"T2+++${header2}" \
			"S+0+1+${property001}" \
			"N+1+X+${property001a}" \
			"S+0+2+${property002}" \
			"S+0+3+${property003}" \
			"S+0+4+${property004}" \
			"S+0+5+${property005}" \
			"S+0+6+${property006}" \
			"R+++" \
			"S+0+c+${sctxt_option_configure}" \
			"S+0+q+${sctxt_option_return}" \
		    )

		    case ${updating} in
		    '1') tmphasprs="$(clsetup_menu_other_telemetry_hastorageplus)"
			 if [[ -z "${tmphasprs}" ]]; then
			    # should never happen, Only if the HA Storage Plus resource
			    # got removed while we were executing this action

			    # go back to the menu without further notice
			    return -1
			 fi

			# If we change the resource, we also need to update the filesystem
			tmpfilesystem="$(clsetup_menu_other_telemetry_filesystem ${tmphasprs})"
			if [[ -z "${tmpfilesystem}" ]]; then
			    echo
			    sc_print_para "$(printf "$(gettext 'There are no FilesystemMountPoints configured in the Resource %s.')" "${tmphasprs}")"
			    sc_print_prompt "\n$(gettext 'Type ENTER to return to the Summary:')"
			    read
			else
			    # Both value are correct, update them
			    hasprs=${tmphasprs}
			    filesystem=${tmpfilesystem}
			fi
			;;

		    '2') filesystem="$(clsetup_menu_other_telemetry_filesystem ${hasprs})"
			 if [[ -z "${filesystem}" ]]; then
			    return -1
			 fi
			 ;;
		    '3') cldbrg=$(sc_prompt "$(gettext 'What is the name of the telemetry database resource group?')") ;;
		    '4') cldbrs=$(sc_prompt "$(gettext 'What is the name of the telemetry database resource?')") ;;
		    '5') cltlmtryrg=$(sc_prompt "$(gettext 'What is the name of the telemetry resource group?')") ;;
		    '6') cltlmtryrs=$(sc_prompt "$(gettext 'What is the name of the telemetry resource?')") ;;
		    'c') break;;
		    'q') return 0 ;;
		    esac
		done

		# last verification
		if [[ -z "${hasprs}" ]] || [[ -z "${filesystem}" ]] || [[ -z "${cldbrg}" ]] ||
		   [[ -z "${cldbrs}" ]] || [[ -z "${cltlmtryrg}" ]] || [[ -z "${cltlmtryrs}" ]]; then
			# This will never happen
			return -1
		fi


		#
		# Set up the command set
		#

		# Initialize
		set -A CLSETUP_DO_CMDSET

		# Add the command
		CLSETUP_DO_CMDSET[0]="${SCTELEMETRY} -i -o hasp_mnt_pt=${filesystem},hasp_rg=${hasprg},hasp_rs=${hasprs},db_rg=${cldbrg},db_rs=${cldbrs},telemetry_rg=${cltlmtryrg},telemetry_rs=${cltlmtryrs}"

		# Attempt to issue the command
		clsetup_do_cmdset || return 1

		# Done
		break
	done

	return 0
}

#####################################################
#
# clsetup_menu_other_telemetry_hastorageplus()
#
#	Ask the user for an HAStoragePlus resource
#	and prints it
#
#	Always return zero
#
#####################################################
clsetup_menu_other_telemetry_hastorageplus()
{
	typeset rlist
	typeset hasprs
	typeset prompt

	# ask for an HA Storage Plus resource
	rlist="$(clsetup_get_rlist_from_rtype "SUNW.HAStoragePlus" "")"

	prompt="$(gettext 'Which HA Storage Plus resource would you like to use?')"
	hasprs="$(
	    sc_get_scrolling_menuoptions	\
		"${prompt}"			\
		"" ""				\
		1 1 1				\
		${rlist}			\
	)"

	echo ${hasprs}

	return 0
}

#####################################################
#
# clsetup_menu_other_telemetry_filesystem() hasprs
#
# hasprs :
#
#	Ask the user for a filesystem from an
#	HAStoragePlus resource and prints it
#
#	Always return zero
#
#####################################################
clsetup_menu_other_telemetry_filesystem()
{
	typeset hasprs=${1}
	typeset prompt="$(gettext ' The following mount points are accessible from the HA Storage Plus resource. Select the one you want to use for the database :')"
	typeset mntptlist
	typeset filesystem

	# ask for the right mount point
	mntptlist=`${SCHA_RS_GET} -O Extension -R ${hasprs} FilesystemMountPoints`
	mntptlist=`echo ${mntptlist}  | awk '{ for (i = 2; i <= NF; i++) { printf $i" " }}'`

	filesystem="$(
	    sc_get_scrolling_menuoptions	\
		"${prompt}"			\
		"" ""				\
		1 1 1				\
		${mntptlist}			\
	)"

	echo ${filesystem}

	return 0
}

#####################################################
#
# clsetup_help_other_menu()
#
#	Print help information from the other cluster props menu.
#	This is the same help information as is printed
#	from the MAIN help menu.  But, we ask for confirmation
#	before returning to the previous menu.
#
#	This function always returns zero.
#
#####################################################
clsetup_help_other_menu()
{
	clear
	clsetup_help_main_other
	sc_prompt_pause

	return 0
}

#####################################################
#
# clsetup_get_other_menuoption()
#
#	Print the other cluster options tasks, and return the selected option.
#
#	This function always returns zero.
#
#####################################################
clsetup_get_other_menuoption()
{
	typeset sctxt_title_1="$(gettext '*** Other Cluster Tasks Menu ***')"
	typeset sctxt_title_2="$(gettext 'Please select from one of the following options:')"
	typeset sctxt_option_001="$(gettext 'Change the name of the cluster')"
	typeset sctxt_option_002="$(gettext 'Configure Telemetry')"
	typeset sctxt_option_help="$(gettext 'Help')"
	typeset sctxt_option_return="$(gettext 'Return to the Main Menu')"

	typeset option

	option=$(sc_get_menuoption \
		"T1+++${sctxt_title_1}" \
		"T2+++${sctxt_title_2}" \
		"S+0+1+${sctxt_option_001}" \
		"S+0+2+${sctxt_option_002}" \
		"R+++" \
		"S+0+?+${sctxt_option_help}" \
		"S+0+q+${sctxt_option_return}" \
	)

	echo "${option}"

	return 0
}

#####################################################
#
# clsetup_menu_other()
#
#	Other cluster tasks menu
#
#	This function always returns zero.
#
#####################################################
clsetup_menu_other()
{
	# Loop around
	while true
	do
		case $(clsetup_get_other_menuoption) in
		'1')    clsetup_menu_other_clustername ;;
		'2')    clsetup_menu_other_telemetry ;;
		'?')    clsetup_help_other_menu ;;
		'q')    break ;;
                esac
	done

	return 0
}

############################################################
#
# clsetup_set_farmnode_props()
#
#	Get the list of farm nodes.
#	This function is also used to populate the
#	global array.
#
#	CLSETUP_FARM_NODELIST - Array containing the list of
#				farmnodes.
#	CLSETUP_FARM_MONITORING_STATE -  Array containing the
#				membership state of the farm nodes.
#
##############################################################
clsetup_set_farmnode_props()
{
	typeset farmnode_list=
	typeset farmnode
	typeset monitoring_state

	# Global arrays.
	set -A CLSETUP_FARM_NODELIST
	set -A CLSETUP_FARM_MONITORING_STATE

	#
	# In debug mode, fabricate data and return success
	#
	if [[ "${SC_DEBUG}" = "3" ]]; then
		CLSETUP_FARM_NODELIST[0]="debug_farm_node1"
		CLSETUP_FARM_MONITORING_STATE[0]="Enabled"

		CLSETUP_FARM_NODELIST[1]="debug_farm_node2"
		CLSETUP_FARM_MONITORING_STATE[1]="Disabled"

		CLSETUP_FARM_NODELIST[2]="debug_farm_node3"
		CLSETUP_FARM_MONITORING_STATE[2]="Enabled"

		return 0
	fi

	if [[ ${CLSETUP_IS_EUROPA} -eq 1 ]]; then

		farmnode_list="$(${CL_CMD_CLNODE} list -t farm 2>/dev/null)"

		# Populate the Farm node array.
		let i=0
		if [[ -z "${farmnode_list}" ]]; then
			return 0
		fi

		for farmnode in ${farmnode_list}
		do
			CLSETUP_FARM_NODELIST[i]="${farmnode}"

			# Populate the farm node Monitoring state array.
			 monitoring_state="$(
			 	LC_ALL=C; export LC_ALL;
				${CL_CMD_CLNODE} show -p monitoring ${farmnode} | awk '/monitoring:/{print $2}'
			 )"
			if [[ ${monitoring_state} != "enabled" && ${monitoring_state} != "disabled" ]]; then
				# Invalid state
				return 1
			fi

			CLSETUP_FARM_MONITORING_STATE[i]="${monitoring_state}"
			((i += 1))
		done
		# Return Success.
		return 0
	else
		return 1
	fi
}

#####################################################
#
# clsetup_get_farmnodelist()
#
#	Print the list of farmnodes.
#
#	Always returns 0
#####################################################
clsetup_get_farmnodelist()
{
	typeset farmnodelist=
	typeset farmnode=

	for farmnode in ${CLSETUP_FARM_NODELIST[*]}
	do
		if [[ -z ${farmnodelist} ]]; then
			farmnodelist=${farmnode}
		else
			farmnodelist="${farmnodelist} ${farmnode}"
		fi
	done

	# Display the farmnode list.
	echo ${farmnodelist}
	return 0
}


#####################################################
#
# clsetup_get_farmnode_index() ${farmnode}
#
#	Print the index of the farmnode in the CLSETUP_FARM_NODELIST array.
#
#	Case is ignored.
#
#	Return values:
#
#		-1		Unknown property
#		0		Success
#
#####################################################
clsetup_get_farmnode_index()
{
	typeset -l farmnode=${1}		# lower case

	typeset -l name				# lower case
	integer index
	integer found

	# Get the index
	let index=0
	let found=0
	for name in ${CLSETUP_FARM_NODELIST[*]}
	do
		if [[ "${farmnode}" == "${name}" ]]; then
			let found=1
			break
		fi

		((index += 1))
	done

	# If we didn't find it, return -1
	if [[ ${found} -ne 1 ]]; then
		return -1
	fi

	# Print the index
	echo ${index}

	return 0
}

#####################################################
#
# clsetup_get_farm_node_menu_option
#
#	Print the farm node menu.
#
#	This function returns zero.
#####################################################
clsetup_get_farm_node_menu_option()
{

	typeset sctxt_title_1="$(gettext '*** Farm Management Menu ***')"
	typeset sctxt_title_2="$(gettext 'Please select from one of the following options:')"
	typeset sctxt_option_001="$(gettext 'Add a farm node ')"
	typeset sctxt_option_002="$(gettext 'Remove a farm node ')"
	typeset sctxt_option_003="$(gettext 'Change a farm node monitoring state ')"
	typeset sctxt_option_status="$(gettext 'Show current status')"
	typeset sctxt_option_return="$(gettext 'Return to the main menu')"

	typeset option

	option=$(sc_get_menuoption \
		"T1+++${sctxt_title_1}" \
		"T2+++${sctxt_title_2}" \
		"S+0+1+${sctxt_option_001}" \
		"S+0+2+${sctxt_option_002}" \
		"S+0+3+${sctxt_option_003}" \
		"R+++" \
		"S+0+s+${sctxt_option_status}" \
		"S+0+q+${sctxt_option_return}" \
	)
		echo ${option}
		return 0
}

####################################################
#
# clsetup_menu_europa_add_farm_node()
#
# 	Add a new farm node. This command is used to add
# 	Farm node.
#
#
####################################################
clsetup_menu_europa_add_farm_node() {
	typeset nohelp=${1}

	typeset sctxt_title="$(gettext '
		>>> Add a farm node<<<
	')"
	typeset sctxt_p1="$(gettext '
		Use this option to identify a host
		as a farm node.
	')"
	typeset sctxt_p2="$(gettext '
		A machine cannot become a farm node unless
		it has the required software and hardware
		correctly configured. These requirements
		include a physical connection to the private
		cluster interconnect. For more information,
		see the documentation for farm node installation.
	')"

	typeset addhost
	typeset answer
	typeset adapterlist=
	typeset -l another=

	#
	# Clear screen and print title
	#
	if [[ -z "${nohelp}" ]]; then
		clear
		sc_print_title "${sctxt_title}"
		sc_print_para "${sctxt_p1}"
		sc_print_para "${sctxt_p2}"

		# Okay to continue?
		clsetup_continue || return 1
	fi

	# Loop until done or Ctrl-D is typed
	while true
	do
		# Get the name of the host.
		answer=$(sc_prompt "$(gettext 'Name of the host to add to the list of Farm nodes?')") || return 1
		addhost=${answer}

		# Check if the farm node is present in the Farm nodelist.
		clsetup_is_farm_node_present "${addhost}"
		if [[ $? -eq 1 ]]; then
			printf "$(gettext 'Farm node %s specified is already present. Please specify a different name. ')\n\n\a" "${addhost}"
			continue
		fi

		# Get the first adapter name for the farm node.
		adapterlist=$(sc_prompt "$(gettext 'Specify the first adapter for the farm node ?')") || return 1

		# Get the second adapter name for the farm node.
		answer=
		printf "\n$(gettext '    Specify the second adapter for the farm node ?')" || return 1
		read answer
		echo
		echo
		if [[ -n ${answer} ]]; then
			adapterlist=${adapterlist},${answer}
		fi

		#
		# Set up the command set
		#

		# Initialize
		set -A CLSETUP_DO_CMDSET

		# Add the command
		CLSETUP_DO_CMDSET[0]="${CL_CMD_CLNODE} add-farm -p adapterlist=${adapterlist} ${addhost}"

		# Attempt to issue the command
		clsetup_do_cmdset || return 1

		# Done
		clsetup_set_farmnode_props

		# Do you want to add another farm node?
		another=
		another=$(sc_prompt_yesno "$(gettext 'Do you want to add another farm node?')" "${YES}") || return 1
		if [[ "${another}" = "no" ]]; then
			# Done
			break
		fi
	done

	return 0

}

#####################################################
#
# clsetup_is_farm_node_present() farmnode
#
#	This function is used to check whether the
#	given farm node is present in the list of
#	Farm nodes.
#
#	CLSETUP_FARM_NODELIST : Array containing the list
#				of Farm nodes.
#
#	Returns
#		1  - If farm node is present.
# 		0  - If Farm node is not present.
#		-1 - Error.
#######################################################
clsetup_is_farm_node_present()
{

	typeset farmnode=${1}
	typeset index=

	# Get the index for the given farmnode.
	index=$(clsetup_get_farmnode_index "${farmnode}")
	if [[ -n ${index} ]]; then
		return 1
	else
		return 0
	fi
	# Return error
	return -1
}

#############################################################
#
# clsetup_is_farm_monitoring_enabled() farmnode
#
#	This function is used to check whether farm
#	monitoring is enabled on the particular farm node.
#
#	CLSETUP_FARM_MONITORING_STATE : Array containing the
#					farm node monitoring state.
#
#	Returns
#		0  - If disabled.
#		1  - If enabled.
#		-1 - Error
################################################################
clsetup_is_farm_monitoring_enabled()
{
	typeset farmnode=${1}
	typeset index=
	typeset -l monitoring_state=

	# Get the index for the given farmnode.
	index=$(clsetup_get_farmnode_index "${farmnode}")
	if [[ -n ${index} ]]; then
		monitoring_state=${CLSETUP_FARM_MONITORING_STATE[index]}
		if [[ ${monitoring_state} == "enabled" ]]; then
			return 1
		else
			return 0
		fi
	fi

	# Error.
	return -1
}

#####################################################
#
# clsetup_menu_europa_remove_farm_node
#
#	This function is used to remove a farm node
#	from the list of Farm nodes.
#
####################################################
clsetup_menu_europa_remove_farm_node() {

	typeset nohelp=${1}

	typeset sctxt_title="$(gettext '
		>>> Remove a farm node<<<
	')"
	typeset sctxt_p1="$(gettext '
		This option removes a host from the
		farm node list.
	')"
	typeset sctxt_p2="$(gettext '
		The farm node should only be removed only if its
		farm monitoring state is disabled.
	')"

	typeset tmpfile=${CLSETUP_TMP_FARMNODE_LIST}
	typeset removehost
	typeset answer
	typeset -l another=
	typeset -l specify
	typeset fnode

	#
	# Clear screen and print title
	#
	if [[ -z "${nohelp}" ]]; then
		clear
		sc_print_title "${sctxt_title}"
		sc_print_para "${sctxt_p1}"
		sc_print_para "${sctxt_p2}"

		# Okay to continue?
		clsetup_continue || return 1
	fi

	# Loop until done or Ctrl-D is typed
	while true
	do
		# Create the temp file
		if [[ -f ${tmpfile} ]]; then
			rm -f ${tmpfile}
		fi

		let found=0
		farmnode=
		for farmnode in ${CLSETUP_FARM_NODELIST[*]}
		do
			((found += 1))
			printf "%-25s \n" "${farmnode}"
		done > ${tmpfile}

		# Are there any farmnodes?
		if [[ ${found} -lt 1 ]]; then
			echo
			# Return if nod farm  nodes are configured.
			printf "    $(gettext 'There are no farmnodes configured.')\n\n\a"
			sc_prompt_pause
			rm -f ${tmpfile}
			return 1
		fi

		specify=$(sc_prompt_yesno "$(gettext 'Do you want to explicitly specify the farm node by name?')" "${YES}") || return 1
		if [[ "${specify}" == "yes" ]]; then
			removehost=$(sc_prompt "$(gettext 'Specify the name of the farm node you want to remove ?')") || return 1
		else
			prompt="$(gettext 'Specify the name of the farm node you want to remove ?')" || return 1
			removehost=
			removehost="$(
	    			sc_get_scrolling_menuoptions	\
				"${prompt}"			\
				"Farm nodes"			\
				""				\
				1 1 1				\
				:${tmpfile}			\
				)"
			rm -f ${tmpfile}
		fi

		clsetup_is_farm_monitoring_enabled "${removehost}"
		# Prevent farm node removal, if the farm node monitoring state is enabled.
		if [[ $? -eq 1 ]]; then
			printf "$(gettext 'Farm node %s monitoring state is enabled')\n\n\a"
			answer=$(sc_prompt_yesno "$(gettext 'Do you want to disable farm monitoring state?')" "${YES}") || return 1
			if [[ ${answer} == "yes" ]]; then
				# Initialize
				set -A CLSETUP_DO_CMDSET
				# Add the command
				CLSETUP_DO_CMDSET[0]="${CL_CMD_CLNODE} unmonitor ${removehost}"
				# Attempt to issue the command
				clsetup_do_cmdset || return 1
			fi
		fi

		# Return if no host is given.
		if [[ -z ${removehost} ]]; then
  			return 1
		fi
		#
		# Set up the command set
		#
		# Initialize
		set -A CLSETUP_DO_CMDSET

		# Add the command
		CLSETUP_DO_CMDSET[0]="${CL_CMD_CLNODE} remove-farm ${removehost}"

		# Attempt to issue the command
		clsetup_do_cmdset || return 1

		# Done
		clsetup_set_farmnode_props
		# Do you want to remove another farm node?
		another=
		another=$(sc_prompt_yesno "$(gettext 'Do you want to remove another farm node?')" "${YES}") || return 1
		if [[ "${another}" = "no" ]]; then
			# Done
			break
		fi
	done

}

##################################################################
#
# clsetup_menu_europa_change_farm_monitoring() [nohelp]
#
#	nohelp		- if this flag is given, do not
#			    clear screen, print title, print help,
#			    or ask for confirmation to continue
#
#	Enable or disable monitoring state of a Farm node.
#
#	Return:
#		zero		Completed
#		non-zero	Not completed
#
####################################################################
clsetup_menu_europa_change_farm_monitoring()
{
	typeset nohelp=${1}

	typeset sctxt_title="$(gettext '
		>>> Change farm monitoring state.<<<
	')"
	typeset sctxt_p1="$(gettext '
		This option enables/disables the farm
		monitoring state.
	')"

	typeset tmpfile=${CLSETUP_TMP_FARMNODE_LIST}

	typeset prompt
	typeset header1
	typeset header2

	typeset prompt2
	typeset answer
	typeset farmnode
	typeset farmnodelist
	typeset state
	typeset statetype

	typeset fnode
	typeset fnodes
	typeset farmnodenames=
	typeset farmnode
	typeset nodename
	typeset -l another=
	typeset foo
	integer found

	# Unless nohelp is specified, print help and get confirmation
	if [[ -z "${nohelp}" ]]; then

		# Clear screen, print title and help
		sc_print_title "${sctxt_title}"
		sc_print_para "${sctxt_p1}"

		# Okay to continue?
		clsetup_continue || return 1
	fi

	# Loop until done or Ctrl-D is typed
	while true
	do
		# Initialize
		if [[ -f ${tmpfile} ]]; then
			rm -f ${tmpfile}
		fi

		prompt="$(gettext 'Select the Farm node for which you want to modify the monitoring state:')"
		header1="$(printf "%-25s %-16.16s" "$(gettext 'Farm node')" "$(gettext 'Monitoring State')")"
		header2="$(printf "%-25s %-16.16s" "===============" "=====================")"

		# Create the temp file
		let found=0
		for farmnode in ${CLSETUP_FARM_NODELIST[*]}
		do
			if [[ ${CLSETUP_ISMEMBER} -eq 1 ]];  then
				clsetup_is_farm_monitoring_enabled ${farmnode}
				case $? in
				1)	state="Enabled" ;;
				0)	state="Disabled" ;;
				*)	state=
					((found -= 1))
					continue
					;;
				esac
			else
				state="<DEBUG>"
			fi
			((found += 1))
			printf "%-25s %-16.16s\n" "${farmnode}" "${state}"
		done > ${tmpfile}

		# Are there any farmnodes?
		if [[ ${found} -lt 1 ]]; then
			echo
			printf "    $(gettext 'There are no farmnode configured.')\n\n\a"
			sc_prompt_pause
			rm -f ${tmpfile}
			return 1
		else
			# Add all to the tmpfile.
			echo "All" >> ${tmpfile}
		fi

		# Get the name of the farmnode to change
		farmnodenames=
		while true
		do
			fnodes="$(
		    		sc_get_scrolling_menuoptions	\
				"${prompt}"			\
				"${header1}"			\
				"${header2}"			\
				0 0 0				\
				:${tmpfile}			\
			)"
			rm -f ${tmpfile}

			if [[ -z ${fnodes} ]]; then
				return 1
			fi
			# Add answers to nodelist
			for fnode in ${fnodes}
			do
				if [[ ${fnode} == "All" ]]; then
					farmnodenames=
					break
				else
					farmnodenames="${farmnodenames} ${fnode}"
				fi
			done

			# verify that the list is correct
			echo
			sc_print_para "$(gettext 'This is the complete list of farm nodes you selected:')"

			if [[ -z ${farmnodenames} ]]; then
				printf "\t All.\n"
			else
				for foo in ${farmnodenames}
				do
					printf "\t${foo}\n"
				done
			fi
			echo
			answer=$(sc_prompt_yesno "$(gettext 'Is it correct?')" "${YES}") || return 1
			if [[ "${answer}" == "yes" ]]; then
				break
			else
				farmnodenames=
				continue
			fi
		done
		farmnodelist="${farmnodenames}"

		# Get the membership state change.
		prompt="$(gettext 'Select the monitoring state:')"
		statetype="$(
	    		sc_get_scrolling_menuoptions	\
			"${prompt}"			\
			"" ""				\
			1 1 1				\
			"Enable"			\
			"Disable"			\
		)"
		if [[ -z "${statetype}" ]]; then
			return 1
		fi

		# Display all if the nodelist is empty.
		if [[ -z ${farmnodenames} ]]; then
			farmnodenames="All"
		fi

		if [[ "${statetype}" == "Enable" ]]; then
			sc_print_para "${sctxt_disable_warning_p1}"
			prompt2="$(printf "$(gettext 'Do you want to enable monitoring state for \"%s\" nodes?')" "${farmnodenames}")"
			answer=$(sc_prompt_yesno "${prompt2}" "${YES}") || continue
			if [[ "${answer}" == "yes" ]]; then
				# Initialize
				set -A CLSETUP_DO_CMDSET
				# Add the command
				if [[ -z ${farmnodelist} ]]; then
					CLSETUP_DO_CMDSET[0]="${CL_CMD_CLNODE} monitor +"
				else
					CLSETUP_DO_CMDSET[0]="${CL_CMD_CLNODE} monitor ${farmnodelist}"
				fi
				# Attempt to issue the command
				clsetup_do_cmdset || return 1
			fi
		fi
		if [[ "${statetype}" == "Disable" ]]; then
			prompt2="$(printf "$(gettext 'Do you want to disable monitoring state for \"%s\" nodes?')" "${farmnodenames}")"
			answer=$(sc_prompt_yesno "${prompt2}" "${YES}") || continue
			if [[ "${answer}" == "yes" ]]; then
				# Initialize
				set -A CLSETUP_DO_CMDSET
				# Add the command
				if [[ -z ${farmnodelist} ]]; then
					CLSETUP_DO_CMDSET[0]="${CL_CMD_CLNODE} unmonitor +"
				else
					CLSETUP_DO_CMDSET[0]="${CL_CMD_CLNODE} unmonitor ${farmnodelist}"
				fi
				# Attempt to issue the command
				clsetup_do_cmdset || return 1
			fi
		fi

		# Set the Farm node properties.
		clsetup_set_farmnode_props
		prompt2="$(gettext 'Do you want to change monitoring state for other node(s) ?')"

		another=$(sc_prompt_yesno "${prompt2}" "${YES}") || return 1
		if [[ ${another} == "no" ]]; then
			break
		fi
	done

	return 0
}

####################################################
#
# clsetup_farm_node_status()
#
#	Displays the Farm node status.
#
#####################################################
clsetup_farm_node_status()
{
	typeset tmpfile=${CLSETUP_TMP_CMDOUT}

	echo
	if [[ ${CLSETUP_ISMEMBER} -eq 1 ]]; then
		rm -f ${tmpfile}
	  	# Display the farm node status.
		${CL_CMD_CLNODE} list -t farm 2>/dev/null 1>${tmpfile}
		if [[ -s "${tmpfile}" ]]; then
			rm -f ${tmpfile}
			${CL_CMD_CLNODE} status -t farm >${tmpfile}
			more ${tmpfile}
		else
			printf "$(gettext 'There are no farmnodes configured.')\n\n\a"
		fi
		rm -f ${tmpfile}
	else
		echo "DEBUG:  Not a cluster member - no status."
	fi
	sc_prompt_pause

	return 0
}

#####################################################
#
# clsetup_menu_farm_node()
#
#	Farm Node Menu. This includes Farm node
#	management.
#
#	This function always returns zero.
#####################################################
clsetup_menu_farm_node()
{
	# Loop around the Europa menu.
	while true
	do
		# Do it
		case $(clsetup_get_farm_node_menu_option) in
		'1')	clsetup_menu_europa_add_farm_node ;;
		'2')	clsetup_menu_europa_remove_farm_node ;;
		'3')	clsetup_menu_europa_change_farm_monitoring;;
		's')	clsetup_farm_node_status ;;
		'q')	break ;;
                esac
	done

	return 0
}

#####################################################
#
# clsetup_menu_farm_rg()
#
#####################################################
clsetup_menu_farm_rg()
{
	is_farm_rg=1
	clsetup_menu_rgm
	is_farm_rg=0
}

#####################################################
#
# clsetup_help_europa()
#
#####################################################
clsetup_help_europa()
{
	typeset sctxt_title="$(gettext '
		*** Help Screen - Farm Management ***
	')"
	typeset sctxt_p1="$(gettext '
	The Farm is a collection of interconnected servers.
	Farm nodes can be heterogeneous. The Server manages
	the Farm. Cluster management actions can be launched
	only from the Server. A Farm node can not takeover the
	role of a Server node.
	')"

	clear
	sc_print_title "${sctxt_title}"
	sc_print_para "${sctxt_p1}"
}

#####################################################
#
# clsetup_get_europa_menu_option()
#
#	Print the europa menu opton and returns the
#	selected menu option.
#
#	This function always returns zero.
#
#####################################################
clsetup_get_europa_menu_option()
{
	typeset sctxt_title_1="$(gettext '*** Farm management menu ***')"
	typeset sctxt_title_2="$(gettext 'Please select from one of the following options:')"
	typeset sctxt_option_001="$(gettext 'Farm nodes management')"
	typeset sctxt_option_002="$(gettext 'Farm resource groups management')"
	typeset sctxt_option_return="$(gettext 'Return to the main menu')"

	option=$(sc_get_menuoption \
		"T1+++${sctxt_title_1}" \
		"T2+++${sctxt_title_2}" \
		"S+0+1+${sctxt_option_001}" \
		"S+0+2+${sctxt_option_002}" \
		"R+++" \
		"S+0+q+${sctxt_option_return}" \
	)

	echo "${option}"

	return 0
}

####################################################
#
# clsetup_menu_farm_management()
#
#	Farm management menu. This includes Farm node and
#	Farm node RG management.
#
#	This function always returns zero.
#####################################################
clsetup_menu_farm_management()
{
	clsetup_set_farmnode_props
	# Loop around the farm management menu.
	while true
	do
		# Do it
		case $(clsetup_get_europa_menu_option) in
		'1')	clsetup_menu_farm_node ;;
		'2')	clsetup_menu_farm_rg ;;
		'q')	break ;;
                esac
	done
	return 0
}

#####################################################
#####################################################
##
## Main menu
##
#####################################################
#####################################################

#####################################################
#
# clsetup_get_main_menuoption() [help]
#
#	help	- if given, print the main help menu
#
#	Print the main menu, and return the selected option.
#	If the "help" option is given, the menu is processed
#	as the main help menu.
#
#	This function always returns zero.
#
#####################################################
clsetup_get_main_menuoption()
{
	typeset help=${1}

	typeset sctxt_title_1_main="$(gettext '*** Main Menu ***')"
	typeset sctxt_title_2_main="$(gettext 'Please select from one of the following options:')"
	typeset sctxt_title_1_help="$(gettext '*** Main Menu Help ***')"
	typeset sctxt_title_2_help="$(gettext 'Please select from one of the following HELP options:')"
	typeset sctxt_option_001="$(gettext 'Quorum')"
	typeset sctxt_option_002="$(gettext 'Resource groups')"
	typeset sctxt_option_003="$(gettext 'Data Services')"
	typeset sctxt_option_004="$(gettext 'Cluster interconnect')"
	typeset sctxt_option_005="$(gettext 'Device groups and volumes')"
	typeset sctxt_option_006="$(gettext 'Private hostnames')"
	typeset sctxt_option_007="$(gettext 'New nodes')"
	typeset sctxt_option_008="$(gettext 'Other cluster tasks')"
	typeset sctxt_option_009="$(gettext 'Farm management')"
	typeset sctxt_option_help="$(gettext 'Help with menu options')"
	typeset sctxt_option_exit="$(gettext 'Quit')"
	typeset sctxt_option_return="$(gettext 'Return to the Main Menu')"

	typeset option
	typeset europa_option=

	if [[ ${CLSETUP_IS_EUROPA} -eq 1 ]]; then
		europa_option="S+0+9+${sctxt_option_009}"
	fi

	if [[ -n "${help}" ]]; then
		option=$(sc_get_menuoption \
			"T1+++${sctxt_title_1_help}" \
			"T2+++${sctxt_title_2_help}" \
			"S+0+1+${sctxt_option_001}" \
			"S+0+2+${sctxt_option_002}" \
			"S+0+3+${sctxt_option_003}" \
			"S+0+4+${sctxt_option_004}" \
			"S+0+5+${sctxt_option_005}" \
			"S+0+6+${sctxt_option_006}" \
			"S+0+7+${sctxt_option_007}" \
			"S+0+8+${sctxt_option_008}" \
			"${europa_option}" \
			"R+++" \
			"S+0+q+${sctxt_option_return}" \
		)
	else
		option=$(sc_get_menuoption \
			"T1+++${sctxt_title_1_main}" \
			"T2+++${sctxt_title_2_main}" \
			"S+0+1+${sctxt_option_001}" \
			"S+0+2+${sctxt_option_002}" \
			"S+0+3+${sctxt_option_003}" \
			"S+0+4+${sctxt_option_004}" \
			"S+0+5+${sctxt_option_005}" \
			"S+0+6+${sctxt_option_006}" \
			"S+0+7+${sctxt_option_007}" \
			"S+0+8+${sctxt_option_008}" \
			"${europa_option}" \
			"R+++" \
			"S+0+?+${sctxt_option_help}" \
			"S+0+q+${sctxt_option_exit}" \
		)
	fi

	echo "${option}"

	return 0
}

#####################################################
#
# clsetup_get_main_menuoption_noncluster_mode() [help]
#
#	help	- if given, print the main help menu
#
#	Print the main menu, and return the selected option.
#	If the "help" option is given, the menu is processed
#	as the main help menu.
#
#	This function always returns zero.
#
#####################################################
clsetup_get_main_menuoption_noncluster_mode()
{
	typeset help=${1}

	typeset sctxt_title_1_main="$(gettext '*** Main Menu ***')"
	typeset sctxt_title_2_main="$(gettext 'Select from one of the following options:')"
	typeset sctxt_title_1_help="$(gettext '*** Main Menu Help ***')"
	typeset sctxt_title_2_help="$(gettext 'Select from one of the following HELP options:')"
	typeset sctxt_option_001_help="$(gettext 'Network Addressing and Ranges for the Cluster Transport')"
	typeset sctxt_option_001="$(gettext 'Change Network Addressing and Ranges for the Cluster Transport')"
	typeset sctxt_option_002="$(gettext 'Show Network Addressing and Ranges for the Cluster Transport')"
	typeset sctxt_option_help="$(gettext 'Help with menu options')"
	typeset sctxt_option_exit="$(gettext 'Quit')"
	typeset sctxt_option_return="$(gettext 'Return to the Main Menu')"

	typeset option

	if [[ -n "${help}" ]]; then
		option=$(sc_get_menuoption \
			"T1+++${sctxt_title_1_help}" \
			"T2+++${sctxt_title_2_help}" \
			"S+0+1+${sctxt_option_001_help}" \
			"R+++" \
			"S+0+q+${sctxt_option_return}" \
		)
	else
		option=$(sc_get_menuoption \
			"T1+++${sctxt_title_1_main}" \
			"T2+++${sctxt_title_2_main}" \
			"S+0+1+${sctxt_option_001}" \
			"S+0+2+${sctxt_option_002}" \
			"R+++" \
			"S+0+?+${sctxt_option_help}" \
			"S+0+q+${sctxt_option_exit}" \
		)
	fi

	echo "${option}"

	return 0
}


#####################################################
#
# clsetup_help_noncluster_mode_menu
#
#	Print help information for the main menu.
#
#####################################################
clsetup_help_noncluster_mode_menu()
{
	while true
	do
		case $(clsetup_get_main_menuoption_noncluster_mode help) in
		'1')	clsetup_help_noncluster_mode_ip_address_range ;;
		'q')	break ;;
		esac

		echo
		sc_print_prompt "$(gettext 'Type ENTER to return to the previous menu:')" || return 1
		read
		echo
	done

	return 0
}

#####################################################
#
# clsetup_help_main_menu
#
#	Print help information for the main menu.
#
#####################################################
clsetup_help_main_menu()
{
	while true
	do
		case $(clsetup_get_main_menuoption help) in
		'1')	clsetup_help_main_quorum ;;
		'2')	clsetup_help_main_rgm ;;
		'3')	clsetup_help_main_dataservices ;;
		'4')	clsetup_help_main_transport ;;
		'5')	clsetup_help_main_devicegroups ;;
		'6')	clsetup_help_main_privatehosts ;;
		'7')	clsetup_help_main_auth ;;
		'8')	clsetup_help_main_other ;;
		'9')	clsetup_help_europa ;;
		'q')	break ;;
		esac

		echo
		sc_print_prompt "$(gettext 'Type ENTER to return to the previous menu:')" || return 1
		read
		echo
	done

	return 0
}

#####################################################
#
# clsetup_menu()
#
#	Menu driven clsetup.
#
#	This function always returns zero.
#
#####################################################
clsetup_menu()
{
	# Set the default RG's as cluster rg's.
	set is_farm_rg=0

	# Initialise farm node information in europa case
	if [[ ${CLSETUP_IS_EUROPA} -eq 1 ]]; then
		clsetup_set_farmnode_props
	fi

	# Loop around the main menu
	while true
	do
		case $(clsetup_get_main_menuoption) in
		'1')    clsetup_menu_quorum ;;
		'2')	clsetup_menu_rgm ;;
		'3')	clsetup_menu_dataservices ;;
		'4')    clsetup_menu_transport ;;
		'5')    clsetup_menu_devicegroups ;;
		'6')    clsetup_menu_privatehosts ;;
		'7')    clsetup_menu_auth ;;
		'8')    clsetup_menu_other ;;
		'9')	clsetup_menu_farm_management ;;
		'?')    clsetup_help_main_menu ;;
		'q')    break ;;
                esac
	done

	return 0
}

#####################################################
#
# clsetup_menu_noncluster_mode()
#
#	Menu driven clsetup for noncluster mode.
#
#####################################################
clsetup_menu_noncluster_mode()
{
	# Loop around the main menu
	while true
	do
		case $(clsetup_get_main_menuoption_noncluster_mode) in
		'1')    clsetup_menu_ip_address_range ;;
		'2')    clsetup_menu_print_ip_address_range ;;
		'?')    clsetup_help_noncluster_mode_menu ;;
		'q')    break ;;
                esac
	done

	return 0
}

#####################################################
#####################################################
##
## Initialize cluster
##
#####################################################
#####################################################

#####################################################
#
# clsetup_initcluster()
#
#	The purpose of this function is to provide an interactive
#	dialog to the user at initial install time for setting up
#	quorum, resetting installmode, etc.
#
#	This function should only be called if installmode is enabled.
#
#	Return:
#		zero 		Success
#		non-zero	Failure
#
#####################################################
clsetup_initcluster()
{
	typeset sctxt_title="$(gettext '
		>>> Initial Cluster Setup <<<
	')"
	typeset sctxt_p1="$(gettext '
		This program has detected that the cluster \"installmode\"
		attribute is still enabled.  As such, certain initial
		cluster setup steps will be performed at this time.
		This includes adding any necessary quorum devices,
		then resetting both the quorum vote counts and the
		\"installmode\" property.
	')"
	typeset sctxt_p2="$(gettext '
		Please do not proceed if any additional nodes have yet
		to join the cluster.
	')"
	typeset sctxt_p1_qdev="$(gettext '
		Following are supported Quorum Devices types in Sun Cluster.
		Please refer to Sun Cluster documentation for detailed
		information on these supported quorum device topologies.
	')"
	typeset sctxt_p1_reset="$(gettext '
		Once the \"installmode\" property has been reset,
		this program will skip \"Initial Cluster Setup\"
		each time it is run again in the future.  However,
		quorum devices can always be added to the cluster
		using the regular menu options.  Resetting this
		property fully activates quorum settings and is
		necessary for the normal and safe operation of
		the cluster.
	')"
	typeset sctxt_reset_cmd1="${CL_CMD_CLQUORUM} reset"
	typeset sctxt_reset_cmd2="${CL_CMD_CLACCESS} deny-all"

	integer nodecount
	integer qdevcount
	integer status
	typeset nodelist
	typeset qdevlist
	typeset answer

	#
	# Print help
	#
	clear
	sc_print_title "${sctxt_title}"
	sc_print_para "${sctxt_p1}"
	sc_print_para "${sctxt_p2}"

	# Okay to continue?
	clsetup_continue || return 1

	# Loop until done or Ctrl-D is typed
	while true
	do
		# Initialize
		let nodecount=0
		let qdevcount=0
		nodelist=
		qdevlist=

		# Get nodelist and nodecount
		if [[ ${CLSETUP_ISMEMBER} -eq 1 ]]; then

			# Get nodelist
			nodelist="$(clsetup_get_nodelist)"

			# Make sure we have a nodelist
			if [[ -z "${nodelist}" ]]; then
				printf "$(gettext 'Unable to establish the list of cluster nodes.')\n\n\a"
				sc_prompt_pause
				return 1
			fi

			# Set node count
			let nodecount=$(set -- ${nodelist}; echo $#)
		else
			# We must be running in DEBUG mode
			while true
			do
				answer=$(sc_prompt "$(gettext 'DEBUG:  How  many nodes are in the cluster?')") || return 1

				# In range?
				if [[ $(expr ${answer} : '[0-9]*') -ne $(expr ${answer} : '.*') ]] ||
				    [[ ${answer} -lt 1 ]] ||
				    [[ ${answer} -gt ${SC_MAXNODEID} ]]; then
					printf "\a"
				else
					break
				fi
			done

			# Set nodecount
			let nodecount=${answer}
		fi

		# Make sure that we have at least two nodes
		if [[ ${nodecount} -lt 2 ]]; then
			printf "$(gettext 'The cluster must have at least two nodes before running %s.')\n\n\a" "${PROG}"
			return 1
		fi

		# Add any needed quorum devices
		while true
		do
			# Do you want to add any quorum devices?
			answer=$(sc_prompt_yesno "$(gettext 'Do you want to add any quorum devices?')" "${YES}") || return 1

			# Okay, add device
			if [[ "${answer}" = "yes" ]]; then

				# Print help
				sc_print_para "${sctxt_p1_qdev}"

				# Add it
				clsetup_menu_quorum_adddev "nohelp"
				if [[ $? -ne 0 ]]; then
					answer=$(sc_prompt_yesno "$(gettext 'Do you want to try again?')") || return 1
					if [[ "${answer}" = "no" ]]; then
						break
					else
						continue
					fi
				fi
			fi

			#
			# Get qdevlist and qdevcount
			# Note that if we are not a cluster member,
			# we must be in DEBUG mode.  In DEBUG, the qdevcount
			# was alredy adjusted for us on successful return
			# from clsetup_menu_quorum_adddev().
			#
			if [[ ${CLSETUP_ISMEMBER} -eq 1 ]]; then

				# Get qdevlist
				qdevlist="$(clsetup_get_qdevlist)"

				# Set qdevcount count
				let qdevcount=$(set -- ${qdevlist}; echo $#)
			fi

			# All two node clusters must have at least one qdev
			if [[ ${nodecount} -eq 2 ]] &&
			    [[ ${qdevcount} -lt 1 ]]; then
				printf "$(gettext 'All two-node clusters must have at least one quorum device.')\n\n\a"
				return 1
			fi

			# Done
			break
		done

		# Reset installmode
		sc_print_para "${sctxt_p1_reset}"
		answer=$(sc_prompt_yesno "$(gettext 'Is it okay to reset \"installmode\"?')" "${YES}") || return 1
		if [[ "${answer}" = "yes" ]] &&
		    [[ ${CLSETUP_ISMEMBER} -eq 1 ]]; then

			# Run the commands
			echo
			echo "${sctxt_reset_cmd1}"
			${sctxt_reset_cmd1} 2>&1
			status=$?
			if [[ ${status} -eq 0 ]]; then
				echo "${sctxt_reset_cmd2}"
				${sctxt_reset_cmd2} 2>&1
				status=$?
			fi

			# Failed
			if [[ ${status} -ne 0 ]]; then
				echo
				printf "$(gettext 'Command failed.')\n\n\a"
				return 1
			fi

			# Log it
			if [[ -n "${CLSETUP_CMD_LOG}" ]]; then
				echo "${sctxt_reset_cmd1}" >>${CLSETUP_CMD_LOG}
				echo "${sctxt_reset_cmd2}" >>${CLSETUP_CMD_LOG}
			fi

			# Success message
			echo
			sc_print_para "$(gettext 'Cluster initialization is complete.')"

			# Pause
			echo
			sc_print_prompt "$(gettext 'Type ENTER to proceed to the main menu:')" || return 1
			read
			echo

		# If no reset to installmode, return error
		elif [[ "${answer}" = "no" ]]; then
			return 1
		fi

		# Done
		break
	done

	return 0
}

#####################################################
#####################################################
##
## Main
##
#####################################################
#####################################################
main()
{
	typeset c
	integer isinstallmode=0
	integer result
	typeset argv
	typeset argv_list
	integer has_err
	typeset opt_argvs
	typeset opt_argv
	integer has_ver
	integer has_file
	integer has_help
	integer flip_mark
	integer is_operand
	integer index

	# load common functions
	loadlib ${SC_SCADMINLIBDIR}/${SC_LIB_SC} ${SC_LOADED_COMMON} || return 1
	let SC_LOADED_COMMON=1

	# Make sure that we are a member of the cluster
        if [[ -x /usr/sbin/clinfo ]]; then
                eval /usr/sbin/clinfo >/dev/null 2>&1
                if [[ $? -eq 0 ]]; then
			let CLSETUP_ISMEMBER=1
		fi
	fi

	# Make sure that the cluster is europa enabled.
	# Check for scxcfg routine and use the 'G' option
	# to find out whether the cluster is Europa enabled or not.
	if [[ -f /usr/cluster/lib/sc/scxcfg ]]; then
		# Call the scxcfg routine if europa is present.
		/usr/cluster/lib/sc/scxcfg -G || return 1
		# Returns 0 if the cluster is europa enabled.
		# Returns a non-zero value if the cluster is not a
		# europa enabled cluster.
		if [[ $? -eq 0 ]]; then
			let CLSETUP_IS_EUROPA=1
		fi
	fi

	# Europa related tasks can also be enabled using SC_DEBUG=3
	if [[ "${SC_DEBUG}" = "3" ]]; then
		let CLSETUP_IS_EUROPA=1
	fi

	# We must be root to use clsetup in cluster or noncluster mode
	verify_isroot || return 1

	# If OS>=S10, set ZONES_OK to 1. Zones support starts from S10.
        if [[ ${SC_SUNOS_MAJ} -gt 5 ]] ||
                [[ ${SC_SUNOS_MAJ} -eq 5 && ${SC_SUNOS_MIN} -ge 10 ]]; then
                let ZONES_OK=1
        fi
        
        # If installmode is enabled, we do not want to do menu driven interface
	let isinstallmode=0
	if [[ ${CLSETUP_ISMEMBER} -eq 1 ]]; then
		let isinstallmode=$(clsetup_get_installmode)
	fi

	# If it is IPS package, check if need to display the SLM and dsconfig menu.
	sc_is_ips
	if [[ $? -eq 0 ]]; then
		IS_IPS=1
		# Do we have Telemtry packages installed?
		/bin/pkg list -s ${SC_SLM_IPSPKG} >/dev/null 2>&1
		if [ $? -ne 0 ]; then
			# Don't disply the menu to configure telemetry
			let LIST_TELEMETRY=0
		fi

		# Is the dsconfig package installed?
		/bin/pkg list -s ${SC_DSCONFIG_IPSPKG} >/dev/null 2>&1
		if [ $? -ne 0 ]; then
			# Don't disply the dsconfig menu
			let LIST_DSCONFIG=0
		fi
	fi

	# Arrange for cleanup on SIGHUP or SIGINT
	trap 'cleanup 10' HUP INT

	# Create clsetup tmpdir, if it does not already exist
	if [[ ! -d "${CLSETUP_TMPDIR}" ]] &&
	    [[ -d "$(dirname ${CLSETUP_TMPDIR})" ]]; then
		mkdir -m 0755 ${CLSETUP_TMPDIR}
		chgrp sys ${CLSETUP_TMPDIR}
	fi

	# If clsetup is just being used as a "library", we are done
	if [[ -n "${CLSETUP_LIBRARY}" ]]; then
		return 0
	fi

	# Get command line options, if there are any.
	if [[ $# -gt 0 ]]; then
		let has_err=0
		let has_ver=0
		let has_file=0
		let has_help=0
		let flip_mark=0
		let is_operand=0
		let index=0

		# Remove the "=" sign if any
		argv_list=$(echo $* |sed 's/=/ /g')

		# Go through the parameters. Check for errors.
		for argv in ${argv_list}
		do
			# Update index
			((index += 1))

			# Check the parameter
			if [[ "${argv}" != -* ]] &&
			    [[ "${argv}" != --* ]]; then
				if [[ ${has_file} -eq 1 ]] && [[ ${flip_mark} -eq 1 ]]; then
					CLSETUP_CMD_LOG=${argv}
					let flip_mark=0
				elif [[ ${index} -eq 1 ]]; then
					printf "$(gettext '%s:  Unrecognized subcommand - \"%s\".')\n" "${PROG}" ${argv} >&2
					let has_err=1
				else
					printf "$(gettext '%s:  Unexpected operand - \"%s\".')\n" "${PROG}" ${argv} >&2
					let is_operand=1
					let has_err=1
				fi
			else
				if [[ ${is_operand} -eq 1 ]]; then
					printf "$(gettext '%s:  Unexpected operand - \"%s\".')\n" "${PROG}" ${argv} >&2
					let has_err=1
				else
					if [[ "${argv}" == "-V" ]] ||
					    [[ "${argv}" == "--version" ]]; then
						let has_ver=1
					elif [[ "${argv}" == "-?" ]] ||
					    [[ "${argv}" == "--help" ]]; then
						let has_help=1
					elif [[ "${argv}" == "-f" ]] ||
					    [[ "${argv}" == "--file" ]]; then
						let has_file=1
						let flip_mark=1
					elif [[ "${argv}" == --* ]]; then
						# Bad long options
						let has_err=1
						printf "$(gettext '%s:  Unrecognized command option - \"%s\".')\n" "${PROG}" ${argv} >&2
					else
						let has_err=1
						opt_argvs=$(echo ${argv} | sed 's/./& /g')
						for opt_argv in ${opt_argvs}
						do
							if [[ "${opt_argv}" != "-" ]]; then
								printf "$(gettext '%s:  Unrecognized command option - \"-%s\".')\n" "${PROG}" "${opt_argv}" >&2
							fi
						done
					fi
				fi
			fi
		done

		# Still usage errors?
		if [[ ${has_help} -eq 1 ]]; then
			if [[ ${has_ver} -eq 1 ]]; then
				let has_err=1
				printf "$(gettext '%s:  The \"-V (--version)\" and \"-? (--help)\" options cannot be used together.')\n" "${PROG}" >&2
			fi
			if [[ ${has_file} -eq 1 ]]; then
				let has_err=1
				printf "$(gettext '%s:  The \"-f (--file)\" and \"-? (--help)\" options cannot be used together.')\n" "${PROG}" >&2
			fi
		elif [[ ${has_ver} -eq 1 ]] && [[ ${has_file} -eq 1 ]]; then
			let has_err=1
			printf "$(gettext '%s:  The \"-V (--version)\" and \"-f (--file)\" options cannot be used together.')\n" "${PROG}" >&2
		fi

		if [[ ${has_err} -eq 1 ]]; then
			printf "$(gettext '%s:  Usage error.')\n\n" "${PROG}" >&2
			print_usage
			return 1
		fi

		# Process the option
		if [[ ${has_help} -eq 1 ]]; then
			print_usage
			return 0
		fi
		if [[ ${has_ver} -eq 1 ]]; then
			printf "${PROG} ${SC_CMD_VERSION}\n"
			return 0
		fi
		
		# Log file
		if [[ ${has_file} -eq 1 ]]; then
			# Make sure the log file is not already set
			if [[ -z "${CLSETUP_CMD_LOG}" ]]; then
				printf "$(gettext '%s:  Option \"-f (--file)\" requires an argument.')\n" "${PROG}" >&2
				printf "$(gettext '%s:  Usage error.')\n\n" "${PROG}" >&2
				print_usage
				return 1
			fi

			# Add heading for this session to the log file
			if [[ -s "${CLSETUP_CMD_LOG}" ]]; then
				printf "\n" >>${CLSETUP_CMD_LOG} 2>/dev/null
			fi
			printf "#\n" >>${CLSETUP_CMD_LOG} 2>/dev/null
			if [[ $? -ne 0 ]]; then
				printf "$(gettext '%s:  Unable to write to the command log.')\n" "${PROG}" >&2
				return 1
			fi
			printf "# %s - %s\n" "${PROG}" "$(date)" >>${CLSETUP_CMD_LOG}
			printf "#\n" >>${CLSETUP_CMD_LOG}
		fi
	fi

	# Set the default RG's as cluster rg's.
	set is_farm_rg=0

	# Initialise farm node information in europa case
	if [[ ${CLSETUP_IS_EUROPA} -eq 1 && ${CLSETUP_ISMEMBER} -eq 1 ]]; then
		clsetup_set_farmnode_props
	fi

	# If isinstallmode, initialize cluster before proceeding to menu
	if [[ ${isinstallmode} -eq 1 ]] || [[ "${SC_DEBUG}" = "2" ]]; then
		# Init cluster
		clsetup_initcluster || return 1

		# Make sure isinstallmode was cleared
		let isinstallmode=0
		if [[ ${CLSETUP_ISMEMBER} -eq 1 ]]; then
			let isinstallmode=$(clsetup_get_installmode)
		fi

		# If isinstallmode is not set, do not proceed to menu
		if [[ ${isinstallmode} -eq 1 ]]; then
			return 0
		fi
	fi

	if [[ ${CLSETUP_ISMEMBER} -ne 1 ]]; then
		clsetup_menu_noncluster_mode
	else
		# Go to the menu
		clsetup_menu
	fi

	# Cleanup files
	cleanup

	# Return
	return 0
}

#####################################################
#
# clsetup_help_main_dataservices()
#
#	Print the main help message for Data services.
#
#	This function always returns zero.
#
#####################################################
clsetup_help_main_dataservices()
{
	typeset sctxt_title="$(gettext '
		*** Help Screen - Data services ***
	')"
	typeset sctxt_p1="$(gettext '
		Data services on a Sun Cluster are applications, such as Oracle
		RAC, that have been configured to run on a cluster rather than
		on a single server. A data service includes the application
		software and the supporting Sun Cluster software that starts,
		stops and monitors the application.
	')"

	sc_print_title "${sctxt_title}"
	sc_print_para "${sctxt_p1}"

	return 0
}

#####################################################
#
# clsetup_menu_dataservices()
#
#	Data services menu
#
#####################################################
clsetup_menu_dataservices()
{
	typeset errmsg=$(lib_test_java_version)
	integer status=$?
	if [[ $status -ne 0 ]]; then
		sc_print_para "${errmsg}"
                sc_prompt_pause
                return 1
	fi

	# Issue message if (IPS) dsconfig package not installed
	if [[ ${LIST_DSCONFIG} -eq 0 ]]; then
		clear
		sc_print_para "$(printf "$(gettext 'To use the data service configuration wizard, you must install the IPS package \"%s\".')" "${SC_DSCONFIG_IPSPKG}")"
                sc_prompt_pause
                return 1
	fi

	# If IPS packages in use then check for any DS registration changes
	if [[ ${IS_IPS} -eq 1 ]]; then
	    clsetup_check_dataservice_registrations
	fi

	# Loop around the dataservices menu
	while true
	do
		case $(clsetup_get_dataservices_menuoption) in
		'1')    clsetup_menu_dataservices_apache ;;
		'2')	clsetup_menu_dataservices_haoracle ;;
		'3')	clsetup_menu_dataservices_nfs ;;
		'4')    clsetup_menu_dataservices_oraclerac ;;
		'5')	clsetup_menu_dataservices_sapwebas ;;
		'6')	clsetup_menu_dataservices_hastorageplus ;;
		'7')	clsetup_menu_dataservices_logicalhost ;;
		'8')	clsetup_menu_dataservices_sharedaddress ;;
		'?')    clsetup_help_dataservices_menu ;;
		'q')    break ;;
                esac
	done

	return 0
}

#####################################################
#
# clsetup_check_agent()
#
#	Checks if Cacao agent is running on all the
#       nodes of the cluster for use by Data services
#	configuration wizards
#
#####################################################
clsetup_check_agent()
{
	typeset nodes=$(clsetup_get_nodelist)
	typeset offline=""
	typeset curnode=""
	sc_print_para "$(gettext 'Checking for the cluster nodes that can be used for configuration. This may take some time...')"
	for node in ${nodes};
	do
		$JAVA -Djavax.net.ssl.trustStore=${ROOT_DIR}/etc/cacao/instances/default/security/jsse/truststore -Djavax.net.ssl.keyStore=${ROOT_DIR}/etc/cacao/instances/default/security/jsse/keystore -Djavax.net.ssl.keyStorePassword=password -classpath ${CLASSPATH} com.sun.cluster.dswizards.common.CheckAgent $node >/dev/null 2>&1
		typeset status=$?
		if [[ $status -ne 0 ]]; then
			if [[ "${node}" == "${MYNAME}" ]]; then
				curnode="${node}"
			fi
			if [[ -z "${offline}" ]]; then
				offline="${node}"
			else
				offline="${offline},${node}"
			fi
		fi
	done
	if [[ -n "${offline}" ]]; then
		sc_print_para "$(printf "$(gettext 'The following nodes cannot be used for configuration, \"%s\". Check the status of the Cacao Agent on these nodes.')\n" "${offline}")"
		if [[ -n "${curnode}" ]]; then
			sc_print_para "$(gettext 'Try data services configuration from a different cluster node.')"
			sc_prompt_pause
			return 1
		fi
		sc_prompt_pause
	fi
	return 0
}

#####################################################
#
# clsetup_help_dataservices_menu()
#
#	Print help information from the Data services menu.
#	This is the same help information as is printed
#	from the MAIN help menu.  But, we ask for confirmation
#	before returning to the previous menu.
#
#	This function always returns zero.
#
#####################################################
clsetup_help_dataservices_menu()
{
	clear
	clsetup_help_main_dataservices
	sc_prompt_pause

	return 0
}

#####################################################
#
# clsetup_get_dataservices_menuoption()
#
#	Print the Data services menu, and return the selected option.
#
#	This function always returns zero.
#
#####################################################
clsetup_get_dataservices_menuoption()
{
	typeset sctxt_title_1="$(gettext '*** Data Services Menu ***')"
	typeset sctxt_title_2="$(gettext 'Please select from one of the following options:')"
	typeset sctxt_option_001="$(gettext 'Apache Web Server')"
	typeset sctxt_option_002="$(gettext 'Oracle')"
	typeset sctxt_option_003="$(gettext 'NFS')"
	typeset sctxt_option_004="$(gettext 'Oracle Real Application Clusters')"
	typeset sctxt_option_005="$(gettext 'SAP Web Application Server')"
	typeset sctxt_option_006="$(gettext 'Highly Available Storage')"
	typeset sctxt_option_007="$(gettext 'Logical Hostname')"
	typeset sctxt_option_008="$(gettext 'Shared Address')"

	typeset sctxt_option_help="$(gettext 'Help')"
	typeset sctxt_option_return="$(gettext 'Return to the Main Menu')"

	# optional agents might not be installed
	# disable selection if missing
	typeset select_apache="S";     [[ -x /opt/SUNWscapc ]] || select_apache="N"
	typeset select_oracle="S";     [[ -x /opt/SUNWscor  ]] || select_oracle="N"
	    # both HA & RAC
	typeset select_nfs="S";        [[ -x /opt/SUNWscnfs ]] || select_nfs="N"
	typeset select_sap="S";        [[ -x /opt/SUNWscsap ]] || select_sap="N"

	typeset option

	option=$(sc_get_menuoption \
		"T1+++${sctxt_title_1}" \
		"T2+++${sctxt_title_2}" \
		"${select_apache}+1+1+${sctxt_option_001}" \
		"${select_oracle}+1+2+${sctxt_option_002}" \
		"${select_nfs}+1+3+${sctxt_option_003}" \
		"${select_oracle}+1+4+${sctxt_option_004}" \
		"${select_sap}+1+5+${sctxt_option_005}" \
		"S+1+6+${sctxt_option_006}" \
	        "S+1+7+${sctxt_option_007}" \
		"S+1+8+${sctxt_option_008}" \
		"R+++" \
		"S+1+?+${sctxt_option_help}" \
		"S+1+q+${sctxt_option_return}" \
	)

	echo "${option}"

	return 0
}

#####################################################
#
# clsetup_menu_dataservices_logicalhost()
#
#       Print the Logical Host Configuration menu.
#
#       This function always returns zero.
#
#####################################################
clsetup_menu_dataservices_logicalhost()
{
	$JAVA -Djava.awt.headless=true -classpath $CLASSPATH com.sun.cluster.dswizards.logicalhost.LogicalHostWizardCreator
	return 0
}

#####################################################
#
# clsetup_menu_dataservices_sharedaddress()
#
#       Print the Shared Address Configuration menu
#
#       This function always returns zero.
#
#####################################################
clsetup_menu_dataservices_sharedaddress()
{
	$JAVA -Djava.awt.headless=true -classpath $CLASSPATH com.sun.cluster.dswizards.sharedaddress.SharedAddressWizardCreator
	return 0
}

#####################################################
#
# clsetup_menu_dataservices_hastorageplus()
#
#       Print the HASP Configuration menu.
#
#       This function always returns zero.
#
#####################################################
clsetup_menu_dataservices_hastorageplus()
{
	$JAVA -Djava.awt.headless=true -classpath $CLASSPATH com.sun.cluster.dswizards.hastorageplus.HASPWizardCreator
	return 0
}

#####################################################
#
# clsetup_menu_dataservices_nfs()
#
#       Print the NFS Configuration menu.
#
#       This function always returns zero.
#
#####################################################
clsetup_menu_dataservices_nfs()
{
        $JAVA -Djava.awt.headless=true -classpath $CLASSPATH com.sun.cluster.dswizards.nfs.NFSWizardCreator
        return 0
}

#####################################################
#
# clsetup_menu_dataservices_apache()
#
#       Print the Apache Configuration menu
#
#       This function always returns zero.
#
#####################################################
clsetup_menu_dataservices_apache()
{
        $JAVA -Djava.awt.headless=true -classpath $CLASSPATH com.sun.cluster.dswizards.apache.ApacheWizardCreator
        return 0
}

#####################################################
#
# clsetup_menu_dataservices_haoracle()
#
#	Print the HAOracle Configuration menu.
#
#	This function always returns zero.
#
#####################################################
clsetup_menu_dataservices_haoracle()
{
 	$JAVA -Djava.awt.headless=true -classpath $CLASSPATH com.sun.cluster.dswizards.haoracle.HAOracleWizardCreator
	return 0
}

#####################################################
#
# clsetup_menu_dataservices_sapwebas()
#
#	Print the SAP Configuration menu.
#
#	This function always returns zero.
#
#####################################################
clsetup_menu_dataservices_sapwebas()
{
	$JAVA -Djava.awt.headless=true -classpath $CLASSPATH com.sun.cluster.dswizards.sapwebas.SapWebasWizardCreator
	return 0
}

#####################################################
#
# clsetup_menu_dataservices_oraclerac_wizard
#
#       Starts OracleRAC DSConfig Wizards
#
#       This function always returns zero.
#
#####################################################
clsetup_menu_dataservices_oraclerac_wizard()
{
	$JAVA -Djava.awt.headless=true -classpath $CLASSPATH com.sun.cluster.dswizards.oraclerac.invoker.RACInvokerCreator
	return 0
}

#####################################################
#
# clsetup_menu_dataservices_oraclerac_framework_wizard()
#
#       Starts OracleRAC DSConfig Wizards
#
#       This function always returns zero.
#
#####################################################
clsetup_menu_dataservices_oraclerac_framework_wizard()
{
        $JAVA -Djava.awt.headless=true -classpath $CLASSPATH com.sun.cluster.dswizards.oraclerac.framework.ORFrameworkInvoker
        return 0
}

#####################################################
#
# clsetup_menu_dataservices_oraclerac_storage_wizard()
#
#       Starts OracleRAC DSConfig Wizards
#
#       This function always returns zero.
#
#####################################################
clsetup_menu_dataservices_oraclerac_storage_wizard()
{
        $JAVA -Djava.awt.headless=true -classpath $CLASSPATH com.sun.cluster.dswizards.oraclerac.storage.ORStorageInvoker
        return 0
}

#####################################################
#
# clsetup_menu_dataservices_oraclerac_database_wizard()
#
#       Starts OracleRAC DSConfig Wizards
#
#       This function always returns zero.
#
#####################################################
clsetup_menu_dataservices_oraclerac_database_wizard()
{
        $JAVA -Djava.awt.headless=true -classpath $CLASSPATH com.sun.cluster.dswizards.oraclerac.database.RacDBInvoker
        return 0
}

#####################################################
#
# clsetup_get_dataservices_oraclerac_menuoption()
#
#	Print the Oracle RAC menu, and return the 
#	selected option.
#
#	This function always returns zero.
#
#####################################################
clsetup_get_dataservices_oraclerac_menuoption()
{
	typeset sctxt_prompt="$(gettext '
		Please select from one of the following options:
	')"
	typeset sctxt_option_001="$(gettext '
		Oracle RAC Create Configuration
	')"
	typeset sctxt_option_002="$(gettext '
		Oracle RAC Ongoing Administration
	')"
	typeset sctxt_option_return="$(gettext '
		Return to the Data Services Menu
	')"

	typeset option

		option=$(sc_get_menuoption \
			"T2+++${sctxt_prompt}" \
			"S+0+1+${sctxt_option_001}" \
			"S+0+2+${sctxt_option_002}" \
			"R+++" \
			"S+0+q+${sctxt_option_return}" \
		)

	echo "${option}"

	return 0
}

#####################################################
#
# clsetup_menu_dataservices_oraclerac_createconfig()
#
#	Print the Oracle RAC Create Configuration 
#	menu and calls the appropriate function
#	based on the selection.
#
#	This function always returns zero.
#
#####################################################
clsetup_menu_dataservices_oraclerac_createconfig()
{
	typeset sctxt_title="$(gettext '
		>>> Oracle RAC Create Configuration <<<
	')"
	typeset sctxt_p1="$(gettext '
		Only one RAC framework resource group is allowed on a cluster.
		You must configure the RAC framework resource group first.
		Subsequently you may configure either one or both of the
		Storage and Database resources.
	')"
	typeset sctxt_prompt="$(gettext '
		Please select from one of the following options:
	')"
	typeset sctxt_option_001="$(gettext '
		RAC Framework Resource Group
	')"
	typeset sctxt_option_002="$(gettext '
		Storage Resources for Oracle Files
	')"
	typeset sctxt_option_003="$(gettext '
		Oracle RAC Database Resources
	')"
	typeset sctxt_option_return="$(gettext '
		Return to the Sun Cluster Support for Oracle RAC Menu
	')"

	typeset option
	typeset rac_resource

	sc_print_title "${sctxt_title}"
	sc_print_para "${sctxt_p1}"

	# Loop around the Oracle RAC Create Configuration menu
	while true
	do
		option=$(sc_get_menuoption \
			"T2+++${sctxt_prompt}" \
			"S+0+1+${sctxt_option_001}" \
			"S+0+2+${sctxt_option_002}" \
			"S+0+3+${sctxt_option_003}" \
			"R+++" \
			"S+0+q+${sctxt_option_return}" \
		)

		#
		# Check for the existance of any resource of 
		# SUNW.rac_framework type
		#
		rac_resource=`${CL_CMD_CLRESOURCE} list \
		   -v | grep SUNW.rac_framework | wc -l`

		case ${option} in
		'1')
			if [[ ${rac_resource} -ne 0 ]]; then
				printf "$(gettext 'RAC framework resource
					group is already created.')\n"
				printf "$(gettext 'Only one RAC framework
					resource group is allowed on a
					cluster.')\n\n\a"
				sc_prompt_pause
				continue
			fi
			clsetup_menu_dataservices_oraclerac_framework_wizard
			;;
		'2')
			if [[ ${rac_resource} -eq 0 ]]; then
				printf "$(gettext 'RAC framework resource group
					is required but not yet created.')\n"
				printf "$(gettext 'Create RAC framework
					resource group first.')\n\n\a"
				sc_prompt_pause
				continue
			fi
			clsetup_menu_dataservices_oraclerac_storage_wizard
			;;
		'3')
			if [[ ${rac_resource} -eq 0 ]]; then
				printf "$(gettext 'RAC framework resource group
					is required but not yet created.')\n"
				printf "$(gettext 'Create RAC framework
					resource group first.')\n\n\a"
				sc_prompt_pause
				continue
			fi
			clsetup_menu_dataservices_oraclerac_database_wizard
			;;
		'q')    break ;;
                esac
	done

	return 0
}
	
#####################################################
#
# clsetup_menu_dataservices_oraclerac_admin()
#
#	Print the Oracle RAC ongoing administration
#	menu and calls the appropriate function
#	based on the selection.
#
#	This function always returns zero.
#
#####################################################
clsetup_menu_dataservices_oraclerac_admin()
{
	typeset sctxt_title="$(gettext '
		>>> Oracle RAC Ongoing Administration <<<
	')"
	typeset sctxt_prompt="$(gettext '
		Please select from one of the following options:
	')"
	typeset sctxt_option_001="$(gettext '
		Remove the RAC framework resource group
	')"
	typeset sctxt_option_002="$(gettext '
		Add nodes to the RAC framework resource group
	')"
	typeset sctxt_option_003="$(gettext '
		Remove nodes from the RAC framework resource group
	')"
	typeset sctxt_option_status="$(gettext '
		Show the status of RAC framework resource group
	')"
	typeset sctxt_option_return="$(gettext '
		Return to the Sun Cluster Support for Oracle RAC Menu
	')"

	typeset option

	sc_print_title "${sctxt_title}"

	# Loop around the Oracle RAC Ongoing Administration menu
	while true
	do
		option=$(sc_get_menuoption \
			"T2+++${sctxt_prompt}" \
			"S+0+1+${sctxt_option_001}" \
			"S+0+2+${sctxt_option_002}" \
			"S+0+3+${sctxt_option_003}" \
			"R+++" \
			"S+0+s+${sctxt_option_status}" \
			"S+0+q+${sctxt_option_return}" \
		)

		case ${option} in
		'1')    clsetup_menu_dataservices_oraclerac_removerg ;;
		'2')    clsetup_menu_dataservices_oraclerac_addnodes ;;
		'3')    clsetup_menu_dataservices_oraclerac_removenodes ;;
		's')    clsetup_menu_dataservices_oraclerac_status ;;
		'q')    break ;;
                esac
	done

	return 0
}

#####################################################
#
# clsetup_menu_dataservices_oraclerac()
#
#	Print the Oracle RAC Configuration menu,
#	and calls the appropriate function based
#	on the selection.
#
#	Return:
#		zero		Completed
#		non-zero	Not completed
#
#####################################################
clsetup_menu_dataservices_oraclerac()
{
	typeset sctxt_title="$(gettext '
		*** Sun Cluster Support for Oracle RAC ***
	')"
	typeset sctxt_p1="$(gettext '
		Sun Cluster provides a support layer for running Oracle Real
		Application Clusters \(RAC\) database instances. This option
		allows you to create the RAC framework resource group,
		storage resources, database resources and administer them,
		for managing the Sun Cluster support for RAC.
	')"
	typeset sctxt_p2="$(gettext '
		After the RAC framework resource group has been created,
		you can use the Sun Cluster system administration tools to
		administer a RAC framework resource group that is
		configured on a global cluster. To administer a RAC framework
		resource group that is configured on a zone cluster,
		instead use the appropriate Sun Cluster command.
	')"

	# Loop around the Sun Cluster Support for Oracle RAC menu
	while true
	do
		sc_print_title "${sctxt_title}"
		sc_print_para "${sctxt_p1}"
		sc_print_para "${sctxt_p2}"

		if [[ ! -x ${RAC_SETUP} ]];then
			printf "$(gettext 'Sun Cluster support for
				Oracle RAC is not installed on this
				system.')\n" "${PROG}" >&2
			sc_prompt_pause
			return 1
		fi

		clsetup_continue || return 1

		case $(clsetup_get_dataservices_oraclerac_menuoption) in
		'1')    clsetup_menu_dataservices_oraclerac_wizard ;;
		'2')    clsetup_menu_dataservices_oraclerac_admin ;;
		'q')    break ;;
                esac
	done

	return 0
}

#####################################################
#
# clsetup_menu_dataservices_oraclerac_removerg()
#
#
#	This function removes the Oracle RAC resource group.
#	This function calls the "rac_setup" script to remove
#	the RAC resource group.
#
#	Return:
#		zero		Completed
#		non-zero	Not successful
#
#####################################################
clsetup_menu_dataservices_oraclerac_removerg()
{
	typeset rg_state
	typeset -l lrg_state
	typeset rg_name=$(${CL_CMD_CLRESOURCEGROUP} list -t SUNW.rac_framework)
	typeset answer
	typeset rg_node
	typeset rg_nodes=""
	typeset rg_node_list
	typeset rg_online_nodes=""
	typeset rg_offline_nodes=""

	# Get resource group name and state
	rg_name=${rg_name:-"rac-framework-rg"}
	lrg_state=$(${SCHA_RG_GET} -G ${rg_name} -O RG_STATE)

	if [[ $? -ne 0 ]];then
		printf "$(gettext 'The RAC framework resource group does not exist.')\n"
		sc_prompt_pause
		return 1
	fi

	# Get the resource group's node list. It can't be null otherwise the previous RG_GET
	# would have failed.
	# rg_node_list has <CR> in so create list rg_nodes without to allow easy
	# comparison

	rg_node_list="$(${SCHA_RG_GET} -G ${rg_name} -O NODELIST )"
	for rg_node in ${rg_node_list}
	do
		if [[ -z "${rg_nodes}" ]]; then
			rg_nodes="${rg_node}"
		else
			rg_nodes="${rg_nodes} ${rg_node}"
		fi
		if [[ "$(${SCHA_RG_GET} -G ${rg_name} \
			-O RG_STATE_NODE ${rg_node})"  == "ONLINE" ]]; then
			if [[ -z "${rg_online_nodes}" ]]; then
				rg_online_nodes="${rg_node}"
			else
				rg_online_nodes="${rg_online_nodes} ${rg_node}"
			fi
		else
			if [[ -z "${rg_offline_nodes}" ]]; then
				rg_offline_nodes="${rg_node}"
			else
				rg_offline_nodes="${rg_offline_nodes} ${rg_node}"
			fi
		fi
	done
	lrg_state=""
	if [[ "${rg_online_nodes}" == "${rg_nodes}" ]]; then
		lrg_state="online on all nodes (${rg_nodes}) in its nodelist"
	elif [[ -n "${rg_online_nodes}" ]]; then
		lrg_state="online on node(s) ${rg_online_nodes}"
	fi
	if [[ "${rg_offline_nodes}" == "${rg_nodes}" ]]; then
		lrg_state="offline on all nodes (${rg_nodes}) in its nodelist"
	elif [[ -n "${rg_offline_nodes}" ]]; then
		lrg_state="${lrg_state} and offline on node(s) ${rg_offline_nodes}"
	fi

	printf "    $(gettext 'The "%s" resource group is %s.')\n\n" "${rg_name}" "${lrg_state}"

	# Initialize the command set array
	set -A CLSETUP_DO_CMDSET

	# Add the commands
	CLSETUP_DO_CMDSET[0]="${CL_CMD_CLRESOURCE} disable -R -g ${rg_name} +"
	CLSETUP_DO_CMDSET[1]="${CL_CMD_CLRESOURCE} delete -g ${rg_name} +"
	CLSETUP_DO_CMDSET[2]="${CL_CMD_CLRESOURCEGROUP} delete ${rg_name}"

	# Attempt to issue the commands
	clsetup_do_cmdset || return 1

	return 0

}

#####################################################
#
# clsetup_menu_dataservices_oraclerac_addnodes()
#
#	This function adds nodes to the Oracle RAC resource group.
#	This function calls the "rac_setup" script to modify
#	the Nodelist property of the RAC resource group. Additionally
#	the program also sets the maximum_primaries and desired_primaries
#	properties to the number of nodes selected.
#
#	Return:
#		zero		Completed
#		non-zero	Not successful
#
#####################################################
clsetup_menu_dataservices_oraclerac_addnodes()
{
	typeset addnodes=
	typeset nonracnodes=
	typeset racnodes
	typeset racnode
	typeset node
	typeset nodelist
	typeset set_nodelist
	typeset set_addlist
	typeset rg_state
	typeset rg_name=$(${CL_CMD_CLRESOURCEGROUP} list -t SUNW.rac_framework)

	# Get resource group name and state
	rg_name=${rg_name:-"rac-framework-rg"}
	rg_state=$(${SCHA_RG_GET} -G ${rg_name} -O RG_STATE)

	if [[ $? -ne 0 ]];then
		printf "$(gettext 'The RAC framework resource group does not exist.')\n"
		sc_prompt_pause
		return 1
	fi

	nodelist=$(clsetup_get_nodelist)
	racnodes=$(${SCHA_RG_GET} -G ${rg_name} -O NODELIST)

	for node in ${nodelist}
	do
		for racnode in ${racnodes}
		do
			if [[ ${racnode} == ${node} ]];then
				continue 2
			fi
		done
		nonracnodes="${nonracnodes} ${node}"
	done

	if [[ -z "${nonracnodes}" ]];then
		sc_print_para "$(printf "$(gettext 'All cluster nodes are already in \"%s\" resource group.')" "${rg_name}")"
		sc_prompt_pause
		return 1
	fi

	prompt="$(gettext 'Select the nodes to add to the RAC framework resource group')"

	addnodes="$(
				sc_get_scrolling_menuoptions	\
					"${prompt}"		\
					"" ""			\
					0 0 0			\
					${nonracnodes}	\
			)"

	if [[ -z "${addnodes}" ]];then
		printf "    $(gettext 'The list of nodes for \"%s\" resource group is not modified.')\n\n" "${rg_name}"
		sc_prompt_pause
		return 1
	fi

	set_nodelist="${racnodes} ${addnodes}"

	sc_print_para "$(printf "$(gettext 'Here is the new list of nodes for \"%s\" resource group:')" "${rg_name}")"
	for node in ${set_nodelist}
	do
		printf "\t${node}\n"
	done
	printf "\n"

	# Initialize the command set array
	set -A CLSETUP_DO_CMDSET

	# Add the commands
	set_addlist=$(echo ${addnodes}|sed -e "s/ /,/g")
	CLSETUP_DO_CMDSET[0]="${CL_CMD_CLRESOURCEGROUP} add-node -n ${set_addlist} -S ${rg_name}"

	# Attempt to issue the commands
	clsetup_do_cmdset || return 1

	return 0
}

#####################################################
#
# clsetup_menu_dataservices_oraclerac_removenodes()
#
#	This function removes nodes from the Oracle RAC resource group.
#	This function calls the "rac_setup" script to modify
#	the Nodelist property of the RAC resource group. Additionally
#	the program also sets the maximum_primaries and desired_primaries
#	properties to the number of nodes selected.
#
#	Return:
#		zero		Completed
#		non-zero	Not successful
#
#####################################################
clsetup_menu_dataservices_oraclerac_removenodes()
{
	typeset remnodes=
	typeset racnodes
	typeset racnode
	typeset set_nodelist
	typeset set_remlist
	typeset node
	typeset prompt
	integer maxremove
	typeset rg_state
	typeset rg_name=$(${CL_CMD_CLRESOURCEGROUP} list -t SUNW.rac_framework)
	typeset sctxt_p1="$(gettext '
		Before you attempt to remove a node from the RAC
		framework resource group, ensure that Sun Cluster
		Support for Oracle RAC is not running on the node.
		Removing a node from the RAC framework resource group
		does not stop Sun Cluster Support for Oracle RAC on the
		node.
	')"

	# Get resource group name and state
	rg_name=${rg_name:-"rac-framework-rg"}
	rg_state=$(${SCHA_RG_GET} -G ${rg_name} -O RG_STATE)

	if [[ $? -ne 0 ]];then
		printf "$(gettext 'The RAC framework resource group does not exist.')\n"
		sc_prompt_pause
		return 1
	fi

	racnodes=$(${SCHA_RG_GET} -G ${rg_name} -O NODELIST)

	maxremove=$(expr $(echo ${racnodes}|wc -w) - 1)

	if [[ ${maxremove} -eq 0 ]]; then
		sc_print_para "$(printf "$(gettext 'There is only one node in \"%s\" resource group.')" "${rg_name}")"
		sc_prompt_pause
		return 1
	fi

	sc_print_para "${sctxt_p1}"

	prompt="$(gettext 'Select the nodes to remove from the RAC framework resource group')"

	remnodes="$(
				sc_get_scrolling_menuoptions	\
					"${prompt}"		\
					"" ""			\
					0 ${maxremove} 0	\
					${racnodes}	\
			)"

	if [[ -z "${remnodes}" ]];then
		printf "    $(gettext 'The list of nodes for \"%s\" resource group is not modified.')\n\n" "${rg_name}"
		sc_prompt_pause
		return 1
	fi

	set_nodelist=
	for racnode in ${racnodes}
	do
		for node in ${remnodes}
		do
			if [[ ${racnode} == ${node} ]];then
				continue 2
			fi
		done
		if [[ -z "${set_nodelist}" ]];then
			set_nodelist="${racnode}"
		else
			set_nodelist="${set_nodelist} ${racnode}"
		fi
	done

	sc_print_para "$(printf "$(gettext 'Here is the new list of nodes for \"%s\" resource group:')" "${rg_name}")"
	for node in ${set_nodelist}
	do
		printf "\t${node}\n"
	done
	printf "\n"

	# Initialize the command set array
	set -A CLSETUP_DO_CMDSET

	# Add the commands
	set_remlist=$(echo ${remnodes}|sed -e "s/ /,/g")
	CLSETUP_DO_CMDSET[0]="${CL_CMD_CLRESOURCEGROUP} remove-node -n ${set_remlist} ${rg_name}"

	# Attempt to issue the commands
	clsetup_do_cmdset || return 1

	return 0
}
#####################################################
#
# clsetup_menu_dataservices_oraclerac_status()
#
#	This function prints the status of the Oracle RAC resource group.
#
#	Return:
#		zero		Completed
#		non-zero	Not successful
#
#####################################################
clsetup_menu_dataservices_oraclerac_status()
{
	typeset rg_state
	typeset rg_name=$(${CL_CMD_CLRESOURCEGROUP} list -t SUNW.rac_framework)
	typeset rs_name
	typeset rs_names
	typeset returnstatus

	# Get resource group name and state
	rg_name=${rg_name:-"rac-framework-rg"}
	rg_state=$(${SCHA_RG_GET} -G ${rg_name} -O RG_STATE)

	if [[ $? -ne 0 ]];then
		printf "$(gettext 'The RAC framework resource group does not exist.')\n"
		sc_prompt_pause
		return 1
	fi

	rs_names=
	for rs_name in $(/usr/cluster/bin/scha_resourcegroup_get -G ${rg_name} -O RESOURCE_LIST)
	do
		if [[ -n ${rs_names} ]]; then
			rs_names="${rs_names},${rs_name}"
		else
			rs_names="${rs_name}"
		fi
	done

	if [[ ${CLSETUP_ISMEMBER} -eq 1 ]]; then
                ${CL_CMD_CLRESOURCEGROUP} show -r ${rs_names} ${rg_name} 2>/dev/null
		returnstatus=$?
	else
		echo "DEBUG:  Not a cluster member - no status."
		sc_prompt_pause
	fi

	sc_prompt_pause
	return ${returnstatus}
}

#####################################################
#
# clsetup_dataservice_registration()
#
#	Performs cacao registration for
#	data service wizards. Global zone only.
#
#	Based on /usr/cluster/lib/ds/serviceregister
#
#	Any failures in registration are handled by error
#	message at time of attempting to run the wizard.
#
#	Inputs: none
#
#	Return:
#		always return 0
#
#####################################################
clsetup_dataservice_registration()
{

    typeset -r CACAOADM=/usr/lib/cacao/bin/cacaoadm

    # cacao actions may take a moment...
    printf "$(gettext 'Updating data service wizard registrations...')\n\n"

    if [ -x ${CACAOADM} ]; then
	${CACAOADM} lock com.sun.cluster.dataservices >/dev/null 2>&1
	if [ $? -ne 0 ]; then
	    ${CACAOADM} restart >/dev/null 2>&1
	else
	    ${CACAOADM} unlock com.sun.cluster.dataservices >/dev/null 2>&1
	    if [ $? -ne 0 ]; then
		${CACAOADM} restart >/dev/null 2>&1
	    fi
	fi
    fi

    return 0
}

#####################################################
#
# clsetup_check_dataservice_registrations()
#
#	This function checks for and performs any cacao
#	registration changes for recently installed or
#	removed data service wizards.
#
#	registration updates are done in bulk if any
#	change at all is detected.
#
#	Any failures are detected when wizard is invoked.
#
#	Return:
#		always return 0
#
#####################################################
clsetup_check_dataservice_registrations()
{
    typeset -r ZONECMD=/sbin/zonename
    typeset -r tmpfile=${CLSETUP_TMP_DSWIZLIST}

    if [ -x ${ZONECMD} ]; then
	if [ "$(${ZONECMD})" != "global" ]; then
	    return 0
	fi
    fi

    if [[ ! -r ${CLSETUP_DSWIZ_REG_DIR} ]]; then
	return 0
    fi

    if [[ ! -e ${CLSETUP_DSWIZ_REG_FILE} ]]; then
	/usr/bin/touch ${CLSETUP_DSWIZ_REG_FILE}
    fi

    # current reg files different from processed reg files?
    /usr/bin/ls -1 ${CLSETUP_DSWIZ_REG_DIR}/*.xml 2>/dev/null > ${tmpfile}
    /usr/bin/diff ${tmpfile} ${CLSETUP_DSWIZ_REG_FILE} >/dev/null 2>&1

    if [[ $? -eq 1 ]]; then

	# registration updates is a bulk operation
	clsetup_dataservice_registration

	# save current reg file
	/usr/bin/mv ${tmpfile} ${CLSETUP_DSWIZ_REG_FILE}
    else
	/usr/bin/rm -f ${tmpfile}
    fi

    return 0
}
	
#####################################################
#
#		Run clsetup
#
#####################################################

	main $*
	exitstatus=$?

	# If clsetup is just being used as a "library", we are done
	if [[ -n "${CLSETUP_LIBRARY}" ]]; then
		return 0
	fi

	exit ${exitstatus}
