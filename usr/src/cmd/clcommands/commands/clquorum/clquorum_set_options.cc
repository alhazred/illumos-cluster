//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clquorum_set_options.cc	1.3	09/01/16 SMI"

//
// Process clquorum "set" subcommand options
//

#include "clquorum_cmd.h"

//
// clquorum "set" options
//
clerrno_t
clquorum_set_options(ClCommand &cmd)
{
	int i;
	int c;
	int errcount = 0;
	char *cl_option = 0;
	int option_index = 0;
	char *shortopts;
	struct option *longopts;
	ValueList operands;
	optflgs_t optflgs = 0;
#ifdef WEAK_MEMBERSHIP
	int global_quorum = 0;
#endif
	NameValueList properties = NameValueList(true);
	ValueList devicetypes = ValueList(true);

	// Get the short and long option lists
	shortopts = clsubcommand_get_shortoptions(cmd);
	longopts = clsubcommand_get_longoptions(cmd);

	// This subcommand should have options
	if (!shortopts || !longopts) {
		clerror("Aborting - subcommand initialization failed.\n");
		abort();
	}

	optind = 2;
	while ((c = getopt_clip(cmd.argc, cmd.argv, shortopts, longopts,
	    &option_index)) != -1) {

		// Get the option
		delete cl_option;
		cl_option = clcommand_option(optopt, cmd.argv[optind - 1]);

		// Switch on option letter
		switch (c) {
		case 'v':
			// Verbose option
			cmd.isVerbose = 1;
			optflgs |= vflg;
			break;

		case '?':
			// Help option?
			if ((strcmp(cl_option, "-?") == 0) ||
			    (strcmp(cl_option, "--help") == 0)) {
				optflgs |= hflg;
				break;
			}

			// Or, unrecognized option?
			clerror("Unrecognized option - \"%s\".\n", cl_option);
			++errcount;
			break;

		case 't':
			// device type
			devicetypes.add(1, optarg);
			break;

		case 'p':
			// Properties
			if (strchr(optarg, '=') == NULL) {
				clerror("No property value is specified in "
				    "\"%s\".\n", optarg);
				++errcount;
			} else if (strchr(optarg, '=') !=
			    strrchr(optarg, '=')) {
				clerror("You must use one \"-p\" for each "
				    "property in \"%s\".\n",
				    optarg);
				++errcount;
#ifdef WEAK_MEMBERSHIP
			} else if (properties.add(0, optarg) != 0) {
#else
			} else if (properties.add(1, optarg) != 0) {
#endif // WEAK_MEMBERSHIP
				clerror("The same property name is specified "
				    "multiple times in \"%s\".\n",
				    optarg);
				++errcount;
			}
			break;

		case ':':
			// Missing argument
			clerror("Option \"%s\" requires an argument.\n",
			    cl_option);
			++errcount;
			break;
#ifdef WEAK_MEMBERSHIP
		case 'F':
			// Force flag
			optflgs |= Fflg;
			break;
#endif
		default:
			//
			// We should never get here.
			// If we do, it means that there
			// is an option in our option list
			// for which there is no case statement.
			// We either have a bad option list or
			// a bad case statement.
			//
			clerror("Unexpected option - \"%s\".\n", cl_option);
			++errcount;
			break;
		}
	}
	delete cl_option;
	cl_option = (char *)0;

	// Errors?
	if (errcount) {
		clcommand_usage(cmd, cmd.getSubCommandName(), errcount);
		return (CL_EINVAL);
	}

	// Just print help?
	if (optflgs & hflg) {
		clcommand_help(cmd, cmd.getSubCommandName());
		return (CL_NOERR);
	}

	// If type is specified, it must be just one
	if (devicetypes.getSize() > 1) {
		++errcount;
		clerror("You can only specify one type of "
		    "quorum device to modify.\n");
	} else if (devicetypes.getSize() == 1) {
		devicetypes.start();
		char *qd_type = devicetypes.getValue();
#ifdef WEAK_MEMBERSHIP
		if (!scconf_check_quorum_devicetype(qd_type) &&
			(strcmp(qd_type, SCCONF_QUORUM_TYPE_SYSTEM) != 0)) {
#else
		if (!scconf_check_quorum_devicetype(qd_type)) {
#endif // WEAK_MEMBERSHIP
			clerror("Invalid type \"%s\".\n", qd_type);
			return (CL_ETYPE);
		}
	}

	if (errcount) {
		clcommand_usage(cmd, cmd.getSubCommandName(), errcount);
		return (CL_EINVAL);
	}

	// There must be a property
	if (properties.getSize() == 0) {
		++errcount;
		clerror("You must specify the properties to modify.\n");
	}

	// Update operands list
	for (i = optind;  i < cmd.argc;  ++i) {
		operands.add(cmd.argv[i]);
	}

	// There must be at least one device
	if (operands.getSize() == 0) {
		++errcount;
		clerror("You must specify the quorum device to "
		    "modify.\n");
	}

#ifdef WEAK_MEMBERSHIP
	//
	// Check if we have the operand value as the GLOBAL_QUORUM object
	//
	for (operands.start(); operands.end() != 1; operands.next()) {
		if (strcmp(operands.getValue(),
			SCCONF_GLOBAL_QUORUM_NAME) == 0) {
			global_quorum = 1;
			break;
		}
	}

	//
	// If it is the GLOBAL_QUORUM
	// 	No other operands should be specified
	//	No Device Type should be specified
	//
	// If not GLOBAL_QUORUM continue
	//
	if (global_quorum) {
		if (operands.getSize() > 1) {
		    ++errcount;
		    clerror("You must specify the \"%s\" as the only operand to"
			"modify its properties.\n", SCCONF_GLOBAL_QUORUM_NAME);
		}
	}
#endif

	if (errcount) {
		clcommand_usage(cmd, cmd.getSubCommandName(), errcount);
		return (CL_EINVAL);
	}

	//
	// Perform clquorum "set" operation
	//
	return (clquorum_set(cmd, operands, optflgs,
	    devicetypes, properties));
}
