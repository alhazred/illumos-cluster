//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clquoruminit_constants.cc	1.6	09/01/16 SMI"

//
// Initialization data for clquorum(1CL)
//

#include "clquorum_cmd.h"

//
// Usage Messages
//
// All usage message lines should begin with a single "%s" for
// the command name. Lines must never include more than a single "%s".
//

// List of "clquorum" command usage message
static char *clquorum_usage [] = {
	"%s <subcommand> [<options>] [+ | <devicename> ...]",
	"%s [<subcommand>]  -? | --help",
	"%s  -V | --version",
	(char *)0
};

// List of clquorum "add" subcommand usage messages
static char *clquorum_add_usage[] = {
	"%s add [<options>] <devicename>",
	"%s add -a [-v]",
	"%s add -i <configfile> [<options>] + | <devicename> ...",
	(char *)0
};

// List of clquorum "disable" subcommand usage messages
static char *clquorum_disable_usage[] = {
	"%s disable [<options>] + | <devicename> ...",
	(char *)0
};

// List of clquorum "enable" subcommand usage messages
static char *clquorum_enable_usage[] = {
	"%s enable [<options>] + | <devicename> ...",
	(char *)0
};

// List of clquorum "export" subcommand usage messages
static char *clquorum_export_usage[] = {
	"%s export [<options>] [+ | <devicename> ...]",
	(char *)0
};

// List of clquorum "list" subcommand usage messages
static char *clquorum_list_usage[] = {
	"%s list [<options>] [+ | <devicename> ...]",
	(char *)0
};

// List of clquorum "set" subcommand usage messages
static char *clquorum_set_usage[] = {
	"%s set -p <name>=<value> [<options>] <devicename>",
	(char *)0
};

// List of clquorum "remove" subcommand usage messages
static char *clquorum_remove_usage[] = {
	"%s remove [<options>] + | <devicename> ...",
	(char *)0
};

// List of clquorum "reset" subcommand usage messages
static char *clquorum_reset_usage[] = {
	"%s reset",
	(char *)0
};

// List of clquorum "show" subcommand usage messages
static char *clquorum_show_usage[] = {
	"%s show [<options>] [+ | <devicename> ...]",
	(char *)0
};

// List of clquorum "status" subcommand usage messages
static char *clquorum_status_usage[] = {
	"%s status [<options>] [+ | <devicename> ...]",
	(char *)0
};

// List of clquorum "votecount" subcommand usage messages
static char *clquorum_votecount_usage[] = {
	"%s votecount -n <node>[,...] <votecount>",
	(char *)0
};

//
// Option lists
//
// All option descriptions should be broken up into lines of no
// greater than 70 characters.  All but the trailing newline ('\n')
// itself should be included in the option descriptions found in these
// tables.
//

// List of options for the clquorum "add" subcommand
static cloptions_t clquorum_add_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'a', "autoconfig", 0, 0,
	    CLDES_OPT_AUTOQ
	},
	{'i', "input", "{- | <clconfiguration>}", 0,
	    CLDES_OPT_INPUT
	},
	{'p', "property", "<name>=<value>", 0,
	    CLDES_OPT_PROPERTY
	},
	{'t', "type", "<type>", 0,
	    CLDES_OPT_TYPE
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the clquorum "disable" subcommand
static cloptions_t clquorum_disable_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'t', "type", "<type>", 0,
	    CLDES_OPT_TYPE
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the clquorum "enable" subcommand
static cloptions_t clquorum_enable_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'t', "type", "<type>", 0,
	    CLDES_OPT_TYPE
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the clquorum "export" subcommand
static cloptions_t clquorum_export_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'o', "output", "{- | <clconfiguration>}", 0,
	    CLDES_OPT_OUTPUT
	},
	{'t', "type", "<type>", CLOPT_ARG_LIST,
	    CLDES_OPT_TYPE
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the clquorum "list" subcommand
static cloptions_t clquorum_list_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'n', "node", "<node>", CLOPT_ARG_LIST,
	    CLDES_OPT_NODE
	},
	{'t', "type", "<type>", CLOPT_ARG_LIST,
	    CLDES_OPT_TYPE
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the clquorum "set" subcommand
static cloptions_t clquorum_set_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'p', "property", "<name>=<value>", 0,
	    CLDES_OPT_PROPERTY
	},
	{'t', "type", "<type>", 0,
	    CLDES_OPT_TYPE
	},
	{'F', "force", 0, 0,
	    CLDES_OPT_FORCE
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the clquorum "remove" subcommand
static cloptions_t clquorum_remove_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'t', "type", "<type>", CLOPT_ARG_LIST,
	    CLDES_OPT_TYPE
	},
	{'F', "force", 0, 0,
	    CLDES_OPT_FORCE
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the clquorum "reset" subcommand
static cloptions_t clquorum_reset_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the clquorum "show" subcommand
static cloptions_t clquorum_show_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'n', "node", "<node>", CLOPT_ARG_LIST,
	    CLDES_OPT_NODE
	},
	{'t', "type", "<type>", CLOPT_ARG_LIST,
	    CLDES_OPT_TYPE
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the clquorum "status" subcommand
static cloptions_t clquorum_status_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'n', "node", "<node>", CLOPT_ARG_LIST,
	    CLDES_OPT_NODE
	},
	{'t', "type", "<type>", CLOPT_ARG_LIST,
	    CLDES_OPT_TYPE
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the clquorum "votecount" subcommand
static cloptions_t clquorum_votecount_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'n', "node", "<node>", CLOPT_ARG_LIST,
	    CLDES_OPT_NODE
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of subcommands for the "clquorum" command
static clsubcommand_t clquorum_subcommands[] = {
	{
		"add",
		CLSUB_DO_COMMAND_LOGGING,
		(char *)CLQ_SUBCMD_ADD,
		CL_AUTH_CLUSTER_MODIFY,
		clquorum_add_usage,
		clquorum_add_optionslist,
		clquorum_add_options
	},
	{
		"disable",
		CLSUB_DO_COMMAND_LOGGING,
		(char *)CLQ_SUBCMD_DISABLE,
		CL_AUTH_CLUSTER_MODIFY,
		clquorum_disable_usage,
		clquorum_disable_optionslist,
		clquorum_disable_options
	},
	{
		"enable",
		CLSUB_DO_COMMAND_LOGGING,
		(char *)CLQ_SUBCMD_ENABLE,
		CL_AUTH_CLUSTER_MODIFY,
		clquorum_enable_usage,
		clquorum_enable_optionslist,
		clquorum_enable_options
	},
	{
		"export",
		CLSUB_DEFAULT,
		(char *)CLQ_SUBCMD_EXPORT,
		CL_AUTH_CLUSTER_READ,
		clquorum_export_usage,
		clquorum_export_optionslist,
		clquorum_export_options
	},
	{
		"list",
		CLSUB_NON_GLOBAL_ZONE_ALLOWED,
		(char *)CLQ_SUBCMD_LIST,
		CL_AUTH_CLUSTER_READ,
		clquorum_list_usage,
		clquorum_list_optionslist,
		clquorum_list_options
	},
	{
		"set",
#ifdef WEAK_MEMBERSHIP
		CLSUB_DO_COMMAND_LOGGING,
#else
		CLSUB_UNDOCUMENTED | CLSUB_DO_COMMAND_LOGGING,
#endif // WEAK_MEMBERSHIP
		(char *)CLQ_SUBCMD_SET,
		CL_AUTH_CLUSTER_MODIFY,
		clquorum_set_usage,
		clquorum_set_optionslist,
		clquorum_set_options
	},
	{
		"remove",
		CLSUB_DO_COMMAND_LOGGING,
		(char *)CLQ_SUBCMD_REMOVE,
		CL_AUTH_CLUSTER_MODIFY,
		clquorum_remove_usage,
		clquorum_remove_optionslist,
		clquorum_remove_options
	},
	{
		"reset",
		CLSUB_DO_COMMAND_LOGGING,
		(char *)CLQ_SUBCMD_RESET,
		CL_AUTH_CLUSTER_MODIFY,
		clquorum_reset_usage,
		clquorum_reset_optionslist,
		clquorum_reset_options
	},
	{
		"show",
		CLSUB_DEFAULT,
		(char *)CLQ_SUBCMD_SHOW,
		CL_AUTH_CLUSTER_READ,
		clquorum_show_usage,
		clquorum_show_optionslist,
		clquorum_show_options
	},
	{
		"status",
		CLSUB_NON_GLOBAL_ZONE_ALLOWED,
		(char *)CLQ_SUBCMD_STATUS,
		CL_AUTH_CLUSTER_READ,
		clquorum_status_usage,
		clquorum_status_optionslist,
		clquorum_status_options
	},
	{
		"votecount",
		CLSUB_UNDOCUMENTED | CLSUB_DO_COMMAND_LOGGING,
		(char *)CLQ_SUBCMD_VOTECOUNT,
		CL_AUTH_CLUSTER_MODIFY,
		clquorum_votecount_usage,
		clquorum_votecount_optionslist,
		clquorum_votecount_options
	},
	{0, 0, 0, 0, 0, 0, 0}
};

// Initialization properties for the "clquorum" command
clcommandinit_t clquoruminit = {
	CLCOMMANDS_COMMON_VERSION,
	(char *)CLQ_CMD_DESC,
	clquorum_usage,
	clquorum_subcommands
};
