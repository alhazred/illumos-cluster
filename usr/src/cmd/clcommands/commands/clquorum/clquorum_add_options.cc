//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clquorum_add_options.cc	1.3	08/05/21 SMI"

//
// Process clquorum "add" subcommand options
//

#include "clquorum_cmd.h"

//
// clquorum "add" options
//
clerrno_t
clquorum_add_options(ClCommand &cmd)
{
	int i;
	int c;
	int wildcard;
	int errcount = 0;
	char *cl_option = 0;
	int option_index = 0;
	char *shortopts;
	struct option *longopts;
	ValueList operands;
	optflgs_t optflgs = 0;
	int iflg = 0;

	ValueList devicetypes = ValueList(true);
	ValueList xml_devicetypes = ValueList(true);
	NameValueList properties = NameValueList(true);
	char *clconfiguration = (char *)0;

	// Get the short and long option lists
	shortopts = clsubcommand_get_shortoptions(cmd);
	longopts = clsubcommand_get_longoptions(cmd);

	// This subcommand should have options
	if (!shortopts || !longopts) {
		clerror("Aborting - subcommand initialization failed.\n");
		abort();
	}

	optind = 2;
	while ((c = getopt_clip(cmd.argc, cmd.argv, shortopts, longopts,
	    &option_index)) != -1) {

		// Get the option
		delete cl_option;
		cl_option = clcommand_option(optopt, cmd.argv[optind - 1]);

		// Switch on option letter
		switch (c) {
		case 'v':
			// Verbose option
			cmd.isVerbose = 1;
			optflgs |= vflg;
			break;

		case '?':
			// Help option?
			if ((strcmp(cl_option, "-?") == 0) ||
			    (strcmp(cl_option, "--help") == 0)) {
				optflgs |= hflg;
				break;
			}

			// Or, unrecognized option?
			clerror("Unrecognized option - \"%s\".\n", cl_option);
			++errcount;
			break;

		case 'a':
			// autoconfig
			optflgs |= aflg;
			break;

		case 't':
			// device type
			devicetypes.add(1, optarg);
			break;

		case 'i':
			// Input file
			clconfiguration = optarg;
			iflg++;
			break;

		case 'p':
			// Properties
			if (strchr(optarg, '=') == NULL) {
				clerror("No property value is specified in "
				    "\"%s\".\n", optarg);
				++errcount;
			} else if (strchr(optarg, '=') !=
			    strrchr(optarg, '=')) {
				clerror("You must use one \"-p\" for each "
				    "property in \"%s\".\n",
				    optarg);
				++errcount;
			} else if (properties.add(1, optarg) != 0) {
				clerror("The same property name is specified "
				    "multiple times in \"%s\".\n",
				    optarg);
				++errcount;
			}
			break;

		case ':':
			// Missing argument
			clerror("Option \"%s\" requires an argument.\n",
			    cl_option);
			++errcount;
			break;

		default:
			//
			// We should never get here.
			// If we do, it means that there
			// is an option in our option list
			// for which there is no case statement.
			// We either have a bad option list or
			// a bad case statement.
			//
			clerror("Unexpected option - \"%s\".\n", cl_option);
			++errcount;
			break;
		}
	}
	delete cl_option;
	cl_option = (char *)0;

	// Check for multiple options
	if (iflg > 1) {
		clerror("You can only specify the \"-i\" "
		    "option once.\n");
		++errcount;
	}

	// Errors?
	if (errcount) {
		clcommand_usage(cmd, cmd.getSubCommandName(), errcount);
		return (CL_EINVAL);
	}

	// Just print help?
	if (optflgs & hflg) {
		clcommand_help(cmd, cmd.getSubCommandName());
		return (CL_NOERR);
	}

	// Update the operands list
	wildcard = 0;
	for (i = optind;  i < cmd.argc;  ++i) {
		operands.add(cmd.argv[i]);
		if (strcmp(cmd.argv[i], CLCOMMANDS_WILD_OPERAND) == 0) {
			++wildcard;
		}
	}

	// Checks for -a
	if (optflgs & aflg) {
		if (devicetypes.getSize() > 1) {
			clerror("You can specify only one quorum "
			    "device type.\n");
			++errcount;
		}
		if ((devicetypes.getSize() > 0) &&
		    !devicetypes.isValue("scsi") &&
		    !devicetypes.isValue("shared_disk")) {
			clerror("You can only specify type \"shared_disk\" "
			    "with the \"-a\" option.\n");
			++errcount;
		}

		if (properties.getSize() != 0) {
			clerror("You cannot specify a property with "
			    "the \"-a\" option.\n");
			++errcount;
		}
		if (operands.getSize() != 0) {
			clerror("You cannot specify an operand with "
			    "the \"-a\" option.\n");
			++errcount;
		}

		if (clconfiguration) {
			clerror("You cannot use the \"-a\" option with "
			    "the \"-i\" option.\n");
			++errcount;
		}
	} else {

		// If there is no -i, set the default type
		if (!clconfiguration) {
			if (devicetypes.getSize() > 1) {
				clerror("You can only specify one quorum "
				    "device type, unless you use \"-i\".\n");
				++errcount;
			} else if (devicetypes.getSize() == 0) {
				devicetypes.add(1, "shared_disk");
			} else {
				devicetypes.start();
				char *qd_type = devicetypes.getValue();
				if (!scconf_check_quorum_devicetype(qd_type)) {
					clerror("Invalid type \"%s\".\n",
					    qd_type);
					return (CL_ETYPE);
				}

				if (strcmp(qd_type, "scsi") != 0 &&
				    strcmp(qd_type, "shared_disk") != 0) {
					if (properties.getSize() == 0) {
						clerror("You must specify the "
						    "required properties.\n");
						++errcount;
					}

					if (operands.getSize() > 1) {
						++errcount;
						clerror("You can add only one "
						    "quorum device at "
						    "a time, unless you use "
						    "\"-i\".\n");
					}
				}
			}

			// Only one device without "-i"
			if (wildcard) {
				++errcount;
				clerror("You cannot use \"%s\" with this "
				    "subcommand unless you use \"-i\".\n",
				    CLCOMMANDS_WILD_OPERAND);
			}

			if (operands.getSize() == 0) {
				++errcount;
				clerror("You must specify the name of the "
				    "quorum device to add.\n");
			}
		} else {
			// Check if the type is valid
			for (devicetypes.start(); !devicetypes.end();
			    devicetypes.next()) {
				char *qd_type = devicetypes.getValue();
				if (!scconf_check_quorum_devicetype(qd_type)) {
					clerror("Invalid type \"%s\". "
					    "Ignored.\n",
					    qd_type);
					++errcount;
				} else {
					xml_devicetypes.add(qd_type);
				}
			}
			if (errcount) {
				if (xml_devicetypes.getSize() == 0) {
					return (CL_ETYPE);
				} else {
					// Reset the error.
					errcount = 0;
				}
			}

			// There must be at least one device
			if (operands.getSize() == 0) {
				++errcount;
				clerror("You must specify either \"%s\" "
				    "or a list of quorum devices "
				    "to add.\n",
				    CLCOMMANDS_WILD_OPERAND);
			}
		}

		// wildcard and other operands cannot be specified together
		if (wildcard && operands.getSize() > 1) {
			++errcount;
			clerror("You cannot specify both \"%s\" and quorum "
			    "devices at the same time.\n",
			    CLCOMMANDS_WILD_OPERAND);
		}
	}

	if (errcount) {
		clcommand_usage(cmd, cmd.getSubCommandName(), errcount);
		return (CL_EINVAL);
	}

	// Perform clquorum "add" operation
	if (!clconfiguration) {
		return (clquorum_add(cmd, operands, optflgs,
		    devicetypes, properties));
	} else {
		return (clquorum_add(cmd, operands, optflgs,
		    clconfiguration,
		    xml_devicetypes, properties));
	}
}
