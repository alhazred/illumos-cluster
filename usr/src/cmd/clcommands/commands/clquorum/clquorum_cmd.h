//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef _CLQUORUM_CMD_H
#define	_CLQUORUM_CMD_H

#pragma ident	"@(#)clquorum_cmd.h	1.3	08/05/20 SMI"

//
// This is the header file for the clquorum(1CL) command
//

#define	CLQ_SUBCMD_ADD		\
	GETTEXT("Add a quorum device to the cluster configuration")
#define	CLQ_SUBCMD_DISABLE	\
	GETTEXT("Put quorum devices into maintenance state")
#define	CLQ_SUBCMD_ENABLE	\
	GETTEXT("Take quorum devices out of maintenance state")
#define	CLQ_SUBCMD_EXPORT	\
	GETTEXT("Export quorum configuration")
#define	CLQ_SUBCMD_LIST		\
	GETTEXT("List quorum devices")
#define	CLQ_SUBCMD_SET		\
	GETTEXT("Set quorum device properties")
#define	CLQ_SUBCMD_REMOVE	\
	GETTEXT("Remove a quorum device from the cluster configuration")
#define	CLQ_SUBCMD_RESET	\
	GETTEXT("Reset the quorum configuration")
#define	CLQ_SUBCMD_SHOW		\
	GETTEXT("Show quorum devices and their properties")
#define	CLQ_SUBCMD_STATUS	\
	GETTEXT("Display the status of the cluster quorum")
#define	CLQ_SUBCMD_VOTECOUNT	\
	GETTEXT("Set the default vote count for a node")
#define	CLQ_CMD_DESC		\
	GETTEXT("Manage cluster quorum")

#include "clcommands.h"
#include "clquorum.h"
#ifdef __cplusplus
#include <sys/os.h>
#endif
#include "scconf.h"

// Subcommand options
extern clerrno_t clquorum_add_options(ClCommand &cmd);
extern clerrno_t clquorum_disable_options(ClCommand &cmd);
extern clerrno_t clquorum_enable_options(ClCommand &cmd);
extern clerrno_t clquorum_export_options(ClCommand &cmd);
extern clerrno_t clquorum_list_options(ClCommand &cmd);
extern clerrno_t clquorum_set_options(ClCommand &cmd);
extern clerrno_t clquorum_remove_options(ClCommand &cmd);
extern clerrno_t clquorum_reset_options(ClCommand &cmd);
extern clerrno_t clquorum_show_options(ClCommand &cmd);
extern clerrno_t clquorum_status_options(ClCommand &cmd);
extern clerrno_t clquorum_votecount_options(ClCommand &cmd);

#endif /* _CLQUORUM_CMD_H */
