//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef	_CLINTERCONNECT_CMD_H
#define	_CLINTERCONNECT_CMD_H

#pragma ident	"@(#)clinterconnect_cmd.h	1.3	08/05/20 SMI"

//
// Header file for command processing functions for clinterconnect(1M)
//
#include <clinterconnect.h>

#define	CLINTR_SUBCMD_ADD	\
	GETTEXT("Add interconnect components")
#define	CLINTR_SUBCMD_DISABLE	\
	GETTEXT("Disable interconnect components")
#define	CLINTR_SUBCMD_ENABLE	\
	GETTEXT("Enable interconnect components")
#define	CLINTR_SUBCMD_EXPORT	\
	GETTEXT("Export interconnect component configuration")
#define	CLINTR_SUBCMD_REMOVE	\
	GETTEXT("Remove interconnect components")
#define	CLINTR_SUBCMD_SHOW	\
	GETTEXT("Show interconnect components and their properties")
#define	CLINTR_SUBCMD_STATUS	\
	GETTEXT("Display interconnect component status")
#define	CLINTR_CMD_DESC		\
	GETTEXT("Manage cluster interconnect components")

#define	CLINTER_OPT_LIMIT	\
	GETTEXT("Limited operation.")

extern clcommandinit_t clinterconnectinit;

// Subcommand options
extern clerrno_t clinterconnect_add_options(ClCommand &cmd);
extern clerrno_t clinterconnect_disable_options(ClCommand &cmd);
extern clerrno_t clinterconnect_enable_options(ClCommand &cmd);
extern clerrno_t clinterconnect_export_options(ClCommand &cmd);
extern clerrno_t clinterconnect_remove_options(ClCommand &cmd);
extern clerrno_t clinterconnect_show_options(ClCommand &cmd);
extern clerrno_t clinterconnect_status_options(ClCommand &cmd);

#endif /* _CLINTERCONNECT_CMD_H */
