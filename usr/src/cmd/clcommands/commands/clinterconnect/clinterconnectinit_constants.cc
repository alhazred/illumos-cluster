/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)clinterconnectinit_constants.cc	1.5	08/05/20 SMI"

//
// Initialization data for clinterconnect(1CL)
//

#include "clinterconnect_cmd.h"

//
// Usage Messages
//
// All usage message lines should begin with a single "%s" for
// the command name.   Lines must never include more than a single "%s".
//

// List of "clinterconnect" command usage message
static char *clinterconnect_usage [] = {
	"%s <subcommand> [<options>] [+ | <endpoint> ...]",
	"%s [<subcommand>]  -? | --help",
	"%s  -V | --version",
	(char *)0
};

// List of clinterconnect "add" subcommand usage messages
static char *clinterconnect_add_usage[] = {
	"%s add [<options>] <endpoint>[,<endpoint>] ...",
	"%s add -i {- | <configfile>} [<options>] "
	"{+ | <endpoint>[,<endpoint>] ...}",
	(char *)0
};

// List of clinterconnect "disable" subcommand usage messages
static char *clinterconnect_disable_usage[] = {
	"%s disable [<options>] {+ | <endpoint>[,<endpoint>] ...}",
	(char *)0
};

// List of clinterconnect "enable" subcommand usage messages
static char *clinterconnect_enable_usage[] = {
	"%s enable [<options>] {+ | <endpoint>[,<endpoint>] ...}",
	(char *)0
};

// List of clinterconnect "export" subcommand usage messages
static char *clinterconnect_export_usage[] = {
	"%s export [-o {- | <configfile>}] [<options>] "
	"{+ | <endpoint> ...}",
	(char *)0
};

// List of clinterconnect "remove" subcommand usage messages
static char *clinterconnect_remove_usage[] = {
	"%s remove [<options>] <endpoint>[,<endpoint] ...",
	(char *)0
};

// List of clinterconnect "show" subcommand usage messages
static char *clinterconnect_show_usage[] = {
	"%s show [<options>] [+ | <endpoint>[,<endpoint>] ...]",
	(char *)0
};

// List of clinterconnect "status" subcommand usage messages
static char *clinterconnect_status_usage[] = {
	"%s status [<options>]",
	(char *)0
};

//
// Option lists
//
// All option descriptions should be broken up into lines of no
// greater than 70 characters.  All but the trailing newline ('\n')
// itself should be included in the option descriptions found in these
// tables.
//

// List of options for the clinterconnect "add" subcommand
static cloptions_t clinterconnect_add_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'d', "disable", 0, 0,
	    CLDES_OPT_DISABLE
	},
	{'i', "input", "{- | <clconfiguration>}", 0,
	    CLDES_OPT_INPUT
	},
	{'n', "node", "<node>", CLOPT_ARG_LIST,
	    CLDES_OPT_NODE
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the clinterconnect "disable" subcommand
static cloptions_t clinterconnect_disable_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'n', "node", "<node>", CLOPT_ARG_LIST,
	    CLDES_OPT_NODE
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the clinterconnect "enable" subcommand
static cloptions_t clinterconnect_enable_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'n', "node", "<node>", CLOPT_ARG_LIST,
	    CLDES_OPT_NODE
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the clinterconnect "export" subcommand
static cloptions_t clinterconnect_export_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'n', "node", "<node>", CLOPT_ARG_LIST,
	    CLDES_OPT_NODE
	},
	{'o', "output", "{- | <clconfiguration>}", 0,
	    CLDES_OPT_OUTPUT
	},
	{0, 0, 0, 0, 0}
};

// List of options for the clinterconnect "remove" subcommand
static cloptions_t clinterconnect_remove_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'l', "limited", 0, 0,
	    CLINTER_OPT_LIMIT
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the clinterconnect "show" subcommand
static cloptions_t clinterconnect_show_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'n', "node", "<node>", CLOPT_ARG_LIST,
	    CLDES_OPT_NODE
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the clinterconnect "status" subcommand
static cloptions_t clinterconnect_status_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'n', "node", "<node>", CLOPT_ARG_LIST,
	    CLDES_OPT_NODE
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of subcommands for the "clinterconnect" command
static clsubcommand_t clinterconnect_subcommands[] = {
	{
		"add",
		CLSUB_DO_COMMAND_LOGGING,
		(char *)CLINTR_SUBCMD_ADD,
		CL_AUTH_CLUSTER_MODIFY,
		clinterconnect_add_usage,
		clinterconnect_add_optionslist,
		clinterconnect_add_options
	},
	{
		"disable",
		CLSUB_DO_COMMAND_LOGGING,
		(char *)CLINTR_SUBCMD_DISABLE,
		CL_AUTH_CLUSTER_MODIFY,
		clinterconnect_disable_usage,
		clinterconnect_disable_optionslist,
		clinterconnect_disable_options
	},
	{
		"enable",
		CLSUB_DO_COMMAND_LOGGING,
		(char *)CLINTR_SUBCMD_ENABLE,
		CL_AUTH_CLUSTER_MODIFY,
		clinterconnect_enable_usage,
		clinterconnect_enable_optionslist,
		clinterconnect_enable_options
	},
	{
		"export",
		CLSUB_DEFAULT,
		(char *)CLINTR_SUBCMD_EXPORT,
		CL_AUTH_CLUSTER_READ,
		clinterconnect_export_usage,
		clinterconnect_export_optionslist,
		clinterconnect_export_options
	},
	{
		"remove",
		CLSUB_DO_COMMAND_LOGGING,
		(char *)CLINTR_SUBCMD_REMOVE,
		CL_AUTH_CLUSTER_MODIFY,
		clinterconnect_remove_usage,
		clinterconnect_remove_optionslist,
		clinterconnect_remove_options
	},
	{
		"show",
		CLSUB_DEFAULT,
		(char *)CLINTR_SUBCMD_SHOW,
		CL_AUTH_CLUSTER_READ,
		clinterconnect_show_usage,
		clinterconnect_show_optionslist,
		clinterconnect_show_options
	},
	{
		"status",
		CLSUB_NON_GLOBAL_ZONE_ALLOWED,
		(char *)CLINTR_SUBCMD_STATUS,
		CL_AUTH_CLUSTER_READ,
		clinterconnect_status_usage,
		clinterconnect_status_optionslist,
		clinterconnect_status_options
	},
	{0, 0, 0, 0, 0, 0, 0}
};

// Initialization properties for the "clinterconnect" command
clcommandinit_t clinterconnectinit = {
	CLCOMMANDS_COMMON_VERSION,
	(char *)CLINTR_CMD_DESC,
	clinterconnect_usage,
	clinterconnect_subcommands
};
