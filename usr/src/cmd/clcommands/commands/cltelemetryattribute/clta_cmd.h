//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef _CLTA_CMD_H
#define	_CLTA_CMD_H

#pragma ident	"@(#)clta_cmd.h	1.3	08/05/20 SMI"

//
// This is the header file for the cltelemetryattribute(1CL) command
//

#define	CLTA_SUBCMD_DISABLE	\
	GETTEXT("Disable monitoring of the telemetry attributes")
#define	CLTA_SUBCMD_PRINT	\
	GETTEXT("Print system resource usage of the telemetry attributes")
#define	CLTA_SUBCMD_ENABLE	\
	GETTEXT("Enable monitoring of the telemetry attributes")
#define	CLTA_SUBCMD_EXPORT	\
	GETTEXT("Export telemetry attribute configuration")
#define	CLTA_SUBCMD_LIST	\
	GETTEXT("List telemetry attributes")
#define	CLTA_SUBCMD_SET		\
	GETTEXT("Set threshold configuration properties")
#define	CLTA_SUBCMD_SHOW	\
	GETTEXT("Display telemetry attribute and threshold configurations")
#define	CLTA_SUBCMD_STATUS	\
	GETTEXT("Display telemetry attribute object instance status")
#define	CLTA_CMD_DESC		\
	GETTEXT("Manage telemetry attributes and thresholds")

#include "clcommands.h"
#include "cltelemetryattribute.h"

// Subcommand options
extern clerrno_t clta_create_threshold_options(ClCommand &cmd);
extern clerrno_t clta_delete_threshold_options(ClCommand &cmd);
extern clerrno_t clta_disable_options(ClCommand &cmd);
extern clerrno_t clta_print_options(ClCommand &cmd);
extern clerrno_t clta_enable_options(ClCommand &cmd);
extern clerrno_t clta_export_options(ClCommand &cmd);
extern clerrno_t clta_list_options(ClCommand &cmd);
extern clerrno_t clta_set_threshold_options(ClCommand &cmd);
extern clerrno_t clta_show_options(ClCommand &cmd);
extern clerrno_t clta_status_options(ClCommand &cmd);

#endif /* _CLTA_CMD_H */
