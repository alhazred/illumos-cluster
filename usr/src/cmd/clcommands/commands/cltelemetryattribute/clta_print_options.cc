//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clta_print_options.cc	1.2	08/05/20 SMI"

//
// Process clta "print" subcommand options
//

#include "clta_cmd.h"

//
// clta "print" options
//
clerrno_t
clta_print_options(ClCommand &cmd)
{
	int i, c;
	int errcount = 0;
	char *cl_option = 0;
	int option_index = 0;
	int wildcard = 0;
	char *shortopts;
	struct option *longopts;
	ValueList operands;
	ValueList obj_types = ValueList(true);
	ValueList obj_instances = ValueList(true);
	ValueList nodes = ValueList(true);
	char *date_range = (char *)0;
	int dflag = 0;
	optflgs_t optflgs = 0;

	// Get the short and long option lists
	shortopts = clsubcommand_get_shortoptions(cmd);
	longopts = clsubcommand_get_longoptions(cmd);

	// This subcommand should have options
	if (!shortopts || !longopts) {
		clerror("Aborting - subcommand initialization failed.\n");
		abort();
	}

	// Initialize
	optind = 2;
	while ((c = getopt_clip(cmd.argc, cmd.argv, shortopts, longopts,
	    &option_index)) != -1) {

		// Get the option
		delete cl_option;
		cl_option = clcommand_option(optopt, cmd.argv[optind - 1]);

		// Switch on option letter
		switch (c) {
		case 'v':
			// Verbose option
			cmd.isVerbose = 1;
			optflgs |= vflg;
			break;

		case '?':
			// Help option?
			if ((strcmp(cl_option, "-?") == 0) ||
			    (strcmp(cl_option, "--help") == 0)) {
				optflgs |= hflg;
				break;
			}

			// Or, unrecognized option?
			clerror("Unrecognized option - \"%s\".\n", cl_option);
			++errcount;
			break;

		case 'a':
			// Average utilization data
			optflgs |= aflg;
			break;

		case 'd':
			// Date range
			date_range = optarg;
			dflag++;
			break;

		case 't':
			// Object types
			obj_types.add(1, optarg);
			break;

		case 'b':
			// Object instances
			obj_instances.add(1, optarg);
			break;

		case 'n':
			// Nodes
			nodes.add(1, optarg);
			break;

		case 'u':
			// Use GMT time format
			optflgs |= Mflg;
			break;

		case ':':
			// Missing argument
			clerror("Option \"%s\" requires an argument.\n",
			    cl_option);
			++errcount;
			break;

		default:
			//
			// We should never get here.
			// If we do, it means that there
			// is an option in our option list
			// for which there is no case statement.
			// We either have a bad option list or
			// a bad case statement.
			//
			clerror("Unexpected option - \"%s\".\n", cl_option);
			++errcount;
			break;
		}
	}
	delete cl_option;
	cl_option = (char *)0;

	// Check for multiple options
	if (dflag > 1) {
		clerror("You can only specify the \"-d\" option once.\n");
		++errcount;
	}

	// Errors?
	if (errcount) {
		clcommand_usage(cmd, cmd.getSubCommandName(), errcount);
		return (CL_EINVAL);
	}

	// Just print help?
	if (optflgs & hflg) {
		clcommand_help(cmd, cmd.getSubCommandName());
		return (CL_NOERR);
	}

	// Update the operands list
	for (i = optind;  i < cmd.argc;  ++i) {
		if (strcmp(cmd.argv[i], CLCOMMANDS_WILD_OPERAND) == 0) {
			++wildcard;
		} else {
			operands.add(cmd.argv[i]);
		}
	}

	// Must specify telemetry attributes
	if (wildcard && (operands.getSize() > 0)) {
		clerror("You cannot specify both \"%s\" and telemetry "
		    "attributes at the same time.\n",
		    CLCOMMANDS_WILD_OPERAND);
		++errcount;
	}

	if (errcount) {
		clcommand_usage(cmd, cmd.getSubCommandName(), errcount);
		return (CL_EINVAL);
	}

	// Perform clta "print" operation
	return (clta_print(cmd, operands, optflgs, date_range,
	    obj_types, obj_instances, nodes));
}
