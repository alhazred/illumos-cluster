//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clta_set_threshold_options.cc	1.5	08/05/20 SMI"

//
// Process clta "set-threshold" subcommand options
//

#include "clta_cmd.h"
#include <sys/sol_version.h>

#define	EQUAL	"="
#define	IS_FLOAT	0
#define	IS_EMPTY	1
#define	IS_NEG		2
#define	IS_NONUM	3

static uint_t is_float(const char *value);

//
// clta "set-threshold" options
//
clerrno_t
clta_set_threshold_options(ClCommand &cmd)
{
	int i, c;
	int errcount = 0;
	char *cl_option = 0;
	int option_index = 0;
	int wildcard = 0;
	char *shortopts;
	struct option *longopts;
	ValueList operands;
	ValueList obj_types = ValueList(true);
	ValueList obj_instances = ValueList(true);
	ValueList new_instances = ValueList(true);
	ValueList nodes = ValueList(true);
	NameValueList threshold = NameValueList(true);
	optflgs_t optflgs = 0;
	int pdup_err = 0;
	int invalid = 0;
	int eprop = 0;
	clerrno_t clerrno = CL_NOERR;
	clerrno_t tmp_err;
	int obj_size = 0;
	int use_new = 0;
	int bad_value = 0;
	bool is_zone = false;

	// Get the short and long option lists
	shortopts = clsubcommand_get_shortoptions(cmd);
	longopts = clsubcommand_get_longoptions(cmd);

	// This subcommand should have options
	if (!shortopts || !longopts) {
		clerror("Aborting - subcommand initialization failed.\n");
		abort();
	}

	// Initialize
	optind = 2;
	while ((c = getopt_clip(cmd.argc, cmd.argv, shortopts, longopts,
	    &option_index)) != -1) {

		// Get the option
		delete cl_option;
		cl_option = clcommand_option(optopt, cmd.argv[optind - 1]);

		// Switch on option letter
		switch (c) {
		case 'v':
			// Verbose option
			cmd.isVerbose = 1;
			optflgs |= vflg;
			break;

		case '?':
			// Help option?
			if ((strcmp(cl_option, "-?") == 0) ||
			    (strcmp(cl_option, "--help") == 0)) {
				optflgs |= hflg;
				break;
			}

			// Or, unrecognized option?
			clerror("Unrecognized option - \"%s\".\n", cl_option);
			++errcount;
			break;

		case 't':
			// Object types
			obj_types.add(1, optarg);
			break;

		case 'b':
			// Object instances
			obj_instances.add(1, optarg);
			break;

		case 'n':
			// Nodes
			nodes.add(1, optarg);
			break;

		case 'p':
			// Threshold
			if (threshold.add(1, optarg) != 0) {
				pdup_err++;
			}
			break;

		case ':':
			// Missing argument
			clerror("Option \"%s\" requires an argument.\n",
			    cl_option);
			++errcount;
			break;

		default:
			//
			// We should never get here.
			// If we do, it means that there
			// is an option in our option list
			// for which there is no case statement.
			// We either have a bad option list or
			// a bad case statement.
			//
			clerror("Unexpected option - \"%s\".\n", cl_option);
			++errcount;
			break;
		}
	}
	delete cl_option;
	cl_option = (char *)0;

	// Check for multiple options
	if (pdup_err != 0) {
		clerror("Threshold may not have duplicated values.\n");
		++errcount;
	}

	// Errors?
	if (errcount) {
		clcommand_usage(cmd, cmd.getSubCommandName(), errcount);
		return (CL_EINVAL);
	}

	// Just print help?
	if (optflgs & hflg) {
		clcommand_help(cmd, cmd.getSubCommandName());
		return (CL_NOERR);
	}

	// Update the operands list
	for (i = optind;  i < cmd.argc;  ++i) {
		if (strcmp(cmd.argv[i], CLCOMMANDS_WILD_OPERAND) == 0) {
			++wildcard;
		} else {
			operands.add(cmd.argv[i]);
		}
	}

	// Must specify object type
	obj_size = obj_types.getSize();
	if (obj_size == 0) {
		clerror("You must specify the object types.\n");
		++errcount;
	} else if (obj_size > 1) {
		clerror("You can only set threshold for one object "
		    "type at a time.\n");
		++errcount;
	}

	// Must specify object instances
	obj_size = obj_instances.getSize();
	if (obj_size == 0) {
		clerror("You must specify the object instances.\n");
		++errcount;
	}

#if SOL_VERSION >= __s10
	// Special process for "zone" object type.
	if (obj_types.isValue("zone") ||
	    obj_types.isValue("mot.sunw.solaris.zone")) {
		is_zone = true;
	}
	if (is_zone) {
		char *the_node = (char *)0;
		char tmp_buf[BUFSIZ];
		char *zone_name = (char *)0;
		char *node_name = (char *)0;
		int multiple_nodes = 0;

		switch (nodes.getSize()) {
		case 0:
			break;
		case 1:
			nodes.start();
			the_node = nodes.getValue();
			break;
		default:
			multiple_nodes++;
			break;
		}

		// Go through the instance
		for (obj_instances.start(); !obj_instances.end();
		    obj_instances.next()) {
			char *the_instance = obj_instances.getValue();
			if (!the_instance) {
				continue;
			}

			node_name = new char[strlen(the_instance) + 1];
			(void) strcpy(node_name, the_instance);

			// If node is specified in the instance
			if ((zone_name = strchr(node_name, ':')) != NULL) {
				// Get the node and zone in the instance
				*zone_name++ = '\0';
				if ((strlen(zone_name) == 0) ||
				    (strlen(node_name) == 0)) {
					clerror("Invalid zone instance "
					    "\"%s\".\n", the_instance);
					++bad_value;
				} else {
					new_instances.add(the_instance);
					use_new++;
				}
			} else if (the_node) {
				// Append the node to the instance
				*tmp_buf = '\0';
				(void) sprintf(tmp_buf, "%s:%s", the_node,
				    the_instance);
				new_instances.add(tmp_buf);
				use_new++;
			} else if (multiple_nodes) {
				clerror("You cannot specify more than "
				    "one node.\n");
				++bad_value;
			} else {
				clerror("You must specify the node "
				    "of zone \"%s\".\n",
				    the_instance);
				++bad_value;
			}
			delete [] node_name;
		}
	}
#endif

	// "resourcegroup" does not take "-n"
	if ((obj_types.isValue("resourcegroup") ||
	    obj_types.isValue("mot.sunw.cluster.resourcegroup")) &&
	    (nodes.getSize() > 0)) {
		clerror("You cannot specify \"-n\" with object type "
		    "\"resourcegroup\".\n");
		++invalid;
	}

	// Must specify thresholds
	if (threshold.getSize() == 0) {
		clerror("You must specify the threshold.\n");
		++errcount;
	} else {
		int has_direction = 0;
		int has_severity = 0;
		int has_value = 0;
		int has_rearm = 0;
		char *value = NULL;
		char *rearm = NULL;

		// Must specify direction and severity
		for (threshold.start(); !threshold.end(); threshold.next()) {
			char *t_name = threshold.getName();
			char *t_val = threshold.getValue();

			// Check for direction
			if (strcasecmp(t_name, STR_DIRECTION) == 0) {
				has_direction++;
				if (!t_val) {
					clerror("You must specify a value for "
					    "\"%s\".\n",
					    STR_DIRECTION);
					++errcount;
				} else if (
				    (strcasecmp(t_val, STR_RISING) != 0) &&
				    (strcasecmp(t_val, STR_FALLING) != 0)) {
					clerror("Invalid value \"%s\" for "
					    "\"%s\".\n",
					    t_val, STR_DIRECTION);
					invalid++;
					eprop++;
				}
			} else if (strcasecmp(t_name, STR_SEVERITY) == 0) {
				// Check for severity
				has_severity++;
				if (!t_val) {
					clerror("You must specify a value for "
					    "\"%s\".\n",
					    STR_SEVERITY);
					++errcount;
				} else if (
				    (strcasecmp(t_val, STR_FATAL) != 0) &&
				    (strcasecmp(t_val, STR_WARNING) != 0)) {
					clerror("Invalid value \"%s\" for "
					    "\"%s\".\n",
					    t_val, STR_SEVERITY);
					invalid++;
					eprop++;
				}
			} else if ((strcasecmp(t_name, STR_VALUELIMIT) == 0) ||
			    (strcasecmp(t_name, STR_RESETVALUE) == 0)) {
				if (strcasecmp(t_name, STR_VALUELIMIT) == 0) {
					has_value++;
					value = t_val;
				} else {
					has_rearm++;
					rearm = t_val;
				}

				// Check the specified value
				uint_t check_float = is_float(t_val);
				if (check_float == IS_EMPTY) {
					clerror("You must specify the \"%s\" "
					    "sign for \"%s\".\n",
					    EQUAL, t_name);
					++errcount;
				} else if (check_float == IS_NONUM) {
					clerror("You must specify a numeric "
					    "number for \"%s\".\n",
					    t_name);
					invalid++;
					eprop++;
				} else if (check_float == IS_NEG) {
					clerror("You must specify a positive "
					    "number for \"%s\".\n",
					    t_name);
					invalid++;
					eprop++;
				}
			} else {
				clerror("Invalid property name \"%s\" for "
				    "threshold.\n", t_name);
				invalid++;
				eprop++;
			}
		}

		// Check for required threshold fields
		if (!has_direction) {
			clerror("You must specify the \"%s\" of the "
			    "threshold.\n", STR_DIRECTION);
			invalid++;
		}
		if (!has_severity) {
			clerror("You must specify the \"%s\" of the "
			    "threshold.\n", STR_SEVERITY);
			invalid++;
		}
		if (!has_value && !has_rearm) {
			clerror("You must specify the \"%s\" and \"%s\" of "
			    "the threshold.\n",
			    STR_VALUELIMIT, STR_RESETVALUE);
			invalid++;
		}
		if (rearm && (strlen(rearm) != 0) &&
		    (value && (strlen(value) == 0))) {
			clerror("You must specify the \"%s\" of the "
			    "threshold.\n", STR_VALUELIMIT);
			invalid++;
		}
	}

	// Cannot use wildcard
	if (wildcard) {
		clerror("You cannot use \"%s\" with this "
		    "sub-command.\n", CLCOMMANDS_WILD_OPERAND);
		++errcount;
	}

	// Must specify telemetry attributes
	obj_size = operands.getSize();
	if (obj_size == 0) {
		clerror("You must specify the telemetry attribute.\n");
		++errcount;
	} else if (obj_size > 1) {
		clerror("You can only set threshold for one telemetry "
		    "attribute at a time.\n");
		++errcount;
	}

	if (errcount) {
		clcommand_usage(cmd, cmd.getSubCommandName(), errcount);
		return (CL_EINVAL);
	}

	if (eprop) {
		// Invalid properties
		return (CL_EPROP);
	}

	if (invalid) {
		return (CL_EINVAL);
	}

	if (bad_value) {
		clerrno = CL_EINVAL;
	}

	// Perform clta "set-threshold" operation
	if (is_zone) {
		if (new_instances.getSize() != 0) {
			tmp_err = clta_set_threshold(cmd, operands, optflgs,
			    obj_types, new_instances, nodes, threshold);
		}
	} else {
		tmp_err = clta_set_threshold(cmd, operands, optflgs,
		    obj_types, obj_instances, nodes, threshold);
	}
	if (clerrno == CL_NOERR) {
		clerrno = tmp_err;
	}
	return (clerrno);
}

uint_t
is_float(const char *value)
{
	double float_num;
	char *end_of_double;

	if (!value) {
		return (IS_EMPTY);
	}
	if (*value == '\0') {
		return (IS_FLOAT);
	}

	float_num = strtod(value, &end_of_double);
	if (*end_of_double != '\0') {
		return (IS_NONUM);
	}

	if (float_num < 0) {
		return (IS_NEG);
	}

	return (IS_FLOAT);
}
