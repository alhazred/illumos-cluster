//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)cltainit_constants.cc	1.6	08/05/20 SMI"

//
// Initialization data for cltelemetryattribute(1CL)
//

#include "clta_cmd.h"

//
// Usage Messages
//
// All usage message lines should begin with a single "%s" for
// the command name. Lines must never include more than a single "%s".
//

// List of "clta" command usage message
static char *clta_usage [] = {
	"%s <subcommand> [<options>] [+ | <node> ...]",
	"%s [<subcommand>]  -? | --help",
	"%s  -V | --version",
	(char *)0
};

// List of clta "disable" subcommand usage messages
static char *clta_disable_usage[] = {
	"%s disable -t <object-type> <telemetry-attribute> ...",
	"%s disable -i <configfile> [<options>] + | <telemetry-attribute> ...",
	(char *)0
};

// List of clta "print" subcommand usage messages
static char *clta_print_usage[] = {
	"%s print [<options>] [+ | <telemetry-attribute> ...]",
	(char *)0
};

// List of clta "enable" subcommand usage messages
static char *clta_enable_usage[] = {
	"%s enable -t <object-type> <telemetry-attribute> ...",
	"%s enable -i <configfile> [<options>] + | <telemetry-attribute> ...",
	(char *)0
};

// List of clta "export" subcommand usage messages
static char *clta_export_usage[] = {
	"%s export [<options>] [+ | <telemetry-attribute> ...",
	(char *)0
};

// List of clta "list" subcommand usage messages
static char *clta_list_usage[] = {
	"%s list [<options>] [+ | <telemetry-attribute> ...]",
	(char *)0
};

// List of clta "set-threshold" subcommand usage messages
static char *clta_set_threshold_usage[] = {
	"%s set_threshold -t <object-type> -b <object-instance> [<options>] "
	"-p <name>=<value>[,...] <telemetry-attribute>",
	(char *)0
};

// List of clta "show" subcommand usage messages
static char *clta_show_usage[] = {
	"%s show [<options>] [+ | <telemetry-attribute> ...]",
	(char *)0
};

// List of clta "status" subcommand usage messages
static char *clta_status_usage[] = {
	"%s status [<options>] [+ | <telemetry-attribute> ...]",
	(char *)0
};

// List of options for the clta "disable" subcommand
static cloptions_t clta_disable_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'i', "input", "{- | <clconfiguration>}", 0,
	    CLDES_OPT_INPUT
	},
	{'t', "object-type", "<object-type>", CLOPT_ARG_LIST,
	    CLDES_OPT_OTYPE
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the clta "print" subcommand
static cloptions_t clta_print_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'a', "average", 0, 0,
	    CLDES_OPT_AVG
	},
	{'b', "object-instance", "<object-instance>", CLOPT_ARG_LIST,
	    CLDES_OPT_INSTANCE
	},
	{'d', "date-range", "<date-range>", 0,
	    CLDES_OPT_DATERANGE
	},
	{'u', "utc", 0, 0,
	    CLDES_OPT_GMT
	},
	{'n', "node", "<node>", CLOPT_ARG_LIST,
	    CLDES_OPT_NODE
	},
	{'t', "object-type", "<object-type>", CLOPT_ARG_LIST,
	    CLDES_OPT_OTYPE
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the clta "enable" subcommand
static cloptions_t clta_enable_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'i', "input", "{- | <clconfiguration>}", 0,
	    CLDES_OPT_INPUT
	},
	{'t', "object-type", "<object-type>", CLOPT_ARG_LIST,
	    CLDES_OPT_OTYPE
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the clta "export" subcommand
static cloptions_t clta_export_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'o', "output", "{- | <clconfiguration>}", 0,
	    CLDES_OPT_OUTPUT
	},
	{'t', "object-type", "<object-type>", CLOPT_ARG_LIST,
	    CLDES_OPT_OTYPE
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the clta "list" subcommand
static cloptions_t clta_list_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'t', "object-type", "<object-type>", CLOPT_ARG_LIST,
	    CLDES_OPT_OTYPE
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the clta "set-threshold" subcommand
static cloptions_t clta_set_threshold_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'b', "object-instance", "<object-instance>", 0,
	    CLDES_OPT_INSTANCE
	},
	{'n', "node", "<node>", CLOPT_ARG_LIST,
	    CLDES_OPT_NODE
	},
	{'p', "property", "<name>=<value>", CLOPT_ARG_LIST,
	    CLDES_OPT_PROPERTY
	},
	{'t', "object-type", "<object-type>", 0,
	    CLDES_OPT_OTYPE
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the clta "show" subcommand
static cloptions_t clta_show_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'b', "object-instance", "<object-instance>", CLOPT_ARG_LIST,
	    CLDES_OPT_INSTANCE
	},
	{'n', "node", "<node>", CLOPT_ARG_LIST,
	    CLDES_OPT_NODE
	},
	{'t', "object-type", "<object-type>", CLOPT_ARG_LIST,
	    CLDES_OPT_OTYPE
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the clta "status" subcommand
static cloptions_t clta_status_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'b', "object-instance", "<object-instance>", CLOPT_ARG_LIST,
	    CLDES_OPT_INSTANCE
	},
	{'n', "node", "<node>", CLOPT_ARG_LIST,
	    CLDES_OPT_NODE
	},
	{'t', "object-type", "<object-type>", CLOPT_ARG_LIST,
	    CLDES_OPT_OTYPE
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of subcommands for the "clta" command
static clsubcommand_t clta_subcommands[] = {
	{
		"disable",
		CLSUB_DO_COMMAND_LOGGING,
		(char *)CLTA_SUBCMD_DISABLE,
		CL_AUTH_CLUSTER_MODIFY,
		clta_disable_usage,
		clta_disable_optionslist,
		clta_disable_options
	},
	{
		"print",
		CLSUB_DEFAULT,
		(char *)CLTA_SUBCMD_PRINT,
		CL_AUTH_CLUSTER_READ,
		clta_print_usage,
		clta_print_optionslist,
		clta_print_options
	},
	{
		"enable",
		CLSUB_DO_COMMAND_LOGGING,
		(char *)CLTA_SUBCMD_ENABLE,
		CL_AUTH_CLUSTER_MODIFY,
		clta_enable_usage,
		clta_enable_optionslist,
		clta_enable_options
	},
	{
		"export",
		CLSUB_DEFAULT,
		(char *)CLTA_SUBCMD_EXPORT,
		CL_AUTH_CLUSTER_READ,
		clta_export_usage,
		clta_export_optionslist,
		clta_export_options
	},
	{
		"list",
		CLSUB_DEFAULT,
		(char *)CLTA_SUBCMD_LIST,
		CL_AUTH_CLUSTER_READ,
		clta_list_usage,
		clta_list_optionslist,
		clta_list_options
	},
	{
		"set-threshold",
		CLSUB_DO_COMMAND_LOGGING,
		(char *)CLTA_SUBCMD_SET,
		CL_AUTH_CLUSTER_MODIFY,
		clta_set_threshold_usage,
		clta_set_threshold_optionslist,
		clta_set_threshold_options
	},
	{
		"show",
		CLSUB_DEFAULT,
		(char *)CLTA_SUBCMD_SHOW,
		CL_AUTH_CLUSTER_READ,
		clta_show_usage,
		clta_show_optionslist,
		clta_show_options
	},
	{
		"status",
		CLSUB_DEFAULT,
		(char *)CLTA_SUBCMD_STATUS,
		CL_AUTH_CLUSTER_READ,
		clta_status_usage,
		clta_status_optionslist,
		clta_status_options
	},
	{0, 0, 0, 0, 0, 0, 0}
};

// Initialization properties for the "clta" command
clcommandinit_t cltainit = {
	CLCOMMANDS_COMMON_VERSION,
	(char *)CLTA_CMD_DESC,
	clta_usage,
	clta_subcommands
};
