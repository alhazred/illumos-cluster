//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clzonecluster_export_options.cc	1.6	08/07/25 SMI"


#include "clzonecluster_cmd.h"

int
clzonecluster_export_options(ClCommand &cmd)
{

	int c, i;
	int errcount = 0;
	char *cl_option = 0;
	int option_index = 0;
	char *shortopts;
	struct option *longopts;
	optflgs_t optflgs = 0;
	ValueList nodes;
	int wildcard;
	int num_operands = 0;
	ValueList operands;
	char *zone_cluster;
	char *command_file = NULL;
	int cmd_file_flag = 0;

	// Get the short and long option lists
	shortopts = clsubcommand_get_shortoptions(cmd);
	longopts = clsubcommand_get_longoptions(cmd);

	// This subcommand should have options
	if (!shortopts || !longopts) {
		clerror("Aborting - subcommand initialization failed.\n");
		abort();
	}

	optind = 2;
	while ((c = getopt_clip(cmd.argc, cmd.argv, shortopts, longopts,
	    &option_index)) != -1) {

		// Get the option
		delete cl_option;
		cl_option = clcommand_option(optopt, cmd.argv[optind - 1]);

		// Switch on option letter
		switch (c) {
		case 'v':
			// Verbose option
			cmd.isVerbose = 1;
			optflgs |= vflg;
			break;

		case '?':
			// Help option?
			if ((strcmp(cl_option, "-?") == 0) ||
			    (strcmp(cl_option, "--help") == 0)) {
				optflgs |= hflg;
				break;
			}

			// Or, unrecognized option?
			clerror("Unrecognized option - \"%s\".\n", cl_option);
			++errcount;
			break;

		case 'f':
			// Output file
			optflgs |= fflg;
			command_file = optarg;
			cmd_file_flag += 1;
			break;

		default:
			//
			// We should never get here.
			// If we do, it means that there
			// is an option in our option list
			// for which there is no case statement.
			// We either have a bad option list or
			// a bad case statement.
			//
			clerror("Unexpected option - \"%s\".\n", cl_option);
			++errcount;
			break;
		}
	}

	// Get the option
	delete cl_option;
	cl_option = (char *)0;

	// Errors?
	if (errcount) {
		clcommand_usage(cmd, cmd.getSubCommandName(), errcount);
		return (CL_EINVAL);
	}

	// Just print help?
	if (optflgs & hflg) {
		clcommand_help(cmd, cmd.getSubCommandName());
		return (CL_NOERR);
	}

	// Wildcard operand is is not allowed for this sub-command
	for (i = optind;  i < cmd.argc;  ++i) {
		num_operands++;
		if (strcmp(cmd.argv[i], CLCOMMANDS_WILD_OPERAND) == 0) {
			// Wildcard was specified, which is not supported
			errcount++;
			clerror("You cannot specify %s as an operand "
				"for this operation.\n",
				CLCOMMANDS_WILD_OPERAND);
		}
	}

	// Only one operand is allowed for this sub-command
	if (num_operands == 0) {
		++errcount;
		clerror("You must specify the name of the zone cluster you "
			"want to export the configuration for.\n");
	} else if (num_operands > 1) {
		++errcount;
		clerror("You can specify only one zone cluster as "
		    "operand.\n");
	}

	// -f flag can be specified only once
	if ((optflgs & fflg) && (cmd_file_flag > 1)) {
		errcount++;
		clerror("You can specify only one output file.\n");
	}

	// Errors?
	if (errcount) {
		clcommand_usage(cmd, cmd.getSubCommandName(), errcount);
		return (CL_EINVAL);
	}

	// Set the zone name
	zone_cluster = cmd.argv[optind];

	// Perform clzone "export" operation
	return (clzonecluster_export(cmd, zone_cluster, optflgs, command_file));
}
