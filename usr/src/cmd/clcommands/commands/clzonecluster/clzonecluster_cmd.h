//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef	_CLZONECLUSTER_CMD_H
#define	_CLZONECLUSTER_CMD_H

#pragma ident	"@(#)clzonecluster_cmd.h	1.13	08/10/14 SMI"

#include <sys/sol_version.h>

#include "clcommands.h"
#include "ClCommand.h"
#include "clzonecluster.h"

#define	CLZONE_CMD_DESC		\
	GETTEXT("Manage zone clusters for Sun Cluster")
#define	CLZONE_SUBCMD_BOOT	\
	GETTEXT("Boot zone clusters")
#define	CLZONE_SUBCMD_CONFIGURE	\
	GETTEXT("Configure a zone cluster")
#define	CLZONE_SUBCMD_HALT	\
	GETTEXT("Halt zone clusters")
#define	CLZONE_SUBCMD_INSTALL	\
	GETTEXT("Install a zone cluster")
#define	CLZONE_SUBCMD_LIST	\
	GETTEXT("List zone clusters")
#define	CLZONE_SUBCMD_REBOOT	\
	GETTEXT("Reboot zone clusters")
#define	CLZONE_SUBCMD_SHOW	\
	GETTEXT("Show zone clusters")
#define	CLZONE_SUBCMD_STATUS	\
	GETTEXT("Status of zone clusters")
#define	CLZONE_SUBCMD_UNINSTALL	\
	GETTEXT("Uninstall a zone cluster")
#define	CLZONE_SUBCMD_VERIFY	\
	GETTEXT("Verify zone clusters")
#define	CLZONE_SUBCMD_READY	\
	GETTEXT("Ready zone clusters")
#define	CLZONE_SUBCMD_MOVE      \
	GETTEXT("Move a zone cluster")
#define	CLZONE_SUBCMD_EXPORT    \
	GETTEXT("Export a zone cluster configuration")
#define	CLZONE_SUBCMD_CLONE    \
	GETTEXT("Clone a zone cluster")
#define	CLZONE_SUBCMD_DELETE    \
	GETTEXT("Delete a zone cluster")

extern clcommandinit_t clzoneclusterinit;

extern int clzonecluster_boot_options(ClCommand &cmd);
extern int clzonecluster_configure_options(ClCommand &cmd);
extern int clzonecluster_halt_options(ClCommand &cmd);
extern int clzonecluster_install_options(ClCommand &cmd);
extern int clzonecluster_list_options(ClCommand &cmd);
extern int clzonecluster_reboot_options(ClCommand &cmd);
extern int clzonecluster_show_options(ClCommand &cmd);
extern int clzonecluster_status_options(ClCommand &cmd);
extern int clzonecluster_uninstall_options(ClCommand &cmd);
extern int clzonecluster_verify_options(ClCommand &cmd);
extern int clzonecluster_ready_options(ClCommand &cmd);
extern int clzonecluster_move_options(ClCommand &cmd);
extern int clzonecluster_export_options(ClCommand &cmd);
extern int clzonecluster_clone_options(ClCommand &cmd);
extern int clzonecluster_delete_options(ClCommand &cmd);
#endif /* _CLZONECLUSTER_CMD_H */
