//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clzonecluster_init_constants.cc	1.16	08/10/14 SMI"

#include "clzonecluster_cmd.h"

static char *clzonecluster_usage [] = {
	"%s <subcommand> [<options>] [+ | <zoneclustername> ...]",
	"%s [<subcommand>] -? | --help",
	"%s -V | --version",
	(char *)0
};

// "boot" sub-command
static char *clzonecluster_boot_usage [] = {
	"%s boot [<options>] + | <zoneclustername> ...",
	(char *)0
};

// List of options for the clzonecluster "boot" subcommand
static cloptions_t clzonecluster_boot_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{'n', "node", "<node>[,...]", 0,
	    CLDES_OPT_NODE
	},
	{0, 0, 0, 0, 0}
};

// "configure" sub-command
static char *clzonecluster_configure_usage [] = {
	"%s configure [<options>] <zoneclustername>",
	(char *)0
};

// List of options for the clzonecluster "configure" subcommand
static cloptions_t clzonecluster_configure_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{'f', "commandfile", "<command file>", 0,
	    CLDES_OPT_COMMANDFILE
	},
	{0, 0, 0, 0, 0}
};

// "halt" sub-command
static char *clzonecluster_halt_usage [] = {
	"%s halt [<options>] + | <zoneclustername> ...",
	(char *)0
};

// List of options for the clzonecluster "halt" subcommand
static cloptions_t clzonecluster_halt_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{'n', "node", "<node>[,...]", 0,
	    CLDES_OPT_NODE
	},
	{0, 0, 0, 0, 0}
};

// "install" sub-command
static char *clzonecluster_install_usage [] = {
	"%s install [<options>] <zoneclustername>",
	(char *)0
};

// List of options for the clzonecluster "install" subcommand
static cloptions_t clzonecluster_install_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{'n', "node", "<node>[,...]", 0,
	    CLDES_OPT_NODE
	},
	{0, 0, 0, 0, 0}
};

// "list" sub-command
static char *clzonecluster_list_usage [] = {
	"%s list [<options>] [+ | <zoneclustername> ...]",
	(char *)0
};

// List of options for the clzonecluster "list" subcommand
static cloptions_t clzonecluster_list_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// "reboot" sub-command
static char *clzonecluster_reboot_usage [] = {
	"%s reboot [<options>] + | <zoneclustername> ...",
	(char *)0
};

// List of options for the clzonecluster "reboot" subcommand
static cloptions_t clzonecluster_reboot_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{'n', "node", "<node>[,...]", 0,
	    CLDES_OPT_NODE
	},
	{0, 0, 0, 0, 0}
};

// "show" sub-command
static char *clzonecluster_show_usage [] = {
	"%s show [<options>] [+ | <zoneclustername> ...]",
	(char *)0
};

// List of options for the clzonecluster "show" subcommand
static cloptions_t clzonecluster_show_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// "status" sub-command
static char *clzonecluster_status_usage [] = {
	"%s status [<options>] [+ | <zoneclustername> ...]",
	(char *)0
};

// List of options for the clzonecluster "status" subcommand
static cloptions_t clzonecluster_status_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// "uninstall" sub-command
static char *clzonecluster_uninstall_usage [] = {
	"%s uninstall [<options>] <zoneclustername>",
	(char *)0
};

// List of options for the clzonecluster "uninstall" subcommand
static cloptions_t clzonecluster_uninstall_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{'n', "node", "<node>[,...]", 0,
	    CLDES_OPT_NODE
	},
	{'F', "force", 0, 0,
	    CLDES_OPT_FORCE
	},
	{0, 0, 0, 0, 0}
};

// "verify" sub-command
static char *clzonecluster_verify_usage [] = {
	"%s verify [<options>] + | <zoneclustername> ...",
	(char *)0
};

// List of options for the clzonecluster "verify" subcommand
static cloptions_t clzonecluster_verify_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{'n', "node", "<node>[,...]", 0,
	    CLDES_OPT_NODE
	},
	{0, 0, 0, 0, 0}
};

// "ready" sub-command
static char *clzonecluster_ready_usage [] = {
	"%s verify [<options>] + | <zoneclustername> ...",
	(char *)0
};

// List of options for the clzonecluster "ready" subcommand
static cloptions_t clzonecluster_ready_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{'n', "node", "<node>[,...]", 0,
	    CLDES_OPT_NODE
	},
	{0, 0, 0, 0, 0}
};

// "move" sub-command
static char *clzonecluster_move_usage [] = {
	"%s move -f <newzonepath> [<options>] <zoneclustername>",
	(char *)0
};

// List of options for the clzonecluster "move" subcommand
static cloptions_t clzonecluster_move_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{'f', "zonepath", "<newzonepath>", 0,
	    CLDES_OPT_ZONEPATH
	},
	{0, 0, 0, 0, 0}
};

// "export" sub-command
static char *clzonecluster_export_usage [] = {
	"%s export [<options>] <zoneclustername>",
	(char *)0
};

// List of options for the clzonecluster "export" subcommand
static cloptions_t clzonecluster_export_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{'f', "output commandfile", "<output commandfile>", 0,
	    CLDES_OPT_COMMANDFILE
	},
	{0, 0, 0, 0, 0}
};

// "clone" sub-command
static char *clzonecluster_clone_usage [] = {
	"%s clone -Z <target-zoneclustername> [<options>] "
	"<source-zoneclustername>",
	(char *)0
};

// List of options for the clzonecluster "clone" subcommand
static cloptions_t clzonecluster_clone_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{'Z', "zonecluster", "<target-zoneclustername>", 0,
	    CLDES_OPT_TGT_ZC_NAME
	},
	{'n', "node", "<node>[,...]", 0,
	    CLDES_OPT_NODE
	},
	{'m', "copymethod", "<copymethod>", 0,
	    CLDES_OPT_ZC_COPYMETHOD
	},
	{0, 0, 0, 0, 0}
};

// "delete" sub-command
static char *clzonecluster_delete_usage [] = {
	"%s delete [<options>] <zoneclustername>",
	(char *)0
};

// List of options for the clzonecluster "install" subcommand
static cloptions_t clzonecluster_delete_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{'F', "force", 0, 0,
	    CLDES_OPT_FORCE
	},
	{0, 0, 0, 0, 0}
};

static clsubcommand_t clzonecluster_subcommands[] = {
	{
		"boot",
		CLSUB_DO_COMMAND_LOGGING,
		(char *)CLZONE_SUBCMD_BOOT,
		CL_AUTH_CLUSTER_ADMIN,
		clzonecluster_boot_usage,
		clzonecluster_boot_optionslist,
		clzonecluster_boot_options
	},
	{
		"configure",
		CLSUB_DO_COMMAND_LOGGING,
		(char *)CLZONE_SUBCMD_CONFIGURE,
		CL_AUTH_CLUSTER_ADMIN,
		clzonecluster_configure_usage,
		clzonecluster_configure_optionslist,
		clzonecluster_configure_options
	},
	{
		"halt",
		CLSUB_DO_COMMAND_LOGGING,
		(char *)CLZONE_SUBCMD_HALT,
		CL_AUTH_CLUSTER_ADMIN,
		clzonecluster_halt_usage,
		clzonecluster_halt_optionslist,
		clzonecluster_halt_options
	},
	{
		"install",
		CLSUB_DO_COMMAND_LOGGING,
		(char *)CLZONE_SUBCMD_INSTALL,
		CL_AUTH_CLUSTER_ADMIN,
		clzonecluster_install_usage,
		clzonecluster_install_optionslist,
		clzonecluster_install_options
	},
	{
		"list",
		CLSUB_DEFAULT | CLSUB_ZONE_CLUSTER_ALLOWED,
		(char *)CLZONE_SUBCMD_LIST,
		CL_AUTH_CLUSTER_READ,
		clzonecluster_list_usage,
		clzonecluster_list_optionslist,
		clzonecluster_list_options
	},
	{
		"reboot",
		CLSUB_DO_COMMAND_LOGGING,
		(char *)CLZONE_SUBCMD_REBOOT,
		CL_AUTH_CLUSTER_ADMIN,
		clzonecluster_reboot_usage,
		clzonecluster_reboot_optionslist,
		clzonecluster_reboot_options
	},
	{
		"show",
		CLSUB_DEFAULT,
		(char *)CLZONE_SUBCMD_SHOW,
		CL_AUTH_CLUSTER_READ,
		clzonecluster_show_usage,
		clzonecluster_show_optionslist,
		clzonecluster_show_options
	},
	{
		"status",
		CLSUB_DEFAULT,
		(char *)CLZONE_SUBCMD_STATUS,
		CL_AUTH_CLUSTER_READ,
		clzonecluster_status_usage,
		clzonecluster_status_optionslist,
		clzonecluster_status_options
	},
	{
		"uninstall",
		CLSUB_DO_COMMAND_LOGGING,
		(char *)CLZONE_SUBCMD_UNINSTALL,
		CL_AUTH_CLUSTER_ADMIN,
		clzonecluster_uninstall_usage,
		clzonecluster_uninstall_optionslist,
		clzonecluster_uninstall_options
	},
	{
		"verify",
		CLSUB_DO_COMMAND_LOGGING,
		(char *)CLZONE_SUBCMD_VERIFY,
		CL_AUTH_CLUSTER_ADMIN,
		clzonecluster_verify_usage,
		clzonecluster_verify_optionslist,
		clzonecluster_verify_options
	},
	{
		"ready",
		CLSUB_DO_COMMAND_LOGGING,
		(char *)CLZONE_SUBCMD_READY,
		CL_AUTH_CLUSTER_ADMIN,
		clzonecluster_ready_usage,
		clzonecluster_ready_optionslist,
		clzonecluster_ready_options
	},
	{
		"move",
		CLSUB_DO_COMMAND_LOGGING,
		(char *)CLZONE_SUBCMD_MOVE,
		CL_AUTH_CLUSTER_ADMIN,
		clzonecluster_move_usage,
		clzonecluster_move_optionslist,
		clzonecluster_move_options
	},
	{
		"export",
		CLSUB_DEFAULT,
		(char *)CLZONE_SUBCMD_EXPORT,
		CL_AUTH_CLUSTER_ADMIN,
		clzonecluster_export_usage,
		clzonecluster_export_optionslist,
		clzonecluster_export_options
	},
	{
		"clone",
		CLSUB_DO_COMMAND_LOGGING,
		(char *)CLZONE_SUBCMD_CLONE,
		CL_AUTH_CLUSTER_ADMIN,
		clzonecluster_clone_usage,
		clzonecluster_clone_optionslist,
		clzonecluster_clone_options
	},
	{
		"delete",
		CLSUB_DO_COMMAND_LOGGING,
		(char *)CLZONE_SUBCMD_DELETE,
		CL_AUTH_CLUSTER_ADMIN,
		clzonecluster_delete_usage,
		clzonecluster_delete_optionslist,
		clzonecluster_delete_options
	},
	{0, 0, 0, 0, 0, 0, 0}
};

clcommandinit_t clzoneclusterinit = {
	CLCOMMANDS_COMMON_VERSION,
	(char *)CLZONE_CMD_DESC,
	clzonecluster_usage,
	clzonecluster_subcommands
};
