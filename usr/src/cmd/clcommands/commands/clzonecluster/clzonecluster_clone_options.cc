//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clzonecluster_clone_options.cc	1.7	08/10/14 SMI"


#include "clzonecluster_cmd.h"

int
clzonecluster_clone_options(ClCommand &cmd) {

	int c, i;
	int errcount = 0;
	char *cl_option = 0;
	int option_index = 0;
	char *shortopts;
	struct option *longopts;
	optflgs_t optflgs = 0;
	ValueList nodes;
	int wildcard;
	int num_operands;
	ValueList operands;
	char *source_zc = NULL;
	char *target_zc = NULL;
	char *copymethod = NULL;
	int source_zc_flag = 0;
	int target_zc_flag = 0;
	int copymethod_flag = 0;

	// Get the short and long option lists
	shortopts = clsubcommand_get_shortoptions(cmd);
	longopts = clsubcommand_get_longoptions(cmd);

	// This subcommand should have options
	if (!shortopts || !longopts) {
		clerror("Aborting - subcommand initialization failed.\n");
		abort();
	}

	optind = 2;
	while ((c = getopt_clip(cmd.argc, cmd.argv, shortopts, longopts,
		&option_index)) != -1) {

		// Get the option
		delete cl_option;
		cl_option = clcommand_option(optopt, cmd.argv[optind - 1]);

		// Switch on option letter
		switch (c) {
			case 'v':
				// Verbose option
				cmd.isVerbose = 1;
				optflgs |= vflg;
				break;

			case '?':
				// Help option?
				if ((strcmp(cl_option, "-?") == 0) ||
					(strcmp(cl_option, "--help") == 0)) {
					optflgs |= hflg;
					break;
				}

				// Or, unrecognized option?
				clerror("Unrecognized option - \"%s\".\n",
					cl_option);
				++errcount;
				break;

			case 'Z':
				// Target zone cluster
				optflgs |= Zflg;
				target_zc = optarg;
				target_zc_flag += 1;
				break;

			case 'm':
				// Clone copy method
				optflgs |= mflg;
				copymethod = optarg;
				copymethod_flag += 1;
				break;

			case 'n':
				// Nodes
				nodes.add(1, optarg);
				break;

			default:
				//
				// We should never get here.
				// If we do, it means that there
				// is an option in our option list
				// for which there is no case statement.
				// We either have a bad option list or
				// a bad case statement.
				//
				clerror("Unexpected option - \"%s\".\n",
					cl_option);
				++errcount;
				break;
		}
	}
	// Get the option
	delete cl_option;
	cl_option = (char *)0;

	// Errors?
	if (errcount) {
		clcommand_usage(cmd, cmd.getSubCommandName(), errcount);
		return (CL_EINVAL);
	}

	// Just print help?
	if (optflgs & hflg) {
		clcommand_help(cmd, cmd.getSubCommandName());
		return (CL_NOERR);
	}

	if (!(optflgs & Zflg)) {
		errcount++;
		clerror("You must specify the source zone cluster to clone.\n");
		clcommand_usage(cmd, cmd.getSubCommandName(), errcount);
		return (CL_EINVAL);
	}

	// -z flag can be specified only once
	if ((optflgs & Zflg) && (target_zc_flag > 1)) {
		errcount++;
		clerror("You can specify only one target zone cluster.\n");
		clcommand_usage(cmd, cmd.getSubCommandName(), errcount);
		return (CL_EINVAL);
	}

	// -m flag can be specified only once
	if ((optflgs & mflg) && (copymethod_flag > 1)) {
		errcount++;
		clerror("You can specify only one copy method to clone.\n");
		clcommand_usage(cmd, cmd.getSubCommandName(), errcount);
		return (CL_EINVAL);
	}

	// If wildcard operand is specified, there should be no other operand
	wildcard = 0;
	num_operands = 0;

	for (i = optind;  i < cmd.argc;  ++i) {
		num_operands++;
		if (strcmp(cmd.argv[i], CLCOMMANDS_WILD_OPERAND) == 0) {
			++wildcard;
		}
	}

	// If WILDCARD is specified or if the number of operands is more than
	// one, we error out with the usage message.
	if ((wildcard) || (num_operands > 2)) {
		++errcount;
		clerror("You must specify only one zone cluster "
			"name as operand. You cannot use \"%s\" as "
			"an operand\n", CLCOMMANDS_WILD_OPERAND);
		clcommand_usage(cmd, cmd.getSubCommandName(), errcount);
		return (CL_EINVAL);
	}

	// Fill operands list with the zone cluster name given as input
	// There should be only one operand specified.
	for (i = optind;  i < cmd.argc;  ++i) {
		operands.add(cmd.argv[i]);
	}

	// There should be exactly one zone cluster specified
	if (operands.getSize() != 1) {
		++errcount;
		clerror("You must specify only one source zone cluster "
			"name as operand.\n");
		clcommand_usage(cmd, cmd.getSubCommandName(), errcount);
		return (CL_EINVAL);
	}

	// If no target were specified then we should return
	if (target_zc != NULL && strlen(target_zc) < 1) {
		clerror("You must specify the name of the target zone cluster "
			" that you want to create.\n");
		clcommand_usage(cmd, cmd.getSubCommandName(), 1);
		return (CL_EINVAL);
	}

	//
	// Copy over the Source Zone cluster name from the operands
	// ValueList to source_zc
	//

	operands.start();
	if (operands.getValue() != NULL) {
		source_zc = strdup(operands.getValue());
	}

	// If no source zc is specified then we should return
	if (source_zc != NULL && strlen(source_zc) < 1) {
		clerror("You must specify the name of the source zone cluster "
			" that you want to duplicate.\n");
		clcommand_usage(cmd, cmd.getSubCommandName(), 1);
		return (CL_EINVAL);
	}

	// Perform clzonecluster "clone" operation
	return (clzonecluster_clone(cmd, source_zc, optflgs, nodes, target_zc,
		copymethod));
}
