//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef	_CLSNMPHOST_CMD_H
#define	_CLSNMPHOST_CMD_H

#pragma ident	"@(#)clsnmphost_cmd.h	1.3	08/05/20 SMI"

#include "clcommands.h"
#include "clsnmphost.h"

//
// This is the main header file for clsnmphost options functions
//

#define	CLSNMPHOST_SUBCMD_ADD		\
	GETTEXT("Add an SNMP host")
#define	CLSNMPHOST_SUBCMD_REMOVE	\
	GETTEXT("Remove and SNMP host")
#define	CLSNMPHOST_SUBCMD_EXPORT	\
	GETTEXT("Export SNMP host configuration")
#define	CLSNMPHOST_SUBCMD_LIST		\
	GETTEXT("List SNMP hosts")
#define	CLSNMPHOST_SUBCMD_SHOW		\
	GETTEXT("Show SNMP hosts and their properties")
#define	CLSNMPHOST_CMD_DESC		\
	GETTEXT("Administer Sun Cluster SNMP hosts")

extern clcommandinit_t clsnmphostinit;

// Subcommand options
extern clerrno_t clsnmphost_add_options(ClCommand &cmd);
extern clerrno_t clsnmphost_remove_options(ClCommand &cmd);
extern clerrno_t clsnmphost_export_options(ClCommand &cmd);
extern clerrno_t clsnmphost_list_options(ClCommand &cmd);
extern clerrno_t clsnmphost_show_options(ClCommand &cmd);

#endif /* _CLSNMPHOST_CMD_H */
