//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident   "@(#)clsnmphostinit_constants.cc	1.3	08/05/20 SMI"

//
// Initialization data for clsnmphost(1CL)
//

#include "clsnmphost_cmd.h"

extern clcommandinit_t clsnmphostinit;

//
// Usage Messages
//
// All usage message lines should begin with a single "%s" for
// the command name.   Lines must never include more than a single "%s".
//

// List of "clsnmphost" command usage message
static char *clsnmphost_usage [] = {
	"%s <subcommand> [<options>] [+ | <snmphost> ...]",
	"%s [<subcommand>]  -? | --help",
	"%s  -V | --version",
	(char *)0
};

// List of clsnmphost "add" subcommand usage messages
static char *clsnmphost_add_usage[] = {
	"%s add [<options>] + | <snmphost> ...",
	(char *)0
};

// List of clsnmphost "export" subcommand usage messages
static char *clsnmphost_export_usage[] = {
	"%s export [<options>] [+ | <snmphost> ...]",
	(char *)0
};

// List of clsnmphost "list" subcommand usage messages
static char *clsnmphost_list_usage[] = {
	"%s list [<options>] [+ | <snmphost> ...]",
	(char *)0
};

// List of clsnmphost "remove" subcommand usage messages
static char *clsnmphost_remove_usage[] = {
	"%s remove [<options>] + | <snmphost> ...",
	(char *)0
};

// List of clsnmphost "show" subcommand usage messages
static char *clsnmphost_show_usage[] = {
	"%s show [<options>] [+ | <snmphost> ...]",
	(char *)0
};

//
// Option lists
//
// All option descriptions should be broken up into lines of no
// greater than 70 characters.  All but the trailing newline ('\n')
// itself should be included in the option descriptions found in these
// tables.
//

// List of options for the clsnmphost "add" subcommand
static cloptions_t clsnmphost_add_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'c', "community", "<community>", 0,
	    CLDES_OPT_COMMUNITY
	},
	{'i', "input", "{- | <clconfiguration>}", 0,
	    CLDES_OPT_INPUT
	},
	{'n', "node", "<node>", CLOPT_ARG_LIST,
	    CLDES_OPT_NODE
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the clsnmphost "remove" subcommand
static cloptions_t clsnmphost_remove_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'c', "community", "<community>", 0,
	    CLDES_OPT_COMMUNITY
	},
	{'n', "node", "<node>", CLOPT_ARG_LIST,
	    CLDES_OPT_NODE
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the clsnmphost "export" subcommand
static cloptions_t clsnmphost_export_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'c', "community", "<community>", 0,
	    CLDES_OPT_COMMUNITY
	},
	{'n', "node", "<node>", CLOPT_ARG_LIST,
	    CLDES_OPT_NODE
	},
	{'o', "output", "{- | <clconfiguration>}", 0,
	    CLDES_OPT_OUTPUT
	},
	{0, 0, 0, 0, 0}
};

// List of options for the clsnmphost "list" subcommand
static cloptions_t clsnmphost_list_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'c', "community", "<community>", 0,
	    CLDES_OPT_COMMUNITY
	},
	{'n', "node", "<node>", CLOPT_ARG_LIST,
	    CLDES_OPT_NODE
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the clsnmphost "show" subcommand
static cloptions_t clsnmphost_show_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'c', "community", "<community>", 0,
	    CLDES_OPT_COMMUNITY
	},
	{'n', "node", "<node>", CLOPT_ARG_LIST,
	    CLDES_OPT_NODE
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of subcommands for the "clsnmphost" command
static clsubcommand_t clsnmphost_subcommands[] = {
	{
		"add",
		CLSUB_DO_COMMAND_LOGGING,
		(char *)CLSNMPHOST_SUBCMD_ADD,
		CL_AUTH_CLUSTER_ADMIN,
		clsnmphost_add_usage,
		clsnmphost_add_optionslist,
		clsnmphost_add_options
	},
	{
		"remove",
		CLSUB_DO_COMMAND_LOGGING,
		(char *)CLSNMPHOST_SUBCMD_REMOVE,
		CL_AUTH_CLUSTER_ADMIN,
		clsnmphost_remove_usage,
		clsnmphost_remove_optionslist,
		clsnmphost_remove_options
	},
	{
		"export",
		CLSUB_DEFAULT,
		(char *)CLSNMPHOST_SUBCMD_EXPORT,
		CL_AUTH_CLUSTER_READ,
		clsnmphost_export_usage,
		clsnmphost_export_optionslist,
		clsnmphost_export_options
	},
	{
		"list",
		CLSUB_DEFAULT,
		(char *)CLSNMPHOST_SUBCMD_LIST,
		CL_AUTH_CLUSTER_READ,
		clsnmphost_list_usage,
		clsnmphost_list_optionslist,
		clsnmphost_list_options
	},
	{
		"show",
		CLSUB_DEFAULT,
		(char *)CLSNMPHOST_SUBCMD_SHOW,
		CL_AUTH_CLUSTER_READ,
		clsnmphost_show_usage,
		clsnmphost_show_optionslist,
		clsnmphost_show_options
	},
	{0, 0, 0, 0, 0, 0, 0}
};

// Initialization properties for the "clsnmphost" command
clcommandinit_t clsnmphostinit = {
	CLCOMMANDS_COMMON_VERSION,
	(char *)CLSNMPHOST_CMD_DESC,
	clsnmphost_usage,
	clsnmphost_subcommands
};
