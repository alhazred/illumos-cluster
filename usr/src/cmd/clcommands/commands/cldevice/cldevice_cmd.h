//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef	_CLDEVICE_CMD_H
#define	_CLDEVICE_CMD_H

#pragma ident	"@(#)cldevice_cmd.h	1.3	08/05/20 SMI"

#include "clcommands.h"
#include "cldevice.h"

//
// This is the main header file for cldevice functions
//

// Tabble text which needs to be translated
#define	CLDEV_SUBCMD_CHECK	\
	GETTEXT("Check to ensure device consistency")
#define	CLDEV_SUBCMD_CLEAR	\
	GETTEXT("Clear all DID references to stale devices")
#define	CLDEV_SUBCMD_COMBINE	\
	GETTEXT("Combine two device instances")
#define	CLDEV_SUBCMD_EXPORT	\
	GETTEXT("Export cluster device configuration")
#define	CLDEV_SUBCMD_LIST	\
	GETTEXT("List cluster devices")
#define	CLDEV_SUBCMD_MONITOR	\
	GETTEXT("Enable monitoring of disk paths")
#define	CLDEV_SUBCMD_POPULATE	\
	GETTEXT("Populate the global-devices namespace")
#define	CLDEV_SUBCMD_REFRESH	\
	GETTEXT("Refresh the device configuration")
#define	CLDEV_SUBCMD_RENAME	\
	GETTEXT("Rename a device instance")
#define	CLDEV_SUBCMD_REPAIR	\
	GETTEXT("Repair device instances")
#define	CLDEV_SUBCMD_REPLICATE	\
	GETTEXT("Replicate and combine device instances")
#define	CLDEV_SUBCMD_SET	\
	GETTEXT("Set device properties")
#define	CLDEV_SUBCMD_SHOW	\
	GETTEXT("Show device properties")
#define	CLDEV_SUBCMD_STATUS	\
	GETTEXT("Display the status of monitored devices")
#define	CLDEV_SUBCMD_UNMONITOR	\
	GETTEXT("Disable monitoring of disk paths")
#define	CLDEV_CMD_DESC		\
	GETTEXT("Administer Sun Cluster device instances")


extern clcommandinit_t cldeviceinit;

// Subcommand options
extern clerrno_t cldevice_check_options(ClCommand &cmd);
extern clerrno_t cldevice_clear_options(ClCommand &cmd);
extern clerrno_t cldevice_combine_options(ClCommand &cmd);
extern clerrno_t cldevice_export_options(ClCommand &cmd);
extern clerrno_t cldevice_list_options(ClCommand &cmd);
extern clerrno_t cldevice_monitor_options(ClCommand &cmd);
extern clerrno_t cldevice_populate_options(ClCommand &cmd);
extern clerrno_t cldevice_refresh_options(ClCommand &cmd);
extern clerrno_t cldevice_rename_options(ClCommand &cmd);
extern clerrno_t cldevice_repair_options(ClCommand &cmd);
extern clerrno_t cldevice_replicate_options(ClCommand &cmd);
extern clerrno_t cldevice_set_options(ClCommand &cmd);
extern clerrno_t cldevice_show_options(ClCommand &cmd);
extern clerrno_t cldevice_status_options(ClCommand &cmd);
extern clerrno_t cldevice_unmonitor_options(ClCommand &cmd);

#endif /* _CLDEVICE_CMD_H */
