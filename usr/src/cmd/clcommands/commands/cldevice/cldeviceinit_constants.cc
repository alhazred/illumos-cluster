//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)cldeviceinit_constants.cc	1.8	09/02/12 SMI"

//
// Initialization data for cldevice(1CL)
//

#include "cldevice_cmd.h"

extern clcommandinit_t cldeviceinit;

//
// Usage Messages
//
// All usage message lines should begin with a single "%s" for
// the command name.   Lines must never include more than a single "%s".
//

// List of "cldevice" command usage message
static char *cldevice_usage [] = {
	"%s <subcommand> [<options>] [+ | <device> ...]",
	"%s [<subcommand>]  -? | --help",
	"%s  -V | --version",
	(char *)0
};

// List of cldevice "check" subcommand usage messages
static char *cldevice_check_usage[] = {
	"%s check [<options>] [+]",
	(char *)0
};

// List of cldevice "clear" subcommand usage messages
static char *cldevice_clear_usage[] = {
	"%s clear [<options>] [+]",
	(char *)0
};

// List of cldevice "combine" subcommand usage messages
static char *cldevice_combine_usage[] = {
	"%s combine [<options>] -d <destination-device> <device>",
	(char *)0
};

// List of cldevice "export" subcommand usage messages
static char *cldevice_export_usage[] = {
	"%s export [<options>] [+ | <device> ...]",
	(char *)0
};

// List of cldevice "list" subcommand usage messages
static char *cldevice_list_usage[] = {
	"%s list [<options>] [+ | <device> ...]",
	(char *)0
};

// List of cldevice "monitor" subcommand usage messages
static char *cldevice_monitor_usage[] = {
	"%s monitor [<options>] {+ | <device> ...}",
	(char *)0
};

// List of cldevice "populate" subcommand usage messages
static char *cldevice_populate_usage[] = {
	"%s populate [<options>]",
	(char *)0
};

// List of cldevice "refresh" subcommand usage messages
static char *cldevice_refresh_usage[] = {
	"%s refresh [<options>] [+]",
	(char *)0
};

// List of cldevice "rename" subcommand usage messages
static char *cldevice_rename_usage[] = {
	"%s rename -d <destination-device> <device>",
	(char *)0
};

// List of cldevice "repair" subcommand usage messages
static char *cldevice_repair_usage[] = {
	"%s repair [<options>] {+ | <device> ...}",
	(char *)0
};

// List of cldevice "replicate" subcommand usage messages
static char *cldevice_replicate_usage[] = {
	"%s replicate [<options>] -D <destination-node> [+]",
	(char *)0
};

// List of cldevice "set" subcommand usage messages
static char *cldevice_set_usage[] = {
	"%s set [<options>] -p <name>=<value> {+ | <device> ...}",
	(char *)0
};

// List of cldevice "show" subcommand usage messages
static char *cldevice_show_usage[] = {
	"%s show [<options>] [+ | <device> ...]",
	(char *)0
};

// List of cldevice "status" subcommand usage messages
static char *cldevice_status_usage[] = {
	"%s status [<options>] [+ | <device> ...]",
	(char *)0
};

// List of cldevice "unmonitor" subcommand usage messages
static char *cldevice_unmonitor_usage[] = {
	"%s unmonitor [<options>] {+ | <device> ...}",
	(char *)0
};

//
// Option lists
//
// All option descriptions should be broken up into lines of no
// greater than 70 characters.  All but the trailing newline ('\n')
// itself should be included in the option descriptions found in these
// tables.
//

// List of options for the cldevice "check" subcommand
static cloptions_t cldevice_check_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'n', "node", "<node>", CLOPT_ARG_LIST,
	    CLDES_OPT_NODE
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the cldevice "clear" subcommand
static cloptions_t cldevice_clear_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'n', "node", "<node>", CLOPT_ARG_LIST,
	    CLDES_OPT_NODE
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the cldevice "combine" subcommand
static cloptions_t cldevice_combine_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'d', "device", "<destination-device>", 0,
	    CLDES_OPT_DESTDEVICE
	},
	{'t', "replicationtype", "<replication-type>", 0,
	    CLDES_OPT_REPLTYPE
	},
	{'g', "replicationdevicegroup", "<replication-device-group>", 0,
	    CLDES_OPT_REPLDEV
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the cldevice "export" subcommand
static cloptions_t cldevice_export_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'n', "node", "<node>", CLOPT_ARG_LIST,
	    CLDES_OPT_NODE
	},
	{'o', "output", "{- | <clconfiguration>}", 0,
	    CLDES_OPT_OUTPUT
	},
	{0, 0, 0, 0, 0}
};

// List of options for the cldevice "list" subcommand
static cloptions_t cldevice_list_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'n', "node", "<node>", CLOPT_ARG_LIST,
	    CLDES_OPT_NODE
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the cldevice "set" subcommand
static cloptions_t cldevice_set_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'p', "property", "<name>=<value>", 0,
	    CLDES_OPT_PROPERTY
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the cldevice "monitor" subcommand
static cloptions_t cldevice_monitor_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'i', "input", "{- | <clconfiguration>}", 0,
	    CLDES_OPT_INPUT
	},
	{'n', "node", "<node>", CLOPT_ARG_LIST,
	    CLDES_OPT_NODE
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the cldevice "populate" subcommand
static cloptions_t cldevice_populate_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the cldevice "refresh" subcommand
static cloptions_t cldevice_refresh_optionslist[] = {
	{'?', "help", 0, 0,
	CLDES_OPT_HELP
	},
	{'n', "node", "<node>", CLOPT_ARG_LIST,
	    CLDES_OPT_NODE
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the cldevice "rename" subcommand
static cloptions_t cldevice_rename_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'d', "device", "<destination-device>", 0,
	    CLDES_OPT_DESTDEVICE
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the cldevice "repair" subcommand
static cloptions_t cldevice_repair_optionslist[] = {
	{'?', "help", 0, 0,
	CLDES_OPT_HELP
	},
	{'n', "node", "<node>", CLOPT_ARG_LIST,
	    CLDES_OPT_NODE
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the cldevice "replicate" subcommand
static cloptions_t cldevice_replicate_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'D', "destinationnode", "<destination-node>", 0,
	    CLDES_OPT_DESTNODE
	},
	{'t', "replicationtype", "<replication-type>", 0,
	    CLDES_OPT_REPLTYPE
	},
	{'S', "sourcenode", "<source-node>", 0,
	    CLDES_OPT_SRCNODE
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the cldevice "show" subcommand
static cloptions_t cldevice_show_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'n', "node", "<node>", CLOPT_ARG_LIST,
	    CLDES_OPT_NODE
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the cldevice "status" subcommand
static cloptions_t cldevice_status_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'n', "node", "<node>", CLOPT_ARG_LIST,
	    CLDES_OPT_NODE
	},
	{'s', "state", "<state>", CLOPT_ARG_LIST,
	    CLDES_OPT_DEV_STATE
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the cldevice "unmonitor" subcommand
static cloptions_t cldevice_unmonitor_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'i', "input", "{- | <clconfiguration>}", 0,
	    CLDES_OPT_INPUT
	},
	{'n', "node", "<node>", CLOPT_ARG_LIST,
	    CLDES_OPT_NODE
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of subcommands for the "cldevice" command
static clsubcommand_t cldevice_subcommands[] = {
	{
		"check",
		CLSUB_DEFAULT,
		(char *)CLDEV_SUBCMD_CHECK,
		CL_AUTH_CLUSTER_READ,
		cldevice_check_usage,
		cldevice_check_optionslist,
		cldevice_check_options
	},
	{
		"clear",
		CLSUB_DO_COMMAND_LOGGING,
		(char *)CLDEV_SUBCMD_CLEAR,
		CL_AUTH_CLUSTER_MODIFY,
		cldevice_clear_usage,
		cldevice_clear_optionslist,
		cldevice_clear_options
	},
	{
		"combine",
		CLSUB_DO_COMMAND_LOGGING,
		(char *)CLDEV_SUBCMD_COMBINE,
		CL_AUTH_CLUSTER_MODIFY,
		cldevice_combine_usage,
		cldevice_combine_optionslist,
		cldevice_combine_options
	},
	{
		"export",
		CLSUB_DEFAULT,
		(char *)CLDEV_SUBCMD_EXPORT,
		CL_AUTH_CLUSTER_READ,
		cldevice_export_usage,
		cldevice_export_optionslist,
		cldevice_export_options
	},
	{
		"list",
		CLSUB_NCLMODE_ALLOWED,
		(char *)CLDEV_SUBCMD_LIST,
		CL_AUTH_CLUSTER_READ,
		cldevice_list_usage,
		cldevice_list_optionslist,
		cldevice_list_options
	},
	{
		"set",
		CLSUB_DO_COMMAND_LOGGING,
		(char *)CLDEV_SUBCMD_SET,
		CL_AUTH_CLUSTER_MODIFY,
		cldevice_set_usage,
		cldevice_set_optionslist,
		cldevice_set_options
	},
	{
		"monitor",
		CLSUB_DO_COMMAND_LOGGING,
		(char *)CLDEV_SUBCMD_MONITOR,
		CL_AUTH_CLUSTER_MODIFY,
		cldevice_monitor_usage,
		cldevice_monitor_optionslist,
		cldevice_monitor_options
	},
	{
		"populate",
		CLSUB_DO_COMMAND_LOGGING,
		(char *)CLDEV_SUBCMD_POPULATE,
		CL_AUTH_CLUSTER_MODIFY,
		cldevice_populate_usage,
		cldevice_populate_optionslist,
		cldevice_populate_options
	},
	{
		"refresh",
		CLSUB_DO_COMMAND_LOGGING,
		(char *)CLDEV_SUBCMD_REFRESH,
		CL_AUTH_CLUSTER_MODIFY,
		cldevice_refresh_usage,
		cldevice_refresh_optionslist,
		cldevice_refresh_options
	},
	{
		"rename",
		CLSUB_DO_COMMAND_LOGGING,
		(char *)CLDEV_SUBCMD_RENAME,
		CL_AUTH_CLUSTER_MODIFY,
		cldevice_rename_usage,
		cldevice_rename_optionslist,
		cldevice_rename_options
	},
	{
		"repair",
		CLSUB_DO_COMMAND_LOGGING,
		(char *)CLDEV_SUBCMD_REPAIR,
		CL_AUTH_CLUSTER_MODIFY,
		cldevice_repair_usage,
		cldevice_repair_optionslist,
		cldevice_repair_options
	},
	{
		"replicate",
		CLSUB_DO_COMMAND_LOGGING,
		(char *)CLDEV_SUBCMD_REPLICATE,
		CL_AUTH_CLUSTER_MODIFY,
		cldevice_replicate_usage,
		cldevice_replicate_optionslist,
		cldevice_replicate_options
	},
	{
		"show",
		CLSUB_NCLMODE_ALLOWED,
		(char *)CLDEV_SUBCMD_SHOW,
		CL_AUTH_CLUSTER_READ,
		cldevice_show_usage,
		cldevice_show_optionslist,
		cldevice_show_options
	},
	{
		"status",
		CLSUB_DEFAULT,
		(char *)CLDEV_SUBCMD_STATUS,
		CL_AUTH_CLUSTER_READ,
		cldevice_status_usage,
		cldevice_status_optionslist,
		cldevice_status_options
	},
	{
		"unmonitor",
		CLSUB_DO_COMMAND_LOGGING,
		(char *)CLDEV_SUBCMD_UNMONITOR,
		CL_AUTH_CLUSTER_MODIFY,
		cldevice_unmonitor_usage,
		cldevice_unmonitor_optionslist,
		cldevice_unmonitor_options
	},
	{0, 0, 0, 0, 0, 0, 0}
};

// Initialization properties for the "cldevice" command
clcommandinit_t cldeviceinit = {
	CLCOMMANDS_COMMON_VERSION,
	(char *)CLDEV_CMD_DESC,
	cldevice_usage,
	cldevice_subcommands
};
