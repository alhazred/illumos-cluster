//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident   "@(#)cldevice_rename_options.cc	1.2	08/05/20 SMI"

//
// Process cldevice "rename" subcommand options
//

#include "cldevice_cmd.h"

//
// cldevice "rename" options
//
clerrno_t
cldevice_rename_options(ClCommand &cmd)
{
	int i;
	int c;
	int wildcard;
	int errcount = 0;
	int destdev_count = 0;
	char *option = 0;
	int option_index = 0;
	char *shortopts;
	struct option *longopts;
	ValueList operands = ValueList(true);
	char *destdev = (char *)0;
	optflgs_t optflgs = 0;

	// Get the short and long option renames
	shortopts = clsubcommand_get_shortoptions(cmd);
	longopts = clsubcommand_get_longoptions(cmd);

	// This subcommand should have options
	if (!shortopts || !longopts) {
		clerror("Aborting - subcommand initialization failed.\n");
		abort();
	}

	optind = 2;
	while ((c = getopt_clip(cmd.argc, cmd.argv, shortopts, longopts,
	    &option_index)) != -1) {

		// Get the option
		delete option;
		option = clcommand_option(optopt, cmd.argv[optind - 1]);

		// Switch on option letter
		switch (c) {
		case 'v':
			// Verbose option
			cmd.isVerbose = 1;
			break;

		case '?':
			// Help option?
			if ((strcmp(option, "-?") == 0) ||
			    (strcmp(option, "--help") == 0)) {
				optflgs |= hflg;
				break;
			}

			// Or, unrecognized option?
			clerror("Unrecognized option - \"%s\".\n", option);
			++errcount;
			break;

		case 'd':
			// Destination device
			destdev = optarg;
			destdev_count++;
			break;

		case ':':
			// Missing argument
			clerror("Option \"%s\" requires an argument.\n",
			    option);
			++errcount;
			break;

		default:
			//
			// We should never get here.
			// If we do, it means that there
			// is an option in our option rename
			// for which there is no case statement.
			// We either have a bad option rename or
			// a bad case statement.
			//
			clerror("Unexpected option - \"%s\".\n", option);
			++errcount;
			break;
		}
	}
	delete option;
	option = (char *)0;

	// Errors?
	if (errcount) {
		clcommand_usage(cmd, cmd.getSubCommandName(), errcount);
		return (CL_EINVAL);
	}

	// Just print help?
	if (optflgs & hflg) {
		clcommand_help(cmd, cmd.getSubCommandName());
		return (CL_NOERR);
	}

	// No destination device specified
	if (destdev_count == 0) {
		++errcount;
		clerror("You must specify a destination device.\n");
	}

	// More than one destination device
	if (destdev_count > 1) {
		++errcount;
		clerror("You cannot specify more than one destination "
		    "device.\n");
	}

	// Update the operands list.
	wildcard = 0;
	for (i = optind;  i < cmd.argc;  ++i) {
		if (strcmp(cmd.argv[i], CLCOMMANDS_WILD_OPERAND) == 0) {
			++wildcard;
		} else {
			operands.add(cmd.argv[i]);
		}
	}

	// Wildcards are not allowed.
	if (wildcard) {
		++errcount;
		clerror("The operand \"%s\" is not allowed.\n",
		    CLCOMMANDS_WILD_OPERAND);
	}

	// An operand must be specified
	if (operands.getSize() == 0) {
		++errcount;
		clerror("You must specify the name of a device instance.\n");
	}

	// Only one operand allowed
	if (operands.getSize() > 1) {
		++errcount;
		clerror("You cannot specify more than one device instance.\n");
	}

	if (errcount) {
		clcommand_usage(cmd, cmd.getSubCommandName(), errcount);
		return (CL_EINVAL);
	}

	// Perform cldevice "rename" operation
	operands.start();
	return (cldevice_rename(cmd, operands.getValue(), destdev));
}
