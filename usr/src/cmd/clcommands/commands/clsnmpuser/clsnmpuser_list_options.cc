//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident   "@(#)clsnmpuser_list_options.cc	1.3	08/05/20 SMI"

//
// Process clsnmpuser "list" subcommand options
//

#include "clsnmpuser_cmd.h"

//
// clsnmpuser "list" options
//
clerrno_t
clsnmpuser_list_options(ClCommand &cmd)
{
	int i;
	int c;
	int wildcard;
	int errcount = 0;
	char *option = 0;
	int option_index = 0;
	char *shortopts;
	struct option *longopts;
	ValueList operands = ValueList(true);
	ValueList nodes = ValueList(true);
	ValueList auth = ValueList(true);
	optflgs_t optflgs = 0;

	// Get the short and long option lists
	shortopts = clsubcommand_get_shortoptions(cmd);
	longopts = clsubcommand_get_longoptions(cmd);

	// This subcommand should have options
	if (!shortopts || !longopts) {
		clerror("Aborting - subcommand initialization failed.\n");
		abort();
	}

	optind = 2;
	while ((c = getopt_clip(cmd.argc, cmd.argv, shortopts, longopts,
	    &option_index)) != -1) {

		// Get the option
		delete option;
		option = clcommand_option(optopt, cmd.argv[optind - 1]);

		// Switch on option letter
		switch (c) {
		case 'v':
			// Verbose option
			cmd.isVerbose = 1;
			optflgs |= vflg;
			break;

		case '?':
			// Help option?
			if ((strcmp(option, "-?") == 0) ||
			    (strcmp(option, "--help") == 0)) {
				optflgs |= hflg;
				break;
			}

			// Or, unrecognized option?
			clerror("Unrecognized option - \"%s\".\n", option);
			++errcount;
			break;

		case 'a':
			// Authentication protocol
			if (strcasecmp(optarg, "MD5") == 0) {

				auth.add((char *)"MD5");
				break;

			} else if (strcasecmp(optarg, "SHA") == 0) {

				auth.add((char *)"SHA");
				break;
			}

			// Or, unrecognized protocol
			clerror("Invalid authentication protocol - \"%s\".\n",
			    optarg);
			++errcount;
			break;

		case 'd':
			// Default
			optflgs |= dflg;
			break;

		case 'n':
			// Nodes
			nodes.add(1, optarg);
			break;

		case ':':
			// Missing argument
			clerror("Option \"%s\" requires an argument.\n",
			    option);
			++errcount;
			break;

		default:
			//
			// We should never get here.
			// If we do, it means that there
			// is an option in our option list
			// for which there is no case statement.
			// We either have a bad option list or
			// a bad case statement.
			//
			clerror("Unexpected option - \"%s\".\n", option);
			++errcount;
			break;
		}
	}
	delete option;
	option = (char *)0;

	// Errors?
	if (errcount) {
		clcommand_usage(cmd, cmd.getSubCommandName(), errcount);
		return (CL_EINVAL);
	}

	// Just print help?
	if (optflgs & hflg) {
		clcommand_help(cmd, cmd.getSubCommandName());
		return (CL_NOERR);
	}

	// Update the operands list.  Don't add the wildcard to the operands
	wildcard = 0;
	for (i = optind;  i < cmd.argc;  ++i) {
		if (strcmp(cmd.argv[i], CLCOMMANDS_WILD_OPERAND) == 0) {
			++wildcard;
		} else {
			operands.add(cmd.argv[i]);
		}
	}

	// wildcard and other operands cannot be specified together
	if (wildcard && operands.getSize() > 0) {
		++errcount;
		clerror("You cannot specify both \"%s\" and SNMP users at "
		    "the same time.\n", CLCOMMANDS_WILD_OPERAND);
	}

	// Default flag and operands
	if (optflgs & dflg && (wildcard || operands.getSize() > 0)) {
		++errcount;
		clerror("You cannot specify \"-d\" along with any operands.\n");
	}

	// Default flag and auth types
	if (optflgs & dflg && auth.getSize() > 0) {
		++errcount;
		clerror("You cannot specify \"-d\" and "
		    "\"-a\" at the same time.\n");
	}

	if (errcount) {
		clcommand_usage(cmd, cmd.getSubCommandName(), errcount);
		return (CL_EINVAL);
	}

	// Perform clsnmpuser "list" operation
	return (clsnmpuser_list(cmd, operands, nodes,
	    auth, optflgs & dflg));
}
