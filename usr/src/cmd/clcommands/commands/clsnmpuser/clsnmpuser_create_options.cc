//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clsnmpuser_create_options.cc	1.6	08/07/24 SMI"

//
// Process clsnmpuser "create" subcommand options
//

#include "clsnmpuser_cmd.h"

//
// clsnmpuser "create" options
//
clerrno_t
clsnmpuser_create_options(ClCommand &cmd)
{
	int i;
	int c;
	int wildcard;
	int iflg = 0;
	int f_flg = 0;
	int errcount = 0;
	char *option = 0;
	int option_index = 0;
	char *shortopts;
	struct option *longopts;
	ValueList operands = ValueList(true);
	ValueList nodes = ValueList(true);
	ValueList auth = ValueList(true);
	char *clconfig = (char *)0;
	char *passfile = (char *)0;
	optflgs_t optflgs = 0;

	// Get the short and long option lists
	shortopts = clsubcommand_get_shortoptions(cmd);
	longopts = clsubcommand_get_longoptions(cmd);

	// This subcommand should have options
	if (!shortopts || !longopts) {
		clerror("Aborting - subcommand initialization failed.\n");
		abort();
	}

	optind = 2;
	while ((c = getopt_clip(cmd.argc, cmd.argv, shortopts, longopts,
	    &option_index)) != -1) {

		// Get the option
		delete option;
		option = clcommand_option(optopt, cmd.argv[optind - 1]);

		// Switch on option letter
		switch (c) {
		case 'v':
			// Verbose option
			cmd.isVerbose = 1;
			optflgs |= vflg;
			break;

		case '?':
			// Help option?
			if ((strcmp(option, "-?") == 0) ||
			    (strcmp(option, "--help") == 0)) {
				optflgs |= hflg;
				break;
			}

			// Or, unrecognized option?
			clerror("Unrecognized option - \"%s\".\n", option);
			++errcount;
			break;

		case 'a':
			// Authentication protocol
			if (strcasecmp(optarg, "MD5") == 0) {

				auth.add("MD5");
				break;

			} else if (strcasecmp(optarg, "SHA") == 0) {

				auth.add("SHA");
				break;
			}

			// Or, unrecognized protocol
			clerror("Invalid authentication protocol - \"%s\".\n",
			    optarg);
			++errcount;
			break;

		case 'f':
			// Passfile
			passfile = optarg;
			f_flg++;
			break;

		case 'i':
			// Input file
			clconfig = optarg;
			iflg++;
			break;

		case 'n':
			// Nodes
			nodes.add(1, optarg);
			break;

		case ':':
			// Missing argument
			clerror("Option \"%s\" requires an argument.\n",
			    option);
			++errcount;
			break;

		default:
			//
			// We should never get here.
			// If we do, it means that there
			// is an option in our option list
			// for which there is no case statement.
			// We either have a bad option list or
			// a bad case statement.
			//
			clerror("Unexpected option - \"%s\".\n", option);
			++errcount;
			break;
		}
	}
	delete option;
	option = (char *)0;

	// Errors?
	if (errcount) {
		clcommand_usage(cmd, cmd.getSubCommandName(), errcount);
		return (CL_EINVAL);
	}

	// Just print help?
	if (optflgs & hflg) {
		clcommand_help(cmd, cmd.getSubCommandName());
		return (CL_NOERR);
	}

	// Too many input files
	if (iflg > 1) {
		clerror("You can only specify the \"-i\" option once.\n");
		++errcount;
	}

	// Too many password files
	if (f_flg > 1) {
		clerror("You can only specify the \"-f\" option once.\n");
		++errcount;
	}

	// Update the operands list.  Don't add the wildcard to the operands
	wildcard = 0;
	for (i = optind;  i < cmd.argc;  ++i) {
		if (strcmp(cmd.argv[i], CLCOMMANDS_WILD_OPERAND) == 0) {
			++wildcard;
		} else {
			operands.add(cmd.argv[i]);
		}
	}

	// wildcard and other operands cannot be specified together
	if (wildcard && operands.getSize() > 0) {
		++errcount;
		clerror("You cannot specify both \"%s\" and SNMP users at "
		    "the same time.\n", CLCOMMANDS_WILD_OPERAND);
	}

	// There must be at least one snmpuser
	if (operands.getSize() == 0 && !wildcard) {
		++errcount;
		clerror("You must specify either \"%s\" or a "
		    "list of SNMP users to create.\n",
		    CLCOMMANDS_WILD_OPERAND);
	}

	// wildcard and no configfile
	if (wildcard && !clconfig) {
		++errcount;
		clerror("You cannot specify a \"%s\" without the \"-i\" "
		    "option.", CLCOMMANDS_WILD_OPERAND);
	}

	// wildcard and default option
	if (wildcard && (optflgs & dflg)) {
		++errcount;
		clerror("You cannot use the default flag with the \"%s\" "
		    "operand.\n", CLCOMMANDS_WILD_OPERAND);
	}

	// config file and no passfile
	if (clconfig && !passfile) {
		++errcount;
		clerror("A password file is required when you use "
		    "the \"-i\" option.\n");
	}

	// no config file and no auth protocol
	if (!clconfig && auth.getSize() == 0) {
		++errcount;
		clerror("You must specify an authentication protocol.\n");
	}

	// no config file and too many auth protocols
	if (!clconfig && auth.getSize() > 1) {
		++errcount;
		clerror("You can specify only one authentication protocol.\n");
	}

	// Try to open the passwd file
	if (passfile) {
		FILE *file = fopen(passfile, "r");
		if (file == NULL) {
			++errcount;
			clerror("Cannot read the given password file "
			    "\"%s\".\n", passfile);
		} else {
			// Now close the file
			(void) fclose(file);
		}
	}

	if (errcount) {
		clcommand_usage(cmd, cmd.getSubCommandName(), errcount);
		return (CL_EINVAL);
	}

	// Perform clsnmpuser "create" operation
	if (!clconfig) {
		auth.start();

		return (clsnmpuser_create(cmd, operands, nodes, auth.getValue(),
		    passfile));
	} else {
		return (clsnmpuser_create(cmd, operands, nodes, auth,
		    passfile, clconfig));
	}
}
