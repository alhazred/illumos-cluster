//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident   "@(#)clsnmpuserinit_constants.cc	1.4	08/05/20 SMI"

//
// Initialization data for clsnmpuser(1CL)
//

#include "clsnmpuser_cmd.h"

extern clcommandinit_t clsnmpuserinit;

//
// Usage Messages
//
// All usage message lines should begin with a single "%s" for
// the command name.   Lines must never include more than a single "%s".
//

// List of "clsnmpuser" command usage message
static char *clsnmpuser_usage [] = {
	"%s <subcommand> [<options>] [+ | <snmpuser> ...]",
	"%s [<subcommand>]  -? | --help",
	"%s  -V | --version",
	(char *)0
};

// List of clsnmpuser "create" subcommand usage messages
static char *clsnmpuser_create_usage[] = {
	"%s create [<options>] + | <snmpuser> ...",
	(char *)0
};

// List of clsnmpuser "export" subcommand usage messages
static char *clsnmpuser_export_usage[] = {
	"%s export [<options>] [+ | <snmpuser> ...]",
	(char *)0
};

// List of clsnmpuser "list" subcommand usage messages
static char *clsnmpuser_list_usage[] = {
	"%s list [<options>] [+ | <snmpuser> ...]",
	"%s list [<options>] -d",
	(char *)0
};

// List of clsnmpuser "set" subcommand usage messages
static char *clsnmpuser_set_usage[] = {
	"%s set -a <authProtocol> [+ | <snmpuser> ...]",
	(char *)0
};

// List of clsnmpuser "set-default" subcommand usage messages
static char *clsnmpuser_set_default_usage[] = {
	"%s set-default -l <secLevel> [<options>] <snmpuser>",
	(char *)0
};

// List of clsnmpuser "delete" subcommand usage messages
static char *clsnmpuser_delete_usage[] = {
	"%s delete [<options>] + | <snmpuser> ...",
	(char *)0
};

// List of clsnmpuser "show" subcommand usage messages
static char *clsnmpuser_show_usage[] = {
	"%s show [<options>] [+ | <snmpuser> ...]",
	(char *)0
};

//
// Option lists
//
// All option descriptions should be broken up into lines of no
// greater than 70 characters.  All but the trailing newline ('\n')
// itself should be included in the option descriptions found in these
// tables.
//

// List of options for the clsnmpuser "create" subcommand
static cloptions_t clsnmpuser_create_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'a', "authentication", "<authentication>", 0,
	    CLDES_OPT_SNMP_AUTH
	},
	{'f', "file", "<passwdfile>", 0,
	    CLDES_OPT_PASSWD
	},
	{'i', "input", "{- | <clconfiguration>}", 0,
	    CLDES_OPT_INPUT
	},
	{'n', "node", "<node>", CLOPT_ARG_LIST,
	    CLDES_OPT_NODE
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the clsnmpuser "delete" subcommand
static cloptions_t clsnmpuser_delete_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'a', "authentication", "<authentication>", 0,
	    CLDES_OPT_SNMP_AUTH
	},
	{'n', "node", "<node>", CLOPT_ARG_LIST,
	    CLDES_OPT_NODE
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the clsnmpuser "export" subcommand
static cloptions_t clsnmpuser_export_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'a', "authentication", "<authentication>", 0,
	    CLDES_OPT_SNMP_AUTH
	},
	{'n', "node", "<node>", CLOPT_ARG_LIST,
	    CLDES_OPT_NODE
	},
	{'o', "output", "{- | <clconfiguration>}", 0,
	    CLDES_OPT_OUTPUT
	},
	{0, 0, 0, 0, 0}
};

// List of options for the clsnmpuser "list" subcommand
static cloptions_t clsnmpuser_list_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'a', "authentication", "<authentication>", 0,
	    CLDES_OPT_SNMP_AUTH
	},
	{'d', "default", 0, 0,
	    CLDES_OPT_DEFAULT
	},
	{'n', "node", "<node>", CLOPT_ARG_LIST,
	    CLDES_OPT_NODE
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the clsnmpuser "set" subcommand
static cloptions_t clsnmpuser_set_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'a', "authentication", "<authentication>", 0,
	    CLDES_OPT_SNMP_AUTH
	},
	{'n', "node", "<node>", CLOPT_ARG_LIST,
	    CLDES_OPT_NODE
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the clsnmpuser "set-default" subcommand
static cloptions_t clsnmpuser_set_default_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'l', "securitylevel", "<seclevel>", 0,
	    CLDES_OPT_SECLEVEL
	},
	{'n', "node", "<node>", CLOPT_ARG_LIST,
	    CLDES_OPT_NODE
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the clsnmpuser "show" subcommand
static cloptions_t clsnmpuser_show_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'a', "authentication", "<authentication>", 0,
	    CLDES_OPT_SNMP_AUTH
	},
	{'d', "default", 0, 0,
	    CLDES_OPT_DEFAULT
	},
	{'n', "node", "<node>", CLOPT_ARG_LIST,
	    CLDES_OPT_NODE
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of subcommands for the "clsnmpuser" command
static clsubcommand_t clsnmpuser_subcommands[] = {
	{
		"create",
		CLSUB_DO_COMMAND_LOGGING,
		(char *)CLSNMPUSER_SUBCMD_CREATE,
		CL_AUTH_CLUSTER_MODIFY,
		clsnmpuser_create_usage,
		clsnmpuser_create_optionslist,
		clsnmpuser_create_options
	},
	{
		"delete",
		CLSUB_DO_COMMAND_LOGGING,
		(char *)CLSNMPUSER_SUBCMD_DELETE,
		CL_AUTH_CLUSTER_MODIFY,
		clsnmpuser_delete_usage,
		clsnmpuser_delete_optionslist,
		clsnmpuser_delete_options
	},
	{
		"export",
		CLSUB_DEFAULT,
		(char *)CLSNMPUSER_SUBCMD_EXPORT,
		CL_AUTH_CLUSTER_READ,
		clsnmpuser_export_usage,
		clsnmpuser_export_optionslist,
		clsnmpuser_export_options
	},
	{
		"list",
		CLSUB_DEFAULT,
		(char *)CLSNMPUSER_SUBCMD_LIST,
		CL_AUTH_CLUSTER_READ,
		clsnmpuser_list_usage,
		clsnmpuser_list_optionslist,
		clsnmpuser_list_options
	},
	{
		"set",
		CLSUB_DO_COMMAND_LOGGING,
		(char *)CLSNMPUSER_SUBCMD_SET,
		CL_AUTH_CLUSTER_MODIFY,
		clsnmpuser_set_usage,
		clsnmpuser_set_optionslist,
		clsnmpuser_set_options
	},
	{
		"set-default",
		CLSUB_DO_COMMAND_LOGGING,
		(char *)CLSNMPUSER_SUBCMD_SET_DEFAULT,
		CL_AUTH_CLUSTER_MODIFY,
		clsnmpuser_set_default_usage,
		clsnmpuser_set_default_optionslist,
		clsnmpuser_set_default_options
	},
	{
		"show",
		CLSUB_DEFAULT,
		(char *)CLSNMPUSER_SUBCMD_SHOW,
		CL_AUTH_CLUSTER_READ,
		clsnmpuser_show_usage,
		clsnmpuser_show_optionslist,
		clsnmpuser_show_options
	},
	{0, 0, 0, 0, 0, 0, 0}
};

// Initialization properties for the "clsnmpuser" command
clcommandinit_t clsnmpuserinit = {
	CLCOMMANDS_COMMON_VERSION,
	(char *)CLSNMPUSER_CMD_DESC,
	clsnmpuser_usage,
	clsnmpuser_subcommands
};
