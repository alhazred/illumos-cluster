//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef	_CLSNMPUSER_CMD_H
#define	_CLSNMPUSER_CMD_H

#pragma ident	"@(#)clsnmpuser_cmd.h	1.4	08/05/20 SMI"

#include "clcommands.h"
#include "clsnmpuser.h"

//
// This is the main header file for clsnmpuser options functions
//

#define	CLSNMPUSER_SUBCMD_CREATE	\
	GETTEXT("Create SNMP users")
#define	CLSNMPUSER_SUBCMD_DELETE	\
	GETTEXT("Delete SNMP users")
#define	CLSNMPUSER_SUBCMD_EXPORT	\
	GETTEXT("Export SNMP user configuration")
#define	CLSNMPUSER_SUBCMD_LIST		\
	GETTEXT("List SNMP users")
#define	CLSNMPUSER_SUBCMD_SET		\
	GETTEXT("Set SNMP user properties")
#define	CLSNMPUSER_SUBCMD_SET_DEFAULT	\
	GETTEXT("Set the default SNMP user")
#define	CLSNMPUSER_SUBCMD_SHOW		\
	GETTEXT("Show SNMP users and their properties")
#define	CLSNMPUSER_CMD_DESC		\
	GETTEXT("Administer Sun Cluster SNMP users")

extern clcommandinit_t clsnmpuserinit;

// Subcommand options
extern clerrno_t clsnmpuser_create_options(ClCommand &cmd);
extern clerrno_t clsnmpuser_delete_options(ClCommand &cmd);
extern clerrno_t clsnmpuser_export_options(ClCommand &cmd);
extern clerrno_t clsnmpuser_list_options(ClCommand &cmd);
extern clerrno_t clsnmpuser_set_options(ClCommand &cmd);
extern clerrno_t clsnmpuser_set_default_options(ClCommand &cmd);
extern clerrno_t clsnmpuser_show_options(ClCommand &cmd);

#endif /* _CLSNMPUSER_CMD_H */
