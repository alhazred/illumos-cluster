//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef _CLRT_H
#define	_CLRT_H

#pragma ident	"@(#)clrt.h	1.3	08/05/20 SMI"

//
// This is the header file for the clrt(1CL) command
//

#define	CLRT_SUBCMD_ADD_NODE	\
	GETTEXT("Add nodes to a resource type nodelist")
#define	CLRT_SUBCMD_EXPORT	\
	GETTEXT("Export resource type configuration")
#define	CLRT_SUBCMD_LIST	\
	GETTEXT("List resource types")
#define	CLRT_SUBCMD_LIST_PROPS	\
	GETTEXT("List resource type properties")
#define	CLRT_SUBCMD_REGISTER	\
	GETTEXT("Register resource types")
#define	CLRT_SUBCMD_REMOVE_NODE	\
	GETTEXT("Remove nodes from a resource type nodelist")
#define	CLRT_SUBCMD_SET		\
	GETTEXT("Set resource type properties")
#define	CLRT_SUBCMD_SHOW	\
	GETTEXT("Display resource types and their properties")
#define	CLRT_SUBCMD_UNREGISTER	\
	GETTEXT("Unregister resource types")
#define	CLRT_CMD_DESC		\
	GETTEXT("Administer resource types for Sun Cluster data services")

#include <sys/sol_version.h>

#include "clcommands.h"
#include "clresourcetype.h"

#include "ClCommand.h"

extern clcommandinit_t clrtinit;

// Subcommand options
extern int clrt_add_node_options(ClCommand &cmd);
extern int clrt_register_options(ClCommand &cmd);
extern int clrt_unregister_options(ClCommand &cmd);
extern int clrt_export_options(ClCommand &cmd);
extern int clrt_list_options(ClCommand &cmd);
extern int clrt_list_props_options(ClCommand &cmd);
extern int clrt_set_options(ClCommand &cmd);
extern int clrt_remove_node_options(ClCommand &cmd);
extern int clrt_show_options(ClCommand &cmd);

#endif /* _CLRT_H */
