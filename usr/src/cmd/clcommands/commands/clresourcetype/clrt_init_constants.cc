//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clrt_init_constants.cc	1.12	08/10/14 SMI"

//
// Initialization data for clresourcetype(1CL)
//

#include "clrt.h"

extern clcommandinit_t clrtinit;

//
// Usage Messages
//
// All usage message lines should begin with a single "%s" for
// the command name.   Lines must never include more than a single "%s".
//

// List of "clresourcetype" command usage message
static char *clrt_usage [] = {
	"%s <subcommand> [<options>] [+ | <resourcetype> ...]",
	"%s [<subcommand>]  -? | --help",
	"%s  -V | --version",
	(char *)0
};

// List of clresourcetype "add-node" subcommand usage messages
static char *clrt_add_node_usage[] = {
#if SOL_VERSION >= __s10
	"%s add-node [-z <zone>] -n <node>[:<zone>][,...] [<options>] "
	"+ | <resourcetype> ...",
#else
	"%s add-node -n <node>[,...] [<options>] + | <resourcetype> ...",
#endif
	(char *)0
};

// List of clresourcetype "register" subcommand usage messages
static char *clrt_register_usage[] = {
	"%s register [<options>] + | <resourcetype> ...",
	(char *)0
};

// List of clresourcetype "unregister" subcommand usage messages
static char *clrt_unregister_usage[] = {
	"%s unregister [<options>] + | <resourcetype> ...",
	(char *)0
};

// List of clresourcetype "export" subcommand usage messages
static char *clrt_export_usage[] = {
	"%s export [<options>] [+ | <resourcetype> ...]",
	(char *)0
};

// List of clrt "list" subcommand usage messages
static char *clrt_list_usage[] = {
	"%s list [<options>] [+ | <resourcetype> ...]",
	(char *)0
};

// List of clrt "list_props" subcommand usage messages
static char *clrt_list_props_usage[] = {
	"%s list-props [<options>] [+ | <resourcetype> ...]",
	(char *)0
};

// List of clresourcetype "set" subcommand usage messages
static char *clrt_set_usage[] = {
	"%s set <options> + | <resourcetype> ...",
	(char *)0
};

// List of clresourcetype "remove-node" subcommand usage messages
static char *clrt_remove_node_usage[] = {
#if SOL_VERSION >= __s10
	"%s remove-node [-z <zone>] -n <node>[:<zone>][,...] "
	"[<options>] [+ | <resourcetype> ...]",
#else
	"%s remove-node -n <node>[,...] [<options>] [+ | <resourcetype> ...]",
#endif
	(char *)0
};

// List of clresourcetype "show" subcommand usage messages
static char *clrt_show_usage[] = {
	"%s show [<options>] [+ | <resourcetype> ...]",
	(char *)0
};

//
// Option lists
//
// All option descriptions should be broken up into lines of no
// greater than 70 characters.  All but the trailing newline ('\n')
// itself should be included in the option descriptions found in these
// tables.
//

// List of options for the clresourcetype "add-node" subcommand
static cloptions_t clrt_add_node_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
#if SOL_VERSION >= __s10
	{'n', "node", "<node>[:<zone>][,...]", 0,
#else
	{'n', "node", "<node>[,...]", 0,
#endif
	    CLDES_OPT_NODE
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
#if SOL_VERSION >= __s10
	{'z', "zone", "<zone>", 0,
	    CLDES_OPT_ZONE
	},
	{'Z', "zonecluster", "{global | <zone-cluster-name>}", 0,
	    CLDES_OPT_ZC_NAME
	},
#endif
	{0, 0, 0, 0, 0}
};

// List of options for the clresourcetype "register" subcommand
static cloptions_t clrt_register_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'f', "rtrfile", "<rtrfile|rtrfiledir>", 0,
	    CLDES_OPT_RTRFILE
	},
	{'i', "input", "{- | <clconfiguration>}", 0,
	    CLDES_OPT_INPUT
	},
	{'N', "allnodes", 0, 0,
	    CLDES_OPT_ALLNODES
	},
#if SOL_VERSION >= __s10
	{'n', "node", "<node>[:<zone>][,...]", 0,
#else
	{'n', "node", "<node>[,...]", 0,
#endif
	    CLDES_OPT_NODE
	},
	{'p', "property", "<name>=<value>", 0,
	    CLDES_OPT_RT_PROPERTY
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
#if SOL_VERSION >= __s10
	{'z', "zone", "<zone>", 0,
	    CLDES_OPT_ZONE
	},
	{'Z', "zonecluster", "{global | <zone-cluster-name>}", 0,
	    CLDES_OPT_ZC_NAME
	},
#endif
	{0, 0, 0, 0, 0}
};

// List of options for the clresourcetype "unregister" subcommand
static cloptions_t clrt_unregister_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
#if SOL_VERSION >= __s10
	{'Z', "zonecluster", "{global | <zone-cluster-name>}", 0,
	    CLDES_OPT_ZC_NAME
	},
#endif
	{0, 0, 0, 0, 0}
};

// List of options for the clresourcetype "export" subcommand
static cloptions_t clrt_export_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{'o', "output", "{- | <clconfiguration>}", 0,
	    CLDES_OPT_OUTPUT
	},
	{0, 0, 0, 0, 0}
};

// List of options for the clresourcetype "list" subcommand
static cloptions_t clrt_list_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
#if SOL_VERSION >= __s10
	{'n', "node", "<node>[:<zone>][,...]", 0,
#else
	{'n', "node", "<node>[,...]", 0,
#endif
	    CLDES_OPT_NODE
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
#if SOL_VERSION >= __s10
	{'z', "zone", "<zone>", 0,
	    CLDES_OPT_ZONE
	},
	{'Z', "zonecluster", "{all | global | <zone-cluster-name>}", 0,
	    CLDES_OPT_ZC_NAME
	},
#endif
	{0, 0, 0, 0, 0}
};

// List of options for the clresourcetype "list-props" subcommand
static cloptions_t clrt_list_props_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'p', "property", "<name>", CLOPT_ARG_LIST,
	    CLDES_OPT_RT_PROPERTIES
	},
#if SOL_VERSION >= __s10
	{'Z', "zonecluster", "{all | global | <zone-cluster-name>}", 0,
	    CLDES_OPT_ZC_NAME
	},
#endif
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the clresourcetype "set" subcommand
static cloptions_t clrt_set_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'N', "allnodes", 0, 0,
	    CLDES_OPT_ALLNODES
	},
#if SOL_VERSION >= __s10
	{'n', "node", "<node>[:<zone>][,...]", 0,
#else
	{'n', "node", "<node>[,...]", 0,
#endif
	    CLDES_OPT_NODE
	},
	{'p', "property", "<name>=<value>", 0,
	    CLDES_OPT_RT_PROPERTY
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
#if SOL_VERSION >= __s10
	{'z', "zone", "<zone>", 0,
	    CLDES_OPT_ZONE
	},
	{'Z', "zonecluster", "{global | <zone-cluster-name>}", 0,
	    CLDES_OPT_ZC_NAME
	},
#endif
	{0, 0, 0, 0, 0}
};

// List of options for the clresourcetype "remove-node" subcommand
static cloptions_t clrt_remove_node_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
#if SOL_VERSION >= __s10
	{'n', "node", "<node>[:<zone>][,...]", 0,
#else
	{'n', "node", "<node>[,...]", 0,
#endif
	    CLDES_OPT_NODE
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
#if SOL_VERSION >= __s10
	{'z', "zone", "<zone>", 0,
	    CLDES_OPT_ZONE
	},
	{'Z', "zonecluster", "{global | <zone-cluster-name>}", 0,
	    CLDES_OPT_ZC_NAME
	},
#endif
	{0, 0, 0, 0, 0}
};

// List of options for the clresourcetype "show" subcommand
static cloptions_t clrt_show_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
#if SOL_VERSION >= __s10
	{'n', "node", "<node>[:<zone>][,...]", 0,
#else
	{'n', "node", "<node>[,...]", 0,
#endif
	    CLDES_OPT_NODE
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
#if SOL_VERSION >= __s10
	{'z', "zone", "<zone>", 0,
	    CLDES_OPT_ZONE
	},
	{'Z', "zonecluster", "{all | global | <zone-cluster-name>}", 0,
	    CLDES_OPT_ZC_NAME
	},
#endif
	{0, 0, 0, 0, 0}
};

// List of subcommands for the "clresourcetype" command
static clsubcommand_t clrt_subcommands[] = {
	{
		"add-node",
		CLSUB_DO_COMMAND_LOGGING | CLSUB_ZONE_CLUSTER_ALLOWED,
		(char *)CLRT_SUBCMD_ADD_NODE,
		CL_AUTH_CLUSTER_MODIFY,
		clrt_add_node_usage,
		clrt_add_node_optionslist,
		clrt_add_node_options
	},
	{
		"register",
		CLSUB_DO_COMMAND_LOGGING | CLSUB_ZONE_CLUSTER_ALLOWED,
		(char *)CLRT_SUBCMD_REGISTER,
		CL_AUTH_CLUSTER_MODIFY,
		clrt_register_usage,
		clrt_register_optionslist,
		clrt_register_options
	},
	{
		"unregister",
		CLSUB_DO_COMMAND_LOGGING | CLSUB_ZONE_CLUSTER_ALLOWED,
		(char *)CLRT_SUBCMD_UNREGISTER,
		CL_AUTH_CLUSTER_MODIFY,
		clrt_unregister_usage,
		clrt_unregister_optionslist,
		clrt_unregister_options
	},
	{
		"export",
		CLSUB_NON_GLOBAL_ZONE_ALLOWED,
		(char *)CLRT_SUBCMD_EXPORT,
		CL_AUTH_CLUSTER_READ,
		clrt_export_usage,
		clrt_export_optionslist,
		clrt_export_options
	},
	{
		"list",
		CLSUB_NON_GLOBAL_ZONE_ALLOWED | CLSUB_ZONE_CLUSTER_ALLOWED,
		(char *)CLRT_SUBCMD_LIST,
		CL_AUTH_CLUSTER_READ,
		clrt_list_usage,
		clrt_list_optionslist,
		clrt_list_options
	},
	{
		"list-props",
		CLSUB_NON_GLOBAL_ZONE_ALLOWED | CLSUB_ZONE_CLUSTER_ALLOWED,
		(char *)CLRT_SUBCMD_LIST_PROPS,
		CL_AUTH_CLUSTER_READ,
		clrt_list_props_usage,
		clrt_list_props_optionslist,
		clrt_list_props_options
	},
	{
		"set",
		CLSUB_DO_COMMAND_LOGGING | CLSUB_ZONE_CLUSTER_ALLOWED,
		(char *)CLRT_SUBCMD_SET,
		CL_AUTH_CLUSTER_MODIFY,
		clrt_set_usage,
		clrt_set_optionslist,
		clrt_set_options
	},
	{
		"remove-node",
		CLSUB_DO_COMMAND_LOGGING | CLSUB_ZONE_CLUSTER_ALLOWED,
		(char *)CLRT_SUBCMD_REMOVE_NODE,
		CL_AUTH_CLUSTER_MODIFY,
		clrt_remove_node_usage,
		clrt_remove_node_optionslist,
		clrt_remove_node_options
	},
	{
		"show",
		CLSUB_NON_GLOBAL_ZONE_ALLOWED | CLSUB_ZONE_CLUSTER_ALLOWED,
		(char *)CLRT_SUBCMD_SHOW,
		CL_AUTH_CLUSTER_READ,
		clrt_show_usage,
		clrt_show_optionslist,
		clrt_show_options
	},
	{0, 0, 0, 0, 0, 0, 0}
};

// Initialization properties for the "clresource" command
clcommandinit_t clrtinit = {
	CLCOMMANDS_COMMON_VERSION,
	(char *)CLRT_CMD_DESC,
	clrt_usage,
	clrt_subcommands
};
