//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef _CLUSTER_CMD_H
#define	_CLUSTER_CMD_H

#pragma ident	"@(#)cluster_cmd.h	1.6	08/05/28 SMI"

//
// This is the header file for the cluster(1CL) command
//

#define	CLUSTER_SUBCMD_CHECK		\
	GETTEXT("Check and report whether the cluster is configured correctly")
/* BEGIN CSTYLED */
#define	CLUSTER_SUBCMD_LIST_CHECKS		\
	GETTEXT("List configuration checks available for use with the check subcommand")
/* END CSTYLED */
#define	CLUSTER_SUBCMD_CREATE		\
	GETTEXT("Create a cluster using a cluster configuration file")
#define	CLUSTER_SUBCMD_EXPORT		\
	GETTEXT("Export the cluster configuration")
#define	CLUSTER_SUBCMD_LIST		\
	GETTEXT("List the cluster name")
#define	CLUSTER_SUBCMD_LIST_CMDS	\
	GETTEXT("List Sun Cluster commands")
#define	CLUSTER_SUBCMD_SET_NETPROPS	\
	GETTEXT("Set private network properties")
#define	CLUSTER_SUBCMD_SET		\
	GETTEXT("Set cluster properties")
#define	CLUSTER_SUBCMD_RENAME		\
	GETTEXT("Rename the cluster")
#define	CLUSTER_SUBCMD_RESTORE_NETPROPS	\
	GETTEXT("Restore the private network properties")
#define	CLUSTER_SUBCMD_SHOW		\
	GETTEXT("Show cluster component configurations")
#define	CLUSTER_SUBCMD_SHOW_NETPROPS	\
	GETTEXT("Show cluster private network properties")
#define	CLUSTER_SUBCMD_SHUTDOWN		\
	GETTEXT("Shutdown the cluster")
#define	CLUSTER_SUBCMD_STATUS		\
	GETTEXT("Display the status of cluster components")
#define	CLUSTER_CMD_DESC		\
	GETTEXT("Manage the cluster")

#include "clcommands.h"
#include "cluster.h"

// Subcommand options
extern clerrno_t cluster_create_options(ClCommand &cmd);
extern clerrno_t cluster_check_options(ClCommand &cmd);
extern clerrno_t cluster_export_options(ClCommand &cmd);
extern clerrno_t cluster_list_options(ClCommand &cmd);
extern clerrno_t cluster_list_cmds_options(ClCommand &cmd);
extern clerrno_t cluster_set_netprops_options(ClCommand &cmd);
extern clerrno_t cluster_set_options(ClCommand &cmd);
extern clerrno_t cluster_remove_options(ClCommand &cmd);
extern clerrno_t cluster_rename_options(ClCommand &cmd);
extern clerrno_t cluster_restore_netprops_options(ClCommand &cmd);
extern clerrno_t cluster_show_options(ClCommand &cmd);
extern clerrno_t cluster_show_netprops_options(ClCommand &cmd);
extern clerrno_t cluster_shutdown_options(ClCommand &cmd);
extern clerrno_t cluster_status_options(ClCommand &cmd);

#endif /* _CLUSTER_CMD_H */
