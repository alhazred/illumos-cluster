//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clusterinit_constants.cc	1.12	08/10/07 SMI"

//
// Initialization data for cluster(1CL)
//

#include "cluster_cmd.h"

//
// Usage Messages
//
// All usage message lines should begin with a single "%s" for
// the command name. Lines must never include more than a single "%s".
//

// List of "cluster" command usage message
static char *cluster_usage [] = {
	"%s <subcommand> [<options>] [<clustername>]",
	"%s [<subcommand>]  -? | --help",
	"%s  -V | --version",
	(char *)0
};

// List of cluster "create" subcommand usage messages
static char *cluster_create_usage[] = {
	"%s create -i {- | <clconfiguration>} [<options>] [<clustername>]",
	(char *)0
};

// List of cluster "check" subcommand usage messages
static char *cluster_check_usage[] = {
	"%s check [<options>] [<clustername>]",
	(char *)0
};

// List of cluster "export" subcommand usage messages
static char *cluster_export_usage[] = {
	"%s export [<options>] [<clustername>]",
	(char *)0
};

// List of cluster "list" subcommand usage messages
static char *cluster_list_usage[] = {
	"%s list [<clustername>]",
	(char *)0
};

// List of cluster "list-cmds" subcommand usage messages
static char *cluster_list_cmds_usage[] = {
	"%s list-cmds",
	(char *)0
};

// List of cluster "set-netprops" subcommand usage messages
static char *cluster_set_netprops_usage[] = {
	"%s set-netprops -p <name>=<value> [<clustername>]",
	(char *)0
};

// List of cluster "set" subcommand usage messages
static char *cluster_set_usage[] = {
	"%s set -p <name>=<value> [<clustername>]",
	(char *)0
};

// List of cluster "rename" subcommand usage messages
static char *cluster_rename_usage[] = {
	"%s rename -c <newclustername> [<clustername>]",
	(char *)0
};

// List of cluster "restore-netprops" subcommand usage messages
static char *cluster_restore_netprops_usage[] = {
	"%s restore-netprops [<clustername>]",
	(char *)0
};

// List of cluster "show" subcommand usage messages
static char *cluster_show_usage[] = {
	"%s show [<options>] [<clustername>]",
	(char *)0
};

// List of cluster "show-netprops" subcommand usage messages
static char *cluster_show_netprops_usage[] = {
	"%s show-netprops [<options>] [<clustername>]",
	(char *)0
};

// List of cluster "shutdown" subcommand usage messages
static char *cluster_shutdown_usage[] = {
	"%s shutdown [<options>] [<clustername>]",
	(char *)0
};

// List of cluster "status" subcommand usage messages
static char *cluster_status_usage[] = {
	"%s status [<options>] [<clustername>]",
	(char *)0
};

//
// Option lists
//
// All option descriptions should be broken up into lines of no
// greater than 70 characters.  All but the trailing newline ('\n')
// itself should be included in the option descriptions found in these
// tables.
//

// List of options for the cluster "create" subcommand
static cloptions_t cluster_create_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'i', "input", "{- | <clconfiguration>}", 0,
	    CLDES_OPT_INPUT
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the cluster "check" subcommand
// List of options for the cluster "list-checks" subcommand
// Managed by the ksh launcher for the java-implemented configuration checker
static cloptions_t cluster_check_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the cluster "export" subcommand
static cloptions_t cluster_export_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'t', "type", "<type>", CLOPT_ARG_LIST,
	    CLDES_OPT_CTYPE
	},
	{'o', "output", "{- | <clconfiguration>}", 0,
	    CLDES_OPT_OUTPUT
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the cluster "list" subcommand
static cloptions_t cluster_list_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the cluster "list-cmds" subcommand
static cloptions_t cluster_list_cmds_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the cluster "set-netprops" subcommand
static cloptions_t cluster_set_netprops_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'p', "property", "<name>=<value>", 0,
	    CLDES_OPT_PROPERTY
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the cluster "set" subcommand
static cloptions_t cluster_set_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'p', "property", "<name>=<value>", 0,
	    CLDES_OPT_PROPERTY
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the cluster "rename" subcommand
static cloptions_t cluster_rename_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'c', "newclustername", "<newclustername>", 0,
	    CLDES_OPT_CLUSTER
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the cluster "restore-netprops" subcommand
static cloptions_t cluster_restore_netprops_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the cluster "show" subcommand
static cloptions_t cluster_show_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'t', "type", "<type>", CLOPT_ARG_LIST,
	    CLDES_OPT_CTYPE
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the cluster "show-netprops" subcommand
static cloptions_t cluster_show_netprops_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the cluster "shutdown" subcommand
static cloptions_t cluster_shutdown_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'g', "graceperiod", "<graceperiod>", 0,
	    CLDES_OPT_GRACE
	},
	{'m', "message", "<message>", 0,
	    CLDES_OPT_MSG
	},
	{'y', "yes", 0, 0,
	    CLDES_OPT_YES
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of options for the cluster "status" subcommand
static cloptions_t cluster_status_optionslist[] = {
	{'?', "help", 0, 0,
	    CLDES_OPT_HELP
	},
	{'t', "type", "<type>", CLOPT_ARG_LIST,
	    CLDES_OPT_CTYPE
	},
	{'v', "verbose", 0, 0,
	    CLDES_OPT_VERBOSE
	},
	{0, 0, 0, 0, 0}
};

// List of subcommands for the "cluster" command
static clsubcommand_t cluster_subcommands[] = {
	{
		"create",
		CLSUB_NCLMODE_REQUIRED,
		(char *)CLUSTER_SUBCMD_CREATE,
		CL_AUTH_CLUSTER_MODIFY,
		cluster_create_usage,
		cluster_create_optionslist,
		cluster_create_options
	},
	{
		"check",
		CLSUB_NCLMODE_ALLOWED,
		(char *)CLUSTER_SUBCMD_CHECK,
		CL_AUTH_CLUSTER_MODIFY,
		cluster_check_usage,
		cluster_check_optionslist,
		cluster_check_options
	},
	{
		"list-checks", // same as "check" in cc code
		CLSUB_NCLMODE_ALLOWED,
		(char *)CLUSTER_SUBCMD_LIST_CHECKS,
		CL_AUTH_CLUSTER_READ,
		cluster_check_usage,
		cluster_check_optionslist,
		cluster_check_options
	},
	{
		"export",
		CLSUB_DEFAULT,
		(char *)CLUSTER_SUBCMD_EXPORT,
		CL_AUTH_CLUSTER_READ,
		cluster_export_usage,
		cluster_export_optionslist,
		cluster_export_options
	},
	{
		"list",
		CLSUB_NON_GLOBAL_ZONE_ALLOWED |
			CLSUB_ZONE_CLUSTER_ALLOWED,
		(char *)CLUSTER_SUBCMD_LIST,
		CL_AUTH_CLUSTER_READ,
		cluster_list_usage,
		cluster_list_optionslist,
		cluster_list_options
	},
	{
		"list-cmds",
		CLSUB_NCLMODE_ALLOWED |
			CLSUB_NON_GLOBAL_ZONE_ALLOWED |
			CLSUB_ZONE_CLUSTER_ALLOWED,
		(char *)CLUSTER_SUBCMD_LIST_CMDS,
		CL_AUTH_CLUSTER_READ,
		cluster_list_cmds_usage,
		cluster_list_cmds_optionslist,
		cluster_list_cmds_options
	},
	{
		"set",
		CLSUB_DO_COMMAND_LOGGING,
		(char *)CLUSTER_SUBCMD_SET,
		CL_AUTH_CLUSTER_MODIFY,
		cluster_set_usage,
		cluster_set_optionslist,
		cluster_set_options
	},
	{
		"set-netprops",
		CLSUB_NCLMODE_ALLOWED,
		(char *)CLUSTER_SUBCMD_SET_NETPROPS,
		CL_AUTH_CLUSTER_MODIFY,
		cluster_set_netprops_usage,
		cluster_set_netprops_optionslist,
		cluster_set_netprops_options
	},
	{
		"restore-netprops",
		CLSUB_NCLMODE_REQUIRED,
		(char *)CLUSTER_SUBCMD_RESTORE_NETPROPS,
		CL_AUTH_CLUSTER_MODIFY,
		cluster_restore_netprops_usage,
		cluster_restore_netprops_optionslist,
		cluster_restore_netprops_options
	},
	{
		"rename",
		CLSUB_DO_COMMAND_LOGGING,
		(char *)CLUSTER_SUBCMD_RENAME,
		CL_AUTH_CLUSTER_MODIFY,
		cluster_rename_usage,
		cluster_rename_optionslist,
		cluster_rename_options
	},
	{
		"show",
		CLSUB_DEFAULT |
			CLSUB_ZONE_CLUSTER_ALLOWED,
		(char *)CLUSTER_SUBCMD_SHOW,
		CL_AUTH_CLUSTER_READ,
		cluster_show_usage,
		cluster_show_optionslist,
		cluster_show_options
	},
	{
		"show-netprops",
		CLSUB_NCLMODE_ALLOWED,
		(char *)CLUSTER_SUBCMD_SHOW_NETPROPS,
		CL_AUTH_CLUSTER_READ,
		cluster_show_netprops_usage,
		cluster_show_netprops_optionslist,
		cluster_show_netprops_options
	},
	{
		"shutdown",
		CLSUB_DO_COMMAND_LOGGING |
			CLSUB_ZONE_CLUSTER_ALLOWED,
		(char *)CLUSTER_SUBCMD_SHUTDOWN,
		CL_AUTH_CLUSTER_ADMIN,
		cluster_shutdown_usage,
		cluster_shutdown_optionslist,
		cluster_shutdown_options
	},
	{
		"status",
		CLSUB_NON_GLOBAL_ZONE_ALLOWED |
			CLSUB_ZONE_CLUSTER_ALLOWED,
		(char *)CLUSTER_SUBCMD_STATUS,
		CL_AUTH_CLUSTER_READ,
		cluster_status_usage,
		cluster_status_optionslist,
		cluster_status_options
	},
	{0, 0, 0, 0, 0, 0, 0}
};

// Initialization properties for the "cluster" command
clcommandinit_t clusterinit = {
	CLCOMMANDS_COMMON_VERSION,
	(char *)CLUSTER_CMD_DESC,
	cluster_usage,
	cluster_subcommands
};
