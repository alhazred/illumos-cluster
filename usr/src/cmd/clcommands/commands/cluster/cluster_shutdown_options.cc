//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)cluster_shutdown_options.cc	1.2	08/05/20 SMI"

//
// Process cluster "shutdown" subcommand options
//

#include "cluster_cmd.h"

//
// cluster "shutdown" options
//
clerrno_t
cluster_shutdown_options(ClCommand &cmd)
{
	int i, c;
	int errcount = 0;
	char *cl_option = 0;
	int option_index = 0;
	char *shortopts;
	struct option *longopts;
	ValueList operands;
	optflgs_t optflgs = 0;
	int msgflg, gflg;
	char *grace_time = (char *)0;
	char *std_message = (char *)0;
	int g_time = -1;
	ushort_t int_grace_time = 60;
	char junk[4];

	// Get the short and long option lists
	shortopts = clsubcommand_get_shortoptions(cmd);
	longopts = clsubcommand_get_longoptions(cmd);

	// This subcommand should have options
	if (!shortopts || !longopts) {
		clerror("Aborting - subcommand initialization failed.\n");
		abort();
	}

	// Initialize
	gflg = msgflg = 0;

	optind = 2;
	while ((c = getopt_clip(cmd.argc, cmd.argv, shortopts, longopts,
	    &option_index)) != -1) {

		// Get the option
		delete cl_option;
		cl_option = clcommand_option(optopt, cmd.argv[optind - 1]);

		// Switch on option letter
		switch (c) {
		case 'v':
			// Verbose option
			cmd.isVerbose = 1;
			optflgs |= vflg;
			break;

		case 'y':
			// Verbose option
			cmd.isVerbose = 1;
			optflgs |= yflg;
			break;

		case 'g':
			// Grace period before shutdown
			grace_time = optarg;
			gflg++;
			break;

		case 'm':
			// Warning message displayed before shutdown
			std_message = optarg;
			msgflg++;
			break;

		case '?':
			// Help option?
			if ((strcmp(cl_option, "-?") == 0) ||
			    (strcmp(cl_option, "--help") == 0)) {
				optflgs |= hflg;
				break;
			}

			// Or, unrecognized option?
			clerror("Unrecognized option - \"%s\".\n", cl_option);
			++errcount;
			break;

		case ':':
			// Missing argument
			clerror("Option \"%s\" requires an argument.\n",
			    cl_option);
			++errcount;
			break;

		default:
			//
			// We should never get here.
			// If we do, it means that there
			// is an option in our option list
			// for which there is no case statement.
			// We either have a bad option list or
			// a bad case statement.
			//
			clerror("Unexpected option - \"%s\".\n", cl_option);
			++errcount;
			break;
		}
	}
	delete cl_option;
	cl_option = (char *)0;

	// Check if option is specified multiple times
	if (gflg > 1) {
		clerror("You can only specify the \"-g\" option once.\n");
		++errcount;
	}
	if (msgflg > 1) {
		clerror("You can only specify the \"-m\" option once.\n");
		++errcount;
	}

	// Errors?
	if (errcount) {
		clcommand_usage(cmd, cmd.getSubCommandName(), errcount);
		return (CL_EINVAL);
	}

	// Just print help?
	if (optflgs & hflg) {
		clcommand_help(cmd, cmd.getSubCommandName());
		return (CL_NOERR);
	}

	// Get the grace period
	if (grace_time) {
		i = sscanf(grace_time, "%d%1s", &g_time, junk);
		if (i != 1) {
			++errcount;
			clerror("You must specify an integer for the grace "
			    "period in seconds.\n");
		} else if (g_time < 0 || g_time > (int)UINT16_MAX) {
			++errcount;
			clerror("The grace period before shutdown must be an "
			    "integer between 0 and %d\n", (int)UINT16_MAX);
		} else {
			int_grace_time = (ushort_t)g_time;
		}
	}

	// Update the operands list
	for (i = optind;  i < cmd.argc;  ++i) {
		operands.add(cmd.argv[i]);
	}

	// Checks for operand
	if (operands.getSize() > 1) {
		++errcount;
		clerror("You can only shut down one cluster.\n");
	}

	if (errcount) {
		clcommand_usage(cmd, cmd.getSubCommandName(), errcount);
		return (CL_EINVAL);
	}

	// Perform cluster "shutdown" operation
	return (cluster_shutdown(cmd, operands, optflgs, int_grace_time,
	    std_message));
}
