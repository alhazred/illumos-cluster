//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)cluster_check_options.cc	1.3	08/05/28 SMI"

//
// Process cluster "check" subcommand options
//

#include "cluster_cmd.h"

//
// cluster "check" options
//
// unlike other cl commands/subcommands, cluster check
// defers input arg processing to the configuration checking engine
//
// convert argv within ClCommand to ValueList
//
clerrno_t
cluster_check_options(ClCommand &cmd)
{
	int i;
	clerrno_t status;
	// critical that ValueList not be a Set else values that differ
	// only in case will be dropped!
	ValueList args = ValueList(false);

	// Add the operands list
	for (i = 2;  i < cmd.argc;  ++i) {
		args.add(cmd.argv[i]);
	}

	// Perform cluster "check" operation
	status = cluster_check(cmd, args);
	return (status);
}
