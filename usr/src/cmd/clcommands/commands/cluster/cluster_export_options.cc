//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)cluster_export_options.cc	1.2	08/05/20 SMI"

//
// Process cluster "export" subcommand options
//

#include "cluster_cmd.h"

//
// cluster "export" options
//
clerrno_t
cluster_export_options(ClCommand &cmd)
{
	int i, c;
	int wildcard;
	int errcount = 0;
	char *cl_option = 0;
	int option_index = 0;
	char *shortopts;
	struct option *longopts;
	ValueList operands;
	ValueList cltypes = ValueList(true);
	optflgs_t optflgs = 0;
	int oflg = 0;
	char *clconfiguration = (char *)0;

	// Get the short and long option lists
	shortopts = clsubcommand_get_shortoptions(cmd);
	longopts = clsubcommand_get_longoptions(cmd);

	// This subcommand should have options
	if (!shortopts || !longopts) {
		clerror("Aborting - subcommand initialization failed.\n");
		abort();
	}

	optind = 2;
	while ((c = getopt_clip(cmd.argc, cmd.argv, shortopts, longopts,
	    &option_index)) != -1) {

		// Get the option
		delete cl_option;
		cl_option = clcommand_option(optopt, cmd.argv[optind - 1]);

		// Switch on option letter
		switch (c) {
		case 'v':
			// Verbose option
			cmd.isVerbose = 1;
			optflgs |= vflg;
			break;

		case '?':
			// Help option?
			if ((strcmp(cl_option, "-?") == 0) ||
			    (strcmp(cl_option, "--help") == 0)) {
				optflgs |= hflg;
				break;
			}

			// Or, unrecognized option?
			clerror("Unrecognized option - \"%s\".\n", cl_option);
			++errcount;
			break;

		case 't':
			// Object types
			cltypes.add(1, optarg);
			break;

		case 'o':
			// Output
			clconfiguration = optarg;
			oflg++;
			break;

		case ':':
			// Missing argument
			clerror("Option \"%s\" requires an argument.\n",
			    cl_option);
			++errcount;
			break;

		default:
			//
			// We should never get here.
			// If we do, it means that there
			// is an option in our option list
			// for which there is no case statement.
			// We either have a bad option list or
			// a bad case statement.
			//
			clerror("Unexpected option - \"%s\".\n", cl_option);
			++errcount;
			break;
		}
	}
	delete cl_option;
	cl_option = (char *)0;

	// Check for duplicated options
	if (oflg > 1) {
		clerror("You can only specify the \"-o\" "
		    "option once.\n");
		++errcount;
	}

	// Errors?
	if (errcount) {
		clcommand_usage(cmd, cmd.getSubCommandName(), errcount);
		return (CL_EINVAL);
	}

	// Just print help?
	if (optflgs & hflg) {
		clcommand_help(cmd, cmd.getSubCommandName());
		return (0);
	}

	// Add the operands list
	wildcard = 0;
	for (i = optind;  i < cmd.argc;  ++i) {
		if (strcmp(cmd.argv[i], CLCOMMANDS_WILD_OPERAND) == 0) {
			++wildcard;
		} else {
			operands.add(cmd.argv[i]);
		}
	}

	// Check operands
	if (operands.getSize() > 1) {
		// Can only have one operand
		clerror("You can only export cluster configuration for one "
		    "cluster.\n");
		++errcount;
	}

	// Cannot specify both + and other operands
	if (wildcard && operands.getSize() > 0) {
		++errcount;
		clerror("You cannot specify both \"%s\" and the cluster name "
		    "at the same time.\n", CLCOMMANDS_WILD_OPERAND);
	}

	if (errcount) {
		clcommand_usage(cmd, cmd.getSubCommandName(), errcount);
		return (CL_EINVAL);
	}

	// Perform cluster "export" operation
	return (cluster_export(cmd, operands, optflgs, cltypes,
	    clconfiguration));
}
