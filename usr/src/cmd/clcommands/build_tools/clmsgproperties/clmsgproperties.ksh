#! /bin/ksh -p
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#pragma ident	"@(#)clmsgproperties.ksh	1.5	08/05/20 SMI"
#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

#####################################################
#
# clmsgproperties [-v] [-h] [-n] [-i <input_properties_file>]
#    -o <output_properties_file> -T <type> <source_files>...
#
# Create the "<output_properties_file>" from the "<input_properties_file>"
# and list of "<source_files>".
#
# The -T option must be specified at least once with one of the
# following types:
#
#	gettext
#	clwarning
#	clmessage
#	clerror
#	getstring
#
# "gettext", "clwarning", and "clmessage" types can be combined,
# since they all use the same type of properties file.
#
# If the -n option is specified, line numbers are included.
#
# If the -v option is specified, verbose messages are printed.
#
#####################################################

#####################################################
#
# Global Variables
#
#####################################################
PATH=/bin:/usr/bin:/sbin:/usr/sbin; export PATH

typeset PROG=${0##*/}
typeset BASEDIR=${0%%/${PROG}}
[[ "${BASEDIR}" == "${PROG}" ]] && BASEDIR=.

typeset CMD_CLMSGFMT=${BASEDIR}/../clmsgfmt/clmsg
typeset CMD_CLXGETTEXT=${BASEDIR}/../clxgettext/clxgettext

typeset TMPPROPS_OUT=/tmp/${PROG}.properties_out.$$

typeset TMPDIR_PO=/tmp/${PROG}.messages.po.d.$$
typeset TMPFILE_PO=${TMPDIR_PO}/messages.po
typeset TMPFILE_PO2=${TMPDIR_PO}/messages2.po

integer CLCOMMAND_NO_LITERAL_STRING_EXIT_CODE=4

#####################################################
#
# print_usage()
#
#	Print usage message to stderr
#
#####################################################
print_usage()
{
	echo "usage:  ${PROG} [-v] [-n] [-i <input_properties_file>] -o <output_properties_file> -T <type> <source_files>..." >&2
}

#####################################################
#
# cleanup()
#
#	Cleanup temp files
#
#####################################################
cleanup()
{
	rm -f ${TMPPROPS_OUT}
	rm -rf ${TMPDIR_PO}
}

#####################################################
#
# Main
#
#####################################################
main()
{
	typeset type=
	typeset verbose=
	typeset input_properties_file=
	typeset ouput_properties_file=
	typeset source_files=
	typeset noption=

	integer errcount=0
	integer status

	typeset -l lower_type
	typeset type_args=
	typeset clmsg_type_arg=
	typeset clmsg_iprop_arg=
	typeset c
	typeset cmd

	# Make sure we can find clmsgfmt and clxgettext
	if [[ ! -x ${CMD_CLMSGFMT} ]]; then
		echo "${PROG}:  Cannot find ${CMD_CLMSGFMT}." >&2
		((errcount += 1))
	fi
	if [[ ! -x ${CMD_CLXGETTEXT} ]]; then
		echo "${PROG}:  Cannot find ${CMD_CLXGETTEXT}." >&2
		((errcount += 1))
	fi
	if [[ ${errcount} -ne 0 ]]; then
		return 1
	fi

	# Cleanup
	cleanup

	# Process options 
	while getopts T:i:o:vhn c 2>/dev/null
	do
		case ${c} in
		T)	# Type
			lower_type=${OPTARG}
			case ${lower_type} in
			gettext|clmessage|clwarning)
				if [[ -n "${type}" ]] &&
				    [[ "${type}" != "gettext" ]] &&
				    [[ "${type}" != "clmessage" ]] &&
				    [[ "${type}" != "clwarning" ]]; then
					echo "${PROG}:  \"-T ${OPTARG}\" cannot be combined with \"-T ${type}\"." >&2
					return 1
				fi
				;;

			clerror)
				if [[ -n "${type}" ]] &&
				    [[ "${type}" != "clerror" ]]; then
					echo "${PROG}:  \"-T ${OPTARG}\" cannot be combined with \"-T ${type}\"." >&2
					return 1
				fi
				;;

			getstring)
				if [[ -n "${type}" ]] &&
				    [[ "${type}" != "getstring" ]]; then
					echo "${PROG}:  \"-T ${OPTARG}\" cannot be combined with \"-T ${type}\"." >&2
					return 1
				fi
				;;

			*)	# Unknown -T <type>
				echo "${PROG}:  Unknown message type \"${OPTARG}\"." >&2
				return 1
				;;
			esac

			type=${lower_type}
			type_args="${type_args} -T ${type}"
			if [[ -z "${clmsg_type_arg}" ]]; then
				clmsg_type_arg="-T ${type}"
			fi
			;;

		i)	# Input properties file
			input_properties_file=${OPTARG}
			;;

		o)	# Output properties file
			output_properties_file=${OPTARG}
			;;

		v)	# Verbose
			verbose=1
			;;

		n)	# Line numbers
			noption="-n"
			;;

		*)	# Error
			print_usage
			return 1
			;;
		esac
	done
	shift $((OPTIND - 1))

	# Make sure that a type option was specified
	if [[ -z "${type_args}" ]]; then
		echo "${PROG}:  At least one message type must be specified using \"-T\"." >&2
		((errcount += 1))
	fi

	# Make sure that an output properties file was specified
	if [[ -z "${output_properties_file}" ]]; then
		echo "${PROG}:  A new properties file must be specified using \"-o\"." >&2
		((errcount += 1))
	fi

	# Construct the list of source files
	while [[ $# -ne 0 ]]
	do
		source_files="${source_files} $1"
		shift
	done
	if [[ -z "${source_files}" ]]; then
		echo "${PROG}:  At lease one source file must be specified." >&2
		((errcount += 1))
	fi

	# Check for usage errors
	if [[ ${errcount} -ne 0 ]]; then
		print_usage
		return 1
	fi

	# Create the .mo file
	mkdir -m 755 ${TMPDIR_PO}
	if [[ $? -ne 0 ]]; then
		echo "${PROG}:  Cannot create ${TMPDIR_PO}." >&2
		return 1
	fi

	# Add uppercase gettext to command, if it was supplied
	echo ${type_args} | grep gettext >/dev/null
	if [[ $? -eq 0 ]]; then
		type_args="${type_args} -T GETTEXT"
	fi

	cmd="${CMD_CLXGETTEXT} ${noption} -i -p ${TMPDIR_PO} ${type_args} ${source_files}"
	if [[ -n "${verbose}" ]]; then
		echo ${cmd}
	fi
	eval ${cmd}
	status=$?
	if [[ ${status} -ne 0 ]] &&
	    [[ ${status} -ne ${CLCOMMAND_NO_LITERAL_STRING_EXIT_CODE} ]]; then
		echo "${PROG}:  clxgettext failed." >&2
		return 1
	fi

	# Remove comments from message IDs
	if [[ -n "${verbose}" ]]; then
		echo "sed 's/^# msg//msg/' ${TMPFILE_PO} >${TMPFILE_PO2}"
	fi
	sed 's/^# msg/msg/' ${TMPFILE_PO} >${TMPFILE_PO2}
	if [[ $? -ne 0 ]]; then
		echo "${PROG}:  Failed to update messages.po file." >&2
		return 1
	fi

	# Set the input properties file, if there is one
	if [[ -f "${input_properties_file}" ]]; then
		clmsg_iprop_arg="-i ${input_properties_file}"
	fi

	# Create the new temp properties file
	cmd="${CMD_CLMSGFMT} ${clmsg_type_arg} -m ${TMPFILE_PO2} ${clmsg_iprop_arg} ${TMPPROPS_OUT}"
	if [[ -n "${verbose}" ]]; then
		echo ${cmd}
	fi
	eval ${cmd}
	if [[ $? -ne 0 ]]; then
		echo "${PROG}:  clmsg failed." >&2
		return 1
	fi

	# Copy out the temp properties file
	rm -f ${output_properties_file}
	cmd="cp ${TMPPROPS_OUT} ${output_properties_file}"
	if [[ -n "${verbose}" ]]; then
		echo ${cmd}
	fi
	eval ${cmd}
	if [[ $? -ne 0 ]]; then
		echo "${PROG}:  Failed to copy properties file." >&2
		return 1
	fi
	chmod 666 ${output_properties_file}

	# Done
	return 0
}

	integer exit_status=0

	# Set umask
	umask 0

	# Call main
	main $*
	exit_status=$?

	# Cleanup
	cleanup

	# Done
	exit ${exit_status}
