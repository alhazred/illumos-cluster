//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident   "@(#)clmsg.cc	1.3	08/05/20 SMI"

//
// clmsg
//
// This program will parse both messages.po and "properties" files, generating
// a new "properties" file as output.  These "properties" files are
// resource bundle properties files as described here:
//
// http:// java.sun.com/j2se/1.4.2/docs/api/java/util/Properties.html
//
// clmsg -T <type> [-m <messages.po_file>] [-i <input_properties_file>]
//    [output_properties_file]
//
//	-T <type>			"gettext", "clerror", "clmessage",
//					    "clwarning", "getstring"
//	-m <messages.po_file>		input messages.po file
//	-i <input_properties_file>	input properties file
//
// If an <output_properties_file> is not given, output is to stdout.
//
// Both -m and -i can be specified any number of times.  If neither is
// specified, there will be no new output.  If both -m and -i are specified,
// the content is merged.
//
// Output is sorted by key.
//

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>

#include "ValueList.h"
#include "Properties.h"
#include "GetText.h"

typedef enum outtype {
	TYPE_UNKNOWN,
	TYPE_GETTEXT,
	TYPE_CLMESSAGE,
	TYPE_CLWARNING,
	TYPE_CLERROR,
	TYPE_GETSTRING
} outtype_t;

static char *commandname;

static void usage(FILE *fp);
static int fileaccess(char *filename);
static int message_file_load(outtype_t outtype, Properties &properties,
    char *filename);

int
main(int argc, char *argv[])
{
	int c;
	int errcount;
	int status;
	ValueList message_files;
	ValueList properties_files;
	Properties properties;
	char *outputfile = "-";
	char tmpfilename[100];
	outtype_t outtype = TYPE_UNKNOWN;

	// Set the command name
	if ((commandname = strrchr(argv[0], '/')) == NULL) {
		commandname = argv[0];
	} else {
		++commandname;
	}

	// Get the options
	opterr = 0;
	optind = 1;
	while ((c = getopt(argc, argv, "T:m:i:")) != EOF) {
		switch (c) {
		case 'T':
			if (outtype != TYPE_UNKNOWN) {
				(void) fprintf(stderr,
				    "%s:  -T can only be used once.\n",
				    commandname);
				usage(stderr);
				return (1);
			}
			if (strcasecmp(optarg, "gettext") == 0) {
				outtype = TYPE_GETTEXT;
			} else if (strcasecmp(optarg, "clmessage") == 0) {
				outtype = TYPE_CLMESSAGE;
			} else if (strcasecmp(optarg, "clwarning") == 0) {
				outtype = TYPE_CLWARNING;
			} else if (strcasecmp(optarg, "clerror") == 0) {
				outtype = TYPE_CLERROR;
			} else if (strcasecmp(optarg, "getstring") == 0) {
				outtype = TYPE_GETSTRING;
			} else {
				(void) fprintf(stderr,
				    "%s:  -T must be \"%s\", \"%s\", "
				    "\"%s\", \"%s\", or \"%s\".\n",
				    commandname,
				    "gettext", "clmessage", "clwarning",
				    "clerror", "getstring");
				return (1);
			}
			break;

		case 'm':
			message_files.add(optarg);
			break;

		case 'i':
			properties_files.add(optarg);
			break;

		default:
			usage(stderr);
			return (1);
		}
	}

	// Output file name
	if (argc > optind)
		outputfile = argv[optind++];

	// Only one operand is allowed
	if (argc > optind) {
		usage(stderr);
		return (1);
	}

	// Make sure that our input files exist and can be read
	errcount = 0;
	for (message_files.start();  !message_files.end();
	    message_files.next()) {
		if (fileaccess(message_files.getValue()) != 0)
			++errcount;
	}
	for (properties_files.start();  !properties_files.end();
	    properties_files.next()) {
		if (fileaccess(properties_files.getValue()) != 0)
			++errcount;
	}
	if (errcount)
		return (1);

	// Properties object should include comments
	properties.setIncludeComments();

	// Load properties files
	for (properties_files.start();  !properties_files.end();
	    properties_files.next()) {
		char *properties_input_filename = properties_files.getValue();

		// Make sure that we have a file
		if (!properties_input_filename) {
			(void) fprintf(stderr,
			    "%s:  Internal error - no properties file.\n");
			return (1);
		}

		status = properties.load(properties_input_filename, 1);
		if (status != 0) {
			(void) fprintf(stderr,
			    "%s - Could not load properties file \"%s\" - %s\n",
			    commandname, properties_input_filename,
			    strerror(status));
			++errcount;
		}
	}

	// Parse and load messages files
	for (message_files.start();  !message_files.end();
	    message_files.next()) {
		char *messages_input_filename = message_files.getValue();

		// Make sure that we have a file
		if (!messages_input_filename) {
			(void) fprintf(stderr,
			    "%s:  Internal error - no messages.po file.\n");
			return (1);
		}

		status = message_file_load(outtype, properties,
		    messages_input_filename);
		if (status != 0) {
			(void) fprintf(stderr,
			    "%s - Could not load messages file \"%s\" - %s\n",
			    commandname, messages_input_filename,
			    strerror(status));
			++errcount;
		}
	}

	// If there is an error count, we are done
	if (errcount)
		return (1);

	// Store properties
	status = properties.store(outputfile);
	if (status != 0) {
		(void) fprintf(stderr,
		    "%s - Could not store properties - %s\n",
		    commandname, strerror(status));
		return (1);
	}

	// Done
	return (0);
}

//
// usage
//
// Print usage message.
//
static void
usage(FILE *fp)
{
	(void) fprintf(fp,
	    "usage:  %s [<options>] [<output_properties_file>]\n",
	    commandname);
	(void) putc('\n', fp);
	(void) fputs("    Options:\n", fp);
	(void) fputs("         -T <type>\n", fp);
	(void) fputs("        [-m <messages.po_file>]\n", fp);
	(void) fputs("        [-i <input_properties_file>]\n", fp);
	(void) putc('\n', fp);
}

//
// fileaccess
//
// Return zero, if file can be read.   Otherwise, return errno.
//
static int
fileaccess(char *filename) {
	if (access(filename, R_OK) == -1) {
		(void) fprintf(stderr, "%s:  %s - \"%s\"\n", commandname,
		    strerror(errno), filename);
		return (errno);
	}

	return (0);
}

//
// message_file_load
//
// Return zero, if file can be parsed and loaded.  Otherwise, return errno.
//
static int
message_file_load(outtype_t outtype, Properties &properties, char *filename)
{
	FILE *fp;
	ValueList comments;
	char buffer[1024];
	char key[sizeof ("clcommands.Cnnnnnnn")];
	char dkey[sizeof ("clcommands.Cnnnnnnn.details")];
	char akey[sizeof ("clcommands.Cnnnnnnn.action")];
	char *ptr;
	char *line;
	int len;
	GetText getmsgid;
	char *messageid;

	// Open the message file
	if ((fp = fopen(filename, "r")) == NULL) {
		(void) fprintf(stderr, "%s:  %s - \"%s\"\n", commandname,
		    strerror(errno), filename);
		return (errno);
	}

	// Read and load the file
	while ((line = fgets(buffer, sizeof (buffer), fp)) != NULL) {
		// Get rid of trailing newline
		len = strlen(line);
		if (line[len - 1] == '\n')
			line[len - 1] = 0;

		// Ignore leading white space
		while (isspace(*line))
			++line;

		// Skip blank lines
		if (!line[0])
			continue;

		// Comment
		if (*line == '#') {
			if (line[1])
				comments.add(line + 1);
			continue;
		}

		// msgid
		if ((strncmp("msgid", line, sizeof ("msgid") - 1) == 0) &&
		    isspace(line[sizeof ("msgid")])) {

			// Skip "msgid "
			line += sizeof ("msgid");

			// Skip other white space
			while (isspace(*line))
				++line;

			// Skip empty lines
			if (!line[0]) {
				comments.clear();
				continue;
			}

			// Quoted?
			if (*line == '\"') {
				// Skip empty lines
				if (strlen(line) < 2) {
					comments.clear();
					continue;
				}

				// Delete leading quote
				++line;

				// Delete trailing quote
				if ((ptr = strrchr(line, '\"')) != (char *)0)
					*ptr = 0;
			}

			// All but GETSTRING relies on a message ID
			if (outtype != TYPE_GETSTRING)
				messageid = getmsgid.getMessageId(line);

			// Keys depend on type
			switch (outtype) {
			case TYPE_CLERROR:
				// Details key
				(void) sprintf(dkey, "clcommands.%7.7s.details",
				    messageid);

				// Set details property, if not already set
				(void) properties.setProperty(dkey, "");

				// Action key
				(void) sprintf(akey, "clcommands.%7.7s.action",
				    messageid);

				// Set action property, if not already set
				(void) properties.setProperty(akey, "");

				/* FALLTHROUGH */

			case TYPE_GETTEXT:
			case TYPE_CLMESSAGE:
			case TYPE_CLWARNING:
				// Main key-value
				(void) sprintf(key, "clcommands.%7.7s",
				    messageid);

				// Done with the messageid
				delete messageid;

				// Set the key-value property
				ptr = (char *)properties.setProperty(key, line);

				//
				// If it is already set to something else,
				// issue a warning.
				//
				if (ptr != (char *)0 && *ptr != 0 &&
				    strcmp(ptr, line) != 0) {
					(void) fprintf(stderr,
					    "%s:  WARNING:  Message ID "
					    "\"%s\" is already used for "
					    "a different message.\n",
					    commandname, key);
					(void) fprintf(stderr,
					    "%s:  WARNING:  Existing message - "
					    "\"%s:  %s\".\n",
					    commandname, key, ptr);
					(void) fprintf(stderr,
					    "%s:  WARNING:  Unable to add - "
					    "\"%s:  %s\".\n",
					    commandname, key, line);
					comments.clear();
					continue;
				}
				break;

			case TYPE_GETSTRING:
				// The string itself is the key
				(void) properties.setProperty(line, "");
				break;

			case TYPE_UNKNOWN:
			default:
				(void) fprintf(stderr,
				    "%s:  Internal error - unknown type.\n",
				    commandname);
				exit(1);
			}

			// Add comments
			if (comments.getSize() != 0) {
				for (comments.start();  !comments.end();
				    comments.next()) {
					(void) properties.addComment(key,
					    comments.getValue(), 1);
				}
			}

			// Reset the comments
			comments.clear();
		}
	}

	return (0);
}
