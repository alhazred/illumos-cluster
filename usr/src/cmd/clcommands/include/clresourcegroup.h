//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef	_CLRESOURCEGROUP_H
#define	_CLRESOURCEGROUP_H

#pragma ident	"@(#)clresourcegroup.h	1.5	08/06/06 SMI"

#include "clcommands.h"
#include "ClShow.h"
#include "ClStatus.h"
#include "ClresourcegroupExport.h"

//
// This is the main header file for clresourcegroup functions
//

extern clerrno_t clrg_add_node(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, ValueList &nodes);
extern clerrno_t clrg_create(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, ValueList &nodes, NameValueList &properties);
extern clerrno_t clrg_create(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, char *clconfiguration, ValueList &nodes,
    NameValueList &properties);
extern clerrno_t clrg_delete(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs);
extern clerrno_t clrg_evacuate(ClCommand &cmd, optflgs_t optflgs, int itimeout,
    ValueList &nodes, char *zc_name);
extern clerrno_t clrg_export(ClCommand &cmd, ValueList &operands,
    char *clconfiguration);
extern clerrno_t clrg_list(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, ValueList &resources, ValueList &resourcetypes,
    ValueList &nodes, ValueList &zc_names);
extern clerrno_t clrg_manage(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs);
extern clerrno_t clrg_set(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, NameValueList &properties, ValueList &nodes);
extern clerrno_t clrg_offline(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, ValueList &nodes);
extern clerrno_t clrg_online(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, ValueList &nodes);
extern clerrno_t clrg_quiesce(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs);
extern clerrno_t clrg_remove_node(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, ValueList &nodes, uint_t flg);
extern clerrno_t clrg_restart(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, ValueList &nodes);
extern clerrno_t clrg_resume(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs);
extern clerrno_t clrg_show(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, ValueList &resources, ValueList &resourcetypes,
    ValueList &properties, ValueList &nodes, ValueList &zc_names);
extern clerrno_t get_clrg_show_obj(optflgs_t optflgs, ClShow *rg_show);
extern clerrno_t clrg_status(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, ValueList &resources, ValueList &resourcetypes,
    ValueList &states, ValueList &nodes, ValueList &zc_names);
extern clerrno_t get_clrg_status_obj(optflgs_t optflgs, ClStatus *rg_status);
extern clerrno_t clrg_suspend(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs);
extern clerrno_t clrg_switch(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, ValueList &nodes);
extern clerrno_t clrg_unmanage(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs);
extern clerrno_t clrg_remaster(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs);
#endif /* _CLRESOURCEGROUP_H */
