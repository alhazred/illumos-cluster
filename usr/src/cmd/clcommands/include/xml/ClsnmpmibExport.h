//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef	_CLSNMPMIBEXPORT_H
#define	_CLSNMPMIBEXPORT_H

#pragma ident	"@(#)ClsnmpmibExport.h	1.3	08/05/20 SMI"

//
// This is the header file for the ClsnmpmibExport class.
//

#include <libxml/tree.h>

#include "NameValue.h"
#include "NameValueList.h"

#include "ClcmdExport.h"

// protocol types
enum protocol_t {
    SNMPv2,
    SNMPv3 };

class ClsnmpmibExport : public ClcmdExport
{

public:

	// Create a new object
	ClsnmpmibExport();

	// Create an SNMP mib
	int createSNMPMib(
	    char *name,
	    char *node,
	    protocol_t protocol);

};

// function to get the snmp mib xml data
extern ClcmdExport *get_clsnmpmib_xml_elements();
extern int parse_stdout_to_snmp_mib_xml(ClsnmpmibExport &export_obj,
    FILE *file, char *node);
extern int get_export_obj(ClsnmpmibExport &export_obj, ValueList &operands,
    ValueList &nodes);

#endif // _CLSNMPMIBEXPORT_H
