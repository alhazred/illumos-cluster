//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef	_CLRESOURCETYPEEXPORT_H
#define	_CLRESOURCETYPEEXPORT_H

#pragma ident	"@(#)ClresourcetypeExport.h	1.3	08/05/20 SMI"

//
// This is the header file for the ClresourcetypeExport class.
//

#include <libxml/tree.h>

#include "NameValue.h"
#include "NameValueList.h"

#include "ClcmdExport.h"

// The function to get the resource type xml data
extern ClcmdExport *get_clresourcetype_xml_elements();

// tunability types
enum tunability_t {
    AT_CREATION,
    WHEN_DISABLED,
    ANY_TIME,
    NONE,
    UNKNOWN_TYPE };

// parameter types
enum param_type_t {
    INTEGER,
    BOOLEAN,
    ENUM,
    STRING,
    STRING_ARRAY };


class ClresourcetypeExport : public ClcmdExport
{

public:

	// Create a new object
	ClresourcetypeExport();

	// Adds a resource type
	int addResourcetype(
	    char *rtName,
	    char *RTRfile);

	// Add a resource type node
	int addResourcetypeNode(char *rtName, char *node);

	// Adds the allNodes element to the resource type
	int setAllNodes(char *rtName);

	// Add a method to the resource type
	int addMethod(
	    char *rtName,
	    char *name,
	    char *type);

	// Add a parameter to the resource type
	int addParameter(
	    char *rtName,
	    char *name,
	    bool extension,
	    bool per_node,
	    char *description,
	    tunability_t tunability,
	    param_type_t type,
	    char *enumList = NULL,
	    char *minLength = NULL,
	    char *maxLength = NULL,
	    char *minArrayLength = NULL,
	    char *maxArrayLength = NULL,
	    char *def = NULL);

	// Add a resource type property
	int addResourcetypeProperty(
	    char *rtName,
	    char *name,
	    char *value,
	    bool readonly = false);

	// Add a resource type property
	int addResourcetypeProperty(
	    char *rtName,
	    NameValue *nv,
	    bool readonly = false);

	// Adds resource type properties
	int addResourcetypeProperties(
	    char *rtName,
	    NameValueList *nvl,
	    bool readonly = false);

private:

	// Returns an xmlNode with the name of the resource type, or NULL
	xmlNode *getResourcetype(char *rtName);

};
#endif // _CLRESOURCETYPEEXPORT_H
