//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef	_CLDEVICEGROUPEXPORT_H
#define	_CLDEVICEGROUPEXPORT_H

#pragma ident	"@(#)CldevicegroupExport.h	1.3	08/05/20 SMI"

//
// This is the header file for the CldevicegroupExport class.
//

#include <libxml/tree.h>

#include "NameValue.h"
#include "NameValueList.h"

#include "ClcmdExport.h"

// The function to get the devicegroup XML data
extern ClcmdExport *get_cldevicegroup_xml_elements();

// The devicegroup types
enum dg_type_t {
    SVM,
    RAWDISK};

class CldevicegroupExport : public ClcmdExport
{

public:

	// Create a new object
	CldevicegroupExport();

	// Create a devicegroup with the given name and type
	int createDevicegroup(
	    char *name,
	    dg_type_t type);

	// Add a devicegroupNode to the devicegroup specified
	int addDevicegroupNode(
	    char *devicegroupName,
	    char *nodeName);

	// Add a member to the devicegroup specified
	int addMember(
	    char *devicegroupName,
	    char *memberName);

	// Add a property to the specified devicegroup
	int addProperty(
	    char *devicegroupName,
	    char *name,
	    char *value,
	    bool readonly = false);

	// Add a property to the specified devicegroup
	int addProperty(
	    char *devicegroupName,
	    NameValue *nv,
	    bool readonly = false);

	// Adds properties to the specified devicegroup
	int addProperties(
	    char *devicegroupName,
	    NameValueList *nvl,
	    bool readonly = false);

private:

	// Returns the xmlNode devicegroup object of the given name, or NULL
	xmlNode *getDevicegroup(char *dgname);

};
#endif // _CLDEVICEGROUPEXPORT_H
