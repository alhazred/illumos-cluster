//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef	_CLNODEEXPORT_H
#define	_CLNODEEXPORT_H

#pragma ident	"@(#)ClnodeExport.h	1.3	08/05/20 SMI"

//
// This is the header file for the ClnodeExport class.
//

#include <libxml/tree.h>

#include "NameValue.h"
#include "NameValueList.h"

#include "ClcmdExport.h"

// The function to get the node xml data
extern ClcmdExport *get_clnode_xml_elements();

class ClnodeExport : public ClcmdExport
{

public:

	// Create a new object
	ClnodeExport();

	// Create a new node with the given name, id
	int createNode(
	    char *name,
	    char *id);

	// Create a new node with the given name, id
	int createFarmNode(
	    char *name,
	    char *id,
	    bool monitored,
	    char *adap1,
	    char *adap2 = NULL);

	// Adds a property to the node
	int addProperty(
	    char *nodeName,
	    char *name,
	    char *value,
	    bool readonly = false);

	// Adds a property to the node
	int addProperty(
	    char *nodeName,
	    NameValue *nv,
	    bool readonly = false);

	// Adds properties to the node
	int addProperties(
	    char *nodeName,
	    NameValueList *nvl,
	    bool readonly = false);

private:

	// Returns an xmlNode with the given node name, or NULL
	xmlNode *getNode(char *nodeName);

};
#endif // _CLNODEEXPORT_H
