//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef	_CLQUORUMEXPORT_H
#define	_CLQUORUMEXPORT_H

#pragma ident	"@(#)ClquorumExport.h	1.4	09/01/16 SMI"

//
// This is the header file for the ClquorumExport class.
//

#include <libxml/tree.h>

#include "NameValue.h"
#include "NameValueList.h"

#include "ClcmdExport.h"

// The function to get the quorum xml data
extern ClcmdExport *get_clquorum_xml_elements();

class ClquorumExport : public ClcmdExport
{

public:

	// Create a new object
	ClquorumExport();

	// Add a quorurm node
	int addQuorumNode(char *nodeName);

	// Add a quorum node property
	int addQuorumNodeProperty(
	    char *nodeName,
	    char *name,
	    char *value,
	    bool readonly = false);

	// Add a quorum node property
	int addQuorumNodeProperty(
	    char *nodeName,
	    NameValue *nv,
	    bool readonly = false);

	// Add quorum node properties
	int addQuorumNodeProperties(
	    char *nodeName,
	    NameValueList *nvl,
	    bool readonly = false);

	// Add a quorum device with the given name and type
	int addQuorumDevice(
	    char *name,
	    char *type);

	// Add a quorum device path
	int addQuorumDevicePath(
	    char *quroumDeviceName,
	    char *nodeName,
	    state_t state);

	// Add a quorum device property
	int addQuorumDeviceProperty(
	    char *deviceName,
	    char *name,
	    char *value,
	    bool readonly = false);

	// Add a quorum device property
	int addQuorumDeviceProperty(
	    char *deviceName,
	    NameValue *nv,
	    bool readonly = false);

	// Add quorum device properties
	int addQuorumDeviceProperties(
	    char *deviceName,
	    NameValueList *nvl,
	    bool readonly = false);

#ifdef WEAK_MEMBERSHIP
	// Add global quorum device
	int addGlobalQuorum(
		char *name,
		char *type);

	// Add global quorum property
	int addGlobalQuorumProperty(
		char *deviceName,
		char *name,
		char *value,
		bool readonly);

	int addGlobalQuorumProperty(
		char *deviceName,
		NameValue *nv,
		bool readonly);

	int addGlobalQuorumProperties(
		char *deviceName,
		NameValueList *nvl,
		bool readonly);
#endif
private:

	// Returns an xmlNode with the given quorum node name, or NULL
	xmlNode *getQuorumNode(char *node);

	// Returns an xmlNode with the given quorum device name, or NULL
	xmlNode *getQuorumDevice(char *device);
#ifdef WEAK_MEMBERSHIP
	// Returns an xmlNode with the given global quorum name, or NULL
	xmlNode *getGlobalQuorum(char *device);
#endif
};
#endif // _CLQUORUMEXPORT_H
