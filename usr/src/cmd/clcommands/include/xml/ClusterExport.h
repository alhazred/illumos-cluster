//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef	_CLUSTEREXPORT_H
#define	_CLUSTEREXPORT_H

#pragma ident	"@(#)ClusterExport.h	1.2	08/05/20 SMI"

//
// This is the header file for the ClusterExport class.
//

#include <libxml/tree.h>

#include "NameValue.h"
#include "NameValueList.h"

#include "ClcmdExport.h"

// function to get the cluster xml data
extern ClcmdExport *get_cluster_xml_elements(uint_t get_flg);

class ClusterExport : public ClcmdExport
{

public:

	// Create a new object, with the specified cluster name
	ClusterExport(char *clusterName);

	// Add a cluster property
	int addProperty(
	    char *name,
	    char *value,
	    bool readonly = false);

	// Add a cluster property
	int addProperty(
	    NameValue *nv,
	    bool readonly = false);

	// Adds cluster properties
	int addProperties(
	    NameValueList *nvl,
	    bool readonly = false);

};
#endif // _CLUSTEREXPORT_H
