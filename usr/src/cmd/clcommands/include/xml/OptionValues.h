//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef _OPTIONVALUES_H
#define	_OPTIONVALUES_H

#pragma ident	"@(#)OptionValues.h	1.6	08/05/20 SMI"

//
// This is the header file for the OptionValues Class
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <list>
#include <iterator>

#include "NameValue.h"
#include "NameValueList.h"
#include "ValueList.h"

// The options for each command which uses the XML
enum option_t {
    CLUSTER_AUTOQUORUM,
    CLUSTER_PROP,
    CLDEVICE_NODE,
    CLDG_PROP,
    CLDG_NODE,
    CLDG_MEMBER,
    CLDG_TYPE,
    CLINTER_NODE,
    CLINTER_ADAP_PROP,
    CLINTER_ADAP_TT_PROP,
    CLINTER_DISABLE,
    CLNAS_TYPE,
    CLNAS_PROP,
    CLNAS_USER,
    CLNAS_HOSTNAME,
    CLNAS_PASSFILE,
    CLNAS_DIR,
    CLNODE_CLUSTER,
    CLNODE_SPONSOR,
    CLNODE_ENDPOINT,
    CLNODE_GDEV,
    CLNODE_PROP,
    CLNODE_TYPE,
    CLQUORUM_TYPE,
    CLQUORUM_PROP,
    CLRLH_GROUP,
    CLRLH_PROP,
    CLRLH_AUTO,
    CLRLH_DISABLE,
    CLRES_PROP,
    CLRES_EXT_PROP,
    CLRES_GROUP,
    CLRES_AUTO,
    CLRES_DISABLE,
    CLRES_XPROP,
    CLRES_YPROP,
    CLRES_TYPE,
    CLRG_ZONE,
    CLRG_NODE,
    CLRG_PROP,
    CLRG_SCALABLE,
    CLRT_ZONE,
    CLRT_NODE,
    CLRT_RTRFILE,
    CLRT_ALLNODES,
    CLRT_PROP,
    CLRSA_GROUP,
    CLRSA_PROP,
    CLRSA_AUXNODE,
    CLRSA_AUTO,
    CLRSA_DISABLE,
    CLSNMP_DEFAULT,
    CLSNMP_PASSFILE,
    CLSNMP_SECLEVEL,
    CLSNMP_AUTH,
    CLSNMP_COMMUNITY,
    CLSNMP_NODE,
    CLTA_TYPE,
    VERBOSE};


class OptionValues {

public:

	// Create a new OptionValues object.
	OptionValues(char *subcmd);

	OptionValues(char *subcmd, int argc_in, char *argv_in[]);

	~OptionValues();

	// Return the subcommand name
	char *getSubcommand();

	// Set the subcommand name
	void setSubcommand(char *subcommand);

	// Add an option, with an optional value
	void addOption(int id, char *value = NULL);

	// Add a list of the same option
	void addOptions(int id, ValueList *list);

	// Add a NameValue option
	void addNVOption(int id, char *name, char *value);

	// Add a NameValue option
	void addNVOption(int id, NameValue *nv);

	// Add a list of NameValue options
	void addNVOptions(int id, NameValueList *nvl);

	// Return the option value associated with the ID
	char *getOption(int id);

	// Return a ValueList of options
	ValueList *getOptions(int id);

	// Return a NameValueList of options
	NameValueList *getNVOptions(int id);

	// Set the operand
	void setOperand(char *value);

	// Get the operand name
	char *getOperand();

	// print all the data in this object
	void dumpData();

	// Item indicating the argument count from "main"
	int argc;

	// Item indicating the argument list from "main"
	char **argv;

	// Option flags
	uint32_t optflgs;

private:

	char *getOptionName(int name);

	// option iterator methods
	void optionStart();
	void optionNext();
	int optionEnd();
	NameValue *optionCurrent();

	struct nv_opt_t {
		int name;
		NameValue *nv;
	};

	// nvoption iterator methods
	void nvoptionStart();
	void nvoptionNext();
	int nvoptionEnd();
	nv_opt_t nvoptionCurrent();

	char *subcommand;
	char *operand;
	std::list<NameValue *> options;
	std::list<NameValue *>::iterator opt_iter;
	std::list<nv_opt_t> nvoptions;
	std::list<nv_opt_t>::iterator nvopt_iter;

};

#endif // _OPTIONVALUES_H
