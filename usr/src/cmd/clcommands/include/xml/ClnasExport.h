//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef	_CLNASEXPORT_H
#define	_CLNASEXPORT_H

#pragma ident	"@(#)ClnasExport.h	1.3	08/05/20 SMI"

//
// This is the header file for the ClnasExport class.
//

#include <libxml/tree.h>

#include "NameValue.h"
#include "NameValueList.h"

#include "ClcmdExport.h"

// The function to get the NAS xml data
extern ClcmdExport *get_clnas_xml_elements();

class ClnasExport : public ClcmdExport
{

public:

	// Create a new object
	ClnasExport();

	// Create a NAS device with the given name, type and userid
	int createNasDevice(
	    char *name,
	    char *type,
	    char *userid);

	// Add a direectory to the Nas device
	int addNasDir(
	    char *nasDeviceName,
	    char *path);
};
#endif // _CLNASEXPORT_H
