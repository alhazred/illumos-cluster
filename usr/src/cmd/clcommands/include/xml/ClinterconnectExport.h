//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef	_CLINTERCONNECTEXPORT_H
#define	_CLINTERCONNECTEXPORT_H

#pragma ident	"@(#)ClinterconnectExport.h	1.2	08/05/20 SMI"

//
// This is the header file for the ClinterconnectExport class.
//

#include <libxml/tree.h>

#include "NameValue.h"
#include "NameValueList.h"

#include "ClcmdExport.h"

// The function to get the interconnect XML data
extern ClcmdExport *get_clinterconnect_xml_elements();

// Adapter types
enum adap_type_t {
    DLPI,
    RSM };

// Cable types
enum cable_t {
    SWITCH,
    ADAPTER };

// Endpoint struct
struct endpoint_t {
    char *name;
    cable_t type;
    char *node;
    char *port;
};

// Cable struct
typedef struct cltr_cable {
	uint_t  cable_id;
	endpoint_t epoint1;
	endpoint_t epoint2;
	struct cltr_cable *cable_next;
} cltr_cable_t;

class ClinterconnectExport : public ClcmdExport
{

public:

	// Create a new object
	ClinterconnectExport();

	// Add an adapter with the given name, node, state, and type
	int addTransportAdapter(
	    char *name,
	    char *node,
	    state_t state,
	    adap_type_t type);

	// Add an adapter property
	int addTransportAdapterProperty(
	    char *nodeName,
	    char *adapterName,
	    char *name,
	    char *value,
	    bool readonly = false);

	// Add an adapter property
	int addTransportAdapterProperty(
	    char *nodeName,
	    char *adapterName,
	    NameValue *nv,
	    bool readonly = false);

	// Add a list of adapter properties
	int addTransportAdapterProperties(
	    char *nodeName,
	    char *adapterName,
	    NameValueList *nvl,
	    bool readonly = false);

	// Add a transport type property
	int addTransportTypeProperty(
	    char *nodeName,
	    char *adapterName,
	    char *name,
	    char *value,
	    bool readonly = false);

	// Add a transport type property
	int addTransportTypeProperty(
	    char *nodeName,
	    char *adapterName,
	    NameValue *nv,
	    bool readonly = false);

	// Add a list of transport type properties
	int addTransportTypeProperties(
	    char *nodeName,
	    char *adapterName,
	    NameValueList *nvl,
	    bool readonly = false);

	// Add a switch with the given name, state, and optionally, port
	int addTransportSwitch(
	    char *name,
	    state_t state,
	    char *port = NULL);

	// Add a cable with the two endpoints and the state
	int addTransportCable(
	    endpoint_t end_1,
	    endpoint_t end_2,
	    state_t state);

private:

	// returns an xmlNode endpoint based off the data in the endpoint given
	xmlNode *createEndpointElement(endpoint_t end);

	// returns an xmlNode with the given adapter name, or NULL
	xmlNode *getAdapter(char *node, char *adapter);

	// returns an xmlNode with the given transport type, or NULL
	xmlNode *getTransportType(char *node, char *adapter);

};
#endif // _CLINTERCONNECTEXPORT_H
