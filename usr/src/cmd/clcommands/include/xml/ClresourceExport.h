//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef	_CLRESOURCEEXPORT_H
#define	_CLRESOURCEEXPORT_H

#pragma ident	"@(#)ClresourceExport.h	1.3	08/05/20 SMI"

//
// This is the header file for the ClresourcetypeExport class.
//

#include <libxml/tree.h>

#include "NameValue.h"
#include "NameValueList.h"

#include "ClcmdExport.h"

// The function to get the resource xml data
extern ClcmdExport *get_clresource_xml_elements(int rs_type);

class ClresourceExport : public ClcmdExport
{

public:

	// Create a new object
	ClresourceExport();

	// Add a resource
	int addResource(
	    char *rName,
	    char *rt,
	    char *rg);

	// Add a resource node
	int addResourceNode(
	    char *rName,
	    char *nodeRef,
	    state_t state,
	    bool monitored,
	    char *zone = NULL);

	// Add a property for a resource node
	int addResourceNodeProperty(
	    char *rName,
	    char *nodeRef,
	    char *name,
	    char *value,
	    char *type,
	    bool extension,
	    bool readonly = false);

	// Add a standard property to the resource
	int addStandardProperty(
	    char *rName,
	    char *name,
	    char *value,
	    char *type,
	    bool readonly = false);

	// Add a standard property to the resource
	int addStandardProperty(
	    char *rName,
	    NameValue *nv,
	    char *type,
	    bool readonly = false);

	// Add standard properties to the resource
	int addStandardProperties(
	    char *rName,
	    NameValueList *nvl,
	    char *type,
	    bool readonly = false);

	// Add an extension proeperty to the resource
	int addExtensionProperty(
	    char *rName,
	    char *name,
	    char *value,
	    char *type,
	    bool readonly = false);

	// Add an extension proeperty to the resource
	int addExtensionProperty(
	    char *rName,
	    NameValue *nv,
	    char *type,
	    bool readonly = false);

	// Add extension proeperties to the resource
	int addExtensionProperties(
	    char *rName,
	    NameValueList *nvl,
	    char *type,
	    bool readonly = false);

private:

	// Returns an xmlNode with the given resource name, or NULL
	xmlNode *getResource(char *rName);

	// Returns an xmlNode with the given resource/node name, or NULL
	xmlNode *getResourceNode(char *rName, char *nodeRef);
};
#endif // _CLRESOURCEEXPORT_H
