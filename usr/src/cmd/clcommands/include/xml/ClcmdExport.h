//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef	_CLCMDEXPORT_H
#define	_CLCMDEXPORT_H

#pragma ident	"@(#)ClcmdExport.h	1.2	08/05/20 SMI"

//
// This is the header file for the ClcmdExport class.
//

#include <stdio.h>
#include <libxml/tree.h>

#include "clcommands.h"

#define	CHECKVALUE(x)	if (!(x)) return (CL_EINTERNAL)

// State type
enum state_t {
    ENABLED,
    DISABLED };

// Property extension types
enum extension_t {
    TRUE,
    FALSE,
    MIXED,
    DOESNOTAPPLY };

class ClcmdExport
{

public:

	// Return the root node
	xmlNode *getRootNode();

	// Create an xmlNode with the given name
	xmlNode *newXmlNode(const char *name);

	// Add an attribute to the xmlNode with the given name and value
	void newXmlAttr(
	    xmlNode *node,
	    const char *name,
	    char *value);

	// Start the propertyList element
	int beginPropertyList(
	    xmlNode *parent,
	    bool readonly = false,
	    extension_t ext = DOESNOTAPPLY);

	// Add a property to the parent node
	int addProperty(
	    xmlNode *parent,
	    char *name,
	    char *value,
	    char *type,
	    bool readonly,
	    bool ro_propList = false,
	    extension_t ext_propList = DOESNOTAPPLY);

	// Add a property to the parent node
	int addProperty(
	    xmlNode *parent,
	    NameValue *nv,
	    char *type,
	    bool readonly,
	    bool ro_propList = false,
	    extension_t ext_propList = DOESNOTAPPLY);

	// Adds properties to the parent node
	int addProperties(
	    xmlNode *parent,
	    NameValueList *nvl,
	    char *type,
	    bool readonly,
	    bool ro_propList = false,
	    extension_t ext_propList = DOESNOTAPPLY);

	// print the xml to stdout.  Debug purposes only
	void dumpXML();

	// prints a common error
	void errorAddingElement(const char *elem, const char *root);

	// the xml root
	xmlNode *root;

};
#endif // _CLCMDEXPORT_H
