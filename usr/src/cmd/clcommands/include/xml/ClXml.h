//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef	_CLXML_H
#define	_CLXML_H

#pragma ident	"@(#)ClXml.h	1.6	08/05/20 SMI"

//
// This is the header file for the ClXml class.
//

#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/clconf_int.h>
#include <fcntl.h>
#include <unistd.h>
#include <libxml/parser.h>
#include <libxml/parserInternals.h>
#include <libxml/tree.h>
#include <libxml/valid.h>
#include <libxml/xpath.h>

#include "OptionValues.h"
#include "ValueList.h"
#include "clcommands.h"
#include "cl_errno.h"
#include "ClcmdExport.h"
#include "CldeviceExport.h"
#include "CldevicegroupExport.h"
#include "ClinterconnectExport.h"
#include "ClnasExport.h"
#include "ClnodeExport.h"
#include "ClquorumExport.h"
#include "ClresourceExport.h"
#include "ClresourcegroupExport.h"
#include "ClresourcetypeExport.h"
#include "ClsnmpmibExport.h"
#include "ClsnmphostExport.h"
#include "ClsnmpuserExport.h"
#include "CltelemetryattributeExport.h"
#include "ClusterExport.h"

// Temp file
#define	TMPFILE		/var/cluster/run/cl_xml.stdin

#define	max(x, y)    (((x) < (y)) ? (y) : (x))

#define	DTD_PATH	"/usr/cluster/lib/xml/cluster.dtd"

// When adding a command, be sure to keep the this value
// up to date, otherwise the array of function pointers
// won't work.  Also, be sure to add the value to the
// command_t enum defined below.
#define	NUMBER_OF_COMMANDS	16

#define	CLUSTER_CMD			cluster_cmd
#define	CLDEVICE_CMD			cldevice_cmd
#define	CLDEVICEGROUP_CMD		cldevicegroup_cmd
#define	CLINTERCONNECT_CMD		clinterconnect_cmd
#define	CLNAS_CMD			clnas_cmd
#define	CLNODE_CMD			clnode_cmd
#define	CLQUORUM_CMD			clquorum_cmd
#define	CLRESOURCE_CMD			clresource_cmd
#define	CLRESLOGICALHOSTNAME_CMD	clreslogicalhostname_cmd
#define	CLRESSHAREDADDRESS_CMD		clressharedaddress_cmd
#define	CLRESOURCEGROUP_CMD		clresourcegroup_cmd
#define	CLRESOURCETYPE_CMD		clresourcetype_cmd
#define	CLSNMPMIB_CMD			clsnmpmib_cmd
#define	CLSNMPHOST_CMD			clsnmphost_cmd
#define	CLSNMPUSER_CMD			clsnmpuser_cmd
#define	CLTELEMETRYATTRIBUTE_CMD	cltelemetryattribute_cmd

// Enum of commands
enum command_t {
    CLUSTER_CMD,
    CLDEVICE_CMD,
    CLDEVICEGROUP_CMD,
    CLINTERCONNECT_CMD,
    CLNAS_CMD,
    CLNODE_CMD,
    CLQUORUM_CMD,
    CLRESOURCE_CMD,
    CLRESLOGICALHOSTNAME_CMD,
    CLRESSHAREDADDRESS_CMD,
    CLRESOURCEGROUP_CMD,
    CLRESOURCETYPE_CMD,
    CLSNMPMIB_CMD,
    CLSNMPHOST_CMD,
    CLSNMPUSER_CMD,
    CLTELEMETRYATTRIBUTE_CMD };

class ClXml {

public:

	ClXml();

	~ClXml();

	// Applies the xml configuration for the specific command
	// and it arguments.
	//
	// Returns a CL error code
	//
	// Arguments:
	//	cmd:		the command, as defined above
	//	*input_file:	the input filename
	//	*opts:		an OptionValues object with all of the
	//			options and operands which where given
	// 			on the command line
	int applyConfig(
	    command_t		cmd,
	    char		*input_file,
	    OptionValues	*opts);


	// Puts all of the object names in a ValueList for the given
	// command.
	//
	// Returns a CL error code
	//
	// Arguments:
	//	cmd:		the command, as defined above
	//	*input_file:	the input filename
	//	*list:		a ValueList object with which to populate
	//			with the object names
	int getAllObjectNames(
	    command_t	cmd,
	    char	*input_file,
	    ValueList	*list);

	// Validates a list of operands to ensure that they exist in
	// the XML file.
	//
	// Returns a CL error code
	//
	// Arguments:
	//	cmd:		the command, as defined above
	//	*input_file:	the input filename
	//	*operands:	a ValueList of operands to validate
	int validateOperands(
	    command_t	cmd,
	    char	*intput_file,
	    ValueList	*operands);

	// Creates an XML file specified by output_file, containing
	// the data contained in the ClcmdExport object.
	//
	// Returns a CL error code
	//
	// Arguments:
	//	cmd:		the command, as defined above
	//	*output_file:	the output filename
	//	*export_obj:	a ClcmdExport subclass object containing
	//			the config information for the given
	//			command
	//	*obj_types	cluster object types. Used with CLUSTER_CMD
	int createExportFile(
	    command_t	cmd,
	    char 	*output_file,
	    ClcmdExport	*export_obj,
	    ValueList	*obj_types = NULL);

	// Get the properties of an XML object of the given name.
	// The properies are saved in the NameValueList.
	//
	// Returns a CL error code
	//
	// Arguments:
	//	command		the command, as defined above
	//	*obj_name	the object name
	//	tr_type		interconnect object type
	//	*input_file	the input xml file
	//	*NameValueList	the properties
	int getObjProps(command_t command, char *obj_name,
	    char *input_file, NameValueList *obj_props);

	// Get Cable and their properties
	int getCableProps(char *obj_name, char *input_file,
	    cltr_cable_t **cable_props);

	//
	// Applies the resource group property settings from the XML
	// for each RG specified, in order.  Returns a CL error code.
	//
	int RGPostProcessor(char *rgname, NameValueList &props,
	    optflgs_t optflgs);

	//
	// Applies the resource property settings from the XML
	// for each RS specified, in order.  Returns a CL error code.
	//
	int RSPostProcessor(char *rsname, NameValueList &props,
	    optflgs_t optflgs);

	//
	// Populates the ValueList, rgs, with the name of a resource
	// group.  The resource group name is the name as specified by
	// each resource specified in the ValueList res.
	//
	// The rgs ValueList will not contain duplicates, but will be
	// sorted in order of the resources specified in the res ValueList.
	//
	clerrno_t
	ClXml::getAllRGsFromResources(ValueList &res, ValueList &rgs);

	//
	// Figure out the RT name for the specified RS name in the xml
	//
	clerrno_t
	ClXml::getRTFromResource(char *rsname, char **rt_xml);

	//
	// Figure out the RG name for the specified RS name in the xml
	//
	clerrno_t
	ClXml::getRGFromResource(char *rsname, char **rg_xml);

	//
	// Go through rs_list and add dependents, set the list_changed flag if
	// dependets found.
	//
	clerrno_t
	ClXml::getDependents(char *rsname, NameValueList &deps);

	void reset_error();

private:

	void set_error(int error);

	// Adds an xml element to another
	int addElementToRoot(xmlNode *element, xmlNode *root, char *name);

	// Puts stdin into TMPFILE, so that it can be read
	char *stdinToFile();

	// Prints all the element names of the given xmlNode
	void printElementNames(xmlNode *node);

	// Gets the XML doc from the filename.
	int getDoc(char *filename);

	// Parses the XML doc to ensure that it is valid
	int parseDoc(char *filename);

	// Evaluate the xpath statement
	int evaluateXpath(char *statement);

	// Calls the specific function based on the command_t
	// which returns the xpath statement to be run
	ValueList *getXpathExpressions(
	    command_t    cmd,
	    OptionValues *opts);

	// Populates the ValueList with the names of the interconnects
	// in the xml file
	int getAllInterconnectNames(ValueList *list);

	// Get the endpoints properties which is on node *node_name
	int getEndpointsProps(char *node_name, cltr_cable_t **cable_props);

	// Gets a cable string from the xmlNode data
	char *getCableEndpoints(xmlNode *cable);

	// Returns whether or not the endpoints in a specific cable
	// are defined in the XML file.
	bool endpointCableComponentsDefined(ValueList *comps, char *cable);

	// Returns a ValueList of the community names found in the XML file,
	// or NULL on any error.
	ValueList *getSNMPHostCommunityNames();

	void genericPropertyOverrides(
	    xmlNode *proplist,
	    NameValueList *overrides,
	    int propid,
	    OptionValues *new_opts);

	xmlNode *getClusterElement();

	char *cmd_xpath[NUMBER_OF_COMMANDS];

	typedef ValueList *(ClXml::*filters)(OptionValues *);
	typedef int(ClXml::*processor)(xmlNode *, OptionValues *);

	// Array of filter methods
	filters filter_method[NUMBER_OF_COMMANDS];

	// Actual filter methods
	ValueList *clusterFilter(OptionValues *opts);
	ValueList *cldeviceFilter(OptionValues *opts);
	ValueList *cldevicegroupFilter(OptionValues *opts);
	ValueList *clinterconnectFilter(OptionValues *opts);
	ValueList *clnasFilter(OptionValues *opts);
	ValueList *clnodeFilter(OptionValues *opts);
	ValueList *clquorumFilter(OptionValues *opts);
	ValueList *clresourceFilter(OptionValues *opts);
	ValueList *clreslogicalhostnameFilter(OptionValues *opts);
	ValueList *clressharedaddressFilter(OptionValues *opts);
	ValueList *clresourcegroupFilter(OptionValues *opts);
	ValueList *clresourcetypeFilter(OptionValues *opts);
	ValueList *clsnmpmibFilter(OptionValues *opts);
	ValueList *clsnmphostFilter(OptionValues *opts);
	ValueList *clsnmpuserFilter(OptionValues *opts);
	ValueList *cltelemetryattributeFilter(OptionValues *opts);

	// Array of processor methods
	processor processor_method[NUMBER_OF_COMMANDS];

	// Actual processor methods
	int clusterProcessor(xmlNode *node, OptionValues *opts);
	int cldeviceProcessor(xmlNode *node, OptionValues *opts);
	int cldevicegroupProcessor(xmlNode *node, OptionValues *opts);
	int clinterconnectProcessor(xmlNode *node, OptionValues *opts);
	int clnasProcessor(xmlNode *node, OptionValues *opts);
	int clnodeProcessor(xmlNode *node, OptionValues *opts);
	int clquorumProcessor(xmlNode *node, OptionValues *opts);
	int clresourceProcessor(xmlNode *node, OptionValues *opts);
	int clreslogicalhostnameProcessor(xmlNode *node, OptionValues *opts);
	int clressharedaddressProcessor(xmlNode *node, OptionValues *opts);
	int clresourcegroupProcessor(xmlNode *node, OptionValues *opts);
	int clresourcetypeProcessor(xmlNode *node, OptionValues *opts);
	int clsnmpmibProcessor(xmlNode *node, OptionValues *opts);
	int clsnmphostProcessor(xmlNode *node, OptionValues *opts);
	int clsnmpuserProcessor(xmlNode *node, OptionValues *opts);
	int cltelemetryattributeProcessor(xmlNode *node, OptionValues *opts);

	int error;
	xmlDocPtr doc;
	xmlXPathContextPtr xpathCtx;
	xmlXPathObjectPtr xpathObj;

};

#endif // _CLXML_H
