//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef	_CLRESOURCEGROUPEXPORT_H
#define	_CLRESOURCEGROUPEXPORT_H

#pragma ident	"@(#)ClresourcegroupExport.h	1.3	08/05/20 SMI"

//
// This is the header file for the ClresourcegroupExport class.
//

#include <libxml/tree.h>

#include "NameValue.h"
#include "NameValueList.h"

#include "ClcmdExport.h"

// The function to get the resource group xml data
extern ClcmdExport *get_clresourcegroup_xml_elements(bool include_resources);

// failover types
enum failover_t {
    FAILOVER,
    SCALABLE };

class ClresourcegroupExport : public ClcmdExport
{

public:

	// Create a new object
	ClresourcegroupExport();

	// Add a resource group
	int addResourcegroup(
	    char *rgName,
	    failover_t failover,
	    bool managed);

	// Add a resource group property
	int addResourcegroupProperty(
	    char *rgName,
	    char *name,
	    char *value,
	    bool readonly = false);

	// Add a resource group property
	int addResourcegroupProperty(
	    char *rgName,
	    NameValue *nv,
	    bool readonly = false);

	// Add resource group properties
	int addResourcegroupProperties(
	    char *rgName,
	    NameValueList *nvl,
	    bool readonly = false);

	// Add a resource group node
	int addResourcegroupNode(char *rgName, char *node, char *zone = NULL);

	// Add a resource group resource
	int addResourcegroupResource(char *rgName, char *resName);

private:

	// Returns an xmlNode with the given rg name, or NULL
	xmlNode *getResourcegroup(char *rgName);

};
#endif // _CLRESOURCEGROUPEXPORT_H
