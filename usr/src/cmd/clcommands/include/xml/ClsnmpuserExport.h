//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef	_CLSNMPUSEREXPORT_H
#define	_CLSNMPUSEREXPORT_H

#pragma ident	"@(#)ClsnmpuserExport.h	1.3	08/05/20 SMI"

//
// This is the header file for the ClsnmpuserExport class.
//

#include <libxml/tree.h>

#include "NameValue.h"
#include "NameValueList.h"

#include "ClcmdExport.h"

// authentication types
enum auth_t {
    MD5,
    SHA };

class ClsnmpuserExport : public ClcmdExport
{

public:

	// Create a new object
	ClsnmpuserExport();

	// Create a SNMP user
	int createSNMPUser(
	    char *name,
	    char *node,
	    auth_t auth,
	    bool def = false,
	    char *seclevel = NULL);

};

// function to get the snmp user xml data
extern ClcmdExport *get_clsnmpuser_xml_elements();

#endif // _CLSNMPUSEREXPORT_H
