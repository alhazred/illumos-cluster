//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef	_CLSNMPHOSTEXPORT_H
#define	_CLSNMPHOSTEXPORT_H

#pragma ident	"@(#)ClsnmphostExport.h	1.2	08/05/20 SMI"

//
// This is the header file for the ClsnmphostExport class.
//

#include <libxml/tree.h>

#include "NameValue.h"
#include "NameValueList.h"

#include "ClcmdExport.h"

class ClsnmphostExport : public ClcmdExport
{

public:

	// Create a new object
	ClsnmphostExport();

	// Create an SNMP host
	int createSNMPHost(
	    char *name,
	    char *node,
	    char *community);

};

// The function to get the snmp host xml data
extern ClcmdExport *get_clsnmphost_xml_elements();

#endif // _CLSNMPHOSTEXPORT_H
