/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_CL_ERRNO_H
#define	_CL_ERRNO_H

#pragma ident	"@(#)cl_errno.h	1.2	08/05/20 SMI"

#ifdef __cplusplus
extern "C" {
#endif

/* CL error number type */
typedef int	clerrno_t;

/*
 * Error codes
 */
#define	CL_NOERR 		0	/* No error */
#define	CL_ENOMEM  		1	/* Not enough swap space */
#define	CL_EINVAL  		3	/* Invalid argument */
#define	CL_ERECONF		5	/* Cluster is reconfiguring */
#define	CL_EACCESS		6	/* Permission denied */
#define	CL_ESTATE 		9	/* Unexpected state */
#define	CL_EMETHOD		10	/* Resource method failed */
#define	CL_EPROP  		15	/* Invalid property */
#define	CL_EINTERNAL  		18	/* Internal error */
#define	CL_EIO			35	/* I/O error */
#define	CL_ENOENT		36	/* No such object */
#define	CL_EOP			37	/* Operation is not allowed */
#define	CL_EBUSY		38	/* Object busy */
#define	CL_EEXIST		39	/* Object already exists */
#define	CL_ETYPE		41	/* Unknown type */
#define	CL_ECLMODE		50	/* Node is in cluster mode */
#define	CL_ENOTCLMODE		51	/* Node is not in cluster mode */

#ifdef __cplusplus
}
#endif

#endif	/* _CL_ERRNO_H */
