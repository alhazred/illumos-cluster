//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef	_CLGETTEXT_H
#define	_CLGETTEXT_H

#pragma ident	"@(#)GetText.h	1.3	08/05/20 SMI"

//
// This is the header file for the GetText class.
//

#include "PropertyResourceBundle.h"

#include <stdio.h>

class GetText {
public:
	// Constructors

	// Basic constructor
	GetText();

	// Constructor with resource bundle
	GetText(PropertyResourceBundle &bundle);

	// Destructor
	~GetText();


	// Member functions

	// Bind to a resource bundle
	void bindPropertyResourceBundle(PropertyResourceBundle &bundle);

	// Get the resource bundle used by this instance
	PropertyResourceBundle getPropertyResourceBundle(void);

	// Get localized text
	const char *getText(char *text);

	// Get localized text by it's message ID
	const char *getTextFromId(char *msgid);

	// Get detailed description associated with message ID
	const char *getDetailsFromId(char *msgid);

	// Get suggested course of action associate with message ID
	const char *getActionFromId(char *msgid);

	// Get text using a resource property key.
	const char *getString(char *key);

	// Get the message ID of a string
	char *getMessageId(char *string);

private:
	// The resource bundle to use for localization
	PropertyResourceBundle *pbundle;
};

#endif /* _CLGETTEXT_H */
