//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef _CLSHOW_H
#define	_CLSHOW_H

#pragma ident	"@(#)ClShow.h	1.11	09/01/16 SMI"

//
// This is the header file for the ClShow class.
//

#define	COL_WIDTH	48
#define	MIN_SPACE	3

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <list>
#include <iterator>

#include "NameValue.h"
#include "NameValueList.h"

#ifdef WEAK_MEMBERSHIP
#define	NUM_HEADINGS	30
#else
#define	NUM_HEADINGS	29
#endif

// The headings
// When adding or removing a new heading,
// be sure to update NUM_HEADINGS
enum show_heading_t {
	HEAD_CLUSTER,
	HEAD_HOSTACCESS,
	HEAD_PRIVNET,
	HEAD_NODES,
	HEAD_FNODES,
	HEAD_SWITCHES,
	HEAD_CABLES,
	HEAD_QUORUMDEVICES,
#ifdef WEAK_MEMBERSHIP
	HEAD_GLOBALQUORUM,
#endif
	HEAD_DEVICEGROUPS,
	HEAD_RESTYPES,
	HEAD_RESGROUPS,
	HEAD_RESOURCES,
	HEAD_DID,
	HEAD_NASDEVICE,
	HEAD_SNMPMIB,
	HEAD_SNMPHOST,
	HEAD_SNMPUSER,
	HEAD_TELEATTR,
	HEAD_ZONECLUSTER,
	HEAD_ZC_NODES,
	SUBHEAD_NODETRANSPORT,
	SUBHEAD_RGRESOURCES,
	SUBHEAD_RTMETHODS,
	SUBHEAD_UPGTUN,
	SUBHEAD_RTPARAMS,
	SUBHEAD_RSPROPS,
	SUBHEAD_ZONES,
	SUBHEAD_TAINST,
	SUBHEAD_ZC_NODES,
	SUBHEAD_ZC_RESOURCES};

class ClShow {

public:

	struct show_object_t {
		char *name;
		char *value;
		int object_num;
		NameValueList *properties;
		std::list<ClShow *> children;
	};

	bool hasHeading;

	// If this is true, then the NameValueList in show_object_t
	// is as NameValueList(true).
	bool propSet;

	// Only print newline if this is set.
	bool print_NL;

	// Construct a new object
	ClShow();

	// Construct a new object with the specified heading
	ClShow(show_heading_t heading);

	// Construct a new object with the specified heading
	// The variable <var> represents a heading variable,
	// and is only required should the specified heading
	// require a variable, too.
	ClShow(show_heading_t heading, char *var);

	// Deconstructor
	~ClShow();

	// Add an object name and value
	void addObject(char *name, char *value);

	//
	// Add a numbered object, the number is used only to decide if a
	// newline should be printed before printing the object.
	// Number 0 is used if it is a series of single line objects, like
	// RT methods. By default, number 1 is used. number > 1 is used for
	// per-node property objects. A newline is printed only if number is 1
	//
	void addNumberedObject(char *name, char *value, int obj_num);

	// Add an object using the given NameValue
	void addObject(NameValue *nv);

	// Add a property name and value to the object name
	void addProperty(char *object, char *name, char *value);

	// Add a RS property name and value to the object name
	void addRSProperty(char *object, char *name, char *value);

	// Add a property as the first property in the last
	// for the given object
	void addPropertyFirst(char *object, char *name, char *value);

	// Add a property using the given NameValue
	void addProperty(char *object, NameValue *nv);

	// Add all properties in the NameValueList given.
	void addProperties(char *object, NameValueList *nvl);

	// Add a child ClShow object.  Returns 1 if the child could
	// not be added, 0 otherwise.
	int addChild(char *object, ClShow *child);

	// Object iterator methods
	void startObjects();
	void nextObjects();
	int endObjects();
	show_object_t *currentObject();

	// Determine if an object has already been added
	bool objectExists(char *name);

	// Objects size
	int getObjectSize();

	// Print the data
	int print();

	// Print the data with the specified ClShow obj and level
	// This is only meant for use by the main() function.  Do
	// NOT use this function from other callers.
	int print(ClShow *obj, int level);

	// get the heading
	char *getHeading();

private:

	// initialize the data
	void init();

	int heading;
	char *h_var;
	std::list<show_object_t *> objects;
	std::list<show_object_t *>::iterator object_iter;

};

#endif // _CLSHOW_H
