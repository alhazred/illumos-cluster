//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef	_CLDEVICE_H
#define	_CLDEVICE_H

#pragma ident	"@(#)cldevice.h	1.5	08/05/20 SMI"

#include "clcommands.h"
#include "ClCommand.h"
#include "CldeviceExport.h"
#include "ClXml.h"
#include "ClShow.h"
#include "ClList.h"
#include "ClStatus.h"
#include "clsnmpmib.h"
#include "didadm.h"

#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>

#define	SCGDEVS		"/usr/cluster/bin/scgdevs"
#define	SCDIDADM	"scdidadm"
#define	SCDPM		"scdpm"

//
// This is the main header file for cldevice functions
//

// Subcommand processing functions
extern clerrno_t cldevice_check(ClCommand &cmd, ValueList &nodes);
extern clerrno_t cldevice_clear(ClCommand &cmd, ValueList &nodes);
extern clerrno_t cldevice_combine(ClCommand &cmd, char *dev, char *destdev,
    char *repl_type, char *repl_dev);
extern clerrno_t cldevice_export(ClCommand &cmd, ValueList &operands,
    ValueList &nodes, char *clconfig);
extern clerrno_t cldevice_list(ClCommand &cmd, ValueList &operands,
    ValueList &nodes);
extern clerrno_t cldevice_monitor(ClCommand &cmd, ValueList &operands,
    ValueList &nodes);
extern clerrno_t cldevice_monitor(ClCommand &cmd, ValueList &operands,
    ValueList &nodes, char *clconfig);
extern clerrno_t cldevice_populate(ClCommand &cmd);
extern clerrno_t cldevice_refresh(ClCommand &cmd, ValueList &nodes);
extern clerrno_t cldevice_rename(ClCommand &cmd, char *dev, char *destdev);
extern clerrno_t cldevice_repair(ClCommand &cmd, ValueList &operands,
    ValueList &nodes);
extern clerrno_t cldevice_replicate(ClCommand &cmd, char *dest_node,
    char *repl_type, char *src_node);
extern clerrno_t cldevice_set(ClCommand &cmd, ValueList &operands,
    NameValueList &props);
extern clerrno_t cldevice_show(ClCommand &cmd, ValueList &operands,
    ValueList &nodes);
extern clerrno_t cldevice_status(ClCommand &cmd, ValueList &operands,
    ValueList &nodes, ValueList &states);
extern clerrno_t cldevice_unmonitor(ClCommand &cmd, ValueList &operands,
    ValueList &nodes);
extern clerrno_t cldevice_unmonitor(ClCommand &cmd, ValueList &operands,
    ValueList &nodes, char *clconfig);

extern clerrno_t get_cldevice_show_obj(optflgs_t flags, ClShow *show_obj);
extern clerrno_t get_cldevice_filtered_show_obj(ClShow *show_obj,
    ValueList &devlist, ValueList &nodelist, bool verbose);

extern clerrno_t get_cldevice_status_obj(optflgs_t flags, ClStatus *status_obj);
extern clerrno_t get_cldevice_filtered_status_obj(ClStatus *status_obj,
    ValueList &devlist, ValueList &nodelist, ValueList &states, bool verbose);

// Helper functions
extern clerrno_t run_cmd_on_node(char *cmd, char *args, char *node);
extern clerrno_t run_scgdevs();
extern clerrno_t exec_cmd(char **argv);
extern clerrno_t exec_cmd(char *cmd);
extern char **conv_string_to_array(char *str);
extern clerrno_t pipe_cmd(FILE **file, char *cmd);
extern int device_exists(char *dev, int global, int &err);
extern int device_exists_on_node(char *dev, char *node, int &err);
extern clerrno_t set_cldevice_env();
extern clerrno_t get_all_devices(ValueList &devs);
extern int is_device_instance_num(char *name);
extern clerrno_t convert_operands_to_instances(ValueList &operands);
extern clerrno_t parse_didadm_output(ClShow *show_obj, FILE *fp, bool verbose);
extern clerrno_t get_export_obj(CldeviceExport &export_obj, ValueList &operands,
    ValueList &nodes);
extern clerrno_t get_device_instances(CldeviceExport &export_obj,
    ValueList &operands, ValueList &nodes);
extern clerrno_t get_device_paths(CldeviceExport &export_obj,
    ValueList &operands, ValueList &nodes);


#endif /* _CLDEVICE_H */
