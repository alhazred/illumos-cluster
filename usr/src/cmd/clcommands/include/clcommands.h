//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef	_CLCOMMANDS_H
#define	_CLCOMMANDS_H

#pragma ident	"@(#)clcommands.h	1.23	09/01/16 SMI"

#ifdef __cplusplus
#include <sys/os.h>
#endif

#include "ValueList.h"
#include "NameValueList.h"
#include "ClCommand.h"
#include "TextMessage.h"

#include "clmessages.h"
#include "cl_errno.h"
#include "clgetopt.h"

#include <rgm/sczones.h>
#include <scadmin/scconf.h>
#include <sys/cl_auth.h>
#include <sys/cl_cmd_event.h>

#include <new>
#include <list>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <errno.h>

#include <sys/types.h>
#include <sys/debug.h>

//
// This is the main header file for common clcommands functions
//

//
// Miscellaneous defines
//
#define	CLCOMMANDS_COMMON_VERSION	"1.0"	// Command version
#define	CLCOMMANDS_WILD_OPERAND		"+"	// Operand wildcard
#define	CLCOMMANDS_BUFSIZ		1024	// Large general-use buf size

// Macros for clerror(), clwarning(), clmessage(), gettext() and getstring()
#ifndef CLCOMMANDS_NO_MSG_MACROS
#define	clerror			clerrorMessage.printError
#define	clwarning		cltextMessage.printWarning
#define	clmessage		cltextMessage.printMessage
#define	gettext			gettextGetText.getText
#define	getstring		getstringGetText.getString
#endif CLCOMMANDS_NO_MSG_MACROS

//
// Option flags
//
// These flags represent boolean options that can be
// specified on the command line.
//
typedef uint32_t optflgs_t;
#define	aflg	(1 << 0)			// --automatic
#define	dflg	(1 << 1)			// --disable, --default
#define	eflg	(1 << 2)			// --enable
#define	Fflg	(1 << 3)			// --force
#define	kflg	(1 << 4)			// --kill
#define	lflg	(1 << 5)			// --limited
#define	mflg	(1 << 6)			// --monitor
#define	Nflg	(1 << 7)			// --allnodes
#define	qflg	(1 << 8)			// --autoconfig
#define	Rflg	(1 << 9)			// --recursive
#define	Sflg	(1 << 10)			// --scalable
#define	vflg	(1 << 11)			// --verbose
#define	Vflg	(1 << 12)			// --version
#define	yflg	(1 << 13)			// --yes
#define	hflg	(1 << 14)			// --help
#define	sflg	(1 << 15)			// --short form
#define	Mflg	(1 << 16)			// --manage
#define	xflg	(1 << 17)			// --extra verbose
#define	uflg	(1 << 18)			// --suspended
#define	fflg	(1 << 19)			// --command file
#define	Zflg	(1 << 20)			// --zone cluster

//
// The following two are not real user specified options, they indicate if
// the special forms of the clresource command was used.
//
#define	LHflg	(1 << 31)			// LH command
#define	SAflg	(1 << 30)			// SA command

//
// Option descriptions
//
// All option descriptions should be broken up into lines of no
// greater than 70 characters.  All but the trailing newline ('\n')
// itself should be included in the option descriptions defined here.
//
#define	CLDES_OPT_ALLNODES	\
	GETTEXT("Make resource type available on all cluster nodes.")
#define	CLDES_OPT_AUTO		\
	GETTEXT("Automatically create other required objects.")
#define	CLDES_OPT_AUTOQ		\
	GETTEXT(		\
	"Automatically add a Shared Disk quorum device for 2-node cluster.")
#define	CLDES_OPT_AUXNODES	\
	GETTEXT("Specify the AuxNodeList property.")
#define	CLDES_OPT_AVG		\
	GETTEXT("Print average of instrumentation data in 3 hours.")
#define	CLDES_OPT_CLUSTER	\
	GETTEXT("Specify the cluster name.")
#define	CLDES_OPT_CTYPE		\
	GETTEXT("Specify the cluster object type.")
#define	CLDES_OPT_DATERANGE	\
	GETTEXT("Specify the date range to collect telemetry data.")
#define	CLDES_OPT_DIR		\
	GETTEXT("Specify one or more exported directories.")
#define	CLDES_OPT_DISABLE	\
	GETTEXT("Disable.")
#define	CLDES_OPT_ENABLE	\
	GETTEXT("Enable.")
#define	CLDES_OPT_ENDPOINT	\
	GETTEXT("Specify one cluster transport connection.")
#define	CLDES_OPT_FORCE		\
	GETTEXT("Force.")
#define	CLDES_OPT_GLBMNT	\
	GETTEXT("Specify the global device mount point.")
#define	CLDES_OPT_GMT	\
	GETTEXT("Specify to use the GMT time format.")
#define	CLDES_OPT_GRACE		\
	GETTEXT("Specify the grace period in seconds before shutdown.")
#define	CLDES_OPT_INSTANCE	\
	GETTEXT("Specify telemetry object instance.")
#define	CLDES_OPT_KILL		\
	GETTEXT("Kill.")
#define	CLDES_OPT_LHOST		\
	GETTEXT("Specify a list of logical host names.")
#define	CLDES_OPT_MANAGE	\
	GETTEXT("Manage resource groups.")
#define	CLDES_OPT_MONITOR	\
	GETTEXT("Monitor resource groups.")
#define	CLDES_OPT_MSG		\
	GETTEXT("Specify the standard system message.")
#define	CLDES_OPT_NETIFLIST	\
	GETTEXT("Specify the NetIfList property.")
#define	CLDES_OPT_OTYPE	\
	GETTEXT("Specify telemetry object type.")
#define	CLDES_OPT_PASSWD	\
	GETTEXT("Specify the password file.")
#define	CLDES_OPT_HELP		\
	GETTEXT("Display this help text.")
#define	CLDES_OPT_IPMP		\
	GETTEXT("Print the IPMP group status.")
#define	CLDES_OPT_INPUT		\
	GETTEXT("Specify XML configuration as input.")
#define	CLDES_OPT_PROPERTY	\
	GETTEXT("Specify the properties.")
#define	CLDES_OPT_NODE		\
	GETTEXT("Specify one or more nodes.")
#define	CLDES_OPT_SINGLE_NODE	\
	GETTEXT("Specify a node.")
#define	CLDES_OPT_OUTPUT	\
	GETTEXT("Specify output XML configuration file.")
#define	CLDES_OPT_OUTPUTDIR	\
	GETTEXT("Specify the report directory.")
#define	CLDES_OPT_RG		\
	GETTEXT("Specify one or more resource groups.")
#define	CLDES_OPT_RG_PROPERTY	\
	GETTEXT("Set a resource group property.")
#define	CLDES_OPT_RG_PROPERTIES	\
	GETTEXT("Specify one or more resource group property.")
#define	CLDES_OPT_RG_STATE	\
	GETTEXT("Specify one or more resource group states.")
#define	CLDES_OPT_RG_TIME	\
	GETTEXT("Specify a time value to the evacuate operation.")
#define	CLDES_OPT_RS		\
	GETTEXT("Specify one or more resources.")
#define	CLDES_OPT_RS_ERRFLAG	\
	GETTEXT("Specify an error flag.")
#define	CLDES_OPT_RS_LISTTYPE	\
	GETTEXT("List \"extension\", \"standard\", or \"all\" properties.")
#define	CLDES_OPT_RS_PROPERTY	\
	GETTEXT("Set a standard or extension resource property.")
#define	CLDES_OPT_RS_PROPERTIES	\
	GETTEXT("Specify one or more standard or extension resource property.")
#define	CLDES_OPT_RS_RECURSIVE_DISABLE	\
	GETTEXT("Recursively disable other resources based on dependencies.")
#define	CLDES_OPT_RS_RECURSIVE_ENABLE	\
	GETTEXT("Recursively enable other resources based on dependencies.")
#define	CLDES_OPT_RS_XPROPERTY	\
	GETTEXT("Set a resource extension property.")
#define	CLDES_OPT_RS_XPROPERTIES\
	GETTEXT("Specify one or more resource extension properties.")
#define	CLDES_OPT_RS_YPROPERTY	\
	GETTEXT("Set a standard resource property.")
#define	CLDES_OPT_RS_YPROPERTIES	\
	GETTEXT("Specify one or more standard resource properties.")
#define	CLDES_OPT_RG		\
	GETTEXT("Specify one or more resource groups.")
#define	CLDES_OPT_RS_STATE 	\
	GETTEXT("Specify one or more resource states.")
#define	CLDES_OPT_RT		\
	GETTEXT("Specify one or more resource types.")
#define	CLDES_OPT_RTRFILE	\
	GETTEXT("Specify the full path to an RTR file or a directory.")
#define	CLDES_OPT_RT_PROPERTY	\
	GETTEXT("Set a resource type property.")
#define	CLDES_OPT_RT_PROPERTIES	\
	GETTEXT("Specify one or more resource type property.")
#define	CLDES_OPT_SCALABLE	\
	GETTEXT("Perform scalable resource group specific actions.")
#define	CLDES_OPT_SEVERITY	\
	GETTEXT("Specify the minimum severity level to report on.")
#define	CLDES_OPT_SHORT		\
	GETTEXT("Short format output.")
#define	CLDES_OPT_SNODE		\
	GETTEXT("Specify the sponsor node.")
#define	CLDES_OPT_WNODE		\
	GETTEXT("Specify the winner node.")
#define	CLDES_OPT_TIME		\
	GETTEXT("Specify time in seconds.")
#define	CLDES_OPT_TYPE		\
	GETTEXT("Specify the device type.")
#define	CLDES_OPT_NTYPE		\
	GETTEXT("Specify the node type.")
#define	CLDES_OPT_USERID		\
	GETTEXT("Specify the userid to access the NAS device.")
#define	CLDES_OPT_VERBOSE	\
	GETTEXT("Verbose output.")
#define	CLDES_OPT_VERSION	\
	GETTEXT("Displays the version of this command.")
#define	CLDES_OPT_YES		\
	GETTEXT("Pre-answers the confirmation question.")
#define	CLDES_OPT_ZONE		\
	GETTEXT("Specify a zone.")
#define	CLDES_OPT_COMMUNITY	\
	GETTEXT("Specify a community name.")
#define	CLDES_OPT_DEFAULT	\
	GETTEXT("Specify the default user flag.")
#define	CLDES_OPT_SECLEVEL	\
	GETTEXT("Specify the SNMP security level.")
#define	CLDES_OPT_SNMP_AUTH	\
	GETTEXT("Specify the SNMP authentication protocol.")
#define	CLDES_OPT_DESTDEVICE	\
	GETTEXT("Specify the destination device.")
#define	CLDES_OPT_DESTNODE	\
	GETTEXT("Specify the destination node.")
#define	CLDES_OPT_REPLTYPE	\
	GETTEXT("Specify the replication type.")
#define	CLDES_OPT_REPLDEV	\
	GETTEXT("Specify the replication device group.")
#define	CLDES_OPT_SRCNODE	\
	GETTEXT("Specify the source node.")
#define	CLDES_OPT_DEV_STATE	\
	GETTEXT("Specify one or more device states.")
#define	CLDES_OPT_DT		\
	GETTEXT("Specify device group type(s).")
#define	CLDES_OPT_DG_DEVICE	\
	GETTEXT("Specify one or more devices.")
#define	CLDES_OPT_DG_PROPERTY	\
	GETTEXT("Specify a device group property.")
#define	CLDES_OPT_CHECKVERBOSE	\
	GETTEXT("Display verbose output for program progress.")
#define	CLDES_OPT_XVERBOSE	\
	GETTEXT("Display Sun Explorer messages as well as verbose messages.")
#define	CLDES_OPT_SUSPENDED	\
	GETTEXT("Include suspended resource groups in the operation.")
#define	CLDES_OPT_COMMANDFILE		\
	GETTEXT("Specify a command file.")
#define	CLDES_OPT_ZC_NAME		\
	GETTEXT("Specify a zone cluster name.")
#define	CLDES_OPT_TGT_ZC_NAME		\
	GETTEXT("Specify a target zone cluster name.")
#define	CLDES_OPT_ZONEPATH		\
	GETTEXT("Specify the new zonepath.")
#define	CLDES_OPT_ZC_COPYMETHOD		\
	GETTEXT("Specify a zonecluster copy method.")
//
// Each command defines it's own command initialization data.
// This is used to intialize the ClCommand class for each command.
//
struct clcommandinit {
	char *command_version;			// Version of this command
	char *command_description;		// Command description
	char **command_usage;			// Command usage
	struct clsubcommand *command_subcommands; // Possible subcommands
};
typedef struct clcommandinit clcommandinit_t;

//
// Options.
//
// All option descriptions in cloptions_t should be initialized with
// text broken up into lines of no greater than 70 characters.  All
// but the trailing newline ('\n') itself should be included in the
// option descriptions used with cloptions_t.
//
#define	CLOPT_ARG_LIST			(1 << 0)	// [-n <node>[,...]]
#define	CLOPT_UNDOCUMENTED		(1 << 1)	// not in usage msg
struct cloptions {
	char shortoption;			// Short option letter
	char *longoption;			// Long option name
	char *optionarg;			// Description of argument
	unsigned int flags;			// Flags
	char *description;			// Option description
};
typedef struct cloptions cloptions_t;

//
// Each command is associated with a list of subcommands.
// The ClCommand class for each command is initialized with an array
// of legal subcommands (clsubcommand_t).
//
#define	CLSUB_DEFAULT			0	    // default flags
#define	CLSUB_UNDOCUMENTED		(1 << 0)    // not in usage msg
#define	CLSUB_NCLMODE_ALLOWED		(1 << 1)    // non-cluster mode allowed
#define	CLSUB_NCLMODE_REQUIRED		(1 << 2)    // non-cluster mode required
#define	CLSUB_NON_GLOBAL_ZONE_ALLOWED	(1 << 3)    // non-global zone allowed
#define	CLSUB_DO_COMMAND_LOGGING	(1 << 4)    // log this command
#define	CLSUB_CHECK_EUROPA		(1 << 5)    // only show the subcommand
						    // if Europa is configured
#define	CLSUB_ZONE_CLUSTER_ALLOWED	(1 << 6)    // zone cluster allowed

//
// The following constants are used throughout the code
// to determine the zone cluster context on which an
// object has to be operated on. Since they are almost
// ubiquitous in the code, we are defining them here.
#define	ZC_AND_OBJ_NAME_SEPARATER_STR	":"
#define	ZC_AND_OBJ_NAME_SEPARATER_CHAR	':'
#define	ALL_CLUSTERS_STR		"all"
#define	GLOBAL_CLUSTER_NAME		"global"

struct clsubcommand {
	char *subcommand_name;			// Subcommand name
	unsigned int flags;			// Subcommand flags
	char *subcommand_description;		// Subcommand description
	char *subcommand_rbac;			// Subcommand rbac
	char **subcommand_usage;		// Subcommand usage
	struct cloptions *subcommand_options;	// Subcommand options
	clerrno_t (*dosub) (ClCommand &cmd);	// Process subcommand
};
typedef struct clsubcommand clsubcommand_t;

//
// Global variables
//
extern ClCommand commandInstance;

//
// Command functions
//
extern void enomem_exception(void);

extern clerrno_t clcommand_command(int argc, char *argv[],
    struct clcommandinit &init);
extern clerrno_t clcommand_check_authorization(char *authorization);
extern clerrno_t clcommand_promote_uid(void);

extern char *clcommand_option(int optopt, char *argument);

extern void clcommand_help(ClCommand &cmd);
extern void clcommand_help(ClCommand &cmd, char *subcommandname);
extern void clcommand_usage(ClCommand &cmd);
extern void clcommand_usage(ClCommand &cmd, int errcount);
extern void clcommand_usage(ClCommand &cmd, char *subcommandname);
extern void clcommand_usage(ClCommand &cmd, char *subcommandname, int errcount);

extern char *clsubcommand_get_shortoptions(ClCommand &cmd);
extern void clsubcommand_free_shortoptions(char *shortopts);
extern struct option *clsubcommand_get_longoptions(ClCommand &cmd);
extern void clsubcommand_free_longoptions(struct option *longopts);

extern void clcommand_perror(clerrno_t clerrno);
extern void clcommand_dumpmessages(char *msgbuffer);

extern FILE *clcommand_fopen(const char *filename, const char *mode);

extern clerrno_t clcommand_zc_usage_check(ValueList &zc_list);
#endif /* _CLCOMMANDS_H */
