//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef _CLQUORUM_H
#define	_CLQUORUM_H

#pragma ident	"@(#)clquorum.h	1.8	09/04/20 SMI"

//
// This is the header file for the clquorum(1CL) command
//

#ifdef __cplusplus
#include <sys/os.h>
#endif

#include "clcommands.h"
#include "ClquorumExport.h"
#include "ClShow.h"
#include "ClStatus.h"
#include "common.h"

#include <scadmin/scconf.h>

#ifdef WEAK_MEMBERSHIP
#include <scadmin/scrgadm.h>
#include <haip.h>
#endif

// Flags for enable and disable subcommands
#define	CLQ_ENABLE	1
#define	CLQ_DISABLE	2
#define	CLQ_REMOVE	3

#ifdef WEAK_MEMBERSHIP
#define	CLPROP_PING_TARGETS		"ping_targets"
#define	CLPROP_MULTIPLE_PARTITIONS	"multiple_partitions"
#endif

extern clcommandinit_t clquoruminit;

// Subcommand processing
extern clerrno_t clquorum_add(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, ValueList &devicetypes,
    NameValueList &properties);
extern clerrno_t clquorum_add(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, char *clconfiguration,
    ValueList &devicetypes, NameValueList &properties);
extern clerrno_t clquorum_disable(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, ValueList &devicetypes);
extern clerrno_t clquorum_enable(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, ValueList &devicetypes);
extern clerrno_t clquorum_export(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, char *clconfiguration,
    ValueList &devicetypes);
extern clerrno_t clquorum_list(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, ValueList &devicetypes, ValueList &nodelist);
extern clerrno_t clquorum_set(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, ValueList &devicetypes,
    NameValueList &properties);
extern clerrno_t clquorum_remove(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, ValueList &devicetypes);
extern clerrno_t clquorum_reset(ClCommand &cmd, optflgs_t optflgs);
extern clerrno_t clquorum_show(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, ValueList &devicetypes, ValueList &nodelist);
extern clerrno_t clquorum_status(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, ValueList &devicetypes, ValueList &nodelist);
extern clerrno_t clquorum_votecount(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, ValueList &nodelist);

// Miscellaneous functions
#ifdef WEAK_MEMBERSHIP
extern clerrno_t get_clquorum_show_obj(optflgs_t optflgs,
    ClShow *clq_node_show, ClShow *clq_dev_show, ClShow *clq_global_show);
extern clerrno_t get_clquorum_status_obj(optflgs_t optflgs,
    ClStatus *status_sum, ClStatus *status_node, ClStatus *status_dev,
    ClStatus *status_global);
#else
extern clerrno_t get_clquorum_show_obj(optflgs_t optflgs,
    ClShow *clq_node_show, ClShow *clq_dev_show);
extern clerrno_t get_clquorum_status_obj(optflgs_t optflgs,
    ClStatus *status_sum, ClStatus *status_node, ClStatus *status_dev);
#endif // WEAK_MEMBERSHIP

extern clerrno_t clquorum_conv_scconf_err(scconf_errno_t err);
extern clerrno_t clquorum_get_quorumdevice_list(ValueList &qd_name,
    ValueList &qd_type, NameValueList &qd_list);
extern clerrno_t clquorum_get_nodes_list(ValueList &node_name,
    NameValueList &qd_list);
extern clerrno_t clquorum_get_nodes_bitmask(ValueList &nodelist, bool print_err,
    node_bitmask_t *node_bitmask);
extern clerrno_t clquorum_get_quorumdevices(char *qd_name,
    char *qd_type, NameValueList &qd_list);
extern clerrno_t clquorum_get_nodes(char *node_name,
    NameValueList &qd_list);
extern clerrno_t clquorum_do_set_operation(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, ValueList &devicetypes, int clq_flg);
extern clerrno_t clquorum_preprocess_read_operation(ValueList &operands,
    bool print_err, ValueList &devicetypes, NameValueList &qdev_list);
extern bool clquorum_are_all_nodes_online();
#endif /* _CLQUORUM_H */
