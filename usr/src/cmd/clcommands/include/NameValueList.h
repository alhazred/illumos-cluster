//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef	_NAMEVALUELIST_H
#define	_NAMEVALUELIST_H

#pragma ident	"@(#)NameValueList.h	1.3	08/05/20 SMI"

//
// This is the header file for the NameValueList class.
//
// This class manages a collection of "string" name-value pairs.
//

#include "NameValue.h"

#include <list>
#include <iterator>

class NameValueList {
public:
	// Constructors

	// Construct an empty name-value list.
	NameValueList(bool isSet = false);

	// Construct a name-value list with it's first pair.
	NameValueList(char *name, char *value, bool isSet = false);

	// Construct a name-value list with it's first pair.   Name-values
	// are seperated by "=" in the "string".
	NameValueList(char *string, bool isSet = false);

	// Construct a name-value list.   If flagged, split the "string".
	NameValueList(int splitflag, char *string, bool isSet = false);

	// Destructor
	~NameValueList();


	// Member functions

	// Add a name-value pair to the list.
	int add(char *name, char *value);

	// Add a name-value pait to the front of the list
	int addFirst(char *name, char *value);

	// Add a name-value pair to the list.   Name-values are seperated
	// by "=" in the "string".
	int add(char *string);

	// Add a name-value pair to the list.   If flagged, split the "string".
	int add(int splitflag, char *string);

	// Clear the list.
	void clear();

	// Reset the iterator to the beginning of the list.
	void start();

	// Increment the iterator.
	void next();

	// Return 1 when the iterator is at the end.
	int end();

	// Get the current name.
	char *getName();

	// Get the current value.
	char *getValue();

	// Get the value from name
	char *getValue(char *name);

	// Get the number of elements in the list.
	int getSize();

	// Put the name/value into one string separated by the separator.
	char *getString(char *separator);

private:
	//
	// Split the string, using only the first "token" found in the "string".
	// Then, add each sub-string to the list.
	//
	int split(char *tokens, char *string);

	// The list
	std::list<NameValue *> l;

	// The iterator
	std::list<NameValue *>::iterator li;

	// Whether the list is a set or not
	bool set;
};

#endif /* _NAMEVALUELIST_H */
