//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef _CLSTATUS_H
#define	_CLSTATUS_H

#pragma ident	"@(#)ClStatus.h	1.11	09/01/16 SMI"

//
// This is the header file for the ClStatus class.
//

#define	STATUS_COL	48
#define	V_STATUS_COL	64
#define	MIN_SPACE	3

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <list>
#include <iterator>

#include "Row.h"

// All the heading titles
enum status_heading_t {
	HEAD_NODE,
	HEAD_FNODE,
	HEAD_TRANSPORT,
	HEAD_QUORUM,
	HEAD_DEVICEGROUP,
	HEAD_RESOURCEGROUP,
	HEAD_RESOURCE,
	HEAD_DEVICE,
	HEAD_TELEATTRIBUTE,
	HEAD_ZC,
	HEAD_ZC_NODE};

// All the section titles
enum section_t {
	SECTION_NODES,
	SECTION_IPMP_NODE,
	SECTION_IPMP_ZONE,
	SECTION_QUORUM_SUMMARY,
	SECTION_QUORUM_NODES,
	SECTION_QUORUM_DEVICES,
#ifdef WEAK_MEMBERSHIP
	SECTION_GLOBAL_QUORUM,
#endif
	SECTION_DEVICEGROUPS,
	SECTION_DG_S_I_ITN,
	SECTION_DG_MULTI_SVM,
	SECTION_ZC_NODES};

class ClStatus {

public:

	ClStatus(bool verbose = false);

	~ClStatus();

	// Add a row with up to 7 column values
	void addRow(
	    char *v1,
	    char *v2 = NULL,
	    char *v3 = NULL,
	    char *v4 = NULL,
	    char *v5 = NULL,
	    char *v6 = NULL,
	    char *v7 = NULL);

	// Set the headings of up to 7 columns
	void setHeadings(
	    char *h1,
	    char *h2 = NULL,
	    char *h3 = NULL,
	    char *h4 = NULL,
	    char *h5 = NULL,
	    char *h6 = NULL,
	    char *h7 = NULL);

	// Set the Status column number
	void setStatusColumn(int col);

	// Disable aligning the status column at a fixed width
	void disableStatusAligning(bool value);

	// Enable verbose mode
	void enableVerbose(bool verbose);

	// Set the heading title
	void setHeadingTitle(status_heading_t heading);

	// Get the heading title string
	char *getHeadingTitle();

	// Set the section title
	void setSectionTitle(section_t title);

	// Get the section title string
	char *getSectionTitle();

	// Set how many spaces to indent berfore all columns
	void setInitialIndent(int num);

	// Enable grouping of like rows
	void enableRowGrouping(bool value);

	// Print the data
	void print();

private:

	// Row iterator methods
	void rowStart();
	Row *rowCurrent();
	void rowNext();
	int rowEnd();

	// Keeps track of the longest column values
	void setLongest(Row *row);

	bool verbose;
	bool hasHeading;
	bool hasSection;
	bool alignStatus;
	bool rowGroups;
	int statusColumn;
	int heading;
	int section;
	int indent;
	int numColumns;
	int longest[MAX_COLS];
	Row *headings[2];
	std::list<Row *> rows;
	std::list<Row *>::iterator iter;

};

#endif /* _CLSTATUS_H */
