//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef _CLLIST_H
#define	_CLLIST_H

#pragma ident	"@(#)ClList.h	1.4	08/05/20 SMI"

//
// This is the header file for the ClList class.
//

#define	COL_WIDTH	20
#define	MIN_SPACE	3

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <list>
#include <iterator>

#include "Row.h"

class ClList {

public:

	ClList(bool verbose = false);

	~ClList();

	// Add a row with up to five column values
	void addRow(
	    char *v1,
	    char *v2 = NULL,
	    char *v3 = NULL,
	    char *v4 = NULL,
	    char *v5 = NULL);

	// Set the headings of up to five columns
	void setHeadings(
	    char *h1,
	    char *h2 = NULL,
	    char *h3 = NULL,
	    char *h4 = NULL,
	    char *h5 = NULL);

	// print the data
	void print();

	// Enable verbose mode
	void enableVerbose(bool verbose);

private:

	// Row iterator methods
	void rowStart();
	Row *rowCurrent();
	void rowNext();
	int rowEnd();

	// Print up to five columns
	void print(char *v1, char *v2, char *v3, char *v4, char *v5);

	// Print a Row object
	void print(Row *row);

	// Keeps track of the longest column values
	void setLongest(Row *row);

	bool verbose;
	int numColumns;
	int longest[MAX_COLS];
	Row *headings[2];
	std::list<Row *> rows;
	std::list<Row *>::iterator iter;

};

#endif // _CLLIST_H
