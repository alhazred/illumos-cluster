//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef	_CLZONECLUSTER_H
#define	_CLZONECLUSTER_H

#pragma ident	"@(#)clzonecluster.h	1.14	08/10/14 SMI"

#include "clcommands.h"
#include "ClCommand.h"
#include "ClShow.h"
#include "ClStatus.h"

#include <scadmin/scconf.h>
#include <libzccfg/libzccfg.h>

#define	GLOBAL_ZONE_NAME	"global"
#define	BASE_CLUSTER_STR	"base"

// Properties to print in the "show" output
#define	CLZC_CLUSTER_NAME	GETTEXT("Zone Cluster Name")
#define	CLZC_NODE_NAME		GETTEXT("Node Name")
#define	CLZC_PROP_NAME		GETTEXT("Resource Name")
#define	CLZC_SYSID		GETTEXT("sysid")
#define	CLZC_CAPPED_MEM		GETTEXT("capped-memory")
#define	CLZC_IPD		GETTEXT("inherit-pkg-dir")
#define	CLZC_RCTL		GETTEXT("rctl")
#define	CLZC_FS			GETTEXT("fs")
#define	CLZC_NOVALUE		GETTEXT("<NULL>")

extern clerrno_t clzonecluster_boot(ClCommand &cmd, ValueList &operands,
	optflgs_t optflgs, ValueList &nodes);
extern clerrno_t clzonecluster_configure(ClCommand &cmd, char *zone_name,
	optflgs_t optflgs, char *command_file);
extern clerrno_t clzonecluster_halt(ClCommand &cmd, ValueList &operands,
	ValueList &nodes);
extern clerrno_t clzonecluster_install(ClCommand &cmd, ValueList &nodes,
	char *zone_name);
extern clerrno_t clzonecluster_list(ClCommand &cmd, optflgs_t optflgs);
extern clerrno_t clzonecluster_reboot(ClCommand &cmd, ValueList &operands,
	ValueList &nodes);
extern clerrno_t clzonecluster_show(ClCommand &cmd, ValueList operands,
	optflgs_t optflgs);
extern clerrno_t clzonecluster_status(ClCommand &cmd, ValueList operands,
	optflgs_t optflgs);
extern clerrno_t clzonecluster_uninstall(ClCommand &cmd, ValueList &nodes,
	char *zone_name, optflgs_t optflgs);
extern clerrno_t clzonecluster_verify(ClCommand &cmd, ValueList &operands,
	ValueList &nodes);
extern clerrno_t clzonecluster_ready(ClCommand &cmd, ValueList &operands,
	ValueList &nodes);
extern clerrno_t clzonecluster_move(ClCommand &cmd, ValueList &operands,
	char *newzonepath);
extern clerrno_t clzonecluster_export(ClCommand &cmd, char *zone_cluster,
	optflgs_t optflgs, char *command_file);
extern clerrno_t clzonecluster_clone(ClCommand &cmd, char *source_zc,
	optflgs_t optflgs, ValueList& nodes, char *target_zc, char *copymethod);
extern clerrno_t clzonecluster_delete(ClCommand &cmd, ValueList operands,
	optflgs_t optflgs);

// Methods called from cluster command
extern clerrno_t get_zonecluster_status_obj(optflgs_t optflgs,
	ClStatus *clzc_status);
extern clerrno_t get_zonecluster_show_obj(optflgs_t optflgs,
	ClShow *clzc_show);

// The following are some common functions which are needed even outside
// the library
//
// get_zone_cluster_names
//
// This method fetches the list of names of zone clusters
// configured in the cluster. The caller is responsible for
// freeing the memory in the list. If this method is called
// inside a zone cluster then the list will be empty. Note
// that in such a case this method will not return an error.
//
// Return Values :
//	CL_NOERR	- No Error.
//	CL_EINTERNAL	- Internal Error.
//
extern clerrno_t
get_zone_cluster_names(ValueList &zc_list);

//
// get_nodelist_for_zc
//
// Fetches the nodelist for a specified zone cluster. This function
// appends to "node_list" the list of nodes across which the zone
// cluster spans.
//
// Return Values :
//	CL_NOERR	- No Error.
//	CL_ENOENT	- No such zone cluster exists.
//	CL_EINTERNAL	- Internal Error.
//
extern clerrno_t
get_nodelist_for_zc(char *zonename, ValueList &node_list);

//
// get_hostname_list_for_zc
//
// Fetches the list of zone hostnames for a specified zone cluster.
// This functions appends to "host_list" the hostnames of the zone
// on nodes across which the zone cluster spans.
//
// Return Values :
//	CL_NOERR	- No Error.
//	CL_ENOENT	- No such zone cluster exists.
//	CL_EINTERNAL	- Internal Error.
//
extern clerrno_t
get_hostname_list_for_zc(char *zonename, ValueList &node_list);

//
// check_zone_cluster_exists
//
// This function checks whether the specified zone cluster exists or not.
// If the zone cluster exists, then this function CL_ENOERR. If the
// zone cluster does not exist, this function returns CL_ENOENT.
// If there is any error determining the validity of the zone cluster
// this function returns CL_EINTERNAL.
//
// Return Values :
//	CL_NOERR	- Zone cluster exists
//	CL_EINVAL	- Zone cluster was not specified
//	CL_ENOENT	- Zone cluster does not exist
//	CL_EINTERNAL	- Internal error
extern clerrno_t
check_zone_cluster_exists(char *zonename);

//
// get_zc_node_status
//
// This function fetches the status of the node in the zone cluster.
// The nodename expected here is the hostname of the zone, which is
// part of the zone cluster. If the zone cluster or the zone-cluster
// hostname does not exist, this function returns CL_ENOENT. Please
// note that this method will function properly only when executing
// in the global zone.
//
// Return Values :
//	CL_NOERR	- No error.
//	CL_ENOENT	- Zone cluster/ZC hostname does not exist
//	CL_EINVAL	- Invalid input
//	CL_EINTERNAL	- Internal error
extern clerrno_t
get_zc_node_status(char *zcname, char *zone_hostname,
    char **status_str);

//
// validate_zonecluster_support
//
// This function validates whether the platform/OS that the
// command is being run is capable of supporting zoneclusters
//
// Return Values :
//	CL_NOERR	- No error.
//	CL_EOP		- Unsupported OS

extern clerrno_t
validate_zonecluster_support();

//
// validate_zonecluster_name
//
// This function validates whether the given zone cluster
// name doesnt not conflict with any of the reserved zone cluster
// names "all", "global" or "base"
//
// Return Values :
//	CL_NOERR	- No error.
//	CL_EINVAL	- Invalid Zone Cluster name due to conflict
//	CL_EINTERNAL	- Internal error

extern clerrno_t
validate_zonecluster_name(char *zcname);
#endif /* _CLZONECLUSTER_H */
