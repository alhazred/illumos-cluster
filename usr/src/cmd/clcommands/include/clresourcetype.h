//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef	_CLRESOURCETYPE_H
#define	_CLRESOURCETYPE_H

#pragma ident	"@(#)clresourcetype.h	1.7	08/07/24 SMI"

#include "clcommands.h"
#include "ClresourcetypeExport.h"
#include "ClShow.h"

//
// This is the main header file for clresourcetype functions
//

extern clerrno_t clrt_add_node(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, ValueList &nodes);
extern clerrno_t clrt_export(ClCommand &cmd, ValueList &operands,
    char *clconfiguration);
extern clerrno_t clrt_list(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, ValueList &nodes, ValueList &zc_names);
extern clerrno_t clrt_list_props(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, ValueList &properties, ValueList &zc_names);
extern clerrno_t clrt_set(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, NameValueList &properties, ValueList &nodes);
extern clerrno_t clrt_register(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, ValueList &nodes, char *rtrfile,
    NameValueList &properties);
extern clerrno_t clrt_register(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, char *clconfiguration, ValueList &nodes, char *rtrfile,
    NameValueList &properties);
extern clerrno_t clrt_remove_node(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, ValueList &nodes);
extern clerrno_t clrt_show(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, ValueList &nodes, ValueList &zc_names);
extern clerrno_t clrt_unregister(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs);
extern clerrno_t get_clrt_show_obj(optflgs_t optflgs, ClShow *rt_show);
extern int find_rt_in_list(NameValueList &type_file_pair, char *rtname,
    char *name_buf, char *full_rtname, int *errflg);


#endif /* _CLRESOURCETYPE_H */
