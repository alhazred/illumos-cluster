//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef _CLNASDEVICE_H
#define	_CLNASDEVICE_H

#pragma ident	"@(#)clnasdevice.h	1.3	08/05/20 SMI"

//
// This is the header file for the clnasdevice(1CL) command
//

#include "clcommands.h"
#include "ClnasExport.h"
#include "ClShow.h"

#ifdef __cplusplus
#include <sys/os.h>
#endif

#include "scnas.h"

extern clcommandinit_t clnasdeviceinit;

// Subcommand processing
extern clerrno_t clnasdevice_add(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, ValueList &nasdevicetypes,
    NameValueList &properties);
extern clerrno_t clnasdevice_add(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, char *clconfiguration,
    ValueList &nasdevicetypes, NameValueList &properties);
extern clerrno_t clnasdevice_add_dir(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs,
    NameValueList &directories);
extern clerrno_t clnasdevice_add_dir(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, char *clconfiguration,
    NameValueList &directories, NameValueList &properties);
extern clerrno_t clnasdevice_export(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, char *clconfiguration,
    ValueList &nasdevicetypes, NameValueList &directories);
extern clerrno_t clnasdevice_list(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, ValueList &nasdevicetypes);
extern clerrno_t clnasdevice_set(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, NameValueList &properties);
extern clerrno_t clnasdevice_remove(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, ValueList &nasdevicetypes);
extern clerrno_t clnasdevice_remove_dir(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs,
    NameValueList &directories, char *all_dir);
extern clerrno_t clnasdevice_show(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, ValueList &nasdevicetypes);

// Miscellaneous functions
extern clerrno_t get_clnasdevice_show_obj(optflgs_t optflgs,
    ClShow *clnas_show);
extern clerrno_t clnasdevice_prop_convert(NameValueList &properties,
    scnas_filer_prop_t **scnas_props);
extern clerrno_t clnasdevice_conv_scnas_err(scnas_errno_t err);
extern void clnasdevice_perror(clerrno_t clerrno);
extern clerrno_t clnasdevice_preprocess_naslist(ValueList &nasdevices,
    ValueList &nastypes, NameValueList &nas_list, bool print_err);

#endif /* _CLNASDEVICE_H */
