//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef	_CLMESSAGES_H
#define	_CLMESSAGES_H

#pragma ident	"@(#)clmessages.h	1.3	08/10/13 SMI"

#include "TextMessage.h"

#include <locale.h>

// Macros for "clxgettext" identification purposes only
#define	GETTEXT(T)	(char *)gettext(T)
#define	GETSTRING(T)	T

// Defaults
#define	CLCOMMANDS_DFLT_PROPS1_DIRNAME \
		"/usr/cluster/lib/clcommands/properties"	// Errors
#define	CLCOMMANDS_DFLT_PROPS1_BASENAME "errors"

#define	CLCOMMANDS_DFLT_PROPS2_DIRNAME \
		"/usr/cluster/lib/clcommands/properties"	// Strings
#define	CLCOMMANDS_DFLT_PROPS2_BASENAME "strings"

#define	CLCOMMANDS_DFLT_PROPS3_DIRNAME \
		"/usr/cluster/lib/clcommands/properties"
#define	CLCOMMANDS_DFLT_PROPS3_BASENAME "shared"		// Shared

//
// Environmment variables
//

// If set, always print verbose error messages
#define	CLCOMMANDS_VAR_VERBOSE_ERRORS	"CLCOMMANDS_VERBOSE_ERRORS"

// If set, do not print message IDs with error messages
#define	CLCOMMANDS_VAR_NO_MESSAGE_IDS	"CLCOMMANDS_NO_MESSAGE_IDS"

// Directory name and base name of properties file for error messages
#define	CLCOMMANDS_VAR_PROPS1_DIRNAME	"CLCOMMANDS_PROPS1_DIRNAME"
#define	CLCOMMANDS_VAR_PROPS1_BASENAME	"CLCOMMANDS_PROPS1_BASENAME"

// Directory name and base name of properties file for other strings
#define	CLCOMMANDS_VAR_PROPS2_DIRNAME	"CLCOMMANDS_PROPS2_DIRNAME"
#define	CLCOMMANDS_VAR_PROPS2_BASENAME	"CLCOMMANDS_PROPS2_BASENAME"

// Directory name and base name of strings shared with SPM
#define	CLCOMMANDS_VAR_PROPS3_DIRNAME	"CLCOMMANDS_PROPS3_DIRNAME"
#define	CLCOMMANDS_VAR_PROPS3_BASENAME	"CLCOMMANDS_PROPS3_BASENAME"

// Test locale
#define	CLCOMMANDS_VAR_TEST_LOCALE	"CLCOMMANDS_TEST_LOCALE"

extern void init_clmessages(char *cmd_name);

PropertyResourceBundle *create_error_bundle(char *locale);
PropertyResourceBundle *create_text_bundle(char *locale);
PropertyResourceBundle *create_shared_bundle(char *locale);

unsigned int get_default_error_flags();
unsigned int get_default_warning_flags();
unsigned int get_default_message_flags();

extern TextMessage clerrorMessage;		// for clerror()
extern TextMessage cltextMessage;		// for clwarning(), clmessage()
extern GetText gettextGetText;			// for gettext()
extern GetText getstringGetText;		// for getstring()

#endif /* _CLMESSAGES_H */
