/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_RGM_SUPPORT_H
#define	_RGM_SUPPORT_H

#pragma ident	"@(#)rgm_support.h	1.14	08/08/07 SMI"

#define	VALUE_STR_NULL		"<NULL>"
#define	VALUE_STR_ALL		"<All>"
#define	VALUE_STR_TRUE		"True"
#define	VALUE_STR_FALSE		"False"
#define	VALUE_STR_UNKNOWN	"<Unknown>"
#define	VALUE_STR_UNSET		"<Unset>"

#define	DELETED_RS	"{}"
#define	DELETED_RG	"+"

typedef enum cl_objtype {
	CL_TYPE_NONE = 0,
	CL_TYPE_RS,
	CL_TYPE_RG
} cl_objtype_t;

typedef enum cl_optype {
	CL_OP_ENABLE = 0,
	CL_OP_DISABLE,
	CL_OP_MONITOR,
	CL_OP_UNMONITOR,
	CL_OP_SWITCH,
	CL_OP_ONLINE,
	CL_OP_SUSPEND,
	CL_OP_RESUME
} cl_optype_t;

extern scha_property_t *nvl_to_prop_array(NameValueList &properties,
    char *objname, int *errflg, cl_objtype_t objtype, char *rtype,
	char *in_zc_name = NULL);
extern namelist_t *strip_commas_in_prop(char *value_str, int *errflg);
extern void print_rgm_error(scha_errmsg_t error, char *operand);
extern void print_scstat_error(scstat_errno_t error, char *operand);
extern void free_prop_array(scha_property_t *prop_array);
extern char **vl_to_names(ValueList &vl, int *errflg);
extern char **namelist_to_names(namelist_t *nl, int *errflg);
extern clerrno_t cluster_init();
extern namelist_t *add_vl_to_names(namelist_t *rg_nodelist, ValueList &nodes,
    int *errflg);
extern namelist_t *remove_vl_from_names(namelist_t *rg_nodelist,
    ValueList &nodes, int *errflg);
extern scha_errmsg_t modify_rg(char *rg_name, scha_property_t *prop, int flag,
	const char **rgnames);
extern namelist_t *add_to_prop(namelist_t *rgm_list, namelist_t *new_list,
    int *errflg);
extern namelist_t *rm_from_prop(namelist_t *rgm_list, namelist_t *new_list,
    int *errflg);
extern clerrno_t map_scha_error(int error);
extern clerrno_t map_scstat_error(int error);
extern char *names_to_str(namelist_t *name_list, int *errflg);
extern char *names_to_str_com(namelist_t *name_list, char *propname,
    int *errflg);
extern clerrno_t check_state(char *state, cl_objtype_t objtype);
extern char *scstat_str_to_cl_str(char *state, cl_objtype_t objtype);
extern clerrno_t get_good_nodes(ValueList &nodes, ValueList &good_nodes,
    ValueList &zc_list = ValueList());
#if (SOL_VERSION >= __s10)
extern clerrno_t get_good_node_for_zc(char *input_node, char **node_name,
	char *zc_name);
#endif
extern clerrno_t get_r_rtname(char *rtype, char **r_rtname,
    char *zc_name = NULL);
extern clerrno_t get_r_props(char *rt_name, NameValueList &proplist,
    char *zc_name = NULL);
extern clerrno_t get_all_r_props(NameValueList &proplist);
extern clerrno_t check_rg(char *rgname, char *zc_name);
extern int match_type(ValueList &resourcetypes, rgm_resource_t *rs);
extern clerrno_t match_rt_rg_in_zc(ValueList &resourcetypes,
    rgm_resource_t *rs, char *zc_name, boolean_t &type_found, int obj_type);
extern int match_group(ValueList &resourcegroupes, rgm_resource_t *rs);
extern int match_rs_node(ValueList &nodes, scstat_rs_t *rsstat);
extern int match_rs_state(ValueList &states, scstat_rs_t *rsstat);
extern clerrno_t set_rs_props(char *rsname, char *rtname,
    NameValueList &properties, NameValueList &xproperties,
    NameValueList &yproperties, char *zc_name = NULL);
extern namelist_t *nodeidlist_to_namelist(nodeidlist_t *list, int *errflg,
    char *zc_name = NULL);
extern char *nodeidzone_to_nodename(uint_t nodeid, char *zonename, int *errflg,
    char *zcname = NULL);
extern void get_all_rg_props(ValueList &proplist);
extern void get_all_rt_props(ValueList &proplist);
namelist_t *
rdep_list_to_nlist(rdeplist_t *r_dependencies, int *errflg);
clerrno_t
clresource_process_resources(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, ValueList &resourcegroups, ValueList &resourcetypes,
    ValueList &nodes, cl_optype_t op_code);
clerrno_t
clrg_process_switch(ClCommand &cmd, ValueList &operands, optflgs_t optflgs,
    ValueList &nodes, cl_optype_t op_code);
clerrno_t
clrg_suspend_resume(ClCommand &cmd, ValueList &operands, optflgs_t optflgs,
    cl_optype_t op_code);
extern clerrno_t append_lh_props(char *rg_name, NameValueList &xproperties,
    ValueList &lhosts, ValueList &netiflist, ValueList &auxnodes,
    char *zc_name = NULL);
clerrno_t verify_yprops(NameValueList &yprops);
clerrno_t process_props(NameValueList &props, NameValueList &pn_props,
    char *rt_name, char *zc_name = NULL);
clerrno_t prop_is_per_node(char *prop_name, char *rt_name,
    boolean_t *is_per_node, char *zc_name = NULL);
clerrno_t disable_rs(ValueList &rslist, boolean_t verbose,
    boolean_t force, char *zc_name = NULL);
clerrno_t eliminate_rs_dependencies(char **rsnames, ValueList &allrs,
    boolean_t force, boolean_t verbose);
clerrno_t remove_enabled_rs_from_list(char **rsnames, boolean_t *list_changed);
clerrno_t get_all_rs(ValueList &allrs, char **rgnames);
clerrno_t remove_rs_dependees_from_list(char **rsnames, ValueList &allrs,
    boolean_t *list_changed);
clerrno_t get_all_rg(ValueList &allrg);
clerrno_t get_all_rt_from_zc(ValueList &allrt, char *zc_name,
    boolean_t print_err);
clerrno_t remove_busy_rg_from_list(char **rg_names, boolean_t *list_changed);
clerrno_t remove_rg_referred_from_list(char **rgnames, ValueList &allrg,
    boolean_t *list_changed);
clerrno_t eliminate_rg_references(char **rgnames, ValueList &allrg,
    boolean_t force, boolean_t verbose);
clerrno_t eliminate_sysprop(char **rg_names, boolean_t verbose);
int get_cluster_nodecount(int *errflg);
clerrno_t check_server_farm_mix(ValueList &nodelist, char *objname);
clerrno_t europa_open(boolean_t *iseuropa);
void europa_close();
void namelist_to_vl(namelist_t *new_nodelist, ValueList &new_nl_vl);
clerrno_t map_scconf_error(int error);
scha_err_t filter_warning(scha_err_t scha_err);
char *replace_nodenames(char *ip_str, int *errflg);
clerrno_t z_getrglist(char ***rg_names, boolean_t verbose,
    optflgs_t susp_included, char *zc_name = NULL);
clerrno_t tok_prop(char *src_prop, boolean_t *pn_specified, char **proppart,
    char **nodepart);
clerrno_t check_rs_error(rgm_resource_t *rs, boolean_t *rs_err,
    char *zc_name = NULL);
boolean_t is_good_rg(char *rgname, boolean_t print_err);
clerrno_t check_system_rg(char *rgname, boolean_t *sysrg);
clerrno_t eliminate_rg_sysprop(char *rsname, boolean_t verbose);
clerrno_t add_lh_or_sa_rts_to_list(ValueList &rt_list, ValueList &zc_names,
    optflgs_t optflgs);
#if SOL_VERSION >= __s10
clerrno_t get_zone_clusters_with_rs(char *rsname, ValueList &zc_names,
    ValueList &zc_check_list = ValueList());
clerrno_t get_zone_clusters_with_rt(char *rtname, ValueList &zc_names,
    ValueList &zc_check_list = ValueList());
clerrno_t get_zone_clusters_with_rg(char *rgname, ValueList &zc_names,
    ValueList &zc_check_list = ValueList());
extern clerrno_t check_phys_virt_nodes_mix(ValueList &nodes, uint_t *flagp);
extern clerrno_t call_rtreg_proxy_register_cmd(ClCommand &cmd);
clerrno_t convert_nodename_to_zone_hostname(
    char *base_nodenamep, char *zc_namep, char **zc_hostnamep);
extern clerrno_t evacuate_rgs_on_zc_nodes(ValueList &nodes, char *zc_namep);
#endif

#endif // _RGM_SUPPORT_H
