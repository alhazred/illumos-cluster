//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef _CLNODE_H
#define	_CLNODE_H

#pragma ident	"@(#)clnode.h	1.13	09/01/16 SMI"

//
// This is the header file for the clnode(1CL) command
//

#include "clcommands.h"
#include "ClCommand.h"
#include "ClnodeExport.h"
#include "ClShow.h"
#include "ClStatus.h"
#if (SOL_VERSION >= __s10)
#include "clzonecluster.h"
#endif

#ifdef __cplusplus
#include <sys/os.h>
#endif

#include <scadmin/scconf.h>
#include <sys/utsname.h>

// Property names specified in command line
#define	NODEPROP_PRIVHOST	"privatehostname"
#define	NODEPROP_ZPRIVHOST	"zprivatehostname"
#define	NODEPROP_FAILOVER_DPM	"reboot_on_path_failure"
#define	NODEPROP_ZONESHARE	"globalzoneshares"
#define	NODEPROP_PSETMIN	"defaultpsetmin"
#define	NODEPROP_MONITOR	"monitoring"
#define	NODEPROP_ADAPTERS	"adapterlist"
#define	NODEPROP_PHYS_HOST	"physical-host"

#define	NODETYPE_SERVER		"cluster"
#define	NODETYPE_FARM		"farm"
#define	NODETYPE_ZONE_CLUSTER	"zonecluster"

#define	MONITOR_ENABLED		"enabled"
#define	MONITOR_DISABLED	"disabled"

#define	SET_MONITOR		1
#define	SET_UNMONITOR		2

#define	DEFAULT_GLOBALDEVFS	"/globaldevices"
#define	LOFI_METHOD		"lofi"

#define	S9			"5.9"
#define	ZC_NODE_NAME_SEPARATOR	":"

// Structure for node properties
struct clnode_proplist {
	char *cli_name;		// Property name in command line
	char *ccr_name;		// Property name saved in CCR
	char *node_type;	// Node type
	bool read_only;		// Whether the property can be changed
};
typedef clnode_proplist clnode_proplist_t;

extern clnode_proplist_t clnode_props[];
extern clcommandinit_t clnodeinit;

// Subcommand processing
extern clerrno_t clnode_add_farm(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, NameValueList &properties);
extern clerrno_t clnode_add_farm(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, char *clconfiguration, NameValueList &properties);
extern clerrno_t clnode_add(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, NameValueList &properties,
    char *sponsornode, char *clustername, char *gmntpoint,
    ValueList &endpoints);
extern clerrno_t clnode_add(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, char *clconfiguration, NameValueList &properties,
    char *sponsornode, char *clustername, char *gmntpoint,
    ValueList &endpoints);
extern clerrno_t clnode_check(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, ValueList &nodetypes, char *output_dir,
    char *severity);
extern clerrno_t clnode_clear(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, char **msg_str_buffer = (char **)0);
extern clerrno_t clnode_evacuate(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, ushort_t evac_timeout);
extern clerrno_t clnode_export(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, char *clconfiguration,
    ValueList &nodetypes);
extern clerrno_t clnode_list(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, ValueList &nodetypes, ValueList &zc_names);
extern clerrno_t clnode_show_rev(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs);
extern clerrno_t clnode_set(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, ValueList &nodetypes, NameValueList &properties);
extern clerrno_t clnode_remove_farm(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs);
extern clerrno_t clnode_remove(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, char *sponsornode, char *gmntpoint);
extern clerrno_t clnode_show(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, ValueList &nodetypes, ValueList &prop_list,
    ValueList &zc_names);
extern clerrno_t clnode_status(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, ValueList &nodetypes, ValueList &zc_names);
extern clerrno_t clnode_monitor_farm(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs);
#ifdef WEAK_MEMBERSHIP
extern clerrno_t clnode_resolve_config(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, char *winnernode);
#endif // WEAK_MEMBERSHIP
extern clerrno_t clnode_unmonitor_farm(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs);
extern clerrno_t delete_node_from_zc(char *nodename, char *zonename);

// Common functions
extern clerrno_t get_clnode_show_obj(optflgs_t optflgs, ClShow *clnode_cshow,
    ClShow *clnode_fshow);
extern clerrno_t get_clnode_status_obj(optflgs_t optflgs,
    ClStatus *clnode_cstatus, ClStatus *clnode_cstatus_ipmp_node,
    ClStatus *clnode_fstatus, ClStatus *clnode_cstatus_ipmp_zone);
extern clerrno_t clnode_conv_scconf_err(int printflag, scconf_errno_t err);
extern clerrno_t clnode_preprocess_nodelist(ValueList &nodes,
    ValueList &nodetypes, NameValueList &node_list, bool print_err,
    ValueList &zc_names = ValueList());
extern bool clnode_valid_prop(char *prop_name, char *node_type);
extern clerrno_t execve_command(char *cli_cmd, char **cli_args,
    char **cli_envs);
extern clerrno_t clnode_get_ng_zones(uint_t node_id, ValueList &zones_list);
extern clerrno_t clnode_set_monitor(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, uint_t set_flg);
extern clerrno_t clnode_set_farm_monstate(NameValueList &node_list,
    optflgs_t optflgs, uint_t set_flg);
extern clerrno_t clnode_check_europa(boolean_t *iseuropa);
extern clerrno_t clnode_open_europa();
extern void clnode_close_europa();
extern clerrno_t clnode_get_supported_node_types(ValueList &node_types);
#if (SOL_VERSION >= __s10)
extern clerrno_t clnode_get_zone_cluster_nodes(ValueList &zc_list,
	bool print_err, ValueList zc_names = ValueList(false));
#endif

#endif /* _CLNODE_H */
