//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef	_CLSNMPUSER_H
#define	_CLSNMPUSER_H

#pragma ident	"@(#)clsnmpuser.h	1.4	08/05/20 SMI"

#include "clcommands.h"
#include "clsnmpmib.h"
#include "ClShow.h"
#include "ClList.h"
#include "ClsnmpuserExport.h"

#define	USER_LIST_HEADER "\n--- SNMP Users on node %s ---\n\n"

//
// This is the main header file for clsnmpuser functions
//

// The subcommand processing functions
extern clerrno_t clsnmpuser_create(ClCommand &cmd, ValueList &operands,
    ValueList &nodes, char *auth, char *passfile);
extern clerrno_t clsnmpuser_create(ClCommand &cmd, ValueList &operands,
    ValueList &nodes, ValueList &auths, char *passfile, char *clconfig);
extern clerrno_t clsnmpuser_delete(ClCommand &cmd, ValueList &operands,
    ValueList &auths, ValueList &nodes);
extern clerrno_t clsnmpuser_export(ValueList &operands,
    ValueList &nodes, ValueList &auths, char *clconfig);
extern clerrno_t clsnmpuser_list(ClCommand &cmd, ValueList &operands,
    ValueList &nodes, ValueList &auths, bool default_only);
extern clerrno_t clsnmpuser_set(ClCommand &cmd, ValueList &operands,
    ValueList &nodes, char *auth);
extern clerrno_t clsnmpuser_set_default(ClCommand &cmd, ValueList &operands,
    ValueList &nodes, char *seclevel);
extern clerrno_t clsnmpuser_show(ValueList &operands, ValueList &nodes,
    ValueList &auths, bool default_only);

// helper functions
extern clerrno_t get_all_snmp_seclevels(ValueList &seclevels, char *node);
extern clerrno_t get_all_snmp_users(ValueList &users, char *node);
extern clerrno_t get_passwords_from_stdin(ValueList &users, char *&passfile);
extern ClShow *get_filtered_clsnmpuser_show_obj(ValueList &operands,
    ValueList &auths, bool default_only, char *node, clerrno_t &error);
extern ClShow *get_clsnmpuser_show_obj(char *node);
extern clerrno_t parse_stdout_to_snmp_user_show(ClShow *&show_obj, FILE *file,
    char *node);
extern clerrno_t get_default_user_and_seclevel(char *user, char *seclevel,
    char *node);
extern clerrno_t iterate_thru_nodelist(ValueList &nodes, char *args,
    ClList &print_list_output);
extern clerrno_t iterate_thru_nodelist(ValueList &nodes, char *args,
    char *operand, char *node_list_output, int *notfndcnt);

#endif /* _CLSNMPUSER_H */
