/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _CLINTERCONNECT_H
#define	_CLINTERCONNECT_H

#pragma ident	"@(#)clinterconnect.h	1.5	08/05/20 SMI"

//
// This is the header file for the clinterconnect(1CL) command
//

#ifdef __cplusplus
#include <sys/os.h>
#endif
#include "clcommands.h"
#include "scadmin/scconf.h"
#include "scadmin/scstat.h"
#include "ClXml.h"
#include "ClShow.h"
#include "ClStatus.h"

#define	STR_ENABLED	"Enabled"
#define	STR_DISABLED	"Disabled"
#define	STR_RSM		"rsm"

// Endpoints
#define	STATE		GETTEXT("State")
#define	TYPE		GETTEXT("Type")
#define	PORTS		GETTEXT("Port Names")
#define	PSTATE		GETTEXT("Port State")

// Cable
#define	TRANS_CABLE	GETTEXT("Transport Cable")
#define	ENDPOINT1	GETTEXT("Endpoint1")
#define	ENDPOINT2	GETTEXT("Endpoint2")

// Switch
#define	TRANS_SWITCH	GETTEXT("Transport Switch")
#define	SWITCH_PROP	GETTEXT("Switch Property")

// Adapter
#define	TRANS_ADAPTER	GETTEXT("Transport Adapter")
#define	TRANS_TYPE	GETTEXT("Transport Type")

// Subcommand
extern clerrno_t clinterconnect_add(ClCommand &cmd, ValueList &operands,
    uint_t noenable);
extern clerrno_t clinterconnect_add(ClCommand &cmd, ValueList &operands,
    char *clconfiguration, ValueList &nodelist, uint_t noenable);
extern clerrno_t clinterconnect_disable(ClCommand &cmd, ValueList &operands,
    ValueList &nodelist);
extern clerrno_t clinterconnect_enable(ClCommand &cmd, ValueList &operands,
    ValueList &nodelist);
extern clerrno_t clinterconnect_export(ClCommand &cmd, ValueList &operands,
    ValueList &nodelist);
extern clerrno_t clinterconnect_export(ClCommand &cmd, ValueList &operands,
    char *clconfiguration, ValueList &nodelist);
extern clerrno_t clinterconnect_remove(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs);
extern clerrno_t clinterconnect_show(ClCommand &cmd, ValueList &operands,
    ValueList &nodelist);
extern clerrno_t clinterconnect_status(ClCommand &cmd, ValueList &operands,
    ValueList &nodelist);

// Miscellaneous functions
extern clerrno_t clintr_get_cluster_endpoints(ValueList &endpoints);
extern clerrno_t clintr_verify_nodes(ValueList &nodes);
extern clerrno_t clintr_verify_operands(ValueList &operands);
extern clerrno_t clintr_is_allports_disabled(const char *switchname,
    bool *is_alldisabled);
extern clerrno_t clintr_conv_scconf_err(scconf_errno_t scconferr);
extern clerrno_t clintr_conv_scstat_err(scstat_errno_t scstaterr);
extern void clintr_perror(clerrno_t clerrno);
extern void get_cable_epoint(scconf_cltr_epoint_t *epoint, char *buffer,
    int noport);
extern void get_ep_node(scconf_cltr_epoint_t *epoint, char *node);
extern void get_ep_name(scconf_cltr_epoint_t *epoint, char *name);
extern void get_ep_port(scconf_cltr_epoint_t *epoint, char *port);
extern void get_ep_type(scconf_cltr_epoint_t *epoint, cable_t *type);
extern int initialize_endpoint(scconf_cltr_epoint_t *epoint, char *endpoint);
extern clerrno_t get_clintr_show_obj(optflgs_t optflgs,
    ClShow *clintr_cable_show, ClShow *clintr_switch_show);
extern ClShow *get_clintr_adapter_show(char *node_name);
extern clerrno_t get_clintr_status_obj(optflgs_t optflgs,
    ClStatus *clintr_cstatus);

#endif /* _CLINTERCONNECT_H */
