//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef _ROW_H
#define	_ROW_H

#pragma ident	"@(#)Row.h	1.3	08/05/20 SMI"

//
// This is the header file for the Row class.
//

#define	MAX_COLS	7

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*
 * The Row class is a simple object which stores what can
 * be thought of as a logical row of related strings.
 * The Row accepts up the five values, any of which may
 * be NULL.
 */
class Row {

public:

	/*
	 * Create a row object with the given strings
	 */
	Row(char *one,
	    char *two,
	    char *three,
	    char *four,
	    char *five,
	    char *six = NULL,
	    char *seven = NULL);

	/*
	 * Deconstructor
	 */
	~Row();

	/*
	 * Get a Row value associated with a number
	 */
	char *getValue(int num);

private:

	// The array of values
	char *values[MAX_COLS];

};

#endif // _ROW_H
