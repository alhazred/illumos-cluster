//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef	_CLCOMMANDS_COMMON_H
#define	_CLCOMMANDS_COMMON_H

#pragma ident	"@(#)common.h	1.14	09/04/20 SMI"

#include "clcommands.h"
#include "ClCommand.h"
#if (SOL_VERSION >= __s10)
#include "clzonecluster.h"
#endif
#include "errorhandler.h"

// This header file will declare functions which
// are commonly used across the new-cli library.
// The purpose of having this header file is to
// avoid duplicity of code in the new-cli codebase.
// If you want to write a function which would be
// used in more places than one, then you might
// want to consider it delcaring here and placing
// the function in the misc directory.

// The following enums can be used to describe the
// the type of object on which a command is trying
// to operate on and the type of operation as well.
// This structure will be required in functions which
// are capable of operating on different objects and
// on different operations. One such simple example
// is the "check_zc_operation" defined below.
typedef enum cluster_obj_type {
	CL_OBJ_TYPE_RS = 0,
	CL_OBJ_TYPE_RG,
	CL_OBJ_TYPE_RT,
	CL_OBJ_TYPE_ZC,
	CL_OBJ_TYPE_DG,
	CL_OBJ_TYPE_NODE
} cluster_obj_type_t;

typedef enum cluster_op_type {
	CL_OP_TYPE_ENABLE = 0,
	CL_OP_TYPE_DISABLE,
	CL_OP_TYPE_MONITOR,
	CL_OP_TYPE_UNMONITOR,
	CL_OP_TYPE_SWITCH,
	CL_OP_TYPE_ONLINE,
	CL_OP_TYPE_SUSPEND,
	CL_OP_TYPE_RESUME,
	CL_OP_TYPE_CREATE,
	CL_OP_TYPE_DELETE,
	CL_OP_TYPE_REGISTER,
	CL_OP_TYPE_UNREGISTER,
	CL_OP_TYPE_OFFLINE,
	CL_OP_TYPE_REMASTER,
	CL_OP_TYPE_RESTART,
	CL_OP_TYPE_EVACUATE,
	CL_OP_TYPE_QUIESCE,
	CL_OP_TYPE_MANAGE,
	CL_OP_TYPE_UNMANAGE,
	CL_OP_TYPE_ADD_NODE,
	CL_OP_TYPE_REMOVE_NODE,
	CL_OP_TYPE_CLEAR,
	CL_OP_TYPE_SET
} cluster_op_type_t;

#if (SOL_VERSION >= __s10)
//
// check_zc_operation(cluster_obj_type obj_type, cluster_op_type op_type,
//			char *zcname);
//
// This method will check whether the specified operation(op_type) is
// allowed on the cluster object (obj_type) in the zone cluster (zcname).
// This method checks for the validity of the zone cluster, zcname,
// and reports an error if it does not exist. Essentially, there are
// certain operations which can be allowed from the global zone on a zone
// cluster based on the status of the zone cluster. And there are certain
// operations which are not allowed. For example, it is not possible to
// create/delete RGs in a zone cluster, when the ZC status is less then
// "Running". This kind of check is handled here. If an operation is not
// allowed this method will return CL_EACCESS. If it is allowed this method
// will return CL_NOERR.
// Most of the commands, which can operate on a zone cluster, call this
// method before performing the actual operation. This method can serve as a
// central place where all such checks can be made.
//
// Return Values :
//	CL_NOERR	- Operation is allowed.
//	CL_EACCESS	- Operation not allowed.
//	CL_ENOENT	- No such zone cluster.
//	CL_EINTERNAL	- Internal error.

extern clerrno_t
check_zc_operation(cluster_obj_type obj_type, cluster_op_type op_type,
    char *zcname);
#endif

//
// names_to_valuelist(const char **names, ValueList names_list);
//
// This method adds each string in "names" to valuelist
// "names_list". Null values will not be added and
// the caller of this function is responsible to clear
// the memory in "names_list"
//
// Return Values :
//	CL_NOERR	- No error.
//	CL_ENOMEM	- Not enough memory

extern clerrno_t
names_to_valuelist(char **names, ValueList &names_list);

//
// parse_zc_names_from_names_str(char **names, ValueList zc_names);
//
// This method will read each string in "names" and parse it
// to read the zone cluster name. The format expected by this
// method is <zonecluster name>:<obj name>. The caller of this
// method is responsible for freeing the memory in "zc_names".
//
// Return Values :
//	CL_NOERR	- No error.
//	CL_ENOMEM	- Not enough memory

extern clerrno_t
parse_zc_names_from_names_str(char **names, ValueList zc_names);

//
// parse_zc_names_from_valuelist(ValueList names, ValueList zc_names);
//
// This method will read each string in "names" and parse it
// to read the zone cluster name. The format expected by this
// method is <zonecluster name>:<obj name>. The caller of this
// method is responsible for freeing the memory in "zc_names".
//
// Return Values :
//	CL_NOERR	- No error.
//	CL_ENOMEM	- Not enough memory

extern clerrno_t
parse_zc_names_from_valuelist(ValueList names, ValueList zc_names);

//
// check_single_zc_specified(ValueList names, char **zc_name,
//	boolean_t is_name);
//
// This method parses all the object names specified in the list "names"
// for zone cluster name and ensures that each name in the list
// has been scoped to the same zone cluster name. The format
// for which this method parses is <zonecluster name>:<object name>.
// This method set the value of "is_same" to "B_TRUE" if all the names
// had the same zone cluster name (or if none of them had any zone
// cluster scoping). In any other case, this method returns false.
// If this method is called from a global zone, this method would
// treat "global" zonecluster name same as a NULL zonecluster name.
// The zonecluster name, which was found in all the object names,
// will be set as the value in "zc_name". This method will not check
// for the validity of the zone cluster. The caller of this method
// has to free the memory of "zc_name". The caller of this method can
// specify the scconf parser as the third parameter. The default parser
// is "scconf_parse_obj_name_from_cluster_scope(). The parsing for RTs
// is different from the parsing for other obj names. So, to use this
// method with scconf RT parser, the caller must specify
// "scconf_parse_rt_name_from_cluster_scope" as the third parameter.
//
// Return Values :
//	CL_NOERR	- No error.
//	CL_EINVAL	- More than one zone cluster was specified.
//	CL_EINTERNAL	- Internal Error.
//
extern clerrno_t
check_single_zc_specified(ValueList &names, char **zc_name,
    boolean_t *is_same,
    scconf_errno_t (*scconf_parser)(char *fullname, char **objname,
					char **zcname) = NULL);

//
// remove_zc_names_from_valuelist(ValueList names, ValueList obj_names);
//
// This method will read each string in "names" and parse it
// to read the zone cluster name. The format expected by this
// method is <zonecluster name>:<obj name>. This method will then
// remove the zone cluster name and add the pure object name to
// "obj_names".The caller of this method is responsible for freeing
// the memory in "obj_names".
//
// Return Values :
//	CL_NOERR	- No error.
//	CL_ENOMEM	- Not enough memory

extern clerrno_t
remove_zc_names_from_valuelist(ValueList &names, ValueList &zc_names);

//
// remove_zc_names_from_names(char** obj_names);
//
// This method will read each string in "names" and parse it
// to read the zone cluster name. The format expected by this
// method is <zonecluster name>:<obj name>. This method will then
// remove the zone cluster name and update the string with the pure
// object name to.
// Note that this method will modify the contents of "obj_names".
//
// Return Values :
//	CL_NOERR	- No error.
//	CL_ENOMEM	- Not enough memory

clerrno_t
remove_zc_names_from_names(char** obj_names);

/*
 * exec_command(char *cmd_str)
 *
 * Executes the command specified by "cmd_str".
 * This function waits for the command to finish execution
 * and then returns. Essentially invokes the "execv" call.
 * Returns the exit value from the exec'd command.
 * Possible return values:
 *
 *	CL_NOERR		- success
 *	CL_EIO			- io error
 *	CL_EINTERNAL		- internal error
 *	CL_EINVAL		- invalid argument(command)
 */

extern clerrno_t
exec_command(char *cmd_str);

/*
 * Executes and pipes the command .
 *
 * Caller is responsible to close the pipe.
 * "file" will be updated to be a pointer to the stream.
 * "cmd" is the command to be executed.
 *
 * Possible return values:
 *
 *	CL_NOERR		- success
 *	CL_EIO			- io error during the pipe
 *	CL_EINTERNAL		- internal error
 *	CL_EINVAL		- invalid argument(command)
 */
extern clerrno_t exec_and_pipe_cmd(FILE **file, char *cmd);

/*
 * Executes a given command("cmd") in a new thread .
 * This function can be used to execute time consuming commands.
 * This function internally uses "exec_and_pipe_cmd" to
 * execute a command. The "thread" variable is updated with the
 * thread ID as is given by the pthread_create() function call.
 * The caller is responsible to determine when to join and wait
 * for the thread.
 *
 * Possible return values:
 *
 * 	CL_NOERR		- success
 *	CL_EIO			- io error during the pipe
 *	CL_EINTERNAL		- internal error
 *	CL_EINVAL		- invalid argument
 */
extern clerrno_t execute_cmd_in_new_thread(pthread_t *thread, char *cmd);

/*
 * Executes a given command on the specified node in a separate
 * thread. This function can be used to execute time consuming
 * commands. The "thread" variable is updated with the
 * thread ID as is given by the pthread_create() function call.
 * The caller is responsible to determine when to join and wait
 * for the thread. This function uses the "scrcmd" command to
 * execute the command on a remote node. This function uses the
 * struct cl_cmd_info to fetch the required information. In
 * the case where the command being executed causes an error,
 * this function will invoke the error handled specified. Please
 * see errorhandler.h for more information about cl_cmd_info_t.
 * Please declare all your error handlers in errorhandler.h, so
 * that this method is able to invoke the error handler in case
 * there is an error in the command execution or if the command
 * returns a non-zeo exit status.
 *
 * Possible return values:
 *
 * 	CL_NOERR		- success
 *	CL_EIO			- io error during the pipe
 *	CL_EINTERNAL		- internal error
 *	CL_EINVAL		- invalid argument
 */
extern clerrno_t execute_cmd_at_node_in_new_thread(pthread_t *thread,
	cl_cmd_info_t *cmd_info);

/*
 * get_valid_nodes
 *
 * Filters the given nodelist and returns only the set of
 * valid cluster nodes. The valid nodes will be added to
 * the ValueList "valid_nodes".
 *
 * Return Values :
 *	CL_NOERR	- All nodes in the list are valid
 *	CL_EINVAL	- One or more nodes are not found or
 *			  not in cluster
 */
extern clerrno_t
get_valid_nodes(ValueList &nodes, ValueList &valid_nodes);

#if (SOL_VERSION >= __s10)
//
// get_valid_zone_clusters
//
// Filters the given zone cluster list and returns only the set of
// valid zone cluster names.
// This method functions according to the context from which it has
// been called. If called from a global zone, this function will
// check whether each zone cluster is valid or not by checking
// the zone cluster id. If called from a zone cluster, this function
// will report an error unless a zone cluster name matches with the
// name of the current zone cluster. Please note that "global"
// will be treated as a valid zone cluster name, when this method
// is called in a global zone.
//
// Return Values :
//	CL_NOERR	- All zone clusters in the list are valid
//	CL_ENOENT	- One or more zone clusters are invalid/unknown
//	CL_EINTERNAL	- Internal Error.
//
extern clerrno_t
get_valid_zone_clusters(ValueList &zc_names, ValueList &valid_zc_names);

//
// get_all_clusters
//
// Adds all the known zone cluster names to "all_clusters". If this method
// is called in a global zone, this method will append "global" to the list.
// If this method is called in a zone cluster, this method will append the
// name of the zone cluster itself. This method internally calls
// get_zone_cluster_names() defined in clzonecluster.h
// Return Values :
//	CL_NOERR	- No Error
//	CL_EACCESS	- Permission Error
//	CL_EINTERNAL	- Internal Error.
extern clerrno_t
get_all_clusters(ValueList &all_clusters);

#endif

/*
 * check_file_exists
 *
 * This function checks whether the specified file exists or not.
 * It also checks if the file is readable or not. If the file exists
 * and is also readable, then this function returns CL_ENOERR. If
 * the file does not exist or if it is not readable then this
 * function returns CL_ENOENT. If the filename is not specified
 * this function returns CL_EINVAL
 *
 * Return Values :
 *	CL_NOERR	- File exists and is readable
 *	CL_EINVAL	- File name was not specified
 *	CL_ENOENT	- File does not exist or is not readable
 */
extern clerrno_t
check_file_exists(char *filename);

#ifdef WEAK_MEMBERSHIP
/*
 * mark_ccr_as_winner
 *
 * This function checks the ccr contents of the the node and assign this copy
 * of ccr as valid copy. It returns 0 on success and non-zero on failure.
 *
 * Return Values :
 *	zero	    - ccr marked valid
 *	non-zero    - Unable to mark ccr valid
 */
extern int
mark_ccr_as_winner();

/*
 * mark_ccr_as_loser
 *
 * This function checks the ccr contents of the the node and assign this copy
 * of ccr as invalid copy. It returns 0 on success and non-zero on failure.
 *
 * Return Values :
 *	zero	    - ccr marked invalid
 *	non-zero    - Unable to mark ccr invalid
 */
extern int
mark_ccr_as_loser();

/*
 * split_brain_ccr_change_exists
 *
 * This function query the cluster and finds whether a split brain has occurred
 * or not. If a split brain has occurred then it returns "true" else
 * it returns "false".
 *
 * Return Values :
 *	true	- Split brain has occured in the cluster
 *	false	- There is no split brain in the cluster
 */
extern bool
split_brain_ccr_change_exists();

/*
 * convert_to_lowercase
 *
 * This function converts the given value in to its lower case equivalent
 * and return that to the caller
 *
 * Returns a NULL if the given value is invalid
 */
extern void
convert_to_lowercase(char *value, char **lowercase_value);
//
// is_valid_ipaddress
//
// This method checks whether a given argument is a valid
// IP Address syntactically
//
extern bool
is_valid_ipaddress(char *ipaddress);
#endif // WEAK_MEMBERSHIP
#endif /* _CLCOMMANDS_COMMON_H */
