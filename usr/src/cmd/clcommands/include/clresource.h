//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef	_CLRESOURCE_H
#define	_CLRESOURCE_H

#pragma ident	"@(#)clresource.h	1.6	08/07/24 SMI"

#include <sys/sol_version.h>

#include "clcommands.h"
#include "ClresourceExport.h"
#include "ClShow.h"
#include "ClStatus.h"

// List types
typedef enum listtype {
	LISTTYPE_NONE,			// "none"
	LISTTYPE_EXTENSION,		// clresource
	LISTTYPE_STANDARD,		// clresource
	LISTTYPE_ALL			// clresource
} listtype_t;

// Resource error flags that can be reset
typedef enum resource_errflag {
	ERRFLAG_NONE,			// "none"
	ERRFLAG_STOP_FAILED		// STOP_FAILED
} resource_errflag_t;

//
// This is the main header file for clresource library functions
//

extern clerrno_t clresource_create(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs,
    ValueList &resourcegroups, ValueList &resourcetypes,
    NameValueList &properties, NameValueList &xproperties,
    NameValueList &yproperties, ValueList &lhosts, ValueList &netiflist,
    ValueList &auxnodes);
extern clerrno_t clresource_create(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, char *clconfiguration,
    ValueList &resourcegroups, ValueList &resourcetypes,
    NameValueList &properties, NameValueList &xproperties,
    NameValueList &yproperties, ValueList &lhosts, ValueList &netiflist,
    ValueList &auxnodes);
extern clerrno_t clresource_delete(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, ValueList &resourcegroups, ValueList &resourcetypes);
extern clerrno_t clresource_disable(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, ValueList &resourcegroups, ValueList &resourcetypes,
    ValueList &nodes);
extern clerrno_t clresource_enable(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, ValueList &resourcegroups, ValueList &resourcetypes,
    ValueList &nodes);
extern clerrno_t clresource_export(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, char *clconfiguration);
extern clerrno_t clresource_list(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, ValueList &resourcegroups, ValueList &resourcetypes,
    ValueList &states, ValueList &zc_names);
extern clerrno_t clresource_list_props(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, ValueList &resourcegroups, ValueList &resourcetypes,
    listtype_t listtype, ValueList &properties, ValueList &xproperties,
    ValueList &yproperties);
extern clerrno_t clresource_set(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, ValueList &resourcegroups, ValueList &resourcetypes,
    NameValueList &properties, NameValueList &xproperties,
    NameValueList &yproperties);
extern clerrno_t clresource_monitor(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, ValueList &resourcegroups, ValueList &resourcetypes,
    ValueList &nodes);
extern clerrno_t clresource_clear(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, ValueList &resourcegroups, ValueList &resourcetypes,
    resource_errflag_t errflag, ValueList &nodes);
extern clerrno_t clresource_show(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, ValueList &resourcegroups, ValueList &resourcetypes,
    ValueList &properties, ValueList &xproperties, ValueList &yproperties,
    ValueList &zc_names);
extern clerrno_t clresource_status(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, ValueList &resourcegroups, ValueList &resourcetypes,
    ValueList &nodes, ValueList &states, ValueList &zc_names);
extern clerrno_t clresource_unmonitor(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, ValueList &resourcegroups, ValueList &resourcetypes,
    ValueList &nodes);
extern clerrno_t get_clresource_status_obj(optflgs_t optflgs,
    ClStatus *rs_status);
extern clerrno_t get_clresource_show_obj_select(ValueList &operands,
    optflgs_t optflgs, ValueList &resourcegroups, ValueList &resourcetypes,
    ValueList &properties, ValueList &xproperties, ValueList &yproperties,
    ClShow *rs_show, ValueList &zc_names = ValueList());
extern clerrno_t get_clresource_show_obj(optflgs_t optflgs, ClShow *rs_show);
extern clerrno_t get_prop_node(char *propname, char *nodename,
    char **prop_node);


#endif /* _CLRESOURCE_H */
