//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef	_CLUSTER_H
#define	_CLUSTER_H

#pragma ident	"@(#)cluster.h	1.14	09/01/16 SMI"

#include "clcommands.h"
#include "ClusterExport.h"
#include "ClShow.h"
#include "ClStatus.h"

#ifdef __cplusplus
#include "sys/os.h"
#endif

#include "scadmin/scconf.h"

// Property names specified in command line
#define	CLPROP_INSTALLMODE		"installmode"
#define	CLPROP_FENCING			"global_fencing"
#define	CLPROP_HB_TIMEOUT		"heartbeat_timeout"
#define	CLPROP_HB_QUANTUM		"heartbeat_quantum"
#define	CLPROP_UDP_SESSION_TIMEOUT	"udp_session_timeout"

#define	CLPROP_CLUSTERID	"clusterid"
#define	CLPROP_PRIV_NETADDR	"private_netaddr"
#define	CLPROP_PRIV_NETMASK	"private_netmask"
#define	CLPROP_PRIV_MAXNODE	"max_nodes"
#define	CLPROP_PRIV_MAXPRIV	"max_privatenets"
#define	CLPROP_NUM_ZCLUSTERS	"num_zoneclusters"
#define	SCPRIV_NETADDR		"netaddr"
#define	SCPRIV_NETMASK		"netmask"
#define	SCPRIV_MAXNODE		"maxnodes"
#define	SCPRIV_MAXPRIV		"maxprivatenets"
#define	CL_AUTHPROTOCOL		"authprotocol"

#define	CLI_SCPRIVIPADM		"/usr/cluster/bin/scprivipadm"
#define	CCR_CONF_FILE		"/etc/cluster/ccr/global/infrastructure"

// Object types that can be specified with cluster command
#define	TYPE_ACCESS		"access"
#define	TYPE_DEVICE		"device"
#define	TYPE_DEVGRP		"devicegroup"
#define	TYPE_GLOBAL		"global"
#define	TYPE_INTR		"interconnect"
#define	TYPE_INTRADAPTER	"adapter"
#define	TYPE_NAS		"nasdevice"
#define	TYPE_NODE		"node"
#define	TYPE_QUORUM		"quorum"
#define	TYPE_RG			"resourcegroup"
#define	TYPE_RS			"resource"
#define	TYPE_RSLH		"reslogicalhostname"
#define	TYPE_RSSA		"ressharedaddress"
#define	TYPE_RT			"resourcetype"
#define	TYPE_RSLH		"reslogicalhostname"
#define	TYPE_RSSA		"ressharedaddress"
#define	TYPE_SNMPHOST		"snmphost"
#define	TYPE_SNMPMIB		"snmpmib"
#define	TYPE_SNMPUSER		"snmpuser"
#define	TYPE_TELATTR		"telemetryattribute"
#define	TYPE_ZONECLUSTER	"zonecluster"

#define	STYPE_INTR		"intr"
#define	STYPE_DEVGRP		"dg"
#define	STYPE_RT		"rt"
#define	STYPE_RG		"rg"
#define	STYPE_RS		"rs"
#define	STYPE_RSLH		"rslh"
#define	STYPE_RSSA		"rssa"
#define	STYPE_DEVICE		"dev"
#define	STYPE_NAS		"nas"
#define	STYPE_TELATTR		"ta"

// Flags to indicate the cluster property attribute:
//	Read-only, can only be modified in cluster mode, non-cluster
//	mode, or both, or unknown properties.
#define	CLPROP_READONLY		1
#define	CLPROP_CLUSTER		2
#define	CLPROP_NONCLUSTER	3
#define	CLPROP_BOTH		4
#define	CLPROP_UNKNOWN		5

//
// NOTE:
// These error codes must match the exit codes in
// /usr/cluster/bin/scprivipadm !!
//
#define	SCIP_NOERR		0
#define	SCIP_EOP		1
#define	SCIP_EUSAGE		2
#define	SCIP_EINVAL		3
#define	SCIP_EINTERNAL		4

//
// Structure for cluster object type, its short name, and the function names
// to get the ClShow or ClStatus object.
//
struct cluster_object_type {
	char *obj_name;		// The object type name
	char *obj_shortname;	// The shortname of an object type
	uint_t obj_level;	// A number indicates the level in the obj tree
	bool has_xml;		// If the type has xml related subcmds or not
	show_heading_t show_head1;
	show_heading_t show_head2;
#ifdef WEAK_MEMBERSHIP
	show_heading_t show_head3;
#endif
	// The functions to get the ClShow object
	int (*do_clshow1) (optflgs_t optflgs, ClShow *obj_show1);
	int (*do_clshow2) (optflgs_t optflgs, ClShow *obj_show1,
	    ClShow *obj_show2);
#ifdef WEAK_MEMBERSHIP
	int (*do_clshow3) (optflgs_t optflgs, ClShow *obj_show1,
	    ClShow *obj_show2, ClShow *obj_show3);
#endif
	// The function to get the ClStatus object
	int (*do_clstatus1) (optflgs_t optflgs, ClStatus *obj_status1);
	int (*do_clstatus2) (optflgs_t optflgs, ClStatus *obj_status1,
	    ClStatus *obj_status2);
	int (*do_clstatus3) (optflgs_t optflgs, ClStatus *obj_status1,
	    ClStatus *obj_status2, ClStatus *obj_status3);
	int (*do_clstatus4) (optflgs_t optflgs, ClStatus *obj_status1,
	    ClStatus *obj_status2, ClStatus *obj_status3,
	    ClStatus *obj_status4);
};
typedef struct cluster_object_type cluster_object_type_t;

//
// Structure for cluster properties specified in command line,
// its name in CCR, and an bool variable to indicate whether it
// can be changed or not.
//
struct cluster_proplist {
	char *prop_cli_name;	// Property name specified in command line
	char *prop_ccr_name;	// Property name saved in CCR
	uint_t prop_attr;	// Property attribute
};
typedef struct cluster_proplist cluster_proplist_t;

extern cluster_object_type_t cl_objtypes[];
extern cluster_object_type_t zc_objtypes[];
extern cluster_proplist_t cluster_props[];
extern clcommandinit_t clusterinit;

//
// This is the main header file for cluster functions
//

extern clerrno_t cluster_create(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, char *clconfiguration);
extern clerrno_t cluster_check(ClCommand &cmd, ValueList &args);
extern clerrno_t cluster_export(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, ValueList &cltypes, char *clconfiguration);
extern clerrno_t cluster_list(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs);
extern clerrno_t cluster_list_cmds(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs);
extern clerrno_t cluster_set_netprops(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, NameValueList &properties);
extern clerrno_t cluster_set(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, NameValueList &properties);
extern clerrno_t cluster_rename(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, char *new_clname);
extern clerrno_t cluster_restore_netprops(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs);
extern clerrno_t cluster_show(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, ValueList &cltypes);
extern clerrno_t cluster_show_netprops(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs);
extern clerrno_t cluster_shutdown(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, ushort_t int_grace_time, char *std_message);
extern clerrno_t cluster_status(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, ValueList &cltypes);

extern clerrno_t cluster_conv_scconf_err(int printflag, scconf_errno_t err);
extern clerrno_t cluster_name_compare(char *cl_name);
extern uint_t cluster_check_property(char *cl_prop_name);

extern clerrno_t get_global_show_obj(optflgs_t optflgs, ClShow *global_clshow);

#endif /* _CLUSTER_H */
