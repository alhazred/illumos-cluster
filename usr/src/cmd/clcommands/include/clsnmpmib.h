//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef	_CLSNMPMIB_H
#define	_CLSNMPMIB_H

#pragma ident	"@(#)clsnmpmib.h	1.4	08/05/20 SMI"

#include "clcommands.h"
#include "ClCommand.h"
#include "ClsnmpmibExport.h"
#include "ClShow.h"

#include <unistd.h>
#include <sys/types.h>
#include <sys/clconf_int.h>
#include <wait.h>

#define	SCEVENTMIB	"/usr/cluster/bin/sceventmib"
#define	MIB_LIST_HEADER	"\n--- SNMP MIBs on node %s ---\n\n"
#define	EXIT_MESSAGE    "SCEVENTMIB_EXIT_STATUS:"

//
// This is the main header file for clsnmpmib functions
//

// The subcommand processing functions
extern int clsnmpmib_disable(ClCommand &cmd, ValueList &operands,
    ValueList &nodes);
extern clerrno_t clsnmpmib_enable(ClCommand &cmd, ValueList &operands,
    ValueList &nodes);
extern clerrno_t clsnmpmib_export(ValueList &operands,
    ValueList &nodes, char *clconfiguration);
extern clerrno_t clsnmpmib_list(ClCommand &cmd, ValueList &operands,
    ValueList &nodes);
extern clerrno_t clsnmpmib_set(ClCommand &cmd, ValueList &operands,
    ValueList &nodes, NameValueList &props);
extern clerrno_t clsnmpmib_show(ValueList &operands, ValueList &nodes);

// Common functions
extern clerrno_t run_sceventmib_cmd_on_node(char *argv[], char *node);
extern clerrno_t run_sceventmib_cmd_on_node(char *argv, char *node);
extern clerrno_t run_sceventmib_cmd_on_node_with_file(
    char *argv, char *node, char *file);
extern clerrno_t pipe_sceventmib_cmd_on_node(FILE **file, char *cmd,
    char *node);
extern clerrno_t execute_scrcmd_command(char **args);
extern clerrno_t conv_err_from_sceventmib(int error);
extern void set_error(clerrno_t &error, clerrno_t value);
extern clerrno_t get_all_cluster_nodes(ValueList &nodes);
extern clerrno_t add_this_node(ValueList &nodes);
extern clerrno_t verify_nodelist(ValueList &nodes);
extern clerrno_t strip_list_headings(FILE *fp);
extern clerrno_t verify_nodes_clustmode(ValueList &list, int &outofcluster);

// Helper functions
extern clerrno_t get_mib_operands(ValueList &operands, ValueList &new_operands);
extern ClShow *get_filtered_clsnmpmib_show_obj(ValueList &operands,
    char *node, clerrno_t &error);
extern ClShow *get_clsnmpmib_show_obj(char *node);
extern clerrno_t parse_stdout_to_snmp_mib_show(ClShow *&show_obj, FILE *file);
extern clerrno_t run_scrcmd_for_list_on_node(char *args, char *node, FILE **fp);

#endif /* _CLSNMPMIB_H */
