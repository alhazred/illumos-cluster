/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_MISC_H
#define	_MISC_H

#pragma ident	"@(#)misc.h	1.3	08/05/20 SMI"

#ifdef	__cplusplus
extern "C" {
#endif

/*
 * This header file contains declarations of functions
 * which need to invoked from out of the new-cli code base. One
 * such example might be some code in the old cli which has to
 * re-use a new-cli routine. Since the old-cli code is written
 * in C, the C++ headers present in this directory do not get
 * compiled with 'cc'. Hence, the need for a separate header
 * file. One such example where the new-cli code is invoked
 * outside is present in usr/src/cmd/rpc.scadmd/rpc_scadmd_proc.c
 */

extern clerrno_t clnode_clear_node(char *nodename,
				uint_t force_flag, char **msg_buffer);


#ifdef __cplusplus
}
#endif

#endif	/* _MISC_H */
