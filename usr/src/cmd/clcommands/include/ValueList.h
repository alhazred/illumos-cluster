//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef	_VALUELIST_H
#define	_VALUELIST_H

#pragma ident	"@(#)ValueList.h	1.7	08/07/24 SMI"

//
// This is the header file for the ValueList class.
//
// This class manages a collection of "string" values.
//

#include <list>
#include <iterator>

class ValueList {
public:
	// Constructors

	// Construct an empty value list.
	ValueList(bool isSet = false);

	// Construct a value list with it's first "string" element.
	ValueList(char *string, bool isSet = false);

	// Construct a value list.   If flagged, split the "string".
	ValueList(int splitflag, char *string, bool isSet = false);

	// Destructor
	~ValueList();


	// Member functions

	// Add a value to the list.
	void add(char *string);

	// Add value(s) to the list.   If flagged, split the "string".
	void add(int splitflag, char *string);

	// Append a string to the current element
	void append(char *string, char separator);

	// Prepend a string to the current element
	void prepend(char *string, char separator);

	// Clear the list.
	void clear();

	// Reset the iterator to the beginning of the list.
	void start();

	// Increment the iterator.
	void next();

	// Return 1 when the iterator is at the end.
	int end();

	// Get the current value.
	char *getValue();

	// Return 1 when the value is in the list.
	int isValue(char *value);

	// Return 1 when the value is in the list.
	int isValue(char *value, bool doCaseCompare);

	// Get the number of elements in the list.
	int getSize();

private:
	//
	// Split the string, using only the first "token" found in the "string".
	// Then, add each sub-string to the list.
	//
	void split(char *tokens, char *string);

	// The list
	std::list<char *> l;

	// The iterator
	std::list<char *>::iterator li;

	// Whether or not the list is a set
	bool set;
};

#endif /* _VALUELIST_H */
