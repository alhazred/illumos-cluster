//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef _CLDEVICEGROUP_H
#define	_CLDEVICEGROUP_H

#pragma ident	"@(#)cldevicegroup.h	1.4	08/05/20 SMI"

//
// Header file for cldevicegroup(1CL)
// Global definitions
//
#ifdef __cplusplus
#include <sys/os.h>
#endif
#include "clcommands.h"
#include "ClCommand.h"
#include "ClList.h"
#include "ClShow.h"
#include "ClStatus.h"
#include "ClXml.h"
#include "CldevicegroupExport.h"

#define	CLDG_RAWDISK_TYPE		"rawdisk"
#define	CLDG_SDS_TYPE			"sds"
#define	CLDG_SVM_TYPE			"svm"
#define	CLDG_PROP_FAILBACK		"failback"
#define	CLDG_PROP_PREFERENCED		"preferenced"
#define	CLDG_PROP_LOCALONLY		"localonly"
#define	CLDG_PROP_GLOBALDEV		"globaldev"
#define	CLDG_PROP_AUTOGEN		"autogen"
#define	CLDG_PROP_NUMSECONDARIES 	"numsecondaries"
#define	CLDG_OPER_SYNC			"sync"

#define	CLDG_MAX_NODEID			NODEID_MAX	// clconf_int.h

// Default values for properties
#define	CLDG_DEFAULT_NUMSECS		0

// scconf related strings
#define	CLDG_SCCONF_RAWDISK		"Disk"
#define	CLDG_SCCONF_LOCALDISK		"Local_Disk"
#define	CLDG_SCCONF_SDS			"SVM"
#define	CLDG_SCCONF_SVM			"Multi-owner_SVM"

#define	STR_TRUE			"true"
#define	STR_FALSE			"false"
#define	STR_YES				"yes"
#define	STR_NO				"no"

// dg suspension states
typedef enum {
	CLDG_NOT_SUSPENDED = 0,
	CLDG_SUSPENDED,
	CLDG_UNKNOWN
} cldg_suspended_state_t;

//
// Functions that does subcommand implementation
//

//
// "Create" sub command implementation for cldevicegroup command
// Expects that all validations are already properly done.
// Relies on scconf_add_ds_vers2 api from libscconf to create the
// device group in CCR.
//
// Possible return values :
//
//	CL_EINTERNAL	- Bad call or internal error
//	CL_ENOMEM	- Insufficient memory or other os resources
//	CL_ECLUSTER	- Not in cluster
//	CL_EINVAL	- Invalid input
//
clerrno_t
cldevicegroup_create(ValueList &dgtypes, ValueList &nodes,
    ValueList &devices, NameValueList &properties, ValueList &operands,
    optflgs_t optflags);


//
// cldevicegroup_create - with -i option
// Create the device group objects based on the information
// provided in a configuration file. The values provided in
// the command line will be written over the ones in the file.
//
// Possible return values :
//
//	CL_EINTERNAL	- Bad call or internal error
//	CL_ENOMEM	- Insufficient memory or other os resources
//	CL_ENOCLUSTER	- Not in cluster
//	CL_EINVAL	- Invalid input
//
clerrno_t
cldevicegroup_create(ClCommand &cmd, ValueList &dgtypes, ValueList &nodes,
    ValueList &devices, NameValueList &properties, ValueList &operands,
    optflgs_t optflags, char *clconfiguration);

//
// "set" sub command implementation for cldevicegroup command
// Expects that all validations are already properly done.
// Relies on scconf_change_ds_vers2 api from libscconf to modify the
// device group in CCR.
//
// Possible return values :
//
//	CL_EINTERNAL	- Bad call or internal error
//	CL_ENOMEM	- Insufficient memory or other os resources
//	CL_EINVAL	- Invalid input
//
clerrno_t
cldevicegroup_set(ValueList &dgtypes, ValueList &nodes,
    ValueList &devices, NameValueList &properties, ValueList &operands,
    optflgs_t optflags);

//
// "delete" sub command implementation for cldevicegroup command
// Expects that all syntax validations are already properly done.
// Relies on scconf_rm_dd_vers2 api from libscconf to delete the
// device group from configuration.
//
// Possible return values :
//
//	CL_EINTERNAL	- Bad call or internal error
//	CL_ENOMEM	- Insufficient memory or other os resources
//	CL_ENOCLUSTER	- Not in cluster
//	CL_EINVAL	- Invalid input
//
clerrno_t
cldevicegroup_delete(ValueList &dgtypes, ValueList &operands,
    optflgs_t optflags);

//
// "add-device" sub command implementation for cldevicegroup command
// Expects that all syntax validations are already properly done.
// Relies on scconf_add_ds_vers2 api from libscconf to add devices to
// the device group.
//
// Possible return values :
//
//	CL_EINTERNAL	- Bad call or internal error
//	CL_ENOMEM	- Insufficient memory or other os resources
//	CL_ENOCLUSTER	- Not in cluster
//	CL_EINVAL	- Invalid input
//
clerrno_t
cldevicegroup_adddevice(ValueList devices, ValueList operands,
    optflgs_t optflgs);

//
// "remove-device" sub command implementation for cldevicegroup command
// Expects that all syntax validations are already properly done.
// Relies on scconf_rm_ds_vers2 api from libscconf to remove devices
// from the device group.
//
// Possible return values :
//
//	CL_EINTERNAL	- Bad call or internal error
//	CL_ENOMEM	- Insufficient memory or other os resources
//	CL_ENOCLUSTER	- Not in cluster
//	CL_EINVAL	- Invalid input
//
clerrno_t
cldevicegroup_removedevice(ValueList devices, ValueList operands,
    optflgs_t optflgs);

//
// "add-node" sub command implementation for cldevicegroup command
// Expects that all syntax validations are already properly done.
// Relies on scconf_add_dd_vers2 api from libscconf to add nodes
// to the device group configuration.
//
// Possible return values :
//
//	CL_EINTERNAL	- Bad call or internal error
//	CL_ENOMEM	- Insufficient memory or other os resources
//	CL_ENOCLUSTER	- Not in cluster
//	CL_EINVAL	- Invalid input
//
clerrno_t
cldevicegroup_addnode(ValueList &dgtypes, ValueList &nodes,
    ValueList operands, optflgs_t optflgs);

//
// "remove-node" sub command implementation for cldevicegroup command
// Expects that all syntax validations are already properly done.
// Relies on scconf_rm_ds_vers2 api from libscconf to remove nodes
// from the device group configuration.
//
// Possible return values :
//
//	CL_EINTERNAL	- Bad call or internal error
//	CL_ENOMEM	- Insufficient memory or other os resources
//	CL_ENOCLUSTER	- Not in cluster
//	CL_EINVAL	- Invalid input
//
clerrno_t
cldevicegroup_removenode(ValueList &dgtypes, ValueList &nodes,
    ValueList operands, optflgs_t optflags);

//
// "enable" subcommmand implementation for cldevicegroup command.
// Takes the specified device groups to maintenance mode.
// Relies on the scswitch_cli_take_service_offline api from libscswitch.
//
// Possible return values :
//
//	CL_EINTERNAL	- Bad call or internal error
//	CL_ENOMEM	- Insufficient memory or other os resources
//	CL_ENOCLUSTER	- Not in cluster
//	CL_EINVAL	- Invalid input
//
clerrno_t
cldevicegroup_enable(ValueList dgtypes, ValueList operands,
    optflgs_t optflgs);

//
// offline/disable subcommmands implementation for cldevicegroup command.
// Takes the specified device groups into maintenance mode.
// Relies on the scswitch_cli_take_service_offline api from libscswitch.
//
// Possible return values :
//
//	CL_EINTERNAL	- Bad call or internal error
//	CL_ENOMEM	- Insufficient memory or other os resources
//	CL_ENOCLUSTER   - Not in cluster
//	CL_EINVAL	- Invalid input
//
clerrno_t
cldevicegroup_offline(ValueList dgtypes, ValueList operands,
    optflgs_t optflags, int disable_flag);

//
// "online"/"switch" subcommmands implementation for cldevicegroup command.
// Online and switch behave exactly in same manner. Switch is retained as
// as it provides more user intutive way of onlining. switch_flag is used
// to control the error messages.
//
// Onlines the specified device groups on a specified node.
// Relies on scswitch_switch_device_service api to switch a dg.
//
// Possible return values :
//
//	CL_EINTERNAL	- Bad call or internal error
//	CL_ENOMEM	- Insufficient memory or other os resources
//	CL_ENOCLUSTER	- Not in cluster
//	CL_EINVAL	- Invalid input
//
clerrno_t
cldevicegroup_online(ValueList dgtypes, ValueList nodes, ValueList operands,
    optflgs_t optflags, int switch_flag);

//
// "sync" sub command implementation for cldevicegroup command
// Expects that all validations are already properly done.
// Relies on scconf_change_ds_vers2 api from libscconf to modify the
// device group in CCR.
//
// Possible return values :
//
//	CL_EINTERNAL    - Bad call or internal error
//	CL_ENOMEM	- Insufficient memory or other os resources
//	CL_EINVAL	- Invalid input
//
clerrno_t
cldevicegroup_sync(ValueList &dgtypes, ValueList &operands, optflgs_t optflags);

//
// "list" subcommand implementation.
//
// Possible return values :
//
//	CL_EINTERNAL	- Bad call or internal error
//	CL_ENOMEM	- Insufficient memory or other os resources
//	CL_EINVAL	- Invalid input
//
clerrno_t
cldevicegroup_list(ValueList dgtypes, ValueList nodes, ValueList operands,
    optflgs_t optflags);

//
// "show" subcommand implementation.
//
// Possible return values :
//
//	CL_EINTERNAL	- Bad call or internal error
//	CL_ENOMEM	- Insufficient memory or other os resources
//	CL_EINVAL	- Invalid input
//
clerrno_t
cldevicegroup_show(ValueList dgtypes, ValueList nodes, ValueList operands,
    optflgs_t optflags);

//
// get_cldevicegroup_show_obj
//
// Gets the cldevicegroup show object. Written
// primarily for cluster show use. The object
// must be preallocated else CL_EINTERNAL will
// be returned.
//
clerrno_t
get_cldevicegroup_show_obj(optflgs_t optflgs, ClShow *cldg_show_obj);

//
// "status" subcommand implementation.
//
// Displays the status information for the device groups
// in various tables. The following tables will be shown
// In non-verbose mode :
//	Servers (primary and secondary), Status, and Multiowner dgs
// In verbose mode :
//	Servers, Spare Nodes, Inactive nodes, In Transistion nodes,
//	Status and Multiowner dgs
//
// Possible return values :
//
//	CL_EINTERNAL    - Bad call or internal error
//	CL_ENOMEM	- Insufficient memory or other os resources
//	CL_EINVAL	- Invalid input
//
clerrno_t
cldevicegroup_status(ValueList dgtypes, ValueList nodes, ValueList operands,
    optflgs_t optflags);

//
// get_cldevicegroup_status_obj
//
// Gets the status object containing the status
// of all device groups. Both Standard status info
// like primary server/ secondary server and additional
// status info like Spares nodes/ Transistion nodes etc
// is filled in respective objects. The memory for these
// objects should be pre-allocated.
//
clerrno_t
get_cldevicegroup_status_obj(optflgs_t optflgs, ClStatus *basic_status_obj,
    ClStatus *addnl_status_obj, ClStatus *multidg_status_obj);


//
// cldevicegroup_export
//
// Exports the device group configuration information to
// a xml file for reusuability. User can filter out dg list
// using node and type filters.
//
// Return Values :
//	CL_NOERR	-	Success
//	CL_ENOMEM	-	No memory
//	CL_EINTERNAL	-	Internal error
//	CL_EINVAL	-	Bad options
//
clerrno_t
cldevicegroup_export(ValueList dgtypes, ValueList nodes, ValueList operands,
    optflgs_t optflags, char *clconfiguration);

// Miscellanous  utility functions

//
// cldevicegroup_fix_dgtype
//
// Replaces the "svm" instances with "sds".
// Clients can provide either svm or sds,
// However libdcs and other libraries currently
// understand only "sds". In long term it is
// necessary to EOL the "sds" usage. The memory
// for the new list should be preallocated and
// freed by users of this functions.
//
void
cldevicegroup_fix_dgtype(ValueList &dgtypes, ValueList &new_dgtypes);

#endif /* _CLDEVICEGROUP_H */
