//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef	_PROPERTIES_H
#define	_PROPERTIES_H

#pragma ident	"@(#)Properties.h	1.4	08/05/20 SMI"

//
// This is the header file for the PropertyValue and Properties classes.
//

#include <map>
#include <string>

#include "ValueList.h"

#include <stdio.h>
#include <string.h>
#include <langinfo.h>
#include <iconv.h>

#define	UTF_8_FACTOR	6

// Format of property values may be "printf style"
enum properties_format {
	format_printf,
	format_unknown
};

class PropertyValue {
	// Allow "Properties" to access us
	friend class Properties;

public:
	// Basic Constuctor
	PropertyValue();

	// Destructor
	~PropertyValue();

	// Member functions

	//
	// Set the property value.
	//
	void setPropertyValue(char *value);

	//
	// Add a comment.
	//
	void addComment(char *comment, int unique);

	// Get the property value.
	const char *getPropertyValue();

	// Get the comments ValueList
	const ValueList *getComments();

private:
	// The value of this property
	std::string value;

	// List of comments associated with this value
	ValueList comments;
};

class Properties {
public:
	// Constructor
	Properties();

	// Destructor
	~Properties();

	// Member functions

	// Set the name of the default properties file.
	void setPropertiesFile(char *filename);

	// Get the name of the default properties file.
	const char *getPropertiesFile();

	// Load the current properties file on the next reference.
	void setLoadOnReference();

	// Include comments in load and store.
	void setIncludeComments();

	//
	// Set the property value for the given key.
	// Return null if successful.   If a key
	// already exists, return it's value.  Other
	// errors will return the null string.
	//
	const char *setProperty(char *key, char *value);

	//
	// Add a comment for the given key.
	// Return zero on success.
	//
	const int addComment(char *key, char *comment, int unique);

	// Get the property value from the key.
	const char *getProperty(char *key);

	// Get the comments ValueList from the key.
	const ValueList *getComments(char *key);

	// Load properties from the file set by setPropertiesFile()
	int load();

	// Load properties from the given properties file.
	int load(char *filename);

	// Load properties from the given properties file.
	// If preserve is non-zero, preserve escape sequences in values.
	int load(char *filename, int preserve);

	// Store properties to the file set by setPropertiesFile()
	int store();

	// Store the properties to the given file.
	int store(char *filename);

private:
	// The map
	std::map<std::string, PropertyValue *> m;

	// The iterator
	std::map<std::string, PropertyValue *>::iterator mi;

	// The default properties file
	char *propertiesFile;

	// If flag is non-zero, the properties file is sufficiently loaded
	unsigned int isLoaded;

	// If flag is non-zero, comments will be included in load and store
	unsigned int includeComments;

	// Add a kay-value pair to the map
	void add_key_value(std::string &key_string, std::string &value_string,
	    ValueList &comments, enum properties_format *pformat);

	// Returns an encoded string based off of the Unicode string found
	// in the next four characters read from the file.
	char *convertUnicode(FILE *fp, char *encoding);

	// Convert a string to given encoding from UCS-2
	char *convertUCS2String(const char *src_str, char *encoding);

	// iconv conversion descriptors
	iconv_t iconv_cd1;
	iconv_t iconv_cd2;
};

#endif /* _PROPERTIES_H */
