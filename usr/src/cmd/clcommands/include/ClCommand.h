//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef	_CLCOMMAND_H
#define	_CLCOMMAND_H

#pragma ident	"@(#)ClCommand.h	1.4	08/05/20 SMI"

//
// This is the header file for the ClCommand class.
//

#include "clcommands.h"

#include "ValueList.h"

class ClCommand {
public:
	// Constructors
	ClCommand();

	ClCommand(int argc_in, char *argv_in[], struct clcommandinit &init);

	// Destructor
	~ClCommand();

	// Member functions

	// Initialize this.
	void initCommand(int argc_in, char *argv_in[],
	    struct clcommandinit &init);

	// Get the command name
	char *getCommandName();

	// Get the version of this command.
	char *getCommandVersion();

	// Get command description
	char *getCommandDescription();

	// Get usage lines
	char **getCommandUsageLines();

	// Initialize properties of this subcommand
	int initSubCommand();

	// Get the list of available subcommands
	struct clsubcommand *getListOfSubcommands();

	// Get the subcommand name
	char *getSubCommandName();

	// Get subcommand flags
	unsigned int getSubCommandFlags();

	// Get the subcommand description
	char *getSubCommandDescription();

	// Get RBAC authorization for the subcommand
	char *getSubCommandRbacAuthorization();

	// Get usage lines for the subcommand
	char **getSubCommandUsageLines();

	// Get subcommand options
	struct cloptions *getSubCommandOptions();

	// Process the subcommand, once initialized.
	int doSubCommand();

	// Get the original uid.
	static uid_t getOriginaluid();

	// Get orginal egid
	static gid_t getOriginalgid();

	// Data

	// Item indicating the name of the command
	char *commandname;

	// Item indicating the argument count from "main"
	int argc;

	// Item indicating the argument list from "main"
	char **argv;

	// If not zero, verbose
	unsigned int isVerbose;

	// If not zero, node is in clustermode
	unsigned int isClusterMode;

private:
	// Initialize everything to zero
	void init_to_zero();

	// Statically defined command properties
	struct clcommandinit *command_properties;

	// Statically defined subcommand properties
	struct clsubcommand *subcommand_properties;

	// The original UID.
	static uid_t original_uid;

	// The original GID.
	static gid_t original_gid;

};

#endif /* _CLCOMMAND_H */
