//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef	_CLSNMPHOST_H
#define	_CLSNMPHOST_H

#pragma ident	"@(#)clsnmphost.h	1.2	08/05/20 SMI"

#include "clcommands.h"

#include "ClsnmphostExport.h"
#include "ClShow.h"
#include "clsnmpmib.h"

#define	HOST_LIST_HEADER "\n--- SNMP Hosts on node %s ---\n\n"

//
// This is the main header file for clsnmphost functions
//

// The subcommand processing functions
extern clerrno_t clsnmphost_add(ClCommand &cmd, ValueList &operands,
    ValueList &nodes, ValueList &communities);
extern clerrno_t clsnmphost_add(ClCommand &cmd, ValueList &operands,
    ValueList &nodes, ValueList &communities, char *clconfiguration);
extern clerrno_t clsnmphost_export(ValueList &operands,
    ValueList &nodes, ValueList &communities, char *clconfiguration);
extern clerrno_t clsnmphost_list(ClCommand &cmd, ValueList &operands,
    ValueList &nodes, ValueList &communities);
extern clerrno_t clsnmphost_remove(ClCommand &cmd, ValueList &operands,
    ValueList &nodes, ValueList &communities);
extern clerrno_t clsnmphost_show(ValueList &operands, ValueList &nodes,
    ValueList &communities);

// Helper functions
extern clerrno_t get_all_snmp_communities(ValueList &communities, char *node);
extern clerrno_t get_all_snmp_hosts(ValueList &hosts, char *community,
    char *node);
extern ClShow *get_filtered_clsnmphost_show_obj(ValueList &operands,
    ValueList &communiities, char *node, clerrno_t &error);
extern ClShow *get_clsnmphost_show_obj(char *node);
extern clerrno_t parse_stdout_to_snmp_host_show(ClShow *&show_obj, FILE *file);

#endif /* _CLSNMPHOST_H */
