//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef	_NAMEVALUE_H
#define	_NAMEVALUE_H

#pragma ident	"@(#)NameValue.h	1.3	08/05/20 SMI"

//
// This is the header file for the NameValue class.
//

class NameValue {
public:
	// Constructors

	// Construct a null name-value pair.
	NameValue();

	// Construct a name-value pair with a null value.
	NameValue(char *name_in);

	// Construct a name-value pair.
	NameValue(char *name_in, char *value_in);

	// Destructor
	~NameValue();


	// Member functions

	// Change the name.
	void setname(char *name_in);

	// Change the value;
	void setvalue(char *value_in);

	// Get the name.
	char *getname();

	// Get the value.
	char *getvalue();

private:
	// Initialize
	void init();

	// The name
	char *name;

	// The value
	char *value;
};

#endif /* _NAMEVALUE_H */
