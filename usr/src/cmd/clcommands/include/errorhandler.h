//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef	_ERRORHANDLER_H
#define	_ERRORHANDLER_H

#pragma ident	"@(#)errorhandler.h	1.10	08/08/01 SMI"

#include "clcommands.h"
#include "ClCommand.h"

// Include this header file if you want to execute
// a bunch of commands in separate threads and want
// to handle the error conditions of the command
// execution. This header file should contain the
// declaration of your error handlers which can be
// invoked by the "execute_cmd" set of functions
// declared in common.h. See commmon.h for more
// information on the functions.

/*
 * struct cl_cmd_info will be used to contain information
 * regarding a command to be executed, node where the command
 * has to be executed and a pointer to a function which be the
 * error handler. This struct is used by the function
 * execute_cmd_at_node_in_new_thread() declared in common.h
 * If you want to execute a command using /usr/cluster/lib/sc/scrcmd
 * then you might want to consider using "execute_cmd" set of
 * functions declared in common.h. If you want to execute a set
 * of commands in separate threads, then you can use the following
 * structure and pass it as an argument to the supported methods.
 * You can specify an error handler which will be invoked in the
 * case of an error status during the command execution. This
 * function has to be declared in this file so that the common
 * library is able to invoke the error handler. If you would like
 * to re-direct the output of the command to some log file (or console)
 * you can specify the file name with "out_redirect_file".
 */
struct cl_cmd_info {
	char *cmd_str;				// Command string
	char *nodename;				// Node where cmd will be run
	int redirect_output;			// Redirect output to console
	int cmd_exit_status;			// Exit status of the command
	char *command_error_str;		// Error message from zoneadm
	void (*err_handler)(cl_cmd_info);	// Error handler function
};

typedef struct cl_cmd_info cl_cmd_info_t;
#endif /* _ERRORHANDLER_H */
