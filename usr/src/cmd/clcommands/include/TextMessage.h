//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef	_TEXTMESSAGE_H
#define	_TEXTMESSAGE_H

#pragma ident	"@(#)TextMessage.h	1.3	08/05/20 SMI"

//
// This is the header file for the TextMessage class.
//

#include "PropertyResourceBundle.h"
#include "GetText.h"

#include <stdio.h>
#include <stdarg.h>

#define	TM_FLAG_VERBOSE		(1 << 0)	// print VERBOSE msgs
#define	TM_FLAG_PRINTID		(1 << 1)	// include message IDs in msgs
#define	TM_FLAG_PRINTCMD	(1 << 2)	// include cmd name in msgs
#define	TM_FLAG_PRINTWARNSTR	(1 << 3)	// include "Warning" prefix

#define	TM_DEFAULT_FLAGS	(TM_FLAG_PRINTCMD | TM_FLAG_PRINTID)
#define	STR_WARNING		"Warning"

class TextMessage {
public:
	// Constructors

	// Basic constructor
	TextMessage();

	// Constructor with command name and resource bundle
	TextMessage(char *commandname, PropertyResourceBundle &bundle);

	// Constructor with command name, resource bundle, and flags
	TextMessage(char *commandname, PropertyResourceBundle &bundle,
	    unsigned int flags);

	//
	// Constructor with command name, resource bundle, flags,
	// and alternate file pointer.
	//
	TextMessage(char *commandname, PropertyResourceBundle &bundle,
	    unsigned int flags, FILE *fp);

	// Destructor
	~TextMessage();


	// Member functions

	// Set the command name
	void setCommandName(char *commandname);

	// Get the command name used by this instance
	char *getCommandName(void);

	// Bind to a resource bundle
	void bindPropertyResourceBundle(PropertyResourceBundle &bundle);

	// Get the resource bundle used by this instance
	PropertyResourceBundle getPropertyResourceBundle(void);

	// Set the flags
	void setFlags(unsigned int flags);

	// Get the flags
	unsigned int getFlags(void);

	// Set the file pointer
	void setFilePointer(FILE *fp);

	// Set the error flags
	void setErrorFlags(unsigned int flags);

	// Get the error flags
	unsigned int getErrorFlags(void);

	// Set the warning flags
	void setWarningFlags(unsigned int flags);

	// Get the warning flags
	unsigned int getWarningFlags(void);

	// Set the message flags
	void setMessageFlags(unsigned int flags);

	// Get the message flags
	unsigned int getMessageFlags(void);

	// Get the file pointer
	FILE *getFilePointer(void);

	// Print error message
	void printError(char *message_in, ...);

	// Print error message
	void va_printError(char *message_in, va_list ap);

	// Print warning message
	void printWarning(char *message_in, ...);

	// Print warning message
	void va_printWarning(char *message_in, va_list ap);

	// Print info message
	void printMessage(char *message_in, ...);

	// Print info message
	void va_printMessage(char *message_in, va_list ap);

	// Print a message
	void printString(char *message_in, ...);

	// Print a message to designated file pointer
	void printString(FILE *fp_in, char *message_in, ...);

	// Print a message to designated file pointer with given flags
	void printString(unsigned int flags_in, FILE *fp_in,
	    char *message_in, ...);

	// Print a message, from it's ID
	int printStringId(char *msgid_in, ...);

	// Print a message, from it's ID, to designated file pointer
	int printStringId(FILE *fp_in, char *msgid_in, ...);

	//
	// Print a message, from it's ID, to designated file
	// pointer with given flags
	//
	int printStringId(unsigned int flags_in, FILE *fp_in,
	    char *msgid_in, ...);

	// Get the message ID of a string
	char *getMessageId(char *string);

private:
	// Initialize
	void init();

	// Print message
	void printstring(char *msgid_in, unsigned int flags_in, FILE *fp_in,
	    char *message_in, va_list ap);

	// Print message from it's messageid
	int printstringid(unsigned int flags_in, FILE *fp_in,
	    char *msgid_in, va_list ap);

	// The command name
	char *commandname;

	// The resource bundle to use for localization
	PropertyResourceBundle *pbundle;

	// GetText
	GetText getText;

	// The file pointer
	FILE *fp;

	// The flags
	unsigned int flags;

	// The error flags
	unsigned int errorflags;

	// The warning flags
	unsigned int warningflags;

	// The message flags
	unsigned int messageflags;
};

#endif /* _TEXTMESSAGE_H */
