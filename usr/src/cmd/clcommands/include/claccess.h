/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _CLACCESS_H
#define	_CLACCESS_H

#pragma ident	"@(#)claccess.h	1.2	08/05/20 SMI"

//
// This is the header file for the claccess(1CL) command
//

#include "clcommands.h"
#include "ClShow.h"

#ifdef __cplusplus
#include <sys/os.h>
#endif

#include <scadmin/scconf.h>

// Display text used by list and show commands
#define	CLA_CLNAME	GETTEXT("Cluster name")
#define	CLA_ALLOWED	GETTEXT("Allowed hosts")
#define	CLA_AUTHPROTO	GETTEXT("Authentication Protocol")
#define	CLA_ANY		GETTEXT("Any")
#define	CLA_NONE	GETTEXT("None")

// Authentication protocol types
#define	CLA_SYS		"sys"
#define	CLA_UNIX	"unix"
#define	CLA_DES		"des"
#define	CLA_UNKNOWN	"unknown"

// Subcommand
extern clerrno_t claccess_allow(ClCommand &cmd, ValueList &hosts);
extern clerrno_t claccess_allow_all(ClCommand &cmd);
extern clerrno_t claccess_deny(ClCommand &cmd, ValueList &hosts);
extern clerrno_t claccess_deny_all(ClCommand &cmd);
extern clerrno_t claccess_list(ClCommand &cmd);
extern clerrno_t claccess_set(ClCommand &cmd, char *authprotocol);
extern clerrno_t claccess_show(ClCommand &cmd);

// Miscellaneous functions
extern clerrno_t claccess_conv_scconf_err(scconf_errno_t scconf_err);
extern clerrno_t get_access_show_obj(optflgs_t optflgs, ClShow *access_clshow);
extern void claccess_perror(clerrno_t clerrno);

#endif /* _CLACCESS_H */
