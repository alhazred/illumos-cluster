//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef _CLTELEMETRYATTRIBUTE_H
#define	_CLTELEMETRYATTRIBUTE_H

#pragma ident	"@(#)cltelemetryattribute.h	1.3	08/05/20 SMI"

//
// This is the header file for the cltelemetryattribute(1CL) command
//

#include "clcommands.h"
#include "ClCommand.h"
#include "scslm_sensor.h"
#include "scslm_ccr.h"
#include "scslm_cli.h"
#include "ClShow.h"
#include "ClStatus.h"

#define	DO_ENABLE	1
#define	DO_DISABLE	2
#define	EMPTY_VALUE	"<NULL>"

#define	CMD_PRINT	1
#define	CMD_STATUS	2

#define	LONG_KI		2

#define	COMMAND_BUFFER_SIZE	8192

extern clcommandinit_t cltainit;

// Subcommand processing
extern clerrno_t clta_disable(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, ValueList &obj_types);
extern clerrno_t clta_disable(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, char *clconfiguration, ValueList &obj_types);
extern clerrno_t clta_print(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, char *date_range, ValueList &obj_types,
    ValueList &obj_instances, ValueList &nodes);
extern clerrno_t clta_enable(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, ValueList &obj_types);
extern clerrno_t clta_enable(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, char *clconfiguration, ValueList &obj_types);
extern clerrno_t clta_export(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, char *clconfiguration, ValueList &obj_types);
extern clerrno_t clta_list(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, ValueList &obj_types);
extern clerrno_t clta_set_threshold(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, ValueList &obj_types, ValueList &obj_instances,
    ValueList &nodes, NameValueList &threshold);
extern clerrno_t clta_show(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, ValueList &obj_types, ValueList &obj_instances,
    ValueList &nodes);
extern clerrno_t clta_status(ClCommand &cmd, ValueList &operands,
    optflgs_t optflgs, ValueList &obj_types, ValueList &obj_instances,
    ValueList &nodes);

extern clerrno_t clta_do_set_talist(ClCommand &cmd, ValueList &clta_attributes,
    ValueList &clta_types, optflgs_t optflgs, uint_t set_flg);
extern clerrno_t clta_do_set_talist_xml(ClCommand &cmd,
    ValueList &clta_attributes, ValueList &clta_types, char *clconfiguration,
    optflgs_t optflgs, uint_t set_flg);
extern clerrno_t clta_preprocess_talist(ValueList &clta_attributes,
    ValueList &clta_types, ki_t **valid_clta_list, bool print_err);
extern clerrno_t clta_preprocess_thresh(ValueList &obj_instances,
    ValueList &nodes, ki_t *valid_clta_list,
    threshold_list_t **valid_thresholds);
extern clerrno_t get_clta_show_obj(optflgs_t optflgs, ClShow *clta_clshow);
extern clerrno_t get_clta_status_obj(optflgs_t optflgs,
    ClStatus *clta_clstatus);
extern void clta_free_ta_list(ki_t *clta_ki_list);
extern bool clta_isvalue_in_kilist(ki_t *clta_ki_list, const char *ki_name,
    const char *ki_motype);
extern char *clta_get_cacao_cmd(uint_t cmd_type);
extern ki_t *clta_get_ki_from_ki_list(ki_t *ki_list, const char *ki_name,
    const char *mo_type);
extern char *clta_convert_valuelist_str(ValueList &value_list, char *starter,
    char *separator, uint_t str_flg);
extern bool clta_is_node(NameValueList &valid_nodes, const char *node_name);

#endif /* _CLTELEMETRYATTRIBUTE_H */
