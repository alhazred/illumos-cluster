//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef	_PROPERTYRESOURCEBUNDLE_H
#define	_PROPERTYRESOURCEBUNDLE_H

#pragma ident	"@(#)PropertyResourceBundle.h	1.3	08/05/20 SMI"

//
// This is the header file for the PropertyResourceBundle class.
//
// This class is used to manage bundles of resource property files.
//
// Each PropertyResourceBundle is associated with a bundle, or family,
// of resource property files.   Each file in the bundle has a common
// directory and base name.
//

#include <Properties.h>

#include <stdio.h>
#include <string.h>

class PropertyResourceBundle {
public:
	// Basic Constructor
	PropertyResourceBundle();

	// Constructor with family directory and base name of properties files
	PropertyResourceBundle(char *directory_in, char *basename_in);

	// Constructor with family directory, base name, and current locale
	PropertyResourceBundle(char *directory_in, char *basename_in,
	    char *locale);

	// Destructor
	~PropertyResourceBundle();

	// Member functions

	//
	// Set the family directory and base name of properties files.
	// Return zero, if there is no default resource properties file here.
	//
	int setFamily(char *directory, char *basename);

	// Set the locale to use
	void setLocale(char *locale);

	// Get the current locale setting used by this bundle.
	char *getLocale();

	// Get a string for the given key from this resource bundle.
	const char *getString(char *key);

private:
	// Initialize
	void init(void);

	// Private data
	char *directory;	// Directory name used by bundle family
	char *basename;		// Base name used by bundle family
	char *locale;		// Current locale
	Properties properties1;	// Search loclized properties first
	Properties properties2;	// Search default properties second
};

#endif /* _PROPERTYRESOURCEBUNDLE_H */
