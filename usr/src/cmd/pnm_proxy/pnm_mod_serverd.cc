/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)pnm_mod_serverd.cc	1.9	08/07/30 SMI"

//
// PNM proxy server daemon.
// The daemon accepts a subset of PNM commands from the global zone
// and executes them in the exclusive-ip zone. It also accepts
// commands from the zone cluster and executes them in the global zone.
//

#include <nslib/ns.h>
#include <libintl.h>
#include <signal.h>
#include <pnm_mod_impl.h>
#include <sys/sc_syslog_msg.h>
#include <zone.h>

//
// The highest version that we currently support.
// This number must be changed whenever we re-version the naming.vp
// also change the vp-idl mapping below in such a case.
//
#define	NAMING_VP_MAX_MAJOR	2
#define	NAMING_VP_MAX_MINOR	0
static version_manager::vp_version_t highest_version =
	{NAMING_VP_MAX_MAJOR, NAMING_VP_MAX_MINOR};

static version_manager::vp_version_t current_version = {0, 0};

//
// The template for the object that will receive the upgrade callbacks.
//
class pnmproxy_upgrade_callback :
    public McServerof < version_manager::upgrade_callback > {
public:
	pnmproxy_upgrade_callback();

	//
	// The callback object can go away once all the associated uccs have
	// unregistered.
	//
	void _unreferenced(unref_t cookie);

	//
	// Actual callback method which will be invoked by the callback
	// framework during upgrade_commit.
	//
	void do_callback(const char *ucc_name,
	    const version_manager::vp_version_t &new_version, Environment &e);
};

pnmproxy_upgrade_callback::pnmproxy_upgrade_callback()
{
}

void
pnmproxy_upgrade_callback::_unreferenced(unref_t cookie)
{
	ASSERT(_last_unref(cookie));
	delete this;
}

void
pnmproxy_upgrade_callback::do_callback(const char *,
	const version_manager::vp_version_t &new_version, Environment &)
{
	pnm_mod::pnm_server_var			pnm_server_v;
	Environment				env;
	naming::naming_context_var		ctx_v;
	sc_syslog_msg_handle_t			handle;
	char					srv_name[100];

	(void) sc_syslog_msg_initialize(&handle, PNM_PROXY_SERVER, "");

	current_version = new_version;
	if (current_version.major_num >= 2) {
		ctx_v = ns::root_nameserver_c1();

		if (CORBA::is_nil(ctx_v)) {
			//
			// SCMSGS
			// @explanation
			// The pnm proxy program could not get the ALL_CLUSTER
			// context from the global nameserver.
			// @user_action
			// Contact your authorized Sun service provider to
			// determine whether a workaround or patch is
			// available.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "Could not get the cluster context from the name "
			    "server. Exiting.");
			sc_syslog_msg_done(&handle);
			exit(1);
		}
		//
		// Register the pnm_server object with the name server and stay
		// around to process requests.
		//
		pnm_server_v = (new pnm_server_impl)->get_objref();
		os::sprintf(srv_name, "pnm_server.%d.%s",
		    orb_conf::node_number(), "global");
		ctx_v->rebind(srv_name, pnm_server_v, env);

		if (env.exception() != NULL) {
			//
			// Rebind failed.
			//
			env.clear();

			//
			// SCMSGS
			// @explanation
			// The pnm proxy program could not bind the corba
			// object in the global name server.
			// @user_action
			// Contact your authorized Sun service provider to
			// determine whether a workaround or patch is
			// available.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "Error while binding 'pnm_proxy_server' "
			    "in the name server. Exiting.");
			sc_syslog_msg_done(&handle);
			exit(1);
		}
		//
		// SCMSGS
		// @explanation
		// The pnm proxy program could bind CORBA
		// object in the all cluster context of the global
		// name server.
		// @user_action
		// This is an informational message. No user
		// action is required.
		//
		(void) sc_syslog_msg_log(handle, LOG_INFO, MESSAGE,
		    "Successfull in binding 'pnm_proxy_server' "
		    "in the name server.");
	}
	sc_syslog_msg_done(&handle);
}

//
// Utility function to mask all the maskable signals.
//
void
block_allsignals(void)
{
	sigset_t		s_mask;
	sc_syslog_msg_handle_t	handle;

	(void) sc_syslog_msg_initialize(&handle, PNM_PROXY_SERVER, "");

	//
	// Block the signals that can be masked.
	//
	if (sigfillset(&s_mask) == -1) {
		//
		// SCMSGS
		// @explanation
		// pnm proxy program has encountered a failed
		// sigfillset(3C) call. The error message
		// indicates the error number for the failure.
		// @user_action
		// Contact your authorized Sun service provider to
		// determine whether a workaround or patch is available.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "PNM proxy : block_allsignals() : sigfillset "
		    "returned %d. Exiting.", errno);
		sc_syslog_msg_done(&handle);
		exit(1);
	}

	if (thr_sigsetmask(SIG_BLOCK, &s_mask, NULL) != 0) {
		//
		// SCMSGS
		// @explanation
		// pnm proxy program has encountered a failed
		// thr_sigsetmask(3C) system call. The error message
		// indicates the error number for the failure.
		// @user_action
		// Contact your authorized Sun service provider to
		// determine whether a workaround or patch is available.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "PNM proxy : block_all_signals() : thr_sigsetmask "
		    "returned %d. Exiting.", errno);
		sc_syslog_msg_done(&handle);
		exit(1);
	}
	sc_syslog_msg_done(&handle);
}

//
// Wait for signals and log any signal received. The SIGHUP signal is
// ignored after logging a message. For any other signal the action is
// to exit after logging the information. This is run as a seperate
// thread.
//
void *
wait_for_signals(void *)
{
	sigset_t	s_mask;
	siginfo_t	sinfo;
	int		sig	= 0;

	sc_syslog_msg_handle_t	handle;
	(void) sc_syslog_msg_initialize(&handle, PNM_PROXY_SERVER, "");

	//
	// Here we block all maskable signals.
	//
	block_allsignals();

	//
	// Fill the mask with all the signals in the system
	//
	if (sigfillset(&s_mask) == -1) {
		//
		// scmsgs already explained.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "sigfillset returned %d. Exiting.", errno);
		sc_syslog_msg_done(&handle);
		exit(1);
	}

	//
	// sigwait can return with EINTR when a thread forks.
	// Also ignore SIGHUP. We could have simply added SIGHUP to the
	// list of signals to be masked but the check is here so that we
	// can add a syslog message if necessary for SIGHUP.
	//
	do {
		sig = sigwaitinfo(&s_mask, &sinfo);
	} while (((sig < 0) && (errno == EINTR)) || (sig == SIGHUP));

	switch (sig) {
	case -1:
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "sigwaitinfo returned %d. Exiting.", errno);
		sc_syslog_msg_done(&handle);
		exit(1);
		/* FALLTHROUGH */
		/* NOTREACHED */
	default:
		//
		// When we get SIGTERM log the fact so that we will know when
		// we terminate normally versus abnormally
		//
		if (sig == SIGTERM) {
			//
			// SCMSGS
			// @explanation
			// The pnm program received a SIGTERM signal and is
			// exiting.
			// @user_action
			// This is an informational message. No user action
			// is required.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "PNM proxy server killed(sigterm) Exiting.");
			sc_syslog_msg_done(&handle);
			exit(1);
			/* NOTREACHED */
		}
		//
		// SCMSGS
		// @explanation
		// The pnm proxy program received an unexpected signal and is
		// exiting.
		// @user_action
		// Contact your authorized Sun service provider to
		// determine whether a workaround or patch is available.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "PNM proxy : Unexpected signal, Exiting.");
		sc_syslog_msg_done(&handle);
		exit(1);
	}
	/* NOTREACHED */
	return ((void *)NULL);
}

//
// The pnm proxy server daemon process. Here we create two threads,
// one to handle signals and the other to handle requests to execute tasks.
//
void
daemon_process()
{
	//
	// Mask the signals.
	//
	block_allsignals();

	pnm_mod::pnm_server_var			pnm_server_v;
	Environment				env;
	thread_t				sig_tid;
	naming::naming_context_var		ctx_v;
	sc_syslog_msg_handle_t			handle;
	char					srv_name[100];
	char					zonename[ZONENAME_MAX];

	//
	// Get the zoneid of the zone where we are executing.
	//
	zoneid_t zoneid = getzoneid();
	(void) sc_syslog_msg_initialize(&handle, PNM_PROXY_SERVER, "");

	//
	// If in the global zone context, then this proxy PNM service
	// is to service PNM invocations from zone clusters. Hence
	// we need to bind in the ALL_ZONES context. If in
	// the local zone context, then this has to be an exclusive-ip zone
	// that handles request from the global zone and therefore will be
	// bound in the global zone context.
	//
	if (zoneid == 0) {
		//
		// The PNM proxy service daemon in the global zone exists to
		// service zone clusters. The proxy service daemon binds its
		// object reference in the ALL_CLUSTER context so that the
		// zone clusters can invoke it. If rolling upgrade is in
		// progress, this context is not available till the upgrade
		// commits. Therefore, we need to register with the VM so
		// that we can then go ahead and register the reference.
		//
		version_manager::vm_admin_var vm_adm = vm_util::get_vm();
		if (CORBA::is_nil(vm_adm)) {
			//
			// SCMSGS
			// @explanation
			// PNM proxy got a nil reference to the Version manager
			// and hence failed to retrieve the version.
			// @user_action
			// Contact your authorized Sun service provider to
			// determine whether a workaround or patch is available.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "PNM proxy : Unable to retrieve version from "
			    "version manager: %s",
			    "nil reference to version manager");
			sc_syslog_msg_done(&handle);
			exit(1);
		}
		// create our callback object
		pnmproxy_upgrade_callback *cbp =
		    new pnmproxy_upgrade_callback();
		version_manager::upgrade_callback_var cb_v = cbp->get_objref();

		//
		// Create our ucc sequence.  We have only one ucc.
		// The sequence frees its elements with delete[], so we must
		// allocate them with new[] (via os::strdup).
		//
		version_manager::ucc_seq_t ucc_seq(1, 1);
		ucc_seq[0].ucc_name = os::strdup("pnm_proxy");
		ucc_seq[0].vp_name = os::strdup("naming");

		//
		// Add the naming ucc's name to the upgrade_before list of
		// the pnmproxy ucc, so that PNM proxy gets a upgrade callback
		// only after naming has finished its upgrade callback work.
		//
		char **pnmproxy_dependency_name = new char *[1];
		//
		// The sequence frees its elements with delete[], so we must
		// allocate them with new[] (via os::strdup).
		//
		pnmproxy_dependency_name[0] = os::strdup("naming");
		version_manager::string_seq_t pnmproxy_ucc_before_seq(
		    1, 1, pnmproxy_dependency_name, true);
		ucc_seq[0].upgrade_before = pnmproxy_ucc_before_seq;

		//
		// Actually register the callbacks and retrieve the current
		// version.
		//
		vm_adm->register_upgrade_callbacks(ucc_seq, cb_v,
		    highest_version, current_version, env);
		if (env.exception()) {
			if (version_manager::invalid_ucc::
			    _exnarrow(env.exception())) {
				//
				// SCMSGS
				// @explanation
				// An exception was raised when PNM proxy
				// tried to register for upgrade callbacks with
				// the Version Manager.
				// @user_action
				// Contact your authorized Sun service provider
				// to determine whether a workaround or patch is
				// available.
				//
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE, "Unable to register "
				    "for upgrade callbacks with version "
				    "manager: %s", "invalid ucc exception");
			} else if (version_manager::unknown_vp::
			    _exnarrow(env.exception())) {
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE, "Unable to register for upgrade "
				    "callbacks with version manager: %s",
				    "unknown vp exception");
			} else if (version_manager::not_found::
			    _exnarrow(env.exception())) {
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE, "Unable to register for upgrade "
				    "callbacks with version manager: %s",
				    "vp not found exception");
			} else if (version_manager::wrong_mode::
			    _exnarrow(env.exception())) {
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE, "Unable to register for upgrade "
				    "callbacks with version manager: %s",
				    "wrong mode exception");
			} else if (version_manager::too_early::
			    _exnarrow(env.exception())) {
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE, "Unable to register for upgrade "
				    "callbacks with version manager: %s",
				    "too early exception");
			} else {
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE, "Unable to register for upgrade "
				    "callbacks with version manager: %s",
				    "unknown exception");
			}
			env.clear();
			sc_syslog_msg_done(&handle);
			exit(1);
		}
		//
		// Check if we already have the current version that supports
		// ALL_CLUSTER context.
		//
		if (current_version.major_num >= 2) {
			ctx_v = ns::root_nameserver_c1();
		}
	} else {
		ctx_v = ns::root_nameserver();
	}

	// Daemonize.
	(void) setsid();

	//
	// Create a thread that handles any signal and logs an
	// appropriate message.
	//
	if (thr_create(NULL, NULL, wait_for_signals, NULL, THR_DETACHED,
	    &sig_tid) != 0) {
		//
		// SCMSGS
		// @explanation
		// The pnm proxy program encountered a failure while
		// executing thr_create(3C). The failure could be because
		// the system may be running low on memory.
		// @user_action
		// Contact your authorized Sun service provider to
		// determine whether a workaround or patch is available.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "PNM proxy: Unable to create thread, errno = %d. "
		    "Exiting\n.", errno);
		sc_syslog_msg_done(&handle);
		exit(1);
	}

	if ((current_version.major_num >= 2) || (zoneid != 0)) {
		//
		// Register the pnm_server object with the name server and stay
		// around to process requests.
		//
		pnm_server_v = (new pnm_server_impl)->get_objref();

		//
		// Get the zonename of the zone where we are executing.
		//
		if (getzonenamebyid(zoneid, zonename, sizeof (zonename)) < 0) {
			//
			// SCMSGS
			// @explanation
			// The pnm proxy program encountered a failure when
			// trying to get the zone name.
			// @user_action
			// Contact your authorized Sun service provider to
			// determine whether a workaround or patch is available.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "Unable to get zonename for zone id %d, "
			    "errno = %d. Exiting\n.", zoneid, errno);
			sc_syslog_msg_done(&handle);
			exit(1);
		}
		ASSERT(!CORBA::is_nil(ctx_v));

		os::sprintf(srv_name, "pnm_server.%d.%s",
		    orb_conf::node_number(), zonename);
		ctx_v->rebind(srv_name, pnm_server_v, env);

		if (env.exception() != NULL) {
			//
			// Rebind failed.
			//
			env.clear();

			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "Error while binding 'pnm_proxy_server' "
			    "in the name server. Exiting.");
			sc_syslog_msg_done(&handle);
			exit(1);
		}
	}
	sc_syslog_msg_done(&handle);
	thr_exit(0);
	// NOT REACHED
}

//
// Wait till the pnm proxy daemon has started. We do this by resolving
// the the pnm server object in the global name server. The daemon will be
// ready to accept client requests after it registers this object in the
// nameserver.
//
static void
wait_for_daemon()
{
	pnm_mod::pnm_server_var		pnm_server_v;
	CORBA::Object_var		obj_v;
	Environment			env;
	CORBA::Exception		*exp;
	naming::naming_context_var	ctx_v	= ns::root_nameserver();
	char				srv_name[100];
	sc_syslog_msg_handle_t		handle;
	char				zonename[ZONENAME_MAX];
	zoneid_t			zoneid = getzoneid();

	(void) sc_syslog_msg_initialize(&handle, PNM_PROXY_SERVER, "");
	if (zoneid == 0) {
		//
		// If RU is in progress, we consider that the proxy service
		// is online. The proxy service daemon will be active
		// only after it gets the upgrade callback from the version
		// manager.
		//
		version_manager::vm_admin_var   vm_v = vm_util::get_vm();
		if (CORBA::is_nil(vm_v)) {
			//
			// SCMSGS
			// @explanation
			// PNM proxy got a nil reference to the Version manager
			// and hence failed to retrieve the version.
			// @user_action
			// Contact your authorized Sun service provider to
			// determine whether a workaround or patch is available.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "PNM proxy : Unable to get a reference to the"
			    " version manager");
			sc_syslog_msg_done(&handle);
			exit(1);
		}

		Environment	e;
		version_manager::vp_version_t   rvers;
		vm_v->get_running_version((char *)"naming", rvers, e);

		if (e.exception()) {
			//
			// SCMSGS
			// @explanation
			// There was a failure to retrieve the name server
			// version from the version manager.
			// @user_action
			// Contact your authorized Sun service provider to
			// determine whether a workaround or patch is available.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "Unable to retrieve name server "
			    "version from version manager");
			sc_syslog_msg_done(&handle);
			exit(1);
		}
		if (rvers.major_num < 2) {
			// RU is in progress.
			return;
		}
	}

	//
	// The parent process waits for the child to register an
	// IDL object with the name server and then exits so
	// the shell will think the command is finished.
	//

	//
	// Get the zoneid and zonename of the zone where we are executing.
	//
	if (getzonenamebyid(zoneid, zonename, sizeof (zonename)) < 0) {
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Unable to get zonename for zone id %d, errno = %d. "
		    "Exiting\n.", zoneid, errno);
		sc_syslog_msg_done(&handle);
		exit(1);
	}
	//
	// Wait till the server object is registered with the name server.
	//
	os::sprintf(srv_name, "pnm_server.%d.%s", orb_conf::node_number(),
	    zonename);
	for (;;) {

		obj_v = ctx_v->resolve(srv_name, env);

		if ((exp = env.exception()) != NULL) {
			if (naming::not_found::_exnarrow(exp) == NULL) {
				sc_syslog_msg_done(&handle);
				exit(1);
			}
			// Wait and then try again.
			env.clear();
		} else {
			//
			// Test to see that the object we got from the
			// name server is active. If not, wait for
			// the child process to register the new
			// object.
			//
			pnm_server_v =
			    pnm_mod::pnm_server::_narrow(obj_v);

			ASSERT(!CORBA::is_nil(pnm_server_v));

			bool alive = pnm_server_v->is_alive(env);
			if (env.exception() == NULL && alive) {
				break;
			}
			env.clear();
		}
		//
		// Wait for 100 milliseconds.
		//
		os::usecsleep(MILLISEC*100);
	}
	sc_syslog_msg_done(&handle);
}


//
// Create the daemon process and wait for it to be ready to accept client
// requests in the parent. The parent then exists which indicates that the
// proxy pnm service is up.
//
static void
daemonize()
{
	pid_t			proc_pid;
	sc_syslog_msg_handle_t	handle;

	(void) sc_syslog_msg_initialize(&handle, PNM_PROXY_SERVER, "");

	//
	// Initialize ORB.
	//
	if (clconf_lib_init() != 0) {
		//
		// SCMSGS
		// @explanation
		// The pnm proxy program was unable to initialize
		// its interface to the low-level cluster machinery.
		// @user_action
		// Make sure the nodes are booted in cluster mode. If so,
		// contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			"Failed to initialize the ORB. Exiting.");
		sc_syslog_msg_done(&handle);
		exit(1);
	}

	pnm_mod::pnm_server_var			pnm_server_v;
	CORBA::Object_var			obj_v;
	Environment				env;
	CORBA::Exception			*exp;
	char					srv_name[100];
	char					zonename[ZONENAME_MAX];
	zoneid_t				zoneid = getzoneid();

	naming::naming_context_var ctx_v	= ns::root_nameserver();

	//
	// Get the zoneid and zonename of the zone where we are executing.
	//
	if (getzonenamebyid(zoneid, zonename, sizeof (zonename)) < 0) {
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Unable to get zonename for zone id %d, errno = %d. "
		    "Exiting\n.", zoneid, errno);
		sc_syslog_msg_done(&handle);
		exit(1);
	}

	//
	// The child process checks to see if there is already a process
	// running and doesn't start another.
	//
	os::sprintf(srv_name, "pnm_server.%d.%s", orb_conf::node_number(),
	    zonename);
	obj_v = ctx_v->resolve(srv_name, env);

	if ((exp = env.exception()) != NULL) {
		if (naming::not_found::_exnarrow(exp) == NULL) {
			//
			// SCMSGS
			// @explanation
			// The pnm proxy program could not resolve the
			// pnm proxy server object from the local
			// nameserver.
			// @user_action
			// Contact your authorized Sun service provider to
			// determine whether a workaround or patch is
			// available.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "Could not resolve 'pnm_proxy_server' in "
			    "the name server.  Exiting.");
			sc_syslog_msg_done(&handle);
			exit(1);
		}
		env.clear();
	} else {
		//
		// If the object is found and is active, flag an error
		// and exit.
		//
		pnm_server_v =
		    pnm_mod::pnm_server::_narrow(obj_v);
		ASSERT(!CORBA::is_nil(pnm_server_v));
		bool alive = pnm_server_v->is_alive(env);
		if (env.exception() == NULL && alive) {
			//
			// SCMSGS
			// @explanation
			// The pnm server program found another active
			// pnm program already in execution.
			// @user_action
			// This is an information message. No user action
			// is required.
			//
			(void) sc_syslog_msg_log(handle, LOG_INFO, MESSAGE,
			    "Found another active instance of pnm "
			    "proxy server.  Exiting pnm_serverd.");
			sc_syslog_msg_done(&handle);
			exit(0);
		}
	}

	//
	// We need a way to declare the pnm proxy service as online,
	// Since pnm proxy service is a daemon that does not go away,
	// we fork off a new process that does the actual work of the
	// pnm proxy service.The parent waits till the initialization is
	// complete and the service is ready to accept client requests and
	// then goes away. Based on the return status of the parent,
	// SMF changes the state of the service.
	//
	proc_pid = fork1();

	if (proc_pid == (pid_t)-1) {
		//
		// SCMSGS
		// @explanation
		// The pnm proxy program has encountered failure of
		// fork1(2) system call. The error message indicates the error
		// number for the failure.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "PNM proxy : fork1 failed, errno = %d. Exiting.", errno);
		sc_syslog_msg_done(&handle);
		exit(1);
	} else if (proc_pid == 0) {
		sc_syslog_msg_done(&handle);
		daemon_process();
		/* NOTREACHED */
	} else {
		//
		// Parent.
		// We exit after making sure the daemon is up and ready
		// to process its client requests.
		//
		wait_for_daemon();
	}
	sc_syslog_msg_done(&handle);
}

//
// Main routine. Start the daemon service here.
//
int
main()
{
	//
	// Create the daemon service.
	//
	daemonize();
	return (0);
}
