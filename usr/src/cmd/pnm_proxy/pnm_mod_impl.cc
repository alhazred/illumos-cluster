//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)pnm_mod_impl.cc	1.12	09/02/13 SMI"

//
// Proxy PNM server object Implementation.
// The proxy PNM server handles PNM requests from a zone cluster and also
// in the exclusive-ip zone. For the requests from the zone cluster,
// it validates the request and communicates with the PNM daemon to fetch
// the required data. The data is filtered and only the information
// the zone cluster is authorized to access is returned back.
//
#include <libintl.h>
#include <locale.h>
#include <pnm_mod_impl.h>
#include <orb/infrastructure/orb.h>
#include <orb/infrastructure/orb_conf.h>
#include <sys/os.h>
#include <nslib/ns.h>
#include <orb/object/adapter.h>
#include <sys/vc_int.h>
#include <rgm/pnm.h>
#include <sys/sc_syslog_msg.h>
#include <zone.h>

#define	CLZONE_CONFIG_TABLE "clzone_config"
#define	MAX_ADPS 64

// Contructor
pnm_server_impl::pnm_server_impl()
{
	//
	// Establish a connection with PNM server on the local node.
	// It is ok to ignore any failures at this time as we retry
	// this later on for a client invocation.
	//
	(void) pnm_init("localhost");
	if (getzoneid() == 0) {
		//
		// The proxy PNM server handles PNM clients running inside
		// zone clusters.
		//
		is_global = true;
	} else {
		//
		// The proxy PNM server runs in an exclusive-ip zone
		// and handles clients from the global zone.
		//
		is_global = false;
	}
}


//
// Unreference notification.
//
void
pnm_server_impl::_unreferenced(unref_t cookie)
{
	(void) _last_unref(cookie);
	exit(0);
}

//
// Returns a list of all the adapters configured in a zone cluster.
//
char **
pnm_server_impl::get_adapters(const char *zcnamep) const
{
	Environment			env;
	CORBA::Exception		*exp;
	ccr::directory_var		ccr_dir_v;
	ccr::readonly_table_var		tab_v;
	uint_t				indx;
	char				**adp_listp;
	nodeid_t			nid = 0;
	char				nodenamep[CL_MAX_LEN+1];

	os::sc_syslog_msg logger(SC_SYSLOG_PNMPROXY_TAG, "", NULL);

	adp_listp = (char **)new char[MAX_ADPS * sizeof (char *)];
	if (adp_listp == NULL) {
		//
		// SCMSGS
		// @explanation
		// Could not allocate memory, PNM proxy program will exit.
		// @user_action
		// The system is running low on memory. Free up resources so
		// that sufficient memory is available.
		//
		(void) logger.log(LOG_ERR, MESSAGE,
		    "Could not allocate memory, size = %d",
		    MAX_ADPS * sizeof (char *));
		exit(errno);
	}
	for (indx = 0; indx < MAX_ADPS; ++indx) {
		adp_listp[indx] = NULL;
	}
	naming::naming_context_var ctx_v = ns::local_nameserver();

	if (CORBA::is_nil(ctx_v)) {
		//
		// SCMSGS
		// @explanation
		// The pnm proxy daemon could not get a reference to the
		// local nameserver.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) logger.log(LOG_ERR, MESSAGE,
		    "Failed to retrieve Nameserver reference");
		abort();
	}
	CORBA::Object_var obj_v = ctx_v->resolve("ccr_directory", env);

	if (env.exception()) {
		//
		// SCMSGS
		// @explanation
		// The pnm proxy daemon could not get a reference to the
		// CCR.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) logger.log(LOG_ERR, MESSAGE,
		    "Failed to resolve CCR directory from the Nameserver");
		abort();
	} else {
		ccr_dir_v = ccr::directory::_narrow(obj_v);
		ASSERT(!CORBA::is_nil(ccr_dir_v));
	}
	tab_v = ccr_dir_v->lookup_zc(zcnamep, CLZONE_CONFIG_TABLE, env);

	if ((exp = env.exception()) != NULL) {
		//
		// SCMSGS
		// @explanation
		// The CCR returned an exception when trying to lookup the
		// zone cluster configuration table.
		// @user_action
		// Verify if the zone cluster CCR is corrupted. If not,
		// contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) logger.log(LOG_ERR, MESSAGE,
		    "Failed to lookup the zone cluster configuration in the"
		    "CCR");
		abort();
	}
	//
	// Lookup the physical adapters allocated to the zone cluster
	// zone on the local node. For this we have to query the
	// "clzone_config" CCR table of the zone cluster.
	// This table does not have the zoneclusters zone nodes listed
	// according to their nodeid's. So what we do here is
	// 1. Get the physical node hostname on the local node.
	// 2. Find the corresponding key entry zone.node.<x>.name
	//   that matches the physical node hostname as the data field
	//   for that key.
	// 3. Find all entries that match the key prefix
	//    zone.node.<x>.network.<y>.physical and store the corresponding
	//    data.
	//
	char *key = new char[50];
	char *data = NULL;
	if (key == NULL) {
		(void) logger.log(LOG_ERR, MESSAGE,
		    "Could not allocate memory, size = %d", 50);
		exit(errno);
	}
	clconf_get_nodename(orb_conf::local_nodeid(), nodenamep);
	for (indx = 1; indx < NODEID_MAX; ++indx) {
		os::sprintf(key, "%s.%s.%d.%s", "zone", "node",
		    indx, "name");
		data = tab_v->query_element(key, env);
		if ((exp = env.exception()) != NULL) {
			if (ccr::no_such_key::_exnarrow(exp)) {
				// No more entries to be found.
				env.clear();
				break;
			}
		}
		ASSERT(data != NULL);
		if (os::strcmp(data, nodenamep) == 0) {
			// Match.
			nid = indx;
			delete [] data;
			break;
		} else {
			delete [] data;
		}
	}
	if (nid == 0) {
		//
		// SCMSGS
		// @explanation
		// The PNM proxy subsystem is called on a physical node for
		// a zone cluster that is not configured to run on that node.
		// @user_action
		// Contact your authorized Sun service provider to
		// determine whether a workaround or patch is
		// available.
		//
		(void) logger.log(LOG_ERR, MESSAGE, "Zone cluster %s is not "
		    "configured to run on node %s", zcnamep, nodenamep);
		return (adp_listp);
	}
	for (indx = 1; indx < MAX_ADPS; ++indx) {
		os::sprintf(key, "%s.%s.%d.%s.%d.%s", "zone", "node",
		    nid, "network", indx, "physical");
		data = tab_v->query_element(key, env);
		if ((exp = env.exception()) != NULL) {
			if (ccr::no_such_key::_exnarrow(exp)) {
				// No more adapters to be found.
				env.clear();
				delete [] key;
				break;
			}
		}
		ASSERT(data != NULL);
		adp_listp[indx - 1] = data;
	}
	return (adp_listp);
}

//
// Verify if the group is authorized to be used within the zone
// cluster.
// Return value:
// True if the IPMP group can be used within the zone cluster.
// False otherwise.
//
bool
pnm_server_impl::verify_group(const char *zonenamep, const char *groupp,
    Environment &env)
{
	bool	found = false;
	uint_t 	indx;
	int	retval;
	pnm_mod::AdpinfoSeq   *adp_listp;

	os::sc_syslog_msg logger(SC_SYSLOG_PNMPROXY_TAG, "", NULL);

	ASSERT(groupp != NULL);
	ASSERT(is_global);

	retval = pnm_adp_info_list_zc(zonenamep, adp_listp, false, env);
	if (retval != 0) {
		//
		// SCMSGS
		// @explanation
		// There was a failure to get the list of adapters
		// configured to be used for the zone cluster.
		// @user_action
		// Contact your authorized Sun service provider to
		// determine whether a workaround or patch is
		// available.
		//
		(void) logger.log(LOG_ERR, MESSAGE,
		    "Failed to get the list of adapters for the zone cluster"
		    " %s, error = %d", zonenamep, retval);
		return (false);
	}
	pnm_mod::AdpinfoSeq &adp_listref = *adp_listp;
	for (indx = 0; indx < adp_listref.length(); ++indx) {
		if (adp_listref[indx].adp_group != NULL) {
			if (os::strcmp(adp_listref[indx].adp_group, groupp)
			    == 0) {
				found = true;
				break;
			}
		}
	}
	delete adp_listp;
	return (found);
}

//
// Verify if the adapter is authorized to be used within the zone
// cluster.
// Return value:
// True if the adapter can be used within the zone cluster.
// False otherwise.
//
bool
pnm_server_impl::verify_adapter(const char *zcnamep,
    const char *adp_namep, Environment &env)
{
	uint_t 	indx;
	bool	found = false;
	int	retval;
	pnm_mod::AdpinfoSeq  *adp_listp;

	ASSERT(adp_namep != NULL);
	ASSERT(is_global);

	os::sc_syslog_msg logger(SC_SYSLOG_PNMPROXY_TAG, "", NULL);
	retval = pnm_adp_info_list_zc(zcnamep, adp_listp, false, env);
	if (retval != 0) {
		(void) logger.log(LOG_ERR, MESSAGE,
		    "Failed to get the list of adapters for the zone cluster"
		    " %s, error = %d", zcnamep, retval);
		return (false);
	}
	pnm_mod::AdpinfoSeq &adp_listref = *adp_listp;
	for (indx = 0; indx < adp_listref.length(); ++indx) {
		if (os::strcmp(adp_listref[indx].adp_name, adp_namep) == 0) {
			found = true;
			break;
		}
	}
	delete adp_listp;
	return (found);
}

//
// Interface definitions.
// Handle the PNM command specified by a client in the zone cluster node.
// Also handle the PNM command specified by a client in the global zone
// for the exclusive-ip zone.
//

//
// This routine returns true if the pnm_server interface is alive and
// working. It can be used to tell if the object reference obtained from
// the name server is valid or not.
//
bool
pnm_server_impl::is_alive(Environment &)
{
	return (true);
}

//
// Return the list of IPMP groups configured for the zone cluster node or
// exclusive-ip zone. The zone cluster is configured with a list of adapters
// that it can use. If the adapter belongs to an IPMP group, then access to
// that IPMP group is allowed from the zone cluster. Only such IPMP groups
// are returned back to the caller.
// Return Value: 0 on success. A pnm error code is returned on failure.
//
int32_t
pnm_server_impl::pnm_group_list(CORBA::String_out listp, Environment &e)
{
	pnm_group_list_t	grp_listp = NULL;
	int32_t			retval = 0;
	char			*zonenamep;
	uint32_t		clid = e.get_cluster_id();

	os::sc_syslog_msg logger(SC_SYSLOG_PNMPROXY_TAG, "", NULL);

	if (!is_global) {
		if (clid >= MIN_CLUSTER_ID) {
			//
			// Client from a Zone cluster and it is trying
			// to get information of an exclusive-ip zone.
			//
			return (PNM_EPERM);
		}
		retval = ::pnm_group_list(&grp_listp);
		if (grp_listp != NULL) {
			listp = (char *)grp_listp;
		} else {
			listp = NULL;
		}
		return (retval);
	}
	if (clid == 0) {
		//
		// SCMSGS
		// @explanation
		// The PNM proxy subsystem is being used for a global
		// zone PNM client.
		// @user_action
		// Contact your authorized Sun service provider to
		// determine whether a workaround or patch is
		// available.
		//
		(void) logger.log(LOG_ERR, MESSAGE, "PNM proxy invoked for "
		    "a global zone PNM client.");
		return (PNM_EPROG);
	} else {
		retval = clconf_get_cluster_name(clid, &zonenamep);
		if (retval != 0) {
			//
			// SCMSGS
			// @explanation
			// There was a failure to map a zone cluster ID
			// to a zone cluster name. This can happen if
			// the zone cluster with the corresponding id has been
			// removed from the system.
			// @user_action
			// Verify if there was any administration related to
			// a zone cluster that was just removed. If not,
			// contact your authorized Sun service provider to
			// determine whether a workaround or patch is
			// available.
			//
			(void) logger.log(LOG_ERR, MESSAGE, "Failed to get "
			    "zone cluster name from zone cluster id %d,"
			    " error = %d", clid, retval);
			return (PNM_EPROG);
		}
		retval = pnm_group_list_zc(zonenamep, listp, e);
		delete [] zonenamep;
	}
	return (retval);
}

//
// This interface is called by clients in the global zone who would
// need to fetch public network information for a zone cluster.
// This interface can also by invoked by clients within a zone cluster.
//
int32_t
pnm_server_impl::pnm_group_list_zc(const char *zonenamep,
    CORBA::String_out listp, Environment &e) {

	pnm_group_list_t	grp_listp = NULL;
	int32_t			retval = 0;
	char			**adp_listp;
	char			*group_namep;
	int			indx;
	uint32_t		clid = e.get_cluster_id();

	os::sc_syslog_msg logger(SC_SYSLOG_PNMPROXY_TAG, "", NULL);

	if (!is_global) {
		//
		// SCMSGS
		// @explanation
		// The PNM proxy subsystem interface for a zone cluster is being
		// used for a exclusive-ip zone.
		// @user_action
		// Contact your authorized Sun service provider to
		// determine whether a workaround or patch is
		// available.
		//
		(void) logger.log(LOG_ERR, MESSAGE,
		    "Zone cluster PNM interface invoked instead of PNM "
		    "interface for exclusive-ip zones");
		return (PNM_EPERM);
	}
	if ((clid != 0) && (clid < MIN_CLUSTER_ID)) {
		//
		// SCMSGS
		// @explanation
		// The PNM proxy subsystem interface for a zone cluster is being
		// used from a client in non-global zones.
		// @user_action
		// Contact your authorized Sun service provider to
		// determine whether a workaround or patch is
		// available.
		//
		(void) logger.log(LOG_ERR, MESSAGE,
		    "Zone cluster PNM interface invoked from a non-global "
		    "zone that does not belong to a zone cluster");
		return (PNM_EPERM);
	}
	//
	// Get the list of all the adapters that the zone cluster is
	// allowed to use.
	//
	adp_listp = get_adapters(zonenamep);

	for (indx = 0; indx < MAX_ADPS; ++indx) {
		if (adp_listp[indx] == NULL) {
			break;
		}
		retval = ::pnm_map_adapter(adp_listp[indx], &group_namep);
		if (retval != 0) {
			//
			// SCMSGS
			// @explanation
			// There was an error while attempting to map
			// an adapter to an IPMP group.
			// @user_action
			// This is an informational message. No action
			// is required.
			//
			(void) logger.log(LOG_NOTICE, MESSAGE,
			    "Failed to map adapter %s to an IPMP group,"
			    " error = %d", adp_listp[indx], retval);

		} else if (group_namep != NULL) {
			if (grp_listp == NULL) {
				grp_listp = group_namep;
			} else {
				char *tmp = new char[os::strlen(grp_listp) +
				    os::strlen(group_namep) + 2];
				if (tmp == NULL) {
					(void) logger.log(LOG_ERR, MESSAGE,
					    "Could not allocate memory, size"
					    " = %d", os::strlen(grp_listp) +
					    os::strlen(group_namep) + 2);
					exit(errno);
				}
				os::sprintf(tmp, "%s:%s", grp_listp,
				    group_namep);
				delete [] grp_listp;
				grp_listp = tmp;
				delete [] group_namep;
				group_namep = NULL;
			}
		}
		delete [] adp_listp[indx];
	}
	if (grp_listp != NULL) {
		listp = (char *)grp_listp;
	} else {
		listp = NULL;
	}
	delete [] adp_listp;
	return (0);
}

//
// Return the IPMP group status, given the group name. If this is for
// a zone cluster, the given group should be authorized to be used by the
// zone cluster. If not, an error value, PNM_EPERM is returned.
// Return value:
// 0 on success
// A PNM error code is returned on failure.
//
int32_t
pnm_server_impl::pnm_group_status(const char *grp_namep,
    pnm_mod::pnm_status_out statusp, Environment &env)
{
	pnm_status_t	grp_status;
	int32_t		retval;
	uint32_t	clid = env.get_cluster_id();
	char		*zonenamep;

	os::sc_syslog_msg logger(SC_SYSLOG_PNMPROXY_TAG, "", NULL);

	if (!is_global) {
		statusp = new pnm_mod::pnm_status();
		if (!statusp) {
			(void) logger.log(LOG_ERR, MESSAGE,
			    "Could not allocate memory, size = %d",
			    sizeof (pnm_mod::pnm_status));
			exit(errno);
		}
		if (clid >= MIN_CLUSTER_ID) {
			//
			// client is from a zone cluster and it is trying
			// to get information of an exclusive-ip zone.
			//
			return (PNM_EPERM);
		}
		pnm_mod::pnm_status &status_ref = *statusp;
		status_ref.backups = (char *)NULL;
		//
		// Invoke the libpnm interface to get the pnm_status data.
		//
		retval = ::pnm_group_status((char *)grp_namep, &grp_status);
		status_ref.status = grp_status.status;

		if (retval != 0) {
			return (retval);
		}
		if (grp_status.backups != NULL) {
			status_ref.backups = grp_status.backups;
		}
		return (0);
	}
	// The PNM proxy daemon is running in the global zone.
	if (clid == 0) {
		(void) logger.log(LOG_ERR, MESSAGE, "PNM proxy invoked for "
		    "a global zone PNM client.");
		return (PNM_EPROG);
	} else {
		retval = clconf_get_cluster_name(clid, &zonenamep);
		if (retval != 0) {
			// Error.
			(void) logger.log(LOG_ERR, MESSAGE, "Failed to get "
			    "zone cluster name from zone cluster id %d,"
			    " error = %d", clid, retval);
			return (PNM_EPROG);
		}
		retval = pnm_group_status_zc(zonenamep, grp_namep, statusp,
		    env);
		delete [] zonenamep;
	}
	return (retval);
}

//
// This interface is called by clients in the global zone who would
// need to fetch public network information for a zone cluster.
// This interface can also by invoked by clients within a zone cluster
//
int32_t
pnm_server_impl::pnm_group_status_zc(const char *zonenamep,
    const char *grp_namep, pnm_mod::pnm_status_out statusp,
    Environment &env)
{
	pnm_status_t	grp_status;
	int32_t		retval;
	uint32_t	clid = env.get_cluster_id();

	os::sc_syslog_msg logger(SC_SYSLOG_PNMPROXY_TAG, "", NULL);

	statusp = new pnm_mod::pnm_status();
	if (!statusp) {
		(void) logger.log(LOG_ERR, MESSAGE,
		    "Could not allocate memory, size = %d",
		    sizeof (pnm_mod::pnm_status));
		exit(errno);
	}
	if (!is_global) {
		(void) logger.log(LOG_ERR, MESSAGE,
		    "Zone cluster PNM interface invoked instead of PNM "
		    "interface for exclusive-ip zones");
		return (PNM_EPERM);
	}
	if ((clid != 0) && (clid < MIN_CLUSTER_ID)) {
		(void) logger.log(LOG_ERR, MESSAGE,
		    "Zone cluster PNM interface invoked from a non-global "
		    "zone that does not belong to a zone cluster");
		return (PNM_EPERM);
	}
	//
	//
	// Verify that the group name mentioned here is authorized to be
	// used by the zone cluster.
	//
	if (!verify_group(zonenamep, (char *)grp_namep, env)) {
		//
		// SCMSGS
		// @explanation
		// The zone cluster PNM client is seeking information
		// about an IPMP group that is not configured
		// to be used for the zone cluster.
		// @user_action
		// This message is informational. No user action
		// is required.
		//
		(void) logger.log(LOG_NOTICE, MESSAGE,
		    "Zone cluster %s is not authorized to access"
		    " IPMP group %s", zonenamep, grp_namep);
		return (PNM_EPERM);
	}
	pnm_mod::pnm_status &status_ref = *statusp;
	status_ref.backups = (char *)NULL;

	retval = ::pnm_group_status((char *)grp_namep, &grp_status);
	status_ref.status = grp_status.status;

	if (retval != 0) {
		return (retval);
	}
	if (grp_status.backups != NULL) {
		status_ref.backups = grp_status.backups;
	}
	return (0);
}

//
// Return the list of adapters and information regarding each adapter.
// Only the adapters that are authroized to be used within a zone cluster
// are returned back to the caller.
// Return value:
// 0 on success. A non-zero PNM error code is returned on failure.
//
int32_t
pnm_server_impl::pnm_adp_info_list(
    pnm_mod::AdpinfoSeq_out adp_seqp, bool alladdrs, Environment &env)
{
	int32_t			retval;
	pnm_adapterinfo_t	*adplistp;
	pnm_adapterinfo_t	*tmpadp;
	uint_t			indx;
	uint_t			listcnt = 0;
	char			*zonenamep;
	uint32_t		clid = env.get_cluster_id();

	os::sc_syslog_msg logger(SC_SYSLOG_PNMPROXY_TAG, "", NULL);

	if (is_global) {
		if (clid == 0) {
			//
			// SCMSGS
			// @explanation
			// PNM proxy is being used to get global zone
			// information.
			// @user_action
			// Contact your authorized Sun service provider to
			// determine whether a workaround or patch is
			// available.
			//
			(void) logger.log(LOG_ERR, MESSAGE, "PNM proxy invoked"
			    " for global zone public network information");
			adp_seqp =  new pnm_mod::AdpinfoSeq();
			if (!adp_seqp) {
				(void) logger.log(LOG_ERR, MESSAGE,
				    "Could not allocate memory, size = %d",
				    sizeof (pnm_mod::AdpinfoSeq));
				exit(errno);
			}
			return (PNM_EPROG);
		} else {
			retval = clconf_get_cluster_name(clid, &zonenamep);
			if (retval != 0) {
				// Error.
				adp_seqp =  new pnm_mod::AdpinfoSeq();
				if (!adp_seqp) {
					(void) logger.log(LOG_ERR, MESSAGE,
					    "Could not allocate memory, size "
					    "= %d",
					    sizeof (pnm_mod::AdpinfoSeq));
					exit(errno);
				}
				(void) logger.log(LOG_ERR, MESSAGE,
				    "Failed to get zone cluster name from zone"
				    " cluster id %d, error = %d", clid,
				    retval);
				return (PNM_EPROG);
			}
		}
		retval = pnm_adp_info_list_zc(zonenamep, adp_seqp, alladdrs,
		    env);
		delete [] zonenamep;
		return (retval);
	}
	// Proxy is running in an exclusive-ip zone.
	if (clid >= MIN_CLUSTER_ID) {
		//
		// Client is from a zone cluster and it is trying to get
		// information of an exclusive-ip zone.
		//
		adp_seqp =  new pnm_mod::AdpinfoSeq();
		if (!adp_seqp) {
			(void) logger.log(LOG_ERR, MESSAGE, "Could not allocate"
			    " memory, size = %d", sizeof (pnm_mod::AdpinfoSeq));
			exit(errno);
		}
		return (PNM_EPERM);
	}
	retval = pnm_adapterinfo_list(&adplistp, (boolean_t)alladdrs);
	if (retval != 0) {
		// Return failure.
		adp_seqp =  new pnm_mod::AdpinfoSeq();
		if (!adp_seqp) {
			(void) logger.log(LOG_ERR, MESSAGE,
			    "Could not allocate memory, size = %d",
			    sizeof (pnm_mod::AdpinfoSeq));
			exit(errno);
		}
		return (retval);
	}
	tmpadp = adplistp;
	while (tmpadp) {
		listcnt++;
		tmpadp = tmpadp->next;
	}
	if (listcnt > 0) {
		adp_seqp =  new pnm_mod::AdpinfoSeq(listcnt, listcnt);
		if (!adp_seqp) {
			(void) logger.log(LOG_ERR, MESSAGE,
			    "Could not allocate memory, size = %d for %d "
			    "elements", sizeof (pnm_mod::AdpinfoSeq), listcnt);
			exit(errno);
		}
	} else {
		adp_seqp =  new pnm_mod::AdpinfoSeq();
		if (!adp_seqp) {
			(void) logger.log(LOG_ERR, MESSAGE,
			    "Could not allocate memory, size = %d",
			    sizeof (pnm_mod::AdpinfoSeq));
			exit(errno);
		}
	}
	pnm_mod::AdpinfoSeq	&adp_seq_ref = *adp_seqp;
	pnm_mod::pnm_adapterinfo	*adpinfo = NULL;
	listcnt = 0;
	tmpadp = adplistp;
	//
	// Walk through the adapter list returned by the libpnm interface
	// and generate the adapter list sequence to be returned by this
	// CORBA invocation.
	//
	while (tmpadp) {
		//  Add to the list of adapters.

		adpinfo = new pnm_mod::pnm_adapterinfo();

		if (!adpinfo) {
			(void) logger.log(LOG_ERR, MESSAGE,
			    "Could not allocate memory, size = %d",
			    sizeof (pnm_mod::pnm_adapterinfo));
			exit(errno);
		}

		adpinfo->adp_name = os::strdup(tmpadp->adp_name);
		if (adpinfo->adp_name == NULL) {
			(void) logger.log(LOG_ERR, MESSAGE,
			    "Could not allocate memory, size = %d",
			    os::strlen(tmpadp->adp_name));
			exit(errno);
		}

		if (tmpadp->adp_group != NULL) {
			adpinfo->adp_group = os::strdup(tmpadp->adp_group);
			if (adpinfo->adp_group == NULL) {
				(void) logger.log(LOG_ERR, MESSAGE,
				    "Could not allocate memory, size = %d",
				    os::strlen(tmpadp->adp_group));
				exit(errno);
			}
		} else {
			adpinfo->adp_group = (char *)NULL;
		}
		adpinfo->adp_flags = tmpadp->adp_flags;

		if (tmpadp->adp_addrs.num_addrs > 0) {
			adpinfo->adp_ipmask.length((uint_t)
			    tmpadp->adp_addrs.num_addrs);
			adpinfo->adp_ipnet.length((uint_t)
			    tmpadp->adp_addrs.num_addrs);
			adpinfo->adp_ipaddr.length((uint_t)
			    tmpadp->adp_addrs.num_addrs);
		}
		for (indx = 0; indx < tmpadp->adp_addrs.num_addrs; ++indx) {
			bcopy(&(tmpadp->adp_addrs.adp_ipmask[indx]),
			    &adpinfo->adp_ipmask[indx],
			    sizeof (struct in6_addr));
			bcopy(&(tmpadp->adp_addrs.adp_ipnet[indx]),
			    &adpinfo->adp_ipnet[indx],
			    sizeof (struct in6_addr));
			bcopy(&tmpadp->adp_addrs.adp_ipaddr[indx],
			    &adpinfo->adp_ipaddr[indx],
			    sizeof (struct in6_addr));
		}
		adp_seq_ref[listcnt++] = *adpinfo;
		tmpadp = tmpadp->next;
		delete adpinfo;
	}
	// Free the pnm adapter list structure.
	if (adplistp) {
		pnm_adapterinfo_free(adplistp);
	}
	return (0);
}

//
// This interface is called by clients in the global zone who would
// need to fetch public network information for a zone cluster.
// This interface can also by invoked by clients within a zone cluster
//
int32_t
pnm_server_impl::pnm_adp_info_list_zc(const char *zonenamep,
    pnm_mod::AdpinfoSeq_out adp_seqp, bool alladdrs, Environment &env)
{
	int32_t			retval;
	pnm_adapterinfo_t	*adplistp;
	pnm_adapterinfo_t	*tmpadp;
	uint_t			indx;
	char			**adp_listp = NULL;
	uint_t			listcnt = 0;
	bool			found;
	char			*group_namep;
	uint32_t		clid = env.get_cluster_id();

	os::sc_syslog_msg logger(SC_SYSLOG_PNMPROXY_TAG, "", NULL);

	if (!is_global) {
		(void) logger.log(LOG_ERR, MESSAGE, "Zone cluster PNM "
		    "interface invoked instead of PNM interface for "
		    "exclusive-ip zones");
		return (PNM_EPERM);
	}
	if ((clid != 0) && (clid < MIN_CLUSTER_ID)) {
		(void) logger.log(LOG_ERR, MESSAGE, "Zone cluster PNM "
		    "interface invoked from a non-global zone that does "
		    "not belong to a zone cluster");
		return (PNM_EPERM);
	}
	retval = pnm_adapterinfo_list(&adplistp, (boolean_t)alladdrs);
	if (retval != 0) {
		// Return failure.
		adp_seqp =  new pnm_mod::AdpinfoSeq();
		if (!adp_seqp) {
			(void) logger.log(LOG_ERR, MESSAGE, "Could not "
			    "allocate memory, size = %d",
			    sizeof (pnm_mod::AdpinfoSeq));
			exit(errno);
		}
		return (retval);
	}
	tmpadp = adplistp;
	while (tmpadp) {
		listcnt++;
		tmpadp = tmpadp->next;
	}
	if (listcnt > 0) {
		adp_seqp =  new pnm_mod::AdpinfoSeq(listcnt, listcnt);
		if (!adp_seqp) {
			(void) logger.log(LOG_ERR, MESSAGE,
			    "Could not allocate memory, size = %d for %d "
			    "elements", sizeof (pnm_mod::AdpinfoSeq), listcnt);
			exit(errno);
		}
	} else {
		adp_seqp =  new pnm_mod::AdpinfoSeq();
		if (!adp_seqp) {
			(void) logger.log(LOG_ERR, MESSAGE,
			    "Could not allocate memory, size = %d",
			    sizeof (pnm_mod::AdpinfoSeq));
			exit(errno);
		}
	}
	pnm_mod::AdpinfoSeq	&adp_seq_ref = *adp_seqp;
	pnm_mod::pnm_adapterinfo	*adpinfo = NULL;
	listcnt = 0;
	tmpadp = adplistp;
	//
	// Walk through the adapter list returned by the libpnm interface
	// and generate the adapter list sequence to be returned by this
	// CORBA invocation.
	//
	adp_listp = get_adapters(zonenamep);
	while (tmpadp) {
		//
		// Walk through the adapter list and filter out the adapters
		// that are not authorized to be used within the zone cluster.
		// For ex-ip zones, we need to include all the adapters
		// in the adapter list.
		//
		found = false;
		for (indx = 0; indx < MAX_ADPS; ++indx) {
			if (adp_listp[indx] == NULL) {
				break;
			}
			if ((os::strcmp(adp_listp[indx], tmpadp->adp_name)
			    == 0)) {
				// Match.
				found = true;
				break;
			} else {
				//
				// If the adapter belongs to the same IPMP
				// group as one of the adapters in the
				// authorized list, we can include that here.
				//
				retval = ::pnm_map_adapter(adp_listp[indx],
				    &group_namep);
				if (retval != 0) {
					(void) logger.log(LOG_NOTICE, MESSAGE,
					    "Failed to map adapter %s to an "
					    "IPMP group, error = %d",
					    adp_listp[indx], retval);
					continue;
				}
				// Found match.
				if ((tmpadp->adp_group != NULL) &&
				    os::strcmp(group_namep, tmpadp->adp_group)
				    == 0) {
					found = true;
					delete [] group_namep;
					break;
				}
				delete [] group_namep;
			}
		}
		//
		// Zone cluster does not have access to this adapter.
		// skip this and try the next one. We skip this for
		// xip zones (is_global = false)
		//
		if (!found) {
			tmpadp = tmpadp->next;
			continue;
		}
		//
		// The adapter is authorized to be used within the zone
		// cluster. Add it to the list of adapters.
		//
		adpinfo = new pnm_mod::pnm_adapterinfo();

		if (!adpinfo) {
			(void) logger.log(LOG_ERR, MESSAGE,
			    "Could not allocate memory, size = %d",
			    sizeof (pnm_mod::pnm_adapterinfo));
			exit(errno);
		}

		adpinfo->adp_name = os::strdup(tmpadp->adp_name);
		if (adpinfo->adp_name == NULL) {
			(void) logger.log(LOG_ERR, MESSAGE, "Could not "
			    "allocate memory, size = %d",
			    os::strlen(tmpadp->adp_name));
			exit(errno);
		}

		if (tmpadp->adp_group != NULL) {
			adpinfo->adp_group = os::strdup(tmpadp->adp_group);
			if (adpinfo->adp_group == NULL) {
				(void) logger.log(LOG_ERR, MESSAGE,
				    "Could not allocate memory, size = %d",
				    os::strlen(tmpadp->adp_group));
				exit(errno);
			}
		} else {
			adpinfo->adp_group = (char *)NULL;
		}
		adpinfo->adp_flags = tmpadp->adp_flags;

		if (tmpadp->adp_addrs.num_addrs > 0) {
			adpinfo->adp_ipmask.length((uint_t)
			    tmpadp->adp_addrs.num_addrs);
			adpinfo->adp_ipnet.length((uint_t)
			    tmpadp->adp_addrs.num_addrs);
			adpinfo->adp_ipaddr.length((uint_t)
			    tmpadp->adp_addrs.num_addrs);
		}
		for (indx = 0; indx < tmpadp->adp_addrs.num_addrs; ++indx) {
			bcopy(&(tmpadp->adp_addrs.adp_ipmask[indx]),
			    &adpinfo->adp_ipmask[indx],
			    sizeof (struct in6_addr));
			bcopy(&(tmpadp->adp_addrs.adp_ipnet[indx]),
			    &adpinfo->adp_ipnet[indx],
			    sizeof (struct in6_addr));
			bcopy(&tmpadp->adp_addrs.adp_ipaddr[indx],
			    &adpinfo->adp_ipaddr[indx],
			    sizeof (struct in6_addr));
		}
		adp_seq_ref[listcnt++] = *adpinfo;
		tmpadp = tmpadp->next;
		delete adpinfo;
	}
	// Resize the Adapter list sequence
	adp_seq_ref.length(listcnt);
	// Free the pnm adapter list structure.
	if (adplistp) {
		pnm_adapterinfo_free(adplistp);
	}
	for (indx = 0; indx < MAX_ADPS; ++indx) {
		if (adp_listp[indx] == NULL) {
			break;
		}
		delete [] adp_listp[indx];
	}
	delete [] adp_listp;
	return (0);
}

//
// Return the IPMP group of a given adapter. If being invoked for a zone
// cluster, the adapter should be allowed to be used in a zone cluster.
// If not an error PNM_EPERM is returned.
// Return value: 0 on success. On failure, a PNM error code
// is returned back to the caller.
//
int32_t
pnm_server_impl::pnm_map_adapter(const char *adp_namep,
    CORBA::String_out grp_namep, Environment &env)
{
	pnm_group_t	group;
	int		retval;
	char 		*zonenamep;
	uint32_t	clid = env.get_cluster_id();

	os::sc_syslog_msg logger(SC_SYSLOG_PNMPROXY_TAG, "", NULL);

	if (!is_global) {
		if (clid >= MIN_CLUSTER_ID) {
			//
			// Client is from a zone cluster and it is trying
			// to get information of an exclusive-ip zone.
			//
			return (PNM_EPERM);
		}
		retval = ::pnm_map_adapter((char *)adp_namep, &group);
		if (retval != 0) {
			grp_namep = (char *)NULL;
		} else {
			grp_namep = (char *)group;
		}
		return (retval);
	}
	if (clid == 0) {
		// Error.
		(void) logger.log(LOG_ERR, MESSAGE, "PNM proxy invoked for "
		    "global zone public network information");
		return (PNM_EPROG);
	} else {
		retval = clconf_get_cluster_name(clid, &zonenamep);
		if (retval != 0) {
			// Error.
			(void) logger.log(LOG_ERR, MESSAGE, "Failed to get "
			    "zone cluster name from zone cluster id %d,"
			    " error = %d", clid, retval);
			return (PNM_EPROG);
		}
		retval = pnm_map_adapter_zc(zonenamep, adp_namep, grp_namep,
		    env);
		delete [] zonenamep;
	}
	return (retval);
}

//
// This interface is called by clients in the global zone who would
// need to fetch public network information for a zone cluster.
// This interface can also by invoked by clients within a zone cluster
//
int32_t
pnm_server_impl::pnm_map_adapter_zc(const char *zonenamep,
    const char *adp_namep, CORBA::String_out grp_namep, Environment &env)
{
	pnm_group_t	group;
	int		retval;
	uint32_t	clid = env.get_cluster_id();

	os::sc_syslog_msg logger(SC_SYSLOG_PNMPROXY_TAG, "", NULL);

	if (!is_global) {
		(void) logger.log(LOG_ERR, MESSAGE,
		    "Zone cluster PNM interface invoked instead of PNM "
		    "interface for exclusive-ip zones");
		return (PNM_EPERM);
	}
	if ((clid != 0) && (clid < MIN_CLUSTER_ID)) {
		(void) logger.log(LOG_ERR, MESSAGE,
		    "Zone cluster PNM interface invoked from a non-global "
		    "zone that does not belong to a zone cluster");
		return (PNM_EPERM);
	}
	//
	// Verify the adapter here is authorized to be used within the zone
	// cluster.
	//
	if (!verify_adapter(zonenamep, (char *)adp_namep, env)) {
		//
		// SCMSGS
		// @explanation
		// An attempt was made to retrieve information
		// about an adapter that is not configured to be used
		// for the zone cluster.
		// @user_action
		// This is an informational message. No user action
		// is required.
		//
		(void) logger.log(LOG_NOTICE, MESSAGE,
		    "Zone cluster %s is not authorized to access"
		    " adapter %s", zonenamep, adp_namep);
		return (PNM_EPERM);
	}
	retval = ::pnm_map_adapter((char *)adp_namep, &group);
	if (retval != 0) {
		grp_namep = (char *)NULL;
	} else {
		grp_namep = (char *)group;
	}
	return (retval);
}

//
// Common code shared by pnm_grp_adp_statusp and pnm_grp_adp_statusp_zc.
//
int32_t
pnm_server_impl::pnm_grp_adp_status_common(const char *grp_namep,
    pnm_mod::AdpstatusSeq_out grp_statusp, Environment &) const {

	int			retval;
	pnm_grp_status_t	group_status;
	uint_t			indx;
	os::sc_syslog_msg	logger(SC_SYSLOG_PNMPROXY_TAG, "", NULL);

	retval = ::pnm_grp_adp_status((char *)grp_namep, &group_status);

	if (retval != 0) {
		grp_statusp = new pnm_mod::AdpstatusSeq();
		if (!grp_statusp) {
			(void) logger.log(LOG_ERR, MESSAGE,
			    "Could not allocate memory, size = %d",
			    sizeof (pnm_mod::AdpstatusSeq));
			exit(errno);
		}
		return (retval);
	}
	if (group_status.num_adps > 0) {
		grp_statusp = new pnm_mod::AdpstatusSeq(group_status.num_adps,
		    group_status.num_adps);
		if (!grp_statusp) {
			//
			// SCMSGS
			// @explanation
			// Could not allocate memory. Node is too low on memory.
			// @user_action
			// Contact your authorized Sun service provider to
			// determine whether a workaround or patch is
			// available.
			//
			(void) logger.log(LOG_ERR, MESSAGE,
			    "Could not allocate memory, size = %d"
			    " for %d elements",
			    sizeof (pnm_mod::AdpstatusSeq),
			    group_status.num_adps);
			exit(errno);
		}
	} else {
		grp_statusp = new pnm_mod::AdpstatusSeq();
		if (!grp_statusp) {
			(void) logger.log(LOG_ERR, MESSAGE,
			    "Could not allocate memory, size = %d",
			    sizeof (pnm_mod::AdpstatusSeq));
			exit(errno);
		}
	}
	pnm_mod::AdpstatusSeq &adp_status_ref = *grp_statusp;
	pnm_mod::pnm_adp_status *adp_statusp = NULL;

	for (indx = 0; indx < group_status.num_adps; ++indx) {
		adp_statusp = new pnm_mod::pnm_adp_status();

		if (!adp_statusp) {
			(void) logger.log(LOG_ERR, MESSAGE,
			    "Could not allocate memory, size = %d",
			    sizeof (pnm_mod::pnm_adp_status));
			exit(errno);
		}
		adp_statusp->adp_name =
		    os::strdup(group_status.pnm_adp_stat[indx].adp_name);
		if (adp_statusp->adp_name == NULL) {
			(void) logger.log(LOG_ERR, MESSAGE,
			    "Could not allocate memory, size = %d",
			    os::strlen(
			    group_status.pnm_adp_stat[indx].adp_name));
			exit(errno);
		}
		adp_statusp->status = group_status.pnm_adp_stat[indx].status;
		adp_status_ref[indx] = *adp_statusp;
		delete adp_statusp;
	}
	pnm_grp_status_free(&group_status);
	return (0);
}

//
// Return the adapter status of all the adapters in the given IPMP
// group. For a zone cluster, the IPMP group should be allowed to be used
// within the zone cluster. If not an error PNM_EPERM is returned back to
// the caller.
// Return value:
// 0 on success. A PNM error code is returned on failure.
//
int32_t
pnm_server_impl::pnm_grp_adp_status(const char *grp_namep,
    pnm_mod::AdpstatusSeq_out grp_statusp, Environment &env)
{
	int			retval;
	char 			*zonenamep = NULL;
	uint32_t		clid = env.get_cluster_id();

	os::sc_syslog_msg logger(SC_SYSLOG_PNMPROXY_TAG, "", NULL);

	if (is_global) {
		if (clid == 0) {
			// Error.
			(void) logger.log(LOG_ERR, MESSAGE, "PNM proxy "
			    "invoked for global zone public network "
			    "information");
			return (PNM_EPROG);
		} else {
			retval = clconf_get_cluster_name(clid, &zonenamep);
			if (retval != 0) {
				// Error.
				(void) logger.log(LOG_ERR, MESSAGE,
				    "Failed to get zone cluster name from "
				    "zone cluster id %d, error = %d", clid,
				    retval);
				return (PNM_EPROG);
			}
			retval = pnm_grp_adp_status_zc(zonenamep, grp_namep,
			    grp_statusp, env);
			delete [] zonenamep;
		}
		return (retval);
	}
	// Proxy server is running within an Exclusive-ip zone.
	if (clid >= MIN_CLUSTER_ID) {
		//
		// Client is from a zone cluster and it is trying
		// to get information of an exclusive-ip zone.
		//
		return (PNM_EPERM);
	}
	retval = pnm_grp_adp_status_common(grp_namep, grp_statusp, env);
	return (retval);
}


//
// This interface is called by clients in the global zone who would
// need to fetch public network information for a zone cluster.
// This interface can also by invoked by clients within a zone cluster.
//
int32_t
pnm_server_impl::pnm_grp_adp_status_zc(const char *zonenamep,
    const char *grp_namep, pnm_mod::AdpstatusSeq_out grp_statusp,
    Environment &env) {

	int	retval;
	os::sc_syslog_msg logger(SC_SYSLOG_PNMPROXY_TAG, "", NULL);
	//
	// Verify that the group name mentioned here is authorized to be used
	// by the zone cluster.
	//
	if (!verify_group(zonenamep, (char *)grp_namep, env)) {
		grp_statusp = new pnm_mod::AdpstatusSeq();
		if (!grp_statusp) {
			(void) logger.log(LOG_ERR, MESSAGE,
			    "Could not allocate memory, size = %d",
			    sizeof (pnm_mod::AdpstatusSeq));
			exit(errno);
		}
		(void) logger.log(LOG_ERR, MESSAGE,
		    "Zone cluster %s is not authorized to access"
		    " IPMP group %s", zonenamep, grp_namep);
		return (PNM_EPERM);
	}
	retval = pnm_grp_adp_status_common(grp_namep, grp_statusp, env);
	return (retval);
}

//
// Return what IP instances are plumbed on the given group. For a zone
// cluster, the group should be authorized to be used in a zone cluster
// environment.
// Return value: 0 on success. A non-zero pnm error code is returned to the
// caller on failure.
//
int32_t
pnm_server_impl::pnm_get_instances(const char *grp_namep, int32_t &result,
    Environment &env)
{
	int		retval;
	uint32_t	clid = env.get_cluster_id();
	char 		*zonenamep = NULL;

	os::sc_syslog_msg logger(SC_SYSLOG_PNMPROXY_TAG, "", NULL);

	if (!is_global) {
		if (clid >= MIN_CLUSTER_ID) {
			//
			// Client is from a zone cluster and it is trying
			// to get information of an exclusive-ip zone.
			//
			return (PNM_EPERM);
		}
		retval = ::pnm_get_instances((char *)grp_namep, &result);
		return (retval);
	}
	if (clid == 0) {
		// Error
		(void) logger.log(LOG_ERR, MESSAGE, "PNM proxy invoked for "
		    "global zone public network information");
		return (PNM_EPROG);
	} else {
		retval = clconf_get_cluster_name(clid, &zonenamep);
		if (retval != 0) {
			// Error.
			(void) logger.log(LOG_ERR, MESSAGE, "Failed to get "
			    "zone cluster name from zone cluster id %d,"
			    " error = %d", clid, retval);
			return (PNM_EPROG);
		}
		retval = pnm_get_instances_zc(zonenamep, grp_namep,
		    result, env);
		delete [] zonenamep;
	}
	return (retval);
}

int32_t
pnm_server_impl::pnm_get_instances_zc(const char *zonenamep,
    const char *grp_namep, int32_t &result, Environment &env)
{
	int		retval;
	uint32_t	clid = env.get_cluster_id();

	os::sc_syslog_msg logger(SC_SYSLOG_PNMPROXY_TAG, "", NULL);

	if (!is_global) {
		(void) logger.log(LOG_ERR, MESSAGE,
		    "Zone cluster PNM interface invoked instead of PNM "
		    "interface for exclusive-ip zones");
		return (PNM_EPERM);
	}
	if ((clid != 0) && (clid < MIN_CLUSTER_ID)) {
		(void) logger.log(LOG_ERR, MESSAGE,
		    "Zone cluster PNM interface invoked from a non-global "
		    "zone that does not belong to a zone cluster");
		return (PNM_EPERM);
	}
	//
	// Verify that the group name mentioned here is authorized to be
	// used by the zone cluster.
	//
	if (!verify_group(zonenamep, (char *)grp_namep, env)) {
		(void) logger.log(LOG_ERR, MESSAGE,
		    "Zone cluster %s is not authorized to access"
		    " IPMP group %s", zonenamep, grp_namep);
		return (PNM_EPERM);
	}
	retval = ::pnm_get_instances((char *)grp_namep, &result);
	return (retval);
}
