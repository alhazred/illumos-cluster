//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// pnm_mod_impl.h
//

#ifndef	_PNM_MOD_IMPL_H
#define	_PNM_MOD_IMPL_H

#pragma ident	"@(#)pnm_mod_impl.h	1.6	09/02/11 SMI"

#include <orb/invo/corba.h>
#include <orb/object/adapter.h>
#include <orb/invo/common.h>
#include <h/pnm_mod.h>

#define	PNM_PROXY_SERVER "pnm_proxy_server"

//
// Implementation object definition for the PNM proxy service
//
// CSTYLED
class pnm_server_impl : public McServerof<pnm_mod::pnm_server>
{

public:
	pnm_server_impl();

	void _unreferenced(unref_t cookie);

	int32_t pnm_group_list(CORBA::String_out listp, Environment &e);

	int32_t pnm_group_status(const char *grp_name,
	    pnm_mod::pnm_status_out status, Environment &e);

	int32_t pnm_adp_info_list(
	    pnm_mod::AdpinfoSeq_out adp_listp, bool alladdrs, Environment &e);
	int32_t pnm_map_adapter(const char *adp_name,
	    CORBA::String_out grp_name, Environment &e);

	int32_t pnm_grp_adp_status(const char *grp_name,
	    pnm_mod::AdpstatusSeq_out grp_statusp, Environment &e);

	int32_t pnm_get_instances(const char *grp_name,
	    int32_t &result, Environment &e);

	bool is_alive(Environment &);

	int32_t pnm_group_list_zc(const char *zonenamep,
	    CORBA::String_out listp, Environment &e);

	int32_t pnm_group_status_zc(const char *zonenamep,
	    const char *grp_name, pnm_mod::pnm_status_out status,
	    Environment &e);

	int32_t pnm_adp_info_list_zc(const char *zonenamep,
	    pnm_mod::AdpinfoSeq_out adp_listp, bool alladdrs,
	    Environment &e);

	int32_t pnm_map_adapter_zc(const char *zonenamep,
	    const char *adp_name, CORBA::String_out grp_name,
	    Environment &e);

	int32_t pnm_grp_adp_status_zc(const char *zonenamep,
	    const char *grp_name, pnm_mod::AdpstatusSeq_out grp_statusp,
	    Environment &e);

	int32_t pnm_get_instances_zc(const char *zonenamep,
	    const char *grp_name, int32_t &result, Environment &e);
private:
	char ** get_adapters(const char *) const;

	bool verify_group(const char *, const char *, Environment&);

	bool verify_adapter(const char *, const char *, Environment&);

	int32_t pnm_grp_adp_status_common(const char *grp_namep,
	    pnm_mod::AdpstatusSeq_out grp_statusp, Environment &e) const;

	bool is_global;
};

#endif // _PNM_MOD_IMPL_H
