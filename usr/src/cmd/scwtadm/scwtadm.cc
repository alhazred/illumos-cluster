//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)scwtadm.cc	1.4	08/05/20 SMI"

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <h/network.h>
#include <nslib/ns.h>
#include <sys/rm_util.h>
#include <cl_net/netlib.h>
#include <scadmin/scconf.h>
#include <scha.h>
#include <rgm/sczones.h>

#include <orb/infrastructure/orb.h>

static network::PDTServer_ptr get_pdt_server(void);
static nodeid_t get_nodeid(const char *nodeid_str);
static char *lbpolicy_to_string(network::lb_policy_t policy);
static int update_weights(char *str, network::PDTServer_ptr pdts);
static void print_services(network::PDTServer_ptr);

static void print_usage(const char *format, ...);
static void print_error(const char *format, ...);

//
// Maximum length of a string representation of a nodeid (counting '\0')
//
#define	NODEID_BUFSZ	64
#define	WEIGHT_DELIMITER	'@'
#define	NODE_DELIMITER	","

//
// The name of the service to query about.
//
static char *rs_name = NULL;
static char *progname;

//
// lb_weight_t structure which contains info about node and weight
//
typedef struct lb_weight {
	uint_t weight;
	uint_t node;
} lb_weight_t;

static FILE *outfp = stdout;
static FILE *errfp = stderr;


int
main(int argc, char *argv[])
{
	Environment		e;
	int			c;
	network::PDTServer_var pdts = NULL;
	int			ret = 0;
	boolean_t change_flag = B_FALSE;
	boolean_t print_flag = B_FALSE;
	char    *weight_str = NULL;
	uint_t ismember = 0;

	if (sc_zonescheck() != 0)
		return (1);

	if ((progname = strrchr(argv[0], '/')) == NULL)
		progname = argv[0];
	else
		progname++;

	// Check the cluster membership before initializing the orb
	(void) scconf_ismember(0, &ismember);
	if (!ismember) {
		print_error("This node is not a cluster member.");
		return (1);
	}

	// Validate the usage options
	if (argc == 1) {
		print_usage("Invalid number of arguments.");
		ret = -1;
		return (ret);
	}

	while ((c = getopt(argc, argv, "pcw:j:")) != EOF) {
		switch (c) {
		case 'j':
			rs_name = optarg;
			break;
		case 'p':
			print_flag = B_TRUE;
			break;
		case 'c':
			change_flag = B_TRUE;
			break;
		case 'w':
			weight_str = optarg;
			break;
		default:
			print_usage("Invalid option is specified.");
			ret = -1;
			return (ret);
		}
	}

	if (!change_flag) {
		if (!print_flag) {
			print_usage("Invalid options, specify either -c/-p."
			    "be specified.");
			ret = -1;
			return (ret);
		}
	}

	if (change_flag && print_flag) {
		print_usage("Invalid options are specified.");
		ret = -1;
		return (ret);
	}

	if (change_flag) {
		if (rs_name == NULL || strlen(rs_name) == 0) {
			print_usage("Invalid value for the resource name.");
			ret = -1;
			return (ret);
		}
	}

	if (change_flag) {
		if ((weight_str == NULL) || (strlen(weight_str) == 0)) {
			print_usage("Invalid input value for the "
			    "Load_balancing_weights property.");
			ret = -1;
			return (ret);
		}
	}

	if (ORB::initialize() != 0) {
		print_error("Failed to initialize the ORB.");
		ret = -1;
		return (ret);
	}
	//
	// Get the PDT server.
	//
	pdts = get_pdt_server();
	if (CORBA::is_nil(pdts)) {
		print_error("Cannot get an object reference to PDT Service");
		ret = -1;
		return (ret);
	}

	// If the weight_string is not null, change the weights
	if (change_flag) {
		ret = update_weights(weight_str, pdts);
		return (ret);
	}

	if (print_flag) {
		print_services(pdts);
	}
	return (0);
}

//
// Concert a nodename into nodeid. If the nodename given can't be
// concerted, it is assumed to be a string form of nodeid and atoi()
// is used to concert it.
//
static nodeid_t
get_nodeid(const char *nodeid_str)
{
#ifndef	UNODE
	scconf_errno_t err;
	scconf_nodeid_t nodeid;
	char nodeid_buf[NODEID_BUFSZ];

	(void) strcpy(nodeid_buf, nodeid_str);
	err = scconf_get_nodeid(nodeid_buf, &nodeid);

	return (err? (nodeid_t)atoi(nodeid_str): nodeid);
#else
	return ((nodeid_t)atoi(nodeid_str));
#endif
}

static void
print_error(const char *format, ...)
{
	char _buf[MAXPATHLEN];

	va_list v;
	va_start(v, format);	/* lint !e40 */
	(void) vsprintf(_buf, format, v);
	(void) fprintf(errfp, "%s: %s\n", progname, _buf);
	va_end(v);
}

static void
print_usage(const char *format, ...)
{
	char _buf[MAXPATHLEN];

	if (format) {
		va_list v;
		va_start(v, format);	// lint !e40
		(void) vsprintf(_buf, format, v);
		(void) fprintf(errfp, "%s: %s\n", progname, _buf);
		va_end(v);
	}

	(void) fprintf(errfp, "Usage: %s\n"
	    "\t -c -w <weights> -j <resource_name>\n"
	    "\t -p [-j <resource_name>]\n",
	    progname);
}

//
// Effects:  Return an object reference to the PDT service if the service
//   is known to the system.  Otherwise, returns NULL.
//
static network::PDTServer_ptr
get_pdt_server()
{
	Environment			e;
	replica::rm_admin_var		rm_ref;
	replica::service_admin_var	controlp;
	CORBA::Object_var		nobjv;

	//
	// Get a reference to PDTServer object.
	//
	rm_ref = rm_util::get_rm();
	ASSERT(!CORBA::is_nil(rm_ref));
	controlp = rm_ref->get_control(network::PDTServer::NS_NAME, e);
	if (e.exception()) {
		sol::op_e *ex_op = sol::op_e::_exnarrow(e.exception());

		if (ex_op) {
			errno = ex_op->error;
			perror("get_pdt_server: Can't find control object\n");
		} else {
			e.exception()->print_exception("get_pdt_server: "
			    "Can't find control object\n");
		}
		e.clear();
		return (NULL);
	}

	//
	// Return the object reference of the current primary.
	//
	nobjv = controlp->get_root_obj(e);
	if (e.exception()) {
		sol::op_e *ex_op = sol::op_e::_exnarrow(e.exception());

		if (ex_op) {
			errno = ex_op->error;
			perror("get_pdt_server: Can't find root object\n");
		} else {
			e.exception()->print_exception("get_pdt_server: "
			    "Can't find root object\n");
		}
		e.clear();
		return (NULL);
	}

	//
	// Coerce corba object to PDTServer object
	//
	network::PDTServer_ptr pdtsp = network::PDTServer::_narrow(nobjv);
	if (CORBA::is_nil(pdtsp)) {
		e.clear();
		return (NULL);
	}
	ASSERT(is_not_nil(pdtsp));

	return (pdtsp);
}

//
// Verified whether the given nodeid is valid.
//
boolean_t
is_nodeid_valid(nodeid_t nodeid)
{
	uint_t	i;
	scha_err_t	e;
	scha_uint_array_t	*all_nodeids = NULL;
	scha_cluster_t	clust_handle;
	boolean_t	found = B_FALSE;

	if (nodeid == 0) {
		return (B_FALSE);
	}

retry_again:

	e = scha_cluster_open(&clust_handle);
	if (e != SCHA_ERR_NOERR) {
		print_error("Failed to retrieve the cluster handle "
		    "while querying for property %s: %s.",
		    SCHA_ALL_NODEIDS, e);
		exit(1);
	}

	e = scha_cluster_get(clust_handle, SCHA_ALL_NODEIDS,
	    &all_nodeids);
	if (e == SCHA_ERR_SEQID) {
		/* close the cluster handle */
		(void) scha_cluster_close(clust_handle);
		clust_handle = NULL;
		(void) sleep(1);
		goto retry_again;
	} else if (e != SCHA_ERR_NOERR) {
		print_error("Failed to retrieve the cluster "
		    "property %s: %s.",
		    SCHA_ALL_NODEIDS, e);
		(void) scha_cluster_close(clust_handle);
		exit(1);
	}

	if (all_nodeids == NULL) {
		print_error("Null value is returned for %s.", SCHA_ALL_NODEIDS);
		(void) scha_cluster_close(clust_handle);
		exit(1);
	}

	for (i = 0; i < all_nodeids->array_cnt; i++) {
		if ((uint_t)nodeid == all_nodeids->uint_array[i]) {
			found = B_TRUE;
			break;
		}
	}
	(void) scha_cluster_close(clust_handle);
	return (found);
}

//
// get_array_size(): Tokenizes the input string using the given
// delimiter. Returns the number of tokens in the input string.
//
size_t
get_array_size(char *str, char *pattern)
{
	char	*in_str;
	size_t	size;
	char	*ptr;

	in_str = strdup(str);
	ptr = strtok(in_str, pattern);
	size = 0;

	while (ptr != NULL) {
		ptr = strtok(NULL, pattern);
		size++;
	}
	free(in_str);
	return (size);
}


//
// process_weights(): Validates the input weight string and
// then populates the lb_weight_t structure with the valid
// node and weight values.
int
process_weights(char **w_array, size_t a_size, lb_weight_t **weights)
{

	size_t	i = 0;
	char	*weight_string = NULL;
	char	*node_string = NULL;
	int	weight_int;
	char    *endp = NULL;
	nodeid_t	 node_id;
	lb_weight_t	*weight_in = NULL;

	weight_in = (lb_weight_t *)
	    malloc(a_size * sizeof (lb_weight_t));
	if (weight_in == NULL) {
		print_error("malloc() failed with error: %s",
		    strerror(errno));
		return (1);
	}

	for (i = 0; i < a_size; i++) {
		weight_string = strdup(w_array[i]);
		if (weight_string == NULL) {
			print_error("strdup() failed with error: %s",
			    strerror(errno));
			free(weight_in);
			return (1);
		}

		// Tokenize the string using '@' delimiter
		node_string = strchr(weight_string, WEIGHT_DELIMITER);
		if (node_string == NULL) {
			print_error("Invalid node value in "
			    "Load_balancing_weights string. The format "
			    "of the string is "
			    "weight@node[,weight@node...].\n");
			free(weight_in);
			return (1);
		} else {
			// the string has proper format, advance the ptr
			*node_string = NULL;
			node_string = node_string + 1;
		}

		if (strlen(node_string) == 0) {
			print_error("Invalid node value.");
			free(weight_in);
			return (1);
		}

		errno = 0;

		//
		// Get the weight value
		// Check whether the weight has been specified
		//
		if (strlen(weight_string) == 0) {
			print_error("Invalid weight in"
			    "Load_balancing_weights string. The format "
			    "of the string is "
			    "weight@node[,weight@node,...].");
			free(weight_in);
			return (1);
		}
		weight_int = strtol(weight_string, &endp, 10);
		if (errno != 0 || *endp != '\0') {
			print_error("Incorrect weight value is specified. "
			    "The weight should be greater than or equal "
			    "to zero.");
			free(weight_in);
			return (1);
		}

		// The weight value should be greater than or equal to zero
		if (weight_int < 0) {
			print_error("Incorrect weight value is specified. "
			    "The weight should be greater than or equal "
			    "to zero.");
			free(weight_in);
			return (1);
		}

		weight_in[i].weight = (uint_t)weight_int;

		// Check if the node is a valid nodename
		node_id = get_nodeid(node_string);

		//
		// Verify whether the nodeid is valid and belongs to the
		// cluster
		//
		if (is_nodeid_valid(node_id) == B_FALSE) {
			print_error("%s is not a valid node name or node ID.",
			    node_string);
			free(weight_in);
			return (1);
		}
		weight_in[i].node = (uint_t)node_id;
	}
	*weights = weight_in;
	free(weight_string);
	return (0);
}


//
// update_weights: Updates the PDT table with the new DLMI weights.
// CCR weights will be unmodified.
//
int
update_weights(char *w_str, network::PDTServer_ptr pdts)
{

	int	ret = 0;
	Environment e;
	lb_weight_t	*weights;
	size_t	array_size = 0;
	char	*tmp_str;
	char	*ptr;
	char 	**w_array;
	size_t	i = 0;
	uint_t	nid;

	if (w_str == NULL) {
		print_error("Null input value is passed in the  "
		    "Load_balancing_weights string.");
		return (1);
	}

	//
	// Get the number of elements in the given weight string.
	// array_size contains the number of nodes where weights
	// need to be updated.
	//
	array_size = get_array_size(w_str, NODE_DELIMITER);
	if (array_size == 0) {
		print_error("Invalid value for the Load_balancing_weight "
		    "value.");
		return (1);
	}

	//
	//  Convert the string to string array for easy processing later
	//
	w_array = (char **)malloc((array_size) * sizeof (char *));
	if (w_array == NULL) {
		print_error("malloc() failed with error: %s",
		    strerror(errno));
		return (1);
	}

	tmp_str = strdup(w_str);
	ptr = strtok(tmp_str, NODE_DELIMITER);
	while (ptr != NULL) {
		w_array[i] = strdup(ptr);
		if (w_array[i] == NULL) {
			print_error("malloc() failed with error: %s",
			    strerror(errno));
			return (1);
		}
		ptr = strtok(NULL, NODE_DELIMITER);
		i++;
	}
	free(tmp_str);

	//
	// Validate the given weight string and store the
	// values in lb_weight_t structure.
	//
	ret = process_weights(w_array, array_size, &weights);
	if (ret != 0) {
		print_error("Failed to validate the given "
		    "Load_balancing_weights string.");
		free(w_array);
		return (1);
	}

	// Send the new weights to the load_balancer

	// Get the lbobj
	network::group_t gname;

	(void) strlcpy(gname.resource, rs_name,
	    (size_t)network::resource_name_len);

	network::lbobj_i_ptr lbobj = pdts->get_lbobj(gname, e);
	if (e.exception()) {
		e.clear();
		free(w_array);
		return (nil);
	}

	if (CORBA::is_nil(lbobj)) {
		print_error("Invalid scalable resource %s", rs_name);
		free(w_array);
		return (nil);
	}

	network::weighted_lbobj_var weighted_lbobj =
		network::weighted_lbobj::_narrow(lbobj);
	if (CORBA::is_nil(weighted_lbobj)) {
		print_error("Invalid scalable resource %s", rs_name);
		free(w_array);
		return (1);
	}
	if (array_size == 1) {
		// Only one node needs weight change, call set_weight
		weighted_lbobj->set_weight((unsigned int)weights[0].node,
		    (unsigned int)weights[0].weight, e);
		if (e.exception() != NULL) {
			sol::op_e *ex_op = sol::op_e::_exnarrow(e.exception());
			if (ex_op) {
				errno = ex_op->error;
				perror("set_weight: set_weight failed");
			} else {
				e.exception()->print_exception(
				    "set_weight: Failed to set the weight");
			}
			e.clear();
			free(w_array);
			return (1);
		}
	} else {
		network::dist_info_t_var new_weights;

		// Get the current weight information
		new_weights = lbobj->get_distribution(e);
		if (e.exception()) {
			// exception is cleared by get_errno()
			perror("get_distribution() failed");
			free(w_array);
			return (1);
		}

		// Plug in the desired weights
		for (i = 0; i < array_size; i++) {
			// Update the weights only on those nodes which
			// are specified in the user input. The weight
			// values on the rest of the nodes will be
			// unchanged.
			nid = weights[i].node;
			new_weights->weights[nid] = weights[i].weight;
		}
		// Try setting it.
		weighted_lbobj->set_distribution(*new_weights, e);
		if (e.exception()) {
			// exception is cleared by get_errno()
			perror("set_distribution() failed");
			free(w_array);
			return (1);
		}
	}
	free(w_array);
	return (0);
}

//
// Print the load_balancer values
void
print_services(network::PDTServer_ptr pdts) {

	Environment	e;
	network::grpinfoseq_t_var	sinfo;
	network::group_t	gname;
	uint_t i, j;
	uint_t count;

	//
	// resource name can be NULL here to specify that
	// we print out all services.
	//
	if (rs_name) {
		(void) strlcpy(gname.resource, rs_name,
		    (size_t)network::resource_name_len);
	} else {
		*gname.resource = NULL;
	}

	if (pdts) {
		pdts->gns_service(sinfo, gname, e);
		if (e.exception() != NULL) {
			sol::op_e *ex_op = sol::op_e::_exnarrow(e.exception());

			if (ex_op) {
				errno = ex_op->error;
				perror(gname.resource);
			} else {
				e.exception()->print_exception(
				    gname.resource);
			}
			e.clear();
			return;
		}
	}

	//
	// Parse the service information sequence and print it out.
	//
	for (i = 0; i < sinfo->length(); i++) {
		(void) fprintf(outfp, "Resource %s:\n",
		    sinfo[i].srv_group.resource);

		(void) fprintf(outfp, "\tLoad_balancing_policy:\t%s\n",
		    lbpolicy_to_string(sinfo[i].srv_policy));

		count = sinfo[i].srv_lb_weights->length();
		if (count > 1) {
			//
			// We skip the first element since it may
			// currently contain garbage (see marshal_sgrp).
			//
			(void) fprintf(outfp, "\tLoad_Balancing_weights:\t");
			for (j = 1; j < count; j++) {
				if (sinfo[i].srv_lb_weights[j]) {
					(void) fprintf(outfp, "%d@%d ",
					    sinfo[i].srv_lb_weights[j], j);
				}
			}
			(void) fprintf(outfp, "\n");
		}
	}
}

//
// Effects:  Given the load-balancing policy "policy" return the
//   character string that represents that policy.
//
static char *
lbpolicy_to_string(network::lb_policy_t policy)
{
	switch (policy) {
		case network::LB_WEIGHTED:
			return ("LB_WEIGHTED");
		case network::LB_PERF:
			return ("LB_PERF");
		case network::LB_USER:
			return ("LB_USER");
		case network::LB_ST:
			return ("LB_STICKY");
		case network::LB_SW:
			return ("LB_STICKY_WILD");
		default:
			return ("UNKNOWN POLICY");
	} // end switch
}
