/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scsymon_clnt.c	1.11	08/05/20 SMI"

/*
 * scsymon_clnt(1M)
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <locale.h>
#include <libintl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <unistd.h>
#include <fcntl.h>
#include <door.h>
#include <scadmin/scsymon_srv.h>

static char *progname;

static void usage(void);
static void print_error(scsymon_errno_t error);
static scsymon_errno_t print_all(int d);
static scsymon_errno_t print_table(int d, char *pname);

static char *tables[] = {
	"cluster_config",
	"node_config",
	"nodedev_config",
	"devgrp_config",
	"qdev_config",
	"qdevPort_config",
	"tr_adapter_config",
	"switch_config",
	"cable_config",
	"RT_config",
	"RT_method_config",
	"RT_param_config",
	"RT_RS_config",
	"FORG_RG_config",
	"FORG_RS_config",
	"FORG_RS_com_prop_config",
	"FORG_RS_ext_prop_config",
	"FORG_RS_timeout_config",
	"SCRG_RG_config",
	"SCRG_RS_config",
	"SCRG_RS_com_prop_config",
	"SCRG_RS_ext_prop_config",
	"SCRG_RS_timeout_config",
	"cluster_status",
	"node_status",
	"nodedev_status",
	"qdev_status",
	"devgrp_status",
	"replica_status",
	"path_status",
	"FORG_RG_status",
	"FORG_RS_status",
	"SCRG_RG_status",
	"SCRG_RS_status",
	NULL
};

/*
 * Parse arguments.
 * Execute the command.
 * Print error if any.
 *
 * Possible return values:
 * 	SCSYMON_ENOERR		- no error
 *	SCSYMON_EPERM		- not root
 *	SCSYMON_ENOTCLUSTER	- not a cluster node
 * 	SCSYMON_ENOTCONFIGURED	- not found in CCR
 *	SCSYMON_ENOMEM          - not enough memory
 *	SCSYMON_EINVAL		- scconf: invalid argument
 *	SCSYMON_ESERVICENAME	- invalid device group name
 *	SCSYMON_ECLUSTERRECONFIG - cluster is reconfiguring
 *	SCSYMON_ERGRECONFIG	- RG is reconfiguring
 *	SCSYMON_EOBSOLETE	- Resource/RG has been updated
 *	SCSYMON_EDOOROPEN	- unable to open door
 *	SCSYMON_EDOORCALL	- unable to make door call
 *      SCSYMON_EUNEXPECTED      - internal or unexpected error
 */
int
main(int argc, char **argv)
{
	scsymon_errno_t error = SCSYMON_ENOERR;
	scconf_errno_t scconf_error = SCCONF_NOERR;
	int i;
	uint_t ismember = 0;
	char **ptable = &tables[0];
	int fddoor;
	boolean_t found = B_FALSE;

	/* Set the program name */
	progname = strrchr(argv[0], '/');
	if (progname == NULL) {
		progname = argv[0];
	} else {
		++progname;
	}

	/* I18N housekeeping */
	(void) setlocale(LC_ALL, "");
	(void) textdomain(TEXT_DOMAIN);
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	/* need not be root to print */
	if (getuid() != 0) {
		if (geteuid() != 0) {
			(void) fprintf(stderr, gettext(
			    "%s:  Effective uid is not set to root - "
			    "unable to run .\n"),
			    progname);
			error = SCSYMON_EPERM;
			print_error(error);

			return (error);
		}
		(void) setuid(0);
	}

	/* must be root to proceed */
	if (getuid() != 0) {
		(void) fprintf(stderr, gettext(
		    "%s:  You must be root.\n"), progname);
		error = SCSYMON_EPERM;
		print_error(error);

		return (error);
	}

	/* make sure that we are active in the cluster */
	scconf_error = scconf_ismember(0, &ismember);
	error = scsymon_convert_scconf_error_code(scconf_error);
	if (error != SCSYMON_ENOERR) {
		print_error(error);

		return (error);
	}

	if (!ismember) {
		print_error(SCSYMON_ENOTCLUSTER);

		return (error);
	}

	if ((fddoor = open(SCSYMON_DOOR_FILE, O_RDONLY)) < 0) {
		error = SCSYMON_EDOOROPEN;
		print_error(error);

		return (error);
	}

	/* check for basic options */
	if (argc < 2) {
		/* no option, print default */
		error = print_all(fddoor);
		if (error != SCSYMON_ENOERR)
			print_error(error);

		return (error);
	}

	for (i = 1; i < argc && error == SCSYMON_ENOERR; i++) {
		while (ptable) {
			if (*ptable == NULL)
				break;

			if (strcasecmp(argv[i], *ptable) == NULL) {
				found = B_TRUE;
				error = print_table(fddoor, argv[i]);
				break;
			}

			ptable++;
		}

		if (found == B_FALSE) {
			error = SCSYMON_EUSAGE;
			usage();
			return (error);
		}
	}

	if (error != SCSYMON_ENOERR)
		print_error(error);

	return (error);
}

/*
 * usage
 *
 *      Print a simple usage message to stderr.
 */
static void
usage()
{
	(void) fprintf(stderr, "usage:  %s [<object>]\n",
	    progname);
	(void) fprintf(stderr, "can be one of more of the following:\n");
	(void) fprintf(stderr, "\tcluster_config\n");
	(void) fprintf(stderr, "\tnode_config\n");
	(void) fprintf(stderr, "\tnodedev_config\n");
	(void) fprintf(stderr, "\tdevgrp_config\n");
	(void) fprintf(stderr, "\tqdev_config\n");
	(void) fprintf(stderr, "\tqdevport_config\n");
	(void) fprintf(stderr, "\ttr_adapter_config\n");
	(void) fprintf(stderr, "\tswitch_config\n");
	(void) fprintf(stderr, "\tcable_config\n");
	(void) fprintf(stderr, "\tRT_config\n");
	(void) fprintf(stderr, "\tRT_method_config\n");
	(void) fprintf(stderr, "\tRT_param_config\n");
	(void) fprintf(stderr, "\tRT_RS_config\n");
	(void) fprintf(stderr, "\tFORG_RG_config\n");
	(void) fprintf(stderr, "\tFORG_RS_config\n");
	(void) fprintf(stderr, "\tFORG_RS_com_prop_config\n");
	(void) fprintf(stderr, "\tFORG_RS_ext_prop_config\n");
	(void) fprintf(stderr, "\tFORG_RS_timeout_config\n");
	(void) fprintf(stderr, "\tSCRG_RG_config\n");
	(void) fprintf(stderr, "\tSCRG_RS_config\n");
	(void) fprintf(stderr, "\tSCRG_RS_com_prop_config\n");
	(void) fprintf(stderr, "\tSCRG_RS_ext_prop_config\n");
	(void) fprintf(stderr, "\tSCRG_RS_timeout_config\n");
	(void) fprintf(stderr, "\tcluster_status\n");
	(void) fprintf(stderr, "\tnode_status\n");
	(void) fprintf(stderr, "\tnodedev_status\n");
	(void) fprintf(stderr, "\tqdev_status\n");
	(void) fprintf(stderr, "\tdevgrp_status\n");
	(void) fprintf(stderr, "\treplica_status\n");
	(void) fprintf(stderr, "\tpath_status\n");
	(void) fprintf(stderr, "\tFORG_RG_status\n");
	(void) fprintf(stderr, "\tFORG_RS_status\n");
	(void) fprintf(stderr, "\tSCRG_RG_status\n");
	(void) fprintf(stderr, "\tSCRG_RS_status\n");
	(void) fprintf(stderr, "\t\n");
}

/*
 * print_error
 *
 *	Print error to stderr.
 */
static void
print_error(scsymon_errno_t error)
{
	char text[SCSYMON_MAX_STRING_LEN];

	scsymon_strerr(error, text);
	(void) fprintf(stderr, "%s:  %s.\n", progname, text);
}

/*
 * print_all
 *
 *	Print all objects in the cluster.
 */
static scsymon_errno_t
print_all(int fddoor)
{
	scsymon_errno_t error = SCSYMON_ENOERR;
	char **ptable = &tables[0];

	while (ptable) {
		if (*ptable == NULL)
			break;

		error = print_table(fddoor, *ptable);
		if (error != SCSYMON_ENOERR)
			break;

		ptable++;
	}

	return (error);
}

/*
 * print_table
 *
 *	Print cluster properties by table.
 */
static scsymon_errno_t
print_table(int d, char *name)
{
	door_arg_t arg;
	char *rbuf = NULL;
	scsymon_errno_t error = SCSYMON_ENOERR;

	arg.data_ptr = name;
	arg.data_size = strlen(arg.data_ptr) + 1;
	arg.desc_ptr = 0;	/* not passing any descriptors */
	arg.desc_num = 0;
	arg.rbuf = rbuf;
	arg.rsize = 0;

	/* get cluster property and status */
	if (door_call(d, &arg) < 0) {
		error = SCSYMON_EDOORCALL;
		return (error);
	}

	if (arg.rbuf != NULL) {
		(void) printf("%s", arg.rbuf);
		(void) munmap(arg.rbuf, arg.rsize);
	}

	return (error);
}
