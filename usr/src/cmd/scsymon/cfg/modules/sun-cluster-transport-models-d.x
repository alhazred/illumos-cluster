#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#pragma ident	"@(#)sun-cluster-transport-models-d.x	1.8	08/05/20 SMI"
#
# Copyright (c) 1999-2001 by Sun Microsystems, Inc.
# All rights reserved.
#
# cluster/src/scsymon/cfg/modules/sun-cluster-transport-models-d.x
#

[ use MANAGED-OBJECT ]
mediumDesc = Cluster Transport  
consoleHint:mediumDesc = base.modules.sun-cluster:transportBranch

statusFolder = { [ use MANAGED-OBJECT ]
	mediumDesc	= Status
    	consoleHint:mediumDesc = base.modules.sun-cluster:transportBranch.statusFolder

	pathTable = { [ use MANAGED-OBJECT-TABLE ]
		mediumDesc	= Paths Table
		consoleHint:mediumDesc = base.modules.sun-cluster:transportBranch.statusFolder.pathTable

		pathTableEntry = { [ use MANAGED-OBJECT-TABLE-ENTRY ]
			mediumDesc	= Paths Table Entry 
			consoleHint:mediumDesc = base.modules.sun-cluster:transportBranch.statusFolder.pathTable.pathTableEntry
			index		= adapterName1 adapterName2

			adapterName1 = { [ use STRING MANAGED-PROPERTY ]
				mediumDesc	= Endpoint #1
				consoleHint:mediumDesc = base.modules.sun-cluster:transportBranch.statusFolder.pathTable.pathTableEntry.adapterName1
			}

			adapterName2 = { [ use STRING MANAGED-PROPERTY ]
				mediumDesc	= Endpoint #2
				consoleHint:mediumDesc = base.modules.sun-cluster:transportBranch.statusFolder.pathTable.pathTableEntry.adapterName2
			}

			status = { [ use STRINGREGEXP MANAGED-PROPERTY ]
				mediumDesc	= Status
				consoleHint:mediumDesc = base.modules.sun-cluster:transportBranch.statusFolder.pathTable.pathTableEntry.status
			}
		}
	}
}

propertyFolder = { [ use MANAGED-OBJECT ]
	mediumDesc	= Property
    	consoleHint:mediumDesc = base.modules.sun-cluster:transportBranch.propertyFolder


	adapterTable = { [ use MANAGED-OBJECT-TABLE ]
		mediumDesc	= Adapters Table
		consoleHint:mediumDesc = base.modules.sun-cluster:transportBranch.propertyFolder.adapterTable

		adapterTableEntry = { [ use MANAGED-OBJECT-TABLE-ENTRY ]
			mediumDesc	= Adapters Table Entry 
			consoleHint:mediumDesc = base.modules.sun-cluster:transportBranch.propertyFolder.adapterTable.adapterTableEntry
			index		= nodeName adapterName

			nodeName = { [ use STRING MANAGED-PROPERTY ]
				mediumDesc	= Node Name
				consoleHint:mediumDesc = base.modules.sun-cluster:transportBranch.propertyFolder.adapterTable.adapterTableEntry.nodeName
			}

			adapterName = { [ use STRING MANAGED-PROPERTY ]
				mediumDesc	= Adapter Name
				consoleHint:mediumDesc = base.modules.sun-cluster:transportBranch.propertyFolder.adapterTable.adapterTableEntry.adapterName
			}

			type = { [ use STRING MANAGED-PROPERTY ]
				mediumDesc	= Type
				consoleHint:mediumDesc = base.modules.sun-cluster:transportBranch.propertyFolder.adapterTable.adapterTableEntry.type
			}

			enabled = { [ use STRING MANAGED-PROPERTY ]
				mediumDesc	= Enabled
				consoleHint:mediumDesc = base.modules.sun-cluster:transportBranch.propertyFolder.adapterTable.adapterTableEntry.enabled
			}
		}
	}

	junctionTable = { [ use MANAGED-OBJECT-TABLE ]
		mediumDesc	= Adapters Table
		consoleHint:mediumDesc = base.modules.sun-cluster:transportBranch.propertyFolder.junctionTable

		junctionTableEntry = { [ use MANAGED-OBJECT-TABLE-ENTRY ]
			mediumDesc	= Junctions Table Entry 
			consoleHint:mediumDesc = base.modules.sun-cluster:transportBranch.propertyFolder.junctionTable.junctionTableEntry
			index		= junctionName

			junctionName = { [ use STRING MANAGED-PROPERTY ]
				mediumDesc	= Junction Name
				consoleHint:mediumDesc = base.modules.sun-cluster:transportBranch.propertyFolder.junctionTable.junctionTableEntry.junctionName
			}

			type = { [ use STRING MANAGED-PROPERTY ]
				mediumDesc	= Type
				consoleHint:mediumDesc = base.modules.sun-cluster:transportBranch.propertyFolder.junctionTable.junctionTableEntry.type
			}

			enabled = { [ use STRING MANAGED-PROPERTY ]
				mediumDesc	= Enabled
				consoleHint:mediumDesc = base.modules.sun-cluster:transportBranch.propertyFolder.junctionTable.junctionTableEntry.enabled
			}

			portList = { [ use STRING MANAGED-PROPERTY ]
				mediumDesc	= Port List
				consoleHint:mediumDesc = base.modules.sun-cluster:transportBranch.propertyFolder.junctionTable.junctionTableEntry.portList
			}
		}
	}

	cableTable = { [ use MANAGED-OBJECT-TABLE ]
		mediumDesc	= Adapters Table
		consoleHint:mediumDesc = base.modules.sun-cluster:transportBranch.propertyFolder.cableTable

		cableTableEntry = { [ use MANAGED-OBJECT-TABLE-ENTRY ]
			mediumDesc	= Junctions Table Entry 
			consoleHint:mediumDesc = base.modules.sun-cluster:transportBranch.propertyFolder.cableTable.cableTableEntry
			index		= endType1 endpoint1 endType2 endpoint2

			endType1 = { [ use STRING MANAGED-PROPERTY ]
				mediumDesc	= Endpoint #1 Type
				consoleHint:mediumDesc = base.modules.sun-cluster:transportBranch.propertyFolder.cableTable.cableTableEntry.endType1
			}

			endpoint1 = { [ use STRING MANAGED-PROPERTY ]
				mediumDesc	= Endpoint #1 name
				consoleHint:mediumDesc = base.modules.sun-cluster:transportBranch.propertyFolder.cableTable.cableTableEntry.endpoint1
			}

			endType2 = { [ use STRING MANAGED-PROPERTY ]
				mediumDesc	= Endpoint #2 Type
				consoleHint:mediumDesc = base.modules.sun-cluster:transportBranch.propertyFolder.cableTable.cableTableEntry.endType2
			}

			endpoint2 = { [ use STRING MANAGED-PROPERTY ]
				mediumDesc	= Endpoint #2 name
				consoleHint:mediumDesc = base.modules.sun-cluster:transportBranch.propertyFolder.cableTable.cableTableEntry.endpoint2
			}

			enabled = { [ use STRING MANAGED-PROPERTY ]
				mediumDesc	= Enabled
				consoleHint:mediumDesc = base.modules.sun-cluster:transportBranch.propertyFolder.cableTable.cableTableEntry.enabled
			}
		}
	}
}

