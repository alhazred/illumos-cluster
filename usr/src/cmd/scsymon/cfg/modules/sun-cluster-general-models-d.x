#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#pragma	ident	"@(#)sun-cluster-general-models-d.x	1.6	08/05/20 SMI"
#
# Copyright (c) 1999 by Sun Microsystems, Inc.
# All rights reserved.
#
# cluster/src/scsymon/cfg/modules/sun-cluster-general-models-d.x
#

#
# Sun Cluster general module file 
#
[ use MANAGED-OBJECT ]
mediumDesc = General   
consoleHint:mediumDesc = base.modules.sun-cluster:generalBranch

statusFolder = { [ use MANAGED-OBJECT ]
	mediumDesc	= Status
    	consoleHint:mediumDesc = base.modules.sun-cluster:generalBranch.statusFolder

	clusterName = { [ use STRING MANAGED-PROPERTY ]
		mediumDesc	= Cluster Name
		consoleHint:mediumDesc = base.modules.sun-cluster:generalBranch.statusFolder.clusterName
	}

	minVotesRequired = { [ use INT MANAGED-PROPERTY ]
		mediumDesc	= Votes Required
		consoleHint:mediumDesc = base.modules.sun-cluster:generalBranch.statusFolder.minVotesRequired
	}

	currentVotes = { [ use INT MANAGED-PROPERTY ]
		mediumDesc	= Current Votes
		consoleHint:mediumDesc = base.modules.sun-cluster:generalBranch.statusFolder.currentVotes
	}
}


propertyFolder = { [ use MANAGED-OBJECT ]
	mediumDesc	= Property
    	consoleHint:mediumDesc = base.modules.sun-cluster:generalBranch.propertyFolder

	clusterName = { [ use STRING MANAGED-PROPERTY ]
		mediumDesc	= Cluster Name
		consoleHint:mediumDesc = base.modules.sun-cluster:generalBranch.propertyFolder.clusterName
	}

	installMode = { [ use STRING MANAGED-PROPERTY ]
		mediumDesc	= Install Mode
		consoleHint:mediumDesc = base.modules.sun-cluster:generalBranch.propertyFolder.installMode
	}

	privateNetAddr = { [ use STRING MANAGED-PROPERTY ]
		mediumDesc	= Private Net
		consoleHint:mediumDesc = base.modules.sun-cluster:generalBranch.propertyFolder.privateNetAddr
			}

	privateNetMask = { [ use STRING MANAGED-PROPERTY ]
		mediumDesc	= Private Netmask
		consoleHint:mediumDesc = base.modules.sun-cluster:generalBranch.propertyFolder.privateNetMask
	}

	addNodeAuthType = { [ use STRING MANAGED-PROPERTY ]
		mediumDesc	= Add Node Authentication Type
		consoleHint:mediumDesc = base.modules.sun-cluster:generalBranch.propertyFolder.addNodeAuthType
	}

	addNodeAuthList = { [ use STRING MANAGED-PROPERTY ]
		mediumDesc	= Add Node List
		consoleHint:mediumDesc = base.modules.sun-cluster:generalBranch.propertyFolder.addNodeAuthList
	}
}


