#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#pragma ident	"@(#)sun-cluster-qdev-models-d.x	1.9	08/05/20 SMI"
#
# Copyright (c) 1999-2001 by Sun Microsystems, Inc.
# All rights reserved.
#
# cluster/src/scsymon/cfg/modules/sun-cluster-qdev-models-d.x
#

[ use MANAGED-OBJECT ]
mediumDesc = Quorum Devices 
consoleHint:mediumDesc = base.modules.sun-cluster:quorumDeviceBranch


statusFolder = { [ use MANAGED-OBJECT ]
	mediumDesc	= Status
    	consoleHint:mediumDesc = base.modules.sun-cluster:quorumDeviceBranch.statusFolder

	quorumDeviceTable = { [ use MANAGED-OBJECT-TABLE ]
		mediumDesc	= Quorum Devices Table
		consoleHint:mediumDesc = base.modules.sun-cluster:quorumDeviceBranch.statusFolder.quorumDeviceTable

		quorumDeviceTableEntry = { [ use MANAGED-OBJECT-TABLE-ENTRY ]
			mediumDesc	= Quorum Devices Table Entry 
			consoleHint:mediumDesc = base.modules.sun-cluster:quorumDeviceBranch.statusFolder.quorumDeviceTable.quorumDeviceTableEntry
			index		= quorumDeviceName

			quorumDeviceName = { [ use STRING MANAGED-PROPERTY ]
				mediumDesc	= Name
				consoleHint:mediumDesc = base.modules.sun-cluster:quorumDeviceBranch.statusFolder.quorumDeviceTable.quorumDeviceTableEntry.quorumDeviceName
			}

			status = { [ use STRINGREGEXP MANAGED-PROPERTY ]
				mediumDesc	= Status
				consoleHint:mediumDesc = base.modules.sun-cluster:quorumDeviceBranch.statusFolder.quorumDeviceTable.quorumDeviceTableEntry.status
			}

			currentVotes = { [ use INT MANAGED-PROPERTY ]
				mediumDesc	= Current Votes
				consoleHint:mediumDesc = base.modules.sun-cluster:quorumDeviceBranch.statusFolder.quorumDeviceTable.quorumDeviceTableEntry.currentVotes
			}
		}
	}
}

propertyFolder = { [ use MANAGED-OBJECT ]
	mediumDesc	= Property
    	consoleHint:mediumDesc = base.modules.sun-cluster:quorumDeviceBranch.propertyFolder

	quorumDeviceTable = { [ use MANAGED-OBJECT-TABLE ]
		mediumDesc	= Quorum Devices Table
		consoleHint:mediumDesc = base.modules.sun-cluster:quorumDeviceBranch.propertyFolder.quorumDeviceTable

		quorumDeviceTableEntry = { [ use MANAGED-OBJECT-TABLE-ENTRY ]
			mediumDesc	= Quorum Devices Table Entry 
			consoleHint:mediumDesc = base.modules.sun-cluster:quorumDeviceBranch.propertyFolder.quorumDeviceTable.quorumDeviceTableEntry
			index		= quorumDeviceName

			quorumDeviceName = { [ use STRING MANAGED-PROPERTY ]
				mediumDesc	= Name
				consoleHint:mediumDesc = base.modules.sun-cluster:quorumDeviceBranch.propertyFolder.quorumDeviceTable.quorumDeviceTableEntry.quorumDeviceName
			}

			quorumDevicePath = { [ use STRING MANAGED-PROPERTY ]
				mediumDesc	= Device Path
				consoleHint:mediumDesc = base.modules.sun-cluster:quorumDeviceBranch.propertyFolder.quorumDeviceTable.quorumDeviceTableEntry.quorumDevicePath
			}

			enabled = { [ use INT MANAGED-PROPERTY ]
				mediumDesc	= Enabled
				consoleHint:mediumDesc = base.modules.sun-cluster:quorumDeviceBranch.propertyFolder.quorumDeviceTable.quorumDeviceTableEntry.enabled
				dataFormat	= boolean
			}

			defaultVotes = { [ use STRING MANAGED-PROPERTY ]
				mediumDesc	= Default Votes
				consoleHint:mediumDesc = base.modules.sun-cluster:quorumDeviceBranch.propertyFolder.quorumDeviceTable.quorumDeviceTableEntry.defaultVotes
			}

			portList = { [ use STRING MANAGED-PROPERTY ]
				mediumDesc	= Port List
				consoleHint:mediumDesc = base.modules.sun-cluster:quorumDeviceBranch.propertyFolder.quorumDeviceTable.quorumDeviceTableEntry.portList
			}
		}
	}

	quorumDevicePortTable = { [ use MANAGED-OBJECT-TABLE ]
		mediumDesc	= Quorum Devices Port Table
		consoleHint:mediumDesc = base.modules.sun-cluster:quorumDeviceBranch.propertyFolder.quorumDevicePortTable

		quorumDevicePortTableEntry = { [ use MANAGED-OBJECT-TABLE-ENTRY ]
			mediumDesc	= Quorum Devices Port Table Entry 
			consoleHint:mediumDesc = base.modules.sun-cluster:quorumDeviceBranch.propertyFolder.quorumDevicePortTable.quorumDevicePortTableEntry
			index		= quorumDeviceName quorumDeviceHost

			quorumDeviceName = { [ use STRING MANAGED-PROPERTY ]
				mediumDesc	= Quorum Device Name
				consoleHint:mediumDesc = base.modules.sun-cluster:quorumDeviceBranch.propertyFolder.quorumDevicePortTable.quorumDevicePortTableEntry.quorumDeviceName
			}

			quorumDeviceHost = { [ use STRING MANAGED-PROPERTY ]
				mediumDesc	= Quorum Device Host
				consoleHint:mediumDesc = base.modules.sun-cluster:quorumDeviceBranch.propertyFolder.quorumDevicePortTable.quorumDevicePortTableEntry.quorumDeviceHost
			}

			enabled = { [ use STRING MANAGED-PROPERTY ]
				mediumDesc	= Enabled
				consoleHint:mediumDesc = base.modules.sun-cluster:quorumDeviceBranch.propertyFolder.quorumDevicePortTable.quorumDevicePortTableEntry.enabled
			}
		}
	}
}
