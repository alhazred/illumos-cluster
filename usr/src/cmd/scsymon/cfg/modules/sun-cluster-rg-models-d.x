#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#pragma ident	"@(#)sun-cluster-rg-models-d.x	1.12	08/05/20 SMI"
#
# Copyright (c) 2000 by Sun Microsystems, Inc.
# All rights reserved.
#
# cluster/src/scsymon/cfg/modules/sun-cluster-rg-models-d.x
#

# this file describes the model for Resource Group subtree
# it gets included (loaded) into the main sun-cluster-models-d.x file
[ use MANAGED-OBJECT ]
mediumDesc = Resource Groups
consoleHint:mediumDesc = base.modules.sun-cluster:rgBranch

statusFolder = { [ use MANAGED-OBJECT ]
	mediumDesc	= Status
    	consoleHint:mediumDesc = base.modules.sun-cluster:rgBranch.statusFolder

	FailoverRGBranch = { [ use MANAGED-OBJECT ]
		mediumDesc = Failover Resource Groups
		consoleHint:mediumDesc = base.modules.sun-cluster:rgBranch.statusFolder.FailoverRGBranch

		[ load sun-cluster-rg-status-d.x ]
	}

	ScalableRGBranch = { [ use MANAGED-OBJECT ]
		mediumDesc = Scalable Resource Groups
		consoleHint:mediumDesc = base.modules.sun-cluster:rgBranch.statusFolder.ScalableRGBranch

		[ load sun-cluster-rg-status-d.x ]
	}
}

propertyFolder = { [ use MANAGED-OBJECT ]
	mediumDesc	= Configuration
    	consoleHint:mediumDesc = base.modules.sun-cluster:rgBranch.propertyFolder

	FailoverRGBranch = { [ use MANAGED-OBJECT ]
		mediumDesc = Failover Resource Groups
		consoleHint:mediumDesc = base.modules.sun-cluster:rgBranch.propertyFolder.FailoverRGBranch

		[ load sun-cluster-rg-config-d.x ]
	}

	ScalableRGBranch = { [ use MANAGED-OBJECT ]
		mediumDesc = Scalable Resource Groups
		consoleHint:mediumDesc = base.modules.sun-cluster:rgBranch.propertyFolder.ScalableRGBranch

		[ load sun-cluster-rg-config-d.x ]
	}
}
