#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#pragma ident	"@(#)sun-cluster-node-models-d.x	1.7	08/05/20 SMI"
#
# Copyright (c) 1999 by Sun Microsystems, Inc.
# All rights reserved.
#
# cluster/src/scsymon/cfg/modules/sun-cluster-node-models-d.x
#

[ use MANAGED-OBJECT ]
mediumDesc = Cluster Nodes    
consoleHint:mediumDesc = base.modules.sun-cluster:nodeBranch

statusFolder = { [ use MANAGED-OBJECT ]
	mediumDesc	= Status
    	consoleHint:mediumDesc = base.modules.sun-cluster:nodeBranch.statusFolder

	nodeTable = { [ use MANAGED-OBJECT-TABLE ]
		mediumDesc	= Nodes Table
		consoleHint:mediumDesc = base.modules.sun-cluster:nodeBranch.statusFolder.nodeTable

		nodeTableEntry = { [ use MANAGED-OBJECT-TABLE-ENTRY ]
			mediumDesc	= Nodes Table Entry 
			consoleHint:mediumDesc = base.modules.sun-cluster:nodeBranch.statusFolder.nodeTable.nodeTableEntry
			index		= nodeName

			nodeName = { [ use STRING MANAGED-PROPERTY ]
				mediumDesc	= Node Name
				consoleHint:mediumDesc = base.modules.sun-cluster:nodeBranch.statusFolder.nodeTable.nodeTableEntry.nodeName
			}

			status = { [ use STRINGREGEXP MANAGED-PROPERTY ]
				mediumDesc	= Status
				consoleHint:mediumDesc = base.modules.sun-cluster:nodeBranch.statusFolder.nodeTable.nodeTableEntry.status
			}

			currentVotes = { [ use INT MANAGED-PROPERTY ]
				mediumDesc	= Current Votes
				consoleHint:mediumDesc = base.modules.sun-cluster:nodeBranch.statusFolder.nodeTable.nodeTableEntry.currentVotes
			}
		}
	}


	nodeDeviceTable = { [ use MANAGED-OBJECT-TABLE ]
		mediumDesc	= Objects Associated with Nodes
		consoleHint:mediumDesc = base.modules.sun-cluster:nodeBranch.statusFolder.nodeDeviceTable

		nodeDeviceTableEntry = { [ use MANAGED-OBJECT-TABLE-ENTRY ]
			mediumDesc	= Nodes Table Entry 
			consoleHint:mediumDesc = base.modules.sun-cluster:nodeBranch.statusFolder.nodeDeviceTable.nodeDeviceTableEntry
			index		= nodeName

			nodeName = { [ use STRING MANAGED-PROPERTY ]
				mediumDesc	= Node Name
				consoleHint:mediumDesc = base.modules.sun-cluster:nodeBranch.statusFolder.nodeDeviceTable.nodeDeviceTableEntry.nodeName
			}

			masteredDeviceGroupList = { [ use STRING MANAGED-PROPERTY ]
				mediumDesc	= Mastered Device Group List
				consoleHint:mediumDesc = base.modules.sun-cluster:nodeBranch.statusFolder.nodeDeviceTable.nodeDeviceTableEntry.masteredDeviceGroupList
			}

			masteredRGList = { [ use STRING MANAGED-PROPERTY ]
				mediumDesc	= Mastered resource Group List
				consoleHint:mediumDesc = base.modules.sun-cluster:nodeBranch.statusFolder.nodeDeviceTable.nodeDeviceTableEntry.masteredRGList
			}
		}
	}
}

propertyFolder = { [ use MANAGED-OBJECT ]
	mediumDesc	= Property
    	consoleHint:mediumDesc = base.modules.sun-cluster:nodeBranch.propertyFolder

	nodeTable = { [ use MANAGED-OBJECT-TABLE ]
		mediumDesc	= Nodes Table
		consoleHint:mediumDesc = base.modules.sun-cluster:nodeBranch.propertyFolder.nodeTable

		nodeTableEntry = { [ use MANAGED-OBJECT-TABLE-ENTRY ]
			mediumDesc	= Nodes Table Entry 
			consoleHint:mediumDesc = base.modules.sun-cluster:nodeBranch.propertyFolder.nodeTable.nodeTableEntry
			index		= nodeName

			nodeName = { [ use STRING MANAGED-PROPERTY ]
				mediumDesc	= Node Name
				consoleHint:mediumDesc = base.modules.sun-cluster:nodeBranch.propertyFolder.nodeTable.nodeTableEntry.nodeName
			}

			defaultVotes = { [ use INT MANAGED-PROPERTY ]
				mediumDesc	= Default Votes
				consoleHint:mediumDesc = base.modules.sun-cluster:nodeBranch.propertyFolder.nodeTable.nodeTableEntry.defaultVotes
			}

			privateNetHostname = { [ use STRING MANAGED-PROPERTY ]
				mediumDesc	= Private Net Hostname
				consoleHint:mediumDesc = base.modules.sun-cluster:nodeBranch.propertyFolder.nodeTable.nodeTableEntry.privateNetHostname
			}

			transportAdapterList = { [ use STRING MANAGED-PROPERTY ]
				mediumDesc	= Transport Adapter List
				consoleHint:mediumDesc = base.modules.sun-cluster:nodeBranch.propertyFolder.nodeTable.nodeTableEntry.transportAdapterList
			} 
		}
	}


	nodeDeviceTable = { [ use MANAGED-OBJECT-TABLE ]
		mediumDesc	= Objects Associated with Nodes
		consoleHint:mediumDesc = base.modules.sun-cluster:nodeBranch.propertyFolder.nodeDeviceTable

		nodeDeviceTableEntry = { [ use MANAGED-OBJECT-TABLE-ENTRY ]
			mediumDesc	= Nodes Table Entry 
			consoleHint:mediumDesc = base.modules.sun-cluster:nodeBranch.propertyFolder.nodeDeviceTable.nodeDeviceTableEntry
			index		= nodeName

			nodeName = { [ use STRING MANAGED-PROPERTY ]
				mediumDesc	= Node Name
				consoleHint:mediumDesc = base.modules.sun-cluster:nodeBranch.propertyFolder.nodeDeviceTable.nodeDeviceTableEntry.nodeName
			}

			quorumDeviceList = { [ use STRING MANAGED-PROPERTY ]
				mediumDesc	= Quorum Device List
				consoleHint:mediumDesc = base.modules.sun-cluster:nodeBranch.propertyFolder.nodeDeviceTable.nodeDeviceTableEntry.quorumDeviceList
			}

			possibleMasteredDeviceGroupList = { [ use STRING MANAGED-PROPERTY ]
				mediumDesc	= Possible Mastered Device Group List
				consoleHint:mediumDesc = base.modules.sun-cluster:nodeBranch.propertyFolder.nodeDeviceTable.nodeDeviceTableEntry.possibleMasteredDeviceGroupList
			}

			possibleMasteredRGList = { [ use STRING MANAGED-PROPERTY ]
				mediumDesc	= Possible Mastered Resource Group List
				consoleHint:mediumDesc = base.modules.sun-cluster:nodeBranch.propertyFolder.nodeDeviceTable.nodeDeviceTableEntry.possibleMasteredRGList
			}
		}
	}
}

