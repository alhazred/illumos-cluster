#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#pragma ident	"@(#)sun-cluster-rt-models-d.x	1.13	08/05/20 SMI"
#
# Copyright (c) 1999-2001 by Sun Microsystems, Inc.
# All rights reserved.
#
# cluster/src/scsymon/cfg/modules/sun-cluster-rt-methods-d.x
#

# this file describes the model for RT subtree
# it gets included (loaded) into the main sun-cluster-models-d.x file

[ use MANAGED-OBJECT ]
mediumDesc = Resource Types    	
consoleHint:mediumDesc = base.modules.sun-cluster:rtBranch

definitionFolder = { [ use MANAGED-OBJECT ]
	mediumDesc	= Definition
    	consoleHint:mediumDesc = base.modules.sun-cluster:rtBranch.definitionFolder

	genPropFolder = { [ use MANAGED-OBJECT ]
		mediumDesc	= Overview
	    	consoleHint:mediumDesc = base.modules.sun-cluster:rtBranch.definitionFolder.genPropFolder

		rtTable = { [ use MANAGED-OBJECT-TABLE ]
			mediumDesc	= Resource Types Table
    			consoleHint:mediumDesc = base.modules.sun-cluster:rtBranch.definitionFolder.genPropFolder.rtTable
	
			rtTableEntry = { [ use MANAGED-OBJECT-TABLE-ENTRY ]
				mediumDesc	= Resource Types Table Entry
				consoleHint:mediumDesc = base.modules.sun-cluster:rtBranch.definitionFolder.genPropFolder.rtTable.rtTableEntry
				index		= rtName

				rtName = { [ use STRING MANAGED-PROPERTY ]
					mediumDesc	= Name
					consoleHint:mediumDesc = base.modules.sun-cluster:rtBranch.definitionFolder.genPropFolder.rtTable.rtTableEntry.rtName
				}

				rtNodes = { [ use STRING MANAGED-PROPERTY ]
					mediumDesc	= Installed nodes
					consoleHint:mediumDesc = base.modules.sun-cluster:rtBranch.definitionFolder.genPropFolder.rtTable.rtTableEntry.rtNodes
				}

				rtDesc = { [ use STRING MANAGED-PROPERTY ]
					mediumDesc	= Description
					consoleHint:mediumDesc = base.modules.sun-cluster:rtBranch.definitionFolder.genPropFolder.rtTable.rtTableEntry.rtDesc
					dataFormat	= unicode
				}

				rtBaseDir = { [ use STRING MANAGED-PROPERTY ]
					mediumDesc	= Base Directory
					consoleHint:mediumDesc = base.modules.sun-cluster:rtBranch.definitionFolder.genPropFolder.rtTable.rtTableEntry.rtBaseDir
				}

				rtSingleInst = { [ use STRING MANAGED-PROPERTY ]
					mediumDesc	= Single Instance
					consoleHint:mediumDesc = base.modules.sun-cluster:rtBranch.definitionFolder.genPropFolder.rtTable.rtTableEntry.rtSingleInst
				}

				rtInitNodes = { [ use STRING MANAGED-PROPERTY ]
					mediumDesc	= Init Nodes
					consoleHint:mediumDesc = base.modules.sun-cluster:rtBranch.definitionFolder.genPropFolder.rtTable.rtTableEntry.rtInitNodes
				}

				rtFailover = { [ use STRING MANAGED-PROPERTY ]
					mediumDesc	= Failover
					consoleHint:mediumDesc = base.modules.sun-cluster:rtBranch.definitionFolder.genPropFolder.rtTable.rtTableEntry.rtFailover
				}

				rtSysDefType = { [ use STRING MANAGED-PROPERTY ]
					mediumDesc	= Type
					consoleHint:mediumDesc = base.modules.sun-cluster:rtBranch.definitionFolder.genPropFolder.rtTable.rtTableEntry.rtSysDefType
				}

				rtDepend = { [ use STRING MANAGED-PROPERTY ]
					mediumDesc	= Dependencies
					consoleHint:mediumDesc = base.modules.sun-cluster:rtBranch.definitionFolder.genPropFolder.rtTable.rtTableEntry.rtDepend
				}

				rtApiVersion = { [ use INT MANAGED-PROPERTY ]
					mediumDesc	= API Version
					consoleHint:mediumDesc = base.modules.sun-cluster:rtBranch.definitionFolder.genPropFolder.rtTable.rtTableEntry.rtAPIVersion
				}

				rtVersion = { [ use STRING MANAGED-PROPERTY ]
					mediumDesc	= Resource Type Version
					consoleHint:mediumDesc = base.modules.sun-cluster:rtBranch.definitionFolder.genPropFolder.rtTable.rtTableEntry.rtVersion
				}

				rtPkglist = { [ use STRING MANAGED-PROPERTY ]
					mediumDesc	= Packages
					consoleHint:mediumDesc = base.modules.sun-cluster:rtBranch.definitionFolder.genPropFolder.rtTable.rtTableEntry.rtPkglist
				}
			}
		}
	}

	methodFolder = { [ use MANAGED-OBJECT ]
		mediumDesc	= Method Pathname
	    	consoleHint:mediumDesc = base.modules.sun-cluster:rtBranch.definitionFolder.methodFolder
	
		rtMethodTable = { [ use MANAGED-OBJECT-TABLE ]
			mediumDesc	= Resource Type Methods Table
			consoleHint:mediumDesc = base.modules.sun-cluster:rtBranch.definitionFolder.methodFolder.rtMethodTable

			rtMethodTableEntry = { [ use MANAGED-OBJECT-TABLE-ENTRY ]
				mediumDesc	= Resource Type Methods Table Entry
				consoleHint:mediumDesc = base.modules.sun-cluster:rtBranch.definitionFolder.methodFolder.rtMethodTable.rtMethodTableEntry
				index		= rtName
	
				rtName	= { [ use STRING MANAGED-PROPERTY ]
					mediumDesc	= Resource Type Name
					consoleHint:mediumDesc = base.modules.sun-cluster:rtBranch.definitionFolder.methodFolder.rtMethodTable.rtMethodTableEntry.rtName
				}

				[ load sun-cluster-rt-method-d.x ]
			}
		}
	}

	paramFolder = { [ use MANAGED-OBJECT ]
		mediumDesc	= Properties
	    	consoleHint:mediumDesc = base.modules.sun-cluster:rtBranch.definitionFolder.paramFolder

		rtParamTable = { [ use MANAGED-OBJECT-TABLE ]
			mediumDesc	= Resource Type Parameters Table
			consoleHint:mediumDesc = base.modules.sun-cluster:rtBranch.definitionFolder.paramFolder.rtParamTable

			rtParamTableEntry = { [ use MANAGED-OBJECT-TABLE-ENTRY ]
				mediumDesc	= Resource Type Parameters Table Entry
				consoleHint:mediumDesc = base.modules.sun-cluster:rtBranch.definitionFolder.paramFolder.rtParamTable.rtParamTableEntry
				index		= rtName rtPropName

				rtName	= { [ use STRING MANAGED-PROPERTY ]
					mediumDesc	= Resource Type Name
					consoleHint:mediumDesc = base.modules.sun-cluster:rtBranch.definitionFolder.paramFolder.rtParamTable.rtParamTableEntry.rtName
				}

				rtPropName	= { [ use STRING MANAGED-PROPERTY ]
					mediumDesc	= Property Name
					consoleHint:mediumDesc = base.modules.sun-cluster:rtBranch.definitionFolder.paramFolder.rtParamTable.rtParamTableEntry.rtPropName
				}

				[ load sun-cluster-rt-param-d.x ]
			}
		}
	}
}

rsFolder = { [ use MANAGED-OBJECT ]
	mediumDesc	= Resource List
    	consoleHint:mediumDesc = base.modules.sun-cluster:rtBranch.rsFolder

	rsTable = { [ use MANAGED-OBJECT-TABLE ]
		mediumDesc	= Resource List
		consoleHint:mediumDesc = base.modules.sun-cluster:rtBranch.rsFolder.rsTable

		resourceTableEntry = { [ use MANAGED-OBJECT-TABLE-ENTRY ]
			mediumDesc	= Resource Table Entry
			consoleHint:mediumDesc = base.modules.sun-cluster:rtBranch.rsFolder.rsTable.rsTableEntry
			index		= rtName rgName rsName

			rtName	= { [ use STRING MANAGED-PROPERTY ]
				mediumDesc	= Resource Type Name
				consoleHint:mediumDesc = base.modules.sun-cluster:rtBranch.rsFolder.rsTable.rsTableEntry.rtName
			}

			rgName	= { [ use STRING MANAGED-PROPERTY ]
				mediumDesc	= Resource Group Name
				consoleHint:mediumDesc = base.modules.sun-cluster:rtBranch.rsFolder.rsTable.rsTableEntry.rgName
			}

			rsName	= { [ use STRING MANAGED-PROPERTY ]
				mediumDesc	= Resource Name
				consoleHint:mediumDesc = base.modules.sun-cluster:rtBranch.rsFolder.rsTable.rsTableEntry.rsName
			}
			
		}
	}
}
