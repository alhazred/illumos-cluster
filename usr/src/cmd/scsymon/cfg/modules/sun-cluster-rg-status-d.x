#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#pragma ident	"@(#)sun-cluster-rg-status-d.x	1.3	08/05/20 SMI"
#
# Copyright (c) 2000-2001 by Sun Microsystems, Inc.
# All rights reserved.
#
# cluster/src/scsymon/cfg/modules/sun-cluster-rg-status-d.x
#

# this file describes the model for Resource Group Status subtree
# it gets included (loaded) into the main sun-cluster-rg-models-d.x file

rgStatusTable = { [ use MANAGED-OBJECT-TABLE ]
	mediumDesc	= Resource Group Status
	consoleHint:mediumDesc = base.modules.sun-cluster:rgBranch.statusFolder.rgStatusTable
	
	rgStatusTableEntry = { [ use MANAGED-OBJECT-TABLE-ENTRY ]
		mediumDesc	= Resource Group Status Table Entry
		consoleHint:mediumDesc = base.modules.sun-cluster:rgBranch.statusFolder.rgStatusTable.rgStatusTableEntry
		index		= rgName rgPrim

#		rowstatus needed to manage rows in the table
		rowstatus = { [ use GLOBROWNODE ROWSTATUS MANAGED-PROPERTY]
			mediumDesc  = Row Status
			consoleHint:hidden = true 
			consoleHint:mediumDesc = base.modules.sun-cluster:rgBranch.statusFolder.rgStatusTable.rgStatusTableEntry.rowstatus
		}

		rgName	= { [ use GLOBROWNODE STRING MANAGED-PROPERTY ]
			mediumDesc	= Name
			consoleHint:mediumDesc = base.modules.sun-cluster:rgBranch.statusFolder.rgStatusTable.rgStatusTableEntry.rgName
	    		dataFormat	= instance 
		}

		rgPrim	= { [ use GLOBROWNODE STRING MANAGED-PROPERTY ]
			mediumDesc	= Primary
			consoleHint:mediumDesc = base.modules.sun-cluster:rgBranch.statusFolder.rgStatusTable.rgStatusTableEntry.rgPrim
	    		dataFormat	= instance 
		}

		rgPrimStatus	= { [ use GLOBROWNODE STRING MANAGED-PROPERTY ]
			mediumDesc	= Primary Status
			consoleHint:mediumDesc = base.modules.sun-cluster:rgBranch.statusFolder.rgStatusTable.rgStatusTableEntry.rgPrimStatus
		}

		rgState	= { [ use GLOBROWNODE STRINGREGEXP MANAGED-PROPERTY ]
			mediumDesc	= Resource Group State
			consoleHint:mediumDesc = base.modules.sun-cluster:rgBranch.statusFolder.rgStatusTable.rgStatusTableEntry.rgState
		}
	}
}

rsStatusTable = { [ use MANAGED-OBJECT-TABLE ]
	mediumDesc	= Resource Status
	consoleHint:mediumDesc = base.modules.sun-cluster:rgBranch.statusFolder.rsStatusTable
	
	rsStatusTableEntry = { [ use MANAGED-OBJECT-TABLE-ENTRY ]
		mediumDesc	= Resource Status Table Entry
		consoleHint:mediumDesc = base.modules.sun-cluster:rgBranch.statusFolder.rsStatusTable.rsStatusTableEntry
		index		= rgName rsName rsPrim

#		rowstatus needed to manage rows in the table
		rowstatus = { [ use ROWSTATUS MANAGED-PROPERTY]
			mediumDesc  = Row Status
			consoleHint:hidden = true 
			consoleHint:mediumDesc = base.modules.sun-cluster:rgBranch.statusFolder.rsStatusTable.rsStatusTableEntry.rowstatus
		}

		rgName	= { [ use STRING MANAGED-PROPERTY ]
			mediumDesc	= Resource Group Name
			consoleHint:mediumDesc = base.modules.sun-cluster:rgBranch.statusFolder.rsStatusTable.rsStatusTableEntry.rgName
	    		dataFormat	= instance 
		}

		rsName	= { [ use STRING MANAGED-PROPERTY ]
			mediumDesc	= Resource Name
			consoleHint:mediumDesc = base.modules.sun-cluster:rgBranch.statusFolder.rsStatusTable.rsStatusTableEntry.rsName
	    		dataFormat	= instance 
		}

		rsPrim	= { [ use STRING MANAGED-PROPERTY ]
			mediumDesc	= Primary
			consoleHint:mediumDesc = base.modules.sun-cluster:rgBranch.statusFolder.rsStatusTable.rsStatusTableEntry.rsPrim
	    		dataFormat	= instance 
		}

		rsPrimStatus	= { [ use STRING MANAGED-PROPERTY ]
			mediumDesc	= Primary Status
			consoleHint:mediumDesc = base.modules.sun-cluster:rgBranch.statusFolder.rsStatusTable.rsStatusTableEntry.rsPrimStatus
		}

		rsFMStatus	= { [ use STRINGREGEXP MANAGED-PROPERTY ]
			mediumDesc	= FM Reported Status
			consoleHint:mediumDesc = base.modules.sun-cluster:rgBranch.statusFolder.rsStatusTable.rsStatusTableEntry.rsFMStatus
		}

		rsRGMState	= { [ use STRINGREGEXP MANAGED-PROPERTY ]
			mediumDesc	= RGM Reported State
			consoleHint:mediumDesc = base.modules.sun-cluster:rgBranch.statusFolder.rsStatusTable.rsStatusTableEntry.rsRGMState
		}
	}
}
