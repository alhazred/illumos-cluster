#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#pragma ident	"@(#)sun-cluster-d.x	1.55	08/05/20 SMI"
#
# Copyright 2000-2007 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# cluster/src/scsymon/cfg/modules/sun-cluster-d.x
#

[ use MANAGED-MODULE ]
[ load sun-cluster-m.x ]
[ requires template sun-cluster-models-d ]

_procedures = { [ use PROC ]
	[ source sun-cluster-d.prc ]
}

_services = { [ use SERVICE ]
	sh = {
		command = "pipe://localhost//bin//sh;transport=shell"
		max= 1
	}
}

generalBranch = { [ use templates.sun-cluster-models-d.generalBranch _procedures ]

	consoleHint:smallIcon(DFT) = stdimages/sun-cluster_16.gif
	consoleHint:largeIcon(DFT) = topoimages/sun-cluster_32.gif

	consoleHint:commands = help
	consoleHint:commandLabel(help) = base.modules.sun-cluster:help
	consoleHint:commandSpec(help) = launchApp com.sun.symon.apps.generic.help.SMHelpBrowser sc-gen-help		

	propertyFolder =  { [ use templates.sun-cluster-models-d.generalBranch.propertyFolder _procedures ]

		consoleHint:smallIcon(DFT) = stdimages/cfg_16.gif
		consoleHint:largeIcon(DFT) = topoimages/cfg_32.gif

		consoleHint:commands = help
		consoleHint:commandLabel(help) = base.modules.sun-cluster:help
		consoleHint:commandSpec(help) = launchApp com.sun.symon.apps.generic.help.SMHelpBrowser sc-gen-prop-help

		type	= active

		refreshService	= _services.sh
		refreshCommand	= /usr/bin/nice /usr/cluster/lib/scsymon/scsymon_clnt cluster_config
		refreshInterval	= 600
		initInterval	= 1
	}

	statusFolder =  { [ use templates.sun-cluster-models-d.generalBranch.statusFolder _procedures ]

		type	= active

		consoleHint:smallIcon(DFT) = stdimages/status_16.gif
		consoleHint:largeIcon(DFT) = topoimages/status_32.gif

		consoleHint:commands = help
		consoleHint:commandLabel(help) = base.modules.sun-cluster:help
		consoleHint:commandSpec(help) = launchApp com.sun.symon.apps.generic.help.SMHelpBrowser sc-gen-stat-help

		refreshService	= _services.sh
		refreshCommand	= /usr/bin/nice /usr/cluster/lib/scsymon/scsymon_clnt cluster_status
		refreshInterval	= 120
		initInterval	= 1
	}
}

nodeBranch = { [ use templates.sun-cluster-models-d.nodeBranch _procedures ]

	consoleHint:smallIcon(DFT) = stdimages/node_16.gif
	consoleHint:largeIcon(DFT) = topoimages/node_32.gif

	consoleHint:commands = help
	consoleHint:commandLabel(help) = base.modules.sun-cluster:help
	consoleHint:commandSpec(help) = launchApp com.sun.symon.apps.generic.help.SMHelpBrowser sc-nodes-help

	propertyFolder =  { [ use templates.sun-cluster-models-d.nodeBranch.propertyFolder _procedures ]

		consoleHint:smallIcon(DFT) = stdimages/cfg_16.gif
		consoleHint:largeIcon(DFT) = topoimages/cfg_32.gif

		consoleHint:commands = help
		consoleHint:commandLabel(help) = base.modules.sun-cluster:help
		consoleHint:commandSpec(help) = launchApp com.sun.symon.apps.generic.help.SMHelpBrowser sc-nodes-prop-help

		nodeTable = { [ use _procedures ]
			type		= active

			refreshService	= _services.sh
			refreshCommand	= /usr/bin/nice /usr/cluster/lib/scsymon/scsymon_clnt node_config
			refreshInterval	= 600
			initInterval	= 1
		}

		nodeDeviceTable = { [ use _procedures ]
			type		= active

			refreshService	= _services.sh
			refreshCommand	= /usr/bin/nice /usr/cluster/lib/scsymon/scsymon_clnt nodedev_config
			refreshInterval	= 600
			initInterval	= 1
		}
	}

	statusFolder =  { [ use templates.sun-cluster-models-d.nodeBranch.statusFolder _procedures ]

		consoleHint:smallIcon(DFT) = stdimages/status_16.gif
		consoleHint:largeIcon(DFT) = topoimages/status_32.gif

		consoleHint:commands = help
		consoleHint:commandLabel(help) = base.modules.sun-cluster:help
		consoleHint:commandSpec(help) = launchApp com.sun.symon.apps.generic.help.SMHelpBrowser sc-nodes-stat-help

		nodeTable = { [ use _procedures ]
			type		= active

			refreshService	= _services.sh
			refreshCommand	= /usr/bin/nice /usr/cluster/lib/scsymon/scsymon_clnt node_status
			refreshInterval	= 120
			initInterval	= 1
		}

		nodeDeviceTable = { [ use _procedures ]
			type		= active

			refreshService	= _services.sh
			refreshCommand	= /usr/bin/nice /usr/cluster/lib/scsymon/scsymon_clnt nodedev_status
			refreshInterval	= 120
			initInterval	= 1
		}
	}
}

deviceGroupBranch = { [ use templates.sun-cluster-models-d.deviceGroupBranch _procedures ]

	consoleHint:smallIcon(DFT) = stdimages/devGrp_16.gif
	consoleHint:largeIcon(DFT) = topoimages/devGrp_32.gif

	consoleHint:commands = help
	consoleHint:commandLabel(help) = base.modules.sun-cluster:help
	consoleHint:commandSpec(help) = launchApp com.sun.symon.apps.generic.help.SMHelpBrowser sc-devGrp-help

	propertyFolder =  { [ use templates.sun-cluster-models-d.deviceGroupBranch.propertyFolder _procedures ]

		consoleHint:smallIcon(DFT) = stdimages/cfg_16.gif
		consoleHint:largeIcon(DFT) = topoimages/cfg_32.gif

		consoleHint:commands = help
		consoleHint:commandLabel(help) = base.modules.sun-cluster:help
		consoleHint:commandSpec(help) = launchApp com.sun.symon.apps.generic.help.SMHelpBrowser sc-devGrp-prop-help

		deviceGroupTable = { [ use _procedures ]
			type		= active

			refreshService	= _services.sh
			refreshCommand	= /usr/bin/nice /usr/cluster/lib/scsymon/scsymon_clnt devgrp_config
			refreshInterval	= 600
			initInterval	= 1

			deviceGroupTableEntry = { [ use _procedures ]
				failback	= { [ use _procedures ]
					getFilter	= yesnoFilter
				}

				nodeOrder	=  { [ use _procedures ]
					getFilter	= yesnoFilter
				}
			}
		}	
	}

	statusFolder =  { [ use templates.sun-cluster-models-d.deviceGroupBranch.statusFolder _procedures ]

		consoleHint:smallIcon(DFT) = stdimages/status_16.gif
		consoleHint:largeIcon(DFT) = topoimages/status_32.gif

		consoleHint:commands = help
		consoleHint:commandLabel(help) = base.modules.sun-cluster:help
		consoleHint:commandSpec(help) = launchApp com.sun.symon.apps.generic.help.SMHelpBrowser sc-devGrp-stat-help

		deviceGroupTable = { [ use _procedures ]
			type		= active

			refreshService	= _services.sh
			refreshCommand	= /usr/bin/nice /usr/cluster/lib/scsymon/scsymon_clnt devgrp_status
			refreshInterval	= 120
			initInterval	= 1
		}	

		replicaTable = { [ use _procedures ]
			type		= active

			refreshService	= _services.sh
			refreshCommand	= /usr/bin/nice /usr/cluster/lib/scsymon/scsymon_clnt replica_status
			refreshInterval	= 120
			initInterval	= 1
		}	
	}
}

quorumDeviceBranch = { [ use templates.sun-cluster-models-d.quorumDeviceBranch _procedures ]

	consoleHint:smallIcon(DFT) = stdimages/quorum_16.gif
	consoleHint:largeIcon(DFT) = topoimages/quorum_32.gif

	consoleHint:commands = help
	consoleHint:commandLabel(help) = base.modules.sun-cluster:help
	consoleHint:commandSpec(help) = launchApp com.sun.symon.apps.generic.help.SMHelpBrowser sc-quorum-help

	propertyFolder =  { [ use templates.sun-cluster-models-d.quorumDeviceBranch.propertyFolder _procedures ]

		consoleHint:smallIcon(DFT) = stdimages/cfg_16.gif
		consoleHint:largeIcon(DFT) = topoimages/cfg_32.gif

		consoleHint:commands = help
		consoleHint:commandLabel(help) = base.modules.sun-cluster:help
		consoleHint:commandSpec(help) = launchApp com.sun.symon.apps.generic.help.SMHelpBrowser sc-quorum-prop-help

		quorumDeviceTable = { [ use _procedures ]
			type		= active

			refreshService	= _services.sh
			refreshCommand	= /usr/bin/nice /usr/cluster/lib/scsymon/scsymon_clnt qdev_config
			refreshInterval	= 600
			initInterval	= 1

			quorumDeviceTableEntry = { [ use _procedures ]
				enabled	= { [ use _procedures ]
					getFilter	= yesnoFilter
				}
			}
		}

		quorumDevicePortTable = { [ use _procedures ]
			type		= active

			refreshService	= _services.sh
			refreshCommand	= /usr/bin/nice /usr/cluster/lib/scsymon/scsymon_clnt qdevport_config
			refreshInterval	= 600
			initInterval	= 1

			quorumDevicePortTableEntry = { [ use _procedures ]
				enabled	= { [ use _procedures ]
					getFilter	= yesnoFilter
				}
			}
		}
	}

	statusFolder =  { [ use templates.sun-cluster-models-d.quorumDeviceBranch.statusFolder _procedures ]

		consoleHint:smallIcon(DFT) = stdimages/status_16.gif
		consoleHint:largeIcon(DFT) = topoimages/status_32.gif

		consoleHint:commands = help
		consoleHint:commandLabel(help) = base.modules.sun-cluster:help
		consoleHint:commandSpec(help) = launchApp com.sun.symon.apps.generic.help.SMHelpBrowser sc-quorum-stat-help

		quorumDeviceTable = { [ use _procedures ]
			type		= active

			refreshService	= _services.sh
			refreshCommand	= /usr/bin/nice /usr/cluster/lib/scsymon/scsymon_clnt qdev_status
			refreshInterval	= 120
			initInterval	= 1
		}
	}
}

transportBranch = { [ use templates.sun-cluster-models-d.transportBranch _procedures ]

	consoleHint:smallIcon(DFT) = stdimages/transp_16.gif
	consoleHint:largeIcon(DFT) = topoimages/transp_32.gif

	consoleHint:commands = help
	consoleHint:commandLabel(help) = base.modules.sun-cluster:help
	consoleHint:commandSpec(help) = launchApp com.sun.symon.apps.generic.help.SMHelpBrowser sc-transp-help

	propertyFolder =  { [ use templates.sun-cluster-models-d.transportBranch.propertyFolder _procedures ]

		consoleHint:smallIcon(DFT) = stdimages/cfg_16.gif
		consoleHint:largeIcon(DFT) = topoimages/cfg_32.gif

		consoleHint:commands = help
		consoleHint:commandLabel(help) = base.modules.sun-cluster:help
		consoleHint:commandSpec(help) = launchApp com.sun.symon.apps.generic.help.SMHelpBrowser sc-transp-prop-help

		adapterTable = { [ use _procedures ]
			type		= active

			refreshService	= _services.sh
			refreshCommand	= /usr/bin/nice /usr/cluster/lib/scsymon/scsymon_clnt tr_adapter_config
			refreshInterval	= 600
			initInterval	= 1

			adapterTableEntry = { [ use _procedures ]
				enabled	= { [ use _procedures ]
					getFilter	= yesnoFilter
				}
			}
		}

		junctionTable = { [ use _procedures ]
			type		= active

			refreshService	= _services.sh
			refreshCommand	= /usr/bin/nice /usr/cluster/lib/scsymon/scsymon_clnt switch_config
			refreshInterval	= 600
			initInterval	= 1

			junctionTableEntry = { [ use _procedures ]
				enabled	= { [ use _procedures ]
					getFilter	= yesnoFilter
				}
			}
		}

		cableTable = { [ use _procedures ]
			type		= active

			refreshService	= _services.sh
			refreshCommand	= /usr/bin/nice /usr/cluster/lib/scsymon/scsymon_clnt cable_config
			refreshInterval	= 600
			initInterval	= 1

			cableTableEntry = { [ use _procedures ]
				enabled	= { [ use _procedures ]
					getFilter	= yesnoFilter
				}
			}
		}
	}

	statusFolder =  { [ use templates.sun-cluster-models-d.transportBranch.statusFolder _procedures ]

		consoleHint:smallIcon(DFT) = stdimages/status_16.gif
		consoleHint:largeIcon(DFT) = topoimages/status_32.gif


		consoleHint:commands = help
		consoleHint:commandLabel(help) = base.modules.sun-cluster:help
		consoleHint:commandSpec(help) = launchApp com.sun.symon.apps.generic.help.SMHelpBrowser sc-transp-stat-help

		pathTable = { [ use _procedures ]
			type		= active

			refreshService	= _services.sh
			refreshCommand	= /usr/bin/nice /usr/cluster/lib/scsymon/scsymon_clnt path_status
			refreshInterval	= 120
			initInterval	= 1
		}
	}
}

rgBranch = { [ use templates.sun-cluster-models-d.rgBranch _procedures ]
 
	consoleHint:smallIcon(DFT) = stdimages/rg_16.gif
	consoleHint:largeIcon(DFT) = topoimages/rg_32.gif	

	consoleHint:commands = help
	consoleHint:commandLabel(help) = base.modules.sun-cluster:help
	consoleHint:commandSpec(help) = launchApp com.sun.symon.apps.generic.help.SMHelpBrowser sc-rg-help

	propertyFolder = { [ use templates.sun-cluster-models-d.rgBranch.propertyFolder _procedures ]
		consoleHint:smallIcon(DFT) = stdimages/cfg_16.gif
		consoleHint:largeIcon(DFT) = topoimages/cfg_32.gif

		consoleHint:commands = help
		consoleHint:commandLabel(help) = base.modules.sun-cluster:help
		consoleHint:commandSpec(help) = launchApp com.sun.symon.apps.generic.help.SMHelpBrowser sc-rg-prop-help

		FailoverRGBranch = { [ use templates.sun-cluster-models-d.rgBranch.propertyFolder.FailoverRGBranch _procedures ]
			consoleHint:smallIcon(DFT) = stdimages/rgFO_16.gif
			consoleHint:largeIcon(DFT) = topoimages/rgFO_32.gif

			consoleHint:commands = help
			consoleHint:commandLabel(help) = base.modules.sun-cluster:help
			consoleHint:commandSpec(help) = launchApp com.sun.symon.apps.generic.help.SMHelpBrowser sc-fo-prop-help

	            	createNode = { [ use _procedures ]
				type            = derived
				access		= rw

				refreshService  = _internal
				refreshCommand  = setValue 0 ""
				refreshInterval = 0

				validateActions(pre) = *clear
				validateService(clear) = _internal
				validateCommand(clear) = is_empty

				setActions      = *create
				shellService    = _services.sh
				setService(create) = _internal
				setCommand(create) = createRG %value

				userAccess(%adminUsers,read) = noauth
				userAccess(%adminUsers,write) = noauth
				groupAccess(%adminGroups,read) = noauth
				groupAccess(%adminGroups,write) = noauth
	                }

			statusNode = { [ use _procedures ]
				type            = derived
				access		= rw

				refreshService  = _internal
				refreshCommand  = setValue 0 ""
				refreshInterval = 0

				userAccess(%adminUsers,read) = noauth
				userAccess(%adminUsers,write) = noauth
				groupAccess(%adminGroups,read) = noauth
				groupAccess(%adminGroups,write) = noauth
	               	}

			rgPropertyFolder = { [ use templates.sun-cluster-models-d.rgBranch.propertyFolder.FailoverRGBranch.rgPropertyFolder _procedures ] 

				consoleHint:smallIcon(DFT) = stdimages/cfg_16.gif
				consoleHint:largeIcon(DFT) = topoimages/cfg_32.gif	

				consoleHint:commands =  help
				consoleHint:commandLabel(help) = base.modules.sun-cluster:help
				consoleHint:commandSpec(help) = launchApp com.sun.symon.apps.generic.help.SMHelpBrowser sc-fo-prop-help
			
				adhocCommand(delRG) = probeserver -u root -c /usr/cluster/bin/scrgadm -r -g %fragment
#				adhocCommand(delRG) = probeserver -u root /bin/sh -c /usr/cluster/bin/scrgadm -r -g %fragment && echo "Command ok"

				adhocCommand(onlineRG) = probeserver -u root -c /usr/cluster/bin/scswitch -Z -g %fragment
				adhocCommand(offlineRG) = probeserver -u root -c /usr/cluster/bin/scswitch -F -g %fragment

				rgTable = {  [ use _procedures ]
					type		= active
					access		= rw

					refreshService	= _services.sh
					refreshCommand	= /usr/bin/nice /usr/cluster/lib/scsymon/scsymon_clnt FORG_RG_config
					refreshInterval	= 600
					initInterval	= 1
				
					rgTableEntry =  {  [ use _procedures ]			
					   	rowstatus = { [ use _procedures ]
					   	}
						consoleHint:tableCommands = onlineRG offlineRG delRG

						consoleHint:commandLabel(onlineRG) = base.modules.sun-cluster:onlineRG
						consoleHint:commandSpec(onlineRG) = probeview %windowID snmp://%targetHost:%targetPort/mod/sun-cluster/rgBranch/propertyFolder/FailoverRGBranch/rgPropertyFolder?runadhoccommand.onlineRG %targetFragment
						consoleHint:commandLabel(offlineRG) = base.modules.sun-cluster:offlineRG
						consoleHint:commandSpec(offlineRG) = probeview %windowID snmp://%targetHost:%targetPort/mod/sun-cluster/rgBranch/propertyFolder/FailoverRGBranch/rgPropertyFolder?runadhoccommand.offlineRG %targetFragment
							
						consoleHint:commandLabel(delRG) = base.modules.sun-cluster:delRG
						consoleHint:commandSpec(delRG) = probeview %windowID snmp://%targetHost:%targetPort/mod/sun-cluster/rgBranch/propertyFolder/FailoverRGBranch/rgPropertyFolder?runadhoccommand.delRG %targetFragment
						
						rgFailbackFlag = { [ use _procedures ]
							getFilter	= yesnoFilter
						}

						netDepend = { [ use _procedures ]
							getFilter	= yesnoFilter
						}				
					}
				}
			}


			rsPropertyFolder = { [ use templates.sun-cluster-models-d.rgBranch.propertyFolder.FailoverRGBranch.rsPropertyFolder _procedures ] 

				consoleHint:smallIcon(DFT) = stdimages/rCfg_16.gif
				consoleHint:largeIcon(DFT) = topoimages/rCfg_32.gif	

				consoleHint:commands = help
				consoleHint:commandLabel(help) = base.modules.sun-cluster:help
				consoleHint:commandSpec(help) = launchApp com.sun.symon.apps.generic.help.SMHelpBrowser sc-fo-resources-help

				rsGenPropFolder = { [ use templates.sun-cluster-models-d.rgBranch.propertyFolder.FailoverRGBranch.rsPropertyFolder.rsGenPropFolder _procedures ]

					consoleHint:smallIcon(DFT) = stdimages/genProp_16.gif
					consoleHint:largeIcon(DFT) = topoimages/genProp_32.gif	

					consoleHint:commands = help
					consoleHint:commandLabel(help) = base.modules.sun-cluster:help
					consoleHint:commandSpec(help) = launchApp com.sun.symon.apps.generic.help.SMHelpBrowser sc-fo-resources-help

					adhocCommand(enableR) = probeserver -u root -c /usr/cluster/bin/scswitch -e -j \[ get_index_part  %fragment 1 \]
					adhocCommand(disableR) = probeserver -u root -c /usr/cluster/bin/scswitch -n -j \[ get_index_part %fragment 1 \]
					adhocCommand(delR) = probeserver -u root -c /usr/cluster/bin/scrgadm -r -j \[ get_index_part  %fragment 1 \]
					
					rsTable = { [ use _procedures ]
						type		= active
						access		= rw											
						refreshService	= _services.sh
						refreshCommand	= /usr/bin/nice /usr/cluster/lib/scsymon/scsymon_clnt FORG_RS_config
						refreshInterval = 600
						initInterval	= 1

						rsTableEntry = { [ use _procedures ]
						
							consoleHint:tableCommands = delR enableR disableR

							consoleHint:commandLabel(enableR) = base.modules.sun-cluster:enableR
							consoleHint:commandSpec(enableR) = probeview %windowID snmp://%targetHost:%targetPort/mod/sun-cluster/rgBranch/propertyFolder/ScalableRGBranch/rsPropertyFolder/rsGenPropFolder?runadhoccommand.enableR %targetFragment

							consoleHint:commandLabel(disableR) = base.modules.sun-cluster:disableR
							consoleHint:commandSpec(disableR) = probeview %windowID snmp://%targetHost:%targetPort/mod/sun-cluster/rgBranch/propertyFolder/ScalableRGBranch/rsPropertyFolder/rsGenPropFolder?runadhoccommand.disableR %targetFragment

							consoleHint:commandLabel(delR) = base.modules.sun-cluster:delR
							consoleHint:commandSpec(delR) = probeview %windowID snmp://%targetHost:%targetPort/mod/sun-cluster/rgBranch/propertyFolder/FailoverRGBranch/rsPropertyFolder/rsGenPropFolder?runadhoccommand.delR %targetFragment

							rsEnabled	= { [ use _procedures ]
								getFilter	= onoffFilter
							}

							rsMonitored	= { [ use _procedures ]
								getFilter	= yesnoFilter
							}
						}
					}
				}

				rsComPropFolder = { [ use templates.sun-cluster-models-d.rgBranch.propertyFolder.FailoverRGBranch.rsPropertyFolder.rsComPropFolder _procedures ]

					consoleHint:smallIcon(DFT) = stdimages/sysdefProp_16.gif
					consoleHint:largeIcon(DFT) = topoimages/sysdefProp_32.gif	

					consoleHint:commands = help
					consoleHint:commandLabel(help) = base.modules.sun-cluster:help
					consoleHint:commandSpec(help) = launchApp com.sun.symon.apps.generic.help.SMHelpBrowser sc-fo-resources-help

					rsComPropTable = { [ use _procedures ]
						type		= active

						refreshService	= _services.sh
						refreshCommand	= /usr/bin/nice /usr/cluster/lib/scsymon/scsymon_clnt FORG_RS_com_prop_config
						refreshInterval	= 600
						initInterval	= 1
					}
				}

				rsExtPropFolder = { [ use templates.sun-cluster-models-d.rgBranch.propertyFolder.FailoverRGBranch.rsPropertyFolder.rsExtPropFolder _procedures ]

					consoleHint:smallIcon(DFT) = stdimages/extProp_16.gif
					consoleHint:largeIcon(DFT) = topoimages/extProp_32.gif	

					consoleHint:commands = help
					consoleHint:commandLabel(help) = base.modules.sun-cluster:help
					consoleHint:commandSpec(help) = launchApp com.sun.symon.apps.generic.help.SMHelpBrowser sc-fo-resources-help

					rsExtPropTable = { [ use _procedures ]
						type		= active

						refreshService	= _services.sh
						refreshCommand	= /usr/bin/nice /usr/cluster/lib/scsymon/scsymon_clnt FORG_RS_ext_prop_config
						refreshInterval	= 600
						initInterval	= 1
					}
				}

				rsTimeoutFolder = { [ use templates.sun-cluster-models-d.rgBranch.propertyFolder.FailoverRGBranch.rsPropertyFolder.rsTimeoutFolder _procedures ] 

					consoleHint:smallIcon(DFT) = stdimages/methodtimeout_16.gif
					consoleHint:largeIcon(DFT) = topoimages/methodtimeout_32.gif	

					consoleHint:commands = help
					consoleHint:commandLabel(help) = base.modules.sun-cluster:help
					consoleHint:commandSpec(help) = launchApp com.sun.symon.apps.generic.help.SMHelpBrowser sc-fo-method-help

					rsTimeoutTable = { [ use _procedures ]
						type		= active

						refreshService	= _services.sh
						refreshCommand	= /usr/bin/nice /usr/cluster/lib/scsymon/scsymon_clnt FORG_RS_timeout_config
						refreshInterval	= 600
						initInterval	= 1
					}
				}
			}
		}

		ScalableRGBranch = { [ use templates.sun-cluster-models-d.rgBranch.propertyFolder.ScalableRGBranch _procedures ]
			consoleHint:smallIcon(DFT) = stdimages/rgSS_16.gif
			consoleHint:largeIcon(DFT) = topoimages/rgSS_32.gif

			consoleHint:commands = help
			consoleHint:commandLabel(help) = base.modules.sun-cluster:help
			consoleHint:commandSpec(help) = launchApp com.sun.symon.apps.generic.help.SMHelpBrowser sc-ss-prop-help

	            	createNode = { [ use _procedures ]
				type            = derived
				access		= rw

				refreshService  = _internal
				refreshCommand  = setValue 0 ""
				refreshInterval = 0

				validateActions(pre) = *clear
				validateService(clear) = _internal
				validateCommand(clear) = is_empty

				setActions      = *create
				shellService    = _services.sh
				setService(create) = _internal
				setCommand(create) = createRG %value

				userAccess(%adminUsers,read) = noauth
				userAccess(%adminUsers,write) = noauth
				groupAccess(%adminGroups,read) = noauth
				groupAccess(%adminGroups,write) = noauth
	                }

	                statusNode = { [ use _procedures ]
				type            = derived
				access		= rw

				refreshService  = _internal
				refreshCommand  = setValue 0 ""
				refreshInterval = 0

				userAccess(%adminUsers,read) = noauth
				userAccess(%adminUsers,write) = noauth
				groupAccess(%adminGroups,read) = noauth
				groupAccess(%adminGroups,write) = noauth
	               	}

			rgPropertyFolder = { [ use templates.sun-cluster-models-d.rgBranch.propertyFolder.ScalableRGBranch.rgPropertyFolder _procedures ] 

				consoleHint:smallIcon(DFT) = stdimages/cfg_16.gif
				consoleHint:largeIcon(DFT) = topoimages/cfg_32.gif

				consoleHint:commands = help
				consoleHint:commandLabel(help) = base.modules.sun-cluster:help
				consoleHint:commandSpec(help) = launchApp com.sun.symon.apps.generic.help.SMHelpBrowser sc-ss-prop-help

				adhocCommand(delRG) = probeserver -u root -c /usr/cluster/bin/scrgadm -r -g %fragment
				adhocCommand(onlineRG) = probeserver -u root -c /usr/cluster/bin/scswitch -Z -g %fragment
				adhocCommand(offlineRG) = probeserver -u root -c /usr/cluster/bin/scswitch -F -g %fragment

				rgTable = {  [ use _procedures ]
					type		= active

					refreshService	= _services.sh
					refreshCommand	= /usr/bin/nice /usr/cluster/lib/scsymon/scsymon_clnt SCRG_RG_config
					refreshInterval	= 600
					initInterval	= 1
		
					rgTableEntry =  {  [ use _procedures ]			
					   	rowstatus = { [ use _procedures ]
					   	}
						consoleHint:tableCommands = delRG onlineRG offlineRG
						consoleHint:commandLabel(delRG) = base.modules.sun-cluster:delRG
						consoleHint:commandSpec(delRG) = probeview %windowID snmp://%targetHost:%targetPort/mod/sun-cluster/rgBranch/propertyFolder/ScalableRGBranch/rgPropertyFolder?runadhoccommand.delRG %targetFragment

						consoleHint:commandLabel(onlineRG) = base.modules.sun-cluster:onlineRG
						consoleHint:commandSpec(onlineRG) = probeview %windowID snmp://%targetHost:%targetPort/mod/sun-cluster/rgBranch/propertyFolder/ScalableRGBranch/rgPropertyFolder?runadhoccommand.onlineRG %targetFragment
						consoleHint:commandLabel(offlineRG) = base.modules.sun-cluster:offlineRG
						consoleHint:commandSpec(offlineRG) = probeview %windowID snmp://%targetHost:%targetPort/mod/sun-cluster/rgBranch/propertyFolder/ScalableRGBranch/rgPropertyFolder?runadhoccommand.offlineRG %targetFragment
						
						rgFailbackFlag = { [ use _procedures ]
							getFilter	= yesnoFilter
						}
						netDepend = { [ use _procedures ]
							getFilter	= yesnoFilter
						}
					}
				}		
			}


			rsPropertyFolder = { [ use templates.sun-cluster-models-d.rgBranch.propertyFolder.ScalableRGBranch.rsPropertyFolder _procedures ] 

				consoleHint:smallIcon(DFT) = stdimages/rCfg_16.gif
				consoleHint:largeIcon(DFT) = topoimages/rCfg_32.gif

				consoleHint:commands = help
				consoleHint:commandLabel(help) = base.modules.sun-cluster:help
				consoleHint:commandSpec(help) = launchApp com.sun.symon.apps.generic.help.SMHelpBrowser sc-ss-resources-help

				rsGenPropFolder = { [ use templates.sun-cluster-models-d.rgBranch.propertyFolder.ScalableRGBranch.rsPropertyFolder.rsGenPropFolder _procedures ]

					consoleHint:smallIcon(DFT) = stdimages/genProp_16.gif
					consoleHint:largeIcon(DFT) = topoimages/genProp_32.gif

					consoleHint:commands = help
					consoleHint:commandLabel(help) = base.modules.sun-cluster:help
					consoleHint:commandSpec(help) = launchApp com.sun.symon.apps.generic.help.SMHelpBrowser sc-ss-resources-help

					adhocCommand(delR) = probeserver -u root -c /usr/cluster/bin/scrgadm -r -j \[ get_index_part  %fragment 1 \]
					adhocCommand(enableR) = probeserver -u root -c /usr/cluster/bin/scswitch -e -j \[ get_index_part  %fragment 1 \]
					adhocCommand(disableR) = probeserver -u root -c /usr/cluster/bin/scswitch -n -j \[ get_index_part %fragment 1 \]

					rsTable = { [ use _procedures ]
						type		= active

						refreshService	= _services.sh
						refreshCommand	= /usr/bin/nice /usr/cluster/lib/scsymon/scsymon_clnt SCRG_RS_config
						refreshInterval = 600
						initInterval	= 1

						rsTableEntry = { [ use _procedures ]

							consoleHint:tableCommands = delR enableR disableR

							consoleHint:commandLabel(delR) = base.modules.sun-cluster:delR
							consoleHint:commandSpec(delR) = probeview %windowID snmp://%targetHost:%targetPort/mod/sun-cluster/rgBranch/propertyFolder/ScalableRGBranch/rsPropertyFolder/rsGenPropFolder?runadhoccommand.delR %targetFragment

							consoleHint:commandLabel(enableR) = base.modules.sun-cluster:enableR
							consoleHint:commandSpec(enableR) = probeview %windowID snmp://%targetHost:%targetPort/mod/sun-cluster/rgBranch/propertyFolder/ScalableRGBranch/rsPropertyFolder/rsGenPropFolder?runadhoccommand.enableR %targetFragment

							consoleHint:commandLabel(disableR) = base.modules.sun-cluster:disableR
							consoleHint:commandSpec(disableR) = probeview %windowID snmp://%targetHost:%targetPort/mod/sun-cluster/rgBranch/propertyFolder/ScalableRGBranch/rsPropertyFolder/rsGenPropFolder?runadhoccommand.disableR %targetFragment

							rsEnabled	= { [ use _procedures ]
								getFilter	= onoffFilter
							}

							rsMonitored	= { [ use _procedures ]
								getFilter	= yesnoFilter
							}
						}
					}
				}

				rsComPropFolder = { [ use templates.sun-cluster-models-d.rgBranch.propertyFolder.ScalableRGBranch.rsPropertyFolder.rsComPropFolder _procedures ]

					consoleHint:smallIcon(DFT) = stdimages/sysdefProp_16.gif
					consoleHint:largeIcon(DFT) = topoimages/sysdefProp_32.gif

					consoleHint:commands = help
					consoleHint:commandLabel(help) = base.modules.sun-cluster:help
					consoleHint:commandSpec(help) = launchApp com.sun.symon.apps.generic.help.SMHelpBrowser sc-ss-resources-help

					rsComPropTable = { [ use _procedures ]
						type		= active

						refreshService	= _services.sh
						refreshCommand	= /usr/bin/nice /usr/cluster/lib/scsymon/scsymon_clnt SCRG_RS_com_prop_config
						refreshInterval	= 600
						initInterval	= 1
					}
				}

				rsExtPropFolder = { [ use templates.sun-cluster-models-d.rgBranch.propertyFolder.ScalableRGBranch.rsPropertyFolder.rsExtPropFolder _procedures ]
					consoleHint:smallIcon(DFT) = stdimages/extProp_16.gif
					consoleHint:largeIcon(DFT) = topoimages/extProp_32.gif

					consoleHint:commands = help
					consoleHint:commandLabel(help) = base.modules.sun-cluster:help
					consoleHint:commandSpec(help) = launchApp com.sun.symon.apps.generic.help.SMHelpBrowser sc-ss-resources-help

					rsExtPropTable = { [ use _procedures ]
						type		= active

						refreshService	= _services.sh
						refreshCommand	= /usr/bin/nice /usr/cluster/lib/scsymon/scsymon_clnt SCRG_RS_ext_prop_config
						refreshInterval	= 600
						initInterval	= 1
					}
				}

				rsTimeoutFolder = { [ use templates.sun-cluster-models-d.rgBranch.propertyFolder.ScalableRGBranch.rsPropertyFolder.rsTimeoutFolder _procedures ] 


					consoleHint:smallIcon(DFT) = stdimages/methodtimeout_16.gif
					consoleHint:largeIcon(DFT) = topoimages/methodtimeout_32.gif

					consoleHint:commands = help
					consoleHint:commandLabel(help) = base.modules.sun-cluster:help
					consoleHint:commandSpec(help) = launchApp com.sun.symon.apps.generic.help.SMHelpBrowser sc-ss-method-help
		
					rsTimeoutTable = { [ use _procedures ]
						type		= active

						refreshService	= _services.sh
						refreshCommand	= /usr/bin/nice /usr/cluster/lib/scsymon/scsymon_clnt SCRG_RS_timeout_config
						refreshInterval	= 600
						initInterval	= 1
					}
				}
			}
		}

	}

	statusFolder = { [ use templates.sun-cluster-models-d.rgBranch.statusFolder _procedures ] 

		consoleHint:smallIcon(DFT) = stdimages/status_16.gif
		consoleHint:largeIcon(DFT) = topoimages/status_32.gif

		FailoverRGBranch = { [ use templates.sun-cluster-models-d.rgBranch.statusFolder.FailoverRGBranch _procedures ]
			consoleHint:smallIcon(DFT) = stdimages/rgFO_16.gif
			consoleHint:largeIcon(DFT) = topoimages/rgFO_32.gif

			consoleHint:commands = help
			consoleHint:commandLabel(help) = base.modules.sun-cluster:help
			consoleHint:commandSpec(help) = launchApp com.sun.symon.apps.generic.help.SMHelpBrowser sc-fo-stat-help

			adhocCommand(onlineRG) = probeserver -u root -c /usr/cluster/bin/scswitch -Z -g \[ get_index_part %fragment 0 \]
			adhocCommand(offlineRG) = probeserver -u root -c /usr/cluster/bin/scswitch -F -g \[ get_index_part %fragment 0 \]
			adhocCommand(delRG) = probeserver -u root -c /usr/cluster/bin/scrgadm -r -g \[ get_index_part %fragment 0 \]

			rgStatusTable = { [ use _procedures ]
				type		= active
				refreshService	= _services.sh
				refreshCommand	= /usr/bin/nice /usr/cluster/lib/scsymon/scsymon_clnt FORG_RG_status
				refreshInterval	= 120
				initInterval	= 1

				rgStatusTableEntry = { [ use _procedures ]
					rowstatus = { [ use _procedures ]
					}

					consoleHint:tableCommands = onlineRG offlineRG delRG

					consoleHint:commandLabel(onlineRG) = base.modules.sun-cluster:onlineRG
					consoleHint:commandSpec(onlineRG) = probeview %windowID snmp://%targetHost:%targetPort/mod/sun-cluster/rgBranch/statusFolder/FailoverRGBranch?runadhoccommand.onlineRG %targetFragment
					consoleHint:commandLabel(offlineRG) = base.modules.sun-cluster:offlineRG
					consoleHint:commandSpec(offlineRG) = probeview %windowID snmp://%targetHost:%targetPort/mod/sun-cluster/rgBranch/statusFolder/FailoverRGBranch?runadhoccommand.offlineRG %targetFragment

					consoleHint:commandLabel(delRG) = base.modules.sun-cluster:delRG
					consoleHint:commandSpec(delRG) = probeview %windowID snmp://%targetHost:%targetPort/mod/sun-cluster/rgBranch/statusFolder/FailoverRGBranch?runadhoccommand.delRG %targetFragment
				}

			}

			adhocCommand(delR) = probeserver -u root -c /usr/cluster/bin/scrgadm -r -j \[ get_index_part  %fragment 1 \]
			adhocCommand(enableR) = probeserver -u root -c /usr/cluster/bin/scswitch -e -j \[ get_index_part  %fragment 1 \]
			adhocCommand(disableR) = probeserver -u root -c /usr/cluster/bin/scswitch -n -j \[ get_index_part %fragment 1 \]

			rsStatusTable = { [ use _procedures ]
				type		= active

				refreshService	= _services.sh
				refreshCommand	= /usr/bin/nice /usr/cluster/lib/scsymon/scsymon_clnt FORG_RS_status
				refreshInterval	= 120
				initInterval	= 1

				rsStatusTableEntry = { [ use _procedures ]
					rowstatus = { [ use _procedures ]
					}

					consoleHint:tableCommands = enableR disableR delR

					consoleHint:commandLabel(delR) = base.modules.sun-cluster:delR
					consoleHint:commandSpec(delR) = probeview %windowID snmp://%targetHost:%targetPort/mod/sun-cluster/rgBranch/statusFolder/FailoverRGBranch?runadhoccommand.delR %targetFragment

					consoleHint:commandLabel(enableR) = base.modules.sun-cluster:enableR
					consoleHint:commandSpec(enableR) = probeview %windowID snmp://%targetHost:%targetPort/mod/sun-cluster/rgBranch/statusFolder/FailoverRGBranch?runadhoccommand.enableR %targetFragment

					consoleHint:commandLabel(disableR) = base.modules.sun-cluster:disableR
					consoleHint:commandSpec(disableR) = probeview %windowID snmp://%targetHost:%targetPort/mod/sun-cluster/rgBranch/statusFolder/FailoverRGBranch?runadhoccommand.disableR %targetFragment
				}
			}
		}

		ScalableRGBranch = { [ use templates.sun-cluster-models-d.rgBranch.statusFolder.ScalableRGBranch _procedures ]
			consoleHint:smallIcon(DFT) = stdimages/rgSS_16.gif
			consoleHint:largeIcon(DFT) = topoimages/rgSS_32.gif

			consoleHint:commands = help
			consoleHint:commandLabel(help) = base.modules.sun-cluster:help
			consoleHint:commandSpec(help) = launchApp com.sun.symon.apps.generic.help.SMHelpBrowser sc-ss-stat-help

			adhocCommand(onlineRG) = probeserver -u root -c /usr/cluster/bin/scswitch -Z -g \[ get_index_part %fragment 0 \]
			adhocCommand(offlineRG) = probeserver -u root -c /usr/cluster/bin/scswitch -F -g \[ get_index_part %fragment 0 \]
			adhocCommand(delRG) = probeserver -u root -c /usr/cluster/bin/scrgadm -r -g \[ get_index_part %fragment 0 \]

			rgStatusTable = { [ use _procedures ]
				type		= active

				refreshService	= _services.sh
				refreshCommand	= /usr/bin/nice /usr/cluster/lib/scsymon/scsymon_clnt SCRG_RG_status
				refreshInterval	= 120
				initInterval	= 1

				rgStatusTableEntry = { [ use _procedures ]
					rowstatus = { [ use _procedures ]
					}

					consoleHint:tableCommands = onlineRG offlineRG delRG

					consoleHint:commandLabel(onlineRG) = base.modules.sun-cluster:onlineRG
					consoleHint:commandSpec(onlineRG) = probeview %windowID snmp://%targetHost:%targetPort/mod/sun-cluster/rgBranch/statusFolder/ScalableRGBranch?runadhoccommand.onlineRG %targetFragment
					consoleHint:commandLabel(offlineRG) = base.modules.sun-cluster:offlineRG
					consoleHint:commandSpec(offlineRG) = probeview %windowID snmp://%targetHost:%targetPort/mod/sun-cluster/rgBranch/statusFolder/ScalableRGBranch?runadhoccommand.offlineRG %targetFragment

					consoleHint:commandLabel(delRG) = base.modules.sun-cluster:delRG
					consoleHint:commandSpec(delRG) = probeview %windowID snmp://%targetHost:%targetPort/mod/sun-cluster/rgBranch/statusFolder/ScalableRGBranch?runadhoccommand.delRG %targetFragment
				}

			}

			adhocCommand(delR) = probeserver -u root -c /usr/cluster/bin/scrgadm -r -j \[ get_index_part  %fragment 1 \]
			adhocCommand(enableR) = probeserver -u root -c /usr/cluster/bin/scswitch -e -j \[ get_index_part  %fragment 1 \]
			adhocCommand(disableR) = probeserver -u root -c /usr/cluster/bin/scswitch -n -j \[ get_index_part %fragment 1 \]

			rsStatusTable = { [ use _procedures ]
				type		= active

				refreshService	= _services.sh
				refreshCommand	= /usr/bin/nice /usr/cluster/lib/scsymon/scsymon_clnt SCRG_RS_status
				refreshInterval	= 120
				initInterval	= 1

				rsStatusTableEntry = { [ use _procedures ]
					rowstatus = { [ use _procedures ]
					}

					consoleHint:tableCommands = enableR disableR delR

					consoleHint:commandLabel(delR) = base.modules.sun-cluster:delR
					consoleHint:commandSpec(delR) = probeview %windowID snmp://%targetHost:%targetPort/mod/sun-cluster/rgBranch/statusFolder/ScalableRGBranch?runadhoccommand.delR %targetFragment

					consoleHint:commandLabel(enableR) = base.modules.sun-cluster:enableR
					consoleHint:commandSpec(enableR) = probeview %windowID snmp://%targetHost:%targetPort/mod/sun-cluster/rgBranch/statusFolder/ScalableRGBranch?runadhoccommand.enableR %targetFragment

					consoleHint:commandLabel(disableR) = base.modules.sun-cluster:disableR
					consoleHint:commandSpec(disableR) = probeview %windowID snmp://%targetHost:%targetPort/mod/sun-cluster/rgBranch/statusFolder/ScalableRGBranch?runadhoccommand.disableR %targetFragment
				}

			}
		}
	}
}

rtBranch = { [ use templates.sun-cluster-models-d.rtBranch _procedures ]

	consoleHint:smallIcon(DFT) = stdimages/rts_16.gif
	consoleHint:largeIcon(DFT) = topoimages/rts_32.gif

	consoleHint:commands = help
	consoleHint:commandLabel(help) = base.modules.sun-cluster:help
	consoleHint:commandSpec(help) = launchApp com.sun.symon.apps.generic.help.SMHelpBrowser sc-rt-help

	definitionFolder =  { [ use templates.sun-cluster-models-d.rtBranch.definitionFolder _procedures ]

		consoleHint:smallIcon(DFT) = stdimages/definition_16.gif
		consoleHint:largeIcon(DFT) = topoimages/definition_32.gif

		genPropFolder =  { [ use templates.sun-cluster-models-d.rtBranch.definitionFolder.genPropFolder _procedures ]

			consoleHint:smallIcon(DFT) = stdimages/overview_16.gif
			consoleHint:largeIcon(DFT) = topoimages/overview_32.gif

			consoleHint:commands = help
			consoleHint:commandLabel(help) = base.modules.sun-cluster:help
			consoleHint:commandSpec(help) = launchApp com.sun.symon.apps.generic.help.SMHelpBrowser sc-rt-prop-help

			rtTable = { [ use _procedures ]
				type		= active

		 		refreshService	= _services.sh
				refreshCommand	= /usr/bin/nice /usr/cluster/lib/scsymon/scsymon_clnt RT_config
				refreshInterval	= 2000
				initInterval	= 1

				rtTableEntry =  { [ use _procedures ]
					rtSingleInst = { [ use _procedures ]
						getFilter	= yesnoFilter
					}

					rtFailover = { [ use _procedures ]
						getFilter	= yesnoFilter
					}
				}
			}
		}

		methodFolder =  { [ use templates.sun-cluster-models-d.rtBranch.definitionFolder.methodFolder _procedures ]

			consoleHint:smallIcon(DFT) = stdimages/methods_16.gif
			consoleHint:largeIcon(DFT) = topoimages/methods_32.gif

			consoleHint:commands = help
			consoleHint:commandLabel(help) = base.modules.sun-cluster:help
			consoleHint:commandSpec(help) = launchApp com.sun.symon.apps.generic.help.SMHelpBrowser sc-rt-method-help
	
			rtMethodTable = { [ use _procedures ]
				type		= active

				refreshService	= _services.sh
				refreshCommand	= /usr/bin/nice /usr/cluster/lib/scsymon/scsymon_clnt RT_method_config
				refreshInterval	= 2000
				initInterval	= 1
			}
		}

		paramFolder =  { [ use templates.sun-cluster-models-d.rtBranch.definitionFolder.paramFolder _procedures ]

			consoleHint:smallIcon(DFT) = stdimages/rtsParam_16.gif
			consoleHint:largeIcon(DFT) = topoimages/rtsParam_32.gif

			consoleHint:commands = help
			consoleHint:commandLabel(help) = base.modules.sun-cluster:help
			consoleHint:commandSpec(help) = launchApp com.sun.symon.apps.generic.help.SMHelpBrowser sc-rt-param-help
		
			rtParamTable = { [ use _procedures ]
				type		= active

				refreshService	= _services.sh
				refreshCommand	= /usr/bin/nice /usr/cluster/lib/scsymon/scsymon_clnt RT_param_config
				refreshInterval	= 2000
				initInterval	= 1

				rtParamTableEntry =  { [ use _procedures ]
					rtPropExt = { [ use _procedures ]
						getFilter	= yesnoFilter
					}
				}
			}
		}
	}

	rsFolder =  { [ use templates.sun-cluster-models-d.rtBranch.rsFolder _procedures ]

		consoleHint:smallIcon(DFT) = stdimages/instances_16.gif
		consoleHint:largeIcon(DFT) = topoimages/instances_32.gif


		consoleHint:commands = help
		consoleHint:commandLabel(help) = base.modules.sun-cluster:help
		consoleHint:commandSpec(help) = launchApp com.sun.symon.apps.generic.help.SMHelpBrowser sc-rt-resources-help

		rsTable = { [ use _procedures ]
			type		= derived

			refreshService	= _services.sh
			refreshCommand	= /usr/bin/nice /usr/cluster/lib/scsymon/scsymon_clnt RT_RS_config
			refreshInterval	= 2000
			refreshTrigger  = *propertyFolder.FailoverRGBranch*rsTable.rsTableEntry.rsName *propertyFolder.ScalableRGBranch*rsTable.rsTableEntry.rsName
#			initInterval	= 1
		}
	}
}


[ load sun-cluster-d.def ]
