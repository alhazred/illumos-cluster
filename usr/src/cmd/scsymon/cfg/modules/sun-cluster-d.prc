#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#pragma ident	"@(#)sun-cluster-d.prc	1.34	08/05/20 SMI"
#
# Copyright (c) 1999-2001 by Sun Microsystems, Inc.
# All rights reserved.
#
# cluster/src/scsymon/cfg/modules/sun-cluster-d.prc
#

proc yesnoFilter { i } {
    if { $i } { return "yes" } else { return "no" }
}

proc onoffFilter { i } {
    if { $i } { return "on" } else { return "off" }
}

proc get_create_node_value {} {
	return [list "create from prc"]
}

proc get_status_node_value {} {
	return [list "status from prc"]
}

proc get_index_part { index_str part_no } {
#	ddl print info "get_index_part: index_str = $index_str part_no = $part_no \n"
	set parts [split $index_str ,]
	set index_part [lindex $parts $part_no]
#	ddl print info "get_index_part: needed index part is $index_part \n"
	return $index_part
}

proc is_empty {} {
	set create_value [ getValue 0 ]
	ddl print info "is_empty: create_value = $create_value\n"
	if { $create_value == "" } {
		set status_value [ toe_send [ locate statusNode ] getValue 0 ]
		ddl print info "is_empty: status_value = $status_value\n"
		if {  $status_value == "" } {
			ddl print info "return 1"
			return 1
		} else {
			ddl print info "return 0"
			return 0
		}
	} else {
		ddl print info "return 0"
		return 0
	}
}

proc createRG { value } {
	ddl print info "createRG: value = $value\n"
	evaluateCommand shell "" $value [ list [ list createRGCallback %result ] ]
	return $value
}

proc createRGCallback { result } {
	ddl print info "createRGCallback: $result\n"
	set code [ lindex $result 0 ]
	switch $code {
		wait {
			# got wait code -- do nothing
		}
		data {
			# got command result
			ddl print info "stdout is [ lindex $result 2 ]"
			ddl print info "set result to statusNode"
			toe_send [ locate statusNode ] setValue 0 SUCCESS refresh
			ddl print info "reset createNode"
			setValue 0 [ list [ lindex "" 2 ] ] refresh
		}
		error {
			# got command error condition
			ddl print info "stderr is [ lindex $result 2 ]"
			ddl print info "set result to statusNode"
			toe_send [ locate statusNode ] setValue 0 [ list [ lindex $result 2 ] ] refresh
			ddl print info "reset createNode"
			setValue 0 [ list [ lindex "" 2 ] ] refresh
		}
		default {
			ddl print error "bad return code: $code\n"
		}
	}
}

proc get_cluster_name {} {
	return "peppy"
}

proc get_cluster_config {} {
	return [list peppy disabled 172.16.0.0 255.255.0.0 unix "<NULL - Allow any node>"]
}

proc get_cluster_status {} {
	return [list peppy 2 3]
}

proc get_node_config {} {
	return [list peppy-1 1 peppy-1-priv hme0,le0 \
	    peppy-2 1 peppy-2-priv hme0,le0]
}

proc get_nodedev_config {} {
	return [list peppy-1 qdev-1,qdev-2 dg-1,dg-2 rg-1,rg-2 \
	    peppy-2 qdev-2 dg-1 rg-1]
}
proc get_node_status {} {
	return [list peppy-1 Online 1 \
	    peppy-2 Online 1]
}

proc get_nodedev_status {} {
	return [list peppy-1 dg-1,dg-2 rg-1,rg-2 \
	    peppy-2 dg-1 rg-1]
}

# Device Groups in Properties folder:
proc get_devgrp_config {} {
	return [list dsk/d1 SDS 1 peppy-1,peppy2 1 dg-1 \
	    dsk/d2 SDS 1 peppy-1,peppy2 0 dg-2 ]
}

# Device Groups in Status folder
proc get_devgrp_status {} {
	return [list \
	    dsk/d1 Online peppy-1 \
	    dsk/d2 Offline peppy-2 ]
}

proc get_replica_status {} {
	return [list dsk/d1 peppy-1 PRIMARY \
	    dsk/d1 peppy-2 SECONDARY \
	    dsk/d2 peppy-1 PRIMARY \
	    dsk/d2 peppy-2 SECONDARY]
}

proc get_qdev_config {} {
	return [list qdev-1 /dev/did/rdsk/d1s2 1 1 peppy-1,peppy-2 \
	    qdev-2 /dev/did/rdsk/d2s2 1 1 peppy-1,peppy-2]
}

proc get_qdevport_config {} {
	return [list qdev-1 peppy-1 1 \
	    qdev-1 peppy-2 0 \
	    qdev-2 peppy-1 1 \
	    qdev-2 peppy-2 1]
}

proc get_qdev_status {} {
	return [list qdev-1 Online 1 \
	    qdev-2 Offline 0]
}

proc get_tr_adapter_config {} {
	return [list peppy-1 qe0 dlpi 1 \
	    peppy-1 qe1 dlpi 1 \
	    peppy-2 qe0 dlpi 1 \
	    peppy-2 qe1 dlpi 1]
}

proc get_junction_config {} {
	return [list hub0 switch 1 1,2 \
	    hub1 switch 1 1,2 ]
}

proc get_cable_config {} {
	return [list ADAPTER peppy-1:qe0@0 JUNCTION hub@1 1 \
	    ADAPTER peppy-1:qe1@0 ADAPTER peppy-2:qe1@0 1 ]
}

proc get_path_status {} {
	return [list qe0@peppy-1 qe0@peppy-2 Online \
	    qe1@peppy-1  qe1@peppy-2 Offline ]
}

proc get_RT_config {} {
	return [list \
	    RT_ORA peppy-1,peppy2 "oracle data service" "base_dir" 1 "*" 1 ""  "" 1 "1.0" "" \
	    RT_SYBASE peppy-1 "sybase data service" "base_dir" 1 "*" 1 ""  "" 1 "2.0" "" \
	    RT_HTTP peppy1,peppy-2 "HTTP service" "base_dir" 0 "*" 1 ""  "" 1 "2.0" "" \
	    LogicalHostname peppy-1,peppy-2 "Logical Hostname Resource Type" "base_dir" \
		1 "*" 1 ""  "" 1 "2.0" "" \
	    SharedAddress peppy-1,peppy-2 "HA Shared Address Resource Type" "base_dir" \
		1 "*" 1 ""  "" 1 "2.0" ""]
}

proc get_RT_method_config {} {
	return [list \
	    RT_ORA start stop primchange validate update ini fini boot moninit mostart monstop moncheck prenetstart postnetstop \
	    RT_SYBASE start stop primchange validate update ini fini boot moninit mostart monstop moncheck prenetstart postnetstop \
	    RT_HTTP start stop primchange validate update ini fini boot moninit mostart monstop moncheck prenetstart postnetstop \
	    LogicalHostname start stop primchange validate update ini fini boot moninit mostart monstop moncheck prenetstart postnetstop \
	    SharedAddress start stop primchange validate update ini fini boot moninit mostart monstop moncheck prenetstart postnetstop
]
}
proc get_RT_param_config {} {
	return [list \
	    RT_ORA "START timeout" false tune_at_creation int 350 0 3600 0 0 "" \
	    RT_ORA "STOP timeout" false tune_at_creation int 350 0 3600 0 0 "" \
	    RT_ORA "VALIDATE timeout" false tune_at_creation int 350 0 3600 0 0 "" \
	    RT_ORA "UPDATE timeout" false tune_at_creation int 60 5 3600 0 0 "" \
	    RT_ORA "INIT timeout" false tune_at_creation int 60 5 3600 0 0 "" \
	    RT_ORA Failover_mode false tune_at_creation "enum - none,soft,hard" hard  5 3600 0 0 "" \
	    RT_ORA NetIfList true tune_at_creation stringarray "" 0 0 0 0 "" \
	    RT_ORA HostnameList true tune_at_creation stringarray "" 0 0 0 0 "" \
	    RT_SYBASE "START timeout" false tune_at_creation int 350 0 3600 0 0 "" \
	    RT_SYBASE "STOP timeout" false tune_at_creation int 350 0 3600 0 0 "" \
	    RT_SYBASE "VALIDATE timeout" false tune_at_creation int 350 0 3600 0 0 "" \
	    RT_SYBASE "UPDATE timeout" false tune_at_creation int 60 5 3600 0 0 "" \
	    RT_SYBASE "INIT timeout" false tune_at_creation int 60 5 3600 0 0 "" \
	    RT_SYBASE Failover_mode false tune_at_creation "enum - none,soft,hard" hard  5 3600 0 0 "" \
	    RT_SYBASE NetIfList true tune_at_creation stringarray "" 0 0 0 0 "" \
	    RT_SYBASE HostnameList true tune_at_creation stringarray "" 0 0 0 0 "" \
	    RT_HTTP "START timeout" false tune_at_creation int 350 0 3600 0 0 "" \
	    RT_HTTP "STOP timeout" false tune_at_creation int 350 0 3600 0 0 "" \
	    RT_HTTP "VALIDATE timeout" false tune_at_creation int 350 0 3600 0 0 "" \
	    RT_HTTP "UPDATE timeout" false tune_at_creation int 60 5 3600 0 0 "" \
	    RT_HTTP "INIT timeout" false tune_at_creation int 60 5 3600 0 0 "" \
	    RT_HTTP Failover_mode false tune_at_creation "enum - none,soft,hard" hard  5 3600 0 0 "" \
	    RT_HTTP SharedAddressesUsed true tunable stringarray "" 0 0 0 0 "" \
	    LogicalHostname "START timeout" false tune_at_creation int 350 0 3600 0 0 "" \
	    LogicalHostname "STOP timeout" false tune_at_creation int 350 0 3600 0 0 "" \
	    LogicalHostname "VALIDATE timeout" false tune_at_creation int 350 0 3600 0 0 "" \
	    LogicalHostname "UPDATE timeout" false tune_at_creation int 60 5 3600 0 0 "" \
	    LogicalHostname "INIT timeout" false tune_at_creation int 60 5 3600 0 0 "" \
	    LogicalHostname Failover_mode false tune_at_creation "enum - none,soft,hard" hard  5 3600 0 0 "" \
	    LogicalHostname NetIfList true tune_at_creation stringarray "" 0 0 0 0 "" \
	    LogicalHostname HostnameList true tune_at_creation stringarray "" 0 0 0 0 "" ]
	    SharedAddress   "START timeout" false tune_at_creation int 350 0 3600 0 0 "" \
	    SharedAddress   "STOP timeout" false tune_at_creation int 350 0 3600 0 0 "" \
	    SharedAddress   "VALIDATE timeout" false tune_at_creation int 350 0 3600 0 0 "" \
	    SharedAddress   "UPDATE timeout" false tune_at_creation int 60 5 3600 0 0 "" \
	    SharedAddress   "INIT timeout" false tune_at_creation int 60 5 3600 0 0 "" \
	    SharedAddress   Failover_mode false tune_at_creation "enum - none,soft,hard" hard  5 3600 0 0 "" \
	    SharedAddress   AuxNodeList true tune_at_creation stringarray "" 0 0 0 0 "" \
	    SharedAddress   NetIfList true tune_at_creation stringarray "" 0 0 0 0 "" \
	    SharedAddress   HostnameList true tune_at_creation stringarray "" 0 0 0 0 "" ]
}

proc get_RT_RS_config {} {
	return [list RT_ORA rg-1 rs-11 \
	    RT_ORA rg-2 rs-21  \
	    RT_SYBASE rg-1 rs-12 \
	    RT_SYBASE rg-2 rs-22 \
	    RT_HTTP rg-3 rs-31,rs-32 \
	    RT_HTTP rg-4 rs-41,rs-42 \
	    LogicalHostname rg-1 rs-13 \
	    LogicalHostname rg-2 rs-23 \
	    SharedAddress rg-1 rs-14 \
	    SharedAddress rg-2 rs-24]
}

proc get_FORG_RG_config {} {
	return [list rg-1 peppy-1,peppy-2 "rg-1 is failover rg" rs-11,rs-12,rs-13,rs-14 1 1 0 1 "" "" "" \
	    rg-2 peppy-2,peppy-1 "rg-2 is failover rg" rs-21,rs-22,rs-23,rs-24 1 1 1 1 "rg-1" "" "" ]
}

proc get_FORG_RS_config {} {
	return [list \
	    rg-1 rs-11 RT_ORA 1 0 "" "" \
	    rg-1 rs-12 RT_SYBASE 1 0 "" "" \
	    rg-1 rs-13 LogicalHostname 0 0 "" "" \
	    rg-1 rs-14 SharedAddress 0 0 "" "" \
	    rg-2 rs-21 RT_ORA 1 1 "" "" \
	    rg-2 rs-22 RT_SYBASE 1 1 "" "" \
	    rg-2 rs-23 LogicalHostname 1 1 "" "" \
	    rg-2 rs-24 SharedAddress 1 1 "" ""]
}

proc get_FORG_RS_com_prop_config {} {
	return [list \
		rg-1 rs-11 Failover_mode SOFT "" \
		rg-1 rs-11 Logical_hostnames_used "" "" \
		rg-1 rs-11 Cheap_probe_interval 60 "" \
		rg-1 rs-11 Thorough_probe_interval 60 "" \
		rg-1 rs-11 Retry_count 2 "" \
		rg-1 rs-11 Retry_interval 20 "" \
	    ]
}

proc get_FORG_RS_ext_prop_config {} {
	return [list \
		rg-1 rs-13 Scalable FALSE "" \
		rg-1 rs-13 Port__List 5001 "" \
		rg-1 rs-13 Description "" "" \
		rg-1 rs-13 Server_Home  "/rgm-test/test_svc/bin" "" \
		rg-1 rs-13 Probe_Timeout 60 "" \
		rg-1 rs-13 Protocol_Family 6 "" \
		rg-1 rs-13 Fail_Method_list "" "" \
		rg-1 rs-13 Sleep_Method_list "" "" \
		rg-1 rs-13 Reboot_Method_list "" "" \
		rg-1 rs-13 Load_Balancing_Policy 0 "" \
		rg-1 rs-13 Load_Balancing_String "" "" \
		rg-1 rs-13 Shared_Addresses_Used "" "" \
	]
}

proc get_FORG_RS_timeout_config {} {
	return [list \
		rg-1 rs-11 START_TIMEOUT 300 "" \
		rg-1 rs-11 STOP_TIMEOUT 300 "" \
		rg-1 rs-11 VALIDATE_TIMEOUT 300 "" \
		rg-1 rs-11 UPDATE_TIMEOUT 300 "" \
		rg-1 rs-11 INIT_TIMEOUT 300 "" \
		rg-1 rs-11 FINI_TIMEOUT 300 "" \
	]	
}

proc get_FORG_RG_status {} {
        return [list rg-1 peppy-1 Online Online \
	    rg-1 peppy-2 Online Offline \
	    rg-2 peppy-1 Online Faulted \
	    rg-2 peppy-2 Online Offline ]
}

proc get_FORG_RS_status {} {
	return [list \
	    rg-1 rs-11 peppy-1 Online Online Online \
	    rg-1 rs-11 peppy-2 Online Unknown Offline \
	    rg-1 rs-12 peppy-1 Online Unknown Online_Not_Monitored \
	    rg-1 rs-12 peppy-2 Online Offline Offline \
	    rg-1 rs-13 peppy-1 Online Unknown  Online_Not_Monitored \
	    rg-1 rs-13 peppy-2 Online Unknown Offline \
	    rg-1 rs-14 peppy-1 Online Online Online\
	    rg-1 rs-14 peppy-2 Online Unknown Offline \
	    rg-2 rs-21 peppy-1 Online Unknown Online_Not_Monitored \
	    rg-2 rs-21 peppy-2 Online Offline Offline \
	    rg-2 rs-22 peppy-1 Online Unknown Online \
	    rg-2 rs-22 peppy-2 Online Online Wait \
	    rg-2 rs-23 peppy-1 Online Degraded Faulted \
	    rg-2 rs-23 peppy-2 Online Unknown Offline \
	    rg-2 rs-24 peppy-1 Online Online Online \
	    rg-2 rs-24 peppy-2 Online Unknown Offline ]
}

proc get_SCRG_RG_config {} {
	return [list rg-3 "peppy-1, peppy-2" "rg-3 is scalable rg" rs-31,rs-32,rs-33,rs-34 2 2 0 0 "" "" ""  \
	    rg-4 "peppy-2, peppy-1" "rg-4 is scalable rg" rs-41,rs-42,rs-43,rs-44 2 2 1 0 "rg-3" "" "" ]
}

proc get_SCRG_RS_config {} {
	return [list rg-3 rs-31 RT_HTTP 1 0 "" "" \
	    rg-3 rs-32 RT_HTTP 0 0 "" "" \
	    rg-4 rs-41 RT_HTTP 1 1 "" "" \
	    rg-4 rs-42 RT_HTTP 1 1 "" "" ]
}

proc get_SCRG_RS_com_prop_config {} {
	return [list  ]
}

proc get_SCRG_RS_ext_prop_config {} {
	return [list  ]
}

proc get_SCRG_RS_timeout_config {} {
	return [list  ]
}

proc get_SCRG_RG_status {} {
	return [list rg-3 peppy-1 Online Online \
	    rg-3 peppy-2 Online Online \
	    rg-4 peppy-1 Online Wait \
	    rg-4 peppy-2 Online Online ]
}

proc get_SCRG_RS_status {} {
	return [list rg-3 rs-31 peppy-1 Online Unknown Online_Not_Monitored \
	    rg-3 rs-31 peppy-2 Online Unknown Online_Not_Monitored \
	    rg-3 rs-32 peppy-1 Online Ok  Online \
	    rg-3 rs-32 peppy-2 Online Ok Online\
	    rg-4 rs-41 peppy-1 Online Degraded Pending_Online \
	    rg-4 rs-41 peppy-2 Online Not_Monitored Online \
	    rg-4 rs-42 peppy-1 Online Unknown Online \
	    rg-4 rs-42 peppy-2 Online Ok Online ]
}
