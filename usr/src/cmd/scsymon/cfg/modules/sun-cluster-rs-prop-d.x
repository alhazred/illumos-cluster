#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#pragma ident	"@(#)sun-cluster-rs-prop-d.x	1.5	08/05/20 SMI"
#
# Copyright (c) 1999 by Sun Microsystems, Inc.
# All rights reserved.
#
# cluster/src/scsymon/cfg/modules/sun-cluster-rs-prop-d.prc
#

rsPropValue	= { [ use GLOBROWNODE STRING MANAGED-PROPERTY ]
	mediumDesc	= Property Value
	consoleHint:mediumDesc = base.modules.sun-cluster:rgBranch.rsPropertyFolder.rsPropTable.rsPropTableEntry.rsPropValue
	required	= true
}

rsPropDesc	= { [ use STRING MANAGED-PROPERTY ]
	mediumDesc	= Property Description
	consoleHint:mediumDesc = base.modules.sun-cluster:rgBranch.rsPropertyFolder.rsPropTable.rsPropTableEntry.rsPropDesc
	dataFormat	= unicode
}

