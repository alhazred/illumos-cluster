#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#pragma ident	"@(#)sun-cluster-devgrp-models-d.x	1.8	08/05/20 SMI"
#
# Copyright (c) 1999-2001 by Sun Microsystems, Inc.
# All rights reserved.
#
# cluster/src/scsymon/cfg/modules/sun-cluster-devgrp-models-d.x
#

[ use MANAGED-OBJECT ]
mediumDesc = Device Groups
consoleHint:mediumDesc = base.modules.sun-cluster:deiveGroupBranch

statusFolder = { [ use MANAGED-OBJECT ]
	mediumDesc	= Status
    	consoleHint:mediumDesc = base.modules.sun-cluster:deviceGroupBranch.statusFolder

	deviceGroupTable = { [ use MANAGED-OBJECT-TABLE ]
		mediumDesc	= Device Groups Table
		consoleHint:mediumDesc = base.modules.sun-cluster:deviceGroupBranch.statusFolder.deviceGroupTable

		deviceGroupTableEntry = { [ use MANAGED-OBJECT-TABLE-ENTRY ]
			mediumDesc	= Device Groups Table Entry 
			consoleHint:mediumDesc = base.modules.sun-cluster:deviceGroupBranch.statusFolder.deviceGroupTable.deviceGroupTableEntry
			index		= deviceGroupName

			deviceGroupName = { [ use STRING MANAGED-PROPERTY ]
				mediumDesc	= Name
				consoleHint:mediumDesc = base.modules.sun-cluster:deviceGroupBranch.statusFolder.deviceGroupTable.deviceGroupTableEntry.deviceGroupName
			}

			status = { [ use STRINGREGEXP MANAGED-PROPERTY ]
				mediumDesc	= Status
				consoleHint:mediumDesc = base.modules.sun-cluster:deviceGroupBranch.statusFolder.deviceGroupTable.deviceGroupTableEntry.status
			}

			primaryList = { [ use STRING MANAGED-PROPERTY ]
				mediumDesc	= Primary
				consoleHint:mediumDesc = base.modules.sun-cluster:deviceGroupBranch.statusFolder.deviceGroupTable.deviceGroupTableEntry.primaryList
			}
		}
	}

	replicaTable = { [ use MANAGED-OBJECT-TABLE ]
		mediumDesc	= Device Group Replicas Table
		consoleHint:mediumDesc = base.modules.sun-cluster:deviceGroupBranch.statusFolder.replicaTable

		replicaTableEntry = { [ use MANAGED-OBJECT-TABLE-ENTRY ]
			mediumDesc	= Replicas Table Entry 

			consoleHint:mediumDesc = base.modules.sun-cluster:deviceGroupBranch.statusFolder.replicaTable.replicaTableEntry
			index		= deviceGroupName nodeName

			deviceGroupName = { [ use STRING MANAGED-PROPERTY ]
				mediumDesc	= Device Group Name
				consoleHint:mediumDesc = base.modules.sun-cluster:deviceGroupBranch.statusFolder.replicaTable.replicaTableEntry.deviceGroupName
			}

			nodeName = { [ use STRING MANAGED-PROPERTY ]
				mediumDesc	= Node Name
				consoleHint:mediumDesc = base.modules.sun-cluster:deviceGroupBranch.statusFolder.replicaTable.replicaTableEntry.nodeName
			}

			status = { [ use STRING MANAGED-PROPERTY ]
				mediumDesc	= Replica Status
				consoleHint:mediumDesc = base.modules.sun-cluster:deviceGroupBranch.statusFolder.replicaTable.replicaTableEntry.status
			}
		}
	}
}

propertyFolder = { [ use MANAGED-OBJECT ]
	mediumDesc	= Property
    	consoleHint:mediumDesc = base.modules.sun-cluster:deviceGroupBranch.propertyFolder

	deviceGroupTable = { [ use MANAGED-OBJECT-TABLE ]
		mediumDesc	= Device Groups Table
		consoleHint:mediumDesc = base.modules.sun-cluster:deviceGroupBranch.propertyFolder.deviceGroupTable

		deviceGroupTableEntry = { [ use MANAGED-OBJECT-TABLE-ENTRY ]
			mediumDesc	= Device Groups Table Entry 
			consoleHint:mediumDesc = base.modules.sun-cluster:deviceGroupBranch.propertyFolder.deviceGroupTable.deviceGroupTableEntry
			index		= deviceGroupName

			deviceGroupName = { [ use STRING MANAGED-PROPERTY ]
				mediumDesc	= Name
				consoleHint:mediumDesc = base.modules.sun-cluster:deviceGroupBranch.propertyFolder.deviceGroupTable.deviceGroupTableEntry.deviceGroupName
			}

			serviceType = { [ use STRING MANAGED-PROPERTY ]
				mediumDesc	= Type
				consoleHint:mediumDesc = base.modules.sun-cluster:deviceGroupBranch.propertyFolder.deviceGroupTable.deviceGroupTableEntry.serviceType
			}

			failback = { [ use STRING MANAGED-PROPERTY ]
				mediumDesc	= Failback
				consoleHint:mediumDesc = base.modules.sun-cluster:deviceGroupBranch.propertyFolder.deviceGroupTable.deviceGroupTableEntry.failback
			}

			nodeList = { [ use STRING MANAGED-PROPERTY ]
				mediumDesc	= Node List
				consoleHint:mediumDesc = base.modules.sun-cluster:deviceGroupBranch.propertyFolder.deviceGroupTable.deviceGroupTableEntry.nodeList
			}

			nodeOrder = { [ use STRING MANAGED-PROPERTY ]
				mediumDesc	= Node Order
				consoleHint:mediumDesc = base.modules.sun-cluster:deviceGroupBranch.propertyFolder.deviceGroupTable.deviceGroupTableEntry.nodeOrder
			}

			deviceList = { [ use STRING MANAGED-PROPERTY ]
				mediumDesc	= Devices
				consoleHint:mediumDesc = base.modules.sun-cluster:deviceGroupBranch.propertyFolder.deviceGroupTable.deviceGroupTableEntry.deviceList
			}
		}
	}
}

