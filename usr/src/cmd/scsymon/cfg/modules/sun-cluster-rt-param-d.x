#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#pragma ident	"%Z%%M%	%I%	%E% SMI"
#
# Copyright (c) 2000-2001 by Sun Microsystems, Inc.
# All rights reserved.
#
# cluster/src/scsymon/cfg/modules/sun-cluster-rt-param-d.x
#

rtPropExt	= { [ use STRING MANAGED-PROPERTY ]
	mediumDesc	= Extension
	consoleHint:mediumDesc = base.modules.sun-cluster:rtBranch.definitionFolder.paramFolder.rtParamTable.rtParamTableEntry.rtPropExt
}

rtPropTunable	= { [ use STRING MANAGED-PROPERTY ]
	mediumDesc	= Tunable
	consoleHint:mediumDesc = base.modules.sun-cluster:rtBranch.definitionFolder.paramFolder.rtParamTable.rtParamTableEntry.rtPropTunable
}

rtPropType	= { [ use STRING MANAGED-PROPERTY ]
	mediumDesc	= Type
	consoleHint:mediumDesc = base.modules.sun-cluster:rtBranch.definitionFolder.paramFolder.rtParamTable.rtParamTableEntry.rtPropType
}	

rtPropDefault	= { [ use STRING MANAGED-PROPERTY ]
	mediumDesc	= Default
	consoleHint:mediumDesc = base.modules.sun-cluster:rtBranch.definitionFolder.paramFolder.rtParamTable.rtParamTableEntry.rtPropDefault
}

rtPropMin	= { [ use STRING MANAGED-PROPERTY ]
	mediumDesc	= Min
	consoleHint:mediumDesc = base.modules.sun-cluster:rtBranch.definitionFolder.paramFolder.rtParamTable.rtParamTableEntry.rtPropMin
}	

rtPropMax	= { [ use STRING MANAGED-PROPERTY ]
	mediumDesc	= Max
	consoleHint:mediumDesc = base.modules.sun-cluster:rtBranch.definitionFolder.paramFolder.rtParamTable.rtParamTableEntry.rtPropMax
}

rtPropArrayMin	= { [ use STRING MANAGED-PROPERTY ]
	mediumDesc	= Array Min
	consoleHint:mediumDesc = base.modules.sun-cluster:rtBranch.definitionFolder.paramFolder.rtParamTable.rtParamTableEntry.rtPropArrayMin
}

rtPropArrayMax	= { [ use STRING MANAGED-PROPERTY ]
	mediumDesc	= Array Max
	consoleHint:mediumDesc = base.modules.sun-cluster:rtBranch.definitionFolder.paramFolder.rtParamTable.rtParamTableEntry.rtPropArrayMax
}

rtPropDesc	= { [ use STRING MANAGED-PROPERTY ]
	mediumDesc	= Property Description
	consoleHint:mediumDesc = base.modules.sun-cluster:rtBranch.definitionFolder.paramFolder.rtParamTable.rtParamTableEntry.rtPropDesc
	dataFormat	= unicode
}
