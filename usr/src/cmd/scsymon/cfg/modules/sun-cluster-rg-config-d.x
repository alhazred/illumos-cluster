#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#pragma ident	"@(#)sun-cluster-rg-config-d.x	1.4	08/05/20 SMI"
#
# Copyright (c) 2000-2001 by Sun Microsystems, Inc.
# All rights reserved.
#
# cluster/src/scsymon/cfg/modules/sun-cluster-rg-models-d.x
#

# this file describes the model for Resource Group Configuration subtree
# it gets included (loaded) into the main sun-cluster-rg-models-d.x file

createNode = { [ use STRING MANAGED-PROPERTY ]
	consoleHint:hidden = true
	mediumDesc	= create Node
	consoleHint:mediumDesc = base.modules.sun-cluster:rgBranch.createNode
}
		
statusNode = { [ use STRING MANAGED-PROPERTY ]
	consoleHint:hidden = true
	mediumDesc	= status Node
	consoleHint:mediumDesc = base.modules.sun-cluster:rgBranch.statusNode
}

rgPropertyFolder = { [ use MANAGED-OBJECT ]
	mediumDesc	= Resource Group Configuration
    	consoleHint:mediumDesc = base.modules.sun-cluster:rgBranch.rgPropertyFolder

	rgTable = { [ use MANAGED-OBJECT-TABLE ]
		mediumDesc	= Resource Group Properties
		consoleHint:mediumDesc = base.modules.sun-cluster:rgBranch.rgPropertyFolder.rgTable
	
		rgTableEntry = { [ use MANAGED-OBJECT-TABLE-ENTRY ]
			mediumDesc	= Resource Group Table Entry
			consoleHint:mediumDesc = base.modules.sun-cluster:rgBranch.rgPropertyFolder.rgTable.rgTableEntry
			index		= rgName

# 			rowstatus needed to manage rows in the table
			rowstatus = { [ use GLOBROWNODE ROWSTATUS MANAGED-PROPERTY]
                    		mediumDesc  = Row Status
                    		consoleHint:hidden = true
                    		consoleHint:mediumDesc = base.modules.sun-cluster:rgBranch.rgPropertyFolder.rgTable.rgTableEntry.rowstatus
			}

			rgName	= { [ use GLOBROWNODE STRING MANAGED-PROPERTY ]
				mediumDesc	= Name
				consoleHint:mediumDesc = base.modules.sun-cluster:rgBranch.rgPropertyFolder.rgTable.rgTableEntry.rgName
				required	= true
		    		dataFormat	= instance 
			}

			rgPrimaries = { [ use GLOBROWNODE STRING MANAGED-PROPERTY ]
				mediumDesc	= Primaries List
				consoleHint:mediumDesc = base.modules.sun-cluster:rgBranch.rgPropertyFolder.rgTable.rgTableEntry.rgPrimaries
				required	= true
			}

			rgDesc	= { [ use GLOBROWNODE STRING MANAGED-PROPERTY ]
				mediumDesc	= Description
				dataFormat	= unicode
				consoleHint:mediumDesc = base.modules.sun-cluster:rgBranch.rgPropertyFolder.rgTable.rgTableEntry.rgDesc
			}

			rsList	= { [ use GLOBROWNODE STRING MANAGED-PROPERTY ]
				mediumDesc	= Resource List
				consoleHint:mediumDesc = base.modules.sun-cluster:rgBranch.rgPropertyFolder.rgTable.rgTableEntry.rsList
			}

			rgMaxPrim = { [ use GLOBROWNODE INT MANAGED-PROPERTY ]
				mediumDesc	= Max Primaries
				consoleHint:mediumDesc = base.modules.sun-cluster:rgBranch.rgPropertyFolder.rgTable.rgTableEntry.rgMaxPrim
			}

			rgDesPrim = { [ use GLOBROWNODE INT MANAGED-PROPERTY ]
				mediumDesc	= Desired Primaries
				consoleHint:mediumDesc = base.modules.sun-cluster:rgBranch.rgPropertyFolder.rgTable.rgTableEntry.rgDesPrim
			}

			rgFailbackFlag	= { [ use GLOBROWNODE STRING MANAGED-PROPERTY ]
				mediumDesc	= Failback Flag
				consoleHint:mediumDesc = base.modules.sun-cluster:rgBranch.rgPropertyFolder.rgTable.rgTableEntry.rgFailbackFlag
			}

			netDepend	= { [ use GLOBROWNODE STRING MANAGED-PROPERTY ]
				mediumDesc	= Dependecies on network resources
				consoleHint:mediumDesc = base.modules.sun-cluster:rgBranch.rgPropertyFolder.rgTable.rgTableEntry.netDepend
			}

			rgDepend	= { [ use GLOBROWNODE STRING MANAGED-PROPERTY ]
				mediumDesc	= Dependecies on other Resource Groups
				consoleHint:mediumDesc = base.modules.sun-cluster:rgBranch.rgPropertyFolder.rgTable.rgTableEntry.rgDepend
			}

			rgGlobRUsed	= { [ use GLOBROWNODE STRING MANAGED-PROPERTY ]
				mediumDesc	= Global Resources Used
				consoleHint:mediumDesc = base.modules.sun-cluster:rgBranch.rgPropertyFolder.rgTable.rgTableEntry.rgGlobRUsed
			}

			rgPathPrefix	= { [ use GLOBROWNODE STRING MANAGED-PROPERTY ]
				mediumDesc	= Path Prefix
				consoleHint:mediumDesc = base.modules.sun-cluster:rgBranch.rgPropertyFolder.rgTable.rgTableEntry.rgPathPrefix
			}
		}
	}
}

rsPropertyFolder = { [ use MANAGED-OBJECT ]
	mediumDesc	= Resource Configuration
    	consoleHint:mediumDesc = base.modules.sun-cluster:rgBranch.rsPropertyFolder

	rsGenPropFolder = { [ use MANAGED-OBJECT ]
		mediumDesc	= General
	    	consoleHint:mediumDesc = base.modules.sun-cluster:rgBranch.rsPropertyFolder.rsGenPropFolder

		rsTable = { [ use MANAGED-OBJECT-TABLE ]
			mediumDesc	= Resources Configuration - General
			consoleHint:mediumDesc = base.modules.sun-cluster:rgBranch.rsPropertyFolder.rsGenPropFolder.rsTable
	
			rsTableEntry = { [ use MANAGED-OBJECT-TABLE-ENTRY ]
				mediumDesc	= Resources Table Entry
				consoleHint:mediumDesc = base.modules.sun-cluster:rgBranch.rsPropertyFolder.rsGenPropFolder.rsTable.rsTableEntry
				index		= rgName rsName

	# 			rowstatus needed to manage rows in the table
				rowstatus = { [ use GLOBROWNODE ROWSTATUS MANAGED-PROPERTY]
                	    		mediumDesc  = Row Status
                    			consoleHint:hidden = true 
                    			consoleHint:mediumDesc = base.modules.sun-cluster:rgBranch.rsPropertyFolder.rsGenPropFolder.rsTable.rsTableEntry.rowstatus
				}

				rgName	= { [ use GLOBROWNODE STRING MANAGED-PROPERTY ]
					mediumDesc	= Resource Group Name
					consoleHint:mediumDesc = base.modules.sun-cluster:rgBranch.rsPropertyFolder.rsGenPropFolder.rsTable.rsTableEntry.rgName
					required	= true
		    			dataFormat	= instance 
				}

				rsName	= { [ use GLOBROWNODE STRING MANAGED-PROPERTY ]
					mediumDesc	= Resource Name
					consoleHint:mediumDesc = base.modules.sun-cluster:rgBranch.rsPropertyFolder.rsGenPropFolder.rsTable.rsTableEntry.rsName
					required	= true
		    			dataFormat	= instance 
				}

				rsType	= { [ use GLOBROWNODE STRING MANAGED-PROPERTY ]
					mediumDesc	= Resource Type
					consoleHint:mediumDesc = base.modules.sun-cluster:rgBranch.rsPropertyFolder.rsGenPropFolder.rsTable.rsTableEntry.rsType
					required	= true
				}


				rsEnabled	= { [ use GLOBROWNODE STRING MANAGED-PROPERTY ]
					mediumDesc	= On/Off switch
					consoleHint:mediumDesc = base.modules.sun-cluster:rgBranch.rsPropertyFolder.rsGenPropFolder.rsTable.rsTableEntry.rsEnabled
				}

				rsMonitored	= { [ use GLOBROWNODE STRING MANAGED-PROPERTY ]
					mediumDesc	= Yes/No switch
					consoleHint:mediumDesc = base.modules.sun-cluster:rgBranch.rsPropertyFolder.rsGenPropFolder.rsTable.rsTableEntry.rsMonitored
				}

				rsStrongDepend	= { [ use GLOBROWNODE STRING MANAGED-PROPERTY ]
					mediumDesc	= Strong Dependecies on other Resource Groups
					consoleHint:mediumDesc = base.modules.sun-cluster:rgBranch.rsPropertyFolder.rsGenPropFolder.rsTable.rsTableEntry.rsStrongDepend
				}

				rsWeakDepend	= { [ use GLOBROWNODE STRING MANAGED-PROPERTY ]
					mediumDesc	= Weak Dependecies on other Resource Groups
					consoleHint:mediumDesc = base.modules.sun-cluster:rgBranch.rsPropertyFolder.rsGenPropFolder.rsTable.rsTableEntry.rsWeakDepend
				}
			}
		}
	}

	rsComPropFolder = { [ use MANAGED-OBJECT ]
		mediumDesc	= Standard Properties
	    	consoleHint:mediumDesc = base.modules.sun-cluster:rgBranch.rsPropertyFolder.rsComPropFolder

		rsComPropTable = { [ use MANAGED-OBJECT-TABLE ]
			mediumDesc	= Resource Standard Properties
			consoleHint:mediumDesc = base.modules.sun-cluster:rgBranch.rsPropertyFolder.rsComPropFolder.rsComPropTable
	
			rsComPropTableEntry = { [ use MANAGED-OBJECT-TABLE-ENTRY ]
				mediumDesc	= Resource Standard Properties Table Entry
				consoleHint:mediumDesc = base.modules.sun-cluster:rgBranch.rsPropertyFolder.rsComPropFolder.rsComPropTable.rsComPropTableEntry
				index		= rgName rsName rsPropName

				rowstatus = { [ use GLOBROWNODE ROWSTATUS MANAGED-PROPERTY]
					mediumDesc  = Row Status
					consoleHint:hidden = true 
					consoleHint:mediumDesc = base.modules.sun-cluster:rgBranch.rsPropertyFolder.rsComPropFolder.rsComPropTable.rsComPropTableEntry.rowstatus
				}

				rgName	= { [ use GLOBROWNODE STRING MANAGED-PROPERTY ]
					mediumDesc	= Resource Group Name
					consoleHint:mediumDesc = base.modules.sun-cluster:rgBranch.rsPropertyFolder.rsPropTable.rsPropTableEntry.rgName
					required	= true
					dataFormat	= instance 
				}

				rsName	= { [ use GLOBROWNODE STRING MANAGED-PROPERTY ]
					mediumDesc	= Resource Name
					consoleHint:mediumDesc = base.modules.sun-cluster:rgBranch.rsPropertyFolder.rsPropTable.rsPropTableEntry.rsName
					required	= true
					dataFormat	= instance 
				}

				rsPropName	= { [ use GLOBROWNODE STRING MANAGED-PROPERTY ]
					mediumDesc	= Property Name
					consoleHint:mediumDesc = base.modules.sun-cluster:rgBranch.rsPropertyFolder.rsPropTable.rsPropTableEntry.rsPropName
					required	= true
					dataFormat	= instance 
				}

				[ load sun-cluster-rs-prop-d.x ]
			}
		}
	}

	rsExtPropFolder = { [ use MANAGED-OBJECT ]
		mediumDesc	= Extended Properties
	    	consoleHint:mediumDesc = base.modules.sun-cluster:rgBranch.rsPropertyFolder.rsExtPropFolder

		rsExtPropTable = { [ use MANAGED-OBJECT-TABLE ]
			mediumDesc	= Resource Extended Properties
			consoleHint:mediumDesc = base.modules.sun-cluster:rgBranch.rsPropertyFolder.rsExtPropFolder.rsExtPropTable
	
			rsExtPropTableEntry = { [ use MANAGED-OBJECT-TABLE-ENTRY ]
				mediumDesc	= Resource Extended Properties Table Entry
				consoleHint:mediumDesc = base.modules.sun-cluster:rgBranch.rsPropertyFolder.rsExtPropFolder.rsExtPropTable.rsExtPropTableEntry
				index		= rgName rsName rsPropName

				rowstatus = { [ use GLOBROWNODE ROWSTATUS MANAGED-PROPERTY]
					mediumDesc  = Row Status
                			consoleHint:hidden = true 
                			consoleHint:mediumDesc = base.modules.sun-cluster:rgBranch.rsPropertyFolder.rsExtPropFolder.rsExtPropTable.rsExtPropTableEntry.rowstatus
				}

				rgName	= { [ use GLOBROWNODE STRING MANAGED-PROPERTY ]
					mediumDesc	= Resource Group Name
					consoleHint:mediumDesc = base.modules.sun-cluster:rgBranch.rsPropertyFolder.rsPropTable.rsPropTableEntry.rgName
					required	= true
					dataFormat	= instance 
				}

				rsName	= { [ use GLOBROWNODE STRING MANAGED-PROPERTY ]
					mediumDesc	= Resource Name
					consoleHint:mediumDesc = base.modules.sun-cluster:rgBranch.rsPropertyFolder.rsPropTable.rsPropTableEntry.rsName
					required	= true
					dataFormat	= instance 
				}

				rsPropName	= { [ use GLOBROWNODE STRING MANAGED-PROPERTY ]
					mediumDesc	= Property Name
					consoleHint:mediumDesc = base.modules.sun-cluster:rgBranch.rsPropertyFolder.rsPropTable.rsPropTableEntry.rsPropName
					required	= true
					dataFormat	= instance 
				}

				[ load sun-cluster-rs-prop-d.x ]
			}
		}
	}

	rsTimeoutFolder = { [ use MANAGED-OBJECT ]
		mediumDesc	= Method Timeout
	    	consoleHint:mediumDesc = base.modules.sun-cluster:rgBranch.rsPropertyFolder.rsTimeoutFolder

		rsTimeoutTable = { [ use MANAGED-OBJECT-TABLE ]
			mediumDesc	= Resource Methods Timeouts
			consoleHint:mediumDesc = base.modules.sun-cluster:rgBranch.rsPropertyFolder.rsTimeoutFolder.rsTimeoutTable

			rsTimeoutTableEntry = { [ use MANAGED-OBJECT-TABLE-ENTRY ]
				mediumDesc	= Resource Method Timeouts Table Entry
				consoleHint:mediumDesc = base.modules.sun-cluster:rgBranch.rsPropertyFolder.rsTimeoutFolder.rsTimeoutTable.rsTimeoutTableEntry
				index		= rgName rsName rsPropName

				rowstatus = { [ use GLOBROWNODE ROWSTATUS MANAGED-PROPERTY]
        		           	mediumDesc  = Row Status
                	    		consoleHint:hidden = true 
        	        	    	consoleHint:mediumDesc = base.modules.sun-cluster:rgBranch.rsPropertyFolder.rsTimeoutFolder.rsTimeoutTable.rsTimeoutTableEntry.rowstatus
				}

				rgName	= { [ use GLOBROWNODE STRING MANAGED-PROPERTY ]
					mediumDesc	= Resource Group Name
					consoleHint:mediumDesc = base.modules.sun-cluster:rgBranch.rsPropertyFolder.rsPropTable.rsPropTableEntry.rgName
					required	= true
		   			dataFormat	= instance 
				}

				rsName	= { [ use GLOBROWNODE STRING MANAGED-PROPERTY ]
					mediumDesc	= Resource Name
					consoleHint:mediumDesc = base.modules.sun-cluster:rgBranch.rsPropertyFolder.rsPropTable.rsPropTableEntry.rsName
					required	= true
		   			dataFormat	= instance 
				}

				rsPropName	= { [ use GLOBROWNODE STRING MANAGED-PROPERTY ]
					mediumDesc	= Property Name
					consoleHint:mediumDesc = base.modules.sun-cluster:rgBranch.rsPropertyFolder.rsPropTable.rsPropTableEntry.rsPropName
					required	= true
					dataFormat	= instance 
				}

				[ load sun-cluster-rs-prop-d.x ]
			}
		}
	}
}
