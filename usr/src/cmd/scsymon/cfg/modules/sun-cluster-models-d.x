#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#pragma ident	"@(#)sun-cluster-models-d.x	1.13	08/05/20 SMI"
#
# Copyright (c) 1999 by Sun Microsystems, Inc.
# All rights reserved.
#
# cluster/src/scsymon/cfg/modules/sun-cluster-models-d.x
#

#
# Sun Cluster main module file 
#

type = reference

generalBranch = { [ load sun-cluster-general-models-d.x ] }

nodeBranch = { [ load sun-cluster-node-models-d.x ] }

deviceGroupBranch = { [ load sun-cluster-devgrp-models-d.x ] }

quorumDeviceBranch = { [ load sun-cluster-qdev-models-d.x ] }

transportBranch = { [ load sun-cluster-transport-models-d.x ] }

rtBranch = { [ load sun-cluster-rt-models-d.x ] }

rgBranch = { [ load sun-cluster-rg-models-d.x ]	}







