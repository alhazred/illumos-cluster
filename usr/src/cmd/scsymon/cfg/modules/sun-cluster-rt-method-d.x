#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#pragma ident	"@(#)sun-cluster-rt-method-d.x	1.8	08/05/20 SMI"
#
# Copyright (c) 1999 by Sun Microsystems, Inc.
# All rights reserved.
#
# cluster/src/scsymon/cfg/modules/sun-cluster-rt-method-d.x
#

rtSTART 	= { [ use GLOBROWNODE STRING MANAGED-PROPERTY ]
	mediumDesc	= START
	consoleHint:mediumDesc = base.modules.sun-cluster:rtBranch.definitionFolder.methodFolder.rtMethodTable.rtMethodTableEntry.rtSTART
}

rtSTOP 	= { [ use GLOBROWNODE STRING MANAGED-PROPERTY ]
	mediumDesc	= STOP
	consoleHint:mediumDesc = base.modules.sun-cluster:rtBranch.definitionFolder.methodFolder.rtMethodTable.rtMethodTableEntry.rtSTOP
}

rtPRIMCHANGE = { [ use GLOBROWNODE STRING MANAGED-PROPERTY ]
	mediumDesc	= Primary CHANGED
	consoleHint:mediumDesc = base.modules.sun-cluster:rtBranch.definitionFolder.methodFolder.rtMethodTable.rtMethodTableEntry.rtPRIMCHANGE
	consoleHint:hidden = true
}

rtVALIDATE 	= { [ use GLOBROWNODE STRING MANAGED-PROPERTY ]
	mediumDesc	= VALIDATE
	consoleHint:mediumDesc = base.modules.sun-cluster:rtBranch.definitionFolder.methodFolder.rtMethodTable.rtMethodTableEntry.rtVALIDATE
}

rtUPDATE 	= { [ use GLOBROWNODE STRING MANAGED-PROPERTY ]
	mediumDesc	= UPDATE
	consoleHint:mediumDesc = base.modules.sun-cluster:rtBranch.definitionFolder.methodFolder.rtMethodTable.rtMethodTableEntry.rtUPDATE
}

rtINIT 	= { [ use GLOBROWNODE STRING MANAGED-PROPERTY ]
	mediumDesc	= INIT
	consoleHint:mediumDesc = base.modules.sun-cluster:rtBranch.definitionFolder.methodFolder.rtMethodTable.rtMethodTableEntry.rtINIT
}

rtFINI 	= { [ use GLOBROWNODE STRING MANAGED-PROPERTY ]
	mediumDesc	= FINI
	consoleHint:mediumDesc = base.modules.sun-cluster:rtBranch.definitionFolder.methodFolder.rtMethodTable.rtMethodTableEntry.rtFINI
}

rtBOOT 	= { [ use GLOBROWNODE STRING MANAGED-PROPERTY ]
	mediumDesc	= BOOT
	consoleHint:mediumDesc = base.modules.sun-cluster:rtBranch.definitionFolder.methodFolder.rtMethodTable.rtMethodTableEntry.rtBOOT
}

rtMONINIT 	= { [ use GLOBROWNODE STRING MANAGED-PROPERTY ]
	mediumDesc	= Monitor INIT
	consoleHint:mediumDesc = base.modules.sun-cluster:rtBranch.definitionFolder.methodFolder.rtMethodTable.rtMethodTableEntry.rtMONINIT
	consoleHint:hidden = true
}
			
rtMONSTART 	= { [ use GLOBROWNODE STRING MANAGED-PROPERTY ]
	mediumDesc	= Monitor START
	consoleHint:mediumDesc = base.modules.sun-cluster:rtBranch.definitionFolder.methodFolder.rtMethodTable.rtMethodTableEntry.rtMONSTART
}
			
rtMONSTOP	= { [ use GLOBROWNODE STRING MANAGED-PROPERTY ]
	mediumDesc	= Monitor STOP
	consoleHint:mediumDesc = base.modules.sun-cluster:rtBranch.definitionFolder.methodFolder.rtMethodTable.rtMethodTableEntry.rtMONSTOP
}
			
rtMONCHECK 	= { [ use GLOBROWNODE STRING MANAGED-PROPERTY ]
	mediumDesc	= Monitor CHECK
	consoleHint:mediumDesc = base.modules.sun-cluster:rtBranch.definitionFolder.methodFolder.rtMethodTable.rtMethodTableEntry.rtMONCHECK
}
			
rtPRENETSTART 	= { [ use GLOBROWNODE STRING MANAGED-PROPERTY ]
	mediumDesc	= Prenet START
	consoleHint:mediumDesc = base.modules.sun-cluster:rtBranch.definitionFolder.methodFolder.rtMethodTable.rtMethodTableEntry.rtPRENETSTART
}

rtPOSTNETSTOP	= { [ use GLOBROWNODE STRING MANAGED-PROPERTY ]
	mediumDesc	= Postnet STOP
	consoleHint:mediumDesc = base.modules.sun-cluster:rtBranch.definitionFolder.methodFolder.rtMethodTable.rtMethodTableEntry.rtPOSTNETSTOP
}
