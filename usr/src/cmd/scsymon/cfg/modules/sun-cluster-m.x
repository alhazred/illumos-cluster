#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#pragma ident	"@(#)sun-cluster-m.x	1.6	08/05/20 SMI"
#
# Copyright (c) 1999 by Sun Microsystems, Inc.
# All rights reserved.
#
# cluster/src/scsymon/cfg/modules/sun-cluster-m.x
#

[load default-m.x]

consoleHint:moduleParams(param) = module i18nModuleName i18nModuleDesc version enterprise i18nModuleType
consoleHint:loadable = true


param:module		= sun-cluster
param:moduleName	= Sun Cluster
param:version		= 1.0
param:console		= sun-cluster
param:moduleType	= operatingSystem
param:enterprise	= sun
param:desc		= Sun Cluster module monitors and manages Sun Cluster configuration.

param:location		= .iso.org.dod.internet.private.enterprises.sun.prod.suncluster.sunmc.modules.sunclustermod
param:oid		= 1.3.6.1.4.1.42.2.80.1.1.1


param:i18nModuleName  = base.modules.sun-cluster:moduleName
param:i18nModuleType  = base.modules.sun-cluster:moduleType
param:i18nModuleDesc  = base.modules.sun-cluster:moduleDesc

?param:i18nModuleName?i18n = yes
?param:i18nModuleType?i18n = yes
?param:i18nModuleDesc?i18n = yes
