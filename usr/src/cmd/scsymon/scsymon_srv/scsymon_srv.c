/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#pragma ident	"@(#)scsymon_srv.c	1.40	08/07/24 SMI"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stropts.h>
#include <locale.h>
#include <libintl.h>
#include <door.h>
#include <thread.h>
#include <errno.h>
#include <pthread.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/types.h>
#include <unistd.h>
#include <scadmin/scsymon_srv.h>
#include <sys/clconf_int.h>
#include <libdcs.h>
#include <rgm/rgm_util.h>

#define	SCSYMON_GROUP_NUM 8
typedef enum scsymon_group_index {
	SCSYMON_GROUP_CLUSTER_CONFIG = 0,	/* cluster config */
	SCSYMON_GROUP_DEVGRP_CONFIG,		/* device group config */
	SCSYMON_GROUP_RT_CONFIG,		/* RT config */
	SCSYMON_GROUP_RG_CONFIG,		/* RG config */
	SCSYMON_GROUP_CLUSTER_STATUS,		/* cluster status */
	SCSYMON_GROUP_DEVGRP_STATUS,		/* device group status */
	SCSYMON_GROUP_TRANSPORT_STATUS,		/* transport status */
	SCSYMON_GROUP_RG_STATUS			/* rg status */
} scsymon_group_index_t;

#define	SCSYMON_GROUP_CLUSTER_CONFIG_TEXT	"cluster config"
#define	SCSYMON_GROUP_DEVGRP_CONFIG_TEXT	"devgrp_config"
#define	SCSYMON_GROUP_RT_CONFIG_TEXT		"RT_config"
#define	SCSYMON_GROUP_RG_CONFIG_TEXT		"RG_config"
#define	SCSYMON_GROUP_CLUSTER_STATUS_TEXT	"cluster_status"
#define	SCSYMON_GROUP_DEVGRP_STATUS_TEXT	"devgrp_status"
#define	SCSYMON_GROUP_TRANSPORT_STATUS_TEXT	"transport_status"
#define	SCSYMON_GROUP_RG_STATUS_TEXT		"RG_status"

typedef unsigned int scsymon_table_index_t;

/* cluster config group */
#define	SCSYMON_GROUP_CLUSTER_CONFIG_TABLE_NUM	8
#define	SCSYMON_TABLE_CLUSTER_CONFIG	0	/* cluster config */
#define	SCSYMON_TABLE_NODE_CONFIG	1	/* node config */
#define	SCSYMON_TABLE_NODEDEV_CONFIG	2	/* node device config */
#define	SCSYMON_TABLE_QDEV_CONFIG	3	/* quorum device config */
#define	SCSYMON_TABLE_QDEVPORT_CONFIG	4	/* quorum device port config */
#define	SCSYMON_TABLE_TRANSPORT_ADAPTER_CONFIG 5 /* transport adapter config */
#define	SCSYMON_TABLE_JUNCTION_CONFIG	6	/* switch config */
#define	SCSYMON_TABLE_CABLE_CONFIG	7	/* cable config */

#define	SCSYMON_TABLE_CLUSTER_CONFIG_TEXT	"cluster_config"
#define	SCSYMON_TABLE_NODE_CONFIG_TEXT		"node_config"
#define	SCSYMON_TABLE_NODEDEV_CONFIG_TEXT	"nodedev_config"
#define	SCSYMON_TABLE_QDEV_CONFIG_TEXT		"qdev_config"
#define	SCSYMON_TABLE_QDEVPORT_CONFIG_TEXT	"qdevport_config"
#define	SCSYMON_TABLE_TRANSPORT_ADAPTER_CONFIG_TEXT "tr_adapter_config"
#define	SCSYMON_TABLE_JUNCTION_CONFIG_TEXT	"switch_config"
#define	SCSYMON_TABLE_CABLE_CONFIG_TEXT		"cable_config"

/* device group config group */
#define	SCSYMON_GROUP_DEVGRP_CONFIG_TABLE_NUM	1
#define	SCSYMON_TABLE_DEVGRP_CONFIG	0	/* device group config */

#define	SCSYMON_TABLE_DEVGRP_CONFIG_TEXT	"devgrp_config"

/* rt config */
#define	SCSYMON_GROUP_RT_CONFIG_TABLE_NUM	4
#define	SCSYMON_TABLE_RT_CONFIG		0	/* RT config */
#define	SCSYMON_TABLE_RT_METHOD_CONFIG	1	/* RT method config */
#define	SCSYMON_TABLE_RT_PARAM_CONFIG 	2	/* RT param config */
#define	SCSYMON_TABLE_RT_RS_CONFIG 	3	/* RT Resource config */

#define	SCSYMON_TABLE_RT_CONFIG_TEXT		"RT_config"
#define	SCSYMON_TABLE_RT_METHOD_CONFIG_TEXT	"RT_method_config"
#define	SCSYMON_TABLE_RT_PARAM_CONFIG_TEXT	"RT_param_config"
#define	SCSYMON_TABLE_RT_RS_CONFIG_TEXT		"RT_RS_config"

/* rg config */
#define	SCSYMON_GROUP_RG_CONFIG_TABLE_NUM	10
#define	SCSYMON_TABLE_FORG_RG_CONFIG		0	/* RG config */
#define	SCSYMON_TABLE_FORG_RS_CONFIG		1	/* RS config */
#define	SCSYMON_TABLE_FORG_RS_COM_PROP_CONFIG	2	/* RS com prop config */
#define	SCSYMON_TABLE_FORG_RS_EXT_PROP_CONFIG	3	/* RS ext prop config */
#define	SCSYMON_TABLE_FORG_RS_TIMEOUT_CONFIG	4	/* RS timeout config */
#define	SCSYMON_TABLE_SCRG_RG_CONFIG		5	/* RG config */
#define	SCSYMON_TABLE_SCRG_RS_CONFIG		6	/* RS config */
#define	SCSYMON_TABLE_SCRG_RS_COM_PROP_CONFIG	7	/* RS com prop config */
#define	SCSYMON_TABLE_SCRG_RS_EXT_PROP_CONFIG	8	/* RS ext prop config */
#define	SCSYMON_TABLE_SCRG_RS_TIMEOUT_CONFIG	9	/* RS timeout config */

#define	SCSYMON_TABLE_FORG_RG_CONFIG_TEXT	"FORG_RG_config"
#define	SCSYMON_TABLE_FORG_RS_CONFIG_TEXT	"FORG_RS_config"
#define	SCSYMON_TABLE_FORG_RS_COM_PROP_CONFIG_TEXT "FORG_RS_com_prop_config"
#define	SCSYMON_TABLE_FORG_RS_EXT_PROP_CONFIG_TEXT "FORG_RS_ext_prop_config"
#define	SCSYMON_TABLE_FORG_RS_TIMEOUT_CONFIG_TEXT "FORG_RS_timeout_config"
#define	SCSYMON_TABLE_SCRG_RG_CONFIG_TEXT	"SCRG_RG_config"
#define	SCSYMON_TABLE_SCRG_RS_CONFIG_TEXT	"SCRG_RS_config"
#define	SCSYMON_TABLE_SCRG_RS_COM_PROP_CONFIG_TEXT "SCRG_RS_com_prop_config"
#define	SCSYMON_TABLE_SCRG_RS_EXT_PROP_CONFIG_TEXT "SCRG_RS_ext_prop_config"
#define	SCSYMON_TABLE_SCRG_RS_TIMEOUT_CONFIG_TEXT "SCRG_RS_timeout_config"

/* cluster status */
#define	SCSYMON_GROUP_CLUSTER_STATUS_TABLE_NUM	4
#define	SCSYMON_TABLE_CLUSTER_STATUS	0	/* cluster status */
#define	SCSYMON_TABLE_NODE_STATUS	1	/* node status */
#define	SCSYMON_TABLE_NODEDEV_STATUS	2	/* node device status */
#define	SCSYMON_TABLE_QDEV_STATUS	3	/* quorum device status */

#define	SCSYMON_TABLE_CLUSTER_STATUS_TEXT	"cluster_status"
#define	SCSYMON_TABLE_NODE_STATUS_TEXT		"node_status"
#define	SCSYMON_TABLE_NODEDEV_STATUS_TEXT	"nodedev_status"
#define	SCSYMON_TABLE_QDEV_STATUS_TEXT		"qdev_status"

/* device group status */
#define	SCSYMON_GROUP_DEVGRP_STATUS_TABLE_NUM	2
#define	SCSYMON_TABLE_DEVGRP_STATUS	0	/* device group status */
#define	SCSYMON_TABLE_REPLICA_STATUS	1	/* replica status */

#define	SCSYMON_TABLE_DEVGRP_STATUS_TEXT	"devgrp_status"
#define	SCSYMON_TABLE_REPLICA_STATUS_TEXT	"replica_status"

/* transport status */
#define	SCSYMON_GROUP_TRANSPORT_STATUS_TABLE_NUM	1
#define	SCSYMON_TABLE_PATH_STATUS	0	/* path status */

#define	SCSYMON_TABLE_PATH_STATUS_TEXT		"path_status"

/* rg status */
#define	SCSYMON_GROUP_RG_STATUS_TABLE_NUM	4
#define	SCSYMON_TABLE_FORG_RG_STATUS		0	/* FORG RG status */
#define	SCSYMON_TABLE_FORG_RS_STATUS		1	/* FORG RS status */
#define	SCSYMON_TABLE_SCRG_RG_STATUS		2	/* SCRG RG status */
#define	SCSYMON_TABLE_SCRG_RS_STATUS		3	/* SCRG RS status */

#define	SCSYMON_TABLE_FORG_RG_STATUS_TEXT		"FORG_RG_status"
#define	SCSYMON_TABLE_FORG_RS_STATUS_TEXT		"FORG_RS_status"
#define	SCSYMON_TABLE_SCRG_RG_STATUS_TEXT		"SCRG_RG_status"
#define	SCSYMON_TABLE_SCRG_RS_STATUS_TEXT		"SCRG_RS_status"

#define	SCSYMON_MAX_TIMEOUT_IN_SECONDS 1800	/* 30 minutes */

typedef struct scsymon_table_struct {
	char *pname;		/* table name */
	void *pdata;		/* data in structure */
	char *pstring;		/* data in string */
	unsigned int size;	/* string buffer size */
} scsymon_table_t;

typedef struct scsymon_group_struct {
	char *pname;		/* group name */
	unsigned int timeout;	/* refresh interval in seconds, 0 if never */
	/* signalled by server thread if user request refresh */
	unsigned int table_count; /* number of tables */
	scsymon_table_t *pltables; /* array of tables in this group */
	thread_t thread_id; 	/* thread id */
	cond_t cond_refresh;
	mutex_t mutex_refresh;	/* mutex - work with conditional variable */
	/* broadcasted by collection thread when string is ready */
	cond_t cond_ready;
	mutex_t mutex_ready;	/* work with conditional variable */
	int	callback_pending;
} scsymon_group_t;

/* global variables */
char *progname;
thread_key_t rbuf_key;
scconf_cfg_cluster_t *pcluster_cfg = NULL;
rwlock_t rwlock_config; /* readers/writer lock to lock the configuration */
rwlock_t rwlock_string; /* readers/writer lock to lock the output string */
mutex_t mutex_group_data; /* mutex lock to lock the group data */
sema_t sema_exit; /* incremented when errors are found in group threads */
/* lint -e785 */
scsymon_group_t group[SCSYMON_GROUP_NUM] = {
	{SCSYMON_GROUP_CLUSTER_CONFIG_TEXT, 0,
	    SCSYMON_GROUP_CLUSTER_CONFIG_TABLE_NUM},
	{SCSYMON_GROUP_DEVGRP_CONFIG_TEXT, 0,
	    SCSYMON_GROUP_DEVGRP_CONFIG_TABLE_NUM},
	{SCSYMON_GROUP_RT_CONFIG_TEXT, 0,
	    SCSYMON_GROUP_RT_CONFIG_TABLE_NUM},
	{SCSYMON_GROUP_RG_CONFIG_TEXT, 0,
	    SCSYMON_GROUP_RG_CONFIG_TABLE_NUM},
	{SCSYMON_GROUP_CLUSTER_STATUS_TEXT, 120,
	    SCSYMON_GROUP_CLUSTER_STATUS_TABLE_NUM},
	{SCSYMON_GROUP_DEVGRP_STATUS_TEXT, 120,
	    SCSYMON_GROUP_DEVGRP_STATUS_TABLE_NUM},
	{SCSYMON_GROUP_TRANSPORT_STATUS_TEXT, 120,
	    SCSYMON_GROUP_TRANSPORT_STATUS_TABLE_NUM},
	{SCSYMON_GROUP_RG_STATUS_TEXT, 120,
	    SCSYMON_GROUP_RG_STATUS_TABLE_NUM}
};
/* lint +e785 */

/* functions */
extern void print_error(scsymon_errno_t);
extern void make_daemon(void);
extern void notify_group_ccr_change(scsymon_group_index_t);
extern void ccr_change_callback(const char *, const char *,
    clconf_ccr_update_t);
extern void get_string(void *, char *, unsigned int, door_desc_t *,
    unsigned int);
extern void free_key(void *);
extern void *get_data(void *);
extern scsymon_errno_t get_index_from_table_name(char *,
    scsymon_group_index_t *, scsymon_table_index_t *);
extern char *get_table_name(scsymon_group_index_t, scsymon_table_index_t);
extern void get_group_data(scsymon_group_index_t);
extern scsymon_errno_t get_cluster_data(scsymon_group_index_t, void **);
extern void free_cluster_data(scsymon_group_index_t, void *);
extern scsymon_errno_t get_table_data(scsymon_group_index_t,
    scsymon_table_index_t, void *);
extern void clean_group_data(scsymon_group_index_t);
extern void clean_table_data(scsymon_group_index_t,
    scsymon_table_index_t);
extern void make_group_string(scsymon_group_index_t);
extern void make_table_string(scsymon_group_index_t, scsymon_table_index_t);

/* ARGSUSED */
int
main(int argc, char *argv[])
{
	int did;
	thread_t departed;
	struct stat buf;
	char *dir_name = NULL;
	char *end;
	int fd;
	void *status = NULL;
	int error;
	scsymon_errno_t scsymon_error = SCSYMON_ENOERR;
	scconf_errno_t scconf_error = SCCONF_NOERR;
	clconf_errnum_t	clconf_error = CL_NOERROR;
	unsigned int i, j;

	/* Set the program name */
	progname = strrchr(argv[0], '/');
	if (progname == NULL) {
		progname = argv[0];
	} else {
		++progname;
	}

	/* I18N housekeeping */
	(void) setlocale(LC_ALL, "");
	(void) textdomain(TEXT_DOMAIN);
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	/* only run it as a daemon when DEBUG_SCSYMON is not set */
	if (getenv("DEBUG_SCSYMON") == NULL)
		make_daemon();

	/* initialize ORB */
	scsymon_dbg_print("initialize ORB\n");
	error = clconf_lib_init();
	if (error != 0) {
		scsymon_error = SCSYMON_EORBINIT;
		goto cleanup;
	}

	/* disable server cancel state */
	scsymon_dbg_print("set PTHREAD_CANCEL_DISABLE\n");
	(void) pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL);

	/* create a door descriptor */
	scsymon_dbg_print("create door\n");
	if ((did = door_create(get_string, 0, 0)) < 0) {
		scsymon_error = SCSYMON_EDOORCREATE;

		goto cleanup;
	}

	/* unbind it first */
	scsymon_dbg_print("unbind door\n");
	(void) fdetach(SCSYMON_DOOR_FILE);

	/* create door file if not exist */
	dir_name = strdup(SCSYMON_DOOR_FILE);
	end = strrchr(dir_name, '/');
	*end = '\0';
	if (stat(dir_name, &buf) < 0) { /* dir not exist */
		if (mkdir(dir_name, S_IRWXU) != 0) { /* unable to create dir */
			scsymon_error = SCSYMON_EMKDOORDIR;

			goto cleanup;
		}
	}

	fd = open(SCSYMON_DOOR_FILE, O_WRONLY | O_CREAT);
	if (fd == -1) { /* unable to create file */
		scsymon_error = SCSYMON_ECREATEDOORFILE;

		goto cleanup;
	}

	/* bind it to the name space */
	scsymon_dbg_print("rebind door\n");
	if (fattach(did, SCSYMON_DOOR_FILE) < 0) {
		scsymon_error = SCSYMON_EDOORBIND;

		goto cleanup;
	}

	scconf_error = scconf_get_clusterconfig(&pcluster_cfg);
	scsymon_error = scsymon_convert_scconf_error_code(scconf_error);
	if (error != SCSYMON_ENOERR) {
		goto cleanup;
	}

	/* initialize global synchronization objects */
	scsymon_dbg_print("initialize rwlock_config\n");
	if (rwlock_init(&rwlock_config, USYNC_THREAD, NULL) != 0) {
		scsymon_error = SCSYMON_EUNEXPECTED;

		goto cleanup;
	}

	scsymon_dbg_print("initialize rwlock_string\n");
	if (rwlock_init(&rwlock_string, USYNC_THREAD, NULL) != 0) {
		scsymon_error = SCSYMON_EUNEXPECTED;

		goto cleanup;
	}

	scsymon_dbg_print("initialize mutex_group_data\n");
	if (mutex_init(&mutex_group_data, USYNC_THREAD, NULL) != 0) {
		scsymon_error = SCSYMON_EUNEXPECTED;

		goto cleanup;
	}

	/* initialize the count to SCSYMON_GROUP_NUM */
	scsymon_dbg_print("initialize semaphore\n");
	if (sema_init(&sema_exit, SCSYMON_GROUP_NUM, USYNC_THREAD, NULL) != 0) {
		scsymon_error = SCSYMON_EUNEXPECTED;

		goto cleanup;
	}

	/* decrement the count to 0 */
	scsymon_dbg_print("decrement semaphore count to 0\n");
	for (i = 0; i < SCSYMON_GROUP_NUM; i++)
		(void) sema_trywait(&sema_exit);

	/* initialize group */
	for (i = 0; i < SCSYMON_GROUP_NUM; i++) {
		scsymon_dbg_print("initialize group %s\n",
		    group[i].pname);

		/* initialize synchronization object */
		scsymon_dbg_print(\
		    "initialize conditional variable cond_refresh\n");
		if (cond_init(&group[i].cond_refresh, USYNC_THREAD, NULL)
		    != 0) {
			scsymon_error = SCSYMON_EUNEXPECTED;

			goto cleanup;
		}
		scsymon_dbg_print("initialize mutex mutex_refresh\n");
		if (mutex_init(&group[i].mutex_refresh, USYNC_THREAD, NULL)
		    != 0) {
			scsymon_error = SCSYMON_EUNEXPECTED;

			goto cleanup;
		}
		scsymon_dbg_print(\
		    "initialize conditional variable cond_ready\n");
		if (cond_init(&group[i].cond_ready, USYNC_THREAD, NULL) != 0) {
			scsymon_error = SCSYMON_EUNEXPECTED;

			goto cleanup;
		}
		scsymon_dbg_print("initialize mutex mutex_ready\n");
		if (mutex_init(&group[i].mutex_ready, USYNC_THREAD, NULL)
		    != 0) {
			scsymon_error = SCSYMON_EUNEXPECTED;

			goto cleanup;
		}
		group[i].callback_pending = 0;

		/* initialize table array */
		group[i].pltables =
		    (scsymon_table_t *)calloc(group[i].table_count,
		    sizeof (scsymon_table_t));
		if (group[i].pltables == NULL) {
			scsymon_error = SCSYMON_ENOMEM;

			goto cleanup;
		}

		for (j = 0; j < group[i].table_count; j++) {
			group[i].pltables[j].pname =
				get_table_name((scsymon_group_index_t)i, j);
			group[i].pltables[j].pdata = NULL;
			group[i].pltables[j].size = SCSYMON_MAX_STRING_LEN;
			group[i].pltables[j].pstring = (char *)calloc(1,
			    group[i].pltables[j].size);
			if (group[i].pltables[j].pstring == NULL) {
				scsymon_error = SCSYMON_ENOMEM;

				goto cleanup;
			}
		}
	}

	/* create group threads */
	for (i = 0; i < SCSYMON_GROUP_NUM; i++) {
		scsymon_dbg_print("create thread %s\n", group[i].pname);
		error = thr_create(NULL, 0, get_data, (void *)i, 0,
		    &group[i].thread_id);
		if (error != 0) {
			scsymon_error = SCSYMON_EUNEXPECTED;

			goto cleanup;
		} else
			scsymon_dbg_print("group thread is created \n");
	}

	/* register callback with the CCR */
	scsymon_dbg_print("register callback function\n");
	clconf_error = clconf_register_ccr_callback(ccr_change_callback);
	if (clconf_error != CL_NOERROR) {
		scsymon_dbg_print(\
		    "error registering callback function\n");
		goto cleanup;
	}

	/* now hang out and wait for incoming invocations or errors */
	scsymon_dbg_print("wait for sema_exit\n");
	(void) sema_wait(&sema_exit);

	scsymon_dbg_print("error found in group threads \n");

	/* destroy synchronization objects */
	scsymon_dbg_print("destroy rwlock_config\n");
	if (rwlock_destroy(&rwlock_config) != 0) {
		scsymon_error = SCSYMON_EUNEXPECTED;

		goto cleanup;
	}

	scsymon_dbg_print("destroy rwlock_string\n");
	if (rwlock_destroy(&rwlock_string) != 0) {
		scsymon_error = SCSYMON_EUNEXPECTED;

		goto cleanup;
	}

	scsymon_dbg_print("destroy mutex_group_data\n");
	if (mutex_destroy(&mutex_group_data) != 0) {
		scsymon_error = SCSYMON_EUNEXPECTED;

		goto cleanup;
	}

	scsymon_dbg_print("destroy semaphore\n");
	if (sema_destroy(&sema_exit) != 0) {
		scsymon_error = SCSYMON_EUNEXPECTED;

		goto cleanup;
	}

	/* wait for all group threads terminated */
	for (i = 0; i < SCSYMON_GROUP_NUM; i++) {
		scsymon_dbg_print("waiting for group thread to exit \n");
		if (thr_join(group[i].thread_id, &departed, &status) != 0) {
			scsymon_error = SCSYMON_EUNEXPECTED;

			goto cleanup;
		}
		scsymon_dbg_print("group thread exit status = %d \n",
		    *(int *)status);
	}

	/* free up group structure */
	for (i = 0; i < SCSYMON_GROUP_NUM; i++) {
		scsymon_dbg_print("free group %s\n", group[i].pname);

		/* destroy synchronization object */
		scsymon_dbg_print(\
		    "destroy conditional variable cond_refresh\n");
		if (cond_destroy(&group[i].cond_refresh) != 0) {
			scsymon_error = SCSYMON_EUNEXPECTED;

			goto cleanup;
		}

		scsymon_dbg_print("destroy mutex mutex_refresh\n");
		if (mutex_destroy(&group[i].mutex_refresh) != 0) {
			scsymon_error = SCSYMON_EUNEXPECTED;

			goto cleanup;
		}

		scsymon_dbg_print(\
		    "destroy conditional variable cond_ready\n");
		if (cond_destroy(&group[i].cond_ready) != 0) {
			scsymon_error = SCSYMON_EUNEXPECTED;

			goto cleanup;
		}

		scsymon_dbg_print("destroy mutex mutex_ready\n");
		if (mutex_destroy(&group[i].mutex_ready) != 0) {
			scsymon_error = SCSYMON_EUNEXPECTED;

			goto cleanup;
		}

		/* free table string */
		for (j = 0; j < group[i].table_count; j++)
			free(group[i].pltables[j].pstring);

		/* free table array */
		free(group[i].pltables);
	}

cleanup:
	/* unregister a previously registered callback function */
	scsymon_dbg_print("unregister callback function\n");
	clconf_error = clconf_unregister_ccr_callback();
	if (clconf_error != CL_NOERROR)
		scsymon_dbg_print(\
		    "error unregistering callback function\n");

	if (pcluster_cfg != NULL)
		scconf_free_clusterconfig(pcluster_cfg);

	/* shutdown the ORB */
	scsymon_dbg_print("shutdown the ORB\n");
	clconf_lib_shutdown();

	if (dir_name != NULL)
		free(dir_name);

	if (scsymon_error != SCSYMON_ENOERR)
		print_error(scsymon_error);

	scsymon_dbg_print("exit the main thread \n");

	return (scsymon_error);
}

/*
 * print_error
 *
 *	Print error to stderr.
 */
void
print_error(scsymon_errno_t error)
{
	char text[SCSYMON_MAX_STRING_LEN];

	scsymon_strerr(error, text);
	(void) fprintf(stderr, "%s:  %s.\n", progname, text);
}

/*
 * Create a daemon process and detach it from the controlling terminal.
 */
void
make_daemon()
{
	scsymon_errno_t error = SCSYMON_ENOERR;
	struct rlimit rl;
	unsigned int size;
	unsigned int i;
	int	fd;
	pid_t	pd;
	pid_t	rc;

	pd = fork();
	if (pd < 0) {
		error = SCSYMON_EFORK;
		goto cleanup;
	}

	/*
	 * parent exits after fork
	 */
	if (pd > 0)
		exit(0);		/* parent */

	/*
	 * child closes stdin and redirects stdout,stderr file descriptors
	 */
	(void) close(0);
	(void) close(1);
	(void) close(2);

	fd = open("/dev/console", O_WRONLY);
	if (fd == -1) {
		error = SCSYMON_EOPENDEVCONSOLE;
		goto cleanup;
	}
	(void) dup2(fd, 1);
	(void) dup2(fd, 2);
	(void) close(fd);

	/*
	 * child closes rest of file descriptors
	 */
	rl.rlim_max = 0;
	(void) getrlimit(RLIMIT_NOFILE, &rl);
	if ((size = rl.rlim_max) == 0)
		exit(1);

	for (i = 3; i < size; i++)
		(void) close((int)i);

	/*
	 * child sets this process as group leader
	 */
	rc = setsid();
	if (rc == (pid_t)-1) {
		error = SCSYMON_ESETSID;
		goto cleanup;
	}

	(void) umask(0);	/* clear out file mode creation mask */

cleanup:
	if (error != SCSYMON_ENOERR) {
		print_error(error);
		exit(1);
	}
}

/*
 * called by callback function to wake up a group.
 */
/* ARGSUSED */
void
notify_group_ccr_change(scsymon_group_index_t group_index)
{
	cond_t *pcond_refresh;
	mutex_t *pmutex_refresh;

	/*
	 * if any of the synchronization object is not initialized,
	 * the lock function should return an error.
	 */
	pcond_refresh = &group[group_index].cond_refresh;
	pmutex_refresh = &group[group_index].mutex_refresh;

	/* signal cond_refresh */
	/*
	 * No need to trylock here because the lock is not held
	 * for long time and we do not wait/sleep with lock held
	 */
	(void) mutex_lock(pmutex_refresh);
	group[group_index].callback_pending = 1;
	(void) cond_signal(pcond_refresh);
	/*
	 * scsymon_dbg_print(\
	 *	  "notify_group_ccr_change: cond_refresh signaled\n");
	 */
	/* printf("notify_group_ccr_change: cond_refresh signaled\n"); */

	(void) mutex_unlock(pmutex_refresh);
}

/*
 * callback function registered for any CCR updates
 * This function is called when any CCR updates occur.
 */
/* ARGSUSED */
void
ccr_change_callback(const char *cluster, const char *name,
    clconf_ccr_update_t flag)
{
	scsymon_group_index_t group_index = SCSYMON_GROUP_CLUSTER_CONFIG;

	scsymon_dbg_print("entering ccr_change_callback\n");
	/* printf("entering ccr_change_callback\n"); */

	/*
	 * we do not do anything for virtual clusters.
	 */
	if (cluster == NULL || strcmp(cluster, "global") != 0) {
		return;
	}

	if (name == NULL)
		return;

	if (strncmp(name, DCS_CCR_TABLE_PREFIX,
	    strlen(DCS_CCR_TABLE_PREFIX)) == 0) {
		group_index = SCSYMON_GROUP_DEVGRP_CONFIG;
	} else if (strncmp(name, RGM_PREFIX_RG, strlen(RGM_PREFIX_RG)) == 0) {
		group_index = SCSYMON_GROUP_RG_CONFIG;
	} else if (strncmp(name, RGM_PREFIX_RT, strlen(RGM_PREFIX_RT)) == 0) {
		group_index = SCSYMON_GROUP_RT_CONFIG;
	}

	notify_group_ccr_change(group_index);

	/* Need to refresh related groups */
	if (group_index == SCSYMON_GROUP_DEVGRP_CONFIG ||
		group_index == SCSYMON_GROUP_RG_CONFIG)
		notify_group_ccr_change(SCSYMON_GROUP_CLUSTER_CONFIG);

	if (group_index == SCSYMON_GROUP_RG_CONFIG)
		notify_group_ccr_change(SCSYMON_GROUP_RT_CONFIG);

	scsymon_dbg_print("leaving ccr_change_callback\n");
	/* printf("leaving ccr_change_callback\n"); */
}

/* Exported server function */
/* ARGSUSED */
void
get_string(void *cookie, char *argp, unsigned int arg_size,
    door_desc_t *dp, unsigned int n_descriptors)
{
	char *rbuf = NULL;
	static int once = 0;
	static mutex_t lock;
	int error = 0;
	scsymon_errno_t scsymon_error = SCSYMON_ENOERR;
	scsymon_group_index_t group_index;
	scsymon_table_index_t table_index;
	cond_t *pcond_refresh;
	mutex_t *pmutex_refresh;
	cond_t *pcond_ready;
	mutex_t *pmutex_ready;
	timestruc_t timeout;
	int user_request_refresh = 0;
	scsymon_table_t *ptable;

	scsymon_dbg_print("entering get_string\n");
	scsymon_dbg_print("%s\n", argp);

	/* initialize thread-specific rbuf */
	error = mutex_lock(&lock);
	if (error == 0) {
		if (!once) {
			once = 1;
			error = thr_keycreate(&rbuf_key, free_key);
			if (error != 0) {
				scsymon_error = SCSYMON_EUNEXPECTED;
				print_error(scsymon_error);
				goto cleanup;
			}
		}
		(void) mutex_unlock(&lock);
	} else {
		scsymon_error = SCSYMON_EUNEXPECTED;
		print_error(scsymon_error);
		goto cleanup;
	}

	/* free thread-specific rbuf */
	error = thr_getspecific(rbuf_key, (void **)&rbuf);
	if (error != 0) {
		scsymon_error = SCSYMON_EUNEXPECTED;
		print_error(scsymon_error);
	}
	if (rbuf != NULL) {
		free(rbuf);
		rbuf = NULL;
	}

	/* clear value stored in rbuf_key so we don't free it again */
	error = thr_setspecific(rbuf_key, rbuf);
	if (error != 0) {
		scsymon_error = SCSYMON_EUNEXPECTED;
		print_error(scsymon_error);
	}

	timeout.tv_sec = time(NULL) + SCSYMON_MAX_TIMEOUT_IN_SECONDS;
	timeout.tv_nsec = 0;

	scsymon_error = get_index_from_table_name(argp, &group_index,
	    &table_index);
	scsymon_dbg_print(\
	    "get_string: group_index = %d table_index = %d\n",
	    group_index, table_index);

	if (scsymon_error != SCSYMON_ENOERR)
		goto cleanup;

	ptable = &group[group_index].pltables[table_index];

	if (!user_request_refresh) { /* not user refresh request */
		scsymon_dbg_print("get_string: locking rdlock\n");
		if ((error = rw_rdlock(&rwlock_string)) == 0) {
			scsymon_dbg_print("get_string: rdlock locked\n");
			/* read string into rbuf */
			rbuf = strdup(ptable->pstring);
			if (rbuf == NULL) {
				scsymon_error = SCSYMON_ENOMEM;
				print_error(scsymon_error);
			}
			/* set rbuf thread-specific */
			error = thr_setspecific(rbuf_key, rbuf);
			if (error != 0) {
				scsymon_error = SCSYMON_EUNEXPECTED;
				print_error(scsymon_error);
			}

			scsymon_dbg_print(\
			    "get_string: unlocking rdlock\n");
			error = rw_unlock(&rwlock_string);
			if (error == 0)
				scsymon_dbg_print(\
				    "get_string: rdlock unlocked\n");
			else {
				scsymon_error = SCSYMON_EUNEXPECTED;
				print_error(scsymon_error);
			}
		} else {
			scsymon_error = SCSYMON_EUNEXPECTED;
			print_error(scsymon_error);
		}
	} else {
		/*
		 * if any of the synchronization object is not initialized,
		 * the lock function should return an error.
		 */
		pcond_refresh = &group[group_index].cond_refresh;
		pmutex_refresh = &group[group_index].mutex_refresh;
		pcond_ready = &group[group_index].cond_ready;
		pmutex_ready = &group[group_index].mutex_ready;

		/* signal cond_refresh */
		(void) mutex_lock(pmutex_refresh);
		(void) cond_signal(pcond_refresh);

		scsymon_dbg_print("get_string: cond_refresh signaled\n");

		/*
		 * lock mutex_ready before unlock mutex_refresh, or we might
		 * lose the broadcast
		 */
		(void) mutex_lock(pmutex_ready);
		(void) mutex_unlock(pmutex_refresh);

		/* wait for cond_ready */
		scsymon_dbg_print(\
		    "get_string: waiting for cond_ready\n");
		error = cond_timedwait(pcond_ready, pmutex_ready, &timeout);
		if (error == 0) {
			scsymon_dbg_print(\
			    "get_string: cond_ready broadcasted\n");
			scsymon_dbg_print(\
			    "get_string: locking rdlock\n");
			if ((error = rw_rdlock(&rwlock_string)) == 0) {
				scsymon_dbg_print(\
				    "get_string: rdlock locked\n");

				/* read string into rbuf */
				rbuf = strdup(ptable->pstring);
				if (rbuf == NULL) {
					scsymon_error = SCSYMON_ENOMEM;
					print_error(scsymon_error);
				}

				/* set rbuf thread-specific */
				error = thr_setspecific(rbuf_key, rbuf);
				if (error != 0) {
					scsymon_error = SCSYMON_EUNEXPECTED;
					print_error(scsymon_error);
				}

				scsymon_dbg_print(\
				    "get_string: unlocking rdlock\n");
				error = rw_unlock(&rwlock_string);
				if (error == 0)
					scsymon_dbg_print(\
					    "get_string: rdlock unlocked\n");
				else {
					scsymon_error = SCSYMON_EUNEXPECTED;
					print_error(scsymon_error);
				}
			} else {
				scsymon_error = SCSYMON_EUNEXPECTED;
				print_error(scsymon_error);
			}
		} else if (error == ETIME)
			scsymon_dbg_print(\
			    "get_string: cond_ready timeout\n");
		else {
			scsymon_error = SCSYMON_EUNEXPECTED;
			print_error(scsymon_error);
		}

		(void) mutex_unlock(pmutex_ready);
	}

cleanup:
	if (rbuf == NULL)
		(void) door_return(NULL, 0, NULL, 0);
	else
		(void) door_return(rbuf, strlen(rbuf), NULL, 0);
}

/* free rbuf when server thread exits */
void
free_key(void *rbuf)
{
	scsymon_dbg_print("free_key!!!\n");
	free(rbuf);
}

/* get cluster data */
void *
get_data(void *arg)
{
	int error = 0;
	scsymon_errno_t scsymon_error = SCSYMON_ENOERR;
	scsymon_group_index_t group_index = (int)arg;
	char *group_name = group[group_index].pname;
	unsigned int interval = group[group_index].timeout;
	cond_t *pcond_refresh = &group[group_index].cond_refresh;
	mutex_t *pmutex_refresh = &group[group_index].mutex_refresh;
	cond_t *pcond_ready = &group[group_index].cond_ready;
	mutex_t *pmutex_ready = &group[group_index].mutex_ready;
	timestruc_t timeout;
	unsigned int i;
	boolean_t error_in_sibling = B_FALSE;

	scsymon_dbg_print("entering get_data: group_index = %d\n",
	    group_index);
	timeout.tv_sec = time(NULL);
	timeout.tv_nsec = 0;

	/* wait for either timeout or refresh request */
	scsymon_dbg_print("%s: waiting for cond_refresh\n", group_name);
	(void) mutex_lock(pmutex_refresh);
	error = cond_timedwait(pcond_refresh, pmutex_refresh, &timeout);

	while ((error == 0) || (error == ETIME)) {
		group[group_index].callback_pending = 0;
		(void) mutex_unlock(pmutex_refresh);

		if (error == ETIME)
			scsymon_dbg_print("%s: cond_refresh timeout\n",
			    group_name);
		else
			scsymon_dbg_print("%s: cond_refresh signaled\n",
			    group_name);

		get_group_data(group_index);

		scsymon_dbg_print("%s: locking rwlock\n", group_name);
		if ((error = rw_wrlock(&rwlock_string)) == 0) {
			scsymon_dbg_print("%s: rwlock locked \n",
			    group_name);

			/* format string */
			make_group_string(group_index);

			scsymon_dbg_print("%s: unlocking rwlock\n",
			    group_name);
			error = rw_unlock(&rwlock_string);
			if (error != 0) {
				scsymon_error = SCSYMON_EUNEXPECTED;
				print_error(scsymon_error);

				break;
			} else
				scsymon_dbg_print(\
				    "%s: rwlock unlocked \n", group_name);
		} else {
			scsymon_error = SCSYMON_EUNEXPECTED;
			print_error(scsymon_error);

			break;
		}

		/*
		 * broadcast cond_ready to all get_string threads which are
		 * waiting for cond_ready
		 */
		(void) mutex_lock(pmutex_ready);
		(void) cond_broadcast(pcond_ready);
		(void) mutex_unlock(pmutex_ready);
		scsymon_dbg_print("%s: cond_ready broadcasted\n",
		    group_name);

		/* test if error occurs in sibling threads */
		scsymon_dbg_print("====== %s: test semaphore \n",
		    group_name);
		error = sema_trywait(&sema_exit);
		if (error != EBUSY) {
			scsymon_dbg_print("%s: semaphore > 0 \n",
			    group_name);

			error_in_sibling = B_TRUE;
			scsymon_error = SCSYMON_EUNEXPECTED;
			print_error(scsymon_error);

			break;
		}
		else
			scsymon_dbg_print("%s: semaphore == 0 \n",
			    group_name);

		error = 0;
		thr_yield();

		/* wait for next refresh request or timeout */
		timeout.tv_sec = time(NULL) + (time_t)interval;
		(void) mutex_lock(pmutex_refresh);

		/*
		 * Do not wait if callbacks are pending.
		 */
		if (group[group_index].callback_pending == 0) {
			scsymon_dbg_print("%s: waiting for"
			    " cond_refresh\n", group_name);
			if (interval == 0)
				error = cond_wait(pcond_refresh,
				    pmutex_refresh);
			else
				error = cond_timedwait(pcond_refresh,
				    pmutex_refresh, &timeout);
		}
	}
	(void) mutex_unlock(pmutex_refresh);

	clean_group_data(group_index);
	/*
	 * increment semaphore by SCSYMON_GROUP_NUM since the sibling thread
	 * and the main thread are waiting on it
	 */
	scsymon_dbg_print("%s: error - increment semaphore\n",
	    group_name);
	for (i = 0; i < SCSYMON_GROUP_NUM; i++) {
		if (error_in_sibling == B_FALSE) {
			/* increment semaphore */
			(void) sema_post(&sema_exit);
		}

		/* wake up sibling but not self */
		if (i == group_index)
			continue;

		pcond_refresh = &group[i].cond_refresh;
		pmutex_refresh = &group[i].mutex_refresh;
		(void) mutex_lock(pmutex_refresh);
		(void) cond_signal(pcond_refresh);
		(void) mutex_unlock(pmutex_refresh);
	}

	scsymon_dbg_print("%s: thread exit error = %d\n", group_name,
	    error);
	thr_exit((void *)&error);

	return ((void *)NULL);
}

scsymon_errno_t
get_index_from_table_name(char *table_name,
    scsymon_group_index_t *pgroup_index, scsymon_table_index_t *ptable_index)
{
	scsymon_errno_t error = SCSYMON_EINVAL;

	/* map table name to group index and table index */
	/* cluster config group */
	if (strcmp(table_name, SCSYMON_TABLE_CLUSTER_CONFIG_TEXT) == 0) {
		*pgroup_index = SCSYMON_GROUP_CLUSTER_CONFIG;
		*ptable_index = SCSYMON_TABLE_CLUSTER_CONFIG;
		error = SCSYMON_ENOERR;
		goto cleanup;
	}

	if (strcmp(table_name, SCSYMON_TABLE_NODE_CONFIG_TEXT) == 0) {
		*pgroup_index = SCSYMON_GROUP_CLUSTER_CONFIG;
		*ptable_index = SCSYMON_TABLE_NODE_CONFIG;
		error = SCSYMON_ENOERR;
		goto cleanup;
	}

	if (strcmp(table_name, SCSYMON_TABLE_NODEDEV_CONFIG_TEXT) == 0) {
		*pgroup_index = SCSYMON_GROUP_CLUSTER_CONFIG;
		*ptable_index = SCSYMON_TABLE_NODEDEV_CONFIG;
		error = SCSYMON_ENOERR;
		goto cleanup;
	}

	if (strcmp(table_name, SCSYMON_TABLE_QDEV_CONFIG_TEXT) == 0) {
		*pgroup_index = SCSYMON_GROUP_CLUSTER_CONFIG;
		*ptable_index = SCSYMON_TABLE_QDEV_CONFIG;
		error = SCSYMON_ENOERR;
		goto cleanup;
	}

	if (strcmp(table_name, SCSYMON_TABLE_QDEVPORT_CONFIG_TEXT) == 0) {
		*pgroup_index = SCSYMON_GROUP_CLUSTER_CONFIG;
		*ptable_index = SCSYMON_TABLE_QDEVPORT_CONFIG;
		error = SCSYMON_ENOERR;
		goto cleanup;
	}

	if (strcmp(table_name, SCSYMON_TABLE_TRANSPORT_ADAPTER_CONFIG_TEXT)
	    == 0) {
		*pgroup_index = SCSYMON_GROUP_CLUSTER_CONFIG;
		*ptable_index = SCSYMON_TABLE_TRANSPORT_ADAPTER_CONFIG;
		error = SCSYMON_ENOERR;
		goto cleanup;
	}

	if (strcmp(table_name, SCSYMON_TABLE_JUNCTION_CONFIG_TEXT) == 0) {
		*pgroup_index = SCSYMON_GROUP_CLUSTER_CONFIG;
		*ptable_index = SCSYMON_TABLE_JUNCTION_CONFIG;
		error = SCSYMON_ENOERR;
		goto cleanup;
	}

	if (strcmp(table_name, SCSYMON_TABLE_CABLE_CONFIG_TEXT) == 0) {
		*pgroup_index = SCSYMON_GROUP_CLUSTER_CONFIG;
		*ptable_index = SCSYMON_TABLE_CABLE_CONFIG;
		error = SCSYMON_ENOERR;
		goto cleanup;
	}

	/* Device group config */
	if (strcmp(table_name, SCSYMON_TABLE_DEVGRP_CONFIG_TEXT) == 0) {
		*pgroup_index = SCSYMON_GROUP_DEVGRP_CONFIG;
		*ptable_index = SCSYMON_TABLE_DEVGRP_CONFIG;
		error = SCSYMON_ENOERR;
		goto cleanup;
	}

	/* RT config group */
	if (strcmp(table_name, SCSYMON_TABLE_RT_CONFIG_TEXT) == 0) {
		*pgroup_index = SCSYMON_GROUP_RT_CONFIG;
		*ptable_index = SCSYMON_TABLE_RT_CONFIG;
		error = SCSYMON_ENOERR;
		goto cleanup;
	}

	if (strcmp(table_name, SCSYMON_TABLE_RT_METHOD_CONFIG_TEXT) == 0) {
		*pgroup_index = SCSYMON_GROUP_RT_CONFIG;
		*ptable_index = SCSYMON_TABLE_RT_METHOD_CONFIG;
		error = SCSYMON_ENOERR;
		goto cleanup;
	}

	if (strcmp(table_name, SCSYMON_TABLE_RT_PARAM_CONFIG_TEXT) == 0) {
		*pgroup_index = SCSYMON_GROUP_RT_CONFIG;
		*ptable_index = SCSYMON_TABLE_RT_PARAM_CONFIG;
		error = SCSYMON_ENOERR;
		goto cleanup;
	}

	if (strcmp(table_name, SCSYMON_TABLE_RT_RS_CONFIG_TEXT) == 0) {
		*pgroup_index = SCSYMON_GROUP_RT_CONFIG;
		*ptable_index = SCSYMON_TABLE_RT_RS_CONFIG;
		error = SCSYMON_ENOERR;
		goto cleanup;
	}

	/* RG config group */
	if (strcmp(table_name, SCSYMON_TABLE_FORG_RG_CONFIG_TEXT) == 0) {
		*pgroup_index = SCSYMON_GROUP_RG_CONFIG;
		*ptable_index = SCSYMON_TABLE_FORG_RG_CONFIG;
		error = SCSYMON_ENOERR;
		goto cleanup;
	}

	if (strcmp(table_name, SCSYMON_TABLE_FORG_RS_CONFIG_TEXT) == 0) {
		*pgroup_index = SCSYMON_GROUP_RG_CONFIG;
		*ptable_index = SCSYMON_TABLE_FORG_RS_CONFIG;
		error = SCSYMON_ENOERR;
		goto cleanup;
	}

	if (strcmp(table_name, SCSYMON_TABLE_FORG_RS_COM_PROP_CONFIG_TEXT)
	    == 0) {
		*pgroup_index = SCSYMON_GROUP_RG_CONFIG;
		*ptable_index = SCSYMON_TABLE_FORG_RS_COM_PROP_CONFIG;
		error = SCSYMON_ENOERR;
		goto cleanup;
	}

	if (strcmp(table_name, SCSYMON_TABLE_FORG_RS_EXT_PROP_CONFIG_TEXT)
	    == 0) {
		*pgroup_index = SCSYMON_GROUP_RG_CONFIG;
		*ptable_index = SCSYMON_TABLE_FORG_RS_EXT_PROP_CONFIG;
		error = SCSYMON_ENOERR;
		goto cleanup;
	}

	if (strcmp(table_name, SCSYMON_TABLE_FORG_RS_TIMEOUT_CONFIG_TEXT)
	    == 0) {
		*pgroup_index = SCSYMON_GROUP_RG_CONFIG;
		*ptable_index = SCSYMON_TABLE_FORG_RS_TIMEOUT_CONFIG;
		error = SCSYMON_ENOERR;
		goto cleanup;
	}

	if (strcmp(table_name, SCSYMON_TABLE_SCRG_RG_CONFIG_TEXT) == 0) {
		*pgroup_index = SCSYMON_GROUP_RG_CONFIG;
		*ptable_index = SCSYMON_TABLE_SCRG_RG_CONFIG;
		error = SCSYMON_ENOERR;
		goto cleanup;
	}

	if (strcmp(table_name, SCSYMON_TABLE_SCRG_RS_CONFIG_TEXT) == 0) {
		*pgroup_index = SCSYMON_GROUP_RG_CONFIG;
		*ptable_index = SCSYMON_TABLE_SCRG_RS_CONFIG;
		error = SCSYMON_ENOERR;
		goto cleanup;
	}

	if (strcmp(table_name, SCSYMON_TABLE_SCRG_RS_COM_PROP_CONFIG_TEXT)
	    == 0) {
		*pgroup_index = SCSYMON_GROUP_RG_CONFIG;
		*ptable_index = SCSYMON_TABLE_SCRG_RS_COM_PROP_CONFIG;
		error = SCSYMON_ENOERR;
		goto cleanup;
	}

	if (strcmp(table_name, SCSYMON_TABLE_SCRG_RS_EXT_PROP_CONFIG_TEXT)
	    == 0) {
		*pgroup_index = SCSYMON_GROUP_RG_CONFIG;
		*ptable_index = SCSYMON_TABLE_SCRG_RS_EXT_PROP_CONFIG;
		error = SCSYMON_ENOERR;
		goto cleanup;
	}

	if (strcmp(table_name, SCSYMON_TABLE_SCRG_RS_TIMEOUT_CONFIG_TEXT)
	    == 0) {
		*pgroup_index = SCSYMON_GROUP_RG_CONFIG;
		*ptable_index = SCSYMON_TABLE_SCRG_RS_TIMEOUT_CONFIG;
		error = SCSYMON_ENOERR;
		goto cleanup;
	}

	/* cluster status group */
	if (strcmp(table_name, SCSYMON_TABLE_CLUSTER_STATUS_TEXT) == 0) {
		*pgroup_index = SCSYMON_GROUP_CLUSTER_STATUS;
		*ptable_index = SCSYMON_TABLE_CLUSTER_STATUS;
		error = SCSYMON_ENOERR;
		goto cleanup;
	}

	if (strcmp(table_name, SCSYMON_TABLE_NODE_STATUS_TEXT) == 0) {
		*pgroup_index = SCSYMON_GROUP_CLUSTER_STATUS;
		*ptable_index = SCSYMON_TABLE_NODE_STATUS;
		error = SCSYMON_ENOERR;
		goto cleanup;
	}

	if (strcmp(table_name, SCSYMON_TABLE_NODEDEV_STATUS_TEXT) == 0) {
		*pgroup_index = SCSYMON_GROUP_CLUSTER_STATUS;
		*ptable_index = SCSYMON_TABLE_NODEDEV_STATUS;
		error = SCSYMON_ENOERR;
		goto cleanup;
	}

	if (strcmp(table_name, SCSYMON_TABLE_QDEV_STATUS_TEXT) == 0) {
		*pgroup_index = SCSYMON_GROUP_CLUSTER_STATUS;
		*ptable_index = SCSYMON_TABLE_QDEV_STATUS;
		error = SCSYMON_ENOERR;
		goto cleanup;
	}

	/* devgrp status group */
	if (strcmp(table_name, SCSYMON_TABLE_DEVGRP_STATUS_TEXT) == 0) {
		*pgroup_index = SCSYMON_GROUP_DEVGRP_STATUS;
		*ptable_index = SCSYMON_TABLE_DEVGRP_STATUS;
		error = SCSYMON_ENOERR;
		goto cleanup;
	}

	if (strcmp(table_name, SCSYMON_TABLE_REPLICA_STATUS_TEXT) == 0) {
		*pgroup_index = SCSYMON_GROUP_DEVGRP_STATUS;
		*ptable_index = SCSYMON_TABLE_REPLICA_STATUS;
		error = SCSYMON_ENOERR;
		goto cleanup;
	}

	/* transport status group */
	if (strcmp(table_name, SCSYMON_TABLE_PATH_STATUS_TEXT) == 0) {
		*pgroup_index = SCSYMON_GROUP_TRANSPORT_STATUS;
		*ptable_index = SCSYMON_TABLE_PATH_STATUS;
		error = SCSYMON_ENOERR;
		goto cleanup;
	}

	/* RG status group */
	if (strcmp(table_name, SCSYMON_TABLE_FORG_RG_STATUS_TEXT) == 0) {
		*pgroup_index = SCSYMON_GROUP_RG_STATUS;
		*ptable_index = SCSYMON_TABLE_FORG_RG_STATUS;
		error = SCSYMON_ENOERR;
		goto cleanup;
	}

	if (strcmp(table_name, SCSYMON_TABLE_FORG_RS_STATUS_TEXT) == 0) {
		*pgroup_index = SCSYMON_GROUP_RG_STATUS;
		*ptable_index = SCSYMON_TABLE_FORG_RS_STATUS;
		error = SCSYMON_ENOERR;
		goto cleanup;
	}

	if (strcmp(table_name, SCSYMON_TABLE_SCRG_RG_STATUS_TEXT) == 0) {
		*pgroup_index = SCSYMON_GROUP_RG_STATUS;
		*ptable_index = SCSYMON_TABLE_SCRG_RG_STATUS;
		error = SCSYMON_ENOERR;
		goto cleanup;
	}

	if (strcmp(table_name, SCSYMON_TABLE_SCRG_RS_STATUS_TEXT) == 0) {
		*pgroup_index = SCSYMON_GROUP_RG_STATUS;
		*ptable_index = SCSYMON_TABLE_SCRG_RS_STATUS;
		error = SCSYMON_ENOERR;
		goto cleanup;
	}

cleanup:
	return (error);
}

char *
get_table_name(scsymon_group_index_t group_index,
    scsymon_table_index_t table_index)
{
	char *ptable_name = NULL;

	switch (group_index) {
	case SCSYMON_GROUP_CLUSTER_CONFIG:
		switch (table_index) {
		case SCSYMON_TABLE_CLUSTER_CONFIG:
			ptable_name = SCSYMON_TABLE_CLUSTER_CONFIG_TEXT;
			break;
		case SCSYMON_TABLE_NODE_CONFIG:
			ptable_name = SCSYMON_TABLE_NODE_CONFIG_TEXT;
			break;
		case SCSYMON_TABLE_NODEDEV_CONFIG:
			ptable_name = SCSYMON_TABLE_NODEDEV_CONFIG_TEXT;
			break;
		case SCSYMON_TABLE_QDEV_CONFIG:
			ptable_name = SCSYMON_TABLE_QDEV_CONFIG_TEXT;
			break;
		case SCSYMON_TABLE_QDEVPORT_CONFIG:
			ptable_name = SCSYMON_TABLE_QDEVPORT_CONFIG_TEXT;
			break;
		case SCSYMON_TABLE_TRANSPORT_ADAPTER_CONFIG:
			ptable_name =
			    SCSYMON_TABLE_TRANSPORT_ADAPTER_CONFIG_TEXT;
			break;
		case SCSYMON_TABLE_JUNCTION_CONFIG:
			ptable_name = SCSYMON_TABLE_JUNCTION_CONFIG_TEXT;
			break;
		case SCSYMON_TABLE_CABLE_CONFIG:
			ptable_name = SCSYMON_TABLE_CABLE_CONFIG_TEXT;
			break;
		default:
			break;
		}
		break;
	case SCSYMON_GROUP_DEVGRP_CONFIG:
		switch (table_index) {
		case SCSYMON_TABLE_DEVGRP_CONFIG:
			ptable_name = SCSYMON_TABLE_DEVGRP_CONFIG_TEXT;
			break;
		default:
			break;
		}
		break;
	case SCSYMON_GROUP_RT_CONFIG:
		switch (table_index) {
		case SCSYMON_TABLE_RT_CONFIG:
			ptable_name = SCSYMON_TABLE_RT_CONFIG_TEXT;
			break;
		case SCSYMON_TABLE_RT_METHOD_CONFIG:
			ptable_name = SCSYMON_TABLE_RT_METHOD_CONFIG_TEXT;
			break;
		case SCSYMON_TABLE_RT_PARAM_CONFIG:
			ptable_name = SCSYMON_TABLE_RT_PARAM_CONFIG_TEXT;
			break;
		case SCSYMON_TABLE_RT_RS_CONFIG:
			ptable_name = SCSYMON_TABLE_RT_RS_CONFIG_TEXT;
			break;
		default:
			break;
		}
		break;
	case SCSYMON_GROUP_RG_CONFIG:
		switch (table_index) {
		case SCSYMON_TABLE_FORG_RG_CONFIG:
			ptable_name = SCSYMON_TABLE_FORG_RG_CONFIG_TEXT;
			break;
		case SCSYMON_TABLE_FORG_RS_CONFIG:
			ptable_name = SCSYMON_TABLE_FORG_RS_CONFIG_TEXT;
			break;
		case SCSYMON_TABLE_FORG_RS_COM_PROP_CONFIG:
			ptable_name =
			    SCSYMON_TABLE_FORG_RS_COM_PROP_CONFIG_TEXT;
			break;
		case SCSYMON_TABLE_FORG_RS_EXT_PROP_CONFIG:
			ptable_name =
			    SCSYMON_TABLE_FORG_RS_EXT_PROP_CONFIG_TEXT;
			break;
		case SCSYMON_TABLE_FORG_RS_TIMEOUT_CONFIG:
			ptable_name = SCSYMON_TABLE_FORG_RS_TIMEOUT_CONFIG_TEXT;
			break;
		case SCSYMON_TABLE_SCRG_RG_CONFIG:
			ptable_name = SCSYMON_TABLE_SCRG_RG_CONFIG_TEXT;
			break;
		case SCSYMON_TABLE_SCRG_RS_CONFIG:
			ptable_name = SCSYMON_TABLE_SCRG_RS_CONFIG_TEXT;
			break;
		case SCSYMON_TABLE_SCRG_RS_COM_PROP_CONFIG:
			ptable_name =
			    SCSYMON_TABLE_SCRG_RS_COM_PROP_CONFIG_TEXT;
			break;
		case SCSYMON_TABLE_SCRG_RS_EXT_PROP_CONFIG:
			ptable_name =
			    SCSYMON_TABLE_SCRG_RS_EXT_PROP_CONFIG_TEXT;
			break;
		case SCSYMON_TABLE_SCRG_RS_TIMEOUT_CONFIG:
			ptable_name = SCSYMON_TABLE_SCRG_RS_TIMEOUT_CONFIG_TEXT;
			break;
		default:
			break;
		}
		break;
	case SCSYMON_GROUP_CLUSTER_STATUS:
		switch (table_index) {
		case SCSYMON_TABLE_CLUSTER_STATUS:
			ptable_name = SCSYMON_TABLE_CLUSTER_STATUS_TEXT;
			break;
		case SCSYMON_TABLE_NODE_STATUS:
			ptable_name = SCSYMON_TABLE_NODE_STATUS_TEXT;
			break;
		case SCSYMON_TABLE_NODEDEV_STATUS:
			ptable_name = SCSYMON_TABLE_NODEDEV_STATUS_TEXT;
			break;
		case SCSYMON_TABLE_QDEV_STATUS:
			ptable_name = SCSYMON_TABLE_QDEV_STATUS_TEXT;
			break;
		default:
			break;
		}
		break;
	case SCSYMON_GROUP_DEVGRP_STATUS:
		switch (table_index) {
		case SCSYMON_TABLE_DEVGRP_STATUS:
			ptable_name = SCSYMON_TABLE_DEVGRP_STATUS_TEXT;
			break;
		case SCSYMON_TABLE_REPLICA_STATUS:
			ptable_name = SCSYMON_TABLE_REPLICA_STATUS_TEXT;
			break;
		default:
			break;
		}
		break;
	case SCSYMON_GROUP_TRANSPORT_STATUS:
		switch (table_index) {
		case SCSYMON_TABLE_PATH_STATUS:
			ptable_name = SCSYMON_TABLE_PATH_STATUS_TEXT;
			break;
		default:
			break;
		}
		break;
	case SCSYMON_GROUP_RG_STATUS:
		switch (table_index) {
		case SCSYMON_TABLE_FORG_RG_STATUS:
			ptable_name = SCSYMON_TABLE_FORG_RG_STATUS_TEXT;
			break;
		case SCSYMON_TABLE_FORG_RS_STATUS:
			ptable_name = SCSYMON_TABLE_FORG_RS_STATUS_TEXT;
			break;
		case SCSYMON_TABLE_SCRG_RG_STATUS:
			ptable_name = SCSYMON_TABLE_SCRG_RG_STATUS_TEXT;
			break;
		case SCSYMON_TABLE_SCRG_RS_STATUS:
			ptable_name = SCSYMON_TABLE_SCRG_RS_STATUS_TEXT;
			break;
		default:
			break;
		}
		break;
	default:
		break;
	}
	return (ptable_name);
}

void
get_group_data(scsymon_group_index_t group_index)
{
	scsymon_errno_t scsymon_error = SCSYMON_ENOERR;
	void *pdata = NULL;
	unsigned int i;

	if (mutex_lock(&mutex_group_data) != 0) {
		scsymon_error = SCSYMON_EUNEXPECTED;
		goto cleanup;
	}

	/* read lock the cached config pointer */
	if (rw_rdlock(&rwlock_config) != 0) {
		scsymon_dbg_print("error rdlock rwlock_config");
		scsymon_error = SCSYMON_EUNEXPECTED;
		goto cleanup;
	}

	/* get data from cluster library */
	scsymon_error = get_cluster_data(group_index, &pdata);
	if (scsymon_error != SCSYMON_ENOERR)
		goto cleanup;

	/* update data in every table */
	for (i = 0; i < group[group_index].table_count; i++) {
		scsymon_error = get_table_data(group_index, i, pdata);
		if (scsymon_error != SCSYMON_ENOERR)
			goto cleanup;
	}

cleanup:
	free_cluster_data(group_index, pdata);

	(void) rw_unlock(&rwlock_config);

	(void) mutex_unlock(&mutex_group_data);

	if (scsymon_error != SCSYMON_ENOERR) {
		scsymon_dbg_print("========================\n");
		print_error(scsymon_error);
		scsymon_dbg_print("========================\n");
	}
}

scsymon_errno_t
get_cluster_data(scsymon_group_index_t group_index, void **ppdata)
{
	scsymon_errno_t scsymon_error = SCSYMON_ENOERR;
	scconf_errno_t scconf_error = SCCONF_NOERR;
	scstat_errno_t scstat_error = SCSTAT_ENOERR;
	scconf_cfg_cluster_t *pcluster_config = NULL;
	scstat_cluster_t *pcluster_status = NULL;
	scconf_cfg_ds_t *pldevgrp_configs = NULL;
	scsymon_rgm_rt_t *plrgm_rts = NULL;
	scsymon_rgm_rg_rs_t *plrgm_rgs = NULL;
	scstat_ds_t *pldevgrp_statuss = NULL;
	scstat_transport_t *ptransport_status = NULL;

	if (ppdata == NULL) {
		scsymon_error = SCSYMON_EINVAL;
		goto cleanup;
	}

	/* collect data for each table */
	switch (group_index) {
	case SCSYMON_GROUP_CLUSTER_CONFIG:
		scconf_error =
		    scconf_get_clusterconfig(&pcluster_config);
		*ppdata = pcluster_config;

		if (scconf_error != SCCONF_NOERR) {
			scsymon_error =
			    scsymon_convert_scconf_error_code(scconf_error);
			goto cleanup;
		}

		/* unlock the cached config pointer before write lock */
		if (rw_unlock(&rwlock_config) != 0) {
			scsymon_dbg_print("error unlock rwlock_config");
			scsymon_error = SCSYMON_EUNEXPECTED;
			goto cleanup;
		}

		/* write lock the cached config pointer when updating it */
		if (rw_wrlock(&rwlock_config) != 0) {
			scsymon_dbg_print("error wrlock rwlock_config");
			scsymon_error = SCSYMON_EUNEXPECTED;
			goto cleanup;
		}
		scconf_free_clusterconfig(pcluster_cfg);

		pcluster_cfg = pcluster_config;
		if (rw_unlock(&rwlock_config) != 0) {
			scsymon_dbg_print("error unlock rwlock_config");
			scsymon_error = SCSYMON_EUNEXPECTED;
			goto cleanup;
		}

		/* reapply the read lock */
		if (rw_rdlock(&rwlock_config) != 0) {
			scsymon_dbg_print("error rdlock rwlock_config");
			scsymon_error = SCSYMON_EUNEXPECTED;
			goto cleanup;
		}

		break;
	case SCSYMON_GROUP_DEVGRP_CONFIG:
		/* get property from scconf */
		scconf_error = scconf_get_ds_config(NULL, SCCONF_BY_NAME_FLAG,
		    &pldevgrp_configs);
		*ppdata = pldevgrp_configs;
		if (scconf_error != SCCONF_NOERR)
			scsymon_error =
			    scsymon_convert_scconf_error_code(scconf_error);

		break;
	case SCSYMON_GROUP_RT_CONFIG:
		/* get property from scsymon */
		scsymon_error = scsymon_get_rgm_rts(&plrgm_rts);
		*ppdata = plrgm_rts;

		break;
	case SCSYMON_GROUP_RG_CONFIG:
		/* get property from scsymon */
		scsymon_error = scsymon_get_rgm_rg_rss(&plrgm_rgs);
		*ppdata = plrgm_rgs;

		break;
	case SCSYMON_GROUP_CLUSTER_STATUS:
		/* get status from scstat */
		scstat_error = scstat_get_cluster_status_with_cached_config(
		    pcluster_cfg, &pcluster_status);
		*ppdata = pcluster_status;
		if (scstat_error != SCSTAT_ENOERR)
			scsymon_error =
			    scsymon_convert_scstat_error_code(scstat_error);

		break;
	case SCSYMON_GROUP_DEVGRP_STATUS:
		/* get status from scstat */
		scstat_error = scstat_get_ds_status_with_cached_config(
		    pcluster_cfg, NULL, &pldevgrp_statuss);
		*ppdata = pldevgrp_statuss;
		if (scstat_error != SCSTAT_ENOERR)
			scsymon_error =
			    scsymon_convert_scstat_error_code(scstat_error);

		break;
	case SCSYMON_GROUP_TRANSPORT_STATUS:
		/* get status from scstat */
		scstat_error = scstat_get_transport(&ptransport_status);
		*ppdata = ptransport_status;
		if (scstat_error != SCSTAT_ENOERR)
			scsymon_error =
			    scsymon_convert_scstat_error_code(scstat_error);

		break;
	case SCSYMON_GROUP_RG_STATUS:
		/* get status from scstat */
		scstat_error = scstat_get_cluster_status_with_cached_config(
		    pcluster_cfg, &pcluster_status);
		*ppdata = pcluster_status;
		if (scstat_error != SCSTAT_ENOERR)
			scsymon_error =
			    scsymon_convert_scstat_error_code(scstat_error);

		break;
	default:
		break;
	}

cleanup:
	return (scsymon_error);
}

void
free_cluster_data(scsymon_group_index_t group_index, void *pdata)
{
	if (pdata == NULL)
		return;

	/* free data got from cluster library */
	switch (group_index) {
	case SCSYMON_GROUP_CLUSTER_CONFIG:
		break;
	case SCSYMON_GROUP_DEVGRP_CONFIG:
		if (pdata != NULL)
			scconf_free_ds_config((scconf_cfg_ds_t *)pdata);

		break;
	case SCSYMON_GROUP_RT_CONFIG:
		if (pdata != NULL)
			scsymon_free_rgm_rts((scsymon_rgm_rt_t *)pdata);

		break;
	case SCSYMON_GROUP_RG_CONFIG:
		if (pdata != NULL)
			scsymon_free_rgm_rg_rss((scsymon_rgm_rg_rs_t *)pdata);

		break;
	case SCSYMON_GROUP_CLUSTER_STATUS:
		if (pdata != NULL)
			scstat_free_cluster_status((scstat_cluster_t *)pdata);

		break;
	case SCSYMON_GROUP_DEVGRP_STATUS:
		if (pdata != NULL)
			scstat_free_ds_status((scstat_ds_t *)pdata);

		break;
	case SCSYMON_GROUP_TRANSPORT_STATUS:
		if (pdata != NULL)
			scstat_free_transport((scstat_transport_t *)pdata);

		break;
	case SCSYMON_GROUP_RG_STATUS:
		if (pdata != NULL)
			scstat_free_cluster_status((scstat_cluster_t *)pdata);
		break;
	default:
		break;
	}
}

scsymon_errno_t
get_table_data(scsymon_group_index_t group_index,
    scsymon_table_index_t table_index, void *pdata)
{
	char *group_name = group[group_index].pname;
	scsymon_table_t *ptable = &group[group_index].pltables[table_index];
	scsymon_cluster_config_t *pcluster_config = NULL;
	scsymon_cluster_status_t *pcluster_status = NULL;
	scsymon_node_config_t *plnode_configs = NULL;
	scsymon_node_status_t *plnode_statuss = NULL;
	scsymon_node_device_config_t *plnodedev_configs = NULL;
	scsymon_node_device_status_t *plnodedev_statuss = NULL;
	scsymon_devgrp_config_t *pldevgrp_configs = NULL;
	scsymon_devgrp_status_t *pldevgrp_statuss = NULL;
	scsymon_replica_status_t *plreplica_statuss = NULL;
	scsymon_qdev_config_t *plqdev_configs = NULL;
	scsymon_qdev_status_t *plqdev_statuss = NULL;
	scsymon_qdevport_config_t *plqdevport_configs = NULL;
	scsymon_path_status_t *plpath_statuss = NULL;
	scsymon_transport_adapter_config_t *pladapter_configs = NULL;
	scsymon_junction_config_t *pljunction_configs = NULL;
	scsymon_cable_config_t *plcable_configs = NULL;
	scsymon_rt_config_t *plrt_configs = NULL;
	scsymon_rt_method_config_t *plrt_method_configs = NULL;
	scsymon_rt_param_config_t *plrt_param_configs = NULL;
	scsymon_rt_rs_config_t *plrt_rs_configs = NULL;
	scsymon_rg_config_t *plrg_configs = NULL;
	scsymon_rg_status_t *plrg_statuss = NULL;
	scsymon_rs_config_t *plrs_configs = NULL;
	scsymon_rs_status_t *plrs_statuss = NULL;
	scsymon_rs_prop_config_t *plrs_prop_configs = NULL;
	scsymon_rg_type_t rg_type_filter = SCSYMON_ANY_RG;
	scsymon_rs_prop_type_t rs_prop_type_filter = SCSYMON_RS_COM_PROP;
	scsymon_errno_t error = SCSYMON_ENOERR;

	if (ptable == NULL)
		return (error);
	/*
	 * If pdata is NULL we should clean up ptable->pdata,
	 * so we should NOT return !!!
	 * if (pdata == NULL)
	 *	return (error);
	 */

	switch (group_index) {
	case SCSYMON_GROUP_CLUSTER_CONFIG:
		switch (table_index) {
		case SCSYMON_TABLE_CLUSTER_CONFIG:
			pcluster_config = ptable->pdata;
			if (pcluster_config != NULL) {
				scsymon_dbg_print("%s-%d: before free\n",
				    group_name, table_index);
				scsymon_free_cluster_config(pcluster_config);
				scsymon_dbg_print("%s-%d: after free\n",
				    group_name, table_index);
				pcluster_config = NULL;
				ptable->pdata = NULL;
			}
			if (pdata == NULL)
				break;
			scsymon_dbg_print("%s-%d: before get\n",
			    group_name, table_index);
			error = scsymon_get_cluster_config(pdata,
			    &pcluster_config);
			scsymon_dbg_print("%s-%d: after get\n",
			    group_name, table_index);
			if (error != SCSYMON_ENOERR)
				goto cleanup;
			ptable->pdata = pcluster_config;
			break;
		case SCSYMON_TABLE_NODE_CONFIG:
			plnode_configs = ptable->pdata;
			if (plnode_configs != NULL) {
				scsymon_dbg_print("%s-%d: before free\n",
				    group_name, table_index);
				scsymon_free_node_configs(plnode_configs);
				scsymon_dbg_print("%s-%d: after free\n",
				    group_name, table_index);
				plnode_configs = NULL;
				ptable->pdata = NULL;
			}
			if (pdata == NULL)
				break;
			scsymon_dbg_print("%s-%d: before get\n",
			    group_name, table_index);
			error = scsymon_get_node_configs(pdata,
			    &plnode_configs);
			scsymon_dbg_print("%s-%d: after get\n",
			    group_name, table_index);
			if (error != SCSYMON_ENOERR)
				goto cleanup;
			ptable->pdata = plnode_configs;
			break;
		case SCSYMON_TABLE_NODEDEV_CONFIG:
			plnodedev_configs = ptable->pdata;
			if (plnodedev_configs != NULL) {
				scsymon_dbg_print("%s-%d: before free\n",
				    group_name, table_index);
				scsymon_free_node_device_configs(\
				    plnodedev_configs);
				scsymon_dbg_print("%s-%d: after free\n",
				    group_name, table_index);
				plnodedev_configs = NULL;
				ptable->pdata = NULL;
			}
			if (pdata == NULL)
				break;
			scsymon_dbg_print("%s-%d: before get\n",
			    group_name, table_index);
			error = scsymon_get_node_device_configs(pdata,
			    &plnodedev_configs);
			scsymon_dbg_print("%s-%d: after get\n",
			    group_name, table_index);
			if (error != SCSYMON_ENOERR)
				goto cleanup;
			ptable->pdata = plnodedev_configs;
			break;
		case SCSYMON_TABLE_QDEV_CONFIG:
			plqdev_configs = ptable->pdata;
			if (plqdev_configs != NULL) {
				scsymon_dbg_print("%s-%d: before free\n",
				    group_name, table_index);
				scsymon_free_qdev_configs(plqdev_configs);
				scsymon_dbg_print("%s-%d: after free\n",
				    group_name, table_index);
				plqdev_configs = NULL;
				ptable->pdata = NULL;
			}
			if (pdata == NULL)
				break;
			scsymon_dbg_print("%s-%d: before get\n",
			    group_name, table_index);
			error = scsymon_get_qdev_configs(pdata,
			    &plqdev_configs);
			scsymon_dbg_print("%s-%d: after get\n",
			    group_name, table_index);
			if (error != SCSYMON_ENOERR)
				goto cleanup;
			ptable->pdata = plqdev_configs;
			break;
		case SCSYMON_TABLE_QDEVPORT_CONFIG:
			plqdevport_configs = ptable->pdata;
			if (plqdevport_configs != NULL) {
				scsymon_dbg_print("%s-%d: before free\n",
				    group_name, table_index);
				scsymon_free_qdevport_configs(\
				    plqdevport_configs);
				scsymon_dbg_print("%s-%d: after free\n",
				    group_name, table_index);
				plqdevport_configs = NULL;
				ptable->pdata = NULL;
			}
			if (pdata == NULL)
				break;
			scsymon_dbg_print("%s-%d: before get\n",
			    group_name, table_index);
			error = scsymon_get_qdevport_configs(pdata,
			    &plqdevport_configs);
			scsymon_dbg_print("%s-%d: after get\n",
			    group_name, table_index);
			if (error != SCSYMON_ENOERR)
				goto cleanup;
			ptable->pdata = plqdevport_configs;
			break;
		case SCSYMON_TABLE_TRANSPORT_ADAPTER_CONFIG:
			pladapter_configs = ptable->pdata;
			if (pladapter_configs != NULL) {
				scsymon_dbg_print("%s-%d: before free\n",
				    group_name, table_index);
				scsymon_free_transport_adapter_configs(\
				    pladapter_configs);
				scsymon_dbg_print("%s-%d: after free\n",
				    group_name, table_index);
				pladapter_configs = NULL;
				ptable->pdata = NULL;
			}
			if (pdata == NULL)
				break;
			scsymon_dbg_print("%s-%d: before get\n",
			    group_name, table_index);
			error = scsymon_get_transport_adapter_configs(pdata,
			    &pladapter_configs);
			scsymon_dbg_print("%s-%d: after get\n",
			    group_name, table_index);
			if (error != SCSYMON_ENOERR)
				goto cleanup;
			ptable->pdata = pladapter_configs;
			break;
		case SCSYMON_TABLE_JUNCTION_CONFIG:
			pljunction_configs = ptable->pdata;
			if (pljunction_configs != NULL) {
				scsymon_dbg_print("%s-%d: before free\n",
				    group_name, table_index);
				scsymon_free_junction_configs(\
				    pljunction_configs);
				scsymon_dbg_print("%s-%d: after free\n",
				    group_name, table_index);
				pljunction_configs = NULL;
				ptable->pdata = NULL;
			}
			if (pdata == NULL)
				break;
			scsymon_dbg_print("%s-%d: before get\n",
			    group_name, table_index);
			error = scsymon_get_junction_configs(pdata,
			    &pljunction_configs);
			scsymon_dbg_print("%s-%d: after get\n",
			    group_name, table_index);
			if (error != SCSYMON_ENOERR)
				goto cleanup;
			ptable->pdata = pljunction_configs;
			break;
		case SCSYMON_TABLE_CABLE_CONFIG:
			plcable_configs = ptable->pdata;
			if (plcable_configs != NULL) {
				scsymon_dbg_print("%s-%d: before free\n",
				    group_name, table_index);
				scsymon_free_cable_configs(plcable_configs);
				scsymon_dbg_print("%s-%d: after free\n",
				    group_name, table_index);
				plcable_configs = NULL;
				ptable->pdata = NULL;
			}
			if (pdata == NULL)
				break;
			scsymon_dbg_print("%s-%d: before get\n",
			    group_name, table_index);
			error = scsymon_get_cable_configs(pdata,
			    &plcable_configs);
			scsymon_dbg_print("%s-%d: after get\n",
			    group_name, table_index);
			if (error != SCSYMON_ENOERR)
				goto cleanup;
			ptable->pdata = plcable_configs;
			break;
		default:
			break;
		}
		break;
	case SCSYMON_GROUP_DEVGRP_CONFIG:
		switch (table_index) {
		case SCSYMON_TABLE_DEVGRP_CONFIG:
			pldevgrp_configs = ptable->pdata;
			if (pldevgrp_configs != NULL) {
				scsymon_dbg_print("%s-%d: before free\n",
				    group_name, table_index);
				scsymon_free_devgrp_configs(pldevgrp_configs);
				scsymon_dbg_print("%s-%d: after free\n",
				    group_name, table_index);
				pldevgrp_configs = NULL;
				ptable->pdata = NULL;
			}
			if (pdata == NULL)
				break;
			scsymon_dbg_print("%s-%d: before get\n",
			    group_name, table_index);
			error = scsymon_get_devgrp_configs(pcluster_cfg, pdata,
				    &pldevgrp_configs);
			scsymon_dbg_print("%s-%d: after get\n",
			    group_name, table_index);
			if (error != SCSYMON_ENOERR)
				goto cleanup;
			ptable->pdata = pldevgrp_configs;
			break;
		default:
			break;
		}
		break;
	case SCSYMON_GROUP_RT_CONFIG:
		switch (table_index) {
		case SCSYMON_TABLE_RT_CONFIG:
			plrt_configs = ptable->pdata;
			if (plrt_configs != NULL) {
				scsymon_dbg_print("%s-%d: before free\n",
				    group_name, table_index);
				scsymon_free_rt_configs(plrt_configs);
				scsymon_dbg_print("%s-%d: after free\n",
				    group_name, table_index);
				plrt_configs = NULL;
				ptable->pdata = NULL;
			}
			if (pdata == NULL)
				break;
			scsymon_dbg_print("%s-%d: before get\n",
			    group_name, table_index);
			error = scsymon_get_rt_configs(pdata, &plrt_configs);
			scsymon_dbg_print("%s-%d: after get\n",
			    group_name, table_index);
			ptable->pdata = plrt_configs;
			if (error != SCSYMON_ENOERR)
				goto cleanup;

			break;
		case SCSYMON_TABLE_RT_METHOD_CONFIG:
			plrt_method_configs = ptable->pdata;
			if (plrt_method_configs != NULL) {
				scsymon_dbg_print("%s-%d: before free\n",
				    group_name, table_index);
				scsymon_free_rt_method_configs(\
				    plrt_method_configs);
				scsymon_dbg_print("%s-%d: after free\n",
				    group_name, table_index);
				plrt_method_configs = NULL;
				ptable->pdata = NULL;
			}
			if (pdata == NULL)
				break;
			scsymon_dbg_print("%s-%d: before get\n",
			    group_name, table_index);
			error = scsymon_get_rt_method_configs(pdata,
			    &plrt_method_configs);
			scsymon_dbg_print("%s-%d: after get\n",
			    group_name, table_index);
			ptable->pdata = plrt_method_configs;
			if (error != SCSYMON_ENOERR)
				goto cleanup;

			break;
		case SCSYMON_TABLE_RT_PARAM_CONFIG:
			plrt_param_configs = ptable->pdata;
			if (plrt_param_configs != NULL) {
				scsymon_dbg_print("%s-%d: before free\n",
				    group_name, table_index);
				scsymon_free_rt_param_configs(\
				    plrt_param_configs);
				scsymon_dbg_print("%s-%d: after free\n",
				    group_name, table_index);
				plrt_param_configs = NULL;
				ptable->pdata = NULL;
			}
			if (pdata == NULL)
				break;
			scsymon_dbg_print("%s-%d: before get\n",
			    group_name, table_index);
			error = scsymon_get_rt_param_configs(pdata,
			    &plrt_param_configs);
			scsymon_dbg_print("%s-%d: after get\n",
			    group_name, table_index);
			ptable->pdata = plrt_param_configs;
			if (error != SCSYMON_ENOERR)
				goto cleanup;

			break;
		case SCSYMON_TABLE_RT_RS_CONFIG:
			plrt_rs_configs = ptable->pdata;
			if (plrt_rs_configs != NULL) {
				scsymon_dbg_print("%s-%d: before free\n",
				    group_name, table_index);
				scsymon_free_rt_rs_configs(\
				    plrt_rs_configs);
				scsymon_dbg_print("%s-%d: after free\n",
				    group_name, table_index);
				plrt_rs_configs = NULL;
				ptable->pdata = NULL;
			}
			if (pdata == NULL)
				break;
			scsymon_dbg_print("%s-%d: before get\n",
			    group_name, table_index);
			error = scsymon_get_rt_rs_configs(pdata,
			    &plrt_rs_configs);
			scsymon_dbg_print("%s-%d: after get\n",
			    group_name, table_index);
			ptable->pdata = plrt_rs_configs;
			if (error != SCSYMON_ENOERR)
				goto cleanup;

			break;
		default:
			break;
		}
		break;
	case SCSYMON_GROUP_RG_CONFIG:
		switch (table_index) {
		case SCSYMON_TABLE_FORG_RG_CONFIG:
		case SCSYMON_TABLE_SCRG_RG_CONFIG:
			if (table_index == SCSYMON_TABLE_FORG_RG_CONFIG)
				rg_type_filter = SCSYMON_FAILOVER_RG;
			else
				rg_type_filter = SCSYMON_SCALABLE_RG;

			plrg_configs = ptable->pdata;
			if (plrg_configs != NULL) {
				scsymon_dbg_print("%s-%d: before free\n",
				    group_name, table_index);
				scsymon_free_rg_configs(plrg_configs);
				scsymon_dbg_print("%s-%d: after free\n",
				    group_name, table_index);
				plrg_configs = NULL;
				ptable->pdata = NULL;
			}
			if (pdata == NULL)
				break;
			scsymon_dbg_print("%s-%d: before get\n",
			    group_name, table_index);
			error = scsymon_get_rg_configs(pdata, rg_type_filter,
			    &plrg_configs);
			scsymon_dbg_print("%s-%d: after get\n",
			    group_name, table_index);
			ptable->pdata = plrg_configs;
			if (error != SCSYMON_ENOERR)
				goto cleanup;

			break;
		case SCSYMON_TABLE_FORG_RS_CONFIG:
		case SCSYMON_TABLE_SCRG_RS_CONFIG:
			if (table_index == SCSYMON_TABLE_FORG_RS_CONFIG)
				rg_type_filter = SCSYMON_FAILOVER_RG;
			else
				rg_type_filter = SCSYMON_SCALABLE_RG;

			plrs_configs = ptable->pdata;
			if (plrs_configs != NULL) {
				scsymon_dbg_print("%s-%d: before free\n",
				    group_name, table_index);
				scsymon_free_rs_configs(plrs_configs);
				scsymon_dbg_print("%s-%d: after free\n",
				    group_name, table_index);
				plrs_configs = NULL;
				ptable->pdata = NULL;
			}
			if (pdata == NULL)
				break;
			scsymon_dbg_print("%s-%d: before get\n",
			    group_name, table_index);
			error = scsymon_get_rs_configs(pdata, rg_type_filter,
			    &plrs_configs);
			scsymon_dbg_print("%s-%d: after get\n",
			    group_name, table_index);
			ptable->pdata = plrs_configs;
			if (error != SCSYMON_ENOERR)
				goto cleanup;

			break;
		case SCSYMON_TABLE_FORG_RS_COM_PROP_CONFIG:
		case SCSYMON_TABLE_SCRG_RS_COM_PROP_CONFIG:
		case SCSYMON_TABLE_FORG_RS_EXT_PROP_CONFIG:
		case SCSYMON_TABLE_SCRG_RS_EXT_PROP_CONFIG:
		case SCSYMON_TABLE_FORG_RS_TIMEOUT_CONFIG:
		case SCSYMON_TABLE_SCRG_RS_TIMEOUT_CONFIG:
			switch (table_index) {
			case SCSYMON_TABLE_FORG_RS_COM_PROP_CONFIG:
				rg_type_filter = SCSYMON_FAILOVER_RG;
				rs_prop_type_filter = SCSYMON_RS_COM_PROP;
				break;
			case SCSYMON_TABLE_SCRG_RS_COM_PROP_CONFIG:
				rg_type_filter = SCSYMON_SCALABLE_RG;
				rs_prop_type_filter = SCSYMON_RS_COM_PROP;
				break;
			case SCSYMON_TABLE_FORG_RS_EXT_PROP_CONFIG:
				rg_type_filter = SCSYMON_FAILOVER_RG;
				rs_prop_type_filter = SCSYMON_RS_EXT_PROP;
				break;
			case SCSYMON_TABLE_SCRG_RS_EXT_PROP_CONFIG:
				rg_type_filter = SCSYMON_SCALABLE_RG;
				rs_prop_type_filter = SCSYMON_RS_EXT_PROP;
				break;
			case SCSYMON_TABLE_FORG_RS_TIMEOUT_CONFIG:
				rg_type_filter = SCSYMON_FAILOVER_RG;
				rs_prop_type_filter = SCSYMON_RS_TIMEOUT_PROP;
				break;
			case SCSYMON_TABLE_SCRG_RS_TIMEOUT_CONFIG:
				rg_type_filter = SCSYMON_SCALABLE_RG;
				rs_prop_type_filter = SCSYMON_RS_TIMEOUT_PROP;
				break;
			default:
				break;
			}

			plrs_prop_configs = ptable->pdata;
			if (plrs_prop_configs != NULL) {
				scsymon_dbg_print("%s-%d: before free\n",
				    group_name, table_index);
				scsymon_free_rs_prop_configs(\
				    plrs_prop_configs);
				scsymon_dbg_print("%s-%d: after free\n",
				    group_name, table_index);
				plrs_prop_configs = NULL;
				ptable->pdata = NULL;
			}
			if (pdata == NULL)
				break;
			scsymon_dbg_print("%s-%d: before get\n",
			    group_name, table_index);
			error = scsymon_get_rs_prop_configs(pdata,
			    rg_type_filter, rs_prop_type_filter,
			    &plrs_prop_configs);
			scsymon_dbg_print("%s-%d: after get\n",
			    group_name, table_index);
			ptable->pdata = plrs_prop_configs;
			if (error != SCSYMON_ENOERR)
				goto cleanup;

			break;
		default:
			break;
		}
		break;
	case SCSYMON_GROUP_CLUSTER_STATUS:
		switch (table_index) {
		case SCSYMON_TABLE_CLUSTER_STATUS:
			pcluster_status = ptable->pdata;
			if (pcluster_status != NULL) {
				scsymon_dbg_print("%s-%d: before free\n",
				    group_name, table_index);
				scsymon_free_cluster_status(pcluster_status);
				scsymon_dbg_print("%s-%d: after free\n",
				    group_name, table_index);
				pcluster_status = NULL;
				ptable->pdata = NULL;
			}
			if (pdata == NULL)
				break;
			scsymon_dbg_print("%s-%d: before get\n",
			    group_name, table_index);
			error = scsymon_get_cluster_status(pdata,
			    &pcluster_status);
			scsymon_dbg_print("%s-%d: after get\n",
			    group_name, table_index);
			if (error != SCSYMON_ENOERR)
				goto cleanup;
			ptable->pdata = pcluster_status;
			break;
		case SCSYMON_TABLE_NODE_STATUS:
			plnode_statuss = ptable->pdata;
			if (plnode_statuss != NULL) {
				scsymon_dbg_print("%s-%d: before free\n",
				    group_name, table_index);
				scsymon_free_node_statuss(plnode_statuss);
				scsymon_dbg_print("%s-%d: after free\n",
				    group_name, table_index);
				plnode_statuss = NULL;
				ptable->pdata = NULL;
			}
			scsymon_dbg_print("%s-%d: before get\n",
			    group_name, table_index);
			error = scsymon_get_node_statuss(pdata,
			    &plnode_statuss);
			scsymon_dbg_print("%s-%d: after get\n",
			    group_name, table_index);
			if (error != SCSYMON_ENOERR)
				goto cleanup;
			ptable->pdata = plnode_statuss;
			break;
		case SCSYMON_TABLE_NODEDEV_STATUS:
			plnodedev_statuss = ptable->pdata;
			if (plnodedev_statuss != NULL) {
				scsymon_dbg_print("%s-%d: before free\n",
				    group_name, table_index);
				scsymon_free_node_device_statuss(\
				    plnodedev_statuss);
				scsymon_dbg_print("%s-%d: after free\n",
				    group_name, table_index);
				plnodedev_statuss = NULL;
				ptable->pdata = NULL;
			}
			if (pdata == NULL)
				break;
			scsymon_dbg_print("%s-%d: before get\n",
			    group_name, table_index);
			error = scsymon_get_node_device_statuss(pdata,
			    &plnodedev_statuss);
			scsymon_dbg_print("%s-%d: after get\n",
			    group_name, table_index);
			if (error != SCSYMON_ENOERR)
				goto cleanup;
			ptable->pdata = plnodedev_statuss;
			break;
		case SCSYMON_TABLE_QDEV_STATUS:
			plqdev_statuss = ptable->pdata;
			if (plqdev_statuss != NULL) {
				scsymon_dbg_print("%s-%d: before free\n",
				    group_name, table_index);
				scsymon_free_qdev_statuss(plqdev_statuss);
				scsymon_dbg_print("%s-%d: after free\n",
				    group_name, table_index);
				plqdev_statuss = NULL;
				ptable->pdata = NULL;
			}
			if (pdata == NULL)
				break;
			scsymon_dbg_print("%s-%d: before get\n",
			    group_name, table_index);
			error = scsymon_get_qdev_statuss(pdata,
			    &plqdev_statuss);
			scsymon_dbg_print("%s-%d: after get\n",
			    group_name, table_index);
			if (error != SCSYMON_ENOERR)
				goto cleanup;
			ptable->pdata = plqdev_statuss;
			break;
		default:
			break;
		}
		break;
	case SCSYMON_GROUP_DEVGRP_STATUS:
		switch (table_index) {
		case SCSYMON_TABLE_DEVGRP_STATUS:
			pldevgrp_statuss = ptable->pdata;
			if (pldevgrp_statuss != NULL) {
				scsymon_dbg_print("%s-%d: before free\n",
				    group_name, table_index);
				scsymon_free_devgrp_statuss(pldevgrp_statuss);
				scsymon_dbg_print("%s-%d: after free\n",
				    group_name, table_index);
				pldevgrp_statuss = NULL;
				ptable->pdata = NULL;
			}
			if (pdata == NULL)
				break;
			scsymon_dbg_print("%s-%d: before get\n",
			    group_name, table_index);
			error = scsymon_get_devgrp_statuss(pdata,
			    &pldevgrp_statuss);
			scsymon_dbg_print("%s-%d: after get\n",
			    group_name, table_index);
			if (error != SCSYMON_ENOERR)
				goto cleanup;
			ptable->pdata = pldevgrp_statuss;
			break;
		case SCSYMON_TABLE_REPLICA_STATUS:
			plreplica_statuss = ptable->pdata;
			if (plreplica_statuss != NULL) {
				scsymon_dbg_print("%s-%d: before free\n",
				    group_name, table_index);
				scsymon_free_replica_statuss(plreplica_statuss);
				scsymon_dbg_print("%s-%d: after free\n",
				    group_name, table_index);
				plreplica_statuss = NULL;
				ptable->pdata = NULL;
			}
			if (pdata == NULL)
				break;
			scsymon_dbg_print("%s-%d: before get\n",
			    group_name, table_index);
			error = scsymon_get_replica_statuss(pdata,
			    &plreplica_statuss);
			scsymon_dbg_print("%s-%d: after get\n",
			    group_name, table_index);
			if (error != SCSYMON_ENOERR)
				goto cleanup;
			ptable->pdata = plreplica_statuss;
			break;
		default:
			break;
		}
		break;
	case SCSYMON_GROUP_TRANSPORT_STATUS:
		switch (table_index) {
		case SCSYMON_TABLE_PATH_STATUS:
			plpath_statuss = ptable->pdata;
			if (plpath_statuss != NULL) {
				scsymon_dbg_print("%s-%d: before free\n",
				    group_name, table_index);
				scsymon_free_path_statuss(plpath_statuss);
				scsymon_dbg_print("%s-%d: after free\n",
				    group_name, table_index);
				plpath_statuss = NULL;
				ptable->pdata = NULL;
			}
			if (pdata == NULL)
				break;
			scsymon_dbg_print("%s-%d: before get\n",
			    group_name, table_index);
			error = scsymon_get_path_statuss(pcluster_cfg, pdata,
			    &plpath_statuss);
			scsymon_dbg_print("%s-%d: after get\n",
			    group_name, table_index);
			if (error != SCSYMON_ENOERR)
				goto cleanup;
			ptable->pdata = plpath_statuss;
			break;
		default:
			break;
		}
		break;
	case SCSYMON_GROUP_RG_STATUS:
		switch (table_index) {
		case SCSYMON_TABLE_FORG_RG_STATUS:
		case SCSYMON_TABLE_SCRG_RG_STATUS:
			if (table_index == SCSYMON_TABLE_FORG_RG_STATUS)
				rg_type_filter = SCSYMON_FAILOVER_RG;
			else
				rg_type_filter = SCSYMON_SCALABLE_RG;

			plrg_statuss = ptable->pdata;
			if (plrg_statuss != NULL) {
				scsymon_dbg_print("%s-%d: before free\n",
				    group_name, table_index);
				scsymon_free_rg_statuss(plrg_statuss);
				scsymon_dbg_print("%s-%d: after free\n",
				    group_name, table_index);
				plrg_statuss = NULL;
				ptable->pdata = NULL;
			}
			if (pdata == NULL)
				break;
			scsymon_dbg_print("%s-%d: before get\n",
			    group_name, table_index);
			error = scsymon_get_rg_status(pdata, rg_type_filter,
			    &plrg_statuss);
			scsymon_dbg_print("%s-%d: after get\n",
			    group_name, table_index);
			if (error != SCSYMON_ENOERR)
				goto cleanup;
			ptable->pdata = plrg_statuss;
			break;
		case SCSYMON_TABLE_FORG_RS_STATUS:
		case SCSYMON_TABLE_SCRG_RS_STATUS:
			if (table_index == SCSYMON_TABLE_FORG_RS_STATUS)
				rg_type_filter = SCSYMON_FAILOVER_RG;
			else
				rg_type_filter = SCSYMON_SCALABLE_RG;

			plrs_statuss = ptable->pdata;
			if (plrs_statuss != NULL) {
				scsymon_dbg_print("%s-%d: before free\n",
				    group_name, table_index);
				scsymon_free_rs_statuss(plrs_statuss);
				scsymon_dbg_print("%s-%d: after free\n",
				    group_name, table_index);
				plrs_statuss = NULL;
				ptable->pdata = NULL;
			}
			if (pdata == NULL)
				break;
			scsymon_dbg_print("%s-%d: before get\n",
			    group_name, table_index);
			error = scsymon_get_rs_statuss(pdata, rg_type_filter,
			    &plrs_statuss);
			scsymon_dbg_print("%s-%d: after get\n",
			    group_name, table_index);
			if (error != SCSYMON_ENOERR)
				goto cleanup;
			ptable->pdata = plrs_statuss;
			break;
		default:
			break;
		}
		break;
	default:
		break;
	}

cleanup:
	return (error);
}

void
clean_group_data(scsymon_group_index_t group_index)
{
	unsigned int i;

	/* free data for each table */
	for (i = 0; i < group[group_index].table_count; i++)
		clean_table_data(group_index, i);
}

void clean_table_data(scsymon_group_index_t group_index,
    scsymon_table_index_t table_index)
{
	char *group_name = group[group_index].pname;
	scsymon_table_t *ptable = &group[group_index].pltables[table_index];
	scsymon_cluster_config_t *pcluster_config = NULL;
	scsymon_cluster_status_t *pcluster_status = NULL;
	scsymon_node_config_t *plnode_configs = NULL;
	scsymon_node_status_t *plnode_statuss = NULL;
	scsymon_node_device_config_t *plnodedev_configs = NULL;
	scsymon_node_device_status_t *plnodedev_statuss = NULL;
	scsymon_devgrp_config_t *pldevgrp_configs = NULL;
	scsymon_devgrp_status_t *pldevgrp_statuss = NULL;
	scsymon_replica_status_t *plreplica_statuss = NULL;
	scsymon_qdev_config_t *plqdev_configs = NULL;
	scsymon_qdev_status_t *plqdev_statuss = NULL;
	scsymon_qdevport_config_t *plqdevport_configs = NULL;
	scsymon_path_status_t *plpath_statuss = NULL;
	scsymon_transport_adapter_config_t *pladapter_configs = NULL;
	scsymon_junction_config_t *pljunction_configs = NULL;
	scsymon_cable_config_t *plcable_configs = NULL;
	scsymon_rt_config_t *plrt_configs = NULL;
	scsymon_rt_method_config_t *plrt_method_configs = NULL;
	scsymon_rt_param_config_t *plrt_param_configs = NULL;
	scsymon_rt_rs_config_t *plrt_rs_configs = NULL;
	scsymon_rg_config_t *plrg_configs = NULL;
	scsymon_rg_status_t *plrg_statuss = NULL;
	scsymon_rs_config_t *plrs_configs = NULL;
	scsymon_rs_status_t *plrs_statuss = NULL;
	scsymon_rs_prop_config_t *plrs_prop_configs = NULL;
	scsymon_errno_t error = SCSYMON_ENOERR;

	if (ptable == NULL)
		return;

	/*
	 * clean up the table string
	 */
	(void) strcpy(ptable->pstring, "");

	switch (group_index) {
	case SCSYMON_GROUP_CLUSTER_CONFIG:
		switch (table_index) {
		case SCSYMON_TABLE_CLUSTER_CONFIG:
			pcluster_config = ptable->pdata;
			if (pcluster_config != NULL) {
				scsymon_dbg_print("%s-%d: before free\n",
				    group_name, table_index);
				scsymon_free_cluster_config(pcluster_config);
				scsymon_dbg_print("%s-%d: after free\n",
				    group_name, table_index);
				pcluster_config = NULL;
				ptable->pdata = NULL;
			}
			break;
		case SCSYMON_TABLE_NODE_CONFIG:
			plnode_configs = ptable->pdata;
			if (plnode_configs != NULL) {
				scsymon_dbg_print("%s-%d: before free\n",
				    group_name, table_index);
				scsymon_free_node_configs(plnode_configs);
				scsymon_dbg_print("%s-%d: after free\n",
				    group_name, table_index);
				plnode_configs = NULL;
				ptable->pdata = NULL;
			}
			break;
		case SCSYMON_TABLE_NODEDEV_CONFIG:
			plnodedev_configs = ptable->pdata;
			if (plnodedev_configs != NULL) {
				scsymon_dbg_print("%s-%d: before free\n",
				    group_name, table_index);
				scsymon_free_node_device_configs(\
				    plnodedev_configs);
				scsymon_dbg_print("%s-%d: after free\n",
				    group_name, table_index);
				plnodedev_configs = NULL;
				ptable->pdata = NULL;
			}
			break;
		case SCSYMON_TABLE_QDEV_CONFIG:
			plqdev_configs = ptable->pdata;
			if (plqdev_configs != NULL) {
				scsymon_dbg_print("%s-%d: before free\n",
				    group_name, table_index);
				scsymon_free_qdev_configs(plqdev_configs);
				scsymon_dbg_print("%s-%d: after free\n",
				    group_name, table_index);
				plqdev_configs = NULL;
				ptable->pdata = NULL;
			}
			break;
		case SCSYMON_TABLE_QDEVPORT_CONFIG:
			plqdevport_configs = ptable->pdata;
			if (plqdevport_configs != NULL) {
				scsymon_dbg_print("%s-%d: before free\n",
				    group_name, table_index);
				scsymon_free_qdevport_configs(\
				    plqdevport_configs);
				scsymon_dbg_print("%s-%d: after free\n",
				    group_name, table_index);
				plqdevport_configs = NULL;
				ptable->pdata = NULL;
			}
			break;
		case SCSYMON_TABLE_TRANSPORT_ADAPTER_CONFIG:
			pladapter_configs = ptable->pdata;
			if (pladapter_configs != NULL) {
				scsymon_dbg_print("%s-%d: before free\n",
				    group_name, table_index);
				scsymon_free_transport_adapter_configs(\
				    pladapter_configs);
				scsymon_dbg_print("%s-%d: after free\n",
				    group_name, table_index);
				pladapter_configs = NULL;
				ptable->pdata = NULL;
			}
			break;
		case SCSYMON_TABLE_JUNCTION_CONFIG:
			pljunction_configs = ptable->pdata;
			if (pljunction_configs != NULL) {
				scsymon_dbg_print("%s-%d: before free\n",
				    group_name, table_index);
				scsymon_free_junction_configs(\
				    pljunction_configs);
				scsymon_dbg_print("%s-%d: after free\n",
				    group_name, table_index);
				pljunction_configs = NULL;
				ptable->pdata = NULL;
			}
			break;
		case SCSYMON_TABLE_CABLE_CONFIG:
			plcable_configs = ptable->pdata;
			if (plcable_configs != NULL) {
				scsymon_dbg_print("%s-%d: before free\n",
				    group_name, table_index);
				scsymon_free_cable_configs(plcable_configs);
				scsymon_dbg_print("%s-%d: after free\n",
				    group_name, table_index);
				plcable_configs = NULL;
				ptable->pdata = NULL;
			}
			break;
		default:
			break;
		}
		break;
	case SCSYMON_GROUP_DEVGRP_CONFIG:
		switch (table_index) {
		case SCSYMON_TABLE_DEVGRP_CONFIG:
			pldevgrp_configs = ptable->pdata;
			if (pldevgrp_configs != NULL) {
				scsymon_dbg_print("%s-%d: before free\n",
				    group_name, table_index);
				scsymon_free_devgrp_configs(pldevgrp_configs);
				scsymon_dbg_print("%s-%d: after free\n",
				    group_name, table_index);
				pldevgrp_configs = NULL;
				ptable->pdata = NULL;
			}
			break;
		default:
			break;
		}
		break;
	case SCSYMON_GROUP_RT_CONFIG:
		switch (table_index) {
		case SCSYMON_TABLE_RT_CONFIG:
			plrt_configs = ptable->pdata;
			if (plrt_configs != NULL) {
				scsymon_dbg_print("%s-%d: before free\n",
				    group_name, table_index);
				scsymon_free_rt_configs(plrt_configs);
				scsymon_dbg_print("%s-%d: after free\n",
				    group_name, table_index);
				plrt_configs = NULL;
				ptable->pdata = NULL;
			}
			break;
		case SCSYMON_TABLE_RT_METHOD_CONFIG:
			plrt_method_configs = ptable->pdata;
			if (plrt_method_configs != NULL) {
				scsymon_dbg_print("%s-%d: before free\n",
				    group_name, table_index);
				scsymon_free_rt_method_configs(\
				    plrt_method_configs);
				scsymon_dbg_print("%s-%d: after free\n",
				    group_name, table_index);
				plrt_method_configs = NULL;
				ptable->pdata = NULL;
			}
			break;
		case SCSYMON_TABLE_RT_PARAM_CONFIG:
			plrt_param_configs = ptable->pdata;
			if (plrt_param_configs != NULL) {
				scsymon_dbg_print("%s-%d: before free\n",
				    group_name, table_index);
				scsymon_free_rt_param_configs(\
				    plrt_param_configs);
				scsymon_dbg_print("%s-%d: after free\n",
				    group_name, table_index);
				plrt_param_configs = NULL;
				ptable->pdata = NULL;
			}
			break;
		case SCSYMON_TABLE_RT_RS_CONFIG:
			plrt_rs_configs = ptable->pdata;
			if (plrt_rs_configs != NULL) {
				scsymon_dbg_print("%s-%d: before free\n",
				    group_name, table_index);
				scsymon_free_rt_rs_configs(\
				    plrt_rs_configs);
				scsymon_dbg_print("%s-%d: after free\n",
				    group_name, table_index);
				plrt_rs_configs = NULL;
				ptable->pdata = NULL;
			}
			break;
		default:
			break;
		}
		break;
	case SCSYMON_GROUP_RG_CONFIG:
		switch (table_index) {
		case SCSYMON_TABLE_FORG_RG_CONFIG:
		case SCSYMON_TABLE_SCRG_RG_CONFIG:
			plrg_configs = ptable->pdata;
			if (plrg_configs != NULL) {
				scsymon_dbg_print("%s-%d: before free\n",
				    group_name, table_index);
				scsymon_free_rg_configs(plrg_configs);
				scsymon_dbg_print("%s-%d: after free\n",
				    group_name, table_index);
				plrg_configs = NULL;
				ptable->pdata = NULL;
			}
			break;
		case SCSYMON_TABLE_FORG_RS_CONFIG:
		case SCSYMON_TABLE_SCRG_RS_CONFIG:
			plrs_configs = ptable->pdata;
			if (plrs_configs != NULL) {
				scsymon_dbg_print("%s-%d: before free\n",
				    group_name, table_index);
				scsymon_free_rs_configs(plrs_configs);
				scsymon_dbg_print("%s-%d: after free\n",
				    group_name, table_index);
				plrs_configs = NULL;
				ptable->pdata = NULL;
			}
			break;
		case SCSYMON_TABLE_FORG_RS_COM_PROP_CONFIG:
		case SCSYMON_TABLE_SCRG_RS_COM_PROP_CONFIG:
		case SCSYMON_TABLE_FORG_RS_EXT_PROP_CONFIG:
		case SCSYMON_TABLE_SCRG_RS_EXT_PROP_CONFIG:
		case SCSYMON_TABLE_FORG_RS_TIMEOUT_CONFIG:
		case SCSYMON_TABLE_SCRG_RS_TIMEOUT_CONFIG:
			plrs_prop_configs = ptable->pdata;
			if (plrs_prop_configs != NULL) {
				scsymon_dbg_print("%s-%d: before free\n",
				    group_name, table_index);
				scsymon_free_rs_prop_configs(\
				    plrs_prop_configs);
				scsymon_dbg_print("%s-%d: after free\n",
				    group_name, table_index);
				plrs_prop_configs = NULL;
				ptable->pdata = NULL;
			}
			break;
		default:
			break;
		}
		break;
	case SCSYMON_GROUP_CLUSTER_STATUS:
		switch (table_index) {
		case SCSYMON_TABLE_CLUSTER_STATUS:
			pcluster_status = ptable->pdata;
			if (pcluster_status != NULL) {
				scsymon_dbg_print("%s-%d: before free\n",
				    group_name, table_index);
				scsymon_free_cluster_status(pcluster_status);
				scsymon_dbg_print("%s-%d: after free\n",
				    group_name, table_index);
				pcluster_status = NULL;
				ptable->pdata = NULL;
			}
			break;
		case SCSYMON_TABLE_NODE_STATUS:
			plnode_statuss = ptable->pdata;
			if (plnode_statuss != NULL) {
				scsymon_dbg_print("%s-%d: before free\n",
				    group_name, table_index);
				scsymon_free_node_statuss(plnode_statuss);
				scsymon_dbg_print("%s-%d: after free\n",
				    group_name, table_index);
				plnode_statuss = NULL;
				ptable->pdata = NULL;
			}
			break;
		case SCSYMON_TABLE_NODEDEV_STATUS:
			plnodedev_statuss = ptable->pdata;
			if (plnodedev_statuss != NULL) {
				scsymon_dbg_print("%s-%d: before free\n",
				    group_name, table_index);
				scsymon_free_node_device_statuss(\
				    plnodedev_statuss);
				scsymon_dbg_print("%s-%d: after free\n",
				    group_name, table_index);
				plnodedev_statuss = NULL;
				ptable->pdata = NULL;
			}
			break;
		case SCSYMON_TABLE_QDEV_STATUS:
			plqdev_statuss = ptable->pdata;
			if (plqdev_statuss != NULL) {
				scsymon_dbg_print("%s-%d: before free\n",
				    group_name, table_index);
				scsymon_free_qdev_statuss(plqdev_statuss);
				scsymon_dbg_print("%s-%d: after free\n",
				    group_name, table_index);
				plqdev_statuss = NULL;
				ptable->pdata = NULL;
			}
			break;
		default:
			break;
		}
		break;
	case SCSYMON_GROUP_DEVGRP_STATUS:
		switch (table_index) {
		case SCSYMON_TABLE_DEVGRP_STATUS:
			pldevgrp_statuss = ptable->pdata;
			if (pldevgrp_statuss != NULL) {
				scsymon_dbg_print("%s-%d: before free\n",
				    group_name, table_index);
				scsymon_free_devgrp_statuss(pldevgrp_statuss);
				scsymon_dbg_print("%s-%d: after free\n",
				    group_name, table_index);
				pldevgrp_statuss = NULL;
				ptable->pdata = NULL;
			}
			break;
		case SCSYMON_TABLE_REPLICA_STATUS:
			plreplica_statuss = ptable->pdata;
			if (plreplica_statuss != NULL) {
				scsymon_dbg_print("%s-%d: before free\n",
				    group_name, table_index);
				scsymon_free_replica_statuss(plreplica_statuss);
				scsymon_dbg_print("%s-%d: after free\n",
				    group_name, table_index);
				plreplica_statuss = NULL;
				ptable->pdata = NULL;
			}
			break;
		default:
			break;
		}
		break;
	case SCSYMON_GROUP_TRANSPORT_STATUS:
		switch (table_index) {
		case SCSYMON_TABLE_PATH_STATUS:
			plpath_statuss = ptable->pdata;
			if (plpath_statuss != NULL) {
				scsymon_dbg_print("%s-%d: before free\n",
				    group_name, table_index);
				scsymon_free_path_statuss(plpath_statuss);
				scsymon_dbg_print("%s-%d: after free\n",
				    group_name, table_index);
				plpath_statuss = NULL;
				ptable->pdata = NULL;
			}
			break;
		default:
			break;
		}
		break;
	case SCSYMON_GROUP_RG_STATUS:
		switch (table_index) {
		case SCSYMON_TABLE_FORG_RG_STATUS:
		case SCSYMON_TABLE_SCRG_RG_STATUS:
			plrg_statuss = ptable->pdata;
			if (plrg_statuss != NULL) {
				scsymon_dbg_print("%s-%d: before free\n",
				    group_name, table_index);
				scsymon_free_rg_statuss(plrg_statuss);
				scsymon_dbg_print("%s-%d: after free\n",
				    group_name, table_index);
				plrg_statuss = NULL;
				ptable->pdata = NULL;
			}
			break;
		case SCSYMON_TABLE_FORG_RS_STATUS:
		case SCSYMON_TABLE_SCRG_RS_STATUS:
			plrs_statuss = ptable->pdata;
			if (plrs_statuss != NULL) {
				scsymon_dbg_print("%s-%d: before free\n",
				    group_name, table_index);
				scsymon_free_rs_statuss(plrs_statuss);
				scsymon_dbg_print("%s-%d: after free\n",
				    group_name, table_index);
				plrs_statuss = NULL;
				ptable->pdata = NULL;
			}
			break;
		default:
			break;
		}
		break;
	default:
		break;
	}

	if (error != SCSYMON_ENOERR) {
		scsymon_dbg_print("========================\n");
		print_error(error);
		scsymon_dbg_print("========================\n");
	}
}

void
make_group_string(scsymon_group_index_t group_index)
{
	unsigned int i;

	/* make string for each table */
	for (i = 0; i < group[group_index].table_count; i++)
		make_table_string(group_index, i);
}		

void
make_table_string(scsymon_group_index_t group_index,
    scsymon_table_index_t table_index)
{
	char *group_name = group[group_index].pname;
	scsymon_table_t *ptable = &group[group_index].pltables[table_index];
	unsigned int buf_size = ptable->size;
	scsymon_cluster_config_t *pcluster_config = NULL;
	scsymon_cluster_status_t *pcluster_status = NULL;
	scsymon_node_config_t *pnode_config = NULL;
	scsymon_node_status_t *pnode_status = NULL;
	scsymon_node_device_config_t *pnodedev_config = NULL;
	scsymon_node_device_status_t *pnodedev_status = NULL;
	scsymon_devgrp_config_t *pdevgrp_config = NULL;
	scsymon_devgrp_status_t *pdevgrp_status = NULL;
	scsymon_replica_status_t *preplica_status = NULL;
	scsymon_qdev_config_t *pqdev_config = NULL;
	scsymon_qdev_status_t *pqdev_status = NULL;
	scsymon_qdevport_config_t *pqdevport_config = NULL;
	scsymon_path_status_t *ppath_status = NULL;
	scsymon_transport_adapter_config_t *padapter_config = NULL;
	scsymon_junction_config_t *pjunction_config = NULL;
	scsymon_cable_config_t *pcable_config = NULL;
	scsymon_rt_config_t *prt_config = NULL;
	scsymon_rt_method_config_t *prt_method_config = NULL;
	scsymon_rt_param_config_t *prt_param_config = NULL;
	scsymon_rt_rs_config_t *prt_rs_config = NULL;
	scsymon_rg_config_t *prg_config = NULL;
	scsymon_rg_status_t *prg_status = NULL;
	scsymon_rs_config_t *prs_config = NULL;
	scsymon_rs_status_t *prs_status = NULL;
	scsymon_rs_prop_config_t *prs_prop_config = NULL;
	unsigned int len = 0;
	unsigned int old_len;
	char format_string[SCSYMON_MAX_STRING_LEN];
	scsymon_errno_t scsymon_error = SCSYMON_ENOERR;

	if (ptable == NULL)
		return;

	/*
	 * clean up the table string
	 */
	(void) strcpy(ptable->pstring, "");

	if (ptable->pdata == NULL)
		return;

	switch (group_index) {
	case SCSYMON_GROUP_CLUSTER_CONFIG:
		switch (table_index) {
		case SCSYMON_TABLE_CLUSTER_CONFIG:
			pcluster_config = ptable->pdata;
			(void) snprintf(ptable->pstring, buf_size,
			    "%s\n%s\n%s\n%s\n%s\n%s\n",
			    pcluster_config->pname,
			    pcluster_config->pinstall_mode,
			    pcluster_config->pprivnetaddr,
			    pcluster_config->pprivnetmask,
			    pcluster_config->pauthtype,
			    pcluster_config->pauthlist);
			break;
		case SCSYMON_TABLE_NODE_CONFIG:
			pnode_config = ptable->pdata;
			while (pnode_config != NULL) {
				(void) snprintf(ptable->pstring + len,
				    buf_size - len,
				    "%s\n%d\n%s\n%s\n",
				    pnode_config->pname,
				    pnode_config->default_vote,
				    pnode_config->pprivnethostname,
				    pnode_config->padapterlist);

				old_len = len;
				len = strlen(ptable->pstring);
				if (len >= buf_size - 1) {
					scsymon_error =
					    scsymon_double_buffer_size(\
					    ptable->size,
					    ptable->pstring,
					    &ptable->size,
					    &ptable->pstring);

					if (scsymon_error != SCSYMON_ENOERR)
						goto cleanup;

					buf_size = ptable->size;
					len = old_len;
					continue;
				} else
					pnode_config = pnode_config->pnext;
			}

			break;
		case SCSYMON_TABLE_NODEDEV_CONFIG:
			pnodedev_config = ptable->pdata;
			while (pnodedev_config != NULL) {
				(void) snprintf(ptable->pstring + len,
				    buf_size - len,
				    "%s\n%s\n%s\n%s\n",
				    pnodedev_config->pname,
				    pnodedev_config->pqdevlist,
				    pnodedev_config->pdevgrplist,
				    pnodedev_config->prglist);

				old_len = len;
				len = strlen(ptable->pstring);
				if (len >= buf_size - 1) {
					scsymon_error =
					    scsymon_double_buffer_size(\
					    ptable->size,
					    ptable->pstring,
					    &ptable->size,
					    &ptable->pstring);

					if (scsymon_error != SCSYMON_ENOERR)
						goto cleanup;

					buf_size = ptable->size;
					len = old_len;
					continue;
				} else
					pnodedev_config =
					    pnodedev_config->pnext;
			}
			break;
		case SCSYMON_TABLE_QDEV_CONFIG:
			pqdev_config = ptable->pdata;
			while (pqdev_config != NULL) {
				(void) snprintf(ptable->pstring + len,
				    buf_size - len,
				    "%s\n%s\n%d\n%d\n%s\n",
				    pqdev_config->pname,
				    pqdev_config->ppath,
				    pqdev_config->enabled,
				    pqdev_config->default_vote,
				    pqdev_config->pportlist);

				old_len = len;
				len = strlen(ptable->pstring);
				if (len >= buf_size - 1) {
					scsymon_error =
					    scsymon_double_buffer_size(\
					    ptable->size,
					    ptable->pstring,
					    &ptable->size,
					    &ptable->pstring);

					if (scsymon_error != SCSYMON_ENOERR)
						goto cleanup;

					buf_size = ptable->size;
					len = old_len;
					continue;
				} else
					pqdev_config = pqdev_config->pnext;
			}
			break;
		case SCSYMON_TABLE_QDEVPORT_CONFIG:
			pqdevport_config = ptable->pdata;
			while (pqdevport_config != NULL) {
				(void) snprintf(ptable->pstring + len,
				    buf_size - len,
				    "%s\n%s\n%d\n",
				    pqdevport_config->pname,
				    pqdevport_config->pnode,
				    pqdevport_config->enabled);

				old_len = len;
				len = strlen(ptable->pstring);
				if (len >= buf_size - 1) {
					scsymon_error =
					    scsymon_double_buffer_size(\
					    ptable->size,
					    ptable->pstring,
					    &ptable->size,
					    &ptable->pstring);

					if (scsymon_error != SCSYMON_ENOERR)
						goto cleanup;

					buf_size = ptable->size;
					len = old_len;
					continue;
				} else
					pqdevport_config =
					    pqdevport_config->pnext;
			}
			break;
		case SCSYMON_TABLE_TRANSPORT_ADAPTER_CONFIG:
			padapter_config = ptable->pdata;
			while (padapter_config != NULL) {
				(void) snprintf(ptable->pstring + len,
				    buf_size - len,
				    "%s\n%s\n%s\n%d\n",
				    padapter_config->pnode,
				    padapter_config->padapter,
				    padapter_config->ptype,
				    padapter_config->enabled);

				old_len = len;
				len = strlen(ptable->pstring);
				if (len >= buf_size - 1) {
					scsymon_error =
					    scsymon_double_buffer_size(\
					    ptable->size,
					    ptable->pstring,
					    &ptable->size,
					    &ptable->pstring);

					if (scsymon_error != SCSYMON_ENOERR)
						goto cleanup;

					buf_size = ptable->size;
					len = old_len;
					continue;
				} else
					padapter_config =
					    padapter_config->pnext;
			}
			break;
		case SCSYMON_TABLE_JUNCTION_CONFIG:
			pjunction_config = ptable->pdata;
			while (pjunction_config != NULL) {
				(void) snprintf(ptable->pstring + len,
				    buf_size - len,
				    "%s\n%s\n%d\n%s\n",
				    pjunction_config->pname,
				    pjunction_config->ptype,
				    pjunction_config->enabled,
				    pjunction_config->pportlist);

				old_len = len;
				len = strlen(ptable->pstring);
				if (len >= buf_size - 1) {
					scsymon_error =
					    scsymon_double_buffer_size(\
					    ptable->size,
					    ptable->pstring,
					    &ptable->size,
					    &ptable->pstring);

					if (scsymon_error != SCSYMON_ENOERR)
						goto cleanup;

					buf_size = ptable->size;
					len = old_len;
					continue;
				} else
					pjunction_config =
					    pjunction_config->pnext;
			}
			break;
		case SCSYMON_TABLE_CABLE_CONFIG:
			pcable_config = ptable->pdata;
			while (pcable_config != NULL) {
				(void) snprintf(ptable->pstring + len,
				    buf_size - len,
				    "%s\n%s\n%s\n%s\n%d\n",
				    pcable_config->pend_type1,
				    pcable_config->pend1,
				    pcable_config->pend_type2,
				    pcable_config->pend2,
				    pcable_config->enabled);

				old_len = len;
				len = strlen(ptable->pstring);
				if (len >= buf_size - 1) {
					scsymon_error =
					    scsymon_double_buffer_size(\
					    ptable->size,
					    ptable->pstring,
					    &ptable->size,
					    &ptable->pstring);

					if (scsymon_error != SCSYMON_ENOERR)
						goto cleanup;

					buf_size = ptable->size;
					len = old_len;
					continue;
				} else
					pcable_config = pcable_config->pnext;
			}
			break;
		default:
			break;
		}
		break;
	case SCSYMON_GROUP_DEVGRP_CONFIG:
		switch (table_index) {
		case SCSYMON_TABLE_DEVGRP_CONFIG:
			pdevgrp_config = ptable->pdata;
			while (pdevgrp_config != NULL) {
				(void) snprintf(ptable->pstring + len,
				    buf_size - len,
				    "%s\n%s\n%d\n%s\n%d\n%s\n",
				    pdevgrp_config->pname,
				    pdevgrp_config->pservice_type,
				    pdevgrp_config->failback_enabled,
				    pdevgrp_config->pnodelist,
				    pdevgrp_config->node_order,
				    pdevgrp_config->pdevicelist);

				old_len = len;
				len = strlen(ptable->pstring);
				if (len >= buf_size - 1) {
					scsymon_error =
					    scsymon_double_buffer_size(\
					    ptable->size,
					    ptable->pstring,
					    &ptable->size,
					    &ptable->pstring);

					if (scsymon_error != SCSYMON_ENOERR)
						goto cleanup;

					buf_size = ptable->size;
					len = old_len;
					continue;
				} else
					pdevgrp_config = pdevgrp_config->pnext;
			}
			break;
		default:
			break;
		}
		break;
	case SCSYMON_GROUP_RT_CONFIG:
		switch (table_index) {
		case SCSYMON_TABLE_RT_CONFIG:
			prt_config = ptable->pdata;
			(void) snprintf(format_string, buf_size, "%s",
			    "%s\n%s\n%s\n%s\n%d\n%s\n%d\n%s\n%s\n%d\n%s\n%s\n");
			while (prt_config != NULL) {
				(void) snprintf(ptable->pstring + len,
				    buf_size - len,
				    format_string,
				    prt_config->pname,
				    prt_config->pinstalled_nodes,
				    prt_config->pdesc,
				    prt_config->pbasedir,
				    prt_config->single_inst,
				    prt_config->pinit_nodes,
				    prt_config->failover,
				    prt_config->psysdeftype,
				    prt_config->pdepend,
				    prt_config->api_version,
				    prt_config->pversion,
				    prt_config->ppkglist);

				old_len = len;
				len = strlen(ptable->pstring);
				if (len >= buf_size - 1) {
					scsymon_error =
					    scsymon_double_buffer_size(\
					    ptable->size,
					    ptable->pstring,
					    &ptable->size,
					    &ptable->pstring);

					if (scsymon_error != SCSYMON_ENOERR)
						goto cleanup;

					buf_size = ptable->size;
					len = old_len;
					continue;
				} else
					prt_config = prt_config->pnext;
			}
			break;
		case SCSYMON_TABLE_RT_METHOD_CONFIG:
			prt_method_config = ptable->pdata;
			(void) snprintf(format_string, buf_size, "%s%s",
			    "%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n",
			    "%s\n%s\n%s\n%s\n%s\n");

			while (prt_method_config != NULL) {
				(void) snprintf(ptable->pstring + len,
				    buf_size - len,
				    format_string,
				    prt_method_config->pname,
				    prt_method_config->pstart,
				    prt_method_config->pstop,
				    prt_method_config->pprim_change,
				    prt_method_config->pvalidate,
				    prt_method_config->pupdate,
				    prt_method_config->pinit,
				    prt_method_config->pfini,
				    prt_method_config->pboot,
				    prt_method_config->pmon_init,
				    prt_method_config->pmon_start,
				    prt_method_config->pmon_stop,
				    prt_method_config->pmon_check,
				    prt_method_config->ppre_net_start,
				    prt_method_config->ppost_net_stop);

				old_len = len;
				len = strlen(ptable->pstring);
				if (len >= buf_size - 1) {
					scsymon_error =
					    scsymon_double_buffer_size(\
					    ptable->size,
					    ptable->pstring,
					    &ptable->size,
					    &ptable->pstring);

					if (scsymon_error != SCSYMON_ENOERR)
						goto cleanup;

					buf_size = ptable->size;
					len = old_len;
					continue;
				} else
					prt_method_config =
					    prt_method_config->pnext;
			}
			break;
		case SCSYMON_TABLE_RT_PARAM_CONFIG:
			prt_param_config = ptable->pdata;
			(void) snprintf(format_string, buf_size, "%s",
			    "%s\n%s\n%d\n%d\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n");
			while (prt_param_config != NULL) {
				(void) snprintf(ptable->pstring + len,
				    buf_size - len,
				    format_string,
				    prt_param_config->prt_name,
				    prt_param_config->pname,
				    prt_param_config->ext,
				    prt_param_config->per_node,
				    prt_param_config->ptunable,
				    prt_param_config->ptype,
				    prt_param_config->pdefault,
				    prt_param_config->pmin,
				    prt_param_config->pmax,
				    prt_param_config->parraymin,
				    prt_param_config->parraymax,
				    prt_param_config->pdesc);

				old_len = len;
				len = strlen(ptable->pstring);
				if (len >= buf_size - 1) {
					scsymon_error =
					    scsymon_double_buffer_size(\
					    ptable->size,
					    ptable->pstring,
					    &ptable->size,
					    &ptable->pstring);

					if (scsymon_error != SCSYMON_ENOERR)
						goto cleanup;

					buf_size = ptable->size;
					len = old_len;
					continue;
				} else
					prt_param_config =
					    prt_param_config->pnext;
			}
			break;
		case SCSYMON_TABLE_RT_RS_CONFIG:
			prt_rs_config = ptable->pdata;
			while (prt_rs_config != NULL) {
				(void) snprintf(ptable->pstring + len,
				    buf_size - len,
				    "%s\n%s\n%s\n",
				    prt_rs_config->prt_name,
				    prt_rs_config->prg_name,
				    prt_rs_config->prslist);

				old_len = len;
				len = strlen(ptable->pstring);
				if (len >= buf_size - 1) {
					scsymon_error =
					    scsymon_double_buffer_size(\
					    ptable->size,
					    ptable->pstring,
					    &ptable->size,
					    &ptable->pstring);

					if (scsymon_error != SCSYMON_ENOERR)
						goto cleanup;

					buf_size = ptable->size;
					len = old_len;
					continue;
				} else
					prt_rs_config = prt_rs_config->pnext;
			}
			break;
		default:
			break;
		}
		break;
	case SCSYMON_GROUP_RG_CONFIG:
		switch (table_index) {
		case SCSYMON_TABLE_FORG_RG_CONFIG:
		case SCSYMON_TABLE_SCRG_RG_CONFIG:
			prg_config = ptable->pdata;
			(void) snprintf(format_string, buf_size, "%s",
			    "%s\n%s\n%s\n%s\n%d\n%d\n%d\n%d\n%s\n%s\n%s\n");
			while (prg_config != NULL) {
				(void) snprintf(ptable->pstring + len,
				    buf_size - len,
				    format_string,
				    prg_config->pname,
				    prg_config->pprimaries,
				    prg_config->pdesc,
				    prg_config->prslist,
				    prg_config->maxPrim,
				    prg_config->desPrim,
				    prg_config->failback,
				    prg_config->net_depend,
				    prg_config->pRG_depend,
				    prg_config->pglobal_res_used,
				    prg_config->ppath_prefix);

				old_len = len;
				len = strlen(ptable->pstring);
				if (len >= buf_size - 1) {
					scsymon_error =
					    scsymon_double_buffer_size(\
					    ptable->size,
					    ptable->pstring,
					    &ptable->size,
					    &ptable->pstring);

					if (scsymon_error != SCSYMON_ENOERR)
						goto cleanup;

					buf_size = ptable->size;
					len = old_len;
					continue;
				} else
					prg_config = prg_config->pnext;
			}
			break;
		case SCSYMON_TABLE_FORG_RS_CONFIG:
		case SCSYMON_TABLE_SCRG_RS_CONFIG:
			prs_config = ptable->pdata;
			while (prs_config != NULL) {
				(void) snprintf(ptable->pstring + len,
				    buf_size - len,
				    "%s\n%s\n%s\n%s\n%s\n%s\n%s\n",
				    prs_config->prg_name,
				    prs_config->prs_name,
				    prs_config->ptype,
				    prs_config->on_off_switch,
				    prs_config->monitored_switch,
				    prs_config->pstrong_depend,
				    prs_config->pweak_depend);

				old_len = len;
				len = strlen(ptable->pstring);
				if (len >= buf_size - 1) {
					scsymon_error =
					    scsymon_double_buffer_size(\
					    ptable->size,
					    ptable->pstring,
					    &ptable->size,
					    &ptable->pstring);

					if (scsymon_error != SCSYMON_ENOERR)
						goto cleanup;

					buf_size = ptable->size;
					len = old_len;
					continue;
				} else
					prs_config = prs_config->pnext;
			}
			break;
		case SCSYMON_TABLE_FORG_RS_COM_PROP_CONFIG:
		case SCSYMON_TABLE_SCRG_RS_COM_PROP_CONFIG:
		case SCSYMON_TABLE_FORG_RS_EXT_PROP_CONFIG:
		case SCSYMON_TABLE_SCRG_RS_EXT_PROP_CONFIG:
		case SCSYMON_TABLE_FORG_RS_TIMEOUT_CONFIG:
		case SCSYMON_TABLE_SCRG_RS_TIMEOUT_CONFIG:
			prs_prop_config = ptable->pdata;
			while (prs_prop_config != NULL) {
				(void) snprintf(ptable->pstring + len,
				    buf_size - len,
				    "%s\n%s\n%s\n%s\n%s\n",
				    prs_prop_config->prg_name,
				    prs_prop_config->prs_name,
				    prs_prop_config->pprop_name,
				    prs_prop_config->pprop_value,
				    prs_prop_config->pprop_desc);

				old_len = len;
				len = strlen(ptable->pstring);
				if (len >= buf_size - 1) {
					scsymon_error =
					    scsymon_double_buffer_size(\
					    ptable->size,
					    ptable->pstring,
					    &ptable->size,
					    &ptable->pstring);

					if (scsymon_error != SCSYMON_ENOERR)
						goto cleanup;

					buf_size = ptable->size;
					len = old_len;
					continue;
				} else
					prs_prop_config =
					    prs_prop_config->pnext;
			}
			break;
		default:
			break;
		}
		break;
	case SCSYMON_GROUP_CLUSTER_STATUS:
		switch (table_index) {
		case SCSYMON_TABLE_CLUSTER_STATUS:
			pcluster_status = ptable->pdata;
			(void) snprintf(ptable->pstring,
			    buf_size,
			    "%s\n%d\n%d\n",
			    pcluster_status->pname,
			    pcluster_status->min_vote,
			    pcluster_status->current_vote);
			break;
		case SCSYMON_TABLE_NODE_STATUS:
			pnode_status = ptable->pdata;
			while (pnode_status != NULL) {
				(void) snprintf(ptable->pstring + len,
				    buf_size - len,
				    "%s\n%s\n%d\n",
				    pnode_status->pname,
				    pnode_status->pstate,
				    pnode_status->current_vote);

				old_len = len;
				len = strlen(ptable->pstring);
				if (len >= buf_size - 1) {
					scsymon_error =
					    scsymon_double_buffer_size(\
					    ptable->size,
					    ptable->pstring,
					    &ptable->size,
					    &ptable->pstring);

					if (scsymon_error != SCSYMON_ENOERR)
						goto cleanup;

					buf_size = ptable->size;
					len = old_len;
					continue;
				} else
					pnode_status = pnode_status->pnext;
			}
			break;
		case SCSYMON_TABLE_NODEDEV_STATUS:
			pnodedev_status = ptable->pdata;
			while (pnodedev_status != NULL) {
				(void) snprintf(ptable->pstring + len,
				    buf_size - len,
				    "%s\n%s\n%s\n",
				    pnodedev_status->pname,
				    pnodedev_status->pdevgrplist,
				    pnodedev_status->prglist);

				old_len = len;
				len = strlen(ptable->pstring);
				if (len >= buf_size - 1) {
					scsymon_error =
					    scsymon_double_buffer_size(\
					    ptable->size,
					    ptable->pstring,
					    &ptable->size,
					    &ptable->pstring);

					if (scsymon_error != SCSYMON_ENOERR)
						goto cleanup;

					buf_size = ptable->size;
					len = old_len;
					continue;
				} else
					pnodedev_status =
					    pnodedev_status->pnext;
			}
			break;
		case SCSYMON_TABLE_QDEV_STATUS:
			pqdev_status = ptable->pdata;
			while (pqdev_status != NULL) {
				(void) snprintf(ptable->pstring + len,
				    buf_size - len,
				    "%s\n%s\n%d\n",
				    pqdev_status->pname,
				    pqdev_status->pstate,
				    pqdev_status->current_vote);

				old_len = len;
				len = strlen(ptable->pstring);
				if (len >= buf_size - 1) {
					scsymon_error =
					    scsymon_double_buffer_size(\
					    ptable->size,
					    ptable->pstring,
					    &ptable->size,
					    &ptable->pstring);

					if (scsymon_error != SCSYMON_ENOERR)
						goto cleanup;

					buf_size = ptable->size;
					len = old_len;
					continue;
				} else
					pqdev_status = pqdev_status->pnext;
			}
			break;
		default:
			break;
		}
		break;
	case SCSYMON_GROUP_DEVGRP_STATUS:
		switch (table_index) {
		case SCSYMON_TABLE_DEVGRP_STATUS:
			pdevgrp_status = ptable->pdata;
			while (pdevgrp_status != NULL) {
				(void) snprintf(ptable->pstring + len,
				    buf_size - len,
				    "%s\n%s\n%s\n",
				    pdevgrp_status->pname,
				    pdevgrp_status->pstate,
				    pdevgrp_status->pprimarylist);

				old_len = len;
				len = strlen(ptable->pstring);
				if (len >= buf_size - 1) {
					scsymon_error =
					    scsymon_double_buffer_size(\
					    ptable->size,
					    ptable->pstring,
					    &ptable->size,
					    &ptable->pstring);

					if (scsymon_error != SCSYMON_ENOERR)
						goto cleanup;

					buf_size = ptable->size;
					len = old_len;
					continue;
				} else
					pdevgrp_status = pdevgrp_status->pnext;
			}
			break;
		case SCSYMON_TABLE_REPLICA_STATUS:
			preplica_status = ptable->pdata;
			while (preplica_status != NULL) {
				(void) snprintf(ptable->pstring + len,
				    buf_size - len,
				    "%s\n%s\n%s\n",
				    preplica_status->pdevgrp_name,
				    preplica_status->pnode_name,
				    preplica_status->pstate);

				old_len = len;
				len = strlen(ptable->pstring);
				if (len >= buf_size - 1) {
					scsymon_error =
					    scsymon_double_buffer_size(\
					    ptable->size,
					    ptable->pstring,
					    &ptable->size,
					    &ptable->pstring);

					if (scsymon_error != SCSYMON_ENOERR)
						goto cleanup;

					buf_size = ptable->size;
					len = old_len;
					continue;
				} else
					preplica_status =
					    preplica_status->pnext;
			}
			break;
		default:
			break;
		}
		break;
	case SCSYMON_GROUP_TRANSPORT_STATUS:
		switch (table_index) {
		case SCSYMON_TABLE_PATH_STATUS:
			ppath_status = ptable->pdata;
			while (ppath_status != NULL) {
				(void) snprintf(ptable->pstring + len,
				    buf_size - len,
				    "%s\n%s\n%s\n",
				    ppath_status->padapter1,
				    ppath_status->padapter2,
				    ppath_status->pstate);

				old_len = len;
				len = strlen(ptable->pstring);
				if (len >= buf_size - 1) {
					scsymon_error =
					    scsymon_double_buffer_size(\
					    ptable->size,
					    ptable->pstring,
					    &ptable->size,
					    &ptable->pstring);

					if (scsymon_error != SCSYMON_ENOERR)
						goto cleanup;

					buf_size = ptable->size;
					len = old_len;
					continue;
				} else
					ppath_status = ppath_status->pnext;
			}
			break;
		default:
			break;
		}
		break;
	case SCSYMON_GROUP_RG_STATUS:
		switch (table_index) {
		case SCSYMON_TABLE_FORG_RG_STATUS:
		case SCSYMON_TABLE_SCRG_RG_STATUS:
			prg_status = ptable->pdata;
			while (prg_status != NULL) {
				(void) snprintf(ptable->pstring + len,
				    buf_size - len,
				    "%s\n%s\n%s\n%s\n",
				    prg_status->pname,
				    prg_status->pprimary,
				    prg_status->pprimary_state,
				    prg_status->prg_state);

				old_len = len;
				len = strlen(ptable->pstring);
				if (len >= buf_size - 1) {
					scsymon_error =
					    scsymon_double_buffer_size(\
					    ptable->size,
					    ptable->pstring,
					    &ptable->size,
					    &ptable->pstring);

					if (scsymon_error != SCSYMON_ENOERR)
						goto cleanup;

					buf_size = ptable->size;
					len = old_len;
					continue;
				} else
					prg_status = prg_status->pnext;
			}
			break;
		case SCSYMON_TABLE_FORG_RS_STATUS:
		case SCSYMON_TABLE_SCRG_RS_STATUS:
			prs_status = ptable->pdata;
			while (prs_status != NULL) {
				(void) snprintf(ptable->pstring + len,
				    buf_size - len,
				    "%s\n%s\n%s\n%s\n%s\n%s\n",
				    prs_status->prg_name,
				    prs_status->prs_name,
				    prs_status->pprimary,
				    prs_status->pprimary_state,
				    prs_status->pfm_state,
				    prs_status->prgm_state);

				old_len = len;
				len = strlen(ptable->pstring);
				/* double the buffer if necessary */
				if (len >= buf_size - 1) {
					scsymon_error =
					    scsymon_double_buffer_size(\
					    ptable->size,
					    ptable->pstring,
					    &ptable->size,
					    &ptable->pstring);

					if (scsymon_error != SCSYMON_ENOERR)
						goto cleanup;

					buf_size = ptable->size;
					len = old_len;
					continue;
				} else
					prs_status = prs_status->pnext;
			}
			break;
		default:
			break;
		}
		break;
	default:
		break;
	}

cleanup:
	scsymon_dbg_print("%s-%d: make_table_string len = %d\n",
	    group_name, table_index, len);
	scsymon_dbg_print("%s\n", ptable->pstring);

	if (scsymon_error != SCSYMON_ENOERR) {
		scsymon_dbg_print("========================\n");
		print_error(scsymon_error);
		scsymon_dbg_print("========================\n");
	}
}
