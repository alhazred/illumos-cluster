/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	QSERVER_SOCKET_H
#define	QSERVER_SOCKET_H

#pragma ident	"@(#)QServerSocket.h	1.14	08/05/20 SMI"

#include <sys/socket.h>

#include <sys/list_def.h>

#include <quorum/quorum_server/qd_qs_msg.h>
#include <quorum/quorum_server/qs_socket.h>

//
// This structure hosts all information related
// to a pending quorum operation. Among which
// the message (operation request) received, and
// the file descriptor on which the reply should be
// sent back.
//
struct quorum_operation : public _SList::ListElem {
	quorum_operation();
	~quorum_operation(); //lint !e1510
	void set_free(int free);
	qs_sockaddr 	socket_addr;
	socklen_t 	socket_len;
	int		file_descriptor;
	qd_qs_msghdr 	msg;
	int		free_data;
	void 		*data;
};

//
// This class implement all socket related operations.
// Among which receiving messages from and sending to clients.
//
class quorum_server_socket {
public:
	quorum_server_socket();
	~quorum_server_socket();

	//
	// Create socket and bind it to a given port.
	//
	int set_values(int family, uint16_t in_port);

	//
	// Sent and receive methods.
	//
	static int send_to(quorum_operation *qop, int socket_out);
	static int receive(quorum_operation *qop, int socket_in);

	//
	// Getters for socket and port information.
	//
	uint16_t get_port();
	int get_sock();

	//
	// Close the socket.
	//
	void close(void);

	//
	// Accept connections and returns a new file descriptor for
	// the socket.
	//
	int accept_connection();

private:
	//
	// Helper.
	//
	int create_socket(int family, in_port_t port);

	//
	// Port and socket used for communication.
	//
	uint16_t port;
	int the_socket;
};


#endif	// QSERVER_SOCKET_H
