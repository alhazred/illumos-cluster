//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef	SCQSD_DEBUG_BUF_H
#define	SCQSD_DEBUG_BUF_H

#pragma ident	"@(#)scqsd_debug_buf.h	1.7	08/05/20 SMI"

#include <stdio.h>
#include <pthread.h>

class dbg_buf;
extern dbg_buf debug_buffer;
#define	SCQS_DEBUG(a) debug_buffer.db_printf a;

//
// This class implements a circular debug buffer
// in a file. It also support circular archive of
// past buffers.
//
class dbg_buf
{
public:
	dbg_buf();
	~dbg_buf();

	//
	// Initialize debugu buffer.
	//
	int initialize(const char *dir);

	//
	// Print in the buffer.
	//
	void db_printf(char *fmt, ...);

	//
	// Close debug buffer.
	//
	void close();

private:

	//
	// Buffer write log.
	//
	pthread_mutex_t	lock;

	//
	// Buffer file.
	//
	FILE *log_file;

	//
	// Current number of lines in the buffer.
	//
	int log_size;
};

#endif // SCQSD_DEBUG_BUF_H
