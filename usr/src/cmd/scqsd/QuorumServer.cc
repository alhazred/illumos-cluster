//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)QuorumServer.cc	1.31	08/05/20 SMI"

#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>    /* sockaddr_in structure */
#include <unistd.h>
#include <strings.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <netdb.h>
#include <signal.h>

#include <quorum/quorum_server/qd_qs_msg.h>
#include <quorum/quorum_server/qs_socket.h>

#include "QuorumServer.h"
#include "QServerSocket.h"
#include "ClusterWorker.h"
#include "scqs_utils.h"

char the_host_name[MAXHOSTNAMELEN+1];
#define	LONG_SLEEP	2592000 // one month - 30 * 24 * 60 * 60

quorum_server *quorum_server::main_quorum_serverp = NULL;

//
// Constructor.
//
quorum_server::quorum_server():
#ifdef SCQSD_DEBUG
	http_server(HTTP_PORT),
	http_buffer(NULL),
#endif /* SCQSD_DEBUG */
	port(0),
	quorum_dir(NULL),
	cluster_number(0),
	removed(0),
	fd_table(NULL),
	fd_size(PENDING_CONNECTION_MAX),
	fd_next(3)
{
#ifdef SCQSD_DEBUG
	http_pos = 0;
	http_size = 0;
	http_buffer = NULL;
#endif /* SCQSD_DEBUG */
	fd_pipe[0] = -1;
	fd_pipe[1] = -1;
	(void) pthread_mutex_init(&operation_list_lock, NULL);
	(void) pthread_mutex_init(&remove_lock, NULL);
	(void) pthread_mutex_init(&administration_lock, NULL);
} /*lint !e1744 */

//
// Destructor.
//
quorum_server::~quorum_server()
{
	quorum_dir = NULL;
#ifdef SCQSD_DEBUG
	if (http_buffer != NULL) {
		free(http_buffer);
	}
#endif /* SCQSD_DEBUG */
	if (fd_table != NULL) {
		free(fd_table);
	}
	if (fd_pipe[0] != -1) {
		(void) close(fd_pipe[0]);
	}
	if (fd_pipe[1] != -1) {
		(void) close(fd_pipe[1]);
	}
	(void) pthread_mutex_destroy(&operation_list_lock);
	(void) pthread_mutex_destroy(&remove_lock);
	(void) pthread_mutex_destroy(&administration_lock);
}

//
// initialize
//
// Initialize the quorum server.
//
int
quorum_server::initialize(const uint16_t in_port, const char *dir)
{
	if (main_quorum_serverp != NULL) {
		return (0);
	}
	int ret = 0;
	ret = cluster_worker_manager::initialize();
	if (ret != 0) {
		return (ret);
	}
	main_quorum_serverp = new quorum_server;
	if (!main_quorum_serverp) {
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		syslog(LOG_ERR,
		    "Failed to allocate memory for Quorum server.");
		return (ENOMEM);
	}
	return (main_quorum_serverp->initialize_int(in_port, dir));
}

//
// signal_handler
//
static void
signal_handler(int sig)
{
	SCQS_DEBUG(("Received signal %d\n", sig));

	switch (sig) {
	case SIGTERM:
		quorum_server::the().mark_stopped();
		break;
	default:
		break;

	}
}

//
// install_signal_handler
//
int
quorum_server::install_signal_handler()
{
	struct sigaction act;

	(void) sigemptyset(&act.sa_mask);
	act.sa_handler = (void(*)(int))signal_handler;
	act.sa_flags = SA_RESTART;

	if (sigaction(SIGTERM, &act, NULL) == -1) {
		return (1);
	}
	return (0);
}

//
// signal_handler_thread
//
void *
quorum_server::signal_handler_thread(void *)
{
	// unblock signals we want to receive
	sigset_t new_mask, orig_mask;
	(void) sigfillset(&new_mask);
	(void) sigdelset(&new_mask, SIGTERM);
	(void) pthread_sigmask(SIG_SETMASK, &new_mask, &orig_mask);
	// Simply wait for signals
	while (true) {
		(void) sleep(LONG_SLEEP);
	}
	return (NULL); /*lint !e527 */
}

//
// mark_stopped
//
// Notifies quorum server of a stop request due
// to a signal.
//
void
quorum_server::mark_stopped()
{
	char value = '!';
	should_stop = 1;
	if (write(fd_pipe[1], &value,
		sizeof (char)) != (ssize_t)sizeof (char)) {
		SCQS_DEBUG(("QS: mark stopped - write on "
		    "pipe failed [%d]\n", errno));
	}
}

//
// initialize_int
//
// Actual initialization.
//
int
quorum_server::initialize_int(const uint16_t in_port, const char *dir)
{
	port = in_port;
	quorum_dir = dir;

#ifdef SCQSD_DEBUG
	int err;
	err = http_server::start();
	if (err != 0) {
		SCQS_DEBUG(("QS: http server init failed\n"));
	} else {
		http_size = 1024;
		http_buffer = (char*)malloc(http_size * sizeof (char));
		if (!http_buffer) {
			http_size = 0;
			SCQS_DEBUG(("QS: Not enought memory to allocate "
			    "http buffer"));
			return (1);
		}
	}
	http_buffer[0] = '\0';
#endif /* SCQSD_DEBUG */

	if (pipe(fd_pipe)) {
		SCQS_DEBUG(("QS: Failed to create pipe: %s\n",
		    strerror(errno)));
		return (1);
	}

	fd_table = (struct pollfd *)malloc(PENDING_CONNECTION_MAX *
	    sizeof (struct pollfd));
	//
	// Debug buffer
	//
	if (debug_buffer.initialize(quorum_dir) != 0) {
		return (1);
	}

	if (master_socket.set_values(AF_INET6, port) != 0) {
		return (1);
	}

	if (master_socket_v4.set_values(AF_INET, port) != 0) {
		return (1);
	}

	pthread_t stat_tid;
	if (pthread_create(&stat_tid, NULL, quorum_server::status_thread,
		(void *)this) != 0) {
		SCQS_DEBUG(("Failed to create status thread: %d\n", errno));
		return (1);
	}

	if (pthread_create(&stat_tid, NULL,
		quorum_server::signal_handler_thread, NULL) != 0) {
		SCQS_DEBUG(("Failed to create signal thread: %d\n", errno));
		return (1);
	}

	if (install_signal_handler() != 0) {
		SCQS_DEBUG(("QS: Failed to install signal handler "
		    "for signal %d.\n"));
		return (1);
	}

	should_stop = 0;

	if (startup_workers() != 0) {
		return (1);
	}
	SCQS_DEBUG(("QS: initialize_int() done.\n"));
	return (0);
}

//
// startup_workers
//
// Browse data directory and starts a worker
// for every valid quorum data file.
//
int
quorum_server::startup_workers()
{
	SCQS_DEBUG(("QS: startup_workers()\n"));
	cluster_worker *worker;
	DIR *dirp;
	struct dirent *direntp;
	struct  stat statbuf;
	char *cluster_name[80];
	uint32_t cluster_id;
	char *string_ptr;

	//
	// Get ready to browse the quorum directory.
	//
	if ((dirp = opendir(quorum_dir)) == NULL) {
		SCQS_DEBUG(("could not open quorum Dir\n"));
		return (1);
	}

	do {
		//
		// Skip hidden files.
		//
		if ((direntp = readdir(dirp)) != NULL) {
			if (direntp->d_name[0] == '.') {
				continue;
			}
			//
			// Skip too long names.
			//
			if ((strlen(quorum_dir)+strlen(direntp->d_name)+1+1) >
			    MAXPATHLEN) { /*lint !e668 */
				continue;
			}
			//
			// Skip non regular files.
			//
			unsigned int tmp_size =
				sizeof (cluster_worker::quorum_info_layout_t);
			(void) snprintf(file, MAXPATHLEN,
			    "%s/%s", quorum_dir, direntp->d_name);
			if ((stat(file, &statbuf) != 0) ||
			    (!(statbuf.st_mode & S_IFREG)) ||
			    ((unsigned int) statbuf.st_size < tmp_size)) {
				continue;
			}
			//
			// Check for expected name format.
			//
			if (strstr(direntp->d_name, ".0x") == NULL) {
				continue;
			}

			(void) strncpy((char*)cluster_name, direntp->d_name,
			    sizeof (cluster_name));
			string_ptr = strchr((char*)cluster_name, '.');
			if (string_ptr == NULL) {
				continue;
			}
			*string_ptr = '\0';

			if (sscanf(string_ptr+1, "%x", &cluster_id) != 1) {
				continue;
			}
			//
			// Configuration file looks valid, we
			// create a worker out of it.
			//
			SCQS_DEBUG(("reading data file %s...\n", file));
			worker = new cluster_worker((char*)cluster_name,
			    cluster_id, quorum_dir);
			if (worker != NULL) {
				if (worker->initialize(master_socket.get_port())
				    != 0) {
					delete worker;
					continue;
				} else {
					cluster_worker_manager::the().
						add_worker(worker);
				}
			} else {
				SCQS_DEBUG(("could not allocate worker\n"));
				(void) closedir(dirp);
				return (1);
			}
		}
	} while (direntp != NULL);

	(void) closedir(dirp);
	return (0);
}

//
// add_new_connection
//
// Add a given socket to the table of socket for which
// we are waiting for a first message (so that then we
// will redirect it to its associated worker).
//
void
quorum_server::add_new_connection(uint32_t new_socket)
{
	struct pollfd *pollfdp;
	unsigned int new_size;
	if (fd_next == fd_size) {
		new_size = 2 * fd_size;
		pollfdp = (struct pollfd *)realloc(fd_table, new_size);
		if (pollfdp == NULL) {
			//
			// drop connection
			//

			//
			// SCMSGS
			// @explanation
			// Need explanation of this message!
			// @user_action
			// Need a user action for this message.
			//
			syslog(LOG_ERR, "Memory reallocation failed. "
			    "Dropping connection request on socket %d.",
			    new_socket);
			SCQS_DEBUG(("Memory reallocation failed. "
			    "Dropping connection request on socket %d.",
			    new_socket));

			(void) close((int)new_socket);
			return;
		} else {
			fd_size = new_size;
			fd_table = pollfdp;
		}
	}
	fd_table[fd_next].fd = (int)new_socket; /*lint !e662 !e661 */
	fd_table[fd_next].events = POLLIN; /*lint !e662 !e661 */
	fd_table[fd_next].revents = 0; /*lint !e662 !e661 */
	fd_next++;
}

//
// remove_connection
//
// Remove given entry in socket table.
//
void
quorum_server::remove_connection(unsigned int idx)
{
	for (unsigned int i = idx; i < fd_next; i++) {
		(void) memcpy(&fd_table[i], &fd_table[i+1],
		    sizeof (struct pollfd)); /*lint !e662 !e661 !e669 !e670 */
	}
	fd_next--;
}

//
// wait_for_requests
//
// Listen to new connections while waiting for a first
// message on newly connected sockets. Dispatch to
// appropriate worker once initial message received.
//
void quorum_server::wait_for_requests()
{
	int selected;
	while (true) {

		//
		// We will listen to connection requests on IPv6 socket.
		//
		fd_table[0].fd = master_socket.get_sock();
		fd_table[0].events = POLLIN;
		fd_table[0].revents = 0;

		//
		// We will listen to connection requests on IPv4 socket.
		//
		fd_table[1].fd = master_socket_v4.get_sock(); /*lint !e661 */
		fd_table[1].events = POLLIN; /*lint !e661 */
		fd_table[1].revents = 0; /*lint !e661 */

		//
		// This pipe is used to awaken the thread when
		// server should go down on signal request.
		//
		fd_table[2].fd = fd_pipe[0]; /*lint !e661 !e662 */
		fd_table[2].events = POLLIN; /*lint !e661 !e662 */
		fd_table[2].revents = 0; /*lint !e661 !e662 */

		SCQS_DEBUG(("QS: Accepting connection... \n"));

		if (fd_next > 3) {
			SCQS_DEBUG(("QS: Waiting for initial messages... \n"));
			for (unsigned int i = 3; i < fd_next; i++) {
				SCQS_DEBUG(("\tidx[%d]socket[%d]\n", i,
				    fd_table[i].fd)); /*lint !e661 !e662 */
			}
		}
		//
		// Wait for connection requests or messages on existing
		// connections.
		//
		do {
			//
			// XXX - we could wakeup periodically
			// and close file_descriptors for which we never
			// received any initial request for sleep duration.
			//
			selected = poll(fd_table, fd_next, -1);
		} while (selected < 0 && errno == EAGAIN);

		if (selected == -1) {
			SCQS_DEBUG(("QS: poll failed - %s\n",
			    strerror(errno)));
			if ((errno == EAGAIN) || (errno == EINTR)) {
				break;
			} else {
				// Unexpected failure
				ASSERT(0);
				break;
			}
		}

		//
		// Check if we received a termination signal.
		//
		if (should_stop == 1) {
			process_stop_message(NULL, 0);
		}

		//
		// Check if there is a new IPv6 connection pending.
		//
		if (fd_table[0].revents & (POLLIN)) {
			int new_socket;
			if ((new_socket =
				master_socket.accept_connection()) < 0) {
				SCQS_DEBUG(("QS:: accept_connection "
				    "failed.\n"));
				master_socket.close();
				if (master_socket.set_values(AF_INET6,
					port) != 0) {
					//
					// SCMSGS
					// @explanation
					// Need explanation of this message!
					// @user_action
					// Need a user action for this
					// message.
					//
					syslog(LOG_ERR, "Failed to "
					    "re-initialize master socket.");
					exit(1);
				}
				continue;
			}
			SCQS_DEBUG(("QS:: accept_connection IPv6\n"));
			add_new_connection((uint32_t)new_socket);
			//
			// Re-enter poll with new socket setup.
			//
			continue;
		}

		//
		// Check if there is a new IPv4 connection pending.
		//
		if (fd_table[1].revents & (POLLIN)) { /*lint !e661 */
			int new_socket;
			if ((new_socket =
				master_socket_v4.accept_connection()) < 0) {
				SCQS_DEBUG(("QS:: accept_connection "
				    "failed.\n"));
				master_socket_v4.close();
				if (master_socket_v4.set_values(AF_INET,
					port) != 0) {
					syslog(LOG_ERR, "Failed to "
					    "re-initialize master socket.");
					exit(1);
				}
				continue;
			}
			SCQS_DEBUG(("QS:: accept_connection IPv4\n"));
			add_new_connection((uint32_t)new_socket);
			//
			// Re-enter poll with new socket setup.
			//
			continue;
		}

		//
		// Now we evaluate the state of existing connections,
		// for read/error status.
		//
		for (unsigned int i = 3; i < fd_next; i++) {
			if (fd_table[i].revents & (POLLERR | POLLHUP |
				POLLNVAL)) { /*lint !e661 !e662 */
				//
				// Check file descriptors for errors.
				//
				SCQS_DEBUG(("QS: poll reports failure on "
				    "sock %d...",
				    fd_table[i].fd)); /*lint !e661 !e662 */
				(void) close(
				    fd_table[i].fd); /*lint !e661 !e662 */
				remove_connection(i);
				continue;
			}
			if (fd_table[i].revents &
			    (POLLIN)) { /*lint !e661 !e662 */
				//
				// Check file descriptors for reads.
				//
				int new_socket =
					fd_table[i].fd; /*lint !e661 !e662 */
				struct quorum_operation *qop = NULL;
				qop = new quorum_operation;
				if (qop == NULL) {
					SCQS_DEBUG(("QS: no memory to "
					    "handle connection of fd %d...",
						new_socket));
					(void) close(new_socket);
					remove_connection(i);
					continue;
				}
				if (quorum_server_socket::receive(qop,
					new_socket) != 0) {
					SCQS_DEBUG(("QS: Receive failed.\n"));
					(void) close(new_socket);
					remove_connection(i);
					delete qop;
					continue;
				}
				// Check version
				if ((qop->msg.get_version_major() !=
					SCQSD_VERSION_MAJOR) ||
				    (qop->msg.get_version_minor() !=
					SCQSD_VERSION_MINOR)) {
					SCQS_DEBUG(("QS: version mismatch.\n"));
					qop->msg.set_error(QD_QS_EIO);
					(void) quorum_server_socket::send_to(
					    qop, new_socket);
					(void) close(new_socket);
					remove_connection(i);
					delete qop;
					continue;
				}

				//
				// We got a first valid message after
				// connection... We now have the sender
				// information (cluster name/id) and the
				// message type... We can now remove
				// the file descriptor from the poll list and
				// process/route the message accordingly.
				//
				remove_connection(i);
				// Process the message
				switch (qop->msg.get_msg_type()) {
				case QD_STATUS:
					process_status_message(qop);
					continue;
				case QD_HOST:
					process_get_host_message(qop,
					    new_socket);
					continue;
				case QD_REMOVE:
					process_remove_message(qop,
					    new_socket);
					continue;
				case QD_STOP:
					process_stop_message(qop, new_socket);
					continue;
				default: // Cluster messages
					process_cluster_message(qop,
					    new_socket);
					break;
				}
			}
		}
	}
}

//
// process_get_host_message
//
// Returns host name.
//
void
quorum_server::process_get_host_message(quorum_operation *qop, int new_sock)
{
	(void) gethostname(the_host_name, MAXHOSTNAMELEN+1);
	SCQS_DEBUG(("QS: Processing Get Host Name request [%s]...\n",
	    the_host_name));
	qop->data = the_host_name;
	qop->set_free(0);
	qop->msg.set_length(MAXHOSTNAMELEN+1);
	qop->msg.set_error(QD_QS_SUCCESS);
	(void) quorum_server_socket::send_to(qop, new_sock);
	delete qop;
	(void) close(new_sock);
}

//
// process_stop_message
//
// Stops all workers and then exits.
//
void
quorum_server::process_stop_message(quorum_operation *qop, int new_sock)
{
	SCQS_DEBUG(("QS: Processing Stop request...\n"));
	MUTEX_LOCK(&administration_lock);
	//
	// Stop Worker Threads
	//
	MUTEX_LOCK(&remove_lock);
	cluster_worker *worker;
	while ((worker = cluster_worker_manager::the().unlink_next_worker())
	    != NULL) {
		removed = 0;
		SCQS_DEBUG(("QS: Stopping worker %s...\n",
		    worker->cluster_name));
		worker->stop();
		while (!removed) {
			COND_WAIT(&remove_cv, &remove_lock);
		}
	}
	MUTEX_UNLOCK(&remove_lock);

	if (qop != NULL) {
		//
		// Reply
		//
		qop->msg.set_error(QD_QS_SUCCESS);
		(void) quorum_server_socket::send_to(qop, new_sock);
		delete qop;
		(void) close(new_sock);
	}

	SCQS_DEBUG(("QS: exiting(0)...\n"));
	MUTEX_UNLOCK(&administration_lock);
	exit(0);
}

//
// process_status_message
//
// Forwards status request to status thread for handling.
//
void
quorum_server::process_status_message(quorum_operation *qop)
{
	//
	// Queue the status request and wakes up
	// status proceeding thread.
	//
	MUTEX_LOCK(&operation_list_lock);
	operation_list.append(qop);
	COND_SIGNAL(&operation_list_cv);
	MUTEX_UNLOCK(&operation_list_lock);
}


//
// process_remove_message
//
// Removes worker and then replies to caller.
//
void
quorum_server::process_remove_message(quorum_operation *qop, int new_sock)
{
	MUTEX_LOCK(&administration_lock);
	SCQS_DEBUG(("QS: Processing Remove request...\n"));
	cluster_worker *worker =
	    cluster_worker_manager::the().get_cluster_worker(qop->msg);

	if (worker) {
		MUTEX_LOCK(&remove_lock);
		removed = 0;
		//
		// Inform worker to stop.
		//
		worker->remove();
		//
		// Wait for worker to be done.
		//
		// Purpose here is to make sure the Server thread
		// sequentializes/completes QD/Worker removal and
		// creation requests (in case target the same QD).
		//
		while (!removed) {
			COND_WAIT(&remove_cv, &remove_lock);
		}
		MUTEX_UNLOCK(&remove_lock);
		qop->msg.set_error(QD_QS_SUCCESS);
	} else {
		qop->msg.set_error(QD_QS_EACCES);
	}
	MUTEX_UNLOCK(&administration_lock);
	(void) quorum_server_socket::send_to(qop, new_sock);
	delete qop;
	(void) close(new_sock);
}

//
// process_cluster_message
//
// Retrieves or creates a worker that should handle the message
// and then forwards message to worker.
//
void
quorum_server::process_cluster_message(quorum_operation *qop, int new_sock)
{
	cluster_worker *worker =
	    cluster_worker_manager::the().get_cluster_worker(qop->msg);

	//
	// Cluster is not known to Quorum Server
	//
	if (!worker) {
		//
		//  Only open(scrub) request is expected to
		//  be a worker creation initial message...
		//
		if (!((qop->msg.get_msg_type() == QD_OPEN) &&
			(qop->msg.op_data.scrub))) {
			SCQS_DEBUG(("QS: unexpected (non QD_OPEN(scrub)) "
				"request with no worker...\n"));
			qop->msg.set_error(QD_QS_ENOENT);
			(void) quorum_server_socket::send_to(qop, new_sock);
			delete qop;
			(void) close(new_sock);
			return;
		}
		SCQS_DEBUG(("QS: No worker, creating one.\n"));
		worker = new cluster_worker(qop->msg.cluster_name,
		    qop->msg.get_cluster_id(), quorum_dir);
		if (worker != NULL) {
			if (worker->initialize(master_socket.get_port()) != 0) {
				//
				// SCMSGS
				// @explanation
				// Need explanation of this message!
				// @user_action
				// Need a user action for this message.
				//
				syslog(LOG_ERR,
				    "Failed to initialize worker thread. "
				    "Cannot provide service to "
				    "cluster %s.", qop->msg.cluster_name);
				SCQS_DEBUG(("Failed to initialize worker "
				    "thread. Cannot provide service to "
				    "cluster %s.", qop->msg.cluster_name));
				qop->msg.set_error(QD_QS_EIO);
				(void) quorum_server_socket::send_to(qop,
				    new_sock);
				delete worker;
				delete qop;
				(void) close(new_sock);
				return;
			} else {
				cluster_worker_manager::the().add_worker(
				    worker);
			}
		} else {
			//
			// SCMSGS
			// @explanation
			// Need explanation of this message!
			// @user_action
			// Need a user action for this message.
			//
			syslog(LOG_ERR,
			    "Memory allocation failed. "
			    "Cannot provide service to cluster %s.",
			    qop->msg.cluster_name);
			SCQS_DEBUG(("Memory allocation failed. "
			    "Cannot provide service to cluster %s.",
			    qop->msg.cluster_name));
			qop->msg.set_error(QD_QS_EIO);
			(void) quorum_server_socket::send_to(qop, new_sock);
			delete qop;
			(void) close(new_sock);
			return;
		}
	} else {
		//
		// Cluster is already known
		//
	}
	//
	// attach new connection
	//
	worker->attach_to_queue_and_signal(qop, new_sock);
}

//
// remove_completed
//
// Called by worker when remove request is completed.
//
void
quorum_server::remove_completed()
{
	MUTEX_LOCK(&remove_lock);
	removed = 1;
	COND_SIGNAL(&remove_cv);
	MUTEX_UNLOCK(&remove_lock);
}

//
// status_thread
//
// Entry point for status thread.
//
void *
quorum_server::status_thread(void *arg)
{
	quorum_server *qsp = (quorum_server *)arg;
	qsp->get_quorum_status();
	// no comeback
	ASSERT(0);
	return ((void *)NULL);
}

//
// get_quorum_status
//
// Consumes and serves status requests.
//
void
quorum_server::get_quorum_status()
{
	quorum_operation *qop;
	while (true) {
		MUTEX_LOCK(&operation_list_lock);

		while ((qop = operation_list.reapfirst()) == NULL) {
			COND_WAIT(&operation_list_cv, &operation_list_lock);
		}
		MUTEX_UNLOCK(&operation_list_lock);
		MUTEX_LOCK(&administration_lock);
		SCQS_DEBUG(("QS: Processing Get Status request...\n"));
		if (cluster_worker_manager::the().get_quorum_status(qop) != 0) {
			qop->msg.set_error(QD_QS_EIO);
		}
		MUTEX_UNLOCK(&administration_lock);
		(void) quorum_server_socket::send_to(qop, qop->file_descriptor);
		(void) close(qop->file_descriptor);
		delete qop;
	}
}

//
// worker_done
//
// Called by worker while getting ready to stop, when
// is safe to remove it from the manager list.
//
void
quorum_server::worker_done(cluster_worker *cwp)
{
	cluster_worker_manager::the().delete_worker(cwp);
}

//
// parse_arguments
//
int
quorum_server::parse_arguments(int, char **)
{
	return (0);
}

//
// the
//
quorum_server &
quorum_server::the()
{
	return (*main_quorum_serverp);
}

//
// Details: using common/cl/sys lists leads to include
// list_def_in.h that will require orb/infrastructure/orb.h
// in non kernel debug mode to support ASSERTs in the destructors
// for _DList and _SList...
// Now at link time definitions for orb_state and asserfail() are
// required.
// Quorum Server software is installed on a node which need not
// have cluster software and is independent. Therefore requiring
// following definitions.
//
#ifdef DEBUG
ORB::orb_state_t ORB::orb_state;
os::mutex_t ORB::state_lock;
extern "C" int
assertfail(const char *assert_string, const char *file_name, int line_number)
{
	SCQS_DEBUG(("Assertion failed: %s,\n\tfile: %s, "
	    "line: %d\n",
		assert_string, file_name, line_number));
	/* Provide symbolic stack trace */
	abort();
	return (0); /*lint !e527 */
}
#endif /* DEBUG */

//
// http stat server compiled for debug bits
//
#ifdef SCQSD_DEBUG

//
// Free http resources
//
void
quorum_server::free_resources(char *)
{
	http_pos = 0;
	http_buffer[0] = '\0';
}

//
// Concatenate string to http buffer.
//
int
quorum_server::concat_string(char *string, uint_t size)
{
	char *new_buffer;

	if (size == 0) {
		size = strlen(string);
	}

	if (http_pos+size >= http_size) {
		http_size *= 2;
		new_buffer = (char *)realloc(http_buffer,
		    http_size  * sizeof (char));
		if (!new_buffer) {
			http_size /= 2;
			SCQS_DEBUG(("Not enought memory to realloc"
			    " http_buffer"));
			return (1);
		}
		http_buffer = new_buffer;
	}

	bcopy(string, http_buffer + http_pos, size);
	http_pos += size;
	http_buffer[http_pos] = '\0'; /*lint !e661 */

	return (0);
}

//
// Process http request.
//
int
quorum_server::process_request(char *, char **answer)
{
	//
	// Head and title.
	//
	(void) concat_string((char*)
	    "<html><head><title>SCQSD Status</title>"
	    "<body bgcolor=\"#666699\" text=\"#FFFFFF\">"
	    "<font face=\"helvetica\" size=\"3\">"
	    "<font size=\"5\"><b>SunCluster Quorum Server Status</b></font>",
	    0);

	quorum_operation *qop = new quorum_operation;
	MUTEX_LOCK(&administration_lock);
	SCQS_DEBUG(("QS: Processing http Get Status request...\n"));
	if (cluster_worker_manager::the().get_quorum_status(qop) != 0) {
		MUTEX_UNLOCK(&administration_lock);
		return (1);
	}
	MUTEX_UNLOCK(&administration_lock);
	char *tmp = (char*)malloc(1024);
	if (tmp == NULL) {
		delete qop;
		return (1);
	}
	//
	// For every QD.
	//
	struct quorum_status *statusp = (quorum_status*)qop->data;
	for (uint_t i = 0; i < qop->msg.get_data_length(); i++) {
		//
		// Name, Id.
		//
		(void) sprintf(tmp, "<br><font size=\"4\"><br>Cluster %s "
		    "(id 0x%8.8X)</font><br><br><table border=\"0\" "
		    "cellpadding=\"0\" cellspacing=\"0\"><tr><td>Reservation"
		    "</td><td WIDTH=\"50\"></td><td>Registrations </td></tr>"
		    "<tr><td><table border=\"1\" cellpadding=\"2\" "
		    "cellspacing=\"0\"><tr><td>Node Id</td><td><center>"
		    "Key</center></td></tr>",
		    statusp[i].cluster_name, statusp[i].cluster_id);
		(void) concat_string((char*)tmp, 0);
		//
		// Reservation Key.
		//
		if (statusp[i].reserved_nid != 0) {
			(void) sprintf(tmp, "<tr><td><center>%d</center></td>"
			    "<td>0x%llx</td></tr>",
			    statusp[i].reserved_nid,
			    key_value(statusp[i].reservation_key));
			(void) concat_string((char*)tmp, 0);
		}
		(void) sprintf(tmp, "</table></td><td></td><td><table "
		    "border=\"1\" cellpadding=\"2\" cellspacing=\"0\"><tr>"
		    "<td>Node Id</td><td><center>Key</center></td></tr>");
		(void) concat_string((char*)tmp, 0);
		int j = 0;
		while ((statusp[i].registered[j]) != 0) {
			//
			// Registration Keys.
			//
			(void) sprintf(tmp, "<tr><td><center>%d</center></td>"
			    "<td>0x%llx</td></tr>",
			    statusp[i].registered[j],
			    key_value(statusp[i].registration_key[j]));
			(void) concat_string((char*)tmp, 0);
			j++;
		}
		(void) concat_string("</table></td></tr></table>", 0);
	}
	//
	// Date, time and document end.
	//
	(void) concat_string((char*) "<br>Page served on <i>", 0);
	time_t the_time = time(NULL);
	(void) concat_string(ctime(&the_time), 0);
	(void) concat_string((char*)"</i><br><hr><br></font></body></html>", 0);
	free(tmp);
	delete qop;
	*answer = http_buffer;
	return (0);
}
#endif /* SCQSD_DEBUG */
