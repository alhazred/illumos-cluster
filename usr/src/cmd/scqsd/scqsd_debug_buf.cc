//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)scqsd_debug_buf.cc	1.12	08/05/20 SMI"

#include "scqsd_debug_buf.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/varargs.h>
#include <pthread.h>
#include <unistd.h>
#include "scqs_utils.h"

dbg_buf debug_buffer;

//
// Constructor.
//
dbg_buf::dbg_buf():
	log_file(NULL),
	log_size(0)
{
	(void) pthread_mutex_init(&lock, NULL);
}

//
// Destructor.
//
dbg_buf::~dbg_buf()
{
	(void) pthread_mutex_destroy(&lock);
}

#define	DBG_BUFFER_LINEMAX 8192

char *dbg_buffer_names[] = {
	".scqsd_dbg_buf",
	".scqsd_dbg_buf.1",
	".scqsd_dbg_buf.2",
	".scqsd_dbg_buf.3",
	".scqsd_dbg_buf.4",
	".scqsd_dbg_buf.5"
};

#define	DBG_BUF_NUM 5

//
// db_printf
//
// This function adds a new debug line
// in the debug buffer.
//
void
dbg_buf::db_printf(char *fmt, ...)
{
	MUTEX_LOCK(&lock);
	if (log_file == NULL) {
		MUTEX_UNLOCK(&lock);
		return;
	}
	va_list ap;
	va_start(ap, fmt); //lint !e40 !e26 !e50 !e10
	//
	// Wrap around ?
	//
	if (log_size >= DBG_BUFFER_LINEMAX) {
		rewind(log_file);
		log_size = 0;
	}
	//
	// Write message header (thread Id and Time stamp).
	//
	int micro_time = (int)((gethrtime() / 10000) % 1000000000);
	(void) fprintf(log_file, "th %p tm %9d: ", (char*)pthread_self(),
	    micro_time);
	//
	// Then write the message body.
	//
	(void) vfprintf(log_file, fmt, ap);
	(void) fflush(log_file);
	log_size++;
	va_end(ap);
	MUTEX_UNLOCK(&lock);
}

//
// initialize
//
// Initialize debug buffer.
//
int
dbg_buf::initialize(const char *dir)
{
	MUTEX_LOCK(&lock);
	if (log_file != NULL) {
		(void) fflush(log_file);
		(void) fclose(log_file);
		log_file = NULL;
	}
	log_size = 0;
	if (chdir(dir) != 0) {
		MUTEX_UNLOCK(&lock);
		return (1);
	}
	//
	// rotates debug buffers archive
	//
	// (only if a non empty buffer file already exists)
	//
	struct  stat statbuf;
	if ((stat(dbg_buffer_names[0], &statbuf) == 0) &&
	    (statbuf.st_size != 0)) {
		(void) unlink(dbg_buffer_names[DBG_BUF_NUM]);
		for (int f = DBG_BUF_NUM;  f >= 1; f--) {
			(void) rename(dbg_buffer_names[f-1],
			    dbg_buffer_names[f]);
		}
	}
	//
	// open debug buffer
	//
	log_file = fopen(dbg_buffer_names[0], "w+");
	int res = chmod(dbg_buffer_names[0], S_IREAD|S_IWRITE);
	if ((log_file == NULL) || (res != 0)) {
		if (log_file != NULL) {
			(void) fclose(log_file);
			log_file = NULL;
		}
		MUTEX_UNLOCK(&lock);
		return (1);
	}
	MUTEX_UNLOCK(&lock);
	return (0);
}

//
// close
//
// Close debug buffer.
//
void
dbg_buf::close()
{
	MUTEX_LOCK(&lock);
	if (log_file != NULL) {
		(void) fflush(log_file);
		(void) fclose(log_file);
		log_file = NULL;
		log_size = 0;
	}
	MUTEX_UNLOCK(&lock);
}
