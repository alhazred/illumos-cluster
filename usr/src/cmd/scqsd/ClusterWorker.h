//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef	CLUSTER_WORKER_H
#define	CLUSTER_WORKER_H

#pragma ident	"@(#)ClusterWorker.h	1.20	09/02/10 SMI"

#include <pthread.h>
#include <sys/list_def.h>
#include "QServerSocket.h"

#define	SCQSD_NODEID_MAX 65 // So that we have 1-64 locations for 64 nodes

#define	QINFO_FILE_MAGIC "SCQS Data File"
#define	QINFO_FILE_MAGIC_SIZE 15
#define	QINFO_FILE_VERSION 1

//
// Support for Atomic Write on Disk of Quorum Information...
//
// CURRENT_QINFO	- disk mapped - most recent committed data.
// ALTERNATIVE_QINFO	- disk mapped - location of next data write.
//
// Always recover/init from CURRENT_QINFO.
// Always write to ALTERNATIVE_QINFO and commit.
//
#define	CURRENT_QINFO	\
	(quorum_info_layout->slot[quorum_info_layout->current_index])
#define	ALTERNATIVE_QINFO	\
	(quorum_info_layout->slot[!quorum_info_layout->current_index])

class qd_qs_msghdr;

//
// Data structure to host quorum status.
//
struct quorum_status {
	quorum_status();

	char		cluster_name[80];
	int		cluster_id;
	nodeid_t	reserved_nid;
	nodeid_t	registered[SCQSD_NODEID_MAX];
	quorum::registration_key_t registration_key[SCQSD_NODEID_MAX];
	quorum::registration_key_t reservation_key;
};

//
// This class implements a list of connections
// (a connection element is composed of node id and
// a file descriptor).
//
class Connection : public _SList::ListElem
{
public:
	~Connection() {}; //lint !e1510
	Connection(nodeid_t nid, int fd) :
		_SList::ListElem(this),
		node_id(nid),
		file_descriptor(fd) {};
	nodeid_t node_id;
	int file_descriptor;
private:
	Connection();
};

//
// This class implements a cluster worker.
//
class cluster_worker :public _SList::ListElem {
public:
	friend cluster_worker_manager;

	enum worker_state  {W_SUSPENDED, W_RUNNING, W_STOPPED, W_REMOVED};

	cluster_worker(const char *cluster, uint32_t id, const char *dir);
	~cluster_worker(); //lint !e1510

	//
	// Initialize cluster worker.
	//
	int initialize(uint_t theport);

	//
	// The independent thread for handling quorum for the cluster
	// starts here.
	//
	static void *run(void *);

	//
	// Called by the thread to process messages put on the work
	// queue for this cluster.
	//
	void process_messages();

	//
	// Called by the QuorumServer to add a request to the
	// cluster's work queue.
	//
	void attach_to_queue_and_signal(struct quorum_operation *qp, int sock);

	//
	// Set of methods called by QuorumServer to change state of
	// this cluster_worker object
	//
	// Asks the cluster worker to just stop, or stop and remove
	// its configuration information.
	//
	void stop();
	void remove();

	//
	// update cluster worker state.
	//
	void suspend();
	void resume();

	//
	// Returns state of the cluster_worker object.
	//
	worker_state get_state();

	//
	// Return quorum status.
	//
	void get_quorum_status(quorum_status *cstp);

	//
	// Stop cluster thread.
	//
	void cleanup_and_stop();

	//
	// Cleanup pending operation queue.
	//
	void cleanup_queue(nodeid_t id, int fd);

	//
	// Implementation of Quorum Operations.
	//
	dq_qs_error	quorum_open(qd_qs_msghdr &msg);
	int		quorum_support_amnesia_prot();
	dq_qs_error	quorum_close(struct quorum_operation *qop);
	dq_qs_error	quorum_register(qd_qs_msghdr &msg);
	dq_qs_error	quorum_reserve(qd_qs_msghdr &msg);
	dq_qs_error	quorum_read_keys(qd_qs_msghdr &msg, void **data);
	dq_qs_error	quorum_read_reservations(qd_qs_msghdr &msg,
				    void **data);
	dq_qs_error	quorum_preempt(qd_qs_msghdr &msg, void **data);

	//
	// This structure hosts the quorum information for
	// the quorum server managed by the cluster worker.
	//
	typedef struct quorum_information {
		quorum::registration_key_t	registered[SCQSD_NODEID_MAX];
		quorum::registration_key_t	reserved[SCQSD_NODEID_MAX];
		nodeid_t			owner;
		uint_t				registration_number;
		uint_t				reservation_number;
	} quorum_info_t;

	//
	// Layout of quorum information on disk
	//
	typedef struct disk_layout {
		char		magic[QINFO_FILE_MAGIC_SIZE]; // magic cookie
		uint_t		version;	// version
		uint8_t		current_index;	// current valid info index
		quorum_info_t	slot[2];	// quorum info slots
	} quorum_info_layout_t;

	//
	// Non quorum related information for current quorum server.
	//
	char cluster_name[80];
	uint32_t cluster_id;

private:

	cluster_worker();

	//
	// Write latest updated quorum information on disk.
	//
	dq_qs_error commit_quorum_info_change();

	//
	// Connections file descriptors manipulation helpers
	//
	void attach(int thesock, nodeid_t nid);
	void detach(nodeid_t nid);
	nodeid_t nodeid_from_fd(int fd);

	//
	// Helpers for normal operation functions.
	//
	void signal();
	void process_quorum_operations();
	nodeid_t nodeid_from_key(quorum::registration_key_t key);
	void print_quorum_info();
	void init_cleanup(bool first_time);
	void handle_quorum_op(struct quorum_operation *qop);
	quorum::registration_key_t *read_keys_internal(qd_qs_msghdr &msg,
					    quorum::registration_key_t *from);

	//
	// Pre-allocated buffer to reduce chances of memory allocation
	// failure later on while processing readkeys calls.
	//
	quorum::registration_key_t	keys_buffer[SCQSD_NODEID_MAX];

	//
	// Quorum Data File.
	//
	char				file[128];
	int				file_descriptor;

	//
	// Data members needed for normal operation.
	//
	quorum_info_t			quorum_info;
	quorum_info_layout_t		*quorum_info_layout;
	uint_t				port;

	//
	// Internal error - the requested operation could not be
	// completed. The error would have been to returned to the node
	// for which the given operation was being performed. When
	// quorum algorithm encounters error it will mark the quorum
	// device offline.
	//
	uint_t				internal_error;
	worker_state			state;

	//
	// List of pending operations and associated lock.
	//
	IntrList<quorum_operation, _SList> 	quorum_operation_list;
	pthread_mutex_t			lock;

	//
	// rwlock for quorum operations. All actual quorum operations
	// are processed serially. The only use for this lock is to
	// serialize get_quorum_status() with rest of the
	// operations. The reason being that get_quorum_status() is
	// called directly by the quorum server rather than being
	// processes as an operation on the operation list. handle_quorum_op()
	// grabs the write lock (although reads reservations and
	// read keys do not require a write lock) and get_quorum_status
	// grabs the readlock.
	//
	pthread_rwlock_t		rw_operation_lock;

	//
	// Polling and socket/nodeid information.
	//
	// enough room for one socket per node + 1 pipe fd (index 0)
	//
	struct pollfd file_descriptors[SCQSD_NODEID_MAX];
	IntrList<Connection, _SList> 	stale_connection_list;

	//
	// Table mapping connected node ids to corresponding socket.
	//
	int fd_table[SCQSD_NODEID_MAX];

	//
	// Pipe used to awake server sleeping in poll().
	//
	int fd_pipe[2];
};

#include "ClusterWorker_in.h"

#endif /* CLUSTER_WORKER_H */
