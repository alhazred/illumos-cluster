//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef	CLUSTER_WORKER_IN_H
#define	CLUSTER_WORKER_IN_H

#pragma ident	"@(#)ClusterWorker_in.h	1.11	08/05/20 SMI"

#include <stdio.h>
#include <errno.h>

//
// Quorum status Constructor.
//
inline
quorum_status::quorum_status():
	cluster_id(0),
	reserved_nid(0)
{
	cluster_name[0] = '\0';
	bzero((char *)registered, NODEID_MAX * sizeof (nodeid_t));
	bzero((char *)registration_key, NODEID_MAX *
	    sizeof (quorum::registration_key_t));
	bzero((char *)&reservation_key, sizeof (quorum::registration_key_t));
}

//
// quorum_support_amnesia_prot
//
inline int
cluster_worker::quorum_support_amnesia_prot()
{
	return (0);
}

//
// resume
//
inline void
cluster_worker::resume()
{
	state = W_RUNNING;
}

//
// suspend
//
inline void
cluster_worker::suspend()
{
	state = W_SUSPENDED;
}

//
// get_state
//
inline cluster_worker::worker_state
cluster_worker::get_state()
{
	return (state);
}

#endif /* CLUSTER_WORKER_IN_H */
