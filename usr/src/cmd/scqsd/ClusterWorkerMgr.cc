//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)ClusterWorkerMgr.cc	1.11	08/05/20 SMI"

#include <pthread.h>
#include <quorum/quorum_server/qd_qs_msg.h>

#include "ClusterWorkerMgr.h"
#include "ClusterWorker.h"
#include "scqs_utils.h"

cluster_worker_manager	*cluster_worker_manager::cluster_worker_managerp = NULL;

cluster_worker_manager::cluster_worker_manager()
	:cluster_number(0)
{
	(void) pthread_mutex_init(&lock, NULL);
}

cluster_worker_manager::~cluster_worker_manager()
{
	(void) pthread_mutex_destroy(&lock);
}

cluster_worker_manager &
cluster_worker_manager::the()
{
	ASSERT(cluster_worker_managerp);
	return (*cluster_worker_managerp);
}

//
// initialize
//
// Constructs manager.
//
int
cluster_worker_manager::initialize()
{
	if (cluster_worker_managerp == NULL) {
		cluster_worker_managerp = new cluster_worker_manager;
		if (cluster_worker_managerp == NULL) {
			return (ENOMEM);
		}
	}
	return (0);
}

//
// add_worker
//
// Add worker to the list.
//
void
cluster_worker_manager::add_worker(cluster_worker *cwp)
{
	if (cwp != NULL) {
		MUTEX_LOCK(&lock);
		workers.append(cwp);
		cluster_number++;
		ASSERT(cluster_number != 0);
		MUTEX_UNLOCK(&lock);
	}
}

//
// delete_worker
//
// Remove a given worker from list.
//
void
cluster_worker_manager::delete_worker(cluster_worker *cwp)
{
	if (cwp == NULL) {
		return;
	}
	MUTEX_LOCK(&lock);
	if (workers.erase(cwp)) {
		cluster_number--;
	}
	MUTEX_UNLOCK(&lock);
}

//
// unlink_next_worker
//
// Remove from list and return next worker.
//
cluster_worker *
cluster_worker_manager::unlink_next_worker()
{
	cluster_worker *cwp;
	MUTEX_LOCK(&lock);
	workers.atfirst();
	if ((cwp = workers.get_current()) != NULL) {
		if (workers.erase(cwp)) {
			cluster_number--;
		}
	}
	MUTEX_UNLOCK(&lock);
	return (cwp);
}

//
// get_cluster_worker
//
// Returns cluster worker for which the message passed as parameter
// is addressed to. Returns NULL if not found.
//
cluster_worker *
cluster_worker_manager::get_cluster_worker(qd_qs_msghdr &msg)
{
	cluster_worker *cwp;

	MUTEX_LOCK(&lock);

	workers.atfirst();

	while ((cwp = workers.get_current()) != NULL) {
		if ((strcmp(cwp->cluster_name, msg.cluster_name) == 0) &&
		    (cwp->cluster_id == msg.get_cluster_id())) {
			MUTEX_UNLOCK(&lock);
			return (cwp);
		}
		workers.advance();
	}
	MUTEX_UNLOCK(&lock);
	return (NULL);
}
//
// get_quorum_status
//
// Returns pointer to area containing the following information:
//	- Cluster name
//	- Node ids of nodes registered with the quorum device.
//	- Node id of node that has reserved the quorum device.
//
int
cluster_worker_manager::get_quorum_status(quorum_operation *qop)
{
	cluster_worker *workerp;

	MUTEX_LOCK(&lock);
	if (cluster_number == 0) {
		qop->data = NULL;
		qop->msg.set_length(0);
		MUTEX_UNLOCK(&lock);
		return (0);
	}
	quorum_status *statusp = new quorum_status[cluster_number]; //lint !e671
	if (statusp == NULL) {
		qop->msg.set_error(QD_QS_EIO);
		qop->data = (void *)NULL;
		qop->msg.set_length(0);
		MUTEX_UNLOCK(&lock);
		return (1);
	}
	int i = 0;
	workers.atfirst();
	while ((workerp = workers.get_current()) != NULL) {
		workerp->get_quorum_status(&statusp[i++]);
		workers.advance();
	}
	qop->data = (void *)statusp;
	qop->msg.set_length(cluster_number * sizeof (quorum_status));
	qop->msg.set_data_length(cluster_number);
	MUTEX_UNLOCK(&lock);
	return (0);
}
