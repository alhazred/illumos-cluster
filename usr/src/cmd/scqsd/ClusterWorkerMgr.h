//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef	CLUSTERWORKERMGR_H
#define	CLUSTERWORKERMGR_H

#pragma ident	"@(#)ClusterWorkerMgr.h	1.8	08/05/20 SMI"

class cluster_worker;
struct quorum_operation;

//
// This class implements a cluster worker manager.
// It manages a simple list workers (add/remove/get/...).
//
class cluster_worker_manager {
public:
	~cluster_worker_manager();

	static int initialize();
	static cluster_worker_manager	&the();

	//
	// Worker operations.
	//
	void		add_worker(cluster_worker *cwp);
	void		delete_worker(cluster_worker *cwp);
	cluster_worker	*get_cluster_worker(qd_qs_msghdr &msg);
	cluster_worker	*unlink_next_worker();
	int		get_quorum_status(quorum_operation *qop);

private:
	cluster_worker_manager();
	IntrList<cluster_worker, _SList> workers;

	//
	// List lock
	//
	pthread_mutex_t			lock;

	//
	// Number of workers in list.
	//
	uint_t				cluster_number;

	static cluster_worker_manager *cluster_worker_managerp;
};


#endif // CLUSTERWORKERMGR_H
