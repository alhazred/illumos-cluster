//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef	SCQS_UTILS_H
#define	SCQS_UTILS_H

#pragma ident	"@(#)scqs_utils.h	1.11	08/05/20 SMI"

#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <strings.h>
#include <sys/resource.h>
#include <errno.h>
#include <stdlib.h>
#include <syslog.h>

#define	SC_SYSLOG_QSD_TAG	"QuorumServer"

#define	MUTEX_LOCK(PARAM) {if (pthread_mutex_lock(PARAM) != 0) {\
		syslog(LOG_ERR, "pthread_mutex_lock failed.");\
		abort(); } }
#define	MUTEX_UNLOCK(PARAM) {if (pthread_mutex_unlock(PARAM) != 0) {\
		syslog(LOG_ERR, "pthread_mutex_unlock failed.");\
		abort(); } }
#define	COND_WAIT(P1, P2) {if (pthread_cond_wait(P1, P2) != 0) {\
		syslog(LOG_ERR, "pthread_cond_wait failed.");\
		abort(); } }
#define	COND_SIGNAL(PARAM) {if (pthread_cond_signal(PARAM) != 0) {\
		syslog(LOG_ERR, "pthread_cond_signal failed.");\
		abort(); } }
#define	RWLOCK_RDLOCK(PARAM) {if (pthread_rwlock_rdlock(PARAM) != 0) {\
		syslog(LOG_ERR, "pthread_rwlock_rdlock failed.");\
		abort(); } }
#define	RWLOCK_WRLOCK(PARAM) {if (pthread_rwlock_wrlock(PARAM) != 0) {\
		syslog(LOG_ERR, "pthread_rwlock_wrlock failed.");\
		abort(); } }
#define	RWLOCK_UNLOCK(PARAM) {if (pthread_rwlock_unlock(PARAM) != 0) {\
		syslog(LOG_ERR, "pthread_rwlock_unlock failed.");\
		abort(); } }

#endif // SCQS_UTILS_H
