//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)QServerSocket.cc	1.22	08/05/20 SMI"

#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <syslog.h>

#include <quorum/quorum_server/qs_socket.h>

#include "QServerSocket.h"
#include "scqsd_debug_buf.h"

//
// Constructor.
//
quorum_operation::quorum_operation()
	:_SList::ListElem(this)
{
	socket_len = sizeof (qs_sockaddr);
	bzero((char *)&socket_addr, socket_len);
	data = NULL;
	free_data = 1;
	file_descriptor = -1;
}

//
// Destructor.
//
quorum_operation::~quorum_operation()
{
	if ((data) && (free_data)) {
		delete data;
	}
}

//
// set_free
//
// Specifies whether or not the reply buffer pointed out
// by this quorum operation should be freed when the
// quorum operation is deleted.
//
void
quorum_operation::set_free(int f)
{
	free_data = f;
}

//
// Constructor.
//
quorum_server_socket::quorum_server_socket():
    port(0),
    the_socket(-1)
{
}

//
// Destructor.
//
quorum_server_socket::~quorum_server_socket()
{
	ASSERT(the_socket == -1);
}

//
// create_socket
//
// Creates a socket for a given family and binds it to
// a given port.
//
int
quorum_server_socket::create_socket(int family, in_port_t in_port)
{
	int new_socket;
	int bind_res;
	struct sockaddr_in sin;
	struct sockaddr_in6 sin6;
	struct sockaddr *name;
	socklen_t namelen;
	const int backlog = 1024;  // Limit number of pending connections

	/* Make sure this is either AF_INET or AF_INET6 */
	if (family != AF_INET && family != AF_INET6) {
		SCQS_DEBUG((" Internal error - "
		    "unsupported network protocol family.\n"));
		return (-1);
	}

	/* Create the socket */
	new_socket = socket(family, SOCK_STREAM, 0);
	if (new_socket < 0) {
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		syslog(LOG_ERR, "Failed to create socket.");
		return (-1);
	}

	/* Tell the system to allow local addresses to be reused. */
	int sock_opt = 1;
	if (setsockopt(new_socket, SOL_SOCKET, SO_REUSEADDR, (void *)&sock_opt,
		sizeof (sock_opt)) == -1) {
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		syslog(LOG_ERR, "Failed to set socket option.");
		(void) ::close(new_socket);
		return (-1);
	}

	/* Bind the socket */
	switch (family) {
	case AF_INET:
		bzero(&sin, sizeof (sin));
		sin.sin_family = AF_INET;
		sin.sin_addr.s_addr = INADDR_ANY;
		sin.sin_port = htons(in_port);
		name = (struct sockaddr *)&sin;
		namelen = sizeof (sin);
		break;

	case AF_INET6:
		bzero(&sin6, sizeof (sin6));
		sin6.sin6_family = AF_INET6;
		sin6.sin6_addr = in6addr_any;
		sin6.sin6_port = htons(in_port);
		name = (struct sockaddr *)&sin6;
		namelen = sizeof (sin6);
		break;

	default:
		(void) ::close(new_socket);
		return (-1);
	}
	bind_res = bind(new_socket, name, namelen);
	if (bind_res < 0) {
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		syslog(LOG_ERR, "Failed to bind socket to"
			    " port %d.", in_port);
		(void) ::close(new_socket);
		return (-1);
	}

	// Listen for connections
	if (listen(new_socket, backlog) < 0) {
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		syslog(LOG_ERR, "Listen() returned error %d.\n",
			errno);
		(void) ::close(new_socket);
		return (-1);
	}

	/* return socket */
	return (new_socket);
}

//
// set_values
//
// Creates a socket and binds it to a given port.
//
int
quorum_server_socket::set_values(int family, uint16_t in_port)
{
	port = in_port;
	the_socket = create_socket(family, in_port);
	if (the_socket != -1) {
		SCQS_DEBUG(("QSS: listening for %s protocol on port %d\n",
		    (family == AF_INET) ? "IPv4" : "IPv6", in_port));
	}

	return ((the_socket != -1) ? 0 : 1);
}

//
// send_to
//
// Sends quorum operation data on a given socket.
//
int
quorum_server_socket::send_to(quorum_operation *qop, int socket_out)
{
	struct msghdr	hdr;
	struct iovec	vec[2];

	SCQS_DEBUG(("QSS: sending msg=%p, datalen=%d, socket =%d...\n",
	    qop, qop->msg.get_length(), socket_out));

	//
	// Set cookie string in header
	//
	qop->msg.set_cookie();

	vec[0].iov_base = (char *)&qop->msg;
	vec[0].iov_len = MSGHDR_SIZE;

	if (qop->msg.get_length()) {
		vec[1].iov_base = (char *)qop->data;
		vec[1].iov_len = (long)qop->msg.get_length();
	}

	hdr.msg_name = (char *)&qop->socket_addr;
	hdr.msg_namelen = qop->socket_len;
	hdr.msg_iov = vec;
	hdr.msg_iovlen = (qop->msg.get_length()) ? 2 : 1;

	hdr.msg_accrights = NULL;
	hdr.msg_accrightslen = 0;
	int error = sendmsg(socket_out, &hdr, 0);
	if (error != (int)(MSGHDR_SIZE + qop->msg.get_length())) {
		SCQS_DEBUG(("QSS: sendmsg failed with error = %d\n",
		    errno));
		return (1);
	}
	return (0);
}

//
// receive
//
// Receive quorum operation data from a given socket.
//
int
quorum_server_socket::receive(quorum_operation *qop, int socket_in)
{
	struct msghdr	hdr;
	struct iovec	vec[2];
	int		ret;
	// const int	BUF_SIZE = 1024;
	// char		buffer[BUF_SIZE];

	// Initialize the message header.
	vec[0].iov_base = (char *)&qop->msg;
	vec[0].iov_len = MSGHDR_SIZE;
	//
	// Set flags in the msghdr structure to peek and return without
	// blocking.
	//
	hdr.msg_iov = vec;
	hdr.msg_iovlen = 1;
	hdr.msg_name = (char *)&qop->socket_addr;
	hdr.msg_namelen = qop->socket_len;
	hdr.msg_accrights = NULL;
	hdr.msg_accrightslen = 0;

	ret = recvmsg(socket_in, &hdr, MSG_PEEK);
	// ret = recv(socket_in, buffer, sizeof (buffer), MSG_PEEK);
	if (ret <= 0) {
		SCQS_DEBUG(("QSS: message header recvmsg returned %d, "
		    "%d != %d\n", errno, ret, MSGHDR_SIZE));
		return (1);
	}

	qop->socket_len = hdr.msg_namelen;

	SCQS_DEBUG(("\tcluster[%s] t_str[%s] nid[%ld] clid[0x%8.8X]\n",
	    qop->msg.cluster_name,
	    qop->msg.type_str,
	    qop->msg.get_nid(),
	    qop->msg.get_cluster_id()));
	SCQS_DEBUG((
	    "\tmsgnum[%u] length[%u] type[%u] incn[%u] error[%u] v[%hu.%hu]\n",
	    qop->msg.get_msg_num(),
	    qop->msg.get_length(),
	    qop->msg.get_msg_type(),
	    qop->msg.get_incn(),
	    qop->msg.get_error(),
	    qop->msg.get_version_major(),
	    qop->msg.get_version_minor()));

	// Some messages have data attached to the message so read it.
	if (qop->msg.get_length() != 0) {
		qop->data = malloc(qop->msg.get_length());
		if (qop->data == NULL) {
			SCQS_DEBUG(("QSS:recvmsg malloc failed.\n"));
			return (ENOMEM);
		} else {
			vec[1].iov_base = (char *)qop->data;
			vec[1].iov_len = (long)qop->msg.get_length();
			hdr.msg_iovlen = 2;
		}
	}
	ret = recvmsg(socket_in, &hdr, 0);

	if (ret != (int)(MSGHDR_SIZE + qop->msg.get_length())) {
		SCQS_DEBUG(("QSS: 2-recvmsg returned errno. %d\n",
		    errno));
		return (1);
	}

	qop->file_descriptor = socket_in;
	return (0);
}

//
// get_port
//
uint16_t
quorum_server_socket::get_port()
{
	return (port);
}

//
// get_sock
//
int
quorum_server_socket::get_sock()
{
	return (the_socket);
}

//
// close
//
// Closes the socket connection.
//
void
quorum_server_socket::close(void)
{
	(void) ::close(the_socket);
	the_socket = -1;
}

//
// accept_connection
//
// Accept connections and returns a new file descriptor for
// the socket.
//
int
quorum_server_socket::accept_connection()
{
	int	new_socket;
	qs_sockaddr	address;
	socklen_t	sizeof_sockaddr;
	sizeof_sockaddr = (socklen_t)sizeof (struct sockaddr_in);

	// Define the address to accept all TCP connections
	bzero((char *)&address, sizeof (qs_sockaddr));
	address.v4.sin_family = AF_INET;
	address.v4.sin_addr.s_addr = htonl(INADDR_ANY);
	address.v4.sin_port = htons(get_port());

	// Accept the connection
	if ((new_socket = accept(the_socket, (struct sockaddr *)&address,
	    &sizeof_sockaddr)) < 0) {
		SCQS_DEBUG(("QSS: accept_connection failed with errno %d\n",
		    errno));
		return (-1);
	}

	int sock_opt = 1;
	if (setsockopt(new_socket, SOL_SOCKET, SO_KEEPALIVE, (void *)&sock_opt,
	    sizeof (sock_opt)) == -1) {
		SCQS_DEBUG(("setsockopt(SO_KEEPALIVE) failed.\n"));
		(void) ::close(new_socket);
		return (1);
	}

	SCQS_DEBUG(("QSS:accepting connection on port %d for "
	    "socket %d\n", get_port(), new_socket));
	return (new_socket);
}
