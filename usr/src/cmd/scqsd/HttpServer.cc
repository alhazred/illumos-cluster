//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)HttpServer.cc	1.9	08/05/20 SMI"

#ifdef SCQSD_DEBUG
#include <signal.h>
#include <pthread.h>

#include "HttpServer.h"
#include "scqsd_debug_buf.h"

#define	NAMELENGTH 128

//
// Constructor.
//
http_server::http_server(uint16_t _port_number)
{
	server_socket = -1;
	client_socket = -1;
	port_number = _port_number;
}

//
// Destructor.
//
http_server::~http_server()
{
	// pthread_kill(thread, 9);

	if (client_socket != -1) {
		(void) close(client_socket);
		client_socket = -1;
	}

	if (server_socket != -1) {
		(void) close(server_socket);
		server_socket = -1;
	}
}

extern "C" {

//
// start_routine
//
// Starts the server.
//
void *
start_routine(void *arg) {
	http_server *server = (http_server*)arg;

	server->start_handler();

	return (NULL);
}
}

//
// start
//
// Actual start method.
//
int
http_server::start()
{
	int	    err, res;
	pthread_attr_t attributes;
	pthread_t thread;

	if ((err = pthread_attr_init(&attributes)) != 0) {
		SCQS_DEBUG(("pthread_attr_init failed [%d]\n",
			err));
		return (1);
	}

	res = pthread_attr_setscope(&attributes, PTHREAD_SCOPE_SYSTEM);
	if (res != 0) {
		SCQS_DEBUG(("Error in setting thread attributes for http "
			"server thread (%d)\n", res));
		return (1);
	}

	if ((err = pthread_create(&thread, &attributes, start_routine,
		    this)) != 0) {
		SCQS_DEBUG(("Cannot create http server thread [%d]\n", err));
		return (1);
	}

	return (0);
}

//
// start_handler
//
// Serves connections and requests.
//
void
http_server::start_handler()
{
	int err;
	err = socket_start();
	if (err != 0) {
		SCQS_DEBUG(("socketStart failed [%d]\n", err));
		return;
	}

	while (1) {
		accept_client();
		if (client_socket != -1) {
			err = deal_with_client();
			if (err != 0) {
				SCQS_DEBUG(("dealWithClient failed [%d]\n",
					err));
			}
			(void) close(client_socket);
			client_socket = -1;
		}
	}
}

//
// socket_start
//
// Creates server socket.
//
int
http_server::socket_start()
{
	struct sockaddr_in addr;
	struct linger lng;
	char hostname[NAMELENGTH];

	server_socket = socket(AF_INET, SOCK_STREAM, 0);
	if (server_socket == -1) {
		SCQS_DEBUG(("Cannot create http server. No socket [%d]\n",
			errno));
		return (1);
	}

	lng.l_onoff = 1;
	lng.l_linger = 0;
	if (setsockopt(server_socket, SOL_SOCKET, SO_LINGER, &lng,
		sizeof (lng))) {
		SCQS_DEBUG(("setsockopt error [%d]\n", errno));
		(void) close(server_socket);
		server_socket = -1;
		return (1);
	}

	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = htonl(INADDR_ANY);
	addr.sin_port = htons(port_number);

	if (bind(server_socket,
		(struct sockaddr*) &addr,
		sizeof (struct sockaddr_in))) {

		// try any port
		addr.sin_port = htons(0);
		if (bind(server_socket,
			(struct sockaddr*) &addr,
			sizeof (struct sockaddr_in))) {
			SCQS_DEBUG(("Cannot bind port [%d]\n", errno));
			(void) close(server_socket);
			server_socket = -1;
			return (1);
		}
		struct sockaddr_in tmpaddr;
		socklen_t tmplen = sizeof (tmpaddr);
		if (getsockname(server_socket, (struct sockaddr *)
			&tmpaddr, &tmplen)) {
			(void) close(server_socket);
			SCQS_DEBUG(("Error in getsockname [%d]\n", errno));
			(void) close(server_socket);
			server_socket = -1;
			return (1);
		}

		port_number = tmpaddr.sin_port;
	}

	if (listen(server_socket, 1)) {
		(void) close(server_socket);
		SCQS_DEBUG(("Error in listen [%d]\n", errno));
		(void) close(server_socket);
		server_socket = -1;
		return (1);
	}

	if (gethostname(hostname, NAMELENGTH)) {
		SCQS_DEBUG(("Cannot get hostname [%d]\n", errno));
		(void) close(server_socket);
		server_socket = -1;
		return (1);
	}

	SCQS_DEBUG(("http_server: listening on port %d.\n",
		port_number));

	return (0);
}

//
// accept_client
//
void
http_server::accept_client()
{
	socklen_t	   client_length;
	struct sockaddr_in  client_addr;

	client_length = sizeof (struct sockaddr_in);

	client_socket = accept(server_socket,
	    (struct sockaddr*) &client_addr,
	    &client_length);
}

//
// read_line
//
// Reads from client socket.
//
void
http_server::read_line(char *buffer,
    int size)
{
	int pos = 0;

	while ((read(client_socket, buffer + pos, 1) == 1) &&
	    (buffer[pos] != EOF) && (buffer[pos] != '\n')) {
		++pos;
		if (pos+1 == size) {
			break;
		}
	}
	buffer[pos] = '\0';
}

//
// format_request
//
int
http_server::format_request(char *buffer)
{
	int pos = 4;

	while (buffer[pos] != ' ') {
		++pos;
	}

	buffer[pos] = '\0';

	return (4);
}

//
// deal_with_client
//
// Gets client request and processes it.
//
int
http_server::deal_with_client()
{
	char buf[1028];
	char answer_header[1028];
	char *request, *answer_body;
	int err;

	do {
		read_line(buf, 1024);
	} while ((strlen(buf) > 0) && (strncmp(buf, "GET", 3) != 0));

	if (strlen(buf) == 0) {
		SCQS_DEBUG(("The HTTP server received a connection with "
			"no request\n"));
		return (1);
	}

	request = buf + format_request(buf);

	answer_body = NULL;
	err = process_request(request, &answer_body);
	if ((err != 0) || (!answer_body)) {
		SCQS_DEBUG(("processRequest failed [%d]\n",
			err));
		if (err != 0) {
			return (err);
		}
		return (1);
	}

	(void) sprintf(answer_header,
	    "HTTP/1.0 200 OK\nContent-Length: %d\nContent-Type: text/html\n\n",
	    strlen(answer_body));

	(void) write(client_socket, answer_header, strlen(answer_header));
	(void) write(client_socket, answer_body, strlen(answer_body));

	/* Wait for the client to close the socket */
	while ((read(client_socket, buf, 1) == 1) && (buf[0] != EOF)) {
	}

	free_resources(answer_body);
	answer_body = NULL;

	return (0);
}
#endif /* SCQSD_DEBUG */
