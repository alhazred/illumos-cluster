//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef	QUORUM_SERVER_H
#define	QUORUM_SERVER_H

#pragma ident	"@(#)QuorumServer.h	1.19	08/05/20 SMI"

#include <sys/param.h>
#include <quorum/common/quorum_util.h>
#include <poll.h>

#include "QServerSocket.h"
#include "ClusterWorkerMgr.h"
#include "scqsd_debug_buf.h"

#ifdef SCQSD_DEBUG
#include "HttpServer.h"
#define	HTTP_PORT 2809
#endif /* SCQSD_DEBUG */

#define	PENDING_CONNECTION_MAX 128

#ifdef SCQSD_DEBUG
class quorum_server : public http_server {
protected:

	//
	// Process http request.
	//
	int process_request(char *href, char **answer);

	//
	// Free http resources.
	//
	void free_resources(char *answer);

	//
	// Concatenate string to http buffer.
	//
	int concat_string(char *string, uint_t size);
private:
	//
	// Http buffer information.
	//
	char *http_buffer;
	uint_t http_pos;
	uint_t http_size;
#else
    class quorum_server {
#endif /* SCQSD_DEBUG */

public:
	~quorum_server();

	//
	// Returns reference to quorum_server
	//
	static quorum_server	&the();
	static int		initialize(const uint16_t in_port,
				    const char *dir);

	//
	// Wait for requests from cluster nodes.
	//
	void		wait_for_requests();

	//
	// Called by worker while getting ready to stop, when
	// is safe to remove it from the manager list.
	//
	void		worker_done(cluster_worker *cwp);

	//
	// Called by worker when remove request is completed.
	//
	void		remove_completed();

	//
	// Requests Quorum Server to stop.
	//
	void		mark_stopped();
private:
	quorum_server();

	//
	// Actual initialization done here.
	//
	int		initialize_int(const uint16_t in_port,
			    const char *dir);
	//
	// Browse data directory and starts a worker
	// for every valid quorum data file.
	//
	int		startup_workers();


	//
	// Parse input arguments - currently does nothing.
	//
	int		parse_arguments(int argc, char **argv);

	//
	// status thread begin function.
	//
	static void	*status_thread(void *arg);

	//
	// Consumes and serves status requests.
	//
	void		get_quorum_status();

	//
	// Process messages.
	//
	void		process_cluster_message(quorum_operation *qop,
			    int new_sock);
	void		process_remove_message(quorum_operation *qop,
			    int new_sock);
	void		process_status_message(quorum_operation *qop);
	void		process_stop_message(quorum_operation *qop,
			    int new_sock);
	void		process_get_host_message(quorum_operation *qop,
			    int new_sock);

	//
	// Manage table of new connections for which we still wait
	// for an initial message.
	//
	void		add_new_connection(uint32_t new_socket);
	void		remove_connection(unsigned int idx);

	//
	// Socket on which the server accepts new connections.
	//
	quorum_server_socket	master_socket;
	quorum_server_socket	master_socket_v4;

	//
	// Buffer for quorum data files names.
	//
	char		file[MAXPATHLEN];

	//
	// Configuration information.
	//
	uint16_t		port;
	const char		*quorum_dir;
	int			cluster_number;
	static quorum_server	*main_quorum_serverp;

	//
	// Lock and condition variable on operation list.
	//
	pthread_mutex_t			operation_list_lock;
	pthread_cond_t			operation_list_cv;
	IntrList<quorum_operation, _SList>	operation_list;

	//
	// Lock and condition variable for remove
	// synchronization between Server and Worker.
	//
	pthread_mutex_t			remove_lock;
	pthread_cond_t			remove_cv;
	int				removed;

	//
	// Lock for exclusive admin stat/remove operations.
	//
	pthread_mutex_t administration_lock;

	//
	// Table of file descriptors on which server
	// expects to receive an initial message.
	//
	struct pollfd *fd_table;
	unsigned int    fd_size;
	unsigned int    fd_next;

	//
	// Should stop flag.
	//
	int should_stop;

	//
	// Pipe used to awake server sleeping in poll().
	//
	int fd_pipe[2];

	//
	// Signal processing.
	//
	static void *signal_handler_thread(void *arg);
	int install_signal_handler();
};

#endif /* QUORUM_SERVER_H */
