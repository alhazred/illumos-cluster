//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)scqsd.cc	1.30	08/05/20 SMI"

#include <locale.h>
#include <unistd.h>
#include <stdio.h>
#include <signal.h>
#include <libintl.h>
#include <libgen.h>
#include <pthread.h>
#include <syslog.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <netdb.h>
#include <sys/mman.h>
#include <sys/resource.h>
#include <sys/types.h>
#include <sys/priocntl.h>
#include <sys/rtpriocntl.h>
#include <sys/param.h>

#include "scqs_utils.h"
#include "QuorumServer.h"

// Globals variables
int portNum = 0;
char quorumDir[MAXPATHLEN];
int restartCount = 3;

// static variables
static char *progname;
static const char CHAR_OK  = 'y';
static int pfildes[2];

// #defines
#define	SERVICE		"scqsd"
#define	SERVICE_PROTO	"tcp"
#define	DEFAULT_PORT	9000
#define	QUORUM_DIR	"/var/scqsd"
#define	TOO_MANY_FAILURES  "Too many failures. Server will not be restarted."

static void
usage()
{
	(void) printf("Usage: %s [-p <port>] "
	    "[-d quorumDirectory] [-i instanceName]\n", progname);
	exit(1);
}

void validate_parameters(void);
void initialize_and_daemonize(int argc, char **argv);
void start_child_and_monitor(void);

//
// The main process first makes a daemon of the Quorum Server,
// then it forks again a child that will actually act as the Quorum
// server. The parent will then wait and monitor this child, and will
// restart it case of unexpected failure.
//
void
main(int argc, char **argv)
{

	//
	// The main process forks once to create a daemon, and
	// exits. The daemon forks again. The child will be the
	// actual Quorum Server. Parent is responsible for
	// monitoring (and restarting if needed) the child.
	//
	initialize_and_daemonize(argc, argv);
	validate_parameters();
	start_child_and_monitor();

	//
	// Parent stops here
	//
	if (restartCount == 0) {
		exit(1);
	}

	//
	// Child will act as a Quorum Server
	//

	//
	// File descriptor limit
	//
	struct rlimit fdlim;
	if (geteuid() == 0) {
		fdlim.rlim_cur = (rlim_t)RLIM_INFINITY;
		fdlim.rlim_max = (rlim_t)RLIM_INFINITY;
	} else if (getrlimit(RLIMIT_NOFILE, &fdlim) < 0) {
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		syslog(LOG_ERR,
		    "Unable to get file descriptor limit for Quorum server."
		    " %s.", strerror(errno));
	} else {
		//
		// Not super user and getrlimit succeeded.
		// Set the current limit to max allowed.
		//
		fdlim.rlim_cur = fdlim.rlim_max;
	}
	if (setrlimit(RLIMIT_NOFILE, &fdlim) < 0) {
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		syslog(LOG_ERR,
		    "Unable to increase file descriptor limit for "
		    "Quorum server. %s.", strerror(errno));
		//
		// We'll still continue to support as many nodes as
		// possible.
		//
	}

	//
	// Initialize Quorum Server
	//
	if (quorum_server::initialize((uint16_t)portNum, quorumDir) != 0) {
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		syslog(LOG_ERR, "Quorum server initialization failed.");
		exit(2);
	}

	//
	// notify of initialization completion to grand-parent
	//
	uint_t write_size;
	char write_buf;
	write_buf = CHAR_OK;
	write_size = sizeof (char);
	(void) write(pfildes[1], &write_buf, write_size);
	(void) close(pfildes[1]);
	//
	// Start delivering service
	//
	quorum_server::the().wait_for_requests();
}

//
// This function interprets the server command line,
// then daemonizes the current process.
//
void
initialize_and_daemonize(int argc, char **argv)
{
	(void) setlocale(LC_ALL, "");
	(void) textdomain(TEXT_DOMAIN);
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	(void) strcpy(quorumDir, QUORUM_DIR);

	progname = argv[0];

	char option;
	while ((option = (char)getopt(argc, argv, "i:d:p:v")) != EOF) {
		switch (option) {
		case 'd':
			if (sscanf(optarg, "%s", quorumDir) == -1) {
				usage();
				/* NOTREACHED */
			}
			break;
		case 'i':
			if (optarg == NULL) {
				usage();
				/* NOTREACHED */
			}
			break;
		case 'p':
			if (sscanf(optarg, "%d", &portNum) == -1) {
				usage();
				/* NOTREACHED */
			}
			break;
		case '?':
		default:
			usage();
			/* NOTREACHED */
		}
	}
	if (optind < argc) {
		usage();
		/* NOTREACHED */
	}
	//
	// Initialize logging
	//
	openlog(SC_SYSLOG_QSD_TAG, LOG_PID, LOG_DAEMON);

	//
	// prepare pipe
	//
	if (pipe(pfildes) == -1) {
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		syslog(LOG_ERR, "pipe() failed: %s.", strerror(errno));
		exit(1);
	}
	//
	// Daemonize
	//
	pid_t child_pid = fork();
	if (child_pid < 0) {
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		syslog(LOG_ERR, "fork() failed: %s.", strerror(errno));
		exit(1);
	}
	if (child_pid > 0) {
		//
		// Parent waits for child status
		//
		ssize_t read_res;
		uint_t read_size;
		char read_buf;
		(void) close(pfildes[1]);
		read_size = sizeof (char);
		read_res = read(pfildes[0], &read_buf, read_size);
		//
		// handle read result
		//
		if ((read_res == -1) ||
		    (read_res != (ssize_t)read_size) ||
		    (read_buf != CHAR_OK)) {
			exit(1);
		}
		exit(0);
	}
	if (setsid() == (pid_t)-1) {
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		syslog(LOG_ERR, "setsid() failed: %s", strerror(errno));
		exit(1);
	}
	(void) close(0);
	(void) close(1);
	(void) close(2);
	(void) close(pfildes[0]);
}

//
// This function validates the Quorum Server parameters.
//
void
validate_parameters()
{
	//
	// Port number.
	//
	if (portNum == 0) {
		struct servent *entryp =
		    getservbyname(SERVICE, SERVICE_PROTO);
		if (entryp == NULL) {
			portNum = DEFAULT_PORT;
		} else {
			// getservbyname() returned a network-ordered port.
			portNum = ntohs(entryp->s_port);
		}
	}
	struct stat statBuf;
	//
	// Quorum Directory.
	//
	if (stat(quorumDir, &statBuf) != 0) {
		if (errno == ENOENT) {
			int err = mkdir(quorumDir, S_IRWXU | S_IRGRP | S_IXGRP);
			if (err != 0) {
				//
				// SCMSGS
				// @explanation
				// Need explanation of this message!
				// @user_action
				// Need a user action for this message.
				//
				syslog(LOG_ERR,
				    "Could not create quorum directory %s."
				    " %s.", quorumDir, strerror(errno));
				exit(1);
			}
		} else {
			//
			// SCMSGS
			// @explanation
			// Need explanation of this message!
			// @user_action
			// Need a user action for this message.
			//
			syslog(LOG_ERR,
			    "Could not access quorum directory %s."
			    " %s.", quorumDir, strerror(errno));
			exit(1);
		}
	} else {
		//
		// Is this a directory ?
		//
		if (!S_ISDIR(statBuf.st_mode)) {
			//
			// SCMSGS
			// @explanation
			// Need explanation of this message!
			// @user_action
			// Need a user action for this message.
			//
			syslog(LOG_ERR,
			    "%s is not a directory.", quorumDir);
			exit(1);
		}
		if ((statBuf.st_mode & S_IAMB & S_IWUSR) == 0) {
			//
			// SCMSGS
			// @explanation
			// Need explanation of this message!
			// @user_action
			// Need a user action for this message.
			//
			syslog(LOG_ERR,
			    "%s directory is not writable.", quorumDir);
			exit(1);
		}
	}

}

//
// This function creates a child that will act as a Quorum Server
// while parent will be dedicated to monitor the child.
//
void
start_child_and_monitor()
{
	//
	// Signals
	//
	sigset_t new_mask, orig_mask;
	(void) sigfillset(&new_mask);
	(void) sigprocmask(SIG_SETMASK, &new_mask, &orig_mask);

	//
	// Set real time process priority
	//
	pcinfo_t	pc_info;
	(void) strcpy(pc_info.pc_clname, "RT");

	if ((priocntl(P_PID, P_MYID, PC_GETCID, (char *)&pc_info)) != -1) {
		pcparms_t	pc_parms;
		rtparms_t *rtp = (rtparms_t *)(pc_parms.pc_clparms);
		pc_parms.pc_cid = (idtype_t)pc_info.pc_cid;

		rtp->rt_pri = ((rtinfo_t *)pc_info.pc_clinfo)->rt_maxpri;

		/* set the timeslice to 100 ms */
		rtp->rt_tqnsecs = 100000000;
		rtp->rt_tqsecs = 0;

		if (priocntl(P_PID, P_MYID, PC_SETPARMS, (char *)&pc_parms)) {
			//
			// SCMSGS
			// @explanation
			// Need explanation of this message!
			// @user_action
			// Need a user action for this message.
			//
			syslog(LOG_ERR,
			    "Unable to run at real-time. %s.",
			    strerror(errno));
		}
	} else {
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		syslog(LOG_ERR,
		    "Unable run obtain at Real-Time class information. %s.",
		    strerror(errno));
	}

	//
	// locking in memory
	//
	if (mlockall(MCL_CURRENT | MCL_FUTURE) != 0) {
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		syslog(LOG_ERR,
		    "Unable to lock Quorum server in memory. %s.",
		    strerror(errno));
	}

	//
	// fork
	//
	pid_t child_pid;
	do {
		child_pid = fork();

		if (child_pid < 0) {
			//
			// SCMSGS
			// @explanation
			// Need explanation of this message!
			// @user_action
			// Need a user action for this message.
			//
			syslog(LOG_ERR, "fork() failed: %s", strerror(errno));
			exit(1);
		}
		if (child_pid > 0) {
			//
			// Parent will monitor/restart child
			//
			int childStatus;
			while (waitpid(child_pid, &childStatus, 0) != -1) {
				// Wait until waitpid returns due to
				// the child exiting.
			}
			restartCount--;
			if (WIFEXITED(childStatus)) {
				/*lint -e702 */
				//
				// - 0: normal termination
				// - 2: abnormal termination with no restart
				//
				// for anything else restart rule applies
				//
				if ((WEXITSTATUS(childStatus) == 0) ||
				    (WEXITSTATUS(childStatus) == 2)) {
					//
					// SCMSGS
					// @explanation
					// Need explanation of this message!
					// @user_action
					// Need a user action for this
					// message.
					//
					syslog(LOG_NOTICE,
					    "scqsd (pid = %d) "
					    "terminated. Exit status %d.",
					    child_pid,
					    WEXITSTATUS(childStatus));
					exit(WEXITSTATUS(childStatus));
				}
				//
				// SCMSGS
				// @explanation
				// Need explanation of this message!
				// @user_action
				// Need a user action for this message.
				//
				syslog(LOG_ERR, "scqsd (pid = %d) "
				    "terminated. Exit status %d. %s",
				    child_pid,
				    WEXITSTATUS(childStatus),
				    (restartCount) ? "Restarting." :
				    TOO_MANY_FAILURES);
			} else if (WIFSIGNALED(childStatus)) {
				//
				// SCMSGS
				// @explanation
				// Need explanation of this message!
				// @user_action
				// Need a user action for this message.
				//
				syslog(LOG_ERR, "scqsd (pid = %d) "
				    "terminated due to signal %d. %s",
				    child_pid,
				    WTERMSIG(childStatus),
				    (restartCount) ? "Restarting" :
				    TOO_MANY_FAILURES);
			}
			/*lint +e702 */
		} else if (child_pid == 0) {
				break;
		}
	} while (restartCount > 0);
}
