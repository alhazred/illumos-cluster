//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)ClusterWorker.cc	1.28	09/02/10 SMI"

#include <unistd.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <strings.h>
#include <string.h>
#include <syslog.h>
#include <pthread.h>

#include <quorum/quorum_server/qd_qs_msg.h>
#include <quorum/quorum_server/qs_socket.h>
#include <quorum/common/quorum_util.h>
#include <h/quorum.h>

#include "QuorumServer.h"
#include "ClusterWorker.h"
#include "scqs_utils.h"
#include <errno.h>

//
// Constructor.
//
cluster_worker::cluster_worker(const char *cluster, uint32_t id,
    const char *dir) :
	_SList::ListElem(this),
	cluster_id(id),
	file_descriptor(-1),
	quorum_info_layout(NULL),
	internal_error(0),
	state(W_SUSPENDED)
{
	(void) strncpy(cluster_name, cluster, 80);

	SCQS_DEBUG(("CW(%s)::cluster_worker\n", cluster_name));

	(void) sprintf(file, "%s/%s.0x%8.8X", dir, cluster, id);

	fd_pipe[0] = -1;
	fd_pipe[1] = -1;

	for (nodeid_t nid = 1; nid < SCQSD_NODEID_MAX; nid++) {
		fd_table[nid] = 0;
	}

	(void) pthread_mutex_init(&lock, NULL);
	(void) pthread_rwlock_init(&rw_operation_lock, NULL);
}

//
// Destructor.
//
cluster_worker::~cluster_worker()
{
	SCQS_DEBUG(("cluster_worker::~cluster_worker()....\n"));
	quorum_info_layout = NULL;
	(void) pthread_mutex_destroy(&lock);
	(void) pthread_rwlock_destroy(&rw_operation_lock);
}

//
// initialize
//
// Initialize the cluster worker.
//
int
cluster_worker::initialize(uint_t the_port)
{
	// Check if the quorum file exists. If it does not, then
	// initialize it.
	SCQS_DEBUG(("CW::Initialize(%s)... \n", cluster_name));

	struct stat	stat_buf;
	bool first_time = false;
	port = the_port;

	if (pipe(fd_pipe)) {
		SCQS_DEBUG(("%s: Failed to create pipe: %s\n",
		    cluster_name, strerror(errno)));
		return (1);
	}
	int ret = stat(file, &stat_buf);
	if (ret == -1) {
		if (errno != ENOENT) {
			SCQS_DEBUG(("%s: Failed to stat quorum "
			    "data [%d] %s: %s\n",
			    cluster_name, errno, file, strerror(errno)));
			init_cleanup(first_time);
			return (1);
		} else {
			file_descriptor = open(file,
			    O_CREAT|O_RDWR|O_DSYNC|O_RSYNC,
			    S_IREAD|S_IWRITE);
			if (ftruncate(file_descriptor,
				sizeof (quorum_info_layout_t)) != 0) {
				SCQS_DEBUG(("%s: Failed to truncate file: "
				    "%s\n", cluster_name, strerror(errno)));
				init_cleanup(first_time);
				return (1);
			}
			first_time = true;
		}
	} else {
		// Open file containing quorum info and map it into memory;
		file_descriptor = open(file,
		    O_RDWR|O_DSYNC|O_RSYNC, S_IREAD|S_IWRITE);
	}
	if (file_descriptor < 0) {
		SCQS_DEBUG(("%s: Failed to open quorum data %s: %s\n",
		    cluster_name, file, strerror(errno)));
		init_cleanup(first_time);
		return (1);
	}
	quorum_info_layout = (cluster_worker::quorum_info_layout_t *)
		mmap((caddr_t)0, sizeof (quorum_info_layout_t),
		    PROT_READ|PROT_WRITE, MAP_SHARED,
		    file_descriptor, 0);

	if (quorum_info_layout == MAP_FAILED) {
		SCQS_DEBUG(("%s: Failed to map quorum data: %s\n",
		    cluster_name, strerror(errno)));
		init_cleanup(first_time);
		return (1);
	}

	if (first_time) {
		//
		// Initialise in-file static info (cookie and version).
		//
		bzero(quorum_info_layout, sizeof (quorum_info_layout_t));
		(void) strncpy(quorum_info_layout->magic, QINFO_FILE_MAGIC,
		    QINFO_FILE_MAGIC_SIZE);
		quorum_info_layout->version = QINFO_FILE_VERSION;
		//
		// Update Quorum Data
		//
		bzero(&quorum_info, sizeof (quorum_info));
		if (commit_quorum_info_change() == QD_QS_EIO) {
			init_cleanup(first_time);
			return (1);
		}
	} else {
		//
		// Check cookie and version.
		//
		if ((quorum_info_layout->magic == NULL) ||
		    (quorum_info_layout->magic[0] == '\0') ||
		    (strncmp(quorum_info_layout->magic, QINFO_FILE_MAGIC,
			    QINFO_FILE_MAGIC_SIZE)) ||
		    (quorum_info_layout->version != QINFO_FILE_VERSION)) {
			SCQS_DEBUG(("CW::Initialize(%s) wrong "
			    "magic/version for file %s\n",
			    cluster_name, file));
			init_cleanup(first_time);
			return (1);
		}
		//
		// Initialise in-memory info from disk committed data.
		//
		(void) memcpy(&quorum_info, &CURRENT_QINFO,
		    sizeof (quorum_info));
		if (commit_quorum_info_change() == QD_QS_EIO) {
			init_cleanup(first_time);
			return (1);
		}
	}

	pthread_t thread_id;
	if (pthread_create(&thread_id, NULL, cluster_worker::run,
		(void *)this) != 0) {
		SCQS_DEBUG(("Failed to create thread for cluster %s: %d\n",
		    cluster_name, errno));

		init_cleanup(first_time);
		return (1);
	}
	state = W_RUNNING;
	SCQS_DEBUG(("CW::Initialize(%s) done \n", cluster_name));

	if (first_time) {
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		syslog(LOG_NOTICE, "Created Quorum device "
		    "for cluster %s with id 0x%8.8X.", cluster_name,
		    cluster_id);
	}
	return (0);
}

void
cluster_worker::init_cleanup(bool first_time)
{
	if (fd_pipe[0] != -1) {
		(void) close(fd_pipe[0]);
	}
	if (fd_pipe[1] != -1) {
		(void) close(fd_pipe[1]);
	}
	if (quorum_info_layout) {
		(void) munmap((char *)quorum_info_layout,
		    sizeof (quorum_info_layout_t));
	}
	if (file_descriptor > 0) {
		(void) close(file_descriptor);
		if (first_time) {
			(void) unlink(file);
		}
	}
}

//
// run
//
// The independent thread for handling quorum for the cluster
// starts here.
//
void *
cluster_worker::run(void *arg)
{
	cluster_worker *self = (cluster_worker *)arg;
	self->process_messages();
	// no comeback
	ASSERT(0);
	return ((void *)NULL);
}

//
// cleanup_and_stop
//
// Stop cluster thread.
//
void
cluster_worker::cleanup_and_stop() {
	//
	// Inform Quorum Server that the Worker
	// can be removed from the Worker list.
	//
	quorum_server::the().worker_done(this);
	//
	// flush pending request
	//
	quorum_operation *qop;
	while ((qop = quorum_operation_list.reapfirst()) != NULL) {
		delete qop;
	}
	//
	// close pipe
	//
	(void) close(fd_pipe[0]);
	(void) close(fd_pipe[1]);
	//
	// close socket file descriptors
	//
	for (nodeid_t nid = 1; nid < SCQSD_NODEID_MAX; nid++) {
		if (fd_table[nid] != 0) {
			(void) close(fd_table[nid]);
		}
	}
	//
	// unmap and close data file descriptor
	//
	if (quorum_info_layout) {
		(void) munmap((char *)quorum_info_layout,
		    sizeof (quorum_info_layout_t));
	}
	if (file_descriptor > 0) {
		(void) close(file_descriptor);
	}
	//
	// Inform Quorum Server of remove completion.
	//
	quorum_server::the().remove_completed();
	//
	// bye
	//
	SCQS_DEBUG(("CW(%s): Worker Done !\n", cluster_name));
	delete this;
	pthread_exit((void *)NULL);
}

//
// process_quorum_operations
//
// Process pending operations and state changes.
//
void
cluster_worker::process_quorum_operations()
{
	quorum_operation *qop = NULL;

	while (true) {
		MUTEX_LOCK(&lock);
		qop = quorum_operation_list.reapfirst();

		//
		// We have been asked to stop
		//
		if (state == W_STOPPED) {
			//
			// stop
			//
			SCQS_DEBUG(("CW:: stopping worker for cluster %s\n",
			    cluster_name));
			MUTEX_UNLOCK(&lock);
			cleanup_and_stop();
		}

		//
		// We have been asked to remove
		//
		if (state == W_REMOVED) {
			SCQS_DEBUG(("CW:: processing remove request for "
			    "cluster %s\n", cluster_name));
			//
			// remove data file for that cluster
			//
			int ret = unlink(file);
			if (ret == -1) {
				SCQS_DEBUG(("%s: Failed to unlink quorum "
				    "data %s: %s\n",
				    cluster_name, file, strerror(errno)));
			}
			//
			// SCMSGS
			// @explanation
			// Need explanation of this message!
			// @user_action
			// Need a user action for this message.
			//
			syslog(LOG_NOTICE, "Removed Quorum device "
			    "for cluster %s with id 0x%8.8X.",
			    cluster_name, cluster_id);
			//
			// then stop
			//
			MUTEX_UNLOCK(&lock);
			cleanup_and_stop();
		}

		//
		// Nothing to do
		//
		if (!qop) {
			MUTEX_UNLOCK(&lock);
			return;
		}

		//
		// Handle Quorum Operation
		//
		MUTEX_UNLOCK(&lock);
		handle_quorum_op(qop); // qop deleted here
	}
}

//
// attach_to_queue_and_signal
//
// Attach a new socket/nodeid, add the corresponding quorum operation
// and signal worker.
//
void
cluster_worker::attach_to_queue_and_signal(struct quorum_operation *qp,
    int the_sock)
{
	MUTEX_LOCK(&lock);
	attach(the_sock, qp->msg.get_nid());
	quorum_operation_list.append(qp);
	signal();
	MUTEX_UNLOCK(&lock);
}

//
// stop
//
// Notifies worker to stop.
//
void
cluster_worker::stop()
{
	MUTEX_LOCK(&lock);
	state = W_STOPPED;
	signal();
	MUTEX_UNLOCK(&lock);
}

//
// remove
//
// Notifies worker that its quorum device must be removed.
//
void
cluster_worker::remove()
{
	MUTEX_LOCK(&lock);
	state = W_REMOVED;
	signal();
	MUTEX_UNLOCK(&lock);
}

//
// attach
//
// Register a new coket to node id mapping for that worker
// to listen on.
// Assume lock held.
//
void
cluster_worker::attach(int the_sock, nodeid_t nid)
{
	if (fd_table[nid] != 0) {
		//
		// Found a stale connection.
		// Add it to the list.
		//
		Connection* sc = new Connection(nid, fd_table[nid]);
		if (sc != NULL) {
			SCQS_DEBUG(("CW(%s): attach adds stale"
			    "sock %d for client %d to list...\n",
			    cluster_name, fd_table[nid], nid));
			stale_connection_list.append(sc);
		} else {
			//
			// Just close it.
			//
			SCQS_DEBUG(("CW(%s): attach closes stale"
			    "sock %d for client %d...\n",
			    cluster_name, fd_table[nid], nid));
			(void) close(fd_table[nid]);
			cleanup_queue(nid, fd_table[nid]);
		}
	}
	fd_table[nid] = the_sock;
}

//
// detach
//
// Unregister a socket to node id mapping.
// Assume lock held.
//
void
cluster_worker::detach(nodeid_t nid)
{
	//
	// Socket entries are detached
	// on error (poll/receive).
	//
	SCQS_DEBUG(("CW(%s): detach closes "
	    "sock %d for client %d...\n",
	    cluster_name, fd_table[nid], nid));
	(void) close(fd_table[nid]);
	fd_table[nid] = 0;
}

//
// nodeid_from_fd
//
// Takes a file descriptor and returns corresponding
// nodeid (0 if not found).
// Assume lock held.
//
nodeid_t
cluster_worker::nodeid_from_fd(int fd) {
	if (fd == 0) {
		return (0);
	}
	for (nodeid_t nid = 1; nid < SCQSD_NODEID_MAX; nid++) {
		if (fd_table[nid] == fd) {
			return (nid);
		}
	}
	return (0);
}

//
// signal
//
// This function awakes the worker thread that might be
// waiting in poll() for new requests to come in.
// Assume lock held.
//
void
cluster_worker::signal() {
	char value = '!';
	if (write(fd_pipe[1], &value, sizeof (char)) !=
	    (ssize_t)sizeof (char)) {
		SCQS_DEBUG(("CW: signal - write on "
		    "fd_pipe failed\n"));
		ASSERT(0);
	}
}

//
// process_messages
//
// Worker thread main function.
//
void
cluster_worker::process_messages() {

	while (true) {
		MUTEX_LOCK(&lock);
		SCQS_DEBUG(("CW(%s): Waiting for incoming messages...\n",
		    cluster_name));

		//
		// Prepare poll() arguments.
		//
		int selected;
		nfds_t file_descriptors_number;
		//
		// entry 0 for quorum server to worker communication pipe
		//
		file_descriptors[0].fd = fd_pipe[0];
		//
		// Interested in read without blocking notifications:
		//
		// POLLIN
		//
		file_descriptors[0].events = POLLIN;
		file_descriptors[0].revents = 0;
		file_descriptors_number = 1;
		//
		// populate with attached nodeid sockets
		//
		for (nodeid_t nid = 1; nid < SCQSD_NODEID_MAX; nid++) {
			if (fd_table[nid] != 0) {
				SCQS_DEBUG(("\tnodeid[%d] on socket[%d]\n",
				    nid, fd_table[nid]));
				file_descriptors[file_descriptors_number].fd =
					fd_table[nid];
				file_descriptors[file_descriptors_number].
					events = POLLIN;
				file_descriptors[file_descriptors_number].
					revents = 0;
				file_descriptors_number++;
			}
		}

		//
		// Enter poll().
		//
		// Release lock to allow quorum server main thread to
		// call attachWithQopAndSignal().
		//
		MUTEX_UNLOCK(&lock);
		do {
			selected = poll(file_descriptors,
			    file_descriptors_number, -1);
		} while (selected < 0 && errno == EAGAIN);

		MUTEX_LOCK(&lock);
		if (selected == -1) {
			SCQS_DEBUG(("CW(%s): poll failed - %s\n",
			    cluster_name, strerror(errno)));
			//
			// Handle error
			//
			if ((errno == EAGAIN) || (errno == EINTR)) {
				break;
			} else {
				// Unexpected failure
				ASSERT(0);
				break;
			}
		}

		//
		// fd_table[] maintains NODEIDs to SOCKET File Descriptors
		// (FDs for short) mapping per cluster worker.
		//
		// FDs are opened at accept time by quorum server thread and
		// mapped to NODEID by attach(FD, NODEID).
		//
		//
		// == Faulty FD management:
		//
		// Faulty FDs are closed by detach(NODEID)
		// - if poll() reports an error.
		// - if receive() fails.
		//
		// == Stale FD management:
		//
		// Stale FDs are detected by attach(FD, NODEID)
		// - if a previously connected node reboots and issues
		// a new connect.
		// - if a previously connected node issues a reconnect.
		// and no error condition yet detected on previous fd
		// for that node.
		//
		// Stale FDs are closed by worker thread once it gets a chance.
		// As attach() is called by server main thread and poll() is
		// called by worker thread, only one of them handle close()
		// to prevent race condition where the same FD would be
		// closed twice (while possibly re-assigned in between).
		//
		// == Miscellaneous borderline FD management conditions:
		//
		//  FDs closed when not mapped to a nodeid
		//  (Orphan FD case (not expected to happen)).
		//
		//  FDs closed by attach() is failed to add
		//  to Stale list.
		//

		//
		// Handle Stale connections.
		//
		Connection* stale_connection;
		while ((stale_connection = stale_connection_list.reapfirst())
		    != NULL) {
			SCQS_DEBUG(("CW(%s): closing stale sock %d...\n",
			    cluster_name,
			    stale_connection->file_descriptor));
			(void) close(stale_connection->file_descriptor);
			cleanup_queue(stale_connection->node_id,
			    stale_connection->file_descriptor);
			delete stale_connection;
		}

		//
		// Return flags we are interested in:
		//
		// POLLIN    - something to read
		//
		// POLLERR   - an error occurred
		// POLLHUP   - a hangup occurred
		// POLLNVAL  - invalid file descriptor
		//
		// Check if we have been awakened by quorum server main thread.
		//
		if (file_descriptors[0].revents & (POLLIN)) {
			//
			// New cluster node connection or stop request.
			//
			// Flush pipe.
			//
			char content;
			(void) read(fd_pipe[0], &content, sizeof (char));
			//
			// Process pending operations.
			//
			MUTEX_UNLOCK(&lock);
			process_quorum_operations();
			//
			// Re-enter poll with new socket setup.
			//
			continue;
		}

		//
		// Handle read an error for client sockets.
		//
		for (unsigned long i = 1; i < file_descriptors_number; i++) {
			//
			// Check file descriptors for errors.
			//
			nodeid_t nid;
			if (file_descriptors[i].revents & (POLLERR | POLLHUP |
			    POLLNVAL)) {
				if ((nid = nodeid_from_fd(
				    file_descriptors[i].fd)) != 0) {
					SCQS_DEBUG(("CW(%s): lost connection "
					    "on sock %d for client %d...\n",
					    cluster_name,
					    file_descriptors[i].fd, nid));
					detach(nid);
					cleanup_queue(nid,
					    file_descriptors[i].fd);
				} else {
					// socket not mapped to any node.
					SCQS_DEBUG(("CW(%s): closes orphan "
					    "sock %d...\n", cluster_name,
						file_descriptors[i].fd));
					(void) close(file_descriptors[i].fd);
				}
				continue;
			}
			//
			// Check file descriptors for reads.
			//
			// Getting around 6319145 sockfs...
			//
			// client socket close() can leads poll() to report
			// a POLLIN event but recvmsg() fails (nothing to
			// read), although this is a clean disconnection
			// case...
			//
			if (file_descriptors[i].revents & (POLLIN)) {
				if ((nid = nodeid_from_fd(
					    file_descriptors[i].fd)) != 0) {
					SCQS_DEBUG(("CW(%s): Receive on "
					    "sock %d, client %d...\n",
					    cluster_name,
					    file_descriptors[i].fd, nid));
					quorum_operation* qop =
						new quorum_operation;
					if (quorum_server_socket::receive
					    (qop, file_descriptors[i].fd) !=
					    0) {
						//
						// See comment above!
						//
						delete qop;
						SCQS_DEBUG(("CW(%s): lost "
						    "connection on sock %d "
						    "for client %d...\n",
						    cluster_name,
						    file_descriptors[i].fd,
						    nid));
						detach(nid);
						cleanup_queue(nid,
						    file_descriptors[i].fd);
					} else {
						quorum_operation_list.append(
						    qop);
					}
				} else {
					SCQS_DEBUG(("CW(%s): closes orphan "
						"sock %d...\n", cluster_name,
						file_descriptors[i].fd));
					(void) close(file_descriptors[i].fd);
				}
			}
		}
		//
		// Process pending Quorum Operations.
		//
		MUTEX_UNLOCK(&lock);
		process_quorum_operations();
	}
}

//
// get_quorum_status
//
// Return quorum status.
//
void
cluster_worker::get_quorum_status(quorum_status *qsp)
{
	RWLOCK_RDLOCK(&rw_operation_lock);
	(void) strncpy(qsp->cluster_name, cluster_name, 80);
	qsp->cluster_id = (int)cluster_id;
	qsp->reserved_nid = quorum_info.owner;
	qsp->reservation_key = quorum_info.reserved[quorum_info.owner];
	for (uint_t i = 0, j = 0; i < SCQSD_NODEID_MAX; i++) {
		if (!null_idl_key(&quorum_info.registered[i])) {
			qsp->registered[j] = i;
			qsp->registration_key[j++] = quorum_info.registered[i];
		}
	}
	RWLOCK_UNLOCK(&rw_operation_lock);
}

//
// cleanup_queue
//
// Cleanup pending operation(s) for given node id and file descriptor.
//
void
cluster_worker::cleanup_queue(nodeid_t id, int fd)
{
	quorum_operation *qop;
	quorum_operation_list.atfirst();
	while ((qop = quorum_operation_list.get_current()) != NULL) {
		quorum_operation_list.advance(); // advance before erase()
		if ((qop->msg.get_nid() == id) &&
		    (qop->file_descriptor == fd)) {
			(void) quorum_operation_list.erase(qop);
		}
	}
}

//
// handle_quorum_op
//
void
cluster_worker::handle_quorum_op(struct quorum_operation *qop)
{
	// This is an error returned to the cluster.
	dq_qs_error	opErr = QD_QS_SUCCESS;

	internal_error = 0;

	// Currently locking across all operations.
	RWLOCK_WRLOCK(&rw_operation_lock);

	switch (qop->msg.get_msg_type()) {
	case QD_OPEN:
		opErr = quorum_open(qop->msg);
		break;
	case QD_RESERVE:
		opErr = quorum_reserve(qop->msg);
		break;
	case QD_READ_RESERVATIONS:
		qop->set_free(0);
		opErr = quorum_read_reservations(qop->msg, &qop->data);
		break;
	case QD_PREEMPT:
		opErr = quorum_preempt(qop->msg, &qop->data);
		break;
	case QD_READ_KEYS:
		qop->set_free(0);
		opErr = quorum_read_keys(qop->msg, &qop->data);
		break;
	case QD_ENABLE_FF:
		break;
	case QD_REGISTER:
		opErr = quorum_register(qop->msg);
		break;
	case QD_CLOSE:
		opErr = quorum_close(qop);
		// we already replied and are done
		RWLOCK_UNLOCK(&rw_operation_lock);
		return;
	case QD_RESET:
	case QD_DATA_READ:
	case QD_DATA_WRITE:
	case QD_REMOVE:
		break;
	default:
		SCQS_DEBUG(("Unknown quorum op received: %s\n",
		    qop->msg.type_to_str()));
		opErr = QD_QS_EIO;
		break;
	}
	RWLOCK_UNLOCK(&rw_operation_lock);
	//
	// Different types of error might happen:
	//
	// . Normal operation handling returns an error
	//    (e.g., quorumOpen returns QD_QS_EACCES)
	//
	// . Internal error while handling operation
	//    (not enough memory to process call or failure to commit)
	//
	//    In this case we return QD_QS_EIO to the quorum algo.
	//    When quorum algorithm encounters error it will mark the
	//    quorum device offline.
	//
	if (internal_error != 0) {
		qop->msg.set_error(QD_QS_EIO);
	} else {
		qop->msg.set_error(opErr);
	}
	//
	// . Failure to send the operation result
	//    (e.g., connection closed)
	//
	if ((quorum_server_socket::send_to(qop, fd_table[qop->msg.get_nid()])
		!= 0) ||
	    (internal_error != 0)) {
		MUTEX_LOCK(&lock);
		cleanup_queue(qop->msg.get_nid(), fd_table[qop->msg.get_nid()]);
		detach(qop->msg.get_nid());
		MUTEX_UNLOCK(&lock);
	}
	delete qop;
}

//
// quorum_close
//
dq_qs_error
cluster_worker::quorum_close(struct quorum_operation *qop)
{
	SCQS_DEBUG(("CW::quorum_Close(%s) called for nid=%d.\n",
	    cluster_name, qop->msg.get_nid()));
	//
	// Reply
	//
	qop->msg.set_error(QD_QS_SUCCESS);
	(void) quorum_server_socket::send_to(qop, fd_table[qop->msg.get_nid()]);
	//
	// Detach file descriptor
	//
	MUTEX_LOCK(&lock);
	detach(qop->msg.get_nid());
	MUTEX_UNLOCK(&lock);
	//
	// Cleanup and Return
	//
	delete qop;
	return (QD_QS_SUCCESS);
}

//
// quorum_open
//
dq_qs_error
cluster_worker::quorum_open(qd_qs_msghdr &msg)
{
	SCQS_DEBUG(("CW::quorum_open(%s) nid=%d\n",
	    cluster_name, msg.get_nid()));

	msg.set_port(port);

	//
	// If the node has asked to scrub any old keys,
	// then do the scrubbing.
	//
	if (msg.op_data.scrub) {
		bzero(&quorum_info, sizeof (quorum_info));
	}

	ASSERT(msg.get_length() == 0);
	return (commit_quorum_info_change());
}

//
// quorum_register
//
dq_qs_error
cluster_worker::quorum_register(qd_qs_msghdr &msg)
{
	SCQS_DEBUG(("CW::quorum_register(%s) nid=%d\n",
	    cluster_name, msg.get_nid()));

	if (null_idl_key(&quorum_info.registered[msg.get_nid()])) {
		quorum_info.registered[msg.get_nid()] = msg.op_data.key;
		quorum_info.registration_number++;

		SCQS_DEBUG(("\tquorumRegister(%s) quorum_info"
		    "->registered[%d] "
		    "= 0x%llx\n", cluster_name, msg.get_nid(),
		    key_value(quorum_info.registered[msg.get_nid()])));
	} else {
		SCQS_DEBUG(("\t Key exists\n"));
	}
	print_quorum_info();
	ASSERT(msg.get_length() == 0);

	return (commit_quorum_info_change());
}

//
// quorum_reserve
//
dq_qs_error
cluster_worker::quorum_reserve(qd_qs_msghdr &msg)
{
	SCQS_DEBUG(("CW::quorum_reserve(%s) nid=%d\n",
	    cluster_name, msg.get_nid()));

	if (null_idl_key(&quorum_info.registered[msg.get_nid()])) {
		return (QD_QS_EACCES);
	}
	//
	// Check if some other node has this device already reserved.
	//
	if (quorum_info.reservation_number != 0) {
		if (null_idl_key(&msg.op_data.key)) {
			ASSERT(msg.get_msg_type() == QD_PREEMPT);
			return (QD_QS_SUCCESS);
		}
		if (match_idl_keys(&quorum_info.reserved[msg.get_nid()],
		    &msg.op_data.key)) {
			//
			// Node calling preempt has the reservation.
			//
			return (QD_QS_SUCCESS);
		}
		return (QD_QS_EACCES);
	}

	quorum_info.reserved[msg.get_nid()] =
		quorum_info.registered[msg.get_nid()];
	quorum_info.reservation_number++;
	quorum_info.owner = msg.get_nid();

	print_quorum_info();
	ASSERT(msg.get_length() == 0);

	return (commit_quorum_info_change());
}

//
// quorum_read_reservations
//
dq_qs_error
cluster_worker::quorum_read_reservations(qd_qs_msghdr &msg, void **data)
{
	SCQS_DEBUG(("\tCW::quorum_read_reservations(%s) nid=%d\n",
	    cluster_name, msg.get_nid()));

	if (null_idl_key(&quorum_info.registered[msg.get_nid()])) {
		return (QD_QS_EACCES);
	}

	*data = read_keys_internal(msg, quorum_info.reserved);

	if (*data == NULL) {
		return (QD_QS_EIO);
	}
	return (QD_QS_SUCCESS);
}

//
// quorum_read_keys
//
dq_qs_error
cluster_worker::quorum_read_keys(qd_qs_msghdr &msg, void **data)
{
	SCQS_DEBUG(("\tCW::quorum_read_keys(%s) nid=%d\n",
	    cluster_name, msg.get_nid()));

	//
	// 'length' member of the msghdr is used to determine the
	// number of entries requested.
	//

	SCQS_DEBUG(("\tquorum_read_keys(%s) registration_number=%d, "
	    "reservation_number=%d, req=%d\n",
	    cluster_name, quorum_info.registration_number,
	    quorum_info.reservation_number, msg.get_data_length()));

	*data = read_keys_internal(msg, quorum_info.registered);

	if (*data == NULL) {
		return (QD_QS_EIO);
	}
	return (QD_QS_SUCCESS);
}

//
// quorum_preempt
//
dq_qs_error
cluster_worker::quorum_preempt(qd_qs_msghdr &msg, void **)
{
	SCQS_DEBUG(("CW::quorum_preempt(%s) nid=%d\n",
	    cluster_name, msg.get_nid()));

	if (null_idl_key(&quorum_info.registered[msg.get_nid()])) {
		return (QD_QS_EACCES);
	}

	quorum::registration_key_t *key = &msg.op_data.victim_key;
	if (!null_idl_key(key)) {
		//
		// Get nodeid from the key
		//
		nodeid_t nid = nodeid_from_key(*key);
		ASSERT((nid > 0) && (nid < SCQSD_NODEID_MAX));

		SCQS_DEBUG(("\t\tPreempting node %d\n", nid));

		bzero((void *)&quorum_info.registered[nid],
		    sizeof (quorum::registration_key_t));

		quorum_info.registration_number--;

		bzero((void *)&quorum_info.reserved[nid],
		    sizeof (quorum::registration_key_t));
		quorum_info.reservation_number--;
		quorum_info.owner = 0;
	}
	msg.set_length(0);
	//
	// Put my key on the device
	// Commit will be performed within this function.
	//
	dq_qs_error ret = quorum_register(msg);
	if (ret != QD_QS_SUCCESS) {
		SCQS_DEBUG(("\t\t++ preempt: register returned %d\n",
		    ret));
		return (ret);
	}
	ASSERT(msg.get_length() == 0);
	ret = quorum_reserve(msg);
	SCQS_DEBUG(("\t\t++ preempt: reserve returned %d\n", ret));
	return (ret);
}

//
// nodeid_from_key
//
nodeid_t
cluster_worker::nodeid_from_key(quorum::registration_key_t key)
{
	uint64_t keyv = key_value(key);
	uint32_t	*nid = (uint32_t *)&keyv;

#if defined(_LITTLE_ENDIAN)
	return (nid[0]);
#elif defined(_BIG_ENDIAN)
	return (nid[1]);
#else
#error "No endianness defined"
#endif
}

//
// print_quorum_info
//
void
cluster_worker::print_quorum_info()
{
	int i;

	for (i = 0; i < SCQSD_NODEID_MAX; i++) {
		if (!null_idl_key(&quorum_info.registered[i])) {
			SCQS_DEBUG(("\t\tregistered[%d]=0x%llx\n", i,
			    key_value(quorum_info.registered[i])));
		}
	}
	for (i = 0; i < SCQSD_NODEID_MAX; i++) {
		if (!null_idl_key(&quorum_info.reserved[i])) {
			SCQS_DEBUG(("\t\treserved[%d]=0x%llx\n", i,
			    key_value(quorum_info.reserved[i])));
		}
	}
	SCQS_DEBUG(("\t\tregistration_number=%d reservation_number=%d, "
	    "owner=%d\n", quorum_info.registration_number,
	    quorum_info.reservation_number, quorum_info.owner));
}

//
// read_keys_internal
//
quorum::registration_key_t *
cluster_worker::read_keys_internal(qd_qs_msghdr &msg,
    quorum::registration_key_t *from_list)
{
	size_t memsize = msg.get_data_length() *
		sizeof (quorum::registration_key_t);

	// Copy the requested number of keys
	uint_t copyNum;
	uint_t numSrc;

	if (msg.get_msg_type() == QD_READ_KEYS) {
		numSrc = quorum_info.registration_number;
	} else {
		numSrc = quorum_info.reservation_number;
	}
	copyNum = ((numSrc - msg.get_data_length()) > 0) ?
	    msg.get_data_length() : numSrc;

	uint_t j = 0;

	for (int i = 0; ((i < SCQSD_NODEID_MAX) && (j < copyNum)); i++) {
		if (!null_idl_key(&from_list[i])) {
			keys_buffer[j++] = from_list[i];
		}
	}

	// Send information about the actual number of keys.
	msg.set_length(memsize);

	// We must have copied at most j keys
	msg.set_data_length(j);
	SCQS_DEBUG(("\t%s: readKeysInternal: len=%d, data_len = %d\n",
	    cluster_name, memsize, j));

	return (keys_buffer);
}

//
// commit_quorum_info_change
//
dq_qs_error
cluster_worker::commit_quorum_info_change()
{
	//
	// Right info on alternative disk slot.
	//
	(void) memcpy(&ALTERNATIVE_QINFO, &quorum_info, //lint !e514
	    sizeof (quorum_info));
	if (fsync(file_descriptor)) {
		//
		// CURRENT_QINFO is untouched and still points
		// to most-recently committed data on disk.
		//
		SCQS_DEBUG(("%s: Failed to commit data write\n",
		    cluster_name));
		internal_error = 1;
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		syslog(LOG_ERR, "Failed to commit change in quorum "
		    "data for cluster %s.", cluster_name);
		return (QD_QS_EIO);
	}
	//
	// Set current index to alternative slot.
	//
	quorum_info_layout->current_index = !quorum_info_layout->current_index;
	//
	// Commit index change.
	//
	if (fsync(file_descriptor)) {
		//
		// CURRENT_QINFO points to consistent on-disk data,
		// either previously or newly committed.
		//
		SCQS_DEBUG(("%s: Failed to commit data write\n",
		    cluster_name));
		internal_error = 1;
		syslog(LOG_ERR, "Failed to commit change in quorum "
		    "data for cluster %s.", cluster_name);
		return (QD_QS_EIO);
	}
	//
	// CURRENT_QINFO now points to the newly
	// committed data on disk.
	//
	return (QD_QS_SUCCESS);
}
