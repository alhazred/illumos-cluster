//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef	HTTP_SERVER_H
#define	HTTP_SERVER_H

#pragma ident	"@(#)HttpServer.h	1.8	08/05/20 SMI"

#include <pthread.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/socket.h>
#include <errno.h>
#include <strings.h>
#include <netdb.h>

class http_server {
private:

	//
	// Client and server socket, and server port.
	//
	int server_socket;
	int client_socket;
	uint16_t port_number;

	//
	// Internal helpers.
	//
	int socket_start();
	void accept_client();
	void read_line(char *buffer, int size);
	int format_request(char *buffer);
	int deal_with_client();

protected:

	//
	// Implementation of request processing.
	//
	virtual int process_request(char *href, char **answer) = 0;

	//
	// Free resource after request processed.
	//
	virtual void free_resources(char *answer) = 0;

public:
	http_server(uint16_t port_number);
	http_server();
	virtual ~http_server();

	//
	// Start server.
	//
	virtual int start();

	//
	// For internal use ONLY.
	//
	void start_handler();
};

#endif /* HTTP_SERVER_H */
