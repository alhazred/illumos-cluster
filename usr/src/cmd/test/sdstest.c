/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)sdstest.c	1.6	08/05/20 SMI"

/*
 * 'sdstest' is a test program to execute interfaces in libscconf, libscstat,
 * and 'libscswitch' that SDS will be using.
 *
 * Suppress the following lint warning:
 * ANSI limit of 511 'external identifiers' exceeded -- processing is unaffected
 */
/*lint -e793 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/sysmacros.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <arpa/inet.h>
#include <dlfcn.h>

#include <scadmin/scconf.h>
#include <scadmin/scstat.h>
#include <scadmin/scswitch.h>

/* Library names */
#define	LIBSCSWITCH "/usr/cluster/lib/libscswitch.so"
#define	LIBSCSTAT "/usr/cluster/lib/libscstat.so"
#define	LIBSCCONF "/usr/cluster/lib/libscconf.so"

/* Function names */
#define	SCSWITCH_SWITCH_SERVICE "scswitch_switch_service"
#define	SCSWITCH_SHUTDOWN_SERVICE "scswitch_take_service_offline"
#define	SCSTAT_GET_DS_STATUS "scstat_get_ds_status"
#define	SCSTAT_GET_NODES "scstat_get_nodes"
#define	SCSTAT_FREE_NODES "scstat_get_nodes"
#define	SCCONF_OPENHANDLE "scconf_cltr_openhandle"
#define	SCCONF_CLOSEHANDLE "scconf_cltr_releasehandle"
#define	SCCONF_GET_NODENAME "scconf_get_nodename"
#define	SCCONF_GET_NODEID "scconf_get_nodeid"
#define	SCCONF_GET_NODEIPADDR "scconf_get_node_privateipaddr"
#define	SCCONF_GET_DIDCONFIG "scconf_get_did_config"
#define	SCCONF_GET_DS_BY_DEVT "scconf_get_ds_by_devt"
#define	SCCONF_CREATE_DS "scconf_add_ds"
#define	SCCONF_CHANGE_DS "scconf_change_ds"
#define	SCCONF_REMOVE_DS "scconf_rm_ds"
#define	SCCONF_GET_DS_CONFIG "scconf_get_ds_config"
#define	SCCONF_STRERR "scconf_strerr"

/* Type of action */
#define	STARTUP 1
#define	SHUTDOWN 2
#define	STATUS 3
#define	MEMBERSHIP 4
#define	INFO 5
#define	CREATE 6
#define	CONFIG 7
#define	REMOVE 8

#define	CREATE_DS 1
#define	CONFIG_DS 2
#define	REMOVE_DS 3

#define	MAXNODES 64

/* Forward declarations */
static void usage(const char *progname);
static void get_status(char *sname);
static void do_startup(char *sname, char *node);
static void do_shutdown(char *sname);
static void get_cluster_membership(void);
static void get_info(nodeid_t id, char *nodename, char *didname, dev_t dev,
    char *dsname, char *dsclassname);
static void process_ds(int type, char *dstype, char *dsname, char *dsnodes[],
    scconf_state_t dspreference, scconf_gdev_range_t *dsdevices,
    scconf_state_t dsfailback, scconf_cfg_prop_t *dspropertylist);
static char *get_state_string(scstat_state_code_t state);
static const char *get_node_state_string(scstat_node_pref_t state);

/*
 * Print out a usage message and exit
 */
void
usage(const char *progname)
{
	(void) printf("%s -c startup\n", progname);
	(void) printf("	-s service_name\n");
	(void) printf("	[-i hostname]\n");
	(void) printf("%s -c shutdown\n", progname);
	(void) printf("	-s service_name\n");
	(void) printf("%s -c membership\n", progname);
	(void) printf("%s -c status\n", progname);
	(void) printf("	[-s service_name]\n");
	(void) printf("%s -c info\n", progname);
	(void) printf("	-n nodename | -i nodeid | -p devicepath | "
	    "-d didname | -s servicename | -S serviceclassname\n");
	(void) printf("%s -c create\n", progname);
	(void) printf("	[-s service_name]\n");
	(void) printf("	[-a enabled|disabled] (node preference)\n");
	(void) printf("	[-n nodename|nodeid]\n");
	(void) printf("	[-f enabled|disabled] (failback)\n");
	(void) printf("	[-N propname -V propvalue\n");
	(void) printf("%s -c config\n", progname);
	(void) printf("	[-s service_name]\n");
	(void) printf("	[-a enabled|disabled] (node preference)\n");
	(void) printf("	[-n nodename|nodeid]\n");
	(void) printf("	[-f enabled|disabled] (failback)\n");
	(void) printf("	[-N propname -V propvalue\n");
	(void) printf("%s -c remove\n", progname);
	(void) printf("	[-s service_name]\n");
	(void) printf("	[-n nodename|nodeid]\n");
	(void) printf("	[-N propname -V propvalue\n");

	exit(1);
}

int
main(int argc, char *argv[])
{
	int c;
	char *sname = NULL;
	char *sclassname = NULL;
	int command = -1;
	int curr_node_num = -1;
	int errno;
	char *node = NULL;
	nodeid_t id = 0;
	char *didname = NULL;
	struct stat file_stat;
	dev_t curr_dev = (dev_t)0;
	char *dsnodes[MAXNODES];
	char *dstype = "sds";
	scconf_state_t dspreference = SCCONF_STATE_UNCHANGED;
	scconf_gdev_range_t *dsdevices = NULL;
	scconf_state_t dsfailback = SCCONF_STATE_UNCHANGED;
	scconf_cfg_prop_t *props = NULL, *curr = NULL, *last = NULL;

	dsnodes[0] = 0;

	/* Parse arguments */
	while ((c = getopt(argc, argv, "?N:V:a:f:p:d:n:c:i:s:S:")) != EOF) {
		switch (c) {
		case 's':
			sname = optarg;
			break;
		case 'S':
			sclassname = optarg;
			break;
		case 'd':
			didname = optarg;
			break;
		case 'N':
			curr = (scconf_cfg_prop_t *)malloc(
			    sizeof (scconf_cfg_prop_t));
			if (curr == NULL) {
				(void) printf("No memory!\n");
				exit(1);
			}
			if (props == NULL) {
				props = curr;
			} else {
				last->scconf_prop_next = curr;
			}
			last = curr;
			curr->scconf_prop_key = optarg;
			curr->scconf_prop_value = "";
			break;
		case 'V':
			if (curr == NULL) {
				usage(argv[0]);
			}
			curr->scconf_prop_value = optarg;
			break;
		case 'a':
			if (strncmp(optarg, "enabled", 1) == 0) {
				dspreference = SCCONF_STATE_ENABLED;
			} else if (strncmp(optarg, "disabled", 1) == 0) {
				dspreference = SCCONF_STATE_DISABLED;
			} else {
				usage(argv[0]);
			}
			break;
		case 'f':
			if (strncmp(optarg, "enabled", 1) == 0) {
				dsfailback = SCCONF_STATE_ENABLED;
			} else if (strncmp(optarg, "disabled", 1) == 0) {
				dsfailback = SCCONF_STATE_DISABLED;
			} else {
				usage(argv[0]);
			}
			break;
		case 'c':
			if (strncmp(optarg, "startup", 4) == 0) {
				command = STARTUP;
			} else if (strncmp(optarg, "shutdown", 4) == 0) {
				command = SHUTDOWN;
			} else if (strncmp(optarg, "status", 4) == 0) {
				command = STATUS;
			} else if (strncmp(optarg, "info", 1) == 0) {
				command = INFO;
			} else if (strncmp(optarg, "membership", 1) == 0) {
				command = MEMBERSHIP;
			} else if (strncmp(optarg, "create", 2) == 0) {
				command = CREATE;
			} else if (strncmp(optarg, "config", 2) == 0) {
				command = CONFIG;
			} else if (strncmp(optarg, "remove", 1) == 0) {
				command = REMOVE;
			} else {
				usage(argv[0]);
			}
			break;
		case 'i':
			/* Node id */
			node = optarg;
			id = (nodeid_t) atoi(node);
			break;
		case 'n':
			/* Node name */
			node = optarg;
			curr_node_num++;
			dsnodes[curr_node_num] = node;
			dsnodes[curr_node_num+1] = 0;
			break;
		case 'p':
			/* Device path */
			errno = stat(optarg, &file_stat);
			if (errno != 0) {
				(void) printf("Could not stat (%s)\n", optarg);
				exit(1);
			}
			if (((file_stat.st_mode & S_IFMT) != S_IFCHR) &&
			    ((file_stat.st_mode & S_IFMT) != S_IFBLK)) {
				(void) printf("(%s) is not a device special "
				    "file\n", optarg);
				exit(1);
			}
			curr_dev = file_stat.st_rdev;
			break;
		case '?':
		default:
			usage(argv[0]);
		}
	}

	/* Validate arguments */
	if (command == -1) {
		usage(argv[0]);
	}

	/* Do the requested action */
	if (command == STARTUP) {
		do_startup(sname, node);
	} else if (command == SHUTDOWN) {
		do_shutdown(sname);
	} else if (command == STATUS) {
		get_status(sname);
	} else if (command == INFO) {
		if (id > (nodeid_t)0) {
			get_info(id, NULL, NULL, 0, NULL, NULL);
		} else if (node != NULL) {
			get_info(0, node, NULL, 0, NULL, NULL);
		} else if (didname != NULL) {
			get_info(0, NULL, didname, 0, NULL, NULL);
		} else if (curr_dev != 0) {
			get_info(0, NULL, NULL, curr_dev, NULL, NULL);
		} else if (sname != NULL) {
			get_info(0, NULL, NULL, 0, sname, NULL);
		} else if (sclassname != NULL) {
			get_info(0, NULL, NULL, 0, NULL, sclassname);
		}
	} else if (command == MEMBERSHIP) {
		get_cluster_membership();
	} else if (command == CREATE) {
		process_ds(CREATE_DS, dstype, sname, dsnodes, dspreference,
		    dsdevices, dsfailback, props);
	} else if (command == CONFIG) {
		process_ds(CONFIG_DS, dstype, sname, dsnodes, dspreference,
		    dsdevices, dsfailback, props);
	} else if (command == REMOVE) {
		process_ds(REMOVE_DS, dstype, sname, dsnodes, dspreference,
		    dsdevices, dsfailback, props);
	}

	return (0);
}

/*
 * Helper function to add, remove and change the configuration of device
 * services via libscconf library interfaces.
 */
void
process_ds(int type, char *dstype, char *dsname, char *dsnodes[],
    scconf_state_t dspreference, scconf_gdev_range_t *dsdevices,
    scconf_state_t dsfailback, scconf_cfg_prop_t *dspropertylist)
{
	scconf_errno_t (*fn_open_handle)(scconf_cltr_handle_t *);
	scconf_errno_t (*fn_close_handle)(scconf_cltr_handle_t);
	scconf_errno_t (*fn_add_ds)(char *, char *,
	    char **, scconf_state_t,
	    scconf_gdev_range_t *, scconf_state_t,
	    scconf_cfg_prop_t *, char *);
	scconf_errno_t (*fn_change_ds)(char *,
	    char **, scconf_state_t,
	    scconf_gdev_range_t *, scconf_state_t,
	    scconf_cfg_prop_t *, char *);
	scconf_errno_t (*fn_rm_ds)(char *dsname, char *dsnodes[],
	    scconf_gdev_range_t *dsdevices, scconf_cfg_prop_t *dspropertylist,
	    char *dsoptions);
	void (*fn_strerr)(char *, scconf_errno_t);
	void *dlhandle;
	scconf_errno_t err;
	char errbuf[SCCONF_MAXSTRINGLEN];

	/* Resolve all the necessary symbols */
	if ((dlhandle = dlopen(LIBSCCONF, RTLD_LAZY)) == NULL) {
		(void) printf("Error in dlopen: %s\n", dlerror());
		exit(1);
	}
	/* lint has 'Suspicious cast' issues with dlsym function ptrs (e611) */
	/*lint -e611 */
	if ((fn_open_handle = (scconf_errno_t(*)())
		dlsym(dlhandle, SCCONF_OPENHANDLE)) == NULL) {
		(void) printf("Error in dlsym: %s\n", dlerror());
		exit(1);
	}

	if ((fn_close_handle = (scconf_errno_t(*)())
	    dlsym(dlhandle, SCCONF_CLOSEHANDLE)) == NULL) {
		(void) printf("Error in dlsym: %s\n", dlerror());
		exit(1);
	}

	if ((fn_add_ds = (scconf_errno_t(*)())
	    dlsym(dlhandle, SCCONF_CREATE_DS)) == NULL) {
		(void) printf("Error in dlsym: %s\n", dlerror());
		exit(1);
	}

	if ((fn_change_ds = (scconf_errno_t(*)())
	    dlsym(dlhandle, SCCONF_CHANGE_DS)) == NULL) {
		(void) printf("Error in dlsym: %s\n", dlerror());
		exit(1);
	}

	if ((fn_rm_ds = (scconf_errno_t(*)())
	    dlsym(dlhandle, SCCONF_REMOVE_DS)) == NULL) {
		(void) printf("Error in dlsym: %s\n", dlerror());
		exit(1);
	}

	if ((fn_strerr = (void(*)())
	    dlsym(dlhandle, SCCONF_STRERR)) == NULL) {
		(void) printf("Error in dlsym: %s\n", dlerror());
		exit(1);
	}
	/*lint +e611 */
	err = (*fn_open_handle)(NULL);
	if (err != SCCONF_NOERR) {
		(void) printf("Error opening scconf handle: %d\n", err);
		goto cleanup;
	}

	/* Invoke the appropriate function */
	if (type == CREATE_DS) {
		err = (*fn_add_ds)(dstype, dsname, dsnodes, dspreference,
		    dsdevices, dsfailback, dspropertylist, NULL);
	} else if (type == CONFIG_DS) {
		err = (*fn_change_ds)(dsname, dsnodes, dspreference,
		    dsdevices, dsfailback, dspropertylist, NULL);
	} else if (type == REMOVE_DS) {
		err = (*fn_rm_ds)(dsname, dsnodes, dsdevices, dspropertylist,
		    NULL);
	} else {
		(void) printf("Unexpected type!\n");
		exit(1);
	}

	/* If there was an error, print it out */
	if (err != SCCONF_NOERR) {
		(*fn_strerr)(errbuf, err);
		if (type == CREATE_DS) {
			(void) printf("Error calling %s : %d(%s)\n",
			    SCCONF_CREATE_DS, err, errbuf);
		} else if (type == CONFIG_DS) {
			(void) printf("Error calling %s : %d(%s)\n",
			    SCCONF_CHANGE_DS, err, errbuf);
		} else if (type == REMOVE_DS) {
			(void) printf("Error calling %s : %d(%s)\n",
			    SCCONF_REMOVE_DS, err, errbuf);
		}
		goto cleanup;
	}

	err = (*fn_close_handle)((scconf_cltr_handle_t)0);
	if (err != SCCONF_NOERR) {
		(void) printf("Error closing scconf handle: %d\n", err);
		goto cleanup;
	}

cleanup:
	(void) dlclose(dlhandle);
	exit(err);
}

static void
print_ds_info(scconf_cfg_ds_t *dsconfig)
{
	scconf_gdev_range_t *tmp_gdev;
	scconf_cfg_prop_t *tmp_prop;
	unsigned int i;

	(void) printf("DS name: %s\n", dsconfig->scconf_ds_name);
	(void) printf("DS type: %s\n", dsconfig->scconf_ds_type);
	(void) printf("DS label: %s\n", dsconfig->scconf_ds_label);
	(void) printf("DS failback: %s\n",
	    ((dsconfig->scconf_ds_failback == SCCONF_STATE_ENABLED)
	    ? "Enabled" : "Disabled"));
	(void) printf("DS nodelist: ");
	for (i = 0; (dsconfig->scconf_ds_nodelist)[i] != 0; i++) {
		(void) printf("%d ", (dsconfig->scconf_ds_nodelist)[i]);
	}
	(void) printf("\n");
	(void) printf("DS preference: %s\n",
	    ((dsconfig->scconf_ds_preference == SCCONF_STATE_ENABLED)
	    ? "Enabled" : "Disabled"));
	(void) printf("DS device list: ");
	tmp_gdev = dsconfig->scconf_ds_devlist;
	while (tmp_gdev) {
		(void) printf("(%lu, %lu-%lu) ", tmp_gdev->major_no,
		    tmp_gdev->start_minor, tmp_gdev->end_minor);
		tmp_gdev = tmp_gdev->scconf_gdev_next;
	}
	(void) printf("\n");
	(void) printf("Properties:\n");
	tmp_prop = dsconfig->scconf_ds_propertylist;
	while (tmp_prop) {
		(void) printf("\t%s - %s\n", tmp_prop->scconf_prop_key,
		    tmp_prop->scconf_prop_value);
		tmp_prop = tmp_prop->scconf_prop_next;
	}
}

/*
 * Helper function that gets configuration information for various components.
 */
void
get_info(nodeid_t id, char *in_nodename, char *didname, dev_t dev, char *sname,
    char *sclassname)
{
	scconf_errno_t (*fn_open_handle)(scconf_cltr_handle_t *);
	scconf_errno_t (*fn_close_handle)(scconf_cltr_handle_t);
	scconf_errno_t (*fn_get_nodename)(scconf_nodeid_t, char **);
	scconf_errno_t (*fn_get_nodeid)(char *, scconf_nodeid_t *);
	scconf_errno_t (*fn_get_nodeip)(scconf_nodeid_t, struct in_addr *);
	scconf_errno_t (*fn_get_didconf)(char *, scconf_cfg_did_t **);
	scconf_errno_t (*fn_get_ds)(major_t maj, minor_t min, char **dsname);
	scconf_errno_t (*fn_get_ds_config)(char *name, const uint_t options,
	    scconf_cfg_ds_t **dsconfigp);
	void (*fn_strerr)(char *, scconf_errno_t);
	void *dlhandle;
	scconf_errno_t err;
	char *nodename, *dsname;
	char errbuf[SCCONF_MAXSTRINGLEN];
	nodeid_t nid;
	scconf_cfg_did_t *didconfig;
	scconf_cfg_ds_t *dsconfig;

	dsname = sname;

	/* Resolve the required symbols */
	if ((dlhandle = dlopen(LIBSCCONF, RTLD_LAZY)) == NULL) {
		(void) printf("Error in dlopen: %s\n", dlerror());
		exit(1);
	}
	/* lint has 'Suspicious cast' issues with dlsym function ptrs (e611) */
	/*lint -e611 */
	if ((fn_open_handle = (scconf_errno_t(*)())
	    dlsym(dlhandle, SCCONF_OPENHANDLE)) == NULL) {
		(void) printf("Error in dlsym: %s\n", dlerror());
		exit(1);
	}

	if ((fn_close_handle = (scconf_errno_t(*)())
	    dlsym(dlhandle, SCCONF_CLOSEHANDLE)) == NULL) {
		(void) printf("Error in dlsym: %s\n", dlerror());
		exit(1);
	}

	if ((fn_get_nodeid = (scconf_errno_t(*)())
	    dlsym(dlhandle, SCCONF_GET_NODEID)) == NULL) {
		(void) printf("Error in dlsym: %s\n", dlerror());
		exit(1);
	}

	if ((fn_get_nodename = (scconf_errno_t(*)())
	    dlsym(dlhandle, SCCONF_GET_NODENAME)) == NULL) {
		(void) printf("Error in dlsym: %s\n", dlerror());
		exit(1);
	}

	if ((fn_get_nodeip = (scconf_errno_t(*)())
	    dlsym(dlhandle, SCCONF_GET_NODEIPADDR)) == NULL) {
		(void) printf("dlerror: %s\n", dlerror());
		exit(1);
	}

	if ((fn_get_didconf = (scconf_errno_t(*)())
	    dlsym(dlhandle, SCCONF_GET_DIDCONFIG)) == NULL) {
		(void) printf("dlerror: %s\n", dlerror());
		exit(1);
	}

	if ((fn_get_ds = (scconf_errno_t(*)())
	    dlsym(dlhandle, SCCONF_GET_DS_BY_DEVT)) == NULL) {
		(void) printf("dlerror: %s\n", dlerror());
		exit(1);
	}

	if ((fn_get_ds_config = (scconf_errno_t(*)())
	    dlsym(dlhandle, SCCONF_GET_DS_CONFIG)) == NULL) {
		(void) printf("dlerror: %s\n", dlerror());
		exit(1);
	}

	if ((fn_strerr = (void(*)())
	    dlsym(dlhandle, SCCONF_STRERR)) == NULL) {
		(void) printf("Error in dlsym: %s\n", dlerror());
		exit(1);
	}
	/*lint +e611 */
	err = (*fn_open_handle)(NULL);
	if (err != SCCONF_NOERR) {
		(void) printf("Error opening scconf handle: %d\n", err);
		goto cleanup;
	}

	/*
	 * Perform the requested action, depending on the value of the
	 * parameters passed in to this function.
	 */
	if (id != 0) {
		err = (*fn_get_nodename)(id, &nodename);
		if (err != SCCONF_NOERR) {
			(*fn_strerr)(errbuf, err);
			(void) printf("Error getting nodename for nodeid %d: "
			    "%s\n", id, errbuf);
			goto cleanup;
		}
		(void) printf("Node name for id %d is %s\n", id, nodename);
	} else if (in_nodename != NULL) {
		err = (*fn_get_nodeid)(in_nodename, &nid);
		if (err != SCCONF_NOERR) {
			(*fn_strerr)(errbuf, err);
			(void) printf("Error getting nodeid for name %s: %s\n",
			    in_nodename, errbuf);
			goto cleanup;
		}
		(void) printf("Node id for %s is %d\n", in_nodename, nid);
		id = nid;
	} else if (didname != NULL) {
		scconf_cfg_devices_t *slist;
		err = (*fn_get_didconf)(didname, &didconfig);
		if (err != SCCONF_NOERR) {
			(*fn_strerr)(errbuf, err);
			(void) printf("Error getting didconfig for %s: %s\n",
			    didname, errbuf);
			goto cleanup;
		}
		(void) printf("Device name: %s\n", didconfig->scconf_didname);
		slist = didconfig->scconf_devlist;
		while (slist) {
			(void) printf("\t%d:%s\n", slist->nodeid,
			    slist->devname);
			slist = slist->scconf_device_next;
		}
	} else if (dev != 0) {
		err = (*fn_get_ds)(getemajor(dev), geteminor(dev), &dsname);
		if (err != SCCONF_NOERR) {
			(*fn_strerr)(errbuf, err);
			(void) printf("Error getting DS name for (%lu, %lu): "
			    "%s\n", getemajor(dev), geteminor(dev), errbuf);
			goto cleanup;
		}
		(void) printf("Device service name for device (%lu, %lu): %s\n",
		    getemajor(dev), geteminor(dev), dsname);
	}

	if (sclassname != NULL) {
		scconf_cfg_ds_t *tmp;

		err = (*fn_get_ds_config)(sclassname, SCCONF_BY_TYPE_FLAG,
		    &dsconfig);
		if (err != SCCONF_NOERR) {
			(*fn_strerr)(errbuf, err);
			(void) printf("Error getting DS info for class (%s): "
			    "%s\n", sclassname, errbuf);
			goto cleanup;
		}

		tmp = dsconfig;
		while (tmp) {
			print_ds_info(tmp);
			tmp = tmp->scconf_ds_next;
		}
	}

	/*
	 * If a device service names is passed in, print out it's configuration.
	 */
	if (dsname != NULL) {
		err = (*fn_get_ds_config)(dsname, SCCONF_BY_NAME_FLAG,
		    &dsconfig);
		if (err != SCCONF_NOERR) {
			(*fn_strerr)(errbuf, err);
			(void) printf("Error getting DS info for service (%s): "
			    "%s\n", dsname, errbuf);
			goto cleanup;
		}

		print_ds_info(dsconfig);
	}

	/* If a node is specified, print out its private IP address */
	if (id != 0) {
		struct in_addr addr;
		err = (*fn_get_nodeip)(id, &addr);
		if (err != SCCONF_NOERR) {
			(*fn_strerr)(errbuf, err);
			(void) printf("Error getting nodeip for id %d: "
			    "%d(%s)\n", id, err, errbuf);
			goto cleanup;
		}
		(void) printf("Private IP address for node %d is %s\n",
		    id, inet_ntoa(addr));
	}

	err = (*fn_close_handle)((scconf_cltr_handle_t)0);
	if (err != SCCONF_NOERR) {
		(void) printf("Error closing scconf handle: %d\n", err);
		goto cleanup;
	}

cleanup:
	(void) dlclose(dlhandle);
	exit(err);
}

/*
 * Convert from 'scstat_state_code_t' to a descriptive string.
 */
char *
get_state_string(scstat_state_code_t state) {
	switch (state) {
	case SCSTAT_ONLINE:
		return ("Online");
	case SCSTAT_OFFLINE:
		return ("Offline");
	case SCSTAT_FAULTED:
		return ("Faulted");
	case SCSTAT_DEGRADED:
		return ("Degraded");
	case SCSTAT_WAIT:
		return ("Wait");
	case SCSTAT_UNKNOWN:
		return ("Unknown");
	case SCSTAT_NOTMONITORED:
		return ("Not monitored");
	default:
		break;
	}
	return ("Unknown state code");
}

/*
 * Helper function that prints out cluster membership information.
 */
void
get_cluster_membership()
{
	void *dlhandle;
	scstat_errno_t (*fn_get_nodes)(scstat_node_t **);
	void (*fn_free_nodes)(scstat_node_t *);
	scstat_errno_t err;
	scstat_node_t *nodes_status, *tmp_status;

	/* Resolve the required symbols */
	if ((dlhandle = dlopen(LIBSCSTAT, RTLD_LAZY)) == NULL) {
		(void) printf("Error in dlopen: %s\n", dlerror());
		exit(1);
	}
	/* lint has 'Suspicious cast' issues with dlsym function ptrs (e611) */
	/*lint -e611 */
	if ((fn_get_nodes = (scstat_errno_t(*)())
	    dlsym(dlhandle, SCSTAT_GET_NODES)) == NULL) {
		(void) printf("Error in dlsym: %s\n", dlerror());
		exit(1);
	}

	if ((fn_free_nodes = (void(*)())
	    dlsym(dlhandle, SCSTAT_FREE_NODES)) == NULL) {
		(void) printf("Error in dlsym: %s\n", dlerror());
		exit(1);
	}
	/*lint +e611 */
	/* Print out node information for the cluster */
	err = (*fn_get_nodes)(&nodes_status);
	if (err != SCSTAT_ENOERR) {
		(void) printf("Error calling %s: %d\n", SCSTAT_GET_NODES, err);
		goto cleanup;
	}
	tmp_status = nodes_status;
	while (tmp_status) {
		(void) printf("Node name: %s\n",
		    (char *)tmp_status->scstat_node_name);
		(void) printf("Node status: %s\n", get_state_string(
		    tmp_status->scstat_node_status));
		(void) printf("Node status string: %s\n",
		    (char *)tmp_status->scstat_node_statstr);
		tmp_status = tmp_status->scstat_node_next;
	}

	(*fn_free_nodes)(nodes_status);

cleanup:
	(void) dlclose(dlhandle);
	exit(err);
}

/*
 * Convert from 'scstat_node_pref_t' to a descriptive string.
 */
const char *
get_node_state_string(scstat_node_pref_t state)
{
	switch (state) {
	case SCSTAT_PRIMARY:
		return ("Primary");
	case SCSTAT_SECONDARY:
		return ("Secondary");
	case SCSTAT_SPARE:
		return ("Spare");
	case SCSTAT_INACTIVE:
		return ("Inactive");
	case SCSTAT_TRANSITION:
		return ("Transition");
	case SCSTAT_INVALID:
		return ("Invalid");
	default:
		break;
	}
	return ("Unknown state code");
}

/*
 * Helper function to print out status information for a device service.
 */
void
get_status(char *sname)
{
	void *dlhandle;
	scstat_errno_t (*fn)(scstat_ds_name_t *, scstat_ds_t **);
	scstat_errno_t err;
	scstat_ds_t *status;
	scstat_ds_node_state_t *node_state;
	scstat_ds_t *tmp;

	if ((dlhandle = dlopen(LIBSCSTAT, RTLD_LAZY)) == NULL) {
		(void) printf("Error in dlopen: %s\n", dlerror());
		exit(1);
	}

	/* lint has 'Suspicious cast' issues with dlsym function ptrs (e611) */
	/*lint -e611 */
	if ((fn = (scstat_errno_t(*)())
	    dlsym(dlhandle, SCSTAT_GET_DS_STATUS)) == NULL) {
		(void) printf("Error in dlsym: %s\n", dlerror());
		exit(1);
	}
	/*lint +e611 */

	err = (*fn)((scstat_ds_name_t *)sname, &status);
	if (err != SCSTAT_ENOERR) {
		(void) printf("Error calling function: %d\n", err);
		goto cleanup;
	}
	tmp = status;
	while (tmp) {
		(void) printf("Service name: %s\n",
		    (char *)tmp->scstat_ds_name);
		(void) printf("\tService status: %s\n",
		    get_state_string(tmp->scstat_ds_status));
		(void) printf("\tService status string: %s\n",
		    (char *)tmp->scstat_ds_statstr);
		node_state = tmp->scstat_node_state_list;
		if (node_state) {
			(void) printf("\tNode status:\n");
			while (node_state) {
				(void) printf("\t\t%s - %s\n",
				    (char *)node_state->scstat_node_name,
				    get_node_state_string(
				    node_state->scstat_node_state));
				node_state = node_state->scstat_node_next;
			}
		}
		(void) printf("\n");
		tmp = tmp->scstat_ds_next;
	}

cleanup:
	(void) dlclose(dlhandle);

	exit(err);
}

/*
 * Helper function to startup/switchover a device service.
 */
void
do_startup(char *sname, char *node) {
	void *dlhandle;
	scswitch_errno_t (*fn)(scswitch_service_t *, char *);
	scswitch_errno_t err;
	scswitch_service_t *slist;

	if ((dlhandle = dlopen(LIBSCSWITCH, RTLD_LAZY)) == NULL) {
		(void) printf("Error in dlopen: %s\n", dlerror());
		exit(1);
	}
	/* lint has 'Suspicious cast' issues with dlsym function ptrs (e611) */
	/*lint -e611 */
	if ((fn = (scswitch_errno_t(*)())
	    dlsym(dlhandle, SCSWITCH_SWITCH_SERVICE)) == NULL) {
		(void) printf("Error in dlsym: %s\n", dlerror());
		exit(1);
	}
	/*lint +e611 */
	slist = (scswitch_service_t *)malloc(sizeof (scswitch_service_t));
	slist->scswitch_service_name = sname;
	slist->next = NULL;

	err = (*fn)(slist, node);
	if (err != SCSWITCH_NOERR) {
		(void) printf("Error calling function: %d\n", err);
	}

	(void) dlclose(dlhandle);

	exit(err);
}

/*
 * Helper function to shutdown a device service.
 */
void
do_shutdown(char *sname) {
	void *dlhandle;
	scswitch_errno_t (*fn)(scswitch_service_t *);
	scswitch_errno_t err;
	scswitch_service_t *slist;

	if ((dlhandle = dlopen(LIBSCSWITCH, RTLD_LAZY)) == NULL) {
		(void) printf("Error in dlopen: %s\n", dlerror());
		exit(1);
	}
	/* lint has 'Suspicious cast' issues with dlsym function ptrs (e611) */
	/*lint -e611 */
	if ((fn = (scswitch_errno_t(*)())
	    dlsym(dlhandle, SCSWITCH_SHUTDOWN_SERVICE)) == NULL) {
		(void) printf("Error in dlsym: %s\n", dlerror());
		exit(1);
	}
	/*lint +e611 */

	slist = (scswitch_service_t *)malloc(sizeof (scswitch_service_t));
	slist->scswitch_service_name = sname;
	slist->next = NULL;

	err = (*fn)(slist);
	if (err != SCSWITCH_NOERR) {
		(void) printf("Error calling function: %d\n", err);
	}

	(void) dlclose(dlhandle);

	exit(err);
}
