/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _PMFD_LINUX_H
#define	_PMFD_LINUX_H

#pragma ident	"@(#)pmfd_linux.h	1.3	08/05/20 SMI"

/*
 * pmfd_linux.h - PMF daemon data structures for the Linux implementation
 */

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <strings.h>
#include <pthread.h>
#include <locale.h>
#include <errno.h>
#include <signal.h>
#include <sys/types.h>
#include <rgm/scutils.h>
#include "sys/st_failfast.h"
#include "sys/cl_assert.h"
#include "pmfd_proto.h"
#include "pmfd_event_pool.h"

extern int debug;
extern int pmf_thread_cnt;
extern struct rlimit pmf_rlimit;

/* used with pre_alloc_buf() to pre-allocate swap */
#define	PMF_MIN_SWAP_RESERVE_SIZE  4 		/* 4 MB */
#define	PMF_MAX_SWAP_RESERVE_SIZE  16		/* 16 MB */

/*
 * MAX_RPC_THREADS is the number of threads that the rpc server mechanism
 * is allowed to spawn at any one time to handle incoming rpc requests.
 * We assume that this number is large enough to handle any legitimate
 * situation, but not large enough to allow the rpc.pmfd to bring down the
 * node.  Empirically, it looks like 512 threads in the rpc.pmfd takes about
 * 64M of memory.
 *
 * On Linux, we feel that 512 threads is too huge so that we decide to reduce
 * it to 64.
 */
#define	MAX_RPC_THREADS	64

/*
 * Used to mark messages that are considered "normal" and would have
 * been targets for i18n using gettext(). SYSTEXT distinguishes this
 * category of messages from unusual or failure messages.
 */
#define	SYSTEXT(s)	s

#ifdef LOCK_DEBUG
#define	LOCK_HANDLE(h) \
	if (debug) dbg_msgout(NOGET("%s/%d LOCK HANDLE (0x%x)\n"), \
	__FILE__, __LINE__, &((h)->handle_lock)); \
	(void) CL_PANIC(pthread_mutex_lock(&((h)->handle_lock)) == 0);
#define	UNLOCK_HANDLE(h) \
	if (debug) dbg_msgout(NOGET("%s/%d UNLOCK HANDLE (0x%x)\n"), \
	__FILE__, __LINE__, &((h)->handle_lock)); \
	(void) CL_PANIC(pthread_mutex_unlock(&((h)->handle_lock)) == 0);
#define	LOCK_THREADS(h) \
	if (debug) dbg_msgout(NOGET("%s/%d LOCK THREADS (0x%x)\n"), \
	__FILE__, __LINE__, &((h)->threads_lock)); \
	(void) CL_PANIC(pthread_mutex_lock(&((h)->threads_lock)) == 0);
#define	UNLOCK_THREADS(h) \
	if (debug) dbg_msgout(NOGET("%s/%d UNLOCK THREADS (0x%x)\n"), \
	__FILE__, __LINE__, &((h)->threads_lock)); \
	(void) CL_PANIC(pthread_mutex_unlock(&((h)->threads_lock)) == 0);
#else
#define	LOCK_HANDLE(h) CL_PANIC(pthread_mutex_lock(&((h)->handle_lock)) == 0);
#define	UNLOCK_HANDLE(h) CL_PANIC(pthread_mutex_unlock(&((h)->handle_lock)) \
	== 0);
#define	LOCK_THREADS(h) CL_PANIC(pthread_mutex_lock(&((h)->threads_lock)) == 0);
#define	UNLOCK_THREADS(h) CL_PANIC(pthread_mutex_unlock(&((h)->threads_lock)) \
	== 0);
#endif

enum PMF_ACTION_WHY {
	PMF_ACTION_STARTED,
	PMF_ACTION_STOPPED,
	PMF_ACTION_KILLED,
	PMF_ACTION_EXITED,
	PMF_ACTION_FAILED
};
typedef enum PMF_ACTION_WHY PMF_ACTION_WHY;

/*
 * This is a buffer nedeed to read /proc/<pid>/stat file
 */
#define	PMF_PROC_BUFFER_SIZE	4096
struct proc_buffer {
	char buf[PMF_PROC_BUFFER_SIZE];
};

enum PMF_TREE_STATE {
	PMF_TREE_STARTING,
	PMF_TREE_STARTED,
	PMF_TREE_STOPPING,
	PMF_TREE_STOPPED
};

typedef enum PMF_TREE_STATE PMF_TREE_STATE;

struct pmf_handle_queue {
	pthread_mutex_t lock;
	pthread_cond_t cond;
	struct pmf_handle  *head;
	int count;
	int stopping;
};

/*
 * Handle to hold all the configuration information
 * for requests, along with state that the thread needs
 * in order to make decisions about restarting them.
 */
struct pmf_handle {
	char **cmd;
	char **env;
	char *pwd;
	char *path;
	PMF_ACTIONS action_type;
	char *action;
	int period;
	time_t last_requeue;
	int retries;
	char *id;
	uid_t uid;
	gid_t gid;
	pid_t pid;
	/*
	 * state of our direct child process
	 */
	int exited;
	int status;
	struct pmf_event *exit_event;
	pthread_t tid;
	pmf_result result;
	/*
	 * handle_lock protects all fields of the handle except for
	 * the threads field, which is protected by a threads_lock, and
	 * the next and prev fields, which are protected by the global
	 * queue_lock.
	 * The threads_lock can be held independently of the handle_lock.
	 *
	 * Note that there are some fields of the handle that are read-only
	 * after creation, or that are only modified in the monitor thread.
	 * These fields are sometimes accessed without holding the handle_lock.
	 * Currently some fields are read without holding the handle_lock
	 * that _can_ be modified by other threads.
	 *
	 * Note that reading a field without acquiring a lock that could be
	 * modified concurrently in another thread could cause problems if,
	 * for example, pointers were switched out from under the reader.
	 * These instances need further investigation (see bugid 4445437).
	 *
	 * Fields that are sometimes accessed without the lock include:
	 *	history (although this can be changed by pmf_modify_svc)
	 *	action, action_type
	 *		(although this can be changed by pmf_stop_svc)
	 *	last_requeue
	 *	flags
	 *	id, pid, gid, uid (protected by pmf_fork_mutex when written)
	 *	result
	 *	pwd, env_passed, path, sema_p2c (protected by pmf_fork_mutex)
	 *
	 *	All fields are read in pmf_status_svc without holding the
	 *	handle_lock.
	 *
	 */
	pthread_mutex_t handle_lock;
	/*
	 * handle_cond is used for 2 different conditions: stop
	 * and kill threads use it to wait until the alive flag is set
	 * to 0, and the start_process thread uses it in pmf_throttle_wait
	 * to wait until a stop thread sets the stop_monitor flag to 1.
	 */
	pthread_cond_t handle_cond;
	int alive;
	struct pmf_process_tag *history;
	struct child_handle  *head;
	struct pmf_handle *next;
	int flags;
	/*
	 * used in option -C to determine if to monitor children.
	 * if yes, what levels of children to monitor;
	 */
	PMF_MONITOR_CHILDREN monitor_children;
	int	monitor_level;
	/*
	 * shows if the user wants to pass the environment or to inherit
	 * the parent's environment
	 */
	bool_t env_passed;
	/*
	 * shows if the stop command was issued; if yes stop restarting tag.
	 */
	bool_t stop_monitor;
	/*
	 * restarting tag.set to true if the service under pmf needs restart.
	 */
	bool_t restart_flag;
	char *project_name;
	struct pmf_event_queue queue;
	struct proc_buffer buf;
	PMF_TREE_STATE state;
};

typedef struct pmf_handle pmf_handle;

/*
 * We have a process tag for each process we start.
 * This is needed so we can calculate how many restarts have
 * occured within the specified period.
 */
struct pmf_process_tag {
	time_t stime; /* start time ... strictly informational */
	time_t etime;
	int wstat;
	struct pmf_process_tag *next;
};
typedef struct pmf_process_tag pmf_process_tag;

/*
 * State of a pmf thread: a thread is created with the state PMF_STARTING.
 * When the monitoring starts, the state is changed to PMF_MONITORED.
 * From PMF_MONITORED, pmfadm -S changes the state to PMF_SUSPENDING and then
 * the state is set to PMF_UNMONITORED if the monitoring has been effectively
 * suspended otherwise the state is set back to PMF_MONITORED.
 * From PMF_UNMONITORED, pmfadm -R changes the state to PMF_RESUMING and then
 * the state is set to PMF_MONITORED if the monitoring has been effectively
 * resumed otherwise the state is set back to PMF_UNMONITORED.
 * The state PMF_EXITING is set when the thread exit (on failure or normal
 * exit).
 *                                     -S
 * PMF_STARTING ----> PMF_MONITORED <====> PMF_SUSPENDING
 *                         ^                    |
 *                         |                    v
 *                    PMF_RESUMING  <====> PMF_UNMONITORED
 *                                   -R
 *
 * PMF_EXITING can be reached from any state.
 */
enum PMF_MONITOR_STATE {
	PMF_FREE,
	PMF_STARTING,
	PMF_RESUMING,
	PMF_MONITORED,
	PMF_SUSPENDING,
	PMF_UNMONITORED,
	PMF_EXITING
};
typedef enum PMF_MONITOR_STATE PMF_MONITOR_STATE;

#define	PMF_IS_MONITORED(t) ((t->monitor_state == PMF_MONITORED) || \
	(t->monitor_state == PMF_STARTING) || (t->monitor_state == PMF_EXITING))

struct child_handle {
	int pid;
	int ppid;
	pmf_handle *handle;
	struct child_handle  *next;
	struct child_handle  *prev;
	unsigned long level;
	int regmask;
	pthread_mutex_t lock;
	pthread_cond_t cv;
	PMF_MONITOR_STATE monitor_state;
	/*
	 * When monitoring of a process is suspended the only reference we
	 * are holding on it is its pid.
	 * When resuming monitoring we must be sure that this pid was not
	 * reassigned to another process. For this purpose before suspending
	 * the monitoring we open a fd on /proc/<pid>/status. When resuming
	 * if initial process does not exist anymore, the read will fail,
	 * even if pid has been reassigned.
	 */
	int proc_status_fd;
	int proc_status_opened;
};
typedef struct child_handle child_handle;

/*
 * We define here the operations on registration status.
 * When a new monitored process is created it must be
 * registered twice, by its parent and by itself before
 * being able to be unregistered.
 */
#define	REGISTER_AT_STARTUP	1
#define	REGISTER_AT_FORK	2
#define	REGISTER_MASK		(REGISTER_AT_STARTUP | REGISTER_AT_FORK)
#define	UNREGISTER		4
#define	UNREGISTER_MASK		UNREGISTER
#define	IS_REGISTERED_AT_STARTUP(h)	\
	(((h)->regmask & REGISTER_AT_STARTUP) == REGISTER_AT_STARTUP)
#define	IS_REGISTERED_AT_FORK(h)	\
	(((h)->regmask & REGISTER_AT_FORK) == REGISTER_AT_FORK)
#define	IS_REGISTERED(h)	\
	(((h)->regmask & REGISTER_MASK) == REGISTER_MASK)
#define	IS_UNREGISTERED(h)	\
	(((h)->regmask & UNREGISTER_MASK) == UNREGISTER_MASK)

/*
 * Walk the process monitoring tree to
 * find the handle associated with pid.
 * Assumption is made that caller hold
 * the monitoring tree handle lock.
 */
static inline struct child_handle *
pid2handle(struct pmf_handle *h, int pid)
{

	struct child_handle *p;

	ASSERT(h);

	for (p = h->head; p; p = p->next)
		if (p->pid == pid) break;
	return (p);
}

/*
 * Same as above but check successively
 * against pid and ppid.
 * This is used to find the handle of a
 * process or of its parent.
 */
static inline struct child_handle *
pidppid2handle(struct pmf_handle *h, int pid, int ppid)
{

	struct child_handle *p;

	ASSERT(h);

	for (p = h->head; p; p = p->next) {
		if (p->pid == pid ||
		    p->pid == ppid) break;
	}
	return (p);
}

/*
 * Time measured in microseconds.
 */
typedef long long usectime_t;

extern void pmf_start_svc(pmf_start_args *args, pmf_result *result,
	security_cred_t *cred);
extern void pmf_modify_svc(pmf_modify_args *args, pmf_result *result,
	security_cred_t *cred);
extern void pmf_status_svc(pmf_args *args, pmf_status_result *result,
	security_cred_t *cred);
extern void pmf_stop_svc(pmf_args *args, pmf_result *result,
	security_cred_t *cred);
extern void pmf_kill_svc(pmf_args *args, pmf_result *result,
	security_cred_t *cred);
extern void pmf_suspend_svc(pmf_args *args, pmf_result *result,
	security_cred_t *cred);
extern void pmf_resume_svc(pmf_args *args, pmf_result *result,
	security_cred_t *cred);
extern void pmf_null_svc(pmf_result *result);

#ifdef __cplusplus
}
#endif

#endif /* _PMFD_LINUX_H */
