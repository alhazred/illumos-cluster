/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)pmfd_util.c	1.124	08/08/01 SMI"

/*
 * pmfd_util.c - Workhorse routines for the pmf daemon
 */

#include "pmfd.h"

#if DOOR_IMPL
#include <rgm/pmf.h>
#else
#include <rgm/rpc/pmf.h>
#endif

#include <ctype.h>
#include <tzfile.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/sc_syslog_msg.h>
#include <sys/rsrc_tag.h>
#include <sys/cl_events.h>


#ifdef SC_SRM
#include <sys/task.h>
#include <project.h>
#include <pwd.h>

#define	PWD_BUF_SIZE	1024
#endif

#ifdef LOCK_DEBUG
#define	LOCK_QUEUE(name) \
	if (debug) dbg_msgout(NOGET("%s/%d LOCK QUEUE (0x%x)\n"), \
	NOGET("name"), __LINE__, &pmf_queue_lock); \
	CL_PANIC(pthread_mutex_lock(&pmf_queue_lock) == 0);
#define	UNLOCK_QUEUE(name) \
	if (debug) dbg_msgout(NOGET("%s/%d UNLOCK QUEUE (0x%x)\n"), \
	NOGET("name"), __LINE__, &pmf_queue_lock); \
	CL_PANIC(pthread_mutex_unlock(&pmf_queue_lock) == 0);
#define	LOCK_FORK() \
	if (debug) dbg_msgout(NOGET("%s/%d LOCK FORK (0x%x)\n"), \
	__FILE__, __LINE__, &pmf_fork_mutex); \
	CL_PANIC(pthread_mutex_lock(&pmf_fork_mutex) == 0);
#define	UNLOCK_FORK() \
	if (debug) dbg_msgout(NOGET("%s/%d UNLOCK FORK (0x%x)\n"), \
	__FILE__, __LINE__, &pmf_fork_mutex); \
	CL_PANIC(pthread_mutex_unlock(&pmf_fork_mutex) == 0);
#else
#define	LOCK_QUEUE(h) CL_PANIC(pthread_mutex_lock(&pmf_queue_lock) == 0);
#define	UNLOCK_QUEUE(h) CL_PANIC(pthread_mutex_unlock(&pmf_queue_lock) == 0);
#define	LOCK_FORK(h) CL_PANIC(pthread_mutex_lock(&pmf_fork_mutex) == 0);
#define	UNLOCK_FORK(h) CL_PANIC(pthread_mutex_unlock(&pmf_fork_mutex) == 0);
#endif


/*
 * Maximum number of seconds to sleep if we're restarting
 * from the action script too often.
 */
#define	PMF_THROTTLE_LIMIT 30

/*
 * Delay (in seconds) used by the suspend and resume options to periodically
 * notify (with SIGUSR1) the monitoring thread that a suspend/resume request
 * is pending.
 */
#define	PMF_REQUEST_NOTIFY_DELAY 5

static int pmf_action_exec(pmf_handle *handle, PMF_ACTION_WHY why);

static pmf_handle *pmf_alloc_handle(pmf_start_args *args,
	struct security_cred_t *cred,
	pmf_result *result);
static void pmf_free_handle(pmf_handle *handle, bool_t detach_thread);
static void start_process(pmf_handle *handle);
static void perform_throttling(pmf_handle *handle, char *cmd_string);
static pmf_handle *pmf_id_to_handle(char *id);
static pmf_process_tag *pmf_alloc_process_history(pmf_handle *handle);
static void pmf_free_process_history(pmf_process_tag **tag);
static uint_t pmf_process_scan_history(pmf_handle *handle);
static int pmf_timedwait(pmf_handle *handle, int seconds,
    bool_t wait_for_queue);
static int pmf_run_process(pmf_handle *handle, char **cmd);
static bool_t pmf_throttle_wait(pmf_handle *handle, time_t req);

/*
 * pmf_queue_lock protects pmf_handle_base, and the ring
 * queue, so it can be updated/inspected by multiple threads.
 * pmf_queue_lock should be held whenever the next or prev
 * fields of a pmf_handle are read or changed.  That is, whenever
 * a handle is created (and added to the queue) or destroyed (and
 * removed from the queue), or whenever the queue is searched for a
 * particular handle.
 *
 * Note that the pmf_queue_lock does not protect fields of the pmf_handle
 * other than next and prev.  Thus, to ensure mutual exclusion when accessing
 * fields of a handle, the handle_lock should be held.  Furthermore, both the
 * pmf_queue_lock and the handle_lock should be held while deleting a handle.
 *
 * The handle_lock for a handle can be acquired while holding the
 * pmf_queue_lock.  Thus, the pmf_queue_lock must not be acquired while
 * holding a handle_lock, to avoid deadlock.
 */
/*
 * Suppress lint warning about "union initialization" for the next ~10 lines.
 * PTHREAD_MUTEX_INITIALIZER is defined in /usr/include/pthread.h
 */
/*lint -e708 */
static pthread_mutex_t pmf_queue_lock = PTHREAD_MUTEX_INITIALIZER;
static pmf_handle *pmf_handle_base = NULL;
static pthread_cond_t pmf_queue_cond = PTHREAD_COND_INITIALIZER;

/*
 * mutex to protect our fork semaphore
 */
static pthread_mutex_t pmf_fork_mutex = PTHREAD_MUTEX_INITIALIZER;
/*lint +e708 */

/*
 * Variables for debugging malloc leakage
 */
int pmf_alloc_cnt = 0;
int pmf_thread_cnt = 0;

/*
 * proc_mode is set to 1 to indicate that we want to do monitoring.
 * Set it to 0 if there's a suspicion that a bug is caused by the monitoring
 * procfs-related operations; they will all be skipped and there will be no
 * monitoring.
 */
static	int	proc_mode = 1;

/*
 * Routines for allocating and freeing process history tags
 */
static pmf_process_tag *
pmf_alloc_process_history(pmf_handle *handle)
{
	pmf_process_tag *new;

	LOCK_HANDLE(handle);

	/* Set the restart_flag to B_TRUE */
	handle->restart_flag = B_TRUE;

	new = (pmf_process_tag *) svc_alloc(sizeof (pmf_process_tag),
		&pmf_alloc_cnt);
	if (new == NULL) {
		UNLOCK_HANDLE(handle);
		return (NULL);
	}
	(void) memset(new, 0, sizeof (pmf_process_tag));

	/*
	 * Initialize
	 */
	new->etime = (time_t)0;
	new->stime = time(NULL);
	new->wstat = NULL;

	/*
	 * Put us at the head of the list
	 */
	new->next = handle->history;
	handle->history = new;

	UNLOCK_HANDLE(handle);

	return (new);
}

/*
 * We free up all the way down to the end, since
 * we are either freeing up the entire list, or
 * entries that are older than we're interested in.
 */
static void
pmf_free_process_history(pmf_process_tag **tag)
{
	if (*tag == NULL)
		return;

	if ((*tag)->next)
		pmf_free_process_history(&(*tag)->next);
	(*tag)->next = NULL;
	svc_free((*tag), &pmf_alloc_cnt);
	*tag = NULL;
}



/*
 * Scan the history list, returning a count
 * of the relevant ones that are there.
 */
static uint_t
pmf_process_scan_history(pmf_handle *handle)
{
	pmf_process_tag **ptr;
	uint_t cnt = 0;

	/*
	 * If the history was reset by a modify command, return 0
	 */
	if (handle->history == NULL)
		return (0);

	/*
	 * Scan through and remove any old (outside of period) entries.
	 */
	for (ptr = &(handle->history); *ptr; ) {
		if ((*ptr)->etime) {
			/*
			 * We simply count all the entries if
			 * the specified period is -1 (infinite).
			 */
			if (handle->period != -1) {
				if (time(NULL) - (*ptr)->etime >
				    handle->period) {
					pmf_free_process_history(ptr);
					break;
				}
			}
			cnt++;
			/*
			 * To avoid having huge history lists, only
			 * allow up to one hundred entries.
			 */
			if (cnt > PMFD_MAX_HISTORY) {
				pmf_free_process_history(ptr);
				break;
			}
		}
		ptr = &(*ptr)->next;
	}

	return (cnt);
}

/*
 * this routine implements throttle wait logic.
 */
static void
perform_throttling(pmf_handle *handle,
    char *cmd_string)
{
	time_t req;
	sc_syslog_msg_handle_t sys_handle;
	req = time(NULL) - handle->last_requeue;

	if (req < PMF_THROTTLE_LIMIT * 2) {
		handle->last_requeue = time(NULL);
		UNLOCK_HANDLE(handle);
		if (req == 0) {
			req++;
		} else if (req < PMF_THROTTLE_LIMIT) {
			req *= 2;
		}
		if (req > PMF_THROTTLE_LIMIT)
			req = PMF_THROTTLE_LIMIT;

		(void) sc_syslog_msg_initialize(
		    &sys_handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_NOTICE, MESSAGE,
		    "\"%s\" restarting too often "
		    "... sleeping %d seconds.",
		    handle->id, req);
		sc_syslog_msg_done(&sys_handle);
		/* post an event */
		(void) sc_publish_event(
		    ESC_CLUSTER_PMF_PROC_NOT_RESTARTED,
		    CL_EVENT_PUB_PMF, CL_EVENT_SEV_INFO,
		    CL_EVENT_INIT_SYSTEM, 0, 0, 0,
		    CL_PMF_NAME_TAG,
		    SE_DATA_TYPE_STRING, handle->id,
		    CL_PMF_CMD_PATH,
		    SE_DATA_TYPE_STRING, cmd_string,
		    CL_FAILURE_REASON, DATA_TYPE_UINT32,
		    CL_FR_PMF_PROC_THROTTLED, NULL);


		/*
		 * Call pmf_throttle_wait
		 * to wait for the requested time
		 * while also waiting for the
		 * stop_monitor flag to be
		 * set to 1.
		 * This is our last chance to bail out
		 * of requeueing the process.
		 */
		if (!pmf_throttle_wait(handle, req)) {
			LOCK_HANDLE(handle);
			handle->restart_flag = B_FALSE;
		}
	} else {
		handle->last_requeue = time(NULL);
	}
}

/*
 * This is our thread main ... we simply sit in a loop
 * executing the command we're given until we reach the
 * limit on the number of times we're allowed to do so.
 *
 */

static void
start_process(pmf_handle *handle)
{
	char **aptr;
	char *cmd_string = NULL;
	size_t newlen, oldlen;
	uint_t history_cnt;
	sc_syslog_msg_handle_t sys_handle;

	if (debug) {
		dbg_msgout(NOGET("starting\n"));
		for (aptr = handle->cmd; aptr && *aptr; aptr++)
			dbg_msgout("\t%s\n", *aptr);
		dbg_msgout("\n");
	}

	/*
	 * get the actual command and the arguments passed
	 *
	 */

	LOCK_HANDLE(handle);
	aptr = handle->cmd;

	while (aptr && *aptr) {

		if (cmd_string != NULL) {
			(void) strcat(cmd_string, " "); /* Add a space */
		}

		oldlen = cmd_string ? strlen(cmd_string) : 0;
		newlen = strlen(*aptr) + 1 + 1 + oldlen;
			/* extra 1+1 for the NULL and space */
		cmd_string = (char *)realloc(cmd_string, newlen);
		if (cmd_string == NULL) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_PMF_PMFD_TAG, "");
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE, "Out of memory.");
			sc_syslog_msg_done(&sys_handle);
			UNLOCK_HANDLE(handle);
			pmf_free_handle(handle, TRUE);
			return;
		}
		cmd_string[oldlen] = '\0';
		(void) strcat(cmd_string, *aptr);
		aptr++;
	}
	UNLOCK_HANDLE(handle);


	/*
	 * Loop, restarting the process.  We loop indefinitely.  The conditions
	 * that cause us to break out of the loop are:
	 * 1) We fail to allocate memory for the history file before executing.
	 * 2) We are told to stop monitoring the process (another thread has
	 *	changed the stop_monitor flag to 1.
	 * 3) We've restarted more than handle->retries number of times, and
	 *	we execute an action script that fails.
	 */
loop:

	/* First, attempt to allocate memory for the history */
	if (pmf_alloc_process_history(handle) == NULL) {
		/*
		 * We failed to allocate memory for the history:
		 * log message, free handle and exit this thread.
		 */
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
		    "Tag %s: could not allocate history.",
		    handle->id);
		sc_syslog_msg_done(&sys_handle);
		pmf_free_handle(handle, TRUE);
		free(cmd_string);
		return;
	} else {
		/*
		 * Actually run the process.
		 *
		 * We ignore whether it fails or not, to allow
		 * the following code to cleanup as if it had
		 * exited.
		 */
		(void) pmf_run_process(handle, handle->cmd);
	}

	/*
	 * At this point the process has just finished executing.
	 * Here's where we need to restart the process.
	 * If the user asked to stop monitoring simply fall through to
	 * the switch statement, which will break out instead of continuing
	 * the loop.
	 * Otherwise, scan the history list, and if we have
	 * fewer tags than the retry count, start it up again.
	 *
	 * Acquire the handle lock so we can read both action_type and
	 * stop_monitor exclusively with respect to other threads.
	 * We need the handle lock instead of the queue lock because
	 * the queue lock does not protect fields of the handle.  We need
	 * to ensure mutual exclusion here because other threads can modify
	 * both the action_type and stop_monitor fields.
	 */
	LOCK_HANDLE(handle);

	/* make sure we're not supposed to stop monitoring */
	if ((handle->action_type == PMFACTION_EXEC) ||
	    ((handle->action_type == PMFACTION_NULL) &&
	    !handle->stop_monitor)) {
		/*
		 * pmf_process_scan_history will return the exit
		 * that just occured.  Since we haven't restarted
		 * for it yet, we need to disregard it so we
		 * only concern ourselves with prior restarts.
		 */
		history_cnt = pmf_process_scan_history(handle);

		if (debug)
			dbg_msgout(NOGET("history_cnt=%d retries=%d\n"),
			    history_cnt, handle->retries);

		if (history_cnt != 0)
			history_cnt--;

		if ((handle->retries == -1) ||
		    ((uint_t)(handle->retries) - history_cnt > 0)) {

			/*
			 * Apply throttling if there is no action script.
			 * or if action script was specified with
			 * infinite retries (-1).
			 * We attempt to quench runaway processes
			 * here by throttling restart requests up
			 * to PMF_THROTTLE_LIMIT seconds.
			 *
			 * Once we start behaving we will restart
			 * immediately without sleeping.
			 */
			if (((handle->action_type == PMFACTION_NULL) ||
			    (handle->retries == -1)) &&
			    (!handle->stop_monitor))
				perform_throttling(handle, cmd_string);
			/*
			 * Check for restart_flag.If its set to false then
			 * another thread requested that we stop monitoring,
			 * by changing the stop_monitor flag.
			 * Do not restart the process.
			 */

			if (handle->restart_flag) {
				if (debug) {
					dbg_msgout(NOGET("restarting\n"));
					for (aptr = handle->cmd;
					    aptr && *aptr; aptr++)
						dbg_msgout("\t%s\n", *aptr);
					dbg_msgout("\n");
				}

				if (PMF_EXIT_QUIETLY_ISCLR(handle->flags)) {
					(void) sc_syslog_msg_initialize(
					    &sys_handle,
					    SC_SYSLOG_PMF_PMFD_TAG, "");
					(void) sc_syslog_msg_log(sys_handle,
					    LOG_NOTICE, MESSAGE,
					    "PMF is restarting process that "
					    "died: tag=%s, cmd_path=%s, "
					    "max_retries=%d, "
					    "num_retries=%d",
					    handle->id, cmd_string,
					    handle->retries, history_cnt);
					sc_syslog_msg_done(&sys_handle);

				}
				/* post an event */
				(void) sc_publish_event(
				    ESC_CLUSTER_PMF_PROC_RESTART,
				    CL_EVENT_PUB_PMF, CL_EVENT_SEV_INFO,
				    CL_EVENT_INIT_SYSTEM, 0, 0, 0,
				    CL_PMF_NAME_TAG, SE_DATA_TYPE_STRING,
				    handle->id,
				    CL_PMF_CMD_PATH, SE_DATA_TYPE_STRING,
				    cmd_string,
				    CL_TOTAL_ATTEMPTS, DATA_TYPE_UINT32,
				    handle->retries,
				    CL_ATTEMPT_NUMBER, DATA_TYPE_UINT32,
				    history_cnt,
				    CL_REASON_CODE, DATA_TYPE_UINT32,
				    CL_REASON_PMF_RESTART, NULL);

				UNLOCK_HANDLE(handle);
				/*
				 * We haven't reached our restart limit,
				 * so execute the process again .
				 */

				goto loop;
			}
		}
		/* If we get here, we've restarted too many times. */
		/* Fall through to the action switch statement */

	}

	/*
	 * We fell through.  Either we've decided to stop monitoring
	 * this process, or we exceeded the number of retries.
	 * Print a message, then enter the action switch statement
	 * to decide whether to continue restarting or to exit the loop.
	 * Don't print the message if we're stopping monitoring, because
	 * the user doesn't care that it failed to stay up (he or she
	 * wants it to stop).
	 */
	if (PMF_EXIT_QUIETLY_ISCLR(handle->flags) &&
	    !handle->stop_monitor) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		(void) sc_syslog_msg_log(sys_handle, LOG_NOTICE, MESSAGE,
		    "Process: tag=\"%s\", cmd=\"%s\", Failed to stay up.",
		    handle->id, cmd_string);
		sc_syslog_msg_done(&sys_handle);

	}

	/* post an event */
	(void) sc_publish_event(
	    ESC_CLUSTER_PMF_PROC_NOT_RESTARTED,
	    CL_EVENT_PUB_PMF, CL_EVENT_SEV_INFO,
	    CL_EVENT_INIT_SYSTEM, 0, 0, 0,
	    CL_PMF_NAME_TAG, SE_DATA_TYPE_STRING,
	    handle->id, CL_PMF_CMD_PATH, SE_DATA_TYPE_STRING,
	    cmd_string, CL_FAILURE_REASON, DATA_TYPE_UINT32,
	    CL_FR_PMF_RETRIES_EXCEEDED, NULL);
	/*
	 * release the handle lock.
	 * bugids 4445437 and 4445426 will investigate holding the lock
	 * throughout this routine to prevent race conditions.
	 */
	UNLOCK_HANDLE(handle);

	/*
	 * Action switch statement:
	 *
	 * There are two cases: either we have an action to execute or we
	 * don't. If we don't have an action, always break out of the loop.
	 * If we have an action, execute it.  If the action exits abnormally,
	 * break out of the loop and stop restarting.  If the action script
	 * exists with status 0, check if we've restarted too many times
	 * in a short amount of time.  If so, call pmf_throttle_wait
	 * where we sleep for a while to give things a chance to settle down.
	 *
	 * If we are supposed to execute an action script, action_type
	 * will be set to PMFACTION_EXEC.
	 * If we are supposed to stop monitoring, action type will have been
	 * changed to PMFACTION_NULL.  Thus, at this point in the execution,
	 * we don't distinguish between user requested stopping of monitoring
	 * and lack of an action script.  Both cases mean an end to monitoring.
	 */
	switch (handle->action_type) {
		case PMFACTION_NULL:
			break;
		case PMFACTION_EXEC:
			/*
			 * If the action routine exits non zero we
			 * stop, otherwise, clear out the history
			 * and start over.
			 */
			if (pmf_action_exec(handle, PMF_ACTION_FAILED) == 0) {
				/*
				 * The action script succeeded.
				 * Remove the old history information, since
				 * we're starting from scratch.
				 */
				if (handle->history)
				    pmf_free_process_history(&handle->history);

				/*
				 * We attempt to quench runaway processes
				 * here by throttling restart requests up
				 * to PMF_THROTTLE_LIMIT seconds.
				 *
				 * Once we start behaving we will restart
				 * immediately without sleeping.
				 */
				LOCK_HANDLE(handle);
				perform_throttling(handle,
				    cmd_string);

				if (!handle->restart_flag) {
					UNLOCK_HANDLE(handle);
					break;
				}

				UNLOCK_HANDLE(handle);
				if (PMF_EXIT_QUIETLY_ISCLR(handle->flags)) {
					(void) sc_syslog_msg_initialize(
					    &sys_handle,
					    SC_SYSLOG_PMF_PMFD_TAG, "");
					/*
					 * SCMSGS
					 * @explanation
					 * The process with the given tag
					 * failed to stay up and exceeded the
					 * allowed number of retry attempts
					 * (given by the 'pmfadm -n' option),
					 * and the action (given by the
					 * 'pmfadm -a' option) was initiated
					 * by rpc.pmfd. The action succeeded
					 * (i.e., returned zero), and rpc.pmfd
					 * restarted the exited process. For
					 * more information, see pmfadm(1M).
					 * @user_action
					 * This message is informational; no
					 * user action is needed.
					 */
					(void) sc_syslog_msg_log(sys_handle,
					    LOG_NOTICE, MESSAGE,
					    "PMF is restarting tag <%s>,"
					    " cmd_path=\"%s\"", handle->id,
					    cmd_string);
					sc_syslog_msg_done(&sys_handle);
				}

				/*
				 * The action script succeeded, so we
				 * continue restarting.
				 */

				/* post an event */
				(void) sc_publish_event(
				    ESC_CLUSTER_PMF_PROC_RESTART,
				    CL_EVENT_PUB_PMF, CL_EVENT_SEV_INFO,
				    CL_EVENT_INIT_SYSTEM, 0, 0, 0,
				    CL_PMF_NAME_TAG, SE_DATA_TYPE_STRING,
				    handle->id,
				    CL_PMF_CMD_PATH, SE_DATA_TYPE_STRING,
				    cmd_string,
				    CL_TOTAL_ATTEMPTS, DATA_TYPE_UINT32,
				    handle->retries,
				    CL_ATTEMPT_NUMBER, DATA_TYPE_UINT32, 0,
				    CL_REASON_CODE, DATA_TYPE_UINT32,
				    CL_REASON_PMF_ACTION_SUCCESS, NULL);
				goto loop;
			} else {
			/*
			 * The action routine failed -- inform user
			 */
				(void) sc_syslog_msg_initialize(&sys_handle,
				    SC_SYSLOG_PMF_PMFD_TAG, "");
				(void) sc_syslog_msg_log(sys_handle,
				    LOG_INFO, MESSAGE,
				    "PMF did not restart tag <%s>, "
				    "cmd_path=\"%s\"", handle->id, cmd_string);
				sc_syslog_msg_done(&sys_handle);

				/* post an event */
				(void) sc_publish_event(
				    ESC_CLUSTER_PMF_PROC_NOT_RESTARTED,
				    CL_EVENT_PUB_PMF, CL_EVENT_SEV_INFO,
				    CL_EVENT_INIT_SYSTEM, 0, 0, 0,
				    CL_PMF_NAME_TAG, SE_DATA_TYPE_STRING,
				    handle->id,
				    CL_PMF_CMD_PATH, SE_DATA_TYPE_STRING,
				    cmd_string,
				    CL_FAILURE_REASON, DATA_TYPE_UINT32,
				    CL_FR_PMF_ACTION_FAILED, NULL);
			}
			break;
		default:
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_PMF_PMFD_TAG, "");
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_WARNING, MESSAGE,
			    "pmfd: unknown action (0x%x)",
			    handle->action_type);
			sc_syslog_msg_done(&sys_handle);
			break;
	}

	/*
	 * If we get here, either the action script failed, there was no
	 * action script, or another thread requested that we stop monitoring,
	 * by changing the stop_monitor flag.
	 * In any case it means stop restarting the process.
	 */

	free(cmd_string);

	/*
	 * This routine should only be called from here.
	 * Otherwise, locking needs to be enhanced to allow
	 * other threads to free a handle without stepping
	 * on us.
	 */
	pmf_free_handle(handle, TRUE);
}

/*
 * pmf_throttle_wait
 *
 * Returns TRUE if we timeout and the stop_monitor flag is FALSE.
 * Returns FALSE if the stop_monitor flag is TRUE or if there is an
 * error in the pthread_cond_timedwait call.  We will wait for req time
 * or until the stop monitor flag is changed and the condition var
 * is broadcast of that fact.
 */
static bool_t
pmf_throttle_wait(pmf_handle *handle, time_t req)
{
	struct timespec to;
	int err = 0;
	sc_syslog_msg_handle_t sys_handle;

	/*
	 * Instead of just sleeping, we do a timed wait on the condition
	 * var that will be signaled if we're supposed to stop monitoring
	 * the process. That way we don't wait for a long time before stopping
	 * monitoring, if the user decides to stop monitoring while we're
	 * waiting.
	 */
	LOCK_HANDLE(handle);
	to.tv_sec = req + time(NULL);
	to.tv_nsec = 0;
	if (debug)
		dbg_msgout(NOGET("entering timedwait\n"));
	while (!handle->stop_monitor) {
		err = pthread_cond_timedwait(&handle->handle_cond,
		    &handle->handle_lock, &to);
		if (err != 0) {
			break;
		}
	}
	/* Check if we timed out */
	if (err == ETIMEDOUT) {
		if (debug)
			dbg_msgout(NOGET("timedout\n"));
	} else if (err != 0) {
		/*
		 * We've received an unexpected error from the
		 * pthread_cond_timedwait call.  Write a syslog message
		 * and return FALSE so that this tag will not be requeued.
		 */
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
		    "Tag %s: error number %d in throttle wait; "
		    "process will not be requeued.", handle->id, err);
		sc_syslog_msg_done(&sys_handle);
		UNLOCK_HANDLE(handle);
		return (FALSE);
	}
	/*
	 * If we are supposed to stop
	 * monitoring, return FALSE
	 */
	if (handle->stop_monitor) {
		if (debug)
			dbg_msgout(NOGET("Caught the stop_monitor flag\n"));
		/* release the lock */
		UNLOCK_HANDLE(handle);
		return (FALSE);
	}
	/* We must release the lock */
	UNLOCK_HANDLE(handle);
	return (TRUE);
}

static int
pmf_run_process(pmf_handle *handle, char **cmd)
{
	int w;
	int err = 0, err1;
	sc_syslog_msg_handle_t sys_handle;

	LOCK_FORK();
	/*
	 * Note that we use fork1 instead of fork to duplicate
	 * only the calling lwp.  Because we link with pthreads, this behavior
	 * would be the same with fork, but we use fork1 for explicitness.
	 *
	 * Note that we synchronize the parent and child processes using
	 * two semaphores.
	 *
	 * The first semaphore requires the parent process
	 * to wait until the child process has forked and completed all its
	 * pre-exec setup before the parent sets up the /proc triggers.  This
	 * serialization prevents race conditions where the monitoring
	 * thread of the new process doesn't know whether the first fork
	 * on which the monitored process stops is the fork that created
	 * it, or a fork that needs to be followed.  The previous code assumed
	 * that there would be no forks between the first fork and the exec,
	 * but it turns out that it is possible for syslog/vsyslog to fork
	 * unexpectedly.
	 *
	 * The second semaphore requires the child process to wait until
	 * the parent process has set up the /proc triggers before the
	 * child calls exec.  That is necessary to ensure that the parent
	 * has set up the triggers before the child does anything interesting
	 * following its exec.
	 */
	if ((handle->pid = fork1()) != 0) {
		/*
		 * Parent
		 */
		if (handle->pid == -1) {
			/*
			 * Suppress lint message about "call to __errno() not
			 * made in the presence of a prototype."
			 * lint is unable to see the prototype for the __errno()
			 * function that errno is macroed to.
			 */
			err1 = errno; /*lint !e746 */
			handle->result.code.type = PMF_SYSERRNO;
			handle->result.code.sys_errno = errno;
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_PMF_PMFD_TAG, "");
			(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
			    "fork: %s", strerror(err1));
			sc_syslog_msg_done(&sys_handle);
			UNLOCK_FORK();
			err = -1;
			goto bad;
		}

		/*
		 * Wait here until our child has finished its pre-exec
		 * tasks.  Note that sema_wait returns an error code directly,
		 * not via errno.
		 */
		while ((err1 = sema_wait(handle->sema_c2p)) != 0) {
			/*
			 * The only error code that we expect is EINTR.
			 * If we get anything else, it's a serious error.
			 * With EINTR, we just try the sema_wait again.
			 */
			if (err1 == EINTR) {
				continue;
			}

			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_PMF_PMFD_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * The rpc.pmfd server was not able to act on a
			 * semaphore. The message contains the system error.
			 * The server does not perform the action requested by
			 * the client, and an error message is output to
			 * syslog.
			 * @user_action
			 * Save the /var/adm/messages file. Contact your
			 * authorized Sun service provider to determine
			 * whether a workaround or patch is available.
			 */
			(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
			    "sema_wait parent: %s", strerror(err1));
			sc_syslog_msg_done(&sys_handle);

			handle->result.code.type = PMF_SYSERRNO;
			handle->result.code.sys_errno = err1;
			UNLOCK_FORK();
			err = -1;
			goto bad;
		}

		if (debug) {
			dbg_msgout(NOGET("after sema_wait on pid %d; "
			    "about to set up triggers\n"), handle->pid);
		}

		/*
		 * Setup triggers and start monitor thread, while our
		 * child waits on the sema_p2c semaphore.
		 *
		 * Don't do it if we're in testing mode
		 */
		if (proc_mode) {
			if (pmf_set_up_monitor(handle) == -1) {
				handle->result.code.type = PMF_SYSERRNO;
				handle->result.code.sys_errno = errno;
				UNLOCK_FORK();
				err = -1;
				goto bad;
			}
		}

		/*
		 * Let child continue execution (into exec).
		 * Note that sema_post returns an error code directly,
		 * not via errno.
		 */
		if ((err1 = sema_post(handle->sema_p2c)) != 0) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_PMF_PMFD_TAG, "");
			(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
			    "sema_post parent: %s", strerror(err1));
			sc_syslog_msg_done(&sys_handle);
		}

		LOCK_HANDLE(handle);
		handle->alive = 1;
		UNLOCK_HANDLE(handle);

	} else {
		/*
		 * Child
		 */

		/*
		 * Make sure we're not running children as realtime
		 * processes.  We restore the scheduling parameters
		 * that rpc.pmfd was started with ... by default TS.
		 *
		 * The schedule information should be passed in
		 *	the RPC request packet and restored, but there
		 *	are security reasons for not doing so.  Force
		 *	the user to reestablish any special scheduling.
		 */
		/*
		 * In debug mode we skip this because we also skipped
		 * setting the priority to RT in svc_init() in pmfd_main.c.
		 * Both functions are in libscutils.
		 */
		if (!debug && svc_restore_priority() == 1) {
			goto badchild;
		}


		/*
		 * Set the fd limit down so that a child ignorant of the fact
		 * that we set FD_CLOEXEC and ignorant of closefrom(3C) doesn't
		 * spend forever trying to close file descriptors upon
		 * daemonization.
		 */
		if (setrlimit(RLIMIT_NOFILE, &pmf_rlimit) == -1) {
			err = errno;
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_PMF_PMFD_TAG, "");
			(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
			    "setrlimit before exec: %s", strerror(err));
			sc_syslog_msg_done(&sys_handle);
		}

		/*
		 * Set close on exec on all open file descriptor
		 */
		close_on_exec();

#ifdef SC_SRM
		/*
		 * Set project to the specified project name before starting
		 * the application.  If setproject() call fails, the
		 * application will be started with the system default project.
		 *
		 * Note that we must call setproject before we set the uid
		 * of the process, because setproject can only be called by
		 * root.
		 */
		if (handle->project_name != NULL &&
		    *handle->project_name != '\0') {
			struct passwd pwd, *pwdp;
			char pwd_buff[PWD_BUF_SIZE];

			if (getpwuid_r(handle->uid, &pwd, pwd_buff,
			    PWD_BUF_SIZE, &pwdp) == 0) {
				if (setproject(handle->project_name,
				    pwd.pw_name, TASK_NORMAL) == -1) {
					err1 = errno;
					(void) sc_syslog_msg_initialize(
					    &sys_handle,
					    SC_SYSLOG_PMF_PMFD_TAG, "");
					(void) sc_syslog_msg_log(
					    sys_handle, LOG_WARNING, MESSAGE,
					    "setproject: %s; attempting to "
					    "continue the process with the "
					    "system default project.",
					    strerror(err1));
					sc_syslog_msg_done(&sys_handle);
					(void) setproject("default",
					    pwd.pw_name, TASK_NORMAL);
				} else {
					if (debug) {
						dbg_msgout(NOGET(
						    "Change project name to "
						    "%s\n"),
						    handle->project_name);
					}
				}
			}
		} else {
			/* project is not specified; set to default project */
			struct passwd pwd, *pwdp;
			char pwd_buff[PWD_BUF_SIZE];
			if (getpwuid_r(handle->uid, &pwd, pwd_buff,
			    PWD_BUF_SIZE, &pwdp) == 0) {
				(void) setproject("default", pwd.pw_name,
				    TASK_NORMAL);
			} else {
				(void) setproject("default", "root",
				    TASK_NORMAL);
			}
		}
#endif

		/*
		 * We must set the groupid before resetting
		 * our userid - otherwise we won't have
		 * permission to do the former.
		 */
		if ((handle->gid != NULL) && (setgid(handle->gid) == -1)) {
			err1 = errno;
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_PMF_PMFD_TAG, "");
			(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
			    "setgid: %s", strerror(err1));
			sc_syslog_msg_done(&sys_handle);
			goto badchild;
		}

		if ((handle->uid != NULL) && (setuid(handle->uid) == -1)) {
			err1 = errno;
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_PMF_PMFD_TAG, "");
			(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
			    "setuid: %s", strerror(err1));
			sc_syslog_msg_done(&sys_handle);
			goto badchild;
		}

		if ((handle->pwd != NULL) && (chdir(handle->pwd) == -1)) {
			err1 = errno;
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_PMF_PMFD_TAG, "");
			(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
			    "chdir: %s", strerror(err1));
			sc_syslog_msg_done(&sys_handle);
			goto badchild;
		}

		/*
		 * if user asked to pass env. vars, don't set the path;
		 * otherwise set it to the caller path.
		 */
		if (!handle->env_passed && (handle->path != NULL)) {
			if (putenv(handle->path) != 0) {
				err1 = errno;
				(void) sc_syslog_msg_initialize(&sys_handle,
				    SC_SYSLOG_PMF_PMFD_TAG, "");
				(void) sc_syslog_msg_log(sys_handle, LOG_ERR,
				    MESSAGE, "putenv: %s",
				    strerror(err1));
				sc_syslog_msg_done(&sys_handle);
				goto badchild;
			}
		}

		if (debug) {
			dbg_msgout(NOGET("Set everything up; posting to "
			    "parent proc.\n"));
		}


		/*
		 * Now that we have set up everything before
		 * execing, we can tell our parent to set up the
		 * /proc triggers.
		 *
		 * Note that sema_post returns an error code directly,
		 * not via errno.
		 */
		if ((err1 = sema_post(handle->sema_c2p)) != 0) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_PMF_PMFD_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * The rpc.pmfd server was not able to act on a
			 * semaphore. The message contains the system error.
			 * The server does not perform the action requested by
			 * the client, and an error message is output to
			 * syslog.
			 * @user_action
			 * Determine if the machine is running out of memory.
			 * If this is not the case, save the /var/adm/messages
			 * file. Contact your authorized Sun service provider
			 * to determine whether a workaround or patch is
			 * available.
			 */
			(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
			    "sema_post child: %s", strerror(err1));
			sc_syslog_msg_done(&sys_handle);
		}


		/*
		 * Wait here until our parent has had a chance to setup
		 * triggers for us.  We don't want to exec until the
		 * triggers have been set, because we might fork immediately
		 * afterward (which the monitoring thread would need to
		 * catch).  Note that sema_wait returns an error code
		 * directly, not via errno.
		 */
		while ((err1 = sema_wait(handle->sema_p2c)) != 0) {
			/*
			 * The only error code that we expect is EINTR.
			 * If we get anything else, it's a serious error.
			 * With EINTR, we just try the sema_wait again.
			 */
			if (err1 == EINTR) {
				continue;
			}

			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_PMF_PMFD_TAG, "");
			(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
			    "sema_wait child: %s", strerror(err1));
			sc_syslog_msg_done(&sys_handle);

			goto badchild;
		}

		if (debug) {
			dbg_msgout(NOGET("Child: after sema_wait; about to "
			    "exec.\n"));
		}


		/*
		 * if user wants to use environment passed to pmfadm,
		 * call execve and pass the environment;
		 * else call execvp, and inherit the parent's environment,
		 * plus the path set to that of the caller.
		 */
		if (handle->env_passed) {
			if (execve(cmd[0], cmd, handle->env) == -1) {
				err1 = errno;
				(void) sc_syslog_msg_initialize(&sys_handle,
				    SC_SYSLOG_PMF_PMFD_TAG, "");
				(void) sc_syslog_msg_log(sys_handle, LOG_ERR,
				    MESSAGE, "Unable to execve %s: %s",
				    cmd[0], strerror(err1));
				sc_syslog_msg_done(&sys_handle);
				_exit(1);
			}
		} else {
			if (execvp(cmd[0], cmd) == -1) {
				err1 = errno;
				(void) sc_syslog_msg_initialize(&sys_handle,
				    SC_SYSLOG_PMF_PMFD_TAG, "");
				(void) sc_syslog_msg_log(sys_handle, LOG_ERR,
				    MESSAGE, "Unable to execvp %s: %s",
				    cmd[0], strerror(err1));
				sc_syslog_msg_done(&sys_handle);
				_exit(1);
			}
		}

		/* NOTREACHED */
		exit(1);
badchild:
		/*
		 * Let our parent continue.  Note that sema_post returns
		 * an error code directly, not via errno.
		 */
		if ((err1 = sema_post(handle->sema_c2p)) != 0) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_PMF_PMFD_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * The rpc.pmfd server was not able to act on a
			 * semaphore. The message contains the system error.
			 * The server does not perform the action requested by
			 * the client, and an error message is output to
			 * syslog.
			 * @user_action
			 * Determine if the machine is running out of memory.
			 * If this is not the case, save the /var/adm/messages
			 * file. Contact your authorized Sun service provider
			 * to determine whether a workaround or patch is
			 * available.
			 */
			(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
			    "sema_post badchild: %s", strerror(err1));
			sc_syslog_msg_done(&sys_handle);
		}

		exit(1);
	}

	UNLOCK_FORK();

	/*
	 * Now we can simply wait to cleanup the zombie from our main process.
	 */
	do {
		w = waitpid(handle->pid, &err, 0);
	} while (w == -1 && errno == EINTR);

	if (handle->history)
		handle->history->wstat = err;

	if (debug)
		dbg_msgout(NOGET("after waitpid wstat=%ld "
		    "WIFEXITED=%d WEXITSTATUS=%d\n"),
		    err, WIFEXITED(err), WEXITSTATUS((uint_t)err));

	/*
	 * Wait for all sub-children to exit
	 */
	pmf_rejoin(handle);

	LOCK_HANDLE(handle);
	handle->alive = 0;
	handle->signal = 0;
	UNLOCK_HANDLE(handle);

	while (handle->condwait) {
		LOCK_HANDLE(handle);
		if (handle->condwait) {
			(void) pthread_cond_broadcast(&handle->handle_cond);
			if (debug) dbg_msgout(NOGET("broadcasting on 0x%x (%d)"
			    " (handle_cond for %s)\n"), &handle->handle_lock,
			    handle->condwait, handle->id);
		}
		UNLOCK_HANDLE(handle);
		thr_yield();
	}

	/*
	 * Save our exit time for pmf_process_scan_history to
	 * evaluate.
	 */
	if (handle->history)
		handle->history->etime = time(NULL);

	if (w == -1) {
		err1 = errno;
		handle->result.code.type = PMF_SYSERRNO;
		handle->result.code.sys_errno = errno;
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The rpc.pmfd or rpc.fed server was not able to wait for
		// a process.
		// The message contains the system error.
		// The server does not perform the action requested by the
		// client, and an error message is output to syslog.
		// @user_action
		// Save the /var/adm/messages file.
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
		    "waitpid: %s", strerror(err1));
		sc_syslog_msg_done(&sys_handle);
	}

bad:

	return (err);
}


/*
 * Routine for allocating request handle. It expects the caller to
 * hold the queue lock.
 *
 * Note that we check the parameters for the empty string "" as well as for
 * NULL because the pmf marshalling converts NULLs to empty strings.
 */
static pmf_handle *
pmf_alloc_handle(pmf_start_args *args,
security_cred_t *cred,
pmf_result *result)
{
	pmf_handle *new = NULL;
	size_t i, bufsize;
	int err;

	/*
	 * Check to make sure the identifier used is unique
	 */
	if (pmf_handle_base != NULL) {
		for (new = pmf_handle_base; ; new = new->next) {
			if (strcmp(new->id, args->identifier) == 0) {
				/*
				 * The request specified a duplicate
				 * identifier.
				 */
				result->code.type = PMF_DUP;
				return (NULL);
			}
			if (new->next == pmf_handle_base)
				break;
		}
	}

	new = (pmf_handle *) svc_alloc(sizeof (pmf_handle), &pmf_alloc_cnt);
	if (new == NULL) {
		result->code.type = PMF_SYSERRNO;
		result->code.sys_errno = ENOMEM;
		return (new);
	}
	/*
	 * Initialize all of handle to 0. This initializes the error codes
	 * in handle->result.code to 0 (success).
	 */
	(void) memset(new, 0, sizeof (pmf_handle));

	/*
	 * Add two semaphores to the handle: handle->sema_p2c and
	 * handle->sema_c2p.
	 *
	 * This pair of semaphores is used to handshake with the child process
	 * that we've created to coordinate setting up system call tracing.
	 *
	 * sema_p2c -- parent to child
	 * sema_c2p -- child to parent
	 *
	 * If we cannot allocate two interprocess semaphores, fail here.
	 */
	if ((err = pmfd_alloc_sema(&(new->sema_p2c))) != 0) {
		result->code.type = PMF_SYSERRNO;
		result->code.sys_errno = err;
		svc_free(new, &pmf_alloc_cnt);
		return (NULL);
	}

	if ((err = pmfd_alloc_sema(&(new->sema_c2p))) != 0) {
		result->code.type = PMF_SYSERRNO;
		result->code.sys_errno = err;
		svc_free(new, &pmf_alloc_cnt);
		return (NULL);
	}


	/*
	 * Create our ring queue - initial or otherwise
	 */
	if (pmf_handle_base) {
		new->next = pmf_handle_base;
		new->prev = pmf_handle_base->prev;
		new->prev->next = new;
		pmf_handle_base->prev = new;
		pmf_handle_base = new;
	} else {
		pmf_handle_base = new;
		new->next = new;
		new->prev = new;
	}

	/*
	 * Duplicate the argument list, making sure that the last element
	 * is NULL, not just empty, for exec().  We assume that the last
	 * element in the array passed to this function should be NULL,
	 * but that the rpc command might have changed it to an empty string.
	 * Thus, we copy only the strings up to (length - 1), then just assign
	 * a NULL to the final element.
	 *
	 * Calculate the bufsize ahead so we can use it in memset below.
	 */
	bufsize = (size_t)(args->cmd.cmd_len * sizeof (char *));
	new->cmd = svc_alloc(bufsize, &pmf_alloc_cnt);
	if (new->cmd == NULL) {
		goto cleanup_handle_nomem;
	}
	(void) memset(new->cmd, 0, bufsize);

	for (i = 0; i < args->cmd.cmd_len - 1; i++) {
		if (args->cmd.cmd_val[i] && *args->cmd.cmd_val[i]) {
			new->cmd[i] = svc_strdup(args->cmd.cmd_val[i],
				&pmf_alloc_cnt);
			if (new->cmd[i] == NULL) {
				goto cleanup_handle_nomem;
			}
		} else {
			/*
			 * Subsequent code cannot handle a NULL except at
			 * the end of the array.  Thus, we need to bail out
			 * here.
			 */
			new->cmd[i] = NULL;
			result->code.type = PMF_SYSERRNO;
			result->code.sys_errno = EINVAL;
			goto cleanup_handle;
		}
	}
	/*
	 * We need to end the list with a NULL pointer, for exec.
	 */
	new->cmd[i] = NULL;

	/*
	 * Duplicate the env vars list, making sure that the last element
	 * is NULL, not just empty, for exec().
	 * Calculate the bufsize ahead so we can use it in memset below.
	 * Only allocate memory for the environment if we have an environment
	 * passed to us.
	 */
	if (args->env_passed) {
		bufsize = (size_t)((args->env.env_len + 1) * sizeof (char *));
		new->env = svc_alloc(bufsize, &pmf_alloc_cnt);
		if (new->env == NULL) {
			goto cleanup_handle_nomem;
		}
		(void) memset(new->env, 0, bufsize);

		for (i = 0; i < args->env.env_len; i++) {
			if (args->env.env_val[i] && *args->env.env_val[i]) {
				new->env[i] = svc_strdup(args->env.env_val[i],
				    &pmf_alloc_cnt);
				if (new->env[i] == NULL) {
					goto cleanup_handle_nomem;
				}
			} else {
				/*
				 * Subsequent code cannot handle a NULL
				 * except in the last element.
				 */
				new->env[i] = NULL;
				result->code.type = PMF_SYSERRNO;
				result->code.sys_errno = EINVAL;
				goto cleanup_handle;
			}
		}
		/*
		 * NULL was not sent by the library, adding it now
		 */
		new->env[i] = NULL;
	}

	/*
	 * Stuff the remaining arguments into our handle.
	 */
	if (args->pwd && *args->pwd) {
		new->pwd = svc_strdup(args->pwd, &pmf_alloc_cnt);
		if (new->pwd == NULL) {
			goto cleanup_handle_nomem;
		}
	} else {
		/*
		 * Subsequent code checks for and handles a NULL ptr here.
		 */
		new->pwd = NULL;
	}

	if (args->path && *args->path) {
		new->path = svc_strdup(args->path, &pmf_alloc_cnt);
		if (new->path == NULL) {
			goto cleanup_handle_nomem;
		}
	} else {
		/*
		 * Subsequent code checks for and handles a NULL ptr here.
		 */
		new->path = NULL;
	}

	if (args->project_name && *args->project_name) {
		new->project_name = svc_strdup(args->project_name,
		    &pmf_alloc_cnt);
		if (new->project_name == NULL) {
			goto cleanup_handle_nomem;
		}
	} else {
		/*
		 * Subsequent code can handle a NULL project_name.
		 */
		new->project_name = NULL;
	}

	if (args->retries > PMFD_MAX_HISTORY) {
		new->id = NULL;
		result->code.type = PMF_SYSERRNO;
		result->code.sys_errno = E2BIG;
		goto cleanup_handle;
	}

	new->retries		= args->retries;
	if (args->identifier && *args->identifier) {
		new->id = svc_strdup(args->identifier, &pmf_alloc_cnt);
		if (new->id == NULL) {
			goto cleanup_handle_nomem;
		}
	} else {
		/*
		 * Subsequent code cannot handle a NULL here,
		 * so we must bail out.
		 */
		new->id = NULL;
		result->code.type = PMF_SYSERRNO;
		result->code.sys_errno = EINVAL;
		goto cleanup_handle;
	}
	new->action_type	= args->action_type;
	if (args->action_type == PMFACTION_EXEC) {
		if (args->action && *args->action) {
			new->action = svc_strdup(args->action, &pmf_alloc_cnt);
			if (new->action == NULL) {
				goto cleanup_handle_nomem;
			}
		} else {
			/*
			 * Subsequent code cannot handle a NULL here.
			 */
			new->action = NULL;
			result->code.type = PMF_SYSERRNO;
			result->code.sys_errno = EINVAL;
			goto cleanup_handle;
		}
	} else {
		new->action	= NULL;
	}

	new->history		= NULL;
	new->monitor_children 	= args->monitor_children;
	new->monitor_level    	= args->monitor_level;
	new->env_passed		= args->env_passed;
	new->uid 		= cred->aup_uid;
	new->gid 		= cred->aup_gid;

	/*
	 * Currently only contains an internal flag to quiet
	 * the daemon on process failure.
	 */
	new->flags		= args->flags;

	/*
	 * Period is specified in minutes by the user, and
	 * is calculated using time_t (seconds).
	 */
	if (args->period != -1)
		new->period = (time_t)(args->period);
	else
		new->period = (time_t)-1;

	(void) pthread_mutex_init(&new->handle_lock, NULL);
	(void) pthread_cond_init(&new->handle_cond, NULL);
	(void) pthread_mutex_init(&new->threads_lock, NULL);

	return (new);

cleanup_handle_nomem:
	result->code.type = PMF_SYSERRNO;
	result->code.sys_errno = ENOMEM;

cleanup_handle:
	/*
	 * something went wrong: free handle and return null;
	 * we have to release the lock because pmf_free_handle() will grab it.
	 */
	UNLOCK_QUEUE(pmf_alloc_handle);
	pmf_free_handle(new, FALSE);
	LOCK_QUEUE(pmf_alloc_handle);
	return (NULL);

}


/*
 * Routine for getting the request handle based on tag id.
 * It expects that the caller holds the queue lock.
 */
static pmf_handle *
pmf_id_to_handle(char *id)
{
	pmf_handle *ptr;

	/*
	 * Start at the base and search for the specified id
	 * It may have been removed before we got a chance to
	 * search a second time for it, so the -1 return value
	 * is not that important to notice.
	 */
	for (ptr = pmf_handle_base; ptr; ptr = ptr->next) {
		if (ptr == NULL)
			return (NULL);
		if (ptr->id != NULL && strcmp(id, ptr->id) == 0) {
			break;
		}
		if (ptr->next == pmf_handle_base) {
			ptr = NULL;
			break;
		}
	}

	return (ptr);
}

/*
 * Routine for getting the request thread based on pid.
 * It expects that the caller holds the queue lock.
 * The found thread is returned with handle_lock and threads_lock held.
 */
static pmf_threads *
pmf_pid_to_thread(pid_t pid)
{
	pmf_handle *handle;
	pmf_threads *thread;
	pmf_threads *start_thr;

	for (handle = pmf_handle_base; handle; handle = handle->next) {
		LOCK_HANDLE(handle);
		LOCK_THREADS(handle);
		start_thr = handle->threads;
		for (thread = start_thr; thread; thread = thread->next) {
			if (thread->pid == pid) {
				return (thread);
			}
			if (thread->next == start_thr)
				break;
		}
		UNLOCK_THREADS(handle);
		UNLOCK_HANDLE(handle);

		if (handle->next == pmf_handle_base)
			break;
	}

	return (NULL);
}

/*
 * is_process_monitored
 *
 * Wrapper for pmf_pid_to_thread that handles the locking.
 * Should be called without any pmf structure locks held.
 */
boolean_t
is_process_monitored(pid_t pid)
{
	pmf_threads *temp_thr;
	LOCK_QUEUE(is_process_monitored);
	temp_thr = pmf_pid_to_thread(pid);
	UNLOCK_QUEUE(is_process_monitored);
	if (temp_thr != NULL) {
		UNLOCK_THREADS(temp_thr->handle);
		UNLOCK_HANDLE(temp_thr->handle);
		if (debug) {
			dbg_msgout(NOGET(
			    "Process %d is monitored.\n"), pid);
		}

		return (B_TRUE);
	}
	return (B_FALSE);
}

/*
 * pmf_free_handle
 *
 * Routine for freeing request handle. It expects that the caller
 * DOESN'T hold the queue lock.
 * This routine should only be called from two places:
 * the end of start_process, and from pmf_alloc_handle, if the allocation
 * partly fails.
 * Otherwise, locking needs to be enhanced to allow
 * other threads to free a handle without stepping on us.
 */
static void
pmf_free_handle(pmf_handle *handle, bool_t detach_thread)
{
	int err;
	char **ptr;
	sc_syslog_msg_handle_t sys_handle;

	LOCK_QUEUE(pmf_free_handle);
	LOCK_HANDLE(handle);

	/*
	 * Unlink us from the ring.
	 *
	 * Make sure we leave a valid handle in
	 * the base pointer.
	 */
	if (handle == pmf_handle_base) {
		pmf_handle_base = handle->next;
	}

	/*
	 * Take us out of the ring
	 */
	handle->next->prev = handle->prev;
	handle->prev->next = handle->next;

	/*
	 * Make sure nobody uses any left over garbage
	 */
	handle->prev = handle->next = NULL;

	/*
	 * If we were the only one left, then pmf_handle_base
	 * is still pointing to us.  Null it out.
	 */
	if (handle == pmf_handle_base) {
		pmf_handle_base = NULL;
	}

	/*
	 * broadcast the condition variable in case there are any waiters
	 * in pmf_timedwait
	 */
	if (debug) dbg_msgout(NOGET("broadcasting on queue_cond\n"));
	(void) pthread_cond_broadcast(&pmf_queue_cond);

	/*
	 * Now, free up any internal strings/structures
	 * that we previously malloc'd and free the
	 * handle.
	 */
	if (handle->cmd) {
		ptr = handle->cmd;
		while (*ptr)
			svc_free(*ptr++, &pmf_alloc_cnt);
		svc_free(handle->cmd, &pmf_alloc_cnt);
	}

	if (handle->env) {
		ptr = handle->env;
		while (*ptr) {
			svc_free(*ptr++, &pmf_alloc_cnt);
		}
		svc_free(handle->env, &pmf_alloc_cnt);
	}
	if (handle->pwd)
		svc_free((void *)handle->pwd, &pmf_alloc_cnt);
	if (handle->path)
		svc_free((void *)handle->path, &pmf_alloc_cnt);
	if (handle->id)
		svc_free((void *)handle->id, &pmf_alloc_cnt);
	if (handle->action)
		svc_free((void *)handle->action, &pmf_alloc_cnt);
	if (handle->project_name)
		svc_free((void *)handle->project_name, &pmf_alloc_cnt);
	if (handle->history)
		pmf_free_process_history(&handle->history);

	/*
	 * Free the two semaphores.
	 */
	pmfd_free_sema(handle->sema_p2c);
	pmfd_free_sema(handle->sema_c2p);

	UNLOCK_HANDLE(handle);
	UNLOCK_QUEUE(pmf_free_handle);

	/* XXX force a context switch - shouldn't need to XXX */
	thr_yield();

	/*
	 * We acquire these locks before destroying them
	 */
	LOCK_QUEUE(pmf_free_handle);
	LOCK_HANDLE(handle);
	LOCK_THREADS(handle);

	(void) pthread_mutex_destroy(&handle->handle_lock);
	(void) pthread_mutex_destroy(&handle->threads_lock);
	(void) pthread_cond_destroy(&handle->handle_cond);
	if (debug) dbg_msgout(NOGET("destroyed 0x%x\n"),
		&handle->handle_cond);

	/*
	 * The thread that originally created us is gone.
	 * Since there's nobody to join us we simply detach
	 * so we're cleaned up properly on exit.
	 *
	 * However, it's possible that the start command may not
	 * have spawned the new thread yet, so we don't need to detach.
	 * Thus, check the detach_thread flag first.
	 */
	if (detach_thread && (err = pthread_detach(pthread_self())) != 0) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
		    "pthread_detach: %s", strerror(err));
		sc_syslog_msg_done(&sys_handle);
	} else {
		pmf_thread_cnt--;
	}

	/*
	 * Now free the handle
	 */
	svc_free((void *)handle, &pmf_alloc_cnt);

	UNLOCK_QUEUE(pmf_free_handle);
}



/*
 * What follows are our RPC entry points.
 *
 * Here's our "start" entry ... grab a new handle for the
 * request, and spawn a thread to exec and monitor the process.
 * args and cred are input args; result is an output arg.
 */
void
pmf_start_svc(pmf_start_args *args, pmf_result *result,
	security_cred_t *cred)
{
	pmf_handle *handle;

	/*
	 * Make sure the queue doesn't change underneath us.
	 */
	LOCK_QUEUE(pmf_alloc_handle);
	if ((handle = pmf_alloc_handle(args, cred, result)) == NULL) {
		UNLOCK_QUEUE(pmf_alloc_handle);
		return;
	}

	if (pthread_create(&handle->tid, NULL,
	    (void *(*)(void *))start_process, handle) != 0) {
		result->code.type = PMF_SYSERRNO;
		result->code.sys_errno = errno;
		/*
		 * We can free our handle here, since a thread
		 * was never started (Re: comment in start_process).
		 */
		UNLOCK_QUEUE(pmf_alloc_handle);
		pmf_free_handle(handle, FALSE);
		LOCK_QUEUE(pmf_alloc_handle);
	} else {
		pmf_thread_cnt++;
	}
	UNLOCK_QUEUE(pmf_alloc_handle);
}


#define	PID_LEN	40


/* ARGSUSED */
/*
 * args and cred are input args; result is an output arg.
 */
void
pmf_status_svc(pmf_args *args, pmf_status_result *result,
	security_cred_t *cred)
{

	char	tmp_buf[PID_LEN];	/* temp buf to get size of each pid */
	char	*ptr;		/* ptr to pid_buf */
	char    *uptr;		/* ptr to upid_buf */
	char **aptr;
	pmf_handle *handle;
	pmf_threads *thread;
	uint_t i;
	uint_t buf_len;
	uint_t ubuf_len;

	LOCK_QUEUE(pmf_status_svc);

	(void) memset(result, 0, sizeof (pmf_status_result));

	if ((handle = pmf_id_to_handle(args->identifier)) == NULL) {

		/*
		 * case PMF_STATUS_ALL:
		 * show status of all tags
		 */
		if (args->query_all) {

			if ((handle = pmf_handle_base) == NULL) {
				UNLOCK_QUEUE(pmf_status_svc);
				return;
			}

			/*
			 * calculate size of buf
			 */
			buf_len = 0;
			do {

				/*
				 * print only tags belonging to user
				 * or if user is root print all tags
				 */
				if (cred->aup_uid == 0 ||
				    (handle->uid == cred->aup_uid &&
				    handle->gid == cred->aup_gid))
					buf_len += (uint_t)strlen(handle->id)
					    + 1; /* 1 is for the space */
				handle = handle->next;
			} while (handle != pmf_handle_base);

			/*
			 * malloc buf of correct size
			 */
			result->identifier = malloc((size_t)(buf_len + 1));
			if (result->identifier == NULL) {
				result->code.type = PMF_SYSERRNO;
				result->code.sys_errno = ENOMEM;
				UNLOCK_QUEUE(pmf_status_svc);
				return;
			}

			/*
			 * copy tags into buf
			 */
			(void) memset(result->identifier, 0,
			    (size_t)(buf_len + 1));
			handle = pmf_handle_base;
			do {
				if (cred->aup_uid == 0 ||
				    (handle->uid == cred->aup_uid &&
				    handle->gid == cred->aup_gid)) {
					(void) strcat(result->identifier,
					    handle->id);
					(void) strcat(result->identifier, " ");
				}
				handle = handle->next;
			} while (handle != pmf_handle_base);
			(void) strcat(result->identifier, "\0");

		} else if (args->identifier[0] == '\0') {
			/*
			 * unadvertised debugging option
			 */

			result->code.type	= PMF_OKAY;

			/*
			 * exit, if pmfd is not monitoring any processes
			 */
			if (pmf_handle_base == NULL) {
				UNLOCK_QUEUE(pmf_status_svc);
				return;
			}

			/*
			 * calculate size of buf
			 */
			buf_len = 0;
			handle = pmf_handle_base;
			do {
				buf_len += (uint_t)strlen(handle->id) + 1;
					/* 1 is for the space */
				handle = handle->next;
			} while (handle != pmf_handle_base);

			/*
			 * malloc buf of correct size
			 */
			result->identifier = malloc((size_t)buf_len + 1);
			if (result->identifier == NULL) {
				result->code.type = PMF_SYSERRNO;
				result->code.sys_errno = ENOMEM;
				UNLOCK_QUEUE(pmf_status_svc);
				return;
			}
			(void) memset(result->identifier, 0,
			    (size_t)buf_len + 1);

			for (handle = pmf_handle_base; handle; ) {
				(void) strcat(result->identifier, handle->id);
				(void) strcat(result->identifier, " ");
				if ((handle = handle->next) == pmf_handle_base)
					break;
			}
		} else if (strcmp(args->identifier, "all") == 0) {
			/*
			 * unadvertised debugging option
			 */

			result->code.type = PMF_SYSERRNO;
			result->code.sys_errno = ENOENT;

			for (handle = pmf_handle_base; handle; ) {

				dbg_msgout(NOGET("%s:"), handle->id);
				for (aptr = handle->cmd; aptr && *aptr; aptr++)
					dbg_msgout(NOGET(" %s"), *aptr);
				dbg_msgout(NOGET(
				    "\tretries=%d period=%d action=%d\n"),
				    handle->retries, handle->period,
				    handle->action_type);
				dbg_msgout(NOGET("threads\n"));
				LOCK_THREADS(handle);
				for (thread = handle->threads; thread;
				    thread = thread->next) {
					dbg_msgout(NOGET(
					    "\tpid=%d running\n"),
					    thread->pid);
					if (thread->next == handle->threads)
						break;
				}
				UNLOCK_THREADS(handle);
				if ((handle = handle->next) == pmf_handle_base)
					break;
			}

			dbg_msgout(NOGET("pmf_alloc_cnt = %d\n"),
			    pmf_alloc_cnt);
			dbg_msgout(NOGET("pmf_thread_cnt = %d\n"),
			    pmf_thread_cnt);
		} else {

			result->code.type = PMF_SYSERRNO;
			result->code.sys_errno = ENOENT;

		}
	} else {
		result->code.type	= PMF_OKAY;

		aptr = handle->cmd;
		for (i = 0; *aptr++; i++)
			;
		result->cmd.cmd_val = malloc(i * sizeof (char *));
		if (result->cmd.cmd_val == NULL) {
			/*
			 * Here we haven't successfully malloced anything
			 * before so there is no need to call pmf_free_status
			 * to free it.
			 */
			result->code.type = PMF_SYSERRNO;
			result->code.sys_errno = ENOMEM;
			UNLOCK_QUEUE(pmf_status_svc);
			return;
		}
		result->cmd.cmd_len = i;

		for (aptr = handle->cmd, i = 0; aptr && *aptr; aptr++, i++) {
			result->cmd.cmd_val[i] = strdup(*aptr);
			if (result->cmd.cmd_val[i] == NULL) {
				/*
				 * Here we have to call pmf_free_status to
				 * set the error and free what was
				 * previously malloced.
				 */
				pmf_free_status(result);
				UNLOCK_QUEUE(pmf_status_svc);
				return;
			}
		}

		/*
		 * handle->env might be null if there are no environment
		 * variables.
		 */
		if (handle->env != NULL) {
			/* We have at least one environment variable */
			aptr = handle->env;
			for (i = 0; *aptr++; i++)
				;
			result->env.env_val = malloc(i * sizeof (char *));
			if (result->env.env_val == NULL) {
				/*
				 * Here we have to call pmf_free_status to
				 * set the error and free what was
				 * previously malloced.
				 */
				pmf_free_status(result);
				UNLOCK_QUEUE(pmf_status_svc);
				return;
			}
			result->env.env_len = i;

			for (aptr = handle->env, i = 0; aptr && *aptr;
			    aptr++, i++) {
				result->env.env_val[i] = strdup(*aptr);
				if (result->env.env_val[i] == NULL) {
					/*
					 * Here we have to call
					 * pmf_free_status to
					 * set the error and free what was
					 * previously malloced.
					 */
					pmf_free_status(result);
					UNLOCK_QUEUE(pmf_status_svc);
					return;
				}
			}
		} else {
			/* we have no environment vars */
			result->env.env_len = 0;
			result->env.env_val = NULL;
		}

		result->retries		= handle->retries;
		result->period		= handle->period;
		if (handle->id) {
			result->identifier = strdup(handle->id);
			if (result->identifier == NULL) {
				/*
				 * Here we have to call pmf_free_status to
				 * set the error and free what was
				 * previously malloced.
				 */
				pmf_free_status(result);
				UNLOCK_QUEUE(pmf_status_svc);
				return;
			}
		}

		result->action_type	= handle->action_type;
		if (handle->action) {
			result->action	= strdup(handle->action);
			if (result->action == NULL) {
				/*
				 * Here we have to call pmf_free_status to
				 * set the error and free what was
				 * previously malloced.
				 */
				pmf_free_status(result);
				UNLOCK_QUEUE(pmf_status_svc);
				return;
			}
		}
		result->nretries	= (int)pmf_process_scan_history(handle);
		result->owner		= handle->uid;
		result->monitor_children = handle->monitor_children;
		result->monitor_level    = handle->monitor_level;

		if (handle->project_name) {
			result->project_name = strdup(handle->project_name);
			if (result->project_name == NULL) {
				/*
				 * Here we have to call pmf_free_status to
				 * set the error and free what was
				 * previously malloced.
				 */
				pmf_free_status(result);
				UNLOCK_QUEUE(pmf_status_svc);
				return;
			}
		}

		/*
		 * if threads is null we're done;
		 * In this situation we're not calling pmf_free_status()
		 * because it might be normal for the threads list to be empty.
		 * That can happen in the following case:
		 * pmfd runs the user-specified action after the monitored
		 * process exited. If the restart occurs too frequently
		 * (less than 1 min apart), pmfd waits for 30 sec. before
		 * restarting. During this time there are no threads running,
		 * so the list of threads maintained by the handle is null.
		 */

		LOCK_THREADS(handle);
		if ((thread = handle->threads) == NULL) {
			UNLOCK_THREADS(handle);
			UNLOCK_QUEUE(pmf_status_svc);
			return;
		}

		/*
		 * calculate size of buf
		 */
		buf_len = 0;
		ubuf_len = 0;
		do {
			(void) memset(tmp_buf, 0, sizeof (tmp_buf));
			(void) sprintf(tmp_buf, "%d", thread->pid);
			if (PMF_IS_MONITORED(thread))
				/* 1 is for the space */
				buf_len += (uint_t)strlen(tmp_buf) + 1;
			else
				ubuf_len += (uint_t)strlen(tmp_buf) + 1;
			thread = thread->next;

		} while (thread != handle->threads);

		/*
		 * malloc buf of correct size
		 */
		result->pids = malloc((size_t)(buf_len + 1));
		if (result->pids == NULL) {
			pmf_free_status(result);
			UNLOCK_THREADS(handle);
			UNLOCK_QUEUE(pmf_status_svc);
			return;
		}
		(void) memset(result->pids, 0, (size_t)(buf_len + 1));

		result->upids = malloc((size_t)(ubuf_len + 1));
		if (result->upids == NULL) {
			pmf_free_status(result);
			UNLOCK_THREADS(handle);
			UNLOCK_QUEUE(pmf_status_svc);
			return;
		}
		(void) memset(result->upids, 0, (size_t)(ubuf_len + 1));

		/*
		 * copy pids into buf
		 */
		if ((thread = handle->threads) != NULL) {
			ptr = result->pids;
			uptr = result->upids;
			if (PMF_IS_MONITORED(handle->threads)) {
				(void) sprintf(ptr, "%d", handle->threads->pid);
				ptr += strlen(ptr);
			} else {
				(void) sprintf(uptr, "%d",
				    handle->threads->pid);
				uptr += strlen(uptr);
			}
			thread = thread->next;
			while (thread != handle->threads) {
				if (PMF_IS_MONITORED(thread)) {
					(void) sprintf(ptr, " %d", thread->pid);
					ptr += strlen(ptr);
				} else {
					(void) sprintf(uptr, " %d",
					    thread->pid);
					uptr += strlen(uptr);
				}
				thread = thread->next;
			}
			*ptr = '\0';
			*uptr = '\0';
		}

		UNLOCK_THREADS(handle);

	}

	UNLOCK_QUEUE(pmf_status_svc);

}

/*
 * pmf_stop_svc
 *
 * Called as result of user executing pmfadm -s
 * Check that the given tag has no unmonitored process.
 * Turns off monitoring of the specified tag by setting the stop_monitor
 * flag of the handle to 1, and setting the action_type variable to
 * PMFACTION_NULL.  If the user specified that a signal should be sent to the
 * processes, send the signal.  If the user specified to wait until all
 * processes finish, call pmf_timedwait to wait for processes to exit and the
 * tag to be removed from the queue.
 */
void
pmf_stop_svc(pmf_args *args, pmf_result *result,
	security_cred_t *cred)
{
	pmf_handle *handle;
	pmf_threads *tptr;
	int err;

	(void) memset(result, 0, sizeof (pmf_result));

	LOCK_QUEUE(pmf_stop_svc);

	if ((handle = pmf_id_to_handle(args->identifier)) == NULL) {
		result->code.type = PMF_SYSERRNO;
		result->code.sys_errno = ENOENT;
		UNLOCK_QUEUE(pmf_stop_svc);
		return;
	} else {
		LOCK_HANDLE(handle);
		UNLOCK_QUEUE(pmf_stop_svc);

		/*
		 * Check to make sure we're either root, or
		 * the same user that started the request.
		 */
		if ((cred->aup_uid != handle->uid) && (cred->aup_uid != 0)) {
			result->code.type = PMF_SYSERRNO;
			result->code.sys_errno = EACCES;
			UNLOCK_HANDLE(handle);
			return;
		} else {
			LOCK_THREADS(handle);
			/*
			 * First we check that the tag to stop has no
			 * unmonitored process.
			 */
			for (tptr = handle->threads; tptr; tptr = tptr->next) {
				if (!PMF_IS_MONITORED(tptr)) {
					sc_syslog_msg_handle_t sys_handle;

					(void) sc_syslog_msg_initialize(
					    &sys_handle,
					    SC_SYSLOG_PMF_PMFD_TAG, "");
					/*
					 * SCMSGS
					 * @explanation
					 * The command 'pmfadm -s' can not be
					 * executed on the given tag because
					 * the monitoring is suspended on the
					 * indicated pid.
					 * @user_action
					 * Resume the monitoring on the
					 * indicated pid with the 'pmfctl -R'
					 * command.
					 */
					(void) sc_syslog_msg_log(sys_handle,
					    LOG_ERR, MESSAGE,
					    "\"pmfadm -s\": "
					    "Can not stop <%s>: Monitoring "
					    "is not resumed on pid %d",
					    handle->id, tptr->pid);
					sc_syslog_msg_done(&sys_handle);
					result->code.type = PMF_SYSERRNO;
					result->code.sys_errno = EBUSY;
					UNLOCK_THREADS(handle);
					UNLOCK_HANDLE(handle);
					return;
				}
				if (tptr->next == handle->threads)
					break;
			}
			/*
			 * We still need to wait for our child to return.
			 * We clear the action field so that when it does
			 * exit we will know to discontinue restarting it.
			 */
			if (handle->action_type != PMFACTION_NULL) {
				handle->action_type = PMFACTION_NULL;
			}

			/*
			 * Set flag stop_monitor so we know not to restart.
			 * Signal the condition var in case the monitoring
			 * thread is in the "throttle wait" state.
			 */
			handle->stop_monitor = TRUE;
			(void) pthread_cond_broadcast(&handle->handle_cond);
			if (debug) {
				dbg_msgout(NOGET("pmf_stop_svc: broadcast\n"));
			}

			/*
			 * Have we been asked to send a signal to all
			 * processes as well?
			 */
			handle->signal = args->signal;
			if (args->signal != 0) {
				for (tptr = handle->threads; tptr;
				    tptr = tptr->next) {
					if (debug) dbg_msgout(NOGET(
					    "KILL -%d %d\n"),
					    args->signal, (int)tptr->pid);
					/*
					 * If kill fails with ESRCH, we ignore
					 * the error because the process has
					 * exited while we walk the list.
					 * Otherwise, print message, set result
					 * and return.
					 */
					if (kill(tptr->pid, args->signal)) {
						int err1 = errno;
						sc_syslog_msg_handle_t
						    sys_handle;

						(void) sc_syslog_msg_initialize(
						    &sys_handle,
						    SC_SYSLOG_PMF_PMFD_TAG, "");
						/*
						 * SCMSGS
						 * @explanation
						 * An error occured while
						 * rpc.pmfd attempted to send a
						 * signal to one of the
						 * processes of the given tag.
						 * The reason for the failure
						 * is also given. The signal
						 * was sent as a result of a
						 * 'pmfadm -s' command.
						 * @user_action
						 * Save the
						 * /var/adm/messages file.
						 * Contact your authorized Sun
						 * service provider to
						 * determine whether a
						 * workaround or patch is
						 * available.
						 */
						(void) sc_syslog_msg_log(
						    sys_handle, err1 == ESRCH ?
						    LOG_NOTICE : LOG_ERR,
						    MESSAGE,
						    "\"pmfadm -s\": "
						    "Error signaling <%s>: %s",
						    handle->id, strerror(err1));
						sc_syslog_msg_done(&sys_handle);

						if (err1 != ESRCH) {
							result->code.type
							    = PMF_SYSERRNO;
							result->code.sys_errno
							    = err1;
							UNLOCK_THREADS(handle);
							UNLOCK_HANDLE(handle);
							return;
						}
					}
					if (tptr->next == handle->threads)
						break;
				}
			}
			UNLOCK_THREADS(handle);
		}
	}

	/*
	 * Have we been asked to wait until all threads exit?
	 * If so, wait ...
	 */
	if (args->timewait) {
		if (debug)
			dbg_msgout(NOGET("Waiting for processes to finish.\n"));
		if ((err = pmf_timedwait(handle, args->timewait, TRUE)) != 0) {
			result->code.type = PMF_SYSERRNO;
			result->code.sys_errno = err;
		}
	} else {
		/*
		 * pmf_timedwait unlocks the handle. If we don't
		 * call pmf_timedwait, unlock the handle here.
		 */
		UNLOCK_HANDLE(handle);
	}
}

void
pmf_modify_svc(pmf_modify_args *args, pmf_result *result,
	security_cred_t *cred)
{
	pmf_handle *handle;

	(void) memset(result, 0, sizeof (pmf_result));

	if (args->retries > PMFD_MAX_HISTORY) {
		result->code.type = PMF_SYSERRNO;
		result->code.sys_errno = E2BIG;
		return;
	}

	LOCK_QUEUE(pmf_modify_svc);

	if ((handle = pmf_id_to_handle(args->identifier)) == NULL) {
		result->code.type = PMF_SYSERRNO;
		result->code.sys_errno = ENOENT;
		UNLOCK_QUEUE(pmf_modify_svc);
	} else {
		LOCK_HANDLE(handle);
		UNLOCK_QUEUE(pmf_modify_svc);
		if ((cred->aup_uid != handle->uid) && (cred->aup_uid != 0)) {
			result->code.type = PMF_SYSERRNO;
			result->code.sys_errno = EACCES;
		} else {
			if (args->retries_modified)
				handle->retries = args->retries;
			if (args->period_modified)
				handle->period = args->period;

			/*
			 * Clear history of earlier failures. Keep only
			 * current run, if it hasn't ended yet.
			 */
			if ((args->retries_modified ||
			    args->period_modified) &&
			    (handle->history != NULL)) {
				if (handle->history->etime == 0)
					pmf_free_process_history(
					    &handle->history->next);
				else
					pmf_free_process_history(
					    &handle->history);
			}
		}
		UNLOCK_HANDLE(handle);
	}
}

void
pmf_kill_svc(pmf_args *args, pmf_result *result,
	security_cred_t *cred)
{
	pmf_handle *handle;
	pmf_threads *tptr;
	int err;

	(void) memset(result, 0, sizeof (pmf_result));

	LOCK_QUEUE(pmf_kill_svc);

	if ((handle = pmf_id_to_handle(args->identifier)) == NULL) {
		result->code.type = PMF_SYSERRNO;
		result->code.sys_errno = ENOENT;
		UNLOCK_QUEUE(pmf_kill_svc);
	} else {
		LOCK_HANDLE(handle);
		UNLOCK_QUEUE(pmf_kill_svc);

		/*
		 * Our waiting threads will cleanup from
		 * any exiting children.
		 *
		 * Check to make sure we're either root, or
		 * the same user that started the request.
		 *
		 * Kill off any subprocesses that we've
		 * been monitoring.
		 */
		if ((cred->aup_uid != handle->uid) && (cred->aup_uid != 0)) {
			result->code.type = PMF_SYSERRNO;
			result->code.sys_errno = EACCES;
		} else {
			LOCK_THREADS(handle);
			/*
			 * First we check that the tag to kill has no
			 * unmonitored process.
			 */
			for (tptr = handle->threads; tptr; tptr = tptr->next) {
				if (!PMF_IS_MONITORED(tptr)) {
					sc_syslog_msg_handle_t sys_handle;

					(void) sc_syslog_msg_initialize(
					    &sys_handle,
					    SC_SYSLOG_PMF_PMFD_TAG, "");
					/*
					 * SCMSGS
					 * @explanation
					 * The command 'pmfadm -k' can not be
					 * executed on the given tag because
					 * the monitoring is suspended on the
					 * indicated pid.
					 * @user_action
					 * Resume the monitoring on the
					 * indicated pid with the 'pmfctl -R'
					 * command.
					 */
					(void) sc_syslog_msg_log(sys_handle,
					    LOG_ERR, MESSAGE,
					    "\"pmfadm -k\": "
					    "Can not signal <%s>: Monitoring "
					    "is not resumed on pid %d",
					    handle->id, tptr->pid);
					sc_syslog_msg_done(&sys_handle);
					result->code.type = PMF_SYSERRNO;
					result->code.sys_errno = EBUSY;
					UNLOCK_THREADS(handle);
					UNLOCK_HANDLE(handle);
					return;
				}
				if (tptr->next == handle->threads)
					break;
			}
			handle->signal = args->signal;
			for (tptr = handle->threads; tptr; tptr = tptr->next) {
				if (debug) dbg_msgout(NOGET("KILL -%d %d\n"),
				    args->signal, (int)tptr->pid);
				/*
				 * If kill fails with ESRCH, we ignore the error
				 * because the process has exited while we walk
				 * the list.
				 * Otherwise, print message, set result
				 * and return.
				 */
				if (kill(tptr->pid, args->signal)) {
					int err1 = errno;
					sc_syslog_msg_handle_t sys_handle;

					(void) sc_syslog_msg_initialize(
					    &sys_handle,
					    SC_SYSLOG_PMF_PMFD_TAG, "");
					/*
					 * SCMSGS
					 * @explanation
					 * An error occured while rpc.pmfd
					 * attempted to send a signal to one
					 * of the processes of the given tag.
					 * The reason for the failure is also
					 * given. The signal was sent as a
					 * result of a 'pmfadm -k' command.
					 * @user_action
					 * Save the /var/adm/messages file.
					 * Contact your authorized Sun service
					 * provider to determine whether a
					 * workaround or patch is available.
					 */
					(void) sc_syslog_msg_log(sys_handle,
					    err1 == ESRCH ? LOG_NOTICE :
					    LOG_ERR, MESSAGE,
					    "\"pmfadm -k\": "
					    "Error signaling <%s>: %s",
					    handle->id, strerror(err1));
					sc_syslog_msg_done(&sys_handle);
					if (err1 != ESRCH) {
						result->code.type =
						    PMF_SYSERRNO;
						result->code.sys_errno = err1;
						UNLOCK_THREADS(handle);
						UNLOCK_HANDLE(handle);
						return;
					}
				}
				if (tptr->next == handle->threads)
					break;
			}
			UNLOCK_THREADS(handle);
		}

		/*
		 * Have we been asked to wait until all threads exit?
		 * If so, wait ...
		 */
		if (args->timewait) {
			/*
			 * Pass the value of stop_monitor as the third arg.
			 * This argument specifies whether pmf_timedwait()
			 * should wait for the handle to be removed from the
			 * queue as well as waiting for all processes to
			 * finish. If stop_monitor is set, then pmf won't
			 * restart the process, so this thread should wait
			 * for it to be removed entirely from the handle queue.
			 */
			if ((err = pmf_timedwait(handle, args->timewait,
			    handle->stop_monitor)) != 0) {
				result->code.type = PMF_SYSERRNO;
				result->code.sys_errno = err;
			}
		} else {
			/*
			 * pmf_timedwait unlocks the handle. If we don't
			 * call pmf_timedwait, unlock the handle here.
			 */
			UNLOCK_HANDLE(handle);
		}
	}
}

/*
 * pmf_suspend_svc
 *
 * Called as result of user executing pmfctl -S
 * Check that the monitoring thread state is PMF_MONITORED otherwise
 * return EBUSY. Set the state to PMF_SUSPENDING and signal the monitoring
 * thread with SIGUSR1. Wait for the state to change from PMF_SUSPENDING.
 * Check that the state PMF_UNMONITORED has been reached otherwise return
 * EAGAIN
 */
void
pmf_suspend_svc(pmf_args *args, pmf_result *result,
	security_cred_t *cred)
{
	pmf_handle *handle;
	pmf_threads *thread;
	int pid;
	int err;
	struct timespec timeout;

	(void) memset(result, 0, sizeof (pmf_result));

	/*
	 * PID that we want to suspend is stored in signal arg
	 */
	pid = atoi(args->identifier);

	LOCK_QUEUE(pmf_suspend_svc);

	if ((thread = pmf_pid_to_thread(pid)) == NULL) {
		result->code.type = PMF_SYSERRNO;
		result->code.sys_errno = ESRCH;
		UNLOCK_QUEUE(pmf_suspend_svc);
	} else {
		/* handle_lock and threads_lock are held */
		handle = thread->handle;
		UNLOCK_QUEUE(pmf_suspend_svc);

		/*
		 * Make sure user has access to suspend this process.
		 */
		if ((cred->aup_uid != handle->uid) && (cred->aup_uid != 0)) {
			result->code.type = PMF_SYSERRNO;
			result->code.sys_errno = EACCES;
		} else {
			(void) pthread_mutex_lock(&thread->lock);

			/*
			 * Check if the monitoring can be suspended
			 */
			if (thread->monitor_state != PMF_MONITORED) {
				(void) pthread_mutex_unlock(&thread->lock);
				result->code.type = PMF_SYSERRNO;
				result->code.sys_errno = EBUSY;
				UNLOCK_THREADS(handle);
				UNLOCK_HANDLE(handle);
				return;
			}

			result->code.type = PMF_OKAY;

			thread->monitor_state = PMF_SUSPENDING;

			while (thread->monitor_state == PMF_SUSPENDING) {
				err = pthread_kill(thread->tid, SIGUSR1);
				if (err != 0) {
					sc_syslog_msg_handle_t sys_handle;
					(void) sc_syslog_msg_initialize(
					    &sys_handle,
					    SC_SYSLOG_PMF_PMFD_TAG, "");
					/*
					 * SCMSGS
					 * @explanation
					 * An error occured while rpc.pmfd
					 * attempted to suspend the monitoring
					 * of the indicated pid, possibly
					 * because the indicated pid has
					 * exited while attempting to suspend
					 * its monitoring.
					 * @user_action
					 * Check if the indicated pid has
					 * exited, if this is not the case,
					 * Save the syslog messages file.
					 * Contact your authorized Sun service
					 * provider to determine whether a
					 * workaround or patch is available.
					 */
					(void) sc_syslog_msg_log(sys_handle,
					    LOG_ERR, MESSAGE,
					    "\"pmfctl -S\": "
					    "Error suspending pid %d "
					    "for tag <%s>: %d",
					    thread->pid, handle->id, err);
					sc_syslog_msg_done(&sys_handle);
					thread->monitor_state = PMF_MONITORED;
					result->code.type = PMF_SYSERRNO;
					result->code.sys_errno = err;
					break;
				}
				timeout.tv_sec = time(NULL) +
				    PMF_REQUEST_NOTIFY_DELAY;
				timeout.tv_nsec = 0;
				(void) pthread_cond_timedwait(&thread->cv,
				    &thread->lock, &timeout);
			}

			if (thread->monitor_state != PMF_UNMONITORED) {
				/*
				 * the state may not be PMF_UNMONITORED if we
				 * were unable to suspend the monitoring or
				 * the monitoring is already being resumed with
				 * the -R option
				 */
				result->code.type = PMF_SYSERRNO;
				result->code.sys_errno = EAGAIN;
			}

			(void) pthread_mutex_unlock(&thread->lock);

		}
		UNLOCK_THREADS(handle);
		UNLOCK_HANDLE(handle);
	}
}

/*
 * pmf_resume_svc
 *
 * Called as result of user executing pmfctl -R
 * Check that the monitoring thread state is PMF_UNMONITORED otherwise
 * return EBUSY. Set the state to PMF_RESUMING and signal the monitoring
 * thread with SIGUSR1. Wait for the state to change from PMF_RESUMING.
 * Check that the state PMF_MONITORED has been reached otherwise return
 * EAGAIN
 */
void
pmf_resume_svc(pmf_args *args, pmf_result *result,
	security_cred_t *cred)
{
	pmf_handle *handle;
	pmf_threads *thread;
	int pid;
	int err;
	struct timespec timeout;

	(void) memset(result, 0, sizeof (pmf_result));

	/*
	 * PID that we want to resume is stored in signal arg
	 */
	pid = atoi(args->identifier);

	LOCK_QUEUE(pmf_suspend_svc);

	if ((thread = pmf_pid_to_thread(pid)) == NULL) {
		result->code.type = PMF_SYSERRNO;
		result->code.sys_errno = ESRCH;
		UNLOCK_QUEUE(pmf_resume_svc);
	} else {
		/* handle_lock and threads_lock are held */
		handle = thread->handle;
		UNLOCK_QUEUE(pmf_suspend_svc);

		/*
		 * Make sure user has access to suspend this process.
		 */
		if ((cred->aup_uid != handle->uid) && (cred->aup_uid != 0)) {
			result->code.type = PMF_SYSERRNO;
			result->code.sys_errno = EACCES;
		} else {
			(void) pthread_mutex_lock(&thread->lock);

			/*
			 * Check if the monitoring can be resumed
			 */
			if (thread->monitor_state != PMF_UNMONITORED) {
				(void) pthread_mutex_unlock(&thread->lock);
				result->code.type = PMF_SYSERRNO;
				result->code.sys_errno = EBUSY;
				UNLOCK_THREADS(handle);
				UNLOCK_HANDLE(handle);
				return;
			}

			result->code.type = PMF_OKAY;

			thread->monitor_state = PMF_RESUMING;

			while (thread->monitor_state == PMF_RESUMING) {
				err = pthread_kill(thread->tid, SIGUSR1);
				if (err != 0) {
					sc_syslog_msg_handle_t sys_handle;
					(void) sc_syslog_msg_initialize(
					    &sys_handle,
					    SC_SYSLOG_PMF_PMFD_TAG, "");
					/*
					 * SCMSGS
					 * @explanation
					 * An error occured while rpc.pmfd
					 * attempted to resume the monitoring
					 * of the indicated pid, possibly
					 * because the indicated pid has
					 * exited while attempting to resume
					 * its monitoring.
					 * @user_action
					 * Check if the indicated pid has
					 * exited, if this is not the case,
					 * Save the syslog messages file.
					 * Contact your authorized Sun service
					 * provider to determine whether a
					 * workaround or patch is available.
					 */
					(void) sc_syslog_msg_log(sys_handle,
					    LOG_ERR, MESSAGE,
					    "\"pmfctl -R\": "
					    "Error resuming pid %d "
					    "for tag <%s>: %d",
					    thread->pid, handle->id, err);
					sc_syslog_msg_done(&sys_handle);
					thread->monitor_state = PMF_UNMONITORED;
					result->code.type = PMF_SYSERRNO;
					result->code.sys_errno = err;
					break;
				}
				timeout.tv_sec = time(NULL) +
				    PMF_REQUEST_NOTIFY_DELAY;
				timeout.tv_nsec = 0;
				(void) pthread_cond_timedwait(&thread->cv,
				    &thread->lock, &timeout);
			}

			if (thread->monitor_state != PMF_MONITORED) {
				/*
				 * the state may not be PMF_MONITORED if we
				 * were unable to suspend the monitoring or
				 * the monitoring is already being resumed with
				 * the -S option
				 */
				result->code.type = PMF_SYSERRNO;
				result->code.sys_errno = EAGAIN;
			}

			(void) pthread_mutex_unlock(&thread->lock);
		}
		UNLOCK_THREADS(handle);
		UNLOCK_HANDLE(handle);
	}
}

/*
 * pmf_timedwait
 *
 * This function has to be called with the handle_lock held.
 * It returns with the handle_lock *not* held
 *
 * seconds is the number of seconds to wait.
 *
 * If wait_for_queue is false, this function waits on the handle->handle_cond
 * condition variable for the condition that handle->alive is 0, or until
 * we've waited seconds seconds, whichever comes first.
 *
 * If wait_for_queue is true, this function waits on the pmf_queue_cond
 * condition variable for the condition that the handle is no longer in
 * the queue.
 *
 * If seconds is -1, then this function will wait forever for the
 * handle->alive flag to be 0.
 */
static int
pmf_timedwait(pmf_handle *handle, int seconds, bool_t wait_for_queue)
{
	timestruc_t timeout;
	int err = 0;
	pmf_handle *lookedup_handle = handle;
	char *tag_id = NULL;

	/*
	 * assert handle lock held
	 */

	/*
	 * Wait the specified time, or until all threads exit.
	 */
	timeout.tv_sec = time(NULL) + seconds;
	timeout.tv_nsec = 0;

	if (debug) dbg_msgout(NOGET(
	    "entering timedwait on tag %s for %d seconds "
	    "(condwait=%d, wait_for_queue=%d)\n"), handle->id,
	    seconds, handle->condwait, wait_for_queue);

	if (wait_for_queue) {
		/*
		 * Grab a deep copy of the tag id because the id in the
		 * handle will be freed when the handle is removed from the
		 * queue (the condition for which we're waiting).
		 */
		tag_id = strdup(handle->id);
		if (tag_id == NULL) {
			UNLOCK_HANDLE(handle);
			return (ENOMEM);
		}

		/*
		 * Release the handle lock before acquiring the queue lock,
		 * in order to avoid potential deadlocks from out-of-order
		 * lock acquisition.
		 */
		UNLOCK_HANDLE(handle);
		LOCK_QUEUE();
		/*
		 * Wait until we can't find the handle anymore.
		 * It's possible that the handle with this id could have
		 * been removed from the queue and then another (different)
		 * one with the same id added back in before this thread
		 * got a chance to run. Thus, we need to check not only for
		 * the handle being NULL (not there), but also that the
		 * handle, if it is there, is not the same ptr as the handle
		 * we expect. It's still theoretically possible that the
		 * new handle with the same tag got allocated in the exact
		 * same memory as the old one, but this seems highly
		 * unlikely and not worth worrying about.
		 */
		if (debug) dbg_msgout(NOGET("About to wait for queue\n"));
		while (((lookedup_handle = pmf_id_to_handle(tag_id))
		    != NULL) && (lookedup_handle == handle)) {
			/*
			 * If seconds is -1, we're supposed to wait forever,
			 * so just use a normal pthread_cond_wait.
			 *
			 * If seconds is not -1, we are to wait for the
			 * condition and the timeout, so use a
			 * pthread_cond_timedwait.
			 */
			if (seconds == -1) {
				err = pthread_cond_wait(&pmf_queue_cond,
				    &pmf_queue_lock);
			} else {
				err = pthread_cond_timedwait(
				    &pmf_queue_cond, &pmf_queue_lock,
				    &timeout);
			}
			/* We break for any error, not just ETIMEDOUT */
			if (err != 0) {
				break;
			}
		}
		free(tag_id);
		if (debug) dbg_msgout(
		    NOGET("timedwait on 0x%x (queue_cond) returned %d\n"),
		    &pmf_queue_cond, err);

		/*
		 * pthred_cond_*wait returns with the lock held, so release
		 * it here.
		 */
		UNLOCK_QUEUE();
	} else {

		/* pre-check our condition */
		if (handle->alive == 0) {
			UNLOCK_HANDLE(handle);
			return (err);
		}

		/*
		 * Wait for the alive flag to go away.  We increment the
		 * condwait counter so the monitoring thread knows to
		 * broadcast on the condition var.
		 */
		if (debug) dbg_msgout(NOGET("About to wait for handle\n"));
		handle->condwait++;
		while (handle->alive) {
			/*
			 * If seconds is -1, we're supposed to wait forever,
			 * so just use a normal pthread_cond_wait.
			 *
			 * If seconds is not -1, we are to wait for the
			 * condition and the timeout, so use a
			 * pthread_cond_timedwait.
			 */
			if (seconds == -1) {
				err = pthread_cond_wait(&handle->handle_cond,
				    &handle->handle_lock);
			} else {
				err = pthread_cond_timedwait(
				    &handle->handle_cond, &handle->handle_lock,
				    &timeout);
			}
			/* We break for any error, not just ETIMEDOUT */
			if (err != 0) {
				break;
			}
		}
		handle->condwait--;
		if (debug) dbg_msgout(
		    NOGET("timedwait on 0x%x (handle_cond) returned %d (%d)\n"),
		    &handle->handle_cond, err, handle->condwait);

		UNLOCK_HANDLE(handle);
	}

	/*
	 * ETIMEDOUT prints a confusing message:
	 *
	 *	"Connection timed out"
	 *
	 * We'll convert it to ETIME to make it clearer:
	 *
	 *	"timer expired"
	 *
	 */
	if (err == ETIMEDOUT)
		err = ETIME;

	return (err);
}


int
pmf_action_exec(pmf_handle *handle, PMF_ACTION_WHY why)
{
	char **cmd;
	uint_t nc, i;
	char *p1 = NULL;
	char *p2 = NULL;
	char *action = NULL;
	char *sav_ptr = NULL;
	int result;
	char *lasts;

	/*
	 * If there is no action program specified, exit as if
	 * we've failed.
	 */
	if (handle->action == NULL || *handle->action == NULL)
		return (-1);

	/*
	 * only PMF_ACTION_FAILED requires handling
	 */
	if (why != PMF_ACTION_FAILED)
		return (-1);

	p1 = handle->action;

	/* skip leading spaces */
	while (p1 && (*p1 != '\0') && (isspace(*p1)))
		p1++;

	if (p1 == NULL || *p1 == '\0')
		return (-1);

	/* remember to allocate an extra byte for the \0 */
	action = (char *)malloc((1 + strlen(p1)) * sizeof (char));
	if (action == (char *)NULL)
		return (-1);

	sav_ptr = action; /* Save start of action in sav_ptr */
	nc = (uint_t)strlen(p1);
	for (i = 0; i < nc && (*p1 != '\0'); i++) {
		/* Non white-space character - simply copy */
		if (!isspace(*p1)) {
			*action++ = *p1++;
			continue;
		}

		/*
		 * If next character is also white-space character or NUL,
		 * skip
		 */
		if (isspace(*(p1+1)) || (*(p1+1) == '\0')) {
			p1++;
		} else {
			/*
			 * Next character is non white-space - so put
			 * just one
			 */
			/* "space" character in action as a delimiter */
			*action++ = ' ';
			p1++;
		}
	}

	*action = '\0'; /* terminate */

	p1 = action = sav_ptr; /* restore to start */
	nc = 0;
	/* count the number of components separated by space */
	while ((p2 = strchr(p1, ' ')) != NULL) {
		nc++;
		p1 = p2+1;
	}

	cmd = (char **)malloc((nc+4) * sizeof (char *));
	if (cmd == (char **)NULL)
		return (-1);

	i = 1;
	if (nc == 0) /* just a single component action command */
		cmd[0] = action;
	else { /* multiple component command separated by spaces */
		cmd[0] = strdup(strtok_r(action, " ", &lasts));
		p1 = action+strlen(cmd[0])+1;
		for (; i < nc; i++) {
			cmd[i] = strdup(strtok_r(NULL, " ", &lasts));
			p1 = p1+strlen(cmd[i])+1;
		}
		cmd[i++] = strdup(p1); /* last component */
	}

	cmd[i++] = strdup(NOGET("failed"));
	cmd[i++] = strdup(handle->id);
	cmd[i] = NULL;

	if (debug) dbg_msgout("running \"");
	for (i = 0; i < (nc + 4); i++) {
		if (cmd[i])
			if (debug) dbg_msgout("%s ", cmd[i]);
	}
	if (debug) dbg_msgout("\"\n");
	result = pmf_run_process(handle, cmd);

	/*
	 * In cases where there is just a single component action
	 * command, cmd[0] and action point to the same memory.
	 * To make sure we don't free the memory twice, check
	 * that action is not equal to cmd[0].
	 */
	if (action && action != cmd[0])
		free(action);
	for (i = 0; i < (nc + 4); i++) {
		if (cmd[i])
			free(cmd[i]);
	}

	if (cmd)
		free(cmd);

	return (result);
}
