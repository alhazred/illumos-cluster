/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _PMFD_MONITOR_H
#define	_PMFD_MONITOR_H

#pragma ident	"@(#)pmfd_monitor.h	1.3	08/05/20 SMI"

/*
 * pmfd_monitor.h  routines associated to the minitoring of 1 process tree
 */

#ifdef __cplusplus
extern "C" {
#endif

#include "pmfd.h"

extern void pmf_register_initial_process(pmf_handle *h, int pid, int ppid);
extern void pmf_loop_consume_events_lock_held(struct pmf_handle *handle);
extern int pmf_throttle_loop(struct pmf_handle *handle, time_t req);

#ifdef __cplusplus
}
#endif

#endif	/* _PMFD_MONITOR_H */
