/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _PMFD_EVENT_POOL_H
#define	_PMFD_EVENT_POOL_H

#pragma ident	"@(#)pmfd_event_pool.h	1.3	08/05/20 SMI"

/*
 * pmf_event_pool.h  internal pmf events management
 */

#ifdef __cplusplus
extern "C" {
#endif

#include <rgm/rpc/pmf.h>
#include <pthread.h>

#ifdef LOCK_DEBUG
#define	LOCK_EVENT(e) \
	if (debug) dbg_msgout(NOGET("%s/%d LOCK EVENT (0x%x)\n"), \
	__FILE__, __LINE__, &((e)->lock)); \
	(void) CL_PANIC(pthread_mutex_lock(&((e)->lock)) == 0);
#define	UNLOCK_EVENT(e) \
	if (debug) dbg_msgout(NOGET("%s/%d UNLOCK EVENT (0x%x)\n"), \
	__FILE__, __LINE__, &((e)->lock)); \
	(void) CL_PANIC(pthread_mutex_unlock(&((e)->lock)) == 0);
#else
#define	LOCK_EVENT(e) CL_PANIC(pthread_mutex_lock(&((e)->lock)) == 0);
#define	UNLOCK_EVENT(e) CL_PANIC(pthread_mutex_unlock(&((e)->lock)) \
	== 0);
#endif

/*
 * Communication between threads is based on events transmitted through user
 * defined event queues.
 * Events are preallocated by chunks and are stored within a pool. If no more
 * events are available at a given time a new chunk is allocated within the
 * pool.
 *
 * Events live-cycle
 * =================
 * Two kinds of life-cycles are implemented:
 *
 * o Anonymous events: No thread owns an event. The event could be reused
 * as soon as it is returned to the pool. This kind of event is well suited
 * to internal asynchronous operations, like  Waiting thread  dispatching
 * events. An example of such usage is given below:
 *
 * - Event producer gets an event from the pool (get_pmf_event_from_pool)
 * - Event producer sets event parameters
 * - Event producer pushes event on the user defined queue (queue_pmf_event)
 * - Event producer forgets about event ...
 *         - Event consumer gets event from queue (dequeue_pmf_event)
 *         - Event consumer performs actions associated to event
 *         - Event consumer returns event to pool (return_pmf_event_to_pool)
 * ...
 *
 * < Event within the pool is owned by pool and can be reused>
 *
 * ...
 *
 *
 * o Owned events: one thread owns an event. When being returned to pool,
 * the event should have been released by its owner before being reused.
 * This kind of event is well suited to synchronous operations, like
 * implementation of API (RPC services) provided by PMF. An example of such
 * usage is given below:
 *
 * - Event producer gets an event from the pool (get_pmf_event_from_pool)
 * - Event producer acquires ownership of event (acquire_pmf_event)
 * - Event producer sets event parameters
 * - Event producer acquires event lock
 * - Event producer pushes event on a queue (queue_pmf_event)
 * - Event producer waits on event condition protected by event lock ...
 *	   - Event consumer gets event from queue (dequeue_pmf_event)
 *	   - Event consumer performs required actions and sets return code
 *	   - Event consumer gets event lock
 *	   - Event consumer signals condition
 *	   - Event consumer releases event lock
 *	   - Event consumer returns event to pool (return_pmf_event_to_pool)
 * ...
 *
 * < Event within the pool is owned by producer and cannot be reused >
 *
 * ...
 *
 * - Event producer exits from condition_wait with event lock held
 * - Event producer gets event return code
 * - Event producer releases event lock
 * - Event producer releases ownership of event (release_pmf_event)
 * ...
 *
 * < Event within the pool is owned by pool and can be reused >
 *
 * ...
 *
 */

struct pmf_event {
	struct pmf_event *next;
	int type;
	int pid;
	int ppid;
	int level;
	int status;
	int signal;
	pthread_cond_t cond;
	pthread_mutex_t lock;
	pthread_t owner;
	struct pmf_error return_code;
	char **cmd;
};

struct pmf_event_queue {
	pthread_mutex_t lock;
	pthread_cond_t cond;
	struct pmf_event *head;
};

#define	PMFD_NOP 		0
#define	PMFD_CHILD		1
#define	PMFD_UNREGISTERED_CHILD	2
#define	PMFD_RESUME		3
#define	PMFD_SUSPEND		4
#define	PMFD_STOP		5
#define	PMFD_CMD		6
#define	PMFD_MODIFY		7
#define	PMFD_KILL		8
#define	PMFD_EXIT		9

#define	PMF_EVENT_POOL_SIZE 	256
#define	PMF_EVENT_QUEUE_INITIALIZER	{PTHREAD_MUTEX_INITIALIZER,\
					PTHREAD_COND_INITIALIZER,\
					(struct pmf_event *)0}

extern struct pmf_event *get_pmf_event_from_pool(void);
extern void return_pmf_event_to_pool(struct pmf_event **evt);
extern int acquire_pmf_event(struct pmf_event *evt);
extern int release_pmf_event(struct pmf_event **evt);
extern void queue_pmf_event(struct pmf_event_queue *queue,
    struct pmf_event *evt);
extern struct pmf_event *dequeue_pmf_event(struct pmf_event_queue *queue);

#ifdef __cplusplus
}
#endif

#endif	/* _PMFD_EVENT_POOL_H */
