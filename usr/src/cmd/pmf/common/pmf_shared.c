/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)pmf_shared.c	1.4	08/05/20 SMI"

#include "pmfd.h"
#include <sys/sc_syslog_msg.h>
#include <sys/rsrc_tag.h>
#include <sys/mman.h>

/*
 * Allocate a semaphore in mapped memory to be shared between this process
 * and a child process.
 */
int
pmfd_alloc_sema(sema_t **sema)
{
	int err;
	sema_t *ptr;
	sc_syslog_msg_handle_t sys_handle;

	/*
	 * Initialize the semaphore to NULL in case we fail to initialize
	 * it in the code below.
	 */
	*sema = NULL;

	/*
	 * The interprocess semaphores need to reside in
	 * a shared mapping between the two processes.
	 */
	if ((ptr = (sema_t *)mmap(0, sizeof (sema_t), PROT_READ|PROT_WRITE,
	    MAP_SHARED|MAP_ANON, -1, NULL)) == MAP_FAILED) {
		/*
		 * lint doesn't understand errno
		 */
		err = errno; /*lint !e746 */
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * The rpc.pmfd server was not able to allocate shared memory
		 * for a semaphore, possibly due to low memory, and the system
		 * error is shown. The server does not perform the action
		 * requested by the client, and pmfadm returns error. An error
		 * message is also output to syslog.
		 * @user_action
		 * Determine if the machine is running out of memory. If this
		 * is not the case, save the /var/adm/messages file. Contact
		 * your authorized Sun service provider to determine whether a
		 * workaround or patch is available.
		 */
		(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
		    "mmap: %s", strerror(err));
		sc_syslog_msg_done(&sys_handle);
		return (err);
	}

	/*
	 * Note that sema_init returns an error code directly, not via
	 * errno.
	 */
	if ((err = sema_init(ptr, 0, USYNC_PROCESS, NULL)) != 0) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * The rpc.pmfd server was not able to initialize a semaphore,
		 * possibly due to low memory, and the system error is shown.
		 * The server does not perform the action requested by the
		 * client, and pmfadm returns error. An error message is also
		 * output to syslog.
		 * @user_action
		 * Determine if the machine is running out of memory. If this
		 * is not the case, save the /var/adm/messages file. Contact
		 * your authorized Sun service provider to determine whether a
		 * workaround or patch is available.
		 */
		(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
		    "sema_init: %s", strerror(err));
		sc_syslog_msg_done(&sys_handle);
		(void) munmap((caddr_t)ptr, sizeof (sema_t));
		return (err);
	}

	*sema = ptr;

	return (0);
}

void
pmfd_free_sema(sema_t *sema)
{
	sc_syslog_msg_handle_t sys_handle;
	int	err;

	if (sema) {
		/*
		 * Destroy the semaphore, but ignore the return value.
		 * There's nothing we can do if it fails.
		 */
		(void) sema_destroy(sema);
		if (munmap((caddr_t)sema, sizeof (sema_t)) == -1) {
			err = errno;
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_PMF_PMFD_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * The rpc.pmfd server was not able to delete shared
			 * memory for a semaphore, possibly due to low memory,
			 * and the system error is shown. This is part of the
			 * cleanup after a client call, so the operation might
			 * have gone through. An error message is output to
			 * syslog.
			 * @user_action
			 * Determine if the machine is running out of memory.
			 * If this is not the case, save the /var/adm/messages
			 * file. Contact your authorized Sun service provider
			 * to determine whether a workaround or patch is
			 * available.
			 */
			(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
			    "munmap: %s", strerror(err));
			sc_syslog_msg_done(&sys_handle);
		}
	}
}

/*
 * Routine for freeing malloced fields of status result.
 * Status result itself is not malloced, so it's not freed.
 * Used only if a malloc or strdup went wrong; otherwise fields are freed by RPC
 * on the server side; they are realloced by RPC on the client side
 * and freed in pmfadm.c by function free_rpc_args().
 */
void
pmf_free_status(pmf_status_result *result)
{
	uint_t j;

	result->code.type = PMF_SYSERRNO;
	result->code.sys_errno = ENOMEM;

	for (j = 0; j < result->cmd.cmd_len; j++) {
		if (result->cmd.cmd_val[j])
			free(result->cmd.cmd_val[j]);
	}
	if (result->cmd.cmd_val)
		free(result->cmd.cmd_val);
	for (j = 0; j < result->env.env_len; j++) {
		if (result->env.env_val[j])
			free(result->env.env_val[j]);
	}
	if (result->env.env_val)
		free(result->env.env_val);
	if (result->identifier)
		free(result->identifier);
	if (result->action)
		free(result->action);
	if (result->pids)
		free(result->pids);
	if (result->upids)
		free(result->upids);
	if (result->project_name)
		free(result->project_name);
}

#if SOL_VERSION >= __s9

/*
 * Solaris 9 (8.1) uses fdwalk here to set
 * close on exec on open file descriptors (>= 3) only.
 * See PSARC/2000/208.
 */

static int
close_on_exec_callback(void *unused, int fd)
{
	(void) unused;
	if (fd >= 3) {
		(void) fcntl(fd, F_SETFD, FD_CLOEXEC);
	}
	return (0);
}

void
close_on_exec(void)
{
	(void) fdwalk(close_on_exec_callback, NULL);
}

#else /* case SOL_VERSION < __s9 || linux */

void
close_on_exec(void)
{
	int err;
	sc_syslog_msg_handle_t sys_handle;
	int i;

	/*
	 * Set close on exec so open files are not inherited.
	 * Ignore EBADF error that indicates file is not open.
	 * Don't close file descriptors 0, 1 and 2 (stdin,
	 * stdout and stderr).
	 */
	for (i = getdtablesize() - 1; i >= 3; i--) {
		if (fcntl(i, F_SETFD, FD_CLOEXEC) == -1) {
			if (errno != EBADF) {
				err = errno;
				(void) sc_syslog_msg_initialize(
				    &sys_handle,
				    SC_SYSLOG_PMF_PMFD_TAG, "");
				(void) sc_syslog_msg_log(sys_handle,
				    LOG_ERR, MESSAGE,
				    "fcntl: %s", strerror(err));
				sc_syslog_msg_done(&sys_handle);
			}
		}
	}
}

#endif /* SOL_VERSION >= __s9 */
