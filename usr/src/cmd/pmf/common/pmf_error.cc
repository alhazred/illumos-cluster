//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)pmf_error.cc	1.4	08/05/20 SMI"

#include "pmf_error.h"

#include <errno.h>
#include <stdio.h>
#include <unistd.h>
#include <stdarg.h>
#include <string.h>
#include <syslog.h>
#include <pthread.h>
#include <sys/syslog.h>
#include <sys/sc_syslog_msg.h>
#include "pmfd.h"

#define	PMF_DBG_BUFSIZE (64 * 1024) // 64KB ring buffer for tracing

dbg_print_buf pmf_dbg_buf(PMF_DBG_BUFSIZE); // ring buffer for tracing

#define	PMF_BUFSIZ	256

//
// Routine for tracing/logging debug info.
// Tracing is always on. Logging (into syslog and the console) is turned on
// only if Debugflag is turned on.
//
void
debug_print(char *fmt, ...)
{
	uint_t	len = 0;
	char	str1[PMF_BUFSIZ], fmt1[PMF_BUFSIZ];
	pthread_t tid = pthread_self();
	va_list args;

	(void) snprintf(fmt1, PMF_BUFSIZ, "[%u] %s", tid, fmt);

	// Add newline if there isn't in 'fmt'.
	len = (uint_t)strlen(fmt1);
	if (fmt1[len - 1] != '\n' && len < PMF_BUFSIZ - 1) {
		fmt1[len] = '\n';
		fmt1[len + 1] = '\0';
	}

	va_start(args, fmt);	//lint !e40
	if (vsnprintf(str1, PMF_BUFSIZ, fmt1, args) >= PMF_BUFSIZ) {
		str1[PMF_BUFSIZ - 1] = '\0';
	}
	va_end(args);

	pmf_dbg_buf.dbprintf(str1);

	if (debug) {
		// syslog and write to stderr -- maybe overkill
		os::sc_syslog_msg logger(SC_SYSLOG_PMF_PMFD_TAG, "", NULL);
		logger.log(LOG_ERR, MESSAGE, str1);
		(void) fprintf(stderr, str1);
	}
}
