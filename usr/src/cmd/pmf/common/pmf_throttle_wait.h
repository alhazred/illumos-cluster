//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef _PMF_THROTTLE_WAIT_H
#define	_PMF_THROTTLE_WAIT_H

#pragma ident	"@(#)pmf_throttle_wait.h	1.6	08/07/23 SMI"

#include "pmf_contract.h"

#include <queue>

#include <libclcontract/smart_ptr.h>

//
// timeout_entry is just a glorified struct, so all methods are public.
// It stores a smart_ptr to a pmf_contract and the time at which the timeout
// should complete.
//
class timeout_entry {
public:
	timeout_entry(smart_ptr<cl_contract> sppc_in, time_t time_in);
	time_t restart_time;
	smart_ptr<cl_contract> sppc;

	// Need operator> for using the priority_queue.
	// Note that we use operator> instead of operator< so that
	// the lowest entries are first in the queue instead of the highest.

	// CSTYLED
	friend bool operator>(const timeout_entry& lhs,
	    const timeout_entry& rhs);
};

class pmf_throttle_wait {
public:
	static pmf_throttle_wait& instance();
	static void create();

	// The only public method is wait(). It tells the pmf_throttle_wait
	// module to "timeout" the pmf_contract pointed to by sppc for
	// time seconds.
	int wait(smart_ptr<cl_contract> sppc, time_t time);

	// timeout_waits is only public so that the timeout thread
	// can call it
	void timeout_waits();

protected:
	pmf_throttle_wait();

	// Don't provide implementations for these
	pmf_throttle_wait(const pmf_throttle_wait& src);
	pmf_throttle_wait& operator=(const pmf_throttle_wait& rhs);

	pthread_t wait_thread;

	std::priority_queue<timeout_entry, std::vector<timeout_entry>,
	    std::greater<timeout_entry> > pq;

	pthread_mutex_t lock;
	pthread_cond_t cond;

	static pmf_throttle_wait* the_throttler;
	static pthread_mutex_t create_lock;
};

#endif /* _PMF_THROTTLE_WAIT_H */
