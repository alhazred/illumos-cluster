/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)pmfd_dispatch.c	1.3	08/05/20 SMI"

/*
 * pmfd_dispatch.c - main dispatching routines for the Linux pmf daemon
 */

#include <rgm/rpc/pmf.h>
#include "pmfd.h"
#include <sys/ptrace.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <sys/wait.h>
#include <errno.h>
#include <fcntl.h>
#include <pthread.h>
#include <rpc/rpc.h>
#include <rpc/pmap_clnt.h>
#include <semaphore.h>
#include <sys/os_compat.h>
#include <sys/sc_syslog_msg.h>
#include <sys/rsrc_tag.h>
#include <rgm/rpc_services.h>

#define	PMF_WAIT4_OPTION __WALL

/*
 * CL_PANIC: We are not able to maintain a coherent internal state. Some
 * monitored processes may remain STOPPED even after PMF daemon has exited.
 * Consequently we exit directly, relying on Failfast to reboot the node.
 */
#define	PMFD_DISPATCH_PANIC(h)	CL_PANIC(0)

#ifdef LOCK_DEBUG
#define	LOCK_QUEUE(name) \
	if (debug) dbg_msgout(NOGET("%s/%d LOCK QUEUE (0x%x)\n"), \
	NOGET(#name), __LINE__, &pmf_handle_list.lock); \
	CL_PANIC(pthread_mutex_lock(&pmf_handle_list.lock) == 0);
#define	UNLOCK_QUEUE(name) \
	if (debug) dbg_msgout(NOGET("%s/%d UNLOCK QUEUE (0x%x)\n"), \
	NOGET(#name), __LINE__, &pmf_handle_list.lock); \
	CL_PANIC(pthread_mutex_unlock(&pmf_handle_list.lock) == 0);
#define	LOCK_GARBAGE(name) \
	if (debug) dbg_msgout(NOGET("%s/%d LOCK GARBAGE (0x%x)\n"), \
	NOGET(#name), __LINE__, &garbage_queue.lock); \
	CL_PANIC(pthread_mutex_lock(&garbage_queue.lock) == 0);
#define	UNLOCK_GARBAGE(name) \
	if (debug) dbg_msgout(NOGET("%s/%d UNLOCK GARBAGE (0x%x)\n"), \
	NOGET(#name), __LINE__, &garbage_queue.lock); \
	CL_PANIC(pthread_mutex_unlock(&garbage_queue.lock) == 0);
#define	LOCK_RPC(name) \
	if (debug) dbg_msgout(NOGET("%s/%d LOCK RPC (0x%x)\n"), \
	NOGET(#name), __LINE__, &rpc_status_lock); \
	CL_PANIC(pthread_mutex_lock(&rpc_startup_lock) == 0);
#define	UNLOCK_RPC(name) \
	if (debug) dbg_msgout(NOGET("%s/%d UNLOCK RPC (0x%x)\n"), \
	NOGET(#name), __LINE__, &rpc_status_lock); \
	CL_PANIC(pthread_mutex_unlock(&rpc_startup_lock) == 0);
#else
#define	LOCK_QUEUE(h) \
	CL_PANIC(pthread_mutex_lock(&pmf_handle_list.lock) == 0);
#define	UNLOCK_QUEUE(h) \
	CL_PANIC(pthread_mutex_unlock(&pmf_handle_list.lock) == 0);
#define	LOCK_GARBAGE(h) \
	CL_PANIC(pthread_mutex_lock(&garbage_queue.lock) == 0);
#define	UNLOCK_GARBAGE(h) \
	CL_PANIC(pthread_mutex_unlock(&garbage_queue.lock) == 0);
#define	LOCK_RPC(h) \
	CL_PANIC(pthread_mutex_lock(&rpc_startup_lock) == 0);
#define	UNLOCK_RPC(h) \
	CL_PANIC(pthread_mutex_unlock(&rpc_startup_lock) == 0);
#endif /* LOCK_DEBUG */

extern int debug;
extern int pmf_thread_cnt;
extern void pmf_program_2(struct svc_req *rqstp, register SVCXPRT * transp);
extern int failfast_arm(char *);

/*
 * Utilities
 */
static void display_monitored_list(void);
static int getppidbypid(struct proc_buffer h, int pid);

/*
 * Events dispatching
 */
static int pmf_dispatch_monitor_event(struct proc_buffer buf, int pid,
    int status);

/*
 * Thread main routines
 */
static void pmf_garbage_collector(void *param);
static void pmf_loop_monitor_children(void *param);
static void pmf_rpc_server(void *param);
static void pmf_terminator(void *param);

/*
 * Exported routines
 */
void pmf_awake_dispatching_thread(void);
void pmf_sig_handler(int signal);
int pmf_ptrace_monitor_startup(void);
int pmf_ptrace_monitor_join(void);
void pmf_send_event_to_GC(struct pmf_event *event);

struct pmf_handle_queue pmf_handle_list = { PTHREAD_MUTEX_INITIALIZER,
	PTHREAD_COND_INITIALIZER, (struct pmf_handle *)0, 0, 0
};

static sem_t dispatching_sem;
static sem_t termination_sem;

static int pmf_is_stopped = 0;

/*
 * Internal threads
 */
static pthread_t dispatching_thread;
static pthread_t termination_thread;
static pthread_t rpc_server_thread;

/*
 * We must synchronize with RPC thread at startup in order to
 * be sure that RPC service registration was successful and
 * that Failfast was armed.
 */
static int rpc_startup_rc = 0;
static pthread_mutex_t rpc_startup_lock = PTHREAD_MUTEX_INITIALIZER;
static pthread_cond_t rpc_startup_cv = PTHREAD_COND_INITIALIZER;

/*
 * When a process event is detected by the dispatching thread and is targetting
 * a stopped handle, then event is redirected to garbage queue.
 * This can happen if a direct child exit after monitoring has been stopped.
 */
static struct pmf_event_queue garbage_queue = PMF_EVENT_QUEUE_INITIALIZER;

/*
 * Buffer used by dispatching thread for reading
 * within /proc
 */
static struct proc_buffer waiting_loop_buffer;

/*
 * Used for debugging purpose to display handle tree states.
 */
static void
display_monitored_list(void)
{

	struct pmf_handle *handle;
	struct child_handle *p;
	int i = 0;

	LOCK_QUEUE(display_monitored_list);

	dbg_msgout(NOGET("Monitored tree list contains %d "
	    "item(s)\n"),
	    pmf_handle_list.count);

	for (handle = pmf_handle_list.head; handle; handle = handle->next) {
		dbg_msgout(NOGET("\tHandle id %s - "
		    "pid/level/reg/state\n"),
		    handle->id);
		i++;

		LOCK_HANDLE(handle);
		for (p = handle->head; p; p = p->next) {
			dbg_msgout(NOGET("\t\t\t\t%d/%ld/0x%x/%d\n"),
			    p->pid, p->level, p->regmask, p->monitor_state);
		}
		UNLOCK_HANDLE(handle);
	}

	if (i != pmf_handle_list.count) {
		dbg_msgout(NOGET("List item count = %d "
		    "list item walked = %d"),
		    pmf_handle_list.count, i);
	}

	UNLOCK_QUEUE(display_monitored_list);
}

/*
 * Get parent process id of a process using /proc
 * This is used when pid of a process generating
 * an event is not found when walking the handle
 * list. This happen when parent has not already
 * registered its new child at fork.
 * This happen also when a direct child that we
 * are no more monitoring is exiting. As this is
 * a direct child we get a notification of its
 * exit status (wait4), but when dispatching
 * thread get event, the process does not exist
 * anymore.
 */
static int
getppidbypid(struct proc_buffer h, int pid)
{

	int ppid = 0;
	int fd;
	char *p;
	int err;
	sc_syslog_msg_handle_t sys_handle;

	if (!h.buf) {
		return (0);
	}

	if (snprintf(h.buf, PMF_PROC_BUFFER_SIZE, "/proc/%d/stat", pid)
	    >= PMF_PROC_BUFFER_SIZE) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
		    "getppidbypid: path \"/proc/%d/stat\" trunked.",
		    pid);
		sc_syslog_msg_done(&sys_handle);
		return (0);
	}

	if ((fd = open(h.buf, O_RDONLY)) < 0) {
		err = errno;
		/*
		 * This is the expected behavior if process was exiting
		 * and was no more monitored.
		 */
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		(void) sc_syslog_msg_log(sys_handle, LOG_INFO, MESSAGE,
		    "getppidbypid: open \"%s\": %s",
		    h.buf, strerror(err));
		sc_syslog_msg_done(&sys_handle);
		return (0);
	}

	if (read(fd, h.buf, PMF_PROC_BUFFER_SIZE) <= 0) {
		err = errno;
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		(void) sc_syslog_msg_log(sys_handle, LOG_INFO, MESSAGE,
		    "getppidbypid: read \"%s\": %s",
		    h.buf, strerror(err));
		sc_syslog_msg_done(&sys_handle);
		(void) close(fd);
		return (0);
	}

	p = strrchr(h.buf, ')');
	if (!p || p < h.buf + 4) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
		    "getppidbypid: parsing of \"%s\" failure "
		    "when looking at \")\"",
		    h.buf);
		(void) close(fd);
		return (0);
	}

	if (sscanf(p + 4, "%d ", &ppid) != 1) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
		    "getppidbypid: failure to convert \"%s\" to ppid",
		    p);
		(void) close(fd);
		return (0);
	}

	if (debug)
		dbg_msgout(NOGET("got ppid=%d "
		    "for pid=%d from /proc\n"),
		    ppid, pid);

	close(fd);
	return (ppid);
}

void
pmf_send_event_to_GC(struct pmf_event *event)
{
	LOCK_GARBAGE(pmf_send_event_to_GC);
	(void) queue_pmf_event(&garbage_queue, event);
	(void) pthread_cond_signal(&garbage_queue.cond);
	UNLOCK_GARBAGE(pmf_send_event_to_GC);
}

static int
pmf_dispatch_monitor_event(struct proc_buffer buf, int pid, int status)
{

	struct pmf_handle *handle;
	struct child_handle *p = (struct child_handle *)0;
	struct pmf_event *event;
	int ppid = 0;

	/*
	 * Create a new event
	 */
	if ((event = get_pmf_event_from_pool()) == (struct pmf_event *)0) {
		return (ENOMEM);
	}
	event->type = PMFD_CHILD;
	event->pid = pid;
	event->status = status;

	/*
	 * Search for PMF handle the pid is refering to
	 */
	LOCK_QUEUE(pmf_dispatch_monitor_event);
	for (handle = pmf_handle_list.head; handle; handle = handle->next) {
		LOCK_HANDLE(handle);
		if (p = pid2handle(handle, pid))
			break;
		UNLOCK_HANDLE(handle);
	}

	if (!p) {

		/*
		 * Process with pid is not registered. This is a new process
		 * that is starting before parent process has had a chance to
		 * register it.
		 * As we do not held any handle lock at this time, it may happen
		 * that fork event concurrently with this thread. This mean that
		 * when searching for ppid we may find that process as already
		 * be reparented to init process (ppid = 1).
		 * In this case parent process must have been registered while
		 * we were walking. We then must walk it again an searching
		 *
		 * before parent process has had a chance to register it.
		 * we must get parent process id in order to know to which
		 * tree it is belonging. We will read /proc/<pid>/stat in
		 * order to get this information. Note that at this stage
		 * the parent process must point the real parent process
		 * and may not point to init (ppid = 1):
		 *	process is stopped just after its creation, it could not
		 *	yet have forked itself.
		 *	process was also stopped at fork by kernel and will be
		 *	restarted after PMF have registered this new child so
		 *	it may not have exited.
		 */

		ppid = getppidbypid(buf, pid);

		if (debug) {
			dbg_msgout(NOGET("Dispatching thread "
			    "Event on pid=%d status=%d 0x%x no "
			    "handle found, ppid=%d\n"),
			    event->pid, status, status, ppid);
		}

		if (ppid > 0) {
			/*
			 * Search for PMF handle the pid or the ppid is
			 * refering to. We search on both because pid
			 * may have been registered by the associated
			 * monitoring thread since the last walk we
			 * performed few lines above. In this case pid
			 * come first in the list.
			 */
			for (handle = pmf_handle_list.head; handle;
			    handle = handle->next) {
				LOCK_HANDLE(handle);
				if (p = pidppid2handle(handle, pid, ppid))
					break;
				UNLOCK_HANDLE(handle);
			}

			if (p && p->pid == ppid) {
				event->type = PMFD_UNREGISTERED_CHILD;
				event->ppid = ppid;
				event->level = p->level + 1;
				if (debug) {
					dbg_msgout(NOGET("Dispatching thread "
					    "event on pid=%d status=%d 0x%x "
					    "parent handle %s found "
					    "ppid=%d\n"),
					    event->pid, status,
					    status,
					    handle->id,
					    event->ppid);
				}
			} else {
				if (debug) {
					dbg_msgout(NOGET("Dispatching thread "
					    "event on pid=%d status=%d 0x%x "
					    "handle not found\n"),
					    event->pid, status, status);
				}
			}
		} /* else p remains null */
	}

	/*
	 * We can release the global lock:
	 * If the tree handle was found we hold the lock on it
	 */
	UNLOCK_QUEUE(pmf_dispatch_monitor_event);

	if (!p) {

		/*
		 * we fail to get pid AND ppid -> redirect event to GC
		 */
		if (debug)
			dbg_msgout(NOGET("Dispatching thread "
			    "event on pid=%d status=%d 0x%x"
			    "pid and ppid not found\n"),
			    pid, status, status);
		(void) pmf_send_event_to_GC(event);

	} else {

		/*
		 * Add new event to list and signal change
		 */
		switch (handle->state) {
			// case PMF_TREE_STOPPING:
		case PMF_TREE_STOPPED:
			/*
			 * Debug message is referring handle. We must
			 * emit this message before releasing the lock.
			 */
			if (debug)
				dbg_msgout(NOGET("Dispatching thread "
				    "Event type %d on pid=%d "
				    "status=%d 0x%x redirected "
				    "handle %s stopped\n"),
				    event->type, event->pid,
				    event->status,
				    event->status,
				    handle->id);
			UNLOCK_HANDLE(handle);
			(void) pmf_send_event_to_GC(event);
			break;
		default:
			(void) queue_pmf_event(&handle->queue, event);
			(void) pthread_cond_signal(&handle->handle_cond);
			/*
			 * Debug message is referring handle. We must
			 * emit this message before releasing the lock.
			 */
			if (debug)
				dbg_msgout(NOGET("Dispatching thread "
				    "Event type %d on pid=%d "
				    "status=%d 0x%x dispatched "
				    "on handle %s\n"),
				    event->type,
				    event->pid,
				    event->status,
				    event->status,
				    handle->id);
			UNLOCK_HANDLE(handle);
			break;
		}

	}
	return (0);
}

/*
 * This is the main of Garbage Collector thread.
 * Aim of GC is to collecte events that are not
 * related to a known monitoring tree.
 */
static void
pmf_garbage_collector(void *param)
{

	int go = 1;
	struct pmf_event *event;
	sc_syslog_msg_handle_t sys_handle;

	LOCK_GARBAGE(pmf_garbage_collector);

	while (go) {

		/*
		 * Consum all enqueued events
		 */
		while ((event = dequeue_pmf_event(&garbage_queue))) {

			switch (event->type) {
			case PMFD_NOP:
				event->return_code.type = PMF_OKAY;
				break;
			case PMFD_CHILD:
			case PMFD_UNREGISTERED_CHILD:
				if (debug)
					dbg_msgout(NOGET
					    ("Garbage Collector got child "
					    "event pid %d status %d 0x%x\n"),
					    event->pid,
					    event->status,
					    event->status);
				if (WIFSTOPPED(event->status)) {
					/*
					 * This event was raised by a
					 * process while it was monitored,
					 * but the associated monitoring
					 * thread does not exit anymore.
					 * This must never happen and we
					 * need to reboot because this
					 * process will remain stopped
					 * forever.
					 */
					(void) sc_syslog_msg_initialize(
					    &sys_handle,
					    SC_SYSLOG_PMF_PMFD_TAG, "");
					/*
					 * SCMSGS
					 * @explanation
					 * Need explanation of this message!
					 * @user_action
					 * Need a user action for this
					 * message.
					 */
					(void) sc_syslog_msg_log(sys_handle,
					    LOG_ERR, MESSAGE,
					    "pmf_garbage_collector: "
					    "child pid %d "
					    "status %d 0x%x"
					    "stopped unexpectedly",
					    event->pid,
					    event->status,
					    event->status);
					sc_syslog_msg_done(&sys_handle);
					PMFD_DISPATCH_PANIC(garbage_collector);
				} else {
					/*
					 * One of our direct child is exiting
					 * and we dont monitor it anymore.
					 * This is a normal behavior and no
					 * further action is required
					 */
					event->return_code.type = PMF_OKAY;
				}
				break;
			case PMFD_RESUME:
			case PMFD_SUSPEND:
			case PMFD_STOP:
				if (debug)
					dbg_msgout(NOGET
					    ("Garbage Collector got unexpected "
					    "event 0x%x on pid=%d\n"),
					    event->type, event->pid);
				event->return_code.type = PMF_SYSERRNO;
				event->return_code.sys_errno = ESRCH;
				break;
			case PMFD_EXIT:
				if (debug)
					dbg_msgout(NOGET
					    ("Garbage Collector got exit "
					    "event\n"));
				goto out;
				break;
			default:
				if (debug)
					dbg_msgout(NOGET
					    (" Garbage Collector got unknown "
					    "event on pid=%d\n"),
					    event->pid);
				event->return_code.type = PMF_SYSERRNO;
				event->return_code.sys_errno = EINVAL;
				break;
			}

			/*
			 * Signal event was processed
			 */
			LOCK_EVENT(event);
			(void) pthread_cond_signal(&event->cond);
			UNLOCK_EVENT(event);

			(void) return_pmf_event_to_pool(&event);
		}

		/*
		 * Wait for new events
		 */
		(void) pthread_cond_wait(&garbage_queue.cond,
		    &garbage_queue.lock);
	}

out:
	UNLOCK_GARBAGE(pmf_garbage_collector);

	if (debug)
		dbg_msgout(NOGET("Garbage Collector exiting\n"));

}

/*
 *
 * This is the main monitoring loop. We should exit on wait4 complaining there
 * is no more children to monitor.
 */
static void
pmf_loop_monitor_children(void *param)
{
	int go = 1;
	int status;
	int pid;
	int rc;
	struct pmf_event *garbage_stop_event;
	pthread_t gc_thread;
	sc_syslog_msg_handle_t sys_handle;

	/*
	 * Pre-allocate event at startup
	 */
	if ((garbage_stop_event = get_pmf_event_from_pool()) == 0) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "Unable to allocate GC stop event");
		sc_syslog_msg_done(&sys_handle);
		pthread_exit((void *) 0);
	}

	if ((rc = acquire_pmf_event(garbage_stop_event)) != 0) {
		(void) return_pmf_event_to_pool(&garbage_stop_event);
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "Unable to acquire ownership of GC stop event");
		sc_syslog_msg_done(&sys_handle);
		pthread_exit((void *) 0);
	}

	/*
	 * Start the GC thread
	 */
	if (pthread_create(&gc_thread, (pthread_attr_t *)0,
		(void *(*)(void *)) pmf_garbage_collector, 0) != 0) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "Unable to start GC thread");
		sc_syslog_msg_done(&sys_handle);
		pthread_exit((void *) 0);
	}

	if (debug)
		dbg_msgout(NOGET("Waiting loop starting\n"));

	/*
	 * Signal starting
	 */

	while (go) {

		/*
		 * Wait for events on the traced processes.
		 * We also get exit status of our real children
		 * even after detaching from them.
		 */
		if ((pid = wait4(-1, &status, PMF_WAIT4_OPTION, 0)) < 0) {
			switch errno {
			case EINTR:
				if (debug) {
					dbg_msgout(NOGET
					    ("Monitoring loop wait4 "
					    "interrupted \n"));
				}
				if (pmf_is_stopped) {
					LOCK_QUEUE(pmf_loop_monitor_children);
					if (pmf_handle_list.head ==
					    (struct pmf_handle *)0) {
						go = 0;
					}
					UNLOCK_QUEUE(pmf_loop_monitor_children);
				}
				break;
			case ECHILD:

				/*
				 * No more child and pmf
				 * is marked as stopped:
				 * we can exit.
				 */
				if (pmf_is_stopped) {
					go = 0;
					break;
				}

				if (debug) {
					LOCK_QUEUE(pmf_loop_monitor_children);
					if (pmf_handle_list.head ==
					    (struct pmf_handle *)0) {
						dbg_msgout(NOGET
						    ("Monitoring loop waiting "
						    "for process to be "
						    "monitored\n"));
					} else {
						dbg_msgout(NOGET
						    ("Monitoring loop waiting "
						    "for process to be "
						    "resumed\n"));
					}
					UNLOCK_QUEUE(pmf_loop_monitor_children);
				}

				/*
				 * wait for a start or resume service request
				 */
				(void) sem_wait(&dispatching_sem);
				if (debug)
					dbg_msgout(NOGET
					    ("Monitoring loop resumed\n"));

				break;
			case EINVAL:
				PMFD_DISPATCH_PANIC(wait4);
				break;
			default:
				break;
			}
		} else {
			rc = pmf_dispatch_monitor_event(waiting_loop_buffer,
			    pid, status);
			if (rc != 0) {
				PMFD_DISPATCH_PANIC(pmf_dispatch_monitor_event);
			}
		}
	}

	/*
	 * Send a stop event to the GC
	 */
	garbage_stop_event->type = PMFD_EXIT;
	garbage_stop_event->return_code.type = PMF_OKAY;
	garbage_stop_event->return_code.sys_errno = 0;

	LOCK_GARBAGE(pmf_loop_monitor_children);
	(void) queue_pmf_event(&garbage_queue, garbage_stop_event);
	(void) pthread_cond_signal(&garbage_queue.cond);
	UNLOCK_GARBAGE(pmf_loop_monitor_children);

	/*
	 * Exit after GC thread.
	 */
	(void) pthread_join(gc_thread, (void **) 0);

	if (debug)
		dbg_msgout(NOGET
		    ("Monitoring thread exiting\n"));

	pthread_exit((void *) 0);
}

void
pmf_awake_dispatching_thread(void)
{

	/*
	 * Dispatching thread is parked waiting on a semaphore when there is
	 * no more process to monitor. Many awakening requests can be triggered
	 * at the same time. This can trigger unecessary loops of the waiting
	 * thread. This is armless but we will try to avoid it by keeping the
	 * value of the semaphore close to 1. This is why we try first to
	 * decrement the semaphore before posting it.
	 */
	(void) sem_trywait(&dispatching_sem);
	(void) sem_post(&dispatching_sem);

}

/*
 * Setup handler for  HUP and USR1 signals
 */
void
pmf_sig_handler(int signal)
{

	switch (signal) {
	case SIGTERM:
		(void) failfast_disarm();
		(void) sem_post(&termination_sem);
		break;
	case SIGUSR1:
		if (debug)
			(void) display_monitored_list();
		break;
	case SIGHUP:
		/*
		 * Used to interrupt wait4 syscall
		 * Nothing else to do.
		 */
		break;
	default:
		/* Unexpected - ignored */
		break;
	}
}

/*
 * Start RPC service
 */
static void
pmf_rpc_server(void *param)
{
	int err;
	sc_syslog_msg_handle_t handle;

	/*
	 * The default stack size on Solaris is about 1m while it is
	 * about 10m on Linux.  On Linux, with that much default stack
	 * size, we are not able to create couple hundred threads (e.g. 512).
	 * So, we need to change the default stack size.
	 */
	int stacksz = 70 * PTHREAD_STACK_MIN;

	if (debug)
		dbg_msgout(NOGET("RPC server thread started\n"));

	/*
	 * This thread is cancelled when pmfd is requested to stop
	 * and nobody care about joining it.
	 */
	if ((err = pthread_detach(pthread_self())) != 0) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * The rpc.pmfd server was not able to detach a thread,
		 * possibly due to low memory. The message contains the system
		 * error. The server does not perform the action requested by
		 * the client, and an error message is output to syslog.
		 * @user_action
		 * Determine if the machine is running out of memory. If all
		 * looks correct, save the /var/adm/messages file. Contact
		 * your authorized Sun service provider to determine whether a
		 * workaround or patch is available.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "pthread_detach: %s", strerror(err));
		sc_syslog_msg_done(&handle);
		goto init_failed;
	}

	/*
	 * Initialize loopbackset array and rpc service.
	 */
	if (rpc_init_rpc_mgr(
	    MAX_RPC_THREADS, debug, stacksz) != RPC_SERVICES_OK) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "rpc_init_rpc_mgr failed.");
		sc_syslog_msg_done(&handle);
		goto init_failed;
	}

	if (security_svc_reg(pmf_program_2, PMF_PROGRAM_NAME,
	    (ulong_t)PMF_PROGRAM, (ulong_t)PMF_VERSION, debug) != 0) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "security_svc_reg failed.");
		sc_syslog_msg_done(&handle);
		goto init_failed;
	}

	if (debug)
		dbg_msgout(NOGET("RPC server thread, service registered\n"));

#ifndef LOCAL
	/*
	 * Initialize the failfast driver.
	 * st_ff_arm returns error codes from the errno set, so use
	 * strerror to read them.
	 * If LOCAL is defined, skip it because we want to test
	 * on a standalone host so we don't link with the failfast libraries.
	 */
	if ((err = failfast_arm("pmfd")) != 0) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "st_ff_arm failed: %s", strerror(err));
		sc_syslog_msg_done(&handle);
		goto init_failed;
	}
	if (debug)
		dbg_msgout(NOGET("RPC server thread, failfast armed\n"));
#endif
	/*
	 * Advertize that the initialization status is available.
	 */
	LOCK_RPC(pmf_rpc_server);
	rpc_startup_rc = 0;
	(void) pthread_cond_broadcast(&rpc_startup_cv);
	UNLOCK_RPC(pmf_rpc_server);

	/*
	 * This thread is cancelled when PMFD is stopping. So svc_run()
	 * should never return.
	 */
	svc_run();
	(void) sc_syslog_msg_initialize(&handle, SC_SYSLOG_PMF_PMFD_TAG, "");
	(void) sc_syslog_msg_log(&handle, LOG_ERR, MESSAGE,
	    "svc_run returned");
	sc_syslog_msg_done(&handle);
	PMFD_DISPATCH_PANIC(pmf_rpc_server);
	/* NOTREACHED */


init_failed:
	LOCK_RPC(pmf_rpc_server);
	rpc_startup_rc = -1;
	(void) pthread_cond_broadcast(&rpc_startup_cv);
	UNLOCK_RPC(pmf_rpc_server);
	pthread_exit((void *) 0);
}

/*
 * PMFD termination thread. Wait on
 * a semaphore that can be signaled by
 * a dedicated RPC call or by a signal
 * handler (sem_post is async safe)
 */
static void
pmf_terminator(void *param)
{
	struct pmf_event *event;
	struct pmf_handle *handle;
	struct pmf_event *exit_event;
	sc_syslog_msg_handle_t sys_handle;

	/*
	 * wait for termination signal
	 */
	while (sem_wait(&termination_sem)) {
	}

	if (debug)
		dbg_msgout(NOGET("Terminator thread started\n"));

	(void) sc_syslog_msg_initialize(&sys_handle,
	    SC_SYSLOG_PMF_PMFD_TAG, "");
	/*
	 * SCMSGS
	 * @explanation
	 * Need explanation of this message!
	 * @user_action
	 * Need a user action for this message.
	 */
	(void) sc_syslog_msg_log(sys_handle, LOG_INFO, MESSAGE,
	    "PMFD exit request received");
	sc_syslog_msg_done(&sys_handle);

	/*
	 * Stop the RPC service thread
	 * Stop monitoring of all trees
	 */
	LOCK_QUEUE(pmf_terminator);

	pmf_handle_list.stopping = 1;

	(void) rpc_cleanup_rpc_mgr(MAX_RPC_THREADS);

	pthread_cancel(rpc_server_thread);
	(void) pthread_kill(rpc_server_thread, SIGHUP);

	while ((handle = pmf_handle_list.head)) {
		LOCK_HANDLE(handle);
		UNLOCK_QUEUE(pmf_terminator);

		if (debug)
			dbg_msgout(NOGET("Terminator thread "
			    "stopping monitoring thread %s\n"),
			    handle->id);

		exit_event = (struct pmf_event *)0;
		if (handle->exit_event) {
			exit_event = handle->exit_event;
			(void) acquire_pmf_event(exit_event);
			LOCK_EVENT(exit_event);
		}

		/*
		 * Push an EXIT event in the monitoring thread
		 * event queue
		 */
		event = get_pmf_event_from_pool();
		event->type = PMFD_EXIT;
		event->return_code.type = PMF_OKAY;
		event->return_code.sys_errno = 0;
		(void) queue_pmf_event(&handle->queue, event);
		(void) pthread_cond_signal(&handle->handle_cond);
		UNLOCK_HANDLE(handle);

		/*
		 * Wait for monitoring thread termination
		 */
		if (exit_event) {
			if (debug)
				dbg_msgout(NOGET("Terminator thread "
				    "waiting for monitoring thread exit\n"));
			(void) pthread_cond_wait(&exit_event->cond,
			    &exit_event->lock);
			UNLOCK_EVENT(exit_event);
			(void) release_pmf_event(&exit_event);
		}

		LOCK_QUEUE(pmf_terminator);
	}

	UNLOCK_QUEUE(pmf_terminator);

	/*
	 * Set termination flag of dispatching thread
	 * and awake it if necessary
	 */
	pmf_is_stopped = 1;
	(void) pthread_kill(dispatching_thread, SIGHUP);
	sem_post(&dispatching_sem);

	if (debug)
		dbg_msgout(NOGET("Terminator thread done\n"));

}

/*
 * Setup of monitoring framework
 */
int
pmf_ptrace_monitor_startup(void)
{

	int err;
	sc_syslog_msg_handle_t handle;

	if (sem_init(&dispatching_sem, 0, 0) != 0) {
		err = errno;
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "sem_init (dispatching): %s", strerror(err));
		sc_syslog_msg_done(&handle);
		return (-1);
	}
	if (sem_init(&termination_sem, 0, 0) != 0) {
		err = errno;
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "sem_init (termination): %s", strerror(err));
		sc_syslog_msg_done(&handle);
		return (-1);
	}

	/*
	 * Lauch termination thread.
	 * Lauch dispatching thread.
	 * Lauch RPC server thread. We need to synchronize with RPC server
	 * thread to get it's initilization status as errors may be raised
	 * when registering services (registration must be done within RPC
	 * server thread itself).
	 */
	if ((err =
		pthread_create(&termination_thread, (pthread_attr_t *)0,
		    (void *(*)(void *)) pmf_terminator, 0)) != 0) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "pthread_create (termination): %s", strerror(err));
		sc_syslog_msg_done(&handle);
		return (-1);
	}
	if ((err =
		pthread_create(&dispatching_thread, (pthread_attr_t *)0,
		    (void *(*)(void *)) pmf_loop_monitor_children, 0)) != 0) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "pthread_create (dispatching): %s", strerror(err));
		sc_syslog_msg_done(&handle);
		return (-1);
	}

	LOCK_RPC(pmf_ptrace_monitor_startup);
	if ((err =
		pthread_create(&rpc_server_thread, (pthread_attr_t *)0,
		    (void *(*)(void *)) pmf_rpc_server, 0)) != 0) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "pthread_create (rpc server): %s", strerror(err));
		sc_syslog_msg_done(&handle);
		UNLOCK_RPC(pmf_ptrace_monitor_startup);
		return (-1);
	}
	(void) pthread_cond_wait(&rpc_startup_cv, &rpc_startup_lock);
	err = rpc_startup_rc;
	UNLOCK_RPC(pmf_ptrace_monitor_startup);

	return (err);

}

/*
 * Rejoin the dispatching thread.
 */
int
pmf_ptrace_monitor_join(void)
{

	int err;
	sc_syslog_msg_handle_t handle;

	/*
	 * Pthread_join should return only when stopping of PMFD has
	 * been explicitely requested.
	 */
	if ((err = pthread_join(dispatching_thread, (void **) 0)) != 0) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "pthread_join (dispatching_thread): %s", strerror(err));
		sc_syslog_msg_done(&handle);
		return (-1);
	}

	if (debug)
		dbg_msgout(NOGET("Dispatching thread joined\n"));

	/*
	 * We are ready to exit now. We make the assumption that failfast was
	 * disarmed before greceful shutdown was requested else pmfd exit will
	 * trigger a node reboot.
	 */

	return (0);
}
