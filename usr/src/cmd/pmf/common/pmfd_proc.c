/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)pmfd_proc.c	1.68	08/05/20 SMI"


/*
 * pmfd_proc.c - Monitor sub processes of processes we were asked to start.
 *
 * We use procfs here to trap for fork system calls in
 * the pid we're passed in.  For each fork, we start
 * a new thread, and recursively trap for fork system
 * calls in those.  Each new thread is kept track of on the
 * handle threads list, which we subsequently use to rejoin
 * them all.  The objective here is to not return from
 * pmf_monitor_children until the last process/sub-process
 * has exited.
 */
#include "pmfd.h"

#if DOOR_IMPL
#include <rgm/pmf.h>
#else
#include <rgm/rpc/pmf.h>
#endif

#include <sys/wait.h>
#include <sys/fcntl.h>
#include <sys/syscall.h>
#include <sys/sc_syslog_msg.h>
#include <sys/rsrc_tag.h>
#include <procfs.h>
#include <poll.h>
#include <stropts.h>
#include <dirent.h>
#include <sys/sol_version.h>


/*
 * size of control structs, used in control read and write operations
 */
#define		DOUBLE_LONG_SIZE (2 * sizeof (long))
#define		LONG_SYSSET_SIZE (sizeof (long) + sizeof (sysset_t))

extern int pmf_alloc_cnt;

static void pmf_monitor_children(int pid, pmf_handle *handle, int child_level);
static void pmf_search_children(pmf_threads *thread);
static void pmf_free_thread(pmf_threads *thread);
static int pmf_monitor_init(pmf_threads *thread, int *control_fd,
    int *status_fd);
static int pmf_monitor_suspend(pmf_threads *thread, int *control_fd,
    int *status_fd);
static int pmf_add_triggers(int ctlpfd, int monitor_children);
static int pmf_remove_triggers(int ctlpfd);
static void pmf_kill_stuck_children(pmf_threads *thread);
static int read_proc_file(const char *procnum, const char *procfile,
    void *struct_ptr);
static int wait_for_stop(pid_t pid, const char *procnum);


/* ARGSUSED */
/*
 * Suspend/Resume signal handler
 *
 * The signal handler must not use dbg_msgout() because it calls fprintf(3c)
 * which is not Async-Signal-Safe.
 */
void
pmf_sig_handler(int sig)
{
}

static void
pmf_search_children(pmf_threads *thread)
{
	pstatus_t pstatus;
	pmf_handle *handle = thread->handle;
	int ctlpfd = 0;
	int statpfd = 0;
	int err;
	sc_syslog_msg_handle_t sys_handle;
	struct {
		long cmd;
		union {
			long flags;
			sysset_t syscalls;
		} arg;
	} ctl;

	if (debug) dbg_msgout(NOGET("%d in pmf_search_children\n"),
	    (int)thread->pid);

	ASSERT(thread->monitor_state == PMF_STARTING);
	(void) pthread_mutex_lock(&thread->lock);
	thread->monitor_state = PMF_MONITORED;
	(void) pthread_mutex_unlock(&thread->lock);

	/*
	 * Open/reopen procfs control and status files so that we can
	 * listen for fork, entry and exit events.
	 */
again:

	if (pmf_monitor_init(thread, &ctlpfd, &statpfd) == -1)
		goto errdone;

	thread->exitcode = 0;

	if (debug)
	    dbg_msgout(NOGET(
	    "pid=%d monitor_children=%d child_level=%d\n"),
	    thread->pid, thread->monitor_children, thread->child_level);

	/*
	 * Check monitoring option and child level: for option -C,
	 * corresponding to PMF_LEVEL, we monitor only some parent
	 * and child levels; in this case we if we're in the child
	 * at the level where monitoring stops, we need to reset
	 * the inherit_on_fork flag to not inherit the parent's behavior,
	 * i.e. not to stop on fork.
	 */
	if ((thread->monitor_children == PMF_LEVEL) &&
	    (thread->child_level == handle->monitor_level)) {
		if (pmf_remove_triggers(ctlpfd) != 0) {
			/*
			 * Suppress lint message about "call to __errno() not
			 * made in the presence of a prototype."
			 * lint is unable to see the prototype for the __errno()
			 * function that errno is macroed to.
			 */
			err = errno; /*lint !e746 */
			if (debug) dbg_msgout(NOGET(
			    "in pmf_remove_triggers errno is %d\n"), err);
			if (err == EAGAIN)
				goto again;
			if (err == ENOENT)
				goto done;
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_PMF_PMFD_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * The rpc.pmfd server was not able to monitor a
			 * process, and the system error is shown. An error
			 * message is output to syslog.
			 * @user_action
			 * Save the syslog messages file. Contact your
			 * authorized Sun service provider to determine
			 * whether a workaround or patch is available.
			 */
			(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
			    "pmf_search_children: pmf_remove_triggers: %s",
			    strerror(err));
			sc_syslog_msg_done(&sys_handle);
			goto errdone;
		}
	}

	/*
	 * With inherit on fork turned on, our child is stopped on
	 * its first return from fork (the one that created it).
	 * We need to continue the child, otherwise it will be stuck in the
	 * fork forever.  But we don't need to monitor its child (there isn't
	 * one).
	 *
	 * The code below handles this case with a special first_call_returned
	 * flag in each monitoring thread structure.  If the initial fork
	 * has returned already, that flag will be true, so we know that
	 * any subsequent fork is a real fork.  If a fork returns, but
	 * first_call_returned is still FALSE, then we know it's the
	 * initial fork that created the process, so we can ignore it.
	 *
	 * However, the first (parent) process monitored never gets stopped
	 * on its first return from fork (because the /proc triggers have not
	 * yet been set up). Thus, the pmf_alloc_thread code special-cases
	 * child-level of 0 by setting first_call_returned already to TRUE.
	 *
	 * See comments below under the fork case.
	 */
	for (;;) {
		/*
		 * Wait for the child to stop on "interesting"
		 * events that we've registered to hear about.
		 */
		ctl.cmd = PCWSTOP;
		ctl.arg.flags = 0L;
		while ((thread->monitor_state == PMF_MONITORED) &&
		    (write(ctlpfd, (char *)&ctl, DOUBLE_LONG_SIZE) == -1)) {
			err = errno;
			if (debug) dbg_msgout(NOGET(
			    "in PCWSTOP errno is %d\n"), err);
			if (err == EINTR)
				continue;
			if ((err == ENOENT) || (errno == EBADF))
				goto done;
			if (err == EAGAIN)
				goto again;
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_PMF_PMFD_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * The rpc.pmfd server was not able to monitor a
			 * process, and the system error is shown. An error
			 * message is output to syslog.
			 * @user_action
			 * Save the /var/adm/messages file. Contact your
			 * authorized Sun service provider to determine
			 * whether a workaround or patch is available.
			 */
			(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
			    "PCWSTOP: %s", strerror(err));
			sc_syslog_msg_done(&sys_handle);
			break;
		}

		/*
		 * Suspend monitoring if requested
		 */
		(void) pthread_mutex_lock(&thread->lock);
		if (thread->monitor_state == PMF_SUSPENDING) {
			err = pmf_monitor_suspend(thread, &ctlpfd,
			    &statpfd);
			(void) pthread_mutex_unlock(&thread->lock);
			if (err == -1)
				goto errdone;
			else
				continue;
		}
		(void) pthread_mutex_unlock(&thread->lock);

		/*
		 * Grab the event status
		 */
		while (pread(statpfd, &pstatus, sizeof (pstatus),
		    (off_t)0) == -1) {
			err = errno;
			if (debug) dbg_msgout(NOGET(
				"in PCSTATUS errno is %d\n"), err);
			if (err == EINTR)
				continue;
			if ((err == ENOENT) || (err == EBADF))
				goto done;
			if (err == EAGAIN)
				goto again;
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_PMF_PMFD_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * The rpc.pmfd server was not able to monitor a
			 * process, and the system error is shown. An error
			 * message is output to syslog.
			 * @user_action
			 * Save the /var/adm/messages file. Contact your
			 * authorized Sun service provider to determine
			 * whether a workaround or patch is available.
			 */
			(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
			    "PCSTATUS: %s", strerror(err));
			sc_syslog_msg_done(&sys_handle);
			goto done;
		}

		/*
		 * Process the event.
		 */
		if (debug) dbg_msgout(NOGET(
		    "pstatus is:\n%d %d %d %d %d %d %d\n"
		    "\t%d %d %d %d %d %d %d %d %d\n"),
		    pstatus.pr_flags, pstatus.pr_nlwp, pstatus.pr_pid,
		    pstatus.pr_ppid, pstatus.pr_pgid, pstatus.pr_sid,
		    pstatus.pr_aslwpid, pstatus.pr_lwp.pr_flags,
		    pstatus.pr_lwp.pr_lwpid, pstatus.pr_lwp.pr_why,
		    pstatus.pr_lwp.pr_what, pstatus.pr_lwp.pr_syscall,
		    pstatus.pr_lwp.pr_nsysarg, pstatus.pr_lwp.pr_errno,
		    pstatus.pr_lwp.pr_rval1, pstatus.pr_lwp.pr_reg[R_R0]);

		switch (pstatus.pr_lwp.pr_why) {
		case PR_SYSEXIT:
			switch (pstatus.pr_lwp.pr_syscall) {
			case SYS_fork:
			case SYS_vfork:
			case SYS_fork1:
				/*
				 * Setup to monitor
				 * the new child
				 */
				if (debug)
				    dbg_msgout(NOGET("%d -> %d\n"),
				    (int)thread->pid,
				    pstatus.pr_lwp.pr_reg[R_R0]);

				/*
				 * If the fork failed, check
				 * the error, then return:
				 * there is no new process forked, so
				 * there is no need for a new monitoring
				 * thread.
				 */
				if (pstatus.pr_lwp.pr_errno != 0) {
					(void) sc_syslog_msg_initialize(
					    &sys_handle,
					    SC_SYSLOG_PMF_PMFD_TAG, "");
					/*
					 * SCMSGS
					 * @explanation
					 * The rpc.pmfd server was not able to
					 * start (fork) the application. This
					 * problem can occur if the machine
					 * has low memory. The system error
					 * number is shown, and an error
					 * message is output to syslog.
					 * @user_action
					 * Determine if the machine is running
					 * out of memory. If this is not the
					 * case, save the /var/adm/messages
					 * file. Contact your authorized Sun
					 * service provider to determine
					 * whether a workaround or patch is
					 * available.
					 */
					(void) sc_syslog_msg_log(sys_handle,
					    LOG_ERR, MESSAGE,
					    "monitored processes forked "
					    "failed (errno=%d)",
					    pstatus.pr_lwp.pr_errno);
					sc_syslog_msg_done(&sys_handle);

					break;
				}

				/*
				 * There's one special case
				 * here ... if we're just
				 * returning from the fork()
				 * that our parent invoked then
				 * the pid returned is that of
				 * our parent.  We can ignore
				 * that case, because our parent
				 * should be already monitored.
				 * In order to ignore it, we keep
				 * track of whether we've returned from
				 * our first syscall or not.  In the case of
				 * any child process, the first syscall we
				 * return from will be the fork that our
				 * parent invoked, and that we can ignore.
				 */
				if (!thread->first_call_returned) {
					thread->first_call_returned = B_TRUE;
					if (debug) dbg_msgout(NOGET(
					    "first fork -- ignoring\n"));
					break;
				}

				/*
				 * If user didn't ask the "-C" option,
				 * monitor all levels of children;
				 * if user asked the "-C N" option,
				 * monitor only parent and first
				 * N levels of children.
				 */
				if ((thread->monitor_children ==
				    PMF_ALL) ||
				    ((thread->monitor_children ==
				    PMF_LEVEL) &&
				    (thread->child_level <
				    handle->monitor_level))) {
					pmf_monitor_children(
					(int)(pstatus.pr_lwp.pr_reg[R_R0]),
					handle,
					thread->child_level + 1);
				}

				break;
			default:
				(void) sc_syslog_msg_initialize(&sys_handle,
				    SC_SYSLOG_PMF_PMFD_TAG, "");
				/*
				 * SCMSGS
				 * @explanation
				 * An event which was not registered for, has
				 * been received. This can happen if the
				 * monitored process is simultaneously being
				 * controlled by another process, most notably,
				 * by a proc tool.
				 * @user_action
				 * This is informational. No action is needed.
				 */
				(void) sc_syslog_msg_log(sys_handle,
				    LOG_INFO, MESSAGE,
				    "Unregistered syscall. %s(%d)",
				    "PR_SYSEXIT", pstatus.pr_lwp.pr_syscall);
				sc_syslog_msg_done(&sys_handle);
			}
			break;
		case PR_SYSENTRY:
			switch (pstatus.pr_lwp.pr_syscall) {
			case SYS_exec:
			case SYS_execve:
				if (debug) dbg_msgout(NOGET("exec\n"));

				if (thread->signal != 0) {
					if (debug) dbg_msgout(
					    NOGET("KILL -%d %d\n"),
					    thread->signal, thread->pid);
					if (kill(thread->pid, thread->signal)) {
					/*
					 *  Kill failed -- print message
					 */
						int err1 = errno;
						(void) sc_syslog_msg_initialize(
						    &sys_handle,
						    SC_SYSLOG_PMF_PMFD_TAG, "");
						/*
						 * SCMSGS
						 * @explanation
						 * An error occured while
						 * rpc.pmfd attempted to send
						 * a signal to one of the
						 * processes of the given tag.
						 * The reason for the failure
						 * is also given. The signal
						 * was sent to the process as
						 * a result of some event
						 * external to rpc.pmfd.
						 * rpc.pmfd "intercepted" the
						 * signal, and is trying to
						 * pass the signal on to the
						 * monitored process.
						 * @user_action
						 * Save the /var/adm/messages
						 * file. Contact your
						 * authorized Sun service
						 * provider to determine
						 * whether a workaround or
						 * patch is available.
						 */
						(void) sc_syslog_msg_log(
						    sys_handle, LOG_ERR,
						    MESSAGE,
						    "pmf_search_children: Error"
						    " signaling <%s>: %s",
						    handle->id, strerror(err1));
						sc_syslog_msg_done(&sys_handle);
					}
				}
				break;
			case SYS_exit:
				thread->exitcode =
				    (int)(pstatus.pr_lwp.pr_sysarg[0]);
				if (debug) dbg_msgout(NOGET("exit< (%d)\n"),
				    thread->exitcode);
				thread->normal_exit = B_TRUE;
				break;
			default:
				(void) sc_syslog_msg_initialize(&sys_handle,
				    SC_SYSLOG_PMF_PMFD_TAG, "");
				(void) sc_syslog_msg_log(sys_handle,
				    LOG_INFO, MESSAGE,
				    "Unregistered syscall. %s(%d)",
				    "PR_SYSENTRY", pstatus.pr_lwp.pr_syscall);
				sc_syslog_msg_done(&sys_handle);
			}
			break;
		default:
			if (debug) dbg_msgout(NOGET(
			    "Stopped on event not traced: %d, flag=0x%x)\n"),
			    pstatus.pr_lwp.pr_why, (int)pstatus.pr_flags);
			break;
		}

		/*
		 * Since we've stopped on an event, we need
		 * to continue the process.
		 */
		ctl.cmd = PCRUN;
		ctl.arg.flags = PRCSIG;
		while (write(ctlpfd, (char *)&ctl, DOUBLE_LONG_SIZE) == -1) {
			err = errno;
			if (debug) dbg_msgout(NOGET(
				"in PCRUN errno is %d\n"), err);
			if (err == EINTR)
				continue;
			if ((err == ENOENT) || (err == EBADF))
				goto done;
			if (err == EAGAIN)
				goto again;
			if (err == EBUSY)
				break;
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_PMF_PMFD_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * The rpc.pmfd server was not able to monitor a
			 * process, and the system error is shown. An error
			 * message is output to syslog.
			 * @user_action
			 * Save the /var/adm/messages file. Contact your
			 * authorized Sun service provider to determine
			 * whether a workaround or patch is available.
			 */
			(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
			    "PCRUN: %s", strerror(err));
			sc_syslog_msg_done(&sys_handle);
		}

	}

errdone:
	/*
	 * We should kill this process, instead of letting it
	 * exist without being monitored.
	 */
	if (kill(thread->pid, SIGKILL)) {
		/*
		 *  Kill failed -- print message
		 *
		 * Don't print message if kill() failed with ESRCH because
		 * it means that the process is already dead.
		 */
		err = errno;
		if (err != ESRCH) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_PMF_PMFD_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * An error occured while rpc.pmfd attempted to send a
			 * KILL signal to one of the processes of the given
			 * tag. The reason for the failure is also given.
			 * rpc.pmfd attempted to kill the process because a
			 * previous error occured while creating a monitor
			 * process for the process to be monitored.
			 * @user_action
			 * Save the /var/adm/messages file. Contact your
			 * authorized Sun service provider to determine
			 * whether a workaround or patch is available.
			 */
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "pmf_search_children: Error stopping <%s>: %s",
			    handle->id, strerror(err));
			sc_syslog_msg_done(&sys_handle);
		}
	}

done:
	/*
	 * If we did not have a normal exit it probably means the process
	 * died because of a signal such as SIGKILL. There is a chance that
	 * it was in a fork call at the time the signal arrived, in which case
	 * it might have managed to fork before it died, without the pmf
	 * knowing about it.  Thus, we need to call pmf_kill_stuck_children
	 * to clean up.
	 *
	 * Note that calling pmf_kill_stuck_children every time there is an
	 * abnormal exit will result in unneccesary calls (most of the time).
	 * The only cases that we care about are when the process was in fork
	 * and received SIGKILL.  However, there is no way to narrow it down
	 * to catch only those cases.  We can't trap SIGKILL with procfs.
	 * Also, we can't rely on trapping the entrance to fork to figure out
	 * if the process was in fork when it died.  It's possible for the
	 * process to fork and die (due to SIGKILL) without stopping at either
	 * the entrance or exit to fork (or at least without the pmf able to
	 * detect the stops).
	 *
	 * Note that we don't need to call pmf_kill_stuck_children if
	 * we are past the last level of monitoring.  If so, we removed
	 * the inherit-on-fork triggers from /proc for the parent proc,
	 * so there should not be any stuck procs.
	 */
	if (!thread->normal_exit && (thread->monitor_children == PMF_ALL ||
	    (thread->monitor_children == PMF_LEVEL && thread->child_level <
		handle->monitor_level))) {
		pmf_kill_stuck_children(thread);
	}

	if (statpfd != 0)
		(void) close(statpfd);
	if (ctlpfd != 0)
		(void) close(ctlpfd);

	if (debug) dbg_msgout(NOGET("%d exited (%d)\n"),
	    (int)thread->pid, thread->exitcode);

	/*
	 * Notify any pending suspend/resume request that it must give up
	 */
	(void) pthread_mutex_lock(&thread->lock);
	thread->monitor_state = PMF_EXITING;
	(void) pthread_cond_signal(&thread->cv);
	(void) pthread_mutex_unlock(&thread->lock);

	pmf_free_thread(thread);

	if ((err = pthread_detach(pthread_self())) != 0) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
		    "pthread_detach: %s", strerror(err));
		sc_syslog_msg_done(&sys_handle);
	} else {
		pmf_thread_cnt--;
	}

}

/*
 * This function opens the /proc files needed to monitor a process.
 * It is called when the monitoring starts and it must be called again
 * each time the /proc file descriptors become invalid (/proc EAGAIN
 * failure because the process has exec'ed a setuid program)
 */
static int
pmf_monitor_init(pmf_threads *thread, int *control_fd, int *status_fd)
{
	char ctl_file[BUFSIZ];
	char status_file[BUFSIZ];
	int ctlpfd;
	int newctlpfd = -1;
	int statpfd;
	int newstatpfd = -1;
	int err;
	sc_syslog_msg_handle_t sys_handle;

	ctlpfd = *control_fd;
	statpfd = *status_fd;

	/*
	 * We must reopen before closing the old descriptor; see proc(4)
	 */
	(void) sprintf(ctl_file, "/proc/%d/ctl", (int)thread->pid);
	(void) sprintf(status_file, "/proc/%d/status", (int)thread->pid);

	while ((newctlpfd == -1) || (newstatpfd == -1)) {

		if ((newctlpfd = open(ctl_file, O_WRONLY)) == -1) {
			err = errno;
			if (debug) dbg_msgout(NOGET(
			    "in open(%s) errno is %d\n"), ctl_file, errno);
			if (err == EAGAIN)
				continue;
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_PMF_PMFD_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * The rpc.pmfd server was not able to open a procfs
			 * control file, and the system error is shown. procfs
			 * control files are required in order to monitor user
			 * processes.
			 * @user_action
			 * Determine if the machine is running out of memory.
			 * If this is not the case, save the /var/adm/messages
			 * file. Contact your authorized Sun service provider
			 * to determine whether a workaround or patch is
			 * available.
			 */
			(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
			    "Error opening procfs control file <%s> "
			    "for tag <%s>: %s",
			    ctl_file, thread->handle->id, strerror(err));
			sc_syslog_msg_done(&sys_handle);
			return (-1);
		}
		if (ctlpfd != 0)
			(void) close(ctlpfd);

		*control_fd = ctlpfd = newctlpfd;

		/*
		 * set close on exec so that monitored processes
		 * don't inherit this open file
		 */
		(void) fcntl(ctlpfd, F_SETFD, FD_CLOEXEC);

		if ((newstatpfd = open(status_file, O_RDONLY)) == -1) {
			err = errno;
			if (debug) dbg_msgout(NOGET(
			    "in open(%s) errno is %d\n"), status_file, errno);
			if (err == EAGAIN)
				continue;
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_PMF_PMFD_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * The rpc.pmfd server was not able to open a procfs
			 * status file, and the system error is shown. procfs
			 * status files are required in order to monitor user
			 * processes.
			 * @user_action
			 * Determine if the machine is running out of memory.
			 * If this is not the case, save the /var/adm/messages
			 * file. Contact your authorized Sun service provider
			 * to determine whether a workaround or patch is
			 * available.
			 */
			(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
			    "Error opening procfs status file <%s> "
			    "for tag <%s>: %s",
			    status_file, thread->handle->id, strerror(err));
			sc_syslog_msg_done(&sys_handle);
			return (-1);
		}
		if (statpfd != 0)
			(void) close(statpfd);

		*status_fd = statpfd = newstatpfd;

		/*
		 * set close on exec so that monitored processes
		 * don't inherit this open file
		 */
		(void) fcntl(statpfd, F_SETFD, FD_CLOEXEC);
	}

	return (0);

}

/*
 * This function is called to suspend the monitoring of a process.
 * We stay in this function while the monitoring is suspended.
 *
 * Return 0 when the monitoring has been successfully resumed
 * otherwise -1 is returned.
 *
 * This function must be called with the pmf_thread lock held. This lock is
 * still held on return.
 */
static int
pmf_monitor_suspend(pmf_threads *thread, int *control_fd, int *status_fd)
{
	char ctl_file[BUFSIZ];
	char status_file[BUFSIZ];
	int ctlpfd;
	int newstatpfd;
	int statpfd;
	int rc;
	int err;
	sc_syslog_msg_handle_t sys_handle;
	struct pollfd pollfd;
	struct {
		long cmd;
		union {
			long flags;
			sysset_t syscalls;
		} arg;
	} ctl;

	ctlpfd = *control_fd;
	statpfd = *status_fd;

	(void) sprintf(ctl_file, "/proc/%d/ctl", (int)thread->pid);
	(void) sprintf(status_file, "/proc/%d/status", (int)thread->pid);
	/*
	 * Disable all triggers so that process does not stop
	 */
	while (pmf_remove_triggers(ctlpfd) == -1) {
		err = errno;
		if (debug) dbg_msgout(NOGET(
		    "in pmf_remove_triggers (suspend) errno is %d\n"), err);
		if ((err == EAGAIN) &&
		    (pmf_monitor_init(thread, control_fd, status_fd) == 0)) {
			ctlpfd = *control_fd;
			statpfd = *status_fd;
			continue;
		}
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * The rpc.pmfd server was not able to suspend the monitoring
		 * of a process and the monitoring of the process has been
		 * aborted. The message contains the system error.
		 * @user_action
		 * Save the syslog messages file. Contact your authorized Sun
		 * service provider to determine whether a workaround or patch
		 * is available.
		 */
		(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
		    "pmf_monitor_suspend: pmf_remove_triggers: %s",
		    strerror(err));
		sc_syslog_msg_done(&sys_handle);
		return (-1);
	}

	/*
	 * Tell process to run as it may have stop while
	 * removing the triggers
	 */
	ctl.cmd = PCRUN;
	ctl.arg.flags = PRCSIG;
	while (write(ctlpfd, (char *)&ctl, DOUBLE_LONG_SIZE) == -1) {
		err = errno;
		if (debug) dbg_msgout(NOGET(
		    "in PCRUN (suspend) errno is %d\n"), err);
		if (err == EINTR)
			continue;
		if (err == EBUSY)
			break;
		if ((err == EAGAIN) &&
		    (pmf_monitor_init(thread, control_fd, status_fd) == 0)) {
			ctlpfd = *control_fd;
			statpfd = *status_fd;
			continue;
		}
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * The rpc.pmfd server was not able to suspend the monitoring
		 * of a process and the monitoring of the process has been
		 * aborted. The message contains the system error.
		 * @user_action
		 * Save the syslog messages file. Contact your authorized Sun
		 * service provider to determine whether a workaround or patch
		 * is available.
		 */
		(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
		    "pmf_monitor_suspend: PCRUN: %s", strerror(err));
		sc_syslog_msg_done(&sys_handle);
		return (-1);
	}

	(void) close(ctlpfd);
	ctlpfd = 0;

	thread->monitor_state = PMF_UNMONITORED;
	(void) pthread_cond_signal(&thread->cv);

	rc = 0;
	pollfd.fd = statpfd;
	pollfd.events = 0;

	while ((rc == 0) && (ctlpfd == 0)) {

		(void) pthread_mutex_unlock(&thread->lock);

		while (thread->monitor_state == PMF_UNMONITORED) {
			if (poll(&pollfd, (nfds_t)1, INFTIM) == -1) {
				err = errno;
				if (debug) dbg_msgout(NOGET(
				    "in poll errno is %d\n"), err);
				if ((err == EINTR) || (err == EAGAIN))
					continue;
				(void) sc_syslog_msg_initialize(&sys_handle,
				    SC_SYSLOG_PMF_PMFD_TAG, "");
				/*
				 * SCMSGS
				 * @explanation
				 * The rpc.pmfd server was not able to monitor
				 * a process. and the system error is shown.
				 * This error occurred for a process whose
				 * monitoring had been suspended. The
				 * monitoring of this process has been aborted
				 * and can not be resumed.
				 * @user_action
				 * Save the syslog messages file. Contact your
				 * authorized Sun service provider to
				 * determine whether a workaround or patch is
				 * available.
				 */
				(void) sc_syslog_msg_log(sys_handle,
				    LOG_ERR, MESSAGE,
				    "pmf_monitor_suspend: poll: %s",
				    strerror(err));
				sc_syslog_msg_done(&sys_handle);
				rc = -1;
				break;
			}

			if (pollfd.revents & POLLHUP) {
				rc = -1;
				break;
			}

			if (pollfd.revents & POLLERR) {
				while ((newstatpfd = open(status_file,
				    O_RDONLY)) == -1) {
					err = errno;
					if (debug) dbg_msgout(NOGET(
					    "in open(%s) (suspend) "
					    "errno is %d\n"),
					    status_file, err);
					if (err == EAGAIN)
						continue;
					(void) sc_syslog_msg_initialize(
					    &sys_handle,
					    SC_SYSLOG_PMF_PMFD_TAG, "");
					/*
					 * SCMSGS
					 * @explanation
					 * The rpc.pmfd server was not able to
					 * open a procfs status file, and the
					 * system error is shown. procfs
					 * status files are required in order
					 * to monitor user processes. This
					 * error occurred for a process whose
					 * monitoring had been suspended. The
					 * monitoring of this process has been
					 * aborted and can not be resumed.
					 * @user_action
					 * Determine if the machine is running
					 * out of memory. If this is not the
					 * case, save the syslog messages
					 * file. Contact your authorized Sun
					 * service provider to determine
					 * whether a workaround or patch is
					 * available.
					 */
					(void) sc_syslog_msg_log(sys_handle,
					    LOG_ERR, MESSAGE,
					    "pmf_monitor_suspend: "
					    "Error opening procfs status file "
					    "<%s> for tag <%s>: %s",
					    status_file, thread->handle->id,
					    strerror(err));
					sc_syslog_msg_done(&sys_handle);
					rc = -1;
					break;
				}
				if (rc == 0) {
					(void) close(statpfd);
					statpfd = newstatpfd;
					/*
					 * set close on exec so that monitored
					 * processes don't inherit this open
					 * file
					 */
					(void) fcntl(statpfd, F_SETFD,
					    FD_CLOEXEC);
					pollfd.fd = statpfd;
				}
			}
		}

		(void) pthread_mutex_lock(&thread->lock);
		if ((rc == 0) && (thread->monitor_state != PMF_UNMONITORED)) {
			while ((ctlpfd = open(ctl_file, O_RDWR|O_EXCL)) == -1) {
				err = errno;
				if (debug) dbg_msgout(NOGET(
				    "in open(%s) (suspend) errno is %d\n"),
				    status_file, err);
				if (err == EAGAIN)
					continue;
				(void) sc_syslog_msg_initialize(&sys_handle,
				    SC_SYSLOG_PMF_PMFD_TAG, "");
				/*
				 * SCMSGS
				 * @explanation
				 * The rpc.pmfd server was not able to resume
				 * the monitoring of a process because the
				 * rpc.pmfd server was not able to open a
				 * procfs control file. If the system error is
				 * 'Device busy', the procfs control file is
				 * being used by another command (like truss,
				 * pstack, dbx...) and the monitoring of this
				 * process remains suspended. If this is not
				 * the case, the monitoring of this process
				 * has been aborted and can not be resumed.
				 * @user_action
				 * If the system error is 'Device busy', stop
				 * the command which is using the procfs
				 * control file and issue the monitoring
				 * resume command again. Otherwise investigate
				 * if the machine is running out of memory. If
				 * this is not the case, save the syslog
				 * messages file. Contact your authorized Sun
				 * service provider to determine whether a
				 * workaround or patch is available.
				 */
				(void) sc_syslog_msg_log(sys_handle,
				    LOG_ERR, MESSAGE,
				    "pmf_monitor_suspend: Error opening procfs "
				    "control file <%s> for tag <%s>: %s",
				    ctl_file, thread->handle->id,
				    strerror(err));
				sc_syslog_msg_done(&sys_handle);
				ctlpfd = 0;
				if (err == EBUSY) {
					thread->monitor_state = PMF_UNMONITORED;
					(void) pthread_cond_signal(&thread->cv);
				} else {
					rc = -1;
				}
				break;
			}
		}
	}

	*control_fd = ctlpfd;
	*status_fd = statpfd;

	if (rc != 0)
		return (-1);

	/*
	 * Resume the monitoring
	 * We only have to add the triggers if we are at a level
	 * where we must still monitor the children
	 */
	if ((thread->monitor_children == PMF_ALL) ||
	    ((thread->monitor_children == PMF_LEVEL) &&
	    (thread->child_level < thread->handle->monitor_level))) {

		while (pmf_add_triggers(ctlpfd, 1) == -1) {
			err = errno;
			if (debug) dbg_msgout(NOGET(
			    "in pmf_add_triggers (suspend) errno is %d\n"),
			    err);
			if ((err == EAGAIN) &&
			    (pmf_monitor_init(thread, control_fd,
				status_fd) == 0)) {
				ctlpfd = *control_fd;
				continue;
			}
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_PMF_PMFD_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * The rpc.pmfd server was not able to resume the
			 * monitoring of a process, and the monitoring of this
			 * process has been aborted. An error message is
			 * output to syslog.
			 * @user_action
			 * Save the syslog messages file. Contact your
			 * authorized Sun service provider to determine
			 * whether a workaround or patch is available.
			 */
			(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
			    "pmf_monitor_suspend: pmf_add_triggers: %s",
			    strerror(err));
			sc_syslog_msg_done(&sys_handle);
			return (-1);
		}
	}

	thread->monitor_state = PMF_MONITORED;
	(void) pthread_cond_signal(&thread->cv);

	return (0);
}

/*
 * Allocate thread and copy into it relevant handle info that it might use
 * at a later point, while not holding the handle.
 */
static pmf_threads *
pmf_alloc_thread(pmf_handle *handle, int pid, int child_level)
{
	pmf_threads *new;
	sc_syslog_msg_handle_t sys_handle;

	if ((new = (pmf_threads *)svc_alloc(sizeof (pmf_threads),
	&pmf_alloc_cnt)) == NULL) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * The rpc.pmfd server was not able to allocate a new monitor
		 * thread. As a consequence, the rpc.pmfd server was not able
		 * to monitor a process. This problem can occur if the machine
		 * has low memory. An error message is output to syslog.
		 * @user_action
		 * Determine if the machine is running out of memory. If this
		 * is not the case, save the /var/adm/messages file. Contact
		 * your authorized Sun service provider to determine whether a
		 * workaround or patch is available.
		 */
		(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
			"pmf_alloc_thread: ENOMEM");
		sc_syslog_msg_done(&sys_handle);
		return (NULL);
	}

	LOCK_HANDLE(handle);
	LOCK_THREADS(handle);

	/*
	 * Slap it onto the end of the list.
	 */
	if (handle->threads) {
		new->next = handle->threads;
		new->prev = handle->threads->prev;
		handle->threads->prev = new;
		new->prev->next = new;
	} else {
		new->prev = new;
		new->next = new;
		handle->threads = new;
	}

	new->handle = handle;

	/*
	 * Save here handle info we need while we don't hold the handle lock.
	 */
	new->pid = pid;
	new->monitor_children = handle->monitor_children;
	new->signal = handle->signal;
	new->child_level = child_level;

	/*
	 * We special-case a child-level of 0 by setting the
	 * first_call_returned flag to B_TRUE.  That is because we know that
	 * the initial child returns from the fork that created it before
	 * we set up the /proc triggers.
	 *
	 * The rest of the time (every other child level), the new child
	 * process will be stuck in its first fork call, so first_call_returned
	 * must be B_FALSE.
	 */
	if (child_level == 0) {
		new->first_call_returned = B_TRUE;
	} else {
		new->first_call_returned = B_FALSE;
	}

	(void) pthread_mutex_init(&new->lock, NULL);
	(void) pthread_cond_init(&new->cv, NULL);

	new->monitor_state = PMF_STARTING;

	new->normal_exit = B_FALSE;

	UNLOCK_THREADS(handle);
	UNLOCK_HANDLE(handle);

	return (new);
}


static void
pmf_monitor_children(int pid, pmf_handle *handle, int child_level)
{
	pmf_threads *new;
	sc_syslog_msg_handle_t sys_handle;
	int	err;
	uint_t  rc = 0;

	if (debug) dbg_msgout(NOGET("%d in pmf_monitor_children\n"), pid);

	/*
	 * Just a sanity check here for duplicates,
	 * for now we'll just edit them out.
	 */
	LOCK_THREADS(handle);
	for (new = handle->threads; new; new = new->next) {
		if (new->pid == pid) {
			UNLOCK_THREADS(handle);
			if (debug) dbg_msgout(NOGET(
				"found a dup request for %d\n"), pid);
			return;
		}
		if (new->next == handle->threads)
			break;
	}
	UNLOCK_THREADS(handle);

	/*
	 * The rc variable is used to know if the creation of the thread
	 * to monitor the process has succeeeded. Possible values are:
	 *
	 *    rc = 0    no failure
	 *    rc = 1    pmf_alloc_thread() succeeded but pthread_create() failed
	 *		so we need to call pmf_free_thread() before returning
	 *    rc = 2    pmf_alloc_thread() failed
	 */

	/*
	 * Get us a holder for the thread id
	 * so we can rejoin it later.
	 */
	if ((new = pmf_alloc_thread(handle, pid, child_level)) == NULL) {
		rc = 2;
	}

	LOCK_HANDLE(handle);
	if (rc == 0) {
		/*
		 * Create a thread to monitor the process for new children.
		 */
		if (pthread_create(&new->tid, NULL,
			(void *(*)(void *))pmf_search_children, new) != 0) {
			err = errno;
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_PMF_PMFD_TAG, "");
			(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
			    "pthread_create: %s", strerror(err));
			sc_syslog_msg_done(&sys_handle);
			rc = 1;
		}
	}

	if (rc != 0) {
		/*
		 * We failed to create monitor thread for the child process.
		 * We should kill this process, instead of letting it
		 * exist without being monitored.
		 */
		if (kill(pid, SIGKILL)) {
			/*
			 *  Kill failed -- print message
			 *
			 * Don't print message if kill() failed with ESRCH
			 * because it means that the process is already dead.
			 */
			int err1 = errno;
			if (err1 != ESRCH) {
				(void) sc_syslog_msg_initialize(&sys_handle,
				    SC_SYSLOG_PMF_PMFD_TAG, "");
				/*
				 * SCMSGS
				 * @explanation
				 * An error occured while rpc.pmfd attempted
				 * to send a KILL signal to one of the
				 * processes of the given tag. The reason for
				 * the failure is also given. rpc.pmfd
				 * attempted to kill the process because a
				 * previous error occured while creating a
				 * monitor process for the process to be
				 * monitored.
				 * @user_action
				 * Save the /var/adm/messages file. Contact
				 * your authorized Sun service provider to
				 * determine whether a workaround or patch is
				 * available.
				 */
				(void) sc_syslog_msg_log(sys_handle,
				    LOG_ERR, MESSAGE,
				    "pmf_monitor_children: "
				    "Error stopping <%s>: %s",
				    handle->id, strerror(err1));
				sc_syslog_msg_done(&sys_handle);
			}
		}
		/*
		 * Release the handle lock BEFORE calling pmf_free_thread()
		 * because it gets the lock again.
		 */
		UNLOCK_HANDLE(handle);
		if (rc == 1)
			pmf_free_thread(new);
		return;
	} else {
		pmf_thread_cnt++;
	}
	UNLOCK_HANDLE(handle);

}


/*
 * Iterative routine to rejoin all threads associated
 * with the handle.
 */
void
pmf_rejoin(pmf_handle *handle)
{
	pthread_t tid;
	pid_t pid;
	int err;

	LOCK_THREADS(handle);
	while (handle->threads) {
		/*
		 * We need to drop the lock to avoid deadlock on
		 * rejoin.  Therefore, we need to keep temporary
		 * copies of tid and pid because the thread may
		 * disappear from underneath us.
		 */
		tid = handle->threads->tid;
		pid = handle->threads->pid;

		UNLOCK_THREADS(handle);
		if (debug) {
			dbg_msgout(NOGET("rejoining %d (%d)\n"),
			(int)pid, (int)tid);
		}
		err = pthread_join(tid, NULL);
		if (err) {
			if (debug) {
				if (strerror(err))
					dbg_msgout(NOGET("pthread_join: %s\n"),
					    strerror(err));
				else
					dbg_msgout(NOGET(
					    "pthread_join: Unknown error "
					    "(%d)\n"), err);
			}
		} else {
			pmf_thread_cnt--;
		}
		LOCK_THREADS(handle);
	}
	UNLOCK_THREADS(handle);
}

static void
pmf_free_thread(pmf_threads *thread)
{
	pmf_handle *handle = thread->handle;

	LOCK_HANDLE(handle);
	LOCK_THREADS(handle);

	/*
	 * If we're at the beginning of the list, move the
	 * handle pointer up to the next entry, since we'll
	 * be removing this one.
	 */
	if (handle->threads == thread) {
		handle->threads = handle->threads->next;
	}

	/*
	 * Now, extract us from the forward/backward
	 * list pointers (if there are any).
	 */
	if (thread->next) {
		thread->next->prev = thread->prev;
	}
	if (thread->prev) {
		thread->prev->next = thread->next;
	}

	/*
	 * If we're still on the list, we were the only one
	 * left.  Terminate the threads list.
	 */
	if (handle->threads == thread) {
		handle->threads = NULL;
	}

	/*
	 * Following is just out of paranoia.
	 */
	thread->handle = NULL;
	thread->next = NULL;
	thread->prev = NULL;

	(void) pthread_mutex_destroy(&thread->lock);
	(void) pthread_cond_destroy(&thread->cv);

	thread->monitor_state = PMF_EXITING;

	svc_free(thread, &pmf_alloc_cnt);

	UNLOCK_THREADS(handle);
	UNLOCK_HANDLE(handle);
}


/*
 * Prepare future processes for monitoring: mark process control file of
 * current process with requests that children stop on same events as
 * parent, and request that parent stop when certain events happen
 * (fork, exec, exit).
 */
int
pmf_set_up_monitor(pmf_handle *handle)
{
	sc_syslog_msg_handle_t sys_handle;
	int	err;

	char    ctl_file[BUFSIZ];
	int	ctlpfd = 0;

	/*
	 * open process control file
	 */
	(void) sprintf(ctl_file, "/proc/%d/ctl", (int)handle->pid);
	if (debug)
		dbg_msgout(NOGET("file for PCSET is: %s\n"), ctl_file);

	if ((ctlpfd = open(ctl_file, O_WRONLY)) == -1) {
		err = errno;
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * The rpc.pmfd server was not able to open the procfs control
		 * file for the parent process, and the system error is shown.
		 * procfs control files are required in order to monitor user
		 * processes.
		 * @user_action
		 * Determine if the machine is running out of memory. If this
		 * is not the case, save the /var/adm/messages file. Contact
		 * your authorized Sun service provider to determine whether a
		 * workaround or patch is available.
		 */
		(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
		    "Error opening procfs control file (for parent process)"
		    " <%s> for tag <%s>: %s",
		    ctl_file, handle->id, strerror(err));
		sc_syslog_msg_done(&sys_handle);
		return (-1);
	}

	if (pmf_add_triggers(ctlpfd, (handle->monitor_children == PMF_ALL) ||
	    ((handle->monitor_children == PMF_LEVEL) &&
	    (handle->monitor_level > 0))) == -1) {
		err = errno;
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * The rpc.pmfd server was not able to monitor a process, and
		 * the system error is shown. An error message is output to
		 * syslog.
		 * @user_action
		 * Save the syslog messages file. Contact your authorized Sun
		 * service provider to determine whether a workaround or patch
		 * is available.
		 */
		(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
		    "pmf_set_up_monitor: pmf_add_triggers: %s", strerror(err));
		sc_syslog_msg_done(&sys_handle);
		(void) close(ctlpfd);
		return (-1);
	}

	(void) close(ctlpfd);

	/*
	 * Setup a thread to catch new children
	 * The last argument is for child_level 0 = parent.
	 */
	pmf_monitor_children(handle->pid, handle, 0);

	return (0);

}



/*
 * Add the triggers to /proc to monitor a process
 * Return 0 on success otherwise -1 is returned and errno
 * is set:
 *
 *    EINVAL: fatal error, can not recover
 *    EAGAIN: ctlpfd has become invalid
 *    ENOENT: the process has exited
 *
 * Note that on error, triggers may have been partially added.
 */
static int
pmf_add_triggers(int ctlpfd, int monitor_children)
{
	int err;
	sc_syslog_msg_handle_t sys_handle;
	struct {
		long cmd;
		union {
			long flags;
			sysset_t syscalls;
		} arg;
	} ctl;

	/*
	 * We set the inherit-on-fork flag so that any children will stop
	 * on the same triggers we've setup for the parent.
	 * We do it only if the user asked to monitor any children.
	 */
	if (monitor_children) {
		ctl.cmd = PCSET;
		ctl.arg.flags = PR_FORK;
		if (write(ctlpfd, (char *)&ctl, DOUBLE_LONG_SIZE) == -1) {
			err = errno;
			if ((err == EAGAIN) || (err == ENOENT))
				return (-1);
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_PMF_PMFD_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * The rpc.pmfd server was not able to monitor a
			 * process, and the system error is shown. An error
			 * message is output to syslog.
			 * @user_action
			 * Save the /var/adm/messages file. Contact your
			 * authorized Sun service provider to determine
			 * whether a workaround or patch is available.
			 */
			(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
			    "PCSET: %s", strerror(err));
			sc_syslog_msg_done(&sys_handle);
			errno = EINVAL;
			return (-1);
		}
	}

	/*
	 * Currently we only trap for exit from fork and
	 * entry to exec and exit.
	 */
	ctl.cmd = PCSEXIT;
	premptyset(&ctl.arg.syscalls);
	praddset(&ctl.arg.syscalls, SYS_fork);
	praddset(&ctl.arg.syscalls, SYS_vfork);
	praddset(&ctl.arg.syscalls, SYS_fork1);
	if (write(ctlpfd, (char *)&ctl, LONG_SYSSET_SIZE) == -1) {
		err = errno;
		if ((err == EAGAIN) || (err == ENOENT))
			return (-1);
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * The rpc.pmfd server was not able to monitor a process, and
		 * the system error is shown. An error message is output to
		 * syslog.
		 * @user_action
		 * Save the /var/adm/messages file. Contact your authorized
		 * Sun service provider to determine whether a workaround or
		 * patch is available.
		 */
		(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
		    "PCSEXIT: %s", strerror(err));
		sc_syslog_msg_done(&sys_handle);
		errno = EINVAL;
		return (-1);
	}
	ctl.cmd = PCSENTRY;
	premptyset(&ctl.arg.syscalls);
	praddset(&ctl.arg.syscalls, SYS_exec);
	praddset(&ctl.arg.syscalls, SYS_execve);
	/*
	 * Suppress lint message for "constant expression evaluates to zero"
	 * "praddset" is a macro which is described in the proc(4) man page.
	 * The first arg. is a pointer to a bit-set, and the second arg is
	 * an enum value which gives the number of the syscall to be added
	 * to the set. These are given in sys/syscall.h.  SYS_exit happens
	 * to be 1, and because the macro praddset subtracts 1 from the flag,
	 * lint gets confused.
	 */
	praddset(&ctl.arg.syscalls, SYS_exit); /*lint !e778 */
	if (write(ctlpfd, (char *)&ctl, LONG_SYSSET_SIZE) == -1) {
		err = errno;
		if ((err == EAGAIN) || (err == ENOENT))
			return (-1);
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * The rpc.pmfd server was not able to monitor a process, and
		 * the system error is shown. An error message is output to
		 * syslog.
		 * @user_action
		 * Save the /var/adm/messages file. Contact your authorized
		 * Sun service provider to determine whether a workaround or
		 * patch is available.
		 */
		(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
		    "PCSENTRY: %s", strerror(err));
		sc_syslog_msg_done(&sys_handle);
		errno = EINVAL;
		return (-1);
	}

	return (0);
}

/*
 * Remove the triggers from /proc to monitor a process
 * Return 0 on success otherwise -1 is returned and errno
 * is set:
 *
 *    EINVAL: fatal error, can not recover
 *    EAGAIN: ctlpfd has become invalid
 *    ENOENT: the process has exited
 *
 * Note that on error, triggers may have been partially removed.
 */
static int
pmf_remove_triggers(int ctlpfd)
{
	int err = 0;
	sc_syslog_msg_handle_t sys_handle;
	struct {
		long cmd;
		union {
			long flags;
			sysset_t syscalls;
		} arg;
	} ctl;

	ctl.cmd = PCUNSET;
	ctl.arg.flags = PR_FORK;
	if (write(ctlpfd, (char *)&ctl, DOUBLE_LONG_SIZE) == -1) {
		err = errno;
		if ((err == EAGAIN) || (err == ENOENT))
			return (-1);
		(void) sc_syslog_msg_initialize(&sys_handle,
			SC_SYSLOG_PMF_PMFD_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * The rpc.pmfd server was not able to monitor a process, and
		 * the system error is shown. An error message is output to
		 * syslog.
		 * @user_action
		 * Save the /var/adm/messages file. Contact your authorized
		 * Sun service provider to determine whether a workaround or
		 * patch is available.
		 */
		(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
			"PCUNSET: %s", strerror(err));
		sc_syslog_msg_done(&sys_handle);
		errno = EINVAL;
		return (-1);
	}

	premptyset(&ctl.arg.syscalls);
	ctl.cmd = PCSEXIT;
	if (write(ctlpfd, (char *)&ctl, LONG_SYSSET_SIZE) == -1) {
		err = errno;
		if ((err == EAGAIN) || (err == ENOENT))
			return (-1);
		(void) sc_syslog_msg_initialize(&sys_handle,
			SC_SYSLOG_PMF_PMFD_TAG, "");
		(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
			"PCSEXIT: %s", strerror(err));
		sc_syslog_msg_done(&sys_handle);
		errno = EINVAL;
		return (-1);
	}
	ctl.cmd = PCSENTRY;
	if (write(ctlpfd, (char *)&ctl, LONG_SYSSET_SIZE) == -1) {
		err = errno;
		if ((err == EAGAIN) || (err == ENOENT))
			return (-1);
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
		    "PCSENTRY: %s", strerror(err));
		sc_syslog_msg_done(&sys_handle);
		errno = EINVAL;
		return (-1);
	}

	return (0);
}

/*
 * The approach we take here is to search /proc for all processes stuck
 * in the exit from fork with an rval_1 equal to the parent pid (that just
 * died).  It seems that the rval_1 in the child process gives the parent
 * process id (even though the fork syscall doesn't actually return the parent
 * pid, it returns 0 in the child).
 *
 * Note that there is a slight complication, in that sometimes the new process
 * has not progressed far enough in its fork call (due to scheduling) for
 * it to be stopped in the fork call.  Thus, we also look for processes on the
 * system that have triggers to stop on fork, that are in fork, who have
 * a parent that is the process that just died, or is init (because we know
 * the parent process already died, so the process should be marked as
 * orphaned), and is not currently monitored by us.  If we find one, we open
 * the control file for the process and wait for it to stop.  Then we
 * check if it meets the conditions above (stopped in fork, unmonitored, and
 * rval_1 set to process who just died).
 *
 * The idea is that we want to give the process time to run and get "stuck"
 * in its exit from fork.  Because pmf runs in real-time, its threads can
 * dominate the CPU(s).
 *
 * We kill with SIGKILL any stuck children that we find.
 */
static void
pmf_kill_stuck_children(pmf_threads *thread)
{
	DIR *dirp;
	struct dirent *dent = NULL;
	struct dirent *dentp = NULL;
	pstatus_t status;  /* process information structure from /proc */
	sc_syslog_msg_handle_t sys_handle;


	if (debug) {
		dbg_msgout(NOGET("Process %d: in pmf_kill_stuck_children\n"),
		    (int)thread->pid);
	}

	/*
	 * Search the /proc directory for all processes.
	 */
	if ((dirp = opendir("/proc")) == NULL) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * The rpc.pmfd server was unable to open the /proc directory
		 * to find a list of the current processes.
		 * @user_action
		 * Save the /var/adm/messages file. Contact your authorized
		 * Sun service provider to determine whether a workaround or
		 * patch is available.
		 */
		(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
		    "Cannot open /proc directory");
		sc_syslog_msg_done(&sys_handle);
		return;
	}

	/*
	 * Allocate memory for the struct dirent.  For some strange reason,
	 * the actual struct dirent only has a one byte character array for
	 * the d_name filename.  That array is the last field in the struct,
	 * so we're supposed to allocate extra space for the filename, but
	 * cast the memory into a struct dirent.  That means that even though
	 * the prototype for readdir_r takes a struct dirent *, it actually
	 * requires a pointer to more memory.  If we don't do this extra
	 * allocation, readdir_r will overwrite the allocated memory and
	 * will cause a segmentation violation.  I have no idea why readdir_r
	 * works this way.  It seems very error-prone.
	 *
	 * 1024 bytes should be more than enough for the filenames in
	 * /proc (which are only pids).
	 */
	dent = (struct dirent *)malloc(sizeof (struct dirent) + 1024);

	/* for each active process --- */
	while (B_TRUE) {
		int retry_count = 0, retval;

		retval = readdir_r(dirp, dent, &dentp);
		if (retval != 0) {
			/*
			 * Serious error.
			 */
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_PMF_PMFD_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * An error occurred when rpc.pmfd tried to use the
			 * readdir_r function. The reason for the failure is
			 * also given.
			 * @user_action
			 * Save the /var/adm/messages file. Contact your
			 * authorized Sun service provider to determine
			 * whether a workaround or patch is available.
			 */
			(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
			    "readdir_r: %s", strerror(retval));
			sc_syslog_msg_done(&sys_handle);
			break;
		}
		if (dentp == NULL) {
			/* We're at the end of the directory list */
			break;
		}

		if (dent->d_name[0] == '.') {
			/* skip . and .. */
			continue;
		}

		if (strcmp(dent->d_name, "0") == 0 ||
		    strcmp(dent->d_name, "1") == 0) {
			/* skip procs 0 and 1 -- we don't care about them */
			continue;
		}

		/*
		 * We loop on this process until we decide that it's not
		 * a process we care about or we kill it.
		 *
		 * We retry only once after waiting for a process to stop.
		 * We don't want to do so indefinitely.
		 */
		while (retry_count <= 1) {
			if (read_proc_file(dent->d_name, "status",
			    &status) == -1) {
				break;
			}

			/*
			 * Now check if this process is stuck in exit from fork
			 * and the rval_1 return value is the process that
			 * was just killed.
			 *
			 * Suppress the lint error about "Constant expression
			 * evaluates to 0 in operation '-'" PR_STOPPED is 1
			 * and the prismember macro uses flag - 1.
			 */
			/*lint -e778 */
			if (prismember(&status.pr_lwp.pr_flags, PR_STOPPED) &&
			    status.pr_lwp.pr_why == PR_SYSEXIT &&
			    (status.pr_lwp.pr_syscall == SYS_fork ||
			    status.pr_lwp.pr_syscall == SYS_vfork ||
			    status.pr_lwp.pr_syscall == SYS_fork1) &&
			    thread->pid == status.pr_lwp.pr_rval1) {
				if (debug) {
					dbg_msgout(NOGET(
					    "Found stopped proc %d that is "
					    "(orphaned) child of proc %d\n"),
					    status.pr_pid, (int)thread->pid);
				}

				/*
				 * Now kill this new process
				 */
				if (is_process_monitored(status.pr_pid)) {
					if (debug) {
						dbg_msgout(NOGET(
						    "Not killing because "
						    "it's monitored.\n"));
					}
				} else if (kill(status.pr_pid, SIGKILL)) {
					int err1 = errno;

					(void) sc_syslog_msg_initialize(
					    &sys_handle,
					    SC_SYSLOG_PMF_PMFD_TAG, "");
					/*
					 * SCMSGS
					 * @explanation
					 * An error occured while rpc.pmfd
					 * attempted to send SIGKILL to the
					 * specified process. The reason for
					 * the failure is also given.
					 * @user_action
					 * Save the /var/adm/messages file.
					 * Contact your authorized Sun service
					 * provider to determine whether a
					 * workaround or patch is available.
					 */
					(void) sc_syslog_msg_log(sys_handle,
					    LOG_ERR, MESSAGE,
					    "Error killing <%d>: %s",
					    status.pr_pid, strerror(err1));
					sc_syslog_msg_done(&sys_handle);

					if (debug) {
						dbg_msgout(NOGET("Failed to "
						    "kill proc %d\n"),
						    status.pr_pid);
					}
				} else if (debug) {
					dbg_msgout(NOGET("Killed proc %d\n"),
					    status.pr_pid);
				}
			} else if (retry_count == 0 && (status.pr_ppid == 1 ||
			    status.pr_ppid == thread->pid) &&
			    prismember(&status.pr_sysexit, SYS_fork) &&
			    prismember(&status.pr_sysexit, SYS_fork1) &&
			    prismember(&status.pr_sysexit, SYS_vfork)) {
				/*
				 * Here is where we catch a process that
				 * was forked off by the dead parent but
				 * hasn't been scheduled yet, so hasn't
				 * actually completed its fork and stopped
				 * on the exit.  It could have init as its
				 * parent or the pid that forked it
				 * (depending on how soon this thread runs).
				 * The main problem is that the rpc.pmfd runs
				 * in real-time, so the other processes
				 * sometimes don't have time to run.
				 */
				if (debug) {
					dbg_msgout(NOGET(
					    "Process %d is a child of %d, "
					    "and is set to stop on exit from "
					    "fork\n"),
					    status.pr_pid, status.pr_ppid);
				}

				/*
				 * Look up the process to see if we're
				 * monitoring it already.
				 */
				if (is_process_monitored(status.pr_pid)) {
					/*
					 * Because this process is monitored,
					 * we don't care about it here.
					 * Go ahead and move on to the next
					 * process.
					 */
					break;
				}

				/*
				 * We found a process that is a child of init
				 * or the dead parent, is supposed to stop
				 * on fork calls and that we're not currently
				 * monitoring.
				 *
				 * There's a good chance that it's a child
				 * of the process that just died.  The problem
				 * is that it might not have run yet, so
				 * isn't stopped in fork yet.
				 *
				 * Wait for it to stop on the
				 * fork call so we can tell who
				 * its parent was (via the rval_1).
				 *
				 * [Note that wait_for_stop checks to
				 * make sure that the process is actually
				 * in a fork call before waiting for it
				 * to stop.]
				 */
				if (wait_for_stop(status.pr_pid, dent->d_name)
				    == 0) {
					if (debug) {
						dbg_msgout(NOGET("Process %d "
						    "stopped in exit from "
						    "fork.\n"), status.pr_pid);
						}
					retry_count++;
					continue;
				} else {
					if (debug) {
						dbg_msgout(NOGET(
						    "Process %d did not stop "
						    "in exit from fork\n"),
						    status.pr_pid);
					}
					break;
				}
			}
			/*lint +e778 */

			/*
			 * If we get here, go ahead and break out of the
			 * loop to move on to the next process.
			 */
			break;
		}
	}
	(void) closedir(dirp);
	free(dent);
}

/*
 * read_proc_file
 *
 * Opens the file /proc/<procnum>/<procfile>
 * Reads the contents into struct_ptr.  Struct_ptr is treated as
 * a pstatus_t * if procfile is "status" and as a psinfo_t * if
 * procfile is "psinfo."
 *
 * Returns 0 on success, -1 on failure.
 */
static int
read_proc_file(const char *procnum, const char *procfile, void *struct_ptr)
{
	long result;
	char pname[BUFSIZ];
	int procfd; /* filedescriptor for /proc/nnnnn/mmmm */
	sc_syslog_msg_handle_t sys_handle;

	(void) snprintf(pname, (unsigned long) BUFSIZ, "/proc/%s/%s", procnum,
	    procfile);

	while (B_TRUE) {
		if ((procfd = open(pname, O_RDONLY)) == -1) {
			if (debug) {
				dbg_msgout(
					NOGET("Could not open %s\n"), pname);
			}
			return (-1);
		}

		/*
		 * Get the structure for the process and close quickly.
		 * If we get EAGAIN, retry, but on any other error give up.
		 */
		if (strcmp(procfile, "status") == 0) {
			result = read(procfd, (pstatus_t *)struct_ptr,
			    sizeof (pstatus_t));
		} else {
			result = read(procfd, (psinfo_t *)struct_ptr,
			    sizeof (psinfo_t));
		}
		if (result == -1) {
			int saverr = errno;

			if (debug) {
				dbg_msgout(
				    NOGET("Could not read %s file "
				    "for proc %s: errno=%d\n"), procfile,
				    procnum, saverr);
			}

			(void) close(procfd);
			if (saverr != ENOENT) {
				/*
				 * Unexpected error.
				 */
				(void) sc_syslog_msg_initialize(
				    &sys_handle,
				    SC_SYSLOG_PMF_PMFD_TAG, "");
				/*
				 * SCMSGS
				 * @explanation
				 * The rpc.pmfd server was unable to open the
				 * specified file because of the specified
				 * error.
				 * @user_action
				 * Save the /var/adm/messages file. Contact
				 * your authorized Sun service provider to
				 * determine whether a workaround or patch is
				 * available.
				 */
				(void) sc_syslog_msg_log(sys_handle,
				    LOG_ERR, MESSAGE,
				    "Error reading %s: %s",
				    pname, strerror(saverr));
				sc_syslog_msg_done(&sys_handle);
			}
			if (saverr != EAGAIN) {
				return (-1);
			}
		} else {
			(void) close(procfd);
			return (0);
		}
	}
	/* NOT REACHED */
}

/*
 * wait_for_stop
 *
 * Opens the procfs control file for process pid and waits for it to stop
 * (or for an error).  Returns 0 if the process stopped, -1 on an error.
 */
static int
wait_for_stop(pid_t pid, const char *procnum)
{
	psinfo_t psstatus;
	pstatus_t status;
	char ctl_file[BUFSIZ];
	int ctlpfd = -1;
	int err;
	struct {
		long cmd;
		union {
			long flags;
			sysset_t syscalls;
		} arg;
	} ctl;


	if (debug) {
		dbg_msgout(NOGET("In wait_for_stop for pid %d\n"), pid);
	}

	(void) snprintf(ctl_file, (unsigned long) BUFSIZ, "/proc/%d/ctl", pid);

	while (B_TRUE) {
		if ((ctlpfd = open(ctl_file, O_WRONLY | O_EXCL)) == -1) {
			err = errno;
			if (debug) dbg_msgout(NOGET(
			    "in open(%s) errno is %d\n"), ctl_file, errno);
			if (err == EAGAIN)
				continue;
			return (-1);
		}

		/*
		 * set close on exec so that monitored processes
		 * don't inherit this open file
		 */
		(void) fcntl(ctlpfd, F_SETFD, FD_CLOEXEC);

		/*
		 * Before we wait for the process to stop,
		 * read the status to find out whether the process
		 * is still set to stop on exits from fork.
		 *
		 * Also, read the psinfo to find out whether
		 * the process is in fork right now.  We
		 * can't rely on the info in the status because
		 * the prsyscall is set in status only when
		 * the process actually stops.
		 *
		 * We have to check the status and psinfo after we open the
		 * control file to make sure that no other process sneaks in
		 * and removes the triggers or tells the monitored process to
		 * run before we get a chance to wait for it to stop.
		 */

		if (read_proc_file(procnum, "status", &status) == -1 ||
		    read_proc_file(procnum, "psinfo", &psstatus) == -1) {
			/*
			 * Return error.
			 */
			if (debug) {
				dbg_msgout(NOGET("Could not open status or "
				    "psinfo for process %d.  Returning.\n"),
				    pid);
			}
			(void) close(ctlpfd);
			return (-1);
		}

		if (!((psstatus.pr_lwp.pr_syscall == SYS_fork &&
		    prismember(&status.pr_sysexit, SYS_fork)) ||
		    (psstatus.pr_lwp.pr_syscall != SYS_fork1 &&
		    prismember(&status.pr_sysexit, SYS_fork1)) ||
		    (psstatus.pr_lwp.pr_syscall != SYS_vfork &&
		    prismember(&status.pr_sysexit, SYS_vfork)))) {
			/*
			 * The process is not in fork and set to stop on
			 * exit from fork, so we don't care about it.
			 */
			if (debug) {
				dbg_msgout(NOGET(
				    "Process %d is not in fork and set "
				    "to stop on exit from fork.\n"), pid);
			}
			(void) close(ctlpfd);
			return (-1);
		}

		/*
		 * Wait for the process to stop.
		 */
		ctl.cmd = PCWSTOP;
		ctl.arg.flags = 0L;
		while (B_TRUE) {
			long res;
			res = write(ctlpfd, (char *)&ctl, DOUBLE_LONG_SIZE);
			if (res == -1) {
				err = errno;
				if (debug) dbg_msgout(NOGET(
					"in PCWSTOP errno is %d\n"), err);
				if (err == EINTR) {
					/*
					 * We were interrupted by a signal.
					 * Try again.
					 */
					continue;
				}

				(void) close(ctlpfd);
				if (err == EAGAIN) {
					/*
					 * We have to reopen the fd.
					 */
					break;
				}
				/*
				 * For any other error, we give up.
				 */
				return (-1);
			}
			/*
			 * Write returned successfully, meaning the
			 * process is actually stopped.
			 */
			(void) close(ctlpfd);
			return (0);
		}
	}
	/* NOT REACHED */
}
