/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */


/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

/*
 * Linux PMF only:
 *
 * Linux does not support MT RPC services.Hence, we have provided our
 * own MT RPC implementation (see libscutils).
 * There are three public interfaces that each individual rpc server needs to
 * call:
 * - Call rpc_init_rpc_mgr() at the startup to initialize the rpc manager.
 * - Call rpc_req_queue_append() when receives rpc client request.
 * - Call rpc_cleanup_rpc_mgr() to clean up the rpc manager at shut down.
 */

#pragma ident	"@(#)pmf_server.c	1.2	08/05/20 SMI"


#include <stdio.h>
#include <stdlib.h>
#include <rpc/pmap_clnt.h>
#include <string.h>
#include <memory.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <rgm/rpc/pmf.h>
#include <rgm/rpc_services.h>

#ifndef SIG_PF
#define	SIG_PF void(*)(int)
#endif

void
pmf_program_2(struct svc_req *rqstp, register SVCXPRT *transp)
{
	union {
		pmf_start_args pmfproc_start_2_arg;
		pmf_args pmfproc_status_2_arg;
		pmf_args pmfproc_stop_2_arg;
		pmf_args pmfproc_kill_2_arg;
		pmf_modify_args pmfproc_modify_2_arg;
		pmf_args pmfproc_suspend_2_arg;
		pmf_args pmfproc_resume_2_arg;
	} argument;
	union {
		pmf_result pmfproc_start_2_res;
		pmf_status_result pmfproc_status_2_res;
		pmf_result pmfproc_stop_2_res;
		pmf_result pmfproc_kill_2_res;
		pmf_result pmfproc_modify_2_res;
		pmf_result pmfproc_suspend_2_res;
		pmf_result pmfproc_resume_2_res;
	} result;
	bool_t retval;
	xdrproc_t _xdr_argument, _xdr_result;
	bool_t (*local)(char *, void *, struct svc_req *);

	switch (rqstp->rq_proc) {
	case PMFPROC_NULL:
		_xdr_argument = (xdrproc_t)xdr_void;
		_xdr_result = (xdrproc_t)xdr_void;
		local = (bool_t (*) (char *, void *, struct svc_req *))
		    pmfproc_null_2_svc;
		break;

	case PMFPROC_START:
		_xdr_argument = (xdrproc_t)xdr_pmf_start_args;
		_xdr_result = (xdrproc_t)xdr_pmf_result;
		local = (bool_t (*) (char *, void *, struct svc_req *))
		    pmfproc_start_2_svc;
		break;

	case PMFPROC_STATUS:
		_xdr_argument = (xdrproc_t)xdr_pmf_args;
		_xdr_result = (xdrproc_t)xdr_pmf_status_result;
		local = (bool_t (*) (char *, void *, struct svc_req *))
		    pmfproc_status_2_svc;
		break;

	case PMFPROC_STOP:
		_xdr_argument = (xdrproc_t)xdr_pmf_args;
		_xdr_result = (xdrproc_t)xdr_pmf_result;
		local = (bool_t (*) (char *, void *, struct svc_req *))
		    pmfproc_stop_2_svc;
		break;

	case PMFPROC_KILL:
		_xdr_argument = (xdrproc_t)xdr_pmf_args;
		_xdr_result = (xdrproc_t)xdr_pmf_result;
		local = (bool_t (*) (char *, void *, struct svc_req *))
		    pmfproc_kill_2_svc;
		break;

	case PMFPROC_MODIFY:
		_xdr_argument = (xdrproc_t)xdr_pmf_modify_args;
		_xdr_result = (xdrproc_t)xdr_pmf_result;
		local = (bool_t (*) (char *, void *, struct svc_req *))
		    pmfproc_modify_2_svc;
		break;

	case PMFPROC_SUSPEND:
		_xdr_argument = (xdrproc_t)xdr_pmf_args;
		_xdr_result = (xdrproc_t)xdr_pmf_result;
		local = (bool_t (*) (char *, void *, struct svc_req *))
		    pmfproc_suspend_2_svc;
		break;

	case PMFPROC_RESUME:
		_xdr_argument = (xdrproc_t)xdr_pmf_args;
		_xdr_result = (xdrproc_t)xdr_pmf_result;
		local = (bool_t (*) (char *, void *, struct svc_req *))
		    pmfproc_resume_2_svc;
		break;

	default:
		svcerr_noproc(transp);
		return;
	}

	/*
	 * Append the request in the rpc request queue
	 */
	(void) rpc_req_queue_append(transp, sizeof (argument),
	    _xdr_argument, _xdr_result, sizeof (result), local, rqstp);
}
