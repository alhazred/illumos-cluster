/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)pmfd_run.c	1.4	08/05/20 SMI"

/*
 * pmfd_run.c - Workhorse routines for the linux pmf daemon
 */

#include <rgm/rpc/pmf.h>
#include "pmfd.h"
#include <ctype.h>
#include <sys/wait.h>
#include <sys/sc_syslog_msg.h>
#include <sys/rsrc_tag.h>
#include <sys/os_compat.h>
#include <dirent.h>
#include <fcntl.h>
#include <syslog.h>
#include <sys/resource.h>

#ifdef SC_SRM
#include <sys/task.h>
#include <project.h>
#include <pwd.h>

#define	PWD_BUF_SIZE	1024
#endif

#include "pmfd_ptrace.h"
#include "pmfd_monitor.h"
#include "pmfd_dispatch.h"

/* used to encode /proc/<pid>/fd directory path */
#define	PMF_FD_PATH_LEN	64

extern struct pmf_handle_queue pmf_handle_list;

#ifdef LOCK_DEBUG
#define	LOCK_QUEUE(name) \
	if (debug) dbg_msgout(NOGET("%s/%d LOCK QUEUE (0x%x)\n"), \
	NOGET(#name), __LINE__, &pmf_handle_list.lock); \
	CL_PANIC(pthread_mutex_lock(&pmf_handle_list.lock) == 0);
#define	UNLOCK_QUEUE(name) \
	if (debug) dbg_msgout(NOGET("%s/%d UNLOCK QUEUE (0x%x)\n"), \
	NOGET(#name), __LINE__, &pmf_handle_list.lock); \
	CL_PANIC(pthread_mutex_unlock(&pmf_handle_list.lock) == 0);
#else
#define	LOCK_QUEUE(h) \
	CL_PANIC(pthread_mutex_lock(&pmf_handle_list.lock) == 0);
#define	UNLOCK_QUEUE(h) \
	CL_PANIC(pthread_mutex_unlock(&pmf_handle_list.lock) == 0);
#endif /* LOCK_DEBUG */

/*
 * Maximum number of seconds to sleep if we're restarting
 * from the action script too often.
 */
#define	PMF_THROTTLE_LIMIT 30

static int pmf_action_exec(pmf_handle *handle, PMF_ACTION_WHY why);

static pmf_handle *pmf_alloc_handle(pmf_start_args *args,
	security_cred_t *cred,
	pmf_result *result);
static void pmf_free_handle(pmf_handle *handle, bool_t detach_thread);
static void start_process(pmf_handle *handle);
static void perform_throttling(pmf_handle *handle, char *cmd_string);
static pmf_handle *pmf_id_to_handle(char *id);
static pmf_process_tag *pmf_alloc_process_history(pmf_handle *handle);
static void pmf_free_process_history(pmf_process_tag **tag);
static uint_t pmf_process_scan_history(pmf_handle *handle);
static int pmf_run_process(pmf_handle *handle, char **cmd);
static void pmf_free_status(pmf_status_result *result);
static bool_t pmf_throttle_wait(pmf_handle *handle, time_t req);

/*
 * Variables for debugging malloc leakage
 */
int pmf_alloc_cnt = 0;
int pmf_thread_cnt = 0;

/*
 * Use /proc/<pid>/fd directory to get the list of open file descriptors
 * and set close_on_exec flag on them ( >= 3 only ).
 */
void
close_on_exec() {

	int err;
	sc_syslog_msg_handle_t sys_handle;
	DIR *dir;
	struct dirent *ent;
	int i;
	char *path;

	path = (char *)malloc(PMF_FD_PATH_LEN);

	if (path == NULL) {
		(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_PMF_PMFD_TAG, "");
		(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE, "Out of memory.");
		sc_syslog_msg_done(&sys_handle);
		return;
	}

	if (snprintf(path, PMF_FD_PATH_LEN, "/proc/%d/fd", getpid())
	    >= PMF_FD_PATH_LEN) {
		(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_PMF_PMFD_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE, "/proc/<pid>/fd path too long.");
		sc_syslog_msg_done(&sys_handle);
		(void) free(path);
		return;
	}

	dir = opendir(path);
	if (!dir) {
		err = errno;
		(void) sc_syslog_msg_initialize(
				&sys_handle,
				SC_SYSLOG_PMF_PMFD_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		(void) sc_syslog_msg_log(sys_handle,
				LOG_ERR, MESSAGE,
				"opendir: %s", strerror(err));
		sc_syslog_msg_done(&sys_handle);
		(void) free(path);
		return;
	}

	while ((ent = readdir(dir))) {
		/*
		 * On error (ex for "." and ".." entries) atoi return 0.
		 * It�s ok as we skip values 0, 1 and 2 (stdin, stdout, stderr).
		 */
		i = atoi(ent->d_name);
		if (i < 3) continue;
		if (fcntl(i, F_SETFD, FD_CLOEXEC) == -1) {
			if (errno != EBADF) {
				err = errno;
				(void) sc_syslog_msg_initialize(
				    &sys_handle,
				    SC_SYSLOG_PMF_PMFD_TAG, "");
				(void) sc_syslog_msg_log(sys_handle,
				    LOG_ERR, MESSAGE,
				    "fcntl: %s", strerror(err));
				sc_syslog_msg_done(&sys_handle);
			}
		}
	}

	(void) closedir(dir);
	(void) free(path);
}

/*
 * Routines for allocating and freeing process history tags
 */
static pmf_process_tag *
pmf_alloc_process_history(pmf_handle *handle)
{
	pmf_process_tag *new;

	new = (pmf_process_tag *) svc_alloc(sizeof (pmf_process_tag),
		&pmf_alloc_cnt);
	if (new == NULL) {
		// UNLOCK_HANDLE(handle);
		return (NULL);
	}
	(void) memset(new, 0, sizeof (pmf_process_tag));

	/*
	 * Initialize
	 */
	new->etime = (time_t)0;
	new->stime = time(NULL);
	new->wstat = 0;

	/*
	 * Put us at the head of the list
	 */
	new->next = handle->history;
	handle->history = new;

	return (new);
}

/*
 * We free up all the way down to the end, since
 * we are either freeing up the entire list, or
 * entries that are older than we're interested in.
 */
static void
pmf_free_process_history(pmf_process_tag **tag)
{
	if (*tag == NULL)
		return;

	if ((*tag)->next)
		pmf_free_process_history(&(*tag)->next);
	(*tag)->next = NULL;
	svc_free((*tag), &pmf_alloc_cnt);
	*tag = NULL;
}

/*
 * Scan the history list, returning a count
 * of the relevant ones that are there.
 */
static uint_t
pmf_process_scan_history(pmf_handle *handle)
{
	pmf_process_tag **ptr;
	uint_t cnt = 0;

	/*
	 * If the history was reset by a modify command, return 0
	 */
	if (handle->history == NULL)
		return (0);

	/*
	 * Scan through and remove any old (outside of period) entries.
	 */
	for (ptr = &(handle->history); *ptr; ) {
		if ((*ptr)->etime) {
			/*
			 * We simply count all the entries if
			 * the specified period is -1 (infinite).
			 */
			if (handle->period != -1) {
				if (time(NULL) - (*ptr)->etime >
				    handle->period) {
					pmf_free_process_history(ptr);
					break;
				}
			}
			cnt++;
			/*
			 * To avoid having huge history lists, only
			 * allow up to one hundred entries.
			 */
			if (cnt > PMFD_MAX_HISTORY) {
				pmf_free_process_history(ptr);
				break;
			}
		}
		ptr = &(*ptr)->next;
	}

	return (cnt);
}


/*
 * this routine implements throttle wait logic.
 */
static void
perform_throttling(pmf_handle *handle,
    char *cmd_string)
{
	time_t req;
	sc_syslog_msg_handle_t sys_handle;
	req = time(NULL) - handle->last_requeue;

	if (req < PMF_THROTTLE_LIMIT * 2) {
		handle->last_requeue = time(NULL);

		if (req == 0) {
			req++;
		} else if (req < PMF_THROTTLE_LIMIT) {
			req *= 2;
		}
		if (req > PMF_THROTTLE_LIMIT)
			req = PMF_THROTTLE_LIMIT;

		(void) sc_syslog_msg_initialize(
		    &sys_handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_NOTICE, MESSAGE,
		    "\"%s\" restarting too often "
		    "... sleeping %d seconds.",
		    handle->id, req);
		sc_syslog_msg_done(&sys_handle);
		/* post an event */
		(void) sc_publish_event(
		    ESC_CLUSTER_PMF_PROC_NOT_RESTARTED,
		    CL_EVENT_PUB_PMF, CL_EVENT_SEV_INFO,
		    CL_EVENT_INIT_SYSTEM, 0, 0, 0,
		    CL_PMF_NAME_TAG,
		    SE_DATA_TYPE_STRING, handle->id,
		    CL_PMF_CMD_PATH,
		    SE_DATA_TYPE_STRING, cmd_string,
		    CL_FAILURE_REASON, DATA_TYPE_UINT32,
		    CL_FR_PMF_PROC_THROTTLED, NULL);


		/*
		 * Call pmf_throttle_wait
		 * to wait for the requested time
		 * while also waiting for the
		 * stop_monitor flag to be
		 * set to 1.
		 * This is our last chance to bail out
		 * of requeueing the process.
		 */
		if (!pmf_throttle_wait(handle, req)) {
			handle->restart_flag = B_FALSE;
		}
	} else {
		handle->last_requeue = time(NULL);
	}
}

/*
 * This is our thread main ... we simply sit in a loop
 * executing the command we're given until we reach the
 * limit on the number of times we're allowed to do so.
 *
 */

static void
start_process(pmf_handle *handle)
{
	char **aptr;
	char *cmd_string = NULL;
	size_t newlen, oldlen;
	uint_t history_cnt;
	sc_syslog_msg_handle_t sys_handle;
	struct pmf_handle *p;

	if (debug) {
		dbg_msgout(NOGET("starting\n"));
		for (aptr = handle->cmd; aptr && *aptr; aptr++)
			dbg_msgout("\t%s\n", *aptr);
		dbg_msgout("\n");
	}

	/*
	 * get the actual command and the arguments passed
	 *
	 */

	aptr = handle->cmd;

	while (aptr && *aptr) {

		if (cmd_string != NULL) {
			(void) strcat(cmd_string, " "); /* Add a space */
		}

		oldlen = cmd_string ? strlen(cmd_string) : 0;
		newlen = strlen(*aptr) + 1 + 1 + oldlen;
			/* extra 1+1 for the NULL and space */
		cmd_string = (char *)realloc(cmd_string, newlen);
		if (cmd_string == NULL) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_PMF_PMFD_TAG, "");
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE, "Out of memory.");
			sc_syslog_msg_done(&sys_handle);
			UNLOCK_HANDLE(handle);
			pmf_free_handle(handle, TRUE);
			return;
		}
		cmd_string[oldlen] = '\0';
		(void) strcat(cmd_string, *aptr);
		aptr++;
	}

	/*
	 * Loop, restarting the process.  We loop indefinitely.  The conditions
	 * that cause us to break out of the loop are:
	 * 1) We fail to allocate memory for the history file before executing.
	 * 2) We are told to stop monitoring the process (another thread has
	 *	changed the stop_monitor flag to 1.
	 * 3) We've restarted more than handle->retries number of times, and
	 *	we execute an action script that fails.
	 */
loop:
	handle->restart_flag = B_TRUE;
	/* First, attempt to allocate memory for the history */
	if (pmf_alloc_process_history(handle) == NULL) {
		if (debug)
			dbg_msgout(NOGET("fail to alloc_process_history\n"));
		/*
		 * We failed to allocate memory for the history:
		 * log message, free handle and exit this thread.
		 */
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
		    "Tag %s: could not allocate history.",
		    handle->id);
		sc_syslog_msg_done(&sys_handle);
		UNLOCK_HANDLE(handle);
		pmf_free_handle(handle, TRUE);
		free(cmd_string);
		return;
	} else {

		/*
		 * Actually run the process.
		 *
		 * We ignore whether it fails or not, to allow
		 * the following code to cleanup as if it had
		 * exited.
		 *
		 * We enter and exit this routine with handle lock held
		 */
		(void) pmf_run_process(handle, handle->cmd);

	}

	/*
	 * At this point the process has just finished executing.
	 * Here's where we need to restart the process.
	 * If the user asked to stop monitoring simply fall through to
	 * the switch statement, which will break out instead of continuing
	 * the loop.
	 * Otherwise, scan the history list, and if we have
	 * fewer tags than the retry count, start it up again.
	 *
	 * we are holding the handle lock so we can read both action_type and
	 * stop_monitor exclusively with respect to other threads.
	 * We need the handle lock instead of the queue lock because
	 * the queue lock does not protect fields of the handle.  We need
	 * to ensure mutual exclusion here because other threads can modify
	 * both the action_type and stop_monitor fields.
	 */

	/* make sure we're not supposed to stop monitoring */
	if ((handle->action_type == PMFACTION_EXEC) ||
	    ((handle->action_type == PMFACTION_NULL) &&
	    !handle->stop_monitor)) {
		/*
		 * pmf_process_scan_history will return the exit
		 * that just occured.  Since we haven't restarted
		 * for it yet, we need to disregard it so we
		 * only concern ourselves with prior restarts.
		 */
		history_cnt = pmf_process_scan_history(handle);

		if (debug)
			dbg_msgout(NOGET("history_cnt=%d retries=%d\n"),
			    history_cnt, handle->retries);

		if (history_cnt != 0)
			history_cnt--;

		if ((handle->retries == -1) ||
		    ((uint_t)(handle->retries) - history_cnt > 0)) {
			/*
			 * Apply throttling if there is no action script.
			 * or if action script was specified with
			 * infinite retries (-1).
			 * We attempt to quench runaway processes
			 * here by throttling restart requests up
			 * to PMF_THROTTLE_LIMIT seconds.
			 *
			 * Once we start behaving we will restart
			 * immediately without sleeping.
			 */
			if (((handle->action_type == PMFACTION_NULL) ||
			    (handle->retries == -1)) &&
			    (!handle->stop_monitor))
				perform_throttling(handle, cmd_string);

			/*
			 * Check for restart_flag.If its set to false then
			 * another thread requested that we stop monitoring,
			 * by changing the stop_monitor flag.
			 * Do not restart the process.
			 */

			if (handle->restart_flag) {

				if (debug) {
					dbg_msgout(NOGET("restarting\n"));
					for (aptr = handle->cmd; aptr && *aptr;
					    aptr++)
						dbg_msgout("\t%s\n", *aptr);
					dbg_msgout("\n");
				}
				/* post an event */
				(void) sc_publish_event(
				    ESC_CLUSTER_PMF_PROC_RESTART,
				    CL_EVENT_PUB_PMF, CL_EVENT_SEV_INFO,
				    CL_EVENT_INIT_SYSTEM, 0, 0, 0,
				    CL_PMF_NAME_TAG, SE_DATA_TYPE_STRING,
				    handle->id,
				    CL_PMF_CMD_PATH, SE_DATA_TYPE_STRING,
				    cmd_string,
				    CL_TOTAL_ATTEMPTS, DATA_TYPE_UINT32,
				    handle->retries,
				    CL_ATTEMPT_NUMBER, DATA_TYPE_UINT32,
				    history_cnt,
				    CL_REASON_CODE, DATA_TYPE_UINT32,
				    CL_REASON_PMF_RESTART, NULL);
				/*
				 * We haven't reached our restart limit,
				 * so execute the process again .
				 */
				goto loop;
			}
		}
		/* If we get here, we've restarted too many times. */
		/* Fall through to the action switch statement */

	}

	/*
	 * We fell through.  Either we've decided to stop monitoring
	 * this process, or we exceeded the number of retries.
	 * Print a message, then enter the action switch statement
	 * to decide whether to continue restarting or to exit the loop.
	 * Don't print the message if we're stopping monitoring, because
	 * the user doesn't care that it failed to stay up (he or she
	 * wants it to stop).
	 */
	if (PMF_EXIT_QUIETLY_ISCLR(handle->flags) &&
	    !handle->stop_monitor) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		(void) sc_syslog_msg_log(sys_handle, LOG_NOTICE, MESSAGE,
		    "\"%s\" Failed to stay up.", handle->id);
		sc_syslog_msg_done(&sys_handle);

		/* post an event */
		(void) sc_publish_event(
		    ESC_CLUSTER_PMF_PROC_NOT_RESTARTED,
		    CL_EVENT_PUB_PMF, CL_EVENT_SEV_INFO,
		    CL_EVENT_INIT_SYSTEM, 0, 0, 0,
		    CL_PMF_NAME_TAG, SE_DATA_TYPE_STRING,
		    handle->id, CL_PMF_CMD_PATH, SE_DATA_TYPE_STRING,
		    cmd_string, CL_FAILURE_REASON, DATA_TYPE_UINT32,
		    CL_FR_PMF_RETRIES_EXCEEDED, NULL);
	}

	/*
	 * Action switch statement:
	 *
	 * There are two cases: either we have an action to execute or we
	 * don't. If we don't have an action, always break out of the loop.
	 * If we have an action, execute it.  If the action exits abnormally,
	 * break out of the loop and stop restarting.  If the action script
	 * exists with status 0, check if we've restarted too many times
	 * in a short amount of time.  If so, call pmf_throttle_wait
	 * where we sleep for a while to give things a chance to settle down.
	 *
	 * If we are supposed to execute an action script, action_type
	 * will be set to PMFACTION_EXEC.
	 * If we are supposed to stop monitoring, action type will have been
	 * changed to PMFACTION_NULL.  Thus, at this point in the execution,
	 * we don't distinguish between user requested stopping of monitoring
	 * and lack of an action script.  Both cases mean an end to monitoring.
	 */
	switch (handle->action_type) {
		case PMFACTION_NULL:
			break;
		case PMFACTION_EXEC:
			/*
			 * If the action routine exits non zero we
			 * stop, otherwise, clear out the history
			 * and start over.
			 */
			if (pmf_action_exec(handle, PMF_ACTION_FAILED) == 0) {
				/*
				 * Exit restart loop if monitoring has been
				 * stopped while action routine was ongoing.
				 */
				if (handle->stop_monitor) {
					if (debug)
						dbg_msgout(NOGET("Monitoring "
						"thread %s stopped during "
						"execution of \"action\" "
						"routine\n"),
						handle->id);
					break;
				}

				/*
				 * The action script succeeded.
				 * Remove the old history information, since
				 * we're starting from scratch.
				 */
				if (handle->history)
				    pmf_free_process_history(&handle->history);

				/*
				 * We attempt to quench runaway processes
				 * here by throttling restart requests up
				 * to PMF_THROTTLE_LIMIT seconds.
				 *
				 * Once we start behaving we will restart
				 * immediately without sleeping.
				 */
				perform_throttling(handle,
				    cmd_string);

				if (!handle->restart_flag)
					break;

				if (PMF_EXIT_QUIETLY_ISCLR(handle->flags)) {
					(void) sc_syslog_msg_initialize(
					    &sys_handle,
					    SC_SYSLOG_PMF_PMFD_TAG, "");
					/*
					 * SCMSGS
					 * @explanation
					 * The tag shown has exited and was
					 * restarted by the rpc.pmfd server.
					 * An error message is output to
					 * syslog.
					 * @user_action
					 * This message is informational; no
					 * user action is needed.
					 */
					(void) sc_syslog_msg_log(sys_handle,
					    LOG_NOTICE, MESSAGE,
					    "\"%s\" requeued",
					    handle->id);
					sc_syslog_msg_done(&sys_handle);
				}
				/*
				 * The action script succeeded, so we
				 * continue restarting.
				 */

				/* post an event */
				(void) sc_publish_event(
				    ESC_CLUSTER_PMF_PROC_RESTART,
				    CL_EVENT_PUB_PMF, CL_EVENT_SEV_INFO,
				    CL_EVENT_INIT_SYSTEM, 0, 0, 0,
				    CL_PMF_NAME_TAG, SE_DATA_TYPE_STRING,
				    handle->id,
				    CL_PMF_CMD_PATH, SE_DATA_TYPE_STRING,
				    cmd_string,
				    CL_TOTAL_ATTEMPTS, DATA_TYPE_UINT32,
				    handle->retries,
				    CL_ATTEMPT_NUMBER, DATA_TYPE_UINT32, 0,
				    CL_REASON_CODE, DATA_TYPE_UINT32,
				    CL_REASON_PMF_ACTION_SUCCESS, NULL);
				goto loop;
			} else {
			/*
			 * The action routine failed -- inform user
			 */
				(void) sc_syslog_msg_initialize(&sys_handle,
				    SC_SYSLOG_PMF_PMFD_TAG, "");
				/*
				 * SCMSGS
				 * @explanation
				 * The given tag has exceeded the allowed
				 * number of retry attempts (given by the
				 * 'pmfadm -n' option) and the action (given
				 * by the 'pmfadm -a' option) was initiated by
				 * rpc.pmfd. The action failed (i.e., returned
				 * non-zero), and rpc.pmfd will delete this
				 * tag from its tag list and discontinue retry
				 * attempts.
				 * @user_action
				 * This message is informational; no user
				 * action is needed.
				 */
				(void) sc_syslog_msg_log(sys_handle,
				    LOG_INFO, MESSAGE,
				    "\"pmfadm -a\" Action failed for <%s>",
				    handle->id);
				sc_syslog_msg_done(&sys_handle);

				/* post an event */
				(void) sc_publish_event(
				    ESC_CLUSTER_PMF_PROC_NOT_RESTARTED,
				    CL_EVENT_PUB_PMF, CL_EVENT_SEV_INFO,
				    CL_EVENT_INIT_SYSTEM, 0, 0, 0,
				    CL_PMF_NAME_TAG, SE_DATA_TYPE_STRING,
				    handle->id,
				    CL_PMF_CMD_PATH, SE_DATA_TYPE_STRING,
				    cmd_string,
				    CL_FAILURE_REASON, DATA_TYPE_UINT32,
				    CL_FR_PMF_ACTION_FAILED, NULL);
			}
			break;
		default:
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_PMF_PMFD_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * An internal error has occured in the rpc.pmfd
			 * server. This should not happen.
			 * @user_action
			 * Save the /var/adm/messages file. Contact your
			 * authorized Sun service provider to determine
			 * whether a workaround or patch is available.
			 */
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_WARNING, MESSAGE,
			    "pmfd: unknown action (0x%x)",
			    handle->action_type);
			sc_syslog_msg_done(&sys_handle);
			break;
	}

	/*
	 * If we get here, either the action script failed, there was no
	 * action script, or another thread requested that we stop monitoring,
	 * by changing the stop_monitor flag.
	 * In any case it means stop restarting the process.
	 */

	free(cmd_string);


	if (debug) {
		dbg_msgout(NOGET("Monitoring thread %s exiting\n"),
		    handle->id);
	}

	/*
	 * Unlink this handle from global list
	 * so that nobody can reference it.
	 * We must first release handle lock
	 * to avoid dead lock.
	 */
	UNLOCK_HANDLE(handle);
	LOCK_QUEUE(handle_release);
	if (handle == pmf_handle_list.head) {
		pmf_handle_list.head = handle->next;
		handle->next = (struct pmf_handle *)0;
		pmf_handle_list.count--;
	} else {
		for (p = pmf_handle_list.head; p; p = p->next) {
			if (p->next == handle) {
				p->next = handle->next;
				handle->next = (struct pmf_handle *)0;
				pmf_handle_list.count--;
				break;
			}
		}
	}
	LOCK_HANDLE(handle);
	UNLOCK_QUEUE(handle_release);

	/*
	 * Signal exit event and return event to pool
	 */
	if (handle->exit_event != (struct pmf_event *)0) {
		LOCK_EVENT(handle->exit_event);
		pthread_cond_broadcast(&handle->exit_event->cond);
		UNLOCK_EVENT(handle->exit_event);
		(void) return_pmf_event_to_pool(&handle->exit_event);
	} else {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_WARNING, MESSAGE,
		    "pmfd: cannot release handle (%s)",
		    handle->id);
		sc_syslog_msg_done(&sys_handle);
	}

	/*
	 * This routine should only be called from here.
	 * Otherwise, locking needs to be enhanced to allow
	 * other threads to free a handle without stepping
	 * on us.
	 */
	pmf_free_handle(handle, TRUE);

	(void) pthread_exit((void *)0);

}
/*
 * pmf_throttle_wait
 *
 * Returns TRUE if we timeout and the stop_monitor flag is FALSE.
 * Returns FALSE if the stop_monitor flag is TRUE or if there is an
 * error while waiting on the event queue.  We will wait for req time
 * or until a stop/exit event is pushed in the event queue.
 */
static bool_t
pmf_throttle_wait(pmf_handle *handle, time_t req)
{
	int err = 0;
	sc_syslog_msg_handle_t sys_handle;

	if (debug)
		dbg_msgout(NOGET("entering pmf_throttle_wait\n"));

	/*
	 * Instead of just sleeping, we do a timed wait on the condition
	 * var that will be signaled if we're supposed to stop monitoring
	 * the process. That way we don't wait for a long time before stopping
	 * monitoring, if the user decides to stop monitoring while we're
	 * waiting.
	 */
	err = pmf_throttle_loop(handle, req);

	/* Check if we timed out */
	if (err == ETIMEDOUT) {
		if (debug)
			dbg_msgout(NOGET("timedout\n"));
	} else if (err != 0) {
		/*
		 * We've received an unexpected error from the
		 * pthread_cond_timedwait call.  Write a syslog message
		 * and return FALSE so that this tag will not be requeued.
		 */
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * An internal error has occured in the rpc.pmfd server while
		 * waiting before restarting the specified tag. rpc.pmfd will
		 * delete this tag from its tag list and discontinue retry
		 * attempts.
		 * @user_action
		 * If desired, restart the tag under pmf using the 'pmfadm -c'
		 * command.
		 */
		(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
		    "Tag %s: error number %d in throttle wait; "
		    "process will not be requeued.", handle->id, err);
		sc_syslog_msg_done(&sys_handle);
		return (FALSE);
	}
	/*
	 * If we are supposed to stop
	 * monitoring, return FALSE
	 */
	if (handle->stop_monitor) {
		if (debug)
			dbg_msgout(NOGET("Caught the stop_monitor flag\n"));
		return (FALSE);
	}
	return (TRUE);
}

static int
pmf_run_process(pmf_handle *handle, char **cmd)
{
	int err = 0, err1;
	sc_syslog_msg_handle_t sys_handle;

	/*
	 * Child in pre-exec stage will request itself to be monitored by its
	 * parent.
	 * As we hold the handle lock, we are sure that dispatching thread will
	 * be blocked when dealing with this child events (or children of this
	 * child event) until we had registered this child.
	 */
	if ((handle->pid = fork()) != 0) {
		/*
		 * Parent
		 */
		if (handle->pid == -1) {
			err1 = errno;
			handle->result.code.type = PMF_SYSERRNO;
			handle->result.code.sys_errno = errno;
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_PMF_PMFD_TAG, "");
			(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
			    "fork: %s", strerror(err1));
			sc_syslog_msg_done(&sys_handle);
			err = -1;
			goto bad;
		}

		handle->alive = 1;

	} else {
		/*
		 * Child
		 */

		/*
		 * Make sure we're not running children as realtime
		 * processes.  We restore the scheduling parameters
		 * that rpc.pmfd was started with ... by default TS.
		 *
		 * The schedule information should be passed in
		 *	the RPC request packet and restored, but there
		 *	are security reasons for not doing so.  Force
		 *	the user to reestablish any special scheduling.
		 */
		/*
		 * In debug mode we skip this because we also skipped
		 * setting the priority to RT in svc_init() in pmfd_main.c.
		 * Both functions are in libscutils.
		 */
		if (!debug && svc_restore_priority() == 1) {
			goto badchild;
		}

		/*
		 * Set the fd limit down so that a child ignorant of the fact
		 * that we set FD_CLOEXEC and ignorant of closefrom(3C) doesn't
		 * spend forever trying to close file descriptors upon
		 * daemonization.
		 */
		if (setrlimit(RLIMIT_NOFILE, &pmf_rlimit) == -1) {
			err = errno;
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_PMF_PMFD_TAG, "");
			(void) sc_syslog_msg_log(sys_handle, LOG_ERR,
			    MESSAGE,
			    "setrlimit before exec: %s", strerror(err));
			sc_syslog_msg_done(&sys_handle);
		}

		/*
		 * Restore blocked signal mask
		 */

		if (svc_sigrestoremask() != 0) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_PMF_PMFD_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * Need explanation of this message!
			 * @user_action
			 * Need a user action for this message.
			 */
			(void) sc_syslog_msg_log(sys_handle, LOG_ERR,
			    MESSAGE,
			    "svc_sigrestoremask failed.");
			sc_syslog_msg_done(&sys_handle);
		}

		/*
		 * Set close on exec on all open file descriptor
		 */
		close_on_exec();

#ifdef SC_SRM
		/*
		 * Set project to the specified project name before starting
		 * the application.  If setproject() call fails, the
		 * application will be started with the system default project.
		 *
		 * Note that we must call setproject before we set the uid
		 * of the process, because setproject can only be called by
		 * root.
		 */
		if (handle->project_name != NULL &&
		    *handle->project_name != '\0') {
			struct passwd pwd, *pwdp;
			char pwd_buff[PWD_BUF_SIZE];

			if (getpwuid_r(handle->uid, &pwd, pwd_buff,
			    PWD_BUF_SIZE, &pwdp) == 0) {
				if (setproject(handle->project_name,
				    pwd.pw_name, TASK_NORMAL) == -1) {
					err1 = errno;
					(void) sc_syslog_msg_initialize(
					    &sys_handle,
					    SC_SYSLOG_PMF_PMFD_TAG, "");
					(void) sc_syslog_msg_log(
					    sys_handle, LOG_WARNING, MESSAGE,
					    "setproject: %s; attempting to "
					    "continue the process with the "
					    "system default project.",
					    strerror(err1));
					sc_syslog_msg_done(&sys_handle);
					(void) setproject("default",
					    pwd.pw_name, TASK_NORMAL);
				} else {
					if (debug) {
						dbg_msgout(NOGET(
						    "Change project name to "
						    "%s\n"),
						    handle->project_name);
					}
				}
			}
		} else {
			/* project is not specified; set to default project */
			struct passwd pwd, *pwdp;
			char pwd_buff[PWD_BUF_SIZE];
			if (getpwuid_r(handle->uid, &pwd, pwd_buff,
			    PWD_BUF_SIZE, &pwdp) == 0) {
				(void) setproject("default", pwd.pw_name,
				    TASK_NORMAL);
			} else {
				(void) setproject("default", "root",
				    TASK_NORMAL);
			}
		}
#endif

		/*
		 * We must set the groupid before resetting
		 * our userid - otherwise we won't have
		 * permission to do the former.
		 */
		if ((handle->gid != 0) && (setgid(handle->gid) == -1)) {
			err1 = errno;
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_PMF_PMFD_TAG, "");
			(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
			    "setgid: %s", strerror(err1));
			sc_syslog_msg_done(&sys_handle);
			goto badchild;
		}

		if ((handle->uid != 0) && (setuid(handle->uid) == -1)) {
			err1 = errno;
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_PMF_PMFD_TAG, "");
			(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
			    "setuid: %s", strerror(err1));
			sc_syslog_msg_done(&sys_handle);
			goto badchild;
		}

		if ((handle->pwd != NULL) && (chdir(handle->pwd) == -1)) {
			err1 = errno;
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_PMF_PMFD_TAG, "");
			(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
			    "chdir: %s", strerror(err1));
			sc_syslog_msg_done(&sys_handle);
			goto badchild;
		}

		/*
		 * if user asked to pass env. vars, don't set the path;
		 * otherwise set it to the caller path.
		 */
		if (!handle->env_passed && (handle->path != NULL)) {
			if (putenv(handle->path) != 0) {
				err1 = errno;
				(void) sc_syslog_msg_initialize(&sys_handle,
				    SC_SYSLOG_PMF_PMFD_TAG, "");
				(void) sc_syslog_msg_log(sys_handle, LOG_ERR,
				    MESSAGE, "putenv: %s",
				    strerror(err1));
				sc_syslog_msg_done(&sys_handle);
				goto badchild;
			}
		}

		/*
		 * Don�t use dbg_msgout as, at fork(), we may have inherited
		 * from locks that will never be released.
		 */
		if (debug) {
			dbg_msgout_nosyslog(NOGET("Set everything up; posting "
			    "to parent proc.\n"));
		}

		/*
		 * Now that we have set up everything before
		 * execing, we can tell our parent to trace us
		 */
		if ((err1 = pmf_ptrace_traceme(handle))) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_PMF_PMFD_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * Need explanation of this message!
			 * @user_action
			 * Need a user action for this message.
			 */
			(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
			    "pmf_ptrace_traceme: %s", strerror(err1));
			sc_syslog_msg_done(&sys_handle);
			goto badchild;
		}

		/*
		 * if user wants to use environment passed to pmfadm,
		 * call execve and pass the environment;
		 * else call execvp, and inherit the parent's environment,
		 * plus the path set to that of the caller.
		 */
		if (handle->env_passed) {
			if (execve(cmd[0], cmd, handle->env) == -1) {
				err1 = errno;
				(void) sc_syslog_msg_initialize(&sys_handle,
				    SC_SYSLOG_PMF_PMFD_TAG, "");
				/*
				 * SCMSGS
				 * @explanation
				 * Need explanation of this message!
				 * @user_action
				 * Need a user action for this message.
				 */
				(void) sc_syslog_msg_log(sys_handle, LOG_ERR,
				    MESSAGE, "execve: %s",
				    strerror(err1));
				sc_syslog_msg_done(&sys_handle);
				_exit(1);
			}
		} else {
			if (execvp(cmd[0], cmd) == -1) {
				err1 = errno;
				(void) sc_syslog_msg_initialize(&sys_handle,
				    SC_SYSLOG_PMF_PMFD_TAG, "");
				/*
				 * SCMSGS
				 * @explanation
				 * Need explanation of this message!
				 * @user_action
				 * Need a user action for this message.
				 */
				(void) sc_syslog_msg_log(sys_handle, LOG_ERR,
				    MESSAGE, "execvp: %s",
				    strerror(err1));
				sc_syslog_msg_done(&sys_handle);
				_exit(1);
			}
		}

		/* NOTREACHED */
		exit(1);
badchild:
		/*
		 * Let our parent continue.
		 */
		exit(1);
	}


	/*
	 * Should be done before dispatching thread get a chance to
	 * reference it.
	 * This is why we get the lock before the fork
	 */
	(void) pmf_register_initial_process(handle, handle->pid, getpid());

	/*
	 * Waiting loop could have been paused if there were no others
	 * processes to monitor. Awake it.
	 */
	(void) pmf_awake_dispatching_thread();

	/*
	 * Consume events raised by our ptraced children tree.
	 * We should exit only whith last monitored child
	 * We must enter with lock queue held in order to not
	 * miss events that could be raised concurrently by
	 * waiting loop.
	 */
	(void) pmf_loop_consume_events_lock_held(handle);

	handle->alive = 0;

	/*
	 * Get the status of our main process if monitoring has
	 * not been stopped.
	 */
	if (handle->stop_monitor) {
		err = 0;

		if (debug)
			dbg_msgout(NOGET("Monitoring thread %s "
			    "exiting from monitoring loop, monitoring "
			    "has been stopped \n"),
			    handle->id);

	} else {
		err = handle->status;

		if (handle->history)
			handle->history->wstat = err;

		if (debug) {
			if (WIFSIGNALED(err)) {
				dbg_msgout(NOGET
				    ("Monitoring thread %s "
				    "last monitored process exited, "
				    "direct child process "
				    "exited on signal %d\n"),
				    handle->id, WTERMSIG(err));
			} else {
				dbg_msgout(NOGET
				    ("Monitoring thread %s "
				    "last monitored process exited, "
				    "direct child process "
				    "exited with status %d\n"),
				    handle->id, WEXITSTATUS(err));
			}
		}

		/*
		 * Save our exit time for pmf_process_scan_history to
		 * evaluate.
		 */
		if (handle->history)
			handle->history->etime = time(NULL);
	}

	/*
	 * We hold the handle tree lock when leaving this routine
	 */

bad:

	return (err);
}

/*
 * Routine for allocating request handle. It expects the caller to
 * hold the queue lock.
 *
 * Note that we check the parameters for the empty string "" as well as for
 * NULL because the pmf marshalling converts NULLs to empty strings.
 */
static pmf_handle *
pmf_alloc_handle(pmf_start_args *args,
security_cred_t *cred,
pmf_result *result)
{
	pmf_handle *new = NULL;
	size_t i, bufsize;

	/*
	 * Check to make sure the identifier used is unique
	 */
	for (new = pmf_handle_list.head; new; new = new->next) {
		if (strcmp(new->id, args->identifier) == 0) {
			/*
			 * The request specified a duplicate
			 * identifier.
			 */
			result->code.type = PMF_DUP;
			return (NULL);
		}
	}

	new = (pmf_handle *) svc_alloc(sizeof (pmf_handle), &pmf_alloc_cnt);
	if (new == NULL) {
		result->code.type = PMF_SYSERRNO;
		result->code.sys_errno = ENOMEM;
		return (new);
	}

	pmf_thread_cnt++;

	/*
	 * Initialize all of handle to 0. This initializes the error codes
	 * in handle->result.code to 0 (success).
	 */
	(void) memset(new, 0, sizeof (pmf_handle));

	new->exit_event = get_pmf_event_from_pool();
	if (new->exit_event == (struct pmf_event *)0) {
		goto cleanup_handle_nomem;
	}

	new->next = (struct pmf_handle *)0;
	new->head = (struct child_handle *)0;
	new->queue.head = (struct pmf_event *)0;
	new->state = PMF_TREE_STARTING;

	/*
	 * Duplicate the argument list, making sure that the last element
	 * is NULL, not just empty, for exec().  We assume that the last
	 * element in the array passed to this function should be NULL,
	 * but that the rpc command might have changed it to an empty string.
	 * Thus, we copy only the strings up to (length - 1), then just assign
	 * a NULL to the final element.
	 *
	 * Calculate the bufsize ahead so we can use it in memset below.
	 */
	bufsize = (size_t)(args->cmd.cmd_len * sizeof (char *));
	new->cmd = (char **)svc_alloc(bufsize, &pmf_alloc_cnt);
	if (new->cmd == NULL) {
		goto cleanup_handle_nomem;
	}
	(void) memset(new->cmd, 0, bufsize);

	for (i = 0; i < args->cmd.cmd_len - 1; i++) {
		if (args->cmd.cmd_val[i] && *args->cmd.cmd_val[i]) {
			new->cmd[i] = svc_strdup(args->cmd.cmd_val[i],
				&pmf_alloc_cnt);
			if (new->cmd[i] == NULL) {
				goto cleanup_handle_nomem;
			}
		} else {
			/*
			 * Subsequent code cannot handle a NULL except at
			 * the end of the array.  Thus, we need to bail out
			 * here.
			 */
			new->cmd[i] = NULL;
			result->code.type = PMF_SYSERRNO;
			result->code.sys_errno = EINVAL;
			goto cleanup_handle;
		}
	}
	/*
	 * We need to end the list with a NULL pointer, for exec.
	 */
	new->cmd[i] = NULL;

	/*
	 * Duplicate the env vars list, making sure that the last element
	 * is NULL, not just empty, for exec().
	 * Calculate the bufsize ahead so we can use it in memset below.
	 * Only allocate memory for the environment if we have an environment
	 * passed to us.
	 */
	if (args->env_passed) {
		bufsize = (size_t)((args->env.env_len + 1) * sizeof (char *));
		new->env = (char **)svc_alloc(bufsize, &pmf_alloc_cnt);
		if (new->env == NULL) {
			goto cleanup_handle_nomem;
		}
		(void) memset(new->env, 0, bufsize);

		for (i = 0; i < args->env.env_len; i++) {
			if (args->env.env_val[i] && *args->env.env_val[i]) {
				new->env[i] = svc_strdup(args->env.env_val[i],
				    &pmf_alloc_cnt);
				if (new->env[i] == NULL) {
					goto cleanup_handle_nomem;
				}
			} else {
				/*
				 * Subsequent code cannot handle a NULL
				 * except in the last element.
				 */
				new->env[i] = NULL;
				result->code.type = PMF_SYSERRNO;
				result->code.sys_errno = EINVAL;
				goto cleanup_handle;
			}
		}
		/*
		 * NULL was not sent by the library, adding it now
		 */
		new->env[i] = NULL;
	}

	/*
	 * Stuff the remaining arguments into our handle.
	 */
	if (args->pwd && *args->pwd) {
		new->pwd = svc_strdup(args->pwd, &pmf_alloc_cnt);
		if (new->pwd == NULL) {
			goto cleanup_handle_nomem;
		}
	} else {
		/*
		 * Subsequent code checks for and handles a NULL ptr here.
		 */
		new->pwd = NULL;
	}

	if (args->path && *args->path) {
		new->path = svc_strdup(args->path, &pmf_alloc_cnt);
		if (new->path == NULL) {
			goto cleanup_handle_nomem;
		}
	} else {
		/*
		 * Subsequent code checks for and handles a NULL ptr here.
		 */
		new->path = NULL;
	}

	if (args->project_name && *args->project_name) {
		new->project_name = svc_strdup(args->project_name,
		    &pmf_alloc_cnt);
		if (new->project_name == NULL) {
			goto cleanup_handle_nomem;
		}
	} else {
		/*
		 * Subsequent code can handle a NULL project_name.
		 */
		new->project_name = NULL;
	}

	if (args->retries > PMFD_MAX_HISTORY) {
		new->id = NULL;
		result->code.type = PMF_SYSERRNO;
		result->code.sys_errno = E2BIG;
		goto cleanup_handle;
	}

	new->retries		= args->retries;
	if (args->identifier && *args->identifier) {
		new->id = svc_strdup(args->identifier, &pmf_alloc_cnt);
		if (new->id == NULL) {
			goto cleanup_handle_nomem;
		}
	} else {
		/*
		 * Subsequent code cannot handle a NULL here,
		 * so we must bail out.
		 */
		new->id = NULL;
		result->code.type = PMF_SYSERRNO;
		result->code.sys_errno = EINVAL;
		goto cleanup_handle;
	}
	new->action_type	= args->action_type;
	if (args->action_type == PMFACTION_EXEC) {
		if (args->action && *args->action) {
			new->action = svc_strdup(args->action, &pmf_alloc_cnt);
			if (new->action == NULL) {
				goto cleanup_handle_nomem;
			}
		} else {
			/*
			 * Subsequent code cannot handle a NULL here.
			 */
			new->action = NULL;
			result->code.type = PMF_SYSERRNO;
			result->code.sys_errno = EINVAL;
			goto cleanup_handle;
		}
	} else {
		new->action	= NULL;
	}

	new->history		= NULL;
	new->monitor_children 	= args->monitor_children;
	new->monitor_level    	= args->monitor_level;
	new->env_passed		= args->env_passed;
	new->uid 		= cred->aup_uid;
	new->gid 		= cred->aup_gid;

	/*
	 * Currently only contains an internal flag to quiet
	 * the daemon on process failure.
	 */
	new->flags		= args->flags;

	/*
	 * Period is specified in minutes by the user, and
	 * is calculated using time_t (seconds).
	 */
	if (args->period != -1)
		new->period = (time_t)(args->period);
	else
		new->period = (time_t)-1;

	(void) pthread_mutex_init(&new->handle_lock, NULL);
	(void) pthread_cond_init(&new->handle_cond, NULL);
	(void) pthread_mutex_init(&new->queue.lock, NULL);
	(void) pthread_cond_init(&new->queue.cond, NULL);

	return (new);

cleanup_handle_nomem:
	result->code.type = PMF_SYSERRNO;
	result->code.sys_errno = ENOMEM;

cleanup_handle:
	/*
	 * something went wrong: free handle and return null;
	 * we have to release the lock because pmf_free_handle() will grab it.
	 */
	UNLOCK_QUEUE(pmf_alloc_handle);
	pmf_free_handle(new, FALSE);
	LOCK_QUEUE(pmf_alloc_handle);
	return (NULL);

}

/*
 * Routine for getting the request handle based on tag id.
 * It expects that the caller holds the queue lock.
 */
static pmf_handle *
pmf_id_to_handle(char *id)
{
	pmf_handle *ptr;

	/*
	 * Start at the base and search for the specified id
	 * It may have been removed before we got a chance to
	 * search a second time for it, so the -1 return value
	 * is not that important to notice.
	 */
	for (ptr = pmf_handle_list.head; ptr; ptr = ptr->next) {
		if (ptr->id != NULL && strcmp(id, ptr->id) == 0) {
			break;
		}
	}

	return (ptr);
}

/*
 * pmf_free_handle
 *
 * Routine for freeing request handle. It expects that the caller
 * DOESN'T hold the queue lock.
 * This routine should only be called from two places:
 * the end of start_process, and from pmf_alloc_handle, if the allocation
 * partly fails.
 * Otherwise, locking needs to be enhanced to allow
 * other threads to free a handle without stepping on us.
 */
static void
pmf_free_handle(pmf_handle *handle, bool_t detach_thread)
{
	int err;
	char **ptr;
	sc_syslog_msg_handle_t sys_handle;
	struct pmf_event *event;

	/*
	 * Purge the event queue. It should be empty
	 */
	while ((event = dequeue_pmf_event(&handle->queue))) {

		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
		    "pmf_free_handle, queued event of type %d found.",
		    event->type);
		sc_syslog_msg_done(&sys_handle);

		switch (event->type) {
		case PMFD_CHILD:
		case PMFD_UNREGISTERED_CHILD:
			/*
			 * This is an event related to a process we
			 * were monitoring. There is no more ressource
			 * to deal with it now. GC has to deal with it.
			 */
			pmf_send_event_to_GC(event);
			break;
		default:
			/*
			 * Set error code and signal that
			 * event was processed and then
			 * return it to pool
			 */
			event->return_code.type = PMF_SYSERRNO;
			event->return_code.sys_errno = EINVAL;
			(void) pthread_cond_signal(&event->cond);
			(void) return_pmf_event_to_pool(&event);
			break;
		}
	}

	/*
	 * Now, free up any internal strings/structures
	 * that we previously malloc'd and free the
	 * handle.
	 */
	if (handle->cmd) {
		ptr = handle->cmd;
		while (*ptr)
			svc_free(*ptr++, &pmf_alloc_cnt);
		svc_free(handle->cmd, &pmf_alloc_cnt);
	}

	if (handle->env) {
		ptr = handle->env;
		while (*ptr) {
			svc_free(*ptr++, &pmf_alloc_cnt);
		}
		svc_free(handle->env, &pmf_alloc_cnt);
	}
	if (handle->pwd)
		svc_free((void *)handle->pwd, &pmf_alloc_cnt);
	if (handle->path)
		svc_free((void *)handle->path, &pmf_alloc_cnt);
	if (handle->id)
		svc_free((void *)handle->id, &pmf_alloc_cnt);
	if (handle->action)
		svc_free((void *)handle->action, &pmf_alloc_cnt);
	if (handle->project_name)
		svc_free((void *)handle->project_name, &pmf_alloc_cnt);
	if (handle->history)
		pmf_free_process_history(&handle->history);

	(void) pthread_mutex_destroy(&handle->handle_lock);
	(void) pthread_mutex_destroy(&handle->queue.lock);
	(void) pthread_cond_destroy(&handle->handle_cond);
	(void) pthread_cond_destroy(&handle->queue.cond);

	if (debug) dbg_msgout(NOGET("destroyed 0x%x\n"),
		&handle->handle_cond);

	/*
	 * The thread that originally created us is gone.
	 * Since there's nobody to join us we simply detach
	 * so we're cleaned up properly on exit.
	 *
	 * However, it's possible that the start command may not
	 * have spawned the new thread yet, so we don't need to detach.
	 * Thus, check the detach_thread flag first.
	 */
	if (detach_thread && (err = pthread_detach(pthread_self())) != 0) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
		    "pthread_detach: %s", strerror(err));
		sc_syslog_msg_done(&sys_handle);
	} else {
		pmf_thread_cnt--;
	}

	/*
	 * Now free the handle
	 */
	svc_free((void *)handle, &pmf_alloc_cnt);

}

/*
 * Routine for freeing malloced fields of status result.
 * Status result itself is not malloced, so it's not freed.
 * Used only if a malloc or strdup went wrong; otherwise fields are freed by RPC
 * on the server side; they are realloced by RPC on the client side
 * and freed in pmfadm.c by function free_rpc_args().
 * pmf_free_status assumes that the calling routine holds and later
 * releases the lock.
 */
static void
pmf_free_status(pmf_status_result *result)
{
	uint_t j;

	result->code.type = PMF_SYSERRNO;
	result->code.sys_errno = ENOMEM;

	for (j = 0; j < result->cmd.cmd_len; j++) {
		if (result->cmd.cmd_val[j])
			free(result->cmd.cmd_val[j]);
	}
	if (result->cmd.cmd_val)
		free(result->cmd.cmd_val);
	for (j = 0; j < result->env.env_len; j++) {
		if (result->env.env_val[j])
			free(result->env.env_val[j]);
	}
	if (result->env.env_val)
		free(result->env.env_val);
	if (result->identifier)
		free(result->identifier);
	if (result->action)
		free(result->action);
	if (result->pids)
		free(result->pids);
	if (result->upids)
		free(result->upids);
	if (result->project_name)
		free(result->project_name);

	return;

}

/*
 * What follows are our RPC entry points.
 *
 * Here's our "start" entry ... grab a new handle for the
 * request, and spawn a thread to exec and monitor the process.
 * args and cred are input args; result is an output arg.
 */
void
pmf_start_svc(pmf_start_args *args, pmf_result *result,
	security_cred_t *cred)
{
	pmf_handle *handle;

	/*
	 * Make sure the queue doesn't change underneath us.
	 */
	LOCK_QUEUE(pmf_alloc_handle);

	/*
	 * Reject the request if PMFD is going down.
	 */
	if (pmf_handle_list.stopping) {
		result->code.type = PMF_SYSERRNO;
		result->code.sys_errno = EBUSY;
		UNLOCK_QUEUE(pmf_alloc_handle);
		return;
	}

	if ((handle = pmf_alloc_handle(args, cred, result)) == NULL) {
		UNLOCK_QUEUE(pmf_alloc_handle);
		return;
	}

	handle->next = pmf_handle_list.head;
	pmf_handle_list.head = handle;
	pmf_handle_list.count++;

	/*
	 * Monitoring thread must start with lock held and will further
	 * manage this lock. Nevertheless if thread fail to start, as we don�t
	 * release the global queue lock, we will be able to unregister this
	 * handle before anybody has had a chance to get a reference on it.
	 * In this case we can safely unlock handle and destroy it.
	 */
	LOCK_HANDLE(handle);

	if (pthread_create(&handle->tid, NULL,
	    (void *(*)(void *))start_process, handle) != 0) {
		result->code.type = PMF_SYSERRNO;
		result->code.sys_errno = errno;
		/*
		 * We can free our handle here, since a thread
		 * was never started (Re: comment in start_process).
		 */
		ASSERT(pmf_handle_list.head == handle);
		pmf_handle_list.head = handle->next;
		handle->next = (struct pmf_handle *)0;
		UNLOCK_HANDLE(handle);
		pmf_free_handle(handle, FALSE);
	} else {
		pmf_thread_cnt++;
	}

	/*
	 * Handle lock in now managed by monitoring thread.
	 * Just release the global lock
	 */
	UNLOCK_QUEUE(pmf_alloc_handle);
}

#define	PID_LEN	40


/* ARGSUSED */
/*
 * args and cred are input args; result is an output arg.
 */
void
pmf_status_svc(pmf_args *args, pmf_status_result *result,
	security_cred_t *cred)
{

	char	tmp_buf[PID_LEN];	/* temp buf to get size of each pid */
	char	*ptr;		/* ptr to pid_buf */
	char    *uptr;		/* ptr to upid_buf */
	char **aptr;
	pmf_handle *handle;
	child_handle *p;
	uint_t i;
	uint_t buf_len;
	uint_t ubuf_len;

	LOCK_QUEUE(pmf_status_svc);

	(void) memset(result, 0, sizeof (pmf_status_result));

	if ((handle = pmf_id_to_handle(args->identifier)) == NULL) {

		/*
		 * case PMF_STATUS_ALL:
		 * show status of all tags
		 */
		if (args->query_all) {

			if (pmf_handle_list.head == NULL) {
				UNLOCK_QUEUE(pmf_status_svc);
				return;
			}

			/*
			 * calculate size of buf
			 */
			buf_len = 0;
			for (handle = pmf_handle_list.head;
			    handle; handle = handle->next) {

				/*
				 * print only tags belonging to user
				 * or if user is root print all tags
				 */
				if (cred->aup_uid == 0 ||
				    (handle->uid == cred->aup_uid &&
				    handle->gid == cred->aup_gid))
					buf_len += (uint_t)strlen(handle->id)
					    + 1; /* 1 is for the space */

			}

			/*
			 * malloc buf of correct size
			 */
			result->identifier =
			    (char *)malloc((size_t)(buf_len + 1));
			if (result->identifier == NULL) {
				result->code.type = PMF_SYSERRNO;
				result->code.sys_errno = ENOMEM;
				UNLOCK_QUEUE(pmf_status_svc);
				return;
			}

			/*
			 * copy tags into buf
			 */
			(void) memset(result->identifier, 0,
			    (size_t)(buf_len + 1));

			for (handle = pmf_handle_list.head; handle;
			    handle = handle->next) {

				if (cred->aup_uid == 0 ||
				    (handle->uid == cred->aup_uid &&
				    handle->gid == cred->aup_gid)) {
					(void) strcat(result->identifier,
					    handle->id);
					(void) strcat(result->identifier, " ");
				}
			}
			(void) strcat(result->identifier, "\0");

		} else if (args->identifier[0] == '\0') {
			/*
			 * unadvertised debugging option
			 */

			result->code.type	= PMF_OKAY;

			/*
			 * exit, if pmfd is not monitoring any processes
			 */
			if (pmf_handle_list.head == NULL) {
				UNLOCK_QUEUE(pmf_status_svc);
				return;
			}

			/*
			 * calculate size of buf
			 */
			buf_len = 0;

			for (handle = pmf_handle_list.head; handle;
			    handle = handle->next) {
				buf_len += (uint_t)strlen(handle->id) + 1;
					/* 1 is for the space */
			}

			/*
			 * malloc buf of correct size
			 */
			result->identifier =
			    (char *)malloc((size_t)buf_len + 1);
			if (result->identifier == NULL) {
				result->code.type = PMF_SYSERRNO;
				result->code.sys_errno = ENOMEM;
				UNLOCK_QUEUE(pmf_status_svc);
				return;
			}
			(void) memset(result->identifier, 0,
			    (size_t)buf_len + 1);

			for (handle = pmf_handle_list.head; handle;
			    handle = handle->next) {
				(void) strcat(result->identifier, handle->id);
				(void) strcat(result->identifier, " ");
			}

		} else if (strcmp(args->identifier, "all") == 0) {
			/*
			 * unadvertised debugging option
			 */

			result->code.type = PMF_SYSERRNO;
			result->code.sys_errno = ENOENT;

			for (handle = pmf_handle_list.head; handle;
			    handle = handle->next) {

				dbg_msgout(NOGET("%s:"), handle->id);
				for (aptr = handle->cmd; aptr && *aptr; aptr++)
					dbg_msgout(NOGET(" %s"), *aptr);
				dbg_msgout(NOGET(
				    "\tretries=%d period=%d action=%d\n"),
				    handle->retries, handle->period,
				    handle->action_type);
				dbg_msgout(NOGET("children\n"));
				LOCK_HANDLE(handle);
				for (p = handle->head; p; p = p->next) {
					dbg_msgout(NOGET(
					    "\tpid=%d running\n"),
					    p->pid);
				}
				UNLOCK_HANDLE(handle);
			}

			dbg_msgout(NOGET("pmf_alloc_cnt = %d\n"),
			    pmf_alloc_cnt);
			dbg_msgout(NOGET("pmf_thread_cnt = %d\n"),
			    pmf_thread_cnt);
		} else {

			result->code.type = PMF_SYSERRNO;
			result->code.sys_errno = ENOENT;

		}
	} else {
		result->code.type = PMF_OKAY;

		aptr = (char **)handle->cmd;
		for (i = 0; *aptr++; i++)
			;
		result->cmd.cmd_val =
		    (char **)malloc(i * sizeof (char *));
		if (result->cmd.cmd_val == NULL) {
			/*
			 * Here we haven't successfully malloced anything
			 * before so there is no need to call pmf_free_status
			 * to free it.
			 */
			result->code.type = PMF_SYSERRNO;
			result->code.sys_errno = ENOMEM;
			UNLOCK_QUEUE(pmf_status_svc);
			return;
		}
		result->cmd.cmd_len = i;

		for (aptr = handle->cmd, i = 0; aptr && *aptr; aptr++, i++) {
			result->cmd.cmd_val[i] = strdup(*aptr);
			if (result->cmd.cmd_val[i] == NULL) {
				/*
				 * Here we have to call pmf_free_status to
				 * set the error and free what was
				 * previously malloced.
				 */
				pmf_free_status(result);
				UNLOCK_QUEUE(pmf_status_svc);
				return;
			}
		}

		/*
		 * handle->env might be null if there are no environment
		 * variables.
		 */
		if (handle->env != NULL) {
			/* We have at least one environment variable */
			aptr = handle->env;
			for (i = 0; *aptr++; i++)
				;
			result->env.env_val =
			    (char **)malloc(i * sizeof (char *));
			if (result->env.env_val == NULL) {
				/*
				 * Here we have to call pmf_free_status to
				 * set the error and free what was
				 * previously malloced.
				 */
				pmf_free_status(result);
				UNLOCK_QUEUE(pmf_status_svc);
				return;
			}
			result->env.env_len = i;

			for (aptr = handle->env, i = 0; aptr && *aptr;
			    aptr++, i++) {
				result->env.env_val[i] = strdup(*aptr);
				if (result->env.env_val[i] == NULL) {
					/*
					 * Here we have to call
					 * pmf_free_status to
					 * set the error and free what was
					 * previously malloced.
					 */
					pmf_free_status(result);
					UNLOCK_QUEUE(pmf_status_svc);
					return;
				}
			}
		} else {
			/* we have no environment vars */
			result->env.env_len = 0;
			result->env.env_val = NULL;
		}

		result->retries		= handle->retries;
		result->period		= handle->period;
		if (handle->id) {
			result->identifier = strdup(handle->id);
			if (result->identifier == NULL) {
				/*
				 * Here we have to call pmf_free_status to
				 * set the error and free what was
				 * previously malloced.
				 */
				pmf_free_status(result);
				UNLOCK_QUEUE(pmf_status_svc);
				return;
			}
		}

		result->action_type	= handle->action_type;
		if (handle->action) {
			result->action	= strdup(handle->action);
			if (result->action == NULL) {
				/*
				 * Here we have to call pmf_free_status to
				 * set the error and free what was
				 * previously malloced.
				 */
				pmf_free_status(result);
				UNLOCK_QUEUE(pmf_status_svc);
				return;
			}
		}
		result->nretries	= (int)pmf_process_scan_history(handle);
		result->owner		= handle->uid;
		result->monitor_children = handle->monitor_children;
		result->monitor_level    = handle->monitor_level;

		if (handle->project_name) {
			result->project_name = strdup(handle->project_name);
			if (result->project_name == NULL) {
				/*
				 * Here we have to call pmf_free_status to
				 * set the error and free what was
				 * previously malloced.
				 */
				pmf_free_status(result);
				UNLOCK_QUEUE(pmf_status_svc);
				return;
			}
		}

		/*
		 * if threads is null we're done;
		 * In this situation we're not calling pmf_free_status()
		 * because it might be normal for the threads list to be empty.
		 * That can happen in the following case:
		 * pmfd runs the user-specified action after the monitored
		 * process exited. If the restart occurs too frequently
		 * (less than 1 min apart), pmfd waits for 30 sec. before
		 * restarting. During this time there are no threads running,
		 * so the list of threads maintained by the handle is null.
		 */

		LOCK_HANDLE(handle);
		if (handle->head == NULL) {
			UNLOCK_HANDLE(handle);
			UNLOCK_QUEUE(pmf_status_svc);
			return;
		}

		/*
		 * calculate size of buf
		 */
		buf_len = 0;
		ubuf_len = 0;
		for (p = handle->head; p; p = p->next) {
			(void) memset(tmp_buf, 0, sizeof (tmp_buf));
			(void) sprintf(tmp_buf, "%d", p->pid);
			if (PMF_IS_MONITORED(p))
				/* 1 is for the space */
				buf_len += (uint_t)strlen(tmp_buf) + 1;
			else
				ubuf_len += (uint_t)strlen(tmp_buf) + 1;

		}

		/*
		 * malloc buf of correct size
		 */
		result->pids = (char *)malloc((size_t)(buf_len + 1));
		if (result->pids == NULL) {
			pmf_free_status(result);
			UNLOCK_HANDLE(handle);
			UNLOCK_QUEUE(pmf_status_svc);
			return;
		}
		(void) memset(result->pids, 0, (size_t)(buf_len + 1));

		result->upids = (char *)malloc((size_t)(ubuf_len + 1));
		if (result->upids == NULL) {
			pmf_free_status(result);
			UNLOCK_HANDLE(handle);
			UNLOCK_QUEUE(pmf_status_svc);
			return;
		}
		(void) memset(result->upids, 0, (size_t)(ubuf_len + 1));

		/*
		 * copy pids into buf
		 */
		if (handle->head != NULL) {
			ptr = result->pids;
			uptr = result->upids;
			for (p = handle->head; p; p = p->next) {
				if (PMF_IS_MONITORED(p)) {
					(void) sprintf(ptr, " %d", p->pid);
					ptr += strlen(ptr);
				} else {
					(void) sprintf(uptr, " %d",
					    p->pid);
					uptr += strlen(uptr);
				}
			}
			*ptr = '\0';
			*uptr = '\0';
		}

		UNLOCK_HANDLE(handle);

	}

	UNLOCK_QUEUE(pmf_status_svc);
}

/*
 * pmf_request_svc
 *
 * Handle a service request.
 * Real work is defered to the monitoring thread. This routine only enqueue
 * the request and wait for the request to be processed by monitoring thread.
 */
void
pmf_request_svc(int type, pmf_args *args, pmf_result *result,
    security_cred_t *cred)
{
	pmf_handle *handle;
	struct child_handle *p = (struct child_handle *)0;
	int err;
	struct pmf_event *event;
	struct pmf_event *exit_event;
	struct timespec timeout;

	(void) memset(result, 0, sizeof (pmf_result));
	exit_event = (struct pmf_event *)0;

	/*
	 * Sanity check on args
	 */
	if (args == NULL || args->identifier == NULL) {
		result->code.type = PMF_SYSERRNO;
		result->code.sys_errno = EINVAL;
		return;
	}

	if ((event = get_pmf_event_from_pool()) == (struct pmf_event *)0) {
		result->code.type = PMF_SYSERRNO;
		result->code.sys_errno = ENOMEM;
		return;
	}

	if ((err = acquire_pmf_event(event)) != 0) {
		(void) return_pmf_event_to_pool(&event);
		result->code.type = PMF_SYSERRNO;
		result->code.sys_errno = err;
		return;
	}

	event->type = type;
	switch (event->type) {

		case PMFD_RESUME:
		case PMFD_SUSPEND:

			/*
			 * Look for handle associated with pid.
			 * If found we go on holding the lock on it.
			 */

			event->pid = atoi(args->identifier);
			LOCK_QUEUE(pmf_request_svc);
			for (handle = pmf_handle_list.head; handle;
			    handle = handle->next) {
				LOCK_HANDLE(handle);
				for (p = handle->head; p; p = p->next) {
					if (p->pid == event->pid) break;
				}
				if (p) break;
				UNLOCK_HANDLE(handle);
			}
			UNLOCK_QUEUE(pmf_request_svc);
			if (!p) {
				(void) return_pmf_event_to_pool(&event);
				result->code.type = PMF_SYSERRNO;
				result->code.sys_errno = ESRCH;
				return;
			}
			break;

		case PMFD_STOP:
		case PMFD_KILL:

			/*
			 * Look for handle associated with identifier.
			 * If found we go on holding the lock on it.
			 */

			LOCK_QUEUE(pmf_request_svc);
			for (handle = pmf_handle_list.head; handle;
			    handle = handle->next) {
				LOCK_HANDLE(handle);
				if (strcmp(handle->id, args->identifier)
				    == 0) break;
				UNLOCK_HANDLE(handle);
			}
			UNLOCK_QUEUE(pmf_request_svc);

			if (!handle) {
				(void) return_pmf_event_to_pool(&event);
				result->code.type = PMF_SYSERRNO;
				result->code.sys_errno = ESRCH;
				return;
			}

			event->signal = args->signal;

			break;

		default:
			(void) return_pmf_event_to_pool(&event);
			result->code.type = PMF_SYSERRNO;
			result->code.sys_errno = EINVAL;
			return;
	}

	/*
	 * We hold the handle lock
	 */
	if ((cred->aup_uid != handle->uid) && (cred->aup_uid != 0)) {
		UNLOCK_HANDLE(handle);
		(void) return_pmf_event_to_pool(&event);
		result->code.type = PMF_SYSERRNO;
		result->code.sys_errno = EACCES;
		return;
	}

	/*
	 * Take ownership of exit event associated to handle if we have
	 * been requested to wait until monitoring thread exits (aka all
	 * monitored processes have exited).
	 */
	if ((event->type == PMFD_STOP || event->type == PMFD_KILL) &&
	    args->timewait) {
		if ((exit_event = handle->exit_event)) {
			if ((err = acquire_pmf_event(exit_event)) != 0) {
				UNLOCK_HANDLE(handle);
				(void) return_pmf_event_to_pool(&event);
				result->code.type = PMF_SYSERRNO;
				result->code.sys_errno = err;
				return;
			}
			LOCK_EVENT(exit_event);
		}

	}

	/*
	 * Enqueue the request and wait for the aknowledge that
	 * the request was taken into account.
	 */
	LOCK_EVENT(event);
	(void) queue_pmf_event(&handle->queue, event);
	(void) pthread_cond_signal(&handle->handle_cond);
	UNLOCK_HANDLE(handle);

	if (debug)
		dbg_msgout(NOGET("Waiting for STOP/KILL svc request ACK"
		    "on tag %s\n"));
	pthread_cond_wait(&event->cond, &event->lock);
	result->code = event->return_code;
	UNLOCK_EVENT(event);
	(void) release_pmf_event(&event);

	if (result->code.type == PMF_OKAY) {

		/*
		 * Have we been asked to wait until all threads exit?
		 * If so, wait ...
		 */
		if (exit_event && args->timewait) {
			if (debug)
				dbg_msgout(NOGET("Waiting for processes to "
				    "finish.\n"));
			if (args->timewait != -1) {
				timeout.tv_sec = time(NULL) + args->timewait;
				timeout.tv_nsec = 0;

				err = pthread_cond_timedwait(
				    &exit_event->cond, &exit_event->lock,
				    &timeout);
			} else {
				err = pthread_cond_wait(&exit_event->cond,
				    &exit_event->lock);
			}
			if (err != 0) {
				/*
				 * ETIMEDOUT prints a confusing message:
				 *
				 *	"Connection timed out"
				 *
				 * We'll convert it to ETIME to make it clearer:
				 *
				 *	"timer expired"
				 *
				*/
				if (err == ETIMEDOUT)
					err = ETIME;
				result->code.type = PMF_SYSERRNO;
				result->code.sys_errno = err;
			}
		}
	}

	if (exit_event) {
		UNLOCK_EVENT(exit_event);
		(void) release_pmf_event(&exit_event);
	}
}


/*
 * pmf_stop_svc
 *
 * Called as result of user executing pmfadm -s
 * Real work is defered to the monitoring thread. This routine only enqueue
 * a request and then wait for the return code
 */
void
pmf_stop_svc(pmf_args *args, pmf_result *result,
	security_cred_t *cred)
{
	if (debug && args && args->identifier)
		dbg_msgout(NOGET("Stop svc request on tag %s\n"),
		    args->identifier);

	pmf_request_svc(PMFD_STOP, args, result, cred);
}

void
pmf_modify_svc(pmf_modify_args *args, pmf_result *result,
	security_cred_t *cred)
{
	pmf_handle *handle;

	(void) memset(result, 0, sizeof (pmf_result));

	if (args->retries > PMFD_MAX_HISTORY) {
		result->code.type = PMF_SYSERRNO;
		result->code.sys_errno = E2BIG;
		return;
	}

	LOCK_QUEUE(pmf_modify_svc);

	for (handle = pmf_handle_list.head; handle; handle = handle->next) {
		LOCK_HANDLE(handle);
		if (strcmp(handle->id, args->identifier) == 0) break;
		UNLOCK_HANDLE(handle);
	}

	if (!handle) {
		result->code.type = PMF_SYSERRNO;
		result->code.sys_errno = ENOENT;
		UNLOCK_QUEUE(pmf_modify_svc);
	} else {
		UNLOCK_QUEUE(pmf_modify_svc);
		if ((cred->aup_uid != handle->uid) && (cred->aup_uid != 0)) {
			result->code.type = PMF_SYSERRNO;
			result->code.sys_errno = EACCES;
		} else {
			if (args->retries_modified)
				handle->retries = args->retries;
			if (args->period_modified)
				handle->period = args->period;

			/*
			 * Clear history of earlier failures. Keep only
			 * current run, if it hasn't ended yet.
			 */
			if ((args->retries_modified ||
			    args->period_modified) &&
			    (handle->history != NULL)) {
				if (handle->history->etime == 0)
					pmf_free_process_history(
					    &handle->history->next);
				else
					pmf_free_process_history(
					    &handle->history);
			}
		}
		UNLOCK_HANDLE(handle);
	}
}

void
pmf_kill_svc(pmf_args *args, pmf_result *result,
	security_cred_t *cred)
{
	pmf_request_svc(PMFD_KILL, args, result, cred);
}

/*
 * pmf_suspend_svc
 *
 * Called as result of user executing pmfctl -S
 * Real work is defered to the monitoring thread. This routine only enqueue
 * a request and then wait for the return code
 */
void
pmf_suspend_svc(pmf_args *args, pmf_result *result,
	security_cred_t *cred)
{
	pmf_request_svc(PMFD_SUSPEND, args, result, cred);
}

/*
 * pmf_resume_svc
 *
 * Called as result of user executing pmfctl -R
 * Real work is deffered to the monitoring thread. This routine only enqueue
 * a request and then wait for the return code
 */
void
pmf_resume_svc(pmf_args *args, pmf_result *result,
	security_cred_t *cred)
{
	pmf_request_svc(PMFD_RESUME, args, result, cred);
}

int
pmf_action_exec(pmf_handle *handle, PMF_ACTION_WHY why)
{
	char **cmd;
	uint_t nc, i;
	char *p1 = NULL;
	char *p2 = NULL;
	char *action = NULL;
	char *sav_ptr = NULL;
	int result;
	char *lasts;

	/*
	 * If there is no action program specified, exit as if
	 * we've failed.
	 */
	if (handle->action == NULL || *handle->action == '\0')
		return (-1);

	/*
	 * only PMF_ACTION_FAILED requires handling
	 */
	if (why != PMF_ACTION_FAILED)
		return (-1);

	p1 = handle->action;

	/* skip leading spaces */
	while (p1 && (*p1 != '\0') && (isspace(*p1)))
		p1++;

	if (p1 == NULL || *p1 == '\0')
		return (-1);

	/* remember to allocate an extra byte for the \0 */
	action = (char *)malloc((1 + strlen(p1)) * sizeof (char));
	if (action == (char *)NULL)
		return (-1);

	sav_ptr = action; /* Save start of action in sav_ptr */
	nc = (uint_t)strlen(p1);
	for (i = 0; i < nc && (*p1 != '\0'); i++) {
		/* Non white-space character - simply copy */
		if (!isspace(*p1)) {
			*action++ = *p1++;
			continue;
		}

		/*
		 * If next character is also white-space character or NUL,
		 * skip
		 */
		if (isspace(*(p1+1)) || (*(p1+1) == '\0')) {
			p1++;
		} else {
			/*
			 * Next character is non white-space - so put
			 * just one
			 */
			/* "space" character in action as a delimiter */
			*action++ = ' ';
			p1++;
		}
	}

	*action = '\0'; /* terminate */

	p1 = action = sav_ptr; /* restore to start */
	nc = 0;
	/* count the number of components separated by space */
	while ((p2 = strchr(p1, ' ')) != NULL) {
		nc++;
		p1 = p2+1;
	}

	cmd = (char **)malloc((nc+4) * sizeof (char *));
	if (cmd == (char **)NULL)
		return (-1);

	i = 1;
	if (nc == 0) /* just a single component action command */
		cmd[0] = action;
	else { /* multiple component command separated by spaces */
		cmd[0] = strdup(strtok_r(action, " ", &lasts));
		p1 = action+strlen(cmd[0])+1;
		for (; i < nc; i++) {
			cmd[i] = strdup(strtok_r(NULL, " ", &lasts));
			p1 = p1+strlen(cmd[i])+1;
		}
		cmd[i++] = strdup(p1); /* last component */
	}

	cmd[i++] = strdup(NOGET("failed"));
	cmd[i++] = strdup(handle->id);
	cmd[i] = NULL;

	if (debug) dbg_msgout("running \"");
	for (i = 0; i < (nc + 4); i++) {
		if (cmd[i])
			if (debug) dbg_msgout("%s ", cmd[i]);
	}
	if (debug) dbg_msgout("\"\n");

	result = pmf_run_process(handle, cmd);

	/*
	 * In cases where there is just a single component action
	 * command, cmd[0] and action point to the same memory.
	 * To make sure we don't free the memory twice, check
	 * that action is not equal to cmd[0].
	 */
	if (action && action != cmd[0])
		free(action);
	for (i = 0; i < (nc + 4); i++) {
		if (cmd[i])
			free(cmd[i]);
	}

	if (cmd)
		free(cmd);

	return (result);
}
