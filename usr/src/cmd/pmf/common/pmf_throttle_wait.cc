//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)pmf_throttle_wait.cc	1.6	08/07/23 SMI"

#include "pmf_throttle_wait.h"
#include "pmf_error.h"

#include <sys/cl_assert.h>
#include <sys/sc_syslog_msg.h>
#include <sys/rsrc_tag.h>
#include <memory>

pmf_throttle_wait* pmf_throttle_wait::the_throttler = NULL;
pthread_mutex_t pmf_throttle_wait::create_lock = PTHREAD_MUTEX_INITIALIZER;

pmf_throttle_wait&
pmf_throttle_wait::instance()
{
	if (the_throttler == NULL) {
		create();
	}

	return (*the_throttler);
}

void
pmf_throttle_wait::create()
{
	// allow only one thread at a time to attempt to create
	// the throttler in order to avoid creating multiple throttlers
	// in a race condition.
	CL_PANIC(pthread_mutex_lock(&create_lock) == 0);
	if (the_throttler == NULL) {
		the_throttler = new pmf_throttle_wait();
	}
	CL_PANIC(pthread_mutex_unlock(&create_lock) == 0);
}


//
// Wrapper function for thread creation.
//
// Defined with "C" linkage because it's called by a C function
// (pthread_create).
//
extern "C" void *throttle_thread_wrapper(void *)
{
	pmf_throttle_wait::instance().timeout_waits();
	return (NULL);
}

pmf_throttle_wait::pmf_throttle_wait()
{
	(void) pthread_mutex_init(&lock, NULL);
	(void) pthread_cond_init(&cond, NULL);

	//
	// Create the timeout thread
	//
	int ret;
	if ((ret = pthread_create(&wait_thread, NULL,
	    throttle_thread_wrapper, NULL)) != 0) {
		char *err_str = strerror(ret);
		os::sc_syslog_msg logger(SC_SYSLOG_PMF_PMFD_TAG, "", NULL);
		logger.log(LOG_ERR, MESSAGE,
		    "pthread_create: %s", err_str);
		exit(ret);
	}
}

//
// timeout_waits()
//
// This method performs the timeout logic. It keeps a priority_queue
// of timeout_entry objects, ordered by their restart times, with the
// next one to restart first in the queue. The thread sleeps on a condition
// variable until a new timeout entry is added to the queue, or until
// it's time to finish the timeout of the first entry on the queue.
// A timeout is completed by calling start() on the pmf_contract pointer.
//
void
pmf_throttle_wait::timeout_waits()
{
	CL_PANIC(pthread_mutex_lock(&lock) == 0);

	while (true) {
		timestruc_t timeout;
		int err;

		// If the queue is empty, do a normal cond_wait.
		if (pq.empty()) {
			err = pthread_cond_wait(&cond, &lock);
		} else {
			// The thread is not empty, so wait until it's time
			// to finish the timeout for the top entry.
			timeout.tv_sec = pq.top().restart_time;
			timeout.tv_nsec = 0;
			err = pthread_cond_timedwait(&cond, &lock, &timeout);
		}

		debug_print(NOGET("pmf_throttle_wait: waking up from cond_wait"
		    ". err=%d\n"), err);

		if (err != 0 && err != ETIMEDOUT) {
			os::sc_syslog_msg logger(SC_SYSLOG_PMF_PMFD_TAG, "",
			    NULL);
			logger.log(LOG_ERR, MESSAGE, "pthread_cond_wait: %s",
			    strerror(err));
		}

		//
		// Here we're holding the lock again (after the return from
		// pthread_cond_wait or pthread_cond_timedwait)
		//
		// Either we timed out and it's time to check the top
		// entry, or a new entry was added to the queue. We don't
		// bother distinguishing the two cases.
		//

		// Restart all processes that are done waiting
		// We can't handle a failure from the time() call here.
		time_t cur_time = time(NULL);
		CL_PANIC(cur_time != -1);

		while (!pq.empty() && pq.top().restart_time <= cur_time) {
			pmf_contract *pc =
			    dynamic_cast<pmf_contract *>(
			    pq.top().sppc.get_ptr());
			pid_t child_pid;
			err = pc->start(child_pid);
			if (err != 0 && err != ECANCELED && err !=
			    EINPROGRESS) {
				// unknown error -- kill the procs
				// and stop monitoring
				debug_print(NOGET("pmf_throttle_wait: "
				    "killing and stopping tag %s"),
				    pc->get_nametag().c_str());
				pc->signal_all(SIGKILL);
				pc->stop();
			}
			pq.pop();
		}
	}
}

int
pmf_throttle_wait::wait(smart_ptr<cl_contract> sppc, time_t time_in)
{
	CL_PANIC(pthread_mutex_lock(&lock) == 0);

	// Add this timeout entry to the priority queue
	try {
		time_t cur_time = time(NULL);
		if (cur_time == -1) {
			int err = errno;
			os::sc_syslog_msg logger(SC_SYSLOG_PMF_PMFD_TAG, "",
			    NULL);
			logger.log(LOG_ERR, MESSAGE, "time in throttle wait: "
			    "%s", strerror(err));
			return (err);
		}
		pq.push(timeout_entry(sppc, time(NULL) + time_in));
	// CSTYLED
	} catch (std::bad_alloc&) {
		return (ENOMEM);
	}

	// signal the timeout thread in case this new entry should wait
	// less time than any of the current entries
	(void) pthread_cond_broadcast(&cond);
	CL_PANIC(pthread_mutex_unlock(&lock) == 0);
	return (0);
}

bool
// CSTYLED
operator>(const timeout_entry& lhs, const timeout_entry& rhs)
{
	return (lhs.restart_time > rhs.restart_time);
}


timeout_entry::timeout_entry(smart_ptr<cl_contract> sppc_in, time_t time)
	: sppc(sppc_in), restart_time(time)
{
}
