/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)pmfd.c	1.45	08/05/20 SMI"

/*
 * pmfd.c  HA Process Monitor Facility daemon
 */
#include "pmfd.h"

#if DOOR_IMPL
#include <rgm/pmf.h>
#include "rgm/door/pmf_door.h"
#include <alloca.h>
#else
#include <rgm/rpc/pmf.h>
#endif

#include <sys/sc_syslog_msg.h>
#include <rpc/rpc.h>
#include <netdb.h>


static void print_args(pmf_start_args *argp);

#if DOOR_IMPL
/* ARGSUSED */
void pmf_door_server(void *cookie,
    char *dataptr, size_t data_size,
    door_desc_t *desc_ptr, uint_t ndesc)
{
	pmf_input_args_t *pmf_arg = NULL;
	XDR xdrs, xdrs_result, xdrs_error;
	void *result = NULL;
	char *result_buf = NULL;
	char *error_buf = NULL;
	size_t result_size;
	int return_code = 0;
	int num_retries = XDR_NUM_RETRIES;
	sc_syslog_msg_handle_t handle;

	if (data_size == 0) {
		/*
		 * This should not happen but we check for it, just
		 * to be sure that the size of the arguments is not
		 * nil.
		 */
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    SYSTEXT("Incoming argument size is zero."));
		sc_syslog_msg_done(&handle);
		return_code = DOOR_INCOMING_ARG_SIZE_NULL;
		goto error_return;
	}
	pmf_arg = (pmf_input_args_t *)calloc(1, sizeof (pmf_input_args_t));
	if (pmf_arg == NULL) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    SYSTEXT("Unable to allocate memory at door server"));
		sc_syslog_msg_done(&handle);
		return_code = UNABLE_TO_ALLOCATE_MEM;
		goto error_return;
	}

	xdrmem_create(&xdrs, dataptr, data_size, XDR_DECODE);
	if (!xdr_pmf_input_args(&xdrs, pmf_arg)) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    SYSTEXT("XDR Error while decoding arguments."));
		sc_syslog_msg_done(&handle);
		return_code = XDR_ERROR_AT_SERVER;
		goto error_return;
	}

	switch (pmf_arg->api_name) {
	case PMFPROC_NULL:
		if (!pmfproc_null_2_svc(NULL, NULL)) {
			return_code = FALSE_RET_FROM_API_FN;
			goto error_return;
		}
		break;
	case PMFPROC_START:
		result = (pmf_result *)calloc(1, sizeof (pmf_result));
		if (!pmfproc_start_2_svc((pmf_start_args *)(pmf_arg->data),
		    (pmf_result *)(result))) {
			return_code = FALSE_RET_FROM_API_FN;
			goto error_return;
		}
		break;
	case PMFPROC_STATUS:
		result = (pmf_status_result *)calloc
		    (1, sizeof (pmf_status_result));
		if (!pmfproc_status_2_svc((pmf_args *)(pmf_arg->data),
		    (pmf_status_result *)(result))) {
			return_code = FALSE_RET_FROM_API_FN;
			goto error_return;
		}
		break;
	case PMFPROC_STOP:
		result = (pmf_result *)calloc(1, sizeof (pmf_result));
		if (!pmfproc_stop_2_svc((pmf_args *)(pmf_arg->data),
		    (pmf_result *)(result))) {
			return_code = FALSE_RET_FROM_API_FN;
			goto error_return;
		}
		break;
	case PMFPROC_KILL:
		result = (pmf_result *)calloc(1, sizeof (pmf_result));
		if (!pmfproc_kill_2_svc((pmf_args *)(pmf_arg->data),
		    (pmf_result *)(result))) {
			return_code = FALSE_RET_FROM_API_FN;
			goto error_return;
		}
		break;
	case PMFPROC_MODIFY:
		result = (pmf_result *)calloc(1, sizeof (pmf_result));
		if (!pmfproc_modify_2_svc((pmf_modify_args *)(pmf_arg->data),
		    (pmf_result *)(result))) {
			return_code = FALSE_RET_FROM_API_FN;
			goto error_return;
		}
		break;
	case PMFPROC_SUSPEND:
		result = (pmf_result *)calloc(1, sizeof (pmf_result));
		if (!pmfproc_suspend_2_svc((pmf_args *)(pmf_arg->data),
		    (pmf_result *)(result))) {
			return_code = FALSE_RET_FROM_API_FN;
			goto error_return;
		}
		break;
	case PMFPROC_RESUME:
		result = (pmf_result *)calloc(1, sizeof (pmf_result));
		if (!pmfproc_resume_2_svc((pmf_args *)(pmf_arg->data),
		    (pmf_result *)(result))) {
			return_code = FALSE_RET_FROM_API_FN;
			goto error_return;
		}
		break;
	default:
		return_code = INVALID_API_NAME;
		goto error_return;
	}

	result_size = pmf_xdr_sizeof_result(result, pmf_arg->api_name);

	/*
	 * For encoding the arguments we will try XDR_NUM_RETRIES times
	 */
	for (num_retries = XDR_NUM_RETRIES; num_retries > 0; num_retries--) {
		result_buf = (char *)alloca(result_size);
		if (result_buf == NULL) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_PMF_PMFD_TAG, "");
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    SYSTEXT("Unable to allocate memory at "
			    "door server"));
			sc_syslog_msg_done(&handle);
			return_code = UNABLE_TO_ALLOCATE_MEM;
			goto error_return;
		}
		(void) memset(result_buf, 0, (int)result_size);

		xdrmem_create(&xdrs_result, result_buf, result_size,
		    XDR_ENCODE);

		if (!xdr_int(&xdrs_result, &return_code)) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_PMF_PMFD_TAG, "");
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    SYSTEXT("XDR Error while encoding return "
			    "arguments."));
			sc_syslog_msg_done(&handle);
			return_code = XDR_ERROR_AT_SERVER;
			goto error_return;
		}

		if (!xdr_pmf_result_args(&xdrs_result, result,
		    pmf_arg->api_name)) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_PMF_PMFD_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * A non-fatal error occurred while rpc.pmfd was
			 * marshalling return values for a remote procedure
			 * call. The operation will be re-tried with a larger
			 * buffer.
			 * @user_action
			 * No user action is required. If the message recurs
			 * frequently, contact your authorized Sun service
			 * provider to determine whether a workaround or patch
			 * is available.
			 */
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    SYSTEXT("PMFD XDR Buffer Shortfall while encoding "
			    "return arguments API num = %d. Will retry"),
			    pmf_arg->api_name);
			sc_syslog_msg_done(&handle);
			/*
			 * Will retry to encode the result
			 * this time with a larger buffer
			 */
			if (num_retries) {
				result_size += EXTRA_BUFFER_SIZE;
				free(result_buf);
				result_buf = NULL;
				continue;
			}
			/*
			 * Unable to encode the arguments even after
			 * retry.
			 */
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_PMF_PMFD_TAG, "");
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    SYSTEXT("XDR Error while encoding return "
			    "arguments."));
			sc_syslog_msg_done(&handle);
			return_code = XDR_ERROR_AT_SERVER;
			goto error_return;
		} else {
			break; /* Successfully encoded the arguments */
		}
	}
	result_size = xdr_getpos(&xdrs_result);

	if (result != NULL) {
		pmf_result_args_xdr_free(pmf_arg->api_name, result);
		free(result);
	}

	if (pmf_arg->data != NULL) {
		pmf_input_args_xdr_free(pmf_arg);
		free(pmf_arg);
	}

	xdr_destroy(&xdrs);
	xdr_destroy(&xdrs_result);

	(void) door_return(result_buf, result_size, NULL, 0);

error_return:
	error_buf = (char *)alloca(RNDUP(sizeof (int)));
	xdrmem_create(&xdrs_error, error_buf, RNDUP(sizeof (int)), XDR_ENCODE);
	if (!xdr_int(&xdrs_error, &return_code)) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    SYSTEXT("Severe XDR Error, Cannot recover."));
		sc_syslog_msg_done(&handle);
			/*
			 * We will return with all parameters zero
			 * this has to be checked at the client end
			 * and the appropriate error has to be
			 * sent to the caller.
			 */
		(void) door_return(NULL, 0, NULL, 0);
	}
	xdr_destroy(&xdrs_error);
	(void) door_return(error_buf, RNDUP(sizeof (int)), NULL, 0);
}
#endif

#if DOOR_IMPL
bool_t
pmfproc_start_2_svc(pmf_start_args *argp, pmf_result *result)
#else
bool_t
pmfproc_start_2_svc(pmf_start_args *argp, pmf_result *result,
    struct svc_req *rqstp)
#endif
{

	security_cred_t	pmfd_cred;
#if DOOR_IMPL
	door_cred_t	door_creds;
#else
	struct authunix_parms rpc_cred;
#endif

	if (debug)
		print_args(argp);

	(void) memset(result, 0, sizeof (pmf_result));

	/*
	 * Check security and if check succeeds execute request
	 * SEC_UNIX_STRONG means that we're checking if
	 * the call came from the loopback transport,
	 * and that the userid matches the one requested - in this
	 * case the uid of the caller.
	 * This means that any user can make the call, not only root.
	 * The last arg value indicates that the caller doesn't have
	 * to be root.
	 */
#if DOOR_IMPL
	if (security_svc_authenticate(&door_creds, FALSE) != SEC_OK) {
#else
	if (security_svc_authenticate(rqstp, &rpc_cred,
	    SEC_UNIX_STRONG,
	    FALSE) != SEC_OK) {
#endif
		return (FALSE);
	}

#if DOOR_IMPL
	pmfd_cred.aup_uid = door_creds.dc_euid;
	pmfd_cred.aup_gid = door_creds.dc_egid;
#else
	/* for rpc, uid,gid contain euid,egid information */
	pmfd_cred.aup_uid = rpc_cred.aup_uid;
	pmfd_cred.aup_gid = rpc_cred.aup_gid;
#endif

	pmf_start_svc(argp, result, &pmfd_cred);
	return (TRUE);
}


/* ARGSUSED */
#if DOOR_IMPL
bool_t
pmfproc_status_2_svc(pmf_args *argp, pmf_status_result *result)
#else
bool_t
pmfproc_status_2_svc(pmf_args *argp, pmf_status_result *result,
    struct svc_req *rqstp)
#endif
{
	security_cred_t	pmfd_cred;
#if DOOR_IMPL
	door_cred_t	door_creds;
#else
	struct authunix_parms rpc_cred;
#endif

	if (debug) dbg_msgout(NOGET("STATUS %s\n"), argp->identifier);
	(void) memset(result, 0, sizeof (pmf_status_result));

	/*
	 * We give everybody access to status.
	 * We still need to pass the credentials to get the uid and gid.
	 * They are used if global status (pmfadm -L) is requested.
	 * Only tags belonging to the user are shown.
	 * SEC_UNIX_WEAK means that we're not checking if
	 * the call came from the loopback transport.
	 * The last arg value indicates that the caller doesn't have
	 * to be root.
	 */
#if DOOR_IMPL
	if (security_svc_authenticate(&door_creds, FALSE) != SEC_OK) {
#else
	if (security_svc_authenticate(rqstp, &rpc_cred,
	    SEC_UNIX_STRONG,
	    FALSE) != SEC_OK) {
#endif
		return (FALSE);
	}
#if DOOR_IMPL
	pmfd_cred.aup_uid = door_creds.dc_euid;
	pmfd_cred.aup_gid = door_creds.dc_egid;
#else
	/* for rpc, uid,gid contain euid,egid information */
	pmfd_cred.aup_uid = rpc_cred.aup_uid;
	pmfd_cred.aup_gid = rpc_cred.aup_gid;
#endif
	pmf_status_svc(argp, result, &pmfd_cred);
	return (TRUE);
}

#if DOOR_IMPL
bool_t
pmfproc_modify_2_svc(pmf_modify_args *argp, pmf_result *result)
#else
bool_t
pmfproc_modify_2_svc(pmf_modify_args *argp, pmf_result *result,
    struct svc_req *rqstp)
#endif
{
	security_cred_t	pmfd_cred;
#if DOOR_IMPL
	door_cred_t	door_creds;
#else
	struct authunix_parms rpc_cred;
#endif

	if (debug) dbg_msgout(NOGET("MODIFY %s\n"), argp->identifier);

	(void) memset(result, 0, sizeof (pmf_result));
	/*
	 * check security and if check succeeds execute request
	 */
#if DOOR_IMPL
	if (security_svc_authenticate(&door_creds, FALSE) != SEC_OK) {
#else
	if (security_svc_authenticate(rqstp, &rpc_cred,
	    SEC_UNIX_STRONG,
	    FALSE) != SEC_OK) {
#endif
		return (FALSE);
	}

#if DOOR_IMPL
	pmfd_cred.aup_uid = door_creds.dc_euid;
	pmfd_cred.aup_gid = door_creds.dc_egid;
#else
	/* for rpc, uid,gid contain euid,egid information */
	pmfd_cred.aup_uid = rpc_cred.aup_uid;
	pmfd_cred.aup_gid = rpc_cred.aup_gid;
#endif

	pmf_modify_svc(argp, result, &pmfd_cred);
	return (TRUE);
}

#if DOOR_IMPL
bool_t
pmfproc_stop_2_svc(pmf_args *argp, pmf_result *result)
#else
bool_t
pmfproc_stop_2_svc(pmf_args *argp, pmf_result *result,
    struct svc_req *rqstp)
#endif
{
	security_cred_t	pmfd_cred;
#if DOOR_IMPL
	door_cred_t	door_creds;
#else
	struct authunix_parms rpc_cred;
#endif

	if (debug) dbg_msgout(NOGET("STOP %s\n"), argp->identifier);
	(void) memset(result, 0, sizeof (pmf_result));

	/*
	 * check security and if check succeeds execute request
	 */
#if DOOR_IMPL
	if (security_svc_authenticate(&door_creds, FALSE) != SEC_OK) {
#else
	if (security_svc_authenticate(rqstp, &rpc_cred,
	    SEC_UNIX_STRONG,
	    FALSE) != SEC_OK) {
#endif
		return (FALSE);
	}


#if DOOR_IMPL
	pmfd_cred.aup_uid = door_creds.dc_euid;
	pmfd_cred.aup_gid = door_creds.dc_egid;
#else
	/* for rpc, uid,gid contain euid,egid information */
	pmfd_cred.aup_uid = rpc_cred.aup_uid;
	pmfd_cred.aup_gid = rpc_cred.aup_gid;
#endif
	pmf_stop_svc(argp, result, &pmfd_cred);
	return (TRUE);
}

#if DOOR_IMPL
bool_t
pmfproc_kill_2_svc(pmf_args *argp, pmf_result *result)
#else
bool_t
pmfproc_kill_2_svc(pmf_args *argp, pmf_result *result,
    struct svc_req *rqstp)
#endif
{
	security_cred_t	pmfd_cred;
#if DOOR_IMPL
	door_cred_t	door_creds;
#else
	struct authunix_parms rpc_cred;
#endif

	if (debug) dbg_msgout(NOGET("KILL -%d %s\n"),
		argp->signal, argp->identifier);
	(void) memset(result, 0, sizeof (pmf_result));

	/*
	 * check security and if check succeeds execute request
	 */
#if DOOR_IMPL
	if (security_svc_authenticate(&door_creds, FALSE) != SEC_OK) {
#else
	if (security_svc_authenticate(rqstp, &rpc_cred,
	    SEC_UNIX_STRONG,
	    FALSE) != SEC_OK) {
#endif
		return (FALSE);
	}

#if DOOR_IMPL
	pmfd_cred.aup_uid = door_creds.dc_euid;
	pmfd_cred.aup_gid = door_creds.dc_egid;
#else
	/* for rpc, uid,gid contain euid,egid information */
	pmfd_cred.aup_uid = rpc_cred.aup_uid;
	pmfd_cred.aup_gid = rpc_cred.aup_gid;
#endif
	pmf_kill_svc(argp, result, &pmfd_cred);
	return (TRUE);

}
#if DOOR_IMPL
bool_t
pmfproc_suspend_2_svc(pmf_args *argp, pmf_result *result)
#else
bool_t
pmfproc_suspend_2_svc(pmf_args *argp, pmf_result *result,
    struct svc_req *rqstp)
#endif
{
	security_cred_t	pmfd_cred;
#if DOOR_IMPL
	door_cred_t	door_creds;
#else
	struct authunix_parms rpc_cred;
#endif

	if (debug)
		dbg_msgout(NOGET("SUSPEND pid:%d\n"), atoi(argp->identifier));

	(void) memset(result, 0, sizeof (pmf_result));

	/*
	 * check security and if check succeeds execute request
	 */
#if DOOR_IMPL
	if (security_svc_authenticate(&door_creds, FALSE) != SEC_OK) {
#else
	if (security_svc_authenticate(rqstp, &rpc_cred,
	    SEC_UNIX_STRONG,
	    FALSE) != SEC_OK) {
#endif
		return (FALSE);
	}

#if DOOR_IMPL
	pmfd_cred.aup_uid = door_creds.dc_euid;
	pmfd_cred.aup_gid = door_creds.dc_egid;
#else
	/* for rpc, uid,gid contain euid,egid information */
	pmfd_cred.aup_uid = rpc_cred.aup_uid;
	pmfd_cred.aup_gid = rpc_cred.aup_gid;
#endif
	pmf_suspend_svc(argp, result, &pmfd_cred);
	return (TRUE);
}
#if DOOR_IMPL
bool_t
pmfproc_resume_2_svc(pmf_args *argp, pmf_result *result)
#else
bool_t
pmfproc_resume_2_svc(pmf_args *argp, pmf_result *result,
    struct svc_req *rqstp)
#endif
{
	security_cred_t	pmfd_cred;
#if DOOR_IMPL
	door_cred_t	door_creds;
#else
	struct authunix_parms rpc_cred;
#endif

	if (debug)
		dbg_msgout(NOGET("RESUME pid:%d\n"), atoi(argp->identifier));
	(void) memset(result, 0, sizeof (pmf_result));

	/*
	 * check security and if check succeeds execute request
	 */
#if DOOR_IMPL
	if (security_svc_authenticate(&door_creds, FALSE) != SEC_OK) {
#else
	if (security_svc_authenticate(rqstp, &rpc_cred,
	    SEC_UNIX_STRONG,
	    FALSE) != SEC_OK) {
#endif
		return (FALSE);
	}

#if DOOR_IMPL
	pmfd_cred.aup_uid = door_creds.dc_euid;
	pmfd_cred.aup_gid = door_creds.dc_egid;
#else
	/* for rpc, uid,gid contain euid,egid information */
	pmfd_cred.aup_uid = rpc_cred.aup_uid;
	pmfd_cred.aup_gid = rpc_cred.aup_gid;
#endif
	pmf_resume_svc(argp, result, &pmfd_cred);
	return (TRUE);

}
#if !DOOR_IMPL
int
pmf_program_2_freeresult(SVCXPRT *transp, xdrproc_t xdr_result,
	caddr_t result)
{
	xdr_free(xdr_result, result);

	/*
	 * Insert additional freeing code here, if needed
	 */

	return (1);
/*
 * Suppress lint messages about unused function args.
 * This is an RPC service routine and must have the
 * signature specified for such routines.
 */
} /*lint !e715 */
#endif

#if DOOR_IMPL
bool_t
pmfproc_null_2_svc(void *argp, void *result)
#else
bool_t pmfproc_null_2_svc(void *argp, void *result,
    struct svc_req *rqstp)
#endif
{

	if (debug) dbg_msgout(NOGET("pmfproc_null_2_svc called\n"));
	return (TRUE);

/*
 * Suppress lint messages about unused function args.
 * This is an RPC service routine and must have the
 * signature specified for such routines.
 */
} /*lint !e715 */


static void
print_args(pmf_start_args *argp)
{
	uint_t i;

	if (argp->identifier)
		dbg_msgout(NOGET("START identifier %s\n"), argp->identifier);
	else
		dbg_msgout(NOGET("START identifier is NULL\n"));

	dbg_msgout(NOGET("\tcmd:"));
	for (i = 0; i < argp->cmd.cmd_len; i++) {
		if (argp->cmd.cmd_val[i])
			dbg_msgout(NOGET(" %s"), argp->cmd.cmd_val[i]);
		else
			dbg_msgout(NOGET(" NULL"));
	}
	dbg_msgout(NOGET("\n"));

	dbg_msgout(NOGET("\tenv:"));
	for (i = 0; i < argp->env.env_len; i++) {
		if (argp->env.env_val[i])
			dbg_msgout(NOGET(" %s"), argp->env.env_val[i]);
		else
			dbg_msgout(NOGET(" NULL"));
	}
	dbg_msgout(NOGET("\n"));

	if (argp->path)
		dbg_msgout(NOGET("\tpath %s\n"), argp->path);
	else
		dbg_msgout(NOGET("\tpath is NULL\n"));

	if (argp->pwd)
		dbg_msgout(NOGET("\tpwd %s\n"), argp->pwd);
	else
		dbg_msgout(NOGET("\tpwd is NULL\n"));

	dbg_msgout(NOGET("\taction_type %d\n"), argp->action_type);
	if (argp->action)
		dbg_msgout(NOGET("\taction %s\n"), argp->action);
	else
		dbg_msgout(NOGET("\taction is NULL\n"));

	dbg_msgout(NOGET("\tretries %u\n"), argp->retries);
	dbg_msgout(NOGET("\tperiod %u\n"), argp->period);
	dbg_msgout(NOGET("\tflags %d\n"), argp->flags);
	dbg_msgout(NOGET("\tmonitor_children %d\n"), argp->monitor_children);
	dbg_msgout(NOGET("\tmonitor_level %d\n"), argp->monitor_level);
	dbg_msgout(NOGET("\tenv_passed %d\n"), argp->env_passed);
}
