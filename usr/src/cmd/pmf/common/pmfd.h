/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _PMFD_H
#define	_PMFD_H

#pragma ident	"@(#)pmfd.h	1.59	08/05/20 SMI"

#ifdef __cplusplus
extern "C" {
#endif

#ifdef linux
#include "pmfd_linux.h"
#else /* !linux */
/*
 * pmfd.h - Wrapper header file for the appropriate implementation
 * header, plus a few common definitions.
 */

#include <stdlib.h>
#include <locale.h>
#include <strings.h>
#include <signal.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include "sys/st_failfast.h"
#include <rgm/scutils.h>
#include <rgm/security.h>

#if DOOR_IMPL
#include <rgm/pmf.h>
#else
#include <rgm/rpc/pmf.h>
#endif

#include "pmfd_proto.h"
#include <sys/sol_version.h>

#if SOL_VERSION >= __s10
#include "pmfd_contracts.h"
#else
#include "pmfd_procfs.h"
#endif

/*
 * Used to mark messages that are considered "normal" and would have
 * been targets for i18n using gettext(). SYSTEXT distinguishes this
 * category of messages from unusual or failure messages.
 */
#define	SYSTEXT(s)	s

extern int debug;
extern struct rlimit pmf_rlimit;

/* used with pre_alloc_buf() to pre-allocate swap */
#define	PMF_MIN_SWAP_RESERVE_SIZE  4 		/* 4 MB */
#define	PMF_MAX_SWAP_RESERVE_SIZE  16		/* 16 MB */

/*
 * MAX_RPC_THREADS is the number of threads that the rpc server mechanism
 * is allowed to spawn at any one time to handle incoming rpc requests.
 * We assume that this number is large enough to handle any legitimate
 * situation, but not large enough to allow the rpc.pmfd to bring down the
 * node.  Empirically, it looks like 512 threads in the rpc.pmfd takes about
 * 64M of memory.
 */
#define	MAX_RPC_THREADS	512


/* these functions are called from a file with C linkage */

void pmf_free_status(pmf_status_result *result);
void pmfd_free_sema(sema_t *sema);
int pmfd_alloc_sema(sema_t **sema);
void close_on_exec(void);

#if DOOR_IMPL
void pmf_door_server(void *cookie, char *dataptr, size_t data_size,
    door_desc_t *desc_ptr, uint_t ndesc);
#endif

extern void pmf_start_svc(pmf_start_args *args, pmf_result *result,
	security_cred_t *cred);
extern void pmf_modify_svc(pmf_modify_args *args, pmf_result *result,
	security_cred_t *cred);
extern void pmf_status_svc(pmf_args *args, pmf_status_result *result,
	security_cred_t *cred);
extern void pmf_stop_svc(pmf_args *args, pmf_result *result,
	security_cred_t *cred);
extern void pmf_kill_svc(pmf_args *args, pmf_result *result,
	security_cred_t *cred);
extern void pmf_suspend_svc(pmf_args *args, pmf_result *result,
	security_cred_t *cred);
extern void pmf_resume_svc(pmf_args *args, pmf_result *result,
	security_cred_t *cred);
extern void pmf_null_svc(pmf_result *result);

#endif /* !linux */

#ifdef __cplusplus
}
#endif

#endif /* _PMFD_H */
