/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)pmfd_event_pool.c	1.3	08/05/20 SMI"

/*
 * pmf_event_pool.c  internal pmf events management
 */

#include "pmfd_event_pool.h"
#include <pthread.h>
#include <errno.h>
#include <sys/cl_assert.h>
#include <sys/sc_syslog_msg.h>
#include <rgm/scutils.h>

#define	POOL_THREAD (pthread_t)0

#ifdef LOCK_DEBUG
#define	LOCK_EVENT_POOL(name) \
	if (debug) dbg_msgout(NOGET("%s/%d LOCK EVENT POOL (0x%x)\n"), \
	NOGET(#name), __LINE__, &pmf_event_pool.lock); \
	(void) CL_PANIC(pthread_mutex_lock(&pmf_event_pool.lock) == 0);
#define	UNLOCK_EVENT_POOL(name) \
	if (debug) dbg_msgout(NOGET("%s/%d UNLOCK EVENT POOL(0x%x)\n"), \
	NOGET(#name), __LINE__, &pmf_event_pool.lock); \
	(void) CL_PANIC(pthread_mutex_unlock(&pmf_event_pool.lock)) == 0);
#else
#define	LOCK_EVENT_POOL(h) \
	CL_PANIC(pthread_mutex_lock(&pmf_event_pool.lock) == 0);
#define	UNLOCK_EVENT_POOL(h) \
	CL_PANIC(pthread_mutex_unlock(&pmf_event_pool.lock) == 0);
#endif

/* for tracking of strings that are not translated */
#define	NOGET(s)	s

extern int debug;
extern int pmf_alloc_cnt;

/*
 * Internally the event pool uses also a pmf_event_queue structure to enqueue
 * the available events of the pool.
 * This particular queue in managed internally and is NOT accessed using
 * the public API queue_pmf_event() and dequeue_pmf_event(). The public API
 * is used to access user defined queues.
 */
static struct pmf_event_queue pmf_event_pool = PMF_EVENT_QUEUE_INITIALIZER;

/*
 * The number of event chunks that have been allocated
 */
static int pmf_event_pool_chunk = 0;

/*
 * Get a free event from the pool. Pool is extended if no
 * more free events are available.
 */
struct pmf_event *
get_pmf_event_from_pool(void)
{
	sc_syslog_msg_handle_t sys_handle;
	struct pmf_event *evt, *prev, *p, *q;
	int chunk_size, i;

	LOCK_EVENT_POOL(get_pmf_event_from_pool);

	/*
	 * Find an event owned by the pool
	 */
	prev = (struct pmf_event *)0;
	for (evt = pmf_event_pool.head; evt; evt = evt->next) {
		if (pthread_equal(evt->owner, POOL_THREAD)) break;
		prev = evt;
	}

	if (!evt) {

		/*
		 * No free event available:
		 *	-allocate a new memory chunk
		 *	-split memory chunk in linked list of events
		 *	-link new list, starting a 2nd element, at pool head
		 *	-return first element of new list
		*/
		chunk_size = sizeof (struct pmf_event) * PMF_EVENT_POOL_SIZE;
		p = evt = (struct pmf_event *)svc_alloc(chunk_size,
		    &pmf_alloc_cnt);

		if (!p) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_PMF_PMFD_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * Need explanation of this message!
			 * @user_action
			 * Need a user action for this message.
			 */
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "Unable to allocate memory for new pmf "
			    "events chunk");
			sc_syslog_msg_done(&sys_handle);
			goto error;
		}

		pmf_event_pool_chunk++;
		if (debug)
			dbg_msgout(NOGET("Chunk number %d of %d events "
			    "allocated at 0x%x\n"),
			    pmf_event_pool_chunk, PMF_EVENT_POOL_SIZE, p);

		(void) memset(p, 0,
		    PMF_EVENT_POOL_SIZE * sizeof (struct pmf_event));
		for (i = 0; i < PMF_EVENT_POOL_SIZE-1; i++) {
			q = p++;
			q->next = p;
		}
		p->next = pmf_event_pool.head;
		pmf_event_pool.head = evt->next;

	} else {

		/*
		 * One free event found, unlink it from pool
		 */
		if (prev) {
			prev->next = evt->next;
		} else {
			pmf_event_pool.head = evt->next;
		}
	}

	UNLOCK_EVENT_POOL(get_pmf_event_from_pool);

	/*
	 * Clear event content
	 */
	(void) memset(evt, 0, sizeof (struct pmf_event));
	evt->type = PMFD_NOP;
	evt->owner = POOL_THREAD;
	evt->return_code.type = PMF_OKAY;
	(void) pthread_mutex_init(&evt->lock, (pthread_mutexattr_t *)0);
	(void) pthread_cond_init(&evt->cond, (pthread_condattr_t *)0);

	return (evt);

error:
	UNLOCK_EVENT_POOL(get_pmf_event_from_pool);
	return ((struct pmf_event *)0);

}

/*
 * Return a linked list of events to the pool, releasing
 * ownership if appropriate (calling thread is the event owner).
 * Events are relinked at pool head.
 */
void
return_pmf_event_to_pool(struct pmf_event **evt)
{

	struct pmf_event *p;

	if (evt && *evt) {
		/*
		 * release ownership of events
		 */
		for (p = *evt; p; p = p->next) {
			if (pthread_equal(p->owner, pthread_self())) {
				p->owner = POOL_THREAD;
				(void) pthread_cond_destroy(&p->cond);
				(void) pthread_mutex_destroy(&p->lock);
			}
		}
		/*
		 * seek at end of event list to relink it
		 * at pool head.
		 */
		p = *evt;
		while (p->next) {
			p = p->next;
		}
		LOCK_EVENT_POOL(return_pmf_event_to_pool);
		p->next = pmf_event_pool.head;
		pmf_event_pool.head = *evt;
		UNLOCK_EVENT_POOL(return_pmf_event_to_pool);
		*evt = (struct pmf_event *)0;
	}
}

/*
 * Acquire ownership of an event. Event must be currently owned by pool.
 */
int
acquire_pmf_event(struct pmf_event *evt)
{

	int rc = EINVAL;

	if (evt) {
		LOCK_EVENT_POOL(acquire_pmf_event);
		if (pthread_equal(evt->owner, POOL_THREAD)) {
			evt->owner = pthread_self();
			rc = 0;
		} else {
			rc = EBUSY;
		}
		UNLOCK_EVENT_POOL(acquire_pmf_event);
	}

	return (rc);

}

/*
 * Release ownership of an event. Calling thread must be the owner of
 * the event.
 * On success reference to event is set to 0.
 */
int
release_pmf_event(struct pmf_event **evt)
{

	int rc = EINVAL;

	if (evt && *evt) {
		LOCK_EVENT_POOL(release_pmf_event);
		if (pthread_equal((*evt)->owner, pthread_self())) {
			(*evt)->owner = POOL_THREAD;
			(void) pthread_cond_destroy(&(*evt)->cond);
			(void) pthread_mutex_destroy(&(*evt)->lock);
			*evt = (struct pmf_event *)0;
			rc = 0;
		} else {
			rc = EBUSY;
		}
		UNLOCK_EVENT_POOL(release_pmf_event);
	}

	return (rc);

}

/*
 * Public helper function used to enqueue an event on a user defined event
 * queue.
 */
void
queue_pmf_event(struct pmf_event_queue *queue, struct pmf_event *evt)
{

	struct pmf_event *p;

	if (queue && evt) {
		if ((p = queue->head)) {
			while (p->next) {
				p = p->next;
			}
			p->next = evt;
		} else {
			queue->head = evt;
		}
	}
}

/*
 * Public helper function used to dequeue an event from a user defined event
 * queue.
 */
struct pmf_event *
dequeue_pmf_event(struct pmf_event_queue *queue)
{

	struct pmf_event *p = (struct pmf_event *)0;

	if (queue && (p = queue->head)) {
		queue->head = p->next;
		p->next = (struct pmf_event *)0;
	}
	return (p);
}
