/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)pmfd_main.c	1.78	08/07/31 SMI"

/*
 * pmfd_main.c - Main for the pmf daemon.
 * (default from pmf_svc.c)
 */

#include "pmfd.h"
#include <sys/sol_version.h>
#if SOL_VERSION >= __s10
#include<zone.h>
#endif

#if DOOR_IMPL
#include <rgm/pmf.h>
#else
#include <rgm/rpc/pmf.h>
#endif


#include <rgm/scutils.h>
#include <sys/resource.h>
#include <sys/sc_syslog_msg.h>
#include <sys/rsrc_tag.h>
#ifndef linux
/* SCSLM addon start */
#include <sys/sol_version.h>
/* SCSLM addon end */
#endif
#ifdef linux
#include <signal.h>
#include "pmfd_dispatch.h"
#endif
#include <dlfcn.h>
#include <sys/stat.h>

/*
 * compile flag debug is used to disable some options during testing
 */
#ifdef DEBUG
static int DBG = 1;
int _rpcpmstart = 0;	/* Not started by a port monitor */
#else
static int DBG = 0;
#endif

#define	PMFD_LOCK	"/var/cluster/run/pmfd.lock"

/*
 * -d == debug (undocumented)
 */
#define	PMF_OPTARGS "d"

int debug = 0;
static char *progname;
struct rlimit pmf_rlimit;

#if !DOOR_IMPL
extern void pmf_program_2(struct svc_req *rqstp, register SVCXPRT *transp);
#endif

#if SOL_VERSION < __s10
extern void pmf_sig_handler(int sig);
#endif

static void usage(void);

static void
usage()
{
	(void) fprintf(stderr, (const char *)
		gettext("Usage: %s [-s]\n"), progname);
}

/*
 * On the cluster nodes, the fail-fast library is different
 * depending on the node type (server node or farm node).
 */
#define	FRGMD "/usr/cluster/lib/sc/frgmd"

#ifdef __sparcv9
#define	LIBCLST		"/usr/cluster/lib/sparcv9/libclst.so.1"
#define	LIBFCLST	"/usr/cluster/lib/sparcv9/libfclst.so.1"
#else
#define	LIBCLST		"/usr/cluster/lib/libclst.so.1"
#define	LIBFCLST	"/usr/cluster/lib/libfclst.so.1"
#endif

#define	ST_FF_ARM	"st_ff_arm"
#define	ST_FF_DISARM	"st_ff_disarm"

bool_t isfarmnode;

void
nodetype_init(void)
{
	struct stat filestat;

	if (stat(FRGMD, &filestat) < 0)
		isfarmnode = B_FALSE;
	else
		isfarmnode = B_TRUE;
}

int
failfast_arm(char *pname) {
	void *dlhandle;
	int (*fn_ff_arm)(char *);
	char *ff_lib = LIBCLST;

	if (isfarmnode) {
		ff_lib = LIBFCLST;
	}
	if ((dlhandle = dlopen(ff_lib, RTLD_LAZY)) == NULL) {
		return (-1);
	}
	if ((fn_ff_arm = (int(*)())
		dlsym(dlhandle, ST_FF_ARM)) /*lint !e611 */
		== NULL) {
		return (-1);
	}
	return ((*fn_ff_arm)(pname));
}

int
failfast_disarm() {
	void *dlhandle;
	int (*fn_ff_disarm)(void);
	char *ff_lib = LIBCLST;

	if (isfarmnode) {
		ff_lib = LIBFCLST;
	}

	if ((dlhandle = dlopen(ff_lib, RTLD_LAZY)) == NULL) {
		return (-1);
	}
	if ((fn_ff_disarm = (int(*)())
		dlsym(dlhandle, ST_FF_DISARM)) /*lint !e611 */
		== NULL) {
		return (-1);
	}
	return ((*fn_ff_disarm)());
}


void
solaris_farmpmf_sigterm_handler(void)
{
	if (isfarmnode) {
		(void) failfast_disarm();
		exit(0);
	}
}

int
main(int argc, char *argv[])
{
	int	i, err, rc;
	sc_syslog_msg_handle_t handle;

#if SOL_VERSION >= __s10
	char		zone_name[ZONENAME_MAX];
#else
	char *zone_name = NULL;
#endif /* SOL_VERSION >= __s10 */


#if !DOOR_IMPL
#ifndef linux
	int mode = RPC_SVC_MT_AUTO;
#endif
#endif

	/* we will increase the fd limit to "no limit" after we daemonize */
	struct rlimit rl = {(rlim_t)RLIM_INFINITY, (rlim_t)RLIM_INFINITY };
	struct sigaction saction;
	sigset_t sig;

	/*
	 * Workaround for 4274696 "code core dumps when calling
	 * _vsyslog while machine is running out of swap":
	 * Call ctime_r() early on to cause the
	 * timezone library to get initialized.
	 * Likewise, initialize the sc_syslog module early on.
	 */
	time_t now;
	char sinkdatebuf[30];

	(void) time(&now);
	(void) ctime_r(&now, sinkdatebuf);

	(void) sc_syslog_msg_set_syslog_tag(SC_SYSLOG_PMF_PMFD_TAG);

	(void) sc_syslog_msg_initialize(&handle, SC_SYSLOG_PMF_PMFD_TAG, "");
	if (handle != NULL)
		sc_syslog_msg_done(&handle);

	nodetype_init();

	if ((progname = strrchr(argv[0], '/')) == NULL)
		progname = argv[0];
	else
		progname++;

	(void) setlocale(LC_ALL, "");
	(void) textdomain(TEXT_DOMAIN);
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	/*
	 * Get command line options
	 */
	while ((i = getopt(argc, argv, PMF_OPTARGS)) != EOF) {
		switch (i) {
			case 'd':
				debug++;
				break;
			default:
				usage();
				exit(1);
		}
	}
#ifdef linux
	if (sigfillset(&sig) != 0) {
		/*
		 * Suppress lint message about "call to __errno() not
		 * made in the presence of a prototype."
		 * lint is unable to see the prototype for the __errno()
		 * function that errno is macroed to.
		 */
		/* CSTYLED */
		err = errno; /*lint !e746 */
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "sigfillset: %s\n", strerror(err));
		sc_syslog_msg_done(&handle);
		exit(1);
	}

	/*
	 * Block all signals an memorize previous mask in order to be able
	 * to restore it for then monitored commands.
	 */
	if (svc_sigprocmask(&sig) != 0) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "svc_sigprocmask failed.\n");
		sc_syslog_msg_done(&handle);
		exit(1);
	}
#endif /* linux */


	if (sigemptyset(&sig) != 0) {
		/*
		 * Suppress lint message about "call to __errno() not
		 * made in the presence of a prototype."
		 * lint is unable to see the prototype for the __errno()
		 * function that errno is macroed to.
		 */
		/* CSTYLED */
		err = errno; /*lint !e746 */
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "sigemptyset: %s\n", strerror(err));
		sc_syslog_msg_done(&handle);
		exit(1);
	}

#ifdef linux
	/*
	 * Linux.
	 * Setup up signal mask:
	 * SIGUSR1 is used in debug mode to display some internal states.
	 * SIGHUP is used internally to awake the dispatching thread and
	 * can be safelly caught at any time.
	 * SIGTERM is used to request termination of PMF daemon.
	 * (for the Europa Farm PMF implementation).
	 * These signals are all aknowledged with the single signal hanlder
	 * pmf_sig_handler.
	 * The implementation of this signal handler for Linux is different
	 * than that of Solaris and can be found within pmfd_dispatch.c
	 * source file.
	 *
	 * PMF daemons should be protected against others signals. If
	 * PMD dies unexpectedly, monitored processes could remain in
	 * stopped state.
	 */

	if (sigaddset(&sig, SIGCHLD) != 0) {
		err = errno;
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "sigaddset (SIGCHLD): %s\n", strerror(err));
		sc_syslog_msg_done(&handle);
		exit(1);
	}

	if (debug) {
		if (sigaddset(&sig, SIGUSR1) != 0) {
			err = errno;
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_PMF_PMFD_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * Need explanation of this message!
			 * @user_action
			 * Need a user action for this message.
			 */
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "sigaddset (SIGUSR1): %s\n", strerror(err));
			sc_syslog_msg_done(&handle);
			exit(1);
		}
	}

	if (sigaddset(&sig, SIGHUP) != 0) {
		err = errno;
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "sigaddset (SIGHUP): %s\n", strerror(err));
		sc_syslog_msg_done(&handle);
		exit(1);
	}

	if (sigaddset(&sig, SIGTERM) != 0) {
		err = errno;
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "sigaddset (SIGTERM): %s\n", strerror(err));
		sc_syslog_msg_done(&handle);
		exit(1);
	}

#else /* linux */
	/*
	 * Solaris:
	 * Setup up signal mask. SIGUSR1 is used by the suspend/resume
	 * options to interrupt the monitoring thread if it is waiting
	 * on blocking system call.
	 * SIGUSR1 is only used on version prior to S10.
	 * For Europa Farm, SIGTERM is also used to request PMF termination
	 * disarming the failfast.
	 */

	if (sigaddset(&sig, SIGUSR1) != 0) {
		err = errno;
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "sigaddset: %s\n", strerror(err));
		sc_syslog_msg_done(&handle);
		exit(1);
	}

#endif /* linux */

	if (sigprocmask(SIG_UNBLOCK, &sig, NULL) != 0) {
		err = errno;
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "sigprocmask: %s\n", strerror(err));
		sc_syslog_msg_done(&handle);
		exit(1);
	}

#ifdef linux
	/*
	 * Setup Linux signal handler to be used by threads.
	 */
	(void) memset(&saction, 0, sizeof (struct sigaction));
	saction.sa_handler = pmf_sig_handler;
	saction.sa_flags = 0;

	if (debug) {
		if (sigaction(SIGUSR1, &saction, NULL)) {
			err = errno;
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_PMF_PMFD_TAG, "");
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "sigaction: %s\n", strerror(err));
			sc_syslog_msg_done(&handle);
			exit(1);
		}
	}
	if (sigaction(SIGHUP, &saction, NULL)) {
		err = errno;
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "sigaction: %s\n", strerror(err));
		sc_syslog_msg_done(&handle);
		exit(1);
	}
	if (sigaction(SIGTERM, &saction, NULL)) {
		err = errno;
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "sigaction: %s\n", strerror(err));
		sc_syslog_msg_done(&handle);
		exit(1);
	}

	/*
	 * We want to wait for the status of our direct children.
	 * So we set action on SIGCHILD to default.
	 */
	saction.sa_handler = SIG_DFL;
	saction.sa_flags = 0;
	if (sigaction(SIGCHLD, &saction, NULL)) {
		err = errno;
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "sigaction: %s\n", strerror(err));
		sc_syslog_msg_done(&handle);
		exit(1);
	}

#else /* linux */

#if SOL_VERSION < __s10
	/*
	 * Setup Solaris signal handler.
	 */
	(void) memset(&saction, 0, sizeof (struct sigaction));
	saction.sa_handler = pmf_sig_handler;
	saction.sa_flags = 0;

	if (sigaction(SIGUSR1, &saction, NULL)) {
		err = errno;
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "sigaction: %s\n", strerror(err));
		sc_syslog_msg_done(&handle);
		exit(1);
	}

#endif /* SOL_VERSION < __s10 */

	if (isfarmnode) {
		(void) memset(&saction, 0, sizeof (struct sigaction));
		saction.sa_handler = solaris_farmpmf_sigterm_handler;
		saction.sa_flags = 0;
		if (sigaction(SIGTERM, &saction, NULL)) {
			err = errno;
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_PMF_PMFD_TAG, "");
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "sigaction: %s\n", strerror(err));
			sc_syslog_msg_done(&handle);
			exit(1);
		}
	}

#endif /* linux */

	/*
	 * Note: using the DBG flag is a way to allow
	 * running the server without linking to failfast during development.
	 */
	if (!DBG && getuid() != 0) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Must be root to start %s", progname);
		sc_syslog_msg_done(&handle);
		exit(1);
	}

	/*
	 * Backup the default number of file descriptors so that
	 * monitored processes can be started with these values.
	 */
	if (getrlimit(RLIMIT_NOFILE, &pmf_rlimit) == -1) {
		err = errno;
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "getrlimit: %s", strerror(err));
		sc_syslog_msg_done(&handle);
		exit(1);
	}

	/*
	 * Ensure that limits are not unlimited otherwise we will spend
	 * forever trying to close file descriptors upon daemonization.
	 */
	if (pmf_rlimit.rlim_cur == (rlim_t)RLIM_INFINITY) {
		pmf_rlimit.rlim_cur = FD_SETSIZE;
	}
	if (pmf_rlimit.rlim_max == (rlim_t)RLIM_INFINITY) {
		pmf_rlimit.rlim_max = pmf_rlimit.rlim_cur;
	}

	if (setrlimit(RLIMIT_NOFILE, &pmf_rlimit) == -1) {
		err = errno;
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "setrlimit: %s", strerror(err));
		sc_syslog_msg_done(&handle);
		exit(1);
	}

#ifndef linux
	/* SCSLM addon start */
#if SOL_VERSION < __s10
	/*
	 * Set microstate accounting for PMF.
	 * All of its childs will inherit from it.
	 * Microstate accounting is by default enabled since Solaris 10.
	 * A failure is not a fatal failure (should never happen).
	 * set_microstate_accounting(): in libscutils.
	 */
	if (set_microstate_accounting() != 0) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Unable to set microstate accounting. Continue.");
		sc_syslog_msg_done(&handle);

		/*
		 * Continue.
		 * We do not want to fail
		 */
	}
#endif
	/* SCSLM addon end */
#endif


#if !DOOR_IMPL
#ifndef linux
	if (!rpc_control(RPC_SVC_MTMODE_SET, &mode)) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Unable to set automatic MT mode.");
		sc_syslog_msg_done(&handle);
		exit(1);
	}

	/*
	 * Set the number of file descriptors to unlimited;
	 * set 2nd arg to non-zero int.
	 */
	mode = 1;
	if (!rpc_control(RPC_SVC_USE_POLLFD, &mode)) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * rpc.pmfd was unable to set the number of file descriptors
		 * used in the RPC server.
		 * @user_action
		 * Look for other syslog error messages on the same node. Save
		 * a copy of the /var/adm/messages files on all nodes, and
		 * report the problem to your authorized Sun service provider.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Unable to set number of file descriptors.");
		sc_syslog_msg_done(&handle);
		exit(1);
	}

	/*
	 * Set the number of concurrent threads allowed to a high number
	 */
	mode = MAX_RPC_THREADS;
	if (!rpc_control(RPC_SVC_THRMAX_SET, &mode)) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * The rpc.pmfd server was not able to set the maximum number
		 * of rpc threads. This happens while the server is starting
		 * up, at boot time. The server does not come up, and an error
		 * message is output to syslog.
		 * @user_action
		 * Save the /var/adm/messages file. Contact your authorized
		 * Sun service provider to determine whether a workaround or
		 * patch is available.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Unable to set maximum number of rpc threads.");
		sc_syslog_msg_done(&handle);
		exit(1);
	}
#endif /* !linux */
#endif /* !DOOR_IMPL */

#ifdef linux
	saction.sa_handler = SIG_IGN;
	saction.sa_flags = 0;
	if (sigaction(SIGPIPE, &saction, NULL)) {
		err = errno;
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "sigaction: %s\n", strerror(err));
		sc_syslog_msg_done(&handle);
		exit(1);
	}
#else
	(void) sigset(SIGPIPE, SIG_IGN);
#endif
	/*
	 * Daemonize if we're not in debug mode.
	 */
	if (!debug)
		make_daemon();

	/*
	 * ensure that another pmfd daemon is not running
	 * This must be done after the fork!
	 */
	make_unique(PMFD_LOCK);

#ifndef linux
	/*
	 * Increase the number of file descriptors to unlimited
	 * AFTER we daemonize, otherwise make_daemon() sits there
	 * forever trying to close file descriptors.
	 * The fix for bug 4351593 (Synopsis: convert Sun Cluster daemons
	 * to use closefrom(3C) in Solaris 8.1) will eliminate this problem.
	 */
	if (setrlimit(RLIMIT_NOFILE, &rl) == -1) {
		err = errno;
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * The rpc.pmfd server was not able to set the limit of files
		 * open. The message contains the system error. This happens
		 * while the server is starting up, at boot time. The server
		 * does not come up, and an error message is output to syslog.
		 * @user_action
		 * Save the /var/adm/messages file. Contact your authorized
		 * Sun service provider to determine whether a workaround or
		 * patch is available.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "setrlimit(RLIMIT_NOFILE): %s", strerror(err));
		sc_syslog_msg_done(&handle);
		exit(1);
	}

#endif /* !linux */

	/*
	 * pre_alloc_heap() will reserve an additional amount of heap
	 * space by mallocing and freeing a large size of memory.
	 * Since there is no limitation on how many processes could be
	 * monitored by pmfd, this pre-allocation cannot fully guarantee
	 * pmfd will not run into out of memory problem, but it will do
	 * some improvement.
	 */
	if (pre_alloc_heap((size_t)PMF_MAX_SWAP_RESERVE_SIZE,
	    (size_t)PMF_MIN_SWAP_RESERVE_SIZE) != 0) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Failed to pre-allocate swap space");
		sc_syslog_msg_done(&handle);
		exit(1);
	}

	/*
	 * This function locks us in memory and sets us in RT mode;
	 * if debug is on, it only sets debug mode and returns.
	 */
	if (svc_init(debug) == -1) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "svc_init failed.");
		sc_syslog_msg_done(&handle);
		exit(1);
	}

#ifdef linux
	/*
	 * Start the monitoring infrastructure. Success means
	 * that rpc services are registered and FF is armed.
	 */
	if ((rc = pmf_ptrace_monitor_startup()) != 0) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "pmf_ptrace_monitor_setup failed.");
		sc_syslog_msg_done(&handle);
		exit(1);
	}

	/*
	 * Wait for termination of the monitoring infrastructure.
	 * Will take care of disarming Failfast.
	 */
	rc = pmf_ptrace_monitor_join();

	exit(rc);

#else /* linux */

	/*
	 * Initialize loopbackset array and rpc service.
	 */
#if DOOR_IMPL
#if SOL_VERSION >= __s10
	if (getzonenamebyid(getzoneid(), zone_name, sizeof (zone_name)) < 0) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * The getzonenamebyid(3C) operation failed.
		 * @user_action
		 * Contact your authorized Sun service
		 * provider to determine whether a workaround
		 * or patch is available.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "getzonenamebyid failed in security_svc_reg.");
		sc_syslog_msg_done(&handle);
		exit(1);
	}
#endif
	if (security_svc_reg(pmf_door_server, RGM_PMF_PROGRAM_CODE,
	    debug, zone_name) < 0) {
#else
	if (security_svc_reg(pmf_program_2, PMF_PROGRAM_NAME,
	    (ulong_t)PMF_PROGRAM, (ulong_t)PMF_VERSION, debug) != 0) {
#endif /* DOOR_IMPL */
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "security_svc_reg failed.");
		sc_syslog_msg_done(&handle);
		exit(1);
	}

	/*
	 * Initialize the failfast driver.
	 * st_ff_arm returns error codes from the errno set, so use
	 * strerror to read them.
	 * If LOCAL is defined, skip it because we want to test
	 * on a standalone host so we don't link with the failfast libraries.
	 */
#ifndef LOCAL
	if ((rc = failfast_arm("pmfd")) != 0) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "st_ff_arm failed: %s", strerror(rc));
		sc_syslog_msg_done(&handle);
		exit(1);
	}
#endif /* LOCAL */

#if SOL_VERSION >= __s10
	contracts_initialize();
#endif

#if DOOR_IMPL
	while (1) {
		(void) pause();
	}
#else /* DOOR_IMPL */
	svc_run();
	(void) sc_syslog_msg_initialize(&handle, SC_SYSLOG_PMF_PMFD_TAG, "");
	(void) sc_syslog_msg_log(&handle, LOG_ERR, MESSAGE,
	    "svc_run returned");
	sc_syslog_msg_done(&handle);
	return (1);
#endif /* DOOR_IMPL */
	/* NOTREACHED */
#endif /* linux */
}
