//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef _RESTART_HISTORY_H
#define	_RESTART_HISTORY_H

#pragma ident	"@(#)restart_history.h	1.4	08/05/20 SMI"

#include <list>

//
// restart_history class stores a list of restart times within the
// specified period. It also discards any old entries if there are more
// than max_restarts_in entries on the list.
//
class restart_history {
public:
	// Asserts that max_restarts_in is > 0
	restart_history(int max_restarts_in, int period_in = -1);

	// Adds the current time to the list of restart times
	int add_restart_time();

	// Returns the number of restarts within the period
	uint_t get_retry_count(int &err);

	int get_period() { return period; }
	void set_period(int p) { period = p; clear(); }
	void clear() { restarts.clear(); }

protected:
	int prune();

	std::list<time_t> restarts;

	// period in seconds
	int period;
	int max_restarts;
};

#endif /* _RESTART_HISTORY_H */
