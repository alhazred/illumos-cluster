/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _PMFD_PROCFS_H
#define	_PMFD_PROCFS_H

#pragma ident	"@(#)pmfd_procfs.h	1.5	08/05/20 SMI"

/*
 * pmfd_procfs.h - PMF daemon data structures for the pre-Solaris 10
 * procfs-based implementation.
 */

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <synch.h>
#include "sys/cl_assert.h"

extern int pmf_thread_cnt;

#ifdef LOCK_DEBUG
#define	LOCK_HANDLE(h) \
	if (debug) dbg_msgout(NOGET("%s/%d LOCK HANDLE (0x%x)\n"), \
	__FILE__, __LINE__, &((h)->handle_lock)); \
	(void) CL_PANIC(pthread_mutex_lock(&((h)->handle_lock)) == 0);
#define	UNLOCK_HANDLE(h) \
	if (debug) dbg_msgout(NOGET("%s/%d UNLOCK HANDLE (0x%x)\n"), \
	__FILE__, __LINE__, &((h)->handle_lock)); \
	(void) CL_PANIC(pthread_mutex_unlock(&((h)->handle_lock)) == 0);
#define	LOCK_THREADS(h) \
	if (debug) dbg_msgout(NOGET("%s/%d LOCK THREADS (0x%x)\n"), \
	__FILE__, __LINE__, &((h)->threads_lock)); \
	(void) CL_PANIC(pthread_mutex_lock(&((h)->threads_lock)) == 0);
#define	UNLOCK_THREADS(h) \
	if (debug) dbg_msgout(NOGET("%s/%d UNLOCK THREADS (0x%x)\n"), \
	__FILE__, __LINE__, &((h)->threads_lock)); \
	(void) CL_PANIC(pthread_mutex_unlock(&((h)->threads_lock)) == 0);
#else
#define	LOCK_HANDLE(h) CL_PANIC(pthread_mutex_lock(&((h)->handle_lock)) == 0);
#define	UNLOCK_HANDLE(h) CL_PANIC(pthread_mutex_unlock(&((h)->handle_lock)) \
	== 0);
#define	LOCK_THREADS(h) CL_PANIC(pthread_mutex_lock(&((h)->threads_lock)) == 0);
#define	UNLOCK_THREADS(h) CL_PANIC(pthread_mutex_unlock(&((h)->threads_lock)) \
	== 0);
#endif

enum PMF_ACTION_WHY {
	PMF_ACTION_STARTED,
	PMF_ACTION_STOPPED,
	PMF_ACTION_KILLED,
	PMF_ACTION_EXITED,
	PMF_ACTION_FAILED
};
typedef enum PMF_ACTION_WHY PMF_ACTION_WHY;

/*
 * Handle to hold all the configuration information
 * for requests, along with state that the thread needs
 * in order to make decisions about restarting them.
 */
struct pmf_handle {
	char **cmd;
	char **env;
	char *pwd;
	char *path;
	PMF_ACTIONS action_type;
	char *action;
	int period;
	time_t last_requeue;
	int retries;
	char *id;
	uid_t uid;
	gid_t gid;
	pid_t pid;
	pthread_t tid;
	pmf_result result;
	/*
	 * handle_lock protects all fields of the handle except for
	 * the threads field, which is protected by a threads_lock, and
	 * the next and prev fields, which are protected by the global
	 * queue_lock.
	 * The threads_lock can be held independently of the handle_lock.
	 *
	 * Note that there are some fields of the handle that are read-only
	 * after creation, or that are only modified in the monitor thread.
	 * These fields are sometimes accessed without holding the handle_lock.
	 * Currently some fields are read without holding the handle_lock
	 * that _can_ be modified by other threads.
	 *
	 * Note that reading a field without acquiring a lock that could be
	 * modified concurrently in another thread could cause problems if,
	 * for example, pointers were switched out from under the reader.
	 * These instances need further investigation (see bugid 4445437).
	 *
	 * Fields that are sometimes accessed without the lock include:
	 *	history (although this can be changed by pmf_modify_svc)
	 *	action, action_type
	 *		(although this can be changed by pmf_stop_svc)
	 *	last_requeue
	 *	flags
	 *	id, pid, gid, uid (protected by pmf_fork_mutex when written)
	 *	result
	 *	pwd, env_passed, path, sema_p2c (protected by pmf_fork_mutex)
	 *
	 *	All fields are read in pmf_status_svc without holding the
	 *	handle_lock.
	 *
	 */
	pthread_mutex_t handle_lock;
	/*
	 * handle_cond is used for 2 different conditions: stop
	 * and kill threads use it to wait until the alive flag is set
	 * to 0, and the start_process thread uses it in pmf_throttle_wait
	 * to wait until a stop thread sets the stop_monitor flag to 1.
	 */
	pthread_cond_t handle_cond;
	int alive;
	/*
	 * condwait keeps track of the number of threads waiting on the
	 * handle_cond for the condition that the alive flag is 0.
	 * This field does not track the total number of threads waiting on
	 * the handle_cond for other conditions.
	 */
	int condwait;
	int signal;
	/*
	 * The two semaphores are used for communication between the
	 * pmf process and the newly forked child process to be monitored.
	 * The parent must not set up its triggers in /proc until the child
	 * has completed its post-fork/pre-exec work, and the child must
	 * not exec until the parent has set up its /proc triggers.
	 */
	sema_t *sema_p2c;
	sema_t *sema_c2p;
	/*
	 * The threads_lock protects the threads field.  It can be acquired
	 * whether or not the thread holds the handle_lock.  To avoid
	 * deadlock, the handle_lock should never be acquired while holding
	 * the threads_lock.
	 */
	pthread_mutex_t threads_lock;
	struct pmf_process_tag *history;
	struct pmf_threads *threads;
	struct pmf_handle *next;
	struct pmf_handle *prev;
	int flags;
	/*
	 * used in option -C to determine if to monitor children.
	 * if yes, what levels of children to monitor;
	 */
	PMF_MONITOR_CHILDREN monitor_children;
	int	monitor_level;
	/*
	 * shows if the user wants to pass the environment or to inherit
	 * the parent's environment
	 */
	bool_t env_passed;
	/*
	 * shows if the stop command was issued; if yes stop restarting tag.
	 */
	bool_t stop_monitor;
	/*
	 * restarting tag.set to true if the service under pmf needs restart.
	 */
	bool_t restart_flag;
	char *project_name;
};
typedef struct pmf_handle pmf_handle;

/*
 * We have a process tag for each process we start.
 * This is needed so we can calculate how many restarts have
 * occured within the specified period.
 */
struct pmf_process_tag {
	time_t stime; /* start time ... strictly informational */
	time_t etime;
	int wstat;
	struct pmf_process_tag *next;
};
typedef struct pmf_process_tag pmf_process_tag;

/*
 * State of a pmf thread: a thread is created with the state PMF_STARTING.
 * When the monitoring starts, the state is changed to PMF_MONITORED.
 * From PMF_MONITORED, pmfadm -S changes the state to PMF_SUSPENDING and then
 * the state is set to PMF_UNMONITORED if the monitoring has been effectively
 * suspended otherwise the state is set back to PMF_MONITORED.
 * From PMF_UNMONITORED, pmfadm -R changes the state to PMF_RESUMING and then
 * the state is set to PMF_MONITORED if the monitoring has been effectively
 * resumed otherwise the state is set back to PMF_UNMONITORED.
 * The state PMF_EXITING is set when the thread exit (on failure or normal
 * exit).
 *                                     -S
 * PMF_STARTING ----> PMF_MONITORED <====> PMF_SUSPENDING
 *                         ^                    |
 *                         |                    v
 *                    PMF_RESUMING  <====> PMF_UNMONITORED
 *                                   -R
 *
 * PMF_EXITING can be reached from any state.
 */
enum PMF_MONITOR_STATE {
	PMF_STARTING,
	PMF_RESUMING,
	PMF_MONITORED,
	PMF_SUSPENDING,
	PMF_UNMONITORED,
	PMF_EXITING
};
typedef enum PMF_MONITOR_STATE PMF_MONITOR_STATE;

#define	PMF_IS_MONITORED(t) ((t->monitor_state == PMF_MONITORED) || \
	(t->monitor_state == PMF_STARTING) || (t->monitor_state == PMF_EXITING))

struct pmf_threads {
	pthread_t tid;
	pid_t pid;
	int exitcode;
	struct pmf_threads *next;
	struct pmf_threads *prev;
	pmf_handle *handle;

	/*
	 * save here handle info we need while we don't hold the handle lock.
	 *
	 * monitor_children is used in options -O and -C
	 * to determine if to monitor children.
	 *
	 * child_level is used in option -C to determine if we're in the
	 * parent or first level child; the rest of the children are not
	 * monitored. It increases from 0 in the parent by 1 for every child
	 * level;
	 *
	 * first_call_returned is used to ignore the first fork call, which is
	 * the one that actually spawned the process.
	 *
	 * normal_exit tracks whether the monitored process exits normally
	 * or abnormally (due to signals).
	 */
	PMF_MONITOR_CHILDREN monitor_children;
	int child_level;
	int signal;
	boolean_t first_call_returned;
	boolean_t normal_exit;

	/*
	 * lock is used to protect the monitor_state field. It can be held
	 * independently of the handle_lock and of the threads_lock, but to
	 * avoid deadlock, the handle_lock or the threads_lock should never
	 * be acquired while holding this lock.
	 *
	 * cv is used to notify some changes of the monitor_state field.
	 *
	 * monitor_state is used to determine if the monitoring is suspended
	 * or not. It can be changed with the options -S and -R.
	 *
	 */
	pthread_mutex_t lock;
	pthread_cond_t cv;
	PMF_MONITOR_STATE monitor_state;
};
typedef struct pmf_threads pmf_threads;

/*
 * Time measured in microseconds.
 */
typedef long long usectime_t;

extern void pmf_freeresult(pmf_result *result);
extern int pmf_set_up_monitor(pmf_handle *handle);
extern void pmf_rejoin(pmf_handle *handle);
extern boolean_t is_process_monitored(pid_t pid);

#ifdef __cplusplus
}
#endif

#endif /* _PMFD_PROCFS_H */
