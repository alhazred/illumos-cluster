//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)pmf_contract.cc	1.14	09/02/27 SMI"

#include "pmf_contract.h"

#include <errno.h>

#include <sys/cl_assert.h>
#include <sys/sc_syslog_msg.h>
#include <sys/rsrc_tag.h>
#include <sys/cl_events.h>
#include <sys/types.h>
#include <sys/wait.h>

#include <sys/contract/process.h>
#include <priv.h>

#include <libclcontract/cl_contract_table.h>
#include <libclcontract/cl_contract_main.h>
#include "pmf_throttle_wait.h"
#include "pmf_error.h"

#ifdef SC_SRM
#include <pwd.h>
#include <sys/task.h>
#include <project.h>
#define	PWD_BUF_SIZE	1024
#endif

#include <sstream>

using namespace std;

const int pmf_contract::PID_LEN = 40;
const int pmf_contract::PMF_THROTTLE_LIMIT = 30;

#define	FATAL_EVENTS		((uint_t)(CT_PR_EV_HWERR))
#define	CRITICAL_EVENTS		\
			((uint_t)(CT_PR_EV_EMPTY|CT_PR_EV_FORK|CT_PR_EV_EXIT))
#define	INFORMATIVE_EVENTS	((uint_t)(CT_PR_EV_SIGNAL|CT_PR_EV_CORE))

//
// pmf_contract constructor
//
pmf_contract::pmf_contract(char *identifier_in, char **cmd_in, char **env_in,
    char *pwd_in, char *path_in, int retries_in, int period_in,
    char *action_in, int flags_in, char *project_name_in, uid_t uid_in,
    gid_t gid_in, int &err) :
    cl_contract(identifier_in, cmd_in,
    FATAL_EVENTS, CRITICAL_EVENTS, INFORMATIVE_EVENTS, err,
    SC_SYSLOG_PMF_PMFD_TAG, &debug_print,
    env_in, pwd_in, path_in),
    action_script_path(NULL), action_script_cmd(NULL),
    project_name(NULL), history(PMFD_MAX_HISTORY, period_in)
{
	debug_print(NOGET("pmf_contract ctor for %s\n"), identifier_in);

	if (err) {
		debug_print(NOGET("cl_contract ctor call from "
		    "pmf_contract ctor for %s gave error %d\n"), strerror(err));
		return;
	}

	// These could all be initialized in the initializer list, but
	// it was getting unwieldy.
	retries_allowed = retries_in;
	uid = uid_in;
	gid = gid_in;
	action_script_running = false;
	last_action_run = 0;
	current_signal = 0;

	//
	// Currently flags only contains an internal flag to quiet
	// the daemon on process failure.
	//
	flags = flags_in;

	if (project_name_in && *project_name_in) {
		project_name = strdup_nothrow(project_name_in);
		if (project_name == NULL) {
			err = ENOMEM;
			return;
		}
	}

	if (retries_in > PMFD_MAX_HISTORY) {
		err = E2BIG;
		// The dtor will cleanup
		return;
	}

	if (action_in && *action_in) {
		action_script_path = strdup_nothrow(action_in);
		if (action_script_path == NULL) {
			err = ENOMEM;
			return;
		}
	}

	if (action_script_path != NULL) {
		// create the cmd array once here so we don't need to
		// do it repeatedly later

		// Read the commands separated by spaces into a temp
		// vector using an istringstream for convenience.
		// The STL data structures throw exceptions.
		vector<string> command_list;
		try {
			istringstream istr(action_script_path);
			string temp_str;
			while (istr >> temp_str) {
				command_list.push_back(temp_str);
			}
		// CSTYLED
		} catch (bad_alloc&) {
			err = ENOMEM;
			return;
		}

		if (command_list.empty()) {
			// Assume this is supposed to be a NULL action script
			delete action_script_path;
			action_script_path = NULL;
		} else {
			// Create the array of char*s of the command
			// and all the arguments
			action_script_cmd = new (nothrow) char *[
				command_list.size() + 3];
			if (action_script_cmd == NULL) {
				err = ENOMEM;
				return;
			}

			// Copy each string argument into a separate slot
			// in the array.
			uint_t i;
			for (i = 0; i < command_list.size(); ++i) {
				action_script_cmd[i] = strdup_nothrow(
					command_list[i].c_str());
				if (action_script_cmd[i] == NULL) {
					err = ENOMEM;
					return;
				}
			}

			// Add the extra arguments that are passed to
			// the action script. See pmfadm(1M).
			action_script_cmd[i] = strdup_nothrow(
				NOGET("failed"));
			if (action_script_cmd[i] == NULL) {
				err = ENOMEM;
				return;
			}
			i++;
			action_script_cmd[i] = strdup_nothrow(
				nametag.c_str());
			if (action_script_cmd[i] == NULL) {
				err = ENOMEM;
				return;
			}
			i++;
			// NULL-terminate the array for exec.
			action_script_cmd[i] = NULL;
		}
	}
}

void
pmf_contract::free_members()
{
	delete action_script_path;
	delete project_name;
	free_string_arr(action_script_cmd);
}

// dtor
pmf_contract::~pmf_contract()
{
	debug_print(NOGET("pmf_contract dtor for %s\n"), nametag.c_str());
	free_members();
}

//
// This function increments the restart count and publishes
// ESC_CLUSTER_PMF_PROC_RESTART event.
//
int
pmf_contract::incr_restart_and_generate_event(boolean_t action_script_flag)
{
	os::sc_syslog_msg logger(SC_SYSLOG_PMF_PMFD_TAG, "", NULL);
	int err;
	int retry_count = history.get_retry_count(err);
	if (err != 0) {
		//
		// Failed to allocate memory for the history:
		// log message and end monitoring.
		//
		logger.log(LOG_ERR, MESSAGE,
		    "Tag %s: could not get retry count",
		    nametag.c_str());
		end_monitoring();
		return (-1);
	}

	// just restart
	debug_print(NOGET("%s: restarting -- retries_allowed"
	    "=%d, retry_count=%d\n"), nametag.c_str(),
	    retries_allowed, retry_count);

	//
	// SCMSGS
	// @explanation
	// A process monitored by PMF has exited and
	// is being restarted. tag represents the name
	// tag under which this process is registered.
	// cmd_path represents the full path and
	// arguments of the command being executed.
	// max_retries represents the total number of
	// retry attempts allowed. If the value of
	// max_retries is -1 then unlimited restarts
	// are allowed. num_retries represents the
	// current retry attempt number. If
	// num_retries is zero, this indicates that
	// the number of retries was exceeded but the
	// pmf action program decided to continue
	// restarting the monitored process. For more
	// information, see pmfadm(1M).
	// @user_action
	// This message is informational; no user
	// action is needed.
	//

	if (PMF_EXIT_QUIETLY_ISCLR(flags)) {
		logger.log(LOG_NOTICE, MESSAGE,
		    "PMF is restarting process that died: "
		    "tag=%s, cmd_path=%s, max_retries=%d, "
		    "num_retries=%d",
		    nametag.c_str(), cmd_string.c_str(),
		    retries_allowed, retry_count);
	}

	//
	// Generate event with reason CL_REASON_PMF_ACTION_SUCCESS
	// if action_script_flag is true.
	//
	if (action_script_flag) {
		// post an event
		(void) sc_publish_event(
		    ESC_CLUSTER_PMF_PROC_RESTART,
		    CL_EVENT_PUB_PMF, CL_EVENT_SEV_INFO,
		    CL_EVENT_INIT_SYSTEM, 0, 0, 0,
		    CL_PMF_NAME_TAG, SE_DATA_TYPE_STRING,
		    nametag.c_str(),
		    CL_PMF_CMD_PATH, SE_DATA_TYPE_STRING,
		    cmd_string.c_str(),
		    CL_TOTAL_ATTEMPTS, DATA_TYPE_UINT32,
		    retries_allowed,
		    CL_ATTEMPT_NUMBER, DATA_TYPE_UINT32, 0,
		    CL_REASON_CODE, DATA_TYPE_UINT32,
		    CL_REASON_PMF_ACTION_SUCCESS, NULL);
	} else {
		// post an event
		(void) sc_publish_event(
		    ESC_CLUSTER_PMF_PROC_RESTART,
		    CL_EVENT_PUB_PMF, CL_EVENT_SEV_INFO,
		    CL_EVENT_INIT_SYSTEM, 0, 0, 0,
		    CL_PMF_NAME_TAG, SE_DATA_TYPE_STRING,
		    nametag.c_str(),
		    CL_PMF_CMD_PATH, SE_DATA_TYPE_STRING,
		    cmd_string.c_str(),
		    CL_TOTAL_ATTEMPTS, DATA_TYPE_UINT32,
		    retries_allowed,
		    CL_ATTEMPT_NUMBER, DATA_TYPE_UINT32, 0,
		    CL_REASON_CODE, DATA_TYPE_UINT32,
		    CL_REASON_PMF_RESTART, NULL);
	}

	if (history.add_restart_time() != 0) {
		os::sc_syslog_msg logger(
		    SC_SYSLOG_PMF_PMFD_TAG, "", NULL);
		//
		// Failed to allocate memory for the history:
		// log message and end monitoring.
		//
		//
		// SCMSGS
		// @explanation
		// The rpc.pmfd server was not able to
		// allocate memory for the history of the tag
		// shown. Monitoring of he process associated
		// with the tag is stopped and pmfadm returns
		// an error. This problem can occur if the
		// machine has low memory.
		// @user_action
		// Determine if the machine is running out of
		// memory. If this is not the case, save the
		// /var/adm/messages file. Contact your
		// authorized Sun service provider to
		// determine whether a workaround or patch is
		// available.
		//
		logger.log(LOG_ERR, MESSAGE,
		    "Tag %s: could not allocate history.",
		    nametag.c_str());
		end_monitoring();
		return (-1);
	}
	return (0);
}

//
// setup_child()
//
// Called by a newly forked child process. Mostly the same as the code
// in pmf_run_process() in pmfd_util.c.
//
void
pmf_contract::setup_child()
{
	int err;
	os::sc_syslog_msg logger(SC_SYSLOG_PMF_PMFD_TAG, "", NULL);

	//
	// Make sure we're not running children as realtime
	// processes.  We restore the scheduling parameters
	// that rpc.pmfd was started with -- by default TS.
	//
	// The schedule information should be passed in
	// the RPC request packet and restored, but there
	// are security reasons for not doing so.  Force
	// the user to reestablish any special scheduling.
	//
	// In debug mode we skip this because we also skipped
	// setting the priority to RT in svc_init() in pmfd_main.c.
	// Both functions are in libscutils.
	//
	if (!debug && svc_restore_priority() == 1) {
		_exit(1);
	}

	//
	// Set the fd limit down so that a child ignorant of the fact
	// that we set FD_CLOEXEC and ignorant of closefrom(3C) doesn't
	// spend forever trying to close file descriptors upon
	// daemonization.
	//
	if (setrlimit(RLIMIT_NOFILE, &pmf_rlimit) == -1) {
		err = errno;
		//
		// SCMSGS
		// @explanation
		// rpc.pmfd was unable to set the number of file descriptors
		// before executing a process.
		// @user_action
		// Look for other syslog error messages on the same node. Save
		// a copy of the /var/adm/messages files on all nodes, and
		// report the problem to your authorized Sun service provider.
		//
		logger.log(LOG_ERR, MESSAGE, "setrlimit before exec: %s",
		    strerror(err));
	}

	//
	// Set close on exec on all open file descriptor
	//
	close_on_exec();

#ifdef SC_SRM
	//
	// Set project to the specified project name before starting
	// the application.  If setproject() call fails, the
	// application will be started with the system default project.
	//
	// Note that we must call setproject before we set the uid
	// of the process, because setproject can only be called by
	// root.
	//
	if (project_name != NULL && *project_name != '\0') {
		struct passwd pwd, *pwdp;
		char pwd_buff[PWD_BUF_SIZE];

		if (getpwuid_r(uid, &pwd, pwd_buff,
		    PWD_BUF_SIZE, &pwdp) == 0) {
			if (setproject(project_name,
			    pwd.pw_name, TASK_NORMAL) == -1) {
				err = errno;
				//
				// SCMSGS
				// @explanation
				// Either the given project name was invalid,
				// or the caller of setproject() was not a
				// valid user of the given project. The
				// process was launched with project "default"
				// instead of the specified project.
				// @user_action
				// Use the projects(1) command to check if the
				// project name is valid and the caller is a
				// valid user of the given project.
				//
				logger.log(LOG_ERR, MESSAGE, "setproject: %s; "
				    "attempting to continue the process with "
				    "the system default project.",
				    strerror(err));
				(void) setproject("default",
				    pwd.pw_name, TASK_NORMAL);
			} else if (debug) {
				debug_print(NOGET(
				    "Change project name to "
				    "%s\n"), project_name);
			}
		}
	} else {
		// project is not specified; set to default project
		struct passwd pwd, *pwdp;
		char pwd_buff[PWD_BUF_SIZE];
		if (getpwuid_r(uid, &pwd, pwd_buff,
		    PWD_BUF_SIZE, &pwdp) == 0) {
			(void) setproject("default", pwd.pw_name,
			    TASK_NORMAL);
		} else {
			(void) setproject("default", "root",
			    TASK_NORMAL);
		}
	}
#endif

	//
	// We must set the groupid before resetting
	// our userid - otherwise we won't have
	// permission to do the former.
	//
	if ((gid != NULL) && (setgid(gid) == -1)) {
		err = errno;
		logger.log(LOG_ERR, MESSAGE, "setgid: %s", strerror(err));
		_exit(1);
	}

	if ((uid != NULL) && (setuid(uid) == -1)) {
		err = errno;
		logger.log(LOG_ERR, MESSAGE, "setuid: %s", strerror(err));
		_exit(1);
	}

	if ((wdir != NULL) && (chdir(wdir) == -1)) {
		err = errno;
		//
		// SCMSGS
		// @explanation
		// The rpc.pmfd server was not able to change directory. The
		// message contains the system error. The server does not
		// perform the action requested by the client, and an error
		// message is output to syslog.
		// @user_action
		// Save the /var/adm/messages file. Contact your authorized
		// Sun service provider to determine whether a workaround or
		// patch is available.
		//
		logger.log(LOG_ERR, MESSAGE, "chdir: %s", strerror(err));
		_exit(1);
	}

	//
	// if user asked to pass env. vars, don't set the path;
	// otherwise set it to the caller path.
	//
	if (!env && (path != NULL)) {
		if (putenv(path) != 0) {
			err = errno;
			//
			// SCMSGS
			// @explanation
			// The rpc.pmfd server was not able to change
			// environment variables. The message contains the
			// system error. The server does not perform the
			// action requested by the client, and an error
			// message is output to syslog.
			// @user_action
			// Save the /var/adm/messages file. Contact your
			// authorized Sun service provider to determine
			// whether a workaround or patch is available.
			//
			logger.log(LOG_ERR, MESSAGE, "putenv: %s",
			    strerror(err));
			_exit(1);
		}
	}
}

// no need for locking, since action_script_path is never
// changed.
char*
pmf_contract::get_action_copy(int &err)
{
	err = 0;
	if (action_script_path != NULL) {
		char *res = strdup(action_script_path);
		if (res == NULL) {
			err = 1;
			return (NULL);
		}
		return (res);
	}
	return (NULL);
}

int
pmf_contract::get_retries()
{
	int res, err;
	CL_PANIC(pthread_mutex_lock(&lock) == 0);
	res = history.get_retry_count(err);
	// just ignore failures here
	CL_PANIC(pthread_mutex_unlock(&lock) == 0);
	return (res);
}


char*
pmf_contract::get_project_name_copy(int &err)
{
	err = 0;
	if (project_name != NULL) {
		char *res = strdup(project_name);
		if (res == NULL) {
			err = 1;
			return (NULL);
		}
		return (res);
	}
	return (NULL);
}

int
pmf_contract::signal_monitored(int signal)
{
	// identical for pmf_contract
	return (signal_all(signal));
}


int
pmf_contract::signal_all(int signal)
{
	CL_PANIC(pthread_mutex_lock(&lock) == 0);

	debug_print(NOGET("%s: signal_all(); sig=%d\n"), nametag.c_str(),
	    signal);

	// use sigsend(2) to send signal to every proc in the contract
	if (contract_id != 0 && signal != 0 &&
	    sigsend(P_CTID, contract_id, signal) == -1) {
		int err1 = errno;
		os::sc_syslog_msg logger(SC_SYSLOG_PMF_PMFD_TAG, "", NULL);
		//
		// SCMSGS
		// @explanation
		// The rpc.pmfd was not able to send the requested signal to
		// the specified contract id.
		// @user_action
		// If the message is isolated, it was probably a transient
		// error. If you were running pmfadm(1M) directly, retry the
		// requested action. If there are many instances of the
		// message, there is a problem with the contract file system.
		// Search for other syslog error messages on the same node.
		// Save a copy of the /var/adm/messages files on all nodes,
		// and report the problem to your authorized Sun service
		// provider.
		//
		logger.log(err1 == ESRCH ? LOG_NOTICE : LOG_ERR, MESSAGE,
		    "Error sending signal to contract <%d>: %s",
		    contract_id, strerror(err1));
		if (err1 != ESRCH) {
			CL_PANIC(pthread_mutex_unlock(&lock) == 0);
			return (err1);
		}
	}
	CL_PANIC(pthread_mutex_unlock(&lock) == 0);
	return (0);
}

void
pmf_contract::set_retries_allowed(int retries_in)
{
	CL_PANIC(pthread_mutex_lock(&lock) == 0);
	retries_allowed = retries_in;
	// Setting the retries clears the history (for historical reasons?)
	history.clear();
	CL_PANIC(pthread_mutex_unlock(&lock) == 0);
}

void
pmf_contract::set_period(int period_in)
{
	CL_PANIC(pthread_mutex_lock(&lock) == 0);
	history.set_period(period_in);
	CL_PANIC(pthread_mutex_unlock(&lock) == 0);
}

//
// event_dispatch()
//
// Switches on the event type. Acks critical events (empty, fork, and exit),
// and calls the appropriate method to process them.
//
void
pmf_contract::event_dispatch(ct_evthdl_t ev)
{
	CL_PANIC(pthread_mutex_lock(&lock) == 0);

	// assert that this event is for us
	ctid_t ev_ctid = ct_event_get_ctid(ev);
	CL_PANIC(ev_ctid == contract_id);

	switch (ct_event_get_type(ev)) {
	case CT_PR_EV_EMPTY:
		empty_event(ev);
		ack_event(ev);
		break;
	case CT_PR_EV_FORK:
		fork_event(ev);
		ack_event(ev);
		break;
	case CT_PR_EV_EXIT:
		exit_event(ev);
		ack_event(ev);
		break;
	case CT_PR_EV_CORE:
		// fall through
	case CT_PR_EV_SIGNAL:
		// fall through
	case CT_PR_EV_HWERR:
		// No need to ack these because they aren't critical
		debug_print(NOGET("%s received non-critical event %d\n"),
		    nametag.c_str(), ct_event_get_type(ev));
		break;
	default:
		debug_print(NOGET("%s received unrecognized event %d\n"),
		    nametag.c_str(), ct_event_get_type(ev));
		break;
	}

	CL_PANIC(pthread_mutex_unlock(&lock) == 0);
}

//
// fork_event()
//
// Handle a fork event for the current contract by storing the
// forked process id in the procs table
//
// Assumes the lock is held
//
void
pmf_contract::fork_event(ct_evthdl_t ev)
{
	pid_t pid, ppid;
	int err;
	os::sc_syslog_msg logger(SC_SYSLOG_PMF_PMFD_TAG, "", NULL);

	//
	// Get the id of the new process and its parent
	//
	if ((err = ct_pr_event_get_pid(ev, &pid)) != 0) {
		//
		// SCMSGS
		// @explanation
		// The rpc.pmfd was unable to retrieve the pid from a contract
		// event. The rpc.pmfd will continue to monitor the process,
		// but the rpc.pmfd may have missed an event of interest.
		// @user_action
		// Search for other syslog error messages on the same node.
		// Save a copy of the /var/adm/messages files on all nodes,
		// and report the problem to your authorized Sun service
		// provider.
		//
		logger.log(LOG_ERR, MESSAGE, "fork_event: "
		    "ct_pr_event_get_pid: %s", strerror(err));

		// Nothing we can do to recover the missing info, but
		// we might as well continue monitoring.
		return;
	}

	if ((err = ct_pr_event_get_ppid(ev, &ppid)) != 0) {
		//
		// SCMSGS
		// @explanation
		// The rpc.pmfd was unable to retrieve the parent pid from a
		// contract event. The rpc.pmfd will continue to monitor the
		// process, but the rpc.pmfd may have missed an event of
		// interest.
		// @user_action
		// Search for other syslog error messages on the same node.
		// Save a copy of the /var/adm/messages files on all nodes,
		// and report the problem to your authorized Sun service
		// provider.
		//
		logger.log(LOG_ERR, MESSAGE, "ct_pr_event_get_ppid: %s",
		    strerror(err));

		// Nothing we can do to recover the missing info, but
		// we might as well continue monitoring.
		return;
	}


	debug_print(NOGET("fork_event() for %s: pid=%d, ppid=%d\n"),
	    nametag.c_str(), pid, ppid);

	//
	// Look up the parent in the process table
	//
	std::map<pid_t, int>::iterator it = procs.find(ppid);
	if (it == procs.end()) {
		//
		// The parent doesn't exist. This one must be the
		// first process.
		//
		// Use operator[] instead of insert() to overwrite current
		// value if it exists. The entry should exist already only
		// if it's the root process of the tree (the parent process
		// of the process tree).
		//
		debug_print(NOGET("Nametag %s: adding pid %d at depth 0\n"),
		    nametag.c_str(), pid);
		procs[pid] = 0;
	} else {
		//
		// Add this entry with a depth of the depth of the parent
		// plus 1.
		//
		debug_print(NOGET("Nametag %s: adding pid %d at depth %d\n"),
		    nametag.c_str(), pid, it->second + 1);
		procs[pid] = it->second + 1;
	}
}

//
// This function restarts the process.
//
void
pmf_contract::restart_process(boolean_t action_script_flag)
{
	os::sc_syslog_msg logger(SC_SYSLOG_PMF_PMFD_TAG, "", NULL);
	int err = 0;

	// Increment the restart count and generate restart event.
	if ((err = incr_restart_and_generate_event(
	    action_script_flag)) != 0)
		return;

	// execute the process again
	pid_t child_pid;
	if (start_process(cmd, child_pid) != 0) {
		debug_print(NOGET("%s: failed to start, so "
		    "ending monitoring."), nametag.c_str());
		end_monitoring();
	}
}

//
// This function contains the throttle wait logic.
//
void
pmf_contract::perform_throttling_or_restart(boolean_t action_script_flag)
{
	os::sc_syslog_msg logger(SC_SYSLOG_PMF_PMFD_TAG, "", NULL);
	// throttle wait logic
	time_t req = time(NULL);
	if (req == (time_t)-1) {
		int err = errno;
		//
		// SCMSGS
		// @explanation
		// The time(2) function failed with the specified
		// error. This error will cause the rpc.pmfd to stop
		// monitoring the specified nametag.
		// @user_action
		// Search for other syslog error messages on the same
		// node. Save a copy of the /var/adm/messages files on
		// all nodes, and report the problem to your
		// authorized Sun service provider.
		//

		os::sc_syslog_msg logger(SC_SYSLOG_PMF_PMFD_TAG, "",
			NULL);
		logger.log(LOG_ERR, MESSAGE, "time for %s: %s",
		    nametag.c_str(), strerror(err));
		//
		// not ideal, but the only thing we can safely do
		//
		end_monitoring();
	}

	req -= last_action_run;
	last_action_run = time(NULL);

	//
	// req now stores the time since the last time
	// we ran the action script. If we have ran the
	// action script within two times the
	// PMF_THROTTLE_LIMIT (curently 2 * 30 == 60
	// seconds), we need to wait a little while
	// before running it again ("throttle back.")
	//
	// We wait for twice as long as it's been since
	// we last ran the action script, up to the
	// PMF_THROTTLE_LIMIT (30 seconds, currently).
	//
	if (req < PMF_THROTTLE_LIMIT * 2) {
		//
		// check if action script is specified or
		// retries specified is infinite (-1).
		//
		if ((action_script_path == NULL) ||
		    (retries_allowed == -1))
			//
			// In this case we need to increment
			// the restart count in the
			// start_process.So set the flag.
			//
			incr_restart_cnt_flag = B_TRUE;
		if (req == 0) {
			req++;
		} else if (req < PMF_THROTTLE_LIMIT) {
			req *= 2;
		}
		if (req > PMF_THROTTLE_LIMIT) {
			req = PMF_THROTTLE_LIMIT;
		}

		debug_print(NOGET("%s: to throttle "
		    "wait\n"),
		    nametag.c_str());

		//
		// SCMSGS
		// @explanation
		// The tag shown, run by rpc.pmfd server, is
		// restarting and exiting too often. This means more
		// than once a minute. This can happen if the
		// application is restarting, then immediately exiting
		// for some reason, then the action is executed and
		// returns OK (0), which causes the server to restart
		// the application. When this happens, the rpc.pmfd
		// server waits for up to 1 min before it restarts the
		// application.
		//
		// An error message is output to syslog.
		// @user_action
		// Examine the state of the application, try to figure
		// out why the application doesn't stay up, and yet
		// the action returns OK.
		//

		logger.log(LOG_NOTICE, MESSAGE,
		    "\"%s\" restarting too often "
		    "... sleeping %d seconds.",
		    nametag.c_str(), req);

		// post an event
		(void) sc_publish_event(
			ESC_CLUSTER_PMF_PROC_NOT_RESTARTED,
			    CL_EVENT_PUB_PMF, CL_EVENT_SEV_INFO,
			    CL_EVENT_INIT_SYSTEM, 0, 0, 0,
			    CL_PMF_NAME_TAG,
			    SE_DATA_TYPE_STRING, nametag.c_str(),
			    CL_PMF_CMD_PATH,
			    SE_DATA_TYPE_STRING, cmd_string.c_str(),
			    CL_FAILURE_REASON, DATA_TYPE_UINT32,
			    CL_FR_PMF_PROC_THROTTLED, NULL);

		// This call returns immediately
		if (pmf_throttle_wait::instance().wait(
		    smart_ptr<cl_contract>(this),
		    req) != 0) {
			// not ideal, but the only
			// thing we can safely do
			debug_print(NOGET("%s: "
			    "pmf_throttle_wait::"
			    "wait() failed. Ending"
			    " monitoring.\n"),
			    nametag.c_str());
			end_monitoring();
		}
	} else {
		// just restart
		restart_process(action_script_flag);
		return;
	}
}

//
// exit_event()
//
// Handle an exit event for the current contract by removing the
// process id from the procs table, and retrieving the exit status
// if the exited process was the "parent" process.
//
// Assumes the lock is held.
//
void
pmf_contract::exit_event(ct_evthdl_t ev)
{
	pid_t pid, ppid;
	int err;
	os::sc_syslog_msg logger(SC_SYSLOG_PMF_PMFD_TAG, "", NULL);

	//
	// Get the id of the process that exited
	//
	if ((err = ct_pr_event_get_pid(ev, &pid)) != 0) {
		//
		// SCMSGS
		// @explanation
		// The rpc.pmfd was unable to retrieve the pid from a contract
		// event. The rpc.pmfd will continue to monitor the process,
		// but the rpc.pmfd may have missed an event of interest.
		// @user_action
		// Search for other syslog error messages on the same node.
		// Save a copy of the /var/adm/messages files on all nodes,
		// and report the problem to your authorized Sun service
		// provider.
		//
		logger.log(LOG_ERR, MESSAGE, "exit_event: "
		    "ct_pr_event_get_pid: %s", strerror(err));

		// Nothing we can do to recover the missing info, but
		// we might as well continue monitoring. We should receive
		// an empty event eventually.
		return;
	}

	//
	// If the process that just exited is the parent root process,
	// get its exit status
	//
	if (pid == parent_pid) {
		if ((err = ct_pr_event_get_exitstatus(ev, &parent_return_code))
		    != 0) {
			//
			// SCMSGS
			// @explanation
			// The rpc.pmfd was unable to determine the exit
			// status of a process under its control that exited.
			// It will assume failure.
			// @user_action
			// Search for other syslog error messages on the same
			// node. Save a copy of the /var/adm/messages files on
			// all nodes, and report the problem to your
			// authorized Sun service provider.
			//
			logger.log(LOG_ERR, MESSAGE,
			    "ct_pr_event_get_exitstatus: %s",
			    strerror(err));

			// Nothing we can do to recover the missing info, but
			// we might as well continue monitoring. We should
			// receive an empty event eventually.
			//
			// Assume failure of the process, in case it's an
			// action script.
			//
			parent_return_code = 1;
			return;
		}
		// clean up the zombie process
		(void) waitpid(pid, &err, 0);
	}

	debug_print(NOGET("%s: process %d exited\n"), nametag.c_str(), pid);

	// Remove this entry from the process table
	procs.erase(pid);
}

//
// empty_event()
//
// Assumes the lock is held
//
void
pmf_contract::empty_event(ct_evthdl_t ev)
{
	os::sc_syslog_msg logger(SC_SYSLOG_PMF_PMFD_TAG, "", NULL);
	int err;

	debug_print(NOGET("%s: empty event -- is_running=%s, "
	    "action_script_running=%s\n"), nametag.c_str(), is_running ?
	    "true" : "false", action_script_running ? "true" : "false");

	// Verify that our list of processes is empty
	if (!procs.empty()) {
		//
		// SCMSGS
		// @explanation
		// The rpc.pmfd received an empty contract event before
		// receiving exit events for one or more processes in the
		// contract. This error will not impact the rpc.pmfd's
		// functionality.
		// @user_action
		// No action required.
		//
		logger.log(LOG_ERR, MESSAGE, "\"%s\": missed one or more "
		    "exit events", nametag.c_str());
	}


	// abandon the contract so it will go away
	if ((err = ct_ctl_abandon(contract_fd)) != 0) {
		//
		// SCMSGS
		// @explanation
		// The rpc.pmfd was unable to abandon an empty contract. The
		// contract will continue to exist even though it has no
		// processes in it.
		// @user_action
		// If the message is isolated, it was probably a transient
		// error. If there are many instances of the message, there is
		// a problem with the contract file system. Search for other
		// syslog error messages on the same node. Save a copy of the
		// /var/adm/messages files on all nodes, and report the
		// problem to your authorized Sun service provider.
		//
		logger.log(LOG_ERR, MESSAGE, "ct_ctl_abandon: %s",
		    strerror(err));
	}
	(void) close(contract_fd);
	contract_fd = 0;

	// Remove the contract / nametag mapping from the contract table
	cl_contract_table::instance().remove_mapping(contract_id);
	contract_id = 0;

	// reset the signal
	current_signal = 0;

	//
	// Set is_running to false and broadcast to any waiters
	// that it's done.
	//
	// If stop_monitoring is set, we know this process won't be
	// restarted, so we can just broadcast without forcing the waiters
	// to wake up. It's important in this case to hold the lock
	// until after we've called end_monitoring(), to avoid a race
	// condition where the pmfadm client thinks the tag is gone
	// but it's still in the contract table, so pmf_status_svc
	// still returns info about.
	//
	// However, if stop_monitoring is not set, then the process may be
	// restarted, so we need to make sure all waiters wake up and run
	// before we restart the process. Otherwise the waiters may wake
	// up, find the process already restarted, and thus miss their
	// window.
	//
	is_running = false;

	if (stop_monitoring) {
		(void) pthread_cond_broadcast(&cond);
	} else {

		while (waiters > 0) {
			debug_print(NOGET("%s: waiters is %d\n"),
			    nametag.c_str(), waiters);
			(void) pthread_cond_broadcast(&cond);
			CL_PANIC(pthread_mutex_unlock(&lock) == 0);
			thr_yield();
			CL_PANIC(pthread_mutex_lock(&lock) == 0);
		}

		// first check if we were running an action script
		if (action_script_running) {
			action_script_finished();
			return;
		}
		err = 0;
		int retry_count = history.get_retry_count(err);
		if (err != 0) {
			//
			// Failed to allocate memory for the history:
			// log message and end monitoring.
			//

			//
			// SCMSGS
			// @explanation
			// The rpc.pmfd server was not able to allocate memory
			// for the history of the tag shown. Monitoring of the
			// process associated with the tag is stopped and
			// pmfadm returns an error. This problem can occur if
			// the machine has low memory.
			// @user_action
			// Determine if the machine is running out of memory.
			// If this is not the case, save the /var/adm/messages
			// file. Contact your authorized Sun service provider
			// to determine whether a workaround or patch is
			// available.
			//
			logger.log(LOG_ERR, MESSAGE,
			    "Tag %s: could not get retry count",
			    nametag.c_str());
			end_monitoring();
			return;
		}

		// Check if we should restart
		if (retries_allowed == -1 || retry_count < retries_allowed) {
			debug_print(NOGET("%s: restarting -- retries_allowed"
			    "=%d, retry_count=%d\n"), nametag.c_str(),
			    retries_allowed, retry_count);
			//
			// Apply throttling logic if action script is not
			// specified for this tag or if specified along
			// with infinite retry count (-1).
			//
			if ((action_script_path == NULL) ||
			    (retries_allowed == -1))
				// Set the action_script_flag to false.
				perform_throttling_or_restart(B_FALSE);
			else
				restart_process(B_FALSE);
			return;
		}

		if (PMF_EXIT_QUIETLY_ISCLR(flags)) {
			//
			// SCMSGS
			// @explanation
			// The tag shown, being run by the rpc.pmfd server,
			// has exited. Either the user has decided to stop
			// monitoring this process, or the process exceeded
			// the number of retries. An error message is output
			// to syslog.
			// @user_action
			// This message is informational; no user action is
			// needed.
			//
			logger.log(LOG_NOTICE, MESSAGE, "Process: tag=\"%s\", "
			"cmd=\"%s\", Failed to stay up.",
			nametag.c_str(), cmd_string.c_str());

		}

		// post an event
		(void) sc_publish_event(
		    ESC_CLUSTER_PMF_PROC_NOT_RESTARTED,
		    CL_EVENT_PUB_PMF, CL_EVENT_SEV_INFO,
		    CL_EVENT_INIT_SYSTEM, 0, 0, 0,
		    CL_PMF_NAME_TAG, SE_DATA_TYPE_STRING,
		    nametag.c_str(), CL_PMF_CMD_PATH, SE_DATA_TYPE_STRING,
		    cmd_string.c_str(), CL_FAILURE_REASON, DATA_TYPE_UINT32,
		    CL_FR_PMF_RETRIES_EXCEEDED, NULL);

		if (action_script_path != NULL) {
			// run the action script
			history.clear();
			start_action_script();
			return;
		}
	}

	// If we reach here we're either supposed to stop monitoring,
	// or we've execeeded our retries and there is no action script
	// to run.
	end_monitoring();
}

//
// start_action_script()
//
// Parallel to start(), but only used internally.
// Wrapper for a call to start_process, passing the action_script_cmd.
// If start_process() is unsuccessful, calls end_monitoring().
// Assumes the lock is held.
//
void
pmf_contract::start_action_script()
{
	pid_t child_pid;
	if (start_process(action_script_cmd, child_pid) == 0) {
		debug_print(NOGET("Successfully started action script for "
		    "tag %s\n"), nametag.c_str());
		action_script_running = true;
	} else {
		debug_print(NOGET("Could not fork action script for tag %s, "
		    "so ending monitoring.\n"), nametag.c_str());
		end_monitoring();
	}
}

//
// action_script_finished()
//
// Processes an action script that finished running.
//
// If the action script succeeded (determined by the return code stored
// in parent_return_code), executes the throttle-wait logic.
// If the script failed, calls end_monitoring().
//
void
pmf_contract::action_script_finished()
{
	os::sc_syslog_msg logger(SC_SYSLOG_PMF_PMFD_TAG, "", NULL);


	debug_print(NOGET("%s: action script had return code %d\n"),
	    nametag.c_str(), parent_return_code);

	// reset the flag
	action_script_running = false;
	if (parent_return_code != 0) {
		//
		// The action script failed.
		// End monitoring immediately.
		//
		parent_return_code = 0;
		//
		// SCMSGS
		// @explanation
		// The process with the given tag failed to stay up and
		// exceeded the allowed number of retry attempts (given by the
		// 'pmfadm -n' option) and the action (given by the 'pmfadm
		// -a' option) was initiated by rpc.pmfd. The action failed
		// (i.e., returned non-zero), and rpc.pmfd will delete this
		// tag from its tag list and discontinue retry attempts. For
		// more information, see pmfadm(1M).
		// @user_action
		// This message is informational; no user action is needed.
		//
		logger.log(LOG_INFO, MESSAGE,
		    "PMF did not restart tag <%s>, cmd_path=\"%s\"",
		    nametag.c_str(), cmd_string.c_str());
		// post an event
		(void) sc_publish_event(
		    ESC_CLUSTER_PMF_PROC_NOT_RESTARTED,
		    CL_EVENT_PUB_PMF, CL_EVENT_SEV_INFO,
		    CL_EVENT_INIT_SYSTEM, 0, 0, 0,
		    CL_PMF_NAME_TAG, SE_DATA_TYPE_STRING,
		    nametag.c_str(),
		    CL_PMF_CMD_PATH, SE_DATA_TYPE_STRING,
		    cmd_string.c_str(),
		    CL_FAILURE_REASON, DATA_TYPE_UINT32,
		    CL_FR_PMF_ACTION_FAILED, NULL);
		end_monitoring();
	} else {
		//
		// The action script succeeded.
		// perform throttling.
		//
		// Set the action_script_flag to true.
		//
		perform_throttling_or_restart(B_TRUE);
	}
}

//
// get_pids_string()
//
// Collects the list of pids and returns it.
//
char*
pmf_contract::get_pids_string(int &err)
{
	uint_t buf_len;
	char	tmp_buf[PID_LEN];	// temp buf to get size of each pid
	char	*ptr;		// ptr to pid_buf
	char *res;

	err = 0;
	CL_PANIC(pthread_mutex_lock(&lock) == 0);
	if (procs.empty()) {
		CL_PANIC(pthread_mutex_unlock(&lock) == 0);
		return (NULL);
	}

	//
	// calculate size of buf
	//
	buf_len = 0;

	// CSTYLED
	for (std::map<pid_t, int>::const_iterator it = procs.begin();
	    it != procs.end(); ++it) {
		(void) memset(tmp_buf, 0, sizeof (tmp_buf));
		(void) sprintf(tmp_buf, "%d", it->first);
		// 1 is for the space
		buf_len += (uint_t)strlen(tmp_buf) + 1;
	}

	// +2 for the extra space and the \0
	buf_len += 2;

	//
	// malloc buf of correct size
	//
	res = (char *)malloc((size_t)(buf_len));
	if (res == NULL) {
		CL_PANIC(pthread_mutex_unlock(&lock) == 0);
		err = 1;
		return (NULL);
	}
	(void) memset(res, 0, (size_t)(buf_len));

	//
	// copy pids into buf
	//
	ptr = res;

	// CSTYLED
	for (std::map<pid_t, int>::const_iterator it = procs.begin();
	    it != procs.end(); ++it) {
		(void) sprintf(ptr, "%d ", it->first);
		ptr += strlen(ptr);
	}
	// We want to overwrite the last trailing space with a \0
	*(ptr - 1) = '\0';

	CL_PANIC(pthread_mutex_unlock(&lock) == 0);
	return (res);
}


//
// get_pids()
//
// Collects the list of pids and returns it.
//
int
pmf_contract::get_pids_vector(std::vector<pid_t>& pids)
{
	CL_PANIC(pthread_mutex_lock(&lock) == 0);

	try {
		pids.resize(procs.size());
	// CSTYLED
	} catch (bad_alloc&) {
		CL_PANIC(pthread_mutex_unlock(&lock) == 0);
		return (1);
	}

	// CSTYLED
	for (std::map<pid_t, int>::const_iterator it = procs.begin();
	    it != procs.end(); ++it) {
		pids.push_back(it->first);
	}
	CL_PANIC(pthread_mutex_unlock(&lock) == 0);
	return (0);
}

// Simply constructs the parent class part of the object and initialzies
// the depth
pmf_contract_depth::pmf_contract_depth(char *identifier_in, char **cmd_in,
    char **env_in, char *pwd_in, char *path_in, int retries_in, int period_in,
    char *action_in, int flags_in, char *project_name_in, uid_t uid_in,
    gid_t gid_in, int depth_in, int &err) : pmf_contract(identifier_in, cmd_in,
    env_in, pwd_in, path_in, retries_in, period_in, action_in, flags_in,
    project_name_in, uid_in, gid_in, err), depth(depth_in)
{
}

//
// signal_monitored()
//
// Sends the specified signal to each process in the list of processes.
//
int
pmf_contract_depth::signal_monitored(int signal)
{
	int err;

	CL_PANIC(pthread_mutex_lock(&lock) == 0);

	debug_print(NOGET("pmf_contract_depth::signal_monitored %d\n"),
	    signal);

	//
	// save the signal in case a process is forking as we
	// are walking the list and we miss the new process.
	// The fork_event() will then send the signal to the newly
	// forked process.
	//
	current_signal = signal;

	// CSTYLED
	for (std::map<pid_t, int>::const_iterator it = procs.begin();
	    it != procs.end(); ++it) {
		if ((err = send_signal(it->first, signal)) != 0) {
			CL_PANIC(pthread_mutex_unlock(&lock) == 0);
			return (err);
		}
	}
	CL_PANIC(pthread_mutex_unlock(&lock) == 0);
	return (0);
}

// Assumes caller holds the lock
void
pmf_contract_depth::exit_event(ct_evthdl_t ev)
{
	pmf_contract::exit_event(ev);
	if (!action_script_running && procs.empty()) {
		// if that was the last process, fake an empty event
		empty_event(ev);
	}
}

//
// fork_event()
//
// Handle a fork event for the current contract by storing the
// forked process id in the procs table.
//
// Doesn't add the new process if the depth exceeds the allowed
// depth for this nametag.
//
// Assumes the lock is held
//
void
pmf_contract_depth::fork_event(ct_evthdl_t ev)
{
	pid_t pid, ppid;
	int err;
	os::sc_syslog_msg logger(SC_SYSLOG_PMF_PMFD_TAG, "", NULL);

	//
	// Get the id of the new process and its parent
	//
	if ((err = ct_pr_event_get_pid(ev, &pid)) != 0) {
		//
		// SCMSGS
		// @explanation
		// The rpc.pmfd was unable to retrieve the pid from a contract
		// event. The rpc.pmfd will continue to monitor the process,
		// but the rpc.pmfd may have missed an event of interest.
		// @user_action
		// Search for other syslog error messages on the same node.
		// Save a copy of the /var/adm/messages files on all nodes,
		// and report the problem to your authorized Sun service
		// provider.
		//
		logger.log(LOG_ERR, MESSAGE, "fork_event depth: "
		    "ct_pr_event_get_pid: %s",
		    strerror(err));

		// Nothing we can do to recover the missing info, but
		// we might as well continue monitoring.
		return;
	}

	if ((err = ct_pr_event_get_ppid(ev, &ppid)) != 0) {
		logger.log(LOG_ERR, MESSAGE, "ct_pr_event_get_ppid: %s",
		    strerror(err));

		// Nothing we can do to recover the missing info, but
		// we might as well continue monitoring.
		return;
	}

	//
	// Look up the parent in the process table
	//
	std::map<pid_t, int>::iterator it = procs.find(ppid);
	if (it == procs.end()) {
		//
		// The parent doesn't exist.
		// There are two cases.
		// 1. This one is the first process.
		// Note that the first process gets added
		// in the table as a result of  procs.insert call in
		// start process. Its not necessary to add the same here.
		// 2. This one is a child process of a parent which
		// is not present in the table. The parent process is not
		// present as we stop storing processes after a certain depth.
		//
		debug_print(NOGET("Nametag %s: Not adding pid %d "
		    "because the parent proc doesn't exist in the "
		    "table\n"), nametag.c_str(), pid);
	} else if (it->second >= depth) {
		debug_print(NOGET(
		    "Nametag %s: NOT adding pid %d at depth %d\n"),
		    nametag.c_str(), pid, it->second + 1);
	} else {

		// If we have a signal, kill the new process
		if (current_signal) {
			send_signal(pid, current_signal);
		}

		//
		// Add this entry with a depth of the depth of the parent
		// plus 1.
		//
		debug_print(NOGET("Nametag %s: adding pid %d at depth %d\n"),
		    nametag.c_str(), pid, it->second + 1);
		procs[pid] = it->second + 1;
	}
}
