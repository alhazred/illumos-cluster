/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _PMFD_DISPATCH_H
#define	_PMFD_DISPATCH_H

#pragma ident	"@(#)pmfd_dispatch.h	1.2	08/05/20 SMI"

/*
 * pmfd_monitor.h  pmfd main dispatching routines
 */

#ifdef __cplusplus
extern "C" {
#endif

#include "pmfd.h"
#include "pmfd_event_pool.h"

void pmf_awake_dispatching_thread(void);
void pmf_sig_handler(int signal);
int pmf_ptrace_monitor_startup(void);
int pmf_ptrace_monitor_join(void);
void pmf_send_event_to_GC(struct pmf_event *event);

#ifdef __cplusplus
}
#endif

#endif	/* _PMFD_DISPATCH_H */
