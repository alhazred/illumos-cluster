/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)pmfd_ptrace.c	1.3	08/05/20 SMI"


/*
 * pmfd_ptrace.c - ptrace related routines for the linux pmf daemon
 */

#include <rgm/rpc/pmf.h>
#include "pmfd.h"
#include <sys/ptrace.h>
#include <sys/os_compat.h>
#include <sys/sc_syslog_msg.h>
#include <sys/rsrc_tag.h>

#ifndef PTRACE_SETOPTIONS
/*
 * Glibc is not up to date. But value will be transmitted
 * to kernel.
 */
#define	PTRACE_SETOPTIONS	(enum __ptrace_request)(0x4200)
#endif

#ifndef PTRACE_GETEVENTMSG
/*
 * Glibc is not up to date. But value will be transmitted
 * to kernel.
 */
#define	PTRACE_GETEVENTMSG	(enum __ptrace_request)(0x4201)
#endif

/*
 * #define PTRACE_GETSIGINFO	(enum __ptrace_request)(0x4202)
 * #define PTRACE_SETSIGINFO	(enum __ptrace_request)(0x4203)
 */

/*
 * options set using PTRACE_SETOPTIONS
 * #define PTRACE_O_TRACESYSGOOD   0x00000001
 */

#ifndef PTRACE_O_TRACEFORK
#define	PTRACE_O_TRACEFORK	0x00000002
#endif
#ifndef PTRACE_O_TRACEVFORK
#define	PTRACE_O_TRACEVFORK	0x00000004
#endif
/*
 * #define PTRACE_O_TRACECLONE     0x00000008
 * #define PTRACE_O_TRACEEXEC      0x00000010
 * #define PTRACE_O_TRACEVFORKDONE 0x00000020
 */
#ifndef PTRACE_O_TRACEEXIT
#define	PTRACE_O_TRACEEXIT	0x00000040
#endif

#ifdef PMFD_PTRACE_OPTION_EXIT
/*
 * We trace the fork, vfork and exit system calls
 */
#define	PMFD_PTRACE_OPTIONS PTRACE_O_TRACEFORK | PTRACE_O_TRACEVFORK \
	| PTRACE_O_TRACEEXIT
#else
/*
 * We trace the fork and vfork system calls
 */
#define	PMFD_PTRACE_OPTIONS PTRACE_O_TRACEFORK | PTRACE_O_TRACEVFORK
#endif

extern int debug;

/*
 * Wrappers around ptrace commands
 */
int pmf_ptrace_traceme(struct pmf_handle *h);
int pmf_ptrace_cont(struct pmf_handle *h, int pid, int sig);
int pmf_ptrace_detach(struct pmf_handle *h, int pid);
int pmf_ptrace_attach(struct pmf_handle *h, int pid);
int pmf_ptrace_setoptions(struct pmf_handle *h, int pid);
int pmf_ptrace_geteventmsg(struct pmf_handle *h, int ppid, int *pid);

int
pmf_ptrace_traceme(struct pmf_handle *h)
{
	int err;
	sc_syslog_msg_handle_t sys_handle;

	/*
	 * WARNING: this routine is likelly to be called between fork
	 * and exec. We should then use dbg_msgout_nosyslog in order to
	 * avoid deadlock within syslog.
	 */
	if (debug && h)
		dbg_msgout_nosyslog(NOGET("In forked process "
		    "Monitoring thread %s "
		    "process pid=%d requesting to "
		    "be traced by its parent pid=%d\n"),
		    h->id, getpid(), getppid());

	if (ptrace(PTRACE_TRACEME, 0, (void *) 0, 0)) {
		err = errno;
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE, "ptrace: %s", strerror(err));
		sc_syslog_msg_done(&sys_handle);
		return (-1);
	}
	return (0);
}

int
pmf_ptrace_cont(struct pmf_handle *h, int pid, int sig)
{
	int err;
	sc_syslog_msg_handle_t sys_handle;

	if (debug && h)
		dbg_msgout(NOGET("Monitoring "
		    "thread %s "
		    "restarting stopped child "
		    "%d with PTRACE_CONT sig=%d\n"),
		    h->id, pid, sig);

	if (ptrace(PTRACE_CONT, pid, (void *) 0, sig)) {
		err = errno;
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE, "ptrace: %s", strerror(err));
		sc_syslog_msg_done(&sys_handle);
		return (-1);
	}
	return (0);
}

/*
 * In this version, if ptrace failed on restarting a process, make an other
 * try using kill.
 *
 */
int
pmf_ptrace_cont_on_error_try_kill(struct pmf_handle *h, int pid, int sig)
{
	int err;
	sc_syslog_msg_handle_t sys_handle;

	if (debug && h)
		dbg_msgout(NOGET("Monitoring "
		    "thread %s "
		    "restarting stopped child "
		    "%d with PTRACE_CONT sig=%d\n"),
		    h->id, pid, sig);

	if (ptrace(PTRACE_CONT, pid, (void *) 0, sig)) {
		err = errno;
		if (debug && h)
			dbg_msgout(NOGET("Monitoring "
			    "thread %s restarting stopped child "
			    "%d with PTRACE_CONT sig=%d  failed "
			    "trying kill\n"),
			    h->id, pid, sig);

		/*
		 * If kill is also failing, log the initial failure
		 */
		if (kill(pid, sig)) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_PMF_PMFD_TAG, "");
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE, "ptrace: %s", strerror(err));
			sc_syslog_msg_done(&sys_handle);
			return (-1);
		}
	}
	return (0);
}

int
pmf_ptrace_detach(struct pmf_handle *h, int pid)
{
	int err;
	sc_syslog_msg_handle_t sys_handle;

	if (debug && h)
		dbg_msgout(NOGET("Monitoring thread %s "
		    "detaching stopped child %d "
		    "with PTRACE_DETACH\n"),
		    h->id, pid);

	if (ptrace(PTRACE_DETACH, pid, (void *) 0, 0)) {
		err = errno;
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE, "ptrace: %s", strerror(err));
		sc_syslog_msg_done(&sys_handle);
		return (-1);
	}
	return (0);
}

int
pmf_ptrace_attach(struct pmf_handle *h, int pid)
{
	int err;
	sc_syslog_msg_handle_t sys_handle;

	if (debug && h)
		dbg_msgout(NOGET("Monitoring thread %s "
		    "attaching to process %d "
		    "with PTRACE_ATTACH\n"),
		    h->id, pid);

	if (ptrace(PTRACE_ATTACH, pid, (void *) 0, PMFD_PTRACE_OPTIONS)) {
		err = errno;
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE, "ptrace: %s", strerror(err));
		sc_syslog_msg_done(&sys_handle);
		return (-1);
	}
	return (0);
}

int
pmf_ptrace_setoptions(struct pmf_handle *h, int pid)
{
	int err;
	sc_syslog_msg_handle_t sys_handle;

	if (debug && h)
		dbg_msgout(NOGET("Monitoring thread %s "
		    "setting ptrace options %x for "
		    "head process %d\n"),
		    h->id, PMFD_PTRACE_OPTIONS, pid);

	if (ptrace(PTRACE_SETOPTIONS, pid, (void *) 0, PMFD_PTRACE_OPTIONS)) {
		err = errno;
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE, "ptrace: %s", strerror(err));
		sc_syslog_msg_done(&sys_handle);
		return (-1);
	}
	return (0);
}

int
pmf_ptrace_geteventmsg(struct pmf_handle *h, int ppid, int *pid)
{
	int err;
	sc_syslog_msg_handle_t sys_handle;

	if (!pid)
		return (EINVAL);

	if (ptrace(PTRACE_GETEVENTMSG, ppid, (void *) 0, pid)) {
		err = errno;
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE, "ptrace: %s", strerror(err));
		sc_syslog_msg_done(&sys_handle);
		return (-1);
	}

	if (debug && h)
		dbg_msgout(NOGET("Monitoring thread %s "
		    "process ppid=%d forked a new child with pid=%d\n"),
		    h->id, ppid, *pid);
	return (0);
}
