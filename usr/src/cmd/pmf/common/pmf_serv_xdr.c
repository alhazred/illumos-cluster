/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)pmf_serv_xdr.c	1.5	08/05/20 SMI"

/*
 * File: pmf.x  RPC interface to HA Process Monitor Facility
 */


#include <rgm/pmf.h>
#include <rgm/door/pmf_door.h>
#include <rgm/security.h>
#include <stdlib.h>
#include <sys/sc_syslog_msg.h>
#include <sys/rsrc_tag.h>
#include <sys/cl_assert.h>


void
pmf_result_args_xdr_free(int api_name, void *result)
{
	switch (api_name) {
		case PMFPROC_NULL:
			break;
		case PMFPROC_START:
		case PMFPROC_STOP:
		case PMFPROC_KILL:
		case PMFPROC_SUSPEND:
		case PMFPROC_RESUME:
		case PMFPROC_MODIFY:
			xdr_free((xdrproc_t)xdr_pmf_result,
			    (char *)result);
			break;
		case PMFPROC_STATUS:
			xdr_free((xdrproc_t)xdr_pmf_status_result,
			    (char *)result);
			break;
		default:
			CL_PANIC(0);
			break;
	}
}

void
pmf_input_args_xdr_free(pmf_input_args_t *pmf_arg)
{
	sc_syslog_msg_handle_t handle;

	switch (pmf_arg->api_name) {
		case PMFPROC_NULL:
			break;
		case PMFPROC_START:
			xdr_free((xdrproc_t)xdr_pmf_start_args,
			    (char *)pmf_arg->data);
			break;
		case PMFPROC_STATUS:
		case PMFPROC_STOP:
		case PMFPROC_KILL:
		case PMFPROC_SUSPEND:
		case PMFPROC_RESUME:
			xdr_free((xdrproc_t)xdr_pmf_args,
			    (char *)pmf_arg->data);
			break;
		case PMFPROC_MODIFY:
			xdr_free((xdrproc_t)xdr_pmf_modify_args,
			    (char *)pmf_arg->data);
			break;
		default:
			CL_PANIC(0);
			break;
	}
	free(pmf_arg->data);
}

bool_t
xdr_pmf_input_args(register XDR *xdrs, pmf_input_args_t *objp)
{
	sc_syslog_msg_handle_t handle;

	if (!xdr_int(xdrs, &objp->api_name)) {
		return (FALSE);
	}
	switch (objp->api_name) {
		case PMFPROC_NULL:
			break;
		case PMFPROC_START:
			objp->data = (pmf_start_args *)
			    calloc(1, sizeof (pmf_start_args));

			if (!xdr_pmf_start_args(xdrs,
			    (pmf_start_args *)objp->data)) {
				return (FALSE);
			}
			break;
		case PMFPROC_STATUS:
		case PMFPROC_STOP:
		case PMFPROC_KILL:
		case PMFPROC_SUSPEND:
		case PMFPROC_RESUME:
			objp->data = (pmf_args *)
			    calloc(1, sizeof (pmf_args));
			if (!xdr_pmf_args(xdrs, (pmf_args *)objp->data)) {
				return (FALSE);
			}
			break;
		case PMFPROC_MODIFY:
			objp->data = (pmf_modify_args *)
			    calloc(1, sizeof (pmf_modify_args));
			if (!xdr_pmf_modify_args(xdrs,
			    (pmf_modify_args *)objp->data)) {
				return (FALSE);
			}
			break;
		default:
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_PMF_PMFD_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * Need explanation of this message!
			 * @user_action
			 * Need a user action for this message.
			 */
			(void) sc_syslog_msg_log(handle,
			    LOG_ERR, MESSAGE, "Invalid API code %d",
			    objp->api_name);
			sc_syslog_msg_done(&handle);
			return (FALSE);
	}
	return (TRUE);
}

bool_t
xdr_pmf_result_args(XDR *xdrs, void *objp, int api_name)
{
	sc_syslog_msg_handle_t handle;

	switch (api_name) {
		case PMFPROC_NULL:
			break;
		case PMFPROC_START:
		case PMFPROC_STOP:
		case PMFPROC_KILL:
		case PMFPROC_SUSPEND:
		case PMFPROC_RESUME:
		case PMFPROC_MODIFY:
			if (!xdr_pmf_result(xdrs, (pmf_result *)objp)) {
				return (FALSE);
			}
			break;
		case PMFPROC_STATUS:
			if (!xdr_pmf_status_result(xdrs,
			    (pmf_status_result *)objp)) {
				return (FALSE);
			}
			break;
		default:
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_PMF_PMFD_TAG, "");
			(void) sc_syslog_msg_log(handle,
			    LOG_ERR, MESSAGE, "Invalid API code %d",
			    api_name);
			sc_syslog_msg_done(&handle);
			return (FALSE);
	}
	return (TRUE);
}

/*
 * How we calculate size for xdr encoding:
 * ---------------------------------------
 * The Size calculation that we do here has to calculate
 * the sizeof the structure members and also the sizeof
 * the data that is pointed to be the pointers if any
 * int he structure.
 * For eg:
 * struct abc {
 *	int a;
 *	char *ch;
 * }
 * in this case we will find the size of the struct abc
 * which will give us the size needed for storing the
 * elements that is an int and a char pointer. And also
 * we will need to add the sizeof the array pointed to
 * by ch.
 * In addition to this, xdr encodes string in the following
 * way:
 * [integer(for size of the string) + string]
 * So, for any string say char *ch = {"example"}
 * The size needed will be :
 * sizeof (ch) + sizeof (int) + strlen(ch)
 * Here the sizeof (int) is taken for storing the sizeof
 * the string.
 */


size_t
pmf_xdr_sizeof_result(void *data, int api)
{
	sc_syslog_msg_handle_t handle;

	size_t size = 0;
	uint_t i = 0;
	size = RNDUP(sizeof (int));
	switch (api) {
		case PMFPROC_NULL:
			break;
		case PMFPROC_STATUS:
			size += RNDUP(sizeof (pmf_status_result)) +
			    RNDUP(safe_strlen(((pmf_status_result *)
			    (data))->identifier)) +
			    RNDUP(safe_strlen(((pmf_status_result *)
			    (data))->action)) +
			    RNDUP(safe_strlen(((pmf_status_result *)
			    (data))->pids)) +
			    RNDUP(safe_strlen(((pmf_status_result *)
			    (data))->upids)) +
			    RNDUP(safe_strlen(((pmf_status_result *)
			    (data))->project_name)) +
			    (5 * RNDUP(sizeof (int)));

			if (((pmf_status_result *)(data))->cmd.cmd_len > 0) {
				for (i = 0; i < ((pmf_status_result *)(data))->
				    cmd.cmd_len; i++) {
					size += RNDUP(safe_strlen(
					    ((pmf_status_result *)
					    (data))->cmd.cmd_val[i]));
					size += RNDUP(sizeof (int));
				}
			}
			if (((pmf_status_result *)(data))->env.env_len > 0) {
				for (i = 0; i < ((pmf_status_result *)(data))->
				    env.env_len; i++) {
					size += RNDUP(safe_strlen(
					    ((pmf_status_result *)
					    (data))->env.env_val[i]));
					size += RNDUP(sizeof (int));
				}
			}
			break;
		case PMFPROC_START:
		case PMFPROC_STOP:
		case PMFPROC_KILL:
		case PMFPROC_SUSPEND:
		case PMFPROC_RESUME:
		case PMFPROC_MODIFY:
			size += RNDUP(sizeof (pmf_result));
			break;
		default:
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_PMF_PMFD_TAG, "");
			(void) sc_syslog_msg_log(handle,
			    LOG_ERR, MESSAGE, "Invalid API code %d",
			    api);
			sc_syslog_msg_done(&handle);
			return (FALSE);
	}
	return (size);
}
