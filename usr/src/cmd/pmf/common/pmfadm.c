/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)pmfadm.c	1.71	08/05/20 SMI"

/*
 * pmfadm.c - file for pmfadm(1) command
 *
 *	RPC client interface to HA Process Monitor Facility
 */

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <strings.h>
#include <unistd.h>
#include <errno.h>
#include <tzfile.h>
#include <rpc/rpc.h>
#include <pwd.h>
#include <locale.h>
#include <sys/sol_version.h>
#include <rgm/security.h>

#if DOOR_IMPL
#include <rgm/pmf.h>
#else
#include <rgm/rpc/pmf.h>
#endif

#ifdef SC_SRM
#include <project.h>
#endif

#include "pmfd_proto.h"

/* for tracking of strings that are not translated */
#define	NOGET(s)	s

/*
 * Extra options for command.  Just to have a
 * tidy place to put them.
 */
struct pmf_opts {
	int type;
	char *id;
	int signal;
	int timewait;
	int cmd_index;
};
typedef struct pmf_opts pmf_opts;

static char *progname;
static int debug = 0;
static int env_specified = 0;

/*
 * Command line options.
 * See Usage below.
 */
#define	PMF_OPTARGS "h:c:l:k:n:t:a:s:w:q:m:e:C:ELd"
#define	PMF_CTL_OPTARGS "S:R:"

static void usage(char *prog);
static int parse_options(int argc, char **argv, char **env, pmf_cmd *pmfcmd,
	pmf_opts *opts);
static void pmf_print_status(pmf_status_result *status, char *host);
static void pmf_print_status_all(pmf_status_result *status);

#ifdef linux
#include <ctype.h>
struct name_value_table {
	char *shortname;
	int sig;
};

static struct name_value_table sig_by_name[] = {
	{ "HUP", SIGHUP },
	{ "INT", SIGINT },
	{ "QUIT", SIGQUIT },
	{ "ILL", SIGILL },
	{ "TRAP", SIGTRAP },
	{ "ABRT", SIGABRT },
	{ "IOT", SIGIOT },
	{ "BUS", SIGBUS },
	{ "FPE", SIGFPE },
	{ "KILL", SIGKILL },
	{ "USR1", SIGUSR1 },
	{ "SEGV", SIGSEGV },
	{ "USR2", SIGUSR2 },
	{ "PIPE", SIGPIPE },
	{ "ALRM", SIGALRM },
	{ "TERM", SIGTERM },
	{ "STKFLT", SIGSTKFLT },
	{ "CLD", SIGCLD },
	{ "CHLD", SIGCHLD },
	{ "CONT", SIGCONT },
	{ "STOP", SIGSTOP },
	{ "TSTP", SIGTSTP },
	{ "TTIN", SIGTTIN },
	{ "TTOU", SIGTTOU },
	{ "URG", SIGURG },
	{ "XCPU", SIGXCPU },
	{ "XFSZ", SIGXFSZ },
	{ "VTALRM", SIGVTALRM },
	{ "PROF", SIGPROF },
	{ "WINCH", SIGWINCH },
	{ "POLL", SIGPOLL },
	{ "IO", SIGIO },
	{ "PWR", SIGPWR },
	{ "SYS", SIGSYS },
	{ "UNUSED", SIGUNUSED }
};

static int str2sig(const char *str, int *signum) {

	char *garbage;
	int i;

	*signum = 0;

	if (isdigit(*str)) {
		i = strtol(str, &garbage, 10);
		if (*garbage == 0 && i >= 0 && i < NSIG) {
			*signum = i;
			return (0);
		}
	} else {
		for (i = 0;
		    i < sizeof (sig_by_name)/sizeof (struct name_value_table);
		    i++) {
			if (strcmp(sig_by_name[i].shortname, str) == 0) {
				*signum = sig_by_name[i].sig;
				return (0);
			}
		}

		if (strcmp(str, "RTMIN") == 0) {
			*signum = SIGRTMIN;
			return (0);
		}
		if (strcmp(str, "RTMIN+1") == 0) {
			*signum = SIGRTMIN + 1;
			return (0);
		}
		if (strcmp(str, "RTMIN+2") == 0) {
			*signum = SIGRTMIN + 2;
			return (0);
		}
		if (strcmp(str, "RTMIN+3") == 0) {
			*signum = SIGRTMIN + 3;
			return (0);
		}
		if (strcmp(str, "RTMAX-3") == 0) {
			*signum = SIGRTMAX - 3;
			return (0);
		}
		if (strcmp(str, "RTMAX-2") == 0) {
			*signum = SIGRTMAX - 2;
			return (0);
		}
		if (strcmp(str, "RTMAX-1") == 0) {
			*signum = SIGRTMAX - 1;
			return (0);
		}
		if (strcmp(str, "RTMAX") == 0) {
			*signum = SIGRTMAX;
			return (0);
		}

	}

	return (-1);
}
#endif /* linux */

/*
 * Usage:
 *		pmfadm -c <name> [-a <action>] [-n <retries>] [-t <period>]
 * 			[[-e <env var>]... | E] [-C <level>]
 *			<cmd...> [args...]
 *		pmfadm -m <name> [-n <retries>] [-t <period>]
 *		pmfadm -s <name> [-w <timeout>] [<signal>]
 *		pmfadm -k <name> [-w <timeout>] [<signal>]
 *		pmfadm -l <name> [-h <host>]
 *		pmfadm -L [-h <host>]
 *		pmfadm -q <name> [-h <host>]
 *		pmfctl -S <pid_to_suspend>	(***UNDOCUMENTED***)
 *		pmfctl -R <pid_to_resume>	(***UNDOCUMENTED***)
 */
static void
usage(char *prog)
{
	(void) fprintf(stderr,
	    gettext("Usage: %s\n"), prog);
	if (strcmp(prog, "pmfctl") == 0) {
		(void) fprintf(stderr,
		    gettext("\tpmfctl -S <pid>\n"));
		(void) fprintf(stderr,
		    gettext("\tpmfctl -R <pid>\n"));
		return;
	}
	(void) fprintf(stderr,
	    gettext("\tpmfadm -c <name> [-a <action>] [-n <retries>] "
	    "[-t <period>]\n\t\t[[-e <env var>]... | E] [-C <level>] "
	    "<cmd...> [args...]\n"));
	(void) fprintf(stderr,
	    gettext("\tpmfadm -m <name> [-n <retries>] [-t <period>]\n"));
	(void) fprintf(stderr,
	    gettext("\tpmfadm -s <name> [-w <timeout>] [<signal>]\n"));
	(void) fprintf(stderr,
	    gettext("\tpmfadm -k <name> [-w <timeout>] [<signal>]\n"));
	(void) fprintf(stderr,
	    gettext("\tpmfadm -l <name> [-h <host>]\n"));
	(void) fprintf(stderr,
	    gettext("\tpmfadm -L [-h <host>]\n"));
	(void) fprintf(stderr,
	    gettext("\tpmfadm -q <name> [-h <host>]\n"));
	(void) fprintf(stderr,
	    gettext("\n\tWhere:\n"));
	(void) fprintf(stderr,
	    gettext("\t\t<name>\t\t== Identifier for process\n"));
	(void) fprintf(stderr,
	    gettext("\t\t<retries>\t== Number of retries allowed (def: 0)\n"));
	(void) fprintf(stderr,
	    gettext("\t\t<period>\t== Mins over which to count retries "
	    "(def: INF)\n"));
	(void) fprintf(stderr,
	    gettext("\t\t<env var>\t== One environmental variable "
	    "in format \"ENV = env\"\n"));
	(void) fprintf(stderr, gettext("\t\t<timeout>\t== "
	    "Number seconds to wait for completion\n"));
	(void) fprintf(stderr,
	    gettext("\t\t<host>\t\t== Host (def: localhost)\n"));
	(void) fprintf(stderr,
	    gettext("\t\t<level>\t\t== Level of child processes to monitor\n"));
	(void) fprintf(stderr,
	    gettext("\t\t<action>\t== <scriptname> (def: none)\n"));
	(void) fprintf(stderr,
	    gettext("\t\t<cmd>\t\t== Process to start and monitor\n"));
}


int
main(int argc, char *argv[], char *envp[])
{
	pmf_cmd pmfcmd = {0};
	pmf_opts pmfopts = {0};
	pmf_result result = {{0}};
	pmf_status_result status = {{0}};
	int err = 0;
	char	*err_str = NULL;

	if ((progname = strrchr(argv[0], '/')) == NULL)
		progname = argv[0];
	else
		progname++;

	(void) setlocale(LC_ALL, "");
	(void) textdomain(TEXT_DOMAIN);
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

#if SOL_VERSION >= __s10
	if (strcmp(progname, "pmfctl") == 0) {
		(void) fprintf(stderr,
		    gettext("pmfctl is no longer supported\n"));
		return (-1);
	}
#endif

	if (parse_options(argc, argv, envp, &pmfcmd, &pmfopts) == -1) {
		usage(progname);
		return (-1);
	}

	/*
	 * Suppress lint warning: "undeclared identifer" (PMF_SECURITY_DEF)
	 * PMF_SECURITY_DEF is defined (-D flag) in cmd/pmf/Makefile.com
	 */
	pmfcmd.security = PMF_SECURITY_DEF; /*lint !e40 */

	switch (pmfopts.type) {
		case PMF_START:
			argc -= pmfopts.cmd_index;
			argv += pmfopts.cmd_index;
			err = pmf_start(pmfopts.id, &pmfcmd, argc, argv,
			    &result);

			if (env_specified && pmfcmd.env)
				free(pmfcmd.env);

			break;
		case PMF_STAT:
			err = pmf_status(pmfopts.id, &pmfcmd, &status);
			result.code = status.code;
			break;
		case PMF_STATUS:
			err = pmf_status(pmfopts.id, &pmfcmd, &status);
			result.code = status.code;
			break;
		case PMF_STATUS_ALL:
			err = pmf_status_all(&pmfcmd, &status);
			result.code = status.code;
			break;
		case PMF_STOP:
			pmfcmd.timewait = pmfopts.timewait;
			pmfcmd.signal = pmfopts.signal;
			err = pmf_stop(pmfopts.id, &pmfcmd, &result);
			break;
		case PMF_KILL:
			pmfcmd.timewait = pmfopts.timewait;
			pmfcmd.signal = pmfopts.signal;
			err = pmf_kill(pmfopts.id, &pmfcmd, &result);
			break;
		case PMF_MODIFY:
			err = pmf_modify(pmfopts.id, &pmfcmd, &result);
			break;
		case PMF_SUSPEND:
			err = pmf_suspend(pmfopts.id, &pmfcmd, &result);
			break;
		case PMF_RESUME:
			err = pmf_resume(pmfopts.id, &pmfcmd, &result);
			break;
		default:
			return (-1);
	}

	if (err == -1)
		return (err);

	if (debug)
		(void) printf(NOGET("err=%d result.code.type=%d\n"),
		    err, result.code.type);

	switch (result.code.type) {
		case PMF_OKAY:
			break;
		case PMF_SYSERRNO:
			if (pmfopts.type != PMF_STAT) {
				if (strerror(result.code.sys_errno)) {
					if (result.code.sys_errno == ENOENT) {
						(void) fprintf(stderr, gettext(
						    "%s: \"%s\" No such "
						    "<nametag> "
						    "registered\n"),
						    progname, pmfopts.id);
					} else if (result.code.sys_errno ==
					    E2BIG) {
						(void) fprintf(stderr, gettext(
						    "%s: Request failed: The "
						    "number of retries "
						    "requested %d is greater "
						    "than the maximum number "
						    "of retries "
						    "allowed %d.\n"
						    "You might want to "
						    "set an infinite number "
						    "of retries using the "
						    "-n -1 option\n"),
						    progname, pmfcmd.retries,
						    PMFD_MAX_HISTORY);
					} else {
						(void) fprintf(stderr,
						    "%s: \"%s\" %s\n",
						    progname, pmfopts.id,
						    strerror(
							result.code.sys_errno));
					}
				} else {
					(void) fprintf(stderr,
					    gettext("%s: \"%s\" Errno: %d\n"),
					    progname, pmfopts.id,
					    result.code.sys_errno);
				}
			}

			switch (result.code.sys_errno) {
				case ENOENT:
					return (1);
				case ETIME:
					return (2);
				default:
					return (-1);
			}
		case PMF_ACCESS:
			(void) fprintf(stderr, gettext(
			    "%s: Authorization error.\n"), progname);
			err_str = security_get_errormsg(result.code.sec_errno);
			if (err_str) {
				(void) fprintf(stderr, "%s: %s\n",
				    progname, err_str);
				free(err_str);
			}
			return (-1);
		case PMF_CONNECT:
			(void) fprintf(stderr, gettext(
			    "%s: Connection error.\n"), progname);
			if (result.code.sys_errno == RPC_PROGNOTREGISTERED)
				(void) fprintf(stderr, gettext(
				    "Program not registered or server "
				    "not started\n."));
			return (-1);
		case PMF_DUP:
			(void) fprintf(stderr,
			    gettext("%s: Request \"%s\" already queued\n"),
			    progname, pmfopts.id);
			return (1);
		default:
			(void) fprintf(stderr,
			    gettext("%s: Unknown error (%d/%d)\n"),
				progname, result.code.type,
				result.code.sys_errno);
			return (-1);
	}

	/*
	 * Print data received from server for status command
	 * and free args malloced by the status command.
	 */
	switch (pmfopts.type) {
	case PMF_STAT:
		free_rpc_args(&status);
		break;
	case PMF_STATUS:
		pmf_print_status(&status, pmfcmd.host);
		free_rpc_args(&status);
		break;
	case PMF_STATUS_ALL:
		pmf_print_status_all(&status);
		free_rpc_args(&status);
		break;
	default:
		break;
	}

	return (0);
}

static void
pmf_print_status(pmf_status_result *status, char *host)
{
	struct passwd pwd, *pwdp;
	pmf_status_result new_status;
	pmf_cmd cmd = {0};
	char buf[BUFSIZ];
	char *lasts;
	char *ptr;
	uint_t i;

	if (status->identifier) {
		/*
		 * Following is an undocumented feature.  Passing
		 * a null name to the status command causes pmfadm
		 * to traverse all existing tags and print status
		 * for each.
		 */
		if (strchr(status->identifier, ' ') != NULL) {

			ptr = (char *)strtok_r(status->identifier,
			    " ", &lasts);
			do {
				(void) printf(gettext("STATUS %s\n"), ptr);
				cmd.host = host;
				if (pmf_status(ptr, &cmd, &new_status))
					continue;
				pmf_print_status(&new_status, host);
			} while ((ptr = (char *)strtok_r(NULL, " ", &lasts))
			    != NULL);

		} else if (*status->identifier) {

			(void) printf(NOGET("pmfadm -c %s "),
			    status->identifier);
			if (status->retries) {
				if (status->retries == -1)
					(void) printf("-n -1 ");
				else
					(void) printf(NOGET("-n %u "),
					    status->retries);
			}

			if (status->period != -1)
				(void) printf(NOGET("-t %u "),
				    status->period / SECSPERMIN);

			if (status->action && *status->action)
				(void) printf(NOGET("-a %s "),
				    status->action);

			for (i = 0; i < status->cmd.cmd_len; i++) {
				if (strchr(status->cmd.cmd_val[i], ' '))
					(void) printf("\'%s\' ",
					    status->cmd.cmd_val[i]);
				else
					(void) printf("%s ",
					    status->cmd.cmd_val[i]);
			}
			(void) printf("\n");

			if ((status->env.env_len > 0) &&
			(status->env.env_val != NULL)) {
				(void) printf(gettext("\tenvironment:\n"));
				for (i = 0; i < status->env.env_len; i++) {
					(void) printf("\t\t%s\n",
					    status->env.env_val[i]);
				}
			}

			(void) printf(gettext("\tretries: %d\n"),
			    status->nretries);
			if (getpwuid_r(status->owner, &pwd, buf, BUFSIZ,
			    &pwdp) != 0) {
				(void) printf(gettext("\towner: %d\n"),
				    (int)status->owner);
			} else {
				(void) printf(gettext("\towner: %s\n"),
				    pwd.pw_name);
			}

			switch (status->monitor_children) {
			case PMF_ALL:
				(void) printf(gettext(
				    "\tmonitor children: all\n"));

				break;
			case PMF_LEVEL:
				(void) printf(gettext(
				    "\tmonitor children: up to level %d\n"),
				    status->monitor_level);

				break;
			default:
				break;
			}

			if (status->pids)
				(void) printf(gettext("\tpids: %s\n"),
				    status->pids);
			else
				(void) printf(gettext("\tpids:\n"));

			/*
			 * We only print the unmonitored pids line if there are
			 * any unmonitored pids present.
			 */
			if ((status->upids) && (status->upids[0] != '\0'))
				(void) printf(gettext(
					"\tunmonitored pids: %s\n"),
					status->upids);
		}
	}
}

static void
pmf_print_status_all(pmf_status_result *status)
{
	if (status->identifier) {
		(void) printf(gettext("\ttags: %s\n"), status->identifier);
	}
}


/*
 * parse_options:
 *
 *	Parse the input options.  We need to keep track
 *	of the optind index so that we can retrieve the
 *	requested command from argv.  Rather than propagate
 *	the usage of optind (getopt is only used here), we
 *	save it in cmd_index in the pmf_cmd structure.
 */
static int
parse_options(int argc, char *argv[], char *env[],
    pmf_cmd *pmfcmd, pmf_opts *opts)
{

	extern char *optarg;
	extern int optind;
	int c, i;
	int retries_specified = 0;
	int period_specified = 0;
	int action_specified = 0;
	int signal_specified = 0;
	int Env_specified = 0;
	int monitor_children_specified = 0;
	char *pmf_optargs;

#ifdef SC_SRM
	projid_t	projid;
	struct project	proj;
	char		buf[PROJECT_BUFSZ];

	/*
	 * Get the project name of the caller
	 */
	projid = getprojid();
	if (projid == -1 ||
	    getprojbyid(projid, &proj, buf, (size_t)PROJECT_BUFSZ) == NULL) {
		(void) fprintf(stderr, gettext(
		    "%s: Cannot get project ID\n"), progname);
		return (-1);
	}

	pmfcmd->project_name = strdup(proj.pj_name);
	if (pmfcmd->project_name == NULL) {
		(void) fprintf(stderr, gettext("%s: No memory\n"), progname);
		return (-1);
	}
#endif

	if (strcmp(progname, "pmfctl") == 0)
		pmf_optargs = PMF_CTL_OPTARGS;
	else
		pmf_optargs = PMF_OPTARGS;

	/*
	 * Set defaults for the following:
	 *
	 *	period == -1	-- infinite
	 *	retries == 0	-- none
	 *	timewait == 0	-- don't wait
	 */

	pmfcmd->period		= -1;
	pmfcmd->retries	= 0;
	pmfcmd->timewait	= 0;
	pmfcmd->monitor_children = PMF_ALL;
	pmfcmd->num_env = 0;

	/*
	 * initialize host to NULL
	 */
	pmfcmd->host = NULL;

	while ((c = getopt(argc, argv, pmf_optargs)) != EOF) {
		switch (c) {
			/*
			 * unadvertised option done for @home
			 * to turn off messages
			 * about processes not staying alive
			 */
			case 'z':
				PMF_SET_EXIT_QUIETLY(pmfcmd->flags);
				break;
			case 'h':
				pmfcmd->host = optarg;
				break;
			case 'c':
				if (opts->type != PMF_NULL) {
					return (-1);
				}
				opts->type = PMF_START;
				opts->id = optarg;
				break;
			case 'l':
				if (opts->type != PMF_NULL) {
					return (-1);
				}
				opts->type = PMF_STATUS;
				opts->id = optarg;
				break;
			case 'L':
				if (opts->type != PMF_NULL) {
					return (-1);
				}
				opts->type = PMF_STATUS_ALL;
				break;
			case 'q':
				if (opts->type != PMF_NULL) {
					return (-1);
				}
				opts->type = PMF_STAT;
				opts->id = optarg;
				break;
			case 'm':
				if (opts->type != PMF_NULL) {
					return (-1);
				}
				opts->type = PMF_MODIFY;
				opts->id = optarg;
				break;
			case 's':
				if (opts->type != PMF_NULL) {
					return (-1);
				}
				opts->type = PMF_STOP;
				opts->id = optarg;
				break;
			case 'k':
				if (opts->type != PMF_NULL) {
					return (-1);
				}
				opts->type = PMF_KILL;
				opts->id = optarg;
				signal_specified++;
				break;
			case 'n':
				pmfcmd->retries = atoi(optarg);
				pmfcmd->retries_modified = 1;
				retries_specified++;
				break;
			case 't':
				pmfcmd->period = atoi(optarg);
				pmfcmd->period_modified = 1;
				period_specified++;
				break;
			case 'w':
				opts->timewait = atoi(optarg);
				break;
			case 'a':
				/*
				 * Execute a program when action
				 * must be taken.
				 */
				pmfcmd->action = optarg;
				action_specified++;
				break;
			case 'e':
				env_specified++;
				pmfcmd->env_passed = TRUE;
				pmfcmd->env = (char **)realloc(
					(void *)pmfcmd->env,
					(uint_t)(pmfcmd->num_env + 1)
					* sizeof (char *));
				pmfcmd->env[pmfcmd->num_env] = optarg;
				pmfcmd->num_env++;
				break;
			case 'E':
				Env_specified++;
				pmfcmd->env_passed = TRUE;
				/* count size of env */
				for (i = 0; env[i] != NULL; i++) {
					/* for loop body intentionally empty */
				}
				pmfcmd->num_env = i + 1;
				pmfcmd->env = env;
				break;
			case 'C':
				monitor_children_specified++;
				pmfcmd->monitor_children = PMF_LEVEL;
				pmfcmd->monitor_level = atoi(optarg);
				break;
			case 'S':
				/*
				 * Undocumented -S <PID> tells pmfd to
				 * relinquish control of that process.
				 * PID is stored in the id field.
				 * This command can only be used with pmfctl
				 */
				if (opts->type != PMF_NULL) {
					return (-1);
				}
				opts->type = PMF_SUSPEND;
				opts->id = optarg;
				break;
			case 'R':
				/*
				 * Undocumented -R <PID> tells pmfd to
				 * regain control of that process.
				 * PID is stored in the id field.
				 * This comamd can only be used with pmfctl
				 */
				if (opts->type != PMF_NULL) {
					return (-1);
				}
				opts->type = PMF_RESUME;
				opts->id = optarg;
				break;
			/*
			 * unadvertised debug option
			 */
			case 'd':
				debug = 1;
				break;
			default:
				return (-1);
		}
	}

	if (opts->type == PMF_START) {

		/*
		 * The user needs to specify a command to monitor.
		 */
		if ((opts->cmd_index = optind) == argc) {
			(void) fprintf(stderr,
			    gettext("%s: Missing command argument\n"),
			    progname);
			return (-1);
		}

		/*
		 * Make sure we have a valid name (not 0-length).
		 */
		if (opts->id[0] == '\0') {
			(void) fprintf(stderr,
			    gettext("%s: Nametag must have at least one "
			    "character\n"), progname);
			return (-1);
		}

		/*
		 * Make sure we have a valid action (not 0-length).
		 *
		 */
		if (action_specified && pmfcmd->action[0] == '\0') {
			(void) fprintf(stderr,
			    gettext("%s: action command must have at least "
			    "one character\n"), progname);
			return (-1);
		}

		/*
		 * We don't accept repeat options.
		 */
		if (retries_specified > 1) {
			(void) fprintf(stderr,
			    gettext("%s: Repeat argument: retries (-n)\n"),
			    progname);
			return (-1);
		}
		if (period_specified  > 1) {
			(void) fprintf(stderr,
			    gettext("%s: Repeat argument: time period (-t)\n"),
			    progname);
			return (-1);
		}
		if (action_specified  > 1) {
			(void) fprintf(stderr,
			    gettext("%s: Repeat argument: action (-a)\n"),
			    progname);
			return (-1);
		}
		if (Env_specified  > 1) {
			(void) fprintf(stderr,
			    gettext("%s: Repeat argument: environment (-E)\n"),
			    progname);
			return (-1);
		}
		if (monitor_children_specified  > 1) {
			(void) fprintf(stderr, gettext(
			    "%s: Repeat argument: monitor children (-C)\n"),
			    progname);
			return (-1);
		}
		if (signal_specified > 1) {
			(void) fprintf(stderr,
			    gettext("%s: Repeat argument: signal (-k)\n"),
			    progname);
			return (-1);
		}
		if (Env_specified && env_specified) {
			(void) fprintf(stderr,
			    gettext(
			    "%s: Double argument: environment (-e and -E)\n"),
			    progname);
			return (-1);
		}

		/*
		 * Check that the level entered for -C option is not negative
		 */
		if (monitor_children_specified &&
		    pmfcmd->monitor_level < 0) {
			(void) fprintf(stderr,
			    gettext(
			    "%s: The number of levels of children monitored"
			    " has to be equal to or larger than zero.\n"),
			    progname);
			return (-1);
		}

		/*
		 * if env was entered, and it doesn't end in NULL,
		 * add NULL at the end
		 */
		if ((env_specified  > 0) &&
		(pmfcmd->env[pmfcmd->num_env - 1] != NULL)) {
			pmfcmd->env = (char **)realloc(
				(void *)pmfcmd->env,
				(uint_t)(pmfcmd->num_env + 1)
				* sizeof (char *));
			pmfcmd->num_env++;
			/* actually write the NULL into the last spot */
			pmfcmd->env[pmfcmd->num_env - 1] = NULL;
		}

		/*
		 * if -d (debug) was entered, print env var
		 */
		if (debug &&
		(pmfcmd->num_env > 0) && (pmfcmd->env != NULL)) {
			(void) printf(NOGET("\tenvironment:\n"));
			for (i = 0; i < pmfcmd->num_env; i++) {
				if (pmfcmd->env[i])
					(void) printf("\t\t%d\t%s\n",
					    i, pmfcmd->env[i]);
			}
		}

	} else if (opts->type == PMF_MODIFY) {

		/*
		 * We can currently only modify the number of retries and/or
		 * the time period to monitor over.
		 */
		if (optind != argc) {
			(void) fprintf(stderr, gettext(
			    "%s: Cannot modify command or command arguments\n"),
			    progname);
			return (-1);
		}
		if (action_specified  >= 1) {
			(void) fprintf(stderr, gettext(
			    "%s: Cannot modify action (-a) argument\n"),
			    progname);
			return (-1);
		}

	} else if (opts->type == PMF_KILL || opts->type == PMF_STOP) {

		/*
		 * Check if the user specified a signal to send.
		 * Otherwise, default for kill is SIGKILL.
		 */
		if (optind != argc) {
			if (str2sig(argv[optind], &opts->signal) == -1) {
				(void) fprintf(stderr,
				    gettext("%s: Unknown signal (%s)\n"),
				    progname, optarg);
				return (-1);
			}
		} else {
			if (opts->type == PMF_KILL)
				(void) str2sig("KILL", &opts->signal);
			else
				opts->signal = 0;
		}

	} else if (opts->type == PMF_NULL) {
		return (-1);
	} else {

		/*
		 * Likewise, no add/start/extra options are valid for
		 * any other subcommand.
		 */
		if (retries_specified != 0 ||
		    period_specified  != 0 ||
		    env_specified  != 0 ||
		    Env_specified  != 0 ||
		    monitor_children_specified != 0 ||
		    action_specified  != 0) {
			return (-1);
		}

		if (optind < argc) {
			(void) fprintf(stderr,
			    gettext("%s: Too many command line arguments\n"),
			    progname);
			return (-1);
		}
	}

	/*
	 * we allow the -h hostname option only for -l or -q options
	 */
	if ((opts->type != PMF_STATUS) && (opts->type != PMF_STAT) &&
	(opts->type != PMF_STATUS_ALL)) {
		if (pmfcmd->host != NULL) {
			(void) fprintf(stderr, gettext(
			"%s: Command line argument -h can only be used "
			"with -l or -L or -q options\n"), progname);

			return (-1);
		}
	}

	return (0);
}
