/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _PMFD_CONTRACTS_H
#define	_PMFD_CONTRACTS_H

#pragma ident	"@(#)pmfd_contracts.h	1.7	08/07/23 SMI"

#ifdef __cplusplus
extern "C" {
#endif

/*
 * pmfd_contracts.h - header file for pmf implementation with contracts
 * on Solaris 10. Gets included by pmfd.h, which is included by some of
 * the C files like pmfd_main.c, so we need the cplusplus guard.
 */

void contracts_initialize(void);

#ifdef __cplusplus
}
#endif

#endif /* _PMFD_CONTRACTS_H */
