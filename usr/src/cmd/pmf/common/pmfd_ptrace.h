/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _PMFD_PTRACE_H
#define	_PMFD_PTRACE_H

#pragma ident	"@(#)pmfd_ptrace.h	1.3	08/05/20 SMI"

/*
 * pmfd_ptrace.h - ptrace related routines for the linux pmf daemon
 */

#ifdef __cplusplus
extern "C" {
#endif

#include "pmfd.h"

#define	WIFATFORK(status) ((((status) & 0xff0000) >> 16) == 1)
#define	WIFATVFORK(status) ((((status) & 0xff0000) >> 16) == 2)
#define	WIFATEXIT(status) ((((status) & 0xff0000) >> 16) == 6)

/*
 * Wrappers around ptrace commands
 */
extern int pmf_ptrace_traceme(struct pmf_handle *h);
extern int pmf_ptrace_cont(struct pmf_handle *h, int pid, int sig);
extern int pmf_ptrace_detach(struct pmf_handle *h, int pid);
extern int pmf_ptrace_attach(struct pmf_handle *h, int pid);
extern int pmf_ptrace_setoptions(struct pmf_handle *h, int pid);
extern int pmf_ptrace_geteventmsg(struct pmf_handle *h, int ppid, int *pid);

#ifdef __cplusplus
}
#endif

#endif	/* _PMFD_PTRACE_H */
