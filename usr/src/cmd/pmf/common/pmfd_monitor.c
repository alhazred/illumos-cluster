/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)pmfd_monitor.c	1.3	08/05/20 SMI"

/*
 * pmfd_monitor.c - routines associated to the monitoring of 1 process tree
 */

#include <rgm/rpc/pmf.h>
#include "pmfd.h"
#include "pmfd_ptrace.h"
#include "pmfd_dispatch.h"
#include <sys/wait.h>
#include <sys/sc_syslog_msg.h>
#include <sys/rsrc_tag.h>
#include <sys/os_compat.h>
#include <fcntl.h>

#define	PMFD_HANDLE_PANIC(h)	CL_PANIC(0)

extern int debug;
extern int pmf_alloc_cnt;

/*
 * Exported routines
 */
int pmf_throttle_loop(struct pmf_handle *handle, time_t req);
void pmf_register_initial_process(pmf_handle * h, int pid, int ppid);
void pmf_loop_consume_events_lock_held(struct pmf_handle *handle);

/*
 * Main events consumption switch
 */
static int pmf_consume_event(struct pmf_handle *handle,
    struct pmf_event *event);

/*
 * RPC svc events management
 */
static int suspend_event(struct pmf_handle *handle, struct pmf_event *event);
static int resume_event(struct pmf_handle *handle, struct pmf_event *event);
static int stop_event(struct pmf_handle *handle, struct pmf_event *event);
static int kill_event(struct pmf_handle *handle, struct pmf_event *event);
static int exit_event(struct pmf_handle *handle, struct pmf_event *event);

/*
 * ptrace events management
 */
static int child_event(struct pmf_handle *handle, struct pmf_event *event);
static int child_stopped_on_stop(struct pmf_handle *handle,
    struct pmf_event *event);
static int child_stopped_on_trap(struct pmf_handle *handle,
    struct pmf_event *event);
static int child_stopped_on_signal(struct pmf_handle *handle,
    struct pmf_event *event, int sig);
static int child_exited(struct pmf_handle *handle,
    struct pmf_event *event);

/*
 * Registration of processes
 */
static struct child_handle *register_child_at_startup(pmf_handle * h, int pid,
    int level);
static int register_child_at_fork(struct child_handle *parent, int ppid);
static struct child_handle *register_child(pmf_handle * h, int pid, int level,
    int regmask);
static void unregister_process(struct child_handle *p);
#ifdef PMFD_PTRACE_OPTION_EXIT
static int register_child_at_exit(struct child_handle *p, int pid);
#endif

/*
 * Utilities
 */
static inline struct child_handle *pid2handle(struct pmf_handle *h, int pid);
static void link_handle(struct child_handle *p);
static void unlink_handle(struct child_handle *p);

static int open_proc_fd(const char *pathname, struct child_handle *p);
static int close_proc_fd(struct child_handle *p);

static pthread_mutexattr_t mutex_default_attr = { PTHREAD_MUTEX_ERRORCHECK_NP };


int
pmf_throttle_loop(struct pmf_handle *handle, time_t req)
{

	struct timespec to;
	struct pmf_event *event;
	time_t deadline;
	int err;
	sc_syslog_msg_handle_t sys_handle;

	ASSERT(handle);

	deadline = req + time(NULL);

	while (!handle->stop_monitor) {

		/*
		 * Only events associated to client RPC requests are
		 * expected here. Among clients requests, STOP and EXIT
		 * are aknowledged, others are rejected.
		 */
		while ((event = dequeue_pmf_event(&handle->queue))) {
			switch (event->type) {
			case PMFD_NOP:
				event->return_code.type = PMF_OKAY;
				break;
			case PMFD_CHILD:
			case PMFD_UNREGISTERED_CHILD:
				/*
				 * This should never happen. This would be
				 * a fatal error because processes may
				 * remain stopped.
				 */
				(void) sc_syslog_msg_initialize(&sys_handle,
				    SC_SYSLOG_PMF_PMFD_TAG, "");
				/*
				 * SCMSGS
				 * @explanation
				 * Need explanation of this message!
				 * @user_action
				 * Need a user action for this message.
				 */
				(void) sc_syslog_msg_log(sys_handle,
				    LOG_CRIT, MESSAGE,
				    "Tag %s: unexpected event while in throttle"
				    "wait.",
				    handle->id);
				sc_syslog_msg_done(&sys_handle);

				PMFD_HANDLE_PANIC(handle);
				/* not reached */
				break;
			case PMFD_SUSPEND:
			case PMFD_RESUME:
			case PMFD_KILL:
				/*
				 * No processes are monitored at this
				 * time.
				 */
				event->return_code.type = PMF_SYSERRNO;
				event->return_code.sys_errno = ESRCH;
				break;
			case PMFD_STOP:
			case PMFD_EXIT:
				/*
				 * Acknowledge STOP/EXIT request
				 */
				handle->state = PMF_TREE_STOPPED;
				handle->action_type = PMFACTION_NULL;
				handle->stop_monitor = 1;
				event->return_code.type = PMF_OKAY;
				break;
			default:
				event->return_code.type = PMF_SYSERRNO;
				event->return_code.sys_errno = EINVAL;
				break;
			}

			/*
			 * Signal event was processed
			 */
			LOCK_EVENT(event);
			(void) pthread_cond_signal(&event->cond);
			UNLOCK_EVENT(event);

			(void) return_pmf_event_to_pool(&event);
		}

		if (!handle->stop_monitor) {
			to.tv_sec = deadline;
			to.tv_nsec = 0;
			err = pthread_cond_timedwait(&handle->handle_cond,
			    &handle->handle_lock, &to);
			/*
			 * re-loop if we were interrupted by a signal
			 */
			if (err != 0 && err != EINTR) {
				return (err);
			}
		}
	}

	return (0);
}

/*
 * This is where monitoring thread is waiting for events associated to
 * one monitoring tree.
 */
void
pmf_loop_consume_events_lock_held(struct pmf_handle *handle)
{

	int go = 1;
	struct pmf_event *event;

	ASSERT(handle);

	/*
	 * We hold tree lock when entering this routine
	 */

	while (go) {

		/*
		 * Consume all enqueued events
		 */
		while ((event = dequeue_pmf_event(&handle->queue))) {
			pmf_consume_event(handle, event);
		}

		/*
		 * No more processes to monitor: exit loop
		 */
		if (!handle->head)
			break;

		/*
		 * Atomically release lock and wait for new events.
		 * When event is trapped lock is reacquired
		 */
		(void) pthread_cond_wait(&handle->handle_cond,
		    &handle->handle_lock);
	}

	/*
	 * Aknowledge stopping request
	 */
	if (handle->state == PMF_TREE_STOPPING) {
		handle->state = PMF_TREE_STOPPED;
		handle->action_type = PMFACTION_NULL;
		handle->stop_monitor = 1;
	}

	/*
	 * We hold tree lock when leaving this routine
	 */

}

/*
 * This is the main event switch called by event
 * consumption loop.
 * Assumption is made that caller is holding the
 * monitoring tree handle lock
 */
static int
pmf_consume_event(struct pmf_handle *handle, struct pmf_event *event)
{

	int type;
	int rc = 0;
	sc_syslog_msg_handle_t sys_handle;

	ASSERT(handle);
	ASSERT(event);

	/*
	 * We hold tree lock when entering this routine
	 */

	type = event->type;

	switch (event->type) {
	case PMFD_NOP:
		rc = 0;
		event->return_code.type = PMF_OKAY;
		break;
	case PMFD_CHILD:
	case PMFD_UNREGISTERED_CHILD:
		if (debug)
			dbg_msgout(NOGET
			    ("Monitoring thread %s got child event "
			    "pid=%d status=%d 0x%x\n"),
			    handle->id, event->pid, event->status,
			    event->status);

		rc = child_event(handle, event);
		break;
	case PMFD_SUSPEND:
		if (debug)
			dbg_msgout(NOGET
			    ("Monitoring thread %s got suspend event "
			    "pid=%d\n"),
			    handle->id, event->pid);

		rc = suspend_event(handle, event);
		break;
	case PMFD_RESUME:
		if (debug)
			dbg_msgout(NOGET
			    ("Monitoring thread %s got resume event "
			    "pid=%d\n"),
			    handle->id, event->pid);

		rc = resume_event(handle, event);
		break;
	case PMFD_STOP:
		if (debug)
			dbg_msgout(NOGET
			    ("Monitoring thread %s got stop event "
			    "sig=%d\n"),
			    handle->id, event->signal);
		rc = stop_event(handle, event);
		break;
	case PMFD_KILL:
		if (debug)
			dbg_msgout(NOGET
			    ("Monitoring thread %s got kill event "
			    "sig=%d\n"),
			    handle->id, event->signal);

		rc = kill_event(handle, event);
		break;
	case PMFD_EXIT:
		if (debug)
			dbg_msgout(NOGET
			    ("Monitoring thread %s got exit event \n"),
			    handle->id);

		rc = exit_event(handle, event);
		break;
	default:
		if (debug)
			dbg_msgout(NOGET
			    ("Monitoring thread %s got unexpected event "
			    "on pid=%d\n"),
			    handle->id, event->pid);
		rc = 0;
		event->return_code.type = PMF_SYSERRNO;
		event->return_code.sys_errno = EINVAL;
		break;
	}

	/*
	 * An unrecoverable error occured.
	 */
	if (rc != 0) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_CRIT, MESSAGE,
		    "Monitoring of \"%s\": unrecoverable error in event"
		    "processing",
		    handle->id);
		sc_syslog_msg_done(&sys_handle);

		PMFD_HANDLE_PANIC(handle);
		/* not reached */
	}

	/*
	 * Signal event was processed
	 */
	LOCK_EVENT(event);
	(void) pthread_cond_signal(&event->cond);
	UNLOCK_EVENT(event);

	(void) return_pmf_event_to_pool(&event);

	return (type);

	/*
	 * We hold tree lock when leaving this routine
	 */
}

static int
open_proc_fd(const char *pathname, struct child_handle *p) {

	if (!p)
		return (EINVAL);

	if ((p->proc_status_fd = open(pathname, O_RDONLY)) < 0) {
		p->proc_status_opened = 0;
		return (errno);
	}
	p->proc_status_opened = 1;

	if (debug && p->handle)
		dbg_msgout(NOGET
		    ("Monitoring thread %s opened %s file fd=%d\n"),
		    p->handle->id, pathname, p->proc_status_fd);

	return (0);
}

static int
close_proc_fd(struct child_handle *p) {

	int rc = 0;

	if (!p)
		return (EINVAL);

	if (p->proc_status_opened) {
		if ((rc = close(p->proc_status_fd)) == 0) {
			p->proc_status_opened = 0;
		}
	}

	return (rc);
}

static int
suspend_event(struct pmf_handle *handle, struct pmf_event *event)
{

	int rc;
	struct child_handle *p;
	sc_syslog_msg_handle_t sys_handle;

	ASSERT(handle);
	ASSERT(event);

	if (handle->state == PMF_TREE_STOPPING) {
		event->return_code.type = PMF_SYSERRNO;
		event->return_code.sys_errno = EBUSY;
		return (0);
	}

	p = pid2handle(handle, event->pid);

	if (p && p->monitor_state == PMF_MONITORED) {

		if (debug)
			dbg_msgout(NOGET
			    ("Monitoring thread %s suspending monitoring of "
			    "process pid=%d\n"),
			    handle->id, event->pid);
		/*
		 * Open the state file associated to process
		 * under /proc.
		 * If open fail we reject the suspend request.
		 * This should never happen, and it should be
		 * safe to retry the command -> return EAGAIN
		 */
		rc = snprintf(handle->buf.buf, sizeof (struct proc_buffer),
		    "/proc/%d/status", event->pid);
		if ((rc < 0) || (rc >= sizeof (struct proc_buffer))) {
			(void) sc_syslog_msg_initialize(&sys_handle,
				    SC_SYSLOG_PMF_PMFD_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * Need explanation of this message!
			 * @user_action
			 * Need a user action for this message.
			 */
			(void) sc_syslog_msg_log(sys_handle,
				    LOG_ERR, MESSAGE, "snprintf: output of "
				    "\"/proc/%d/status\" returned %d\n",
				    event->pid, rc);
			sc_syslog_msg_done(&sys_handle);
			event->return_code.type = PMF_SYSERRNO;
			event->return_code.sys_errno = EINVAL;
			return (0);
		}

		if ((rc = open_proc_fd(handle->buf.buf, p)) != 0) {
			(void) sc_syslog_msg_initialize(&sys_handle,
				    SC_SYSLOG_PMF_PMFD_TAG, "");
			(void) sc_syslog_msg_log(sys_handle,
				    LOG_ERR, MESSAGE, "open: %s",
				    strerror(rc));
			sc_syslog_msg_done(&sys_handle);
			event->return_code.type = PMF_SYSERRNO;
			event->return_code.sys_errno = EAGAIN;
			return (0);
		}

		/*
		 * kill may fail if process is exiting
		 * We should then reject the suspend request.
		 */
		if ((rc = kill(event->pid, SIGSTOP))) {
			(void) close_proc_fd(p);
			event->return_code.type = PMF_SYSERRNO;
			event->return_code.sys_errno = rc;
		} else {
			p->monitor_state = PMF_SUSPENDING;
			event->return_code.type = PMF_OKAY;
		}
	} else if (p && p->monitor_state == PMF_SUSPENDING) {
		event->return_code.type = PMF_OKAY;
	} else {
		event->return_code.type = PMF_SYSERRNO;
		event->return_code.sys_errno = EINVAL;
	}

	return (0);
}

static int
resume_event(struct pmf_handle *handle, struct pmf_event *event)
{
	int rc;
	struct child_handle *p;
	sc_syslog_msg_handle_t sys_handle;

	ASSERT(handle);
	ASSERT(event);

	p = pid2handle(handle, event->pid);

	if (!p) {
		event->return_code.type = PMF_SYSERRNO;
		event->return_code.sys_errno = ESRCH;
		goto out;
	}

	if (p->monitor_state == PMF_UNMONITORED) {

		if (debug)
			dbg_msgout(NOGET
			    ("Monitoring thread %s resuming monitoring of "
			    "process pid=%d\n"),
			    handle->id, event->pid);

		/*
		 * Verify that a process with pid exist
		 */
		if (kill(event->pid, 0) == ESRCH) {
			if (debug)
				dbg_msgout(NOGET
				    ("Monitoring thread %s resuming of "
				    "process pid=%d, non existing process\n"),
				    handle->id, event->pid);
			unregister_process(p);
			event->return_code.type = PMF_SYSERRNO;
			event->return_code.sys_errno = ESRCH;
			goto out;
		}

		if (p->proc_status_opened != 1) {
			if (debug)
				dbg_msgout(NOGET
				    ("Monitoring thread %s resuming of "
				    "process pid=%d, no open fd on "
				    "/proc/%d/status file\n"),
				    handle->id, event->pid);
			unregister_process(p);
			event->return_code.type = PMF_SYSERRNO;
			event->return_code.sys_errno = ESRCH;
			goto out;
		}

		/*
		 * Verify that pid was not reassigned to a new process. We
		 * must redo this test after being reattached to the process.
		 * But this initial check will likelly capture most of the
		 * occurance of such situation which will avoid unecessary
		 * attachement
		 */
		if (read(p->proc_status_fd, handle->buf.buf,
		    sizeof (struct proc_buffer)) < 0) {
			if (debug)
				dbg_msgout(NOGET
				    ("Monitoring thread %s resuming of "
				    "process pid=%d pid reassigne while "
				    "monitoring was suspended\n"),
				    handle->id, event->pid);
			unregister_process(p);
			event->return_code.type = PMF_SYSERRNO;
			event->return_code.sys_errno = ESRCH;
			goto out;
		}

		/*
		 * Re-attach to process.
		 */
		rc = pmf_ptrace_attach(handle, event->pid);
		if (rc) {
			/*
			 * Process does not exist anymore or cannot
			 * be accessed by ptrace. In all cases unregister
			 * the process and return ptrace error code
			 */
			unregister_process(p);
			event->return_code.type = PMF_SYSERRNO;
			event->return_code.sys_errno = ESRCH;
			goto out;
		}

		/*
		 * Houston ... we have a problem
		 *
		 * Process dies and pid was reassigned in the small windows
		 * of time between previous read and ptrace attachement.
		 *
		 * This is very, very unlikelly to happen (in particular
		 * due to the fact that pid are assigned sequentialy) and
		 * suspend/resume is an undocumented feature reserved for
		 * debugging purpose => fatal error (exit from PMF)
		 *
		 * Note that it could be quite difficult to recover from such
		 * situation because event queue could contains events related
		 * to both processes: the initial process we were monitoring,
		 * the new one to wich was assigned the same pid. There is no
		 * criteria to distinguish between them (need to add a
		 * timestamp).
		 */
		if (read(p->proc_status_fd, handle->buf.buf,
		    sizeof (handle->buf.buf)) < 0) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_PMF_PMFD_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * Need explanation of this message!
			 * @user_action
			 * Need a user action for this message.
			 */
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_CRIT, MESSAGE,
			    "Monitoring of \"%s\": pid has been "
			    "reallocated to another process",
			    handle->id);
			sc_syslog_msg_done(&sys_handle);

			PMFD_HANDLE_PANIC(handle);
			/* not reached */
		}

		/*
		 * All lights are green
		 */
		(void) close_proc_fd(p);
		event->return_code.type = PMF_OKAY;
		p->monitor_state = PMF_RESUMING;
		(void) pthread_cond_broadcast(&p->cv);

		/*
		 * Dispatching thread could be waiting on sem if there was
		 * no more processes to monitor
		 */
		pmf_awake_dispatching_thread();


	} else {
		event->return_code.type = PMF_SYSERRNO;
		event->return_code.sys_errno = ESRCH;
	}

out:
	return (0);
}

static int
exit_event(struct pmf_handle *handle, struct pmf_event *event)
{
	struct child_handle *p;

	ASSERT(handle);
	ASSERT(event);

	handle->state = PMF_TREE_STOPPING;

	for (p = handle->head; p; p = p->next) {

		switch (p->monitor_state) {
		case PMF_MONITORED:
			/*
			 * Process is monitored. Send it a SIGSTOP so
			 * that we will be able to detach from it.
			 * If process startup is not already registered
			 * then a SIGSTOP is already pending on it, and
			 * we MUST NOT send another SIGSTOP.
			 */
			p->monitor_state = PMF_SUSPENDING;
			(void) pthread_cond_broadcast(&p->cv);
			if (IS_REGISTERED_AT_STARTUP(p)) {
				if (debug)
					dbg_msgout(NOGET("Monitoring thread "
					    "%s exit event "
					    "send signal SIGSTOP to pid=%d\n"),
					    handle->id, p->pid);
				(void) kill(p->pid, SIGSTOP);
			} else {
				if (debug)
					dbg_msgout(NOGET("Monitoring thread "
					    "%s exit event "
					    "signal SIGSTOP already pending "
					    "on pid=%d\n"),
					    handle->id, p->pid);
			}
			break;
		case PMF_UNMONITORED:
			/*
			 * Process is already detached, unregister it.
			 */
			if (debug)
				dbg_msgout(NOGET("Monitoring thread %s "
				    "exit event "
				    "unregistering process pid=%d\n"),
				    handle->id, p->pid);
			p->monitor_state = PMF_FREE;
			(void) pthread_cond_broadcast(&p->cv);
			unregister_process(p);
			break;
		case PMF_RESUMING:
			/*
			 * A signal was already sent when resuming of
			 * monitoring was requested.
			 * Change state to PMF_SUSPENDING so that process
			 * could be detached when event is acknowledged.
			 */
			p->monitor_state = PMF_SUSPENDING;
			(void) pthread_cond_broadcast(&p->cv);
			break;
		case PMF_SUSPENDING:
			/*
			 * A signal was already sent and handle->state has
			 * been set to PMF_TREE_STOPPING: nothing to do here.
			 */
			break;
		default:
			break;
		}
	}

	return (0);
}

static int
stop_event(struct pmf_handle *handle, struct pmf_event *event)
{
	struct child_handle *p;
	int sig;

	ASSERT(handle);
	ASSERT(event);

	/*
	 * Check that we are in a stable state
	 */
	for (p = handle->head; p; p = p->next) {
		if (p->monitor_state != PMF_MONITORED) {
			event->return_code.type = PMF_SYSERRNO;
			event->return_code.sys_errno = EAGAIN;
			return (0);
		}
	}

	sig = event->signal;

	handle->state = PMF_TREE_STOPPING;

	for (p = handle->head; p; p = p->next) {
		p->monitor_state = PMF_SUSPENDING;
		/*
		 * If requested, send the SIGNAL to the process.  We will
		 * detach from it when the process has exited.  If process
		 * startup initial event was not already seen, then a
		 * SIGSTOP is pending and we MUST NOT send another SIGSTOP
		 * to this process.
		 */
		if (sig != 0) {
			if (IS_REGISTERED_AT_STARTUP(p)) {
				if (debug)
					dbg_msgout(NOGET("Monitoring thread %s "
					"send signal %d to pid=%d\n"),
					handle->id, sig, p->pid);
				(void) kill(p->pid, sig);
			} else {
			if (debug)
				dbg_msgout(NOGET("Monitoring thread %s "
				    "STOP signal already pending on pid=%d\n"),
				    handle->id, p->pid);
			}
		}
	}
	return (0);
}

static int
kill_event(struct pmf_handle *handle, struct pmf_event *event)
{
	struct child_handle *p;
	int sig;

	ASSERT(handle);
	ASSERT(event);

	sig = event->signal;
	for (p = handle->head; p; p = p->next) {
		if (debug)
			dbg_msgout(NOGET("Monitoring thread %s "
			    "send signal %d to pid=%d\n"),
			    handle->id, sig, p->pid);
		(void) kill(p->pid, sig);
	}

	return (0);
}

/*
 *
 *  Dispatching one event triggered by a traced process or a real child
 *
 */
static int
child_event(struct pmf_handle *handle, struct pmf_event *event)
{

	int sig;

	ASSERT(handle);
	ASSERT(event);

	/*
	 * If child is stopped we have something to do. There a 3 differents
	 * cases
	 *
	 *    - process stopped on SIGTRAP
	 *	An event we are tracing was raised (fork, exit ...)
	 *	we must restart the process.
	 *
	 *    - process stopped on SIGSTOP
	 *	- New processes are created in this state
	 *	- A SIGSTOP was send explicitely to this process
	 *	in any case we must restart process
	 *
	 *    - process stopped on another signal:
	 *	A signal was posted to process and we intercept it.
	 *	We must repost the signal using PTRACE_CONT
	 *
	 * If child not stopped, it exited either normally or because it
	 * receive a signal.
	 */
	if (WIFSTOPPED(event->status)) {

		sig = WSTOPSIG(event->status);
		if (sig == SIGSTOP) {
			return (child_stopped_on_stop(handle, event));
		} else if (sig == SIGTRAP) {
			return (child_stopped_on_trap(handle, event));
		} else {
			return (child_stopped_on_signal(handle, event, sig));
		}

	} else {
		return (child_exited(handle, event));
	}

}

/*
 * Child is stopped on SIGSTOP. This is either a new child that is started
 * with SIGSTOP pending, or the process receive a SIGSTOP.
 */
static int
child_stopped_on_stop(struct pmf_handle *handle, struct pmf_event *event)
{

	struct child_handle *p;
	sc_syslog_msg_handle_t sys_handle;

	ASSERT(handle);
	ASSERT(event);

	if (debug)
		dbg_msgout(NOGET("Monitoring thread %s "
		    "child %d stopped on SIGSTOP\n"),
		    handle->id, event->pid);

	/*
	 * Try to find the handle associated to process.
	 * Event type, PMFD_CHILD or PMFD_UNREGISTERED_CHILD,
	 * was initially guessed by dispatching thread. So even if
	 * event type is PMFD_UNREGISTERED_CHILD, it may happen
	 * that process was registered in between and that it may be
	 * found now.
	 */
	if (!(p = pid2handle(handle, event->pid))) {
		if (event->type == PMFD_UNREGISTERED_CHILD) {
			/*
			 * event from a new process not already registered.
			 * register it (event contains necessary parameters:
			 * pid and level)
			 *
			 */
			if (debug)
				dbg_msgout(NOGET("Monitoring thread %s "
				    "Register new child %d at startup\n"),
				    handle->id, event->pid);

			if (!(p = register_child_at_startup(handle,
			    event->pid, event->level))) {
				/*
				 * Registration could return a NULL handle
				 * only if and unregistration was already
				 * pending. As we are in the case where this
				 * process was unknown this should never
				 * happen.
				 */
				(void) sc_syslog_msg_initialize(&sys_handle,
				    SC_SYSLOG_PMF_PMFD_TAG, "");
				/*
				 * SCMSGS
				 * @explanation
				 * Need explanation of this message!
				 * @user_action
				 * Need a user action for this message.
				 */
				(void) sc_syslog_msg_log(sys_handle,
				    LOG_CRIT, MESSAGE,
				    "Monitoring of \"%s\": registration of "
				    "starting process failed",
				    handle->id);
				sc_syslog_msg_done(&sys_handle);

				PMFD_HANDLE_PANIC(handle);
				/* not reached */
			}
		} else {
			/*
			 * Dispatching was able to route this event to us,
			 * but now we are unable to find this process in our
			 * list. This is an internal error and we cannot
			 * recover. This should never happen.
			 */
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_PMF_PMFD_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * Need explanation of this message!
			 * @user_action
			 * Need a user action for this message.
			 */
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_CRIT, MESSAGE,
			    "Monitoring of \"%s\": unknown stopped child "
			    "process", handle->id);
			sc_syslog_msg_done(&sys_handle);

			PMFD_HANDLE_PANIC(handle);
			/* not reached */
		}
	}

	/*
	 * at this point p references a valid handle
	 */

	/*
	 * If this process is not registered at startup
	 * then we are dealing with the SIGSTOP that was
	 * pending at startup.
	 */
	if (!IS_REGISTERED_AT_STARTUP(p)) {
		if (debug)
			dbg_msgout(NOGET("Monitoring thread %s "
			    "completing registration of child %d "
			    "at startup\n"),
			    handle->id, event->pid);
		p->regmask |= REGISTER_AT_STARTUP;
	}

	if (handle->monitor_children == PMF_LEVEL &&
		    p->level > handle->monitor_level) {
		/*
		 * Detach from process in any case if process level
		 * exceeds monitoring level.
		 */
		if (pmf_ptrace_detach(handle, p->pid) < 0) {
			return (-1);
		}
		p->monitor_state = PMF_FREE;
		(void) pthread_cond_broadcast(&p->cv);
		unregister_process(p);
	} else {
		if (p->monitor_state == PMF_SUSPENDING) {
			if (pmf_ptrace_detach(handle, p->pid) < 0) {
				return (-1);
			}
			p->monitor_state = PMF_UNMONITORED;
			(void) pthread_cond_broadcast(&p->cv);
			if (handle->state == PMF_TREE_STOPPING) {
				unregister_process(p);
			}
		} else if (p->monitor_state == PMF_RESUMING) {
			/*
			 * configure ptrace options and restart the
			 * process
			 */
			if (pmf_ptrace_setoptions(handle, event->pid) < 0) {
				return (-1);
			}
			if (pmf_ptrace_cont(handle, event->pid, 0) < 0) {
				return (-1);
			}
			p->monitor_state = PMF_MONITORED;
			(void) pthread_cond_broadcast(&p->cv);

		} else {
			if (pmf_ptrace_cont(handle, event->pid, 0) < 0) {
				return (-1);
			}
		}
	}
	return (0);
}

/*
 * Process is stopped on SIGTRAP. This is an event associated to an event
 * for which we registered (fork, vfork ...) or process received a SIGTRAP
 * signal.
 */
static int
child_stopped_on_trap(struct pmf_handle *handle, struct pmf_event *event)
{
	struct child_handle *p;
	int pid;
	int status;
	sc_syslog_msg_handle_t sys_handle;

	ASSERT(handle);
	ASSERT(event);

	pid = event->pid;
	status = event->status;

	if (debug)
		dbg_msgout(NOGET("Monitoring thread %s "
		    "child %d stopped on SIGTRAP\n"),
		    handle->id, event->pid);

	if (!(p = pid2handle(handle, pid))) {
		/*
		 * Dispatching was able to route this event to us,
		 * but now we are unable to find this process in our
		 * list. This is an internal error and we cannot
		 * recover. This should never happen.
		 */
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_CRIT, MESSAGE,
		    "Monitoring of \"%s\": unknown trapped child "
		    "process", handle->id);
		sc_syslog_msg_done(&sys_handle);

		PMFD_HANDLE_PANIC(handle);
		/* not reached */
	}

	if (p->monitor_state == PMF_STARTING ||
	    p->monitor_state == PMF_RESUMING) {

		/*
		 * This is the initial process of the tree or
		 * we just reattach to this process.
		 * configure ptrace options and restart the
		 * process
		 */

		if (pmf_ptrace_setoptions(handle, pid) < 0) {
			return (-1);
		}
		if (pmf_ptrace_cont(handle, pid, 0) < 0) {
			return (-1);
		}
		p->monitor_state = PMF_MONITORED;
		(void) pthread_cond_broadcast(&p->cv);

	} else {
		/*
		 * ptraced process had just forked we must
		 * get te pid of the new child.
		 */
		if (WIFATFORK(status) || WIFATVFORK(status)) {
			if (register_child_at_fork(p, pid) < 0) {
				return (-1);
			}
#ifdef PMFD_PTRACE_OPTION_EXIT
		} else if (WIFATEXIT(status)) {
			if (register_child_at_exit(p, pid) < 0) {
				return (-1);
			}
#endif
		}

		/*
		 * In any case restart the process. The state
		 * PMF_SUSPENDING, where we should detach from
		 * process, is not managed here. This state
		 * should be aknowledged when process is stopped
		 * on SIGSTOP.
		 */
		if (pmf_ptrace_cont(handle, pid, 0) < 0) {
			if (debug)
				dbg_msgout(NOGET("Monitoring "
				    "thread %s "
				    "restarting trapped "
				    "process %d with kill "
				    "SIGCONT (ptrace_cont "
				    "failed)\n"),
				    handle->id, pid);
			if (kill(pid, SIGCONT)) {
				return (-1);
			}
		}
	}

	return (0);
}

static int
child_stopped_on_signal(struct pmf_handle *handle, struct pmf_event *event,
	int sig)
{
	ASSERT(handle);
	ASSERT(event);

	/*
	 * Child was stopped because it receives a signal that
	 * we have intercepted. We must forward this signal
	 * using PTRACE_CONT
	 */
	if (debug)
		dbg_msgout(NOGET("Monitoring thread %s "
		    "child %d stopped on signal %d\n"),
		    handle->id, event->pid, sig);

	if (pmf_ptrace_cont(handle, event->pid, sig) < 0) {
		if (debug)
			dbg_msgout(NOGET("Monitoring thread %s "
			    "restarting trapped process %d with kill "
			    "%d (ptrace_cont failed)\n"),
			    handle->id, event->pid, sig);
		if (kill(event->pid, sig)) {
			return (-1);
		}
	}

	return (0);
}

static int
child_exited(struct pmf_handle *handle, struct pmf_event *event)
{

	struct child_handle *p;
	int pid;
	int status;

	ASSERT(handle);
	ASSERT(event);

	pid = event->pid;
	status = event->status;

	if (debug) {

		if (WIFSIGNALED(status)) {
			dbg_msgout(NOGET("Monitoring thread %s "
			    "process %d terminating on signal %d\n"),
			    handle->id, pid, WTERMSIG(status));
		} else {
			dbg_msgout(NOGET("Monitoring thread %s "
			    "process %d exiting with status %d\n"),
			    handle->id, pid, WEXITSTATUS(status));
		}
	}

	/*
	 * Free the handle associated with exiting process
	 */
	if ((p = pid2handle(handle, pid))) {
		/*
		 * If this is the main process
		 * of the monitored tree, get
		 * the exit status.
		 */
		if (pid == handle->pid) {
			handle->exited = 1;
			handle->status = status;
		}
		p->monitor_state = PMF_FREE;
		(void) pthread_cond_broadcast(&p->cv);
		unregister_process(p);
	}

	return (0);
}

/*
 * Initial monitored process registration.
 * Should be done before the waiting thread
 * try to dispatch the first event notification
 * it get about this new process.
 * Lock on pmf_handle_list must be held on
 * entry and will remain untouched.
 */
void
pmf_register_initial_process(pmf_handle * h, int pid, int ppid)
{

	struct child_handle *handle;
	sc_syslog_msg_handle_t sys_handle;

	ASSERT(h);

	if ((handle =
	    (struct child_handle *)svc_alloc(
		    sizeof (struct child_handle), &pmf_alloc_cnt))) {

		(void) memset(handle, 0, sizeof (struct child_handle));

		handle->pid = pid;
		handle->ppid = ppid;
		handle->monitor_state = PMF_STARTING;
		(void) pthread_cond_init(&handle->cv, NULL);
		(void) pthread_mutex_init(&handle->lock, &mutex_default_attr);
		handle->handle = h;
		handle->regmask |= (REGISTER_AT_FORK | REGISTER_AT_STARTUP);
		handle->proc_status_fd = -1;
		link_handle(handle);

	} else {
		/*
		 * Allocation failed. This is a fatal error as
		 * we already forked a child and are not able to
		 * monitor it. This can happen when starting or
		 * re-starting a command or when performing the
		 * requested recovery action.
		 */
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_CRIT, MESSAGE,
		    "Unable to allocate memory for child handle");
		sc_syslog_msg_done(&sys_handle);

		PMFD_HANDLE_PANIC(h);
		/* not reached */
	}
}


/*
 * A new child process of an already monitored process is
 * stopped at startup (created with SIGSTOP pending).
 * Pid of child and its monitoring level are passed
 * as parameter.
 */
static struct child_handle *
register_child_at_startup(pmf_handle * h, int pid, int level)
{

	if (debug)
		dbg_msgout(NOGET("Monitoring thread %s "
		    "startup of a new child pid=%d\n"),
		    h->id, pid);

	return (register_child(h, pid, level, REGISTER_AT_STARTUP));
}

/*
 * A monitored process is trapped after having performed a fork.
 * We want to create a handle for the new child. Trapped process,
 * whose pid is passed as parameter, is the parent in the fork.
 * We should use ptrace GETEVENTMSG option to get new child pid.
 * If monotoring level exceed threshold (option -C) we must
 * detach from child. This will be done later when child will be
 * stopped.
 */
static int
register_child_at_fork(struct child_handle *parent, int ppid)
{

	int pid = 0;
	struct pmf_handle *h;

	ASSERT(parent && parent->handle);

	h = parent->handle;

	if (pmf_ptrace_geteventmsg(h, ppid, &pid))
		return (-1);

	/*
	 * Register this child.
	 * Returned handle may be null if child process was
	 * already pre-unregistered.
	 */
	(void) register_child(h, pid, parent->level + 1, REGISTER_AT_FORK);

	return (0);
}

/*
 * Register a child process within a monitoring tree.
 * If child was already registered, update the
 * registering level and aknowledge any pending
 * unregistration.
 */
static struct child_handle *
register_child(pmf_handle * h, int pid, int level, int regmask)
{


	struct child_handle *handle = (struct child_handle *)0;
	sc_syslog_msg_handle_t sys_handle;

	ASSERT(h);

	if ((handle = pid2handle(h, pid))) {
		/*
		 * Update registration level and aknowledge any
		 * previous unregistration request. Note that
		 * unregister_process() does nothing if registration
		 * is not complete.
		 */
		handle->regmask |= regmask;
		if (IS_UNREGISTERED(handle)) {
			(void) unregister_process(handle);
			/* handle should be NULL here */
		}
	} else {

		/*
		 * this is the fist time we have to deal with this process.
		 * Get an initilized handle for it.
		 */
		if ((handle =
		    (struct child_handle *)svc_alloc(
			    sizeof (struct child_handle), &pmf_alloc_cnt))) {

			(void) memset(handle, 0, sizeof (struct child_handle));

			handle->pid = pid;
			handle->level = level;
			handle->monitor_state =
			    (h->state ==
			    PMF_TREE_STOPPING) ? PMF_SUSPENDING : PMF_MONITORED;
			(void) pthread_cond_init(&handle->cv, NULL);
			(void) pthread_mutex_init(&handle->lock,
			    &mutex_default_attr);
			handle->handle = h;
			handle->regmask |= regmask;
			handle->proc_status_fd = -1;

			if (debug)
				dbg_msgout(NOGET("Monitoring thread %s "
				    "registering new child pid=%d "
				    "level=%ld reg=0x%x\n"),
				    h->id, handle->pid, handle->level,
				    handle->regmask);
			link_handle(handle);

		} else {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_PMF_PMFD_TAG, "");
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_CRIT, MESSAGE,
			    "Unable to allocate memory for child handle");
			sc_syslog_msg_done(&sys_handle);

			PMFD_HANDLE_PANIC(h);
			/* not reached */
		}

	}
	return (handle);
}

/*
 * Unregister a process from monitoring tree list.
 * Process registration must be completed before
 * underlaying handle is effectively unlinked.
 * If this is not the case unregistration request
 * is memorized by setting UNREGISTER flag.
 */
static void
unregister_process(struct child_handle *p)
{

	ASSERT(p && p->handle);

	p->regmask |= UNREGISTER;

	if (IS_REGISTERED(p)) {
		if (debug)
			dbg_msgout(NOGET("Monitoring thread %s "
			    "unregistering process pid=%d\n"),
			    p->handle->id, p->pid);
		unlink_handle(p);
	} else {
		if (debug)
			dbg_msgout(NOGET("Monitoring thread %s "
			    "pre-unregistering process pid=%d\n"),
			    p->handle->id, p->pid);
	}

}

#ifdef PMFD_PTRACE_OPTION_EXIT
/*
 * One monitored process is trapped at exit.
 * We cannot get its exit status using ptrace
 * so, for now, there is nothing usefull we can
 * do.
 * This would be usefull if we have something to
 * do at exit and wanted to be sure that the OS
 * didnt already release ressources associated
 * to this process (ex. pid).
 */
static int
register_child_at_exit(struct child_handle *h, int pid)
{

	if (debug && h && h->handle)
		dbg_msgout(NOGET("Monitoring thread %s "
		    "child pid=%d trapped at exit\n"),
		    h->handle->id, pid);
	return (0);
}
#endif

/*
 * Link a child handle at head of
 * monitoring tree handle list.
 * Assumption is made that caller hold
 * the monitoring tree handle lock.
 */
static void
link_handle(struct child_handle *p)
{

	ASSERT(p && p->handle);

	p->prev = (struct child_handle *)0;
	if ((p->next = p->handle->head))
		p->next->prev = p;
	p->handle->head = p;

}

/*
 * Unlink a child handle from the
 * monitoring tree handle list.
 * Assumption is made that caller hold
 * the monitoring tree handle lock.
 */
static void
unlink_handle(struct child_handle *p)
{

	struct child_handle *next, *prev;

	ASSERT(p && p->handle);

	if ((prev = p->prev)) {
		prev->next = p->next;
	} else {
		p->handle->head = p->next;
	}

	if ((next = p->next))
		next->prev = prev;

	(void) close_proc_fd(p);

	if (p->monitor_state == PMF_FREE) {
		(void) pthread_mutex_destroy(&p->lock);
		(void) pthread_cond_destroy(&p->cv);
		svc_free(p, &pmf_alloc_cnt);
	}

}
