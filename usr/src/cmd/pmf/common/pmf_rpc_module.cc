//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)pmf_rpc_module.cc	1.7	08/07/23 SMI"

#include "pmfd.h"
#include <libclcontract/cl_contract_table.h>
#include <libclcontract/cl_contract_main.h>
#include "pmf_contract.h"
#include "pmf_error.h"

#include <memory>
using namespace std;

static char **
rpc_strings_to_char_arr(int len, char **vals)
{
	char **temp;
	//
	// Duplicate the argument list, making sure that the last element
	// is NULL, not just empty, for exec().  We assume that the last
	// element in the array passed to this function should be NULL,
	// but that the rpc command might have changed it to an empty string.
	// Thus, we copy only the strings up to (length - 1), then just assign
	// a NULL to the final element.
	//
	temp = new char *[len];
	int i;
	for (i = 0; i < len - 1; i++) {
		if (vals[i] && *vals[i]) {
			temp[i] = strdup_nothrow(vals[i]);
			if (temp[i] == NULL) {
				free_string_arr(temp);
				return (NULL);
			}
		} else {
			//
			// Subsequent code cannot handle a NULL except at
			// the end of the array.  Thus, we need to bail out
			// here.
			//
			temp[i] = NULL;
			free_string_arr(temp);
			return (NULL);
		}
	}
	//
	// We need to end the list with a NULL pointer, for exec.
	//
	temp[i] = NULL;
	return (temp);
}

//
// What follows are our RPC entry points.
//

//
// pmf_start_svc
//
// Check for duplicate identifiers first. Then create a new pmf_contract
// object, add it to the pmf_contract_table, and call start() on it.
//
// cred is used only to obtain the uid and gid.
//
void
pmf_start_svc(pmf_start_args *args, pmf_result *result,
	security_cred_t *cred)
{
	int err;
	bool duplicate = true;
	char **cmd = NULL, **env = NULL;
	char *real_action = NULL;

	debug_print(NOGET("entering pmf_start_svc for id %s"),
	    args->identifier);

	result->code.type = PMF_OKAY;

	smart_ptr<cl_contract> sp(NULL);
	if (cl_contract_table::instance().lookup(args->identifier, sp) == 0) {
		debug_print(NOGET("pmf_start_svc: duplicate argument"));
		//
		// The request specified a duplicate identifier.
		//
		debug_print(NOGET("%s: duplicate nametag"),
		    args->identifier);

		result->code.type = PMF_DUP;
		return;
	}

	pmf_contract *pmfc = NULL;

	cmd = rpc_strings_to_char_arr(args->cmd.cmd_len, args->cmd.cmd_val);
	if (cmd == NULL) {
		debug_print(NOGET("%s: rpc_strings_to_char_arr error for cmd"),
		    args->identifier);
		result->code.type = PMF_SYSERRNO;
		result->code.sys_errno = EINVAL;
		return;
	}

	if (args->env_passed) {
		// Pass len + 1 because rpc_strings_to_char_arr ignores
		// position len - 1.
		env = rpc_strings_to_char_arr(args->env.env_len + 1,
		    args->env.env_val);
		if (env == NULL) {
			debug_print(NOGET("%s: rpc_strings_to_char_arr error"
			    "for env"), args->identifier);
			result->code.type = PMF_SYSERRNO;
			result->code.sys_errno = EINVAL;
			free_string_arr(cmd);
			return;
		}
	}
	if (args->action_type == PMFACTION_EXEC) {
		real_action = args->action;
	}
	if (args->monitor_children == PMF_ALL) {
		pmfc = new pmf_contract(args->identifier, cmd, env,
		    args->pwd, args->path, args->retries, args->period,
		    real_action, args->flags, args->project_name,
		    cred->aup_uid, cred->aup_gid, err);
	} else {
		pmfc = new pmf_contract_depth(args->identifier, cmd, env,
		    args->pwd, args->path, args->retries, args->period,
		    real_action, args->flags, args->project_name,
		    cred->aup_uid, cred->aup_gid, args->monitor_level, err);
	}

	if (pmfc == NULL) {
		// Handle out-of-memory conditions
		result->code.type = PMF_SYSERRNO;
		result->code.sys_errno = ENOMEM;
		free_string_arr(cmd);
		free_string_arr(env);
		return;
	} else if (err != 0) {
		// Handle other errors.
		debug_print(NOGET("%s: error from pmf_contract ctor"),
		    args->identifier);
		result->code.type = PMF_SYSERRNO;
		result->code.sys_errno = err;
		delete pmfc;
		return;
	}

	// Add the new pmf contract to the contract table
	sp = smart_ptr<cl_contract>(pmfc);
	debug_print(NOGET("pmf_start_svc: adding to table, pmfc %x\n"), pmfc);
	if ((err = cl_contract_table::instance().add_contract(sp)) != 0) {
		// Handle out-of-memory conditions
		if (err == ENOMEM) {
			result->code.type = PMF_SYSERRNO;
			result->code.sys_errno = ENOMEM;
		} else {
			debug_print(NOGET("pmf_start_svc: duplicate argument "
			    "while trying to add contract to table"));
			result->code.type = PMF_DUP;
		}
		// no need to free pmfc -- smart pointer will do that for us
		return;
	}

	// start the new process
	pid_t child_pid;
	if ((err = sp->start(child_pid)) != 0) {
		//
		// The start failed. Remove this pmf_contract object from
		// the table.
		//
		debug_print(NOGET("%s: start failed"),
		    args->identifier);
		(void) cl_contract_table::instance().remove(sp->get_nametag());
		result->code.type = PMF_SYSERRNO;
		result->code.sys_errno = err;
	}
}

//
// pmf_status_svc
//
// args and cred are input args; result is an output arg.
//
// If the identifier is a valid nametag, retrieve the status by
// calling the accessors on the pmf_contract object.
//
// Otherwise, if the identifier is the empty string or the
// special "query-all" tag, return a list of all the nametags
// currently registered.
//
// Otherwise, if the identifier is "all", print the basic status
// information, but don't return anything.
//
void
pmf_status_svc(pmf_args *args, pmf_status_result *result,
	security_cred_t *cred)
{
	uint_t i;

	debug_print(NOGET("entering pmf_status_svc for id %s"),
	    args->identifier);

	(void) memset(result, 0, sizeof (pmf_status_result));
	result->code.type	= PMF_OKAY;

	smart_ptr<cl_contract> sp_clcontract(NULL);

	if (cl_contract_table::instance().lookup(
	    args->identifier, sp_clcontract) != 0) {
		//
		// That identifier does not exist.
		//

		debug_print(NOGET("id doesn't exist"));

		//
		// case PMF_STATUS_ALL:
		// list the names of all tags
		//
		if (args->query_all || args->identifier[0] == '\0') {
			debug_print(NOGET("Assembling list of tags"));
			std::vector<smart_ptr<cl_contract> > tags;
			if (cl_contract_table::instance().get_all(
			    tags) != 0) {
				result->code.type = PMF_SYSERRNO;
				result->code.sys_errno = ENOMEM;
				return;
			}

			if (tags.empty()) {
				// No tags registered
				return;
			}

			string all_tags;
			// CSTYLED
			for (std::vector<smart_ptr<cl_contract> >::
			    const_iterator it = tags.begin();
			    it != tags.end(); ++it) {
				//
				// We list all tags, regardless of credentials.
				// Any user can see status of all tags.
				//
				all_tags += (*it)->get_nametag() + " ";
			}

			//
			// malloc buf of correct size
			//
			result->identifier = (char *)
			    malloc((size_t)(all_tags.size() + 1));
			if (result->identifier == NULL) {
				result->code.type = PMF_SYSERRNO;
				result->code.sys_errno = ENOMEM;
				return;
			}
			strcpy(result->identifier, all_tags.c_str());
		} else if (strcmp(args->identifier, "all") == 0) {
			//
			// unadvertised debugging option
			//

			result->code.type = PMF_SYSERRNO;
			result->code.sys_errno = ENOENT;

			std::vector<smart_ptr<cl_contract> > tags;

			if (cl_contract_table::instance().get_all(
			    tags) != 0) {
				result->code.type = PMF_SYSERRNO;
				result->code.sys_errno = ENOMEM;
				return;
			}

			if (tags.empty()) {
				return;
			}

			// CSTYLED
			for (std::vector<smart_ptr<cl_contract> >::
			    const_iterator it = tags.begin();
			    it != tags.end(); ++it) {
				pmf_contract *sppc_it =
				    dynamic_cast<pmf_contract*>
				    ((*it).get_ptr());
				debug_print(NOGET("%s: %s\n"),
				    sppc_it->get_nametag().c_str(),
				    sppc_it->get_cmd_string());
				debug_print(NOGET(
				    "\tretries allowed=%d period=%d\npids\n"),
				    sppc_it->get_retries_allowed(),
				    sppc_it->get_period());

				std::vector<pid_t> pids;
				if (sppc_it->get_pids_vector(pids) != 0) {
					result->code.type = PMF_SYSERRNO;
					result->code.sys_errno = ENOMEM;
					return;
				}

				// CSTYLED
				for (std::vector<pid_t>:: const_iterator it2 =
				    pids.begin(); it2 != pids.end(); ++it2) {
					debug_print(NOGET(
					    "\tpid=%d running\n"), *it2);
				}
			}
		} else {
			result->code.type = PMF_SYSERRNO;
			result->code.sys_errno = ENOENT;
		}
		return;
	}

	pmf_contract *sppc = dynamic_cast<pmf_contract*>
	    (sp_clcontract.get_ptr());

	int err, length;
	// Retrieve all the info using the accessors
	result->cmd.cmd_val = sppc->get_cmd_copy(length);
	if (length == NULL) {
		//
		// Here we have to call pmf_free_status to
		// set the error and free what was
		// previously malloced.
		//
		debug_print(NOGET("error copying cmd\n"));
		pmf_free_status(result);
		return;
	}
	result->cmd.cmd_len = length;

	result->env.env_val = sppc->get_env_copy(length);
	if (length == -1) {
		//
		// Here we have to call pmf_free_status to
		// set the error and free what was
		// previously malloced.
		//
		debug_print(NOGET("error copying env\n"));
		pmf_free_status(result);
		return;
	}
	result->env.env_len = length;

	result->retries = sppc->get_retries_allowed();
	result->period = sppc->get_period();
	result->identifier = sppc->get_nametag_copy();
	if (result->identifier == NULL) {
		//
		// Here we have to call pmf_free_status to
		// set the error and free what was
		// previously malloced.
		//
		debug_print(NOGET("error copying id\n"));
		pmf_free_status(result);
		return;
	}

	result->action = sppc->get_action_copy(err);
	if (err != 0) {
		//
		// Here we have to call pmf_free_status to
		// set the error and free what was
		// previously malloced.
		//
		debug_print(NOGET("error copying action\n"));
		pmf_free_status(result);
		return;
	}

	if (result->action == NULL) {
		result->action_type = PMFACTION_NULL;
	} else {
		result->action_type = PMFACTION_EXEC;
	}

	result->nretries = sppc->get_retries();
	result->owner = sppc->get_uid();
	result->pids = sppc->get_pids_string(err);
	if (err != 0) {
		//
		// Here we have to call pmf_free_status to
		// set the error and free what was
		// previously malloced.
		//
		debug_print(NOGET("error copying pids\n"));
		pmf_free_status(result);
		return;
	}

	result->upids = NULL;
	pmf_contract_depth *sppc_depth = dynamic_cast<pmf_contract_depth*>
	    (sppc);
	if (sppc_depth == NULL) {
		result->monitor_children = PMF_ALL;
	} else {
		result->monitor_children = PMF_LEVEL;
		result->monitor_level = sppc_depth->get_monitor_level();
	}
	result->project_name = sppc->get_project_name_copy(err);
	if (err != 0) {
		//
		// Here we have to call pmf_free_status to
		// set the error and free what was
		// previously malloced.
		//
		debug_print(NOGET("error copying project name\n"));
		pmf_free_status(result);
		return;
	}
}

static void
stop_kill_impl(pmf_args* args, pmf_result* result,
    security_cred_t *cred, bool stop_flag)
{
	int err;

	(void) memset(result, 0, sizeof (pmf_result));

	smart_ptr<cl_contract> sp_clcontract(NULL);

	// Look up the identifier
	if (cl_contract_table::instance().lookup(
	    args->identifier, sp_clcontract) != 0) {
		//
		// That identifier does not exist.
		//
		debug_print(NOGET("id doesn't exist"));
		result->code.type = PMF_SYSERRNO;
		result->code.sys_errno = ENOENT;
		return;
	}

	pmf_contract *sppc = dynamic_cast<pmf_contract*>
	    (sp_clcontract.get_ptr());

	//
	// Check to make sure we're either root, or
	// the same user that started the request.
	//
	if (!(cred->aup_uid == 0 || sppc->get_uid() == cred->aup_uid)) {
		debug_print(NOGET("credentials check failed"));
		result->code.type = PMF_SYSERRNO;
		result->code.sys_errno = EACCES;
		return;
	}
	if (stop_flag) {
		sppc->stop();
	}

	// If we're supposed to send a signal, call signal_monitored().
	if (args->signal != 0) {
		err = sppc->signal_monitored(args->signal);
		if (err) {
			result->code.type = PMF_SYSERRNO;
			result->code.sys_errno = err;
			return;
		}
	}

	// Have we been asked to wait until all threads exit?
	if (args->timewait) {
		err = sppc->wait(args->timewait);
		if (err != 0) {
			result->code.type = PMF_SYSERRNO;
			result->code.sys_errno = err;
		}
	}
}

//
// pmf_stop_svc
//
// Called as result of user executing pmfadm -s
//
void
pmf_stop_svc(pmf_args *args, pmf_result *result,
	security_cred_t *cred)
{
	debug_print(NOGET("entering pmf_stop_svc for id %s; timewait=%d"),
	    args->identifier, args->timewait);
	stop_kill_impl(args, result, cred, true);
}

void
pmf_modify_svc(pmf_modify_args *args, pmf_result *result,
	security_cred_t *cred)
{
	(void) memset(result, 0, sizeof (pmf_result));

	if (args->retries > PMFD_MAX_HISTORY) {
		result->code.type = PMF_SYSERRNO;
		result->code.sys_errno = E2BIG;
		return;
	}

	smart_ptr<cl_contract> sp_clcontract(NULL);

	// Look up the identifier
	if (cl_contract_table::instance().lookup(
	    args->identifier, sp_clcontract) != 0) {
		//
		// That identifier does not exist.
		//
		result->code.type = PMF_SYSERRNO;
		result->code.sys_errno = ENOENT;
		return;
	}

	pmf_contract *sppc = dynamic_cast<pmf_contract*>
	    (sp_clcontract.get_ptr());

	if (!(cred->aup_uid == 0 || sppc->get_uid() == cred->aup_uid)) {
		result->code.type = PMF_SYSERRNO;
		result->code.sys_errno = EACCES;
		return;
	}
	if (args->retries_modified) {
		sppc->set_retries_allowed(args->retries);
	}
	if (args->period_modified) {
		sppc->set_period(args->period);
	}
}

void
pmf_kill_svc(pmf_args *args, pmf_result *result,
	security_cred_t *cred)
{
	debug_print(NOGET("entering pmf_kill_svc for id %s"),
	    args->identifier);
	stop_kill_impl(args, result, cred, false);
}

//
// pmf_suspend_svc
//
// Called as result of user executing pmfctl -S
// Should never be called in this implementation (disabled on client
// side), so it's a no-op.
//
void
pmf_suspend_svc(pmf_args *args, pmf_result *result,
	security_cred_t *cred)
{
	result->code.type = PMF_OKAY;
}

//
// pmf_resume_svc
//
// Called as result of user executing pmfctl -R
// Should never be called in this implementation (disabled on client side),
// so it's a no-op.
//
void
pmf_resume_svc(pmf_args *args, pmf_result *result,
	security_cred_t *cred)
{
	result->code.type = PMF_OKAY;
}
