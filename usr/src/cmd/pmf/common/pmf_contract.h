//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef _PMF_CONTRACT_H
#define	_PMF_CONTRACT_H

#pragma ident	"@(#)pmf_contract.h	1.6	08/07/23 SMI"

#include <pthread.h>
#include <synch.h>
#include <string>
#include <map>
#include <vector>

#include <libclcontract/cl_contract.h>

#include "restart_history.h"
#include "pmfd.h"

using std::string;

//
// class pmf_contract
//
// Stores all the information associated with a pmf nametag.
// Provides all the start, restart, stop, signal, and wait functionality
// for the process tree.
//
// User accesses this functionality by calling start() to launch the process,
// stop() to stop restarting it, wait() to wait until the process tree dies,
// and signal_all() or signal_monitored() to send a signal to all or monitored
// processes in the process tree.
//
class pmf_contract : public cl_contract {
public:
	//
	// Constructor is not dependent on the transfer mechanism,
	// so takes each argument independently, instead of using
	// the (current) rpc args structure.
	//
	// Shallow copies of cmd_in and env_in are stored. Everything
	// else is deep copied.
	//
	pmf_contract(char *identifier_in, char **cmd_in, char **env_in,
	    char *pwd_in, char *path_in, int retries_in, int period_in,
	    char *action_in, int flags_in, char *project_name_in,
	    uid_t uid_in, gid_t gid_in, int &err);

	virtual ~pmf_contract();

	// ***********************
	// control methods
	// ************************

	// Return the errno error code if something goes wrong.
	virtual int signal_monitored(int signal);
	virtual int signal_all(int signal);

	// Process a contract event
	void event_dispatch(ct_evthdl_t ev);

	// *******************
	// access methods
	// *******************
	int get_retries_allowed() { return (retries_allowed); }
	int get_period() { return (history.get_period()); }

	// Sets err to 1 on error
	char *get_action_copy(int &err);
	int get_retries();

	// Sets err to 1 on error
	char *get_project_name_copy(int &err);
	uid_t get_uid() { return uid; }

	int get_pids_vector(std::vector<pid_t>&);

	// Sets err to 1 on error
	char *pmf_contract::get_pids_string(int &err);

	// ****************
	// setter methods
	// ****************
	void set_retries_allowed(int retries_in);
	void set_period(int period_in);

protected:

	// ******************************************
	// Helper methods. See .cc file for details.
	// ******************************************

	void start_action_script();
	void action_script_finished();
	void setup_child();
	void free_members();
	void perform_throttling_or_restart(boolean_t action_script_flag);
	void restart_process(boolean_t action_script_flag);
	int incr_restart_and_generate_event(boolean_t action_script_flag);

	virtual void fork_event(ct_evthdl_t ev);
	virtual void exit_event(ct_evthdl_t ev);
	virtual void empty_event(ct_evthdl_t ev);

	// *************************
	// user-specified parameters
	// These dont change throughout
	// the lifetime of the object
	// *************************
	char *action_script_path;
	char **action_script_cmd;
	int retries_allowed;
	uid_t uid;
	gid_t gid;
	char *project_name;
	int flags;

	// ********************
	// Dynamic state
	// ********************

	// A list of the recent restarts
	restart_history history;

	// Absolute time at which the action script was last run --
	// used for the throttle wait algorithm
	time_t last_action_run;

	// The most recent signal specified by signal_monitored().
	// Needed only by subclass (pmf_contract_depth)
	int current_signal;

	// *****************************
	// flags to track internal state
	// *****************************

	// action_script_running is set whenever an action script launched
	// by this pmf_contract is running.
	bool action_script_running;

	// **********
	// constants
	// **********
	static const int PID_LEN;
	static const int PMF_THROTTLE_LIMIT;

private:
	// prevent assignment and pass-by-value
	pmf_contract(const pmf_contract &src);
	// CSTYLED
	pmf_contract &operator=(const pmf_contract &rhs);
};

//
// pmf_contract_depth
//
// Subclass of pmf_contract that monitors processes only to a specified
// depth.
//
class pmf_contract_depth : public pmf_contract {
public:
	//
	// Constructor is not dependent on the transfer mechanism,
	// so takes each argument independently, instead of using
	// the (current) rpc args structure.
	//
	// Shallow copies of cmd_in and env_in are stored. Everything
	// else is deep copied.
	//
	pmf_contract_depth(char *identifier_in, char **cmd_in,
	    char **env_in, char *pwd_in, char *path_in, int retries_in,
	    int period_in, char *action_in, int flags_in,
	    char *project_name_in, uid_t uid_in, gid_t gid_in, int depth_in,
	    int &err);

	// Different implementation of only one public method
	virtual int signal_monitored(int signal);
	int get_monitor_level() { return depth; }

protected:
	// Different implementations of two protected methods
	virtual void fork_event(ct_evthdl_t ev);
	virtual void exit_event(ct_evthdl_t ev);

	int depth;
};

#endif /* _PMF_CONTRACT_H */
