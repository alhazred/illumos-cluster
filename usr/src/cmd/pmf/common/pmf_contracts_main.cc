/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)pmf_contracts_main.cc	1.6	08/07/23 SMI"

#include "pmfd.h"
#include "pmf_throttle_wait.h"
#include <libclcontract/cl_contract_main.h>

#include <new>
using namespace std;

//
// The only thing we need to do here is initialize the cluster
// contracts library, and create the throttle wait object.
//
void
contracts_initialize()
{
	libclcontracts_initialize();
	pmf_throttle_wait::create();
}
