//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)restart_history.cc	1.6	08/05/20 SMI"

#include "restart_history.h"
#include "pmf_error.h"

using namespace std;

#include <sys/types.h>
#include <time.h>


restart_history::restart_history(int max_restarts_in, int period_in)
{
	ASSERT(max_restarts_in > 0);
	period = period_in;
	max_restarts = max_restarts_in;
}

int
restart_history::add_restart_time()
{
	try {
		restarts.push_back(time(NULL));
	// CSTYLED
	} catch (bad_alloc&) {
		return (1);
	}
	return (prune());
}

uint_t
restart_history::get_retry_count(int &err)
{
	err = prune();
	return (restarts.size());
}

int
restart_history::prune()
{
	//
	// Make sure we're not over the max
	//
	for (int size = restarts.size(); size > max_restarts; size--) {
		restarts.pop_front();
	}

	//
	// If period is -1, we count all failures.
	//
	if (period != -1) {
		// We know the list is ordered, so we can just remove
		// all entries from the beginning of the list to the
		// first entry within the period.
		time_t cur_time = time(NULL);
		if (cur_time == -1) {
			return (errno);
		}
		std::list<time_t>::iterator it = lower_bound(restarts.begin(),
		    restarts.end(), cur_time - period);
		restarts.erase(restarts.begin(), it);
	}
	return (0);
}
