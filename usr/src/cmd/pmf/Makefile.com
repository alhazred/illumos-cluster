#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#ident	"@(#)Makefile.com	1.87	08/05/27 SMI"
#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# cmd/pmf/Makefile.com
#

PROG	= $(CLIENT)
DAEMON	= $(SERVER)

SERVER	= rpc.pmfd
CLIENT	= pmfadm

include ../../Makefile.cmd

# include the macros for doors or rpc implementation
include $(SRC)/Makefile.ipc

RPCFILE	= $(ROOTCLUSTINC)/rgm/door/pmf_door.x
$(RPC_IMPL)RPCFILE	= $(ROOTCLUSTINC)/rgm/rpc/pmf.x

RPCFILE_SVC =
RPCFILE_XDR = pmf_door_xdr.c
$(RPC_IMPL)RPCFILE_SVC     = pmf_svc.c
$(RPC_IMPL)RPCFILE_XDR     = pmf_xdr.c


SERVER_SOURCES_C = $(SERVER_OBJECTS_C:%.o=../common/%.c)
SERVER_SOURCES_CC = $(SERVER_OBJECTS_CC:%.o=../common/%.cc)
SERVER_SOURCES = $(SERVER_SOURCES_C) $(SERVER_SOURCES_CC)
CLIENT_SOURCES	= $(CLIENT_OBJECTS:%.o=../common/%.c)
RPC_SOURCES	= $(RPCFILE_SVC) $(RPCFILE_XDR)
RPC_OBJECTS	= $(RPC_SOURCES:%.c=%.o)
LINTFILES	= $(SERVER_OBJECTS:%.o=%.ln) $(CLIENT_OBJECTS:%.o=%.ln)
PIFILES		= $(SERVER_SOURCES_C:%.c=%.pi) $(SERVER_SOURCES_CC:%.cc=%.pi)
PIFILES		+= $(CLIENT_SOURCES:%.c=%.pi) $(RPC_SOURCES:%.c=%.pi)
POFILE		= pmfstuff.po


CLOBBERFILES	+= $(RPCFILE_SVC) $(RPCFILE_XDR) $(DAEMON) $(PROG)

DOOR_SERVER_OBJECTS = pmf_serv_xdr.o
$(RPC_IMPL)DOOR_SERVER_OBJECTS =

$(PRE_S10_BUILD)SERVER_OBJECTS	= $(SERVER_OBJECTS_C)
$(POST_S9_BUILD)SERVER_OBJECTS	= $(SERVER_OBJECTS_C) $(SERVER_OBJECTS_CC)

$(PRE_S10_BUILD)SERVER_OBJECTS_C = pmfd.o pmfd_util.o pmfd_main.o pmfd_proc.o
$(PRE_S10_BUILD)SERVER_OBJECTS_C += pmf_shared.o
$(POST_S9_BUILD)SERVER_OBJECTS_C = pmfd.o pmfd_main.o pmf_shared.o
SERVER_OBJECTS_C += $(DOOR_SERVER_OBJECTS)

$(PRE_S10_BUILD)SERVER_OBJECTS_CC =

$(POST_S9_BUILD)SERVER_OBJECTS_CC = restart_history.o pmf_rpc_module.o
$(POST_S9_BUILD)SERVER_OBJECTS_CC += pmf_contract.o
$(POST_S9_BUILD)SERVER_OBJECTS_CC += pmf_throttle_wait.o
$(POST_S9_BUILD)SERVER_OBJECTS_CC += pmf_error.o pmf_contracts_main.o

$(PRE_S10_BUILD)SERVER_OBJECTS_CC =

CLIENT_OBJECTS	= pmfadm.o

OBJECTS = $(SERVER_OBJECTS) $(CLIENT_OBJECTS)
OBJECTS += $(RPCFILE_SVC:%.c=%.o) $(RPCFILE_XDR:%.c=%.o)

CHECKHDRS	= pmfd.h pmfd_proto.h

$(POST_S9_BUILD)CHECKHDRS += pmf_contract.h
$(POST_S9_BUILD)CHECKHDRS += pmf_error.h
$(POST_S9_BUILD)CHECKHDRS += pmf_throttle_wait.h pmfd_contracts.h
$(POST_S9_BUILD)CHECKHDRS += restart_history.h

$(PRE_S10_BUILD)CHECKHDRS += pmfd_procfs.h 

CHECK_FILES	= $(SERVER_OBJECTS:%.o=%.c_check) 
CHECK_FILES	+= $(CLIENT_OBJECTS:%.o=%.c_check) $(CHECKHDRS:%.h=%.h_check)

.KEEP_STATE:

#
# Compiler flags
#
CPPFLAGS	+= $(CL_CPPFLAGS) -I. -I$(SRC)/common/cl -I$(SRC)/head
CPPFLAGS += -DSC_SRM

LINTFLAGS	+= -I. -D_REENTRANT -D_POSIX_PTHREAD_SEMANTICS
LDLIBS	+= -lc -lnsl -lsecurity -lclos
LDLIBS	+=  -lclevent $(DOOR_LDLIBS) -lproject
$(POST_S9_BUILD)LDLIBS += -lcontract
$(POST_S9_BUILD)LDLIBS += -lclcontract

# libclcomm library cannot be used anymore because the PMF binary
# must be the same for Sun Cluster, Europa Server and Europa Farm.
# As the ORB is not available on Farm nodes, a small library
# libclcomm_compat is built and contains all the necessary symbols
# used by PMF.
$(POST_S9_BUILD)LDLIBS += -lclcomm_compat

RPCGENFLAGS	= $(RPCGENMT) -C -K -1

MTFLAG = -mt

CPPFLAGS += $(MTFLAG)

$(CLIENT):= LDLIBS += -lpmf 
$(CLIENT):= CPPFLAGS += -DPMF_SECURITY_DEF=SEC_UNIX_STRONG

OTHER_LIBS = -lscutils -ldl
$(SERVER):= LDLIBS += $(OTHER_LIBS)

#
# The 64 bit compilation uses the POSIX semantics for readdir_r, even if
# we do not define _POSIX_PTHREAD_SEMANTICS.  Thus, we must define
# _POSIX_PTHREAD_SEMANTICS for 32 bit so it uses the POSIX semantics for
# readdir_r as well.  We must also define the flag for 64-bit, so that
# other functions (like getpwuid_r and ctime_r) use POSIX semantics
# in both 32 and 64-bit.
#
$(MACH)_CFLAGS += -D_POSIX_PTHREAD_SEMANTICS
$(MACH64)_CFLAGS += -D_POSIX_PTHREAD_SEMANTICS

CPPFLAGS += -D_POSIX_PTHREAD_SEMANTICS


#
# Targets
#

all: $(DAEMON) $(PROG)

clean :
	$(RM) *.o $(CLOBBERFILES)

$(PRE_S10_BUILD)SERVERLINK = $(LINK.c)
$(POST_S9_BUILD)SERVERLINK = $(LINK.cc)

$(SERVER): $(SERVER_OBJECTS) $(RPC_OBJECTS)
	$(SERVERLINK) -o $@ $(SERVER_OBJECTS) $(RPC_OBJECTS) $(LDLIBS)
	$(POST_PROCESS)

$(CLIENT): $(CLIENT_OBJECTS)
	$(LINK.c) -o $@ $(CLIENT_OBJECTS) $(LDLIBS)
	$(POST_PROCESS)

#
# build rules
#

$(RPCFILE_SVC): $(RPCFILE)
	$(RM) $@
	$(RPCGEN) $(RPCGENFLAGS) -m -o $@ $(RPCFILE)

$(RPCFILE_XDR): $(RPCFILE)
	$(RM) $@
	$(RPCGEN) $(RPCGENFLAGS) -c -o $@ $(RPCFILE)

%.o: ../common/%.c
	$(COMPILE.c) -o $@ $<

%.o: ../common/%.cc
	$(COMPILE.cc) -o $@ $<

%.ll:	../common/%.c
	$(WLCC) $(CFLAGS) $(CPPFLAGS) -o $@ $<

include ../../Makefile.targ
