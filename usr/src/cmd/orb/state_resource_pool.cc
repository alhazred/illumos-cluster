//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)state_resource_pool.cc	1.9	08/05/20 SMI"

#include "state_resource_pool.h"
#include <orb/flow/resource_defs.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/clconf_int.h>

#ifndef _KERNEL_ORB
#include <errno.h>
#include <sys/os.h>
#include <sys/cladm_int.h>
#endif	// _KERNEL_ORB

state_resource_pool	state_resource_pool::the_state_resource_pool;

//
// These enum values correspond to the array indexes for
// state_resource_pool_opts
//
enum {STATE_RESOURCE_POOL_USAGE,
	STATE_RESOURCE_POOL_ALL,
	STATE_RESOURCE_POOL_POOL,
	STATE_RESOURCE_POOL_TOTAL,
	STATE_RESOURCE_POOL_FREE,
	STATE_RESOURCE_POOL_NEEDED,
	STATE_RESOURCE_POOL_TORECALL,
	STATE_RESOURCE_POOL_TORELEASE,
	STATE_RESOURCE_POOL_ACTIVE
};

static char	*state_resource_pool_opts[] = {
	"?",			// Provide option usage
	"all",			// Print all statistics
	"pool",
	"total",
	"free",
	"needed",
	"torecall",
	"torelease",
	"active",
	NULL
};

state_resource_pool::state_resource_pool()
{
	init();
}

//
// usage - explain options for resource_pool state
//
// static
void
state_resource_pool::usage()
{
	(void) fprintf(stderr,
	    "resource_pool state requires options:\n");
	(void) fprintf(stderr,
	    "\tpool=xxx - specify resource_pool number\n");
	(void) fprintf(stderr,
	    "\t\tif not specified use DEFAULT_POOL\n");
	(void) fprintf(stderr,
	    "\tall - display all resource_pool state options\n");
	(void) fprintf(stderr,
	    "\ttotal - total number of threads\n");
	(void) fprintf(stderr,
	    "\tfree - number of free threads\n");
	(void) fprintf(stderr,
	    "\tneeded - number of needed threads\n");
	(void) fprintf(stderr,
	    "\ttorecall - number of threads to recall\n");
	(void) fprintf(stderr,
	    "\ttorelease - number of threads to release\n");
	(void) fprintf(stderr,
	    "\tactive - number of active threads\n"
	    "\t\tconvenience function for (total - free)\n");
}

//
// init - initialize class
//
void
state_resource_pool::init()
{
	// Initialize to no statistics selected
	stats_selected = 0;

	// Use this resource_pool by default.
	state.pool = resource_defs::DEFAULT_POOL;
}

//
// parse_options - resource_pool state was requested.
// Now determine the options.
// There will always be at least one option.
//
// Returns true upon successfully parsing valid options.
//
bool
state_resource_pool::parse_options(char *optionp)
{
	bool	options_ok = true;
	char	*options = optionp;
	char	*value;

	init();

	while (options_ok && *options != '\0') {
		switch (getsubopt(&options, state_resource_pool_opts, &value)) {
		case STATE_RESOURCE_POOL_USAGE:
			options_ok = false;
			break;

		case STATE_RESOURCE_POOL_ALL:
			stats_selected |= ALL_STATS;
			break;

		case STATE_RESOURCE_POOL_POOL:
			if (value == NULL) {
				(void) fprintf(stderr,
				    "No value for resource_pool\n");
				options_ok = false;
			} else {
				state.pool = atoi(value);
				if (state.pool < 0 ||
				    state.pool >= resource_defs::NUMBER_POOLS) {
					(void) fprintf(stderr,
					    "Invalid pool %s\n",
					    value);
					options_ok = false;
				}
			}
			break;

		case STATE_RESOURCE_POOL_TOTAL:
			stats_selected |= TOTAL;
			break;

		case STATE_RESOURCE_POOL_FREE:
			stats_selected |= FREE;
			break;

		case STATE_RESOURCE_POOL_NEEDED:
			stats_selected |= NEEDED;
			break;

		case STATE_RESOURCE_POOL_TORECALL:
			stats_selected |= TORECALL;
			break;

		case STATE_RESOURCE_POOL_TORELEASE:
			stats_selected |= TORELEASE;
			break;

		case STATE_RESOURCE_POOL_ACTIVE:
			stats_selected |= ACTIVE;
			break;

		default:
			(void) fprintf(stderr,
			    "Unknown state resource_pool option: %s\n",
			    value);
			options_ok = false;
		}
	}
	if (!options_ok) {
		state_resource_pool::usage();
		return (false);
	}
	if (stats_selected == 0) {
		(void) fprintf(stderr, "Must specify a display option\n");
		state_resource_pool::usage();
		return (false);
	}
	return (true);
}

//
// display_state - the system prints selected state of
// the resource_pool subsystem.
//
int
state_resource_pool::display_state(int node_number)
{
	int		error;

	for (int node = 1; node <= NODEID_MAX; node++) {
		if (node_number == 0 || node_number == node) {
			state.node = node;

#if defined(_KERNEL_ORB)
			// This code runs only in unode kernels.
			error = cl_read_orb_state_pool(&state);
#else
			errno = 0;
			error = os::cladm(CL_CONFIG, CL_ORBADMIN_STATE_POOL,
			    &state);
			if (error != 0) {
				// cladm places the error result in errno
				error = errno;
			}
#endif	// defined(_KERNEL_ORB)

			if (error != 0) {
				(void) fprintf(stderr, "state_resource_pool::"
				    "display_state: error %s\n",
				    strerror(error));
				return (error);
			}
			//
			// Discard if node is currently not in cluster
			//
			if (state.node == NODEID_UNKNOWN) {
				if (node_number == node) {
					(void) printf("node %d not in "
					    "cluster\n", node);
				}
				continue;
			}
			//
			// Received resource_pool state for a node
			// in the cluster.
			//
			if (node_number == 0) {
				(void) printf("node number: %d\n", node);
			}
			if ((stats_selected & TOTAL) != 0) {
				(void) printf("thread_total: %d\n",
				    state.thread_total);
			}
			if ((stats_selected & FREE) != 0) {
				(void) printf("thread_free: %d\n",
				    state.thread_free);
			}
			if ((stats_selected & NEEDED) != 0) {
				(void) printf("thread_needed: %d\n",
				    state.thread_needed);
			}
			if ((stats_selected & TORECALL) != 0) {
				(void) printf("thread_torecall: %d\n",
				    state.thread_torecall);
			}
			if ((stats_selected & TORELEASE) != 0) {
				(void) printf("thread_torelease: %d\n",
				    state.thread_torelease);
			}
			if ((stats_selected & ACTIVE) != 0) {
				(void) printf("thread_active: %d\n",
				    state.thread_total - state.thread_free);
			}
		}
	}
	return (0);
}
