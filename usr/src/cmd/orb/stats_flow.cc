//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)stats_flow.cc	1.12	08/05/20 SMI"

#include "stats_flow.h"
#include <stdio.h>
#include <orb/flow/resource.h>

#ifdef _KERNEL_ORB
#include <orb/debug/orb_stats_mgr.h>
#else
#include <errno.h>
#include <sys/cladm_int.h>
#endif	// _KERNEL_ORB

stats_flow	stats_flow::the_stats_flow;

//
// These enum values correspond to the array indexes for stats_flow_opts
//
enum {STATS_FLOW_USAGE,
	STATS_FLOW_ALL,
	STATS_FLOW_POLICY,
	STATS_FLOW_BLOCK,
	STATS_FLOW_POOL,
	STATS_FLOW_BALANCER
};

static char	*stats_flow_opts[] = {
	"?",		// Provide option usage
	"all",		// Print all statistics
	"policy",	// statistics - msg_by_policy
	"block",	// statistics - blocked, unblocked, reject
	"pool",		// statistics - resource_pool actions
	"balancer",	// statistics - resource_balancer actions
	NULL
};

stats_flow::stats_flow()
{
	init();
}

//
// stats_flow_usage - explain options for message statistics
//
// static
void
stats_flow::usage()
{
	(void) fprintf(stderr,
	    "flow control stats requires at least one option:\n");
	(void) fprintf(stderr,
	    "\tall - display all flow control statistic options\n");
	(void) fprintf(stderr,
	    "\tpolicy - statistics - msg_by_policy\n");
	(void) fprintf(stderr,
	    "\tblock - statistics - blocked, unblocked, reject\n");
	(void) fprintf(stderr,
	    "\tpool - statistics - resource_pool actions\n");
	(void) fprintf(stderr,
	    "\tbalancer - statistics - resource_balancer actions\n");
}

//
// init - initialize class
//
void
stats_flow::init()
{
	// Initialize to no statistics selected
	stats_selected = 0;
	for (int i = 1; i <= NODEID_MAX; i++) {
		old_flow_stats[i].stats.init();
	}
}

//
// parse_stats_flow_options - flow control statistics were requested.
// Now determine the options.
// There will always be at least one option.
//
// Returns true upon successfully parsing valid options.
//
bool
stats_flow::parse_options(char *optionp)
{
	bool	options_ok = true;
	char	*options = optionp;
	char	*value;

	init();

	while (options_ok && *options != '\0') {
		switch (getsubopt(&options, stats_flow_opts, &value)) {
		case STATS_FLOW_USAGE:
			stats_flow::usage();
			options_ok = false;
			break;

		case STATS_FLOW_ALL:
			stats_selected |= ALL_STATS;
			break;

		case STATS_FLOW_POLICY:
			stats_selected |= POLICY;
			break;

		case STATS_FLOW_BLOCK:
			stats_selected |= BLOCK;
			break;

		case STATS_FLOW_POOL:
			stats_selected |= POOL;
			break;

		case STATS_FLOW_BALANCER:
			stats_selected |= BALANCER;
			break;

		default:
			(void) fprintf(stderr,
			    "Unknown flow control statistics option: %s\n",
			    value);
			stats_flow::usage();
			options_ok = false;
		}
	}
	return (options_ok);
}

//
// display_stats_flow - statistics for flow control are printed.
//
int
stats_flow::display_stats(int node_number)
{
	int		error;
	cl_flow_stats	new_flow_stats;

	for (nodeid_t node = 1; node <= NODEID_MAX; node++) {
		if (node_number == 0 || (nodeid_t)node_number == node) {
			new_flow_stats.nodeid = node;

#if defined(_KERNEL_ORB)
			// This code runs only in unode kernels.
			error = cl_read_orb_stats_flow(&new_flow_stats);
#else
			errno = 0;
			error = os::cladm(CL_CONFIG, CL_ORBADMIN_STATS_FLOW,
			    &new_flow_stats);
			if (error != 0) {
				// cladm places the error result in errno
				error = errno;
			}
#endif	// defined(_KERNEL_ORB)

			if (error != 0) {
				(void) fprintf(stderr,
				    "stats_flow::display_stats: error %s\n",
				    strerror(error));
				return (error);
			}
			//
			// Discard if node is currently not in cluster
			//
			if (new_flow_stats.nodeid == NODEID_UNKNOWN) {
				if ((nodeid_t)node_number == node) {
					(void) fprintf(stderr, "node %d not in "
					    "cluster\n", node);
				}
				continue;
			}
			//
			// Received flow stats for a node in the cluster.
			//
			if (node_number == 0) {
				(void) printf("node number: %d\n", node);
			}
			if ((stats_selected & POLICY) != 0) {
				(void) printf("msg_by_policy:");
				for (int i = 0;
				    i < resources::NUMBER_POLICIES; i++) {
					(void) printf(" %d",
					    new_flow_stats.
						stats.msg_by_policy[i] -
					    old_flow_stats[node].
						stats.msg_by_policy[i]);
				}
				(void) printf("\n");
			}
			if ((stats_selected & BLOCK) != 0) {
				(void) printf("blocked: %d\n",
				    new_flow_stats.stats.blocked -
				    old_flow_stats[node].stats.blocked);
				(void) printf("unblocked: %d\n",
				    new_flow_stats.stats.unblocked -
				    old_flow_stats[node].stats.unblocked);
				(void) printf("reject: %d\n",
				    new_flow_stats.stats.reject -
				    old_flow_stats[node].stats.reject);
			}
			if ((stats_selected & POOL) != 0) {
				(void) printf("pool_requests: %d\n",
				    new_flow_stats.stats.pool_requests -
				    old_flow_stats[node].stats.pool_requests);
				(void) printf("pool_grants: %d\n",
				    new_flow_stats.stats.pool_grants -
				    old_flow_stats[node].stats.pool_grants);
				(void) printf("pool_denies: %d\n",
				    new_flow_stats.stats.pool_denies -
				    old_flow_stats[node].stats.pool_denies);
				(void) printf("pool_recalls: %d\n",
				    new_flow_stats.stats.pool_recalls -
				    old_flow_stats[node].stats.pool_recalls);
				(void) printf("pool_releases: %d\n",
				    new_flow_stats.stats.pool_releases -
				    old_flow_stats[node].stats.pool_releases);
			}
			if ((stats_selected & BALANCER) != 0) {
				(void) printf("balancer_requests: %d\n",
				    new_flow_stats.stats.balancer_requests -
				    old_flow_stats[node].stats.
					balancer_requests);
				(void) printf("balancer_grants: %d\n",
				    new_flow_stats.stats.balancer_grants -
				    old_flow_stats[node].stats.balancer_grants);
				(void) printf("balancer_denies: %d\n",
				    new_flow_stats.stats.balancer_denies -
				    old_flow_stats[node].stats.balancer_denies);
				(void) printf("balancer_recalls: %d\n",
				    new_flow_stats.stats.balancer_recalls -
				    old_flow_stats[node].stats.
					balancer_recalls);
				(void) printf("balancer_releases: %d\n",
				    new_flow_stats.stats.balancer_releases -
				    old_flow_stats[node].stats.
					balancer_releases);
			}
			old_flow_stats[node] = new_flow_stats;
		}
	}
	return (0);
}
