/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _CMD_ORB_STATE_REFCOUNT_H
#define	_CMD_ORB_STATE_REFCOUNT_H

#pragma ident	"@(#)state_refcount.h	1.8	08/05/20 SMI"

#include <sys/inttypes.h>
#include <sys/boolean.h>
#include <orb/debug/orbadmin_state.h>

class state_refcount {
public:
	static state_refcount	&the();

	bool	parse_options(char *optionp);

	int	display_state(int node_number);

private:
	static void	usage();

	// Ensure that nobody else can create this object
	state_refcount();

	void		init();

	// There is only one instance of this class
	static state_refcount	the_state_refcount;

	//
	// Define the bit flags that control the display of message statistics
	//
	enum {ALL_STATS		= ~0,
		REF		= 0x01,
		ACK		= 0x02
	};

	// Record the selected statistics.
	int		stats_selected;

	state_refcnt	state;
};

#include "state_refcount_in.h"

#endif	/* _CMD_ORB_STATE_REFCOUNT_H */
