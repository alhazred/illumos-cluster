//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)read_tickstats.cc	1.6	08/05/20 SMI"


#include <stdlib.h>
#include <kstat.h>
#include <sys/os.h>
#include <orb/transport/tick_stat.h>
#include <math.h>

//
// reads and prints tick statistics of the various pathends
//
// usage: read_tickstats [-h] [-v]
// options:
//		-h	print out help
//		-v	verbose printout, computed quantities etc
//
//

// print out usage information
void
usage(char *name)
{
	os::printf("usage: %s [-h] [-v]\n", name);
}

static const char *tick_class		= TICK_STAT_CLASS;
static const char *tick_ks_class	= TICK_STAT_KS_CLASS;

void
print_generic_named_kstat(kstat_t *ks_p)
{
	kstat_named_t	*kn_p;	// named kstat pointer
	unsigned int 	i;

	for (i = 0, kn_p = KSTAT_NAMED_PTR(ks_p);
		(kn_p != NULL) && (i < ks_p->ks_ndata); i++, kn_p++) {
		os::printf("    %-30s ", kn_p->name);
		switch (kn_p->data_type) {
		case KSTAT_DATA_CHAR:
			os::printf(" %c\n", kn_p->value.c);
			break;
		case KSTAT_DATA_INT32:
			os::printf(" %d\n", kn_p->value.i32);
			break;
		case KSTAT_DATA_UINT32:
			os::printf(" %u\n", kn_p->value.ui32);
			break;
		case KSTAT_DATA_INT64:
			os::printf(" %lld\n", kn_p->value.i64);
			break;
		case KSTAT_DATA_UINT64:
			os::printf(" %llu\n", kn_p->value.ui64);
			break;
		default:
			os::printf(" unknown data type\n");
			break;
		}
	}

}

// print out fancier information here

void
print_tick_kstat(tick_kstat_t	*tks_p)
{
	double		average, variance, max_len, min_len;
	uint64_t	count;  // number of recorded counts
	double		sigma;	// standard deviation

	// turn everything into doubles and in ms
	average		= ((double)tks_p->tk_average.value.ui64)	/ 1e3;
	variance	= ((double)tks_p->tk_variance.value.ui64) 	/ 1e3;
	max_len		= ((double)tks_p->tk_max.value.ui64) 		/ 1e3;
	min_len		= ((double)tks_p->tk_min.value.ui64) 		/ 1e3;
	count		= tks_p->tk_count.value.ui64;

	if (count > 1) {
		// unbiased estimator of variance has prefactor n/(n-1)
		variance = variance / (double)(count - 1) * (double)count;
		sigma	= sqrt(variance); // standard deviation

		os::printf(" average: %12.4f ms, variance      %12.4f ms^2,"
			" sigma %10.4f ms\n", average, variance, sigma);
	}

	os::printf(" maximum: %12.4f ms, above average %12.4f ms, %5.2f%%\n",
		max_len, max_len - average,
		(max_len - average)/average * 100.0);

	os::printf(" minimum: %12.4f ms, below average %12.4f ms, %5.2f%%\n",
		min_len, average - min_len,
		(average - min_len)/average * 100.0);

	os::printf(" number of ticks recorded: %llu\n", count);
}

int
main(int argc, char **argv)
{
	kstat_ctl_t	*ks_ctl_p;	 // pointer to control structure
	kstat_t		*ks_p;		 // pointer to walk kstat chain
	kid_t		kcid;		 // kstat chain id
	int		opt;		 // option
	bool		verbose = false; // verbosity

	// Parse options..
	while ((opt = getopt(argc, argv, "vh")) != EOF) {
		switch (opt) {
		case 'v':
			verbose = true;
			break;
		case 'h':
			usage(argv[0]);
			os::printf(" prints out kernel tick stats"
				" for clock ticks, send ticks, and"
				" receive ticks.\n");
			exit(-1);
			break;
		default:
			usage(argv[0]);
			exit(-1);
		}
	}

	// get pointer to control structure
	ks_ctl_p = kstat_open();

	if (ks_ctl_p == NULL) {
		os::printf("kstat open failed!\n");
		exit(-1);
	}



	// walk chain of kstats
	for (ks_p = ks_ctl_p->kc_chain; ks_p != NULL; ks_p = ks_p->ks_next) {
		tick_kstat_t	*tks_p;		// pointer to tick kstat

		// get data from kernel
		kcid = kstat_read(ks_ctl_p, ks_p, NULL);
		if (kcid == -1) {
			os::printf("kstat read failed!\n");
			exit(-1);
		}

		// can safely skip over everything that is not a named kstat.
		if (ks_p->ks_type != KSTAT_TYPE_NAMED) {
			continue;
		}

		if (strstr(ks_p->ks_class, tick_class) == NULL) {
			continue;
		}

		// issue warning and exit if not right version
		if (os::strcmp(ks_p->ks_class, tick_ks_class) != 0) {
			os::printf("version error: user land version is "
				"%s, kernel version %s.\n",
				tick_ks_class, ks_p->ks_class);
			exit(-1);
		}

		os::printf("Module: %s \tName: %s\n",
			ks_p->ks_module, ks_p->ks_name);

		tks_p = (tick_kstat_t *)KSTAT_NAMED_PTR(ks_p);

		if (tks_p == NULL) {
			os::printf("cannot get to tick kstat!\n");
			exit(-1);
		}
		if (verbose) {
			// elaborate printout if requested
			print_tick_kstat(tks_p);
		} else {
			// should work for any kstat
			print_generic_named_kstat(ks_p);

		}
		os::printf("\n"); // separate modules by newline
	}

	return (0);
}
