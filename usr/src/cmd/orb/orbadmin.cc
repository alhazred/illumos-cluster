//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)orbadmin.cc	1.9	08/05/20 SMI"

#include "orbadmin.h"
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "stats_msg.h"
#include "stats_flow.h"
#include "stats_unref.h"
#include "flow_spec.h"
#include "state_unref_threadpool.h"
#include "state_refcount.h"
#include "state_resource_pool.h"
#include "state_resource_balancer.h"

//
// orb admin tool - this tool provides human access to selected orb functions.
// The tool provides the following:
//
// 1. Statistics can be obtained.
//
// 2. Flow control parameters can be read.
//
// 3. Various Flow control parameters can be changed.
//
// 4. State can be obtained.
//

orbadmin_tool	orbadmin_tool::the_orbadmin_tool;

//
// orbadmin_tool constructor
//
orbadmin_tool::orbadmin_tool()
{
	init();
}

//
// usage - explain command usage
//
// static
void
orbadmin_tool::usage()
{
	(void) fprintf(stderr,
	    "Usage information: orbadmin options [interval [count]]\n");
	(void) fprintf(stderr,
	    "\tinterval - command is repeated once each interval seconds.\n"
	    "\tcount - number of command iterations.\n"
	    "\t\tDefaults to performing command once\n"
	    "\t\twhen interval is not specified.\n"
	    "\t\tWhen interval is specified without a count,\n"
	    "\t\tthe command repeats forever.\n");
	(void) fprintf(stderr,
	    "\t\tstate cmds report current values each interval\n"
	    "\t\tstatistics cmds report current value first time,\n"
	    "\t\tand the difference on subsequent intervals.\n");
	(void) fprintf(stderr,
	    "Supported options are:\n");
	(void) fprintf(stderr,
	    "\tU - display state unref_threadpool\n");
	(void) fprintf(stderr,
	    "\tR options - display state refcount\n");
	(void) fprintf(stderr,
	    "\tP options - display state resource_pool\n");
	(void) fprintf(stderr,
	    "\tB options - display state resource_balancer\n");
	(void) fprintf(stderr,
	    "\tm options - display message statistics\n");
	(void) fprintf(stderr,
	    "\tf options - display flow control statistics\n");
	(void) fprintf(stderr,
	    "\tu options - display unref_threadpool statistics\n");
	(void) fprintf(stderr,
	    "\tc options - change flow control settings\n");
	(void) fprintf(stderr,
	    "\tr options - read flow control settings\n");
	(void) fprintf(stderr,
	    "\tn node_number - info related to destination node.\n"
	    "\t\tDefault provides info for all destination nodes.\n");
	(void) fprintf(stderr,
	    "\t? - this message\n");
	(void) fprintf(stderr,
	    "A '?' suboption results in option usage info\n");
	(void) fprintf(stderr,
	    "Multiple options in one command are supported.\n");
}

//
// parse_options - determines selected options.
// Suboptions are parsed by command specific routines.
//
// Returns true upon successfully parsing valid options.
//
bool
orbadmin_tool::parse_options(int argc, char *argv[])
{
	if (argc < 2) {
		(void) fprintf(stderr, "No arguments\n");
		usage();
		return (false);
	}

	int		opt_char;
	bool		options_ok = true;

#if defined(_KERNEL_ORB) && !defined(_KERNEL)
	//
	// The unode version of this command is loaded into a process once,
	// and then it can be executed multiple times.
	// The following value determines which argument function getopt
	// will start its search from. The system sets this value initially
	// to "1". On subsequent unode commands, we must do this manually.
	//
	optind = 1;
#endif

	while (options_ok &&
	    //
	    // getopt will generate an error upon encountering
	    // an option requiring suboptions that does not have suboptions.
	    //
	    (opt_char = getopt(argc, argv, "?UR:P:B:r:c:m:f:u:n:")) != EOF) {
		switch (opt_char) {
		case 'U':		// Display unref_threadpool state
			cmd_state_unref = true;
			break;

		case 'R':		// Display refcount state
			cmd_state_refcount = true;
			options_ok = state_refcount::the().
			    parse_options(optarg);
			break;

		case 'P':		// Display resource_pool state
			cmd_state_resource_pool = true;
			options_ok = state_resource_pool::the().
			    parse_options(optarg);
			break;

		case 'B':		// Display resource_balancer state
			cmd_state_resource_balancer = true;
			options_ok = state_resource_balancer::the().
			    parse_options(optarg);
			break;

		case 'r':		// Flow Control - read tuning specs
					// No option arguments
			cmd_flow_read = true;
			options_ok = flow_spec::the().
			    parse_read_options(optarg);
			break;

		case 'c':		// Flow Control - change tuning specs
			cmd_flow_change = true;
			options_ok = flow_spec::the().
			    parse_change_options(optarg);
			break;

		case 'm':		// Display orb message statistics
			cmd_stats_msg = true;
			options_ok = stats_msg::the().parse_options(optarg);
			break;

		case 'f':		// Display orb flow control statistics
			cmd_stats_flow = true;
			options_ok = stats_flow::the().parse_options(optarg);
			break;

		case 'u':		// Display orb unref_threadpool stats
			cmd_stats_unref = true;
			options_ok = stats_unref::the().parse_options(optarg);
			break;

		case 'n':		// Identify node to use with command
			node_number = atoi(optarg);
			if (node_number <= 0 ||
			    node_number > NODEID_MAX) {
				(void) fprintf(stderr,
				    "Invalid node number %d\n",
				    node_number);
				usage();
				interval_number = 0;
				options_ok = false;
			}
			break;

		case '?':
			usage();
			options_ok = false;
			break;

		default:
			(void) fprintf(stderr,
			    "Unknown options char: %c\n", optopt);
			usage();
			options_ok = false;
			break;
		}
	}
	// Determine whether user specified interval time
	if (optind < argc) {
		int	iter;
		if ((iter = atoi(argv[optind])) <= 0) {
			(void) fprintf(stderr,
			    "Invalid interval time in seconds: %d\n", iter);
			usage();
			options_ok = false;
		} else {
			interval_time = (uint_t)iter;
		}
		optind++;
	}
	// Determine whether user specified interval count
	if (options_ok && optind < argc) {
		if ((interval_number = atoi(argv[optind])) <= 0) {
			(void) fprintf(stderr,
			    "Invalid count: %d\n", interval_number);
			usage();
			options_ok = false;
		}
		optind++;
	}
	// Determine whether user specified too many arguments
	if (options_ok && optind < argc) {
		(void) fprintf(stderr, "Extra invalid arguments supplied\n");
		usage();
		options_ok = false;
	}
	return (options_ok);
}

//
// init - reset all orbadmin values
//
void
orbadmin_tool::init()
{
	cmd_stats_msg = false;
	cmd_stats_flow = false;
	cmd_stats_unref = false;
	cmd_flow_read = false;
	cmd_flow_change = false;
	cmd_state_unref = false;
	cmd_state_refcount = false;
	cmd_state_resource_pool = false;
	cmd_state_resource_balancer = false;

	// A negative number means that there is no limit
	// on the number of times the command is executed.
	interval_number = -1;

	// Show no interval specified.
	interval_time = 0;

	// There is no node_number zero. Use zero to represent all nodes.
	node_number = 0;
}

//
// Entry point for unode or user level version of the orbadmin tool
//
int
#if !defined(_KERNEL) && defined(_KERNEL_ORB)
orbadmin(int argc, char *argv[])
#else
main(int argc, char *argv[])
#endif // !_KERNEL && _KERNEL_ORB
{
	return (orbadmin_tool::the().main_program(argc, argv));
}

//
// Top level control module for the orbadmin tool
//
int
orbadmin_tool::main_program(int argc, char *argv[])
{
#ifndef _KERNEL_ORB
	setbuf(stdout, NULL);
	setbuf(stderr, NULL);
#endif

	int	result;

	init();

	if (!parse_options(argc, argv)) {
		// Did not find a valid command
		return (EINVAL);
	}
	if (interval_time == 0) {
		//
		// Command did not specify interval time.
		// In this case the command will be issued once.
		//
		interval_number = 1;
	}

	if (interval_number > 1 && cmd_flow_change) {
		(void) fprintf(stderr,
		    "Cannot change flow control settings in a loop\n");
		return (EINVAL);
	}

	//
	// Repeat command forever when the interval number is negative.
	//
	for (int i = 0; i < interval_number || interval_number == -1; i++) {
		//
		// Do not sleep the first time through the loop.
		// Also do not sleep after last command issued.
		//
		if (i != 0 && interval_time > 0) {
			(void) sleep(interval_time);
		}

		if (cmd_state_unref) {
			// Display unref_threadpool state
			result = state_unref_threadpool::the().display_state();
			if (result != 0) {
				return (result);
			}
		}

		if (cmd_state_refcount) {
			// Display refcount state
			result = state_refcount::the().
			    display_state(node_number);
			if (result != 0) {
				return (result);
			}
		}

		if (cmd_state_resource_pool) {
			// Display resource_pool state
			result = state_resource_pool::the().
			    display_state(node_number);
			if (result != 0) {
				return (result);
			}
		}

		if (cmd_state_resource_balancer) {
			// Display resource_balancer state
			result = state_resource_balancer::the().display_state();
			if (result != 0) {
				return (result);
			}
		}

		if (cmd_stats_msg) {
			// Display orb message statistics
			result = stats_msg::the().display_stats(node_number);
			if (result != 0) {
				return (result);
			}
		}

		if (cmd_stats_flow) {
			// Display orb flow control statistics
			result = stats_flow::the().display_stats(node_number);
			if (result != 0) {
				return (result);
			}
		}

		if (cmd_stats_unref) {
			// Display orb unref_threadpool statistics
			result = stats_unref::the().display_stats();
			if (result != 0) {
				return (result);
			}
		}

		if (cmd_flow_read) {
			// Show flow control settings
			result = flow_spec::the().read_specs();
			if (result != 0) {
				return (result);
			}
		}

		if (cmd_flow_change) {
			// Change flow control settings
			result = flow_spec::the().change_specs();
			if (result != 0) {
				return (result);
			}
		}
	}
	return (0);
}

#if defined(_KERNEL_ORB) && !defined(_KERNEL)
//
// unode_init(void)
//
int
unode_init(void)
{
	(void) fprintf(stderr, "orbadmin UNODE module loaded\n");
	return (0);
}
#endif	// defined(_KERNEL_ORB) && !defined(_KERNEL)
