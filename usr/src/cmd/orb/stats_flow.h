/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _CMD_ORB_STATS_FLOW_H
#define	_CMD_ORB_STATS_FLOW_H

#pragma ident	"@(#)stats_flow.h	1.8	08/05/20 SMI"

#include <sys/inttypes.h>
#include <sys/boolean.h>
#include <orb/flow/flow_stats.h>

class stats_flow {
public:
	static stats_flow	&the();

	bool	parse_options(char *optionp);

	int	display_stats(int node_number);

private:
	static void	usage();

	// Ensure that nobody else can create this object
	stats_flow();

	void		init();

	// There is only one instance of this class
	static stats_flow	the_stats_flow;

	//
	// Define the bit flags that control the display of message statistics
	//
	enum {ALL_STATS		= ~0,
		POLICY		= 0x01,
		BLOCK		= 0x02,
		POOL		= 0x04,
		BALANCER	= 0x08
	};

	// Record the selected statistics.
	int	stats_selected;

	//
	// Records the latest message statistics.
	// This is used to compute differences for a time interval.
	//
	cl_flow_stats	old_flow_stats[NODEID_MAX + 1];
};

#include "stats_flow_in.h"

#endif	/* _CMD_ORB_STATS_FLOW_H */
