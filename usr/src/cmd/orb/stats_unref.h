/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _CMD_ORB_STATS_UNREF_H
#define	_CMD_ORB_STATS_UNREF_H

#pragma ident	"@(#)stats_unref.h	1.9	08/05/20 SMI"

#include <sys/inttypes.h>
#include <sys/boolean.h>
#include <orb/refs/unref_threadpool.h>

class stats_unref {
public:
	static stats_unref	&the();

	bool	parse_options(char *optionp);

	int	display_stats();

private:
	static void	usage();

	// Ensure that nobody else can create this object
	stats_unref();

	void		init();

	// There is only one instance of this class
	static stats_unref	the_stats_unref;

	//
	// Define the bit flags that control the display of message statistics
	//
	enum {ALL_STATS			= ~0,
		WAIT_HANDLER		= 0x01,
		WAIT_XDOOR		= 0x02,
		QUEUE_LENGTH		= 0x04,
		WORK_HANDLER		= 0x08,
		WORK_XDOOR		= 0x10
	};

	// Record the selected statistics.
	int	stats_selected;

	//
	// Records the latest unref statistics.
	// This is used to compute differences for a time interval.
	//
	unref_stats	old_unref_stats;

	unref_stats	new_unref_stats;
};

#include "stats_unref_in.h"

#endif	/* _CMD_ORB_STATS_UNREF_H */
