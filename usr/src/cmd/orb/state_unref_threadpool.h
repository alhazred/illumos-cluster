/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _CMD_ORB_STATE_UNREF_THREADPOOL_H
#define	_CMD_ORB_STATE_UNREF_THREADPOOL_H

#pragma ident	"@(#)state_unref_threadpool.h	1.6	08/05/20 SMI"

#include <orb/debug/orbadmin_state.h>

class state_unref_threadpool {
public:
	static state_unref_threadpool	&the();

	int	display_state();

private:
	// Ensure that nobody else can create this object
	state_unref_threadpool();

	// There is only one instance of this class
	static state_unref_threadpool	the_state_unref_threadpool;

	state_unref	state;
};

#include "state_unref_threadpool_in.h"

#endif	/* _CMD_ORB_STATE_UNREF_THREADPOOL_H */
