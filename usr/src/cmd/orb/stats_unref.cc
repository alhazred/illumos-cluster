//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)stats_unref.cc	1.12	08/05/20 SMI"

#include "stats_unref.h"
#include <stdio.h>

#ifdef _KERNEL_ORB
#include <orb/refs/unref_stats.h>
#else
#include <errno.h>
#include <sys/clconf_int.h>
#include <sys/cladm_int.h>
#endif	// _KERNEL_ORB

stats_unref	stats_unref::the_stats_unref;

//
// These enum values correspond to the array indexes for stats_unref_opts
//
enum {STATS_UNREF_USAGE,
	STATS_UNREF_ALL,
	STATS_UNREF_WAIT_HANDLER,
	STATS_UNREF_WAIT_XDOOR,
	STATS_UNREF_QUEUE_LENGTH,
	STATS_UNREF_WORK_HANDLER,
	STATS_UNREF_WORK_XDOOR
};

static char	*stats_unref_opts[] = {
	"?",			// Provide option usage
	"all",			// Print all statistics
	"hist_wait_handler",
	"hist_wait_xdoor",
	"hist_queue_length",
	"hist_work_handler",
	"hist_work_xdoor",
	NULL
};

stats_unref::stats_unref()
{
	init();
}

//
// usage - explain options for unref statistics
//
// static
void
stats_unref::usage()
{
	(void) fprintf(stderr,
	    "unref threadpool stats requires at least one option:\n");
	(void) fprintf(stderr, "\tall - display all unref statistic options\n");
	(void) fprintf(stderr, "\thist_wait_handler\n");
	(void) fprintf(stderr, "\thist_wait_xdoor\n");
	(void) fprintf(stderr, "\thist_queue_length\n");
	(void) fprintf(stderr, "\thist_work_handler\n");
	(void) fprintf(stderr, "\thist_work_xdoor\n");
	(void) fprintf(stderr,
	    "unref threadpool statistics software is optional,\n");
	(void) fprintf(stderr,
	    "and the cmd reports an error when not supported\n");
}

//
// init - initialize class
//
void
stats_unref::init()
{
	// Initialize to no statistics selected
	stats_selected = 0;

	old_unref_stats.init();
	new_unref_stats.init();
}

//
// parse_options - unref statistics were requested.
// Now determine the options.
// There will always be at least one option.
//
// Returns true upon successfully parsing valid options.
//
bool
stats_unref::parse_options(char *optionp)
{
	bool	options_ok = true;
	char	*options = optionp;
	char	*value;

	init();

	while (options_ok && *options != '\0') {
		switch (getsubopt(&options, stats_unref_opts, &value)) {
		case STATS_UNREF_USAGE:
			stats_unref::usage();
			options_ok = false;
			break;

		case STATS_UNREF_ALL:
			stats_selected |= ALL_STATS;
			break;

		case STATS_UNREF_WAIT_HANDLER:
			stats_selected |= WAIT_HANDLER;
			break;

		case STATS_UNREF_WAIT_XDOOR:
			stats_selected |= WAIT_XDOOR;
			break;

		case STATS_UNREF_QUEUE_LENGTH:
			stats_selected |= QUEUE_LENGTH;
			break;

		case STATS_UNREF_WORK_HANDLER:
			stats_selected |= WORK_HANDLER;
			break;

		case STATS_UNREF_WORK_XDOOR:
			stats_selected |= WORK_XDOOR;
			break;

		default:
			(void) fprintf(stderr,
			    "Unknown unref statistics option: %s\n",
			    value);
			stats_unref::usage();
			options_ok = false;
		}
	}
	return (options_ok);
}

//
// display_stats - statistics on unref_threadpool activity during the
// latest period are printed. Note that the first time this executes,
// it will print out the current values.
//
int
stats_unref::display_stats()
{
	int		error;

#if defined(_KERNEL_ORB)
	// This code runs only in unode kernels.
	error = cl_read_orb_stats_unref(&new_unref_stats);
#else
	errno = 0;
	error = os::cladm(CL_CONFIG, CL_ORBADMIN_STATS_UNREF, &new_unref_stats);
	if (error != 0) {
		// cladm places the error result in errno
		error = errno;
	}
#endif	// defined(_KERNEL_ORB)

	if (error != 0) {
		(void) fprintf(stderr, "stats_unref::display_stats: error %s\n",
		    strerror(error));
		return (error);
	}

	if ((stats_selected & WAIT_HANDLER) != 0) {
		(void) printf("hist_wait_handler:");
		for (int i = 0; i < unref_stats::HIST_SIZE; i++) {
			(void) printf(" %d",
			    new_unref_stats.hist_wait_handler[i] -
			    old_unref_stats.hist_wait_handler[i]);
		}
		(void) printf("\n");
	}
	if ((stats_selected & WAIT_XDOOR) != 0) {
		(void) printf("hist_wait_xdoor:");
		for (int i = 0; i < unref_stats::HIST_SIZE; i++) {
			(void) printf(" %d",
			    new_unref_stats.hist_wait_xdoor[i] -
			    old_unref_stats.hist_wait_xdoor[i]);
		}
		(void) printf("\n");
	}
	if ((stats_selected & QUEUE_LENGTH) != 0) {
		(void) printf("hist_queue_length:");
		for (int i = 0; i < unref_stats::HIST_SIZE; i++) {
			(void) printf(" %d",
			    new_unref_stats.hist_queue_length[i] -
			    old_unref_stats.hist_queue_length[i]);
		}
		(void) printf("\n");
	}
	if ((stats_selected & WORK_HANDLER) != 0) {
		(void) printf("hist_work_handler:");
		for (int i = 0; i < unref_stats::HIST_SIZE; i++) {
			(void) printf(" %d",
			    new_unref_stats.hist_work_handler[i] -
			    old_unref_stats.hist_work_handler[i]);
		}
		(void) printf("\n");
	}
	if ((stats_selected & WORK_XDOOR) != 0) {
		(void) printf("hist_work_xdoor:");
		for (int i = 0; i < unref_stats::HIST_SIZE; i++) {
			(void) printf(" %d",
			    new_unref_stats.hist_work_xdoor[i] -
			    old_unref_stats.hist_work_xdoor[i]);
		}
		(void) printf("\n");
	}
	old_unref_stats = new_unref_stats;

	return (0);
}
