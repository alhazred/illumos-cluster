//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)flow_spec.cc	1.12	08/05/20 SMI"

#include "flow_spec.h"
#include <stdlib.h>
#include <stdio.h>
#include <orb/flow/resource_defs.h>

#ifdef _KERNEL_ORB
#include <orb/flow/resource_balancer.h>
#else
#include <errno.h>
#include <sys/cladm_int.h>
#endif

flow_spec	flow_spec::the_flow_spec;

//
// These enum values correspond to the array indexes for flow_change_opts
//
enum {FLOW_CHANGE_USAGE,
	FLOW_CHANGE_POOL,
	FLOW_CHANGE_LOW,
	FLOW_CHANGE_MODERATE,
	FLOW_CHANGE_HIGH,
	FLOW_CHANGE_MIN
};

static char	*flow_change_opts[] = {
	"?",		// Provide option usage
	"pool",		// Identify resource_pool
	"low",		// New value for threads low
	"mod",		// New value for threads moderate
	"high",		// New value for threads high
	"min",		// New value for threads minimum
	NULL
};

//
// These enum values correspond to the array indexes for flow_read_opts
//
enum {FLOW_READ_USAGE,
	FLOW_READ_POOL,
	FLOW_READ_ALL
};

static char	*flow_read_opts[] = {
	"?",		// Provide option usage
	"pool",		// Identify resource_pool
	"all",
	NULL
};

flow_spec::flow_spec()
{
	init();
}

//
// change_usage - explain options for changing flow control settings
//
// static
void
flow_spec::change_usage()
{
	(void) fprintf(stderr, "flow control change options:\n");
	(void) fprintf(stderr, "\tpool=xxx - resource_pool number\n");
	(void) fprintf(stderr, "\t\t defaults to DEFAULT_POOL\n");
	(void) fprintf(stderr, "\tlow=xxx - new value for threads low\n");
	(void) fprintf(stderr, "\tmod=xxx - new value for threads moderate\n");
	(void) fprintf(stderr, "\thigh=xxx - new value for threads high\n");
	(void) fprintf(stderr, "\tmin=xxx - new value for threads minimum\n");
	(void) fprintf(stderr, "Any unspecified thread value remains unchanged"
	    ".\n");
}

//
// read_usage - explain options for reading flow control settings
//
// static
void
flow_spec::read_usage()
{
	(void) fprintf(stderr, "flow control read options:\n");
	(void) fprintf(stderr, "\tpool=xxx - resource_pool number\n");
	(void) fprintf(stderr, "\t\t defaults to DEFAULT_POOL\n");
	(void) fprintf(stderr, "\tall\n");
}

//
// init - initialize class
//
void
flow_spec::init()
{
	new_flow_specs.threads_low = 0;
	new_flow_specs.threads_moderate = 0;
	new_flow_specs.threads_high = 0;

	new_flow_min.threads_minimum = 0;
}

//
// parse_change_options - a change of flow control settings was requested.
// Now determine the options.
// There will always be at least one option.
//
// Returns true upon successfully parsing valid options.
//
bool
flow_spec::parse_change_options(char *optionp)
{
	bool	options_ok = true;
	char	*options = optionp;
	char	*value;

	init();

	while (options_ok && *options != '\0') {
		switch (getsubopt(&options, flow_change_opts, &value)) {
		case FLOW_CHANGE_USAGE:
			flow_spec::change_usage();
			options_ok = false;
			break;

		case FLOW_CHANGE_POOL:
			if (value == NULL) {
				(void) fprintf(stderr, "No value for pool\n");
				options_ok = false;
			} else {
				int pool_number = atoi(value);
				if (pool_number < 0 || pool_number >=
				    resource_defs::NUMBER_POOLS) {
					(void) fprintf(stderr,
					    "Invalid pool %s\n", value);
					options_ok = false;
				} else {
					new_flow_specs.pool =
					    (resource_defs::resource_pool_t)
					    pool_number;

					old_flow_specs.pool =
					    old_flow_min.pool =
					    new_flow_min.pool =
					    new_flow_specs.pool;
				}
			}
			break;

		case FLOW_CHANGE_LOW:
			if (value == NULL) {
				(void) fprintf(stderr, "No value for low\n");
				options_ok = false;
			} else {
				new_flow_specs.threads_low = atoi(value);
				if (new_flow_specs.threads_low <= 0) {
					(void) fprintf(stderr,
					    "Invalid new low %s\n", value);
					options_ok = false;
				}
			}
			break;

		case FLOW_CHANGE_MODERATE:
			if (value == NULL) {
				(void) fprintf(stderr,
				    "No value for moderate\n");
				options_ok = false;
			} else {
				new_flow_specs.threads_moderate = atoi(value);
				if (new_flow_specs.threads_moderate <= 0) {
					(void) fprintf(stderr,
					    "Invalid new moderate %s\n", value);
					options_ok = false;
				}
			}
			break;

		case FLOW_CHANGE_HIGH:
			if (value == NULL) {
				(void) fprintf(stderr, "No value for high\n");
				options_ok = false;
			} else {
				new_flow_specs.threads_high = atoi(value);
				if (new_flow_specs.threads_high <= 0) {
					(void) fprintf(stderr,
					    "Invalid new high %s\n", value);
					options_ok = false;
				}
			}
			break;

		case FLOW_CHANGE_MIN:
			if (value == NULL) {
				(void) fprintf(stderr, "No value for min\n");
				options_ok = false;
			} else {
				new_flow_min.threads_minimum = atoi(value);
				if (new_flow_min.threads_minimum <= 0) {
					(void) fprintf(stderr,
					    "Invalid new minimum %s\n", value);
					options_ok = false;
				}
			}
			break;

		default:
			(void) fprintf(stderr,
			    "Unknown flow control change option: %s\n", value);
			flow_spec::change_usage();
			options_ok = false;
		}
	}
	return (options_ok);
}

//
// parse_read_options - requested reading flow control settings.
// Now determine the options.
// There will always be at least one option.
//
// Returns true upon successfully parsing valid options.
//
bool
flow_spec::parse_read_options(char *optionp)
{
	bool	options_ok = true;
	char	*options = optionp;
	char	*value;

	init();

	while (options_ok && *options != '\0') {
		switch (getsubopt(&options, flow_read_opts, &value)) {
		case FLOW_READ_USAGE:
			flow_spec::read_usage();
			options_ok = false;
			break;

		case FLOW_READ_POOL:
			if (value == NULL) {
				(void) fprintf(stderr, "No value for pool\n");
				options_ok = false;
			} else {
				int pool_number = atoi(value);
				if (pool_number < 0 || pool_number >=
				    resource_defs::NUMBER_POOLS) {
					(void) fprintf(stderr,
					    "Invalid pool %s\n", value);
					options_ok = false;
				} else {
					new_flow_specs.pool =
					    (resource_defs::resource_pool_t)
					    pool_number;
					old_flow_specs.pool =
					    old_flow_min.pool =
					    new_flow_min.pool =
					    new_flow_specs.pool;
				}
			}
			break;

		case FLOW_READ_ALL:
			// Currently always read all options
			break;

		default:
			(void) fprintf(stderr,
			    "Unknown flow control read option: %s\n", value);
			flow_spec::read_usage();
			options_ok = false;
		}
	}
	return (options_ok);
}

//
// print_specs - output flow control information
//
// static
void
flow_spec::print_specs(cl_flow_specs &flow_specs,
    cl_flow_threads_min &flow_min)
{
	(void) printf("pool: %d\n", flow_specs.pool);
	(void) printf("number_nodes: %d\n", flow_specs.number_nodes);
	(void) printf("threads_low: %d\n", flow_specs.threads_low);
	(void) printf("threads_moderate: %d\n", flow_specs.threads_moderate);
	(void) printf("threads_high: %d\n", flow_specs.threads_high);
	(void) printf("threads_increment: %d\n", flow_specs.threads_increment);
	(void) printf("threads_minimum: %d\n", flow_min.threads_minimum);
}

//
// read_specs - the flow control policy and threads minimum
// settings are read and displayed.
//
int
flow_spec::read_specs()
{
	int		error;

#if defined(_KERNEL_ORB)
	// This code runs only in unode kernels.
	error = cl_read_threads_min(&old_flow_min);
#else
	errno = 0;
	error = os::cladm(CL_CONFIG, CL_ORBADMIN_THREADS_MIN_READ,
	    &old_flow_min);
	if (error != 0) {
		// cladm places the error result in errno
		error = errno;
	}
#endif	// defined(_KERNEL_ORB)

	if (error != 0) {
		(void) fprintf(stderr,
		    "flow_spec::read_specs: threads_minimum error %s\n",
		    strerror(error));
		return (error);
	}

#if defined(_KERNEL_ORB)
	// This code runs only in unode kernels.
	error = cl_read_flow_settings(&old_flow_specs);
#else
	errno = 0;
	error = os::cladm(CL_CONFIG, CL_ORBADMIN_FLOW_READ, &old_flow_specs);
	if (error != 0) {
		// cladm places the error result in errno
		error = errno;
	}
#endif	// defined(_KERNEL_ORB)

	if (error != 0) {
		(void) fprintf(stderr,
		    "flow_spec::read_specs: error %s\n",
		    strerror(error));
		return (error);
	}

	print_specs(old_flow_specs, old_flow_min);
	return (0);
}

//
// change_specs - The existing flow control
// policy and threads minimum settings are read and
// displayed. The old flow control settings are used for any field not
// specified in the command. Then these values are changed.
//
int
flow_spec::change_specs()
{
	int		error;

#if defined(_KERNEL_ORB)
	// This code runs only in unode kernels.
	error = cl_read_threads_min(&old_flow_min);
#else
	errno = 0;
	error = os::cladm(CL_CONFIG, CL_ORBADMIN_THREADS_MIN_READ,
	    &old_flow_min);
	if (error != 0) {
		// cladm places the error result in errno
		error = errno;
	}
#endif	// defined(_KERNEL_ORB)

	if (error != 0) {
		(void) fprintf(stderr,
		    "flow_spec::change_specs: read threads_minimum error %s\n",
		    strerror(error));
		return (error);
	}

#if defined(_KERNEL_ORB)
	// This code runs only in unode kernels.
	error = cl_read_flow_settings(&old_flow_specs);
#else
	errno = 0;
	error = os::cladm(CL_CONFIG, CL_ORBADMIN_FLOW_READ, &old_flow_specs);
	if (error != 0) {
		// cladm places the error result in errno
		error = errno;
	}
#endif	// defined(_KERNEL_ORB)

	if (error != 0) {
		(void) fprintf(stderr,
		    "flow_spec::change_specs read error %s\n",
		    strerror(error));
		return (error);
	}

	//
	// Any threads_minimum change must occur prior to policy changes.
	// Only change threads minimum value when requested
	//
	if (new_flow_min.threads_minimum == 0) {
		//
		// No change.
		//
		new_flow_min.threads_minimum = old_flow_min.threads_minimum;
	} else {
#if defined(_KERNEL_ORB)
		// This code runs only in unode kernels.
		error = cl_change_threads_min(&new_flow_min);
#else
		errno = 0;
		error = os::cladm(CL_CONFIG, CL_ORBADMIN_THREADS_MIN_CHANGE,
		    &new_flow_min);
		if (error != 0) {
			// cladm places the error result in errno
			error = errno;
		}
#endif	// defined(_KERNEL_ORB)

		if (error != 0) {
			(void) fprintf(stderr,
			    "flow_spec::change_specs change threads_minimum"
			    " error %s\n",
			    strerror(error));
			return (error);
		}

		//
		// The system limits how low threads_minimum can be set.
		// Read the value to see what actually resulted.
		//
#if defined(_KERNEL_ORB)
		// This code runs only in unode kernels.
		error = cl_read_threads_min(&new_flow_min);
#else
		errno = 0;
		error = os::cladm(CL_CONFIG, CL_ORBADMIN_THREADS_MIN_READ,
		    &new_flow_min);
		if (error != 0) {
			// cladm places the error result in errno
			error = errno;
		}
#endif	// defined(_KERNEL_ORB)

		if (error != 0) {
			(void) fprintf(stderr, "flow_spec::change_specs: "
			    "read threads_minimum error %s\n",
			    strerror(error));
			return (error);
		}
	}

	//
	// Use existing values to fill in any value not specified in command
	//
	new_flow_specs.number_nodes = old_flow_specs.number_nodes;
	if (new_flow_specs.threads_low == 0) {
		new_flow_specs.threads_low = old_flow_specs.threads_low;
	}
	if (new_flow_specs.threads_moderate == 0) {
		new_flow_specs.threads_moderate =
		    old_flow_specs.threads_moderate;
	}
	if (new_flow_specs.threads_high == 0) {
		new_flow_specs.threads_high = old_flow_specs.threads_high;
	}
	// Currently cannot change threads increment level
	new_flow_specs.threads_increment = old_flow_specs.threads_increment;

#if defined(_KERNEL_ORB)
	// This code runs only in unode kernels.
	error = cl_change_flow_settings(&new_flow_specs);
#else
	errno = 0;
	error = os::cladm(CL_CONFIG, CL_ORBADMIN_FLOW_CHANGE, &new_flow_specs);
	if (error != 0) {
		// cladm places the error result in errno
		error = errno;
	}
#endif	// defined(_KERNEL_ORB)

	if (error != 0) {
		(void) fprintf(stderr,
		    "flow_spec::change_specs change error %s\n",
		    strerror(error));
		return (error);
	}

	(void) printf("Old flow control settings:\n");
	print_specs(old_flow_specs, old_flow_min);

	(void) printf("new flow control settings:\n");
	print_specs(new_flow_specs, new_flow_min);
	return (0);
}
