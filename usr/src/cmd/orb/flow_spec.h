/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _CMD_ORB_FLOW_SPEC_H
#define	_CMD_ORB_FLOW_SPEC_H

#pragma ident	"@(#)flow_spec.h	1.9	08/05/20 SMI"

#include <sys/inttypes.h>
#include <sys/boolean.h>
#include <orb/flow/resource_balancer.h>

class flow_spec {
public:
	static flow_spec	&the();

	bool	parse_read_options(char *optionp);
	bool	parse_change_options(char *optionp);

	int	read_specs();
	int	change_specs();

private:
	static void	read_usage();
	static void	change_usage();
	static void	print_specs(cl_flow_specs &flow_specs,
			    cl_flow_threads_min &flow_min);

	// Ensure that nobody else can create this object
	flow_spec();

	void		init();

	// There is only one instance of this class
	static flow_spec	the_flow_spec;

	cl_flow_specs		old_flow_specs;
	cl_flow_specs		new_flow_specs;

	cl_flow_threads_min	old_flow_min;
	cl_flow_threads_min	new_flow_min;
};

#include "flow_spec_in.h"

#endif	/* _CMD_ORB_FLOW_SPEC_H */
