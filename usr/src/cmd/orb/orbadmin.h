/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _CMD_ORB_ORBADMIN_H
#define	_CMD_ORB_ORBADMIN_H

#pragma ident	"@(#)orbadmin.h	1.5	08/05/20 SMI"

#include <sys/types.h>
#include <sys/inttypes.h>
#include <sys/boolean.h>

class orbadmin_tool {
public:
	static orbadmin_tool	&the();

	int		main_program(int argc, char *argv[]);

private:
	// Ensure that nobody else can create this object
	orbadmin_tool();

	void		init();

	bool		parse_interval_options(char *optionp);
	bool		parse_node_options(char *optionp);
	bool		parse_options(int argc, char *argv[]);

	static void	usage();

	// There is only one instance of this class
	static orbadmin_tool	the_orbadmin_tool;

	//
	// Each of the following values is set to true if that command
	// has been requested.
	//
	bool	cmd_stats_msg;
	bool	cmd_stats_flow;
	bool	cmd_stats_unref;
	bool	cmd_flow_read;
	bool	cmd_flow_change;
	bool	cmd_state_unref;
	bool	cmd_state_refcount;
	bool	cmd_state_resource_pool;
	bool	cmd_state_resource_balancer;

	//
	// These values control the number of iterations and the
	// time ins seconds between iterations.
	//
	int	interval_number;
	uint_t	interval_time;

	//
	// Real nodes are numbered starting from 1.
	// Thus this program will use zero to represent all nodes.
	//
	int	node_number;
};

#include "orbadmin_in.h"

#endif	/* _CMD_ORB_ORBADMIN_H */
