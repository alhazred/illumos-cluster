/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _CMD_ORB_STATS_MSG_H
#define	_CMD_ORB_STATS_MSG_H

#pragma ident	"@(#)stats_msg.h	1.8	08/05/20 SMI"

#include <sys/inttypes.h>
#include <sys/boolean.h>
#include <orb/msg/msg_stats.h>

class stats_msg {
public:
	static stats_msg	&the();

	bool	parse_options(char *optionp);

	int	display_stats(int node_number);

private:
	static void	usage();

	// Ensure that nobody else can create this object
	stats_msg();

	void		init();

	// There is only one instance of this class
	static stats_msg	the_stats_msg;

	//
	// Define the bit flags that control the display of message statistics
	//
	enum {ALL_STATS			= ~0,
		MSG_TYPES_OUT		= 0x01,
		MSG_TYPE_IN		= 0x02,
		ORPHANS			= 0x04,
		MEMORY_OUT		= 0x08,
		MEMORY_IN		= 0x10,
		SIZE_HISTOGRAM_OUT	= 0x20,
		SIZE_HISTOGRAM_IN	= 0x40
	};

	// Record the selected statistics.
	int	stats_selected;

	//
	// Records the latest message statistics.
	// This is used to compute differences for a time interval.
	//
	cl_msg_stats	old_message_stats[NODEID_MAX + 1];
};

#include "stats_msg_in.h"

#endif	/* _CMD_ORB_STATS_MSG_H */
