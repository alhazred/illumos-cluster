//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)state_resource_balancer.cc	1.9	08/05/20 SMI"

#include "state_resource_balancer.h"
#include <orb/flow/resource_defs.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#ifndef _KERNEL_ORB
#include <errno.h>
#include <sys/os.h>
#include <sys/cladm_int.h>
#endif	// _KERNEL_ORB

state_resource_balancer	state_resource_balancer::the_state_resource_balancer;

//
// These enum values correspond to the array indexes
// for state_resource_balancer_opts
//
enum {STATE_RESOURCE_BALANCER_USAGE,
	STATE_RESOURCE_BALANCER_ALL,
	STATE_RESOURCE_BALANCER_POOL,
	STATE_RESOURCE_BALANCER_TOTAL,
	STATE_RESOURCE_BALANCER_UNALLOCATED
};

static char	*state_resource_balancer_opts[] = {
	"?",			// Provide option usage
	"all",			// Print all statistics
	"pool",
	"total",
	"unallocated",
	NULL
};

state_resource_balancer::state_resource_balancer()
{
	init();
}

//
// usage - explain options for resource_balancer state
//
// static
void
state_resource_balancer::usage()
{
	(void) fprintf(stderr,
	    "resource_balancer state requires at least one option:\n");
	(void) fprintf(stderr,
	    "\tpool=xxx - specify resource_pool number\n");
	(void) fprintf(stderr,
	    "\t\tif not specified use DEFAULT_POOL\n");
	(void) fprintf(stderr,
	    "\tall - display all resource_balancer state options\n");
	(void) fprintf(stderr,
	    "\ttotal - total number of threads\n");
	(void) fprintf(stderr,
	    "\tunallocated - number of unallocated threads\n");
}

//
// init - initialize class
//
void
state_resource_balancer::init()
{
	// Initialize to no statistics selected
	stats_selected = 0;

	// Use this resource_pool by default.
	state.pool = resource_defs::DEFAULT_POOL;
}

//
// parse_options - resource_balancer state was requested.
// Now determine the options.
// There will always be at least one option.
//
// Returns true upon successfully parsing valid options.
//
bool
state_resource_balancer::parse_options(char *optionp)
{
	bool	options_ok = true;
	char	*options = optionp;
	char	*value;

	init();

	while (options_ok && *options != '\0') {
		switch (getsubopt(&options, state_resource_balancer_opts,
		    &value)) {
		case STATE_RESOURCE_BALANCER_USAGE:
			options_ok = false;
			break;

		case STATE_RESOURCE_BALANCER_POOL:
			if (value == NULL) {
				(void) fprintf(stderr,
				    "No value for resource_pool\n");
				options_ok = false;
			} else {
				state.pool = atoi(value);
				if (state.pool < 0 ||
				    state.pool >= resource_defs::NUMBER_POOLS) {
					(void) fprintf(stderr,
					    "Invalid pool %s\n",
					    value);
					options_ok = false;
				}
			}
			break;

		case STATE_RESOURCE_BALANCER_ALL:
			stats_selected |= ALL_STATS;
			break;

		case STATE_RESOURCE_BALANCER_TOTAL:
			stats_selected |= TOTAL;
			break;

		case STATE_RESOURCE_BALANCER_UNALLOCATED:
			stats_selected |= UNALLOCATED;
			break;

		default:
			(void) fprintf(stderr,
			    "Unknown state resource_balancer option: %s\n",
			    value);
			options_ok = false;
		}
	}
	if (!options_ok) {
		state_resource_balancer::usage();
		return (false);
	}
	if (stats_selected == 0) {
		(void) fprintf(stderr, "Must specify a display option\n");
		state_resource_balancer::usage();
		return (false);
	}
	return (true);
}

//
// display_state - the system prints selected state of the
// resource_balancer subsystem.
//
int
state_resource_balancer::display_state()
{
	int		error;

#if defined(_KERNEL_ORB)
	// This code runs only in unode kernels.
	error = cl_read_orb_state_balancer(&state);
#else
	errno = 0;
	error = os::cladm(CL_CONFIG, CL_ORBADMIN_STATE_BALANCER, &state);
	if (error != 0) {
		// cladm places the error result in errno
		error = errno;
	}
#endif	// defined(_KERNEL_ORB)

	if (error != 0) {
		(void) fprintf(stderr,
		    "state_resource_balancer::display_state: error %s\n",
		    strerror(error));
		return (error);
	}
	//
	// Received resource_balancer state
	//
	if ((stats_selected & TOTAL) != 0) {
		(void) printf("threads_total: %d\n",
		    state.threads_total);
	}
	if ((stats_selected & UNALLOCATED) != 0) {
		(void) printf("threads_unallocated: %d\n",
		    state.threads_unallocated);
	}
	return (0);
}
