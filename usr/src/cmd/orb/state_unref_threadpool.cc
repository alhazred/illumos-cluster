//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)state_unref_threadpool.cc	1.7	08/05/20 SMI"

#include "state_unref_threadpool.h"
#include <stdio.h>
#include <string.h>

#ifndef _KERNEL_ORB
#include <errno.h>
#include <sys/os.h>
#include <sys/cladm_int.h>
#endif	// _KERNEL_ORB

state_unref_threadpool	state_unref_threadpool::the_state_unref_threadpool;

state_unref_threadpool::state_unref_threadpool()
{
	state.length = 0;
}

//
// display_state - print the number of tasks in the unref_threadpoool
//
int
state_unref_threadpool::display_state()
{
	int		error;

#if defined(_KERNEL_ORB)
	// This code runs only in unode kernels.
	error = cl_read_orb_state_unref(&state);
#else
	errno = 0;
	error = os::cladm(CL_CONFIG, CL_ORBADMIN_STATE_UNREF, &state);
	if (error != 0) {
		// cladm places the error result in errno
		error = errno;
	}
#endif	// defined(_KERNEL_ORB)

	if (error != 0) {
		(void) fprintf(stderr,
		    "state_unref_threadpool::display_state: error %s\n",
		    strerror(error));
		return (error);
	}
	(void) printf("length: %d\n", state.length);
	return (0);
}
