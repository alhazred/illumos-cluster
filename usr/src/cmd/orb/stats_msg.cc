//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)stats_msg.cc	1.13	08/05/20 SMI"

#include "stats_msg.h"
#include <stdlib.h>
#include <stdio.h>
#include <orb/msg/msg_stats.h>

#ifdef	_KERNEL_ORB
#include <orb/debug/orb_stats_mgr.h>
#else
#include <errno.h>
#include <sys/cladm_int.h>
#endif	// _KERNEL_ORB

stats_msg	stats_msg::the_stats_msg;

//
// These enum values correspond to the array indexes for stats_msg_opts
//
enum {STATS_MSG_USAGE,
	STATS_MSG_ALL,
	STATS_MSG_TYPES,
	STATS_MSG_TYPES_OUT,
	STATS_MSG_TYPES_IN,
	STATS_MSG_ORPHANS,
	STATS_MSG_MEMORY,
	STATS_MSG_MEMORY_OUT,
	STATS_MSG_MEMORY_IN,
	STATS_MSG_SIZE,
	STATS_MSG_SIZE_OUT,
	STATS_MSG_SIZE_IN
};

static char	*stats_msg_opts[] = {
	"?",			// Provide option usage
	"all",			// Print all statistics
	"msg_types",		// Both out and in message types
	"msg_types_out",
	"msg_types_in",
	"orphans",
	"memory",		// Both out and in memory
	"memory_out",
	"memory_in",
	"size_histogram",	// Both out and in size_histogram
	"size_histogram_out",
	"size_histogram_in",
	NULL
};

stats_msg::stats_msg()
{
	init();
}

//
// usage - explain options for message statistics
//
// static
void
stats_msg::usage()
{
	(void) fprintf(stderr,
	    "message stats requires at least one option:\n");
	(void) fprintf(stderr,
	    "\tall - display all message statistic options\n");
	(void) fprintf(stderr,
	    "\tmsg_types - display msg_types_out + msg_types_in\n");
	(void) fprintf(stderr,
	    "\tmsg_types_out\n");
	(void) fprintf(stderr,
	    "\tmsg_types_in\n");
	(void) fprintf(stderr,
	    "\torphans\n");
	(void) fprintf(stderr,
	    "\tmemory - display both memory_out + memory_in\n");
	(void) fprintf(stderr,
	    "\tmemory_out\n");
	(void) fprintf(stderr,
	    "\tmemory_in\n");
	(void) fprintf(stderr,
	    "\tsize_histogram - display for both out and in\n");
	(void) fprintf(stderr,
	    "\tsize_histogram_out\n");
	(void) fprintf(stderr,
	    "\tsize_histogram_in\n");
}

//
// init - initialize class
//
void
stats_msg::init()
{
	// Initialize to no statistics selected
	stats_selected = 0;
	for (int i = 1; i <= NODEID_MAX; i++) {
		old_message_stats[i].stats.init();
	}
}

//
// parse_options - message statistics were requested.
// Now determine the options.
// There will always be at least one option.
//
// Returns true upon successfully parsing valid options.
//
bool
stats_msg::parse_options(char *optionp)
{
	bool	options_ok = true;
	char	*options = optionp;
	char	*value;

	init();

	while (options_ok && *options != '\0') {
		switch (getsubopt(&options, stats_msg_opts, &value)) {
		case STATS_MSG_USAGE:
			stats_msg::usage();
			options_ok = false;
			break;

		case STATS_MSG_ALL:
			stats_selected |= ALL_STATS;
			break;

		case STATS_MSG_TYPES:
			stats_selected |= MSG_TYPES_OUT | MSG_TYPE_IN;
			break;

		case STATS_MSG_TYPES_OUT:
			stats_selected |= MSG_TYPES_OUT;
			break;

		case STATS_MSG_TYPES_IN:
			stats_selected |= MSG_TYPE_IN;
			break;

		case STATS_MSG_ORPHANS:
			stats_selected |= ORPHANS;
			break;

		case STATS_MSG_MEMORY:
			stats_selected |= MEMORY_OUT | MEMORY_IN;
			break;

		case STATS_MSG_MEMORY_OUT:
			stats_selected |= MEMORY_OUT;
			break;

		case STATS_MSG_MEMORY_IN:
			stats_selected |= MEMORY_IN;
			break;

		case STATS_MSG_SIZE:
			stats_selected |= SIZE_HISTOGRAM_OUT |
			    SIZE_HISTOGRAM_IN;
			break;

		case STATS_MSG_SIZE_OUT:
			stats_selected |= SIZE_HISTOGRAM_OUT;
			break;

		case STATS_MSG_SIZE_IN:
			stats_selected |= SIZE_HISTOGRAM_IN;
			break;

		default:
			(void) fprintf(stderr,
			    "Unknown message statistics option: %s\n",
			    value);
			stats_msg::usage();
			options_ok = false;
		}
	}
	return (options_ok);
}

//
// display_stats - statistics on message activity during the latest period
// are printed. Note that the first time this executes, it will print out
// the current values.
//
int
stats_msg::display_stats(int node_number)
{
	int		error;
	cl_msg_stats	message_stats;

	for (nodeid_t node = 1; node <= NODEID_MAX; node++) {
		if (node_number == 0 || (nodeid_t)node_number == node) {
			message_stats.nodeid = node;

#if defined(_KERNEL_ORB)
			// This code runs only in unode kernels.
			error = cl_read_orb_stats_msg(&message_stats);
#else
			errno = 0;
			error = os::cladm(CL_CONFIG, CL_ORBADMIN_STATS_MSG,
			    &message_stats);
			if (error != 0) {
				// cladm places the error result in errno
				error = errno;
			}
#endif	// defined(_KERNEL_ORB)

			if (error != 0) {
				(void) fprintf(stderr,
				    "stats_msg::display_stats: error %s\n",
				    strerror(error));
				exit(error);
			}
			//
			// Discard if node is currently not in cluster
			//
			if (message_stats.nodeid == NODEID_UNKNOWN) {
				if ((nodeid_t)node_number == node) {
					(void) fprintf(stderr, "node %d not in "
					    "cluster\n", node);
				}
				continue;
			}
			//
			// Received message stats for a node in the cluster.
			//
			if (node_number == 0) {
				os::printf("node number: %d\n", node);
			}
			if ((stats_selected & MSG_TYPES_OUT) != 0) {
				os::printf("msg_types_out:");
				for (int i = 0; i < N_MSGTYPES; i++) {
					os::printf(" %d",
					    message_stats.stats.
						msg_types_out[i] -
					    old_message_stats[node].stats.
						msg_types_out[i]);
				}
				os::printf("\n");
			}
			if ((stats_selected & MSG_TYPE_IN) != 0) {
				os::printf("msg_types_in:");
				for (int i = 0; i < N_MSGTYPES; i++) {
					os::printf(" %d",
					    message_stats.stats.
						msg_types_in[i] -
					    old_message_stats[node].stats.
						msg_types_in[i]);
				}
				os::printf("\n");
			}
			if ((stats_selected & ORPHANS) != 0) {
				os::printf("orphans:");
				for (int i = 0; i < N_MSGTYPES; i++) {
					os::printf(" %d",
					    message_stats.stats.orphans[i] -
					    old_message_stats[node].
						stats.orphans[i]);
				}
				os::printf("\n");
			}
			if ((stats_selected & MEMORY_OUT) != 0) {
				os::printf("memory_out: %lld\n",
				    message_stats.stats.memory_out -
				    old_message_stats[node].stats.memory_out);
			}
			if ((stats_selected & MEMORY_IN) != 0) {
				os::printf("memory_in: %lld\n",
				    message_stats.stats.memory_in -
				    old_message_stats[node].stats.memory_in);
			}
			if ((stats_selected & SIZE_HISTOGRAM_OUT) != 0) {
				os::printf("size_histogram_out:");
				for (int i = 0;
				    i < msg_stats::NUMBER_HISTOGRAM_SIZES;
				    i++) {
					os::printf(" %d",
					    message_stats.stats.
						size_histogram_out[i] -
					    old_message_stats[node].stats.
						size_histogram_out[i]);
				}
				os::printf("\n");
			}
			if ((stats_selected & SIZE_HISTOGRAM_IN) != 0) {
				os::printf("size_histogram_in:");
				for (int i = 0;
				    i < msg_stats::NUMBER_HISTOGRAM_SIZES;
				    i++) {
					os::printf(" %d",
					    message_stats.stats.
						size_histogram_in[i] -
					    old_message_stats[node].stats.
						size_histogram_in[i]);
				}
				os::printf("\n");
			}
			old_message_stats[node] = message_stats;
		}
	}
	return (0);
}
