//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)state_refcount.cc	1.9	08/05/20 SMI"

#include "state_refcount.h"
#include <stdio.h>
#include <string.h>
#include <sys/clconf_int.h>

#ifndef _KERNEL_ORB
#include <errno.h>
#include <sys/os.h>
#include <sys/cladm_int.h>
#endif	// _KERNEL_ORB

state_refcount	state_refcount::the_state_refcount;

//
// These enum values correspond to the array indexes for state_refcount_opts
//
enum {STATE_REFCOUNT_USAGE,
	STATE_REFCOUNT_ALL,
	STATE_REFCOUNT_REF,
	STATE_REFCOUNT_ACK
};

static char	*state_refcount_opts[] = {
	"?",			// Provide option usage
	"all",			// Print all statistics
	"ref",
	"ack",
	NULL
};

state_refcount::state_refcount()
{
	init();
}

//
// usage - explain options for resource_balancer state
//
// static
void
state_refcount::usage()
{
	(void) fprintf(stderr,
	    "refcount state requires at least one option:\n");
	(void) fprintf(stderr, "\tall - display all refcount state options\n");
	(void) fprintf(stderr, "\tref - number of reference jobs\n");
	(void) fprintf(stderr, "\tack - number of ack jobs\n");
}

//
// init - initialize class
//
void
state_refcount::init()
{
	// Initialize to no statistics selected
	stats_selected = 0;
}

//
// parse_options - refcount state was requested.
// Now determine the options.
// There will always be at least one option.
//
// Returns true upon successfully parsing valid options.
//
bool
state_refcount::parse_options(char *optionp)
{
	bool	options_ok = true;
	char	*options = optionp;
	char	*value;

	init();

	while (options_ok && *options != '\0') {
		switch (getsubopt(&options, state_refcount_opts, &value)) {
		case STATE_REFCOUNT_USAGE:
			state_refcount::usage();
			options_ok = false;
			break;

		case STATE_REFCOUNT_ALL:
			stats_selected |= ALL_STATS;
			break;

		case STATE_REFCOUNT_REF:
			stats_selected |= REF;
			break;

		case STATE_REFCOUNT_ACK:
			stats_selected |= ACK;
			break;

		default:
			(void) fprintf(stderr,
			    "Unknown state refcount option: %s\n",
			    value);
			state_refcount::usage();
			options_ok = false;
		}
	}
	return (options_ok);
}

//
// display_state - the system prints selected state of the refcount subsystem
//
int
state_refcount::display_state(int node_number)
{
	int		error;

	for (int node = 1; node <= NODEID_MAX; node++) {
		if (node_number == 0 || node_number == node) {
			state.node = node;

#if defined(_KERNEL_ORB)
			// This code runs only in unode kernels.
			error = cl_read_orb_state_refcount(&state);
#else
			errno = 0;
			error = os::cladm(CL_CONFIG, CL_ORBADMIN_STATE_REFCOUNT,
			    &state);
			if (error != 0) {
				// cladm places the error result in errno
				error = errno;
			}
#endif	// defined(_KERNEL_ORB)

			if (error != 0) {
				(void) fprintf(stderr,
				    "state_refcount::display_state: error %s\n",
				    strerror(error));
				return (error);
			}
			//
			// Discard if node is currently not in cluster
			//
			if (state.node == NODEID_UNKNOWN) {
				if (node_number == node) {
					(void) printf("node %d not in "
					    "cluster\n", node);
				}
				continue;
			}
			//
			// Received refcount state for a node in the cluster.
			//
			if (node_number == 0) {
				(void) printf("node number: %d\n", node);
			}
			if ((stats_selected & REF) != 0) {
				(void) printf("ref_jobs: %d\n", state.ref_jobs);
			}
			if ((stats_selected & ACK) != 0) {
				(void) printf("ack_jobs: %d\n",
				    state.ack_jobs);
			}
		}
	}
	return (0);
}
