/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)rdt_setmtu.c 1.4	08/05/20 SMI"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <errno.h>
#include <locale.h>

#include "rdtapi.h" 	/* RDT API header file */

/*
 * Sets the global MTU size in RSMRDT driver.
 *
 * Usage: /usr/cluster/bin/rdt_setmtu [ MTU size ]
 *
 * If run without any argument, it displays the MTU size of
 * RSMRDT driver.
 */

int
main(int argc, char **argv)
{
	size_t		hdlsz, mtusz;
	rdt_handle_t	*hdl;

	(void) setlocale(LC_ALL, "");
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);
	(void) textdomain(TEXT_DOMAIN);

	if (argc >= 3) {
		(void) fprintf(stderr,
			gettext("\nUsage: rdt_setmtu [MTU size]\n\n"));
		return (1);
	}

	/*
	 * Suppressing the lint error for now. Not sure why FlexeLint
	 * is complaining about missing prototype.
	 */
	/*lint -e746 */
	hdlsz = rdt_hdlsize();
	/*lint +e746 */

	hdl = (rdt_handle_t *)malloc(hdlsz);
	if (hdl == NULL) {
		(void) fprintf(stderr,
			gettext("\nUnable to allocate memory\n\n"));
		return (1);
	}

	if (rdt_create(hdl, 0) != 0) {
		/*
		 * Suppressing the lint error for now. Not sure why FlexeLint
		 * is complaining about missing prototype.
		 */
		/*lint -e746 */
		(void) fprintf(stderr,
			gettext("\nUnable to create handle: %s\n\n"),
			strerror(errno));
		/*lint +e746 */
		free(hdl);
		return (1);
	}

	if (argc == 2) {
		mtusz = (size_t)atoi(argv[1]);

		if (rdt_setparam(hdl, RDT_MAXMSGSIZE, &mtusz, sizeof (mtusz))
		    != 0) {
			(void) fprintf(stderr,
				gettext("\nUnable to set MTU size: %s\n\n"),
				strerror(errno));
			(void) rdt_destroy(hdl);
			free(hdl);
			return (1);
		}
	} else {
		if (rdt_getparam(hdl, RDT_MAXMSGSIZE, &mtusz, sizeof (mtusz))
		    != 0) {
			(void) fprintf(stderr,
				gettext("\nUnable to get MTU size: %s\n\n"),
				strerror(errno));
			(void) rdt_destroy(hdl);
			free(hdl);
			return (1);
		}
		(void) fprintf(stderr,
			gettext("\nMTU size: %d bytes\n\n"), mtusz);
	}
	(void) rdt_destroy(hdl);
	free(hdl);
	return (0);
}
