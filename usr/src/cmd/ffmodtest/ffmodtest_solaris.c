/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)ffmodtest_solaris.c	1.2	08/05/20 SMI"

#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/param.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <pthread.h>
#include "ffmod_ctl.h"

int silent = 0;
int batch = 0;

static char *cmdname;

FILE *fdi;

int fct_index = 0;

static char *function_name[] =
{
	"UNKNOWN",
	"OPEN CTL",
	"CLOSE CTL",
	"CREATE",
	"DELETE",
	"OPEN",
	"CLOSE",
	"ARM_EXIT_CHECKING",
	"DISARM_EXIT_CHECKING",
	"CONFIGURE_FAILURE_FUNCTION",
};

void
usage() {
	(void) fprintf(stderr, "usage: %s [-h] [-s] [-m] [-i <scenario>]\n",
	    cmdname);
	exit(1);
}

void
error(char *err) {
	(void) fprintf(stderr, "(%s:%d) ERROR: %s\n",
	    cmdname, pthread_self(), err);
}

void
print_fct(int i) {
	(void) fprintf(stdout, "(%s:%d) FUNCTION: %s\n",
	    cmdname, pthread_self(), function_name[i]);
}

void
print_fd(int fd) {
	(void) fprintf(stdout, "(%s:%d) FD: %d\n",
	    cmdname, pthread_self(), fd);
}

void
print_name(char *name) {
	(void) fprintf(stdout, "(%s:%d) NAME: %s\n",
	    cmdname, pthread_self(), name);
}

void
print_mode(int mode) {
	(void) fprintf(stdout, "(%s:%d) MODE: %d\n",
	    cmdname, pthread_self(), mode);
}


void
print_minor(unsigned int minor) {
	(void) fprintf(stdout, "(%s:%d) MINOR: %d\n",
	    cmdname, pthread_self(), minor);
}


void
print_config(failure_function_type *config) {
	(void) fprintf(stdout, "(%s:%d) MODE : %d\n",
		cmdname, pthread_self(), (int)config);
}

void
retcode(int err) {
	(void) fprintf(stdout, "(%s:%d) RETURN CODE: %s\n",
	    cmdname, pthread_self(), strerror(err));
}

void
press_enter() {
	char enter;
	(void) fflush(fdi);
	(void) fscanf(fdi, "%c", &enter);
	(void) fscanf(fdi, "%c", &enter);
}

void
new_line() {
	char cr;
	if (batch) {
		(void) fflush(fdi);
		(void) fscanf(fdi, "%c", &cr);
	}
}

void
test_open_ctl()
{
	int fd;

	print_fct(fct_index);

	fd = open("/dev/ffmodctl", O_RDWR);
	if (fd > 0) {
		print_fd(fd);
		errno = 0;
	}
	retcode(errno);
}

void
test_close()
{
	char s[80];
	int fd;
	int rc;

	print_fct(fct_index);

	bzero(s, 80);
	(void) fflush(fdi);
	if (!silent)
		(void) fprintf(stdout, "fd ? ");
	new_line();
	(void) fscanf(fdi, "%s", s);
	(void) sscanf(s, "%d", &fd);

	if (batch) {
		print_fd(fd);
	}

	rc = close(fd);
	if (rc == 0)
		errno = 0;
	retcode(errno);
}

void
test_create()
{
	char s[80];
	int fd;
	int rc;
	struct ffmod_ioctl ioc;

	print_fct(fct_index);

	bzero(s, 80);
	(void) fflush(fdi);
	if (!silent)
		(void) fprintf(stdout, "fd ? ");
	new_line();
	(void) fscanf(fdi, "%s", s);
	(void) sscanf(s, "%d", &fd);

	if (batch) {
		print_fd(fd);
	}
	(void) fflush(fdi);
	if (!silent)
		(void) fprintf(stdout, "ff unit name ? ");
	new_line();
	(void) fscanf(fdi, "%s", ioc.ffmod_name);
	ioc.ffmod_minor = 0;

	rc = ioctl(fd, FFMOD_IOC_CREATE, &ioc);
	if (rc == 0)
		errno = 0;

	print_minor(ioc.ffmod_minor);

	retcode(errno);
}


void
test_delete()
{
	char s[80];
	int fd;
	int rc;
	struct ffmod_ioctl ioc;

	print_fct(fct_index);

	bzero(s, 80);
	(void) fflush(fdi);
	if (!silent)
		(void) fprintf(stdout, "fd ? ");
	new_line();
	(void) fscanf(fdi, "%s", s);
	(void) sscanf(s, "%d", &fd);

	if (batch) {
		print_fd(fd);
	}
	(void) fflush(fdi);
	if (!silent)
		(void) fprintf(stdout, "ff unit name ? ");
	new_line();
	(void) fscanf(fdi, "%s", ioc.ffmod_name);

	(void) fflush(fdi);
	if (!silent)
		(void) fprintf(stdout, "ff minor ? ");
	new_line();
	(void) fscanf(fdi, "%s", s);
	(void) sscanf(s, "%d", &ioc.ffmod_minor);

	rc = ioctl(fd, FFMOD_IOC_DELETE, &ioc);
	if (rc == 0)
		errno = 0;

	retcode(errno);
}

void
test_open()
{
	char s[80];
	int fd;
	char dev[80];

	print_fct(fct_index);
	bzero(s, 80);
	(void) fflush(fdi);
	if (!silent)
		(void) fprintf(stdout, "minor ? ");
	new_line();
	(void) fscanf(fdi, "%s", s);
	(void) sprintf(dev, "/dev/ffmod%s", s);

	(void) printf("OPEN DEVICE %s\n", dev);

	fd = open(dev, O_RDWR);
	if (fd > 0) {
		print_fd(fd);
		errno = 0;
	}
	retcode(errno);
}

void
test_arm_exit_checking()
{
	char s[80];
	int fd;
	int rc;

	print_fct(fct_index);

	bzero(s, 80);
	(void) fflush(fdi);
	if (!silent)
		(void) fprintf(stdout, "fd ? ");
	new_line();
	(void) fscanf(fdi, "%s", s);
	(void) sscanf(s, "%d", &fd);

	if (batch) {
		print_fd(fd);
	}

	rc = ioctl(fd, FFMOD_IOC_ARM);
	if (rc == 0)
		errno = 0;
	retcode(errno);
}

void
test_disarm_exit_checking()
{
	char s[80];
	int fd;
	int rc;

	print_fct(fct_index);

	bzero(s, 80);
	(void) fflush(fdi);
	if (!silent)
		(void) fprintf(stdout, "fd ? ");
	new_line();
	(void) fscanf(fdi, "%s", s);
	(void) sscanf(s, "%d", &fd);

	if (batch) {
		print_fd(fd);
	}

	rc = ioctl(fd, FFMOD_IOC_DISARM);
	if (rc == 0)
		errno = 0;
	retcode(errno);
}

void
test_configure_failure_function()
{
	char s[80];
	int fd;
	failure_function_type config;
	int rc;

	print_fct(fct_index);

	bzero(s, 80);
	(void) fflush(fdi);
	if (!silent)
		(void) fprintf(stdout, "fd ? ");
	new_line();
	(void) fscanf(fdi, "%s", s);
	(void) sscanf(s, "%d", &fd);

	if (batch) {
		print_fd(fd);
	}
	bzero(s, 80);
	(void) fflush(fdi);
	if (!silent)
		(void) fprintf(stdout, "ff unit mode ? ");
	new_line();
	(void) fscanf(fdi, "%s", s);
	(void) sscanf(s, "%d", &config);

	if (batch) {
		print_mode(fd);
	}

	rc = ioctl(fd, FFMOD_IOC_SCONF, &config);
	if (rc == 0)
		errno = 0;
	retcode(errno);
}

int
main(int argc, char **argv) {
	char s[80];
	int choice;
	char c;

	cmdname = argv[0];
	fdi = stdin;

	while ((c = (char)getopt(argc, argv, "i:sh")) != EOF) {
		switch (c) {

		case 'i':
			if (!optarg)
				usage();
			fdi = fopen(optarg, "r");
			if (fdi == NULL) {
				error("Cannot not open scenario file");
				exit(1);
			}
			batch = 1;
			break;
		case 's':
			silent = 1;
			break;
		case '?':
		case 'h':
			usage();
			exit(0);
		default:
			usage();
			exit(1);

		}
	}

	while (1) {

		if (!silent) {
			(void) system("clear");
			(void) fprintf(stdout, "FAILFAST DRIVER testing\n\n");
			(void) fprintf(stdout, "1.  OPEN FFMOD CTL\n");
			(void) fprintf(stdout, "2.  CLOSE FFMOD CTL\n");
			(void) fprintf(stdout, "3.  FFMOD_IOC_CREATE\n");
			(void) fprintf(stdout, "4.  FFMOD_IOC_DELETE\n");
			(void) fprintf(stdout, "5.  OPEN\n");
			(void) fprintf(stdout, "6.  CLOSE\n");
			(void) fprintf(stdout, "7.  FFMOD_IOC_ARM\n");
			(void) fprintf(stdout, "8.  FFMOD_IOC_DISARM\n");
			(void) fprintf(stdout, "9.  FFMOD_IOC_SCONF\n");
			(void) fprintf(stdout, "\nChoice (q for quit)? ");
		}
		(void) fscanf(fdi, "%s", s);
		if ((s[0] == 'q') || (s[0] == 'Q'))
			break;
		(void) sscanf(s, "%d", &choice);

		fct_index = choice;

		switch (choice) {
		case 1:
			test_open_ctl();
			break;
		case 2:
			test_close();
			break;
		case 3:
			test_create();
			break;
		case 4:
			test_delete();
			break;
		case 5:
			test_open();
			break;
		case 6:
			test_close();
			break;
		case 7:
			test_arm_exit_checking();
			break;
		case 8:
			test_disarm_exit_checking();
			break;
		case 9:
			test_configure_failure_function();
			break;
		default:
			error("Invalid Choice");
		}

		press_enter();
	}
	return (0);
}
