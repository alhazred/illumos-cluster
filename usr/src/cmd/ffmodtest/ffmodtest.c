/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)ffmodtest.c	1.2	08/05/20 SMI"

#include <stdio.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <getopt.h>
#include <errno.h>
#include "if_failfast.h"

int silent = 0;
int batch = 0;

static char *cmdname;

FILE *fdi;

int fct_index = 0;

static char *function_name[] =
{
	"UNKNOWN",
	"OPEN",
	"CLOSE",
	"FFIOCENABLE",
	"FFIOCDISABLE",
	"FFIOCARM",
	"FFIOCDISARM",
	"FFIOCGCONF",
	"FFIOCSCONF",
};

void
usage() {
	fprintf(stderr, "usage: %s [-h] [-s] [-m] [-i <scenario>]\n",
	    cmdname);
	exit(1);
}

void
error(char *err) {
	fprintf(stderr, "(%s:%d) ERROR: %s\n",
	    cmdname, pthread_self(), err);
}

void
print_fct(int fct_index) {
	fprintf(stdout, "(%s:%d) FUNCTION: %s\n",
	    cmdname, pthread_self(), function_name[fct_index]);
}

void
print_fd(int fd) {
	fprintf(stdout, "(%s:%d) FD: %d\n",
	    cmdname, pthread_self(), fd);
}

void
print_millisecs(int millisecs) {
	fprintf(stdout, "(%s:%d) MILLISECS: %d\n",
	    cmdname, pthread_self(), millisecs);
}

void
print_name(char *name) {
	fprintf(stdout, "(%s:%d) NAME: %s\n",
	    cmdname, pthread_self(), name);
}

void
print_mode(int mode) {
	fprintf(stdout, "(%s:%d) MODE: %d\n",
	    cmdname, pthread_self(), mode);
}

void
print_config(ffconf_t *config) {
	fprintf(stdout, "(%s:%d) NAME : %s\n",
	    cmdname, pthread_self(), config->name);
	fprintf(stdout, "(%s:%d) MODE : %d\n",
	    cmdname, pthread_self(), config->mode);
}

void
retcode(int err) {
	fprintf(stdout, "(%s:%d) RETURN CODE: %s\n",
	    cmdname, pthread_self(), strerror(err));
}

void
press_enter() {
	char enter;
	fflush(fdi);
	fscanf(fdi, "%c", &enter);
	fscanf(fdi, "%c", &enter);
}

void
new_line() {
	char cr;
	if (batch) {
		fflush(fdi);
		fscanf(fdi, "%c", &cr);
	}
}

void
test_open()
{
	int fd;

	print_fct(fct_index);

	fd = open("/dev/ffmodd", O_RDWR);
	if (fd > 0) {
		print_fd(fd);
		errno = 0;
	}
	retcode(errno);
}

void
test_close()
{
	char s[80];
	int fd;
	int rc;

	print_fct(fct_index);

	bzero(s, 80);
	fflush(fdi);
	if (!silent)
		fprintf(stdout, "fd ? ");
	new_line();
	fscanf(fdi, "%s", s);
	sscanf(s, "%d", &fd);

	if (batch) {
		print_fd(fd);
	}

	rc = close(fd);
	if (rc == 0)
		errno = 0;
	retcode(errno);
}

void
test_ffiocenable()
{
	char s[80];
	int fd;
	int rc;

	print_fct(fct_index);

	bzero(s, 80);
	fflush(fdi);
	if (!silent)
		fprintf(stdout, "fd ? ");
	new_line();
	fscanf(fdi, "%s", s);
	sscanf(s, "%d", &fd);

	if (batch) {
		print_fd(fd);
	}

	rc = ioctl(fd, FFIOCENABLE, NULL);
	if (rc == 0)
		errno = 0;
	retcode(errno);
}

void
test_ffiocdisable()
{
	char s[80];
	int fd;
	int rc;

	print_fct(fct_index);

	bzero(s, 80);
	fflush(fdi);
	if (!silent)
		fprintf(stdout, "fd ? ");
	new_line();
	fscanf(fdi, "%s", s);
	sscanf(s, "%d", &fd);

	if (batch) {
		print_fd(fd);
	}

	rc = ioctl(fd, FFIOCDISABLE, NULL);
	if (rc == 0)
		errno = 0;
	retcode(errno);
}

void
test_ffiocarm()
{
	char s[80];
	int fd;
	int millisecs;
	int rc;

	print_fct(fct_index);

	bzero(s, 80);
	fflush(fdi);
	if (!silent)
		fprintf(stdout, "fd ? ");
	new_line();
	fscanf(fdi, "%s", s);
	sscanf(s, "%d", &fd);

	if (batch) {
		print_fd(fd);
	}
	bzero(s, 80);
	fflush(fdi);
	if (!silent)
		fprintf(stdout, "millisecs ? ");
	new_line();
	fscanf(fdi, "%s", s);
	sscanf(s, "%d", &millisecs);

	if (batch) {
		print_millisecs(millisecs);
	}

	rc = ioctl(fd, FFIOCARM, &millisecs);
	if (rc == 0)
		errno = 0;
	retcode(errno);
}

void
test_ffiocdisarm()
{
	char s[80];
	int fd;
	int millisecs;
	int rc;

	print_fct(fct_index);

	bzero(s, 80);
	fflush(fdi);
	if (!silent)
		fprintf(stdout, "fd ? ");
	new_line();
	fscanf(fdi, "%s", s);
	sscanf(s, "%d", &fd);

	if (batch) {
		print_fd(fd);
	}

	rc = ioctl(fd, FFIOCDISARM, NULL);
	if (rc == 0)
		errno = 0;
	retcode(errno);
}

void
test_ffiocgconf()
{
	char s[80];
	int fd;
	int rc;
	ffconf_t config;

	print_fct(fct_index);

	bzero(s, 80);
	fflush(fdi);
	if (!silent)
		fprintf(stdout, "fd ? ");
	new_line();
	fscanf(fdi, "%s", s);
	sscanf(s, "%d", &fd);

	if (batch) {
		print_fd(fd);
	}

	rc = ioctl(fd, FFIOCGCONF, &config);
	if (rc == 0) {
		print_config(&config);
		errno = 0;
	}
	retcode(errno);
}

void
test_ffiocsconf()
{
	char s[80];
	int fd;
	ffconf_t config;
	int rc;

	print_fct(fct_index);

	bzero(s, 80);
	fflush(fdi);
	if (!silent)
		fprintf(stdout, "fd ? ");
	new_line();
	fscanf(fdi, "%s", s);
	sscanf(s, "%d", &fd);

	if (batch) {
		print_fd(fd);
	}
	fflush(fdi);
	if (!silent)
		fprintf(stdout, "ff unit name ? ");
	new_line();
	fscanf(fdi, "%s", config.name);

	if (batch) {
		print_name(config.name);
	}
	bzero(s, 80);
	fflush(fdi);
	if (!silent)
		fprintf(stdout, "ff unit mode ? ");
	new_line();
	fscanf(fdi, "%s", s);
	sscanf(s, "%d", &config.mode);

	if (batch) {
		print_mode(fd);
	}

	rc = ioctl(fd, FFIOCSCONF, &config);
	if (rc == 0)
		errno = 0;
	retcode(errno);
}

int
main(int argc, char **argv) {
	char s[80];
	int choice;
	char c;

	cmdname = argv[0];
	fdi = stdin;

	while ((c = getopt(argc, argv, "i:sh")) != EOF) {
		switch (c) {
		case '?':
		case 'h':
			usage();
		case 'i':
			if (!optarg)
				usage();
			fdi = fopen(optarg, "r");
			if (fdi == NULL) {
				error("Cannot not open scenario file");
				exit(1);
			}
			batch = 1;
			break;
		case 's':
			silent = 1;
			break;
		}
	}

	while (1) {

		if (!silent) {
			system("clear");
			fprintf(stdout, "FAILFAST DRIVER testing\n\n");
			fprintf(stdout, "1. OPEN\n");
			fprintf(stdout, "2. CLOSE\n");
			fprintf(stdout, "3. FFIOCENABLE\n");
			fprintf(stdout, "4. FFIOCDISABLE\n");
			fprintf(stdout, "5. FFIOCARM\n");
			fprintf(stdout, "6. FFIOCDISARM\n");
			fprintf(stdout, "7. FFIOCGCONF\n");
			fprintf(stdout, "8. FFIOCSCONF\n");
			fprintf(stdout, "\nChoice (q for quit)? ");
		}
		fscanf(fdi, "%s", s);
		if ((s[0] == 'q') || (s[0] == 'Q'))
			break;
		sscanf(s, "%d", &choice);

		fct_index = choice;

		switch (choice) {
		case 1:
			test_open();
			break;
		case 2:
			test_close();
			break;
		case 3:
			test_ffiocenable();
			break;
		case 4:
			test_ffiocdisable();
			break;
		case 5:
			test_ffiocarm();
			break;
		case 6:
			test_ffiocdisarm();
			break;
		case 7:
			test_ffiocgconf();
			break;
		case 8:
			test_ffiocsconf();
			break;
		default:
			error("Invalid Choice");
		}

		press_enter();
	}

	return (0);
}
