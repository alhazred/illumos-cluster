/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)haglbd_fmm.c 1.3	08/05/20 SMI"

#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <poll.h>
#include <assert.h>
#include <errno.h>
#include <sys/sc_syslog_msg.h>
#include <pthread.h>
#include <rgm/libdsdev.h>
#include "haglbd.h"
#include <saf_clm.h>

unsigned fmm_notification_thr = 0;
SaClmHandleT handle;

#define	MAX_FARM_NODES 512

/* ARGUSED */
void
clusterNodeGet(SaInvocationT invocation,
    SaClmClusterNodeT *clusterNode,
    SaErrorT error)
{
	if (debug) {
		(void) printf("ClusterNodeGet callback\n");
		(void) printf("\terror %d\n", error);
		(void) printf("\tinvocation %llu\n", invocation);
		(void) printf("\tnodeId %d\n", clusterNode->nodeId);
	}
}

void
clusterTrackStart(SaClmClusterNotificationT *notificationBuffer,
    SaUint32T numberOfItems,
    SaUint32T numberOfMembers,
    SaUint64T viewNumber,
    SaErrorT error)
{
	unsigned int i = 0;
	nodelist_t *current = NULL;

	if (debug) {
		(void) printf("clusterTrackStart callback\n");
		(void) printf("\terror %d\n", error);
		dump_hashtable(g_props.NodesTable);
	}
	if (error != SA_OK)
		return;


	(void) pthread_mutex_lock(&config_lock);

	if (debug) {
		(void) printf("\tnumberOfItems %d\n", numberOfItems);
		(void) printf("\tnumberOfMembers %d\n", numberOfMembers);
		(void) printf("\tviewNumber %llu\n", viewNumber);
	}
	for (i = 0; i < numberOfItems; i++) {
		if (debug) {
			(void) printf("\tchange %d\n",
			    notificationBuffer[i].clusterChanges);
			(void) printf("\t\tnodeId %d\n",
			    notificationBuffer[i]
			    .clusterNode.nodeId);
			(void) printf("\t\tnodeAddress %s\n",
			    notificationBuffer[i]
			    .clusterNode.nodeAddress.value);
			(void) printf("\t\tclusterName %s\n",
			    notificationBuffer[i]
			    .clusterNode.clusterName.value);
			(void) printf("\t\tmember %s\n",
			    (notificationBuffer[i]
				.clusterNode.member)?"YES":"NO");
			(void) printf("\t\tbootTimestamp %lld\n",
			    notificationBuffer[i]
			    .clusterNode.bootTimestamp);
			(void) printf("\t\tinitialViewNumber %llu\n",
			    notificationBuffer[i]
			    .clusterNode.initialViewNumber);
		}
		if (notificationBuffer[i].clusterChanges == 0) {
			/* No more items */
			if (debug)
				(void) printf("No more items\n");
			goto finished;
		}

		/* Check if this node is in LB_Nodelist */
		if ((current = isnode_in_hashtable(g_props.NodesTable,
		    notificationBuffer[i].clusterNode.nodeId)) == NULL) {
			if (debug)
				(void) printf("Nodeid %d is not "
				    "in our NodeList\n",
				    notificationBuffer[i].clusterNode.nodeId);
			continue;
		}

		switch (notificationBuffer[i].clusterChanges) {
			case SA_CLM_NODE_NO_CHANGE:
				/* FALLTHRU */
			case SA_CLM_NODE_JOINED:;
				if (current->status != IN) {
				/*
				 * Will add this node to the load balancing
				 * group
				 */
					(void) run_lb_command(ADD_COMMAND,
					    current->nodename);
					current->status = IN;
				}
				break;
			case SA_CLM_NODE_LEFT:
				if (current->status != OUT) {
				/*
				 * Will remove this node to the load balancing
				 * group
				 */
					(void) run_lb_command(REMOVE_COMMAND,
					    current->nodename);
					current->status = OUT;
				}
				break;
			default:
				assert(0);
		}

	}

finished:
	(void) pthread_mutex_unlock(&config_lock);
}


void
fmm_notification_thread(void)
{
	SaErrorT result = SA_OK;
	SaSelectionObjectT selectionObject;
	SaClmClusterNotificationT *notificationBuffer;
	SaUint32T numberOfItems = MAX_FARM_NODES;
	boolean_t go_on = B_TRUE;

	struct pollfd poll_fd;

	notificationBuffer = (SaClmClusterNotificationT *)malloc(numberOfItems
	    * sizeof (SaClmClusterNotificationT));
	bzero(notificationBuffer,
	    numberOfItems * sizeof (SaClmClusterNotificationT));
	result = saClmClusterTrackStart(handle, SA_TRACK_CURRENT,
	    notificationBuffer, numberOfItems);
	if (debug)
		(void) printf("saClmClusterTrackStart returned %d\n", result);

	result = saClmClusterTrackStart(handle, SA_TRACK_CHANGES,
	    notificationBuffer, numberOfItems);
	if (debug)
		(void) printf("saClmClusterTrackStart returned %d\n", result);

	result = saClmSelectionObjectGet(handle, &selectionObject);
	if (debug)
		(void) printf("saClmSelectionObject returned %d\n", result);

	while (go_on) {
		poll_fd.fd = (int)selectionObject;
		poll_fd.events = POLLIN;


		switch (poll(&poll_fd, 1, -1)) {
		case -1:
			if (debug)
				(void) printf("polling error %s\n",
				    strerror(errno)); /*lint !e746 */
			go_on = B_FALSE;
			break;
		case 0:
			break;
		case 1:
			if ((!(poll_fd.revents & POLLIN)) ||
			    (poll_fd.revents & POLLHUP) ||
			    (poll_fd.revents & POLLERR) ||
			    (poll_fd.revents & POLLNVAL)) {
				if (debug)
					(void) printf("polling error\n");
				go_on = B_FALSE;
				break;
			}

			result = saClmDispatch(handle, SA_DISPATCH_ALL);
			if (debug)
				(void) printf("saClmDispatch returned %d\n",
				    result);
			break;
		default:
			break;
		}

	}

	result = saClmClusterTrackStop(handle);
	if (debug)
		(void) printf("saClusterTrackStop returned %d\n", result);
	result = saClmFinalize(handle);
	if (debug)
		(void) printf("saClmFinalize returned %d\n", result);

	exit(0);
}

int
fmm_notification_module_shutdown()
{
	SaErrorT result = SA_OK;

	result = saClmClusterTrackStop(handle);
	if (debug)
		(void) printf("saClusterTrackStop returned %d\n", result);
	result = saClmFinalize(handle);
	if (debug)
		(void) printf("saClmFinalize returned %d\n", result);

	return (0);
}

int
fmm_notification_module_initialize()
{
	sc_syslog_msg_handle_t schandle;
	SaVersionT version;
	SaClmCallbacksT callbacks;
	SaErrorT result = SA_OK;
	int ret;

	scds_syslog_debug(1, "Initializing fmm_notification_module\n");

	callbacks.saClmClusterNodeGetCallback = clusterNodeGet;
	callbacks.saClmClusterTrackCallback = clusterTrackStart;
	version.releaseCode = 'A';
	version.major = 0x01;
	version.minor = 0xff;

	result = saClmInitialize(&handle, &callbacks, &version);
	if (result != SA_OK) {
		scds_syslog_debug(1, "Unable to initialize fmm library\n");
		return (-1);
	}

	if ((ret = pthread_create(&fmm_notification_thr, NULL,
	    (void * (*) (void *)) fmm_notification_thread,
	    NULL)) != 0) {
		char *err_str = strerror(ret);
		(void) sc_syslog_msg_initialize(&schandle,
		    SC_SYSLOG_HAGLBD_TAG, "");
		(void) sc_syslog_msg_log(schandle, LOG_ERR, MESSAGE,
		    "pthread_create: %s", err_str ? err_str : err_str);
		sc_syslog_msg_done(&schandle);
		return (ret);
	}

	return (0);
}

boolean_t
in_fmm_membership(uint_t nodeid)
{
	SaErrorT result = SA_OK;
	SaClmClusterNodeT clusterNode;
	SaTimeT timeout = 5000000000; /* 5 s */
	boolean_t rc = B_FALSE;

	result = saClmClusterNodeGet(handle, (SaClmNodeIdT) nodeid,
	    timeout, &clusterNode);

	if (result == SA_OK) {
		if (clusterNode.member)
			rc = B_TRUE;
		else
			rc = B_FALSE;
	}

	return (rc);
}
