/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma	ident	"@(#)haglbd_main.c 1.4	08/05/20 SMI"

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <signal.h>
#include <errno.h>
#include <string.h>
#include <sys/sc_syslog_msg.h>
#include <sys/boolean.h>
#include <pthread.h>

#include <rgm/libdsdev.h>
#include <scadmin/scconf.h>
#include "nodehashtable.h"
#include "haglbd.h"
#include "haglbd_fmm.h"

static int initialize_modules(void);
static int read_ext_props(boolean_t at_start);
static void shutdown_modules(void);
static void make_daemon(void);
static void init_signal_handlers(void);
static void sig_handler(void *);
static void reload_configuration(void);

boolean_t debug = B_FALSE;

pthread_mutex_t config_lock = PTHREAD_MUTEX_INITIALIZER; /*lint !e708 */

/*
 * Global struct to keep extension properties
 */
ext_props_t g_props;

scds_handle_t scds_handle;

int
main(int argc, char **argv)
{
	sc_syslog_msg_handle_t handle;
	char *progname = argv[0];
	scconf_errno_t scconf_status =	SCCONF_NOERR;

	/*
	 * set up the syslog tag
	 */
	(void) sc_syslog_msg_set_syslog_tag(SC_SYSLOG_HAGLBD_TAG);

	/*
	 * Check if the process was started by the superuser.
	 */
	if (getuid() != 0) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_HAGLBD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "fatal: must be superuser to start %s",
		    progname);
		sc_syslog_msg_done(&handle);
		exit(1);
	}

	/*
	 * daemonize
	 *
	 */
	if (!debug)
		make_daemon();

	/*
	 * we're the child
	 */

	/*
	 * set up the signal handlers
	 */
	init_signal_handlers();

	/*
	 * Process command line arguments
	 */
	if (scds_initialize(&scds_handle, argc, argv)
		!= SCHA_ERR_NOERR) {
		return (1);
	}

	/*
	 * Open the Europa configuration through scconf
	 */
	scconf_status = scconf_libscxcfg_open();
	if (scconf_status != SCCONF_NOERR) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_HAGLBD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Error in initialization; exiting.");
		sc_syslog_msg_done(&handle);
		exit(1);
	}

	/*
	 * Zero out the global extension properties
	 */
	bzero(&g_props, sizeof (g_props));

	/*
	 * read the extension properties
	 */
	if (read_ext_props(B_TRUE) != 0) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_HAGLBD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Error reading properties; exiting.");
		sc_syslog_msg_done(&handle);
		exit(1);
	}

	/*
	 * initialize the modules
	 */
	if (initialize_modules() != 0) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_HAGLBD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Error in initialization; exiting.");
		sc_syslog_msg_done(&handle);
		exit(1);
	}

	/*
	 * The modules initiate their own threads, so we can exit this thread
	 */
	pthread_exit(NULL);

	/*
	 * to make lint happy...
	 */
	return (0);
}


/*
 * make_daemon
 * ----------------------
 * Create a daemon process and detach it from the controlling terminal.
 * Slightly modified from the rgm version in rgm_main.cc
 */
static void make_daemon(void)
{
	struct rlimit	rl;
	int	size;
	int	i;
	int	fd;
	pid_t	pd;
	int	err;
	pid_t	rc;
	sc_syslog_msg_handle_t handle;

	pd = fork();
	if (pd < 0) {
		err = errno; /*lint !e746 */
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_HAGLBD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "fork: %s", strerror(err));
		sc_syslog_msg_done(&handle);
		exit(1);
	}

	/*
	 * parent exits after fork
	 */
	if (pd > 0)
		exit(0); /* parent */

	/*
	 * In the child, redirect stdin to /dev/null so that if
	 * the daemon reads from stdin, it doesn't block.
	 * Redirect stdout, stderr to /dev/console so that any
	 * output printed by the daemon goes somewhere.
	 */

	if ((fd = open("/dev/null", O_RDONLY)) == -1) {
		err = errno;
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_HAGLBD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Unable to open /dev/null: %s", strerror(err));
		sc_syslog_msg_done(&handle);
		exit(1);
	}

	if (dup2(fd, 0) == -1)
		(void) close(fd);

	/*
	 * redirect stdout and stderr by opening /dev/console
	 * and duping to the appropriate descriptors.
	 */
	fd = open("/dev/console", O_WRONLY);
	if (fd == -1) {
		err = errno;
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_HAGLBD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Unable to open /dev/console: %s", strerror(err));
		sc_syslog_msg_done(&handle);
		exit(1);
	}

	if (dup2(fd, 1) == -1 || dup2(fd, 2) == -1)
		(void) close(fd);

	/*
	 * child closes rest of file descriptors
	 */
	rl.rlim_max = 0;
	(void) getrlimit(RLIMIT_NOFILE, &rl);
	if ((size = (int)rl.rlim_max) == 0)
		exit(1);

	for (i = 3; i < size; i++)
		(void) close(i);

	/*
	 * child sets this process as group leader
	 */
	rc = setsid();
	if (rc == (pid_t)-1) {
		err = errno;
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_HAGLBD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "setsid: %s", strerror(err));
		sc_syslog_msg_done(&handle);
		exit(1);
	}
}

/*
 * init_signal_handlers
 * ---------------------
 * Blocks all signals in this thread, which will be the parent of the
 * other threads, so that they will inherit the signal mask.
 *
 * Creates a thread to handle all the signals.  It runs the sig_handler
 * method below.
 *
 * If we fail in any of this, exit the daemon.
 */
static void init_signal_handlers(void)
{
	sc_syslog_msg_handle_t handle;
	pthread_t sig_thr;

	/*
	 * block all signals for all threads
	 */
	sigset_t sig_set;
	int err;

	if (sigfillset(&sig_set) != 0) {
		err = errno;
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_HAGLBD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "sigfillset: %s", strerror(err));
		sc_syslog_msg_done(&handle);
		exit(err);
	}

	if ((err = pthread_sigmask(SIG_BLOCK, &sig_set, NULL)) != 0) {
		char *err_str = strerror(err);
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_HAGLBD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "pthread_sigmask: %s", err_str ? err_str : "");
		sc_syslog_msg_done(&handle);
		exit(err);
	}

	/*
	 * now spawn the thread that will actually handle the signals
	 */

	if ((err = pthread_create(&sig_thr, NULL,
	    (void *(*)(void *))sig_handler, NULL)) != 0) {
		char *err_str = strerror(err);
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_HAGLBD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "pthread_create: %s", err_str ? err_str : "");
		sc_syslog_msg_done(&handle);
		exit(err);
	}
}


/*
 * sig_handler
 * --------------
 * Signal handler for the SIGHUP and SIGTERM signals.
 *
 * For SIGTERM, calls shutdown_modules, then exits the program.
 * For SIGHUP, calls reload_configuration, then continues.
 *
 * This function runs in a dedicated signal handling thread.  The thread
 * should be created with a signal mask blocking all signals.
 *
 * Uses an infinite loop around sigwait, to wait for all signals.
 * Handles TERM and HUP as described.  Exits on all others.
 *
 * If anything goes awry, exits immediately.
 */
/*ARGSUSED*/
static void sig_handler(void * arg)
{
	sc_syslog_msg_handle_t handle;

	/*
	 * block all signals for all threads
	 */
	sigset_t sig_set;
	int err, sig;

	if (sigfillset(&sig_set) != 0) {
		err = errno;
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_HAGLBD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "sigfillset: %s", strerror(err));
		sc_syslog_msg_done(&handle);
		exit(err);
	}

	while (true) {
		if (sigwait(&sig_set, &sig) != 0) {
			err = errno;
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_HAGLBD_TAG, "");
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "sigwait: %s", strerror(err));
			sc_syslog_msg_done(&handle);
			exit(err);
		}

		switch (sig) {
		case SIGTERM:
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_HAGLBD_TAG, "");
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "fatal: received signal %d", sig);
			sc_syslog_msg_done(&handle);

			/*
			 * Attempt to do an orderly shutdown
			 */
			shutdown_modules();

			scds_syslog_debug(1,
			    "Completed shutdown of modules.\n");
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_HAGLBD_TAG, "");
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "Successful shutdown; terminating daemon");
			sc_syslog_msg_done(&handle);

			/*
			 * now exit
			 */
			_exit(1);
			break;
		case SIGHUP:
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_HAGLBD_TAG, "");
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "received signal %d", sig);
			sc_syslog_msg_done(&handle);
			reload_configuration();
			break;
		default:
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_HAGLBD_TAG, "");
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "received signal %d: exiting", sig);
			sc_syslog_msg_done(&handle);
			_exit(1);
		}
	}
}


int
update_nodestable(char * const * nodearray, uint_t nodecount)
{
	uint_t nodeid;
	uint_t i;
	scconf_errno_t err;
	nodelist_t *current;
	nodelist_t *current_ptr;
	sc_syslog_msg_handle_t handle;

	if (debug)
		dump_hashtable(g_props.NodesTable);

	g_props.update++;
	for (i = 0; i < nodecount; i++) {
		err = scconf_get_farmnodeid((char *)nodearray[i], &nodeid);
		if (err) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_HAGLBD_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * Need explanation of this message!
			 * @user_action
			 * Need a user action for this message.
			 */
			(void) sc_syslog_msg_log(handle, LOG_INFO, MESSAGE,
			    "Node %s is not in cluster - Ignoring",
			    nodearray[i]);
			sc_syslog_msg_done(&handle);
			continue;
		}
		if ((current_ptr =
		    isnode_in_hashtable(g_props.NodesTable,
			nodeid)) != NULL) {
			current_ptr->update = g_props.update;
		} else {
			if ((current_ptr = add_node_to_hashtable(
				g_props.NodesTable,
				nodeid,
				(char *)nodearray[i])) == NULL) {
				/* Error */;
				return (-1);
			}
			if (in_fmm_membership(nodeid)) {
				(void) run_lb_command(ADD_COMMAND,
				    current_ptr->nodename);
				current_ptr->status = IN;
			} else {
				(void) run_lb_command(REMOVE_COMMAND,
				    current_ptr->nodename);
				current_ptr->status = OUT;
			}
			current_ptr->update = g_props.update;
		}
	}
	for (i = 0; i < MAX_ENTRIES; i++) {
		current = g_props.NodesTable[i];
		if (current == NULL)
			continue;
		while (current != NULL) {
			if (current->update != g_props.update) {
				if (current->status == IN) {
					(void) run_lb_command(
						REMOVE_COMMAND,
						current->nodename);
					if ((remove_node_from_hashtable(
						g_props.NodesTable,
						current->nodeid,
						current->nodename)) != 0) {
					/* Error  */
						return (-1);
					}
				}
			}
			current = current-> next;
		};
	}

	return (0);
}

/*
 * read_ext_props
 * --------------
 * Reads the extension properties from the resource using scha_resource_get.
 * Returns 0 on success, failure code otherwise.
 * Assumes props is not NULL.
 */
static int read_ext_props(boolean_t at_start)
{
	scha_extprop_value_t	*xprop = NULL;
	scha_err_t	e;
	int err;
	int i;

	e = scds_get_ext_property(scds_handle, "LB_Nodelist",
	    SCHA_PTYPE_STRINGARRAY, &xprop);
	if (e != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to retrieve the "
		    "property %s: %s.", "LB_Nodelist",
		    scds_error_string(e));
		return (1);
	}
	scds_syslog_debug(2, "LB_Nodelist = [%d]",
	    xprop->val.val_strarray->array_cnt);

	if (at_start) {
		for (i = 0; i < MAX_ENTRIES; i++) {
			g_props.NodesTable[i] = NULL;
		}
		err = create_nodestable(g_props.NodesTable,
		    xprop->val.val_strarray->str_array,
		    xprop->val.val_strarray->array_cnt);
		if (err) {
			scds_free_ext_property(xprop);
			return (-1);
		}
	} else {
		err = update_nodestable(xprop->val.val_strarray->str_array,
		    xprop->val.val_strarray->array_cnt);
		if (err != 0) {
			scds_free_ext_property(xprop);
			return (-1);
		}
	}

	if (!g_props.NodesTable) {
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		scds_syslog(LOG_ERR, "Failed to create node table");
		return (1);
	}
	scds_free_ext_property(xprop);

	e = scds_get_ext_property(scds_handle, "LB_IPAddress",
	    SCHA_PTYPE_STRING, &xprop);
	if (e != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to retrieve the "
		    "property %s: %s.", "LB_IPAddress",
		    scds_error_string(e));
		return (1);
	}
	if (!at_start)
		free(g_props.LB_IPAddress);

	g_props.LB_IPAddress = strdup(xprop->val.val_str);
	scds_free_ext_property(xprop);

	e = scds_get_ext_property(scds_handle, "LB_Group",
	    SCHA_PTYPE_STRING, &xprop);
	if (e != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to retrieve the "
		    "property %s: %s.", "LB_Group",
		    scds_error_string(e));
		return (1);
	}

	if (!at_start)
		free(g_props.LB_Group);
	g_props.LB_Group = strdup(xprop->val.val_str);
	scds_free_ext_property(xprop);

	e = scds_get_ext_property(scds_handle, "AddNodeCommand",
	    SCHA_PTYPE_STRING, &xprop);
	if (e != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to retrieve the "
		    "property %s: %s.", "AddNodeCommand",
		    scds_error_string(e));
		return (1);
	}

	if (!at_start)
		free(g_props.AddNodeCommand);
	g_props.AddNodeCommand = strdup(xprop->val.val_str);
	scds_free_ext_property(xprop);

	e = scds_get_ext_property(scds_handle, "RemoveNodeCommand",
	    SCHA_PTYPE_STRING, &xprop);
	if (e != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to retrieve the "
		    "property %s: %s.", "RemoveNodeCommand",
		    scds_error_string(e));
		return (1);
	}

	if (!at_start)
		free(g_props.RemoveNodeCommand);
	g_props.RemoveNodeCommand = strdup(xprop->val.val_str);
	scds_free_ext_property(xprop);

	e = scds_get_ext_property(scds_handle, "SpecificLBInformation",
	    SCHA_PTYPE_STRING, &xprop);
	if (e != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to retrieve the "
		    "property %s: %s.", "SpecificLBInformation",
		    scds_error_string(e));
		return (1);
	}

	if (!at_start)
		free(g_props.SpecificLBInformation);
	g_props.SpecificLBInformation = strdup(xprop->val.val_str);
	scds_free_ext_property(xprop);

	return (0);
}

/*
 * initialize_modules
 * ----------------------
 * Initialize the modules passing the properties from props as required.
 * Assumes props is not NULL, and
 * that all properties are filled in properly.  Returns 0 if all modules
 * initialize successfully, error code otherwise.
 */
static int initialize_modules()
{
	int res;

	scds_syslog_debug(1, "About to init fmm module\n");

	/*
	 * initialize cache module
	 */
	if ((res = fmm_notification_module_initialize())
	    != 0) {
		return (res);
	}
	scds_syslog_debug(1, "Initialized fmm module\n");
	return (0);
}

/*
 * shutdown
 * ----------
 * Attemps to shutdown each module.  Calls the shutdown method on each
 * module.
 */
static void shutdown_modules(void)
{
	scds_syslog_debug(1, "Attempting a gracefull shutdown\n");

	/* Shutdown the FMM notification module */
	fmm_notification_module_shutdown();
}

/*
 * reload_configuration
 * ---------------------
 * Re-reads the extension properties from the resource type and
 * reconfigures the appropriate modules.
 */
static void reload_configuration(void)
{
	sc_syslog_msg_handle_t handle;

	(void) pthread_mutex_lock(&config_lock);
	/*
	 * reread the extension properties
	 */
	if (read_ext_props(B_FALSE) != 0) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_HAGLBD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Error reading properties; using old properties");
		sc_syslog_msg_done(&handle);
		return;
	}

	(void) pthread_mutex_unlock(&config_lock);
}
