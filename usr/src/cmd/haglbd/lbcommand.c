/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

/*
 * lb_command.c
 *
 *	Launch lb reconfiguration command using the FED
 */

#pragma ident	"@(#)lbcommand.c	1.3	08/05/20 SMI"

#include <sys/wait.h>
#include <signal.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <assert.h>
#include <sys/sc_syslog_msg.h>

#include <rgm/fecl.h>
#include "haglbd.h"


#define	EPROG_SUCCESS	0
#define	EPROG_FAILURE	1
#define	EPROG_BAILOUT	2

launch_args_t launch_template;

static int launch_fed_prog(launch_args_t *l);

int
run_lb_command(enum lbcommand command, char *nodename)
{
	launch_args_t l;
	int ret;

	l = launch_template;
	bzero((void *) &l, sizeof (l));

	switch (command) {
	case ADD_COMMAND:
		l.la_progname = g_props.AddNodeCommand;
		break;
	case REMOVE_COMMAND:
		l.la_progname = g_props.RemoveNodeCommand;
		break;
	default:
		assert(0);
	}

	(void) strlcpy(l.la_nodename, nodename,
	    sizeof (l.la_nodename));
	(void) strlcpy(l.la_lbgroup, g_props.LB_Group,
	    sizeof (l.la_lbgroup));
	(void) strlcpy(l.la_lb_ipaddress, g_props.LB_IPAddress,
	    sizeof (l.la_lb_ipaddress));
	(void) strlcpy(l.la_lb_specific_information,
	    g_props.SpecificLBInformation,
	    sizeof (l.la_lb_specific_information));

	(void) strlcpy(l.la_basedir, "/", sizeof ("/"));
	ret = launch_fed_prog(&l);
	return (ret);
}

/*
 * Return 0 on success non-zero on failure.
 */
static int
launch_fed_prog(launch_args_t *l)
{
	char 		fed_opts_tag[64];
	char 		*fed_opts_cmd_path = NULL;
	char 		*fed_opts_argv[256];
	char 		**fed_opts_env;

	fe_cmd		fed_cmd = {0};
	fe_run_result	fed_result = {FE_OKAY, 0, 0, SEC_OK, NULL};

	int		res = 0;
	uint_t		i;
	uint_t		rc = 0;
	char		*err_str;
	sc_syslog_msg_handle_t	handle;


	fed_cmd.host = getlocalhostname();
	if (fed_cmd.host == NULL) {
		(void) sc_syslog_msg_initialize(&handle,
			SC_SYSLOG_HAGLBD_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * The ucmmd was unable to obtain the name of the local host.
		 * Launching of a method failed.
		 * @user_action
		 * Examine other syslog messages occurring at about the same
		 * time to see if the problem can be identified. Save a copy
		 * of the /var/adm/messages files on all nodes and contact
		 * your authorized Sun service provider for assistance in
		 * diagnosing the problem.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			"launch_fed_prog: getlocalhostname() failed "
			"for program <%s>", l->la_progname);
		sc_syslog_msg_done(&handle);
		return (1);
	}

	fed_cmd.timeout = LBMETHOD_TIMEOUT;
	fed_cmd.security = SEC_UNIX_STRONG;

	(void) snprintf(fed_opts_tag, sizeof (fed_opts_tag), "%s.%s",
	    l->la_progname, l->la_nodename);

	/* assemble name of full path */
	fed_opts_cmd_path = fe_method_full_name(l->la_basedir, l->la_progname);
	if (fed_opts_cmd_path == NULL) {
		(void) sc_syslog_msg_initialize(&handle,
			SC_SYSLOG_HAGLBD_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * The ucmmd was unable to assemble the full method pathname
		 * for the fed program to be launched. This is considered a
		 * launch_fed_prog failure.
		 * @user_action
		 * Examine other syslog messages occurring at about the same
		 * time to see if the problem can be identified. Save a copy
		 * of the /var/adm/messages files on all nodes and contact
		 * your authorized Sun service provider for assistance in
		 * diagnosing and correcting the problem.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			"launch_fed_prog: fe_method_full_name() failed "
			"for program <%s>", l->la_progname);
		sc_syslog_msg_done(&handle);
		return (1);
	}
	i = 0;
	fed_opts_argv[i++] = l->la_progname;

	fed_opts_argv[i++] = l->la_nodename;
	fed_opts_argv[i++] = l->la_lbgroup;
	fed_opts_argv[i++] = l->la_lb_ipaddress;
	fed_opts_argv[i++] = l->la_lb_specific_information;

	fed_opts_argv[i] = NULL;

	/*
	 * set environment for method execution
	 */
	if ((fed_opts_env = fe_set_env_vars()) == NULL) {
		(void) sc_syslog_msg_initialize(&handle,
			SC_SYSLOG_HAGLBD_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			"launch_fed_prog: fe_set_env_vars() failed "
			"for program <%s>, node <%s>",
			l->la_progname, l->la_nodename);
		sc_syslog_msg_done(&handle);
		if (fed_opts_cmd_path)
			free(fed_opts_cmd_path);
		return (1);
	}

	fed_cmd.slm_flags = -1; /* bypass SLM handling in fed */

	/* Call the fork execution daemon to run the method */
	if (fe_run(fed_opts_tag, &fed_cmd, fed_opts_cmd_path,
	    fed_opts_argv, fed_opts_env, &fed_result) != 0) {
		(void) sc_syslog_msg_initialize(&handle,
			SC_SYSLOG_HAGLBD_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			"launch_fed_prog: call to rpc.fed failed "
			"for program <%s>",
			l->la_progname);
		sc_syslog_msg_done(&handle);
		if (fed_opts_cmd_path)
			free(fed_opts_cmd_path);
		return (1);
	}

	switch (fed_result.type) {
	case FE_OKAY:
		/*
		 * method_retcode now is the unprocessed return
		 * from wait, which needs to be processed by client;
		 */

		/* rc is used here and re-used below */
		rc = fed_result.method_retcode;
		if (!WIFEXITED(rc)) {
			if (WIFSIGNALED(rc)) {
				(void) sc_syslog_msg_initialize(&handle,
					SC_SYSLOG_HAGLBD_TAG, "");
				/*
				 * SCMSGS
				 * @explanation
				 * Need explanation of this message!
				 * @user_action
				 * Need a user action for this message.
				 */
				(void) sc_syslog_msg_log(handle, LOG_ERR,
					MESSAGE,
					"prog <%s> node <%s> terminated"
					" due to receipt of signal  <%d>",
					l->la_progname,
					l->la_nodename,
					WTERMSIG(rc));
				sc_syslog_msg_done(&handle);
			} else if (WIFSTOPPED(rc)) {
				(void) sc_syslog_msg_initialize(&handle,
					SC_SYSLOG_HAGLBD_TAG, "");
				(void) sc_syslog_msg_log(handle, LOG_ERR,
					MESSAGE,
					"prog <%s> node <%s> terminated"
					" due to receipt of signal  <%d>",
					l->la_progname,
					l->la_nodename,
					WSTOPSIG(rc));
				sc_syslog_msg_done(&handle);
			} else {
				(void) sc_syslog_msg_initialize(&handle,
					SC_SYSLOG_HAGLBD_TAG, "");
				/*
				 * SCMSGS
				 * @explanation
				 * Need explanation of this message!
				 * @user_action
				 * Need a user action for this message.
				 */
				(void) sc_syslog_msg_log(handle, LOG_ERR,
					MESSAGE,
					"prog <%s> node <%s> terminated"
					" due to receipt of signal",
					l->la_progname,
					l->la_nodename);
				sc_syslog_msg_done(&handle);
			}
			res = 1;
			break;
		}

		/*  rc is used above and re-used here */
		rc = WEXITSTATUS(fed_result.method_retcode);
		switch (rc) {
		case EPROG_SUCCESS:
			/* all went well */
			res = 0;
			break;

		case EPROG_FAILURE:
		case EPROG_BAILOUT:
		default:
			(void) sc_syslog_msg_initialize(&handle,
				SC_SYSLOG_HAGLBD_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * Need explanation of this message!
			 * @user_action
			 * Need a user action for this message.
			 */
			(void) sc_syslog_msg_log(handle, LOG_ERR,
				MESSAGE,
				"prog <%s> failed for node <%s> retcode <%d>",
				l->la_progname, l->la_nodename, rc);
			sc_syslog_msg_done(&handle);
			res = (int)rc;
			break;
		}
		break;

	case FE_SLM_ERROR:
		(void) sc_syslog_msg_initialize(&handle,
			SC_SYSLOG_OPS_UCMMD_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR,
			MESSAGE, "Prog <%s> failed to execute "
			"for node <%s> - SLM error",
			l->la_progname, l->la_nodename);
		sc_syslog_msg_done(&handle);
		res = 1;
		break;

	case FE_SYSERRNO:
		if (strerror(fed_result.sys_errno)) {
			(void) sc_syslog_msg_initialize(&handle,
				SC_SYSLOG_HAGLBD_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * Need explanation of this message!
			 * @user_action
			 * Need a user action for this message.
			 */
			(void) sc_syslog_msg_log(handle, LOG_ERR,
				MESSAGE, "Prog <%s> failed to execute "
				"node <%s> - <%s>",
				l->la_progname, l->la_nodename,
				strerror(fed_result.sys_errno));
			sc_syslog_msg_done(&handle);
		} else {
			(void) sc_syslog_msg_initialize(&handle,
				SC_SYSLOG_HAGLBD_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * Need explanation of this message!
			 * @user_action
			 * Need a user action for this message.
			 */
			(void) sc_syslog_msg_log(handle, LOG_ERR,
				MESSAGE, "Prog <%s> failed to execute "
				"node <%s> - error=<%d>",
				l->la_progname, l->la_nodename,
				fed_result.sys_errno);
			sc_syslog_msg_done(&handle);
		}
		res = 1;
		break;

	case FE_DUP:
		(void) sc_syslog_msg_initialize(&handle,
			SC_SYSLOG_HAGLBD_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR,
			MESSAGE,
			"Error: duplicate prog <%s> launched for node <%s>",
			l->la_progname, l->la_nodename);
		sc_syslog_msg_done(&handle);
		res = 1;
		break;

	case FE_UNKNC:
		(void) sc_syslog_msg_initialize(&handle,
			SC_SYSLOG_HAGLBD_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * An internal error in ucmmd has prevented it from
		 * successfully executing a program.
		 * @user_action
		 * Save a copy of /var/adm/messages files on all nodes.
		 * Contact your authorized Sun service provider for assistance
		 * in diagnosing the problem.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR,
			MESSAGE,
			"Prog <%s>: unknown command.",
			l->la_progname);
		sc_syslog_msg_done(&handle);
		res = 1;
		break;

	case FE_ACCESS:
		err_str = security_get_errormsg(fed_result.sec_errno);
		if (err_str) {
			(void) sc_syslog_msg_initialize(&handle,
				SC_SYSLOG_HAGLBD_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * Need explanation of this message!
			 * @user_action
			 * Need a user action for this message.
			 */
			(void) sc_syslog_msg_log(handle, LOG_ERR,
				MESSAGE, "Prog <%s> for node <%s>: "
				"authorization error: %s.",
				l->la_progname, l->la_nodename, err_str);
			sc_syslog_msg_done(&handle);
			free(err_str);
		} else {
			(void) sc_syslog_msg_initialize(&handle,
				SC_SYSLOG_HAGLBD_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * Need explanation of this message!
			 * @user_action
			 * Need a user action for this message.
			 */
			(void) sc_syslog_msg_log(handle, LOG_ERR,
				MESSAGE, "Prog <%s> for node <%s>: "
				"authorization error.",
				l->la_progname, l->la_nodename);
			sc_syslog_msg_done(&handle);
		}
		res = 1;
		break;

	case FE_CONNECT:
		(void) sc_syslog_msg_initialize(&handle,
			SC_SYSLOG_HAGLBD_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR,
			MESSAGE,
			"Prog <%s> for node <%s>: RPC connection error.",
			l->la_progname, l->la_nodename);
		sc_syslog_msg_done(&handle);
		if (fed_result.sys_errno == RPC_PROGNOTREGISTERED) {
			(void) sc_syslog_msg_initialize(&handle,
				SC_SYSLOG_HAGLBD_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * The rpc.fed daemon was not correctly initialized,
			 * or has died. This has caused a step invocation
			 * failure, and may cause the node to reboot.
			 * @user_action
			 * Check that the Solaris Clustering software has been
			 * installed correctly. Use ps(1) to check if rpc.fed
			 * is running. If the installation is correct, the
			 * reboot should restart all the required daemons
			 * including rpc.fed.
			 */
			(void) sc_syslog_msg_log(handle, LOG_ERR,
				MESSAGE,
				"rpc.fed: program not registered "
				"or server not started");
			sc_syslog_msg_done(&handle);
		}
		res = 1;
		break;

	case FE_TIMEDOUT:
		(void) sc_syslog_msg_initialize(&handle,
			SC_SYSLOG_HAGLBD_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR,
			MESSAGE,
			"Prog <%s> for node <%s>: timed out.",
			l->la_progname, l->la_nodename);
		sc_syslog_msg_done(&handle);
		res = 1;
		break;

	case FE_UNKILLABLE:
		(void) sc_syslog_msg_initialize(&handle,
			SC_SYSLOG_HAGLBD_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR,
		    MESSAGE,
		    "Prog <%s> for node <%s>: unkillable.",
		    l->la_progname, l->la_nodename);
		sc_syslog_msg_done(&handle);
		res = 1;
		break;

	case FE_NOTAG:
		(void) sc_syslog_msg_initialize(&handle,
			SC_SYSLOG_HAGLBD_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR,
			MESSAGE, "Prog <%s> for node <%s>: "
			"Execution failed: no such method tag.",
			l->la_progname, l->la_nodename);
		sc_syslog_msg_done(&handle);
		res = 1;
		break;
	case FE_QUIESCED:
		/* FALLTHRU */
	default:
		/* log message and run abort transition */
		(void) sc_syslog_msg_initialize(&handle,
			SC_SYSLOG_HAGLBD_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR,
			MESSAGE,
			"Received unexpected result <%d> from rpc.fed, ",
			fed_result.type);
		sc_syslog_msg_done(&handle);
		res = 1;
		break;
	}

	/* free the xdr resources */
	xdr_free((xdrproc_t)xdr_fe_run_result, (char *)&fed_result);

	if (fed_opts_cmd_path)
		free(fed_opts_cmd_path);
	return (res);
}
