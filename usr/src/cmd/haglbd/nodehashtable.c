/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)nodehashtable.c 1.2	08/05/20 SMI"
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <malloc.h>
#include <strings.h>
#include <scadmin/scconf.h>
#include "nodehashtable.h"
#include "haglbd.h"


nodelist_t *
add_node_to_nodelist(nodelist_t ** nodelist_head,
    uint_t nodeid, char *nodename)
{
	nodelist_t *newelement;

	newelement = (nodelist_t *)malloc(sizeof (nodelist_t));

	if (newelement == NULL)
		return (NULL);

	newelement->nodeid = nodeid;
	newelement->nodename = strdup(nodename);
	newelement-> status = UNINIT;
	newelement-> update = 0;
	newelement->next = *nodelist_head;
	*nodelist_head = newelement;

	return (newelement);
}

int
remove_node_from_nodelist(nodelist_t ** nodelist_head,
    uint_t nodeid, char *nodename)
{

	nodelist_t *current, *previous, *tmp;

	current = *nodelist_head;
	previous = *nodelist_head;

	while (current) {
		if ((current->nodeid == nodeid) &&
		    (strcmp(current->nodename, nodename) == 0)) {
			free(current->nodename);
			tmp = current->next;
			free(current);
			previous->next = tmp;
			break;
		}
		previous = current;
		current = current->next;
	}
	return (0);
}

nodelist_t *isnode_in_nodelist(nodelist_t **nodelist_head,
    uint_t nodeid)
{
	nodelist_t *current;

	current = *nodelist_head;

	while (current) {
		if (current->nodeid == nodeid) {
			break;
		}
		current = current->next;
	}
	return (current);
}

int
nodehashvalue(uint_t nodeid)
{
	return (nodeid % MAX_ENTRIES);
}

nodelist_t *
add_node_to_hashtable(nodelist_t **nodehashtable,
    uint_t nodeid, char *nodename)
{
	nodelist_t *nl = NULL;
	int hashindex;
	nodelist_t	** node_element_ptr;

	hashindex = nodehashvalue(nodeid);

	node_element_ptr = &nodehashtable[hashindex];
	nl = add_node_to_nodelist(node_element_ptr, nodeid,
	    nodename);

	return (nl);
}

int
remove_node_from_hashtable(nodelist_t ** nodehashtable,
    uint_t nodeid, char *nodename)
{
	int rc = -1;
	int hashindex;

	hashindex = nodehashvalue(nodeid);

	rc = remove_node_from_nodelist(&nodehashtable[hashindex],
	    nodeid, nodename);

	return (rc);
}

nodelist_t  *
isnode_in_hashtable(nodelist_t ** nodehashtable, uint_t nodeid)
{
	int hashindex;

	hashindex = nodehashvalue(nodeid);

	return (isnode_in_nodelist(&nodehashtable[hashindex],
	    nodeid));
}

int
create_nodestable(nodelist_t ** nodehashtable, char * const * nodearray,
    uint_t nodecount)
{
	uint_t nodeid;
	uint_t i;
	scconf_errno_t err;
	sc_syslog_msg_handle_t handle;

	if (!nodehashtable)
		return (-1);

	for (i = 0; i < nodecount; i++) {
		err = scconf_get_farmnodeid((char *)nodearray[i], &nodeid);
		if (err) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_HAGLBD_TAG, "");
			(void) sc_syslog_msg_log(handle, LOG_INFO, MESSAGE,
			    "Node %s is not in cluster - Ignoring",
			    nodearray[i]);
			sc_syslog_msg_done(&handle);
			continue;
		}
		if (debug)
			(void) printf("Adding node %s nodeid "
			    "%d to the hashtable\n",
			    nodearray[i],
			    nodeid);
		if ((add_node_to_hashtable(nodehashtable,
		    nodeid, (char *)nodearray[i])) == NULL) {
			/* Error */
			return (-1);
		}
	}

	return (0);
}

void
dump_hashtable(nodelist_t ** nodehashtable)
{
	int i;
	nodelist_t *current;

	for (i = 0; i < MAX_ENTRIES; i++) {
		current = nodehashtable[i];
		if (current == NULL) {
			/* Do not dump empty entries */
			continue;
		}
		while (current) {
			(void) printf("Entry %d: \n", i);
			(void) printf("  NodeId: %d\n",
			    current->nodeid);
			(void) printf("  Nodename: %s\n", current->nodename);
			(void) printf("  Status: %d\n", current->status);
			(void) printf("  Update: %d\n", current->update);
			(void) printf("--------------------------------\n\n");
			current = current-> next;
		}
	}
}
