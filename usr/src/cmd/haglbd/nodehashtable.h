/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_NODEHASHTABLE_H
#define	_NODEHASHTABLE_H

#pragma ident	"@(#)nodehashtable.h	1.3	08/05/20 SMI"

#define	MAX_ENTRIES 512

typedef struct nodelist
{
	uint_t nodeid;
	char *nodename;
	int status;
	int update;
	struct nodelist *next;
} nodelist_t;

typedef nodelist_t * nodehashtable_t[];

int create_nodestable(nodelist_t ** nodehashtable,
    char * const * nodearray, uint_t nodecount);

nodelist_t * isnode_in_hashtable(nodelist_t ** nodehashtable,
    uint_t nodeid);

int remove_node_from_nodelist(nodelist_t ** nodelist_head,
    uint_t nodeid, char * nodename);

nodelist_t * add_node_to_hashtable(nodelist_t ** nodehashtable,
    uint_t nodeid, char * nodename);

int remove_node_from_hashtable(nodelist_t ** nodehashtable,
    uint_t nodeid, char * nodename);

void dump_hashtable(nodelist_t ** nodehashtable);

#endif	/* _NODEHASHTABLE_H */
