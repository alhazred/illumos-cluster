/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_HAGLBD_H
#define	_HAGLBD_H

#pragma ident	"@(#)haglbd.h	1.3	08/05/20 SMI"

#include <stdio.h>
#include <stdlib.h>
#include <syslog.h>
#include <stdarg.h>
#include <strings.h>
#include "nodehashtable.h"

#define	SC_SYSLOG_HAGLBD_TAG "Cluster.HAGLB.haglbd"

#define	UNINIT 0
#define	IN 1
#define	OUT 2

typedef enum lbcommand {
	ADD_COMMAND,
	REMOVE_COMMAND
} lbcommand_t;

typedef struct {
	char    * la_progname;
	char	la_nodename[MAXNAMELEN];
	char	la_lbgroup[MAXNAMELEN];
	char	la_lb_ipaddress[MAXNAMELEN];
	char	la_lb_specific_information[MAXNAMELEN];
	char	la_basedir[MAXPATHLEN];
} launch_args_t;

#define	LBMETHOD_TIMEOUT 20

extern pthread_mutex_t config_lock;	/* lock around config */

typedef struct ext_props
{
	char 	*LB_IPAddress;
	char 	*LB_Group;
	char 	*AddNodeCommand;
	char 	*RemoveNodeCommand;
	char 	*SpecificLBInformation;
	nodelist_t * NodesTable[MAX_ENTRIES];
	int	update;
} ext_props_t;

extern ext_props_t g_props;

extern boolean_t debug;

int run_lb_command(enum lbcommand command, char *nodename);

#endif	/* _HAGLBD_H */
