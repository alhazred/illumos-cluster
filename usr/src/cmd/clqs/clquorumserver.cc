//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clquorumserver.cc	1.16	08/09/19 SMI"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <strings.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <ctype.h>

#include <libintl.h>
#include <regex.h>
#include <locale.h>
#include <quorum/quorum_server/qs_socket.h>
#include <quorum/quorum_server/qd_qs_msg.h>
#include <ClusterWorker.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/cl_auth.h>
#include <cl_errno.h>
#include <clgetopt.h>

#define	QSD_DIR		"/etc/scqsd/"
#define	QSD_CONFIG	"/etc/scqsd/scqsd.conf"
#define	SERVICE		"scqsd"
#define	SERVICE_PROTO	"tcp"
#define	DEFAULT_PORT	9000
#define	WILDCARD	"+"
#define	DFT_DIR		"/var/scqsd"

#define	FLG_STOP	1	// stop subcommand
#define	FLG_SHOW	2	// show subcommand
#define	FLG_CLEAR	3	// clear subcommand
#define	FLG_START	4	// start subcommand

#define	QS_VERS		"1.0"

typedef uint32_t optflgs_t;
#define	hflg 	(1 << 0)	// --help
#define	Vflg 	(1 << 1)	// --version
#define	vflg 	(1 << 2)	// --verbose
#define	yflg 	(1 << 3)	// --yes
#define	dflg	(1 << 4)	// --disable
#define	lflg	(1 << 5)	// --limited

static char *progname;

static void usage(void);
static void show_usage(void);
static void start_usage(void);
static void stop_usage(void);
static void clear_usage(void);
static int process_one_daemon(const char *server_hostname,
    const char *client_hostname, char *port, const char *cluster_name,
    uint32_t clusterID, uint_t cmdflg, optflgs_t optflgs);
static boolean_t chk_duplicate(const char *arg, char **args,
    uint_t args_count);
static void clerror(char *message_in, ...);
static char *clcommand_option(int optopt, const char *argument);
static int get_default_port(uint_t *default_port);
static int clqs_common_options(int argc, char **argv, uint_t cmd_flg,
    optflgs_t *optflgs, char **operands, uint_t *operand_count);
static int clqs_clear_options(int argc, char **argv, char **cluster_name,
    uint32_t &clusterID, optflgs_t *optflgs, char **operands);
static bool enable_qs(const char *port);
static bool disable_qs(const char *port);
static bool is_qs_enabled(const char *port);
uint_t dft_port_num;

int
main(int argc, char **argv)
{
	FILE *fd = NULL;
	char line[BUFSIZ];
	char line_bk[BUFSIZ];
	int clerrno = CL_NOERR;
	int first_err = CL_NOERR;
	int found = 0;
	uint_t i, qs_args_count;
	char *port = NULL;
	char *instance = NULL;
	char *tmp = (char *)0;
	char tmp_str[BUFSIZ];
	char server_hostname[BUFSIZ];
	char client_hostname[BUFSIZ];
	char *operands[BUFSIZ];
	int qs_done[BUFSIZ];
	char *cluster_name = NULL;
	uint32_t clusterID = 0;
	optflgs_t optflgs = 0;
	uint_t cmdflg = 0;
	char *qs_dirs[BUFSIZ];
	uint_t qs_dir_count = 0;

	// Initialize and demote to normal uid */
	cl_auth_init();
	cl_auth_demote();

	// Set the program name
	progname = strrchr(argv[0], '/');
	if (progname == NULL) {
		progname = argv[0];
	} else {
		++progname;
	}

	// I18N housekeeping
	(void) setlocale(LC_ALL, "");
	(void) textdomain(TEXT_DOMAIN);
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	server_hostname[0] = '\0';
	client_hostname[0] = '\0';
	qs_args_count = 0;

	// Process arguments
	if (argc == 1) {
		optflgs |= hflg;
	} else if (argc >= 2) {
		if (strcasecmp(argv[1], "show") == 0) {
			clerrno = get_default_port(&dft_port_num);
			if (clerrno != CL_NOERR) {
				return (clerrno);
			}
			clerrno = clqs_common_options(argc, argv,
			    FLG_SHOW, &optflgs, operands, &qs_args_count);
			cmdflg = FLG_SHOW;
		} else if (strcasecmp(argv[1], "start") == 0) {
			clerrno = get_default_port(&dft_port_num);
			if (clerrno != CL_NOERR) {
				return (clerrno);
			}
			clerrno = clqs_common_options(argc, argv,
			    FLG_START, &optflgs, operands, &qs_args_count);
			cmdflg = FLG_START;
		} else if (strcasecmp(argv[1], "stop") == 0) {
			clerrno = get_default_port(&dft_port_num);
			if (clerrno != CL_NOERR) {
				return (clerrno);
			}
			clerrno = clqs_common_options(argc, argv,
			    FLG_STOP, &optflgs, operands, &qs_args_count);
			cmdflg = FLG_STOP;
		} else if (strcasecmp(argv[1], "clear") == 0) {
			clerrno = get_default_port(&dft_port_num);
			if (clerrno != CL_NOERR) {
				return (clerrno);
			}
			clerrno = clqs_clear_options(argc, argv,
			    &cluster_name, clusterID, &optflgs,
			    operands);
			if ((clerrno != CL_NOERR) ||
			    !(optflgs & yflg)) {
				return (clerrno);
			}
			qs_args_count = 1;
			cmdflg = FLG_CLEAR;
		} else if ((strcmp(argv[1], "-V") == 0) ||
			    (strcasecmp(argv[1], "--version") == 0)) {
				optflgs |= Vflg;
		} else if ((strcmp(argv[1], "-?") == 0) ||
		    (strcasecmp(argv[1], "--help") == 0)) {
			optflgs |= hflg;
		} else {
			clerror(gettext(
			    "Unrecognized subcommand - \"%s\".\n\n"), argv[1]);
			usage();
			return (CL_EINVAL);
		}

		// Subcommand problem?
		if (cmdflg && (!qs_args_count || (clerrno != CL_NOERR))) {
			return (clerrno);
		}
	}

	// Checks for the -V anf -?
	if (optflgs & Vflg) {
		if (argc > 2) {
			clerror(gettext("Usage error.\n"));
			usage();
			return (CL_EINVAL);
		} else {
			(void) fprintf(stdout, gettext(
			    "%s %s\n"), progname, QS_VERS);
			return (clerrno);
		}
	} else if (optflgs & hflg) {
		if (argc > 2) {
			clerror(gettext("Usage error.\n"));
			clerrno = CL_EINVAL;
		}
		usage();
		return (clerrno);
	}

	// Check for rbac authorization
	if (cmdflg == FLG_STOP || cmdflg == FLG_START ||
	    cmdflg == FLG_CLEAR) {
		cl_auth_check_command_opt_exit(progname, NULL,
		    "solaris.cluster.admin", 1);
	} else if (cmdflg == FLG_SHOW) {
		cl_auth_check_command_opt_exit(progname, NULL,
		    "solaris.cluster.read", 1);
	}

	// promote to euid=0
	cl_auth_promote();

	// Get the hostname. Client host is the locale host.
	(void) gethostname(client_hostname, sizeof (client_hostname));
	if (!server_hostname[0]) {
		(void) strcpy(server_hostname, client_hostname);
	}

	// Open the scqsd config file
	if ((fd = fopen(QSD_CONFIG, "r")) == 0) {
		clerror(gettext("Cannot open file \"%s\".\n"),
		    QSD_CONFIG);
		clerrno = CL_EINVAL;
		goto cleanup;
	}

	// Initialize
	for (i = 0; i < qs_args_count; i++) {
		qs_done[i] = 0;
	}

	// Go through all the lines
	line[0] = '\0';
	while (fgets(line, sizeof (line), fd)) {
		// skip comment line
		if (*line == '#')
			continue;
		if (!strstr(line, SERVICE))
			continue;

		// Process the start subcommand here
		if (cmdflg == FLG_START) {
			// Save the line
			(void) sprintf(line_bk, "%s", line);
		}

		found = 0;

		// Get the port number
		port = strdup(strstr(line, "-p "));
		if (port) {
			char *ptr;

			// Get the port string
			port += 2;
			while (*port == ' ')
				port++;
			ptr = port;
			while ((*ptr != ' ') &&
			    (*ptr != '\n') &&
			    (*ptr != '\0'))
				ptr++;

			if ((*ptr == ' ') || (*ptr == '\n')) {
				*ptr = '\0';
			}

			// Invalid port?
			for (ptr = port;  *ptr;  ++ptr) {
				if (!isdigit(*ptr))
					break;
			}
			if ((*ptr != '\0') || (atoi(port) == 0)) {
				bool matched = false;
				for (i = 0; i < qs_args_count; i++) {
					if (strcmp(port,
					    operands[i]) == 0) { //lint !e644
						matched = true;
						break;
					}
				}
				if (matched) {
					clerror(gettext(
					    "Invalid port \"%s\" in file "
					    "\"%s\".\n"),
					    port, QSD_CONFIG);
					if (first_err == CL_NOERR) {
						first_err = CL_EINVAL;
					}
					qs_done[i] = 1;
				}
				continue;
			}
			found = 1;
		}

		// Get the instance name
		tmp = strstr(line, "-i ");
		if (tmp) {
			tmp++;
			while (*++tmp == ' '); //lint !e722
			instance = tmp;
			while (*tmp != ' ' && *tmp != '\n') tmp++;
			*tmp = '\0';
		}

		// Use default port?
		if (!found) {
			(void) sprintf(tmp_str, "%d", dft_port_num);
			port = tmp_str;
		}

		// Go through the QS arguments
		for (i = 0; i < qs_args_count; i++) {
			// Skip if processed
			if (qs_done[i] != 0)	//lint !e771
				continue;

			if ((port && (strcmp(operands[i], port) == 0)) ||
			    (instance &&
			    (strcmp(operands[i], instance) == 0))) {
				// For start subcommand
				if (cmdflg == FLG_START) {
					bool getdir = false;

					//
					// Prepare the command to run.
					// The line will be appended with
					// " >/dev/null 2>&1" so the
					// extra length here.
					//
					char *line_sv = new char[
					    strlen(line_bk) + 17]; //lint !e645
					*line_sv = '\0';
					(void) strcpy(line_sv, line_bk);

					// Remove '\n'.
					if (line_sv[strlen(line_bk) - 1] ==
					    '\n') {
						line_sv[strlen(line_bk) - 1] =
						    '\0';
					}

					// Get the first entry in the line
					char *chk_str = strtok(line_bk, " ");

					// This should be the QS binary.
					if (chk_str && (access(chk_str,
					    (R_OK | X_OK)) != 0)) {
						clerror(gettext(
						    "Cannot execute the "
						    "\"%s\" line in file "
						    "\"%s\".\n"),
						    line_sv, QSD_CONFIG);
						if (first_err == CL_NOERR) {
							first_err = CL_EINVAL;
						}
						qs_done[i] = 1;
						delete [] line_sv;
						continue;
					}

					// Get the output directory
					while (1) {
						chk_str = strtok(NULL, " ");
						if (!chk_str)
							break;
						if (strcmp(chk_str, "-d")
						    == 0) {
							// Get the argument
							chk_str = strtok(NULL,
							    " ");
							if (chk_str) {
								getdir = true;
							}
							break;
						}
					}
					if (!getdir) {
						// Use the default direcotry
						chk_str = new char[
						    strlen(DFT_DIR) + 1];
						(void) strcpy(chk_str, DFT_DIR);
					}
					// Check the output directory
					if (chk_duplicate(chk_str,
					    qs_dirs, qs_dir_count)) {
						if (!getdir) {
							clerror(gettext(
							"The default quorum "
							"directory in line "
							"\"%s\" is already "
							"used.\n"),
							line_sv);
						} else {
							clerror(gettext(
							"Quorum directory "
							"\"%s\" in line "
							"\"%s\" is not "
							"unique.\n"),
							chk_str, line_sv);
						}
						if (first_err == CL_NOERR) {
							first_err = CL_EINVAL;
						}
						qs_done[i] = 1;
						delete [] line_sv;
						continue;
					} else {
						qs_dirs[qs_dir_count] =
						new char[strlen(chk_str) + 1];
						(void) strcpy(
						    qs_dirs[qs_dir_count++],
						    chk_str);
						if (!getdir) {
							delete [] chk_str;
						}
					}

					//
					// If it is a limited start, then if
					// the lock file exists, do not start
					// the QS
					//
					bool qs_enabled = is_qs_enabled(port);
					if ((optflgs & lflg) &&
					    (!qs_enabled)) {
						// Do not start the qs
						clerrno = CL_NOERR;
					} else {
						// Start this quorum server
						(void) strcat(line_sv,
						    " >/dev/null 2>&1");
						clerrno = system(line_sv);
						delete [] line_sv;
						(void) enable_qs(port);
					}
					// Errors?
					if (clerrno != CL_NOERR) {
						clerror(gettext(
						    "Failed to start "
						    "quorum server on "
						    "port \"%s\". Refer "
						    "to system log for "
						    "details.\n"),
						    port);
						clerrno = CL_EINVAL;
					} else if (optflgs & vflg) {
						(void) fprintf(stdout,
						    gettext("Quorum "
						    "server on port "
						    "\"%s\" is started.\n"),
						    port);
					}
				} else if (cmdflg == FLG_CLEAR) {
					clerrno = process_one_daemon(
					    server_hostname,
					    client_hostname, port,
					    cluster_name, clusterID,
					    FLG_CLEAR, optflgs);
				} else if (cmdflg == FLG_STOP) {
					clerrno = process_one_daemon(
					    server_hostname,
					    client_hostname, port,
					    NULL, NULL,
					    FLG_STOP, optflgs);
					if ((clerrno == CL_NOERR) &&
					    (optflgs & dflg)) {
						bool ret = disable_qs(port);
						if (!ret) {
							clerror(gettext(
							    "Failed to "
							    "disable quorum "
							    "server on port "
							    "\"%s\".\n"),
							    port);
						}
					}
				} else {
					// FLG_SHOW
					clerrno = process_one_daemon(
					    server_hostname,
					    client_hostname, port,
					    NULL, NULL,
					    cmdflg, optflgs);
				}
				if (clerrno != CL_NOERR &&
				    first_err == CL_NOERR) {
					first_err = clerrno;
				}
				qs_done[i] = 1;
			}
		}
	}

	// If some operands are not processed?
	for (i = 0; i < qs_args_count; i++) {
		if (qs_done[i] == 0) {
			clerror(gettext("Quorum server \"%s\" is not "
			    "configured in file \"%s\".\n"),
			    operands[i], QSD_CONFIG);
			if (first_err == CL_NOERR) {
				first_err = CL_ENOENT;
			}
		}
	}

cleanup:
	if (fd) {
		(void) fclose(fd);
	}

	// Delete memory
	if (cluster_name) {
		delete [] cluster_name;
	}
	for (i = 0; i < qs_dir_count; i++) {
		if (qs_dirs[i]) {	//lint !e644
			delete [] qs_dirs[i];
		}
	}

	// Return the first error
	if (first_err != CL_NOERR) {
		clerrno = first_err;
	}
	return (clerrno);
}

static void
usage()
{
	(void) fprintf(stderr, gettext(
	    "Usage: %s <subcommand> [<quorumserver> ...]\n"),
	    progname);
	(void) fprintf(stderr, gettext("       %s <subcommand> -? | --help\n"),
	    progname);
	(void) fprintf(stderr, gettext("       %s -V | --version\n"),
	    progname);
	(void) fprintf(stderr, gettext(
	    "\nManage quorum servers\n\nSUBCOMMANDS:\n"));
	(void) fprintf(stderr, gettext(
	    "\nstart\tStart the quorum servers"));
	(void) fprintf(stderr, gettext(
	    "\nstop\tStop the quorum servers"));
	(void) fprintf(stderr, gettext(
	    "\nshow\tShow the configuration of the quorum servers"));
	(void) fprintf(stderr, gettext(
	    "\nclear\tCleanup the cluster configuration for a "
	    "quorum server\n\n"));
}

static void
show_usage()
{
	(void) fprintf(stderr,
	    "\n%s: %s show [<options>] [%s | <quorumserver> ...]\n",
	    gettext("Usage"),
	    progname, WILDCARD);
	(void) fprintf(stderr, gettext(
	    "\nShow the configuration of the quorum servers\n"));
	(void) fprintf(stderr, gettext("\nOPTIONS:\n"));
	(void) fprintf(stderr, gettext(
	    "\n  -?\tDisplay help message\n"));
	(void) fprintf(stderr, gettext(
	    "\n  -v\tVerbose output\n\n\n"));
}

static void
start_usage()
{
	(void) fprintf(stderr,
	    "\n%s: %s start [<options>] %s | <quorumserver> ...\n",
	    gettext("Usage"),
	    progname, WILDCARD);
	(void) fprintf(stderr, gettext("\nStart the quorum servers\n"));
	(void) fprintf(stderr, gettext("\nOPTIONS:\n"));
	(void) fprintf(stderr, gettext(
	    "\n  -?\tDisplay help message\n"));
	(void) fprintf(stderr, gettext(
	    "\n  -v\tVerbose output\n\n\n"));

	//
	// This command takes a -l option
	// but is not a documented option
	//
}

static void
stop_usage()
{
	(void) fprintf(stderr,
	    "\n%s: %s stop [<options>] %s | <quorumserver> ...\n",
	    gettext("Usage"),
	    progname, WILDCARD);
	(void) fprintf(stderr, gettext("\nStop the quorum servers\n"));
	(void) fprintf(stderr, gettext("\nOPTIONS:\n"));
	(void) fprintf(stderr, gettext(
	    "\n  -?\tDisplay help message\n"));
	(void) fprintf(stderr, gettext(
	    "\n  -d\tDisable quorum server\n"));
	(void) fprintf(stderr, gettext(
	    "\n  -v\tVerbose output\n\n\n"));
}

static void
clear_usage()
{
	(void) fprintf(stderr,
	    "\n%s: %s clear <options> <quorumserver>\n",
	    gettext("Usage"),
	    progname);
	(void) fprintf(stderr, gettext("\nClear the cluster configuration "
	    "for a quorum server\n"));
	(void) fprintf(stderr, gettext("\nOPTIONS:\n"));
	(void) fprintf(stderr, gettext(
	    "\n  -?\tDisplay help message\n"));
	(void) fprintf(stderr, gettext(
	    "\n  -c <clustername>\n\tSpecify the cluster name\n"));
	(void) fprintf(stderr, gettext(
	    "\n  -I <clusterID>\n\tSpecify the cluster ID "
	    "as a hexadecimal\n"));
	(void) fprintf(stderr, gettext(
	    "\n  -y\tAutomatically respond \"yes\" to the confirmation "
	    "question\n"));
	(void) fprintf(stderr, gettext(
	    "\n  -v\tVerbose output\n\n\n"));
}

uint64_t
key_value(const quorum::registration_key_t &key)
{
	uint64_t keyval = 0;
	for (int j = 0; j < 8; j++) {
		keyval = (keyval << 8) + key.key[j];
	}

	return (keyval);
}

/*
 * chk_duplicate
 *
 *	Check if arg is already in args. If it is there,
 *	return true; otherwise returns false.
 */
boolean_t
chk_duplicate(const char *arg, char **args, uint_t args_count)
{
	uint_t i;
	boolean_t found = B_FALSE;

	/* Check for arguments */
	if (!arg || !args || !args_count) {
		return (B_FALSE);
	}

	for (i = 0; i < args_count; i++) {
		if (!args[i])
			continue;
		if (strcmp(args[i], arg) == 0) {
			found = B_TRUE;
			break;
		}
	}

	return (found);
}

/*
 * process_one_daemon
 *
 *	Send the status request to the quorum server process that uses
 *	the given port number, and receive the status back from it.
 *
 */
static int
process_one_daemon(const char *server_hostname, const char *client_hostname,
    char *port, const char *cluster_name, uint32_t clusterID, uint_t cmdflg,
    optflgs_t optflgs)
{
	int sock_fd = -1;
	struct in6_addr serv_addr;
	struct sockaddr_in6 serv_sockaddr;
	struct hostent *ipnode_p;
	struct hostent *host_p;
	char **ptr;
	char serv_ipaddr[INET6_ADDRSTRLEN];
	uint_t addr_num;
	uint_t port_num;
	uint_t recv_size;
	char *status_m;
	qd_qs_msghdr *status_msg = NULL;
	int clerrno = CL_NOERR;

	/* Check parameters */
	if (!server_hostname || !client_hostname || !port) {
		clerror(gettext("Internal error.\n"));
		return (CL_EINTERNAL);
	}

	if ((cmdflg == FLG_CLEAR) && (!cluster_name || !clusterID)) {
		clerror(gettext("Internal error.\n"));
		return (CL_EINTERNAL);
	}

	/* Get the port number */
	if (sscanf(port, "%d", &port_num) != 1) {
		clerror(gettext("Invalid port \"%s\".\n"),
		    port);
		return (CL_EINVAL);
	}

	/* Check server address */
	bzero((char *)&serv_sockaddr, sizeof (serv_sockaddr));
	serv_sockaddr.sin6_family = AF_INET6;
	serv_sockaddr.sin6_port = (in_port_t)htons(port_num);

	/* Print the show header */
	if (cmdflg == FLG_SHOW) {
		// Print the header
		(void) fprintf(stdout, gettext(
		    "=== Quorum Server on port %s ===\n"), port);
		if (is_qs_enabled(port)) {
			(void) fprintf(stdout,
			    gettext("\nDisabled\t\t\tFalse\n\n"));
		} else {
			(void) fprintf(stdout,
			    gettext("\nDisabled\t\t\tTrue\n\n"));
		}
	}

	int error;
	if ((ipnode_p = getipnodebyname(server_hostname, AF_INET,
		    AI_ALL | AI_ADDRCONFIG | AI_V4MAPPED, &error)) == NULL) {
		clerror(gettext("Cannot get the IP node for host \"%s\".\n"),
		    server_hostname);
		return (CL_EINTERNAL);
	}

	ptr = ipnode_p->h_addr_list;
	struct in6_addr in6;
	bcopy(*ptr, (caddr_t)&in6, (size_t)ipnode_p->h_length);

	host_p = ipnode_p;
	(void) inet_ntop(AF_INET, (void *)&in6, serv_ipaddr,
	    sizeof (serv_ipaddr));

	/* Use appropriate type - depending on IPv6 or IPv4 */
	if (inet_pton(AF_INET, serv_ipaddr, &serv_addr) == 1) {
		/* IPv4 */
		(void) memcpy((char *)&addr_num, host_p->h_addr, 4);
		IN6_IPADDR_TO_V4MAPPED(addr_num, &(serv_sockaddr.sin6_addr));
	} else if (inet_pton(AF_INET6, serv_ipaddr, &serv_addr) == 1) {
		/* IPv6 */
		serv_sockaddr.sin6_addr = serv_addr;
	}

	freehostent(host_p);

	/* Create a TCP socket */
	if ((sock_fd = socket(AF_INET6, SOCK_STREAM, 0)) == -1) {
		clerror(gettext("Cannot create socket on port \"%s\".\n"),
		    port);
		return (CL_EINTERNAL);
	}

	if (connect(sock_fd, (struct sockaddr *)&serv_sockaddr,
	    sizeof (serv_sockaddr)) < 0) {
		if (cmdflg == FLG_STOP) {
			/* Not running; already stopped */
			if (optflgs & vflg) {
				(void) fprintf(stdout, gettext(
				    "Quorum server on port \"%s\" "
				    "is not running.\n"), port);
			}
			return (CL_NOERR);
		}
		if (is_qs_enabled(port)) {
			clerror(gettext("Quorum server is not yet started on "
			    "port \"%s\".\n\n"),
			    port);
			return (CL_ESTATE);
		} else {
			return (CL_NOERR);
		}
	}

	if (cmdflg == FLG_SHOW) {
		status_msg = new qd_qs_msghdr("cmd_clqs", 0, 0, QD_STATUS);
	} else if (cmdflg == FLG_STOP) {
		status_msg = new qd_qs_msghdr("cmd_clqs", 0, 0, QD_STOP);
	} else if (cmdflg == FLG_CLEAR) {
		status_msg = new qd_qs_msghdr(cluster_name, clusterID,
		    0, QD_REMOVE);
	}

	if (status_msg == NULL) {
		clerror(gettext("Not enough memory.\n"));
		return (CL_ENOMEM);
	}

	if (send(sock_fd, status_msg, sizeof (qd_qs_msghdr), 0) == -1) {
		clerror(gettext("Cannot send messages on port \"%s\".\n"),
		    port);
		if (status_msg) {
			delete status_msg;
		}
		return (CL_EINTERNAL);
	}

	qd_qs_msghdr result_status;
	result_status.set_error(QD_QS_INVALID);

	if (recv(sock_fd, &result_status, sizeof (qd_qs_msghdr), 0) < 0) {
		clerror(gettext("Cannot receive messages on port \"%s\".\n"),
		    port);
		clerrno = CL_EINTERNAL;
		goto cleanup;
	}

	//
	// Perform validity checks on received message.
	//
	if ((result_status.get_error() != QD_QS_SUCCESS) ||
	    (result_status.get_version_major() !=
		(uint16_t)SCQSD_VERSION_MAJOR) ||
	    (result_status.get_version_minor() !=
		(uint16_t)SCQSD_VERSION_MINOR) ||
	    (!result_status.is_cookie_set())) {
		if (cmdflg == FLG_CLEAR) {
			clerror(gettext("The cluster name or ID "
			    "is not configured for quorum server "
			    "on port \"%s\".\n"), port);
			clerrno = CL_EINVAL;
		} else {
			clerror(gettext(
			    "Cannot get length of received data.\n"));
			clerrno = CL_EINTERNAL;
		}
		goto cleanup;
	}

	if (cmdflg != FLG_SHOW) {
		if (optflgs & vflg) {
			if (cmdflg == FLG_STOP) {
				(void) fprintf(stdout, gettext(
				    "Quorum server on port \"%s\" "
				    "is stopped.\n"), port);
			} else if (cmdflg == FLG_CLEAR) {
				(void) fprintf(stdout, gettext(
				    "Quorum server on port \"%s\" "
				    "is cleared.\n"), port);
			}
		}
		goto cleanup;
	}

	/* Obtain the length of data received */
	recv_size = result_status.get_length();

	/* Get the message */
	if (recv_size > 0) {
		/* Allocate space */
		status_m = (char *)malloc(sizeof (char) * recv_size);
		if (status_m == NULL) {
			goto cleanup;
		}
		/* Read in the remaining msg here */
		if (recv(sock_fd, status_m, recv_size, 0) != (int)recv_size) {
			clerror(gettext(
			    "Cannot receive messages on port \"%s\".\n"),
			    port);
			clerrno = CL_EINTERNAL;
			goto cleanup;
		}

		struct quorum_status *statusp = (quorum_status *)status_m;
		//
		// Display stat result
		//
		if (cmdflg == FLG_SHOW) {

			// Print the cluster and nodes
			for (int i = 0;
			    i < (int)result_status.get_data_length();
			    i++) {

				(void) fprintf(stdout, gettext(
				    "\n  ---  Cluster %s (id 0x%8.8X) "
				    "Reservation ---\n"),
				    statusp[i].cluster_name,
				    statusp[i].cluster_id);

				// Reservations
				if (statusp[i].reserved_nid != 0) {
					(void) fprintf(stdout,
					    gettext(
					    "\n  Node ID:\t\t\t%d\n"),
					    statusp[i].reserved_nid);

					(void) fprintf(stdout,
					    gettext("    Reservation "
					    "key:\t\t0x%llx\n"),
					    key_value(statusp[i].
						reservation_key));
				}

				// Registrations
				(void) fprintf(stdout, gettext(
				    "\n  ---  Cluster %s (id 0x%8.8X) "
				    "Registrations ---\n"),
				    statusp[i].cluster_name,
				    statusp[i].cluster_id);

				int j = 0;
				while ((statusp[i].registered[j]) != 0) {
					(void) fprintf(stdout, gettext(
					    "\n  Node ID:\t\t\t%d\n"),
					    statusp[i].registered[j]);

					(void) fprintf(stdout, gettext(
					    "    Registration "
					    "key:\t\t0x%llx\n"),
					    key_value(statusp[i].
						registration_key[j]));
					j++;
				}
			}
		}
	} else {
		(void) fprintf(stdout, gettext(
		    "Quorum server on port \"%s\" is not configured "
		    "in any cluster.\n"), port);
	}
	(void) fprintf(stdout, "\n");

cleanup:
	(void) close(sock_fd);
	if (status_msg) {
		delete status_msg;
	}

	return (clerrno);
}

//
// get_default_port
//
//	Get the default port number from the service file.
//
int
get_default_port(uint_t *default_port)
{
	FILE *fd = NULL;
	char line[BUFSIZ];
	char field1[BUFSIZ], field2[BUFSIZ];
	int found = 0;

	// Open the services file
	if ((fd = fopen("/etc/services", "r")) == 0) {
		clerror(gettext("Cannot open file \"%s\".\n"),
		    "/etc/services");
		return (CL_EINVAL);
	}

	while (fgets(line, sizeof (line), fd)) {
		if (sscanf(line, "%s%d/%s", field1, default_port,
		    field2) == 0)
			continue;

		if ((strcmp(field1, SERVICE) == 0) &&
		    (strcmp(field2, SERVICE_PROTO) == 0)) {
			found = 1;
			break;
		}
	}
	(void) fclose(fd);

	if (!found) {
		*default_port = DEFAULT_PORT;
	}

	return (CL_NOERR);
}

void
clerror(char *message_in, ...)
{
	va_list ap;
	char *msgid_in;
	uint_t id;

	if (!message_in)
		return;

	va_start(ap, message_in);
	msgid_in = new char[sizeof ("Cnnnnnn")];

	// Generate the ID
	STRLOG_MAKE_MSGID(message_in, id);
	(void) sprintf(msgid_in, "C%6.6d", id);

	// va_start(ap, message_in);
	(void) fprintf(stderr, "%s:  (%s) ", progname, msgid_in);

	// Print the message
	(void) vfprintf(stderr, message_in, ap);

	delete [] msgid_in;
	va_end(ap);
}

//
// Return the option name, either "-<shortoption>", "--<longoption>",
// or the null string.
//
char *
clcommand_option(int optopt, const char *argument)
{	//lint !e578
	char *the_option;
	char *s;

	if (optopt) {
		the_option = new char[3];
		(void) sprintf(the_option, "-%c", optopt);
	} else if (argument && (argument[0] == '-') && (argument[1] == '-')) {
		the_option = new char[strlen(argument) + 1];
		(void) strcpy(the_option, argument);
		if ((s = strchr(the_option, '=')) != (char *)0)
			*s = 0;
	} else if (argument) {
		the_option = new char[strlen(argument) + 1];
		(void) strcpy(the_option, argument);
	} else {
		the_option = new char[1];
		*the_option = 0;
	}

	return (the_option);
}

//
// clqs_common_options
//
//	Process the command options based on the cmd_flg,
//	which can be the "start", "stop", and "show" subcommand.
//	These subcommands have the similar usage.
//
int
clqs_common_options(int argc, char **argv, uint_t cmd_flg,
    optflgs_t *optflgs, char **operands, uint_t *operand_count)
{
	int c;
	int i;
	int errcount = 0;
	int wildcard = 0;
	char *cl_option = 0;
	int option_index = 0;
	char shortopts[] = ":?vdl";
	struct option longopts[] = {
		{"verose", 0, NULL, 'v'},
		{"limited", 0, NULL, 'l'},
		{"disable", 0, NULL, 'd'},
		{"help", 0, NULL, '?'}
	};

	// Check inputs
	if (argc == 1 || !argv || !cmd_flg || !operands || !operand_count) {
		clerror(gettext("Internal error.\n"));
		return (CL_EINTERNAL);
	}

	optind = 2;
	while ((c = getopt_clip(argc, argv, shortopts, longopts,
	    &option_index)) != -1) {

		// Get the option
		delete cl_option;
		cl_option = clcommand_option(optopt, argv[optind - 1]);

		// Switch on option letter
		switch (c) {
		case 'v':
			// Verbose option
			*optflgs |= vflg;
			break;

		case 'd':
			// Disable option
			*optflgs |= dflg;
			if (cmd_flg != FLG_STOP) {
				++errcount;
			}
			break;

		case 'l':
			// Limited option
			*optflgs |= lflg;
			if (cmd_flg != FLG_START) {
				++errcount;
			}
			break;

		case '?':
			// Help option?
			if ((strcmp(cl_option, "-?") == 0) ||
			    (strcmp(cl_option, "--help") == 0)) {
				*optflgs |= hflg;
				break;
			}

			// Or, unrecognized option?
			clerror(gettext("Unrecognized option - \"%s\".\n"),
			    cl_option);
			++errcount;
			break;

		case ':':
			// Missing argument
			clerror(gettext(
			    "Option \"%s\" requires an argument.\n"),
			    cl_option);
			++errcount;
			break;

		default:
			// We should never get here.
			clerror(gettext("Unexpected option - \"%s\".\n"),
			    cl_option);
			++errcount;
			break;
		}
	}
	delete cl_option;
	cl_option = (char *)0;

	// Print usage
	if (errcount || (*optflgs & hflg)) {
		if (errcount) {
			clerror(gettext("Usage error.\n"));
		}

		// Print the subcommand usage
		if (cmd_flg == FLG_STOP) {
			stop_usage();
		} else if (cmd_flg == FLG_START) {
			start_usage();
		} else if (cmd_flg == FLG_SHOW) {
			show_usage();
		}

		if (errcount) {
			return (CL_EINVAL);
		} else {
			return (CL_NOERR);
		}
	}

	// Update the operands list.
	wildcard = *operand_count = 0;
	for (i = optind;  i < argc;  i++) {
		if (strcmp(argv[i], WILDCARD) == 0) {
			++wildcard;
		} else if (!chk_duplicate(argv[i], operands, *operand_count)) {
			operands[*operand_count] = argv[i];
			(*operand_count)++;
		}
	}

	// wildcard and other operands cannot be specified together
	if (wildcard && *operand_count > 0) {
		errcount++;
		clerror(gettext("You cannot specify both \"%s\" and quorum "
		    "server at the same time.\n"), WILDCARD);
	}

	// Errors for start/stop
	if (cmd_flg == FLG_START || cmd_flg == FLG_STOP) {
		if (!wildcard && !*operand_count) {
			clerror(gettext("You must specify either \"%s\" "
			    "or a list of quorum servers.\n"), WILDCARD);
			errcount++;
		}
	}

	if (errcount) {
		clerror(gettext("Usage error.\n"));
		if (cmd_flg == FLG_STOP) {
			stop_usage();
		} else if (cmd_flg == FLG_START) {
			start_usage();
		} else if (cmd_flg == FLG_SHOW) {
			show_usage();
		}
		return (CL_EINVAL);
	}

	// Get all the quorumservers configured and save to operands.
	if ((cmd_flg == FLG_SHOW) && !*operand_count && !wildcard) {
		++wildcard;
	}
	if (wildcard) {
		FILE *fd = NULL;
		char line[BUFSIZ];
		char *port;
		*operand_count = 0;

		// Open the scqsd config file
		if ((fd = fopen(QSD_CONFIG, "r")) == 0) {
			clerror(gettext("Cannot open file \"%s\".\n"),
			    QSD_CONFIG);
			return (CL_EINVAL);
		}

		// Go through all the lines
		line[0] = '\0';
		while (fgets(line, sizeof (line), fd)) {
			// skip comment line
			if (*line == '#')
				continue;
			if (!strstr(line, SERVICE))
				continue;

			// Get the port number
			port = strstr(line, "-p ");
			if (port) {
				char *ptr = 0;

				// Get the port string
				port++;
				while (*++port == ' '); //lint !e722
				ptr = port;
				while (*++ptr != ' '); //lint !e722
				if (*ptr == ' ') {
					*ptr = '\0';
				}

				// Remove newline, if any
				if (port[strlen(port) - 1] == '\n') {
					port[strlen(port) - 1] = '\0';
				}

				// Save this port
				if (!chk_duplicate(port, operands,
				    *operand_count)) {
					operands[*operand_count] =
					    new char [strlen(port) + 1];
					(void) sprintf(operands[*operand_count],
					    "%s", port);
					(*operand_count)++;
				}
			} else {
				// Use default port?
				(void) sprintf(line, "%d", dft_port_num);
				if (!chk_duplicate(line, operands,
				    *operand_count)) {
					operands[*operand_count] = new char [
					    strlen(line) + 1];
					(void) sprintf(operands[*operand_count],
					    "%d", dft_port_num);
					(*operand_count)++;
				}
			}
		}
		// Close the file
		(void) fclose(fd);
	}

	return (CL_NOERR);
}

//
// clqs_clear_options
//
//	Process the options for the "clear" subcommand.
int
clqs_clear_options(int argc, char **argv, char **cluster_name,
    uint32_t &clusterID, optflgs_t *optflgs, char **operands)
{
	int i, c;
	int errcount = 0;
	int operand_count = 0;
	char *cl_option = 0;
	int option_index = 0;
	char reply[BUFSIZ];
	int invalid = 0;
	char shortopts[] = ":?c:I:yv";
	struct option longopts[] = {
		{"clustername", 1, NULL, 'c'},
		{"clusterID", 1, NULL, 'I'},
		{"yes", 0, NULL, 'y'},
		{"verose", 0, NULL, 'v'},
		{"help", 0, NULL, '?'}
	};
	char *cluster_id = (char *)0;
	int cflg, Iflg;

	// Check inputs
	if (argc == 1 || !argv || !operands) {
		clerror(gettext("Internal error.\n"));
		return (CL_EINTERNAL);
	}

	optind = 2;
	cflg = Iflg = 0;
	while ((c = getopt_clip(argc, argv, shortopts, longopts,
	    &option_index)) != -1) {

		// Get the option
		delete cl_option;
		cl_option = clcommand_option(optopt, argv[optind - 1]);

		// Switch on option letter
		switch (c) {
		case 'v':
			// Verbose option
			*optflgs |= vflg;
			break;

		case '?':
			// Help option?
			if ((strcmp(cl_option, "-?") == 0) ||
			    (strcmp(cl_option, "--help") == 0)) {
				*optflgs |= hflg;
				break;
			}

			// Or, unrecognized option?
			clerror(gettext("Unrecognized option - \"%s\".\n"),
			    cl_option);
			++errcount;
			break;

		case 'c':
			// Cluster name
			if (!cflg) {
				*cluster_name = new char [strlen(optarg) + 1];
				(void) sprintf(*cluster_name, "%s", optarg);
			}
			cflg++;
			break;

		case 'I':
			// Cluster ID
			cluster_id = optarg;
			Iflg++;
			break;

		case 'y':
			*optflgs |= yflg;
			break;

		case ':':
			// Missing argument
			clerror(gettext(
			    "Option \"%s\" requires an argument.\n"),
			    cl_option);
			++errcount;
			break;

		default:
			// We should never get here.
			clerror(gettext("Unexpected option - \"%s\".\n"),
			    cl_option);
			++errcount;
			break;
		}
	}
	delete cl_option;
	cl_option = (char *)0;

	// Should not have duplicate -c or -I
	if (cflg > 1) {
		clerror(gettext("You can only specify the \"-c\" "
		    "option once.\n"));
		++errcount;
	}
	if (Iflg > 1) {
		clerror(gettext("You can only specify the \"-I\" "
		    "option once.\n"));
		++errcount;
	}

	// Print usage
	if (errcount || (*optflgs & hflg)) {
		if (errcount) {
			clerror(gettext("Usage error.\n"));
		}

		// Print the subcommand usage
		clear_usage();
		if (errcount) {
			return (CL_EINVAL);
		} else {
			return (CL_NOERR);
		}
	}

	// Check the required arguments
	if (!cluster_name || !cluster_id) {
		clerror(gettext("You must specify both the "
		    "cluster name and the cluster ID.\n"));
		++errcount;
	}

	// Convert the cluster ID
	if (cluster_id) {
		char *cl_id_save = new char [strlen(cluster_id) + 1];
		char c_ptr;

		(void) sprintf(cl_id_save, "%s", cluster_id);
		clusterID = 0;
		if ((strlen(cluster_id) > 2) &&
		    (cluster_id[0] == '0') &&
		    (cluster_id[1] == 'x')) {
			cluster_id += 2;
		}

		// Invalid ID
		if (strlen(cluster_id) != 8) {
			clerror(gettext("Invalid cluster ID "
			    "\"%s\".\n"), cl_id_save);
			invalid = 1;
		} else {
			for (i = 0; i < (int)strlen(cluster_id); i++) {
				c_ptr = cluster_id[i];
				if ((c_ptr >= '0') && (c_ptr <= '9')) {
					clusterID = (clusterID << 4) +
					    (uchar_t)(c_ptr - '0');
				} else if ((c_ptr >= 'A') && (c_ptr <= 'F')) {
					clusterID = (clusterID << 4) + 0xa
					    + (uchar_t)(c_ptr - 'A');
				} else if ((c_ptr >= 'a') && (c_ptr <= 'f')) {
					clusterID = (clusterID << 4) + 0xa
					    + (uchar_t)(c_ptr - 'a');
				} else {
					invalid = 1;
					break;
				}
			}
			if (invalid) {
				clerror(gettext("Cluster ID \"%s\" is "
				    "not a hexadecimal.\n"),
				    cl_id_save);
			}
		}
		delete [] cl_id_save;
	}

	// Update the operands list.
	operand_count = 0;
	for (i = optind;  i < argc;  i++) {
		if (!chk_duplicate(argv[i], operands, (uint_t)operand_count)) {
			operands[operand_count] = argv[i];
			operand_count++;
		}
	}

	// Only accept one operand
	if (operand_count > 1) {
		errcount++;
		clerror(gettext("You can only clear one quorum "
		    "server at a time.\n"));
	}
	if (operand_count == 0) {
		errcount++;
		clerror(gettext("You must specify the quorum server "
		    "to clear.\n"));
	}

	if (errcount) {
		clerror(gettext("Usage error.\n"));
		clear_usage();
		return (CL_EINVAL);
	}

	// Invalid but not usage error
	if (invalid) {
		return (CL_EINVAL);
	}

	if (!(*optflgs & yflg)) {
		(void) fprintf(stdout, gettext(
		    "The quorum server to be cleared must "
		    "have been removed from the cluster. "
		    "Clearing a valid quorum server could "
		    "compromise the cluster quorum.\n"));
		(void) fprintf(stdout, "%s? (yes or no): ",
		    gettext("Do you want to continue"));
		if (fgets(reply, sizeof (reply) - 1, stdin)
		    != NULL) {
			i = (int)strlen(reply) - 1;
			if (strncasecmp(reply, "y", (uint_t)i) == 0 ||
			    strncasecmp(reply, "yes", (uint_t)i) == 0) {
				*optflgs |= yflg;
			}
		}
	}

	return (CL_NOERR);
}

//
// This sub routine will delete the QS disable
// flag file. The file looks like
// /etc/scqsd/9000.qs where 9000 is the port
//
bool
enable_qs(const char *port)
{
	char *filename = NULL;
	unsigned int size = strlen(port) + strlen(QSD_DIR) + 4;

	ASSERT(port != NULL);

	filename = (char *)malloc(sizeof (char) * size);
	if (filename == NULL) {
		// Not enough memory
		return (false);
	}

	(void) strcpy(filename, QSD_DIR);
	(void) strcat(filename, port);
	(void) strcat(filename, ".qs");

	if (access(filename, F_OK) != 0) {
		// There is no file
		// Nothing to unlink
		return (true);
	}

	// Delete the file
	if (unlink(filename) != 0) {
		return (false);
	}

	// Success
	return (true);
}

//
// This sub routine will create the QS disable
// flag file. The file looks like
// /etc/scqsd/9000.qs where 9000 is the port
//
bool
disable_qs(const char *port)
{
	int fd;
	char *filename = NULL;
	unsigned int size = strlen(port) + strlen(QSD_DIR) + 4;

	ASSERT(port != NULL);

	filename = (char *)malloc(sizeof (char) * size);
	if (filename == NULL) {
		// Not enough memory
		(void) printf("Not enough memory\n");
		return (false);
	}

	(void) strcpy(filename, QSD_DIR);
	(void) strcat(filename, port);
	(void) strcat(filename, ".qs");

	fd = creat(filename, S_IRUSR);
	if (fd == -1) {
		return (false);
	}

	// Success
	return (true);
}

//
// If the lock file does not exist, the QS is
// enabled. This routine will return true if the
// QS is enabled.
//
bool
is_qs_enabled(const char *port)
{
	char *filename = NULL;
	unsigned int size = strlen(port) + strlen(QSD_DIR) + 4;

	ASSERT(port != NULL);

	filename = (char *)malloc(sizeof (char) * size);
	if (filename == NULL) {
		//
		// Not enough memory
		//
		return (false);
	}

	(void) strcpy(filename, QSD_DIR);
	(void) strcat(filename, port);
	(void) strcat(filename, ".qs");

	if (access(filename, F_OK) == 0) {
		//
		// The file exists and hence
		// the QS is disabled.
		//
		return (false);
	}

	//
	// The file does not exist and hence
	// the QS is not enabled.
	//
	return (true);
}
