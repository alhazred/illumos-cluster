/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scxcfgd_main.c	1.5	08/05/20 SMI"

#include <unistd.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <strings.h>
#include <errno.h>

#define	PORTMAP
#include <rpc/rpc.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <sys/clconf.h>
#include <sys/cladm_int.h>
#include <sys/clconf_int.h>

#include <scxcfg/scxcfg.h>
#include "scxcfg_int.h"

#include <scxcfg/rpc/scxcfg_rpc.h>

#include "scxcfgd.h"

#include <rgm/sczones.h>

/*
 * scxcfgd
 *
 * RPC server daemon for Europa. See also RPC defs in scxcfg_rpc.x.
 *
 * Runs on servers nodes.
 *
 * Responsible for serving basic configuration requests from farm nodes.
 * - refresh of the config cache maintained in file on the farm node
 * - handle installation requests from incoming farm nodes.
 * - it also implements RPC routine to send back Farm membership status
 *   of a specific farm node. (used by DAM daemon on farm nodes).
 *
 * It uses libscxcfg  standard API to access the farm configuration.
 * It connects to the FMM to be able to give farm node membership status.
 *
 */

int _rpcpmstart = 0;

extern void
scxcfg_server_1(struct svc_req *rqstp, register SVCXPRT *transp);

/*
 * start_rpc_service()
 *
 * Start the RPC services on a specifics.
 * It starts two transport:
 *  One UDP transport to respond to UDP broadcast requests from farm nodes.
 *  One TCP transport to do the actual operations.
 */
void
start_rpc_service()
{
	int res = 0;
	SVCXPRT *transp;
	int sockfd;
	struct sockaddr_in address;

	/*
	 * Create RPC service.
	 */
	res = svc_create(scxcfg_server_1, SCXCFG_SERVER, SCXCFG_VERS, "udp");
	if (res == 0) {
		scxcfg_log(LOG_ERR, "cannot start rpc services");
		return;
	}
	/*
	 * Create TCP transport.
	 */
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	address.sin_family = AF_INET;
	/*
	 * listen on all interfaces.
	 */
	address.sin_addr.s_addr = inet_addr("0.0.0.0");
	address.sin_port = htons(SCXCFG_PORT);

	res  = bind(sockfd, (struct sockaddr *)&address, sizeof (address));
	if (res != 0) {
		scxcfg_log(LOG_ERR, "bind error: %d", res);
		return;
	}

	if ((transp = (SVCXPRT *)svctcp_create(sockfd, 0, 0)) == NULL) {
		scxcfg_log(LOG_ERR, "cannot create RPC TCP  server");
		return;
	}

	scxcfg_log(LOG_DEBUG, "TCP SERVER: port = %d\n", transp->xp_port);

	/*
	 * Create UDP transport.
	 */
	sockfd = socket(AF_INET, SOCK_DGRAM, 0);
	address.sin_family = AF_INET;
	/*
	 * listen on all interfaces, on port SCXCFG_PORT
	 */
	address.sin_addr.s_addr = inet_addr("0.0.0.0");
	address.sin_port = htons(SCXCFG_PORT);

	res  = bind(sockfd, (struct sockaddr *)&address, sizeof (address));
	if (res != 0) {
		scxcfg_log(LOG_ERR, "bind error: %d", res);
		return;
	}
	if ((transp = (SVCXPRT *)svcudp_create(sockfd)) == NULL) {
		scxcfg_log(LOG_ERR, "cannot create RPC UCP  server");
		return;
	}
	scxcfg_log(LOG_DEBUG, "UDP SERVER: port = %d\n", transp->xp_port);

	/*
	 * run RPC service.
	 */
	svc_run();
}

/*
 * daemonize myself
 */
static void
daemonize(void)
{
	pid_t pid;
	extern int errno;

	pid = fork();
	if (pid < 0) {
		(void) fprintf(stderr, "fork: %s", strerror(errno));
		exit(1);
	}
	if (pid > 0) {
		exit(0);
	}
	/*
	 * set process group
	 */
	pid = setsid();
	if (pid == (pid_t)-1) {
		(void) fprintf(stderr, "setsid: %s", strerror(errno));
		exit(1);
	}

	(void) signal(SIGCHLD, SIG_IGN);
	(void) signal(SIGHUP, SIG_IGN);

	pid = fork();
	if (pid < 0) {
		(void) fprintf(stderr, "fork: %s", strerror(errno));
	}
	if (pid > 0) {
		exit(0);
	}

	(void) chdir("/");

	(void) close(STDIN_FILENO);
	(void) close(STDOUT_FILENO);
	(void) close(STDERR_FILENO);
}

/*
 * signal handler
 */
void
sigint_handler(int sig)
{
	end_ccr_access();
	exit(1);
}


/*
 * main()
 */
int
main(int argc, char *argv[])
{
	int rc = 0;
	int debug_mode = 0;
	int num = 1;

	if (sc_zonescheck() != 0)
		return (1);

	scxcfg_log_set_tag("Cluster.scxcfg.daemon");
	/*
	 * Hidden option, debug.
	 */
	if (argc > 1) {
		if (strcmp(argv[1], "-d") == 0) {
			scxcfg_log_set_level(LOG_DEBUG);
			debug_mode = 1;
		}
	}
	if (!debug_mode) {
		daemonize();
	}

	if (scxcfgd_fmm_init()) {
		scxcfg_log(LOG_ERR, "failed to init FMM access");
		exit(SCXCFG_ENOCLUSTER);
	}

	if (init_ccr_access()) {
		scxcfg_log(LOG_ERR, "failed to init CCR access");
		exit(SCXCFG_ENOCLUSTER);
	}
	(void) signal(SIGINT, sigint_handler);

	/*
	 * Ignore the SIGPIPE signal
	 */
	(void) sigset(SIGPIPE, SIG_IGN);

	start_rpc_service();

	scxcfgd_fmm_close();
	end_ccr_access();
	exit(0);
}
