/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scxcfgd_fmm.c	1.4	08/05/20 SMI"

#include <fmm/saf_clm.h>
#include "scxcfgd.h"


/*
 * scxcfgd_fmm
 *
 * Simple access to FMM to get the status of a farm node.
 */

SaClmHandleT ClmHandle;

/*
 * Not used, just required by saClmInitialize().
 */
static void
clusterNodeGet(SaInvocationT invocation,
	SaClmClusterNodeT *clusterNode,
	SaErrorT error)
{
}

/*
 * Not used, just required by saClmInitialize().
 */
static void
clusterTrackStart(SaClmClusterNotificationT *notificationBuffer,
	SaUint32T numberOfItems,
	SaUint32T numberOfMembers,
	SaUint64T viewNumber,
	SaErrorT error)
{
}

/*
 * scxcfgd_fmm_init()
 *
 * Returns:
 *  SCXCFG_OK if ok
 *  SCXCFG_ENOCLUSTER if nok.
 */
int
scxcfgd_fmm_init()
{
	SaClmCallbacksT callbacks;
	SaVersionT version;
	SaErrorT result = SA_OK;
	int rstatus = SCXCFG_OK;

	version.releaseCode = 'A';
	version.major = 0x01;
	version.minor = 0xff;
	callbacks.saClmClusterNodeGetCallback = clusterNodeGet;
	callbacks.saClmClusterTrackCallback = clusterTrackStart;
	result = saClmInitialize(&ClmHandle, &callbacks, &version);
	if (result != SA_OK) {
		scxcfg_log(LOG_ERR, "Failed initialize FMM");
		rstatus = SCXCFG_ENOCLUSTER;
	}
	return (rstatus);
}

/*
 * scxcfgd_fmm_node_get()
 *
 * gets the status of a farm node by its node-id.
 *
 * Returns:
 *  SCXCFG_OK is nodeid is in current farm membership.
 *  SCXCFG_ENOEXIST is it not.
 */
int
scxcfgd_fmm_node_get(scxcfg_nodeid_t nodeid)
{
	int rstatus;
	SaErrorT result = SA_OK;
	SaClmClusterNodeT clusterNode;
	SaTimeT timeout = 5000000;
	SaClmNodeIdT nid;

	result = saClmClusterNodeGet(ClmHandle, (SaClmNodeIdT)nodeid,
	    timeout, &clusterNode);
	scxcfg_log(LOG_DEBUG, "FMM NodeGet :nid %d : %d", nodeid, result);
	if (result == SA_OK) {
		if (clusterNode.member)
			return (SCXCFG_OK);
	}
	return (SCXCFG_ENOEXIST);
}

/*
 * scxcfgd_fmm_close()
 */
int
scxcfgd_fmm_close()
{
	int result;
	result = saClmFinalize(ClmHandle);
	return (result);
}
