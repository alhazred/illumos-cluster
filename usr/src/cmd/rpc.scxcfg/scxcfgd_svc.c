/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scxcfgd_svc.c	1.4	08/05/20 SMI"

#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <rpc/rpc.h>
#include <rpc/xdr.h>
#include <rpc/svc.h>

#include <scxcfg/scxcfg.h>
#include <scxcfg/rpc/scxcfg_rpc.h>

#include "scxcfgd.h"

/*
 * scxcfgd_svc
 *
 * Implementation of scxcfg RPC service routines.
 * See also scxcfg_data_remote class (the client) in libscxcfg.
 *
 * Some basic routines like get_property, set_property,
 * del_property are implemented.
 *
 * Note:
 * Current implementation of the farm libscxcfg reads config
 * data from a file.
 * The file is created from a bulk transfer of the config
 * scxcfg_bulk_transfer_1_svc, that is used to download
 * the  configuration (or a subset for a particular
 * farm node).
 *
 */

/*
 * scxcfg_get_property_1_svc()
 */
bool_t
scxcfg_get_property_1_svc(scxcfg_prop_input_t *argp,
    scxcfg_prop_output_t *result, struct svc_req *rqst)
{
	char *context;
	int rc;
	char value[FCFG_MAX_LEN];

	result->error = SCXCFG_OK;
	if ((argp->context == NULL) || (argp->context[0] == 0)) {
		context = FARM_TABLE;
	} else {
		context = argp->context;
	}
	rc = scxcfgd_read_element(context, argp->key, value);
	if (rc) {
		result->error = rc;
		result->value = NULL;
	} else {
		result->value = strdup(value);
	}
	return (TRUE);
}

/*
 * scxcfg_set_property_1_svc()
 */
bool_t
scxcfg_set_property_1_svc(scxcfg_prop_input_t *argp,
    scxcfg_output_t *result, struct svc_req *rqst)
{
	char *context;
	int rc;

	result->error = SCXCFG_OK;
	if ((argp->context == NULL) || (argp->context[0] == 0)) {
		context = FARM_TABLE;
	} else {
		context = argp->context;
	}
	rc = scxcfgd_write_element(context, argp->key, argp->value);
	if (rc) {
		result->error = rc;
	}
	return (TRUE);
}

/*
 * scxcfg_del_property_1_svc()
 */
bool_t
scxcfg_del_property_1_svc(scxcfg_prop_input_t *argp,
    scxcfg_output_t *result, struct svc_req *rqst)
{
	char *context;
	int rc;

	result->error = SCXCFG_OK;
	if ((argp->context == NULL) || (argp->context[0] == 0)) {
		context = FARM_TABLE;
	} else {
		context = argp->context;
	}
	rc = scxcfgd_remove_element(context, argp->key);
	if (rc) {
		result->error = rc;
	}
	return (TRUE);
}

/*
 * scxcfg_bulk_transfer_1_svc()
 *
 * RPC routine.
 */
bool_t
scxcfg_bulk_transfer_1_svc(scxcfg_input_t *argp,
    scxcfg_bulk_output_t *result, struct svc_req *rqstp)
{
	char *context;
	char *buf;
	int rc;

	context = argp->name;
	/*
	 * XXX Verify context == table name.
	 */
	rc = scxcfgd_read_table(context, &buf, argp->node_id);
	if (rc) {
		result->error = rc;
		result->buffer = NULL;
		result->context = NULL;
		return (TRUE);
	}
	result->buffer = buf;
	result->context = strdup(context);
	result->error = SCXCFG_OK;
	/*
	 * buf has been allocated by scxcfgd_read_table() with malloc and will
	 * be freed by xdr routines.. (using free()).
	 */
	return (TRUE);
}

/*
 * scxcfg_install_farm_1_svc()
 *
 * RPC routine.
 *
 * Do the "installation" (ie: farm node creation/re-configuration) and
 * return back the needed information and status to the requesting farm
 * node.
 */
bool_t
scxcfg_install_farm_1_svc(scxcfg_install_input_t *argp,
    scxcfg_install_output_t *result, struct svc_req *rqstp)
{
	int rc = SCXCFG_OK;

	rc = scxcfgd_install(argp, result);
	if (rc) {
		result->error = rc;
		result->node_id = 0;
		result->cluster_id = strdup("");
		return (TRUE);
	}
	result->error = SCXCFG_OK;
	return (TRUE);
}

/*
 * scxcfg_auth_farm_1_svc()
 *
 * RPC routine.
 *
 * Do a pseudo authentication of a farm node request.
 * this is called by the farm node itself which must distinguish
 * between all the servers that have answered the UDP  broadcast.
 * The "authentification" is based on node_id, nodename, cluster_id.
 * XXX it should used also the auth_key.
 *
 * Returns: in result->error
 *  SCXCFG_OK: the farm node is configured in this farm.
 *  SCXCFG_ENOCLUSTER: the farm node is not part of this farm.
 */
bool_t
scxcfg_auth_farm_1_svc(scxcfg_auth_input_t *argp,
    scxcfg_output_t *result, struct svc_req *rqstp)
{
	int rc;
	uint32_t nodeId;
	scxcfg_error error;

	if (strncmp(argp->cluster_id, clusterId, FCFG_MAX_LEN) != 0) {
		scxcfg_log(LOG_DEBUG, "Failed auth %s, cluster_id %s",
		    argp->node_name, argp->cluster_id);
		result->error = SCXCFG_ENOCLUSTER;
		goto end;
	}
	rc = scxcfg_get_nodeid(&scxCfg, argp->node_name, &nodeId, &error);
	if (rc != FCFG_OK) {
		scxcfg_log(LOG_NOTICE, "Failed auth %s, not found nodeid %d",
		    argp->node_name, argp->node_id);
		result->error = SCXCFG_ENOCLUSTER;
		goto end;
	}
	if (argp->node_id != nodeId) {
		scxcfg_log(LOG_NOTICE, "Failed auth %s, mismatch nodeid %d",
		    argp->node_name, argp->node_id);
		result->error = SCXCFG_ENOCLUSTER;
		goto end;
	}
	scxcfg_log(LOG_DEBUG, "Authenticated %s, cluster_id %s",
	    argp->node_name, argp->cluster_id);
	result->error = SCXCFG_OK;
end:
	return (TRUE);
}

/*
 * scxcfg_alive_nodeid_1_svc()
 *
 * RPC routine:
 *
 * Returns the farm membership status of a farm node,
 * The result is stored in result->error.
 */
bool_t
scxcfg_alive_nodeid_1_svc(scxcfg_input_t *argp,
    scxcfg_output_t *result, struct svc_req *rqstp)
{
	scxcfg_nodeid_t nodeId;
	nodeId = argp->node_id;
	result->error = scxcfgd_fmm_node_get(nodeId);
	return (TRUE);
}

/*
 * scxcfg_server_1_freeresult()
 *
 * Utility function.
 */
bool_t
scxcfg_server_1_freeresult(SVCXPRT * transp, xdrproc_t xdr_result,
    caddr_t result)
{
	xdr_free(xdr_result, result);
	return (TRUE);
}
