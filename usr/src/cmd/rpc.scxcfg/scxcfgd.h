/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_SCXCFGD_H
#define	_SCXCFGD_H

#pragma ident	"@(#)scxcfgd.h	1.3	08/05/20 SMI"

#include <scxcfg/scxcfg.h>
#include <scxcfg/rpc/scxcfg_rpc.h>
#include <scxcfg_int.h>

#ifdef	__cplusplus
extern "C" {
#endif

int init_ccr_access();
void end_ccr_access();
int scxcfgd_read_table(char *, char **, scxcfg_nodeid_t);
int scxcfgd_write_element(char *, char *, char *);
int scxcfgd_read_element(char *, char *, char *);
int scxcfgd_remove_element(char *, char *);
int scxcfgd_install(scxcfg_install_input_t *argp,
    scxcfg_install_output_t *result);
int scxcfgd_fmm_init();
int scxcfgd_fmm_node_get(scxcfg_nodeid_t);
int scxcfgd_fmm_close();
extern char clusterId[];
extern scxcfg scxCfg;

#ifdef	__cplusplus
}
#endif

#endif /* _SCXCFGD_H */
