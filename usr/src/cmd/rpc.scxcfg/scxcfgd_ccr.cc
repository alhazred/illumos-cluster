/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scxcfgd_ccr.cc	1.4	08/05/20 SMI"

#include <stdlib.h>
#include <limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <strings.h>

#include <scxcfg/scxcfg.h>
#include "scxcfg_int.h"

#include "scxcfg_data_ccr.h"

#include "scxcfgd.h"

//
// scxcfgd_ccr
//
// Daemon code that deals with the CCR data store.
// It uses the services of libscxcfg to access CCR.
// It uses some knowledge about the internal implementation
// of libscxcfg.
//

char clusterId[FCFG_MAX_LEN];
scxcfg scxCfg;

static scxcfg_data_ccr *dataAccess = NULL;

//
// init_ccr_access()
//
// scxcfgd daemon initialize access through the standard
// scxcfg API, then it gets a indirect access through
// the handle of struct scxcfg.
//
int
init_ccr_access()
{
	scxcfg_log(LOG_DEBUG, "scxcfgd: init_ccr_access");
	int rc;
	scxcfg_error error;

	rc = scxcfg_open(&scxCfg, &error);
	if (rc != SCXCFG_OK) {
		scxcfg_log(LOG_ERR, "cannot open API lib");
		return (SCXCFG_ESYSERR);
	}
	//
	// The methods are not directly available
	// through API, get the pointer to the scxcfg_data_ccr object.
	//
	dataAccess = (scxcfg_data_ccr*)scxCfg.handle;

	if (!dataAccess) {
		scxcfg_log(LOG_ERR, "cannot create CCR access obj");
		return (SCXCFG_ESYSERR);
	}
	//
	// Retrieve the cluster_id of this cluster for future use.
	// It is retrieved from the SVC infrastructure CCR table, not from the
	// farm table.
	//
	rc = dataAccess->getProperty(INFR_TABLE,
	    "cluster.properties.cluster_id", clusterId);
	if (rc) {
		scxcfg_log(LOG_ERR, "cannot find cluster_id");
		dataAccess->close();
		delete dataAccess;
		return (rc);
	}
	scxcfg_log(LOG_DEBUG, "init_ccr: cluster_id %s", clusterId);
	return (SCXCFG_OK);
}

//
// end_ccr_access()
//
void
end_ccr_access()
{
	scxcfg_log(LOG_DEBUG, "scxcfgd: end_ccr_access");
	dataAccess = NULL;
	(void) scxcfg_close(&scxCfg);
}

//
// scxcfgd_read_table()
//
// Read a table from CCR. The nodeid is used to
// filter the data to retrieve only the data for this farm node.
//
// buf is allocated by  getConfig, it must be freed by
// the client of scxcfgd_read_table().
//
int
scxcfgd_read_table(char *table_name, char **buf, scxcfg_nodeid_t n)
{
	scxcfg_log(LOG_DEBUG, "scxcfgd_read_table");
	return (dataAccess->getConfig(table_name, buf, n));
}

//
// scxcfgd_read_element()
//
// Read one element from a table.
//
int
scxcfgd_read_element(char *table_name, char *key, char *value)
{
	scxcfg_log(LOG_DEBUG, "scxcfgd_read_element");
	return (dataAccess->getProperty(table_name, key, value));
}

//
// scxcfgd_write_element()
//
// Write an element in a table.
//
int
scxcfgd_write_element(char  *table_name, char *key, char *value)
{
	scxcfg_log(LOG_DEBUG, "scxcfgd_write_element");
	std::list<Prop*> oneProp;
	Prop *v = new Prop(key, value);
	oneProp.push_back(v);
	return (dataAccess->setListProperty(table_name, &oneProp));
}

//
// scxcfgd_remove_element()
//
// Remove an element from a table.
//
int
scxcfgd_remove_element(char *table_name, char *key)
{
	scxcfg_log(LOG_DEBUG, "scxcfgd_remove_element");
	std::list<Prop*> oneProp;
	Prop *v = new Prop(key, "");
	oneProp.push_back(v);
	return (dataAccess->delListProperty(table_name, &oneProp));
}

//
// scxcfgd_install()
//
// Configuration of a farm node. Service of the scxcfg_install_farm_1_svc
// request.
//
// Input contains:
//  - the farm node-name
//  - the authentication key
//  - the cluster name
//
// If successful, the output will contain
//
//  - the nodeid of the farm node.
//  - the cluster_id of the cluster.
//
// If scxcfg_add_node() fails, the output error is set to
// the return value of scxcfg_add_node().
//
int
scxcfgd_install(scxcfg_install_input_t *input,
    scxcfg_install_output_t *output)
{
	scxcfg_log(LOG_DEBUG, "scxcfgd_install");
	int rc = SCXCFG_OK;
	char value[FCFG_MAX_LEN];
	char ipAddr[FCFG_MAX_LEN];
	scxcfg_nodeid_t nodeId;
	char *nodeName;
	char *adapName;

	output->node_id = 0;

	if (!dataAccess) {
		scxcfg_log(LOG_ERR, "scxcfgd_install, access not initiated");
		output->error = SCXCFG_ENOINIT;
		return (SCXCFG_ENOINIT);
	}
	scxcfg_log(LOG_DEBUG, "scxcfgd_install, node: %s cluster: %s key: %s",
	    input->node_name, input->cluster_name, input->auth_key);

	//
	// XXX missing :need to use the auth_key
	//
	nodeName = input->node_name;
	//
	// Verify cluster_name. The cluster name identifies
	// the cluster (and its farm), it is stored in the
	// infrastructure CCR table, not in the default farm
	// table.
	//
	rc = dataAccess->getProperty(INFR_TABLE, "cluster.name", value);
	if (rc != SCXCFG_OK) {
		scxcfg_log(LOG_ERR, "inst: error cluster/farm name: %d", rc);
		output->error = SCXCFG_ENOINIT;
		return (rc);
	}
	if (strcmp(value, input->cluster_name) != 0) {
		scxcfg_log(LOG_WARNING, "inst: invalid farm name"
		    " %s from %s", input->cluster_name, nodeName);
		output->error = SCXCFG_ENOCLUSTER;
		return (SCXCFG_ENOCLUSTER);
	}
	f_property_t adapter;
	f_property_t adapter2;
	scxcfg_error error;
	adapter.next = NULL;
	adapter.value = input->adap1;

	if (strcmp(input->adap2, "") != 0) {
		// 2 adapters defined
		adapter2.next = NULL;
		adapter2.value = input->adap2;
		adapter.next = &adapter2;
	}
	//
	// Add (or overwrite) this farm node in the configuration.
	//
	rc = scxcfg_add_node(&scxCfg, nodeName, &adapter, &nodeId, &error);
	if (rc == FCFG_EEXIST) {
		scxcfg_log(LOG_NOTICE, "install: config for node %s"
		" re-created and possibly overwritten", nodeName);
	} else if (rc != FCFG_OK) {
		scxcfg_log(LOG_ERR, "install: add farm node: %d", rc);
		output->error = rc;
		return (rc);
	}
	scxcfg_log(LOG_DEBUG, "Sending back cluster_id %s", clusterId);
	output->node_id = nodeId;
	output->cluster_id = strdup(clusterId);
	output->error = SCXCFG_OK;
	return (SCXCFG_OK);
}
