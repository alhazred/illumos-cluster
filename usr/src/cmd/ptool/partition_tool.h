//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// partition_tool.h
//

#ifndef	_PARTITION_TOOL_H
#define	_PARTITION_TOOL_H

#pragma ident	"@(#)partition_tool.h	1.3	08/05/20 SMI"


#include <stdio.h>
#include <limits.h>

#define	BUFFER_SIZE		512
#define	MAX_NODE_NAME_LENGTH	256


#define	NODE_LIST_CMD "scstat -n | grep \"Cluster node:\" | awk '{print $3}' "

#define	DEV_LIST_CMD "scdidadm -L "

#define	NODE_LIST_FILE "/var/cluster/run/node_list.txt"
#define	DEV_LIST_FILE  "/var/cluster/run/dev_list.txt"


class partition_tool {
public:

	partition_tool();
	~partition_tool();

	// Will find out the correct partitions
	bool partition(int n);

	//
	// Will decide whether the partition being created is a valid
	// partition or not.
	// Move to private
	//
	bool good(int n);

	//
	// Will display a valid partition to the console.
	// Move to private
	//
	void print_partition();

	//
	// Will display the partition for a 2 node
	// cluster.
	//
	void print_partition_2node();

	// Build array of device to node mapping
	int get_node_device_info(const char *inputfile);

	// Build the array of nodes present in the cluster
	int get_node_info(const char *inputfile);

	//
	// Add a dummy disk to all the nodes.
	// Used when the cluster has no shared disks
	//
	int add_dummy_disk();

	// Allocate memory for the partition related data structures.
	void alloc_partitions();

	//
	// Will display the node_device internal mapping
	// This is for debugging purposes only.
	//
	void print_node_device();

	// Returns the node count
	int get_node_count();

	// Returns the partition count
	int get_partition_count();

	// Save the current locale
	void save_env();

	// Set the LC_ALL Environment value to "C".
	int set_env_posix();

	//
	// set the  LC_ALL environment value to the
	// value saved in 'current_locale'
	//
	int reset_env();

	// Execute a command
	int execute_cmd_with_popen(const char *, int *);

private:

	// No of partitions discovered
	int partition_count;

	// Names of the cluster nodes.
	char **nodelist;

	// No of nodes in the cluster.
	int node_count;

	// No of shared devices.
	int device_count;

	//
	// Double dimentional array of devices per node.
	// This is maintained as an array of BYTE pointers.
	// It is accessed as node_dev[dev_no][node_no]
	//
	char **node_dev;

	// Nodes in Partition A
	char *part_a;

	// Nodes in Partition B
	char *part_b;

	//
	// Devices outside Partition A or B.
	// This array is populated and used by the function good().
	//
	char *dev_c;

	// Save the locale in this variable.
	char current_locale[PATH_MAX];

	// Indiactes if the locale is saved or not. 0 means not saved.
	int locale_saved;

};

#endif	/* _PARTITION_TOOL_H */
