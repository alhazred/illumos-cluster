//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// partition_tool.cc
//
#pragma ident	"@(#)partition_tool.cc	1.4	08/05/20 SMI"

#include <stdlib.h>
#include <string.h>
#include <libintl.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include "partition_tool.h"

#define	ZONECHECK_CMD	"/usr/cluster/lib/sc/sc_zonescheck"

partition_tool::partition_tool()
{
	partition_count = 0;
	nodelist = NULL;
	node_count = 0;
	partition_count = 0;
	node_dev = NULL;
	device_count = 0;
	part_a = NULL;
	part_b = NULL;
	dev_c = NULL;
	current_locale[0] = '\0';
	locale_saved = 0;
}

partition_tool::~partition_tool()
{

	if (nodelist != NULL) {
		for (int i = 0; i < node_count; i++) {
			free(nodelist[i]);
		}
		free(nodelist);
	}

	if (node_dev != NULL) {
		for (int i = 0; i < device_count; i++) {
			free(node_dev[i]);
		}
		free(node_dev);

	}

	if (part_a != NULL) {
		free(part_a);
	}

	if (part_b != NULL) {
		free(part_b);
	}

	if (dev_c != NULL) {
		free(dev_c);
	}

}

void
partition_tool::alloc_partitions()
{
	part_a = (char *)calloc((unsigned int)node_count, sizeof (char));
	part_b = (char *)calloc((unsigned int)node_count, sizeof (char));
	dev_c = (char *)calloc((unsigned int)device_count, sizeof (char));

	//
	// Node 0 is always in partition A.
	// This way we avoid generating one duplicate partition set.
	//
	part_a[0] = true;
}

//
// This will build the array of nodes present in the cluster
// Takes a file as input where each line of the file is a node name
// Returns 1 on error and 0 on success
//
int
partition_tool::get_node_info(const char *inputfile)
{
	FILE *fp = NULL;
	char buff[BUFFER_SIZE];
	int len;

	node_count = 0;

	fp = fopen(inputfile, "r");
	if (fp == NULL) {
		(void) printf(gettext("Error: Could not open the file %s\n"),
		    inputfile);
		return (1);
	}

	while (fgets(buff, BUFFER_SIZE, fp) != NULL) {
		if (node_count % 10 == 0) {
			//
			// We have to realloc.
			// Allocate memory for 10 node names at a time
			//
			nodelist = (char **)realloc(nodelist,
			    (sizeof (char *) *
			    ((unsigned int)node_count + 10)));
		}
		nodelist[node_count] = (char *)malloc(MAX_NODE_NAME_LENGTH);

		len = (int)strlen(buff);
		if (len > 0) {
			if (buff[len - 1] == '\n') {
				buff[strlen(buff)-1] = 0;
			}
			(void) strcpy(nodelist[node_count], buff);
			node_count++;
		} else {
			// Ignore
		}
	}

	(void) fclose(fp);
	return (0);
}


//
// This function will build the node and device mappings.
// The array populated has devices as rows and node names as columns
// This must be used on any machine with files as inputs.
// The input file should resembling the output of scdidadm -L
// e.g.
// 1 phys-tycho-1:/dev/rdsk/c0t1d0  /dev/did/rdsk/d1
// 2 phys-tycho-1:/dev/rdsk/c0t0d0  /dev/did/rdsk/d2
// 3 phys-tycho-4:/dev/rdsk/c1t1d0  /dev/did/rdsk/d3
// 3 phys-tycho-3:/dev/rdsk/c1t1d0  /dev/did/rdsk/d3
// 4 phys-tycho-4:/dev/rdsk/c1t2d0  /dev/did/rdsk/d4
// 4 phys-tycho-3:/dev/rdsk/c1t2d0  /dev/did/rdsk/d4
//
// returns 1 on error and 0 on success
//
int
partition_tool::get_node_device_info(const char *inputfile)
{

	FILE *fp = NULL;
	char buff[BUFFER_SIZE];

	int line_position;
	int j;
	int k;
	int token_size;
	int start_token;

	//
	// The first column of scdidadm -L output
	// Will contain the global disk number
	//
	char *token1 = NULL;

	//
	// The second column of scdidadm -L output
	// Will contain the hostname:devicename
	//
	char *token2 = NULL;

	//
	// the thrid column of scdidadm -L output
	// Will contain the did device name
	//
	char *token3 = NULL;

	int node_name_end = 0;

	char *previous_disk_name = NULL;

	int previous_disk_node_count = 0;

	device_count = 0;

	fp = fopen(inputfile, "r");
	if (fp == NULL) {
		(void) printf(gettext("Error get_node_device_info: "
		    "Failed to open file %s\n"), inputfile);
		return (1);
	}

	while (fgets(buff, BUFFER_SIZE, fp) != NULL) {
		if (device_count % 10 == 0) {
			//
			// We have to realloc.
			// Allocate memory for 10 rows at a time
			//
			node_dev = (char **)realloc(node_dev, (sizeof (char *)
			    * ((unsigned int)device_count + 10)));
		}
		node_dev[device_count] = (char *)calloc(
		    (unsigned int)node_count, sizeof (char));

		// Parse the whole input line for token 1
		for (line_position = 0; line_position < BUFFER_SIZE &&
		    buff[line_position] != '\n'; line_position++) {
			// Look for token 1
			if (buff[line_position] != ' ' &&
			    buff[line_position] != '\t') {
				continue;
			} else {
				// Allocate memory for token 1 and store it
				token1 = (char *)realloc(token1,
				    (unsigned int)(line_position + 1)
				    * sizeof (char));
				for (j = 0; j < line_position; j++) {
					token1[j] = buff[j];
				}
				token1[j] = '\0';
				break;
			}
		} // End of for(line_position...)

		// Skip the white spaces till u reach the next token
		for (; line_position < BUFFER_SIZE &&
		    buff[line_position] != '\n'; line_position++) {
			if (buff[line_position] == ' ' ||
			    buff[line_position] == '\t') {
				continue;
			} else {
				break;
			}
		}

		// Store location of Start of token 2 in start_token
		start_token = line_position;

		//
		// node_name_end will store the end of the
		// point where the nnode name ends
		//
		node_name_end = 0;

		for (; line_position < BUFFER_SIZE &&
		    buff[line_position] != '\n'; line_position++) {
			// Look for token 2
			if (buff[line_position] != ' ' &&
			    buff[line_position] != '\t') {
				if (buff[line_position] == ':') {
					//
					// Store the location of the
					// end of node name
					//
					node_name_end = line_position;
				}
				continue;
			} else {
				// Allocate memory for token 2 and store it
				token2 = (char *)realloc(token2,
				    (unsigned int)(node_name_end - start_token
				    + 1) * sizeof (char)); /*lint !e671 */
				token_size = 0;
				for (j = start_token; j < node_name_end; j++) {
					token2[token_size++] =
					    buff[j]; /*lint !e661 */
				}
				token2[token_size] = '\0';
				break;
			}
		} // End of for(line_position...) for token 2

		// Skip the white spaces till you reach the next token
		for (; line_position < BUFFER_SIZE &&
		    buff[line_position] != '\n'; line_position++) {
			if (buff[line_position] == ' ' ||
			    buff[line_position] == '\t') {
				continue;
			} else {
				break;
			}
		}

		// Store location of Start of token 3 in start_token
		start_token = line_position;

		//
		// The  size of the disk name
		// i.e the size of token 3
		//

		for (; line_position < BUFFER_SIZE &&
		    buff[line_position] != '\n'; line_position++) {
			// Look for token 3
			if (buff[line_position] != ' ' &&
			    buff[line_position] != '\t') {
				continue;
			} else {
				break;
			}
		} // End of for(line_position...) for token 3

		// Allocate memory for token 3 and store it
		token3 = (char *)realloc(token3,
		    (unsigned int)(line_position - start_token + 1)
		    * sizeof (char));

		token_size = 0;
		for (j = start_token; j < line_position; j++) {
			token3[token_size++] = buff[j]; /*lint !e661 */
		}
		token3[token_size] = '\0';

		// we don't care about the rest of the line

		//
		// Start filling the node_dev array. The array looks like
		// node_dev[device_name][node_name]
		// Local disks should not be inserted into node_dev array
		//

		if (device_count == 0) {
			// Processing the first disk
			previous_disk_name = (char *)realloc(
			    previous_disk_name, (strlen(token3) + 1) *
			    sizeof (char)); /*lint !e668 */
			(void) strcpy(previous_disk_name,
			    token3); /*lint !e668 */

			//
			// We have probably encountered the first
			// multihosted disk.
			//
			device_count = 1;

			// We are processing the first instance of this disk
			previous_disk_node_count = 1;

			//
			// Search for the hostname in nodelist and update
			// the corresponding column in node_dev
			//
			for (k = 0; k < node_count; k++) {
				if (strcmp(nodelist[k],
				    token2) == 0) { /*lint !e668 */
					node_dev[device_count - 1][k] = true;
					break;
				}
			}// end of for(k = 0 ...
		} else {
			//
			// device_count != 0
			// Thus we have already found a probable
			// multi hosted disk
			//
			if (strcmp(previous_disk_name,
			    token3) == 0) { /*lint !e668 */

				// This is a multi hosted disk for sure.
				previous_disk_node_count++;

				//
				// Search for the hostname in nodelist
				// and update the corresponding column
				// in node_dev
				//
				for (k = 0; k < node_count; k++) {
					if (strcmp(nodelist[k],
					    token2) == 0) { /*lint !e668 */
						node_dev[device_count - 1][k]
						    = true;
						break;
					}
				}// end of for(k = 0 ...
			} else {
				//
				// New disk is found.
				// Check if the previous disk was
				// multi hosted or not
				//
				if (previous_disk_node_count > 1) {
					//
					// The previous disk was multi hosted
					// Nothing to do
					//
				} else {
					//
					// The previous disk was not multihosted
					// Clean up the table in node_dev
					//
					for (k = 0; k < node_count; k++) {
						node_dev[device_count - 1][k]
						    = false;
					}

					// Decrement the device count
					device_count--;

				}

				// A new disk has been found.
				device_count++;

				// The new disk is atleast attached to one node
				previous_disk_node_count = 1;

				// Store the name of the new disk
				previous_disk_name = (char *)realloc(
				    previous_disk_name, (strlen(token3) + 1)
				    * sizeof (char)); /*lint !e668 */
				(void) strcpy(previous_disk_name,
				    token3); /*lint !e668 */

				//
				// Search for the hostname in nodelist
				// and update the corresponding
				// column in node_dev
				//
				for (k = 0; k < node_count; k++) {
					if (strcmp(nodelist[k],
					    token2) == 0) { /*lint !e668 */
						node_dev[device_count - 1][k]
						    = true;
						break;
					}
				}// end of for(k = 0 ...

			}

		} // End of (device_count == 0)

	} // End of while

	//
	// We need to check if the last disk read was really
	// a multihosted disk or not
	//
	if (device_count > 0) {
		if (previous_disk_node_count > 1) {
			//
			// The previous disk was multi hosted
			// Nothing to do
			//
		} else {
			//
			// The previous disk was not multihosted
			// Clean up the table in node_dev
			//
			for (k = 0; k < node_count; k++) {
				node_dev[device_count - 1][k] = false;
			}

			// Decrement the device count
			device_count--;
		}
	}

	free(token1);
	free(token2);
	free(token3);

	(void) fclose(fp);

	return (0);
}

//
// This function will add a dummy disk to all the nodes only if there are
// no shared disks. It adds this information in the node_dev array.
// This is done when we detect that the cluster has no shared disk
// In order for using the same algorith to find out the partitions
// we add a dummy disk and generate the partition list.
//
int
partition_tool::add_dummy_disk()
{
	int i;

	if (device_count == 0) {
		for (i = 0; i < node_count; i++) {
			node_dev[0][i] = true;

		}
		device_count = 1;
	}

	return (0);
}

//
// Returns true if it finds a partition with the present
// 0 to n-1 nodes placed as they are by manipulating the n to node_count
// nodes around
//
// Logic:
// We follow a logic similar to N Queens
//
// We have two arrays, one for each partition
// part_a : Partiton A nodes
// part_b : Partition B nodes
// dev_c  : All devices not in partition A or B
//
// A "good partition" set is one in which all the shared disks are
// available to both the partitions. The algorithm assumes that the
// system will always be able to assign a device to both partitions
// if the nodes attached to the devices has not been assigned to
// any of the partitions.  Using the function "good" we check
// if it is possible to get a good partition once we have placed all the
// nodes in the 2 partitions.
//
// We place a node in the first partition and then check the possibility
// of getting a good partition. If yes, then we add the next node to the
// first partition and check for the possibility of a good partition.
// Like this we go on till we have assigned all the nodes to each of the
// partitions and the partition is good.
// Whenever we detect that the possibility doesnt exist, then we back track
// and assign the node in the other partition. Once we have assigned it to
// both partitions, we back track further to reassign the previous node.
//
// e.g
// We have a 4 node cluster
// Nodes: N1, N2, N3, N4
// Disks: D1, D2
// N1 and N2 connected to D1
// N3 and N4 connected to D2
// PA: PARTITION A
// PB: PARTITION B
//
// We call partition() with 1
//
// 	N1	N2	N3	N4
// PA	1
// PB	0
//
// Check for possibility of a good partition calling good()
// good() returns true
//
// So we assign the next node N2 to PA
//
//  	N1	N2	N3	N4
// PA	1	1
// PB	0	0
//
// Check for possibility of a good partition calling good()
// good() returns false becasue if we go on to build a partition like
// this, then D1 will always be connected to PA
//
// So we dont assign the 3rd node, instead assign N2 to PB
//
//  	N1	N2	N3	N4
// PA	1	0
// PB	0	1
//
// Check for possibility of a good partition calling good()
// good() returns true, because now D1 can be in both partitions
//
// Now assign the next node
//
//  	N1	N2	N3	N4
// PA	1	0	1
// PB	0	1	0
//
// Check for possibility of a good partition calling good()
// good() returns true. Still there is a potential for finding a suitable
// partition
//
// We assign the next node
//
//  	N1	N2	N3	N4
// PA	1	0	1	1
// PB	0	1	0	0
//
// Check for possibility of a good partition calling good()
// good() returns false since D2 will be available only to PB
//
// We back track and interchange the assignment of N4
//
//  	N1	N2	N3	N4
// PA	1	0	1	0
// PB	0	1	0	1
//
// Check for possibility of a good partition calling good()
// good returns true. Also we have assigned all the nodes to each
// of the partitions. Hence we have found the first good partition set
//
// Output:
//	PA : N1, N3
//	PB : N2, N4
//
// Now we back track. We have tried out assigning N4 to both PA and PB
// So we reassign N3
//
//  	N1	N2	N3	N4
// PA	1	0	0
// PB	0	1	1
// Check for possibility of a good partition calling good()
// good returns true.
//
// So we assgn N4
//
//  	N1	N2	N3	N4
// PA	1	0	0	1
// PB	0	1	1	0
//
// Check for possibility of a good partition calling good()
// good returns true. Also we have assigned all the nodes to each
// of the partitions. Hence we have found the second good partition set
//
// Output:
//	PA : N1, N4
//	PB : N2, N3
//
// Now we back track again, interchange the assignment of N4
//
//  	N1	N2	N3	N4
// PA	1	0	0	0
// PB	0	1	1	1
//
// Check for possibility of a good partition calling good()
// good returns false. Back track further. We have tried both combinations
// for N4 and N3 and N2 and hence we have come to the end of finding
// all possible valid partitions.
//
bool
partition_tool::partition(int n)
{
	bool ret = false;

	// Try placing the node 'n' in partition A
	part_a[n] = true;
	part_b[n] = false;
	if (good(n)) {
		if (n == (node_count - 1)) {
			// We have found a valid partition!
			print_partition();
			ret = true;
		} else {
			ret = partition(n+1);
		}
	}

	// Try placing the node 'n' in partition B
	part_a[n] = false;
	part_b[n] = true;
	if (good(n)) {
		if (n == (node_count - 1)) {
			// We have found a valid partition!
			print_partition();
			ret = true;
		} else {
			ret = partition(n+1);
		}
	}

	return (ret);
}

//
// Returns true if the partition so far is ok.
// Returns false if, with the current partition information
// there is no way we can proceed and complete successfully
//
bool
partition_tool::good(int n)
{
	int i;
	int j;

	// Array of node to device mappings for partition a
	char *dev_a;

	// Array of node to device mappings for partition b
	char *dev_b;

	dev_a = (char *)calloc((unsigned int)device_count, sizeof (char));
	dev_b = (char *)calloc((unsigned int)device_count, sizeof (char));
	(void) memset(dev_c, 0,
		((unsigned int)device_count * sizeof (char))); /*lint !e668 */

	//
	// Construct dev_c such that it contains all the devices
	// not assigned to partition A or B
	//
	for (i = n + 1; i < node_count; i++) {
		for (j = 0; j < device_count; j++) {
			dev_c[j] = (node_dev[j][i] | dev_c[j]);
		}
	}

	// Construct dev_a  containing all the devices in A
	for (i = 0; i <= n; i++) {
		if (part_a[i]) {
			for (j = 0; j < device_count; j++) {
				dev_a[j] = (node_dev[j][i] | dev_a[j]);
			}
		}
	}

	// Construct dev_b containing all the devices in B
	for (i = 0; i <= n; i++) {
		if (part_b[i]) {
			for (j = 0; j < device_count; j++) {
				dev_b[j] = (node_dev[j][i] | dev_b[j]);
			}
		}
	}

	//
	// Would partition A have full storage connectivity if it were
	// merged with the devices in C ?
	//
	for (j = 0; j < device_count; j++) {
		if (dev_a[j] == (char)false && dev_c[j] == (char)false) {
			//
			// Partition A would not have full connectivity!
			//
			return (false);
		}
	}

	//
	// Would partition B have full storage connectivity if it were
	// merged with devices in C ?
	//
	for (j = 0; j < device_count; j++) {
		if (dev_b[j] == (char)false && dev_c[j] == (char)false) {
			//
			// Partition B would not have full connectivity!
			//
			return (false);
		}
	}

	//
	// There is a posibility that we can proceed and
	// still get full connectivity for both partitions.
	//
	return (true);
}

void
partition_tool::print_partition()
{
	int i;

	//
	// Node with index zero is node 1
	// This is the only part of this code where
	// a zero based index is NOT used.
	// The rest of the program uses a 0 based index.
	//

	// We have found another possible partition
	partition_count++;

	(void) printf(gettext("  Option %d\n"), partition_count);
	(void) printf(gettext("    First partition\n"));
	for (i = 0; i < node_count; i++) {
		if (part_a[i]) {
			(void) printf(gettext("      %s\n"), nodelist[i]);
		}
	}

	(void) printf(gettext("    Second partition\n"));
	for (i = 0; i < node_count; i++) {
		if (part_b[i]) {
			(void) printf(gettext("      %s\n"), nodelist[i]);
		}
	}

	(void) printf(gettext("\n"));
}

void
partition_tool::print_partition_2node()
{

	(void) printf(gettext("  Option 1\n"));
	(void) printf(gettext("    First partition\n"));
	(void) printf(gettext("      %s\n"), nodelist[0]);
	(void) printf(gettext("    Second partition\n"));
	(void) printf(gettext("      %s\n"), nodelist[1]);
	(void) printf(gettext("\n"));
}

//
// This function is for debugging purposes.
// This displays the mappings of nodes to devices.
// This function needs to be improved to display o/p
// in a more easily readable and understandable form
//
void
partition_tool::print_node_device()
{
	int i;
	int j;

	(void) printf("****** DEBUG INFORMATION ******\n");

	(void) printf("Number of nodes = %d\n", node_count);
	(void) printf("Number of shared disks = %d\n", device_count);

	(void) printf(gettext("\nDevice list\n-----------\n"));
	for (i = 0; i < node_count; i++) {
		(void) printf(gettext("%s: "), nodelist[i]);
		for (j = 0; j < device_count; j++) {
			if (node_dev[j][i]) {
				(void) printf(gettext("d%d "), (j + 1));
			}
		}
		(void) printf(gettext("\n"));
	}

	(void) printf(gettext("\n"));

	(void) printf("****** END OF DEBUG INFORMATION ******\n");

}

//
// Returns the total number of cluster nodes
//
int
partition_tool::get_node_count()
{
	return (node_count);
}

//
// Returns the total number of valid partirions
//
int
partition_tool::get_partition_count()
{
	return (partition_count);
}


//
// ------- Methods to execute SC cli commands -------------
//

//
// save the current locale.
//
void
partition_tool::save_env()
{
	char *lc_all = NULL;
	if (!locale_saved) {
		lc_all = getenv("LC_ALL");
		if (lc_all != NULL) {
			// Save the LC_ALL environment value
			(void) strncpy(current_locale, lc_all, PATH_MAX);
			locale_saved = 1;
			return;
		} else {
			// LC_ALL not set in the environment
			current_locale[0] = NULL;
			return;
		}
	} else {
		return;
	}
}

//
// set the LC_ALL Environment value to "C".
//
int
partition_tool::set_env_posix()
{
	save_env();
	return (putenv("LC_ALL=C"));
}

//
// set the  LC_ALL environment value to the value saved in 'current_locale'
//
int
partition_tool::reset_env()
{
	char env[PATH_MAX];

	if (!locale_saved) {
		// Nothing has been saved to reset.
		return (0);
	}
	if (current_locale[0] == NULL) {
		(void) snprintf(env, PATH_MAX, "LC_ALL=");
	} else {
		(void) snprintf(env, PATH_MAX, "LC_ALL=%s", current_locale);
	}
	return (putenv(env));
}


//
// Executes a command with popen. It returns the return value
// of the command through the status argument.
//
int
partition_tool::execute_cmd_with_popen(const char *cmd, int *status)
{
	FILE *stream = NULL;
	int retval = 0;

	if ((stream = popen(cmd, "r")) == NULL) {
		(void) printf(gettext("Failed to execute the command %s\n"),
		    cmd);
		return (1);
	}
	if ((retval = pclose(stream)) == -1) {
		    (void) printf(gettext("Failed to close the pipe created for"
		    " command %s\n"), cmd);
		return (1);
	}
	if (WIFEXITED(retval)) {

		//
		// Obtain the exit status as the command completed normally.
		//
		*status =  WEXITSTATUS((uint_t)retval);
	} else {

		//
		// Process was terminated by the signal.
		//
		(void) printf(gettext("The command %s was terminated by the "
		    "signal\n"), cmd);

		*status = -1;
		return (1);
	}

	// Success
	return (0);
}


//
// ------------------ MAIN ------------------
//

int
main(int argc, char *argv[])
{

	char cmd_buf[256];
	partition_tool ptool;
	int retval = 0;
	int fd;

	//
	// This command should be blocked from running from non-global zones.
	// We dont know beforehand the OS and SC versions of the nodes on which
	// this command is run. We determine if the OS is S10 and 3.2. Zone
	// support exists only on S10 and 3.2, it suffices to check only for
	// these versions.
	//
	if ((fd = open(ZONECHECK_CMD, O_RDONLY)) > 0) {

		(void) sprintf(cmd_buf, "%s > %s", ZONECHECK_CMD, "/dev/null");

		if (ptool.execute_cmd_with_popen(cmd_buf, &retval) != 0) {
			(void) printf(gettext("Failed to execute command %s."
			    " Exiting.\n"), ZONECHECK_CMD);
			return (1);
		}
		//
		// If the command retuned a non-zero value, it means we are not
		// in the global zone.
		//
		if (retval != 0) {
			return (1);
		}
		(void) close(fd);
	}

	if (argc == 1) {
		//
		// No parameters
		// To be run on a real cluster
		//

		// Save the current locale
		ptool.save_env();

		// Change the locale to C
		(void) ptool.set_env_posix();

		//
		// Execute the command to get the device information
		//
		(void) sprintf(cmd_buf, "%s > %s", DEV_LIST_CMD, DEV_LIST_FILE);
		if (ptool.execute_cmd_with_popen(cmd_buf, &retval) != 0) {
			(void) printf(gettext("Failed to get the device list."
			    " Exiting\n"));
			return (1);
		}

		// Execute the command to get node information
		(void) sprintf(cmd_buf, "%s > %s", NODE_LIST_CMD,
		    NODE_LIST_FILE);
		if (ptool.execute_cmd_with_popen(cmd_buf, &retval) != 0) {
			(void) printf(gettext("Failed to get the node list."
			    " Exiting\n"));
		}

		// Restore the original locale
		(void) ptool.reset_env();

		// Get the node information
		(void) ptool.get_node_info(NODE_LIST_FILE);

		//
		// If it is a single node cluster then
		// return an error.
		//
		if (ptool.get_node_count() == 1) {
			(void) printf(gettext("Single-node clusters are not"
			    " candidates for dual-partition software swap\n"));
			return (1);
		}

		//
		// If two node cluster, then directly
		// assign the nodes
		//
		if (ptool.get_node_count() == 2) {
			ptool.print_partition_2node();
			return (0);
		}

		// Build the device to node mappings
		(void) ptool.get_node_device_info(DEV_LIST_FILE);

	} else if (argc != 3) {
		(void) printf(
		    gettext("Usage: %s <nodelistfile> <devicelist file>\n"));
		(void) printf(gettext("\nEach line in the devicelist file must"
		    "be of the format:\n"));
		(void) printf(
		    gettext("DiskNo hostname:cxtxdxsx     D<DiskNo>\n"));
		return (2);
	} else {
		// (argc == 3)
		(void) ptool.get_node_info(argv[1]);
		(void) ptool.get_node_device_info(argv[2]);
	}

	//
	// check if no shared disks were found, then add a dummy device
	//
	(void) ptool.add_dummy_disk();

	//
	// Print the node device mappings - Debugging purposes only
	//
	// ptool.print_node_device();

	//
	// Allocate memory for the partition data structures
	//
	ptool.alloc_partitions();

	//
	// Node 0 is always in partition A.
	// This way we avoid generating one duplicate partition set.
	// - Moved to the alloc_partitions
	// A[0] = true;
	//

	(void) ptool.partition(1);

	return (0);
}
