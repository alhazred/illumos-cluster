/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */


#pragma ident	"@(#)scha_resourcetype_get.cc	1.18	08/07/21 SMI"

#include <sys/os.h>
#include "schacmd.h"
#include "rrtcmd.h"
#include <sys/cl_auth.h>
#include <sys/sol_version.h>
#if SOL_VERSION >= __s10
#include <zone.h>
#endif

static schaxrot_t *xlate_optag(const char *intag);

int
main(int argc, char **argv)
{
	schaxrot_t *rotrecp	= NULL;
	scha_err_t retval	= SCHA_ERR_NOERR;
	char *extra_arg = NULL;
	scha_resourcetype_t	handle	= NULL;

	Arguments arguments;

	/* Initialize, demote to normal uid, check */
	/* authorization and then promote to euid=0 */
	cl_auth_init();
	cl_auth_demote();
	cl_auth_check_command_opt_exit(*argv, NULL,
	    CL_AUTH_RESOURCE_READ, SCHA_ERR_ACCESS);
	cl_auth_promote();

	getarguments_optstring(argc, argv, "O:T:Z:", &arguments);

	/* scha_resourcetype_get, looking for "O","T" args */
	if (arguments.hadbadarg == 1 ||
		(arguments.Targ == NULL) ||
		(arguments.Oarg == NULL)) {

		return (SCHA_ERR_INVAL);
	}

	rotrecp = xlate_optag(arguments.Oarg);
	if (rotrecp == NULL || rotrecp->rotcode == ROT_NONE) {
		return (SCHA_ERR_TAG);
	}

	/*
	 * Except for the PER_NODE query, no extra argument
	 * is required for any of RT query tags. SCHA_ERR_INVAL
	 * is returned in all other cases.
	 */
	if (arguments.optind < argc) {
		if (rotrecp->rotcode != ROT_IS_PER_NODE)
			return (SCHA_ERR_INVAL);
		extra_arg = argv[arguments.optind];
	}

	if (arguments.optind + 1 < argc) {
		return (SCHA_ERR_INVAL);
	}
	if (arguments.Zarg) {
#if SOL_VERSION >= __s10
		zoneid_t zoneid = getzoneid();
		if (zoneid != GLOBAL_ZONEID) {
			char zonename[ZONENAME_MAX];
			getzonenamebyid(getzoneid(), zonename, ZONENAME_MAX);
			if (strcmp(zonename, arguments.Zarg) != 0) {
				return (SCHA_ERR_INVAL);
			}
		}
#else
		return (SCHA_ERR_INVAL);
#endif
	}

	retval = scha_resourcetype_open_zone(arguments.Zarg,
	    arguments.Targ, &handle);

	if (retval != SCHA_ERR_NOERR) {
		return (retval);
	}
	if (handle == NULL) {
		return (SCHA_ERR_HANDLE);
	}


	switch (rotrecp->rottype) {

	default:
	case ROT_T_NONE:
	case ROT_T_ULL:
	case ROT_T_FOM:
	case ROT_T_SWITCH:
	case ROT_T_STATUS:
	case ROT_T_STATE:
	case ROT_T_EXT:

		retval = SCHA_ERR_TAG;
		break;

	case ROT_T_STR:
	{
		/*
		 * String value results
		 * No extra vararg argument except out parameter
		 */
		char 		*strval = NULL;
		retval  = scha_resourcetype_get_zone(arguments.Zarg, handle,
				rotrecp->schatag, &strval);
		if (errval_ok(&retval) && (strval != NULL)) {
			(void) printf(STRFMT, strval);
		}
		break;
	}
	case ROT_T_INT:
	{
		/*
		 * Int value results
		 * No extra vararg argument except out parameter
		 */
		int	intval = 0;
		retval  = scha_resourcetype_get_zone(arguments.Zarg, handle,
					rotrecp->schatag, &intval);
		if (errval_ok(&retval)) {
			(void) printf(INTFMT, intval);
		}
		break;
	}
	case ROT_T_BOOL:
	{
		/*
		 * boolean_t (declared in <sys/types.h> value results
		 * Extra vararg argument(property name) would be passed
		 * when the rotcode is IS_PER_NODE.
		 */
		boolean_t bval = B_FALSE;
		if (rotrecp->rotcode == ROT_IS_PER_NODE) {
			retval = scha_resourcetype_get_zone(arguments.Zarg,
			    handle,
				rotrecp->schatag, extra_arg, &bval);
		} else {
			retval = scha_resourcetype_get_zone(arguments.Zarg,
			    handle,
				rotrecp->schatag, &bval);
		}
		if (errval_ok(&retval)) {
			if (bval == B_TRUE) {
				(void) printf(TRUEFMT);
			} else {
				(void) printf(FALSEFMT);
			}
		}
		break;
	}
	case ROT_T_STRAR:
	{
		/*
		 * Pointer-to str_array_t (declared in scha_types.h)
		 * value results
		 * No extra vararg argument except out parameter
		 */
		scha_str_array_t *strarval = NULL;
		retval  = scha_resourcetype_get_zone(arguments.Zarg,
		    handle, rotrecp->schatag,
						&strarval);
		if (errval_ok(&retval) && (strarval != NULL)) {
			print_str_array(strarval);
		}
		break;
	}
	case ROT_T_INIT:
	{
		/*
		 * scha_initnodes_flag_t (declared in scha_types.h) results
		 * No extra vararg argument except out parameter
		 */
		scha_initnodes_flag_t ifval = SCHA_INFLAG_RG_PRIMARIES;
		retval  = scha_resourcetype_get_zone(arguments.Zarg, handle,
					rotrecp->schatag, &ifval);
		if (errval_ok(&retval)) {
			/*
			 * Translate enums to strings defined
			 * in scha_strings.h
			 */
			char *str = SCHA_RG_PRIMARIES;
			if (ifval == SCHA_INFLAG_RG_PRIMARIES) {
				str = SCHA_RG_PRIMARIES;
			} else if (ifval == SCHA_INFLAG_RT_INSTALLED_NODES) {
				str = SCHA_RT_INSTALLED_NODES;
			}
			(void) printf(STRFMT, str);
		}
		break;
	}
	}	/* end of switch (rotrecp->rottype) */

	/*
	 * Return value of close operation not checked.
	 * what is returned from the command is the status
	 * of the "get" function
	 */
	(void) scha_resourcetype_close_zone(arguments.Zarg, handle);
	return (retval);
}

/*
 * Translation table for resource-type operation tags is common
 * to both scha_resource_get and scha_resourcetype_get ans
 * is declared in rrtcmd.h
 */

static schaxrot_t *
xlate_optag(const char *intag)
{
	schaxrot_t *tabp = NULL;

	if (intag == NULL || *intag == 0) {
		return (NULL);
	}

	for (tabp = schaxrtot_tab; tabp->schatag != NULL; tabp++) {
		if (strcasecmp(intag, tabp->schatag) == 0) {
			return (tabp);
		}
	}
	return (NULL);
}
