/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scha_cluster_get.cc	1.17	08/07/21 SMI"

#include <sys/os.h>
#include "schacmd.h"
#include <sys/sol_version.h>
#if SOL_VERSION >= __s10
#include <zone.h>
#include <scadmin/scconf.h>
#include <libzccfg/libzccfg.h>
#endif


/*
 * enum values for every optag that is to be handled by scha_cluster_get.
 * String operation tags are translated to these codes to simplify
 * handling of each optag case.
 */

typedef enum cot_tag_t {
	COT_NONE = 0,
	COT_NODENAME_LOCAL,
	COT_NODENAME_NODEID,
	COT_ALL_NODENAMES,
	COT_NODEID_LOCAL,
	COT_NODEID_NODENAME,
	COT_ALL_NODEIDS,
	COT_PRIVATELINK_HOSTNAME_LOCAL,
	COT_PRIVATELINK_HOSTNAME_NODE,
	COT_ALL_PRIVATELINK_HOSTNAMES,
	COT_NODESTATE_LOCAL,
	COT_NODESTATE_NODE,
	COT_SYSLOG_FACILITY,
	COT_ALL_RESOURCEGROUPS,
	COT_ALL_RESOURCETYPES,
	COT_CLUSTERNAME,
	COT_ZONE_LOCAL,
	COT_ALL_ZONES,
	COT_ALL_NONGLOBAL_ZONES,
	COT_ALL_ZONES_NODEID,
	COT_ALL_NONGLOBAL_ZONES_NODEID,
	COT_ZONE_TYPE,
	COT_VERIFY_IP,
	COT_VERIFY_ADAPTER
} cot_tag_t;

/*
 * Enum codes for indicating what argument types may be needed by
 * the above operation tags.
 * These are a subset of the types for the scha_resource_get
 * operation tags.
 */
typedef enum cot_type_t {
	COT_T_NONE = 0,
	COT_T_STR,
	COT_T_UINT,
	COT_T_UINTAR,
	COT_T_NSTAT,
	COT_T_STRAR
} cot_type_t;

typedef struct schaxcot_t {
	char			*schatag;
	cot_tag_t		cotcode;
	cot_type_t		cottype;
} schaxcot_t;

static schaxcot_t *xlate_optag(const char *intag);

int
main(int argc, char **argv)
{
	schaxcot_t *cotrecp		= NULL;
	scha_err_t retval		= SCHA_ERR_NOERR;
	scha_cluster_t handle		= NULL;
	char *extra_arg			= "";
	uint_t extra_ui_arg		= 0; /* converted extra_arg */
	int sscanf_result 		= 0; /* result of conversion of */
						/* extra_arg */
#if SOL_VERSION >= __s10
	uint_t zc_id			= 0;
	scconf_errno_t scconf_err	= SCCONF_NOERR;
	zone_cluster_state_t zc_state	= ZC_STATE_UNKNOWN;
	int zc_cfg_err			= ZC_OK;
#endif

	Arguments arguments;

	getarguments_optstring(argc, argv, "O:Z:", &arguments);

	/*
	 * scha_cluster_get, only looking for "O" arg
	 * An Extra vararg arument may be needed
	 */
	if (arguments.hadbadarg == 1 ||
		(arguments.Oarg == NULL)) {

		return (SCHA_ERR_INVAL);
	}

	cotrecp = xlate_optag(arguments.Oarg);
	if (cotrecp == NULL || cotrecp->cotcode == COT_NONE) {
		return (SCHA_ERR_TAG);
	}

	/*
	 * Extra vararg argument may be needed for specifying
	 * nodename or nodeid.
	 * Generally ignore any errors in providing extra
	 * arguments that may not be needed, but for the optag that
	 * needs a nodeid, make sure the extra arg converts to
	 * an unsigned int.
	 */
	if (arguments.optind < argc) {
		extra_arg = argv[arguments.optind];
	}
	/*
	 * Check for uint converted nodeid extra argument for the operations
	 * that need it
	 */
	if ((cotrecp->cotcode == COT_NODENAME_NODEID ||
	    cotrecp->cotcode == COT_ALL_ZONES_NODEID ||
	    cotrecp->cotcode == COT_ALL_NONGLOBAL_ZONES_NODEID) &&
	    sscanf_result != 1) {
		sscanf_result = sscanf(extra_arg, UINTFMT, &extra_ui_arg);
		if (sscanf_result != 1) {
			/* failed to convert an unsigned-int extra_arg */
			return (SCHA_ERR_NODE);
		}
	}

	if (arguments.Zarg) {
#if SOL_VERSION >= __s10
		zoneid_t zoneid = getzoneid();
		if (zoneid != GLOBAL_ZONEID) {
			char zonename[ZONENAME_MAX];
			getzonenamebyid(getzoneid(), zonename, ZONENAME_MAX);
			if (strcmp(zonename, arguments.Zarg) != 0) {
				return (SCHA_ERR_INVAL);
			}
		} else {
			/*
			 * We are in the global zone. We have to check whether
			 * the specified zone cluster is valid or not.
			 */
			scconf_err = scconf_get_zone_cluster_id(
							(char *)arguments.Zarg,
							&zc_id);
			if (scconf_err != SCCONF_NOERR) {
				/*
				 * The specified zone cluster is an invalid
				 * zone cluster
				 */
				return (SCHA_ERR_ZONE_CLUSTER);
			}

			/*
			 * This zone cluster is valid. We have to check whether
			 * it is in the "Running" state or not.
			 */
			zc_cfg_err = get_zone_cluster_state(arguments.Zarg,
								&zc_state);
			if (zc_cfg_err != ZC_OK) {
				/*
				 * We cannot determine the status. We will
				 * return an internal error.
				 */
				return (SCHA_ERR_INTERNAL);
			}

			if (zc_state < ZC_STATE_RUNNING) {
				/*
				 * The zone cluster is not in "Running" state.
				 */
				return (SCHA_ERR_ZC_DOWN);
			}
		}
#else
		return (SCHA_ERR_INVAL);
#endif
	}
	retval = scha_cluster_open_zone(arguments.Zarg, &handle);

	if (retval != SCHA_ERR_NOERR) {
		return (retval);
	}
	if (handle == NULL) {
		return (SCHA_ERR_HANDLE);
	}

	switch (cotrecp->cottype) {

	default:
	case COT_T_NONE:
		retval = SCHA_ERR_TAG;
		break;

	case COT_T_STR:
	{
		/*
		 * String value results
		 * Extra nodeid vararg argument may be needed
		 */
		char 		*strval = NULL;
		if (cotrecp->cotcode == COT_NODENAME_NODEID) {
			retval  = scha_cluster_get_zone(arguments.Zarg, handle,
					cotrecp->schatag, extra_ui_arg,
					&strval);
		} else if (cotrecp->cotcode == COT_PRIVATELINK_HOSTNAME_NODE) {
			retval  = scha_cluster_get_zone(arguments.Zarg,
			    handle, cotrecp->schatag,
					extra_arg, &strval);
		} else {
			retval  = scha_cluster_get_zone(arguments.Zarg, handle,
					cotrecp->schatag, &strval);
		}
		if (errval_ok(&retval) && (strval != NULL)) {
			(void) printf(STRFMT, strval);
		}
		break;
	}
	case COT_T_UINT:
	{
		/*
		 * Int value results
		 * Extra nodename vararg argument may be needed
		 */
		int	uintval = 0;
		if (cotrecp->cotcode == COT_NODEID_NODENAME ||
		    cotrecp->cotcode == COT_VERIFY_IP ||
		    cotrecp->cotcode == COT_VERIFY_ADAPTER) {
			retval  = scha_cluster_get_zone(
			    arguments.Zarg, handle,
			    cotrecp->schatag, extra_arg,
			    &uintval);
		} else {
			retval  = scha_cluster_get_zone(
			    arguments.Zarg, handle,
			    cotrecp->schatag, &uintval);
		}
		if (errval_ok(&retval)) {
			(void) printf(UINTFMT, uintval);
		}
		break;
	}
	case COT_T_UINTAR:
	{
		/*
		 * scha_uint_array_t (declared in scha_types.h) value results
		 * for all node-ids in a cluster.
		 * No extra vararg argument except out parameter
		 */
		scha_uint_array_t *uintarval = NULL;
		retval  = scha_cluster_get_zone(arguments.Zarg,
		    handle, cotrecp->schatag,
						&uintarval);
		if (errval_ok(&retval) && (uintarval != NULL)) {
			uint_t ix;
			for (ix = 0; ix < uintarval->array_cnt; ix++) {
			    (void) printf(UINTFMT, uintarval->uint_array[ix]);
			}
			/*
			 * There should always be nodes in the cluster, but
			 * handle degenerate zero-sized array  anyway
			 */
			if (uintarval->array_cnt == 0) {
				(void) printf(EMPTYLINEFMT);
			}
		}
		break;
	}
	case COT_T_STRAR:
	{
		/*
		 * scha_str_array_t (declared in scha_types.h) value results
		 * No extra vararg argument except out parameter
		 */
		scha_str_array_t *strarval = NULL;
		if (cotrecp->cotcode == COT_ALL_ZONES_NODEID ||
		    cotrecp->cotcode == COT_ALL_NONGLOBAL_ZONES_NODEID) {
			retval = scha_cluster_get_zone(arguments.Zarg,
			    handle, cotrecp->schatag,
			    extra_ui_arg, &strarval);
		} else {
			retval  = scha_cluster_get_zone(arguments.Zarg,
			    handle, cotrecp->schatag,
			    &strarval);
		}
		if (errval_ok(&retval) && (strarval != NULL)) {
			print_str_array(strarval);
		}
		break;
	}
	case COT_T_NSTAT:
	{
		/*
		 * scha_node_state_t (declared in scha_types.h) results
		 * Extra vararg argument needed for SCHA_NODESTATE_NODE
		 */
		scha_node_state_t nstatval = SCHA_NODE_UP;
		if (cotrecp->cotcode == COT_NODESTATE_NODE) {
			retval  = scha_cluster_get_zone(arguments.Zarg, handle,
					cotrecp->schatag, extra_arg,
					&nstatval);
		} else {
			retval  = scha_cluster_get_zone(arguments.Zarg, handle,
					cotrecp->schatag, &nstatval);
		}
		if (errval_ok(&retval)) {
			/*
			 * Translate enums to strings defined
			 * in scha_strings.h
			 */
			char *str = SCHA_UP;
			if (nstatval == SCHA_NODE_DOWN) {
				str = SCHA_DOWN;
			}
			(void) printf(STRFMT, str);
		}
		break;
	}
	} /* end of switch (cotrecp->cottype) */

	/*
	 * Return value of close operation not checked.
	 * what is returned from the command is the status
	 * of the "get" function
	 */
	(void) scha_cluster_close_zone(arguments.Zarg, handle);
	return (retval);
}

/*
 * Translation table for resource-group operation tags.
 */
static schaxcot_t schaxcot_tab[] = {
{SCHA_NODENAME_LOCAL,		COT_NODENAME_LOCAL,		COT_T_STR},
{SCHA_NODENAME_NODEID,		COT_NODENAME_NODEID,		COT_T_STR},
{SCHA_ALL_NODENAMES,		COT_ALL_NODENAMES,		COT_T_STRAR},
{SCHA_NODEID_LOCAL,		COT_NODEID_LOCAL,		COT_T_UINT},
{SCHA_NODEID_NODENAME,		COT_NODEID_NODENAME,		COT_T_UINT},
{SCHA_ALL_NODEIDS,		COT_ALL_NODEIDS,		COT_T_UINTAR},
{SCHA_PRIVATELINK_HOSTNAME_LOCAL, COT_PRIVATELINK_HOSTNAME_LOCAL, COT_T_STR},
{SCHA_PRIVATELINK_HOSTNAME_NODE, COT_PRIVATELINK_HOSTNAME_NODE, COT_T_STR},
{SCHA_ALL_PRIVATELINK_HOSTNAMES, COT_ALL_PRIVATELINK_HOSTNAMES, COT_T_STRAR},
{SCHA_NODESTATE_LOCAL,		COT_NODESTATE_LOCAL,		COT_T_NSTAT},
{SCHA_NODESTATE_NODE,		COT_NODESTATE_NODE,		COT_T_NSTAT},
{SCHA_SYSLOG_FACILITY,		COT_SYSLOG_FACILITY,		COT_T_UINT},
{SCHA_ALL_RESOURCEGROUPS,	COT_ALL_RESOURCEGROUPS,		COT_T_STRAR},
{SCHA_ALL_RESOURCETYPES,	COT_ALL_RESOURCETYPES,		COT_T_STRAR},
{SCHA_CLUSTERNAME,		COT_CLUSTERNAME,		COT_T_STR},
{SCHA_ZONE_LOCAL,		COT_ZONE_LOCAL,			COT_T_STR},
{SCHA_ALL_ZONES,		COT_ALL_ZONES,			COT_T_STRAR},
{SCHA_ALL_NONGLOBAL_ZONES,	COT_ALL_NONGLOBAL_ZONES,	COT_T_STRAR},
{SCHA_ALL_ZONES_NODEID,		COT_ALL_ZONES_NODEID,		COT_T_STRAR},
{SCHA_ALL_NONGLOBAL_ZONES_NODEID, COT_ALL_NONGLOBAL_ZONES_NODEID, COT_T_STRAR},
{SCHA_ZONE_TYPE,		COT_ZONE_TYPE,			COT_T_STR},
{SCHA_VERIFY_IP,		COT_VERIFY_IP,			COT_T_UINT},
{SCHA_VERIFY_ADAPTER,		COT_VERIFY_ADAPTER,		COT_T_UINT},
{NULL,				COT_NONE,			COT_T_NONE}
};

static schaxcot_t *
xlate_optag(const char *intag)
{
	schaxcot_t *tabp = NULL;

	if (intag == NULL || *intag == 0) {
		return (NULL);
	}

	for (tabp = schaxcot_tab; tabp->schatag != NULL; tabp++) {
		if (strcasecmp(intag, tabp->schatag) == 0) {
			return (tabp);
		}
	}
	return (NULL);
}
