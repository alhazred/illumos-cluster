/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)hatimerun.cc	1.15	08/05/20 SMI"

/*
 * Originally SC2.2 hatimerun.c 1.7
 *
 * Usage: hatimerun [-v] [-a] [-k signalname] [-e exitcode] -t timeOutSecs
 * 	prog args
 *
 *
 * Runs prog with args as a child subprocess under a timeout, and as its
 * own process group.  The timeout is specified in seconds, by the
 * timeOutSecs  parameter.  If the timeout expires, this process
 * kills the child subprocess's
 * process group with a -KILL signal, and then this process exits with
 * exit code 99.
 *
 * The -v switch means verbose output, on stderr.
 *
 * The -k switch allows one to change what signal is used to kill
 * the child  process group.
 *
 * The -a switch changes the behavior radically.  Instead of killing
 * the child when the timeout expires, the parent process simply
 * exits, with exitcode, leaving the child to run asynchronously.
 *
 * It is illegal to supply both the -a switch and the -k switch.
 *
 * The -e switch allows one to change the exit code for the timeout
 * case to some other value.
 *
 * The -t switch is mandatory and allows one to specify the timeout
 * in seconds.
 *
 * If the timeout does not occur, then we exit with the child's exit
 * status, unless that exit status was equal to our timeout exit
 * code, in which
 * case we remap the child exit status to some other value.
 *
 * This program catches the signal SIGTERM.  It responds to the signal
 * by killing the child as if a timeout had occurred, and then
 * exiting non-zero.
 *
 */


#include <sys/os.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/syslog.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <libintl.h>
#include <locale.h>
#include <ctype.h>

#define	DEFAULT_TIMEOUTEXITCODE 99
#define		INITIAL_BADEXIT 88
#define		HATIMERUN_BADEXIT 98   /* Used for badexit */

#define		DEFAULT_KILLSIG SIGKILL
#define		SIGEXITBASE 128

static pid_t parentsidepid;
static pid_t childpid = 0;
static int cancelalarm = 0;
static int childstatus;
static int timeoutsecs;


static char *myprogname = NULL;
static int verbose = 0;
static int asynch = 0;
static int hadkswitch = 0;
static int timeoutexitcode = DEFAULT_TIMEOUTEXITCODE;
static int killsig = DEFAULT_KILLSIG;
static int badexit = INITIAL_BADEXIT;  	/* Used only when run as fdl_timedrun */

/*
 * lower2upper() - convert data from lower case to upper case
 *
 */
static void
lower2upper(char *in_str)
{

	if (in_str == NULL)
		return;
	while (*in_str) {
		*in_str = (char)toupper((int)*in_str);
		in_str++;
	}
}

static void
usagehalt()
{

	(void) fprintf(stderr, gettext(
		"Usage: %s [-va] [-k signalname] [-e exitcode] -t timeOutSecs "
		"prog args\n"), myprogname);
	exit(HATIMERUN_BADEXIT);
}

static void
ha_error(int flg, const char *msg, const char *fmt, ...)
{
	/*
	 * Place-holder to be filled-in with SC3.0 message logging
	 */

	/*
	 * Simple SC 3.0 substitute - the SC 2.x ha_error() prints the
	 * message to stderr.  This ignores the flg (syslog level) and
	 * msg (message ID) args.
	 */

	va_list	args;

	/*
	 * The following 2 lines are needed to pass lint.
	 */
	flg = flg;
	msg = msg;

	/*
	 * override FlexeLint error 40;
	 * our lint doesn't understand compiler
	 * builtin variable __builtin_va_alist
	 */
	va_start(args, fmt);	/*lint !e40 */
	(void) fprintf(stderr, "%s: ", myprogname);
	(void) vfprintf(stderr, fmt, args);
	va_end(args);
	/*lint -restore */
}

/*ARGSUSED*/
static void
alrmhandler(int unused)
{
	if (cancelalarm)
		return;
	if (verbose)
		ha_error(LOG_INFO, "hatimerun.1001",
		    gettext("timeout expired.\n"));

	if (asynch)
		exit(timeoutexitcode);

	/*
	 * Ignore errors from kill, assume they happened because the
	 * child has already exited.
	 */
	if ((kill(-childpid, killsig) < 0) && verbose) {
		/*
		 * errno.h is missing void arg, so suppress lint error.
		 * [should be errno(void) rather than errno()]
		 */
		ha_error(LOG_ERR, "hatimerun.1002",
			gettext("kill process group of "
			"child process %d failed, errno=%d '%s'\n"),
			childpid, errno, strerror(errno));	/*lint !e746 */
	}

	exit(timeoutexitcode);
} /* alrmhandler */

static void
sighandler(int signal)
{
	/* See if we are in the child process, if so exit. */
	if (getpid() != parentsidepid) {
		(signal == SIGABRT) ? abort() : exit(HATIMERUN_BADEXIT);
	}

	/*
	 * See if we're past the critical section that forks the
	 * child, if not, simply exit.
	 */
	if (childpid <= 0) {
		(signal == SIGABRT) ? abort() : exit(HATIMERUN_BADEXIT);
	}

	/* See if we're past the point where the child finished. */
	if (cancelalarm)
		return;

	if (verbose) {
		ha_error(LOG_INFO, "hatimerun.1003",
		    gettext("caught signal number:%d, killing child."), signal);
	}

	if (signal == SIGABRT)
		killsig = SIGABRT;
	/*
	 * Ignore errors from kill, assume they happened because the
	 * child has already exited.
	 */
	if (kill(-childpid, killsig) < 0) {
		if (verbose)
			ha_error(LOG_ERR, "hatimerun.1004",
			    gettext(" kill process group of child process %d "
			    "failed, errno=%d '%s'\n"),
			    childpid, errno, strerror(errno));
	}

	(signal == SIGABRT) ? abort() : exit(HATIMERUN_BADEXIT);
}

int
main(int argc, char **argv)
{
	int argoff;
	pid_t waitres;
	int rc;
	int saveerrno;
	int c;
	int hadtswitch = 0;
	char *ridx;

	myprogname = argv[0];
	if ((ridx = strrchr(myprogname, '/')) != NULL) {
		myprogname = ridx+1;
	}

	/* I18N */
	(void) setlocale(LC_ALL, "");
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);
	(void) textdomain(TEXT_DOMAIN);

	parentsidepid = getpid();

	if (argc < 3)
		usagehalt();
	opterr = 0;
	while ((c = getopt(argc, argv, "vak:e:t:")) != EOF) {
		switch (c) {
			case 'v':
				verbose = 1;
				break;
			case 'a':
				asynch = 1;
				break;
			case 'k':
				hadkswitch = 1;
				lower2upper(optarg);
				if (os::str2sig(optarg, &killsig) == -1) {
					ha_error(LOG_ERR, "hatimerun.1005",
						gettext(
						"Error: illegal signal option"
						" to -k switch '%s'\n"),
						optarg);
					usagehalt();
				}
				break;
			case 'e':
				if ((sscanf(optarg, "%d", &timeoutexitcode)
								!= 1) ||
					(timeoutexitcode <= 0) ||
					(timeoutexitcode >= 256)) {
					ha_error(LOG_ERR, "hatimerun.1006",
						gettext(
						"Error: -e argument must be "
						"a positive integer "
						"less than 256.\n"));
					usagehalt();
				}
				if (timeoutexitcode == badexit)
					badexit--;
				break;
			case 't':
				hadtswitch = 1;
				if ((sscanf(optarg, "%d", &timeoutsecs) != 1) ||
					(timeoutsecs <= 0)) {
					ha_error(LOG_ERR, "hatimerun.1007",
						gettext(
						"Error: -t timeOutSecs "
						"must be positive integer.\n"));
					usagehalt();
				}
				break;
			default:
				ha_error(LOG_ERR, "hatimerun.1008",
					gettext("Error: illegal switch '%c'\n"),
					optopt);
				usagehalt();
		} /* switch */
	} /* while */
	if (asynch && hadkswitch) {
		ha_error(LOG_ERR, "hatimerun.1009", gettext("Error: illegal to "
			"supply both -a and -k switches.\n"));
		usagehalt();
	}

	argoff = optind;
	if (argoff >= argc)
		usagehalt();
	if (! hadtswitch) {
		ha_error(LOG_ERR, "hatimerun.1010", gettext("Error: -t "
			"timeoutsecs switch is mandatory.\n"));
		usagehalt();
	}

	if (! hadtswitch) {
		if ((sscanf(argv[argoff], "%d", &timeoutsecs) != 1) ||
			(timeoutsecs <= 0)) {
			ha_error(LOG_ERR, "hatimerun.1011", gettext("Error: "
				"timeOutSecs must be positive integer.\n"));
			usagehalt();
		}
		argoff++;
		if (argoff >= argc)
			usagehalt();
	}
	if (verbose) {
		ha_error(LOG_INFO, "hatimerun.1012", gettext("sig=%d "
		    "timeoutexitcode=%d timeoutsecs=%d\n"),
		    killsig, timeoutexitcode, timeoutsecs);
	}

	/*
	 * Flush stdio before fork, to avoid having it appear twice,
	 * once from child and once from parent.
	 */
	(void) fflush(stdout);
	(void) fflush(stderr);
	if (sigset(SIGTERM, sighandler) == SIG_ERR) {
		ha_error(LOG_ERR, "hatimerun.1013", gettext("Error: sigset for "
			"SIGTERM failed, errno=%d '%s'\n"),
			errno, strerror(errno));
		exit(HATIMERUN_BADEXIT);
	}
	if (signal(SIGABRT, sighandler) == SIG_ERR) {
		ha_error(LOG_ERR, "hatimerun.1014", gettext("Error: sigset for "
			"SIGABRT failed, errno=%d '%s'\n"),
			errno, strerror(errno));
		exit(HATIMERUN_BADEXIT);
	}
	if (sighold(SIGTERM) == -1) {
		ha_error(LOG_ERR, "hatimerun.1015", gettext("Error: "
			"sighold for SIGTERM failed, errno=%d " "'%s'\n"),
			errno, strerror(errno));
		exit(HATIMERUN_BADEXIT);
	}
	if (sighold(SIGABRT) == -1) {
		ha_error(LOG_ERR, "hatimerun.1016", gettext("Error: "
			"sighold for SIGABRT failed, errno=%d " "'%s'\n"),
			errno, strerror(errno));
		exit(HATIMERUN_BADEXIT);
	}
	childpid = fork();
	if (childpid == -1) {
		ha_error(LOG_ERR, "hatimerun.1017", gettext("Error: cannot "
			"fork: errno=%d '%s'\n"), errno, strerror(errno));
		exit(HATIMERUN_BADEXIT);
	}
	if (sigrelse(SIGTERM) == -1) {
		ha_error(LOG_ERR, "hatimerun.1018", gettext("Error: "
			"sigrelse for SIGTERM failed, errno=%d '%s'\n"),
			errno, strerror(errno));
		(void) fflush(stderr);
		if (childpid == 0) /* in child process */
			exit(HATIMERUN_BADEXIT);
		exit(HATIMERUN_BADEXIT);
	}
	if (sigrelse(SIGABRT) == -1) {
		ha_error(LOG_ERR, "hatimerun.1019", gettext("Error: "
			"sigrelse for SIGABRT failed, errno=%d '%s'\n"),
			errno, strerror(errno));
		(void) fflush(stderr);
		if (childpid == 0) /* in child process */
			exit(HATIMERUN_BADEXIT);
		exit(HATIMERUN_BADEXIT);
	}

	if (childpid == 0) {
		/* child process */
		if (setpgid(0, 0) == -1) {
			ha_error(LOG_INFO, "hatimerun.1020", gettext(
				"Warning: child "
				"setpgid to self failed, errno=%d '%s'\n"),
				errno, strerror(errno));
			(void) fflush(stderr);
		}
		(void) execvp(argv[argoff], &argv[argoff]);
		/* Here only on error. */
		ha_error(LOG_ERR, "hatimerun.1021", gettext("Error: child "
			"couldn't execute program '%s', errno=%d '%s'\n"),
			argv[argoff], errno, strerror(errno));
		exit(HATIMERUN_BADEXIT);
	}

	/* In parent process for everything that follows. */
	if (sigset(SIGALRM, alrmhandler) == SIG_ERR) {
		ha_error(LOG_ERR, "hatimerun.1022", gettext("Error: sigset for "
			"SIGALRM failed, errno=%d '%s'\n"),
			errno, strerror(errno));
		exit(HATIMERUN_BADEXIT);
	}
	(void) alarm((uint_t)timeoutsecs);

	do {
		waitres = waitpid(childpid, &childstatus, 0);
	} while ((waitres == -1) && (errno == EINTR));

	cancelalarm = 1;
	saveerrno = errno;
	if (sigignore(SIGALRM) == -1) {
		ha_error(LOG_ERR, "hatimerun.1023", gettext("Error: sigignore "
			"for SIGALRM failed, errno=%d '%s'\n"),
			errno, strerror(errno));
		exit(HATIMERUN_BADEXIT);
	}
	errno = saveerrno;

	if (waitres == -1) {
		ha_error(LOG_ERR, "hatimerun.1024", gettext("Error: waitpid "
			"failed, errno=%d '%s'\n"), errno, strerror(errno));
		exit(HATIMERUN_BADEXIT);
	}

	if (waitres != childpid) {
		ha_error(LOG_ERR, "hatimerun.1025", gettext(
			"Error: waitpid botch: "
			"returned %d but waited for %d.\n"), waitres, childpid);
		exit(HATIMERUN_BADEXIT);
	}

	if (WIFEXITED(childstatus)) {
		rc = WEXITSTATUS(childstatus);
		exit(rc);
	} else if (WIFSIGNALED(childstatus)) {
		rc = WTERMSIG(childstatus) + SIGEXITBASE;
		exit(rc);
	} else {
		ha_error(LOG_ERR, "hatimerun.1026", gettext("Error: waitpid "
			"returned weird status 0x%X\n"), childstatus);
		exit(HATIMERUN_BADEXIT);
	}
} /* main */
