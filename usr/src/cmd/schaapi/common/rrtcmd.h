/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * Declarations common to both scha_resource_get and
 * scha_resourcetype_get
 */

#ifndef	_SCHARRTCMD_H
#define	_SCHARRTCMD_H

#pragma ident	"@(#)rrtcmd.h	1.20	08/05/20 SMI"

#ifdef __cplusplus
extern "C" {
#endif

/*
 * enum values for every optag that is to be handled by scha_resource_get.
 * string operation tags are translated to these codes to simplify
 * handling of each optag case.
 */
typedef enum rot_tag_t {
	ROT_NONE = 0,
	ROT_R_DESCRIPTION,
	ROT_TYPE,
	ROT_ON_OFF_SWITCH,
	ROT_ON_OFF_SWITCH_NODE,
	ROT_MONITORED_SWITCH,
	ROT_MONITORED_SWITCH_NODE,
	ROT_RESOURCE_PROJECT_NAME,
	ROT_TYPE_VERSION,
	ROT_RESOURCE_STATE,
	ROT_UPDATE_FAILED,
	ROT_INIT_FAILED,
	ROT_FINI_FAILED,
	ROT_BOOT_FAILED,
	ROT_CHEAP_PROBE_INTERVAL,
	ROT_THOROUGH_PROBE_INTERVAL,
	ROT_RETRY_COUNT,
	ROT_RETRY_INTERVAL,
	ROT_FAILOVER_MODE,
	ROT_RESOURCE_DEPENDENCIES,
	ROT_RESOURCE_DEPENDENCIES_WEAK,
	ROT_RESOURCE_DEPENDENCIES_RESTART,
	ROT_RESOURCE_DEPENDENCIES_OFFLINE_RESTART,
	ROT_NETWORK_RESOURCES_USED,
	ROT_SCALABLE,
	ROT_PORT_LIST,
	ROT_LOAD_BALANCING_POLICY,
	ROT_LOAD_BALANCING_WEIGHTS,
	ROT_AFFINITY_TIMEOUT,
	ROT_UDP_AFFINITY,
	ROT_WEAK_AFFINITY,
	ROT_GENERIC_AFFINITY,
	ROT_ROUND_ROBIN,
	ROT_CONN_THRESHOLD,
	ROT_STATUS,
	ROT_GENERATION_NUMBER,
	ROT_START_TIMEOUT,
	ROT_STOP_TIMEOUT,
	ROT_VALIDATE_TIMEOUT,
	ROT_UPDATE_TIMEOUT,
	ROT_INIT_TIMEOUT,
	ROT_FINI_TIMEOUT,
	ROT_BOOT_TIMEOUT,
	ROT_MONITOR_START_TIMEOUT,
	ROT_MONITOR_STOP_TIMEOUT,
	ROT_MONITOR_CHECK_TIMEOUT,
	ROT_PRENET_START_TIMEOUT,
	ROT_POSTNET_STOP_TIMEOUT,
	ROT_STATUS_NODE,
	ROT_RESOURCE_STATE_NODE,
	ROT_EXTENSION,
	ROT_EXTENSION_NODE,
	ROT_ALL_EXTENSIONS,
	ROT_GROUP,
	ROT_RT_DESCRIPTION,
	ROT_RT_BASEDIR,
	ROT_SINGLE_INSTANCE,
	ROT_INIT_NODES,
	ROT_INSTALLED_NODES,
	ROT_FAILOVER,
	ROT_PROXY,
	ROT_API_VERSION,
	ROT_RT_VERSION,
	ROT_PKGLIST,
	ROT_START,
	ROT_STOP,
	ROT_VALIDATE,
	ROT_UPDATE,
	ROT_INIT,
	ROT_FINI,
	ROT_BOOT,
	ROT_MONITOR_START,
	ROT_MONITOR_STOP,
	ROT_MONITOR_CHECK,
	ROT_PRENET_START,
	ROT_POSTNET_STOP,
	ROT_NUM_RESOURCE_RESTARTS,
	ROT_NUM_RG_RESTARTS,
	ROT_NUM_RESOURCE_RESTARTS_ZONE,
	ROT_NUM_RG_RESTARTS_ZONE,
	ROT_IS_LOGICAL_HOSTNAME,
	ROT_IS_SHARED_ADDRESS,
	ROT_RT_SYSTEM,
	ROT_RESOURCE_LIST,
	ROT_IS_PER_NODE,
	ROT_GLOBALZONE,
	ROT_GLOBAL_ZONE_OVERRIDE
} rot_tag_t;

/*
 * Enum codes for indicating what argument types may be needed by
 * the above operation tags.
 */
typedef enum rot_type_t {
	ROT_T_NONE = 0,
	ROT_T_STR,
	ROT_T_INT,
	ROT_T_ULL,	/* Type of Generation number: unsigned long long */
	ROT_T_BOOL,
	ROT_T_FOM,
	ROT_T_SWITCH,
	ROT_T_STATUS,
	ROT_T_STATE,
	ROT_T_INIT,
	ROT_T_STRAR,
	ROT_T_EXT
} rot_type_t;

typedef struct schaxrot_t {
	char			*schatag;
	rot_tag_t		rotcode;
	rot_type_t		rottype;
} schaxrot_t;

/*
 * Translation table for Resource Type operation tags
 * Common to both scha_resource_get and scha_resourcetype get
 */

static schaxrot_t schaxrtot_tab[] = {

{SCHA_RT_DESCRIPTION,		ROT_RT_DESCRIPTION,		ROT_T_STR},
{SCHA_RT_BASEDIR,		ROT_RT_BASEDIR,			ROT_T_STR},
{SCHA_SINGLE_INSTANCE,		ROT_SINGLE_INSTANCE,		ROT_T_BOOL},
{SCHA_INIT_NODES,		ROT_INIT_NODES,			ROT_T_INIT},
{SCHA_INSTALLED_NODES,		ROT_INSTALLED_NODES,		ROT_T_STRAR},
{SCHA_FAILOVER,			ROT_FAILOVER,			ROT_T_BOOL},
{SCHA_API_VERSION,		ROT_API_VERSION,		ROT_T_INT},
{SCHA_RT_VERSION,		ROT_RT_VERSION,			ROT_T_STR},
{SCHA_PKGLIST,			ROT_PKGLIST,			ROT_T_STRAR},

{SCHA_START,			ROT_START,			ROT_T_STR},
{SCHA_STOP,			ROT_STOP,			ROT_T_STR},
{SCHA_VALIDATE,			ROT_VALIDATE,			ROT_T_STR},
{SCHA_UPDATE,			ROT_UPDATE,			ROT_T_STR},
{SCHA_INIT,			ROT_INIT,			ROT_T_STR},
{SCHA_FINI,			ROT_FINI,			ROT_T_STR},
{SCHA_BOOT,			ROT_BOOT,			ROT_T_STR},
{SCHA_MONITOR_START,		ROT_MONITOR_START,		ROT_T_STR},
{SCHA_MONITOR_STOP,		ROT_MONITOR_STOP,		ROT_T_STR},
{SCHA_MONITOR_CHECK,		ROT_MONITOR_CHECK,		ROT_T_STR},
{SCHA_PRENET_START,		ROT_PRENET_START,		ROT_T_STR},
{SCHA_POSTNET_STOP,		ROT_POSTNET_STOP,		ROT_T_STR},

{SCHA_IS_LOGICAL_HOSTNAME,	ROT_IS_LOGICAL_HOSTNAME,	ROT_T_BOOL},
{SCHA_IS_SHARED_ADDRESS,	ROT_IS_SHARED_ADDRESS,		ROT_T_BOOL},
{SCHA_RT_SYSTEM,		ROT_RT_SYSTEM,			ROT_T_BOOL},
{SCHA_RESOURCE_LIST,		ROT_RESOURCE_LIST,		ROT_T_STRAR},
{SCHA_PER_NODE,			ROT_IS_PER_NODE,		ROT_T_BOOL},
{SCHA_PROXY,			ROT_PROXY,			ROT_T_BOOL},
{SCHA_GLOBALZONE,		ROT_GLOBALZONE,			ROT_T_BOOL},
{SCHA_GLOBAL_ZONE_OVERRIDE,	ROT_GLOBAL_ZONE_OVERRIDE,	ROT_T_BOOL},
{NULL,				ROT_NONE,			ROT_T_NONE}
};

#ifdef __cplusplus
}
#endif

#endif	/* _SCHARRTCMD_H */
