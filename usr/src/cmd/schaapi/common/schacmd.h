/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_SCHACMD_H
#define	_SCHACMD_H

#pragma ident	"@(#)schacmd.h	1.16	08/05/20 SMI"

#ifdef __cplusplus
extern "C" {
#endif

#include <stdlib.h>
#include <stdio.h>
#include <strings.h>
#define	_SCHA_NO_TRANSITION	/* turn-off transition symbols in scha.h */
#include <scha.h>
#include <rgm/scha_strings.h>


/*
 * Formats for printing return values to stdout
 */
#define	STRFMT		"%s\n"
#define	INTFMT		"%d\n"
#define	UINTFMT		"%u\n"
#define	ULLFMT		"%llu\n"
#define	TRUEFMT		"TRUE\n"
#define	FALSEFMT	"FALSE\n"
#define	EMPTYLINEFMT	"\n"

/*
 * Parse for any scha_* command
 * Multi-command arguments:
 *	-O optag
 *	-R resourc-name
 *	-G group-name
 *	-T type-name
 * Specific command arguments:
 *	-g genno	scha_control
 *	-s status	scha_resource_setstatus
 *	-m msg		scha_resource_setstatus
 *	-Z zone
 */


typedef struct Arguments {
	const char *Oarg;
	const char *Rarg;
	const char *Garg;
	const char *Targ;
	const char *garg;
	const char *sarg;
	const char *marg;
	const char *Zarg;
	boolean_t Qarg;
	int optind;
	int hadbadarg;
} Arguments;

static void
clearArguments(Arguments *arguments) {
	arguments->Oarg = NULL;
	arguments->Rarg = NULL;
	arguments->Garg = NULL;
	arguments->Targ = NULL;
	arguments->garg = NULL;
	arguments->sarg = NULL;
	arguments->marg = NULL;
	arguments->Zarg = NULL;
	arguments->Qarg = B_FALSE;
	arguments->optind = 0;
	arguments->hadbadarg = 0;
}

static void
getarguments_optstring
(int argc, char *const*argv, const char *optstring, Arguments *arguments)
{
	int c = 0;
	clearArguments(arguments);
	opterr = 0;	/* Turn off getop error messages to stderr */
	while ((c = getopt(argc, argv, optstring)) != EOF) {
		switch (c) {

		case 'O':
			if (arguments->Oarg != NULL) {
				arguments->hadbadarg = 1;
			}
			arguments->Oarg = optarg;
			break;
		case 'R':
			if (arguments->Rarg != NULL) {
				arguments->hadbadarg = 1;
			}
			arguments->Rarg = optarg;
			break;
		case 'G':
			if (arguments->Garg != NULL) {
				arguments->hadbadarg = 1;
			}
			arguments->Garg = optarg;
			break;
		case 'T':
			if (arguments->Targ != NULL) {
				arguments->hadbadarg = 1;
			}
			arguments->Targ = optarg;
			break;
		case 'g':
			if (arguments->garg != NULL) {
				arguments->hadbadarg = 1;
			}
			arguments->garg = optarg;
			break;
		case 's':
			if (arguments->sarg != NULL) {
				arguments->hadbadarg = 1;
			}
			arguments->sarg = optarg;
			break;
		case 'm':
			if (arguments->marg != NULL) {
				arguments->hadbadarg = 1;
			}
			arguments->marg = optarg;
			break;
		case 'Z':
			if (arguments->Zarg != NULL) {
				arguments->hadbadarg = 1;
			}
			arguments->Zarg = optarg;
			break;
		case 'Q':
			if (arguments->Qarg) {
				arguments->hadbadarg = 1;
			}
			arguments->Qarg = B_TRUE;
			break;
		case '?':
		case ':':
		default:
			arguments->hadbadarg = 1;

		}
	}
	arguments->optind = optind;
	return;

}



static void
getarguments
(int argc, char *const*argv, Arguments *arguments)
{
	getarguments_optstring(argc, argv, "O:R:G:T:g:s:m:Q", arguments);
}


/*
 * Check the return code of a libscha access function .
 * Re-map sequence id errors (SCHA_ERR_SEQID) to SCHA_ERR_NOERR, by
 * changing the in/out parameter 'errval' to SCHA_ERR_NOERR.  We do this
 * because it doesn't make sense for the scha API command line programs
 * to exit with SCHA_ERR_SEQID.
 *
 * The purpose of SCHA_ERR_SEQID is to inform the caller that she is not
 * getting a "snapshot"; that the data may have changed out from under
 * her.  For these programs, the interval over which the change out from
 * under is being observed is tiny (but the race still exists):  the open
 * and the get occur all within the program, which then exits back to the
 * calling shell script.  Thus, the calling shell script can never achieve
 * the snapshot semantics -- it has no "handle" by which it can observe
 * that the world is changing out from under it.  Necessarily, the shell
 * script is observing the world piecemeal, not as a snapshot.
 *
 * Given all that, the exit code SCHA_ERR_SEQID from these programs does
 * not provide any benefit to the calling shell script.  Worse, it
 * complicates the shell script code -- without the re-mapping of
 * SCHA_ERR_SEQID to SCHA_ERR_NOERR, that code must check for both exit
 * status of zero (success) and exit status of SCHA_ERR_SEQID and treat
 * both values as success.  This introduces a lot of cumbersome 'if'
 * testing into the script.  Worse, if the script were to neglect to do
 * that, if would fail mysteriously whenever SCHA_ERR_SEQID were returned
 * by one of these programs.
 */
static int
errval_ok(scha_err_t *errval)
{
	if (*errval == SCHA_ERR_NOERR) {
		return (1);
	}
	if (*errval == SCHA_ERR_SEQID) {
		*errval = SCHA_ERR_NOERR;
		return (1);
	}
	return (0);
}

/*
 * Print a string array structure to stdout
 */
static void
print_str_array(scha_str_array_t *strarp)
{
	uint_t ix;

	if (strarp->is_ALL_value) {
		(void) printf(STRFMT, "*");
	} else {
		/* handle degenerate zero-sized array */
		if ((strarp == NULL) || (strarp->array_cnt == 0))
			(void) printf(EMPTYLINEFMT);
		else {
			for (ix = 0; ix < strarp->array_cnt; ix++)
				(void) printf(STRFMT, strarp->str_array[ix]);
		}
	}

}

#ifdef __cplusplus
}
#endif

#endif	/* _SCHACMD_H */
