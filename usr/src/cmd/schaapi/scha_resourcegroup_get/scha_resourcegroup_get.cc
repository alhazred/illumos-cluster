/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */


#pragma ident	"@(#)scha_resourcegroup_get.cc	1.30	08/07/21 SMI"

#include <sys/os.h>
#include "schacmd.h"
#include <sys/cl_auth.h>
#include <sys/sol_version.h>
#if SOL_VERSION >= __s10
#include <zone.h>
#include <scadmin/scconf.h>
#include <libzccfg/libzccfg.h>
#endif

/*
 * enum values for every optag that is to be handled by scha_resourcegroup_get.
 * String operation tags are translated to these codes to simplify
 * handling of each optag case.
 */
typedef enum got_tag_t {
	GOT_NONE = 0,
	GOT_RG_DESCRIPTION,
	GOT_NODELIST,
	GOT_MAXIMUM_PRIMARIES,
	GOT_DESIRED_PRIMARIES,
	GOT_RG_PROJECT_NAME,
	/* SC SLM addon start */
	GOT_RG_SLM_TYPE,
	GOT_RG_SLM_PSET_TYPE,
	GOT_RG_SLM_CPU_SHARES,
	GOT_RG_SLM_PSET_MIN,
	/* SC SLM addon end */
	GOT_RG_AFFINITIES,
	GOT_RG_AUTO_START,
	GOT_FAILBACK,
	GOT_RESOURCE_LIST,
	GOT_RG_STATE,
	GOT_RG_DEPENDENCIES,
	GOT_GLOBAL_RESOURCES_USED,
	GOT_RG_MODE,
	GOT_IMPL_NET_DEPEND,
	GOT_PATHPREFIX,
	GOT_RG_STATE_NODE,
	GOT_RG_PINGPONG,
	GOT_RG_SYSTEM,
	GOT_RG_IS_FROZEN,
	GOT_RG_SUSP_AUTO_RECOVERY
} got_tag_t;

/*
 * Enum codes for indicating what argument types may be needed by
 * the above operation tags.
 * These are a subset of the types for the scha_resource_get
 * operation tags.
 */
typedef enum got_type_t {
	GOT_T_NONE = 0,
	GOT_T_STR,
	GOT_T_INT,
	GOT_T_BOOL,
	GOT_T_RGMODE,
	GOT_T_GSTAT,
	GOT_T_STRAR
} got_type_t;

typedef struct schaxgot_t {
	char			*schatag;
	got_tag_t		gotcode;
	got_type_t		gottype;
} schaxgot_t;

static schaxgot_t *xlate_optag(const char *intag);

int
main(int argc, char **argv)
{
	schaxgot_t *gotrecp	= NULL;
	scha_err_t retval	= SCHA_ERR_NOERR;
	scha_resourcegroup_t handle	= NULL;
	char *extra_arg		= "";
#if SOL_VERSION >= __s10
	uint_t zc_id			= 0;
	scconf_errno_t scconf_err	= SCCONF_NOERR;
	zone_cluster_state_t zc_state	= ZC_STATE_UNKNOWN;
	int zc_cfg_err			= ZC_OK;
#endif

	Arguments arguments;

	/* Initialize, demote to normal uid, check */
	/* authorization and then promote to euid=0 */
	cl_auth_init();
	cl_auth_demote();
	cl_auth_check_command_opt_exit(*argv, NULL,
	    CL_AUTH_RESOURCE_READ, SCHA_ERR_ACCESS);
	cl_auth_promote();

	getarguments_optstring(argc, argv, "G:O:Z:", &arguments);

	/*
	 * scha_resourcegroup_get, looking for "O","G" args
	 * Extra vararg argument may be needed.
	 */

	// SKV/... QARG?
	if (arguments.hadbadarg == 1 ||
		(arguments.Garg == NULL) ||
		(arguments.Oarg == NULL)) {

		return (SCHA_ERR_INVAL);
	}

	gotrecp = xlate_optag(arguments.Oarg);
	if (gotrecp == NULL || gotrecp->gotcode == GOT_NONE) {
		return (SCHA_ERR_TAG);
	}

	/*
	 * Extra vararg argument is needed only for
	 * SCHA_RG_STATE_NODE operation.
	 * Return SCHA_ERR_INVAL if an extra argument is provided that is
	 * not needed.
	 */
	if (arguments.optind < argc) {
		if (gotrecp->gotcode != GOT_RG_STATE_NODE)
			return (SCHA_ERR_INVAL);
		extra_arg = argv[arguments.optind];
	}

	if (arguments.Zarg) {
#if SOL_VERSION < __s10
		return (SCHA_ERR_INVAL);
#else
		zoneid_t zoneid = getzoneid();
		if (zoneid == GLOBAL_ZONEID) {
			/*
			 * We are in the global zone. We have to check whether
			 * the specified zone cluster is valid or not.
			 */
			scconf_err = scconf_get_zone_cluster_id(
							(char *)arguments.Zarg,
							&zc_id);
			if (scconf_err != SCCONF_NOERR) {
				/*
				 * The specified zone cluster is an invalid
				 * zone cluster
				 */
				return (SCHA_ERR_ZONE_CLUSTER);
			}

			/*
			 * This zone cluster is valid. We have to check whether
			 * it is in the "Running" state or not.
			 */
			zc_cfg_err = get_zone_cluster_state(arguments.Zarg,
								&zc_state);
			if (zc_cfg_err != ZC_OK) {
				/*
				 * We cannot determine the status. We will
				 * return an internal error.
				 */
				return (SCHA_ERR_INTERNAL);
			}

			if (zc_state < ZC_STATE_RUNNING) {
				/*
				 * The zone cluster is not in "Running" state.
				 */
				return (SCHA_ERR_ZC_DOWN);
			}
		}
#endif
	}

	retval = scha_resourcegroup_open_zone(arguments.Zarg,
	    arguments.Garg, &handle);

	if (retval != SCHA_ERR_NOERR) {
		return (retval);
	}
	if (handle == NULL) {
		return (SCHA_ERR_HANDLE);
	}

	switch (gotrecp->gottype) {

	default:
	case GOT_T_NONE:
		retval = SCHA_ERR_TAG;
		break;

	case GOT_T_STR:
	{
		/*
		 * String value results
		 * No extra vararg argument except out parameter
		 */
		char 		*strval = NULL;
		retval  = scha_resourcegroup_get_zone(arguments.Zarg, handle,
				gotrecp->schatag, &strval);
		if (errval_ok(&retval) && (strval != NULL)) {
			(void) printf(STRFMT, strval);
		}
		break;
	}
	case GOT_T_INT:
	{
		/*
		 * Int value results
		 * No extra vararg argument except out parameter
		 */
		int	intval = 0;
		retval  = scha_resourcegroup_get_zone(arguments.Zarg, handle,
		    gotrecp->schatag, &intval);
		if (errval_ok(&retval)) {
			(void) printf(INTFMT, intval);
		}
		break;
	}
	case GOT_T_BOOL:
	{
		/*
		 * boolean_t (declared in <sys/types.h> value results
		 * No extra vararg argument except out parameter
		 */
		boolean_t bval = B_FALSE;
		retval  = scha_resourcegroup_get_zone(arguments.Zarg, handle,
		    gotrecp->schatag, &bval);
		if (errval_ok(&retval)) {
			if (bval == B_TRUE) {
				(void) printf(TRUEFMT);
			} else {
				(void) printf(FALSEFMT);
			}
		}
		break;
	}
	case GOT_T_RGMODE:
	{
		/*
		 * scha_rgmode_t (declared in <scha_types.h>).
		 * No extra vararg argument except out parameter
		 */
		scha_rgmode_t 	rgmodeval = RGMODE_FAILOVER;
		char	*str = NULL;
		retval  = scha_resourcegroup_get_zone(arguments.Zarg, handle,
		    gotrecp->schatag, &rgmodeval);
		if (errval_ok(&retval) && (rgmodeval != 0)) {
			switch (rgmodeval) {
			default:
			case RGMODE_NONE:
				/*
				 * If the code falls in here
				 * there is something wrong.
				 */
				str = SCHA_NONE;
				retval = SCHA_ERR_INVAL;
				break;
			case RGMODE_FAILOVER:
				str = SCHA_FAILOVER;
				break;
			case RGMODE_SCALABLE:
				str = SCHA_SCALABLE;
				break;
			}
			(void) printf(STRFMT, str);
		}
		break;
	}
	case GOT_T_STRAR:
	{
		/*
		 * Pointer-to str_array_t (declared in <scha_types.h>)
		 * value results
		 * No extra vararg argument except out parameter
		 */
		scha_str_array_t *strarval = NULL;
		retval  = scha_resourcegroup_get_zone(arguments.Zarg,
		    handle, gotrecp->schatag,
		    &strarval);
		if (errval_ok(&retval) && (strarval != NULL)) {
			print_str_array(strarval);
		}
		break;
	}
	case GOT_T_GSTAT:
	{
		/*
		 * scha_rg_state_t (declared in <scha_types.h>) results
		 * Extra vararg argument needed for SCHA_RG_STATE_NODE
		 */
		scha_rgstate_t gstatval = SCHA_RGSTATE_UNMANAGED;
		if (gotrecp->gotcode == GOT_RG_STATE_NODE) {
			retval  = scha_resourcegroup_get_zone(arguments.Zarg,
			handle,
					gotrecp->schatag, extra_arg,
					&gstatval);
		} else {
			retval  = scha_resourcegroup_get_zone(arguments.Zarg,
			    handle,
					gotrecp->schatag, &gstatval);
		}
		if (errval_ok(&retval)) {
			/*
			 * Translate enums to strings defined
			 * in scha_strings.h
			 */
			char *str = SCHA_UNMANAGED;
			switch (gstatval) {
			default:
			case SCHA_RGSTATE_UNMANAGED:
				str = SCHA_UNMANAGED;
				break;
			case SCHA_RGSTATE_ONLINE:
				str = SCHA_ONLINE;
				break;
			case SCHA_RGSTATE_OFFLINE:
				str = SCHA_OFFLINE;
				break;
			case SCHA_RGSTATE_PENDING_ONLINE:
				str = SCHA_PENDING_ONLINE;
				break;
			case SCHA_RGSTATE_PENDING_ONLINE_BLOCKED:
				str = SCHA_PENDING_ONLINE_BLOCKED;
				break;
			case SCHA_RGSTATE_PENDING_OFFLINE:
				str = SCHA_PENDING_OFFLINE;
				break;
			case SCHA_RGSTATE_ERROR_STOP_FAILED:
				str = SCHA_ERROR_STOP_FAILED;
				break;
			case SCHA_RGSTATE_ONLINE_FAULTED:
				str = SCHA_ONLINE_FAULTED;
				break;
			}
			(void) printf(STRFMT, str);
		}
		break;
	}
	} /* end of switch (gotrecp->gottype) */

	/*
	 * Return value of close operation not checked.
	 * what is returned from the command is the status
	 * of the "get" function
	 */
	(void) scha_resourcegroup_close(handle);
	return (retval);
}

/*
 * Translation table for resource-group operation tags.
 */
static schaxgot_t schaxgot_tab[] = {
{SCHA_RG_DESCRIPTION,		GOT_RG_DESCRIPTION,	GOT_T_STR},
{SCHA_NODELIST,			GOT_NODELIST,		GOT_T_STRAR},
{SCHA_MAXIMUM_PRIMARIES,	GOT_MAXIMUM_PRIMARIES,	GOT_T_INT},
{SCHA_DESIRED_PRIMARIES,	GOT_DESIRED_PRIMARIES,	GOT_T_INT},
{SCHA_RG_PROJECT_NAME,		GOT_RG_PROJECT_NAME,	GOT_T_STR},
/* SC SLM addon start */
{SCHA_RG_SLM_TYPE,		GOT_RG_SLM_TYPE,	GOT_T_STR},
{SCHA_RG_SLM_PSET_TYPE,		GOT_RG_SLM_PSET_TYPE,	GOT_T_STR},
{SCHA_RG_SLM_CPU_SHARES,	GOT_RG_SLM_CPU_SHARES,	GOT_T_INT},
{SCHA_RG_SLM_PSET_MIN,		GOT_RG_SLM_PSET_MIN,	GOT_T_INT},
/* SC SLM addon end */
{SCHA_RG_AFFINITIES,		GOT_RG_AFFINITIES,	GOT_T_STRAR},
{SCHA_RG_AUTO_START,		GOT_RG_AUTO_START,	GOT_T_BOOL},
{SCHA_FAILBACK,			GOT_FAILBACK,		GOT_T_BOOL},
{SCHA_RESOURCE_LIST,		GOT_RESOURCE_LIST,	GOT_T_STRAR},
{SCHA_RG_STATE,			GOT_RG_STATE,		GOT_T_GSTAT},
{SCHA_RG_DEPENDENCIES,		GOT_RG_DEPENDENCIES,	GOT_T_STRAR},
{SCHA_GLOBAL_RESOURCES_USED,	GOT_GLOBAL_RESOURCES_USED, GOT_T_STRAR},
{SCHA_RG_MODE,			GOT_RG_MODE,		GOT_T_RGMODE},
{SCHA_IMPL_NET_DEPEND,		GOT_IMPL_NET_DEPEND,	GOT_T_BOOL},
{SCHA_PATHPREFIX,		GOT_PATHPREFIX,		GOT_T_STR},
{SCHA_RG_STATE_NODE,		GOT_RG_STATE_NODE,	GOT_T_GSTAT},
{SCHA_PINGPONG_INTERVAL,	GOT_RG_PINGPONG,	GOT_T_INT},
{SCHA_RG_SYSTEM,		GOT_RG_SYSTEM,		GOT_T_BOOL},
{SCHA_RG_IS_FROZEN,		GOT_RG_IS_FROZEN,	GOT_T_BOOL},
{SCHA_RG_SUSP_AUTO_RECOVERY,    GOT_RG_SUSP_AUTO_RECOVERY, GOT_T_BOOL},
{NULL,				GOT_NONE,		GOT_T_NONE}
};

static schaxgot_t *
xlate_optag(const char *intag)
{
	schaxgot_t *tabp = NULL;

	if (intag == NULL || *intag == 0) {
		return (NULL);
	}

	for (tabp = schaxgot_tab; tabp->schatag != NULL; tabp++) {
		if (strcasecmp(intag, tabp->schatag) == 0) {
			return (tabp);
		}
	}
	return (NULL);
}
