/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scha_resource_setstatus.cc	1.13	08/05/20 SMI"

#include <sys/os.h>
#include "schacmd.h"
#include <sys/cl_auth.h>

typedef struct cmdxscha_stat_t {
	char 		*schastr;
	scha_rsstatus_t	statcode;

} cmdxscha_stat_t;

static cmdxscha_stat_t *xlate_status(const char *);

int
main(int argc, char **argv)
{
	Arguments arguments;
	cmdxscha_stat_t *xstatp = NULL;
	const char *msg = "";
	scha_err_t retval	= SCHA_ERR_NOERR;

	/* Initialize, demote to normal uid, check */
	/* authorization and then promote to euid=0 */
	cl_auth_init();
	cl_auth_demote();
	cl_auth_check_command_opt_exit(*argv, NULL,
	    CL_AUTH_RESOURCE_ADMIN, SCHA_ERR_INTERNAL);
	cl_auth_promote();

	getarguments_optstring(argc, argv, "R:G:s:m:Z:", &arguments);

	/*
	 * scha_resource_setstatus, looking for "R","G","s", optional "m" & "Z"
	 * arguments.  No extra vararg parameters are expected so check
	 * that the optind has advanced to the total argument count in
	 * processing the flagged arguments
	 */
	if ((arguments.optind < argc) ||
		(arguments.hadbadarg  == 1) ||
		(arguments.Rarg == NULL) ||
		(arguments.Garg == NULL) ||
		(arguments.sarg == NULL)) {
		/* arguments.marg and arguments.Zarg may or may not be set */

		return (SCHA_ERR_INVAL);
	}

	xstatp = xlate_status(arguments.sarg);
	if (xstatp == NULL) {
		return (SCHA_ERR_INVAL);
	}
	if (arguments.marg != NULL) {
		msg = arguments.marg;
	}

	retval = scha_resource_setstatus_zone(arguments.Rarg, arguments.Garg,
	    arguments.Zarg, xstatp->statcode, msg);
	/* filter out SCHA_ERR_SEQID errors */
	(void) errval_ok(&retval);
	return (retval);
}

static cmdxscha_stat_t cmdxscha_stat_tab[] = {
{SCHA_OK,		SCHA_RSSTATUS_OK},
{SCHA_OFFLINE,		SCHA_RSSTATUS_OFFLINE},
{SCHA_FAULTED,		SCHA_RSSTATUS_FAULTED},
{SCHA_DEGRADED,		SCHA_RSSTATUS_DEGRADED},
{SCHA_UNKNOWN,		SCHA_RSSTATUS_UNKNOWN},
{NULL,			SCHA_RSSTATUS_UNKNOWN}
};

static cmdxscha_stat_t *
xlate_status(const char *instr)
{
	cmdxscha_stat_t *tabp = cmdxscha_stat_tab;

	if (instr == NULL || *instr == 0) {
		return (NULL);
	}
	for (; tabp->schastr != NULL; tabp++) {
		if (strcasecmp(instr, tabp->schastr) == 0) {
			return (tabp);
		}
	}
	return (NULL);
}
