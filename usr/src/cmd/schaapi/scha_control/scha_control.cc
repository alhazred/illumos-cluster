/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scha_control.cc	1.19	08/05/20 SMI"

#include <sys/os.h>
#include <schacmd.h>
#include <sys/cl_auth.h>

static const char *xlate_optag(const char *);

#ifdef	DEBUG
void
usage()
{
	(void) fprintf(stdout,
"usage: scha_control -R <r_name> -G <rg_name> -O { GIVEOVER | RESTART |\n"
"           RESOURCE_RESTART | CHECK_GIVEOVER | CHECK_RESTART |\n"
"           IGNORE_FAILED_START | RESOURCE_DISABLE |\n"
"           CHANGE_STATE_ONLINE | CHANGE_STATE_OFFLINE }\n");
}
#endif	/* DEBUG */

int
main(int argc, char **argv)
{
	Arguments arguments;
	const char *optag = NULL;
	scha_err_t retval	= SCHA_ERR_NOERR;

	/* Initialize, demote to normal uid, check authorization */
	/* and then promote to euid=0 */
	cl_auth_init();
	cl_auth_demote();
	cl_auth_check_command_opt_exit(*argv, NULL,
	    CL_AUTH_RESOURCE_ADMIN, SCHA_ERR_INTERNAL);
	cl_auth_promote();

	getarguments_optstring(argc, argv, "G:R:O:Z:", &arguments);
	/* scha_control only looking for "O","R","G" and optional "Z" args */
	if ((arguments.optind < argc) ||
		(arguments.hadbadarg  == 1) ||
		(arguments.Rarg == NULL) ||
		(arguments.Garg == NULL) ||
		(arguments.Oarg == NULL)) {
#ifdef	DEBUG
		usage();
#endif	/* DEBUG */

		return (SCHA_ERR_INVAL);
	}

#ifdef	DEBUG
	(void) fprintf(stderr, "optag = %s, Garg = %s, Rarg = %s Zarg = %s\n",
	    arguments.Oarg, arguments.Garg, arguments.Rarg,
	    arguments.Zarg ? arguments:Zarg : "null");
#endif	/* DEBUG */

	optag = xlate_optag(arguments.Oarg);
	if (optag == NULL) {
#ifdef	DEBUG
		usage();
#endif	/* DEBUG */
		return (SCHA_ERR_INVAL);
	}

	retval = (scha_control_zone(optag, arguments.Garg, arguments.Rarg,
	    arguments.Zarg));

	/* filter out SCHA_ERR_SEQID errors */
	(void) errval_ok(&retval);
	return (retval);
}

static const char *
xlate_optag(const char *tag)
{
	/*
	 * Should check optag string,
	 * make sure it's right for use by the
	 * scha_control function.
	 */
	if (strcasecmp(tag, SCHA_GIVEOVER) == 0) {
		return (SCHA_GIVEOVER);
	} else if (strcasecmp(tag, SCHA_RESTART) == 0) {
		return (SCHA_RESTART);
	} else if (strcasecmp(tag, SCHA_RESOURCE_RESTART) == 0) {
		return (SCHA_RESOURCE_RESTART);
	} else if (strcasecmp(tag, SCHA_RESOURCE_IS_RESTARTED) == 0) {
		return (SCHA_RESOURCE_IS_RESTARTED);
	} else if (strcasecmp(tag, SCHA_CHECK_GIVEOVER) == 0) {
		return (SCHA_CHECK_GIVEOVER);
	} else if (strcasecmp(tag, SCHA_CHECK_RESTART) == 0) {
		return (SCHA_CHECK_RESTART);
	} else if (strcasecmp(tag, SCHA_IGNORE_FAILED_START) == 0) {
		return (SCHA_IGNORE_FAILED_START);
	} else if (strcasecmp(tag, SCHA_RESOURCE_DISABLE) == 0) {
		return (SCHA_RESOURCE_DISABLE);
	} else if (strcasecmp(tag, SCHA_CHANGE_STATE_ONLINE) == 0) {
		return (SCHA_CHANGE_STATE_ONLINE);
	} else if (strcasecmp(tag, SCHA_CHANGE_STATE_OFFLINE) == 0) {
		return (SCHA_CHANGE_STATE_OFFLINE);
	} else {
		return ((const char *)NULL);
	}
}
