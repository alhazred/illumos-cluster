/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */


#pragma ident 	"@(#)halockrun.c	1.12	08/05/20 SMI"

/*
 * Started from SC2.2 source base, halockrun 1.6
 *
 * Usage: halockrun [-vsn] [-e exitcode] lockfile prog [args]
 *
 * Opens the lockfile and claims an exclusive mode lock on it.
 * Then runs the prog with args as subprocess.  When the
 * subprocess returns, we exit and, as a side-effect, the exit
 * releases the lock and closes the file.
 *
 * The motivation for this program is that it lets one
 * run another program under a mutex.  Often, this other
 * program is a shell script, which could not do its own
 * locking directly.
 *
 * The -v switch means verbose output, on stderr.
 *
 * The -s switch means a shared rather than an exclusive lock.
 *
 * The -n switch means non-blocking: attempt to acquire the
 * lock with a non-blocking request: if we fail to obtain the
 * lock then exit 1.
 *
 * Exits 1 for failures where prog did not run; that exit code is
 * overriden by the -e switch.
 * Exits 2 for failures where prog may have run.
 *
 * If the child finishes, then we exit with its exit code,
 * unless its exit code was 1, which we remap into our exit code 2
 * (because we've given 1 the special meaning above).
 *
 *
 */



#include <sys/os.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/syslog.h>
#include <fcntl.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <errno.h>
#include <assert.h>
#include <libintl.h>
#include <locale.h>


static char *lockfilename;
static char *myprogname = NULL;
static int lockedfiledescriptor;
static pid_t childpid;
static int childstatus;
static int verbose = 0;
static int didnotrunec = 99;
static int ranbutbadec = 98;
static int iswritelock = 1;
static int blocking = 1;

static void
usagehalt()
{
	(void) fprintf(stderr, gettext(
		"Usage: %s [-vsn] [-e exitcode] lockfilename prog [args]\n"),
		myprogname);
	exit(didnotrunec);
}

static void
ha_error(int flg, const char *msg, const char *fmt, ...)
{
	/*
	 * Simple SC 3.0 substitute - the SC 2.x ha_error() prints the
	 * message to stderr.  This ignores the flg (syslog level) and
	 * msg (message ID) args.
	 */

	va_list args;

	/*
	 * The following 2 lines are needed to pass lint.
	 */
	flg = flg;
	msg = msg;

	/*
	 * override FlexeLint error 40;
	 * our lint doesn't understand compiler
	 * builtin variable __builtin_va_alist
	 */
	va_start(args, fmt);	/*lint !e40 */
	(void) fprintf(stderr, "%s: ", myprogname);
	(void) vfprintf(stderr, fmt, args);
	va_end(args);
}

int
main(int argc, char **argv)
{
	int argoffset;  /* position of first non-switch argument */
	pid_t waitres;
	int rc;
	int c;
	flock_t flockarg;
	short ltype;
	char *ltypename;
	char *ridx;

	myprogname = argv[0];
	if ((ridx = strrchr(myprogname, '/')) != NULL) {
		myprogname = ridx+1;
	}

	/* I18N */
	(void) setlocale(LC_ALL, "");
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);
	(void) textdomain(TEXT_DOMAIN);

	if (argc < 3) usagehalt();

	opterr = 0;
	while ((c = getopt(argc, argv, "vdsne:")) != EOF) {
		switch (c) {
			case 'v':
				verbose = 1;
				break;
			case 's':
				iswritelock = 0;
				break;
			case 'n':
				blocking = 0;
				break;
			case 'e':
				if ((sscanf(optarg, "%d", &didnotrunec) != 1) ||
						(didnotrunec <= 0) ||
						(didnotrunec >= 256)) {
					ha_error(LOG_ERR, "halockrun.1001",
						gettext(
						"Error: -e argument must be "
						"a positive integer less "
						"than 256.\n"));
					usagehalt();
				}
				if (didnotrunec == ranbutbadec) {
					/*
					 * Change ranbutbadec to a value
					 * other than didnotrunec but which
					 * is also non-zero.
					 */
					ranbutbadec--;
					if (ranbutbadec == 0) {
						assert(didnotrunec == 1);
						ranbutbadec = didnotrunec + 1;
					}
				}
				break;
			default:
				ha_error(LOG_ERR, "halockrun.1002",
					gettext("Error: "
					"illegal switch '%c'\n"),  optopt);
			usagehalt();
		}
	}
	argoffset = optind;
	if (argoffset >= argc) usagehalt();
	lockfilename = argv[argoffset++];
	if (argoffset >= argc) usagehalt();

	lockedfiledescriptor = open(lockfilename, (O_RDWR | O_CREAT),
		0600);
	if (lockedfiledescriptor < 0) {
		/*
		 * errno.h is missing void arg, so suppress lint error.
		 * [should be errno(void) rather than errno()]
		 */
		ha_error(LOG_ERR, "halockrun.1003", gettext(
			"Error:  could not "
			"open/creat file '%s': errno=%d '%s'\n"),
			lockfilename, errno, strerror(errno));	/*lint !e746 */
		exit(didnotrunec);
	}

	if (iswritelock) {
		ltype = F_WRLCK;
		ltypename = "F_WRLCK";
	} else {
		ltype = F_RDLCK;
		ltypename = "F_RDLCK";
	}


	(void) memset(&flockarg, 0, sizeof (flockarg));
	flockarg.l_type = ltype;
	flockarg.l_whence = flockarg.l_start = flockarg.l_len = 0;
	if (fcntl(lockedfiledescriptor, (blocking ? F_SETLKW : F_SETLK),
		&flockarg) < 0) {
		int saveerrno = errno;

		if (saveerrno == EAGAIN) {
			if (verbose) {
				(void) memset(&flockarg, 0, sizeof (flockarg));
				flockarg.l_type = F_WRLCK;
				/* Fetch who holds the lock */
				if ((fcntl(lockedfiledescriptor,
						F_GETLK, &flockarg) != -1) &&
						(flockarg.l_type != F_UNLCK)) {
#ifndef linux
					ha_error(LOG_ERR, "halockrun.1002",
						gettext(
						"Warning: file '%s' may "
						"already be locked: l_type=%d,"
						" l_pid=%d, l_sysid=0x%X\n"),
						lockfilename,
						flockarg.l_type,
						flockarg.l_pid,
						flockarg.l_sysid);
#else
					/*
					 * l_sysid of struct flock_t does not
					 * exist in RHAS 3.
					 */
					ha_error(LOG_ERR, "halockrun.1002",
						gettext(
						"Warning: file '%s' may "
						"already be locked: l_type=%d,"
						" l_pid=%d\n"),
						lockfilename,
						flockarg.l_type,
						flockarg.l_pid);
#endif
					(void) fflush(stderr);
				} else {
					/*
					 * Could not fetch who holds it,
					 * but EAGAIN indicates pretty
					 * strongly that somebody does.
					 */
					ha_error(LOG_ERR, "halockrun.1005",
						gettext(
						"Warning: file '%s' may "
						"already be locked.\n"),
						lockfilename);
					(void) fflush(stderr);
				} /* if fcntl to fetch who holds the lock */
			} /* if verbose */
			if (! blocking) exit(1);
		} /* if EAGAIN */

		errno = saveerrno;

		ha_error(LOG_ERR, "halockrun.1006", gettext(
			"Error:  fcntl %s %s "
			"of file '%s' failed, errno=%d '%s'\n"),
			(blocking ? "F_SETLKW" : "F_SETLK"), ltypename,
			lockfilename, errno, strerror(errno));
		exit(didnotrunec);
	}


	switch (childpid = fork()) {
		case -1:
			/* Error in forking */
			ha_error(LOG_ERR, "halockrun.1007", gettext(
				"Error:  cannot "
				"fork: errno=%d '%s'\n"),
				errno, strerror(errno));
			exit(didnotrunec);
			break;

		case 0:
			/* child */
			(void) close(lockedfiledescriptor);
			(void) execvp(argv[argoffset], &argv[argoffset]);
			/* Here only on error. */
			ha_error(LOG_ERR, "halockrun.1008", gettext(
				"Error:  child "
				"could not execute program '%s', errno=%d "
				"'%s'\n"),
				argv[argoffset], errno, strerror(errno));
			exit(didnotrunec);
			break;

		default:
			/* parent, fall thru */
			break;
	} /* switch */

	do {
		waitres = waitpid(childpid, &childstatus, 0);
	} while ((waitres == -1) && (errno == EINTR));

	if (waitres == -1) {
		ha_error(LOG_ERR, "halockrun.1009", gettext("Error:  waitpid "
			"failed, errno=%d '%s'\n"), errno, strerror(errno));
		exit(ranbutbadec);
	}

	if (waitres != childpid) {
		ha_error(LOG_ERR, "halockrun.1010", gettext("Error:  waitpid "
			"botch: returned  %d but waited for %d.\n"), waitres,
			childpid);
		exit(ranbutbadec);
	}

	if (WIFEXITED(childstatus)) {
		rc = WEXITSTATUS((uint_t)childstatus);
		exit(rc);
	} else if (WIFSIGNALED(childstatus)) {
		rc = WTERMSIG(childstatus);
		/* Adding 128 to the signal is what some shells do. */
		exit(128+rc);
	} else {
		ha_error(LOG_ERR, "halockrun.1011", gettext("Error:  waitpid "
			"returned weird status 0x%X\n"), childstatus);
		exit(ranbutbadec);
	}

} /* main */
