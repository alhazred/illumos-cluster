/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scha_resource_get.cc	1.43	08/06/04 SMI"

#include <sys/os.h>
#include "schacmd.h"
#include "rrtcmd.h"
#include <sys/cl_auth.h>
#include <syslog.h>
#include <sys/sol_version.h>
#if SOL_VERSION >= __s10
#include <zone.h>
#include <scadmin/scconf.h>
#include <libzccfg/libzccfg.h>
#endif

static schaxrot_t *xlate_optag(const char *intag);

static boolean_t extra_arg_needed(const rot_type_t rottype,
    const rot_tag_t rotcode);
int
main(int argc, char **argv)
{
	schaxrot_t *rotrecp	= NULL;
	scha_err_t retval	= SCHA_ERR_NOERR;
	scha_resource_t	handle	= NULL;
	char *extra_arg		= "";
	char *extra_arg_ext	= "";
	char *Qtag		= "";
#if SOL_VERSION >= __s10
	uint_t zc_id			= 0;
	scconf_errno_t scconf_err	= SCCONF_NOERR;
	zone_cluster_state_t zc_state	= ZC_STATE_UNKNOWN;
	int zc_cfg_err			= ZC_OK;
#endif

	Arguments arguments;

	/* Initialize, demote to normal uid, check */
	/* authorization and then promote to euid=0 */
	cl_auth_init();
	cl_auth_demote();
	cl_auth_check_command_opt_exit(*argv, NULL,
	    CL_AUTH_RESOURCE_READ, SCHA_ERR_ACCESS);
	cl_auth_promote();

	getarguments_optstring(argc, argv, "G:R:O:Z:Q", &arguments);

	/*
	 * scha_resource_get, looking for "O","R", "G" and "Z" args
	 * The "G" argument is optional. scha_resource_open() should
	 * search for the correct group if the argument is NULL
	 */
	if (arguments.hadbadarg == 1 ||
		(arguments.Rarg == NULL) ||
		/* arguments.Garg -G argument is optional */
		(arguments.Oarg == NULL)) {

		return (SCHA_ERR_INVAL);
	}

	rotrecp = xlate_optag(arguments.Oarg);
	if (rotrecp == NULL || rotrecp->rotcode == ROT_NONE) {
		return (SCHA_ERR_TAG);
	}
	if ((arguments.Qarg &&
	    (rotrecp->rotcode != ROT_RESOURCE_DEPENDENCIES) &&
	    (rotrecp->rotcode != ROT_RESOURCE_DEPENDENCIES_WEAK) &&
	    (rotrecp->rotcode != ROT_RESOURCE_DEPENDENCIES_RESTART) &&
	    (rotrecp->rotcode != ROT_RESOURCE_DEPENDENCIES_OFFLINE_RESTART))) {
		return (SCHA_ERR_TAG);
	}
	if (arguments.Qarg) {
		if (rotrecp->rotcode == ROT_RESOURCE_DEPENDENCIES)
			Qtag = strdup(SCHA_RESOURCE_DEPENDENCIES_Q);
		else if (rotrecp->rotcode == ROT_RESOURCE_DEPENDENCIES_WEAK)
			Qtag = strdup(SCHA_RESOURCE_DEPENDENCIES_Q_WEAK);
		else if (rotrecp->rotcode == ROT_RESOURCE_DEPENDENCIES_RESTART)
			Qtag = strdup(SCHA_RESOURCE_DEPENDENCIES_Q_RESTART);
		else if (rotrecp->rotcode ==
		    ROT_RESOURCE_DEPENDENCIES_OFFLINE_RESTART)
			Qtag = strdup(
			    SCHA_RESOURCE_DEPENDENCIES_Q_OFFLINE_RESTART);
	}
	/*
	 * Get any extra vararg argument for operations that need one.
	 * Return SCHA_ERR_INVAL if an extra argument is provided that is
	 * not needed.
	 */
	if (arguments.optind < argc) {
		if (!extra_arg_needed(rotrecp->rottype, rotrecp->rotcode))
			return (SCHA_ERR_INVAL);
		extra_arg = argv[arguments.optind];
	}

	if (arguments.optind+1 < argc) {
		if (strcmp(rotrecp->schatag, SCHA_EXTENSION_NODE) != 0) {
			return (SCHA_ERR_INVAL);
		}
		extra_arg_ext = argv[arguments.optind + 1];
	}

	if ((strcmp(rotrecp->schatag, SCHA_EXTENSION_NODE) == 0) &&
	    (argc - arguments.optind) != 2) {
		return (SCHA_ERR_INVAL);
	}

	/*
	 * The -Z option can be used either from the global zone or from
	 * any zone cluster node. But, if it is used from a zone cluster node
	 * (say 'a') and a different zone cluster name is specified (say 'b'),
	 * we will be able to fetch the information from zone cluster 'b' only
	 * if a resource in 'a' has some dependency on a resource in 'b'.
	 * This check will be made by the rgmd for zone cluster 'b'.
	 */

	if (arguments.Zarg) {
#if SOL_VERSION < __s10
		return (SCHA_ERR_INVAL);
#else
		zoneid_t zoneid = getzoneid();
		if (zoneid == GLOBAL_ZONEID) {
			/*
			 * We are in the global zone. We have to check whether
			 * the specified zone cluster is valid or not.
			 */
			scconf_err = scconf_get_zone_cluster_id(
							(char *)arguments.Zarg,
							&zc_id);
			if (scconf_err != SCCONF_NOERR) {
				/*
				 * The specified zone cluster is an invalid
				 * zone cluster
				 */
				return (SCHA_ERR_ZONE_CLUSTER);
			}

			/*
			 * This zone cluster is valid. We have to check whether
			 * it is in the "Running" state or not.
			 */
			zc_cfg_err = get_zone_cluster_state(arguments.Zarg,
								&zc_state);
			if (zc_cfg_err != ZC_OK) {
				/*
				 * We cannot determine the status. We will
				 * return an internal error.
				 */
				return (SCHA_ERR_INTERNAL);
			}

			if (zc_state < ZC_STATE_RUNNING) {
				/*
				 * The zone cluster is not in "Running" state.
				 */
				return (SCHA_ERR_ZC_DOWN);
			}
		}
#endif
	}

	retval = scha_resource_open_zone(arguments.Zarg,
	    arguments.Rarg, arguments.Garg, &handle);

	if (retval != SCHA_ERR_NOERR) {
		return (retval);
	}
	if (handle == NULL) {
		return (SCHA_ERR_HANDLE);
	}


	switch (rotrecp->rottype) {

	default:
	case ROT_T_NONE:
		retval = SCHA_ERR_TAG;
		break;

	case ROT_T_STR:
	{
		/*
		 * String value results
		 * No extra vararg argument except out parameter
		 */
		char 		*strval = NULL;
		retval  = scha_resource_get_zone(arguments.Zarg,
		    handle, rotrecp->schatag, &strval);
		if (errval_ok(&retval) && (strval != NULL)) {
			(void) printf(STRFMT, strval);
		}
		break;
	}
	case ROT_T_INT:
	{
		/*
		 * Int value results
		 * No extra vararg argument except out parameter
		 */
		int	intval = 0;
		if (rotrecp->rotcode == ROT_NUM_RG_RESTARTS_ZONE ||
		    rotrecp->rotcode == ROT_NUM_RESOURCE_RESTARTS_ZONE) {
			retval  = scha_resource_get_zone(arguments.Zarg,
			    handle, rotrecp->schatag,
			    extra_arg, &intval);
		} else {
			retval  = scha_resource_get_zone(arguments.Zarg,
			    handle, rotrecp->schatag,
			    &intval);
		}

		if (errval_ok(&retval)) {
			(void) printf(INTFMT, intval);
		}
		break;
	}
	case ROT_T_ULL:
	{
		/*
		 * A resources's generation-number has an unsigned long long
		 * value.
		 * No extra vararg argument except out parameter
		 */
		unsigned long long ullval = 0;
		retval  = scha_resource_get_zone(arguments.Zarg, handle,
					rotrecp->schatag, &ullval);
		if (errval_ok(&retval)) {
			(void) printf(ULLFMT, ullval);
		}
		break;
	}
	case ROT_T_BOOL:
	{
		/*
		 * boolean_t (declared in <sys/types.h> value results
		 * No extra vararg argument except out parameter
		 */
		boolean_t bval = B_FALSE;
		retval  = scha_resource_get_zone(arguments.Zarg,
		    handle, rotrecp->schatag, &bval);
		if (errval_ok(&retval)) {
			if (bval == B_TRUE) {
				(void) printf(TRUEFMT);
			} else {
				(void) printf(FALSEFMT);
			}
		}
		break;
	}
	case ROT_T_STRAR:
	{
		/*
		 * Pointer to str_array_t (declared in scha_types.h)
		 * value results
		 * No extra vararg argument except out parameter
		 */
		scha_str_array_t *strarval = NULL;
		/*
		 * An additional flag -Q while querying the dependencies
		 * would imply that the result would be appendend with
		 * the qualifier as <res_name>{locality_type},<res_name>...
		 * The locality_type above would be either LOCAL_NODE or
		 * ANY_NODE.
		 */
		if (arguments.Qarg)
			retval = scha_resource_get_zone(arguments.Zarg,
			    handle, Qtag, &strarval);
		else
			retval  = scha_resource_get_zone(arguments.Zarg,
			    handle, rotrecp->schatag,
				&strarval);
		if (arguments.Qarg)
			free(Qtag);
		if (errval_ok(&retval) && (strarval != NULL)) {
			print_str_array(strarval);
		}
		break;
	}
	case ROT_T_INIT:
	{
		/*
		 * scha_initnodes_flag_t (declared in scha_types.h) results
		 * No extra vararg argument except out parameter
		 */
		scha_initnodes_flag_t ifval = SCHA_INFLAG_RG_PRIMARIES;
		retval  = scha_resource_get_zone(arguments.Zarg,
		    handle, rotrecp->schatag, &ifval);
		if (errval_ok(&retval)) {
			/*
			 * Translate enums to strings defined
			 * in scha_strings.h
			 */
			char *str = SCHA_RG_PRIMARIES;
			if (ifval == SCHA_INFLAG_RG_PRIMARIES) {
				str = SCHA_RG_PRIMARIES;
			} else if (ifval == SCHA_INFLAG_RT_INSTALLED_NODES) {
				str = SCHA_RT_INSTALLED_NODES;
			}
			(void) printf(STRFMT, str);
		}
		break;
	}
	case ROT_T_FOM:
	{
		/*
		 * scha_failover_mode_t (declared in scha_types.h) value results
		 * No extra vararg argument except out parameter
		 */
		scha_failover_mode_t fomval = SCHA_FOMODE_NONE;
		retval  = scha_resource_get_zone(arguments.Zarg,
		    handle, rotrecp->schatag, &fomval);
		if (errval_ok(&retval)) {
			/*
			 * Translate enums to strings defined
			 * in scha_strings.h
			 */
			char *str = SCHA_NONE;
			if (fomval == SCHA_FOMODE_HARD) {
				str = SCHA_HARD;
			} else if (fomval == SCHA_FOMODE_SOFT) {
				str = SCHA_SOFT;
			} else if (fomval == SCHA_FOMODE_RESTART_ONLY) {
				str = SCHA_RESTART_ONLY;
			} else if (fomval == SCHA_FOMODE_LOG_ONLY) {
				str = SCHA_LOG_ONLY;
			}
			(void) printf(STRFMT, str);
		}
		break;
	}
	case ROT_T_SWITCH:
	{
		/*
		 * scha_switch_t (declared in scha_types.h) value results
		 * Extra nodename vararg argument is needed for
		 * ROT_ON_OFF_SWITCH_NODE and ROT_MONITORED_SWITCH_NODE
		 * in addition to outparameter.
		 */
		scha_switch_t sval = SCHA_SWITCH_DISABLED;
		if (rotrecp->rotcode == ROT_ON_OFF_SWITCH_NODE ||
		    rotrecp->rotcode == ROT_MONITORED_SWITCH_NODE) {
			retval  = scha_resource_get_zone(arguments.Zarg,
			    handle, rotrecp->schatag,
			    extra_arg, &sval);
		} else {
			retval  = scha_resource_get_zone(arguments.Zarg,
			    handle, rotrecp->schatag,
								&sval);
		}
		if (errval_ok(&retval)) {
			/*
			 * Translate enums to strings defined
			 * in scha_strings.h
			 */
			char *str = SCHA_DISABLED;
			if (sval == SCHA_SWITCH_ENABLED) {
				str = SCHA_ENABLED;
			}
			(void) printf(STRFMT, str);
		}
		break;
	}
	case ROT_T_STATUS:
	{
		/*
		 * Pointer-to scha_status_value_t (declared in scha_types.h)
		 * value results
		 * Extra nodename vararg argument is needed for ROT_STATUS_NODE,
		 * in addition to outparamter
		 */

		scha_status_value_t *statval = NULL;
		if (rotrecp->rotcode == ROT_STATUS_NODE) {
			retval  = scha_resource_get_zone(arguments.Zarg,
			    handle, rotrecp->schatag,
					extra_arg, &statval);
		} else {
			retval  = scha_resource_get_zone(arguments.Zarg,
			    handle, rotrecp->schatag,
					&statval);
		}

		if (errval_ok(&retval) && (statval != NULL)) {
			/*
			 * Translate enums to strings defined
			 * in scha_strings.h
			 */
			char *str = SCHA_UNKNOWN;
			switch (statval->status) {
			case SCHA_RSSTATUS_OK:
				str = SCHA_OK;
				break;
			case SCHA_RSSTATUS_OFFLINE:
				str = SCHA_OFFLINE;
				break;
			case SCHA_RSSTATUS_FAULTED:
				str = SCHA_FAULTED;
				break;
			case SCHA_RSSTATUS_DEGRADED:
				str = SCHA_DEGRADED;
				break;
			case SCHA_RSSTATUS_UNKNOWN:
			default:
				str = SCHA_UNKNOWN;
			}
			(void) printf(STRFMT, str);
			(void) printf(STRFMT, statval->status_msg != NULL ?
			    statval->status_msg : "");
		}
		break;
	}
	case ROT_T_STATE:
	{
		/*
		 * scha_rsstate_t (declared in scha_types.h) value results
		 * Extra nodename vararg argument is needed for
		 * ROT_RESOURCE_STATE_NODE, in addition to outparamter
		 */

		scha_rsstate_t statval = SCHA_RSSTATE_OFFLINE;
		if (rotrecp->rotcode == ROT_RESOURCE_STATE_NODE) {
			retval  = scha_resource_get_zone(arguments.Zarg,
			    handle, rotrecp->schatag,
					extra_arg, &statval);
		} else {
			retval  = scha_resource_get_zone(arguments.Zarg,
			    handle, rotrecp->schatag,
					&statval);
		}
		if (errval_ok(&retval)) {
			/*
			 * Translate enums to strings defined
			 * in scha_strings.h
			 */
			char *str = SCHA_OFFLINE;

			switch (statval) {
			default:
			case SCHA_RSSTATE_OFFLINE:
				str = SCHA_OFFLINE;
				break;
			case SCHA_RSSTATE_ONLINE:
				str = SCHA_ONLINE;
				break;
			case SCHA_RSSTATE_START_FAILED:
				str = SCHA_START_FAILED;
				break;
			case SCHA_RSSTATE_STOP_FAILED:
				str = SCHA_STOP_FAILED;
				break;
			case SCHA_RSSTATE_MONITOR_FAILED:
				str = SCHA_MONITOR_FAILED;
				break;
			case SCHA_RSSTATE_ONLINE_NOT_MONITORED:
				str = SCHA_ONLINE_NOT_MONITORED;
				break;
			case SCHA_RSSTATE_STARTING:
				str = SCHA_STARTING;
				break;
			case SCHA_RSSTATE_STOPPING:
				str = SCHA_STOPPING;
				break;
			case SCHA_RSSTATE_DETACHED:
				str = SCHA_DETACHED;
				break;
			}
			(void) printf(STRFMT, str);
		}
		break;
	}
	case ROT_T_EXT:
	{
		/*
		 * Pointer-to scha_extprop_value_t (declared in scha_types.h)
		 * return value for extension property access.
		 * A property name vararg argument is needed
		 * as well as the out parameter.
		 */
		scha_extprop_value_t *extval = NULL;
		if (rotrecp->rotcode == ROT_EXTENSION_NODE) {
			retval  = scha_resource_get_zone(arguments.Zarg,
			    handle, rotrecp->schatag,
					extra_arg, extra_arg_ext, &extval);
		} else {
			retval = scha_resource_get_zone(arguments.Zarg,
			    handle, rotrecp->schatag,
					extra_arg, &extval);
		}
		if (errval_ok(&retval) && (extval != NULL)) {
			/*
			 * Translate type to string name
			 * and output value in type-dependent format
			 */
			switch (extval->prop_type) {
			case SCHA_PTYPE_STRING:
			case SCHA_PTYPE_ENUM:
				if (extval->prop_type == SCHA_PTYPE_ENUM &&
					extval->val.val_enum) {
					(void) printf(STRFMT, SCHA_ENUM);
					(void) printf(STRFMT,
						extval->val.val_enum);
				} else
				if (extval->prop_type == SCHA_PTYPE_STRING &&
					extval->val.val_str) {
					(void) printf(STRFMT, SCHA_STRING);
					(void) printf(STRFMT,
						extval->val.val_str);
				} else
					(void) printf(EMPTYLINEFMT);
				break;
			case SCHA_PTYPE_INT:
				(void) printf(STRFMT, SCHA_INT);
				(void) printf(INTFMT, extval->val.val_int);
				break;
			case SCHA_PTYPE_BOOLEAN:
				(void) printf(STRFMT, SCHA_BOOLEAN);
				if (extval->val.val_boolean == B_TRUE) {
					(void) printf(TRUEFMT);
				} else {
					(void) printf(FALSEFMT);
				}
				break;
			case SCHA_PTYPE_STRINGARRAY:
			{
				scha_str_array_t *strarp = NULL;

				(void) printf(STRFMT, SCHA_STRINGARRAY);
				strarp = extval->val.val_strarray;
				if (strarp == NULL) {
					(void) printf(EMPTYLINEFMT);
				} else
					print_str_array(strarp);
				break;
			}
			case SCHA_PTYPE_UINTARRAY:
			case SCHA_PTYPE_UINT:
			default:
				(void) printf(EMPTYLINEFMT);
				break;
			}
		}
		break;
	}
	} /* end of switch (rotrecp->rottype) */

	/*
	 * Return value of close operation not checked.
	 * what is returned from the command is the status
	 * of the "get" function
	 */

	(void) scha_resource_close(handle);
	return (retval);
}

static schaxrot_t schaxrot_tab[] = {
{SCHA_R_DESCRIPTION,		ROT_R_DESCRIPTION,		ROT_T_STR},
{SCHA_TYPE,			ROT_TYPE,			ROT_T_STR},
{SCHA_RESOURCE_PROJECT_NAME,	ROT_RESOURCE_PROJECT_NAME,	ROT_T_STR},
{SCHA_TYPE_VERSION,		ROT_TYPE_VERSION,		ROT_T_STR},
{SCHA_ON_OFF_SWITCH,		ROT_ON_OFF_SWITCH,		ROT_T_SWITCH},
{SCHA_ON_OFF_SWITCH_NODE,	ROT_ON_OFF_SWITCH_NODE,		ROT_T_SWITCH},
{SCHA_MONITORED_SWITCH,		ROT_MONITORED_SWITCH,		ROT_T_SWITCH},
{SCHA_MONITORED_SWITCH_NODE,	ROT_MONITORED_SWITCH_NODE,	ROT_T_SWITCH},
{SCHA_RESOURCE_STATE,		ROT_RESOURCE_STATE,		ROT_T_STATE},
{SCHA_UPDATE_FAILED,		ROT_UPDATE_FAILED,		ROT_T_BOOL},
{SCHA_INIT_FAILED,		ROT_INIT_FAILED,		ROT_T_BOOL},
{SCHA_FINI_FAILED,		ROT_FINI_FAILED,		ROT_T_BOOL},
{SCHA_BOOT_FAILED,		ROT_BOOT_FAILED,		ROT_T_BOOL},
{SCHA_CHEAP_PROBE_INTERVAL,	ROT_CHEAP_PROBE_INTERVAL,	ROT_T_INT},
{SCHA_THOROUGH_PROBE_INTERVAL,	ROT_THOROUGH_PROBE_INTERVAL,	ROT_T_INT},
{SCHA_RETRY_COUNT,		ROT_RETRY_COUNT,		ROT_T_INT},
{SCHA_RETRY_INTERVAL,		ROT_RETRY_INTERVAL,		ROT_T_INT},
{SCHA_FAILOVER_MODE,		ROT_FAILOVER_MODE,		ROT_T_FOM},
{SCHA_RESOURCE_DEPENDENCIES,	ROT_RESOURCE_DEPENDENCIES,	ROT_T_STRAR},
{SCHA_RESOURCE_DEPENDENCIES_WEAK,
				ROT_RESOURCE_DEPENDENCIES_WEAK,	ROT_T_STRAR},
{SCHA_RESOURCE_DEPENDENCIES_RESTART,
				ROT_RESOURCE_DEPENDENCIES_RESTART,
								ROT_T_STRAR},
{SCHA_RESOURCE_DEPENDENCIES_OFFLINE_RESTART,
				ROT_RESOURCE_DEPENDENCIES_OFFLINE_RESTART,
								ROT_T_STRAR},
{SCHA_NETWORK_RESOURCES_USED,	ROT_NETWORK_RESOURCES_USED,	ROT_T_STRAR},
{SCHA_SCALABLE,			ROT_SCALABLE,			ROT_T_BOOL},
{SCHA_PORT_LIST,		ROT_PORT_LIST,			ROT_T_STRAR},
{SCHA_LOAD_BALANCING_POLICY,	ROT_LOAD_BALANCING_POLICY,	ROT_T_STR},
{SCHA_LOAD_BALANCING_WEIGHTS,	ROT_LOAD_BALANCING_WEIGHTS,	ROT_T_STRAR},
{SCHA_AFFINITY_TIMEOUT,		ROT_AFFINITY_TIMEOUT,		ROT_T_INT},
{SCHA_UDP_AFFINITY,		ROT_UDP_AFFINITY,		ROT_T_BOOL},
{SCHA_WEAK_AFFINITY,		ROT_WEAK_AFFINITY,		ROT_T_BOOL},
{SCHA_GENERIC_AFFINITY,		ROT_GENERIC_AFFINITY,		ROT_T_BOOL},
{SCHA_ROUND_ROBIN,		ROT_ROUND_ROBIN,		ROT_T_BOOL},
{SCHA_CONN_THRESHOLD,		ROT_CONN_THRESHOLD,		ROT_T_INT},
{SCHA_STATUS,			ROT_STATUS,			ROT_T_STATUS},

{SCHA_START_TIMEOUT,		ROT_START_TIMEOUT,		ROT_T_INT},
{SCHA_STOP_TIMEOUT,		ROT_STOP_TIMEOUT,		ROT_T_INT},
{SCHA_VALIDATE_TIMEOUT,		ROT_VALIDATE_TIMEOUT,		ROT_T_INT},
{SCHA_UPDATE_TIMEOUT,		ROT_UPDATE_TIMEOUT,		ROT_T_INT},
{SCHA_INIT_TIMEOUT,		ROT_INIT_TIMEOUT,		ROT_T_INT},
{SCHA_FINI_TIMEOUT,		ROT_FINI_TIMEOUT,		ROT_T_INT},
{SCHA_BOOT_TIMEOUT,		ROT_BOOT_TIMEOUT,		ROT_T_INT},
{SCHA_MONITOR_START_TIMEOUT,	ROT_MONITOR_START_TIMEOUT,	ROT_T_INT},
{SCHA_MONITOR_STOP_TIMEOUT,	ROT_MONITOR_STOP_TIMEOUT,	ROT_T_INT},
{SCHA_MONITOR_CHECK_TIMEOUT,	ROT_MONITOR_CHECK_TIMEOUT,	ROT_T_INT},
{SCHA_PRENET_START_TIMEOUT,	ROT_PRENET_START_TIMEOUT,	ROT_T_INT},
{SCHA_POSTNET_STOP_TIMEOUT,	ROT_POSTNET_STOP_TIMEOUT,	ROT_T_INT},

{SCHA_STATUS_NODE,		ROT_STATUS_NODE,		ROT_T_STATUS},
{SCHA_RESOURCE_STATE_NODE,	ROT_RESOURCE_STATE_NODE,	ROT_T_STATE},
{SCHA_EXTENSION,		ROT_EXTENSION,			ROT_T_EXT},
{SCHA_EXTENSION_NODE,		ROT_EXTENSION_NODE,		ROT_T_EXT},
{SCHA_ALL_EXTENSIONS,		ROT_ALL_EXTENSIONS,		ROT_T_STRAR},
{SCHA_GROUP,			ROT_GROUP,			ROT_T_STR},
{SCHA_NUM_RESOURCE_RESTARTS,	ROT_NUM_RESOURCE_RESTARTS,	ROT_T_INT},
{SCHA_NUM_RG_RESTARTS,		ROT_NUM_RG_RESTARTS,		ROT_T_INT},
{SCHA_NUM_RESOURCE_RESTARTS_ZONE,
				ROT_NUM_RESOURCE_RESTARTS_ZONE, ROT_T_INT},
{SCHA_NUM_RG_RESTARTS_ZONE,	ROT_NUM_RG_RESTARTS_ZONE,	ROT_T_INT},
{SCHA_GLOBAL_ZONE_OVERRIDE,	ROT_GLOBAL_ZONE_OVERRIDE,	ROT_T_BOOL},
{NULL,				ROT_NONE,			ROT_T_NONE}
};

static schaxrot_t *
xlate_optag(const char *intag)
{
	schaxrot_t *tabp = NULL;

	if (intag == NULL || *intag == 0) {
		return (NULL);
	}

	for (tabp = schaxrot_tab; tabp->schatag != NULL; tabp++) {
		if (strcasecmp(intag, tabp->schatag) == 0) {
			return (tabp);
		}
	}
	/*
	 * If not found in the table or resource operations,
	 * check in resource type table
	 */
	for (tabp = schaxrtot_tab; tabp->schatag != NULL; tabp++) {
		if (strcasecmp(intag, tabp->schatag) == 0) {
			return (tabp);
		}
	}
	return (NULL);
}


/*
 * Return B_TRUE if an extra command line argument is needed for the
 * given type and tag.  Return B_FALSE otherwise.
 */
static boolean_t extra_arg_needed(const rot_type_t rottype,
	const rot_tag_t rotcode)
{
	if (rottype == ROT_T_EXT ||
	    rotcode == ROT_STATUS_NODE ||
	    rotcode == ROT_RESOURCE_STATE_NODE ||
	    rotcode == ROT_ON_OFF_SWITCH_NODE ||
	    rotcode == ROT_MONITORED_SWITCH_NODE ||
	    rotcode == ROT_NUM_RG_RESTARTS_ZONE ||
	    rotcode == ROT_NUM_RESOURCE_RESTARTS_ZONE) {
		return (B_TRUE);
	} else {
		return (B_FALSE);
	}
}
