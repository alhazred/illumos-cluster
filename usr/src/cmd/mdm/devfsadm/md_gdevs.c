/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)md_gdevs.c	1.5	08/05/20 SMI"

/*
 * Helper user program to make global device links for newly created
 * metadevices. It assumes that the diskset already exists, and that
 * the CCR for the device group is already updated. This is called
 * via clexecd on every node when a new metadisk is added anywhere in
 * the cluster.
 */

#include <meta.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <sys/sol_version.h>
#define	CLUSTER_LIBRARY_SOURCE
#include <sdssc.h>
#include <metacl.h>
#include <sys/mkdev.h>

#if SOL_VERSION > __s10
mdc_errno_t sdssc_makegdev(int set, int disk, dev_t dev, char *device_name);
#else
mdc_errno_t sdssc_makegdev(int set, int disk, dev_t dev);
#endif

void
main(int argc, char *argv[])
{
	int set_number;
	int disk_number;
	major_t major;
	minor_t minor;
	dev_t device;
	int err = 0;

#if SOL_VERSION > __s10
	if (argc != 4) {
		printf("usage: %s major minor device_name\n", argv[0]);
		exit(1);
	}
#else
	if (argc != 3) {
		printf("usage: %s major minor\n", argv[0]);
		exit(1);
	}
#endif
	major = (major_t)atoi(argv[1]);
	minor = (minor_t)atoi(argv[2]);
	device = makedev(major, minor);
	set_number = minor / SDSSC_METADDEV_MAX;
	disk_number = minor % SDSSC_METADDEV_MAX;
#if SOL_VERSION > __s10
	err = sdssc_makegdev(set_number, disk_number, device, argv[3]);
#else
	err = sdssc_makegdev(set_number, disk_number, device);
#endif
	if (err) {
		exit(err);
	}
	exit(0);
}
