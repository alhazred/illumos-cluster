/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scmd_link.c	1.8	08/05/20 SMI"

/*
 * SUNW_scmd_link.so
 * Link generator plug in for devfsadm to make shared metdisk devices
 * on all nodes of a suncluster
 */

#include <regex.h>
#include <sys/sol_version.h>
#include <dc/libdcs/libdcs.h>
#include <devfsadm.h>
#include <stdio.h>
#include <strings.h>
#include <stdlib.h>
#include <limits.h>
#include <sys/mkdev.h>
#include <syslog.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#if SOL_VERSION > __s10
#include <meta.h>
#include <sdssc.h>
#endif

#if SOL_VERSION > __s10
#define	MD_LINK_RE_SHARED	"^md/shared/[0-9]+/r?dsk/.+$"
#else
#define	MD_LINK_RE_SHARED	"^md/shared/[0-9]+/r?dsk/d[0-9]+$"
#endif

/* This flag is used to test whether dcs_initialized() has been called */
static int dcs_initialized = 0;

/*
 * This function lives in /usr/cluster/lib/sc/libsds_sc. Header for this is
 * part of Solaris for historical reasons. This function is placed there so
 * that as much of SunCluster's knowledge of SDS internals is concentrated
 * in one place.
 */
#if SOL_VERSION > __s10
void sdssc_create_device_nodes(dev_t dev, int set, char *device_name);
#else
void sdssc_create_device_nodes(dev_t dev, int set);
#endif

/*
 * The devfsadm link module require the next section to
 * be defined in order to know what and when to call functions
 * in the module on device creation and removal.
 */

/* setup for device creation */

static int scmd_create(di_minor_t minor, di_node_t node);

static devfsadm_create_t scmd_cbt[] = {
	{ "pseudo", "ddi_pseudo", "md",
	    TYPE_EXACT | DRV_EXACT, ILEVEL_1, scmd_create,
	},
};

DEVFSADM_CREATE_INIT_V0(scmd_cbt);

/*
 * remove devices
 */
static int scmd_remove(char *devname);

/*
 * Block removal of devices that are registered with the CCR. Do not
 * block remove the local disk set (set 0) or the admin device.
 */
static devfsadm_remove_V1_t scmd_remove_cbt[] = {
	{"pseudo", MD_LINK_RE_SHARED, RM_ALWAYS | RM_PRE | RM_HOT,
	    ILEVEL_1, scmd_remove},
};

DEVFSADM_REMOVE_INIT_V1(scmd_remove_cbt);

/*
 * Minor init is called by devfsadm when this module is loaded. We initialise
 * the dcs library here. This will initialise the ORB, which may cause fork()
 * problems. This module should not fail to load even if not being in cluster
 * mode.
 */

int
minor_init(void)
{
	if (dcs_initialize() == 0) {
		dcs_initialized = 1;
	}

#if SOL_VERSION > __s10
	/*
	 * Initialize sdssc entry points. Don't worry about the return
	 * value here since the interface functions will get initialized
	 * correctly regardless.
	 */
	(void) sdssc_bind_library();
#endif
	return (DEVFSADM_SUCCESS);
}

/*
 * Minor fini is called by devfsadm before unloading the module.
 */
int
minor_fini(void)
{
	dcs_shutdown();
	return (DEVFSADM_SUCCESS);
}

/*
 * SunCluster does not globalize the admin device or the non shared metasets.
 *
 * For each other device
 *	/dev/md/shared/M/dsk/dN /devices/pseudo/md@0:M,N,blk
 *	/dev/md/shared/M/rdsk/dN /devices/pseudo/md@0:M,N,raw
 */
static int
scmd_create(di_minor_t minor, di_node_t node)
{
	char		mn[MAXNAMELEN + 1];
	int		set = -1, mdev = -1, ret;
	dev_t		dev;
#if SOL_VERSION > __s10
	char		*device_name;
	int		key;
	md_error_t	ep;
#endif

	(void) strcpy(mn, di_minor_name(minor));

	/*
	 * First, check if DCS is already initialized; if not,
	 * do that now and if we fail, return DEVFSADM_CONTINUE.
	 */
	if (!dcs_initialized) {
		if (dcs_initialize() != 0) {
			return (DEVFSADM_CONTINUE);
		}
		dcs_initialized++;
	}

	/*
	 * Call DCS to record the metadisk in the CCR and make links
	 * for all cluster nodes. SunCluster only uses metasets other than
	 * set 0 for global devices
	 */

	if (!(strcmp(mn, "admin") == 0)) {
		/*
		 * Extract out the minor components and create the
		 * appropriate links. The node looks like:
		 * md@<set>,<mdev>,<type>
		 * where the <set> number is the named diskset,
		 * <mdev> is the metadevice number, and <type>
		 * is the trailing "blk" or "raw" indication.
		 *
		 * NOTE: when <set> is non-zero, the set is a "shared"
		 * metaset and we must make global device links for it.
		 */
		ret = sscanf(mn, "%d,%d,", &set, &mdev);
		if (ret == 2 && set != 0) {
			dev = di_minor_devt(minor);
#if SOL_VERSION > __s10
			(void) memset(&ep, '\0', sizeof (ep));
			if ((device_name = meta_getnmentbydev(set,
			    MD_SIDEWILD, dev, NULL, NULL,
			    &key, &ep)) == NULL) {
				(void) close_admin(&ep);
				return (DEVFSADM_CONTINUE);
			}
			sdssc_create_device_nodes(dev, set,
							basename(device_name));
			Free(device_name);
#else /* !SOL_VERSION > __s10 */
			sdssc_create_device_nodes(dev, set);
#endif /* SOL_VERSION > __s10 */
		}
	}
	return (DEVFSADM_CONTINUE);
}

/*
 * Callback to remove shared diskset device links.
 * Strategy is to only remove links for disksets that are not
 * registered with the DCS. If we are not in cluster mode, or
 * get an error inquiring if this diskset is registered with
 * DCS, then the device link is not removed.
 */
static int
scmd_remove(char *devname) {
	dc_error_t	dc_error = 0;
	struct stat	statbuf;
	char		*service_name = NULL;
	char		path[MAXPATHLEN];
	int		error;

	/*
	 * If DCS is not initialized, then we should not remove any
	 * shared diskset nodes as we can not tell if the diskset
	 * may exist as a device group. Note that we only register for
	 * shared disksets, so this code does not check for shared.
	 */
	if (!dcs_initialized) {
		return (DEVFSADM_TERMINATE);
	}

	(void) snprintf(path, sizeof (path), "/dev/%s", devname);
	error = stat(path, &statbuf);

	if (error == -1) {
		/*
		 * if either the link doesn't exist, or the device
		 * to which the link points, just do the default remove.
		 */
		if (errno == ENOENT) {
			return (DEVFSADM_CONTINUE);
		}
		devfsadm_errprint("SUNW_scmd_link: stat failed for %s: %s\n",
		    path, strerror(errno));
		/*
		 * Without a good stat, we can't tell if the cluster is
		 * using the link.
		 */
		return (DEVFSADM_TERMINATE);
	}
	/*
	 * We don't need the actual service name. We just need to know
	 * if it exists.
	 */
	dc_error = dcs_get_service_name_by_dev_t(major(statbuf.st_rdev),
	    minor(statbuf.st_rdev), &service_name);

	if (dc_error) {
		if (dc_error != DCS_ERR_DEVICE_INVAL) {
			/*
			 * There is a problem with DCS. We do not know if
			 * the cluster is still using this device.
			 * Do not remove to be safe.
			 */
			devfsadm_errprint("SUNW_scmd_link: "
			    "dcs_get_service_name_by_dev_t failed: %s",
			    dcs_error_to_string(dc_error));
			if (service_name != NULL) {
				free(service_name);
			}
			return (DEVFSADM_TERMINATE);
		}

		/* The device is not known to the cluster. Ok to remove. */
		if (service_name != NULL) {
			free(service_name);
		}
		return (DEVFSADM_CONTINUE);
	}

	/* No error. The device is still known by the cluster do not remove */
	if (service_name != NULL) {
		free(service_name);
	}
	return (DEVFSADM_TERMINATE);
}
