#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#ident	"@(#)Makefile.com	1.7	08/05/20 SMI"
#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
# cmd/mdm/devfsadm/Makefile.com
#

PROG		= SUNWmd_gdevs
SRCS		= $(PROG:%=%.c)
OBJECTS		= $(PROG:%=%.o)
LINTFILES	= $(PROG:%=%.ln)
PIFILES		= $(SRCS:%.c=%.pi)
POFILE		= mdm.po

include $(SRC)/cmd/Makefile.cmd

LIBS = -Bstatic -lgen -Bdynamic -ladm -lnsl -ldevid

#
# Compiler flags
#
CPPFLAGS += -I$(SRC)/common/cl -I$(REF_PROTO)/usr/src/head
CPPFLAGS += -I$(REF_PROTO)/usr/src/cmd/devfsadm

CLOBBERFILES += $(PROG:%=%.o)

.KEEP_STATE:

COMMON = ..

PROG_SRC = $(COMMON)/$(%=%.c)
PROG_OBJ = md_gdevs.o

LINKMOD_DIR = linkmod
DEVFSADM_DIR = devfsadm

CLOBBERFILES = $(MODS)

LINK_SRCS = $(COMMON)/scmd_link.c

LINT_MODULES = $(LINK_SRCS:.c=.ln) $(COMMON)/md_gdevs.ln

LINK_OBJS = scmd_link.o
PROG_OBJS = md_gdevs.o

LINK_MODS = SUNW_scmd_link.so

CPPFLAGS += -D_POSIX_PTHREAD_SEMANTICS -D_REENTRANT
CFLAGS += -v -I.. -I$(MODLOADDIR)

LINTFLAGS += -m -I.. -I$(MODLOADDIR)
LINTFLAGS += -v
LINTFLAGS += -erroff=E_NAME_USED_NOT_DEF2
LINTFLAGS += -erroff=E_NAME_DEF_NOT_USED2
LINTFLAGS += -erroff=E_NAME_MULTIPLY_DEF2
LINTFLAGS += -erroff=E_FUNC_DECL_VAR_ARG2
LINTFLAGS += -erroff=E_FUNC_USED_VAR_ARG2
LINTFLAGS += -erroff=E_INCONS_VAL_TYPE_DECL2
LINTFLAGS += -erroff=E_INCONS_VAL_TYPE_USED2
LINTFLAGS += -erroff=E_INCONS_ARG_DECL2
LINTFLAGS += -errtags=yes

LDLIBS += -ldevinfo -lgen -lsysevent -lnvpair -lcmd -ldoor -lclconf -lscconf -R /usr/cluster/lib/sc -L $(ROOTCLUSTLIBSC) -lsds_sc
$(POST_S10_BUILD)LDLIBS += -lmeta

SRCS = $(LINK_SRCS) $(COMMON)/md_gdevs.c
OBJS = $(LINK_OBJS) $(PROG_OBJS)
MODS = $(LINK_MODS) $(PROG)

POFILES = $(LINK_SRCS:.c=.po) $(DEVFSADM_SRC:.c=.po)
POFILE = md_gdevs.po

# install specifics

ROOTLIB_DEVFSADM = $(ROOTLIB)/$(DEVFSADM_DIR)
ROOTLIB_DEVFSADM_LINKMOD = $(ROOTLIB_DEVFSADM)/$(LINKMOD_DIR)
ROOTLIB_DEVFSADM_LINK_MODS = $(LINK_MODS:%=$(ROOTLIB_DEVFSADM_LINKMOD)/%)
ROOTCLUSTPRIVPROG = $(PROG:%=$(ROOTCLUSTLIBSC)/%)

all :=		TARGET= all
install :=	TARGET= install
clean :=	TARGET= clean
clobber :=	TARGET= clobber
lint :=		TARGET= lint


.KEEP_STATE:

all: $(MODS) $(DEVLINKTAB)

install: all				\
	$(ROOTLIB_DEVFSADM_LINKMOD)	\
	$(ROOTLIB_DEVFSADM_LINK_MODS)	\
	$(ROOTCLUSTPRIVPROG)


clean:
	$(RM) $(OBJS) 


lint: $(DEVFSADM_MOD).ln $(LINT_MODULES)

devfsadm.ln: FRC
	$(LINT.c) $(DEVFSADM_SRC) $(LDLIBS)

%.ln: FRC
	$(LINT.c) $(DEVFSADM_SRC) $(@:.ln=.c) $(LDLIBS)

FRC:


$(POFILE):      $(POFILES)
	$(RM) $@; cat $(POFILES) > $@

SUNW_%.so: %.o
	$(LINK.c) -o $@ -G -h $@ $(LDLIBS) $<

%.o: $(COMMON)/%.c
	$(COMPILE.c) -o $@ $<

$(PROG): $(PROG_OBJS)
	$(LINK.c) -o $@ $(PROG_OBJS) $(LDLIBS)
	$(POST_PROCESS)

$(ROOTUSRSBIN):
	$(INS.dir)

$(ROOTLIB_DEVFSADM):
	$(INS.dir)

$(ROOTUSRINCLUDE):
	$(INS.dir)

$(ROOTLIB_DEVFSADM_LINKMOD):
	$(INS.dir)

$(ROOTLIB_DEVFSADM_LINKMOD)/%: %
	$(INS.file)

$(ROOTLIB_DEVFSADM_DAEMON):
	$(RM) $@; $(SYMLINK) ../../sbin/$(DEVFSADM_DIR) $@

$(ROOTUSRSBIN_COMPAT_LINKS):	$(ROOTUSRSBIN_DEVFSADM)
	$(RM) $@ ; $(LN) $(ROOTUSRSBIN_DEVFSADM) $@

include $(SRC)/cmd/Makefile.targ
