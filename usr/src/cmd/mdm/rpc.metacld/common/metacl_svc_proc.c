/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

/*
 * These are the service routines for the DiskSuite cluster rpc's.
 */

#pragma ident	"@(#)metacl_svc_proc.c	1.11	08/05/20 SMI"

#include <rpc/rpc.h>
#include <meta.h>
#include <metacl.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/cladm.h>
#include <poll.h>
#include <sdssc.h>
#include "mdc_local.h"

/*
 * This verifies that the library references are bound.
 */
static bool_t
bind_library_idem()
{
	static bool_t library_is_bound = FALSE;

	if (library_is_bound == TRUE) {
		return (TRUE);
	} else {
		if (sdssc_bind_library() == SDSSC_OKAY) {
			library_is_bound = TRUE;
			return (TRUE);
		} else {
			return (FALSE);
		}
	}
}

/*
 * return a response
 */
/*ARGSUSED*/
bool_t
mdc_null_1_svc(
	void		*argp,
	mdc_bind_res_t	*res,
	struct svc_req	*rqstp		/* RPC stuff */
)
{
	/* Initialization */
	res->mdc_status = mdc_null_err;

	/* do nothing */
	return (TRUE);
}

/*
 * Bind the local device references to the global namespace.
 */
/*ARGSUSED*/
bool_t
mdc_bind_devs_1_svc(
	void			*argp,
	mdc_bind_res_t		*res,
	struct svc_req		*rqstp		/* RPC stuff */
)
{
	int		err;
	mdc_err_t	*mdcep = &res->mdc_status;

	/*
	 * We can't really get here unless this is cluster and the cluster
	 * library is available but you never know. Maybe this
	 * particular node is broken or the library is corrupt.
	 * So, we first bind and check.
	 */
	if (bind_library_idem() == TRUE) {
		/* Initialization */
		*mdcep = mdc_null_err;

		/* setup, check permissions */
		if ((err = mdc_init(rqstp, W_OK, &(res->mdc_status))) < 0) {
			return (FALSE);
		} else if (err != 0) {
			return (TRUE);
		}

		/* call the local function */
		err = sdssc_binddevs();

		(void) mdc_error(&(res->mdc_status), err,
		    mdc_errnum_to_str(err), err);
	}

	return (TRUE);
}

/*
 * Execute an arbitrary command on a remote client..
 */
bool_t
mdc_proxy_1_svc(
	mdcrpc_proxy_args_t	*argp,		/* the cmd to exec. */
	mdc_bind_res_t		*res,
	struct svc_req		*rqstp		/* RPC stuff */
)
{
	pid_t		pid;
	int		exit_no,
			status,
			fildes[2];
	size_t		alloc_size,
			current_alloc_size;
	mdc_err_t	*mdcep = &res->mdc_status;
	void		(*func)();
	char		*char_ptr,
			*resp,
			readbp[256];
	struct pollfd	fds[1];
	sdssc_boolean_e	got_status;

	/*
	 * We can't really get here unless this is a cluster and
	 * the cluster
	 * library is available but you never know. Maybe this
	 * particular node is broken or the library is corrupt.
	 * So, we first bind and check.
	 */
	if (bind_library_idem() == TRUE) {
		/* Initialization */
		*mdcep = mdc_null_err;

		/* setup, check permissions */
		if ((status = mdc_init(rqstp,
		    W_OK, &(res->mdc_status))) < 0) {
			return (FALSE);
		} else if (status != 0) {
			return (TRUE);
		}

		if (pipe(fildes)) {
			/*
			 * Make sure we don't try to use them
			 */

			fildes[0] = -1;
			fildes[1] = -1;
		}

		/* Fork off the process that will do the exec. */
		pid = fork();

		if (pid < 0) {
			(void) close(fildes[0]);
			(void) close(fildes[1]);
			(void) mdc_error(&(res->mdc_status), MDC_PROXYNOFORK,
			    mdc_errnum_to_str(MDC_PROXYNOFORK), -1);
			return (TRUE);
		} else if (pid) {
			func = signal(SIGINT, SIG_IGN);

			alloc_size = 256;
			resp = (char *)malloc(alloc_size);
			if (resp)
				*resp = '\0'; /* null terminated string */
			current_alloc_size = alloc_size;

			fds[0].fd = fildes[0];
			fds[0].events = POLLIN;

			got_status = SDSSC_False;
			while (poll(fds, 1, 100) >= 0) {
				if (fds[0].revents & POLLIN) {
					(void) memset(readbp, '\0', alloc_size);
					(void) read(fildes[0], readbp,
					    alloc_size);
					if (resp) {
						/*
						 * If we got a memory pointer
						 * try to copy the new data
						 * into the buffer.
						 */

						if ((strlen(resp) +
						    strlen(readbp) + 1) >=
						    current_alloc_size) {
							/*
							 * If the old buffer
							 * plus the new data
							 * is longer than the
							 * current length
							 * realloc space.
							 */

			/*lint -e525 */
			current_alloc_size += alloc_size;
			resp = realloc(resp, current_alloc_size);
			/*lint +e525 */
						}

						if (resp) {
							/*
							 * Need to check to
							 * see if we still have
							 * a buffer after
							 * a possible realloc
							 * failure.
							 */

							(void) strcat(resp,
							    readbp);
						}
					}

				} else if (got_status == SDSSC_True) {
					/*
					 * Okay we've picked up the exit status
					 * from the program and there's nothing
					 * left in the pipe so terminate the
					 * loop
					 */

					break;
				}

				if ((got_status == SDSSC_False) &&
				    (waitpid(pid, &status, WNOHANG))) {
					/*
					 * The returns will be:
					 * -1 which indicates an error and
					 *    should bother continuing
					 * >0 which means the process has
					 *    finished so look at the status
					 *
					 * Once here set the state to show
					 * that we can exit the loop when
					 * the read side of the pipe is dry.
					 */

					got_status = SDSSC_True;
				}
			}

			if (WIFEXITED(status)) {
				exit_no = WEXITSTATUS(status); /*lint !e702 */

				(void) mdc_error(&(res->mdc_status),
				    exit_no ? MDC_PROXYFAILED : MDC_NOERROR,
				    resp,
				    exit_no);
				free(resp);
			} else if (WIFSTOPPED(status)) {
				exit_no = WSTOPSIG(status); /*lint !e702 */
				(void) mdc_error(&(res->mdc_status),
				    MDC_PROXYFAILED,
				    mdc_errnum_to_str(MDC_PROXYFAILED),
				    exit_no);
			} else if (WIFSIGNALED(status)) {
				exit_no = WTERMSIG(status);
				(void) mdc_error(&(res->mdc_status),
				    MDC_PROXYKILLED,
				    mdc_errnum_to_str(MDC_PROXYKILLED),
				    exit_no);
			} else {
				exit_no = -1;
				(void) mdc_error(&(res->mdc_status),
				    MDC_PROXYFAILED,
				    mdc_errnum_to_str(MDC_PROXYFAILED),
				    exit_no);
			}

			(void) close(fildes[0]);
			(void) close(fildes[1]);
			(void) signal(SIGINT, func);
			return (TRUE);
		}

		/* child process, call the local function */
		/*
		 * RPC tends to convert (char *)NULL's to (char *)"" so
		 * here we check to see if that hasn't been fixed yet. If
		 * it hasn't, we convert the terminators back to
		 * (char *)NULL's.
		 */
		char_ptr =
		    argp->argvlist.argvlist_val[argp->argvlist.argvlist_len-1];

		if (*char_ptr == '\000') {
			free(char_ptr);
			argp->argvlist.argvlist_val[argp->
			    argvlist.argvlist_len-1] = NULL;
			(argp->argvlist.argvlist_len)--;
		}

		char_ptr = argp->environment.environment_val[
		    argp->environment.environment_len-1];

		if (*char_ptr == '\000') {
			free(char_ptr);
			argp->environment.environment_val[
		    argp->environment.environment_len-1] = NULL;
			(argp->environment.environment_len)--;
		}

		/*
		 * Tie stdout and stderr to the write side of the pipe.
		 */
		(void) dup2(fildes[1], 1);
		(void) dup2(fildes[1], 2);

		exit_no = execve(
		    (argp->argvlist.argvlist_val[0]),
		    (argp->argvlist.argvlist_val),
		    (argp->environment.environment_val));

		_exit(exit_no);

		/*
		 * exit_no = execle(
		 * (argp->argvlist.argvlist_val[0]),
		 * (argp->argvlist.argvlist_val[0]), (char *)NULL,
		 * (argp->environment.environment_val));
		 *
		 * _exit(0);
		 */
	} else {	/* bind library failed. */
		(void) mdc_error(&(res->mdc_status),
		    MDC_PROXYFAILED,
		    mdc_errnum_to_str(MDC_PROXYFAILED),
		    -2);

		return (TRUE);
	}
	/*NOTREACHED*/
}
