/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)mdc_init.c	1.14	08/05/20 SMI"

#include "mdc_local.h"

#include <signal.h>
#include <syslog.h>
#include <netdir.h>
#include <netdb.h>
#include <sys/rtpriocntl.h>
#include <sys/utsname.h>
#include <metacl.h>
#include <sdssc.h>

/* daemon name */
static char	*daemonname = MDC_SERVNAME;

/*
 * reset and exit daemon
 */
void
mdc_exit(
	int	eval
)
{
	/* log exit */
	mdc_eprintf("exiting with %d\n", eval);

	/* exit with value */
	exit(eval);
}

/*
 * signal catchers
 */
static void
mdc_catcher(
	int	sig
)
{
	char	buf[128];
	char	*msg;

	/* log signal */
	if ((msg = strsignal(sig)) == NULL) {
		(void) sprintf(buf, "unknown signal %d", sig);
		msg = buf;
	}
	mdc_eprintf("%s\n", msg);

	/* let default handler do it's thing */
	(void) sigset(sig, SIG_DFL);
	if (kill(getpid(), sig) != 0) {
		mdc_perror("kill(getpid())");
		mdc_exit(-sig);
	}
}

/*
 * initialize daemon
 */
static int
mdc_setup(
	mdc_err_t	*mdcep
)
{
	/* catch common signals */
	if ((sigset(SIGHUP, mdc_catcher) == SIG_ERR) ||
	    (sigset(SIGINT, mdc_catcher) == SIG_ERR) ||
	    (sigset(SIGABRT, mdc_catcher) == SIG_ERR) ||
	    (sigset(SIGBUS, mdc_catcher) == SIG_ERR) ||
	    (sigset(SIGSEGV, mdc_catcher) == SIG_ERR) ||
	    (sigset(SIGPIPE, mdc_catcher) == SIG_ERR) ||
	    (sigset(SIGTERM, mdc_catcher) == SIG_ERR)) {
		/*lint -e746 */
		return (mdc_error(mdcep, errno, "sigset", -1));
		/*lint +e746 */
	}

	/* ignore SIGALRM (used in mdc_cv_timedwait) */
	if (sigset(SIGALRM, SIG_IGN) == SIG_ERR) {
		return (mdc_error(mdcep, errno, "sigset", -1));
	}

	/* return success */
	return (0);
}

/*
 * (re)initalize daemon
 */
static int
mdc_init_daemon(
	mdc_err_t	*mdcep
)
{
	static int	already = 0;

	/* setup */
	if (! already) {
		if (mdc_setup(mdcep) != 0)
			return (-1);
		openlog(daemonname, LOG_CONS, LOG_DAEMON);
		already = 1;
	}

	/* return success */
	return (0);
}

/*
 * get my nodename
 */
char *
mynode(void)
{
	static struct utsname	myuname;
	static int		done = 0;

	if (! done) {
		if (uname(&myuname) == -1) {
			mdc_perror("uname");
			assert(0);
		}
		done = 1;
	}
	return (myuname.nodename);
}

/*
 * check for trusted host and user
 *
 * XXX this is hopelessly TIRPC dependent
 */
static int
check_host(
	struct svc_req		*rqstp		/* RPC stuff */
)
{
	struct authsys_parms	*sys_credp;
	SVCXPRT			*transp = rqstp->rq_xprt;
	struct netconfig	*nconfp = NULL;
	struct nd_hostservlist	*hservlistp = NULL;
	int			i;
	int			rval = -1;
	char			inplace[MD_MAX_SETNAME_PLUS_1];

	/* check for root */
	/*LINTED*/
	sys_credp = (struct authsys_parms *)rqstp->rq_clntcred;
	assert(sys_credp != NULL);
	if (sys_credp->aup_uid != 0)
		goto out;

	/* get hostnames */
	if (transp->xp_netid == NULL) {
		mdc_eprintf("transp->xp_netid == NULL\n");
		goto out;
	}
	if ((nconfp = getnetconfigent(transp->xp_netid)) == NULL) {
#ifdef	DEBUG
		nc_perror("getnetconfigent(transp->xp_netid)");
#endif
		goto out;
	}
	if ((netdir_getbyaddr(nconfp, &hservlistp, &transp->xp_rtaddr) != 0) ||
	    (hservlistp == NULL)) {
#ifdef	DEBUG
		netdir_perror("netdir_getbyaddr(transp->xp_rtaddr)");
#endif
		goto out;
	}

	/* check hostnames */
	for (i = 0; (i < hservlistp->h_cnt); ++i) {
		struct nd_hostserv	*hservp = &hservlistp->h_hostservs[i];
		char			*hostname = hservp->h_host;

		/* localhost is OK */
		if (strcmp(hostname, mynode()) == 0) {
			rval = 0;
			goto out;
		}

#ifdef lets_break_the_daemon
		/*
		 * XXX Currently this code is commented out because ruserok()
		 *    starts to fail after some number of calls. At this
		 *    point libdcs.so is suspect. After libdcs is
		 *    initialized fork() & exec() must be avoided else the
		 *    doors calls will be screwed up. This rpc daemon uses
		 *    fork() & exec(). Once the new version of libdcs.so is
		 *    delivered this code should be tried again.
		 *
		 * check for remote root access
		 */
		if (ruserok(hostname, 1, "root", "root") == 0) {
			rval = 0;
			goto out;
		}
#else
		/*
		 * Try to convert the hostname to nodeid. The function
		 * sdssc_cm_nm2nid does the work in place if it can which
		 * is why we need a temporary copy of the current hostname.
		 */
		(void) strncpy(inplace, hostname, sizeof (inplace)-1);
		sdssc_cm_nm2nid(inplace);

		if (strcmp(inplace, hostname)) {
			/*
			 * If the names are now different it indicates
			 * that hostname was converted to a nodeid. If
			 * the request hostname is part of our cluster
			 * than it's okay to let them in.
			 */

			rval = 0;
			goto out;
		}
#endif
	}

	/* cleanup, return success */
out:
	if (hservlistp != NULL)
		netdir_free(hservlistp, ND_HOSTSERVLIST);
	if (nconfp != NULL)
		free(nconfp);
	return (rval);
}

/*
 * check AUTH_SYS
 */
static int
check_sys(
	struct svc_req		*rqstp,		/* RPC stuff */
	int			amode,		/* R_OK | W_OK */
	mdc_err_t		*mdcep		/* returned status */
)
{
#ifdef	_REENTRANT
	static mutex_t		mx = DEFAULTMUTEX; /*lint !e708 */
#endif	/* _REENTRANT */

	/* for read, anything is OK */
	if (! (amode & W_OK))
		return (0);

#ifdef	_REENTRANT
	/* single thread (not really needed if daemon stays single threaded) */
	(void) mutex_lock(&mx);
#endif	/* _REENTRANT */

	/* check for remote root or METAMED_GID */
	/*LINTED*/
	if (check_host(rqstp) == 0) {
#ifdef	_REENTRANT
		(void) mutex_unlock(&mx);
#endif	/* _REENTRANT */
		return (0);
	}

	/* return failure */
#ifdef	_REENTRANT
	(void) mutex_unlock(&mx);
#endif	/* _REENTRANT */
	return (mdc_error(mdcep, EACCES, daemonname, -1));
}

/*
 * setup RPC service
 *
 * if can't authenticate return < 0
 * if any other error return > 0
 */
int
mdc_init(
	struct svc_req	*rqstp,		/* RPC stuff */
	int		amode,		/* R_OK | W_OK */
	mdc_err_t	*mdcep		/* returned status */
)
{
	SVCXPRT		*transp = rqstp->rq_xprt;

	/*
	 * initialize
	 */
	(void) memset(mdcep, 0, sizeof (*mdcep));

	/*
	 * check credentials
	 */
	switch (rqstp->rq_cred.oa_flavor) {

	/* UNIX flavor */
	case AUTH_SYS:
	{
		if (check_sys(rqstp, amode, mdcep) != 0)
			return (1);	/* error */
		break;
	}

	/* can't authenticate anything else */
	default:
		svcerr_weakauth(transp);
		return (-1);		/* weak authentication */

	}

	/*
	 * (re)initialize
	 */
	if (mdc_init_daemon(mdcep) != 0)
		return (1);		/* error */

	/* return success */
	return (0);
}
