/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident "@(#)mdc_error.c   1.9     08/05/20 SMI"

#include "mdc_local.h"

#include <syslog.h>
#include <metacl.h>
#include <meta.h>

/*
 * Here's the message pool for recommended error messages. This can get
 * sent back to the client in (for instance) mdc_err_t.
 */
char *
mdc_errnum_to_str(int errnum)
{
	switch (errnum) {
	    case MDC_NOERROR:
		return (gettext("No Error"));
	    case BIND_BADDEVICE:
		return (gettext("a local device is not configurable"));
	    case BIND_NOACCESS_DEVICE:
		return (gettext("a local device cannot be accessed"));
	    case MDC_NOACCESS_CCR:
		return (gettext("cannot access the cluster configuration"));
	    case MDC_RPCFAILED:
		return (gettext("rpc failure"));
	    case BIND_NODISKSETCLASS:
		return (gettext("this cluster does not have any disk sets"));
	    case BIND_LOCALSET:
		return (gettext("cannot convert local diskset to global"));
	    case BIND_NOACCESS_SHARED:
		return (gettext("metadevice shared area is not accessible"));
	    case BIND_LINKISDIR:
		return (gettext("cannot convert local diskset to global"));
	    case MDC_NOACCESS:
		return (gettext("access failure"));
	    case MDC_NOTINCLUSTER:
		return (gettext("this host is not in a cluster"));
	    case MDC_PROXYFAILED:
		return (gettext("remote command exit code was non-zero"));
	    case MDC_PROXYNOFORK:
		return (gettext("remote daemon could not fork"));
	    case MDC_PROXYKILLED:
		return (gettext("remote daemon killed by signal"));
	    default:
		return (NULL);
	}
}

/*
 * free and clear error
 */
static void
mdc_clrerror(
	mdc_err_t	*mdcep
)
{
	if (mdcep->mdc_node != NULL)
		free(mdcep->mdc_node);
	if (mdcep->mdc_misc != NULL)
		free(mdcep->mdc_misc);
	(void) memset(mdcep, 0, sizeof (*mdcep));
}

/*
 * Exported Entry Points
 */

/*
 * setup error
 */
int
mdc_error(
	mdc_err_t	*mdcep,
	int		errnum,
	char		*misc,
	int		exit_code
)
{
	mdc_clrerror(mdcep);

	mdcep->mdc_errno = errnum;
	mdcep->mdc_exitcode = exit_code;

	mdcep->mdc_node = strdup(mynode());

	if (misc != NULL)
		mdcep->mdc_misc = strdup(misc);
	else
		mdcep->mdc_misc = strdup("No Error");

	if (errnum != 0)
		return (-1);
	else
		return (0);
}

/*
 * mdc_err_t to string
 */
static char *
mdc_strerror(
	mdc_err_t	*mdcep
)
{
	static char	buf[1024];
	char		*p = buf;
	char		*emsg;

	if (mdcep->mdc_errno < 0) {
		if ((emsg = mdc_errnum_to_str(mdcep->mdc_errno)) != NULL)
			return (emsg);
		(void) sprintf(p,
		    "unknown metadisk cluster errno %d\n", mdcep->mdc_errno);
		return (buf);
	} else {
		if ((emsg = strerror(mdcep->mdc_errno)) != NULL)
			return (emsg);
		(void) sprintf(p,
		    "errno %d out of range", mdcep->mdc_errno);
		return (buf);
	}
}

/*
 * printf-like log
 */
static void
mdc_vprintf(
	const char	*fmt,
	va_list		ap
)
{
	if (isatty(fileno(stderr))) {
#ifdef	_REENTRANT
		static mutex_t	stderr_mx = DEFAULTMUTEX; /*lint !e708 */

		(void) mutex_lock(&stderr_mx);
#endif	/* _REENTRANT */
		(void) vfprintf(stderr, fmt, ap);
		(void) fflush(stderr);
		(void) fsync(fileno(stderr));
#ifdef	_REENTRANT
		(void) mutex_unlock(&stderr_mx);
#endif	/* _REENTRANT */
	}
	vsyslog(LOG_ERR, fmt, ap);
}

/* PRINTFLIKE1 */
void
mdc_eprintf(
	const char	*fmt,
	...
)
{
	va_list		ap;

	va_start(ap, fmt);	/*lint !e40 */
	mdc_vprintf(fmt, ap);
	va_end(ap);
}

/*
 * printf-like perror() log
 */
static void
mdc_vperror(
	mdc_err_t	*mdcep,
	const char	*fmt,
	va_list		ap
)
{
	char		buf[1024];
	char		*p = buf;

	if ((mdcep->mdc_node != NULL) && (mdcep->mdc_node[0] != '\0')) {
		(void) sprintf(p, "%s: ", mdcep->mdc_node);
		p = &buf[strlen(buf)];
	}
	if ((mdcep->mdc_misc != NULL) && (mdcep->mdc_misc[0] != '\0')) {
		(void) sprintf(p, "%s: ", mdcep->mdc_misc);
		p = &buf[strlen(buf)];
	}
	if ((fmt != NULL) && (*fmt != '\0')) {
		(void) vsprintf(p, fmt, ap);
		p = &buf[strlen(buf)];
		(void) sprintf(p, ": ");
		p = &buf[strlen(buf)];
	}
	(void) sprintf(p, "%s", mdc_strerror(mdcep));
	mdc_eprintf("%s\n", buf);
}

/* PRINTFLIKE2 */
void
mdcde_perror(
	mdc_err_t	*mdcep,
	const char	*fmt,
	...
)
{
	va_list		ap;

	va_start(ap, fmt);	/*lint !e40 */
	mdc_vperror(mdcep, fmt, ap);
	va_end(ap);
}

/* PRINTFLIKE1 */
void
mdc_perror(
	const char	*fmt,
	...
)
{
	va_list		ap;
	mdc_err_t	status = mdc_null_err;

	/*lint -e746 */
	(void) mdc_error(&status, errno, NULL, 0);
	/*lint +e746 */
	va_start(ap, fmt);	/*lint !e40 */
	mdc_vperror(&status, fmt, ap);
	va_end(ap);
	mdc_clrerror(&status);
}
