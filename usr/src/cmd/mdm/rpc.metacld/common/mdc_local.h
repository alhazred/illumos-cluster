/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_MDC_LOCAL_H
#define	_MDC_LOCAL_H

#pragma ident	"@(#)mdc_local.h	1.7	08/05/20 SMI"

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <assert.h>
#include <stdarg.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/sysmacros.h>
#include <sys/mkdev.h>
#include <sys/time.h>
#include <sys/dkio.h>
#include <sys/vtoc.h>

#include <metacl.h>

#ifdef	__cplusplus
extern "C" {
#endif

#ifdef	_REENTRANT
/*
 * millisecond time
 */
typedef	ulonglong_t	mdc_msec_t;
#endif	/* _REENTRANT */

/*
 * extern functions
 */
extern char		*mdc_errnum_to_str(int errnum);

extern	int		mdc_error(mdc_err_t *mdcep, int errnum, char *name,
			    int exit_code);
/*PRINTFLIKE2*/
extern	void		mdcde_perror(mdc_err_t *mdcep, const char *fmt, ...);
/*PRINTFLIKE1*/
extern	void		mdc_perror(const char *fmt, ...);
/*PRINTFLIKE1*/
extern	void		mdc_eprintf(const char *fmt, ...);

/* mdc_init.c */
extern	void		mdc_exit(int eval);
extern	int		mdc_init(struct svc_req *rqstp, int amode,
			    mdc_err_t *mdcep);

#ifdef	__cplusplus
}
#endif

#endif	/* _MDC_LOCAL_H */
