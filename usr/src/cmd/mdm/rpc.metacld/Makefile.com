#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#ident	"@(#)Makefile.com	1.17	08/05/20 SMI"
#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# cmd/mdm/rpc.metacld/Makefile.com
#
PROG = rpc.metacld

include $(SRC)/cmd/Makefile.cmd

#
# This is available in our s9 reference proto area as private
# interfaces via rpc source files.  These are not delivered
# into any package we can install, so they are pulled over
# from the source tree into our reference proto area.
#
METACL_X	= metacl.x
METACL_X_SRC	= $(REF_PROTO)/usr/src/head/$(METACL_X)
META_ARR_X	= meta_arr.x
META_ARR_X_SRC	= $(REF_PROTO)/usr/src/uts/common/sys/lvm/$(META_ARR_X)
META_BASIC_X	= meta_basic.x
META_BASIC_X_SRC = $(REF_PROTO)/usr/src/uts/common/sys/lvm/$(META_BASIC_X)

OBJS = mdc_error.o mdc_freeresult.o mdc_init.o metacl_svc_proc.o
RPC_OBJS += $(METACL_X:%.x=%_svc.o) $(METACL_X:%.x=%_xdr.o)
RPC_OBJS += $(META_BASIC_X:%.x=%_xdr.o)
OBJECTS = $(OBJS) $(RPC_OBJS)
CHECKHDRS = mdc_local.h
CHECK_FILES = $(OBJS:%.o=%.c_check) $(CHECKHDRS:%.h=%.h_check)

SRCS		= $(OBJS:%.o=../common/%.c)
RPC_SRCS	= metacl_svc.c metacl_xdr.c meta_basic_xdr.c
ALL_SRCS	= $(SRCS) $(RPC_SRCS)
PIFILES		= $(ALL_SRCS:%.c=%.pi)
POFILE		= $(PROG).po
LINTFILES	= $(OBJECTS:%.o=%.ln)

.KEEP_STATE:

#
# Compiler flags
#
LDLIBS += -lmeta -lgen -lsocket -lnsl
CPPFLAGS += $(MDM_FLAGS) -I$(REF_PROTO)/usr/src/head
RPCGENFLAGS = -C -M $(MDM_FLAGS)
metacl_svc.c:= RPCGENFLAGS = -C -M $(MDM_FLAGS) -K -1
metacl_svc.o:= CPPFLAGS += -erroff=E_OLD_STYLE_DECL_OR_BAD_TYPE

CLOBBERFILES += $(METACL_X) $(META_BASIC_X) $(META_ARR_X)
CLOBBERFILES += meta_basic_xdr.c metacl_svc.c metacl_xdr.c
CLOBBERFILES += $(PROG) $(OBJECTS)

#
# Targets
#
all: $(PROG)

clean:

$(PROG): $(OBJECTS)
	$(LINK.c) -o $@ $(OBJECTS) $(LDLIBS)
	$(POST_PROCESS)

#
# Create a symbolic link in the build area to the .x file in
# the Solaris reference proto area.
# This eliminates the need to awk the include
# directives created by rpcgen.
# meta_arr.x is included by meta_basic.x
#
$(METACL_X): $(METACL_X_SRC)
	$(RM) -f $@
	$(LN) -s $(METACL_X_SRC) $@

$(META_BASIC_X): $(META_BASIC_X_SRC)
	$(RM) -f $@
	$(LN) -s $(META_BASIC_X_SRC) $@

$(META_ARR_X): $(META_ARR_X_SRC)
	$(RM) -f $@
	$(LN) -s $(META_ARR_X_SRC) $@

metacl_svc.c: $(METACL_X)
	$(RPCGEN) $(RPCGENFLAGS) -s circuit_n $(METACL_X) > $@

metacl_xdr.c: $(METACL_X) $(META_ARR_X)
	$(RPCGEN) $(RPCGENFLAGS) -c $(METACL_X) > $@

meta_basic_xdr.c: $(META_BASIC_X) $(META_ARR_X)
	$(RPCGEN) $(RPCGENFLAGS) -c $(META_BASIC_X) > $@

include $(SRC)/cmd/Makefile.targ

%.o: %.c
	$(COMPILE.c) -o $@ $<

%.o: ../common/%.c
	$(COMPILE.c) -o $@ $<
