/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Establish cluster device binding to global namespace.
 */

#pragma ident "@(#)metaclusterbinddevs.c   1.7     08/05/20 SMI"

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */
#include <meta.h>
#include <sdssc.h>
#include <metacl.h>
#include <rgm/sczones.h>


/*
 * Very simple, just call sdssc_binddevs in libsds_sc.
 */
int
main()
{
	mdc_errno_t retval = -1;

	if (sc_zonescheck() != 0)
		return (1);
	if (sdssc_bind_library() == SDSSC_OKAY) {
		retval = sdssc_binddevs();
	}

	if (retval == 0) {
		return (0);
	} else {
		return (-1);
	}

}
