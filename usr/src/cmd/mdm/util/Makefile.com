#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#ident	"@(#)Makefile.com	1.14	08/05/20 SMI"
#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
# cmd/mdm/util/Makefile.com
#
include $(SRC)/cmd/Makefile.cmd

SRCS		= $(PROG:%=%.c)
OBJECTS		= $(PROG:%=%.o)
LINTFILES	= $(PROG:%=%.ln)
PIFILES		= $(SRCS:%.c=%.pi)
POFILE		= mdm.po

LIBS = -Bstatic -lgen -Bdynamic -ladm -lnsl -ldevid

#
# Compiler flags
#
CPPFLAGS		+= $(MDM_FLAGS) -I$(REF_PROTO)/usr/src/head

metaclusterbinddevs	:= LDLIBS += -lmeta -lsczones

CLOBBERFILES += $(PROG:%=%.o)

.KEEP_STATE:

#
# Targets
#
all: $(PROG)

clean:

$(PROG): $$(@:%=%.o)
	$(LINK.c) -o $@ $@.o $(LDLIBS)
	$(POST_PROCESS)

%.o:	../common/%.c
	$(COMPILE.c) $<

%.pi:	../common/%.c
	$(COMPILE.CPP) $< > $@

include $(SRC)/cmd/Makefile.targ
