//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)rtreg_proxy_serverd.cc	1.5	08/07/25 SMI"

//
// rtreg server daemon.
// The daemon accepts clrt register command from a virtual
// cluster node and executes them in the global zone. The results
// are returned back to the virtual cluster node.
//
#include <nslib/ns.h>
#include <libintl.h>
#include <signal.h>
#include <rtreg_proxy_impl.h>
#include <rgm/sczones.h>
#include <sys/sc_syslog_msg.h>

//
// Utility function to mask all the maskable signals.
//
void
block_allsignals(void)
{
	sigset_t		s_mask;
	sc_syslog_msg_handle_t	handle;

	(void) sc_syslog_msg_initialize(&handle, RTREG_PROXY_SERVER, "");

	//
	// Block the signals that can be masked.
	//
	if (sigfillset(&s_mask) == -1) {
		//
		// SCMSGS
		// @explanation
		// The rtreg server program has encountered a failed
		// sigfillset(3C) call. The error message
		// contains the error number for the failure.
		// @user_action
		// Contact your authorized Sun service provider to
		// determine whether a workaround or patch is available.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "sigfillset returned %d. Exiting.", errno);
		sc_syslog_msg_done(&handle);
		exit(1);
	}

	if (thr_sigsetmask(SIG_BLOCK, &s_mask, NULL) != 0) {
		//
		// SCMSGS
		// @explanation
		// The rtreg server program has encountered a failed
		// thr_sigsetmask(3C) system call. The error message
		// indicates the error number for the failure.
		// @user_action
		// Contact your authorized Sun service provider to
		// determine whether a workaround or patch is available.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "thr_sigsetmask returned %d. Exiting.", errno);
		sc_syslog_msg_done(&handle);
		exit(1);
	}
}

//
// Wait for signals and log any signal received. The SIGHUP signal is
// ignored after logging a message. For any other signal the action is
// to exit after logging the information. This is run as a seperate
// thread.
//
void *
wait_for_signals(void *)
{
	sigset_t	s_mask;
	siginfo_t	sinfo;
	int		sig	= 0;

	sc_syslog_msg_handle_t	handle;
	(void) sc_syslog_msg_initialize(&handle, RTREG_PROXY_SERVER, "");

	//
	// Here we block all maskable signals.
	//
	block_allsignals();

	//
	// Fill the mask with all the signals in the system
	//
	if (sigfillset(&s_mask) == -1) {
		//
		// scmsgs already explained.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "sigfillset returned %d. Exiting.", errno);
		sc_syslog_msg_done(&handle);
		exit(1);
	}

	//
	// sigwait can return with EINTR when a thread forks.
	// Also ignore SIGHUP. We could have simply added SIGHUP to the
	// list of signals to be masked but the check is here so that we
	// can add a syslog message if necessary for SIGHUP.
	//
	do {
		sig = sigwaitinfo(&s_mask, &sinfo);
	} while (((sig < 0) && (errno == EINTR)) || (sig == SIGHUP));

	switch (sig) {
	case -1:
		//
		// SCMSGS
		// @explanation
		// The rtreg server program has encountered a failed
		// sigwaitinfo(3RT) system call. The error message contains
		// the error number for the failure.
		// @user_action
		// Contact your authorized Sun service provider to
		// determine whether a workaround or patch is available.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "sigwaitinfo returned %d. Exiting.", errno);
		sc_syslog_msg_done(&handle);
		exit(1);
		/* FALLTHROUGH */
		/* NOTREACHED */
	default:
		//
		// When we get SIGTERM log the fact so that we will know when
		// we terminate normally versus abnormally
		//
		if (sig == SIGTERM) {
			//
			// SCMSGS
			// @explanation
			// The rtreg server program received a SIGTERM signal
			// and is exiting.
			// @user_action
			// This is an informational message. No user action
			// is required.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "Killed(sigterm) Exiting.");
			sc_syslog_msg_done(&handle);
			exit(1);
			/* NOTREACHED */
		}
		//
		// SCMSGS
		// @explanation
		// The rtreg server program got an unexpected signal and is
		// exiting.
		// @user_action
		// Contact your authorized Sun service provider to
		// determine whether a workaround or patch is available.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Unexpected signal, Exiting.");
		sc_syslog_msg_done(&handle);
		exit(1);
	}
	/* NOTREACHED */
	return ((void *)NULL);
}

//
// The rtreg server daemon process. Here we create two threads,
// one to handle signals and the other to handle requests to execute tasks.
//
void
daemon_process()
{
	//
	// Mask the signals.
	//
	block_allsignals();

	rtreg_proxy::rtreg_server_var	rtreg_server_v;
	Environment				env;
	thread_t				sig_tid;
	naming::naming_context_var		ctx_v;
	CORBA::Exception			*exp = NULL;
	sc_syslog_msg_handle_t			handle;

	ctx_v = ns::local_nameserver_c1();

	(void) sc_syslog_msg_initialize(&handle, RTREG_PROXY_SERVER, "");

	// Daemonize.
	(void) setsid();

	//
	// Create a thread that handles any signal and logs an
	// appropriate message.
	//
	if (thr_create(NULL, NULL, wait_for_signals, NULL, THR_DETACHED,
	    &sig_tid) != 0) {
		//
		// SCMSGS
		// @explanation
		// The rtreg server program encountered a failure while
		// executing thr_create(3C).
		// @user_action
		// Contact your authorized Sun service provider to
		// determine whether a workaround or patch is available.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Unable to create thread, errno = %d. Exiting\n.", errno);
		sc_syslog_msg_done(&handle);
		exit(1);
	}

	//
	// Now we are ready to accept connection. Register the rtreg_server
	// object with the name server and stay around to process requests.
	//
	rtreg_server_v = (new rtreg_server_impl)->get_objref();

	ctx_v->rebind(RTREG_PROXY_SERVER, rtreg_server_v, env);

	exp = env.exception();
	if (exp != NULL) {
		//
		// Rebind failed. We will try to unbind and try binding again.
		//
		env.clear();
		ctx_v->unbind(RTREG_PROXY_SERVER, env);

		if ((exp = env.exception()) != NULL) {

			//
			// SCMSGS
			// @explanation
			// The rtreg server program could not register the
			// corba object in the name server.
			// @user_action
			// Contact your authorized Sun service provider to
			// determine whether a workaround or patch is
			// available.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "Error while unbinding 'rtreg_proxy_server' "
			    "in the name server. Exiting.");
			sc_syslog_msg_done(&handle);
			exit(1);
		}
		ctx_v->bind(RTREG_PROXY_SERVER, rtreg_server_v, env);

		if ((exp = env.exception()) != NULL) {
			//
			// SCMSGS
			// @explanation
			// The rtreg server program could not bind the corba
			// object from the local name server.
			// @user_action
			// Contact your authorized Sun service provider to
			// determine whether a workaround or patch is
			// available.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "Error while binding 'rtreg_proxy_server' "
			    "in the name server. Exiting.");
			sc_syslog_msg_done(&handle);
			exit(1);
		}
	}
	thr_exit(0);
	// NOT REACHED
}

//
// Wait till the rtreg server daemon has started. We do this by resolving
// the the rtreg server object in the global name server. The daemon will be
// ready to accept client requests after it registers this object in the
// name server.
//
static void
wait_for_daemon()
{
	rtreg_proxy::rtreg_server_var	rtreg_server_v;
	CORBA::Object_var		obj_v;
	Environment			env;
	CORBA::Exception		*exp;
	naming::naming_context_var	ctx_v	= ns::local_nameserver();

	//
	// The parent process waits for the child to register an
	// IDL object with the name server and then exits so
	// the shell will think the command is finished.
	//

	//
	// Wait till the server object is registered with the name server.
	//
	for (;;) {

		obj_v = ctx_v->resolve(RTREG_PROXY_SERVER, env);

		if ((exp = env.exception()) != NULL) {
			if (naming::not_found::_exnarrow(exp) == NULL) {
				exit(1);
			}
			// Wait and then try again.
			env.clear();
		} else {
			//
			// Test to see that the object we got from the
			// name server is active. If not, wait for
			// the child process to register the new
			// object.
			//
			rtreg_server_v =
			    rtreg_proxy::rtreg_server::_narrow(obj_v);

			ASSERT(!CORBA::is_nil(rtreg_server_v));

			bool alive = rtreg_server_v->is_alive(env);
			if (env.exception() == NULL && alive) {
				break;
			}
			env.clear();
		}
		//
		// Wait for 100 milliseconds.
		//
		os::usecsleep(MILLISEC*100);
	}
}


//
// Create the daemon process and wait for it to be ready to accept client
// requests in the parent. The parent then exists which indicates that the
// proxy rtreg service is up.
//
static void
daemonize()
{
	pid_t			proc_pid;
	sc_syslog_msg_handle_t	handle;

	(void) sc_syslog_msg_initialize(&handle, RTREG_PROXY_SERVER, "");

	//
	// Initialize ORB.
	//
	if (clconf_lib_init() != 0) {
		//
		// SCMSGS
		// @explanation
		// The rtreg server program was unable to initialize
		// its interface to the low-level cluster machinery.
		// @user_action
		// Make sure the nodes are booted in cluster mode. If so,
		// contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			"Could not initialize the ORB. Exiting.");
		sc_syslog_msg_done(&handle);
		exit(1);
	}

	rtreg_proxy::rtreg_server_var	rtreg_server_v;
	CORBA::Object_var			obj_v;
	Environment				env;
	CORBA::Exception			*exp;

	naming::naming_context_var ctx_v	= ns::local_nameserver();

	//
	// The child process checks to see if there is already a process
	// running and doesn't start another.
	//
	obj_v = ctx_v->resolve(RTREG_PROXY_SERVER, env);

	if ((exp = env.exception()) != NULL) {
		if (naming::not_found::_exnarrow(exp) == NULL) {
			//
			// SCMSGS
			// @explanation
			// The rtreg server program could not resolve the
			// rtreg proxy server object from the local
			// name server.
			// @user_action
			// Contact your authorized Sun service provider to
			// determine whether a workaround or patch is
			// available.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "Could not resolve 'rtreg_proxy_server' in "
			    "the name server.  Exiting.");
			sc_syslog_msg_done(&handle);
			exit(1);
		}
		env.clear();
	} else {
		//
		// If the object is found and is active, flag an error
		// and exit.
		//
		rtreg_server_v =
		    rtreg_proxy::rtreg_server::_narrow(obj_v);
		ASSERT(!CORBA::is_nil(rtreg_server_v));
		bool alive = rtreg_server_v->is_alive(env);
		if (env.exception() == NULL && alive) {
			//
			// SCMSGS
			// @explanation
			// The rtreg server program found another active
			// rtreg program already in execution.
			// @user_action
			// This is an information message. No user action
			// is required.
			//
			(void) sc_syslog_msg_log(handle, LOG_INFO, MESSAGE,
			    "Found another active instance of rtreg "
			    "proxy server.  Exiting rtreg_serverd.");
			sc_syslog_msg_done(&handle);
			exit(0);
		}
	}

	//
	// We need a way to declare the rtreg proxy server service as online,
	// Since rtreg proxy service is a daemon that does not go away,
	// we fork off a new process that does the actual work of the
	// rtreg proxy service.The parent waits till the initialization is
	// complete and the service is ready to accept client requests and
	// then goes away. Based on the return status of the parent,
	// SMF changes the state of the service.
	//
	proc_pid = fork1();

	if (proc_pid == (pid_t)-1) {
		//
		// SCMSGS
		// @explanation
		// The rtreg server program has encountered failure of
		// fork1(2) system call. The error message contains the error
		// number for the failure.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "fork1 failed, errno = %d. Exiting.", errno);
		sc_syslog_msg_done(&handle);
		exit(1);
	} else if (proc_pid == 0) {
		daemon_process();
		/* NOTREACHED */
	} else {
		//
		// Parent.
		// We exit after making sure the daemon is up and ready
		// to process its client requests.
		//
		wait_for_daemon();
	}
}

//
// Main routine. Start the daemon service here.
//
int
main()
{
	//
	// Return an error if run from a non-global zone.
	//
	if (sc_zonescheck() != 0) {
		return (1);
	}

	//
	// Create the daemon service.
	//
	daemonize();
	return (0);
}
