//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)rtreg_proxy_impl.cc	1.6	08/08/01 SMI"

//
// RTREG proxy server object Implementation.
//
#include <libintl.h>
#include <locale.h>
#include <rtreg_proxy_impl.h>
#include <h/remote_exec.h>
#include <orb/infrastructure/orb_conf.h>
#include <orb/infrastructure/orb.h>
#include <sys/os.h>
#include <nslib/ns.h>
#include <orb/object/adapter.h>
#include <sys/vc_int.h>
#include <libzccfg/libzccfg.h>

#define	MAX_CMD_LEN	8192
#define	RTREG_PROXY_CMD "/usr/cluster/bin/clrt register"

//
// This routine returns true if the rt_server interface is alive and
// working. It can be used to tell if the object reference obtained from
// the name server is valid or not.
//
bool
rtreg_server_impl::is_alive(Environment &)
{
	return (true);
}

//
// Unreference notification.
//
void
rtreg_server_impl::_unreferenced(unref_t cookie)
{
	(void) _last_unref(cookie);
	exit(0);
}

//
// Interface definition.
//

//
// Execute the clrt program specified by 'rtreg_cmd' in user space.
//
// Arguments:
//	rtreg_cmd: clrt command with its arguments to be executed.
// Output:
//	On failure, returns an non-zero value. On success returns
//	0.
//	retval: exit status of the command.
//	stdout_str: Standard output that the command generates.
//	stderr_str: Standard error that the command generates.
//
// The caller of this interface has to do the necessary cleanup of the
// memory allocated to these strings.
//
int32_t
rtreg_server_impl::handle_rtreg_cmd(int32_t &retval,
    CORBA::String_out stdout_str, CORBA::String_out stderr_str,
    const char *cmd_args, Environment &e)
{
	int		exec_ret_val;
	nodeid_t 	nid = orb_conf::local_nodeid();
	char		clexec_name[ZONENAME_MAX + 4];

	os::sprintf(clexec_name, "cl_exec.%d", nid);

	remote_exec::cl_exec_var	cl_exec_v;
	naming::naming_context_var	ctx_v = ns::root_nameserver();
	CORBA::Object_var		obj_v;
	CORBA::Exception		*exp;
	char				*cmd;
	char				*tmp_cmd_args;
	char				*cluster_namep = NULL;
	char				*stderrstr = NULL;
	char 				*cli_arg = NULL;
	char 				zc_root[MAXPATHLEN];
	char 				modified_rtrpath[MAXPATHLEN];
	char 				modified_filepath[MAXPATHLEN];
	int 				err = ZC_OK;
	int				libzccfg_err = ZC_OK;
	zc_dochandle_t			handle;

	uint32_t clid = e.get_cluster_id();
	if (clid < MIN_CLUSTER_ID) {
		//
		// Execution from a global zone or 1334 zone is not
		// supported.
		//
		stderrstr = new char[100];
		ASSERT(stderrstr != NULL);
		(void) sprintf(stderrstr, gettext("cluster ID : %d, "
		    "rtreg proxy can be initiated only from a virtual "
		    "cluster node.\n"), clid);
		stderr_str = stderrstr;
		return (-1);
	}
	exec_ret_val = clconf_get_cluster_name(clid, &cluster_namep);

	if (exec_ret_val != 0) {
		stderrstr = new char[100];
		ASSERT(stderrstr != NULL);
		(void) sprintf(stderrstr, gettext("Failed to get the cluster "
		    "name from cluster id %d.\n"), exec_ret_val);
		stderr_str = stderrstr;
		return (-1);
	}

	//
	// Get the Zone Root path
	//

	tmp_cmd_args = strdup(cmd_args);

	if ((handle = zccfg_init_handle()) == NULL) {
		stderrstr = new char[100];
		ASSERT(stderrstr != NULL);
		sprintf(stderrstr, gettext("Failed to get "
			"zonepath for zone cluster "
			"%s."), cluster_namep);

		return (-1);
	}

	if ((libzccfg_err = zccfg_get_handle(cluster_namep, handle))
			!= ZC_OK) {
		stderrstr = new char[100];
		ASSERT(stderrstr != NULL);
		sprintf(stderrstr, gettext("Failed to get "
			"zonepath for zone cluster "
			"%s."), cluster_namep);

		return (-1);
	}

	if ((err = zccfg_get_zonepath(handle, zc_root,
		sizeof (zc_root))) != ZC_OK) {
		stderrstr = new char[100];
		ASSERT(stderrstr != NULL);
		sprintf(stderrstr, gettext("Failed to get "
			"zonepath for zone cluster "
			"%s."), cluster_namep);

		return (-1);
	}

	zccfg_fini_handle(handle);

	//
	// Prepend the zone root path to the -i and -f arguments if present
	//

	tmp_cmd_args = strdup(cmd_args);
	ASSERT(tmp_cmd_args != NULL);
	cli_arg = strtok(tmp_cmd_args, " ");

	//
	// Check if we have enough space for accomadating the command
	// <clrt> <register> -Z <zcname> [-f <zcroot>/<filepath>] [-i
	// <zcroot>/<filepath>] <other_arguments>
	//
	cmd = new char[(strlen(RTREG_PROXY_CMD) + strlen(cluster_namep) + 6 +
	    strlen(cmd_args) + 2 * strlen(zc_root) + 10)];

	(void) sprintf(cmd, "%s -Z %s", RTREG_PROXY_CMD, cluster_namep);

	if (cli_arg[0] == '-') {
		if (cli_arg[1] == 'f') {
			//
			// We have a -f option specified
			//
			strcat(cmd, " ");
			strcat(cmd, cli_arg);

			cli_arg = strtok(NULL, " ");
			if (cli_arg != NULL) {
				sprintf(modified_rtrpath, "%s/root%s",
					zc_root, cli_arg);
			}
			strcat(cmd, " ");
			strcat(cmd, modified_rtrpath);
		} else if (cli_arg[1] == 'i') {
			//
			// We have a -i option specified
			//
			strcat(cmd, " ");
			strcat(cmd, cli_arg);

			cli_arg = strtok(NULL, " ");
			if (cli_arg != NULL) {
				sprintf(modified_filepath, "%s/root%s",
					zc_root, cli_arg);
			}

			strcat(cmd, " ");
			strcat(cmd, modified_filepath);
		} else {
			strcat(cmd, " ");
			strcat(cmd, cli_arg);
		}
	} else {
		strcat(cmd, " ");
		strcat(cmd, cli_arg);
	}

	while ((cli_arg = strtok(NULL, " ")) != NULL) {
		if (cli_arg[0] == '-') {
			if (cli_arg[1] == 'f') {
				//
				// We have a -f option specified
				//
				strcat(cmd, " ");
				strcat(cmd, cli_arg);

				cli_arg = strtok(NULL, " ");
				if (cli_arg != NULL) {
					sprintf(modified_rtrpath, "%s/root%s",
						zc_root, cli_arg);
				}

				strcat(cmd, " ");
				strcat(cmd, modified_rtrpath);
			} else if (cli_arg[1] == 'i') {
				//
				// We have a -i option specified
				//
				strcat(cmd, " ");
				strcat(cmd, cli_arg);

				cli_arg = strtok(NULL, " ");
				if (cli_arg != NULL) {
					sprintf(modified_filepath, "%s/root%s",
						zc_root, cli_arg);
				}

				strcat(cmd, " ");
				strcat(cmd, modified_filepath);
			} else {
				strcat(cmd, " ");
				strcat(cmd, cli_arg);
			}
		} else {
			strcat(cmd, " ");
			strcat(cmd, cli_arg);
		}
	}


	delete [] cluster_namep;

	obj_v = ctx_v->resolve(clexec_name, e);

	if ((exp = e.exception()) != NULL) {
		if (naming::not_found::_exnarrow(exp) != NULL) {
			e.clear();
			stderrstr = new char[100];
			ASSERT(stderrstr != NULL);
			(void) sprintf(stderrstr, gettext("Nodeid : %d, "
			    "remote execution service not up.\n"), nid);
			stderr_str = stderrstr;
			delete [] cmd;
			return (-1);
		}
	} else {
		//
		// Test to see that the object we got from the
		// name server is active.
		//
		cl_exec_v = remote_exec::cl_exec::_narrow(obj_v);
		ASSERT(!CORBA::is_nil(cl_exec_v));
		(void) cl_exec_v->is_alive(e);
		if (e.exception() != NULL) {
			e.clear();
			stderrstr = new char[100];
			ASSERT(stderrstr != NULL);
			(void) sprintf(stderrstr, gettext("The remote "
			    "execution server object is inactive.\n"));
			stderr_str = stderrstr;
			delete [] cmd;
			return (-1);
		}
	}
	//
	// Here we get the stdout and stderr of the task.
	// We inform the infrastructure that we do require this info to be
	// passed back to us.
	//
	exec_ret_val = cl_exec_v->exec_program_get_output(retval, stdout_str,
	    stderr_str, cmd, remote_exec::DEF, 0, e);

	delete [] cmd;
	return (exec_ret_val);
}
