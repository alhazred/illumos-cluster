//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
#pragma ident	"@(#)parallel_client.cc	1.2	08/07/16 SMI"

#include <orb/infrastructure/orb.h>
#include <nslib/ns.h>
#include <pthread.h>

// Number of threads to create
#define	NUM_THREAD	10

void * execute_cmd(void *);

typedef struct {
	int id;
} my_para;

void *
execute_cmd(void *arg)
{

	my_para *p = (my_para *)arg;
	naming::naming_context_var ctx_v = ns::root_nameserver();
	// naming::naming_context_var ctx_v = ns::local_nameserver();
	if (CORBA::is_nil(ctx_v)) {
		(void) printf("Could not get nameserver for %d\n", p->id);
		return (NULL);
	}

	for (int i = 1; i <= 1000; i++) {
		for (int j = 1; j <= 1000; j++) {
			int k = i * j;
			k = k + 1;
		}
	}

	// Success
	return (NULL);
}

int
main()
{
	int idx = 0;
	pthread_t tid[NUM_THREAD];
	my_para *p;

	//
	// Initialize the ORB
	//
	if (ORB::initialize() != 0) {
		(void) printf("Failed to initialize orb\n");
		return (1);
	}

	p = (my_para *)malloc(sizeof (my_para)*NUM_THREAD);
	ASSERT(p != NULL);

	for (idx = 0; idx < NUM_THREAD; idx++) {
		p[idx].id = idx;
	}

	for (idx = 0; idx < NUM_THREAD; idx++) {
		(void) pthread_create(&tid[idx], NULL,
		    execute_cmd, (void *)(p+idx));
	}

	for (idx = 0; idx < NUM_THREAD; idx++) {
		(void) pthread_join(tid[idx], NULL);
	}

	free(p);
	return (0);
}
