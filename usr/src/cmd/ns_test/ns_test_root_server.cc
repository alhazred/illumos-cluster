//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
#pragma ident	"@(#)ns_test_root_server.cc	1.2	08/07/16 SMI"

#include <orb/infrastructure/orb.h>
#include <nslib/ns.h>
#include <ns_test_obj_impl.h>

static ns_test_obj_impl	*ns_test_obj_implp	= NULL;

const char *ns_test::ns_test_obj::NS_TEST_OBJ_KEY;

int
register_ns_test_object(ns_test::ns_test_obj_ptr ns_test_obj_p)
{
	Environment			e;
	naming::naming_context_var	local_ns_v;
	int				retval = 0;

	local_ns_v = ns::root_nameserver();

	if (CORBA::is_nil(local_ns_v)) {
		(void) printf("Root name-server reference is Null\n");
		return (1);
	}

	local_ns_v->bind(ns_test::ns_test_obj::NS_TEST_OBJ_KEY,
	    ns_test_obj_p, e);

	if (e.exception() != NULL) {
		(void) printf("Exception occured while binding ns_test_obj\n");
		e.clear();
		retval = 1;
	} else {
		(void) printf("Binding of ns_test_obj successful\n");
	}
	return (retval);
}

int
unregister_ns_test_object()
{
	Environment			e;
	naming::naming_context_var	local_ns_v;
	int				retval = 0;

	local_ns_v = ns::root_nameserver();

	if (CORBA::is_nil(local_ns_v)) {
		(void) printf("Root name-server reference is Null\n");
		return (1);
	}

	local_ns_v->unbind(ns_test::ns_test_obj::NS_TEST_OBJ_KEY, e);

	if (e.exception() != NULL) {
		(void) printf(
		    "Exception occured while unbinding ns_test_obj \n");
		e.clear();
		retval = 1;
	} else {
		(void) printf("Unbinding of ns_test_obj successful \n");
	}
	return (retval);
}

main(int argc, char *argv[])
{
	ns_test::ns_test_obj_ptr	ns_test_obj_p;
	int retval			= 0;

	//
	// Initialize the ORB
	//
	if (ORB::initialize() != 0) {
		(void) printf("Failed to initialize orb in server program\n");
		return (1);
	}


	//
	// Register the ns_test_obj in the local name server
	//
	ns_test_obj_implp = new ns_test_obj_impl();

	if (ns_test_obj_implp == NULL) {
		(void) printf("Failed to allocate ns_test object\n");
		return (1);
	}

	if (argc == 1) {
		//
		// Register the object
		//
		ns_test_obj_p = ns_test_obj_implp->get_objref();
		retval = register_ns_test_object(ns_test_obj_p);
	} else {
		retval = unregister_ns_test_object();
	}

	while (1) {}

	return (retval); //lint !e527
} //lint !e715 !e818
