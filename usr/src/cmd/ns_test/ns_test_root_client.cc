//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
#pragma ident	"@(#)ns_test_root_client.cc	1.2	08/07/16 SMI"


#include <orb/infrastructure/orb.h>
#include <nslib/ns.h>
#include <ns_test_obj_impl.h>

const char *ns_test::ns_test_obj::NS_TEST_OBJ_KEY;

int
my_func()
{
	naming::naming_context_var	t_ctx_v = ns::root_nameserver();
	if (CORBA::is_nil(t_ctx_v)) {
		return (1);
	}
	return (0);
}

main()
{

	Environment			env;
	naming::naming_context_var	ctx_v;
	CORBA::Exception 		*ex	= NULL;
	CORBA::Object_var		obj_v;
	int				retval	= 0;

	//
	// Initialize the ORB
	//
	if (ORB::initialize() != 0) {
		(void) printf("Failed to initialize orb in client program\n");
		return (1);
	}

	ctx_v = ns::root_nameserver();

	ASSERT(!CORBA::is_nil(ctx_v));

	(void) printf("Lookup the ns_test obj\n");

	// Look for the ns_test object
	obj_v = ctx_v->resolve(ns_test::ns_test_obj::NS_TEST_OBJ_KEY, env);

	if ((ex = env.exception()) != NULL) {
			if (naming::not_found::_exnarrow(ex) != NULL) {
				//
				// This is not an error condition. We are done.
				// return.
				//
				(void) printf("The ns_test object is not "
				    "found\n");
				return (2);
			} else {
				(void) printf("Exception encountered during "
				    "ns_test object lookup, "
				    "id = %d\n", ex->exception_enum());
				retval = 1;
			}
			env.clear();
	}

	ASSERT(!CORBA::is_nil(obj_v));
	ns_test::ns_test_obj_ptr myobj_p =
	    ns_test::ns_test_obj::_narrow(obj_v);
	myobj_p->display_obj(env);
	(void) printf("The object address = %p\n", myobj_p);

	if ((ex = env.exception()) != NULL) {
		(void) printf("Exception occurred while doing invocation\n");
		if (CORBA::INV_OBJREF::_exnarrow(ex) != NULL) {
			(void) printf("Invalid obj ref exception\n");
		} else {
			(void) printf("Some other excep\n");
		}
		return (1);
	}

	return (retval);
}
