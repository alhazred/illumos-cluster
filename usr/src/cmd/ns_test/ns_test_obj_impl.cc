//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
#pragma ident	"@(#)ns_test_obj_impl.cc	1.2	08/07/16 SMI"

#include <ns_test_obj_impl.h>

ns_test_obj_impl::ns_test_obj_impl() { int a = 1; } //lint !e529
ns_test_obj_impl::~ns_test_obj_impl() { int b = 1; } //lint !e529


void
ns_test_obj_impl::_unreferenced(unref_t cookie)
{
	(void) printf("Unreference called \n");
	if (_last_unref(cookie)) {
		delete this;
	} else {
		ASSERT(0);
	}

}

void
ns_test_obj_impl::display_obj(CORBA::Environment& e)
{
	(void) printf("From display object ns_test_obj\n");
} //lint !e715

bool
ns_test_obj_impl:: display_zone_id(CORBA::Environment& e)
{
	(void) printf("From display_zone_id ns_test_obj\n");

	return (true);
} //lint !e715
