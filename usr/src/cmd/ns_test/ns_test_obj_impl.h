//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef _NS_TEST_OBJ_IMPL_H_
#define	_NS_TEST_OBJ_IMPL_H_

#pragma ident	"@(#)ns_test_obj_impl.h	1.2	08/07/16 SMI"

#include <orb/object/adapter.h>
#include <h/ns_test.h>

class ns_test_obj_impl : public McServerof <ns_test::ns_test_obj> {

public:
	ns_test_obj_impl();
	~ns_test_obj_impl();

	void _unreferenced(unref_t);

	//
	// Interface definitions
	//

	void display_obj(CORBA::Environment&);
	bool display_zone_id(CORBA::Environment&);
};

#endif /* _NS_TEST_OBJ_IMPL_H_ */
