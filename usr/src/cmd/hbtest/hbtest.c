/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)hbtest.c	1.4	08/05/20 SMI"

#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <stdio.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <stropts.h>
#include <pthread.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <termio.h>
#include <sys/sysmacros.h>
#include <sys/hbxctl.h>

/*
 * Debug flag and related macros
 */
static int DEBUG = 0;

#define	HBTEST_ARG_MAX 10
#define	HBTEST_ARG_MIN 4

#ifdef linux
#define	HBE_DEV "/dev/hbtdrv0"
#else
#define	HBE_DEV "/dev/hbxdrv"
#endif

#ifdef linux
#define	HBR_DEV "/dev/hbrdrv0"
#else
#define	HBR_DEV "/dev/hbxdrv"
#endif

in_addr_t src_addr_1;
in_addr_t src_addr_2;
in_addr_t broad_addr_1;
in_addr_t broad_addr_2;

#define	IF_NAME_SIZE 16
char if_name_1[IF_NAME_SIZE];
char if_name_2[IF_NAME_SIZE];

#define	SRC_ADDR_1	src_addr_1
#define	SRC_ADDR_2	src_addr_2

#define	BROAD_ADDR_1	broad_addr_1
#define	BROAD_ADDR_2	broad_addr_2

/* devices */
static unsigned int dev_nb;
static hbx_ioc_list_dev_t ldev;

/* hb driver */
static int sd;

/* thread */
static pthread_t evt_thread;

/* ==================================================================== */
/*									*/
/* EVENT MANAGEMENT							*/
/*									*/
/* ==================================================================== */

void *evtlisten(void *arg);

/* -------------------------------------------------------------------- */
/*									*/
/* start_evt								*/
/*									*/
/* -------------------------------------------------------------------- */

void
start_evt()
{
	pthread_attr_t sys_attr;
	if (pthread_attr_init(&sys_attr) != 0) {
		(void) fprintf(stderr, "pthread_attr_init failed");
		exit(1);
	}
	(void) pthread_attr_setscope(&sys_attr, PTHREAD_SCOPE_SYSTEM);
	if (pthread_create(&evt_thread,
	    &sys_attr, evtlisten, (void *)NULL) != 0) {
		(void) fprintf(stderr, "pthread_create failed");
		exit(1);
	}
}

/* -------------------------------------------------------------------- */
/*									*/
/* evtprint								*/
/*									*/
/* -------------------------------------------------------------------- */

void
evtprint(hbx_event_t *evt)
{
#ifdef Solaris
	(void) printf("(%lld)EVT dev %d,%d id %d %s: src %x"
	    " state %s delay %d\n",
	    evt->time,
	    major((dev_t)evt->dev),
	    minor((dev_t)evt->dev),
	    evt->id,
	    hbx_events[evt->type],
	    evt->src,
	    hbx_host_status[evt->state],
	    evt->delay);
#endif
}

/* -------------------------------------------------------------------- */
/*									*/
/* evtlisten								*/
/*									*/
/* -------------------------------------------------------------------- */

/*ARGSUSED*/
void *
evtlisten(void *arg)
{
#ifdef Solaris
	char bufc[1024];
	char bufd[1024];
	struct strbuf ctlbuf, databuf;
	int flags = 0;
#endif
#ifdef linux
	hbx_event_t evt_buf;
#endif
	hbx_event_t *evt;
	int ret;

	while (1) {
#ifdef Solaris
		(void) memset(bufd, 0, sizeof (bufd));
		ctlbuf.buf = (char *)bufc;
		ctlbuf.maxlen = sizeof (bufc);
		ctlbuf.len = 0;
		databuf.buf = (char *)bufd;
		databuf.maxlen = sizeof (bufd);
		databuf.len = 0;
		ret = getmsg(sd, &ctlbuf, &databuf, &flags);
		if (ret != 0) {
			(void) printf("getmsg ret %d error %d\n", ret,
			    errno); /*lint !e746 */
			(void) printf("ctlbuf.len %d databuf.len %d\n",
			    ctlbuf.len, databuf.len);
		}
		evt = (hbx_event_t *)&bufd[0];
#endif
#ifdef linux
		memset(&evt_buf, 0, sizeof (hbx_event_t));
		ret = read(sd, (char*)&evt_buf, sizeof (evt_buf));

		if (ret < 0) {
			perror("read");
		}

		int evt_count = ret / sizeof (hbx_event_t);
		int i;
		(void) printf("< %d > events have been read\n", evt_count);
		for (i = 0; i < evt_count; i++) {
			evt = (hbx_event_t *)&evt_buf;
#endif
			if (evt->type == EVT_DEV_ADDED ||
			    evt->type == EVT_DEV_REMOVED) {
#ifdef Solaris
				(void) printf("(%lld)EVT dev %d,%d id %d %s\n",
				    evt->time,
				    major((dev_t)evt->dev),
				    minor((dev_t)evt->dev),
				    evt->id,
				    hbx_events[evt->type]);
#endif
#ifdef linux
				(void) printf("()EVT dev %p id %d %s\n",
				    evt->dev,
				    evt->id,
				    hbx_events[evt->type]);
#endif
			} else {
				evtprint(evt);
				if (evt->type == EVT_HOST_DOWN) {
					(void) printf(
					    "-> HOST %x id %d is DOWN\n",
					    evt->src, evt->node_id);
				}
				if (evt->type == EVT_HOST_DETECTED) {
					(void) printf(
					    "-> HOST %x id %d has been "
					    "DETECTED\n",
					    evt->src, evt->node_id);
				}
				if (evt->type == EVT_HOST_UP) {
					(void) printf(
					    "-> HOST %x id %d is UP\n",
					    evt->src, evt->node_id);
				}
			}
#ifdef linux
		}
#endif
	}
}

/* ==================================================================== */
/*									*/
/* DRIVER ACCESS							*/
/*									*/
/* ==================================================================== */

/* -------------------------------------------------------------------- */
/*									*/
/* hb_print_error							*/
/*									*/
/* -------------------------------------------------------------------- */

void
hb_print_error(int cmd, int ret) {
#ifdef linux
	int cmd_ind = _IOC_NR(cmd) - 1;
#endif
#ifdef Solaris
	int cmd_ind = cmd - HBX_IOC - 1;
#endif
	(void) printf("ioctl cmd %s ret %d error %d\n",
#ifdef linux
	    hbx_ioc[cmd_ind],
#endif
#ifdef Solaris
	    hbx_ioc[cmd_ind],
#endif
	    ret, errno);
	perror("ERRNO");
}

/* -------------------------------------------------------------------- */
/*									*/
/* hb_get_devices							*/
/*									*/
/* get the information for all devices					*/
/*									*/
/* -------------------------------------------------------------------- */

void
hb_get_devices(void)
{
	int ret;
	unsigned int i;
	struct strioctl io;
	hbx_dev_t *pdev;
	hbx_ioc_get_dev_t gdev;
	hbx_ioc_set_id_t sdev;

	(void) printf("\n--------- Getting devices --------\n");

	/*
	 * get the list of devices with the hbxmod pushed on
	 * first call to get the number, second call to get the info
	 */

	ldev.dev_nb = 0;
	ldev.dev = NULL;

#ifdef Solaris
	io.ic_cmd = HBX_IOC_LIST_DEV;
	io.ic_timout = -1;
	io.ic_len = sizeof (ldev);
	io.ic_dp = (char *)&ldev;
	ret = ioctl(sd, I_STR, &io);
#endif
#ifdef linux
	ret = ioctl(sd, HBX_IOC_LIST_DEV, &(ldev), NULL);
#endif
	if (ret != 0) {
		hb_print_error(HBX_IOC_LIST_DEV, ret);
		exit(1);
	}
	dev_nb = ldev.real_dev_nb;

	pdev = (hbx_dev_t *)malloc(dev_nb * sizeof (hbx_dev_t));
	if (pdev == NULL) {
		exit(1);
	}
	ldev.dev_nb = dev_nb;
	ldev.dev = pdev;

#ifdef Solaris
	io.ic_cmd = HBX_IOC_LIST_DEV;
	io.ic_timout = -1;
	io.ic_len = sizeof (ldev);
	io.ic_dp = (char *)&ldev;
	ret = ioctl(sd, I_STR, &io);
#endif
#ifdef linux
	ret = ioctl(sd, HBX_IOC_LIST_DEV, &(ldev), NULL);
#endif
	if (ret != 0) {
		hb_print_error(HBX_IOC_LIST_DEV, ret);
		exit(1);
	}
	dev_nb = ldev.real_dev_nb;
	(void) printf("\n--------- hbxdrv configured devices ----------\n");
	(void) printf("real dev nb %d\n", dev_nb);

	for (i = 0; i < dev_nb; i++) {
#ifdef Solaris
		(void) printf("\n(%d) dev %p maj %d min %d\n",
		    i, (void *)ldev.dev[i],
		    major((dev_t)ldev.dev[i]),
		    minor((dev_t)ldev.dev[i]));
#endif
#ifdef linux
		(void) printf("\n(%d) dev %p\n", i,
		    (hbx_dev_t)(ldev.dev[i]));
#endif
		sdev.dev = ldev.dev[i]; /* put our own id */
		sdev.id = i;		/* setup a user id coockie */
#ifdef Solaris
		io.ic_cmd = HBX_IOC_SET_ID;
		io.ic_timout = -1;
		io.ic_len = sizeof (sdev);
		io.ic_dp = (char *)&sdev;
		ret = ioctl(sd, I_STR, &io);
#endif
#ifdef linux
		ret = ioctl(sd, HBX_IOC_SET_ID, &sdev, NULL);
#endif
		if (ret < 0) {
			hb_print_error(HBX_IOC_SET_ID, ret);
			exit(1);
		}

		(void) memset(&gdev, 0, sizeof (hbx_ioc_get_dev_t));
		gdev.dev = ldev.dev[i];
#ifdef Solaris
		io.ic_cmd = HBX_IOC_GET_DEV;
		io.ic_timout = -1;
		io.ic_len = sizeof (gdev);
		io.ic_dp = (char *)&gdev;
		ret = ioctl(sd, I_STR, &io);
#endif
#ifdef linux
		ret = ioctl(sd, HBX_IOC_GET_DEV, &gdev, NULL);
#endif
		if (ret < 0) {
			hb_print_error(HBX_IOC_GET_DEV, ret);
			exit(1);
		}

		(void) printf("id %d\n", gdev.id);
		(void) printf("host_nb %d\n", gdev.host_nb);
		(void) printf("host_coll_nb %d\n", gdev.host_coll_nb);
		(void) printf("max_hosts %d\n", gdev.max_hosts);
#ifdef Solaris
		(void) printf("mac_info %d\n", gdev.mac_info);
		(void) printf("mac_version %d\n", gdev.mac_version);
		(void) printf("mac_ok %d\n", gdev.mac_ok);
		(void) printf("mac_type %d\n", gdev.mac_type);
		(void) printf("mac_len %d\n", gdev.mac_len);
		(void) printf("mac_sap_len %d\n", gdev.mac_sap_len);
		(void) printf("mac_sap_off %d\n", gdev.mac_sap_off);
		(void) printf("mac_sap %x.%x\n", gdev.mac_sap[0],
		    gdev.mac_sap[1]);
#endif
		(void) printf("src %x\n", gdev.src);
		(void) printf("dst %x\n", gdev.dst);
		(void) printf("mac_src %x.%x.%x.%x.%x.%x\n",
		    gdev.mac_src[0],
		    gdev.mac_src[1],
		    gdev.mac_src[2],
		    gdev.mac_src[3],
		    gdev.mac_src[4],
		    gdev.mac_src[5]);
		(void) printf("mac_bca %x.%x.%x.%x.%x.%x\n",
		    gdev.mac_bca[0],
		    gdev.mac_bca[1],
		    gdev.mac_bca[2],
		    gdev.mac_bca[3],
		    gdev.mac_bca[4],
		    gdev.mac_bca[5]);
		(void) printf("mac_dst %x.%x.%x.%x.%x.%x\n",
		    gdev.mac_dst[0],
		    gdev.mac_dst[1],
		    gdev.mac_dst[2],
		    gdev.mac_dst[3],
		    gdev.mac_dst[4],
		    gdev.mac_dst[5]);
		(void) printf("state %s\n", hbx_emit_status[gdev.state]);
		(void) printf("in    %x\n", gdev.in);
		(void) printf("mode  %s %s\n",
		    (gdev.mode & HBX_EMT_MODE_ON ?
		    hbx_mode[HBX_EMT_MODE_ON]:""),
		    (gdev.mode & HBX_RCV_MODE_ON ?
		    hbx_mode[HBX_RCV_MODE_ON]:""));
	}

}

/* -------------------------------------------------------------------- */
/*									*/
/* hb_get_hosts								*/
/*									*/
/* get the information for all hosts					*/
/* (currently limited to one remote host)				*/
/*									*/
/* -------------------------------------------------------------------- */

void
hb_get_hosts(void)
{
	unsigned int i;
	unsigned int j;
	int ret;
	unsigned int host_nb;
	struct strioctl io;
	ipaddr_t *phost;
	hbx_ioc_get_host_t ghost;
	hbx_ioc_list_host_t lhost;

	/* get the initial status */
	for (i = 0; i < dev_nb; i++) {
		lhost.host_nb = 0;
		lhost.dev = ldev.dev[i];
		lhost.host = NULL;
#ifdef Solaris
		io.ic_cmd = HBX_IOC_LIST_HOST;
		io.ic_timout = -1;
		io.ic_len = sizeof (lhost);
		io.ic_dp = (char *)&lhost;
		ret = ioctl(sd, I_STR, &io);
#endif
#ifdef linux
		ret = ioctl(sd, HBX_IOC_LIST_HOST, &lhost, NULL);
#endif
		if (ret != 0) {
			hb_print_error(HBX_IOC_LIST_HOST, ret);
			exit(1);
		}
		host_nb = lhost.real_host_nb;
		(void) printf("\n----------- hb hosts ----------\n");
#ifdef Solaris
		(void) printf("dev %d, %d: host nb: found %d\n",
		    major((dev_t)ldev.dev[i]),
		    minor((dev_t)ldev.dev[i]),
		    host_nb);
#endif
#ifdef linux
		(void) printf("dev: %p host nb: found %d\n",
		    ldev.dev[i], host_nb);
#endif
		phost = (ipaddr_t *)
		    malloc(host_nb * sizeof (ipaddr_t));
		if (phost == NULL) {
			exit(1);
		}
		lhost.host_nb = host_nb;
		lhost.dev = ldev.dev[i];
		lhost.host = phost;
#ifdef Solaris
		io.ic_cmd = HBX_IOC_LIST_HOST;
		io.ic_timout = -1;
		io.ic_len = sizeof (lhost);
		io.ic_dp = (char *)&lhost;
		ret = ioctl(sd, I_STR, &io);
#endif
#ifdef linux
		ret = ioctl(sd,  HBX_IOC_LIST_HOST, &lhost, NULL);
#endif
		if (ret != 0) {
			hb_print_error(HBX_IOC_LIST_HOST, ret);
			exit(1);
		}
		host_nb = lhost.real_host_nb;
#ifdef Solaris
		(void) printf("dev %d, %d: host nb: real %d\n",
		    major((dev_t)ldev.dev[i]),
		    minor((dev_t)ldev.dev[i]),
		    lhost.host_nb);
#endif
#ifdef linux
		(void) printf("dev: %p host nb: found %d\n",
		    ldev.dev[i], host_nb);
#endif
		for (j = 0; j < host_nb; j++) {
#ifdef Solaris
			(void) printf("\n(%d) dev %d,%d has detected host %x\n",
			    i,
			    major((dev_t)ldev.dev[i]),
			    minor((dev_t)ldev.dev[i]),
			    lhost.host[j]);
#endif
#ifdef linux
			(void) printf("\n(%d) dev %p has detected host %x\n",
			    i, ldev.dev[i], lhost.host[j]);
#endif
			ghost.dev = ldev.dev[i];
			ghost.host = lhost.host[j];
			ghost.node_id = 0;
#ifdef Solaris
			io.ic_cmd = HBX_IOC_GET_HOST;
			io.ic_timout = -1;
			io.ic_len = sizeof (ghost);
			io.ic_dp = (char *)&ghost;
			ret = ioctl(sd, I_STR, &io);
#endif
#ifdef linux
			ret = ioctl(sd, HBX_IOC_GET_HOST, &ghost, NULL);
#endif
			if (ret < 0) {
				hb_print_error(HBX_IOC_GET_HOST, ret);
				exit(1);
			}
			(void) printf("--> state %s delay %d id %d\n",
			    hbx_host_status[ghost.state],
			    ghost.delay, ghost.node_id);
			if (ghost.state == HOST_STATE_UP) {
				(void) printf("---- HOST ALREADY UP id %d\n",
				    ghost.node_id);
			}
		}
		free(phost);
	}
}

/* -------------------------------------------------------------------- */
/*									*/
/* hb_start								*/
/*									*/
/* start the heartbeat on one configured devices			*/
/*									*/
/* -------------------------------------------------------------------- */

void
hb_start(unsigned int nb_dev, enum hbx_mode_t mode)
{
	int ret;
	hbx_ioc_emit_cmd_t emit;
	struct strioctl io;
	unsigned int i;

	(void) printf("\n--------- Starting hb emission ---------\n");

	for (i = 0; i < nb_dev; i++) {
		(void) printf("device (%d): %p\n", i, ldev.dev[i]);
		emit.cmd = HBX_EMIT_CTL_START;
		emit.dev = ldev.dev[i];
		emit.mode = mode;
#ifdef Solaris
		io.ic_cmd = HBX_IOC_EMIT_CTL;
		io.ic_timout = -1;
		io.ic_len = sizeof (emit);
		io.ic_dp = (char *)&emit;
		ret = ioctl(sd, I_STR, &io);
#endif
#ifdef linux
		ret = ioctl(sd, HBX_IOC_EMIT_CTL, &emit, NULL);
#endif
		if (ret < 0) {
			hb_print_error(HBX_IOC_EMIT_CTL, ret);
		}
	}
}

/* -------------------------------------------------------------------- */
/*									*/
/* hb_set_frequency							*/
/*									*/
/* set the info regarding heartbeat					*/
/*									*/
/* -------------------------------------------------------------------- */

void
hb_set_frequency(unsigned int delay, unsigned int numhb)
{
	int ret;
	struct strioctl io;
	hbx_ioc_freq_t freq;

	/*
	 * Set the frequency
	 */
	freq.delay = delay;
	freq.numhb = numhb;
	(void) printf("\n--------- hb frequency --------\n");
	(void) printf("Setting frequency: delay=%d, numhb=%d\n",
	    freq.delay, freq.numhb);
#ifdef Solaris
	io.ic_cmd = HBX_IOC_EMIT_SET_FREQ;
	io.ic_timout = -1;
	io.ic_len = sizeof (freq);
	io.ic_dp = (char *)&freq;
	ret = ioctl(sd, I_STR, &io);
#endif
#ifdef linux
	ret = ioctl(sd, HBX_IOC_EMIT_SET_FREQ, &freq, NULL);
#endif
	if (ret < 0) {
		hb_print_error(HBX_IOC_EMIT_SET_FREQ, ret);
		exit(1);
	}
}

/* -------------------------------------------------------------------- */
/*									*/
/* hb_get_frequency							*/
/*									*/
/* get the info regarding heartbeat					*/
/*									*/
/* -------------------------------------------------------------------- */

void
hb_get_frequency(void)
{
	int ret;
	struct strioctl io;
	hbx_ioc_freq_t freq;

	bzero((void *)&freq, sizeof (freq));

#ifdef Solaris
	io.ic_cmd = HBX_IOC_EMIT_GET_FREQ;
	io.ic_timout = -1;
	io.ic_len = sizeof (freq);
	io.ic_dp = (char *)&freq;
	ret = ioctl(sd, I_STR, &io);
#endif
#ifdef linux
	ret = ioctl(sd, HBX_IOC_EMIT_GET_FREQ, &freq, NULL);
#endif
	if (ret < 0) {
		hb_print_error(HBX_IOC_EMIT_GET_FREQ, ret);
		exit(1);
	}
	(void) printf("Getting frequency: delay %u, numhb %u, set %u\n",
	    freq.delay,
	    freq.numhb,
	    freq.set);
	if (freq.set != 1) {
		(void) printf("ERROR freq not set\n");
		exit(1);
	}
}

/* -------------------------------------------------------------------- */
/*									*/
/* hb_set_ident								*/
/*									*/
/* set the identity parameters						*/
/*									*/
/* -------------------------------------------------------------------- */

void
hb_set_ident(hbx_ioc_ident_t *ident_addr)
{
	int ret;
	struct strioctl io;

	(void) printf("\n--------- identity --------\n");
	(void) printf("Setting identity parameters: %d\n", ident_addr->node_id);
	(void) printf("Setting identity parameters: 0x%x\n", ident_addr->clid);
	(void) printf("Setting identity parameters: 0x%x\n",
	    ident_addr->version);

#ifdef Solaris
	io.ic_cmd = HBX_IOC_SET_IDENT;
	io.ic_timout = -1;
	io.ic_len = sizeof (hbx_ioc_ident_t);
	io.ic_dp = (char *)ident_addr;
	ret = ioctl(sd, I_STR, &io);
#endif
#ifdef linux
	ret = ioctl(sd, HBX_IOC_SET_IDENT, ident_addr, NULL);
#endif
	if (ret < 0) {
		hb_print_error(HBX_IOC_SET_IDENT, ret);
		exit(1);
	}
}


void
hb_set_auth(hbx_ioc_auth_t *auth_addr)
{
	int		ret;
	struct strioctl	io;

	(void) printf("\n--------- authentication --------\n");
	(void) printf("Setting authentication parameter: %s\n",
	    auth_addr->pkey);
	(void) printf("Setting authentication parameter: %s\n",
	    auth_addr->alt_pkey);
	(void) printf("Setting authentication parameter: %s\n",
	    hbx_auth_cmd[auth_addr->cmd]);

#ifdef Solaris
	io.ic_cmd = HBX_IOC_SET_AUTH;
	io.ic_timout = -1;
	io.ic_len = sizeof (hbx_ioc_auth_t);
	io.ic_dp = (char *)auth_addr;
	ret = ioctl(sd, I_STR, &io);
#endif
#ifdef linux
	ret = ioctl(sd, HBX_IOC_SET_AUTH, auth_addr, NULL);
#endif
	if (ret < 0) {
		hb_print_error(HBX_IOC_SET_AUTH, ret);
		exit(1);
	}
}

/* -------------------------------------------------------------------- */
/*									*/
/* hb_get_ident								*/
/*									*/
/* get the identity parameters						*/
/*									*/
/* -------------------------------------------------------------------- */

void
hb_get_ident(hbx_ioc_ident_t *ident_addr)
{
	int ret;
	struct strioctl io;

	bzero((void *)ident_addr, sizeof (hbx_ioc_ident_t));
#ifdef Solaris
	io.ic_cmd = HBX_IOC_GET_IDENT;
	io.ic_timout = -1;
	io.ic_len = sizeof (hbx_ioc_ident_t);
	io.ic_dp = (char *)ident_addr;
	ret = ioctl(sd, I_STR, &io);
#endif
#ifdef linux
	ret = ioctl(sd, HBX_IOC_GET_IDENT, ident_addr, NULL);
#endif
	if (ret < 0) {
		hb_print_error(HBX_IOC_GET_IDENT, ret);
		exit(1);
	}
	(void) printf("\n--------- identity --------\n");
	(void) printf("Getting identity parameters: %d\n", ident_addr->node_id);
	(void) printf("Getting identity parameters: 0x%x\n", ident_addr->clid);
	(void) printf("Getting identity parameters: 0x%x\n",
	    ident_addr->version);
	(void) printf("Getting identity parameters: %d\n", ident_addr->set);
}

/* -------------------------------------------------------------------- */
/*									*/
/* hb_set_debug								*/
/*									*/
/* set the debug flag value to 0 or 1					*/
/*									*/
/* -------------------------------------------------------------------- */

void
hb_set_debug()
{
	int ret;
	struct strioctl io;

	(void) printf("\n--------- debug flag --------\n");
	(void) printf("Setting debug flag: %d\n", DEBUG);

#ifdef Solaris
	io.ic_cmd = HBX_IOC_SET_DEBUG;
	io.ic_timout = -1;
	io.ic_len = sizeof (int);
	io.ic_dp = (char*)&(DEBUG);
	ret = ioctl(sd, I_STR, &io);
#endif
#ifdef linux
	ret = ioctl(sd, HBX_IOC_SET_DEBUG, &(DEBUG), NULL);
#endif
	if (ret < 0) {
		hb_print_error(HBX_IOC_SET_DEBUG, ret);
		exit(1);
	}
}

#ifdef linux
/* -------------------------------------------------------------------- */
/*									*/
/* hb_remove_devices							*/
/*									*/
/* remove the devices handled by the driver (linux only)		*/
/*									*/
/* -------------------------------------------------------------------- */
void
hb_remove_devices()
{
	int ret;
	int i;

	(void) printf("\n--------- Removing devices --------\n");

	for (i = 0; i < ldev.real_dev_nb; i++) {
		(void) printf("device (%d): %p\n", i, ldev.dev[i]);
		ret = ioctl(sd, HBX_IOC_DEL_DEV, &ldev.dev[i], NULL);
		if (ret < 0) {
			hb_print_error(HBX_IOC_DEL_DEV, ret);
			exit(1);
		}
	}
}

/* -------------------------------------------------------------------- */
/*									*/
/* hb_set_devices							*/
/*									*/
/* set the devices handled by the driver (linux only)			*/
/*									*/
/* -------------------------------------------------------------------- */

void
hb_set_devices(int nb_if)
{
	int ret;
	int i;

	(void) printf("\n--------- Setting devices --------\n");

	for (i = 1; i <= nb_if; i++) {
		if (i == 1) {
			(void) printf("Setting device: %s\n", if_name_1);
			ret = ioctl(sd, HBX_IOC_ADD_DEV, &if_name_1, NULL);
			if ((ret < 0) && (errno != EEXIST)) {
				hb_print_error(HBX_IOC_ADD_DEV, ret);
				exit(1);
			}
		}
		if (i == 2) {
			(void) printf("Setting device: %s\n", if_name_2);
			ret = ioctl(sd, HBX_IOC_ADD_DEV, &if_name_2, NULL);
			if ((ret < 0) && (errno != EEXIST)) {
				hb_print_error(HBX_IOC_ADD_DEV, ret);
				exit(1);
			}
		}
	}
}
#endif

/* -------------------------------------------------------------------- */
/*									*/
/* hb_reset								*/
/*									*/
/* stop the heartbeat packets emission on the device configured		*/
/* reset the heartbeat parameters					*/
/*									*/
/* -------------------------------------------------------------------- */
void
hb_reset(int nb_dev)
{
	hbx_ioc_emit_cmd_t emit;
	int ret;
	int i;
	struct strioctl io;

	(void) printf("\n--------- Resetting emit params --------\n");

	for (i = 0; i < nb_dev; i++) {
		(void) printf("device (%d): %p\n", i, ldev.dev[i]);
		emit.cmd = HBX_EMIT_CTL_RESET;
		emit.dev = ldev.dev[i];
#ifdef Solaris
		io.ic_cmd = HBX_IOC_EMIT_CTL;
		io.ic_timout = -1;
		io.ic_len = sizeof (emit);
		io.ic_dp = (char *)&emit;
		ret = ioctl(sd, I_STR, &io);
#endif
#ifdef linux
		ret = ioctl(sd, HBX_IOC_EMIT_CTL, &emit, NULL);
#endif
		if (ret < 0) {
			hb_print_error(HBX_IOC_EMIT_CTL, ret);
		}
	}
}

/* -------------------------------------------------------------------- */
/*									*/
/* hb_stop								*/
/*									*/
/* stop the heartbeat packets emission on the device configured		*/
/*									*/
/* -------------------------------------------------------------------- */
void
hb_stop(int nb_dev)
{
	hbx_ioc_emit_cmd_t emit;
	int ret;
	int i;
	struct strioctl io;

	(void) printf("\n--------- Stopping hb emission --------\n");

	for (i = 0; i < nb_dev; i++) {
		(void) printf("device (%d): %p\n", i, ldev.dev[i]);
		emit.cmd = HBX_EMIT_CTL_STOP;
		emit.dev = ldev.dev[i];
#ifdef Solaris
		io.ic_cmd = HBX_IOC_EMIT_CTL;
		io.ic_timout = -1;
		io.ic_len = sizeof (emit);
		io.ic_dp = (char *)&emit;
		ret = ioctl(sd, I_STR, &io);
#endif
#ifdef linux
		ret = ioctl(sd, HBX_IOC_EMIT_CTL, &emit, NULL);
#endif
		if (ret < 0) {
			hb_print_error(HBX_IOC_EMIT_CTL, ret);
		}
	}
}

/*
 * ***************************************************************
 *
 * main
 *
 * ************************************************************** */

static char usage[] = \
		"\n-n <netdev-0 ip-src> [-n <netdev-1 ip-src>] "
		"\n-b <netdev-0 ip-dst> [-b <netdev_1 ip-dst>] "
		"\n-N <netdev-0 name> [-N <netdev-1 name>]"
		"\n-d <detection-delay> -r <retry-count>"
		"\n[-D] [-M <mode>]"
		"\n<mode> = e | r | x";

int
main(int argc, char **argv)
{
	int  ret, nb_nic, nb_bcst, nb_arg, nb_if;
	int c;
	unsigned int  detection_time = 0;
	unsigned int  retry_count = 0;
	struct strioctl	io;

	hbx_ioc_emit_cmd_t	emit;
	hbx_ioc_ident_t		ident;
	hbx_ioc_auth_t		auth;
	hbx_node_id_t		node_id = 10;
	char			device_name[30];
	enum hbx_mode_t		mode = HBX_MODE_OFF;

	bzero((void *)&device_name[0], sizeof (device_name));
	bzero((void *)&ident, sizeof (hbx_ioc_ident_t));
	bzero((void *)&auth, sizeof (hbx_ioc_auth_t));
	bzero((void *)if_name_1, sizeof (if_name_1));
	bzero((void *)if_name_2, sizeof (if_name_2));
	(void) strcpy(&auth.pkey[0], (char *)"secret key");

	nb_nic = 0;
	nb_bcst = 0;
	nb_arg = 0;
	nb_if = 0;

	while ((c = getopt(argc, argv, "n:b:d:r:t:N:B:M:D")) != EOF) {
		switch (c) {
		case 'M':	/* mode x | e | r */
			if (strcmp(optarg, "e") == 0) {
				mode = HBX_EMT_MODE_ON;
				(void) strcpy(&device_name[0], HBE_DEV);
			} else if (strcmp(optarg, "r") == 0) {
				mode = HBX_RCV_MODE_ON;
				(void) strcpy(&device_name[0], HBR_DEV);
			} else if (strcmp(optarg, "x") == 0) {
				mode = HBX_EMT_MODE_ON | HBX_RCV_MODE_ON;
				(void) strcpy(&device_name[0], HBE_DEV);
			} else {
				(void) printf("%s %s\n", argv[0], usage);
				exit(1);
			}
			break;

		case 'D':	/* debug */
			DEBUG = 1;
			break;

		case 'n':	/* nic address */
			nb_nic++;
			if (nb_nic == 1) {
				src_addr_1 = htonl(inet_addr(optarg));
				nb_arg++;
			} else if (nb_nic == 2) {
				src_addr_2 = htonl(inet_addr(optarg));
				nb_arg++;
			} else {
				(void) printf("%s %s\n", argv[0], usage);
				exit(1);
			}
			break;

		case 'b':	/* broadcast address */
			nb_bcst++;
			if (nb_bcst == 1) {
				broad_addr_1 = ntohl(inet_addr(optarg));
				nb_arg++;
			} else if (nb_bcst == 2) {
				broad_addr_2 = ntohl(inet_addr(optarg));
				nb_arg++;
			} else {
				(void) printf("%s %s\n", argv[0], usage);
				exit(1);
			}
			break;

		case 'd':	/* probe delay */
			nb_arg++;
			detection_time = (unsigned int) atol(optarg);
			break;

		case 'r':	/* retry count */
			nb_arg++;
			retry_count = (unsigned int) atol(optarg);
			break;

		case 'N':	/* net device */
#ifdef Solaris
			(void) printf("Option not supported on Solaris: -N\n");
			exit(1);
#endif
			nb_if++;
			if (nb_if == 1) {
				(void) strcpy(if_name_1, optarg);
			} else if (nb_if == 2) {
				(void) strcpy(if_name_2, optarg);
			} else {
				(void) printf("%s %s\n", argv[0], usage);
				exit(1);
			}
			break;

		case 'k':

			(void) strcpy(auth.pkey, optarg);
			break;

		case '?':
		default:
			(void) printf("%s %s\n", argv[0], usage);
			exit(1);
		}
	}

	/* check the arguments */
	if ((nb_arg < HBTEST_ARG_MIN) || (nb_arg > HBTEST_ARG_MAX)) {
		(void) printf("%s %s\n", argv[0], usage);
		exit(1);
	}

	(void) printf("\n------- input params ----------\n");
#ifdef linux
	(void) printf("if name  1: %s\n", if_name_1);
	(void) printf("if name  2: %s\n", if_name_2);
#endif
	(void) printf("src addr 1: %08x\n", src_addr_1);
	(void) printf("src addr 2: %08x\n", src_addr_2);
	(void) printf("dst addr 1: %08x\n", broad_addr_1);
	(void) printf("dst addr 2: %08x\n", broad_addr_2);
	(void) printf("detection : %d\n", detection_time);
	(void) printf("retry     : %d\n", retry_count);
	if (mode & HBX_RCV_MODE_ON && mode & HBX_EMT_MODE_ON) {
		(void) printf("mode      : %s\n", "XXX_MODE_ON");
	} else {
		(void) printf("mode      : %s\n", hbx_mode[mode]);
	}
	/* open the driver */
	if (mode == HBX_MODE_OFF) {
		mode = HBX_EMT_MODE_ON;
		(void) strcpy(&device_name[0], HBE_DEV);
	}
	sd = open(device_name, O_RDWR);
	if (sd == -1) {
		(void) printf("Error opening device %s\n", device_name);
		perror("open");
		(void) close(sd);
		return (-1);
	}

	hb_set_debug();

	/* launch the event listener thread */
/* 	start_evt(); */

#ifdef linux
	if (nb_if != 0) {
		hb_set_devices(nb_if);
	}
#endif

	hb_get_devices();

	if (mode & HBX_EMT_MODE_ON) {
		hb_set_frequency(detection_time, retry_count);
		hb_get_frequency();
	}

	ident.clid = 0x12345678;
	ident.version = 0xabcd;
	ident.node_id = node_id;

	hb_set_ident(&ident);
	hb_get_ident(&ident);

	hb_set_auth(&auth);
/* 	hb_get_auth(&auth); */

	(void) printf("\n---------- Setting emit params for nic0 ---------\n");
	(void) printf("Setting: %x\n", SRC_ADDR_1);
	(void) printf("Setting: %x\n", BROAD_ADDR_1);
	/* set params for nic0 */
	emit.cmd = HBX_EMIT_CTL_SET_PARAMS;
	emit.dev = ldev.dev[0];

	emit.src = SRC_ADDR_1;
	emit.dst = BROAD_ADDR_1;
	emit.mode = mode;
#ifdef Solaris
	emit.ttl = HBX_IP_TTL;
	emit.udp_dst_port = HBX_UDP_DST_PORT;
	io.ic_cmd = HBX_IOC_EMIT_CTL;
	io.ic_timout = -1;
	io.ic_len = sizeof (emit);
	io.ic_dp = (char *)&emit;
	ret = ioctl(sd, I_STR, &io);
#endif
#ifdef linux
	ret = ioctl(sd, HBX_IOC_EMIT_CTL, &emit, NULL);
#endif
	if (ret < 0) {
		hb_print_error(HBX_IOC_EMIT_CTL, ret);
		exit(1);
	}

	if (nb_nic == 2) {
		(void) printf(
		    "\n---------- Setting emit params for nic1 ---------\n");
		(void) printf("Setting: %x\n", SRC_ADDR_2);
		(void) printf("Setting: %x\n", BROAD_ADDR_2);
		emit.cmd = HBX_EMIT_CTL_SET_PARAMS;
		emit.dev = ldev.dev[1];

		emit.src = SRC_ADDR_2;
		emit.dst = BROAD_ADDR_2;
		emit.mode = mode;
#ifdef Solaris
		io.ic_cmd = HBX_IOC_EMIT_CTL;
		io.ic_timout = -1;
		io.ic_len = sizeof (emit);
		io.ic_dp = (char *)&emit;
		ret = ioctl(sd, I_STR, &io);
#endif
#ifdef linux
		ret = ioctl(sd, HBX_IOC_EMIT_CTL, &emit, NULL);
#endif
		if (ret < 0) {
			hb_print_error(HBX_IOC_EMIT_CTL, ret);
		}
	}

	/* get the list of already present nodes */
#ifdef Solaris
	hb_get_hosts();
#endif
	hb_start(dev_nb, mode);

	(void) printf("Exiting from hbtest\n");

	pthread_exit(NULL);
}
