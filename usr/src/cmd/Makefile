#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)Makefile	1.132	09/01/26 SMI"
#
# cmd/Makefile
#
# include global definitions
include $(SRC)/Makefile.master

#
#	Commands in the FIRST_SUBDIRS list are built before starting the
#	build of other commands.
#
#	Commands are listed one per line so that TeamWare can
#	auto-merge most changes.

FIRST_SUBDIRS=		\
	ccr		\
	clcommands

COMMON_SUBDIRS=		\
	dcs		\
	cfgchk		\
	clconfig	\
	did		\
	initpkg		\
	load_balancer	\
	rcm_daemon	\
	scdpmd		\
	scdpm		\
	scconf		\
	scrconf		\
	scinstall	\
	sc_update_hosts	\
	sc_update_ntp	\
	scsetup		\
	scswitch	\
	scstat		\
	scrgadm		\
	ql-rgm		\
	scshutdown	\
	scsymon		\
	rpc.scadmd	\
	sccheck		\
	rpc.scrcmd	\
	scrcmd		\
	scds_syslog	\
	pmf		\
	schaapi		\
	test		\
	ssm_wrapper	\
	scds_pmf_as	\
	mcboot		\
	fed	.WAIT rgmd \
	pnm	.WAIT ha-services \
	transport	\
	support		\
	debug_scripts	\
	pxfs		\
	replctl		\
	orb		\
	fault_injection	\
	cmm		\
	net		\
	mdb		\
	dtk		\
	mdm		\
	rdt		\
	scnm		\
	vm		\
	scversions	\
	scversioncheck  \
	sc_publish_event \
	patch_prebackout \
	rpc.clquery	\
	scnas		\
	scnasdir	\
	scslmadm	\
	scqsd		\
	clqs       	\
	ql		\
	ptool		\
	postconfig	\
	sceventmib	\
	scprivipadm	\
	zones		\
	modify_ip	\
	scslm		\
	scslmthresh     \
	scwtadm		\
	scprt		\
	send_ioctl	\
	scqdmd		\
	cl_execd	\
	ns_test		\
	hasp_util

$(EUROPA)COMMON_SUBDIRS += \
	europa_install \
	hbtest \
	hbadm \
	scxcfg \
	damd \
	damadm \
	frgmd \
	ffmodtest

$(EUROPA)COMMON_SUBDIRS += \
	fmmd		\
	fmmclient	\
	rpc.scxcfg	\
	hbrsimu_adm	\
	haglbd

sparc_COMMON_SUBDIRS =
i386_COMMON_SUBDIRS =

COMMON_SUBDIRS +=  cl_eventd cl_eventlogd
COMMON_SUBDIRS += scdsbuilder
COMMON_SUBDIRS += $($(MACH)_COMMON_SUBDIRS)
COMMON_SUBDIRS += cl_apid

$(POST_S9_BUILD)COMMON_SUBDIRS += scprivipd \
				sc_dg_restarter \
				pnm_proxy \
				sczonecfg \
				zc_support \
				cz_util \
				cznetd \
				ifconfig \
				rtreg

i386_SUBDIRS=

sparc_SUBDIRS=

#
# Commands that are messaged.
#
MSGSUBDIRS=		\
	$(FIRST_SUBDIRS) \
	dcs		\
	cfgchk		\
	clconfig	\
	did		\
	initpkg		\
	load_balancer	\
	scconf		\
	scrconf		\
	scinstall	\
	sc_update_hosts	\
	sc_update_ntp	\
	scsetup		\
	scswitch	\
	scstat		\
	ql-rgm		\
	scrgadm		\
	scshutdown	\
	scsymon		\
	rpc.scadmd	\
	sccheck		\
	rpc.scrcmd	\
	scrcmd		\
	scds_syslog	\
	pmf		\
	schaapi		\
	test		\
	ssm_wrapper	\
	scds_pmf_as	\
	fed	.WAIT rgmd \
	pnm	.WAIT ha-services \
	scdsbuilder	\
	pxfs		\
	rdt		\
	scdpm		\
	scversioncheck	\
	scversions	\
	sceventmib	\
	ptool		\
	ql		\
	patch_prebackout \
	scqsd 		\
	clqs		\
	zones		\
	postconfig	\
	scslmadm	\
	scslmthresh	\
	scslm		\
	scqdmd		\
	ns_test

$(POST_S9_BUILD)MSGSUBDIRS += scprivipd sc_dg_restarter pnm_proxy

#
# directory dependencies
#
ha-services: scdsbuilder

all :=		TARGET= all
install :=	TARGET= install
clean :=	TARGET= clean
clobber :=	TARGET= clobber
lint :=		TARGET= lint
_msg :=		TARGET= _msg
check :=	TARGET= check

.KEEP_STATE:

SUBDIRS = $(COMMON_SUBDIRS) $($(MACH)_SUBDIRS)
ALLSUBDIRS = $(FIRST_SUBDIRS) .WAIT $(SUBDIRS)

.PARALLEL:	$(BWOSDIRS) $(SUBDIRS) $(MSGSUBDIRS)

all install clean clobber lint check: $(ALLSUBDIRS)

#
# The .WAIT directive works around an apparent bug in parallel make.
# Evidently make was getting the target _msg vs. _dc confused under
# some level of parallelization, causing some of the _dc objects
# not to be built.
#
_msg: $(MSGSUBDIRS) $($(MACH)_MSGSUBDIRS)

$(FIRST_SUBDIRS) $(SUBDIRS): FRC
	@cd $@; pwd; $(MAKE) $(TARGET)

FRC:
