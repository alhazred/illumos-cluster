/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma	ident	"@(#)smart_string.cc	1.9	08/05/20 SMI"

#include <sys/os.h>
#include <sys/sc_syslog_msg.h>
#include <errno.h>

#include "smart_string.h"
#include "cl_apid_include.h"

smart_string::smart_string(const char *in_str)
{
	sc_syslog_msg_handle_t handle;
	int err;

	num_refs = 1;
	status = 0;

	scds_syslog_debug(1, "smart_string::smart_string\n");
	/* copy the string */
	if ((the_str = os::strdup(in_str)) == NULL) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Error: low memory");
		sc_syslog_msg_done(&handle);
		status = ENOMEM;
		return;
	}

	/*
	 * Create the mutex
	 */
	if ((err = pthread_mutex_init(&refs_lock, NULL)) != 0) {
		status = err;
		char *err_str = strerror(status);
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "pthread_mutex_init: %s", err_str ? err_str : "");
		sc_syslog_msg_done(&handle);
		return;
	}
}

smart_string::~smart_string()
{
	scds_syslog_debug(1, "smart_string::~smart_string\n");
	(void) pthread_mutex_destroy(&refs_lock);
	delete the_str;
}

smart_string &smart_string::get_ref()
{
	CL_PANIC(pthread_mutex_lock(&refs_lock) == 0);
	num_refs++;
	scds_syslog_debug(1, "smart_string::get_ref: num_refs=%d\n", num_refs);
	CL_PANIC(pthread_mutex_unlock(&refs_lock) == 0);
	return (*this);
}

smart_string *smart_string::get_ptr_ref()
{
	CL_PANIC(pthread_mutex_lock(&refs_lock) == 0);
	num_refs++;
	scds_syslog_debug(1,
		"smart_string::get_ptr_ref: num_refs=%d\n",
		num_refs);
	CL_PANIC(pthread_mutex_unlock(&refs_lock) == 0);
	return (this);
}


void smart_string::release_ref()
{
	CL_PANIC(pthread_mutex_lock(&refs_lock) == 0);
	num_refs--;
	scds_syslog_debug(1,
		"smart_string::release_ref: num_refs=%d\n",
		num_refs);
	if (num_refs == 0) {
		CL_PANIC(pthread_mutex_unlock(&refs_lock) == 0);
		delete this;
	} else {
		CL_PANIC(pthread_mutex_unlock(&refs_lock) == 0);
	}
}
