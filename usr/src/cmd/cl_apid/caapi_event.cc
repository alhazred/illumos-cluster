/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */
#pragma ident   "@(#)caapi_event.cc 1.14     08/05/20 SMI"


#include "caapi_event.h"
#include "client_reg_info.h"
#include <stdio.h>

caapi_event::caapi_event(const sc_event &src):
	sc_event(src),
	client_backptr(NULL),
	in_ccr(true),
	match_frequency(0),
	num_nvpairs(nvlist->count())
{
	//
	// nv count is set to total name value pairs associated with the
	// event, if it is zero then it is set to 1
	//
	nv_count = (nvlist->count() > 0)?nvlist->count():1;
}

caapi_event::caapi_event(const caapi_event &src):
	sc_event(src),
	client_backptr(NULL),
	match_frequency(0),
	num_nvpairs(nvlist->count())
{
	// copy values from the incoming caapi_event
	in_ccr = src.is_in_ccr();
	//
	// nv count is set to total name value pairs associated with the
	// event, if it is zero then it is set to 1
	//
	nv_count = (nvlist->count() > 0)?nvlist->count():1;
}


/*
 * op=
 * -------------
 * Combination of destructor and constructor.
 */
caapi_event &caapi_event::operator = (const caapi_event &src)
{
	sc_event::operator = (src);
	if (this != &src) {
		delete [] evt_class;
		delete [] evt_subclass;

		in_ccr = src.is_in_ccr();
		client_backptr = src.get_client_backptr();
		nv_count = src.get_nv_count();
		match_frequency = src.get_match_frequency();
		// set the num of nvpairs associated with the event
		num_nvpairs = nvlist->count();
	}
	return (*this);
}

// following constructor parses the event string into event properties
caapi_event::caapi_event(const char *eventstr_in):
			sc_event(),
			client_backptr(NULL),
			in_ccr(true),
			match_frequency(0),
			num_nvpairs(nvlist->count())
{
	char *eventstr = os::strdup(eventstr_in);
	if (eventstr == NULL) {
		construct_status = ENOMEM;
		return;
	}

	parse_event(eventstr, evt_class, evt_subclass, *nvlist);

	//
	// nv count is set to total name value pairs associated with the
	// event, if it is zero then it is set to 1
	//
	nv_count = (nvlist->count() > 0)?nvlist->count():1;
	delete [] eventstr;
}

/*
 * destructor
 * ------------
 * Frees the memory of all the attributes. Caller of delete should
 * ensure there are no dangling references to the attributes.
 */
caapi_event::~caapi_event()
{
}

// Accessor Functions

const uint_t
caapi_event::get_nv_count() const
{
	return (nv_count);
}

const uint_t
caapi_event::get_num_nvpairs() const
{
	return (num_nvpairs);
}

const uint_t
caapi_event::get_match_frequency()  const
{
	return (match_frequency);
}

void
caapi_event::set_match_frequency(uint_t frequency_in)
{
	match_frequency = frequency_in;
}

void
caapi_event::increment_frequency()
{
	match_frequency++;
}

void
caapi_event::set_client_backptr(caapi_client *clientp_in)
{
	client_backptr = clientp_in;
}

caapi_client*
caapi_event::get_client_backptr() const
{
	return (client_backptr);
}

bool
caapi_event::is_in_ccr() const
{
	return (in_ccr);
}

void
caapi_event::set_in_ccr(bool in_ccr_new)
{
	in_ccr = in_ccr_new;
}

//
// parse_event parses the  event string into class, subclass and
// list of name-value pairs this function is
// called by parse_client_info. evt_class, evt_subclass
// and nvstr is allocated in this class and deallocated in
// parse_client_info function
//
void
caapi_event::parse_event(char *eventstr_in,
				    char *& evt_class_out,
				    char *& evt_subclass_out,
				    SList<nvstrpair> & evt_nvlist_out)
{
	SList <char> nvstrlist;
	char *eventstr = NULL;
	char *nvstr = NULL;
	char *tmp_nvstr = NULL;
	char *tmp_evt_class = NULL;
	char *tmp_evt_subclass = NULL;
	char *lasts = NULL;
	bool allocation_status;

	// XXX check for  max nvpairs exceeded
	// return if eventstr_in is NULL
	if (!eventstr_in)
		return;

	//
	// memory allocated to eventstr will be deallocated
	// in this function
	//
	eventstr = os::strdup(eventstr_in);
	if (eventstr == NULL) {
		construct_status = ENOMEM;
		return;
	}
	//
	// memory allocated to evt_class_out and evt_subclass_out are
	// deallocated in parse_client_info function
	//
	tmp_evt_class = strtok_r(eventstr, EVENT_DELIMITER, &lasts);
	if (tmp_evt_class) {
		evt_class_out = os::strdup(tmp_evt_class);
	}
	if (evt_class_out == NULL) {
		construct_status = ENOMEM;
		delete [] eventstr;
		return;
	}

	// parse eventstr to get subclass.
	tmp_evt_subclass = strtok_r(NULL, EVENT_DELIMITER, &lasts);
	if (!tmp_evt_subclass) {
		// event is registered with no subclass.
		return;
	}
	evt_subclass_out = os::strdup(tmp_evt_subclass);

	if (evt_subclass_out == NULL) {
		construct_status = ENOMEM;
		delete [] eventstr;
		return;
	}
	while ((tmp_nvstr = strtok_r(NULL, EVENT_DELIMITER, &lasts)) != NULL) {
		nvstr = os::strdup(tmp_nvstr);

		if (nvstr == NULL) {
			construct_status = ENOMEM;
			nvstrlist.dispose();
			delete [] eventstr;
			return;
		}
		nvstrlist.append(nvstr, &allocation_status);
		if (!allocation_status) {
			construct_status = ENOMEM;
			nvstrlist.dispose();
			delete [] eventstr;
			return;
		}
	} // end of while

	if (parse_nvpair(nvstrlist, evt_nvlist_out) != 0) {
		evt_nvlist_out.dispose();
		nvstrlist.dispose();
		delete [] eventstr;
		return;
	}
	nvstrlist.dispose();

	delete [] eventstr;
}

//
// parse_nvpair parses the nvstring into name and value
// stores in nvstrpair struct this function is called
// by parse_nvpair function
//
int
caapi_event::parse_nvpair(SList<char> &nvstrlist_in,
			    SList <nvstrpair> &evt_nvlist_out)
{
	char *name = NULL;
	char *value = NULL;
	char *nvstr = NULL;
	char *lasts = NULL;
	bool allocation_status;

	num_nvpairs = 0;
	nvstrlist_in.atfirst();
	while ((nvstr = nvstrlist_in.get_current()) != NULL) {

		nvstrlist_in.advance();

		name = strtok_r(nvstr, NV_DELIMITER, &lasts);
		if (name != NULL)
			value = strtok_r(NULL, NV_DELIMITER, &lasts);

		nvstrpair *evt_nvpair = new nvstrpair(name, value);
		if ((evt_nvpair == NULL) ||
		    (evt_nvpair->get_construction_status() != 0)) {
			construct_status = ENOMEM;
			delete evt_nvpair;
			return (ENOMEM);
		}
		evt_nvlist_out.append(evt_nvpair, &allocation_status);
		if (!allocation_status) {
			construct_status = ENOMEM;
			evt_nvlist_out.dispose();
			delete evt_nvpair;
			return (ENOMEM);
		}

		num_nvpairs++;
	}
	return (0);
}
#ifdef TEST_STUB
int
main()
{

	caapi_event event1("EC_CLUSTER:ESC_MEM:name1=val1:name2:val2");
	caapi_event event2("EC_CLUSTER:ESC_MEM");
	caapi_event event3("EC_CLUSTER");

	(void) printf("\n event1 props class =%s, subclass =%s",
	    event1.get_class(), event1.get_subclass());

	(void) printf("\n event2 props class =%s, subclass =%s",
	    event2.get_class(), event2.get_subclass());

	(void) printf("\n event3 props class =%s",
	    event3.get_class());

	return (0);
}
#endif
