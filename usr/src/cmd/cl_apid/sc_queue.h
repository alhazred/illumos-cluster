/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_SC_QUEUE_H_
#define	_SC_QUEUE_H_

#pragma ident	"@(#)sc_queue.h	1.9	08/05/20 SMI"

#include <sys/list_def.h>
#include <pthread.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

/*
 * class sc_queue
 * --------------------
 * Provides a simple queue functionality.
 * Templated class allows efficient, type-safe, storage of
 * any pointer type.
 *
 * The underlying data structure is an SList, templated to whatever
 * this class is templated to.
 *
 * This class is _not_ thread safe.  It must be protected by external
 * locks.
 */

template <class T>
class sc_queue
{
public:
	sc_queue();
	~sc_queue();

	/*
	 * get_construct_status
	 * ---------------------
	 * Set in the constructor to specify whether all the memory allocation
	 * succeeded or not.  Should be called right after construction.
	 * Returns 0 on successs, -1 or other error code on failure.
	 */
	int get_construct_status() {return construct_status; }

	/*
	 * enqueue
	 * ---------
	 * Adds the element elem to the tail of the queue.
	 * Does not copy it to new memory.
	 */
	int enqueue(T *elem);

	/*
	 * dequeue
	 * ----------
	 * Removes the element at the head of queue, and
	 * returns it.  If the queue is empty, returns NULL.
	 */
	T *dequeue();


	/*
	 * dequeue_all
	 * -------------------
	 * Returns a pointer to the current underlying list structure,
	 * and uses the incoming SList trade_in as its new underlying
	 * list structure.  The trade_in list must not be modified
	 * or accesssed by the client after passing it in.
	 *
	 * This "trade" of lists is an optimization to save reallocing
	 * a new list or copying each elemtn to a new list each time.
	 */
	SList<T> *dequeue_all(SList<T> *trade_in);

	/*
	 * peek
	 * ---------
	 * Returns a const ptr to the element at the head of the queue
	 * without removing it.  The next call to dequeue will return
	 * the same element.
	 */
	const T *peek();


	/*
	 * Accessor function for the underlying list structure.
	 * This function should be used with care.  The list should
	 * not be modified, and other calls on this queue should not
	 * be done while the list is being manipulated.
	 */
	SList<T> *get_list() {return list; }

protected:
	SList<T> *list;
	int construct_status;

private:
	/*
	 * Declare, but do not define, private copy constructor and op=,
	 * thus forcing pass by reference.
	 */
	sc_queue(const sc_queue<T> &);

	/* cstyle doesn't understand operator overloading */
	/*CSTYLED*/
	const sc_queue<T> &operator=(const sc_queue<T> &);
};

template <class T>
sc_queue<T>::sc_queue()
{
	construct_status = 0;
	list = new SList<T>;
	if (list == NULL) {
		construct_status = ENOMEM;
	}
}

template <class T>
sc_queue<T>::~sc_queue()
{
	/* don'd delete the elements on the list... */
	list->erase_list();
	delete list;
}

template <class T>
int sc_queue<T>::enqueue(T *elem)
{
	// no error code from append yet...
	list->append(elem);
	return (0);
}

template <class T>
T *sc_queue<T>::dequeue()
{
	T *elem = list->reapfirst();
	return (elem);
}

template <class T>
SList<T> *sc_queue<T>::dequeue_all(SList<T> *trade_in)
{
	SList<T> *ret_list = list;
	list = trade_in;
	list->dispose();
	return (ret_list);
}


template <class T>
const T *sc_queue<T>::peek()
{
	list->atfirst();
	const T *elem = list->get_current();
	return (elem);
}

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#endif /* _SC_QUEUE_H_ */
