/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma	ident	"@(#)sc_xml_reply.cc	1.13	08/05/20 SMI"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>

#include <libxml/xmlmemory.h>
#include <libxml/parser.h>
#include <libxml/parserInternals.h>
#include <libxml/xmlerror.h>

#include <libnvpair.h>
#include <errno.h>
#include <sys/sc_syslog_msg.h>
#include <sys/os.h>
#include <orb/fault/fault_injection.h>

#include "sc_xml_reply.h"
#include "cl_apid_include.h"

/*
 * The following is declared in parserInternals.h
 *   extern int xmlDoValidityCheckingDefaultValue;
 * If we ever fail to #include it, we need to declare this var again.
 */

sc_xml_reply::sc_xml_reply(const char *xml) : sc_xml_instance(xml)
{
	code = OK;
	reply_msg = NULL;

	/*
	 * Parse the xml right away so our accessors can be const
	 *
	 * We know that we're making a call to a virtual function in
	 * a constructor, and that subclasses will not get the correct
	 * version.  That's fine.  Suppress the lint warning.
	 */
	parse_xml(); /*lint !e1506 */
}

sc_xml_reply::sc_xml_reply(reply_code code_in, const char *reply_msg_in) :
    sc_xml_instance()
{
	code = code_in;
	if (reply_msg_in) {
		if ((reply_msg = os::strdup(reply_msg_in)) == NULL) {
			xml_status = ENOMEM;
			return;
		}
	} else {
		reply_msg = NULL;
	}

	/*
	 * We know that we're making a call to a virtual function in
	 * a constructor, and that subclasses will not get the correct
	 * version.  That's fine.  Suppress the lint warning.
	 */
	create_xml(); /*lint !e1506 */
}

/* destructor */
sc_xml_reply::~sc_xml_reply()
{
	delete [](reply_msg);
}

reply_code sc_xml_reply::get_reply_code() const
{
	return (code);
}

const char *sc_xml_reply::get_reply_message() const
{
	return (reply_msg);
}

/* for debugging */
void sc_xml_reply::print() const
{
	const char *code_str = code_to_str(code);
	(void) fprintf(stderr, "%s: %s\n", code_str ? code_str : "NULL",
	    reply_msg ? reply_msg : "");
}

void sc_xml_reply::create_xml()
{
#ifdef _FAULT_INJECTION
	void		*f_argp;
	uint32_t	f_argsize;
	if (fault_triggered(FAULTNUM_CLAPI_REPLY_CREATE_XML_R,
		&f_argp, &f_argsize)) {
		ASSERT(f_argsize == sizeof (int));
		xml_status = *((int *)f_argp);
		return;
	}
#endif
	string_buffer str_buf;

	if (!parse_code) {
		/* error */
		xml_status = -1;
		return;
	}
	if (xml_str != NULL) {
		return;
	}

	/* create it here */
	if (write_reply(str_buf) == -1) {
		xml_status = -1;
		return;
	}
	if ((xml_str = os::strdup(str_buf.get_buf())) == NULL) {
		xml_status = ENOMEM;
		return;
	}
}

const char *sc_xml_reply::code_to_str(reply_code code_in) const
{
	char *code_in_str;
	sc_syslog_msg_handle_t handle;

	switch (code_in) {
	case OK: code_in_str = "OK";
		break;
	case FAIL: code_in_str = "FAIL";
		break;
	case RETRY: code_in_str = "RETRY";
		break;
	case LOW_RESOURCES: code_in_str = "LOW_RESOURCES";
		break;
	case SYSTEM_ERROR: code_in_str = "SYSTEM_ERROR";
		break;
	case INVALID_MSG: code_in_str = "INVALID";
		break;
	case MALFORMED_MSG: code_in_str = "MALFORMED";
		break;
	default:
		/* internal error */
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The cl_apid encountered an invliad reply code in an
		// sc_reply message.
		// @user_action
		// This particular error should not cause problems with the
		// CRNP service, but it might be indicative of other errors.
		// Examine other syslog messages occurring at about the same
		// time to see if the problem can be identified. Save a copy
		// of the /var/adm/messages files on all nodes and contact
		// your authorized Sun service provider for assistance in
		// diagnosing and correcting the problem.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "internal error: invalid reply code");
		sc_syslog_msg_done(&handle);
		return (NULL);
	}
	return (code_in_str);
}

/* returns strlen of new string */
int sc_xml_reply::write_reply(string_buffer &str_buf)
{
	int total = 0, inc = 0;
	const char *code_str = NULL;

	code_str = code_to_str(code);
	if (code_str == NULL) {
		xml_status = -1;
		return (-1);
	}

	/* cstyle doesn't understand this line */
	total = str_buf.append("<?xml version=\"1.0\" standalone=\"no\"?>"
	    /*CSTYLED*/
	    "<!DOCTYPE SC_REPLY PUBLIC \"-//SUNW//sc_reply//EN\" "
	    "\"sc_reply.dtd\">");
	if (total == -1) {
		xml_status = -1;
		return (-1);
	}
	if ((inc = str_buf.append("<SC_REPLY STATUS_CODE=\"%s\">",
	    code_str)) == -1) {
		xml_status = -1;
		return (-1);
	}
	total += inc;
	if ((inc = write_status_msg(str_buf)) == -1) {
		xml_status = -1;
		return (-1);
	}
	total += inc;
	if ((inc = str_buf.append("</SC_REPLY>")) == -1) {
		xml_status = -1;
		return (-1);
	}
	total += inc;
	return (total);
}

/* returns strlen of new string, or -1 on error */
int sc_xml_reply::write_status_msg(string_buffer &str_buf)
{
	int total = 0;

	total = str_buf.append("<SC_STATUS_MSG><![CDATA[%s]]>"
	    "</SC_STATUS_MSG>", reply_msg ? reply_msg : "");
	if (total == -1) {
		xml_status = -1;
	}
	return (total);
}

void sc_xml_reply::parse_xml()
{
	xmlDocPtr doc;
	xmlNodePtr cur;
	int wellFormed;
	xmlParserCtxtPtr ctxt;
	char *code_str;
	sc_syslog_msg_handle_t handle;

	if (xml_str == NULL || xml_str[0] == '\0') {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Failed to parse xml for %s: %s", "sc_reply", "NULL xml");
		sc_syslog_msg_done(&handle);
		return;
	}

	(void) xmlKeepBlanksDefault(0);
	ctxt = (xmlParserCtxtPtr)xmlCreateMemoryParserCtxt
	    (xml_str, (int)strlen(xml_str));

	if (ctxt == NULL) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_INFO, MESSAGE,
		    "Failed to parse xml for %s: %s", "sc_reply",
		    "low memory");
		sc_syslog_msg_done(&handle);
		xml_status = ENOMEM;
		return;
	}

	ctxt->sax->error = xmlSCErrorFunc;
	ctxt->sax->warning = xmlSCErrorFunc;
	ctxt->sax->fatalError = xmlSCErrorFunc;

	/*
	 * Redirect errors to our debug function.
	 * Looks like we need to do this each time.
	 */
	xmlSetGenericErrorFunc((void *)NULL,
	    (xmlGenericErrorFunc)xmlSCErrorFunc);


	/* parse the document */
	if (xmlParseDocument(ctxt) != 0) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_INFO, MESSAGE,
		    "Failed to parse xml for %s: %s", "sc_reply",
		    "mal-formed");
		sc_syslog_msg_done(&handle);
		xmlFreeParserCtxt(ctxt);
		parse_code = MAL_FORMED;
		return;
	}
	doc = ctxt->myDoc;
	wellFormed = ctxt->wellFormed;

	if (doc == NULL || !wellFormed) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_INFO, MESSAGE,
		    "Failed to parse xml for %s: %s", "sc_reply",
		    "mal-formed");
		sc_syslog_msg_done(&handle);
		xmlFreeParserCtxt(ctxt);
		parse_code = MAL_FORMED;
		return;
	}

	scds_syslog_debug(1, "xml is well-formed\n");

	/*
	 * Redirect errors to our debug function.
	 * Looks like we need to do this each time.
	 */
	xmlSetGenericErrorFunc((void *)NULL,
	    (xmlGenericErrorFunc)xmlSCErrorFunc);

	/*
	 * Now check validity.  First, load the DTD.
	 * We hard-code the name of the DTD here...
	 */
	xmlDtdPtr theDtd = xmlParseDTD(NULL,
	    (const xmlChar *)"sc_reply.dtd");
	if (theDtd == NULL) {
		scds_syslog_debug(1,
			"Unable to load dtd: skipping validation\n");
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_INFO, MESSAGE,
		    "Error loading dtd for %s", "sc_reply");
		sc_syslog_msg_done(&handle);
	} else {
		/*
		 * Now try to do the actual validation
		 */
		xmlValidCtxt cvp;
		/*
		 * Set up the error message callbacks.  Again, some
		 * documentation would be nice...
		 */
		cvp.userData = (void *)NULL;
		cvp.error = (xmlValidityErrorFunc) xmlSCErrorFunc;
		cvp.warning = (xmlValidityWarningFunc) xmlSCErrorFunc;
		if (!xmlValidateDtd(&cvp, doc, theDtd)) {
			scds_syslog_debug(1, "sc_reply is invalid\n");
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
			(void) sc_syslog_msg_log(handle, LOG_INFO, MESSAGE,
			    "Failed to parse xml for %s: %s",
			    "sc_reply", "invalid");
			sc_syslog_msg_done(&handle);
			xmlFreeParserCtxt(ctxt);
			xmlFreeDoc(doc);
			xmlFreeDtd(theDtd);
			parse_code = INVALID;
			return;
		}
		scds_syslog_debug(1, "xml is valid\n");
	}

	/* we can free this now; we don't need it anymore */
	xmlFreeDtd(theDtd);

	/* do we need to free the xmlValidCtxt??? */

	/*
	 * Check the document is of the right kind
	 */

	cur = xmlDocGetRootElement(doc);
	if (cur == NULL) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_INFO, MESSAGE,
		    "Failed to parse xml: invalid element %s", "NULL");
		sc_syslog_msg_done(&handle);
		xmlFreeParserCtxt(ctxt);
		xmlFreeDoc(doc);
		parse_code = MAL_FORMED;
		return;
	}
	if (strcasecmp((const char *)cur->name, "SC_REPLY")) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_INFO, MESSAGE,
		    "Failed to parse xml: invalid element %s", cur->name);
		sc_syslog_msg_done(&handle);
		xmlFreeParserCtxt(ctxt);
		xmlFreeDoc(doc);
		parse_code = MAL_FORMED;
		return;
	}

	/*
	 * At this point we consider it parsed.
	 * If there's an error, it will be set to something else
	 */
	parse_code = PARSED;


	/* get the code attribute of the SC_REPLY */
	code_str = (char *)xmlGetProp(cur,
	    (const unsigned char *)"STATUS_CODE");
	if (code_str == NULL) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_INFO, MESSAGE,
		    "Failed to parse xml for %s: %s", "sc_reply",
		    "NULL attribute");
		sc_syslog_msg_done(&handle);
		xmlFreeParserCtxt(ctxt);
		xmlFreeDoc(doc);
		parse_code = MAL_FORMED;
		return;
	}
	if (strcasecmp(code_str, "OK") == 0) {
		code = OK;
	} else if (strcasecmp(code_str, "RETRY") == 0) {
		code = RETRY;
	} else if (strcasecmp(code_str, "FAIL") == 0) {
		code = FAIL;
	} else if (strcasecmp(code_str, "INVALID") == 0) {
		code = INVALID_MSG;
	} else if (strcasecmp(code_str, "MALFORMED") == 0) {
		code = MALFORMED_MSG;
	} else if (strcasecmp(code_str, "SYSTEM_ERROR") == 0) {
		code = SYSTEM_ERROR;
	} else if (strcasecmp(code_str, "LOW_RESOURCES") == 0) {
		code = LOW_RESOURCES;
	} else {
		code = FAIL;
	}

	/*
	 * Get the first subelement.  It should be an SC_STATUS_MSG
	 */
	cur = cur->xmlChildrenNode;
	if (cur == NULL) {
		/* no action */
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_INFO, MESSAGE,
		    "Failed to parse xml: invalid element %s",
		    "NULL");
		sc_syslog_msg_done(&handle);
		xmlFreeParserCtxt(ctxt);
		xmlFreeDoc(doc);
		parse_code = MAL_FORMED;
		return;
	}

	if (strcasecmp((const char *)cur->name, "SC_STATUS_MSG")) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_INFO, MESSAGE,
		    "Failed to parse xml: invalid element %s",
		    cur->name);
		sc_syslog_msg_done(&handle);
		xmlFreeParserCtxt(ctxt);
		xmlFreeDoc(doc);
		parse_code = MAL_FORMED;
		return;
	}

	/* text is a child of the sc_status_msg node */
	reply_msg = (char *)xmlNodeListGetString(doc,
	    cur->xmlChildrenNode, 0);
	if (reply_msg == NULL) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_INFO, MESSAGE,
		    "Failed to parse xml for %s: %s", "sc_reply",
		    "NULL attribute");
		sc_syslog_msg_done(&handle);
		xmlFreeParserCtxt(ctxt);
		xmlFreeDoc(doc);
		parse_code = MAL_FORMED;
		return;
	}
	xmlFreeParserCtxt(ctxt);
	xmlFreeDoc(doc);
}
