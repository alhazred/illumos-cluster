/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_CAAPI_CLIENT_H_
#define	_CAAPI_CLIENT_H_

#pragma ident	"@(#)caapi_client.h	1.16	08/05/20 SMI"

#include <sys/param.h>
#include <sys/os.h>
#include <sys/rm_util.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#include "sc_callback_info.h"
#include "sc_registration_instance.h"
#include "caapi_event.h"
#include "client_reg_info.h"

/*
 * 4763564 ACTIVE defines conflict with libsysevent.h in Solaris 10
 */
#define	BUG_4763564

//
// class caapi_client stores client informaton callback ip and list
// of events , client has registered for.
// This class provides a set of interfaces for updating events list
//

//
// Suppress lint warning about no default constructor.
// default constructor not needed
//
/*lint -e1712 */


// class  caapi_client_handle;
class caapi_client: public client_reg_info {
public:
	//
	// status_flag is used to indicate whether the client
	// active/deleted in the in the in-memory client_list
	//
#ifdef	BUG_4763564
	typedef enum { CAAPI_ACTIVE, DELETED } status_flag;
#else
	typedef enum { ACTIVE, DELETED } status_flag;
#endif

	// used when reading from caapi_reg ccr table
	caapi_client(const char *key_in, const char *data_in);


	// used when registration instance is passed
	caapi_client(const sc_registration_instance &
			    client_reg_instance);

	// method used to get the reference/handle to caapi_client
	caapi_client_handle* get_client_handle();

	//
	// decrements ref count for this object also if the status of client
	// is DELETED then client instance is deleted
	// release_ref is called by the client_handler
	// destroy itself if nref == 0 and status == DELETED
	//
	// returns the status of the client, returns TRUE
	// when client is active.
	//
	bool	is_active();
	void    release_ref();
	bool	is_notified() const;
	void set_notify(bool notify_status);

	//
	// try_delete function is called from caapi_reg_cache when client
	// chooses unregister, this function will delete the client instance
	// when there are no outstanding of client, else the status of client
	// is set to DELETED to indicate that client is unregistered and is
	// removed from the client list of caapi_reg_cache when the last handler
	// releases reference of client then client is deleted
	//
	void try_delete();

	// set the back pointer in event to client
	void set_client_backptr();

	~caapi_client();
protected:
	// disallowed copy constructor
	caapi_client(const caapi_client &src);
	// disallowed = operator overloading
	caapi_client& operator=(const caapi_client &rhs);

	status_flag		status;

	/*
	 * nref and nfail are not currently used.
	 */
	int			nref;
	int			nfail;
	// time			lastcontacttime;
	// mutex lock protects nref and status updation
	os::mutex_t		mutex_lck;
	//
	// notify boolean is used in search hash for notifying all clients
	// with notify set to true
	//
	bool 			notify;
};
/*lint +e1712 */

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#endif /* _CAAPI_CLIENT_H_ */
