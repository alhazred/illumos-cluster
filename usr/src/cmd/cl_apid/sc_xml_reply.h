/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_SC_XML_REPLY_H_
#define	_SC_XML_REPLY_H_

#pragma ident	"@(#)sc_xml_reply.h	1.7	08/05/20 SMI"

#include <libsysevent.h>
#include <libxml/parser.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#include "sc_xml_instance.h"
#include "string_buffer.h"

typedef enum {OK, RETRY, LOW_RESOURCES, SYSTEM_ERROR, FAIL, MALFORMED_MSG,
    INVALID_MSG} reply_code;

/*
 * class sc_xml_reply
 * --------------------
 * Subclass of sc_xml_instance.
 * A reply is a reply_code and a string message.
 *
 * As a subclass of sc_xml_instance, this class knows how to convert
 * the attribtues to/from xml.  It can be created with either attributes or
 * xml, but is immutable once created.
 *
 * The xml is parsed or created upon object creation.
 */

/*
 * Suppress lint warning about no default constructor.
 * We don't want a default constructor.
 */
/*lint -e1712 */
class sc_xml_reply : public sc_xml_instance {
public:

	/*
	 * constructors
	 * ----------------
	 * Construct from xml, or from attributes directly
	 */
	sc_xml_reply(const char *xml);
	sc_xml_reply(reply_code code_in, const char *reply_message);

	/*
	 * destructor
	 * -----------
	 * frees the memory
	 */
	virtual ~sc_xml_reply();

	/*
	 * accessors
	 * -----------
	 * Return references to the attributes.
	 */
	reply_code get_reply_code() const;
	const char *get_reply_message() const;

	/*
	 * print
	 * ------
	 * for debugging
	 */
	virtual void print() const;

protected:
	char *reply_msg;
	reply_code code;

	virtual void parse_xml();
	virtual void create_xml();

	int write_reply(string_buffer &str_buf);
	int write_status_msg(string_buffer &str_buf);

private:
	/*
	 * Declare a private copy constructor and op=, but do not define them,
	 * to force pass by reference.
	 */
	sc_xml_reply(const sc_xml_reply &src);

	/* cstyle doesn't understand operator overloading */
	/*CSTYLED*/
	sc_xml_reply &operator=(const sc_xml_reply &src);

	const char *code_to_str(reply_code) const;
};
/*lint +e1712 */

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#endif /* _SC_XML_REPLY_H_ */
