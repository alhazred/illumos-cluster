/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma	ident	"@(#)sc_xml_event.cc	1.13	08/05/20 SMI"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <libxml/xmlmemory.h>
#include <libxml/parser.h>
#include <libxml/parserInternals.h>
#include <libxml/xmlerror.h>

#include <libsysevent.h>
#include <libnvpair.h>
#include <errno.h>
#include <sys/sc_syslog_msg.h>
#include <sys/os.h>
#include <orb/fault/fault_injection.h>

#include "sc_xml_event.h"
#include "cl_apid_include.h"

/*
 * The following is declared in parserInternals.h
 *   extern int xmlDoValidityCheckingDefaultValue;
 * If we ever fail to #include parserInternals.h,
 * we need to declare this var again.
 */

/*
 * constructors
 * ----------------
 * construct from sysevent, from xml, or from attributes
 */
sc_xml_event::sc_xml_event(sysevent_t *evt) : sc_xml_instance(),
    sc_event(evt)
{
	scei_construct_helper(sysevent_get_vendor_name(evt),
	    sysevent_get_pub_name(evt));
}

sc_xml_event::sc_xml_event(const char *xml) : sc_xml_instance(xml),
    sc_event()
{
	/* Don't parse yet; initialize everything to NULL */
	pub = NULL;
	vendor = NULL;

	/*
	 * Parse the xml right away so our accessors can be const
	 *
	 * We know that we're making a call to a virtual function in
	 * a constructor, and that subclasses will not get the correct
	 * version.  That's fine.  Suppress the lint warning.
	 */
	parse_xml(); /*lint !e1506 */
}

sc_xml_event::sc_xml_event(const char *new_evt_class,
    const char *new_evt_subclass, const char *new_vendor, const char *new_pub,
    const SList<nvstrpair> &list_in) : sc_xml_instance(),
    sc_event(new_evt_class, new_evt_subclass, list_in)
{
	scei_construct_helper(new_vendor, new_pub);
}

/* copy constructor */
sc_xml_event::sc_xml_event(const sc_xml_event &src) :
	sc_event(src), sc_xml_instance(src)
{
	scei_construct_helper(src.get_vendor(), src.get_pub());
}

/*
 * op=
 * -----------
 * Combo of destructor and constructor.
 */
/* cstyle doesn't understand operator overloading */
/*CSTYLED*/
const sc_xml_event &sc_xml_event::operator=(
    const sc_xml_event &src)
{
	/*CSTYLED*/
	sc_event::operator=(src);
	/*CSTYLED*/
	sc_xml_instance::operator=(src);

	if (this != &src) {
		delete [](vendor);
		delete [](pub);

		vendor = NULL;
		pub = NULL;
		if (src.get_vendor() != NULL) {
			if ((vendor = os::strdup(src.get_vendor())) == NULL) {
				xml_status = ENOMEM;
			}
		}
		if (src.get_pub() != NULL) {
			if ((pub = os::strdup(src.get_pub())) == NULL) {
				xml_status = ENOMEM;
			}
		}
		// don't construct xml here, because we copy it
	}
	return (*this);
}

/*
 * scei_construct_helper
 * ----------------------
 * Common construction code form all the constructors.
 */
void sc_xml_event::scei_construct_helper(const char *new_vendor,
    const char *new_pub)
{
	vendor = NULL;
	pub = NULL;

	if (new_vendor != NULL) {
		if ((vendor = os::strdup(new_vendor)) == NULL) {
			xml_status = ENOMEM;
			return;
		}
	}
	if (new_pub != NULL) {
		if ((pub = os::strdup(new_pub)) == NULL) {
			xml_status = ENOMEM;
			return;
		}
	}

	create_xml();
}

int sc_xml_event::get_construct_status() const
{
	int ret1 = sc_xml_instance::get_construct_status();
	int ret2 = sc_event::get_construct_status();

	if (ret1 != 0)
		return (ret1);
	return (ret2);
}

/*
 * destructor
 * ---------
 * Free everything
 */
sc_xml_event::~sc_xml_event()
{
	delete [](vendor);
	delete [](pub);
}



/*
 * Accessors
 * ------------
 */
const char *sc_xml_event::get_vendor() const
{
	return (vendor);
}

const char *sc_xml_event::get_pub() const
{
	return (pub);
}

/*
 * print
 * -------
 * For debugging
 */
void sc_xml_event::print() const
{
	sc_event::print();

	(void) fprintf(stderr, "pub: %s\n", pub ? pub : "");
	(void) fprintf(stderr, "vendor: %s\n", vendor ? vendor : "");
}

/*
 * create_xml
 * ------------
 *
 */
void sc_xml_event::create_xml()
{
	string_buffer str_buf;

#ifdef _FAULT_INJECTION
	void		*f_argp;
	uint32_t	f_argsize;
	if (fault_triggered(FAULTNUM_CLAPI_EVENT_CREATE_XML_R,
		&f_argp, &f_argsize)) {
		ASSERT(f_argsize == sizeof (int));
		xml_status = *((int *)f_argp);
		return;
	}
#endif
	if (!parse_code) {
		/* error */
		xml_status = -1;
		return;
	}
	if (xml_str != NULL) {
		/* we already created it */
		return;
	}

	/* create it here */
	if (write_event(str_buf) == -1) {
		xml_status = -1;
		return;
	}
	/* If we successfully created it, allocate new memory for it */
	if ((xml_str = os::strdup(str_buf.get_buf())) == NULL) {
		xml_status = ENOMEM;
		return;
	}
}

/* returns strlen of new string, or -1 on error */
int sc_xml_event::write_event(string_buffer &str_buf)
{
	int total = 0, inc = 0;

	total = str_buf.append("<?xml version=\"1.0\" standalone=\"no\"?>"
	    /*CSTYLED*/
	    "<!DOCTYPE SC_EVENT PUBLIC \"-//SUNW//sc_event//EN\" "
	    "\"sc_event.dtd\">");
	if (total == -1) {
		xml_status = -1;
		return (-1);
	}
	if ((inc = str_buf.append("<SC_EVENT CLASS=\"%s\" "
	    "SUBCLASS=\"%s\" VENDOR=\"%s\" PUBLISHER=\"%s\">",
	    evt_class ? evt_class : "", evt_subclass ? evt_subclass : "",
	    vendor ? vendor : "", pub ? pub : "")) == -1) {
		xml_status = -1;
		return (-1);
	}
	total += inc;

	/* Write the list, if we have one */
	if (nvlist != NULL) {
		if ((inc = write_attr_list(str_buf, *nvlist)) == -1) {
			xml_status = -1;
			return (-1);
		}
		total += inc;
	}

	if ((inc = str_buf.append("</SC_EVENT>")) == -1) {
		xml_status = -1;
		return (-1);
	}
	total += inc;
	return (total);
}

/*
 * write_attr_list
 * ------------------
 * Writes the attribute list.
 * Iterates through the list of nvpairs, writing each nvpair element
 * in the xml.  Calls a helper function to write the value, which can be
 * a list of a single element (or no elements).
 */
int sc_xml_event::write_attr_list(string_buffer &str_buf,
    const SList<nvstrpair> &a_list)
{
	nvstrpair *pair = NULL;
	const char *name;
	int total = 0, inc = 0;

	/* Cheat because there's no const_iterator */
	SList<nvstrpair>::ListIterator iter(const_cast<SList<nvstrpair> *>
	    (&a_list));
	for (; (pair = iter.get_current()) != NULL; iter.advance()) {
		name = pair->get_name();
		if ((inc = str_buf.append("<NVPAIR><NAME>"
		    "<![CDATA[%s]]></NAME>", name)) == -1) {
			return (-1);
		}
		total += inc;
		if ((inc = write_value_list(str_buf, pair->get_value_list()))
		    == -1) {
			return (-1);
		}
		total += inc;
		if ((inc = str_buf.append("</NVPAIR>")) == -1) {
			return (-1);
		}
		total += inc;
	}
	return (total);
}

/*
 * write_value_list
 * ---------------------
 * Writes the list of values for an nvpair.
 *
 * Returns the number of bytes written, or -1 on failure.
 */
int sc_xml_event::write_value_list(string_buffer &str_buf,
    const SList<char> &values)
{
	char *value;
	int total = 0, inc = 0;

	/* Cheat because there's no const_iterator */
	SList<char>::ListIterator iter(const_cast<SList<char> *>
	    (&values));
	for (; (value = iter.get_current()) != NULL; iter.advance()) {
		if ((inc = str_buf.append("<VALUE><![CDATA[%s]]></VALUE>",
		    value)) == -1) {
			return (-1);
		}
		total += inc;
	}
	return (total);
}

void sc_xml_event::parse_xml()
{
	xmlDocPtr doc;
	xmlNodePtr cur;
	int wellFormed;
	xmlParserCtxtPtr ctxt;
	sc_syslog_msg_handle_t handle;

	if (xml_str == NULL || xml_str[0] == '\0') {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Failed to parse xml for %s: %s", "sc_event", "NULL xml");
		sc_syslog_msg_done(&handle);
		return;
	}

	(void) xmlKeepBlanksDefault(0);
	ctxt = (xmlParserCtxtPtr)xmlCreateMemoryParserCtxt
	    (xml_str, (int)strlen(xml_str));

	if (ctxt == NULL) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_INFO, MESSAGE,
		    "Failed to parse xml for %s: %s", "sc_event",
		    "low memory");
		sc_syslog_msg_done(&handle);
		xml_status = ENOMEM;
		return;
	}

	ctxt->sax->error = xmlSCErrorFunc;
	ctxt->sax->warning = xmlSCErrorFunc;
	ctxt->sax->fatalError = xmlSCErrorFunc;

	/*
	 * Redirect errors to our debug function.
	 * Looks like we need to do this each time.
	 */
	xmlSetGenericErrorFunc((void *)NULL,
	    (xmlGenericErrorFunc)xmlSCErrorFunc);

	/* parse the document */
	if (xmlParseDocument(ctxt) != 0) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_INFO, MESSAGE,
		    "Failed to parse xml for %s: %s", "sc_event",
		    "mal-formed");
		sc_syslog_msg_done(&handle);
		xmlFreeParserCtxt(ctxt);
		parse_code = MAL_FORMED;
		return;
	}
	doc = ctxt->myDoc;
	wellFormed = ctxt->wellFormed;

	if (doc == NULL || !wellFormed) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_INFO, MESSAGE,
		    "Failed to parse xml for %s: %s", "sc_event",
		    "mal-formed");
		sc_syslog_msg_done(&handle);
		xmlFreeParserCtxt(ctxt);
		parse_code = MAL_FORMED;
		return;
	}

	scds_syslog_debug(1, "xml is well-formed\n");

	/*
	 * Redirect errors to our debug function.
	 * Looks like we need to do this each time.
	 */
	xmlSetGenericErrorFunc((void *)NULL,
	    (xmlGenericErrorFunc)xmlSCErrorFunc);

	/*
	 * Now check validity.  First, load the DTD.
	 * We hard-code the name of the DTD here...
	 */
	xmlDtdPtr theDtd = xmlParseDTD(NULL,
	    (const xmlChar *)"sc_event.dtd");
	if (theDtd == NULL) {
		scds_syslog_debug(1,
			"Unable to load dtd: skipping validation\n");
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_INFO, MESSAGE,
		    "Error loading dtd for %s", "sc_event");
		sc_syslog_msg_done(&handle);
	} else {
		/*
		 * Now try to do the actual validation
		 */
		xmlValidCtxt cvp;
		/*
		 * Set up the error message callbacks.  Again, some
		 * documentation would be nice...
		 */
		cvp.userData = (void *)NULL;
		cvp.error = (xmlValidityErrorFunc) xmlSCErrorFunc;
		cvp.warning = (xmlValidityWarningFunc) xmlSCErrorFunc;
		if (!xmlValidateDtd(&cvp, doc, theDtd)) {
			scds_syslog_debug(1, "sc_event is invalid\n");
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
			(void) sc_syslog_msg_log(handle, LOG_INFO, MESSAGE,
			    "Failed to parse xml for %s: %s",
			    "sc_event", "invalid");
			sc_syslog_msg_done(&handle);
			xmlFreeParserCtxt(ctxt);
			xmlFreeDoc(doc);
			xmlFreeDtd(theDtd);
			parse_code = INVALID;
			return;
		}
		scds_syslog_debug(1, "xml is valid\n");
	}

	/* we can free this now; we don't need it anymore */
	xmlFreeDtd(theDtd);

	/* do we need to free the xmlValidCtxt??? */

	/*
	 * Check the document is of the right kind
	 */

	cur = xmlDocGetRootElement(doc);
	if (cur == NULL) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_INFO, MESSAGE,
		    "Failed to parse xml: invalid element %s", "NULL");
		sc_syslog_msg_done(&handle);
		xmlFreeParserCtxt(ctxt);
		xmlFreeDoc(doc);
		parse_code = MAL_FORMED;
		return;
	}
	if (strcasecmp((const char *)cur->name, "SC_EVENT")) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_INFO, MESSAGE,
		    "Failed to parse xml: invalid element %s", cur->name);
		sc_syslog_msg_done(&handle);
		xmlFreeParserCtxt(ctxt);
		xmlFreeDoc(doc);
		parse_code = MAL_FORMED;
		return;
	}

	/*
	 * At this point we consider it parsed.
	 * If there's an error, it will be set to something else
	 */
	parse_code = PARSED;


	/*
	 * Read the attributes :
	 * Supposedly it's newly allocated memory.
	 */
	evt_class = (char *)xmlGetProp(cur, (const unsigned char *)"CLASS");
	evt_subclass = (char *)xmlGetProp(cur, (const unsigned char *)
	    "SUBCLASS");
	vendor = (char *)xmlGetProp(cur, (const unsigned char *)"VENDOR");
	pub = (char *)xmlGetProp(cur, (const unsigned char *)"PUBLISHER");

	if (evt_class == NULL || evt_subclass == NULL || vendor == NULL ||
	    pub == NULL) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_INFO, MESSAGE,
		    "Failed to parse xml for %s: %s", "sc_event",
		    "NULL attribute");
		sc_syslog_msg_done(&handle);
		xmlFreeParserCtxt(ctxt);
		xmlFreeDoc(doc);
		parse_code = MAL_FORMED;
		return;
	}

	/* call a separate function to parse the nvlist */
	parse_code = parse_nvlist(doc, cur->xmlChildrenNode, *nvlist,
	    xml_status);
	xmlFreeParserCtxt(ctxt);
	xmlFreeDoc(doc);
}


/*
 * parses an nvlist
 */
parse_result sc_xml_event::parse_nvlist(
    xmlDocPtr doc, xmlNodePtr cur, SList<nvstrpair> &a_list, int &status)
{
	xmlNodePtr cur_nv;
	sc_syslog_msg_handle_t handle;
	parse_result ret_val = PARSED;
	SList<char> temp_list;

	status = 0;
	if (cur == NULL) {
		/* we have no nvpairs... that's acceptable */
		return (PARSED);
	}

	for (; cur; cur = cur->next) {
		char *name, *value;

		if (strcasecmp((const char *)cur->name, "NVPAIR")) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
			(void) sc_syslog_msg_log(handle, LOG_INFO, MESSAGE,
			    "Failed to parse xml: invalid element %s",
			    cur->name);
			sc_syslog_msg_done(&handle);
			return (MAL_FORMED);
		}
		cur_nv = cur->xmlChildrenNode;
		if (strcasecmp((const char *)cur_nv->name, "NAME")) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
			(void) sc_syslog_msg_log(handle, LOG_INFO, MESSAGE,
			    "Failed to parse xml: invalid element %s",
			    cur_nv->name);
			sc_syslog_msg_done(&handle);
			return (MAL_FORMED);
		}
		/* text is a child of the name node */
		name = (char *)xmlNodeListGetString(doc,
		    cur_nv->xmlChildrenNode, 0);
		if (name == NULL || name[0] == '\0') {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
			(void) sc_syslog_msg_log(handle, LOG_INFO, MESSAGE,
			    "Failed to parse xml for %s: %s", "sc_event",
			    "NULL attribute");
			sc_syslog_msg_done(&handle);

			return (MAL_FORMED);
		}

		/*
		 * There can be multiple values, representing
		 * an array of values.
		 */
		for (cur_nv = cur_nv->next; cur_nv; cur_nv = cur_nv->next) {
			if (strcasecmp((const char *)cur_nv->name, "VALUE")) {
				(void) sc_syslog_msg_initialize(&handle,
				    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
				(void) sc_syslog_msg_log(handle, LOG_INFO,
				    MESSAGE,
				    "Failed to parse xml: invalid element %s",
				    cur_nv->name);
				sc_syslog_msg_done(&handle);

				/*
				 * Need to use free instead of delete
				 * because libxml2 uses malloc.
				 */
				free(name);
				ret_val = MAL_FORMED;
				goto free_list_bad;
			}
			/* text is a child of the value node */
			value = (char *)xmlNodeListGetString(doc,
			    cur_nv->xmlChildrenNode, 1);
			if (value == NULL || value[0] == '\0') {
				(void) sc_syslog_msg_initialize(&handle,
				    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
				(void) sc_syslog_msg_log(handle, LOG_INFO,
				    MESSAGE,
				    "Failed to parse xml for %s: %s",
				    "sc_event", "NULL attribute");
				sc_syslog_msg_done(&handle);
				ret_val = MAL_FORMED;
				goto free_list_bad;
			}
			temp_list.append(value);
		}
		nvstrpair *strpair = new nvstrpair(name, temp_list);
		if (strpair == NULL || strpair->get_construction_status()
		    != 0) {
			status = strpair ? strpair->get_construction_status() :
			    ENOMEM;
			delete strpair;
			ret_val = NOT_PARSED;
			goto free_list_bad;
		}
		a_list.append(strpair);

		char *free_str;
		while ((free_str = temp_list.reapfirst()) != NULL) {
			/*
			 * Need to use free instead of delete
			 * because libxml2 uses malloc.
			 */
			free(free_str);
		}
		/*
		 * Need to use free instead of delete
		 * because libxml2 uses malloc.
		 */
		free(name);
	}
	return (PARSED);

free_list_bad:
	char *free_str;
	while ((free_str = temp_list.reapfirst()) != NULL) {
		/*
		 * Need to use free instead of delete
		 * because libxml2 uses malloc.
		 */
		free(free_str);
	}
	return (ret_val);
}
