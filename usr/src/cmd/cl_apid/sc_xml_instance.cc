/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma	ident	"@(#)sc_xml_instance.cc	1.7	08/05/20 SMI"

#include <strings.h>
#include <sys/param.h>
#include <sys/varargs.h>

#include <sys/sc_syslog_msg.h>
#include <sys/os.h>

#include "sc_xml_instance.h"
#include "cl_apid_include.h"

#define	XML_ERROR_SIZE 1024

void
xmlSCErrorFunc(void *, const char *msg, ...)
{
	va_list ap;
	char print_buf[XML_ERROR_SIZE];

	va_start(ap, format); /*lint !e40 */
	(void) vsnprintf(print_buf, XML_ERROR_SIZE, msg, ap);
	scds_syslog_debug(1, "%s", print_buf);
	va_end(ap);
}

/*
 * constructor
 * -------------
 * When created with an xml string, simply copy the xml string.
 * We assume that if we are constructed with an xml string we do not
 * have attributes yet, and are not parsed.
 */
sc_xml_instance::sc_xml_instance(const char *xml) : parse_code(NOT_PARSED)
{
	xml_status = 0;

	xml_str = NULL;
	if (xml) {
		if ((xml_str = os::strdup(xml)) == NULL) {
			xml_status = ENOMEM;
			return;
		}
	}
}

/*
 * default constructor
 * ----------------
 * Assume that if we are constructed without an xml string that we
 * are parsed, and have attributes.
 */
sc_xml_instance::sc_xml_instance() : xml_str(NULL), parse_code(PARSED),
    xml_status(0)
{}

/*
 * copy constructor
 * --------------------
 * Copy the members
 */
sc_xml_instance::sc_xml_instance(const sc_xml_instance &src)
{
	xml_status = 0;

	parse_code = src.get_parse_result();
	if ((xml_str = os::strdup(src.get_xml())) == NULL) {
		xml_status = ENOMEM;
		return;
	}
}

/*
 * op=
 * ------
 * Combo of destructor and copy constructor.
 */
/* cstyle doesn't understand operator overloading */
/*CSTYLED*/
const sc_xml_instance &sc_xml_instance::operator=(const sc_xml_instance &src)
{
	if (this != &src) {
		delete [](xml_str);

		xml_status = 0;

		parse_code = src.get_parse_result();
		if ((xml_str = os::strdup(src.get_xml())) == NULL) {
			xml_status = ENOMEM;
		}

	}
	return (*this);
}

/*
 * destructor
 * ------------
 */
sc_xml_instance::~sc_xml_instance()
{
	delete [](xml_str);
}
