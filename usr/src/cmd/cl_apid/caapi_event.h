/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */


#ifndef	_CAAPI_EVENT_H_
#define	_CAAPI_EVENT_H_

#pragma ident	"@(#)caapi_event.h	1.16	08/05/20 SMI"

#include <libsysevent.h>
#include <sys/os.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#include "sc_event.h"
#include "nvstrpair.h"


// forward declaration of caapi_client
class caapi_client;
//
// Suppress lint warning about no default constructor.
// default constructor not needed
//
/*lint -e1712 */

// XXX issues : nv_count redundant

class caapi_event:public sc_event {
public:

	/*
	 * constructors
	 * --------------
	 * Can be constructued only from attributes or other caapi_event
	 */
	caapi_event(const char *evt_class_in, const char *evt_subclass_in,
	    const SList<nvstrpair> &nvstrlist_in);

	/*
	 * copy constructor and op=
	 * --------------------------
	 * To do deep copies.
	 */
	caapi_event(const caapi_event &src);
	caapi_event(const sc_event &src);
	caapi_event &operator = (const caapi_event &src);

	// following constructor parses the event string into event properties
	caapi_event(const char *eventstr_in);

	/*
	 * destructor
	 * ------------
	 * Frees the memory of all the attributes. Caller of delete should
	 * ensure there are no dangling references to the attributes.
	 */
	virtual ~caapi_event();

	/*
	 * Accessor functions
	 * ------------------
	 * Return references to the attributes.
	 */

	//
	// set  the client back pointer in the event
	// useful in searching for unregistered events.
	//
	void  set_client_backptr(caapi_client *clientp_in);
	caapi_client* get_client_backptr() const;

	//
	// set in_ccr to indicate whether the event should
	// be written into caapi_reg ccr  table.
	//
	bool  is_in_ccr()const;
	void set_in_ccr(bool in_ccr_new);

	//
	// total number of name-value pairs associated with the event.
	// it is set to 1 if the event has no nvpairs.
	// nv_count is used in search hashtable to identify if
	// a this event is a super-set of generated  sysevent.
	//
	const uint_t get_nv_count() const;

	// returns the total nvpairs associated with the event.
	const uint_t get_num_nvpairs() const;
	//
	// the following functions are useful in searching for a event
	// in the hashtable. if a events whose match frequency matches the
	// the nvcount  then the client associated with this event
	// is notified of the occurance of the event.
	//
	const uint_t get_match_frequency() const;
	void set_match_frequency(uint_t frequency_in);
	void increment_frequency();

protected:
	void parse_event(char *evenstr_in,
			char *& evt_class_out,
			char *& evt_subclass_out,
			SList <nvstrpair> & evt_nvlist_out);

	int parse_nvpair(SList<char> &nvstrlist_in,
			SList <nvstrpair> & evt_nvlist_out);

	caapi_client		*client_backptr;
	bool			in_ccr;
	//
	// match frequency keeps a count of the total
	// number of times event is listed in the search hashtable
	// this is initiliazed with -1 when the event object is created
	// and incremented to 0 when it is added to hash table.
	//
	uint_t			match_frequency;
	//
	// total number of name-value pairs associated with the event
	// it is set to 1 if the event has no nvpairs.
	// nv_count is used in search hashtable to identify if
	// a this event is a super-set of generated  sysevent.
	//
	uint_t			nv_count;

	// number of nvpairs associated with the event.
	uint_t			num_nvpairs;
};

/*lint +e1712 */

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#endif /* _CAAPI_EVENT_H_ */
