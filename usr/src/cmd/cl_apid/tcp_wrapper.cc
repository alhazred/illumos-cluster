/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma	ident	"@(#)tcp_wrapper.cc	1.9	08/05/20 SMI"

#include <sys/sc_syslog_msg.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <sys/os.h>

#include <net/if.h>
#include <sys/sockio.h>
#include <orb/fault/fault_injection.h>

#include "tcp_wrapper.h"
#include "cl_apid_include.h"


/*
 * constructors
 * --------------
 * Parses the hosts_allow_in and hosts_deny_in lists, converting them
 * to address and 32 bit masks form.
 */
tcp_wrapper::tcp_wrapper(const char * const *hosts_allow_in,
    uint_t num_hosts_allow_in, const char * const *hosts_deny_in,
    uint_t num_hosts_deny_in)
{
#ifdef _FAULT_INJECTION
	void		*f_argp;
	uint32_t	f_argsize;
	if (fault_triggered(FAULTNUM_CLAPI_TCP_WRAPPER_R,
		&f_argp, &f_argsize)) {
		ASSERT(f_argsize == sizeof (int));
		status = *((int *)f_argp);
		return;
	}
#endif
	sc_syslog_msg_handle_t handle;

	/*
	 * Initialize the variables so that if
	 * we stop construction part-way through, we're
	 * always in a stable state.
	 */
	num_hosts_allow = num_hosts_deny = 0;
	hosts_allow = hosts_deny = NULL;

	/* The default is to accept no one */
	deny_all = false;
	allow_all = false;

	status = 0;

	/* First, process hosts_allow */
	if (num_hosts_allow_in > 0) {
		num_hosts_allow = num_hosts_allow_in;

		/* allocate memory */
		if ((hosts_allow = new net_id[num_hosts_allow]) == NULL) {
			/* failed */
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "Error: low memory");
			sc_syslog_msg_done(&handle);
			status = ENOMEM;
			return;
		}
		/*
		 * zero out the array so that if we bail out part-way
		 * through the convert_host_list function, we can safely
		 * call delete on all the ptrs, which will either be valid
		 * or NULL.
		 */
		(void) memset(hosts_allow, '\0',
		    sizeof (net_id) * num_hosts_allow);

		/* Do the converstion of ip/mask_num to addr and 32 bit mask */
		if (convert_host_list(hosts_allow, hosts_allow_in,
		    num_hosts_allow, allow_all) != 0) {
			status = -1;
			return;
		}
		/* If we're allowing all, we don't need to keep the list */
		if (allow_all) {
			scds_syslog_debug(1,
				"tcp_wrapper init: Found allow_all\n");
			delete_host_list(hosts_allow, num_hosts_allow);
		}
	}

	/* Now process hosts_deny */
	if (num_hosts_deny_in > 0) {
		num_hosts_deny = num_hosts_deny_in;
		if ((hosts_deny = new net_id[num_hosts_deny]) == NULL) {
			/* failed */
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "Error: low memory");
			sc_syslog_msg_done(&handle);
			status = ENOMEM;
			return;
		}
		/*
		 * zero out the array so that if we bail out part-way
		 * through the convert_host_list function, we can safely
		 * call delete on all the ptrs, which will either be valid
		 * or NULL.
		 */
		(void) memset(hosts_deny, '\0',
		    sizeof (net_id) * num_hosts_deny);
		if (convert_host_list(hosts_deny, hosts_deny_in,
		    num_hosts_deny, deny_all) != 0) {
			status = -1;
			return;
		}
		/* If we're denying all, we don't need to keep the list */
		if (deny_all) {
			scds_syslog_debug(1,
				"tcp_wrapper init: Found deny_all\n");
			delete_host_list(hosts_deny, num_hosts_deny);
		}
	}
}

/*
 * convert_host_list
 * -------------------
 * Helper function for converting an array of strings to
 * an array of net_ids.
 */
int tcp_wrapper::convert_host_list(net_id *&hosts,
    const char *const *hosts_in, uint_t &num_hosts, bool &all)
{
	sc_syslog_msg_handle_t handle;
	uint_t i;

	/* save a local copy, because we may be modifying num_hosts */
	uint_t num_hosts_in = num_hosts;
	for (i = 0; i < num_hosts_in; i++) {
		char *lasts, *ip_str, *mask_num_str;
		long mask_num;

		/* strtok_r modifies the string, so we make a temp copy */
		char *temp_host = os::strdup(hosts_in[i]);

		/* get the ip address */
		ip_str = strtok_r(temp_host, "/", &lasts);

		/* check for NULL */
		if ((ip_str == NULL)) {
			/* failed */
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
			//
			// SCMSGS
			// @explanation
			// The allow_hosts or deny_hosts for the CRNP service
			// contained an invalid IP address. This error may
			// cause the validate method to fail or prevent the
			// cl_apid from starting up.
			// @user_action
			// Remove the offending IP address from the
			// allow_hosts or deny_hosts property.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "invalid IP address in hosts list: %s", "NULL");
			sc_syslog_msg_done(&handle);
			delete temp_host;
			return (-1);
		}

		/* check for the special "ALL" keyword, or for a * */
		if (strcmp(ip_str, "ALL") == 0 || strcmp(ip_str, "*") == 0) {
			/*
			 * If we find it, we set all to true, and
			 * stop processing the rest of the list.
			 */
			scds_syslog_debug(1, "tcp_wrapper init: Found ALL\n");
			all = true;
			delete temp_host;
			return (0);
		}

		/*
		 * Check for the special "LOCAL" keyword.
		 * If it's there, accept everything on the subnet,
		 * for all interfaces.
		 */
		if (strcmp(ip_str, "LOCAL") == 0) {
			net_id *subnets = NULL;
			uint_t num_subnets = 0;

			/* we won't use it again */
			delete temp_host;

			/* First we get the subnet info in a new array */
			if (get_subnets(&subnets, num_subnets) != 0) {
				return (-1);
			}

			/* If we didn't find any, something's wrong */
			if (num_subnets == 0) {
				(void) sc_syslog_msg_initialize(&handle,
				    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
				//
				// SCMSGS
				// @explanation
				// The allow_hosts or deny_hosts for the CRNP
				// service specifies LOCAL and the cl_apid is
				// unable to find any network interfaces on
				// the local host. This error may prevent the
				// cl_apid from starting up.
				// @user_action
				// Examine other syslog messages occurring at
				// about the same time to see if the problem
				// can be identified. Save a copy of the
				// /var/adm/messages files on all nodes and
				// contact your authorized Sun service
				// provider for assistance in diagnosing and
				// correcting the problem.
				//
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE,
				    "No interfaces found");
				sc_syslog_msg_done(&handle);
				delete_host_list(subnets, num_subnets);
				return (-1);
			}

			/*
			 * Copy the first subnet into the current spot in the
			 * array, but put the rest of them at the end, so
			 * our loop doesn't get confused.
			 */
			if (copy_one_net_id(hosts[i], subnets[0]) != 0) {
				delete_host_list(subnets, num_subnets);
				return (-1);
			}

			/*
			 * If we have more than 1 subnet, we need to create
			 * more space in our array.
			 */
			if (num_subnets > 1) {
				uint_t num_hosts_new, m;
				net_id *new_hosts;
				int ret_val;

				num_hosts_new = num_hosts + num_subnets - 1;
				if ((ret_val = copy_host_list(&new_hosts,
				    hosts, num_hosts, num_hosts_new)) != 0) {
					delete_host_list(subnets, num_subnets);
					return (ret_val);
				}

				/* now put the extra subnets at the end */
				for (m = num_hosts; m < num_hosts_new; m++) {
					if (copy_one_net_id(new_hosts[m],
					    subnets[m - num_hosts + 1])
					    != 0) {
						delete_host_list(new_hosts,
						    num_hosts_new);
						delete_host_list(subnets,
						    num_subnets);
						return (-1);
					}
				}

				/* delete the original list */
				delete_host_list(hosts, num_hosts);
				/* assign the new list to it */
				hosts = new_hosts;
				num_hosts = num_hosts_new;
			}
			/* delete the subnet list */
			delete_host_list(subnets, num_subnets);
			/* We skip trying to convert this addr normally */
			continue;
		}

		/* If we get here, we have a normal addr to conver */

		/* Try to convert ip to numeric representation */
		if (inet_pton(AF_INET, ip_str, &(hosts[i].addr)) != 1) {
			/* failed */
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "invalid IP address in hosts list: %s", ip_str);
			sc_syslog_msg_done(&handle);
			delete temp_host;
			return (-1);
		}

		/* read the mask size */
		mask_num_str = strtok_r(NULL, "/", &lasts);
		errno = 0;
		mask_num = 0;

		if (mask_num_str == NULL) {
			/* failed */
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
			//
			// SCMSGS
			// @explanation
			// The allow_hosts or deny_hosts for the CRNP service
			// contained an invalid mask. This error may prevent
			// the cl_apid from starting up.
			// @user_action
			// Remove the offending IP address from the
			// allow_hosts or deny_hosts property, or fix the
			// mask.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "invalid mask in hosts list: %s",
			    temp_host);
			sc_syslog_msg_done(&handle);
			delete temp_host;
			return (-1);

		}

		mask_num = strtol(mask_num_str, NULL, 0);
		if ((mask_num == 0) && (errno != 0)) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * strtol failed. Should never occur.
			 * This might have failed if the return
			 * value could not be represented.
			 * @user_action
			 * Contact your authorized Sun service
			 * provider to determine whether
			 * a workaround or patch is available.
			 */
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "strtol: %s", strerror(errno));
			sc_syslog_msg_done(&handle);
			delete temp_host;
			return (-1);
		}

		if (mask_num <= 0 || mask_num > 32) {
			/* failed */
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "invalid mask in hosts list: %s",
			    mask_num_str ? mask_num_str : "NULL");
			sc_syslog_msg_done(&handle);
			delete temp_host;
			return (-1);
		}

		/* now create the mask */
		create_mask(hosts[i], mask_num);

		/* also save the ip_str */
		hosts[i].ip_str = os::strdup(hosts_in[i]);
		delete temp_host;

		scds_syslog_debug(1, "tcp_wrapper init: saved %s as %d/%d\n",
		    hosts[i].ip_str, hosts[i].addr, hosts[i].mask);
	}
	return (0);
}

/*
 * get_subnets
 * -----------------
 * Searches through all the interfaces on the system, using
 * ioctl, with SIOCGLIFNUM and SIOCGLIFCONF.  For each interface,
 * looks up the subnet mask using SIOCGLIFSUBNET.
 *
 * Allocates space for all the subnets in *subnets, and records the number
 * in num_subnets.
 */
int tcp_wrapper::get_subnets(net_id **subnets, uint_t &num_subnets)
{
	int s, err, i, rc = 0;
	uint_t  j;
	ulong_t numifs, len;
	struct lifconf lifc;
	struct lifreq *pifr, templifreq;
	struct lifnum lifn;
	struct sockaddr_in *psin;
	char    *ifbuf = NULL;
	sc_syslog_msg_handle_t handle;

	/* First we need a socket on which to call ioctl */
	if ((s = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		char *err_str = strerror(errno);
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "socket: %s", err_str ? err_str : "");
		sc_syslog_msg_done(&handle);
		return (-1);
	}

	/* We only care about AF_INET for now. */
	lifn.lifn_family = AF_INET;
	lifn.lifn_flags  = 0;

	/* Get the number of AF_INET interfaces */
	if (ioctl(s, SIOCGLIFNUM, &lifn) < 0) {
		char *err_str = strerror(errno);
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The ioctl command with this option failed in the cl_apid.
		// This error may prevent the cl_apid from starting up.
		// @user_action
		// Examine other syslog messages occurring at about the same
		// time to see if the problem can be identified. Save a copy
		// of the /var/adm/messages files on all nodes and contact
		// your authorized Sun service provider for assistance in
		// diagnosing and correcting the problem.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "SIOCGLIFNUM: %s", err_str ? err_str : "");
		sc_syslog_msg_done(&handle);
		(void) close(s);
		return (-1);
	}

	/*
	 * Store the number of interfaces, and allocate our subnets array
	 * accordingly.
	*/
	numifs = (uint_t)lifn.lifn_count;
	num_subnets = numifs;
	*subnets = new net_id[num_subnets];
	if (*subnets == NULL) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Error: low memory");
		sc_syslog_msg_done(&handle);
		(void) close(s);
		return (-1);
	}
	(void) memset(*subnets, '\0',
	    sizeof (net_id) * num_subnets);
	/*
	 * Now that we've allocated memory, we can goto end_subnets when
	 * we error out.
	 */

	/* Allocate memory to store the interfaces returned by ioctl */
	len = numifs * sizeof (struct lifreq);
	if ((ifbuf = (char *)malloc(len)) == NULL) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Error: low memory");
		sc_syslog_msg_done(&handle);
		rc = -1;
		goto end_subnets;
	}

	/* ignore V6 for now... */
	lifc.lifc_family = AF_INET;
	lifc.lifc_flags = LIFC_NOXMIT;
	lifc.lifc_len = (int)len;
	lifc.lifc_buf = ifbuf;

	/* Get all the interfaces */
	if (ioctl(s, SIOCGLIFCONF, &lifc) < 0) {
		char *err_str = strerror(errno);
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The ioctl command with this option failed in the cl_apid.
		// This error may prevent the cl_apid from starting up.
		// @user_action
		// Examine other syslog messages occurring at about the same
		// time to see if the problem can be identified. Save a copy
		// of the /var/adm/messages files on all nodes and contact
		// your authorized Sun service provider for assistance in
		// diagnosing and correcting the problem.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "SIOCGLIFCONF: %s", err_str ? err_str : "");
		sc_syslog_msg_done(&handle);
		rc = -1;
		goto end_subnets;
	}

	/* Iterate through the interfaces */
	pifr = lifc.lifc_req;
	for (j = (uint_t)lifc.lifc_len, i = 0; j > 0;
	    j -= sizeof (struct lifreq), pifr++, i++) {
		psin = (struct sockaddr_in *)((void *)&pifr->lifr_addr);

		/*
		 * copy the addr out first...
		 * memcpy just returns the dest ptr, so we can
		 * ignore it.
		 * Note that the host address is returned, and stored,
		 * in network byte order.
		 */
		(void) memcpy(&((*subnets)[i].addr),
		    &(psin->sin_addr.s_addr), sizeof (struct in_addr));

		/* Convert to presentation form as well */
		char temp_buf[INET6_ADDRSTRLEN];
		if (inet_ntop(AF_INET, &(psin->sin_addr.s_addr),
		    temp_buf, INET6_ADDRSTRLEN) == NULL) {
			err = errno;
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
			(void) sc_syslog_msg_log(handle,
			    LOG_ERR, MESSAGE, "inet_ntop: %s",
			    strerror(err));
			sc_syslog_msg_done(&handle);
			rc = -1;
			goto end_subnets;
		}
		(*subnets)[i].ip_str = os::strdup(temp_buf);

		/* lookup the subnet */
		templifreq = *pifr;
		if (ioctl(s, SIOCGLIFSUBNET, &templifreq) < 0) {
			char *err_str = strerror(errno);
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
			//
			// SCMSGS
			// @explanation
			// The ioctl command with this option failed in the
			// cl_apid. This error may prevent the cl_apid from
			// starting up.
			// @user_action
			// Examine other syslog messages occurring at about
			// the same time to see if the problem can be
			// identified. Save a copy of the /var/adm/messages
			// files on all nodes and contact your authorized Sun
			// service provider for assistance in diagnosing and
			// correcting the problem.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "SIOCGLIFSUBNET: %s", err_str ? err_str : "");
			sc_syslog_msg_done(&handle);
			rc = -1;
			goto end_subnets;
		}

		/*
		 * now construct the mask.
		 * We only need the length of the subnet prefix, so we can
		 * construct a mask of that many 1s.
		 */
		create_mask((*subnets)[i], templifreq.lifr_addrlen);

		scds_syslog_debug(1, "get_subnets: found %s as 0x%x/0x%x\n",
		    (*subnets)[i].ip_str, (*subnets)[i].addr,
		    (*subnets)[i].mask);
	}

end_subnets:

	/* cleanup */
	(void) close(s);
	free(ifbuf);
	if (rc != 0) {
		delete_host_list(*subnets, num_subnets);
	}
	return (rc);
}

/*
 * create_mask
 * -------------
 * Expects host.addr to be filled in already.
 * Will create a mask of mask_num 1's, starting from the most
 * significant bit.  Will and in the mask to the addr, so that
 * incoming addresses can be compared by anding with the mask, and
 * checking the addr for equality.
 *
 * Note that 0x80000000 is 10000000000000000000000000000000 in binary.
 */
void tcp_wrapper::create_mask(net_id &host, long mask_num)
{
	host.mask = 0;

	for (int k = 0; k < mask_num; k++) {
		/* shift over and add a 1 as the MSB */
		host.mask >>= 1;
		host.mask |= (uint_t)0x80000000;
	}
	/*
	 * The mask we just constructed is in host byte order.
	 * However, we store and analyze all IP addrs in network
	 * byte order, so we must convert the mask to network byte
	 * order.
	 */
	host.mask = htonl(host.mask);

	/*
	 * and in the mask to the addr to get rid of extraneous stuff
	 * for later comparisons.
	 */
	host.addr &= host.mask;
}

/*
 * copy_host_list
 * ----------------
 * Helper function for deep copying a hosts_allow or hosts_deny list.
 *
 * If new_num_in_list is non-zero, then we allocate a new list of that size
 * (as long as its bigger than num_in_list), but we always copy over exactly
 * num_in_list elements.
 */
int tcp_wrapper::copy_host_list(net_id **new_list, net_id *old_list,
    uint_t num_in_list, uint_t &new_num_in_list)
{
	sc_syslog_msg_handle_t handle;
	uint_t i;

	if (new_num_in_list == 0) {
		new_num_in_list = num_in_list;
	} else if (new_num_in_list < num_in_list) {
		/* failed */
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The cl_apid experienced an internal error.
		// @user_action
		// Examine other syslog messages occurring at about the same
		// time to see if the problem can be identified. Save a copy
		// of the /var/adm/messages files on all nodes and contact
		// your authorized Sun service provider for assistance in
		// diagnosing and correcting the problem.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Error: attempting to copy larger list to smaller");
		sc_syslog_msg_done(&handle);
		return (0);
	}

	if ((*new_list = new net_id[new_num_in_list]) == NULL) {
		/* failed */
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Error: low memory");
		sc_syslog_msg_done(&handle);
		return (ENOMEM);
	}
	(void) memset(*new_list, '\0', sizeof (net_id) * new_num_in_list);
	for (i = 0; i < num_in_list; i++) {
		if (copy_one_net_id((*new_list)[i], old_list[i]) != 0) {
			return (ENOMEM);
		}
	}
	return (0);
}

/*
 * copy_one_net_id
 * ----------------------
 * Does a deep copy of a net_id struct.
 */
int tcp_wrapper::copy_one_net_id(net_id &dest, const net_id &src)
{
	sc_syslog_msg_handle_t handle;

	dest.addr = src.addr;
	dest.mask = src.mask;

	/* check for NULL, before string copying */
	if (src.ip_str == NULL) {
		dest.ip_str = NULL;
		return (0);
	}

	dest.ip_str = os::strdup(src.ip_str);
	if (dest.ip_str == NULL) {
		/* failed */
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Error: low memory");
		sc_syslog_msg_done(&handle);
		return (ENOMEM);
	}
	return (0);
}


/*
 * delete_host_list
 * --------------------
 * Deletes all the memory associated with the list, sets the num to 0, and
 * sets the list to NULL.
 */
void tcp_wrapper::delete_host_list(net_id *&list, uint_t &num)
{
	for (uint_t i = 0; i < num; i++) {
		delete list[i].ip_str;
	}
	delete [] list;
	list = NULL;
	num = 0;
}

/*
 * copy constructor
 * -------------------
 * Does a deep copy.
 */
tcp_wrapper::tcp_wrapper(const tcp_wrapper &src)
{
	allow_all = src.allow_all;
	deny_all = src.deny_all;

	num_hosts_deny = num_hosts_allow = 0;
	hosts_allow = hosts_deny = NULL;

	status = 0;
	int ret_val;
	if ((ret_val = copy_host_list(&hosts_allow, src.hosts_allow,
	    src.num_hosts_allow, num_hosts_allow)) != 0) {
		status = ret_val;
		return;
	}
	if ((ret_val = copy_host_list(&hosts_deny, src.hosts_deny,
	    src.num_hosts_deny, num_hosts_deny)) != 0) {
		status = ret_val;
		return;
	}
}

/*
 * destructor
 * -----------
 * Frees the memory.
 */
tcp_wrapper::~tcp_wrapper()
{
	delete_host_list(hosts_allow, num_hosts_allow);
	delete_host_list(hosts_deny, num_hosts_deny);
}

/*
 * ip_in_list
 * ------------
 * Iterates through hosts, looking for a match for ip.
 */
bool tcp_wrapper::ip_in_list(net_id hosts[], uint_t num_net_ids, uint32_t ip,
    const char *ip_str) const
{
	for (uint_t i = 0; i < num_net_ids; i++) {
		/*
		 * Compare by anding the mask with the incoming ip and
		 * testing for equality with the stored ip addr.
		 */
		if ((ip & hosts[i].mask) == hosts[i].addr) {
			scds_syslog_debug(1, "tcp_wrapper: matched %s to %s\n",
				ip_str, hosts[i].ip_str);
			return (true);
		}
	}
	return (false);
}

/*
 * verify_ip
 * --------------
 * Checks that ip is in hosts_allow, but not in hosts_deny.
 * Queries with the helper function ip_in_list.
 */
int tcp_wrapper::verify_ip(uint32_t ip, const char *ip_str) const
{
	sc_syslog_msg_handle_t handle;

	if (allow_all || ip_in_list(hosts_allow, num_hosts_allow, ip,
	    ip_str)) {
		if (deny_all || ip_in_list(hosts_deny, num_hosts_deny, ip,
		    ip_str)) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
			//
			// SCMSGS
			// @explanation
			// The cl_apid received a CRNP client connection
			// attempt from an IP address that it could not accept
			// because of its allow_hosts or deny_hosts.
			// @user_action
			// If you wish to allow access to the CRNP service for
			// the client at the specified IP address, modify the
			// allow_hosts or deny_hosts as required. Otherwise,
			// no action is required.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "Invalid connection attempted from %s: %s",
			    ip_str, "in hosts.deny");
			sc_syslog_msg_done(&handle);

			return (2);
		} else {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
			//
			// SCMSGS
			// @explanation
			// The cl_apid received a valid client connection
			// attempt from the specified IP address.
			// @user_action
			// No action required. If you do not wish to allow
			// access to the client with this IP address, modify
			// the allow_hosts or deny_hosts as required.
			//
			(void) sc_syslog_msg_log(handle, LOG_NOTICE, MESSAGE,
			    "Valid connection attempted from %s: %s",
			    ip_str, "");
			sc_syslog_msg_done(&handle);
			scds_syslog_debug(1,
				"tcp_wrapper: valid connection from %s\n",
				ip_str);
			return (0);
		}
	} else {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Invalid connection attempted from %s: %s",
		    ip_str, "not in hosts.allow");
		sc_syslog_msg_done(&handle);
		return (1);
	}
}

/*
 * The two 1-parameter forms of verify_ip are just wrappers
 * for the 2-parameter form.  They call inet_ntop or inet_pton
 * to get the presentation or numeric form of the ip respectively, then
 * call the 2-parameter form.
 */
int tcp_wrapper::verify_ip(uint32_t ip) const
{
	sc_syslog_msg_handle_t handle;
	char cp[INET6_ADDRSTRLEN];
	const char *ip_str = inet_ntop(AF_INET, &ip, cp, INET6_ADDRSTRLEN);
	if (ip_str == NULL) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Invalid connection attempted from %s: %s",
		    "", "invalid ip");
		sc_syslog_msg_done(&handle);
		return (3);
	}
	return (verify_ip(ip, ip_str));
}

int tcp_wrapper::verify_ip(const char *ip_str) const
{
	sc_syslog_msg_handle_t handle;

	uint32_t ip;
	if (inet_pton(AF_INET, ip_str, &ip) != 1) {
		/* conversion failed  */
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Invalid connection attempted from %s: %s",
		    ip_str, "invalid ip");
		sc_syslog_msg_done(&handle);
		return (3);
	}
	return (verify_ip(ip, ip_str));
}

/*
 * print
 * -------------
 * for debugging
 */
void tcp_wrapper::print() const
{
	uint_t i;
	(void) printf("hosts_allow: \n");
	for (i = 0; i < num_hosts_allow; i++) {
		(void) printf("\t%s: %d/%d\n", hosts_allow[i].ip_str,
		    hosts_allow[i].addr, hosts_allow[i].mask);
	}
	(void) printf("hosts_deny: \n");
	for (i = 0; i < num_hosts_deny; i++) {
		(void) printf("\t%s: %d/%d\n", hosts_deny[i].ip_str,
		    hosts_deny[i].addr, hosts_deny[i].mask);
	}
}
