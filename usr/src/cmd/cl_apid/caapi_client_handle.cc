/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	  "@(#)caapi_client_handle.cc 1.9	  08/05/20 SMI"

#include "caapi_client_handle.h"
#include "cl_apid_include.h"
#include "xml_module.h"

//
// Implementation of caapi_client_handle class
//
// This class provides
// reference counting capability to the caapi_client and exposes
// basic set of interfaces to outside world. This class is created
// only by caapi_client to associate its instance with particular
// client handle.  caapi_client is a friend of caapi_client_handle
// so it is able to create new instance of handles even though
// it had only private constructor
//

//
// Private constructor called from caapi_client
// which is a friend of caapi_client_handle
// input parameter is a instance of caapi_client
// with which the handle is associated
//
caapi_client_handle::caapi_client_handle(caapi_client *clientp_in):
			clientp(clientp_in)
{
}

//
// Destructor calls release_ref on the underlying client object.
//

caapi_client_handle::~caapi_client_handle()
{
	ASSERT(clientp != NULL);
	// release client reference,
	clientp->release_ref();
	clientp = NULL;
}

// Accessor functions
//
const sc_callback_info*
caapi_client_handle::get_callback_info() const
{
	return (clientp->get_callback_info());
}

//
// ishandle functions is used by caapi_reg_module
// to identify if the handle is associated with given client
//
bool
caapi_client_handle::is_handle(const caapi_client *clientp_in)
{
	return ((clientp == clientp_in) ? true : false);
}

//
// is_alive function is called to know the status of the
// client. The client can be in ACTIVE or DELETED state.
// returns TRUE when the client is ACTIVE
//
bool
caapi_client_handle::is_alive()
{
	return (clientp->is_active());
}

// this interface is used to change the client status
int
caapi_client_handle::remove_client()
{
	sc_syslog_msg_handle_t handle;

	/*
	 * Construct a fake sc_registration_instance that represents
	 * the REMOVE_CLIENT for this client.
	 */
	const sc_callback_info *cb_info = clientp->get_callback_info();
	SList<sc_event> list;
	sc_registration_instance *remove_reg = new
	    sc_registration_instance(*cb_info, list, REMOVE_CLIENT);
	if (remove_reg == NULL || remove_reg->get_construct_status() != 0) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Error: low memory");
		sc_syslog_msg_done(&handle);
		if (remove_reg != NULL) {
			delete remove_reg;
		}
		return (ENOMEM);
	}
	scds_syslog_debug(1, "Enqueueing \"fake\" remove message for %s\n",
	    cb_info->get_key());
	return (xml_module::the()->queue_registration(remove_reg));
}

//
// Simply ask our underlying caapi_client for another handle.
//
caapi_client_handle *caapi_client_handle::duplicate()
{
	return (clientp->get_client_handle());
}
