/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma	ident	"@(#)sc_event.cc	1.7	08/05/20 SMI"

#include <sys/os.h>
#include "sc_event.h"
#include "string_buffer.h"

/*
 * copy_nvlist_helper
 * -------------------
 * Helper function for doing deep copies of nvstrpair lists.
 * Called by iterate of an nvstrpair list.  Appends a new copy of
 * each element to the list passed as the void * parameter.
 */
static bool copy_nvlist_helper(nvstrpair *in_nvpair, void *out_list)
{
	/*
	 * If we fail to allocate an nvstrpair, or if it isn't
	 * constructed properly, we return false, to discontinue
	 * the copying, and force the caller to note the error.
	 */
	nvstrpair *strpair;
	if (((strpair = new nvstrpair(*in_nvpair)) == NULL) ||
	    (strpair->get_construction_status() != 0)) {
		return (false);
	}

	((SList<nvstrpair> *)out_list)->append(strpair);
	return (true);
}

/*
 * print_nvlist_helper
 * --------------------
 * Called by iterate -- prints each nvstrpair element.
 */
static bool print_nvlist_helper(nvstrpair *in_nvpair, void *)
{
	in_nvpair->print();
	return (true);
}


/*
 * constructors
 * --------------
 * Can be constructed from its attributes, from a sysevent, from
 * nothing, or from another sc_event.
 *
 * When constructed from a sysevent_t, ignores the publisher and vendor.
 */
sc_event::sc_event(const char *evt_class_in, const char *evt_subclass_in,
    const SList<nvstrpair> &list_in)
{
	construct_helper(evt_class_in, evt_subclass_in, list_in);
}

sc_event::sc_event(sysevent_t *evt)
{
	nvlist_t *nv_list_temp = NULL;
	evt_class = evt_subclass = key = NULL;
	nvlist = NULL;
	construct_status = 0;

	/* get the string attributes */
	if ((evt_class = os::strdup(sysevent_get_class_name(evt))) == NULL) {
		construct_status = ENOMEM;
		return;
	}
	if ((evt_subclass = os::strdup(sysevent_get_subclass_name(evt))) ==
	    NULL) {
		construct_status = ENOMEM;
		return;
	}

	/* get the name/value pairs */
	if (sysevent_get_attr_list(evt, &(nv_list_temp)) != 0) {
		construct_status = errno;
		return;
	}

	/* create our name/value pairs list */
	/* cstyle doesn't understand templates */
	/*CSTYLED*/
	if ((nvlist = new SList<nvstrpair>) == NULL) {
		construct_status = ENOMEM;
		return;
	}

	/* construct an nvstrpair from each nvpair and add it to our list */
	nvpair_t *pair = NULL;
	nvstrpair *strpair = NULL;
	int ret = -1;
	while ((pair = nvlist_next_nvpair(nv_list_temp, pair)) != NULL) {
		if (((strpair = new nvstrpair(pair)) == NULL) ||
		    ((ret = strpair->get_construction_status()) != 0)) {
			construct_status = strpair ? ret : ENOMEM;
			return;
		}
		nvlist->append(new nvstrpair(pair));
	}

	/* We don't need this list anymore */
	nvlist_free(nv_list_temp);
}

sc_event::sc_event()
{
	construct_status = 0;
	evt_class = NULL;
	evt_subclass = NULL;
	key = NULL;

	/* cstyle doesn't understand templates */
	/*CSTYLED*/
	if ((nvlist = new SList<nvstrpair>) == NULL) {
		construct_status = ENOMEM;
		return;
	}
}

sc_event::sc_event(const sc_event &src)
{
	construct_helper(src.get_class(), src.get_subclass(),
	    src.get_nvlist());
}


/*
 * construct_helper
 * ------------------
 * Does the main construction given the attributes.
 */
void sc_event::construct_helper(const char *evt_class_in,
    const char *evt_subclass_in, const SList<nvstrpair> &list_in)
{
	evt_class = NULL;
	evt_subclass = NULL;
	key = NULL;

	/*
	 * Lint thinks this assignment is a memory leak because
	 * we're not doing it in the constructor.
	 */
	nvlist = NULL; /*lint !e423 */
	construct_status = 0;

	if (evt_class_in != NULL) {
		if ((evt_class = os::strdup(evt_class_in)) == NULL) {
			construct_status = ENOMEM;
			return;
		}
	}
	if (evt_subclass_in != NULL) {
		if ((evt_subclass = os::strdup(evt_subclass_in)) == NULL) {
			construct_status = ENOMEM;
			return;
		}
	}

	/*
	 * allocate new memory, and make copies of all the
	 * list elements
	 */

	/* cstyle doesn't understand templates */
	/*CSTYLED*/
	if ((nvlist = new SList<nvstrpair>) == NULL) {
		construct_status = ENOMEM;
		return;
	}

	SList<nvstrpair> *temp_list = const_cast<SList<nvstrpair> *>(
	    &list_in);

	/*
	 * If we get back non-NULL from iterate, it means it stopped
	 * part-way, because something went wrong.
	 * The only things that can go wrong are memory errors, so
	 * we note it as such.
	 */
	if (temp_list->iterate(copy_nvlist_helper, (void *)nvlist) != NULL) {
		construct_status = ENOMEM;
		return;
	}
}


/*
 * op=
 * -------------
 * Combination of destructor and constructor.
 * Frees the old memory, calls construct_helper to do the new
 * construction.
 */
/* cstyle doesn't understand operator overloading */
/*CSTYLED*/
sc_event &sc_event::operator=(const sc_event &src)
{
	if (this != &src) {
		delete [](evt_class);
		delete [](evt_subclass);
		delete [](key);

		nvstrpair *pair;
		if (nvlist != NULL) {
			while ((pair = nvlist->reapfirst()) != NULL) {
				delete(pair);
			}
		}
		delete nvlist;

		construct_helper(src.get_class(), src.get_subclass(),
		    src.get_nvlist());
	}
	return (*this);
}


/*
 * destructor
 * ------------
 * Frees the memory of all the attributes. Caller of delete should
 * ensure there are no dangling references to the attributes.
 */
sc_event::~sc_event()
{
	delete [](evt_class);
	delete [](evt_subclass);
	delete [](key);
	nvstrpair *pair;
	if (nvlist != NULL) {
		while ((pair = nvlist->reapfirst()) != NULL) {
			delete(pair);
		}
	}
	delete nvlist;
}

/*
 * Accessor functions
 * ------------------
 * Return references to the attributes.
 */
const char *sc_event::get_class() const
{
	return (evt_class);
}

const char *sc_event::get_subclass() const
{
	return (evt_subclass);
}

const char *sc_event::get_key()
{
	if (key != NULL) {
		return (key);
	}

	if (evt_class == NULL || evt_subclass == NULL) {
		return (NULL);
	}

	string_buffer str_buf;
	if (str_buf.append("%s.%s", evt_class, evt_subclass) == -1) {
		return (NULL);
	}
	key = os::strdup(str_buf.get_buf());
	return (key);
}

const SList<nvstrpair> &sc_event::get_nvlist() const
{
	return (*nvlist);
}

void sc_event::print() const
{
	(void) printf("class: %s\n", evt_class ? evt_class : "");
	(void) printf("subclass: %s\n", evt_subclass ?
	    evt_subclass : "");

	if (nvlist == NULL) {
		(void) printf("No list\n");
		return;
	}
	(void) printf("\tList:\n");
	(void) nvlist->iterate(print_nvlist_helper, (void *)NULL);
}

/*
 * list_find_pair_helper
 * --------------------------
 * Helper function for iterating over an nvstrpair list looking
 * for an nvstrpair.
 *
 * If the two pairs are equal, return false, so the search
 * ends, and iterate returns a ptr to the matching pair.
 *
 * Otherwise, return true, so the search continues.
 */
bool
list_find_pair_helper(nvstrpair *pair_1, void *pair_2)
{
	if (*pair_1 == *((nvstrpair *)pair_2)) {
		return (false);
	}
	return (true);
}

/*
 * list_find_name_helper
 * -------------------------
 * Helper function for iterating over an nvstrpair list looking
 * for an nvstrpair given only its name.  So we only compare
 * names.
 *
 * If the two names are equal, return false, so the search
 * ends, and iterate returns a ptr to the matching pair.
 *
 * Otherwise, return true, so the search continues.
 */
bool
list_find_name_helper(nvstrpair *pair_1, void *name_2)
{
	if (strcmp(pair_1->get_name(), (char *)name_2) == 0) {
		return (false);
	}
	return (true);
}

/*
 * lookup_nvpair
 * -----------------
 * Return a pointer to the pair if found, otherwise returns NULL.
 * Lookup can work on a name/value pair explicitly or just a name.
 *
 * Uses linear search of list, and is thus O(n).
 */
const nvstrpair *sc_event::lookup_nvpair(const nvstrpair &in_pair) const
{
	return (nvlist->iterate(list_find_pair_helper, (void *)&in_pair));
}

const nvstrpair *sc_event::lookup_nvpair(const char *in_name) const
{
	return (nvlist->iterate(list_find_name_helper, (void *)in_name));
}

/*
 * list_comp_helper
 * ------------------
 * Helper function for iterating over an nvstrpair list, trying to
 * make sure it's equal to another list (passed as the void * param).
 * This means that every nvstrpair from the first list is found in the
 * second list.
 *
 * Caller should previously make sure the lists are the same length.
 */
bool
list_comp_helper(nvstrpair *list_elem, void *rhs_list)
{
	/*
	 * If iterate finds a match, it will return a non -NULL ptr.
	 * If it doesn't find a match, it will return NULL.
	 * If we don't find a match, we return false, so that
	 * the outer iterate will end, and the caller will know that
	 * the lists are not equal.
	 *
	 * If we find a match, return true, so the search through
	 * the outer list will continue.
	 */
	SList<nvstrpair> *temp_list = (SList<nvstrpair> *)rhs_list;
	if (temp_list->iterate(list_find_pair_helper, list_elem) == NULL) {
		return (false);
	}
	return (true);
}


/*
 * operator==
 * -----------
 * For comparison with other sc_events.
 */
/*
 * cstyle doesn't understand operator overloading */
/*CSTYLED*/
bool sc_event::operator==(const sc_event &rhs) const
{
	if (this == &rhs) {
		return (true);
	}

	/*
	 * first, compare the CLASS
	 * The ways it can be false is if one's NULL and the other's not,
	 * or if they aren't equal with string compare.
	 */
	const char *rhs_class = rhs.get_class();
	if ((evt_class == NULL && rhs_class != NULL) || (rhs_class == NULL &&
	    evt_class != NULL)) {
		return (false);
	}
	/*
	 * We know that, if evt_class is not NULL, rhs_class is not NULL
	 * as well.  So suppress the lint error.
	 */
	/*lint -e668 */
	if (evt_class != NULL && strcmp(evt_class, rhs_class) != 0) {
		return (false);
	}
	/*lint +e668 */

	/*
	 * Next, compaire the SUBCLASS
	 */
	const char *rhs_subclass = rhs.get_subclass();
	if ((evt_subclass == NULL && rhs_subclass != NULL) ||
	    (rhs_subclass == NULL && evt_subclass != NULL)) {
		return (false);
	}
	/*
	 * We know that, if evt_subclass is not NULL, rhs_subclass is not NULL
	 * as well.  So suppress the lint error.
	 */
	/*lint -e668 */
	if (evt_subclass != NULL && strcmp(evt_subclass, rhs_subclass) != 0) {
		return (false);
	}
	/*lint +e668 */


	/*
	 * Now compare the lists
	 */

	const SList<nvstrpair> &temp_list_const = rhs.get_nvlist();
	SList<nvstrpair> *temp_list = const_cast<SList<nvstrpair> *>(
	    &temp_list_const);

	if (nvlist->count() != temp_list->count()) {
		return (false);
	}

	/*
	 * Iterate through the list, checking that each element is
	 * contained in the temp_list.
	 * If we successfully find all the elements, iterate will return
	 * NULL.  If we don't, it will return non-NULL.
	 */
	if (nvlist->iterate(list_comp_helper, (void *)temp_list) == NULL) {
		return (true);
	}
	return (false);
}
