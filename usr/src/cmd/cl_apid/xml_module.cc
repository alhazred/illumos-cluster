/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma	ident	"@(#)xml_module.cc	1.16	08/05/20 SMI"

#include <pthread.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/cl_assert.h>
#include <orb/fault/fault_injection.h>
#include <libsysevent.h>
#include <libxml/parser.h>
#include <libxml/catalog.h>

#include "xml_module.h"
#include "cl_apid_include.h"
#include "cl_apid_error.h"
#include "caapi_reg_cache.h"
#include "caapi_client_handle.h"
#include "reg_comm_module.h"
#include "evt_comm_module.h"
#include "sc_event_cache.h"

/*
 * debug variable set from command-line, and used
 * to control debug output.
 */
extern bool debug;

/*
 * First, define the methods that are run by the two main threads in
 * this module.
 */

/*
 * xml_reg_thread
 * ----------------
 * This function is run by the xml registration thread.
 * It loops, waiting for registrations to arrive on the reg_queue.
 * When one or more registrations arrive, it dequeues all of them,
 * and asks the cache module to process the registrations.
 *
 * The cache module sets an error code in each of the sc_registration_instance
 * objects, which we use to set up a reply for the client.
 */
void xml_module::xml_reg_thread()
{
#ifdef _FAULT_INJECTION
	if (fault_triggered(FAULTNUM_CLAPI_XML_REG_THREAD_R,
		NULL, NULL)) {
		exit(1);
	}
#endif
	FAULTPT_CLAPI(FAULTNUM_CLAPI_XML_REG_THREAD_B,
	FaultFunctions::generic);

	scds_syslog_debug(1, "xml_reg_thread begin\n");

	sc_syslog_msg_handle_t handle;
	sc_xml_reply *reply = NULL;
	int res;

	// Create a list for the registrations we currently process.
	SList<sc_registration_instance> *cur_regs = new
	    SList<sc_registration_instance>;
	/*
	 * Check for memory allocation failure.  We're still early
	 * enough that we can just bail out.
	 */
	if (cur_regs == NULL) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Error: low memory");
		sc_syslog_msg_done(&handle);
		exit(1);
	}

	sc_registration_instance *one_reg = NULL;

	// Get pointers to the various modules
	sc_event_cache &event_cache = sc_event_cache::the();
	caapi_reg_cache &reg_cache = caapi_reg_cache::the();

	// Can't get pointer to reg_comm module because it hasn't
	// been created yet.

	/*
	 * Loop forever, waiting for registrations to arrive, or for
	 * the module to be shutdown.
	 */
	while (true) {
		/*
		 * Grab the lock protecting the reg_queue and the shutdown_reg.
		 */
		CL_PANIC(pthread_mutex_lock(&reg_lock) == 0);

		/*
		 * Wait for there to be something on the queue or
		 * to be told to shutdown.
		 */
		while (reg_queue.peek() == NULL && !shutdown_reg) {
			CL_PANIC(pthread_cond_wait(&reg_cond, &reg_lock) == 0);
		}

		/*
		 * If we're here we could either be told to be shutdown,
		 * there's something on the queue, or both.
		 * If just something on the queue: process it.
		 * If just shutting down: break out of the big loop.
		 * If both, process what's on the queue, then loop back
		 * again.  If there is still something on the queue when
		 * we get back, keep processing.  Eventually there will
		 * be nothing on the queue.  At that point we break out of
		 * the loop.
		 */
		if (reg_queue.peek() == NULL) {

			/*
			 * If we wake up, but there's nothing on the list,
			 * we're shutting down.
			 */
			CL_PANIC(pthread_mutex_unlock(&reg_lock) == 0);

			scds_syslog_debug(1,
				"registration processing shutting down\n");
			// Here's where we break out of the infinite loop.
			break;
		}

		/*
		 * Read the registrations off the queue.
		 * We trade-in our old list and get a pointer to a new
		 * one back.
		 */
		cur_regs = reg_queue.dequeue_all(cur_regs);

		// Release the lock for someone else.
		CL_PANIC(pthread_mutex_unlock(&reg_lock) == 0);

		/*
		 * the two "check" functions will remove invalid
		 * registrations and send replies to them.
		 * After the calls, all remaining registrations should
		 * be valid.
		 */
		check_registration_parsing(cur_regs);
		check_registration_events(cur_regs);

		// call into the cache module to actually do the
		// registrations
		scds_syslog_debug(1, "About to call update_clients\n");
		res = reg_cache.update_clients(*cur_regs);

		FAULTPT_CLAPI(FAULTNUM_CLAPI_UPDATE_CACHE_A,
		    FaultFunctions::generic);

		scds_syslog_debug(1, "Finished update clients\n");
		if (res != 0) {
			// syslog a message
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
			//
			// SCMSGS
			// @explanation
			// The cl_apid was unable to perform the requested
			// registration for the CRNP service.
			// @user_action
			// Examine other syslog messages occurring at about
			// the same time to see if the problem can be
			// identified. Save a copy of the /var/adm/messages
			// files on all nodes and contact your authorized Sun
			// service provider for assistance in diagnosing and
			// correcting the problem.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR,
			    MESSAGE, "Bulk registration failed");
			sc_syslog_msg_done(&handle);

			// assume we set individual clients as well,
			// so just carry on as normal
		}

		// Construct reply based on result for each client.
		// Also request snapshots for all the clients.
		cur_regs->atfirst();
		bool is_remove = false;
		while ((one_reg = cur_regs->get_current()) != NULL) {
			cur_regs->advance();
			if (one_reg->get_reg_type() == REMOVE_CLIENT) {
				is_remove = true;
			}
			reply = get_reg_reply(one_reg->get_reg_res());
			scds_syslog_debug(1,
			    "Got reply: %d from cache.  Will send: \n",
			    one_reg->get_reg_res());
			if (debug)
				reply->print();

			// call into reg comm module to send reply
			(void) reg_comm_module::the()->send_reply_message(
			    one_reg->get_sock(), reply->get_xml());

			/*
			 * Ask the event cache to send messages for
			 * all events for which the client registered.
			 *
			 * The cache will make copies of anything
			 * that it needs to save.
			 */
			if (event_cache.send_cached_event(one_reg) == ENOMEM) {
				scds_syslog_debug(1,
				    "No memory, could not send evts\n");
				/* what else can we do ??? */
			}
		}
		/*
		 * If we removed a client, tell the event comm module,
		 * so that it can clean up this client.
		 */
		if (is_remove) {
			scds_syslog_debug(1,
			    "Found a remove: telling evt_comm\n");
			evt_comm_module::the()->check_remove_client();
		}


		//
		// must remove every element from the list, and delete it.
		//
		cur_regs->dispose();

	}
	/*
	 * If we get here, we're shutting down.
	 * There should not be any pending registrations.
	 * We need to delete the current registration list.
	 */
	cur_regs->dispose();
	delete cur_regs;
	pthread_exit(NULL);
}

void xml_module::check_registration_parsing(
    SList<sc_registration_instance> *cur_regs)
{
	sc_registration_instance *one_reg = NULL;
	sc_syslog_msg_handle_t handle;
	reg_comm_module *reg_comm = reg_comm_module::the();

	// Check each registration to make sure the request
	// was well-formed and valid.
	cur_regs->atfirst();
	while ((one_reg = cur_regs->get_current()) != NULL) {
		cur_regs->advance();

		if (debug) {
			one_reg->print();
		}

		int res;
		if ((res = one_reg->get_parse_result()) != PARSED) {
			// syslog a message
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
			//
			// SCMSGS
			// @explanation
			// A CRNP client submitted a registration message that
			// could not be parsed.
			// @user_action
			// No action required. This message represents a CRNP
			// client error.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR,
			    MESSAGE, "Unparsable registration");
			sc_syslog_msg_done(&handle);

			// construct a reply based on the error
			sc_xml_reply *reply = get_parse_error_reply(res);

			// call into reg comm module to send reply
			// Nothing we can do about it if the message
			// can't be sent.
			(void) reg_comm->send_reply_message(
			    one_reg->get_sock(), reply->get_xml());

			// remove from our list
			// Can safely assert, because we know the one_reg
			// is in the list.
			CL_PANIC(cur_regs->erase(one_reg));

			// Here we can delete the reg, because no
			// one has ptrs to it.
			delete one_reg;
		}
	}
}

void xml_module::check_registration_events(
    SList<sc_registration_instance> *cur_regs)
{
	sc_registration_instance *one_reg = NULL;
	reg_comm_module *reg_comm = reg_comm_module::the();

	sc_event_cache::the().approve_registrations(*cur_regs);

	// Check each registration to make sure the request
	// was approved
	cur_regs->atfirst();
	while ((one_reg = cur_regs->get_current()) != NULL) {
		cur_regs->advance();

		if (one_reg->get_reg_res() != 0) {
			// construct a reply based on the error
			sc_xml_reply *reply = get_reg_reply(
			    one_reg->get_reg_res());

			// call into reg comm module to send reply
			// Nothing we can do about it if the message
			// can't be sent.
			(void) reg_comm->send_reply_message(
			    one_reg->get_sock(), reply->get_xml());

			// remove from our list
			// Can safely assert, because we know the one_reg
			// is in the list.
			CL_PANIC(cur_regs->erase(one_reg));
			scds_syslog_debug(1,
			    "removed reg from list; deleting it");

			// Here we can delete the reg, because no
			// one has ptrs to it.
			delete one_reg;
		}
	}
}


/*
 * xml_evt_thread
 * ----------------
 * This function is run by the xml event thread.
 * It loops, waiting for events to arrive on the evt_queue.
 * When an event arrives, it dequeues it, looks up the
 * clients that want that event in the cache, asks the snapshot module
 * to cull the client list, then passes it to th ommunication module to
 * send the event to those clients.
 */
void xml_module::xml_evt_thread()
{
#ifdef _FAULT_INJECTION
	if (fault_triggered(FAULTNUM_CLAPI_XML_EVT_THREAD_R,
		NULL, NULL)) {
		exit(1);
	}
#endif
	FAULTPT_CLAPI(FAULTNUM_CLAPI_XML_EVT_THREAD_B,
	FaultFunctions::generic);

	sc_syslog_msg_handle_t handle;
	sc_xml_event *evt = NULL;

	scds_syslog_debug(1, "xml_evt_thread begin\n");

	sc_event_cache &event_cache = sc_event_cache::the();
	evt_comm_module *evt_comm = evt_comm_module::the();
	caapi_reg_cache &reg_cache = caapi_reg_cache::the();

	/*
	 * Loop forever, waiting for events to arrive, or for
	 * the module to be shutdown.
	 */
	while (true) {
		/*
		 * Grab the lock protecting the evt_queue and the shutdown_evt.
		 */
		CL_PANIC(pthread_mutex_lock(&evt_lock) == 0);

		/*
		 * Wait for there to be something on the queue or
		 * to be told to shutdown.
		 */
		while (evt_queue.peek() == NULL && !shutdown_evt) {
			CL_PANIC(pthread_cond_wait(&evt_cond, &evt_lock) == 0);
		}

		/*
		 * If we're here we could either be told to be shutdown,
		 * there's something on the queue, or both.
		 * If just something on the queue: process it.
		 * If just shutting down: break out of the big loop.
		 * If both, process what's on the queue, then loop back
		 * again.  If there is still something on the queue when
		 * we get back, keep processing.  Eventually there will
		 * be nothing on the queue.  At that point we break out of
		 * the loop.
		 */
		if (evt_queue.peek() == NULL) {

			/*
			 * If we wake up, but there's nothing on the list,
			 * we're shutting down.
			 */
			CL_PANIC(pthread_mutex_unlock(&evt_lock) == 0);

			scds_syslog_debug(1,
			    "event processing shutting down\n");

			// Here's where we break out of the infinite loop.
			break;
		}

		/*
		 * Read the first event off the queue.
		 */
		evt = evt_queue.dequeue();

		// Release the lock for someone else.
		CL_PANIC(pthread_mutex_unlock(&evt_lock) == 0);

		if (debug) {
			scds_syslog_debug(1, "Read event: ");
			evt->print();
		}
		if (evt->get_parse_result() != PARSED) {
			// syslog a message
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
			//
			// SCMSGS
			// @explanation
			// The cl_apid was unable to process an incoming
			// sysevent. The message will be dropped.
			// @user_action
			// Examine other syslog messages occurring at about
			// the same time to see if the problem can be
			// identified. Save a copy of the /var/adm/messages
			// files on all nodes and contact your authorized Sun
			// service provider for assistance in diagnosing and
			// correcting the problem.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "Unparsed sysevent received");
			sc_syslog_msg_done(&handle);
			delete evt;
		} else {
			SList<caapi_client_handle> *cli_list;

			// we're clear
			// call into the cache module to get the client list.

			cli_list = reg_cache.lookup_clients(evt);
			if (cli_list == NULL) {
				scds_syslog_debug(1, "No matches for event\n");
			} else {

				if (debug) {
					scds_syslog_debug(1,
					    "Found the following matches:\n");
					cli_list->atfirst();
					caapi_client_handle *one_cli;
					while ((one_cli = cli_list->
					    get_current()) != NULL) {
						cli_list->advance();
						scds_syslog_debug(1,
						    "s\n", one_cli->
						    get_callback_info()->
						    get_key());
					}
					scds_syslog_debug(1, "n");
				}


				FAULTPT_CLAPI(FAULTNUM_CLAPI_SEND_EVT_REPLY_B,
				    FaultFunctions::generic);

				// call into evt comm to send events
				// Don't know what to do if it fails... ???
				(void) evt_comm->send_message(evt->get_xml(),
				    cli_list);

				/*
				 * Delete our list of clients
				 * First call erase_list, which removes the
				 * elements, but does not call delete on them.
				 * We don't want to delete the elements
				 * because the evt_comm_module is using
				 * them to deliver the messages.
				 */
				cli_list->erase_list();
				delete cli_list;
			}

			/*
			 * Give the event to the event cache to cache
			 */
			if (event_cache.store_event(evt) != 0) {
				scds_syslog_debug(1,
				    "Event cache failed to store evt\n");
				delete evt;
			}
		}
	}
	/*
	 * If we get here, we're shutting down.
	 * There should not be any pending events.
	 */
	pthread_exit(NULL);
}


/*
 * define/initialize the static members
 */
xml_module *xml_module::the_xml_module = NULL;
pthread_t xml_module::evt_thr;
pthread_t xml_module::reg_thr;


/*
 * The strings used in the xml replies
 */
const char *xml_module::parse_error_strs[NUM_PARSE_REPLIES] = {
	"Error: xml formatted incorrectly",
	"",
	"Error: xml formatted incorrectly",
	"Error: xml not to specification"};

const char *xml_module::error_strs[NUM_ERROR_REPLIES] = {
	"System Error: inter-node networking error",
	"System Error: unable to save registration to permanent storage",
	"Low resources: too many clients",
	"Violating constraints: too many events",
	"Violating constraints: too many nvpairs",
	"Unable to authenticate request",
	"Unable to complete request because client could not be found",
	"Invalid operation requested",
	"Invalid request",
	"Unable to complete request because client has been deleted",
	"Unknown event type in registration",
	"Event type failed to specify all required name/value pairs"};

const char *xml_module::warning_strs[NUM_WARNING_REPLIES] = {
	"Error: client already exists"};

const reply_code xml_module::parse_error_codes[NUM_PARSE_REPLIES] = {
	MALFORMED_MSG, OK, MALFORMED_MSG, INVALID_MSG};
const reply_code xml_module::error_codes[NUM_ERROR_REPLIES] = {
	SYSTEM_ERROR,
	SYSTEM_ERROR,
	LOW_RESOURCES,
	FAIL,
	FAIL,
	FAIL,
	FAIL,
	RETRY,
	RETRY,
	FAIL,
	FAIL,
	FAIL};

const reply_code xml_module::warning_codes[NUM_WARNING_REPLIES] = {
	FAIL};

/*
 * ctor/dtor
 * -------------
 * Note that these are private, so ctor only called from initialize, and
 * dtor only from shutdown.
 *
 * Constructor initializes the two mutexes and condition variables.
 * Exits the program if they don't initialize properly.
 */
xml_module::xml_module()
{
#ifdef _FAULT_INJECTION
	if (fault_triggered(FAULTNUM_CLAPI_XML_MODULE_R,
		NULL, NULL)) {
		exit(1);
	}
#endif
	sc_syslog_msg_handle_t handle;
	int err;

	/*
	 * Check the status of the two queues.
	 */
	if (evt_queue.get_construct_status() != 0) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Error: low memory");
		sc_syslog_msg_done(&handle);
		exit(1);
	}
	if (reg_queue.get_construct_status() != 0) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Error: low memory");
		sc_syslog_msg_done(&handle);
		exit(1);
	}

	/*
	 * Create the mutexes.
	 */
	if ((err = pthread_mutex_init(&evt_lock, NULL)) != 0) {
		char *err_str = strerror(err);
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "pthread_mutex_init: %s", err_str ? err_str : "");
		sc_syslog_msg_done(&handle);
		exit(1);
	}
	if ((err = pthread_mutex_init(&reg_lock, NULL)) != 0) {
		char *err_str = strerror(err);
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "pthread_mutex_init: %s", err_str ? err_str : "");
		sc_syslog_msg_done(&handle);
		exit(1);
	}
	if ((err = pthread_cond_init(&evt_cond, NULL)) != 0) {
		char *err_str = strerror(err);
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "pthread_cond_init: %s", err_str ? err_str : "");
		sc_syslog_msg_done(&handle);
		exit(1);
	}
	if ((err = pthread_cond_init(&reg_cond, NULL)) != 0) {
		char *err_str = strerror(err);
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "pthread_cond_init: %s", err_str ? err_str : "");
		sc_syslog_msg_done(&handle);
		exit(1);
	}
	shutdown_evt = shutdown_reg = false;

	// Create all the possible replies ahead of time, so we
	// don't have to allocate memory at the time.
	if (create_xml_replies() != 0) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Error: low memory");
		sc_syslog_msg_done(&handle);
		exit(1);
	}
}

xml_module::~xml_module()
{
	(void) pthread_cond_destroy(&evt_cond);
	(void) pthread_cond_destroy(&reg_cond);
	(void) pthread_mutex_destroy(&evt_lock);
	(void) pthread_mutex_destroy(&reg_lock);

	/*
	 * Delete our error replies
	 */
	int i;
	for (i = 0; i < NUM_PARSE_REPLIES; i++) {
		delete parse_error_replies[i];
	}
	for (i = 0; i < NUM_ERROR_REPLIES; i++) {
		delete error_replies[i];
	}
	for (i = 0; i < NUM_WARNING_REPLIES; i++) {
		delete warning_replies[i];
	}
	delete unknown_error_reply;
	delete ok_reply;
}


/*
 * Wrapper functions for thread creation.
 * They get a reference to teh one xml_module, and
 * call the method on that object that we really want to run.
 *
 * Defined with "C" linkage becasue they're called by a C function.
 */
extern "C" void xml_evt_thread_wrapper(void *)
{
	xml_module::the()->xml_evt_thread();
}

extern "C" void xml_reg_thread_wrapper(void *)
{
	xml_module::the()->xml_reg_thread();
}


/*
 * catalog_path
 * -------------
 * This path controls where the external entity loader looks for the
 * catalog to the dtds.
 * It can be relative or absolute, but absolute is reccommended.  I think
 * a relative path would work off of the directory from which the daemon
 * is launched.
 */
char *xml_module::catalog_path = os::strdup("");

const char *xml_module::get_catalog_path()
{
	return (catalog_path);
}

void xml_module::set_catalog_path(const char *in_path)
{
	delete [](catalog_path);
	catalog_path = os::strdup(in_path);
}

int xml_module::initialize(char *catalog_path_in)
{
#ifdef _FAULT_INJECTION
	void		*f_argp;
	uint32_t	f_argsize;
	if (fault_triggered(FAULTNUM_CLAPI_XML_INITIALIZE_R,
		&f_argp, &f_argsize)) {
		ASSERT(f_argsize == sizeof (int));
		return (*((int *)f_argp));
	}
#endif
	FAULTPT_CLAPI(FAULTNUM_CLAPI_XML_INITIALIZE_B,
	FaultFunctions::generic);

	sc_syslog_msg_handle_t handle;

	scds_syslog_debug(1, "Initializing xml_module\n");

	/* initialize should only be called once */
	CL_PANIC(the_xml_module == NULL);

	/* initialize libxml2 */
	set_catalog_path(catalog_path_in);

	if (xmlLoadCatalog(catalog_path_in) != 0) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The cl_apid was unable to load the xml catalog for the
		// dtds. No validation will be performed on CRNP xml messages.
		// @user_action
		// No action is required. If you want to diagnose the problem,
		// examine other syslog messages occurring at about the same
		// time to see if the problem can be identified. Save a copy
		// of the /var/adm/messages files on all nodes and contact
		// your authorized Sun service provider for assistance in
		// diagnosing and correcting the problem.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Error: failed to load catalog %s", catalog_path_in);
		sc_syslog_msg_done(&handle);
	} else {
		scds_syslog_debug(1, "Loaded catalog %s\n", catalog_path_in);
	}

	/* create the one instance of xml_module */
	the_xml_module = new xml_module();
	if (the_xml_module == NULL) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Error: low memory");
		sc_syslog_msg_done(&handle);
		return (ENOMEM);
	}

	/*
	 * Spawn the two threads in the xml module.  When we
	 * create them, have them start with the wrapper functions
	 * defined above, because we can't have a C function (pthread_create)
	 * calling a C++ method on an object, because it doesn't know how
	 * to pass the this pointer properly.
	 *
	 */
	int ret;
	if ((ret = pthread_create(&reg_thr, NULL,
	    (void *(*)(void *))xml_reg_thread_wrapper, NULL)) != 0) {
		char *err_str = strerror(ret);
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "pthread_create: %s", err_str ? err_str : "");
		sc_syslog_msg_done(&handle);
		return (ret);
	}
	if ((ret = pthread_create(&evt_thr, NULL,
	    (void *(*)(void *))xml_evt_thread_wrapper, NULL)) != 0) {
		char *err_str = strerror(ret);
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "pthread_create: %s", err_str ? err_str : err_str);
		sc_syslog_msg_done(&handle);
		return (ret);
	}

	return (0);
}

void xml_module::shutdown_finish()
{
	scds_syslog_debug(1, "Finishing shutdown of xml module\n");
	// delete ourselves
	delete the_xml_module;
	the_xml_module = NULL;
}

void xml_module::shutdown_start()
{
	/* terminate the threads */

	// tell them to shutdown
	CL_PANIC(pthread_mutex_lock(&reg_lock) == 0);
	shutdown_reg = true;
	CL_PANIC(pthread_cond_broadcast(&reg_cond) == 0);
	CL_PANIC(pthread_mutex_unlock(&reg_lock) == 0);

	CL_PANIC(pthread_mutex_lock(&evt_lock) == 0);
	shutdown_evt = true;
	CL_PANIC(pthread_cond_broadcast(&evt_cond) == 0);
	CL_PANIC(pthread_mutex_unlock(&evt_lock) == 0);

	// wait for them to exit
	(void) pthread_join(evt_thr, NULL);
	(void) pthread_join(reg_thr, NULL);
}

xml_module *xml_module::the()
{
	// make sure client has called initialize
	CL_PANIC(the_xml_module != NULL);
	return (the_xml_module);
}

int xml_module::create_xml_replies()
{
#ifdef _FAULT_INJECTION
	void		*f_argp;
	uint32_t	f_argsize;
	if (fault_triggered(FAULTNUM_CLAPI_XML_CREATE_XML_REPLY_R,
		&f_argp, &f_argsize)) {
		ASSERT(f_argsize == sizeof (int));
		return (*((int *)f_argp));
	}
#endif
	int i;

	/*
	 * Create the replies.
	 */
	for (i = 0; i < NUM_PARSE_REPLIES; i++) {
		parse_error_replies[i] = new sc_xml_reply(parse_error_codes[i],
		    parse_error_strs[i]);
		if (parse_error_replies[i] == NULL ||
		    parse_error_replies[i]->get_construct_status() != 0) {
			return (ENOMEM);
		}
	}

	for (i = 0; i < NUM_ERROR_REPLIES; i++) {
		error_replies[i] = new sc_xml_reply(error_codes[i],
		    error_strs[i]);
		if (error_replies[i] == NULL ||
		    error_replies[i]->get_construct_status() != 0) {
			return (ENOMEM);
		}
	}

	for (i = 0; i < NUM_WARNING_REPLIES; i++) {
		warning_replies[i] = new sc_xml_reply(warning_codes[i],
		    warning_strs[i]);
		if (warning_replies[i] == NULL ||
		    warning_replies[i]->get_construct_status() != 0) {
			return (ENOMEM);
		}
	}

	unknown_error_reply = new sc_xml_reply(FAIL, "Unknown error");
	if (unknown_error_reply == NULL ||
	    unknown_error_reply->get_construct_status() != 0) {
		return (ENOMEM);
	}

	ok_reply = new sc_xml_reply(OK, "");
	if (ok_reply == NULL || ok_reply->get_construct_status() != 0) {
		return (ENOMEM);
	}

	return (0);
}

sc_xml_reply *xml_module::get_reg_reply(int error)
{
	sc_syslog_msg_handle_t handle;

	/*
	 */
	if (error == 0) {
		return (ok_reply);
	}
	if (error >= CLEP_FIRST_ERROR && error <= CLEP_LAST_ERROR) {
		return (error_replies[error - CLEP_FIRST_ERROR]);
	} else if (error >= CLEP_FIRST_WAR && error <= CLEP_LAST_WAR) {
		return (warning_replies[error - CLEP_FIRST_WAR]);
	} else {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The cl_apid encountered an internal error.
		// @user_action
		// Save a copy of the /var/adm/messages files on all nodes and
		// contact your authorized Sun service provider for assistance
		// in diagnosing and correcting the problem.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Error: unknown error code\n", error);
		sc_syslog_msg_done(&handle);
		return (unknown_error_reply);
	}
}

sc_xml_reply *xml_module::get_parse_error_reply(int error)
{
	/*
	 * Return the pre-created sc_xml_reply object.
	 * The parse error code indexes directly into the
	 * error_replies array.
	 */
	return (parse_error_replies[error]);
}

int xml_module::queue_registration(sc_registration_instance *reg)
{
	CL_PANIC(pthread_mutex_lock(&reg_lock) == 0);

	FAULTPT_CLAPI(FAULTNUM_CLAPI_REG_ENQUEUE_B, FaultFunctions::generic);

	int res = reg_queue.enqueue(reg);

	FAULTPT_CLAPI(FAULTNUM_CLAPI_REG_ENQUEUE_A, FaultFunctions::generic);

	CL_PANIC(pthread_cond_signal(&reg_cond) == 0);
	CL_PANIC(pthread_mutex_unlock(&reg_lock) == 0);
	return (res);
}

int xml_module::queue_event(sc_xml_event *evt)
{
	CL_PANIC(pthread_mutex_lock(&evt_lock) == 0);

	FAULTPT_CLAPI(FAULTNUM_CLAPI_EVT_ENQUEUE_B, FaultFunctions::generic);

	int res = evt_queue.enqueue(evt);

	FAULTPT_CLAPI(FAULTNUM_CLAPI_EVT_ENQUEUE_A, FaultFunctions::generic);

	CL_PANIC(pthread_cond_signal(&evt_cond) == 0);
	CL_PANIC(pthread_mutex_unlock(&evt_lock) == 0);
	return (res);
}
