/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_SMART_STRING_H_
#define	_SMART_STRING_H_

#pragma ident	"@(#)smart_string.h	1.6	08/05/20 SMI"

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#include "pthread.h"

/*
 * class smart_string
 * -------------------
 */
/*
 * Suppress lint warning about no default constructor.
 * We don't want a default constructor.
 */
/*lint -e1712 */
class smart_string {
public:

	/*
	 * ctor
	 * -------
	 * Makes a local copy of the in_str, so the caller can delete
	 * the memory for it immediately after this call returns.
	 */
	smart_string(const char *in_str);
	smart_string &get_ref();
	smart_string *get_ptr_ref();
	void release_ref();

	/*
	 * Instead of an accessor for the string, provide an
	 * automatic conversion to const char *.
	 *
	 * Note that this cast works only on actual objects of type
	 * smart_string, not on smart_string * pointers!
	 */
	operator const char *() const {return the_str; }


	int get_construct_status() {return status; }

private:

	/*
	 * Destructor is private, so only we can delete ourselves
	 */
	~smart_string();

	/*
	 * The actual string that we store.
	 */
	char *the_str;
	/*
	 * We keep track of the number of references to this string.
	 * The refs_lock protects the num_refs, to provent race conditions
	 * with concurrent updates and reads of the ref count.
	 */
	int num_refs;
	pthread_mutex_t refs_lock;
	int status;
};
/*lint +e1712 */

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#endif /* _SMART_STRING_H_ */
