/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_CAAPI_MAPPING_H_
#define	_CAAPI_MAPPING_H_

#pragma ident	"@(#)caapi_mapping.h	1.13	08/05/20 SMI"

#include <sys/param.h>
#include <sys/os.h>
#include <sys/rm_util.h>
#include <sys/hashtable.h>
#include <h/naming.h>
#include <nslib/ns.h>
#include <ccr/readonly_copy_impl.h>
#include <ccr/updatable_copy_impl.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#include "sc_registration_instance.h"
#include "caapi_event.h"
#include "caapi_client.h"
#include "nvstrpair.h"


//
// The primary function of class cappi_event_client_mapping  is to
// organize the events and client information
// in a way that it facilitates faster retrieval of a list of
// clients for a specified event.
// This class supports following interfaces
// 1. lookup_clients : This method returns a list of
// 	clients registered for a specific event.
// 2. update_mapping : This method facilitates updation of the
//	caapi_mapping data-structure when a new client
// 	requests to register or unregister or update its list of events.
//
// Structure of caapi_mapping class:
// Each event is characterized by class, subclass and list of name-value
// pairs.caapi_mapping class has a string hashtable indexed one of the
// string combinations:
// a. classname:subclassname:nvpair1
// 	Associated with this key is a list of all events
// with the properties classname,subclassname and nvpair1
// b. classname:subclassname:
// 	Associated with this key is a list of all events
// with the properties classname,subclassname
// c. classname::
//	Associated with this key is a list of all events
// with the properties classname
//
// For eg. if we have a CLIENT1 registered for 2 events
// EVENT1 has following properties, class="EC_CLUSTER",
// subclass = "ResourceGroupStateChange" name-value pairs.
// ResourceGroupName=foo-rg
// ResourceGroupState=Online
// and EVENT2, class="EC_CLUSTER",
// subclass = "ResourceGroupStateChange"
// ResourceGroupName=foo-rg
//
//  and another CLIENT2 registered for 1 event
// EVENT1, class="EC_CLUSTER"
// In the above case hashtable will have 3 key combinations:
// key1 : EC_CLUSTER:ResourceGroupStateChange:ResourceGroupName=foo-rg
//	Associated with this key is a list which has just event1
//	and event2 of client1.
// key2 : EC_CLUSTER:ResourceGroupStateChange:ResourceGroupState=Online
//	Associated with this key is a list which has just event1
// key3 : EC_CLUSTER::
// 	Associated with this key is a list which has just event1
//	of client3.
//
// when a new sysevent is generated with properties
// class = EC_CLUSTER
// subclass = ResourceGroupStateChange
// nvpair = ResourceGroupState=foo-rg
// then all clients registered for events with above properties are
// notified, in the above case we have EVENT2 of CLIENT1 has same
// properties and event1 of CLIENT2 has same properties so
// for above event both clients are notified of the occurance of
// event
//

class caapi_mapping {
public:
	caapi_mapping();
	~caapi_mapping();
	//
	// This method returns a list of clients for a specified
	// event. This method is called ffrom caapi_reg_cache when
	// communication subsystem needs to notify
	// all clients for a given event
	//
	SList<caapi_client_handle>*
	lookup_clients(const sc_event *eventp_in);

	//
	// This interface is called when we need to verify
	// if a client has registered for a specific event.
	// if client is registered then event_out is set to
	// instance of the event else it is set to NULL
	//
	int lookup_event_in_client(const caapi_client *clientp_in,
	    const sc_event *eventp_in,
	    caapi_event *&eventp_out);


	//
	// This method called from caapi_reg_cache when a new
	// client chooses to register or unregister or update list
	// of interested  events
	//
	int update_mapping(reg_type op, caapi_client *clientp);
	// deallocate all the entries of hashtable
	int dispose();

private:
	//
	// get_client_list fucntion iterates the event list and
	// identifies the client's which need to be informed for
	// the incoming sysevent. when a new sysevent is generated
	// communication module will request caapi_reg_cache
	// module for a list of client's interested in that sysevent.
	// for eg: if sysevent is generated with properties
	// class = EC_CLUSTER, subclass = ResourceGroupStateChange
	// and two name value pairs associated with the sysevent
	// ResourceGroupName=oracle and ResourceGroupState=online
	// the get_client_list is supposed return all clients
	// 1. registered for all EC_CLUSTER events
	// 2. registered for all EC_CLUSTER and ResourceGroupStateChange
	// events
	// 3. registered for all EC_CLUSTER and ResourceGroupStateChange
	// events and ResourceGroupName=oracle
	// 4. registered for all EC_CLUSTER and ResourceGroupStateChange
	// events and ResourceGroupState=online
	// 5. registered for all EC_CLUSTER and ResourceGroupStateChange
	// events and ResourceGroupName=oracle and
	// ResourceGroupState=online
	//  this function in above case is called 4 times
	// once for EC_CLUSTER
	// once for EC_CLUSTER:ResourceGroupStateChange
	// once for EC_CLUSTER:ResourceGroupStateChange:ResourceGroupState
	// once for EC_CLUSTER:ResourceGroupStateChange:ResourceGroupName
	//
	// event_list is the list retuned by the hash.lookup for
	// the given key
	//
	// marked event list is a list of events which have been
	// associated with one of the hash keys we are storing this
	// list so that in the end we can set the match frequency of all
	// events to 0.
	//
	// notify_client_list is the list of clients who are interested
	// in the incoming event
	//
	int get_client_list(SList<caapi_event> *event_list,
			    SList<caapi_event> &marked_event_list,
			    SList<caapi_client> &notify_client_list);

	//
	// returns a list of clients for a specified event properties
	// specified by classname, subclassname, and nvpairstring
	// identifies the client's which need to be informed for the
	// incoming sysevent. when a new sysevent is generated
	// communication module will request caapi_reg_cache module
	// for a list of client's interested in that sysevent.
	// for eg: if sysevent is generated with properties
	// class = EC_CLUSTER, subclass = ResourceGroupStateChange
	// and two name value pairs associated with the sysevent
	// ResourceGroupName=oracle and ResourceGroupState=online
	// the get_client_list is supposed return all clients
	// 1. registered for all EC_CLUSTER events
	// 2. registered for all EC_CLUSTER and ResourceGroupStateChange
	// events
	// 3. registered for all EC_CLUSTER and ResourceGroupStateChange
	// events and ResourceGroupName=oracle
	// 4. registered for all EC_CLUSTER and ResourceGroupStateChange
	// events and ResourceGroupState=online
	// 5. registered for all EC_CLUSTER and ResourceGroupStateChange
	// events and ResourceGroupName=oracle and
	// ResourceGroupState=online
	// NOTE:
	// caller of the function is reponsible for deallocating the
	// memory allocated to SList of caapi_clients
	//

	int get_event_string(const char *evt_class_in,
			const char *evt_subclass_in,
			const char *nvstring_in,
			char *&eventstr_out);

	int update_event(reg_type op, caapi_event * eventp);

	int update_hashtable(reg_type op, char *eventstr, caapi_event *eventp);

	int add_event(char *eventstr_in, caapi_event *eventp);
	int remove_event(char *eventstr_in, caapi_event *eventp);

	//
	// match_event_to_client is a helper function for lookup_event_in_client
	// it identifies exact event instance of a particular client.
	//
	int match_event_to_client(SList<caapi_event> *event_list,
	    SList<caapi_event> &marked_event_list,
	    const caapi_client *clientp_in,
	    uint_t nv_count_in,
	    caapi_event *&eventp_out);


	//
	// The main hashtable which is indexed by one of the
	// following strings
	// classname EVENT_DELIMITER EVENT_DELIMITER
	// classname EVENT_DELIMITER subclassname EVENT_DELIMITER
	// classname EVENT_DELIMITER subclassname EVENT_DELIMITER
	// name=value
	// associated with each key is a list of events
	// which have the same properties as listed in the
	// key.. NEEDS more commenting
	//

	// The constructor allows for a caller-specified hash table size.
	// hash table size defaults to DEFAULT_HASHTABLE_SIZE (97 currently)

	// XXX need work on dynamic hashtable size

	string_hashtable_t<SList<caapi_event>* >	client_event_hash;
};

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#endif /* _CAAPI_MAPPING_H_ */
