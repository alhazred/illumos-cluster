/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _TCP_WRAPPER_H_
#define	_TCP_WRAPPER_H_

#pragma ident	"@(#)tcp_wrapper.h	1.6	08/05/20 SMI"

#include <string.h>
#include <sys/types.h>
#include <sys/boolean.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

/*
 * class tcp_wrapper
 * ------------------------
 * This class stores hosts_allow and hosts_deny lists, and answers queries
 * about whether a given ip address is "acceptable."
 *
 * The hosts_allow and hosts_deny lists are lists of VLSM formatted ip
 * addresses.  The VLSM ip addresses consist of a full 32 bit IP address and
 * a mask.  The only exceptions are the special strings "ALL" and "*", which
 * represent all IP addresses, and the special string "LOCAL", which
 * represents all ip addresses in the subnet (for all interfaces).
 *
 * An IP address is "acceptable" to the tcp_wrapper if it is in hosts_allow,
 * and is not in hosts_deny.
 *
 * An IP addres is "in" a list if anding it with the mask for an ip makes
 * it equal to that ip anded with the mask.
 */

/*
 * Suppress lint warning about no default constructor.
 * We don't want a default constructor.
 */
/*lint -e1712 */
class tcp_wrapper {
    public:
	/*
	 * constructors
	 * --------------
	 * hosts_allow_in and hosts_deny_in are arrays of char *s, where
	 * each string is either:
	 * 1. VLSM IP, in the form: <32 dotted-quad ip>/mask size
	 * ie. 1.2.0.0/16
	 * 2. The words "ALL" or "*"
	 * 3. The word "LOCAL"
	 *
	 * The num_hosts_allow_in and num_hosts_deny_in must be the length
	 * of the respective hosts arrays.  This constructor has no way
	 * to verify whether those numbers are accurate.
	 */
	tcp_wrapper(const char * const *hosts_allow_in,
	    uint_t num_hosts_allow_in, const char * const *hosts_deny_in,
	    uint_t num_hosts_deny_in);

	/*
	 * copy constructor
	 * -------------------
	 * Does deep copy of all fields.
	 */
	tcp_wrapper(const tcp_wrapper &src);

	/*
	 * get_construct_status
	 * --------------------
	 * Returns 0 if construction went ok, error code or -1 otherwise.
	 */
	int get_construct_status() const {return (status); }

	/*
	 * destructor
	 * -----------
	 * Frees the memory.
	 */
	~tcp_wrapper();

	/*
	 * verify_ip
	 * -------------------------------------
	 * Takes either a dotted-quad ip string or a numeric ip address,
	 * and checks whether the ip is in hosts_allow but no in hosts_deny.
	 *
	 * If numeric, the address must be in network byte order.
	 *
	 * If possible, specify the version that takes both the string and
	 * the numeric representation, because it is more efficient.
	 *
	 * Returns 0 if ip is accepted.
	 * Returns 1 if it's not in hosts.allow.
	 * Returns 2 if it's in hosts.allow, but also in hosts.deny
	 * Returns 3 for other error.
	 */
	int verify_ip(uint32_t ip) const;
	int verify_ip(const char *ip_str) const;
	int verify_ip(uint32_t ip, const char *ip_str) const;


	/*
	 * print
	 * -------------
	 * for debugging
	 */
	void print() const;


    private:

	/*
	 * op= to disallow assignment
	 * cstyle doesn't understand operator overloading
	 */
	/*CSTYLED*/
	tcp_wrapper &operator=(const tcp_wrapper &src);

	/* struct to store an ip and mask */
	typedef struct {
		uint32_t addr;
		uint32_t mask;
		char *ip_str;
	} net_id;

	/* helper functions */
	bool ip_in_list(net_id hosts[], uint_t num_net_ids, uint32_t ip,
	    const char *ip_str) const;
	int convert_host_list(net_id *&hosts,
	    const char * const*hosts_in, uint_t &num_hosts, bool &all);
	int copy_host_list(net_id **new_list, net_id *old_list,
	    uint_t num_in_list, uint_t &new_num_in_list);
	int copy_one_net_id(net_id &dest, const net_id &src);
	static void create_mask(net_id &host, long mask_num);

	static void delete_host_list(net_id *&list, uint_t &num);
	static int get_subnets(net_id **subnets, uint_t &num_subnets);

	/* the hosts_allow and hosts_deny members */
	net_id *hosts_allow;
	uint_t num_hosts_allow;
	net_id *hosts_deny;
	uint_t num_hosts_deny;

	// for optimization
	bool allow_all, deny_all;

	/* construction status */
	int status;
};

/*
 * Turn off lint suppression
 */
/*lint +e1712 */

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#endif /* _TCP_WRAPPER_H_ */
