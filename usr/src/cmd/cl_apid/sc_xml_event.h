/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _SC_XML_EVENT_H_
#define	_SC_XML_EVENT_H_

#pragma ident	"@(#)sc_xml_event.h	1.6	08/05/20 SMI"

#include <libsysevent.h>
#include <libxml/parser.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#include "sc_xml_instance.h"
#include "sc_event.h"
#include "string_buffer.h"

/*
 * class sc_xml_event
 * ---------------------------
 * Subclass of sc_xml_instance and sc_event
 *
 * This event contains a vendor, and publisher (as strings).
 * It is a subclass of sc_event, which stores the class, subclass,
 * and attributes associated with a sysevent.  This class is meant
 * to be an object-oriented version of a sysevent_t that knows how
 * to translate to/from xml.
 *
 * As a subclass of sc_xml_instance, it knows how to create an xml
 * representation, and how to parse the xml to get its attributes.
 *
 * As a subclass of sc_event, subscribes to that interface.
 *
 * Note that the xml is parsed or created immediately upon creation.
 *
 * The xml string and attributes returned are references to the internal
 * storage, not newly allocated memory.
 */

/*
 * Suppress lint warning about no default constructor (1712).
 * We don't want a default constructor.
 */

/*lint -e1712 */
class sc_xml_event : public sc_xml_instance, public sc_event {
public:
	/*
	 * constructors
	 * --------------
	 * Construct from sysevent, from xml, or from event characteristics
	 * directly.
	 * When constructed from a sysevent_t, the nvpairs are converted
	 * to nvstrpairs.
	 */
	sc_xml_event(sysevent_t *evt);
	sc_xml_event(const char *xml);
	sc_xml_event(const char *evt_class, const char *evt_subclass,
	    const char *vendor, const char *pub,
	    const SList<nvstrpair> &list_in);

	/*
	 * copy constructor and op=
	 */
	sc_xml_event(const sc_xml_event &src);
	/*CSTYLED*/
	const sc_xml_event &operator=(const sc_xml_event &src);

	virtual int get_construct_status() const;

	/*
	 * destructor
	 * -----------
	 * frees the memory
	 */
	virtual ~sc_xml_event();

	/*
	 * Accessors
	 * -----------
	 * Return references to the strings.
	 */

	const char *get_vendor() const;
	const char *get_pub() const;

	/*
	 * print
	 * -------
	 * for debugging
	 */
	virtual void print() const;


	/*
	 * static helper functions for parsing and xml creation that
	 * can be used by sc_registration_instance as well.
	 */

	/*
	 * Sets status to status: 0 for success, error code for failure.
	 * If status set to error, return NOT_PARSED.
	 */
	static parse_result parse_nvlist(xmlDocPtr doc, xmlNodePtr cur,
	    SList<nvstrpair> &a_list, int &status);
	static int write_attr_list(string_buffer &buf,
	    const SList<nvstrpair> &a_list);
	static int write_value_list(string_buffer &buf,
	    const SList<char> &values);

protected:
	char *pub, *vendor;

	virtual void create_xml();
	int write_event(string_buffer &buf);
	void parse_xml();

	void scei_construct_helper(const char *new_vendor,
	    const char *new_pub);

};

/*
 * Turn off the suppression.
 */
/*lint +e1712 */

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif


#endif /* _SC_XML_EVENT_H_ */
