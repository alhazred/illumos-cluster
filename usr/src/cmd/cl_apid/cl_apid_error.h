/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_CL_APID_ERROR_H_
#define	_CL_APID_ERROR_H_

#pragma ident	"@(#)cl_apid_error.h	1.7	08/05/20 SMI"

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

/*
 * The private CLEP_ERR_* error codes start with 101 and (are expected to)
 * go upto 200.
 *
 *
 */

#define	CLEP_FIRST_ERROR	101
#define	CLEP_ERR_ORB		101	/* ORB problems */
#define	CLEP_ERR_CCR		102	/* read/write to CCR failed */
#define	CLEP_ERR_MAX_CLIENTS	103	/* exceeded maximum supported */
					/* client registrations */
#define	CLEP_ERR_MAX_EVENTS	104	/* exceeded maximum supported */
					/* event registrations */
#define	CLEP_ERR_MAX_NVPAIRS	105	/* exceeded maximum supported */
					/* nvpairs registrations */
#define	CLEP_ERR_AUTH		106	/* Client authentication failed */
#define	CLEP_ERR_UNKW		107	/* Client not found in CCR table */
#define	CLEP_ERR_OP		108	/* Invalid operation */
					/* requested by Client */
#define	CLEP_ERR_INV		109	/* Invalid value passed */
#define	CLEP_ERR_CLIENT_DEAD	110	/* Client DELETED */
					/* In this case client can */
					/* just Unregister itself */
#define	CLEP_ERR_UNKW_EVT	111	/* the client specified an event */
					/* about which we don't know */
#define	CLEP_ERR_EVT_NAMES	112	/* the client failed to specify */
					/* all distinguishing names for */
					/* an event */
#define	CLEP_LAST_ERROR		112

/*
 * The private CLEP_WAR_* error codes start with 201 and (are expected to)
 * go upto 250.
 *
 */


#define	CLEP_FIRST_WAR  201
#define	CLEP_WAR_EXISTS	201	/* Warning Client Exists */
#define	CLEP_LAST_WAR   201

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#endif	/* _CL_APID_ERROR_H_ */
