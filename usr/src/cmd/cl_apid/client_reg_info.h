/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _CLIENT_REG_INFO_H_
#define	_CLIENT_REG_INFO_H_

#pragma ident	"@(#)client_reg_info.h	1.16	08/05/20 SMI"

#include <sys/param.h>
#include <sys/os.h>
#include <sys/rm_util.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#include "sc_callback_info.h"
#include "sc_registration_instance.h"
#include "caapi_event.h"



//
// class client_reg_info stores  persistent information client
// caapi_client is the only class using this class
//
const char * const DATA_DELIMITER = ";";
const char * const EVENT_DELIMITER = ":";
const char * const NV_DELIMITER = "=";
//
// Suppress lint warning about no default constructor.
// default constructor not needed
//
/*lint -e1712 */


class client_reg_info {
public:
	//
	// constructor of caapi_client, each client is initialized with a
	// callback ip and a list of events.
	//
	client_reg_info(const sc_registration_instance
			    &client_reg_instance);

	// used when reading from caapi_reg ccr file
	client_reg_info(const char *key_in,
			    const char *data_in);


	// destructor caller should ensure regarding dangling references
	virtual ~client_reg_info();

	//
	// equals is used for comparison of caapi_clients
	// comparison is based on the callback ip and port
	// and source ip
	//
	bool equals(const sc_registration_instance & client_reg_in);

	//
	// add_events adds the specified list of events in the registration
	// instance to the client's list of events
	// add_events is called when new registration request comes
	// for adding new events, in case the caapi_reg ccr updation
	// fails then add_events_revoke is called and events are removed from
	// event_list
	//

	int add_events(SList<sc_event> &reg_event_list);

	//
	// in add_events_revoke all the events with client back ptr set to NULL
	// are erase from client event_list as they are not added successfully
	// into caapi_reg ccr table
	//
	int add_events_revoke();



	//
	// method used to a remove event from client's event list.
	// Note : client needs to be removed from the caapi_reg file
	// when he is not registered for any event.
	//
	int remove_events_prepare(SList<caapi_event> &unreg_event_list);

	//
	// in remove_events_commit events with in_ccr set to false are
	// finally removed from the event list
	//
	int remove_events_commit();

	//
	// in remove_events_revoke events with in_ccr set to false are set
	// to true as the events werent removed caapi_reg ccr table
	//
	int remove_events_revoke();


	// Accessor functions
	//
	// caller of get_key_data passes NULL key and data values
	// get_key_data function populates the callback info into key
	// and event details into data caller is responsible for freeing up
	// the memory allocated to key and data
	//
	int get_key_data(char *&key_out, char *&data_out);


	//
	// get_events does not return const parameter and returns pointer
	// beacuse the mapping class needs to store the event
	// pointer associated with the client
	//
	SList<caapi_event>* get_events() const;
	const sc_callback_info* get_callback_info() const;

	void set_in_ccr(bool in_ccr_new);

	//
	// get_construct_status
	// ---------------------
	// Should be called after construction.  Returns 0 if there
	// were no problems, -1 or errno error otherwise.
	//
	// If non-zero, object should not be used.
	//
	virtual int get_construct_status() const {return construct_status; }

protected:
	int parse_client_info(const char *key_in,
				const char *data_in);
	int parse_data(char *data_in, SList <char> & event_str_list);

	void construct_helper(const client_reg_info &src);

	//
	// O(N) function to identify is a event exists in the client
	// return's true if it exists and sets the eventp
	// else return false and eventp is set to NULL
	// reg_event is the event in registration instance
	// eventp_out is the event which is found in the client
	//
	bool find_event(sc_event *reg_event,
		caapi_event *&eventp_out);


	// copy constructor to allow deep copies
	client_reg_info(const client_reg_info &src);
	const client_reg_info& operator=(const client_reg_info &rhs);
	// deallocates the data members
	void destruct_helper();

	SList <caapi_event>		*evtlistp;
	sc_callback_info		*callbackp;

	// number os events the client has registered for
	uint_t			num_events;

	// in_ccr indicates whether details of client needs to be written
	// caapi_reg ccr table
	bool			in_ccr;

	// indicates whether the client_reg_info instance was created
	int			construct_status;

};
/*lint -e1712 */

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#endif /* _CLIENT_REG_INFO_H_ */
