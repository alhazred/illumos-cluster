/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _CAAPI_CLIENT_HANDLE_H_
#define	_CAAPI_CLIENT_HANDLE_H_

#pragma ident	"@(#)caapi_client_handle.h	1.12	08/05/20 SMI"

#include <sys/param.h>
#include <sys/os.h>
#include <sys/rm_util.h>
#include <syslog.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#include "caapi_client.h"

//
// caapi_client_handle class is a proxy class for caapi_client
// it supports reference counting capability to caapi_client
//
//
// Suppress lint warning about no default constructor.
// default constructor not needed
//
/*lint -e1712 */

class caapi_client_handle {
public:
	//
	// caapi_client is a friend of this class as it needs to
	// invoke private constructor of this class
	//
	friend caapi_client;

	//
	// get_callback_info
	// -----------------
	// Returns the callback info for this client.
	// This method will always be valid, as long as the handle is
	// in use, even if the client is "removed" in the meantime.
	//
	const sc_callback_info* get_callback_info() const;

	//
	// is_handle
	// ---------
	// Utility method to determine whether this handle is a handle
	// of the specified caapi_client.
	//
	bool is_handle(const caapi_client *clientp_in);

	//
	// is_alive
	// ---------
	// Checks the status of the client.
	// Returns true if client is ACTIVE and not pending deletion.
	// Returns false otherwise.
	bool is_alive();

	//
	// remove_client
	// -------------
	// "deletes" this client by generating a fake unregistration message.
	// Note that the handles (including this one) will remain valid
	// until they are all deleted.
	//
	int remove_client();

	//
	// duplicate
	// ---------
	// This method creates a new caapi_client_handle that references
	// the same caapi_client, and returns it to the caller.
	//
	caapi_client_handle *duplicate();

	// void update_last_connect_status(bool is_success, time timestamp);
	// void get_connect_status(int &nfail, time &last_timestamp);

	//
	// destructor
	// -----------
	//
	~caapi_client_handle();

private:
	// private constructor
	caapi_client_handle(caapi_client *clientp_in);

	// manipulates the caapi_client using this pointer
	caapi_client		*clientp;
};
/*lint +e1712 */

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#endif /* _CAAPI_CLIENT_HANDLE_H_ */
