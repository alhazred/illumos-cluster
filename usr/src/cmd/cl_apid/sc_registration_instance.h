/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_SC_REGISTRATION_INSTANCE_H_
#define	_SC_REGISTRATION_INSTANCE_H_

#pragma ident	"@(#)sc_registration_instance.h	1.11	08/05/20 SMI"

#include <libsysevent.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#include "sc_xml_instance.h"
#include "sc_callback_info.h"
#include "sc_event.h"
#include "string_buffer.h"

/* can't #include it because of circular includes */
class caapi_client_handle;

typedef enum {ADD_CLIENT, ADD_EVENTS, REMOVE_CLIENT, REMOVE_EVENTS} reg_type;

/*
 * class sc_registration_instance
 * -------------------------------
 * Subclass of sc_xml_instance.
 * Stores one "registration instance" for the cluster-aware-api.
 * A registration instance consists of an sc_callback_info, a list
 * of events (sc_event), and a registration type (reg_type).
 *
 * It can also contain a src_ip (char *) and an open socket (int).
 * Neither attribute is used in the object or are part of the xml;
 * they are only kept for storage purposes, because they are directly
 * associated with the registration of a client.
 *
 * An object of this class can be constructed either from a string
 * representation of the xml formatted registration or from the 3 main
 * attributes described above.  Once created, both the xml string and the
 * attributes can be retrieved, but not changed.  Thus, the object is
 * immutable.
 *
 * At creation, the xml string is created or parsed (whichever is appropriate).
 */

/*
 * Suppress lint warning about no default constructor.
 * We don't want a default constructor.
 */
/*lint -e1712 */
class sc_registration_instance : public sc_xml_instance {
    public:

	/*
	 * constructor
	 * --------------
	 * construct from xml or from attributes
	 */
	sc_registration_instance(const char *xml, const char *src_ip_in,
	    int sock_in = -1);
	sc_registration_instance(const sc_callback_info &cb_info,
	    SList<sc_event> &list, reg_type reg_in);

	/*
	 * destructor
	 * ----------
	 * frees all the memory
	 */
	~sc_registration_instance();

	/*
	 * Accessors for the attributes.
	 * ---------------------------------
	 * The actual attributes returned are references, not copies.
	 * Thus, they are const.
	 */
	const sc_callback_info &get_callback_info() const;
	const SList<sc_event> &get_events() const;
	reg_type get_reg_type() const;
	void set_sock(int sock_in) {sock = sock_in; }
	int get_sock() const;

	/*
	 * getters and setters for the registration result and client_handle
	 */
	inline void set_reg_res(int res) { reg_res = res; }
	inline int get_reg_res() { return reg_res; }

	inline void set_client_handle(caapi_client_handle *in_hand)
	    { cli_handle = in_hand; }
	inline caapi_client_handle *get_client_handle() { return cli_handle; }

	/*
	 * print
	 * --------
	 * for debugging
	 */
	virtual void print() const;

	/*
	 * is_complete_message
	 * ---------------------
	 * Takes a string and verifies that it represents a "complete"
	 * sc_callback_reg xml message.  It first tries to create an
	 * sc_registration_instance from it, returning the result in
	 * *new_reg.  If that fails, it means what we have so far in str
	 * is not complete, is mal-formed, or is invalid.  If any of the
	 * latter two, it could be that the sender screwed up, in which
	 * case we check for the ending tag, and return true if it's present,
	 * false otherwise.  In the latter case, *new_reg will be NULL.
	 */
	static bool is_complete_message(const char *str, const char *ip,
	    sc_registration_instance **new_reg);

    private:
	sc_callback_info *cb_info;
	SList<sc_event> *event_list;
	reg_type reg;
	int sock;
	char *src_ip;

	// The registration instance stores the info that gets set
	// by the cache upon registration
	int reg_res;
	caapi_client_handle *cli_handle;

	virtual void parse_xml();
	virtual void create_xml();

	int write_registration_instance(string_buffer &str_buf);
	int write_callback_info(string_buffer &str_buf);

	int write_events(string_buffer &buf);
	int write_event_list(string_buffer &buf);

	void parse_event_list(xmlDocPtr doc, xmlNodePtr cur);

	/*
	 * Declare a private copy constructor and op=, but do not define them,
	 * to force pass by reference.
	 */
	sc_registration_instance(const sc_registration_instance &src);
	sc_registration_instance &operator = (const sc_registration_instance
	    &src);
};

/*lint +e1712 */

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#endif /* _SC_REGISTRATION_INSTANCE_H_ */
