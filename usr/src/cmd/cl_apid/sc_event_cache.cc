/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma	ident	"@(#)sc_event_cache.cc	1.12	08/05/27 SMI"

#include <pthread.h>
#include <errno.h>
#include <stdlib.h>

#include <sys/hashtable.h>
#include <sys/cl_eventdefs.h>
#include <orb/fault/fault_injection.h>
#include "cl_apid_include.h"
#include "sc_event_cache.h"
#include "caapi_client_handle.h"
#include "evt_comm_module.h"
#include "cl_apid_error.h"
//
// hack, because SCHA_ERR_TIMEOUT is defined in both the public libdsdev.h,
// and in the private scha_priv.h
//
#undef SCHA_ERR_TIMEOUT
#include <rgm/rgm_call_pres.h>

/*
 * Implementation details:
 * -----------------------
 * The class keeps a hashtable of event types, which it creates at start-up.
 * This hashtable is harcoded currently to have three event types.
 * Eventually, the constructor will look for .so files in a specific
 * directory, and dynamically find the event types.
 *
 * Each event_type_info contains a pointer to the request_events function
 * for that type, a list of the "distinguishing names" for that type,
 * and the most recent event of that type (or events, one for each combination
 * of distinguishing names).  The distinguishing names are those
 * by which the event type space are divided (for example, rg_name for
 * the subclass ESC_cluster_rg_state).  As each new event arrives,
 * it is looked up in the hashtable by class.subclass (its type).  If we don't
 * have distinguishing names for the event, we just store the most recent
 * event of that type.  If we do, we store one event for each unique
 * name/value pair, for all combinations of the distinguishing names.
 * We use a hashtable for each event type for this purpose.  Thus, finding
 * events for a newly registered client consists simply of looking up the
 * event for which the client registered in the main hashtable, and sending
 * the client the cached eent for that type.  If there is more than one
 * cached event, we look up the relevent one by the name/value pairs
 * specified by the client.
 */


/*
 * approve_registrations
 * ----------------------
 * Takes a list of sc_registration_instance objects.  For each
 * one, checks each event registration, ensuring that for each
 * event type, all required nvpairs are present.
 *
 * The sc_registration_instance is marked as approved or denied in its
 * reg_res field.
 *
 */
void sc_event_cache::approve_registrations(
    SList<sc_registration_instance> &reg_list)
{
	/* grab the lock for mutual exclusion */
	CL_PANIC(pthread_mutex_lock(&event_cache_lock) == 0);

	/* iterate through the sc_registration_instances */
	sc_registration_instance *one_reg;
	reg_list.atfirst();
	while ((one_reg = reg_list.get_current()) != NULL) {
		reg_list.advance();

		scds_syslog_debug(1, "Verifying registration for %s\n",
		    one_reg->get_callback_info().get_key());

		/* iterate through this reg's events */
		SList<sc_event> *evts = const_cast<SList<sc_event> *>(
		    &(one_reg->get_events()));
		sc_event *one_evt;
		evts->atfirst();
		while ((one_evt = evts->get_current()) != NULL) {
			evts->advance();

			/*
			 * If we don't have a subclass specified,
			 * we have to approve it.  Otherwise, we
			 * need to check the specifics.
			 *
			 * Note that approval, in this case, is to
			 * do nothing.
			 */
			if (one_evt->get_subclass() != NULL) {
				/* lookup the event_type_info */
				event_type_info *cur_info = event_info.
					lookup(one_evt->get_key());
				if (cur_info == NULL) {
					/*
					 * we don't know about the event
					 * type!
					 */
					scds_syslog_debug(1,
						"Don't know about evt %s\n",
						one_evt->get_key());
					one_reg->set_reg_res(
					    CLEP_ERR_UNKW_EVT);
					CL_PANIC(pthread_mutex_unlock(
					    &event_cache_lock) == 0);
					return;
				}
				/*
				 * We have to make sure that the
				 * user event has a name for each
				 * of the distinguishing names
				 * for this event type.
				 *
				 * get_event_nv_key returns non-zero
				 * if it can't find all the required
				 * names.
				 */
				char *key = NULL;
				int ret = get_event_nv_key(one_evt,
				    cur_info->dist_names, key);
				if (ret != 0) {
					scds_syslog_debug(1,
						"didn't match all names "
					    "for evt %s\n", one_evt->get_key());
					one_reg->set_reg_res(
					    CLEP_ERR_EVT_NAMES);
					CL_PANIC(pthread_mutex_unlock(
					    &event_cache_lock) == 0);
					return;
				}
				delete []key;
			}
		}
		scds_syslog_debug(1, "All evt ok for %s\n",
			one_reg->get_callback_info().get_key());
	}

	CL_PANIC(pthread_mutex_unlock(&event_cache_lock) == 0);
}

/*
 * request_events
 * --------------
 * Requests event generation, via the loadable modules, for _all_ event
 * types for which we have loadable modules.
 *
 * Note that the event requests, through the loadable modules,
 * are preformed synchronously ( _not_ on a separate thread).
 *
 * Iterates through each event_type_info in the event_info hashtable.
 * For each one, calls the callback function saved in it.
 *
 */
void sc_event_cache::request_events()
{
	/* grab the lock for mutual exclusion */
	CL_PANIC(pthread_mutex_lock(&event_cache_lock) == 0);

	scds_syslog_debug(1, "sc_event_cache::request_events\n");

	/*
	 * Iterate through each of the event_type_info's stored in the
	 * hashtable, calling the request_func on each of them.
	 */
	string_hashtable_t<event_type_info *>::iterator hash_it(event_info);
	event_type_info *cur_event_info;

	for (cur_event_info = hash_it.get_current(); cur_event_info != NULL;
	    cur_event_info = hash_it.get_current()) {

		/* advance the iterator */
		hash_it.advance();

		/*
		 * Call the function with empty parameters to
		 * specify that it should request all events that
		 * it knows about.
		 */
		scds_syslog_debug(1, "Calling request_func for %s\n",
		    cur_event_info->get_key());
		(void) cur_event_info->request_func(NULL, NULL, 0);
	}

	CL_PANIC(pthread_mutex_unlock(&event_cache_lock) == 0);
}

/*
 * send_cached_events
 * -------------
 * send_cached_events is just a wrapper for send cached_event below.
 * send_cached_event is called once for every sc_registration_instance
 * in the reg_list list.
 */
int sc_event_cache::send_cached_events(
    SList<sc_registration_instance> &reg_list)
{
	/* iterate through the sc_registration_instances */
	sc_registration_instance *one_reg;
	reg_list.atfirst();
	while ((one_reg = reg_list.get_current()) != NULL) {
		reg_list.advance();
		if (send_cached_event(one_reg) == ENOMEM) {
			return (ENOMEM);
		}
	}
	return (0);
}

/*
 * send_cached_event
 * ------------------
 * For each sc_event in the one_reg, looks
 * up the most recent event for that type in its cache and
 * calls into the evt_comm_module to deliver the event to
 * this client.  Uses the client_handle stored in each
 * sc_registration_instance to pass to the event delivery module.
 */
int sc_event_cache::send_cached_event(sc_registration_instance *one_reg)
{

	/*
	 * verify that this reg is really one to whom we should be sending
	 * events.
	 */

	reg_type rtype = one_reg->get_reg_type();
	if (rtype != ADD_CLIENT && rtype != ADD_EVENTS) {
		scds_syslog_debug(1, "Registration is for the wrong type: "
		    "no event to send\n");
		return (0);
	}

	if (one_reg->get_reg_res() != 0) {
		scds_syslog_debug(1, "Reg did not pass reg cache: "
		    "will not send events\n");
		return (0);
	}

	/* grab the lock for mutual exclusion */
	CL_PANIC(pthread_mutex_lock(&event_cache_lock) == 0);


	/*
	 * iterate through this reg's events
	 * We need to const_cast because we don't have a const
	 * iterator.
	 */
	SList<sc_event> *evts = const_cast<SList<sc_event> *>(
	    &(one_reg->get_events()));
	sc_event *one_evt;
	evts->atfirst();
	while ((one_evt = evts->get_current()) != NULL) {
		evts->advance();

		/*
		 * If we don't have a subclass specified,
		 * we can send all events, then go on to the
		 * next client, because we've already sent
		 * everything possible to this client.
		 */
		if (one_evt->get_subclass() == NULL) {
			send_all_events(one_reg);
			break;
		}

		/*
		 * we do have a subclass specified.
		 * lookup by key in the event_info hashtable
		 */
		event_type_info *cur_info = event_info.lookup(
		    one_evt->get_key());
		if (cur_info == NULL) {
			scds_syslog_debug(1, "Could not match event %s\n",
			    one_evt->get_key());
			break;
		}
		/* call a helper method to send the events */
		if (send_matching_event(one_reg, cur_info, one_evt)
		    == ENOMEM) {
			CL_PANIC(pthread_mutex_unlock(&event_cache_lock) == 0);
			return (ENOMEM);
		}

	}
	CL_PANIC(pthread_mutex_unlock(&event_cache_lock) == 0);
	return (0);
}


/*
 * send_all_events
 * --------------
 * Helper method to send all cached events to the one_reg client.
 */
void sc_event_cache::send_all_events(sc_registration_instance *one_reg)
{
	scds_syslog_debug(1, "Sending all events to %s\n",
	    one_reg->get_client_handle()->get_callback_info()->get_key());

	/*
	 * iterate through the entire hashtable of event_type_infos,
	 * sending every cached event stored.
	 */
	string_hashtable_t<event_type_info *>::iterator hash_it(event_info);
	event_type_info *cur_event_info;

	for (cur_event_info = hash_it.get_current(); cur_event_info != NULL;
	    cur_event_info = hash_it.get_current()) {

		/* advance the iterator */
		hash_it.advance();

		/*
		 * If it's a single cached event, send that one,
		 * otherwise iterate through the stored events, sending
		 * all of them.
		 */
		if (cur_event_info->num_names == 0) {
			/* do the actual send */
			/* note that we copy the client handle */
			/* see what to do if it fails... ??? */
			if (cur_event_info->cached_event != NULL) {
				(void) evt_comm_module::the()->send_message(
				    cur_event_info->cached_event->get_xml(),
				    one_reg->get_client_handle()->duplicate());
			}
		} else {
			/* we have to do a nested hashtable iteration */
			string_hashtable_t<sc_xml_event *>::iterator
			    evt_hash_it(cur_event_info->cached_events);
			sc_xml_event *one_evt;
			for (one_evt = evt_hash_it.get_current();
			    one_evt != NULL; one_evt =
			    evt_hash_it.get_current()) {

				evt_hash_it.advance();

				/* do the actual send */
				/* note that we copy the client handle */
				/* see what to do if it fails... ??? */
				(void) evt_comm_module::the()->send_message(
				    one_evt->get_xml(),
				    one_reg->get_client_handle()->duplicate());
			}
		}
	}
}

/*
 * send_matching_event
 * -----------------
 * Helper method to find the one cached event to match the one_reg's reg_evt
 * in subclass and nvpairs, and to send it to the one_reg.
 *
 * We know that cur_info matches the class/subclass of the reg_evt already.
 */
int sc_event_cache::send_matching_event(sc_registration_instance *one_reg,
    event_type_info *cur_info, sc_event *reg_evt)
{
#ifdef _FAULT_INJECTION
	void		*f_argp;
	uint32_t	f_argsize;
	if (fault_triggered(FAULTNUM_CLAPI_EVENT_SEND_MATCHING_R,
		&f_argp, &f_argsize)) {
		ASSERT(f_argsize == sizeof (int));
		return (*((int *)f_argp));
	}
#endif
	sc_xml_event *real_evt;
	sc_syslog_msg_handle_t handle;

	scds_syslog_debug(1, "Sending one event to %s: matching %s\n",
	    one_reg->get_client_handle()->get_callback_info()->get_key(),
	    cur_info->get_key());

	/*
	 * The easy case is if there are no distinguishing names;
	 * we just send the one cached_event.
	 */
	if (cur_info->num_names == 0) {
		real_evt = cur_info->cached_event;
	} else {
		/* the harder case is when we look up by nvpairs */
		char *evt_key = NULL;
		int ret = get_event_nv_key(reg_evt,
		    cur_info->dist_names, evt_key);
		if (ret != 0) {
			scds_syslog_debug(1,
				"Error: reg_evt did not contain correct "
				" names to match cached event\n");
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
			//
			// SCMSGS
			// @explanation
			// The cl_apid was unable to find cached events to
			// deliver to the newly registered client.
			// @user_action
			// No action required.
			//
			(void) sc_syslog_msg_log(handle, LOG_NOTICE, MESSAGE,
			    "Error: reg_evt missing correct names");
			sc_syslog_msg_done(&handle);
			CL_PANIC(pthread_mutex_unlock(&event_cache_lock) == 0);
			return (0);
		}
		if (evt_key == NULL) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "Error: low memory");
			sc_syslog_msg_done(&handle);
			CL_PANIC(pthread_mutex_unlock(&event_cache_lock) == 0);
			return (ENOMEM);
		}
		/*
		 * lookup in the cached events hashtabale for this
		 * particular class/subclass, based on the nv pair key
		 * returned from get_event_nv_key.
		 */
		real_evt = cur_info->cached_events.lookup(evt_key);
		delete []evt_key;
	}

	/*
	 * Do the actual send of the event that we found here.
	 */
	if (real_evt == NULL) {
		scds_syslog_debug(1, "\tNo event found to match\n");
	} else {
		scds_syslog_debug(1, "\tSending event\n");
		/* do the actual send */
		/* see what to do if it fails ??? */
		(void) evt_comm_module::the()->send_message(
		    real_evt->get_xml(),
		    one_reg->get_client_handle()->duplicate());
	}
	return (0);
}


/*
 * store_event
 * --------------
 * Stores the event in its event cache.
 */
int sc_event_cache::store_event(sc_xml_event *evt)
{
	sc_syslog_msg_handle_t handle;

	/* grab the lock for mutual exclusion */
	CL_PANIC(pthread_mutex_lock(&event_cache_lock) == 0);

	scds_syslog_debug(1, "Storing event: %s\n", evt->get_key());

	/*
	 * First make sure we know about events of this type.
	 */
	event_type_info *cur_info = event_info.lookup(evt->get_key());
	if (cur_info == NULL) {
		scds_syslog_debug(1, "No record of event type %s found\n",
		    evt->get_key());
		CL_PANIC(pthread_mutex_unlock(&event_cache_lock) == 0);
		return (-1);
	}

	/*
	 * If we don't have any names, we can just store it
	 * in the single cached_event spot.
	 *
	 * However, if we have names, we need to lookup by those names.
	 */
	if (cur_info->num_names == 0) {
		scds_syslog_debug(1, "\tStoring in single cached_event\n");
		delete (cur_info->cached_event);
		cur_info->cached_event = evt;
	} else {
		char *evt_key = NULL;
		int res = get_event_nv_key(evt, cur_info->dist_names, evt_key);
		if (res != 0) {
			scds_syslog_debug(1,
				"Could not match dist names for evt");
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
			//
			// SCMSGS
			// @explanation
			// The cl_apid event cache was unable to store an
			// event because of the specified reason.
			// @user_action
			// No action required.
			//
			(void) sc_syslog_msg_log(handle, LOG_NOTICE, MESSAGE,
			    "event lacking correct names");
			sc_syslog_msg_done(&handle);
			CL_PANIC(pthread_mutex_unlock(&event_cache_lock) == 0);
			return (-1);
		}
		if (evt_key == NULL) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "Error: low memory");
			sc_syslog_msg_done(&handle);
			CL_PANIC(pthread_mutex_unlock(&event_cache_lock) == 0);
			return (ENOMEM);
		}
		scds_syslog_debug(1, "\tStoring in ht, with key %s\n", evt_key);

		/* first, remove the old event, if it's present */
		sc_xml_event *old_evt =
		    cur_info->cached_events.remove(evt_key);
		delete(old_evt);

		/* now add the new one */
		cur_info->cached_events.add(evt, evt_key);

		delete []evt_key;
	}
	CL_PANIC(pthread_mutex_unlock(&event_cache_lock) == 0);
	return (0);
}

/*
 * get_event_nv_key
 * -------------
 * Helper function to figure out what the nvpair key for the evt is
 * based on the list of names.  The function looks up each name in
 * the evt to find the value.  It then constructs the key as:
 * <name>=<value>;[...]
 */
int sc_event_cache::get_event_nv_key(sc_event *evt, SList<char> &names,
    char *&ret_key)
{
	string_buffer str_buf;
	sc_syslog_msg_handle_t handle;

	char *one_name;
	const nvstrpair *cur_nvpair;

	/* first set it to NULL */
	ret_key = NULL;

	/*
	 * iterate through each of the names, looking up each
	 * one in the event.
	 */
	names.atfirst();
	while ((one_name = names.get_current()) != NULL) {
		names.advance();
		cur_nvpair = evt->lookup_nvpair(one_name);

		/* if we find a match, make it part of our key */
		if (cur_nvpair) {
			if (str_buf.append("%s=%s;\n", cur_nvpair->get_name(),
			    cur_nvpair->get_value_str()) == -1) {
				(void) sc_syslog_msg_initialize(&handle,
				    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				MESSAGE, "Error: low memory");
				sc_syslog_msg_done(&handle);

				return (-1);
			}
		} else {
			scds_syslog_debug(1,
				"Could not find match for %s\n", one_name);
			return (-1);
		}
	}
	ret_key = os::strdup(str_buf.get_buf());
	return (0);
}

/*
 * print
 * --------
 * for debugging
 */
void sc_event_cache::print()
{
	(void) fprintf(stderr, "\nPrinting sc_event_cache contents\n");
	/*
	 * iterate through the entire hashtable of event_type_infos,
	 * sending every cached event stored.
	 */
	string_hashtable_t<event_type_info *>::iterator hash_it(event_info);
	event_type_info *cur_event_info;

	for (cur_event_info = hash_it.get_current(); cur_event_info != NULL;
	    cur_event_info = hash_it.get_current()) {

		/* advance the iterator */
		hash_it.advance();

		(void) fprintf(stderr,
		    "Event Info: %s\n", cur_event_info->key);
		if (cur_event_info->num_names == 0) {
			(void) fprintf(stderr,
			    "One cached event (0 dist names) \n");
			if (cur_event_info->cached_event != NULL) {
				cur_event_info->cached_event->print();
			} else {
				(void) fprintf(stderr, "Event is NULL\n");
			}
		} else {
			(void) fprintf(stderr,
			    "Many events (>= 1 dist names) \n");

			/* we have to do a nested hashtable iteration */
			string_hashtable_t<sc_xml_event *>::iterator
			    evt_hash_it(cur_event_info->cached_events);
			sc_xml_event *one_evt;
			for (one_evt = evt_hash_it.get_current();
			    one_evt != NULL; one_evt =
			    evt_hash_it.get_current()) {

				evt_hash_it.advance();

				/* do the actual print */
				one_evt->print();
			}

		}
		(void) fprintf(stderr, "\n");
	}
}


/*
 * reload_modules
 * ----------------
 * Request that the sc_event_cache object reload the shared-object
 * modules from the well-known directory path.
 *
 * Should be called when a SIGHUP is received.
 */
int sc_event_cache::reload_modules(const char *plugin_path_in)
{
	/* grab the lock for mutual exclusion */
	CL_PANIC(pthread_mutex_lock(&event_cache_lock) == 0);

	delete []plugin_path;

	plugin_path = os::strdup(plugin_path_in);
	if (plugin_path == NULL) {
		sc_syslog_msg_handle_t handle;
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Error: low memory");
		sc_syslog_msg_done(&handle);

		CL_PANIC(pthread_mutex_unlock(&event_cache_lock) == 0);

		return (ENOMEM);
	}

	/* delete everything in the hashtable */
	hash_delete_helper();


	/* reload the modules */
	if (load_modules() != 0) {
		/* nothin' else to do */
		scds_syslog_debug(1, "exiting with error code 1\n");
		exit(1);
	}

	/* release the lock */
	CL_PANIC(pthread_mutex_unlock(&event_cache_lock) == 0);

	/* re-request everything */
	request_events();
	return (0);
}

/*
 * ctor
 * ----
 * Just creates the mutex and loads the modules.
 */
sc_event_cache::sc_event_cache(const char *plugin_path_in)
{
	int err;
	sc_syslog_msg_handle_t handle;

	plugin_path = os::strdup(plugin_path_in);

	/*
	 * Create the mutex.
	 */
	if ((err = pthread_mutex_init(&event_cache_lock, NULL)) != 0) {
		char *err_str = strerror(err);
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "pthread_mutex_init: %s", err_str ? err_str : "");
		sc_syslog_msg_done(&handle);
		exit(1);
	}

	if (load_modules() != 0) {
		/* nothin' else to do */
		scds_syslog_debug(1, "exiting with error code 1\n");
		exit(1);
	}
}

/*
 * dtor
 * ----
 * Remove and delte all event_type_info structs from the ht.
 * destroy the mutex and free the plugin_path.
 */
sc_event_cache::~sc_event_cache()
{
	/* delete everything in the hashtable */
	hash_delete_helper();
	(void) pthread_mutex_destroy(&event_cache_lock);
	delete [] plugin_path;
}

void sc_event_cache::hash_delete_helper()
{
	/*
	 * Iterate through the hashtable, removing and deleting each entry.
	 * Can't use dispose, because it doesn't work for string hashtables.
	 */
	string_hashtable_t<event_type_info *>::iterator hash_it(event_info);
	event_type_info *one_info;

	for (one_info = hash_it.get_current(); one_info != NULL;
	    one_info = hash_it.get_current()) {

		/* advance the iterator */
		hash_it.advance();

		(void) event_info.remove(one_info->key);

		/* delete does all the cleanup */
		delete (one_info);
	}
}

/*
 * the
 * ---
 * Returns a reference to the one sc_event_cache object.
 */
sc_event_cache &sc_event_cache::the()
{
	CL_PANIC(the_sc_event_cache != NULL);
	return (*the_sc_event_cache);
}

/*
 * define/initialize the static members
 */
sc_event_cache *sc_event_cache::the_sc_event_cache = NULL;

/*
 * initialize
 * ---------
 * This method should be called exactly once in a program to
 * create and initialize the one instance of sc_event_cache.
 */
int sc_event_cache::initialize(const char *plugin_path_in)
{
	sc_syslog_msg_handle_t handle;

	scds_syslog_debug(1, "Initializing sc_event_cache\n");

	/* initialize should only be called once */
	CL_PANIC(the_sc_event_cache == NULL);

	/* create the one instance of sc_event_cache */
	the_sc_event_cache = new sc_event_cache(plugin_path_in);

	if (the_sc_event_cache == NULL) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Error: low memory");
		sc_syslog_msg_done(&handle);
		exit(1);
	}

	return (0);
}

/*
 * shutdown
 * -----------
 * Called on the sc_event_cache object to tell it to destroy
 * itself.
 */
void sc_event_cache::shutdown()
{
	scds_syslog_debug(1, "Shutting down sc_event_cache\n");

	// delete ourselves
	delete the_sc_event_cache;
	the_sc_event_cache = NULL;
}


/*
 * private helper methods
 */

/*
 * wrapper functions for callbacks to force event generation until
 * we get the .so mods loaded
 */
int
membership_gen_wrapper(char **, char **, int)
{
	scha_errmsg_t ret = call_event_gen_membership(NULL); // default zone
	if (ret.err_code != SCHA_ERR_NOERR) {
		if (ret.err_msg != NULL) {
			// should be a syslog?
			scds_syslog_debug(1, ret.err_msg);
		}
		return (ret.err_code);
	}
	return (0);
}

int
rg_state_gen_wrapper(char **, char **, int)
{
	scha_errmsg_t ret = call_event_gen_rg_state(NULL, NULL); // default zone
	if (ret.err_code != SCHA_ERR_NOERR) {
		if (ret.err_msg != NULL) {
			// should be a syslog?
			scds_syslog_debug(1, ret.err_msg);
		}
		return (ret.err_code);
	}
	return (0);
}

int
r_state_gen_wrapper(char **, char **, int)
{
	scha_errmsg_t ret = call_event_gen_rs_state(NULL, NULL); // default zone
	if (ret.err_code != SCHA_ERR_NOERR) {
		if (ret.err_msg != NULL) {
			// should be a syslog?
			scds_syslog_debug(1, ret.err_msg);
		}
		return (ret.err_code);
	}
	return (0);
}


/*
 * load_modules
 * ------------
 * Eventually: load the dynamic libraries in pluging-path, and
 * create an event_type_info for each of them.
 *
 * currently: the three event types are hardcoded, with a dummy callback
 * function for initial generation.
 *
 * Note that this method sets up the list of known event types in the
 * event_info hashtable.  Any event types not set up in this method will
 * not be know about for the duration of this class' existence.
 */
int sc_event_cache::load_modules()
{
	sc_syslog_msg_handle_t handle;

	/*
	 * Eventually, we will load all the libraries in the
	 * plugin_path directory, but for now we hardcode the info
	 * about the three rgm-generated events.
	 */

	char *rg_dist_names[1] = {CL_RG_NAME};
	char *r_dist_names[1] = {CL_R_NAME};
	const char *memb_key = EC_CLUSTER "." ESC_CLUSTER_MEMBERSHIP;
	const char *rg_key = EC_CLUSTER "." ESC_CLUSTER_RG_STATE;
	const char *r_key = EC_CLUSTER "." ESC_CLUSTER_R_STATE;

	/* create the event_type_info structs for each type */
	event_type_info *memb_info = new event_type_info(
	    membership_gen_wrapper, NULL, 0, memb_key);
	event_type_info *rg_info = new event_type_info(
	    rg_state_gen_wrapper, rg_dist_names, 1, rg_key);
	event_type_info *r_info = new event_type_info(
	    r_state_gen_wrapper, r_dist_names, 1, r_key);

	if (memb_info == NULL || memb_info->status != 0 || rg_info == NULL ||
	    rg_info->status != 0 || r_info == NULL || r_info->status != 0) {

		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Error: low memory");
		sc_syslog_msg_done(&handle);

		return (ENOMEM);
	}

	/* add the event_type_infos to the hashtable */
	event_info.add(memb_info, memb_key);
	event_info.add(rg_info, rg_key);
	event_info.add(r_info, r_key);

	return (0);
}

/*
 * ctor
 * ------
 * Save the function pointer, the names, and the key.
 * Initialize other vars to reasonable values.
 */
event_type_info::event_type_info(request_funcp_t func_in, char **names_in,
    int num_names_in, const char *key_in)
{
	int i;

	/* set the function pointer to the proper callback */
	request_func = func_in;
	status = 0;
	cached_event = NULL;

	/*
	 * Create the key
	 */
	key = os::strdup(key_in);
	if (key == NULL) {
		status = ENOMEM;
		return;
	}

	/*
	 * Copy all the names, adding them to our
	 * list as we go.
	 */
	for (i = 0; i < num_names_in; i++) {
		char *temp = os::strdup(names_in[i]);
		if (temp == NULL) {
			status = ENOMEM;
			return;
		}
		dist_names.append(temp);
	}

	/* cache the number of names */
	num_names = num_names_in;
}

/*
 * destructor
 * ----------
 * Frees memory for the key and all the dist names.
 * deletes all sc_xml_events stored as well.
 */
/*lint -e1740 */
event_type_info::~event_type_info()
{
	char *str;

	/*
	 * delete the cached events
	 */
	delete cached_event;

	/*
	 * Iterate through the hashtable, removing and deleting each entry.
	 * Can't use dispose, because it doesn't work for string hashtables.
	 */
	string_hashtable_t<sc_xml_event *>::iterator hash_it(cached_events);
	sc_xml_event *one_evt;

	for (one_evt = hash_it.get_current(); one_evt != NULL;
	    one_evt = hash_it.get_current()) {

		/* advance the iterator */
		hash_it.advance();

		str = NULL;
		if (sc_event_cache::the().get_event_nv_key(
		    one_evt, dist_names, str) == 0) {
			(void) cached_events.remove(str);
			delete []str;
			/* delete does all the cleanup */
			delete (one_evt);
		}
	}

	/* remove everything, just in case */
	cached_events.dispose();

	/* delete the names and key */
	delete []key;

	while ((str = dist_names.reapfirst()) != NULL) {
		delete []str;
	}
}
/*lint +e1740 */
