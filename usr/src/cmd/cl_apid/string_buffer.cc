/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma	ident	"@(#)string_buffer.cc	1.8	08/05/20 SMI"

#include <string.h>
#include <sys/varargs.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/sc_syslog_msg.h>

#include "string_buffer.h"
#include "cl_apid_include.h"

/* define static members */
const int string_buffer::alloc_init = 2;

string_buffer::string_buffer()
{
	buf = (char *)malloc(alloc_init);
	/* don't check for NULL here... will check in append */
	allocated = alloc_init;
	current = 0;
}

void string_buffer::clear()
{
	free(buf);

	buf = (char *)malloc(alloc_init);
	/* don't check for NULL here... will check in append */
	allocated = alloc_init;
	current = 0;
}

string_buffer::~string_buffer()
{
	free(buf);
}

int string_buffer::append(const char *format, /* args */ ...)
{
	va_list ap;
	sc_syslog_msg_handle_t handle;
	int err;

	/*
	 * Suppress lint warning.
	 * Lint doesn't understand varargs stuff.
	 */
	va_start(ap, format); /*lint !e40 */

	if (buf == NULL) {
		return (-1);
	}

	FILE *devnull = NULL;
	if ((devnull = fopen("/dev/null", "w")) != NULL) {
		int size = vfprintf(devnull, format, ap);
		if (size < 0) {
			(void) fclose(devnull);
			return (-1);
		}
		while ((current + (size_t)size) >= allocated) {
			allocated *= 2;
			char *temp = (char *)realloc(buf, allocated);
			/* check for realloc failure */
			if (temp == NULL) {
				err = errno;
				(void) sc_syslog_msg_initialize(&handle,
				    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
				//
				// SCMSGS
				// @explanation
				// The call to realloc(3C) in the cl_apid
				// failed with the specified error code.
				// @user_action
				// Increase swap space, install more memory,
				// or reduce peak memory consumption.
				//
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE, "realloc failed with error code: "
				    "%d", err);
				sc_syslog_msg_done(&handle);
				(void) fclose(devnull);
				return (-1);
			}
			buf = temp;
		}
		size = vsprintf(buf + current, format, ap);
		if (size < 0) {
			(void) fclose(devnull);
			return (-1);
		}
		current += (size_t)size;
		(void) fclose(devnull);
		return (size);
	} else {
		err = errno;
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The cl_apid was unable to open /dev/null because of the
		// specified error.
		// @user_action
		// Examine other syslog messages occurring at about the same
		// time to see if the problem can be identified. Save a copy
		// of the /var/adm/messages files on all nodes and contact
		// your authorized Sun service provider for assistance in
		// diagnosing and correcting the problem.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Failed to open /dev/null for writing; fopen failed with "
		    "error: %s", strerror(err));
		sc_syslog_msg_done(&handle);
	}
	return (-1);
}

const char *string_buffer::get_buf() const
{
	return (buf);
}
