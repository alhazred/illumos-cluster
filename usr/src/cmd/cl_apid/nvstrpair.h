/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _NVSTRPAIR_H_
#define	_NVSTRPAIR_H_

#pragma ident	"@(#)nvstrpair.h	1.6	08/05/20 SMI"

#include <libnvpair.h>
#include <sys/list_def.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

/*
 * nvstrpair class:
 *
 * Like nvpair_t, from Solaris libsysevent, except all values are strings or
 * string arrays.
 *
 * Makes its own local copy of the name and value (or value array).
 * Name and value (or value array) returned are references to the local
 * copies, not newly allocated memory.
 *
 * This class is immutable once created.
 */

/*
 * Suppress lint warning about no default constructor.
 * We don't want a default constructor.
 */
/*lint -e1712 */
class nvstrpair {
public:
	/*
	 * constructors
	 * ---------------
	 * Construct from nvpair, from name and scalar value,
	 * or from name and array value.  If constructued from nvpair,
	 * converts the value to string or string array.
	 *
	 * The constructor will set the construction status.  After
	 * constructing, the user should call get_construction_status
	 * to see if everything went ok.
	 */
	nvstrpair(nvpair_t *nvpair);
	nvstrpair(const char *name_in, const char *value_in);
	nvstrpair(const char *name_in, const SList<char> &value_list_in);

	/* copy constructor */
	nvstrpair(const nvstrpair &src);
	/* op=: cstyle doesn't understand it */
	/*CSTYLED*/
	const nvstrpair &operator=(const nvstrpair &rhs);

	/*
	 * get_construction_status
	 * -------------------------
	 * After constructing an nvstrpair object, the caller should
	 * call get_construction_status.  It will return 0 if everything is
	 * ok.  If not, it will return an errno error, or -1 for unspecified
	 * error.
	 *
	 * If the function returns anything but 0, the object should
	 * immediately be deleted.
	 */
	int get_construction_status();

	/*
	 * destructor
	 * -------------
	 * Frees all memory.  Caller should ensure there are no dangling
	 * references to attributes, as they will be freed.
	 */
	~nvstrpair();

	/*
	 * Accessors
	 * -----------------
	 * Return references to the attributes.  Note that if the
	 * value is a scalar, the list will contain one element.
	 */
	const char *get_name() const;
	const SList<char> &get_value_list() const;

	/*
	 * get_value_str
	 * -----------------
	 * Returns a string version of the value, whether it is an array
	 * or a scalar.  If an array, the array elements will by delimited
	 * by elem_delim.
	 */
	const char *get_value_str() const;

	/*
	 * The delimiter for array values
	 */
	static const char elem_delim;


	/*
	 * operator==
	 * -------------
	 * For comparison with other nvstrpairs.
	 * Two nvstrpairs are identical if their names are equal, and
	 * their values are equal.  Values are considered sets.  The
	 * sets must be equal, without regard to order.  This means the
	 * equality check is O(num_value_this * num_value_rhs), or
	 * essentially an O(n^2) operation.
	 */
	/* cstyle doesn't understand operator overloading */
	/*CSTYLED*/
	bool operator==(const nvstrpair &rhs) const;

	/*
	 * print
	 * -------------
	 * For debuggin
	 */
	void print() const;

protected:
	char *name, *value_str;
	SList<char> *value_list;

	void create_value_str();

	void construct_helper(const char *name_in,
	    const SList<char> &value_list_in);
	void destruct_helper();

	/* stores the status of the construction. 0 on success */
	int status;
};
/*lint +e1712 */

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#endif /* _NVSTRPAIR_H_ */
