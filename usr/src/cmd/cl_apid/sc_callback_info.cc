/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma	ident	"@(#)sc_callback_info.cc	1.6	08/05/20 SMI"

#include <stdlib.h>
#include <stdio.h>

#include <sys/os.h>
#include "sc_callback_info.h"
#include "net_tools.h"
#include "string_buffer.h"

/*
 * constructors and op=
 * --------------
 * Copy the ip and the port to our members.
 */
sc_callback_info::sc_callback_info(const char *cb_ip_in, int port) :
    cb_port(port)
{
	status = 0;
	cb_ip = NULL;
	key = NULL;
	done_lookup = 0;
	(void) memset(&cached_lookup, '\0', sizeof (cached_lookup));

	if (cb_ip_in != NULL) {
		if ((cb_ip = os::strdup(cb_ip_in)) == NULL) {
			status = ENOMEM;
			return;
		}
		string_buffer str_buf;
		if (str_buf.append("%s:%d", cb_ip, cb_port) < 0) {
			status = ENOMEM;
			return;
		}
		if ((key = os::strdup(str_buf.get_buf())) == NULL) {
			status = ENOMEM;
			return;
		}
	} else {
		cb_ip = NULL;
	}
}

sc_callback_info::sc_callback_info(const sc_callback_info &src)
{
	cb_ip = NULL;
	key = NULL;
	status = 0;
	done_lookup = 0;

	/* transfer the lookup, if we've already done it */
	cached_lookup = src.cached_lookup;
	done_lookup = src.done_lookup;

	cb_port = src.get_cb_port();
	const char *temp = src.get_cb_ip();
	if (temp != NULL) {
		if ((cb_ip = os::strdup(temp)) == NULL) {
			status = ENOMEM;
			return;
		}
		string_buffer str_buf;
		if (str_buf.append("%s:%d", cb_ip, cb_port) < 0) {
			status = ENOMEM;
			return;
		}
		if ((key = os::strdup(str_buf.get_buf())) == NULL) {
			status = ENOMEM;
			return;
		}
	}
}

/* cstyle doesn't understand operator overloading */
/*CSTYLED*/
sc_callback_info &sc_callback_info::operator=(const sc_callback_info &src)
{
	if (this != &src) {
		delete [](cb_ip);
		delete [](key);

		cb_ip = NULL;
		key = NULL;
		status = 0;
		done_lookup = 0;

		cb_port = src.get_cb_port();
		const char *temp = src.get_cb_ip();
		if (temp != NULL) {
			if ((cb_ip = os::strdup(temp)) == NULL) {
				status = ENOMEM;
			}
			string_buffer str_buf;
			if (str_buf.append("%s:%d", cb_ip, cb_port) < 0) {
				status = ENOMEM;
				return (*this);
			}
			if ((key = os::strdup(str_buf.get_buf())) == NULL) {
				status = ENOMEM;
				return (*this);
			}
		}

		/* transfer the lookup, if we've already done it */
		cached_lookup = src.cached_lookup;
		done_lookup = src.done_lookup;
	}
	return (*this);
}


/*
 * destructor
 * -----------
 * Frees the memory.
 */
sc_callback_info::~sc_callback_info()
{
	delete [](cb_ip);
	delete [](key);
}

/*
 * Accessors
 * -----------
 * Get references to the attributes.
 */

const char *sc_callback_info::get_cb_ip() const
{
	return (cb_ip);
}

int sc_callback_info::get_cb_port() const
{
	return (cb_port);
}

const char *sc_callback_info::get_key() const
{
	return (key);
}

int sc_callback_info::get_ip_n(struct sockaddr_in &sin_in)
{
	if (!done_lookup) {
		if (symbolic_or_IP_to_binary(cb_ip, &cached_lookup) == -1) {
			return (-1);
		}
		done_lookup = 1;
	}
	sin_in = cached_lookup;
	return (0);
}


/*
 * print
 * -------------
 * for debugging
 */
void sc_callback_info::print() const
{
	(void) printf("cb_ip=%s cb_port=%d key=%s\n", cb_ip ? cb_ip : "",
	    cb_port, key ? key : "");
}
