//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
#pragma ident   "@(#)caapi_client.cc 1.13     08/05/20 SMI"

#include "caapi_client.h"
#include "caapi_client_handle.h"
#include <orb/fault/fault_injection.h>
#include <sys/sc_syslog_msg.h>

//
// Implementation of caapi_client
//
// caapi_client constructor Initlialize the client with key and data
// This constructor is called when caapi_reg_cache reads the
// the key data from the ccr table and initilializes the client
//
caapi_client::caapi_client(const char *key_in, const char *data_in) :
#ifdef	BUG_4763564
    client_reg_info(key_in, data_in), status(CAAPI_ACTIVE),
#else
    client_reg_info(key_in, data_in), status(ACTIVE),
#endif
    nref(0), nfail(0), notify(false)
{
}

//
//
// caapi_client constructor
// used when registration instance is passed
//
caapi_client::caapi_client(const sc_registration_instance &
    client_reg_instance) : client_reg_info(client_reg_instance),
#ifdef	BUG_4763564
    status(CAAPI_ACTIVE), nref(0), nfail(0), notify(false)
#else
    status(ACTIVE), nref(0), nfail(0), notify(false)
#endif
{
}

//
//
// caapi_client destructor
//
caapi_client::~caapi_client()
{
}

//
//
// Returns client instances
//
caapi_client_handle*
caapi_client::get_client_handle()
{
	sc_syslog_msg_handle_t handle;

	//
	// multiple client handler's make a call to this function nref
	// needs a lock protected and maintain atomic operation
	//
	mutex_lck.lock();

	caapi_client_handle *client_handlep =
	    new caapi_client_handle(this);

	if (client_handlep == NULL) {
		// log an error msg
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The rpc.fed or cl_apid server was not able to allocate
		// memory. If the message if from the rpc.fed, the server
		// might not be able to capture the output from methods it
		// runs.
		// @user_action
		// Determine if the host is running out of memory. If not save
		// the /var/adm/messages file. Contact your authorized Sun
		// service provider to determine whether a workaround or patch
		// is available.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Error: low memory");
		sc_syslog_msg_done(&handle);
	} else  {
		// increment number of references to client
		nref++;
	}

	mutex_lck.unlock();

	FAULTPT_CLAPI(FAULTNUM_CLAPI_GET_CLIENT_HANDLE_E,
	    FaultFunctions::generic);

	return (client_handlep);
}

//
//
// decrements ref count for this object also if the status of client
// is DELETED then client instance is deleted
// release_ref is called by the client_handler
//
void
caapi_client::release_ref()
{
	//
	// multiple client handler's make a call to this function
	// nref needs a lock protected and maintain atomic operation
	// if the nref falls to 0 and status is DELELTED then the
	// client instance is deleted
	//
	mutex_lck.lock();

	if ((--nref == 0) && (status == DELETED))
		delete this;
	else {
		mutex_lck.unlock();
	}
}
//
// try_delete function is called from caapi_reg_cache when client chooses
// unregister, this function will delete the client instance when there are no
// outstanding of client, else the status of client is set to DELETED
// to indicate that client is unregistered and is removed from the client list
// of caapi_reg_cache when the last handler releases reference of client
// then client is deleted
//

void
caapi_client::try_delete()
{
	//
	// status flag is also lock protected else handler may
	// get a inconsistent client status
	//
	mutex_lck.lock();

	status = DELETED;
	if (nref == 0) {
		//
		// no handler holding the client's reference
		// so the client is deleted
		//
		delete this;
	} else {
		mutex_lck.unlock();
	}
}

//
bool
caapi_client::is_notified() const
{
	return (notify);
}
//
void
caapi_client::set_notify(bool notify_status)
{
	notify = notify_status;
}
// set the back pointer in event to client ptr
void
caapi_client::set_client_backptr()
{

	caapi_event *eventp = NULL;
	evtlistp->atfirst();
	while ((eventp = evtlistp->get_current()) != NULL) {
		evtlistp->advance();
		eventp->set_client_backptr(this);
	}
}
//
// is_active function is called to know the status of client
// returns TRUE when client status is active else FALSE
//
bool
caapi_client::is_active()
{
	bool flag = true;
	mutex_lck.lock();
#ifdef	BUG_4763564
	flag = (status == CAAPI_ACTIVE)?true:false;
#else
	flag = (status == ACTIVE)?true:false;
#endif
	mutex_lck.unlock();
	return (flag);
}
