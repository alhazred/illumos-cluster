/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _SC_CALLBACK_INFO_H_
#define	_SC_CALLBACK_INFO_H_

#pragma ident	"@(#)sc_callback_info.h	1.6	08/05/20 SMI"

#include <string.h>
#include <netinet/in.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif


/*
 * class sc_callback_info
 * ------------------------
 * A glorified structure, meant to hold an ip/port pair.
 * The IP addr is a string and the port is an integer.
 *
 * The object is immutable once created.
 */

/*
 * Suppress lint warning about no default constructor.
 * We don't want a default constructor.
 */
/*lint -e1712 */
class sc_callback_info {
    public:
	/*
	 * constructors
	 * --------------
	 * Only two: from attributes and from another sc_callback_info.
	 */
	sc_callback_info(const char *cb_ip_in, int port);
	sc_callback_info(const sc_callback_info &src);

	/*
	 * op= for deep copies
	 * cstyle doesn't understand operator overloading
	 */
	/*CSTYLED*/
	sc_callback_info &operator=(const sc_callback_info &src);

	/*
	 * get_construct_status
	 * --------------------
	 * Returns 0 if construction went ok, error code or -1 otherwise.
	 */
	int get_construct_status() {return (status); }

	/*
	 * destructor
	 * -----------
	 * Frees the memory.
	 */
	~sc_callback_info();

	/*
	 * Accessors
	 * -----------
	 * Get references to the attributes.
	 */
	const char *get_cb_ip() const;
	int get_cb_port() const;

	/*
	 * get_key
	 * --------
	 * Returns the "key", which is the string "ip:port".
	 */
	const char *get_key() const;

	/*
	 * get_ip_n
	 * ----------
	 * Returns the network version of the ip address, ready for
	 * use in a struct sockaddr.
	 *
	 * The first time this function is called it does a dns lookup
	 * (if the original ip is symbolic), or a pton conversion (if
	 * it's dotted quad).  Subsequent calls return a cached value from
	 * the first call.
	 *
	 * Returns -1 if the ip cannot be resolved.  0 on success.
	 */
	int get_ip_n(struct sockaddr_in &sin_in);

	/*
	 * print
	 * -------------
	 * for debugging
	 */
	virtual void print() const;

    private:
	char *cb_ip, *key;
	int cb_port;
	int status;
	struct sockaddr_in cached_lookup;
	int done_lookup;
};

/*
 * Turn off lint suppression
 */
/*lint +e1712 */

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#endif /* _SC_CALLBACK_INFO_H_ */
