/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma	ident	"@(#)net_tools.cc	1.7	08/05/20 SMI"

#include <errno.h>
#include <sys/sc_syslog_msg.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>

#include "net_tools.h"
#include "cl_apid_include.h"

int
sock_write(int sock, const char *str)
{
	ssize_t temp;
	int err;
	sc_syslog_msg_handle_t handle;

	/* Loop until we've sent everything */
	while (1) {
		temp = write(sock, str, strlen(str));
		/*
		 * if we didn't write everything, increment until the
		 * pointer is where we want to start writing again.
		 */
		if (temp == -1) {
			err = errno;
			char *err_str = strerror(err);
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "write: %s", err_str ? err_str : "");
			sc_syslog_msg_done(&handle);
			return (-1);
		}
		if (temp != (ssize_t)strlen(str)) {
			str += temp;
		} else break;
	}
	return (0);
}

int
create_listening_socket(int *fd, const char *ip, int port)
{
	struct sockaddr_in sin;
	sc_syslog_msg_handle_t handle;

	scds_syslog_debug(1,
		"Listening on %s:%d\n", ip ? ip : "INADDR_ANY", port);

	/*
	 * Set up the callback socket, binding it to the specified port,
	 * and ip addr, and calling listen.
	 */

	/* Make the socket */
	if ((*fd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		char *err_str = strerror(errno);
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The cl_apid experienced an error while constructing a
		// socket. This error may prohibit event delivery to CRNP
		// clients.
		// @user_action
		// Examine other syslog messages occurring at about the same
		// time to see if the problem can be identified. Save a copy
		// of the /var/adm/messages files on all nodes and contact
		// your authorized Sun service provider for assistance in
		// diagnosing and correcting the problem.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "socket: %s", err_str ? err_str : "");
		sc_syslog_msg_done(&handle);
		return (-1);
	}

	bzero(&sin, sizeof (sin));
	sin.sin_family = AF_INET;

	/* Convert from host to network byte order */
	sin.sin_port = (in_port_t)htons(port);

	/*
	 * If ip is NULL, use INADDR_ANY
	 */
	if (ip == NULL) {
		sin.sin_addr.s_addr = htonl(INADDR_ANY);
	} else {

		/*
		 * Call a helper function to convert the host name to the
		 * proper form.
		 */
		if (symbolic_or_IP_to_binary(ip, &sin) != 0) {
			/* error */
			(void) close(*fd);
			return (-1);
		}
	}

	/*
	 * Set the SO_REUSEADDR option on our socket, in case we're
	 * restarted.
	 */
	const int on = 1;
	if (setsockopt(*fd, SOL_SOCKET, SO_REUSEADDR, &on, sizeof (on)) ==
	    -1) {
		char *err_str = strerror(errno);
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The cl_apid experienced an error while configuring a
		// socket. This error may prohibit event delivery to CRNP
		// clients.
		// @user_action
		// Examine other syslog messages occurring at about the same
		// time to see if the problem can be identified. Save a copy
		// of the /var/adm/messages files on all nodes and contact
		// your authorized Sun service provider for assistance in
		// diagnosing and correcting the problem.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "setsockopt: %s", err_str ? err_str : "");
		sc_syslog_msg_done(&handle);
		(void) close(*fd);
		return (-1);
	}

	if (bind(*fd, (struct sockaddr *)&sin, sizeof (sin)) == -1) {
		char *err_str = strerror(errno);
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The cl_apid received the specified error while creating a
		// listening socket. This error may prevent the cl_apid from
		// starting up.
		// @user_action
		// Examine other syslog messages occurring at about the same
		// time to see if the problem can be identified. Save a copy
		// of the /var/adm/messages files on all nodes and contact
		// your authorized Sun service provider for assistance in
		// diagnosing and correcting the problem.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "bind: %s", err_str ? err_str : "");
		sc_syslog_msg_done(&handle);
		(void) close(*fd);
		return (-1);
	}

	/* Convert to an active listening socket */
	if (listen(*fd, 10) == -1) {
		char *err_str = strerror(errno);
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The cl_apid received the specified error while creating a
		// listening socket. This error may prevent the cl_apid from
		// starting up.
		// @user_action
		// Examine other syslog messages occurring at about the same
		// time to see if the problem can be identified. Save a copy
		// of the /var/adm/messages files on all nodes and contact
		// your authorized Sun service provider for assistance in
		// diagnosing and correcting the problem.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "listen: %s", err_str ? err_str : "");
		sc_syslog_msg_done(&handle);
		(void) close(*fd);
		return (-1);
	}
	return (0);
}

int
create_connected_socket(char *host, int port)
{
	struct sockaddr_in sin;
	int sock, ret_val;
	sc_syslog_msg_handle_t handle;

	/* 0 out the address struct */
	bzero(&sin, sizeof (sin));

	sin.sin_family = AF_INET;

	/* Convert from host to network byte order */
	sin.sin_port = (in_port_t)htons(port);

	/*
	 * Call a helper function to convert the host name to the
	 * proper form.
	 */
	if (symbolic_or_IP_to_binary(host, &sin) != 0) {
		/* error */
		return (-1);
	}

	/* Create the socket */
	if ((ret_val = (sock = socket(AF_INET, SOCK_STREAM, 0))) < 0) {
		char *err_str = strerror(errno);
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "socket: %s", err_str ? err_str : "");
		sc_syslog_msg_done(&handle);
		return (-1);
	}

	/* Open the connection to the registration daemon */
	if ((ret_val = connect(sock, (struct sockaddr *)&sin,
	    sizeof (sin))) < 0) {
		char *err_str = strerror(errno);
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The cl_apid received the specified error while attempting
		// to deliver an event to a CNRP client.
		// @user_action
		// Examine other syslog messages occurring at about the same
		// time to see if the problem can be identified. Save a copy
		// of the /var/adm/messages files on all nodes and contact
		// your authorized Sun service provider for assistance in
		// diagnosing and correcting the problem.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "connect: %s", err_str ? err_str : "");
		sc_syslog_msg_done(&handle);

		(void) close(sock);
		return (ret_val);
	}
	return (sock);
}

int
create_connected_nb_socket(sc_callback_info *cb_info)
{
	struct sockaddr_in sin;
	int sock, ret_val;
	sc_syslog_msg_handle_t handle;

	/* 0 out the address struct */
	bzero(&sin, sizeof (sin));


	/*
	 * Get the ip address.
	 */
	if (cb_info->get_ip_n(sin) != 0) {
		/* error */
		return (-1);
	}

	/*
	 * Need to assign family and port AFTER we call get_ip_n, because
	 * get_ip_n overwrites the entire struct.
	 */
	sin.sin_family = AF_INET;

	/* Convert from host to network byte order */
	sin.sin_port = (in_port_t)htons(cb_info->get_cb_port());

	/* Create the socket */
	if ((ret_val = (sock = socket(AF_INET, SOCK_STREAM, 0))) < 0) {
		char *err_str = strerror(errno);
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "socket: %s", err_str ? err_str : "");
		sc_syslog_msg_done(&handle);
		return (-1);
	}

	/*
	 * put the socket in non-blocking mode
	 */
	int flags;
	if ((flags = fcntl(sock, F_GETFL, 0)) < 0) {
		char *err_str = strerror(errno);
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The cl_apid received the specified error while attempting
		// to deliver an event to a CNRP client.
		// @user_action
		// Examine other syslog messages occurring at about the same
		// time to see if the problem can be identified. Save a copy
		// of the /var/adm/messages files on all nodes and contact
		// your authorized Sun service provider for assistance in
		// diagnosing and correcting the problem.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "fctl: %s", err_str ? err_str : "");
		sc_syslog_msg_done(&handle);
		return (-1);
	}
	flags |= O_NONBLOCK;
	if (fcntl(sock, F_SETFL, flags) < 0) {
		char *err_str = strerror(errno);
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "fctl: %s", err_str ? err_str : "");
		sc_syslog_msg_done(&handle);
		return (-1);
	}

	/*
	 * Open the connection.  We expect to get back an errno of EINPROGRESS.
	 */
	if ((ret_val = connect(sock, (struct sockaddr *)&sin,
	    sizeof (sin))) < 0) {
		int err = errno;

		/* this is what we expect */
		if (errno == EINPROGRESS) {
			scds_syslog_debug(1,
				"connect nb returned EINPROGRESS\n");
			return (sock);
		}
		char *err_str = strerror(err);
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "connect: %s", err_str ? err_str : "");
		sc_syslog_msg_done(&handle);

		(void) close(sock);
		return (ret_val);
	}
	/*
	 * If we're here, it means we actually connected immediately.
	 * It must be on the same host or something.
	 */
	scds_syslog_debug(1, "Connect nb completed immediately\n");
	return (sock);
}

/*
 * symbolic_or_IP_to_binary
 * ------------------------
 * First attempt to treat the host name as a dotted-quad IP.
 * If that fails, call gethostbyname to get the DNS info for
 * the host name.  Fills the binary, network ordered, host name in
 * sinPtr, or returns non-zero.
 */
int
symbolic_or_IP_to_binary(const char *host_str, struct sockaddr_in *sin_ptr)
{
	struct hostent hostent_ptr;
	char buf[1024];
	int err_val;
	sc_syslog_msg_handle_t handle;

	/*
	 * First try to convert the host name as a dotted-decimal number.
	 * Only if that fails do we call gethostbyname().
	 */
	if (inet_pton(AF_INET, host_str, &sin_ptr->sin_addr) != 0) {
		/* conversion succeeded  */
	} else {
		if (gethostbyname_r(host_str, &hostent_ptr,
		    buf, 1024, &err_val) == NULL) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
			//
			// SCMSGS
			// @explanation
			// The cl_apid could not resolve the specified
			// hostname. If this error occurs at start-up of the
			// daemon, it may prevent the daemon from starting.
			// Otherwise, it will prevent the daemon from
			// delivering an event to a CRNP client.
			// @user_action
			// If the error occurred at start-up, verify the
			// integrity of the resource properties specifying the
			// IP address on which the CRNP server should listen.
			// Otherwise, no action is required.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "Unable to resolve hostname %s", host_str);
			sc_syslog_msg_done(&handle);
			return (-1);
		}
		bcopy(hostent_ptr.h_addr, (char *)&(sin_ptr->sin_addr),
		    (size_t)(hostent_ptr.h_length));
	}
	return (0);
}
