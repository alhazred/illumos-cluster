/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _SC_EVENT_H_
#define	_SC_EVENT_H_

#pragma ident	"@(#)sc_event.h	1.7	08/05/20 SMI"

#include <libsysevent.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#include "nvstrpair.h"

/*
 * sc_event class
 * ----------------------------
 * An immutable class.  Stores one event, which is a class, subclass, and
 * list of nvpairs.  This class stores a list of nvstrpairs, in which all
 * values are strings or string arrays.
 *
 * The constructor copies the in parameters to new memory.
 * The accessors do not return new memory copies.  They return references
 * to the internal memory.
 */
class sc_event {
public:

	/*
	 * constructors
	 * --------------
	 * Can be constructued from attributes, from a sysevent_t or
	 * as empty event.
	 */
	sc_event(const char *evt_class_in, const char *evt_subclass_in,
	    const SList<nvstrpair> &nvstrlist_in);
	sc_event(sysevent_t *evt);
	sc_event();

	/*
	 * copy constructor and op=
	 * --------------------------
	 * To do deep copies.
	 */
	sc_event(const sc_event &src);

	/* cstyle doesn't understand operator overloading */
	/*CSTYLED*/
	sc_event &operator=(const sc_event &src);

	/*
	 * get_construct_status
	 * ---------------------
	 * Should be called after construction.  Returns 0 if there
	 * were no problems, -1 or errno error otherwise.
	 *
	 * If non-zero, object should not be used.
	 */
	virtual int get_construct_status() const {return construct_status; }

	/*
	 * destructor
	 * ------------
	 * Frees the memory of all the attributes. Caller of delete should
	 * ensure there are no dangling references to the attributes.
	 */
	virtual ~sc_event();

	/*
	 * Accessor functions
	 * ------------------
	 * Return references to the attributes.
	 */
	const char *get_class() const;
	const char *get_subclass() const;
	const char *get_key();
	const SList<nvstrpair> &get_nvlist() const;

	/*
	 * print
	 * --------
	 * For debugging
	 */
	virtual void print() const;

	/*
	 * lookup_nvpair
	 * -----------------
	 * Return a pointer to the pair if found, otherwise returns NULL.
	 * Lookup can work on a name/value pair explicitly or just a name.
	 *
	 * Uses linear search of list, and is thus O(n).
	 */
	const nvstrpair *lookup_nvpair(const nvstrpair &in_pair) const;
	const nvstrpair *lookup_nvpair(const char *in_name) const;

	/*
	 * operator==
	 * -----------
	 * For comparison with other sc_events.
	 * Returns true if the events are equal.  The list of nvpairs
	 * is treated as a set.  For events to be equal, the sets must
	 * be equal, without respect to order.
	 *
	 * Note that this is an O(n^2) operation to find set equality.
	 *
	 * Note also that determining if two nvstrpairs are equal is
	 * also a time consuming event.
	 */
	/* cstyle doesn't understand operator overloading */
	/*CSTYLED*/
	bool operator==(const sc_event &rhs) const;


protected:
	char	*evt_class;
	char	*evt_subclass;
	char *key;
	SList<nvstrpair> *nvlist;

	void construct_helper(const char *evt_class_in,
	    const char *evt_subclass_in, const SList<nvstrpair> &list_in);

	int construct_status;
};

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#endif /* _SC_EVENT_H_ */
