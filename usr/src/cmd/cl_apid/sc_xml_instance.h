/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_SC_XML_INSTANCE_H_
#define	_SC_XML_INSTANCE_H_

#pragma ident	"@(#)sc_xml_instance.h	1.6	08/05/20 SMI"

#include <stdlib.h>

#include <libxml/xmlmemory.h>
#include <libxml/parser.h>
#include <libxml/parserInternals.h>
#include <libxml/xmlerror.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

typedef enum {NOT_PARSED, PARSED, MAL_FORMED, INVALID} parse_result;

void
xmlSCErrorFunc(void *ctx ATTRIBUTE_UNUSED, const char *msg, ...);


/*
 * class sc_xml_instance
 * ----------------------
 * This abstract superclass does not provide much functionality.
 * It is intended to be used as an interface to which all classes
 * that parse xml can subscribe (in C++ this must be done be using
 * public inheritence from this class).
 *
 * The sc_xml_instance provides three public methods (other than the
 * constructors/destructor): get_xml returns the xml string associated with
 * this particular xml instance, and print prints the contents of the
 * xml instance in an implementation-dependent way (not necesarily the
 * xml string).  get_parse_result returns the result of parsing the xml,
 * which subclasses should always do as soon as the class is constructed.
 *
 * The parse_code represents the state of the existence of attributes.
 * This implementation of the superclass assumes than an xml_instance
 * constructed with an xml string has no attributes yet, and is not
 * parsed.  An xml_instance constructed with no parameters is assumed
 * to have attributes, and is considered to be parsed.
 *
 * Only a parsed xml_instance should be able to create_xml.
 *
 */
class sc_xml_instance {
    public:

	/*
	 * Constructors
	 * -------------
	 * construct from xml, or without anything, to allow subclasses to
	 * construct from attributes or xml.
	 */
	sc_xml_instance(const char *xml);
	sc_xml_instance();

	/*
	 * Copy constructor and op= for deep copies
	 */
	sc_xml_instance(const sc_xml_instance &src);
	/* cstyle doesn't understand operator overloading */
	/*CSTYLED*/
	const sc_xml_instance &operator=(const sc_xml_instance &src);

	/*
	 * get_construct_status
	 * ------------
	 * Returns the status of construction of this xml object.
	 * 0 for success, -1 or other error code on failure.
	 * If status is non-zero, the object should not be used.
	 */
	virtual int get_construct_status() const {return (xml_status); }

	/* destructor */
	virtual ~sc_xml_instance();

	/*
	 * parse_xml
	 * ------------
	 * Get the result of the xml parsing.
	 * If constructed with an xml string, the client
	 * should always check the parse result before
	 * attempting to use the xml string.
	 */
	parse_result get_parse_result() const {return (parse_code); }

	/*
	 * get_xml
	 * ----------
	 * Get the xml representation.
	 * Note that it's a reference to the actual string, not a copy.
	 */
	const char *get_xml() const {return (xml_str); }

	/*
	 * print
	 * --------
	 * for debugging
	 */
	virtual void print() const = NULL;

    protected:
	/* The xml representation */
	char *xml_str;

	/*
	 * The current status of parsing the xml.
	 * Subclasses should set this status in parse_xml when
	 * trying to parse it.
	 */
	parse_result parse_code;

	/*
	 * xml_status is different from the parse code.  It stores the
	 * status of construction of the object, and stores any internal
	 * errors (ENOMEM, etc).
	 * parse_code stores whether the parse was successful or not.
	 */
	int xml_status;

	/*
	 * create_xml
	 * ----------
	 * Subclass must fill in this method to provide functionality
	 * to create the xml.  Should be called by constructors.
	 */
	virtual void create_xml() = NULL;

	/*
	 * parse_xml
	 * ----------
	 * Should be called from the constructor to get everything set
	 * up so that accessors can be const.
	 * Subclasses must implement the method in an implementation-dependent
	 * way.
	 */
	virtual void parse_xml() = NULL;
};

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#endif /* _SC_XML_INSTANCE_H_ */
