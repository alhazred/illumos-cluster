/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_XML_MODULE_H_
#define	_XML_MODULE_H_

#pragma ident	"@(#)xml_module.h	1.10	08/05/20 SMI"

#include <semaphore.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#include "sc_registration_instance.h"
#include "sc_xml_event.h"
#include "sc_queue.h"
#include "sc_xml_reply.h"
#include "cl_apid_error.h"

#define	NUM_PARSE_REPLIES 4
#define	NUM_ERROR_REPLIES (CLEP_LAST_ERROR + 1 - CLEP_FIRST_ERROR)
#define	NUM_WARNING_REPLIES (CLEP_LAST_WAR + 1 - CLEP_FIRST_WAR)

/*
 * class xml_module
 * -------------------
 * This class encapsulates the xml functionality of the cl_apid.
 * There is only ever one instance of this class in existence in a program.
 * Clients should call the static initialize method to create that one
 * instance.  Subsequently, references to the instance can be obtained by
 * calling the static method the().
 */
class xml_module {
public:
	/*
	 * initialize
	 * -------------
	 * This method should be called exactly once in a program to
	 * create and initialize the one instance of xml_module.
	 */
	static int initialize(char *dtd_path);

	/*
	 * shutdown
	 * ----------
	 * Called on the xml_module object to tell it to destroy itself.
	 * There are two phases because the xml_module depends on the
	 * reg_comm_module and vice-versa.  The shutdown_start stops
	 * dependencies on other modules, and the shtudown_finish finishes
	 * the shutdowning and deletion.
	 */
	void shutdown_start();
	void shutdown_finish();

	/*
	 * the
	 * ----
	 * Gives a reference to the one xml_module instance.  Initialize
	 * must be called before the.
	 */
	static xml_module *the();

	/*
	 * queue_registration, queue_event
	 * -------------------------------
	 * Interfaces for the registration communication module and
	 * event reception module, respectively, to enqueue incoming
	 * registrations or events.
	 */
	int queue_registration(sc_registration_instance *reg);
	int queue_event(sc_xml_event *evt);


	/*
	 * Helper methods for generating client replies.
	 *
	 * get_parse_error_reply returns the error reply corresponding to the
	 * error code from sc_xml_instance.h.
	 *
	 * get_reg_reply return the error reply corresponding to the error
	 * code from cl_apid_error.h
	 */
	sc_xml_reply *get_reg_reply(int);
	sc_xml_reply *get_parse_error_reply(int);

	/*
	 * Thread routines.  They are public so that the extern "C" defined
	 * wrapper functions called by pthread_create can call the
	 * actual methods on the xml_module object.
	 */
	void xml_reg_thread();
	void xml_evt_thread();

private:
	/*
	 * ctor/dtor are private so only the static initialize method
	 * and shutdown method can create/destroy it.
	 */
	xml_module();
	~xml_module();

	/*
	 * Declare the queue structures for queueing the incoming
	 * registrations and events.
	 *
	 * Each queue has an associated mutex and condition variable.
	 */
	sc_queue<sc_registration_instance> reg_queue;
	sc_queue<sc_xml_event> evt_queue;

	pthread_mutex_t reg_lock;
	pthread_mutex_t evt_lock;
	pthread_cond_t reg_cond;
	pthread_cond_t evt_cond;

	/*
	 * We have flags for each of the threads, in case we want to
	 * shut them down.
	 */
	bool shutdown_evt, shutdown_reg;

	/*
	 * Variables to keep track of the thread ids for our two threads.
	 */
	static pthread_t evt_thr, reg_thr;

	int create_xml_replies();

	/*
	 * check_registration_parsing
	 * ---------------------------
	 * Helper function for the registration thread.
	 */
	void check_registration_parsing(
	    SList<sc_registration_instance> *cur_regs);
	void check_registration_events(
	    SList<sc_registration_instance> *cur_regs);

	/*
	 * Storage space for the xml replies
	 */
	sc_xml_reply *parse_error_replies[NUM_PARSE_REPLIES];
	sc_xml_reply *error_replies[NUM_ERROR_REPLIES];
	sc_xml_reply *warning_replies[NUM_WARNING_REPLIES];
	sc_xml_reply *unknown_error_reply;
	sc_xml_reply *ok_reply;

	/*
	 * The strings that we use in the error reply messages.
	 */
	static const char *parse_error_strs[NUM_PARSE_REPLIES];
	static const char *error_strs[NUM_ERROR_REPLIES];
	static const char *warning_strs[NUM_WARNING_REPLIES];

	static const reply_code parse_error_codes[NUM_PARSE_REPLIES];
	static const reply_code error_codes[NUM_ERROR_REPLIES];
	static const reply_code warning_codes[NUM_WARNING_REPLIES];

	/* for libxml2 */
	static char *catalog_path;
	static const char *get_catalog_path();
	static void set_catalog_path(const char *catalog_path_in);

	/*
	 * The one instance of xml_module
	 */
	static xml_module *the_xml_module;
};

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#endif /* _XML_MODULE_H_ */
