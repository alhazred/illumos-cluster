/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma	ident	"@(#)reg_comm_module.cc	1.13	08/05/20 SMI"

#include <pthread.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/cl_assert.h>
#include <sys/hashtable.h>
#include <orb/fault/fault_injection.h>

#include "reg_comm_module.h"
#include "cl_apid_include.h"
#include "cl_apid_error.h"
#include "net_tools.h"
#include "sc_xml_reply.h"
#include <orb/fault/fault_injection.h>

/*
 * First, define the methods that are run by the two main threads in
 * this module.
 */

#define	BUF_SIZE 4096

/*
 * recept_thread
 * ----------------
 * This function is run by the registration reception thread.
 * It runs around a select loop, waiting only for sockets to be readable.
 * It starts with only the listening socket, but as each socket is accepted,
 * it is added to the list of listening sockets.  As it becomes readable,
 * the registration info is read from the socket, and when the read side
 * of the socket closes, an sc_registration_instance object is constructed
 * based on the registration string, and is queued in the xml_module.
 */
void reg_comm_module::recept_thread()
{
#ifdef _FAULT_INJECTION
	if (fault_triggered(FAULTNUM_CLAPI_REG_REPT_THREAD_R,
		NULL, NULL)) {
		exit(1);
	}
#endif
	FAULTPT_CLAPI(FAULTNUM_CLAPI_REG_REPT_THREAD_B,
		FaultFunctions::generic);

	sc_syslog_msg_handle_t handle;
	fd_set allset, rset;
	int maxfd, listenfd, nready, err;
	hashtable_t<reg_cli_connect *, int> connections;
	time_t next_connection_timeout;
	struct timeval timer;

	scds_syslog_debug(1, "recept_thread begin\n");

	/*
	 * First, set up our listening socket.
	 */
	if (create_listening_socket(&listenfd, net_name->get_cb_ip(),
				    net_name->get_cb_port()) == -1) {
		/*
		 * The function should have already logged an error message.
		 * We need to terminate the program.
		 */
		exit(1);
	}

	maxfd = listenfd;
	/*
	 * allset tracks the current sockets on which we're waiting.
	 * Can't just use rset, because select modifies the fd_sets.
	 */
	FD_ZERO(&allset); /*lint !e534 */
	FD_SET(listenfd, &allset); /*lint !e573 !e737 !e573 !e713 */

	/*
	 * Now set up the signal pipe
	 */
	FD_SET(signal_pipe[0], &allset); /*lint !e573 !e737 !e573 !e713 */
	if (signal_pipe[0] > maxfd) {
		maxfd = signal_pipe[0];
	}

	/*
	 * Loop forever, processing incoming registrations.
	 * The only way we break out is if we receive a message on the
	 * signal_pipe.
	 */
	while (B_TRUE) {

		/*
		 * Check if any of our currently open connections
		 * have timed out.
		 */
		next_connection_timeout = check_timedout_clients(&allset,
		    &connections);

		/*
		 * reinitialize reset each time, because select modified
		 * it last time.
		 */
		rset = allset;

		/*
		 * We have two cases here: if we have any active connections,
		 * we have to use a timeout.  Otherwise we can sleep until
		 * our pipe gets written to.
		 */
		if (next_connection_timeout != 0) {
			/* We have active */
			time_t cur_time = time(NULL);

			/*
			 * set up our timeout value to the timeout for checking
			 * the connection timeout.
			 */
			timer.tv_sec = next_connection_timeout - cur_time;
			if (timer.tv_sec < 0)
				timer.tv_sec = 0;
			timer.tv_usec = 0;

			scds_syslog_debug(1, "reg_comm "
			    "connection to be timed out: "
			    "sleeping for %d\n", timer.tv_sec);
			nready = select(maxfd + 1, &rset, NULL, NULL, &timer);
		} else {
			scds_syslog_debug(1, "reg_comm --  "
			    "no connections to be timed out: "
			    "sleeping forever\n");

			nready = select(maxfd + 1, &rset, NULL, NULL, NULL);
		}

		scds_syslog_debug(1, "recept_thr: select. nread=%d\n", nready);

		/* check for error condition */
		if (nready < 0) {
			err = errno;

			/* error on select */
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
			(void) sc_syslog_msg_log(handle, LOG_ERR,
			    MESSAGE, "select: %s", strerror(err));
			sc_syslog_msg_done(&handle);

			/* just call select again */
			continue;
		}

		/*
		 * First, check if there's something to read on the pipe.
		 * If so, we break out of the loop.
		 */
		/*lint -e713 -e737 -e573 */
		if (FD_ISSET(signal_pipe[0], &rset)) {
			scds_syslog_debug(1,
				"recept_thread: received shutdown msg "
			    " thru pipe\n");
			break;
		}
		/*lint +e713 +e737 +e573 */

		/*
		 * We know we have at least one socket ready to read
		 * first check the listening fd
		 */
		check_listening_socket(listenfd, rset, &allset, &connections,
		    &maxfd, &nready);

		/* optimization */
		if (nready == 0)
			continue;

		/*
		 * Now check for all the connection on which we're
		 * waiting for input.
		 */
		check_accepted_connections(rset, &allset, &connections,
		    nready);
	}

	/*
	 * If we get here, we're done processing registrations.
	 */
	/* close all open connections, starting with our listening socket */
	(void) close(listenfd);

	/*
	 * Iterate through the hashtable, closing each socket
	 */
	hashtable_t<reg_cli_connect *, int>::iterator hash_it(connections);
	reg_cli_connect *cur_connect;

	for (cur_connect = hash_it.get_current(); cur_connect != NULL;
	    cur_connect = hash_it.get_current()) {

		/* advance the iterator */
		hash_it.advance();

		(void) close(cur_connect->sock);
		(void) connections.remove(cur_connect->sock, true);
	}
	scds_syslog_debug(1, "recept_thread shutting down\n");
	pthread_exit(NULL);
}

/*
 * check_accepted_connections
 * --------------------------
 * Iterates through each reg_cli_connect in the conns hashtable, checking
 * if its socket has been set in the rset.  If so, it reads from the socket
 * (which should not block).  If the read is 0 bytes, the socket has
 * been closed in the reading direction (the client is done writing),
 * so we can process this client's registration.
 */
void reg_comm_module::check_accepted_connections(const fd_set &rset,
    fd_set *allset, hashtable_t<reg_cli_connect *, int> *conns, int nready)
{
	int bytesread, err;
	sc_syslog_msg_handle_t handle;
	reg_cli_connect *cur_connect;
	char read_buf[BUF_SIZE];
	sc_registration_instance *new_reg = NULL;

	/*
	 * Iterate through the hashtable, checking if each
	 * connection is ready for reading.
	 */
	hashtable_t<reg_cli_connect *, int>::iterator hash_it(*conns);

	for (cur_connect = hash_it.get_current(); (cur_connect != NULL) &&
	    (nready > 0); cur_connect = hash_it.get_current()) {

		/* advance the iterator */
		hash_it.advance();

		/* Check if this client is ready to read */

		/*
		 * Lint doesn't like the FD_*
		 * macros.  Suppress the errors.
		 */
		/*lint -e713 -e737 -e573 */
		if (FD_ISSET(cur_connect->sock, &rset)) {
			/*lint +e713 +e737 +e573 */
			/* decrement our counter */
			nready--;

			/* do the read */
			bytesread = read(cur_connect->sock, read_buf,
			    BUF_SIZE);

			scds_syslog_debug(1, "recept_thr: read %d from %s:%d\n",
			    bytesread, cur_connect->host, cur_connect->port);

			/* Reset the timeout time */
			cur_connect->connection_timeout = time(NULL) +
			    client_timeout;

			/* Check for an error condition */
			if (bytesread == -1) {
				err = errno;
				(void) sc_syslog_msg_initialize(&handle,
				    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
				//
				// SCMSGS
				// @explanation
				// The rpc.fed server was not able to execute
				// the read system call properly. The message
				// contains the system error. The server will
				// not capture the output from methods it
				// runs.
				// @user_action
				// Save the /var/adm/messages file. Contact
				// your authorized Sun service provider to
				// determine whether a workaround or patch is
				// available.
				//
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE, "read: %s", strerror(err));
				sc_syslog_msg_done(&handle);
				/*
				 * Clear out this client without processing
				 * its registration.
				 */
				/*
				 * Lint doesn't like the FD_*
				 * macros.  Suppress the errors.
				 */
				/*lint -e713 -e737 -e573 */
				FD_CLR(cur_connect->sock, allset);
				/*lint +e713 +e737 +e573 */
				(void) close(cur_connect->sock);
				(void) conns->remove(cur_connect->sock, true);
			} else if (bytesread == 0) {
				/*
				 * The client is done writing
				 * We'll assume they only closed their write
				 * half, and left the read half open so we
				 * can send our response.  Technically, it's
				 * a protocol error, but we accept it.
				 */
				create_registration(cur_connect, NULL);
				/*
				 * Lint doesn't like the FD_*
				 * macros.  Suppress the errors.
				 */
				/*lint -e713 -e737 -e573 */
				FD_CLR(cur_connect->sock, allset);
				/*lint +e713 +e737 +e573 */
				(void) conns->remove(cur_connect->sock, true);
				/*
				 * We don't close our half of the
				 * connection because we will
				 * need to write back to the client
				 * after we have processed the
				 * registration.
				 */
			} else {
				/*
				 * Save the bytes we read, and check
				 * if we've read the entire message.
				 */
				read_buf[bytesread] = '\0';
				if (cur_connect->buf.append(read_buf) == -1) {
					(void) sc_syslog_msg_initialize(
					    &handle,
					    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
					//
					// SCMSGS
					// @explanation
					// The cl_apid experienced an internal
					// error (probably a memory error)
					// that prevented it from processing a
					// client registration request.
					// @user_action
					// Examine other syslog messages
					// occurring at about the same time to
					// see if the problem can be
					// identified. Save a copy of the
					// /var/adm/messages files on all
					// nodes and contact your authorized
					// Sun service provider for assistance
					// in diagnosing and correcting the
					// problem.
					//
					(void) sc_syslog_msg_log(handle,
					    LOG_ERR, MESSAGE,
					    "Unable to process client "
					    "registration");
					sc_syslog_msg_done(&handle);
					/*
					 * Clear out this client without
					 * processing its registration.
					 */
					/*
					 * Lint doesn't like the FD_*
					 * macros.  Suppress the errors.
					 */
					/*lint -e713 -e737 -e573 */
					FD_CLR(cur_connect->sock, allset);
					/*lint +e713 +e737 +e573 */
					(void) close(cur_connect->sock);
					(void) conns->remove(
					    cur_connect->sock, true);
				} else if (sc_registration_instance::
				    is_complete_message(cur_connect->
				    buf.get_buf(), cur_connect->host,
				    &new_reg)) {
					/*
					 * We have read an entire
					 * sc_registration_info xml message.
					 */
					create_registration(cur_connect,
					    new_reg);
					/*
					 * Lint doesn't like the FD_*
					 * macros.  Suppress the errors.
					 */
					/*lint -e713 -e737 -e573 */
					FD_CLR(cur_connect->sock, allset);
					/*lint +e713 +e737 +e573 */
					(void) conns->remove(
					    cur_connect->sock, true);
				}
			}
		} /* if (FD_ISSET(...)) */
	} /* for */
}

/*
 * Iterate through the list of clients in the conns table, checking
 * if any of them have a connection_timeout value that is past the current
 * time.  If so, we time out the connection by removing the client from
 * the table and from the allset.
 *
 * We also find the lowest, non-zero, connection_timeout field of all
 * the clients, and return it.
 */
time_t reg_comm_module::check_timedout_clients(fd_set *allset,
    hashtable_t<reg_cli_connect *, int> *conns)
{
	reg_cli_connect *cur_connect;
	time_t lowest_time = 0;
	time_t cur_time = time(NULL);

	/*
	 * Iterate through the hashtable, checking if each
	 * connection should be timed out.
	 */
	hashtable_t<reg_cli_connect *, int>::iterator hash_it(*conns);

	for (cur_connect = hash_it.get_current(); cur_connect != NULL;
	    cur_connect = hash_it.get_current()) {

		/* advance the iterator */
		hash_it.advance();

		/*
		 * Check if this client should be timed out.
		 * We only have clients in this table if they are (or should
		 * be) active.
		 */
		if (cur_connect->connection_timeout <= cur_time) {

			/* This connection has timed out */
			scds_syslog_debug(1, "reg_comm: client "
			    "connection timed out.  Removing it.\n");

			/*
			 * Clear out this client without processing
			 * its registration.
			 */
			/*
			 * Lint doesn't like the FD_*
			 * macros.  Suppress the errors.
			 */
			/*lint -e713 -e737 -e573 */
			FD_CLR(cur_connect->sock, allset);
			/*lint +e713 +e737 +e573 */
			(void) close(cur_connect->sock);
			(void) conns->remove(cur_connect->sock, true);

		} else if (lowest_time == 0 ||
		    cur_connect->connection_timeout < lowest_time) {
			lowest_time = cur_connect->connection_timeout;
		}
	} /* for */
	return (lowest_time);
}


/*
 * check_listening_socket
 * -------------------------
 * Based on the reset, checks if the listenfd is ready for reading.
 * If so, it accepets a connection from it, creates a reg_cli_connect
 * to track the connection, and stores it in the conns hashtable.
 * Updates maxfd, allset, and nready if appropriate.
 */
void reg_comm_module::check_listening_socket(int listenfd, const fd_set &rset,
    fd_set *allset, hashtable_t<reg_cli_connect *, int> *conns, int *maxfd,
    int *nready)
{
	socklen_t clilen;
	struct sockaddr_in cliaddr;
	reg_cli_connect *cur_connect;
	int err, connfd;
	sc_syslog_msg_handle_t handle;

	/*
	 * Lint doesn't like the FD_*
	 * macros.  Suppress the errors.
	 */
	if (FD_ISSET(listenfd, &rset)) { /*lint !e737 !e573 */
		/* this is one of our ready sockets */
		(*nready)--;

		/* accept the new connection */
		clilen = sizeof (cliaddr);
		if ((connfd = accept(listenfd, (struct sockaddr *)&cliaddr,
		    &clilen)) == -1) {
			err = errno;
			/*
			 * This is an error condition, but there's
			 * not much we can do about it.
			 * Just syslog an error, then keep going.
			 */
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
			//
			// SCMSGS
			// @explanation
			// The cl_apid received the specified error from
			// accept(3SOCKET). The attempted connection was
			// ignored.
			// @user_action
			// No action required. If the problem persists, save a
			// copy of the /var/adm/messages files on all nodes
			// and contact your authorized Sun service provider
			// for assistance in diagnosing and correcting the
			// problem.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR,
			    MESSAGE, "accept: %s", strerror(err));
			sc_syslog_msg_done(&handle);
		} else {
			/* Create the structure to track the connection */
			if ((cur_connect = new reg_cli_connect) == NULL) {
				scds_syslog_debug(1, "check_listening_socket: "
					"NULL cur_con\n");
				/* low memory; skip this one */
				(void) sc_syslog_msg_initialize(&handle,
				    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
				(void) sc_syslog_msg_log(handle,
				    LOG_ERR, MESSAGE, "Error: low memory");
				sc_syslog_msg_done(&handle);

				(void) close(connfd);
				return;
			}

			/* save the socket */
			cur_connect->sock = connfd;

			/* get the host and port */
			if (inet_ntop(AF_INET, &cliaddr.sin_addr,
			    cur_connect->host, INET6_ADDRSTRLEN) == NULL) {
				err = errno;
				(void) sc_syslog_msg_initialize(&handle,
				    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
				//
				// SCMSGS
				// @explanation
				// The cl_apid received the specified error
				// from inet_ntop(3SOCKET). The attempted
				// connection was ignored.
				// @user_action
				// No action required. If the problem
				// persists, save a copy of the
				// /var/adm/messages files on all nodes and
				// contact your authorized Sun service
				// provider for assistance in diagnosing and
				// correcting the problem.
				//
				(void) sc_syslog_msg_log(handle,
				    LOG_ERR, MESSAGE, "inet_ntop: %s",
				    strerror(err));
				sc_syslog_msg_done(&handle);

				/* don't save the info */
				(void) close(connfd);
				delete cur_connect;
				return;
			}
			/* save the numeric version as well */
			cur_connect->host_n =
			    (uint32_t)(cliaddr.sin_addr.s_addr);
			/* get the port */
			cur_connect->port = ntohs(cliaddr.sin_port);

			/* Set the timeout time */
			cur_connect->connection_timeout = time(NULL) +
			    client_timeout;

			/*
			 * We've accepted the new connection.
			 * Update the fd set and put the socket
			 * in the hashtable.
			 */

			/*
			 * FD_SET does a lot of things that
			 * lint doesn't like, so we suppress the lint warnings.
			 */
			FD_SET(connfd, allset);	/*lint !e713 !e737 !e573 */
			if (connfd > *maxfd) {
				*maxfd = connfd;
			}

			/* actually add it to our hashtable */
			conns->add(cur_connect, connfd);
			scds_syslog_debug(1,
				"recept_thr: accepted con. from %s:%d\n",
			    cur_connect->host, cur_connect->port);
		}
	}
}

/*
 * create_registration
 * ---------------------
 * Creates a registration instance and queues it in the xml
 * module.  We can be passed an already existing sc_registration_instance
 * which we should use, if it exists.
 */
void reg_comm_module::create_registration(reg_cli_connect *client,
    sc_registration_instance *reg)
{
	FAULTPT_CLAPI(FAULTNUM_CLAPI_REG_COMM_CREAT_REG_B,
	    FaultFunctions::generic);

	sc_syslog_msg_handle_t handle;
	int res;

	// if we don't have an sc_registration_instance yet, create one
	if (reg == NULL) {
		reg = new sc_registration_instance(client->buf.get_buf(),
		    client->host, client->sock);
	} else {
		// if we already have one, we need to set its socket.
		reg->set_sock(client->sock);
	}

	if (reg == NULL || reg->get_construct_status() != 0) {
		/* low memory; skip this one */
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The cl_apid experienced a memory error that prevented it
		// from processing a client registration request.
		// @user_action
		// Increase swap space, install more memory, or reduce peak
		// memory consumption.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Low memory: unable to process client registration");
		sc_syslog_msg_done(&handle);
		return;
	}

	/*
	 * Check that the message was well-formed.
	 */
	if ((res = reg->get_parse_result()) != PARSED) {
		scds_syslog_debug(1, "Unable to parse: %s\n", reg->get_xml() ?
		    reg->get_xml() : "NULL");
		/* send the client a message */
		(void) send_reply_message(client->sock,
		    xml_module::the()->get_parse_error_reply(res)->get_xml());
		delete reg;
		return;
	}

	/* check it against the tcp_wrapper */
	CL_PANIC(pthread_mutex_lock(&wrapper_lock) == 0);
	if (wrapper->verify_ip(client->host_n, client->host) != 0 ||
	    wrapper->verify_ip(reg->get_callback_info().get_cb_ip())
	    != 0) {
		CL_PANIC(pthread_mutex_unlock(&wrapper_lock) == 0);
		/* send the client a message */
		(void) send_reply_message(client->sock,
		    xml_module::the()->get_reg_reply(CLEP_ERR_AUTH)->
		    get_xml());
		delete reg;
		return;
	}
	CL_PANIC(pthread_mutex_unlock(&wrapper_lock) == 0);
	scds_syslog_debug(1, "queueing reg for %s:%d -- %s\n", client->host,
	    client->port, client->buf.get_buf());
	if (xml_module::the()->queue_registration(reg) != 0) {
		delete reg;
	}
	FAULTPT_CLAPI(FAULTNUM_CLAPI_REG_COMM_CREAT_REG_E,
	    FaultFunctions::generic);

}

/*
 * send_thread
 * ----------------
 * This function is run by the send thread.
 * It waits for a msg_reply to appear on the send queue, then sends
 * the msg to the sock specified.
 */
void reg_comm_module::send_thread()
{
	msg_reply *rep;

	scds_syslog_debug(1, "send_thread begin\n");

#ifdef _FAULT_INJECTION
	if (fault_triggered(FAULTNUM_CLAPI_REG_SEND_THREAD_R,
		NULL, NULL)) {
		exit(1);
	}
#endif
	FAULTPT_CLAPI(FAULTNUM_CLAPI_REG_SEND_THREAD_B,
		FaultFunctions::generic);

	/*
	 * Loop forever, waiting for reply deliveries to arrive, or for
	 * the module to be shutdown.
	 */
	while (true) {
		/*
		 * Grab the lock protecting the send_queue and
		 * the shutdown_send.
		 */
		CL_PANIC(pthread_mutex_lock(&send_lock) == 0);

		/*
		 * Wait for there to be something on the queue or
		 * to be told to shutdown.
		 */
		while (send_queue.peek() == NULL && !shutdown_send) {
			CL_PANIC(
			    pthread_cond_wait(&send_cond, &send_lock) == 0);
		}

		/*
		 * If we're here we could either be told to shutdown,
		 * there's something on the queue, or both.
		 * If just something on the queue: process it.
		 * If just shutting down: break out of the big loop.
		 * If both, process what's on the queue, then loop back
		 * again.  If there is still something on the queue when
		 * we get back, keep processing.  Eventually there will
		 * be nothing on the queue.  At that point we break out of
		 * the loop.
		 */
		if (send_queue.peek() == NULL) {

			/*
			 * If we wake up, but there's nothing on the list,
			 * we're shutting down.
			 */
			CL_PANIC(pthread_mutex_unlock(&send_lock) == 0);

			scds_syslog_debug(1, "reg send thread shutting down\n");
			// Here's where we break out of the infinite loop.
			break;
		}

		/*
		 * Read the first event off the queue.
		 */
		rep = send_queue.dequeue();

		// Release the lock for someone else.
		CL_PANIC(pthread_mutex_unlock(&send_lock) == 0);

		scds_syslog_debug(1, "Writing %s to %d\n", rep->msg, rep->sock);

		/*
		 * It doesn't matter whether we succeed or not,
		 * because we have no way of reconnecting to
		 * retry.
		 *
		 * sock_write is a utility in the net_tools module
		 */
		(void) sock_write(rep->sock, rep->msg);
		(void) close(rep->sock);
		delete rep;
	}

	/*
	 * If we get here, we're shutting down.
	 * There should not be any pending events.
	 */
	scds_syslog_debug(1, "send_thread exiting\n");
	pthread_exit(NULL);
}


/*
 * define/initialize the static members
 */
reg_comm_module *reg_comm_module::the_reg_comm_module = NULL;
pthread_t reg_comm_module::recept_thr;
pthread_t reg_comm_module::send_thr;


/*
 * ctor/dtor
 * -------------
 * Note that these are private, so ctor only called from initialize, and
 * dtor only from shutdown.
 *
 * Constructor initializes the two mutexes and condition variables,
 * and saves the initial ip,port, and hosts lists.
 *
 * Exits the program if anything goes wrong.
 */
reg_comm_module::reg_comm_module(char *ip_in, int port_in,
    tcp_wrapper &wrapper_in, int timeout_in)
{
	sc_syslog_msg_handle_t handle;
	int err;

	/*
	 * Check the status of the queue
	 */
	if (send_queue.get_construct_status() != 0) {
		scds_syslog_debug(1, "reg_comm_module::reg_comm_module: "
			"invalid send_queue\n");
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Error: low memory");
		sc_syslog_msg_done(&handle);
		exit(1);
	}

	/*
	 * Create the sc_callback_info
	 */
	net_name = new sc_callback_info(ip_in, port_in);
	if (net_name == NULL || net_name->get_construct_status() != 0) {
		scds_syslog_debug(1, "reg_comm_module::reg_comm_module: "
		    "net_name NULL or invalid\n");
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Error: low memory");
		sc_syslog_msg_done(&handle);
		exit(1);
	}

	/*
	 * Copy the tcp_wrapper.
	 */
	wrapper = new tcp_wrapper(wrapper_in);
	if (wrapper == NULL || wrapper->get_construct_status() != 0) {
		scds_syslog_debug(1, "reg_comm_module::reg_comm_module: "
		    "wrapper NULL or invalid\n");
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Error: low memory");
		sc_syslog_msg_done(&handle);
		exit(1);
	}

	/* Assign the timeout */
	client_timeout = timeout_in;

	/*
	 * Create the mutexes.
	 */
	if ((err = pthread_mutex_init(&send_lock, NULL)) != 0) {
		char *err_str = strerror(err);
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "pthread_mutex_init: %s", err_str ? err_str : "");
		sc_syslog_msg_done(&handle);
		exit(1);
	}
	if ((err = pthread_cond_init(&send_cond, NULL)) != 0) {
		char *err_str = strerror(err);
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The cl_apid was unable to initialize a synchronization
		// object, so it was not able to start-up. The error message
		// is specified.
		// @user_action
		// Save a copy of the /var/adm/messages files on all nodes and
		// contact your authorized Sun service provider for assistance
		// in diagnosing and correcting the problem.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "pthread_cond_init: %s", err_str ? err_str : "");
		sc_syslog_msg_done(&handle);
		exit(1);
	}
	if ((err = pthread_mutex_init(&wrapper_lock, NULL)) != 0) {
		char *err_str = strerror(err);
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "pthread_mutex_init: %s", err_str ? err_str : "");
		sc_syslog_msg_done(&handle);
		exit(1);
	}

	/*
	 * Create the pipe
	 */
	if (pipe(signal_pipe) != 0) {
		err = errno;
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "pipe: %s", strerror(err));
		sc_syslog_msg_done(&handle);
		exit(1);
	}

	shutdown_send = false;

	/*
	 * Create the xml_reply for clients that fail authentication.
	 */
	if ((authent_reply = new sc_xml_reply(FAIL,
	    "Error: Failed Authentication")) == NULL ||
	    authent_reply->get_construct_status() != 0) {
		scds_syslog_debug(1, "reg_comm_module::reg_comm_module: "
		    "NULL or invalid authent_reply\n");
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Error: low memory");
		sc_syslog_msg_done(&handle);
		exit(1);
	}
}

reg_comm_module::~reg_comm_module()
{
	(void) pthread_cond_destroy(&send_cond);
	(void) pthread_mutex_destroy(&send_lock);
	(void) pthread_mutex_destroy(&wrapper_lock);

	(void) close(signal_pipe[1]);
	(void) close(signal_pipe[0]);

	delete net_name;
	delete wrapper;
	delete authent_reply;
}


/*
 * Wrapper functions for thread creation.
 * They get a reference to the one reg_comm_module, and
 * call the method on that object that we really want to run.
 *
 * Defined with "C" linkage becasue they're called by a C function.
 */
extern "C" void recept_thread_wrapper(void *)
{
	reg_comm_module::the()->recept_thread();
}

extern "C" void send_thread_wrapper(void *)
{
	reg_comm_module::the()->send_thread();
}

int reg_comm_module::initialize(char *ip, int port, tcp_wrapper &wrapper_in,
    int timeout_in)
{
	sc_syslog_msg_handle_t handle;

	scds_syslog_debug(1, "Initializing reg_comm_module\n");

#ifdef _FAULT_INJECTION
	void		*f_argp;
	uint32_t	f_argsize;
	if (fault_triggered(FAULTNUM_CLAPI_REG_COMM_INITIALIZE_R,
		&f_argp, &f_argsize)) {
		ASSERT(f_argsize == sizeof (int));
		return (*((int *)f_argp));
	}
#endif
	FAULTPT_CLAPI(FAULTNUM_CLAPI_REG_COMM_INITIALIZE_B,
		FaultFunctions::generic);

	/* initialize should only be called once */
	CL_PANIC(the_reg_comm_module == NULL);

	/* create the one instance of reg_comm_module */
	the_reg_comm_module = new reg_comm_module(ip, port, wrapper_in,
	    timeout_in);
	if (the_reg_comm_module == NULL) {
		scds_syslog_debug(1,
			"r_c_m::initialize: NULL the_reg_comm_module\n");
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Error: low memory");
		sc_syslog_msg_done(&handle);
		return (ENOMEM);
	}

	/*
	 * Spawn the two threads in the reg_comm module.  When we
	 * create them, have them start with the wrapper functions
	 * defined above, because we can't have a C function (pthread_create)
	 * calling a C++ method on an object, because it doesn't know how
	 * to pass the this pointer properly.
	 *
	 */
	int ret;
	if ((ret = pthread_create(&recept_thr, NULL,
	    (void *(*)(void *))recept_thread_wrapper, NULL)) != 0) {
		char *err_str = strerror(ret);
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "pthread_create: %s", err_str ? err_str : "");
		sc_syslog_msg_done(&handle);
		return (ret);
	}
	if ((ret = pthread_create(&send_thr, NULL,
	    (void *(*)(void *))send_thread_wrapper, NULL)) != 0) {
		char *err_str = strerror(ret);
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "pthread_create: %s", err_str ? err_str : "");
		sc_syslog_msg_done(&handle);
		return (ret);
	}

	return (0);
}

/*
 * shutdown
 * -------------
 * Set the shutdown_send boolean to true and write to the
 * signal_pipe, and then wait for the two threads to finish.
 */
void reg_comm_module::shutdown_start()
{
	sc_syslog_msg_handle_t handle;

	/* terminate the threads */

	// tell them to shutdown
	scds_syslog_debug(1, "Writing to signal pipe\n");
	if (write(signal_pipe[1], "\n", 1) <= 0)  {
		char *err_str = strerror(errno);
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "write: %s", err_str ? err_str : "");
		sc_syslog_msg_done(&handle);
		scds_syslog_debug(1, "Failed to write to signal pipe\n");

		/*
		 * we're trying to exit anyway; now we can't do it
		 * gracefully.
		 */
		exit(1);
	}

	CL_PANIC(pthread_mutex_lock(&send_lock) == 0);
	shutdown_send = true;
	CL_PANIC(pthread_cond_broadcast(&send_cond) == 0);
	CL_PANIC(pthread_mutex_unlock(&send_lock) == 0);

	// wait for them to exit
	(void) pthread_join(recept_thr, NULL);
	(void) pthread_join(send_thr, NULL);
}

void reg_comm_module::shutdown_finish()
{
	scds_syslog_debug(1, "Finishing shutdown of reg_comm_module\n");
	// delete ourselves
	delete the_reg_comm_module;
	the_reg_comm_module = NULL;
}

reg_comm_module *reg_comm_module::the()
{
	// DON'T check for NULL and assert here, because the xml
	// module gets shut down after us, but calls the on us.
	return (the_reg_comm_module);
}

/*
 * reload
 * ---------
 * This method can be called to change the parameters specified
 * originally in the initialize method.
 */
int reg_comm_module::reload(tcp_wrapper &wrapper_in, int timeout_in)
{
	/* need to get lock */
	sc_syslog_msg_handle_t handle;

	scds_syslog_debug(1, "in reg_comm_module::reload\n");
	tcp_wrapper *new_wrapper = new tcp_wrapper(wrapper_in);
	if (new_wrapper == NULL || new_wrapper->get_construct_status() != 0) {
		scds_syslog_debug(1,
			"r_c_m::reload: NULL or invalid new_wrapper\n");
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Error: low memory");
		sc_syslog_msg_done(&handle);

		/* just keep the old object */
		scds_syslog_debug(1, "Keeping old tcp_wrapper\n");
		return (ENOMEM);
	}

	CL_PANIC(pthread_mutex_lock(&wrapper_lock) == 0);
	delete wrapper;
	wrapper = new_wrapper;
	client_timeout = timeout_in;
	CL_PANIC(pthread_mutex_unlock(&wrapper_lock) == 0);

	scds_syslog_debug(1, "Successfully made new tcp_wrapper\n");
	return (0);
}


/*
 * send_reply_message
 * ----------------
 * Just puts the message on the send_queue and returns.
 */
int reg_comm_module::send_reply_message(int sock, const char *msg)
{
#ifdef _FAULT_INJECTION
	void		*f_argp;
	uint32_t	f_argsize;
	if (fault_triggered(FAULTNUM_CLAPI_REG_SEND_REPLY_M_R,
		&f_argp, &f_argsize)) {
		ASSERT(f_argsize == sizeof (int));
		return (*((int *)f_argp));
	}
#endif
	sc_syslog_msg_handle_t handle;

	/* Make sure we have a valid socket file descriptor */
	if (sock < 0) {
		scds_syslog_debug(1, "Cannot send reply to socket %d\n", sock);
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The cl_apid experienced an internal error.
		// @user_action
		// No action required. If the problem persists, save a copy of
		// the /var/adm/messages files on all nodes and contact your
		// authorized Sun service provider for assistance in
		// diagnosing and correcting the problem.
		//
		(void) sc_syslog_msg_log(handle, LOG_INFO, MESSAGE,
		    "Cannot send reply: invalid socket");
		sc_syslog_msg_done(&handle);
		return (0);
	}

	msg_reply *rep = new msg_reply(sock, msg);
	if (rep == NULL || rep->msg == NULL) {
		scds_syslog_debug(1,
			"send_reply_message: NULL or invalid msg_reply\n");
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Error: low memory");
		sc_syslog_msg_done(&handle);
		return (ENOMEM);
	}
	CL_PANIC(pthread_mutex_lock(&send_lock) == 0);
	int res = send_queue.enqueue(rep);
	CL_PANIC(pthread_cond_signal(&send_cond) == 0);
	CL_PANIC(pthread_mutex_unlock(&send_lock) == 0);
	return (res);
}
