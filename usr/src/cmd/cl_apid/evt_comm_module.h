/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_EVT_COMM_MODULE_H_
#define	_EVT_COMM_MODULE_H_

#pragma ident	"@(#)evt_comm_module.h	1.9	08/05/20 SMI"

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#include "sc_queue.h"
#include "caapi_client_handle.h"
#include "smart_string.h"

/*
 * Helper class for storing the msg and the client_handles
 * to whom we need to send the message.  We need to combine them
 * into a class so that we can store it on the staging_queue.
 */

/*
 * We don't have default constructors, and we don't want them */
/*lint -e1712 */
class comm_req {
public:
	comm_req(const char *msg_in, caapi_client_handle *handle_in);
	comm_req(const char *msg_in, SList<caapi_client_handle> *handles_in);
	int get_construct_status() { return (status); }
	~comm_req();

	smart_string *msg;
	SList<caapi_client_handle> *handles;
	int status;

private:
	/*
	 * Disallow pass by value, assignment
	 */
	comm_req(const comm_req &);
	/* Cstyle doesn't understand operator overloading */
	/*CSTYLED */
	comm_req &operator=(const comm_req &);
};


/*
 * class used to track a current client, including
 * the currently open socket to it, the handle to its
 * info in the cache, and the msgs to be delivered to it.
 */
class client_connect {
public:
	client_connect(caapi_client_handle *handle_in, smart_string *);
	int get_construct_status() { return (status); }
	~client_connect();

	/*
	 * The sock is the current connection we have open to this client.
	 * If there is no current connection, sock is 0.
	 */
	int sock;

	/*
	 * The handle to the client.  This should always be valid.
	 */
	caapi_client_handle *handle;

	/*
	 * The msgs to send to this client, in order.
	 */
	sc_queue<smart_string> msgs;

	/*
	 * The cur_msg is the message we are currently attempting to send to
	 * the client.  The cur_msg is not duplicated on the msgs queue.
	 */
	smart_string *cur_msg;

	/*
	 * cur_bytes_sent is the number of bytes of the cur_msg that
	 * we have sent sucessfully to the client on the currently open
	 * connection.
	 */
	uint_t cur_bytes_sent;
	uint_t bytes_to_send;

	/*
	 * num_retries tracks the number of times in a row that we have
	 * unsucessfully attempted to contact/deliver a message to this
	 * client.
	 */
	int num_retries;

	/*
	 * next_retry_time keeps track of the next time at which we should
	 * retry delivery to this client.  If it is 0, we are not currently
	 * waiting to retry.
	 */
	time_t next_retry_time;

	/*
	 * connection_timeout keeps track of the time at which this connection
	 * will timeout if there is no response/activity from the client.
	 *
	 * A setting of 0 means that the client connection is not active.
	 */
	time_t connection_timeout;

	int status;

private:
	/*
	 * Disallow pass by value, assignment
	 */
	client_connect(const client_connect &);
	/* Cstyle doesn't understand operator overloading */
	/*CSTYLED */
	client_connect &operator=(const client_connect &);
};

/*
 * We don't have a default constructor, and we don't want one.*/
/*lint -e1712 */
class evt_comm_module
{
public:

	/*
	 * initialize
	 * ---------
	 * This method should be called exactly once in a program to
	 * create and initialize the one instance of evt_comm_module.
	 *
	 */
	static int initialize(int num_retries, int retry_interval,
	    int client_timeout);

	/*
	 * shutdown
	 * -----------
	 * Called on the evt_comm_module object to tell it to destroy itself.
	 */
	void shutdown();

	/*
	 * check_remove_client
	 * --------------------
	 * Should be called when a client is removed from the cache,
	 * to tell the evt_comm module that it needs to remove the client
	 * as well.
	 */
	void check_remove_client();

	/*
	 * the
	 * -----------
	 * Returns a reference to the one evt_comm_module object.
	 */
	static evt_comm_module *the();

	/*
	 * reload
	 * ---------
	 * This method can be called to change the parameters specified
	 * originally in the initialize method.
	 */
	int reload(int num_retries, int retry_interval, int client_timeout);

	/*
	 * send_message
	 * ------------------
	 * Request that the string msg be sent to the client or list
	 * of clients represented by the list of client_handles.
	 *
	 * This method will return immediately, before the clients have
	 * actually been contacted.  Success or failure of the contact will be
	 * logged through the client_handle interface.  Note that send_message
	 * will make a copy of the msg string, so that the caller can free it
	 * when (s)he is through with it.  The function will not make copies
	 * of the caapi_client_handles, however, and will free them at a later
	 * time after the message has been delivered.  Thus, the client of
	 * this function should not use the caapi_client_handles after this
	 * function has been called.  The SList, however, will not be used
	 * by this function, so the client can re-use it.
	 *
	 */
	int send_message(const char *msg, SList<caapi_client_handle> *handles);
	int send_message(const char *msg, caapi_client_handle *handle);

	/*
	 * Thread routine.  It is public so that the extern "C" defined
	 * wrapper functions called by pthread_create can call the
	 * actual method on the evt_comm_module object.
	 */
	void send_thread();

private:
	/*
	 * ctor/dtor are private so only the static initialize method
	 * and shutdown method can create/destroy it.
	 */
	evt_comm_module(int retries_in, int retry_interval_in,
	    int timeout_in);
	~evt_comm_module();

	/*
	 * Disallow pass by value, assignment
	 */
	evt_comm_module(const evt_comm_module &);
	/* Cstyle doesn't understand operator overloading */
	/*CSTYLED */
	evt_comm_module &operator=(const evt_comm_module &);

	/* Helper functions */
	void process_staging_queue(
	    string_hashtable_t<client_connect *> &cur_clients, fd_set *allset,
	    int &maxfd);
	int create_new_client(
	    string_hashtable_t<client_connect *> &cur_clients, fd_set *allset,
	    int &maxfd, caapi_client_handle *new_handle,
	    smart_string *new_msg);
	int initiate_msg_delivery(
	    string_hashtable_t<client_connect *> *cur_clients,
	    client_connect *client, fd_set *allset, int &maxfd);
	void retry_enqueue(
	    string_hashtable_t<client_connect *> *cur_clients,
	    client_connect *client, fd_set *allset);
	void process_retry_queue(
	    string_hashtable_t<client_connect *> *cur_clients, fd_set *allset,
	    int &maxfd);
	void check_ready_connections(const fd_set &wset,
	    fd_set *allset, int &maxfd,
	    string_hashtable_t<client_connect *> *conns, int nready);
	time_t check_timedout_clients(
	    string_hashtable_t<client_connect *> *cur_clients, fd_set *allset);
	void write_signal_pipe();


	/* our tunable parameters */
	int num_retries;
	time_t retry_interval;
	time_t client_timeout;

	/*
	 * Declare the queue structure for queueing the incoming
	 * messages
	 *
	 * The queue has an associated mutex.
	 */
	sc_queue<comm_req> staging_queue;

	pthread_mutex_t staging_lock;

	/*
	 * We use a pipe to signal the send thread that there's a new
	 * message on the staging queue, or that the module is shutting
	 * down.
	 */
	int signal_pipe[2];

	/*
	 * The retry queue, which stores clients that need to be retried.
	 * We don't need a lock, because the send_thread is the only thread
	 * that accesses it.
	 */
	sc_queue<client_connect> retry_queue;

	/*
	 * We have a flag to shut down the thread
	 */
	bool shutdown_send;

	/*
	 * Variables to keep track of the thread id
	 */
	static pthread_t send_thr;

	/*
	 * The one instance of evt_comm_module
	 */
	static evt_comm_module *the_evt_comm_module;
};
/*lint +e1712 */

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#endif /* _EVT_COMM_MODULE_H_ */
