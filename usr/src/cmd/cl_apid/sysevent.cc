/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma	ident	"@(#)sysevent.cc	1.8	08/05/20 SMI"

#include <sys/os.h>
#include <sys/sc_syslog_msg.h>
#include <libsysevent.h>
#include <sys/cl_events_int.h>

#ifndef SYSEVENTD_CHAN
#include <cl_event.h>
#endif

#include "cl_apid_include.h"
#include "xml_module.h"

/* Handle to syseventd over doors */
static sysevent_handle_t *sysevent_hp = NULL;

/*
 * queue_sysevent()
 * Enqueue the event into the XML event reception queue.
 */

#ifdef	SYSEVENTD_CHAN
static void
#else
static int
#endif
queue_sysevent(sysevent_t *ev)
{
	sc_syslog_msg_handle_t handle;

	sc_xml_event *scevt = new sc_xml_event(ev);
	if (scevt == NULL) {
		(void) sc_syslog_msg_initialize(&handle,
			SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		//
		// SCMSGS
		// @explanation
		// A process has failed to allocate new memory, most likely
		// because the system has run out of swap space.
		// @user_action
		// Reboot your system. If the problem recurs, you might need
		// to increase swap space by configuring additional swap
		// devices. See swap(1M) for more information.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			"Out of memory.");
		sc_syslog_msg_done(&handle);
#ifdef	SYSEVENTD_CHAN
		return;
#else
		return (EAGAIN);
#endif
	}
	if (xml_module::the()->queue_event(scevt) != 0) {
		// Must be out of memory
		(void) sc_syslog_msg_initialize(&handle,
			SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The cl_apid was unable to queue the incoming sysevent
		// specified.
		// @user_action
		// Examine other syslog messages occurring at about the same
		// time to see if the problem can be identified. Save a copy
		// of the /var/adm/messages files on all nodes and contact
		// your authorized Sun service provider for assistance in
		// diagnosing and correcting the problem.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			"Unable to queue event %lld", sysevent_get_seq(ev));
		sc_syslog_msg_done(&handle);

#ifdef	SYSEVENTD_CHAN
		return;
#else
		return (EAGAIN);
#endif
	}
	(void) sc_syslog_msg_initialize(&handle,
	    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
	//
	// SCMSGS
	// @explanation
	// The cl_apid successfully queued the incoming sysevent.
	// @user_action
	// This message is informational only. No action is required.
	//
	(void) sc_syslog_msg_log(handle, LOG_DEBUG, MESSAGE,
	    "Queued event %lld", sysevent_get_seq(ev));
	sc_syslog_msg_done(&handle);

#ifdef	SYSEVENTD_CHAN
		return;
#else
	return (0);
#endif
}

/*
 * Register with cl_eventd for events.
 */
int
evt_receive_initialize(void)
{
	sc_syslog_msg_handle_t handle;

#ifdef	SYSEVENTD_CHAN
	char	*subclass_list[] = {
			EC_SUB_ALL,
			NULL
			};
	sysevent_hp = sysevent_bind_handle(queue_sysevent);
	if (sysevent_hp == NULL) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The cl_apid or cl_eventd was unable to create the channel
		// by which it receives sysevent messages. It will exit.
		// @user_action
		// Save a copy of the /var/adm/messages files on all nodes and
		// contact your authorized Sun service provider for assistance
		// in diagnosing and correcting the problem.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "sysevent_bind_handle(): %s", strerror(errno));
		sc_syslog_msg_done(&handle);

		return (1);
	}
	if (sysevent_subscribe_event(sysevent_hp, "EC_Cluster",
	    (const char **)subclass_list, 1) != 0) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The cl_apid or cl_eventd was unable to create the channel
		// by which it receives sysevent messages. It will exit.
		// @user_action
		// Save a copy of the /var/adm/messages files on all nodes and
		// contact your authorized Sun service provider for assistance
		// in diagnosing and correcting the problem.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "sysevent_subscribe_event(): %s", strerror(errno));
		sc_syslog_msg_done(&handle);

		return (1);
	}
#else
	sc_syslog_msg_handle_t handle;
	if ((sysevent_hp = cl_event_open_channel(
		CL_API_DOOR)) == NULL) {
		(void) sc_syslog_msg_initialize(&handle,
			SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The cl_apid was unable to create the channel by which it
		// receives sysevent messages. It will exit.
		// @user_action
		// Save a copy of the /var/adm/messages files on all nodes and
		// contact your authorized Sun service provider for assistance
		// in diagnosing and correcting the problem.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			"Unable to open door %s: %s",
			CL_API_DOOR, strerror(errno));
		sc_syslog_msg_done(&handle);
		return (1);
	}
	if (cl_event_bind_channel(sysevent_hp, queue_sysevent) != 0) {
		(void) sc_syslog_msg_initialize(&handle,
			SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The cl_apid was unable to create the channel by which it
		// receives sysevent messages. It will exit.
		// @user_action
		// Save a copy of the /var/adm/messages files on all nodes and
		// contact your authorized Sun service provider for assistance
		// in diagnosing and correcting the problem.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			"Unable to bind door %s: %s",
			CL_API_DOOR, strerror(errno));
		sc_syslog_msg_done(&handle);
		return (1);
	}
#endif
	return (0);
}

/*
 * Shutdown sysevent reception.
 */
void
evt_receive_shutdown(void)
{
	if (sysevent_hp == NULL)
		return;

#ifdef	SYSEVENTD_CHAN
	sysevent_unbind_handle(sysevent_hp);
#else
	cl_event_close_channel(sysevent_hp);
#endif
	sysevent_hp = NULL;
}
