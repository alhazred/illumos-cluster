/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _CAAPI_REG_IO_H_
#define	_CAAPI_REG_IO_H_

#pragma ident	"@(#)caapi_reg_io.h	1.11	08/05/20 SMI"

#include <sys/param.h>
#include <sys/os.h>
#include <sys/rm_util.h>
#include <h/naming.h>
#include <nslib/ns.h>
#include <ccr/readonly_copy_impl.h>
#include <ccr/updatable_copy_impl.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#include "caapi_mapping.h"
#include "caapi_event.h"
#include "caapi_client.h"
#include "sc_callback_info.h"
#include "sc_registration_instance.h"


//
// class caapi_reg_io is the mediator class for interfacing with ccr table
// caapi_reg.
// This class has two prime public methods
//
// 1. update_clients: This method is responsible for updating the caapi_reg ccr
// table with the updated set of client details.
//
// 2. read_clients : This method reads the caapi_reg ccr table and updated the
// the client_list of caapi_reg_cache module, it also updates the event_mapping
// structure which maintains details of clients and events associated with them
// to make lookup of a event faster.
// primary client(probably the only) of this class is caapi_reg_cache class
//
// caapi_reg is the ccr table used to store cluster aware client information
// i.e the list of events for which each client has registered.
//
const char * const CAAPI_TABLE = "caapi_reg";

class caapi_reg_io {
public:
	// constructor for caapi_reg_io
	caapi_reg_io();
	// destructor for caapi_reg_io
	~caapi_reg_io();
	//
	// Get the ccr directory pointer. This method uses the interfaces
	// provided by ccr::directory in updating the caapi_reg table.
	//
	int initialize();

	//
	// update_clients method updates the caapi_reg ccr table. it reads
	// a list of client information, retrives the <key,data> for each
	// client and removes the previous contents of ccr table and writes the
	// new sequence of <key,data> to ccr table, so whenever update_clients
	// is called a new sequence of <key,data> is constructed and written
	// into ccr table.

	// NOTE update_clients in a single transaction remove's all the
	// previous contents of the caapi_reg ccr table and writes the in-memory
	// contents to table this is done because single update to caapi_reg
	// table is inefficient as compared to writing all at once.
	//
	int update_clients(SList<caapi_client> &client_list_in);

	//
	// read_clients reads caapi_reg ccr table and retrives a sequence of
	// <key,data> which is representation of details of each client
	// this information is stored in in-memory client_list of
	// caapi_reg_cache also this function updates the event_mapping stucture
	// this function is called during intializing caapi_reg_cache module
	//
	int read_clients(SList<caapi_client> &client_list,
			caapi_mapping &event_mapping);
private:
	// This function retreives the dir ptr for the caapi_reg ccr table
	bool lookup_dir();

	// disallowed - pass by value and assignment
	caapi_reg_io(const caapi_reg_io &);
	caapi_reg_io &operator = (const caapi_reg_io &);

	// directory ptr for updating and lookup of caapi_reg ccr table
	ccr::directory_var	ccrdir_v;


};

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#endif /* _CAAPI_REG_IO_H_ */
