/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma	ident	"@(#)sc_registration_instance.cc	1.16	08/05/20 SMI"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <libxml/xmlmemory.h>
#include <libxml/parser.h>
#include <libxml/parserInternals.h>
#include <libxml/xmlerror.h>
#include <libnvpair.h>
#include <errno.h>
#include <assert.h>
#include <sys/sc_syslog_msg.h>
#include <orb/fault/fault_injection.h>

#include "cl_apid_include.h"
#include "sc_registration_instance.h"
#include "sc_xml_event.h"
#include "caapi_client_handle.h"


/*
 * The following is declared in parserInternals.h
 *   extern int xmlDoValidityCheckingDefaultValue;
 * If we ever fail to #include it, we need to declare this var again.
 */

static bool copy_sc_event_helper(sc_event *in_evt, void *out_list)
{
	/*
	 * Copy the event.  If it doesn't copy correctly,
	 * we bial out and return false so the caller is made aware of
	 * the issues.
	 */
	sc_event *evt = new sc_event(*in_evt);
	if (evt == NULL || evt->get_construct_status() != 0) {
		return (false);
	}
	((SList<sc_event> *)out_list)->append(evt);
	return (true);
}


/* construct from xml, or from attributes */

sc_registration_instance::sc_registration_instance(const char *xml,
    const char *src_ip_in, int sock_in) : sc_xml_instance(xml)
{
	cb_info = NULL;
	src_ip = NULL;
	cli_handle = NULL;
	sock = sock_in;
	reg_res = 0;

	/* cstyle doesn't understand operator overloading */
	/*CSTYLED*/
	if ((event_list = new SList<sc_event>) == NULL) {
		xml_status = ENOMEM;
		return;
	}
	if (src_ip_in != NULL) {
		if ((src_ip = os::strdup(src_ip_in)) == NULL) {
			xml_status = ENOMEM;
			return;
		}
	}
	/*
	 * We know that we're making a call to a virtual function in
	 * a constructor, and that subclasses will not get the correct
	 * version.  That's fine.  Suppress the lint warning.
	 */
	parse_xml(); /*lint !e1506 */
}

/* makes its own copies of params */
sc_registration_instance::sc_registration_instance(
    const sc_callback_info &cb_in, SList<sc_event> &list, reg_type reg_in) :
    sc_xml_instance()
{
	src_ip = NULL;
	reg = reg_in;
	sock = -1;
	cli_handle = NULL;
	reg_res = 0;
	event_list = NULL;

	if ((cb_info = new sc_callback_info(cb_in)) == NULL) {
		xml_status = ENOMEM;
		return;
	}

	/*
	 * allocate new memory, and make copies of all the
	 * list elements
	 */
	/* cstyle doesn't understand operator overloading */
	/*CSTYLED*/
	if ((event_list = new SList<sc_event>) == NULL) {
		xml_status = ENOMEM;
		return;
	}
	/*
	 * if the iteration returns non-NULL, it means we stopped part way,
	 * and thus did not construct the registration correctly.
	 * We set the xml_status to the only error that could have
	 * occured, and error out of the constructor.
	 */
	if (list.iterate(copy_sc_event_helper, (void *)event_list) != NULL) {
		xml_status = ENOMEM;
		return;
	}
	/*
	 * We know that we're making a call to a virtual function in
	 * a constructor, and that subclasses will not get the correct
	 * version.  That's fine.  Suppress the lint warning.
	 */
	create_xml(); /*lint !e1506 */
}

sc_registration_instance::~sc_registration_instance()
{
	delete cb_info;
	delete [](src_ip);

	sc_event *evt;
	if (event_list != NULL) {
		while ((evt = event_list->reapfirst()) != NULL) {
			delete(evt);
		}
		delete event_list;
	}

	/*
	 * Delete the client handle
	 */
	if (cli_handle != NULL) {
		delete cli_handle;
	}
}


const sc_callback_info &sc_registration_instance::get_callback_info() const
{
	/* check for NULL */
	assert(cb_info);
	return (*cb_info);
}

const SList<sc_event> &sc_registration_instance::get_events() const
{
	return (*event_list);
}

reg_type sc_registration_instance::get_reg_type() const
{
	return (reg);
}

int sc_registration_instance::get_sock() const
{
	return (sock);
}

static bool event_print_helper(sc_event *event, void *)
{
	event->print();
	return (true);
}

void sc_registration_instance::print() const
{
	char *reg_str = NULL;
	switch (reg) {
	case ADD_CLIENT:
		reg_str = "ADD_CLIENT";
		break;
	case ADD_EVENTS:
		reg_str = "ADD_EVENT";
		break;
	case REMOVE_CLIENT:
		reg_str = "REMOVE_CLIENT";
		break;
	case REMOVE_EVENTS:
		reg_str = "REMOVE_EVENTS";
		break;
	default:
		reg_str = "UNKNOWN";
		break;
	}
	(void) fprintf(stderr, "src_ip: %s  reg_type: %s\n",
	    src_ip ? src_ip : "", reg_str);
	if (cb_info != NULL)
		cb_info->print();
	(void) event_list->iterate(event_print_helper, NULL);
}

void sc_registration_instance::create_xml()
{
	string_buffer str_buf;

	if (!parse_code) {
		/* error */
		xml_status = -1;
		return;
	}
	if (xml_str != NULL) {
		/* we already created it */
		return;
	}

	/* create it here */
	if ((write_registration_instance(str_buf)) == -1) {
		xml_status = -1;
		return;
	}
	/* If we successfully created it, allocate new memory for it */
	if ((xml_str = os::strdup(str_buf.get_buf())) == NULL) {
		xml_status = ENOMEM;
	}
}

/*
 * write_registration_instance
 * ----------------------------
 * Returns the number of bytes written to str_buf, or -1 on error.
 */
int sc_registration_instance::write_registration_instance(
    string_buffer &str_buf)
{
	int total, inc;

	/* cstyle seems to think the // in quotes is a start comment */
	total = str_buf.append("<?xml version=\"1.0\" standalone=\"no\"?>"
	    "<!DOCTYPE SC_CALLBACK_REG PUBLIC "
	    /*CSTYLED*/
	    "\"-//SUNW//sc_callback_reg//EN\" "
	    "\"sc_callback_reg.dtd\">");
	if (total == -1) {
		xml_status = -1;
		return (-1);
	}
	if ((inc = str_buf.append("<SC_CALLBACK_REG>")) == -1) {
		xml_status = -1;
		return (-1);
	}
	total += inc;
	if ((inc = write_callback_info(str_buf)) == -1) {
		xml_status = -1;
		return (-1);
	}
	total += inc;
	if ((inc = write_events(str_buf)) == -1) {
		xml_status = -1;
		return (-1);
	}
	total += inc;
	if ((inc = str_buf.append("</SC_CALLBACK_REG>")) == -1) {
		xml_status = -1;
		return (-1);
	}
	total += inc;
	return (total);
}

int sc_registration_instance::write_callback_info(string_buffer &str_buf)
{
	int total = 0;

	assert(cb_info != NULL);

	total =  str_buf.append("<SC_CALLBACK_INFO IP=\"%s\" PORT=\"%d\">"
	    "</SC_CALLBACK_INFO>", cb_info->get_cb_ip(),
	    cb_info->get_cb_port());
	return (total);
}

int sc_registration_instance::write_events(string_buffer &str_buf)
{
	int total = 0;
	int inc;

	switch (reg) {
	case ADD_CLIENT:
		if ((total = str_buf.append("<ADD_CLIENT>")) == -1) {
			xml_status = -1;
			return (-1);
		}
		if ((inc = write_event_list(str_buf)) == -1) {
			xml_status = -1;
			return (-1);
		}
		total += inc;
		if ((inc = str_buf.append("</ADD_CLIENT>")) == -1) {
			xml_status = -1;
			return (-1);
		}
		total += inc;
		break;
	case ADD_EVENTS:
		if ((total = str_buf.append("<ADD_EVENTS>")) == -1) {
			xml_status = -1;
			return (-1);
		}
		if ((inc = write_event_list(str_buf)) == -1) {
			xml_status = -1;
			return (-1);
		}
		total += inc;
		if ((inc = str_buf.append("</ADD_EVENTS>")) == -1) {
			xml_status = -1;
			return (-1);
		}
		total += inc;
		break;
	case REMOVE_CLIENT:
		if ((total = str_buf.append("<REMOVE_CLIENT>")) == -1) {
			xml_status = -1;
			return (-1);
		}
		if ((inc = write_event_list(str_buf)) == -1) {
			xml_status = -1;
			return (-1);
		}
		total += inc;
		if ((inc = str_buf.append("</REMOVE_CLIENT>")) == -1) {
			xml_status = -1;
			return (-1);
		}
		total += inc;
		break;
	case REMOVE_EVENTS:
		if ((total = str_buf.append("<REMOVE_EVENTS>")) == -1) {
			xml_status = -1;
			return (-1);
		}
		if ((inc = write_event_list(str_buf)) == -1) {
			xml_status = -1;
			return (-1);
		}
		total += inc;
		if ((inc = str_buf.append("</REMOVE_EVENTS>")) == -1) {
			xml_status = -1;
			return (-1);
		}
		total += inc;
		break;
	}
	return (total);
}

/*
 * write_event_list
 * ------------------
 * Returns the number of bytes written to str_buf, or -1 on error.
 */
int sc_registration_instance::write_event_list(string_buffer &str_buf)
{
	const char *subclass;
	sc_event *event;

	int total = 0;
	int inc = 0;

	SList<sc_event>::ListIterator iter(event_list);
	for (; (event = (sc_event *)(iter.get_current())) != NULL;
	    iter.advance()) {
		if ((inc = str_buf.append("<SC_EVENT_REG CLASS=\"%s\">",
		    event->get_class())) == -1) {
			xml_status = -1;
			return (-1);
		}
		total += inc;
		if ((subclass = event->get_subclass()) != NULL) {
			if ((inc = str_buf.append(
			    "<SC_EVENT_SUBCLASS SUBCLASS=\"%s\">",
			    subclass)) == -1) {
				xml_status = -1;
				return (-1);
			}
			total += inc;
			// call static method
			if ((inc = sc_xml_event::write_attr_list(
			    str_buf, event->get_nvlist())) == -1) {
				xml_status = -1;
				return (-1);
			}
			total += inc;
			if ((inc = str_buf.append("</SC_EVENT_SUBCLASS>")) ==
			    -1) {
				xml_status = -1;
				return (-1);
			}
			total += inc;
		}
		if ((inc = str_buf.append("</SC_EVENT_REG>")) == -1) {
			xml_status = -1;
			return (-1);
		}
		total += inc;
	}
	return (total);
}

/*
 * parse_xml
 * ------------
 * The meat of this class, this function parses an xml string and
 * saves the registration in easily accessible variables.
 */
void sc_registration_instance::parse_xml()
{
	xmlDocPtr doc;
	xmlNodePtr cur;
	int wellFormed;
	xmlParserCtxtPtr ctxt;
	char *port_str, *reg_str;
	int port;
	sc_syslog_msg_handle_t handle;

#ifdef _FAULT_INJECTION
	void		*f_argp;
	uint32_t	f_argsize;
	if (fault_triggered(FAULTNUM_CLAPI_REG_PARSE_XML_R,
		&f_argp, &f_argsize)) {
		ASSERT(f_argsize == sizeof (int));
		xml_status = *((int *)f_argp);
		return;
	}
#endif
	if (xml_str == NULL || xml_str[0] == '\0') {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The cl_apid was unable to parse the specified xml message
		// for the specified reason. Unless the reason is "low
		// memory", this message probably represents a CRNP client
		// error.
		// @user_action
		// If the specified reason is "low memory", increase swap
		// space, install more memory, or reduce peak memory
		// consumption. Otherwise, no action is needed.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Failed to parse xml for %s: %s", "sc_callback_reg",
		    "NULL xml");
		sc_syslog_msg_done(&handle);
		return;
	}
	if (src_ip == NULL) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Failed to parse xml for %s: %s", "sc_callback_reg",
		    "NULL src ip");
		sc_syslog_msg_done(&handle);
		return;
	}


	(void) xmlKeepBlanksDefault(0);
	ctxt = (xmlParserCtxtPtr)xmlCreateMemoryParserCtxt
	    (xml_str, (int)strlen(xml_str));
	if (ctxt == NULL) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_INFO, MESSAGE,
		    "Failed to parse xml for %s: %s", "sc_callback_reg",
		    "low memory");
		sc_syslog_msg_done(&handle);
		xml_status = ENOMEM;
		return;
	}

	ctxt->sax->error = xmlSCErrorFunc;
	ctxt->sax->warning = xmlSCErrorFunc;
	ctxt->sax->fatalError = xmlSCErrorFunc;

	/*
	 * Redirect errors to our debug function.
	 * Looks like we need to do this each time.
	 */
	xmlSetGenericErrorFunc((void *)NULL,
	    (xmlGenericErrorFunc)xmlSCErrorFunc);

	/* parse the document */
	if (xmlParseDocument(ctxt) != 0) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_INFO, MESSAGE,
		    "Failed to parse xml for %s: %s", "sc_callback_reg",
		    "mal-formed");
		sc_syslog_msg_done(&handle);
		xmlFreeParserCtxt(ctxt);
		parse_code = MAL_FORMED;
		return;
	}

	doc = ctxt->myDoc;
	wellFormed = ctxt->wellFormed;

	if (doc == NULL || !wellFormed) {
		scds_syslog_debug(1, "sc_callback_reg is mal-formed\n");
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_INFO, MESSAGE,
		    "Failed to parse xml for %s: %s", "sc_callback_reg",
		    "mal-formed");
		sc_syslog_msg_done(&handle);
		xmlFreeParserCtxt(ctxt);
		parse_code = MAL_FORMED;
		return;
	}

	scds_syslog_debug(1, "xml is well-formed\n");

	/*
	 * Redirect errors to our debug function.
	 * Looks like we need to do this each time.
	 */
	xmlSetGenericErrorFunc((void *)NULL,
	    (xmlGenericErrorFunc)xmlSCErrorFunc);

	/*
	 * Now check validity.  First, load the DTD.
	 * We hard-code the name of the DTD here...
	 */
	xmlDtdPtr theDtd = xmlParseDTD(NULL,
	    (const xmlChar *)"sc_callback_reg.dtd");

	/*
	 * Redirect errors to our debug function.
	 * Looks like we need to do this each time.
	 */
	xmlSetGenericErrorFunc((void *)NULL,
	    (xmlGenericErrorFunc)xmlSCErrorFunc);

	/* make sure we can load the nvpair.dtd as well */
	xmlDtdPtr nvpairDtd = xmlParseDTD(NULL,
	    (const xmlChar *)"nvpair.dtd");
	if (theDtd == NULL || nvpairDtd == NULL) {
		scds_syslog_debug(1,
			"Unable to load dtd: skipping validation\n");
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The cl_apid was unable to load the specified dtd. No
		// validation will be performed on CRNP xml messages.
		// @user_action
		// No action is required. If you want to diagnose the problem,
		// examine other syslog messages occurring at about the same
		// time to see if the problem can be identified. Save a copy
		// of the /var/adm/messages files on all nodes and contact
		// your authorized Sun service provider for assistance in
		// diagnosing and correcting the problem.
		//
		(void) sc_syslog_msg_log(handle, LOG_INFO, MESSAGE,
		    "Error loading dtd for %s", "sc_callback_reg");
		sc_syslog_msg_done(&handle);
	} else {
		/*
		 * Now try to do the actual validation
		 */
		xmlValidCtxt cvp;
		/*
		 * Set up the error message callbacks.  Again, some
		 * documentation would be nice...
		 */
		cvp.userData = (void *)NULL;
		cvp.error = (xmlValidityErrorFunc) xmlSCErrorFunc;
		cvp.warning = (xmlValidityWarningFunc) xmlSCErrorFunc;
		if (!xmlValidateDtd(&cvp, doc, theDtd)) {
			scds_syslog_debug(1, "sc_callback_reg is invalid\n");
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
			(void) sc_syslog_msg_log(handle, LOG_INFO, MESSAGE,
			    "Failed to parse xml for %s: %s",
			    "sc_callback_reg", "invalid");
			sc_syslog_msg_done(&handle);
			xmlFreeParserCtxt(ctxt);
			xmlFreeDoc(doc);
			xmlFreeDtd(theDtd);
			xmlFreeDtd(nvpairDtd);
			parse_code = INVALID;
			return;
		}
		scds_syslog_debug(1, "xml is valid\n");
	}

	/* we can free this now; we don't need it anymore */
	xmlFreeDtd(theDtd);
	xmlFreeDtd(nvpairDtd);

	/* do we need to free the xmlValidCtxt??? */

	/*
	 * Check the document is of the right kind
	 */
	cur = xmlDocGetRootElement(doc);
	if (cur == NULL) {
		scds_syslog_debug(1,
			"Failed to parse xml: invalid element NULL\n");
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The cl_apid was unable to parse an xml message because of
		// an invalid attribute. This message probably represents a
		// CRNP client error.
		// @user_action
		// No action needed.
		//
		(void) sc_syslog_msg_log(handle, LOG_INFO, MESSAGE,
		    "Failed to parse xml: invalid element %s", "NULL");
		sc_syslog_msg_done(&handle);
		xmlFreeParserCtxt(ctxt);
		xmlFreeDoc(doc);
		parse_code = MAL_FORMED;
		return;
	}
	if (strcasecmp((const char *)cur->name, "SC_CALLBACK_REG")) {
		scds_syslog_debug(1,
			"Failed to parse xml: invalid element %s\n",
		    cur->name);
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_INFO, MESSAGE,
		    "Failed to parse xml: invalid element %s", cur->name);
		sc_syslog_msg_done(&handle);
		xmlFreeParserCtxt(ctxt);
		xmlFreeDoc(doc);
		parse_code = MAL_FORMED;
		return;
	}

	/*
	 * At this point we consider it parsed.
	 * If there's an error, it will be set to something else
	 */
	parse_code = PARSED;

	/*
	 * Read out the port from the message.
	 */
	port_str = (char *)xmlGetProp(cur, (const unsigned char *)"PORT");
	if (port_str == NULL) {
		scds_syslog_debug(1, "Failed to parse xml: NULL attribute\n");
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_INFO, MESSAGE,
		    "Failed to parse xml for %s: %s", "sc_callback_reg",
		    "NULL attribute");
		sc_syslog_msg_done(&handle);
		xmlFreeParserCtxt(ctxt);
		xmlFreeDoc(doc);
		parse_code = MAL_FORMED;
		return;
	}

	port = atoi(port_str);

	/*
	 * There's no memory leak here.  It's just that lint can't
	 * see where we free it (in the destructor).
	 */
	cb_info = new sc_callback_info(src_ip, port); /*lint !e423 */

	// Need to use C free here, because libxml2 allocated it using
	// malloc.
	free(port_str);

	if (cb_info == NULL || cb_info->get_construct_status() != 0) {
		scds_syslog_debug(1, "Failed to parse xml: low memory\n");
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_INFO, MESSAGE,
		    "Failed to parse xml for %s: %s", "sc_callback_reg",
		    "low memory");
		sc_syslog_msg_done(&handle);
		xmlFreeParserCtxt(ctxt);
		xmlFreeDoc(doc);
		parse_code = MAL_FORMED;
		xml_status = ENOMEM;
		return;
	}

	/*
	 * The next attribute is reg_type (either ADD_CLIENT, ADD_EVENTS,
	 * REMOVE_CLIENT, REMOVE_EVENTS)
	 */
	reg_str = (char *)xmlGetProp(cur, (const unsigned char *)"REG_TYPE");
	if (reg_str == NULL) {
		scds_syslog_debug(1,
			"Failed to parse xml: NULL attribute\n");
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_INFO, MESSAGE,
		    "Failed to parse xml for %s: %s", "sc_callback_reg",
		    "NULL attribute");
		sc_syslog_msg_done(&handle);
		xmlFreeParserCtxt(ctxt);
		xmlFreeDoc(doc);
		parse_code = MAL_FORMED;
		return;
	}
	if (strcasecmp((const char *)reg_str, "ADD_CLIENT") == 0) {
		reg = ADD_CLIENT;
		parse_event_list(doc, cur->xmlChildrenNode);
	} else if (strcasecmp((const char *)reg_str, "ADD_EVENTS") == 0) {
		reg = ADD_EVENTS;
		parse_event_list(doc, cur->xmlChildrenNode);
	} else if (strcasecmp((const char *)reg_str, "REMOVE_CLIENT") == 0) {
		reg = REMOVE_CLIENT;
		parse_event_list(doc, cur->xmlChildrenNode);
	} else if (strcasecmp((const char *)reg_str, "REMOVE_EVENTS") == 0) {
		reg = REMOVE_EVENTS;
		parse_event_list(doc, cur->xmlChildrenNode);
	} else {
		scds_syslog_debug(1,
			"Failed to parse xml: invalid reg_type %s\n",
			reg_str);
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The cl_apid was unable to parse an xml message because of
		// an invalid registration type.
		// @user_action
		// No action needed.
		//
		(void) sc_syslog_msg_log(handle, LOG_INFO, MESSAGE,
		    "Failed to parse xml: invalid reg_type [%s]", reg_str);
		sc_syslog_msg_done(&handle);
		xmlFreeParserCtxt(ctxt);
		xmlFreeDoc(doc);
		free(reg_str);
		parse_code = MAL_FORMED;
		return;
	}
	free(reg_str);
	xmlFreeParserCtxt(ctxt);
	xmlFreeDoc(doc);

	if (parse_code != PARSED || xml_status != 0) {
		return;
	}

	/*
	 * Check that we have the correct number of events.
	 * For ADD_CLIENT, ADD_EVENTS, and REMOVE_EVENTS, we need at
	 * least 1 event.  For REMOVE_EVENTS, we are required to have 0.
	 */
	switch (reg) {
	case ADD_CLIENT:
	case ADD_EVENTS:
	case REMOVE_EVENTS:
		if (event_list->count() < 1) {
			scds_syslog_debug(1,
			    "Failed to parse xml: no events\n");
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
			(void) sc_syslog_msg_log(handle, LOG_INFO, MESSAGE,
			    "Failed to parse xml for %s: %s",
			    "sc_callback_reg", "no events");
			sc_syslog_msg_done(&handle);

			parse_code = INVALID;
			return;
		}
		break;
	case REMOVE_CLIENT:
		if (event_list->count() != 0) {
			scds_syslog_debug(1,
			    "Failed to parse xml for %s: %s",
			    "sc_callback_reg", "too many events");
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
			(void) sc_syslog_msg_log(handle, LOG_INFO, MESSAGE,
			    "Failed to parse xml for %s: %s",
			    "sc_callback_reg", "too many events");
			sc_syslog_msg_done(&handle);

			parse_code = INVALID;
			return;
		}
		break;
	}
}


void sc_registration_instance::parse_event_list(xmlDocPtr doc,
    xmlNodePtr cur)
{
	char *evt_class = NULL, *evt_subclass = NULL;
	SList<nvstrpair> nvlist;
	sc_event *new_evt;
	sc_syslog_msg_handle_t handle;

	/* loop through all the events */
	for (; cur; cur = cur->next) {
		if (strcasecmp((const char *)cur->name, "SC_EVENT_REG")) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
			(void) sc_syslog_msg_log(handle, LOG_INFO, MESSAGE,
			    "Failed to parse xml: invalid element %s",
			    cur->name);
			sc_syslog_msg_done(&handle);
			parse_code = MAL_FORMED;
			return;
		}

		evt_class = (char *)xmlGetProp(cur,
		    (const unsigned char *)"CLASS");
		if (evt_class == NULL) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
			(void) sc_syslog_msg_log(handle, LOG_INFO, MESSAGE,
			    "Failed to parse xml for %s: %s",
			    "sc_callback_reg", "NULL attribute");
			sc_syslog_msg_done(&handle);
			parse_code = MAL_FORMED;
			return;
		}

		/*
		 * Now get the subclass.
		 * It can be NULL - then we don't try to get the
		 * nvlist.
		 */
		evt_subclass = (char *)xmlGetProp(cur,
		    (const unsigned char *)"SUBCLASS");

		/* parse the nvlist no matter what */
		parse_code = sc_xml_event::parse_nvlist(doc,
		    cur->xmlChildrenNode, nvlist, xml_status);

		if (parse_code == MAL_FORMED || xml_status != 0) {
			nvlist.dispose();
			return;
		}

		/* Make sure that we don't have nvpairs without a subclass */
		if (evt_subclass == NULL && nvlist.count() > 0) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
			(void) sc_syslog_msg_log(handle, LOG_INFO, MESSAGE,
			    "Failed to parse xml for %s: %s",
			    "sc_callback_reg", "nvpairs without subclass");
			sc_syslog_msg_done(&handle);
			parse_code = INVALID;
			nvlist.dispose();
			return;
		}

		// create the event
		if ((new_evt = new sc_event(evt_class, evt_subclass, nvlist))
		    == NULL) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
			(void) sc_syslog_msg_log(handle, LOG_INFO, MESSAGE,
			    "Failed to parse xml for %s: %s",
			    "sc_callback_reg", "low memory");
			sc_syslog_msg_done(&handle);
			parse_code = MAL_FORMED;
			xml_status = ENOMEM;
			nvlist.dispose();
			return;
		}
		/*
		 * Need to use free instead of delete[]
		 * because libxml2 uses malloc.
		 */
		free(evt_subclass);
		free(evt_class);
		evt_subclass = NULL;
		evt_class = NULL;
		nvstrpair *pair;
		while ((pair = nvlist.reapfirst()) != NULL) {
			delete pair;
		}
		/* actually add the new event to our list */
		event_list->append(new_evt);
	}
}


/*
 * is_complete_message
 * --------------------
 * To determine whether it's a complete message, we first try to construct
 * the actual sc_registration_instance.  There are three possibilities
 * for the parse_result of parsing the message at this point.  If the message
 * is complete and correct, the parse_result will be PARSED.  If the message
 * is incorrect in some way (complete or not) it will be INVALID.  If the
 * message is not yet complete, it will be MAL_FORMED.  We want to return true,
 * specifying that it's a complete message, if it's either PARSED or INVALID.
 * The reason we return true if the message is INVALID is because with that
 * information, we know enough to send a response to the client specifying
 * that the message is invalid.  Also, once the message is invalid, we will
 * never get a PARSED result from trying to parse it in the future.
 *
 * Note that returning true from this method implies only that the
 * message is "complete", not that it is correct.  The code in the
 * reg_comm_module that calls this method will check the parse_result again
 * and return the appropriate error message to the client if needed.
 *
 * Note also that, even if the parse_result is MAL_FORMED, we still
 * could have the complete message, but the sender screwed up in some major
 * way.  In that case, look for the ending tag from the xml message.  Note
 * that this method itself is not failsafe either.  In the worse case, the
 * reg_comm_module will have to time out the client connection.
 */
bool sc_registration_instance::is_complete_message(const char *str,
    const char *ip, sc_registration_instance **new_reg)
{
	*new_reg = new sc_registration_instance(str, ip);
	if (*new_reg != NULL && (*new_reg)->get_construct_status() == 0 &&
	    ((*new_reg)->get_parse_result() == PARSED ||
	    (*new_reg)->get_parse_result() == INVALID)) {
		//
		// It's complete and well-formed (but might be invalid)
		// If invalid, we will catch that later.
		//
		return (true);
	} else if (strstr(str, "</SC_CALLBACK_REG>") != NULL ||
	    strstr(str, "</sc_callback_reg>") != NULL) {
		// it's complete, but invalid or mal-formed
		return (true);
	}
	// it's not complete; keep waiting
	delete *new_reg;
	*new_reg = NULL;
	return (false);
}
