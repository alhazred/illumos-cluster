/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */
#pragma ident	"@(#)caapi_reg_io.cc 1.17	08/05/20 SMI"

// XX need to add more comments, check for lint and cstyle

#include "caapi_reg_io.h"
#include "cl_apid_error.h"
#include <orb/infrastructure/orb.h>
#include <orb/fault/fault_injection.h>
#include <sys/sc_syslog_msg.h>

//
// class caapi_reg_io is the mediator class for interfacing with ccr table
// caapi_reg.
// This class has two prime public methods
// 1. update_clients: This method is responsible for updating the caapi_reg ccr
// table with the updated set of client details.
// 2. read_clients : This method reads the caapi_reg ccr table and updated the
// the client_list of caapi_reg_cache module, it also updates the event_mapping
// structure which maintains details of clients and events associated with it
// to make lookup of a event faster.
// this class is invoked only by caapi_reg_cache instance
//
// caapi_reg is the ccr table used to store cluster aware client information
// i.e the list of events each client has registered.
//
// Constructor of caapi_reg_io
//
caapi_reg_io::caapi_reg_io():
		ccrdir_v(ccr::directory::_nil())
{

}

//
// Destructor of caapi_reg_io
//
caapi_reg_io::~caapi_reg_io()
{
}

//
// update_clients method updates the caapi_reg ccr table. it reads
// a list of client information, retrives the <key,data> for each
// client and removes the previous contents of ccr table and writes the
// new sequence of <key,data> to ccr table, so whenever update_clients
// is called a new sequence of <key,data> is contructed and written
// into ccr table
//
int
caapi_reg_io::update_clients(SList<caapi_client> &client_list_in)
{
	Environment env;
	int ret_value = 0;
	ccr::updatable_table_var	update_v;
	ccr::element_seq *seq = NULL;
	caapi_client	*clientp = NULL;
	uint_t len = 0;
	char *client_key = NULL;
	char *client_data = NULL;
	sc_syslog_msg_handle_t handle;

	FAULTPT_CLAPI(FAULTNUM_CLAPI_WRITE_TO_CCR_B,
		FaultFunctions::generic);

#ifdef _FAULT_INJECTION
	void		*f_argp;
	uint32_t	f_argsize;
	if (fault_triggered(FAULTNUM_CLAPI_WRITE_TO_CCR_R,
		&f_argp, &f_argsize)) {
		ASSERT(f_argsize == sizeof (int));
		return (*((int *)f_argp));
	}
#endif
	//
	// create a sequence of key-data for all clients in the
	// client-list
	//
	seq = new ccr::element_seq(client_list_in.count());
	if (seq == NULL) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Error: low memory");
		sc_syslog_msg_done(&handle);

		return (ENOMEM);
	}
	client_list_in.atfirst();
	while ((clientp = client_list_in.get_current()) != NULL) {

		client_list_in.advance();
		//
		// get key-data for each client and this key-data of the client
		// is stored in a sequence which would be written to caapi_reg
		// table.
		// NOTE get_key_data allocates memory for key and data
		// parameters so this function needs to delete this memory
		// explicitly. delete seq will take care of deletion
		// of key and data
		//
		ret_value = clientp->get_key_data(client_key, client_data);

		//
		// client key is set to NULL when client unregisters and thus
		// it is not written tho caapi_reg table
		//
		if (client_key == NULL)
			continue;

		if (ret_value != 0) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
			//
			// SCMSGS
			// @explanation
			// The cl_apid experienced in internal error that
			// prevented it from modifying the client
			// registrations as requested.
			// @user_action
			// Examine other syslog messages occurring at about
			// the same time to see if the problem can be
			// identified. Save a copy of the /var/adm/messages
			// files on all nodes and contact your authorized Sun
			// service provider for assistance in diagnosing and
			// correcting the problem.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "cl_apid internal error: unable to update client "
			    "registrations.");
			sc_syslog_msg_done(&handle);

			// deallocate memory allocated to sequence
			delete seq;

			return (ret_value);
		}
		(*seq)[len].key = client_key;
		(*seq)[len].data = client_data;
		len++;
	}

	// set the length of the sequence
	seq->length(len);

	// Start an update transaction on the table.
	ASSERT(!CORBA::is_nil(ccrdir_v));

	update_v = ccrdir_v->begin_transaction(CAAPI_TABLE, env);
	if (env.exception() || CORBA::is_nil(update_v)) {
		env.exception()->print_exception("failed to update caapi_reg ");
		//
		// return failed to get update access to caapi table
		// reason maybe table doesnt exist or out of service
		//
		env.clear();
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The cl_apid experienced an error with the CCR table that
		// prevented it from modifying the client registrations as
		// requested.
		// @user_action
		// Examine other syslog messages occurring at about the same
		// time to see if the problem can be identified. Save a copy
		// of the /var/adm/messages files on all nodes and contact
		// your authorized Sun service provider for assistance in
		// diagnosing and correcting the problem.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Error modifying CRNP CCR table: unable to update client "
		    "registrations.");
		sc_syslog_msg_done(&handle);
		ret_value = CLEP_ERR_CCR;
		goto ABORT_UPDATE;
	}

	//
	// remove all elements from ccr table and write the new sequence to ccr
	// table which consists of the new registration information of the
	// client
	//
	update_v->remove_all_elements(env);
	if (env.exception() != NULL) {
		//
		// if failed to remove all elements from ccr table
		// abort transaction and return
		//
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Error modifying CRNP CCR table: unable to update client "
		    "registrations.");
		sc_syslog_msg_done(&handle);

		env.clear();
		// return failed during remove elements
		ret_value = CLEP_ERR_CCR;
		goto ABORT_UPDATE;
	}

	//
	// add the new sequence of elements created from the client_list
	// to caapi_reg ccr table
	//
	update_v->add_elements(*seq, env);
	if (env.exception() != NULL) {
		env.clear();
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Error modifying CRNP CCR table: unable to update client "
		    "registrations.");
		sc_syslog_msg_done(&handle);

		// return failed during remove elements
		ret_value = CLEP_ERR_CCR;
		goto ABORT_UPDATE;
	}

	// commit transaction
	update_v->commit_transaction(env);
	if (env.exception()) {
		env.clear();
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Error modifying CRNP CCR table: unable to update client "
		    "registrations.");
		sc_syslog_msg_done(&handle);
		ret_value = CLEP_ERR_CCR;
	}

	// deallocate memory allocated to sequence
	delete seq;

	return (ret_value);

ABORT_UPDATE:

	// deallocate memory allocated to sequence
	delete seq;

	if (update_v) {
		update_v->abort_transaction(env);
		if (env.exception())
			// exception ignored
			env.clear();
	}
	return (ret_value);
}

//
// lookup_dir functions queries the name server and
// retrieves a ccr::directory pointer
//
bool
caapi_reg_io::lookup_dir()
{
	Environment env;
	ccrdir_v = ccr::directory::_nil();
	naming::naming_context_var ctxp = ns::local_nameserver();
	if (CORBA::is_nil(ctxp)) {
		os::warning("can't find local name server");
		return (false);
	}
	CORBA::Object_var obj = ctxp->resolve("ccr_directory", env);
	if (env.exception()) {
		os::warning("\n ccr_directory not found in name server");
		env.clear();
		return (false);
	}
	ccrdir_v = ccr::directory::_narrow(obj);
	return (true);
}

//
// initialize function queries the  name server and gets
// ccr::directory pointer, then it creates caapi_reg table, if the table exists
// exception is ignored
//
int
caapi_reg_io::initialize()
{
	Environment env;
	CORBA::Exception *ex = NULL;
	sc_syslog_msg_handle_t handle;

	if (ORB::initialize() != 0) {
		// ORB init failed so cant proceed
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The cl_apid or cl_eventd was unable to initialize the ORB
		// during start-up. This error will prevent the daemon from
		// starting.
		// @user_action
		// Examine other syslog messages occurring at about the same
		// time to see if the problem can be identified. Save a copy
		// of the /var/adm/messages files on all nodes and contact
		// your authorized Sun service provider for assistance in
		// diagnosing and correcting the problem.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Error: unable to initialize ORB.");
		sc_syslog_msg_done(&handle);

		return (CLEP_ERR_ORB);
	}

	if (!lookup_dir()) {
		// cant initialize ccr dir can't proceed
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The cl_apid was unable to initialize the CCR during
		// start-up. This error will prevent the cl_apid from
		// starting.
		// @user_action
		// Examine other syslog messages occurring at about the same
		// time to see if the problem can be identified. Save a copy
		// of the /var/adm/messages files on all nodes and contact
		// your authorized Sun service provider for assistance in
		// diagnosing and correcting the problem.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Error: unable to initialize CCR.");
		sc_syslog_msg_done(&handle);
		return (CLEP_ERR_ORB);
	}

	//
	// create caapi_reg_table, if table exists exception is raised
	// it is ignored
	//
	ccrdir_v->create_table(CAAPI_TABLE, env);
	if ((ex = env.exception()) != NULL) {
		if (ccr::table_exists::_exnarrow(ex)) {
			// table exists exception is OK
			env.clear();
		} else {
			// return create table failed
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "Error: unable to initialize CCR.");
			sc_syslog_msg_done(&handle);

			env.clear();
			return (CLEP_ERR_CCR);
		}
	}
	return (0);
}

//
// read_clients reads caapi_reg ccr table and retrives a sequence of
// <key,data> which is represenation of details of each client
// this information is stored in in-memory client_list of
// caapi_reg_cache also this function updates the event_mapping stucture
// this function is called during intializing caapi_reg_cache module
//
int
caapi_reg_io::read_clients(SList<caapi_client> &client_list,
			    caapi_mapping &event_mapping)
{
	FAULTPT_CLAPI(FAULTNUM_CLAPI_READ_FROM_CCR_B,
		FaultFunctions::generic);

#ifdef _FAULT_INJECTION
	void		*f_argp;
	uint32_t	f_argsize;

	if (fault_triggered(FAULTNUM_CLAPI_READ_FROM_CCR_R,
		&f_argp, &f_argsize)) {
		ASSERT(f_argsize == sizeof (int));
		return (*((int *)f_argp));
	}
#endif
	Environment env;
	int32_t total_records = 0;
	uint32_t actual_records = 0;
	ccr::readonly_table_var read_v;
	ccr::element_seq_var elems_v;
	caapi_client *clientp = NULL;
	int ret_value = 0;
	bool allocation_status;
	sc_syslog_msg_handle_t handle;

	ASSERT(!CORBA::is_nil(ccrdir_v));

	read_v = ccrdir_v->lookup(CAAPI_TABLE, env);
	if (env.exception()) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The cl_apid was unable to read the specified CCR table.
		// This error will prevent the cl_apid from starting.
		// @user_action
		// Examine other syslog messages occurring at about the same
		// time to see if the problem can be identified. Save a copy
		// of the /var/adm/messages files on all nodes and contact
		// your authorized Sun service provider for assistance in
		// diagnosing and correcting the problem.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Error reading caapi_reg CCR table");
		sc_syslog_msg_done(&handle);

		env.clear();
		// return read access to table denied error
		return (CLEP_ERR_CCR);
	}

	total_records = read_v->get_num_elements(env);
	//
	// elems_v is only populated if the number of records
	// retrieved greater than 0
	//
	if (total_records > 0) {
		read_v->atfirst(env);
		if (env.exception()) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "Error reading caapi_reg CCR table");
			sc_syslog_msg_done(&handle);

			env.clear();
			// return failed to read caapi_reg table
			return (CLEP_ERR_CCR);
		}
		read_v->next_n_elements((uint32_t)total_records, elems_v, env);

		if (env.exception()) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "Error reading caapi_reg CCR table");
			sc_syslog_msg_done(&handle);

			env.clear();

			// return failed to get all records from the table
			return (CLEP_ERR_CCR);
		}

		actual_records = elems_v->length();

		ASSERT(actual_records == (uint32_t)total_records);

		// each loop will initialize a new client
		for (uint_t i = 0; i < actual_records; i++) {

			// create new client
			clientp = new caapi_client(elems_v[i].key,
			    elems_v[i].data);

			if ((clientp == NULL) ||
			    (clientp->get_construct_status() != 0)) {

				(void) sc_syslog_msg_initialize(&handle,
				    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE, "Error: low memory");
				sc_syslog_msg_done(&handle);

				//
				// delete the allocated  client's from
				// the client_list
				//
				client_list.dispose();
				// deallocate memory in event_mapping
				ret_value = event_mapping.dispose();

				return (ENOMEM);
			}
			// set the client back-ptr in all events
			clientp->set_client_backptr();

			// for each client update the event mapping
			ret_value =
			    event_mapping.update_mapping(ADD_CLIENT, clientp);

			if (ret_value != 0) {

				// deallocate memory in event_mapping
				ret_value = event_mapping.dispose();
				//
				// delete the allocated  client's from
				// the client_list
				//
				client_list.dispose();

				(void) sc_syslog_msg_initialize(&handle,
				    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE, "Error: low memory");
				sc_syslog_msg_done(&handle);

				//
				// return failed to update mapping probably
				// due to ENOMEM
				//
				return (ENOMEM);
			}
			client_list.append(clientp, &allocation_status);
			if (!allocation_status) {

				// deallocate memory in event_mapping
				ret_value = event_mapping.dispose();
				//
				// delete the allocated  client's from
				// the client_list
				//
				client_list.dispose();

				(void) sc_syslog_msg_initialize(&handle,
				    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE, "Error: low memory");
				sc_syslog_msg_done(&handle);

				return (ENOMEM);
			}
		}
	}

	FAULTPT_CLAPI(FAULTNUM_CLAPI_READ_FROM_CCR_E,
		FaultFunctions::generic);

	// return success reading table
	return (0);
}
