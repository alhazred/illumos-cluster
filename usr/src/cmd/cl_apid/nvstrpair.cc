/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma	ident	"@(#)nvstrpair.cc	1.7	08/05/20 SMI"

#include <sys/os.h>

#include "nvstrpair.h"
#include "string_buffer.h"

/* initialize static members */
const char elem_delim = ',';


/*
 * value_print_helper
 * --------------------
 * Helper function for printing a char list.
 */
static bool value_print_helper(char *in_str, void *)
{
	(void) printf("%s ", in_str);
	return (true);
}

/*
 * copy_value_helper
 * --------------------
 * Helper function for doing deep copies of char * lists.
 */
static bool copy_value_helper(char *in_str, void *out_list)
{
	char *temp = os::strdup(in_str);

	/* if strdup failed, we quit copying */
	if (temp == NULL) {
		return (false);
	}
	((SList<char> *)out_list)->append(temp);
	return (true);
}

/*
 * Constructors
 * -------------
 */
nvstrpair::nvstrpair(nvpair_t *nvpair)
{
	/* we start assuming everything will go well */
	status = 0;

	/* get the name of the nvpair */
	char *temp = nvpair_name(nvpair);
	if (temp == NULL || ((name = os::strdup(temp)) == NULL)) {
		status = ENOMEM;
		return;
	}

	/* temporarily */
	value_str = NULL;

	/* get the type of the value */
	data_type_t type = nvpair_type(nvpair);
	string_buffer str_buf;
	char *temp_str;
	uint_t i;
	uint_t nelem;

	/* We always make a list, even if only for one element */
	/* cstyle doesn't understand templates */
	/*CSTYLED*/
	if ((value_list = new SList<char>) == NULL) {
		status = ENOMEM;
		return;
	}

	/* Based on the type, we get the value and convert it to a string */
	switch (type) {
	case DATA_TYPE_UNKNOWN:
	case DATA_TYPE_HRTIME:
		/* error */
		status = -1;
		return;
	case DATA_TYPE_BOOLEAN:
		if (str_buf.append("TRUE") == -1) {
			status = -1;
			return;
		}
		if ((temp_str = os::strdup(str_buf.get_buf())) == NULL) {
			status = ENOMEM;
			return;
		}
		value_list->append(temp_str);
		break;
	case DATA_TYPE_BYTE:
		uchar_t val_uchar;
		if (nvpair_value_byte(nvpair, &val_uchar) != 0) {
			status = errno;
			return;
		}
		if (str_buf.append("%c", val_uchar) == -1) {
			status = -1;
			return;
		}
		if ((temp_str = os::strdup(str_buf.get_buf())) == NULL) {
			status = ENOMEM;
			return;
		}
		value_list->append(temp_str);
		break;
	case DATA_TYPE_INT16:
		int16_t val_int16;
		if (nvpair_value_int16(nvpair, &val_int16) != 0) {
			status = errno;
			return;
		}
		if (str_buf.append("%hd", val_int16) == -1) {
			status = -1;
			return;
		}
		if ((temp_str = os::strdup(str_buf.get_buf())) == NULL) {
			status = ENOMEM;
			return;
		}
		value_list->append(temp_str);
		break;
	case DATA_TYPE_UINT16:
		uint16_t val_uint16;
		if (nvpair_value_uint16(nvpair, &val_uint16) != 0) {
			status = errno;
			return;
		}

		if (str_buf.append("%hd", val_uint16) == -1) {
			status = -1;
			return;
		}
		if ((temp_str = os::strdup(str_buf.get_buf())) == NULL) {
			status = ENOMEM;
			return;
		}
		value_list->append(temp_str);
		break;
	case DATA_TYPE_INT32:
		int32_t val_int32;
		if (nvpair_value_int32(nvpair, &val_int32) != 0) {
			status = errno;
			return;
		}
		if (str_buf.append("%d", val_int32) == -1) {
			status = -1;
			return;
		}
		if ((temp_str = os::strdup(str_buf.get_buf())) == NULL) {
			status = ENOMEM;
			return;
		}
		value_list->append(temp_str);
		break;
	case DATA_TYPE_UINT32:
		uint32_t val_uint32;
		if (nvpair_value_uint32(nvpair, &val_uint32) != 0) {
			status = errno;
			return;
		}
		if (str_buf.append("%d", val_uint32) == -1) {
			status = -1;
			return;
		}
		if ((temp_str = os::strdup(str_buf.get_buf())) == NULL) {
			status = ENOMEM;
			return;
		}
		value_list->append(temp_str);
		break;
	case DATA_TYPE_INT64:
		int64_t val_int64;
		if (nvpair_value_int64(nvpair, &val_int64) != 0) {
			status = errno;
			return;
		}
		if (str_buf.append("%lld", val_int64) == -1) {
			status = -1;
			return;
		}
		if ((temp_str = os::strdup(str_buf.get_buf())) == NULL) {
			status = ENOMEM;
			return;
		}
		value_list->append(temp_str);
		break;
	case DATA_TYPE_UINT64:
		uint64_t val_uint64;
		if (nvpair_value_uint64(nvpair, &val_uint64) != 0) {
			status = errno;
			return;
		}
		if (str_buf.append("%lld", val_uint64) == -1) {
			status = -1;
			return;
		}
		if ((temp_str = os::strdup(str_buf.get_buf())) == NULL) {
			status = ENOMEM;
			return;
		}
		value_list->append(temp_str);
		break;
	case DATA_TYPE_STRING:
		char *val_str;
		if (nvpair_value_string(nvpair, &val_str) != 0) {
			status = errno;
			return;
		}
		if (str_buf.append("%s", val_str) == -1) {
			status = -1;
			return;
		}
		if ((temp_str = os::strdup(str_buf.get_buf())) == NULL) {
			status = ENOMEM;
			return;
		}
		value_list->append(temp_str);
		break;
	case DATA_TYPE_BYTE_ARRAY:
		uchar_t *val_uchar_arr;
		if (nvpair_value_byte_array(nvpair, &val_uchar_arr,
		    &nelem) != 0) {
			status = errno;
			return;
		}
		for (i = 0; i < nelem; i++) {
			str_buf.clear();
			if (str_buf.append("%c", *(val_uchar_arr + i)) == -1) {
				status = -1;
				return;
			}
			if ((temp_str = os::strdup(str_buf.get_buf())) ==
			    NULL) {
				status = ENOMEM;
				return;
			}
			value_list->append(temp_str);
		}
		break;
	case DATA_TYPE_INT16_ARRAY:
		int16_t *val_int16_arr;
		if (nvpair_value_int16_array(nvpair, &val_int16_arr,
		    &nelem) != 0) {
			status = errno;
			return;
		}
		for (i = 0; i < nelem; i++) {
			str_buf.clear();
			if (str_buf.append("%hd", *(val_int16_arr + i)) == -1) {
				status = -1;
				return;
			}
			if ((temp_str = os::strdup(str_buf.get_buf())) ==
			    NULL) {
				status = ENOMEM;
				return;
			}
			value_list->append(temp_str);
		}
		break;
	case DATA_TYPE_UINT16_ARRAY:
		uint16_t *val_uint16_arr;
		if (nvpair_value_uint16_array(nvpair, &val_uint16_arr,
		    &nelem) != 0) {
			status = errno;
			return;
		}
		for (i = 0; i < nelem; i++) {
			str_buf.clear();
			if (str_buf.append("%hd", *(val_uint16_arr + i)) ==
			    -1) {
				status = -1;
				return;
			}
			if ((temp_str = os::strdup(str_buf.get_buf())) ==
			    NULL) {
				status = ENOMEM;
				return;
			}
			value_list->append(temp_str);
		}
		break;
	case DATA_TYPE_INT32_ARRAY:
		int32_t *val_int32_arr;
		if (nvpair_value_int32_array(nvpair, &val_int32_arr,
		    &nelem) != 0) {
			status = errno;
			return;
		}
		for (i = 0; i < nelem; i++) {
			str_buf.clear();
			if (str_buf.append("%d", *(val_int32_arr + i)) == -1) {
				status = -1;
				return;
			}
			if ((temp_str = os::strdup(str_buf.get_buf())) ==
			    NULL) {
				status = ENOMEM;
				return;
			}
			value_list->append(temp_str);
		}
		break;
	case DATA_TYPE_UINT32_ARRAY:
		uint32_t *val_uint32_arr;
		if (nvpair_value_uint32_array(nvpair, &val_uint32_arr,
		    &nelem) != 0) {
			status = errno;
			return;
		}
		for (i = 0; i < nelem; i++) {
			str_buf.clear();
			if (str_buf.append("%d", *(val_uint32_arr + i)) == -1) {
				status = -1;
				return;
			}
			if ((temp_str = os::strdup(str_buf.get_buf())) ==
			    NULL) {
				status = ENOMEM;
				return;
			}
			value_list->append(temp_str);
		}
		break;
	case DATA_TYPE_INT64_ARRAY:
		int64_t *val_int64_arr;
		if (nvpair_value_int64_array(nvpair, &val_int64_arr,
		    &nelem) != 0) {
			status = errno;
			return;
		}
		for (i = 0; i < nelem; i++) {
			str_buf.clear();
			if (str_buf.append("%lld", *(val_int64_arr + i)) ==
			    -1) {
				status = -1;
				return;
			}
			if ((temp_str = os::strdup(str_buf.get_buf())) ==
			    NULL) {
				status = ENOMEM;
				return;
			}
			value_list->append(temp_str);
		}
		break;
	case DATA_TYPE_UINT64_ARRAY:
		uint64_t *val_uint64_arr;
		if (nvpair_value_uint64_array(nvpair, &val_uint64_arr,
		    &nelem) != 0) {
			status = errno;
			return;
		}
		for (i = 0; i < nelem; i++) {
			str_buf.clear();
			if (str_buf.append("%lld", *(val_uint64_arr + i)) ==
			    -1) {
				status = -1;
				return;
			}
			if ((temp_str = os::strdup(str_buf.get_buf())) ==
			    NULL) {
				status = ENOMEM;
				return;
			}
			value_list->append(temp_str);
		}
		break;
	case DATA_TYPE_STRING_ARRAY:
		char **val_str_arr;
		if (nvpair_value_string_array(nvpair, &val_str_arr,
		    &nelem) != 0) {
			status = errno;
			return;
		}
		for (i = 0; i < nelem; i++) {
			str_buf.clear();
			if (str_buf.append("%s", *(val_str_arr + i)) == -1) {
				status = -1;
				return;
			}
			if ((temp_str = os::strdup(str_buf.get_buf())) ==
			    NULL) {
				status = ENOMEM;
				return;
			}
			value_list->append(temp_str);
		}
		break;
	}
	create_value_str();
}

nvstrpair::nvstrpair(const char *name_in, const char *value_in)
{
	status = 0;

	if ((name = os::strdup(name_in)) == NULL) {
		status = ENOMEM;
		return;
	}
	/* cstyle doesn't understand templates */
	/*CSTYLED*/
	if ((value_list = new SList<char>) == NULL) {
		status = ENOMEM;
		return;
	}
	value_list->append(os::strdup(value_in));

	value_str = NULL;
	create_value_str();
}

nvstrpair::nvstrpair(const char *name_in, const SList<char> &value_in)
{
	construct_helper(name_in, value_in);
}

nvstrpair::nvstrpair(const nvstrpair &src)
{
	construct_helper(src.get_name(), src.get_value_list());
}

/*
 * construct_helper
 * -------------------
 * Helps with the construction common to multiple constructors.
 */
void nvstrpair::construct_helper(const char *name_in,
    const SList<char> &value_in)
{
	status = 0;

	if ((name = os::strdup(name_in)) == NULL) {
		status = ENOMEM;
		return;
	}

	/*
	 * Lint thinks that we never free value_list, because
	 * we're allocating it in this function that isn't the destructor.
	 *
	 * We know that we free it.
	 */
	value_list = new SList<char>;	/*lint !e423 */
	if (value_list == NULL) {
		status = ENOMEM;
		return;
	}

	/*
	 * allocate new memory, and make copies of all the
	 * list elements
	 */
	SList<char> *temp_list = const_cast<SList<char> *>
	    (&value_in);
	/*
	 * If we don't get back NULL from the iteration, it means we
	 * stopped part-way, and something went wrong (a memory allocation
	 * failed).
	 */
	if (temp_list->iterate(copy_value_helper, (void *)value_list) !=
	    NULL) {
		status = ENOMEM;
		return;
	}

	value_str = NULL;
	create_value_str();
}

/*
 * op=
 * ----------
 * Does a deep copy.  Destructs then constructs again.
 */
/*
 * note: cstyle doesn't understand operator overloading.
 */
/*CSTYLED*/
const nvstrpair &nvstrpair::operator=(const nvstrpair &rhs)
{
	if (this != &rhs) {
		destruct_helper();
		construct_helper(rhs.get_name(), rhs.get_value_list());
	}
	return (*this);
}

int nvstrpair::get_construction_status()
{
	return (status);
}


/*
 * destructor
 * -------------
 * Frees all memory.  Caller should ensure there are no dangling
 * references.
 */
nvstrpair::~nvstrpair()
{
	destruct_helper();
}

/*
 * destruct_helper
 * -----------------
 * Common functionality between destructor and op=
 */
void nvstrpair::destruct_helper()
{
	delete [](name);

	/* can't get out of constructor without allocating list */
	char *str;
	if (value_list != NULL) {
		while ((str = value_list->reapfirst()) != NULL) {
			delete [](str);
		}
		delete value_list;
	}
	delete [](value_str);
}

/*
 * Accessors
 * -----------------
 * Return references to the attributes.  Note that if the
 * value is a scaler, the list will contain one element.
 */
const char *nvstrpair::get_name() const
{
	return (name);
}

const SList<char> &nvstrpair::get_value_list() const
{
	/* assume we always have a list, even if it might be empty */
	return (*value_list);
}


/*
 * get_value_str
 * -----------------
 * Returns a string version of the value, whether it is an array
 * or a scaler.  If an array, the array elements will by delimited
 * by elem_delim.
 */
const char *nvstrpair::get_value_str() const
{
	return (value_str);
}

/*
 * list_find_helper
 * --------------------
 * Called by iterator of char list.  Compares the char *element
 * to the element passed as void *param.
 */
bool
list_find_helper(char *list_elem, void *list_elem_comp)
{
	/*
	 * If the strings match, return false, so we end the search,
	 * forcing iterate to return non-NULL, which will tell the
	 * caller that a match was found.
	 *
	 * If we don't match, return true, so the search continues.
	 */
	if (strcmp(list_elem, (char *)list_elem_comp) == 0) {
		return (false);
	}
	return (true);
}

/*
 * list_comp_helper
 * -------------------
 * Helper for list iterate.  Called by iterate for each char *element.
 * For each one, iterates over rhs_list, checking of element is
 * found in that list.
 */
bool
list_comp_helper(char *list_elem, void *rhs_list)
{
	/*
	 * If iterate finds a match, it will return a non -NULL ptr.
	 * If it doesn't find a match, it will return NULL.
	 * If we don't find a match, we return false, so that
	 * the outer iterate will end, and the caller will know that
	 * the lists are not equal.
	 *
	 * If we find a match, return true, so the search through
	 * the outer list will continue.
	 */
	SList<char> *temp_list = (SList<char> *)rhs_list;
	if (temp_list->iterate(list_find_helper, list_elem) == NULL) {
		return (false);
	}
	return (true);
}

/*
 * operator==
 * -------------
 * For comparison with other nvstrpairs
 */

/*
 * note: cstyle doesn't understand operator overloading.
 */
/*CSTYLED*/
bool nvstrpair::operator==(const nvstrpair &rhs) const
{

	if (&rhs == this) {
		return (true);
	}

	/*
	 * first, compare the names.
	 * The ways it can be false is if one's NULL and the other's not,
	 * or if they aren't equal with string compare.
	 */
	const char *rhs_name = rhs.get_name();
	if ((name == NULL && rhs_name != NULL) || (rhs_name == NULL &&
	    name != NULL)) {
		return (false);
	}
	/*
	 * We know that rhs_name cannot be NULL here if name is not
	 * NULL. No need to check both, but lint doesn't know that.
	 */
	/*lint -e668 */
	if (name != NULL && strcmp(name, rhs_name) != 0) {
		return (false);
	}
	/*lint +e668 */

	/*
	 * Now compare the value lists
	 */

	const SList<char> &temp_list_const = rhs.get_value_list();
	SList<char> *temp_list = const_cast<SList<char> *>(&temp_list_const);

	if (value_list->count() != temp_list->count()) {
		return (false);
	}

	/*
	 * Iterate through the list, checking that each element is
	 * contained in the temp_list.
	 * If we successfully find all the elements, iterate will return
	 * NULL.  If we don't, it will return non-NULL.
	 */
	if (value_list->iterate(list_comp_helper, temp_list) == NULL) {
		return (true);
	}
	return (false);
}

void nvstrpair::print() const
{
	(void) printf("nvstrpair: name=%s val_str=%s\n",
	    get_name(), get_value_str());
	(void) printf("\tvalues: ");
	(void) value_list->iterate(value_print_helper, NULL);
	(void) printf("\n");
}


/*
 * value_str_helper
 * ----------------
 * Helper for iterating over a char * list, constructing a string
 * concatenation of the elements.  Adds each element to the
 * the end of the string_buffer passed as void *param.
 */
static bool value_str_helper(char *in_str, void *str_buf)
{
	if (((string_buffer *)str_buf)->append("%s%c", in_str, elem_delim) ==
	    -1) {
		/* failed to append the string: return false */
		return (false);
	}
	return (true);
}

/*
 * create_value_str
 * ------------------
 * Constructs a concatenation of all the values. Uses an iterator, using
 * the value_str_helper function.
 */
void nvstrpair::create_value_str()
{
	string_buffer str_buf;
	/*
	 * If iterate returns non-NULL, it means it stopped in the middle
	 * because something went wrong (in our case a memory error)
	 */
	if (value_list->iterate(value_str_helper, (void *)&str_buf) != NULL) {
		status = -1;
		return;
	}

	/* Copy the string to new memory and check for strdup failures... */
	if ((value_str = os::strdup(str_buf.get_buf())) == NULL) {
		status = ENOMEM;
		return;
	}

	// cut off the last comma
	value_str[strlen(value_str) - 1] = '\0';
}
