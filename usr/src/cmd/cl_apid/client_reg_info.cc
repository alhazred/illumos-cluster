/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */
#pragma ident   "@(#)client_reg_info.cc 1.19     08/05/20 SMI"

// XXX need to add more comments, check for lint and cstyle

#include "client_reg_info.h"
#include "string_buffer.h"
#include "cl_apid_error.h"
#include "cl_apid_include.h"
#include <sys/sc_syslog_msg.h>


// HELPER Functions
//
// copy_value_helper function facilitates deep copy of SList members
//
static bool copy_value_helper(caapi_event *evt_in, void *out_list)
{
	caapi_event  *temp_evt = evt_in;
	((SList<caapi_event> *)out_list)->append(temp_evt);
	return (true);
}

//
// Implementation of client_reg_info class
//

client_reg_info::client_reg_info(const sc_registration_instance &
			client_reg_instance):
			evtlistp(NULL),
			callbackp(NULL),
			num_events(0),
			in_ccr(true),
			construct_status(0)

{
	caapi_event *eventp = NULL;
	sc_event  *reg_sc_eventp = NULL;
	SList<sc_event> *reg_event_list = NULL;


	// instantiate a event_list
	evtlistp = new SList<caapi_event>();
	/*lint +e1740 */
	if (evtlistp == NULL) {
		construct_status = ENOMEM;
		return;
	}

	// copy properties of registered client
	callbackp = new sc_callback_info(
	    client_reg_instance.get_callback_info());

	if ((callbackp == NULL) ||
	    (callbackp->get_construct_status() != 0)) {
		construct_status = ENOMEM;
		return;
	}

	// get list of events client has registered for and
	// copy the event details into evtlist
	//
	reg_event_list = const_cast<SList<sc_event>*>(
			    &(client_reg_instance.get_events()));

	reg_event_list->atfirst();
	while ((reg_sc_eventp = reg_event_list->get_current()) != NULL) {
		reg_event_list->advance();

		// check if the event is a duplicate
		if (find_event(reg_sc_eventp, eventp)) {
			// if found dont add the event again into the list.
			eventp = NULL;
			continue;
		}

		// instantiate a new caapi_event for registered sc_event
		eventp = new caapi_event(*reg_sc_eventp);
		if ((eventp == NULL) ||
		    (eventp->get_construct_status() != 0)) {
			construct_status = ENOMEM;
			delete eventp;
			return;
		}
		evtlistp->append(eventp);
	}

	// total number of events client has registered
	num_events = evtlistp->count();

}

client_reg_info::client_reg_info(const char *key_in,
				    const char *data_in):
				    evtlistp(NULL),
				    callbackp(NULL),
				    num_events(0),
				    in_ccr(true),
				    construct_status(0)
{
	evtlistp = new SList<caapi_event>();

	if (evtlistp == NULL) {
		construct_status = ENOMEM;
		return;
	}

	if (parse_client_info(key_in, data_in) != 0) {
		construct_status = ENOMEM;
		return;
	}

	// total number of events client has registered
	num_events = evtlistp->count();
}

//
// following functions deallocates all data members
// called in destructor and operator overloading
//
void
client_reg_info::destruct_helper()
{
	delete callbackp;
	evtlistp->dispose();
	delete evtlistp;
}

/*
//
// operator overloading =
//
const client_reg_info&
client_reg_info::operator = (const client_reg_info
			    & rhs)
{
	if (this != &rhs) {
		//
		// make sure data members are deallocated
		//
		destruct_helper();
		construct_helper(rhs);

	}
	return (*this);
}

client_reg_info::client_reg_info(const client_reg_info
				    & src)
{
	construct_helper(src);
}

*/
void
client_reg_info::construct_helper(const client_reg_info &src)
{

	// copy the values
	/* lint doesn't know we free callbackp in destruct_helper */
	/*lint -e423 */
	callbackp = new sc_callback_info(*(src.get_callback_info()));
	/*lint +e423 */

	//
	// allocate new memory, and make copies of all the
	// src event list to "this" event list
	//
	SList<caapi_event> *temp_list =  src.get_events();

	(void) temp_list->iterate(copy_value_helper, (void *)evtlistp);

}

// copy constructor to allow deep copy

//
// destructor
// dealloc memory of all data members
//
// lint suppression, as it cant locate  deallocation of evtlistp
/*lint -e1740 */
client_reg_info::~client_reg_info()
{
	destruct_helper();
}
/*lint +e1740 */

//
// caapi_client HELPER functions
//
// following set of functions are used to parse the key-data into
// callback info and event properties
// 	parse_client_info()
// 	parse_data()
//
// parse_client_info parses the key into callback_info
// and data into event properties ie.. class
// subclass and list of name value pairs
// key and data both are in parameteres
//
int
client_reg_info::parse_client_info(const char *key_in,
				    const char *data_in)
{
	SList<char> eventstr_list;
	char *callback_ip = NULL;
	int callback_port = 0;
	char *callback_portstr = NULL;
	char *client_key = NULL;
	char *client_data = NULL;
	char *eventstr = NULL;
	char *lasts = NULL;

	if (key_in)
		client_key = os::strdup(key_in);
	if (data_in)
		client_data = os::strdup(data_in);

	if ((client_key == NULL) ||
	    (client_data == NULL)) {
		construct_status = ENOMEM;
		delete [] client_key;
		delete [] client_data;
		return (ENOMEM);
	}

	//
	// this will return a list of string
	// each string contains properties of a event
	//
	if (parse_data(client_data, eventstr_list) != 0) {
		construct_status = ENOMEM;
		delete [] client_key;
		delete [] client_data;
		eventstr_list.dispose();
		return (ENOMEM);
	}

	// get callback ip and port from key character string
	if (client_key) {
		callback_ip = strtok_r(client_key, EVENT_DELIMITER, &lasts);
		if (callback_ip != NULL) {
			callback_portstr = strtok_r(NULL, EVENT_DELIMITER,
			    &lasts);
			if (callback_portstr)
				callback_port = atoi(callback_portstr);
		}
	}

	// initialize callback info class...
	/* We free callbackp in destruct_helper, so suppress the warning */
	/*lint -e423 */
	callbackp = new sc_callback_info(callback_ip, callback_port);
	/*lint +e423 */
	if ((callbackp == NULL) ||
	    (callbackp->get_construct_status() != 0)) {
		construct_status = ENOMEM;
		delete callbackp;
		eventstr_list.dispose();
		return (ENOMEM);
	}

	// for each string in event_str_list a caapi_event is created
	eventstr_list.atfirst();
	while ((eventstr = eventstr_list.get_current()) != NULL) {
		eventstr_list.advance();

		caapi_event *eventp = new caapi_event(eventstr);

		if ((eventp == NULL) ||
		    (eventp->get_construct_status() != 0)) {
			construct_status = ENOMEM;
			delete eventp;
			eventstr_list.dispose();
			return (ENOMEM);
		}

		// add this caapi-event list of events of client
		evtlistp->append(eventp);
		// increment  the total events associated with client
		num_events++;
	}
	// deallocate the list's created in this function
	eventstr_list.dispose();

	delete [] client_key;
	delete [] client_data;

	// allocated in parse_data function
	return (0);
}

//
// parse_data parses data into source ip and a list
// of event_strings which has details of event
// this function is called by parse_client_info
// eventstr is allocated in this function
// but deallocated in parse_client_info function
//
int
client_reg_info::parse_data(char *data_in, SList <char> &event_str_list)
{
	char *temp_data = NULL;
	char *tmp_eventstr = NULL;
	char *eventstr = NULL;
	char *lasts = NULL;
	bool allocation_status;
	if (data_in) {
		temp_data  = os::strdup(data_in);
		if (temp_data == NULL) {
			construct_status = ENOMEM;
			return (ENOMEM);
		}
		tmp_eventstr = strtok_r(temp_data, DATA_DELIMITER, &lasts);
		if (tmp_eventstr) {
			eventstr =
			    os::strdup(tmp_eventstr);
			if (eventstr == NULL) {
				construct_status = ENOMEM;
				delete [] temp_data;
				return (ENOMEM);
			}
			event_str_list.append(eventstr, &allocation_status);
			if (!allocation_status) {
				construct_status = ENOMEM;
				delete [] temp_data;
				return (ENOMEM);
			}
		}
		// data has more events to be parsed.
		while ((tmp_eventstr = strtok_r(NULL, DATA_DELIMITER, &lasts))
		    != NULL) {
			eventstr = os::strdup(tmp_eventstr);
			if (eventstr == NULL) {
				construct_status = ENOMEM;
				delete [] temp_data;
				return (ENOMEM);
			}
			event_str_list.append(eventstr, &allocation_status);
			if (!allocation_status) {
				construct_status = ENOMEM;
				delete [] temp_data;
				return (ENOMEM);
			}
		}
		delete [] temp_data;
	}
	return (0);
}

//
// get_key_data concatenates the
// different data memebers of a client into key and data
// NOTE caller of get_key_data passes NULL key and data values
// get_key_data function populates the callback info into key
// and event details into data caller is responsible for freeing up
// the memory allocated to key and data
//
int
client_reg_info::get_key_data(char*& key_out, char*& data_out)
{
	caapi_event * client_event = NULL;
	nvstrpair * evt_nvpair = NULL;
	string_buffer data_str;
	string_buffer key_str;
	sc_syslog_msg_handle_t handle;

	//
	// if the in_ccr is false then client needs to be ignored
	// as the client information shouldnt be written to caapi_reg
	// ccr table and key is set to NULL
	//
	if (!in_ccr) {
		key_out = NULL;
		data_out = NULL;
		return (0);
	}

	if (callbackp) {
		//
		// concatenate the callback ip and port
		// into data member key
		//
		// if callbackip  is NULL then  return with error
		if (callbackp->get_cb_ip() == NULL)
			return (CLEP_ERR_INV);

		(void) key_str.append("%s%s%d", callbackp->get_cb_ip(),
		    EVENT_DELIMITER,
		    callbackp->get_cb_port());

		key_out = os::strdup(key_str.get_buf());
		if (key_out == NULL) {
			// allocation failure return no memory
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "Error: low memory");
			sc_syslog_msg_done(&handle);
			return (ENOMEM);
		}
	}

	//
	// if no events associated with the client then data is empty
	// string.
	//
	if (!evtlistp) {
		data_out = os::strdup(data_str.get_buf());
		data_str.clear();
		if (data_out == NULL) {
			// allocation failure return no memory
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "Error: low memory");
			sc_syslog_msg_done(&handle);
			return (ENOMEM);
		}
	}

	// concatenate the event properties and append to data
	evtlistp->atfirst();
	while ((client_event = evtlistp->get_current()) != NULL) {

		evtlistp->advance();
		// start of new event details

		//
		// if event of a client is to be removed from the
		// event list the in_ccr is false as this event detail
		// should be written in caapi_reg ccr table
		// so event is ignored in the data formation
		//
		if (!client_event->is_in_ccr())
			continue;

		// event with no class name is not added
		if (client_event->get_class() == NULL)
			continue;

		(void) data_str.append("%s", client_event->get_class());

		// only non-null subclass are added
		if (client_event->get_subclass() == NULL) {
			// add delimiter between events
			(void) data_str.append("%s", DATA_DELIMITER);
			continue;
		}

		(void) data_str.append("%s%s", EVENT_DELIMITER,
		    client_event->get_subclass());

		//
		// iterate the nvlist and concatenate the nv-name and
		// nv-value to eventstr since no const iterator is
		// supported by SList const_cast is used
		//
		SList<nvstrpair> *tmp_nvlist =
			const_cast<SList<nvstrpair> *>
		    (&(client_event->get_nvlist()));

		tmp_nvlist->atfirst();
		while ((evt_nvpair = tmp_nvlist->get_current()) != NULL) {
			tmp_nvlist->advance();

			// ignore NULL name-value pairs
			if (!(evt_nvpair->get_name()) ||
			    (!evt_nvpair->get_value_str()))
				continue;

			(void) data_str.append("%s%s%s%s", EVENT_DELIMITER,
				evt_nvpair->get_name(),
				NV_DELIMITER,
				evt_nvpair->get_value_str());

		}
		// add delimiter between events
		(void) data_str.append("%s", DATA_DELIMITER);
	}
	//
	// allocate new memory for data and caller is supposed to deallocated
	// memory allocated to key_out and data_out
	//
	data_out = os::strdup(data_str.get_buf());
	data_str.clear();
	if (data_out == NULL) {
		// allocation failure return no memory
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Error: low memory");
		sc_syslog_msg_done(&handle);
		return (ENOMEM);
	}
	return (0);
}

//
// equals function is used to compare the registration instance
// with this client. returns true if the callback ip, port matches
// the callback ip and port of registration instance.
//
bool
client_reg_info::equals(const sc_registration_instance &
			    client_reg_in)
{
	if ((client_reg_in.get_callback_info().get_cb_port() ==
	    callbackp->get_cb_port()) &&
	    (strcmp(client_reg_in.get_callback_info().get_cb_ip(),
	    callbackp->get_cb_ip()) == 0)) {
		return (true);
	}

	return (false);
}

//
// remove_events deletes the specified list of events
// from the client's list of events.
//
int
client_reg_info::remove_events_prepare(SList<caapi_event> &unreg_event_list)
{
	// XXX provide lookup for a event in the mapping structure

	caapi_event *unregister_event = NULL;

	unreg_event_list.atfirst();
	while ((unregister_event = unreg_event_list.get_current()) != NULL) {
		scds_syslog_debug(1, "\nclient_reg_info: "
		    "remove_events_prepare :");
		// remove  event from evtlistp
		unreg_event_list.advance();
		unregister_event->set_in_ccr(false);

		// (void) evtlistp->erase(unregister_event);
	} // end of outer while
	return (0);
}

//
// in remove_events_commit events with in_ccr set to false are finally removed
// from the event list
//
int
client_reg_info::remove_events_commit()
{
	caapi_event *eventp = NULL;
	evtlistp->atfirst();
	while ((eventp = evtlistp->get_current()) != NULL) {
		evtlistp->advance();
		//
		// events with in_ccr set to false are finally removed
		// from the event list
		//
		if (!eventp->is_in_ccr()) {
			scds_syslog_debug(1, "\nclient_reg_info: "
			    "remove_events_commit: ");
			(void) evtlistp->erase(eventp);
			delete eventp;
			num_events--;
		}
	}
	return (0);
}

//
// events with in_ccr set to false are set to true as the events
// werent removed
//
int
client_reg_info::remove_events_revoke()
{
	caapi_event *eventp = NULL;
	//
	// events with in_ccr set to false are set to true as the events
	// werent removed
	//
	evtlistp->atfirst();
	while ((eventp = evtlistp->get_current()) != NULL) {
		evtlistp->advance();

		// in_ccr for all events are set to true
		eventp->set_in_ccr(true);
	}
	return (0);
}

//
// add_events adds the specified list of events in the registration instance
// to the client's list of events
//
int
client_reg_info::add_events(SList<sc_event> &reg_event_list)
{
	sc_event *reg_sc_eventp = NULL;
	caapi_event *eventp = NULL;

	// add the new list at the end of event list
	reg_event_list.atfirst();

	while ((reg_sc_eventp = reg_event_list.get_current()) != NULL) {
		reg_event_list.advance();

		// check if the event is a duplicate
		if (find_event(reg_sc_eventp, eventp)) {
			// if found dont add the event again into the list.
			eventp = NULL;
			continue;
		}

		eventp = new caapi_event(*reg_sc_eventp);
		if ((eventp == NULL) ||
		    (eventp->get_construct_status() != 0)) {
			delete eventp;
			return (ENOMEM);
		}
		evtlistp->append(eventp);
		num_events++;
	}
	return (0);
}

//
// in add_events_revoke all the events with client back ptr set to NULL
// are erase from client event_list as they are not added successfully into
// caapi_reg ccr table
//
int
client_reg_info::add_events_revoke()
{

	caapi_event *eventp = NULL;
	//
	// in this case the client's new events should be removed from
	// the client as add_events failed
	//
	evtlistp->atfirst();
	while ((eventp = evtlistp->get_current()) != NULL) {
		evtlistp->advance();

		// all events whose client back ptr is set NULL are erased
		if (eventp->get_client_backptr() == NULL) {
			(void) evtlistp->erase(eventp);
			delete eventp;
		}
	} // end of while
	return (0);
}

// Accessor Functions
const sc_callback_info*
client_reg_info::get_callback_info() const
{
	return (callbackp);
}

SList<caapi_event>*
client_reg_info::get_events() const
{
	return (evtlistp);
}

void
client_reg_info::set_in_ccr(bool in_ccr_new)
{
	in_ccr = in_ccr_new;

}
//
// O(N) function to identify is a event exists in the client
// return's true if it exists and sets the eventp
// else return false and eventp is set to NULL
// reg_event is the event in registration instance
// eventp_out is the event which is found in the client
//
bool
client_reg_info::find_event(sc_event *reg_event, caapi_event
				*&eventp_out)
{
	caapi_event *eventp = NULL;
	sc_event *sc_eventp = NULL;

	bool event_found = false;
	eventp_out = NULL;

	evtlistp->atfirst();
	while ((eventp = evtlistp->get_current()) != NULL) {
		evtlistp->advance();

		//
		// caapi event eventp is casted to type
		// sc_event as the incoming registeration
		// event is a sc_event type
		//
		sc_eventp = static_cast<sc_event*>(eventp);

		//
		// check if the event listed in client
		// matches the event in registration request
		//
		if (*sc_eventp == *reg_event) {
			eventp_out = eventp;
			event_found = true;
			break;
		}
	}
	return (event_found);
}
