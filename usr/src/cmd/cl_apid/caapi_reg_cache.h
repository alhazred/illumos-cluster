/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _CAAPI_REG_CACHE_H_
#define	_CAAPI_REG_CACHE_H_

#pragma ident	"@(#)caapi_reg_cache.h	1.16	08/05/20 SMI"

#include <sys/param.h>
#include <sys/os.h>
#include <sys/rm_util.h>
#include <h/naming.h>
#include <nslib/ns.h>
#include <ccr/readonly_copy_impl.h>
#include <ccr/updatable_copy_impl.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#include "sc_registration_instance.h"
#include "sc_callback_info.h"
#include "caapi_reg_io.h"
#include "caapi_event.h"
#include "caapi_client.h"
#include "caapi_mapping.h"

//
// class caapi_reg_cache is the main class interfacing with the communication
// subsystem of caapi module.The caapi_reg_cache class has the following three
// primary functions
// 1. providing a interface for updating client information in caapi_reg
// file and maintaining a data-structure caapi_client_info for faster updation
// of client information.
// 2. supporting a interface to get list of clients for a given event.
// 3. supporting a mechanism of removing stale client data from caapi_reg file.1
// 4. supporting a interface to get a list oc lient sorted by subclass.
//
class caapi_reg_cache {
public:
	//
	// This is the primary method for updating client information.
	// This method is used for client to register, add/remove event
	// and unregister itself. This method makes use of
	// caapi_reg_io class for writing to caapi_reg file.
	// This is also responsible for updating the client_info list
	// and the event-client mapping data struture
	// only one thread is allowed to update client information
	// two-phase commit is implemented , wherein in-memory strutures.
	// are changed according to client's registration info and then
	// changes are updated in caapi_reg ccr file if this is succeeds then
	// in-memory changes are commited else it is revoked
	//
	int update_clients(SList<sc_registration_instance>
			    &reg_instance_list);

	//
	// This is the primary interface for getting a list interested
	// clients for a particular event. This method uses
	// event_mapping class for getting list of clients.
	// Note: caller is supposed to delete memory for client_handle
	// and SList
	//
	SList <caapi_client_handle> *lookup_clients(const sc_event
	    *eventp_in);

	//
	// returns hashtable sorted by subclass and each
	// subclass is associated with a list of clients handles interested
	// in the subclass
	// Note: caller is supposed to delete memory of hashtable
	// and all the data contentained ie client handle's
	//
	string_hashtable_t<SList<caapi_client_handle>*>* get_clients();

	static int initialize(uint_t max_num_client_registration);

	int update_registration_parameters(
	    uint_t max_total_client_registration);

	//
	// static method used for creating a single instance
	// of caapi_reg_cache
	//
	static caapi_reg_cache &the();

	void shutdown();
private:
	//
	// private constructor is called from static method
	// single instance of caapi_reg_cache supported
	//
	caapi_reg_cache();

	// destructor
	~caapi_reg_cache();

	// disallowed - pass by value and assignment
	caapi_reg_cache(const caapi_reg_cache &);
	caapi_reg_cache &operator = (const caapi_reg_cache &);

	//
	// intializes data members client_list and event_mapping structures
	// with the values in caapi_reg ccr file
	//
	int initialize_cache(uint_t max_num_client_registration);

	//
	// returns if registration instance matches with any of the client
	// in the in-memory client-list
	//
	int find_client(caapi_client *&clientp,
			    sc_registration_instance &reg_instance);



	//
	// update_client_prepare iterates the sc_registration_instance and
	// for each incoming request updates the in-memory client with new
	// set of events or unregister's client. it also updates the
	// reg_instance list with the associated client handle's
	//
	int update_client_prepare(SList<sc_registration_instance>
				&reg_instance_list,
				SList<caapi_client> &updated_client_list);

	// commits the changes requested by the registration instance
	void update_client_commit(SList<sc_registration_instance>
				&reg_instance_list,
				SList<caapi_client> &updated_client_list);

	// revokes the changes done to client_list and event mapping structures
	void update_client_revoke(SList<sc_registration_instance>
				&reg_instance_list,
				SList<caapi_client> &updated_client_list,
				int ret_value);


	//
	// get_events_to_update function verifies if a event is already
	// registered by the client. If the regsitration type is ADD_EVENTS
	// and if the event is already registered then that event is ignored
	// else it is the event information is added to the client's
	// registration information. else if the registration type is
	// REMOVE_EVENTS then the event is removed only if it has been already
	// registered by the client.This function uses the event mapping hash
	// table for optimal search.
	//
	int get_events_to_update(caapi_client *clientp,
	    const sc_registration_instance * reg_instancep,
	    void *event_listp);



	// returns total_num_clients registered
	inline uint_t get_num_clients() { return (num_clients); }

	//
	// functionality for cleanup thread is not considered as of now.
	// class periodic_cleanup_thread {
	// }
	//
	// This list of clients is initialized with the values in
	// caapi_reg file.
	//
	SList<caapi_client>		client_list;
	// Maximum number of client registration suppported
	uint_t 			MAXIMUM_REGISTRATION;
	//
	// instance of caapi_reg_io is used in reading/writing into
	// caapi_reg file
	//
	caapi_reg_io			caapi_reg_io_instance;

	//
	// This data stucture maintains a list of events and
	// associated clients information. This data-structure also implements
	// optimized search algorithms for getting list of clients for
	// a given event and nvpairs.
	//
	caapi_mapping			event_mapping;

	//
	// since single instance of caapi_reg_cache is supported
	// lock is desired to avoid race conditions.
	//
	// two separate locks are provided one to lock the
	// event_mapping structure and other to update the
	// the ccr table so that lookup can take place when ccr
	// updation is in progress
	//
	os::mutex_t			registration_lck;
	os::mutex_t			mapping_lck;
	static caapi_reg_cache		*the_caapi_reg_cache;

	//
	// The total allowed number of clients is restricted and
	// is governed by MAXIMUM_REGISTRATION
	//
	uint_t				num_clients;

};


//
// client_hashlist_by_subclass is a wrapper class for the string_hashtable
// subclass_hashp. The hashtable is indexed by subclass and associated with
// each subclass is a list of clients registered for a event with this subclass
// as property. This class also provides a interface  get_clients which
// returns the subclass_hashp hashtable.
//
class	client_hashlist_by_subclass {
public:
	client_hashlist_by_subclass();
	~client_hashlist_by_subclass();
	//
	// returns hashtable sorted by subclass and each
	// subclass is associated with a list of clients handles interested
	// in the subclass.
	// Note: caller is supposed to delete memory of hashtable
	// and all the data contentained ie client handle's.
	//
	string_hashtable_t<SList<caapi_client_handle> *>* get_clients(
	    SList<caapi_client> &client_list);
	//
	// cleanup_hashtable function -> de-allocates all the memory allocated
	// to the hash_elements. this function is called only during low-memory
	// scenarios.
	//
	void destruct_helper();
protected:
	string_hashtable_t<SList<caapi_client_handle> *>* subclass_hashp;
private:
	// disallow pass by value and assignment.
	client_hashlist_by_subclass(const client_hashlist_by_subclass &src);
	client_hashlist_by_subclass& operator=(const
	    client_hashlist_by_subclass &rhs);

};

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#endif /* _CAAPI_REG_CACHE_H_ */
