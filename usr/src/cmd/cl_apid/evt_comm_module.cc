/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma	ident	"@(#)evt_comm_module.cc	1.12	08/05/20 SMI"

#include <pthread.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/cl_assert.h>
#include <sys/hashtable.h>
#include <orb/fault/fault_injection.h>

#include "evt_comm_module.h"
#include "cl_apid_include.h"
#include "net_tools.h"

/*
 * First, define the method run by the main thread in this module
 */


/*
 * send_thread
 * ----------------
 * This function will loop until the shutdown_send flag gets set to true.
 * It keeps track of all the current clients with pending events to deliver
 * in the cur_clients hashtable of client_connect objects.
 *
 * Each time through the loop it does the following tasks, in the order
 * shown:
 * 1. Check for new events to deliver.  Events are queued up on the
 * staging queue.  The process_staging_queue function will add the event to
 * each client_connect object in the cur_clients hashtable that is in that
 * event's delivery list, or create new client_connect objects if an object
 * for a client to deliver it to does not exist.
 *
 * 2. Check if any clients on the retry queue are ready to be re-tried.
 * Clients are stored on the retry_queue in order, such that the first client
 * on the queue will be the first that is ready to be re-tried.  Each
 * client_connect object stores a next_retry_time value, which is the absolute
 * time at which the client should be re-tried.
 *
 * 3. Check if any clients with currently open connections need to be timed
 * out.  Each client_connect object stores a connection_object value, which
 * is the absolute time at which we will give up on trying to contact the
 * client (and add it to the retry-queue).  If the time is 0, the client is
 * not currently actively trying to connect.  Each time we hear from the client
 * we reset the timeout time.  If any clients need to be timedout, we
 * add them to the retry_queue.  We also collect the time at which the
 * next client to be timed out will be timed out, for use in the select call.
 *
 * 4. Call select, waiting for all active client connection sockets to be
 * become writable, or to timeout on a retry-queue or connection timeout.
 * The timeout time (which is relative) is set to the smaller of the next
 * retry time and the next connection timeout time minus the current time.
 * Note that if there are no active connections and nothing on the retry
 * queue, we don't call select with a timeout value.
 *
 * 5. After the select call, we write to any sockets that are writable,
 * clear out any clients for which we have delivered everything, and start
 * new event deliveries for clients to whom we have finished delivering a
 * previous event.
 */
void evt_comm_module::send_thread()
{
#ifdef _FAULT_INJECTION
	if (fault_triggered(FAULTNUM_CLAPI_EVT_SEND_THREAD_R,
		NULL, NULL)) {
		exit(1);
	}
#endif
	FAULTPT_CLAPI(FAULTNUM_CLAPI_EVT_SEND_THREAD_B,
		FaultFunctions::generic);

	sc_syslog_msg_handle_t handle;
	fd_set allset, wset, rset;
	int maxfd, nready, err;
	string_hashtable_t<client_connect *> cur_clients;
	struct timeval timer;
	time_t next_connection_timeout;

	scds_syslog_debug(1, "evt_comm::send_thread begin\n");

	// we start with only the pipe
	maxfd = signal_pipe[0];

	/*
	 * allset tracks the current sockets on which we're waiting.
	 * Can't just use wset, because select modifies the fd_sets.
	 */
	FD_ZERO(&allset); /*lint !e534 */

	/*
	 * Loop forever, sending events.
	 * The only way we break out is if the shutdown_send variable
	 * is set.
	 *
	 * ?? should we hold a lock while reading shutdown_send ??
	 */
	while (!shutdown_send) {

		/*
		 * First thing through the loop, we check the staging queue
		 * for new event delivery requests that may have arrived.
		 */
		process_staging_queue(cur_clients, &allset, maxfd);

		/*
		 * Next we check the retry_queue, to see if the wait time
		 * has expired for any of the clients.
		 */
		process_retry_queue(&cur_clients, &allset, maxfd);

		/*
		 * Next, we see if any of our currently open connections
		 * have timed out.
		 */
		next_connection_timeout = check_timedout_clients(&cur_clients,
		    &allset);

		/*
		 * Initialize rset to be our signal_pipe
		 */
		/* appease lint */
		/*lint -e713 -e737 -e573 */
		FD_ZERO(&rset); /*lint !e534 */
		FD_SET(signal_pipe[0], &rset);
		/*lint +e713 +e737 +e573 */

		/*
		 * reinitialize wset each time, because select modified
		 * it last time.
		 */
		wset = allset;

		/*
		 * We have three cases here: if we have something on the retry
		 * queue, or something that we're currently waiting to become
		 * wraitable, we have to use a timeout.  If there is nothing on
		 * the retry queue and no open connections, we can sleep until
		 * our pipe gets written to.
		 */

		const client_connect *client_peek = retry_queue.peek();
		if (client_peek != NULL || next_connection_timeout != 0) {
			time_t cur_time = time(NULL);
			time_t timeout_time = 0;
			/*
			 * set up our timeout value to the timeout for checking
			 * the retry queue or the connection timeout,
			 * whichever is sooner.
			 */
			if (client_peek != NULL) {
				/* Case where there is something on retry q. */
				timeout_time = client_peek->next_retry_time;
				if (next_connection_timeout != 0 &&
				    next_connection_timeout < timeout_time) {
					/* Case of both */
					timeout_time = next_connection_timeout;
				}
			} else {
				/*
				 * Case of connection timeout, but no retry
				 * queue timeout.
				 */
				timeout_time = next_connection_timeout;
			}

			timer.tv_sec = timeout_time - cur_time;
			if (timer.tv_sec < 0)
				timer.tv_sec = 0;
			timer.tv_usec = 0;

			scds_syslog_debug(1, "Something on retry queue or "
			    "connection to be timed out: "
			    "sleeping for %d\n", timer.tv_sec);
			nready = select(maxfd + 1, &rset, &wset, NULL, &timer);
		} else {
			scds_syslog_debug(1, "Nothing on retry queue and "
			    "no connections to be timed out: "
			    "sleeping forever\n");
			nready = select(maxfd + 1, &rset, &wset, NULL, NULL);
		}

		scds_syslog_debug(1,
			"evt_send_thr: select. nread=%d\n", nready);

		/* check for error condition */
		if (nready < 0) {
			err = errno;

			/* error on select */
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
			//
			// SCMSGS
			// @explanation
			// The cl_apid received the specified error from
			// select(3C). No action was taken by the daemon.
			// @user_action
			// No action required. If the problem persists, save a
			// copy of the /var/adm/messages files on all nodes
			// and contact your authorized Sun service provider
			// for assistance in diagnosing and correcting the
			// problem.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR,
			    MESSAGE, "select: %s", strerror(err));
			sc_syslog_msg_done(&handle);

			/* just loop around and call select again */
			continue;
		}

		/* appease lint */
		/*lint -e713 -e737 -e573 */
		if (FD_ISSET(signal_pipe[0], &rset)) {
			/*
			 * read off a lot, in case more than
			 * one message has appeared, and we've
			 * been signaled more than once.
			 */
			char signal_pipe_buf[128];
			int amt;
			scds_syslog_debug(1,
				"evt_send_thr: signal_pipe was signaled\n");
			amt = read(signal_pipe[0], signal_pipe_buf, 128);
			scds_syslog_debug(1, "Read: %d\n", amt);
			nready--;
		}
		/*lint +e713 +e737 +e573 */

		/*
		 * check for timeout or only the signal pipe ready.
		 */
		if (nready == 0) {
			/*
			 * timeout: forces us to check the staging queue
			 * again.
			 */
			scds_syslog_debug(1,
				"evt_send_thr: timeout or pipe signal\n");
			continue;
		}

		/*
		 * We know we have at least one socket ready to read,
		 * or with an error condition pending.
		 */

		/*
		 * Now check for all the connections to see if we can write,
		 * or if there's an error.
		 *
		 * Need to pass allset and cur_clients, because we may need
		 * to close a client socket and/or remove a client from the
		 * list.
		 */
		check_ready_connections(wset, &allset, maxfd, &cur_clients,
		    nready);
	}

	/*
	 * If we get here, shutdown_send is true, so we need to stop
	 * delivering events.
	 */
	scds_syslog_debug(1, "event send thread exiting\n");


	/*
	 * Get everything off the retry queue, so the queue can be
	 * deleted when this class is destructed.
	 */
	while (retry_queue.dequeue() != NULL) {
		/* intentionally empty */
	}

	/*
	 * close all open connections
	 * Iterate through the cur_clients hashtable,
	 * removing and deleting each client.
	 */
	string_hashtable_t<client_connect *>::iterator hash_it(cur_clients);
	client_connect *cur_connect;

	for (cur_connect = hash_it.get_current(); cur_connect != NULL;
	    cur_connect = hash_it.get_current()) {

		/* advance the iterator */
		hash_it.advance();

		(void) cur_clients.remove(cur_connect->handle->
		    get_callback_info()->get_key());

		/* delete does all the cleanup, including closing the socket */
		delete (cur_connect);
	}
	scds_syslog_debug(1, "Event send thread shutting down\n");
	pthread_exit(NULL);
}

/*
 * process_staging_queue
 * -------------------------
 * For every comm_req entry on the staging queue, for each client
 * in the comm_req client list, either adds the client to an existing
 * client_connect object for it, or creates a new client_connect object
 * for that client.  If a new client_connect object is created, a new
 * connection is started as well.
 */
void evt_comm_module::process_staging_queue(
    string_hashtable_t<client_connect *> &cur_clients, fd_set *allset,
    int &maxfd)
{
	comm_req *cur_com = NULL;
	caapi_client_handle *cur_handle = NULL;
	client_connect *cur_client;
	const char *key;

	/* grab the lock for mutual exclusion */
	CL_PANIC(pthread_mutex_lock(&staging_lock) == 0);

	scds_syslog_debug(1, "evt_send_thread: processing staging queue\n");

	/* take everything off the staging queue */
	while ((cur_com = staging_queue.dequeue()) != NULL) {
		scds_syslog_debug(1, "Staging queue: found msg\n");
		/*
		 * Each entry on the staging queue consists of
		 * multiple client handles for one message.
		 *
		 * Iterate through the client handles.
		 */
		cur_com->handles->atfirst();
		while ((cur_handle = cur_com->handles->get_current())
		    != NULL) {
			cur_com->handles->advance();
			/*
			 * For each client handle, look up that client
			 * in the cur_clients table.
			 */
			key = cur_handle->get_callback_info()->get_key();
			cur_client = cur_clients.lookup(key);

			/*
			 * If we find that client, it means that we're
			 * currently trying to send stuff to the client, so
			 * we can just add the new msg to the tail of the
			 * queue.
			 *
			 * If we don't find the client, we need to do a little
			 * more work, so we call the new_client subroutine
			 * to help us out.
			 */
			if (cur_client != NULL) {
				scds_syslog_debug(1,
					"\tenqueuing msg for client %s\n",
				    key);
				(void) cur_client->msgs.enqueue(cur_com->msg->
				    get_ptr_ref());
			} else {
				scds_syslog_debug(1,
					"\tadding client %s\n", key);
				/*
				 * What should we do if it fails ???
				 */
				(void) create_new_client(cur_clients, allset,
				    maxfd, cur_handle, cur_com->msg);
			}
		}
		delete cur_com;
	}
	CL_PANIC(pthread_mutex_unlock(&staging_lock) == 0);
}


/*
 * create_new_client
 * -------------------
 * Creates the structure for storing the client info, and
 * adds it to the cur_clients hashtable.
 * Calls initiate_msg_delivery to start off the event delivery for this
 * first message.
 */
int evt_comm_module::create_new_client(
    string_hashtable_t<client_connect *> &cur_clients, fd_set *allset,
    int &maxfd, caapi_client_handle *new_handle, smart_string *new_msg)
{
	sc_syslog_msg_handle_t handle;

	/* create the object */
	client_connect *new_client = new client_connect(new_handle,
	    new_msg->get_ptr_ref());

	/* verify the creation */
	if (new_client == NULL || new_client->get_construct_status() != 0) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Error: low memory");
		sc_syslog_msg_done(&handle);

		if (new_client != NULL)
			delete new_client;
		return (ENOMEM);
	}

	/* add the new client to the table */
	cur_clients.add(new_client,
	    new_handle->get_callback_info()->get_key());

	/* Now we start off the delivery */
	return (initiate_msg_delivery(&cur_clients, new_client, allset,
	    maxfd));
}

/*
 * initiate_msg_delivery
 * ------------------------
 * Called under three different scenarios:
 *
 * 1. The first time a client_connect is created.
 * 2. Following a successful full delivery of one message, to deliver the
 *    next message.
 * 3. After the client_connect has sat on the retry_queue, and is now being
 *    retried.
 *
 * In cases one and two, cur_msg should be NULL, but in case 3 three should
 * be a cur_msg ready to go.
 */
int evt_comm_module::initiate_msg_delivery(
    string_hashtable_t<client_connect *> *cur_clients, client_connect *client,
    fd_set *allset, int &maxfd)
{
	/*
	 * If there's no current message, grab the next
	 * message to deliver.
	 */
	if (client->cur_msg == NULL) {
		client->cur_msg = client->msgs.dequeue();
	}

	/*
	 * If there's no message at all, remove this client from the hashtable.
	 */
	if (client->cur_msg == NULL) {
		scds_syslog_debug(1,
			"No message for %s: removing.\n", client->handle->
			get_callback_info()->get_key());
		(void) cur_clients->remove(client->handle->
		    get_callback_info()->get_key());
		delete client;
		return (0);
	}

	/*
	 * Make sure the client is still alive.
	 */
	if (!(client->handle->is_alive())) {
		scds_syslog_debug(1,
			"Client %s is not alive: removing.\n", client->
			handle->get_callback_info()->get_key());
		(void) cur_clients->remove(client->handle->
		    get_callback_info()->get_key());
		delete client;
		return (0);
	}

	scds_syslog_debug(1, "Starting message delivery for client %s\n",
		client->handle->get_callback_info()->get_key());

	/* initialize the vars by which we keep track of how much we've sent */
	client->cur_bytes_sent = 0;
	const char *mystr = (const char *)(*(client->cur_msg));
	scds_syslog_debug(1, "will deliver %s\n", mystr);
	client->bytes_to_send = strlen(mystr);

	scds_syslog_debug(1, "bytes_to_send=%d\n", client->bytes_to_send);

	/* set up and start the connect on the socket */
	sc_callback_info *cb_info = const_cast<sc_callback_info *>(client->
	    handle->get_callback_info());
	client->sock = create_connected_nb_socket(cb_info);

	/*
	 * If we can't setup/start the connect, then give up for now and
	 * retry later.
	 */
	if (client->sock < 0) {
		/* something screwed up; add to retry queue */
		retry_enqueue(cur_clients, client, allset);
		return (0);
	}

	/* Set the timeout time, in case it fails to connect */
	client->connection_timeout = time(NULL) + client_timeout;

	/* we need to add this socket to ones on which we select */
	/* make lint happy */
	/*lint -e713 -e737 -e573 */
	FD_SET(client->sock, allset);
	/*lint +e713 +e737 +e573 */
	if (client->sock > maxfd) {
		maxfd = client->sock;
	}
	return (0);
}

/*
 * retrey_enqueue
 * --------------------
 * Add this client to the retry queue for retry_interval length of time.
 */
void evt_comm_module::retry_enqueue(
    string_hashtable_t<client_connect *> *cur_clients, client_connect *client,
    fd_set *allset)
{
	/* First clean up the current connection, if there was one */
	if (client->sock > 0) {
		/*
		 * Lint doesn't like the FD_*
		 * macros.  Suppress the errors.
		 */
		/*lint -e713 -e737 -e573 */
		FD_CLR(client->sock, allset);
		/*lint +e713 +e737 +e573 */
		(void) close(client->sock);
	}
	client->sock = 0;
	client->cur_bytes_sent = client->bytes_to_send = 0;

	/* increment the number of retries */
	client->num_retries++;
	if (client->num_retries >= num_retries) {
		/*
		 * we've exceeded our limit.
		 * Remove this one from the list and
		 * mark the client as unreachable.
		 */
		(void) cur_clients->remove(client->handle->
		    get_callback_info()->get_key());

		scds_syslog_debug(1, "Client %s unreachable: deleting.\n",
		    client->handle->get_callback_info()->get_key());

		/*
		 * Remove the client from the cache and ccr.
		 * what to do if it fails ???
		 */
		(void) client->handle->remove_client();
		/* delete does all the cleanup, including closing the socket */
		delete (client);

		return;
	}

	/*
	 * The next_retry_time is the current time plus the time we're
	 * supposed to wait between tries (the retry_interval).
	 */
	client->next_retry_time = time(NULL) + retry_interval;
	client->connection_timeout = 0;

	scds_syslog_debug(1, "Client %s added to retry_queue\n",
		client->handle->get_callback_info()->get_key());

	/*
	 * Add the client to the retry queue
	 * what can we do if it fails ???
	 */
	(void) retry_queue.enqueue(client);
}

/*
 * Next we check the retry_queue, to see if the wait time
 * has expired for any of the clients.
 */
void evt_comm_module::process_retry_queue(
    string_hashtable_t<client_connect *> *cur_clients, fd_set *allset,
    int &maxfd)
{
	const client_connect *client_peek;
	client_connect *client;
	time_t cur_time = time(NULL);

	scds_syslog_debug(1, "evt send thread: checking retry queue\n");
	/*
	 * We look through the queue, removing and starting retries for
	 * all clients whose retry time has been passed.
	 */
	while (true) {
		client_peek = retry_queue.peek();
		if (client_peek != NULL && client_peek->next_retry_time
		    <= cur_time) {
			client = retry_queue.dequeue();
			scds_syslog_debug(1,
				"Client %s removed from retry_queue.\n",
			    client->handle->get_callback_info()->get_key());

			/* returns only 0 */
			(void) initiate_msg_delivery(cur_clients, client,
			    allset, maxfd);
		} else {
			break;
		}
	}
	/*
	 * Now we look through the queue for any clients that are not
	 * alive any more.
	 */
	SList<client_connect> *client_list = retry_queue.get_list();
	client_list->atfirst();
	while ((client = client_list->get_current()) != NULL) {
		client_list->advance();
		/*
		 * Make sure the client is still alive.
		 */
		if (!(client->handle->is_alive())) {
			scds_syslog_debug(1,
			    "Client %s is not alive: removing from retry "
			    "queue and from hashtable\n", client->
			    handle->get_callback_info()->get_key());
			/* remove from the hashtable */
			(void) cur_clients->remove(client->handle->
			    get_callback_info()->get_key());
			/* remove from the retry_queue */
			(void) client_list->erase(client);
			delete client;
		}
	}
}


/*
 * check_ready_connections
 * --------------------------
 * Iterates through each client_connect in the conns hashtable, checking
 * if its socket has been set in the wset.  If so, uses getsockopt to
 * find out if there is an error on the socket, or if it is really ready
 * for writing.  If there's an error, call retry_enqueue to try again later.
 * If not, check if we've finished writing (in the
 * client_connect::bytes_to_write and bytes_written fields).  If we're
 * done writing the message, call initiaget_msg_delivery to start the next
 * one.  Otherwise, write some of the message.
 */
void evt_comm_module::check_ready_connections(const fd_set &wset,
    fd_set *allset, int &maxfd, string_hashtable_t<client_connect *> *conns,
    int nready)
{
	int error, len;
	sc_syslog_msg_handle_t handle;
	client_connect *cur_connect;
	int byteswritten;
	int err;

	/*
	 * Iterate through the hashtable, checking if each
	 * connection is ready for reading.
	 */
	string_hashtable_t<client_connect *>::iterator hash_it(*conns);

	for (cur_connect = hash_it.get_current(); (cur_connect != NULL) &&
	    (nready > 0); cur_connect = hash_it.get_current()) {

		/* advance the iterator */
		hash_it.advance();

		/* Check if this client is ready to write */

		/*
		 * Lint doesn't like the FD_*
		 * macros.  Suppress the errors.
		 */
		/*lint -e713 -e737 -e573 */
		if (FD_ISSET(cur_connect->sock, &wset)) {
			/*lint +e713 +e737 +e573 */
			/* decrement our counter */
			nready--;

			scds_syslog_debug(1, "Found %s ready to write\n",
			    cur_connect->handle->get_callback_info()->
			    get_key());

			/* check for error conditions */
			error = 0;
			len = sizeof (int);
			if (getsockopt(cur_connect->sock, SOL_SOCKET, SO_ERROR,
			    &error, &len) != 0 || error != 0) {
				/*
				 * If we got an error value from the call,
				 * use it.  Otherwise, fetch errno.
				 */
				if (error == 0) {
					err = errno;
				} else {
					err = error;
				}
				// syslog something
				(void) sc_syslog_msg_initialize(&handle,
				    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
				//
				// SCMSGS
				// @explanation
				// The cl_apid received the following error
				// while trying to deliver an event to a CRNP
				// client. This error probably represents a
				// CRNP client error or temporary network
				// congestion.
				// @user_action
				// No action required, unless the problem
				// persists (ie. there are many messages of
				// this form). In that case, examine other
				// syslog messages occurring at about the same
				// time to see if the problem can be
				// identified. Save a copy of the
				// /var/adm/messages files on all nodes and
				// contact your authorized Sun service
				// provider for assistance in diagnosing and
				// correcting the problem.
				//
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE, "getsockopt: %s",
				    strerror(err));
				sc_syslog_msg_done(&handle);

				/*
				 * if there's an error, we move to retry
				 * queue.
				 */
				retry_enqueue(conns, cur_connect, allset);
				continue;
			}

			/*
			 * See if we're done writing.  We're done if the
			 * bytes to send is equal to the bytes_sent
			 */

			scds_syslog_debug(1,
				"cur_bytes_sent=%d bytes_to_send=%d\n",
			    cur_connect->cur_bytes_sent,
			    cur_connect->bytes_to_send);

			if (cur_connect->cur_bytes_sent >=
			    cur_connect->bytes_to_send) {

				scds_syslog_debug(1,
					"Successfully sent msg to %s\n",
				    cur_connect->handle->get_callback_info()->
				    get_key());

				/* we're done */
				/*
				 * Lint doesn't like the FD_*
				 * macros.  Suppress the errors.
				 */
				/*lint -e713 -e737 -e573 */
				FD_CLR(cur_connect->sock, allset);
				/*lint +e713 +e737 +e573 */
				(void) close(cur_connect->sock);
				cur_connect->sock = 0;

				/* we don't need the message anymore */
				cur_connect->cur_msg->release_ref();
				cur_connect->cur_msg = NULL;

				/*
				 * Start the next message send.
				 * We can ignore the return value, because it
				 * always returns 0.
				 */
				(void) initiate_msg_delivery(conns,
				    cur_connect, allset, maxfd);
				continue;
			}

			/* do the write */
			byteswritten = write(cur_connect->sock,
			    (const char *)(*(cur_connect->cur_msg)) +
			    cur_connect->cur_bytes_sent,
			    cur_connect->bytes_to_send -
			    cur_connect->cur_bytes_sent);

			scds_syslog_debug(1, "evt send_thr: wrote %d to %s\n",
			    byteswritten, cur_connect->handle->
			    get_callback_info()->get_key());

			/* Check for an error condition */
			if (byteswritten == -1) {
				err = errno;
				(void) sc_syslog_msg_initialize(&handle,
				    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
				//
				// SCMSGS
				// @explanation
				// The cl_apid experienced the specified error
				// when attempting to write to a file
				// descripter. If this error occurs during
				// termination of the daemon, it may cause the
				// cl_apid to fail to shutdown properly (or at
				// all). If it occurs at any other time, it is
				// probably a CRNP client error.
				// @user_action
				// Examine other syslog messages occurring at
				// about the same time to see if the problem
				// can be identified. Save a copy of the
				// /var/adm/messages files on all nodes and
				// contact your authorized Sun service
				// provider for assistance in diagnosing and
				// correcting the problem.
				//
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE, "write: %s", strerror(err));
				sc_syslog_msg_done(&handle);

				retry_enqueue(conns, cur_connect, allset);
			} else {
				/*
				 * we know byteswritten can't be -1 at this
				 * point.
				 */
				cur_connect->cur_bytes_sent += (uint_t)
				    byteswritten;

				/*
				 * Now we just wait for the write to go
				 * through so we can write again.
				 *
				 * Reset the next connection_timeout value.
				 */
				cur_connect->connection_timeout = time(NULL) +
				    client_timeout;
			}
		} /* if (FD_ISSET(...)) */
	} /* for */
}

/*
 * Iterate through the list of clients in the cur_clients table, checking
 * if any of them that have active connections have a connection_timeout
 * value that is past the current time.  If so, we time out the connection
 * and add the client to the retry queue (counting this timeout as
 * a connection failure).
 *
 * We also find the lowest, non-zero, connection_timeout field of all
 * the clients, and return it.
 */
time_t evt_comm_module::check_timedout_clients(
    string_hashtable_t<client_connect *> *cur_clients, fd_set *allset)
{
	client_connect *cur_connect;
	time_t lowest_time = 0;
	time_t cur_time = time(NULL);

	/*
	 * Iterate through the hashtable, checking if each
	 * connection should be timed out.
	 */
	string_hashtable_t<client_connect *>::iterator hash_it(*cur_clients);

	for (cur_connect = hash_it.get_current(); cur_connect != NULL;
	    cur_connect = hash_it.get_current()) {

		/* advance the iterator */
		hash_it.advance();

		/*
		 * Check if this client should be timed out.
		 * Note that a client with a connection_timeout of 0 is
		 * not currently active (is on the retry-queue).
		 */
		if (cur_connect->connection_timeout != 0) {
			if (cur_connect->connection_timeout <= cur_time) {

				/* This connection has timed out */

				scds_syslog_debug(1, "evt send_thr: client "
				    "%s connection timed out.  Adding to "
				    "retry_queue\n",
				    cur_connect->handle->get_callback_info()->
				    get_key());

				retry_enqueue(cur_clients, cur_connect,
				    allset);
				//
				// Note that 0 is never a valid value for
				// a connection_timeout field at this point
				// (we've already ruled it out).  Thus,
				// lowest_time will only be 0 before we've
				// set it to any connection_timeout.  We
				// check for that case before checking of
				// connection_timeout is lower than
				// lowest_time.
				//
			} else if (lowest_time == 0 ||
			    cur_connect->connection_timeout < lowest_time) {
				lowest_time = cur_connect->connection_timeout;
			}
		}
	} /* for */
	return (lowest_time);
}


/*
 * define/initialize the static members
 */
evt_comm_module *evt_comm_module::the_evt_comm_module = NULL;
pthread_t evt_comm_module::send_thr;


/*
 * ctor/dtor
 * -------------
 * Note that these are private, so ctor only called from initialize, and
 * dtor only from shutdown.
 *
 * Constructor initializes the mutex and condition variable,
 * and saves the initial parameters.
 *
 * Exits the program if anything goes wrong.
 */
evt_comm_module::evt_comm_module(int retries_in, int retry_interval_in,
    int timeout_in)
{
	sc_syslog_msg_handle_t handle;
	int err;

	num_retries = retries_in;
	retry_interval = retry_interval_in;
	client_timeout = timeout_in;

	/*
	 * Check the status of the queues
	 */
	if (staging_queue.get_construct_status() != 0 ||
	    retry_queue.get_construct_status() != 0) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Error: low memory");
		sc_syslog_msg_done(&handle);
		exit(1);
	}

	/*
	 * Create the mutex.
	 */
	if ((err = pthread_mutex_init(&staging_lock, NULL)) != 0) {
		char *err_str = strerror(err);
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The cl_apid was unable to initialize a synchronization
		// object, so it was not able to start-up. The error message
		// is specified.
		// @user_action
		// Save a copy of the /var/adm/messages files on all nodes and
		// contact your authorized Sun service provider for assistance
		// in diagnosing and correcting the problem.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "pthread_mutex_init: %s", err_str ? err_str : "");
		sc_syslog_msg_done(&handle);
		exit(1);
	}

	/*
	 * Create the pipe
	 */
	if (pipe(signal_pipe) != 0) {
		err = errno;
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "pipe: %s", strerror(err));
		sc_syslog_msg_done(&handle);
		exit(1);
	}
	shutdown_send = false;
}

evt_comm_module::~evt_comm_module()
{
	(void) pthread_mutex_destroy(&staging_lock);

	(void) close(signal_pipe[1]);
	(void) close(signal_pipe[0]);
}


/*
 * Wrapper function for thread creation.
 * It gets a reference to the one evt_comm_module, and
 * calls the method on that object that we really want to run.
 *
 * Defined with "C" linkage becasue it's called by a C function.
 */

extern "C" void send_evt_thread_wrapper(void *)
{
	evt_comm_module::the()->send_thread();
}

int evt_comm_module::initialize(int retries, int retry_interval_in,
    int client_timeout_in)
{
#ifdef _FAULT_INJECTION
	void		*f_argp;
	uint32_t	f_argsize;
	if (fault_triggered(FAULTNUM_CLAPI_EVT_COMM_INITIALIZE_R,
		&f_argp, &f_argsize)) {
		ASSERT(f_argsize == sizeof (int));
		return (*((int *)f_argp));
	}
#endif

	FAULTPT_CLAPI(FAULTNUM_CLAPI_EVT_COMM_INITIALIZE_B,
		FaultFunctions::generic);

	sc_syslog_msg_handle_t handle;

	scds_syslog_debug(1, "Initializing evt_comm_module\n");

	/* initialize should only be called once */
	CL_PANIC(the_evt_comm_module == NULL);

	/* create the one instance of evt_comm_module */
	the_evt_comm_module = new evt_comm_module(retries, retry_interval_in,
	    client_timeout_in);
	if (the_evt_comm_module == NULL) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Error: low memory");
		sc_syslog_msg_done(&handle);
		return (ENOMEM);
	}

	/*
	 * Spawn the thread in the evt_comm module.  When we
	 * create it, have it start with the wrapper function
	 * defined above, because we can't have a C function (pthread_create)
	 * calling a C++ method on an object, because it doesn't know how
	 * to pass the this pointer properly.
	 *
	 */
	int ret;
	if ((ret = pthread_create(&send_thr, NULL,
	    (void *(*)(void *))send_evt_thread_wrapper, NULL)) != 0) {
		char *err_str = strerror(ret);
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "pthread_create: %s", err_str ? err_str : "");
		sc_syslog_msg_done(&handle);
		return (ret);
	}

	return (0);
}

void evt_comm_module::check_remove_client()
{
	write_signal_pipe();
	scds_syslog_debug(1, "In evt_comm_module::check_remove_client\n");
}

void evt_comm_module::write_signal_pipe()
{
	sc_syslog_msg_handle_t handle;

	scds_syslog_debug(1, "Writing to signal pipe\n");
	if (write(signal_pipe[1], "\n", 1) <= 0)  {
		char *err_str = strerror(errno);
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "write: %s", err_str ? err_str : "");
		sc_syslog_msg_done(&handle);

		scds_syslog_debug(1, "Failed to write to signal pipe\n");

		/*
		 * Sleep and retry once, in case it was temporary
		 */
		(void) sleep(2);
		(void) write(signal_pipe[1], "\n", 1);
	}
}

/*
 * shutdown
 * -------------
 * Set the shutdown_send boolean to true, and
 * then wait for the thread to finish.
 */
void evt_comm_module::shutdown()
{
	scds_syslog_debug(1, "Shutting down evt_comm_module\n");
	/* terminate the thread */

	CL_PANIC(pthread_mutex_lock(&staging_lock) == 0);
	shutdown_send = true;
	write_signal_pipe();
	CL_PANIC(pthread_mutex_unlock(&staging_lock) == 0);

	// wait for it to exit
	(void) pthread_join(send_thr, NULL);

	// delete ourselves
	delete the_evt_comm_module;
	the_evt_comm_module = NULL;
}

evt_comm_module *evt_comm_module::the()
{
	CL_PANIC(the_evt_comm_module != NULL);
	return (the_evt_comm_module);
}

/*
 * reload
 * ---------
 * This method can be called to change the parameters specified
 * originally in the initialize method.
 */
int evt_comm_module::reload(int retries_in, int retry_interval_in,
    int timeout_in)
{
	scds_syslog_debug(1, "in evt_comm_module::reload\n");

	/* no need for locks */
	num_retries = retries_in;
	retry_interval = retry_interval_in;
	client_timeout = timeout_in;
	return (0);
}


/*
 * send_message
 * ----------------
 * Just puts the message on the staging_queue and returns.
 */
int evt_comm_module::send_message(const char *msg,
    SList<caapi_client_handle> *handles)
{
	sc_syslog_msg_handle_t handle;

	comm_req *new_req = new comm_req(msg, handles);
	if (new_req == NULL || new_req->get_construct_status() != 0) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Error: low memory");
		sc_syslog_msg_done(&handle);
		return (ENOMEM);
	}
	CL_PANIC(pthread_mutex_lock(&staging_lock) == 0);
	scds_syslog_debug(1, "Enqueueing com_req on staging queue...\n");
	int res = staging_queue.enqueue(new_req);
	write_signal_pipe();
	CL_PANIC(pthread_mutex_unlock(&staging_lock) == 0);
	return (res);
}

/*
 * send_message
 * ----------------
 * Just puts the message on the staging_queue and returns.
 */
int evt_comm_module::send_message(const char *msg,
    caapi_client_handle *cli_handle)
{
	sc_syslog_msg_handle_t handle;

	comm_req *new_req = new comm_req(msg, cli_handle);
	if (new_req == NULL || new_req->get_construct_status() != 0) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Error: low memory");
		sc_syslog_msg_done(&handle);
		return (ENOMEM);
	}
	CL_PANIC(pthread_mutex_lock(&staging_lock) == 0);
	scds_syslog_debug(1, "Enqueueing com_req on staging queue...\n");
	int res = staging_queue.enqueue(new_req);
	write_signal_pipe();
	CL_PANIC(pthread_mutex_unlock(&staging_lock) == 0);
	return (res);
}


comm_req::comm_req(const char *msg_in, caapi_client_handle *handle_in)
{
#ifdef _FAULT_INJECTION
	void		*f_argp;
	uint32_t	f_argsize;
	if (fault_triggered(FAULTNUM_CLAPI_EVT_COMM_REQ_R,
		&f_argp, &f_argsize)) {
		ASSERT(f_argsize == sizeof (int));
		status = *((int *)f_argp);
		return;
	}
#endif
	status = 0;
	/* lint doesn't understand templates sometimes */
	/*CSTYLED */
	if ((handles = new SList<caapi_client_handle>) == NULL) {
		status = ENOMEM;
		return;
	}
	if ((msg = new smart_string(msg_in)) == NULL) {
		status = ENOMEM;
		return;
	}

	/* add the  one handle to the list */
	handles->append(handle_in);
}

comm_req::comm_req(const char *msg_in, SList<caapi_client_handle> *handles_in)
{
	status = 0;
	/* lint doesn't understand templates sometimes */
	/*CSTYLED */
	if ((handles = new SList<caapi_client_handle>) == NULL) {
		status = ENOMEM;
		return;
	}
	if ((msg = new smart_string(msg_in)) == NULL) {
		status = ENOMEM;
		return;
	}

	/* copy all the handles over */
	caapi_client_handle *one_handle;
	handles_in->atfirst();
	while ((one_handle = handles_in->get_current()) != NULL) {
		handles_in->advance();
		handles->append(one_handle);
	}
}

comm_req::~comm_req()
{
	if (msg != NULL) {
		msg->release_ref();
	}
	/* we know release_ref frees the string if need be */
	msg = NULL; /*lint !e423 */
	if (handles) {
		/*
		 * Doesn't delete the objects, just
		 * removes them
		 */
		handles->erase_list();
		delete handles;
	}
}

client_connect::client_connect(caapi_client_handle *handle_in,
    smart_string *msg_in)
{
#ifdef _FAULT_INJECTION
	void		*f_argp;
	uint32_t	f_argsize;
	if (fault_triggered(FAULTNUM_CLAPI_EVT_COMM_CLIENT_CONN_R,
		&f_argp, &f_argsize)) {
		ASSERT(f_argsize == sizeof (int));
		status = *((int *)f_argp);
		return;
	}
#endif
	status = 0;

	sock = 0;
	handle = handle_in;
	cur_msg = NULL;
	cur_bytes_sent = 0;
	bytes_to_send = 0;

	num_retries = 0;
	next_retry_time = 0;
	connection_timeout = 0;

	if (msgs.get_construct_status() != 0) {
		status = msgs.get_construct_status();
		return;
	}
	(void) msgs.enqueue(msg_in);
}

/* Lint doesn't understand the smart_string */
/*lint -e1740 */
client_connect::~client_connect()
{

	if (sock != 0) {
		(void) close(sock);
	}

	delete handle;

	/* release references for all smart strings */
	if (cur_msg) {
		cur_msg->release_ref();
	}
	/* we know release_ref frees the string if need be */
	cur_msg = NULL; /*lint !e423 */
	smart_string *cur_str;
	while ((cur_str = msgs.dequeue()) != NULL) {
		cur_str->release_ref();
	}
}
/*lint +e1740 */
