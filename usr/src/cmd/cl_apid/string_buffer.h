/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_STRING_BUFFER_H_
#define	_STRING_BUFFER_H_

#pragma ident	"@(#)string_buffer.h	1.6	08/05/20 SMI"

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

/*
 * class string_buffer
 * ----------------------
 * Provides a growable string, useful for concatenating multiple strings.
 *
 * Sample use follows:
 *
 * string_buffer str_buf;
 * int sock;
 * char buf[1024];
 *
 * // set up socket for reading
 *
 * while((size = read(sock, buf, 1024)) > 0) {
 * buf[size] = '\0';
 * str_buf.append("%s", buf);
 * }
 *
 * printf("Read: %s\n", str_buf.get_buf());
 *
 */
class string_buffer {
public:
	string_buffer();
	~string_buffer();

	/*
	 * append
	 * ---------
	 * Takes formatted string, as in printf, plus format args,
	 * and appends it to the buffer, filling in the formatting.
	 *
	 * Returns the number of characters written, or -1 on error.
	 */
	int append(const char *format, /* args */ ...);

	/*
	 * clear
	 * -------
	 * Frees the memory and starts with an empty string.
	 */
	void clear();

	/*
	 * get_buf
	 * ----------
	 * Returns the string that has been built so far.
	 * Clients should make a copy of the string to save for future use,
	 * as it will be deleted by this object's destructor.
	 */
	const char *get_buf() const;

private:
	char *buf;
	size_t allocated, current;

	static const int alloc_init;
};

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#endif /* _STRING_BUFFER_H_ */
