/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)cl_apid_main.cc	1.21	09/01/06 SMI"

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <signal.h>
#include <errno.h>
#include <string.h>
#include <sys/sc_syslog_msg.h>
#include <sys/sol_version.h>
#if SOL_VERSION >= __s10
#include <zone.h>
#endif

#include <rgm/libdsdev.h>

#include "xml_module.h"
#include "cl_apid_include.h"
#include "caapi_reg_cache.h"
#include "reg_comm_module.h"
#include "tcp_wrapper.h"
#include "smart_string.h"
#include "evt_comm_module.h"
#include "sc_event_cache.h"

typedef struct {
	char *catalog_path, *plugin_path;
	int max_clients, max_events, max_nvpairs;
	tcp_wrapper *wrapper;
	char *reg_host;
	int reg_port;
	int client_timeout;
	int client_retry_count;
	int	client_retry_interval;
} ext_props;

static int initialize_modules(ext_props *props);
static int read_ext_props(ext_props *props, bool at_start);
static void shutdown_modules(void);
static void make_daemon(void);
static void init_signal_handlers(void);
static void sig_handler(void *);
static void reload_configuration(void);

bool debug = false;

// Global struct to keep extenstion properties
ext_props g_props;

scds_handle_t scds_handle;

int
main(int argc, char **argv)
{
	sc_syslog_msg_handle_t handle;

	// set up the syslog tag
	(void) sc_syslog_msg_set_syslog_tag(SC_SYSLOG_CLAPI_CLAPID_TAG);

	char *progname = argv[0];

#if SOL_VERSION >= __s10
	zoneid_t zoneid;
	if ((zoneid = getzoneid()) < 0) {
		//
		// SCMSGS
		// @explanation
		// The cl_apid was unable to query the zoneid and hence
		// exited prematurely.
		// @user_action
		// Save a copy of the /var/adm/messages files on all nodes and
		// contact your authorized Sun service provider for assistance
		// in diagnosing and correcting the problem.
		//
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "fatal: getzoneid() failed. "
		    "Unable to verify privilege.");
		sc_syslog_msg_done(&handle);
		exit(1);
	}
	if (zoneid != 0) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The cl_apid can't be executed from a non-global zone.
		// @user_action
		// Run cl_apid only from the global zone.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "fatal: can't be run in a non-global "
		    "zone.");
		sc_syslog_msg_done(&handle);
		exit(1);
	}
#endif

	/*
	 * Check if the process was started by the superuser.
	 */
	if (getuid() != 0) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "fatal: must be superuser to start %s",
		    progname);
		sc_syslog_msg_done(&handle);
		exit(1);
	}

	// daemonize
	make_daemon();

	// we're the child

	// set up the signal handlers
	init_signal_handlers();

	// Process command line arguments
	if (scds_initialize(&scds_handle, argc, argv)
		!= SCHA_ERR_NOERR) {
		return (1);
	}

	// Zero out the global extension properties
	bzero(&g_props, sizeof (g_props));

	// read the extension properties
	if (read_ext_props(&g_props, true) != 0) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The cl_apid was unable to read the SUNW.Event resource
		// properties that it needs to run.
		// @user_action
		// Save a copy of the /var/adm/messages files on all nodes and
		// contact your authorized Sun service provider for assistance
		// in diagnosing and correcting the problem.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Error reading properties; exiting.");
		sc_syslog_msg_done(&handle);
		exit(1);
	}

	// initialize the modules
	if (initialize_modules(&g_props) != 0) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The cl_apid was unable to start-up. There should be other
		// error message with more detailed explanations of the
		// specific problems.
		// @user_action
		// Save a copy of the /var/adm/messages files on all nodes and
		// contact your authorized Sun service provider for assistance
		// in diagnosing and correcting the problem.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Error in initialization; exiting.");
		sc_syslog_msg_done(&handle);
		exit(1);
	}

	/* free the props memory */
	delete g_props.wrapper;

	// The modules initiate their own threads, so we can exit this thread
	pthread_exit(NULL);

	// to make lint happy...
	return (0);
}


/*
 * make_daemon
 * ----------------------
 * Create a daemon process and detach it from the controlling terminal.
 * Slightly modified from the rgm version in rgm_main.cc
 */
static void make_daemon(void)
{
	struct rlimit	rl;
	int	size;
	int	i;
	int	fd;
	pid_t	pd;
	int	err;
	pid_t	rc;
	sc_syslog_msg_handle_t handle;

	pd = fork();
	if (pd < 0) {
		err = errno;
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The cl_apid daemon was not able to fork a
		// process, possibly due to low swap space. The message
		// contains the system error. This can happen while the daemon
		// is starting up (during the node boot process), or when
		// executing a client call. If it happens when starting up,
		// the daemon does not come up. If it happens during a client
		// call, the server does not perform the action requested by
		// the client.
		// @user_action
		// Determine if the machine is running out of swap space. If
		// this is not the case, save the /var/adm/messages file.
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "fork: %s", strerror(err));
		sc_syslog_msg_done(&handle);
		exit(1);
	}

	/*
	 * parent exits after fork
	 */
	if (pd > 0)
		exit(0); /* parent */

	/*
	 * In the child, redirect stdin to /dev/null so that if
	 * the daemon reads from stdin, it doesn't block.
	 * Redirect stdout, stderr to /dev/console so that any
	 * output printed by the daemon goes somewhere.
	 */

	if ((fd = open("/dev/null", O_RDONLY)) == -1) {
		err = errno;
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		//
		// SCMSGS
		// @explanation
		// While starting up, the cl_apid daemon was not able to
		// open /dev/null. The message contains the system error. This
		// will prevent the daemon from starting on this node.
		// @user_action
		// Examine other syslog messages occurring at about the same
		// time to see if the problem can be identified. Save a copy
		// of the /var/adm/messages files on all nodes and contact
		// your authorized Sun service provider for assistance in
		// diagnosing and correcting the problem.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Unable to open /dev/null: %s", strerror(err));
		sc_syslog_msg_done(&handle);
		exit(1);
	}

	if (dup2(fd, 0) == -1)
		(void) close(fd);

	/*
	 * redirect stdout and stderr by opening /dev/console
	 * and duping to the appropriate descriptors.
	 */
	fd = open("/dev/console", O_WRONLY);
	if (fd == -1) {
		err = errno;
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		//
		// SCMSGS
		// @explanation
		// While starting up, the cl_apid daemon was not able to
		// open /dev/console. The message contains the system error.
		// This will prevent the daemon from starting on this node.
		// @user_action
		// Examine other syslog messages occurring at about the same
		// time to see if the problem can be identified. Save a copy
		// of the /var/adm/messages files on all nodes and contact
		// your authorized Sun service provider for assistance in
		// diagnosing and correcting the problem.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Unable to open /dev/console: %s", strerror(err));
		sc_syslog_msg_done(&handle);
		exit(1);
	}

	if (dup2(fd, 1) == -1 || dup2(fd, 2) == -1)
		(void) close(fd);

	/*
	 * child closes rest of file descriptors
	 */
	rl.rlim_max = 0;
	(void) getrlimit(RLIMIT_NOFILE, &rl);
	if ((size = (int)rl.rlim_max) == 0)
		exit(1);

	for (i = 3; i < size; i++)
		(void) close(i);

	/*
	 * child sets this process as group leader
	 */
	rc = setsid();
	if (rc == (pid_t)-1) {
		err = errno;
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The cl_apid was not able to set the
		// session id, and the system error is shown. An error message
		// is output to syslog.
		// @user_action
		// Save the /var/adm/messages file. Contact your authorized
		// Sun service provider to determine whether a workaround or
		// patch is available.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "setsid: %s", strerror(err));
		sc_syslog_msg_done(&handle);
		exit(1);
	}
}

/*
 * init_signal_handlers
 * ---------------------
 * Blocks all signals in this thread, which will be the parent of the
 * other threads, so that they will inherit the signal mask.
 *
 * Creates a thread to handle all the signals.  It runs the sig_handler
 * method below.
 *
 * If we fail in any of this, exit the daemon.
 */
static void init_signal_handlers(void)
{
	sc_syslog_msg_handle_t handle;

	// block all signals for all threads
	sigset_t sig_set;
	int err;

	if (sigfillset(&sig_set) != 0) {
		err = errno;
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "sigfillset: %s", strerror(err));
		sc_syslog_msg_done(&handle);
		exit(err);
	}

	if ((err = pthread_sigmask(SIG_BLOCK, &sig_set, NULL)) != 0) {
		char *err_str = strerror(err);
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "pthread_sigmask: %s", err_str ? err_str : "");
		sc_syslog_msg_done(&handle);
		exit(err);
	}

	// now spawn the thread that will actually handle the signals
	pthread_t sig_thr;
	if ((err = pthread_create(&sig_thr, NULL,
	    (void *(*)(void *))sig_handler, NULL)) != 0) {
		char *err_str = strerror(err);
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "pthread_create: %s", err_str ? err_str : "");
		sc_syslog_msg_done(&handle);
		exit(err);
	}
}


/*
 * sig_handler
 * --------------
 * Signal handler for the SIGHUP and SIGTERM signals.
 *
 * For SIGTERM, calls shutdown_modules, then exits the program.
 * For SIGHUP, calls reload_configuration, then continues.
 *
 * This function runs in a dedicated signal handling thread.  The thread
 * should be created with a signal mask blocking all signals.
 *
 * Uses an infinite loop around sigwait, to wait for all signals.
 * Handles TERM and HUP as described.  Exits on all others.
 *
 * If anything goes awry, exits immediately.
 */
static void sig_handler(void *)
{
	sc_syslog_msg_handle_t handle;

	// block all signals for all threads
	sigset_t sig_set;
	int err, sig;

	if (sigfillset(&sig_set) != 0) {
		err = errno;
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "sigfillset: %s", strerror(err));
		sc_syslog_msg_done(&handle);
		exit(err);
	}

	while (true) {
		if (sigwait(&sig_set, &sig) != 0) {
			err = errno;
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "sigwait: %s", strerror(err));
			sc_syslog_msg_done(&handle);
			exit(err);
		}

		switch (sig) {
		case SIGTERM:
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "fatal: received signal %d", sig);
			sc_syslog_msg_done(&handle);

			// Attempt to do an orderly shutdown
			shutdown_modules();

			scds_syslog_debug(1, "Competely done shutting done\n");
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
			//
			// SCMSGS
			// @explanation
			// The cl_apid daemon is shutting down normally due to
			// a SIGTERM.
			// @user_action
			// No action required.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "Successful shutdown; terminating daemon");
			sc_syslog_msg_done(&handle);

			// now exit
			_exit(1);
			break;
		case SIGHUP:
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
			//
			// SCMSGS
			// @explanation
			// The daemon indicated in the message tag (cl_apid)
			// has received a signal, possibly caused by an
			// operator-initiated kill(1) command. The signal is
			// ignored.
			// @user_action
			// The operator must use clnode and shutdown to take
			// down a node, rather than directly killing the
			// daemon.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "received signal %d", sig);
			sc_syslog_msg_done(&handle);
			reload_configuration();
			break;
		default:
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
			//
			// SCMSGS
			// @explanation
			// The cl_apid daemon is terminating abnormally due to
			// receipt of a signal.
			// @user_action
			// No action required.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "received signal %d: exiting", sig);
			sc_syslog_msg_done(&handle);
			_exit(1);
		}
	}
}

/*
 * read_ext_props
 * --------------
 * Reads the extension properties from the resource using scha_resource_get.
 * Returns 0 on success, failure code otherwise.
 * Assumes props is not NULL.
 */
static int read_ext_props(ext_props *props, bool at_start)
{
	sc_syslog_msg_handle_t handle;
	scha_extprop_value_t	*xprop = NULL;
	scha_extprop_value_t	*xprop2 = NULL;
	scds_port_list_t	*portlist;
	scds_net_resource_list_t	*snrlp;
	scha_err_t	e;

	e = scds_get_ext_property(scds_handle, "Max_clients",
		SCHA_PTYPE_INT, &xprop);
	if (e != SCHA_ERR_NOERR) {
		//
		// SCMSGS
		// @explanation
		// API operation has failed in retrieving the cluster
		// property.
		// @user_action
		// For property name, check the syslog message. For more
		// details about API call failure, check the syslog messages
		// from other components.
		//
		scds_syslog(LOG_ERR, "Failed to retrieve the "
			"property %s: %s.", "Max_clients",
			scds_error_string(e));
		return (1);
	}
	props->max_clients = xprop->val.val_int;
	scds_free_ext_property(xprop);

	props->max_events = 20;

	props->max_nvpairs = 10;

	e = scds_get_ext_property(scds_handle, "Client_timeout",
		SCHA_PTYPE_INT, &xprop);
	if (e != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to retrieve the "
			"property %s: %s.", "Client_timeout",
			scds_error_string(e));
		return (1);
	}
	props->client_timeout = xprop->val.val_int;
	scds_free_ext_property(xprop);

	e = scds_get_ext_property(scds_handle, "Client_retry_count",
		SCHA_PTYPE_INT, &xprop);
	if (e != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to retrieve the "
			"property %s: %s.", "Client_retry_count",
			scds_error_string(e));
		return (1);
	}
	props->client_retry_count = xprop->val.val_int;
	scds_free_ext_property(xprop);

	e = scds_get_ext_property(scds_handle, "Client_retry_interval",
		SCHA_PTYPE_INT, &xprop);
	if (e != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to retrieve the "
			"property %s: %s.", "Client_retry_interval",
			scds_error_string(e));
		return (1);
	}
	props->client_retry_interval = xprop->val.val_int;
	scds_free_ext_property(xprop);

	if (at_start) {
		e = scds_get_port_list(scds_handle, &portlist);
		if (e != SCHA_ERR_NOERR) {
			scds_syslog(LOG_ERR, "Failed to retrieve the "
			    "property %s: %s.", "Port_list",
			    scds_error_string(e));
			return (1);
		}
		if (portlist == NULL || portlist->num_ports != 1) {
			//
			// SCMSGS
			// @explanation
			// The cl_apid encountered an invalid property value.
			// If it is trying to start, it will terminate. If it
			// is trying to reload the properties, it will use the
			// old properties instead.
			// @user_action
			// Save a copy of the /var/adm/messages files on all
			// nodes and contact your authorized Sun service
			// provider for assistance in diagnosing and
			// correcting the problem.
			//
			scds_syslog(LOG_ERR, "Invalid value for "
			    "property %s.", "Port_list");
			return (1);
		}
		props->reg_port = portlist->ports[0].port;
		scds_free_port_list(portlist);

		e = scds_get_rs_hostnames(scds_handle, &snrlp);
		if (e != SCHA_ERR_NOERR) {
			scds_syslog(LOG_ERR, "Failed to retrieve the "
			    "property %s: %s.", "Network_resources_used",
			    scds_error_string(e));
			return (1);
		}
		if (snrlp == NULL || snrlp->num_netresources == 0) {
			//
			// SCMSGS
			// @explanation
			// The cl_apid encountered an invalid property value.
			// If it is trying to start, it will terminate. If it
			// is trying to reload the properties, it will use the
			// old properties instead.
			// @user_action
			// Save a copy of the /var/adm/messages files on all
			// nodes and contact your authorized Sun service
			// provider for assistance in diagnosing and
			// correcting the problem.
			//
			scds_syslog(LOG_ERR, "No network address resources "
			    "in resource group.");
			return (1);
		}
		if (props->reg_host)
			free(props->reg_host);
		props->reg_host = strdup(snrlp->netresources[0].hostnames[0]);
		if (props->reg_host == NULL) {
			//
			// SCMSGS
			// @explanation
			// Need explanation of this message!
			// @user_action
			// Need a user action for this message.
			//
			scds_syslog(LOG_ERR, "No memory.");
			return (1);
		}
		scds_free_net_list(snrlp);
	}


	props->catalog_path =
	    "/usr/cluster/lib/sc/events/dtds/crnp_catalog.xml";

	props->plugin_path = "/usr/cluster/lib/sc/events/modules";


	e = scds_get_ext_property(scds_handle, "Allow_hosts",
	    SCHA_PTYPE_STRINGARRAY, &xprop);
	if (e != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to retrieve the "
		    "property %s: %s.", "Allow_hosts",
		    scds_error_string(e));
		return (1);
	}
	scds_syslog_debug(2, "Allow_hosts = [%d]",
	    xprop->val.val_strarray->array_cnt);

	e = scds_get_ext_property(scds_handle, "Deny_hosts",
	    SCHA_PTYPE_STRINGARRAY, &xprop2);
	if (e != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to retrieve the "
		    "property %s: %s.", "Deny_hosts",
		    scds_error_string(e));
		return (1);
	}
	scds_syslog_debug(2, "Deny_hosts = [%d]",
	    xprop2->val.val_strarray->array_cnt);

	// XXX Handle additional allow/deny directives
	props->wrapper = new tcp_wrapper((const char *const *)
	    xprop->val.val_strarray->str_array,
	    xprop->val.val_strarray->array_cnt,
	    (const char *const *)
	    xprop2->val.val_strarray->str_array,
	    xprop2->val.val_strarray->array_cnt);
	if (props->wrapper == NULL ||
	    props->wrapper->get_construct_status() == ENOMEM) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Error: low memory");
		sc_syslog_msg_done(&handle);
		return (ENOMEM);
	} else if (props->wrapper->get_construct_status() != 0) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The allow_hosts or deny_hosts for the CRNP service contains
		// an error. This error may prevent the cl_apid from starting
		// up.
		// @user_action
		// Examine other syslog messages occurring at about the same
		// time to see if the problem can be identified. Save a copy
		// of the /var/adm/messages files on all nodes and contact
		// your authorized Sun service provider for assistance in
		// diagnosing and correcting the problem.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Error with allow_hosts or deny_hosts");
		sc_syslog_msg_done(&handle);
		return (1);
	}
	scds_free_ext_property(xprop);
	scds_free_ext_property(xprop2);

	return (0);
}

/*
 * initialize_modules
 * ----------------------
 * Calls the static initialize method on each of the cl_apid modules, passing
 * the properties from props as required.  Assumes props is not NULL, and
 * that all properties are filled in properly.  Returns 0 if all modules
 * initialize successfully, error code otherwise.
 */
static int initialize_modules(ext_props *props)
{
	int res;

	/*
	 * The order of module initialization is very important.
	 * The caapi_reg_cache, evt_comm_module, and snapshot_module
	 * all must be initialized before the xml module is
	 * initialized, because the xml module will generate the
	 * snapshot events for all registered clients, using the
	 * cache, evt_comm, and snapshot modules.
	 */

	scds_syslog_debug(1, "About to init cache module\n");

	// initialize cache module
	if ((res = caapi_reg_cache::initialize((uint_t)props->max_clients))
	    != 0) {
		return (res);
	}
	scds_syslog_debug(1, "Initialized cache_module\n");

	// initialize event communication module
	if ((res = evt_comm_module::initialize(props->client_retry_count,
	    props->client_retry_interval, props->client_timeout)) != 0) {
		return (res);
	}

	// initialize event_cache module
	if ((res = sc_event_cache::initialize(props->plugin_path)) != 0) {
		return (res);
	}

	// initialize xml module
	if ((res = xml_module::initialize(props->catalog_path)) != 0) {
		return (res);
	}

	// initialize the registration communication module
	if ((res = reg_comm_module::initialize(props->reg_host,
	    props->reg_port, *(props->wrapper), props->client_timeout)) != 0) {
		return (res);
	}

	// We can free the wrapper now, because the constructor made
	// a deep copy of it.
	delete (props->wrapper);
	props->wrapper = NULL;

	// Initialize the event reception module
	if ((res = evt_receive_initialize()) != 0) {
		return (res);
	}

	// now get our initial events, _after_ we're ready to receive them
	sc_event_cache::the().request_events();

	return (0);
}

/*
 * shutdown
 * ----------
 * Attemps to shutdown each module.  Calls the shutdown method on each
 * module.
 */
static void shutdown_modules(void)
{
	scds_syslog_debug(1, "Attempting a gracefull shutdown\n");

	/*
	 * The order of module shutdown is very important.
	 * The modules are shut down in the reverse order of their
	 * initialization.
	 */

	// Shutdown the event reception module
	evt_receive_shutdown();

	// the xml and reg_comm modules need to be shut down
	// simultaneously, because they depend on each other.
	//
	// start the shutdown of the registration communication module
	reg_comm_module::the()->shutdown_start();

	// start the shutdown of the xml module
	xml_module::the()->shutdown_start();

	// the sc_event_cache is needed by the xml module, but relies on
	// the evt_comm_module, so it must be shutdown now, between the
	// two.
	sc_event_cache::the().shutdown();

	// the evt_comm_module need to be shut down now, before the
	// xml_module is deleted, because it depends on the xml module
	// also.

	// shutdown event communication module
	evt_comm_module::the()->shutdown();

	// finish shutting down the two modules
	reg_comm_module::the()->shutdown_finish();
	xml_module::the()->shutdown_finish();

	// shutdown snapshot module
	// *
	// snapshot_module::the()->shutdown();
	// *

	// shutdown cache module
	caapi_reg_cache::the().shutdown();
}

/*
 * reload_configuration
 * ---------------------
 * Re-reads the extension properties from the resource type and
 * reconfigures the appropriate modules.  Tells the snapshot_gen module
 * to reload its shared object libraries.
 */
static void reload_configuration(void)
{
	sc_syslog_msg_handle_t handle;

	// reread the extension properties
	if (read_ext_props(&g_props, false) != 0) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The cl_apid was unable to read all the properties
		// correctly. It will use the versions of the properties that
		// it read previously.
		// @user_action
		// Save a copy of the /var/adm/messages files on all nodes and
		// contact your authorized Sun service provider for assistance
		// in diagnosing and correcting the problem.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Error reading properties; using old properties");
		sc_syslog_msg_done(&handle);
		return;
	}

	if (evt_comm_module::the()->reload(g_props.client_retry_count,
	    g_props.client_retry_interval, g_props.client_timeout) != 0) {
		/* just continue; nothing to do */
	}

	if (reg_comm_module::the()->reload(*(g_props.wrapper),
	    g_props.client_timeout) != 0) {
		/* just continue; nothing to do */
	}
	// We can free the wrapper now, because the reg_comm module makes
	// a deep copy.
	delete (g_props.wrapper);
	g_props.wrapper = NULL;

	if (caapi_reg_cache::the().update_registration_parameters(
	    (uint_t)(g_props.max_clients)) != 0) {
		/* just continue; nothing to do */
	}

	/*
	 * reload the modules and re-request events
	 *
	 * Disable for now, until we implement the dynamic loading of
	 * event plugins.
	 */
	// sc_event_cache::the().reload_modules(g_props.plugin_path);

}
