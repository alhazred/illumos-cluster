/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)caapi_reg_cache.cc 1.27	08/05/20 SMI"
#include "caapi_reg_cache.h"
#include "caapi_client_handle.h"
#include "cl_apid_error.h"
#include "cl_apid_include.h"
#include <orb/fault/fault_injection.h>
#include <sys/sc_syslog_msg.h>


// XXX need to add more comments, check for lint and cstyle

// XXX implement DEBUG buffer the way it is implemented in rgm libs
//
// Initialize static instance of cappi_reg_cache
//
caapi_reg_cache *caapi_reg_cache::the_caapi_reg_cache = NULL;
//
// Implementation of caapi_reg_cache class
//
// constructor
//   no dynamically allocated  members, data members are intialized in
//   intialize() function
//
caapi_reg_cache::caapi_reg_cache():
		    MAXIMUM_REGISTRATION(0),
		    num_clients(0)
{
}
//
// creates a static instance of caapi_reg_cache
// calls the initialize_cache function to intialize all the data
// members
//
int
caapi_reg_cache::initialize(uint_t max_num_client_registration)
{
	sc_syslog_msg_handle_t handle;

#ifdef _FAULT_INJECTION
	void		*f_argp;
	uint32_t	f_argsize;
	if (fault_triggered(FAULTNUM_CLAPI_CACHE_INITIALIZE_R,
		&f_argp, &f_argsize)) {
		ASSERT(f_argsize == sizeof (int));
		return (*((int *)f_argp));
	}
#endif
	FAULTPT_CLAPI(FAULTNUM_CLAPI_CACHE_INITIALIZE_B,
		FaultFunctions::generic);

	ASSERT(the_caapi_reg_cache == NULL);
	the_caapi_reg_cache = new caapi_reg_cache();
	if (the_caapi_reg_cache == NULL) {

		// could not allocate memory
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Error: low memory");
		sc_syslog_msg_done(&handle);

		return (ENOMEM);
	}

	return (the_caapi_reg_cache->initialize_cache(
		    max_num_client_registration));
}

//
// function the creates a single instance of caapi_reg_cache
// calling function is supposed to call intialize function
// before calling this function
//
caapi_reg_cache&
caapi_reg_cache::the() {
	ASSERT(the_caapi_reg_cache != NULL);
	return (*the_caapi_reg_cache);
}


//
// Method to update the max_clients registration parameter
//
int
caapi_reg_cache::update_registration_parameters(
    uint_t max_total_clients)
{
	sc_syslog_msg_handle_t handle;

	//
	// If the max_total clients is less than the num_clients
	// registered then issue a warning, but go ahead and set the
	// new maximum anyway.
	//
	if (max_total_clients < the_caapi_reg_cache->get_num_clients()) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The cl_apid has received a change to the max_clients
		// property such that the number of current clients exceeds
		// the desired maximum.
		// @user_action
		// If desired, modify the max_clients parameter on the
		// SUNW.Event resource so that it is greater than the current
		// number of clients.
		//
		(void) sc_syslog_msg_log(handle, LOG_WARNING, MESSAGE,
		    "The new maximum number of clients <%d> is smaller than "
		    "the current number of clients <%d>.",
		    max_total_clients, the_caapi_reg_cache->get_num_clients());
		sc_syslog_msg_done(&handle);
	}
	registration_lck.lock();
	MAXIMUM_REGISTRATION = max_total_clients;
	registration_lck.unlock();

	return (0);
}


//
// initialize_cache initializes all the data members
//
int
caapi_reg_cache::initialize_cache(uint_t max_total_clients)
{
	int ret_value = 0;

#ifdef _FAULT_INJECTION
	void		*f_argp;
	uint32_t	f_argsize;
	if (fault_triggered(FAULTNUM_CLAPI_CACHE_INITIALIZE_CACHE_R,
		&f_argp, &f_argsize)) {
		ASSERT(f_argsize == sizeof (int));
		return (*((int *)f_argp));
	}
#endif

	FAULTPT_CLAPI(FAULTNUM_CLAPI_CACHE_INITIALIZE_CACHE_B,
		FaultFunctions::generic);

	// if caapi_reg_io init failure cant proceed
	if ((ret_value = caapi_reg_io_instance.initialize()) != 0) {
		//
		// Pass up the failure of the caapi_reg_io to initialize.
		//
		return (ret_value);
	}

	MAXIMUM_REGISTRATION = max_total_clients;

	// total number of clients registered
	num_clients = 0;
	//
	// lock both the registration_lck and mapping lock  to avoid
	// race conditions during initialization
	//

	registration_lck.lock();
	mapping_lck.lock();

	//
	// read_clients reads the caapi_reg ccr file and for each key/data
	// creates a client and adds client to client_list and updates
	// the event_mapping accordingly
	//
	ret_value =
	    caapi_reg_io_instance.read_clients(client_list, event_mapping);

	//
	// set the total number of clients to the number registered in
	// caapi_reg table
	//
	num_clients = client_list.count();

	mapping_lck.unlock();
	registration_lck.unlock();

	return (ret_value);
}

// destructor for caapi_reg_cache
//
caapi_reg_cache::~caapi_reg_cache() {
}

//
// shutdown function delete's the static instance of caapi_reg_cache
// and deletes all the dynamically allocated members
//
void
caapi_reg_cache::shutdown() {
	ASSERT(the_caapi_reg_cache == this);
	// ignoring return value of dispose casting to avoid lint warnings
	(void) event_mapping.dispose();
	client_list.dispose();
	delete the_caapi_reg_cache;
}

//
// This is the primary method for updating client information.
// This method is used for client to register, add/remove event
// and unregister itself. This method makes use of
// caapi_reg_io class for writing to caapi_reg file.
// this is responsible for updating the client_info list
// and the event-client mapping data struture
// only one thread is allowed to update client information
// two-phase commit is implemented , wherein in-memory strutures
// are changed according to client's registration info and then
// changes are updated in caapi_reg ccr file if this is succeeds then
// in-memory changes are commited else it is revoked
//
int
caapi_reg_cache::update_clients(SList<sc_registration_instance>
			    &reg_instance_list)
{
	int ret_value = 0; // initialize with success
	SList<caapi_client> updated_client_list;

	FAULTPT_CLAPI(FAULTNUM_CLAPI_UC_B,
		FaultFunctions::generic);

	registration_lck.lock();
	//
	// update_client_prepare makes a temporary changes to
	// in-memory client_list and mapping strutures such that
	// if updation to caapi_reg ccr fails this action can be revoked
	//

	// grab mapping lock()
	mapping_lck.lock();
	scds_syslog_debug(1, "\nentering update_clients_prepare: ");
	ret_value = update_client_prepare(reg_instance_list,
	    updated_client_list);
	mapping_lck.unlock();

	FAULTPT_CLAPI(FAULTNUM_CLAPI_UC_CALL_PREPARE_A,
	    FaultFunctions::generic);

	//
	// proceed changes to ccr only if the in-memory changes
	// succeeded, it could fail due to memory allocation
	// failure write the client registration into ccr file
	//
	if (ret_value == 0) {

		scds_syslog_debug(1, "\nupdating ccr table: ");
		ret_value = caapi_reg_io_instance.update_clients(client_list);

		FAULTPT_CLAPI(FAULTNUM_CLAPI_UC_CALL_UPDATE_CCR_A,
			FaultFunctions::generic);
	} else {
		//
		// if prepare fails then revoke any in-memory changes
		// made during this transaction
		//
		mapping_lck.lock();

		update_client_revoke(reg_instance_list,
		    updated_client_list, ret_value);


		mapping_lck.unlock();

		// unlock caapi_reg table access
		registration_lck.unlock();

		FAULTPT_CLAPI(FAULTNUM_CLAPI_UC_CALL_REVOKE_A,
			FaultFunctions::generic);

		return (ret_value);
	}
	//
	// if the ccr updation is success then proceed with commiting the
	// changes to in-memory structure else revoke the changes
	//
	mapping_lck.lock();

	if (ret_value == 0) {
		scds_syslog_debug(1, "\ncommiting ccr changes : ");
		update_client_commit(reg_instance_list,
			updated_client_list);

		FAULTPT_CLAPI(FAULTNUM_CLAPI_UC_CALL_COMMIT_A,
			FaultFunctions::generic);

	} else {
		update_client_revoke(reg_instance_list,
			updated_client_list, ret_value);

		FAULTPT_CLAPI(FAULTNUM_CLAPI_UC_CALL_REVOKE_A,
			FaultFunctions::generic);
	}

	// erase the elements of updated_client_list.
	updated_client_list.erase_list();


	mapping_lck.unlock();

	// unlock caapi_reg table access
	registration_lck.unlock();

	FAULTPT_CLAPI(FAULTNUM_CLAPI_UC_E,
		FaultFunctions::generic);

	return (ret_value);

}
//
// returns 0 if registration instance matches with any of the client
// in the in-memory client-list
//
int
caapi_reg_cache::find_client(caapi_client *&clientp,
			    sc_registration_instance &reg_instance)
{

	bool client_found = false;
	int ret_value = 0;
	//
	// find if the in coming request relates to any
	// client in the list
	//
	client_list.atfirst();
	while ((clientp = client_list.get_current()) != NULL) {
		client_list.advance();

		// returns true if some client matchs the
		// registration instance
		if (clientp->equals(reg_instance)) {
			client_found = true;
			break;
		}
	}

	// if no client matches the registration instance
	if (!client_found) {
		return (CLEP_ERR_UNKW);
	}
	return (ret_value);
}

//
// update_client_prepare iterates the sc_registration_instance and
// for each incoming request updates the in-memory client with new
// set of events or unregister's client. it also updates the
// reg_instance list with the associated client handle's
//
int
caapi_reg_cache::update_client_prepare(SList<sc_registration_instance>
			    &reg_instance_list,
			    SList<caapi_client> &updated_client_list)
{

	FAULTPT_CLAPI(FAULTNUM_CLAPI_UCP_B,
		FaultFunctions::generic);

#ifdef _FAULT_INJECTION
	void 		*f_argp;
	uint32_t	f_argsize;
	if (fault_triggered(FAULTNUM_CLAPI_UCP_R, &f_argp, &f_argsize)) {
		ASSERT(f_argsize == sizeof (int));
		return (*((int *)f_argp));
	}
#endif

	reg_type  client_reg_type;
	int ret_value = 0; // initialize with success
	sc_registration_instance *reg_instance = NULL;
	SList<sc_event> reg_event_list;
	SList<caapi_event> unreg_event_list;
	bool allocation_status;
	sc_syslog_msg_handle_t handle;

	caapi_client *clientp = NULL;
	caapi_client_handle *client_handlep = NULL;

	//
	// iterate through reg_instance_list and for
	// each registration retrive the client information
	// and make changes to client according to registration
	// request
	//

	reg_instance_list.atfirst();
	while ((reg_instance = reg_instance_list.get_current()) != NULL) {
		reg_instance_list.advance();

		client_reg_type = reg_instance->get_reg_type();
		switch (client_reg_type) {

		case  ADD_CLIENT:

			//
			// create a client with in_ccr status true
			// indicating this client detail should be added
			// caapi_reg ccr table
			//
			// check if this client exists in the in-memory
			// list if it exists then replace the old client with
			// new client
			//
			// XXX need to add this as of now it ignores
			// new client request and removes the registration
			// request making it IDEMPOTENT
			//

			// check if cache module has exceeded max registration
			if (num_clients >= MAXIMUM_REGISTRATION) {
				(void) sc_syslog_msg_initialize(&handle,
				    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
				//
				// SCMSGS
				// @explanation
				// The cl_apid refused a CRNP client
				// registration request because it would have
				// exceeded the maximum number of clients
				// allowed to register.
				// @user_action
				// If you wish to provide access to the CRNP
				// service for more clients, modify the
				// max_clients parameter on the SUNW.Event
				// resource.
				//
				(void) sc_syslog_msg_log(handle, LOG_WARNING,
				    MESSAGE, "Attempted client registration "
				    "would exceed the maximum clients "
				    "allowed: %d", MAXIMUM_REGISTRATION);
				sc_syslog_msg_done(&handle);

				reg_instance->set_reg_res(
				    CLEP_ERR_MAX_CLIENTS);
				continue;
			}

			if (find_client(clientp, *reg_instance) == 0) {
				// set a WARNING in client already registered
				reg_instance->set_reg_res(CLEP_WAR_EXISTS);
				continue;
			}
			clientp = new caapi_client(*reg_instance);
			if ((clientp == NULL) ||
			    (clientp->get_construct_status() != 0)) {
				(void) sc_syslog_msg_initialize(&handle,
				    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE, "Error: low memory");
				sc_syslog_msg_done(&handle);

				ret_value = ENOMEM;
				// set error code in the registration request
				reg_instance->set_reg_res(ret_value);
				delete clientp;
				return (ret_value);
			}

			// update the event mapping structure with new client
			if (event_mapping.update_mapping(ADD_CLIENT, clientp)) {

				//
				// return Mapping failed probably due to memory
				// allocation
				//
				// ENOMEM
				ret_value = ENOMEM;
				//
				// set the error code in the registration
				// request
				//
				reg_instance->set_reg_res(ret_value);
				return (ret_value);
			}

			FAULTPT_CLAPI(
				FAULTNUM_CLAPI_UCP_AC_UPDATE_M_A,
				FaultFunctions::generic);

			client_list.append(clientp, &allocation_status);
			if (!allocation_status) {
				(void) sc_syslog_msg_initialize(&handle,
				    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE, "Error: low memory");
				sc_syslog_msg_done(&handle);

				// ENOMEM
				ret_value = ENOMEM;
				// set the error code in the registration
				// request
				//
				reg_instance->set_reg_res(ret_value);
				return (ret_value);
			}

			FAULTPT_CLAPI(
				FAULTNUM_CLAPI_UCP_AC_UPDATE_CL_A,
				FaultFunctions::generic);

			// get client handle
			client_handlep = clientp->get_client_handle();
			if (client_handlep == NULL) {
				(void) sc_syslog_msg_initialize(&handle,
				    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE, "Error: low memory");
				sc_syslog_msg_done(&handle);

				// ENOMEM
				ret_value = ENOMEM;
				//
				// set the error code in the registration
				// request
				//
				reg_instance->set_reg_res(ret_value);
				return (ret_value);
			}

			reg_instance->set_client_handle(client_handlep);
			//
			// add the client to updated client list so that he
			// can be updated during commit and revoke
			//
			updated_client_list.append(clientp, &allocation_status);
			if (!allocation_status) {
				(void) sc_syslog_msg_initialize(&handle,
				    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE, "Error: low memory");
				sc_syslog_msg_done(&handle);

				// ENOMEM
				ret_value = ENOMEM;
				// set the error code in the registration
				// request
				//
				reg_instance->set_reg_res(ret_value);
				return (ret_value);
			}

			num_clients++;
			break;
		case REMOVE_CLIENT:
			//
			// iterate the in-memory client list and check if
			// any client matches the incoming registration instance
			//
			if ((ret_value = find_client(clientp, *reg_instance))
			    != 0) {
				//
				// set the error code in the registration
				// request
				//
				reg_instance->set_reg_res(ret_value);
				// proceed with next registration instance
				continue;
			}

			//
			// set in_ccr to false as this client detail
			// should not be added to caapi_reg ccr table
			//
			clientp->set_in_ccr(false);

			FAULTPT_CLAPI(
				FAULTNUM_CLAPI_UCP_RC_UPDATE_UCL_B,
				FaultFunctions::generic);

			updated_client_list.append(clientp, &allocation_status);
			if (!allocation_status) {
				(void) sc_syslog_msg_initialize(&handle,
				    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE, "Error: low memory");
				sc_syslog_msg_done(&handle);

				// ENOMEM
				ret_value = ENOMEM;
				// set the error code in the registration
				// request
				//
				reg_instance->set_reg_res(ret_value);
				return (ret_value);
			}
			// number of clients is decremented
			num_clients--;
			break;

		case ADD_EVENTS:
			//
			// iterate the in-memory client list and check if
			// any client matchs the incoming registration instance
			//
			if ((ret_value = find_client(clientp, *reg_instance))
			    != 0) {
				//
				// set the error code in the registration
				// request
				//
				reg_instance->set_reg_res(ret_value);
				// proceed with next registration instance
				continue;
			}
			//
			// if the client is not in ACTIVE state then
			// events are not added to client
			//
			if (!clientp->is_active()) {
				//
				// set the error code in the registration
				// request
				//
				reg_instance->set_reg_res(
				    CLEP_ERR_CLIENT_DEAD);

				// proceed with next registration instance
				continue;
			}
			//
			// add the requested events to the client set the
			// in_ccr value of events to true
			//
			// get the events to be added to the client registration
			// check is made to avoid duplicate events
			//
			ret_value = get_events_to_update(clientp,
			    reg_instance,
			    (void *)&reg_event_list);
			if (ret_value != 0) {
				reg_instance->set_reg_res(
				    ret_value);

				// proceed with next registration instance
				continue;
			}

			ret_value = clientp->add_events(
			    reg_event_list);


			// add events to clients failed
			if (ret_value != 0) {
				(void) sc_syslog_msg_initialize(&handle,
				    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
				//
				// SCMSGS
				// @explanation
				// The cl_apid experienced an internal error
				// that prevented proper updates to a CRNP
				// client.
				// @user_action
				// Examine other syslog messages occurring at
				// about the same time to see if the problem
				// can be identified. Save a copy of the
				// /var/adm/messages files on all nodes and
				// contact your authorized Sun service
				// provider for assistance in diagnosing and
				// correcting the problem.
				//
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE, "Failed to add events to client");
				sc_syslog_msg_done(&handle);

				//
				// set the error code in the registration
				// request
				//
				reg_instance->set_reg_res(ret_value);
				reg_event_list.erase_list();
				return (ret_value);
			}

			// update the mapping with new events of client
			ret_value = event_mapping.update_mapping(ADD_EVENTS,
			    clientp);

			FAULTPT_CLAPI(
				FAULTNUM_CLAPI_UCP_AE_UPDATE_M_A,
				FaultFunctions::generic);

			// probably memory allocation
			if (ret_value != 0) {
				(void) sc_syslog_msg_initialize(&handle,
				    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE, "Error: low memory");
				sc_syslog_msg_done(&handle);

				//
				// set the error code in the registration
				// request
				//
				reg_instance->set_reg_res(ret_value);
				reg_event_list.erase_list();
				return (ret_value);
			}
			// get client handle
			client_handlep = clientp->get_client_handle();
			if (client_handlep == NULL) {
				(void) sc_syslog_msg_initialize(&handle,
				    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE, "Error: low memory");
				sc_syslog_msg_done(&handle);

				// set the error code in the registration
				// request
				reg_instance->set_reg_res(ENOMEM);
				reg_event_list.erase_list();
				return (ENOMEM);
			}

			// set the client_handle in the registration instance
			reg_instance->set_client_handle(client_handlep);

			FAULTPT_CLAPI(
				FAULTNUM_CLAPI_UCP_AE_UPDATE_UCL_B,
				FaultFunctions::generic);

			updated_client_list.append(clientp, &allocation_status);
			if (!allocation_status) {
				(void) sc_syslog_msg_initialize(&handle,
				    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE, "Error: low memory");
				sc_syslog_msg_done(&handle);

				// ENOMEM
				ret_value = ENOMEM;
				// set the error code in the registration
				// request
				//
				reg_instance->set_reg_res(ret_value);
				return (ret_value);
			}

			break;
		case REMOVE_EVENTS:
			//
			// iterate the in-memory client list and check if
			// any client matchs the incoming registration instance
			//

			scds_syslog_debug(1, "\nremove requested events.");
			if (find_client(clientp, *reg_instance) != 0) {
				//
				// set the error code in the registration
				// request
				//
				reg_instance->set_reg_res(CLEP_ERR_UNKW);
				return (CLEP_ERR_UNKW);
			}
			//
			// if the client is not in ACTIVE state then
			// events are not removed from client
			//
			if (!clientp->is_active()) {
				//
				// set the error code in the registration
				// request
				//
				reg_instance->set_reg_res(
				    CLEP_ERR_CLIENT_DEAD);

				// proceed with next registration instance
				continue;
			}
			//
			//
			// get the events to be removed from the client
			// registration verify whether client has registered
			// for the event to be removed.
			//
			ret_value = get_events_to_update(clientp,
			    reg_instance,
			    (void *)&unreg_event_list);

			if (ret_value != 0) {
				reg_instance->set_reg_res(
				    ret_value);

				// proceed with next registration instance
				continue;
			}
			scds_syslog_debug(1, "\nremove events : received event "
			    "list");

			ret_value = clientp->remove_events_prepare(
					    unreg_event_list);

			scds_syslog_debug(1, "\nremove events : removed "
			    "events");
			FAULTPT_CLAPI(
				FAULTNUM_CLAPI_UCP_RE_CALL_PREPARE_A,
				FaultFunctions::generic);

			// remove events from client failed
			if (ret_value != 0) {

				(void) sc_syslog_msg_initialize(&handle,
				    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
				//
				// SCMSGS
				// @explanation
				// The cl_apid experienced an internal error
				// that prevented proper updates to a CRNP
				// client.
				// @user_action
				// Examine other syslog messages occurring at
				// about the same time to see if the problem
				// can be identified. Save a copy of the
				// /var/adm/messages files on all nodes and
				// contact your authorized Sun service
				// provider for assistance in diagnosing and
				// correcting the problem.
				//
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE, "Failed to remove events from "
				    "client");
				sc_syslog_msg_done(&handle);

				//
				// set the error code in the registration
				// request
				//
				reg_instance->set_reg_res(ret_value);
				unreg_event_list.erase_list();
				return (ret_value);
			}
			// get client handle
			client_handlep = clientp->get_client_handle();
			if (client_handlep == NULL) {

				(void) sc_syslog_msg_initialize(&handle,
				    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE, "Error: low memory");
				sc_syslog_msg_done(&handle);

				//
				// set the error code in the registration
				// request
				//
				reg_instance->set_reg_res(ENOMEM);
				unreg_event_list.erase_list();
				return (ENOMEM);
			}

			// set the client_handle in the registration instance
			reg_instance->set_client_handle(client_handlep);

			FAULTPT_CLAPI(
				FAULTNUM_CLAPI_UCP_RE_UPDATE_UCL_B,
				FaultFunctions::generic);

			// add the updated client to list
			updated_client_list.append(clientp, &allocation_status);
			if (!allocation_status) {
				(void) sc_syslog_msg_initialize(&handle,
				    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE, "Error: low memory");
				sc_syslog_msg_done(&handle);

				// ENOMEM
				ret_value = ENOMEM;
				// set the error code in the registration
				// request
				//
				reg_instance->set_reg_res(ret_value);
				return (ret_value);
			}
			break;

		default:
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "Invalid registration operation");
			sc_syslog_msg_done(&handle);

		} // close switch statement
	}// close while

	reg_event_list.erase_list();
	unreg_event_list.erase_list();
	return (0);
}
void
caapi_reg_cache::update_client_commit(SList<sc_registration_instance>
			    &reg_instance_list,
			    SList<caapi_client> &updated_client_list)
{
	FAULTPT_CLAPI(FAULTNUM_CLAPI_UCC_B,
		FaultFunctions::generic);

	reg_type  client_reg_type;
	sc_registration_instance *reg_instance = NULL;
	sc_syslog_msg_handle_t handle;

	caapi_client *clientp = NULL;

	reg_instance_list.atfirst();
	updated_client_list.atfirst();

	//
	// NOTE iterating both the sc_registration instance list and
	// updated client list as the registration type of client
	// is known only to sc_registration_instance
	//
	ASSERT(updated_client_list.count() <= reg_instance_list.count());

	while (((clientp = updated_client_list.get_current()) != NULL) &&
	    ((reg_instance = reg_instance_list.get_current()) != NULL)) {

		//
		// if Error occurred during processing this request
		// then do  not ignore this registration request
		// as client wont be listed in updated client list
		//
		if (reg_instance->get_reg_res() != 0) {
			reg_instance_list.advance();
			continue;
		}
		updated_client_list.advance();
		reg_instance_list.advance();

		client_reg_type = reg_instance->get_reg_type();
		switch (client_reg_type) {

		case  ADD_CLIENT:
		case  ADD_EVENTS:

			//
			// for each updated client the back pointer in event
			// is set client ptr
			//
			clientp->set_client_backptr();
			break;
		case  REMOVE_EVENTS:
			//
			// update the mapping structure remove requested
			// events from structure corresponding to this client
			//
			// Ignoring return value as update_mapping
			// wont be returning for this case.
			//
			(void) event_mapping.update_mapping(REMOVE_EVENTS,
			    clientp);

			FAULTPT_CLAPI(FAULTNUM_CLAPI_UCC_RE_UPDATE_M_A,
				FaultFunctions::generic);

			//
			// new events which needed to be removed are finally
			// are removed from client list
			//
			(void) clientp->remove_events_commit();
			break;
		case  REMOVE_CLIENT:

			//
			// update the mapping structure remove all
			// events from structure corresponding to this client
			//
			(void) event_mapping.update_mapping(REMOVE_CLIENT,
			    clientp);

			FAULTPT_CLAPI(FAULTNUM_CLAPI_UCC_RC_UPDATE_M_A,
				FaultFunctions::generic);

			//
			// if there is no outstanding references to client
			// then try delete will return true and can be
			// deleted else this client is deleted by client
			// handler holding the references
			// erase this client from list
			//
			(void) client_list.erase(clientp);
			//
			// delete the client instance if there are
			// no-outstanding references to client
			//
			clientp->try_delete();
			break;
		default:
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "Invalid registration operation");
			sc_syslog_msg_done(&handle);

		} // end of switch
	}

	// erase all elements of updated client_list
	updated_client_list.erase_list();
}
void
caapi_reg_cache::update_client_revoke(SList<sc_registration_instance>
			    &reg_instance_list,
			    SList<caapi_client> &updated_client_list,
			    int return_status)
{
	FAULTPT_CLAPI(FAULTNUM_CLAPI_UCR_B,
		FaultFunctions::generic);

	int ret_value = 0;
	reg_type  client_reg_type;
	sc_registration_instance *reg_instancep = NULL;
	sc_syslog_msg_handle_t handle;

	caapi_client *clientp = NULL;

	reg_instance_list.atfirst();
	updated_client_list.atfirst();
	//
	// NOTE iterating both the sc_registration instance list and
	// updated client list as the registration type of client
	// is known only to sc_registration_instance
	//
	ASSERT(updated_client_list.count() <= reg_instance_list.count());

	while (((clientp = updated_client_list.get_current()) != NULL) &&
	    ((reg_instancep = reg_instance_list.get_current()) != NULL)) {
		updated_client_list.advance();
		reg_instance_list.advance();
		//
		// if Error occurred during processing this request
		// then do  not ignore this registration request
		// as client wont be listed in updated client list
		//
		if (reg_instancep->get_reg_res() != 0) {
			reg_instance_list.advance();
			continue;
		}


		client_reg_type = reg_instancep->get_reg_type();
		switch (client_reg_type) {

		case  ADD_CLIENT:
			//
			// update the mapping structure remove all
			// events from structure corresponding to this client
			//
			ret_value = event_mapping.update_mapping(REMOVE_CLIENT,
			    clientp);

			FAULTPT_CLAPI(FAULTNUM_CLAPI_UCR_AC_UPDATE_M_A,
				FaultFunctions::generic);

			ASSERT(ret_value == 0);
			(void) client_list.erase(clientp);

			// reduce the number of clients registered
			num_clients--;

			break;

		case REMOVE_CLIENT:
			//
			// the in_ccr value of client is set to true as this
			// client was not removed from ccr.
			//
			clientp->set_in_ccr(true);
			// number client is again incremented
			num_clients++;
			break;
		case ADD_EVENTS:

			//
			// in this case the client's new events should
			// be removed from the event-mapping and client
			// as add_events failed
			//
			ret_value = event_mapping.update_mapping(REMOVE_EVENTS,
			    clientp);

			FAULTPT_CLAPI(FAULTNUM_CLAPI_UCR_AE_UPDATE_M_A,
				FaultFunctions::generic);

			// also new events should be removed from client
			(void) clientp->add_events_revoke();

			break;
		case REMOVE_EVENTS:
			//
			// the in_ccr for all events is set to true as this
			// was not removed from ccr table
			//
			ret_value = clientp->remove_events_revoke();
			break;
		default:
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "Invalid registration operation");
			sc_syslog_msg_done(&handle);

		} // end of switch
	} // end of while

	// since updation to client failed the client_handle in
	// registration instance is set to NULL
	//

	// XXX sc_registration instance should delete the handler
	// also set the regsitration result status to failure.
	// Bulk registration failure.
	reg_instance_list.atfirst();
	while ((reg_instancep = reg_instance_list.get_current()) != NULL) {
		reg_instance_list.advance();
		// set the client_handle in the registration instance
		reg_instancep->set_client_handle(NULL);
		reg_instancep->set_reg_res(return_status);
	}
	// erase all elements of updated client_list
	updated_client_list.erase_list();
}



//
//
// lookup_clients calls lookup method function of event-mapping
// NOTE: caller needs to deallocate memory allocated to SList
//
SList<caapi_client_handle>*
caapi_reg_cache::lookup_clients(const sc_event *eventp_in)
{
	// returned to caller
	SList<caapi_client_handle> *client_handle_list = NULL;

	// only one thread is allowed to lookup/update
	mapping_lck.lock();

	if (eventp_in) {
		scds_syslog_debug(1, "\n===notify client event_class %s "
		    "event_subclass %s", eventp_in->get_class(),
		    eventp_in->get_subclass());
	}
	client_handle_list = event_mapping.lookup_clients(eventp_in);
	if (client_handle_list) {
		scds_syslog_debug(1, "\n===total clients notified for this "
		    "event == %d ", client_handle_list->count());
	}

	mapping_lck.unlock();
	return (client_handle_list);
}

//
// get_events_to_update function verifies if a event is already
// registered by the client. If the regsitration type is ADD_EVENTS
// and if the event is already registered then that event is ignored
// else it is the event information is added to the client's registration
// information. else if the registration type is REMOVE_EVENTS then the
// event is removed only if it has been already registered by the client.
// This function uses the event mapping hash table for optimal search.
//
int
caapi_reg_cache::get_events_to_update(caapi_client *clientp,
				const sc_registration_instance *reg_instancep,
				void *event_listp)

{
	SList<sc_event> *sc_event_listp = NULL;
	sc_event *reg_sc_eventp = NULL;
	caapi_event *reg_eventp = NULL;
	int ret_value = 0;
	reg_type client_reg_type;
	sc_syslog_msg_handle_t handle;

	client_reg_type = reg_instancep->get_reg_type();

	// get list of events from registration instance
	sc_event_listp = const_cast<SList<sc_event>*>(&(reg_instancep->
	    get_events()));


	// add the new list at the end of event list
	sc_event_listp->atfirst();

	while ((reg_sc_eventp = sc_event_listp->get_current()) != NULL) {
		sc_event_listp->advance();

		ret_value = event_mapping.lookup_event_in_client(
				clientp, reg_sc_eventp, reg_eventp);

		if (ret_value != 0) {
			return (ret_value);
		}

		switch (client_reg_type) {
		case ADD_EVENTS:
			//
			// if the client has registered for the event
			// then ignore the event
			//
			if (reg_eventp) {

				// This event is not added into the
				// the client registration list.
				reg_eventp = NULL;
				continue;
			} else {
				((SList<sc_event>*)event_listp)->append(
				    reg_sc_eventp);
			}
			break;
		case REMOVE_EVENTS:
			//
			// if the client has registered for the event
			// then add the event instance to the list.
			// this list will be used to remove the event
			// from the client registration information.
			//
			if (reg_eventp != NULL)
				((SList<caapi_event>*)event_listp)->
				    append(reg_eventp);
			break;
		case ADD_CLIENT:
		case REMOVE_CLIENT:
		default:
			// not a valid option this function is not called with
			// this option.
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "Invalid registration operation");
			sc_syslog_msg_done(&handle);
		} // end of switch
	} // end of while
	return (0);
}

//
// get_clients returns a hashtable which is indexed by subclass
// each subclass is associated with a list of clients
// interested in the subclass i.e sorted hashtable by subclass
// and associated with subclass is a list of clients registerted
// for event with the same subclass
// Note : caller is responsible for deallocating the memory
// for the string hashtable.
//
string_hashtable_t<SList<caapi_client_handle> *>*
caapi_reg_cache::get_clients()
{
	client_hashlist_by_subclass		client_subclass_hashlist;

	string_hashtable_t<SList<caapi_client_handle> *> *subclass_hashp = NULL;
	mapping_lck.lock();
	subclass_hashp = client_subclass_hashlist.get_clients(client_list);
	mapping_lck.unlock();
	return (subclass_hashp);
}
//
// The following is the client_hashlist_by_subclass class implementation
//
// Void constructor of client_hashlist_by_sublcass
//

client_hashlist_by_subclass::client_hashlist_by_subclass():
	subclass_hashp(NULL)
{
}

client_hashlist_by_subclass::~client_hashlist_by_subclass()
{
	// sanity check
	if (subclass_hashp) {
		destruct_helper();
	}
	subclass_hashp = NULL;
}
//
// get_clients returns a hashtable which is indexed by subclass
// each subclass is associated with a list of clients
// interested in the subclass i.e sorted hashtable by subclass
// and associated with subclass is a list of clients registerted
// for event with the same subclass
// Note : caller is responsible for deallocating the memory
// for the string hashtable.
//
string_hashtable_t<SList<caapi_client_handle> *>*
client_hashlist_by_subclass::get_clients(SList<caapi_client> &client_list)
{
	SList<caapi_event> *event_listp = NULL;
	SList<caapi_client_handle> *client_listp = NULL;
	bool client_found = false;
	caapi_client *clientp = NULL;
	caapi_event *eventp = NULL;
	const char *evt_subclass = NULL;
	caapi_client_handle *client_handlep = NULL;
	bool allocation_status;
	sc_syslog_msg_handle_t handle;

	//
	// this list will have clients who have registered for a event
	// with just class name, as a result this client is
	// interested in all subclassess
	//
	SList<caapi_client> client_list_by_class;

	subclass_hashp = new string_hashtable_t<SList<caapi_client_handle>*>;
	if (subclass_hashp != NULL) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Error: low memory");
		sc_syslog_msg_done(&handle);
		return (NULL);
	}

	// for each client in the client_list
	client_list.atfirst();
	while ((clientp = client_list.get_current()) != NULL) {
		client_list.advance();

		// for each event in the evtlistp of client
		event_listp = clientp->get_events();

		// no events associated with the client shouldnt happen
		if (event_listp == NULL)
			continue;

		event_listp->atfirst();
		while ((eventp = event_listp->get_current()) != NULL) {
			event_listp->advance();

			evt_subclass = eventp->get_subclass();

			//
			// check if client exists in the list_by_class
			// if it does then proceed with next client
			//
			if (client_list_by_class.exists(clientp))
				break;

			// this client is associated with all subclasses
			if (evt_subclass == NULL) {
				// add client to list
				client_list_by_class.append(
				    clientp, &allocation_status);
				if (!allocation_status) {
					(void) sc_syslog_msg_initialize(
					    &handle,
					    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
					(void) sc_syslog_msg_log(handle,
					    LOG_ERR, MESSAGE,
					    "Error: low memory");
					sc_syslog_msg_done(&handle);

					destruct_helper();
					client_list_by_class.erase_list();
					return (NULL);
				}
				break;
			}

			client_listp = subclass_hashp->lookup(evt_subclass);

			//
			// if the subclass is not found in hashtable
			// create a entry for the subclass in the hashtable
			// and associate this client with the subclass
			//
			if (client_listp == NULL) {
				client_listp = new SList<caapi_client_handle>();
				if (client_listp != NULL) {
					(void) sc_syslog_msg_initialize(
					    &handle,
					    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
					(void) sc_syslog_msg_log(handle,
					    LOG_ERR, MESSAGE,
					    "Error: low memory");
					sc_syslog_msg_done(&handle);

					destruct_helper();
					client_list_by_class.erase_list();
					return (NULL);
				}
				(*subclass_hashp).add(client_listp,
				    evt_subclass, &allocation_status);
				if (!allocation_status) {
					(void) sc_syslog_msg_initialize(
					    &handle,
					    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
					(void) sc_syslog_msg_log(handle,
					    LOG_ERR, MESSAGE,
					    "Error: low memory");
					sc_syslog_msg_done(&handle);

					destruct_helper();
					client_list_by_class.erase_list();
					return (NULL);
				}

				// add the client handle into list
				client_found = false;
			} else {
				//
				// if the subclass does exist in the hash-table
				// then add the client handle to the list
				// associated with the subclass
				//
				client_listp->atfirst();
				//
				// iterate through the list and add client
				// handle only if it doesnt exist in the list
				//
				client_found = false;
				while ((client_handlep =
				    client_listp->get_current()) != NULL) {

					client_listp->advance();

					// if client exists in the list
					if (client_handlep->is_handle(
					    clientp)) {
						client_found = true;
						break;
					}
				} // end of inner while
			}// end else

			//
			// if client not found in the list then
			// create a handle for client and add to list
			//
			if (!client_found) {
				//
				// create a handle for the client and
				// add this client handle to list associated
				// with subclass
				//

				client_handlep = clientp->get_client_handle();

				// This indicates a system in low-memory.
				if (client_handlep == NULL) {
					//
					// need to de-allocate memory of
					// elements in the hash-table.
					//
					destruct_helper();
					client_list_by_class.erase_list();
					return (NULL);
				}
				client_listp->append(client_handlep,
				    &allocation_status);
				if (!allocation_status) {
					destruct_helper();
					client_list_by_class.erase_list();
					client_listp->erase_list();
					return (NULL);
				}
			}
		}// end while 2nd
	} // end while 1st

	//
	// add the client's in client_list_by_class to list associated with
	// each subclass
	//
	string_hashtable_t<SList<caapi_client_handle> *>::iterator
	    iter(subclass_hashp);
	SList<caapi_client_handle> *hash_elem = NULL;
	while ((hash_elem = iter.get_current()) != NULL) {
		iter.advance();

		//
		// each client in the client_list_by_class is added
		// to the list associated with subclass.
		//
		client_list_by_class.atfirst();
		while ((clientp = client_list_by_class.get_current()) != NULL) {

			client_list_by_class.advance();
			client_handlep = clientp->get_client_handle();
			if (client_handlep == NULL) {
				//
				// need to de-allocate memory of elements
				// in the hash-table
				//
				destruct_helper();
				client_list_by_class.erase_list();
				return (NULL);
			}
			hash_elem->append(client_handlep, &allocation_status);
			if (!allocation_status) {
				destruct_helper();
				client_list_by_class.erase_list();
				return (NULL);
			}
		}
	}
	// erase all elements from the client_list_by_class
	client_list_by_class.erase_list();
	return (subclass_hashp);
}
//
// destruct_helper function -> de-allocates all the memory allocated to
// the hash_elements. this function is called only during low-memory
// scenarios
//
void
client_hashlist_by_subclass::destruct_helper()
{
	if (subclass_hashp) {

		string_hashtable_t<SList<caapi_client_handle> *>::iterator
		    iter(subclass_hashp);
		SList<caapi_client_handle> *hash_elem = NULL;
		while ((hash_elem = iter.get_current()) != NULL) {
			iter.advance();
			hash_elem->dispose();
			delete hash_elem;
		}
		subclass_hashp->dispose();
		delete subclass_hashp;
		subclass_hashp = NULL;
	} // end of if.
}
