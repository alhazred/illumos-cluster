/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_SC_EVENT_CACHE_H_
#define	_SC_EVENT_CACHE_H_

#pragma ident	"@(#)sc_event_cache.h	1.6	08/05/20 SMI"

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#include "sc_registration_instance.h"
#include "sc_xml_event.h"

/* the function pointer type for the request function */
typedef int (*request_funcp_t) (char **, char **, int);

/*
 * event_type_info
 * ---------------
 * Helper class for storing the information for one event type
 * (class.subclass).
 */

/*
 * We don't have a default constructor, and we don't want one.*/
/*lint -e1712 */
class event_type_info
{
public:
	event_type_info(request_funcp_t func_in, char **names_in,
	    int num_names, const char *key);
	~event_type_info();
	char *get_key() {return key; }

	request_funcp_t request_func;
	SList<char> dist_names;
	char *key;

	int num_names;
	sc_xml_event *cached_event;
	string_hashtable_t<sc_xml_event *> cached_events;

	int status;
};
/*lint +e1712 */


/*
 * class sc_event_cache
 * --------------------
 * This class has three functions:
 * 1. Requesting events from external components upon startup.
 * 2. Approving client registrations for events.
 * 3. Caching the most recent event for each type, and for each different
 * "distinguishing names" within that type, and looking up events for newly
 * registered clients.
 *
 * Note: This class functions as a monitor, allowing only one thread
 * at a time into its methods.  Thus, a call to any of the public
 * methods may block while waiting for another thread to complete.
 *
 * Therefore, all calls are MT safe.
 *
 * For implementation details, see sc_event_cache.cc
 */

/*
 * We don't have a default constructor, and we don't want one.*/
/*lint -e1712 */
class sc_event_cache
{
public:
	/*
	 * approve_registrations
	 * ----------------------
	 * Takes a list of sc_registration_instance objects.  For each
	 * one, checks each event registration, ensuring that for each
	 * event type, all required nvpairs are present.
	 *
	 * The sc_registration_instance is marked as approved or denied in its
	 * reg_res field.  It is marked denied if any one of the events
	 * specified are invalid.
	 */
	void approve_registrations(SList<sc_registration_instance> &regs);

	/*
	 * request_events
	 * --------------
	 * Requests event generation, via the loadable modules, for _all_ event
	 * types for which we have loadable modules.
	 *
	 * Note that the event requests, through the loadable modules,
	 * are preformed synchronously ( _not_ on a separate thread).
	 *
	 * It is expected that this method will be called once, at start-up
	 * of the daemon.
	 */
	void request_events();

	/*
	 * send_cached_event(s)
	 * -------------
	 * For each sc_registration_instance, for each sc_xml_event, looks
	 * up the most recent event for that type in its cache and
	 * calls into the evt_comm_module to deliver the event to
	 * this client.  Uses the client_handle stored in each
	 * sc_registration_instance to pass to the event delivery module.
	 *
	 * Checks the reg_res of each sc_registration_instance before
	 * processing it.  If the reg_res is valid (0), assumes that
	 * it has a valid client_handle.
	 *
	 * Also checks (does not assume) that the registration is of a
	 * valid type (ADD_CLIENT or ADD_EVENTS).
	 */
	int send_cached_events(SList<sc_registration_instance> &regs);
	int send_cached_event(sc_registration_instance *reg);


	/*
	 * store_event
	 * --------------
	 * Stores the event in its event cache.
	 *
	 * Returns 0 if the event is stored properly, -1 or an error
	 * code otherwise.  If the event is stored, the client
	 * should not delete the memory for evt, or use it further.
	 *
	 * If the event is not stored, the client stil owns
	 * the memory, and should delete it.
	 */
	int store_event(sc_xml_event *evt);

	/*
	 * reload_modules
	 * ----------------
	 * Request that the sc_event_cache object reload the shared-object
	 * modules from the well-known directory path.
	 *
	 * Should be called when a SIGHUP is received.
	 */
	int reload_modules(const char *plugin_path_in);

	/*
	 * the
	 * -----------
	 * Returns a reference to the one sc_event_cache object.
	 */
	static sc_event_cache &the();

	/*
	 * initialize
	 * ---------
	 * This method should be called exactly once in a program to
	 * create and initialize the one instance of sc_event_cache.
	 */
	static int initialize(const char *plugin_path);

	/*
	 * shutdown
	 * -----------
	 * Called on the sc_event_cache object to tell it to destroy
	 * itself.
	 */
	void shutdown();

	/*
	 * print
	 * --------
	 * for debugging
	 */
	void print();

	/* event_type_info es mi amigo. Me gustan los event_type_infos. */
	friend event_type_info;

private:
	/*
	 * ctor/dtor are private so only the static initialize method
	 * and shutdown method can create/destroy it.
	 */
	sc_event_cache(const char *plugin_path_in);
	~sc_event_cache();

	/*
	 * Disallow pass by value, assignment
	 */
	sc_event_cache(const sc_event_cache &);
	/* Cstyle doesn't understand operator overloading */
	/*CSTYLED */
	sc_event_cache &operator=(const sc_event_cache &);


	/*
	 * helper methods
	 */
	void hash_delete_helper();
	void send_all_events(sc_registration_instance *one_reg);
	int send_matching_event(sc_registration_instance *one_reg,
	    event_type_info *cur_info, sc_event *reg_evt);
	int get_event_nv_key(sc_event *evt, SList<char> &names, char *&);
	int load_modules();

	/*
	 * The mutex, for mutual exclusion
	 */
	pthread_mutex_t event_cache_lock;

	string_hashtable_t<event_type_info *> event_info;

	char *plugin_path;

	/*
	 * The one instance of sc_event_cache
	 */
	static sc_event_cache *the_sc_event_cache;
};
/*lint +e1712 */

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#endif /* _SC_EVENT_CACHE_H_ */
