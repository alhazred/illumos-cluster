/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_NET_TOOLS_H_
#define	_NET_TOOLS_H_

#pragma ident	"@(#)net_tools.h	1.5	08/05/20 SMI"

#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <strings.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#include "sc_callback_info.h"

/*
 * This file provides a set of useful, generic, networking functions.
 */

/*
 * symbolic_or_IP_to_binary
 * --------------------------
 * First attempt to treat the host name as a dotted-quad IP.
 * If that fails, call gethostbyname to get the DNS info for
 * the host name.  Fills the binary, network ordered, host name in
 * sinPtr, or returns non-zero.
 *
 * Returns 0 on success.
 */
int symbolic_or_IP_to_binary(const char *host_str,
    struct sockaddr_in *sin_ptr);

/*
 * create_listening_socket
 * -----------------------
 * Creates a tcp socket, storing it in *fd.
 * Calls bind and listen, using the ip and port provided,
 * to set it up as a listening socket.  If the ip is NULL,
 * then the socket is bound to INADDR_ANY.
 *
 * Returns 0 on success, -1 on failure.
 */
int create_listening_socket(int *fd, const char *ip, int port);

/*
 * create_connected_socket
 * -----------------------
 * Creates a tcp socket and calls connect, using host and port as the
 * remote host and port.
 *
 * Returns the new socket on success, -1 on error.
 */
int create_connected_socket(char *host, int port);

/*
 * create_connected_nb_socket
 * -----------------------------
 * Creates a non-blocking tcp socket and calls connect, using
 * the ip and port from the sc_callback_info.
 */
int create_connected_nb_socket(sc_callback_info *cb_info);

/*
 * sock_write
 * ------------
 * Writes the entire str to sock, assuming sock is already connected.
 *
 * Returns bytes read on success, -1 on failure.
 */
int sock_write(int sock, const char *str);

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#endif /* _NET_TOOLS_H_ */
