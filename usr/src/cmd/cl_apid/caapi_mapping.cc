/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)caapi_mapping.cc 1.21     08/05/20 SMI"



#include "caapi_mapping.h"
#include "cl_apid_error.h"
#include "sc_registration_instance.h"
#include "cl_apid_include.h"
#include <orb/fault/fault_injection.h>
#include <sys/sc_syslog_msg.h>

// XXX more comments
// XXX update_event in nvstring is statically defined



caapi_mapping::caapi_mapping()
{}


//
// destructor deallocate the memory allocated to the SList associated with
// keys of the hashtable
//
caapi_mapping::~caapi_mapping()
{
	// delete each SList, the key in the hash-table is pointing.
	string_hashtable_t<SList<caapi_event> *>::iterator
	    iter(client_event_hash);
	SList<caapi_event> *hash_elem = NULL;
	while ((hash_elem = iter.get_current()) != NULL) {
		iter.advance();
		hash_elem->erase_list();
		delete hash_elem;
	}
	client_event_hash.dispose();
}

//
// get_client_list function iterates the event list and identifies the
// client's which need to be informed for the incoming sysevent. when a
// new sysevent is generated communication module will request
// caapi_reg_cache.
// module for a list of client's interested in that sysevent.
// for eg: if sysevent is generated with properties
// class = EC_CLUSTER, subclass = ResourceGroupStateChange
// and two name value pairs associated with the sysevent
// ResourceGroupName=oracle and ResourceGroupState=online
// the get_client_list is supposed return all clients
// 1. registered for all EC_CLUSTER events
// 2. registered for all EC_CLUSTER and ResourceGroupStateChange events
// 3. registered for all EC_CLUSTER and ResourceGroupStateChange events
//    and ResourceGroupName=oracle
// 4. registered for all EC_CLUSTER and ResourceGroupStateChange events
//    and ResourceGroupState=online
// 5. registered for all EC_CLUSTER and ResourceGroupStateChange events
//    and ResourceGroupName=oracle and ResourceGroupState=online
//  this function in above case is called 4 times
// once for EC_CLUSTER
// once for EC_CLUSTER:ResourceGroupStateChange
// once for EC_CLUSTER:ResourceGroupStateChange:ResourceGroupState
// once for EC_CLUSTER:ResourceGroupStateChange:ResourceGroupName
//
// event_list is the list retuned by the hash.lookup for the given key
//
// marked event list is a list of events which have been associated with
// one of the hash keys we are storing this list so that in the end
// we can set the match frequency of all events to 0.
//
// notify_client_list is the list of clients who are interested in the
// incoming event
//

int
caapi_mapping::get_client_list(SList<caapi_event> *event_list,
				SList<caapi_event> & marked_event_list,
				SList<caapi_client> & notify_client_list)
{
	caapi_client *clientp = NULL;
	caapi_event *eventp = NULL;
	bool allocated_status;

	// no clients associated with event string
	if (event_list == NULL)
		return (0);

	event_list->atfirst();
	while ((eventp = event_list->get_current()) != NULL) {
		event_list->advance();
		//
		// if the event client pointer is set to NULL
		// then that event is not in active state
		// so all clients corresponding to such events
		// are ignored
		// Also if the client is not ACTIVE then events should
		// not be sent to him and client is ignored
		//
		if ((eventp->get_client_backptr() == NULL) ||
		    (!eventp->get_client_backptr()->is_active()))
			continue;

		eventp->increment_frequency();
		//
		// inform client for the incoming event if the match frequency
		// of the event matches the name-value count of the event
		// match frequency is the count of the number of times a event
		// matches with the incoming event and name-value count is the
		// total number of name value associated with this event
		// registered by client
		//
		if (eventp->get_match_frequency() == eventp->get_nv_count()) {
			clientp = eventp->get_client_backptr();

			//
			// if client notify is false that means he hasnt been
			// informed else client exists in notify_client_list.
			//
			if (!clientp->is_notified()) {
				clientp->set_notify(true);
				// add clients to notify client list
				notify_client_list.append(clientp,
				    &allocated_status);
				if (!allocated_status) {
					return (ENOMEM);
				}
			}
		}  // end of if
		//
		// the event which is associated with atleast one of the
		// key's is stored in the marked_event_list so that the match
		// frequency of each client is reset to 0, once we get a final
		// list of clients associated with the incoming sysevent.
		//
		if (eventp->get_match_frequency() == 1) {
			marked_event_list.append(eventp);
		}
	} // end of while
	return (0);
}

//
// returns a list of clients for a specified event properties specified
// by classname, subclassname, and nvpairstring
// identifies the client's which need to be informed for the
// incoming sysevent. when a new sysevent is generated
// communication module will request caapi_reg_cache module
// for a list of client's interested in that sysevent.
// for eg: if sysevent is generated with properties
// class = EC_CLUSTER, subclass = ResourceGroupStateChange
// and two name value pairs associated with the sysevent
// ResourceGroupName=oracle and ResourceGroupState=online
// the get_client_list is supposed return all clients
// 1. registered for all EC_CLUSTER events
// 2. registered for all EC_CLUSTER and ResourceGroupStateChange events
// 3. registered for all EC_CLUSTER and ResourceGroupStateChange events
//    and ResourceGroupName=oracle
// 4. registered for all EC_CLUSTER and ResourceGroupStateChange events
//    and ResourceGroupState=online
// 5. registered for all EC_CLUSTER and ResourceGroupStateChange events
//    and ResourceGroupName=oracle and ResourceGroupState=online
// NOTE:
// caller of the function is reponsible for deallocating the
// memory allocated to SList of caapi_clients
//
SList<caapi_client_handle>*
caapi_mapping::lookup_clients(const sc_event *eventp_in)
{
	SList <caapi_client>  notify_client_list;
	SList <caapi_client_handle>  *notify_client_handle_list = NULL;
	SList <caapi_event>  marked_event_list;
	SList <caapi_event>  *event_list = NULL;
	caapi_client *clientp = NULL;
	caapi_event *eventp = NULL;
	const char *evt_class = NULL;
	const char *evt_subclass = NULL;
	nvstrpair *evt_nvpair = NULL;
	const char *evt_nvname = NULL;
	const char *evt_nvvalue = NULL;
	char *nvstring = NULL;
	char *eventstr = NULL;
	SList<nvstrpair> *nv_listp = NULL;
	caapi_client_handle *client_handlep = NULL;
	bool allocation_status;

	// Null returned in case of NULL event
	if (eventp_in == NULL)
		return (NULL);

	evt_class =  eventp_in->get_class();
	evt_subclass =  eventp_in->get_subclass();

	// no constant iterator defined os using const cast
	nv_listp = const_cast<SList<nvstrpair> *>(&
	    (eventp_in->get_nvlist()));

	scds_syslog_debug(1, "event_mapping= looking for clients interested in "
	    "event class == %s, subclass == %s", eventp_in->get_class(),
	    eventp_in->get_subclass());

	// log error classname not specified none of client's will be notified
	if (evt_class == NULL)
		return (NULL);

	//
	// will get a list of all events characterised classname only.
	// as a result subclass and name-value string is passed NULL
	//
	if (get_event_string(evt_class, NULL, NULL, eventstr) == ENOMEM)
		return (NULL);

	event_list = client_event_hash.lookup(eventstr);

	// eventstr is allocated in get_event_string function
	delete [] eventstr;

	//
	// will get a list of all client's interested in events
	// characterised classname only.
	//
	if (get_client_list(event_list, marked_event_list,
	    notify_client_list) != 0) {
		notify_client_list.erase_list();
		marked_event_list.erase_list();
		return (NULL);
	}

	//
	// will get a list of all events characterised classname , subclass
	// only. as a result subclass and name-value string is passed NULL
	//
	if (evt_subclass) {

		if (get_event_string(evt_class, evt_subclass, NULL, eventstr)
		    == ENOMEM) {
			notify_client_list.erase_list();
			marked_event_list.erase_list();
			return (NULL);
		}

		event_list = client_event_hash.lookup(eventstr);

		// deallocate eventstr;
		delete [] eventstr;

		//
		// will get a list of all client's interested in events
		// characterized classname and subclassname only
		//
		if (get_client_list(event_list, marked_event_list,
		    notify_client_list) != 0) {
			notify_client_list.erase_list();
			marked_event_list.erase_list();
			return (NULL);
		}
	}

	if (evt_subclass && nv_listp) {
		//
		// for each nvpair form a eventstr
		// classname:subclassname:name=value
		// for each eventstr lookup the hash and a get
		// get all clients associated with the eventstr
		//
		nv_listp->atfirst();
		while ((evt_nvpair = nv_listp->get_current())
				!= NULL) {
			nv_listp->advance();
			evt_nvname = evt_nvpair->get_name();
			evt_nvvalue = evt_nvpair->get_value_str();

			// NULL name-value are ignores
			if ((evt_nvname == NULL) ||
			    (evt_nvvalue == NULL))
				continue;

			nvstring = new char[strlen(evt_nvname) +
			    strlen(evt_nvvalue) + 2];

			if (nvstring == NULL) {
				notify_client_list.erase_list();
				marked_event_list.erase_list();
				return (NULL);
			}
			(void) sprintf(nvstring, "%s%s%s", evt_nvname,
			    NV_DELIMITER, evt_nvvalue);

			//
			// will get a list of all events characterised
			// classname, subclass and nvpair only.
			//

			if (get_event_string(evt_class, evt_subclass,
			    nvstring, eventstr) == ENOMEM) {
				notify_client_list.erase_list();
				marked_event_list.erase_list();
				return (NULL);
			}

			event_list = client_event_hash.lookup(eventstr);

			//
			// will get a list of all client's interested in events
			// characterized by classname, subclassname and nvstring
			//
			if (get_client_list(event_list, marked_event_list,
			    notify_client_list) != 0) {
				notify_client_list.erase_list();
				marked_event_list.erase_list();
				return (NULL);
			}

			delete [] eventstr;
			delete [] nvstring;

		}
	}

	//
	// before returning from this function marked_event_list needs to
	// be updated, all events need to reset their match frequency to 0.
	//
	marked_event_list.atfirst();
	while ((eventp = marked_event_list.get_current()) != NULL)  {
		marked_event_list.advance();
		eventp->set_match_frequency(0);
	}

	// erase all elements from marked_event_list
	marked_event_list.erase_list();

	//
	// client notify is set to false so that client's are
	// notified for any new incoming events
	//
	notify_client_list.atfirst();
	while ((clientp = notify_client_list.get_current()) != NULL)  {
		notify_client_list.advance();
		clientp->set_notify(false);
	}

	//
	// for each client returned in the notify client list create a
	// client handle and return to caller
	//
	if (notify_client_list.count() > 0) {
		notify_client_handle_list = new SList<caapi_client_handle>();

		// if ENOMEM then return NULL
		if (notify_client_handle_list == NULL) {
			notify_client_list.erase_list();
			return (NULL);
		}

		notify_client_list.atfirst();
		while ((clientp = notify_client_list.get_current())
		    != NULL) {
			notify_client_list.advance();
			client_handlep = clientp->get_client_handle();
			notify_client_handle_list->append(client_handlep,
			    &allocation_status);
			// append failure.
			if (!allocation_status) {
				notify_client_list.erase_list();
				notify_client_handle_list->erase_list();
				delete notify_client_handle_list;
				return (NULL);

			}

		}
		notify_client_list.erase_list();
	}
	scds_syslog_debug(1, "\nevent_mapping:= sending all client interested "
	    "in the event==");


	return (notify_client_handle_list);
}

//
// update_mapping function updates the hash table with new list
// of events corresponding to a given client
// this function is called from caapi_reg_cache when
// a new client registers, adds or deletes some of its events
// or when is unregisters. The hash table maintains a
// mapping of events and all clients interested in it.
// the key for the hash-table is a string which comprises of
// one of the combinations..
// 	1. class1-subclass1-nvpair1
// 		associated with this key is a list of all events
//			consisting of class1, subclass1 and nvpair1
// 	2. class1-subclass1
//			associated with this key is a list of all events
//			consisting of class1 and subclass1 and no nvpairs
// 	3. class1
//			associated with this key is a list of all events
//			consisting of class1 no subclass and nvpairs
//	NOTE: each event has  back pointer to its client.
// For eg. if we have a CLIENT1 registered for 2 events
// EVENT1 has following properties, class="EC_CLUSTER",
// subclass = "ResourceGroupStateChange" name-value pairs.
// ResourceGroupName=foo-rg
// ResourceGroupState=Online
// and EVENT2, class="EC_CLUSTER",
// subclass = "ResourceGroupStateChange"
// ResourceGroupName=foo-rg
//
//  and another CLIENT2 registered for 1 event
// EVENT1, class="EC_CLUSTER"
// In the above case hashtable will have 3 key combinations:
// key1 : EC_CLUSTER:ResourceGroupStateChange:ResourceGroupName=foo-rg
//	Associated with this key is a list which has just event1
//	and event2 of client1.
// key2 : EC_CLUSTER:ResourceGroupStateChange:ResourceGroupState=Online
//	Associated with this key is a list which has just event1
// key3 : EC_CLUSTER::
//	Associated with this key is a list which has just event1
//	of client3.
//
// when a new sysevent is generated with properties
// class = EC_CLUSTER
// subclass = ResourceGroupStateChange
// nvpair = ResourceGroupState=foo-rg
// then all clients registered for events with above properties are
// notified, in the above case we have EVENT2 of CLIENT1 has same
// properties and event1 of CLIENT2 has same properties so
// for above event both clients are notified of the occurance of event
//
//

int
caapi_mapping::update_mapping(reg_type op, caapi_client *clientp)
{
	FAULTPT_CLAPI(FAULTNUM_CLAPI_UPDATE_MAPPING_B,
		FaultFunctions::generic);

#ifdef _FAULT_INJECTION
	void		*f_argp;
	uint32_t	f_argsize;
	if (fault_triggered(FAULTNUM_CLAPI_UPDATE_MAPPING_R,
		&f_argp, &f_argsize)) {
		ASSERT(f_argsize == sizeof (int));
		return (*((int *)f_argp));
	}
#endif
	caapi_event *eventp = NULL;
	SList<caapi_event> *event_listp =  NULL;
	int ret_value = 0;
	sc_syslog_msg_handle_t handle;

	// not a valid instance of client
	if (clientp == NULL)
		return (CLEP_ERR_INV);

	event_listp = clientp->get_events();

	event_listp->atfirst();
	while ((eventp = event_listp->get_current()) != NULL) {
		event_listp->advance();

		switch (op) {
		// select all events as the client has just registered
		case ADD_CLIENT:
			ret_value = update_event(op, eventp);
			break;

		case ADD_EVENTS:
			// if event back pointer is set NULL then this is a
			// new event
			if (eventp->get_client_backptr() != NULL)
				continue;

			ret_value = update_event(op, eventp);
			break;

		// remove_event is called when client request to remove event
		// or add_events is failed so we need to revoke the update to
		// mapping
		//
		case REMOVE_EVENTS:
		//
		// if event back pointer is set NULL then this is a new event
		// we have to remove this event as update to caapi_reg ccr table
		// failed or if event's  in_ccr is set to false
		// then this client needs to be removed
		//
			if ((eventp->get_client_backptr() == NULL) ||
			    (!eventp->is_in_ccr())) {

				ret_value = update_event(op, eventp);
			}
			break;
		case REMOVE_CLIENT:
			// all events should be removed
			ret_value = update_event(op, eventp);
			break;
		default:
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
			//
			// SCMSGS
			// @explanation
			// The cl_apid experienced an internal error that
			// prevented proper registration of a CRNP client.
			// @user_action
			// Examine other syslog messages occurring at about
			// the same time to see if the problem can be
			// identified. Save a copy of the /var/adm/messages
			// files on all nodes and contact your authorized Sun
			// service provider for assistance in diagnosing and
			// correcting the problem.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "Invalid registration operation");
			sc_syslog_msg_done(&handle);
		}
	}
	return (ret_value);
}

//
// get_event_string function concatenates the three in-parameters
// and returns a single string. this string is the key to lookup
// the hashtable.
//
// NOTE caller is responsible to deallocate the memory allocated
// to event_string
//
int
caapi_mapping::get_event_string(const char *evt_class_in,
			const char *evt_subclass_in,
			const char *nvstring_in,
			char *&eventstr_out)
{
	char *emptystr = "";
	uint_t eventstr_len = 0;
	const char *evt_subclass = NULL;
	const char *nvstring = NULL;

	//
	// if class not specified by client then it isnt a valid event
	// syslog the event listed in the client as not valid
	// return without adding the event to hashtable
	//
	if (evt_class_in == NULL)
		return (0);

	//
	// if subclass is NULL evt_subclass is set to emptystr
	// and hashtable is indexed by only
	// class EVENT_DELIMITER EVENT_DELIMITER
	// if nvstring_in is empty then hashtable is indexed by
	// class EVENT_DELIMITER subclass EVENT_DELIMITER
	// else hashtable indexed by
	// class EVENT_DELIMITER subclass EVENT_DELIMITER nvstring
	//
	//

	if (evt_subclass_in == NULL) {
		evt_subclass = emptystr;
		nvstring = emptystr;
	} else {
		evt_subclass = evt_subclass_in;
		if (nvstring_in == NULL)
			nvstring = emptystr;
		else
			nvstring = nvstring_in;
	}

	// dynamically allocated event_string length
	eventstr_len = strlen(evt_class_in) + strlen(evt_subclass) +
	    strlen(nvstring) + 2*strlen(EVENT_DELIMITER) + 1;

	// dynamically allocated event_string
	eventstr_out = new char[eventstr_len];

	if (eventstr_out == NULL)
		return (ENOMEM);

	(void) sprintf(eventstr_out, "%s%s%s%s%s", evt_class_in,
	    EVENT_DELIMITER,
	    evt_subclass,
	    EVENT_DELIMITER,
	    nvstring);

	// eventstr_out = eventstr;
	return (0);
}


int
caapi_mapping::update_event(reg_type op, caapi_event * eventp)
{
	const char *evt_class = NULL;
	const char *evt_subclass = NULL;
	const char *evt_nvname = NULL;
	const char *evt_nvvalue = NULL;
	char  *eventstr = NULL;
	nvstrpair *evt_nvpair = NULL;
	SList<nvstrpair> *nv_listp = NULL;
	char nvstring[2048];
	int ret_value = 0;

	ASSERT(eventp != NULL);

	evt_class = eventp->get_class();
	evt_subclass = eventp->get_subclass();

	nv_listp = const_cast<SList<nvstrpair>*> (
			&(eventp->get_nvlist()));
	//
	// if class not specified by client then it isnt a valid event
	// syslog the event listed in the client as not valid
	// return without adding the event to hashtable
	//
	if (evt_class == NULL)
		return (CLEP_ERR_INV);

	//
	// if subclass not specified by client, when client has registered
	// for all suncluster events key is combination of class
	// EVENT_DELIMITER EVENT_DELIMITER.
	//
	if (evt_subclass == NULL) {

		if (get_event_string(evt_class, NULL, NULL, eventstr) == ENOMEM)
			return (ENOMEM);

		ret_value = update_hashtable(op, eventstr, eventp);
		delete [] eventstr;

	} else if ((evt_subclass) && ((nv_listp == NULL) ||
	    nv_listp->count() == 0)) {

		if (get_event_string(evt_class, evt_subclass, NULL, eventstr)
		    == ENOMEM)
			return (ENOMEM);

		ret_value = update_hashtable(op, eventstr, eventp);
		delete [] eventstr;

	} else if ((evt_subclass) && (nv_listp)) {

		//
		// for each nvpair form a eventstr
		// classname:subclassname:name=value
		// for each eventstr lookup the hash and a get
		// get all clients associated with the eventstr
		//
		nv_listp->atfirst();
		while ((evt_nvpair = nv_listp->get_current())
				!= NULL) {
			nv_listp->advance();
			evt_nvname = evt_nvpair->get_name();
			evt_nvvalue = evt_nvpair->get_value_str();

			// NULL name-value are ignores
			if ((evt_nvname == NULL) || (evt_nvvalue == NULL))
				continue;
			// XXX dynamic alloc of nvstring ???
			// allocate for nvstring char array
			//	nvstring = new char[strlen(evt_nvname) +
			//	strlen(evt_nvvalue) + 2];

			// if (nvstring == NULL)
			// 	return (ENOMEM);
			(void) sprintf(nvstring, "%s%s%s", evt_nvname,
			    NV_DELIMITER, evt_nvvalue);

			//
			// will get a list of all events characterised
			// classname, subclass and nvpair only.
			//

			if (get_event_string(evt_class, evt_subclass,
			    nvstring, eventstr) == ENOMEM)
				return (ENOMEM);

			ret_value = update_hashtable(op, eventstr, eventp);
			// deallocate dynamically allocated members
			delete [] eventstr;
			// delete [] nvstring;
		}
	} else {
		CL_PANIC(false);
	}
	// return success
	return (ret_value);
}

//
// update_hashtable function updates the hashtable
// based on operation op ie either adds or removes new client_events
// associated with the eventstr which is a concatenated string of
// classname, subclassname and name-value pair
//
int caapi_mapping::update_hashtable(reg_type op,
		char *eventstr,
		caapi_event *eventp)
{
#ifdef _FAULT_INJECTION
	void		*f_argp;
	uint32_t	f_argsize;
	if (fault_triggered(FAULTNUM_CLAPI_MAPPING_UPDATE_HASH_R,
		&f_argp, &f_argsize)) {
		ASSERT(f_argsize == sizeof (int));
		return (*((int *)f_argp));
	}
#endif
	switch (op) {
		//
		// for case when client registers and
		// updates with new events
		//
		case ADD_CLIENT:
		case ADD_EVENTS:
			return (add_event(eventstr, eventp));
		case REMOVE_CLIENT:
		case REMOVE_EVENTS:
			return (remove_event(eventstr, eventp));
		default:
			// syslog invalid operation requested return failure
			return (CLEP_ERR_OP);
	}
}

//
// add_event function adds a new client-event
// associated with a particular eventstr key
// in the hashtable. if the eventstr key doesnt
// exists then new key is associated with the key is a list
// caapi_events and client_event is added to this list
//

int
caapi_mapping::add_event(char *eventstr_in, caapi_event *eventp)
{

#ifdef _FAULT_INJECTION
	void		*f_argp;
	uint32_t	f_argsize;
	if (fault_triggered(FAULTNUM_CLAPI_MAPPING_ADD_EVENT_R,
		&f_argp, &f_argsize)) {
		ASSERT(f_argsize == sizeof (int));
		return (*((int *)f_argp));
	}
#endif
	SList<caapi_event> *event_list = NULL;
	bool allocation_status;
	sc_syslog_msg_handle_t handle;

	event_list = client_event_hash.lookup(eventstr_in);
	//
	// if the key doesnt exists in hashtable add a new key = eventstr
	// to hash table and associated with it is a new SList of
	// caapi_event's
	//
	if (event_list == NULL) {
		event_list = new SList<caapi_event>();
		if (event_list == NULL) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "Error: low memory");
			sc_syslog_msg_done(&handle);

			// return ENOMEM
			return (ENOMEM);
		}
		client_event_hash.add(event_list, eventstr_in,
		    &allocation_status);
		if (!allocation_status) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_CLAPI_CLAPID_TAG, "");
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "Error: low memory");
			sc_syslog_msg_done(&handle);

			delete event_list;
			// return ENOMEM
			return (ENOMEM);
		}
		// add the event to list.
		event_list->append(eventp, &allocation_status);
		if (!allocation_status) {
			delete event_list;
			return (ENOMEM);
		}
	} else {
		// check if the element exists in the list, add otherwise
		if (!event_list->exists(eventp)) {
			event_list->append(eventp, &allocation_status);
			if (!allocation_status) {
				return (ENOMEM);
			}

		}
	}
	return (0);
}

//
// remove_event functions is called when client unregisters
// or requests to delete some of the events he is interested in.
// the selected event is deleted from the hashtable , if the event_list
// is empty then  this entry is deleted from the hashtable and
// eventlist is freed
//

int
caapi_mapping::remove_event(char *eventstr_in, caapi_event *eventp)
{
#ifdef _FAULT_INJECTION
	void		*f_argp;
	uint32_t	f_argsize;
	if (fault_triggered(FAULTNUM_CLAPI_MAPPING_REMOVE_EVENT_R,
		&f_argp, &f_argsize)) {
		ASSERT(f_argsize == sizeof (int));
		return (*((int *)f_argp));
	}
#endif
	SList<caapi_event> *event_list = NULL;

	event_list = client_event_hash.lookup(eventstr_in);

	// if no event_list associated with the key
	if (!event_list)
		return (0);

	// erase the event from the list
	if (!event_list->erase(eventp)) {

		// no need to syslog -- just add a debug message
		scds_syslog_debug(1, "caapi_mapping: event not found to "
		    "remove\n");
	} else {
		//
		// check if event_list is empty in that case
		// the key is rmeoved from hashtable as there
		// events associated with the key
		//
		if (event_list->empty()) {
			//
			// key is removed from the hashtable
			// list is deleted by the remove function
			//
			(void) client_event_hash.remove(eventstr_in);
		}// end if
	}
	// return success
	return (0);
}

int
caapi_mapping::dispose()
{
	client_event_hash.dispose();
	return (0);

}

//
// lookup_event_in_client: This function search's the hash-table and
// identifies the event which matches *exactly* with the
// incoming event. This functions is called when we need to
// lookup for a exact event match associated with a client mainly
// when the client adds a new set of events or deletes previously
// registered events
//
int
caapi_mapping::lookup_event_in_client(const caapi_client *clientp_in,
		const sc_event *eventp_in,
		caapi_event *&eventp_out)
{

	const char *evt_class = NULL;
	const char *evt_subclass = NULL;
	const char *evt_nvname = NULL;
	const char *evt_nvvalue = NULL;
	char  *eventstr = NULL;
	nvstrpair *evt_nvpair = NULL;
	SList <caapi_event>  *event_list = NULL;
	SList<nvstrpair> *nv_listp = NULL;
	SList <caapi_event>  marked_event_list;
	caapi_event *tmp_eventp = NULL;
	char *nvstring = NULL;
	uint_t nv_count_in = 0; // total name-value pairs associated
			    // the incoming event
	if (clientp_in == NULL)
		return (CLEP_ERR_INV);

	// return event not found
	if (eventp_in == NULL)
		return (CLEP_ERR_INV);

	evt_class = eventp_in->get_class();
	evt_subclass = eventp_in->get_subclass();

	nv_listp = const_cast<SList<nvstrpair>*> (
			&(eventp_in->get_nvlist()));

	nv_count_in = nv_listp->count();

	// if class not specified by client then it isnt a valid event
	if (evt_class == NULL)
		return (CLEP_ERR_INV);

	//
	// if subclass not specified by client
	// then event is characterized by only class name
	// key is combination of class EVENT_DELIMITER EVENT_DELIMITER
	//
	if (evt_subclass == NULL) {

		if (get_event_string(evt_class, NULL, NULL, eventstr) ==
			ENOMEM) {
			marked_event_list.erase_list();
			return (ENOMEM);
		}

		event_list = client_event_hash.lookup(eventstr);

		// eventstr is allocated in get_event_string function
		delete [] eventstr;
		//
		// find the event which matches exactly with the incoming event
		// and is registerted by the incoming client
		//
		(void) match_event_to_client(event_list,
		    marked_event_list,
		    clientp_in,
		    nv_count_in,
		    eventp_out);

	} else if ((evt_subclass) && ((nv_listp == NULL) ||
	    nv_listp->count() == 0)) {

		if (get_event_string(evt_class, evt_subclass, NULL, eventstr)
		    == ENOMEM) {
			marked_event_list.erase_list();
			return (ENOMEM);
		}

		event_list = client_event_hash.lookup(eventstr);

		// eventstr is allocated in get_event_string function
		delete [] eventstr;
		//
		// find the event which matches exactly with the incoming
		// event and is registerted by the incoming client
		//
		(void) match_event_to_client(event_list,
		    marked_event_list,
		    clientp_in,
		    nv_count_in,
		    eventp_out);

	} else if ((evt_subclass) && (nv_listp)) {

		//
		// for each nvpair form a eventstr
		// classname:subclassname:name=value
		// for each eventstr lookup the hash and a get
		// get all clients associated with the eventstr
		//
		nv_listp->atfirst();
		while ((evt_nvpair = nv_listp->get_current())
				!= NULL) {
			nv_listp->advance();
			evt_nvname = evt_nvpair->get_name();
			evt_nvvalue = evt_nvpair->get_value_str();

			// NULL nam-value are ignores
			if ((evt_nvname == NULL) || (evt_nvvalue == NULL))
				continue;
			// allocate for nvstring char array
			nvstring = new char[strlen(evt_nvname) +
			    strlen(evt_nvvalue) + 2];

			if (nvstring == NULL) {
				marked_event_list.erase_list();
				return (ENOMEM);
			}

			(void) sprintf(nvstring, "%s%s%s", evt_nvname,
			    NV_DELIMITER, evt_nvvalue);

			//
			// will get a list of all events characterised
			// classname, subclass and nvpair only.
			//

			if (get_event_string(evt_class, evt_subclass,
			    nvstring, eventstr) == ENOMEM) {
				marked_event_list.erase_list();
				return (ENOMEM);
			}

			event_list = client_event_hash.lookup(eventstr);
			//
			// find the event which matches exactly
			// with the incoming event and is registeted
			// by the incoming client, find event returns
			// 0 if all name -value pair of the incoming
			// event matches the event registered by the
			// client.
			//

			(void) match_event_to_client(event_list,
			    marked_event_list,
			    clientp_in,
			    nv_count_in,
			    eventp_out);

			// deallocate dynamically allocated members
			delete [] eventstr;
			delete [] nvstring;
		}
	}
	//
	// before returning from this function marked_event_list needs to
	// be updated . all events need to reset their match frquency to 0
	//
	marked_event_list.atfirst();
	while ((tmp_eventp = marked_event_list.get_current()) != NULL)  {
		marked_event_list.advance();
		tmp_eventp->set_match_frequency(0);
	}

	// erase all elements from marked_event_list
	marked_event_list.erase_list();

	// return success
	return (0);
}
//
// match_event_to_client is a helper function used by lookup_event function.
// this function  gets a list of events and client instance as in
// parameter and this function returns a event (if any) which
// is registered by the in-coming client
//
int
caapi_mapping::match_event_to_client(SList<caapi_event> *event_list,
				SList<caapi_event> & marked_event_list,
				const caapi_client *clientp_in,
				uint_t nv_count_in,
				caapi_event *&eventp_out)
{
	caapi_event *eventp = NULL;
	caapi_client *clientp = NULL;
	int ret_value = -1; // initialized with not found

	// no clients associated with event string
	if (event_list == NULL)
		return (CLEP_ERR_INV);

	event_list->atfirst();
	while ((eventp = event_list->get_current()) != NULL) {
		event_list->advance();
		//
		// if the event client pointer is set to NULL
		// then that event is not in active state
		// so all clients corresponding to such events
		// are ignored. we are concerned with events registered
		// by the incoming client only
		//
		clientp = eventp->get_client_backptr();
		if (clientp == NULL || clientp != clientp_in)
			continue;

		eventp->increment_frequency();
		//
		// inform client for the incoming event if the match frequency
		// of the event matches the name-value count of the event
		// match frequency is the count of the number of times a event
		// matches with the incoming event and name-value count is the
		// total number of name value associated with this event
		// registered by client . Also for a exact match of the event
		// the nv_count_in should be equal to the total name-value
		// associated with the event registered by the client.
		//
		if ((eventp->get_match_frequency() == eventp->get_nv_count()) &&
		    (eventp->get_num_nvpairs() == nv_count_in)) {
			// we have found a match
			ret_value = 0;
			eventp_out = eventp;
			marked_event_list.append(eventp);
			break;
		}  // end of if
		//
		// the event which is associated with atleast one of the
		// key's is stored in the marked_event_list so that the match
		// frequency of each client is reset to 0, once we get a final
		// list of clients associated with the incoming sysevent
		//
		if (eventp->get_match_frequency() == 1)
			marked_event_list.append(eventp);
	} // end of while
	return (ret_value);
}
#ifdef TEST_STUB
int
main()
{

	caapi_mapping event_mapping;

	caapi_client client1("101:20", "class1:subclass1");

	caapi_client client2("103:20", "class1:subclass1:"
		"name1=value1:name2=value2:name3=value3");

	caapi_client client3("105:20", "class1:subclass1:"
		"name1=value1");

	client1.set_client_backptr();
	client2.set_client_backptr();
	client3.set_client_backptr();

	nvstrpair nv1("name1", "value1");
	nvstrpair nv2("name3", "value3");
	nvstrpair nv3("name3", "value3");
	SList<nvstrpair> nv_list;
	nv_list.append(&nv1);
	nv_list.append(&nv2);
	nv_list.append(&nv3);

	sc_event event1("class1", "subclass1", nv_list);

	(void) event_mapping.update_mapping(ADD_CLIENT, &client1);
	(void) event_mapping.update_mapping(ADD_CLIENT, &client2);
	(void) event_mapping.update_mapping(ADD_CLIENT, &client3);

	SList<caapi_client_handle> *client_list = NULL;

	client_list = event_mapping.lookup_clients(&event1);

	(void) printf("\nevent generated = class1, subclass1,name1=value1"
	    "name2=value2,name3=value3");

	if (client_list) {
		printf("\n Total Clients Found = %d", client_list->count());
	}
	else
		printf("\n No Clients Found ");

	nv_list.erase(&nv3);
	nv_list.erase(&nv2);

	sc_event event2("class1", "subclass1", nv_list);
	client_list = event_mapping.lookup_clients(&event2);

	printf("\nevent generated = class1, subclass1,name1=value1");

	if (client_list)
		printf("\n Total Clients Found = %d", client_list->count());
	else
		printf("\n No Clients Found ");


	nv_list.erase(&nv1);

	sc_event event3("class1", "subclass1", nv_list);
	client_list = event_mapping.lookup_clients(&event3);

	printf("\nevent generated = class1, subclass1");

	if (client_list)
		printf("\n Total Clients Found = %d", client_list->count());
	else
		printf("\n No Clients Found ");

/*
	caapi_event *eventp = NULL;
	if (event_mapping.lookup_event_in_client(&client1, &event1, eventp)
	    == 0) {
		printf("\n Event Found ");
		printf("\n classname = %s", eventp->get_class());
	}
	else
		printf("\n Event Not Found ");

	if (event_mapping.lookup_event_in_client(&client2, &event1, eventp)
	    == 0) {
		printf("\n Event Found ");
		printf("\n classname = %s", eventp->get_class());
	}
	else
		printf("\n Event Not Found ");
*/
	event_mapping.dispose();
	nv_list.erase_list();
	client_list->erase_list();
}
#endif
