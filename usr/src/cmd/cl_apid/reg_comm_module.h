/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_REG_COMM_MODULE_H_
#define	_REG_COMM_MODULE_H_

#pragma ident	"@(#)reg_comm_module.h	1.9	08/05/20 SMI"

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#include "sc_registration_instance.h"
#include "sc_queue.h"
#include "sc_xml_reply.h"
#include "xml_module.h"
#include "tcp_wrapper.h"

/*
 * We don't have a default constructor, and we don't want one.*/
/*lint -e1712 */
class reg_comm_module
{
public:

	/*
	 * initialize
	 * ---------
	 * This method should be called exactly once in a program to
	 * create and initialize the one instance of reg_comm_module.
	 *
	 * Note: the reg_comm_module will make its own copy of wrapper.
	 * The original should be freed by the caller.
	 */
	static int initialize(char *ip, int port, tcp_wrapper &wrapper,
	    int client_timeout);

	/*
	 * shutdown
	 * -----------
	 * Called on the reg_comm_module object to tell it to destroy itself.
	 * There are two phases.  The first should halt all dependencies on
	 * other modules.  The second should finish the deletion.
	 */
	void shutdown_start();
	void shutdown_finish();

	/*
	 * the
	 * -----------
	 * Returns a reference to the one reg_comm_module object.
	 */
	static reg_comm_module *the();

	/*
	 * reload
	 * ---------
	 * This method can be called to change the parameters specified
	 * originally in the initialize method.
	 */
	int reload(tcp_wrapper &wrapper_in, int client_timeout);

	/*
	 * send_reply_message
	 * ----------------
	 * Requests that the reg_comm_module send the msg to the sock.
	 * This method will return immediately, before having actually sent
	 * the message.  It will make its own copy of the memory for the msg,
	 * so the original can be deleted.
	 */
	int send_reply_message(int sock, const char *msg);

	/*
	 * Thread routines.  They are public so that the extern "C" defined
	 * wrapper functions called by pthread_create can call the
	 * actual methods on the reg_comm_module object.
	 */
	void recept_thread();
	void send_thread();

private:
	/*
	 * ctor/dtor are private so only the static initialize method
	 * and shutdown method can create/destroy it.
	 */
	reg_comm_module(char *ip, int port, tcp_wrapper &wrapper_in,
	    int timeout_in);
	~reg_comm_module();

	/*
	 * Disallow pass by value, assignment
	 */
	reg_comm_module(const reg_comm_module &);
	/* Cstyle doesn't understand operator overloading */
	/*CSTYLED */
	reg_comm_module &operator=(const reg_comm_module &);

	/*
	 * Our ip and port on which we listen.
	 * It's not really a "callback_info" per se, but the
	 * class works here.
	 */
	sc_callback_info *net_name;

	/*
	 * For the "tcp wrappers"
	 */
	tcp_wrapper *wrapper;
	pthread_mutex_t wrapper_lock;

	/*
	 * The client timeout value.
	 */
	time_t client_timeout;

	/*
	 * Helper class for storing the socket and msg of a client
	 * to whom we need to send a reply.  We need to combine them
	 * into a class so that we can store it on the send_queue.
	 */
	class msg_reply {
	public:
		msg_reply(int sock_in, const char *msg_in) {
			sock = sock_in;
			msg = os::strdup(msg_in);
		}
		~msg_reply() { delete msg; }
		int sock;
		char *msg;
	};

	/*
	 * Declare the queue structure for queueing the incoming
	 * messages
	 *
	 * The queue has an associated mutex and condition variable.
	 */
	sc_queue<msg_reply> send_queue;

	pthread_mutex_t send_lock;
	pthread_cond_t send_cond;


	/*
	 * We have a flag for the send thread, in case we want to
	 * shut it down.
	 */
	bool shutdown_send;

	/*
	 * We use a pipe to signal the reception thread that
	 * the module is shutting down.
	 */
	int signal_pipe[2];

	/*
	 * Variables to keep track of the thread ids for our two threads.
	 */
	static pthread_t recept_thr, send_thr;

	/*
	 * The one instance of reg_comm_module
	 */
	static reg_comm_module *the_reg_comm_module;


	/*
	 * Struct used to track a current client registration connection
	 * from which we're awaiting data.
	 */
	typedef struct {
		int sock;
		string_buffer buf;
		/*
		 * we're not really using IPv6, but I wanted to make
		 * sure there's enough space.
		 */
		char host[INET6_ADDRSTRLEN];
		uint32_t host_n;
		int port;

		/*
		 * connection_timeout keeps track of the time at which this
		 * connection will timeout if there is no response/activity
		 * from the client.
		 */
		time_t connection_timeout;
	} reg_cli_connect;

	/* helper methods */
	void create_registration(reg_cli_connect *client,
	    sc_registration_instance *new_reg);
	void check_accepted_connections(const fd_set &rset, fd_set *allset,
	    hashtable_t<reg_cli_connect *, int> *conns, int nready);
	time_t check_timedout_clients(fd_set *allset,
	    hashtable_t<reg_cli_connect *, int> *conns);
	void check_listening_socket(int listenfd, const fd_set &rset,
	    fd_set *allset, hashtable_t<reg_cli_connect *, int> *conns,
	    int *maxfd, int *nready);

	/* a reply for a non-authenticated client */
	sc_xml_reply *authent_reply;
};
/*lint +e1712 */

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#endif /* _REG_COMM_MODULE_H_ */
