/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#pragma ident "@(#)event_update.c 1.5 08/05/20 SMI"

/*
 * event_update.c - Update method for event
 */

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <rgm/libdsdev.h>

/*
 * Some of the resource properties might have been updated. All such
 * updatable properties are related to the fault monitor. Hence, just
 * restarting the monitor should be enough.
 * However, cl_apid also declares many of its properties to
 * by dynamically tunable. We send a SIGHUP to it and rely on
 * cl_apid to re-read properties from CCR.
 */

int
main(int argc, char *argv[])
{
	scds_handle_t   scds_handle;
	scha_err_t	result;

	/* Process the arguments passed by RGM and initialize syslog */
	if (scds_initialize(&scds_handle, argc, argv) != SCHA_ERR_NOERR)
		return (1);

	/*
	 * check if the Fault monitor is already running and if so stop and
	 * restart it. The second parameter to scds_pmf_restart_fm() uniquely
	 * identifies the instance of the fault monitor that needs to be
	 * restarted.
	 */

	result = scds_pmf_restart_fm(scds_handle, 0);
	if (result != SCHA_ERR_NOERR) {
		/*
		 * SCMSGS
		 * @explanation
		 * The resource property that was updated needed the fault
		 * monitor to be restarted inorder for the change to take
		 * effect, but the attempt to restart the fault monitor
		 * failed.
		 * @user_action
		 * Look at the prior syslog messages for specific problems.
		 * Correct the errors if possible.
		 *
		 * Look for the process <dataservice>_probe operating on the
		 * desired resource (indicated by the argument to "-R"
		 * option). This can be found from the command: ps -ef | egrep
		 * <dataservice>_probe | grep "\-R <resourcename>" Send a kill
		 * signal to this process.
		 *
		 * If the process does not get killed and restarted by the
		 * process monitor facility, reboot the node.
		 */
		scds_syslog(LOG_ERR,
		    "Failed to restart fault monitor.");
		/* Free up all the memory allocated by scds_initialize */
		scds_close(&scds_handle);
		return (1);
	}

	/*
	 * SIGHUP the cl_apid daemon to re-read
	 * properties from CCR
	 */
	result = scds_pmf_signal(scds_handle, SCDS_PMF_TYPE_SVC, 0,
		SIGHUP, 0);
	if (result != SCHA_ERR_NOERR) {
		/*
		 * SCMSGS
		 * @explanation
		 * The update method for the SUNW.Event service was unable to
		 * notify the cl_apid following changes to property values.
		 * @user_action
		 * Examine other syslog messages occurring at about the same
		 * time to see if the problem can be identified. Save a copy
		 * of the /var/adm/messages files on all nodes and contact
		 * your authorized Sun service provider for assistance in
		 * diagnosing and correcting the problem.
		 */
		scds_syslog(LOG_ERR,
		    "Failure in sending signal to cl_apid.");
		/* Free up all the memory allocated by scds_initialize */
		scds_close(&scds_handle);
		return (1);
	}



	/*
	 * SCMSGS
	 * @explanation
	 * Data service method completed successfully.
	 * @user_action
	 * No action required.
	 */
	scds_syslog(LOG_INFO,
	    "Completed successfully.");

	/* Free up all the memory allocated by scds_initialize */
	scds_close(&scds_handle);

	return (0);
}
