/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident "@(#)event_probe.c 1.4 08/05/20 SMI"

/*
 * event_probe.c - Probe for event
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <rgm/libdsdev.h>
#include "event.h"

int
main(int argc, char *argv[])
{
	scds_handle_t	scds_handle;

	int		timeout;
	int		port, probe_result;
	hrtime_t	ht1, ht2;
	long	dt;
	scds_netaddr_list_t *netaddr;
	char *hostname;

	if (scds_initialize(&scds_handle, argc, argv) != SCHA_ERR_NOERR)
		return (1);

	/* Get the ip addresses available for this resource */
	if (scds_get_netaddr_list(scds_handle, &netaddr)) {
		scds_syslog(LOG_ERR,
		    "No network address resource in resource group.");
		scds_close(&scds_handle);
		return (1);
	}

	/* Return an error if there are no network resources */
	if (netaddr == NULL || netaddr->num_netaddrs == 0) {
		scds_syslog(LOG_ERR,
		    "No network address resource in resource group.");
		return (1);
	}

	/*
	 * Get the timeout from the extension props. This means that
	 * each probe iteration will get a full timeout on each network
	 * resource without chopping up the timeout between all of the
	 * network resources configured for this resource.
	 */
	timeout = scds_get_ext_probe_timeout(scds_handle);

	for (;;) {

		/*
		 * sleep for a duration of thorough_probe_interval between
		 *  successive probes.
		 */
		(void) scds_fm_sleep(scds_handle,
		    scds_get_rs_thorough_probe_interval(scds_handle));


		/*
		 * Probe on the first hostname:port
		 * Grab the hostname and port on which the
		 * health has to be monitored.
		 */
		hostname = netaddr->netaddrs[0].hostname;
		port = netaddr->netaddrs[0].port_proto.port;
		ht1 = gethrtime(); /* Latch probe start time */

		probe_result = svc_probe(scds_handle, hostname, port,
		    timeout);

		ht2 = gethrtime();

		/* Convert to milliseconds */
		dt = (long)((ht2 - ht1) / 1e6);

		/*
		 * Compute failure history and take action if needed
		 */
		(void) scds_fm_action(scds_handle, probe_result, dt);

	} 	/* Keep probing forever */
}
