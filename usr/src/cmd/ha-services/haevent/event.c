/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * event.c - Common utilities for SUNW.Event RT
 */

#pragma ident	"@(#)event.c	1.17	09/01/06 SMI"

/*
 * This utility has the methods for performing the validation,
 * starting and stopping the data service and the fault monitor.
 * It also contains the method to probe the health of the
 * data service.  The probe just returns either success or
 * failure. Action is taken based on this returned value in the
 * method found in the file event_probe.c
 */

#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <locale.h>
#include <libintl.h>
#include <scha.h>
#include <rgm/libdsdev.h>
#include <errno.h>
#include "event.h"
#include <dlfcn.h>
#include <sys/sol_version.h>
#if SOL_VERSION >= __s10
#include <zone.h>
#endif

/*
 * The initial timeout allowed  for the event dataservice to
 * be fully up and running. We will wait for for 3 % (SVC_WAIT_PCT)
 * of the start_timeout time before probing the service.
 */
#define	SVC_WAIT_PCT		1

/*
 * We need to use 95% of probe_timeout to connect to the port and the
 * remaining time is used to disconnect from port in the svc_probe function.
 */
#define	SVC_CONNECT_TIMEOUT_PCT		95

/*
 * We need to wait for SVC_WAIT_TIME ( 2 secs) for pmf
 * to send the failure message before probing the service
 */

#define	SVC_WAIT_TIME		2

/*
 * This value will be used as disconnect timeout, if there is no
 * time left from the probe_timeout.
 */

#define	SVC_DISCONNECT_TIMEOUT_SECONDS		2

/*
 * validate_hlist()
 * Validates that the specified list of
 * hosts are in valid format.
 */
static int
validate_hlist(scha_str_array_t *hlist)
{
	char	*h = NULL, *ip = NULL, *mask = NULL, *endp;
	uint32_t	numeric_ip[4];
	uint_t		i;
	long		numeric_mask;
	int		rc = 0;

	if (hlist == NULL) {
		/* Empty is fine. Means deny all */
		return (0);
	}
	/* A '*' maps into is_ALL_value */
	if (hlist->is_ALL_value) {
		return (0);
	}
	for (i = 0; i < hlist->array_cnt; i++) {
		h = strdup(hlist->str_array[i]);

		if (h == NULL) {
			scds_syslog(LOG_ERR, "Out of memory.");
			rc = 1; /* Validation Failure */
			break;
		}
		/* Check for valid special values */
		if (!(strncmp(h, "ALL", 4) && strncmp(h, "*", 2) &&
			strncmp(h, "LOCAL", 6))) {
			free(h);
			continue;
		}

		/* Not special. Hence <ip>/<masklen> */
		ip = strtok(h, "/");
		if (ip == NULL) {
			scds_syslog(LOG_ERR,
				"invalid IP address in hosts list: %s",
				"NULL");
			rc = 1; /* Validation Failure */
			break;
		}
		/* Convert into numeric */
		if (inet_pton(AF_INET, ip, numeric_ip) != 1) {
			scds_syslog(LOG_ERR,
				"invalid IP address in hosts list: %s",
				ip);
			rc = 1; /* Validation Failure */
			break;
		}
		/* Get the mask */
		mask = strtok(NULL, "/");
		if (mask == NULL) {
			scds_syslog(LOG_ERR,
				"invalid mask in hosts list: %s",
				"NULL");
			rc = 1; /* Validation Failure */
			break;
		}
		/* Mask must be between 1 and 32 inclusive */
		numeric_mask = strtol(mask, &endp, 0);

		/*lint -save -e746 */
		if (numeric_mask == 0 && errno != 0) {
			scds_syslog(LOG_ERR,
			    "strtol: %s", strerror(errno));
			rc = 1;
			break;
		}

		if (*endp != '\0') {
			/*
			 * SCMSGS
			 * @explanation
			 * The allow_hosts or deny_hosts for the CRNP service
			 * contains an invalid mask.  This error may prevent
			 * the cl_apid from starting up.
			 * @user_action
			 * Remove the offending IP address from the
			 * allow_hosts or deny_hosts property, or fix the mask.
			 */
			scds_syslog(LOG_ERR,
			    "invalid mask in hosts list: <%s> (%d)",
			    mask, numeric_mask);
			rc = 1;
			break;
		}

		if (numeric_mask < 1 || numeric_mask > 32) {
			scds_syslog(LOG_ERR,
			    "invalid mask in hosts list: <%s> (%d)",
			    mask, numeric_mask);
			rc = 1; /* Validation Failure */
			break;
		}
		/*lint -restore */
		/* Looks good */
		free(h);
		h = NULL;
	}

	/*
	 * Lint complains the h below might not be initialized.
	 * we do know that it is set to NULL at initialization.
	 * Hence we ignore the warning.
	 */
	/*lint -save -e771 */
	if (h)
		free(h);
	/*lint -restore */
	return (rc);
}

/*
 * svc_validate():
 *
 * Do event specific validation of the resource configration.
 *
 */

int
svc_validate(scds_handle_t scds_handle)
{
	struct stat statbuf;
	char *cmd = "/usr/cluster/lib/sc/cl_apid";
	int rc = 0;
	scds_net_resource_list_t *snrlp = NULL;
	scds_port_list_t *portlist = NULL;
	scha_err_t	e = SCHA_ERR_NOERR;
	scha_extprop_value_t	*xprop = NULL;
	void	*libxml_handle = NULL;

#if SOL_VERSION >= __s10
	zoneid_t	zoneid;
#endif
	(void) setlocale(LC_ALL, "");
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);
	(void) textdomain(TEXT_DOMAIN);

#if SOL_VERSION >= __s10
	if ((zoneid = getzoneid()) < 0) {
		(void) fprintf(stderr, gettext("getzoneid() failed."
		    " Unable to verify privilege."));
		return (1);
	}
	if (zoneid != 0) {
		(void) fprintf(stderr, gettext("The service can't be hosted in "
		    "non-global zone"));
		return (1);
	}
#endif

	if (stat(cmd, &statbuf) != 0) {
		(void) fprintf(stderr,
		    gettext("Failed to access <%s>: <%s>"),
		    cmd, strerror(errno));	/*lint !e746 */
		rc = 1;
		goto finished;
	}

	if (!(statbuf.st_mode & S_IXUSR)) {
		(void) fprintf(stderr,
		    gettext("The %s command does not have execute "
		    "permissions."), cmd);
		rc = 1;
		goto finished;
	}

	/*
	 * Network aware service should have at least one port specified
	 */

	rc = scds_get_port_list(scds_handle, &portlist);
	if (rc != SCHA_ERR_NOERR) {
		(void) fprintf(stderr,
		    gettext("Failed to retrieve the resource property %s: %s."),
		    SCHA_PORT_LIST, scds_error_string(rc));
		goto finished;
	}

	if (portlist == NULL || portlist->num_ports < 1) {
		(void) fprintf(stderr,
		    gettext("Property %s is not set."), SCHA_PORT_LIST);
		rc = 1;
		goto finished;
	}

	if (portlist->num_ports != 1) {
		(void) fprintf(stderr,
		    gettext("Multiple ports specified in property %s."),
		    SCHA_PORT_LIST);
		rc = 1;
		goto finished;
	}

	/*
	 * Return an error if there is an error when trying to get the
	 * available network address resources for this resource
	 */
	if ((rc = scds_get_rs_hostnames(scds_handle, &snrlp))
		!= SCHA_ERR_NOERR) {
		(void) fprintf(stderr,
		    gettext("Error in trying to access the configured network "
		    "resources : %s."), scds_error_string(rc));
		goto finished;
	}

	/* Return an error if there are no network address resources */
	if (snrlp == NULL || snrlp->num_netresources == 0) {
		(void) fprintf(stderr,
		    gettext("No network address resource in resource group."));
		rc = 1;
		goto finished;
	}

	/* Check to make sure other important extension props are set */
	if (scds_get_ext_monitor_retry_count(scds_handle) <= 0) {
		(void) fprintf(stderr,
		    gettext("Monitor_retry_count or Monitor_retry_interval "
		    "is not set."));
		rc = 1; /* Validation Failure */
		goto finished;
	}
	if (scds_get_ext_monitor_retry_interval(scds_handle) <= 0) {
		(void) fprintf(stderr,
		    gettext("Monitor_retry_count or Monitor_retry_interval "
		    "is not set."));
		rc = 1; /* Validation Failure */
		goto finished;
	}

	/* Make sure libxml2 is available */
	libxml_handle = dlopen("libxml2.so", RTLD_LAZY);
	if (libxml_handle == NULL) {
		(void) fprintf(stderr,
		    gettext("Unable to open libxml2.so."));
		rc = 1; /* Validation Failure */
		goto finished;
	}
	(void) dlclose(libxml_handle);

	/* Check Allow_hosts property */
	e = scds_get_ext_property(scds_handle, "Allow_hosts",
		SCHA_PTYPE_STRINGARRAY, &xprop);
	if (e != SCHA_ERR_NOERR) {
		(void) fprintf(stderr,
		    gettext("Failed to retrieve the property %s: %s."),
		    "Allow_hosts", scds_error_string(e));
		rc = 1; /* Validation Failure */
		goto finished;
	}
	if (validate_hlist(xprop->val.val_strarray) != 0) {
		rc = 1;	/* Validation failure */
		goto finished;
	}
	scds_free_ext_property(xprop);
	xprop = NULL;

	/* Check Deny_hosts property */
	e = scds_get_ext_property(scds_handle, "Deny_hosts",
		SCHA_PTYPE_STRINGARRAY, &xprop);
	if (e != SCHA_ERR_NOERR) {
		(void) fprintf(stderr,
		    gettext("Failed to retrieve the property %s: %s."),
		    "Deny_hosts", scds_error_string(e));
		rc = 1; /* Validation Failure */
		goto finished;
	}
	if (validate_hlist(xprop->val.val_strarray) != 0) {
		rc = 1;	/* Validation failure */
		goto finished;
	}
	/* All validation checks were successful */
	rc = 0;


finished:

	scds_free_net_list(snrlp);
	scds_free_port_list(portlist);
	if (xprop)
		scds_free_ext_property(xprop);

	return (rc); /* return result of validation */
}

/*
 * svc_start():
 *
 */

int
svc_start(scds_handle_t scds_handle, int argc, char *argv[])
{
	int rc = 0;
	int	i;
	char cmd[2048];

	(void) strcpy(cmd, "/usr/cluster/lib/sc/cl_apid");
	for (i = 1; i < argc; i++) {
		(void) strcat(cmd, " ");
		(void) strcat(cmd, argv[i]);
	}

	/*
	 * Start event under PMF.
	 */

	rc = scds_pmf_start(scds_handle, SCDS_PMF_TYPE_SVC, 0, cmd,
	    -1);

	if (rc == SCHA_ERR_NOERR) {
		/*
		 * SCMSGS
		 * @explanation
		 * The start command of the application completed
		 * successfully.
		 * @user_action
		 * No action required.
		 */
		scds_syslog(LOG_INFO,
		    "Start of %s completed successfully.", cmd);
	} else {
		/*
		 * SCMSGS
		 * @explanation
		 * Sun Cluster could not start the application. It would
		 * attempt to start the service on another node if possible.
		 * @user_action
		 * 1) Check prior syslog messages for specific problems and
		 * correct them.
		 *
		 * 2) This problem may occur when the cluster is under load
		 * and Sun Cluster cannot start the application within the
		 * timeout period specified. Consider increasing the
		 * Start_timeout property.
		 *
		 * 3) If the resource was unable to start on any node,
		 * resource would be in the START_FAILED state. In this case,
		 * use clresourcegroup to bring the resource ONLINE on this
		 * node.
		 *
		 * 4) If the service was successfully started on another node,
		 * attempt to restart the service on this node by using
		 * clresourcegroup.
		 *
		 * 5) If the above steps do not help, disable the resource by
		 * using clresource. Check to see that the application can run
		 * outside of the Sun Cluster framework. If it cannot, fix any
		 * problems specific to the application, until the application
		 * can run outside of the Sun Cluster framework. Enable the
		 * resource by using clresource. If the application runs
		 * outside of the Sun Cluster framework but not in response to
		 * starting the data service, contact your authorized Sun
		 * service provider for assistance in diagnosing the problem.
		 */
		scds_syslog(LOG_ERR,
		    "Failed to start %s.", cmd);
		goto finished;
	}

finished:

	return (rc); /* return Success/failure status */
}

/*
 * svc_stop():
 *
 * Stop the event server
 * Return 0 on success, > 0 on failures.
 *
 */
int
svc_stop(scds_handle_t scds_handle)
{
	int rc = 0;


	/*
	 * If no stop command is specified, we use
	 * scds_pmf_stop to stop the application.
	 */
	if ((rc = scds_pmf_stop(scds_handle, SCDS_PMF_TYPE_SVC,
	    0, SIGTERM,
	    scds_get_rs_stop_timeout(scds_handle))) !=
	    SCHA_ERR_NOERR) {
		/*
		 * SCMSGS
		 * @explanation
		 * Sun Cluster failed to stop the application.
		 * @user_action
		 * Use process monitor facility (pmfadm (1M)) with -L option
		 * to retrieve all the tags that are running on the server.
		 * Identify the tag name for the application in this resource.
		 * This can be easily identified as the tag ends in the string
		 * ".svc" and contains the resource group name and the
		 * resource name. Then use pmfadm (1M) with -s option to stop
		 * the application.
		 *
		 * This problem may occur when the cluster is under load and
		 * Sun Cluster cannot stop the application within the timeout
		 * period specified. Consider increasing the Stop_timeout
		 * property.
		 *
		 * If the error still persists, then reboot the node.
		 */
		scds_syslog(LOG_ERR,
		    "Failed to stop %s.", "the application");
		/*
		 * Since the Data service did not stop with a
		 * scds_pmf_stop, we return non-zero.
		 */
		goto finished;
	}

finished:

	if (rc == SCHA_ERR_NOERR)
		/*
		 * SCMSGS
		 * @explanation
		 * The resource was successfully stopped by Sun Cluster.
		 * @user_action
		 * No user action is required.
		 */
		scds_syslog(LOG_INFO, "Successfully stopped %s.",
		    "the application");

	return (rc); /* Successfully stopped */
}

/*
 * svc_wait():
 *
 * wait for the data service to start up fully and make sure
 * it is running healthy.
 */

int
svc_wait(scds_handle_t scds_handle)
{
	int rc, svc_start_timeout;
	long probe_timeout;
	scds_netaddr_list_t	*netaddr;

	/* obtain the network resource to use for probing */
	/*
	 * SCMSGS
	 * @explanation
	 * A resource has no associated network address.
	 * @user_action
	 * For a failover data service, add a network address resource
	 * to the resource group. For a scalable data service, add a
	 * network resource to the resource group referenced by the
	 * RG_dependencies property.
	 */

	if (scds_get_netaddr_list(scds_handle, &netaddr)) {
		scds_syslog(LOG_ERR,
		    "No network address resource in resource group.");
		return (1);
	}

	/* Return an error if there are no network resources */
	if (netaddr == NULL || netaddr->num_netaddrs == 0) {
		scds_syslog(LOG_ERR,
		    "No network address resource in resource group.");
		return (1);
	}

	/*
	 * Get the Start method timeout, port number on which to probe,
	 * the Probe timeout value
	 */
	svc_start_timeout = scds_get_rs_start_timeout(scds_handle);
	probe_timeout = scds_get_ext_probe_timeout(scds_handle);

	/*
	 * sleep for SVC_WAIT_PCT percentage of start_timeout time
	 * before actually probing the dataservice. This is to allow
	 * the dataservice to be fully up inorder to reply to the
	 * probe. NOTE: the value for SVC_WAIT_PCT could be different
	 * for different dataservices.
	 */
	if (scds_svc_wait(scds_handle, (svc_start_timeout * SVC_WAIT_PCT)/100)
	    != SCHA_ERR_NOERR) {
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		scds_syslog(LOG_ERR, "Failed to start service.");
		return (1);
	}

	do {
		/*
		 * probe the data service on the IP address of the
		 * network resource and the portname
		 */
		rc = svc_probe(scds_handle,
		    netaddr->netaddrs[0].hostname,
		    netaddr->netaddrs[0].port_proto.port, probe_timeout);
		if (rc == SCHA_ERR_NOERR) {
			/* Success. Free up resources and return */
			scds_free_netaddr_list(netaddr);
			return (SCHA_ERR_NOERR);
		}

		/*
		 * Dataservice is still trying to come up. Sleep for a while
		 * before probing again.
		 */
		if (scds_svc_wait(scds_handle, SVC_WAIT_TIME)
			!= SCHA_ERR_NOERR) {
			scds_syslog(LOG_ERR, "Failed to start service.");
			return (1);
		}


	/* We rely on RGM to timeout and terminate the program */
	} while (1);

}

/*
 * This function starts the fault monitor for a event resource.
 * This is done by starting the probe under PMF. The PMF tag
 * is derived as <RG-name,RS-name,instance_number.mon>. The restart option
 * of PMF is used but not the "infinite restart". Instead
 * interval/retry_time is obtained from the RTR file.
 */

int
mon_start(scds_handle_t scds_handle)
{
	scha_err_t	err;

	scds_syslog_debug(DBG_LEVEL_HIGH,
	    "Calling MONITOR_START method for resource <%s>.",
	    scds_get_resource_name(scds_handle));

	/*
	 * The probe event_probe is assumed to be available in the same
	 * subdirectory where the other callback methods for the RT are
	 * installed. The last parameter to scds_pmf_start denotes the
	 * child monitor level. Since we are starting the probe under PMF
	 * we need to monitor the probe process only and hence we are using
	 * a value of 0.
	 */

	err = scds_pmf_start(scds_handle, SCDS_PMF_TYPE_MON,
	    SCDS_PMF_SINGLE_INSTANCE, "event_probe", 0);

	if (err != SCHA_ERR_NOERR) {
		/*
		 * SCMSGS
		 * @explanation
		 * The fault monitor for this data service was not started.
		 * There may be prior messages in syslog indicating specific
		 * problems.
		 * @user_action
		 * The user should correct the problems specified in prior
		 * syslog messages.
		 *
		 * This problem may occur when the cluster is under load and
		 * Sun Cluster cannot start the application within the timeout
		 * period specified. Consider increasing the
		 * Monitor_Start_timeout property.
		 *
		 * Try switching the resource group to another node by using
		 * clresourcegroup.
		 */
		scds_syslog(LOG_ERR, "Failed to start fault monitor.");
		return (err);
	}

	/*
	 * SCMSGS
	 * @explanation
	 * The fault monitor for this data service was started successfully.
	 * @user_action
	 * No action needed.
	 */
	scds_syslog(LOG_INFO, "Started the fault monitor.");

	return (SCHA_ERR_NOERR); /* Successfully started Monitor */
}


/*
 * This function stops the fault monitor for a event resource.
 * This is done via PMF. The PMF tag for the fault monitor is
 * constructed based on <RG-name_RS-name,instance_number.mon>.
 */

int
mon_stop(scds_handle_t scds_handle)
{
	scha_err_t	err;

	scds_syslog_debug(DBG_LEVEL_HIGH, "Calling scds_pmf_stop method");

	err = scds_pmf_stop(scds_handle, SCDS_PMF_TYPE_MON,
	    SCDS_PMF_SINGLE_INSTANCE, SIGKILL,
	    scds_get_rs_monitor_stop_timeout(scds_handle));

	if (err != SCHA_ERR_NOERR) {
		/*
		 * SCMSGS
		 * @explanation
		 * An attempt was made to stop the fault monitor and it
		 * failed. There may be prior messages in syslog indicating
		 * specific problems.
		 * @user_action
		 * If there are prior messages in syslog indicating specific
		 * problems, these should be corrected. If that doesn't
		 * resolve the issue, the user can try the following.
		 *
		 * Use process monitor facility (pmfadm (1M)) with -L option
		 * to retrieve all the tags that are running on the server.
		 * Identify the tag name for the fault monitor of this
		 * resource. This can be easily identified as the tag ends in
		 * string ".mon" and contains the resource group name and the
		 * resource name. Then use pmfadm (1M) with -s option to stop
		 * the fault monitor.
		 *
		 * This problem may occur when the cluster is under load and
		 * Sun Cluster cannot stop the fault monitor within the
		 * timeout period specified. Consider increasing the
		 * Monitor_Stop_timeout property.
		 *
		 * If the error still persists, then reboot the node.
		 */
		scds_syslog(LOG_ERR, "Failed to stop fault monitor.");
		return (err);
	}

	/*
	 * SCMSGS
	 * @explanation
	 * The fault monitor for this data service was stopped successfully.
	 * @user_action
	 * No action needed.
	 */
	scds_syslog(LOG_INFO, "Stopped the fault monitor.");

	return (SCHA_ERR_NOERR); /* Successfully stopped monitor */
}


/*
 * svc_probe(): Do data service specific probing. Return a float value
 * between 0 (success) and 100(complete failure).
 *
 * The probe does a simple socket connection to the event server on
 * the specified port which is configured as the resource extension
 * property (Port_list) and pings the dataservice. If the probe fails
 * to connect to the port, we return a value of 100 indicating that
 * there is a total failure. If the connection goes through and the
 * disconnect to the port fails, then a value of 50 is
 * returned indicating a partial failure.
 *
 * XXX We need a way to be able to contact cl_apid and get back
 * a response. Simply connecting and disconnecting to the TCP
 * port would appear like a communication error to the server
 * (cl_apid) and it would syslog that.
 * Fake a successful probe by returning 0 from here. We still
 * take action if cl_apid dies.
 */
int
svc_probe(scds_handle_t scds_handle, char *hostname, int port,
	long timeout)
{
	int  rc = 0;
	int probe_status = 0;
	hrtime_t	t1, t2;
	int 	sock;
	double 	time_used;
	long	time_remaining;
	long	connect_timeout;
	scha_err_t	err;
	scds_pmf_status_t  status;

	err = scds_pmf_get_status(scds_handle,
	    SCDS_PMF_TYPE_SVC, SCDS_PMF_SINGLE_INSTANCE, &status);
	if (err != SCHA_ERR_NOERR) {
		/*
		 * SCMSGS
		 * @explanation
		 * Failed to create the tag that is used to register with the
		 * process monitor facility.
		 * @user_action
		 * Check the syslog messages that occurred just before this
		 * message. In case of internal error, save the
		 * /var/adm/messages file and contact authorized Sun service
		 * provider.
		 */
		scds_syslog(LOG_ERR,
		    "Failed to retrieve process monitor facility tag.");
		return (SCDS_PROBE_COMPLETE_FAILURE/2);
	}

	/* Check if the dataservice is still up and running */
	if (status != SCDS_PMF_MONITORED) {
		/*
		 * SCMSGS
		 * @explanation
		 * The probe for the SUNW.Event data service found that the
		 * cl_apid application failed to stay up.
		 * @user_action
		 * Examine other syslog messages occurring at about the same
		 * time to see if the problem can be identified. Save a copy
		 * of the /var/adm/messages files on all nodes and contact
		 * your authorized Sun service provider for assistance in
		 * diagnosing and correcting the problem.
		 */
		scds_syslog(LOG_ERR,
		    "Application failed to stay up.");
		return (SCDS_PROBE_COMPLETE_FAILURE);
	}

	if (port)
		return (0);		/* Fake successful probe */
	/*
	 * probe the dataservice by doing a socket connection to the port
	 * specified in the port_list property to the host that is
	 * serving the event dataservice. If the event service which is
	 * configured to listen on the specified port, replies to the
	 * connection, then the probe is successfull.
	 * Else we will wait for a time period set
	 * in probe_timeout property before concluding that the probe failed.
	 */

	/*
	 * Use the SVC_CONNECT_TIMEOUT_PCT percentage of timeout
	 * to connect to the port
	 */
	connect_timeout = (SVC_CONNECT_TIMEOUT_PCT * timeout)/100;
	t1 = gethrtime();

	/*
	 * the probe makes a connection to the specified hostname and port.
	 * The connection is timed for 95% of the actual probe_timeout.
	 */
	rc = scds_fm_tcp_connect(scds_handle, &sock, hostname, port,
	    connect_timeout);
	if (rc) {
		/*
		 * SCMSGS
		 * @explanation
		 * An error occurred the while fault monitor attempted to make
		 * a connection to the specified hostname and port.
		 * @user_action
		 * Wait for the fault monitor to correct this by doing restart
		 * or failover. For more error descriptions, look at the
		 * syslog messages.
		 */
		scds_syslog(LOG_ERR,
		    "Failed to connect to the host <%s> and port <%d>.",
		    hostname, port);
		/* this is a complete failure */
		probe_status = SCDS_PROBE_COMPLETE_FAILURE;
		goto finished;
	}

	t2 = gethrtime();

	/*
	 * Compute the actual time it took to connect. This should be less than
	 * or equal to connect_timeout, the time allocated to connect.
	 * If the connect uses all the time that is allocated for it,
	 * then the remaining value from the probe_timeout that is passed to
	 * this function will be used as disconnect timeout. Otherwise, the
	 * the remaining time from the connect call will also be added to
	 * the disconnect timeout.
	 *
	 */

	time_used = (int)(t2 -t1)/1E9;

	/*
	 * Use the remaining time(timeout - time_took_to_connect) to disconnect
	 */

	time_remaining = timeout - (int)time_used;

	/*
	 * If all the time is used up, use a small hardcoded timeout
	 * to still try to disconnect. This will avoid the fd leak.
	 */
	if (time_remaining <= 0) {
		scds_syslog_debug(DBG_LEVEL_LOW,
		    "svc_probe used entire timeout of "
		    "%d seconds during connect operation and exceeded the "
		    "timeout by %f seconds. Attempting disconnect with timeout"
		    " %d ",
		    connect_timeout,
		    time_used,
		    SVC_DISCONNECT_TIMEOUT_SECONDS);

		time_remaining = SVC_DISCONNECT_TIMEOUT_SECONDS;
	}

	/*
	 * Return partial failure in case of disconnection failure.
	 * Reason: The connect call is successful, which means
	 * the application is alive. A disconnection failure
	 * could happen due to a hung application or heavy load.
	 * If it is the later case, don't declare the application
	 * as dead by returning complete failure. Instead, declare
	 * it as partial failure. If this situation persists, the
	 * disconnect call will fail again and the application will be
	 * restarted.
	 */
	rc = scds_fm_tcp_disconnect(scds_handle, sock, time_remaining);
	if (rc != SCHA_ERR_NOERR) {
		/*
		 * SCMSGS
		 * @explanation
		 * An error occurred while fault monitor attempted to
		 * disconnect from the specified hostname and port.
		 * @user_action
		 * Wait for the fault monitor to correct this by doing restart
		 * or failover. For more error descriptions, look at the
		 * syslog messages.
		 */
		scds_syslog(LOG_ERR,
		    "Failed to disconnect from port %d of resource %s.",
		    port, scds_get_resource_name(scds_handle));
		/* this is a partial failure */
		probe_status = SCDS_PROBE_COMPLETE_FAILURE/2;
		goto finished;
	}

	t2 = gethrtime();
	time_used = (t2 -t1)/1E9;
	time_remaining = (long)(timeout - time_used);

	if (time_remaining <= 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * The simple probe on the network aware application timed
		 * out.
		 * @user_action
		 * This problem might occur when the cluster is under heavy
		 * load. Consider increasing the Probe_timeout property.
		 */
		scds_syslog(LOG_ERR, "Probe timed out.");
		probe_status = SCDS_PROBE_COMPLETE_FAILURE/2;
		goto finished;
	}

finished:

	return (probe_status);
}
