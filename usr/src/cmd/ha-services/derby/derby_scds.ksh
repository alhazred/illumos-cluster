#!/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident	"@(#)derby_scds.ksh	1.3	08/05/20 SMI"
#
#
##############################################################################

#include_lib

# Note: include_lib above is needed because it includes the definition of
# scds_syslog

typeset SYSLOG_TAG=$1
typeset ERROR_CODE=$2

case ${ERROR_CODE} in
1)	

	# SCMSGS
	# @explanation
	# The Derby server could not be started probably because the
	# port specified in DB_port is already used.
	# @user_action
	# Check the resource property DB_port of the Derby data service
	# and try to use another port number.
	scds_syslog -p error -t "${SYSLOG_TAG}" -m \
		"Error: could not start the Derby server, check whether the port specified in the resource property DB_port is already used."
	;;
2)	

	# SCMSGS
	# @explanation
	# The Derby server could not be started because the path specified
	# in DB_path is not valid.
	# @user_action
	# Use a valid directory path as the value of the DB_path property.
	scds_syslog -p error -t "${SYSLOG_TAG}" -m \
		"Error: could not start the Derby server because the directory path specified in the resource property DB_path is not valid."
	;;
3)	

	# SCMSGS
	# @explanation
	# The Derby server could not be started for an unknown reason.
	# @user_action
	# Contact your authorized Sun service provider to determine
	# whether a workaround or patch is available.
	scds_syslog -p error -t "${SYSLOG_TAG}" -m \
		"Error: could not start the Derby server for an unknown reason."
	;;
4)

	# SCMSGS
	# @explanation
	# The Derby server could not be started probably because the
	# the port specified in DB_probe_port is already used.
	# @user_action
	# Check the resource property DB_probe_port of the Derby data service
	# and try to use another port number.
	scds_syslog -p error -t "${SYSLOG_TAG}" -m \
		"Error: could not start the Derby server, check whether the port specified in the resource property DB_probe_port is already used."
	;;
*)
	# Error codes 5 and 6

	# SCMSGS
	# @explanation
	# The Derby command server could not be started because of
	# an internal error.
	# @user_action
	# Contact your authorized Sun service provider to determine
	# whether a workaround or patch is available.
	scds_syslog -p error -t "${SYSLOG_TAG}" -m \
		"Error: could not start the Derby server because of an internal error."
	;;
esac

exit 0
