#!/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

##############################################################################
# Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident	"@(#)derby_lib.ksh	1.11	09/02/26 SMI"
##############################################################################

export PATH=/bin:/usr/bin:/usr/cluster/bin:/usr/sbin:/usr/proc/bin:$PATH

typeset -r JAVA="/usr/bin/java"
typeset -r PKGPARAM="/usr/bin/pkgparam"
typeset -r WC="/usr/bin/wc"
typeset -r SED="/usr/bin/sed"
typeset -r SCHA_RESOURCE_GET="/usr/cluster/bin/scha_resource_get"
typeset -r SCHA_CLUSTER_GET="/usr/cluster/bin/scha_cluster_get"
typeset -r SCHA_CONTROL="/usr/cluster/bin/scha_control"
typeset -r PMFADM="/usr/cluster/bin/pmfadm"

# use an alias as opposed to a variable for the messageid/id.ksh script
# to recognize the logger command
alias scds_syslog="/usr/cluster/lib/sc/scds_syslog"

# Default syslog tag, final tag set in full_parse_args and parse_args
SYSLOG_TAG="[SUNW.derby]"

DERBYJARS="derbyclient.jar derbynet.jar derbytools.jar derby.jar"
AGENTJAR="DerbyAgent.jar"

PKGINFO="/usr/bin/pkginfo"
PKGINFO_CMD="/usr/bin/pkginfo"
DERBYPKG=SUNWjavadb-core
# Package name and package commands are different in OpenSolaris
if [[ -f "/var/cluster/ips/.ips" ]]; then
	PKGINFO="/usr/bin/pkg"
	PKGINFO_CMD="/usr/bin/pkg info"
	DERBYPKG=SUNWjavadb
fi

function get_derby_home
{
    # No package parameter BASEDIR in OpenSolaris
    if [[ -f "/var/cluster/ips/.ips" ]]; then
	DERBYBASEDIR="/opt"
    else
	DERBYBASEDIR=`${PKGPARAM} ${DERBYPKG} BASEDIR`
	if [[ -z ${DERBYBASEDIR} ]]
	then
		# SCMSGS
		# @explanation
		# Unable to retrieve information related to a package.
		# @user_action
		# Verify that the package is installed. If it is correctly installed,
		# contact your authorized Sun service provider for assistance in
		# diagnosing and correcting the problem.
		scds_syslog -p error -t "${SYSLOG_TAG}" -m \
			"Could not retrieve the BASEDIR information of the derby package (%s)" \
			"${PKG}"
		return 1
    	fi
    fi

    DERBYHOME=${DERBYBASEDIR}/SUNWjavadb/lib

    return 0
}

function set_classpath # [DERBY_HOME] [RT_BASEDIR]
{

    if [[ $# -ne 2 ]]; then
	return 1
    fi

    if [[ -z ${1} ]] && [[ -z ${2} ]];then
	return 1
    fi

    typeset derbyhome=${1}
    typeset rtbasedir=${2}

    is_a_file ${rtbasedir}/${AGENTJAR} || return $?
    CLASSPATH=${rtbasedir}/${AGENTJAR}

    for JAR in ${DERBYJARS}
    do
	is_a_file ${derbyhome}/${JAR} || return $?
	CLASSPATH=${CLASSPATH}:${derbyhome}/${JAR}
    done

    return 0
}

###############################################################################
# validate have more options to parse
#
###############################################################################
function full_parse_args # [args ...]
{
    typeset opt
    typeset -i rc=0

    while getopts 'cur:x:g:R:T:G:' opt
    do
      case "$opt" in

	  R) RESOURCE_NAME=$OPTARG;;
	  G) RESOURCEGROUP_NAME=$OPTARG;;
	  T) RESOURCETYPE_NAME=$OPTARG;;
	  r) ;;
	  g) ;;
	  c) ;;
	  u) ;;
	  x) ;;
	  *)
	      # SCMSGS
	      # @explanation
	      # An unknown option was given to a program or script of the
	      # derby data service. This should never occur.
	      # @user_action
	      # Contact your authorized Sun service provider to determine
	      # whether a workaround or patch is available.
	      scds_syslog -p error -t "${SYSLOG_TAG}" -m \
	          "ERROR: Option %s passed to a script of the derby data service is unknown" "${OPTARG}"
	      rc=1
	      ;;

      esac
    done

    SYSLOG_TAG="[${RESOURCETYPE_NAME},${RESOURCE_NAME},${RESOURCEGROUP_NAME}]"

    return $rc
}


###############################################################################
# Parse program arguments.
#
###############################################################################
function parse_args # [args ...]
{
    typeset -i rc=0
    typeset opt

    while getopts 'R:G:T:' opt
    do
      case "$opt" in

	  R) RESOURCE_NAME=$OPTARG;;
	  G) RESOURCEGROUP_NAME=$OPTARG;;
	  T) RESOURCETYPE_NAME=$OPTARG;;
	  *)
	     scds_syslog -p error -t "${SYSLOG_TAG}" -m \
		"ERROR: Option %s passed to a script of the derby data service is unknown" "${OPTARG}"
	      rc=1
	      ;;

      esac
    done

    SYSLOG_TAG="[${RESOURCETYPE_NAME},${RESOURCE_NAME},${RESOURCEGROUP_NAME}]"

    return $rc
}


#############################################################
# error_exit()
# param:
#   o exit_code: exit argument
#############################################################
error_exit()
{
    typeset exit_code="${1:-0}"
    exit $exit_code
}

#############################################################
# The following function checks if a package (parameter) is
# installed
#
# param:
#   o package to check
#
# returned code:
#   o 0 if the param is installed
#   o 1 if the param is not installed or not a package
#
#############################################################
function check_package
{
    if [[ $# -ne 1 ]]; then
	return 1
    fi

    if [[ -z $1 ]]; then
	return 1
    fi

    ${PKGINFO_CMD} ${1} >/dev/null
    if [[ $? -ne 0 ]]; then
	# SCMSGS
	# @explanation
	# A package required for the derby data service is not installed.
	# @user_action
	# Install the missing package.
	scds_syslog -p error -t "${SYSLOG_TAG}" -m \
	    "Package %s required for the derby data service is missing" "${1}"
	return 1
    fi

    return 0;
}


#############################################################
# The following function checks if a directory (parameter) is
# present
#
# param:
#   o directory to check
#
# returned code:
#   o 0 if the param is empty or a directory
#   o 1 if the param is not empty and not a directory
#
#############################################################
is_a_directory()
{
    if [[ $# -ne 1 ]]; then
	return 1
    fi

    if [[ -z $1 ]];then
	return 1
    fi

    if [[ ! -d $1 ]]; then
	# SCMSGS
	# @explanation
	# A program or script of the derby data service could not execute
	# because a directory does not exist. This should never occur.
	# @user_action
	# Contact your authorized Sun service provider to determine whether a
	# workaround or patch is available.
	scds_syslog -p error -t "${SYSLOG_TAG}" -m \
	    "The directory %s needed by the derby data service does not exist" "${1}"
	return 1
    fi

    return 0;
}

#############################################################
# The following function checks if a file (parameter) exists
# and is executable
#
# param:
#   o file to check
#
# returned code:
#   o 0 if the param is empty or executable
#   o 1 if the param is not empty and not executable
#
#############################################################
is_executable()
{
    if [[ $# -ne 1 ]]; then
	return 1
    fi

    if [[ -z $1 ]];then
	return 1
    fi

    if [[ ! -f $1 ]]; then
	# SCMSGS
	# @explanation
	# A program or script of the derby data service could not execute
	# because a file does not exist. This should never occur.
	# @user_action
	# Contact your authorized Sun service provider to determine whether a
	# workaround or patch is available.
	scds_syslog -p error -t "${SYSLOG_TAG}" -m \
	    "The file %s needed by the derby data service does not exist" "${1}"
	return 1
    fi

    if [[ ! -x $1 ]]; then
	# SCMSGS
	# @explanation
	# A program or script of the derby data service could not execute
	# because a file is not executable. This should never occur.
	# @user_action
	# Contact your authorized Sun service provider to determine whether a
	# workaround or patch is available.
	scds_syslog -p error -t "${SYSLOG_TAG}" -m \
	    "The file %s which the derby data service needs to execute is not executable" "${1}"
	return 1
    fi

    return 0;
}


#############################################################
# The following function checks if a file (parameter) is
# present
#
# param:
#   o file to check
#
# returned code:
#   o 0 if the param is empty or is a file
#   o 1 if the param is not empty and not a file
#
#############################################################
is_a_file()
{
    if [[ $# -ne 1 ]]; then
	return 1
    fi

    if [[ -z $1 ]];then
	return 1
    fi

    if [[ ! -f $1 ]]; then
	scds_syslog -p error -t "${SYSLOG_TAG}" -m \
	    "The file %s needed by the derby data service does not exist" "${1}"
	return 1
    fi

    return 0;
}

#############################################################
# check hastorage plus
#
# param:
#   o N/A
#
# returned code:
#   o 0 if the param is empty or is a file
#   o 1 if the param is not empty and not a file
#
#############################################################
call_hasp_check()
{
    is_executable ${RT_BASEDIR}/hasp_check


    if [[ $? -eq 0 ]]; then 		# if is executable
	${RT_BASEDIR}/hasp_check "$@"
	hasp_status=$?
    else
	# the binary doesn't exist so we cannot call it
	hasp_status=2
    fi

    case "$hasp_status" in

	1)
	    # SCMSGS
	    # @explanation
	    # This is an internal error during SUNW.HAStoragePlus resource
	    # dependencies status validation.
	    # @user_action
	    # Examine other syslog messages occurring at about the same time
	    # to see if the problem can be identified. Save a copy of the
	    # /var/adm/messages files on all nodes and contact your authorized
	    # Sun service provider for assistance in diagnosing and correcting
	    # the problem.
	    scds_syslog -p error -t "${SYSLOG_TAG}" -m \
		"Internal Error. Failed to check status of SUNW.HAStoragePlus resource."
	    return 1
	    ;;

	2)
	    # SCMSGS
	    # @explanation
	    # The data service normally checks for the status of its dependent
	    # SUNW.HAStoragePlus resources. The check is not done because
	    # there is not dependency on an HAStoragePlus resource in your
	    # configuration.
	    # @user_action
	    # This message is informational; no user action is needed.
	    scds_syslog -p info -t "${SYSLOG_TAG}" -m \
		"This resource doesn't depend on any SUNW.HAStoragePlus resources. Proceeding with the normal checks."
	    return 0
	    ;;

	3)
	    # SCMSGS
	    # @explanation
	    # It is an invalid configuration to have an application resource
	    # depend on one or more SUNW.HAStoragePlus resource(s) that are in
	    # a different resource group.
	    # @user_action
	    # Change the resource/resource group configuration such that the
	    # application resource and the SUNW.HAStoragePlus resource(s) are
	    # in the same resource group.
	    scds_syslog -p error -t "${SYSLOG_TAG}" -m \
		"One or more of the SUNW.HAStoragePlus resources that this resource depends on is in a different resource group. Failing validate method configuration checks."
	    return 1
	    ;;

	4)
	    # SCMSGS
	    # @explanation
	    # The data service depends on one or more SUNW.HAStoragePlus
	    # resources that are not online.
	    # @user_action
	    # Put the SUNW.HAStoragePlus resources online.
	    scds_syslog -p error -t "${SYSLOG_TAG}" -m \
		"One or more of the SUNW.HAStoragePlus resources that this resource depends on is not online. Failing validate method."
	    return 1
	    ;;

	5)
	    # SCMSGS
	    # @explanation
	    # Some checks are only performed when the SUNW.HAStoragePlus
	    # resource is online on the local node.
	    # @user_action
	    # This message is informational; no user action is needed.
	    scds_syslog -p info -t "${SYSLOG_TAG}" -m \
		"None of the SUNW.HAStoragePlus resources that this resource depends on are online on the local node. Skipping the checks for the existence and permissions of the start/stop/probe commands."
	    return 0
	    ;;

	6)
	    # SCMSGS
	    # @explanation
	    # The SUNW.HAStoragePlus resource(s) that this application
	    # resource depends on is online on the local node and therefore
	    # the validation checks related to start/stop/probe commands will
	    # be carried out on the local node.
	    # @user_action
	    # None.
	    scds_syslog -p info -t "${SYSLOG_TAG}" -m \
		"All the SUNW.HAStoragePlus resources that this resource depends on are online on the local node. Proceeding with the checks for the existence and permissions of the start/stop/probe commands."
	    return 0
	    ;;

	*)
	    # SCMSGS
	    # @explanation
	    # An unexpected error occurred while the checking the status of an
	    # HAStoragePlus resource.
	    # @user_action
	    # Save a copy of the /var/adm/messages files on all nodes and
	    # contact your authorized Sun service provider for assistance in
	    # diagnosing and correcting the problem.
	    scds_syslog -p error -t "${SYSLOG_TAG}" -m \
		"Unknown status code %s" "${hasp_status}"
	    return 1
	    ;;

    esac
}

###############################################################################
# set and check properties
#
# param:
#   o param1: Resource Group Name
#   o param1: Resource Name
#
# returned code:
#   o 0 if the valid succeeds
#   o 1 if the valid fails
#
###############################################################################
function check_properties
{
    typeset -i rc=0

    if [[ $# -ne 2 ]]; then
	# SCMSGS
	# @explanation
	# Should never occur.
	# @user_action
	# Contact your authorized Sun service provider to determine whether a
	# workaround or patch is available.
	scds_syslog -p error -t "${SYSLOG_TAG}" -m \
	    "Wrong number of parameters for check_properties"
	return 1
    fi

    if [[ -z $1 ]]; then
	# SCMSGS
	# @explanation
	# Should never occur.
	# @user_action
	# Contact your authorized Sun service provider to determine whether a
	# workaround or patch is available.
	scds_syslog -p error -t "${SYSLOG_TAG}" -m \
	    "Invalid Parameter for check_properties"
	return 1
    fi

    if [[ -z $2 ]]; then
	scds_syslog -p error -t "${SYSLOG_TAG}" -m \
	    "Invalid Parameter for check_properties"
	return 1
    fi

    typeset RG=$1
    typeset R=$2

    DB_PATH=`${SCHA_RESOURCE_GET} -O Extension -R $R -G $RG DB_PATH | tail -1`

    if [[ -z ${DB_PATH} ]]; then
	# SCMSGS
	# @explanation
	# The property DB_PATH is a mandatory property of the data service.
	# Should never occur.
	# @user_action
	# Contact your authorized Sun service provider to determine
	# whether a workaround or patch is available.
	scds_syslog -p error -t "${SYSLOG_TAG}" -m \
	    "Resource %s does not have the property DB_PATH defined" "${R}"
	rc=1
    fi

    DB_PORT=`${SCHA_RESOURCE_GET} -O Extension -R $R -G $RG DB_PORT | tail -1`

    if [[ -z ${DB_PORT} ]]; then
	# SCMSGS
	# @explanation
	# The property DB_PORT is a mandatory property of the data service.
	# Should never occur.
	# @user_action
	# Contact your authorized Sun service provider to determine
	# whether a workaround or patch is available.
	scds_syslog -p error -t "${SYSLOG_TAG}" -m \
	    "Resource %s does not have the property DB_PORT defined" "${R}"
    rc=1
    fi

    DB_PROBE_PORT=`${SCHA_RESOURCE_GET} -O Extension \
-R $R -G $RG DB_PROBE_PORT | tail -1`

    if [[ -z ${DB_PROBE_PORT} ]]; then
	# SCMSGS
	# @explanation
	# The property DB_PROBE_PORT is a mandatory property of the data
	# service. Should never occur.
	# @user_action
	# Contact your authorized Sun service provider to determine
	# whether a workaround or patch is available.
	scds_syslog -p error -t "${SYSLOG_TAG}" -m \
	    "Resource %s does not have the property DB_PROBE_PORT defined" "${R}"
	rc=1
    fi

    DB_NETIF=`${SCHA_CLUSTER_GET} -O PRIVATELINK_HOSTNAME_LOCAL | tail -1`
    if [[ -z ${DB_NETIF} ]]; then
	# SCMSGS
	# @explanation
	# The data service tried to find the local nodename associated with
	# the private interconnects.
	# @user_action
	# Examine other syslog messages occurring at about the same time to
	# see if the problem can be identified. Save a copy of the
	# /var/adm/messages files on all nodes and contact your authorized Sun
	# service provider for assistance in diagnosing and correcting the
	# problem.
	scds_syslog -p error -t "${SYSLOG_TAG}" -m \
	    "The derby data service failed to retrieve private local nodename"
	rc=1
    fi

    return $rc

}


#############################################################
# The following function  makes all the necessary checks for
# the validation
#
# param:
#   o param1: Resource Group Name
#   o param1: Resource Name
#
# returned code:
#   o 0 if the valid succeeds
#   o 1 if the valid fails
#
#############################################################

validate()
{
    typeset RG=$1
    typeset R=$2

    # We need to know the location of the probe method in order to
    # restart it successfully. This value is obtained from the
    # RT_BASEDIR property of the RTR file.
    RT_BASEDIR=`${SCHA_RESOURCE_GET} -O RT_BASEDIR -R $R -G $RG`

    # we use the default package path as the RT_BASEDIR value
    if [[ -z "${RT_BASEDIR}" ]]; then
	RT_BASEDIR=/usr/cluster/lib/rgm/rt/derby
    fi


    # Validate is called before the creation of the R, in that case
    start_cmd_prog="${RT_BASEDIR}/derby_start"
    stop_cmd_prog="${RT_BASEDIR}/derby_stop"
    probe_cmd_prog="${RT_BASEDIR}/derby_probe"

    is_executable $start_cmd_prog || return $?
    is_executable $stop_cmd_prog || return $?
    is_executable $probe_cmd_prog || return $?

    is_executable ${WC} || return $?
    is_executable ${JAVA} || return $?
    is_executable ${PKGINFO} || return $?
    if [[ ! -f "/var/cluster/ips/.ips" ]]; then
    	is_executable ${PKGPARAM} || return $?
    fi
    is_executable ${SED} || return $?

    check_package ${DERBYPKG} || return $?

    # will set DERBYHOME
    get_derby_home || return $?

    is_a_directory ${DERBYHOME} || return $?

    set_classpath ${DERBYHOME} ${RT_BASEDIR} || return $?

    return 0
}



