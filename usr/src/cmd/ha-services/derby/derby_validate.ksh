#!/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident	"@(#)derby_validate.ksh	1.7	08/05/20 SMI"
#
# Validate method for derby data service.
# ex: When the resource is being created command args will be
#
# derby_validate -c -R <..> -G <...> -T <..> -r <sysdef-prop=value>...
#       -x <extension-prop=value>.... -g <resourcegroup-prop=value>....
#
# when the resource property is being updated
#
# derby_validate -u -R <..> -G <...> -T <..> -r <sys-prop_being_updated=value>
#   OR
# derby_validate -u -R <..> -G <...> -T <..> -x <extn-prop_being_updated=value>
#
##############################################################################

#include_lib
# Parse the arguments that have been passed to this method.
full_parse_args "$@" || error_exit $?

validate $RESOURCEGROUP_NAME $RESOURCE_NAME || error_exit $?

# Log a message indicating that validate method was successful.

# SCMSGS
# @explanation
# The derby data service could be validated.
# @user_action
# This message is informational; no user action is needed.
scds_syslog -p info -t "${SYSLOG_TAG}" -m \
	"Derby data service validate method completed successfully"

exit 0
