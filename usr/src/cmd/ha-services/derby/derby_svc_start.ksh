#!/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident	"@(#)derby_svc_start.ksh	1.10	08/05/20 SMI"
#
# Start Method for derby data service.
#
# The data service is started under the control of PMF. Prior to starting the
# process(es) for derby some sanity checks are done. The PMF tries to start the
# service specified number of times and if the number of attempts exceeds this
# value within a specified interval of time the PMF reports a failure to start
# the service. The number of times to retry and the interval in which it is to
# be tried are both properties of the resource set in RTR file.

#include_lib

# Parse the arguments that have been passed to this method
parse_args "$@" || error_exit $?

# these checks are performed here and not in validate as extended properties
# does not seem to be initialized at validate call
check_properties $RESOURCEGROUP_NAME \
    $RESOURCE_NAME || error_exit $?

PMF_TAG=$RESOURCEGROUP_NAME,$RESOURCE_NAME,0.svc


# We need to know the full path for the validate method which resides
# in the directory <RT_BASEDIR>. Get this from the RT_BASEDIR property of the
# resource type.
RT_BASEDIR=`${SCHA_RESOURCE_GET} -O RT_BASEDIR -R $RESOURCE_NAME \
	-G $RESOURCEGROUP_NAME`


# Get the value for retry count from the RTR file.
RETRY_CNT=`${SCHA_RESOURCE_GET} -O Retry_Count -R $RESOURCE_NAME \
	-G $RESOURCEGROUP_NAME`

# Get the value for retry interval from the RTR file. The value for the
# RETRY_INTERVAL in the RTR file will be in seconds. Convert this value from
# seconds to minutes for passing on to pmfadm. Note that this is necessarily
# a conversion with round-up, eg. 59 seconds --> 1 minute.
((RETRY_INTRVAL = (`${SCHA_RESOURCE_GET} -O Retry_Interval -R $RESOURCE_NAME -G \
	$RESOURCEGROUP_NAME` + 59) / 60 ))

# Get the probe port number
DB_PROBE_PORT=`${SCHA_RESOURCE_GET} -O Extension \
	-R ${RESOURCE_NAME} -G ${RESOURCEGROUP_NAME} DB_PROBE_PORT | tail -1`

# Get the private cluster node name (clusternoden_priv)
DB_PRIV_LOCAL_HOSTNAME=`${SCHA_CLUSTER_GET} -O PRIVATELINK_HOSTNAME_LOCAL | tail -1`

start_cmd_args="${RT_BASEDIR}/derby_start"
start_cmd_prog=`echo $start_cmd_args | nawk '{print $1}'`
start_cmd_args="$start_cmd_args -R $RESOURCE_NAME -G $RESOURCEGROUP_NAME -T $RESOURCETYPE_NAME"

# start the daemon under the control of Sun Cluster Process Monitor
# Facility. Let it crash and restart up to $RETRY_COUNT times in a period of
# $RETRY_INTERVAL; if it crashes more often than that, the process monitor
# facility will cease trying to restart it

$PMFADM -c $PMF_TAG -n $RETRY_CNT -t $RETRY_INTRVAL $start_cmd_args

# Log a message indicating that derby data service has been started.
if [[ $? -ne 0 ]]; then
	# SCMSGS
	# @explanation
	# The derby data service could not start.
	# @user_action
	# Contact your authorized Sun service provider to
	# determine whether a workaround or patch is
	# available.
	scds_syslog -p error -t "${SYSLOG_TAG}" -m \
		"Derby data service start method failed"
	error_exit 1
fi

# Wait for the Derby server to be operational.
# FED will stop us if the Derby server can not start.
STATUS="STATUS: FAILED"
while [ "${STATUS}" != "STATUS: OK" ] ; do

	# Check if the DB is still under PMF control.
	# If not, no need for further checks, an error is returned
	$PMFADM -q $PMF_TAG 2>/dev/null
    	if [[ $? -ne 0 ]]; then
    		scds_syslog -p error -t "${SYSLOG_TAG}" -m \
			"Derby data service start method failed"
		error_exit 1
	fi

	# Verify if the DB is accepting requests
	STATUS=$(
        {
	    echo "TEST ${RESOURCE_NAME}"; sleep 1
	} |  telnet ${DB_PRIV_LOCAL_HOSTNAME} ${DB_PROBE_PORT}  2>&1 | grep "STATUS")
    	sleep 1
done

scds_syslog -p info -t "${SYSLOG_TAG}" -m \
	"Derby data service start method completed sucessfully"

exit 0
