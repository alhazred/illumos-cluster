#!/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident	"@(#)derby_svc_stop.ksh	1.8	08/05/20 SMI"
#
# Stop method for derby data service
#

#include_lib

# Parse the arguments that have been passed to this method
parse_args "$@" || error_exit $?

# get the Timeout value allowed for stop method from the RTR file
STOP_TIMEOUT=`${SCHA_RESOURCE_GET} -O STOP_TIMEOUT -R $RESOURCE_NAME \
	-G $RESOURCEGROUP_NAME`

# We will try to wait for 80% of the stop_method_timeout value when we
# send a SIGTERM through PMF to the Data service. This is to make sure that
# the application stops in a decent manner. If the application does not
# respond favourably to this then we use SIGKILL to stop the data service
# and this will be done for a 15% of the Stop_method_timeout value. However,
# if the data service has not stopped by now, we conclude that there was a
# Failure in the stop method and exit non-zero. The remaining 5% of the
# stop method timeout is for other needs.
((SMOOTH_TIMEOUT=$STOP_TIMEOUT * 80/100))

((HARD_TIMEOUT=$STOP_TIMEOUT * 15/100))

PMF_TAG=$RESOURCEGROUP_NAME,$RESOURCE_NAME,0.svc

# We need to know the full path for the validate method which resides
# in the directory <RT_BASEDIR>. Get this from the RT_BASEDIR property of the
# resource type.
RT_BASEDIR=`${SCHA_RESOURCE_GET} -O RT_BASEDIR -R $RESOURCE_NAME \
	-G $RESOURCEGROUP_NAME`

stop_cmd_args="${RT_BASEDIR}/derby_stop -R $RESOURCE_NAME \
	-G $RESOURCEGROUP_NAME"

stop_cmd_prog=`echo $stop_cmd_args | nawk '{print $1}'`

typeset -i SEND_KILL=0

$PMFADM -q $PMF_TAG
if [[ $? -eq 0 ]]; then
	if [[ -f $stop_cmd_prog && -x $stop_cmd_prog ]]; then

		$PMFADM -s $PMF_TAG 2>/dev/null
		if [[ $? -ne 0  ]]; then
			# SCMSGS
			# @explanation
			# The data service was not able to take derby out of
			# the PMF control.
			# @user_action
			# This message is informational; no user action is
			# needed. Automatic recovery kills the data service
			# processes. Verify if other messages followed this
			# one.
			scds_syslog -p info -t "${SYSLOG_TAG}" -m \
				"Failed to take derby data service out of PMF control; trying to send SIGKILL now"
			SEND_KILL=1
		else
			# execute the user specified stop_cmd using hatimerun
			hatimerun -k KILL -t $SMOOTH_TIMEOUT $stop_cmd_args
			if [[ $? -ne 0 ]]; then
				# SCMSGS
				# @explanation
				# The derby data service did not stop correctly.
				# @user_action
				# This message is informational; no user
				# action is needed. Automatic recovery kills
				# the data service processes. Verify if other
				# messages followed this one.
				scds_syslog -p error -t "${SYSLOG_TAG}" -m \
					"Failed to stop derby data service using the custom stop command; trying SIGKILL now."
			fi

			# Regardless of whether the command succeeded or not we
			# send KILL signal to the pmf tag. This will ensure
			# that the process tree goes away if it still exists.
			# If it doesn't exist by then, we return NOERR.
			SEND_KILL=1
		fi
	else
		# Send a SIGTERM signal to the Data service and wait for
		# 80% of the total timeout value.
		$PMFADM -s $PMF_TAG -w $SMOOTH_TIMEOUT TERM 2>/dev/null
		# We compare the exit status of pmfadm to be greater than 2
		# because "exit status = 1" means nametag doesn't exist, which
		# is a OK, for the stop method has to be idempotent.
		if [[ $? -ge 2 ]]; then
			# SCMSGS
			# @explanation
			# The derby data service did not stop correctly.
			# @user_action
			# This message is informational; no user action is
			# needed. Automatic recovery kills the data service
			# processes. Verify if other messages followed this
			# one.
			scds_syslog -p error -t "${SYSLOG_TAG}" -m \
				"Failed to stop derby data service with SIGTERM; retry with SIGKILL"
			SEND_KILL=1;
		fi
	fi

	$PMFADM -q $PMF_TAG
	if [[ $SEND_KILL -eq 1 && $? -eq 0 ]]; then
		# Note : We can end up here if the derby JVM was still exiting when
                # we executed the pmfadm -q command above
                # In that case it is possible that the following pmfadm fails due to
                # a race condition if the JVM exits in between both calls

		# Since the Data service did not stop with a SIGTERM we will
		# use a SIGKILL now and wait for another 15% of total timeout.
		$PMFADM -s $PMF_TAG -w $HARD_TIMEOUT KILL 2>/dev/null
		# We compare the exit status of pmfadm to be greater than 2
		# because "exit status = 1" means nametag doesn't exist, which
		# is a OK, for the stop method has to be idempotent.
		if [[ $? -ge 2 ]]; then
			# SCMSGS
			# @explanation
			# Could not stop the derby data service.
			# @user_action
			# Contact your authorized Sun service provider to
			# determine whether a workaround or patch is
			# available.
			scds_syslog -p error -t "${SYSLOG_TAG}" -m \
				"Failed to stop derby data service; unsuccessful exit"
			exit 1
		fi
	fi
else
	# The Data service is not running as of now. Log a message and
	# exit suceess.

	# SCMSGS
	# @explanation
	# Trying to stop the derby data service when it is not running.
	# @user_action
	# This message is informational; no user action is needed.
	scds_syslog -p info -t "${SYSLOG_TAG}" -m \
		"The derby data service is not already running"

	# Even if the derby data service is not running,
	# we exit success, for idempotency of the STOP method.
	exit 0
fi

# Successfully stopped derby data service.
# Log a message and exit success.

scds_syslog -p info -t "${SYSLOG_TAG}" -m \
	"Derby data service stop method completed successfully"
exit 0
