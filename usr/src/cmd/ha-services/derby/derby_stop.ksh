#!/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident	"@(#)derby_stop.ksh	1.3	08/05/20 SMI"
#
# script for starting the Derby server
#

#include_lib

# Parse the arguments that have been passed to this method
parse_args "$@" || error_exit $?

#
# get command port
#
DB_PROBE_PORT=`${SCHA_RESOURCE_GET} -O Extension \
	  -R ${RESOURCE_NAME} -G ${RESOURCEGROUP_NAME} DB_PROBE_PORT | tail -1`

#
# get private hostname
#
DB_PRIV_LOCAL_HOSTNAME=`${SCHA_CLUSTER_GET} -O PRIVATELINK_HOSTNAME_LOCAL`


#
# Main
#

# THE METHOD HERE CONSISTS IN USING THE SOCKET OPENED BY
# DerbyAgent.jar.  THIS SOCKET RECEIVES COMMANDS AND REPLY TO
# THEM. THE SENT COMMAND IS "QUIT" THAT MAKE THE SERVERS STOP.

STATUS=$(
{
echo "QUIT" ; sleep 1
} |  telnet ${DB_PRIV_LOCAL_HOSTNAME} ${DB_PROBE_PORT}  2>&1 | grep "STATUS")

[ "${STATUS}" = "STATUS: OK" ] && exit 0
exit -1
