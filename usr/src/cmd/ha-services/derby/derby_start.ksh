#!/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident	"@(#)derby_start.ksh	1.9	08/05/20 SMI"
#
# Manually-made script for starting the Derby server
#
##############################################################################

#include_lib

# Parse the arguments that have been passed to this method
parse_args "$@" || error_exit $?

check_properties ${RESOURCEGROUP_NAME} ${RESOURCE_NAME} || return $?

RT_BASEDIR=`${SCHA_RESOURCE_GET} -O RT_BASEDIR -R $RESOURCE_NAME -G $RESOURCEGROUP_NAME`

# will set DERBYHOME
get_derby_home || return $?

set_classpath ${DERBYHOME} ${RT_BASEDIR} || return $?

#
# get data base location
#

DB_PATH=`${SCHA_RESOURCE_GET} -O Extension \
     -R ${RESOURCE_NAME} -G ${RESOURCEGROUP_NAME} DB_PATH | tail -1`
if [[ -z ${DB_PATH} ]]; then
    scds_syslog  -p error -t "${SYSLOG_TAG}" -m \
	    "Resource %s does not have the property DB_PATH defined" \
	    "${RESOURCE_NAME}"
    return 1
fi


DB_PORT=`${SCHA_RESOURCE_GET} -O Extension \
	-R ${RESOURCE_NAME} -G ${RESOURCEGROUP_NAME} DB_PORT | tail -1`

if [[ -z ${DB_PORT} ]]; then
    scds_syslog -p error -t "${SYSLOG_TAG}" -m \
	    "Resource %s does not have the property DB_PORT defined" \
	    "${RESOURCE_NAME}"
    return 1
fi

DB_PROBE_PORT=`${SCHA_RESOURCE_GET} -O Extension \
	-R ${RESOURCE_NAME} -G ${RESOURCEGROUP_NAME} DB_PROBE_PORT | tail -1`

if [[ -z ${DB_PROBE_PORT} ]]; then
    scds_syslog -p error -t "${SYSLOG_TAG}" -m \
	    "Resource %s does not have the property DB_PROBE_PORT defined" \
	    "${RESOURCE_NAME}"
    exit 1
fi

DB_PRIV_LOCAL_HOSTNAME=`${SCHA_CLUSTER_GET} -O PRIVATELINK_HOSTNAME_LOCAL | tail -1`
if [[ -z ${DB_PRIV_LOCAL_HOSTNAME} ]]; then
    scds_syslog -p error -t "${SYSLOG_TAG}" -m \
	"The derby data service failed to retrieve private local nodename"
    exit 1
fi


# note: a default is provided if user didn't set it

# SCMSGS
# @explanation
# The monitor start method prints the path configured.
# @user_action
# This message is informational; no user action is needed.
scds_syslog -p info -t "${SYSLOG_TAG}" -m \
    "DB path is %s" "${DB_PATH}"

# note: a default is provided if user didn't set it

# SCMSGS
# @explanation
# The monitor start method prints the port number configured.
# @user_action
# This message is informational; no user action is needed.
scds_syslog -p info -t "${SYSLOG_TAG}" -m \
    "DB port is %s" "${DB_PORT}"

# start Derby
# CLASSPATH IS SET IN LIBRARY. It is also checked in validate
${JAVA} -cp ${CLASSPATH} -server DerbyAgent -t ${SYSLOG_TAG} \
	-p ${DB_PORT} -c ${DB_PROBE_PORT} -d ${DB_PATH} \
	-h ${DB_PRIV_LOCAL_HOSTNAME} -i ${RESOURCE_NAME} \
	-s ${RT_BASEDIR}/derby_scds &

# $! contains the PID of the last process executed
# in background, we save it in a variable
JAVA_ERROR_CODE=$!

# Wait for the Derby server to be operational. FED will
# stop us if the Derby server could not start.
STATUS="STATUS: FAILED"
while [ "${STATUS}" != "STATUS: OK" ] ; do
    ps -p ${JAVA_ERROR_CODE} > /dev/null
    if [[ $? -ne 0 ]]; then
	# The JVM exited, most probably because the server port (DB_port)
	# is already used, the probe port (DB_probe_port) is already used,
	# or the DB content directory (DB_path) is invalid.
	error_exit 1
    fi

    STATUS=$(
    {
	echo "TEST ${RESOURCE_NAME}"; sleep 1
    } |  telnet ${DB_PRIV_LOCAL_HOSTNAME} ${DB_PROBE_PORT}  2>&1 | grep "STATUS")
    sleep 1

done


exit 0
