/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)DerbyAgent.java 1.9     08/05/20 SMI"
 *
 */

import org.apache.derby.drda.NetworkServerControl;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;

import java.lang.Runtime;
import java.lang.String;

import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;


public class DerbyAgent {

    /*
     * Usage: -p <port> to specify the derby listener port
     * -h <hostname> to specify the derby listener host -c
     * <port> to specify the command listener port -d <dir>
     * to specify the DB content directory -i <instance> to
     * specify the instance name -t <tag> to specify the
     * syslog tag -s <path> to specify the syslogger path -D
     * to specify debug mode
     */

    private static final String _PROMPT = "STATUS: ";
    private static final String ANSWER_OK = _PROMPT + "OK";
    private static final String ANSWER_FAILED = _PROMPT + "FAILED";

    private static final String QUIT_CMD = "QUIT";
    private static final String TEST_CMD = "TEST";
    private static final String DEBUG_CMD = "DEBUG";
    private static final String NODEBUG_CMD = "NODEBUG";

    private static final int ERROR_DERBY_SOCKET = 1;
    private static final int ERROR_DERBY_HOME = 2;
    private static final int ERROR_DERBY_START = 3;
    private static final int ERROR_CMD_SOCKET = 4;
    private static final int ERROR_USAGE = 5;
    private static final int ERROR_INTERNAL = 6;

    private static boolean debug = false;

    private static int cmdPort = -1;
    private static int derbyPort = -1;
    private static String derbyHost = null;
    private static InetAddress derbyInetAddress = null;
    private static String derbyHome = null;
    private static String sysloggerPath = null;
    private static String instance = null;
    private static String syslogTag = "[SUNW.derby]";

    private static NetworkServerControl sc;
    private static ServerSocket cmdServerSocket;

    private static void debug(String msg) {

        if (debug) {
            System.out.println("[DerbyAgent," + syslogTag + "] " + msg);
        }
    }

    private static void syslogError(int errorCode) {

        if (sysloggerPath == null)
            return;

        String command = sysloggerPath + " " + syslogTag + " " + errorCode;

        try {
            Runtime.getRuntime().exec(command);
        } catch (Exception e) {

            /*
             * If for some reason we could not execute the
             * logger command then no syslog error messages
             * explaining why we could not start the derby
             * java process will be provided.
             */
            debug("failed to execute syslogger (" + e + ")");
        }
    }

    private static void fatal(int errorCode, String msg) {
        debug(msg);
        syslogError(errorCode);
        System.exit(errorCode);
    }

    private static void parseArgs(String args[]) {

        for (int i = 0; i < args.length; i++) {

            // get derby port
            if (args[i].equals("-p")) {
                i++;

                if (i == args.length) {
                    fatal(ERROR_USAGE, "missing value for -p option");
                } else {
                    derbyPort = Integer.parseInt(args[i]);
                    debug("derby port: " + String.valueOf(derbyPort));
                }
            }

            // get derby content directory
            if (args[i].equals("-d")) {
                i++;

                if (i == args.length) {
                    fatal(ERROR_USAGE, "missing value for -d option");
                } else {
                    derbyHome = args[i];
                    debug("derby home: " + derbyHome);
                }
            }
            // get command port
            else if (args[i].equals("-c")) {
                i++;

                if (i == args.length) {
                    fatal(ERROR_USAGE, "missing value for -c option");
                } else {
                    cmdPort = Integer.parseInt(args[i]);
                    debug("command port: " + String.valueOf(cmdPort));
                }
            }
            // get instance
            else if (args[i].equals("-i")) {
                i++;

                if (i == args.length) {
                    fatal(ERROR_USAGE, "missing value for -i option");
                } else {
                    instance = args[i];
                    debug("instance name: " + instance);
                }
            }
            // get derby host
            else if (args[i].equals("-h")) {
                i++;

                if (i == args.length) {
                    fatal(ERROR_USAGE, "missing value for -h option");
                } else {
                    derbyHost = args[i];
                    debug("derby host: " + derbyHost);

                    // convert hostname to InetAddress
                    try {
                        derbyInetAddress = InetAddress.getByName(derbyHost);
                    } catch (java.net.UnknownHostException e) {
                        fatal(ERROR_USAGE, "host " + derbyHost + " is unknown");
                    }

                    debug("derby host IP: " + derbyInetAddress.toString());
                }
            }
            // get syslog tag
            else if (args[i].equals("-t")) {
                i++;

                if (i == args.length) {
                    fatal(ERROR_USAGE, "missing value for -t option");
                } else {
                    syslogTag = args[i];
                    debug("syslog tag: " + syslogTag);
                }
            }
            // get syslogger path
            else if (args[i].equals("-s")) {
                i++;

                if (i == args.length) {
                    fatal(ERROR_USAGE, "missing value for -s option");
                } else {
                    sysloggerPath = args[i];
                    debug("syslogger path: " + sysloggerPath);
                }
            }
            // get debug mode
            else if (args[i].equals("-D")) {
                debug = true;
            }
        }

        if (derbyPort == -1)
            fatal(ERROR_USAGE, "option -p is required");

        if (derbyHome == null)
            fatal(ERROR_USAGE, "option -d is required");

        if (cmdPort == -1)
            fatal(ERROR_USAGE, "option -c is required");

        if (instance == null)
            fatal(ERROR_USAGE, "option -i is required");

        if (derbyHost == null)
            fatal(ERROR_USAGE, "option -h is required");

    }

    /**
     * Open a server socket on cmdPort
     */
    private static void createCmdSocket() {

        debug("create cmd server socket");

        try {
            cmdServerSocket = new ServerSocket(cmdPort, 0, derbyInetAddress);
        } catch (IOException e) {
            fatal(ERROR_CMD_SOCKET,
                "failed to create server socket on " + derbyInetAddress + ":" +
                cmdPort + " (" + e + ")");
        }
    }

    private static Socket waitForConnection() {

        debug("wait for new connection");

        Socket s = null;

        try {
            s = cmdServerSocket.accept();
            debug("got new command connection");
        } catch (IOException e) {
            debug("accept on command socket failed (" + e + ")");
        }

        return s;
    }

    private static boolean manageConnection(Socket cmdSocket) {
        BufferedReader reader;
        PrintStream writer;

        debug("manage command connection");

        try {
            reader = new BufferedReader(new InputStreamReader(
                        cmdSocket.getInputStream()));
            writer = new PrintStream(cmdSocket.getOutputStream(),
                    true /* autoflush */);

        } catch (IOException e) {
            debug("IOException caught while creating " +
                "reader or writer on command socket (" + e + ")");

            return true;
        }

        String receivedCmd;

        try {
            receivedCmd = reader.readLine();
        } catch (IOException e) {
            debug("IOException caught while reading command (" + e + ")");
            writer.println(ANSWER_FAILED);

            return true;
        }

        if (receivedCmd == null) {
            debug("empty command, indicate error");
            writer.println(ANSWER_FAILED);

            return true;
        }

        debug("received command: " + receivedCmd);

        if (receivedCmd.startsWith(TEST_CMD)) {

            if (!receivedCmd.equals(TEST_CMD + " " + instance)) {
                debug("received a test command for another instance");
                writer.println(ANSWER_FAILED);

                return true;
            }

            try {
                sc.logConnections(false);
            } catch (Exception e) {
                debug("Could not set logConnections to false, continue.");
            }

            try {
                sc.ping();
                writer.println(ANSWER_OK);
                debug("ping DB successful");
            } catch (Exception e) {
                writer.println(ANSWER_FAILED);
                debug("derby server is not ready (" + e + ")");
            }

            return true;

        } else if (receivedCmd.equals(DEBUG_CMD)) {

            debug = true;
            writer.println(ANSWER_OK);

            return true;

        } else if (receivedCmd.equals(NODEBUG_CMD)) {

            debug = false;
            writer.println(ANSWER_OK);

            return true;

        } else if (receivedCmd.equals(QUIT_CMD)) {

            debug("bye");
            writer.println(ANSWER_OK);

            return false;

        } else {
            debug("unknown command [" + receivedCmd + "], continue.");

            return true;
        }
    }

    private static void checkDerby() {

        debug("check derby port");

        try {
            ServerSocket s = new ServerSocket(derbyPort,
                    0 /* default backlog size */, derbyInetAddress);
            s.close();

        } catch (IOException e) {
            fatal(ERROR_DERBY_SOCKET,
                "failed to create server socket on " + derbyInetAddress + ":" +
                derbyPort + " (" + e + ")");
        }

        debug("check derby home");

        File f = new File(derbyHome);

        while ((f != null) && !f.exists()) {
            f = f.getParentFile();
        }

        if (f == null) {
            fatal(ERROR_DERBY_HOME, "derby home is not valid");
        }

        if (!f.isDirectory()) {
            fatal(ERROR_DERBY_HOME, f.getPath() + " is not a directory");
        }

        if (!f.canWrite()) {
            fatal(ERROR_DERBY_HOME, f.getPath() + " is not writable");
        }
    }

    private static void startDerby() {

        System.setProperty("derby.locks.monitor", "true");
        System.setProperty("derby.locks.deadlockTrace", "true");
        System.setProperty("derby.system.home", derbyHome);
        System.setProperty("derby.drda.logConnections", "false");

        debug("start derby");

        try {
            sc = new NetworkServerControl(derbyInetAddress, derbyPort);
            sc.start(null);
        } catch (Exception e) {
            fatal(ERROR_DERBY_START, "failed to start derby (" + e + ")");
        }

        try {
            sc.logConnections(false);
        } catch (Exception e) {

            // Derby isn't ready yet
            debug("Could not set logConnections to false, continue.");
        }
    }

    public static void main(String args[]) {

        parseArgs(args);
        checkDerby();
        startDerby();
        createCmdSocket();

        while (true) {
            Socket cmdSocket = waitForConnection();

            if (cmdSocket == null)
                continue;

            if (!manageConnection(cmdSocket))
                break;

            // close connection
            debug("closing command connection");

            try {
                cmdSocket.close();
            } catch (IOException e) {
                debug("failed to close command socket (" + e + ")");
            }
        }

        try {
            cmdServerSocket.close();
        } catch (IOException e) {
            debug("failed to close command server socket (" + e + ")");
        }
    }
}
