/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#pragma ident "@(#)glb_update.c 1.3	08/05/20 SMI"

/*
 * glb_update.c - Update method for haglb
 */

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <rgm/libdsdev.h>

/*
 * Some of the resource properties might have been updated. Restart
 * the fault monitor.and send a SIGHUP to the haglbd daemon
 */

int
main(int argc, char *argv[])
{
	scds_handle_t   scds_handle;
	scha_err_t	result;

	/* Process the arguments passed by RGM and initialize syslog */
	if (scds_initialize(&scds_handle, argc, argv) != SCHA_ERR_NOERR)
		return (1);

	/*
	 * SIGHUP the haglbd daemon to re-read
	 * properties
	 */
	result = scds_pmf_signal(scds_handle, SCDS_PMF_TYPE_SVC, 0,
		SIGHUP, 0);
	if (result != SCHA_ERR_NOERR) {
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		scds_syslog(LOG_ERR,
		    "Failed to signal haglbd.");
		/* Free up all the memory allocated by scds_initialize */
		scds_close(&scds_handle);
		return (1);
	}

	scds_syslog(LOG_INFO,
	    "Completed successfully.");

	/* Free up all the memory allocated by scds_initialize */
	scds_close(&scds_handle);

	return (0);
}
