/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * glb.c - Common utilities for SUNW.Glb RT
 */

#pragma ident	"@(#)glb.c	1.7	09/01/06 SMI"

/*
 * This utility has the methods for performing the validation,
 * starting and stopping the data service and the fault monitor.
 * It also contains the method to probe the health of the
 * data service.  The probe just returns either success or
 * failure. Action is taken based on this returned value in the
 * method found in the file glb_probe.c
 */

#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <netdb.h>
#include <resolv.h>
#include <sys/wait.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <scha.h>
#include <rgm/libdsdev.h>
#include <errno.h>
#include "glb.h"
#include <dlfcn.h>
#include <libintl.h>

/*
 * The initial timeout allowed  for the glb dataservice to
 * be fully up and running. We will wait for for 3 % (SVC_WAIT_PCT)
 * of the start_timeout time before probing the service.
 */
#define	SVC_WAIT_PCT		1

boolean_t print_errors = B_TRUE;

/*
 * validate_hlist()
 * Validates that the specified list of
 * hosts are in valid format.
 */
static int
validate_hlist(scha_str_array_t *hlist)
{
	unsigned i;
	int rc = 0;
	int err;
	struct hostent *hp;

	if (hlist == NULL)
		return (-1);
	/*
	 * Iterate over the whole list of hostnames. Look for *all* mappings
	 * for each hostname and then prune the list based on instances. We
	 * do the pruning after the lookup because we'd like to lookup a name
	 * in one getipnodebyname call and there is no way to lookup IPv4
	 * addresses only with address family set to AF_INET6
	 */
	for (i = 0; i < hlist->array_cnt; i++) {
		hp = getipnodebyname(hlist->str_array[i],
		    AF_INET6, AI_V4MAPPED | AI_ALL | AI_ADDRCONFIG, &err);
		if (hp == NULL) {
			/*
			 * SCMSGS
			 * @explanation
			 * The hostname could not be resolved into its IP
			 * address.
			 * @user_action
			 * Check the settings in /etc/nsswitch.conf and verify
			 * that the resolver is able to resolve the hostname.
			 */
			scds_syslog(LOG_ERR, "Hostname lookup failed for %s: "
			    "%s", hlist->str_array[i],
			    hstrerror(err));
			if (print_errors) {
				(void) fprintf(stderr, dgettext(TEXT_DOMAIN,
				    "Hostname lookup failed for %s: %s\n"),
				    hlist->str_array[i],
				    hstrerror(err));
			}
			rc = -1;
			break;
		}
		freehostent(hp);
	}
	return (rc);
}

/*
 * svc_validate():
 *
 * Do specific validation of the resource configuration.
 *
 */

int
svc_validate(scds_handle_t scds_handle)
{
	struct stat statbuf;
	char *cmd = "/usr/cluster/lib/sc/haglbd";
	int rc = 0;
	scha_err_t	e = SCHA_ERR_NOERR;
	scha_extprop_value_t	*xprop = NULL;
	struct hostent *hp;
	int err;

	if (stat(cmd, &statbuf) != 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * The validate method for the SUNW.Glb service was unable
		 * to access the specified command. Thus, the service could
		 * not be started.
		 * @user_action
		 * Make sure that the command exists and is executable.
		 */
		scds_syslog(LOG_ERR,
		    "Failed to access <%s>: <%s>",
		    cmd, strerror(errno)); /*lint !e746 */
		rc = 1;
		goto finished;
	}

	if (!(statbuf.st_mode & S_IXUSR)) {
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		scds_syslog(LOG_ERR,
		    "The %s command does not have execute "
		    "permission.", cmd);
		rc = 1;
		goto finished;
	}

	/* Check LB_Nodelist  property */
	e = scds_get_ext_property(scds_handle, "LB_Nodelist",
		SCHA_PTYPE_STRINGARRAY, &xprop);
	if (e != SCHA_ERR_NOERR || xprop == NULL) {
		scds_syslog(LOG_ERR,
			"Failed to retrieve the property %s: %s.",
			"LB_Nodelist", scds_error_string(e));
		rc = 1; /* Validation Failure */
		goto finished;
	}
	if (validate_hlist(xprop->val.val_strarray) != 0) {
		rc = 1;	/* Validation failure */
		goto finished;
	}
	scds_free_ext_property(xprop);
	xprop = NULL;

	/* Check LB_IPAddress  property */
	e = scds_get_ext_property(scds_handle, "LB_IPAddress",
		SCHA_PTYPE_STRING, &xprop);
	if (e != SCHA_ERR_NOERR || xprop == NULL) {
		scds_syslog(LOG_ERR,
			"Failed to retrieve the property %s: %s.",
			"LB_IPAddress", scds_error_string(e));
		rc = 1; /* Validation Failure */
		goto finished;
	}

	/* Check that it is an IP address */
	hp = getipnodebyname(xprop->val.val_str,
	    AF_INET6, AI_V4MAPPED | AI_ALL | AI_ADDRCONFIG, &err);
	if (hp == NULL) {
		scds_syslog(LOG_ERR, "Hostname lookup failed for %s: "
		    "%s", xprop->val.val_str, hstrerror(err));
		if (print_errors) {
			(void) fprintf(stderr, dgettext(TEXT_DOMAIN,
			    "Hostname lookup failed for %s: %s\n"),
			    xprop->val.val_str,
			    hstrerror(err));
		}
		rc = 1; /* Validation Failure */
		goto finished;
	}
	freehostent(hp);
	scds_free_ext_property(xprop);
	xprop = NULL;

	/* Check LB_Group  property */
	e = scds_get_ext_property(scds_handle, "LB_Group",
		SCHA_PTYPE_STRING, &xprop);
	if (e != SCHA_ERR_NOERR || xprop == NULL) {
		scds_syslog(LOG_ERR,
			"Failed to retrieve the property %s: %s.",
			"LB_Group", scds_error_string(e));
		rc = 1; /* Validation Failure */
		goto finished;
	}
	scds_free_ext_property(xprop);
	xprop = NULL;

	/* Check AddNodeCommand  property */
	e = scds_get_ext_property(scds_handle, "AddNodeCommand",
		SCHA_PTYPE_STRING, &xprop);
	if (e != SCHA_ERR_NOERR || xprop == NULL) {
		scds_syslog(LOG_ERR,
			"Failed to retrieve the property %s: %s.",
			"AddNodeCommand", scds_error_string(e));
		rc = 1; /* Validation Failure */
		goto finished;
	}

	if (stat(xprop->val.val_str, &statbuf) != 0) {
		scds_syslog(LOG_ERR,
		    "Failed to access <%s>: <%s>",
		    xprop->val.val_str, strerror(errno));	/*lint !e746 */
		rc = 1;
		goto finished;
	}

	if (!(statbuf.st_mode & S_IXUSR)) {
		scds_syslog(LOG_ERR,
		    "The %s command does not have execute "
		    "permission.", xprop->val.val_str);
		rc = 1;
		goto finished;
	}
	scds_free_ext_property(xprop);
	xprop = NULL;

	/* Check RemoveNodeCommand  property */
	e = scds_get_ext_property(scds_handle, "RemoveNodeCommand",
		SCHA_PTYPE_STRING, &xprop);
	if (e != SCHA_ERR_NOERR || xprop == NULL) {
		scds_syslog(LOG_ERR,
		    "Failed to retrieve the property %s: %s.",
		    "RemoveNodeCommand", scds_error_string(e));
		rc = 1; /* Validation Failure */
		goto finished;
	}

	if (stat(xprop->val.val_str, &statbuf) != 0) {
		scds_syslog(LOG_ERR,
		    "Failed to access <%s>: <%s>",
		    xprop->val.val_str, strerror(errno));	/*lint !e746 */
		rc = 1;
		goto finished;
	}

	if (!(statbuf.st_mode & S_IXUSR)) {
		scds_syslog(LOG_ERR,
		    "The %s command does not have execute "
		    "permission.", xprop->val.val_str);
		rc = 1;
		goto finished;
	}
	scds_free_ext_property(xprop);
	xprop = NULL;

	/* Check SpecificLBInformation  property */
	e = scds_get_ext_property(scds_handle, "SpecificLBInformation",
		SCHA_PTYPE_STRING, &xprop);
	if (e != SCHA_ERR_NOERR || xprop == NULL) {
		scds_syslog(LOG_ERR,
		    "Failed to retrieve the property %s: %s.",
		    "SpecificLBInformation", scds_error_string(e));
		rc = 1; /* Validation Failure */
		goto finished;
	}
	scds_free_ext_property(xprop);
	xprop = NULL;

	/* All validation checks were successful */
	rc = 0;


finished:
	if (xprop)
		scds_free_ext_property(xprop);

	return (rc); /* return result of validation */
}

/*
 * svc_start():
 *
 */

int
svc_start(scds_handle_t scds_handle, int argc, char *argv[])
{
	int rc = 0;
	int	i;
	char cmd[2048];

	(void) strcpy(cmd, "/usr/cluster/lib/sc/haglbd");
	for (i = 1; i < argc; i++) {
		(void) strcat(cmd, " ");
		(void) strcat(cmd, argv[i]);
	}

	/*
	 * Start haglbd under PMF.
	 */

	rc = scds_pmf_start(scds_handle, SCDS_PMF_TYPE_SVC, 0, cmd,
	    -1);

	if (rc == SCHA_ERR_NOERR) {
		scds_syslog(LOG_INFO,
		    "Start of %s completed successfully.", cmd);
	} else {
		scds_syslog(LOG_ERR,
		    "Failed to start %s.", cmd);
		goto finished;
	}

finished:

	return (rc); /* return Success/failure status */
}

/*
 * svc_stop():
 *
 * Stop the haglbd daemon
 * Return 0 on success, > 0 on failures.
 *
 */
int
svc_stop(scds_handle_t scds_handle)
{
	int rc = 0;


	/*
	 * If no stop command is specified, we use
	 * scds_pmf_stop to stop the application.
	 */
	if ((rc = scds_pmf_stop(scds_handle, SCDS_PMF_TYPE_SVC,
	    0, SIGTERM,
	    scds_get_rs_stop_timeout(scds_handle))) !=
	    SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
		    "Failed to stop %s.", "the application");
		/*
		 * Since the Data service did not stop with a
		 * scds_pmf_stop, we return non-zero.
		 */
		goto finished;
	}

finished:

	if (rc == SCHA_ERR_NOERR)
		scds_syslog(LOG_INFO, "Successfully stopped %s.",
		    "the application");

	return (rc); /* Successfully stopped */
}

/*
 * svc_wait():
 *
 * wait for the data service to start up fully and make sure
 * it is running healthy.
 */

int
svc_wait(scds_handle_t scds_handle)
{
	int svc_start_timeout;

	/*
	 * Get the Start method timeout, port number on which to probe,
	 * the Probe timeout value
	 */
	svc_start_timeout = scds_get_rs_start_timeout(scds_handle);

	/*
	 * sleep for SVC_WAIT_PCT percentage of start_timeout time
	 * before actually probing the dataservice. This is to allow
	 * the dataservice to be fully up inorder to reply to the
	 * probe. NOTE: the value for SVC_WAIT_PCT could be different
	 * for different dataservices.
	 */
	if (scds_svc_wait(scds_handle, (svc_start_timeout * SVC_WAIT_PCT)/100)
	    != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to start service.");
		return (1);
	}

	return (SCHA_ERR_NOERR);
}
