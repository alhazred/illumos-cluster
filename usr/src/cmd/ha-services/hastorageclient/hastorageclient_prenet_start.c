/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * hastorageclient_prenet_start.c
 *
 * Prenet start method implementation
 * for the HAStorageClient resource type.
 *
 */

#pragma ident	"@(#)hastorageclient_prenet_start.c 1.3	08/05/20 SMI"

#include <stdlib.h>

#include "hastorageclient.h"


/*
 *
 * Function: main
 *
 * Arguments:
 *
 * argc		Number of arguments
 *
 * argv		Argument list from command line
 *
 * Return:
 *
 * 0	if completed successfully. HAStorageClient resource
 *	is now online. RGM will attempt to bring the next
 *	resource online. A resource group will be primaried
 *	i.e. brought online on one or more nodes if the
 *	start/prenet start methods  for all contained
 *	resources return 0.
 *
 * 0	If file system mount point lists are empty.
 *	This might seem strange, but
 *	it is possible to have null lists. HAStoragePlus
 *	is effectively a noop here.
 *
 *
 * 1	if an error occurred. RGM aborts the process of
 *	bringing the resource group online.
 *
 * Comments:
 *
 * A HAStorageClient resource can belong to a resource group which is either
 * failover ( primaried on only one node ) or scalable ( primaried on
 * more than one node ). Accordingly, the prenet start method is invoked on
 * one or more nodes.
 *
 * The RGM invokes a resource's prenet start method prior to configuring
 * the network addresses. HAStorageClient prenet start
 * method ensures that it will be an early candidate in the list of
 * start methods to be invoked by the RGM.
 */
int
main(int argc, char *argv[])
{
	scds_handle_t	ds_handle;
	int		rc = 0;

	svcMethodId = HASTORAGECLIENT_PRENET_START;

	/*
	 * Initialize the DSDL subsystem (which processes the command line
	 * arguments passed by RGM and also initializes logging).
	 */

	if (scds_initialize(&ds_handle, argc, argv) != SCHA_ERR_NOERR) {

		hasc_scds_syslog_0(LOG_ERR, "Failed to initialize the DSDL.");

		return (1);
	}

	/*
	 * Revalidate all devices services prior to starting.
	 */

	if (svc_validate(ds_handle)) {

		rc = 1;
		goto end;
	}

	hasc_scds_syslog_0(LOG_INFO, "Validations completed.");

	/*
	 * Mount the file system(s) specified in the FilesystemMountPoints list.
	 */

	if (svc_mountFilesystems()) {
		/*
		 * Error messages related to mounting the file systems are
		 * already logged.
		 */
		rc = 1;
		goto end;
	}

end:
	scds_close(&ds_handle);
	return (rc);
}
