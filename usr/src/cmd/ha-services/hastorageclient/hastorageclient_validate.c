/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * hastorageclient_validate.c
 *
 * Validate method implementation for the HAStorageClient resource type.
 *
 */

#pragma ident	"@(#)hastorageclient_validate.c 1.3	08/05/20 SMI"

#include <stdlib.h>
#include <libintl.h>
#include <locale.h>

#include "hastorageclient.h"


/*
 *
 * Function: main
 *
 * Arguments:
 *
 * argc		Number of arguments
 *
 * argv 	Command line argument list
 *
 *
 * Return:
 *
 * 0 	if validations completed successfully.
 *
 * 1	if an error occurred during validation.
 *	This error causes RGM to abort the
 *	HAStorageClient resource creation.
 *
 *
 * Comments:
 *
 * While resource creation RGM invokes validate callback method on all
 * potential primary nodes i.e. all nodes which can act as a primary
 * for the resource group containing the HAStorageClient resource.
 *
 * The validate method is also invoked when resource properties are
 * updated.
 *
 */

int
main(int argc, char *argv[])
{
	scds_handle_t	ds_handle;
	int		option;
	int		rc = 0;
	extern int	optind;

	svcMethodId = HASTORAGECLIENT_VALIDATE;

	/*
	 * I18N support.
	 */
	(void) textdomain(TEXT_DOMAIN);
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);
	(void) setlocale(LC_ALL, "");

	/*
	 * Initialize the DSDL subsystem which process the command line
	 * arguments passed by RGM and also initializes logging.
	 */

	if (scds_initialize(&ds_handle, argc, argv) != SCHA_ERR_NOERR) {

		hasc_scds_syslog_0(LOG_ERR, "Failed to initialize the DSDL.");

		return (1);
	}


	/*
	 * Perform all validations.
	 */
	if (svc_validate(ds_handle)) {

		/*
		 * Error message for failure is already logged.
		 */
		rc = 1;
		goto end;
	}

	/*
	 * Determine if validate is invoked as part of update.
	 */

	optind = 1;

	while ((option = getopt(argc, argv, "R:T:G:r:x:g:cu")) != EOF) {

		if (option == 'u') {
			svcMethodId = HASTORAGECLIENT_VALIDATE_FOR_UPDATE;
		}
	}

	/*
	 * In case VALIDATE is called for UPDATE, save the old mount points
	 * because properties are overridden once the validation completes
	 * successfully.
	 * The mount points are saved to find out removed ones for update,
	 * and will be unmounted during update.
	 */

	if ((svcMethodId == HASTORAGECLIENT_VALIDATE_FOR_UPDATE) &&
		(svc_saveOldMntPts())) {

		/*
		 * Failed to save mount points, log error and return.
		 */
		hasc_scds_syslog_0(LOG_ERR, "Failed to save old mount points.");

		rc = 1;
		goto end;
	}

	hasc_scds_syslog_0(LOG_INFO,
	    "All specified mount points validated successfully.");

end:
	scds_close(&ds_handle);
	return (rc);
}
