/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * hastorageclient_validate.c
 *
 * Validate method implementation for the HAStorageClient resource type.
 *
 */

#pragma ident	"@(#)hastorageclient_update.c 1.3	08/05/20 SMI"

#include <stdlib.h>
#include <libintl.h>
#include <locale.h>

#include "hastorageclient.h"

/*
 *
 * Function:	main
 *
 * Arguments:	argc	Number of arguments.
 *		argv	Command line arguments passed by RGM.
 *
 * Return code:	0	If everything succeeded.
 *		>0	If something failed.
 *
 * Sequence:	1.	Starts device services.
 *		2.	Verify device services availability.
 *
 * Comments:	A HAStorageClient resource can belong to a resource group which
 *		is either failover (primaried on only one node) or scalable
 *		(primaried on more than one node). Accordingly, the update
 *		method is invoked on one or more nodes.
 *		If an errors occurs, the resource will be flagged as FAULTED by
 *		the update callback method.
 */
int
main(int argc, char *argv[])
{
	/*
	 * Update callback is treated in a different way. Instead of returning
	 * when an (non-fatal) error encountered, all error information is
	 * gathered related to update and at the end the resource status set to
	 * FAULTED state with the status message.
	 *
	 * NOTE:
	 * Failure of the Update method causes the syslog(3) function to
	 * generate an error message but does not affect RGM management
	 * of the resource.
	 */

	scds_handle_t	ds_handle;
	int		rc = 0;

	svcMethodId = HASTORAGECLIENT_UPDATE;

	/*
	 * Initialize the DSDL subsystem (which processes the command line
	 * arguments passed by RGM and also initializes logging).
	 */

	if (scds_initialize(&ds_handle, argc, argv) != SCHA_ERR_NOERR) {

		hasc_scds_syslog_0(LOG_ERR, "Failed to initialize the DSDL.");

		return (1);
	}

	/*
	 * Validate prior to update.
	 */

	if (svc_validate(ds_handle)) {

		/*
		 * Error message for failure is already logged.
		 */

		rc++;
		ADD_TO_STATUS_MSG("Validation failed on devices");
		goto end;
	}

	/*
	 * Unmount the file systems which are removed for update.
	 */

	scds_syslog_debug(DBG_LEVEL_HIGH, "Unmounting the removed file "
	    "systems for update");

	if (svc_umountRemovedMntPts()) {
		scds_syslog_debug(DBG_LEVEL_HIGH,
		    "Failed in unmouting the file systems");
		rc++;
	}


	if (gSvcInfo.count == 0) {

		/*
		 * FilesystemMountPoints is null. A message to
		 * this effect has already been logged.
		 * Here we return as there is no devices to be started.
		 */

		goto end;
	}

	/*
	 * Mount the file system(s) specified in the FilesystemMountPoints
	 * extension property.
	 */

	if (svc_mountFilesystems()) {
		/*
		 * Error messages related to mounting the file systems are
		 * already logged.
		 * Set the HASClient in FAULTED state and return.
		 */
		hasc_scds_syslog_1(LOG_ERR,
		    "Update on the resource failed: %s.",
		    "Failed to mount all filesystems");

		rc++;
		goto end;
	}

end:
	if (rc) {

		/*
		 * Set the HASClient resource in FAULTED state.
		 */

		(void) scha_resource_setstatus(gSvcInfo.rsName,
			gSvcInfo.rgName, SCHA_RSSTATUS_FAULTED,
			update_status_msg.str);
	} else {

		hasc_scds_syslog_0(LOG_INFO,
		    "Update on the resource is completed successfully.");

		/*
		 * Set the HASClient resource in OK state.
		 */

		(void) scha_resource_setstatus(gSvcInfo.rsName,
			gSvcInfo.rgName, SCHA_RSSTATUS_OK, "");
	}

	scds_close(&ds_handle);
	return (rc);
}
