/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * hastorageclient_monitor_stop.c
 *
 * Monitor stop method implementation for the HAStorageClient resource type.
 *
 */

#pragma ident	"@(#)hastorageclient_monitor_stop.c 1.3	08/05/20 SMI"

/*
 *
 * Function: main
 *
 * Arguments:	None
 *
 * Return:
 *
 * 0		Return 0 always.
 *
 * Comments:
 *
 * This method is invoked by RGM to stop monitoring HAStorageClient resource.
 * This is a dummy method now.
 *
 */


int
main()
{

	return (0);
}
