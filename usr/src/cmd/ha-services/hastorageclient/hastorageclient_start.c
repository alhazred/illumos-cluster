/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * hastorageclient_start.c
 *
 * Start method implementation for the HAStorageClient resource type.
 *
 */

#pragma ident	"@(#)hastorageclient_start.c 1.3	08/05/20 SMI"

/*
 *
 * Function: main
 *
 * Arguments:	None
 *
 * Return:
 *
 * 0		Return 0 always.
 *
 * Comments:
 *
 * This method is invoked by RGM to bring online the resource. This is a dummy
 * method as all the work to bring the resource is done in prenet_start.
 * HAStorageClient uses prenet_start as it has to ensure the
 * file systems available before data services start method.
 *
 */


int
main()
{

	return (0);
}
