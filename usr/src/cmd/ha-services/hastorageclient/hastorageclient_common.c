/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * hastorageclient_common.c
 *
 * Common functions invoked by various
 * HAStorageClient method implementations.
 *
 *
 */
#pragma ident	"@(#)hastorageclient_common.c	1.3	08/05/20 SMI"

#ifndef	_REENTRANT
#define	_REENTRANT
#endif

#include <errno.h>
#include <thread.h>
#include <synch.h>
#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <sys/debug.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <assert.h>
#ifdef linux
#include <fstab.h>
#include <mntent.h>
#include <string.h>
#else
#include <sys/vfstab.h>
#include <sys/mnttab.h>
#endif
#include <sys/wait.h>

#include <unistd.h>
#include <limits.h>
#include <stdarg.h>
#include <dirent.h>
#include <fcntl.h>

#include "hastorageclient.h"
#include <pthread.h>

#ifdef linux
#define	VFS_LINE_MAX	512
#define	MNT_LINE_MAX	512
#define	MAXPATHLEN	1024
#endif
/*
 * Private defines
 */

#define	SvcMountOptLocalNFS	1	/* Local mount option  */
#define	SvcMountOptError	2

/*
 * Private functions declarations
 */
static int svc_fillAndValidateList(scha_extprop_value_t *);
#ifdef linux
static int svc_evalFsTabMountOption(const struct fstab *, char *);

static int svc_evalMntEntMountOption(const struct mntent *, char *);
#else
static int svc_evalVfsTabMountOption(const struct vfstab *, char *);

static int svc_evalMntTabMountOption(const struct mnttab *, char *);
#endif

static uint_t svc_getpathdepth(char *path);

static void svc_setrpath(fsentry_t *fsp);

static int svc_mntcompar(const void *a, const void *b);

static int svc_doParMount(fsentry_t **mount_list, unsigned int nentries);

static int svc_executeCmdusingPopen(char *cmd, char **cmd_output);

static int svc_executeCmdusingFork(char *cmd, char **cmd_output);

void *svc_doMount(void *arg);

static int svc_doParUmount(char *umnt_pts);

static int svc_sprintf(char **, const char *, ...);



/*
 *
 * Function: svc_validate
 *
 * Arguments:
 *
 * argc 	Number of arguments
 *
 * argv		Command line argument list
 *
 * method	Method Id
 *
 * Return:
 *
 * 0	if all validations completed
 *	successfully.
 *
 * 1	an error occurred during validation.
 *
 *
 * Comments:
 *
 * Individual steps explained below. svc_validate() is called
 * during (a) resource validation (b) RG switchovers
 * (c) Updates of resource properties.
 *
 */
int
svc_validate(scds_handle_t ds_handle)
{

	const char *rg_name, *res_name;
	scha_extprop_value_t *fmp_prop_value = NULL;
	scha_err_t ret;

	/*
	 * Obtain the resource name.
	 */
	res_name = scds_get_resource_name(ds_handle);

	if (!res_name) {
		hasc_scds_syslog_0(LOG_ERR,
		    "Failed to retrieve resource name.");

		goto error;
	}

	scds_syslog_debug(DBG_LEVEL_HIGH, "Resource name is %s.", res_name);

	/*
	 * Store a copy of the DSDL ext property value string in 'gSvcInfo'.
	 */
	if ((gSvcInfo.rsName = strdup(res_name)) == NULL) {

		hasc_scds_syslog_0(LOG_ERR, "Failed to allocate memory.");

		return (1);
	}

	/*
	 * Obtain the resource group name.
	 */
	rg_name = scds_get_resource_group_name(ds_handle);

	if (!rg_name) {
		hasc_scds_syslog_0(LOG_ERR,
		    "Failed to retrieve resource group name.");

		goto error;
	}

	/*
	 * Store a copy of the DSDL ext property value string in 'gSvcInfo'.
	 */
	if ((gSvcInfo.rgName = strdup(rg_name)) == NULL) {
		hasc_scds_syslog_0(LOG_ERR, "Failed to allocate memory.");
		goto error;
	}

	scds_syslog_debug(DBG_LEVEL_HIGH, "Resource group name is %s.",
			rg_name);

	/*
	 * Store the resource group mode in 'gSvcInfo'.
	 */

	gSvcInfo.rgMode = scds_get_rg_rg_mode(ds_handle);

	/*
	 * Return if the RG mode is neither failover nor
	 * scalable.
	 */

	if (gSvcInfo.rgMode != RGMODE_FAILOVER &&
	    gSvcInfo.rgMode != RGMODE_SCALABLE) {
			hasc_scds_syslog_0(LOG_ERR,
			    "Failed to retrieve resource group mode.");
		goto error;
	}

	scds_syslog_debug(DBG_LEVEL_HIGH, "Resource group mode is %d.",
			gSvcInfo.rgMode);
	/*
	 * Retrieve the FilesystemMountPoints extension
	 * property information.
	 */
	ret = scds_get_ext_property(ds_handle,
	    FILESYSTEMMOUNTPOINTS, SCHA_PTYPE_STRINGARRAY,
	    &fmp_prop_value);

	if (ret != SCHA_ERR_NOERR) {
		hasc_scds_syslog_2(LOG_ERR,
		    "Failed to retrieve property %s: %s.",
		    FILESYSTEMMOUNTPOINTS,	/* CSTYLED */
		    scds_error_string(ret));	/*lint !e666 */

		goto error;
	}

	scds_syslog_debug(DBG_LEVEL_HIGH,
			"Number of file system mount points: %d.",
			fmp_prop_value->val.val_strarray->array_cnt);

	/*
	 * Check if the file system mount point list values is empty.
	 * If so, return 0 instead of flagging this as an error.
	 *
	 * The ext property is stored in an internal string array.
	 *
	 */
	if (fmp_prop_value->val.val_strarray->array_cnt == 0) {

		hasc_scds_syslog_1(LOG_WARNING,
		    "Extension property %s is empty.",
		    FILESYSTEMMOUNTPOINTS);

		/*
		 * Set the count to 0. We'll inspect count
		 * later to know if gSvcInfo.list is empty.
		 *
		 */

		gSvcInfo.count = 0;

		return (0);
	}


	/*
	 * Set gSvcInfo.count to the total number
	 * of FilesystemMountPoints.
	 */
	gSvcInfo.count = fmp_prop_value->val.val_strarray->array_cnt;

	/*
	 * Invoke the function with file system mountpoint arguments.
	 */
	if (svc_fillAndValidateList(fmp_prop_value)) {
		/*
		 * Error message already logged.
		 */
		goto error;
	}
	if (fmp_prop_value) scds_free_ext_property(fmp_prop_value);

	/*
	 * Signifies successful completion of svc_validate().
	 */
	return (0);

error:
	if (fmp_prop_value) scds_free_ext_property(fmp_prop_value);

	return (1);
}


#ifdef linux
/*
 *
 * Function: svc_fillAndValidateList
 *
 * Arguments:
 *
 * scha_extprop_value_t *fmp Array of file system mount
 *			points
 *
 * Return:
 *
 * 0		if all file system mount points were validated
 *              successfully.
 *
 * 1		if an error occurred.
 *
 * Comments:
 *
 * Individual steps explained below.
 *
 * Note:
 *
 * Dynamically allocated memory is not freed. This is because
 * all objects created with dynamic memory are accessed till
 * the end of program execution. Rather than free'ing memory
 * at the end of the program execution ( exit ), no
 * free() calls are made.
 *
 */

int
svc_fillAndValidateList(scha_extprop_value_t *fmp)
{

	uint_t i = 0;
	char buffer[1024];
	struct fstab *vp, *gvp;
	FILE *fp = NULL;

	/*
	 * Allocate memory for an array of 'gSvcInfo.count' HAStorageClientSvc
	 * objects. Each object represents a global device path or a file
	 * system mount point.
	 *
	 */
	gSvcInfo.list = (HAStorageClientSvc *)
	    malloc(sizeof (HAStorageClientSvc) * gSvcInfo.count);

	if (!gSvcInfo.list) {

		hasc_scds_syslog_0(LOG_ERR, "Failed to allocate memory.");

		return (1);

	}

	/*
	 * Memset the newly allocated array to nullify all elements.
	 *
	 */
	(void) memset(gSvcInfo.list, 0,
			sizeof (HAStorageClientSvc) * gSvcInfo.count);

	/*
	 * FSTAB has to be opened prior to verifying
	 * fstab entries.
	 */
	if ((fp = fopen(_PATH_FSTAB, "r")) == NULL) {
		/* CSTYLED */
		int ret = errno;    /*lint !e746 */
		hasc_scds_syslog_2(LOG_ERR,
		    "Failed to open %s: %s.",	/* CSTYLED */
		    _PATH_FSTAB, strerror(ret));	/*lint !e666 */
		return (1);
	}

	/*
	 *
	 * For each file system mount point,
	 * verify that a vfstab entry does exist
	 * Store each file system mount point string
	 * in the 'svcPath' member.
	 */
	for (i = 0; i < gSvcInfo.count; i++) {
		int ret;

		if ((gSvcInfo.list[i].svcPath =
		    strdup(fmp->val.val_strarray->str_array[i]))
		    == NULL) {
			hasc_scds_syslog_0(LOG_ERR,
			    "Failed to allocate memory.");
			return (1);
		}

		/*
		 * Obtain a filled 'struct vfstab' entry for the
		 * given mount point.
		 */
		vp = getfsfile((char *)gSvcInfo.list[i].svcPath);

		if (vp != NULL) {

				/*
				 *  Mount point is present in VFSTAB.
				 */
			scds_syslog_debug(DBG_LEVEL_HIGH,
			    "Detected a VFSTAB entry for file"
			    " system mount point %s.",
			    vp->fs_file);

		} else  {

				/*
				 *  Reached end of file. Hence entry
				 *  for mount point not present in
				 *  FSTAB.
				 */

			hasc_scds_syslog_2(LOG_ERR,
			    "Entry for file system mount point %s"
			    " absent from %s.",
			    gSvcInfo.list[i].svcPath, _PATH_FSTAB);
			return (1);

		}

		/*
		 * Evaluate the 'struct vfstab' for this
		 * mount point. This is to know whether the mount
		 * point is for a  global/local file
		 * file system.
		 *
		 */
		ret = svc_evalFsTabMountOption(vp, buffer);

		switch (ret)  {
		case SvcMountOptLocalNFS :

			/* Local mount point */

			scds_syslog_debug(DBG_LEVEL_HIGH,
			    "File system associated with"
			    " mount point %s is to be"
			    " locally mounted.",
			    gSvcInfo.list[i].svcPath);
			break;

		case SvcMountOptError :
		default : {

			/*
			 * Vfstab entry not recognized.
			 */

			hasc_scds_syslog_3(LOG_ERR,
			    "Entry in %s for file system mount"
			    " point %s is incorrect: %s.",
			    FSTAB, gSvcInfo.list[i].svcPath,
			    buffer);

			return (1);

		}

		}
		/*
		 * Copy the vfstab entry information to global HASClient
		 * structure which can be used later during mounting
		 * to avoid re-read mount point information.
		 */

		gvp = &gSvcInfo.list[i].fsEntry;

		gvp->fs_spec = strdup(vp->fs_spec);
		gvp->fs_file = strdup(vp->fs_file);
		gvp->fs_vfstype = strdup(vp->fs_vfstype);
		gvp->fs_passno = vp->fs_passno;
		gvp->fs_mntops = strdup(vp->fs_mntops);

		if (!(gvp->fs_spec && gvp->fs_vfstype &&
		    gvp->fs_mntops)) {
			hasc_scds_syslog_0(LOG_ERR,
			    "Failed to allocate memory.");
			return (1);
		}

		/*
		 * To enable searches from
		 * the beginning of the VFSTAB file.
		 * This is to allow the ordering of
		 * file systems in FilesystemMountPoints
		 * to be independent of those
		 * specified in the VFSTAB file.
		 *
		 */
		rewind(fp);

	} /* for */

	(void) fclose(fp);

	return (0);
}

/*
 *
 * Function: svc_evalFsTabMountOption
 *
 * Arguments:
 *
 * const struct fstab *vp	fstab entry
 *
 * char *buffer		Explanation  if
 *			an incorrect vfstab
 *			entry was detected.
 *
 *
 * Return:
 *
 * SvcMountOptLocalNFS	vfstab entry represents a file
 *			system to be NFS mounted locally
 * SvcMountOptErr 	error i.e incorrect vfstab
 * 			entry.
 *			'buffer' contains an explanation.
 *
 * Comments:
 *
 *
 * Note: Case insensitive searchs are performed. As in the case
 * with HAStorage, white spaces are not removed prior to string
 * comparisons.
 *
 */
int
svc_evalFsTabMountOption(const struct fstab *vp, char *buffer)
{

	int tmpvar = SvcMountOptError;
	char tmpbuf[VFS_LINE_MAX]; /* buffer large enough to handle mntopts */

	buffer[0] = '\0';	/* null terminate it */

	/*
	 * Check that the file sytem is of type NFS
	 */
	if (strcmp(vp->fs_vfstype, "nfs")) {
		return (SvcMountOptError);
	} else {
		tmpvar = SvcMountOptLocalNFS;
	}

	/*
	 * strstr() performs a string match.
	 */
	if (vp->fs_mntops != NULL) {
		/*
		 * mntops may be of the forms:
		 *  - PATTERN
		 *  - <something>,PATTERN
		 *  - PATTERN,<something>
		 *  - <something>,PATTERN,<something>
		 * By encapsulating it in commas, mntops will be of the form:
		 * - <something or nil>,PATTERN,<something or nil>
		 */
		tmpbuf[0] = ',';
		(void) strcpy(tmpbuf+1, vp->fs_mntops);	/* CSTYLED */
		tmpbuf[strlen(tmpbuf)+1] = '\0';		/*lint !e661 */
						/* mntopt < fstab line max */
		tmpbuf[strlen(tmpbuf)] = ',';


		if (!strstr(tmpbuf, ",noauto,")) {
			tmpvar = SvcMountOptError;
			(void) strcpy(buffer, "File system mount"
			    "point should specify "
			    "mount options 'noauto'");
		} else {
			tmpvar = SvcMountOptLocalNFS;
		}

	}

	return (tmpvar);

}

/*
 * Function: svc_evalMntEntMountOption
 *
 * Arguments:
 *
 * mp		mntent entry.
 *
 * buffer	Explanation  if	an incorrect mnt entry was detected.
 *
 *
 * Return:
 *
 * SvcMountOptLocalNFS	mnt entry represents a NFS file
 *			system locally mounted
 *
 * SvcMountOptErr 	error i.e incorrect mnt
 * 			entry.
 *			'buffer' contains an explanation.
 */

static int
svc_evalMntEntMountOption(const struct mntent *mp, char *buffer)
{

	int	tmpvar = SvcMountOptError;
	char	tmpbuf[MNT_LINE_MAX]; /* buffer to handle mntopts */

	buffer[0] = '\0';	/* null terminate it */

	/*
	 * Check that the file sytem is of type NFS
	 */
	if (strcmp(mp->mnt_type, "nfs") != 0) {
		return (SvcMountOptError);
	} else {
		tmpvar = SvcMountOptLocalNFS;
	}

	return (tmpvar);
}

#else /* !linux i.e. solaris */

/*
 *
 * Function: svc_fillAndValidateList
 *
 * Arguments:
 *
 * scha_extprop_value_t *fmp Array of file system mount
 *			points
 *
 * Return:
 *
 * 0		if all device services were validated
 *              successfully.
 *
 * 1		if an error occurred.
 *
 * Comments:
 *
 * Individual steps explained below.
 *
 * Note:
 *
 * Dynamically allocated memory is not freed. This is because
 * all objects created with dynamic memory are accessed till
 * the end of program execution. Rather than free'ing memory
 * at the end of the program execution ( exit ), no
 * free() calls are made.
 *
 */

int
svc_fillAndValidateList(scha_extprop_value_t *fmp)
{

	uint_t i = 0;
	char buffer[1024];
	struct vfstab vp, vref, *gvp;
	FILE *fp = NULL;

	/*
	 * Allocate memory for an array of 'gSvcInfo.count' HAStorageClientSvc
	 * objects. Each object represents a global device path or a file
	 * system mount point.
	 *
	 */

	gSvcInfo.list = (HAStorageClientSvc *)
	    malloc(sizeof (HAStorageClientSvc) * gSvcInfo.count);

	if (!gSvcInfo.list) {
		hasc_scds_syslog_0(LOG_ERR, "Failed to allocate memory.");

		return (1);
	}

	/*
	 * Memset the newly allocated array to nullify all elements.
	 *
	 */
	(void) memset(gSvcInfo.list, 0,
			sizeof (HAStorageClientSvc) * gSvcInfo.count);

	/*
	 * Examine the list of file system mount points.
	 *
	 */

	(void) memset(&vref, 0, sizeof (vref));

	/*
	 * VFSTAB has to be opened prior to verifying
	 * vfstab entries.
	 */
	if ((fp = fopen(VFSTAB, "r")) == NULL) {
		/* CSTYLED */
		int ret = errno;    /*lint !e746 */
		hasc_scds_syslog_2(LOG_ERR,
		    "Failed to open %s: %s.",	/* CSTYLED */
		    VFSTAB, strerror(ret));	/*lint !e666 */
		return (1);
	}

	/*
	 * For each file system mount point,
	 * verify that a vfstab entry does exist
	 *
	 */
	for (i = 0; i < gSvcInfo.count; i++) {

		int ret;

		if ((gSvcInfo.list[i].svcPath =
		    strdup(fmp->val.val_strarray->str_array[i]))
		    == NULL) {
			hasc_scds_syslog_0(LOG_ERR,
			    "Failed to allocate memory.");
			return (1);
		}

		/*
		 * vref is a reference vfstab object. Initialize it
		 * to the mount point whose existence we are
		 * trying to verify.
		 */
		vref.vfs_mountp = (char *)gSvcInfo.list[i].svcPath;

		/*
		 * Obtain a filled 'struct vfstab' entry for the
		 * given mount point.
		 */
		ret = getvfsany(fp, &vp, &vref);

		if (ret == 0) {
			/*
			 *  Mount point is present in VFSTAB.
			 */
			scds_syslog_debug(DBG_LEVEL_HIGH,
			    "Detected a VFSTAB entry for file"
			    " system mount point %s.",
			    vref.vfs_mountp);

		} else if (ret == -1)  {

			/*
			 *  Reached end of file. Hence entry
			 *  for mount point not present in
			 *  VFSTAB.
			 */

			hasc_scds_syslog_2(LOG_ERR,
			    "Entry for file system mount point %s"
			    " absent from %s.",
			    vref.vfs_mountp, VFSTAB);

			return (1);
		} else {

				/*
				 * A read error occurred.
				 */

			hasc_scds_syslog_2(LOG_ERR,
			    "Failed to read %s: %s.",
			    VFSTAB,		/* CSTYLED */
			    strerror(ret));	/*lint !e666 */

			return (1);
		}


		/*
		 * Evaluate the 'struct vfstab' for this
		 * mount point. This is to know whether the mount
		 * point is for a  global/local file
		 * file system.
		 *
		 */
		ret = svc_evalVfsTabMountOption(&vp, buffer);
		switch (ret)  {
		case SvcMountOptLocalNFS :
			/*
			 * Local mount point
			 * of a NFS file system
			 */

			scds_syslog_debug(DBG_LEVEL_HIGH,
			    "File system associated with"
			    " mount point %s is to be"
			    " locally mounted.",
			    gSvcInfo.list[i].svcPath);
			break;

		case SvcMountOptError :
		default :

			/*
			 * Vfstab entry not recognized.
			 */

			hasc_scds_syslog_3(LOG_ERR,
			    "Entry in %s for file system mount"
			    " point %s is incorrect: %s.",
			    VFSTAB, gSvcInfo.list[i].svcPath,
			    buffer);

			return (1);

		}

		/*
		 * Copy the vfstab entry information to global HASClient
		 * structure which can be used later during mounting
		 * to avoid re-read mount point information.
		 */

		gvp = &gSvcInfo.list[i].vfsEntry;

		gvp->vfs_special = strdup(vp.vfs_special);
		gvp->vfs_mountp = strdup(vp.vfs_mountp);
		gvp->vfs_fstype = strdup(vp.vfs_fstype);
		gvp->vfs_automnt = strdup(vp.vfs_automnt);
		gvp->vfs_mntopts = strdup(vp.vfs_mntopts);

		if (!(gvp->vfs_special && gvp->vfs_mountp &&
		    gvp->vfs_fstype && gvp->vfs_automnt &&
		    gvp->vfs_mntopts)) {

			hasc_scds_syslog_0(LOG_ERR,
			    "Failed to allocate memory.");
			return (1);
		}

		/*
		 * To enable searches from
		 * the beginning of the VFSTAB file.
		 * This is to allow the ordering of
		 * file systems in FilesystemMountPoints
		 * to be independent of those
		 * specified in the VFSTAB file.
		 *
		 */
		rewind(fp);

	} /* for */

	(void) fclose(fp);

	return (0);
}

/*
 *
 * Function: svc_evalVfsTabMountOption
 *
 * Arguments:
 *
 * const struct vfstab *vp	vfstab entry
 *
 * char *buffer		Explanation  if
 *			an incorrect vfstab
 *			entry was detected.
 *
 *
 * Return:
 *
 * SvcMountOptLocalNFS	vfstab entry represents a file
 *			system to be NFS mounted locally
 *
 * SvcMountOptGlobal	vfstab entry represents a global file
 *			system
 *
 * SvcMountOptErr 	error i.e incorrect vfstab
 * 			entry.
 *			'buffer' contains an explanation.
 *
 * Comments:
 *
 *
 * Note: Case insensitive searches are performed. As in the case
 * with HAStorage, white spaces are not removed prior to string
 * comparisons.
 *
 */
int
svc_evalVfsTabMountOption(const struct vfstab *vp, char *buffer)
{

	int tmpvar = SvcMountOptError;
	char tmpbuf[VFS_LINE_MAX]; /* buffer large enough to handle mntopts */

	assert(vp != NULL && buffer != NULL);

	buffer[0] = '\0';	/* null terminate it */

	/*
	 * Check that the file sytem is of type NFS
	 */
	if (strcmp(vp->vfs_fstype, "nfs") != 0) {
		(void) strcpy(buffer, "File system mount point is "
		    "not of nfs type");
		return (SvcMountOptError);
	} else {
		tmpvar = SvcMountOptLocalNFS;
	}


	/*
	 * strstr() performs a string match.
	 */
	if (vp->vfs_mntopts != NULL) {
		/*
		 * mntopts may be of the forms:
		 *  - PATTERN
		 *  - <something>,PATTERN
		 *  - PATTERN,<something>
		 *  - <something>,PATTERN,<something>
		 * By encapsulating it in commas, mntopts will be of the form:
		 * - <something or nil>,PATTERN,<something or nil>
		 */
		tmpbuf[0] = ',';
		(void) strcpy(tmpbuf+1, vp->vfs_mntopts);	/* CSTYLED */
		tmpbuf[strlen(tmpbuf)+1] = '\0';		/*lint !e661 */
						/* mntopts < vfstab line max */
		tmpbuf[strlen(tmpbuf)] = ',';

		if (strstr(tmpbuf, ",m,")) {

			/*
			 * 'm' option is forbidden.
			 */
			(void) strcpy(buffer, "File system mount point should "
				"not specify the 'm' mount option");
			return (SvcMountOptError);

		}

		if (strstr(tmpbuf, ",global,"))
			tmpvar = SvcMountOptError;
		else
			tmpvar = SvcMountOptLocalNFS;

	}

	/*
	 * At last look at the 'mount at boot'
	 * option.
	 */
	if ((vp->vfs_automnt == NULL) ||
	    ((tmpvar == SvcMountOptLocalNFS) &&
	    (strcmp(vp->vfs_automnt, "no")))) {
		/*
		 * Incorrect vfstab entry.
		 */
		(void) strcpy(buffer, "File system mount point should specify "
			"'mount at boot' as 'no'");

		return (SvcMountOptError);
	}

	return (tmpvar);
}

/*
 * Function: svc_evalMntTabMountOption
 *
 * Arguments:
 *
 * mp		mnttab entry.
 *
 * buffer	Explanation  if	an incorrect mnttab entry was detected.
 *
 *
 * Return:
 *
 * SvcMountOptLocalNFS	mnttab entry represents a local file
 *			system
 *
 * SvcMountOptErr 	error i.e incorrect mnttab
 * 			entry.
 *			'buffer' contains an explanation.
 */

static int
svc_evalMntTabMountOption(const struct mnttab *mp, char *buffer)
{

	int	tmpvar = SvcMountOptError;
	char	tmpbuf[MNT_LINE_MAX]; /* buffer to handle mntopts */

	assert(mp != NULL && buffer != NULL);

	buffer[0] = '\0';	/* null terminate it */

	/*
	 * Check that the file sytem is of type NFS
	 */
	if (strcmp(mp->mnt_fstype, "nfs") != 0) {
		return (SvcMountOptError);
	} else {
		tmpvar = SvcMountOptLocalNFS;
	}

	if (mp->mnt_mntopts == NULL) {

		/*
		 * A '-' is not specified for the mount option.
		 */

		/*
		 * mntopts may be of the forms:
		 *  - PATTERN
		 *  - <something>,PATTERN
		 *  - PATTERN,<something>
		 *  - <something>,PATTERN,<something>
		 * By encapsulating it in commas, mntopts will be of the form:
		 * - <something or nil>,PATTERN,<something or nil>
		 */

		tmpbuf[0] = ',';
		(void) strcpy(tmpbuf+1, mp->mnt_mntopts);	/* CSTYLED */
		tmpbuf[strlen(tmpbuf)+1] = '\0';		/*lint !e661 */
		tmpbuf[strlen(tmpbuf)] = ',';

		if (strstr(tmpbuf, ",global,"))
			tmpvar = SvcMountOptError;
		else
			tmpvar = SvcMountOptLocalNFS;

	}

	return (tmpvar);
}
#endif /* linux */

/*
 * Function: svc_addStr2Array
 *
 * Arguments:
 *
 * const char *str 	string
 *
 * scha_strarray_t	string container terminated by NULL
 *
 * Return:
 *
 *	0 	if the addition succeeded.
 *
 *	1	if an error occurred.
 *
 * Comments:
 *
 * A copy of the string is made after which it is added to
 * the string array. The scha_strarray_t's internal array
 * of character pointers is realloc'ed as a part of this
 * addition.
 *
 */
int
svc_addStr2Array(const char *str, scha_str_array_t *sa)
{
	char *str_copy, **ptr;

	ASSERT(str != NULL);
	ASSERT(sa != NULL);

	str_copy = strdup(str);

	if (str_copy == NULL) {

		hasc_scds_syslog_0(LOG_ERR, "Failed to allocate memory.");
		return (1);

	}

	if (sa->array_cnt == 0) {

		ptr = (char **)malloc(2*(sizeof (char *)));
		if (ptr == NULL) {
			hasc_scds_syslog_0(LOG_ERR,
			    "Failed to allocate memory.");
			return (1);
		}

		ptr[sa->array_cnt++] = str_copy;
		ptr[sa->array_cnt] = NULL;

		sa->str_array = ptr;

	} else {

		/*
		 * Increase the str_array's number of elements by
		 * one, to store an additional character string.
		 */
		ptr = (char **)realloc(sa->str_array,
			(sa->array_cnt + 2)*(sizeof (char *)));

		if (ptr == NULL) {

			hasc_scds_syslog_0(LOG_ERR,
			    "Failed to allocate memory.");
			return (1);
		}

		ptr[sa->array_cnt++] = str_copy;
		ptr[sa->array_cnt] = NULL;

		sa->str_array = ptr;
	}

	return (0);

}

#define	STRBUF_SIZE	BUFSIZ

/*
 * Function: svc_addStr2Strbuf
 *
 * Arguments:
 *
 * const char *str	string
 *
 * strbuf		string buffer container
 *
 * Return:
 *
 *	0	if append succeeded.
 *
 *	1	if an error occurred.
 *
 * Comments: *
 * This function appends the string to the existing string buffer.
 * Initially STRBUF_SIZE bytes memory is allocated to string buffer, and it is
 * dynamically extended by STRBUF_SIZE bytes whenever needed.
 */

int
svc_addStr2Strbuf(hasc_strbuf_t *strbuf, char *str) {

	char *ptr;
	int x;

	if (str == NULL)
		return (0);

	/*
	 * Check if this is the first string to be added in string buffer.
	 */

	if (strbuf->str == NULL) {

		strbuf->str = (char *)malloc(STRBUF_SIZE*sizeof (char));

		if (strbuf->str == NULL) {

			hasc_scds_syslog_0(LOG_ERR,
			    "Failed to allocate memory.");

			return (1);
		}

		strbuf->str_capacity = STRBUF_SIZE;
		strbuf->str_length = 0;
		strbuf->str[0] = '\0';
	}

	/*
	 * Check the buffer for sufficient memory to copy the string.
	 */

	if ((strbuf->str_length + strlen(str) + 1) > strbuf->str_capacity) {

		/*
		 * Not having sufficient memory, extend the buffer capacity.
		 */

		x = strlen(str) / STRBUF_SIZE;

		ptr = (char *)realloc(strbuf->str,
		    (strbuf->str_capacity + (x * STRBUF_SIZE)) * sizeof (char));

		if (ptr == NULL) {
			hasc_scds_syslog_0(LOG_ERR,
			    "Failed to allocate memory.");

			return (1);
		}

		strbuf->str = ptr;

		/* Update str_capacity property of the strbuf. */

		strbuf->str_capacity += STRBUF_SIZE;
	}

	/*
	 * Append string to string buffer.
	 */

	(void) strcpy(&strbuf->str[strbuf->str_length], str);

	/*
	 * Update the str_length property of strbuf.
	 */

	strbuf->str_length += strlen(str);

	return (0);
}

/*
 *
 * Function: svc_getpathdepth
 *
 * Arguments:
 *
 * path		The path name to find the depth
 *
 * Return:
 *
 * uint_t	The number of components (depth) of the path
 *
 * Comments:
 *
 * This function returns the depth of the given pathname.
 */

static uint_t
svc_getpathdepth(char *path)
{
	uint_t	depth;
	char	*cp;

	if (path == NULL || *path == '\0' || *path != '/')
		return (0);	/* this should never happen */

	/* root (/) is the minimal case */
	depth = 1;

	for (cp = path + 1; *cp; cp++, path++) {

		/* "///" counts as 1 */
		if (*path == '/' && *cp != '/')
			depth++;
	}

	return (depth);
}


/*
 * Function: svc_setrpath
 *
 * Arguments:
 *
 * fsp		The file system entry
 *
 * Return: void
 *
 * Comments:
 *
 * This function sets the real path for the file system mount point entry.
 */

static void
svc_setrpath(fsentry_t *fsp)
{
	char	real_path[PATH_MAX];

	assert(fsp != NULL);

	if ((realpath(fsp->mountp, real_path)) == NULL)
		fsp->status |= FS_RPFAILED;
	else
		fsp->status &= ~FS_RPFAILED;

	if (fsp->rpath)
		free(fsp->rpath);

	if ((fsp->rpath = strdup(real_path)) == NULL) {

		hasc_scds_syslog_0(LOG_ERR, "Failed to allocate memory.");

		exit(1);
	}
}

/*
 *
 * Function: svc_mntcompar
 *
 * Arguments:
 *
 * a, b		The mount point entries to compare
 *
 * Return:
 *
 * int		The difference in number of components of "a" and "b"
 *
 * Comments:
 *
 * This function sorts mount entries based on the number of components in
 * ascending order.
 */

static int
svc_mntcompar(const void *a, const void *b)
{
	assert(a != NULL && b != NULL);
	return ((int)(((*(fsentry_t **)a)->depth)-((*(fsentry_t **)b)->depth)));
}


#ifdef linux
/*
 *
 * Function: svc_mountFilesystems
 *
 * Arguments: none
 *
 * Return:
 *
 * 0		if no entries to mount, or
 *		if all file systems specified in FilesystemMountPoints
 *		property were mounted successfully
 *
 * 1		if an error occurred.
 *
 * Comments:
 * The mount points that are specified in "FilesystemMountPoints" extension
 * property are mounted if they are not mounted already (i.e no entry in
 * MNTTAB file).
 *
 * LINUX version
 */

int
svc_mountFilesystems()
{
	fsentry_t	**mntlist; /* Contains the list of mount entries */
	uint_t		nentries;	/* Number of entries to mount */
	struct mntent	*mp;
	FILE		*mfp = NULL;
	int		rc;
	uint_t		count, i;
	fsentry_t	**validmounts, **tmplist, **tmpvalidmounts;

	/*
	 * Examine the number of FMP entries and return if no entry exist.
	 */

	if (gSvcInfo.count == 0)
		return (0);

	/*
	 * Open the MNTTAB file used to check if a file system is
	 * mounted or not.
	 * NOTE:
	 * Checking the mount entry in VFSTAB is not done here (again) as it
	 * is done during validation (in svc_validate).
	 */

	if ((mfp = setmntent(_PATH_MOUNTED, "r")) == NULL) {

		rc = errno;

		hasc_scds_syslog_2(LOG_ERR,
		    "Failed to open %s: %s.",
		    _PATH_MOUNTED,		/* CSTYLED */
		    strerror(rc));	/*lint !e666 */

		return (1);
	}

	/*
	 * Allocate space for maximum possible mount entries in the list.
	 */

	mntlist = (fsentry_t **)
	    calloc((gSvcInfo.count +1), sizeof (fsentry_t *));

	if (mntlist == NULL) {

		hasc_scds_syslog_0(LOG_ERR, "Failed to allocate memory.");

		return (1);
	}

	/*
	 * Nullify the elements in the mnttab structure.
	 */

	for (i = 0, count = 0; i < gSvcInfo.count; i++) {

		if (mntlist[count]  == NULL) {

			mntlist[count] =
			    (fsentry_t *)malloc(sizeof (fsentry_t));

			if (mntlist[count]  == NULL) {

				hasc_scds_syslog_0(LOG_ERR,
				    "Failed to allocate memory.");

				return (1);

			}

			mntlist[count]->rpath = NULL;
		}

		mntlist[count]->vp = &gSvcInfo.list[i].fsEntry;
		mntlist[count]->mountp = gSvcInfo.list[i].fsEntry.fs_file;

		svc_setrpath(mntlist[count]);

		mntlist[count]->depth =
		    svc_getpathdepth(mntlist[count]->rpath);

		/*
		 * Rewind the MTAB file pointer so that the next mount point
		 * entry search starts from the beginning.
		 */

		rewind(mfp);

		/*
		 * Mount point entry existence in MNTTAB file has to be checked
		 * with realpath of mount point.
		 */

		while ((mp = getmntent(mfp)) != NULL) {
			if (strcmp(mntlist[count]->rpath,
			    mp->mnt_dir) == 0)
				break;
		};

		if (mp != NULL) {

			/*
			 * File system is already mounted. Skip it.
			 */

			scds_syslog_debug(DBG_LEVEL_HIGH,
			    "File system associated with %s is already mounted",
			    mp->mnt_dir);

			continue;

		}

		/*
		 * Mount point is not in MNTTAB file. So keep it in mount list.
		 * NOTE: Incrementing count leaves the current mount point
		 * entry in mount list and points to next empty slot.
		 */

		count++;

	}

	/*
	 * Mark the end position to the list of mount entries.
	 * Now the mntlist contains the list of entries which has to be
	 * mounted.
	 */

	mntlist[count] = NULL;

	nentries = count;

	scds_syslog_debug(DBG_LEVEL_HIGH, "The number of entries to mount "
	    "are: %d", nentries);

	if (nentries == 0) {

		/* All entries in FMP property are already mounted */

		return (0);
	}

	/*
	 * Sort the mount entries based on number of components of the
	 * real path.
	 */

	qsort((void *)mntlist, nentries, sizeof (fsentry_t *), svc_mntcompar);

	/*
	 * Return in case of any mount failure. Otherwise check and wait till
	 * every valid file system entry is in MNTTAB file.
	 */

	if (svc_doParMount(mntlist, nentries)) {

		/*
		 * Error message is already logged.
		 */

		return (1);
	}

	/*
	 * Confirm the mount status by checking MNTTAB file.
	 * NOTE: Confirmation of mount with MNTTAB is must for scalable RG,
	 * since it is the only way to know if a file system is mounted or
	 * not(see also the comments in svc_doMount()).
	 *
	 */
	tmplist = (fsentry_t **)malloc(sizeof (fsentry_t *) * (nentries+1));

	if (tmplist == NULL) {

		hasc_scds_syslog_0(LOG_ERR, "Failed to allocate memory.");

		return (1);

	}

	validmounts = (fsentry_t **)malloc(sizeof (fsentry_t *) * (nentries+1));

	if (validmounts == NULL) {

		hasc_scds_syslog_0(LOG_ERR, "Failed to allocate memory.");

		return (1);

	}

	for (i = 0; mntlist[i]; i++) {

		validmounts[i] = mntlist[i];

	}

	validmounts[i] = NULL;

	/*
	 * Check and wait till every valid file system has an entry in MNTTAB.
	 */

	do {
		for (i = 0, count = 0; validmounts[i]; i++) {

			rewind(mfp);

			while ((mp = getmntent(mfp)) != NULL) {
				if (strcmp(validmounts[i]->rpath,
				    mp->mnt_dir) == 0)
					break;
			};

			if (mp != NULL) {

				hasc_scds_syslog_1(LOG_INFO,
				    "%s is confirmed as mounted.",
				    validmounts[i]->rpath);

				validmounts[i]->status |= FS_MOUNTCONFIRM;

			} else {

				tmplist[count++] = validmounts[i];
			}
		}

		if (count == 0) {

			/*
			 * All file systems are confirmed as mounted.
			 */

			break;
		}

		tmplist[count] = NULL;

		tmpvalidmounts = validmounts;

		validmounts = tmplist;

		tmplist = tmpvalidmounts;

		(void) sleep(5);

	} while (1);


	(void) endmntent(mfp);

	hasc_scds_syslog_0(LOG_INFO,
	    "Mounting the file systems is completed successfully.");

	/*
	 * It is not required to free memory as the process is going away
	 * after returning from here.
	 */

	return (0);
}

/*
 *
 * Function: svc_doParMount
 *
 * Arguments:
 *
 * mntlist	The list of mount entries which has to be mounted.
 *
 * nentries	The number of entries to be mounted.
 *
 * Return:
 *
 * 0		if no entries to mount, or
 *		if all file systems specified in mntlist were mounted
 *		successfully
 *
 * 1		if an error occurred.
 *
 * Comments:
 * This routine mounts the file systems in parallel similar to Solaris mount -a.
 *
 * LINUX version
 */

static int
svc_doParMount(fsentry_t **mntlist, unsigned int nentries)
{

	fsentry_t	**mntpp,
			**tmntpp,
			*mntprev,
			*mntcur; /* Pointers to pass through mount entries */
	int		mount_failed = 0;
	int		i;
	hasc_strbuf_t	mount_failure_msg;
	int rc;

	/*
	 * Return if no file systems to mount.
	 */

	if (nentries == 0) {
		return (0);
	}

	/*
	 * The following mount logic is taken from mount command code
	 * (mount.c)
	 */

	for (mntpp = mntlist, mntprev = *mntlist; ((mntcur = *mntpp) != NULL);
	    mntprev = mntcur, mntpp++, nentries--) {

		/*
		 * Check to see if we cross a mount level: e.g.,
		 * /a/b -> /a/b/c. Is so wait for current mounts to finish,
		 * rerun realpath on the remaining mount points and re-sort
		 * the list.
		 */

		if (mntcur->depth > mntprev->depth) {

			/*
			 * Wait for the threads which are delegated for
			 * mounting since there is possibility for dependency
			 * due to nested mount points in earlier level.
			 */

			for (i = 0; mntlist[i]; i++) {
				if (mntlist[i]->tid != 0) {
					(void) sleep(1);
					i = 0;
					continue;
				}
			}

			for (tmntpp = mntpp; *tmntpp; tmntpp++) {

				/*
				 * There is chance for changes in real
				 * path due to existence of symbolic links.
				 * Recalculate the number of components
				 * for mount point's changed real path.
				 */

				svc_setrpath(*tmntpp);

				(*tmntpp)->depth =
				    svc_getpathdepth((*tmntpp)->rpath);
			}

			/*
			 * Sort the remaining mount entries.
			 */

			qsort((void *) mntpp, nentries, sizeof (fsentry_t *),
				svc_mntcompar);

			mntcur = *mntpp;
		}

		/*
		 * Check whether the mount point is a valid file name.
		 */

		if (mntcur->status & FS_RPFAILED) {

			hasc_scds_syslog_1(LOG_ERR,
			    "Nonexistent mount point: %s.",
			    mntcur->vp->fs_file);

			mount_failed = 1;

			continue;
		}

		/*
		 * Create a thread to perform the mount
		 */
		if (rc = pthread_create(&mntcur->tid, NULL,
		    &svc_doMount, (void *) mntcur)) {
			hasc_scds_syslog_1(LOG_ERR,
			    "Unable to perform the mount"
			    "operation: %s.",
			    mntcur->vp->fs_file);

			mount_failed = 1;

			continue;
		}

	} /* End of mount "for" loop */

	/*
	 * Wait for mount tasks completion.
	 */


	for (i = 0; mntlist[i]; i++) {
		if (mntlist[i]->tid != 0) {
			sleep(1);
			i = 0;
			continue;
		}
	}
	/*
	 * Check mount points status.
	 */

	for (i = 0; mntlist[i]; i++) {
		if (mntlist[i]->status & FS_MOUNTFAILED) {
			mount_failed = 1;
			break;
		}
	}

	if (mntlist[i]) {

		/*
		 * Some file systems are failed to mount.
		 * Log the error message and also update status message.
		 */

		mount_failure_msg.str = NULL;

		ADD_STR2STRBUF(mount_failure_msg, "Failed to mount:");

		for (; mntlist[i]; i++) {

			if (mntlist[i]->status & FS_MOUNTFAILED) {

				ADD_STR2STRBUF(mount_failure_msg,
				    mntlist[i]->vp->fs_file);
				ADD_STR2STRBUF(mount_failure_msg, " ");
			}
		}

		/*
		 * Add to status message failing mount points.
		 * Message will be of the form : (On one line only)
		 *
		 * -- Failed to mount: m1 m2 [...].
		 *
		 * where m1 m2 stands for the mount points.
		 * NOTE:
		 * The above similar format is applicable for fsck/umount
		 * failures.
		 */

		ADD_TO_STATUS_MSG_IF_NOT_EMPTY(" -- ");
		ADD_TO_STATUS_MSG(mount_failure_msg.str);

		hasc_scds_syslog_0(LOG_ERR, mount_failure_msg.str);
	}

	return (mount_failed);
}

/*
 * Function: svc_umountFilesystems
 *
 * Arguments:
 * umnts_array	The array of mount points which has to be unmounted.
 *
 * Return:
 *
 * 0		if no entries to unmount or
 *		if all mount points in list are unmounted successfully
 *
 * 1		if an error occurred.
 *
 * Comments:
 * The mount points which are specified in umount array are unmounted.
 *
 * LINUX version
 *
 */


int
svc_umountFilesystems(scha_str_array_t  *umnts_array)
{
	fsentry_t	**umntlist; /* Contains the list of umount entries */
	uint_t		nentries;	/* Number of file systems to unmount */
	int		umount_failed = 0;	/* unmount status */
	int		rc;
	uint_t		count, i;
	FILE		*mfp = NULL;
	struct mntent	*mp;
	char		buffer[1024];
	hasc_strbuf_t	umntpts_string;
	fsentry_t	**tmplist, **localmounts, **tmplocalmounts;
	int		tries;
	hasc_strbuf_t	umount_failure_msg;

	/*
	 * Return if no entry in the umount array.
	 */

	scds_syslog_debug(DBG_LEVEL_HIGH, "No.of entries to unmount %d",
	    umnts_array->array_cnt);

	if (umnts_array->array_cnt == 0)
		return (0);

	/*
	 * Open the MNTTAB file used to check if a file system is unmounted
	 * already or not.
	 * NOTE:
	 * While unmouting it is not required to bother about the mount
	 * point entry existence in MNTTAB.
	 */

	if ((mfp = setmntent(_PATH_MOUNTED, "r")) == NULL) {

		rc = errno;

		hasc_scds_syslog_2(LOG_ERR, "Failed to open %s: %s.",
		    _PATH_MOUNTED,		/* CSTYLED */
		    strerror(rc));	/*lint !e666 */

		return (1);
	}

	/*
	 * Allocate space for maximum possible unmount entries in the list.
	 */

	umntlist = (fsentry_t **)calloc((umnts_array->array_cnt +1),
	    sizeof (fsentry_t *));

	if (umntlist == NULL) {

		hasc_scds_syslog_0(LOG_ERR, "Failed to allocate memory.");
		return (1);
	}

	for (i = 0, count = 0; i < umnts_array->array_cnt; i++) {

		if (umntlist[count] == NULL) {

			umntlist[count] =
			    (fsentry_t *)malloc(sizeof (fsentry_t));

			if (umntlist[count] == NULL) {

				hasc_scds_syslog_0(LOG_ERR,
				    "Failed to allocate memory.");

				return (1);

			}

			umntlist[count]->rpath = NULL;

		}

		umntlist[count]->mountp = umnts_array->str_array[i];

		svc_setrpath(umntlist[count]);

		/*
		 * Rewind the MNTTAB file pointer, so that the next mount point
		 * entry search starts from the beginning.
		 */

		rewind(mfp);

		/*
		 * Check the file system mount point existence in MNTTAB, using
		 * the real path.
		 */

		while ((mp = getmntent(mfp)) != NULL) {
			if (strcmp(umntlist[count]->rpath,
			    mp->mnt_dir) == 0)
				break;
		};

		if (mp == NULL) {

			/*
			 * Reached end of file i.e file system is found to be
			 * already unmounted. Log a informational message and
			 * continue. Returning an error here would make the
			 * stop method non-idempotent.
			 */

			hasc_scds_syslog_1(LOG_INFO,
			    "%s is erroneously found to be "
			    "unmounted.", umntlist[count]->rpath);

			continue;

		} else {
			/*
			 * Mount point exists in MNTTAB (file system is mounted)
			 * HAStorageClient unmounts only local file systems.
			 */

			rc = svc_evalMntEntMountOption(mp, buffer);

			if (rc == SvcMountOptLocalNFS) {

				/*
				 * Add this to umount list.
				 * NOTE: Incrementing count leaves the current
				 * umount point entry in umount list and
				 * points to next empty slot.
				 */

				count++;

				hasc_scds_syslog_1(LOG_INFO,
				    "%s is locally mounted. Unmounting it.",
				    mp->mnt_dir);

			} else {

				/*
				 * Not a local mount point of a NFS file
				 * system. Log a warning
				 * and proceed to umount remaining entries.
				 */
				hasc_scds_syslog_1(LOG_WARNING,
				    "%s is not a local mount point "
				    "in /etc/mtab", mp->mnt_dir);

			}
		}
	}

	/*
	 * Mark the end position for the list of umount entries.
	 */

	umntlist[count] = NULL;

	nentries = count;

	/*
	 * Return if the number of entries is zero.
	 */

	scds_syslog_debug(DBG_LEVEL_HIGH, "The no.of entries in unmount list "
		" %d", nentries);

	if (nentries == 0) {

		/* All mount entries are already mounted */

		return (0);
	}

	umntpts_string.str = NULL;

	for (i = 0; umntlist[i]; i++) {

		ADD_STR2STRBUF(umntpts_string, umntlist[i]->mountp);
		ADD_STR2STRBUF(umntpts_string, " ");
	}

	/*
	 * Unmount the file systems in the umntlist list.
	 */

	(void) svc_doParUmount(umntpts_string.str);

	/*
	 * Confirm the umount status by checking MNTTAB file.
	 */

	tmplist = (fsentry_t **)malloc(sizeof (fsentry_t *) * (nentries+1));

	if (tmplist == NULL) {

		hasc_scds_syslog_0(LOG_ERR, "Failed to allocate memory.");

		return (1);
	}

	localmounts = (fsentry_t **)malloc(sizeof (fsentry_t *) * (nentries+1));

	if (localmounts == NULL) {

		hasc_scds_syslog_0(LOG_ERR, "Failed to allocate memory.");

		return (1);

	}

	for (i = 0; umntlist[i]; i++) {

			localmounts[i] = umntlist[i];
	}



	localmounts[i] = NULL;

	tries = 0;

	do {
		for (i = 0, count = 0; localmounts[i]; i++) {
			(void) endmntent(mfp);
			/*
			 * Open the MNTTAB file used to check if a
			 * file system is still mounted or not.
			 */

			if ((mfp =
			    setmntent(_PATH_MOUNTED, "r")) == NULL) {

				rc = errno;

				hasc_scds_syslog_2(LOG_ERR,
				    "Failed to open %s: %s.",
				    _PATH_MOUNTED,		/* CSTYLED */
				    strerror(rc));	/*lint !e666 */

				return (1);
			}

			while ((mp = getmntent(mfp)) != NULL) {
				hasc_scds_syslog_2(LOG_INFO,
				    "Mount %s %s",
				    localmounts[i]->rpath,
				    mp->mnt_dir);
				if (strcmp(localmounts[i]->rpath,
				    mp->mnt_dir) == 0)
					break;
			};

			if (mp == NULL) {

				hasc_scds_syslog_1(LOG_INFO,
				    "%s is confirmed as unmounted.",
				    localmounts[i]->rpath);

				localmounts[i]->status |= FS_UMOUNTCONFIRM;

			} else {

				tmplist[count++] = localmounts[i];
			}
		}

		if (count == 0) {

			/*
			 * All file systems are confirmed as unmounted.
			 */

			break;
		}

		tmplist[count] = NULL;

		tmplocalmounts = localmounts;

		localmounts = tmplist;

		tmplist = tmplocalmounts;

		/*
		 * If umount failed, we cannot loop forever if we are doing an
		 * Update. Let's sleep 3 times at most (i.e. check 4 times).
		 */

		if ((svcMethodId == HASTORAGECLIENT_UPDATE) && (tries == 3)) {

			break;
		}

		tries++;

		(void) sleep(5);

	} while (1);

	/*
	 * Check umount status of the file systems.
	 */

	for (i = 0; umntlist[i]; i++) {

		if (!(umntlist[i]->status & FS_UMOUNTCONFIRM)) {

			umount_failed = 1;

			break;
		}
	}

	if (umntlist[i]) {

		/*
		 * Some file systems are failed to unmount.
		 * Log the error message and also update status message.
		 */

		umount_failure_msg.str = NULL;

		ADD_STR2STRBUF(umount_failure_msg, "Failed to unmount :");

		for (; umntlist[i]; i++) {

			if (!(umntlist[i]->status & FS_UMOUNTCONFIRM)) {

				ADD_STR2STRBUF(umount_failure_msg,
				    umntlist[i]->mountp);
				ADD_STR2STRBUF(umount_failure_msg, " ");
			}
		}

		ADD_TO_STATUS_MSG_IF_NOT_EMPTY(" -- ");
		ADD_TO_STATUS_MSG(umount_failure_msg.str);

		hasc_scds_syslog_0(LOG_ERR, umount_failure_msg.str);
	}

	return (umount_failed);
}

#else /* !linux i.e. solaris */

/*
 *
 * Function: svc_mountFilesystems
 *
 * Arguments: none
 *
 * Return:
 *
 * 0		if no entries to mount, or
 *		if all file systems specified in FilesystemMountPoints
 *		property were mounted successfully
 *
 * 1		if an error occurred.
 *
 * Comments:
 * The mount points that are specified in "FilesystemMountPoints" extension
 * property are mounted if they are not mounted already (i.e no entry in
 * MNTTAB file).
 *
 */

int
svc_mountFilesystems()
{
	fsentry_t	**mntlist; /* Contains the list of mount entries */
	uint_t		nentries;	/* Number of entries to mount */
	struct mnttab	mp, mref;
	FILE		*mfp = NULL;
	int		rc;
	uint_t		count, i;
	fsentry_t	**validmounts, **tmplist, **tmpvalidmounts;

	/*
	 * Examine the number of FMP entries and return if no entry exist.
	 */
	if (gSvcInfo.count == 0)
		return (0);

	/*
	 * Open the MNTTAB file used to check if a file system is
	 * mounted or not.
	 * NOTE:
	 * Checking the mount entry in VFSTAB is not done here (again) as it
	 * is done during validation (in svc_validate).
	 */

	if ((mfp = fopen(MNTTAB, "r")) == NULL) {

		rc = errno;

		hasc_scds_syslog_2(LOG_ERR,
		    "Failed to open %s: %s.",
		    VFSTAB,		/* CSTYLED */
		    strerror(rc));	/*lint !e666 */

		return (1);
	}

	/*
	 * Allocate space for maximum possible mount entries in the list.
	 */

	mntlist = (fsentry_t **)
	    calloc((gSvcInfo.count +1), sizeof (fsentry_t *));

	if (mntlist == NULL) {

		hasc_scds_syslog_0(LOG_ERR, "Failed to allocate memory.");

		return (1);
	}

	/*
	 * Nullify the elements in the mnttab structure.
	 */

	(void) memset(&mref, 0, sizeof (struct mnttab));

	for (i = 0, count = 0; i < gSvcInfo.count; i++) {

		if (mntlist[count]  == NULL) {

			mntlist[count] =
			    (fsentry_t *)malloc(sizeof (fsentry_t));

			if (mntlist[count]  == NULL) {

				hasc_scds_syslog_0(LOG_ERR,
				    "Failed to allocate memory.");

				return (1);

			}

			mntlist[count]->rpath = NULL;
		}

		mntlist[count]->vp = &gSvcInfo.list[i].vfsEntry;
		mntlist[count]->mountp = gSvcInfo.list[i].vfsEntry.vfs_mountp;

		svc_setrpath(mntlist[count]);

		mntlist[count]->depth =
		    svc_getpathdepth(mntlist[count]->rpath);

		/*
		 * Rewind the MNTTAB file pointer so that the next mount point
		 * entry search starts from the beginning.
		 */

		rewind(mfp);

		/*
		 * Mount point entry existence in MNTTAB file has to be checked
		 * with realpath of mount point.
		 */

		mref.mnt_mountp = mntlist[count]->rpath;

		rc = getmntany(mfp, &mp, &mref);

		if (rc == 0) {

			/*
			 * File system is already mounted. Skip it.
			 */

			scds_syslog_debug(DBG_LEVEL_HIGH,
			    "File system associated with %s is already mounted",
			    mref.mnt_mountp);

			continue;

		} else if (rc > 0) {

			/*
			 * Unable to read mount point from MNTTAB.
			 */

			hasc_scds_syslog_2(LOG_ERR, "Failed to read %s: %s.",
			    MNTTAB,		/* CSTYLED */
			    strerror(rc));	/*lint !e666 */

			return (1);
		}


		/*
		 * Mount point is not in MNTTAB file. So keep it in mount list.
		 * NOTE: Incrementing count leaves the current mount point
		 * entry in mount list and points to next empty slot.
		 */

		count++;

	}

	/*
	 * Mark the end position to the list of mount entries.
	 * Now the mntlist contains the list of entries which has to be
	 * mounted.
	 */

	mntlist[count] = NULL;

	nentries = count;

	scds_syslog_debug(DBG_LEVEL_HIGH, "The number of entries to mount "
	    "are: %d", nentries);

	if (nentries == 0) {

		/* All entries in FMP property are already mounted */

		return (0);
	}

	/*
	 * Sort the mount entries based on number of components of the
	 * real path.
	 */

	qsort((void *)mntlist, nentries, sizeof (fsentry_t *), svc_mntcompar);

	/*
	 * Return in case of any mount failure. Otherwise check and wait till
	 * every valid file system entry is in MNTTAB file.
	 */

	if (svc_doParMount(mntlist, nentries)) {

		/*
		 * Error message is already logged.
		 */

		return (1);
	}

	/*
	 * Confirm the mount status by checking MNTTAB file.
	 * NOTE: Confirmation of mount with MNTTAB is must for scalable RG,
	 * since it is the only way to know if a file system is mounted or
	 * not(see also the comments in svc_doMount()).
	 *
	 */
	tmplist = (fsentry_t **)malloc(sizeof (fsentry_t *) * (nentries+1));

	if (tmplist == NULL) {

		hasc_scds_syslog_0(LOG_ERR, "Failed to allocate memory.");

		return (1);

	}

	validmounts = (fsentry_t **)malloc(sizeof (fsentry_t *) * (nentries+1));

	if (validmounts == NULL) {

		hasc_scds_syslog_0(LOG_ERR, "Failed to allocate memory.");

		return (1);

	}

	for (i = 0; mntlist[i]; i++) {

		validmounts[i] = mntlist[i];

	}

	validmounts[i] = NULL;

	/*
	 * Check and wait till every valid file system has an entry in MNTTAB.
	 */

	do {
		for (i = 0, count = 0; validmounts[i]; i++) {

			mref.mnt_mountp = validmounts[i]->rpath;

			rewind(mfp);

			rc = getmntany(mfp, &mp, &mref);

			if (rc == 0) {

				hasc_scds_syslog_1(LOG_INFO,
				    "%s is confirmed as mounted.",
				    validmounts[i]->rpath);

				validmounts[i]->status |= FS_MOUNTCONFIRM;

			} else {

				tmplist[count++] = validmounts[i];
			}
		}

		if (count == 0) {

			/*
			 * All file systems are confirmed as mounted.
			 */

			break;
		}

		tmplist[count] = NULL;

		tmpvalidmounts = validmounts;

		validmounts = tmplist;

		tmplist = tmpvalidmounts;

		(void) sleep(5);

	} while (1);


	hasc_scds_syslog_0(LOG_INFO,
	    "Mounting the file systems is completed successfully.");

	/*
	 * It is not required to free memory as the process is going away
	 * after returning from here.
	 */

	return (0);
}

/*
 *
 * Function: svc_doParMount
 *
 * Arguments:
 *
 * mntlist	The list of mount entries which has to be mounted.
 *
 * nentries	The number of entries to be mounted.
 *
 * Return:
 *
 * 0		if no entries to mount, or
 *		if all file systems specified in mntlist were mounted
 *		successfully
 *
 * 1		if an error occurred.
 *
 * Comments:
 * This routine mounts the file systems in parallel similar to Solaris mount -a.
 */

static int
svc_doParMount(fsentry_t **mntlist, unsigned int nentries)
{

	fsentry_t	**mntpp,
			**tmntpp,
			*mntprev,
			*mntcur; /* Pointers to pass through mount entries */
	int		mount_failed = 0;
	int		i;
	hasc_strbuf_t	mount_failure_msg;
	int rc;

	/*
	 * Return if no file systems to mount.
	 */

	if (nentries == 0) {
		return (0);
	}

	/*
	 * The following mount logic is taken from mount command code
	 * (mount.c)
	 */

	for (mntpp = mntlist, mntprev = *mntlist; ((mntcur = *mntpp) != NULL);
	    mntprev = mntcur, mntpp++, nentries--) {

		/*
		 * Check to see if we cross a mount level: e.g.,
		 * /a/b -> /a/b/c. Is so wait for current mounts to finish,
		 * rerun realpath on the remaining mount points and re-sort
		 * the list.
		 */

		if (mntcur->depth > mntprev->depth) {

			/*
			 * Wait for the threads which are delegated for
			 * mounting since there is possibility for dependency
			 * due to nested mount points in earlier level.
			 */

			for (i = 0; mntlist[i]; i++) {
				if (mntlist[i]->tid != 0) {
					(void) sleep(1);
					i = 0;
					continue;
				}
			}

			for (tmntpp = mntpp; *tmntpp; tmntpp++) {

				/*
				 * There is chance for changes in real
				 * path due to existence of symbolic links.
				 * Recalculate the number of components
				 * for mount point's changed real path.
				 */

				svc_setrpath(*tmntpp);

				(*tmntpp)->depth =
				    svc_getpathdepth((*tmntpp)->rpath);
			}

			/*
			 * Sort the remaining mount entries.
			 */

			qsort((void *) mntpp, nentries, sizeof (fsentry_t *),
				svc_mntcompar);

			mntcur = *mntpp;
		}

		/*
		 * Check whether the mount point is a valid file name.
		 */

		if (mntcur->status & FS_RPFAILED) {

			hasc_scds_syslog_1(LOG_ERR,
			    "Nonexistent mount point: %s.",
			    mntcur->vp->vfs_mountp);

			mount_failed = 1;

			continue;
		}

		/*
		 * Create a thread to perform the mount
		 */
		if ((rc = pthread_create(&mntcur->tid, NULL,
		    svc_doMount, (void *) mntcur)) != 0) {
			hasc_scds_syslog_2(LOG_ERR,
			    "Unable to perform the mount"
			    "operation: %s error %d.",
			    mntcur->vp->vfs_mountp, rc);

			mount_failed = 1;

			continue;
		}

	} /* End of mount "for" loop */

	/*
	 * Wait for mount tasks completion.
	 */


	for (i = 0; mntlist[i]; i++) {
		if (mntlist[i]->tid != 0) {
			(void) sleep(1);
			i = 0;
			continue;
		}
	}
	/*
	 * Check mount points status.
	 */

	for (i = 0; mntlist[i]; i++) {
		if (mntlist[i]->status & FS_MOUNTFAILED) {
			mount_failed = 1;
			break;
		}
	}

	if (mntlist[i]) {

		/*
		 * Some file systems are failed to mount.
		 * Log the error message and also update status message.
		 */

		mount_failure_msg.str = NULL;

		ADD_STR2STRBUF(mount_failure_msg, "Failed to mount:");

		for (; mntlist[i]; i++) {

			if (mntlist[i]->status & FS_MOUNTFAILED) {

				ADD_STR2STRBUF(mount_failure_msg,
				    mntlist[i]->vp->vfs_mountp);
				ADD_STR2STRBUF(mount_failure_msg, " ");
			}
		}

		/*
		 * Add to status message failing mount points.
		 * Message will be of the form : (On one line only)
		 *
		 * -- Failed to mount: m1 m2 [...].
		 *
		 * where m1 m2 stands for the mount points.
		 * NOTE:
		 * The above similar format is applicable for fsck/umount
		 * failures.
		 */

		ADD_TO_STATUS_MSG_IF_NOT_EMPTY(" -- ");
		ADD_TO_STATUS_MSG(mount_failure_msg.str);

		hasc_scds_syslog_0(LOG_ERR, mount_failure_msg.str);
	}

	return (mount_failed);
}

/*
 * Function: svc_umountFilesystems
 *
 * Arguments:
 * umnts_array	The array of mount points which has to be unmounted.
 *
 * Return:
 *
 * 0		if no entries to unmount or
 *		if all mount points in list are unmounted successfully
 *
 * 1		if an error occurred.
 *
 * Comments:
 * The mount points which are specified in umount array are unmounted.
 */


int
svc_umountFilesystems(scha_str_array_t  *umnts_array)
{
	fsentry_t	**umntlist; /* Contains the list of umount entries */
	uint_t		nentries;	/* Number of file systems to unmount */
	int		umount_failed = 0;	/* unmount status */
	int		rc;
	uint_t		count, i;
	FILE		*mfp = NULL;
	struct mnttab	mp, mref;
	char		buffer[1024];
	hasc_strbuf_t	umntpts_string;
	fsentry_t	**tmplist, **localmounts, **tmplocalmounts;
	int		tries;
	hasc_strbuf_t	umount_failure_msg;

	/*
	 * Return if no entry in the umount array.
	 */

	scds_syslog_debug(DBG_LEVEL_HIGH, "No.of entries to unmount %d",
	    umnts_array->array_cnt);

	if (umnts_array->array_cnt == 0)
		return (0);

	/*
	 * Open the MNTTAB file used to check if a file system is unmounted
	 * already or not.
	 * NOTE:
	 * While unmouting it is not required to bother about the mount
	 * point entry existence in VFSTAB.
	 */

	if ((mfp = fopen(MNTTAB, "r")) == NULL) {

		rc = errno;

		hasc_scds_syslog_2(LOG_ERR, "Failed to open %s: %s.",
		    MNTTAB,		/* CSTYLED */
		    strerror(rc));	/*lint !e666 */

		return (1);
	}

	/*
	 * Allocate space for maximum possible unmount entries in the list.
	 */

	umntlist = (fsentry_t **)calloc((umnts_array->array_cnt +1),
	    sizeof (fsentry_t *));

	if (umntlist == NULL) {

		hasc_scds_syslog_0(LOG_ERR, "Failed to allocate memory.");
		return (1);
	}

	/*
	 * Nullify the elements in the mnttab structure.
	 */

	(void) memset(&mref, 0, sizeof (struct mnttab));

	for (i = 0, count = 0; i < umnts_array->array_cnt; i++) {

		if (umntlist[count] == NULL) {

			umntlist[count] =
			    (fsentry_t *)malloc(sizeof (fsentry_t));

			if (umntlist[count] == NULL) {

				hasc_scds_syslog_0(LOG_ERR,
				    "Failed to allocate memory.");

				return (1);

			}

			umntlist[count]->rpath = NULL;

		}

		umntlist[count]->mountp = umnts_array->str_array[i];

		svc_setrpath(umntlist[count]);

		/*
		 * Rewind the MNTTAB file pointer, so that the next mount point
		 * entry search starts from the beginning.
		 */

		rewind(mfp);

		/*
		 * Check the file system mount point existence in MNTTAB, using
		 * the real path.
		 */

		mref.mnt_mountp = umntlist[count]->rpath;
		rc = getmntany(mfp, &mp, &mref);

		if (rc == -1) {

			/*
			 * Reached end of file i.e file system is found to be
			 * already unmounted. Log a informational message and
			 * continue. Returning an error here would make the
			 * stop method non-idempotent.
			 */

			hasc_scds_syslog_1(LOG_INFO,
			    "%s is erroneously found to be "
			    "unmounted.", mref.mnt_mountp);

			continue;

		} else if (rc == 0) {

			/*
			 * Mount point exists in MNTTAB (file system is mounted)
			 * HAStorageClient unmounts only local file systems.
			 */

			rc = svc_evalMntTabMountOption(&mp, buffer);

			if (rc == SvcMountOptLocalNFS) {

				/*
				 * Add this to umount list.
				 * NOTE: Incrementing count leaves the current
				 * umount point entry in umount list and
				 * points to next empty slot.
				 */

				count++;

				hasc_scds_syslog_1(LOG_INFO,
				    "%s is locally mounted. Unmounting it.",
				    mref.mnt_mountp);

			} else {

				/*
				 * Not a local mount point of a NFS file
				 * system. Log a warning
				 * and proceed to umount remaining entries.
				 */

				hasc_scds_syslog_1(LOG_WARNING,
				    "%s is not a local mount point"
				    "of an NFS mounted"
				    "file system in /etc/mnttab",
				    mref.mnt_mountp);
			}
		} else {

			/*
			 * Unable to read mount point from MNTTAB.
			 */

			rc = errno;

			hasc_scds_syslog_2(LOG_ERR,
			    "Failed to read %s: %s.",
			    MNTTAB,		/* CSTYLED */
			    strerror(rc));	/*lint !e666 */

			return (1);
		}
	}

	/*
	 * Mark the end position for the list of umount entries.
	 */

	umntlist[count] = NULL;

	nentries = count;

	/*
	 * Return if the number of entries is zero.
	 */

	scds_syslog_debug(DBG_LEVEL_HIGH, "The no.of entries in unmount list "
		" %d", nentries);

	if (nentries == 0) {

		/* All mount entries are already mounted */

		return (0);
	}

	umntpts_string.str = NULL;

	for (i = 0; umntlist[i]; i++) {

		ADD_STR2STRBUF(umntpts_string, umntlist[i]->mountp);
		ADD_STR2STRBUF(umntpts_string, " ");
	}

	/*
	 * Unmount the file systems in the umntlist list.
	 */

	(void) svc_doParUmount(umntpts_string.str);

	/*
	 * Confirm the umount status by checking MNTTAB file.
	 */

	tmplist = (fsentry_t **)malloc(sizeof (fsentry_t *) * (nentries+1));

	if (tmplist == NULL) {

		hasc_scds_syslog_0(LOG_ERR, "Failed to allocate memory.");

		return (1);
	}

	localmounts = (fsentry_t **)malloc(sizeof (fsentry_t *) * (nentries+1));

	if (localmounts == NULL) {

		hasc_scds_syslog_0(LOG_ERR, "Failed to allocate memory.");

		return (1);

	}

	for (i = 0; umntlist[i]; i++) {

			localmounts[i] = umntlist[i];
	}

	localmounts[i] = NULL;

	tries = 0;

	do {
		for (i = 0, count = 0; localmounts[i]; i++) {

			(void) memset(&mref, 0, sizeof (struct mnttab));

			mref.mnt_mountp = localmounts[i]->rpath;

			rewind(mfp);

			rc = getmntany(mfp, &mp, &mref);

			if (rc == -1) {

				hasc_scds_syslog_1(LOG_INFO,
				    "%s is confirmed as unmounted.",
				    localmounts[i]->rpath);

				localmounts[i]->status |= FS_UMOUNTCONFIRM;

			} else {

				tmplist[count++] = localmounts[i];
			}
		}

		if (count == 0) {

			/*
			 * All file systems are confirmed as unmounted.
			 */

			break;
		}

		tmplist[count] = NULL;

		tmplocalmounts = localmounts;

		localmounts = tmplist;

		tmplist = tmplocalmounts;

		/*
		 * If umount failed, we cannot loop forever if we are doing an
		 * Update. Let's sleep 3 times at most (i.e. check 4 times).
		 */

		if ((svcMethodId == HASTORAGECLIENT_UPDATE) && (tries == 3)) {

			break;
		}

		tries++;

		(void) sleep(5);

	} while (1);

	/*
	 * Check umount status of the file systems.
	 */

	for (i = 0; umntlist[i]; i++) {

		if (!(umntlist[i]->status & FS_UMOUNTCONFIRM)) {

			umount_failed = 1;

			break;
		}
	}

	if (umntlist[i]) {

		/*
		 * Some file systems are failed to unmout.
		 * Log the error message and also update status message.
		 */

		umount_failure_msg.str = NULL;

		ADD_STR2STRBUF(umount_failure_msg, "Failed to unmount :");

		for (; umntlist[i]; i++) {

			if (!(umntlist[i]->status & FS_UMOUNTCONFIRM)) {

				ADD_STR2STRBUF(umount_failure_msg,
				    umntlist[i]->mountp);
				ADD_STR2STRBUF(umount_failure_msg, " ");
			}
		}

		ADD_TO_STATUS_MSG_IF_NOT_EMPTY(" -- ");
		ADD_TO_STATUS_MSG(umount_failure_msg.str);

		hasc_scds_syslog_0(LOG_ERR, umount_failure_msg.str);
	}

	return (umount_failed);
}
#endif /* !linux */

#if 0
/*
 * Function: svc_doDAM
 *
 * Arguments:
 *
 * arg Which contains the mount entry which has to be monitored
 *     or unmonitored
 *
 * Return:
 *
 *      NONE
 *
 * Comments:
 * This function tries to monitor/unmonitor the file system.
 */

void *
svc_doDAM(void *arg, int action)
{

	fsentry_t	*mnt_entry = (fsentry_t *)arg;
	DIR		*dirp;
	char		*cmd = NULL;
	char		*cmd_output = NULL;
	int		cmd_status;
	int		totalfiles = 0;
	int		rc = 0;
	char		*mnt_point;

#ifdef linux
	mnt_point = mnt_entry->vp->fs_file;
#else
	mnt_point = mnt_entry->vp->vfs_mountp;
#endif

	if (action == DAM_MONITOR) {
		hasc_scds_syslog_1(LOG_INFO,
		    "About to monitor the mount point %s.", mnt_point);
	} else {
		hasc_scds_syslog_1(LOG_INFO,
		    "About to unmonitor the mount point %s.", mnt_point);
	}

	/*
	 * Prepare the command string for damadm.
	 * Eg: damadm -m mnt_point
	 * damadm -u mnt_point
	 */

	if (action == DAM_MONITOR) {
		if (svc_sprintf(&cmd, "%s -m %s", DAMADM, mnt_point)) {
			rc = 1;
			goto out;
		}
	} else {
		if (svc_sprintf(&cmd, "%s -u %s", DAMADM, mnt_point)) {
			rc = 1;
			goto out;
		}
	}

	cmd_status = svc_executeCmdusingFork(cmd, &cmd_output);

	if (action == DAM_MONITOR) {
		if (cmd_status == 0) {
			scds_syslog_debug(LOG_INFO,
			    "Monitoring of %s successful.",
			    mnt_point);
		} else {
			scds_syslog_debug(LOG_INFO,
			    "Monitoring of %s failed: (%d) %s.",
			    mnt_point, cmd_status, cmd_output);
		}
	} else {
		if (cmd_status == 0) {
			scds_syslog_debug(LOG_INFO,
			    "Unonitoring of %s successful.",
			    mnt_point);
		} else {
			scds_syslog_debug(LOG_INFO,
			    "Unonitoring of %s failed: (%d) %s.",
			    mnt_point, cmd_status, cmd_output);
		}
	}

out:
	if (cmd_output)
		free(cmd_output);
	if (cmd)
		free(cmd);
}
#endif

/*
 * Function: svc_doParUmount
 *
 * Arguments:
 *
 * umnt_pts	The mount points of the file systems which has to be unmounted
 *
 * Return:
 *
 * 0		if umount is done successfully
 *
 * 1		if an error occurred.
 *
 * Comments:
 * This function unmounts the file systems with force option.
 */

static int
svc_doParUmount(char *umnt_pts)
{
	char	*cmd = NULL;
	char	*cmd_output = NULL;
	int	rc = 0;
	int	cmd_status;

	/*
	 * The umount error is also redirected to stdout. In case the command
	 * succeeds the output is stored in cmd_output, otherwise it contains
	 * the failure message.
	 */

	/*
	 * Form umount command having umount entries with force and parallel
	 * option.
	 * i.e umount -f -a "file_systems" 2>&1
	 */

	if (svc_sprintf(&cmd, "%s %s 2>&1", UMOUNTFA, umnt_pts)) {
		return (1);
	}

	hasc_scds_syslog_1(LOG_INFO, "About to unmount %s.", umnt_pts);

	cmd_status = svc_executeCmdusingPopen(cmd, &cmd_output);

	if (cmd_status == 0) {

		/*
		 * Unmount succeeded.
		 */
		hasc_scds_syslog_1(LOG_INFO,
		    "Successfully unmounted %s.", umnt_pts);

	} else {

		/*
		 * Unmount command failed.
		 */

		hasc_scds_syslog_3(LOG_ERR, "Failed to unmount %s: (%d) %s.",
		    umnt_pts, cmd_status, cmd_output);
		rc = 1;
	}

	if (cmd)
		free(cmd);
	if (cmd_output)
		free(cmd_output);

	return (rc);
}

static char NEWMNTS_FILE[MAXPATHLEN];
static char OLDMNTS_FILE[MAXPATHLEN];
static char REMOVEDMNTS_FILE[MAXPATHLEN];

/*
 *
 * Function: svc_saveOldMntPts
 *
 * Arguments: none
 *
 * Return:
 *
 * 0		if all the old mount points are saved in temporary file
 *		successfully.
 *
 * 1		if an error occurred.
 *
 * Comments:
 * This routine is used to save the old mount points.
 * When update is done on the HAStorageClient resource,
 * RGM invokes first validate
 * method and invokes update iff validate is success. When validate completes
 * successfully, RGM updates the resource with the new properties by overriding
 * the old properties. Since the old mount points are required (to unmount the
 * FMP which are removed for update), they are stored in temporary file.
 */

int
svc_saveOldMntPts()
{
	scha_resource_t		handle;
	scha_extprop_value_t	*fmp;
	uint_t			fmpcount, i;
	FILE			*fp;
	int			rc = 0;
	scha_err_t		err;

	/*
	 * Get the file name to store old mount points.
	 */

	(void) sprintf(OLDMNTS_FILE,
	    "/var/cluster/run/HAStorageClient_oldmounts.%s.%s",
	    gSvcInfo.rsName, gSvcInfo.rgName);

	fp = fopen(OLDMNTS_FILE, "w");

	if (fp == NULL) {

		rc = errno;

		hasc_scds_syslog_2(LOG_ERR,
		    "Failed to open %s: %s.",
		    OLDMNTS_FILE,	/* CSTYLED */
		    strerror(rc));	/*lint !e666 */

		return (1);
	}

	/*
	 * SCHA API is used instead of scds_get_ext_property() to get old
	 * file system mount points.
	 * REASON::
	 * SCDS API (scds_get_ext_property) first looks in the list of
	 * properties specified in the method argument list which contains the
	 * new arguments and then uses scha API if property is not found.
	 * To avoid reading new arguments scha API is used which gets from
	 * CCR which contains old mount points.
	 */

	err = scha_resource_open(gSvcInfo.rsName, gSvcInfo.rgName, &handle);

	if (err != SCHA_ERR_NOERR) {

		hasc_scds_syslog_1(LOG_ERR,
		    "Failed to retrieve the cluster handle: %s.",
						/* CSTYLED */
		    scds_error_string(err));	/*lint !e666 */

		return (1);
	}

	err = scha_resource_get(handle, SCHA_EXTENSION,
	    FILESYSTEMMOUNTPOINTS, &fmp);

	if (err != SCHA_ERR_NOERR) {

		hasc_scds_syslog_2(LOG_ERR,
		    "Failed to retrieve property %s: %s.",
		    FILESYSTEMMOUNTPOINTS,	/* CSTYLED */
		    scds_error_string(err));	/*lint !e666 */

		rc = 1;

		goto out;
	}

	fmpcount = fmp->val.val_strarray->array_cnt;

	for (i = 0; i < fmpcount; i++) {

		if ((fprintf(fp, "%s\n",
		    fmp->val.val_strarray->str_array[i])) < 0) {

			/* This shouldn't happen */

			rc = errno;
			hasc_scds_syslog_2(LOG_ERR,
			    "Failed to write %s: %s.",
			    OLDMNTS_FILE,	/* CSTYLED */
			    strerror(rc));	/*lint !e666 */
			rc = 1;
			goto out;
		}
	}

out:
	(void) scha_resource_close(handle);
	(void) fclose(fp);
	return (rc);
}


/*
 *
 * Function: svc_umountRemovedMntPts
 *
 * Arguments: none
 *
 * Return:
 *
 * 0		if all the removed mount points are unmounted.
 *
 * 1		if an error occurred.
 *
 * Comments:
 * This routine is used to unmount the file systems which are removed
 * during update operation.
 * This routine prepares the list of file systems to be unmounted and calls
 * svc_umountFilesystems().
 *
 */

int
svc_umountRemovedMntPts()
{

	FILE			*fp;
	int			rc = 0;
	char			*cmd = NULL;
	char			*cmd_output = NULL;
	int			cmd_status;
	scha_str_array_t	umntpts_array;
	char			mnt_point[VFS_LINE_MAX];
	scha_resource_t		handle;
	scha_extprop_value_t	*fmp;
	scha_err_t		err;
	uint_t			fmpcount, i;

	/*
	 * Save the new mount points in temporary file.(The new mount points
	 * are stored by HASClient already)
	 */

	(void) sprintf(NEWMNTS_FILE,
	    "/var/cluster/run/HAStorageClient_newmounts.%s.%s",
	    gSvcInfo.rsName, gSvcInfo.rgName);

	fp = fopen(NEWMNTS_FILE, "w");

	if (fp == NULL) {

		rc = errno;
		hasc_scds_syslog_2(LOG_ERR,
		    "Failed to open %s: %s.",
		    NEWMNTS_FILE,	/* CSTYLED */
		    strerror(rc));	/*lint !e666 */

		return (1);
	}

	/*
	 * Get the FileSystemMountPoints.
	 */

	err = scha_resource_open(gSvcInfo.rsName, gSvcInfo.rgName, &handle);

	if (err != SCHA_ERR_NOERR) {

		hasc_scds_syslog_1(LOG_ERR,
		    "Failed to retrieve the cluster handle: %s.",
						/* CSTYLED */
		    scds_error_string(err));	/*lint !e666 */

		return (1);
	}

	err = scha_resource_get(handle, SCHA_EXTENSION,
	    FILESYSTEMMOUNTPOINTS, &fmp);

	if (err != SCHA_ERR_NOERR) {

		hasc_scds_syslog_2(LOG_ERR,
		    "Failed to retrieve property %s: %s.",
		    FILESYSTEMMOUNTPOINTS,	/* CSTYLED */
		    scds_error_string(err));	/*lint !e666 */

		rc = 1;
		goto out;
	}

	fmpcount = fmp->val.val_strarray->array_cnt;

	for (i = 0; i < fmpcount; i++) {

		if ((fprintf(fp, "%s\n",
			fmp->val.val_strarray->str_array[i])) < 0) {

			rc = errno;
			/* This shouldn't happen */
			hasc_scds_syslog_2(LOG_ERR,
			    "Failed to write %s: %s.",
			    NEWMNTS_FILE,	/* CSTYLED */
			    strerror(rc));	/*lint !e666 */
			rc = 1;
			goto out;
		}
	}

	(void) fclose(fp);

	/*
	 * Get the file names used to save the old mount points and removed
	 * mount points.
	 */

	(void) sprintf(OLDMNTS_FILE,
	    "/var/cluster/run/HAStorageClient_oldmounts.%s.%s",
	    gSvcInfo.rsName, gSvcInfo.rgName);

	(void) sprintf(REMOVEDMNTS_FILE,
	    "/var/cluster/run/HAStorageClient_removedmounts.%s.%s",
	    gSvcInfo.rsName, gSvcInfo.rgName);

	/*
	 * Find the mount points which are being removed for update
	 * by the following command ...
	 * DIFF OLDMNTS_FILE NEWMNTS_FILE | grep '^<' | %s -f2 -d ' ' >
	 * REMOVEDMNTS_FILE
	 */

	if (svc_sprintf(&cmd, "%s %s %s | grep '^<' | %s -f2 -d ' ' > %s",
	    DIFF, OLDMNTS_FILE, NEWMNTS_FILE, CUT, REMOVEDMNTS_FILE)) {

		rc = 1;
		goto out;
	}

	cmd_status = svc_executeCmdusingPopen(cmd, &cmd_output);

	if (cmd_status) {

		/*
		 * Command failed.
		 */

		hasc_scds_syslog_2(LOG_ERR, "Failed to find the "
		    "removed mount points: (%d) %s.", rc, cmd_output);
		rc = 1;
		goto out;

	} else {

		/*
		 * Command completed successfully.
		 * The removed mount points are in cmd_output. Prepare the list
		 * which has be unmounted and pass to svc_umountFilesystems().
		 */

		fp = fopen(REMOVEDMNTS_FILE, "r");

		if (fp == NULL) {

			rc = errno;
			hasc_scds_syslog_2(LOG_ERR,
			    "Failed to open %s: %s.",
			    REMOVEDMNTS_FILE,	/* CSTYLED */
			    strerror(rc));	/*lint !e666 */

			return (1);
		}

		/*
		 * Nullify the default values in the umntpts_array.
		 */

		umntpts_array.array_cnt = 0;
		umntpts_array.str_array = NULL;

		while (fgets(mnt_point, VFS_LINE_MAX, fp)) {

			/* Strip the trailing '\n' character(s) */

			for (i = strlen(mnt_point)-1; i > 0; i--) {
				if ((mnt_point[i]) == '\n')
					mnt_point[i] = '\0';
				else
					break;
			}

			if (svc_addStr2Array(mnt_point, &umntpts_array)) {
				rc = 1;
				goto out;
			}
		}

		(void) fclose(fp);

		if (svc_umountFilesystems(&umntpts_array)) {

			/*
			 * The umount failures are already logged
			 */

			rc = 1;
			goto out;
		}

	}

out:

	(void) scha_resource_close(&handle);

	if (cmd_output)
		free(cmd_output);

	return (rc);
}

/*
 * Function: svc_executeCmdusingPopen
 *
 * Arguments:
 *
 * cmd		The command string which has to be executed
 *
 * cmd_output	The location where the command output to be placed
 *
 * Return:
 *
 * 0	Command execution completed successfully.
 *
 * >0	Command execution failed.
 *
 * Comments:
 * The command output is stored in cmd_output. It is the responsibility of
 * caller of this routine to free the memory allocated to cmd_output.
 * NOTE:
 * popen/pclose is used to implement this routine, which is expensive because
 * of shell process creation. So it is recommended to use
 * svc_executeCmdusingFork() to execute a command which does not involve pipe
 * or output redirection characters.
 */

static int
svc_executeCmdusingPopen(char *cmd, char **cmd_output)
{
	FILE		*stream = NULL;
	hasc_strbuf_t	stream_output;
	char		buf[BUFSIZ];
	char		*errstr = NULL;
	int		status = 0;

	stream_output.str = NULL;

	/*
	 * Assign empty string to the command output to avoid dereferencing
	 * NULL pointer by caller during the error cases that have no output
	 * for the given command.
	 */

	if (svc_addStr2Strbuf(&stream_output, "")) {
		exit(1);
	}

	if ((stream = popen(cmd, "r")) == NULL) {

		(void) svc_sprintf(&errstr,
		    "Failed to initiate a pipe for command %s : %s ",
		    cmd, strerror(errno));
		(void) svc_addStr2Strbuf(&stream_output, errstr);
		status = 1;
		goto out;
	}

	while (fgets(buf, BUFSIZ, stream)) {
		if (svc_addStr2Strbuf(&stream_output, buf)) {
			status = 1;
			goto out;
		}
	}

	if ((status = pclose(stream)) == -1) {

		(void) svc_sprintf(&errstr,
		    "Failed to close the pipe created for command %s : %s",
		    cmd, strerror(errno));
		(void) svc_addStr2Strbuf(&stream_output, errstr);
		status = 1;
		goto out;
	}

	/*
	 * Check the status of command completion.
	 */

	if (WIFEXITED(status)) {

		/*
		 * Obtain the exit status as the command completed normally.
		 */

		status = WEXITSTATUS((uint_t)status);

	} else if (WIFSIGNALED(status)) {

		/*
		 * Process(es) is terminated by signal.
		 */

		(void) svc_sprintf(&errstr, "The process(es) created by "
		    "command : %s is terminated by the signal : %d ",
		    cmd, WTERMSIG(status));
		(void) svc_addStr2Strbuf(&stream_output, errstr);

		status = 1;

	} else {

		/*
		 * Process(es) is stopped by signal.
		 */

		(void) svc_sprintf(&errstr, "The process(es) created by "
		    "command : %s is stopped by the signal : %d ",
		    cmd, WSTOPSIG((uint_t)status));
		(void) svc_addStr2Strbuf(&stream_output, errstr);

		status = 1;
	}

out:
	if (errstr)
		free(errstr);
	*cmd_output = stream_output.str;
	return (status);
}

#define	RDPIPE	0
#define	WRPIPE	1

/*
 * Function: svc_executeCmdusingFork
 *
 * Arguments:
 *
 * cmd		The command string which has to be executed
 *
 * cmd_output	The location where the command output to be placed
 *
 * Return:
 *
 * 0	Command execution completed successfully
 *
 * >0	Command execution failed.
 *
 * Comments:
 * The command output is stored in cmd_output. It is the responsibility of
 * caller of this routine to free the memory allocated to cmd_output.
 * NOTE:
 * See also the comments of svc_executeCmdusingPopen().
 * This routine is implemented using fork(), execv() and pipe().
 * The following are the steps ..
 * 1)Convert the given command with options to an array of strings (arguments)
 *   which is used to pass as argument list for execv.
 * 2)The parent forks a child.
 * 3)The child redirects stdout ans stderr output to the pipe which will read
 *   by parent.
 * 4)The child will execute the command using execv.
 * 5)Parent reads the command output from pipe and waits for the child.
 */

int
svc_executeCmdusingFork(char *cmd, char **cmd_output)
{

	scha_str_array_t cmdargv;		/* arg list for command */
	char		*tmpcmd;
	char		*argtok;
	hasc_strbuf_t	stream_output;
	int		copipe[2];		/* pipe for command output */
	int		nbytes;
	pid_t		child;
	char		*errstr = NULL;
	char		buf[BUFSIZ];
	int		rc, status = 0;

	stream_output.str = NULL;

	/*
	 * Assign empty string to the command output to avoid dereferencing
	 * NULL pointer by caller during the error cases that have no output
	 * for the given command.
	 */

	if (svc_addStr2Strbuf(&stream_output, "")) {
		exit(1);
	}

	/*
	 * Initialize the command argument list.
	 */

	cmdargv.array_cnt = 0;
	cmdargv.str_array = NULL;


	/*
	 * Make a temporary copy of original command string, because the
	 * strtok() modifies the given string during tokenization.
	 */

	if ((tmpcmd = strdup(cmd)) == NULL) {
		hasc_scds_syslog_0(LOG_ERR, "Failed to allocate memory.");
		return (1);
	}

	/*
	 * Convert command string to array of strings with SPACE/TAB as
	 * delimiter.
	 */

	if ((argtok = strtok(tmpcmd, " \t")) != NULL) {
		if (svc_addStr2Array(argtok, &cmdargv)) {
			return (1);
		}
		while ((argtok = strtok(NULL, " ")) != NULL) {
			if (svc_addStr2Array(argtok, &cmdargv)) {
				return (1);
			}
		}
	}

	/*
	 * Create a pipe (command output pipe) to read child output.
	 */

	if (pipe(copipe) == -1) {
		(void) svc_sprintf(&errstr,
		    "Failed to create pipe for the command %s : %s",
		    cmd, strerror(errno));
		(void) svc_addStr2Strbuf(&stream_output, errstr);
		*cmd_output = stream_output.str;
		return (1);
	}

	/*
	 * Don't block on an empty pipe.
	 */

	(void) fcntl(copipe[RDPIPE], F_SETFL, O_NDELAY|O_NONBLOCK);

	switch (child = fork()) {

		case 0 :
			/*
			 * Child process.
			 * Redirect the command stdout and stderr to the pipe
			 * and do execv.
			 */

			(void) dup2(copipe[WRPIPE], fileno(stdout));
			(void) dup2(copipe[WRPIPE], fileno(stderr));
			(void) close(copipe[WRPIPE]);

			(void) execv(cmdargv.str_array[0], cmdargv.str_array);

			(void) svc_sprintf(&errstr,
			    "Exec failed for command %s : %s",
			    cmd, strerror(errno));
			(void) svc_addStr2Strbuf(&stream_output, errstr);

			_exit(127);		/* exec error */

		default :
			/*
			 * Parent process.
			 * Collect the command output and wait for the child.
			 */

			while ((nbytes = read(copipe[RDPIPE], buf,
			    BUFSIZ - 1)) > 0) {
				buf[nbytes] = '\0';
				if (svc_addStr2Strbuf(&stream_output, buf)) {
					status = 1;
					break;
				}
			}

			(void) close(copipe[RDPIPE]);

			rc = waitpid(child, &status, 0);
			if (rc == -1) {
				(void) svc_sprintf(&errstr,
				    "Failed to obtain return status of the "
				    "command %s: %s.", cmd, strerror(errno));
				(void) svc_addStr2Strbuf(&stream_output,
				    errstr);
				*cmd_output = stream_output.str;
				return (1);
			}

			/*
			 * Check the status of command completion.
			 */

			if (WIFEXITED(status)) {

				status = WEXITSTATUS((uint_t)status);

			} else if (WIFSIGNALED(status)) {

				/*
				 * Process(es) is terminated by signal.
				 */

				(void) svc_sprintf(&errstr,
				    "The process(es) created by command : %s "
				    "is terminated by the signal : %d ",
				    cmd, WTERMSIG(status));
				(void) svc_addStr2Strbuf(&stream_output,
				    errstr);

				status = 1;

			} else {

				/*
				 * Process(es) is stopped by signal.
				 */

				(void) svc_sprintf(&errstr,
				    "The process(es) created by command : %s "
				    "is stopped by the signal : %d ",
				    cmd, WSTOPSIG((uint_t)status));
				(void) svc_addStr2Strbuf(&stream_output,
				    errstr);

				status = 1;
			}

			*cmd_output = stream_output.str;

			return (status);

		case -1 :
			(void) svc_sprintf(&errstr,
			    "Failed to fork for command %s : %s",
			    cmd, strerror(errno));
			(void) svc_addStr2Strbuf(&stream_output, errstr);
			*cmd_output = stream_output.str;
			return (1);
	}
}

/*
 * Function: svc_doMount
 *
 * Arguments:
 *
 * arg		Which contains the mount entry which has to be mounted
 *
 * Return:
 *
 * 0		if mount is done successfully
 *
 * 1		if an error occurred.
 *
 * Comments:
 * This function tries to mount the file system at the given mount point.
 * If the mount fails it tries to do an overlay mount.
 */

void *
svc_doMount(void *arg)
{

	fsentry_t	*mnt_entry = (fsentry_t *)arg;
	DIR		*dirp;
	char		*cmd = NULL;
	char		*cmd_output = NULL;
	int		cmd_status;
	int		totalfiles = 0;
	int		rc = 0;
	char		*mnt_point;

#ifdef linux
	mnt_point = mnt_entry->vp->fs_file;
#else
	mnt_point = mnt_entry->vp->vfs_mountp;
#endif

	/*
	 * Check if the mount directory is not empty i.e. any entries other
	 * than "." and ".." do exist. Log an informational message for the
	 * non empty mount directory.
	 */

	if ((dirp = opendir(mnt_point)) == NULL) {
		rc = errno;
		hasc_scds_syslog_2(LOG_ERR,
		    "Failed to open %s: %s.",
		    mnt_point,		/* CSTYLED */
		    strerror(rc));	/*lint !e666 */
		rc = 1;
		goto out;
	}

	while (readdir(dirp) != NULL) {
		totalfiles++;
		if (totalfiles > 2)
			break;
	}

	(void) closedir(dirp);

	if (totalfiles <= 2) {
		hasc_scds_syslog_1(LOG_INFO, "About to mount %s.", mnt_point);
	} else {
		hasc_scds_syslog_1(LOG_INFO, "About to mount %s. "
		    "Underlying files/directories will be "
		    "inaccessible.", mnt_point);
	}

	/*
	 * Prepare the command string for mount.
	 * Eg: mount mnt_point
	 */

	if (svc_sprintf(&cmd, "%s %s", MOUNT, mnt_point)) {
		rc = 1;
		goto out;
	}

	/*
	 * Try mount initially without overlay option.
	 */

	cmd_status = svc_executeCmdusingFork(cmd, &cmd_output);

	if (cmd_status == 0) {

		/*
		 * Mount succeeded.
		 */

		scds_syslog_debug(LOG_INFO, "Mount of %s successful.",
		    mnt_point);

	} else {

		/*
		 * Mount command failed, try overlay mount.
		 */

		scds_syslog_debug(LOG_INFO, "Mount of %s failed: (%d) %s."
		    "(Trying an overlay mount)", mnt_point,
		    cmd_status, cmd_output);

		if (cmd) free(cmd);
		if (cmd_output) free(cmd_output);

		if (svc_sprintf(&cmd, "%s %s", MOUNTO, mnt_point)) {
			rc = 1;
			goto out;
		}

		cmd_status = svc_executeCmdusingFork(cmd, &cmd_output);

		if (cmd_status == 0) {

			/*
			 * Overlay mount succeeded.
			 */

			scds_syslog_debug(LOG_INFO, "Mount of %s successful.",
			    mnt_point);

		} else {

			/*
			 * Overlay mount too failed.
			 */

			/*
			 * In scalable RG, mounting a file system globally is
			 * tried by all nodes selected by RGM to invoke
			 * prenet_start. So the failure of mount on a node
			 * shouldn't be treated as failure as it can be success
			 * on some other node. After mount invocation by a node
			 * (irrespective of result) the confirmation of mount
			 * is done by checking MNTTAB.
			 */

			if (gSvcInfo.rgMode == RGMODE_SCALABLE) {

				/* Log a warning message */
				hasc_scds_syslog_1(LOG_WARNING,
				    "Attempt to mount %s failed on this node.",
				    mnt_point);
			} else {
				hasc_scds_syslog_3(LOG_ERR,
				    "Mount of %s failed: (%d) %s.",
				    mnt_point, cmd_status, cmd_output);
				rc = 1;
				goto out;
			}
		}
	}

	if (cmd_output)
		free(cmd_output);

out:
	if (rc) {
		mnt_entry->status |= FS_MOUNTFAILED;
	} else {
		mnt_entry->status &= ~FS_MOUNTFAILED;
	}

	mnt_entry->tid = 0;
	return (NULL);
}

/*
 * Function: svc_sprintf
 *
 * Arguments:
 *
 * str		The string location where the output has to placed
 *
 * format, ...	The format and argument list
 *
 * Return:
 *
 * 0		if the arguments are written to string correctly.
 *
 * 1		if an error occurs.
 *
 * Comments:
 * This function allocates required memory and writes the argument list to
 * to location provided.
 */

static int
svc_sprintf(char **str, const char *format, ...)
{
	int	size;
	char	s;	/* Used to know size of memory for format string */
	va_list	ap;
	int	rc = 0;

	/*
	 * override FlexeLint error 40;
	 * lint doesn't understand compiler builtin variable __builtin_va_alist
	 */
				/* CSTYLED */
	va_start(ap, format);	/*lint !e40 */

	/*
	 * Trick to know the size of memory required.
	 */

	if ((size = vsnprintf(&s, 1, format, ap)) < 0) {
		/*
		 * Assuming memory failure
		 * (as EINVAL, EILSEQ errors not possible)
		 */
		hasc_scds_syslog_0(LOG_ERR, "Failed to allocate memory.");
		rc = 1;
		goto error;
	}

	if ((*str = (char *)malloc(((uint_t)size + 1))) == NULL) {
		hasc_scds_syslog_0(LOG_ERR, "Failed to allocate memory.");
		rc = 1;
		goto error;
	}
	(void) vsprintf(*str, format, ap);
error:
	va_end(ap);
	return (rc);
}
