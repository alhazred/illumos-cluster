/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * hastorageclient_postnet_stop.c
 *
 * Postnet stop method implementation
 * for the HAStorageClient resource type.
 *
 */

#pragma ident	"@(#)hastorageclient_postnet_stop.c 1.3	08/05/20 SMI"

#include <stdlib.h>

#include "hastorageclient.h"

/*
 *
 * Function: main
 *
 * Arguments:
 *
 * argc		Number of arguments
 *
 * argv		Argument list from command line passed by RGM
 *
 * Return:
 *
 * 0	if completed successfully i.e  validation on the configuration
 *	and unmount the file systems which are part of resource.
 *
 * 1	if an error occurred.
 *
 * Comments:
 *
 * A HAStorageClient resource can belong to a resource group which is either
 * failover ( primaried on only one node ) or scalable ( primaried on
 * more than one node ). Accordingly, the postnet stop method is invoked on
 * one or more nodes.
 *
 * The RGM invokes a resource's postnet stop method after bringing the
 * the network addresses down.
 *
 */

int
main(int argc, char *argv[])
{
	scds_handle_t		ds_handle;
	scha_extprop_value_t	*fmp_prop_value = NULL;
	scha_err_t		ret;
	int			rc = 0;

	/*
	 * Initialize the DSDL subsystem (which processes the command line
	 * arguments passed by RGM and also initializes logging).
	 */

	if (scds_initialize(&ds_handle, argc, argv) != SCHA_ERR_NOERR) {

		hasc_scds_syslog_0(LOG_ERR, "Failed to initialize the DSDL.");

		return (1);
	}

	/*
	 * Retrieve the FilesystemMountPoints extension
	 * property information.
	 */

	ret = scds_get_ext_property(ds_handle, FILESYSTEMMOUNTPOINTS,
	    SCHA_PTYPE_STRINGARRAY, &fmp_prop_value);

	if (ret != SCHA_ERR_NOERR) {

		hasc_scds_syslog_2(LOG_ERR,
		    "Failed to retrieve property %s: %s",
		    FILESYSTEMMOUNTPOINTS,	/* CSTYLED */
		    scds_error_string(ret));	/*lint !e666 */

		rc = 1;
		goto end;
	}

	/*
	 * Check if FilesystemMountPoints is null.
	 * If null, log a warning message and return.
	 */

	if (fmp_prop_value->val.val_strarray->array_cnt == 0) {

		hasc_scds_syslog_0(LOG_WARNING,
		    "Extension properties FilesystemMountPoints "
		    "are empty.");

		goto end;
	}

	/*
	 * Unmount the file systems of the HAStorageClient resource
	 */

	if (svc_umountFilesystems(fmp_prop_value->val.val_strarray)) {
		/*
		 * Error messages related to mounting the file systems are
		 * already logged.
		 */
		rc = 1;
		goto end;
	}

	hasc_scds_syslog_0(LOG_INFO,
	    "Unmounting the file systems is completed successfully.");

end:
	if (fmp_prop_value) scds_free_ext_property(fmp_prop_value);

	scds_close(&ds_handle);

	return (rc);
}
