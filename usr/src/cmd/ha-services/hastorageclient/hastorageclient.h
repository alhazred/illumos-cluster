/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_HASTORAGECLIENT_H_
#define	_HASTORAGECLIENT_H_

#pragma ident	"@(#)hastorageclient.h	1.3	08/05/20 SMI"

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Header file for the HAStorageClient resource type implementation.
 */

/*
 * Notes for generating scmsgs.
 * ---------------------------
 * The script (usr/src/messageid/id.ksh) used in sun cluster to generate
 * scmsgs looks for sc_syslog_msg_log() and scds_syslog() only. It does not
 * understand the wrapper functions on these syslog messages.
 *
 * So to generate the scmsgs for HAStorageClient
 * 1)Keep a backup copy of all HAStorageClient files.
 * 2)Change hasc_scds_syslog_*() in all files to scds_sylog().
 * 3)Run scmsgs (dmake OS=5.x scmsgs) and enter messages to
 *   usr/src/messageid/sc_msgid_expl in specified format.
 * 4)Restore the original files.
 *
 */

#include <errno.h>
#include <libintl.h>
#include <locale.h>
#include <rgm/libdsdev.h>
#include <scha.h>
#include <stdio.h>
#include <strings.h>
#ifdef linux
#include <fstab.h>
#else
#include <sys/vfstab.h>
#include <sys/mnttab.h>
#endif
#include <thread.h>
#include <synch.h>

#define	FILESYSTEMMOUNTPOINTS 	"FilesystemMountPoints"

#define	CAT	"/bin/cat"
#define	CUT	"/bin/cut"
#define	DATE	"/bin/date"
#define	DIFF	"/bin/diff"
#define	DIRNAME	"/bin/dirname"
#define	GREP	"/bin/grep"
#define	LS	"/bin/ls"
#define	MKDIR	"/bin/mkdir"
#define	MV	"/bin/mv"
#define	PRINTF	"/bin/printf"
#define	RM	"/bin/rm"
#define	SED	"/bin/sed"
#define	SLEEP	"/bin/sleep"
#define	SORT	"/bin/sort"
#define	TOUCH	"/bin/touch"
#define	WC	"/usr/bin/wc"

#ifdef linux
#define	MOUNT		"/bin/mount "	/* Mount */
#define	MOUNTO		"/bin/mount -O "	/* Mount in overlay mode */

#define	UMOUNT		"/bin/umount "	/* Unmount */
#define	UMOUNTF		"/bin/umount -f "	/* Forced unmount */
#define	UMOUNTFA	"/bin/umount -f "
				/* forced unmount - No parallel mode */
#else
#define	MOUNT		"/usr/sbin/mount "	/* Mount */
#define	MOUNTO		"/usr/sbin/mount -O "	/* Mount in overlay mode */

#define	UMOUNT		"/usr/sbin/umount "	/* Unmount */
#define	UMOUNTF		"/usr/sbin/umount -f "	/* Forced unmount */
#define	UMOUNTFA	"/usr/sbin/umount -f -a "
				/* Parallel forced unmount */
#endif


/*
 * damadm (Monitoring)
 */
#define	DAMADM		"/usr/cluster/lib/sc/damadm "
#define	DAM_MONITOR	1
#define	DAM_UNMONITOR	2

/*
 * Debug levels for syslog debug messaging.
 */
#define	DBG_LEVEL_HIGH	9
#define	DBG_LEVEL_MED	5
#define	DBG_LEVEL_LOW	1

/*
 * Type defining method codes.
 */
typedef enum {

	HASTORAGECLIENT_VALIDATE,
	HASTORAGECLIENT_PRENET_START,
	HASTORAGECLIENT_UPDATE,
	HASTORAGECLIENT_VALIDATE_FOR_UPDATE

} HAStorageClientSvcMethod;

/*
 * A structure representing a device service.
 */
typedef struct HAStorageClientSvc {

	const char	*svcPath; 	/* service path	   */
	scha_str_array_t svcName;	/* service name(s) */
#ifdef linux
	struct fstab	fsEntry;	/* FSTAB entry for mount point */
#else
	struct vfstab	vfsEntry;	/* VFSTAB entry for mount point */
#endif
} HAStorageClientSvc;

/*
 * A structure representing information global to all device services. In
 * addition, it contains a pointer to the list of HAStorageClientSvc structures.
 * Each such structure represents a global device service.
 */
typedef struct HAStorageClientSvcInfo {

	uint_t 		count;		/* Number of list entries */

	HAStorageClientSvc *list;	/* List of device services */

	scha_rgmode_t	rgMode;		/* Resource group mode	*/

	const char 	*rgName;	/* Resource group name 	*/

	const char 	*rsName;	/* Resource name	*/

} HAStorageClientSvcInfo;

/*
 * A structure used to maintain information of a file system. This is used
 * during mount/umount operation. The same structure is used
 * for simplicity though all fields are not used for every operation.
 */

/*
 * Symbolic constants which represent the status of a file system.
 */

#define	FS_RPFAILED		0x00100	/* realpath failed on mount point */
					/* (i.e mount point doesn't exist) */
#define	FS_MOUNTFAILED		0x00200	/* mount failed on file system */
#define	FS_MOUNTCONFIRM		0x00400	/* mount confirmed on file system */


#define	FS_UMOUNTFAILED		0x10000	/* unmount failed on file system */
#define	FS_UMOUNTCONFIRM	0x20000	/* unmount confirmed on file system */

typedef struct fsentry {

	char		*mountp;	/* The mount point of file system */

	char		*rpath;		/* real pathname for mount point */

	uint_t		depth;		/* depth of mount point */

	uint_t		status;		/* status of file system */
					/* (see above symbolic constants) */
#ifdef linux
	struct fstab	*vp;		/* fstab entry  of file system */
#else
	struct vfstab	*vp;		/* vfstab entry  of file system */
#endif

	pthread_t	tid;		/* Thread id */

} fsentry_t;

/*
 * A structure which can be used as string buffer to append the strings.
 * NOTE: The string in the instance of the structure should be initialized
 * to NULL (to specify that it is empty string, otherwise it contains garbage
 * value which leads to problems)
 */

typedef struct hasc_strbuf {

	char *str;
	unsigned int str_length;	/* The current length of string */
	unsigned int str_capacity;	/* The maximum length of string */

} hasc_strbuf_t;


extern HAStorageClientSvcInfo gSvcInfo;
extern HAStorageClientSvcMethod svcMethodId;
extern hasc_strbuf_t update_status_msg;	/* The status message for update */
extern hasc_strbuf_t failed_switchovers_msg;

extern int svc_validate(scds_handle_t);
extern int svc_umountRemovedMntPts(void);
extern int svc_saveOldMntPts(void);
extern int svc_umountFilesystems(scha_str_array_t  *);
extern int svc_mountFilesystems(void);
extern int svc_addStr2Array(const char *, scha_str_array_t *);
extern int svc_addStr2Strbuf(hasc_strbuf_t *, char *);

/*
 * This set of macros prints:
 *	- a non i18n'ed message to syslog
 *  and
 *	- a fully i18n'ed message to stderr is method == validate and priority
 *	    is LOG_ERR.
 *
 * ONLY hasc_scds_syslog_X() should be used (__hasc_scds_syslog() is internal
 * to the macro).
 *
 * X is the number of arguments required.
 *
 * These defines are used because flexelint does not recognise __VA_ARGS__.
 * Also, having the developer to explicitly write the number of arguments
 * required makes the code checkable by the compiler.
 *
 * Note that this macro make functions passed in argument called more than
 * once, hence DON'T USE it with *side effect or non idempotent* functions.
 *
 * This macro requires the lint !e666 directive when using functions as
 * arguments since functions MUST be idempotent.
 *
 *	/\	Because errno is resetted between the call to
 *     /  \	hasc_scds_syslog() and the call to scds_syslog() and between
 *    /STOP\	the call to scds_syslog() and the call to fprintf(), HAS+
 *   /  !!  \	developers SHOULD NEVER use errno when using
 *  +--------+	hasc_scds_syslog() but a temporary variable for storing errno.
 *
 *		Ex: - hasc_scds_syslog_1("%s", strerror(errno)); IS FORBIDDEN.
 *		    - int ret = errno;
 *		      hasc_scds_syslog_1("%s", strerror(ret)); IS ALLOWED.
 */
#define	__hasc_scds_syslog(pri, fmt, a1, a2, a3, a4)			\
	do {	/* ugly, but needed for lint! */			\
	    /* temporarily reset locale to C locale */			\
	    (void) setlocale(LC_ALL, "C");				\
	    scds_syslog(pri, fmt, a1, a2, a3, a4);			\
	    /* restore locale from env */				\
	    (void) setlocale(LC_ALL, "");				\
									\
	    if ((pri == LOG_ERR) &&					\
		    (svcMethodId == HASTORAGECLIENT_VALIDATE)) {	\
		    (void) fprintf(stderr, gettext(fmt), a1, a2, a3,	\
			a4);						\
		    (void) fprintf(stderr, "\n");			\
	    }								\
	} while (0 != 0)
#define	hasc_scds_syslog_0(p, f)			\
	__hasc_scds_syslog(p, f, 0, 0, 0, 0)
#define	hasc_scds_syslog_1(p, f, a1)			\
	__hasc_scds_syslog(p, f, a1, 0, 0, 0)
#define	hasc_scds_syslog_2(p, f, a1, a2)		\
	__hasc_scds_syslog(p, f, a1, a2, 0, 0)
#define	hasc_scds_syslog_3(p, f, a1, a2, a3)		\
	__hasc_scds_syslog(p, f, a1, a2, a3, 0)
#define	hasc_scds_syslog_4(p, f, a1, a2, a3, a4)	\
	__hasc_scds_syslog(p, f, a1, a2, a3, a4)

#define	ADD_STR2STRBUF(strbuf, str)			\
	do {						\
		if (svc_addStr2Strbuf(&strbuf, str)) {	\
			return (1);			\
		}					\
	} while (0 != 0);

#define	ADD_TO_STATUS_MSG(str) ADD_STR2STRBUF(update_status_msg, str)

#define	ADD_TO_STATUS_MSG_IF_NOT_EMPTY(str)		\
	do {						\
		if (update_status_msg.str_length != 0)	\
			ADD_TO_STATUS_MSG(str);		\
	} while (0 != 0)

#ifdef __cplusplus
}
#endif

#endif	/* !_HASTORAGECLIENT_H_ */
