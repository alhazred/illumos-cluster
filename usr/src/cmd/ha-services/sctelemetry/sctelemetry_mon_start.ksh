#!/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident	"@(#)sctelemetry_mon_start.ksh	1.8	08/05/20 SMI"

# Sun Cluster Data Services Builder template version 1.0
#
# Monitor start Method for sctelemetry.
#
##############################################################################

#include_lib

# Parse the arguments that have been passed to this method
parse_args "$@" || error_exit $?

# We need to know where the probe method resides. This is specified in the
# RT_BASEDIR property of the resource type.
RT_BASEDIR=`${SCHA_RESOURCE_GET} -O RT_BASEDIR -R $RESOURCE_NAME \
	-G $RESOURCEGROUP_NAME`

PMF_TAG=$RESOURCEGROUP_NAME,$RESOURCE_NAME,0.mon

# Start the probe for the data service under PMF. Use the infinite retries
# option to start the probe. Pass the the Resource name, type and group to the
# probe method.
$PMFADM -c $PMF_TAG -n -1 -t -1 \
	$RT_BASEDIR/sctelemetry_probe -R $RESOURCE_NAME -G $RESOURCEGROUP_NAME \
	-T $RESOURCETYPE_NAME

# Log a message indicating that the monitor for sctelemetry has been started.
if [[ $? -eq 0 ]]; then
	# SCMSGS
	# @explanation
	# The monitor program of the telemetry data service started
	# successfully.
	# @user_action
	# This message is informational; no user action needed.
	scds_syslog -p info -t "${SYSLOG_TAG}" -m \
		"Telemetry data service monitor successfully started"
else
	# SCMSGS
	# @explanation
	# The monitor program of the telemetry data service could not start.
	# This should never occur.
	# @user_action
	# Contact your authorized Sun service provider to determine whether a
	# workaround or patch is available.
	scds_syslog -p error -t "${SYSLOG_TAG}" -m \
		"Telemetry data service monitor method failed to start"
fi

exit 0
