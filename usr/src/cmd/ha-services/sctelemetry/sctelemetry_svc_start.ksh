#!/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident	"@(#)sctelemetry_svc_start.ksh	1.17	08/05/20 SMI"
#
# Start Method for sctelemetry.
#

#include_lib

# Parse the arguments that have been passed to this method
parse_args "$@" || error_exit $?

# Loop until we can connect to our module
# This will ensure that CACAO is up and running
#
# The loop will continue for the whole duration of the START_TIMEOUT
typeset CACAO_RESTARTED=0
typeset READY=1
while [[ ${READY} -ne 0 ]]
do
	# We do not need to retrieve the value
	# We are only interested to know if the operation succeeded
	${CACAOCSC} -h localhost -p ${PORT} -d ${WELLKNOWN} -f ${PASSWORD} -c 'com.sun.cacao.jmx:getAttribute "com.sun.cacao:type=module,instance=\"com.sun.cluster.ganymede\"" AdministrativeState' > /dev/null 2>&1


	if [[ $? -ne 0 ]]; then
	    READY=1
	    sleep 1;
	else
	    READY=0
	fi
done

# unlock the telemetry (Ganymede) Cacao module
${CACAOCSC} -h localhost -p ${PORT} -d ${WELLKNOWN} -f ${PASSWORD} \
	-c 'com.sun.cacao.jmx:invoke "com.sun.cacao:type=module,instance=\"com.sun.cluster.ganymede\"" unlock'


# Log a message indicating that telemetry has not been started.
if [[ $? -ne 0 ]]; then
	# SCMSGS
	# @explanation
	# The telemetry data service has failed to start(in this case
	# cacao may have been started before JavaDB packages were installed).
	# @user_action
	# This message is informational; no user action needed.

	scds_syslog -p info -t "${SYSLOG_TAG}" -m \
	    "Telemetry data service start method has failed. The system will restart cacao and then attempt to retry..."
        ${CACAOADM} restart > /dev/null 2>&1
        CACAO_RESTARTED=1
        ${CACAOCSC} -h localhost -p ${PORT} -d ${WELLKNOWN} -f ${PASSWORD} \
       	    -c 'com.sun.cacao.jmx:invoke "com.sun.cacao:type=module,instance=\"com.sun.cluster.ganymede\"" unlock'
	if [[ $? -ne 0 ]]; then
	    scds_syslog -p error -t "${SYSLOG_TAG}" -m \
		"Telemetry data service start method failed for unknown reason"
  	    error_exit 1
	fi

fi

# OperationalState is DISABLED if the module failed during start
typeset STATE=`${CACAOCSC} -h localhost -p ${PORT} -d ${WELLKNOWN} -f ${PASSWORD} -c 'com.sun.cacao.jmx:getAttribute "com.sun.cacao:type=module,instance=\"com.sun.cluster.ganymede\"" OperationalState' 2>&1`

if [[ ${STATE} == "DISABLED" ]]; then
        if [[ ${CACAO_RESTARTED} -eq 0 ]]; then

	    scds_syslog -p info -t "${SYSLOG_TAG}" -m \
		"Telemetry data service start method has failed. The system will restart cacao and then attempt to retry..."
            ${CACAOADM} restart > /dev/null 2>&1
            CACAO_RESTARTED=1
	    STATE=`${CACAOCSC} -h localhost -p ${PORT} -d ${WELLKNOWN} -f ${PASSWORD} -c 'com.sun.cacao.jmx:getAttribute "com.sun.cacao:type=module,instance=\"com.sun.cluster.ganymede\"" OperationalState' 2>&1`
	fi

	if [[ ${STATE} == "DISABLED" ]]; then
	    scds_syslog -p error -t "${SYSLOG_TAG}" -m \
		"Telemetry data service start method failed for unknown reason"
  	    error_exit 1
        fi 
fi

scds_syslog -p info -t "${SYSLOG_TAG}" -m \
    "Telemetry data service start method completed sucessfully"

exit 0
