#!/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident	"@(#)sctelemetry_mon_check.ksh	1.8	08/05/20 SMI"

# Sun Cluster Data Services Builder template version 1.0
#
# Monitor check  Method for sctelemetry.
#
# This method is called by the RGM whenever there is a failover to be performed
# by the fault monitor. This method is called to ensure that the new node that
# dataservice will be failing over to is actually healthy enough to host the
# dataservice. This method makes a call to the validate method that has been
# registered in order to achieve this.
##############################################################################

#include_lib

# Parse the arguments that have been passed to this method
parse_args "$@" || error_exit $?

PMF_TAG=$RESOURCEGROUP_NAME,$RESOURCE_NAME,0.svc

# We need to know the full path for the validate method which resides
# in the directory <RT_BASEDIR>. Get this from the RT_BASEDIR property of the
# resource type.
RT_BASEDIR=`${SCHA_RESOURCE_GET} -O RT_BASEDIR -R $RESOURCE_NAME \
	-G $RESOURCEGROUP_NAME`

# Obtain the name of the validate method for this resource.
VALIDATE_METHOD=`${SCHA_RESOURCE_GET} -O VALIDATE \
	-R $RESOURCE_NAME -G $RESOURCEGROUP_NAME`

# Call the validate method so that the dataservice can be failed over
# successfully to the new node.
$RT_BASEDIR/$VALIDATE_METHOD -R $RESOURCE_NAME -G $RESOURCEGROUP_NAME \
	-T $RESOURCETYPE_NAME

# Log a message indicating that monitor check was successful.
if [[ $? -ne 0 ]]; then
	# SCMSGS
	# @explanation
	# The monitor program of the telemetry data service detected an error.
	# The cause of the error is given in some error messages displayed
	# before this one.
	# @user_action
	# Check if there are other error messages before this one, and contact
	# your authorized Sun service provider to determine whether a
	# workaround or patch is available.
	scds_syslog -p error -t "${SYSLOG_TAG}" -m \
		"Telemetry data service monitor check method failed"
	exit 1
fi

# SCMSGS
# @explanation
# The monitor program of the telemetry data service reported success.
# @user_action
# This message is informational; no user action needed.
scds_syslog -p info -t "${SYSLOG_TAG}" -m \
    "Telemetry data service monitor check method succeeded"

exit 0
