#!/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident	"@(#)sctelemetry_lib.ksh	1.9	08/05/20 SMI"
#
# Sun Cluster Data Services Builder template version 1.0
#
# lib for telemetry
#
# This lib is integrated in all other scripts at compilation
##############################################################################

export PATH=/bin:/usr/bin:/usr/cluster/bin:/usr/sbin:/usr/proc/bin:$PATH



typeset -r CACAOCSC="/usr/lib/cacao/lib/tools/cacaocsc"
typeset -r CACAOADM="/usr/lib/cacao/bin/cacaoadm"

typeset -r SCHA_CLUSTER_GET="/usr/cluster/bin/scha_cluster_get"
typeset -r SCHA_RESOURCE_GET="/usr/cluster/bin/scha_resource_get"
typeset -r SCHA_RESOURCE_SETSTATUS="/usr/cluster/bin/scha_resource_setstatus"
typeset -r PMFADM="/usr/cluster/bin/pmfadm"

typeset -r PASSWORD="/etc/cacao/instances/default/security/password"
typeset -r WELLKNOWN="/etc/cacao/instances/default/security/nss/wellknown"

typeset -r PKGINFO="/usr/bin/pkginfo"
typeset -r WC="/usr/bin/wc"

# use an alias as opposed to a variable for the messageid/id.ksh script
# to recognize the logger command
alias scds_syslog="/usr/cluster/lib/sc/scds_syslog"

# note: cacaoadm doesn't launch a JVM for that operation
typeset -r PORT=`${CACAOADM} get-param -v commandstream-adaptor-port`

# Default syslog tag, final tag is set in full_parse_args and parse_args
SYSLOG_TAG="[SUNW.sctelemetry]"

###############################################################################
# validate have more options to parse
#
###############################################################################
function full_parse_args # [args ...]
{
    typeset opt
    typeset -i rc=0

    while getopts 'cur:x:g:R:T:G:' opt
    do
      case "$opt" in

	  R) RESOURCE_NAME=$OPTARG;;
	  G) RESOURCEGROUP_NAME=$OPTARG;;
	  T) RESOURCETYPE_NAME=$OPTARG;;
	  r) ;;
	  g) ;;
	  c) ;;
	  u) ;;
	  x) ;;
	  *)
 	      # SCMSGS
	      # @explanation
	      # An unknown option was given to a program or script of the
	      # telemetry data service. This should never occur.
	      # @user_action
	      # Contact your authorized Sun service provider to determine
	      # whether a workaround or patch is available.
	      scds_syslog -p error -t "${SYSLOG_TAG}" -m \
	         "ERROR: Option %s passed to a script of the telemetry data service is unknown" "${OPTARG}"
	      rc=1
	      ;;

      esac
    done

    SYSLOG_TAG="[${RESOURCETYPE_NAME},${RESOURCE_NAME},${RESOURCEGROUP_NAME}]"

    return $rc
}


###############################################################################
# Parse program arguments.
#
###############################################################################
function parse_args # [args ...]
{
    typeset -i rc=0
    typeset opt

    while getopts 'R:G:T:' opt
    do
      case "$opt" in

	  R) RESOURCE_NAME=$OPTARG;;
	  G) RESOURCEGROUP_NAME=$OPTARG;;
	  T) RESOURCETYPE_NAME=$OPTARG;;
	  *)
	     scds_syslog -p error -t "${SYSLOG_TAG}" -m \
	         "ERROR: Option %s passed to a script of the telemetry data service is unknown" "${OPTARG}"
	     rc=1
	     ;;

      esac
    done

    SYSLOG_TAG="[${RESOURCETYPE_NAME},${RESOURCE_NAME},${RESOURCEGROUP_NAME}]"

    return $rc
}


#############################################################
# error_exit()
# param:
#   o exit_code: exit argument
#############################################################
error_exit()
{
    typeset exit_code="${1:-0}"
    exit $exit_code
}

#############################################################
# The following function checks if a directory (parameter) is
# present
#
# param:
#   o directory to check
#
# returned code:
#   o 0 if the param is empty or a directory
#   o 1 if the param is not empty and not a directory
#
#############################################################
is_a_directory()
{
    if [[ $# -ne 1 ]]; then
	return 0
    fi

    if [[ -z $1 ]];then
	return 0
    fi

    if [[ ! -d $1 ]]; then
	# SCMSGS
	# @explanation
	# A program or script of the telemetry data service could not execute
	# because a directory does not exist. This should never occur.
	# @user_action
	# Contact your authorized Sun service provider to determine whether a
	# workaround or patch is available.
	scds_syslog -p error -t "${SYSLOG_TAG}" -m \
	    "The directory %s needed by the telemetry data service does not exist" "${1}"
	return 1
    fi

    return 0;
}

#############################################################
# The following function checks if a file (parameter) exists
# and is executable
#
# param:
#   o file to check
#
# returned code:
#   o 0 if the param is empty or executable
#   o 1 if the param is not empty and not executable
#
#############################################################
is_executable()
{
    if [[ $# -ne 1 ]]; then
	return 0
    fi

    if [[ -z $1 ]];then
	return 0
    fi

    if [[ ! -f $1 ]]; then
	# SCMSGS
	# @explanation
	# A program or script of the telemetry data service could not execute
	# because a file does not exist. This should never occur.
	# @user_action
	# Contact your authorized Sun service provider to determine whether a
	# workaround or patch is available.
	scds_syslog -p error -t "${SYSLOG_TAG}" -m \
	    "The file %s needed by the telemetry data service does not exist" "${1}"
	return 1
    fi

    if [[ ! -x $1 ]]; then
	# SCMSGS
	# @explanation
	# A program or script of the telemetry data service could not execute
	# because a file is not executable. This should never occur.
	# @user_action
	# Contact your authorized Sun service provider to determine whether a
	# workaround or patch is available.
	scds_syslog -p error -t "${SYSLOG_TAG}" -m \
	    "The file %s which the telemetry data service needs to execute is not executable" "${1}"
	return 1
    fi

    return 0;
}


#############################################################
# The following function checks if a file (parameter) is
# present
#
# param:
#   o file to check
#
# returned code:
#   o 0 if the param is empty or is a file
#   o 1 if the param is not empty and not a file
#
#############################################################
is_a_file()
{
    if [[ $# -ne 1 ]]; then
	return 0
    fi

    if [[ -z $1 ]];then
	return 0
    fi

    if [[ ! -f $1 ]]; then
	scds_syslog -p error -t "${SYSLOG_TAG}" -m \
	    "The file %s needed by the telemetry data service does not exist" "${1}"
	return 1
    fi

    return 0;
}

#############################################################
# The following function checks that all necessary files are
# Present
#############################################################

validate()
{
    typeset RG=$1
    typeset R=$2

    is_executable ${CACAOADM} || error_exit $?
    is_executable ${CACAOCSC} || error_exit $?
    is_a_file ${PASSWORD} || error_exit $?

    if [[ ! -d ${WELLKNOWN} ]]; then
        # SCMSGS
        # @explanation
        # A directory related to the Cacao security does not exist, most
        # probably because the security have not been generated or copied
        # over. This should never occur as the Sun Cluster Installation should
        # take care of that.
        # @user_action
        # Contact your authorized Sun service provider to determine whether a
        # workaround or patch is available.
        scds_syslog -p error -t "${SYSLOG_TAG}" -m \
	    "Directory %s does not exist; the Cacao security keys have not been generated or copied" "${WELLKNOWN}"
	error_exit 1
    fi

    # We need to know the location of the probe method in order to
    # restart it successfully. This value is obtained from the
    # RT_BASEDIR property of the RTR file.
    RT_BASEDIR=`${SCHA_RESOURCE_GET} -O RT_BASEDIR -R $R -G $RG`

    # we use the default package path as the RT_BASEDIR value
    if [[ -z "${RT_BASEDIR}" ]]; then
	RT_BASEDIR=/usr/cluster/lib/rgm/rt/sctelemetry
    fi

    # Validate is called before the creation of the R, in that case
    start_cmd_args="${RT_BASEDIR}/sctelemetry_svc_start"
    start_cmd_prog=`echo $start_cmd_args | nawk '{print $1}'`

    is_executable $start_cmd_prog || error_exit $?

    stop_cmd_args="${RT_BASEDIR}/sctelemetry_svc_stop"
    stop_cmd_prog=`echo $stop_cmd_args | nawk '{print $1}'`

    is_executable $stop_cmd_prog || error_exit $?

    probe_cmd_args="${RT_BASEDIR}/sctelemetry_probe"
    probe_cmd_prog=`echo $probe_cmd_args | nawk '{print $1}'`

    is_executable $probe_cmd_prog || error_exit $?

    if [[ -z $PORT ]]; then
	# SCMSGS
	# @explanation
	# The port number to connect to Cacao could not be retrieved. This
	# should never occur.
	# @user_action
	# Contact your authorized Sun service provider to determine whether a
	# workaround or patch is available.
	scds_syslog -p error -t "${SYSLOG_TAG}" -m \
		"Telemetry data service failed to get cacao port"
	return 1
    fi

    return 0
}



