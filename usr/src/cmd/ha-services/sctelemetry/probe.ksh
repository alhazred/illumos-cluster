#!/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident	"@(#)probe.ksh	1.5	08/05/20 SMI"

#include_lib

ADMINISTRATIVE_STATE=`${CACAOCSC} -h localhost -p ${PORT} -d ${WELLKNOWN} -f ${PASSWORD} -c 'com.sun.cacao.jmx:getAttribute "com.sun.cacao:type=module,instance=\"com.sun.cluster.ganymede\"" AdministrativeState' 2>&1`

if [[ ${ADMINISTRATIVE_STATE} != "UNLOCKED" ]]
then
	exit 1
fi

HEALTHY=`${CACAOCSC} -h localhost -p ${PORT} -d ${WELLKNOWN} -f ${PASSWORD} -c 'com.sun.cacao.jmx:getAttribute "com.sun.cacao:type=module,instance=\"com.sun.cluster.ganymede\"" Healthy' 2>&1`

if [[ ${HEALTHY} != "true" ]]
then
	exit 1
fi

exit 0
