#!/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident	"@(#)sctelemetry_svc_stop.ksh	1.7	08/05/20 SMI"

# Sun Cluster Data Services Builder template version 1.0
#
# Stop method for sctelemetry
#
###############################################################################

#include_lib

# cacao_is_running returns zero if Cacao is running, non-zero otherwise
function cacao_is_running
{
	# Use Cacao's echo service
	${CACAOCSC} -h localhost -p ${PORT} -d ${WELLKNOWN} -f ${PASSWORD} \
		-c 'com.sun.cacao:echo cacao_is_up' > /dev/null 2>&1

	return $?
}

function exit_0_if_cacao_is_not_running
{
	if ! cacao_is_running; then
		# SCMSGS
		# @explanation
		# The telemetry data service stopped successfully (in this
		# case it was already stopped as Cacao is not running).
		# @user_action
		# This message is informational; no user action needed.
		scds_syslog -p info -t "${SYSLOG_TAG}" -m \
		    "Stop method completed sucessfully (Cacao is not running)"
		error_exit 0
	fi
}

#
# Main
#

# Parse the arguments that have been passed to this method
parse_args "$@" || error_exit $?

exit_0_if_cacao_is_not_running

# Execute the stop command
${CACAOCSC} -h localhost -p ${PORT} -d ${WELLKNOWN} -f ${PASSWORD} \
    -c 'com.sun.cacao.jmx:invoke "com.sun.cacao:type=module,instance=\"com.sun.cluster.ganymede\"" lock' \
    > /dev/null 2>&1

# Handle the return code of this command
CACAOCSC_RT=$?
case ${CACAOCSC_RT} in
    #
    # We double check whether Cacao is running before
    # indicating that stop has failed
    #
    0)
	scds_syslog -p info -t "${SYSLOG_TAG}" -m \
	    "Telemetry data service stop method completed sucessfully"
        error_exit 0;;
    *)
	exit_0_if_cacao_is_not_running

	scds_syslog -p error -t "${SYSLOG_TAG}" -m \
	    "Telemetry data service stop method returned with error %s" "${CACAOCSC_RT}"

	error_exit 1;;
esac
