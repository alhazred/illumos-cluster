#!/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident	"@(#)sctelemetry_update.ksh	1.7	08/05/20 SMI"

# Sun Cluster Data Services Builder template version 1.0
#
# Update method for sctelemetry.
#
# This method only restarts the fault monitor.
#
###############################################################################

#include_lib

# Parse the arguments that have been passed to this method
parse_args "$@" || error_exit $?

PMF_TAG=$RESOURCEGROUP_NAME,$RESOURCE_NAME,0.mon

$PMFADM -q $PMF_TAG
if [[ $? == 0 ]]; then

	# kill the monitor that is running already
	$PMFADM -k $PMF_TAG KILL 2>/dev/null
	if [[ $? != 0 ]]; then
		# SCMSGS
		# @explanation
		# The monitor program of the telemetry data service could not
		# be stopped. This should never occur.
		# @user_action
		# Contact your authorized Sun service provider to determine
		# whether a workaround or patch is available.
		scds_syslog -p error -t "${SYSLOG_TAG}" -m \
			"Could not kill the telemetry data service monitor"
		exit 1
	else
		# could successfully stop sctelemetry. Log a message.

		# SCMSGS
		# @explanation
		# When the data service properties are updated, the data
		# service restarts the monitor.
		# @user_action
		# This message is informational; no user action needed.
		scds_syslog -p info -t "${SYSLOG_TAG}" -m \
			"Monitor for the telemetry data service stopped successfully. PMF will restart it"
	fi

else
    # SCMSGS
    # @explanation
    # The monitor program of the telemetry data service does not seem to be
    # registered with the PMF. This should never occur.
    # @user_action
    # Contact your authorized Sun service provider to determine whether a
    # workaround or patch is available.
    scds_syslog -p info -t "${SYSLOG_TAG}" -m \
	"No PMF tag registered for the telemetry data service monitor"
fi

exit 0
