#!/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)sctelemetry.ksh	1.19	09/02/26 SMI"
#
#

typeset -x TEXTDOMAIN=SUNW_SC_CMD
typeset -x TEXTDOMAINDIR=/usr/cluster/lib/locale

function usage
{
	echo "$(gettext 'Usage:')\n"
	echo "  "`basename ${0}`" -d"
	echo "  "`basename ${0}`" -e"
	echo "  "`basename ${0}`" -i -o hasp_rg=<rg>,hasp_rs=<rs>,[db_rg=<rg>][,db_rs=<rs>]"
	echo "      [,telemetry_rg=<rg>][,telemetry_rs=<rs>][,hasp_mnt_pt=<mnt_pt>]"
	echo "  "`basename ${0}`" -i -o hasp_mnt_pt=<mnt_pt>,hasp_nodelist=<node>[:<node]"
	echo "      [,hasp_rs=<rs>][,db_rg=<rg>][,db_rs=<rs>]"
	echo "      [,telemetry_rg=<rg>][,telemetry_rs=<rs>]"
	echo "  "`basename ${0}`" -u"
}

function verb_print
{
	[[ -n ${vflag} ]] && printf "$@"
}

function error_print
{
	printf "$@"
}

# Execute a command, place its output in OUTPUT and its return code in ERROR_CODE
function execute # [args ...]
{
	typeset args=$*

	OUTPUT=

	verb_print "${args}\n"

	#
	# Execute the command, and place the command output (both stdin and
	# stderr in the global OUTPUT variable
	#
	OUTPUT=$(${args} 2>&1)
	ERROR_CODE=$?
}

# This func is to be used in conjunction with the function execute
function check_return_code_abort
{
	if [[ ${ERROR_CODE} -ne 0 ]]; then
		error_print "$@"
		error_print "${OUTPUT}\n"
		abort
	fi
}

# This func is to be used in conjunction with the function execute
function check_return_code_abort_noundo
{
	if [[ ${ERROR_CODE} -ne 0 ]]; then
		error_print "$@"
		error_print "${OUTPUT}\n"
		abort_noundo
	fi
}

# This func is to be used in conjunction with the function execute
function check_return_code_noop
{
	if [[ ${ERROR_CODE} -ne 0 ]]; then
		error_print "$@"
		error_print "${OUTPUT}\n"
	fi
}

# SCHA calls do not output error messages. This func places an error message
# in OUTPUT based on the return code in ERROR_CODE
function _set_scha_error_message
{
	#
	# SCHA error codes are public and available in scha_calls(3HA)
	#

	case ${ERROR_CODE} in
	1)	# SCHA_ERR_NOMEM
		OUTPUT="$(gettext 'scha call failed; Not enough swap')"
		;;
	2)	# SCHA_ERR_HANDLE
		OUTPUT="$(gettext 'scha call failed; Invalid resource management handle')"
		;;
	3)	# SCHA_ERR_INVAL
		OUTPUT="$(gettext 'scha call failed; Invalid input argument')"
		;;
	4)	# SCHA_ERR_TAG
		OUTPUT="$(gettext 'scha call failed; Invalid API tag')"
		;;
	5)	# SCHA_ERR_RECONF
		OUTPUT="$(gettext 'scha call failed; Cluster is reconfiguring')"
		;;
	6)	# SCHA_ERR_ACCESS
		OUTPUT="$(gettext 'scha call failed; Permission denied')"
		;;
	7)	# SCHA_ERR_SEQID
		OUTPUT="$(gettext 'scha call failed; Resource, resource group, or resource type has been updated since last scha_*_open call')"
		;;
	8)	# SCHA_ERR_DEPEND
		OUTPUT="$(gettext 'scha call failed; Object dependency problem')"
		;;
	9)	# SCHA_ERR_STATE
		OUTPUT="$(gettext 'scha call failed; Object is in wrong state')"
		;;
	10)	# SCHA_ERR_METHOD
		OUTPUT="$(gettext 'scha call failed; Invalid method')"
		;;
	11)	# SCHA_ERR_NODE
		OUTPUT="$(gettext 'scha call failed; Invalid node')"
		;;
	12)	# SCHA_ERR_RG
		OUTPUT="$(gettext 'scha call failed; Invalid resource group')"
		;;
	13)	# SCHA_ERR_RT
		OUTPUT="$(gettext 'scha call failed; Invalid resource type')"
		;;
	14)	# SCHA_ERR_RSRC
		OUTPUT="$(gettext 'scha call failed; Invalid resource')"
		;;
	15)	# SCHA_ERR_PROP
		OUTPUT="$(gettext 'scha call failed; Invalid property')"
		;;
	16)	# SCHA_ERR_CHECKS
		OUTPUT="$(gettext 'scha call failed; Sanity checks failed')"
		;;
	17)	# SCHA_ERR_RSTATUS
		OUTPUT="$(gettext 'scha call failed; Bad resource status')"
		;;
	18)	# SCHA_ERR_INTERNAL
		OUTPUT="$(gettext 'scha call failed; Internal error was encountered')"
		;;
	31)	# SCHA_ERR_TIMEOUT
		OUTPUT="$(gettext 'scha call failed; Operation timed out')"
		;;
	32)	# SCHA_ERR_FAIL
		OUTPUT="$(gettext 'scha call failed; Failover attempt failed')"
		;;
	*)
		OUTPUT="$(gettext 'scha call failed; Unknown reason')"
	esac
}

# This func is to be used in conjunction with the function execute
function check_scha_return_code_abort
{
	if [[ ${ERROR_CODE} -eq 0 ]]; then
		# SCHA_ERR_NOERR
		return
	fi

	_set_scha_error_message # set OUTPUT

	error_print "$@"
	error_print "${OUTPUT}\n"

	abort
}

# This func is to be used in conjunction with the function execute
function check_scha_return_code_abort_noundo
{
	if [[ ${ERROR_CODE} -eq 0 ]]; then
		# SCHA_ERR_NOERR
		return
	fi

	_set_scha_error_message # set OUTPUT

	error_print "$@"
	error_print "${OUTPUT}\n"

	abort_noundo
}

function check_package
{
	typeset pkg=${1}

	verb_print "\nCheck that package %s is installed\n" "${pkg}"

	execute ${PKGINFO} ${pkg}
	check_return_code_abort "$(gettext 'Error: package \"%s\" is required; install it')\n" "${pkg}"
}


# exit 1 must not be used in the code, abort_undo nust be used instead
function abort_noundo
{
	verb_print "\nAbort\n"

	exit 1
}

function abort
{

	#
	# The functions called in here (disable_r, delete_r, delete_rg,
	# unregister_rt, destroy_sensors_ccr_table) must not call abort to avoid
	# recursion. They must not call check_return_code_abort either as
	# check_return_code_abort calls abort. Instead, they can call abort_noundo,
	# check_return_code_abort_noundo, and check_return_code_noop.
        #

	if [[ -n $iflag ]]
	then
		verb_print "\nUndoing...\n"

		execute ${SCHA_CLUSTER_GET} -O All_ResourceGroups
		check_scha_return_code_abort_noundo "$(gettext 'Error: cannot undo')\n"

		typeset rg_list=
		typeset all_rg_list="${OUTPUT}"

		for rg1 in ${RG_CREATED}; do
			for rg2 in ${all_rg_list}; do
				if [[ ${rg1} = ${rg2} ]]; then
					rg_list="${rg_list} ${rg1}"
				fi
			done
		done

		for rg in ${rg_list}; do
			execute ${SCHA_RESOURCEGROUP_GET} -G ${rg} -O Resource_list
			check_scha_return_code_abort_noundo "$(gettext 'Error: cannot undo')\n"

			typeset r_list="${OUTPUT}"

			for r in ${r_list}; do
				disable_r ${r}
				delete_r ${r}
			done
			delete_rg ${rg}
		done

		execute ${SCHA_CLUSTER_GET} -O All_ResourceTypes
		check_scha_return_code_abort_noundo "$(gettext 'Error: cannot undo')\n"

		typeset rt_list=
		typeset all_rt_list="${OUTPUT}"

		for rt1 in ${RT_REGISTERED}; do
			for rt2 in ${all_rt_list}; do
				if [[ ${rt1} = ${rt2} ]]; then
					rt_list="${rt_list} ${rt1}"
				fi
			done
		done

		for rt in ${rt_list}; do
			unregister_rt ${rt}
		done

		# if we abort as a result of SIGINT or SIGHUP at CCR table creation
		# time, and if the creation failed, we're going to attempt destroying
		# the table while it's not created. This is a problem, but we can't
		# fix it until we have a command to check if the table exists

		if [[ -n ${SENSORS_CCR_TABLE_CREATED} ]]; then
			destroy_sensors_ccr_table
		fi
	fi

	abort_noundo
}

#
# SIGINT and SIGHUP handler.
#
# If SIGINT or SIGHUP is received during -i, everything that's been done by the
# script is reverted, if it's received during -u, -e or -d: the script ignores
# the signal
#
# Important note: for -u, -e or -d, if the signal is received while an
# underlying SC command is being executed, that command is also signalled. As a
# result, the command may immediately finish. Thus, there's no guarantee that
# the action (unistall, enable or disable) has successfully completed. If the
# action did not effectively complete, the overall state of Ganymede RGs is
# still manageable by the script; in particular doing the same action again will
# work as expected.
#
function signal_handler
{
	verb_print "SIGNAL received\n"

	#
	# wait for child to exit
	#
	# note: we also wait for child in the case of -u, -e or -d, because
	# it seems that the OS makes the process go out of wait() after the
	# the signal has been handled. This causes problems as SC commands
	# can thus step on one another.
	#
	while true ; do
		typeset parent=

		for p in $(${PTREE} $$ | ${NAWK} '{print $1}'); do

			parent=$(ps -o ppid="" -p ${p})

			if [[ ${parent} -eq $$ ]]; then
				break
			fi
		done

		if [[ -z ${parent} ]]; then
			# ok
			break;
		fi

		verb_print "Waiting for child to exit\n"
		${SLEEP} 1
	done

	# abort only on -i
	if [[ -n ${iflag} ]]; then
		abort
	fi
}

#
# Resource Type handling functions
#

function register_rt # [args ...]
{
	typeset RT=${1}

	verb_print "\nRegistering resource type ${RT}\n"

	execute ${SCHA_CLUSTER_GET} -O ALL_RESOURCETYPES
	check_scha_return_code_abort "$(gettext 'Error: registering resource type \"%s\" failed')\n" "${RT}"

	echo "${OUTPUT}" | ${GREP} ${RT} > /dev/null 2>&1
	if [[ $? -eq 0 ]]; then
		verb_print "${RT} already registered, continue\n"
		return
	fi

	# we add the RT to be registered to the RT_REGISTERED list before
	# doing the actual regsitration because, in the case where we trap
	# a SIGHUP or SIGINT while the RT is being registered, we want to
	# be able to unregister that RT in the signal handler (in function
	# abort). In abort, not to attempt to unregister a non-registered
	# RT, we check that the RTs in the RT_REGISTERED list do exist.

	RT_REGISTERED="${RT} ${RT_REGISTERED}"
	execute ${SCRGADM} -a -t ${RT}
	check_return_code_abort "$(gettext 'Error: registering resource type \"%s\" failed')\n" "${RT}"
}

# This function must not call check_return_code_abort(), c.f. comment in abort()
function unregister_rt # [args ...]
{
	typeset RT=${1}

	verb_print "\nUnregistering resource type ${RT}\n"

	execute ${SCRGADM} -r -t ${RT}
	check_return_code_noop "$(gettext 'Note: unregistering resource type \"%s\" failed, continue')\n" "${RT}"
}


#
# Resource handling functions
#

function create_r  # [args ...] R RG RT R_properties
{
	typeset R=${1}
	shift
	typeset RG=${1}
	shift
	typeset RT=${1}
	shift
	typeset PROPERTIES=$*

	verb_print "\nCreating resource ${R}\n"

	execute ${SCRGADM} -a -j ${R} -g ${RG} -t ${RT} ${PROPERTIES}
	check_return_code_abort "$(gettext 'Error: creating resource \"%s\" failed')\n" "${R}"
}

# This function must not call abort() nor check_return_code_abort(), c.f. comment in abort()
function check_stop_failed_r # [args ...] RG R
{
	typeset RG=${1}
	typeset R=${2}

	execute ${SCHA_RESOURCEGROUP_GET} -O NodeList -G ${RG}
	check_scha_return_code_abort_noundo "$(gettext 'Error: checking state of resource \"%s\" failed')\n" "${R}"
	typeset nodelist="${OUTPUT}"

	if [[ -z ${nodelist} ]]; then # can this happen? let's be paranoid
		error_print "$(gettext 'Error: cannot retrieve the node list of resource group \"%s\"')\n" "${RG}"
		abort_noundo
	fi

	typeset notcleared=1
	for node in ${nodelist}; do

		execute ${SCHA_RESOURCE_GET} -O Resource_State_Node -R ${R} ${node}
		check_scha_return_code_abort_noundo "$(gettext 'Error: checking state of resource \"%s\" failed')\n" "${R}"
		state="${OUTPUT}"

		if [[ ${state} = "STOP_FAILED" ]]; then

			verb_print "\nClearing STOP_FAILED state of resource ${R} on node ${node}\n"

			execute ${SCSWITCH} -c -h ${node} -j ${R} -f STOP_FAILED
			check_return_code_abort_noundo "$(gettext 'Error: clearing STOP_FAILED state of resource \"%s\" on node \"%s\" failed')\n" "${R}" "${node}"

			notcleared=0
		fi
	done

	return ${notcleared}
}

# This function must not call abort() nor check_return_code_abort(), c.f. comment in abort()
function delete_r # [args ...] R
{
	typeset R=${1}

	verb_print "\nDeleting resource ${R}\n"

	execute ${SCRGADM} -r -j ${R}

	if [[ ${ERROR_CODE} -eq 0 ]]; then
		# everything went fine, return
		return
	fi

	#
	# Deleting ${R} failed
	#

	typeset scrgadm_output="${OUTPUT}"

	execute ${SCHA_RESOURCE_GET} -O Group -R ${R}
	check_scha_return_code_abort_noundo "$(gettext 'Error: deleting resource \"%s\" failed')\n" "${R}"
	typeset RG="${OUTPUT}"

	if [[ -z ${RG} ]]; then # can this happen? let's be paranoid...
		error_print "$(gettext 'Error: deleting resource \"%s\" failed; cannot retrieve its resource group')" "${R}"
		abort_noundo
	fi

	check_stop_failed_r ${RG} ${R}
	if [[ $? -ne 0 ]]; then
		# ${R} isn't in STOP_FAILED state on any node
		error_print "$(gettext 'Error: deleting resource \"%s\" failed')\n" "${R}"
		error_print "${scrgadm_output}"
		abort_noundo
	fi

	# ${R} was in STOP_FAILED state on some node(s)

	delete_r ${R}
}

# This function must not call abort() nor check_return_code_abort(), c.f. comment in abort()
function disable_r # [args ...] R
{
	typeset R=${1}

	verb_print "\nDisabling resource ${R}\n"

	execute ${SCSWITCH} -n -j ${R}

	if [[ ${ERROR_CODE} -eq 0 ]]; then
		# everything went fine, return
		return
	fi

	#
	# Disabling ${R} failed
	#

	typeset scswitch_output="${OUTPUT}"

	execute ${SCHA_RESOURCE_GET} -O Group -R ${R}
	check_scha_return_code_abort_noundo "$(gettext 'Error: disabling resource \"%s\" failed')\n" "${R}"
	typeset RG="${OUTPUT}"

	if [[ -z ${RG} ]]; then # can this happen? let's be paranoid...
		error_print "$(gettext 'Error: disabling \"%s\" failed; cannot retrieve its resource group')\n" "${R}"
		abort_noundo
	fi

	check_stop_failed_r ${RG} ${R}
	if [[ $? -ne 0 ]]; then
		# R isn't in STOP_FAILED state on any node
		error_print "$(gettext 'Error: disabling resource \"%s\" failed')\n" "${R}"
		error_print "${scswitch_output}"
		abort_noundo
	fi

	# ${R} was in STOP_FAILED state on some node(s)

	disable_r ${R}
}

function enable_r # [args ...] R
{
	typeset R=${1}

	verb_print "\nEnabling resource ${R}\n"

	execute ${SCSWITCH} -e -j ${R}
	check_return_code_abort "$(gettext 'Error: enabling resource \"%s\" failed')\n" "${R}"
}


#
# Resource Group handling functions
#

function create_rg # [args ...] RG RG_properties
{
	typeset RG=${1}
	shift
	typeset PROPERTIES=$*

	verb_print "\nCreating resource group ${RG}\n"

	# we add the RG to be created to the RG_CREATED list before doing
	# the actual creation because, in the case where we trap a SIGHUP
	# or SIGINT while the RG is being created, we want to be able to
	# destroy that RG in the signal handler (in function abort). In
	# abort, not to attempt to destroy a non-created RG, we check
	# that the RGs in the RG_CREATED list do exist.

	RG_CREATED="${RG} ${RG_CREATED}"
	execute ${SCRGADM} -a -g ${RG} ${PROPERTIES}
	check_return_code_abort "$(gettext 'Error: creating resource group \"%s\" failed')\n" "${RG}"
}

# This function must not call abort() nor check_return_code(), c.f. comment in abort()
function check_stop_failed_rg # [args ...] RG
{
	typeset RG=${1}

	execute ${SCHA_RESOURCEGROUP_GET} -O Resource_list -G ${RG}
	check_scha_return_code_abort_noundo "$(gettext 'Error: checking state of resource group \"%s\" failed')\n" "${RG}"
	resource_list="${OUTPUT}"

	if [[ -z ${resource_list} ]]; then # can this happen? let's be paranoid
		error_print "$(gettext 'Error: cannot retrieve the resource list of resource group \"%s\"')\n" "${RG}"
		abort_noundo
	fi

	typeset notcleared=1
	for res in ${resource_list}; do
		check_stop_failed_r ${RG} ${res}
		if [[ $? -eq 0 ]]; then
			notcleared=0
		fi
	done

	return ${notcleared}
}

# This function must not call abort() nor check_return_code_abort(), c.f. comment in abort()
function delete_rg # [args ...] RG
{
	typeset RG=${1}

	verb_print "\nDeleting resource group ${RG}\n"

	execute ${SCRGADM} -r -g ${RG}

	if [[ ${ERROR_CODE} -eq 0 ]]; then
		# everything went fine, return
		return
	fi

	#
	# Deleting ${RG} failed
	#

	typeset scrgadm_output="${OUTPUT}"

	check_stop_failed_rg ${RG}
	if [[ $? -ne 0 ]]; then
		# ${RG} isn't in ERROR_STOP_FAILED state on any node
		error_print "$(gettext 'Error: deleting resource group \"%s\" failed')\n" "${RG}"
		error_print "${scrgadm_output}"
		abort_noundo
	fi

	# ${RG} was in ERROR_STOP_FAILED state on some node(s)

	delete_rg ${RG}
}

function create_rg_dependency # [args ...] RG1 RG2
{
	typeset RG1=${1}
	typeset RG2=${2}

	verb_print "\nCreating RG dependency between ${RG1} and ${RG2}\n"

	execute ${SCRGADM} -c -g ${RG1} -yRG_dependencies=${RG2}
	check_return_code_abort "$(gettext 'Error: creating resource group dependency between \"%s\" and \"%s\" failed')\n" "${RG1}" "${RG2}"
}

function create_strong_affinity # [args ...] RG1 RG2
{
	typeset RG1=${1}
	typeset RG2=${2}

	verb_print "\nCreating strong affinity of ${RG1} for ${RG2}\n"

	execute ${SCRGADM} -c -g ${RG1} -yRG_affinities="+++${RG2}"
	check_return_code_abort "$(gettext 'Error: creating strong affinity of \"%s\" to \"%s\" failed')\n" "${RG1}" "${RG2}"
}

function bring_online # [args ...] RG
{
	typeset RG=${1}

	verb_print "\nBringing ${RG} online\n"

	execute ${SCSWITCH} -Z -g ${RG}
	check_return_code_abort "$(gettext 'Error: bringing resource group \"%s\" online failed')\n" "${RG}"
}

# This function waits for an RG to be ONLINE on the node
# on which the this script is called
# This is a best effort only, the function exits silently
# in case of failure

function wait_if_pending_online # [args ...] RG times
{
	typeset rg=${1}
	typeset times=${2}

	execute ${SCHA_RESOURCEGROUP_GET} -O NODELIST -G ${rg}
	if [[ ${ERROR_CODE} -ne 0 ]]; then
		# Best effort only, we exit if this fails
		return;
	fi

	typeset nodelist=${OUTPUT}

	for node in ${nodelist}
	do
	    verb_print "\nChecking ${rg} state on node ${node}\n"
	    while [[ ${times} -gt 0 ]]; do
		execute ${SCHA_RESOURCEGROUP_GET} -O RG_STATE_NODE -G ${rg} ${node}
		if [[ ${ERROR_CODE} -ne 0 ]]; then
		    # Best effort only, we exit if this fails
		    break;
		elif [[ ${OUTPUT} = "PENDING_ONLINE" ]]; then
		    # We wait only if STATE is PENDING_ONLINE on the local node
		    ${SLEEP} 1
		else
		    break
		fi
		times=`expr ${times} - 1`
	    done
	done
}

# This function must not call abort() nor check_return_code_abort(), c.f. comment in abort()
function bring_offline # [args ...] RG
{
	typeset RG=${1}

	verb_print "\nBringing ${RG} offline\n"

	execute ${SCSWITCH} -F -g ${RG}

	if [[ ${ERROR_CODE} -eq 0 ]]; then
		# everything went fine, return
		return
	fi

	#
	# Bringing ${RG} offline failed
	#

	typeset scswitch_output="${OUTPUT}"

	check_stop_failed_rg ${RG}
	if [[ $? -ne 0 ]]; then
		# ${RG} isn't in ERROR_STOP_FAILED state on any node
		error_print "$(gettext 'Error: bringing resource group \"%s\" offline failed')\n" "${RG}"
		error_print "${scswitch_output}"
		abort_noundo
	fi

	# ${RG} was in ERROR_STOP_FAILED state on some node(s)

	bring_offline ${RG}
}

function ij # [derbyrootdir] [url] [sqlschemafile] [ijlogfile] [derbylogfile]
{
    typeset derbydir=${1}
    typeset url=${2}
    typeset sqlschema=${3}
    typeset ijlogfile=${4}
    typeset derbylogfile=${5}

    typeset __CLASSPATH=${derbydir}/derby.jar
	__CLASSPATH=${__CLASSPATH}:${derbydir}/derbyclient.jar
	__CLASSPATH=${__CLASSPATH}:${derbydir}/derbynet.jar
	__CLASSPATH=${__CLASSPATH}:${derbydir}/derbytools.jar

    # derby.stream.error.file is used to set the name of a logfile
    # created by the org.apache.derby.tools.ij class
    # By default the file is name "derby.log" in the current directory
    # which is not acceptable, so we rename it.
    # The content of the file is empty.

    ${JAVA} -cp ${__CLASSPATH} \
	-Dderby.stream.error.file=${derbylogfile} \
	-Dij.driver=org.apache.derby.jdbc.ClientDriver \
	-Dij.database="${url};create=true" \
	org.apache.derby.tools.ij ${sqlschema} >> ${ijlogfile}
}

function create_db_schema # [args ...]
{
	# retrieve the hostname on which "db_rg" is online
	typeset dbhost="$(
		LC_ALL=C; export LC_ALL;
		${SCSTAT} -g | ${GREP} ${db_rg} | ${GREP} Online | ${NAWK} '{ print $3 }'
	)"
	if [[ -z ${dbhost} ]]; then
		error_print "$(gettext 'Error: cannot retrieve the name of the cluster node hosting the database on which system resource monitoring relies')\n"
		abort
	fi

	# find the privatelink hostname coresponding to the node where "db_rg" is online
	execute ${SCHA_CLUSTER_GET} -O PRIVATELINK_HOSTNAME_NODE ${dbhost}
	check_scha_return_code_abort "$(gettext 'Error: creating DB schema failed')\n"
	typeset host="${OUTPUT}"

	if [[ -z ${host} ]]; then # can this happen? let's be paranoid
		error_print "$(gettext 'Error: cannot retrieve the private hostname the cluster node hosting the database on which system resource monitoring relies')\n"
		abort
	fi

	execute ${SCHA_RESOURCE_GET} -O EXTENSION -R ${db_rs} DB_PORT
	check_scha_return_code_abort "$(gettext 'Error: creating DB schema failed')\n"
	typeset port=`echo ${OUTPUT} | ${NAWK} '{print $2}'`

	if [[ -z ${port} ]]; then # can this happen? let's be paranoid
		error_print "$(gettext 'Error: cannot retrieve the port number of the database on which system resource monitoring relies')\n"
		abort
	fi

	typeset URL=`echo ${url_format} | ${SED} s/%HOST%/${host}/ | ${SED} s/%PORT%/${port}/`

	verb_print "Database JDBC URL: ${URL}\n"

	ij ${DERBYROOTDIR} ${URL} ${SQLSCHEMAFILE} ${IJLOGFILE} ${DERBYLOGFILE}

	typeset ERRONEOUS=`${GREP} Exception ${IJLOGFILE} | ${WC} -l`

	if [[ ${ERRONEOUS} -ne 0 ]]; then
		error_print "$(gettext 'Error: database schema creation failed')\n"
		error_print "$(gettext 'See logfiles \"%s\" and \"%s\" for details')\n" "${IJLOGFILE}" "${DERBYLOGFILE}"
		abort
	fi

	# Delete log file as there was no error
	${RM} ${IJLOGFILE}
	${RM} ${DERBYLOGFILE}
	verb_print "Database schema creation done\n"
}

function create_sensors_ccr_table
{
	verb_print "\nCreating Sensors CCR table\n"

	# don't abort in case of error here, not to undo the R/RG
	# creation/config

	if [[ ! -x ${SCSLMCCR} ]]; then
		error_print "$(gettext 'Error: failed to create the CCR table for the telemetry attributes; the command \"%s\" either does not exist or is not executable')\n" "${SCSLMCCR}"
		abort
	fi

	# we set SENSORS_CCR_TABLE_CREATED before doing the actual
        # creation to be able to destroy the table should a signal
	# occurs while we're creating the table.

	SENSORS_CCR_TABLE_CREATED=1
	execute ${SCSLMCCR} -c sensors
	if [[ ${ERROR_CODE} -ne 0 ]]; then
		error_print "$(gettext 'Error: failed to create the CCR table for the telemetry attributes')\n"
		error_print "${OUTPUT}"
		abort
	fi

}

# This function must not call abort() nor check_return_code_abort(), c.f. comment in abort()
function destroy_sensors_ccr_table
{
	verb_print "\nDestroying Sensors CCR table\n"

	if [[ ! -x ${SCSLMCCR} ]]; then
		error_print "$(gettext 'Error: failed to destroy the CCR table for the telemetry attributes; the command \"%s\" either does not exist or is not executable')\n" "${SCSLMCCR}"
		abort_noundo
	fi

	execute ${SCSLMCCR} -d sensors
	check_return_code_abort_noundo "$(gettext 'Error: failed to destroy the CCR table for the telemetry attributes')\n"
}

# This function must not call abort() nor check_return_code_abort(), c.f. comment in abort()
function destroy_thresholds_ccr_table
{
	verb_print "\nDestroying Threshold CCR table\n"

	if [[ ! -x ${SCSLMCCR} ]]; then
		error_print "$(gettext 'Error: failed to destroy the CCR table for the thresholds; the command \"%s\" either does not exist or is not executable')\n" "${SCSLMCCR}"
		abort_noundo
	fi

	execute ${SCSLMCCR} -d thresholds
	check_return_code_abort_noundo "$(gettext 'Error: failed to destroy the CCR table for the thresholds')\n"
}

function is_telemetry_configured
{
	verb_print "\nCheck whether resource monitoring is already configured\n"

	execute ${SCHA_CLUSTER_GET} -O All_ResourceGroups
	check_scha_return_code_abort_noundo "$(gettext 'Error: cannot get the list of resource groups configured on the cluster')\n"
	typeset all_rg_list="${OUTPUT}"

	for rg in ${all_rg_list}; do
		execute ${SCHA_RESOURCEGROUP_GET} -O Resource_list -G ${rg}
		check_scha_return_code_abort_noundo "$(gettext 'Error: cannot get the list of resources of resource group \"%s\"')\n" "${rg}"
		typeset resource_list="${OUTPUT}"
		for rs in ${resource_list}; do
			execute ${SCHA_RESOURCE_GET} -O Type -R ${rs}
			check_scha_return_code_abort_noundo "$(gettext 'Error: cannot get the type of resource \"%s\"')\n" "${rs}"
			typeset type="${OUTPUT}"
			if  [[ ${type} = ${telemetry_rt} ]] || [[ ${type} = ${telemetry_rt_versioned} ]]; then
				return 1
			fi
		done
	done

	return 0
}

function get_resource_resourcegroup_names
{
	verb_print "\nGet telemetry_rs, telemetry_rg, db_rs, and db_rg\n"

	execute ${SCHA_CLUSTER_GET} -O All_ResourceGroups
	check_scha_return_code_abort "$(gettext 'Error: cannot get the list of resource groups configured on the cluster')\n"
	typeset rg_list="${OUTPUT}"
	if [[ -z ${rg_list} ]]; then
		error_print "$(gettext 'Error: no resource groups found; the configuration for system resource monitoring might have been removed')\n"
		abort
	fi

	#
	# Get telemetry resource and resource group names
	#

	telemetry_rs=
	telemetry_rg=

	for rg in ${rg_list}; do
		execute ${SCHA_RESOURCEGROUP_GET} -O Resource_list -G ${rg}
		check_scha_return_code_abort "$(gettext 'Error: cannot get the list of resources of resource group \"%s\"')\n" "${rg}"
		typeset resource_list="${OUTPUT}"
		for rs in ${resource_list}; do
			execute ${SCHA_RESOURCE_GET} -O Type -R ${rs}
			check_scha_return_code_abort "$(gettext 'Error: cannot get the type of resource \"%s\"')\n" "${rs}"
			typeset type="${OUTPUT}"
			if [[ ${type} = ${telemetry_rt} ]] || [[ ${type} = ${telemetry_rt_versioned} ]]; then
				telemetry_rg=${rg}
				telemetry_rs=${rs}
			fi
		done
	done

	# can't do anything if no resource of type telemetry_rt could be found
	if [[ -z ${telemetry_rs} ]]; then
		error_print "$(gettext 'Error: cannot retrieve any \"%s\" resource; the configuration for system resource monitoring might have been removed')\n" "${telemetry_rt}"
		abort
	fi

	#
	# Get database resource and resource group names
	#

	db_rs=
	db_rg=

	execute ${SCHA_RESOURCEGROUP_GET} -O RG_DEPENDENCIES -G ${telemetry_rg}
	check_scha_return_code_abort "$(gettext 'Error: cannot get the dependencies of resource group \"%s\"')\n" "${telemetry_rg}"
	typeset rg_dependencies="${OUTPUT}"

	for rg in ${rg_dependencies}; do
		execute ${SCHA_RESOURCEGROUP_GET} -O Resource_list -G ${rg}
		check_scha_return_code_abort "$(gettext 'Error: cannot get the list of resources of resource group \"%s\"')\n" "${rg}"
		typeset resource_list="${OUTPUT}"
		for rs in ${resource_list}; do
			execute ${SCHA_RESOURCE_GET} -O Type -R ${rs}
			check_scha_return_code_abort "$(gettext 'Error: cannot get the type of resource \"%s\"')\n" "${rs}"
			typeset type="${OUTPUT}"
			if [[ ${type} = ${db_rt} ]] || [[ ${type} = ${db_rt_versioned} ]]; then
				db_rg=${rg}
				db_rs=${rs}
			fi
		done
	done

	if [[ -z ${db_rs} ]]; then
		error_print "$(gettext 'Error: \"%s\" does not have any dependency containing a \"%s\" resource')\n" "${telemetry_rg}" "${db_rt}"
		abort
	fi
}

#
#
# Main
#
#

if [[ $# -le 0 ]]
then
	usage
	abort_noundo
fi

# zone privilege check
${SC_ZONESCHECK}
if [[ $? -ne 0 ]]; then
	error_print "$(gettext 'Error: execution out of global zone not allowed')\n"
	abort_noundo
fi

# Paths definition
typeset -r DERBYROOTDIR="/opt/SUNWjavadb/lib"
typeset -r SQLSCHEMAFILE="/usr/cluster/lib/rgm/rt/sctelemetry/Database.Derby.sql"
typeset -r IJLOGFILE="/tmp/sctelemetry_loadsql.$$"
typeset -r DERBYLOGFILE="/tmp/sctelemetry_derby.$$"


# Commands definition
typeset -r RM="/bin/rm"
typeset -r TEE="/usr/bin/tee"
typeset PKGINFO="/usr/bin/pkginfo"
if [[ -f "/var/cluster/ips/.ips" ]]; then
	PKGINFO="/usr/bin/pkg info"
fi
typeset -r GREP="/usr/bin/grep"
typeset -r WC="/usr/bin/wc"
typeset -r SED="/usr/bin/sed"
typeset -r SLEEP="/usr/bin/sleep"
typeset -r NAWK="/usr/bin/nawk"
typeset -r PTREE="/usr/bin/ptree"
typeset -r JAVA="/usr/bin/java"
typeset -r SCRGADM="/usr/cluster/bin/scrgadm"
typeset -r SCSWITCH="/usr/cluster/bin/scswitch"
typeset -r SCSTAT="/usr/cluster/bin/scstat"
typeset -r SCSLMCCR="/usr/cluster/lib/sc/scslm_ccr"
typeset -r SCHA_CLUSTER_GET="/usr/cluster/bin/scha_cluster_get"
typeset -r SCHA_RESOURCE_GET="/usr/cluster/bin/scha_resource_get"
typeset -r SCHA_RESOURCEGROUP_GET="/usr/cluster/bin/scha_resourcegroup_get"
typeset -r SC_ZONESCHECK="/usr/cluster/lib/sc/sc_zonescheck"

typeset RT_REGISTERED=
typeset RG_CREATED=
typeset SENSORS_CCR_TABLE_CREATED=

typeset OUTPUT=		# output (stdin and stdout) of the underlying command
typeset ERROR_CODE=	# error code of the underlying command

typeset -r possible_keys="hasp_rg hasp_rs hasp_mnt_pt hasp_nodelist \
	       db_rg db_rs \
	       telemetry_rg telemetry_rs"
typeset iflag=
typeset uflag=
typeset eflag=
typeset dflag=
typeset vflag= # undocumented flag
typeset oflag=

# HAStorage+
typeset -r hasp_rt="SUNW.HAStoragePlus"
typeset _hasp_rs="cl-dbstrg-rs"		# default database storage resource name
typeset hasp_rg=
typeset hasp_mnt_pt=
typeset hasp_nodelist=

# Database
typeset -r db_rt="SUNW.derby"
typeset -r db_rt_versioned="${db_rt}:1"
typeset -r url_format="jdbc:derby://%HOST%:%PORT%/APP"
typeset db_rs="cl-db-rs"		# default database resource name
typeset db_rg="cl-db-rg"		# default database resource group name
typeset db_rs_properties=
typeset db_rg_properties="-yRG_SLM_TYPE=automated -yImplicit_network_dependencies=false"

# Telemetry
typeset -r telemetry_rt="SUNW.sctelemetry"
typeset -r telemetry_rt_versioned="${telemetry_rt}:1"
typeset telemetry_rs="cl-tlmtry-rs"	# default telemetry resource name
typeset telemetry_rg="cl-tlmtry-rg"	# default telemetry resource group name
typeset telemetry_rg_properties=	# set at install time

#
# Parsing and Checking of options
#

while getopts iuedvo: name 2> /dev/null
do
	case $name in
	i)	# install
		if [[ -n $iflag ]]; then
			usage
			abort_noundo
		fi
		iflag=1
		;;
	u)	# uninstall
		if [[ -n $uflag ]]; then
			usage
			abort_noundo
		fi
		uflag=1
		;;
	e)	# enable
		if [[ -n $eflag ]]; then
			usage
			abort_noundo
		fi
		eflag=1
		;;
	d)	# disable
		if [[ -n $dflag ]]; then
			usage
			abort_noundo
		fi
		dflag=1
		;;
	v)	# verbose (undocumented flag)
		if [[ -n $vflag ]]; then
			usage
			abort_noundo
		fi
		vflag=1
		;;
	o)	# sub-options for -i
		if [[ -n $oflag ]]; then
			usage
			abort_noundo
		fi
		oflag=${OPTARG}
		;;
	\?)
		usage
		abort_noundo
		;;
	esac
done

# one of the -i, -u, -e, -d options must be used
if [[ -z $iflag ]] && [[ -z $uflag ]] && [[ -z $eflag ]] && [[ -z $dflag ]]
then
	usage
	abort_noundo
fi

# options -i, -u, -e, -d are exclusive
if [[ -n $iflag ]] && ([[ -n $uflag ]] || [[ -n $eflag ]] || [[ -n $dflag ]])
then
	usage
	abort_noundo
fi

if [[ -n $uflag ]] && ([[ -n $eflag ]] || [[ -n $dflag ]] || [[ -n $iflag ]])
then
	usage
	abort_noundo
fi

if [[ -n $eflag ]] && ([[ -n $dflag ]] || [[ -n $uflag ]] || [[ -n $iflag ]])
then
	usage
	abort_noundo
fi

if [[ -n $dflag ]] && ([[ -n $iflag ]] || [[ -n $uflag ]] || [[ -n $eflag ]])
then
	usage
	abort_noundo
fi

# option -o cannot be used with something else than -i
if [[ -n $oflag ]] && [[ -z $iflag ]]
then
	usage
	abort_noundo
fi

# arrange for abort on SIGHUP or SIGINT
trap 'signal_handler' HUP INT

if [[ -n ${iflag} ]]; then

	#
	#
	# Install
	#
	#

	if [[ -z ${oflag} ]]; then
		usage
		abort_noundo
	fi


	#
	# Check that telemetry isn't already online
	#

	is_telemetry_configured
	if [[ $? -eq 1 ]]; then
		error_print "$(gettext 'Error: system resource monitoring is already configured')\n"
		abort
	fi

	#
	# Read the key/value pairs after -o
	#

	typeset option_line=${oflag}

	typeset options="$(echo ${option_line} | ${NAWK} -F ',' '{
			for (i = 1; i <= NF; i++) {
				print $i
			}
		}'
	)"


	for opt in ${options}; do
		typeset key="$(echo ${opt} | ${NAWK} -F '=' '{
				print $1
			}'
		)"
		typeset val="$(echo ${opt} | ${NAWK} -F '=' '{
				print $2
			}'
		)"

		typeset gotit=0
		for k in ${possible_keys}; do
			if [[ ${key} = ${k} ]]; then
				typeset ${key}=${val}
				gotit=1
				break
			fi
		done

		if [[ ${gotit} -eq 0 ]]; then
			error_print "$(gettext 'Error: option \"%s\" is unknown')\n" "${key}"
			abort
		fi
	done

	# one of hasp_rg and hasp_nodelist must be defined
	if [[ -z ${hasp_rg} ]] && [[ -z ${hasp_nodelist} ]]; then
		usage
		abort
	fi

	# hasp_rg and hasp_nodelist are exclusive
	if [[ -n ${hasp_rg} ]] && [[ -n ${hasp_nodelist} ]]; then
		usage
		abort
	fi
	if [[ -n ${hasp_nodelist} ]] && [[ -n ${hasp_rg} ]]; then
		usage
		abort
	fi

	# hasp_rg requires hasp_rs
	if [[ -n ${hasp_rg} ]] && [[ -z ${hasp_rs} ]]; then
		usage
		abort
	fi

	# hasp_nodelist requires hasp_mnt_pt
	if [[ -n ${hasp_nodelist} ]] && [[ -z ${hasp_mnt_pt} ]]; then
		usage
		abort
	fi

	if [[ -n ${hasp_nodelist} ]]; then
		hasp_nodelist="$(echo ${hasp_nodelist} | ${SED} 's/:/\,/g')"
		hasp_nodelist="$(echo ${hasp_nodelist} | ${NAWK} '{ print $1 }')"
	fi

	execute ${SCHA_CLUSTER_GET} -O ClusterName
	check_scha_return_code_abort "$(gettext 'Error: cannot retrieve cluster name')\n"
	typeset clname="${OUTPUT}"
	if [[ -z ${clname} ]]; then # can this happen? let's be paranoid
		error_print "$(gettext 'Error: cannot retrieve cluster name')\n"
		abort
	fi

	# check RG names aren't already taken
	verb_print "\nCheck RG names validity\n"

	execute ${SCHA_CLUSTER_GET} -O All_ResourceGroups
	check_scha_return_code_abort_noundo "$(gettext 'Error: cannot get the list of resource groups configured on the cluster')\n"
	typeset all_rg_list="${OUTPUT}"

	typeset rg_list="${db_rg} ${telemetry_rg}"
	for rg1 in ${all_rg_list}; do
		for rg2 in ${rg_list}; do
			if [[ ${rg2} = ${rg1} ]]; then
				error_print "$(gettext 'Error: resource group \"%s\" already exists')\n" "${rg1}"
				abort
			fi
		done
	done

	if [[ -f "/var/cluster/ips/.ips" ]]; then
		check_package SUNWjavadb
		check_package SUNWscderby
		check_package SUNWsctelemetry
	else
		check_package SUNWjavadb-core
		check_package SUNWjavadb-client
		check_package SUNWscderby
		check_package SUNWsctelemetry
	fi

	typeset dbroot=

	if [[ -n ${hasp_rg} ]]; then

		#
		# An HAStoragePlus Resource and its container RG are provided by
		# the user.
       		#
		create_rg ${db_rg} ${db_rg_properties}
		create_strong_affinity ${db_rg} ${hasp_rg}

		verb_print "\nGet the property NodeList of resource group %s\n" "${hasp_rg}"
		execute ${SCHA_RESOURCEGROUP_GET} -O Nodelist -G ${hasp_rg}
		check_scha_return_code_abort "$(gettext 'Error: cannot get the property Nodelist of resource group \"%s\"')\n" "${hasp_rg}"

		echo ${OUTPUT} | ${GREP} ':' > /dev/null 2>&1
		if [[ $? -eq 0 ]]; then
			error_print "$(gettext 'Error: System resource monitoring cannot rely on an HAStoragePlus resource (\"%s\") that can be hosted on non-global zones, provide an HAStoragePlus resource hosted on global zones only')\n" "${hasp_rs}"
			abort
		fi

		verb_print "\nGet the property FilesystemMountPoints of resource %s\n" "${hasp_rs}"
		execute ${SCHA_RESOURCE_GET} -O Extension -R ${hasp_rs} -G ${hasp_rg} FilesystemMountPoints
		check_scha_return_code_abort "$(gettext 'Error: cannot get the property FilesystemMountPoints of resource \"%s\"')\n" "${hasp_rs}"
		typeset mntpt=`echo ${OUTPUT} | ${NAWK} '{ for (i = 2; i <= NF; i++) { printf $i " " }}'`
		mntpt=`echo ${mntpt}` # remove trailing whitespace

		if [[ -z ${mntpt} ]]; then
			# This can happen if the HAStorage+ has been configured using the property GlobalDevicePaths as opposed
			# to FilesystemMountPoints. This is a limitation, we can only deal with HAStorage+ resources that have
			# been configured using FilesystemMountPoints.
			error_print "$(gettext 'Error: There is no value configured for the property FilesystemMountPoints for the resource named \"%s\", provide an HAStoragePlus resource with a valid FilesystemMountPoints property')\n" "${hasp_rs}"
			abort
		fi

		typeset -i nbmnt=`echo ${mntpt} | ${NAWK} '{ printf NF }'`

		if [[ ${nbmnt} -ne 1 ]]; then
			# There is more than one mount point configured, will see if
			# we can find the one provided by the user or return an error
			if [[ -z ${hasp_mnt_pt} ]]; then
				error_print "$(gettext 'Error: There is more than one value configured for the property FilesystemMountPoints for the resource named \"%s\", provide the one to use with the parameter \"hasp_mnt_pt\"')\n" "${hasp_rs}"
				abort
			else
				typeset tmpmntpt=""
				for item in ${mntpt}
				do
					if [[ "${hasp_mnt_pt}" = "${item}" ]]; then
						tmpmntpt=${hasp_mnt_pt}
					fi
				done

				if [[ -z "${tmpmntpt}" ]]; then
					error_print "$(gettext 'Error: The mountpoint \"%s\" is not part of the property FilesystemMountPoints for the resource named \"%s\", provide the right one to use with the parameter \"hasp_mnt_pt\"')\n" "${hasp_mnt_pt}" "${hasp_rs}"
					abort
				else
					mntpt=${tmpmntpt}
				fi
			fi
		else
			# There is only one mount point configured, will use this one
			# if the user did not provide any (Nothing to do in that case)


			# if the user provided one, verify that we found the same one
			if [[ -n ${hasp_mnt_pt} ]] && [[ "${hasp_mnt_pt}" != "${mntpt}" ]]; then
			    	error_print "$(gettext 'Error: The mountpoint \"%s\" is not part of the property FilesystemMountPoints for the resource named \"%s\", provide the right one to use with the parameter \"hasp_mnt_pt\"')\n" "${hasp_mnt_pt}" "${hasp_rs}"

				abort
			fi
		fi

		dbroot="${mntpt}/${clname}"
		db_rs_properties="-x DB_PATH=${dbroot} \
			-y Resource_dependencies=${hasp_rs}"

	else # if [[ -z ${hasp_rg} ]]; then

		#
		# No HAStoragePlus Resource provided by the user, we need to
		# create it ourself based on mount point and nodelist provided
		# by the user.
		#

		db_rg_properties="${db_rg_properties}"
		if [[ -n "${hasp_nodelist}" ]]; then
			db_rg_properties="${db_rg_properties} -yNodeList=${hasp_nodelist}"
		fi

		create_rg ${db_rg} ${db_rg_properties}

		register_rt ${hasp_rt}

		if [[ -z ${hasp_rs} ]]; then
			hasp_rs=${_hasp_rs}
		fi

		hasp_rs_properties=" \
			-x FilesystemMountPoints=${hasp_mnt_pt} \
			-x AffinityOn=TRUE"
		create_r ${hasp_rs} ${db_rg} ${hasp_rt} \
			${hasp_rs_properties}

		dbroot="${hasp_mnt_pt}/${clname}"
		db_rs_properties="-x DB_PATH=${dbroot} \
			-y Resource_dependencies=${hasp_rs}"
	fi

	#
	# Bringing db_rg online before creating db_rs is required because
	# of the dependency from db_rs to hasp_rs if HAStoragePlus is used.
        #
	bring_online ${db_rg}

	register_rt ${db_rt}
	create_r ${db_rs} ${db_rg} ${db_rt} ${db_rs_properties}


	#
	# Bring db_rg online again otherwise db_rs will remain offline
	#
	bring_online ${db_rg}

	#
	# Create DB schema
	#
	verb_print "\nCreating database schema\n"
	create_db_schema

	#
	# Create the sensors CCR Table
	#
	create_sensors_ccr_table

	#
	# Set the telemetry_rg properties
	#
	verb_print "\nDetermining the number of cluster nodes\n"
	execute ${SCHA_CLUSTER_GET} -O ALL_NODENAMES
	check_scha_return_code_abort_noundo "$(gettext 'Error: cannot determine the number of cluster nodes')\n"
	typeset nbnodes=`echo ${OUTPUT} | ${NAWK} '{ printf NF }'`
	if [[ -z ${nbnodes} ]]; then # can this happen, let's be paranoid
		error_print "$(gettext 'Error: cannot determine the number of cluster nodes')\n"
		abort
	fi

	telemetry_rg_properties="-yDesired_primaries=${nbnodes} -yMaximum_primaries=${nbnodes} \
	 -yImplicit_network_dependencies=false -yRG_SLM_TYPE=automated"

	#
	# bring telemetry_rg online
	#
	register_rt ${telemetry_rt}
	create_rg ${telemetry_rg} ${telemetry_rg_properties}
	create_r  ${telemetry_rs} ${telemetry_rg} ${telemetry_rt}

	create_rg_dependency ${telemetry_rg} ${db_rg}

	bring_online ${telemetry_rg}


elif [[ -n ${uflag} ]]; then

	#
	#
	# Uninstall
	#
	#

	#
	# Get the R and RG names (telemetry_rs, telemetry_rg, db_rs, and db_rg)
	#
	get_resource_resourcegroup_names

	#
	# Uninstall Telemetry
	#
	bring_offline ${telemetry_rg}
	disable_r ${telemetry_rs}
	delete_r ${telemetry_rs}
	unregister_rt ${telemetry_rt}
	delete_rg ${telemetry_rg}

	typeset db_path=
	if [[ -n ${db_rs} ]]; then
	        # Get DB PATH
		execute ${SCHA_RESOURCE_GET} -O EXTENSION -R ${db_rs} DB_PATH
		check_scha_return_code_abort "$(gettext 'Error: cannot get the property DB_PATH of resource \"%s\"')\n" "${db_rs}"
		db_path=`echo ${OUTPUT} | ${NAWK} '{print $2}'`
		if [[ -z ${db_path} ]]; then
			error_print "$(gettext 'Error: the property DB_PATH of resource \"%s\" is empty')\n" "${db_rs}"
			abort
		fi
	fi

	#
	# Uninstall Derby
        #
	bring_offline ${db_rg}

	verb_print "\nGet ${db_rg} resource list\n"
	execute ${SCHA_RESOURCEGROUP_GET} -O Resource_list -G ${db_rg}
	check_scha_return_code_abort "$(gettext 'Error: cannot get the list of resources of resource group \"%s\"')\n" "${db_rg}"
	typeset resource_list="${OUTPUT}"

	for rs in ${resource_list}; do
		execute ${SCHA_RESOURCE_GET} -O Type -R ${rs}
		check_scha_return_code_abort "$(gettext 'Error: cannot get the type of resource \"%s\"')\n" "${rs}"
		typeset type="${OUTPUT}"
		if [[ -z ${type} ]]; then # can this happen? let's be paranoid
			error_print "$(gettext 'Error: cannot get the type of resource \"%s\"')\n" "${rs}"
			abort
		fi
		disable_r ${rs}
		delete_r ${rs}
		unregister_rt ${type}
	done

	delete_rg ${db_rg}

	#
	# Destroy the Sensors and Threshold CCR tables
	#
	destroy_sensors_ccr_table
	destroy_thresholds_ccr_table

	# the following note isn't displayed if one of the CCR table destruction
	# operations failed; that's ok these operations should fail only if the
	# user himself removed the table before putting Ganymede offline
	if [[ -z ${db_path} ]]; then
		db_path="unknown"
	fi
	echo
	echo "Note: the database containing the monitoring data is not deleted (location: ${db_path})"


elif [[ -n ${eflag} ]]; then

	#
	#
	# Enable
	#
	#

	typeset nargs=1
	if [[ -n ${vflag} ]]; then
		nargs=2
	fi

	if [[ $# -ne ${nargs} ]]; then
		usage
		abort_noundo
	fi

	#
	# Get the R and RG names (telemetry_rs, telemetry_rg, db_rs, and db_rg)
	#
	get_resource_resourcegroup_names

	#
	# Bring db_rg online
	#
	# Note: with the telemetry-to-derby RG dependency, if both RGs
	# are offline then bringing derby RG online will automatically
	# bring telemetry RG online, derby RG being brought online
	# first. Therefore there's no need to explicitely bring
	# telemetry RG online here.
	#

	bring_online ${db_rg}

	#
	# Waits 50 seconds until the telemetry RG is brought online
	# through the dependency
	#
	wait_if_pending_online ${telemetry_rg} 50

elif [[ -n ${dflag} ]]; then

	#
	#
	# Disable
	#
	#

	typeset nargs=1
	if [[ -n ${vflag} ]]; then
		nargs=2
	fi

	if [[ $# -ne ${nargs} ]]; then
		usage
		abort_noundo
	fi

	#
	# Get the R and RG names (telemetry_rs, telemetry_rg, db_rs, and db_rg)
	#
	get_resource_resourcegroup_names

	#
	# Bring telemetry_rg and db_rg offline
	#
	bring_offline ${telemetry_rg}
	bring_offline ${db_rg}

else
	error_print "$(gettext 'Error: internal bug!')\n"
	abort
fi

#
# Do not add code here.
#

exit 0
