#!/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident	"@(#)sctelemetry_probe.ksh	1.7	08/05/20 SMI"

# Sun Cluster Data Services Builder template version 1.0
#
# Probe method for sctelemetry.
#
# This method checks the health of the data service.
###############################################################################

#include_lib

###############################################################################
# restart_service ()
#
# This function tries to restart the dataservice by calling the STOP method
# followed by the START method of the dataservice.
#
function restart_service
{

	# Obtain the STOP method name and the STOP_TIMEOUT value for
	# this resource.
	STOP_TIMEOUT=`${SCHA_RESOURCE_GET} -O STOP_TIMEOUT \
		-R $RESOURCE_NAME -G $RESOURCEGROUP_NAME`
	STOP_METHOD=`${SCHA_RESOURCE_GET} -O STOP \
		-R $RESOURCE_NAME -G $RESOURCEGROUP_NAME`
	hatimerun -t $STOP_TIMEOUT $RT_BASEDIR/$STOP_METHOD \
		-R $RESOURCE_NAME -G $RESOURCEGROUP_NAME \
		-T $RESOURCETYPE_NAME

	HATIMERUN_RESULT=$?
 	case ${HATIMERUN_RESULT} in
	99)
		# SCMSGS
		# @explanation
		# The telemetry data service could not stop before the
		# configured timeout elapsed.
		# @user_action
		# This message is informational; no user action needed.
		scds_syslog -p error -t "${SYSLOG_TAG}" -m \
			"Telemetry data service stop method timed out"
		return 1;;
	98)

		# SCMSGS
		# @explanation
		# The telemetry data service could not stop for an
		# unknown reason.
		# @user_action
		# This message is informational; no user action needed.
		scds_syslog -p error -t "${SYSLOG_TAG}" -m \
			"Telemetry data service stop method failed for unknown reason"
		return 1;;
	0)
	    	# SCMSGS
	    	# @explanation
	    	# The telemetry data service stopped successfully.
	    	# @user_action
	    	# This message is informational; no user action needed.
	    	scds_syslog -p info -t "${SYSLOG_TAG}" -m \
			"Telemetry data service stop method completed sucessfully"
		;;
	*)
		# SCMSGS
		# @explanation
		# The telemetry data service could not stop for an
		# unknown reason.
		# @user_action
		# This message is informational; no user action needed.
		scds_syslog -p error -t "${SYSLOG_TAG}" -m \
			"Telemetry data service stop method returned with error %s" "${HATIMERUN_RESULT}"
		return 1;;
	esac

	# Obtain the START method name and the START_TIMEOUT value for
        # this resource.
        START_TIMEOUT=`${SCHA_RESOURCE_GET} -O START_TIMEOUT \
		-R $RESOURCE_NAME -G $RESOURCEGROUP_NAME`
        START_METHOD=`${SCHA_RESOURCE_GET} -O START \
		-R $RESOURCE_NAME -G $RESOURCEGROUP_NAME`
        hatimerun -t $START_TIMEOUT $RT_BASEDIR/$START_METHOD \
                -R $RESOURCE_NAME -G $RESOURCEGROUP_NAME \
                -T $RESOURCETYPE_NAME

	HATIMERUN_RESULT=$?
 	case ${HATIMERUN_RESULT} in
	99)
		# SCMSGS
		# @explanation
		# The telemetry data service could not start before the
		# configured timeout elapsed.
		# @user_action
		# This message is informational; no user action needed.
		scds_syslog -p error -t "${SYSLOG_TAG}" -m \
			"Telemetry data service start method timed out"
		return 1;;
	98)
		# SCMSGS
		# @explanation
		# The telemetry data service could not start for an
		# unknown reason.
		# @user_action
		# This message is informational; no user action needed.
		scds_syslog -p error -t "${SYSLOG_TAG}" -m \
			"Telemetry data service start method failed for unknown reason"
		return 1;;
	0)
	    	# SCMSGS
	    	# @explanation
	    	# The telemetry data service started successfully.
	    	# @user_action
	    	# This message is informational; no user action needed.
	    	scds_syslog -p info -t "${SYSLOG_TAG}" -m \
			"Telemetry data service start method completed sucessfully"
		;;
	*)
		# SCMSGS
		# @explanation
		# The telemetry data service could not start for an
		# unknown reason.
		# @user_action
		# This message is informational; no user action needed.
		scds_syslog -p error -t "${SYSLOG_TAG}" -m \
			"Telemetry data service start method returned with error %s" "${HATIMERUN_RESULT}"
		return 1;;
	esac

	${SCHA_RESOURCE_SETSTATUS} -R $RESOURCE_NAME \
		-G $RESOURCEGROUP_NAME \
		-s OK

	return 0
}

###############################################################################
# decide_restart_or_fault ()
#
# This function decides the action to be taken upon the failure of a probe.
# The action could be to restart the data service locally or it could be to
# place the resource in status FAULTED.
#
function decide_restart_or_fault
{
	# Check if this is the first time we are trying to restart
	if [[ $retries -eq 0 ]]; then
		# This is the first failure. Note the time when we are doing
		# this first attempt.
		start_time=`$RT_BASEDIR/gettime`
		retries=`expr $retries + 1`
		# Since this the first ever failure, we shall try to restart
		# the dataservice.
		restart_service
		if [[ $? -ne 0 ]] then
			# SCMSGS
			# @explanation
			# The Telemetry data service could not be restarted.
			# @user_action
			# This message is informational; no user action needed.
			scds_syslog -p error -t "${SYSLOG_TAG}" -m \
				"Could not restart telemetry data service"
                        return 1
		fi
		return 0
	else
		# This is not the first failure
		current_time=`$RT_BASEDIR/gettime`
		time_diff=`expr $current_time - $start_time`
		if [[ $time_diff -ge $RETRY_INTERVAL ]]; then
			# This failure happened after the time window
			# elapsed, so we reset the retries counter,
			# slide the window, and do a retry.
			retries=1
			start_time=$current_time
                	# Since the previous failure occured quite sometime
			# back (i.e beyond the retry_interval duration), we
			# will try to do a restart
			restart_service
			if [[ $? -ne 0 ]] then
				scds_syslog -p error -t "${SYSLOG_TAG}" -m \
                                	"Could not restart telemetry data service"
                        	return 1
			fi
			return 0
		elif [[ $retries -ge $RETRY_COUNT ]]; then
			# We are still within the time window,
			# and the retry counter expired. We set the
			# resource status to FAULTED to make the user
			# aware of a problem
			retries=0

			${SCHA_RESOURCE_SETSTATUS} -R $RESOURCE_NAME \
				-G $RESOURCEGROUP_NAME \
				-s FAULTED

			# SCMSGS
			# @explanation
			# The Telemetry data service could be restarted and was placed
			# to FAULTED state.
			# @user_action
			# Save a copy of the log file
			# /var/cacao/instances/default/logs/cacao.0, and contact your
			# authorized Sun service provider to determine whether a
			# workaround or patch is available.
			scds_syslog -p error -t "${SYSLOG_TAG}" -m \
       		               	"Could not restart telemetry data service, placed in FAULTED state"
			return 1
		else
			# We are still within the time window,
			# and retry counter has not expired,
			# so do another retry.
			retries=`expr $retries + 1`
			# Since we have not reached the maximum no of retries
			# allowed with in the specified retry_interval duration
			# we will try to restart again.
			restart_service
			if [[ $? -ne 0 ]] then
				scds_syslog -p error -t "${SYSLOG_TAG}" -m \
                                	"Could not restart telemetry data service"
                        	return 1
			fi
			return 0
		fi
	fi
}


###############################################################################
# MAIN
###############################################################################

# Parse the arguments that have been passed to this method
parse_args "$@" || error_exit $?

PMF_TAG=$RESOURCEGROUP_NAME,$RESOURCE_NAME,0.svc

# The interval at which probing is to be done is set in the system defined
# property THOROUGH_PROBE_INTERVAL. Obtain this information using
# ${SCHA_RESOURCE_GET}
PROBE_INTERVAL=`${SCHA_RESOURCE_GET} -O THOROUGH_PROBE_INTERVAL \
	-R $RESOURCE_NAME -G $RESOURCEGROUP_NAME`

# Obtain the time-out value allowed for the probe.
# This value is set in the extension property probe_timeout of the data
# service.
probe_timeout_info=`${SCHA_RESOURCE_GET} -O Extension -R $RESOURCE_NAME \
	-G $RESOURCEGROUP_NAME Probe_timeout`
PROBE_TIMEOUT=`echo $probe_timeout_info | awk '{print $2}'`

# We need to know the full path for the gettime utility which resides in the
# directory <RT_BASEDIR>. Get this from the RT_BASEDIR property of the
# resource type.
RT_BASEDIR=`${SCHA_RESOURCE_GET} -O RT_BASEDIR -R $RESOURCE_NAME \
	-G $RESOURCEGROUP_NAME`

# Get the Retry count value from the system defined property Retry_count
RETRY_COUNT=`${SCHA_RESOURCE_GET} -O RETRY_COUNT -R $RESOURCE_NAME \
	-G $RESOURCEGROUP_NAME`

# Get the Retry Interval value from the system defined property Retry_interval
RETRY_INTERVAL=`${SCHA_RESOURCE_GET} -O RETRY_INTERVAL -R $RESOURCE_NAME \
	-G $RESOURCEGROUP_NAME`


typeset -i retries=0

# We need to know the location of the probe method in order to restart it
# successfully. This value is obtained from the RT_BASEDIR property of the
# RTR file.
RT_BASEDIR=`${SCHA_RESOURCE_GET} -O RT_BASEDIR -R $RESOURCE_NAME \
	-G $RESOURCEGROUP_NAME`

probe_cmd_args="${RT_BASEDIR}/probe"

# Obtain the program name of the probe command and make sure it exists.
probe_cmd_prog=`echo $probe_cmd_args | nawk '{print $1}'`

while :
do
	# The interval at which the probe needs to run is specified
	# in the property THOROUGH_PROBE_INTERVAL. So we need to
	# sleep for a duration of <THOROUGH_PROBE_INTERVAL>
	sleep $PROBE_INTERVAL

	if [[ -f $probe_cmd_prog && -x $probe_cmd_prog ]]; then
		hatimerun -t $PROBE_TIMEOUT $probe_cmd_args
	fi

	if [[ $? != 0 ]]; then
		decide_restart_or_fault
	else
		# SCMSGS
		# @explanation
		# Telemetry data service probe method completed successfully.
		# @user_action
		# This message is informational; no user action needed.
		scds_syslog -p info -t "${SYSLOG_TAG}" -m \
			"Probe for resource sctelemetry successful"
	fi

done
