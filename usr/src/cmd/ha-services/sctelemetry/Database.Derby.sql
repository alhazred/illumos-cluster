--
-- CDDL HEADER START
--
-- The contents of this file are subject to the terms of the
-- Common Development and Distribution License (the License).
-- You may not use this file except in compliance with the License.
--
-- You can obtain a copy of the license at usr/src/CDDL.txt
-- or http://www.opensolaris.org/os/licensing.
-- See the License for the specific language governing permissions
-- and limitations under the License.
--
-- When distributing Covered Code, include this CDDL HEADER in each
-- file and include the License file at usr/src/CDDL.txt.
-- If applicable, add the following below this CDDL HEADER, with the
-- fields enclosed by brackets [] replaced with your own identifying
-- information: Portions Copyright [yyyy] [name of copyright owner]
--
-- CDDL HEADER END
--
-- Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
-- Use is subject to license terms.
--
-- ident	"@(#)Database.Derby.sql	1.3	08/05/20 SMI"
--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
-- THIS FILE IN CONTAINING THE SCHEMA DEFINITION FOR DERBY DATABASE
--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

-- DROP TABLE HOURLYMEASURES;
-- DROP TABLE RAWMEASURES;
-- DROP TABLE DAILYMEASURES;
-- DROP TABLE WEEKLYMEASURES;
-- DROP TABLE SENSORS;
-- DROP TABLE ManagedObjects;
-- DROP TABLE MOTYPES;
-- DROP TABLE SEQNUMS;
-- DROP TABLE KeyIndicators;
-- DROP TABLE Nodes;

----------------------------------------------------------------------

CREATE TABLE Versions (
  Name        VARCHAR(64) NOT NULL,
  Version     VARCHAR(64),
  PRIMARY KEY  (Name)
) ;

insert into versions values ('DB Schema', '1.0');

CREATE TABLE KeyIndicators (
  Id  INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY,
  Name        VARCHAR(64) NOT NULL,
  Description VARCHAR(1024),
  PRIMARY KEY  (Id)
) ;

CREATE TABLE Nodes (
  Id INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY,
  PhysId INTEGER DEFAULT -1,
  Name VARCHAR(64) NOT NULL UNIQUE,
  Description VARCHAR(1024),
  PRIMARY KEY  (Id)
);

CREATE TABLE MOTypes (
  Id  INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY,
  Name VARCHAR(64) NOT NULL,
  PRIMARY KEY (Id),
  Unique (Name)
);

CREATE TABLE ManagedObjects (
  Id  INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY,
  Name VARCHAR(1280) NOT NULL,	-- 1024 is MAXPATHLEN + 256 MAXNAMELEN
  Node INTEGER NOT NULL,
  Description VARCHAR(1024) default 'No Description',
  Type INTEGER NOT NULL,
  PRIMARY KEY  (Id),
  UNIQUE (Name, Node, Type),
  FOREIGN KEY (Type) REFERENCES MOTypes(Id),
  FOREIGN KEY (Node) REFERENCES Nodes(Id)
);

CREATE TABLE LatestSeqNums(
  Node       INTEGER NOT NULL,
  Hourly     INTEGER,
  Weekly     INTEGER,
  FOREIGN KEY (Node) REFERENCES Nodes(Id),
  PRIMARY KEY (Node)
);

CREATE TABLE HourlyMeasures (
  MO   INTEGER NOT NULL,	-- MO is instanciated for as node
  KI   INTEGER NOT NULL,
  -- THIS INT IS USED TO RECOGNIZE WHEN A SENSOR I SNOT ACTIVE FOR A SAMPLE.
  SeqNum INTEGER,
  -- THIS INT IS THE NUMBER OF SECONDS SINCE EPOCH (1/1/1970) GMT
  Time  INTEGER,
  Value BIGINT,
  UpperBound BIGINT DEFAULT 0,		-- UPPER BOUND
  FOREIGN KEY (MO)   REFERENCES ManagedObjects(Id),
  FOREIGN KEY (KI)   REFERENCES KeyIndicators(Id)
) ;


CREATE UNIQUE INDEX Hourly_pkey ON HourlyMeasures (SeqNum, KI, MO);
CREATE INDEX Hourly_time ON HourlyMeasures (time);
CREATE INDEX Hourly_Seq ON HourlyMeasures (SeqNum);

CREATE TABLE WeeklyMeasures (
  MO   INTEGER NOT NULL,	-- MO is instanciated for as node
  KI   INTEGER NOT NULL,
  -- THIS IS USED TO GET THE LASTEST :-)
  SeqNum INTEGER,
  -- THIS INT IS THE NUMBER OF SECONDS SINCE EPOCH (1/1/1970) GMT
  Time  INTEGER,
  Value BIGINT,
  UpperBound BIGINT DEFAULT 0,		-- UPPER BOUND
  FOREIGN KEY (KI)   REFERENCES KeyIndicators(Id),
  FOREIGN KEY (MO)   REFERENCES ManagedObjects(Id),
  minValue BIGINT,
  maxValue BIGINT,
  avgValue BIGINT,
  devValue BIGINT
) ;

CREATE UNIQUE INDEX Weekly_pkey ON WeeklyMeasures (SeqNum, KI, MO);
CREATE INDEX Weekly_time ON WeeklyMeasures (time);
CREATE INDEX Weekly_Seq ON WeeklyMeasures (SeqNum);

