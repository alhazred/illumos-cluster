/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * hascip_init.c - INIT method for highly available scalable ipaddress
 */

#pragma ident	"@(#)hascip_init.c	1.32	08/07/25 SMI"

#include "hascip.h"

#include <stdio.h>
#include <stdlib.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/sol_version.h>

/*
 * Called on all nodes where the RT is installed (= ALL cluster nodes)
 * If the current node is not in the AuxList, exit()
 * Configure ipaddresses on the loopback.
 * If the current node is not in the RG nodelist, exit()
 * Otherwise on a node which is in the RG nodelist,
 *
 * Remember all the ipaddresses configured
 * on the adapters, Unplumb all the adapters in the IPMP group,
 * autopush the mcnet module, plumb the adapters back, configure all
 * the ipaddresses back on the adapter.
 *
 */

int
main(int argc, char *argv[])
{
	char *ipmp_group = NULL;
	int	global_lockid, lockid, rc = 0, instances;
	char    internal_err_str[SCDS_ARRAY_SIZE];
	scds_handle_t handle;
	scha_str_array_t *auxlist, *rgnlist;
	scha_str_array_t *rg_nodelist;
	scha_extprop_value_t *aux;
	in6addr_list_t al;
	boolean_t node_in_auxlist, node_in_rgnlist;
	const char *zone;
	boolean_t zone_in_auxlist, zone_in_rgnlist;
	scha_str_array_t *aux_zone_list = NULL, *node_zone_list = NULL;


	if (scds_initialize(&handle, argc, argv) != 0) {
		scds_syslog(LOG_ERR, "%s initialization failure", argv[0]);
		exit(1);
	}

	zone = scds_get_zone_name(handle);

	/* get rg nodelist */
	rgnlist = (scha_str_array_t *)scds_get_rg_nodelist(handle);

	/* parse the nodelist from the node:zone string */
	rc = hascip_parse_nodelist(rgnlist, &rg_nodelist);
	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to parse the Nodelist property");
		exit(1);
	}

	/* get the auxlist for this resource */
	rc = scds_get_ext_property(handle, HAIP_AUXLIST, SCHA_PTYPE_STRINGARRAY,
	    &aux);
	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to retrieve the resource "
		    "property %s: %s.", HAIP_AUXLIST, scds_error_string(rc));
		exit(1);
	}
	auxlist = aux->val.val_strarray;

	/* it is possible for a node to be in none/one/both of these lists */
	node_in_rgnlist = haip_is_node_in_rgnodelist(zone, rg_nodelist);
	node_in_auxlist = hascip_is_aux(auxlist);

	/* is this node of any interest to us? */
	if ((node_in_rgnlist == B_FALSE) && (node_in_auxlist == B_FALSE))
		exit(0);

#if (SOL_VERSION >= __s10)

	if ((zone != NULL) && (strcmp(zone, GLOBAL_ZONENAME) != 0) &&
	    scds_is_zone_cluster(handle) != B_TRUE) {

		/* Get the zone list from the nodelist */
		rc = hascip_parse_zonelist(rgnlist, &node_zone_list);
		if (rc != SCHA_ERR_NOERR) {
			/*
			 * SCMSGS
			 * @explanation
			 * Need explanation of this message!
			 * @user_action
			 * Need a user action for this message.
			 */
			scds_syslog(LOG_ERR, "Failed to parse the zone names "
			    "from nodelist.");
			exit(1);
		}
		zone_in_rgnlist = hascip_is_zone_in_list(node_zone_list,
		    (char *)zone);

		if (auxlist->array_cnt > 0) {
			/* Get the zone list from the auxnodelist */
			rc = hascip_parse_zonelist(auxlist, &aux_zone_list);
			if (rc != SCHA_ERR_NOERR) {
				/*
				 * SCMSGS
				 * @explanation
				 * Need explanation of this message!
				 * @user_action
				 * Need a user action for this message.
				 */
				scds_syslog(LOG_ERR, "Failed to parse the "
				    "zone names from auxnodelist.");
				exit(1);
			}

			zone_in_auxlist = hascip_is_zone_in_list(aux_zone_list,
			    (char *)zone);
		} else {
			zone_in_auxlist = B_FALSE;
		}

		/* If the zone is not in nodelist or auxnodelist, exit */
		if ((zone_in_rgnlist == B_FALSE) &&
		    (zone_in_auxlist == B_FALSE))
			exit(0);
	}
#endif

	/* not needed any more */
	scds_free_ext_property(aux);


	/*
	 * This node is going to need scalable networking.
	 * Load net module. XXX If this has to really serve
	 * any useful purpose (avoid loading cl_net module and
	 * thus save memory on nodes/clusters which don't need
	 * scalable networking), we should disable loading
	 * of "mcnet" module at boot time in the first place.
	 */
	(void) do_modload(NET_MOD_PATH);

	/*
	 * If the node is in rgnlist (could be in auxlist too - we dont care),
	 * we plumb stuff on lo0 based on the instances of the IPMP group.
	 * If the node is not in rgnlist but is in auxlist, we plumb all IPs
	 * that we can. Because haip_hostlist_to_addrlist() does an
	 * AI_ADDRCONFIG type of call, we end up doing the right thing.
	 * No checks for any IPMP stuff are made in case of this node being
	 * in auxlist only.
	 */
	if (node_in_rgnlist) {
		/* get the ipmp group for this node */
		ipmp_group  = haip_get_ipmp_group(handle);
		if (ipmp_group == NULL) {
			exit(1);
		}

		if (haip_is_group_valid(ipmp_group) == B_FALSE) {
			scds_syslog(LOG_ERR, "%s is not a valid IPMP group "
			    "name on this node.", ipmp_group);
			exit(1);
		}

		if (pnm_get_instances_direct(ipmp_group, &instances) != 0) {
			scds_syslog(LOG_ERR, "Failed to enumerate instances "
			    "for IPMP group %s", ipmp_group);
			exit(1);
		}
	} else if (node_in_auxlist) { /* else if means node is only in aux */
		/*
		 * Dont bother with any IPMP group, just plumb everything that
		 * can be plumbed
		 */
		instances = PNMV4MASK | PNMV6MASK;
	} else {
		/* How did this happen!? We'd checked for this earlier */
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
		    "Abnormal termination");
		exit(1);
	}

	/*
	 * instances has been set according to the IPMP group (if node is in
	 * rgnlist) or to everything (if node is in auxlist only)
	 */
	rc = haip_hostlist_to_addrlist(handle, &al, instances);
	if (rc != 0) {
		scds_syslog(LOG_ERR, "Failed to obtain list of IP "
		    "addresses for this resource");
		exit(1);
	}

	/* Add ipaddresses to loopback */
	lockid = haip_serialize_lock(LOOPBACK);
	(void) hascip_add_resource_to_loopback(al, (char *)zone);
	(void) haip_serialize_unlock(lockid);

	/* Rest is applicable to a node in rgnlist only */
	if (node_in_rgnlist == B_FALSE)
		goto finished;

	/*
	 * Now autopush the mcnet module on ALL the adapters under
	 * this IPMP group. This is kinda complicated by various
	 * thingies.... We need to unplumb/plumb it for the autopush
	 * to take effect, but then we have to make sure that we
	 * restore all the ipaddresses which were configured on the
	 * adapter(s), also we got to hold IPMP or else pnmd might
	 * go berserk while all this is going on, no way to tell if
	 * some other resource is also currently messing with the
	 * same IPMP group. So better grab a IPMP-wide per node lock
	 * while we are messing with it.
	 *
	 * Update: With IPMP, we are no longer doing hold/release
	 */
	global_lockid = haip_serialize_lock(ALLIPMPS);
	lockid = haip_serialize_lock(ipmp_group);
	(void) setup_autopush(ipmp_group);
	(void) haip_serialize_unlock(lockid);
	(void) haip_serialize_unlock(global_lockid);


	/* Register with IPMP. We are interested in update callbacks */
	rc = haip_register_callback((char *)scds_get_resource_name(handle),
	    (char *)scds_get_resource_group_name(handle), (char *)zone,
	    ipmp_group, IPMP_REG_INIT, HASCIP_CALLBACK_PROG);

	if (rc != 0) {
		(void) snprintf(internal_err_str, sizeof (internal_err_str),
		    "Register callback with IPMP %s failed: Error %d.",
		    ipmp_group, rc);
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		exit(1);
	}

finished:
	scds_syslog(LOG_INFO, "Completed successfully.");

	scds_close(&handle);

	/* Well... what do you know? It all worked!!.. We think... */
	return (0);
}
