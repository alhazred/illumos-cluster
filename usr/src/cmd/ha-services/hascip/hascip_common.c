/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * hascip_common.c - Common utilities for HA scalable ipaddress
 */

#pragma ident	"@(#)hascip_common.c	1.77	08/05/27 SMI"

#include <unistd.h>
#include <stropts.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <ctype.h>
#include <signal.h>
#include <sys/errno.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <sys/socket.h>
#include <sys/sockio.h>
#include <arpa/inet.h>
#include <sys/modctl.h>
#include <sys/stream.h>
#include <sys/sad.h>
#include <sys/flock.h>

#include <net/if.h>
#include <inet/common.h>

#include <libgen.h>
#include <sys/ssm.h>
#include <sys/clconf_int.h>
#include <scha_ssm.h>
#include <sys/sol_version.h>

#include "hascip.h"

#if (SOL_VERSION >= __s10)
#include <zone.h>
#else
#define	GLOBAL_ZONENAME "global"
#endif

static int unplumb_log_if(int sock, char *ifname, sa_family_t family);

static int get_interface_list(int sock, struct lifconf *iflist);
static void free_interface_list(struct lifconf iflist);

/*
 * hascip_is_aux()
 * True if this node is in the auxlist.
 */

boolean_t
hascip_is_aux(scha_str_array_t *auxlist)
{
	uint_t i;
	int mynodeid, rc;
	scha_cluster_t clust_handle;

	/* get the nodeid for this node */
	rc = scha_cluster_open(&clust_handle);
	if (rc != SCHA_ERR_NOERR) {
		/*
		 * SCMSGS
		 * @explanation
		 * An API operation has failed while retrieving the cluster
		 * information.
		 * @explanation-2
		 * HA Storage Plus was not able to access the cluster
		 * configuration.
		 * @user_action
		 * This might be solved by rebooting the node. For more
		 * details about API failure, check the messages from other
		 * components.
		 * @user_action-2
		 * Check that the cluster configuration. If the problem
		 * persists, contact your authorized Sun service provider.
		 */
		scds_syslog(LOG_ERR,
		    "Failed to retrieve the cluster handle : %s.",
		    scds_error_string(rc));
		return (B_FALSE);
	}
	rc = scha_cluster_get(clust_handle, SCHA_NODEID_LOCAL, &mynodeid);
	(void) scha_cluster_close(&clust_handle);
	if (rc == SCHA_ERR_SEQID) {
		/*
		* Just ignore the SCHA_ERR_SEQID error as NODEID will not
		* change during the cluster reconfigurations.
		*/
		scds_syslog_debug(DBG_LEVEL_HIGH,
		    "Ignoring the SCHA_ERR_SEQID while retrieving %s",
		    SCHA_NODEID_LOCAL);
	} else if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
		"Failed to retrieve the property %s: %s.",
		SCHA_NODEID_LOCAL, scds_error_string(rc));
		return (B_FALSE);
	}

	if (auxlist) {
		for (i = 0; i < auxlist->array_cnt; i++) {
			if ((int)atol(auxlist->str_array[i]) == mynodeid)
				return (B_TRUE);
		}
	}
	return (B_FALSE);
}


/*
 * parse_adapter_name(): Returns the device instance number, and fills in the
 * driver name in the out parameter "driver". For example, for le0, instance
 * number is 0 and the driver name is "le". This function can not deal with
 * logical interface names - eg qfe0:1. We get the adapter list using
 * pnm_get_status so we know we wont get any logical interface names here.
 *
 * returns -1 for errors.
 */

static minor_t
parse_adapter_name(char *aname, char *driver)
{
	minor_t m;
	char *p;
	uint_t len;

	len = strlen(aname);
	if (len < 1) {
		*driver = 0;
		return ((minor_t)-1);
	}

	/*
	 * Adapters like 'e1000g' have numbers in the driver name.
	 * So we look for numbers starting at the last character in
	 * the name rather than the first. This ensures e1000g0 will
	 * be broken into e1000g and 0, and not e and 1000.
	 *
	 * p will point to the last char thats not a digit
	 */
	for (p = aname + len - 1; isdigit(*p); p--)
		;

	/* driver name stretches from aname to p, copy it to driver */
	while (aname <= p)
		*driver++ = *aname++;
	*driver = 0;

	m = (minor_t)atol(p);
	return (m);
}

/*
 * get_ap_list()
 * First reads the autopush list then checks if one or more of clhbsndr and
 * mcnet are absent and if so, adds the missing ones to the list at
 * appropriate postions.
 * The strapush is supplied by the caller. Note that we operate only on the
 * device major number (ALL minor numbers).
 * Returns
 * 0: Success: clhbsndr and mcnet already present
 * 1: More work needed: clhbsndr and mcnet configured in the list
 * -1: Errors
 */

static int
get_ap_list(major_t maj, struct strapush *s)
{
	int		ufd;
	uint_t		i;
	int		rc = -1;
	int		mcnet_pos = -1;
	int		hbsndr_pos = -1;
	uint_t		need_push = 0;

	if ((ufd = open(USERDEV, O_RDWR)) < 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * HA Storage Plus failed to open the specified file.
		 * @user_action
		 * Check the system configuration. If the problem persists,
		 * contact your authorized Sun service provider.
		 */
		scds_syslog(LOG_ERR, "Failed to open %s: %s.",
		    USERDEV, strerror(errno));	/*lint !e746 */
		return (-1);
	}

	bzero(s, sizeof (*s));
	s->sap_major = (major_t)maj;
	s->sap_minor = (minor_t)0;

	/* Get the current settings */
	if (ioctl(ufd, SAD_GAP, s) < 0) {
		if (errno != ENODEV) {
			/*
			 * SCMSGS
			 * @explanation
			 * Specified system operation could not complete
			 * successfully.
			 * @user_action
			 * This is as an internal error. Contact your
			 * authorized Sun service provider with the following
			 * information. 1) Saved copy of /var/adm/messages
			 * file. 2) Output of "ifconfig -a" command.
			 */
			scds_syslog(LOG_ERR, "%s operation failed : %s.",
			    "SAD_GAP", strerror(errno));	/*lint !e746 */
			rc = -1;
			goto finished;
		}
		/* Autopush Not configured */
		s->sap_npush = 0;
	}
	for (i = 0; i < s->sap_npush; i++) {
		scds_syslog_debug(4, "ap[%d]\t%s ",
			i, s->sap_list[i]);
		if (strncmp(s->sap_list[i], NET_MOD_NAME,
			strlen(NET_MOD_NAME)) == 0) {
			scds_syslog_debug(3, "mcnet already "
				"configured for autopush.");
			mcnet_pos = (int)i;
		} else if (strncmp(s->sap_list[i], HB_MOD_NAME,
			strlen(HB_MOD_NAME)) == 0) {
			hbsndr_pos = (int)i;
		}
	}
	if (mcnet_pos != -1 && hbsndr_pos != -1) {
		/*
		 * Assuming that the rest of the code is correct so that when
		 * the autopush configuration contains both modules, they are
		 * in the right order.
		 */
		rc = 0;
		goto finished;
	} else {
		if (mcnet_pos == -1) {
			need_push++;
		}
		if (hbsndr_pos == -1) {
			need_push++;
		}
	}

	/* Check if there is room */
	if (s->sap_npush + need_push > MAXAPUSH) {
		/*
		 * SCMSGS
		 * @explanation
		 * The system attempted to configure a clustering STREAMS
		 * module for autopush but too many modules were already
		 * configured.
		 * @user_action
		 * Check in your /etc/iu.ap file if too many modules have been
		 * configured to be autopushed on a network adapter. Reduce
		 * the number of modules. Use autopush(1m) command to remove
		 * some modules from the autopush configuration.
		 */
		scds_syslog(LOG_ERR, "Too many modules "
			"configured for autopush.");
		rc = -1;
		goto finished;
	}
	/* Flag that More work is needed by caller */
	rc = 1;

	if (hbsndr_pos == -1) {
		/* First, make room at the front of list */
		for (i = 0; i < s->sap_npush; i++) {
			(void) strcpy(s->sap_list[s->sap_npush - i],
				s->sap_list[s->sap_npush - i - 1]);
		}
		/* Now tack hbsndr at the front */
		hbsndr_pos = 0;
		(void) strcpy(s->sap_list[hbsndr_pos], HB_MOD_NAME);
		s->sap_npush++;		/* one more module */
	}
	if (mcnet_pos == -1) {
		/* First, make room after hbsndr in the list */
		for (i = 0; i < s->sap_npush - (uint_t)hbsndr_pos - 1; i++) {
			(void) strcpy(s->sap_list[s->sap_npush - i],
				s->sap_list[s->sap_npush - i - 1]);
		}
		/* Now tack mcnet after hbsndr */
		mcnet_pos = hbsndr_pos + 1;
		(void) strcpy(s->sap_list[mcnet_pos], NET_MOD_NAME);
		s->sap_npush++;		/* yet another module */
	}

finished:
	(void) close(ufd);
	return (rc);
}

/*
 * do_autopush()
 * Configure the specified adapter to have the mcnet module
 * autopushed on it.
 */

static int
do_autopush(char *adp)
{
	major_t				maj;
	struct strapush		p, new_ap;
	int					sadfd;
	int					rc = 0;
	char				driver[16];
	minor_t				minor_num;

	minor_num = parse_adapter_name(adp, driver);

	/* Get device major number */
	if ((modctl(MODGETMAJBIND, driver, strlen(driver) + 1,
		&maj)) < 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * System was unable to translate the given driver name into
		 * device major number.
		 * @user_action
		 * Check whether the /etc/name_to_major file is corrupted.
		 * Reboot the node if problem persists.
		 */
		scds_syslog(LOG_ERR, "Unable to get device major number for %s "
		    "driver : %s.", driver, strerror(errno));	/*lint !e746 */
		return (-1);
	}

	scds_syslog_debug(2, "major = %d minor = %d", maj, minor_num);

	if ((sadfd = open(ADMINDEV, O_RDWR)) < 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * This is an internal error. System failed to perform the
		 * specified operation.
		 * @user_action
		 * For specific error information check the syslog message.
		 * Provide the following information to your authorized Sun
		 * service provider to diagnose the problem. 1) Saved copy of
		 * /var/adm/messages file 2) Output of "ls -l /dev/sad"
		 * command 3) Output of "modinfo | grep sad" command.
		 */
		scds_syslog(LOG_ERR, "Failed to open the device %s: %s.",
		    ADMINDEV, strerror(errno));	/*lint !e746 */
		return (-1);
	}
	/*
	 * Setup the autopush list
	 * if it returns +ve, mean WE have to do
	 * more work (i.e. actually set it up).
	 */
	rc = get_ap_list(maj, &new_ap);
	if (rc < 1) {
		goto finished;
	}

	/* Clear autopush list */
	bzero(&p, sizeof (p));
	p.sap_major = maj;
	p.sap_minor = 0;
	p.sap_cmd = SAP_CLEAR;

	if (ioctl(sadfd, SAD_SAP, &p) < 0) {
		if (errno != ENODEV) {
			scds_syslog(LOG_ERR, "%s operation failed : %s.",
			    "SAD_SAP CLEAR", strerror(errno));	/*lint !e746 */
			rc = -1;	/* Sets INIT_FAILED on the resource */
			goto finished;
		}
	}


	/*
	 * Now perform autopush, new_ap is already setup
	 * with the list (sap_npush and sap_list[]).
	 */

	new_ap.sap_major = (major_t)maj;
	new_ap.sap_minor = (minor_t)-1;
	new_ap.sap_lastminor = 0;
	new_ap.sap_cmd = SAP_ALL;

	if (ioctl(sadfd, SAD_SAP, &new_ap) < 0) {
		scds_syslog(LOG_ERR, "%s operation failed : %s.",
		    "SAD_SAP PUSH", strerror(errno));	/*lint !e746 */
		rc = -1;
		goto finished;
	}

finished:
	(void) close(sadfd);
	return (rc);
}

/*
 * setup_autopush(): Gets a list of all physical adapters in a IPMP group and
 * sets them up for autopush of the mcnet module.
 */

int
setup_autopush(char *ipmp_group)
{
	int		rc;
	char		*next_adp, *adp;
	pnm_status_t	st;

	extern char *strtok_r(char *, const char *, char **);

	rc = pnm_group_status_direct(ipmp_group, &st);
	if (rc != 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * The specified IPMP group is not in functional state.
		 * Logical host resource can't be started without a functional
		 * IPMP group.
		 * @user_action
		 * LogicalHostname resource will not be brought online on this
		 * node. Check the messages (cl_pnmd errors) that encountered
		 * just before this message for any IPMP or adapter problem.
		 * Correct the problem and rerun the clresourcegroup command.
		 */
		scds_syslog(LOG_ERR, "Unable to get status for IPMP group %s.",
		    ipmp_group);
		return (-1);
	}
	if (st.backups == NULL || st.backups[0] == '\0') {
		pnm_status_free(&st);
		return (0);
	}

	next_adp = st.backups;
	while ((adp = strtok_r(next_adp, ":", &next_adp)) != 0) {
		scds_syslog_debug(2, "adp = %s", adp);
		/* Autopush on this adapter */
		(void) hascip_configure_adapter(adp);
	}

	pnm_status_free(&st);
	return (0);
}


/*
 * ip_domux2fd().  It gets a fd to the lower IP
 * stream and I_PUNLINK's the lower stream.  It
 * also initializes the variable lifr.
 *
 * Param:
 *	int *udp_fd: (referenced) fd to /dev/udp (upper IP stream).
 *	int *fd: (referenced) fd to the lower IP stream.
 *
 * Return:
 *	-1 if operation fails, 0 otherwise.
 */
static struct	lifreq lifr;

static int
ip_domux2fd(char *adp, int *udp_fd, int *fd, sa_family_t af)
{
	int ip_fd, id = 0;
	char *ipdev[] = {"/dev/ip", "/dev/ip6"};
	char *udpdev[] = {"/dev/udp", "/dev/udp6"};

	(void) strncpy(lifr.lifr_name, adp, sizeof (lifr.lifr_name));

	if (af == AF_INET) {
		id = 0;
	} else if (af == AF_INET6) {
		id = 1;
	} else {
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", "Invalid address "
		    "family");
	}

	if ((ip_fd = open(ipdev[id], O_RDWR)) < 0) {
		scds_syslog(LOG_ERR, "Failed to open %s: %s.",
			ipdev[id], strerror(errno));	/*lint !e746 */
		return (-1);
	}
	if ((*udp_fd = open(udpdev[id], O_RDWR)) < 0) {
		scds_syslog(LOG_ERR, "Failed to open %s: %s.",
			ipdev[id], strerror(errno));	/*lint !e746 */
		(void) close(ip_fd);
		return (-1);
	}
	if (ioctl(ip_fd, SIOCGLIFMUXID, (caddr_t)&lifr) < 0) {
		scds_syslog(LOG_ERR, "%s operation failed : %s.",
		    "SIOCGLIFMUXID", strerror(errno));	/*lint !e746 */
		(void) close(ip_fd);
		(void) close(*udp_fd);
		return (-1);
	}
	scds_syslog_debug(3, "ARP_muxid %d IP_muxid %d\n",
		lifr.lifr_arp_muxid, lifr.lifr_ip_muxid);
	if ((*fd = ioctl(*udp_fd, _I_MUXID2FD, lifr.lifr_ip_muxid)) < 0) {
		scds_syslog(LOG_ERR, "%s operation failed : %s.",
		    "_I_MUXID2FD", strerror(errno));	/*lint !e746 */
		(void) close(ip_fd);
		(void) close(*udp_fd);
		return (-1);
	}
	if (ioctl(*udp_fd, I_PUNLINK, lifr.lifr_ip_muxid) < 0) {
		scds_syslog(LOG_ERR, "%s operation failed : %s.",
		    "I_PUNLINK", strerror(errno));	/*lint !e746 */
		(void) close(ip_fd);
		(void) close(*udp_fd);
		return (-1);
	}
	return (0);
}

/*
 * ip_plink().  It I_PLINK's back the upper and
 * lower IP streams.  Note that this function must be
 * called after ip_domux2fd().  In ip_domux2fd(), the global
 * variable lifr is initialized and ip_plink() needs
 * information in lifr.  So ip_domux2fd() and ip_plink()
 * must be called in pairs.
 *
 * Param:
 *	int udp_fd: fd to /dev/udp (upper IP stream).
 *	int fd: fd to the lower IP stream.
 *
 * Return:
 *	-1 if operation fails, 0 otherwise.
 */
static int
ip_plink(int udp_fd, int fd)
{
	int mux_id;

	if ((mux_id = ioctl(udp_fd, I_PLINK, fd)) < 0) {
		scds_syslog(LOG_ERR, "%s operation failed : %s.",
		    "I_PLINK", strerror(errno));	/*lint !e746 */
		return (-1);
	}
	scds_syslog_debug(3, "New IP_muxid %d\n", mux_id);
	lifr.lifr_ip_muxid = mux_id;
	if (ioctl(udp_fd, SIOCSLIFMUXID,	/*lint !e737 */
		(caddr_t)&lifr) < 0) {
		scds_syslog(LOG_ERR, "%s operation failed : %s.",
		    "SIOCSLIFMUXID", strerror(errno));	/*lint !e746 */
		return (-1);
	}
	return (0);
}


/*
 * insert_mcnet()
 * Inserts the chhbsndr and the mcnet modules on the specified adapter,
 * just above the device driver. Returns
 * -1: Errors
 *  0: Success
 */

static int
insert_mcnet(char *adp, sa_family_t af)
{
	int udp_fd;
	int fd;
	int num_mods;
	int i;
	struct str_list strlist;
	int rc = 0;
	struct strmodconf mod;
	int mcnet_pos = -1;
	int hbsndr_pos = -1;

	if ((af != AF_INET) && (af != AF_INET6)) {
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", "Invalid address "
		    "family");
		return (-1);
	}

	if (ip_domux2fd(adp, &udp_fd, &fd, af) < 0) {
		return (-1);
	}
	if ((num_mods = ioctl(fd, I_LIST, NULL)) < 0) {
		scds_syslog(LOG_ERR, "%s operation failed : %s.",
		    "I_LIST", strerror(errno));	/*lint !e746 */
		rc = -1;
	} else {
		scds_syslog_debug(3, "Listing (%d) modules above %s\n",
			num_mods, adp);

		strlist.sl_nmods = num_mods;
		strlist.sl_modlist = malloc(sizeof (struct str_mlist) *
		    (uint_t)num_mods);
		if (strlist.sl_modlist == NULL) {
			scds_syslog(LOG_ERR, "No memory.");
		} else {
			if (ioctl(fd, I_LIST, (caddr_t)&strlist) < 0) {
				scds_syslog(LOG_ERR, "%s operation failed :"
					" %s.", "I_LIST",
					strerror(errno));	/*lint !e746 */
			} else {
				for (i = 0; i < strlist.sl_nmods; i++) {
					scds_syslog_debug(5, "position(%d) "
						"mod(%s).", i,
						strlist.sl_modlist[i].l_name);
					if (strcmp(strlist.sl_modlist[i].l_name,
						NET_MOD_NAME) == 0) {
						mcnet_pos = i;
						scds_syslog_debug(3,
							"mcnet already "
							"inserted at "
							"pos %d\n", i);
					} else if (0 == strcmp(HB_MOD_NAME,
					    strlist.sl_modlist[i].l_name)) {
						hbsndr_pos = i;
					}
				}
				if (hbsndr_pos == -1) {
					/*
					 * Insert clhbsndr module at the
					 * bottom of the stream.
					 */
					hbsndr_pos = strlist.sl_nmods - 1;
					mod.mod_name = HB_MOD_NAME;
					mod.pos = hbsndr_pos;
					scds_syslog_debug(3, "Inserting module "
						"%s at %d\n",
						mod.mod_name, mod.pos);
					if (ioctl(fd, _I_INSERT,
						(caddr_t)&mod) < 0) {
						scds_syslog(LOG_ERR,
							"%s operation "
							"failed : %s.",
							"_I_INSERT",
							strerror(errno));
						rc = -1;
					}
				}
				if (rc == 0 && mcnet_pos == -1) {
					/*
					 * Insert mcnet module right above
					 * the clhbsndr module.
					 */
					mod.mod_name = NET_MOD_NAME;
					mod.pos = hbsndr_pos;

					scds_syslog_debug(3, "Inserting module "
						"%s at %d\n",
						mod.mod_name, mod.pos);
					if (ioctl(fd, _I_INSERT,
						(caddr_t)&mod) < 0) {
						scds_syslog(LOG_ERR,
							"%s operation "
							"failed : %s.",
							"_I_INSERT",
							/*lint !e746 */
							strerror(errno));
						rc = -1;
					}
				}
			}
			free(strlist.sl_modlist);
		}
	}
	if (ip_plink(udp_fd, fd) < 0) {
		rc = -1;
	}
	scds_syslog_debug(3, "insert_mcnet returning %d.", rc);
	return (rc);
}

/*
 * hascip_configure_adapter()
 * 1. Check to see if adapter is plumbed.
 * 2. If plumbed, call insert_mcnet() to push mcnet module on it
 * 3. Set up Autopush on the adapter
 *
 * The IPMP group is supposed to be "held" thruout this.
 * returns 0 for success -1 for any failures.
 */

int
hascip_configure_adapter(char *adp)
{
	int	rc = 0, i;

	/* AF_MAX+1 is the end-marker for this list */
	sa_family_t af[] = {AF_INET, AF_INET6, AF_MAX + 1};

	for (i = 0; af[i] != (AF_MAX + 1); i++) {
		/*
		 * If the adapter is plumbed for an address family right now,
		 * we push the mcnet module on it.
		 */
		if (is_adapter_plumbed(adp, af[i]) == 1) {
			rc = insert_mcnet(adp, af[i]);
		}
	}

	/*
	 * Regardless of whether or not the adapter is plumbed
	 * we always set it up for autopush.
	 * We leave this autopush enabled so that if/when IPMP switches
	 * over an adapter we're guaranteed that the mcnet module will
	 * be pushed.
	 */
	if (do_autopush(adp) != 0)
		rc = -1;

	return (rc);
}


/*
 * is_adapter_plumbed():
 * Uses a simple socket ioctl to find out if the
 * specified adapter is currently plumbed for a particular address family.
 * Return
 * 1 if plumbed.
 * 0 if not plumbed.
 * -1 errors.
 */

int
is_adapter_plumbed(char *adaptername, sa_family_t af)
{
	int s;
	struct lifreq lifr_loc;

	s = socket(af, SOCK_DGRAM, 0);
	if (s < 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		scds_syslog(LOG_ERR, "Socket creation failed: %s.",
			strerror(errno));	/*lint !e746 */
		return (-1);
	}
	(void) strncpy(lifr_loc.lifr_name, adaptername,
		sizeof (lifr_loc.lifr_name));
	if (ioctl(s, SIOCGLIFFLAGS, (char *)&lifr_loc) < 0) {
		scds_syslog_debug(1, "%s operation failed : %s.",
		    "SIOCGLIFFLAGS", strerror(errno));	/*lint !e746 */
		(void) close(s);
		return (0);		/* FALSE i.e. not plumbed */
	}
	if ((lifr_loc.lifr_flags & IFF_LOOPBACK) ||
		(lifr_loc.lifr_flags & IFF_POINTOPOINT)) {
		/*
		 * SCMSGS
		 * @explanation
		 * The specified adapter may be either point to point adapter
		 * or loopback adapter which is not suitable for global
		 * networking.
		 * @user_action
		 * Reconfigure the appropriate IPMP group to exclude this
		 * adapter.
		 */
		scds_syslog(LOG_ERR, "Interface %s is plumbed but is "
		    "not suitable for global networking.", adaptername);
		(void) close(s);
		return (0);
	}
	(void) close(s);
	return (1);		/* TRUE, the adapter IS plumbed */
}

/*
 * hascip_remove_resource_from_loopback()
 * Removes all the ipaddresses in the resource
 * from the loopback interface.
 */

int
hascip_remove_resource_from_loopback(in6addr_list_t al)
{
	int i, j;
	uint_t looplen = strlen(LOOPBACK);
	struct lifconf lc;
	struct lifreq *lr;
	int s, rc = 0;
	int s4 = -1, s6 = -1;

	/* same storage used by both the sockaddr_* ptrs */
	struct sockaddr_storage ss;
	struct sockaddr_in *sin4 = (struct sockaddr_in *)&ss, *p4;
	struct sockaddr_in6 *sin6 = (struct sockaddr_in6 *)&ss, *p6;

	s4 = socket(AF_INET, SOCK_DGRAM, 0);
	if (s4 < 0) {
		scds_syslog(LOG_ERR, "Failed to create socket: %s.",
			strerror(errno));	/*lint !e746 */
		return (-1);
	}

	s6 = socket(AF_INET6, SOCK_DGRAM, 0);
	if (s6 < 0) {
		scds_syslog(LOG_ERR, "Failed to create socket: %s.",
			strerror(errno));	/*lint !e746 */
		(void) close(s4);
		return (-1);
	}

	/*
	 * Note that an AF_INET socket can retrieve *all* interfaces for
	 * us, if we put AF_UNSPEC in the lifconf structure (thats what
	 * get_interface_list does). But for the actual unplumb ioctl,
	 * the right kind of socket - AF_INET/AF_INET6 has to be used.
	 */
	if (get_interface_list(s4, &lc) < 0) {
		(void) close(s4);
		return (-1); /* error already logged */
	}

	/* loop over all IP addresses that this resource is dealing with */
	for (i = 0; i < al.count; i++) {
		if (IN6_IS_ADDR_V4MAPPED(al.addr + i)) {
			/* setup things for a v4 operation */
			s = s4;
			sin4->sin_family = AF_INET;
			IN6_V4MAPPED_TO_INADDR(al.addr + i, &(sin4->sin_addr));
		} else {
			/* setup things for a v6 operation */
			s = s6;
			sin6->sin6_family = AF_INET6;
			bcopy(al.addr + i, &(sin6->sin6_addr),
			    sizeof (struct in6_addr));
		}
		/* loop over all interfaces that are present on this node */
		for (j = lc.lifc_len, lr = lc.lifc_req; j;
		    j -= (int)sizeof (struct lifreq), lr++) {
			/* we are interested only if the AFs match... */
			if (lr->lifr_addr.ss_family != ss.ss_family)
				continue;
			/* ... and if the interface is loopback */
			if (strncmp(lr->lifr_name, LOOPBACK, looplen))
				continue;

			p4 = (struct sockaddr_in *)(&(lr->lifr_addr));
			p6 = (struct sockaddr_in6 *)(&(lr->lifr_addr));

			/* does the address match what we are looking for? */
			if (((ss.ss_family == AF_INET) &&
			    (p4->sin_addr.s_addr == sin4->sin_addr.s_addr)) ||
			    ((ss.ss_family == AF_INET6) &&
			    IN6_ARE_ADDR_EQUAL(&(p6->sin6_addr),
			    &(sin6->sin6_addr)))) {
				/* addresses match! unplumb the interface */
				if (unplumb_log_if(s, lr->lifr_name,
				    ss.ss_family) != 0) {
					char ipa[INET6_ADDRSTRLEN];
					const char *ipstr;

					ipstr = lr->lifr_addr.ss_family ==
					    AF_INET ? inet_ntop(AF_INET,
					    &(p4->sin_addr), ipa,
					    sizeof (ipa)) : inet_ntop(AF_INET6,
					    &(p6->sin6_addr), ipa,
					    sizeof (ipa));
					/*
					 * SCMSGS
					 * @explanation
					 * Need explanation of this message!
					 * @user_action
					 * Need a user action for this
					 * message.
					 */
					scds_syslog(LOG_ERR, "Failed to "
					    "unplumb %s from %s.", ipstr,
					    lr->lifr_name);
					/* record the failure but continue */
					rc = -1;
				}
			}
		}
	}

	/* close all sockets that were opened */
	if (s4 >= 0)
		(void) close(s4);
	if (s6 >= 0)
		(void) close(s6);

	free_interface_list(lc);

	return (rc);
}

/*
 * unplumb_log_if()
 * Removes the given interface name and address family
 * Uses SIOCLIFREMOVEIF to remove the logical interface.
 * Returns 0 for success -1 otherwise.
 */
int
unplumb_log_if(int sock, char *ifname, sa_family_t family)
{
	struct lifreq lifr_loc;

	/*
	 * In Solaris7 one needed to mark the logical
	 * interface DOWN (by clearing the IFF_UP flag)
	 * and then set the address of the LIF to zero
	 * in order to remove the interface. With Solaris8,
	 * marking the interface DOWN is no longer
	 * necessary. The code here simply calls the
	 * SIOCLIFREMOVEIF to remove the interface.
	 */
	bzero(&lifr_loc, sizeof (lifr_loc));
	(void) strcpy(lifr_loc.lifr_name, ifname);

	lifr_loc.lifr_addr.ss_family = family;
	if (ioctl(sock, SIOCLIFREMOVEIF, &lifr_loc) < 0) { /*lint !e737 */
		scds_syslog(LOG_ERR, "%s operation failed : %s.",
		    "SIOCLIFREMOVEIF", strerror(errno));	/*lint !e746 */
		return (-1);
	}
	return (0);
}

/*
 * hascip_add_resource_to_loopback(void)
 */

int
hascip_add_resource_to_loopback(in6addr_list_t al, char *zone)
{
	struct lifreq lifr_loc;
	struct lifreq *lr;
	struct lifconf lc;
	int found, i;
	uint_t looplen = strlen(LOOPBACK);
	int s4 = -1, s6 = -1, s, rc = 0, gil_rc;
	id_t    zid = 0;

	struct sockaddr_storage ss;
	struct sockaddr_in *sin4 = (struct sockaddr_in *)&ss, *p4;
	struct sockaddr_in6 *sin6 = (struct sockaddr_in6 *)&ss, *p6;

	s4 = socket(AF_INET, SOCK_DGRAM, 0);
	if (s4 < 0) {
		scds_syslog(LOG_ERR, "Failed to create socket: %s.",
			strerror(errno));	/*lint !e746 */
		return (-1);
	}

	s6 = socket(AF_INET6, SOCK_DGRAM, 0);
	if (s6 < 0) {
		scds_syslog(LOG_ERR, "Failed to create socket: %s.",
			strerror(errno));	/*lint !e746 */
		(void) close(s4);
		return (-1);
	}

	/*
	 * Note that an AF_INET socket can retrieve *all* interfaces for
	 * us, if we put AF_UNSPEC in the lifconf structure (thats what
	 * get_interface_list does). But for the actual ADDIF ioctl,
	 * the right kind of socket - AF_INET/AF_INET6 has to be used.
	 */
	gil_rc = get_interface_list(s4, &lc);
	if (gil_rc < 0) {
		/*
		 * Assume this ip is NOT plumbed on lo0:x and continue.
		 * The worst that could happen is duplicate plumbing,
		 * but thats better that failing here.
		 */
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
			"Failed to create interface list");
	}

	/* loop over all IP addresses that this resource deals with */
	for (i = 0; i < al.count; i++) {
		if (IN6_IS_ADDR_V4MAPPED(al.addr + i)) {
			/* setup things for a v4 operation */
			s = s4;
			sin4->sin_family = AF_INET;
			IN6_V4MAPPED_TO_INADDR(al.addr + i, &(sin4->sin_addr));
		} else {
			/* setup things for a v6 operation */
			s = s6;
			sin6->sin6_family = AF_INET6;
			bcopy(al.addr + i, &(sin6->sin6_addr),
			    sizeof (struct in6_addr));
		}

		bzero(&lifr_loc, sizeof (lifr_loc));
		(void) strcpy(lifr_loc.lifr_name, LOOPBACK);

		/*
		 * If the IP Address is already found plumbed on
		 * loopback, we will reuse it rather than unplumbing
		 * and ADDIF'ing it again.
		 */

		/* check if the ip is already plumbed on lo0:x */
		found = 0;
		if (gil_rc >= 0) {
			for (found = lc.lifc_len, lr = lc.lifc_req; found;
			    found -= (int)sizeof (struct lifreq), lr++) {

				/* we are interested only if the AFs match... */
				if (lr->lifr_addr.ss_family != ss.ss_family)
					continue;
				/* ... and if the interface is loopback */
				if (strncmp(lr->lifr_name, LOOPBACK, looplen))
					continue;

				p4 = (struct sockaddr_in *)(&(lr->lifr_addr));
				p6 = (struct sockaddr_in6 *)(&(lr->lifr_addr));

				/* break out of loop if address is plumbed */
				if (((ss.ss_family == AF_INET) &&
				    (p4->sin_addr.s_addr ==
				    sin4->sin_addr.s_addr)) ||
				    ((ss.ss_family == AF_INET6) &&
				    IN6_ARE_ADDR_EQUAL(&(p6->sin6_addr),
				    &(sin6->sin6_addr)))) {
					/*
					 * all subsequent UP and netmask etc ops
					 * should work on this interface name
					 */
					(void) strcpy(lifr_loc.lifr_name,
						lr->lifr_name);
					break;
				}
			}
		}

		/*
		 * if the address was not plumbed already, found will
		 * be 0. The IP will be ADDIF'ed only in that case.
		 */
		if (!found) {
			lifr_loc.lifr_addr = ss;

			if (ioctl(s, SIOCLIFADDIF, &lifr_loc) < 0) {
				scds_syslog(LOG_ERR, "%s operation "
				    "failed : %s.", "SIOCLIFADDIF",
				    strerror(errno));	/*lint !e746 */
				rc = -1;
				continue;
			}
		}

		/*
		 * All subsequent operations are attempted even if the
		 * ip was already plumbed and we didnt ADDIF it above.
		 */

		if (ss.ss_family == AF_INET) {
			sin4->sin_addr.s_addr = (uint_t)0xFFFFFFFF;
		} else if (ss.ss_family == AF_INET6) {
			(void) inet_pton(AF_INET6, "FFFF:FFFF:FFFF:"
			    "FFFF:FFFF:FFFF:FFFF:FFFF",
			    &(sin6->sin6_addr));
		} else {
			scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
			    "Invalid address family");
			rc = -1;
			continue;
		}

		lifr_loc.lifr_addr = ss;

		if (ioctl(s, SIOCSLIFNETMASK, /*lint !e737 */
		    &lifr_loc) < 0) {
			scds_syslog(LOG_ERR, "%s operation "
			    "failed : %s.", "SIOCSLIFNETMASK",
			    strerror(errno));	/*lint !e746 */
			rc = -1;
			continue;
		}

		if (ioctl(s, SIOCGLIFFLAGS, &lifr_loc) < 0) {
			scds_syslog(LOG_ERR, "%s operation failed : "
			    "%s.", "SIOCGLIFFLAGS",
			    strerror(errno));	/*lint !e746 */
			rc = -1;
			continue;
		}
		lifr_loc.lifr_flags |= (IFF_UP | IFF_NOARP |
		    IFF_PRIVATE);
		if (ioctl(s, SIOCSLIFFLAGS, /*lint !e737 */
		    &lifr_loc) < 0) {
			scds_syslog(LOG_ERR, "%s operation failed : "
			    "%s.", "SIOCSLIFFLAGS",
			    strerror(errno));	/*lint !e746 */
			rc = -1;
			continue;
		}

		/*
		 * If it's S10 and local zones are present, plumb it
		 * on them
		 */
#if (SOL_VERSION >= __s10)
		if ((zone != NULL) && (strcmp(zone, "global") != 0)) {
			zid = getzoneidbyname(zone);
			if (zid == -1) {
				/*
				 * SCMSGS
				 * @explanation
				 * Need explanation of this message!
				 * @user_action
				 * Need a user action for this message.
				 */
				scds_syslog(LOG_ERR, "zoneid lookup failed.");
				rc = -1;
				continue;
			}
			lifr_loc.lifr_zoneid = zid;
			if (ioctl(s, SIOCSLIFZONE, /*lint !e737 */
			    &lifr_loc) < 0) {
				scds_syslog(LOG_ERR, "%s operation failed : "
				    "%s.", "SIOCSLIFFLAGS",
				    strerror(errno));	/*lint !e746 */
				rc = -1;
				continue;
			}
		}
#endif

	}
	free_interface_list(lc);

	if (s4 >= 0)
		(void) close(s4);
	if (s6 >= 0)
		(void) close(s6);

	return (rc);
}

/*
 * Load a module(): Wrapper around modctl() syscall.
 */

int
do_modload(char *modname)
{
	int		rc;

	/*
	 * 1 as second arg means use module search path.  NULL
	 * for the last arg means we don't care which module id
	 * was assigned.  It must be cast to an int * because
	 * there isn't a prototype for modctl().
	 */
	rc = modctl(MODLOAD, 1, modname, (int *)NULL);
	if (rc < 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * A shared address resource was in the process of being
		 * created. In order to prepare this node to handle scalable
		 * services, the specified kernel module was attempted to be
		 * loaded into the system, but failed.
		 * @user_action
		 * This might be the result from the lack of system resources.
		 * Check whether the system is low in memory and take
		 * appropriate action (e.g., by killing hung processes). For
		 * specific information check the syslog message. After more
		 * resources are available on the system , attempt to create
		 * shared address resource. If problem persists, reboot.
		 */
		scds_syslog(LOG_ERR,
		    "Attempt to load %s failed: %s.",
		    modname, strerror(errno));	/*lint !e746 */

		/* If this fails, nothing is going to work */
		exit(1);
	}
	return (0);
}


/*
 * hascip_attach_resource_to_gin()
 * Calls lower level networking API to attach all ipaddresses
 * in the resource to the local node.
 */

int
hascip_attach_resource_to_gin(const char *zonename, char *rsname,
    char *rgname, int mynodeid)
{
	scha_ssm_exceptions		rc;
	char    internal_err_str[SCDS_ARRAY_SIZE];

	rc = scha_ssm_address_ip_zone(zonename, rsname, rgname, 0,
		(int)mynodeid);
	if (rc != SCHA_SSM_SUCCESS) {
		(void) snprintf(internal_err_str,
		    sizeof (internal_err_str),
		    "Unable to register with cluster networking. "
		    "The cluster networking call failed with %d", rc);
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		return (rc);
	}
	return (0);
}

/*
 * Enumerates all IP interfaces on this node and returns the
 * results obtained via iflist. Allocates mem for the caller which
 * the caller should free with free_interface_list() after its done.
 * returns 0 on success, -1 on failure. No mem remains allocated
 * if this function returns -1
 */

int
get_interface_list(int sock, struct lifconf *lifc)
{
	struct lifnum lifn;
	size_t bufsize;

	/* get the number of interfaces on this node */
	lifn.lifn_family = AF_UNSPEC;
	lifn.lifn_flags = LIFC_NOXMIT;
	lifn.lifn_count = 0;

#ifdef LIFC_ALLZONES
	lifn.lifn_flags |= LIFC_ALLZONES;
#endif

	if (ioctl(sock, SIOCGLIFNUM, &lifn) < 0) {
		scds_syslog(LOG_ERR, "%s operation failed : %s.",
		    "SIOCGLIFNUM", strerror(errno));	/*lint !e746 */
		return (-1);
	}

	/*
	 * Increase the interface count by 2. This is so that we
	 * allocate space for two extra elements later, just in
	 * case an interface or two is added between SIOCGLIFNUM
	 * above and SIOCGLIFCONF below.
	 */
	lifn.lifn_count += 2;

	/* zero out the contents of lifc */
	bzero(lifc, sizeof (struct lifconf));

	bufsize  = (unsigned)lifn.lifn_count * sizeof (struct lifreq);

	/* allocate space for the list and zero it out */
	lifc->lifc_buf = calloc(1, bufsize);
	if (lifc->lifc_buf == NULL) {
		scds_syslog(LOG_ERR, "No memory.");
		return (-1);
	}
	lifc->lifc_family = AF_UNSPEC;
	lifc->lifc_flags = LIFC_NOXMIT;
	lifc->lifc_len = (signed)bufsize;

#ifdef LIFC_ALLZONES
	lifc->lifc_flags |= LIFC_ALLZONES;
#endif

	/* get a list of all interfaces */
	if (ioctl(sock, SIOCGLIFCONF, lifc) < 0) {
		scds_syslog(LOG_ERR, "%s operation failed : %s.",
		    "SIOCGLIFCONF", strerror(errno));	/*lint !e746 */
		free(lifc->lifc_buf);
		return (-1);
	}

	return (0);
}

/* Frees the memory allocated by get_interface_list() */
void
free_interface_list(struct lifconf lc)
{
	free(lc.lifc_buf);
}

scha_err_t
hascip_parse_nodelist(scha_str_array_t *ina, scha_str_array_t **rgnlist) {
	scha_err_t err = NULL;
	char *val = NULL;
	uint_t i = 0;
	scha_str_array_t *rgl = NULL;
	char *a;


	if (ina == NULL || ina->array_cnt == 0) {
		/* If the input string is NULL, return success */
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
		    "Input string is null.");
		return (SCHA_ERR_INVAL);
	}

	rgl = (scha_str_array_t *)malloc(sizeof (scha_str_array_t));
	if (rgl == NULL) {
		scds_syslog(LOG_ERR,
		    "Out of memory.");
		return (SCHA_ERR_NOMEM);
	}

	/* Initialize the structure elements */
	rgl->is_ALL_value = B_FALSE;
	rgl->array_cnt = 0;
	rgl->str_array = NULL;

	/* Initialize the array */
	rgl->str_array =
	    (char **)malloc((ina->array_cnt) * (sizeof (char *) + 1));

	if (rgl->str_array == NULL) {
		scds_syslog(LOG_ERR, "Out of memory.");
		return (SCHA_ERR_NOMEM);
	}

	for (i = 0; i < ina->array_cnt; i++) {
		/*
		 * strtok function will change the input string, so save the
		 * input string.
		 */

		val = strdup(ina->str_array[i]);
		if (val == NULL) {
			scds_syslog(LOG_ERR,
			    "Out of memory.");
			err = SCHA_ERR_NOMEM;
			goto finished;
		}
		a = strchr(val, ':');
		if (a != NULL) {
			*a = '\0';
		}

		if (strlen(val) > 0) {
			/* Update the rgl structure with new values */
			rgl->str_array[i] = (char *)malloc(strlen(val) + 1);
			(void) strcpy(rgl->str_array[i], val);
			rgl->array_cnt++;
			if (val)
				free(val);
			continue;
		}
	finished:
		if (rgl) {
			if (rgl->str_array) {
				for (i = 0; i < rgl->array_cnt; i++) {
					free(rgl->str_array[i]);
				}
				free(rgl->str_array);
			}
			free(rgl);
		}
		if (val)
			free(val);

		return (err);
	}

	*rgnlist = rgl;

	return (SCHA_ERR_NOERR);
}

int
verify_dup_nodes(scha_str_array_t *rg_nodelist) {

	scha_str_array_t *rgl = NULL;
	int rc = SCHA_ERR_NOERR;
	uint_t i, j;

	if (rg_nodelist == NULL || rg_nodelist->array_cnt == 0) {
		/* If the input string is NULL, return success */
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
		    "Input string is null.");
		return (SCHA_ERR_INVAL);
	}

	/* Parse the nodelist from node:zone format */
	rc = hascip_parse_nodelist(rg_nodelist, &rgl);
	if (rc != SCHA_ERR_NOERR) {
		/*
		 * SCMSGS
		 * @explanation
		 * A LogicalHostname resource could not be initialized because
		 * of failure to parse the resource group's Nodelist property.
		 * @user_action
		 * This is an internal error. Contact your authorized Sun
		 * service provider to determine whether a workaround or patch
		 * is available.
		 */
		scds_syslog(LOG_ERR, "Failed to parse the Nodelist property");
		return (1);
	}

	/*
	 * Iterate thru the parsed node list and verify that there
	 * are no duplicate node names
	 */
	for (i = 0; i < rgl->array_cnt; ++i) {
		for (j = i + 1; j < rgl->array_cnt; ++j) {
			if (strcmp(rgl->str_array[i], rgl->str_array[j]) == 0) {
				/*
				 * SCMSGS
				 * @explanation
				 * Need explanation of this message!
				 * @user_action
				 * Need a user action for this message.
				 */
				scds_syslog(LOG_ERR,
				    "Resource group node list has "
				    "multiple zone names for node %s",
				    rgl->str_array[i]);
				return (1);
			}
		}
	}
	free(rgl);
	return (0);
}

int
hascip_parse_zonelist(scha_str_array_t *ina, scha_str_array_t **zonea) {
	scha_err_t err = NULL;
	char *zonename = NULL;
	char *val = NULL;
	scha_str_array_t *zonel = NULL;
	uint_t i;

	if (ina == NULL || ina->array_cnt == 0) {
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
		    "Input string is null.");
		return (SCHA_ERR_INVAL);
	}

	zonel = (scha_str_array_t *)malloc(sizeof (scha_str_array_t));
	if (zonel == NULL) {
		scds_syslog(LOG_ERR,
		    "Out of memory.");
		return (SCHA_ERR_NOMEM);
	}

	/* Initialize the structure elements */
	zonel->is_ALL_value = B_FALSE;
	zonel->array_cnt = 0;
	zonel->str_array = NULL;

	/* Initialize the array */
	zonel->str_array =
	    (char **)malloc((ina->array_cnt) * (sizeof (char *) + 1));

	if (zonel->str_array == NULL) {
		scds_syslog(LOG_ERR, "Out of memory.");
		return (SCHA_ERR_NOMEM);
	}

	for (i = 0; i < ina->array_cnt; i++) {
		/*
		 * strtok function will change the input string,
		 * so save the input string.
		 */

		val = strdup(ina->str_array[i]);
		if (val == NULL) {
			scds_syslog(LOG_ERR,
			    "Out of memory.");
			err = SCHA_ERR_NOMEM;
			goto finished;
		}
		zonename = strchr(val, ':');
		if (zonename == NULL) {
			zonename = "";
		} else {
			zonename += strlen(":");
		}

		/* Update the zonel structure with new values */
		zonel->str_array[i] = (char *)malloc(strlen(zonename) + 1);
		(void) strcpy(zonel->str_array[i], zonename);
		zonel->array_cnt++;
		continue;

	finished:
		if (zonel) {
			if (zonel->str_array) {
				for (i = 0; i < zonel->array_cnt; i++) {
					free(zonel->str_array[i]);
				}
				free(zonel->str_array);
			}
			free(zonel);
		}

		if (zonename)
			free(zonename);

		if (val)
			free(val);

		return (err);
	}

	*zonea = zonel;

	return (SCHA_ERR_NOERR);
}

boolean_t
hascip_is_zone_in_list(scha_str_array_t *in_array, char *zone) {
	uint_t i;

	if (in_array) {
		for (i = 0; i < in_array->array_cnt; i++) {
			if (strlen(in_array->str_array[i]) != 0) {
				if (strcmp(in_array->str_array[i], zone) == 0) {
				    return (B_TRUE);
				}
			}
		}
	}
	return (B_FALSE);
}
