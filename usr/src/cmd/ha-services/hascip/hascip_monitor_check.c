/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * hascip_monitor_check.c
 * Monitor Check method for highly available scalable ipaddress
 */

#pragma ident "@(#)hascip_monitor_check.c 1.20 08/05/20 SMI"

#include "hascip.h"

#include <stdlib.h>
#include <netdb.h>
#include <netinet/in.h>

/*
 * Check with IPMP about the current status of the IPMP group.
 * exit non-zero if the IPMP status is not good.
 */

int
main(int argc, char *argv[])
{
	char	*ipmp_group = NULL;
	char    ipmpstate[MAXIPMPMSGLEN];
	int	rc;
	scds_handle_t handle;

	if (scds_initialize(&handle, argc, argv) != 0) {
		scds_syslog(LOG_ERR, "%s initialization failure", argv[0]);
		exit(1);
	}

	/* get the ipmp group for this node */
	ipmp_group  = haip_get_ipmp_group(handle);
	if (ipmp_group == NULL) {
		exit(1);
	}

	if (haip_is_group_valid(ipmp_group) == B_FALSE) {
		scds_syslog(LOG_ERR, "%s is not a valid IPMP group name on "
		    "this node.", ipmp_group);
		exit(1);
	}

	rc = haip_ipmpstatus(ipmp_group, ipmpstate);
	if (rc != 0) {
		scds_syslog(LOG_ERR, "IPMP group %s has status %s.",
		    ipmp_group, ipmpstate);
		exit(1);
	}

	/* Checks whether the name service is available and responsive. */
	if (haip_check_nameservice(handle) != 0) {
		scds_syslog(LOG_ERR, "Name service not available.");
		exit(1);
	}

	scds_syslog(LOG_INFO, "Completed successfully.");
	scds_close(&handle);

	/* IPMP adapter is fine on this node, exit 0 */
	return (0);
}
