/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * hascip_prenet_start.c - Start method for highly available scalable ipaddress
 */

#pragma ident	"@(#)hascip_prenet_start.c	1.37	08/05/27 SMI"
#include "hascip.h"

#include <stdio.h>
#include <stdlib.h>
#include <netdb.h>
#include <netinet/in.h>

/*
 * Configure the ipaddresses on the right IPMP group and
 * then call the lower level Networking API to "attach" these
 * ipaddresses to the local node.
 * There is all there is to it. + some error checks, of course.
 */

int
main(int argc, char *argv[])
{
	char	*ipmp_group = NULL;
	int	rc, rc2, instances;
	int	lockid;
	char    internal_err_str[SCDS_ARRAY_SIZE];
	in6addr_list_t al;
	int mynodeid;
	scha_cluster_t clust_handle;
	scds_handle_t handle;
	const char *rname, *rgname, *zone;

	if (scds_initialize(&handle, argc, argv) != 0) {
		scds_syslog(LOG_ERR, "%s initialization failure", argv[0]);
		exit(1);
	}

	rname = scds_get_resource_name(handle);
	rgname = scds_get_resource_group_name(handle);
	zone = scds_get_zone_name(handle);

	/* get the nodeid for this node */
	rc = scha_cluster_open_zone(zone, &clust_handle);
	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
		    "Failed to retrieve the cluster handle : %s.",
		    scds_error_string(rc));
		exit(1);
	}
	rc = scha_cluster_get(clust_handle, SCHA_NODEID_LOCAL, &mynodeid);
	(void) scha_cluster_close(&clust_handle);
	if (rc == SCHA_ERR_SEQID) {
		/*
		* Just ignore the SCHA_ERR_SEQID error as NODEID will not
		* change during the cluster reconfigurations.
		*/
		scds_syslog_debug(DBG_LEVEL_HIGH,
		    "Ignoring the SCHA_ERR_SEQID while retrieving %s",
		    SCHA_NODEID_LOCAL);
	} else if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
		"Failed to retrieve the property %s: %s.",
		SCHA_NODEID_LOCAL, scds_error_string(rc));
		exit(1);
	}


	/* get the ipmp group for this node */
	ipmp_group  = haip_get_ipmp_group(handle);
	if (ipmp_group == NULL) {
		(void) snprintf(internal_err_str, sizeof (internal_err_str),
		    "%s group name is not found in the netiflist for this "
		    "node.", pnm_group_name());
		(void) scha_resource_setstatus_zone(rname, rgname, zone,
		    SCHA_RSSTATUS_FAULTED, internal_err_str);
		exit(1);
	}

	/*
	 * Sanity check that the IPMP group specified in the
	 * resource property is a real IPMP group.
	 */
	if (haip_is_group_valid(ipmp_group) == B_FALSE) {
		(void) snprintf(internal_err_str, sizeof (internal_err_str),
		    "%s is not a valid IPMP group name on this node.",
		    ipmp_group);
		(void) scha_resource_setstatus_zone(rname, rgname, zone,
		    SCHA_RSSTATUS_FAULTED, internal_err_str);
		exit(1);
	}

	/* make a list of ip addresses to work with */
	if (pnm_get_instances_direct(ipmp_group, &instances) != 0) {
		(void) snprintf(internal_err_str, sizeof (internal_err_str),
		    "Failed to enumerate instances for IPMP group %s",
		    ipmp_group);
		(void) scha_resource_setstatus_zone(rname, rgname, zone,
		    SCHA_RSSTATUS_FAULTED, internal_err_str);
		exit(1);
	}

	rc = haip_hostlist_to_addrlist(handle, &al, instances);
	if (rc != 0) {
		(void) scha_resource_setstatus_zone(rname, rgname, zone,
		    SCHA_RSSTATUS_FAULTED,
		    "Failed to obtain list of IP addresses for this resource");
		exit(1);
	}

	/*
	 * First, plumb the address(es) via the IPMP group,
	 * we do not remove the ipaddress from the loopback FIRST
	 * because that would mean that for a time period, the
	 * ipaddress is not available on the node. The PLUMB
	 * operation configures the ipaddress(es) on the IPMP
	 * adapter but does not mark it UP.
	 *
	 * The overall goal here is that the ipaddress(es) being
	 * managed by the implementaion always exist on the node,
	 * either on the IPMP interface or on the loopback. To switch
	 * them from one to other, we first PLUMB them on one
	 * them remove them from other, followed by marking them UP
	 * in their new location.
	 *
	 * The start method (called after prenet_start) actually marks
	 * the ipaddresses UP on the IPMP adapter.
	 */
	rc = haip_do_ipmp_op(ipmp_group, zone, PNM_IFCONFIG_PLUMB, al);
	if (rc > 0) {
		/* None of the ops succeeded */
		(void) snprintf(internal_err_str, sizeof (internal_err_str),
		    "Could not %s any ip addresses.", "plumb");
		(void) scha_resource_setstatus_zone(rname, rgname, zone,
		    SCHA_RSSTATUS_FAULTED, internal_err_str);
		exit(1);
	}
	if (rc < 0) {
		/* Some of the ops succeeded, but not all */
		scds_syslog(LOG_WARNING,
		    "Some ip addresses may not be plumbed.");
	}

	/* Continue anyway because some ipaddresses might be OK */

	/*
	 * Now, remove ipaddresses from loopback if there.
	 * Note that we need to serialize these operations on the
	 * loopback interface via a locking mechanism. The reason
	 * is that if two SharedAddress resource method are
	 * running simultaneously on the node and they both
	 * attempt to add/remove loopback logical interfaces, they
	 * would step on each other.
	 *
	 * The locking here is via a file lock, using fcntl() on
	 * a well known file. The file name is derived from the
	 * interface name. For the loopback, the file name would be
	 * /var/opt/cluster/rgm/rt/hascip/haip.lock.loopback.
	 */

	lockid = haip_serialize_lock(LOOPBACK);

	rc = hascip_remove_resource_from_loopback(al);
	if (rc != 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * Some of the ip addresses managed by the specified
		 * SharedAddress resource were not removed from the loopback
		 * interface.
		 * @user_action
		 * Use the ifconfig command to make sure that the ip addresses
		 * being managed by the SharedAddress implementation are
		 * present either on the loopback interface or on a physical
		 * adapter. If they are present on both, use ifconfig to
		 * delete them from the loopback interface. Then use
		 * clresourcegroup to move the resource group that contains
		 * the SharedAddresses to another node to make sure that the
		 * resource group can be switched over successfully.
		 */
		scds_syslog(LOG_ERR,
		    "Some ip addresses might still be on loopback.");
	}

	(void) haip_serialize_unlock(lockid);

	/* Inform the PDT that these ipaddresses live on this node */
	rc2 = hascip_attach_resource_to_gin(zone, (char *)
	    scds_get_resource_name(handle),
	    (char *)scds_get_resource_group_name(handle), mynodeid);
	if (rc2 != 0) {
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
		    "Unable to register");
		exit(1);
	}

	if (rc == 0) {
		scds_syslog(LOG_INFO, "Completed successfully.");
	}

	free(al.addr);
	scds_close(&handle);
	return (0);
}
