/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_HASCIP_H_
#define	_HASCIP_H_

#pragma ident	"@(#)hascip.h	1.38	08/07/31 SMI"

#include <scha.h>
#include <rgm/pnm.h>
#include <syslog.h>
#include <limits.h>
#include <libintl.h>
#include <unistd.h>
#include <rgm/haip.h>
#include <rgm/libdsdev.h>

#ifdef __cplusplus
extern "C" {
#endif

/*
 * hascip.h - Header file for HA Scalable IPaddress RT
 */

#define	LOOPBACK "lo0"
#define	ALLIPMPS "ipmp_all"

#define	HB_MOD_NAME "clhbsndr"
#define	NET_MOD_NAME "mcnet"
#define	NET_MOD_PATH "misc/cl_net"

#define	HASCIP_CALLBACK_PROG	"hascip/hascip_ipmp_callback"
#define	RETRY_PROG	"/usr/cluster/lib/rgm/rt/hascip/hascip_retry"

/*
 * HASCIP_SAFE_DELAY is the time we wait for other node
 * to "completely die" (i.e. stop responding to pings).
 * After this interval, however, we go ahead and takeover
 * the ipaddress anyway. The value 270 sec is derived from
 * the default time used by the CMM in its safe_to_takeover
 * algorithms.
 */
#define	HASCIP_SAFE_DELAY		270

extern boolean_t hascip_is_aux(scha_str_array_t *auxlist);
extern int is_adapter_plumbed(char *adp, sa_family_t af);
extern int hascip_configure_adapter(char *adp);
extern int hascip_attach_resource_to_gin(const char *zonename, char *rsname,
    char *rgname, int mynodeid);
extern int setup_autopush(char *ipmp_group);
extern int hascip_add_resource_to_loopback(in6addr_list_t al, char *zone);
extern int hascip_remove_resource_from_loopback(in6addr_list_t al);
extern int hascip_parse_nodelist(scha_str_array_t *input_str,
    scha_str_array_t **rglist);
extern int verify_dup_nodes(scha_str_array_t *rg_nodelist);
extern int do_modload(char *modname);
extern int hascip_parse_zonelist(scha_str_array_t *ina,
    scha_str_array_t **zonea);
extern boolean_t hascip_is_zone_in_list(scha_str_array_t *in_array, char *zone);

/* This is not in sys/modctl.h, where is it? */
extern int modctl(int op, ...);

#ifdef __cplusplus
}
#endif

#endif	/* !_HASCIP_H_ */
