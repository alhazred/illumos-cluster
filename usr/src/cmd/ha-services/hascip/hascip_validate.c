/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * hascip_validate.c - validate method for highly available scalable ipaddress
 */

#pragma ident  "@(#)hascip_validate.c 1.39     08/09/10 SMI"

#include "hascip.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <locale.h>
#include <libintl.h>

/*
 * Checks for the validity of the IPMP group
 * specified on this node and makes sure that all
 * the ipaddresses managed by this resource are
 * resolvable via the name service.
 */

int
main(int argc, char *argv[])
{
	char *ipmp_group;
	char *new_group;
	int rc, c, cu_flag = 0, instances;
	scds_handle_t handle;
	scha_str_array_t *rg_nodelist;
	in6addr_list_t al;
	const char *rname, *zone_name;

	(void) setlocale(LC_ALL, "");
	(void) textdomain(TEXT_DOMAIN);
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	/* funcs in libhaip & hascip_common.c should also print to stderr */
	print_errors = B_TRUE;

	if (scds_initialize(&handle, argc, argv) != 0) {
		scds_syslog(LOG_ERR, "%s initialization failure", argv[0]);
		(void) fprintf(stderr, gettext("%s initialization failure\n"),
			argv[0]);
		exit(1);
	}

	rname = scds_get_resource_name(handle);
	zone_name = scds_get_zone_name(handle);

	/* Can't do more if this node is not a RG node */
	rg_nodelist = (scha_str_array_t *)scds_get_rg_nodelist(handle);
	if (haip_is_node_in_rgnodelist(zone_name, rg_nodelist) == B_FALSE) {
		scds_close(&handle);
		exit(0);	/* Exit success */
	}

	/*
	 * If it's S10 and zones are present, verify that the same physical
	 * node is NOT present more than once. RGM allows node list to contain
	 * the same physical node more than once, for different zones like
	 * node1,node1:zone1 or node1:zone1,node1:zone2. But for shared_address
	 * resource, any physical node can be specified only once.
	 */
	rc = verify_dup_nodes(rg_nodelist);
	if (rc != SCHA_ERR_NOERR) {
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		scds_syslog(LOG_ERR, "Failed to validate the nodelist.");
		exit(1);
	}

	/*
	 * args were already parsed by scds_initialize, so no error checks here.
	 * We just want to know whether the resource is being created or
	 * updated. 'c' or 'u' is stored in the flag accordingly.
	 */
	while ((c = getopt(argc, argv, "R:T:G:r:x:g:Z:cu")) != EOF) {
		if ((c == 'c') || (c == 'u'))
			cu_flag = c;
	}

	/* Make sure there is a group name defined on this node */
	ipmp_group  = haip_get_ipmp_group(handle);
	if (ipmp_group == NULL) {
		scds_close(&handle);
		exit(1);	/* Exit non-zero */
	}

	/* Make sure this looks like a real IPMP group */
	if (haip_is_group_valid(ipmp_group) == B_FALSE) {
		scds_syslog(LOG_ERR, "%s is not a valid IPMP group name "
		    "on this node.", ipmp_group);
		(void) fprintf(stderr, gettext("%s is not a valid IPMP group "
			"name on this node.\n"), ipmp_group);
		scds_close(&handle);
		exit(1);	/* Exit non-zero */
	}

	/*
	 * Make sure the IPMP group actually supports the type of
	 * IP addresses that we'll plumb on it later. Its *not* an error
	 * if it does not - we'll issue a warning here during VALIDATE and
	 * silently work with the IPs that the group can handle subsequently.
	 */
	rc = pnm_get_instances_direct(ipmp_group, &instances);
	if (rc == PNM_ENONHOMOGENOUS) {
		/*
		 * SCMSGS
		 * @explanation
		 * Not all adapters in the IPMP group can host the same types
		 * of IP addresses. eg some adapters are IPv4 only, others are
		 * capable of hosting IPv4 and IPv6 both and so on.
		 * @user_action
		 * Configure the IPMP group such that each adapter is capable
		 * of hosting IP addresses that the rest of the adapters in
		 * the group can.
		 */
		scds_syslog(LOG_ERR, "IPMP group %s is not homogenous",
		    ipmp_group);
		(void) fprintf(stderr, gettext("IPMP group %s is not "
		    "homogenous\n"), ipmp_group);
		exit(1);
	} else if (rc != 0) {
		scds_syslog(LOG_ERR, "Failed to enumerate instances for "
		    "IPMP group %s", ipmp_group);
		(void) fprintf(stderr, gettext("Failed to enumerate instances "
		    "for IPMP group %s\n"), ipmp_group);
		exit(1);
	}

	/* check if neither of the two instance flags is set */
	if ((instances & (PNMV4MASK | PNMV6MASK)) == 0) {
		/* wont really get here... more of a sanity check */
		/*
		 * SCMSGS
		 * @explanation
		 * The cluster is undergoing a reconfiguration.
		 * @user_action
		 * None. This is an informational message only.
		 */
		scds_syslog(LOG_ERR, "IPMP group %s is incapable of hosting "
		    "%s addresses", ipmp_group, "IPv4 or IPv6");
		(void) fprintf(stderr, gettext("IPMP group %s is incapable of "
		    "hosting %s addresses\n"), ipmp_group, "IPv4 or IPv6");
		exit(1);
	}

	/* if group has only one instance set, validate more closely */
	if ((instances & (PNMV4MASK | PNMV6MASK)) != (PNMV4MASK | PNMV6MASK)) {
		char *cant_host;
		int cant_host_instances;
		/*
		 * now invert the instances flag and use that to create a
		 * addrlist: what we want to do is check if any host has a
		 * mapping outside the instances that the IPMP group supports.
		 * If yes, issue a warning
		 *
		 * scrgadm is supposed to catch cross node homogeneity
		 * problems. This check helps when the ipmp groups are
		 * homogenous across nodes but do not have both instances.
		 * It also helps people using non-convenience options.
		 */
		cant_host_instances = instances ^ (PNMV4MASK | PNMV6MASK);
		cant_host = (cant_host_instances & PNMV4MASK) ? "IPv4" : "IPv6";
		if (haip_hostlist_to_addrlist(handle, &al,
		    cant_host_instances) != 0) {
			scds_syslog(LOG_ERR, "Failed to obtain list of IP "
			    "addresses for this resource");
			(void) fprintf(stderr, gettext("Failed to obtain "
			    "list of IP addresses for this resource\n"));
			exit(1);
		}
		if (al.count != 0) {
			/*
			 * SCMSGS
			 * @explanation
			 * The IPMP group cannot host IP addresses of the type
			 * specified. One or more hostnames in the resource
			 * have mappings of that type. Those IP addresses will
			 * not be hosted on this node.
			 * @user_action
			 * Need a user action for this message.
			 */
			scds_syslog(LOG_ERR, "WARNING: IPMP group %s can not "
			    "host %s addresses. Atleast one of the hostnames "
			    "that were specified has %s mapping(s). All such "
			    "mappings will be ignored on this node.",
			    ipmp_group, cant_host, cant_host);
			(void) fprintf(stderr, gettext("WARNING: IPMP group "
			    "%s can not host %s addresses. Atleast one of the "
			    "hostnames that were specified has %s mapping(s). "
			    "All such mappings will be ignored on this "
			    "node.\n"), ipmp_group, cant_host, cant_host);
			free(al.addr);
		}
	}

	/*
	 * If the zone name is not null and belongs to zone cluster,
	 * verify the adapters in the ipmp group are authorized to
	 * be used for this zone cluster.
	 */
	if (zone_name != NULL && scds_is_zone_cluster(handle)) {
		/* Validate the zone adapter list */
		rc = haip_validate_zone_adp(handle, ipmp_group);
		if (rc != 0) {
			scds_syslog(LOG_ERR, "No suitable adapter is "
			    "authorized "
			    "to be used with resource %s on zone cluster %s.",
			    rname, zone_name);
			(void) fprintf(stderr, gettext(
			    "No suitable adapter is authorized to be used with "
			    "resource %s on zone cluster %s.\n"),
			    rname, zone_name);
			exit(1);
		}
	}


	/*
	 * Check if the ipmp name is being changed by comparing the name
	 * with the one in ccr.
	 * Note: the ipmp name pointed by ipmp_group will be overwritten
	 * by haip_check_group_changed() which calls haip_get_group_name().
	 */
	new_group = strdup(ipmp_group);
	if (new_group == NULL) {
		scds_syslog(LOG_ERR, "Out of memory.");
		(void) fprintf(stderr, gettext("Out of memory.\n"));
		exit(1);
	}

	switch (cu_flag) {
	case 'c':
		/*
		 * We should have atleast one IP to handle and none of them
		 * should be plumbed already
		 */
		if (haip_check_dup(handle) != 0) {
			/* Messages logged by haip_check_dup() */
			exit(1);
		}
		break;
	case 'u':
		rc = haip_check_group_changed(zone_name, handle, new_group);
		if (rc == 1) {
			/*
			 * SCMSGS
			 * @explanation
			 * A different IPMP group for property NetIfList is
			 * specified in clreslogicalhostname command. The IPMP
			 * group on local node is set at resource creation
			 * time. Users may only update the value of property
			 * NetIfList for adding a IPMP group on a new node.
			 * @user_action
			 * Rerun the clreslogicalhostname command with proper
			 * value for the NetIfList property.
			 */
			scds_syslog(LOG_ERR, "Cannot change the IPMP group "
			    "on the local host.");
			(void) fprintf(stderr, gettext("Cannot change the "
			    "IPMP group on the local host. Bring resource %s "
			    "offline on this node and try again.\n"), rname);
			exit(1);
		} else if (rc == -1)
			/* messages already logged */
			exit(1);
		break;
	default:
		/* cant get here!! */
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
		    "create/update flag corruption");
		(void) fprintf(stderr, gettext("INTERNAL ERROR: %s.\n"),
		    gettext("create/update flag corruption"));
		exit(1);
	}

	/* make sure all the hostnames/ipaddress are good */
	rc = haip_validate_hostnamelist(handle, ipmp_group, instances);
	if (rc != B_TRUE) {
		scds_syslog(LOG_ERR, "Resource contains invalid hostnames.");
		(void) fprintf(stderr, gettext("Resource contains invalid "
			"hostnames.\n"));
		exit(1);
	}
	scds_syslog_debug(DBG_LEVEL_LOW, "Validating uniqueness of hostnames "
					"in the hostlist");
	rc = haip_validate_uniqueness(handle);
	if (rc != B_TRUE) {
		(void) fprintf(stderr, gettext("One or more hostname(s) in the "
					"hostlist of this shared address "
					"resource already exist "
					"in another resource group.\n"));
		exit(1);
	}

	scds_syslog(LOG_INFO, "Completed successfully.");

	scds_close(&handle);

	return (0);
}
