/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * hascip_ipmp_callback.c - Callback utility registered with IPMP to
 * call us back when the IPMP group goes bad.
 */

#pragma ident	"@(#)hascip_ipmp_callback.c	1.43	08/07/31 SMI"

#include "hascip.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netdb.h>
#include <netinet/in.h>
#include <errno.h>
#include <syslog.h>
#include <rgm/haip.h>

/*
 * This script is registered by both the INIT/BOOT methods
 * as well as the monitor method. These two registrations
 * differ by virtue of the first command line argument
 * in the callback. The INIT/BOOT methods register the
 * first arg as "init", the MONITOR registers it as "mon".
 * We refer to these two as "registration mode".
 *
 * This script is called by IPMP under three scenarios
 * 1. A IPMP group has failed.
 * 2. A IPMP group has been updated.
 * 3. An IPMP group is repaired.
 * These three scenarios are distinguished by the fourth
 * arg passed to the command line. These could be "failed",
 * "updated", or "repaired". Thus the command line looks like:
 *
 * prog_name mon/init rgname rname failed/updated/repaired ipmp_name
 *
 * If the event is "failed" and the registration mode is "mon"
 * then call scha_control() to cause the containing RG to be moved
 * to some other node.
 *
 * The scha_control() call is now made in a separate program
 * hascip_retry. The scha_control() call is retried by that
 * utility if it fails. We launch it under PMF via
 * haip_start_retry() routine.
 *
 * If the event is "updated" and the mode is "init", then
 * replumb the mcnet module onto the IPMP group.
 *
 * The code needs to distinguish between the two so that
 * scha_control() is not called twice, or the adapters
 * under IPMP are not plumbed twice.
 *
 * If the event is "repaired", and the mode is "mon", then
 * haip_stop_retry() is called. This allows us to stop the
 * retry utility the moment a repair is detected, rather than
 * waiting for it to wake up and find out that the group is OK
 * now.
 */

int
main(int argc, char *argv[])
{
	char	*ipmp_group = NULL;
	char    ipmp_state[MAXIPMPMSGLEN];
	int	rc = 0, child_pid;
	char	*rgname, *rsname, *ipmp_name, *regmode, *event, *action;
	int	fake_argc;
	char	*fake_argv[10];
	char	internal_err_str[SCDS_ARRAY_SIZE];
	int	global_lockid, ipmp_lockid;
	char	*rtname;
	scds_handle_t handle;
	const	char *zone;

	/*
	 * The callback parameters are
	 * progname mon/init rgname rsname failed/updated/repaired ipmp_name
	 * delay/nodelay
	 */
	if (argc < 7) {
		/* scds_syslog_initialize has not yet been called */
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		syslog(LOG_ERR, "Usage: %s mon/init RG_name R_name "
			"[zone] failed/updated/repaired ipmpname "
			"delay/nodelay", argv[0]);
		exit(1);
	}
	regmode = argv[1];
	rgname = argv[2];
	rsname = argv[3];

	if (argc == 8) {
		zone = argv[4];
		event = argv[5];
		ipmp_name = argv[6];
		action = argv[7];
	} else {
		zone = NULL;
		event = argv[4];
		ipmp_name = argv[5];
		action = argv[6];
	}

	/* we dont bother freeing this memory later */
	rtname = haip_get_rtname(zone, rsname, rgname);
	if (rtname == NULL) {
		syslog(LOG_ERR, "Failed to determine the resource type of %s",
		    rsname);
		exit(1);
	}

	fake_argc = 9;
	fake_argv[0] = argv[0];
	fake_argv[1] = "-R";
	fake_argv[2] = rsname;
	fake_argv[3] = "-G";
	fake_argv[4] = rgname;
	fake_argv[5] = "-T";
	fake_argv[6] = rtname;
	fake_argv[7] = "-Z";
	fake_argv[8] = (char *)zone;
	fake_argv[9] = NULL;

	if (scds_initialize(&handle, fake_argc, fake_argv) != 0) {
		scds_syslog(LOG_ERR, "%s initialization failure", argv[0]);
		exit(1);
	}

	scds_syslog_debug(DBG_LEVEL_LOW, "mode: %s, "
	    "rg: %s, r: %s, event:%s, IPMP:%s", regmode,
	    rgname, rsname, event, ipmp_name);

	/* get the ipmp group for this node */
	ipmp_group  = haip_get_ipmp_group(handle);
	if (ipmp_group == NULL) {
		(void) snprintf(internal_err_str, sizeof (internal_err_str),
		    "%s group name is not found in the netiflist for this "
		    "node.", pnm_group_name());
		(void) scha_resource_setstatus_zone(rsname, rgname, zone,
		    SCHA_RSSTATUS_FAULTED, internal_err_str);
		exit(1);
	}

	/*
	 * At this point the IPMP daemon is executing us via
	 * a call to system(). If we make RPC calls to it,
	 * there is a chance of deadlock. Thus we spawn
	 * a child and make the parent exit. The deadlock
	 * prevention and locking in the IPMP daemon is
	 * on an "best effort basis only", particularly
	 * when it comes to callbacks. Better play it safe.
	 */
	child_pid = fork();
	if (child_pid == -1) {
		/*
		 * If can't even fork(). There is something very
		 * wrong with the system. Let us not make it worse
		 * by doing other stuff.
		 */
		rc = errno; /*lint !e746 */
		(void) snprintf(internal_err_str, sizeof (internal_err_str),
			"Unable to fork(): %s.", strerror(rc));
		(void) scha_resource_setstatus_zone((char *)
		    scds_get_resource_name(handle),
		    (char *)scds_get_resource_group_name(handle), zone,
		    SCHA_RSSTATUS_FAULTED, internal_err_str);
		exit(1);
	}

	if (child_pid != 0) {
		scds_syslog_debug(DBG_LEVEL_LOW, "Parent exiting");
		exit(0);
	}
	/* Child continues here */

	if (haip_is_group_valid(ipmp_group) == B_FALSE) {
		(void) snprintf(internal_err_str, sizeof (internal_err_str),
		    "%s is not a valid IPMP group name on this node.",
		    ipmp_group);
		(void) scha_resource_setstatus_zone(rsname, rgname, zone,
		    SCHA_RSSTATUS_FAULTED, internal_err_str);
		exit(1);
	}

	if (strcmp(ipmp_name, ipmp_group) != 0) {
		(void) sprintf(internal_err_str, "Mismatched IPMP group "
		    "callback %s CCR %s", ipmp_name, ipmp_group);
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		exit(1);
	}

	/*
	 * If the IPMP group has been updated, re-plumb the
	 * mcnet module if the registration mode is INIT
	 */
	if (strcmp(event, "updated") == 0) {
		if (strcmp(regmode, IPMP_REG_INIT) == 0) {
			/* Replumb adapters */
			global_lockid = haip_serialize_lock(ALLIPMPS);
			ipmp_lockid = haip_serialize_lock(ipmp_group);
			(void) setup_autopush(ipmp_group);
			(void) haip_serialize_unlock(ipmp_lockid);
			(void) haip_serialize_unlock(global_lockid);
			return (0);
		}
		if (strcmp(regmode, IPMP_REG_MON) == 0) {
			return (0);	/* Don't do anything */
		}
		(void) snprintf(internal_err_str, sizeof (internal_err_str),
		    "Unknown IPMP regmode %s", regmode);

		/* ignore err return code above */
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		exit(1);
	}
	if (strcmp(event, "repaired") == 0) {
		if (strcmp(regmode, IPMP_REG_INIT) == 0)
			return (0); /* nothing to be done by init callback */

		if (strcmp(regmode, IPMP_REG_MON) == 0) {
			/*
			 * if the retry command was launched earlier, we'd
			 * like it to stop because the group has recovered
			 * now. The status message is also updated to reflect
			 * that.
			 */
			(void) scha_resource_setstatus_zone(
			    rsname, rgname, zone,
			    SCHA_RSSTATUS_OK, "SharedAddress online.");
			(void) haip_stop_retry(rgname, rsname, ipmp_name);
			return (0);
		}
		(void) snprintf(internal_err_str, sizeof (internal_err_str),
		    "Unknown IPMP regmode %s", regmode);

		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		exit(1);
	}
	if (strcmp(event, "failed") != 0) {
		(void) snprintf(internal_err_str, sizeof (internal_err_str),
		    "Unrecognized callback event %s.", event);

		/* ignore err return code above */
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		/* create the whole string */
		(void) snprintf(internal_err_str, sizeof (internal_err_str),
			"INTERNAL ERROR: Unrecognized callback event %s.",
			event);
		(void) scha_resource_setstatus_zone(rsname, rgname, zone,
			SCHA_RSSTATUS_FAULTED, internal_err_str);
		exit(1);
	}
	if (strcmp(regmode, IPMP_REG_INIT) == 0) {
		return (0);	/* Do nothing */
	}
	if (strcmp(regmode, IPMP_REG_MON) != 0) {
		(void) snprintf(internal_err_str, sizeof (internal_err_str),
		    "Unrecognized registration mode %s.", regmode);

		/* ignore err return code above */
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		(void) snprintf(internal_err_str, sizeof (internal_err_str),
			"INTERNAL ERROR: Unrecognized registration mode %s.",
			regmode);
		(void) scha_resource_setstatus_zone(rsname, rgname, zone,
			SCHA_RSSTATUS_FAULTED, internal_err_str);
		exit(1);
	}

	/* action can be delay or nodelay */
	if (strcmp(action, "delay") == 0) {
		/*
		 * We want to wait here for some time to make sure that
		 * IPMP state has stabilized on all nodes.
		 */
		haip_ipmp_delay();
		/*
		 * Check to see if the IPMP group is still bad..
		 */
		rc = haip_ipmpstatus(ipmp_group, ipmp_state);
		if (rc == 0) {		/* It appears to be OK now */
			/*
			 * SCMSGS
			 * @explanation
			 * The specified IPMP group is not in functional
			 * state. Logical host resource can't be started
			 * without a functional IPMP group.
			 * @user_action
			 * LogicalHostname resource will not be brought online
			 * on this node. Check for messages (cl_pnmd errors)
			 * encountered just before this message for any IPMP
			 * or adapter problem. Correct the problem and rerun
			 * the clresourcegroup command.
			 */
			scds_syslog(LOG_ERR, "IPMP group %s has status %s.",
			    ipmp_group, ipmp_state);
			exit(1);
		}
	} else {
		/*
		 * pnmd has requested an immediate failover. No need to
		 * wait in the hope that NIC will repair
		 */
		/*
		 * SCMSGS
		 * @explanation
		 * All network interfaces in the IPMP group have failed. All
		 * of these failures were detected by the hardware drivers for
		 * the network interfaces, and not in.mpathd. In such a
		 * situation cl_pnmd requests all highly available IP addresses
		 * (LogicalHostname and SharedAddress) to fail over to another
		 * node without any delay.
		 * @user_action
		 * No user action is needed. This is an informational message
		 * that indicates that the IP addresses will be failed over to
		 * another node immediately.
		 */
		scds_syslog(LOG_NOTICE, "cl_pnmd has requested an immediate "
		    "failover of all HA IP addresses hosted on IPMP group %s",
		    ipmp_group);

		/*
		 * wait for a very short (100ms) duration, just in case
		 * this is a cluster wide network outage. Ideally, all the
		 * nodes will know about the outage at the same instant.
		 * This is just an extra precaution.
		 */
		(void) usleep(100000);
	}

	/*
	 * Launch the utility hascip_retry which would
	 * retry the scha_control call.
	 */
	scds_syslog_debug(DBG_LEVEL_LOW, "Calling haip_retry()");

	(void) scha_resource_setstatus_zone(rsname, rgname, zone,
		SCHA_RSSTATUS_DEGRADED, "IPMP Failure.");

	rc = haip_start_retry(RETRY_PROG, rgname, rsname, ipmp_name, zone);
	if (rc < 0) {		/* Something went wrong */
		(void) snprintf(internal_err_str, sizeof (internal_err_str),
		    "Unable to launch failover utility %s.", RETRY_PROG);
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		exit(1);
	}

	scds_syslog(LOG_INFO, "Completed successfully.");

	scds_close(&handle);

	return (0);
}
