/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * hascip_monitor_start.c
 * Monitor Start method for highly available scalable ipaddress
 */

#pragma ident "@(#)hascip_monitor_start.c 1.24 08/05/20 SMI"

#include <stdio.h>
#include <stdlib.h>
#include <netdb.h>
#include <netinet/in.h>

#include "hascip.h"

/*
 * Register with IPMP so that it will call us back in case there
 * are problems with the IPMP group we are using.
 */

int
main(int argc, char *argv[])
{
	char 	*ipmp_group = NULL;
	int	rc = 0;
	char    internal_err_str[SCDS_ARRAY_SIZE];
	scds_handle_t handle;
	const char *rname, *rgname, *zone;
	char ipmpstate[MAXIPMPMSGLEN];

	if (scds_initialize(&handle, argc, argv) != 0) {
		scds_syslog(LOG_ERR, "%s initialization failure", argv[0]);
		exit(1);
	}

	rname = scds_get_resource_name(handle);
	rgname = scds_get_resource_group_name(handle);
	zone = scds_get_zone_name(handle);

	/* get the ipmp group for this node */
	ipmp_group  = haip_get_ipmp_group(handle);
	if (ipmp_group == NULL) {
		(void) snprintf(internal_err_str, sizeof (internal_err_str),
		    "%s group name is not found in the netiflist for this "
		    "node.", pnm_group_name());
		(void) scha_resource_setstatus_zone(rname, rgname, zone,
		    SCHA_RSSTATUS_FAULTED, internal_err_str);
		exit(1);
	}

	if (haip_is_group_valid(ipmp_group) == B_FALSE) {
		(void) snprintf(internal_err_str, sizeof (internal_err_str),
		    "%s is not a valid IPMP group name on this node.",
		    ipmp_group);
		(void) scha_resource_setstatus_zone(rname, rgname, zone,
		    SCHA_RSSTATUS_FAULTED, internal_err_str);
		exit(1);
	}

	rc = haip_register_callback((char *)rname, (char *)rgname,
	    (char *)zone, ipmp_group, IPMP_REG_MON, HASCIP_CALLBACK_PROG);
	if (rc != 0) {
		(void) snprintf(internal_err_str, sizeof (internal_err_str),
		    "Register callback with IPMP %s failed: Error %d",
		    ipmp_group, rc);
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		exit(1);
	}

	/*
	 * If the ipmp group is bad to begin with, failover.
	 *
	 * Note that we do this _after_ registering the callback.
	 * This is because the failover utility will likely cause
	 * monitor_stop to run. monitor_stop will then attempt to
	 * unregister the callbacks and logs an error if they
	 * arent registered.
	 */
	rc = haip_ipmpstatus(ipmp_group, ipmpstate);
	if (rc != 0) {
		scds_syslog(LOG_ERR, "IPMP group %s has status %s.",
		    ipmp_group, ipmpstate);
		(void) scha_resource_setstatus_zone(rname, rgname, zone,
		    SCHA_RSSTATUS_DEGRADED, "IPMP Failure.");

		/* launch the failover utility */
		rc = haip_start_retry(RETRY_PROG, (char *)rgname, (char *)rname,
		    ipmp_group, (char *)zone);
		if (rc < 0) {		/* Something went wrong */
			(void) snprintf(internal_err_str,
			    sizeof (internal_err_str),
			    "Unable to launch failover utility %s", RETRY_PROG);
			scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
			    internal_err_str);
			exit(1);
		}
	}

	scds_syslog(LOG_INFO, "Completed successfully.");

	scds_close(&handle);

	return (0);
}
