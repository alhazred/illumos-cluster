/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * hascip_retry.c - Utility to retry RGM scha_control(GIVEOVER)
 * operation upon IPMP failures.
 * argv[1] is the RG name
 * argv[2] is the resource name
 * argv[3] is the IPMP group name
 *
 * Started by hascip_ipmp_callback under PMF tag.
 * PMF tag is <RGname>.<resource-name>.<ipmp_name>.failover
 *
 * It exits when either of the following conditions is met
 * scha_control(GIVEOVER) is successful
 * IPMP group status is no longer DOWN
 * RG is no longer mastered on the local node.
 *
 * In addition a monitor_stop operation on the SharedAddress
 * resource kills it.
 *
 */


/*
 * If a scha_control() call fails. We are going to
 * retry it after sleeping a bit. The idea is that
 * the original scha_control() call might have
 * failed because there is no healthy node. If
 * a IPMP group on some other node comes up, we
 * want to use it. For a DOWN adapter, IPMP could
 * take upto 60 seconds in between retrying the
 * various adapters in the group. Also, once a
 * test is performed on an adapter, it could take
 * upto 30 seconds for the adapter to be declared
 * healthy. We use a conservative interval of
 * 3 minutes in between scha_control() attempts.
 * IPMP_WAIT is that interval.
 */

#define	IPMP_WAIT	180

#pragma ident "@(#)hascip_retry.c 1.15 08/07/31 SMI"

#include "hascip.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/wait.h>
#include <syslog.h>
#include <rgm/haip.h>

int
main(int argc, char *argv[])
{
	char	*ipmp_group = NULL;
	int	rc;
	char    ipmp_state[MAXIPMPMSGLEN];
	char	*rgname, *rsname, *ipmp_name, *zone_name;
	int	fake_argc;
	char	*fake_argv[10];
	char	internal_err_str[SCDS_ARRAY_SIZE];
	char	*rtname;
	scds_handle_t handle;

	/*
	 * The command line is
	 * progname rgname rsname ipmp_name
	 */
	if (argc < 4) {
		/* scds_syslog_initialize has not been called yet */
		syslog(LOG_ERR, "%s RG_name R_name ipmpname [zone]", argv[0]);
		exit(1);
	}
	rgname = argv[1];
	rsname = argv[2];
	ipmp_name = argv[3];

	if (argc == 5) {
		zone_name = argv[4];
	} else {
		zone_name = NULL;
	}

	/* we dont bother freeing this memory later */
	rtname = haip_get_rtname(zone_name, rsname, rgname);
	if (rtname == NULL) {
		syslog(LOG_ERR, "Failed to determine the resource type of %s",
		    rsname);
		exit(1);
	}

	fake_argc = 9;
	fake_argv[0] = argv[0];
	fake_argv[1] = "-R";
	fake_argv[2] = rsname;
	fake_argv[3] = "-G";
	fake_argv[4] = rgname;
	fake_argv[5] = "-T";
	fake_argv[6] = rtname;
	fake_argv[7] = "-Z";
	fake_argv[8] = (char *)zone_name;
	fake_argv[9] = NULL;

	if (scds_initialize(&handle, fake_argc, fake_argv) != 0) {
		scds_syslog(LOG_ERR, "%s initialization failure", argv[0]);
		exit(1);
	}

	scds_syslog_debug(DBG_LEVEL_LOW, "rg: %s, r: %s, IPMP:%s",
	    rgname, rsname, ipmp_name);

	/* get the ipmp group for this node */
	ipmp_group  = haip_get_ipmp_group(handle);
	if (ipmp_group == NULL) {
		(void) sprintf(internal_err_str,
		    "%s group name is not found in the netiflist for this "
		    "node.", pnm_group_name());
		(void) scha_resource_setstatus_zone(rsname, rgname, zone_name,
		    SCHA_RSSTATUS_FAULTED, internal_err_str);
		exit(1);
	}

	if (strcmp(ipmp_name, ipmp_group) != 0) {
		(void) sprintf(internal_err_str, "Mismatched IPMP group "
		    "callback %s CCR %s", ipmp_name, ipmp_group);
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		exit(1);
	}

	/*
	 * OK. Call scha_control() to move this RG someplace else
	 *
	 * If the scha_control() attempts fails for some reason,
	 * we need to retry that operation.
	 */
	for (; ; ) {
		scds_syslog_debug(DBG_LEVEL_LOW, "Calling scha_control()");

		rc = scha_control_zone
		    (SCHA_GIVEOVER, rgname, rsname, zone_name);

		if (rc == SCHA_ERR_NOERR) {

			/*
			 * scha_control() succeded. We should have
			 * been killed by the Monitor_stop method.
			 * It would have printed error messages.
			 * We should not continue none-the-less.
			 * Break out.
			 */

			break;
		} else if (rc == SCHA_ERR_RECONF) {
			/*
			 * Cluster is already reconfiguring. This could
			 * happen if there are > 1 haips in an RG and the
			 * network fails. All of them will try the scha_control
			 * giveover (above). The first one's request goes
			 * through but the others get this error.
			 *
			 * Log a less alarming message in this case.
			 */
			scds_syslog(LOG_NOTICE,
			    "Cluster undergoing reconfiguration. Will retry.");
		} else if (rc == SCHA_ERR_STATE) {
			/*
			 * One or more resources are either starting or stopping
			 * Resource Group is not in a valid state for giveover
			 * Log less alarming message and retry.
			 *
			 * This is done to fix RFE 6278678: Failover IP should
			 * handle SCHA_ERR_STATE and SCHA_ERR_CHECKS for
			 * better logging purpose.
			 */
			scds_syslog(LOG_INFO, "Failover attempt failed: "
				"resource group is currently starting or "
				"stopping. Will retry.");
		} else if (rc == SCHA_ERR_CHECKS) {
			/*
			 * No other node found that can host this RG
			 * Log less alarming message and retry.
			 *
			 * This is done to fix RFE 6278678: Failover IP should
			 * handle SCHA_ERR_STATE and SCHA_ERR_CHECKS for
			 * better logging purpose.
			 */
			scds_syslog(LOG_INFO, "Failover attempt failed: "
				"no other node is currently available to "
				"host the resource group.  Will retry.");
		} else {
			/* Failover attempt failed. syslog the reason */
			scds_syslog(LOG_ERR, "Failover attempt failed: "
			"Will retry. Error is: %s.", scds_error_string(rc));
		}

		/*
		 * wait for some time and retry.
		 */

		(void) sleep(IPMP_WAIT);

		/*
		 * Just woke up after a sleep. If IPMP group has
		 * repaired itself, we don't want to retry.
		 */
		rc = haip_ipmpstatus(ipmp_group, ipmp_state);
		if (rc == 0) {		/* It appears to be OK now */
			break;
		}
		/*
		 * Check to see that the RG is still on the
		 * local node, it might have moved away to
		 * some other node for other reasons. If so,
		 * we should exit.
		 */


		if (haip_rg_local(zone_name, rgname) != B_TRUE) {
			break;
		}
	}

	/*
	 * We come here upon breaking out of the for loop.
	 * Either the IPMP group has been repaired, or
	 * the RG has moved elsewhere. Set the resource status
	 * to OK and exit OK. Call haip_stop_retry() to
	 * inform PMF that we are about to exit. haip_stop_retry()
	 * would kill this program via PMF using SIGTERM, so
	 * we never really get to execute that return (0)
	 * statement below.
	 */
	(void) scha_resource_setstatus_zone(rsname, rgname, zone_name,
		SCHA_RSSTATUS_OK, "SharedAddress online.");
	(void) haip_stop_retry(rgname, rsname, ipmp_name);

	return (0);
}
