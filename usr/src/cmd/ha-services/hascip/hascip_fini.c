/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * hascip_fini.c - FINI method for highly available scalable ipaddress
 */

#pragma ident	"@(#)hascip_fini.c	1.30	08/07/25 SMI"

#include "hascip.h"

#include <stdio.h>
#include <stdlib.h>
#include <netdb.h>
#include <netinet/in.h>

/*
 * Called on all nodes where the RT is installed (= ALL cluster nodes)
 * Just remove all the ipaddresses from the loopback interfaces.
 *
 */

int
main(int argc, char *argv[])
{
	char 	*ipmp_group = NULL;
	int lockid, rc, instances;
	char internal_err_str[SCDS_ARRAY_SIZE];
	scds_handle_t handle;
	scha_str_array_t *rgnlist;
	scha_str_array_t *rg_nodelist;
	in6addr_list_t al;
	boolean_t node_in_rgnlist;
	const char *zone_name;

	if (scds_initialize(&handle, argc, argv) != 0) {
		scds_syslog(LOG_ERR, "%s initialization failure", argv[0]);
		exit(1);
	}

	/* get rg nodelist */
	rgnlist = (scha_str_array_t *)scds_get_rg_nodelist(handle);

	/* parse the nodelist from the node:zone string */
	rc = hascip_parse_nodelist(rgnlist, &rg_nodelist);
	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to parse the Nodelist property");
		exit(1);
	}

	zone_name = scds_get_zone_name(handle);

	/* check if this node is in the rg nodelist */
	node_in_rgnlist = haip_is_node_in_rgnodelist(zone_name, rg_nodelist);


	/*
	 * If the node is in rgnlist (could be in auxlist too - we dont care),
	 * we unplumb stuff on lo0 based on the instances of the IPMP group.
	 * If the node is not in rgnlist we unplumb all IPs that we can.
	 * Because haip_hostlist_to_addrlist() does an AI_ADDRCONFIG type of
	 * call, we end up doing the right thing.
	 */
	if (node_in_rgnlist) {
		/* get the ipmp group for this node */
		ipmp_group  = haip_get_ipmp_group(handle);
		if (ipmp_group == NULL) {
			exit(1);
		}

		if (haip_is_group_valid(ipmp_group) == B_FALSE) {
			/*
			 * SCMSGS
			 * @explanation
			 * Validation of the adapter information has failed.
			 * The specified IPMP group does not exist on this
			 * node.
			 * @user_action
			 * Create appropriate IPMP group on this node or
			 * recreate the logical host with correct IPMP group.
			 */
			scds_syslog(LOG_ERR, "%s is not a valid IPMP group "
			    "name on this node.", ipmp_group);
			exit(1);
		}

		if (pnm_get_instances_direct(ipmp_group, &instances) != 0) {
			/*
			 * SCMSGS
			 * @explanation
			 * There was a failure while trying to determine the
			 * instances (v4, v6 or both) that the IPMP group can
			 * host.
			 * @user_action
			 * Contact your authorized Sun service provider.
			 */
			scds_syslog(LOG_ERR, "Failed to enumerate instances "
			    "for IPMP group %s", ipmp_group);
			exit(1);
		}
	} else {
		/*
		 * Dont bother with any IPMP group;
		 * just unplumb everything that can be unplumbed
		 */
		instances = PNMV4MASK | PNMV6MASK;
	}

	/*
	 * instances has been set according to the IPMP group (if node is in
	 * rgnlist) or to everything
	 */
	rc = haip_hostlist_to_addrlist(handle, &al, instances);
	if (rc != 0) {
		scds_syslog(LOG_ERR, "Failed to obtain list of IP "
		    "addresses for this resource");
		exit(1);
	}

	lockid = haip_serialize_lock(LOOPBACK);
	(void) hascip_remove_resource_from_loopback(al);
	(void) haip_serialize_unlock(lockid);

	/*
	 * Rest is applicable to a node in rgnlist only.
	 * The stop method should have previously unregistered the callback if
	 * this node is not in the rg nodelist.
	 */
	if (node_in_rgnlist == B_FALSE)
		goto finished;

	/* Unregister with IPMP */
	rc = haip_unregister_callback((char *)scds_get_resource_name(handle),
	    ipmp_group, IPMP_REG_INIT);
	if (rc != 0) {
		(void) snprintf(internal_err_str, sizeof (internal_err_str),
		    "Unregister callback with IPMP %s failed.  Error %d.",
		    ipmp_group, rc);
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		exit(1);
	}

finished:
	scds_syslog(LOG_INFO, "Completed successfully.");

	scds_close(&handle);
	return (0);
}
