/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * hascip_update.c - Update method for highly available scalable ipaddress
 */

#pragma ident "@(#)hascip_update.c 1.25 08/05/20 SMI"

#include "hascip.h"

#include <stdlib.h>
#include <netdb.h>
#include <netinet/in.h>
#include <libintl.h>

/*
 * The only extension property which is dynamically updatable
 * in SUNW.SharedAddress is NetIfList. Those updates are
 * checked in the VALIDATE methods. The VALIDATE check
 * disallows changing of IPMP adapters. The end result is
 * that while IPMP group for a individual node cannot
 * be changed, the nodelist of the RG can be updated (new
 * nodes are added and deleted).
 *
 * In UPDATE method, essentially no work needs to be done.
 * To be doublly sure, we just check if the IPMP group for
 * this node is specified and is valid.
 *
 */

int
main(int argc, char *argv[])
{
	char *ipmp_group = NULL;
	scds_handle_t handle;

	if (scds_initialize(&handle, argc, argv) != 0) {
		scds_syslog(LOG_ERR, "%s initialization failure", argv[0]);
		exit(1);
	}

	/* get the ipmp group for this node */
	ipmp_group  = haip_get_ipmp_group(handle);
	if (ipmp_group == NULL) {
		exit(1);
	}

	if (haip_is_group_valid(ipmp_group) == B_FALSE) {
		scds_syslog(LOG_ERR, "%s is not a valid IPMP group name on "
		    "this node.", ipmp_group);
		exit(1);
	}

	scds_syslog(LOG_INFO, "Completed successfully.");
	scds_close(&handle);

	return (0);
}
