/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * hascip_stop.c - Stop method for highly available scalable ipaddress
 */

#pragma ident "@(#)hascip_stop.c 1.30 08/05/20 SMI"

#include "hascip.h"

#include <stdio.h>
#include <stdlib.h>
#include <netdb.h>
#include <netinet/in.h>

/*
 * STOP method for SharedAddress implementation.
 * Called to move the ipaddress from the IPMP group
 * to the loopback. The sequence of operations are
 * 1. ifconfig DOWN on the IPMP adaptor
 * 2. bring ip UP on the loopback
 * 3. UNPLUMB ip from the IPMP adapter.
 *
 * This sequence assures us that the ipaddress is
 * always present on the node, either on the loopback
 * or on the IPMP adapter. There might be some time
 * windows where the ip is DOWN, but there is no
 * time window where the ip does not exist on
 * the system.
 */

int
main(int argc, char *argv[])
{
	char *ipmp_group = NULL;
	int lockid, rc, instances;
	char internal_err_str[SCDS_ARRAY_SIZE];
	in6addr_list_t al;
	scds_handle_t handle;
	const char *rname, *rgname, *zone;

	if (scds_initialize(&handle, argc, argv) != 0) {
		scds_syslog(LOG_ERR, "%s initialization failure", argv[0]);
		exit(1);
	}

	rname = scds_get_resource_name(handle);
	rgname = scds_get_resource_group_name(handle);
	zone = scds_get_zone_name(handle);

	/* get the ipmp group for this node */
	ipmp_group  = haip_get_ipmp_group(handle);
	if (ipmp_group == NULL) {
		(void) snprintf(internal_err_str, sizeof (internal_err_str),
		    "%s group name is not found in the netiflist for this "
		    "node.", pnm_group_name());
		(void) scha_resource_setstatus_zone(rname, rgname, zone,
		    SCHA_RSSTATUS_FAULTED, internal_err_str);
		exit(1);
	}

	/*
	 * Validate the IPMP adapter name specified in
	 * the resource configuration.
	 */
	if (haip_is_group_valid(ipmp_group) == B_FALSE) {
		(void) snprintf(internal_err_str, sizeof (internal_err_str),
		    "%s is not a valid IPMP group name on this node.",
		    ipmp_group);
		(void) scha_resource_setstatus_zone(rname, rgname, zone,
		    SCHA_RSSTATUS_FAULTED, internal_err_str);
		exit(1);
	}

	/*
	 * We try to remove monitor callback if remains registered
	 * even after monitor_stop executes, useful specially when
	 * hascip_monitor_stop does not complete successfully.
	 * Note: Does not have any side effect even if hascip_monitor_stop
	 * completes successfully
	 */
	rc = haip_unregister_callback((char *)rname, ipmp_group, IPMP_REG_MON);
	if (rc != 0) {
		/*
		 * Need to be idempotent, so dont error out here. Just log
		 * an error message and leave it at that.
		 */
		(void) snprintf(internal_err_str, sizeof (internal_err_str),
		    "Unregister callback with IPMP %s failed.  Error %d",
		    ipmp_group, rc);
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
	}

	/* make a list of the IP addresses on which we'll work */
	if (pnm_get_instances_direct(ipmp_group, &instances) != 0) {
		(void) snprintf(internal_err_str, sizeof (internal_err_str),
		    "Failed to enumerate instances for IPMP group %s",
		    ipmp_group);
		(void) scha_resource_setstatus_zone(rname, rgname, zone,
		    SCHA_RSSTATUS_FAULTED, internal_err_str);
		exit(1);
	}

	rc = haip_hostlist_to_addrlist(handle, &al, instances);
	if (rc != 0) {
		(void) scha_resource_setstatus_zone(rname, rgname, zone,
		    SCHA_RSSTATUS_FAULTED,
		    "Failed to obtain list of IP addresses for this resource");
		exit(1);
	}

	/*
	 * First, mark the address DOWN on the IPMP group
	 * We ignore any failures here because we would be
	 * unplumbing the logical address a little later
	 * anyway.
	 * There is a legit case where this operation could
	 * fail, when the START method did not succeed (because
	 * perhaps the IPMP group was DOWN), there is no
	 * logical interface, so attempt to IFCONFIG_DOWN
	 * it would fail.
	 */
	rc = haip_do_ipmp_op(ipmp_group, zone, PNM_IFCONFIG_DOWN, al);
	if (rc > 0) {
		/* None of the ops succeeded */
		/*
		 * SCMSGS
		 * @explanation
		 * Failed to unplumb any ip addresses. The resource cannot be
		 * brought offline. Node will be rebooted by Sun cluster.
		 * @user_action
		 * Check the syslog messages from other components for
		 * possible root cause. Save a copy of /var/adm/messages and
		 * contact Sun service provider for assistance in diagnosing
		 * and correcting the problem.
		 */
		scds_syslog(LOG_ERR, "Could not unplumb any ipaddresses.");
	}

	/*
	 * Then, Add ipaddresses to loopback, this
	 * brings it up as well. See the detailed comment
	 * about locking issues in the prenet_start method
	 * comments.
	 */
	lockid = haip_serialize_lock(LOOPBACK);
	(void) hascip_add_resource_to_loopback(al, (char *)zone);
	(void) haip_serialize_unlock(lockid);

	/*
	 * A Note about failure handling in the loopback
	 * handling code.
	 * The adding of ips to loopback could fail due
	 * to a variety of issues, mostly failures of
	 * Solaris socket ioctls such as SIOCADDIF, SIFFLAGS,
	 * etc. There are two approaches we could take
	 *
	 * 1. exit(1) here, putting the resource in STOP_FAILED
	 * 		state. The SharedAddress is not on-line on any
	 *		node and all scalable services using this
	 *		SharedAddress are not providing service. The
	 *		admin would notice the STOP_FAILED state and
	 *		fix the situation (perhaps by rebooting the node,
	 *		or maybe just clearing the logical interfaces)
	 *		and bring the SharedAddress on-line via scswitch.
	 *
	 * 2. syslog() an error message [done by
	 *		hascip_add_resource_to_loopback() upon any failure.]
	 *		and continue. The SharedAddress would be failed over
	 *		by RGM to some other node. On this node, the
	 *		SharedAddress would not be available on loopback.
	 *		Eventually the admin would fix that situation,
	 *		perhaps by rebooting the node. In this situation,
	 *		nodes other then this node continue to provide
	 *		service.
	 *
	 * We go with approach (2) here.
	 *
	 * Reconsidering above: IPMP is idempotent in its operations,
	 *	so that means that even if the LIF is not plumbed, it
	 * would still return success for UNPLUMB operations. Note
	 * that this works even if the LIF is in "bad" state, i.e.
	 * IFCONFIG_DOWN operation had problems. What it boils
	 * down to is the following, if we are unable to UNPLUMB
	 * the LIF here, it indicates a problem and perhaps
	 * continuing blissfully is not a good thing.
	 * We resort to taking approach (1), i.e. exit failure.
	 * This would most likely be an extreme case.
	 * We continue to follow the philosphy with the partial
	 * failures, which is that a partial failure(some LIFs
	 * unplumbed, some not) results in a warning.
	 */

	/* Now, last step, unplumb addresses from IPMP group */
	rc = haip_do_ipmp_op(ipmp_group, zone, PNM_IFCONFIG_UNPLUMB, al);
	if (rc > 0) {
		/* None of the ops succeeded */
		(void) scha_resource_setstatus_zone(rname, rgname, zone,
		    SCHA_RSSTATUS_FAULTED,
		    "Could not unplumb any ipaddresses.");

		/* We exit failure here: See discussion above */
		exit(1);

	} else if (rc == 0) {
		(void) scha_resource_setstatus_zone(rname, rgname, zone,
		    SCHA_RSSTATUS_OFFLINE, "SharedAddress offline.");
	} else {
		(void) scha_resource_setstatus_zone(rname, rgname, zone,
		    SCHA_RSSTATUS_DEGRADED,
		    "Could not unplumb some ipaddresses.");
	}
	/*
	 * SCMSGS
	 * @explanation
	 * The stop method is completed and the resource is stopped.
	 * @user_action
	 * This is informational message. No user action required.
	 */
	scds_syslog(LOG_INFO, "SharedAddress stopped.");

	scds_close(&handle);
	return (0);
}
