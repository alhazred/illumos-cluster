/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * hafoip_start.c - Start method for highly available failover ipaddress
 */

#pragma ident	"@(#)hafoip_start.c	1.27	09/04/23 SMI"

#include <sys/os.h>
#include <stdio.h>
#include <stdlib.h>
#include <netdb.h>
#include <netinet/in.h>
#include <rgm/haip.h>

#include "hafoip.h"

/*
 * Call IPMP interface to bring up the ipaddress which was configured
 * by the PRENET_START method (the logical ipaddresses were added
 * but they were not brought UP at that time).
 */

int
main(int argc, char *argv[])
{
	char 	*ipmp_group = NULL;
	int rc, instances;
	char internal_err_str[SCDS_ARRAY_SIZE];
	scds_handle_t handle;
	in6addr_list_t al;
	struct in6_addr firstip;
	const char *rname, *rgname, *zone;

	if (scds_initialize(&handle, argc, argv) != 0) {
		scds_syslog(LOG_ERR, "%s initialization failure", argv[0]);
		exit(1);
	}

	rname = scds_get_resource_name(handle);
	rgname = scds_get_resource_group_name(handle);
	zone = scds_get_zone_name(handle);

	/* get the ipmp group for this node */
	ipmp_group  = haip_get_ipmp_group(handle);
	if (ipmp_group == NULL) {
		(void) sprintf(internal_err_str,
		    "%s group name is not found in the netiflist for "
		    "this node.", pnm_group_name());
		(void) scha_resource_setstatus_zone(rname, rgname, zone,
		    SCHA_RSSTATUS_FAULTED, internal_err_str);
		exit(1);
	}

	if (haip_is_group_valid(ipmp_group) == B_FALSE) {
		(void) sprintf(internal_err_str,
		    "%s is not a valid %s group name on this node.",
		    ipmp_group, pnm_group_name());
		(void) scha_resource_setstatus_zone(rname, rgname, zone,
		    SCHA_RSSTATUS_FAULTED, internal_err_str);
		exit(1);
	}

	if (pnm_get_instances_direct(ipmp_group, &instances) != 0) {
		(void) sprintf(internal_err_str,
		    "Failed to enumerate instances for %s group %s.",
		    pnm_group_name(), ipmp_group);
		(void) scha_resource_setstatus_zone(rname, rgname, zone,
		    SCHA_RSSTATUS_FAULTED, internal_err_str);
		exit(1);
	}

	rc = haip_hostlist_to_addrlist(handle, &al, instances);
	if (rc != 0) {
		(void) scha_resource_setstatus_zone(rname, rgname, zone,
		    SCHA_RSSTATUS_FAULTED,
		    "Failed to obtain list of IP addresses for this resource.");
		exit(1);
	}

	if (al.count <= 0) {
		(void) scha_resource_setstatus_zone(rname, rgname, zone,
		    SCHA_RSSTATUS_FAULTED,
		    "No usable IP addresses for this resource.");
		exit(1);
	}
	bcopy(al.addr, &firstip, sizeof (firstip));

	/*
	 * Try to Make sure the resource is not up on some other
	 * node. We make the best effort here. If the first ip
	 * in the resource is up even after HAFOIP_SAFE_DELAY,
	 * we takeover the ipaddress anyway.
	 */
	rc = haip_safe_wait(firstip, HAFOIP_SAFE_DELAY);

#if (SOL_VERSION >= __s11)
	/*
	 *
	 * With the Solaris IP duplicate address detection (DAD)
	 * feature, the IP address will be marked as DUPLICATE, and
	 * won't be functional here if it's already in use elsewhere.
	 * So, taking over the IP address will be less releveant and will
	 * not be useful. Hence report the failure and return error if the
	 * address is pingable.
	 *
	 */
	if (rc == 1) {
		/*
		 * SCMSGS
		 * @explanation
		 * The resource is failed to start on this node because
		 * the IP address is already in use.
		 * @user_action
		 * User can specify a new IP address/hostname to
		 * create this resource. Otherwise, the IP address can
		 * be brought down where it is in-use before
		 * bringing the resource online.
		 */
		scds_syslog(LOG_ERR, "Failed to bring the resource online"
		    " because the IP address is already in use.");
		exit(1);
	}
#endif
	rc = haip_do_ipmp_op(ipmp_group, zone, PNM_IFCONFIG_UP, al);
	free(al.addr); /* done with the list */
	if (rc > 0) {
		/* None of the ops succeeded */
		(void) sprintf(internal_err_str,
		    "Could not %s any ip addresses.", "bring up");
		(void) scha_resource_setstatus_zone(rname, rgname, zone,
		    SCHA_RSSTATUS_FAULTED, internal_err_str);
		exit(1);
	}
	if (rc < 0) {
		/* Some of the ops succeeded, but not all */
		(void) scha_resource_setstatus_zone(rname, rgname, zone,
		    SCHA_RSSTATUS_DEGRADED,
		    "Not all hostnames brought online.");
	}

	/*
	 * XXX Regarding partial successes, see the detailed comment
	 * in the prenet_start method after the do_ipmp() call.
	 */

	if (rc == 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * The status of the logicalhost resource is online.
		 * @user_action
		 * This is informational message. No user action required.
		 */
		scds_syslog(LOG_INFO, "LogicalHostname online.");
		(void) scha_resource_setstatus_zone(rname, rgname, zone,
		    SCHA_RSSTATUS_OK, "LogicalHostname online.");
	}

	scds_close(&handle);

	return (0);
}
