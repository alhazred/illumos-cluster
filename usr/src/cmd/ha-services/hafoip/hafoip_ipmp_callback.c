/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * hafoip_ipmp_callback.c - Callback utility registered with IPMP to
 * call us back when the IPMP group goes bad.
 */

#pragma ident	"@(#)hafoip_ipmp_callback.c	1.37	08/07/25 SMI"

#include <sys/os.h>
#include <stdio.h>
#include <stdlib.h>
#include <netdb.h>
#include <syslog.h>
#include <netinet/in.h>
#include <rgm/haip.h>

#include "hafoip.h"
#include <errno.h>
#include <string.h>

/*
 * Call scha_control() to cause the containing RG to be moved
 * to some other node.
 * The scha_control() call is now made in a separate program
 * hafoip_retry. The scha_control() call is retried by that
 * utility if it fails. We launch it under PMF via
 * haip_start_retry() routine.
 *
 */

int
main(int argc, char *argv[])
{
	char	*ipmp_group = NULL;
	char    ipmp_state[MAXIPMPMSGLEN];
	int	rc = 0, child_pid;
	char	*rgname, *rsname, *ipmp_name, *regmode, *event, *action;
	int	fake_argc;
	char	*fake_argv[10];
	char	internal_err_str[SCDS_ARRAY_SIZE];
	char	*rtname;
	scds_handle_t handle;
	const	char *zone;

	/*
	 * The callback string is
	 * "progname "mon" rgname rsname failed/updated/repaired ipmp_name
	 * delay/nodelay"
	 */

	if (argc < 7) {
		/*
		 * scds_syslog_initialize has not been called yet,
		 * use syslog here (the system will use reasonable
		 * defaults for tag etc.)
		 */
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		syslog(LOG_ERR, "Usage: %s mon RG_name R_name "
		    "[zone] failed/updated/repaired ipmpname delay/nodelay",
		    argv[0]);
		exit(1);
	}
	regmode = argv[1];
	rgname = argv[2];
	rsname = argv[3];

	if (argc == 8) {
		zone = argv[4];
		event = argv[5];
		ipmp_name = argv[6];
		action = argv[7];
	} else {
		zone = NULL;
		event = argv[4];
		ipmp_name = argv[5];
		action = argv[6];
	}
	/* we dont bother freeing this memory later */
	rtname = haip_get_rtname(zone, rsname, rgname);
	if (rtname == NULL) {
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		syslog(LOG_ERR, "Failed to determine the resource type of %s",
		    rsname);
		exit(1);
	}

	fake_argc = 9;
	fake_argv[0] = argv[0];
	fake_argv[1] = "-R";
	fake_argv[2] = rsname;
	fake_argv[3] = "-G";
	fake_argv[4] = rgname;
	fake_argv[5] = "-T";
	fake_argv[6] = rtname;
	fake_argv[7] = "-Z";
	fake_argv[8] = (char *)zone;
	fake_argv[9] = NULL;

	if (scds_initialize(&handle, fake_argc, fake_argv) != 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * Failed to initialize the hafoip or hascip callback method.
		 * @user_action
		 * Retry the operation. If the error persists, contact your
		 * Sun service representative.
		 */
		scds_syslog(LOG_ERR, "%s initialization failure", argv[0]);
		exit(1);
	}

	scds_syslog_debug(DBG_LEVEL_LOW,
	    " mode: %s, rg: %s, r: %s, event: %s, %s: %s, action: %s",
	    regmode, rgname, rsname, event, pnm_group_name(),
	    ipmp_name, action);

	/* NOOP if the callback mode is update */
	if (strcmp(event, "updated") == 0) {
		scds_syslog_debug(DBG_LEVEL_LOW, "Update callback");
		exit(0);
	}

	/* get the ipmp group for this node */
	ipmp_group  = haip_get_ipmp_group(handle);
	if (ipmp_group == NULL) {
		(void) sprintf(internal_err_str,
		    "%s group name is not found in the netiflist for "
		    "this node.", pnm_group_name());
		(void) scha_resource_setstatus_zone(rsname, rgname, zone,
		    SCHA_RSSTATUS_FAULTED, internal_err_str);
		exit(1);
	}

	/*
	 * At this point the IPMP daemon is executing us via
	 * a call to system(). If we make RPC calls to it,
	 * there is a chance of deadlock. Thus we spawn
	 * a child and make the parent exit. The deadlock
	 * prevention and locking in the IPMP daemon is
	 * on an "best effort basis only", particularly
	 * when it comes to callbacks. Better play it safe.
	 */
	child_pid = fork();
	if (child_pid == -1) {
		/*
		 * If can't even fork(). There is something very
		 * wrong with the system. Let us not make it worse
		 * by doing other stuff.
		 */
		(void) sprintf(internal_err_str, /*CSTYLED*/
		    "Unable to fork(): %s.", strerror(errno)); /*lint !e746 */
		(void) scha_resource_setstatus_zone(rsname, rgname, zone,
		    SCHA_RSSTATUS_FAULTED, internal_err_str);
		exit(1);
	}

	if (child_pid != 0) {
		scds_syslog_debug(DBG_LEVEL_LOW, "Parent exiting");
		exit(0);
	}
	/* Child continues here */

	if (haip_is_group_valid(ipmp_group) == B_FALSE) {
		(void) sprintf(internal_err_str,
		    "%s is not a valid %s group name on this node.",
		    ipmp_group, pnm_group_name());
		(void) scha_resource_setstatus_zone(rsname, rgname, zone,
		    SCHA_RSSTATUS_FAULTED, internal_err_str);
		exit(1);
	}

	if (strcmp(ipmp_name, ipmp_group) != 0) {
		(void) sprintf(internal_err_str, "Mismatched %s group "
		    "callback %s CCR %s", pnm_group_name(),
		    ipmp_name, ipmp_group);
		/*
		 * SCMSGS
		 * @explanation
		 * An internal error has occurred.
		 * @user_action
		 * Save a copy of the /var/adm/messages files on all nodes.
		 * Contact your authorized Sun service provider for assistance
		 * in diagnosing the problem.
		 */
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		exit(1);
	}

	/* This script is registered by monitor method only */
	if (strcmp(regmode, IPMP_REG_MON) != 0) {
		(void) sprintf(internal_err_str,
		    "Unrecognized callback registration mode %s", regmode);
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		exit(1);
	}

	if (strcmp(event, "repaired") == 0) {
		/*
		 * if the retry command was launched earlier, we'd
		 * like it to stop because the group has recovered
		 * now. The status message is also updated to reflect
		 * that.
		 */
		(void) scha_resource_setstatus_zone(rsname, rgname, zone,
		    SCHA_RSSTATUS_OK, "LogicalHostname online.");
		(void) haip_stop_retry(rgname, rsname, ipmp_name);
		exit(0);
	}

	if (strcmp(event, "failed") != 0) {
		(void) sprintf(internal_err_str,
		    "Unrecognized callback mode %s", event);
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		exit(1);
	}

	/* action can be delay or nodelay */
	if (strcmp(action, "delay") == 0) {
		/*
		 * We want to wait here for some time to make sure that
		 * IPMP state has stabilized on all nodes.
		 */
		haip_ipmp_delay();
		/*
		 * Check to see if the IPMP group is still bad..
		 */
		rc = haip_ipmpstatus(ipmp_group, ipmp_state);
		if (rc == 0) {		/* It appears to be OK now */
			/*
			 * SCMSGS
			 * @explanation
			 * Need explanation of this message!
			 * @user_action
			 * Need a user action for this message.
			 */
			scds_syslog(LOG_ERR, "%s group %s has status %s.",
			    pnm_group_name(), ipmp_group, ipmp_state);
			exit(1);
		}
	} else {
		/*
		 * pnmd has requested an immediate failover. No need to
		 * wait in the hope that NIC will repair
		 */
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		scds_syslog(LOG_NOTICE, "cl_pnmd has requested an immediate "
		    "failover of all HA IP addresses hosted on %s group %s",
		    pnm_group_name(), ipmp_group);

		/*
		 * wait for a very short (100ms) duration, just in case
		 * this is a cluster wide network outage. Ideally, all the
		 * nodes will know about the outage at the same instant.
		 * This is just an extra precaution.
		 */
		(void) usleep(100000);
	}


	(void) sprintf(internal_err_str,
	    "%s Failure.", pnm_group_name());
	(void) scha_resource_setstatus_zone(rsname, rgname, zone,
	    SCHA_RSSTATUS_DEGRADED, internal_err_str);

	/*
	 * Launch the utility hafoip_retry which would
	 * retry the scha_control call.
	 */
	scds_syslog_debug(DBG_LEVEL_LOW, "Calling haip_start_retry().");
	rc = haip_start_retry(RETRY_PROG, rgname, rsname, ipmp_name, zone);
	if (rc < 0) {		/* Something went wrong */
		(void) sprintf(internal_err_str,
		    "Unable to launch failover utility %s", RETRY_PROG);
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
	} else
		scds_syslog(LOG_INFO, "Completed successfully.");

	/*
	 * Actually, we arent very particular about cleaning up this stuff
	 * explicitly - this can be seen from the whole bunch of exit()s above.
	 */
	scds_close(&handle);

	return (0);
}
