/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * hafoip_monitor_check.c
 * Monitor Check method for highly available failover ipaddress
 */

#pragma ident	"@(#)hafoip_monitor_check.c	1.23	08/05/20 SMI"

#include <stdio.h>
#include <stdlib.h>
#include <netdb.h>
#include <netinet/in.h>

#include "hafoip.h"

/*
 * Check with IPMP about the current status of the IPMP group.
 * exit non-zero if the IPMP status is not good.
 */

int
main(int argc, char *argv[])
{
	char	*ipmp_group = NULL;
	char    ipmp_state[MAXIPMPMSGLEN];
	int	rc = 0;
	scds_handle_t handle;

	if (scds_initialize(&handle, argc, argv) != 0) {
		scds_syslog(LOG_ERR, "%s initialization failure", argv[0]);
		exit(1);
	}

	/* get the name of the IPMP group on this node */
	ipmp_group  = haip_get_ipmp_group(handle);
	if (ipmp_group == NULL) {
		exit(1);
	}

	if (haip_is_group_valid(ipmp_group) == B_FALSE) {
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		scds_syslog(LOG_ERR, "%s is not a valid %s group name on "
		    "this node.", ipmp_group, pnm_group_name());
		exit(1);

	}

	rc = haip_ipmpstatus(ipmp_group, ipmp_state);
	if (rc != 0) {
		scds_syslog(LOG_ERR, "%s group %s has status %s.",
		    pnm_group_name(), ipmp_group, ipmp_state);
		exit(1);
	}

	/* Checks whether the name service is available and responsive. */
	if (haip_check_nameservice(handle) != 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * The monitor_check method detected that name service is not
		 * responsive.
		 * @user_action
		 * Check if name servcie is configured correctly. Try some
		 * commands to query name serves, such as ping and nslookup,
		 * and correct the problem. If the error still persists, then
		 * reboot the node.
		 */
		scds_syslog(LOG_ERR, "Name service not available.");
		exit(1);
	}

	scds_syslog(LOG_INFO, "Completed successfully.");

	/* cleanup */
	scds_close(&handle);

	/* IPMP group is fine on this node, exit 0 */
	return (0);
}
