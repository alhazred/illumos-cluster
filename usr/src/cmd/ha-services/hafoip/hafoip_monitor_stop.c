/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * hafoip_monitor_stop.c
 * Monitor Stop method for highly available failover ipaddress
 */

#pragma ident	"@(#)hafoip_monitor_stop.c	1.21	08/05/20 SMI"

#include <stdio.h>
#include <stdlib.h>
#include <netdb.h>
#include <netinet/in.h>

#include "hafoip.h"

/*
 * UnRegister with IPMP so that it will NOT call us back.
 */

int
main(int argc, char *argv[])
{
	char	*ipmp_group = NULL;
	int	rc;
	char    internal_err_str[SCDS_ARRAY_SIZE];
	scds_handle_t handle;
	char *rname, *rgname, *zone;

	if (scds_initialize(&handle, argc, argv) != 0) {
		scds_syslog(LOG_ERR, "%s initialization failure", argv[0]);
		exit(1);
	}

	rname = (char *)scds_get_resource_name(handle);
	rgname = (char *)scds_get_resource_group_name(handle);
	zone = (char *)scds_get_zone_name(handle);

	/* get the ipmp group for this node */
	ipmp_group  = haip_get_ipmp_group(handle);
	if (ipmp_group == NULL) {
		(void) sprintf(internal_err_str,
		    "%s group name is not found in the netiflist "
		    "for this node.", pnm_group_name());
		(void) scha_resource_setstatus_zone(rname, rgname, zone,
		    SCHA_RSSTATUS_FAULTED, internal_err_str);
		exit(1);
	}

	if (haip_is_group_valid(ipmp_group) == B_FALSE) {
		(void) sprintf(internal_err_str,
		    "%s is not a valid %s group name on this node.",
		    ipmp_group, pnm_group_name());
		(void) scha_resource_setstatus_zone(rname, rgname, zone,
		    SCHA_RSSTATUS_FAULTED, internal_err_str);
		exit(1);
	}

	rc = haip_unregister_callback((char *)rname, ipmp_group, IPMP_REG_MON);
	if (rc != 0) {
		/*
		 * Need to be idempotent, so dont error out here. Just log
		 * an error message and leave it at that.
		 */
		(void) sprintf(internal_err_str,
		    "Unregister callback with %s %s failed.  Error %d",
		    pnm_group_name(), ipmp_group, rc);
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
	}

	/*
	 * Stop the failover retry utility if running
	 */
	if (haip_stop_retry(rgname, rname, ipmp_group) < 0) {
		(void) sprintf(internal_err_str,
		    "Unable to stop retry utility.");
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		exit(1);
	}

	scds_syslog(LOG_INFO, "Completed successfully.");

	/* cleanup */
	scds_close(&handle);

	return (0);
}
