/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_HAFOIP_H_
#define	_HAFOIP_H_

#pragma ident	"@(#)hafoip.h	1.28	08/05/20 SMI"

/* commonly used headers */
#include <sys/os.h>
#include <scha.h>
#include <rgm/haip.h>
#include <rgm/libdsdev.h>
#include <rgm/pnm.h>

#ifdef __cplusplus
extern "C" {
#endif

/*
 * hafoip.h - Header file for HA Failover IPaddress RT
 *
 * Functions declared here are defined in hafoip_common.c
 */

void hafoip_conn_abort(in6addr_list_t al);

/*
 * HAFOIP_SAFE_DELAY: Timeout in seconds for haip_safe_wait()
 * HAFOIP_SAFE_DELAY is the time we wait for other node
 * to "completely die" (i.e. stop responding to pings).
 * After this interval, however, we go ahead and takeover
 * the ipaddress anyway. The value 270 sec is derived from
 * the default time used by the CMM in its safe_to_takeover
 * algorithms.
 */
#define	HAFOIP_SAFE_DELAY	270

#define	HAFOIP_CALLBACK_PROG	"hafoip/hafoip_ipmp_callback"
#define	RETRY_PROG	"/usr/cluster/lib/rgm/rt/hafoip/hafoip_retry"

#ifdef __cplusplus
}
#endif

#endif	/* !_HAFOIP_H_ */
