/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * hafoip_prenet_start.c - Start method for highly available failover ipaddress
 */

#pragma ident	"@(#)hafoip_prenet_start.c	1.24	08/05/20 SMI"

#include <stdio.h>
#include <stdlib.h>
#include <netdb.h>
#include <netinet/in.h>

#include "hafoip.h"

/*
 * Configure the ipaddresses on the right IPMP group and
 * then call the lower level Networking API to "attach" these
 * ipaddresses to the local node.
 * There is all there is to it. + some error checks, of course.
 */

int
main(int argc, char *argv[])
{
	char *ipmp_group = NULL;
	int rc, instances;
	char internal_err_str[SCDS_ARRAY_SIZE];
	scds_handle_t handle;
	in6addr_list_t al = {NULL, 0};
	const char *rname, *rgname, *zone;

	if (scds_initialize(&handle, argc, argv) != 0) {
		scds_syslog(LOG_ERR, "%s initialization failure", argv[0]);
		exit(1);
	}

	rname = scds_get_resource_name(handle);
	rgname = scds_get_resource_group_name(handle);
	zone = scds_get_zone_name(handle);

	/* get the ipmp group for this node */
	ipmp_group  = haip_get_ipmp_group(handle);
	if (ipmp_group == NULL) {
		(void) sprintf(internal_err_str,
		    "%s group name is not found in the netiflist "
		    "for this node.", pnm_group_name());
		(void) scha_resource_setstatus_zone(rname, rgname, zone,
		    SCHA_RSSTATUS_FAULTED, internal_err_str);
		exit(1);
	}

	if (haip_is_group_valid(ipmp_group) == B_FALSE) {
		(void) sprintf(internal_err_str,
		    "%s is not a valid %s group name on this node.",
		    ipmp_group, pnm_group_name());
		(void) scha_resource_setstatus_zone(rname, rgname, zone,
		    SCHA_RSSTATUS_FAULTED, internal_err_str);
		exit(1);
	}

	if (pnm_get_instances_direct(ipmp_group, &instances) != 0) {
		(void) sprintf(internal_err_str,
		    "Failed to enumerate instances for %s group %s.",
		    pnm_group_name(), ipmp_group);
		(void) scha_resource_setstatus_zone(rname, rgname, zone,
		    SCHA_RSSTATUS_FAULTED, internal_err_str);
		exit(1);
	}

	/* get a list of all addresses that we are dealing with */
	if (haip_hostlist_to_addrlist(handle, &al, instances) !=
	    0) {
		(void) scha_resource_setstatus_zone(rname, rgname, zone,
		    SCHA_RSSTATUS_FAULTED, "Failed to obtain list of IP "
		    "addresses for this resource.");
		exit(1);
	}

	rc = haip_do_ipmp_op(ipmp_group, zone, PNM_IFCONFIG_PLUMB, al);
	free(al.addr); /* done with the list */
	if (rc > 0) {
		/* None of the ops succeeded */
		(void) sprintf(internal_err_str,
		    "Could not %s any ip addresses.", "plumb");
		(void) scha_resource_setstatus_zone(rname, rgname, zone,
		    SCHA_RSSTATUS_FAULTED, internal_err_str);
		exit(1);
	}
	if (rc < 0) {
		/* Some of the ops succeeded, but not all */
		/*
		 * SCMSGS
		 * @explanation
		 * Some of the ip addresses managed by the LogicalHostname
		 * resource were not successfully brought online on this node.
		 * @user_action
		 * Use ifconfig command to make sure that the ip addresses are
		 * indeed absent. Check for any error message before this
		 * error message for a more precise reason for this error. Use
		 * clresourcegroup to move the resource group to some other
		 * node.
		 */
		scds_syslog(LOG_WARNING,
		    "Some ip addresses may not be plumbed.");
	}

	/*
	 * XXX
	 * We would like to have the following behaviour here
	 * a) If ALL the ipaddresses were successfully plumbed,
	 *    Consider the operation a success, and only log the
	 *    last success message.
	 * b) If NONE of the ipaddresses were successfully plumbed,
	 *    consider the operation a failure and exit non-zero
	 *    and log an error message.
	 * c) If there is a partial success, log a warning message,
	 *    and do not print the last success message, but exit
	 *    zero anyways (so that START failed flag is not set on
	 *    the resource).
	 *
	 * Obsolete: fixed
	 * Currently that is not possible because IPMP does not pass
	 * back that information to us. We should update the code
	 * here once the appropiate error code is in place.
	 */

	if (rc == 0) {
		scds_syslog(LOG_INFO, "Completed successfully.");
	}

	scds_close(&handle);

	return (0);
}
