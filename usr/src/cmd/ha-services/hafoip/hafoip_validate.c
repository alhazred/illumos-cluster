/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * hafoip_validate.c - validate method for highly available failover ipaddress
 */

#pragma ident	"@(#)hafoip_validate.c	1.33	08/09/10 SMI"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <locale.h>
#include <libintl.h>

#include "hafoip.h"

int
main(int argc, char *argv[])
{
	char *ipmp_group = NULL;
	char *new_group;
	int rc, c, cu_flag = 0, instances = 0;
	scds_handle_t handle;
	in6addr_list_t al;
	const char *rname, *zone_name;
	pnm_adapterinfo_t *adp_list = NULL, *p = NULL;
	int adp_auth = B_FALSE;

	(void) setlocale(LC_ALL, "");
	(void) textdomain(TEXT_DOMAIN);
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	/* funcs in libhaip & hafoip_common.c should also print to stderr */
	print_errors = B_TRUE;

	if (scds_initialize(&handle, argc, argv) != 0) {
		scds_syslog(LOG_ERR, "%s initialization failure", argv[0]);
		(void) fprintf(stderr, gettext("%s initialization failure\n"),
		    argv[0]);
		exit(1);
	}

	rname = scds_get_resource_name(handle);
	zone_name = scds_get_zone_name(handle);

	/*
	 * args were already parsed by scds_initialize, so no error checks here.
	 * We just want to know whether the resource is being created or
	 * updated. 'c' or 'u' is stored in the flag accordingly.
	 */
	while ((c = getopt(argc, argv, "R:T:G:x:g:r:Z:cu")) != EOF) {
		if ((c == 'c') || (c == 'u'))
			cu_flag = c;
	}

	/* Make sure there is a group name defined on this node */
	scds_syslog_debug(DBG_LEVEL_LOW, "Checking for valid ipmpname");
	ipmp_group  = haip_get_ipmp_group(handle);
	if (ipmp_group == NULL) {
		exit(1);	/* Exit non-zero */
	}

	/* Make sure this looks like a real IPMP group */
	scds_syslog_debug(DBG_LEVEL_LOW, "Validating ipmpname %s", ipmp_group);
	if (haip_is_group_valid(ipmp_group) == B_FALSE) {
		scds_syslog(LOG_ERR, "%s is not a valid %s group name "
		    "on this node.", ipmp_group, pnm_group_name());
		(void) fprintf(stderr, gettext("%s is not a valid %s group "
		    "name on this node.\n"), ipmp_group, pnm_group_name());
		exit(1);	/* Exit non-zero */
	}

	/*
	 * Make sure the IPMP group actually supports the type of
	 * IP addresses that we'll plumb on it later. Its *not* an error
	 * if it does not - we'll issue a warning here during VALIDATE and
	 * silently work with the IPs that the group can handle subsequently.
	 */
	rc = pnm_get_instances_direct(ipmp_group, &instances);
	if (rc == PNM_ENONHOMOGENOUS) {
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		scds_syslog(LOG_ERR, "%s group %s is not homogenous",
		    pnm_group_name(), ipmp_group);
		(void) fprintf(stderr, gettext("%s group %s is not "
		    "homogenous.\n"), pnm_group_name(), ipmp_group);
		exit(1);
	} else if (rc != 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		scds_syslog(LOG_ERR, "Failed to enumerate instances for "
		    "%s group %s", pnm_group_name(), ipmp_group);
		(void) fprintf(stderr, gettext("Failed to enumerate instances "
		    "for %s group %s.\n"), pnm_group_name(), ipmp_group);
		exit(1);
	}

	/* check if neither of the two instance flags is set */
	if ((instances & (PNMV4MASK | PNMV6MASK)) == 0) {
		/* wont really get here... more of a sanity check */
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		scds_syslog(LOG_ERR, "%s group %s is incapable of hosting "
		    "%s addresses", pnm_group_name(), ipmp_group, "IPv4/IPv6");
		(void) fprintf(stderr, gettext("%s group %s is incapable of "
		    "hosting %s addresses.\n"), pnm_group_name(),
		    ipmp_group, "IPv4 or IPv6");
		exit(1);
	}

	/* if group has only one instance set, validate more closely */
	if ((instances & (PNMV4MASK | PNMV6MASK)) != (PNMV4MASK | PNMV6MASK)) {
		char *cant_host;
		int cant_host_instances;
		/*
		 * now invert the instances flag and use that to create a
		 * addrlist: what we want to do is check if any host has a
		 * mapping outside the instances that the IPMP group supports.
		 * If yes, issue a warning. eg if the IPMP group can host
		 * v4 addresses only and a host with v4 and v6 mappings is
		 * specified, this warning will show up.
		 *
		 * scrgadm is supposed to catch cross node homogeneity
		 * problems. This check helps when the ipmp groups are
		 * homogenous across nodes but do not have both instances.
		 * It also helps people using non-convenience options.
		 */
		cant_host_instances = instances ^ (PNMV4MASK | PNMV6MASK);
		cant_host = (cant_host_instances & PNMV4MASK) ? "IPv4" : "IPv6";
		if (haip_hostlist_to_addrlist(handle, &al,
		    cant_host_instances) != 0) {
			/*
			 * SCMSGS
			 * @explanation
			 * There was a failure in obtaining a list of IP
			 * addresses for the hostnames in the resource.
			 * Messages logged immediately before this message may
			 * indicate what the exact problem is.
			 * @user_action
			 * Check the settings in /etc/nsswitch.conf and verify
			 * that the resolver is able to resolve the hostnames.
			 */
			scds_syslog(LOG_ERR, "Failed to obtain list of IP "
			    "addresses for this resource");
			(void) fprintf(stderr, gettext("Failed to obtain "
			    "list of IP addresses for this resource.\n"));
			exit(1);
		}
		if (al.count != 0) {
			/*
			 * SCMSGS
			 * @explanation
			 * Need explanation of this message!
			 * @user_action
			 * Need a user action for this message.
			 */
			scds_syslog(LOG_ERR, "WARNING: %s group %s can not "
			    "host %s addresses. Atleast one of the hostnames "
			    "that were specified has %s mapping(s). All such "
			    "mappings will be ignored on this node.",
			    pnm_group_name(), ipmp_group, cant_host, cant_host);
			(void) fprintf(stderr, gettext("WARNING: %s group "
			    "%s can not host %s addresses. Atleast one of the "
			    "hostnames that were specified has %s mapping(s). "
			    "All such mappings will be ignored on this "
			    "node.\n"), pnm_group_name(),
			    ipmp_group, cant_host, cant_host);
			free(al.addr);
		}
	}

	/*
	 * If the zone name is not null and belongs to zone cluster,
	 * verify the adapters in the ipmp group are authorized to
	 * be used for this zone cluster.
	 */
	if (zone_name != NULL && scds_is_zone_cluster(handle)) {
		/* Validate the zone adapter list */
		rc = haip_validate_zone_adp(handle, ipmp_group);
		if (rc != 0) {
			/*
			 * SCMSGS
			 * @explanation
			 * There is no adapter found on this system that is
			 * authorized to be used for this zone cluster.
			 * @user_action
			 * Use the clzonecluster(1M) command to configure
			 * the adapters to be used for this zone cluster and
			 * run the command to create the resource.
			 */
			scds_syslog(LOG_ERR, "No suitable adapter is "
			    "authorized "
			    "to be used with resource %s on zone cluster %s.",
			    rname, zone_name);
			(void) fprintf(stderr, gettext(
			    "No suitable adapter is authorized to be used with "
			    "resource %s on zone cluster %s.\n"),
			    rname, zone_name);
			exit(1);
		}
	}

	/*
	 * Check if the ipmp name is being changed by comparing the name
	 * with the one in ccr.
	 * Note: the ipmp name pointed by ipmp_group will be overwritten
	 * by haip_check_group_changed() which calls haip_get_group_name().
	 */
	new_group = strdup(ipmp_group);
	if (new_group == NULL) {
		scds_syslog(LOG_ERR, "Out of memory.");
		(void) fprintf(stderr, gettext("Out of memory.\n"));
		exit(1);
	}

	switch (cu_flag) {
	case 'c':
		/*
		 * Make sure that none of the IPs (that the hostnames for
		 * this resource map to) are plumbed already
		 */
		if (haip_check_dup(handle) != 0) {
			/* Messages logged by haip_check_dup() */
			exit(1);
		}
		break;
	case 'u':
		rc = haip_check_group_changed(zone_name, handle, new_group);
		if (rc == 1) {
			/*
			 * SCMSGS
			 * @explanation
			 * Need explanation of this message!
			 * @user_action
			 * Need a user action for this message.
			 */
			scds_syslog(LOG_ERR, "Cannot change the %s group "
			    "on the local host.", pnm_group_name());
			(void) fprintf(stderr, gettext("Cannot change the "
			    "%s group on the local host. Bring resource %s "
			    "offline on this node and try again.\n"),
			    pnm_group_name(), rname);
			exit(1);
		} else if (rc == -1)
			/* messages already logged */
			exit(1);
		break;
	default:
		/* cant get here!! */
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
		    "Invalid callback option");
		(void) fprintf(stderr, gettext("INTERNAL ERROR: %s.\n"),
		    gettext("Invalid callback option"));
		exit(1);
	}

	/* make sure all the hostnames/ipaddress are good */
	rc = haip_validate_hostnamelist(handle, ipmp_group, instances);
	if (rc != B_TRUE) {
		/*
		 * SCMSGS
		 * @explanation
		 * The hostnames that has to be made available by this logical
		 * host resource are invalid.
		 * @user_action
		 * It is advised to keep the hostnames in /etc/inet/hosts file
		 * and enable "files" for host lookup in nsswitch.conf file.
		 * Any of the following situations might have occured.
		 *
		 * 1) If hosts are not in /etc/inet/hosts file then make sure
		 * the nameserver is reachable and has host name entries
		 * specified.
		 *
		 * 2) Invalid hostnames might have been specified while
		 * creating the logical host resource. If this is the case,
		 * use the clreslogicalhostname command to respecify the
		 * hostnames for this logical host resource.
		 */
		scds_syslog(LOG_ERR, "Resource contains invalid hostnames.");
		(void) fprintf(stderr, gettext("Resource contains invalid "
		    "hostnames.\n"));
		exit(1);
	}
	scds_syslog_debug(DBG_LEVEL_LOW, "Validating uniqueness of hostnames "
					"in the hostlist");
	rc = haip_validate_uniqueness(handle);
	if (rc != B_TRUE) {
		(void) fprintf(stderr, gettext("One or more hostname(s) "
					"in the hostlist of this logical "
					"hostname resource already exist "
					"in another resource group.\n"));
		exit(1);
	}
	scds_syslog(LOG_INFO, "Completed successfully.");

	scds_close(&handle);

	return (0);
}
