/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)hafoip_common.c	1.40	08/05/20 SMI"

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libgen.h>
#include <sys/clconf_int.h>

#include <netdb.h>
#include <netinet/in.h>

#ifndef linux
#include <stropts.h>
#include <inet/tcp.h>
#endif

#include "hafoip.h"

/*
 * TCP abort mechanism is a Solaris specific feature that provide an interface
 * to instruct to kernel to abort TCP connections to the IP address of a
 *  failed remote node, and/or to abort the TCP connections from a local IP
 * address when the IP address is being migrated to a different node.
 * Applications can then perform the necessary handling and recovery.
 *
 * This feature does not exist the Linux kernel and no alternative has been
 * found to implement this without modifying the Linux IP stack. Therefore,
 * this functionality is not present in DC Linux cluster release 1.
 *
 */
void
#ifdef linux
hafoip_conn_abort(in6addr_list_t al)
{
}
#else
hafoip_conn_abort(in6addr_list_t al)
{
	int s4 = -1, s6 = -1, s;
	struct strioctl 	iocb;
	tcp_ioc_abort_conn_t	tcpac;
	struct sockaddr_in	*sinp4 = (struct sockaddr_in *)&tcpac.ac_local;
	struct sockaddr_in6	*sinp6 = (struct sockaddr_in6 *)&tcpac.ac_local;
	uint_t	i;

	for (i = 0; i < (uint_t)al.count; i++) {
		/*
		 * Note that zeroing out the whole tcpac structure here means
		 * we dont have to worry about filling inaddrany for remote v4
		 * or v6 address because in solaris, both are a string of zeroes
		 */
		bzero(&tcpac, sizeof (tcpac));
		bzero(&iocb, sizeof (iocb));

		/* need to deal differently with v4 and v6 addresses */
		if (IN6_IS_ADDR_V4MAPPED(al.addr + i)) {
			if (s4 < 0) {
				s4 = socket(AF_INET, SOCK_STREAM, 0);
				if (s4 < 0) {
				    /* BEGIN CSTYLED */
				    /*
				     * CSTYLED is used to prevent cstyle
				     * complaints about block comments
				     * indented with spaces.
				     */
				    /*
				     * SCMSGS
				     * @explanation
				     * Failure in communication between fault
				     * monitor and process monitor facility.
				     * @user_action
				     * This is internal error. Save
				     * /var/adm/messages file and contact the
				     * Sun service provider.
				     */
				    /* END CSTYLED */
				    scds_syslog(LOG_ERR,
					"Failed to create socket: %s.",
					strerror(errno)); /*lint !e746 */
					/*
					 * just continue here, we are willing
					 * to try and create a v4 sock again
					 * if there are more v4 addresses in
					 * the list
					 */
				    continue;
				}
			}

			/*
			 * Setup the TCP_IOC_ABORT_CONN ioctl connection filters
			 * First the local connection filter.
			 * If the local endpoint has the Logical IP,
			 * we need to terminate it, irrespective of whether
			 * it is an outbound or inbound connection.
			 */
			tcpac.ac_local.ss_family  = AF_INET;
			IN6_V4MAPPED_TO_INADDR(al.addr + i, &(sinp4->sin_addr));

			/* The remote connection filter. */
			tcpac.ac_remote.ss_family = AF_INET;

			/* ioctl should operate on s4 */
			s = s4;
		} else {
			if (s6 < 0) {
				s6 = socket(AF_INET6, SOCK_STREAM, 0);
				if (s6 < 0) {
				    scds_syslog(LOG_ERR,
					"Failed to create socket: %s.",
					strerror(errno)); /*lint !e746 */
					/*
					 * just continue here, we are willing
					 * to try and create a v6 sock again
					 * if there are more v6 addresses in
					 * the list
					 */
				    continue;
				}
			}

			/*
			 * Setup the TCP_IOC_ABORT_CONN ioctl connection filters
			 * First the local connection filter.
			 * If the local endpoint has the Logical IP,
			 * we need to terminate it, irrespective of whether
			 * it is an outbound or inbound connection.
			 */
			tcpac.ac_local.ss_family = AF_INET6;
			bcopy(al.addr + i, (char *)&sinp6->sin6_addr,
			    sizeof (struct in6_addr));

			/* The remote connection filter. */
			tcpac.ac_remote.ss_family = AF_INET6;

			/* ioctl should operate on s6 */
			s = s6;
		}

		/* TCP connection state filter. */
		tcpac.ac_start	= TCPS_SYN_SENT;
		tcpac.ac_end	= TCPS_TIME_WAIT;

		/* Prepare the ioctl. */
		iocb.ic_cmd	= TCP_IOC_ABORT_CONN;
		iocb.ic_timout	= 0;
		iocb.ic_len	= (int)sizeof (tcp_ioc_abort_conn_t);
		iocb.ic_dp	= (char *)&tcpac;

		/* Send the ioctl. */
		if (ioctl(s, I_STR, &iocb) != 0) {
			/*
			 * TCP_IOC_ABORT_CONN fails with ENOENT if there are
			 * no connections to be reset. Hence making this a
			 * debug message.
			 */
			scds_syslog_debug(DBG_LEVEL_LOW, "%s operation "
			    "failed : %s.", "TCP_IOC_ABORT_CONN",
			    strerror(errno));	/*lint !e746 */
		}
	}

	/* close the sockets */
	if (s4 >= 0)
		(void) close(s4);
	if (s6 >= 0)
		(void) close(s6);
}
#endif
