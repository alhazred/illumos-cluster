/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * hafoip_stop.c - Stop method for highly available failover ipaddress
 */

#pragma ident	"@(#)hafoip_stop.c	1.26	08/05/20 SMI"

#include <stdio.h>
#include <stdlib.h>
#include <netdb.h>
#include <netinet/in.h>

#include "hafoip.h"

/*
 * Call IPMP to unplumb the ipaddresses
 * There is all there is to it. + some error checks, of course.
 */

int
main(int argc, char *argv[])
{
	char 	*ipmp_group = NULL;
	int rc, instances;
	char internal_err_str[SCDS_ARRAY_SIZE];
	scds_handle_t handle;
	in6addr_list_t al;
	const char *rname, *rgname, *zone;

	if (scds_initialize(&handle, argc, argv) != 0) {
		scds_syslog(LOG_ERR, "%s initialization failure", argv[0]);
		exit(1);
	}

	rname = scds_get_resource_name(handle);
	rgname = scds_get_resource_group_name(handle);
	zone = scds_get_zone_name(handle);

	/* get the ipmp group for this node */
	ipmp_group  = haip_get_ipmp_group(handle);
	if (ipmp_group == NULL) {
		(void) sprintf(internal_err_str,
		    "%s group name is not found in the netiflist for "
		    "this node.", pnm_group_name());
		(void) scha_resource_setstatus_zone(rname, rgname, zone,
		    SCHA_RSSTATUS_FAULTED, internal_err_str);
		exit(1);
	}

	if (haip_is_group_valid(ipmp_group) == B_FALSE) {
		(void) sprintf(internal_err_str,
		    "%s is not a valid %s group name on this node.",
		    ipmp_group, pnm_group_name());
		(void) scha_resource_setstatus_zone(rname, rgname, zone,
		    SCHA_RSSTATUS_FAULTED, internal_err_str);
		exit(1);
	}

	/*
	 * We remove monitor callback if remains registered
	 * even after monitor_stop executes, useful specially when
	 * hafoip_monitor_stop does not complete successfully.
	 * Note: Does not have any side effect even if hafoip_monitor_stop
	 * completes successfully
	 */
	rc = haip_unregister_callback((char *)rname, ipmp_group, IPMP_REG_MON);
	if (rc != 0) {
		/*
		 * Need to be idempotent, so dont error out here. Just log
		 * an error message and leave it at that.
		 */
		(void) snprintf(internal_err_str, sizeof (internal_err_str),
		    "Unregister callback with IPMP %s failed.  Error %d",
		    ipmp_group, rc);
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
	}

	if (pnm_get_instances_direct(ipmp_group, &instances) != 0) {
		(void) sprintf(internal_err_str,
		    "Failed to enumerate instances for %s group %s.",
		    pnm_group_name(), ipmp_group);
		(void) scha_resource_setstatus_zone(rname, rgname, zone,
		    SCHA_RSSTATUS_FAULTED, internal_err_str);
		exit(1);
	}

	/* get a list of all addresses that we are dealing with */
	rc = haip_hostlist_to_addrlist(handle, &al, instances);
	if (rc != 0) {
		(void) scha_resource_setstatus_zone(rname, rgname, zone,
		    SCHA_RSSTATUS_FAULTED,
		    "Failed to obtain list of IP addresses for this resource.");
		exit(1);
	}

	/* IF_UNPLUMB does BOTH down AND unplumb */
	rc = haip_do_ipmp_op(ipmp_group, zone, PNM_IFCONFIG_UNPLUMB, al);
	if (rc > 0) {
		/* None of the ops succeeded */
		(void) scha_resource_setstatus_zone(rname, rgname, zone,
		    SCHA_RSSTATUS_FAULTED,
		    "Could not unplumb any ipaddresses.");
		exit(1);
	}

	/*
	 * Reset active TCP connections using failover IPs
	 * We do it AFTER having removed the IP. If we did it
	 * BEFORE, there always would be the race that a client
	 * could sneak in before the IP was removed.
	 * Note that aborting the connection just cleans up the
	 * TCP state on the SERVER side (on the cluster). It
	 * does NOT do anything with the client, if the client
	 * were to attempt to use the connection, it will get
	 * a reset because the server state is gone.
	 * So aborting the connections does not NEED the IP
	 * to be up.
	 * The bottom line here is that this protects us
	 * from 2 issues
	 * 1. TCP ACK Wars, if the IP were to FAIL BACK (within
	 *		2 x MSL) to this node.
	 * 2. Data service Postnet_stop methods can be confident
	 * 		that there are no client connections pending
	 *		when they run. Depending on the data service,
	 *		even if the STOP method stops services, clients
	 *		can still attempt to connect to service between
	 *		the time the STOP method completes and the IP
	 *		is unplumbed.
	 */
	hafoip_conn_abort(al);

	/*
	 * XXX
	 * for handling partial failures, look at the detailed
	 * comment in the prenet start method. We would like to
	 * implement the same behaviour here as well.
	 */

	if (rc == 0) {
		(void) scha_resource_setstatus_zone(rname, rgname, zone,
		    SCHA_RSSTATUS_OFFLINE, "LogicalHostname offline.");
	} else {
		(void) scha_resource_setstatus_zone(rname, rgname, zone,
		    SCHA_RSSTATUS_DEGRADED,
		    "Could not unplumb some ipaddresses.");
	}

	free(al.addr);
	scds_close(&handle);

	return (0);
}
