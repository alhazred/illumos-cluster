#!/bin/sh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#


#
#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)cmas_service_ctrl_stop.sh	1.10	08/12/04 SMI"
#
#

#
# stop cmas service ctrl
#
JAVA_OPTS=
ROOT_DIR=

if [ ! -f "/var/cluster/ips/.ips" ]
then
	CACAO_ROOT=`/usr/bin/pkgparam SUNWcacaort BASEDIR 2>/dev/null`
	JDMK_ROOT=`/usr/bin/pkgparam SUNWjdmk-runtime BASEDIR 2>/dev/null`
	OPENJDMK_ROOT=`/usr/bin/pkgparam SUNWjdmk-base BASEDIR 2>/dev/null`
else
	CACAO_ROOT=
	JDMK_ROOT=
	OPENJDMK_ROOT=
fi

AGENT="localhost"
FAILOVER_GROUP=$1
COMMAND="lock"

SED=/usr/bin/sed 

CMASSPATH=`echo ${ROOT_DIR}/usr/cluster/lib/cmass/*.jar | ${SED} 's+  *+:+g'`
CACAOPATH=`echo ${ROOT_DIR}/${CACAO_ROOT}/usr/lib/cacao/lib/*.jar | ${SED} 's+  *+:+g'`
JDMKPATH=`echo ${ROOT_DIR}/${JDMK_ROOT}/SUNWjdmk/5.1/lib/*.jar . | ${SED} 's+  *+:+g'`
JMXPATH="${ROOT_DIR}/usr/jdk/latest/jre/lib/rt.jar"
OPENJDMKPATH=`echo ${ROOT_DIR}/${OPENJDMK_ROOT}/usr/share/lib/jdmk/*.jar | ${SED} 's+  *+:+g'`

CLASSPATH=${CMASSPATH}:${CACAOPATH}:${JDMKPATH}:${JMXPATH}:${OPENJDMKPATH}

# is cacao ready?
${ROOT_DIR}/usr/sbin/cacaoadm list-trusted-certs > /dev/null 2>&1 
status=$?
exitcode=0 # force exit 0 if cacao not reachable or ready

# wait for cacao startup if necessary
# rgm will kill this loop if it goes too long
while [ $status -eq 11 ]
do
    sleep 5
    # is cacao ready?
    ${ROOT_DIR}/usr/sbin/cacaoadm list-trusted-certs > /dev/null 2>&1 
    status=$?
    exitcode=0 # force exit 0 if cacao not reachable or ready
done

# shutdown the module
if [ $status -eq 0 ]
then
    java $JAVA_OPTS -classpath ${CLASSPATH} ServiceControl $AGENT $COMMAND $FAILOVER_GROUP
    exitcode=$? # exit with true exit code
fi

exit $exitcode


