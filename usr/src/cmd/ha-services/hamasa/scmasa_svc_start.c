/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 *
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *
 */

/* Sun Cluster Data Services Builder template version 1.0 */
/*
 * scmasa_svc_start.c - Start method for scmasa
 */
#pragma ident	"@(#)scmasa_svc_start.c	1.3	08/05/20 SMI"

#include <rgm/libdsdev.h>
#include "scmasa.h"

/*
 * The start method for scmasa. Does some sanity checks on
 * the resource settings then starts the scmasa under PMF with
 * an action script.
 */

int
main(int argc, char *argv[])
{
	scds_handle_t	scds_handle;
	int rc;

	/*
	 * Process all the arguments that have been passed to us from RGM
	 * and do some initialization for syslog
	 */

	if (scds_initialize(&scds_handle, argc, argv) != SCHA_ERR_NOERR) {
		return (1);
	}

	/* Validate the configuration and if there is an error return back */
	rc = svc_validate(scds_handle);
	if (rc != 0) {
		scds_syslog(LOG_ERR,
		    "Failed to validate configuration.");
		goto finished;
	}

	/* Start the data service, if it fails return with an error */
	rc = svc_start(scds_handle);
	if (rc != 0) {
		goto finished;
	}

	/* Wait for the service to start up fully */
	rc = svc_wait(scds_handle);
	scds_syslog_debug(DBG_LEVEL_HIGH, "Returned from svc_wait");

finished:
	/* Free up the Environment resources that were allocated */
	scds_close(&scds_handle);

	return (rc);
}
