/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 *
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */
#pragma ident	"@(#)scmasa.c	1.8	09/01/06 SMI"


/* Sun Cluster Data Services Builder template version 1.0 */
/*
 * scmasa.c - Common utilities for scmasa
 *
 *
 * This utility has the methods for performing the validation, starting and
 * stopping the data service and the fault monitor. It also contains the method
 * to probe the health of the data service.  The probe just returns either
 * success or failure. Action is taken based on this returned value in the
 * method found in the file scmasa_probe.c
 *
 * 09/29/03 : suppress all code not necessary for CMAS failover
 *            service since we don't start/stop any process,
 *            it is not possible to use PMF.
 */

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <strings.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <netinet/in.h>
#include <scha.h>
#include <rgm/libdsdev.h>
#include <errno.h>
#include <dlfcn.h>
#include "scmasa.h"

/*
 * The initial timeout allowed  for the scmasa dataservice to
 * be fully up and running. We will wait for for 3 % (SVC_WAIT_PCT)
 * of the start_timeout time before probing the service.
 */
#define	SVC_WAIT_PCT		3

/*
 * We need to use 95% of probe_timeout to connect to the port and the
 * remaining time is used to disconnect from port in the svc_probe function.
 */
#define	SVC_CONNECT_TIMEOUT_PCT		95

/*
 * We need to wait for SVC_WAIT_TIME ( 2 secs) for pmf
 * to send the failure message before probing the service
 */

#define	SVC_WAIT_TIME		2

/*
 * This value will be used as disconnect timeout, if there is no
 * time left from the probe_timeout.
 */

#define	SVC_DISCONNECT_TIMEOUT_SECONDS		2



/* This is the same as a pointer to the scds_hasp_check api */
typedef scha_err_t (*scds_hasp_check_t)(scds_handle_t, scds_hasp_status_t *);

int get_cmas_failover_group(scds_handle_t scds_handle, char *ptr_fo_group);

/*
 * svc_validate():
 *
 * Do scmasa specific validation of the resource configuration.
 *
 */

int
svc_validate(scds_handle_t scds_handle)
{
	struct stat statbuf;
	char *start_cmd_prog[] = {
	"/usr/cluster/lib/rgm/rt/hamasa/cmas_service_ctrl_start"
	};
	int no_start_cmd = 1;
	int i, err;
	char fo_group[SCDS_CMD_SIZE];
	char stop_cmd_prog[SCDS_CMD_SIZE] =
	"/usr/cluster/lib/rgm/rt/hamasa/cmas_service_ctrl_stop";
	char probe_cmd_prog[SCDS_CMD_SIZE] =
	"/usr/cluster/lib/rgm/rt/hamasa/cmas_service_ctrl_check";
	int rc = 0, do_cmd_checks = 1;
	char cmd[SCDS_CMD_SIZE];
	scds_hasp_status_t hasp_status;
	scds_hasp_check_t scds_hasp_check_p;

	scds_net_resource_list_t *snrlp = NULL;
	scds_port_list_t *portlist = NULL;


	/* Are we running with a libdsdev that does not have scds_hasp_check? */
	scds_hasp_check_p = (scds_hasp_check_t)dlsym(RTLD_DEFAULT,
							"scds_hasp_check");

	if (scds_hasp_check_p == NULL) {
		/* fake a call to scds_hasp_check() */
		err = SCHA_ERR_NOERR;
		hasp_status = SCDS_HASP_NO_RESOURCE;
	} else {
		/* actually check for HAStoragePlus resources */
		err = (*scds_hasp_check_p)(scds_handle, &hasp_status);
	}

	if (err != SCHA_ERR_NOERR) {
		/* scha_hasp_check() logs a message to syslog when it fails */
		rc = 1;
		/*
		 * validation has failed for this resource
		 */
		goto finished;
	}

	switch (hasp_status) {

	case SCDS_HASP_NO_RESOURCE:
		/*
		 * We do not depend on any SUNW.HAStoragePlus resources
		 */
		/*
		 * SCMSGS
		 * @explanation
		 * This is an informational message only.
		 * @user_action
		 * None.
		 */
		scds_syslog(LOG_INFO,
		    "This resource does not depend on any SUNW.HAStoragePlus "
		    "resources. Proceeding with normal checks.");
		do_cmd_checks = 1;
		break;

	case SCDS_HASP_ERR_CONFIG:
		/*
		 * Configuration error, SUNW.HAStoragePlus resource is
		 * in a different RG. Fail the validation.
		 */
		scds_syslog(LOG_ERR,
		    "One or more of the SUNW.HAStoragePlus resources that "
		    "this resource depends on is in a different resource "
		    "group. Failing validate method configuration checks.");
		rc = 1;
		goto finished;

	case SCDS_HASP_NOT_ONLINE:
		/*
		 * There is at least one SUNW.HAStoragePlus resource not
		 * online anywhere.
		 */
		/*
		 * SCMSGS
		 * @explanation
		 * It is an invalid configuration to create an application
		 * resource that depends on one or more SUNW.HAStoragePlus
		 * resource(s) that are not online on any node.
		 * @user_action
		 * Bring the SUNW.HAStoragePlus resource(s) online before
		 * creating the application resource that depend on them and
		 * then try the command again.
		 */
		scds_syslog(LOG_ERR,
		    "One or more of the SUNW.HAStoragePlus resources that "
		    "this resource depends on is not online anywhere. "
		    "Failing validate method.");
		rc = 1;
		goto finished;

	case SCDS_HASP_ONLINE_NOT_LOCAL:
		/*
		 * Not all SUNW.HAStoragePlus we need, are online locally.
		 */
		/*
		 * SCMSGS
		 * @explanation
		 * This is an informational message which means that the
		 * SUNW.HAStoragePlus resource(s) that this application
		 * resource depends on is not online on the local node and
		 * therefore the validation checks related to start/stop/probe
		 * commands can not be carried out on the local node.
		 * @user_action
		 * None.
		 */
		scds_syslog(LOG_INFO,
		    "All the SUNW.HAStoragePlus resources that this resource "
		    "depends on are not online on the local node. "
		    "Skipping the checks for the existence and permissions "
		    "of the start/stop/probe commands.");
		do_cmd_checks = 0;
		break;

	case SCDS_HASP_ONLINE_LOCAL:
		/*
		 * All SUNW.HAStoragePlus resources we need are available on
		 * this node.
		 */
		scds_syslog(LOG_INFO,
		    "All the SUNW.HAStoragePlus resources that this resource "
		    "depends on are online on the local node. "
		    "Proceeding with the checks for the existence and "
		    "permissions of the start/stop/probe commands.");
		do_cmd_checks = 1;
		break;

	default:
		/* Unknown status code */
		/*
		 * SCMSGS
		 * @explanation
		 * This message indicates that an unknown status code was
		 * rerturned by one of the underlying subsystems and an
		 * internal error has occured.
		 * @user_action
		 * Report this problem.
		 */
		scds_syslog(LOG_ERR, "Unknown status code %d.", hasp_status);
		rc = 1;
		break;
	}

	/*
	 * If we got here and do_cmd_checks is not set, it means that
	 * the SUNW.HAStorage resource is online but not on this node.
	 * Therefore, skip the checks for the existence and permissions
	 * of the start/stop/probe commands and jump (goto) straight to
	 * "global_checks".
	 */

	if (!do_cmd_checks) {
		goto global_checks;
	}

	/*
	 * If do_cmd_checks is set, it means that the SUNW.HAStorage
	 * resource is online on the local node. Therefore, proceed with
	 * the checks for the existence and permissions of the
	 * start/stop/probe commands.
	 */

	for (i = 0; i < no_start_cmd; i++) {
		/*
		 * Make sure that scmasa start command exists and that the
		 * permissions are correct.
		 */
		(void) strcpy(cmd, start_cmd_prog[i]);

		if (stat(cmd, &statbuf) != 0) {
			/*
			 * SCMSGS
			 * @explanation
			 * The command input to the agent builder is not
			 * accessible and executable. This may be due to the
			 * program not existing or the permissions not being
			 * set properly.
			 * @user_action
			 * Make sure the program in the command exists, is in
			 * the proper directory, and has read and execute
			 * permissions set appropriately.
			 */
			scds_syslog(LOG_ERR,
			    "Cannot access the %s command <%s> : <%s>",
			    "start", cmd, strerror(errno));
			rc = 1;
			goto finished;
		}

		if (!(statbuf.st_mode & S_IXUSR)) {
			/*
			 * SCMSGS
			 * @explanation
			 * This command input to the agent builder does not
			 * have the expected default execute permissions.
			 * @user_action
			 * Reset the permissions to allow execute permissions
			 * using the chmod command.
			 */
			scds_syslog(LOG_ERR,
			    "The %s command does not have execute "
			    "permissions: <%s>", "start", cmd);
			rc = 1;
			goto finished;
		}
	}

	/*
	 * Make sure that if scmasa stop command is specified, then it exists
	 * and that the permissions are correct.
	 */
	if (strcmp(stop_cmd_prog, "") != 0) {
		if (stat(stop_cmd_prog, &statbuf) != 0) {
			scds_syslog(LOG_ERR,
			    "Cannot access the %s command <%s> : <%s>",
			    "stop", stop_cmd_prog, strerror(errno));
			rc = 1;
			goto finished;
		}

		if (!(statbuf.st_mode & S_IXUSR)) {
			scds_syslog(LOG_ERR,
			    "The %s command does not have execute "
			    "permissions: <%s>", "stop", cmd);
			rc = 1;
			goto finished;
		}
	}

	/*
	 * Make sure that if scmasa probe command is specified, then it exists
	 * and that the permissions are correct.
	 */
	if (strcmp(probe_cmd_prog, "") != 0) {
		if (stat(probe_cmd_prog, &statbuf) != 0) {
			scds_syslog(LOG_ERR,
			    "Cannot access the %s command <%s> : <%s>",
			    "probe", probe_cmd_prog, strerror(errno));
			rc = 1;
			goto finished;
		}

		if (!(statbuf.st_mode & S_IXUSR)) {
			scds_syslog(LOG_ERR,
			    "The %s command does not have execute "
			    "permissions: <%s>", "probe", probe_cmd_prog);
			rc = 1;
			goto finished;
		}
	}

global_checks:

	/*
	 * CMAS module : try to read the failover group name
	 */
	if (get_cmas_failover_group(scds_handle, fo_group) != 0) {
		goto finished;
	}

	/*
	 * Network aware service should have at least one port specified
	 */

	rc = scds_get_port_list(scds_handle, &portlist);

	if (rc != SCHA_ERR_NOERR) {
		/*
		 * SCMSGS
		 * @explanation
		 * An API operation has failed while retrieving the resource
		 * property. Low memory or API call failure might be the
		 * reasons.
		 * @user_action
		 * In case of low memory, the problem will probably cured by
		 * rebooting. If the problem recurs, you might need to
		 * increase swap space by configuring additional swap devices.
		 * Otherwise, if it is API call failure, check the syslog
		 * messages from other components. For the resource name and
		 * property name, check the current syslog message.
		 */
		scds_syslog(LOG_ERR,
		    "Failed to retrieve the resource property %s: %s.",
		    SCHA_PORT_LIST, scds_error_string(rc));
		goto finished;
	}

	if (portlist == NULL || portlist->num_ports < 1) {
		/*
		 * SCMSGS
		 * @explanation
		 * The property has not been set by the user and must be.
		 * @user_action
		 * Reissue the clresource command with the required property
		 * and value.
		 */
		scds_syslog(LOG_ERR,
		    "Property %s is not set.", SCHA_PORT_LIST);
		rc = 1;
		goto finished;
	}

	/*
	 * Return an error if there is an error when trying to get the
	 * available network address resources for this resource
	 */
	if ((rc = scds_get_rs_hostnames(scds_handle, &snrlp))
		!= SCHA_ERR_NOERR) {
		/*
		 * SCMSGS
		 * @explanation
		 * Failed to get the available network address resources for
		 * this resource.
		 * @user_action
		 * This is an internal error. Save the /var/adm/messages file
		 * and contact an authorized Sun service provider.
		 */
		scds_syslog(LOG_ERR,
		    "Error in trying to access the configured network "
		    "resources : %s.", scds_error_string(rc));
		goto finished;
	}

	/* Return an error if there are no network address resources */
	if (snrlp == NULL || snrlp->num_netresources == 0) {
		scds_syslog(LOG_ERR,
		    "No network address resource in resource group.");
		rc = 1;
		goto finished;
	}

	/* Check to make sure other important extension props are set */
	if ((scds_get_ext_monitor_retry_count(scds_handle) <= 0) ||
	    (scds_get_ext_monitor_retry_interval(scds_handle) <= 0)) {
		/*
		 * SCMSGS
		 * @explanation
		 * The resource properties Monitor_retry_count or
		 * Monitor_retry_interval has not set. These properties
		 * control the restarts of the fault monitor.
		 * @user_action
		 * Check whether the properties are set. If not, set these
		 * values by using clresource.
		 */
		scds_syslog(LOG_ERR,
		"Monitor_retry_count or Monitor_retry_interval is not set.");
		rc = 1; /* Validation Failure */
		goto finished;
	}


	/* All validation checks were successful */


finished:

	scds_free_net_list(snrlp);
	scds_free_port_list(portlist);

	return (rc); /* return result of validation */
}

/*
 * svc_start():
 *
 */

int
svc_start(scds_handle_t scds_handle)
{
	int rc = 0;
	char *service_start_cmd[] = {
	"/usr/cluster/lib/rgm/rt/hamasa/cmas_service_ctrl_start"
	};
	int no_start_cmd = 1;
	int i;
	char cmd[SCDS_CMD_SIZE];
	char fo_group[SCDS_CMD_SIZE];
	scha_extprop_value_t *child_mon_level_prop = NULL;
	int child_mon_level;

	rc = scds_get_ext_property(scds_handle, "Child_mon_level",
	    SCHA_PTYPE_INT, &child_mon_level_prop);

	if (rc == SCHA_ERR_NOERR) {
		child_mon_level = child_mon_level_prop->val.val_int;
		/*
		 * SCMSGS
		 * @explanation
		 * Resource property Child_mon_level is set to the given
		 * value.
		 * @user_action
		 * This is an informational message, no user action is needed.
		 */
		scds_syslog(LOG_INFO,
		    "Extension property <Child_mon_level> has a value of <%d>",
		    child_mon_level);
	} else {
		/*
		 * SCMSGS
		 * @explanation
		 * Property Child_mon_level might not be defined in RTR file.
		 * The process used the default value of -1.
		 * @user_action
		 * This is an informational message; no user action is needed.
		 */
		scds_syslog(LOG_INFO,
		    "Either extension property <Child_mon_level> is not "
		    "defined, or an error occurred while retrieving this "
		    "property; using the default value of -1.");
		child_mon_level = -1;
	}

	for (i = 0; i < no_start_cmd; i++) {

		(void) strcpy(cmd, service_start_cmd[i]);

		if ((rc = preprocess_cmd(scds_handle, cmd)) != 0) {
			/*
			 * SCMSGS
			 * @explanation
			 * The method searches the commands input to the Agent
			 * Builder for the occurence of specific Builder
			 * defined variables, e.g. $hostnames, and replaces
			 * them with appropriate value. This action failed.
			 * @user_action
			 * Check syslog messages and correct the problems
			 * specified in prior syslog messages. If the error
			 * still persists, report this problem.
			 */
			scds_syslog(LOG_ERR,
			    "Failed to form the %s command.", "start");
			goto finished;
		}

		/*
		 * add the cmas failover group name to the end of the command
		 */
		if (get_cmas_failover_group(scds_handle, fo_group) != 0) {
			scds_syslog(LOG_ERR,
		"Failed to form the %s command.",
				"start");
			goto finished;
		}

		(void) strcat(cmd, " ");
		(void) strcat(cmd, fo_group);

		/* execute simply the start command */
		rc = WEXITSTATUS(system(cmd));

		if (rc == 0) {
			scds_syslog(LOG_INFO,
			    "Start of %s completed successfully.", cmd);
		} else {
			scds_syslog(LOG_ERR,
			    "Failed to start %s.", cmd);
			goto finished;
		}
	}

finished:

	return (rc); /* return Success/failure status */
}

/*
 * svc_stop():
 *
 * Stop the scmasa server
 * Return 0 on success, > 0 on failures.
 *
 */
int
svc_stop(scds_handle_t scds_handle)
{
	int rc = 0;
	char fo_group[SCDS_CMD_SIZE];
	char service_stop_cmd[SCDS_CMD_SIZE] =
	"/usr/cluster/lib/rgm/rt/hamasa/cmas_service_ctrl_stop";
	char cmd[SCDS_CMD_SIZE];

	if (strcmp(service_stop_cmd, "") != 0) {

		/*
		 * First try to stop the application using the stop command
		 * provided.
		 */
		(void) strcpy(cmd, service_stop_cmd);
		if ((rc = preprocess_cmd(scds_handle, cmd)) != 0) {
			scds_syslog(LOG_ERR,
			    "Failed to form the %s command.", "stop");
			/*
			 * We failed to preprocess the stop command so can't
			 * use that. We still proceed to send KILL signal and
			 * try to stop the application anyway.
			 */
			goto send_kill;
		}

		/*
		 * add the cmas failover group name to the end of the command
		 */
		if (get_cmas_failover_group(scds_handle, fo_group) != 0) {
			scds_syslog(LOG_ERR,
		"Failed to form the %s command.",
			"stop");
			goto finished;
		}

		(void) strcat(cmd, " ");
		(void) strcat(cmd, fo_group);

		/* execute simply the stop command */
		rc = WEXITSTATUS(system(cmd));

		if (rc != 0) {
			/*
			 * SCMSGS
			 * @explanation
			 * The user provided stop command cannot stop the
			 * application.
			 * @user_action
			 * No action required.
			 */
			scds_syslog(LOG_ERR,
				"The stop command <%s> failed to stop the "
				"application.", cmd);
		}
	}

send_kill:
finished:

	if (rc == SCHA_ERR_NOERR)
		/*
		 * SCMSGS
		 * @explanation
		 * This message is to inform the administrator that the
		 * application was stopped successfully.
		 * @user_action
		 * This is an informational message, no user action is needed.
		 */
		scds_syslog(LOG_INFO, "Successfully stopped the application");

	return (rc); /* Successfully stopped */
}

/*
 * svc_wait():
 *
 * wait for the data service to start up fully and make sure it is running
 * healthy
 */

int
svc_wait(scds_handle_t scds_handle)
{
	int rc, svc_start_timeout, probe_timeout;
	scds_netaddr_list_t	*netaddr;

	/* obtain the network resource to use for probing */
	if (scds_get_netaddr_list(scds_handle, &netaddr)) {
		scds_syslog(LOG_ERR,
		    "No network address resource in resource group.");
		return (1);
	}

	/* Return an error if there are no network resources */
	if (netaddr == NULL || netaddr->num_netaddrs == 0) {
		scds_syslog(LOG_ERR,
		    "No network address resource in resource group.");
		return (1);
	}

	/*
	 * Get the Start method timeout, port number on which to probe,
	 * the Probe timeout value
	 */
	svc_start_timeout = scds_get_rs_start_timeout(scds_handle);
	probe_timeout = scds_get_ext_probe_timeout(scds_handle);

	/*
	 * sleep for SVC_WAIT_PCT percentage of start_timeout time
	 * before actually probing the dataservice. This is to allow
	 * the dataservice to be fully up in order to reply to the
	 * probe. NOTE: the value for SVC_WAIT_PCT could be different
	 * for different dataservices.
	 */
	if (scds_svc_wait(scds_handle, (svc_start_timeout * SVC_WAIT_PCT)/100)
	    != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to start service.");
		return (1);
	}

	do {
		/*
		 * probe the data service on the IP address of the
		 * network resource and the portname
		 */
		rc = svc_probe(scds_handle,
		    netaddr->netaddrs[0].hostname,
		    netaddr->netaddrs[0].port_proto.port, probe_timeout);
		if (rc == SCHA_ERR_NOERR) {
			/* Success. Free up resources and return */
			scds_free_netaddr_list(netaddr);
			return (SCHA_ERR_NOERR);
		}

		/*
		 * Dataservice is still trying to come up. Sleep for a while
		 * before probing again.
		 */
		(void) scds_svc_wait(scds_handle, SVC_WAIT_TIME);

	/* We rely on RGM to timeout and terminate the program */
	} while (1);

}

/*
 * This function starts the fault monitor for a scmasa resource.
 * This is done by starting the probe under PMF. The PMF tag
 * is derived as <RG-name,RS-name,instance_number.mon>. The restart option
 * of PMF is used but not the "infinite restart". Instead
 * interval/retry_time is obtained from the RTR file.
 */

int
mon_start(scds_handle_t scds_handle)
{
	scha_err_t	err;

	scds_syslog_debug(DBG_LEVEL_HIGH,
	    "Calling MONITOR_START method for resource <%s>.",
	    scds_get_resource_name(scds_handle));

	/*
	 * The probe scmasa_probe is assumed to be available in the same
	 * subdirectory where the other callback methods for the RT are
	 * installed. The last parameter to scds_pmf_start denotes the
	 * child monitor level. Since we are starting the probe under PMF
	 * we need to monitor the probe process only and hence we are using
	 * a value of 0.
	 */

	err = scds_pmf_start(scds_handle, SCDS_PMF_TYPE_MON,
	    SCDS_PMF_SINGLE_INSTANCE, "scmasa_probe", 0);

	if (err != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to start fault monitor.");
		return (err);
	}

	scds_syslog(LOG_INFO, "Started the fault monitor.");

	return (SCHA_ERR_NOERR); /* Successfully started Monitor */
}


/*
 * This function stops the fault monitor for a scmasa resource.
 * This is done via PMF. The PMF tag for the fault monitor is
 * constructed based on <RG-name_RS-name,instance_number.mon>.
 */

int
mon_stop(scds_handle_t scds_handle)
{
	scha_err_t	err;

	scds_syslog_debug(DBG_LEVEL_HIGH, "Calling scds_pmf_stop method");

	err = scds_pmf_stop(scds_handle, SCDS_PMF_TYPE_MON,
	    SCDS_PMF_SINGLE_INSTANCE, SIGKILL,
	    scds_get_rs_monitor_stop_timeout(scds_handle));

	if (err != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to stop fault monitor.");
		return (err);
	}
	scds_syslog(LOG_INFO, "Stopped the fault monitor.");

	return (SCHA_ERR_NOERR); /* Successfully stopped monitor */
}


/*
 * svc_probe(): Do data service specific probing. Return a float value
 * between 0 (success) and 100(complete failure).
 *
 * The probe does a simple socket connection to the scmasa server
 * on the specified
 * port which is configured as the resource extension property (Port_list) and
 * pings the dataservice. If the probe fails to connect to the port, we return
 * a value of 100 indicating that there is a total failure. If the connection
 * goes through and the disconnect to the port fails, then a value of 50 is
 * returned indicating a partial failure.
 *
 */
int
svc_probe(scds_handle_t scds_handle, char *hostname, int port, int timeout)
{
	int  rc = 0;
	int probe_status = 0;
	ulong_t	t1, t2;
	int 	sock;
	int 	time_used, time_remaining;
	ulong_t		connect_timeout;
	int cmd_exit_status = 0;
	char service_probe_cmd[SCDS_CMD_SIZE] =
	"/usr/cluster/lib/rgm/rt/hamasa/cmas_service_ctrl_check $hostnames";
	char fo_group[SCDS_CMD_SIZE];
	char cmd[SCDS_CMD_SIZE];

	if (strcmp(service_probe_cmd, "") != 0) {

		/*
		 * probe command is specified; so use that to probe the
		 * application.
		 */
		(void) strcpy(cmd, service_probe_cmd);
		if ((rc = preprocess_cmd(scds_handle, cmd)) != 0) {
			scds_syslog(LOG_ERR,
			    "Failed to form the %s command.", "probe");

			/*
			 * If we fail to preprocess the user specified probe
			 * command we log an error message and proceed to do
			 * the simple probe, i.e. the tcp connect/disconnect
			 * probing.
			 */
			goto simple_probe;
		}

		/*
		 * add the cmas failover group name to the end of the command
		 */
		if (get_cmas_failover_group(scds_handle, fo_group) != 0) {
			scds_syslog(LOG_ERR,
		"Failed to form the %s command.",
		"probe");

			/*
			 * If we fail to preprocess the user specified probe
			 * command we log an error message and proceed to do
			 * the simple probe, i.e. the tcp connect/disconnect
			 * probing.
			 */
			goto simple_probe;
		}

		(void) strcat(cmd, " ");
		(void) strcat(cmd, fo_group);

		rc = scds_timerun(scds_handle, cmd, timeout, SIGKILL,
		    &cmd_exit_status);

		if (rc == SCHA_ERR_TIMEOUT) {
			/*
			 * SCMSGS
			 * @explanation
			 * Timeout occured when executing the probe command
			 * provided by user under the hatimerun(1M) utility.
			 * @user_action
			 * This problem might occur when the cluster is under
			 * load. Consider increasing the Probe_timeout
			 * property.
			 */
			scds_syslog(LOG_ERR,
			    "The probe command <%s> timed out", cmd);

			/*
			 * We return half of the SCDS_PROBE_COMPLETE_FAILURE,
			 * so that two failures in a row will be required
			 * for any action to be taken.
			 */
			probe_status = SCDS_PROBE_COMPLETE_FAILURE/2;
			goto finished;
		}

		if (rc == SCHA_ERR_INTERNAL) {
			char internal_err_str[SCDS_CMD_SIZE];

			(void) snprintf(internal_err_str,
			    SCDS_CMD_SIZE,
			    "error occurred while launching "
			    "the probe command <%s>", cmd);

			scds_syslog(LOG_ERR,
			    "INTERNAL ERROR: %s.",
			    internal_err_str);

			/*
			 * An internal error is likely to be caused by some
			 * system level problem (out of swap space etc.).
			 * Since most of these problems are transient, we
			 * return half of the SCDS_PROBE_COMPLETE_FAILURE,
			 * so that two failures in a row will be required
			 * for any action to be taken.
			 */
			probe_status = SCDS_PROBE_COMPLETE_FAILURE/2;
			goto finished;
		}

		if (rc == SCHA_ERR_INVAL) {
			/*
			 * SCMSGS
			 * @explanation
			 * Failure in executing the command.
			 * @user_action
			 * Check the syslog message for the command
			 * description. Check whether the system is low in
			 * memory or the process table is full and take
			 * appropriate action. Make sure that the executable
			 * exists.
			 */
			scds_syslog(LOG_ERR,
			    "Cannot execute %s: %s.", cmd, cmd_exit_status);
			probe_status = SCDS_PROBE_COMPLETE_FAILURE;
			goto finished;
		}

		/*
		 * return the exit status of the command.
		 */
		probe_status = cmd_exit_status;
		goto finished;
	}

simple_probe:
	/*
	 * If the probe command is not specified we use the normal probe
	 * which simply connects to the port and disconnects.
	 */

	/*
	 * probe the dataservice by doing a socket connection to the port
	 * specified in the port_list property to the host that is
	 * serving the scmasa dataservice. If the scmasa service
	 * which is configured
	 * to listen on the specified port, replies to the connection, then
	 * the probe is successful. Else we will wait for a time period set
	 * in probe_timeout property before concluding that the probe failed.
	 */

	/*
	 * Use the SVC_CONNECT_TIMEOUT_PCT percentage of timeout
	 * to connect to the port
	 */
	connect_timeout = (ulong_t)((SVC_CONNECT_TIMEOUT_PCT * timeout)/100);
	t1 = (ulong_t)(gethrtime()/1E9);

	/*
	 * the probe makes a connection to the specified hostname and port.
	 * The connection is timed for 95% of the actual probe_timeout.
	 */
	rc = scds_fm_tcp_connect(scds_handle, &sock, hostname, port,
				(long)connect_timeout);
	if (rc) {
		scds_syslog(LOG_ERR,
		    "Failed to connect to the host <%s> and port <%d>.",
		    hostname, port);
		/* this is a complete failure */
		probe_status = SCDS_PROBE_COMPLETE_FAILURE;
		goto finished;
	}

	t2 = (ulong_t)(gethrtime()/1E9);

	/*
	 * Compute the actual time it took to connect. This should be less than
	 * or equal to connect_timeout, the time allocated to connect.
	 * If the connect uses all the time that is allocated for it,
	 * then the remaining value from the probe_timeout that is passed to
	 * this function will be used as disconnect timeout. Otherwise, the
	 * the remaining time from the connect call will also be added to
	 * the disconnect timeout.
	 *
	 */

	time_used = (int)(t2 - t1);

	/*
	 * Use the remaining time(timeout - time_took_to_connect) to disconnect
	 */

	time_remaining = timeout - (int)time_used;

	/*
	 * If all the time is used up, use a small hardcoded timeout
	 * to still try to disconnect. This will avoid the fd leak.
	 */
	if (time_remaining <= 0) {
		scds_syslog_debug(DBG_LEVEL_LOW,
		    "svc_probe used entire timeout of "
		    "%d seconds during connect operation and exceeded the "
		    "timeout by %d seconds. Attempting disconnect with timeout"
		    " %d ",
		    connect_timeout,
		    abs(time_used),
		    SVC_DISCONNECT_TIMEOUT_SECONDS);

		time_remaining = SVC_DISCONNECT_TIMEOUT_SECONDS;
	}

	/*
	 * Return partial failure in case of disconnection failure.
	 * Reason: The connect call is successful, which means
	 * the application is alive. A disconnection failure
	 * could happen due to a hung application or heavy load.
	 * If it is the later case, don't declare the application
	 * as dead by returning complete failure. Instead, declare
	 * it as partial failure. If this situation persists, the
	 * disconnect call will fail again and the application will be
	 * restarted.
	 */
	rc = scds_fm_tcp_disconnect(scds_handle, sock, time_remaining);
	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
		    "Failed to disconnect from port %d of resource %s.",
		    port, scds_get_resource_name(scds_handle));
		/* this is a partial failure */
		probe_status = SCDS_PROBE_COMPLETE_FAILURE/2;
		goto finished;
	}

	t2 = (ulong_t)(gethrtime()/1E9);
	time_used = (int)(t2 - t1);
	time_remaining = timeout - time_used;

	/*
	 * If there is no time left, don't do the full test with
	 * fsinfo. Return SCDS_PROBE_COMPLETE_FAILURE/2
	 * instead. This will make sure that if this timeout
	 * persists, server will be restarted.
	 */
	if (time_remaining <= 0) {
		scds_syslog(LOG_ERR, "Probe timed out.");
		probe_status = SCDS_PROBE_COMPLETE_FAILURE/2;
		goto finished;
	}


finished:

	return (probe_status);
}

/* The following are utility routines */

/*
 * This utility routine reads the cmas failover group extension property
 * and if this one is empty, it defaults to the resource name.
 */
int
get_cmas_failover_group(scds_handle_t scds_handle, char *fo_group)
{
	int rc = 0;
	scha_extprop_value_t *fo_group_prop = NULL;
	const char *rs_name = NULL;

	/* first try to read the extension property */
	rc = scds_get_ext_property(scds_handle,
	"Cmas_failover_group", SCHA_PTYPE_STRING, &fo_group_prop);

	if (rc == SCHA_ERR_NOERR) {
		if ((fo_group_prop->val.val_str != NULL) &&
			(strlen(fo_group_prop->val.val_str) != 0)) {

			(void) strcpy(fo_group, fo_group_prop->val.val_str);

			scds_free_ext_property(fo_group_prop);
			return (0);
		}
	}

	/* Extension property empty : try to get the resource name */
	rs_name = scds_get_resource_name(scds_handle);

	if (rs_name == NULL) {
		/*
		 * SCMSGS
		 * @explanation
		 * An internal error prevented the application from recovering
		 * its resource name
		 * @user_action
		 * This is an informational message, no user action is needed
		 */
		scds_syslog(LOG_ERR,
		"impossible to read the resource name");
		return (-1);
	} else {
		(void) strcpy(fo_group, rs_name);
		return (0);
	}
}

/*
 * This utility routine computes the list of hostnames being used by
 * the given "resource".
 * The list of network resources is put in env_var_hostnames.
 */
int
compute_hostnames(scds_handle_t scds_handle, char *env_var_hostnames)
{
	scds_net_resource_list_t *snrlp;
	scds_net_resource_t snrp;
	int rs, ip;


	if (scds_get_rs_hostnames(scds_handle, &snrlp)) {
		/*
		 * SCMSGS
		 * @explanation
		 * The resource requires access to the resource group's
		 * hostnames to perform its action
		 * @user_action
		 * Investigate if the hamasa resource type is correctly
		 * configured. Contact your authorized Sun service provider to
		 * determine whether a workaround or patch is available.
		 */
		scds_syslog(LOG_ERR,
		    "No hostname address found in resource group.");
		return (1);
	}

	/*
	 * Iterate through all resources to get the hostnames
	 */
	for (rs = 0; rs < snrlp->num_netresources; rs++) {
		snrp = snrlp->netresources[rs];
		for (ip = 0; ip < snrp.num_hostnames; ip++) {

			if (rs != 0 || ip != 0)
				(void) strlcat(env_var_hostnames, ",",
					SCDS_CMD_SIZE);

			(void) strlcat(env_var_hostnames, snrp.hostnames[ip],
			    SCDS_CMD_SIZE);
		}
	}

	scds_free_net_list(snrlp);

	return (0);
}

/*
 * This utility routine replaces all occurrences of "var_name" with "var_val"
 * in orig_cmd and puts the output back in orig_cmd. In that sense orig_cmd
 * is an in/out parameter.
 */
int
substitute_var(char *orig_cmd, char *var_name, char *var_val)
{
	char dest[SCDS_CMD_SIZE];
	unsigned int var_len = strlen(var_name);
	unsigned int value_len = strlen(var_val);
	int first_part_len = 0;
	int end_part_len = 0;
	const char *var_occurence = NULL;

	while ((var_occurence = strstr(orig_cmd, var_name)) != 0) {

		first_part_len = var_occurence - orig_cmd;

		end_part_len = orig_cmd + strlen(orig_cmd) -
		    (var_occurence + var_len);

		(void) strncpy(dest, orig_cmd, (unsigned int)first_part_len);
		dest[first_part_len] = '\0';

		if (value_len > (SCDS_CMD_SIZE - strlen(dest) -
			(unsigned int)end_part_len)) {
			return (1);
		}

		(void) strcat(dest, var_val);

		(void) strcat(dest, var_occurence + var_len);
		(void) strcpy(orig_cmd, dest);
	}

	return (0);
}

/*
 * This utility routine searches a given string, typically the commands
 * input to the Builder, for the occurrence of specific Builder defined
 * variables, e.g. $hostnames, and replace them with their appropriate value.
 * Currently, only $hostnames is supported as a Builder defined variable.
 */
int
preprocess_cmd(scds_handle_t scds_handle, char *orig_cmd)
{
	char hostnames[SCDS_CMD_SIZE];
	int rc = 0;

	(void) memset(hostnames, '\0', SCDS_CMD_SIZE);
	if ((rc = compute_hostnames(scds_handle, hostnames)) != 0)
		return (rc);

	if ((rc = substitute_var(orig_cmd, "$hostnames", hostnames)) != 0)
		return (rc);

	return (rc);
}
