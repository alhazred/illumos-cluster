/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident   "@(#)hastorageplus_threads.cc 1.3     08/05/20 SMI"

#include "hastorageplus_threads.h"
#include <sys/threadpool.h>

/*
 * Disabling lint errors.
 */
/* CSTYLED */
/*lint -e1712 */
/* CSTYLED */
/*lint -e1720 */


/* Private definitions */

/*
 * The number of threads that give optimal performance for fsck and mount
 * depends on a number of factors like number of CPU's, number of controllers,
 * number of file systems under each controller, size of each file system etc..
 * It is not easy to arrive at ideal heuristic to find the number of threads
 * by considering all these factors. Also allowing thread creation whenever
 * required leads resource exhaustion and may be without any performance
 * improvement.
 * So the current HAStoragePlus uses HASP_MAX_THREADS threads as hard limit
 * and it can be modified based on requirement.
 */

#define	HASP_MAX_THREADS	25

/*
 * Class definition for the hasp_task.
 * The hasp_task represent the task required by HAStoragePlus like
 * bring device service online, fsck the device and mount the device.
 */


class hasp_task : public defer_task {
public:
	hasp_task(task_func task_handler, void *task_arg) {

		method = task_handler;
		arg = task_arg;
	}

	~hasp_task();

	void execute() {

		(*method)(arg);
		delete this;

	}

private:
	task_func	method;
	void		*arg;

	/*
	 * Disallow assignment and pass by value.
	 */

						/* CSTYLED */
	hasp_task(const hasp_task &);		/*lint -e754 */
						/* CSTYLED */
	hasp_task &operator = (hasp_task &);	/*lint -e754 */

};

static threadpool HAStoragePlus_Threadpool(true, 1,
		"HAStoragePlus Threadpool", HASP_MAX_THREADS);

/*
 * Lint complains about neither zero'ing nor freeing the pointer
 * members : method, arg.
 */
/* CSTYLED */
/*lint -e1540 */
hasp_task::~hasp_task()
{
}
/* CSTYLED */
/*lint +e1540 */

/*
 * API implementation published in hasp_threads.h
 */

int
svc_addTasktoThreadPool(task_func task_handler, void *task_arg)
{
	hasp_task *taskp = new hasp_task(task_handler, task_arg);

	if (taskp == NULL) {

		/*
		 * Object creation failed, execute the task with current thread.
		 */

		(*task_handler)(task_arg);

	} else {

		HAStoragePlus_Threadpool.defer_processing(taskp);
	}
	return (0);

}

void
svc_wait4tasksinThreadPool(void)
{

	HAStoragePlus_Threadpool.quiesce(threadpool::QEMPTY);

}
