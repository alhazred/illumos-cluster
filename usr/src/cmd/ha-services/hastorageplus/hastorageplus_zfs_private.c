/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * hastorageplus_zfs_private.c
 *
 * Common functions invoked by various
 * HAStoragePlus method implementations.
 *
 *
 */
#pragma ident	"@(#)hastorageplus_zfs_private.c	1.12	09/01/21 SMI"

				/* CSTYLED */
#include "hastorageplus.h"	/*lint	-e766 */

#ifdef ZFS_AVAILABLE

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <libintl.h>
#include <sys/mount.h>
#include <dlfcn.h>
#include <link.h>

#include <librtutils.h>

/*
 * Private functions declarations
 */

static int svc_addciZpool(zpool_handle_t *zhp, void *data);

#if	KEEP_ZFS_LEGACY_RESTRICTION
static int svc_checkMntPtProperty(zfs_handle_t *, void *);
#endif

static int svc_mountshareDatasets(zpool_handle_t *, boolean_t, const char *);

static int svc_mountshareDataset(zfs_handle_t *, void *);

static int svc_umountDatasets(zpool_handle_t *, boolean_t);

static int svc_umountDataset(zfs_handle_t *, void *);

/*
 * Private definitions.
 */
#define	ZPOOLS_SEARCHDIR	"/dev/dsk"
#if	KEEP_ZFS_LEGACY_RESTRICTION
#define	ZFS_PROP_LEGACY		"legacy"
#endif

/*
 * XXX Definitions that are taken from zfs private files.
 */
#define	HASP_ALTROOT_PROP_NAME		"altroot"
#define	HASP_CACHEFILE_PROP_NAME	"cachefile"


#define	ZFS_LIB_NAME		"libzfs.so"

#define	ZPOOL_OPEN		"zpool_open"
#define	ZPOOL_OPEN_CANFAIL	"zpool_open_canfail"
#define	ZPOOL_CLOSE		"zpool_close"
#define	ZPOOL_EXPORT		"zpool_export"
#define	ZPOOL_IMPORT		"zpool_import"
#define	ZPOOL_IMPORT_PROPS	"zpool_import_props"
#define	ZPOOL_ITER		"zpool_iter"
#define	ZPOOL_FIND_IMPORT	"zpool_find_import"
#define	ZPOOL_FIND_IMPORT_CACHED	"zpool_find_import_cached"
#define	ZPOOL_GET_NAME		"zpool_get_name"
#define	ZPOOL_GET_STATE		"zpool_get_state"
#define	ZPOOL_SET_PROP		"zpool_set_prop"
#define	ZFS_OPEN		"zfs_open"
#define	ZFS_CLOSE		"zfs_close"
#define	ZFS_GET_TYPE		"zfs_get_type"
#define	ZFS_PROP_GET		"zfs_prop_get"
#define	ZFS_ITER_CHILDREN	"zfs_iter_children"
#define	ZFS_ITER_DEPENDENTS	"zfs_iter_dependents"
#define	ZFS_MOUNT		"zfs_mount"
#define	ZFS_UNMOUNT		"zfs_unmount"
#define	ZFS_SHARE		"zfs_share"
#define	LIBZFS_INIT		"libzfs_init"
#define	LIBZFS_FINI		"libzfs_fini"
#define	LIBZFS_ERRNO		"libzfs_errno"
#define	LIBZFS_ERROR_ACTION	"libzfs_error_action"
#define	LIBZFS_ERROR_DESCRIPTION	"libzfs_error_description"
#define	ZPOOL_MOUNT_DATASETS	"zpool_mount_datasets"
#define	ZPOOL_UNMOUNT_DATASETS	"zpool_unmount_datasets"

#if	SOL_VERSION == __s10
#define	ZFS_SET_ERROR_HANDLER	"zfs_set_error_handler"
static boolean_t		libzfs_is_initial_version = B_FALSE;
#endif

typedef	zpool_handle_t 	*(*libzfs_zpool_open2_t)(libzfs_handle_t *,
			const char *);
typedef	zpool_handle_t	*(*libzfs_zpool_open_canfail2_t)(libzfs_handle_t *,
			const char *);
typedef	void		(*libzfs_zpool_close_t)(zpool_handle_t *);

typedef	int		(*libzfs_zpool_export_t)(zpool_handle_t *);
typedef	int		(*libzfs_zpool_import4_t)(libzfs_handle_t *,
			nvlist_t *, const char *, const char *);

typedef int		(*libzfs_zpool_iter3_t)(libzfs_handle_t *,
			zpool_iter_f, void *);

typedef	nvlist_t	*(*libzfs_zpool_find_import4_t)(libzfs_handle_t *,
			int argc, char **argv, boolean_t active_ok);

typedef const char	*(*libzfs_zpool_get_name_t)(zpool_handle_t *);
typedef int		(*libzfs_zpool_get_state_t)(zpool_handle_t *);

typedef	zfs_handle_t	*(*libzfs_zfs_open3_t)(libzfs_handle_t *,
			const char *, int);
typedef	void		(*libzfs_zfs_close_t)(zfs_handle_t *);
typedef	zfs_type_t	(*libzfs_zfs_get_type_t)(const zfs_handle_t *);

typedef int		(*libzfs_zfs_prop_get_t)(zfs_handle_t *, zfs_prop_t,
			char *, size_t, zprop_source_t *, char *, size_t, int);

typedef	int		(*libzfs_zfs_iter_children_t)(zfs_handle_t *,
			zfs_iter_f, void *);
typedef	int		(*libzfs_zfs_iter_dependents_t)(zfs_handle_t *,
			zfs_iter_f, void *);

typedef	int		(*libzfs_zfs_mount_t)(zfs_handle_t *, const char *,
			int);
typedef	int		(*libzfs_zfs_unmount_t)(zfs_handle_t *, const char *,
			int);
typedef	int		(*libzfs_zfs_share_t)(zfs_handle_t *);

typedef libzfs_handle_t	*(*libzfs_libzfs_init_t)(void);
typedef void		(*libzfs_libzfs_fini_t)(libzfs_handle_t *);
typedef int		(*libzfs_libzfs_errno_t)(libzfs_handle_t *);
typedef const char 	*(*libzfs_libzfs_error_action_t)(libzfs_handle_t *);
typedef const char 	*(*libzfs_libzfs_error_description_t)(
			libzfs_handle_t *);
typedef int 		(*libzfs_zpool_mount_datasets_t)(zpool_handle_t *,
			const char *, int);
typedef int 		(*libzfs_zpool_unmount_datasets_t)(zpool_handle_t *,
			boolean_t);
typedef int		(*libzfs_zpool_import_props_t)(libzfs_handle_t *,
			nvlist_t *, const char *, nvlist_t *);
typedef int		(*libzfs_zpool_set_prop_t)(zpool_handle_t *,
			const char *, const char *);
#if	SOL_VERSION == __s10
typedef nvlist_t	*(*libzfs_zpool_find_import_cached_t)(libzfs_handle_t *,
			const char *, boolean_t, const char *, uint64_t);
#else
typedef nvlist_t	*(*libzfs_zpool_find_import_cached_t)(libzfs_handle_t *,
			const char *, const char *, uint64_t);
#endif

#if	SOL_VERSION == __s10
typedef	zpool_handle_t 	*(*libzfs_zpool_open1_t)(const char *);
typedef	zpool_handle_t	*(*libzfs_zpool_open_canfail1_t)(const char *);
typedef	int		(*libzfs_zpool_import3_t)(nvlist_t *, const char *,
			const char *);
typedef int		(*libzfs_zpool_iter2_t)(zpool_iter_f, void *);
typedef	nvlist_t	*(*libzfs_zpool_find_import2_t)(int argc, char **argv);
typedef	zfs_handle_t	*(*libzfs_zfs_open2_t)(const char *, int);
typedef	void		(*libzfs_zfs_set_error_handler_t)(void (*)(const char *,
			va_list));
#endif

static void		*dl_handle = NULL;
static boolean_t	libzfs_loaded = B_FALSE;
static libzfs_handle_t	*g_zfs;
static boolean_t	is_zfs_cachefile_available = B_FALSE;
static boolean_t	zpools_find_import_done = B_FALSE;
static nvlist_t		*import_pools_list = NULL;

static libzfs_zpool_open2_t		libzfs_zpool_open2;
static libzfs_zpool_open_canfail2_t	libzfs_zpool_open_canfail2;
static libzfs_zpool_close_t		libzfs_zpool_close;
static libzfs_zpool_export_t		libzfs_zpool_export;
static libzfs_zpool_import4_t		libzfs_zpool_import4;
static libzfs_zpool_iter3_t		libzfs_zpool_iter3;
static libzfs_zpool_find_import4_t	libzfs_zpool_find_import4;
static libzfs_zpool_get_name_t		libzfs_zpool_get_name;
static libzfs_zpool_get_state_t		libzfs_zpool_get_state;
static libzfs_zfs_open3_t		libzfs_zfs_open3;
static libzfs_zfs_close_t		libzfs_zfs_close;
static libzfs_zfs_get_type_t		libzfs_zfs_get_type;
								/* CSTYLED */
static libzfs_zfs_prop_get_t		libzfs_zfs_prop_get;	/*lint -e551*/
static libzfs_zfs_iter_children_t	libzfs_zfs_iter_children;
static libzfs_zfs_iter_dependents_t	libzfs_zfs_iter_dependents;
static libzfs_zfs_mount_t		libzfs_zfs_mount;
static libzfs_zfs_unmount_t		libzfs_zfs_unmount;
static libzfs_zfs_share_t		libzfs_zfs_share;
static libzfs_libzfs_init_t		libzfs_libzfs_init;
static libzfs_libzfs_fini_t		libzfs_libzfs_fini;
static libzfs_libzfs_errno_t		libzfs_libzfs_errno;
static libzfs_libzfs_error_action_t	libzfs_libzfs_error_action;
static libzfs_libzfs_error_description_t libzfs_libzfs_error_description;
static libzfs_zpool_mount_datasets_t	libzfs_zpool_mount_datasets;
static libzfs_zpool_unmount_datasets_t	libzfs_zpool_unmount_datasets;
static libzfs_zpool_import_props_t	libzfs_zpool_import_props;
static libzfs_zpool_set_prop_t		libzfs_zpool_set_prop;
static libzfs_zpool_find_import_cached_t libzfs_zpool_find_import_cached;

#if	SOL_VERSION == __s10
static libzfs_zpool_open1_t		libzfs_zpool_open1;
static libzfs_zpool_open_canfail1_t	libzfs_zpool_open_canfail1;
static libzfs_zpool_iter2_t		libzfs_zpool_iter2;
static libzfs_zpool_import3_t		libzfs_zpool_import3;
static libzfs_zpool_find_import2_t	libzfs_zpool_find_import2;
static libzfs_zfs_open2_t		libzfs_zfs_open2;
static libzfs_zfs_set_error_handler_t	libzfs_zfs_set_error_handler;
#endif

/*
 * The wrapper functions for which the function symbols has different number
 * of arguments across versions.
 */
static zpool_handle_t *
hasp_zpool_open(const char *pool)
{
#if	SOL_VERSION == __s10
	if (libzfs_is_initial_version)
		return ((*libzfs_zpool_open1)(pool));
	else
#endif
		return ((*libzfs_zpool_open2)(g_zfs, pool));
}

static zpool_handle_t *
hasp_zpool_open_canfail(const char *pool)
{
#if	SOL_VERSION == __s10
	if (libzfs_is_initial_version)
		return ((*libzfs_zpool_open_canfail1)(pool));
	else
#endif
		return ((*libzfs_zpool_open_canfail2)(g_zfs, pool));
}

static int
hasp_zpool_iter(zpool_iter_f func, void *data)
{
#if	SOL_VERSION == __s10
	if (libzfs_is_initial_version)
		return ((*libzfs_zpool_iter2)(func, data));
	else
#endif
		return ((*libzfs_zpool_iter3)(g_zfs, func, data));
}

static int
hasp_zpool_import(nvlist_t *config, const char *newname, const char *altroot)
{
#if	SOL_VERSION == __s10
	if (libzfs_is_initial_version)
		return (*libzfs_zpool_import3)(config, newname, altroot);
	else
#endif
		return (*libzfs_zpool_import4)(g_zfs, config, newname, altroot);
}

static int
hasp_zpool_import_props(nvlist_t *config, const char *newname, nvlist_t *props)
{
	return (*libzfs_zpool_import_props)(g_zfs, config, newname, props);
}

/*
 * The API zpool_find_import of libzfs.so.2 in s10u6 introduced a new argument
 * "active_ok" to skip the import checks, but the change is not in s11.
 * To cover all the versions, we call the API with extra argument and it
 * will ignore the last argument in cases where the extra argument does not
 * required.
 */
static nvlist_t *
hasp_zpool_find_import(int argc, char **argv)
{
#if	SOL_VERSION == __s10
	if (libzfs_is_initial_version)
		return ((*libzfs_zpool_find_import2)(argc, argv));
	else
#endif
		return
		    ((*libzfs_zpool_find_import4)(g_zfs, argc, argv, B_FALSE));
}

static nvlist_t *
hasp_zpool_find_import_cached(const char *cachefile, const char *poolname)
{
#if	SOL_VERSION == __s10
	return ((*libzfs_zpool_find_import_cached)(g_zfs, cachefile, B_FALSE,
	    poolname, 0ll));
#else
	return ((*libzfs_zpool_find_import_cached)(g_zfs, cachefile,
	    poolname, 0ll));
#endif
}

static zfs_handle_t *
hasp_zfs_open(const char *path, int types)
{
#if	SOL_VERSION == __s10
	if (libzfs_is_initial_version)
		return ((*libzfs_zfs_open2)(path, types));
	else
#endif
		return ((*libzfs_zfs_open3)(g_zfs, path, types));
}

/*
 * Wrapper function to report the zfs messages.
 * In the initial version of libzfs, an error handler can be registered.
 * In the next versions, the error number is retuned by libzfs handle
 * and we have print the error messages using libzfs_error_action() /
 * libzfs_error_description().
 */
static void
hasp_print_msg(int log_level)
{
#if	SOL_VERSION == __s10
	if (!libzfs_is_initial_version)
#endif
	{
		if ((*libzfs_libzfs_errno)(g_zfs) == EZFS_UNKNOWN) {
			/*
			 * SCMSGS
			 * @explanation
			 * Self explanatory.
			 * @user_action
			 * Contact your authorized Sun service provider
			 * to determine whether a workaround or patch is
			 * available.
			 */
			hasp_scds_syslog_1(log_level, "Internal error: %s.",
			    (*libzfs_libzfs_error_description)(g_zfs));
			return;
		}

		/*
		 * SCMSGS
		 * @explanation
		 * Self explanatory.
		 * @user_action
		 * Check the syslog messages and try to resolve the problem.
		 * If the problem persists, contact your authorized Sun service
		 * provider.
		 */
		hasp_scds_syslog_2(log_level, "%s : %s",
		    (*libzfs_libzfs_error_action)(g_zfs),
		    (*libzfs_libzfs_error_description)(g_zfs));
	}
}

static void
hasp_print_zfs_errmsg(void) {
	hasp_print_msg(LOG_ERR);
}

/*
 * The following is the common structure that contains call back information
 * which will be used for all zfs iterator functions in HAStoragePlus.
 */

typedef struct hasp_cbdata {
	int			result;		/* Result of the call back */
	scha_str_array_t	*zpool_list;	/* Location of pool list */
#if	KEEP_ZFS_LEGACY_RESTRICTION
	const char		*poolname;	/* Pool name */
#endif
	boolean_t		force;		/* Force import/export ? */
	boolean_t		overlay;	/* Overlay mount? */
	const char		*mntoptions;	/* ZFS mount options */
} hasp_cbdata_t;


/*
 * Function: svc_getciZpoolsList
 *
 * Arguments:
 *
 * zpool_list	The location where the list of currently imported pools to be
 *		saved.
 *
 * Return:
 *
 * 0	If list of currently imported pools are retrieved and saved
 *	successfully.
 *
 * >0	Failed to get the pools.
 *
 * Comments:
 * This function retrieves the pools that are currently active on this node.
 * NOTE: This function implements "zpool list -H -o name"
 */
int
svc_getciZpoolsList(scha_str_array_t *zpool_list)
{
	hasp_cbdata_t	hcb;

	hcb.zpool_list = zpool_list;
	hcb.result = 0;

	(void) hasp_zpool_iter(svc_addciZpool, (void *) &hcb);
	return (hcb.result);
}

/*
 * Function: svc_addciZpool
 *
 * Arguments:
 *
 * zhp		The zpool handle which is currently imported on this node.
 *
 * data		Contains call back information.
 *
 * Return:
 *
 * 0		If the pool named is added to zpool_list successfully.
 *
 * >0		Failed to add the zpool name.
 *
 * Comments:
 * This is call back function of svc_getciZpoolsList(). It adds the pool name
 * of the zfs handle 'zhp' to the pool list.
 */
static int
svc_addciZpool(zpool_handle_t *zhp, void *data)
{
	hasp_cbdata_t	*hcbp = (hasp_cbdata_t *)data;

	hcbp->result += svc_addStr2Array((*libzfs_zpool_get_name)(zhp),
	    hcbp->zpool_list);

	/*
	 * Close the zpool handle.
	 * The zpool_iter() opens and calls this function and it is
	 * responsibility of the call back function to close the zpool handle.
	 */

	(*libzfs_zpool_close)(zhp);

	return (hcbp->result);
}

/*
 * Function: svc_findZpoolsToImport
 *
 * Arguments:
 *
 * cache_conf_ok	Determines whether to use the cached list of all
 * 			pools configuration.
 *
 * Return:
 * nvlist_t*	The list ZFS pools that can be imported.
 *
 * Comments:
 * This function returns the list of zpools configuration that can be imported.
 * It caches the ZFS pools list as zpool_find_import() is expensive operation.
 */
nvlist_t *
svc_findZpoolsToImport(boolean_t cache_conf_ok)
{
	char	*searchdirs[1] = { NULL };

	/*
	 * Return if pools already found and requester is fine with cached info.
	 */
	if (cache_conf_ok && zpools_find_import_done == B_TRUE) {
		return (import_pools_list);
	}

	/*
	 * Free the old configuration.
	 */
	nvlist_free(import_pools_list);
	import_pools_list = NULL;

	/*
	 * Find the pools that can be imported.
	 */
	if ((gSvcInfo.zpoolsSearchDir == NULL) ||
	    STREQ(gSvcInfo.zpoolsSearchDir, SvcNullString)) {
		searchdirs[0] = ZPOOLS_SEARCHDIR;
	} else {
		searchdirs[0] = (char *)gSvcInfo.zpoolsSearchDir;
	}

	/*
	 * SCMSGS
	 * @explanation
	 * This is informational message that HAStoragePlus has started
	 * searching for potential ZFS pools to import.
	 * @user_action
	 * No user action is needed.
	 */
	hasp_scds_syslog_1(LOG_NOTICE, "Started searching for devices "
	    "in '%s' to find the importable pools.", searchdirs[0]);

	import_pools_list = hasp_zpool_find_import(1, searchdirs);

	/*
	 * SCMSGS
	 * @explanation
	 * This is informational message that HAStoragePlus has completed
	 * searching for potential ZFS pools to import.
	 * @user_action
	 * No user action is needed.
	 */
	hasp_scds_syslog_1(LOG_NOTICE, "Completed searching the devices "
	    "in '%s' to find the importable pools.", searchdirs[0]);

	zpools_find_import_done = B_TRUE;

	return (import_pools_list);
}

/*
 * Function: svc_findZpoolConf
 *
 * Arguments:
 *
 * poolname		pool name which the configuration to search
 *
 * pools		list of potential pools that cen be imported
 *
 * pool_config		retuned value stored here after successful search
 *
 * Return:
 * nvlist_t*	The list ZFS pools that can be imported.
 *
 * Comments:
 * This function obtains the specific pool configuration from the set of
 * potential pools configuration. It also validates the exitence of the pool
 * and also ensures there is unique pool name in the configuration.
 */
static int
svc_findZpoolConf(const char *poolname, nvlist_t *pools, nvlist_t **pool_config)
{
	nvpair_t	*elem = NULL;
	nvlist_t	*config;
	char		*name = NULL;
	uint64_t	pool_state;

	*pool_config = NULL;
	if (pools == NULL) {
		return (0);
	}

	while ((elem = nvlist_next_nvpair(pools, elem)) != NULL) {

		/*
		 * Obtain the nvlist entry of the pool.
		 */
		verify(nvpair_value_nvlist(elem, &config) == 0);

		/*
		 * Skip the destroyed pools.
		 */
		verify(nvlist_lookup_uint64(config,
		    ZPOOL_CONFIG_POOL_STATE, &pool_state) == 0);
		if (pool_state == POOL_STATE_DESTROYED) {
			continue;
		}

		/*
		 * Obtain the name of the pool.
		 */
		verify(nvlist_lookup_string(config,
		    ZPOOL_CONFIG_POOL_NAME, &name) == 0);

		if (strcmp(name, poolname) == 0) {
			if (*pool_config != NULL) {
				/* Multiple pools with this name */
				hasp_scds_syslog_1(LOG_ERR,
				    "More than one matching zpool for "
				    "'%s': zpools configured to "
				    "HAStoragePlus must be unique.",
				    poolname);
				return (1);
			}
			*pool_config = config;
		}
	}

	return (0);
}

/*
 * Function: svc_getpiZpoolsList
 *
 * Arguments:
 *
 * zpool_list	The location where the list of pools that can be potentially
 *		imported to be saved.
 *
 * Return:
 *
 * 0	If list of pools are retrieved and saved successfully.
 *
 * >0	Failed to get the pools.
 *
 * Comments:
 * This function retrieves the pools that can be potentially imported to this
 * node under the provided directory location.
 * The default location is "/dev/dsk".
 * NOTE: This function implements "zpool import [-d dir]"
 */
int
svc_getpiZpoolsList(scha_str_array_t *zpool_list, boolean_t cache_conf_ok)
{
	nvlist_t	*pools;
	nvpair_t	*elem = NULL;
	nvlist_t	*config;
	char		*name;
	int		rc = 0;
	uint64_t	pool_state;

	/*
	 * Obtain the pools.
	 */
	if ((pools = svc_findZpoolsToImport(cache_conf_ok)) == NULL) {
		return (0);
	}

	/*
	 * Iterate through pool list and copy each pool name.
	 */

	while ((elem = nvlist_next_nvpair(pools, elem)) != NULL) {

		/*
		 * Obtain the nvlist entry of the pool.
		 */
		verify(nvpair_value_nvlist(elem, &config) == 0);

		/*
		 * Skip the destroyed pools.
		 */
		verify(nvlist_lookup_uint64(config,
		    ZPOOL_CONFIG_POOL_STATE, &pool_state) == 0);
		if (pool_state == POOL_STATE_DESTROYED) {
			continue;
		}

		/*
		 * Obtain the name of the pool.
		 */
		verify(nvlist_lookup_string(config,
		    ZPOOL_CONFIG_POOL_NAME, &name) == 0);

		/*
		 * Add the name of the pool to the list.
		 */
		if (svc_addStr2Array(name, zpool_list)) {
			rc = 1;
			break;
		}
	}
	return (rc);
}

#if	KEEP_ZFS_LEGACY_RESTRICTION

/*
 * Function: svc_validateMntPtProperty
 *
 * Arguments:
 *
 * poolname		The pool name to validate.
 *
 * Return:
 *
 * 0	If the mount point property of all file systems in the pool is not set
 *	to 'legacy'
 *
 * 1	If set to 'legacy'
 *
 * Comments:
 * This function validate the mountpoint property of any file system in the
 * pool is not set to 'legacy'.
 * NOTE:
 * HAStoragePlus will not support the pools which has file systems with
 * mountpoint property set to 'legacy'.
 */
int
svc_validateMntPtProperty(const char *poolname)
{
	hasp_cbdata_t	hcb;
	zpool_handle_t	*zhp;
	zfs_handle_t	*zfsp;

	hcb.result = 0;
	hcb.poolname = poolname;

	if ((zhp = hasp_zpool_open(poolname)) == NULL) {
		hasp_print_zfs_errmsg();
		return (1);
	}

	if ((zfsp = hasp_zfs_open((*libzfs_zpool_get_name)(zhp),
	    ZFS_TYPE_FILESYSTEM)) == NULL) {
		hasp_print_zfs_errmsg();
		(*libzfs_zpool_close)(zhp);
		return (1);
	}

	/*
	 * Check for this dataset.
	 */
	if (svc_checkMntPtProperty(zfsp, (void *) &hcb) || hcb.result != 0) {
		(*libzfs_zfs_close)(zfsp);
		(*libzfs_zpool_close)(zhp);
		return (1);
	}

	(*libzfs_zfs_close)(zfsp);
	(*libzfs_zpool_close)(zhp);

	scds_syslog_debug(DBG_LEVEL_HIGH, "The pool \" %s \" is validated "
	    "successfully", poolname);
	return (0);
}

/*
 * Function: svc_checkMntPtProperty
 *
 * Arguments:
 *
 * zfsp		The file system handle.
 *
 * data		Contains call back information.
 *
 * Return:
 *
 * 0	If the mount point property of file system is not set to 'legacy'
 *
 * 1	If set to 'legacy'
 *
 * Comments:
 * This is call back function of svc_validateMntPtProperty(). It checks
 * whether the mount point propery of file system is set to 'legacy' or not.
 */
static int
svc_checkMntPtProperty(zfs_handle_t *zfsp, void *data)
{
	hasp_cbdata_t	*hcbp = (hasp_cbdata_t *)data;
	char		mountpoint[ZFS_MAXPROPLEN];
	char		fs_name[ZFS_MAXNAMELEN];
	int		ret;

	/*
	 * The mountpoint property applies to only file system datasets, so
	 * skip any other dataset types.
	 */
	if ((*libzfs_zfs_get_type)(zfsp) != ZFS_TYPE_FILESYSTEM)
		return (0);

	/*
	 * Obtain the mount point property of the file system.
	 */
	verify((*libzfs_zfs_prop_get)(zfsp, ZFS_PROP_MOUNTPOINT, mountpoint,
	    sizeof (mountpoint), NULL, NULL, 0, B_FALSE) == 0);

	/*
	 * Verify the property and return error if set to 'legacy'.
	 */
	if (strcmp(mountpoint, ZFS_PROP_LEGACY) == 0) {

		/*
		 * Obtain the name of the file system.
		 */
		verify((*libzfs_zfs_prop_get)(zfsp, ZFS_PROP_NAME, fs_name,
		    sizeof (fs_name), NULL, NULL, 0, B_FALSE) == 0);

		/*
		 * SCMSGS
		 * @explanation
		 * Self explanatory. HAStoragePlus does not support Zettabyte
		 * File Systems (ZFS) as Failover File Systems (FFS) that have
		 * the mountpoint property set to 'legacy'.
		 * @user_action
		 * Set the mountpoint property for the specified file system
		 * to 'default'.
		 */
		hasp_scds_syslog_2(LOG_ERR, "The mount point property "
		    "of file system '%s' in pool '%s' is set to "
		    "'legacy', which is not supported by HAStoragePlus.",
		    fs_name, hcbp->poolname);

		hcbp->result = 1;
		return (1);
	}

	/*
	 * Check for all children of this dataset.
	 */
	ret = (*libzfs_zfs_iter_children)(zfsp, svc_checkMntPtProperty, data);

	return (ret);
}

#endif

/*
 * Function: svc_importZpoolListNormal
 *
 * Arguments:
 *
 * zpoollist		The list of pools to be imported.
 *
 * force		Flag to specify whether to import forcefully or not.
 *
 * overlay		Flag to specify whether to import by mouting the
 *			file system by overlay mount.
 *
 * faillist		list of zpools that are failed to import
 *
 * altroot		Alternate root path where the zpools to be imported.
 *
 * mntoptions		The mount options used to import.
 *
 * Return:
 *
 * 0	If imported successfully.
 *
 * 1	If import fails.
 *
 * Comments:
 * This function import the pool and then mount the file systems in the pool.
 * HAStoragePlus requires pool import (and mouting file systems) should not
 * persist across reboots. This is to avoid a pool being imported on 2 nodes
 * during failover time.
 * This is achieved by importing a pool always with an explicit alternate root.
 * NOTE: This function implements
 * "zpool import [-d dir] [-f] [-O] [-o opts] -R <altroot> <pool1> <pool2> ..
 */
static int
svc_importZpoolListNormal(const scha_str_array_t *zpoollist,
    boolean_t force, boolean_t overlay, const char *mntoptions,
    scha_str_array_t *faillist, const char *altroot)
{
	uint_t		i;
	nvlist_t	*pools;
	uint64_t	pool_state;
	nvlist_t	*config;
	zpool_handle_t	*zhp = NULL;
	char		*poolname = NULL;
	int		rc = 0;
	int		ret = 0;
	int		flags = (overlay) ? MS_OVERLAY : 0;

	if ((zpoollist == NULL) || (zpoollist->array_cnt == 0)) {
		return (0);
	}

	/*
	 * Obtain the pools.
	 */
	pools = svc_findZpoolsToImport(B_TRUE);

	/*
	 * Iterate through pool list to import.
	 */

	for (i = 0; i < zpoollist->array_cnt; i++) {

		rc = 0;

		poolname = zpoollist->str_array[i];

		if (svc_findZpoolConf(poolname, pools, &config)) {
			rc = 1;
			goto error;
		}

		if (config == NULL) {
			/*
			 * No such poolname.
			 */
			hasp_scds_syslog_1(LOG_ERR,
			    "The zpool '%s' is not available from this node.",
			    poolname);
			rc = 1;
			goto error;
		}

		verify(nvlist_lookup_uint64(config,
		    ZPOOL_CONFIG_POOL_STATE, &pool_state) == 0);

		if (pool_state != POOL_STATE_EXPORTED && !force) {
			/*
			 * SCMSGS
			 * @explanation
			 * Self explanatory.
			 * @user_action
			 * Check the cluster node that has imported the
			 * specified pool. Try to export the zpool again by
			 * issuing "zpool export -f <poolname>". If the
			 * problem persists, contact your authorized Sun
			 * service provider.
			 */
			hasp_scds_syslog_1(LOG_ERR, "Cannot import '%s' : "
			    "pool may be in use from other system.", poolname);
			rc = 1;
			goto error;
		}

		if (hasp_zpool_import(config, NULL, altroot) != 0) {
			rc = 1;
			goto error;
		}

#if	KEEP_ZFS_LEGACY_RESTRICTION
		/*
		 * Validate the mountpoint property of file systems in the
		 * pool.
		 */
		if (svc_validateMntPtProperty(poolname)) {
			rc = 1;
			goto error;
		}

		scds_syslog_debug(DBG_LEVEL_HIGH, "The pool \"%s\" mountpoint "
		    " property is validated.", poolname);
#endif

		if ((zhp = hasp_zpool_open(poolname)) == NULL) {
			rc = 1;
			goto error;
		}

		if (libzfs_zpool_mount_datasets == NULL) {
			if (svc_mountshareDatasets(zhp, overlay, mntoptions)) {
				rc = 1;
				goto error;
			}
		} else {
			if ((*libzfs_zpool_mount_datasets)(zhp, mntoptions,
			    flags) != 0) {
				hasp_print_zfs_errmsg();
				rc = 1;
				goto error;
			}
		}

		scds_syslog_debug(DBG_LEVEL_HIGH, "The pool \" %s \" has "
		    "completed mount", poolname);
error:
		if (rc) {
			if (svc_addStr2Array(poolname, faillist)) {
				if (zhp)
					(*libzfs_zpool_close)(zhp);
				return (1);
			}
			ret = 1;
		}
		if (zhp) {
			(*libzfs_zpool_close)(zhp);
			zhp = NULL;
		}
	}
	return (ret);
}

/*
 * This function change ZFS pool cachefile property value to a specified
 * new value.
 */
static int
svc_setZpoolCachefileLocation(const char *poolname, const char *newcachefile)
{

	zpool_handle_t	*zhp;

	if ((zhp = hasp_zpool_open_canfail(poolname)) == NULL) {
		hasp_print_msg(LOG_WARNING);
		return (1);
	}

	/*
	 * Set the zpool cachefile option.
	 */
	if ((*libzfs_zpool_set_prop)(zhp,
	    HASP_CACHEFILE_PROP_NAME, newcachefile) != 0) {
		hasp_print_msg(LOG_WARNING);
		(*libzfs_zpool_close)(zhp);
		return (1);
	}

	(*libzfs_zpool_close)(zhp);

	scds_syslog_debug(DBG_LEVEL_HIGH, "The pool '%s' cachefile value "
	    "set to %s.", poolname, newcachefile);

	return (0);
}

/*
 * Imports the pool by scanning the disks connected to the
 * system and also populates the CCR table with the cachefile contents
 * it has found for future use.
 * It simulates
 * 	zpool import -o cachefile=<import_path> <poolname>
 * 	zpool set cachefile=<cachefile_path> <poolname>
 * This function initially imports the ZFS pool configuration in a
 * non standard location and laters set the cachefile path to standard location.
 * This is to prevent the ZFS pool plugin to update the CCR with an event even
 * before HAStoragePlus updates the CCR with correct values (including the
 * latest ZpoolssearchDir value which is not known to zpool plugin)
 */
static int
svc_importZpoolAndPopulateCCRCachefile(const char *poolname, boolean_t force,
    boolean_t overlay, const char *mntoptions, const char *altroot)
{
	nvlist_t	*pools = NULL;
	nvlist_t	*pool_config = NULL;
	uint64_t	pool_state;
	nvlist_t	*props = NULL;
	zpool_handle_t	*zhp = NULL;
	int		flags = (overlay) ? MS_OVERLAY : 0;
	err_msg_t	err;
	int		rc = 0;
	char		zcache_ccr_tab[MAXPATHLEN];
	char		cachefile_path[MAXPATHLEN];
	char		import_cachefile_path[MAXPATHLEN];

	pools = svc_findZpoolsToImport(B_TRUE);

	if (svc_findZpoolConf(poolname, pools, &pool_config)) {
		return (1);
	}
	if (pool_config == NULL) {
		/*
		 * No such poolname availble.
		 */
		hasp_scds_syslog_1(LOG_ERR, "The zpool '%s' is not "
		    "available from this node.", poolname);
		return (1);
	}

	verify(nvlist_lookup_uint64(pool_config,
	    ZPOOL_CONFIG_POOL_STATE, &pool_state) == 0);

	if (pool_state != POOL_STATE_EXPORTED && !force) {
		hasp_scds_syslog_1(LOG_ERR, "Cannot import '%s' : "
		    "pool may be in use from other system.", poolname);
		return (1);
	}

	/*
	 * We found the zpool configuration.
	 * Build the properties like altroot, cachefile to import the zpool.
	 */
	get_pool_import_cachefile_path(poolname, import_cachefile_path,
	    MAXPATHLEN);

	if ((nvlist_alloc(&props, NV_UNIQUE_NAME, 0) != 0) ||
	    (nvlist_add_string(props, HASP_ALTROOT_PROP_NAME, altroot) != 0) ||
	    (nvlist_add_string(props, HASP_CACHEFILE_PROP_NAME,
	    import_cachefile_path) != 0)) {
		hasp_scds_syslog_0(LOG_ERR, "Failed to allocate memory.");
		return (1);
	}

	/*
	 * Import the zpool with properties.
	 */
	if (hasp_zpool_import_props(pool_config, NULL, props) != 0) {
		hasp_print_zfs_errmsg();
		rc = 1;
		goto error;
	}

	if ((zhp = hasp_zpool_open(poolname)) == NULL) {
		hasp_print_zfs_errmsg();
		rc = 1;
		goto error;
	}

	if ((*libzfs_zpool_mount_datasets)(zhp, mntoptions, flags) != 0) {
		hasp_print_zfs_errmsg();
		rc = 1;
		goto error;
	}

error:
	if (zhp) {
		(*libzfs_zpool_close)(zhp);
	}
	if (rc) {
		/*
		 * SCMSGS
		 * @explanation
		 * HAStoragePlus failed to import the specified ZFS pool.
		 * @user_action
		 * Check the error messages realted to specified pool in the log
		 * message to determine the failure. Re-try the operation after
		 * rectifying the problem. If the problem persists contact your
		 * authorized Sun service provider.
		 */
		hasp_scds_syslog_1(LOG_ERR, "The pool '%s' failed to "
		    "import and populate cachefile.", poolname);
		return (rc);
	}

	scds_syslog_debug(DBG_LEVEL_HIGH, "The pool '%s' succeed to "
	    "import and populated cachefile '%s'.",
	    poolname, import_cachefile_path);

	/*
	 * The ZFS pool is imported successfully and cachefile is
	 * generated.
	 * Now dump the cachefile contents to CCR table for future use.
	 */
	get_pool_ccr_tabname(poolname, zcache_ccr_tab, MAXPATHLEN);
	if (dump_zcache_to_ccr(poolname, import_cachefile_path,
	    gSvcInfo.zpoolsSearchDir, &err)) {
		/*
		 * Failure to update the cachefile contents to CCR is
		 * not a fatal, as the configuration can be found from
		 * the system.
		 * Log a warning message and proceed.
		 */
		/*
		 * SCMSGS
		 * @explanation
		 * HAStoragePlus detected that there is change in
		 * specfified pool configuration, but failed to update
		 * the changes to specfified CCR table.
		 * This will not affect the pool operations in any way.
		 * An update failure of cachefile to CCR misses the
		 * chance for HAStoragePlus to import the pool faster.
		 * @user_action
		 * Contact your authorized Sun service provider with
		 * /var/adm/messages of all nodes to determine whether a
		 * workaround or patch is available.
		 */
		hasp_scds_syslog_4(LOG_WARNING,
		    "Failed to update the cachefile contents in %s to "
		    "CCR table %s for pool %s : %s.",
		    import_cachefile_path, zcache_ccr_tab, poolname,
		    err.message);

		/* Continue to set the cachefile property */
	}

	scds_syslog_debug(DBG_LEVEL_HIGH,
	    "The cachefile contents in %s is updated to CCR table %s "
	    "for pool %s : %s.",
	    import_cachefile_path, zcache_ccr_tab, poolname, err.message);

	/*
	 * Now update the cachefile property to standard location
	 * so that the online pool configuration changes will be recorded
	 * in it, and the plugin (zpool_cachefile_plugin) will updates the
	 * modified changes to CCR for future pool imports.
	 */
	get_pool_cachefile_path(poolname, cachefile_path, MAXPATHLEN);

	if (svc_setZpoolCachefileLocation(poolname, cachefile_path)) {
		/*
		 * Failure to set cachefile value is not fatal as the future
		 * changes of ZFS pool can discovered by scanning all the disks.
		 * Log a message about failure and proceed.
		 */
		/*
		 * SCMSGS
		 * @explanation
		 * HAStoragePlus failed to change the specified pool
		 * 'cachefile' property.
		 * This will not affect the pool operations in any way.
		 * It will cause HAStoragePlus fail to record the online
		 * pool configuration and thus prevents to import the
		 * pool faster.
		 * @user_action
		 * Contact your authorized Sun service provider with
		 * /var/adm/messages of all nodes to determine whether a
		 * workaround or patch is available.
		 */
		hasp_scds_syslog_2(LOG_WARNING,
		    "Failed to set cachefile location "
		    "to '%s' for ZFS pool '%s'.",
		    cachefile_path, poolname);
	}
	return (rc);
}

/*
 * This function tries to import the ZFS pool with the cachefile contents
 * exists in the CCR table.
 * This simulates "zpool import -c <file> -o cachefile=<file> <poolname>".
 * A failure to read the cachefile contents or failure to import with invalid
 * contents etc will be handled transparently with out reporting error and not
 * causing HAStoragePlus methods to fail. In case of failure, it will fallback
 * to the approach where it queries the entire connected disks to discover
 * the pool configuration.
 */
static int
svc_importZpoolWithCCRCachefile(const char *poolname, boolean_t force,
    boolean_t overlay, const char *mntoptions, const char *altroot)
{
	boolean_t	zcache_ccr_tab_exists;
	char		zcache_ccr_tab[MAXPATHLEN];
	char		cachefile_path[MAXPATHLEN];
	nvlist_t	*pools = NULL;
	nvlist_t	*pool_config = NULL;
	nvlist_t	*props = NULL;
	uint64_t	pool_state;
	zpool_handle_t	*zhp = NULL;
	int		flags = (overlay) ? MS_OVERLAY : 0;
	char		*search_dir_value = NULL;
	err_msg_t	err;
	int		rc = 0;

	get_pool_cachefile_path(poolname, cachefile_path, MAXPATHLEN);
	get_pool_ccr_tabname(poolname, zcache_ccr_tab, MAXPATHLEN);

	scds_syslog_debug(DBG_LEVEL_HIGH,
	    "Trying to importing the pool %s using CCR table %s contents.",
	    poolname, zcache_ccr_tab);
	if (check_pool_table(poolname, &zcache_ccr_tab_exists, &err)) {
		/*
		 * A check on the existence of the CCR table to read the
		 * cachefile contents is failed.
		 * So import the pool normally and populate cachefile in CCR.
		 */
		/*
		 * SCMSGS
		 * @explanation
		 * HAStoragePlus failed to check the CCR table which will
		 * contain the cachefile contents of the specified pool.
		 * This will not affect the pool operations in any way.
		 * This will prevent HAStoragePlus to use the cachefile contents
		 * to import the pool faster.
		 * @user_action
		 * Contact your authorized Sun service provider with
		 * /var/adm/messages of all nodes to determine whether a
		 * workaround or patch is available.
		 */
		hasp_scds_syslog_3(LOG_WARNING,
		    "Check for existence of cachefile CCR table %s "
		    "for pool %s is failed : %s",
		    zcache_ccr_tab, poolname, err.message);
		return (svc_importZpoolAndPopulateCCRCachefile(poolname,
		    force, overlay, mntoptions, altroot));
	}

	if (!zcache_ccr_tab_exists) {

		/*
		 * The CCR table doesn't exists.
		 * This can be case for first time, where ZFS pool is being
		 * controlled by HAStoragePlus.
		 * So import the pool normally and populate cachefile after
		 * creating the CCR table for future use.
		 */
		scds_syslog_debug(DBG_LEVEL_HIGH, "The CCR table %s for "
		    "pool %s does not exists. Creating it ..",
		    zcache_ccr_tab, poolname);
		if (create_pool_table(poolname, &err)) {
			/*
			 * SCMSGS
			 * @explanation
			 * HAStoragePlus failed to create CCR table to store
			 * the cachefile contents of the specified pool.
			 * This will not affect the pool operations in any way.
			 * This will prevent HAStoragePlus to store the
			 * cachefile contents for future use and import of the
			 * pool will be slower.
			 * @user_action
			 * Contact your authorized Sun service provider with
			 * /var/adm/messages of all nodes to determine whether a
			 * workaround or patch is available.
			 */
			hasp_scds_syslog_3(LOG_WARNING,
			    "Failed to create CCR table %s to store cachefile "
			    "contents for pool %s : %s",
			    zcache_ccr_tab, poolname, err.message);
		}
		return (svc_importZpoolAndPopulateCCRCachefile(poolname,
		    force, overlay, mntoptions, altroot));
	}

	scds_syslog_debug(DBG_LEVEL_HIGH,
	    "The cachefile contents CCR table %s exists for pool %s.",
	    zcache_ccr_tab, poolname);

	if (read_zcache_from_ccr(poolname, cachefile_path,
	    &search_dir_value, &err)) {

		/*
		 * Failed to read the cachefile contents in the CCR.
		 * So import the pool normally and populate cachefile in CCR.
		 */
		/*
		 * SCMSGS
		 * @explanation
		 * HAStoragePlus failed to read the CCR table which
		 * contains the cachefile contents of the specified pool.
		 * This will not affect the pool operations in any way.
		 * This will prevent HAStoragePlus to use the cachefile
		 * contents to import the pool faster.
		 * @user_action
		 * Contact your authorized Sun service provider with
		 * /var/adm/messages of all nodes to determine whether a
		 * workaround or patch is available.
		 */
		hasp_scds_syslog_3(LOG_WARNING,
		    "Failed to read CCR table %s for cachefile contents "
		    "of pool %s : %s.",
		    zcache_ccr_tab, poolname, err.message);
		return (svc_importZpoolAndPopulateCCRCachefile(poolname,
		    force, overlay, mntoptions, altroot));
	}

	scds_syslog_debug(DBG_LEVEL_HIGH,
	    "Read the CCR table %s and stored the cachefile contents to %s "
	    "for pool %s.",
	    zcache_ccr_tab, cachefile_path, poolname);

	if (!STREQ(search_dir_value, gSvcInfo.zpoolsSearchDir)) {
		/*
		 * The devices in the cachefile contents belongs to different
		 * "ZpoolsSearchDir" to the currently configured value.
		 * So import the pool normally and populate cachefile in CCR.
		 */
		scds_syslog_debug(DBG_LEVEL_HIGH, "The current configured "
		    "ZpoolsSearchDir property value %s is different from "
		    "value stored in CCR table %s for pool %s.",
		    gSvcInfo.zpoolsSearchDir, search_dir_value, poolname);
		return (svc_importZpoolAndPopulateCCRCachefile(poolname,
		    force, overlay, mntoptions, altroot));
	}

	pools = hasp_zpool_find_import_cached(cachefile_path, poolname);

	if (svc_findZpoolConf(poolname, pools, &pool_config)) {
		return (1);
	}

	if (pool_config == NULL) {
		/*
		 * No such poolname in the cachefile.
		 * So import the pool normally and populate cachefile in CCR.
		 */
		scds_syslog_debug(DBG_LEVEL_HIGH, "The ZFS pool '%s' "
		    "configuration is not available in cachefile '%s' "
		    "got from CCR table '%s'.",
		    poolname, cachefile_path, zcache_ccr_tab);
		return (svc_importZpoolAndPopulateCCRCachefile(poolname,
		    force, overlay, mntoptions, altroot));
	}

	verify(nvlist_lookup_uint64(pool_config,
	    ZPOOL_CONFIG_POOL_STATE, &pool_state) == 0);

	if (pool_state != POOL_STATE_EXPORTED && !force) {
		hasp_scds_syslog_1(LOG_ERR, "Cannot import '%s' : "
		    "pool may be in use from other system.", poolname);
		return (1);
	}

	/*
	 * We found the zpool configuration.
	 * Build the properties like altroot, cachefile to import the zpool.
	 * Set the ZFS pool 'cachefile' property to standard location
	 * so that the online pool configuration changes will be recorded
	 * in it, and the plugin (zpool_cachefile_plugin) will updates the
	 * modified changes to CCR for future pool imports.
	 */
	if ((nvlist_alloc(&props, NV_UNIQUE_NAME, 0) != 0) ||
	    (nvlist_add_string(props, HASP_ALTROOT_PROP_NAME, altroot) != 0) ||
	    (nvlist_add_string(props, HASP_CACHEFILE_PROP_NAME,
	    cachefile_path) != 0)) {
		hasp_scds_syslog_0(LOG_ERR, "Failed to allocate memory.");
		return (1);
	}

	/*
	 * Import the zpool with the properties.
	 */
	if (hasp_zpool_import_props(pool_config, NULL, props) != 0) {
		/*
		 * Import failed with the configuration found in the cachefile.
		 * This can happen in the case when the cachefile becomes stale.
		 * So import the pool normally and populate cachefile in CCR.
		 */
		scds_syslog_debug(DBG_LEVEL_HIGH, "The ZFS pool '%s' "
		    "failed to import with configuration in cachefile '%s'.",
		    poolname, cachefile_path);
		return (svc_importZpoolAndPopulateCCRCachefile(poolname,
		    force, overlay, mntoptions, altroot));
	}

	if ((zhp = hasp_zpool_open(poolname)) == NULL) {
		hasp_print_zfs_errmsg();
		rc = 1;
		goto error;
	}

	if ((*libzfs_zpool_mount_datasets)(zhp, mntoptions, flags) != 0) {
		hasp_print_zfs_errmsg();
		rc = 1;
		goto error;
	}

error:
	if (zhp) {
		(*libzfs_zpool_close)(zhp);
	}
	if (rc) {
		/*
		 * SCMSGS
		 * @explanation
		 * HAStoragePlus failed to import the specified ZFS pool.
		 * @user_action
		 * Check the error messages realted to specified pool in the log
		 * message to determine the failure. Re-try the operation after
		 * rectifying the problem. If the problem persists contact your
		 * authorized Sun service provider.
		 */
		hasp_scds_syslog_2(LOG_ERR, "The pool '%s' failed to "
		    "import using cachefile %s.",
		    poolname, cachefile_path);
		return (rc);
	}

	scds_syslog_debug(DBG_LEVEL_HIGH,
	    "Imported the pool %s successfully from cachefile contents of %s.",
	    poolname, cachefile_path);

	return (rc);
}

/*
 * Imports the list of ZFS pools using the cachefile contents stored in CCR
 * table for each pool.
 */
static int
svc_importZpoolListUsingCCRCachefile(const scha_str_array_t *zpoollist,
    boolean_t force, boolean_t overlay, const char *mntoptions,
    scha_str_array_t *faillist, const char *altroot)
{
	char		*poolname;
	uint_t	i;

	for (i = 0; i < zpoollist->array_cnt; i++) {

		poolname = zpoollist->str_array[i];

		if (svc_importZpoolWithCCRCachefile(poolname, force, overlay,
		    mntoptions, altroot)) {
			/*
			 * Import failed. Add the poolname to the faillist.
			 */
			(void) svc_addStr2Array(poolname, faillist);
			return (1);
		}
	}
	return (0);
}

/*
 * This is an entry call to import the list of ZFS pools.
 * It call the corresponding calls by checking the availablity of the ZFS
 * cachefile feature.
 */
int
svc_importZpoolList(const scha_str_array_t *zpoollist,
    boolean_t force, boolean_t overlay, const char *mntoptions,
    scha_str_array_t *faillist, const char *altroot)
{
	if ((zpoollist == NULL) || (zpoollist->array_cnt == 0)) {
		return (0);
	}

	if (is_zfs_cachefile_available) {
		return (svc_importZpoolListUsingCCRCachefile(zpoollist,
		    force, overlay, mntoptions, faillist, altroot));
	}
	return (svc_importZpoolListNormal(zpoollist, force,
	    overlay, mntoptions, faillist, altroot));
}

/*
 * Function: svc_exportZpool
 *
 * Arguments:
 *
 * poolname		The pool name to export.
 *
 * force		Flag to specify whether to import forcefully or not.
 *
 * Return:
 *
 * 0	If exported successfully.
 *
 * 1	If export fails.
 *
 * Comments:
 * This function umount the file systems and then export the pool.
 * NOTE: This function implements "zpool export [-f] <poolname>.
 */
int
svc_exportZpool(const char *poolname, boolean_t force)
{
	zpool_handle_t	 *zhp;

	if (is_zfs_cachefile_available) {

		/*
		 * The cachefile contents could have changed after
		 * HAStoragePlus imported the ZFS pool.
		 * Dump the latest cachefile contents to CCR table for
		 * future use.
		 */

		char		zcache_ccr_tab[MAXPATHLEN];
		char		cachefile_path[MAXPATHLEN];
		err_msg_t	err;

		get_pool_cachefile_path(poolname, cachefile_path, MAXPATHLEN);
		get_pool_ccr_tabname(poolname, zcache_ccr_tab, MAXPATHLEN);

		if (dump_zcache_to_ccr(poolname, cachefile_path,
		    gSvcInfo.zpoolsSearchDir, &err)) {
			/*
			 * Failure to update the cachefile contents to CCR is
			 * not a fatal, as the configuration can be found from
			 * the system.
			 * Log a warning message and proceed.
			 */
			hasp_scds_syslog_4(LOG_WARNING,
			    "Failed to update the cachefile contents in %s to "
			    "CCR table %s for pool %s : %s.",
			    cachefile_path, zcache_ccr_tab, poolname,
			    err.message);
		} else {
			scds_syslog_debug(DBG_LEVEL_HIGH,
			    "The cachefile contents in %s is updated "
			    "to CCR table %s for pool %s : %s.",
			    cachefile_path, zcache_ccr_tab, poolname,
			    err.message);
		}
	}

	if ((zhp = hasp_zpool_open_canfail(poolname)) == NULL) {
		hasp_print_zfs_errmsg();
		return (1);
	}

	if (libzfs_zpool_unmount_datasets == NULL) {
		if (svc_umountDatasets(zhp, force) != 0) {
			(*libzfs_zpool_close)(zhp);
			return (1);
		}
	} else {
		if ((*libzfs_zpool_unmount_datasets)(zhp, force) != 0) {
			hasp_print_zfs_errmsg();
			(*libzfs_zpool_close)(zhp);
			return (1);
		}
	}

	if ((*libzfs_zpool_export)(zhp) != 0) {
		hasp_print_zfs_errmsg();
		(*libzfs_zpool_close)(zhp);
		return (1);
	}

	(*libzfs_zpool_close)(zhp);
	return (0);
}

/*
 * Function: svc_mountshareDatasets
 *
 * Arguments:
 *
 * zhp			The zpool pool handle in which the data sets to be
 *			mounted.
 *
 * overlay		Flag to specify whether to import by mouting the
 *			file system by overlay mount.
 *
 * mntoptions		The mount options used to import.
 *
 * Return:
 *
 * 0	If mounting completes successfully.
 *
 * 1	If mounting fails.
 *
 * Comments:
 * Go through and mount all datasets within a pool.  We need to mount all
 * datasets in order, so that we mount parents before any children.  A complete
 * fix would gather all mountpoints, sort them, and mount them in lexical order.
 * There are many more problems if you start to have nested filesystems - we
 * just want to get inherited filesystems right.
 * NOTE: The function is copy of mount_datasets() of
 * cmd/zpool/zpool_dataset.c
 */
static int
svc_mountshareDatasets(zpool_handle_t *zhp, boolean_t overlay,
    const char *options)
{
	hasp_cbdata_t	hcb;
	zfs_handle_t	*zfsp;


	/*
	 * Initialize the call back information.
	 */
	hcb.result = 0;
	hcb.overlay = overlay;
	hcb.mntoptions = options;

	/* For unavailable pool, we don't do anything */
	if ((*libzfs_zpool_get_state)(zhp) == POOL_STATE_UNAVAIL)
		return (0);

	if ((zfsp = hasp_zfs_open((*libzfs_zpool_get_name)(zhp),
	    ZFS_TYPE_FILESYSTEM)) == NULL) {
		hasp_print_zfs_errmsg();
		return (1);
	}

	if (svc_mountshareDataset(zfsp, (void *)&hcb) != 0 || hcb.result != 0) {
		(*libzfs_zfs_close)(zfsp);
		return (1);
	}

	(*libzfs_zfs_close)(zfsp);
	return (0);
}

/*
 * Function: svc_mountshareDataset
 *
 * Arguments:
 *
 * zfsp		The zfs handle which has to be mounted.
 *
 * data		Contains call back information.
 *
 * Return:
 *
 * 0		After mount completes.
 *
 * Comments:
 * This is call back function of svc_mountshareDatasets(). It does mount a
 * single ZFS dataset/file system.
 * NOTE: The function is copy of do_mount() from cmd/zpool/zpool_dataset.c
 */
static int
svc_mountshareDataset(zfs_handle_t *zfsp, void *data)
{
	hasp_cbdata_t	*hcbp = (hasp_cbdata_t *)data;
	int 		ret;

	if ((*libzfs_zfs_get_type)(zfsp) != ZFS_TYPE_FILESYSTEM)
		return (0);

	if ((*libzfs_zfs_mount)(zfsp, hcbp->mntoptions,
	    hcbp->overlay ? MS_OVERLAY : 0) != 0) {
		hasp_print_zfs_errmsg();
		hcbp->result = 1;
	} else if ((*libzfs_zfs_share)(zfsp) != 0) {
		hasp_print_zfs_errmsg();
		hcbp->result = 1;
	}

	ret = (*libzfs_zfs_iter_children)(zfsp, svc_mountshareDataset, data);

	return (ret);
}

/*
 * Function: svc_umountDatasets
 *
 * Arguments:
 *
 * zhp			The zpool pool handle in which the data sets to be
 *			umounted.
 *
 * force		Flag to specify whether to umount forcefully or not.
 *
 * Return:
 *
 * 0	If unmount completes successfully.
 *
 * 1	If unmount fails.
 *
 * Comments:
 * This function unmount all datasets/file systems within the given pool.
 * NOTE: The function is copy of umount_datasets() from
 * cmd/zpool/zpool_dataset.c
 */
static int
svc_umountDatasets(zpool_handle_t *zhp, boolean_t force)
{
	hasp_cbdata_t	hcb;
	zfs_handle_t	*zfsp;

	/*
	 * Initialize the call back information.
	 */
	hcb.result = 0;
	hcb.force = force;

	/* For unavailable pool, we don't do anything */
	if ((*libzfs_zpool_get_state)(zhp) == POOL_STATE_UNAVAIL)
		return (0);

	if ((zfsp = hasp_zfs_open((*libzfs_zpool_get_name)(zhp),
	    ZFS_TYPE_FILESYSTEM)) == NULL) {
		hasp_print_zfs_errmsg();
		return (1);
	}

	if ((*libzfs_zfs_iter_dependents)(zfsp, svc_umountDataset,
	    (void *)&hcb) != 0 || hcb.result != 0) {
		(*libzfs_zfs_close)(zfsp);
		return (1);
	}

	if (svc_umountDataset(zfsp, (void *) &hcb) != 0 || hcb.result != 0) {
		(*libzfs_zfs_close)(zfsp);
		return (1);
	}

	(*libzfs_zfs_close)(zfsp);
	return (0);
}

/*
 * Function: svc_umountDataset
 *
 * Arguments:
 *
 * zfsp		The zfs handle which has to be umounted.
 *
 * data		Contains call back information.
 *
 * Return:
 *
 * 0		After umount completes.
 *
 * Comments:
 * This is call back function of svc_unmountDatasets(). It does unmount a
 * single ZFS dataset/file system.
 * NOTE: The function is copy of do_umount() from cmd/zpool/zpool_dataset.c
 */
static int
svc_umountDataset(zfs_handle_t *zfsp, void *data)
{
	hasp_cbdata_t	*hcbp = (hasp_cbdata_t *)data;

	if ((*libzfs_zfs_unmount)(zfsp, NULL,
	    hcbp->force ? MS_FORCE : 0) != 0) {
		hasp_print_zfs_errmsg();
		hcbp->result = 1;
	}
	return (0);
}

/*
 * Function: svc_initializeZFS
 *
 * Arguments: None
 *
 * Return:
 *
 * 0		After umount completes.
 *
 * Comments:
 * It does the work to make use of libzfs. It gets the addresses of all required
 * functions and registers the error handler.
 */
int
svc_initializeZFS()
{
	if (libzfs_loaded) {
		/*
		 * The libzfs library is already initialized/loaded.
		 */
		return (0);
	}

	/*
	 * Open the zfs library.
	 */
	if ((dl_handle = dlopen(ZFS_LIB_NAME, RTLD_NOW)) == NULL) {

		hasp_scds_syslog_0(LOG_ERR, "Zettabyte File System (ZFS) is "
		    "not available in this release.");
		return (1);
	}

#if	SOL_VERSION == __s10
	/*
	 * Find out the library version level.
	 * We are using libzfs_init() symbol to find whether we are using the
	 * initial version of libzfs library.
	 */
	if (dlsym(dl_handle, LIBZFS_INIT) == NULL) {
		libzfs_is_initial_version = B_TRUE;
	}
#endif

	/*
	 * Get the addresses of all functions being used.
	 *
	 * Some of the functions is the libzfs library were introduced
	 * additional argument(s) in libzfs.so.2 when compared to libzfs.so.1
	 * in Solaris 10
	 */
	/* lint has 'Suspicious cast' issues with dlsym function ptrs (e611) */
	/*lint -e611 */
#if	SOL_VERSION == __s10
	if (libzfs_is_initial_version) {
		if ((libzfs_zpool_open1 = (libzfs_zpool_open1_t)
		    dlsym(dl_handle, ZPOOL_OPEN)) == NULL ||
		    (libzfs_zpool_open_canfail1 = (libzfs_zpool_open_canfail1_t)
		    dlsym(dl_handle, ZPOOL_OPEN_CANFAIL)) == NULL ||
		    (libzfs_zpool_import3 = (libzfs_zpool_import3_t)
		    dlsym(dl_handle, ZPOOL_IMPORT)) == NULL ||
		    (libzfs_zpool_iter2 = (libzfs_zpool_iter2_t)
		    dlsym(dl_handle, ZPOOL_ITER)) == NULL ||
		    (libzfs_zpool_find_import2 = (libzfs_zpool_find_import2_t)
		    dlsym(dl_handle, ZPOOL_FIND_IMPORT)) == NULL ||
		    (libzfs_zfs_open2 = (libzfs_zfs_open2_t)
		    dlsym(dl_handle, ZFS_OPEN)) == NULL ||
		    (libzfs_zfs_set_error_handler =
		    (libzfs_zfs_set_error_handler_t)
		    dlsym(dl_handle, ZFS_SET_ERROR_HANDLER)) == NULL) {
			/*
			 * SCMSGS
			 * @explanation
			 * HAStoragePlus failed to initialize the libzfs
			 * library.
			 * @user_action
			 * Contact your authorized Sun service provider
			 * to determine whether a workaround or patch is
			 * available.
			 */
			hasp_scds_syslog_0(LOG_ERR,
			    "Failed to initialize ZFS library");
			return (1);
		}
	} else
#endif
	{
		if ((libzfs_zpool_open2 = (libzfs_zpool_open2_t)
		    dlsym(dl_handle, ZPOOL_OPEN)) == NULL ||
		    (libzfs_zpool_open_canfail2 = (libzfs_zpool_open_canfail2_t)
		    dlsym(dl_handle, ZPOOL_OPEN_CANFAIL)) == NULL ||
		    (libzfs_zpool_import4 = (libzfs_zpool_import4_t)
		    dlsym(dl_handle, ZPOOL_IMPORT)) == NULL ||
		    (libzfs_zpool_iter3 = (libzfs_zpool_iter3_t)
		    dlsym(dl_handle, ZPOOL_ITER)) == NULL ||
		    (libzfs_zpool_find_import4 = (libzfs_zpool_find_import4_t)
		    dlsym(dl_handle, ZPOOL_FIND_IMPORT)) == NULL ||
		    (libzfs_zfs_open3 = (libzfs_zfs_open3_t)
		    dlsym(dl_handle, ZFS_OPEN)) == NULL ||
		    (libzfs_libzfs_init = (libzfs_libzfs_init_t)
		    dlsym(dl_handle, LIBZFS_INIT)) == NULL ||
		    (libzfs_libzfs_fini = (libzfs_libzfs_fini_t)
		    dlsym(dl_handle, LIBZFS_FINI)) == NULL ||
		    (libzfs_libzfs_errno = (libzfs_libzfs_errno_t)
		    dlsym(dl_handle, LIBZFS_ERRNO)) == NULL ||
		    (libzfs_libzfs_error_action = (libzfs_libzfs_error_action_t)
		    dlsym(dl_handle, LIBZFS_ERROR_ACTION)) == NULL ||
		    (libzfs_libzfs_error_description =
		    (libzfs_libzfs_error_description_t)
		    dlsym(dl_handle, LIBZFS_ERROR_DESCRIPTION)) == NULL) {
			hasp_scds_syslog_0(LOG_ERR,
			    "Failed to initialize ZFS library");
			return (1);
		}
	}
	if ((libzfs_zpool_close = (libzfs_zpool_close_t)
	    dlsym(dl_handle, ZPOOL_CLOSE)) == NULL ||
	    (libzfs_zpool_export = (libzfs_zpool_export_t)
	    dlsym(dl_handle, ZPOOL_EXPORT)) == NULL ||
	    (libzfs_zpool_get_name = (libzfs_zpool_get_name_t)
	    dlsym(dl_handle, ZPOOL_GET_NAME)) == NULL ||
	    (libzfs_zpool_get_state = (libzfs_zpool_get_state_t)
	    dlsym(dl_handle, ZPOOL_GET_STATE)) == NULL ||
	    (libzfs_zfs_close = (libzfs_zfs_close_t)
	    dlsym(dl_handle, ZFS_CLOSE)) == NULL ||
	    (libzfs_zfs_get_type = (libzfs_zfs_get_type_t)
	    dlsym(dl_handle, ZFS_GET_TYPE)) == NULL ||
	    (libzfs_zfs_prop_get = (libzfs_zfs_prop_get_t)
	    dlsym(dl_handle, ZFS_PROP_GET)) == NULL ||
	    (libzfs_zfs_iter_children = (libzfs_zfs_iter_children_t)
	    dlsym(dl_handle, ZFS_ITER_CHILDREN)) == NULL ||
	    (libzfs_zfs_iter_dependents = (libzfs_zfs_iter_dependents_t)
	    dlsym(dl_handle, ZFS_ITER_DEPENDENTS)) == NULL ||
	    (libzfs_zfs_mount = (libzfs_zfs_mount_t)
	    dlsym(dl_handle, ZFS_MOUNT)) == NULL ||
	    (libzfs_zfs_unmount = (libzfs_zfs_unmount_t)
	    dlsym(dl_handle, ZFS_UNMOUNT)) == NULL ||
	    (libzfs_zfs_share = (libzfs_zfs_share_t)
	    dlsym(dl_handle, ZFS_SHARE)) == NULL) {
		hasp_scds_syslog_0(LOG_ERR, "Failed to initialize ZFS library");
		return (1);
	}

	/*
	 * Obtain the fuction symbols which used to mount/umount all the
	 * datasets in a zpool.
	 */

	libzfs_zpool_mount_datasets = (libzfs_zpool_mount_datasets_t)
	    dlsym(dl_handle, ZPOOL_MOUNT_DATASETS);
	libzfs_zpool_unmount_datasets = (libzfs_zpool_unmount_datasets_t)
	    dlsym(dl_handle, ZPOOL_UNMOUNT_DATASETS);

	/*
	 * Obtain the fuction symbols used to import the pool using cachefile.
	 */
	libzfs_zpool_find_import_cached = (libzfs_zpool_find_import_cached_t)
	    dlsym(dl_handle, ZPOOL_FIND_IMPORT_CACHED);
	libzfs_zpool_import_props = (libzfs_zpool_import_props_t)
	    dlsym(dl_handle, ZPOOL_IMPORT_PROPS);
	libzfs_zpool_set_prop = (libzfs_zpool_set_prop_t)
	    dlsym(dl_handle, ZPOOL_SET_PROP);

	if (libzfs_zpool_find_import_cached != NULL) {
		is_zfs_cachefile_available = B_TRUE;
	}

	/*
	 * Register the libzfs cleanup handler to atexit.
	 */
	(void) atexit(libzfs_cleanup_handler);

#if	SOL_VERSION == __s10
	/*
	 * Register the error handler with libzfs.
	 * NOTE:
	 * In the initial version of libzfs, an error handler can be registered.
	 */
	if (libzfs_is_initial_version) {
		(*libzfs_zfs_set_error_handler)(hasp_scds_log);
	}

	/*
	 * Initialize the libzfs library.
	 */
	if (!libzfs_is_initial_version)
#endif
	{
		if ((g_zfs = (*libzfs_libzfs_init)()) == NULL) {
			hasp_scds_syslog_0(LOG_ERR,
			    "Failed to initialize ZFS library");
			return (1);
		}
	}
	libzfs_loaded = B_TRUE;
	return (0);
}

/*
 * Function: libzfs_cleanup_handler
 *
 * Arguments: None
 *
 * Return: void
 *
 * Comments:
 * It cleanup the libzfs object by closing libzfs shared object.
 */
void
libzfs_cleanup_handler()
{

#if	SOL_VERSION == __s10
	if (!libzfs_is_initial_version)
#endif
		(*libzfs_libzfs_fini)(g_zfs);

	if (dl_handle) {
		(void) dlclose(dl_handle);
	}
	nvlist_free(import_pools_list);
}
#endif
