/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * hastorageplus_postnet_stop.c
 *
 * Postnet stop method implementation
 * for the HAStoragePlus resource type.
 *
 */

#pragma ident	"@(#)hastorageplus_postnet_stop.c	1.13	08/06/04 SMI"

#include <stdlib.h>

#include "hastorageplus.h"

/*
 *
 * Function: main
 *
 * Arguments:
 *
 * argc		Number of arguments
 *
 * argv		Argument list from command line passed by RGM
 *
 * Return:
 *
 * 0	if completed successfully i.e  validation on the configuration
 *	and unmount the file systems which are part of resource.
 *
 * 1	if an error occurred.
 *
 * Comments:
 *
 * A HAStorage resource can belong to a resource group which is either
 * failover ( primaried on only one node ) or scalable ( primaried on
 * more than one node ). Accordingly, the postnet stop method is invoked on
 * one or more nodes.
 *
 * The RGM invokes a resource's postnet stop method after bringing the
 * the network addresses down.
 *
 */

int
main(int argc, char *argv[])
{
	scds_handle_t		ds_handle;
	scha_extprop_value_t	*gdp = NULL,
				*fmp = NULL,
				*zpools = NULL;
	int			rc = 0;

	/*
	 * Initialize the DSDL subsystem (which processes the command line
	 * arguments passed by RGM and also initializes logging).
	 */

	if (scds_initialize(&ds_handle, argc, argv) != SCHA_ERR_NOERR) {

		hasp_scds_syslog_0(LOG_ERR, "Failed to initialize the DSDL.");

		return (1);
	}

	/*
	 * Fill the resource properties.
	 */
	if (svc_fill_properties(ds_handle)) {
		/*
		 * Error message logged.
		 */
		return (1);
	}

	gdp = gSvcInfo.gdp_prop_value;
	fmp = gSvcInfo.fmp_prop_value;
	zpools = gSvcInfo.zpool_prop_value;

#ifdef	ZFS_AVAILABLE
	if (is_zpools_property_exists()) {
		/*
		 * Check for zpools existence and initialize ZFS if required.
		 */
		if (zpools->val.val_strarray->array_cnt > 0) {
			if (svc_initializeZFS()) {
				rc = 1;
				goto end;
			}
		}

		/*
		 * Export the zpools configured.
		 */
		if (svc_exportZpools(zpools->val.val_strarray)) {
			/*
			 * Error messages for export failures are already
			 * logged.
			 */
			rc = 1;
			goto end;
		}
	}
#endif

	/*
	 * Check if GlobalDevicePaths, FilesystemMountPoints and Zpools are
	 * null.
	 * If they are null, log a warning message and return.
	 */
	if (fmp->val.val_strarray->array_cnt == 0 &&
	    gdp->val.val_strarray->array_cnt == 0) {

		if (!is_zpools_property_exists()) {

			hasp_scds_syslog_2(LOG_WARNING,
			    "Extension properties %s and %s are empty.",
			    FILESYSTEMMOUNTPOINTS, GLOBALDEVICEPATHS);

			goto end;

		} else if (zpools->val.val_strarray->array_cnt == 0) {

			hasp_scds_syslog_3(LOG_WARNING,
			    "Extension properties %s and %s and %s are empty.",
			    FILESYSTEMMOUNTPOINTS, GLOBALDEVICEPATHS, ZPOOLS);

			goto end;
		}

	}

	if (fmp->val.val_strarray->array_cnt == 0) {
		goto end;
	}

	/*
	 * Unmount the filesystems.
	 */
	if (svc_umountFMPEntries(fmp->val.val_strarray)) {
		/*
		 * Error message of filesystems which unmount failed are already
		 * logged.
		 */
		return (1);
	}

	/*
	 * SCMSGS
	 * @explanation
	 * Self explanatory.
	 * @user_action
	 * This is an informational message, no user action is needed.
	 */
	hasp_scds_syslog_0(LOG_INFO,
	    "Unmounting the file systems is completed successfully.");

end:
	scds_close(&ds_handle);

	return (rc);
}
