/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

/*	Copyright (c) 1984, 1986, 1987, 1988, 1989 AT&T */
/*	  All Rights Reserved   */


/*
 * hastorageplus_common.c
 *
 * Common functions invoked by various HAStoragePlus method implementations.
 *
 *
 */
#pragma ident	"@(#)hastorageplus_common.c	1.80	09/04/02 SMI"

#ifndef	_REENTRANT
#define	_REENTRANT
#endif

#include <errno.h>
#include <thread.h>
#include <synch.h>
#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <sys/debug.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/vfstab.h>
#include <sys/mnttab.h>
#include <sys/wait.h>
#include <unistd.h>
#include <limits.h>
#include <stdarg.h>
#include <dirent.h>
#include <fcntl.h>
#include <sys/statvfs.h>

#include "hastorageplus.h"
#include "hastorageplus_threads.h"

#ifdef ZONE_SUPPORT
#include <stdbool.h>
#include <libzccfg/libzccfg.h>
#include <zone.h>
#include <sys/cladm_int.h>
#include <sys/clconf_int.h>
#include <libzonecfg.h>
#include <netdb.h>
#include <fnmatch.h>
#include <libcontract.h>
#include <sys/contract/process.h>
#include <sys/ctfs.h>
#include <sys/vc_int.h>
#endif

/*
 * Private defines
 */

#define	SvcMountOptGlobal	0	/* Global mount option */
#define	SvcMountOptLocal	1	/* Local mount option  */
#define	SvcMountLofs		2	/* Local lofs mount */
#define	SvcMountOptError	3

#define	SvcReadSize		1024	/* Size for reading samfs config */

#define	SvcSamfsExec		"/opt/SUNWsamfs/sbin/HAStoragePlus_samfs"

#define	SvcDevGlbHdr		"/dev/global/"	/* /dev/global hdr substring */
#define	SvcDevMdHdr		"/dev/md/"	/* SVM device  hdr substring */

#if	SOL_VERSION >= __s10
#define	SVC_DEV_PREFIX		"/dev/"
#endif

#define	SUNW_QFS	"SUNW.qfs"
#define	QFSFILESYSTEM	"QFSFileSystem"

#define	SVC_BUFFER_LEN		1024

/*
 * Private functions declarations
 */
static int svc_fillAndValidateList(void);

static int svc_base_fillAndValidateList(void);

#if	SOL_VERSION >= __s10
static int svc_zc_fillAndValidateList(void);

static int svc_zc_validateFMPEntries(void);

static int svc_zc_validateGDPEntries(void);

static int svc_zc_checkDeviceMapping(const char *, boolean_t *, char *);

static int svc_zc_checkFSMapping(const char *, boolean_t *, struct zc_fstab *);

static int svc_zc_validateAndCopyMappedFS(struct vfstab *, struct zc_fstab *);

static boolean_t svc_zc_isAllZpoolsMapped(const scha_extprop_value_t *zpools);
#endif

static int svc_validateDeviceServices(void);

static int svc_validateNodeDeviceReplica(const char *, const char *,
    const dc_replica_seq_t *ds_replicas);

static int svc_validateVfsTabEntry(const struct vfstab *, struct vfstab *);

static void svc_reportVfsError(int, char *);

static void svc_reportMntError(int, char *);

static int svc_fillAndValidateZpools(const scha_extprop_value_t *);

static int svc_fillRtVersion(scds_handle_t);

static int svc_getGDPSvcName(const char *, char **);

static int svc_fillSAMFSSvcName(const char *, HAStoragePlusSvc *);

static int svc_getDCSPathServiceName(const char *, char **);

static scha_err_t svc_findOurMDSrs(char**, const char *);

static scha_err_t svc_checkMDSrt(char **, const char *, const char *);

static scha_err_t svc_checkMDSrs(const char *, const char *, int *);

static scha_err_t svc_checkRSdependency(const scds_handle_t *,
	const char *);

#ifdef	ZFS_AVAILABLE
static int svc_zpoolsCrossCheck(const scha_extprop_value_t *);

static int svc_CheckZpoolsInThisResourceType(const scha_extprop_value_t *,
						const char *);
#endif

static int svc_fillNodeId(void);

static int svc_evalVfsTabMountOption(const struct vfstab *, char *);

static int svc_evalMntTabMountOption(const struct mnttab *, char *);

static int svc_fillAndCheckFSCKCmd(const char *);

static int svc_SAMFSVols(const char *, char **);

static int svc_checkFSCKCmdPath(void);

static int svc_evalFSCKWithFS(const struct vfstab *vp,
				const HAStoragePlusSvc *dsvc);

static int svc_verifyAffinityOn(void);

static int svc_verifyAffinityOff(void);

static int svc_parseDeviceList(const char *val, scha_str_array_t *sa);

static boolean_t svc_isDCSDevicePath(const char *path);

static int svc_fillNodeIdArray(scds_handle_t ds_handle);

static int svc_fillFailbackSetting(int, char *const []);

static int svc_fillZoneSetting(scds_handle_t ds_handle);

static int svc_fillMethodExecutionNode(scds_handle_t);

static int svc_fillClusterName(scds_handle_t);

char *svc_getFullLocalPath(const char *zoneroot, char *localpath);

static uint_t svc_getpathdepth(char *path);

static int svc_setrpath(fsentry_t *fsp);

static int svc_mntcompar(const void *a, const void *b);

static int svc_umntcompar(const void *a, const void *b);

static int svc_populateMountPoints(fsentry_t *);

static int svc_isFSMounted(FILE *, fsentry_t *, boolean_t *);

static int svc_doParFsck(fsentry_t *const *fsck_list, unsigned int nentries);

static int svc_doParMount(fsentry_t **mount_list, unsigned int nentries);

static int svc_executeCmdusingPopen(char *cmd, char **cmd_output);

static int svc_executeCmdusingFork(char *cmd, char **cmd_output);

void svc_doMount(void *);

static int svc_gz_doMount(const fsentry_t *);

#if	SOL_VERSION >= __s10
static int svc_lz_doMount(const fsentry_t *);

static int svc_zc_doMount(const fsentry_t *);
#endif

static int svc_createMountPoint(const char *);

static int svc_doParUmount(char *umnt_pts, int numlofs);

void svc_doFsck(void *arg);

static int svc_sprintf(char **, const char *, ...);

static void svc_enableQuota(const fsentry_t *);

static void svc_enableUFSQuota(const struct vfstab *vp);

static int svc_confirmFSMountbyStatvfs(const fsentry_t *);

#ifdef	ZONE_SUPPORT
static int svc_initContractTemplate(void);
#endif

/*
 *
 * Function: svc_fill_properties
 *
 * Arguments:
 *
 * ds_handle	dsdl handle
 *
 * argc 	Number of arguments
 *
 * argv		Command line argument list
 *
 * Return:
 *
 * 0	if properties filled successfully.
 *
 * 1	an error occurred during filling properties.
 *
 *
 * Comments:
 *
 * This function retrieves the properties of the resource and fills in
 * global HAStorageplus gSvcinfo structure which will be used by call back
 * methods later.
 * NOTE: The extension properties returned from scds API are stored directly
 * without copying the values for efficiency reasons. The properties can be
 * freed explicitly by calling svc_free_properties().
 */
int
svc_fill_properties(scds_handle_t ds_handle)
{

	scha_err_t ret;

	/*
	 * Initialize the DCS sub system.
	 */
	if (dcs_initialize() != 0) {

		/*
		 * SCMSGS
		 * @explanation
		 * HAStoragePlus was not able to connect to the DCS.
		 * @user_action
		 * Contact your authorized Sun service provider to determine
		 * whether a workaround or patch is available.
		 */
		hasp_scds_syslog_0(LOG_ERR, "Failed to initialize the DCS.");

		return (1);

	}

	/*
	 * Obtain the resource name.
	 */
	gSvcInfo.rsName = scds_get_resource_name(ds_handle);


	if (gSvcInfo.rsName == NULL) {
		/*
		 * SCMSGS
		 * @explanation
		 * HAStoragePlus was not able to retrieve the resource name
		 * from the CCR.
		 * @user_action
		 * Check that the cluster configuration. If the problem
		 * persists, contact your authorized Sun service provider.
		 */
		hasp_scds_syslog_0(LOG_ERR,
		    "Failed to retrieve resource name.");

		return (1);
	}

	scds_syslog_debug(DBG_LEVEL_HIGH,
	    "Resource name is %s.", gSvcInfo.rsName);

	/*
	 * Obtain the resource group name.
	 */
	gSvcInfo.rgName = scds_get_resource_group_name(ds_handle);

	if (gSvcInfo.rgName == NULL) {
		/*
		 * SCMSGS
		 * @explanation
		 * HAStoragePlus was not able to retrieve the resource group
		 * name to which it belongs from the CCR.
		 * @user_action
		 * Check the cluster configuration. If the problem persists,
		 * contact your authorized Sun service provider.
		 */
		hasp_scds_syslog_0(LOG_ERR,
		    "Failed to retrieve resource group name.");

		return (1);
	}

	scds_syslog_debug(DBG_LEVEL_HIGH, "Resource group name is %s.",
	    gSvcInfo.rgName);


	/*
	 * Obtain the resource group mode.
	 */

	gSvcInfo.rgMode = scds_get_rg_rg_mode(ds_handle);

	scds_syslog_debug(DBG_LEVEL_HIGH, "Resource group mode is %d.",
	    gSvcInfo.rgMode);

	/*
	 * Retrieve the AffinityOn extension property
	 * information.
	 */
	ret = scds_get_ext_property(ds_handle,
	AFFINITYON, SCHA_PTYPE_BOOLEAN, &gSvcInfo.affinity_prop_value);


	if (ret != SCHA_ERR_NOERR || gSvcInfo.affinity_prop_value == NULL) {
		/*
		 * SCMSGS
		 * @explanation
		 * HAStoragePlus was not able to retrieve the resource
		 * property from the CCR.
		 * @user_action
		 * Check the cluster configuration. If the problem persists,
		 * contact your authorized Sun service provider.
		 */
		hasp_scds_syslog_2(LOG_ERR,
		    "Failed to retrieve property %s: %s.",  /* CSTYLED */
		    AFFINITYON, scds_error_string(ret));    /*lint !e666 */

		return (1);
	}

	scds_syslog_debug(DBG_LEVEL_HIGH, "Affinity On value is %d.",
	    gSvcInfo.affinity_prop_value->val.val_boolean);

	/*
	 *  Store the AffinityOn value in 'gSvcInfo'.
	 */

	gSvcInfo.affinityOn = gSvcInfo.affinity_prop_value->val.val_boolean;

	/*
	 * Retrieve the GlobalDevicePaths extension property
	 * information.
	 */
	ret = scds_get_ext_property(ds_handle, GLOBALDEVICEPATHS,
	    SCHA_PTYPE_STRINGARRAY, &gSvcInfo.gdp_prop_value);


	if (ret != SCHA_ERR_NOERR) {
		hasp_scds_syslog_2(LOG_ERR,
		    "Failed to retrieve property %s: %s.",
		    GLOBALDEVICEPATHS,		/* CSTYLED */
		    scds_error_string(ret));	/*lint !e666 */

		return (1);
	}

	scds_syslog_debug(DBG_LEVEL_HIGH, "Number of global devices paths: %d.",
	    gSvcInfo.gdp_prop_value->val.val_strarray->array_cnt);

	/*
	 * Retrieve the FilesystemMountPoints extension
	 * property information.
	 */
	ret = scds_get_ext_property(ds_handle, FILESYSTEMMOUNTPOINTS,
	    SCHA_PTYPE_STRINGARRAY, &gSvcInfo.fmp_prop_value);

	if (ret != SCHA_ERR_NOERR) {
		hasp_scds_syslog_2(LOG_ERR,
		    "Failed to retrieve property %s: %s.",
		    FILESYSTEMMOUNTPOINTS,	/* CSTYLED */
		    scds_error_string(ret));	/*lint !e666 */

		return (1);
	}

	scds_syslog_debug(DBG_LEVEL_HIGH,
	    "Number of file system mount points: %d.",
	    gSvcInfo.fmp_prop_value->val.val_strarray->array_cnt);

	/*
	 * Retrieve the FilesystemCheckCommand extension
	 * property information.
	 */
	ret = scds_get_ext_property(ds_handle, FILESYSTEMCHECKCOMMAND,
	    SCHA_PTYPE_STRINGARRAY, &gSvcInfo.fsck_string);

	if (ret != SCHA_ERR_NOERR) {

		hasp_scds_syslog_2(LOG_ERR,
		    "Failed to retrieve property %s: %s.",
		    FILESYSTEMCHECKCOMMAND,	/* CSTYLED */
		    scds_error_string(ret));	/*lint !e666 */

		return (1);
	}

	/*
	 * Fill the resource RT version. This is used to determine the existence
	 * of new properties and new feature support.
	 */
	if (svc_fillRtVersion(ds_handle)) {
		/*
		 * Error message is already logged.
		 */
		return (1);
	}

	/* store ds_handle in gSvcInfo.ds_handle */
	gSvcInfo.ds_handle = &ds_handle;

	if (is_zpools_property_exists()) {

		/*
		 * Retrieve the Zpools extension property information.
		 */
		ret = scds_get_ext_property(ds_handle, ZPOOLS,
		    SCHA_PTYPE_STRINGARRAY, &gSvcInfo.zpool_prop_value);

		if (ret != SCHA_ERR_NOERR) {
			hasp_scds_syslog_2(LOG_ERR,
			    "Failed to retrieve property %s: %s.",
			    ZPOOLS,			/* CSTYLED */
			    scds_error_string(ret));	/*lint !e666 */

			return (1);
		}

		scds_syslog_debug(DBG_LEVEL_HIGH,
		    "Number of zpools configured: %d.",
		    gSvcInfo.zpool_prop_value->val.val_strarray->array_cnt);
	}

	if (is_zpoolssearchdir_property_exists()) {
		/*
		 * Retrieve the ZpoolsSearchDir property information.
		 */
		ret = scds_get_ext_property(ds_handle,
		    ZPOOLSSEARCHDIR, SCHA_PTYPE_STRING,
		    &gSvcInfo.zpools_dir_value);
		if (ret != SCHA_ERR_NOERR) {
			hasp_scds_syslog_2(LOG_ERR,
			    "Failed to retrieve property %s: %s.",
			    ZPOOLSSEARCHDIR,		/* CSTYLED */
			    scds_error_string(ret));	/*lint !e666 */
			return (1);
		}

		gSvcInfo.zpoolsSearchDir =
		    gSvcInfo.zpools_dir_value->val.val_str;

		scds_syslog_debug(DBG_LEVEL_HIGH,
		    "The zpool search directory location is: %s.",
		    (gSvcInfo.zpoolsSearchDir == NULL) ? "NULL":
		    gSvcInfo.zpoolsSearchDir);

	}

	/*
	 * Fill the zone setting which includes zone name, zone root,
	 * node on which resource is execution, cluster name for which rs
	 * is configured.
	 */
	if (svc_fillZoneSetting(ds_handle)) {
		/*
		 * Error message already logged.
		 */
		return (1);
	}

	return (0);
}

/*
 * Function: svc_free_properties
 *
 * Arguments:	void
 *
 * Return:	void
 */
void
svc_free_properties(void)
{
	if (gSvcInfo.affinity_prop_value)
		scds_free_ext_property(gSvcInfo.affinity_prop_value);
	if (gSvcInfo.gdp_prop_value)
		scds_free_ext_property(gSvcInfo.gdp_prop_value);
	if (gSvcInfo.fmp_prop_value)
		scds_free_ext_property(gSvcInfo.fmp_prop_value);
	if (gSvcInfo.fsck_string)
		scds_free_ext_property(gSvcInfo.fsck_string);
	if (gSvcInfo.zpool_prop_value)
		scds_free_ext_property(gSvcInfo.zpool_prop_value);
	if (gSvcInfo.zpools_dir_value)
		scds_free_ext_property(gSvcInfo.zpools_dir_value);
}

/*
 *
 * Function: svc_validate
 *
 * Arguments:
 *
 * ds_handle	dsdl handle
 *
 * argc 	Number of arguments
 *
 * argv		Command line argument list
 *
 * Return:
 *
 * 0	if all validations completed successfully.
 *
 * 1	an error occurred during validation.
 *
 * Comments:
 *
 * Individual steps explained below. svc_validate() is called during
 * (a) resource validation
 * (b) RG online/switchovers
 * (c) Update of resource properties.
 * It also fill some information which will be used by callbacks after
 * validation.
 */
int
svc_validate(scds_handle_t ds_handle, int argc, char *const argv[])
{

	/*
	 * Return failure if the RG mode is neither failover nor scalable.
	 */

	if (gSvcInfo.rgMode != RGMODE_FAILOVER &&
	    gSvcInfo.rgMode != RGMODE_SCALABLE) {
		/*
		 * SCMSGS
		 * @explanation
		 * HAStoragePlus was not able to retrieve the resource group
		 * mode to which it belongs from the CCR.
		 * @user_action
		 * Check the cluster configuration. If the problem persists,
		 * contact your authorized Sun service provider.
		 */
		hasp_scds_syslog_0(LOG_ERR,
		    "Failed to retrieve resource group mode.");

		return (1);
	}

#ifdef ZONE_SUPPORT
	/*
	 * The resource at RT version where there is no zones support
	 * should not be allowed to configure in local zones.
	 */
	if ((!is_zones_supported()) && (getzoneid() != GLOBAL_ZONEID)) {
		/*
		 * SCMSGS
		 * @explanation
		 * The specific resource is at resourcetype version where
		 * there is no zones support.
		 * @user_action
		 * Upgrade the resource to latest resourcetype version which
		 * has zones support.
		 */
		hasp_scds_syslog_2(LOG_ERR,
		    "The resource '%s' is at resourcetype version '%d' that "
		    "does not support zones.",
		    gSvcInfo.rsName, gSvcInfo.rs_rt_version);
		return (1);
	}
#endif

	/*
	 * Log warning message if the extension properties are empty.
	 */
	if (gSvcInfo.fmp_prop_value->val.val_strarray->array_cnt == 0 &&
	    gSvcInfo.gdp_prop_value->val.val_strarray->array_cnt == 0) {

		if (!is_zpools_property_exists()) {

			/*
			 * SCMSGS
			 * @explanation
			 * HAStoragePlus detected that no devices or
			 * file systems are to be managed.
			 * @user_action
			 * This is an informational message, no user action is
			 * needed.
			 */
			hasp_scds_syslog_2(LOG_WARNING,
			    "Extension properties %s and %s are empty.",
			    FILESYSTEMMOUNTPOINTS, GLOBALDEVICEPATHS);

		} else if (gSvcInfo.zpool_prop_value
		    ->val.val_strarray->array_cnt == 0) {

			/*
			 * SCMSGS
			 * @explanation
			 * HAStoragePlus detected that no devices,
			 * file systems or zpools are to be managed.
			 * @user_action
			 * This is an informational message, no user action is
			 * needed.
			 */
			hasp_scds_syslog_3(LOG_WARNING,
			    "Extension properties %s and %s and %s are empty.",
			    FILESYSTEMMOUNTPOINTS, GLOBALDEVICEPATHS, ZPOOLS);
		}

	}

	/*
	 * Log warning message if AffinityOn is TRUE for scalable RG.
	 */
	if (gSvcInfo.rgMode == RGMODE_SCALABLE &&
	    gSvcInfo.affinityOn == B_TRUE) {

		/*
		 * SCMSGS
		 * @explanation
		 * Self explanatory.
		 * @user_action
		 * This is an informational message, no user action is needed.
		 */
		hasp_scds_syslog_2(LOG_WARNING,
		    "Resource %s is associated with scalable resource group"
		    " %s. AffinityOn set to TRUE will be ignored.",
		    gSvcInfo.rsName, gSvcInfo.rgName);
	}

	/*
	 * Set gSvcInfo.count to the total number
	 * of devices services. Device services are explicitly
	 * specified with GlobalDevicePaths, and implicitly with
	 * FilesystemMountPoints.
	 */
	gSvcInfo.fmpCount =
	    gSvcInfo.fmp_prop_value->val.val_strarray->array_cnt;
	gSvcInfo.gdpCount =
	    gSvcInfo.gdp_prop_value->val.val_strarray->array_cnt;
	gSvcInfo.count = gSvcInfo.fmp_prop_value->val.val_strarray->array_cnt +
	    gSvcInfo.gdp_prop_value->val.val_strarray->array_cnt;

	/*
	 * Fill (set) gSvcInfo.nodeId to contain the RGM node id. Concurrent
	 * execution on multiple nodes will obviously result in different
	 * node ids being stored.
	 */

	if (svc_fillNodeId()) {
		/*
		 * Error message already logged.
		 */
		return (1);
	}


	/*
	 * Fill the failback value of the resource group in gSvcInfo.failback.
	 */
	if (svc_fillFailbackSetting(argc, argv)) {
		/*
		 * Error message already logged.
		 */
		return (1);
	}

	/*
	 * Fill the NodeId list of resource group in gSvcInfo.rgNodeIdArray.
	 */
	if (svc_fillNodeIdArray(ds_handle)) {
		/*
		 * Error message already logged.
		 */
		return (1);
	}

	/*
	 * Validate the file system check command string.
	 */
	if (gSvcInfo.fsck_string->val.val_strarray->array_cnt == 0) {

		/*
		 * User did not specify any value i.e.
		 * default RTR value holds.
		 */
		if (svc_fillAndCheckFSCKCmd(SvcNullString))
			return (1);

	} else {

		/*
		 * Call function with user specified ext. property value.
		 */
		if (svc_fillAndCheckFSCKCmd(gSvcInfo.fsck_string->val.
			val_strarray->str_array[0]))
			return (1);

	}

	/*
	 * Fill and Validate the global device paths and file system mountpoint
	 * properties.
	 */
	if (svc_fillAndValidateList()) {
		/*
		 * Error message already logged.
		 */
		return (1);
	}

	if (is_zpools_property_exists()) {
		/*
		 * Fill zpool information in gSvcInfo and validate each zpool.
		 */
		if (svc_fillAndValidateZpools(gSvcInfo.zpool_prop_value)) {
			/*
			 * Error message already logged.
			 */
			return (1);
		}
	}

	/*
	 * Signifies successful completion of svc_validate().
	 */
	return (0);
}

/*
 * Function: svc_fillRtVersion
 *
 * Arguments:
 *
 * ds_handle 	dsdl handle
 *
 * rt_version	location to store the resourcetype version.
 *
 * Return: int
 *
 * 0  fills the resourcetype version of resource
 *
 * 1  if an error occurred.
 *
 * Comments:
 * This function fills the current HAStoragePlus RT version.
 *
 */
static int
svc_fillRtVersion(scds_handle_t ds_handle)
{
	const char	*rs_rt_version = NULL;
	scha_resource_t	rs_handle = NULL;
	scha_err_t	err = SCHA_ERR_NOERR;

	/*
	 * Obtain the resource RT version.
	 */
	err = scha_resource_open_zone(gSvcInfo.cl_name, gSvcInfo.rsName,
	    gSvcInfo.rgName, &rs_handle);

	if (err != SCHA_ERR_NOERR) {

		if (err == SCHA_ERR_RSRC) {
			/*
			 * The resource does not exist. This can be case
			 * when the resource is being created. So get the
			 * current version of resource from scds API.
			 */
			rs_rt_version = scds_get_rt_rt_version(ds_handle);
		} else {
			/*
			 * SCMSGS
			 * @explanation
			 * HAStoragePlus failed to access the resource
			 * information.
			 * @user_action
			 * Check the cluster configuration. If the problem
			 * persists, contact your authorized Sun service
			 * provider.
			 */
			hasp_scds_syslog_2(LOG_ERR,
			    "Failed to open the resource %s handle: %s.",
							/* CSTYLED */
			    gSvcInfo.rsName,
			    scds_error_string(err));	/*lint !e666 */
			return (1);
		}
	} else {

		err = scha_resource_get_zone(gSvcInfo.cl_name, rs_handle,
		    SCHA_RT_VERSION, &rs_rt_version);
		if (err != SCHA_ERR_NOERR) {
			hasp_scds_syslog_2(LOG_ERR,
			    "Failed to retrieve the resource property %s: %s.",
							/* CSTYLED */
			    SCHA_RT_VERSION,
			    scds_error_string(err));	/*lint !e666 */
			(void) scha_resource_close(rs_handle);
			return (1);
		}
	}

	(void) scha_resource_close(rs_handle);

	if (rs_rt_version == NULL) {
		/*
		 * SCMSGS
		 * @explanation
		 * The HAStoragePlus resource type version of the specified
		 * resource was found to be NULL.
		 * @user_action
		 * Check the cluster configuration. If the problem persists,
		 * contact your authorized Sun service provider.
		 */
		hasp_scds_syslog_1(LOG_ERR, "The resource type version of "
		    "resource %s is NULL.", gSvcInfo.rsName);
		return (1);
	}

	gSvcInfo.rs_rt_version = atoi(rs_rt_version);

	scds_syslog_debug(DBG_LEVEL_HIGH, "The RT version of the resource %s "
	    " is : %d", gSvcInfo.rsName, gSvcInfo.rs_rt_version);

	return (0);
}

/*
 * Function to determine the support of zones.
 */
boolean_t
is_zones_supported(void)
{
	ASSERT(gSvcInfo.rs_rt_version > 0);
	return (gSvcInfo.rs_rt_version >= ZONE_SUPPORT_IN_RT_VERSION);
}

/*
 * Function to determine the existence of zpools property.
 */
boolean_t
is_zpools_property_exists(void)
{
	ASSERT(gSvcInfo.rs_rt_version > 0);
	return (gSvcInfo.rs_rt_version >= ZPOOLS_IN_RT_VERSION);
}

/*
 * Function to determine the existence of zpoolssearchdir property.
 */
boolean_t
is_zpoolssearchdir_property_exists(void)
{
	ASSERT(gSvcInfo.rs_rt_version > 0);
	return (gSvcInfo.rs_rt_version >= ZPOOLSSEARCHDIR_IN_RT_VERSION);
}

/*
 * Function: svc_fillAndValidateZpools
 *
 * Arguments:
 *
 * zpools	Array of zpools configured in the resource.
 *
 * Return:
 *
 * 0		if all zpools were validated and saved successfully.
 *
 * 1		if an error occurred.
 *
 * Comments:
 *
 * Individual steps explained below.
 */
static int
svc_fillAndValidateZpools(const scha_extprop_value_t *zpools)
{
#ifdef	ZFS_AVAILABLE
	scha_str_array_t  ci_poollist = { 0 };
	scha_str_array_t  pi_poollist = { 0 };
	uint_t		  i, j;
#endif

	/*
	 * Return if no zpools configured.
	 */

	if (zpools->val.val_strarray->array_cnt == 0)
		return (0);

#ifndef	ZFS_AVAILABLE
	/*
	 * SCMSGS
	 * @explanation
	 * Packages that contain ZFS are not installed.
	 * @user_action
	 * Upgrade to a release that contains ZFS. If ZFS is available and you
	 * see this message, contact your authorized Sun service provider.
	 */
	hasp_scds_syslog_0(LOG_ERR, "Zettabyte File System (ZFS) is "
	    "not available in this release.");
	return (1);
#else
	/*
	 * Fill the zpool information in structure gSvcInfo.
	 */
	for (i = 0; i < zpools->val.val_strarray->array_cnt; i++) {
		if (svc_addStr2Array(zpools->val.val_strarray->str_array[i],
		    &gSvcInfo.zpoolsInfo.zpoolsList)) {
			return (1);
		}
	}
	if (svc_initializeZFS()) {
		/*
		 * Error message already logged.
		 */
		return (1);
	}

	if ((svcMethodId == HASTORAGEPLUS_PRENET_START) ||
	    (svcMethodId == HASTORAGEPLUS_UPDATE)) {
		/*
		 * It is fine to skip the validation during
		 * resource start/update as the errors will be caught
		 * during actual operation.
		 * This is basically to avoid calling expensive operation
		 * in finding out the potential zfs pools that can be imported
		 * during validation.
		 */
		scds_syslog_debug(DBG_LEVEL_HIGH,
		    "Skipping validation for method %d.", svcMethodId);
		return (0);
	}

	/*
	 * Zpools cannot be configured within a scalable RG.
	 */
	if (gSvcInfo.rgMode == RGMODE_SCALABLE) {
		/*
		 * SCMSGS
		 * @explanation
		 * HAStoragePlus detected an inconsistency in its
		 * configuration: you defined the resource group as a scalable
		 * service. HAStoragePlus supports zpools that are defined
		 * only as a failover service. You cannot specify in this
		 * case.
		 * @user_action
		 * Remove the zpools from the service or change the scalable
		 * service for the zpool to a failover service.
		 */
		hasp_scds_syslog_0(LOG_ERR, "You cannot specify zpools in a "
		    "scalable service resource.");
		return (1);
	}

	/*
	 * Verify that specified zpools are not already included in
	 * existing HAStoragePlus resources.
	 */
	if (svc_zpoolsCrossCheck(zpools)) {
		/*
		 * Error message already logged.
		 */
		return (1);
	}

	/*
	 * In case the resource configured for zone cluster we need to ensure
	 * that all zpools are configured to zone cluster.
	 */
	if (gSvcInfo.svcExecNode == SVC_ZC_NODE_RUNS_IN_GZ) {

		if (!svc_zc_isAllZpoolsMapped(zpools)) {
			/*
			 * Error message logged.
			 */
			return (1);
		}
	}

	/*
	 * Obtain the pools that are currently imported on this node.
	 */
	if (svc_getciZpoolsList(&ci_poollist))
		return (1);

	scds_syslog_debug(DBG_LEVEL_HIGH, "The number of current zpools "
	    "available on this node are : %d.", ci_poollist.array_cnt);
	/*
	 * Obtain the pools that can be potentially imported to this node.
	 */
	if (svc_getpiZpoolsList(&pi_poollist, B_TRUE))
		return (1);

	scds_syslog_debug(DBG_LEVEL_HIGH, "The number of potential zpools "
	    "available on this node are : %d.", pi_poollist.array_cnt);
	/*
	 * Now start validation of each zpool and store the pool name in
	 * zpoolsList of global HAStoragePlus structure gSvcInfo.
	 */
	for (i = 0; i < zpools->val.val_strarray->array_cnt; i++) {

		/*
		 * Ensure that this pool can be accessed from this node.
		 * This is done is by ensuring that the pool configured should
		 * be in either in currently imported pools or in the pools
		 * that can be potentially imported.
		 */
		boolean_t found = B_FALSE;
		char	*poolname = zpools->val.val_strarray->str_array[i];

		for (j = 0; j < ci_poollist.array_cnt; j++) {
			if (strcmp(poolname, ci_poollist.str_array[j]) == 0) {
				found = B_TRUE;
				break;
			}
		}

#if	KEEP_ZFS_LEGACY_RESTRICTION
		/*
		 * Ensure ZFS mount points in a pool are not set to 'legacy'.
		 * HAStoragePlus will not support the pools which has file
		 * systems with mountpoint property set to 'legacy'.
		 * NOTE: Since we can't get the properties of the file system
		 * unless the pool that contains is imported, the validation
		 * of already imported pool is done here, and for the other
		 * pools will be done while importing/mounting the pool.
		 */
		if (found && svc_validateMntPtProperty(poolname)) {
			return (1);
		}
#endif

		/*
		 * HAStoragePlus needs to ensure there is only one matching
		 * pool for each zpools name configured in "ZPOOLS" property.
		 */
		for (j = 0; j < pi_poollist.array_cnt; j++) {
			if (strcmp(poolname, pi_poollist.str_array[j]) == 0) {
				if (found) {
					/*
					 * Multiple zpools with same name.
					 */
					/*
					 * SCMSGS
					 * @explanation
					 * Self explanatory.
					 * @user_action
					 * Ensure there is a unique zpool for
					 * the specified zpool name by renaming
					 * the duplicate zpools.
					 */
					hasp_scds_syslog_1(LOG_ERR,
					    "More than one matching zpool for "
					    "'%s': zpools configured to "
					    "HAStoragePlus must be unique.",
					    poolname);
					return (1);
				}
				found = B_TRUE;
			}
		}

		if (!found) {

			/*
			 * Zpool is not in current active pools or in the
			 * pools that can be potentially imported.
			 * Return error.
			 */
			/*
			 * SCMSGS
			 * @explanation
			 * The device that you added to the zpool is not
			 * accessible from this node.
			 * @user_action
			 * Check the zpools that can be accessed from this
			 * node by doing "zpool list" and "zpool import". If
			 * the zpool that is listed in the error message is
			 * among them, contact your authorized Sun service
			 * provider. Otherwise, remove the zpool from the
			 * resource group or remove the specified node from
			 * the resource group.
			 */
			hasp_scds_syslog_1(LOG_ERR, "The zpool '%s' is not "
			    "available from this node.",
			    poolname);
			return (1);
		}
	}

	scds_syslog_debug(DBG_LEVEL_HIGH, "Validation on all the zpools "
	    "completed successfully.");
	return (0);
#endif
}

#ifdef	ZFS_AVAILABLE
/*
 * Function: svc_zpoolsCrossCheck
 *
 * Arguments:
 *
 * zpools	Array of zpools configured in the current resource.
 *
 * Return:
 *
 * 0		if all zpools of current resource has not specified in any other
 *		existing resources.
 *
 * 1		if an error occurred or any of zpools of current resource is
 *		specified in some existing resource.
 *
 * Comments:
 * This function ensures that specified zpools of the current
 * resource is not included in any existing HAStoragePlus resources. This
 * validation is to avoid following problems.
 * 1)Data corruption: When the two resources using same zpool are online on
 * different nodes.
 * 2)Data unavailability: When the two resources using same zpool are online,
 * and one of the resource is brought offline.
 */
static int
svc_zpoolsCrossCheck(const scha_extprop_value_t *zpools)
{

	scha_cluster_t		sc_handle = NULL;
	scha_str_array_t	*rs_type_name = NULL;
	scha_err_t		err = SCHA_ERR_NOERR;
	uint_t			i;

retry_cluster_get :

	/* Obtain the cluster handle. */
	err =  scha_cluster_open_zone(gSvcInfo.cl_name, &sc_handle);
	if (err != SCHA_ERR_NOERR) {
		/*
		 * SCMSGS
		 * @explanation
		 * Access to the object named failed. The reason for the
		 * failure is given in the message.
		 * @user_action
		 * Save a copy of the /var/adm/messages files on all nodes.
		 * Contact your authorized Sun service provider for assistance
		 * in diagnosing the problem.
		 */
		hasp_scds_syslog_1(LOG_ERR,
		    "Failed to retrieve the cluster handle: %s.",
					    /* CSTYLED */
		    scds_error_string(err)); /*lint !e666 */
		return (1);
	}

	/* Get list of registered resource type names */
	err = scha_cluster_get_zone(gSvcInfo.cl_name, sc_handle,
	    SCHA_ALL_RESOURCETYPES, &rs_type_name);

	if (err == SCHA_ERR_SEQID) {

		/*
		 * SCMSGS
		 * @explanation
		 * HAStoragePlus was not able to the cluster configuration,
		 * but it will retry automatically.
		 * @user_action
		 * This is an informational message, no user action is needed.
		 */
		hasp_scds_syslog_0(LOG_INFO,
		    "Failed to retrieve the cluster information. Retrying...");
		(void) scha_cluster_close(sc_handle);
		goto retry_cluster_get;

	} else if (err != SCHA_ERR_NOERR) {

		/*
		 * SCMSGS
		 * @explanation
		 * HAStoragePlus was not able to retrieve the specified
		 * information from the cluster configuration.
		 * @user_action
		 * Check the cluster configuration. If the problem persists,
		 * contact your authorized Sun service provider.
		 */
		hasp_scds_syslog_2(LOG_ERR,
		    "Failed to retrieve information on %s: %s.",
		    SCHA_ALL_RESOURCETYPES,	/* CSTYLED */
		    scds_error_string(err));	/*lint !e666 */
		(void) scha_cluster_close(sc_handle);
		return (1);
	}

	/*
	 * Check each resource type name and process those beginning with
	 * "SUNW.HAStoragePlus" or "SUNW.HAStoragePlus:". Each such type found
	 * will be a version of HAStoragePlus. Obtain the list of resources
	 * of each such type and check the zpools in each resource.
	 */

	for (i = 0; i < rs_type_name->array_cnt; i++) {

		const char *rt_name = rs_type_name->str_array[i];
		size_t hasp_rt_len = strlen(HASP_RT);

		if ((strncmp(rt_name, HASP_RT, hasp_rt_len) != 0) ||
		    ((strlen(rt_name) > hasp_rt_len) &&
		    rt_name[hasp_rt_len] != ':')) {
			continue;
		}
		if (svc_CheckZpoolsInThisResourceType(zpools, rt_name)) {
			(void) scha_cluster_close(sc_handle);
			return (1);
		}
	}
	(void) scha_cluster_close(sc_handle);
	return (0);
}

/*
 * Function: svc_CheckZpoolsInThisResourceType
 *
 * Arguments:
 *
 * zpools	Array of zpools configured in the current resource.
 *
 * rt_name	The resource type name.
 *
 * Return:
 *
 * 0		if all zpools of current resource is not specified in the
 *		resources of the specified resource type.
 *
 * 1		if an error occurred or any of zpools of current resource is
 *		specified in some resource of this resource type.
 *
 * This function checks for specified zpools in the resources of the specified
 * resource type. It skips the resource type which does not support Zpools
 * extension property.
 */
static int
svc_CheckZpoolsInThisResourceType(const scha_extprop_value_t *zpools_list,
    const char *rt_name)
{
	scha_resourcetype_t	rs_type_handle = NULL;
	scha_resource_t		rs_handle = NULL;
	const char		*rs_type_version = NULL;
	scha_str_array_t	*rs_list = NULL;
	scha_extprop_value_t	*zpools = NULL;
	scha_err_t		err = SCHA_ERR_NOERR;
	uint_t			i, j, k;

	scds_syslog_debug(DBG_LEVEL_HIGH, "Checking the zpools in all the "
	    "resources belong to resource type: %s.", rt_name);

	err = scha_resourcetype_open_zone(gSvcInfo.cl_name, rt_name,
	    &rs_type_handle);
	if (err != SCHA_ERR_NOERR) {

		/*
		 * SCMSGS
		 * @explanation
		 * HAStoragePlus failed to access the resource type
		 * information.
		 * @user_action
		 * Check the cluster configuration. If the problem persists,
		 * contact your authorized Sun service provider.
		 */
		hasp_scds_syslog_1(LOG_ERR,
		    "Failed to open the resource type handle: %s.",
						/* CSTYLED */
		    scds_error_string(err));	/*lint !e666 */

		return (1);
	}
	err = scha_resourcetype_get_zone(gSvcInfo.cl_name, rs_type_handle,
	    SCHA_RT_VERSION, &rs_type_version);
	if (err != SCHA_ERR_NOERR) {
		/*
		 * SCMSGS
		 * @explanation
		 * An API operation has failed while retrieving the resource
		 * type property. Low memory or API call failure might be the
		 * reasons.
		 * @user_action
		 * In case of low memory, the problem will probably cured by
		 * rebooting. If the problem recurs, you might need to
		 * increase swap space by configuring additional swap devices.
		 * Otherwise, if it is API call failure, check the syslog
		 * messages from other components.
		 */
		hasp_scds_syslog_2(LOG_ERR,
		    "Failed to retrieve the resource type property %s: %s.",
						/* CSTYLED */
		    SCHA_RT_VERSION,
		    scds_error_string(err));	/*lint !e666 */
		(void) scha_resourcetype_close(rs_type_handle);
		return (1);
	}

	if (rs_type_version == NULL) {
		/*
		 * SCMSGS
		 * @explanation
		 * The HAStoragePlus resource type version of the specified
		 * resource type was found to be NULL.
		 * @user_action
		 * Check the cluster configuration. If the problem persists,
		 * contact your authorized Sun service provider.
		 */
		hasp_scds_syslog_1(LOG_ERR, "The resource type version of "
		    "resource type %s is NULL.", rt_name);
		(void) scha_resourcetype_close(rs_type_handle);
		return (1);
	}

	/*
	 * Skip the HAStoragePlus resource type which is not having
	 * Zpools extension property.
	 */
	if ((atoi(rs_type_version)) < ZPOOLS_IN_RT_VERSION) {
		(void) scha_resourcetype_close(rs_type_handle);

		scds_syslog_debug(DBG_LEVEL_HIGH, "Skipping the resource type "
		    " %s as Zpools property is not supported.", rt_name);
		return (0);
	}

	/*
	 * Retrieve the list of resources of this resource type and check
	 * for the zpools.
	 */

	err = scha_resourcetype_get_zone(gSvcInfo.cl_name, rs_type_handle,
	    SCHA_RESOURCE_LIST, &rs_list);
	if (err != SCHA_ERR_NOERR) {
		hasp_scds_syslog_2(LOG_ERR,
		    "Failed to retrieve the resource type property %s: %s.",
						/* CSTYLED */
		    SCHA_RESOURCE_LIST,
		    scds_error_string(err));	/*lint !e666 */

		(void) scha_resourcetype_close(rs_type_handle);
		return (1);
	}

	for (i = 0; i < rs_list->array_cnt; i++) {

		/*
		 * Skip the current resource.
		 */
		if (STREQ(gSvcInfo.rsName, rs_list->str_array[i])) {
			continue;
		}

		err = scha_resource_open_zone(gSvcInfo.cl_name,
		    rs_list->str_array[i], NULL, &rs_handle);

		if (err != SCHA_ERR_NOERR) {

			hasp_scds_syslog_2(LOG_ERR,
			    "Failed to open the resource %s handle: %s.",
							/* CSTYLED */
			    rs_list->str_array[i],
			    scds_error_string(err));	/*lint !e666 */

			(void) scha_resourcetype_close(rs_type_handle);
			return (1);
		}

		err = scha_resource_get_zone(gSvcInfo.cl_name, rs_handle,
		    SCHA_EXTENSION, ZPOOLS, &zpools);

		if (err != SCHA_ERR_NOERR) {

			hasp_scds_syslog_2(LOG_ERR,
			    "Failed to retrieve property %s: %s.",
			    ZPOOLS,			/* CSTYLED */
			    scds_error_string(err));	/*lint !e666 */
			(void) scha_resource_close(rs_handle);
			(void) scha_resourcetype_close(rs_type_handle);
			return (1);
		}

		for (j = 0; j < zpools->val.val_strarray->array_cnt; j++) {
			for (k = 0;
			    k < zpools_list->val.val_strarray->array_cnt; k++) {
				if (STREQ(
				    zpools_list->val.val_strarray->str_array[k],
				    zpools->val.val_strarray->str_array[j])) {
					/*
					 * SCMSGS
					 * @explanation
					 * HAStoragePlus detected that the
					 * specified ZFS pool is already
					 * configured in another resource.
					 * HAStoragePlus does not allow the
					 * same ZFS pool to be configured in
					 * multiple resources to avoid data
					 * corruption and availability
					 * problems.
					 * @user_action
					 * Do not use the specified ZFS pool
					 * in this resource.
					 */
					hasp_scds_syslog_2(LOG_ERR,
					    "The ZFS pool '%s' is already "
					    "configured in the resource %s.",
					    zpools->val.val_strarray->
					    str_array[j],
					    rs_list->str_array[i]);

					(void) scha_resource_close(rs_handle);
					(void) scha_resourcetype_close(
					    rs_type_handle);
					return (1);
				}
			}
		}
		(void) scha_resource_close(rs_handle);
	}
	(void) scha_resourcetype_close(rs_type_handle);

	return (0);
}
#endif

#if	SOL_VERSION >= __s10
/*
 * Function: svc_zc_isAllZpoolsMapped
 *
 * Arguments:
 *
 * zpools	The list of zpools which the mapping status to be checked.
 *
 * Return:
 * 		B_TRUE	all zpools are configured to zone cluster.
 * 		B_FALSE	error occurred while checking or some zpools are not
 * 			mapped.
 *
 * Comments:
 * This function checks the status of zfs pool name mapping to zone cluster.
 */
static boolean_t
svc_zc_isAllZpoolsMapped(const scha_extprop_value_t *zpools)
{
	zc_dochandle_t	zhandle;
	struct zc_dstab	dstab;
	uint_t		i;
	boolean_t	is_mapped = B_TRUE;

	/*
	 * Return success if no zpools configured.
	 */
	if (zpools->val.val_strarray->array_cnt == 0) {
		return (is_mapped);
	}

	if (clconf_lib_init() != 0) {
		hasp_scds_syslog_1(LOG_ERR, "INTERNAL ERROR: %s.",
		    "Failed to initialize clconf");
		return (B_FALSE);
	}

	if ((zhandle = zccfg_init_handle()) == NULL) {
		hasp_scds_syslog_0(LOG_ERR, "Failed to allocate memory.");
		return (B_FALSE);
	}

	if (zccfg_get_handle(gSvcInfo.cl_name, zhandle) != ZC_OK) {
		hasp_scds_syslog_2(LOG_ERR, "INTERNAL ERROR: %s: %s.",
		    "Failed to get zone handle for zone",
		    gSvcInfo.cl_name);
		zccfg_fini_handle(zhandle);
		return (B_FALSE);
	}

	for (i = 0; i < zpools->val.val_strarray->array_cnt; i++) {

		char *zpool_name = zpools->val.val_strarray->str_array[i];

		/*
		 * Initialize status that zpool is not mapped.
		 */
		is_mapped = B_FALSE;

		scds_syslog_debug(DBG_LEVEL_HIGH,
		    "Checking the data set %s mapping to zone cluster %s.",
		    zpool_name, gSvcInfo.cl_name);

		if (zccfg_setdsent(zhandle) != ZC_OK) {

			hasp_scds_syslog_2(LOG_ERR, "INTERNAL ERROR: %s: %s.",
			    "Failed to set to file system entries for zone",
			    gSvcInfo.cl_name);
			zccfg_fini_handle(zhandle);
			return (B_FALSE);
		}

		while (zccfg_getdsent(zhandle, &dstab) == ZC_OK) {
			if (strcmp(zpool_name, dstab.zc_dataset_name) == 0) {
				is_mapped = B_TRUE;
				break;
			}
		}

		scds_syslog_debug(DBG_LEVEL_HIGH,
		    "The data set %s mapping status to zone cluster %s : %d.",
		    zpool_name, gSvcInfo.cl_name, is_mapped);

		(void) zccfg_enddsent(zhandle);

		if (!is_mapped) {
			/*
			 * SCMSGS
			 * @explanation
			 * The specified zpool is not delegated to zone cluster
			 * and hence not authorized to configure in resource.
			 * @user_action
			 * Delegate the complete zpool to the zone cluster and
			 * repeat the operation. If the problem persists,
			 * contact your authorized Sun service provider with
			 * /var/adm/message and zone cluster configuration.
			 */
			hasp_scds_syslog_3(LOG_ERR,
			    "The zfs pool '%s' requires delegation of all "
			    "its datasets to use in zone cluster %s. "
			    "Use clzc(1CL)'s \"add dataset\" "
			    "subcommand and do \"set name = %s\" to delegate.",
			    zpool_name, gSvcInfo.cl_name, zpool_name);

			/*
			 * Skip checking remaining zpools.
			 */
			break;
		}
	}

	zccfg_fini_handle(zhandle);
	return (is_mapped);
}

#endif

/*
 *
 * Function: svc_fillAndValidateList
 *
 * Arguments:
 *
 *		None
 * Return:
 *
 * 0		if validation completes successfully.
 *
 * 1		if an error occurred.
 *
 * Comments:
 *
 * This function delegates the validation of global device path and file
 * system mountpoint entries to corresponding function.
 */
int
svc_fillAndValidateList()
{
	switch (gSvcInfo.svcExecNode) {

		case SVC_BASE_CLUSTER_GZ_NODE:

			if (svc_base_fillAndValidateList()) {
				return (1);
			}

			break;

		case SVC_BASE_CLUSTER_LZ_NODE_RUNS_IN_GZ:
#if	SOL_VERSION >= __s10
			/*
			 * The global devices are not supported in non cluster
			 * branded zones. So fail validation if global devices
			 * are specified.
			 */
			if (gSvcInfo.gdpCount > 0) {
				/*
				 * SCMSGS
				 * @explanation
				 * Global devices paths are not supported in
				 * the specified zone.
				 * @user_action
				 * Remove the global device paths and repeat
				 * creating the resource. If problem persists,
				 * contact your authorized Sun service provider.
				 */
				hasp_scds_syslog_1(LOG_ERR, "Global devices "
				    "are not supported in local zone : %s.",
				    gSvcInfo.zonename);
				return (1);
			}

			if (svc_base_fillAndValidateList()) {
				return (1);
			}

			break;
#endif
		case SVC_ZC_NODE_RUNS_IN_GZ:
#if	SOL_VERSION >= __s10

			if (svc_zc_fillAndValidateList()) {
				return (1);
			}

			break;
#endif
		case SVC_ZC_NODE:
		case SVC_BASE_CLUSTER_LZ_NODE:
		case SVC_UNKNOWN_NODE:
		default:
			ASSERT(0);
			break;
	}

	return (0);
}

/*
 *
 * Function: svc_base_fillAndValidateList
 *
 * Arguments:
 *
 *		None
 * Return:
 *
 * 0		if all the global device path and file system
 *		mount points were validated successfully.
 *
 * 1		if an error occurred.
 *
 * Comments:
 *
 * Individual steps explained below.
 *
 * Note:
 *
 * Dynamically allocated memory is not freed. This is because
 * all objects created with dynamic memory are accessed till
 * the end of program execution. Rather than free'ing memory
 * at the end of the program execution ( exit ), no
 * free() calls are made. Certain DCS related objects
 * which are allocated within DCS functions are an exception,
 * and are free'ed up using DCS calls.
 *
 * The 'base' in svc_base_fillAndValidateList() means Base Cluster.
 * It supports the resource created in global zone and non-cluster brand zones.
 *
 */

int
svc_base_fillAndValidateList(void)
{

	uint_t i = 0, j = 0;
	char *sername;
	char buffer[SVC_BUFFER_LEN];
	char *tmp, *localpath, *globalpath;
	char *delimiter = PATH_DELIM;
	const scha_extprop_value_t *gdp = gSvcInfo.gdp_prop_value;
	const scha_extprop_value_t *fmp = gSvcInfo.fmp_prop_value;

	/*
	 * Check if the global devices list AND file system mount
	 * point empty.
	 * If so, return 0.
	 */
	if (!gSvcInfo.count)
		return (0);

	/*
	 * Allocate memory for an array of 'gSvcInfo.count' HAStoragePlusSvc
	 * objects. Each object represents a global device path or a file
	 * system mount point.
	 *
	 */
	gSvcInfo.list = (HAStoragePlusSvc *) calloc(sizeof (HAStoragePlusSvc),
	    gSvcInfo.count);


	if (gSvcInfo.list == NULL) {

		/*
		 * SCMSGS
		 * @explanation
		 * HAStoragePlus ran out of resources.
		 * @user_action
		 * Usually, this means that the system has exhausted its
		 * resources. Check if the swap file is big enough to run
		 * Sun Cluster software.
		 */
		hasp_scds_syslog_0(LOG_ERR, "Failed to allocate memory.");

		return (1);
	}

	/*
	 * Examine the list of global device paths
	 */
	if (gdp->val.val_strarray->array_cnt != 0) {

		/*
		 * Point each global device path string in the 'svcPath'
		 * member.
		 *
		 * Set isMountPoint field to be B_FALSE, since a global
		 * device path can never be a mount point.
		 */

		for (i = 0; i < gdp->val.val_strarray->array_cnt; i++) {

			gSvcInfo.list[i].svcPath =
			    gdp->val.val_strarray->str_array[i];
			gSvcInfo.list[i].isMountPoint = B_FALSE;
		}
	}

	/*
	 * Examine the list of file system mount points.
	 */
	if (fmp->val.val_strarray->array_cnt != 0) {

		/*
		 * Store each file system mount point string
		 * in the 'svcPath' member.
		 *
		 * Set isMountPoint field to be B_TRUE, since this
		 * member is being treated as mount point (at least
		 * for the time being).
		 */

		for (i = gdp->val.val_strarray->array_cnt, j = 0;
		    j < fmp->val.val_strarray->array_cnt; i++, j++) {

			tmp = strdup(fmp->val.val_strarray->str_array[j]);
			if (tmp == NULL) {
				hasp_scds_syslog_0(LOG_ERR,
				    "Failed to allocate memory.");
				return (1);
			}

			/*
			 * Determine if the mount point is a local:global
			 * or a global style mount point. Parse apart
			 * the mountpoints if needed.
			 */
			localpath = strtok(tmp, delimiter);
			globalpath = strtok(NULL, delimiter);

			if (globalpath == NULL) {
				gSvcInfo.list[i].svcPath = strdup(localpath);
			} else {
				gSvcInfo.list[i].svcPath = strdup(globalpath);
			}

			gSvcInfo.list[i].svcLocalPath = strdup(localpath);

			if ((gSvcInfo.list[i].svcPath == NULL) ||
			    (gSvcInfo.list[i].svcLocalPath == NULL)) {
				hasp_scds_syslog_0(LOG_ERR,
				    "Failed to allocate memory.");
				return (1);
			}

			free(tmp);
			gSvcInfo.list[i].isMountPoint = B_TRUE;
		}
	}

	if (gdp->val.val_strarray->array_cnt != 0) {

		for (i = 0; i < gdp->val.val_strarray->array_cnt; i++) {

			/*
			 * Obtain service name associated with global
			 * device path.
			 */
			if (svc_getGDPSvcName(gSvcInfo.list[i].svcPath,
			    &sername)) {
				/*
				 * Error message logged.
				 */
				return (1);
			}

			scds_syslog_debug(DBG_LEVEL_HIGH,
				"Global service %s is associated"
				" with global device path %s.",
				sername, gSvcInfo.list[i].svcPath);

			if (svc_addStr2Array(sername,
				&gSvcInfo.list[i].svcName)) {

				return (1);

			}
		}  /* for */
	}

	if (fmp->val.val_strarray->array_cnt != 0) {

		struct vfstab vp, vref;
		FILE *fp = NULL;

		(void) memset(&vref, 0, sizeof (struct vfstab));

		/*
		 * VFSTAB has to be opened prior to verifying
		 * vfstab entries.
		 */

		if ((fp = fopen(VFSTAB, "r")) == NULL) {

			int ret = errno;    /*lint !e746 */
			/*
			 * SCMSGS
			 * @explanation
			 * Failed to open the specific file.
			 * @user_action
			 * Check for specific error in the error messages and
			 * rectify the problem.
			 */
			hasp_scds_syslog_2(LOG_ERR,
			    "Failed to open global zone %s: %s.",
			    VFSTAB, strerror(ret));	/*lint !e666 */

			return (1);
		}

		/*
		 * Increment i till we can start accessing file system
		 * mount points.
		 *
		 * For each file system mount point,
		 *
		 * 1. Verify that a vfstab entry does exist
		 *
		 * 2. Use DCS to translate its special file entry
		 *    to obtain the service path.
		 */
		for (i = gdp->val.val_strarray->array_cnt; i < gSvcInfo.count;
		    i++) {

			int ret;

			/*
			 * vref is a reference vfstab object. Initialize it
			 * to the mount point whose existence we are
			 * trying to verify.
			 */
			vref.vfs_mountp = (char *)gSvcInfo.list[i].svcPath;

			/*
			 * Obtain a filled 'struct vfstab' entry for the
			 * given mount point.
			 */
			ret = getvfsany(fp, &vp, &vref);

			if (ret == 0) {

				/*
				 *  Mount point is present in VFSTAB.
				 */
				scds_syslog_debug(DBG_LEVEL_HIGH,
					"Detected a VFSTAB entry for file"
					" system mount point %s.",
					vref.vfs_mountp);

			} else if (ret == -1)  {

				/*
				 *  Reached end of file. Hence entry
				 *  for mount point not present in
				 *  VFSTAB.
				 */

				/*
				 * SCMSGS
				 * @explanation
				 * HAStoragePlus looked for the specified
				 * mount point in the specified file (usually
				 * /etc/vfstab) but didn't find it.
				 * @user_action
				 * Usually, this means that a typo has been
				 * made when filling the FilesystemMountPoints
				 * property -or- that the entry for the file
				 * system and mount point does not exist in
				 * the specified file.
				 */
				hasp_scds_syslog_2(LOG_ERR,
				    "Entry for file system mount point %s is "
				    "absent from global zone %s.",
				    vref.vfs_mountp, VFSTAB);

				return (1);

			} else {

				/*
				 * A read error occurred.
				 */

				svc_reportVfsError(ret, vref.vfs_mountp);
				/*
				 * SCMSGS
				 * @explanation
				 * An error occurred while reading the vfstab
				 * entry for the specified mount point.
				 * @user_action
				 * Verify that the vfstab entry for the mount
				 * point is correct and repeat the operation.
				 */
				hasp_scds_syslog_2(LOG_ERR,
				    "An error occurred while reading global "
				    "zone %s file for filesystem "
				    "mount point %s.",
				    VFSTAB, vref.vfs_mountp);
				return (1);

			}


			/*
			 * Validate and copy the vfstab entry.
			 */
			if (svc_validateVfsTabEntry(&vp,
			    &gSvcInfo.list[i].vfsEntry)) {
				/*
				 * SCMSGS
				 * @explanation
				 * HAStoragePlus failed while validating
				 * the global zone vfstab entry for the
				 * specified mountpoint.
				 * @user_action
				 * Usually, this happens due to
				 * incorrect entries in vfstab.
				 * Check the log messages for
				 * specified error and repeat the operation.
				 */
				hasp_scds_syslog_2(LOG_ERR,
				    "Validation of global zone %s entry for "
				    "filesystem mount point %s is failed.",
				    VFSTAB, vref.vfs_mountp);
				return (1);
			}

			/*
			 * Evaluate the 'struct vfstab' for this
			 * mount point. This is to know whether the mount
			 * point is for a  global/local file
			 * file system.
			 *
			 */
			ret = svc_evalVfsTabMountOption(&vp, buffer);


			switch (ret)  {

				case SvcMountOptGlobal :

					/* Global mount point */

					scds_syslog_debug(DBG_LEVEL_HIGH,
						"File system associated with"
						" mount point %s is to be"
						" globally mounted.",
						gSvcInfo.list[i].svcPath);

					gSvcInfo.list[i].isGlobalFS = B_TRUE;

					break;

				case SvcMountOptLocal :

					/* Local mount point */

					scds_syslog_debug(DBG_LEVEL_HIGH,
						"File system associated with"
						" mount point %s is to be"
						" locally mounted.",
						gSvcInfo.list[i].svcPath);

					gSvcInfo.list[i].isGlobalFS = B_FALSE;

					/*
					 * A local file system mount point
					 * requires AffinityOn
					 * to be set to TRUE.
					 *
					 */

					if (gSvcInfo.affinityOn == B_FALSE) {

						/*
						 * SCMSGS
						 * @explanation
						 * HAStoragePlus detected
						 * that the specified mount
						 * point in /etc/vfstab is a
						 * local mount point, hence
						 * extension property
						 * AffinityOn must be set to
						 * True.
						 * @user_action
						 * Set the AffinityOn
						 * extension property of the
						 * resource to True.
						 */
						hasp_scds_syslog_1(LOG_ERR,
						    "File system associated"
						    " with mount point %s is"
						    " to be locally mounted."
						    " The AffinityOn value"
						    " cannot be FALSE.",
						    gSvcInfo.list[i].svcPath);

						return (1);
					}

					/*
					 * AffinityOn being TRUE
					 * is meaningless for scalable
					 * services. Affinity switchovers
					 * are ignored if the RG is
					 * scalable.
					 *
					 * However, local file systems
					 * mount operations do
					 * require AffinityOn = TRUE.
					 *
					 * Hence, local file system mount
					 * points cannot be
					 * specified within the context
					 * of a scalable RG.
					 */

					if (gSvcInfo.rgMode ==
						RGMODE_SCALABLE) {

						/*
						 * SCMSGS
						 * @explanation
						 * HAStoragePlus detected an
						 * inconsistency in its
						 * configuration: the resource
						 * group has been specified as
						 * scalable, hence local file
						 * systems cannot be specified
						 * in the
						 * FilesystemMountPoints
						 * extension property.
						 * @user_action
						 * Correct either the resource
						 * group type or remove the
						 * local file system from the
						 * FilesystemMountPoints
						 * extension property.
						 */
						hasp_scds_syslog_1(LOG_ERR,
						    "File system associated"
						    " with mount point %s is"
						    " to be locally mounted."
						    " Local file systems"
						    " cannot be specified with"
						    " scalable service"
						    " resources.",
						    gSvcInfo.list[i].svcPath);

						return (1);
					}

					break;

				case SvcMountOptError :
				default : {

					/*
					 * Vfstab entry not recognized.
					 */

					/*
					 * SCMSGS
					 * @explanation
					 * The format of the entry for the
					 * specified mount point in the
					 * specified file is invalid.
					 * @user_action
					 * Edit the file (usually /etc/vfstab)
					 * and check that entries conform to
					 * its format.
					 */
					hasp_scds_syslog_3(LOG_ERR,
					    "Entry in the global zone %s for "
					    "file system mount point "
					    "%s is incorrect: %s.",
					    VFSTAB, gSvcInfo.list[i].svcPath,
					    buffer);

					return (1);
				}
			}

			/*
			 * Evaluate the file system check command string for
			 * each individual file system.
			 *
			 * The evaluation is necessary only in the context of
			 * a failover resource group and therefore ignored
			 * for a scalable resource group.
			 */

			if (gSvcInfo.rgMode == RGMODE_FAILOVER) {

				if (svc_evalFSCKWithFS(&vp,
				&(gSvcInfo.list[i])) != 0) {

					return (1);
				}
			}

			/*
			 * Determine if special processing is needed for
			 * 'samfs' mount points.
			 */
			if (strcmp(vp.vfs_fstype, "samfs") == 0) {

				/*
				 * 'samfs' mount point present in VFSTAB.
				 */
				scds_syslog_debug(DBG_LEVEL_HIGH,
					"Detected a SAM-FS vfstab entry for"
					" %s.", vp.vfs_special);

				if (gSvcInfo.list[i].isGlobalFS == B_TRUE) {
					if (gSvcInfo.rgMode
						== RGMODE_FAILOVER) {
						/*
						 * SCMSGS
						 * @explanation
						 * The specified HAStoragePlus
						 * resource is a failover
						 * resource, hence shared QFS
						 * file systems cannot be
						 * specified in the
						 * FilesystemMountPoints
						 * extension property.
						 * @user_action
						 * Correct either the resource
						 * group type or remove the
						 * shared QFS file system from
						 * the FilesystemMountPoints
						 * extension property.
						 */
						hasp_scds_syslog_1(LOG_ERR,
						    "shared QFS file system"
						    " associated with mount"
						    " point %s cannot be"
						    " specified with"
						    " failover resources.",
						    gSvcInfo.list[i].svcPath);

						return (1);
					}
					char *mds_rs = NULL;
					/*
					 * Check that a dependency between
					 * HAStoragepPlus and MDS resource
					 * exists.
					 * First find out our MDS resource.
					 */

					if (svc_findOurMDSrs(&mds_rs,
						gSvcInfo.list[i].svcPath)
							!= SCHA_ERR_NOERR) {
						if (mds_rs)
							free(mds_rs);
						return (1);
					}

					/* check for the MDS rs dependency */
					if (svc_checkRSdependency(
						gSvcInfo.ds_handle, mds_rs)
							== SCHA_ERR_CHECKS) {
						/*
						 * SCMSGS
						 * @explanation
						 * The specifed HAStoragePlus
						 * resource requires an explicit
						 * dependency on the metadata
						 * server resource that
						 * represents the mount point.
						 * @user_action
						 * Define the required
						 * dependency between the
						 * HAStoragePlus resource and
						 * the metadata server resource.
						 */
						hasp_scds_syslog_2(LOG_ERR,
							"Dependency between"
							" resource %s and its"
							" MDS resource %s has "
							"not been defined.",
							gSvcInfo.rsName,
							mds_rs);
						if (mds_rs)
							free(mds_rs);
						return (1);
					}

					/* free up mds_rs */
					if (mds_rs)
						free(mds_rs);
					/*
					 * This is HA-sQFS, so skip the HA-QFS
					 * special processing checks.
					 */
					continue; /* sQFS */
				}

				/*
				 * If we are HA-QFS, retrive the service names
				 * for the global device paths associated with
				 * SAMFS special file.
				 */
				if (svc_fillSAMFSSvcName(vp.vfs_special,
				    &gSvcInfo.list[i])) {
					return (1);
				}

				/*
				 * Done with special SAMFS processing.
				 * Process next mount point.
				 */
				continue; /* HA-QFS */
			}

			/*
			 * Detect and flag an invalid global device path.
			 */
			if (svc_isDCSDevicePath(vp.vfs_special) == B_FALSE) {

				/*
				 * SCMSGS
				 * @explanation
				 * The specified global device path is not
				 * valid.
				 * @user_action
				 * Check the path and correct the entry with
				 * a valid global device path.
				 */
				hasp_scds_syslog_2(LOG_ERR,
				    "Invalid global device path %s detected "
				    "for mount point %s.",
				    vp.vfs_special, vp.vfs_mountp);
				return (1);
			}

			/*
			 * Obtain device service name of global device path.
			 */
			if (svc_getDCSPathServiceName(
			    vp.vfs_special, &sername)) {
				/*
				 * SCMSGS
				 * @explanation
				 * The DCS was not able to find the global
				 * service name.
				 * @user_action
				 * Check the global service configuration.
				 */
				hasp_scds_syslog_2(LOG_ERR,
				    "Failed to obtain the global service name "
				    "of device %s for mount point %s.",
				    vp.vfs_special, vp.vfs_mountp);
				return (1);
			}

			/*
			 * Duplicate the service name returned by DCS
			 * and set it to svcName.
			 */

			if (svc_addStr2Array(sername,
				&gSvcInfo.list[i].svcName)) {

				return (1);

			}

			scds_syslog_debug(DBG_LEVEL_HIGH,
				"Global service %s is associated"
				" with file system mount point %s.",
				sername, gSvcInfo.list[i].svcPath);

			/*
			 * Enable search from the beginning of the VFSTAB file,
			 * to allow the ordering of file systems in
			 * FilesystemMountPoints to be independent of those
			 * specified in the VFSTAB file.
			 *
			 */
			rewind(fp);

		} /* for */

		(void) fclose(fp);

	} /* if */

	/*
	 * All device service objects ( of types global device paths and
	 * file system mount  points ) are now complete with
	 * mount point, service path/name information. Complete the
	 * remaining DCS validations.
	 *
	 * Each device service is completely validated before
	 * moving to the one.
	 */
	if (svc_validateDeviceServices()) {
		return (1);
	}

	return (0);
}


/*
 * Function: svc_validateDeviceServices
 *
 * Arguments:
 *
 *		None
 * Return:
 *
 * 0		if all device services of global device path and file system
 *		mount point were validated successfully.
 *
 * 1		if an error occurred.
 *
 * Comments:
 *
 * Individual steps explained below.
 *
 * This function validate each device service.
 */
static int
svc_validateDeviceServices(void)
{
	uint_t i = 0, j = 0;
	int is_susp = 0;
	sc_state_code_t state;
	dc_error_t dc;
	dc_replica_status_seq_t *act_rep;
	dc_replica_seq_t *nodes_seq;
	int failbackEnabled;
	boolean_t b_flag;

	for (i = 0; i < gSvcInfo.count; i++) {

		for (j = 0; j < gSvcInfo.list[i].svcName.array_cnt; j++) {

			dc = dcs_get_service_status(
			gSvcInfo.list[i].svcName.str_array[j],
			&is_susp, &act_rep, &state, NULL);


			if (dc != DCS_SUCCESS) {

				/*
				 * SCMSGS
				 * @explanation
				 * The DCS reported that the specified global
				 * service was unavailable.
				 * @user_action
				 * Check the device links (SCSI, Fiberchannel,
				 * etc.) cables.
				 */
				hasp_scds_syslog_3(LOG_ERR,
				    "Global service %s associated with path %s"
				    " is found to be unavailable: %s.",
				    gSvcInfo.list[i].svcName.str_array[j],
				    gSvcInfo.list[i].svcPath,	/* CSTYLED */
				    dcs_error_to_string(dc));	/*lint !e666 */

				return (1);

			}

			/*
			 * Ensure that active replicas did get returned
			 * from the dcs function call.
			 */
			ASSERT(act_rep != NULL);

			/*
			 * Check if a device service is in suspended state.
			 *
			 */
			if (is_susp != 0) {


				/*
				 * We are being conservative here. It is
				 * possible that this device service
				 * may become non suspended at a later
				 * point in time ( especially between
				 * the validate and prenet_start time
				 * time intervals ). Either way,
				 * this is being treated as an error.
				 */
				/*
				 * SCMSGS
				 * @explanation
				 * Self explanatory.
				 * @user_action
				 * Check the global service configuration.
				 */
				hasp_scds_syslog_2(LOG_ERR,
				    "Global service %s associated with path %s"
				    " is found to be in maintenance state.",
				    gSvcInfo.list[i].svcName.str_array[j],
				    gSvcInfo.list[i].svcPath);

				return (1);

			}

			/*
			 * Obtain all the replica sequences for the device
			 * service.
			 */

			dc = dcs_get_service_parameters(
				gSvcInfo.list[i].svcName.str_array[j],
				NULL, &failbackEnabled, &nodes_seq,
				NULL, NULL, NULL);


			if (dc != DCS_SUCCESS) {

				/*
				 * SCMSGS
				 * @explanation
				 * The DCS was not able to obtain the replica
				 * information for the specified global
				 * service.
				 * @user_action
				 * Check the cluster configuration. If the
				 * problem persists, contact your authorized
				 * Sun service provider.
				 */
				hasp_scds_syslog_3(LOG_ERR,
				    "Failed to obtain replica information for"
				    " global service %s associated with path"
				    " %s: %s.",
				    gSvcInfo.list[i].svcName.str_array[j],
				    gSvcInfo.list[i].svcPath,	/* CSTYLED */
				    dcs_error_to_string(dc));	/*lint !e666 */

				return (1);

			}

			if (failbackEnabled)
				b_flag = B_TRUE;
			else
				b_flag = B_FALSE;

			scds_syslog_debug(DBG_LEVEL_HIGH,
			    "Global service %s has Failback set to %s.",
			    gSvcInfo.list[i].svcName.str_array[j],
			((b_flag == B_TRUE) ? "True" : "False"));

			/*
			 * Inspect the service's failback setting. If the
			 * device failback setting does not match that
			 * of the RG, error return if this function is
			 * being called within the validate method.
			 *
			 * Otherwise, log a warning message and continue.
			 *
			 *
			 * This check is applicable only to preserve the
			 * colocation rule. Hence, AffinityOn should be
			 * TRUE.
			 */
			if (gSvcInfo.affinityOn == B_TRUE) {

				if (gSvcInfo.failback != b_flag) {

					/*
					 * Detected a mismatch.
					 */
					if (svcMethodId ==
					    HASTORAGEPLUS_VALIDATE) {

						/*
						 * SCMSGS
						 * @explanation
						 * HAStoragePlus detected a
						 * mismatch between the
						 * Failback setting for the
						 * resource group and the
						 * Failback setting for the
						 * specified DCS global
						 * service.
						 * @user_action
						 * Correct either the Failback
						 * setting of the resource
						 * group -or- the Failback
						 * setting of the DCS global
						 * service.
						 */
						hasp_scds_syslog_4(LOG_ERR,
						    "Mismatch between the"
						    " Failback policies for"
						    " the resource group %s"
						    " (%s) and global service"
						    " %s (%s) detected.",
						    gSvcInfo.rgName,
						    (gSvcInfo.failback ==
							B_TRUE) ? "True" :
							"False",
						    gSvcInfo.list[i].svcName.\
							str_array[j],
						    (b_flag == B_TRUE) ? "True"
							: "False");

						/*
						 * Return an error to cause
						 * the validate method to
						 * fail.
						 */
						return (1);

					}

					hasp_scds_syslog_4(LOG_WARNING,
					    "Mismatch between the Failback"
					    " policies for the resource group"
					    " %s (%s) and global service %s"
					    " (%s) detected.",
					    gSvcInfo.rgName,
					    (gSvcInfo.failback == B_TRUE)
						?  "True" : "False",
					    gSvcInfo.list[i].svcName.\
						str_array[j],
					    (b_flag == B_TRUE)
						? "True" : "False");

					/*
					 * Continue - even after
					 * detecting this failback
					 * misconfiguration.
					 *
					 * Returning an error for
					 * other methods is
					 * regarded as being too
					 * severe.
					 */

				}

			}
			/*
			 * Validate the device service replica list.
			 */
			if (svc_validateNodeDeviceReplica(
			    gSvcInfo.list[i].svcName.str_array[j],
			    gSvcInfo.list[i].svcPath, nodes_seq)) {
				return (1);
			}

			scds_syslog_debug(DBG_LEVEL_HIGH,
				"Validations for global service %s associated"
				" with path %s complete.",
				gSvcInfo.list[i].svcName.str_array[j],
				gSvcInfo.list[i].svcPath);


		} /* for (j) */

	} /* for (i) */

	return (0);
}

/*
 * Function: svc_validateNodeDeviceReplica
 *
 * Arguments:
 *
 *		svcName	The global service name which the device replica to
 *			validate.
 *		svcPath	The global service path for the above service name.
 *		ds_replicas	The device service node replica's list.
 * Return:
 *
 * 0		if global device service is validated successfully.
 *
 * 1		if an error occurred.
 *
 * Comments:
 * This function examine the device replica of current node on which the
 * resource group is online and does corresponding validation.
 */
static int
svc_validateNodeDeviceReplica(const char *svcName, const char *svcPath,
    const dc_replica_seq_t *ds_replicas)
{
	boolean_t found = B_FALSE;
	uint_t *dev_nodeid_array; /* array of nodeids ordered by pref */
	uint_t *dev_pref_array;   /* array of preferences ordered by pref */
	uint_t dev_array_count;   /* number of elements in the arrays */
	uint_t i, j;

	found = B_FALSE;

	/*
	 * Allocate memory for two arrays of nodes attached to
	 * the device (array of node ids + array of pref).
	 */
	dev_array_count = ds_replicas->count;
	dev_nodeid_array = (uint_t *)malloc(
			dev_array_count * sizeof (uint_t));
	dev_pref_array = (uint_t *)malloc(
			dev_array_count * sizeof (uint_t));
	if (!(dev_nodeid_array && dev_pref_array)) {

		hasp_scds_syslog_0(LOG_ERR, "Failed to allocate memory.");
		return (1);
	}

	/*
	 * Examine each replica instance in the replica sequence.
	 */
	for (i = 0; i < dev_array_count; i++) {
		uint_t id;
		uint_t pref;

		id = ds_replicas->replicas[i].id;
		pref = ds_replicas->replicas[i].preference;

		/*
		 * Put the node in the already ordered by preference arrays.
		 */
		for (j = 0; j < i; j++)
			/*
			 * the more preferred, the smallest
			 * number the DCS preference is.
			 */
			if (pref < dev_pref_array[j])
				break;

		/* j is the place where to place the node. */

		/*
		 * First, we have to shift right the remaining
		 * of the arrays: (i - j) elements left
		 */
		(void) memcpy(&(dev_nodeid_array[j+1]),
				&(dev_nodeid_array[j]),
				(i-j) * sizeof (uint_t));
		(void) memcpy(&(dev_pref_array[j+1]),
				&(dev_pref_array[j]),
				(i-j) * sizeof (uint_t));
		/*
		 * Then put the node in the arrays.
		 */
		dev_nodeid_array[j] = id;
		dev_pref_array[j] = pref;

		if (id == gSvcInfo.nodeId)
			found = B_TRUE;
	}

	if (found == B_FALSE) {

		/*
		 * A device service replica for this node does not exist.
		 *
		 * The device service can therefore never be
		 * primaried i.e. switched over to this node.
		 */

		if (gSvcInfo.rgMode == RGMODE_FAILOVER &&
		    gSvcInfo.affinityOn == B_TRUE) {

			/*
			 * Return an error because affinity switchover of the
			 * device group to this node is not possible.
			 *
			 * In other words, attempts to primary the device
			 * service to this node will fail.
			 */
			/*
			 * SCMSGS
			 * @explanation
			 * Self explanatory.
			 * @user_action
			 * Remove the specified node from the resource group.
			 */
			hasp_scds_syslog_2(LOG_ERR,
			    "This node is not in the replica"
			    " node list of global service %s"
			    " associated with path %s."
			    " Although AffinityOn is set to"
			    " TRUE, device switchovers cannot"
			    " be performed to this node.",
			    svcName, svcPath);

			return (1);

		}

		/*
		 * SCMSGS
		 * @explanation
		 * Self explanatory.
		 * @user_action
		 * This is an informational message, no user action is needed.
		 */
		hasp_scds_syslog_2(LOG_INFO,
		    "This node is not in the replica node list"
		    " of global service %s associated with"
		    " path %s. This is acceptable because the"
		    " existing affinity settings do not"
		    " require resource and device"
		    " colocation.",
		    svcName, svcPath);

	} else {

		if (gSvcInfo.rgMode == RGMODE_FAILOVER &&
		    gSvcInfo.affinityOn == B_TRUE) {

			if (gSvcInfo.failback == B_TRUE) {

				/*
				 * HASP nodeid array and device
				 * service nodeid array should match.
				 */

				if (memcmp(dev_nodeid_array,
				    gSvcInfo.rgNodeIdArray,
				    gSvcInfo.rgNodeIdArraySize
					* sizeof (uint_t))) {

					if (svcMethodId ==
					    HASTORAGEPLUS_VALIDATE) {

						/*
						 * SCMSGS
						 * @explanation
						 * HAStoragePlus detected that
						 * the RGM node list preference
						 * and the DCS device service
						 * node list preference do
						 * not match. This is not
						 * allowed
						 * because AffinityOn is
						 * set to True.
						 * @user_action
						 * Correct either the RGM node
						 * list preference -or- the DCS
						 * node list preference.
						 */
						hasp_scds_syslog_2(LOG_ERR,
						    "Mismatch between the "
						    "resource group %s node "
						    "list preference and the "
						    "global device %s node "
						    "list preference detected.",
						    gSvcInfo.rgName,
						    svcName);

						return (1);
					}

					hasp_scds_syslog_2(LOG_WARNING,
					    "Mismatch between the resource "
					    "group %s node list preference "
					    "and the global device %s node "
					    "list preference detected.",
					    gSvcInfo.rgName,
					    svcName);

					/*
					 * Continue - even after detecting this
					 * mis-configuration.
					 *
					 * Returning an error for other methods
					 * is regarded as being too severe.
					 */

				}
			}

			if (dev_nodeid_array[0] != gSvcInfo.nodeId) {
				if (svcMethodId == HASTORAGEPLUS_VALIDATE) {

					/*
					 * SCMSGS
					 * @explanation
					 * HAStoragePlus determined that the
					 * node is less preferred to another
					 * node from the DCS global service
					 * view, but as the Failback setting
					 * is Off, this is not a problem.
					 * @user_action
					 * This is an informational message,
					 * no user action is needed.
					 */
					hasp_scds_syslog_3(LOG_INFO,
					    "This node has a lower preference "
					    "to node %d for global service %s "
					    "associated with path %s.Device "
					    "switchover can still be done to "
					    "this node.",
					    dev_nodeid_array[0],
					    svcName,
					    svcPath);

					/*
					 * Continue - although the local node is
					 * less preferred from becoming primary
					 * for the device service.
					 */

				}
			}
		}
	}

	free(dev_nodeid_array);
	free(dev_pref_array);
	return (0);
}

/*
 * Function: svc_getGDPSvcName
 *
 * Arguments:
 *
 * svcPath	The service path which the service name is required.
 *
 * sername	The location which contains the service name of given path
 *
 * Return:
 * 		0	if service name is obtained successfully.
 * 		1	if an error occurs
 *
 * Comments:
 * This function returns the service name for a valid global device path
 * entry.
 */
int
svc_getGDPSvcName(const char *svcPath, char **sername)
{
	dc_error_t dc;

	/*
	 * Examine each svcPath, and decide whether it
	 * represents a device group or a device special file.
	 */

	if (*svcPath != '/') {

		/*
		 * The global device path is assumed to be a
		 * a device group name. Check its validity.
		 *
		 * dsk/d1, metcalf-1 are some valid examples.
		 */

		dc = dcs_get_service_parameters(svcPath,
		    NULL, NULL, NULL, NULL, NULL, NULL);

		if (dc != DCS_SUCCESS) {

			/*
			 * SCMSGS
			 * @explanation
			 * HAStoragePlus found that the specified path is
			 * not a global device.
			 * @user_action
			 * Usually, this means that a typo has been made when
			 * filling the GlobalDevicePaths property.
			 * Check and specify valid global device path.
			 */
			hasp_scds_syslog_2(LOG_ERR,
			    "Specified global device path %s is invalid : %s.",
			    svcPath,
						/* CSTYLED */
						/*lint !e666 */
			    dcs_error_to_string(dc));

			return (1);

		}

		/*
		 * The service name is same as svcPath.
		 */

		*sername = (char *)svcPath;

	} else if (svc_isDCSDevicePath(svcPath) == B_TRUE) {
		/*
		 * Global device path is a device special file.
		 *
		 * /dev/global/dsk/d4s0, /dev/md/cookies-1/dsk/d4s2 are some
		 * valid examples.
		 *
		 * Obtain the corresponding service name.
		 */

		if (svc_getDCSPathServiceName(svcPath, sername)) {
			/*
			 * Error message already logged.
			 */
			return (1);
		}

	} else {

		/*
		 * Global device path is not recognized as a device group or
		 * a device special file.
		 */

		/*
		 * SCMSGS
		 * @explanation
		 * Self explanatory.
		 * @user_action
		 * Check that the specified global device path is valid.
		 */
		hasp_scds_syslog_1(LOG_ERR,
		    "Global device path %s is not recognized as a device "
		    "group or a device special file.", svcPath);

		return (1);
	}

	return (0);
}

/*
 * Function: svc_fillSAMFSSvcName
 *
 * Arguments:
 *
 * special	The special file of samfs which needs the service names.
 *
 * listp	The location where the service names are filled.
 *
 * Return:
 * 		0	if service name are filled successfully.
 * 		1	if an error occurs
 *
 * Comments:
 * This function fill the service names associated with SAMFS special file.
 */
int
svc_fillSAMFSSvcName(const char *special, HAStoragePlusSvc *listp)
{
	char	*dev_list = NULL;
	char	*sername;
	uint_t	i;

	/*
	 * Determine if the special mount point is known
	 * If so, obtain a list of constituent volumes
	 * in dev_list.
	 */
	if (svc_SAMFSVols(special, &dev_list)) {

		/*
		 * SCMSGS
		 * @explanation
		 * HAStoragePlus was not able to
		 * determine the volumes that are part
		 * of this SAM-FS special device.
		 * @user_action
		 * Check the SAM-FS file system
		 * configuration. If the problem
		 * persists, contact your authorized
		 * Sun service provider.
		 */
		hasp_scds_syslog_1(LOG_ERR,
		    "Failed to obtain SAM-FS constituent volumes for %s.",
		    special);
		return (1);
	}

	/*
	 * Set list of constituent volumes in svcName.
	 */

	if (svc_parseDeviceList(dev_list, &listp->svcName)) {

		/*
		 * Message already logged.
		 */
		return (1);
	}


	scds_syslog_debug(DBG_LEVEL_HIGH,
		"Global service %s is associated"
		" with file system mount point %s.",
		dev_list, listp->svcPath);

	free(dev_list);

	/*
	* Validate the global device paths of each
	* constituent volume
	*/
	for (i = 0; i < listp->svcName.array_cnt; i++) {

		/*
		 * Detect and flag an invalid global
		 * device path.
		 */
		if (svc_isDCSDevicePath(listp->svcName.str_array[i])
		    == B_FALSE) {

			/*
			 * SCMSGS
			 * @explanation
			 * The specified global device path used by the SAM-FS
			 * file system is not valid.
			 * @user_action
			 * Check the SAM-FS file system configuration.
			 */
			hasp_scds_syslog_2(LOG_ERR,
			    "Invalid global device path %s detected for SAMFS "
			    "mount point %s.",
			    listp->svcPath,
			    listp->svcName.str_array[i]);

			return (1);
		}

		/*
		 * Global device path is device special
		 * file. Obtain the corresponding service name.
		 */
		if (svc_getDCSPathServiceName(listp->svcName.str_array[i],
		    &sername)) {
			/*
			 * Error message already logged.
			 */
			return (1);
		}

		/*
		 * Set the svcName to be the one returned by DCS.
		 */
		free(listp->svcName.str_array[i]);

		listp->svcName.str_array[i] = strdup(sername);

		if (listp->svcName.str_array[i] == NULL) {

			hasp_scds_syslog_0(LOG_ERR,
			    "Failed to allocate memory.");
			return (1);
		}
	}
	return (0);
}

#if	SOL_VERSION >= __s10
/*
 *
 * Function: svc_zc_fillAndValidateList
 *
 * Arguments:
 *
 *		None
 * Return:
 *
 * 0		if all the global device path and file system
 *		mount points of zone cluster were validated successfully.
 *
 * 1		if an error occurred.
 *
 * Comments:
 *
 * Individual steps explained below.
 *
 * The 'zc' in svc_zc_fillAndValidateList() means Zone Cluster.
 * It supports the resource created for zone cluster.
 *
 */
int
svc_zc_fillAndValidateList(void)
{
	uint_t i = 0, j = 0;
	const scha_extprop_value_t *gdp = gSvcInfo.gdp_prop_value;
	const scha_extprop_value_t *fmp = gSvcInfo.fmp_prop_value;

	/*
	 * Check if the global devices list AND file system mount
	 * point empty.
	 * If so, return 0.
	 */
	if (!gSvcInfo.count)
		return (0);

	/*
	 * Allocate memory for an array of 'gSvcInfo.count' HAStoragePlusSvc
	 * objects. Each object represents a global device path or a file
	 * system mount point.
	 *
	 */
	gSvcInfo.list = (HAStoragePlusSvc *) calloc(sizeof (HAStoragePlusSvc),
	    gSvcInfo.count);


	if (gSvcInfo.list == NULL) {

		hasp_scds_syslog_0(LOG_ERR, "Failed to allocate memory.");

		return (1);
	}

	/*
	 * Examine the list of global device paths
	 *
	 * Point each global device path string in the 'svcPath'
	 * member.
	 *
	 * Set isMountPoint field to be B_FALSE, since a global
	 * device path can never be a mount point.
	 */

	for (i = 0; i < gdp->val.val_strarray->array_cnt; i++) {

		gSvcInfo.list[i].svcPath = gdp->val.val_strarray->str_array[i];
		gSvcInfo.list[i].isMountPoint = B_FALSE;
	}

	/*
	 * Examine the list of file system mount points.
	 *
	 * Point each file system mount point string
	 * in the 'svcPath' member.
	 *
	 * Set isMountPoint field to be B_TRUE, since this
	 * member is being treated as mount point (at least
	 * for the time being).
	 */
	for (i = gdp->val.val_strarray->array_cnt, j = 0;
	    j < fmp->val.val_strarray->array_cnt; i++, j++) {

		gSvcInfo.list[i].svcPath = fmp->val.val_strarray->str_array[j];
		gSvcInfo.list[i].isMountPoint = B_TRUE;
	}

	/*
	 * Validate the GDP entries.
	 */
	if (svc_zc_validateGDPEntries()) {
		/*
		 * Error message already logged.
		 */
		return (1);
	}

	/*
	 * Validate the FMP entries.
	 */
	if (svc_zc_validateFMPEntries()) {
		/*
		 * Error message already logged.
		 */
		return (1);
	}

	/*
	 * All device service objects ( of types global device paths and
	 * file system mount  points ) are now complete with
	 * mount point, service path/name information. Complete the
	 * remaining DCS validations.
	 *
	 * Each device service is completely validated before
	 * moving to the one.
	 */
	if (svc_validateDeviceServices()) {
		return (1);
	}

	return (0);
}

/*
 * Function: svc_zc_validateGDPEntries
 *
 * Arguments:
 *
 *		None
 * Return:
 *
 * 0		if all the global device paths of zone cluster were
 * 		validated successfully.
 *
 * 1		if an error occurred.
 *
 * Comments:
 *
 * This function validates the each global device path entry in the
 * following steps
 *
 * 1. Obtain the service name of associated global device path
 *    to ensure it is valid in base cluster.
 *
 * 2. Ensure that device is configured to zone cluster.
 */
static int
svc_zc_validateGDPEntries(void)
{
	char		*sername;
	boolean_t	is_mapped;
	uint_t		i;
	char		map_pattern[MAXPATHLEN];
	const scha_extprop_value_t *gdp = gSvcInfo.gdp_prop_value;

	/*
	 * Return success if no GDP values configured.
	 */
	if (gdp->val.val_strarray->array_cnt == 0) {
		return (0);
	}

	for (i = 0; i < gdp->val.val_strarray->array_cnt; i++) {

		/*
		 * Obtain service name associated with global
		 * device path to ensure it is valid in base cluster.
		 */
		if (svc_getGDPSvcName(gSvcInfo.list[i].svcPath, &sername)) {
			/*
			 * Error message logged.
			 */
			return (1);
		}

		scds_syslog_debug(DBG_LEVEL_HIGH,
			"Global service %s is associated"
			" with global device path %s.",
			sername, gSvcInfo.list[i].svcPath);

		if (svc_addStr2Array(sername, &gSvcInfo.list[i].svcName)) {

			return (1);

		}

		/*
		 * Ensure that device is configured to zone cluster.
		 */
		if (svc_zc_checkDeviceMapping(gSvcInfo.list[i].svcPath,
		    &is_mapped, map_pattern)) {
			/*
			 * Error message logged.
			 */
			return (1);
		}

		if (!is_mapped) {
			/*
			 * SCMSGS
			 * @explanation
			 * The specified device is not authorized to
			 * use in the specified zone cluster as they
			 * are not configured to that zone cluster.
			 * @user_action
			 * The operation needs to be re-tried after
			 * the devices associated with specified
			 * global device path is configured to zone cluster.
			 * If the problem persists, contact your
			 * authorized Sun service provider with
			 * /var/adm/messages of all cluster nodes.
			 */
			hasp_scds_syslog_3(LOG_ERR,
			    "There are no device (of pattern %s) "
			    "that is associated with the "
			    "global device path %s that is configured "
			    "to zone cluster %s.",
			    map_pattern,
			    gSvcInfo.list[i].svcPath,
			    gSvcInfo.cl_name);
			return (1);
		}

		scds_syslog_debug(DBG_LEVEL_HIGH,
		    "The device(s) (of pattern %s) associated with "
		    "global device path %s is configured to "
		    "zone cluster %s.",
		    map_pattern,
		    gSvcInfo.list[i].svcPath, gSvcInfo.cl_name);

	}  /* for */

	return (0);
}

/*
 * Function: svc_zc_validateFMPEntries
 *
 * Arguments:
 *
 *		None
 * Return:
 *
 * 0		if all the file system mount points of zone cluster were
 * 		validated successfully.
 *
 * 1		if an error occurred.
 *
 * Comments:
 *
 * This function validates the each file system mount point entry in the
 * following steps
 *
 * 1. Verify that a vfstab entry does exist
 *
 * 2. If the entry does not exist in vfstab, verify in the
 *    zone cluster configuration.
 *
 * 3. Use DCS to translate its special file entry
 *    to obtain the service path.
 */
static int
svc_zc_validateFMPEntries(void)
{
	struct vfstab	vp, vref;
	struct vfstab	*vfsp;
	FILE		*fp = NULL;
	char		*sername;
	boolean_t	mappedfs;
	char		vfstab_file[MAXPATHLEN];
	boolean_t	is_mapped;
	char		buffer[SVC_BUFFER_LEN];
	uint_t		i;
	const scha_extprop_value_t *gdp = gSvcInfo.gdp_prop_value;
	const scha_extprop_value_t *fmp = gSvcInfo.fmp_prop_value;

	/*
	 * Return success if no FMP values configured.
	 */
	if (fmp->val.val_strarray->array_cnt == 0) {
		return (0);
	}

	(void) memset(&vref, 0, sizeof (struct vfstab));

	/*
	 * VFSTAB of the zone cluster has to be opened prior to
	 * verifying vfstab entries.
	 */
	(void) snprintf(vfstab_file, MAXPATHLEN, "%s%s",
	    gSvcInfo.zoneroot, VFSTAB);

	if ((fp = fopen(vfstab_file, "r")) == NULL) {

				    /* CSTYLED */
		int ret = errno;    /*lint !e746 */
		/*
		 * SCMSGS
		 * @explanation
		 * Failed to open the specific file of zone cluster.
		 * @user_action
		 * Check for specific error in the error message and
		 * rectify the problem.
		 */
		hasp_scds_syslog_3(LOG_ERR,
		    "Failed to open %s of zone cluster %s: %s.",
		    VFSTAB,
		    gSvcInfo.cl_name,		/* CSTYLED */
		    strerror(ret));		/*lint !e666 */
		return (1);
	}

	/*
	 * Validate each file system mount point.
	 */
	for (i = gdp->val.val_strarray->array_cnt; i < gSvcInfo.count; i++) {

		int ret;

		/*
		 * vref is a reference vfstab object. Initialize it
		 * to the mount point whose existence we are
		 * trying to verify.
		 */
		vref.vfs_mountp = (char *)gSvcInfo.list[i].svcPath;

		/*
		 * Obtain a filled 'struct vfstab' entry for the
		 * given mount point.
		 */
		ret = getvfsany(fp, &vp, &vref);

		if (ret == 0) {

			/*
			 *  Mount point is present in ZC VFSTAB.
			 */
			scds_syslog_debug(DBG_LEVEL_HIGH,
				"Detected a VFSTAB entry for file "
				"system mount point %s in zone "
				"cluster %s.",
				vref.vfs_mountp, gSvcInfo.cl_name);
			/*
			 * Validate and copy the vfstab entry.
			 */
			if (svc_validateVfsTabEntry(&vp,
			    &gSvcInfo.list[i].vfsEntry)) {
				/*
				 * SCMSGS
				 * @explanation
				 * HAStoragePlus failed while
				 * validating the vfstab entry for the
				 * specified mountpoint.
				 * @user_action
				 * Usually, this happens due to
				 * incorrect entries in vfstab.
				 * Check the log messages for
				 * specified error case and repeat the
				 * operation.
				 */
				hasp_scds_syslog_2(LOG_ERR,
				    "Validation of VFSTAB entry for "
				    "filesystem mount point %s in zone "
				    " cluster %s is failed.",
				    vref.vfs_mountp, gSvcInfo.cl_name);
				return (1);
			}


			gSvcInfo.list[i].isMappedFS = B_FALSE;

		} else if (ret == -1)  {
			struct zc_fstab fstab;

			/*
			 *  Reached end of file. Hence entry for mount
			 *  point not present in ZC VFSTAB.
			 *  This is not fatal as the filesystem may be
			 *  configured to zone cluster. And if the file
			 *  system is not in mapped list of filesystems
			 *  of zone cluster it is fatal.
			 */
			if (svc_zc_checkFSMapping(gSvcInfo.list[i].svcPath,
			    &is_mapped, &fstab)) {
				/*
				 * Error logged.
				 */
				return (1);
			}
			if (!is_mapped) {
				/*
				 * SCMSGS
				 * @explanation
				 * Self explanatory.
				 * @user_action
				 * This happens when user forgets to
				 * keep the file system information in
				 * appropriate location.
				 * Re-try the operation after providing
				 * the filesystem mountpoint
				 * information. If the problem persists,
				 * contact your authorized Sun
				 * service provider.
				 */
				hasp_scds_syslog_3(LOG_ERR,
				    "The filesystem mount point %s "
				    "is neither present in "
				    "VFSTAB of zone cluster %s "
				    "nor configured to zone cluster %s.",
				    gSvcInfo.list[i].svcPath,
				    gSvcInfo.cl_name, gSvcInfo.cl_name);
				return (1);
			}
			if (svc_zc_validateAndCopyMappedFS(
			    &gSvcInfo.list[i].vfsEntry, &fstab)) {
				/*
				 * Error message logged.
				 */
				return (1);
			}
			gSvcInfo.list[i].isMappedFS = B_TRUE;

			scds_syslog_debug(DBG_LEVEL_HIGH,
			    "Detected filesystem mountpoint %s "
			    "configured to zone cluster %s.",
			    gSvcInfo.list[i].svcPath, gSvcInfo.cl_name);
		} else {

			/*
			 * A read error occurred while reading VFSTAB.
			 */
			svc_reportVfsError(ret, vref.vfs_mountp);
			/*
			 * SCMSGS
			 * @explanation
			 * An error occurred while the vfstab entry
			 * for the specified mount point was being read.
			 * @user_action
			 * Verify that the vfstab entry for the mount
			 * point is correct and repeat the operation.
			 */
			hasp_scds_syslog_2(LOG_ERR,
			    "An error occurred while reading VFSTAB "
			    "file for filesystem mount point %s in "
			    "zone cluster %s.",
			    vref.vfs_mountp, gSvcInfo.cl_name);
			return (1);
		}

		mappedfs = gSvcInfo.list[i].isMappedFS;
		/*
		 * Point vfsp to vfstab/fs fields stored information.
		 */
		vfsp = &(gSvcInfo.list[i].vfsEntry);

		/*
		 * Evaluate the 'vfstab/fs' fields for this
		 * mount point. This is to know whether the mount
		 * point is for a  global/local file file system.
		 *
		 */
		ret = svc_evalVfsTabMountOption(vfsp, buffer);

		switch (ret)  {

			case SvcMountOptGlobal :

				/* Global mount point */

				scds_syslog_debug(DBG_LEVEL_HIGH,
					"File system associated with"
					" mount point %s is to be"
					" globally mounted.",
					gSvcInfo.list[i].svcPath);

				gSvcInfo.list[i].isGlobalFS = B_TRUE;

				/*
				 * Global file systems are not supported
				 * for zone cluster.
				 * XXX This check can be removed when the
				 * support of global file systems (PxFS) is
				 * available in zone cluster.
				 */

				/*
				 * SCMSGS
				 * @explanation
				 * Global file systems are not supported
				 * inside zone cluster. Check the release notes
				 * for supported file systems in zone cluster.
				 * @user_action
				 * Modify the configured file system mount
				 * points such that there are no global file
				 * systems and repeat the operation.
				 */
				hasp_scds_syslog_3(LOG_ERR,
				    "File system associated "
				    "with mount point %s %s %s is global. "
				    "Global file systems are not "
				    "supported inside Zone Cluster.",
				    gSvcInfo.list[i].svcPath,
				    (mappedfs ? "configured to" :
				    "in vfstab of"),
				    gSvcInfo.cl_name);
				return (1);

			case SvcMountOptLocal :

				/* Local mount point */

				scds_syslog_debug(DBG_LEVEL_HIGH,
					"File system associated with"
					" mount point %s is to be"
					" locally mounted.",
					gSvcInfo.list[i].svcPath);

				gSvcInfo.list[i].isGlobalFS = B_FALSE;

				/*
				 * A local file system mount point
				 * requires AffinityOn
				 * to be set to TRUE.
				 */

				if (gSvcInfo.affinityOn == B_FALSE) {

					/*
					 * SCMSGS
					 * @explanation
					 * HAStoragePlus detected
					 * that the specified mount
					 * point in /etc/vfstab is a
					 * local mount point, hence
					 * extension property
					 * AffinityOn must be set to
					 * True.
					 * @user_action
					 * Set the AffinityOn
					 * extension property of the
					 * resource to True.
					 */
					hasp_scds_syslog_2(LOG_ERR,
					    "File system associated"
					    " with mount point %s is"
					    " to be locally mounted "
					    " in zone cluster %s."
					    " The AffinityOn value"
					    " cannot be FALSE.",
					    gSvcInfo.list[i].svcPath,
					    gSvcInfo.cl_name);

					return (1);
				}

				/*
				 * AffinityOn being TRUE
				 * is meaningless for scalable
				 * services. Affinity switchovers
				 * are ignored if the RG is
				 * scalable.
				 *
				 * However, local file systems
				 * mount operations do
				 * require AffinityOn = TRUE.
				 *
				 * Hence, local file system mount
				 * points cannot be
				 * specified within the context
				 * of a scalable RG.
				 */

				if (gSvcInfo.rgMode == RGMODE_SCALABLE) {

					/*
					 * SCMSGS
					 * @explanation
					 * HAStoragePlus detected an
					 * inconsistency in its
					 * configuration: the resource
					 * group has been specified as
					 * scalable, hence local file
					 * systems cannot be specified
					 * in the
					 * FilesystemMountPoints
					 * extension property.
					 * @user_action
					 * Correct either the resource
					 * group type or remove the
					 * local file system from the
					 * FilesystemMountPoints
					 * extension property.
					 */
					hasp_scds_syslog_2(LOG_ERR,
					    "File system associated"
					    " with mount point %s is"
					    " to be locally mounted"
					    " in zone cluster %s."
					    " Local file systems"
					    " cannot be specified with"
					    " scalable service"
					    " resources.",
					    gSvcInfo.list[i].svcPath,
					    gSvcInfo.cl_name);

					return (1);
				}

				break;

			case SvcMountOptError :
			default : {

				/*
				 * Incorrect mount options.
				 */

				/*
				 * SCMSGS
				 * @explanation
				 * The information for the
				 * specified mount point is incorrect.
				 * @user_action
				 * Check the entries and edit them
				 * with valid values.
				 */
				hasp_scds_syslog_3(LOG_ERR,
				    "Mount options for file system "
				    "mount point %s is incorrect: %s.",
				    VFSTAB, gSvcInfo.list[i].svcPath,
				    buffer);
				return (1);
			}
		}

		/*
		 * Evaluate the file system check command string for
		 * each individual file system.
		 *
		 * The evaluation is necessary only in the context of
		 * a failover resource group and therefore ignored
		 * for a scalable resource group.
		 */

		if (gSvcInfo.rgMode == RGMODE_FAILOVER) {

			if (svc_evalFSCKWithFS(vfsp,
			    &(gSvcInfo.list[i])) != 0) {

				return (1);
			}
		}

		/*
		 * Determine if special processing is needed for
		 * 'samfs' mount points.
		 */
		if (strcmp(vfsp->vfs_fstype, "samfs") == 0) {

			if (svc_fillSAMFSSvcName(vfsp->vfs_special,
			    &gSvcInfo.list[i])) {
				/*
				 * SCMSGS
				 * @explanation
				 * HAStoragePlus failed to obtain the
				 * global service name(s) for the
				 * specified mount point.
				 * @user_action
				 * Usually this happens when the
				 * devices specified for the mountpoint
				 * are not valid. Check the cluster
				 * configuration. If the problem
				 * persists, contact your
				 * authorized Sun service provider.
				 */
				hasp_scds_syslog_3(LOG_ERR,
				    "Failed to retrieve the global "
				    "service name(s) for mount "
				    "point %s %s zone cluster %s.",
				    vfsp->vfs_mountp,
				    (mappedfs ? "configured to" :
				    "in vfstab of"),
				    gSvcInfo.cl_name);
				return (1);
			}

			/*
			 * Done with special SAMFS processing.
			 * Process next mount point.
			 */
			continue;
		}

		/*
		 * Detect and flag for invalid special device file.
		 */
		if (svc_isDCSDevicePath(vfsp->vfs_special) == B_FALSE) {

			/*
			 * SCMSGS
			 * @explanation
			 * The specified special device path is not
			 * valid.
			 * @user_action
			 * Check the path and correct the entry with
			 * a valid global device path.
			 */
			hasp_scds_syslog_4(LOG_ERR,
			    "Invalid special device path %s detected "
			    "for mount point %s %s zone cluster %s.",
			    vfsp->vfs_special,
			    vfsp->vfs_mountp,
			    (mappedfs ? "configured to" : "in vfstab of"),
			    gSvcInfo.cl_name);
			return (1);
		}

		/*
		 * Obtain device service name of global device path.
		 */
		if (svc_getDCSPathServiceName(vfsp->vfs_special, &sername)) {
			hasp_scds_syslog_3(LOG_ERR,
			    "Failed to retrieve the global "
			    "service name(s) for mount "
			    "point %s %s zone cluster %s.",
			    vfsp->vfs_mountp,
			    (mappedfs ? "configured to" : "in vfstab of"),
			    gSvcInfo.cl_name);
			return (1);
		}

		/*
		 * Duplicate the service name returned by DCS
		 * and set it to svcName.
		 */

		if (svc_addStr2Array(sername, &gSvcInfo.list[i].svcName)) {

			return (1);

		}

		/*
		 * Detect and flag for invalid raw device file.
		 */
		if (svc_isDCSDevicePath(vfsp->vfs_fsckdev) == B_FALSE) {

			/*
			 * SCMSGS
			 * @explanation
			 * The specified fsck device path is not
			 * valid.
			 * @user_action
			 * Check the path and correct the entry with
			 * a valid global device path.
			 */
			hasp_scds_syslog_4(LOG_ERR,
			    "Invalid fsck device path %s detected "
			    "for mount point %s %s zone cluster %s.",
			    vfsp->vfs_fsckdev,
			    vfsp->vfs_mountp,
			    (mappedfs ? "configured to" : "in vfstab of"),
			    gSvcInfo.cl_name);
			return (1);
		}

		/*
		 * Obtain device service name of fsck device file.
		 * The intention here is to validate the fsck device.
		 */
		if (svc_getDCSPathServiceName(vfsp->vfs_fsckdev, &sername)) {
			hasp_scds_syslog_3(LOG_ERR,
			    "Failed to retrieve the global "
			    "service name(s) for mount "
			    "point %s %s zone cluster %s.",
			    vfsp->vfs_mountp,
			    (mappedfs ? "configured to" : "in vfstab of"),
			    gSvcInfo.cl_name);
			return (1);
		}

		/*
		 * The raw and block devices are confirmed as valid
		 * devices.
		 * Now ensure that the raw and block paths kept
		 * in vfstab for the filesystem are configured to zone
		 * cluster for authorized use.
		 */
		if (!mappedfs) {
			/*
			 * Block device mapping check.
			 */
			if (svc_zc_checkDeviceMapping(vp.vfs_special,
			    &is_mapped, NULL)) {
				/* Error message logged. */
				return (1);
			}
			if (!is_mapped) {
				/*
				 * SCMSGS
				 * @explanation
				 * The specified device is not
				 * authorized to use in specified zone
				 * cluster as it are not configured to
				 * zone cluster.
				 * @user_action
				 * The operation needs to be re-tried
				 * after the specified device got
				 * configured to zone cluster.
				 * If the problem persists, contact your
				 * authorized Sun service provider with
				 * /var/adm/messages of cluster nodes.
				 */
				hasp_scds_syslog_3(LOG_ERR,
				    "The special device %s of the "
				    "filesystem mount point %s in "
				    "vfstab of zone cluster %s is not "
				    "mapped.",
				    vp.vfs_special,
				    gSvcInfo.list[i].svcPath,
				    gSvcInfo.cl_name);
				return (1);
			}

			/*
			 * Raw device mapping check.
			 */
			if (svc_zc_checkDeviceMapping(vp.vfs_fsckdev,
			    &is_mapped, NULL)) {
				/* Error message logged. */
				return (1);
			}
			if (!is_mapped) {
				/*
				 * SCMSGS
				 * @explanation
				 * The specified device is not
				 * authorized to use in specified zone
				 * cluster as it are not configured to
				 * zone cluster.
				 * @user_action
				 * The operation needs to be re-tried
				 * after the specified device got
				 * configured to zone cluster.
				 * If the problem persists, contact your
				 * authorized Sun service provider with
				 * /var/adm/messages of cluster nodes.
				 */
				hasp_scds_syslog_3(LOG_ERR,
				    "The device to fsck %s of the "
				    "filesystem mount point %s in "
				    "vfstab of zone cluster %s is not "
				    "mapped.",
				    vp.vfs_fsckdev,
				    gSvcInfo.list[i].svcPath,
				    gSvcInfo.cl_name);
				return (1);
			}
		}

		scds_syslog_debug(DBG_LEVEL_HIGH,
			"Global service %s is associated"
			" with file system mount point %s.",
			sername, gSvcInfo.list[i].svcPath);

		/*
		 * Enable search from the beginning of the VFSTAB file,
		 * to allow the ordering of file systems in
		 * FilesystemMountPoints to be independent of those
		 * specified in the VFSTAB file.
		 *
		 */
		rewind(fp);

	} /* for */

	(void) fclose(fp);

	return (0);
}

/*
 * Function: svc_zc_checkDeviceMapping
 *
 * Arguments:
 *
 * svcPath	The device service path which the mapping status to be checked.
 *
 * is_mapped	The location to store the mapping status
 *
 * map_pattern	The pattern which this function used to check with zone
 * 		cluster configuration.
 *
 * Return:
 * 		0	if mapping status checked successfully.
 * 		1	if an error occurs
 *
 * Comments:
 * This function checks whether the given device is configured to zone cluster.
 */
static int
svc_zc_checkDeviceMapping(const char *svcPath, boolean_t *is_mapped,
    char *map_pattern)
{
	char		*service_class;
	boolean_t	isDCSDevicePath;
	char		rawdev_to_check[MAXPATHLEN];
	char		blkdev_to_check[MAXPATHLEN];
	dc_error_t	dc;
	zc_dochandle_t	zhandle;
	struct zc_devtab devtab;

	/*
	 * Initialize such that device is not configured to zone cluster.
	 */
	*is_mapped = B_FALSE;

	/*
	 * At this point the svcPath is validated such that it is in the from
	 * a) /dev [ DCS complete path]
	 * b) dsk/dX [ DISK type /dev/global/dsk/dX]
	 * c) metcalf [ Is SVM disk setname]
	 */

	/*
	 * Create the device path to test with zone cluster configuration.
	 * The device path should be excluded the '/dev/' part to make use
	 * with fnmatch.
	 */
	isDCSDevicePath = svc_isDCSDevicePath(svcPath);

	if (isDCSDevicePath) {
		/*
		 * The svcPath is of type /dev/md/dg1/dsk/dX,
		 * /dev/vx/dsk/dg1/XXX or /dev/global/dsk/dXsY.
		 */
		(void) snprintf(rawdev_to_check, MAXPATHLEN, "%s", svcPath+5);
		if (map_pattern) {
			(void) sprintf(map_pattern, "%s", svcPath);
		}
	} else {

		const char	*svcName;
		/*
		 * In this case, the svcName is same as svcPath.
		 */
		svcName = svcPath;

		/*
		 * Obtain service class for the device service.
		 * This will be used to formulate the device to test with zone
		 * cluster configuration.
		 */

		dc = dcs_get_service_parameters(
			svcPath, &service_class, NULL, NULL,
			NULL, NULL, NULL);

		if (dc != DCS_SUCCESS) {

			/*
			 * SCMSGS
			 * @explanation
			 * The DCS was not able to obtain the service class
			 * information for the specified global service.
			 * @user_action
			 * Check the cluster configuration. If the
			 * problem persists, contact your authorized
			 * Sun service provider.
			 */
			hasp_scds_syslog_3(LOG_ERR,
			    "Failed to obtain service class information for"
			    " global service %s associated with path"
			    " %s: %s.",
			    svcName,
			    svcPath,			/* CSTYLED */
			    dcs_error_to_string(dc));	/*lint !e666 */
			return (1);
		}

		scds_syslog_debug(DBG_LEVEL_HIGH,
		    "The service class for global service %s associated with "
		    "path %s: %s.",
		    svcName, svcPath, service_class);
		/*
		 * The current code doesn't have provision to enumerate the
		 * volumes associated with given diskset names.
		 * So we form a device path with wild card and check whether
		 * atleast one device is configured to zone cluster.
		 */
		if (STREQ(service_class, HASP_DISK_SVC_CLASS)) {
			/*
			 * The svcPath is of type dsk/dX. So the device path
			 * to test is "global/[r]<svcName>*".
			 */
			(void) snprintf(rawdev_to_check, MAXPATHLEN,
			    "global/r%s*", svcName);
			(void) snprintf(blkdev_to_check, MAXPATHLEN,
			    "global/%s*", svcName);

			if (map_pattern) {
				(void) sprintf(map_pattern,
				    "/dev/global/[r]%s*", svcName);
			}
		} else if (STREQ(service_class, HASP_SVM_SVC_CLASS)) {
			/*
			 * svcPath is of type SVM diskset name. So the device
			 * path to test will be "md/<setname>/[r]dsk/".
			 */
			(void) snprintf(rawdev_to_check, MAXPATHLEN,
			    "md/%s/rdsk/*", svcName);
			(void) snprintf(blkdev_to_check, MAXPATHLEN,
			    "md/%s/dsk/*", svcName);

			if (map_pattern) {
				(void) sprintf(map_pattern,
				    "/dev/md/%s/[r]dsk/", svcName);
			}
		} else {
			/*
			 * Unsupported service class.
			 */
			/*
			 * SCMSGS
			 * @explanation
			 * HAStoragePlus recognized an unsupported
			 * service class for a specified global service.
			 * It supports only the service classes of type
			 * DISK and SUNWmd.
			 * @user_action
			 * Specify global devices of supported type and
			 * repeat the operation. If the problem
			 * persists, contact your authorized Sun
			 * service provider.
			 */
			hasp_scds_syslog_3(LOG_ERR,
			    "Unsupported service class (%s) for global "
			    "service %s associated with path %s.",
			    service_class, svcName, svcPath);
			return (1);
		}
	}

	scds_syslog_debug(DBG_LEVEL_HIGH,
	    "Checking the device paths %s, %s mapping to zone cluster %s.",
	    rawdev_to_check, blkdev_to_check, gSvcInfo.cl_name);

	if (clconf_lib_init() != 0) {
		hasp_scds_syslog_1(LOG_ERR, "INTERNAL ERROR: %s.",
		    "Failed to initialize clconf");
		return (1);
	}

	if ((zhandle = zccfg_init_handle()) == NULL) {
		hasp_scds_syslog_0(LOG_ERR, "Failed to allocate memory.");
		return (1);
	}

	if (zccfg_get_handle(gSvcInfo.cl_name, zhandle) != ZC_OK) {
		hasp_scds_syslog_2(LOG_ERR, "INTERNAL ERROR: %s: %s.",
		    "Failed to get zone handle for zone",
		    gSvcInfo.cl_name);
		zccfg_fini_handle(zhandle);
		return (1);
	}

	if (zccfg_setdevent(zhandle) != ZC_OK) {

		hasp_scds_syslog_2(LOG_ERR, "INTERNAL ERROR: %s: %s.",
		    "Failed to set to device entries for zone",
		    gSvcInfo.cl_name);
		zccfg_fini_handle(zhandle);
		return (1);
	}

	while (zccfg_getdevent(zhandle, &devtab) == ZC_OK) {
		char *m;
		uint_t	len_devprefix = strlen(SVC_DEV_PREFIX);
		m = devtab.zc_dev_match;
		/*
		 * fnmatch gives us simple but powerful shell-style
		 * matching but first, we need to strip out /dev/ from
		 * the matching rule.
		 */
		if (strncmp(m, SVC_DEV_PREFIX, len_devprefix) == 0) {
			m += len_devprefix;
		}
		scds_syslog_debug(DBG_LEVEL_HIGH,
				"The device found in zc %s is: %s.",
				gSvcInfo.cl_name, m);
		if (isDCSDevicePath) {
			if (fnmatch(m, rawdev_to_check, FNM_PATHNAME) == 0) {
				*is_mapped = B_TRUE;
				break;
			}
		} else {
			/*
			 * In case of non DCS Path, we will do normal and
			 * reverse check since the device to test has wild
			 * cards and zone cluster configuration may or
			 * may not with wild cards.
			 * This ensure atleast one volume of setname is mapped.
			 */
			if ((fnmatch(m, rawdev_to_check, FNM_PATHNAME) == 0) ||
			    (fnmatch(rawdev_to_check, m, FNM_PATHNAME) == 0) ||
			    (fnmatch(m, blkdev_to_check, FNM_PATHNAME) == 0) ||
			    (fnmatch(blkdev_to_check, m, FNM_PATHNAME) == 0)) {
				*is_mapped = B_TRUE;
				break;
			}
		}
	}

	(void) zccfg_enddevent(zhandle);
	zccfg_fini_handle(zhandle);

	if (!(*is_mapped)) {
		/*
		 * The mapping of device to the zone cluster doesn't exist.
		 */
		return (0);
	}

	/*
	 * The given global device is configured to zone cluster.
	 * Validate the device path existence in zone cluster.
	 * NOTE: This validation is currently only possible in case
	 * the device path is DCS path.
	 */
	if (isDCSDevicePath) {

		/*
		 * Re-use the device_to_check variable to get complete path
		 * of device under zone cluster node.
		 */
		(void) snprintf(rawdev_to_check, MAXPATHLEN, "%s%s",
		    gSvcInfo.zoneroot, svcPath);

		if (access(rawdev_to_check, F_OK) != 0) {

					    /* CSTYLED */
			int ret = errno;    /*lint !e578 */
			/*
			 * SCMSGS
			 * @explanation
			 * The specified global device path entry does not
			 * exist in the zone cluster even though the device
			 * is configured.
			 * @user_action
			 * Check if the device configuration is done correctly.
			 * Reboot the zone cluster and see whether the problem
			 * is resolved. If the problem persists, contact your
			 * authorized Sun service provider.
			 */
			hasp_scds_syslog_3(LOG_ERR,
			    "The path of mapped global device file %s "
			    "does not exists in zone cluster %s : %s.",
			    svcPath,
			    gSvcInfo.cl_name,		/* CSTYLED */
			    strerror(ret));		/*lint !e666 */

			return (1);
		}
	}

	return (0);
}

/*
 * Function: svc_zc_checkFSMapping
 *
 * Arguments:
 *
 * svcPath	The mount service path which the mapping status to be checked.
 *
 * is_mapped	The location to store the mapping status
 *
 * fstabp	The information of mapped filesystem
 *
 * Return:
 * 		0	if mapping status checked successfully.
 * 		1	if an error occurs
 *
 * Comments:
 * This function checks whether a filesystem mount point is configured to zone
 * cluster or not. If the fs is mapped it also returns the filesystem mapped
 * information.
 */
static int
svc_zc_checkFSMapping(const char *svcPath, boolean_t *is_mapped,
    struct zc_fstab *fstabp)
{
	zc_dochandle_t	zhandle;

	/*
	 * Initialize such that filesystem is not configured to zone cluster.
	 */
	*is_mapped = B_FALSE;

	scds_syslog_debug(DBG_LEVEL_HIGH,
	    "Checking the filesystem mountpoint %s mapping to zone cluster %s.",
	    svcPath, gSvcInfo.cl_name);

	if (clconf_lib_init() != 0) {
		hasp_scds_syslog_1(LOG_ERR, "INTERNAL ERROR: %s.",
		    "Failed to initialize clconf");
		return (1);
	}

	if ((zhandle = zccfg_init_handle()) == NULL) {
		hasp_scds_syslog_0(LOG_ERR, "Failed to allocate memory.");
		return (1);
	}

	if (zccfg_get_handle(gSvcInfo.cl_name, zhandle) != ZC_OK) {
		hasp_scds_syslog_2(LOG_ERR, "INTERNAL ERROR: %s: %s.",
		    "Failed to get zone handle for zone",
		    gSvcInfo.cl_name);
		zccfg_fini_handle(zhandle);
		return (1);
	}

	if (zccfg_setfsent(zhandle) != ZC_OK) {

		hasp_scds_syslog_2(LOG_ERR, "INTERNAL ERROR: %s: %s.",
		    "Failed to set to file system entries for zone",
		    gSvcInfo.cl_name);
		zccfg_fini_handle(zhandle);
		return (1);
	}

	while (zccfg_getfsent(zhandle, fstabp) == ZC_OK) {
		if (strcmp(svcPath, fstabp->zc_fs_dir) == 0) {
			*is_mapped = B_TRUE;
			break;
		}
	}

	(void) zccfg_enddevent(zhandle);
	zccfg_fini_handle(zhandle);

	scds_syslog_debug(DBG_LEVEL_HIGH,
	    "The filesystem mountpoint %s mapping status is : %d",
	    svcPath, *is_mapped);
	return (0);
}

/*
 * Function: svc_zc_validateAndCopyMappedFS
 *
 * Arguments:
 *
 * gsvcvp	The vfstab entry which needs to be populated for mapped FS
 *
 * fstabp	The information of mapped filesystem
 *
 * Return:
 * 		0	if mapped information is valid and copied successfully.
 * 		1	if an error occurs
 *
 * Comments:
 * This function validates the entries of zone fstab. If validation succeeds
 * it copies the values in global HAS+ structure vfstab structure which will
 * be later used during mounting etc.
 */
static int
svc_zc_validateAndCopyMappedFS(struct vfstab *gsvcvp, struct zc_fstab *fstabp)
{
	struct zc_fsopt *fsops;
	char	mntopts[MAX_MNTOPT_STR];
	/*
	 * Validate the entries of zone fstab.
	 * NOTE: The cluster command to map the filesystem already ensures
	 * that 'dir', 'special', 'type' are not NULL.
	 */
	/*
	 * Validate the raw device existence. The raw device in case of samfs
	 * filesystem could be NULL.
	 */
	if ((!STREQ(fstabp->zc_fs_type, "samfs")) &&
	    (strlen(fstabp->zc_fs_raw) == 0)) {
		/*
		 * SCMSGS
		 * @explanation
		 * The 'raw device to fsck' field is missing from filesystem
		 * mapping information for the specified mount point.
		 * @user_action
		 * Specify the 'raw device to fsck' field to the mapping
		 * information for specified mount point and repeat the
		 * operation.
		 */
		hasp_scds_syslog_2(LOG_ERR,
		    "The raw device to fsck is not specified in mapped "
		    "filesystem information for mount point %s "
		    "in zone cluster %s.",
		    fstabp->zc_fs_dir, gSvcInfo.cl_name);
		return (1);
	}

	/*
	 * Prepare the mount options string.
	 */
	mntopts[0] = '\0';
	for (fsops = fstabp->zc_fs_options; fsops != NULL;
	    fsops = fsops->zc_fsopt_next) {
		(void) strlcat(mntopts, fsops->zc_fsopt_opt, MAX_MNTOPT_STR);
		if (fsops->zc_fsopt_next) {
			(void) strlcat(mntopts, ",", MAX_MNTOPT_STR);
		}
	}


	scds_syslog_debug(DBG_LEVEL_HIGH, "The filesystem mountpoint mapped "
	    "information of %s in zone cluster %s : dir(%s) special(%s) "
	    "type(%s) raw(%s) options(%s).",
	    fstabp->zc_fs_dir, gSvcInfo.cl_name,
	    fstabp->zc_fs_dir, fstabp->zc_fs_special,
	    fstabp->zc_fs_type, fstabp->zc_fs_raw, mntopts);
	/*
	 * Copy the fstab information in global HAS+ structure.
	 * NOTE: The zc_fstab will not have 'fsckpass' and 'mount at boot'
	 * values.
	 */
	if (((gsvcvp->vfs_mountp = strdup(fstabp->zc_fs_dir)) == NULL) ||
	    ((gsvcvp->vfs_special = strdup(fstabp->zc_fs_special)) == NULL) ||
	    ((gsvcvp->vfs_fstype = strdup(fstabp->zc_fs_type)) == NULL) ||
	    ((strlen(fstabp->zc_fs_raw) != 0) &&
	    ((gsvcvp->vfs_fsckdev = strdup(fstabp->zc_fs_raw)) == NULL)) ||
	    ((strlen(mntopts) != 0) &&
	    ((gsvcvp->vfs_mntopts = strdup(mntopts)) == NULL))) {
		hasp_scds_syslog_0(LOG_ERR, "Failed to allocate memory.");
		return (1);
	}

	gsvcvp->vfs_fsckpass = NULL;
	gsvcvp->vfs_automnt = NULL;

	return (0);
}
#endif

/*
 * Function: svc_validateVfsTabEntry
 *
 * Arguments:
 *
 * vp		The pointer to vfstab entry which has to be validated.
 *
 * gsvcvp	The pointer to global HAS+ vfstab to store after validation.
 *
 * Returns:
 *
 * 0		If validation (and also storing) is successful.
 *
 * 1		If any failure.
 *
 * Comments:
 * This function does possible validation of the vfstab entries. And also
 * stores in global HAS+ structure which can be used later during mounting etc.
 * This avoids re-read vfstab entry information.
 *
 * NOTE:
 * The mount point entry in the vfstab needs not to be checked.
 */

int
svc_validateVfsTabEntry(const struct vfstab *vp, struct vfstab *gsvcvp)
{
	/* Validate mount device. */
	if (vp->vfs_special == NULL) {
		/*
		 * SCMSGS
		 * @explanation
		 * The 'device to mount' field is missing from the vfstab
		 * entry for the specified mount point.
		 * @user_action
		 * Add the 'device to mount' field to the vfstab entry for the
		 * specified mount point and repeat the operation.
		 */
		hasp_scds_syslog_1(LOG_ERR,
		    "The device to mount is not specified in vfstab "
		    "for mount point : %s.", vp->vfs_mountp);
		return (1);
	}

	/* Validate file system type */
	if (vp->vfs_fstype == NULL) {
		/*
		 * SCMSGS
		 * @explanation
		 * The 'file system type' field in the vfstab entry for the
		 * specified mount point does not specify the type of file
		 * system.
		 * @user_action
		 * Add the type of file system to the 'file system type' field
		 * in the vfstab entry for the specified mount point and
		 * repeat the operation.
		 */
		hasp_scds_syslog_1(LOG_ERR,
		    "The file system type is not specified in vfstab "
		    "for mount point : %s.", vp->vfs_mountp);
		return (1);
	}

	/* Validate fsckdev entry */
	/* fsckdev entry is NULL for samfs file system */
	if ((!STREQ(vp->vfs_fstype, "samfs")) && vp->vfs_fsckdev == NULL) {
		/*
		 * SCMSGS
		 * @explanation
		 * The 'device to fsck' field is missing from the vfstab entry
		 * for the specified mount point.
		 * @user_action
		 * Add the 'device to fsck' field to the vfstab entry for the
		 * specified mount point and repeat the operation.
		 */
		hasp_scds_syslog_1(LOG_ERR,
		    "The raw device to fsck is not specified in vfstab "
		    "for mount point : %s.", vp->vfs_mountp);
		return (1);
	}

	/* The remaining entries fsckpass, automnt, mntopts could be NULL */

	gsvcvp->vfs_fsckpass = NULL;
	gsvcvp->vfs_automnt = NULL;
	gsvcvp->vfs_mntopts = NULL;
	gsvcvp->vfs_fsckdev = NULL;

	/*
	 * Copy the vfstab entry to HAS+ global structure.
	 */

	if (((gsvcvp->vfs_mountp = strdup(vp->vfs_mountp)) == NULL) ||
	    ((gsvcvp->vfs_special = strdup(vp->vfs_special)) == NULL) ||
	    ((gsvcvp->vfs_fstype = strdup(vp->vfs_fstype)) == NULL) ||
	    (vp->vfs_fsckdev &&
	    ((gsvcvp->vfs_fsckdev = strdup(vp->vfs_fsckdev)) == NULL)) ||
	    (vp->vfs_fsckpass &&
	    ((gsvcvp->vfs_fsckpass = strdup(vp->vfs_fsckpass)) == NULL)) ||
	    (vp->vfs_automnt &&
	    ((gsvcvp->vfs_automnt = strdup(vp->vfs_automnt)) == NULL)) ||
	    (vp->vfs_mntopts &&
	    ((gsvcvp->vfs_mntopts = strdup(vp->vfs_mntopts)) == NULL))) {

		hasp_scds_syslog_0(LOG_ERR, "Failed to allocate memory.");
		return (1);
	}
	return (0);
}

/*
 * Function: svc_reportVfsError
 *
 * Arguments:
 *
 * err_val	The error value occurred while reading vfstab.
 *
 * mnt_point	The mount point for which vfstab being read.
 *
 * Returns: void
 *
 * Comments:
 * This functions reports the error occurred while reading vfstab for an entry.
 */

void
svc_reportVfsError(int err_val, char *mnt_point)
{
	switch (err_val) {

		case VFS_TOOLONG :
			/*
			 * SCMSGS
			 * @explanation
			 * The internal buffer size of the vfstab entry for
			 * the specified mount point has exceeded
			 * VFS_LINE_MAX characters.
			 * @user_action
			 * Shorten the vfstab entry for that mount point so
			 * that the length of the entry does not exceed
			 * VFS_LINE_MAX characters. You can shorten the vfstab
			 * entry by reducing the length of, for example, the
			 * block device, character device, or mount point.
			 */
			hasp_scds_syslog_2(LOG_ERR,
			    "The entry in vfstab for mount point "
			    "'%s' exceeds %d characters.",
			    mnt_point, VFS_LINE_MAX);
			break;
		case VFS_TOOMANY:
			/*
			 * SCMSGS
			 * @explanation
			 * The entry in vfstab for the specified mount point
			 * has more fields than are defined for a single
			 * vfstab entry.
			 * @user_action
			 * Remove the extraneous fields from the entry in
			 * vfstab for the mount point and repeat the
			 * operation.
			 */
			hasp_scds_syslog_1(LOG_ERR,
			    "The entry in vfstab for mount point "
			    "'%s' has too many fields.", mnt_point);
			break;
		case VFS_TOOFEW:
			/*
			 * SCMSGS
			 * @explanation
			 * The entry in vfstab for the specified mount point
			 * has fewer fields than are defined for a single
			 * vfstab entry.
			 * @user_action
			 * Add the missing fields from the entry in vfstab for
			 * the mount point and repeat the operation.
			 */
			hasp_scds_syslog_1(LOG_ERR,
			    "The entry in vfstab for mount point "
			    "'%s' has too few fields.", mnt_point);
			break;
		default:
			/*
			 * SCMSGS
			 * @explanation
			 * An error occurred while the vfstab entry for the
			 * specified mount point was being read.
			 * @user_action
			 * Verify that the vfstab entry for the mount point is
			 * correct and repeat the operation. If the problem
			 * persists, contact your authorized Sun service
			 * provider to determine whether a workaround or patch
			 * is available.
			 */
			hasp_scds_syslog_1(LOG_ERR,
			    "Error in the vfstab entry for "
			    "mount point '%s'.", mnt_point);
			break;
	}
}

/*
 * Function: svc_reportMntError
 *
 * Arguments:
 *
 * err_val	The error value occurred while reading mnttab.
 *
 * mnt_point	The mount point for which mnttab being read.
 *
 * Returns: void
 *
 * Comments:
 * This functions reports the error occurred while reading mnttab for an entry.
 */

void
svc_reportMntError(int err_val, char *mnt_point)
{
	switch (err_val) {
		case MNT_TOOLONG :
			/*
			 * SCMSGS
			 * @explanation
			 * The internal buffer size of the mnttab entry for
			 * the specified mount point has exceeded
			 * MNT_LINE_MAX characters.
			 * @user_action
			 * Contact your authorized Sun service provider to
			 * determine whether a workaround or patch is
			 * available.
			 */
			hasp_scds_syslog_2(LOG_ERR,
			    "The entry in mnttab for mount point "
			    "'%s' exceeds %d characters.",
			    mnt_point, MNT_LINE_MAX);
			break;
		case MNT_TOOMANY:
			/*
			 * SCMSGS
			 * @explanation
			 * The entry in mnttab for the specified mount point
			 * has more fields than are defined for a single
			 * mnttab entry.
			 * @user_action
			 * Contact your authorized Sun service provider to
			 * determine whether a workaround or patch is
			 * available.
			 */
			hasp_scds_syslog_1(LOG_ERR,
			    "The entry in mnttab for mount point "
			    "'%s' has too many fields.", mnt_point);
			break;
		case MNT_TOOFEW:
			/*
			 * SCMSGS
			 * @explanation
			 * The entry in mnttab for the specified mount point
			 * has fewer fields than are defined for a single
			 * mnttab entry.
			 * @user_action
			 * Contact your authorized Sun service provider to
			 * determine whether a workaround or patch is
			 * available.
			 */
			hasp_scds_syslog_1(LOG_ERR,
			    "The entry in mnttab for mount point "
			    "'%s' has too few fields.", mnt_point);
			break;
		default:
			/*
			 * SCMSGS
			 * @explanation
			 * An error occurred while the mnttab entry for the
			 * specified mount point was being read.
			 * @user_action
			 * Contact your authorized Sun service provider to
			 * determine whether a workaround or patch is
			 * available.
			 */
			hasp_scds_syslog_1(LOG_ERR,
			    "Error in the mnttab entry for "
			    "mount point '%s'.", mnt_point);
			break;
	}
}

/*
 * Structure used for parallelization of device switchovers.
 */

typedef struct dsentry {
	uint_t i;	/* index to service path name in gSvcInfo */
	uint_t j;	/* index to service name to above path name */
	dc_error_t dc;	/* status of switchover operation */
} dsentry_t;

/*
 * Function: startDeviceService_thread
 *
 * Arguments:	arg	Pointer to the thread pool data.
 *
 * Return:	NULL
 *
 * Comment:	This function defines a working thread which tries to bring
 *		the device service available (i.e to SC_STATE_ONLINE/
 *		SC_STATE_DEGRADED state) by performing startup/switchover on
 *		device service.
 *
 *		The device service availability and the node which has to be
 *		primary for that service depends on HAStoragePlus configuration
 *		and is as follows ...
 *		i)If configured in failover rg with affinityOn=true, the
 *		device service has to be available and primaried on the node
 *		where resource group has to be primaried (i.e on node where
 *		the prenet_start is invoked by RGM).
 *		ii)If configured in failover rg with affinityOn=false, the
 *		device service has to be available and can be primaried on any
 *		node in the cluster.
 *		iii)If configured in scalable rg, the device service has to be
 *		available and can be primaried on any node in the cluster.
 *
 */

void
startDeviceService_thread(void * arg)
{
	dsentry_t *dsvcinfo = (dsentry_t *)arg;
	uint_t i, j, k;
	int is_susp;
	dc_replica_status_seq_t *act_rep;
	sc_state_code_t state;
	dc_replica_seq_t *nodes_seq;
	int msg_cnt;
	nodeid_t repl_nodeid;
	uint_t repl_prefid;
	boolean_t service_avail;


	/*
	 * Obtain the indexes of the service.
	 */

	i = dsvcinfo->i;
	j = dsvcinfo->j;

	/*
	 * In case the resource group is scalable or failover (with
	 * affinityOn=false), check the device service status and
	 * make available if not (No need to bother where it is
	 * primaried).
	 *
	 * NOTE:
	 * There is a possibility of multiple device service switchovers
	 * since DCS startup API is invoked by multiple nodes. To avoid
	 * this all nodes will start the device service on node which
	 * is having highest preference replica(i.e lowest number).
	 * Here the assumption is that DCS API dcs_startup_service()
	 * with positive node ID as parameter is idempotent
	 * (i.e multiple invocations should cause service start on that
	 * node without multiple switchovers).
	 */

	if ((gSvcInfo.rgMode == RGMODE_SCALABLE) ||
		(gSvcInfo.affinityOn == B_FALSE)) {

		service_avail = B_FALSE;

		msg_cnt = 0;

		scds_syslog_debug(DBG_LEVEL_HIGH, "Checking "
			"availability of global device service %s "
			"associated with path %s .",
			gSvcInfo.list[i].svcName.str_array[j],
			gSvcInfo.list[i].svcPath);

		while (service_avail == B_FALSE) {
			dsvcinfo->dc = dcs_get_service_status(
			    gSvcInfo.list[i].svcName.str_array[j],
			    &is_susp, &act_rep, &state, NULL);

			if (dsvcinfo->dc != DCS_SUCCESS) {

				/*
				 * The error examining and reporting
				 * is done later for all services
				 */
				break;
			}

			switch (state) {

			    case SC_STATE_OFFLINE:
				/*
				 * Get all the replicas of the service.
				 */
				dsvcinfo->dc = dcs_get_service_parameters(
				    gSvcInfo.list[i].svcName.str_array[j],
				    NULL, NULL, &nodes_seq, NULL, NULL, NULL);

				if (dsvcinfo->dc != DCS_SUCCESS) {

				    service_avail = B_TRUE;
				    break;
				}

				ASSERT(nodes_seq->count > 0);

				/*
				 * Find the node ID having replica with
				 * highest preference(i.e lowest id)
				 */

				repl_nodeid = nodes_seq->replicas[0].id;
				repl_prefid = nodes_seq->replicas[0].preference;

				for (k = 1; k < nodes_seq->count; k++) {
				    if (repl_prefid >
				    nodes_seq->replicas[k].preference) {
					repl_prefid =
					    nodes_seq->replicas[k].preference;
					repl_nodeid = nodes_seq->replicas[k].id;
				    }
				}

				scds_syslog_debug(DBG_LEVEL_HIGH,
				    "Global device service %s "
				    "associated with %s is SC_STATE_OFFLINE. "
				    "Startup on service is initiated.",
				    gSvcInfo.list[i].svcName.str_array[j],
				    gSvcInfo.list[i].svcPath);

				dsvcinfo->dc = dcs_startup_service(
				    gSvcInfo.list[i].svcName.str_array[j],
				    repl_nodeid);

				/*
				 * Mark service as available and return.
				 * The service availability is
				 * checked again during verification.
				 */

				service_avail = B_TRUE;
				break;

			    case SC_STATE_WAIT:

				/*
				 * Device service is in transition,
				 * check the status again after sleep
				 */
				msg_cnt++;
				if (msg_cnt %
				HASTORAGEPLUS_RETRIES_BEFORE_WARNING
				== 0) {

					/*
					 * SCMSGS
					 * @explanation
					 * The specified global service was in
					 * between states.
					 * @user_action
					 * This is an informational message,
					 * no user action is needed.
					 */
					hasp_scds_syslog_2(LOG_WARNING,
					    "Device service %s"
					    " associated with path %s"
					    " is in transition state.",
					    gSvcInfo.list[i].\
						svcName.str_array[j],
					    gSvcInfo.list[i].svcPath);

					msg_cnt = 0;

				}
				(void) sleep(HASTORAGEPLUS_WAIT);
				break;
			    case SC_STATE_ONLINE:
			    case SC_STATE_DEGRADED:
			    case SC_STATE_FAULTED:
			    case SC_STATE_UNKNOWN:
			    case SC_STATE_NOT_MONITORED:
			    default:
				scds_syslog_debug(DBG_LEVEL_HIGH,
				    "Global device service %s status "
				    "associated with %s is %d.",
				    gSvcInfo.list[i].svcName.\
				    str_array[j],
				    gSvcInfo.list[i].svcPath, state);
				/*
				 * Not bothering other state values as
				 * HASP intention to ensure that
				 * service shouldn't be in SC_STATE_OFFLINE
				 */
				service_avail = B_TRUE;
				break;
			} /* End of switch */

		} /* End of while */

	} else {

		/*
		 * For failover resource group (with affinityOn=true), start
		 * the device service on the local node (i.e where the
		 * prenet_start invoked) because HASP requires this node to
		 * be primary for the service.
		 */

		scds_syslog_debug(DBG_LEVEL_HIGH, "Device switchover "
			"of global service %s associated with path %s "
			"initiated to this node.",
			gSvcInfo.list[i].svcName.str_array[j],
			gSvcInfo.list[i].svcPath);

		dsvcinfo->dc = dcs_startup_service(
			gSvcInfo.list[i].svcName.str_array[j], gSvcInfo.nodeId);
	}

}

/*
 *
 * Function: svc_startDeviceServices
 *
 * Arguments:  	none
 *
 * Return:
 *
 * 0	All devices services are successfully available
 *
 * 1	If an internal error occurs.
 *
 * Comments:
 *		HAStoragePlus has to ensure the availability of all device
 *		services specified in GDP and FMP extension properties to
 *		perform necessary fsck/mount operations. It is the job of HAS+
 *		to bring the service online if it is offline. (see RFE 6209791)
 *
 * Note:
 *
 */
int
svc_startDeviceServices(void)
{

	int rc = 0;
	uint_t i, j;
	dsentry_t *dslist;
	uint_t	dscnt = 0;

	/*
	 * Initialize the device service list by finding number of services.
	 */

	for (i = 0; i < gSvcInfo.count; i++)
		dscnt += gSvcInfo.list[i].svcName.array_cnt;

	dslist = (dsentry_t *)malloc(dscnt * sizeof (dsentry_t));

	if (! dslist) {

		hasp_scds_syslog_0(LOG_ERR, "Failed to allocate memory.");
		rc = 1;
		goto out;

	}

	dscnt = 0;

	for (i = 0; i < gSvcInfo.count; i++) {
		for (j = 0; j < gSvcInfo.list[i].svcName.array_cnt; j++) {

			dslist[dscnt].i = i;
			dslist[dscnt].j = j;
			dslist[dscnt].dc = (dc_error_t)0;
			dscnt++;

		}
	}

	/* If we are all SQFS, return success */
	if (dscnt == 0) {
		rc = 0;
		goto out;
	}

	/*
	 * Add device switchover tasks to the threadpool.
	 */

	for (i = 0; i < dscnt; i++) {

		(void) svc_addTasktoThreadPool(startDeviceService_thread,
		    (void *)&dslist[i]);
	}

	/*
	 * Wait for completion of device switchovers.
	 */

	svc_wait4tasksinThreadPool();

	/*
	 * Examine output of the device service start.
	 */
	for (i = 0; i < dscnt; i++) {

		uint_t _i = dslist[i].i;
		uint_t _j = dslist[i].j;
		dc_error_t dc = dslist[i].dc;

		if (dc != DCS_SUCCESS) {

			/*
			 * Device service start failed i.e.
			 * affinity switchover to local node
			 * ( gSvcInfo.nodeId  ) failed.
			 *
			 * Continue instead of returning with an
			 * error. HAStorage implementation does
			 * the same.
			 * Do not log an error message here as it might be
			 * confusing.
			 * The proper status message of this service will be
			 * reported during svc_verifyDeviceServices().
			 */

			rc = 1;

			/*
			 * Enter the name of the failing device to
			 * failed_switchover string, so that it get processed
			 * by hastorageplus_update.
			 */
			ADD_TO_FAILED_SWITCHOVERS(
			    gSvcInfo.list[_i].svcName.str_array[_j]);
		} else {

			/*
			 * Device service started on local
			 * node. Continue.
			 */
			scds_syslog_debug(DBG_LEVEL_HIGH,
			    "Device switchover of global service"
			    " %s associated with path %s"
			    " to this node succeeded.",
			    gSvcInfo.list[_i].svcName.str_array[_j],
			    gSvcInfo.list[_i].svcPath);

		}

	}

	if (rc == 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * Self explanatory.
		 * @user_action
		 * This is an informational message, no user action is needed.
		 */
		hasp_scds_syslog_0(LOG_INFO,
		    "All global device services successfully switched over to"
		    " this node.");
	}

out:
	if (dslist)
		free(dslist);

	return (rc);

}

/*
 *
 * Function: svc_verifyDeviceServices
 *
 * Arguments:
 *
 * Return:
 *
 * 0	all device services verified as	being available.
 *
 * 1	an error occurred.
 *
 * Comments:
 *
 * Function returns only if one of the following condition
 * are satisfied.
 *
 * a. All device services verified to be available. Each service is
 * repeatedly checked in a sleep wait loop till it is
 * verified as available.
 *
 * b. Any service is found to be suspended ( error ).
 *
 * c. dcs_service_status() returned an error.
 *
 */

int
svc_verifyDeviceServices(void)
{
	int status = 0;

	if (gSvcInfo.count == 0)
		return (status);

	switch (gSvcInfo.rgMode) {
	case RGMODE_FAILOVER:
		if (gSvcInfo.affinityOn == B_TRUE)
			status = svc_verifyAffinityOn();
		else
			status = svc_verifyAffinityOff();

		break;
	case RGMODE_SCALABLE:
		status = svc_verifyAffinityOff();

		break;
	case RGMODE_NONE:
	default:
		/*
		 * SCMSGS
		 * @explanation
		 * The RG_mode of the specified resource group is neither
		 * Failover or Scalable.
		 * @user_action
		 * Contact your authorized Sun service provider to determine
		 * whether a workaround or patch is available.
		 */
		hasp_scds_syslog_1(LOG_ERR,
		    "An unknown value for the RG_mode property was found for "
		    "resource group %s.",
		    gSvcInfo.rgName);
		status = 1;
		break;

	}
	return (status);
}

/*
 *
 * Function: svc_verifyAffinityOn
 *
 * Arguments:
 *
 * Return:
 *
 * 0    all device services verified as
 *      being available and primaried on
 * 	current node.
 *
 * 1    an error occurred.
 *
 * Comments:
 *
 * This function should be called only if AffinityOn=TRUE.
 *
 * Function returns only if one of the following condition are
 * satisfied.
 *
 * a. All device services verified to be available and primaried on
 * the current node. Each service is repeatedly checked in a sleep
 * wait loop till a primary replica exists for the current node.
 *
 * b. Any service is found to be suspended ( error ).
 *
 * c. dcs_service_status() returned an error.
 *
 * d. If the service replica for the current node was not a primary or
 * was not in transition.
 *
 */

int
svc_verifyAffinityOn(void)
{

	uint_t i, j, k;
	dc_error_t dc;
	dc_replica_status_seq_t *act_rep;
	int is_susp;
	sc_state_code_t state;
	int msg_cnt = 0;

	for (i = 0; i < gSvcInfo.count; i++) {
		for (j = 0; j < gSvcInfo.list[i].svcName.array_cnt; j++) {

			/*
			 * Assume that none of the
			 * device services are
			 * initially available.
			 */
			boolean_t avail = B_FALSE;

			while (avail == B_FALSE) {

				scds_syslog_debug(DBG_LEVEL_HIGH,
				"Obtaining the status of global "
				" service %s associated with path "
				" %s ",
				gSvcInfo.list[i].svcName.str_array[j],
				gSvcInfo.list[i].svcPath);

				dc = dcs_get_service_status(
					gSvcInfo.list[i].svcName.str_array[j],
					&is_susp, &act_rep, &state, NULL);

				if (dc != DCS_SUCCESS) {

					(void) scha_resource_setstatus_zone(
					    gSvcInfo.rsName, gSvcInfo.rgName,
					    gSvcInfo.cl_name,
					    SCHA_RSSTATUS_FAULTED, "");

					/*
					 * SCMSGS
					 * @explanation
					 * The DCS was not able to obtain the
					 * status of the specified global
					 * service.
					 * @user_action
					 * Check the cluster configuration. If
					 * the problem persists, contact your
					 * authorized Sun service provider.
					 */
					hasp_scds_syslog_3(LOG_ERR,
					    "Failed to obtain the status of"
					    " global service %s associated"
					    " with path %s: %s.",
					    gSvcInfo.list[i].svcName.\
						str_array[j],
					    gSvcInfo.list[i].svcPath,
								/* CSTYLED */
				    dcs_error_to_string(dc));	/*lint !e666 */

					return (1);

				}

				if (is_susp != 0) {

					/*
					 * HAStorage sets the resource to
					 * be in a faulted state. Hence
					 * this is done here. A Scha
					 * handle is not valid here.
					 *
					 */
					(void) scha_resource_setstatus_zone(
					    gSvcInfo.rsName, gSvcInfo.rgName,
					    gSvcInfo.cl_name,
					    SCHA_RSSTATUS_FAULTED, "");

					hasp_scds_syslog_2(LOG_ERR,
					    "Global service %s associated with"
					    " path %s is found to be in"
					    " maintenance state.",
					    gSvcInfo.list[i].svcName.\
						str_array[j],
					    gSvcInfo.list[i].svcPath);

					return (1);

				}

				/*
				 * Ensure that active replicas did get returned
				 * from the dcs function call.
				 *
				 */
				if (act_rep == NULL) {

					/*
					 * SCMSGS
					 * @explanation
					 * The DCS did not find any
					 * information on the specified global
					 * service.
					 * @user_action
					 * Check the global service
					 * configuration.
					 */
					hasp_scds_syslog_1(LOG_ERR,
					    "No active replicas where detected"
					    " for the global service %s.",
					    gSvcInfo.list[i].svcName.\
						str_array[j]);

					return (1);
				}


				/*
				 * Find the replica associated with that node
				 */
				for (k = 0; k < (uint_t)act_rep->count; k++) {
					if (gSvcInfo.nodeId ==
					(act_rep->replicas)[k].id)
						break;
				}

				/*
				 * This condition is true if there is no
				 * replica associated with that node.
				 */
				if (k == (uint_t)act_rep->count) {

					hasp_scds_syslog_2(LOG_ERR,
					    "Global service %s associated with"
					    " path %s is found to be in"
					    " maintenance state.",
					    gSvcInfo.list[i].svcName.\
						str_array[j],
					    gSvcInfo.list[i].svcPath);

					dcs_free_dc_replica_status_seq(act_rep);

					return (1);

				}

				switch ((act_rep->replicas)[k].state) {
				case PRIMARY:
					avail = B_TRUE;
					break;

				case SPARE:
					/* FALLTHRU */
				case SECONDARY:
					/* FALLTHRU */
				case INACTIVE:

					/*
					 * SCMSGS
					 * @explanation
					 * HAStoragePlus was not able to
					 * switchover the specified global
					 * service to the primary node.
					 * @user_action
					 * Check the cluster configuration. If
					 * the problem persists, contact your
					 * authorized Sun service provider.
					 */
					hasp_scds_syslog_3(LOG_ERR,
					    "Global service %s associated"
					    " with path %s is unable to become"
					    " a primary on node %d.",
					    gSvcInfo.list[i].svcName.\
						str_array[j],
					    gSvcInfo.list[i].svcPath,
					    gSvcInfo.nodeId);

					dcs_free_dc_replica_status_seq(act_rep);

					return (1);

				case TRANSITION:
					break;

				default:
					break;
				}

				/*
				 * We loop until the replica is available
				 * while logging a a warning message.
				 */

				if (avail == B_TRUE) {

					scds_syslog_debug(DBG_LEVEL_HIGH,
					"Global service %s associated with"
					"path %s is now available and made "
					"primary on node %d.",
					gSvcInfo.list[i].svcName.str_array[j],
					gSvcInfo.list[i].svcPath,
					gSvcInfo.nodeId);

				} else {

					/*
					 * Decide whether to log a syslog
					 * warning about service
					 * unavailability.
					 */
					if (msg_cnt %
					HASTORAGEPLUS_RETRIES_BEFORE_WARNING
					== 0) {

						/*
						 * SCMSGS
						 * @explanation
						 * Self explanatory.
						 * @user_action
						 * This is an informational
						 * message, no user action is
						 * needed.
						 */
						hasp_scds_syslog_2(LOG_WARNING,
						    "Global service %s"
						    " associated with path %s"
						    " is unavailable."
						    " Retrying...",
						    gSvcInfo.list[i].\
							svcName.str_array[j],
						    gSvcInfo.list[i].svcPath);

						msg_cnt = 0;

					}

					/*
					 * Sleep before re-verifying
					 * service availability.
					 */
					(void) sleep(HASTORAGEPLUS_WAIT);
					msg_cnt++;

				}

				dcs_free_dc_replica_status_seq(act_rep);

			} /* while */

		} /* for (j) */
	} /* for (i) */

	return (0);

}

/*
 *
 * Function: svc_verifyAffinityOff
 *
 * Arguments:
 *
 * Return:
 *
 * 0    all device services verified as
 *      being available.
 *
 * 1    an error occurred.
 *
 * Comments:
 *
 * This function should be called only if AffinityOn=OFF or if we
 * don't care about the AffinityOn settings.
 *
 * Function returns only if one of the following condition are
 * satisfied.
 *
 * a. All device services verified to be available. Each service is
 * repeatedly checked in a sleep wait loop till a primary replica
 * exists some where (current node or some other cluster node).
 *
 * b. Any service is found to be suspended ( error ).
 *
 * c. dcs_service_status() returned an error.
 *
 */

int
svc_verifyAffinityOff(void)
{

	uint_t i, j, k;
	dc_error_t dc;
	dc_replica_status_seq_t *act_rep;
	int is_susp;
	sc_state_code_t state;
	int msg_cnt = 0;

	for (i = 0; i < gSvcInfo.count; i++) {
		for (j = 0; j < gSvcInfo.list[i].svcName.array_cnt; j++) {

			/*
			 * Assume that none of the
			 * device services are
			 * initially available.
			 */
			boolean_t avail = B_FALSE;

			while (avail == B_FALSE) {

				scds_syslog_debug(DBG_LEVEL_HIGH,
				"Obtaining the status of global "
				" service %s associated with path "
				" %s ",
				gSvcInfo.list[i].svcName.str_array[j],
				gSvcInfo.list[i].svcPath);

				dc = dcs_get_service_status(
				gSvcInfo.list[i].svcName.str_array[j],
				&is_susp, &act_rep, &state, NULL);

				if (dc != DCS_SUCCESS) {

					(void) scha_resource_setstatus_zone(
					    gSvcInfo.rsName, gSvcInfo.rgName,
					    gSvcInfo.cl_name,
					    SCHA_RSSTATUS_FAULTED, "");

					hasp_scds_syslog_3(LOG_ERR,
					    "Failed to obtain the status of"
					    " global service %s associated"
					    " with path %s: %s.",
					    gSvcInfo.list[i].svcName.\
						str_array[j],
					    gSvcInfo.list[i].svcPath,
								/* CSTYLED */
				    dcs_error_to_string(dc));	/*lint !e666 */

					return (1);

				}

				if (is_susp != 0) {


					/*
					 * HAStorage sets the resource to
					 * be in a faulted state. Hence
					 * this is done here. A Scha
					 * handle is not valid here.
					 *
					 */
					(void) scha_resource_setstatus_zone(
					    gSvcInfo.rsName, gSvcInfo.rgName,
					    gSvcInfo.cl_name,
					    SCHA_RSSTATUS_FAULTED, "");

					hasp_scds_syslog_2(LOG_ERR,
					    "Global service %s associated with"
					    " path %s is found to be in"
					    " maintenance state.",
					    gSvcInfo.list[i].svcName.\
						str_array[j],
					    gSvcInfo.list[i].svcPath);

					return (1);

				}

				/*
				 * Ensure that active replicas did get returned
				 * from the dcs function call.
				 *
				 */
				if (act_rep == NULL) {

					/*
					 * SCMSGS
					 * @explanation
					 * The DCS was not able to find any
					 * replica for the specified global
					 * service.
					 * @user_action
					 * Check the cluster configuration. If
					 * the problem persists, contact your
					 * authorized Sun service provider.
					 */
					hasp_scds_syslog_1(LOG_ERR,
					    "No active replicas for the"
					    " service %s.",
					    gSvcInfo.list[i].svcName.\
						str_array[j]);

					return (1);
				}

				/*
				 * Examine replica in the replica sequence till
				 * the primary is found.
				 */
				for (k = 0; k < (uint_t)act_rep->count &&
					avail == B_FALSE; k++) {

					switch ((act_rep->replicas)[k].state) {

					case PRIMARY:
						avail = B_TRUE;
						break;

					/*
					 * Not primaried yet...
					 */
					case SECONDARY:
					case SPARE:
					case INACTIVE:
					case TRANSITION:
						break;

					}

				}

				/*
				 * We loop until the replica is available.
				 * A warning message is logged in the
				 * meantime.
				 */
				if (avail == B_TRUE) {

					scds_syslog_debug(DBG_LEVEL_HIGH,
					"Global service %s associated with"
					" path %s is now available.",
					gSvcInfo.list[i].svcName.str_array[j],
					gSvcInfo.list[i].svcPath);

				} else {

					/*
					 * Decide whether to log a syslog
					 * warning about service unavailability.
					 */
					if (msg_cnt %
					HASTORAGEPLUS_RETRIES_BEFORE_WARNING
					== 0) {

						hasp_scds_syslog_2(LOG_WARNING,
						    "Global service %s"
						    " associated with path %s"
						    " is unavailable."
						    " Retrying...",
						    gSvcInfo.list[i].\
							svcName.str_array[j],
						    gSvcInfo.list[i].svcPath);

						msg_cnt = 0;

					}

					/*
					 * Sleep before re-verifying
					 * service availability.
					 */
					(void) sleep(HASTORAGEPLUS_WAIT);
					msg_cnt++;

				}

				dcs_free_dc_replica_status_seq(act_rep);
			} /* while */

		} /* for (j) */
	} /* for (i) */

	return (0);

}

/*
 *
 * Function: svc_fillNodeId
 *
 * Arguments: none
 *
 *
 * Return:
 *
 * 0  if the RGM node id is read and stored in gSvcInfo.nodeID
 *
 * 1  if an error occurred.
 *
 * Comments:
 *
 * scha API's are used here since there are no equivalent
 * DSDL API's for obtaining node ids.
 *
 */
int
svc_fillNodeId(void)
{

	scha_cluster_t handle;

	int rc;

retry_cluster_get:

	rc = scha_cluster_open_zone(gSvcInfo.cl_name, &handle);


	if (rc != SCHA_ERR_NOERR) {

		hasp_scds_syslog_1(LOG_ERR,
		    "Failed to retrieve the cluster handle: %s.",
					    /* CSTYLED */
		    scds_error_string(rc)); /*lint !e666 */

		return (1);

	}

	rc = scha_cluster_get_zone(gSvcInfo.cl_name, handle,
	    SCHA_NODEID_LOCAL, &gSvcInfo.nodeId);

	if (rc == SCHA_ERR_SEQID) {

		hasp_scds_syslog_0(LOG_INFO,
		    "Failed to retrieve the cluster information. Retrying...");

		(void) scha_cluster_close(handle);

		goto retry_cluster_get;

	} else if (rc != SCHA_ERR_NOERR) {

		hasp_scds_syslog_2(LOG_ERR,
		    "Failed to retrieve information on %s: %s.",
		    SCHA_NODEID_LOCAL,		/* CSTYLED */
		    scds_error_string(rc));	/*lint !e666 */

		(void) scha_cluster_close(handle);

		return (1);

	}

	/*
	 * Discard the handle, since scha calls are not
	 * going to be made anymore.
	 */

	(void) scha_cluster_close(handle);


	return (0);

}

/*
 *
 * Function: svc_evalVfsTabMountOption
 *
 * Arguments:
 *
 * const struct vfstab *vp	vfstab entry
 *
 * char *buffer		Explanation  if
 *			an incorrect vfstab
 *			entry was detected.
 *
 *
 * Return:
 *
 * SvcMountOptLocal	vfstab entry represents a local file
 *			system
 *
 * SvcMountOptGlobal	vfstab entry represents a global file
 *			system
 *
 * SvcMountOptErr 	error i.e incorrect vfstab
 * 			entry.
 *			'buffer' contains an explanation.
 *
 * Comments:
 *
 * A global file system is identified as one where the keyword
 * "global" is present in the vp->vfs_mountopts character string.
 *
 * A local file system is one where vp->vfs_automnt character string
 * = "no" i.e. it cannot be mounted at boot time AND either of the
 * following is true -
 *
 * (a) vp->vfs_mntopts character string contains the substring
 * "noglobal"
 *
 * (b) The substring "noglobal" is altogether absent.
 * This includes the case when the mount option field contains
 * a '-' character i.e. signifying that defaults are to be used.
 *
 * Other cases are treated as an error.
 *
 * Note: Case insensitive searchs are performed. As in the case
 * with HAStorage, white spaces are not "eaten" prior to string
 * comparisons.
 *
 */
int
svc_evalVfsTabMountOption(const struct vfstab *vp, char *buffer)
{

	int tmpvar = SvcMountOptError;
	char tmpbuf[VFS_LINE_MAX]; /* buffer large enough to handle mntopts */

	buffer[0] = '\0';	/* null terminate it */

	/*
	 * strstr() performs a string match.
	 */
	if (vp->vfs_mntopts == NULL) {

		/*
		 * A '-' is specified for the mount
		 * option.
		 *
		 */
		tmpvar = SvcMountOptLocal;

	} else {
		/*
		 * mntopts may be of the forms:
		 *  - PATTERN
		 *  - <something>,PATTERN
		 *  - PATTERN,<something>
		 *  - <something>,PATTERN,<something>
		 * By encapsulating it in commas, mntopts will be of the form:
		 * - <something or nil>,PATTERN,<something or nil>
		 */
		tmpbuf[0] = ',';
		(void) strcpy(tmpbuf+1, vp->vfs_mntopts);	/* CSTYLED */
		tmpbuf[strlen(tmpbuf)+1] = '\0';		/*lint !e661 */
						/* mntopts < vfstab line max */
		tmpbuf[strlen(tmpbuf)] = ',';

		if (strstr(tmpbuf, ",m,")) {

			/*
			 * 'm' option is forbidden.
			 */
			(void) strcpy(buffer, "File system mount point should "
				"not specify the 'm' mount option");
			return (SvcMountOptError);

		}

		/* return SvcMountOptGlobal for PxFS and sQFS mounts */
		if (strstr(tmpbuf, ",global,") || strstr(tmpbuf, ",shared,"))
			tmpvar = SvcMountOptGlobal;
		else
			tmpvar = SvcMountOptLocal;

	}

	/*
	 * At last look at the 'mount at boot' option.
	 */
	if ((tmpvar == SvcMountOptLocal) && (vp->vfs_automnt != NULL) &&
	    (strcmp(vp->vfs_automnt, "no"))) {

		/*
		 * Incorrect vfstab entry.
		 */

		(void) strcpy(buffer, "File system mount point should specify "
			"'mount at boot' as 'no'");

		return (SvcMountOptError);

	}

	return (tmpvar);
}

/*
 * Function: svc_evalMntTabMountOption
 *
 * Arguments:
 *
 * mp		mnttab entry.
 *
 * buffer	Explanation  if	an incorrect mnttab entry was detected.
 *
 *
 * Return:
 *
 * SvcMountOptLocal	mnttab entry represents a local file
 *			system
 *
 * SvcMountLofs		mnttab entry represents a loopback mount
 *
 * SvcMountOptGlobal	mnttab entry represents a global file
 *			system
 *
 * SvcMountOptErr 	error i.e incorrect mnttab
 * 			entry.
 *			'buffer' contains an explanation.
 */

static int
svc_evalMntTabMountOption(const struct mnttab *mp, char *buffer)
{

	int	tmpvar = SvcMountOptError;
	char	tmpbuf[MNT_LINE_MAX]; /* buffer to handle mntopts */

	buffer[0] = '\0';	/* null terminate it */

	if (mp->mnt_mntopts == NULL) {

		/*
		 * A '-' is specified for the mount option.
		 */

		tmpvar = SvcMountOptLocal;

	} else {

		/*
		 * mntopts may be of the forms:
		 *  - PATTERN
		 *  - <something>,PATTERN
		 *  - PATTERN,<something>
		 *  - <something>,PATTERN,<something>
		 * By encapsulating it in commas, mntopts will be of the form:
		 * - <something or nil>,PATTERN,<something or nil>
		 */

		tmpbuf[0] = ',';
		(void) strcpy(tmpbuf+1, mp->mnt_mntopts);	/* CSTYLED */
		tmpbuf[strlen(tmpbuf)+1] = '\0';		/*lint !e661 */
		tmpbuf[strlen(tmpbuf)] = ',';

		if (strstr(tmpbuf, ",global,")) {
			tmpvar = SvcMountOptGlobal;
		} else {
			if (strcmp(mp->mnt_fstype, "lofs") == 0)
				tmpvar = SvcMountLofs;
			else
				tmpvar = SvcMountOptLocal;
		}
	}
	return (tmpvar);
}


/*
 *
 * Function: svc_fillAndCheckFSCKCmd
 *
 * Arguments:
 *
 * const char *fsck_command
 *
 * Return:
 *
 *	0	gSvcInfo.fsckCmd stores either
 *
 *		the default RTR value i.e.
 *		the NULL string, or
 *
 *		the "do nothing" sentinel
 *		value, or
 *
 *		the user specified file system
 *		check command.
 *		In this case, the user specified
 *		command string is checked further.
 *
 *	1	An error occurred.
 *
 * Comments:
 *
 *
 */
int
svc_fillAndCheckFSCKCmd(const char *fsck_cmd)
{

	if (STREQ(fsck_cmd, SvcNullString)) {

		gSvcInfo.fsckCmd = SvcNullString;

		scds_syslog_debug(DBG_LEVEL_HIGH,
		    "The FilesystemCheckCommand extension property is set to"
		    " NULL.", gSvcInfo.fsckCmd);


	} else if (STREQ(fsck_cmd, HASTORAGEPLUS_NO_FSCK)) {

		gSvcInfo.fsckCmd = HASTORAGEPLUS_NO_FSCK;

		scds_syslog_debug(DBG_LEVEL_HIGH,
		    "The FilesystemCheckCommand extension property is set to"
		    " %s.Hence no file system checks will be performed.",
		    gSvcInfo.fsckCmd);

	} else {

		/*
		 * A user specified FilesystemCheckCommand
		 * string.
		 *
		 */

		if (gSvcInfo.rgMode == RGMODE_SCALABLE) {


			/*
			 * SCMSGS
			 * @explanation
			 * Self explanatory.
			 * @user_action
			 * This is an informational message, no user action is
			 * needed.
			 */
			hasp_scds_syslog_2(LOG_WARNING,
			    "The FilesystemCheckCommand extension property is"
			    " set to %s. File system checks will however be"
			    " skipped because the resource group %s is"
			    " scalable.", fsck_cmd, gSvcInfo.rgName);

			gSvcInfo.fsckCmd = SvcNullString;

			/*
			 * In case of a scalable resource group,
			 * we are not checking for the existence
			 * of the the command, executability etc.
			 */

		} else {

			/*
			 * The RG is a failover one.
			 *
			 */


			/*
			 * Keep a copy separate from the DSDL
			 * version.
			 */
			if ((gSvcInfo.fsckCmd = strdup(fsck_cmd))
				== NULL) {

				hasp_scds_syslog_0(LOG_ERR,
				    "Failed to allocate memory.");

				return (1);

			}

			/*
			 * SCMSGS
			 * @explanation
			 * Self explanatory.
			 * @user_action
			 * This is an informational message, no user action is
			 * needed.
			 */
			hasp_scds_syslog_1(LOG_INFO,
			    "The FilesystemCheckCommand extension property is"
			    " set to %s.", gSvcInfo.fsckCmd);

			/*
			 * Check this user specified
			 * command string for correctness.
			 *
			 * Note: We know for sure that
			 * the value is not SvcNullString,
			 * or HASTORAGEPLUS_NO_FSCK.
			 *
			 */
			if (svc_checkFSCKCmdPath() != 0) {
				return (1);
			}

		}



	}


	return (0);

}

/*
 * Function: svc_evalFSCKWithFS
 *
 * Arguments:
 *
 * struct vfstab *vp		vfstab entry for a FS.
 *
 * HAStoragePlusSvc *dsvc 	device service
 *
 * Return:
 *
 *	0		Log warning in case of an
 *			inappropriate mismatch between the
 *			file system and the FS check command
 *			string.
 *
 *			No errors are returned.
 *
 *
 * Comments:
 *
 *	This function compares the file system check command
 *	with a given mount point's file system type. The
 *	objective to only log warning messages in case of
 *	"abnormal" file system check command settings.
 *
 *
 *	For example, a "do nothing" directive for a UFS
 *	file system is considered abnormal - potentially
 *	impacting high availability.
 *
 *	PxFS			File system check compulsory.
 *				Any command string can be specified.
 *
 *	UFS			File system check compulsory.
 *				Any command string can be specified.
 *
 *	VxFS			File system check compulsory.
 *				Any command string can be specified.
 *
 *	SAMFS/QFS		No file system check allowed.
 *
 *	Other			Specify one of the following -
 *				(a) Default command string, or
 *				(b) Skip file system check, or
 *				(c) Use the user specified command
 *				    string.
 *
 * Note:
 *
 * File systems (local/global) are never checked, mounted or
 * unmounted in the context of scalable resource groups.
 *
 * This function should never be called in case the RG is
 * scalable.
 *
 */
int
svc_evalFSCKWithFS(const struct vfstab *vp, const HAStoragePlusSvc *dsvc)
{
	if (dsvc->isGlobalFS) {

		/*
		 * A global file system.
		 */
		if (STREQ(gSvcInfo.fsckCmd, HASTORAGEPLUS_NO_FSCK)) {

			/*
			 * SCMSGS
			 * @explanation
			 * The FilesystemCheckCommand has been specified as
			 * '/bin/true'. This means that no file system check
			 * will be performed on the specified file system.
			 * This is not advised.
			 * @user_action
			 * This is an informational message, no user action is
			 * needed. However, it is recommended to make HA
			 * Storage Plus check the file system upon switchover
			 * or failover, in order to avoid possible file system
			 * inconsistencies.
			 */
			hasp_scds_syslog_1(LOG_WARNING,
			    "File system checking is disabled for global file"
			    " system %s.", dsvc->svcPath);

		}

		return (0);

	} else if (STREQ(vp->vfs_fstype, "ufs")) {

			if (STREQ(gSvcInfo.fsckCmd, HASTORAGEPLUS_NO_FSCK)) {

				/*
				 * SCMSGS
				 * @explanation
				 * The FilesystemCheckCommand has been
				 * specified as '/bin/true'. This means that
				 * no file system check will be performed on
				 * the specified file system of the specified
				 * type. This is not advised.
				 * @user_action
				 * This is an informational message, no user
				 * action is needed. However, it is
				 * recommended to make HAStoragePlus check
				 * the file system upon switchover or
				 * failover, in order to avoid possible file
				 * system inconsistencies.
				 */
				hasp_scds_syslog_2(LOG_WARNING,
				    "File system checking is disabled for %s"
				    " file system %s.", "UFS", dsvc->svcPath);

			}

	} else if (STREQ(vp->vfs_fstype, "vxfs")) {

			if (STREQ(gSvcInfo.fsckCmd, HASTORAGEPLUS_NO_FSCK)) {

				hasp_scds_syslog_2(LOG_WARNING,
				    "File system checking is disabled for %s"
				    " file system %s.", "VxFS", dsvc->svcPath);
			}

	} else if (STREQ(vp->vfs_fstype, "samfs")) {

		if (STREQ(gSvcInfo.fsckCmd, HASTORAGEPLUS_NO_FSCK)) {

			hasp_scds_syslog_2(LOG_INFO,
			    "File system checking is disabled for %s file"
			    " system %s.", "SAM-FS", dsvc->svcPath);

		} else {

			/*
			 * SCMSGS
			 * @explanation
			 * Self explanatory.
			 * @user_action
			 * This is an informational message, no user action is
			 * needed.
			 */
			hasp_scds_syslog_2(LOG_WARNING,
			    "File system checking is enabled for %s file"
			    " system %s.", "SAM-FS", dsvc->svcPath);
		}

	} else {

		/*
		 * An unrecognized file system.
		 *
		 */
		if (STREQ(gSvcInfo.fsckCmd, SvcNullString)) {

			/*
			 * The FilesystemCheckCommand has the
			 * default RTR value i.e. NULL string.
			 */

			scds_syslog_debug(DBG_LEVEL_HIGH,
			"File system %s of type %s will be checked "
			" using %s\n", dsvc->svcPath, vp->vfs_fstype,
			HASTORAGEPLUS_U_FSCK);

		} else  {

			/*
			 * The default value has been overridden.
			 *
			 */

			scds_syslog_debug(DBG_LEVEL_HIGH,
			"File system %s of type %s will be checked "
			" using %s\n", dsvc->svcPath, vp->vfs_fstype,
			gSvcInfo.fsckCmd);

		}


	}

	return (0);

}

/*
 * Function: svc_checkFSCKCmdPath
 *
 * Arguments: none
 *
 *
 * Return:
 *
 *	0	If the file system check command string
 *		is "valid".
 *
 *	1	If an error occurred.
 *
 * Comments:
 *
 *	A valid file system check command string is one where
 *	the first parsed substring (the substring could be
 *	the entire string) is (a) accessible i.e. present
 *	(b) Executable by its owner.
 *
 *	The file system check command string is parsed into
 * 	substrings. The first substring is expected to be a
 *	valid executable (binary/script). Specifically, it
 *	should exist and be capable of being executed by its
 *	owner. Of course, the substring and the entire
 *	command string could be identical.
 *
 *	Some examples are (a) "/usr/sbin/fsck -o p"
 *			  (b) "/usr/sbin/fsck"
 *			  (c) "/<directory(s)>/<executable>"
 *
 * Note:
 *	(1) Only absolute pathnames are valid for the
 *	first substring.
 *	(2) White spaces are assumed to be spaces and
 *	tabs.
 */
int
svc_checkFSCKCmdPath(void)
{

	char *substr;
	char *fsck_cmd;

	/*
	 * White spaces specification -
	 * spaces, new line, tab, feed.
	 *
	 */
	char *separator = " \r\n\t";

	struct stat statbuf;

	/*
	 *  This function should never be called
	 *  in case gSvcInfo.fsckCmd is NULL.
	 */
	ASSERT(gSvcInfo.fsckCmd != NULL);

	if ((fsck_cmd = strdup(gSvcInfo.fsckCmd)) == NULL) {
		hasp_scds_syslog_0(LOG_ERR, "Failed to allocate memory.");
		return (1);
	}

	/*
	 * We are using the non re-entrant version
	 * namely the strtok function.
	 *
	 * Note: strtok() has the side effect of
	 * modifying fsck_cmd i.e.
	 * inserting a '\0' after the first token.
	 * This is OK because we never use the whole
	 * string for command line execution within
	 * this private program i.e. we are interested
	 * only in the command sub string part.
	 */
	substr = strtok(fsck_cmd, separator);

	if (substr == NULL) {

		if (*(fsck_cmd) != '/') {

			/*
			 * Relative pathnames not allowed. Return an
			 * error.
			 */
			/*
			 * SCMSGS
			 * @explanation
			 * Self explanatory.
			 * @user_action
			 * Specify an absolute path name to your command in
			 * the FilesystemCheckCommand extension property.
			 */
			hasp_scds_syslog_1(LOG_ERR,
			    "The path name %s associated with the"
			    " FilesystemCheckCommand extension property is"
			    " detected to be a relative path name. Only"
			    " absolute path names are allowed.",
			    fsck_cmd);

			return (1);

		}

		/*
		 * For code simplicity.
		 */
		substr = fsck_cmd;

	} else {


		if (*substr != '/') {

			/*
			 * Relative pathnames not allowed. Return an
			 * error.
			 */

			/*
			 * SCMSGS
			 * @explanation
			 * Self explanatory. For security reasons, this is not
			 * allowed.
			 * @user_action
			 * Correct the FilesystemCheckCommand extension
			 * property by specifying an absolute path name.
			 */
			hasp_scds_syslog_1(LOG_ERR,
			    "The path name %s associated with the"
			    " FilesystemCheckCommand extension property is"
			    " detected to be a relative pathname. Only"
			    " absolute path names are allowed.",
			    fsck_cmd);

			return (1);

		}

	}

	scds_syslog_debug(DBG_LEVEL_HIGH,
	"The path name %s associated with the "
	"FilesystemCheckCommand extension property "
	" is detected to be an absolute path name.",
	fsck_cmd);

	/*
	 * Ensure that substr is accessible.
	 */

	if (stat(substr, &statbuf) != 0) {

		int ret = errno;
		/*
		 * SCMSGS
		 * @explanation
		 * Self explanatory.
		 * @user_action
		 * Check and correct the rights of the specified filename by
		 * using the chown/chmod commands.
		 */
		hasp_scds_syslog_2(LOG_ERR,
		    "Unable to access the executable %s associated with the"
		    " FilesystemCheckCommand extension property: %s.",
		    substr,		/* CSTYLED */
		    strerror(ret));	/*lint !e666 */

		return (1);

	}

	/*
	 * Check that FilesystemCheckCommand value is indeed
	 * a regular file. The stat() call translates a link
	 * to the regular file. All other file types are
	 * treated as errors.
	 *
	 */

	if (S_ISREG(statbuf.st_mode)) {

		scds_syslog_debug(DBG_LEVEL_HIGH,
		"The path name %s associated with the "
		"FilesystemCheckCommand extension property "
		" is detected to be a regular file.",
		fsck_cmd);

	} else  {

		/*
		 * SCMSGS
		 * @explanation
		 * HAStoragePlus found that the specified file was not a
		 * plain file, but of different type (directory, symbolic
		 * link, etc.).
		 * @user_action
		 * Correct the value of the FilesystemCheckCommand extension
		 * property by specifying a regular executable.
		 */
		hasp_scds_syslog_1(LOG_ERR,
		    "The path name %s associated with the"
		    " FilesystemCheckCommand extension property is not a"
		    " regular file.", fsck_cmd);

		return (1);

	}

	/*
	 * Check that the script/binary is executable
	 * by the owner.
	 */

	if ((statbuf.st_mode & S_IXUSR) != S_IXUSR) {

		int ret = errno;
		/*
		 * SCMSGS
		 * @explanation
		 * The specified executable associated with the
		 * FilesystemCheckCommand extension property is not owned by
		 * user "root" or is not executable.
		 * @user_action
		 * Correct the rights of the filename by using the chmod/chown
		 * commands.
		 */
		hasp_scds_syslog_2(LOG_ERR,
		    "Incorrect permissions detected for the executable %s"
		    " associated with the FilesystemCheckCommand extension"
		    " property: %s.", substr,	/* CSTYLED */
		    strerror(ret));		/*lint !e666 */

		exit(1);

	}

	return (0);

}

/*
 *
 * Function: svc_SAMFSVols
 *
 * Arguments:
 *
 * char *samfs	SAMFS device special file to query
 *
 * char **rp	Buffer containing the list of constituent
 *		devices (volumes) for the 'samfs' device.
 *		Memory for the character buffer will be
 *		allocated in this function.
 *
 * Return:
 *
 * 0	    'rp' buffer contains a list of devices (volumes).
 *	    The buffer is guaranteed to be null terminated.
 *
 * 1	    if an error occurred.
 *
 * Comments:
 *      This function will fill an array to contain all constituent
 *      volumes associated with a single SAMFS mount point
 *
 * Note:
 *	The existence/execution check for SvcSamfsExec is done
 *	only once i.e. the first time svc_SAMFSVols is called.
 *
 */
static int
svc_SAMFSVols(const char *samfs, char **rp)
{
	FILE *qfp;
	char *tfile;
	char *cp, cmdline[256];
	struct stat statbuf;

	static boolean_t isFileExec = B_FALSE;

	if (isFileExec == B_FALSE) {

		/*
		 * Ensure that SvcSamfsExec is accessible.
		 */

		if (stat(SvcSamfsExec, &statbuf) != 0) {

			int ret = errno;
			/*
			 * SCMSGS
			 * @explanation
			 * Self explanatory.
			 * @user_action
			 * Check and correct the rights of the specified
			 * filename by using the chown/chmod commands.
			 */
			hasp_scds_syslog_2(LOG_ERR,
			    "Unable to access the executable %s: %s.",
			    SvcSamfsExec,	/* CSTYLED */
			    strerror(ret));	/*lint !e666 */

			return (1);

		}

		/*
		 * Check that SvcSamfsExec is indeed
		 * a regular file. The stat() call translates a link
		 * to the regular file. All other file types are
		 * treated as errors.
		 */

		if (S_ISREG(statbuf.st_mode)) {

			scds_syslog_debug(DBG_LEVEL_HIGH,
			"Executable %s is a regular file.",
			SvcSamfsExec);

		} else  {

			/*
			 * SCMSGS
			 * @explanation
			 * HAStoragePlus found that the specified file was
			 * not a plain file but of different type (directory,
			 * symbolic link, etc.).
			 * @user_action
			 * Usually, this means that an incorrect filename was
			 * given in one of the extension properties.
			 */
			hasp_scds_syslog_1(LOG_ERR,
			    "Executable %s is not a regular file.",
			    SvcSamfsExec);

			return (1);

		}

		/*
		 * Check that the script/binary is executable
		 * by the owner.
		 */

		if ((statbuf.st_mode & S_IXUSR) != S_IXUSR) {

			int ret = errno;
			/*
			 * SCMSGS
			 * @explanation
			 * The specified executable is not owned by user
			 * "root" or is not executable.
			 * @user_action
			 * Correct the rights of the filename by using the
			 * chmod/chown commands.
			 */
			hasp_scds_syslog_2(LOG_ERR,
			    "Incorrect permissions detected for the executable"
			    " %s: %s.", SvcSamfsExec,	/* CSTYLED */
			    strerror(ret));		/*lint !e666 */

			exit(1);

		}

		isFileExec = B_TRUE;
	}

	/*
	 * Allocate a temporary filename
	 * for HAStoragePlus_samfs results
	 */

	if ((tfile = tmpnam(NULL)) == NULL) {

		/*
		 * SCMSGS
		 * @explanation
		 * HAStoragePlus was not able to create a temporary filename
		 * in /var/cluster/run.
		 * @user_action
		 * Check that permissions are right for directory
		 * /var/cluster/run. If the problem persists, contact your
		 * authorized Sun service provider.
		 */
		hasp_scds_syslog_0(LOG_ERR,
		    "Failed to create a temporary filename.");

		return (1);
	}

	scds_syslog_debug(DBG_LEVEL_HIGH,
		"%s is the temporary file name.", tfile);

	/*
	 * Construct and invoke HAStoragePlus_samfs command line
	 * It determines if this mount point is known by samfs
	 * If so, obtain a list of its constituent volumes
	 */
	(void) sprintf(cmdline,
		SvcSamfsExec" %s 1> %s",
		samfs, tfile);

	scds_syslog_debug(DBG_LEVEL_HIGH,
		"About to execute command %s.", cmdline);

	if (system(cmdline) != 0) {

		int ret = errno;
		/*
		 * SCMSGS
		 * @explanation
		 * HAStoragePlus was not able to determine the volumes that
		 * are part of this SAM-FS file system.
		 * @user_action
		 * Check the SAM-FS file system configuration. If the problem
		 * persists, contact your authorized Sun service provider.
		 */
		hasp_scds_syslog_2(LOG_ERR,
		    "Failed to obtain SAM-FS constituent volumes from"
		    " mount point %s: %s.", samfs,	/* CSTYLED */
		    strerror(ret));			/*lint !e666 */

		return (1);
	}

	/*
	 * Open resulting file and populate results with the constituent volumes
	 */
	if ((qfp = fopen(tfile, "r")) == NULL) {

		int ret = errno;
		/*
		 * SCMSGS
		 * @explanation
		 * HAStoragePlus was not able to open the specified volume
		 * that is part of the SAM-FS file system.
		 * @user_action
		 * Check the SAM-FS file system configuration. If the problem
		 * persists, contact your authorized Sun service provider.
		 */
		hasp_scds_syslog_2(LOG_ERR,
		    "Failed to open SAM-FS volume file %s: %s.",
		    tfile,		/* CSTYLED */
		    strerror(ret));	/*lint !e666 */

		(void) remove(tfile);

		return (1);
	}

	/*
	 * Allocate a return buffer
	 */
	if ((*rp = (char *)malloc(SvcReadSize)) == NULL) {

		hasp_scds_syslog_0(LOG_ERR, "Failed to allocate memory.");

		return (1);
	}

	/*
	 * Read in comma delimited lines, until EOF.
	 * NOTE: The KornShell may have limited the line length from the above
	 *	 system call to 2048 characters. The extranious line termination
	 *	 will need to replaced with a single comma.
	 * A	 fgets will write '\0' if the file is completely empty.
	 */
	for (cp = *rp; fgets(cp, SvcReadSize, qfp); cp = *rp + strlen(*rp)) {

		/*
		 * Replace line termination, with comma
		 */
		while ((cp = strpbrk(cp, "\n")) != NULL)
			*cp = ',';

		/*
		 * Prepare for next read of SvcReadSize
		 */
		*rp = (char *)realloc(*rp, strlen(*rp) + SvcReadSize);

		if (*rp == NULL) {

			hasp_scds_syslog_0(LOG_ERR,
			    "Failed to allocate memory.");

			return (1);
		}
	}

	/*
	 * Remove trailing comma, if present
	 */
	if ((strlen(*rp)) && (cp[-1] == ','))
		cp[-1] = '\0';

	(void) fclose(qfp);

	(void) remove(tfile);

	/*
	 * Return an error if the buffer is empty.
	 */
	return (strlen(*rp) == 0);

}

/*
 * Function: svc_addStr2Array
 *
 * Arguments:
 *
 * const char *str 	string
 *
 * scha_strarray_t	string container terminated by NULL
 *
 * Return:
 *
 *	0 	if the addition succeeded.
 *
 *	1	if an error occurred.
 *
 * Comments:
 *
 * A copy of the string is made after which it is added to
 * the string array. The scha_strarray_t's internal array
 * of character pointers is realloc'ed as a part of this
 * addition.
 *
 */
int
svc_addStr2Array(const char *str, scha_str_array_t *sa)
{
	char *str_copy, **ptr;

	ASSERT(str != NULL);
	ASSERT(sa != NULL);

	str_copy = strdup(str);

	if (str_copy == NULL) {

		hasp_scds_syslog_0(LOG_ERR, "Failed to allocate memory.");
		return (1);

	}

	if (sa->array_cnt == 0) {

		ptr = (char **)malloc(2*(sizeof (char *)));
		if (ptr == NULL) {
			hasp_scds_syslog_0(LOG_ERR,
			    "Failed to allocate memory.");
			return (1);
		}

		ptr[sa->array_cnt++] = str_copy;
		ptr[sa->array_cnt] = NULL;

		sa->str_array = ptr;

	} else {

		/*
		 * Increase the str_array's number of elements by
		 * one, to store an additional character string.
		 */
		ptr = (char **)realloc(sa->str_array,
			(sa->array_cnt + 2)*(sizeof (char *)));

		if (ptr == NULL) {

			hasp_scds_syslog_0(LOG_ERR,
			    "Failed to allocate memory.");
			return (1);
		}

		ptr[sa->array_cnt++] = str_copy;
		ptr[sa->array_cnt] = NULL;

		sa->str_array = ptr;
	}

	return (0);

}

/*
 * Function: svc_releaseStr2Array
 *
 * Arguments:
 *
 * scha_strarray_t	string container terminated by NULL
 *
 * Return:	void
 *
 * Comments:
 *
 * The memory allocated to scha_strarray_t's internal array is released.
 */
void
svc_releaseStr2Array(const scha_str_array_t *sa)
{
	uint_t	i;

	ASSERT(sa != NULL);

	/* Release memory allocated for individual strings */
	for (i = 0; i < sa->array_cnt; i++) {
		free(sa->str_array[i]);
	}

	/* Release memory allocated to store string pointers */
	if (sa->str_array != NULL) {
		free(sa->str_array);
	}
}

#define	STRBUF_SIZE	BUFSIZ

/*
 * Function: svc_addStr2Strbuf
 *
 * Arguments:
 *
 * const char *str	string
 *
 * strbuf		string buffer container
 *
 * Return:
 *
 *	0	if append succeeded.
 *
 *	1	if an error occurred.
 *
 * Comments:
 *
 * This function appends the string to the existing string buffer.
 * Initially STRBUF_SIZE bytes memory is allocated to string buffer, and it is
 * dynamically extended by STRBUF_SIZE bytes whenever needed.
 */

int
svc_addStr2Strbuf(hasp_strbuf_t *strbuf, const char *str) {

	char *ptr;

	if (str == NULL)
		return (0);

	/*
	 * Check if this is the first string to be added in string buffer.
	 */

	if (strbuf->str == NULL) {

		strbuf->str = (char *)malloc(STRBUF_SIZE*sizeof (char));

		if (strbuf->str == NULL) {

			hasp_scds_syslog_0(LOG_ERR,
			    "Failed to allocate memory.");

			return (1);
		}

		strbuf->str_capacity = STRBUF_SIZE;
		strbuf->str_length = 0;
		strbuf->str[0] = '\0';
	}

	/*
	 * Check the buffer for sufficient memory to copy the string.
	 */

	if ((strbuf->str_length + strlen(str) + 1) > strbuf->str_capacity) {

		/*
		 * Not having sufficient memory, extend the buffer capacity.
		 */

		ptr = (char *)realloc(strbuf->str,
			(strbuf->str_capacity + STRBUF_SIZE) * sizeof (char));

		if (ptr == NULL) {

			hasp_scds_syslog_0(LOG_ERR,
			    "Failed to allocate memory.");

			return (1);
		}

		strbuf->str = ptr;

		/* Update str_capacity property of the strbuf. */

		strbuf->str_capacity += STRBUF_SIZE;
	}

	/*
	 * Append string to string buffer.
	 */

	(void) strcpy(&strbuf->str[strbuf->str_length], str);

	/*
	 * Update the str_length property of strbuf.
	 */

	strbuf->str_length += strlen(str);

	return (0);
}


/*
 * Function: svc_parseDeviceList
 *
 * Arguments:
 *
 *	const char *list	comma separated list
 *				of devices
 *	scha_strarray_t		string container
 *
 * Return:
 *
 *	0	If the list was parsed into individual
 *		strings and added to sa.
 *
 *	1	If an error occurred.
 *
 * Comments:
 *	The list is assumed to be a list of one or more
 *	character strings, delimited by a comma. White
 *	spaces if present are left as is.
 *
 *	We assume that there is no trailing comma at
 *	the very end of the string.
 *
 */
int
svc_parseDeviceList(const char *list, scha_str_array_t *sa)
{
	char *del_chars = ",";
	char *list_copy, *device, *ptr;

	ASSERT(list != NULL);
	ASSERT(sa != NULL);
	ASSERT(sa->array_cnt != 0);

	list_copy = strdup(list);

	if (list_copy == NULL) {

		hasp_scds_syslog_0(LOG_ERR, "Failed to allocate memory.");

		return (1);

	}

	for (device = strtok(list_copy, del_chars);
		device != NULL; device = strtok(NULL, del_chars)) {

		ptr = strdup(device);

		if (ptr == NULL) {

			hasp_scds_syslog_0(LOG_ERR,
			    "Failed to allocate memory.");

			return (1);

		}

		scds_syslog_debug(DBG_LEVEL_HIGH,
		    "%s to be added as %d element in device array.",
		    ptr, sa->array_cnt);

		if (svc_addStr2Array(ptr, sa)) {
			return (1);
		}


	}

	/*
	 * Check if the device list did not contain even a
	 * single delimiter.
	 * In this case, device list is assumed to be a single
	 * device. The strtok function would have returned null
	 * in this case, with sa being empty i.e. sa->array_cnt
	 * = 0.
	 */

	if (sa->array_cnt == 0) {

		if (svc_addStr2Array(list, sa)) {
			return (1);
		}

	}

	free(list_copy);

	return (0);

}

/*
 * Function: svc_isDCSDevicePath
 *
 * Arguments:
 *
 * const char	*path
 *
 * Return:
 *
 *	B_TRUE	If the initial portion of the
 *		device path matches any of the,
 *		predefined templates for valid
 *		device paths.
 *
 *	B_FALSE	If no string comparisons were
 *		successful.
 *
 * Comments:
 *
 *	Valid device paths are /dev/global/dsk/xxx,
 *	/dev/md/dsk/xxxx, and /dev/vx/dsk/xxx.
 *
 */
boolean_t
svc_isDCSDevicePath(const char *path)
{

	ASSERT(path != NULL);

	if (strncmp(path,
		SvcDevGlbHdr, strlen(SvcDevGlbHdr)) == 0)
		return (B_TRUE);
	else if (strncmp(path,
		SvcDevMdHdr, strlen(SvcDevMdHdr)) == 0)
		return (B_TRUE);
	else
		return (B_FALSE);
}

/*
 * Function: svc_getDCSPathServiceName
 *
 * Arguments:
 *
 * devpath	DCS Device path name to obtain the service name.
 *
 * sername	The return value of pointer to service name.
 *
 * Return:
 *
 * 0		If service name returned successfully.
 *
 * 1		An error while obtaining service name.
 *
 * Comments:
 *		This function retrieves the service name associated with the
 *		given DCS Device path.
 */
static int
svc_getDCSPathServiceName(const char *devpath, char **sername)
{
	struct stat	statbuf;
	dev_t		dev;
	dc_error_t	dc;

	if (stat(devpath, &statbuf) != 0) {

				    /* CSTYLED */
		int ret = errno;    /*lint !e578 */
		/*
		 * SCMSGS
		 * @explanation
		 * HAStoragePlus was not able to analyze the device file.
		 * @user_action
		 * Check that this device is valid.
		 */
		hasp_scds_syslog_2(LOG_ERR,
		    "Failed to analyze the device file"
		    " %s: %s.",
		    devpath,			/* CSTYLED */
		    strerror(ret));		/*lint !e666 */

		return (1);

	}
	if (!(S_ISCHR(statbuf.st_mode) || S_ISBLK(statbuf.st_mode))) {
		/*
		 * SCMSGS
		 * @explanation
		 * Self explanatory.
		 * @user_action
		 * Specify either a character or a block device path.
		 */
		hasp_scds_syslog_1(LOG_ERR,
		    "The device special file (%s) is not either character or "
		    "block device.",
		    devpath);
		return (1);
	}

	dev = statbuf.st_rdev;

	/*
	 * Obtain the device service name
	 */

	dc = dcs_get_service_name_by_dev_t(
	    getemajor(dev),	/*lint !e685 */
	    geteminor(dev),
	    sername);

	if (dc != DCS_SUCCESS) {

		/*
		 * SCMSGS
		 * @explanation
		 * The DCS was not able to find the global service name.
		 * @user_action
		 * Check the global service configuration.
		 */
		hasp_scds_syslog_2(LOG_ERR,
		    "Failed to obtain the global service name"
		    " of device %s: %s.",
		    devpath,
				/* CSTYLED */
				/*lint !e666 */
		    dcs_error_to_string(dc));

		return (1);

	}

	return (0);
}

/*
 *
 * Function: svc_findOurMDSrs
 *
 * Arguments:
 *
 * mds_rs 	address of a variable for MDS Resource Type name
 * mountpoint	name of mountpoint
 *
 * Return:
 *
 * SCHA_ERR_NOERR	found our MDS Resource
 * SCHA_ERR_CHECKS	No resource of type SUNW.qfs found
 * SCHA_ERR_ENOMEM	system runs out of memory
 *
 * Comments:
 * 	This function retrieves all of the Resource Types in the cluster
 * 	and then for each of SUNW.qfs RT, calls svc_checkMDSrt() to see if
 * 	a resource of type SUNW.qfs RT exists.
 */
static scha_err_t
svc_findOurMDSrs(char** mds_rs, const char *mountpoint)
{
	scha_err_t err = SCHA_ERR_NOERR;
	scha_cluster_t	clust_handle;
	scha_str_array_t *rt_list = NULL;

	int rt_found = 0;
	int i = 0;

retry_cluster_get:
	/* Find out all resource types that are registered */
	if ((err = scha_cluster_open_zone(gSvcInfo.cl_name,
				&clust_handle)) != SCHA_ERR_NOERR) {
		hasp_scds_syslog_1(LOG_ERR,
		    "Failed to open the cluster handle: %s.",
		    scha_strerror(err));	/*lint !e666 */
		return (err);
	}

	if ((err = scha_cluster_get_zone(gSvcInfo.cl_name, clust_handle,
			SCHA_ALL_RESOURCETYPES, &rt_list)) != SCHA_ERR_NOERR) {
		if (err == SCHA_ERR_SEQID) {
			hasp_scds_syslog_0(LOG_INFO,
			    "Retrying to retrieve the cluster information.");
			(void) scha_cluster_close(clust_handle);
			goto retry_cluster_get;
		} else {
			hasp_scds_syslog_1(LOG_ERR,
			    "scha_cluster_get() failed: %s\n",
				scha_strerror(err));	/*lint !e666 */
			(void) scha_cluster_close(clust_handle);
			return (err);
		}
	}

	if (rt_list == NULL || (rt_list -> str_array[0]) == NULL) {
		/*
		 * SCMSGS
		 * @explanation
		 * No resource type registered in the cluster.
		 * @user_action
		 * Register the required resource types, create the
		 * required instances of those resource types and
		 * repeat the operation. For information about how to
		 * create resource types and their instances, see
		 * your Sun Cluster documentation.
		 */
		hasp_scds_syslog_0(LOG_ERR,
			"No resource type found in the cluster.\n");
		(void) scha_cluster_close(clust_handle);
		return (SCHA_ERR_CHECKS);
	}

	/* For each SUNW.qfs RT, check if there's a resource */
	for (i = 0; i < (int)(rt_list -> array_cnt); i++) {
		if (strstr(rt_list -> str_array[i], SUNW_QFS) != NULL) {

			rt_found = 1;
			if ((err = svc_checkMDSrt(mds_rs, rt_list->str_array[i],
					mountpoint)) != SCHA_ERR_NOERR) {
				(void) scha_cluster_close(clust_handle);
				return (err);
			} else {
				if (*mds_rs != NULL) {
				/* We have found our MDS resource */
					break;
				}
			}
		}
	}

	if (!rt_found) {
		/*
		 * SCMSGS
		 * @explanation
		 * The SUNW.qfs resource type is not registered in the
		 * cluster.
		 * @user_action
		 * Register the SUNW.qfs resource type, create the
		 * required instances of this resource type, and
		 * repeat the operation. For information about how to
		 * configure the shared QFS file system with Sun
		 * Cluster, see your Sun Cluster documentation and
		 * your Sun StorEdge QFS documentation.
		 */
		hasp_scds_syslog_1(LOG_ERR,
			"Resource type \"%s\" does not exist.\n", SUNW_QFS);
		(void) scha_cluster_close(clust_handle);
		return (SCHA_ERR_CHECKS);
	}

	if (*mds_rs == NULL) {
		/*
		 * SCMSGS
		 * @explanation
		 * No metadata server resource has been created to represent
		 * the specified file system.
		 * @user_action
		 * Ensure that a metadata server resource for the file
		 * system is created. For information about how to configure
		 * the shared QFS file system with Sun Cluster, see your
		 * Sun Cluster and documentation your Sun StorEdge QFS
		 * documentation.
		 */
		hasp_scds_syslog_1(LOG_ERR,
			"No MDS resource found for mount-point %s.\n",
			mountpoint);
		err = SCHA_ERR_CHECKS;
	}

	(void) scha_cluster_close(clust_handle);
	return (err);
}

/*
 *
 * Function: svc_checkMDSrt
 *
 * Arguments:
 *
 * mds_rs 	Address of a variable for MDS Resource name
 * mds_rt	Name of an MDS (SUNW.qfs) RT
 * mountpoint	File-system mountpoint
 *
 * Return:
 *
 * SCHA_ERR_NOERR	Successfully checked the given SUNW.qfs RT
 * SCHA_ERR_ENOMEM	system runs out of memory
 * Any other		Failure occurred while checking the given RT
 *
 * Comments:
 *	This function checks whether a given SUNW.qfs RT has any
 *	resource. It does so, by first getting the list of Resources of
 *	the given SUNW.qfs type and then finding which one of these
 *	Resources reprsents our mountpoint.
 */
static scha_err_t
svc_checkMDSrt(char **mds_rs, const char *mds_rt,
		const char *mountpoint)
{
	scha_err_t err = SCHA_ERR_NOERR;
	scha_resourcetype_t rt_handle;
	scha_str_array_t *rs_list = NULL;

	int result = 0;
	int i = 0;

retry_rt_get:
	if ((err = scha_resourcetype_open_zone(gSvcInfo.cl_name,
				mds_rt, &rt_handle)) != SCHA_ERR_NOERR) {
		hasp_scds_syslog_1(LOG_ERR,
			"scha_resourcetype_open() failed: %s\n",
					scha_strerror(err));	/*lint !e666 */
		return (err);
	}

	/* Get the list of resources of the given SUNW.qfs RT */
	if ((err = scha_resourcetype_get_zone(gSvcInfo.cl_name, rt_handle,
			SCHA_RESOURCE_LIST, &rs_list)) != SCHA_ERR_NOERR) {
		if (err == SCHA_ERR_SEQID) {
			hasp_scds_syslog_0(LOG_INFO,
			    "Retrying to retrieve the cluster information.");
			(void) scha_cluster_close(rt_handle);
			goto retry_rt_get;
		} else if (err == SCHA_ERR_RT) {
			hasp_scds_syslog_1(LOG_ERR,
				"Resource type %s does not exist.\n", mds_rt);
		} else {
			hasp_scds_syslog_1(LOG_ERR,
				"scha_resourcetype_get() failed: %s\n",
					scha_strerror(err));	/*lint !e666 */
		}

		(void) scha_resourcetype_close(rt_handle);
		return (err);
	}

	/* There are not any resources in the given SUNW.qfs RT. */
	if (rs_list == NULL || rs_list -> array_cnt == 0) {
		scds_syslog_debug(DBG_LEVEL_LOW,
			"No resource found in resource type %s\n", mds_rt);
		(void) scha_resourcetype_close(rt_handle);
		*mds_rs = NULL;
		return (err);
	}

	/*
	 * There are resources in the given SUNW.qfs RT. Now see if there is one
	 * that represents our HAStoragePlus resource.
	 */
	for (i = 0; i < (int)(rs_list -> array_cnt); i++) {

		if ((err = svc_checkMDSrs(rs_list -> str_array[i], mountpoint,
					&result)) != SCHA_ERR_NOERR) {
			break;
		}

		if (result) {
			/* We have found our MDS Resource */
			scds_syslog_debug(DBG_LEVEL_LOW,
					"Our MDS resource : %s.\n",
					rs_list -> str_array[i]);

			if ((*mds_rs = strdup(rs_list -> str_array[i]))
						== NULL) {
				hasp_scds_syslog_0(LOG_ERR, "Out of memory\n");
				err = SCHA_ERR_NOMEM;
			}

			break;
		}
	}

	(void) scha_resourcetype_close(rt_handle);
	return (err);
}

/*
 * Function: svc_checkMDSrs
 *
 * Arguments:
 *
 * rs_name 	Name of an MDS Resource
 * mountpoint	File-system mountpoint
 * result	Address of an integer variable
 *
 * Return:
 *
 * SCHA_ERR_NOERR	successfully checked the extension property
 *			of the MDS Resource
 * SCHA_ERR_INTERNAL	Internal error occurred
 *
 * Comments:
 *	This function retrieves the "QFSFileSystem" extension property
 *	and checks if it contains our file-system mount-point.
 */
static scha_err_t
svc_checkMDSrs(const char *rs_name, const char *mountpoint, int *result) {

	scha_err_t err = SCHA_ERR_NOERR;
	scha_resource_t rs_handle;
	scha_extprop_value_t *prop_val = NULL;

	int i = 0;

retry_rs:
	if ((err = scha_resource_open_zone(gSvcInfo.cl_name, rs_name,
			NULL, &rs_handle)) != SCHA_ERR_NOERR) {
		hasp_scds_syslog_2(LOG_ERR,
			"Failed to open the resource %s handle: %s.",
			rs_name, scha_strerror(err));	/*lint !e666 */
		return (err);
	}

	if ((err = scha_resource_get_zone(gSvcInfo.cl_name, rs_handle,
		SCHA_EXTENSION, QFSFILESYSTEM, &prop_val))
				!= SCHA_ERR_NOERR) {
		if (err == SCHA_ERR_SEQID) {
			hasp_scds_syslog_0(LOG_INFO,
			    "Retrying to retrieve the resource information.");
			(void) scha_cluster_close(rs_handle);
			goto retry_rs;
		} else {
			hasp_scds_syslog_2(LOG_ERR,
			    "Failed to retrieve property %s: %s.",
				QFSFILESYSTEM,
				scha_strerror(err));	/*lint !e666 */
			goto cleanup;
		}
	}

	if (prop_val -> val.val_strarray -> array_cnt == 0) {
		hasp_scds_syslog_2(LOG_ERR, "Extension property \"%s\""
					" of MDS resource %s is empty.\n",
					QFSFILESYSTEM, rs_name);
		err = SCHA_ERR_INTERNAL;
		goto cleanup;
	}

	for (i = 0; i < (int)(prop_val -> val.val_strarray -> array_cnt);
							i ++) {
		if (strcmp(mountpoint,
			prop_val -> val.val_strarray -> str_array[i]) == 0) {
			*result = 1;
			goto cleanup;
		}
	}

cleanup:
	(void) scha_resource_close(rs_handle);

	return (err);
}

/*
 * Function: svc_checkRSdependency
 *
 * Arguments:
 *
 * handlep 	Pointer to scds_handle_t
 * dependee	Name of the dependee resource
 *
 * Return:
 *
 * SCHA_ERR_NOERR	dependency has been defined
 * SCHA_ERR_INTERNAL	failed to retrieve property
 *			"Resource_dependencies"
 * SCHA_ERR_CHECKS	dependency not defined
 *
 * Comments:
 *	This function checks whether the RGM-dependency between the
 *	given resource and the given dependee resource has been defined.
 */
static scha_err_t
svc_checkRSdependency(const scds_handle_t *handlep, const char *dependee)
{
	scha_str_array_t *rs_depend = NULL;
	scha_err_t err = SCHA_ERR_NOERR;

	int flg = 0;
	int i = 0;

	if ((rs_depend = (scha_str_array_t*)
			scds_get_rs_resource_dependencies(*handlep)) == NULL) {
		/*
		 * SCMSGS
		 * @explanation
		 * A call to scds_get_rs_resource_dependencies() failed.
		 * @user_action
		 * Contact your authorized Sun service provider to determine
		 * whether a workaround or patch is available.
		 */
		hasp_scds_syslog_0(LOG_ERR,
			"scds_get_rs_resource_dependencies()"
					" failed.\n");
		err = SCHA_ERR_INTERNAL;
		goto cleanup;
	}

	if (rs_depend -> array_cnt == 0) {
		err = SCHA_ERR_CHECKS;
		goto cleanup;
	}

	for (i = 0; i < (int)(rs_depend -> array_cnt); i++) {
		if (strcmp(rs_depend -> str_array[i], dependee) == 0) {
			flg = 1;
			break;
		}
	}

	if (!flg) {
		err = SCHA_ERR_CHECKS;
	}

cleanup:
	return (err);
}


/*
 *
 * Function: svc_fillFailbackSetting
 *
 * Arguments:
 *
 * argc 	Number of arguments
 *
 * argv		Command line argument list of call back method
 *
 * Return:
 *
 * 0  if the RGM failback setting is read and stored
 *    in gSvcInfo.failback
 *
 * 1  if an error occurred.
 *
 * Comments:
 *
 * This function first tries to retrieve the failback property value from the
 * command line arguments passed by RGM. If not found it uses scha API.
 *
 */
static int
svc_fillFailbackSetting(int argc, char *const argv[])
{
	char *delimiter = "=";
	char *key, *val, *tmp;
	int option;
	scha_resourcegroup_t handle = NULL;
	scha_err_t retval = SCHA_ERR_NOERR;

	while ((option = getopt(argc, argv, "R:T:G:Z:r:x:g:cu")) != EOF) {

		switch (option) {

			/*
			 * We are interested in "g" option only
			 */

			case 'g':

				/*
				 * Break the string of format "keyword=values"
				 * into keyword and values (delimiter is '=')
				 */

				tmp = strdup(optarg);

				if (tmp == NULL) {

					hasp_scds_syslog_0(LOG_ERR,
					    "Failed to allocate memory.");

					return (1);

				}

				key = strtok(tmp, delimiter);

				if (key == NULL) {

					/* Supposed not to be NULL */

					/*
					 * SCMSGS
					 * @explanation
					 * Unable to retrieve the failback
					 * property value.
					 * @user_action
					 * Check the resource group
					 * configuration in which this
					 * resource is configured. If the
					 * problem persists, contact your
					 * authorized Sun service provider
					 * with copies of /var/adm/messages
					 * files on all nodes.
					 */
					hasp_scds_syslog_0(LOG_ERR,
					    "Failed to retrieve failback "
					    "value.");

					return (1);

				}

				val = strtok(NULL, delimiter);

				if (strcasecmp(key, SCHA_FAILBACK) == 0) {

					if (strcasecmp(val, "FALSE") == 0) {

						gSvcInfo.failback = B_FALSE;

						scds_syslog_debug(
						    DBG_LEVEL_HIGH, "Failback "
						    "for resource group %s is "
						    "set to FALSE.",
						    gSvcInfo.rgName);

					} else {

						gSvcInfo.failback = B_TRUE;

						scds_syslog_debug(
						    DBG_LEVEL_HIGH, "Failback "
						    "for resource group %s is "
						    "set to TRUE.",
						    gSvcInfo.rgName);
					}

					free(tmp);

					return (0);
				}

				free(tmp);

				break;

			default :
				break;
		}
	}

	/*
	 * Failback value is not found in arguments, use scha API
	 */

	retval = scha_resourcegroup_open_zone(gSvcInfo.cl_name,
	    gSvcInfo.rgName, &handle);

	if (retval != SCHA_ERR_NOERR) {

		/*
		 * SCMSGS
		 * @explanation
		 * An API operation has failed while retrieving the resource
		 * group property. Low memory or API call failure might be the
		 * reasons.
		 * @user_action
		 * In case of low memory, the problem will probably cured by
		 * rebooting. If the problem recurs, you might need to
		 * increase swap space by configuring additional swap devices.
		 * Otherwise, if it is API call failure, check the syslog
		 * messages from other components. For resource group name and
		 * the property name, check the current syslog message.
		 */
		hasp_scds_syslog_1(LOG_ERR,
		    "Failed to open the resource group handle: %s.",
						/* CSTYLED */
		    scds_error_string(retval));	/*lint !e666 */

		return (1);

	}

	if (handle == NULL) {

		hasp_scds_syslog_1(LOG_ERR,
		    "Failed to open the resource group handle: %s.",
						/* CSTYLED */
		    scds_error_string(retval));	/*lint !e666 */

		return (1);

	}

	retval = scha_resourcegroup_get_zone(gSvcInfo.cl_name, handle,
	    SCHA_FAILBACK, &gSvcInfo.failback);

	if (retval == SCHA_ERR_NOERR || retval == SCHA_ERR_SEQID) {

		if (gSvcInfo.failback == B_TRUE) {

			scds_syslog_debug(DBG_LEVEL_HIGH,
			    "Failback for resource group %s is set to TRUE.",
			    gSvcInfo.rgName);

		} else {

			scds_syslog_debug(DBG_LEVEL_HIGH,
			    "Failback for resource group %s is set to FALSE.",
			    gSvcInfo.rgName);
		}
	} else {

		/*
		 * SCMSGS
		 * @explanation
		 * HAStoragePlus was not able to retrieve the resource group
		 * Failback property from the CCR.
		 * @user_action
		 * Check the cluster configuration. If the problem persists,
		 * contact your authorized Sun service provider.
		 */
		hasp_scds_syslog_1(LOG_ERR,
		    "Failed to retrieve the resource group Failback property:"
		    " %s.",			/* CSTYLED */
		    scds_error_string(retval));	/*lint !e666 */

		return (1);
	}

	(void) scha_resourcegroup_close(handle);

	return (0);

}

/*
 *
 * Function: svc_fillZoneSetting
 *
 * Arguments:
 *
 * ds_handle 	dsdl handle
 *
 * Return:
 *
 * 0	if the Zone setting values are filled successfully.
 *
 * 1	if an error occurred.
 *
 * Comments:
 * 	This function fills values related to zones like zonename, zone root
 * 	path, node type on which resource is running, cluster name for which
 * 	the resource is configured etc ...
 */
static int
svc_fillZoneSetting(scds_handle_t ds_handle)
{
#ifdef ZONE_SUPPORT
	char rootpath[MAXPATHLEN];
#endif

	/*
	 * If we are running for the global zone, scds_get_zone_name shall
	 * return NULL.
	 */
	gSvcInfo.zonename = scds_get_zone_name(ds_handle);
	if (gSvcInfo.zonename != NULL) {
#ifdef ZONE_SUPPORT
		if (zone_get_rootpath((char *)gSvcInfo.zonename, rootpath,
		    MAXPATHLEN) != Z_OK) {
			/*
			 * SCMSGS
			 * @explanation
			 * Need explanation of this message!
			 * @user_action
			 * Need a user action for this message.
			 */
			hasp_scds_syslog_0(LOG_ERR,
				"Failed to get zone root path.");
			return (1);
		}
		gSvcInfo.zoneroot = strdup(rootpath);
		if (gSvcInfo.zoneroot == NULL) {
			hasp_scds_syslog_0(LOG_ERR,
			    "Failed to allocate memory.");
			return (1);
		}

		scds_syslog_debug(DBG_LEVEL_HIGH,
		    "The resource configured for zone (%s) with "
		    "root path (%s).",
		    gSvcInfo.zonename, gSvcInfo.zoneroot);
#else
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		hasp_scds_syslog_0(LOG_ERR,
		    "Zones are not supported in this release.");
		return (1);
#endif
	}


	/*
	 * Fill the information about the node on which the resource is
	 * being executed.
	 */
	if (svc_fillMethodExecutionNode(ds_handle)) {
		return (1);
	}

	scds_syslog_debug(DBG_LEVEL_HIGH,
	    "The resource is being executed on node :%d.",
	    gSvcInfo.svcExecNode);
	/*
	 * Fill the cluster name for which the resource is configured.
	 */
	if (svc_fillClusterName(ds_handle)) {
		return (1);
	}

	scds_syslog_debug(DBG_LEVEL_HIGH,
	    "The resource is configured for cluster : %s.",
	    gSvcInfo.cl_name);

	return (0);
}

/*
 * Name:		svc_fillMethodExecutionNode
 * Argument:
 * 			1. dsdl handle
 * Return:
 *			0 if no error occurs, otherwise 1
 *
 * Comments:
 *	This function fills with information where the call back method is
 *	being executed.
 */
/*lint -e715 -e818 */
int
svc_fillMethodExecutionNode(scds_handle_t ds_handle)
{
#ifdef ZONE_SUPPORT
	zoneid_t	zoneid;

	/*
	 * We are addressing the following different cases
	 * 1. resource configured for global zone.
	 * 2. resource configured for non cluster brand zone but the method
	 *    runs in global zone (GLOBAL_ZONE = TRUE).
	 * 3. resource configured for cluster brand zone but the method
	 *    runs in global zone (GLOBAL_ZONE = TRUE).
	 * 4. resource configured for non cluster brand zone and method runs in
	 *    same zone.
	 * 5. resource configured for cluster brand zone and method runs
	 *    in same zone.
	 * and determine for which node the method is running.
	 */
	const char *zonename = scds_get_zone_name(ds_handle);
	if (zonename == NULL) {
		/* Case 1 */
		gSvcInfo.svcExecNode = SVC_BASE_CLUSTER_GZ_NODE;
		return (0);
	}

	/*
	 * Case 2,3 vs Case 4, 5 can be identified by getting current
	 * zone id where method is running.
	 */
	zoneid = getzoneid();
	if (zoneid == -1) {
		hasp_scds_syslog_1(LOG_ERR, "INTERNAL ERROR: %s.",
		    "Failed to obtain zoneid.");
		return (1);
	}
	if (zoneid == GLOBAL_ZONEID) {
		/*
		 * Case 2, 3
		 */
		cz_id_t	czid;
		czid.clid = 0;
		(void) strcpy(czid.name, zonename);
		if (cladm(CL_CONFIG, CL_GET_ZC_ID, &czid) != 0) {
			int rc = errno;
			if (rc != ENOENT) {
				/*
				 * The ENOENT valid error when the non cluster
				 * brand zone name is passed as zone cluster
				 * name.
				 */
				hasp_scds_syslog_2(LOG_ERR,
				    "INTERNAL ERROR: %s: %s.",
				    "cladm(CL_GET_ZC_ID) failed",
							/* CSTYLED */
				    strerror(rc));	/*lint !e666 */
				return (1);
			}
		}
		if (czid.clid < MIN_CLUSTER_ID) {
			/*
			 * Case 2. This is non cluster brand zone.
			 */
			gSvcInfo.svcExecNode =
			    SVC_BASE_CLUSTER_LZ_NODE_RUNS_IN_GZ;
		} else {
			/*
			 * Case 3. This is cluster brand node.
			 */
			gSvcInfo.svcExecNode = SVC_ZC_NODE_RUNS_IN_GZ;
		}
	} else {
		/*
		 * Case 4, 5.
		 * These cases can be distinguished based on cluster name
		 * obtained from cladm.
		 * For Case 4, the cluster name from cladm() will be the
		 * base cluster name.
		 * For Case 5, the cluster name from cladm() will be same
		 * as zone name from RGM.
		 */
		clcluster_name_t	clname;
		char			cladm_cl_name[MAXHOSTNAMELEN];

		clname.name = cladm_cl_name;
		clname.len = sizeof (cladm_cl_name);
		if (cladm(CL_CONFIG, CL_GET_CLUSTER_NAME, &clname)) {
			hasp_scds_syslog_1(LOG_ERR, "INTERNAL ERROR: %s.",
			    "cladm() call failed");
			return (1);
		}
		if (strcmp(cladm_cl_name, zonename) == 0) {
			/*
			 * Case 5. This is cluster brand node.
			 */
			gSvcInfo.svcExecNode = SVC_ZC_NODE;
		} else {
			/*
			 * Case 4. This is non cluster brand zone.
			 */
			gSvcInfo.svcExecNode = SVC_BASE_CLUSTER_LZ_NODE;
		}
	}
#else
	gSvcInfo.svcExecNode = SVC_BASE_CLUSTER_GZ_NODE;
#endif
	return (0);
} /*lint +e715 -e818 */

/*
 * Name:		svc_fillClusterName
 * Argument:
 * 			1. dsdl handle
 * Return:
 *			0 if no error occurs, otherwise 1
 *
 * Comments:
 *	This function fills the cluster name for which the resource is
 *	configured.
 */
/*lint -e715 -e818 */
int
svc_fillClusterName(scds_handle_t ds_handle)
{
#if SOL_VERSION <= __s9
	/*
	 * There is no zone support. We just return the cluster name as
	 * NULL to make use of scha_*_zone() API in all OS
	 * versions.
	 */
	gSvcInfo.cl_name = NULL;
#else
	switch (gSvcInfo.svcExecNode) {
		case	SVC_BASE_CLUSTER_GZ_NODE:
			/* fall through */
		case	SVC_BASE_CLUSTER_LZ_NODE_RUNS_IN_GZ:
			/* fall through */
		case 	SVC_BASE_CLUSTER_LZ_NODE:
			gSvcInfo.cl_name = GLOBAL_ZONENAME;
			break;
		case	SVC_ZC_NODE:
			/* fall through */
		case	SVC_ZC_NODE_RUNS_IN_GZ:
			/*
			 * The zone name on which ths resource is running
			 * is also the cluster name.
			 */
			gSvcInfo.cl_name = scds_get_zone_name(ds_handle);
			ASSERT(cl_name);
			break;
		case	SVC_UNKNOWN_NODE:
		default:
			ASSERT(0);
			break;
	}
#endif
	return (0);
} /*lint +e715 -e818 */

/*
 *
 * Function: svc_fillNodeIdArray
 *
 * Arguments: none
 *
 *
 * Return:
 *
 * 0  if the RGM nodelist is read and stored
 *    in gSvcInfo.rgNodeIdArray
 *
 * 1  if an error occurred.
 *
 * Comments:
 *
 * SCDS API's are used to obtain the latest nodelist.
 *
 */
static int
svc_fillNodeIdArray(scds_handle_t ds_handle)
{
	scha_err_t retval = SCHA_ERR_NOERR;
	const scha_str_array_t *nodelistarray = NULL;
	scha_cluster_t cl_handle = NULL;
	uint_t ix;
	uint_t NodeIdCount = 0;
	uint_t TmpNodeId;
	uint_t i;

	retval = scha_cluster_open_zone(gSvcInfo.cl_name, &cl_handle);

	if ((retval != SCHA_ERR_NOERR) || (cl_handle == NULL)) {

		/*
		 * SCMSGS
		 * @explanation
		 * HAStoragePlus was not able to access the cluster
		 * configuration.
		 * @user_action
		 * Check the cluster configuration. If the problem persists,
		 * contact your authorized Sun service provider.
		 */
		hasp_scds_syslog_1(LOG_ERR,
		    "Failed to open the cluster handle: %s.",	/* CSTYLED */
		    scds_error_string(retval));			/*lint !e666 */

		return (1);
	}

	nodelistarray = scds_get_rg_nodelist(ds_handle);

	if ((nodelistarray == NULL) || (nodelistarray->array_cnt == 0)) {

		/*
		 * SCMSGS
		 * @explanation
		 * HAStoragePlus was not able to retrieve the resource group
		 * Nodelist property from the CCR.
		 * @user_action
		 * Check the cluster configuration. If the problem persists,
		 * contact your authorized Sun service provider.
		 */
		hasp_scds_syslog_0(LOG_ERR,
		    "Failed to retrieve the resource group Nodelist property.");

		return (1);

	} else {

		/*
		 * Allocate memory for an array of rgNodeIdArraySize node ids.
		 */
		gSvcInfo.rgNodeIdArray = (uint_t *)
			calloc(nodelistarray->array_cnt, sizeof (uint_t));

		if (!gSvcInfo.rgNodeIdArray) {

			hasp_scds_syslog_0(LOG_ERR,
			    "Failed to allocate memory.");

			return (1);

		}

		for (ix = 0; ix < nodelistarray->array_cnt; ix++) {

			retval = scha_cluster_get_zone(gSvcInfo.cl_name,
			    cl_handle, SCHA_NODEID_NODENAME,
			    nodelistarray->str_array[ix], &TmpNodeId);

			if (retval != SCHA_ERR_NOERR) {

				/*
				 * SCMSGS
				 * @explanation
				 * Self explanatory.
				 * @user_action
				 * Check the cluster configuration. If the
				 * problem persists, contact your authorized
				 * Sun service provider.
				 */
				hasp_scds_syslog_2(LOG_ERR,
				    "Failed to retrieve the node id from the"
				    " node name for node %s: %s.",
				    nodelistarray->str_array[ix], /* CSTYLED */
				    scds_error_string(retval)); /*lint !e666 */

				return (1);

			}

			for (i = 0; i < NodeIdCount; i++) {
				if (gSvcInfo.rgNodeIdArray[i] == TmpNodeId) {
					if ((NodeIdCount != 0) &&
					    (NodeIdCount-1 != i)) {
						/*
						 * SCMSGS
						 * @explanation
						 * NodeList contains a
						 * zone that is not consecutive
						 * with other zones on the same
						 * physical host.
						 * @user_action
						 * Reorder the NodeList for the
						 * resource group so that all
						 * zones on a physical host are
						 * consecutive.
						 */
						hasp_scds_syslog_0(LOG_ERR,
						    "Multiple zones on the same"
						    " physical host must appear"
						    " consecutively in the"
						    " nodelist.");
						return (1);
					}
				}
			}
			if ((NodeIdCount == 0) ||
			    gSvcInfo.rgNodeIdArray[NodeIdCount-1] !=
								TmpNodeId) {
				gSvcInfo.rgNodeIdArray[NodeIdCount] = TmpNodeId;
				NodeIdCount++;
				scds_syslog_debug(DBG_LEVEL_HIGH,
				    "Node %s (id %d) belongs to "
				    "resource group %s.",
				    nodelistarray->str_array[ix],
				    gSvcInfo.rgNodeIdArray[ix],
				    gSvcInfo.rgName);
			}
		}
		gSvcInfo.rgNodeIdArraySize = NodeIdCount;

	}

	(void) scha_cluster_close(cl_handle);

	return (0);
}

#ifdef	ZFS_AVAILABLE

#define	GLOBAL_ZONE_ALT_ROOT	"/"

/*
 * Function: svc_importZpools
 *
 * Arguments: none
 *
 * Return:
 *
 * 0		if no zpools to import, or
 *		if all zpools specified in Zpools property were imported
 *		successfully
 *
 * 1		if an error occurred.
 *
 * Comments:
 * The zpools that are specified in "Zpools" extension property are imported.
 */
int
svc_importZpools()
{
	scha_str_array_t  ci_poollist = { 0 };
	scha_str_array_t  faillist = { 0 };
	scha_str_array_t  *importlist = NULL;
	scha_str_array_t  pools_imported = { 0 };
	scha_str_array_t  pools_not_imported = { 0 };
	hasp_strbuf_t	  import_failure_msg = { 0 };
	char		  *altroot = NULL;
	uint_t		  i, j;

	/*
	 * Return if there are no zpools to be imported.
	 */
	if (gSvcInfo.zpoolsInfo.zpoolsList.array_cnt == 0)
		return (0);

	/*
	 * Obtain the pools that are currently imported on this node.
	 */
	if (svc_getciZpoolsList(&ci_poollist)) {
		return (1);
	}

	scds_syslog_debug(DBG_LEVEL_HIGH, "The number of current zpools "
	    "available on this node are : %d.", ci_poollist.array_cnt);

	/*
	 * Find the pools which has to be imported and already imported
	 * to this node.
	 */
	for (i = 0; i < gSvcInfo.zpoolsInfo.zpoolsList.array_cnt; i++) {

		boolean_t found = B_FALSE;
		char	*poolname = gSvcInfo.zpoolsInfo.zpoolsList.str_array[i];

		for (j = 0; j < ci_poollist.array_cnt; j++) {
			if (strcmp(poolname, ci_poollist.str_array[j]) == 0) {
				scds_syslog_debug(DBG_LEVEL_HIGH,
				    "The pool \"%s\" is already imported.",
				    poolname);
				/*
				 * Add to the list of already imported.
				 */
				if (svc_addStr2Array(poolname, &pools_imported))
					return (1);
				found = B_TRUE;
				break;
			}
		}
		if (!found) {

			/*
			 * Add to the list to be imported.
			 */
			if (svc_addStr2Array(poolname, &pools_not_imported))
				return (1);
		}
	}

	if (svcMethodId == HASTORAGEPLUS_UPDATE) {
		/*
		 * During update we need to import the zpools which are not
		 * imported.
		 */
		importlist = &pools_not_imported;
	} else {
		/*
		 * Export all the zpools which are already imported.
		 * Reason: Consider the case of failover from one local to
		 * other local zone on same node. The zpool is in imported
		 * state on the node having file systems mounted at previous
		 * zone root path.
		 * So we need to export, and has be imported again on zone
		 * being failed over to get the file systems mounted on its
		 * root path.
		 */
		for (i = 0; i < pools_imported.array_cnt; i++) {
			(void) svc_exportZpool(pools_imported.str_array[i],
			    B_TRUE);
		}
		/*
		 * The list of pools to be imported are all configured zpools.
		 */
		importlist = &gSvcInfo.zpoolsInfo.zpoolsList;
	}

	/*
	 * Import zpools with the following options ..
	 * - Directory location to search for devices of zpools.
	 * - Use force option
	 * - Do overlay mounting
	 * - ZFS mount specific options to NULL
	 * - "/" is alternate root to import for global zone
	 * - "root of local zone" is alternate root to import for local zone.
	 * NOTE: The function svc_importZpoolList() imports (and also does
	 * mountpoint property validation) the pools which are passed which
	 * matches with the pools that can be imported. Since we have already
	 * validated that pools configured are in currently imported list or
	 * in list that can be imported, we can ensure all the pools which
	 * are passed will be imported.
	 */

	/*
	 * A NULL zonename indicates that resource is called for global zone.
	 */
	if (gSvcInfo.zonename == NULL) {
		altroot = GLOBAL_ZONE_ALT_ROOT;
	} else {
		altroot = gSvcInfo.zoneroot;
	}

	if (!svc_importZpoolList(importlist, B_TRUE, B_TRUE, NULL,
	    &faillist, altroot)) {

		/*
		 * Imported all the zpools successfully.
		 */
		return (0);
	}

	/*
	 * All zpools are not imported successfully.
	 * NOTE:
	 * We have export the pools which failed to import, if importing
	 * the pools is called during update. This to avoid leaving the
	 * pools in half through of import.
	 *
	 * Log the error messages with the zpools which are failed and also
	 * update the status message.
	 */
	if (faillist.array_cnt) {

		ADD_STR2STRBUF(import_failure_msg, "Failed to import:");
		for (i = 0; i < faillist.array_cnt; i++) {
			ADD_STR2STRBUF(import_failure_msg,
			    faillist.str_array[i]);
			ADD_STR2STRBUF(import_failure_msg, " ");

			if (svcMethodId == HASTORAGEPLUS_UPDATE) {
				/*
				 * Export the pool ignoring the return value.
				 */
				(void) svc_exportZpool(faillist.str_array[i],
				    B_TRUE);
			}
		}

		hasp_scds_syslog_0(LOG_ERR, import_failure_msg.str);

		/*
		 * Add to status message the zpools which failed.
		 * Message will be of the form : (On one line only)
		 *
		 * -- Failed to import: m1 m2 [...].
		 *
		 * where m1 m2 stands for zpools.
		 * NOTE:
		 * The above similar format is applicable for export failures.
		 */

		ADD_TO_STATUS_MSG_IF_NOT_EMPTY(" -- ");
		ADD_TO_STATUS_MSG(import_failure_msg.str);
	}

	return (1);
}

/*
 * Function: svc_exportZpools
 *
 * Arguments:
 * zpool_list	The list of zpools which has to be exported.
 *
 * Return:
 *
 * 0		if no zpools to export or
 *		if all zpools in list are exported successfully
 *
 * 1		if an error occurred.
 *
 * Comments:
 * The zpools points which are specified in zpool_list are exported.
 */
int
svc_exportZpools(const scha_str_array_t  *zpool_list)
{
	scha_str_array_t	ci_poollist = { 0 };
	uint_t			i, j;
	int			export_failed = 0;
	hasp_strbuf_t		export_failure_msg = { 0 };

	/*
	 * Return if there are no zpools to be exported.
	 */
	if (zpool_list->array_cnt == 0)
		return (0);

	/*
	 * Initialize the libzfs library.
	 */
	if (svc_initializeZFS()) {
		/*
		 * Error message already logged.
		 */
		return (1);
	}

	/*
	 * Get the list of pools that are currently imported on node.
	 */
	if (svc_getciZpoolsList(&ci_poollist))
		return (1);

	/*
	 * Export the zpool only if it is in the currently imported list.
	 */

	for (i = 0; i < zpool_list->array_cnt; i++) {

		boolean_t found = B_FALSE;

		for (j = 0; j < ci_poollist.array_cnt; j++) {

			if (strcmp(zpool_list->str_array[i],
			    ci_poollist.str_array[j]) == 0) {

				found = B_TRUE;
				break;
			}
		}

		if (!found) {

			/*
			 * Zpool is not in currently imported list. Log
			 * informational message and continue.
			 */
			/*
			 * SCMSGS
			 * @explanation
			 * HAStoragePlus determined that the zpool that you
			 * specified is exported but should not have been.
			 * @user_action
			 * This is an informational message, no user action is
			 * needed.
			 */
			hasp_scds_syslog_1(LOG_INFO, "The pool '%s' is "
			    "erroneously found to be exported.",
			    zpool_list->str_array[i]);

			continue;
		}

		/*
		 * Export each zpool with the following options ..
		 * -Unmount the file systems in the pool forcefully.
		 */
		if (svc_exportZpool(zpool_list->str_array[i], B_TRUE)) {

			if ((svcMethodId == HASTORAGEPLUS_INIT) ||
			    (svcMethodId ==
			    HASTORAGEPLUS_VALIDATE_FOR_UPDATE)) {
				/*
				 * Don't log any errors while export is
				 * happening while VALIDATE or INIT callback.
				 * They will complete export silently.
				 */

				scds_syslog_debug(DBG_LEVEL_HIGH,
				    "Failed to export the zpool \"%s\".",
				    zpool_list->str_array[i]);

				continue;
			}

			/*
			 * Error related to export already logged.
			 * Store the failed zpool name.
			 */
			if (!export_failed) {
				ADD_STR2STRBUF(export_failure_msg,
				    "Failed to export :");
				export_failed = 1;
			}

			ADD_STR2STRBUF(export_failure_msg,
			    zpool_list->str_array[i]);
			ADD_STR2STRBUF(export_failure_msg, " ");

			continue;
		}
		scds_syslog_debug(DBG_LEVEL_HIGH, "The zpool \"%s\" is "
		    "exported successfully.", zpool_list->str_array[i]);
	}

	if (export_failed) {
		/*
		 * Failed to export all zpools.
		 * Log error message and also update status message.
		 */
		hasp_scds_syslog_0(LOG_ERR, export_failure_msg.str);

		ADD_TO_STATUS_MSG_IF_NOT_EMPTY(" -- ");
		ADD_TO_STATUS_MSG(export_failure_msg.str);
	}

	return (export_failed);
}

static char OLDZPOOLS_FILE[MAXPATHLEN];

/*
 * Function: svc_saveOldZpools
 *
 * Arguments: none
 *
 * Return:
 *
 * 0		if all the old zpools are saved in temporary file successfully.
 *
 * 1		if an error occurred.
 *
 * Comments:
 * This routine is used to save the old zpools.
 * When update is done on the HAStoragePlus resource, RGM invokes first validate
 * method and invokes update iff validate is success. When validate completes
 * successfully, RGM updates the resource with the new properties by overriding
 * the old properties. Since the old zpools are required to find the zpools
 * which are removed for update, they are stored in temporary file.
 */

int
svc_saveOldZpools()
{
	scha_resource_t		handle;
	scha_extprop_value_t	*zpools = NULL;
	uint_t			zpoolscount, i;
	FILE			*fp = NULL;
	int			rc = 0;
	scha_err_t		err;

	/*
	 * Get the file name to store old mount points.
	 */

#if SOL_VERSION <= __s9
	(void) snprintf(OLDZPOOLS_FILE, MAXPATHLEN,
	    "/var/cluster/run/HAStoragePlus_oldzpools.%s.%s",
	    gSvcInfo.rsName, gSvcInfo.rgName);
#else
	(void) snprintf(OLDZPOOLS_FILE, MAXPATHLEN,
	    "/var/cluster/run/HAStoragePlus_oldzpools.%s.%s.%s",
	    gSvcInfo.cl_name, gSvcInfo.rsName, gSvcInfo.rgName);
#endif

	fp = fopen(OLDZPOOLS_FILE, "w");

	if (fp == NULL) {

		rc = errno;

		hasp_scds_syslog_2(LOG_ERR,
		    "Failed to open %s: %s.",
		    OLDZPOOLS_FILE,	/* CSTYLED */
		    strerror(rc));	/*lint !e666 */

		return (1);
	}

	/*
	 * SCHA API is used instead of scds_get_ext_property() to get old
	 * zpools.
	 */

	err = scha_resource_open_zone(gSvcInfo.cl_name, gSvcInfo.rsName,
	    gSvcInfo.rgName, &handle);

	if (err != SCHA_ERR_NOERR) {

		hasp_scds_syslog_2(LOG_ERR,
		    "Failed to open the resource %s handle: %s.",
						/* CSTYLED */
		    gSvcInfo.rsName,
		    scds_error_string(err));	/*lint !e666 */

		return (1);
	}

	err = scha_resource_get_zone(gSvcInfo.cl_name, handle, SCHA_EXTENSION,
	    ZPOOLS, &zpools);

	if (err != SCHA_ERR_NOERR) {

		hasp_scds_syslog_2(LOG_ERR,
		    "Failed to retrieve property %s: %s.",
		    ZPOOLS,			/* CSTYLED */
		    scds_error_string(err));	/*lint !e666 */

		rc = 1;

		goto out;
	}

	zpoolscount = zpools->val.val_strarray->array_cnt;

	for (i = 0; i < zpoolscount; i++) {

		if ((fprintf(fp, "%s\n",
		    zpools->val.val_strarray->str_array[i])) < 0) {

			/* This shouldn't happen */

			rc = errno;
			/*
			 * SCMSGS
			 * @explanation
			 * A write operation to the specified file is failed.
			 * @user_action
			 * Check the man page for errors and try to resolve the
			 * the problem. Otherwise  contact your authorized
			 * Sun service provider.
			 */
			hasp_scds_syslog_2(LOG_ERR,
			    "Failed to write to file %s: %s.",
			    OLDZPOOLS_FILE,	/* CSTYLED */
			    strerror(rc));	/*lint !e666 */
			rc = 1;
			goto out;
		}
	}

out:
	(void) scha_resource_close(handle);
	if (zpools)
		scds_free_ext_property(zpools);
	if (fp)
		(void) fclose(fp);
	return (rc);
}

/*
 * Function: svc_exportRemovedZpools
 *
 * Arguments:
 *
 * ds_handle	dsdl handle
 *
 * Return:
 *
 * 0		if all the removed zpools are exported successfully.
 *
 * 1		if an error occurred.
 *
 * Comments:
 * This routine exports the zpools which are removed during update operation.
 * This routine prepares the list of zpools to be exported and calls
 * svc_exportZpools().
 */
int
svc_exportRemovedZpools(scds_handle_t ds_handle)
{

	FILE			*fp = NULL;
	int			rc = 0;
	scha_str_array_t	oldzpools = { 0 },
				removedzpools = { 0 },
				*currentzpools;
	char			pool_name[MAXPATHLEN];
	scha_extprop_value_t	*zpools = NULL;
	scha_err_t		err;
	uint_t			i, j;

	/*
	 * Retrieve the current zpools configured.
	 */
	err = scds_get_ext_property(ds_handle,
	ZPOOLS, SCHA_PTYPE_STRINGARRAY, &zpools);

	if (err != SCHA_ERR_NOERR) {
		hasp_scds_syslog_2(LOG_ERR,
		    "Failed to retrieve property %s: %s.",
		    ZPOOLS,			/* CSTYLED */
		    scds_error_string(err));	/*lint !e666 */

		rc = 1;
		goto out;
	}

	currentzpools = zpools->val.val_strarray;

	/*
	 * Obtain the old zpools from the temporary file which are saved
	 * during validate.
	 */

#if SOL_VERSION <= __s9
	(void) snprintf(OLDZPOOLS_FILE, MAXPATHLEN,
	    "/var/cluster/run/HAStoragePlus_oldzpools.%s.%s",
	    gSvcInfo.rsName, gSvcInfo.rgName);
#else
	(void) snprintf(OLDZPOOLS_FILE, MAXPATHLEN,
	    "/var/cluster/run/HAStoragePlus_oldzpools.%s.%s.%s",
	    gSvcInfo.cl_name, gSvcInfo.rsName, gSvcInfo.rgName);
#endif

	fp = fopen(OLDZPOOLS_FILE, "r");

	if (fp == NULL) {

		/*
		 * Consider the case of upgrading the version of HAStoragePlus
		 * from the version where the Zpools property is not there to
		 * a version where Zpools exists. In this case the temporary
		 * file which contains old Zpools property values will not
		 * exists. So we have to handle this case gracefully by just
		 * returning.
		 */
		if (errno == ENOENT) {
			rc = 0;
			goto out;
		}

		rc = errno;
		hasp_scds_syslog_2(LOG_ERR,
		    "Failed to open %s: %s.",
		    OLDZPOOLS_FILE,	/* CSTYLED */
		    strerror(rc));	/*lint !e666 */

		rc = 1;
		goto out;
	}

	while (fgets(pool_name, MAXPATHLEN, fp)) {

		/* Strip the trailing '\n' character(s) */

		for (i = strlen(pool_name)-1; i > 0; i--) {
			if ((pool_name[i]) == '\n')
				pool_name[i] = '\0';
			else
				break;
		}

		if (svc_addStr2Array(pool_name, &oldzpools)) {
			rc = 1;
			goto out;
		}
	}

	/*
	 * Obtain the removed pools by comparing old zpools with the current
	 * zpools.
	 */

	for (i = 0; i < oldzpools.array_cnt; i++) {

		boolean_t found = B_FALSE;

		for (j = 0; j < currentzpools->array_cnt; j++) {
			if (strcmp(oldzpools.str_array[i],
			    currentzpools->str_array[j]) == 0) {
				found = B_TRUE;
				break;
			}
		}

		if (!found) {

			/*
			 * Zpool is removed, so add to the removed list.
			 */
			if (svc_addStr2Array(oldzpools.str_array[i],
			    &removedzpools)) {
				rc = 1;
				goto out;
			}
		}
	}

	/*
	 * Export the zpools which are removed for update.
	 */
	if (svc_exportZpools(&removedzpools)) {

		/*
		 * The export failures are already logged
		 */

		rc = 1;
		goto out;
	}

out:
	if (zpools)
		scds_free_ext_property(zpools);
	if (fp)
		(void) fclose(fp);
	return (rc);
}

/*
 * Function: svc_exportNewlyAddedZpools
 *
 * Arguments:
 *
 * ds_handle	dsdl handle
 *
 * Return:
 *
 * 0		if all the newly added zpools are exported successfully.
 *
 * 1		if an error occurred.
 *
 * Comments:
 * This routine exports the zpools which are added newly during update.
 * This is to make ensure that new pools are not imported on any other node
 * before it get imported on the node where resource is being brought online.
 * This routine prepares the list of zpools to be exported and calls
 * svc_exportZpools().
 */
int
svc_exportNewlyAddedZpools(scds_handle_t ds_handle)
{

	scha_resource_t		handle;
	scha_extprop_value_t	*oldzpools = NULL,
				*currentzpools = NULL;
	scha_str_array_t	newlyaddedzpools = { 0 };
	int			rc = 0;
	scha_err_t		err;
	uint_t			i, j;

	/*
	 * Get the old zpools.
	 */

	err = scha_resource_open_zone(gSvcInfo.cl_name, gSvcInfo.rsName,
	    gSvcInfo.rgName, &handle);

	if (err != SCHA_ERR_NOERR) {

		hasp_scds_syslog_2(LOG_ERR,
		    "Failed to open the resource %s handle: %s.",
						/* CSTYLED */
		    gSvcInfo.rsName,
		    scds_error_string(err));	/*lint !e666 */

		return (1);
	}

	err = scha_resource_get_zone(gSvcInfo.cl_name, handle, SCHA_EXTENSION,
	    ZPOOLS, &oldzpools);

	if (err != SCHA_ERR_NOERR) {

		hasp_scds_syslog_2(LOG_ERR,
		    "Failed to retrieve property %s: %s.",
		    ZPOOLS,			/* CSTYLED */
		    scds_error_string(err));	/*lint !e666 */

		rc = 1;
		goto out;
	}

	/*
	 * Retrieve the current zpools configured.
	 */
	err = scds_get_ext_property(ds_handle,
	ZPOOLS, SCHA_PTYPE_STRINGARRAY, &currentzpools);

	if (err != SCHA_ERR_NOERR) {
		hasp_scds_syslog_2(LOG_ERR,
		    "Failed to retrieve property %s: %s.",
		    ZPOOLS,			/* CSTYLED */
		    scds_error_string(err));	/*lint !e666 */

		rc = 1;
		goto out;
	}

	/*
	 * Obtain the newly added pools by comparing current zpools with the
	 * old zpools.
	 */

	for (i = 0; i < currentzpools->val.val_strarray->array_cnt; i++) {

		boolean_t found = B_FALSE;

		for (j = 0; j < oldzpools->val.val_strarray->array_cnt; j++) {
			if (strcmp(
			    currentzpools->val.val_strarray->str_array[i],
			    oldzpools->val.val_strarray->str_array[j]) == 0) {
				found = B_TRUE;
				break;
			}
		}

		if (!found) {

			/*
			 * Zpool is newly added.
			 */
			if (svc_addStr2Array(
			    currentzpools->val.val_strarray->str_array[i],
			    &newlyaddedzpools)) {
				rc = 1;
				goto out;
			}
		}
	}

	/*
	 * Export the newly added zpools which are added for update.
	 * We are ignoring the return value not to make VALDATE callback
	 * failure because of this.
	 */
	(void) svc_exportZpools(&newlyaddedzpools);
out:
	(void) scha_resource_close(&handle);
	if (oldzpools)
		scds_free_ext_property(oldzpools);
	if (currentzpools)
		scds_free_ext_property(currentzpools);
	return (rc);
}
#endif

/*
 *
 * Function: svc_getpathdepth
 *
 * Arguments:
 *
 * path		The path name to find the depth
 *
 * Return:
 *
 * uint_t	The number of components (depth) of the path
 *
 * Comments:
 *
 * This function returns the depth of the given pathname.
 */

static uint_t
svc_getpathdepth(char *path)
{
	uint_t	depth;
	char	*cp;

	if (path == NULL || *path == NULL || *path != '/')
		return (0);	/* this should never happen */

	/* root (/) is the minimal case */
	depth = 1;

	for (cp = path + 1; *cp; cp++, path++) {

		/* "///" counts as 1 */
		if (*path == '/' && *cp != '/')
			depth++;
	}

	return (depth);
}


/*
 * Function: svc_setrpath
 *
 * Arguments:
 *
 * fsp		The file system entry
 *
 * Return:
 * 		1 failure - the current is only due to memory.
 * 		0 no error occurred.
 *
 * Comments:
 *
 * This function sets the real path for the file system mount point entry.
 */

static int
svc_setrpath(fsentry_t *fsp)
{
	char	real_path[PATH_MAX];

	if (fsp->rpath) {
		free(fsp->rpath);
	}

	/*
	 * In case of realpath() failure, keep the rpath same as mount point.
	 * Otherwise rpath contains actual resolved path.
	 */
	if ((realpath(fsp->mountp, real_path)) == NULL) {
		fsp->rpath = strdup(fsp->mountp);
	} else {
		fsp->rpath = strdup(real_path);
	}

	if (fsp->rpath == NULL) {

		hasp_scds_syslog_0(LOG_ERR, "Failed to allocate memory.");
		return (1);
	}

	return (0);
}


/*
 *
 * Function: svc_mntcompar
 *
 * Arguments:
 *
 * a, b		The mount point entries to compare
 *
 * Return:
 *
 * int		The difference in number of components of "a" and "b"
 *
 * Comments:
 *
 * This function sorts mount entries based on the number of components in
 * ascending order.
 */

static int
svc_mntcompar(const void *a, const void *b)
{
	return ((int)(((*(fsentry_t **)a)->depth)-((*(fsentry_t **)b)->depth)));
}

/*
 *
 * Function: svc_umntcompar (reverse of svc_mntcompar for unmounting)
 *
 * Arguments:
 *
 * a, b		The mount point entries to compare
 *
 * Return:
 *
 * int		The difference in number of components of "a" and "b"
 *
 * Comments:
 *
 * This function sorts mount entries based on the number of components in
 * descending order.
 */

static int
svc_umntcompar(const void *a, const void *b)
{
	return ((int)(((*(fsentry_t **)b)->depth)-((*(fsentry_t **)a)->depth)));
}


/*
 *
 * Function: svc_mountFilesystems
 *
 * Arguments: none
 *
 * Return:
 *
 * 0		if no entries to mount, or
 *		if all file systems specified in FilesystemMountPoints
 *		property were mounted successfully
 *
 * 1		if an error occurred.
 *
 * Comments:
 * The mount points that are specified in "FilesystemMountPoints" extension
 * property are mounted if they are not mounted already (i.e no entry in
 * MNTTAB file).
 *
 */

int
svc_mountFilesystems()
{
	fsentry_t	**mntlist; /* Contains the list of mount entries */
	uint_t		nentries;	/* Number of entries to mount */
	struct mnttab	mref;
	FILE		*mfp = NULL;
	int		rc;
	uint_t		count, i;
	boolean_t	is_fsmounted;
	fsentry_t	**validmounts, **tmplist, **tmpvalidmounts;

	/*
	 * Examine the number of FMP entries and return if no entry exist.
	 */

	if (gSvcInfo.fmpCount == 0)
		return (0);

	/*
	 * Open the MNTTAB file used to check if a file system is
	 * mounted or not.
	 * NOTE:
	 * Checking the mount entry in VFSTAB is not done here (again) as it
	 * is done during validation (in svc_validate).
	 */

	if ((mfp = fopen(MNTTAB, "r")) == NULL) {

		rc = errno;

		hasp_scds_syslog_2(LOG_ERR,
		    "Failed to open %s: %s.",
		    VFSTAB,		/* CSTYLED */
		    strerror(rc));	/*lint !e666 */

		return (1);
	}

	/*
	 * Allocate space for maximum possible mount entries in the list.
	 */

	mntlist = (fsentry_t **)
	    calloc((gSvcInfo.fmpCount +1), sizeof (fsentry_t *));

	if (mntlist == NULL) {

		hasp_scds_syslog_0(LOG_ERR, "Failed to allocate memory.");

		return (1);
	}

	/*
	 * Nullify the elements in the mnttab structure.
	 */

	(void) memset(&mref, 0, sizeof (struct mnttab));

	for (i = gSvcInfo.gdpCount, count = 0; i < gSvcInfo.count; i++) {

		if (mntlist[count]  == NULL) {

			mntlist[count] =
			    (fsentry_t *)malloc(sizeof (fsentry_t));

			if (mntlist[count]  == NULL) {

				hasp_scds_syslog_0(LOG_ERR,
				    "Failed to allocate memory.");

				return (1);

			}

			mntlist[count]->rpath = NULL;
		}

		mntlist[count]->svcp = &gSvcInfo.list[i];
		mntlist[count]->status = 0;

		if (svc_populateMountPoints(mntlist[count])) {
			/* Error message logged. */
			return (1);
		}

		/* Set rpath and depth for the file systems */
		if (svc_setrpath(mntlist[count])) {
			/* Error message logged. */
			return (1);
		}
		mntlist[count]->depth = svc_getpathdepth(mntlist[count]->rpath);

		if (svc_isFSMounted(mfp, mntlist[count], &is_fsmounted)) {
			/* Error message logged. */
			return (1);
		}

		if (is_fsmounted) {
			scds_syslog_debug(DBG_LEVEL_HIGH,
			    "File system associated with %s is "
			    "already mounted.",
			    mntlist[count]->mountp);
			continue;
		}

		/*
		 * All the mount points are not in MNTTAB file.
		 * So keep it in mount list for mounting them later.
		 * NOTE: Incrementing count leaves the current mount point
		 * entry in mount list and points to next empty slot.
		 */

		count++;

	}

	/*
	 * Mark the end position to the list of mount entries.
	 * Now the mntlist contains the list of entries which has to be
	 * mounted.
	 */

	mntlist[count] = NULL;

	nentries = count;

	scds_syslog_debug(DBG_LEVEL_HIGH, "The number of entries to mount "
	    "are: %d", nentries);

	if (nentries == 0) {

		/* All entries in FMP property are already mounted */

		return (0);
	}

	/*
	 * Before proceeding to mount, perform fsck on file systems which
	 * has to be mounted. If atleast one fails return error.
	 *
	 * File system checks will be skipped
	 * 1)if the FilesystemCheckCommand =/bin/true
	 * 2)if the resource group is scalable.
	 * 3)if the resource group with AffinityOn set to false
	 *
	 * NOTES:
	 * Fsck on the file systems is skipped in scalable RG and failover RG
	 * with AffinityOn=false because fsck on a file system can be done only
	 * on the node where DG of that file system is primaried.
	 */

	scds_syslog_debug(DBG_LEVEL_HIGH, "Starting fsck on the devices");

	if (!((STREQ(gSvcInfo.fsckCmd, HASTORAGEPLUS_NO_FSCK)) ||
	    (gSvcInfo.rgMode == RGMODE_SCALABLE) ||
	    (gSvcInfo.affinityOn == B_FALSE))) {

		if (svc_doParFsck(mntlist, nentries)) {
			/*
			 * Errors are already logged. Return error.
			 * Memory is not being freed as the process is going
			 * away here.
			 */
			return (1);
		}
	}

	scds_syslog_debug(DBG_LEVEL_HIGH, "Fsck on all devices is completed "
	    "successfully");
	/*
	 * Sort the mount entries based on number of components of the
	 * real path.
	 */

	qsort((void *)mntlist, nentries, sizeof (fsentry_t *), svc_mntcompar);

	/*
	 * Return in case of any mount failure. Otherwise check and wait till
	 * every valid file system entry is in MNTTAB file.
	 */

	if (svc_doParMount(mntlist, nentries)) {

		/*
		 * Error message is already logged.
		 */

		return (1);
	}

	/*
	 * Confirm the mount status by checking MNTTAB file.
	 * NOTE: Confirmation of mount with MNTTAB is must for scalable RG,
	 * since it is the only way to know the file system mount status.
	 */
	tmplist = (fsentry_t **)malloc(sizeof (fsentry_t *) * (nentries+1));

	if (tmplist == NULL) {

		hasp_scds_syslog_0(LOG_ERR, "Failed to allocate memory.");

		return (1);

	}

	validmounts = (fsentry_t **)malloc(sizeof (fsentry_t *) * (nentries+1));

	if (validmounts == NULL) {

		hasp_scds_syslog_0(LOG_ERR, "Failed to allocate memory.");

		return (1);

	}

	for (i = 0; mntlist[i]; i++) {

		validmounts[i] = mntlist[i];

	}

	validmounts[i] = NULL;

	/*
	 * Check and wait till every valid file system has an entry in MNTTAB.
	 */

	do {
		for (i = 0, count = 0; validmounts[i]; i++) {

			if (svc_isFSMounted(mfp, validmounts[i],
			    &is_fsmounted)) {
				/* Error message logged. */
				return (1);
			}

			if (is_fsmounted) {

				/*
				 * Confirm the filesystem mount by statvfs
				 * also.
				 */
				if (svc_confirmFSMountbyStatvfs(
				    validmounts[i])) {
					/* Error message logged. */
					return (1);
				}
				/*
				 * SCMSGS
				 * @explanation
				 * HAStoragePlus certifies that the
				 * specified file system is in /etc/mnttab.
				 * @user_action
				 * This is an informational message, no user
				 * action is needed.
				 */
				hasp_scds_syslog_1(LOG_INFO,
				    "%s is confirmed as mounted.",
				    validmounts[i]->mountp);
				/*
				 * Now enable quota for the filesystem.
				 */
				svc_enableQuota(validmounts[i]);

			} else {

				/*
				 * Filesystem mount not confirmed. Add to the
				 * check list again.
				 */
				tmplist[count++] = validmounts[i];
			}
		}

		if (count == 0) {

			/*
			 * All file systems are confirmed as mounted.
			 */

			break;
		}

		tmplist[count] = NULL;

		tmpvalidmounts = validmounts;

		validmounts = tmplist;

		tmplist = tmpvalidmounts;

		(void) sleep(5);

	} while (1);


	/*
	 * SCMSGS
	 * @explanation
	 * Self explanatory.
	 * @user_action
	 * This is an informational message, no user action is needed.
	 */
	hasp_scds_syslog_0(LOG_INFO,
	    "Mounting the file systems is completed successfully.");

	/*
	 * It is not required to free memory as the process is going away
	 * after returning from here.
	 */

	return (0);
}

/*
 * Function: svc_populateMountPoints
 *
 * Arguments:
 *
 * fsp		The entry in which the mount points for all env's toi populate
 *
 * Return:
 * 		0	if mount points are populated successfully
 * 		1	if an error occurs
 *
 * Comments:
 * This function populates the mount points based on resource environment.
 */
static int
svc_populateMountPoints(fsentry_t *fsp)
{
	/*
	 * Initialize the mount points with NULL vlaues.
	 */
	fsp->mountp = NULL;
	fsp->lzpath = NULL;
	fsp->lzmountp = NULL;
	fsp->zcmountp = NULL;
	fsp->zcpath = NULL;

	switch (gSvcInfo.svcExecNode) {
		case	SVC_BASE_CLUSTER_GZ_NODE:
			/*
			 * In this case the mount point same as provided.
			 */
			fsp->mountp = fsp->svcp->vfsEntry.vfs_mountp;

			break;

		case SVC_BASE_CLUSTER_LZ_NODE_RUNS_IN_GZ:
			/*
			 * Here the actual mount happens in global zone and
			 * then lofs mount happens in local zone.
			 */
			/*
			 * The global zone mount point path is same as user
			 * provided.
			 */
			fsp->mountp = fsp->svcp->vfsEntry.vfs_mountp;

			/*
			 * For LZ, the actual mount point path is
			 * full path including zoneroot to user provided
			 * local zone mountpoint.
			 */
			fsp->lzmountp = fsp->svcp->svcLocalPath;
			fsp->lzpath = svc_getFullLocalPath(
			    gSvcInfo.zoneroot, fsp->lzmountp);
			if (fsp->lzpath == NULL) {
				/* Error message logged. */
				return (1);
			}

			break;

		case SVC_ZC_NODE_RUNS_IN_GZ:

			/*
			 * In case of zone cluster actual mount point path is
			 * full path including zoneroot to user provided
			 * zone cluster mountpoint.
			 */
			fsp->zcmountp = fsp->svcp->vfsEntry.vfs_mountp;
			fsp->zcpath = svc_getFullLocalPath(
			    gSvcInfo.zoneroot, fsp->zcmountp);
			if (fsp->zcpath == NULL) {
				/* Error message logged. */
				return (1);
			}

			/*
			 * Keep the user provided mount point in mountp also.
			 */
			fsp->mountp = fsp->svcp->vfsEntry.vfs_mountp;

			break;

		case SVC_ZC_NODE:
		case SVC_BASE_CLUSTER_LZ_NODE:
		case SVC_UNKNOWN_NODE:
		default:
			ASSERT(0);
			break;
	}
	return (0);
}

/*
 * Function: svc_isFSMounted
 *
 * Arguments:
 *
 * mfp		Pointer to MNTTAB file
 *
 * fsp		The entry which has mount points to be verified
 *
 * is_fsmounted	The local to store the status of mount.
 *
 * Return:
 * 		0	if mount points are verified in MNTTAB successfully.
 * 		1	if an error occurs
 *
 * Comments:
 * This function checks for the HAStoragePlus filesystem mount point status
 * by looking in to MNTTAB. It always tries to use resolved path of the
 * mount point while checking MNTTAB file.
 */
static int
svc_isFSMounted(FILE *mfp, fsentry_t *fsp, boolean_t *is_fsmounted)
{
	struct mnttab mp, mref;
	int rc;
	char	rpath[PATH_MAX];

	/*
	 * Rewind the MNTTAB file pointer to ensure we are searching
	 * entry search starts from the beginning.
	 */

	rewind(mfp);

	/*
	 * Initialize as the FS not found in MNTTAB.
	 */
	*is_fsmounted = B_FALSE;

	/*
	 * Nullify the elements in the mref mnttab structure.
	 */
	(void) memset(&mref, 0, sizeof (struct mnttab));

	switch (gSvcInfo.svcExecNode) {

		case	SVC_BASE_CLUSTER_GZ_NODE:

			/*
			 * Check the mount point of global zone.
			 */
			if (realpath(fsp->mountp, rpath) == NULL) {
				/*
				 * Unable to resolve the path. Use the
				 * unresolved path only.
				 */
				(void) strlcpy(rpath, fsp->mountp, PATH_MAX);
			}
			mref.mnt_mountp = rpath;

			rc = getmntany(mfp, &mp, &mref);
			if (rc == 0) {

				*is_fsmounted = B_TRUE;

			} else if (rc > 0) {
				/*
				 * Read error for mount point in MNTTAB.
				 */
				svc_reportMntError(rc, mref.mnt_mountp);
				return (1);
			}

			break;
		case SVC_BASE_CLUSTER_LZ_NODE_RUNS_IN_GZ:

			/*
			 * Check the mount point of global zone.
			 */
			if (realpath(fsp->mountp, rpath) == NULL) {
				/*
				 * Unable to resolve the path. Use the
				 * unresolved path only.
				 */
				(void) strlcpy(rpath, fsp->mountp, PATH_MAX);
			}
			mref.mnt_mountp = rpath;

			rc = getmntany(mfp, &mp, &mref);
			if (rc == 0) {

				/*
				 * mark global zone mount as complete.
				 */
				fsp->status |= FS_GLOBALDONE;

				/*
				 * Rewind the mnttab to search from beginning.
				 */
				rewind(mfp);

				/*
				 * Check the mount point for local zone.
				 */
				if (realpath(fsp->lzpath, rpath) == NULL) {
					/*
					 * Unable to resolve the path. Use the
					 * unresolved path only.
					 */
					(void) strlcpy(rpath, fsp->lzpath,
					    PATH_MAX);
				}
				mref.mnt_mountp = rpath;

				rc = getmntany(mfp, &mp, &mref);
				if (rc == 0) {

					*is_fsmounted = B_TRUE;

				} else if (rc > 0) {
					/*
					 * Read error for mount point in MNTTAB.
					 */
					svc_reportMntError(rc, mref.mnt_mountp);
					return (1);
				}
			} else if (rc > 0) {
				/*
				 * Read error for mount point in MNTTAB.
				 */
				svc_reportMntError(rc, mref.mnt_mountp);
				return (1);
			}

			break;

		case SVC_ZC_NODE_RUNS_IN_GZ:
			/*
			 * Check the mount point of zone cluster.
			 */
			if (realpath(fsp->zcpath, rpath) == NULL) {
				/*
				 * Unable to resolve the path. Use the
				 * unresolved path only.
				 */
				(void) strlcpy(rpath, fsp->zcpath, PATH_MAX);
			}
			mref.mnt_mountp = rpath;

			rc = getmntany(mfp, &mp, &mref);
			if (rc == 0) {

				*is_fsmounted = B_TRUE;

			} else if (rc > 0) {
				/*
				 * Read error for mount point in MNTTAB.
				 */
				svc_reportMntError(rc, mref.mnt_mountp);
				return (1);
			}

			break;

		case SVC_ZC_NODE:
		case SVC_BASE_CLUSTER_LZ_NODE:
		case SVC_UNKNOWN_NODE:
		default:
			ASSERT(0);
			break;
	}
	return (0);
}

/*
 * Function: svc_confirmMountbyStatvfs
 *
 * Arguments:
 *
 * fsentry_t	The filesystem mount point information.
 *
 * Return:
 *
 * 0		If the statvfs is successful on the mount point.
 *
 * 1		If statvfs fails on the mount point.
 *
 * Comments:
 * This functions will do statvfs() on the mount point.
 * This is one more level of confirming the mount other than checking the
 * mnttab. This level of checking is important in case of PxFS where the
 * file system will have mnttab entries though the file system is dead and not
 * available.
 */
static int
svc_confirmFSMountbyStatvfs(const fsentry_t *fsp)
{
	int		err, rc;
	struct statvfs  buf;

	switch (gSvcInfo.svcExecNode) {

		case	SVC_BASE_CLUSTER_GZ_NODE:

			/*
			 * Check the global zone mountpoint.
			 */
			do {
				err = statvfs(fsp->mountp, &buf);

			} while (err == -1 && errno == EINTR);

			if (err) {
				rc = errno;
				/*
				 * SCMSGS
				 * @explanation
				 * The file system specified on the mount
				 * point is not available.
				 * @user_action
				 * Check the statvfs(2) man page for errors
				 * and try to resolve the problem. Otherwise
				 * contact your authorized Sun service
				 * provider.
				 */
				hasp_scds_syslog_2(LOG_ERR,
				    "statvfs failed on the mount point "
				    "%s : %s",
				    fsp->mountp,	/* CSTYLED */
				    strerror(rc));	/*lint !e666 */
				return (1);
			}

			break;

		case SVC_BASE_CLUSTER_LZ_NODE_RUNS_IN_GZ:
			/*
			 * Check the global zone mountpoint.
			 */
			do {
				err = statvfs(fsp->mountp, &buf);

			} while (err == -1 && errno == EINTR);

			if (err) {
				rc = errno;
				hasp_scds_syslog_2(LOG_ERR,
				    "statvfs failed on the mount point "
				    "%s : %s",
				    fsp->mountp,	/* CSTYLED */
				    strerror(rc));	/*lint !e666 */
				return (1);
			}

			/*
			 * Check the local zone mountpoint.
			 */
			do {
				err = statvfs(fsp->lzpath, &buf);

			} while (err == -1 && errno == EINTR);

			if (err) {
				rc = errno;
				/*
				 * SCMSGS
				 * @explanation
				 * The file system specified on the mount
				 * point is not available for local zone.
				 * @user_action
				 * Check the statvfs(2) man page for errors
				 * and try to resolve the problem. Otherwise
				 * contact your authorized Sun service
				 * provider.
				 */
				hasp_scds_syslog_3(LOG_ERR,
				    "statvfs failed on the mount point "
				    "%s for local zone %s : %s",
				    fsp->lzmountp,
				    gSvcInfo.zonename,		/* CSTYLED */
				    strerror(rc));		/*lint !e666 */
				return (1);
			}

			break;

		case SVC_ZC_NODE_RUNS_IN_GZ:
			/*
			 * Check the zone cluster node mountpoint.
			 */
			do {
				err = statvfs(fsp->zcpath, &buf);

			} while (err == -1 && errno == EINTR);

			if (err) {
				rc = errno;
				/*
				 * SCMSGS
				 * @explanation
				 * The file system specified on the mount
				 * point is not available for zone cluster.
				 * @user_action
				 * Check the statvfs(2) man page for errors
				 * and try to resolve the problem. Otherwise
				 * contact your authorized Sun service
				 * provider.
				 */
				hasp_scds_syslog_3(LOG_ERR,
				    "statvfs failed on the mount point "
				    "%s for zone cluster %s node : %s",
				    fsp->zcmountp,
				    gSvcInfo.cl_name,		/* CSTYLED */
				    strerror(rc));		/*lint !e666 */
				return (1);
			}

			break;

		case SVC_ZC_NODE:
		case SVC_BASE_CLUSTER_LZ_NODE:
		case SVC_UNKNOWN_NODE:
		default:
			ASSERT(0);
			break;
	}

	return (0);
}

/*
 * Function: svc_enableQuota
 *
 * Arguments:
 *
 * fsp		The entry which contains the fs information.
 *
 * Return:
 * 		None
 *
 * Comments:
 * This function enable quotas for the filesystem.
 */
static void
svc_enableQuota(const fsentry_t *fsp)
{
	switch (gSvcInfo.svcExecNode) {

		case	SVC_BASE_CLUSTER_GZ_NODE:
			/* fall through */
		case	SVC_BASE_CLUSTER_LZ_NODE_RUNS_IN_GZ:
			/*
			 * Enable quota for UFS file systems.
			 */
			if (STREQ(fsp->svcp->vfsEntry.vfs_fstype, "ufs")) {
				svc_enableUFSQuota(&fsp->svcp->vfsEntry);
			}
			break;
		case SVC_ZC_NODE_RUNS_IN_GZ:
		case SVC_ZC_NODE:
		case SVC_BASE_CLUSTER_LZ_NODE:
		case SVC_UNKNOWN_NODE:
		default:
			break;
	}
}

/*
 *
 * Function: svc_enableUFSQuota
 *
 * Arguments:
 *
 * mp		The mnttab structure containing details of the file system
 *
 *
 * Return:	void
 *
 * Comments:
 * Checks vfstab mount options to see if quota is needed and then
 * enables quota for the UFS filesystem specified in vp.
 * Should be called only after a successful mount.
 * All errors are logged to syslog but mounting process is not stopped
 * for any error condition encountered while executing this routine
 * (as per requirement).
 *
 */
static void
svc_enableUFSQuota(const struct vfstab *vp)
{
	int cmd_status;
	char *cmd = NULL;
	char *cmd_output = NULL;
	char *mntopts = NULL;
	char *opt = NULL;
	int needquota = 0;

	if (vp->vfs_mntopts == NULL) {
		return;
	}

	/*
	 * We don't want to mess with the original mount options with strtok()
	 * Hence copy it.
	 */
	mntopts = strdup(vp->vfs_mntopts);

	/* Check the mount options to see if quotas are needed */
	opt = strtok(mntopts, ",");

	while (opt) {
		/* We don't need quotas for a global or readonly filesystem */
		if ((strcmp(opt, "global") == 0) ||
		    (strcmp(opt, "ro") == 0)) {
			free(mntopts);
			return;
		}

		/*
		 * The global or readonly option could have been specified
		 * after quota or rq option. Hence we need to continue
		 * checking every option and won't break the loop when
		 * needquota = 1
		 */
		if (needquota == 0) {
			if ((strcmp(opt, "rq") == 0) ||
			    (strcmp(opt, "quota") == 0))
				needquota = 1;
		}

		opt = strtok(NULL, ",");
	}

	free(mntopts);

	if (needquota == 0)
		return; /* Filesystem doesn't require quota */

	/* Filesystem requires quota */
	/*
	 * SCMSGS
	 * @explanation
	 * The UFS mount point specified by %s, which has quotas enabled, is
	 * being checked using /usr/sbin/quotacheck.
	 * @user_action
	 * This is an informational message. No action is necessary.
	 */
	hasp_scds_syslog_1(LOG_INFO,
	    "Checking UFS quota for %s", vp->vfs_mountp);

	if (svc_sprintf(&cmd, "%s %s", QUOTACHECK, vp->vfs_mountp)) {
		hasp_scds_syslog_1(LOG_ERR,
		    "Failed to construct command %s.", QUOTACHECK);
		return;
	}

	cmd_status = svc_executeCmdusingFork(cmd, &cmd_output);

	if (cmd_status != 0) {
		/* Error code 32 means no quotas file found */
		if (cmd_status == 32) {
			/*
			 * SCMSGS
			 * @explanation
			 * The UFS mount point has quotas enabled but there is
			 * no "quotas" file under the mounted file system.
			 * @user_action
			 * Create a "quotas" file under the mounted file
			 * system if quotas are desired. Check the man page of
			 * quotacheck(1M). If the quotas file is created you
			 * must restart the corresponding SUNW.HAStoragePlus
			 * resource.
			 */
			hasp_scds_syslog_2(LOG_ERR,
			    "No %s/quotas file exists - not starting quotas "
			    "for %s", vp->vfs_mountp, vp->vfs_mountp);
		} else {
			/*
			 * SCMSGS
			 * @explanation
			 * The /usr/sbin/quotacheck command failed for
			 * $mountp.
			 * @user_action
			 * The system administrator should run the
			 * /usr/sbin/quotacheck command manually. If that
			 * succeeds, also run /usr/sbin/quotaon. If the
			 * quotacheck attempt fails, stop any applications
			 * that are using the file system and attempt the
			 * quotacheck command again.
			 */
			hasp_scds_syslog_2(LOG_ERR,
			    "Error %d running quotacheck for %s",
			    cmd_status, vp->vfs_mountp);
		}

		/* Log whatever error output we got */
		if (cmd_output)
			/*
			 * SCMSGS
			 * @explanation
			 * Need explanation of this message!
			 * @user_action
			 * Need a user action for this message.
			 */
			hasp_scds_syslog_1(LOG_ERR, "%s", cmd_output);

		if (cmd)
			free(cmd);

		if (cmd_output)
			free(cmd_output);

		return;
	}

	/*
	 * SCMSGS
	 * @explanation
	 * The quotas for the UFS mountpoint specified by %s are going to be
	 * enabled.
	 * @user_action
	 * This is an informational message. No action is necessary.
	 */
	hasp_scds_syslog_1(LOG_INFO,
	    "Starting UFS quotas for %s", vp->vfs_mountp);

	if (svc_sprintf(&cmd, "%s %s", QUOTAON, vp->vfs_mountp)) {
		hasp_scds_syslog_1(LOG_ERR,
		    "Failed to construct command %s.", QUOTAON);
		return;
	}

	cmd_status = svc_executeCmdusingFork(cmd, &cmd_output);

	if (cmd_status != 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * The /usr/sbin/quotaon command failed for the UFS mount
		 * point specified by %s.
		 * @user_action
		 * Contact your Sun service representative.
		 */
		hasp_scds_syslog_2(LOG_ERR,
		    "Error %d starting quota for %s",
		    cmd_status, vp->vfs_mountp);

		/* Log whatever error output we got */
		if (cmd_output)
			hasp_scds_syslog_1(LOG_ERR, "%s", cmd_output);
	}

	if (cmd)
		free(cmd);

	if (cmd_output)
		free(cmd_output);
}

/*
 *
 * Function: svc_doParMount
 *
 * Arguments:
 *
 * mntlist	The list of mount entries which has to be mounted.
 *
 * nentries	The number of entries to be mounted.
 *
 * Return:
 *
 * 0		if no entries to mount, or
 *		if all file systems specified in mntlist were mounted
 *		successfully
 *
 * 1		if an error occurred.
 *
 * Comments:
 * This routine mounts the file systems in parallel similar to Solaris mount -a.
 */

static int
svc_doParMount(fsentry_t **mntlist, unsigned int nentries)
{

	fsentry_t	**mntpp,
			**tmntpp,
			*mntprev,
			*mntcur; /* Pointers to pass through mount entries */
	int		mount_failed = 0;
	int		i;
	hasp_strbuf_t	mount_failure_msg;

	/*
	 * Return if no file systems to mount.
	 */

	if (nentries == 0) {
		return (0);
	}

	/*
	 * The following mount logic is taken from mount command code
	 * (mount.c)
	 */

	for (mntpp = mntlist, mntprev = *mntlist; ((mntcur = *mntpp) != NULL);
	    mntprev = mntcur, mntpp++, nentries--) {

		/*
		 * Check to see if we cross a mount level: e.g.,
		 * /a/b -> /a/b/c. Is so wait for current mounts to finish,
		 * rerun realpath on the remaining mount points and re-sort
		 * the list.
		 */

		if (mntcur->depth > mntprev->depth) {

			/*
			 * Wait for the threads which are delegated for
			 * mounting since there is possibility for dependency
			 * due to nested mount points in earlier level.
			 */

			svc_wait4tasksinThreadPool();

			for (tmntpp = mntpp; *tmntpp; tmntpp++) {

				/*
				 * There is chance for changes in real
				 * path due to existence of symbolic links.
				 * Recalculate the number of components
				 * for mount point's changed real path.
				 */

				if (svc_setrpath(*tmntpp)) {
					/* Error message logged. */
					return (1);
				}

				(*tmntpp)->depth =
				    svc_getpathdepth((*tmntpp)->rpath);
			}

			/*
			 * Sort the remaining mount entries.
			 */

			qsort((void *) mntpp, nentries, sizeof (fsentry_t *),
				svc_mntcompar);

			mntcur = *mntpp;
		}

		/*
		 * Add the mount task to the threadpool.
		 */

		(void) svc_addTasktoThreadPool(svc_doMount, (void *) mntcur);

	} /* End of mount "for" loop */

	/*
	 * Wait for mount tasks completion.
	 */

	svc_wait4tasksinThreadPool();

	/*
	 * Check mount points status.
	 */

	for (i = 0; mntlist[i]; i++) {
		if (mntlist[i]->status & FS_MOUNTFAILED) {
			mount_failed = 1;
			break;
		}
	}

	if (mntlist[i]) {

		/*
		 * Some file systems are failed to mount.
		 * Log the error message and also update status message.
		 */

		mount_failure_msg.str = NULL;

		ADD_STR2STRBUF(mount_failure_msg, "Failed to mount: ");

		for (; mntlist[i]; i++) {

			if (mntlist[i]->status & FS_MOUNTFAILED) {

				ADD_STR2STRBUF(mount_failure_msg,
				    mntlist[i]->mountp);
				ADD_STR2STRBUF(mount_failure_msg, " ");
			}
		}

		/*
		 * Add to status message failing mount points.
		 * Message will be of the form : (On one line only)
		 *
		 * -- Failed to mount: m1 m2 [...].
		 *
		 * where m1 m2 stands for the mount points.
		 * NOTE:
		 * The above similar format is applicable for fsck/umount
		 * failures.
		 */

		ADD_TO_STATUS_MSG_IF_NOT_EMPTY(" -- ");
		ADD_TO_STATUS_MSG(mount_failure_msg.str);

		hasp_scds_syslog_0(LOG_ERR, mount_failure_msg.str);
	}

	return (mount_failed);
}

/*
 * Function: svc_doParFsck
 *
 * Arguments:
 * fsck_list	The list of fsck entries which has to be fsck'ed.
 *
 * Return:
 *
 * 0		if no entries to fsck
 *
 * 0		if all entries are fscked successfully
 *
 * 1		if an error occurred.
 *
 * Comments:
 * This function runs fsck on the given file systems entries.
 * The file systems with an fsckpass number 1 specified in vfstab, will be
 * completed first sequentially and the other file systems with an fsckpass
 * number greater than 1 are checked in parallel.
 */

static int
svc_doParFsck(fsentry_t *const *fsck_list, unsigned int nentries)
{
	int		fsck_failed = 0;
	int		passno = 0;
	int		i;
	hasp_strbuf_t	fsck_failure_msg;

	/*
	 * Return if there are no entries to fsck.
	 */

	if (nentries == 0)
		return (0);

	/*
	 * Find file systems with an fsckpass number 1 and do fsck sequentially.
	 */

	for (i = 0; fsck_list[i]; i++) {

		char *fsckpass = fsck_list[i]->svcp->vfsEntry.vfs_fsckpass;
		if (fsckpass != NULL)
			passno = atoi(fsckpass);
		if (passno < 1)
			continue;
		else if (passno == 1) {
			fsck_list[i]->status |= FS_FSCKPROCESSED;
			if (!(fsck_list[i]->status & FS_GLOBALDONE))
				svc_doFsck((void *)fsck_list[i]);
			if (fsck_list[i]->status & FS_FSCKFAILED) {
				fsck_failed = 1;
			}
		}
	}

	/*
	 * Log error message if fsck failed on file systems with fsckpass = 1.
	 */

	if (fsck_failed)
		goto log_fsck_failure;
	/*
	 * Now the file systems with fsckpass > 1, are added to threadpool
	 * to execute fsck in parallel.
	 */

	for (i = 0; fsck_list[i]; i++) {

		if (!(fsck_list[i]->status & FS_FSCKPROCESSED)) {
			fsck_list[i]->status |= FS_FSCKPROCESSED;
			if (!(fsck_list[i]->status & FS_GLOBALDONE))
				(void) svc_addTasktoThreadPool(svc_doFsck,
				    (void *)fsck_list[i]);
		}
	}

	/*
	 * Wait for completion of fsck tasks.
	 */

	svc_wait4tasksinThreadPool();

	/*
	 * Check the status of fsck on all the file systems and return error if
	 * any one fails.
	 */

log_fsck_failure:

	for (i = 0; fsck_list[i]; i++) {

		if ((fsck_list[i]->status & FS_FSCKPROCESSED) &&
		    (fsck_list[i]->status & FS_FSCKFAILED)) {
			fsck_failed = 1;
			break;
		}
	}

	if (fsck_list[i]) {

		/*
		 * Some file systems are failed in fsck.
		 * Log error message and update status message.
		 */

		fsck_failure_msg.str = NULL;
		ADD_STR2STRBUF(fsck_failure_msg, "Failed to fsck : ");

		for (; fsck_list[i]; i++) {

			if ((fsck_list[i]->status & FS_FSCKPROCESSED) &&
			    (fsck_list[i]->status & FS_FSCKFAILED)) {

				ADD_STR2STRBUF(fsck_failure_msg,
				    fsck_list[i]->mountp);
				ADD_STR2STRBUF(fsck_failure_msg, " ");
			}
		}

		ADD_TO_STATUS_MSG_IF_NOT_EMPTY(" -- ");
		ADD_TO_STATUS_MSG(fsck_failure_msg.str);

		hasp_scds_syslog_0(LOG_ERR, fsck_failure_msg.str);
	}

	return (fsck_failed);
}

/*
 * Function: svc_umountFilesystems
 *
 * Arguments:
 * umnts_array	The array of mount points which has to be unmounted.
 *
 * Return:
 *
 * 0		if no entries to unmount or
 *		if all mount points in list are unmounted successfully
 *
 * 1		if an error occurred.
 *
 * Comments:
 * The mount points which are specified in umount array are unmounted.
 */
int
svc_umountFilesystems(const scha_str_array_t  *umnts_array)
{
	fsentry_t	**umntlist; /* Contains the list of umount entries */
	uint_t		nentries;	/* Number of file systems to unmount */
	int		umount_failed = 0;	/* unmount status */
	int		rc;
	uint_t		count, i;
	FILE		*mfp = NULL;
	struct mnttab	mp, mref;
	char		buffer[SVC_BUFFER_LEN];
	hasp_strbuf_t	umntpts_string;
	fsentry_t	**tmplist, **localmounts, **tmplocalmounts;
	int		tries;
	hasp_strbuf_t	umount_failure_msg;
	int		numlofs = 0;
	char		*cmd = NULL;
	char		*cmd_output = NULL;

	/*
	 * Return if no entry in the umount array.
	 */

	scds_syslog_debug(DBG_LEVEL_HIGH, "No.of entries to umount %d",
	    umnts_array->array_cnt);

	if (umnts_array->array_cnt == 0) {
		return (0);
	}

	/*
	 * Open the MNTTAB file used to check if a file system is unmounted
	 * already or not.
	 * NOTE:
	 * While unmouting it is not required to bother about the mount
	 * point entry existence in VFSTAB.
	 */

	if ((mfp = fopen(MNTTAB, "r")) == NULL) {

		rc = errno;

		hasp_scds_syslog_2(LOG_ERR, "Failed to open %s: %s.",
		    MNTTAB,		/* CSTYLED */
		    strerror(rc));	/*lint !e666 */

		return (1);
	}

	/*
	 * Allocate space for maximum possible unmount entries in the list.
	 */

	umntlist = (fsentry_t **)calloc((umnts_array->array_cnt +1),
	    sizeof (fsentry_t *));

	if (umntlist == NULL) {

		hasp_scds_syslog_0(LOG_ERR, "Failed to allocate memory.");
		return (1);
	}

	/*
	 * Nullify the elements in the mnttab structure.
	 */

	(void) memset(&mref, 0, sizeof (struct mnttab));

	for (i = 0, count = 0; i < umnts_array->array_cnt; i++) {

		if (umntlist[count] == NULL) {

			umntlist[count] =
			    (fsentry_t *)malloc(sizeof (fsentry_t));

			if (umntlist[count] == NULL) {

				hasp_scds_syslog_0(LOG_ERR,
				    "Failed to allocate memory.");

				return (1);

			}

			umntlist[count]->rpath = NULL;

		}

		umntlist[count]->mountp = umnts_array->str_array[i];


		if (svc_setrpath(umntlist[count])) {
			/* Error message logged. */
			return (1);
		}

		/*
		 * Set the depth so that we can sort the unmounts in reverse
		 * depth order
		 */
		umntlist[count]->depth =
		    svc_getpathdepth(umntlist[count]->rpath);

		/*
		 * Rewind the MNTTAB file pointer, so that the next mount point
		 * entry search starts from the beginning.
		 */

		rewind(mfp);

		/*
		 * Check the file system mount point existence in MNTTAB, using
		 * the real path.
		 */

		mref.mnt_mountp = umntlist[count]->rpath;
		rc = getmntany(mfp, &mp, &mref);

		if (rc == -1) {

			/*
			 * Reached end of file i.e file system is found to be
			 * already unmounted. Log a informational message and
			 * continue. Returning an error here would make the
			 * stop method non-idempotent.
			 */

			/*
			 * SCMSGS
			 * @explanation
			 * HAStoragePlus found that the specified mount
			 * point was unmounted but should not have been.
			 * @user_action
			 * This is an informational message, no user action is
			 * needed.
			 */
			hasp_scds_syslog_1(LOG_INFO,
			    "%s is erroneously found to be "
			    "unmounted.", mref.mnt_mountp);

			continue;

		} else if (rc == 0) {

			/*
			 * For QFS we rely on vfstab file to check
			 * for the shared mount options.
			 */
			if (strcmp(mp.mnt_fstype, "samfs") == 0) {

				struct vfstab vp, vref;
				FILE *fp = NULL;
				int ret;

				if ((fp = fopen(VFSTAB, "r")) == NULL) {

					ret = errno;    /*lint !e746 */
					hasp_scds_syslog_2(LOG_ERR,
					    "Failed to open %s: %s.", VFSTAB,
					    strerror(ret));	/*lint !e666 */

					return (1);
				}

				(void) memset(&vref, 0, sizeof (struct vfstab));
				vref.vfs_mountp = (char *)mp.mnt_mountp;

				/*
				 * Obtain a filled 'struct vfstab' entry for the
				 * given mount point.
				 */
				ret = getvfsany(fp, &vp, &vref);

				if (ret > 1)  {
					svc_reportVfsError(ret,
						vref.vfs_mountp);
					(void) fclose(fp);
					return (1);
				}

				rc = svc_evalVfsTabMountOption(&vp, buffer);

				(void) fclose(fp);
			} else {
				/*
				 * Mount point exists in MNTTAB (file system is
				 * mounted). HAStoragePlus unmounts only local
				 * file systems.
				 */

				rc = svc_evalMntTabMountOption(&mp, buffer);
			}

			if ((rc == SvcMountOptLocal) || (rc == SvcMountLofs)) {
				if (rc == SvcMountLofs) {
					numlofs++;

					cmd = NULL;
					cmd_output = NULL;

					/*
					 * When HAStoragePlus tries to bring
					 * the resource offline, the processes
					 * related to data services will not be
					 * accessing the lofs file system
					 * managed by it.
					 * There are chances that unrelated
					 * processes will be accessing while
					 * the resource is brought offline.
					 * Since force unmount option is not
					 * available for lofs file system,
					 * the process which are accessing the
					 * will be killed first and later
					 * umount is issued.
					 */
					if (svc_sprintf(&cmd, "%s -k -c %s",
					    FUSER, mref.mnt_mountp)) {
						hasp_scds_syslog_0(LOG_ERR,
						    "Failed to allocate "
						    "memory.");
						return (1);
					}
					(void) svc_executeCmdusingFork(cmd,
					    &cmd_output);
					if (cmd)
						free(cmd);
					if (cmd_output)
						free(cmd_output);
				}


				/*
				 * Add this to umount list.
				 * NOTE: Incrementing count leaves the current
				 * umount point entry in umount list and
				 * points to next empty slot.
				 */

				count++;

				/*
				 * SCMSGS
				 * @explanation
				 * HAStoragePlus found the specified mount
				 * point mounted as a local file system. It
				 * will unmount it, as asked for.
				 * @user_action
				 * This is an informational message, no user
				 * action is needed.
				 */
				hasp_scds_syslog_1(LOG_INFO,
				    "%s is locally mounted. Unmounting it.",
				    mref.mnt_mountp);

			} else if (rc ==  SvcMountOptGlobal) {

				scds_syslog_debug(DBG_LEVEL_HIGH,
				    "%s is a global file system, not "
				    "unmounting it", mref.mnt_mountp);

			} else {

				/*
				 * Not a local/global mount point. Log warning
				 * and proceed to umount remaining entries.
				 */

				/*
				 * SCMSGS
				 * @explanation
				 * Self explanatory.
				 * @user_action
				 * This is a warning message. If the file
				 * system that is associated with the
				 * specified mount point is not available,
				 * copy the /var/adm/messages and contact an
				 * authorized Sun service provider.
				 */
				hasp_scds_syslog_1(LOG_WARNING,
				    "%s is not a local or a global mount point "
				    "in /etc/mnttab.", mref.mnt_mountp);

			}
		} else {

			/*
			 * Read error for mount point in MNTTAB.
			 */

			svc_reportMntError(rc, mref.mnt_mountp);
			return (1);
		}
	}

	/*
	 * Mark the end position for the list of umount entries.
	 */

	umntlist[count] = NULL;

	nentries = count;

	/*
	 * Return if the number of entries is zero.
	 */

	scds_syslog_debug(DBG_LEVEL_HIGH, "The no.of entries in umount list "
		" %d", nentries);

	if (nentries == 0) {

		/* All mount entries are already mounted */

		return (0);
	}

	/*
	 * sort filesystem mount points in depth first order. This
	 * is because umount -a does not sort mountpoints if any filesystems
	 * are lofs.
	 */

	qsort((void *)umntlist, nentries, sizeof (fsentry_t *), svc_umntcompar);

	umntpts_string.str = NULL;

	for (i = 0; umntlist[i]; i++) {

		ADD_STR2STRBUF(umntpts_string, umntlist[i]->mountp);
		ADD_STR2STRBUF(umntpts_string, " ");
	}

	/*
	 * Unmount the file systems in the umntlist list.
	 */

	if (svc_doParUmount(umntpts_string.str, numlofs)) {
		return (1);
	}

	/*
	 * Confirm the umount status by checking MNTTAB file.
	 */

	tmplist = (fsentry_t **)malloc(sizeof (fsentry_t *) * (nentries+1));

	if (tmplist == NULL) {

		hasp_scds_syslog_0(LOG_ERR, "Failed to allocate memory.");

		return (1);
	}

	localmounts = (fsentry_t **)malloc(sizeof (fsentry_t *) * (nentries+1));

	if (localmounts == NULL) {

		hasp_scds_syslog_0(LOG_ERR, "Failed to allocate memory.");

		return (1);

	}

	for (i = 0; umntlist[i]; i++) {

			localmounts[i] = umntlist[i];
	}

	localmounts[i] = NULL;

	tries = 0;

	do {
		for (i = 0, count = 0; localmounts[i]; i++) {

			(void) memset(&mref, 0, sizeof (struct mnttab));

			mref.mnt_mountp = localmounts[i]->rpath;

			rewind(mfp);

			rc = getmntany(mfp, &mp, &mref);

			if (rc == -1) {

				/*
				 * SCMSGS
				 * @explanation
				 * HAStoragePlus certifies that the
				 * specified file system is not in
				 * /etc/mnttab.
				 * @user_action
				 * This is an informational message, no user
				 * action is needed.
				 */
				hasp_scds_syslog_1(LOG_INFO,
				    "%s is confirmed as unmounted.",
				    localmounts[i]->rpath);

				localmounts[i]->status |= FS_UMOUNTCONFIRM;

			} else {

				tmplist[count++] = localmounts[i];
			}
		}

		if (count == 0) {

			/*
			 * All file systems are confirmed as unmounted.
			 */

			break;
		}

		tmplist[count] = NULL;

		tmplocalmounts = localmounts;

		localmounts = tmplist;

		tmplist = tmplocalmounts;

		/*
		 * If umount failed, we cannot loop forever if we are doing an
		 * Update. Let's sleep 3 times at most (i.e. check 4 times).
		 */

		if ((svcMethodId == HASTORAGEPLUS_UPDATE) && (tries == 3)) {

			break;
		}

		tries++;

		(void) sleep(5);

	} while (1);

	/*
	 * Check umount status of the file systems.
	 */

	for (i = 0; umntlist[i]; i++) {

		if (!(umntlist[i]->status & FS_UMOUNTCONFIRM)) {

			umount_failed = 1;

			break;
		}
	}

	if (umntlist[i]) {

		/*
		 * Some file systems are failed to unmout.
		 * Log the error message and also update status message.
		 */

		umount_failure_msg.str = NULL;

		ADD_STR2STRBUF(umount_failure_msg, "Failed to umount :");

		for (; umntlist[i]; i++) {

			if (!(umntlist[i]->status & FS_UMOUNTCONFIRM)) {

				ADD_STR2STRBUF(umount_failure_msg,
				    umntlist[i]->mountp);
				ADD_STR2STRBUF(umount_failure_msg, " ");
			}
		}

		ADD_TO_STATUS_MSG_IF_NOT_EMPTY(" -- ");
		ADD_TO_STATUS_MSG(umount_failure_msg.str);

		hasp_scds_syslog_0(LOG_ERR, umount_failure_msg.str);
	}

	return (umount_failed);
}

/*
 * Function: svc_doParUmount
 *
 * Arguments:
 *
 * umnt_pts	The mount points of the file systems which has to be unmounted
 *
 * numlofs	the number of mount points in umnt_pts that are lofs mounts.
 *
 * Return:
 *
 * 0		if umount is done successfully
 *
 * 1		if an error occurred.
 *
 * Comments:
 * This function unmounts the file systems with force option.
 */

static int
svc_doParUmount(char *umnt_pts, int numlofs)
{
	char	*cmd = NULL;
	char	*cmd_output = NULL;
	int	rc = 0;
	int	cmd_status;

	/*
	 * The umount error is also redirected to stdout. In case the command
	 * succeeds the output is stored in cmd_output, otherwise it contains
	 * the failure message.
	 */

	if (numlofs != 0) {
		/*
		 * Form umount command having umount entries and the parallel
		 * option. Not forced. This is because lofs does not support
		 * forced unmount.
		 */
		if (svc_sprintf(&cmd, "%s %s 2>&1", UMOUNTA, umnt_pts)) {
			return (1);
		}
	} else {
		/*
		 * Form umount command having umount entries with force and
		 * parallel option.
		 * i.e umount -f -a "file_systems" 2>&1
		 */

		if (svc_sprintf(&cmd, "%s %s 2>&1", UMOUNTFA, umnt_pts)) {
			return (1);
		}
	}

	/*
	 * SCMSGS
	 * @explanation
	 * HAStoragePlus will umount the underlying device corresponding to
	 * the mount point that is specified in /etc/mnttab.
	 * @user_action
	 * This is an informational message, no user action is needed.
	 */
	hasp_scds_syslog_1(LOG_INFO, "About to umount %s.", umnt_pts);

	cmd_status = svc_executeCmdusingPopen(cmd, &cmd_output);

	if (cmd_status == 0) {

		/*
		 * Unmount succeeded.
		 */
		/*
		 * SCMSGS
		 * @explanation
		 * HAStoragePlus successfully mounted the specified file
		 * system.
		 * @user_action
		 * This is an informational message, no user action is needed.
		 */
		hasp_scds_syslog_1(LOG_INFO,
		    "Successfully unmounted %s.", umnt_pts);

	} else {

		/*
		 * Unmount command failed.
		 */

		/*
		 * SCMSGS
		 * @explanation
		 * HAStoragePlus was not able to unmount the specified file
		 * system. The return code and output of the umount command is
		 * also embedded in the message.
		 * @user_action
		 * Check the system configuration. If the problem persists,
		 * contact your authorized Sun service provider.
		 */
		hasp_scds_syslog_3(LOG_ERR, "Failed to unmount %s: (%d) %s.",
		    umnt_pts, cmd_status, cmd_output);
		rc = 1;
	}

	if (cmd)
		free(cmd);
	if (cmd_output)
		free(cmd_output);

	return (rc);
}

/*
 * Function: svc_umountFMPEntries
 *
 * Arguments:
 * fmp_entries	The list of filesystem mountpoints to be unmounted.
 *
 * Return:
 *
 * 0		if all mount points in list are unmounted successfully
 *
 * 1		if an error occurred.
 *
 * Comments:
 * This function converts the FMP entries to list of absolute entries
 * to umount. And later invokes the function to unmount those entries.
 */
int
svc_umountFMPEntries(const scha_str_array_t *fmp_entries)
{
	HASPMethodExecutionNode svcExecNode = gSvcInfo.svcExecNode;
	uint_t	i;
	char	*mountp = NULL;		/* Mount path for gz */
	char	*lzmountp = NULL;	/* Mount path for local zone */
	char	*zcmountp = NULL;	/* Mount path for zone cluster */
	char	*lzpath = NULL;	/* Full path of local zone in MNTTAB */
	char	*zcpath = NULL;	/* Full path of zone cluster in MNTTAB */
	scha_str_array_t gz_mntpts;
	scha_str_array_t lz_mntpts;
	scha_str_array_t zc_mntpts;

	/*
	 * Nullify the default values in mount points.
	 */
	gz_mntpts.array_cnt = 0;
	gz_mntpts.str_array = NULL;
	lz_mntpts.array_cnt = 0;
	lz_mntpts.str_array = NULL;
	zc_mntpts.array_cnt = 0;
	zc_mntpts.str_array = NULL;

	for (i = 0; i < fmp_entries->array_cnt; i++) {

		if ((svcExecNode == SVC_BASE_CLUSTER_GZ_NODE) ||
		    (svcExecNode == SVC_BASE_CLUSTER_LZ_NODE_RUNS_IN_GZ)) {

			char *localpath, *globalpath, *tmp;
			char *delimiter = PATH_DELIM;
			/*
			 * Determine if the mount point is a local:global
			 * or a global style mount point. Parse apart
			 * the mountpoints if needed.
			 */

			tmp = strdup(fmp_entries->str_array[i]);
			if (tmp == NULL) {
				hasp_scds_syslog_0(LOG_ERR,
				    "Failed to allocate memory.");
				return (1);
			}

			localpath = strtok(tmp, delimiter);
			globalpath = strtok(NULL, delimiter);

			if (globalpath == NULL) {
				mountp = strdup(localpath);
			} else {
				mountp = strdup(globalpath);
			}

			lzmountp = strdup(localpath);

			if ((mountp == NULL) || (lzmountp == NULL)) {
				hasp_scds_syslog_0(LOG_ERR,
				    "Failed to allocate memory.");
				return (1);
			}
			free(tmp);
		} else {
			zcmountp = fmp_entries->str_array[i];
		}

		/*
		 * Construct and copy the paths that presents in MNTTAB.
		 */
		switch (svcExecNode) {
			case SVC_BASE_CLUSTER_GZ_NODE:

				/* Add the global zone list. */
				if (svc_addStr2Array(mountp, &gz_mntpts)) {
					return (1);
				}

				break;

			case SVC_BASE_CLUSTER_LZ_NODE_RUNS_IN_GZ:

				/* Add the global zone list. */
				if (svc_addStr2Array(mountp, &gz_mntpts)) {
					return (1);
				}

				/* Get local zone full path and add it. */
				lzpath = svc_getFullLocalPath(
				    gSvcInfo.zoneroot, lzmountp);
				if (lzpath == NULL) {
					/* Error is already logged. */
					return (1);
				}
				if (svc_addStr2Array(lzpath, &lz_mntpts)) {
					return (1);
				}

				break;

			case SVC_ZC_NODE_RUNS_IN_GZ:

				/* Get zone cluster full path and add it. */
				zcpath = svc_getFullLocalPath(
				    gSvcInfo.zoneroot, zcmountp);
				if (zcpath == NULL) {
					/* Error is already logged. */
					return (1);
				}
				if (svc_addStr2Array(zcpath, &zc_mntpts)) {
					return (1);
				}

				break;

			case SVC_ZC_NODE:
			case SVC_BASE_CLUSTER_LZ_NODE:
			case SVC_UNKNOWN_NODE:
			default:
				ASSERT(0);
				break;
		}
	}

	/*
	 * Now execute umount on the generated list.
	 */
	switch (svcExecNode) {
		case SVC_BASE_CLUSTER_GZ_NODE:
			/*
			 * Umount the file systems in global zone.
			 */
			if (svc_umountFilesystems(&gz_mntpts)) {
				/*
				 * Error message on failed mounts already
				 * logged.
				 */
				return (1);
			}
			break;

		case SVC_BASE_CLUSTER_LZ_NODE_RUNS_IN_GZ:

			/*
			 * Umount the file systems in local zone first.
			 */
			if (svc_umountFilesystems(&lz_mntpts)) {
				/*
				 * Error message on failed mounts already
				 * logged.
				 */
				return (1);
			}
			/*
			 * Umount the file systems in global zone.
			 */
			if (svc_umountFilesystems(&gz_mntpts)) {
				/*
				 * Error message on failed mounts already
				 * logged.
				 */
				return (1);
			}
			break;
		case SVC_ZC_NODE_RUNS_IN_GZ:

			/*
			 * Umount the file systems of zone cluster.
			 */
			if (svc_umountFilesystems(&zc_mntpts)) {
				/*
				 * Error message on failed mounts already
				 * logged.
				 */
				return (1);
			}
			break;
		case SVC_ZC_NODE:
		case SVC_BASE_CLUSTER_LZ_NODE:
		case SVC_UNKNOWN_NODE:
		default:
			ASSERT(0);
			break;
	}
	return (0);
}

static char OLDMNTS_FILE[MAXPATHLEN];

/*
 *
 * Function: svc_saveOldMntPts
 *
 * Arguments: none
 *
 * Return:
 *
 * 0		if all the old mount points are saved in temporary file
 *		successfully.
 *
 * 1		if an error occurred.
 *
 * Comments:
 * This routine is used to save the old mount points.
 * When update is done on the HAStoragePlus resource, RGM invokes first validate
 * method and invokes update iff validate is success. When validate completes
 * successfully, RGM updates the resource with the new properties by overriding
 * the old properties. Since the old mount points are required (to unmount the
 * FMP which are removed for update), they are stored in temporary file.
 */

int
svc_saveOldMntPts()
{
	scha_resource_t		handle;
	scha_extprop_value_t	*fmp;
	uint_t			fmpcount, i;
	FILE			*fp;
	int			rc = 0;
	scha_err_t		err;

	/*
	 * Get the file name to store old mount points.
	 */

#if SOL_VERSION <= __s9
	(void) snprintf(OLDMNTS_FILE, MAXPATHLEN,
	    "/var/cluster/run/HAStoragePlus_oldmounts.%s.%s",
	    gSvcInfo.rsName, gSvcInfo.rgName);
#else
	(void) snprintf(OLDMNTS_FILE, MAXPATHLEN,
	    "/var/cluster/run/HAStoragePlus_oldmounts.%s.%s.%s",
	    gSvcInfo.cl_name, gSvcInfo.rsName, gSvcInfo.rgName);
#endif

	fp = fopen(OLDMNTS_FILE, "w");

	if (fp == NULL) {

		rc = errno;

		hasp_scds_syslog_2(LOG_ERR,
		    "Failed to open %s: %s.",
		    OLDMNTS_FILE,	/* CSTYLED */
		    strerror(rc));	/*lint !e666 */

		return (1);
	}

	/*
	 * SCHA API is used instead of scds_get_ext_property() to get old
	 * file system mount points.
	 * REASON::
	 * SCDS API (scds_get_ext_property) first looks in the list of
	 * properties specified in the method argument list which contains the
	 * new arguments and then uses scha API if property is not found.
	 * To avoid reading new arguments scha API is used which gets from
	 * CCR which contains old mount points.
	 */

	err = scha_resource_open_zone(gSvcInfo.cl_name, gSvcInfo.rsName,
	    gSvcInfo.rgName, &handle);

	if (err != SCHA_ERR_NOERR) {

		hasp_scds_syslog_2(LOG_ERR,
		    "Failed to open the resource %s handle: %s.",
						/* CSTYLED */
		    gSvcInfo.rsName,
		    scds_error_string(err));	/*lint !e666 */

		return (1);
	}

	err = scha_resource_get_zone(gSvcInfo.cl_name, handle, SCHA_EXTENSION,
	    FILESYSTEMMOUNTPOINTS, &fmp);

	if (err != SCHA_ERR_NOERR) {

		hasp_scds_syslog_2(LOG_ERR,
		    "Failed to retrieve property %s: %s.",
		    FILESYSTEMMOUNTPOINTS,	/* CSTYLED */
		    scds_error_string(err));	/*lint !e666 */

		rc = 1;

		goto out;
	}

	fmpcount = fmp->val.val_strarray->array_cnt;

	for (i = 0; i < fmpcount; i++) {

		if ((fprintf(fp, "%s\n",
		    fmp->val.val_strarray->str_array[i])) < 0) {

			/* This shouldn't happen */

			rc = errno;
			hasp_scds_syslog_2(LOG_ERR,
			    "Failed to write to file %s: %s.",
			    OLDMNTS_FILE,	/* CSTYLED */
			    strerror(rc));	/*lint !e666 */
			rc = 1;
			goto out;
		}
	}

out:
	(void) scha_resource_close(handle);
	(void) fclose(fp);
	return (rc);
}


static int
svc_mntpts_compar(const void *mnt_a, const void *mnt_b)
{
	return (strcmp(*(char **)mnt_a, *(char **)mnt_b));
}

/*
 *
 * Function: svc_umountRemovedMntPts
 *
 * Arguments: none
 *
 * Return:
 *
 * 0		if all the removed mount points are unmounted.
 *
 * 1		if an error occurred.
 *
 * Comments:
 * This routine is used to unmount the file systems which are removed
 * during update operation.
 * This routine prepares the list of file systems to be unmounted and calls
 * svc_umountFMPentries().
 *
 */

int
svc_umountRemovedMntPts()
{

	FILE			*fp;
	int			rc = 0;
	scha_str_array_t	new_mntpts_array;	/* New mount points */
	scha_str_array_t	rem_mntpts_array;	/* Removed mount pts */
	char			*mnt_point = NULL;
	scha_resource_t		handle;
	scha_extprop_value_t	*fmp;
	scha_err_t		err;
	uint_t			fmpcount, i;

	/*
	 * Nullify the default values in the new and removed mount points
	 * arrays.
	 */

	new_mntpts_array.array_cnt = 0;
	new_mntpts_array.str_array = NULL;
	rem_mntpts_array.array_cnt = 0;
	rem_mntpts_array.str_array = NULL;


	/*
	 * Prepare the list of new mounts by querying the resource property.
	 */

retry_rs_get:
	err = scha_resource_open_zone(gSvcInfo.cl_name, gSvcInfo.rsName,
	    gSvcInfo.rgName, &handle);

	if (err != SCHA_ERR_NOERR) {

		hasp_scds_syslog_2(LOG_ERR,
		    "Failed to open the resource %s handle: %s.",
						/* CSTYLED */
		    gSvcInfo.rsName,
		    scds_error_string(err));	/*lint !e666 */

		return (1);
	}

	err = scha_resource_get_zone(gSvcInfo.cl_name, handle, SCHA_EXTENSION,
	    FILESYSTEMMOUNTPOINTS, &fmp);

	if (err == SCHA_ERR_SEQID) {
		hasp_scds_syslog_0(LOG_INFO,
		    "Retrying to retrieve the cluster information.");
		(void) scha_resource_close(handle);
		goto retry_rs_get;
	}
	if (err != SCHA_ERR_NOERR) {

		hasp_scds_syslog_2(LOG_ERR,
		    "Failed to retrieve property %s: %s.",
		    FILESYSTEMMOUNTPOINTS,	/* CSTYLED */
		    scds_error_string(err));	/*lint !e666 */

		rc = 1;
		goto out;
	}

	fmpcount = fmp->val.val_strarray->array_cnt;

	for (i = 0; i < fmpcount; i++) {

		if (svc_addStr2Array(fmp->val.val_strarray->str_array[i],
		    &new_mntpts_array)) {
			rc = 1;
			goto out;
		}
	}

	/*
	 * Sort the new mount points.
	 */
	qsort(new_mntpts_array.str_array, new_mntpts_array.array_cnt,
	    sizeof (char *), svc_mntpts_compar);

	if ((mnt_point = (char *)malloc(VFS_LINE_MAX+1)) == NULL) {
		hasp_scds_syslog_0(LOG_ERR, "Failed to allocate memory.");
		rc = 1;
		goto out;
	}

	/*
	 * Get the old mount points by reading the file which
	 * got created during validate for upgrade.
	 */

#if SOL_VERSION <= __s9
	(void) snprintf(OLDMNTS_FILE, MAXPATHLEN,
	    "/var/cluster/run/HAStoragePlus_oldmounts.%s.%s",
	    gSvcInfo.rsName, gSvcInfo.rgName);
#else
	(void) snprintf(OLDMNTS_FILE, MAXPATHLEN,
	    "/var/cluster/run/HAStoragePlus_oldmounts.%s.%s.%s",
	    gSvcInfo.cl_name, gSvcInfo.rsName, gSvcInfo.rgName);
#endif

	fp = fopen(OLDMNTS_FILE, "r");

	if (fp == NULL) {

		rc = errno;
		hasp_scds_syslog_2(LOG_ERR,
		    "Failed to open %s: %s.",
		    OLDMNTS_FILE,	/* CSTYLED */
		    strerror(rc));	/*lint !e666 */

		return (1);
	}

	while (fgets(mnt_point, VFS_LINE_MAX, fp)) {

		char	*mntpt_ptr;

		/* Strip the trailing '\n' character(s) */
		for (i = strlen(mnt_point)-1; i > 0; i--) {
			if ((mnt_point[i]) == '\n')
				mnt_point[i] = '\0';
			else
				break;
		}

		/*
		 * If this mount point is removed one add to removed
		 * mount points array.
		 */
		mntpt_ptr = bsearch(&mnt_point,
		    new_mntpts_array.str_array, new_mntpts_array.array_cnt,
		    sizeof (char *), svc_mntpts_compar);
		if (mntpt_ptr == NULL) {
			/* mount point removed */
			if (svc_addStr2Array(mnt_point, &rem_mntpts_array)) {
				rc = 1;
				(void) fclose(fp);
				goto out;
			}
		}
	}
	(void) fclose(fp);

	/*
	 * Umount the removed file systems.
	 */
	if (svc_umountFMPEntries(&rem_mntpts_array)) {
		/*
		 * Error messages related to unmounting the file
		 * systems are already logged.
		 */
		rc = 1;
		goto out;
	}

out:
	if (mnt_point != NULL) {
		free(mnt_point);
	}
	(void) scha_resource_close(&handle);
	svc_releaseStr2Array(&new_mntpts_array);
	svc_releaseStr2Array(&rem_mntpts_array);
	return (rc);
}

/*
 * Function: svc_executeCmdusingPopen
 *
 * Arguments:
 *
 * cmd		The command string which has to be executed
 *
 * cmd_output	The location where the command output to be placed
 *
 * Return:
 *
 * 0	Command execution completed successfully.
 *
 * >0	Command execution failed.
 *
 * Comments:
 * The command output is stored in cmd_output. It is the responsibility of
 * caller of this routine to free the memory allocated to cmd_output.
 * NOTE:
 * popen/pclose is used to implement this routine, which is expensive because
 * of shell process creation. So it is recommended to use
 * svc_executeCmdusingFork() to execute a command which does not involve pipe
 * or output redirection characters.
 */

static int
svc_executeCmdusingPopen(char *cmd, char **cmd_output)
{
	FILE		*stream = NULL;
	hasp_strbuf_t	stream_output;
	char		buf[BUFSIZ];
	char		*errstr = NULL;
	int		status = 0;

	stream_output.str = NULL;

	/*
	 * Assign empty string to the command output to avoid dereferencing
	 * NULL pointer by caller during the error cases that have no output
	 * for the given command.
	 */

	if (svc_addStr2Strbuf(&stream_output, "")) {
		exit(1);
	}

	if ((stream = popen(cmd, "r")) == NULL) {

		(void) svc_sprintf(&errstr,
		    "Failed to initiate a pipe for command %s : %s ",
		    cmd, strerror(errno));
		(void) svc_addStr2Strbuf(&stream_output, errstr);
		status = 1;
		goto out;
	}

	while (fgets(buf, BUFSIZ, stream)) {
		if (svc_addStr2Strbuf(&stream_output, buf)) {
			status = 1;
			goto out;
		}
	}

	if ((status = pclose(stream)) == -1) {

		(void) svc_sprintf(&errstr,
		    "Failed to close the pipe created for command %s : %s",
		    cmd, strerror(errno));
		(void) svc_addStr2Strbuf(&stream_output, errstr);
		status = 1;
		goto out;
	}

	/*
	 * Check the status of command completion.
	 */

	if (WIFEXITED(status)) {

		/*
		 * Obtain the exit status as the command completed normally.
		 */

		status = WEXITSTATUS((uint_t)status);

	} else if (WIFSIGNALED(status)) {

		/*
		 * Process(es) is terminated by signal.
		 */

		(void) svc_sprintf(&errstr, "The process(es) created by "
		    "command : %s is terminated by the signal : %d ",
		    cmd, WTERMSIG(status));
		(void) svc_addStr2Strbuf(&stream_output, errstr);

		status = 1;

	} else {

		/*
		 * Process(es) is stopped by signal.
		 */

		(void) svc_sprintf(&errstr, "The process(es) created by "
		    "command : %s is stopped by the signal : %d ",
		    cmd, WSTOPSIG((uint_t)status));
		(void) svc_addStr2Strbuf(&stream_output, errstr);

		status = 1;
	}

out:
	if (errstr)
		free(errstr);
	*cmd_output = stream_output.str;
	return (status);
}

#define	RDPIPE	0
#define	WRPIPE	1

/*
 * Function: svc_executeCmdusingFork
 *
 * Arguments:
 *
 * cmd		The command string which has to be executed
 *
 * cmd_output	The location where the command output to be placed
 *
 * run_in_zone	Determines to run inside zone or not.
 *
 * Return:
 *
 * 0	Command execution completed successfully
 *
 * >0	Command execution failed.
 *
 * Comments:
 * The command output is stored in cmd_output. It is the responsibility of
 * caller of this routine to free the memory allocated to cmd_output.
 * NOTE:
 * See also the comments of svc_executeCmdusingPopen().
 * This routine is implemented using fork1(), execv() and pipe().
 * The following are the steps ..
 * 1)Convert the given command with options to an array of strings (arguments)
 *   which is used to pass as argument list for execv.
 * 2)The parent forks a child.
 * 3)The child redirects stdout ans stderr output to the pipe which will read
 *   by parent.
 * 4)The child will execute the command using execv.
 * 5)Parent reads the command output from pipe and waits for the child.
 *
 * This function provides provision to run the command inside zone.
 */
/*lint -e715 */
int
svc_zone_executeCmdusingFork(char *cmd, char **cmd_output,
    boolean_t run_in_zone)
{

	scha_str_array_t cmdargv;		/* arg list for command */
	char		*tmpcmd;
	char		*argtok;
	hasp_strbuf_t	stream_output;
	int		copipe[2];		/* pipe for command output */
	int		nbytes;
	pid_t		child;
	char		*errstr = NULL;
	char		buf[BUFSIZ];
	int		rc, status = 0;
	char		*delimiter = " \t";
	char		*lasts;
#ifdef	ZONE_SUPPORT
	zoneid_t	zoneid;
	int		tmpl_fd = 0;

	if ((zoneid = getzoneidbyname(gSvcInfo.zonename)) == -1) {
		hasp_scds_syslog_2(LOG_ERR, "INTERNAL ERROR: %s: %s.",
		    "Failed to retrieve zoneid of zone",
		    gSvcInfo.zonename);
	}

	if (run_in_zone &&
	    (tmpl_fd = svc_initContractTemplate()) == -1) {
		hasp_scds_syslog_1(LOG_ERR, "INTERNAL ERROR: %s.",
		    "Failed to initialize the contract subsystem");
	}
#endif

	stream_output.str = NULL;

	/*
	 * Assign empty string to the command output to avoid dereferencing
	 * NULL pointer by caller during the error cases that have no output
	 * for the given command.
	 */

	if (svc_addStr2Strbuf(&stream_output, "")) {
		exit(1);
	}

	/*
	 * Initialize the command argument list.
	 */

	cmdargv.array_cnt = 0;
	cmdargv.str_array = NULL;


	/*
	 * Make a temporary copy of original command string, because the
	 * strtok() modifies the given string during tokenization.
	 */

	if ((tmpcmd = strdup(cmd)) == NULL) {
		hasp_scds_syslog_0(LOG_ERR, "Failed to allocate memory.");
		return (1);
	}

	/*
	 * Convert command string to array of strings with SPACE/TAB as
	 * delimiter.
	 */

	if ((argtok = strtok_r(tmpcmd, delimiter, &lasts)) != NULL) {
		if (svc_addStr2Array(argtok, &cmdargv)) {
			return (1);
		}
		while ((argtok = strtok_r(NULL, delimiter, &lasts)) != NULL) {
			if (svc_addStr2Array(argtok, &cmdargv)) {
				return (1);
			}
		}
	}

	/*
	 * Create a pipe (command output pipe) to read child output.
	 */

	if (pipe(copipe) == -1) {
		(void) svc_sprintf(&errstr,
		    "Failed to create pipe for the command %s : %s",
		    cmd, strerror(errno));
		(void) svc_addStr2Strbuf(&stream_output, errstr);
		*cmd_output = stream_output.str;
		return (1);
	}

	switch (child = fork1()) {

		case 0 :
#ifdef	ZONE_SUPPORT
			if (run_in_zone) {
				(void) ct_tmpl_clear(tmpl_fd);
			}
#endif

			/*
			 * Child process.
			 * Redirect the command stdout and stderr to the pipe
			 * and do execv.
			 */

			(void) dup2(copipe[WRPIPE], fileno(stdout));
			(void) dup2(copipe[WRPIPE], fileno(stderr));
			(void) close(copipe[WRPIPE]);
			(void) close(copipe[RDPIPE]);

#ifdef	ZONE_SUPPORT
			if (run_in_zone && zone_enter(zoneid) != 0) {
				hasp_scds_syslog_2(LOG_ERR,
				    "INTERNAL ERROR: %s: %s.",
				    "Failed to execute a command in zone",
				    gSvcInfo.zonename);
			}
#endif
			(void) execv(cmdargv.str_array[0], cmdargv.str_array);

			/*
			 * Print the error message to stderr so that parent
			 * can get actual error message.
			 */
			(void) fprintf(stderr,
			    "Execv failed for command %s : %s",
			    cmd, strerror(errno));

			_exit(127);		/* exec error */

			break;	/* Does not reach here but to make lint happy */

		default :
			/*
			 * Parent process.
			 */
#ifdef	ZONE_SUPPORT
			if (run_in_zone) {
				(void) ct_tmpl_clear(tmpl_fd);
				(void) close(tmpl_fd);
			}
#endif
			/*
			 * Collect the command output and wait for the child.
			 */

			(void) close(copipe[WRPIPE]);

			while ((nbytes = read(copipe[RDPIPE], buf,
			    BUFSIZ - 1)) > 0) {
				buf[nbytes] = '\0';
				if (svc_addStr2Strbuf(&stream_output, buf)) {
					status = 1;
					break;
				}
			}

			(void) close(copipe[RDPIPE]);

			while ((rc = waitpid(child, &status, 0)) < 0) {

				/*
				 * If waitpid fails with EINTR, ignore it
				 * and restart the waitpid call.
				 */
				if (errno != EINTR) {
					break;
				}
			}

			if (rc == -1) {
				(void) svc_sprintf(&errstr,
				    "Failed to obtain return status of the "
				    "command %s: %s.", cmd, strerror(errno));
				(void) svc_addStr2Strbuf(&stream_output,
				    errstr);
				*cmd_output = stream_output.str;
				return (1);
			}

			/*
			 * Check the status of command completion.
			 */

			if (WIFEXITED(status)) {

				status = WEXITSTATUS((uint_t)status);

			} else if (WIFSIGNALED(status)) {

				/*
				 * Process(es) is terminated by signal.
				 */

				(void) svc_sprintf(&errstr,
				    "The process(es) created by command : %s "
				    "is terminated by the signal : %d ",
				    cmd, WTERMSIG(status));
				(void) svc_addStr2Strbuf(&stream_output,
				    errstr);

				status = 1;

			} else {

				/*
				 * Process(es) is stopped by signal.
				 */

				(void) svc_sprintf(&errstr,
				    "The process(es) created by command : %s "
				    "is stopped by the signal : %d ",
				    cmd, WSTOPSIG((uint_t)status));
				(void) svc_addStr2Strbuf(&stream_output,
				    errstr);

				status = 1;
			}

			*cmd_output = stream_output.str;

			return (status);

		case -1 :
#ifdef	ZONE_SUPPORT
			if (run_in_zone) {
				(void) ct_tmpl_clear(tmpl_fd); /*lint !e644 */
				(void) close(tmpl_fd);
			}
#endif
			(void) svc_sprintf(&errstr,
			    "Failed to fork for command %s : %s",
			    cmd, strerror(errno));
			(void) svc_addStr2Strbuf(&stream_output, errstr);
			*cmd_output = stream_output.str;
			return (1);
	}

	return (status);	/* Does not come here but to make lint clean */
}/*lint +e715 */

#ifdef ZONE_SUPPORT
/*
 * This function initialize the contract subsystem.
 * NOTE: The contract related code in this file is borrowed from fed_util.c.
 */
static int
svc_initContractTemplate(void)
{
	int fd;

	/*
	 * doesn't do anything with the contract.
	 * Deliver no events, don't inherit, and allow it to be orphaned.
	 */
	if (((fd = open64(CTFS_ROOT "/process/template", O_RDWR)) == -1) ||
	    (ct_tmpl_set_critical(fd, 0)) ||
	    (ct_tmpl_set_informative(fd, 0)) ||
	    (ct_pr_tmpl_set_fatal(fd, CT_PR_EV_HWERR)) ||
	    (ct_pr_tmpl_set_param(fd, CT_PR_PGRPONLY | CT_PR_REGENT)) ||
	    (ct_tmpl_activate(fd))) {

		(void) close(fd);
		return (-1);
	} else {
		return (fd);
	}
}

/*
 * This function executes the given command inside a zone.
 * This is wrapper on svc_zone_executeCmdusingFork() which supports to execute
 * normally and inside zone.
 */
int
svc_executeCmdInZone(char *cmd, char **cmd_output)
{
	return (svc_zone_executeCmdusingFork(cmd, cmd_output, B_TRUE));
}
#endif

/*
 * This function executes the given command normally.
 * This is wrapper on svc_zone_executeCmdusingFork() which supports to execute
 * normally and inside zone.
 */
int
svc_executeCmdusingFork(char *cmd, char **cmd_output)
{
	return (svc_zone_executeCmdusingFork(cmd, cmd_output, B_FALSE));
}

/*
 * Function: svc_doMount
 *
 * Arguments:
 *
 * arg	Which contains the mount entry which has to be mounted
 *
 * Return:
 *
 * 0		if mount is done successfully
 *
 * 1		if an error occurred.
 *
 * Comments:
 * This function called by threadpool.
 * It calls individual function to mount based on environment the resource
 * configured.
 */
void
svc_doMount(void *arg)
{
	fsentry_t	*mnt_entry = (fsentry_t *)arg;

	/*
	 * Initialize as there are no mount failure.
	 */
	mnt_entry->status &= ~FS_MOUNTFAILED;

	switch (gSvcInfo.svcExecNode) {

		case SVC_BASE_CLUSTER_GZ_NODE:
			/*
			 * The mount is done only in global zone.
			 */
			if (svc_gz_doMount(mnt_entry)) {
				mnt_entry->status |= FS_MOUNTFAILED;
			}

			break;

		case SVC_BASE_CLUSTER_LZ_NODE_RUNS_IN_GZ:
#if	SOL_VERSION >= __s10
			/*
			 * The mount is done in local zone followed by
			 * mount success in global zone.
			 */
			if (svc_gz_doMount(mnt_entry)) {
				mnt_entry->status |= FS_MOUNTFAILED;
			} else {
				if (svc_lz_doMount(mnt_entry)) {
					mnt_entry->status |= FS_MOUNTFAILED;
				}
			}

			break;
#endif
		case SVC_ZC_NODE_RUNS_IN_GZ:
#if	SOL_VERSION >= __s10
			/*
			 * The mounts needs to be for zone cluster.
			 */
			if (svc_zc_doMount(mnt_entry)) {
				mnt_entry->status |= FS_MOUNTFAILED;
			}

			break;
#endif
		case SVC_ZC_NODE:
		case SVC_BASE_CLUSTER_LZ_NODE:
		case SVC_UNKNOWN_NODE:
		default:
			ASSERT(0);
			break;
	}
}

/*
 * Function: svc_gz_doMount
 *
 * Arguments:
 *
 * mnt_entry	Which contains the mount entry which has to be mounted
 *
 * Return:
 *
 * 0		if mount is done successfully
 *
 * 1		if an error occurred.
 *
 * Comments:
 * This function tries to mount the file system at the given mount point.
 * If the mount fails it tries to do an overlay mount.
 */

static int
svc_gz_doMount(const fsentry_t *mnt_entry)
{

	DIR		*dirp;
	char		*cmd = NULL;
	char		*cmd_output = NULL;
	int		cmd_status;
	int		totalfiles = 0;
	int		rc = 0;
	char		*mnt_point;

	mnt_point = mnt_entry->mountp;

	/*
	 * Return if already mounted in global zone
	 */
	if (mnt_entry->status & FS_GLOBALDONE) {
		return (0);
	}
	/*
	 * Check if the mount directory is not empty i.e. any entries other
	 * than "." and ".." do exist. Log an informational message for the
	 * non empty mount directory.
	 */

	if ((dirp = opendir(mnt_point)) == NULL) {

		rc = errno;

		if (rc == ENOENT) {

			/*
			 * Mount point does exist. Create it.
			 */
			if (svc_createMountPoint(mnt_point)) {
				/* Error message logged. */
				return (1);
			}
		} else {

			hasp_scds_syslog_2(LOG_ERR,
			    "Failed to open %s: %s.",
			    mnt_point,		/* CSTYLED */
			    strerror(rc));	/*lint !e666 */
			return (1);
		}
	} else {

		while (readdir(dirp) != NULL) {
			totalfiles++;
			if (totalfiles > 2)
				break;
		}

		(void) closedir(dirp);

		if (totalfiles > 2) {
			/*
			 * SCMSGS
			 * @explanation
			 * HAStoragePlus detected that the directory that
			 * will become the specified mount point is not empty,
			 * hence once mounted, the underlying files will be
			 * inaccessible.
			 * @user_action
			 * This is an informational message, no user action
			 * is needed.
			 */
			hasp_scds_syslog_1(LOG_INFO, "About to mount %s. "
			    "Underlying files and directories will be "
			    "inaccessible.", mnt_point);
		}
	}

	/*
	 * SCMSGS
	 * @explanation
	 * HAStoragePlus will mount the underlying device
	 * corresponding to the specified mount point specified in
	 * /etc/vfstab.
	 * @user_action
	 * This is an informational message, no user action is needed.
	 */
	hasp_scds_syslog_1(LOG_INFO, "About to mount %s.", mnt_point);

	/*
	 * rc might contain the errno status from opendir(), reset rc
	 */
	rc = 0;

	/*
	 * Prepare the command string for mount.
	 * Eg: mount mnt_point
	 */

	if (svc_sprintf(&cmd, "%s %s", MOUNT, mnt_point)) {
		/*
		 * SCMSGS
		 * @explanation
		 * HAStoragePlus failed to construct command path using
		 * dynamic memory allocation.
		 * @user_action
		 * Usually, this happens when the system has exhausted its
		 * resources. Check if the swap file is big enough to run
		 * Sun Cluster software.
		 */
		hasp_scds_syslog_1(LOG_ERR,
		    "Failed to construct command %s.", MOUNT);
		rc = 1;
		goto out;
	}

	/*
	 * Try mount initially without overlay option.
	 */

	cmd_status = svc_executeCmdusingFork(cmd, &cmd_output);

	if (cmd_status == 0) {

		/*
		 * Mount succeeded.
		 */

		scds_syslog_debug(LOG_INFO, "Mount of %s successful.",
		    mnt_point);

	} else {

		/*
		 * Mount command failed, try overlay mount.
		 */

		scds_syslog_debug(LOG_INFO, "Mount of %s failed: (%d) %s."
		    "(Trying an overlay mount)", mnt_point,
		    cmd_status, cmd_output);

		if (cmd) free(cmd);
		if (cmd_output) free(cmd_output);

		if (svc_sprintf(&cmd, "%s %s", MOUNTO, mnt_point)) {
			hasp_scds_syslog_1(LOG_ERR,
			    "Failed to construct command %s.", MOUNTO);
			rc = 1;
			goto out;
		}

		cmd_status = svc_executeCmdusingFork(cmd, &cmd_output);

		if (cmd_status == 0) {

			/*
			 * Overlay mount succeeded.
			 */

			scds_syslog_debug(LOG_INFO, "Mount of %s successful.",
			    mnt_point);

		} else {

			/*
			 * Overlay mount too failed.
			 */

			/*
			 * In scalable RG, mounting a file system globally is
			 * tried by all nodes selected by RGM to invoke
			 * prenet_start. So the failure of mount on a node
			 * shouldn't be treated as failure as it can be success
			 * on some other node. After mount invocation by a node
			 * (irrespective of result) the confirmation of mount
			 * is done by checking MNTTAB.
			 */

			if ((gSvcInfo.rgMode == RGMODE_SCALABLE) &&
			    (mnt_entry->svcp->isGlobalFS)) {

				/* Log a warning message */
				/*
				 * SCMSGS
				 * @explanation
				 * In a scalable resource group, each node
				 * that is configured in the resource group
				 * attempts to mount the file system. Only one
				 * node can succeed in mounting the file
				 * system. The remaining nodes which fail to
				 * mount the file system issue this message.
				 * @user_action
				 * This is an informational message. However,
				 * if no node succeeds in mounting the file
				 * system, check the failure message of each
				 * node. If you are unable to resolve the
				 * problem contact your authorized Sun service
				 * provider with copies of /var/adm/messages
				 * files on all nodes.
				 */
				hasp_scds_syslog_1(LOG_WARNING,
				    "Attempt to mount %s failed on this node.",
				    mnt_point);
			} else {
				/*
				 * SCMSGS
				 * @explanation
				 * HAStoragePlus was not able to mount the
				 * specified file system.
				 * @user_action
				 * Check the system configuration. Also check
				 * if the FilesystemCheckCommand is not empty
				 * (it is not advisable to have it empty since
				 * file system inconsistency may occur).
				 */
				hasp_scds_syslog_3(LOG_ERR,
				    "Mount of %s failed: (%d) %s.",
				    mnt_point, cmd_status, cmd_output);
				rc = 1;
				goto out;
			}
		}
	}

	if (cmd_output)
		free(cmd_output);
out:
	if (cmd)
		free(cmd);

	return (rc);
}

#if	SOL_VERSION >= __s10
/*
 * Function: svc_lz_doMount
 *
 * Arguments:
 *
 * mnt_entry	Which contains the mount entry which has to be mounted
 *
 * Return:
 *
 * 0		if mount is done successfully
 *
 * 1		if an error occurred.
 *
 * Comments:
 * This function tries to loopback mount the file system at the given
 * local zone mount point. If the mount fails it tries to do an overlay mount.
 */

static int
svc_lz_doMount(const fsentry_t *mnt_entry)
{

	DIR		*dirp;
	char		*cmd = NULL;
	char		*cmd_output = NULL;
	int		cmd_status;
	int		totalfiles = 0;
	int		rc = 0;
	char		*zmnt_point;
	char		real_path[PATH_MAX];


	zmnt_point = mnt_entry->lzpath;

	/*
	 * Obtain the real path of the local mount point and
	 * ensure that the path is still prefixed with the
	 * local zone's root path.
	 */
	if (realpath(zmnt_point, real_path) == NULL) {

		/*
		 * If the error is due to missing mount
		 * point directory, try to create it.
		 */
		rc = errno;
		if (rc == ENOENT) {
			/*
			 * Mount point does exist. Create it.
			 */
			if (svc_createMountPoint(zmnt_point)) {
				/* Error message logged. */
				return (1);
			}
		} else {

			/*
			* SCMSGS
			* @explanation
			* HAStoragePlus failed to obtain the
			* absolute pathname for the specified
			* mount point of local zone.
			* @user_action
			* Check the realpath(3C) man page for
			* errors and try to resolve the problem.
			* Otherwise contact your authorized
			* Sun service provider.
			*/
			hasp_scds_syslog_3(LOG_ERR,
			    "Failed to obtain the absolute "
			    "path of local mount point %s "
			    "(%s) in zone %s",
			    mnt_entry->lzmountp,	/* CSTYLED */
			    strerror(rc),		/*lint !e666 */
			    gSvcInfo.zonename);
			return (1);
		}
	}

	if (strncmp(real_path, gSvcInfo.zoneroot, strlen(gSvcInfo.zoneroot))) {
		/*
		 * SCMSGS
		 * @explanation
		 * HAStoragePlus detected that the local zone
		 * mount point falls outside of zone root.
		 * HAStoragePlus avoid such mount points for
		 * security reasons.
		 * @user_action
		 * Change the specified local zone mount point
		 * so that it falls inside the zone root.
		 */
		hasp_scds_syslog_2(LOG_ERR,
		    "Mountpoint %s in zone %s falls outside its zone root.",
		    mnt_entry->lzmountp, gSvcInfo.zonename);
		return (1);
	}

	/*
	 * Check if the mount directory is not empty i.e. any entries other
	 * than "." and ".." do exist. Log an informational message for the
	 * non empty mount directory.
	 */

	if ((dirp = opendir(zmnt_point)) == NULL) {
		rc = errno;
		hasp_scds_syslog_2(LOG_ERR,
		    "Failed to open %s: %s.",
		    zmnt_point,		/* CSTYLED */
		    strerror(rc));	/*lint !e666 */
		rc = 1;
		goto out;
	}

	while (readdir(dirp) != NULL) {
		totalfiles++;
		if (totalfiles > 2)
			break;
	}

	(void) closedir(dirp);

	if (totalfiles <= 2) {
		/*
		 * SCMSGS
		 * @explanation
		 * HAStoragePlus will loopback mount of file system existing
		 * on specified path name onto a specified mount point in
		 * local zone.
		 * @user_action
		 * This is an informational message, no user action is needed.
		 */
		hasp_scds_syslog_3(LOG_INFO, "About to lofs mount of %s on %s "
		    "in local zone '%s'.",
		    mnt_entry->mountp, zmnt_point, gSvcInfo.zonename);
	} else {
		/*
		 * SCMSGS
		 * @explanation
		 * HAStoragePlus detected that the directory of local zone on
		 * which loopback mount is about to happen is not empty, hence
		 * once mounted,  the underlying files will be inaccessible.
		 * @user_action
		 * This is an informational message, no user action is needed.
		 */
		hasp_scds_syslog_3(LOG_INFO, "About to lofs mount of %s on %s "
		    "in local zone '%s'. "
		    "Underlying files and directories will be inaccessible.",
		    mnt_entry->mountp, zmnt_point, gSvcInfo.zonename);
	}

	/*
	 * rc might contain the errno status from realpath(), reset rc
	 */
	rc = 0;

	/*
	 * Prepare the command string for mount.
	 * Eg: mount -F lofs <gzmnt_point> <lzmnt_point>
	 */

	if (svc_sprintf(&cmd, "%s -F lofs %s %s", MOUNT, mnt_entry->mountp,
	    zmnt_point)) {
		rc = 1;
		goto out;
	}

	/*
	 * Try mount initially without overlay option.
	 */

	cmd_status = svc_executeCmdusingFork(cmd, &cmd_output);

	if (cmd_status == 0) {

		/*
		 * Mount succeeded.
		 */

		scds_syslog_debug(LOG_INFO, "Mount of %s successful.",
		    zmnt_point);

	} else {

		/*
		 * Mount command failed, try overlay mount.
		 */

		scds_syslog_debug(LOG_INFO, "Mount of %s failed: (%d) %s."
		    "(Trying an overlay mount)", zmnt_point,
		    cmd_status, cmd_output);

		if (cmd) free(cmd);
		if (cmd_output) free(cmd_output);

		if (svc_sprintf(&cmd, "%s -F lofs -O %s %s", MOUNT,
		    mnt_entry->mountp, zmnt_point)) {
			rc = 1;
			goto out;
		}

		cmd_status = svc_executeCmdusingFork(cmd, &cmd_output);

		if (cmd_status == 0) {

			/*
			 * Overlay mount succeeded.
			 */

			scds_syslog_debug(LOG_INFO, "Mount of %s successful.",
			    zmnt_point);

		} else {

			/*
			 * Overlay mount too failed.
			 */

			hasp_scds_syslog_3(LOG_ERR,
			    "Mount of %s failed: (%d) %s.",
			    zmnt_point, cmd_status, cmd_output);
			rc = 1;
			goto out;
		}
	}

	if (cmd_output)
		free(cmd_output);

out:
	return (rc);
}

/*
 * Function: svc_zc_doMount
 *
 * Arguments:
 *
 * mnt_entry	Which contains the mount entry which has to be mounted
 *
 * Return:
 *
 * 0		if mount is done successfully
 *
 * 1		if an error occurred.
 *
 * Comments:
 * This function mounts the filesystem for zone cluster.
 * 1. In case of mapped filesystem, it executes the mount command with proper
 *    parameters.
 * 2. In case of non-mapped filesystem, it executes the mount command inside
 *    zone using zone_enter(). The reason to execute inside is to get privileges
 *    of zone cluster admin for the filesystem.
 *    If the mount command is executed in global zone for non-mapped filesystem,
 *    the zone cluster admin cannot unmount that filesystem.
 */
static int
svc_zc_doMount(const fsentry_t *mnt_entry)
{
	DIR		*dirp;
	char		*cmd = NULL;
	char		*cmd_output = NULL;
	int		cmd_status;
	int		totalfiles = 0;
	int		rc = 0;
	char		*zcmnt_point;
	char		real_path[PATH_MAX];
	struct vfstab	*vp;


	zcmnt_point = mnt_entry->zcpath;
	vp = &mnt_entry->svcp->vfsEntry;


	/*
	 * Obtain the real path of the local mount point and
	 * ensure that the path is still prefixed with the
	 * local zone's root path.
	 */
	if (realpath(zcmnt_point, real_path) == NULL) {

		/*
		 * If the error is due to missing mount
		 * point directory, try to create it.
		 */
		rc = errno;
		if (rc == ENOENT) {
			/*
			 * Mount point does exist. Create it.
			 */
			if (svc_createMountPoint(zcmnt_point)) {
				/* Error message logged. */
				return (1);
			}
		} else {

			/*
			* SCMSGS
			* @explanation
			* HAStoragePlus failed to obtain the
			* absolute pathname for the specified
			* mount point of local zone.
			* @user_action
			* Check the realpath(3C) man page for
			* errors and try to resolve the problem.
			* Otherwise contact your authorized
			* Sun service provider.
			*/
			hasp_scds_syslog_3(LOG_ERR,
			    "Failed to obtain the absolute "
			    "path of mount point %s "
			    "(%s) in zone cluster %s.",
			    mnt_entry->zcmountp,	/* CSTYLED */
			    strerror(rc),		/*lint !e666 */
			    gSvcInfo.cl_name);
			return (1);
		}
	}

	if (strncmp(real_path, gSvcInfo.zoneroot, strlen(gSvcInfo.zoneroot))) {
		/*
		 * SCMSGS
		 * @explanation
		 * HAStoragePlus detected that the mount point for
		 * specified zone cluster falls outside of zone root.
		 * HAStoragePlus avoid such mount points for
		 * security reasons.
		 * @user_action
		 * Change the specified zone cluster mount point
		 * so that it falls inside the zone root.
		 */
		hasp_scds_syslog_2(LOG_ERR,
		    "Mountpoint %s for zone cluster %s falls outside its "
		    "zone root.",
		    zcmnt_point, gSvcInfo.cl_name);
		return (1);
	}

	/*
	 * Check if the mount directory is not empty i.e. any entries other
	 * than "." and ".." do exist. Log an informational message for the
	 * non empty mount directory.
	 */

	if ((dirp = opendir(zcmnt_point)) == NULL) {
		rc = errno;
		hasp_scds_syslog_2(LOG_ERR,
		    "Failed to open %s: %s.",
		    zcmnt_point,		/* CSTYLED */
		    strerror(rc));	/*lint !e666 */
		return (1);
	}

	while (readdir(dirp) != NULL) {
		totalfiles++;
		if (totalfiles > 2)
			break;
	}

	(void) closedir(dirp);

	if (totalfiles <= 2) {
		/*
		 * SCMSGS
		 * @explanation
		 * HAStoragePlus will mount the file system for zone cluster
		 * on specified path name onto a specified mount point.
		 * @user_action
		 * This is an informational message, no user action is needed.
		 */
		hasp_scds_syslog_2(LOG_INFO, "About to mount on %s "
		    "for zone cluster '%s'.",
		    mnt_entry->zcmountp, gSvcInfo.cl_name);
	} else {
		/*
		 * SCMSGS
		 * @explanation
		 * HAStoragePlus detected that the directory of zone cluster on
		 * which mount is about to happen is not empty, hence once
		 * mounted, the underlying files will be inaccessible.
		 * @user_action
		 * This is an informational message, no user action is needed.
		 */
		hasp_scds_syslog_2(LOG_INFO, "About to mount on %s "
		    "for zone cluster '%s'."
		    "Underlying files and directories will be inaccessible.",
		    mnt_entry->zcmountp, gSvcInfo.cl_name);
	}

	/*
	 * rc might contain the errno status from realpath(), reset rc
	 */
	rc = 0;

	if (!mnt_entry->svcp->isMappedFS) {

		/*
		 * Non-mapped filesystem.
		 * Construct the mount command to run inside zone.
		 * mount -F <type> -o <opts> <special> <zcmountp>
		 */
		if (vp->vfs_mntopts == NULL) {
			rc = svc_sprintf(&cmd,
			    "%s -F %s %s %s",
			    MOUNT, vp->vfs_fstype,
			    vp->vfs_special, vp->vfs_mountp);
		} else {
			rc = svc_sprintf(&cmd,
			    "%s -F %s -o %s %s %s",
			    MOUNT, vp->vfs_fstype, vp->vfs_mntopts,
			    vp->vfs_special, vp->vfs_mountp);
		}
		if (rc) {
			hasp_scds_syslog_1(LOG_ERR,
			    "Failed to construct command %s.", MOUNT);
			return (1);
		}

		scds_syslog_debug(DBG_LEVEL_HIGH,
		    "The mount command used to mount %s is : %s.",
		    vp->vfs_mountp, cmd);
		/*
		 * Execute the command inside zone.
		 */
		if (svc_executeCmdInZone(cmd, &cmd_output)) {
			/*
			 * SCMSGS
			 * @explanation
			 * HAStoragePlus was not able to mount the
			 * specified file system inside zone cluster.
			 * @user_action
			 * Check the system configuration. Also check
			 * if the FilesystemCheckCommand is not empty
			 * (it is not advisable to have it empty since
			 * file system inconsistency may occur).
			 */
			hasp_scds_syslog_4(LOG_ERR,
			    "Mounting the device %s on path %s failed "
			    "in zone cluster %s : %s",
			    vp->vfs_special, vp->vfs_mountp, gSvcInfo.cl_name,
			    cmd_output);
			return (1);
		}

		if (cmd) free(cmd);
		if (cmd_output) free(cmd_output);

		scds_syslog_debug(LOG_INFO, "Mount of %s successful.",
		    zcmnt_point);

		return (0);
	}



	/*
	 * Mapped filesystem.
	 * Construct the command string to execute.
	 * Eg: mount -F <type> -o <opts> <special> <zcpath>
	 */

	if (vp->vfs_mntopts == NULL) {
		rc = svc_sprintf(&cmd, "%s -F %s %s %s",
		    MOUNT, vp->vfs_fstype, vp->vfs_special, zcmnt_point);
	} else {
		rc = svc_sprintf(&cmd, "%s -F %s -o %s %s %s",
		    MOUNT, vp->vfs_fstype, vp->vfs_mntopts,
		    vp->vfs_special, zcmnt_point);
	}
	if (rc) {
		hasp_scds_syslog_1(LOG_ERR,
		    "Failed to construct command %s.", MOUNT);
		return (1);
	}

	scds_syslog_debug(DBG_LEVEL_HIGH,
	    "The mount command used to mount %s is : %s.",
	    vp->vfs_mountp, cmd);

	/*
	 * Try mount initially without overlay option.
	 */

	cmd_status = svc_executeCmdusingFork(cmd, &cmd_output);

	if (cmd_status == 0) {

		/*
		 * Mount succeeded.
		 */

		scds_syslog_debug(LOG_INFO, "Mount of %s successful.",
		    zcmnt_point);

	} else {

		/*
		 * Mount command failed, try overlay mount.
		 */

		scds_syslog_debug(LOG_INFO, "Mount of %s failed: (%d) %s."
		    "(Trying an overlay mount)", vp->vfs_special,
		    cmd_status, cmd_output);

		if (cmd) free(cmd);
		if (cmd_output) free(cmd_output);

		if (vp->vfs_mntopts == NULL) {
			rc = svc_sprintf(&cmd, "%s -F %s %s %s",
			    MOUNTO, vp->vfs_fstype, vp->vfs_special,
			    zcmnt_point);
		} else {
			rc = svc_sprintf(&cmd, "%s -F %s -o %s %s %s",
			    MOUNTO, vp->vfs_fstype, vp->vfs_mntopts,
			    vp->vfs_special, zcmnt_point);
		}
		if (rc) {
			hasp_scds_syslog_1(LOG_ERR,
			    "Failed to construct command %s.", MOUNTO);
			return (1);
		}

		scds_syslog_debug(DBG_LEVEL_HIGH,
		    "The mount command used to mount %s is : %s.",
		    vp->vfs_mountp, cmd);

		cmd_status = svc_executeCmdusingFork(cmd, &cmd_output);

		if (cmd_status == 0) {

			/*
			 * Overlay mount succeeded.
			 */

			scds_syslog_debug(LOG_INFO, "Mount of %s successful.",
			    zcmnt_point);

		} else {

			/*
			 * Overlay mount too failed.
			 */

			/*
			 * SCMSGS
			 * @explanation
			 * HAStoragePlus was not able to mount the
			 * specified file system for zone cluster.
			 * @user_action
			 * Check the system configuration. Also check
			 * if the FilesystemCheckCommand is not empty
			 * (it is not advisable to have it empty since
			 * file system inconsistency may occur).
			 */
			hasp_scds_syslog_4(LOG_ERR,
			    "Mount of %s failed in zone cluster %s: (%d) %s.",
			    vp->vfs_special, gSvcInfo.cl_name,
			    cmd_status, cmd_output);
			return (1);
		}
	}

	if (cmd_output)
		free(cmd_output);

	return (0);
}
#endif

/*
 * This function attempts to create non-existent mount point.
 */
static int
svc_createMountPoint(const char *mnt_point)
{
	char	*cmd = NULL;
	char	*cmd_output = NULL;
	int	rc = 0;

	/*
	 * Construct the command used to create mount point.
	 */
	if (svc_sprintf(&cmd, "%s -m %s %s", MKDIRP,
	    MODE_OF_MOUNTPT, mnt_point)) {
		hasp_scds_syslog_1(LOG_ERR,
		    "Failed to construct command %s.", MKDIRP);
		rc = 1;
		goto out;
	}

	if (svc_executeCmdusingFork(cmd, &cmd_output)) {
		/*
		 * Failed to create the mount point.
		 */
		/*
		 * SCMSGS
		 * @explanation
		 * HAStoragePlus detected that the mount point
		 * was missing and failed to create the mount
		 * point.
		 * @user_action
		 * Check the error message. Rectify the
		 * problem and retry the operation. If the
		 * problem persists, contact your authorized
		 * Sun service provider.
		 */
		hasp_scds_syslog_2(LOG_ERR,
		    "Failed to create non-existent mount "
		    "point %s: %s.",
		    mnt_point, cmd_output);
		rc = 1;
		goto out;
	} else {
		/*
		 * Mount point is created successfully.
		 * Log informational message.
		 */
		/*
		 * SCMSGS
		 * @explanation
		 * HAStoragePlus detected the mount point was
		 * missing, and created the mount point with
		 * the specified information.
		 * @user_action
		 * This is an informational message, no user
		 * action is needed.
		 */
		hasp_scds_syslog_2(LOG_INFO,
		    "The non-existent mount point %s was "
		    "created successfully with permissions %s "
		    "and owned by the superuser.",
		    mnt_point, MODE_OF_MOUNTPT);
	}
out:
	if (cmd)
		free(cmd);
	if (cmd_output)
		free(cmd_output);

	return (rc);
}

/*
 * Function: svc_doFsck
 *
 * Arguments:
 *
 * arg		Which contains fsck entry which has to be fsck'ed
 *
 * Return:
 *
 * 0		if the file system is consistent and suitable for mounting.
 *		Also tries to repair if possible without manual intervention.
 *
 * 1		if an error occurred.
 *
 * Comments:
 * This function performs fsck check on file system.
 * When the FilesystemCheckCommand is set to NULL, /usr/sbin/fsck -o p is
 * used to check the UFS/VxFS file system and /usr/sbin/fsck is used for
 * other file systems.
 * When the FilesystemCheckCommand is set to a user specified command string,
 * it will be elected to check the file system.
 * For SAMFS file system check is skipped.
 *
 */
void
svc_doFsck(void *arg)
{
	fsentry_t	*fsck_entry = (fsentry_t *)arg;
	char		*fsck_cmd;
	char		*cmd = NULL;
	int		rc = 0;
	int		cmd_status;
	char		*cmd_output = NULL;
	char		*fstype, *mountp, *special;

	fstype = fsck_entry->svcp->vfsEntry.vfs_fstype;
	mountp = fsck_entry->svcp->vfsEntry.vfs_mountp;
	special = fsck_entry->svcp->vfsEntry.vfs_fsckdev;

	if (STREQ(fstype, "samfs")) {
		goto out;
	}

	if (STREQ(gSvcInfo.fsckCmd, SvcNullString)) {

		if (STREQ(fstype, "ufs") || STREQ(fstype, "vxfs")) {

			/* Recognized as UFS/VxFS file systems */
			fsck_cmd = HASTORAGEPLUS_R_FSCK;

		} else {

			/* Unrecognized file systems */
			fsck_cmd = HASTORAGEPLUS_U_FSCK;
		}

	} else {

		/* Use the user specified fsck command string */
		fsck_cmd = gSvcInfo.fsckCmd;
	}

	/*
	 * Prepare the command string for fsck.
	 * Eg: fsck_cmd device_to_mount
	 */

	if (svc_sprintf(&cmd, "%s %s", fsck_cmd, special)) {
		rc = 1;
		goto out;
	}

	/*
	 * SCMSGS
	 * @explanation
	 * HAStoragePlus will perform a file system check on the specified
	 * device.
	 * @user_action
	 * This is an informational message, no user action is needed.
	 */
	hasp_scds_syslog_3(LOG_INFO, "About to perform file system check "
	    "of %s (%s) using command %s.", special, mountp, fsck_cmd);

	cmd_status = svc_executeCmdusingFork(cmd, &cmd_output);

	if (cmd_status == 0) {

		/*
		 * fsck command succeeded.
		 */

		/*
		 * SCMSGS
		 * @explanation
		 * Self explanatory.
		 * @user_action
		 * This is an informational message, no user action is needed.
		 */
		hasp_scds_syslog_2(LOG_INFO,
		    "File system check of %s (%s) successful.",
		    mountp, special);

	} else {

		/*
		 * fsck command failed.
		 */

		/*
		 * SCMSGS
		 * @explanation
		 * Fsck reported inconsistencies while checking the specified
		 * device. The return value and output of the fsck command is
		 * also embedded in the message.
		 * @user_action
		 * Try to manually check and repair the file system which
		 * reports errors.
		 */
		hasp_scds_syslog_4(LOG_ERR,
		    "File system check of %s (%s) failed: (%d) %s.",
		    mountp, special, cmd_status, cmd_output);
		rc = 1;
	}

	if (cmd)
		free(cmd);
	if (cmd_output)
		free(cmd_output);

out:
	if (rc) {
		fsck_entry->status |= FS_FSCKFAILED;
	} else {
		fsck_entry->status &= ~FS_FSCKFAILED;
	}
}

/*
 * Function: svc_sprintf
 *
 * Arguments:
 *
 * str		The string location where the output has to placed
 *
 * format, ...	The format and argument list
 *
 * Return:
 *
 * 0		if the arguments are written to string correctly.
 *
 * 1		if an error occurs.
 *
 * Comments:
 * This function allocates required memory and writes the argument list to
 * to location provided.
 */

static int
svc_sprintf(char **str, const char *format, ...)
{
	int	size;
	char	s;	/* Used to know size of memory for format string */
	va_list	ap;
	int	rc = 0;

	/*
	 * override FlexeLint error 40;
	 * lint doesn't understand compiler builtin variable __builtin_va_alist
	 */
				/* CSTYLED */
	va_start(ap, format);	/*lint !e40 !e26 !e50 !e10 */

	/*
	 * Trick to know the size of memory required.
	 */

	if ((size = vsnprintf(&s, 1, format, ap)) < 0) {
		/*
		 * Assuming memory failure
		 * (as EINVAL, EILSEQ errors not possible)
		 */
		hasp_scds_syslog_0(LOG_ERR, "Failed to allocate memory.");
		rc = 1;
		goto error;
	}

	if ((*str = (char *)malloc(((uint_t)size + 1))) == NULL) {
		hasp_scds_syslog_0(LOG_ERR, "Failed to allocate memory.");
		rc = 1;
		goto error;
	}
	(void) vsprintf(*str, format, ap);
error:
	va_end(ap);
	return (rc);
}

/*
 * Function: svc_getFullLocalPath(char *zoneroot, char *localpath)
 *
 * Arguments:
 *
 * zoneroot	The string with the path to the zone's root filesystem.
 *
 * localpath	The path to the mount point within the local zone.
 *
 * Return:
 *
 * char *	A pointer to a newly allocated string that is a global
 *		zone relative path to the local zone mount point for
 *		loopback mounting. This function is expected to resolve
 *		any symbolic links, and ensure that they do not point
 *		outside the zone's root.
 *
 * NULL		if an error occurs.
 *
 * Comments:
 * This function allocates required memory the calling function should free
 * the allocated memory.
 */
char *
svc_getFullLocalPath(const char *zoneroot, char *localpath)
{
	char	*returnpath;
	char	combinedpath[PATH_MAX];

	if (*localpath != '/') {
		/*
		 * SCMSGS
		 * @explanation
		 * The specified mount point in local zone is not an absolute
		 * path. An absolute path must start with slash(/).
		 * @user_action
		 * Correct the mount point path value so that it is absolute
		 * path and repeat the operation.
		 */
		hasp_scds_syslog_2(LOG_ERR,
		    "Local mountpoint %s must begin with / in local zone '%s'.",
		    localpath, gSvcInfo.zonename);
		return (NULL);
	}

	if ((returnpath = (char *)malloc(PATH_MAX)) == NULL) {
		hasp_scds_syslog_0(LOG_ERR, "Failed to allocate memory.");
		return (NULL);
	}

	/*
	 * compute the global root relative path.
	 */
	(void) strlcpy(combinedpath, zoneroot, PATH_MAX);
	(void) strlcat(combinedpath, localpath, PATH_MAX);

	/*
	 * resolve ., .., and symbolic links out of the path.
	 * An error should not be returned if path is resolved because this can
	 * be the case of nested mount point.
	 */
	if (realpath(combinedpath, returnpath) == NULL) {
		/*
		 * Unable to resolve the path. Return the unresolved path
		 * itself which will be used during actual mount.
		 */
		(void) strcpy(combinedpath, returnpath);
	}
	return (returnpath);
}

#ifdef	ZFS_AVAILABLE
/*
 * Function: hasp_scds_syslog
 *
 * Comments:
 * This function implements the log functionality.
 */
void
hasp_scds_syslog(int pri, const char *format, ...)
{
	va_list		args;

				/* CSTYLED */
	va_start(args, format);	/*lint !e40 */
	/* Log remainder of message */
	hasp_scds_vsyslog(pri, format, args);
	va_end(args);
}

/*
 * Function: hasp_scds_vsyslog
 *
 * Comments:
 * This function implements the log functionality which sends to syslog and
 * also to the terminal.
 */
void
hasp_scds_vsyslog(int pri, const char *format, va_list args)
{
extern mutex_t log_mp;

	(void) mutex_lock(&log_mp);
	/* temporarily reset locale to C locale */
	(void) setlocale(LC_ALL, "C");
	vsyslog(pri, format, args);

	/* restore locale from env */
	(void) setlocale(LC_ALL, "");

	if ((pri == LOG_ERR) && (svcMethodId == HASTORAGEPLUS_VALIDATE)) {
		(void) vfprintf(stderr, gettext(format), args);
		(void) fprintf(stderr, "\n");
	}
	(void) mutex_unlock(&log_mp);
}

/*
 * Function: hasp_scds_log
 *
 * Comments:
 * This function implements the log functionality which used to libzfs handler.
 */
void
hasp_scds_log(const char *format, va_list args)
{
	hasp_scds_vsyslog(LOG_ERR, format, args);
}
#endif
