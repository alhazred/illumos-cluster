/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

/*
 *
 * hastorageplus_glb.c
 *
 * File containing global declarations. These are used by different
 * HAStoragePlus methods.
 */


#pragma ident	"@(#)hastorageplus_glb.c	1.9	08/06/04 SMI"

#include "hastorageplus.h"

HAStoragePlusSvcInfo gSvcInfo;

HAStoragePlusSvcMethod svcMethodId;

hasp_strbuf_t update_status_msg; /* Contains status message for update */

hasp_strbuf_t failed_switchovers_msg;
		/* Contains device groups failed to switchover */
mutex_t log_mp = DEFAULTMUTEX;	/*lint !e708 */
