/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_HASTORAGEPLUS_H_
#define	_HASTORAGEPLUS_H_

#pragma ident	"@(#)hastorageplus.h	1.26	09/01/21 SMI"

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Header file for the HAStoragePlus resource type implementation.
 */

/* CSTYLED */
/*lint -e793 -save ANSI limit of 511 'external identifiers' exceeded */
/* CSTYLED */
/*lint -e525 -save Negative indentation */
/* Supressing 'Use of goto is deprecated' message */
/* CSTYLED */
/*lint -e801 */

#include <sys/sol_version.h>
#include <errno.h>
#include <libdcs.h>
#include <libintl.h>
#include <locale.h>
#include <rgm/libdsdev.h>
#include <scha.h>
#include <stdio.h>
#include <strings.h>
#include <sys/vfstab.h>
#include <sys/mnttab.h>
#include <thread.h>
#include <synch.h>
#include <stdarg.h>

#if SOL_VERSION >= __s10
#include <libzfs.h>
#define	ZFS_AVAILABLE
#endif

#if SOL_VERSION >= __s10
#define	ZONE_SUPPORT
#endif

#define	HASP_RT			"SUNW.HAStoragePlus"
#define	GLOBALDEVICEPATHS	"GlobalDevicePaths"
#define	FILESYSTEMMOUNTPOINTS 	"FilesystemMountPoints"
#define	AFFINITYON 		"AffinityOn"
#define	FILESYSTEMCHECKCOMMAND	"FilesystemCheckCommand"
#define	ZPOOLS			"Zpools"
#define	ZPOOLSSEARCHDIR		"ZpoolsSearchDir"

#define	CAT	"/bin/cat"
#define	CUT	"/bin/cut"
#define	DATE	"/bin/date"
#define	DIFF	"/bin/diff"
#define	DIRNAME	"/bin/dirname"
#define	GREP	"/bin/grep"
#define	LS	"/bin/ls"
#define	MKDIR	"/bin/mkdir"
#define	MKDIRP	"/bin/mkdir -p"
#define	MV	"/bin/mv"
#define	PRINTF	"/bin/printf"
#define	RM	"/bin/rm"
#define	SED	"/bin/sed"
#define	SLEEP	"/bin/sleep"
#define	SORT	"/bin/sort"
#define	TOUCH	"/bin/touch"
#define	WC	"/usr/bin/wc"

#define	FSCK	"/usr/sbin/fsck"

#define	MOUNT		"/usr/sbin/mount "	/* Mount */
#define	MOUNTO		"/usr/sbin/mount -O "	/* Mount in overlay mode */

#define	UMOUNT		"/usr/sbin/umount "	/* Unmount */
#define	UMOUNTA		"/usr/sbin/umount -a "	/* Parallel Unmount */
#define	UMOUNTF		"/usr/sbin/umount -f "	/* Forced unmount */
#define	UMOUNTFA	"/usr/sbin/umount -f -a "
				/* Parallel forced unmount */
#define	FUSER		"/usr/sbin/fuser"

#define	QUOTACHECK	"/usr/sbin/quotacheck "
#define	QUOTAON		"/usr/sbin/quotaon "

#define	SvcNullString		""

#define	STREQ(s1, s2)		(strcmp((s1), (s2)) == 0)

/*
 * Time in seconds that the data service will sleep before checking to see if a
 * global device service is available.
 */
#define	HASTORAGEPLUS_WAIT	5


/*
 * Number of service checks for the availability of a global device service
 * before logging a syslog message.
 */
#define	HASTORAGEPLUS_RETRIES_BEFORE_WARNING	12

/*
 * A reserved keyword for disallowing file system checks on the entire
 * FilesystemMountPoint list.
 */
#define	HASTORAGEPLUS_NO_FSCK	"/bin/true"

/*
 * The file system check command for recognized file systems (UFS and VxFS).
 */
#define	HASTORAGEPLUS_R_FSCK	"/usr/sbin/fsck -o p"

/*
 * The file system check command for unrecognized file systems.
 */
#define	HASTORAGEPLUS_U_FSCK	"/usr/sbin/fsck"


/*
 * The restriction on legacy filesystems is being relaxed as it prevents
 * implementing delagated administration of a zpool inside a non-global zone.
 */
#define	KEEP_ZFS_LEGACY_RESTRICTION	0

/*
 * The permission used by HAStoragePlus to create the non-extistent mount
 * point.
 */
#define	MODE_OF_MOUNTPT	"0755"

/*
 * Debug levels for syslog debug messaging.
 */
#define	DBG_LEVEL_HIGH	9
#define	DBG_LEVEL_MED	5
#define	DBG_LEVEL_LOW	1

/*
 * NOTE:
 * The HAStoargePlus callback methods should be
 * -> Backward compatible with older RT versions.
 * -> Handle non-existing extension properties gracefully.
 * -> Handle resources with version where there is no zones support.
 */
/*
 * The resourcetype version where zpools extension property exists.
 */
#define	ZPOOLS_IN_RT_VERSION	4

/*
 * The resourcetype version which provides location to import the zpools
 * managed by HAStoragePlus.
 */
#define	ZPOOLSSEARCHDIR_IN_RT_VERSION	6

/*
 * The resourcetype version which has zones support.
 */

#define	ZONE_SUPPORT_IN_RT_VERSION	4

/*
 * The delimiter to separate local zone mount point from the corresponding
 * global zone mount point.
 */
#define	PATH_DELIM	":"

/*
 * Type defining method codes.
 */
typedef enum {

	HASTORAGEPLUS_VALIDATE,
	HASTORAGEPLUS_PRENET_START,
	HASTORAGEPLUS_UPDATE,
	HASTORAGEPLUS_VALIDATE_FOR_UPDATE,
	HASTORAGEPLUS_INIT

} HAStoragePlusSvcMethod;

/*
 * Types of Method execution nodes.
 */
typedef enum {
	SVC_UNKNOWN_NODE = 0,
	SVC_BASE_CLUSTER_GZ_NODE,
	SVC_BASE_CLUSTER_LZ_NODE_RUNS_IN_GZ,
	SVC_BASE_CLUSTER_LZ_NODE,
	SVC_ZC_NODE,
	SVC_ZC_NODE_RUNS_IN_GZ
} HASPMethodExecutionNode;

/*
 * The device service class types.
 */
#define	HASP_DISK_SVC_CLASS	"DISK"		/* /dev/global/ */
#define	HASP_SVM_SVC_CLASS	"SUNWmd"	/* /dev/md/ */

/*
 * A structure representing a device service.
 */
typedef struct HAStoragePlusSvc {

	const char	*svcPath; 	/* service path	   */
	char		*svcLocalPath;	/* service path	in the local zone */
	scha_str_array_t svcName;	/* service name(s) */

	boolean_t 	isMountPoint;	/* is this a mount point? */
	boolean_t	isMappedFS;	/* is a mapped filesystem to zc? */
	boolean_t 	isGlobalFS;	/* is a global filesystem? */
	struct vfstab	vfsEntry;	/* VFSTAB entry for mount point */
					/* It conatins the actual fields */
					/* used to mount the filesystem. */
} HAStoragePlusSvc;

/*
 * A structure representing the information of zpools of a node.
 */
typedef struct HAStoragePlusZpoolSvc {

	scha_str_array_t zpoolsList;	/* List of zpools configured */

} HAStoragePlusZpoolSvc;

/*
 * A structure representing information global to all device services. In
 * addition, it contains a pointer to the list of HAStoragePlusSvc structures.
 * Each such structure represents a global device service.
 */
typedef struct HAStoragePlusSvcInfo {
	const char 	*rsName;	/* Resource name	*/
	const char 	*rgName;	/* Resource group name 	*/
	scha_rgmode_t 	rgMode;		/* Resource group mode 	*/
	boolean_t 	affinityOn;
	scha_extprop_value_t *gdp_prop_value;	/* GDP ext property */
	scha_extprop_value_t *fmp_prop_value;	/* FMP ext property */
	scha_extprop_value_t *affinity_prop_value;	/* AffinityOn prop */
	scha_extprop_value_t *fsck_string;	/* FS check command property */
	scha_extprop_value_t *zpool_prop_value;	/* Zpools extr property */
	scha_extprop_value_t *zpools_dir_value;	/* Zpool Search Dir ext prop */
	uint_t		count;		/* Number of list entries */
	uint_t		gdpCount;	/* Number of GDP entries */
	uint_t		fmpCount;	/* Number of FMP entries */
	char 		*fsckCmd;	/* FS check command string */
	int		rs_rt_version;	/* resource RT version */
	HAStoragePlusSvc *list;		/* List of device services */
	boolean_t	failback;	/* Resource group failback policy */
	nodeid_t 	nodeId;		/* Nodeid on which rs is running */
	uint_t		*rgNodeIdArray;	/* Array of node ids ordered by pref */
	uint_t		rgNodeIdArraySize;  /* Number of nodes for the RG */
	HAStoragePlusZpoolSvc zpoolsInfo;	/* Zpools information */
	const char	*zpoolsSearchDir;	/* Location to import zpools */
	const char	*zonename;	/* name of local zone (this node) */
	char		*zoneroot;	/* Root of the local zone */
	HASPMethodExecutionNode svcExecNode;	/* where rs config/running? */
	const char	*cl_name;		/* rs configured cluster name */
	scds_handle_t	*ds_handle;		/* pointer to DSDL handler */
} HAStoragePlusSvcInfo;

/*
 * Symbolic constants which represent the status of a file system.
 */

#define	FS_FSCKPROCESSED	0x00001	/* fsck is executed on device */
#define	FS_FSCKFAILED		0x00002	/* fsck on the device failed */


#define	FS_RPFAILED		0x00100	/* realpath failed on mount point */

#define	FS_MOUNTFAILED		0x00200	/* mount failed on file system */
#define	FS_MOUNTCONFIRM		0x00400	/* mount confirmed on file system */


#define	FS_UMOUNTFAILED		0x10000	/* unmount failed on file system */
#define	FS_UMOUNTCONFIRM	0x20000	/* unmount confirmed on file system */

#define	FS_GLOBALDONE		0x40000 /* Global zone mount completed. */

/*
 * A structure used to maintain information of a file system. This is used
 * during fsck/mount/umount operation. The same structure is used
 * for simplicity though all fields are not used for every operation.
 */

typedef struct fsentry {
	char		*mountp;	/* The mount point of file system */
	char		*rpath;		/* real pathname for mount point */
	char		*lzpath;	/* mount pathname for the local zone */
	char		*lzmountp;	/* mount point for the local zone */
	char		*zcpath;	/* mount path for zone cluster node */
	char		*zcmountp;	/* mount point for zone cluster node */
	uint_t		depth;		/* depth of mount point */
	uint_t		status;		/* status of file system */
					/* (see above symbolic constants) */
	HAStoragePlusSvc *svcp;		/* Points to its filesystem svc info */
} fsentry_t;

/*
 * A structure which can be used as string buffer to append the strings.
 * NOTE: The string in the instance of the structure should be initiliazed
 * to NULL (to specify that it is empty string, otherwise it contains garbage
 * value which leads to problems)
 */

typedef struct hasp_strbuf {

	char *str;
	unsigned int str_length;	/* The current length of string */
	unsigned int str_capacity;	/* The maximum length of string */

} hasp_strbuf_t;


extern HAStoragePlusSvcInfo gSvcInfo;
extern HAStoragePlusSvcMethod svcMethodId;
extern mutex_t log_mp;
extern hasp_strbuf_t update_status_msg;	/* The status message for update */
extern hasp_strbuf_t failed_switchovers_msg;

extern int svc_fill_properties(scds_handle_t);
extern int svc_validate(scds_handle_t, int, char *const[]);
extern boolean_t is_zones_supported(void);
extern boolean_t is_zpools_property_exists(void);
extern boolean_t is_zpoolssearchdir_property_exists(void);
extern int svc_startDeviceServices(void);
extern int svc_verifyDeviceServices(void);
extern int svc_umountRemovedMntPts(void);
extern int svc_saveOldMntPts(void);
extern int svc_umountFMPEntries(const scha_str_array_t *);
extern int svc_umountFilesystems(const scha_str_array_t  *);
extern int svc_mountFilesystems(void);
extern int svc_addStr2Array(const char *, scha_str_array_t *);
extern int svc_addStr2Strbuf(hasp_strbuf_t *, const char *);

#ifdef	ZFS_AVAILABLE
extern int svc_getciZpoolsList(scha_str_array_t *);
extern int svc_getpiZpoolsList(scha_str_array_t *, boolean_t);
#if	KEEP_ZFS_LEGACY_RESTRICTION
extern int svc_validateMntPtProperty(const char *);
#endif
extern int svc_importZpools(void);
extern int svc_importZpoolList(const scha_str_array_t *,
    boolean_t, boolean_t, const char *, scha_str_array_t *, const char *);
extern int svc_exportZpools(const scha_str_array_t  *);
extern int svc_exportZpool(const char *, boolean_t);
extern int svc_saveOldZpools(void);
extern int svc_exportRemovedZpools(scds_handle_t);
extern int svc_exportNewlyAddedZpools(scds_handle_t);
extern void libzfs_cleanup_handler(void);
extern int svc_initializeZFS(void);
extern void hasp_scds_log(const char *, va_list);
extern void hasp_scds_syslog(int, const char *, ...);
extern void hasp_scds_vsyslog(int, const char *, va_list);
#endif

/*
 * This set of macros prints:
 *	- a non i18n'ed message to syslog
 *  and
 *	- a fully i18n'ed message to stderr is method == validate and priority
 *	    is LOG_ERR.
 *
 * ONLY hasp_scds_syslog_X() should be used (__hasp_scds_syslog() is internal
 * to the macro).
 *
 * X is the number of arguments required.
 *
 * These defines are used because flexelint does not recognise __VA_ARGS__.
 * Also, having the developer to explicitly write the number of arguments
 * required makes the code checkable by the compiler.
 *
 * Note that this macro make functions passed in argument called more than
 * once, hence DON'T USE it with *side effect or non idempotent* functions.
 *
 * This macro requires the lint !e666 directive when using functions as
 * arguments since functions MUST be idempotent.
 *
 *	/\	Because errno is resetted between the call to
 *     /  \	hasp_scds_syslog() and the call to scds_syslog() and between
 *    /STOP\	the call to scds_syslog() and the call to fprintf(), HAS+
 *   /  !!  \	developers SHOULD NEVER use errno when using
 *  +--------+	hasp_scds_syslog() but a temporary variable for storing errno.
 *
 *		Ex: - hasp_scds_syslog_1("%s", strerror(errno)); IS FORBIDDEN.
 *		    - int ret = errno;
 *		      hasp_scds_syslog_1("%s", strerror(ret)); IS ALLOWED.
 */
#define	__hasp_scds_syslog(pri, fmt, a1, a2, a3, a4)			\
	do {	/* ugly, but needed for lint! */			\
	    (void) mutex_lock(&log_mp);					\
	    /* temporarily reset locale to C locale */			\
	    (void) setlocale(LC_ALL, "C");				\
	    scds_syslog(pri, fmt, a1, a2, a3, a4);			\
	    /* restore locale from env */				\
	    (void) setlocale(LC_ALL, "");				\
									\
	    if ((pri == LOG_ERR) &&					\
		    (svcMethodId == HASTORAGEPLUS_VALIDATE)) {		\
		    (void) fprintf(stderr, gettext(fmt), a1, a2, a3,	\
			a4);						\
		    (void) fprintf(stderr, "\n");			\
	    }								\
	    (void) mutex_unlock(&log_mp);				\
	} while (0 != 0)
#define	hasp_scds_syslog_0(p, f)			\
	__hasp_scds_syslog(p, f, 0, 0, 0, 0)
#define	hasp_scds_syslog_1(p, f, a1)			\
	__hasp_scds_syslog(p, f, a1, 0, 0, 0)
#define	hasp_scds_syslog_2(p, f, a1, a2)		\
	__hasp_scds_syslog(p, f, a1, a2, 0, 0)
#define	hasp_scds_syslog_3(p, f, a1, a2, a3)		\
	__hasp_scds_syslog(p, f, a1, a2, a3, 0)
#define	hasp_scds_syslog_4(p, f, a1, a2, a3, a4)	\
	__hasp_scds_syslog(p, f, a1, a2, a3, a4)

#define	ADD_STR2STRBUF(strbuf, str)			\
	do {						\
		if (svc_addStr2Strbuf(&strbuf, str)) {	\
			return (1);			\
		}					\
	} while (0 != 0);

#define	ADD_TO_STATUS_MSG(str) ADD_STR2STRBUF(update_status_msg, str)

#define	ADD_TO_STATUS_MSG_IF_NOT_EMPTY(str)		\
	do {						\
		if (update_status_msg.str_length != 0)	\
			ADD_TO_STATUS_MSG(str);		\
	} while (0 != 0)

#define	ADD_TO_FAILED_SWITCHOVERS(str)			\
	ADD_STR2STRBUF(failed_switchovers_msg, str)

#ifdef __cplusplus
}
#endif

#endif	/* !_HASTORAGEPLUS_H_ */
