/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * hastorageplus_validate.c
 *
 * Validate method implementation for the HAStoragePlus resource type.
 *
 */

#pragma ident	"@(#)hastorageplus_validate.c	1.7	08/06/04 SMI"

#include <stdlib.h>
#include <libintl.h>
#include <locale.h>

#include "hastorageplus.h"


/*
 *
 * Function: main
 *
 * Arguments:
 *
 * argc		Number of arguments
 *
 * argv 	Command line argument list
 *
 *
 * Return:
 *
 * 0 	if validations completed successfully.
 *
 * 1	if an error occurred during validation.
 *	This error causes RGM to abort the
 *	HAStoragePlus resource creation.
 *
 *
 * Comments:
 *
 * While resource creation RGM invokes validate callback method on all
 * potential primary nodes i.e. all nodes which can act as a primary
 * for the resource group containing the HAStoragePlus resource.
 *
 * The validate method is also invoked when resource properties are
 * updated.
 *
 */

int
main(int argc, char *argv[])
{
	scds_handle_t	ds_handle;
	int		option;
	int		rc = 0;
	extern int	optind;

	svcMethodId = HASTORAGEPLUS_VALIDATE;

	/*
	 * I18N support.
	 */
	(void) textdomain(TEXT_DOMAIN);
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);
	(void) setlocale(LC_ALL, "");

	/*
	 * Initialize the DSDL subsystem which process the command line
	 * arguments passed by RGM and also initializes logging.
	 */

	if (scds_initialize(&ds_handle, argc, argv) != SCHA_ERR_NOERR) {

		hasp_scds_syslog_0(LOG_ERR, "Failed to initialize the DSDL.");

		return (1);
	}

	/*
	 * Fill the resource properties.
	 */
	if (svc_fill_properties(ds_handle)) {
		/*
		 * Error message logged.
		 */
		return (1);
	}

	/*
	 * Perform all validations.
	 */
	if (svc_validate(ds_handle, argc, argv)) {

		/*
		 * Error message for failure is already logged.
		 */
		rc = 1;
		goto end;
	}

	/*
	 * Determine if validate is invoked as part of update.
	 */

	optind = 1;

	while ((option = getopt(argc, argv, "R:T:G:Z:r:x:g:cu")) != EOF) {

		if (option == 'u') {
			svcMethodId = HASTORAGEPLUS_VALIDATE_FOR_UPDATE;
		}
	}

#ifdef	ZFS_AVAILABLE
	/*
	 * Export the zpools which are configured in HAStoragePlus resource.
	 * This is to ensure that no zpool is imported on any other node while
	 * the zpools are being imported on the node where the resource will
	 * brought online.
	 */

	if ((is_zpools_property_exists()) &&
	    (svcMethodId == HASTORAGEPLUS_VALIDATE)) {
		(void) svc_exportZpools(&gSvcInfo.zpoolsInfo.zpoolsList);
	}
#endif

	/*
	 * In case VALIDATE is called for UPDATE, save the old mount points
	 * because properties are overridden once the validation completes
	 * successfully.
	 * The mount points are saved to find out removed ones for update,
	 * and will be unmounted during update.
	 */

	if ((svcMethodId == HASTORAGEPLUS_VALIDATE_FOR_UPDATE)) {

		if (svc_saveOldMntPts()) {

			/*
			 * Failed to save mount points, log error and return.
			 */
			/*
			 * SCMSGS
			 * @explanation
			 * The online update of the HAStoragePlus resource is
			 * not successful because of failure in saving the old
			 * mount points of file systems that were present
			 * before updating the resource.
			 * @user_action
			 * Check the syslog messages and try to resolve the
			 * problem. Try again to update the resource. If the
			 * problem persists, contact your authorized Sun
			 * service provider.
			 */
			hasp_scds_syslog_0(LOG_ERR,
			    "Failed to save old mount points.");

			rc = 1;
			goto end;
		}

#ifdef	ZFS_AVAILABLE
		if (is_zpools_property_exists()) {
			if (svc_saveOldZpools()) {

				/*
				 * Failed to save zpools, log error and return.
				 */
				/*
				 * SCMSGS
				 * @explanation
				 * The online update of the HAStoragePlus
				 * resource is not successful because of
				 * failure in saving the old zpools that were
				 * present before updating the resource.
				 * @user_action
				 * Check the syslog messages and try to
				 * resolve the problem. Try again to update
				 * the resource. If the problem persists,
				 * contact your authorized Sun service
				 * provider.
				 */
				hasp_scds_syslog_0(LOG_ERR,
				    "Failed to save old zpools.");

				rc = 1;
				goto end;
			}

			/*
			 * Export the pools which are being added for update.
			 * This to ensure that new added zpools are not imported
			 * by any other node at the time these zpools will be
			 * imported by the node when the resource is primaried.
			 * Ignoring the return value to avoid validation
			 * failure.
			 */
			(void) svc_exportNewlyAddedZpools(ds_handle);
		}
#endif
	}

	/*
	 * SCMSGS
	 * @explanation
	 * All device services specified directly or indirectly via the
	 * GlobalDevicePath and FilesystemMountPoint extension properties
	 * respectively are found to be correct. Other Sun Cluster components
	 * like DCS, DSDL, RGM are found to be in order. Specified file system
	 * mount point entries are found to be correct.
	 * @user_action
	 * This is an informational message, no user action is needed.
	 */
	hasp_scds_syslog_0(LOG_INFO,
	    "All specified device services validated successfully.");

end:
	scds_close(&ds_handle);
	return (rc);
}
