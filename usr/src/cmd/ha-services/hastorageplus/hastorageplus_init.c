/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * hastorageplus_init.c
 *
 * Init method implementation for the HAStoragePlus resource type.
 *
 */


#pragma ident	"@(#)hastorageplus_init.c	1.8	08/06/04 SMI"

				/* CSTYLED */
#include "hastorageplus.h"	/*lint	-e766 */
#ifdef	ZFS_AVAILABLE
#include <stdlib.h>
#endif

/*
 *
 * Function: main
 *
 * Arguments:
 *
 * argc		Number of arguments
 *
 * argv		Argument list from command line
 *
 * Return:
 *
 * 0	if successfull.
 *
 * 1	if an error occurred. RGM aborts the process of
 *	bringing the resource group online.
 *
 * Comments:
 *
 * The INIT method is invoked  when  the  resource  group containing
 * containing the resource is put under the management of the RGM.
 * It is  called  on  nodes  determined  by  the Init_nodes resource
 * type  property. For ZFS support, the zpools defined by the extension
 * property are exported here. The export is done without any check and
 * errors are ignored.  The zpools will be subsequently imported for
 * use on the node which receives the START callback.
 */
#ifdef	ZFS_AVAILABLE
int
main(int argc, char *argv[])
{
	scds_handle_t		ds_handle;
	int			rc = 0;

	svcMethodId = HASTORAGEPLUS_INIT;

	/*
	 * Initialize the DSDL subsystem which process the command line
	 * arguments passed by RGM and also initializes logging.
	 */

	if (scds_initialize(&ds_handle, argc, argv) != SCHA_ERR_NOERR) {

		/*
		 * SCMSGS
		 * @explanation
		 * HA Storage Plus was not able to connect to the DSDL.
		 * @user_action
		 * Contact your authorized Sun service provider to determine
		 * whether a workaround or patch is available.
		 */
		hasp_scds_syslog_0(LOG_ERR, "Failed to initialize the DSDL.");

		return (1);
	}

	/*
	 * Fill the resource properties.
	 */
	if (svc_fill_properties(ds_handle)) {
		/*
		 * Error message logged.
		 */
		return (1);
	}

	if (is_zpools_property_exists()) {

		scha_extprop_value_t *zpools = gSvcInfo.zpool_prop_value;

		/*
		 * Check for zpools existence and initialize ZFS if required.
		 */
		if (zpools->val.val_strarray->array_cnt > 0) {
			if (svc_initializeZFS()) {
				rc = 1;
				goto end;
			}
		}

		/*
		 * Export each zpool to ensure that that the associated file
		 * systems are not mounted any more.
		 */
		(void) svc_exportZpools(zpools->val.val_strarray);
	}

end:
	scds_close(&ds_handle);
	return (rc);
}
#else
int
main()
{
	return (0);
}
#endif
