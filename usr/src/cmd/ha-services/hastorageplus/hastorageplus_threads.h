/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

/*
 * This header file contains the API for HAStoragePlus to make use of
 * SC threadpool which is implemented in C++. The API created is a wrapper on
 * existing API of SC threadpool.
 * NOTE: The current API wrapper allows to use a global threadpool
 * ("HAStoragePlus Threadpool"). This will need modification if there
 * is requirement for additional threadpool(s).
 */

#ifndef	_HASTORGEPLUS_THREADS_H_
#define	_HASTORGEPLUS_THREADS_H_

#pragma ident	"@(#)hastorageplus_threads.h	1.3	08/05/20 SMI"

#ifdef __cplusplus
extern "C" {
#endif

typedef void (*task_func)(void *);

/*
 * The following are the API which can used to interact with threadpool.
 */

/*
 * Routine to add the task the threadpool.
 */

extern int svc_addTasktoThreadPool(task_func task_handler, void *task_arg);

/*
 * Routine which provides to ensure, all the added tasks are done.
 */

extern void svc_wait4tasksinThreadPool(void);

#ifdef __cplusplus
}
#endif

#endif	/* !_HASTORGEPLUS_THREADS_H_ */
