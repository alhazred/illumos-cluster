/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * hastorageplus_prenet_start.c
 *
 * Prenet start method implementation
 * for the HAStoragePlus resource type.
 *
 */

#pragma ident	"@(#)hastorageplus_prenet_start.c	1.6	08/06/04 SMI"

#include <stdlib.h>

#include "hastorageplus.h"


/*
 *
 * Function: main
 *
 * Arguments:
 *
 * argc		Number of arguments
 *
 * argv		Argument list from command line
 *
 * Return:
 *
 * 0	if completed successfully. HAStoragePlus resource
 *	is now online. RGM will attempt to bring the next
 *	resource online. A resource group will be primaried
 *	i.e. brought online on one or more nodes if the
 *	start/prenet start methods  for all contained
 *	resources return 0.
 *
 * 0	If device path and file system mount point lists
 *	are both empty. This might seem strange, but
 *	it is possible to have null lists. HAStoragePlus
 *	is effectively a noop here.
 *
 *
 * 1	if an error occurred. RGM aborts the process of
 *	bringing the resource group online.
 *
 * Comments:
 *
 * A HAStorage resource can belong to a resource group which is either
 * failover ( primaried on only one node ) or scalable ( primaried on
 * more than one node ). Accordingly, the prenet start method is invoked on
 * one or more nodes.
 *
 * The RGM invokes a resource's prenet start method prior to configuring
 * the network addresses. HAStoragePlus prenet start
 * method ensures that it will be an early candidate in the list of
 * start methods to be invoked by the RGM.
 */
int
main(int argc, char *argv[])
{
	scds_handle_t	ds_handle;
	int		rc = 0;

	svcMethodId = HASTORAGEPLUS_PRENET_START;

	/*
	 * Initialize the DSDL subsystem (which processes the command line
	 * arguments passed by RGM and also initializes logging).
	 */

	if (scds_initialize(&ds_handle, argc, argv) != SCHA_ERR_NOERR) {

		hasp_scds_syslog_0(LOG_ERR, "Failed to initialize the DSDL.");

		return (1);
	}

	/*
	 * Fill the resource properties.
	 */
	if (svc_fill_properties(ds_handle)) {
		/*
		 * Error message logged.
		 */
		return (1);
	}

	/*
	 * Revalidate all devices services and zpools prior to starting.
	 */

	if (svc_validate(ds_handle, argc, argv)) {

		rc = 1;
		goto end;
	}

#ifdef	ZFS_AVAILABLE
	if (is_zpools_property_exists()) {
		/*
		 * Import the zpools which are specified in the Zpools property.
		 */
		if (svc_importZpools()) {
			rc = 1;
			goto end;
		}
	}
#endif

	if (gSvcInfo.count == 0) {

		/*
		 * GlobalDevicePaths and FilesystemMountPoints are
		 * are both null. A message to this effect
		 * has already been logged.
		 *
		 */
		goto end;
	}

	/*
	 * SCMSGS
	 * @explanation
	 * All device services specified directly or indirectly via the
	 * GlobalDevicePath and FilesystemMountPoint extension properties
	 * respectively are found to be correct. Other Sun Cluster components
	 * like DCS, DSDL, RGM are found to be in order. Specified file system
	 * mount point entries are found to be correct.
	 * @user_action
	 * This is an informational message, no user action is needed.
	 */
	hasp_scds_syslog_0(LOG_INFO, "Validations of all specified global "
	    "device services complete.");

	/*
	 * Start all specified device services.
	 *
	 * The return status of svc_startDeviceServices() is ignored because
	 * it is possible that the DCS returns error for starting the device
	 * services initially and starts later. The device services will be
	 * checked later by svc_verifyDeviceServices() whether they are
	 * actually started or not and necessary action is taken.
	 */

	(void) svc_startDeviceServices();

	/*
	 * Verify that all device services are indeed available.
	 * The function call will block till all devices are available or
	 * if an error occurred.
	 */

	if (svc_verifyDeviceServices()) {

		/*
		 * Error message already logged.
		 */
		rc = 1;
		goto end;
	}

	/*
	 * SCMSGS
	 * @explanation
	 * All global device services specified directly or indirectly via the
	 * GlobalDevicePath and FilesystemMountPoint extension properties
	 * respectively are found to be available i.e up and running.
	 * @user_action
	 * This is an informational message, no user action is needed.
	 */
	hasp_scds_syslog_0(LOG_INFO,
	    "All specified global device services are available.");

	/*
	 * Mount the file system(s) specified in the FilesystemMountPoints
	 * list.
	 */

	if (svc_mountFilesystems()) {
		/*
		 * Error messages related to mounting the file systems are
		 * already logged.
		 */
		rc = 1;
		goto end;
	}

end:
	scds_close(&ds_handle);
	return (rc);
}
