/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * hastorageplus_validate.c
 *
 * Validate method implementation for the HAStoragePlus resource type.
 *
 */

#pragma ident	"@(#)hastorageplus_update.c	1.9	08/06/04 SMI"

#include <stdlib.h>
#include <libintl.h>
#include <locale.h>

#include "hastorageplus.h"

/*
 *
 * Function:	main
 *
 * Arguments:	argc	Number of arguments.
 *		argv	Command line arguments passed by RGM.
 *
 * Return code:	0	If everything succeeded.
 *		>0	If something failed.
 *
 * Sequence:	1.	Starts device services.
 *		2.	Verify device services availability.
 *
 * Comments:	A HAStoragePlus resource can belong to a resource group which
 *		is either failover (primaried on only one node) or scalable
 *		(primaried on more than one node). Accordingly, the update
 *		method is invoked on one or more nodes.
 *		If an errors occurs, the resource will be flagged as FAULTED by
 *		the update callback method.
 */
int
main(int argc, char *argv[])
{
	/*
	 * Update callback is treated in a different way. Instead of returning
	 * when an (non-fatal) error encountered, all error information is
	 * gathered related to update and at the end the resource status set to
	 * FAULTED state with the status message.
	 *
	 * NOTE:
	 * Failure of the Update method causes the syslog(3) function to
	 * generate an error message but does not affect RGM management
	 * of the resource.
	 */

	scds_handle_t	ds_handle;
	int		rc = 0;

	svcMethodId = HASTORAGEPLUS_UPDATE;

	/*
	 * Initialize the DSDL subsystem (which processes the command line
	 * arguments passed by RGM and also initializes logging).
	 */

	if (scds_initialize(&ds_handle, argc, argv) != SCHA_ERR_NOERR) {

		hasp_scds_syslog_0(LOG_ERR, "Failed to initialize the DSDL.");

		return (1);
	}

	/*
	 * Fill the resource properties.
	 */
	if (svc_fill_properties(ds_handle)) {
		/*
		 * Error message logged.
		 */
		return (1);
	}

	/*
	 * Validate all devices services prior to update.
	 */

	if (svc_validate(ds_handle, argc, argv)) {

		/*
		 * Error message for failure is already logged.
		 */

		rc++;
		ADD_TO_STATUS_MSG("Validation failed on devices");
		goto end;
	}

#ifdef	ZFS_AVAILABLE
	if (is_zpools_property_exists()) {
		/*
		 * Export the zpools which are removed for update.
		 */

		scds_syslog_debug(DBG_LEVEL_HIGH, "Exporting the removed "
		    "zpools for update");

		if (svc_exportRemovedZpools(ds_handle)) {
			scds_syslog_debug(DBG_LEVEL_HIGH,
			    "Failed in exporting the zpools");
			rc++;
		}
	}
#endif

	/*
	 * Unmount the file systems which are removed for update.
	 */

	scds_syslog_debug(DBG_LEVEL_HIGH, "Unmounting the removed file "
	    "systems for update");

	if (svc_umountRemovedMntPts()) {
		scds_syslog_debug(DBG_LEVEL_HIGH,
		    "Failed in unmouting the file systems");
		rc++;
	}

#ifdef	ZFS_AVAILABLE
	if (is_zpools_property_exists()) {
		if (svc_importZpools()) {
			/*
			 * Error messages related to export failures are already
			 * logged.
			 */
			/*
			 * SCMSGS
			 * @explanation
			 * The online update operation of the HAStoragePlus
			 * resource is not successful.
			 * @user_action
			 * Examine the syslog messages, to see if the problem
			 * can be identified. This problem is usually caused
			 * by a failure in device switchovers or failure in
			 * mounting file sytems. Check the global service
			 * configuration and system configuration. If unable
			 * to resolve this problem save a copy of
			 * /var/adm/messages and contact your authorized Sun
			 * service provider for assistance.
			 */
			hasp_scds_syslog_1(LOG_ERR,
			    "Update on the resource failed: %s.",
			    "Failed to import all zpools");
			rc++;
		}
	}
#endif

	if (gSvcInfo.count == 0) {

		/*
		 * GlobalDevicePaths and FilesystemMountPoints are
		 * are both null. A message to this effect
		 * has already been logged.
		 * Here we return as there is no devices to be started.
		 */

		goto end;
	}

	/*
	 * Start all specified services.
	 *
	 * The return status of svc_startDeviceServices() is ignored because
	 * it is possible that the DCS returns error for starting the device
	 * services initially and starts later. The device services will be
	 * checked later by svc_verifyDeviceServices() whether they are
	 * actually started or not and necessary action is taken.
	 */

	(void) svc_startDeviceServices();

	/*
	 * Verify that all device services are indeed available.
	 * The function call will block till all devices are
	 * available or if an error occurred.
	 */

	if (svc_verifyDeviceServices()) {

		/*
		 * Error message already logged and the failed device services
		 * are stored in "failedswitchovers" string which is used to
		 * construct status message for HAStoragePlus resource.
		 */

		rc++;

		/*
		 * If any error occurs during update the resource state is set
		 * to FAULTED.
		 * status_message is used to displayed the status of the
		 * resource as a cause to be in FAULTED state.
		 */


		if (failed_switchovers_msg.str_length == 0) {

			/*
			 * An internal error occurred: no device could
			 * switchover.
			 * This may happen if some malloc() fail for example.
			 * Add to status message this message: (On one line
			 * only)
			 * [...] -- An internal error occurred while switching
			 * over the devices.
			 */

			ADD_TO_STATUS_MSG_IF_NOT_EMPTY(" -- ");
			ADD_TO_STATUS_MSG(
			    "Internal error occured during switchover devices");

			hasp_scds_syslog_1(LOG_ERR,
			    "Update on the resource failed: %s.",
			    "Internal error occured during switchover devices");
		} else {

			/*
			 * Add to status message device which couldn't
			 * switchover.
			 * Message will be of the form: (On one line only)
			 *
			 * [...] -- Switchover failed for device groups: d1 d2
			 * where d1 and d2 stands for the device groups.
			 */

			ADD_TO_STATUS_MSG_IF_NOT_EMPTY(" -- ");

			ADD_TO_STATUS_MSG(
			    "Switchover failed for device groups ");
			/*
			 * Enter the device services to status msg which are
			 * failed.
			 */

			ADD_TO_STATUS_MSG(failed_switchovers_msg.str);

			hasp_scds_syslog_1(LOG_ERR,
			    "Update on the resource failed: %s.",
			    "Failed to switchover all the devices");
		}
	} else {

		hasp_scds_syslog_0(LOG_INFO,
		    "All specified global device services "
		    "are available.");
	}

	/*
	 * Mount the file system(s) specified in the FilesystemMountPoints
	 * extension property.
	 */

	if (svc_mountFilesystems()) {
		/*
		 * Error messages related to mounting the file systems are
		 * already logged.
		 * Set the HAS+ in FAULTED state and return.
		 */
		hasp_scds_syslog_1(LOG_ERR,
		    "Update on the resource failed: %s.",
		    "Failed to mount all filesystems");

		rc++;
		goto end;
	}

end:
	if (rc) {

		/*
		 * Set the HAS+ resource in FAULTED state.
		 */

		(void) scha_resource_setstatus_zone(gSvcInfo.rsName,
			gSvcInfo.rgName, gSvcInfo.zonename,
			SCHA_RSSTATUS_FAULTED, update_status_msg.str);
	} else {

		/*
		 * SCMSGS
		 * @explanation
		 * Self explanatory.
		 * @user_action
		 * This is an informational message, no user action is needed.
		 */
		hasp_scds_syslog_0(LOG_INFO,
		    "Update on the resource is completed successfully.");

		/*
		 * Set the HAS+ resource in OK state.
		 */

		(void) scha_resource_setstatus_zone(gSvcInfo.rsName,
			gSvcInfo.rgName, gSvcInfo.zonename,
			SCHA_RSSTATUS_OK, "");
	}

	scds_close(&ds_handle);
	return (rc);
}
