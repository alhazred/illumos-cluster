//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)smf_proxy_resource_monitor_check.cc	1.408/05/20 SMI"

// This file contains the monitor check method of SMF proxy resource

#include "smf_proxy_common.h"
// main routine.
int
main(int argc, char *argv[]) {

	scds_handle_t handle;
	int method_time_out = -1;
	int return_code = 0;
	// Init the SCDS handle.
	if (scds_initialize(&handle, argc, argv) != 0) {
		scds_syslog(LOG_ERR, "%s initialization failure", argv[0]);
		return_code = ERROR_GETTING_SCDS_HANDLE;
		goto finished;
	}

	if ((return_code = get_smf_proxy_method_timeout(
		METHOD_MONITOR_CHECK, handle,
		&method_time_out)) != 0) {
		return_code = ERROR_GETTING_METH_TIMEOUT;
		goto finished;
	}

	// Check the SC delegated restarter state.
	if ((return_code = check_and_enable_sc_restarter(
	    CHECK_SC_RESTARTER, method_time_out)) != 0) {
		goto finished;
	}
finished:
	if (handle)
		scds_close(&handle);
	return (return_code);
}
