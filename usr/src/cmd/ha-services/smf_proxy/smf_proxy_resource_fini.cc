//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)smf_proxy_resource_fini.cc	1.6	09/02/27 SMI"

// This file contains the fini method implementation of SMF proxy resource

#include "smf_proxy_common.h"


#include "smf_proxy_common.h"
// Main routine.
int
main(int argc, char *argv[])
{
	// Handle used for SCDS calls.
	scds_handle_t handle = NULL;
	// Door file fd.
	int reim_server_fd = -1;
	int return_code = -1;
	// Container struct for storing the input arguments
	// that are to be sent to the server via the door_call
	smf_proxy_rs_input_args *fini_inp_args = NULL;
	smf_proxy_rs_common_args *rs_fini_args = NULL;
	// Used to get the FMRI value.
	smf_proxy_rs_boot_start_args *rs_temp_args = NULL;
	int method_time_out = -1;
	int rc = 0;
	// Init the SCDS handle.
	if (scds_initialize(&handle, argc, argv) != 0) {
		scds_syslog(LOG_ERR, "%s initialization failure", argv[0]);
		return_code = ERROR_GETTING_SCDS_HANDLE;
		goto finished;
	}

	if ((return_code = get_smf_proxy_method_timeout(METHOD_FINI,  handle,
	    &method_time_out)) != 0) {
		return_code = ERROR_GETTING_METH_TIMEOUT;
		goto finished;
	}

	// Check the SC delegated restarter state.
	if ((return_code = check_and_enable_sc_restarter(
	    CHECK_SC_RESTARTER, method_time_out)) != 0) {
		goto finished;
	}

	// Check the RT type is of SMF_PROXY.
	if (!is_smf_proxy_resource_type(handle)) {
		scds_syslog(LOG_ERR, "Resource type is not of PROXY SMF type");
		return_code = RESOURCE_TYPE_NOT_PROXY_SMF_TYPE;
		goto finished;
	}


	// open and attach to door server and call the door call
	if (ReIM_clnt_connect(&reim_server_fd)) {

		// Allocate memory.
		rs_fini_args = (smf_proxy_rs_common_args *)
		    calloc(1, sizeof (smf_proxy_rs_common_args));

		if (rs_fini_args == NULL) {
			proxysvc_errmsgnomem();
			return_code = MEMORY_ALLOCATION_FAILED;
			goto finished;
		}

		return_code = populate_proxy_rs_rg_name(rs_fini_args,
		    handle);
		if (return_code != 0) {
			goto finished;
		}

		// Allocate memory for storing the arguments to be passed
		fini_inp_args = (smf_proxy_rs_input_args *)
		    calloc(1, sizeof (smf_proxy_rs_input_args));
		if (fini_inp_args == NULL) {
			proxysvc_errmsgnomem();
			return_code = MEMORY_ALLOCATION_FAILED;
			goto finished;
		}

		// Prepare the container structure
		fini_inp_args->method = METHOD_FINI;
		fini_inp_args->data = rs_fini_args;
		rc = process_door_call(fini_inp_args, reim_server_fd);
		if (rc >= 0) {
			// Populate the Proxied Service instances,retry count
			// and retry interval.

			// Allocate memory for common args.
			rs_temp_args = (smf_proxy_rs_boot_start_args *)
			calloc(1, sizeof (smf_proxy_rs_boot_start_args));


			if (rs_temp_args == NULL) {
				proxysvc_errmsgnomem();
				return_code = MEMORY_ALLOCATION_FAILED;
				goto finished;
			}

			if ((return_code = populate_proxy_boot_args(
			    rs_temp_args, handle)) != 0) {
				goto finished;
			}

			// Parse the FMRI file.
			if (process_fmri(rs_temp_args->boot_args->fmri,
			    B_FALSE,  METHOD_FINI, handle) != 0) {
				return_code = FAILURE_IN_PROCESSING_FMRI;
				goto finished;
			}

			return_code = 0;
		} else {
			return_code = DOOR_CALL_RETURN_ERROR;
		}

	} else {
		scds_syslog(LOG_ERR, "Could not connect to door server");
		return (REIM_DOOR_CONNECT_FAILED);
	}

finished :
	// Cleanup.
	if (handle)
		scds_close(&handle);
	if (rs_fini_args) {
		if (rs_fini_args->rs_name)
			free(rs_fini_args->rs_name);
		if (rs_fini_args->rg_name)
			free(rs_fini_args->rg_name);
		free(rs_fini_args);
	}
	if (fini_inp_args)
		free(fini_inp_args);

	if (rs_temp_args) {
		if (rs_temp_args->boot_args) {
			if (rs_temp_args->boot_args->fmri)
				free(rs_temp_args->boot_args->fmri);
			free(rs_temp_args->boot_args);
		}
		free(rs_temp_args);
	}
	return (return_code);
}
