//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef _SMF_PROXY_COMMON_H
#define	_SMF_PROXY_COMMON_H

#pragma ident	"@(#)smf_proxy_common.h	1.6	09/02/27 SMI"

// Include headers.
#include <rgm/reim_common.h>
#include <rgm/reim_xdr.h>
#include <rgm/libdsdev.h>
#include <scha.h>

// Function definetions.
// Utility function to check if RT is of SMF PROXY type.
int  is_smf_proxy_resource_type(scds_handle_t handle);
// ReIM Door server connect function.
boolean_t ReIM_clnt_connect(int *door_desc);
// Processing of the Proxied_service_instances property
int process_fmri(char *input_file, boolean_t res_create_flag,
    method_type_t method, scds_handle_t handle);
// Encoding of Input arguments.
bool_t xdr_smf_proxy_resource_input_args(register XDR *xdrs,
    smf_proxy_rs_input_args_t *res_args);
// ReIM Door server call.
int make_ReIMdoor_call(int serverfd, door_arg_t *arg);
// debug print function.
void debug_print(char *fmt, ...);
// Function to generate syslog in ase of memory failure.
void proxysvc_errmsgnomem(void);
int check_and_enable_sc_restarter(check_restarter_flag_t flag, int timeout);
int populate_proxy_common_args(smf_proxy_rs_boot_start_args *rs_args,
    scds_handle_t handle);
int populate_proxy_boot_args(smf_proxy_rs_boot_start_args  *rs_args,
    scds_handle_t handle);
int populate_proxy_rs_rg_name(smf_proxy_rs_common_args *rs_args,
    scds_handle_t handle);
int process_door_call(smf_proxy_rs_input_args_t  *rs_args, int reim_server_fd);
boolean_t get_resource_status_info(int reim_server_fd, char *r_name,
    char *rg_name, check_proxy_svc_info_t flag, char *fmri = NULL);
int get_smf_proxy_method_timeout(method_type_t method, scds_handle_t handle,
    int *time_out);

#endif /* _SMF_PROXY_COMMON_H */
