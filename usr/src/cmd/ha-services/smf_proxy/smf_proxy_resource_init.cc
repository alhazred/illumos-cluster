//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
#pragma ident	"@(#)smf_proxy_resource_init.cc	1.5	09/02/27 SMI"
//
// This file contains the init method implementation of SMF proxy resource.
//

#include "smf_proxy_common.h"

// Main routine.
int
main(int argc, char *argv[])
{
	// Handle used for SCDS calls.
	scds_handle_t handle = NULL;
	int return_code = -1;
	int reim_server_fd = -1;
	// Container struct for storing the input arguments
	// that are to be sent to the server via the door_call
	smf_proxy_rs_input_args_t *boot_inp_args = {0};
	smf_proxy_rs_boot_start_args *rs_boot_args = {0};
	int method_time_out = -1;
	// Init the SCDS handle.
	if (scds_initialize(&handle, argc, argv) != 0) {
		scds_syslog(LOG_ERR, "%s initialization failure", argv[0]);
		return_code = ERROR_GETTING_SCDS_HANDLE;
		goto finished;
	}

	if ((return_code = get_smf_proxy_method_timeout(METHOD_BOOT,  handle,
	    &method_time_out)) != 0) {
		return_code = ERROR_GETTING_METH_TIMEOUT;
		goto finished;
	}

	// Check the SC delegated restarter state and enable if it is disabled.
	if ((return_code = check_and_enable_sc_restarter(
	    CHECK_ENABLE_SC_RESTARTER, method_time_out)) != 0) {
		goto finished;
	}
	// Check the RT type is of SMF_PROXY.
	if (!is_smf_proxy_resource_type(handle)) {
		scds_syslog(LOG_ERR, "Resource type is not of PROXY SMF type");
		return_code = RESOURCE_TYPE_NOT_PROXY_SMF_TYPE;
		goto finished;
	}

	// open and attach to door serverl
	if (ReIM_clnt_connect(&reim_server_fd)) {
		// Allocate memory for storing the arguments to be passed
		rs_boot_args = (smf_proxy_rs_boot_start_args *)
		    calloc(1, sizeof (smf_proxy_rs_boot_start_args));
		if (rs_boot_args == NULL) {
			proxysvc_errmsgnomem();
			return_code = MEMORY_ALLOCATION_FAILED;
			goto finished;
		}

		// Populate the common args R Name and RG Name
		if ((return_code = populate_proxy_common_args(
		    rs_boot_args, handle)) != 0) {
			goto finished;
		}
		// Populate the Proxied Service instances,retry count
		// and retry interval.
		if ((return_code = populate_proxy_boot_args(
		    rs_boot_args, handle)) != 0) {
			goto finished;
		}


		// Allocate memory for storing the arguments to be passed
		boot_inp_args = (smf_proxy_rs_input_args_t *)
		    calloc(1, sizeof (smf_proxy_rs_input_args_t));

		if (boot_inp_args  == NULL) {
			proxysvc_errmsgnomem();
			return_code = MEMORY_ALLOCATION_FAILED;
			goto finished;
		}

		// Prepare the container structure
		boot_inp_args->method = METHOD_INIT;
		boot_inp_args->data = rs_boot_args;

		if (process_door_call(boot_inp_args, reim_server_fd) != 0) {
			return_code = DOOR_CALL_RETURN_ERROR;
			goto finished;
		}

		// Parse the FMRI file.
		if (process_fmri(rs_boot_args->boot_args->fmri,
		    B_FALSE,  METHOD_INIT, handle) != 0) {
			return_code = FAILURE_IN_PROCESSING_FMRI;
			goto finished;
		} else {
			return_code = 0;
		}
	} else {
			scds_syslog(LOG_ERR,
			    "Could not connect to door server");
			return_code = REIM_DOOR_CONNECT_FAILED;
	}

finished :

	// Cleanup steps.
	if (handle)
		scds_close(&handle);
	if (rs_boot_args) {
		if (rs_boot_args->common_args) {
			if (rs_boot_args->common_args->rs_name)
				free(rs_boot_args->common_args->rs_name);
			if (rs_boot_args->common_args->rg_name)
				free(rs_boot_args->common_args->rg_name);
			free(rs_boot_args->common_args);
		}
		if (rs_boot_args->boot_args) {
			if (rs_boot_args->boot_args->fmri)
				free(rs_boot_args->boot_args->fmri);
			free(rs_boot_args->boot_args);
		}
		free(rs_boot_args);
	}
	if (boot_inp_args)
		free(boot_inp_args);
	return (return_code);
}
