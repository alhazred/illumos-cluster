//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)smf_proxy_common.cc	1.14	09/02/27 SMI"

// This file contains helper functions used in SMF proxy methods.

// Include files.
#include <rgm/libdsdev.h>
#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <door.h>
#include <unistd.h>
#include <rgm/reim_common.h>
#include <time.h>
#include<libintl.h>
#include<locale.h>

// Debug switch.
#ifdef DEBUG
int debug = 1;
#else
int debug = 0;
#endif

//
// This is used for reading a line from the input file
// specified by the Proxied_service_instances property.
// This is used for a temporory solution untill the bug
// in Greenline for changing the default restarter is fixed.
//
#ifndef MAX_LINE_LENGTH
#define	MAX_LINE_LENGHT 200
#endif

// Used in getting the START/STOP timeouts of services.
#ifndef REP_HDL_BIND_RETRIES
#define	REP_HDL_BIND_RETRIES 10
#endif


// debug print ring buffer.
dbg_print_buf sc_dg_dbg_buf(SC_DG_DBG_BUFSIZE);
//
// Routine for tracing/logging debug info.
// Tracing is always on. Logging (into syslog and the console) is turned
// only if Debugflag is turned on.
//
void
debug_print(char *fmt, ...)
{
	uint_t	len = 0;
	char	str1[SC_DG_BUFSIZ], fmt1[SC_DG_BUFSIZ];
	va_list args;

	(void) snprintf(fmt1, SC_DG_BUFSIZ, "%s", fmt);

	// Add newline if there isn't in 'fmt'.
	len = (uint_t)strlen(fmt1);
	if (fmt1[len - 1] != '\n' && len < SC_DG_BUFSIZ - 1) {
		fmt1[len] = '\n';
		fmt1[len + 1] = '\0';
	}

	va_start(args, fmt);	//lint !e40
	if (vsnprintf(str1, SC_DG_BUFSIZ, fmt1, args) >= SC_DG_BUFSIZ) {
		str1[SC_DG_BUFSIZ - 1] = '\0';
	}
	va_end(args);

	sc_dg_dbg_buf.dbprintf(str1);

	if (debug) {
		syslog(LOG_NOTICE, str1);
		(void) fprintf(stderr, str1);
	}
}


// Utility function to check the validity of file.
int
check_file(char *filepath) {
	struct stat buff;
	return (stat(filepath, &buff));
}

//  Error message that we cannot allocate memory
void
proxysvc_errmsgnomem(void) {
	(void) fprintf(stderr, gettext("Out of memory.\n"));
}


// Function to get the timeout for services.
void
get_svc_timeout(char *svc_inst, const char *pg_name, const char
			*prop_name, int64_t *int_val, int *err)
{
	scf_handle_t		*rep_handle;
	scf_simple_prop_t	*sprop;
	int64_t			*i_val;
	int retry_count;

	*err = 0;

	//
	// Create and bind the handle for the repository
	//
	if ((rep_handle = scf_handle_create(SCF_VERSION)) == NULL) {
		//
		// SCMSGS
		// @explanation
		// Error in creating the handle to interact with the SMF
		// repository.
		// @user_action
		// Check the SMF manpage to know more about the error.Also
		// make sure the basic SMF functionalities are working
		// fine.Contact SUN vendor for more help.
		//
		scds_syslog(LOG_ERR,
		    "Could not create the repository handle: %s",
		    scf_strerror(scf_error()));
		*err = -1;
	}


	for (retry_count = 0; retry_count <= REP_HDL_BIND_RETRIES;
	    retry_count++) {
		if ((scf_handle_bind(rep_handle) == 0) ||
		    (scf_error() == SCF_ERROR_IN_USE))
			break;
		}

	if (retry_count >= REP_HDL_BIND_RETRIES) {
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		scds_syslog(LOG_ERR,
		    "Error in binding the handle %s",
		    scf_strerror(scf_error()));
		*err = -1;
	}

	//
	// Get the method from the property of the instance
	//
	sprop = scf_simple_prop_get(rep_handle, svc_inst, pg_name, prop_name);


	if (sprop == NULL) {
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		scds_syslog(LOG_ERR, "scf_simple_prop_get could not retrieve"
			    "the value of the prop %s of svc inst %s",
			    prop_name, svc_inst);
		*err = -1;
		return;
	} else {
		//
		// Here have to incorporate the logic if more than one
		// values exist in the property to concat all values.
		// As of now assume one val
		//
		while ((i_val = (int64_t *)
		    scf_simple_prop_next_count(sprop)) != NULL) {
			*int_val = *i_val;
		}
		if (scf_error() != SCF_ERROR_NONE) {
			//
			// An error occurred during reading the
			// property
			//

			//
			// SCMSGS
			// @explanation
			// Need explanation of this message!
			// @user_action
			// Need a user action for this message.
			//
			scds_syslog(LOG_ERR,
			    "Error Reading service property %s",
			    scf_strerror(scf_error()));
			*err = -1;
		}
		scf_simple_prop_free(sprop);
	}
}


// This function is used to xgr the proxy resource input args.
bool_t
xdr_smf_proxy_resource_input_args(register XDR *xdrs,
    smf_proxy_rs_input_args_t *res_args)
{
	if (!xdr_int(xdrs, (int *)&res_args->method)) {
		return (FALSE);
	}

	switch (res_args->method) {
		case METHOD_BOOT:
		case METHOD_START:
		case METHOD_INIT:
		case METHOD_VALIDATE:
		case METHOD_VALIDATE_IS_SVC_MANAGED:
		case METHOD_VALIDATE_U:
			if (!xdr_smf_proxy_rs_boot_start_args(xdrs,
			    (smf_proxy_rs_boot_start_args *)res_args->data)) {
				return (FALSE);
			}
			break;
		case METHOD_STOP:
		case METHOD_FINI:
		case METHOD_START_IS_SVC_OFFLINE:
		case METHOD_START_IS_RES_REG:
			if (!xdr_smf_proxy_rs_common_args(xdrs,
			    (smf_proxy_rs_common_args *)res_args->data)) {
				return (FALSE);
			}
			break;
		case METHOD_IS_THIS_SVC_MANAGED:
			if (!xdr_smf_proxy_rs_boot_extra_args(xdrs,
			    (smf_proxy_rs_boot_extra_args *)res_args->data)) {
				return (FALSE);
			}
			break;
		case METHOD_RESTART:
		case METHOD_MONITOR_CHECK:
		default:
			break;
	}
	return (TRUE);
}


// This function checks if the RT is of PROXY_SMF type.
int  is_smf_proxy_resource_type(scds_handle_t handle) {
	char *rt_name = NULL;
	rt_name = strdup(scds_get_resource_type_name(handle));
	if ((strcmp(rt_name, "PROXY_SMF_FAILOVER")) ||
		(strcmp(rt_name, "PROXY_SMF_LOAD_BAlANCED")) ||
		(strcmp(rt_name, "PROXY_SMF_MULTI_MASTER")))
		return (1);
	else
		return (0);
}


// Interface to block signals.
static void
block_signals(sigset_t sig_set, sigset_t orig_set) {

	int err;
	// block all signals during the door call
	if ((err = sigfillset(&sig_set)) != 0) {
		debug_print(NOGET("sigfillset: %d\n"), err);
		exit(err);
	}

	if ((err = pthread_sigmask(SIG_SETMASK, &sig_set, &orig_set)) != 0) {
		debug_print(NOGET("pthread_sigmask: %d\n"), err);
		exit(err);
	}
}


// Interface to restore signals.
static void
restore_signals(const sigset_t &orig_set) {
	int err;
	/* restore the original signal mask */
	if ((err = pthread_sigmask(SIG_SETMASK, &orig_set, NULL)) != 0) {
		debug_print(NOGET("pthread_sigmask: %d\n"), err);
		exit(err);
	}
}

// This function is used to connect to the ReIM door server.
boolean_t
ReIM_clnt_connect(int *mydoor_desc) {

	// Only for debugging.
	debug_print(NOGET("DOOR PATH:%s \n"), REIM_DOOR_PATH);

	// open the door descriptor

	if ((*mydoor_desc = open(REIM_DOOR_PATH, O_RDONLY)) < 0) {
		debug_print(NOGET("Unable to open door descriptor %s"),
		    REIM_DOOR_PATH);
		return (B_FALSE);
	}
	return (B_TRUE);
}


// This function is used to call the door server.
int
make_ReIMdoor_call(int serverfd, door_arg_t *arg) {
	sigset_t sig_set = {0}, orig_set = {0};
	int err, i;

	// block all signals during the door call
	block_signals(sig_set,  orig_set);

	// We retry at most twice for EAGAIN or EINTR before giving up.
	for (i = 0; i < 3; i++) {
		if (door_call(serverfd, arg) != 0) {
			err = errno;
			if (i < 2 && (err == EAGAIN || err == EINTR)) {
				debug_print(NOGET("door_call: %d; will retry"),
				    err);
				// sleep, then retry
				(void) sleep((uint_t)i + 1);
			} else {
				// give up
				debug_print(NOGET("door_call failed: %d;"),
				    err);
				// restore signals.
				restore_signals(orig_set);
				return (-1);
			}
		} else {
			// successful call
			break;
		}
	}

	// restore signals.
	restore_signals(orig_set);
	return (0);
}

// This is a helper function to populate
// the R name and RG name into
// the smf_proxy_rs_common_args
// structure.
int
populate_proxy_rs_rg_name(smf_proxy_rs_common_args *rs_args,
    scds_handle_t handle) {
	int return_code = 0;

	// Get the RS name.
	rs_args->rs_name =
	    strdup(scds_get_resource_name(handle));

	if (rs_args->rs_name == NULL) {
		proxysvc_errmsgnomem();
		return_code = MEMORY_ALLOCATION_FAILED;
		return (return_code);
	}

	// Get the RG name.
	rs_args->rg_name =
	    strdup(
	    scds_get_resource_group_name(handle));

	if (rs_args->rg_name == NULL) {
		proxysvc_errmsgnomem();
		return_code = MEMORY_ALLOCATION_FAILED;
		return (return_code);
	}
	return (return_code);
}

// This is a helper function to populate
// smf_proxy_rs_common_args structure.
// This is called by the proxy methods.
int
populate_proxy_common_args(smf_proxy_rs_boot_start_args *rs_args,
    scds_handle_t handle) {

	int return_code = 0;
	rs_args->common_args = (smf_proxy_rs_common_args *)
	    calloc(1, sizeof (smf_proxy_rs_common_args));

	if (rs_args->common_args == NULL) {
		proxysvc_errmsgnomem();
		return_code = MEMORY_ALLOCATION_FAILED;
		return (return_code);
	}
	return_code = populate_proxy_rs_rg_name(
	    rs_args->common_args, handle);
	return (return_code);
}


// This is a helper function to populate
// smf_proxy_rs_boot_args structure.
// This is called by the proxy methods.
int
populate_proxy_boot_args(smf_proxy_rs_boot_start_args *rs_args,
    scds_handle_t handle) {

	int return_code = 0;
	scha_err_t e = SCHA_ERR_NOERR;
	// Stores the xtension property value.
	scha_extprop_value_t *xprop = NULL;

	// Allocate Memory.
	rs_args->boot_args = (smf_proxy_rs_boot_extra_args *)
	    calloc(1, sizeof (smf_proxy_rs_boot_extra_args));

	if (rs_args->boot_args == NULL) {
		proxysvc_errmsgnomem();
		return_code = MEMORY_ALLOCATION_FAILED;
		return (return_code);
	}

	// Get the FMRI ext. prop.
	(void) scds_get_ext_property(handle, "Proxied_service_instances",
		SCHA_PTYPE_STRING, &xprop);

	e = scds_get_ext_property(handle,
	    "Proxied_service_instances",
	    SCHA_PTYPE_STRING, &xprop);

	if (e != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to retrieve the "
			"property %s: %s.", "Proxied_service_instances",
			scds_error_string(e));
		return_code = ERROR_GETTING_PROXY_SVC_INSTANCES_PROP;
		return (return_code);
	}

	rs_args->boot_args->fmri = strdup(xprop->val.val_str);

	if (rs_args->boot_args->fmri  == NULL) {
		proxysvc_errmsgnomem();
		return_code = MEMORY_ALLOCATION_FAILED;
		scds_free_ext_property(xprop);
		return (return_code);
	}

	// Get the retry count.
	rs_args->boot_args->retry_cnt =
	    scds_get_rs_retry_count(handle);

	// Get the retry interval.
	rs_args->boot_args->retry_interval =
	    scds_get_rs_retry_interval(handle);

	scds_free_ext_property(xprop);
	return (return_code);
}



// This function is used to get the Timeout value for the Proxy Methods.
// decided by the method parameter
int
get_smf_proxy_method_timeout(method_type_t method, scds_handle_t handle,
    int *time_out) {
	scha_resource_t	rs_handle;
	scha_err_t e;
	int return_code = 0;
	int mytimeval = 0;

	// Open the scha handle.
	(void) scha_resource_open(scds_get_resource_name(handle),
	    scds_get_resource_group_name(handle), &rs_handle);

	// Switch based on method.
	switch (method) {
		case METHOD_START :
			e = scha_resource_get(rs_handle,
			    SCHA_START_TIMEOUT, &mytimeval);
			if (e == SCHA_ERR_SEQID) {
				//
				// SCMSGS
				// @explanation
				// Need explanation of this message!
				// @user_action
				// Need a user action for this message.
				//
				scds_syslog(LOG_ERR,
				    "Error in getting Proxy Resource"
				    " Method Timeouts");
				return_code =
				    ERROR_GETTING_SCHA_RS_TIMEOUTS;
				goto finished;
			}
		break;

		case METHOD_STOP :
			e = scha_resource_get(rs_handle,
			    SCHA_STOP_TIMEOUT, &mytimeval);
			if (e == SCHA_ERR_SEQID) {
				scds_syslog(LOG_ERR,
				    "Error in getting Proxy Resource"
				    " Method Timeouts");
				return_code =
				    ERROR_GETTING_SCHA_RS_TIMEOUTS;
				goto finished;
			}
		break;

		case METHOD_BOOT :
			e = scha_resource_get(rs_handle,
			    SCHA_BOOT_TIMEOUT, &mytimeval);
			if (e == SCHA_ERR_SEQID) {
				scds_syslog(LOG_ERR,
				    "Error in getting Proxy Resource"
				    " Method Timeouts");
				return_code =
				    ERROR_GETTING_SCHA_RS_TIMEOUTS;
				goto finished;
			}
		break;

		case METHOD_INIT :
			e = scha_resource_get(rs_handle,
			    SCHA_INIT_TIMEOUT, &mytimeval);
			if (e == SCHA_ERR_SEQID) {
				scds_syslog(LOG_ERR,
				    "Error in getting Proxy Resource"
				    " Method Timeouts");
				return_code =
				    ERROR_GETTING_SCHA_RS_TIMEOUTS;
				goto finished;
			}
		break;

		case METHOD_FINI :
			e = scha_resource_get(rs_handle,
			    SCHA_FINI_TIMEOUT, &mytimeval);
			if (e == SCHA_ERR_SEQID) {
				scds_syslog(LOG_ERR,
				    "Error in getting Proxy Resource"
				    " Method Timeouts");
				return_code =
				    ERROR_GETTING_SCHA_RS_TIMEOUTS;
				goto finished;
			}
		break;

		case METHOD_VALIDATE :
			e = scha_resource_get(rs_handle,
			    SCHA_VALIDATE_TIMEOUT, &mytimeval);
			if (e == SCHA_ERR_SEQID) {
				scds_syslog(LOG_ERR,
				    "Error in getting Proxy Resource"
				    " Method Timeouts");
				return_code =
				    ERROR_GETTING_SCHA_RS_TIMEOUTS;
				goto finished;
			}
		break;

		case METHOD_MONITOR_CHECK :
			e = scha_resource_get(rs_handle,
			    SCHA_MONITOR_CHECK_TIMEOUT,
			    &mytimeval);
			if (e == SCHA_ERR_SEQID) {
				scds_syslog(LOG_ERR,
				    "Error in getting Proxy Resource"
				    " Method Timeouts");
				return_code =
				    ERROR_GETTING_SCHA_RS_TIMEOUTS;
				goto finished;
			}
		break;

		default:
		break;
	}

finished:
	*time_out = mytimeval;
	(void) scha_resource_close(rs_handle);
	return (return_code);
}


// This function is used for two purposes
// 1.Checking the state of restarter is online
// in case of Stop,Fini,Monitor chk.
// 2.Enabling the restarter if its disabled in case
// of start,init,boot or validate method.
// If the restarter is not online then we wait till
// the method timeout occurs.We return error
// once the method timout occurs.
int
check_and_enable_sc_restarter(check_restarter_flag_t flag, int timeout) {
	char *delegated_restarter_state = NULL;
	int return_code = 0;
	time_t begin_time = 0, current_time = 0;

	// Get the delegated restarter state.
	(smf_get_state("svc:/system/cluster/sc_restarter:default")?
	    delegated_restarter_state = strdup(
	    smf_get_state("svc:/system/cluster/sc_restarter:default")):
	    0);
	// delegated_restarter_state = NULL);
	// if NULL then SC restarter is not configured.
	if (delegated_restarter_state == NULL) {
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		scds_syslog(LOG_ERR, "SC Delegated Restarter not configured"
		    " on this node");
		return_code = SC_DELEGATED_RESTARTER_NOT_CONFIGURED;
		goto finished;
	}
	// Case to handle enabling if restarter is disabled.
	if (flag == CHECK_ENABLE_SC_RESTARTER) {
		// check if delegated restarter state is disabled
		if (strcasecmp(delegated_restarter_state,
		    "disabled") == 0) {
			// enable the restarter.
			if (smf_enable_instance(
			    "svc:/system/cluster/sc_restarter:default",
			    0) != 0) {
				// if enabling fails wait till
				// restarter becomes online
				// or method timeouts.
				while (strcasecmp(
				    delegated_restarter_state,
				    "online") != 0) {
					(void) time(&current_time);
					if ((int)(current_time -
					    begin_time) > timeout) {
						//
						// SCMSGS
						// @explanation
						// Need explanation of this
						// message!
						// @user_action
						// Need a user action for this
						// message.
						//
						scds_syslog(LOG_ERR,
						    "SC Delegated Restarter "
						    "Enabling Failed"
						    " on this node : %s",
						    scf_strerror(scf_error()));
						return_code =
						    SC_RESTARTER_ENABLE_FAILED;
						goto finished;
					} else {
						(void) sleep(5);
					}
				}
			} else {
				//
				// SCMSGS
				// @explanation
				// Need explanation of this message!
				// @user_action
				// Need a user action for this message.
				//
				scds_syslog(LOG_ERR, "SC Delegated Restarter"
				    " Enabled on this node");
			}
		}
	} else if (flag == CHECK_SC_RESTARTER) {

		// Check if restarter is online
		if (strcasecmp(delegated_restarter_state,
		    "online") != 0) {
			//
			// loop till restarter is online
			// or method times out.
			//
			(void) time(&begin_time);
			while (strcasecmp(
			    delegated_restarter_state,
			    "online") != 0) {
				(void) time(&current_time);
				if ((int)(current_time -
				    begin_time) > timeout) {
					//
					// SCMSGS
					// @explanation
					// Need explanation of this message!
					// @user_action
					// Need a user action for this
					// message.
					//
					scds_syslog(LOG_ERR,
					    "SC Delegated Restarter"
					    " Not Online");
					return_code =
					    SC_RESTARTER_NOT_ONLINE;
					goto finished;
				} else {
					(void) sleep(5);
				}
			}
		}
	}
finished:
	if (delegated_restarter_state)
		free(delegated_restarter_state);
	return (return_code);
}

//
// Helper function for encoding the data in xdr format
// and making the door call.
//
int process_door_call(smf_proxy_rs_input_args_t  *rs_args,
    int reim_server_fd) {
	// door args.
	door_arg_t proxy_door_arg = {0};
	// door call result.
	int door_call_result = -1;
	// XDR Streams for arguments and result
	XDR xdrs;
	// Stores the marshalled arguments to be sent via
	// door_call
	char *arg_buf = NULL;
	// Stores the total size of the arguments being passed
	size_t arg_size;
	// Return code.
	int return_code = -1;

	// Calculate the size of the arguments being passed
	// need to do some research on xdr_sizeof
	// arg_size = xdr_sizeof(boot_inp_args);
	arg_size = 2000;

	// Allocate the space to store marshalled data
	arg_buf = (char *)calloc(1, arg_size);

	if (arg_buf == NULL) {
		proxysvc_errmsgnomem();
		return_code = MEMORY_ALLOCATION_FAILED;
		goto finished;
	}

	// Initialize the XDR Stream for Encoding data
	xdrmem_create(&xdrs, arg_buf, arg_size, XDR_ENCODE);

	// Use xdr function to marshall the input arguments
	if (!xdr_smf_proxy_resource_input_args(&xdrs, rs_args)) {
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		scds_syslog(LOG_ERR,
		    "XDR Error in encoding arguments");
		door_call_result = XDR_ERROR_AT_CLIENT;
		goto finished;
	}

	// Prepare the door arguments before making the door call
	proxy_door_arg.data_ptr = arg_buf;
	proxy_door_arg.data_size = xdr_getpos(&xdrs);
	proxy_door_arg.desc_ptr = NULL;
	proxy_door_arg.desc_num = 0;
	proxy_door_arg.rbuf = (char *)&door_call_result;
	proxy_door_arg.rsize = sizeof (int);
	// Even if we set this as null the door_return
	// will set it to the correct size when the
	// door server returns

	// Make the door call
	if (make_ReIMdoor_call(reim_server_fd,
	    &proxy_door_arg) < 0) {
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		scds_syslog(LOG_ERR, "Error making ReIM door call");
		return_code = UNABLE_TO_MAKE_DOOR_CALL;
		goto finished;
	}

	debug_print(NOGET("Returned from door call. Size = %d"),
	    proxy_door_arg.rsize);
	//
	// Now, we are back from the door_call and we need to
	// unmarshall the
	// data and get the return_code.
	// But if the returned size is 0 and the rbuf = NULL, then
	// we can be
	// sure that a severe error has occured at the server and
	// it has not
	// been able to complete the call successfully.
	//

	// Check if the returned result and rsize are NULL

	if ((proxy_door_arg.rbuf == NULL) &&
	    (proxy_door_arg.rsize == 0)) {
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		scds_syslog(LOG_ERR, "XDR args returned NULL by server");
		return_code = XDR_ERROR_AT_SERVER;
		goto finished;
	}

	if (rs_args->method == METHOD_FINI) {
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		syslog(LOG_ERR, "fini*** %d", door_call_result);
		if (door_call_result > 0) {
			return_code = 0;

		} else if (door_call_result == 0) {
			//
			// SCMSGS
			// @explanation
			// Need explanation of this message!
			// @user_action
			// Need a user action for this message.
			//
			syslog(LOG_ERR, "Resarter disabled");
			return_code = 0;
		}

	} else {
		if (door_call_result ==  0)
			return_code = 0;
	}
finished :

	// Cleanup steps.
	if (arg_buf)
		free(arg_buf);
	xdr_destroy(&xdrs);
	//
	// SCMSGS
	// @explanation
	// Need explanation of this message!
	// @user_action
	// Need a user action for this message.
	//
	scds_syslog(LOG_ERR, "Return the door call result: %d", return_code);
	return (return_code);
}

//
// This function checks if boot/init method was successful.
// This function returns true if the service is already registered.
// This is used by the start method.If this function returns true then
// start method does not need to register the svc.
// Note:fmri can be a single svc fmri or svc fmri file from the extended prop
//
boolean_t
get_resource_status_info(int reim_server_fd, char *r_name,
    char *rg_name, check_proxy_svc_info_t flag, char *fmri = NULL) {
	smf_proxy_rs_input_args *svc_inp_args = NULL;
	smf_proxy_rs_common_args *rs_svc_args = NULL;
	smf_proxy_rs_boot_extra_args *svc_xtra_args = NULL;
	int return_code = -1;

	if (flag == CHECK_IS_THIS_SVC_MANAGED) {
		svc_xtra_args = (smf_proxy_rs_boot_extra_args *)
		calloc(1, sizeof (smf_proxy_rs_boot_extra_args));

		if (svc_xtra_args == NULL) {
			proxysvc_errmsgnomem();
			return_code = MEMORY_ALLOCATION_FAILED;
			goto finished;
		}
		if (fmri == NULL) {
			//
			// SCMSGS
			// @explanation
			// Need explanation of this message!
			// @user_action
			// Need a user action for this message.
			//
			scds_syslog(LOG_ERR, "fmri value expected to check"
				    "if it is managed");
			goto finished;
		} else {
			//
			// SCMSGS
			// @explanation
			// Need explanation of this message!
			// @user_action
			// Need a user action for this message.
			//
			scds_syslog(LOG_ERR, "fmri value in check"
				    "if it is managed %s", fmri);
		}
		svc_xtra_args->fmri = strdup(fmri);

		// These two values are not required.Just to keep XDR type
		// checking happy on the server, it is assigned 0 to safety
		svc_xtra_args->retry_interval = 0;
		svc_xtra_args->retry_cnt = 0;
	} else {
		// Allocate memory.
		rs_svc_args = (smf_proxy_rs_common_args *)
			calloc(1, sizeof (smf_proxy_rs_common_args));

		if (rs_svc_args == NULL) {
			proxysvc_errmsgnomem();
			return_code = MEMORY_ALLOCATION_FAILED;
			goto finished;
		}

		// Get the R and RG name.
		rs_svc_args->rs_name = strdup(r_name);

		if (rs_svc_args->rs_name == NULL) {
			proxysvc_errmsgnomem();
			return_code = MEMORY_ALLOCATION_FAILED;
			goto finished;
		}

		rs_svc_args->rg_name = strdup(rg_name);

		if (rs_svc_args->rs_name == NULL) {
			proxysvc_errmsgnomem();
			return_code = MEMORY_ALLOCATION_FAILED;
			goto finished;
		}
	}
	// Allocate memory for storing the arguments to be passed
	svc_inp_args = (smf_proxy_rs_input_args *)
		calloc(1, sizeof (smf_proxy_rs_input_args));

	if (svc_inp_args == NULL) {
		proxysvc_errmsgnomem();
		return_code = MEMORY_ALLOCATION_FAILED;
		goto finished;
	}

	// Prepare the container structure
	switch (flag) {
		case CHECK_PROXY_BOOT_METHOD_SUCCESSFUL :
			svc_inp_args->method = METHOD_START_IS_RES_REG;
			svc_inp_args->data = rs_svc_args;
			break;
		case CHECK_PROXIED_SVC_OFFLINE :
			svc_inp_args->method = METHOD_START_IS_SVC_OFFLINE;
			svc_inp_args->data = rs_svc_args;
			break;
		case CHECK_IS_THIS_SVC_MANAGED :
			svc_inp_args->method = METHOD_IS_THIS_SVC_MANAGED;
			svc_inp_args->data = svc_xtra_args;
			break;
		default:
			break;
	}

	// open and attach to door server and call the door call
	if (ReIM_clnt_connect(&reim_server_fd)) {
		if (process_door_call(svc_inp_args, reim_server_fd) == 0)
			return_code = 0;
	} else {
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		scds_syslog(LOG_ERR, "Could not connect to door server");
		return_code = REIM_DOOR_CONNECT_FAILED;
	}
	//
	// SCMSGS
	// @explanation
	// Need explanation of this message!
	// @user_action
	// Need a user action for this message.
	//
	scds_syslog(LOG_ERR, "In get status info door result: %d", return_code);
finished:
	// cleanup.
	if (rs_svc_args) {
		if (rs_svc_args->rs_name)
			free(rs_svc_args->rs_name);
		if (rs_svc_args->rg_name)
			free(rs_svc_args->rg_name);
		free(rs_svc_args);
	}
	if (svc_xtra_args) {
		if (svc_xtra_args->fmri)
			free(svc_xtra_args->fmri);
		free(svc_xtra_args);
	}
	if (svc_inp_args)
		free(svc_inp_args);
	if (return_code == 0)
		return (B_TRUE);
	else
		return (B_FALSE);
}



//
// This function is written as temporory solution for changing the
// the default restarter of the proxied greenline service to SC restarter.
// Right now there is no way to change the restarter for the greenline service
// and the only solution is to programitically parse the manifest file for
// the service and change the restarter in the manifest file.
// For achieving this we need to find the manifest file for the svc,
// make a copy of that manifest file, disable the service instances,
// delete the service, parse and change the manifest file and finally
// import the changed manifest file and re-enable the proxied
// greenline service under SC restarter.
// THIS FUNCTION WILL BE DELETED ONCE THE BUG OF
// CHANGING THE RESTARTER IS FIXED IN THE GREENLINE.
// This function opens the FMRI file.
// Input parameter input_file is the path of the FMRI file
// The fmri file syntax is shown as below
// <FMRI SVC1> <Path to manifest file1>
// <FMRI SVC2> <Path to manifest file2>
// <FMRI SVC3> <Path to manifest file3>
// <FMRI SVCn> <Path to manifest filen>
// The parser code parses the file and gets the FMRI svc value
// as well as the manifest file.
// The function further disables the FMRI svc and FMRI default instance.
// The maniest file is copied to the SC restarter temp location
// provided by the PROXY_SVC_MANIFEST_DIR.
// Once the FMRI svc and instance is disabled it runs the parse_file.ksh
// parser script to check if the svc is under any other restarter and
// changes the restarter to SC_delegated restarter.
// After changing the restarter the new changed manifest file is imported
// and svc is re-enabled.
// Create flag if true then this is during the R creation.If false then its
// case of R updation.


int
process_fmri(char *input_file, boolean_t res_create_flag,
    method_type_t method, scds_handle_t handle) {

	// File pointer.
	FILE *fp = NULL;

	// To store data read from file.
	char stream_read[MAX_LINE_LENGHT] = {0};
	// Filename
	char *file_name = NULL;
	// Complete filepath
	char *file_path = NULL;
	// FMRI instance.
	char *fmri_val = NULL;
	// FMRI SVC.
	char *fmri_svc = NULL;
	// Utility pointers.
	char *ptr1 = NULL, *ptr2 = NULL;
	// outstr to hold the command that is to be executed.
	char outstr[200];
	int resource_start_timeout = 0;
	int resource_stop_timeout = 0;
	int64_t svc_start_timeout = 0;
	int64_t svc_stop_timeout = 0;
	int err = 0;
	int return_code = 0;
	boolean_t empty_file = B_TRUE;
	// If this is called during R creation the create the temp directory.
	if ((res_create_flag) && (method == METHOD_VALIDATE)) {


		(void) sprintf(outstr, "mkdir -p %s", PROXY_SVC_MANIFEST_DIR);
		if (system(outstr) != 0) {
			//
			// SCMSGS
			// @explanation
			// Need explanation of this message!
			// @user_action
			// Need a user action for this message.
			//
			scds_syslog(LOG_ERR,
			    "Creation of Temp Dir for Manifest "
			    "files failed");
			return_code = CREATION_OF_MANIFEST_TEMP_DIR_FAILED;
			goto finished;
		}
	}

	// Open the FMRI file.
	if ((fp = fopen(input_file, "r")) == NULL) {
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		scds_syslog(LOG_ERR, "Can't open FMRI instance file %s",
		    input_file);
		return_code = OPEN_FMRI_FILE_FAILED;
		goto finished;
	}

	// read the file.
	while (!feof(fp)) {
		(void) fscanf(fp, "%s", stream_read);
		if (strcmp(stream_read, "") == 0)
			break;
		empty_file = B_FALSE;
		// Get the filepath.
		ptr1 = strrchr(stream_read, '>');
		ptr2 = strrchr(stream_read, '<');

		//
		// Check if invalid format is followed
		// while entering the Manifest filename.
		//
		if ((ptr1 == NULL) || (ptr2 == NULL) || (ptr1 < ptr2)) {
			//
			// SCMSGS
			// @explanation
			// Need explanation of this message!
			// @user_action
			// Need a user action for this message.
			//
			scds_syslog(LOG_ERR,
			    "Invalid Manifest Filename specified in"
			    " the file list specified by the "
			    "Proxied_service_instanses prop");
			return_code = INVALID_MANIFEST_FILE_NAME_SPECIFIED;
			goto finished;
		}

		file_path = (char *)calloc(1, (unsigned int)(ptr1 - ptr2));
		if (file_path == NULL) {
			proxysvc_errmsgnomem();
			return_code = MEMORY_ALLOCATION_FAILED;
			goto finished;
		}
		(void) strncpy(file_path, (ptr2 + 1),
		    (unsigned int)(ptr1 - ptr2 - 1));
		debug_print(NOGET("filepath : %s"), file_path);

		// check the validity of the file.
		if (check_file(file_path)) {
			scds_syslog(LOG_ERR,
			    "Invalid Manifest Filename specified in"
			    " the file list specified by the "
			    "Proxied_service_instanses prop");
			return_code = INVALID_MANIFEST_FILE_NAME_SPECIFIED;
			goto finished;
		}

		// Get the filename.
		ptr1 = NULL;
		ptr2 = NULL;

		ptr1 =  file_path + strlen(file_path);
		ptr2 = strrchr(file_path, '/');

		if (ptr2 != NULL) {
			file_name = (char *)calloc(1, (unsigned int)
			    (ptr1 - ptr2 + 1));
			if (file_name == NULL) {
				proxysvc_errmsgnomem();
				return_code = MEMORY_ALLOCATION_FAILED;
				goto finished;

			}
			(void) strncpy(file_name, ptr2 + 1,
			    (unsigned int) (ptr1 - ptr2));
		} else {
			file_name = strdup(file_path);
			if (file_name == NULL) {
				proxysvc_errmsgnomem();
				return_code = MEMORY_ALLOCATION_FAILED;
				goto finished;

			}
		}

		debug_print(NOGET("filename : %s"), file_name);

		if (method != METHOD_FINI) {
			(void) sprintf(outstr, "cp %s %s", file_path,
			    PROXY_SVC_MANIFEST_DIR);
			(void) system(outstr);
			if (file_path)
				free(file_path);

			file_path = (char *)calloc(1,
			    strlen(PROXY_SVC_MANIFEST_DIR) +
			    strlen(file_name) + 2);
			if (file_path == NULL) {
				proxysvc_errmsgnomem();
				return_code = MEMORY_ALLOCATION_FAILED;
				goto finished;
			}
			(void) sprintf(file_path,
			    "%s/%s", PROXY_SVC_MANIFEST_DIR, file_name);
		}

		// Get the FMRI value.

		ptr1 = strchr(stream_read, '<');
		ptr2 = strchr(stream_read, '>');
		if ((ptr1 == NULL) || (ptr2 == NULL) || (ptr2 < ptr1)) {
			//
			// SCMSGS
			// @explanation
			// Need explanation of this message!
			// @user_action
			// Need a user action for this message.
			//
			scds_syslog(LOG_ERR, "Invalid FMRI Service specified "
				"in the file list specified by the "
				"Proxied_service_instanses prop");
			return_code = INVALID_FMRI_SVC_SPECIFIED;
			goto finished;
		}

		fmri_val = (char *)calloc(1, (unsigned int) (ptr2 - ptr1));
		if (fmri_val == NULL) {
			proxysvc_errmsgnomem();
			return_code = MEMORY_ALLOCATION_FAILED;
			goto finished;
		}

		(void) strncpy(fmri_val, ptr1 + 1,
		    (unsigned int) (ptr2 - ptr1 - 1));
		debug_print(NOGET("%s\n"), fmri_val);

		// Get the FMRI instance.
		ptr1 = fmri_val;
		ptr2 = strrchr(fmri_val, ':');
		if (ptr2 == NULL) {
			//
			// SCMSGS
			// @explanation
			// Need explanation of this message!
			// @user_action
			// Need a user action for this message.
			//
			scds_syslog(LOG_ERR, "Invalid FMRI Service specified "
				"in the file list specified by the"
				" Proxied_service_instances prop");
			return_code = INVALID_FMRI_SVC_SPECIFIED;
			goto finished;
		}

		fmri_svc = (char *)calloc(1, (unsigned int) (ptr2 - ptr1 + 1));
		if (fmri_svc == NULL) {
			proxysvc_errmsgnomem();
			return_code = MEMORY_ALLOCATION_FAILED;
			goto finished;
		}
		(void) strncpy(fmri_svc, ptr1, (unsigned int) (ptr2 - ptr1));

		// If R creation then check the svc timeout and compare with
		// resource timeouts.
		if (method != METHOD_FINI) {
			if (res_create_flag) {
				// Get timeout for svc start.
				get_svc_timeout(fmri_val, "start",
				    "timeout_seconds",
				    &svc_start_timeout, &err);
				if (err) {
					get_svc_timeout(fmri_val, "inetd_start",
					    "timeout_seconds",
					    &svc_start_timeout, &err);

					if (err == 0) {
						fprintf(stderr, gettext("Error:"
						    "The service %s can not be"
						    "proxied under the sun "
						    "cluster restarter as its"
						    " default restarter"
						    " is inetd."),
						    fmri_val);
					}
					//
					// SCMSGS
					// @explanation
					// Need explanation of this message!
					// @user_action
					// Need a user action for this
					// message.
					//
					scds_syslog(LOG_ERR,
					    "Error getting start timeout for "
					    "proxied service");
					return_code =
					    SERVICE_TIMEOUT_RETRIVAL_FAILED;
					goto finished;
				}
				// Get timeout for svc stop.
				get_svc_timeout(fmri_val, "stop",
				    "timeout_seconds",
				    &svc_stop_timeout, &err);
				if (err) {
					//
					// SCMSGS
					// @explanation
					// Need explanation of this message!
					// @user_action
					// Need a user action for this
					// message.
					//
					scds_syslog(LOG_ERR,
					    "Error getting stop timeout for "
					    "proxied service");
					return_code =
					    SERVICE_TIMEOUT_RETRIVAL_FAILED;
					goto finished;
				}

				// Get R Method START and STOP timeout.
				resource_start_timeout =
				    scds_get_rs_start_timeout(handle);
				resource_stop_timeout =
				    scds_get_rs_stop_timeout(handle);
				//
				// Validate timeouts.R timeouts are
				// to be greater than
				// svc timeouts.
				//
				if ((resource_start_timeout
				    < svc_start_timeout) ||
				    (resource_stop_timeout
				    < svc_stop_timeout)) {

					//
					// SCMSGS
					// @explanation
					// Need explanation of this message!
					// @user_action
					// Need a user action for this
					// message.
					//
					scds_syslog(LOG_ERR,
					    "Proxy Method timeout less than "
					    "Proxied sevices timeout");
					return_code = RESOURCE_TIMEOUT_LESS;
					goto finished;
				}
			} else {
				// Check if its update.In that case, only the
				// updated service need to be changed
				if (method == METHOD_VALIDATE_U) {
					int reim_server_fd = -1;
					if (get_resource_status_info(
					reim_server_fd, NULL, NULL,
					CHECK_IS_THIS_SVC_MANAGED, fmri_val)) {
						// This svc is already present
						continue;
					}
				}
				// Disable FMRI svc.
				if (smf_disable_instance(fmri_val, 0) != 0) {
					//
					// SCMSGS
					// @explanation
					// Need explanation of this message!
					// @user_action
					// Need a user action for this
					// message.
					//
					scds_syslog(LOG_ERR,
					    "Cannot disable FMRI: %s",
					    scf_strerror(scf_error()));
					return_code =
					    SMF_DISABLE_SERVICE_FAILED;
					goto finished;
				}

				(void) sleep(5);
				debug_print(NOGET("\n\n--Disabled Inst--\n"));
				// Delete FMRI svc.
				(void) sprintf(
				    outstr, "/usr/sbin/svccfg delete %s",
				    fmri_val);
				if (system(outstr) != 0) {
					//
					// SCMSGS
					// @explanation
					// Need explanation of this message!
					// @user_action
					// Need a user action for this
					// message.
					//
					scds_syslog(LOG_ERR,
					    "Deletion of FMRI %s failed",
					    fmri_val);
					return_code =
					    DELETION_SVC_INSTANCE_FAILED;
					goto finished;
				}

				(void) sleep(5);
				debug_print(NOGET("\n\n--Deleted Inst--\n"));

				// Delete FMRI instance.
				(void) sprintf(outstr,
				    "/usr/sbin/svccfg delete %s", fmri_svc);
				if (system(outstr) != 0) {
					scds_syslog(LOG_ERR,
					    "Deletion of FMRI %s failed\n",
					    fmri_svc);
					return_code =
					    DELETION_SVC_INSTANCE_FAILED;
					goto finished;
				}

				(void) sleep(5);
				debug_print(NOGET("\n\n--Deleted SVC--\n"));

				// check the validity of the file.
				if (check_file(
				    "/opt/SUNWscsmf/etc/parse_file.ksh")) {
					//
					// SCMSGS
					// @explanation
					// Need explanation of this message!
					// @user_action
					// Need a user action for this
					// message.
					//
					scds_syslog(LOG_ERR,
					    "Parse file parse_file.ksh"
					    " not found");
					return_code =
					    PARSE_FILE_SCRIPT_NOT_FOUND;
					goto finished;
				}

				// fstat here.
				// Run the parser to change the restarter.

				//
				// SCMSGS
				// @explanation
				// Need explanation of this message!
				// @user_action
				// Need a user action for this message.
				//
				syslog(LOG_ERR, " **%s**", file_path);
				(void) sprintf(outstr,
				    "/opt/SUNWscsmf/etc/parse_file.ksh %s",
				    file_path);

				if (system(outstr) != 0) {
					//
					// SCMSGS
					// @explanation
					// Need explanation of this message!
					// @user_action
					// Need a user action for this
					// message.
					//
					scds_syslog(LOG_ERR,
					    "Can't open Manifest File %s",
					    file_path);
					return_code = OPEN_MANIFEST_FILE_FAILED;
					goto finished;
				}

				// check the validity of the file.
				if (check_file(file_path)) {
				return_code =
				    INVALID_MANIFEST_FILE_NAME_SPECIFIED;
					goto finished;
				}
				// Import the manifest file.

				(void) sprintf(outstr,
				    "/usr/sbin/svccfg import %s", file_path);
				if (system(outstr) != 0) {
					//
					// SCMSGS
					// @explanation
					// Need explanation of this message!
					// @user_action
					// Need a user action for this
					// message.
					//
					scds_syslog(LOG_ERR,
					    "Can't import Manifest File%s",
					    file_path);
					return_code =
					    IMPORT_MANIFEST_FILE_FAILED;
					goto finished;
				}

				(void) sleep(5);
				debug_print(NOGET("\n\n--Imported Svc--\n"));

				// Enable the FMRI instance.
				if (smf_enable_instance(fmri_val, 0) != 0) {
					//
					// SCMSGS
					// @explanation
					// Need explanation of this message!
					// @user_action
					// Need a user action for this
					// message.
					//
					scds_syslog(LOG_ERR,
					    "Cannot Re-Enable FMRI: %s",
					    scf_strerror(scf_error()));
					return_code =
					    SMF_REENABLE_SERVICE_FAILED;
					goto finished;
				}
				(void) sleep(5);

				debug_print(NOGET("\n\n--Enabled Svc--\n"));
			}
		}

		if (method == METHOD_FINI) {
			// Import the manifest file.

			debug_print(NOGET("\n\n--Deleted Inst--\n"));


			// Delete FMRI instance.
			(void) sprintf(outstr,
			    "/usr/sbin/svccfg delete %s", fmri_svc);
			if (system(outstr) != 0) {
				scds_syslog(LOG_ERR,
				    "Deletion of FMRI %s failed\n", fmri_svc);
				return_code = DELETION_SVC_INSTANCE_FAILED;
				goto finished;
			}

			(void) sleep(5);
			debug_print(NOGET("\n\n--Deleted SVC--\n"));

			(void) sprintf(outstr,
			    "/usr/sbin/svccfg import %s", file_path);
			if (system(outstr) != 0) {
				scds_syslog(LOG_ERR,
				    "Can't import Manifest File%s", file_path);
				return_code = IMPORT_MANIFEST_FILE_FAILED;
				goto finished;
			}

			(void) sleep(5);
			debug_print(NOGET("\n\n--Imported Svc--\n"));
		}
		(void) memset(stream_read, 0, sizeof (stream_read));
		if (fmri_val)
			free(fmri_val);
		if (file_name)
			free(file_name);
		if (file_path)
			free(file_path);
		if (fmri_svc)
			free(fmri_svc);
	}

	if (empty_file)  {
		scds_syslog(LOG_ERR, "Invalid FMRI Service specified in"
		    " the file list specified by the "
		    "Proxied_service_instanses prop");
		return_code = INVALID_FMRI_SVC_SPECIFIED;
		goto finished;
	}
	debug_print(NOGET("Going out of process FMRI"));
finished:
	if (fmri_val)
		free(fmri_val);
	if (file_name)
		free(file_name);
	if (file_path)
		free(file_path);
	if (fmri_svc)
		free(fmri_svc);
	return (return_code);
}
