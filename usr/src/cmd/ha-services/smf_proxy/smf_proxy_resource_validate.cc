//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)smf_proxy_resource_validate.cc	1.17	09/03/02 SMI"
//
// This file contains the validate method implementation
// of SMF proxy resource.
//
#include "smf_proxy_common.h"
#include <locale.h>
#include <libintl.h>


// Main routine.
int
main(int argc, char *argv[]) {
	int c, cu_flag = 0;
	smf_proxy_rs_boot_start_args *rs_val_args = {0};
	// Container struct for storing the input arguments
	// that are to be sent to the server via the door_call
	smf_proxy_rs_input_args *val_inp_args = NULL;

	// Stores the total size of the arguments being passed
	scha_resource_t	rs_handle;
	scha_rsstate_t state;
	scha_err_t e;
	int rc;
	int err = 0;

	scds_handle_t handle = NULL;
	int reim_server_fd = -1;

	// Return code.
	int return_code = -1;

	(void) setlocale(LC_ALL, "");
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);
	(void) textdomain(TEXT_DOMAIN);

	int method_time_out = -1;
	// Init the SCDS handle.
	if (scds_initialize(&handle, argc, argv) != 0) {
		fprintf(stderr,
		    gettext("%s initialization failure\n"), argv[0]);
		return_code = ERROR_GETTING_SCDS_HANDLE;
		goto finished;
	}

	if ((return_code = get_smf_proxy_method_timeout(
		METHOD_VALIDATE,  handle,
		&method_time_out)) != 0) {
		return_code = ERROR_GETTING_METH_TIMEOUT;
		goto finished;
	}


	// Check the RT type is of SMF_PROXY.
	if (!is_smf_proxy_resource_type(handle)) {
		fprintf(stderr,
		    gettext("Resource type"
		    " is not of PROXY SMF type"));
		return_code = RESOURCE_TYPE_NOT_PROXY_SMF_TYPE;
		goto finished;
	}

	// Allocate memory for storing the
	// arguments to be passed
	rs_val_args = (smf_proxy_rs_boot_start_args *)
		calloc(1,
		    sizeof (smf_proxy_rs_boot_start_args));

	if (rs_val_args == NULL) {
		proxysvc_errmsgnomem();
		return_code = MEMORY_ALLOCATION_FAILED;
		goto finished;
	}

	// Populate the common args R Name and RG Name
	if ((return_code = populate_proxy_common_args(
	    rs_val_args, handle)) != 0) {
		goto finished;
	}

	// Populate the FMRI,retry count and
	// retry interval.
	if ((return_code = populate_proxy_boot_args(
	    rs_val_args, handle)) != 0) {
		goto finished;
	}

	// Allocate memory for storing the
	// arguments to be passed
	val_inp_args = (smf_proxy_rs_input_args_t *)
		calloc(1,
		    sizeof (smf_proxy_rs_input_args_t));

	if (val_inp_args == NULL) {
		proxysvc_errmsgnomem();
		return_code = MEMORY_ALLOCATION_FAILED;
		goto finished;
	}

	// Prepare the container structure
	val_inp_args->data = rs_val_args;

	// Check the op type create or update.
	while ((c = getopt(argc, argv, "R:T:G:r:x:g:cu")) != EOF) {
		if ((c == 'c') || (c == 'u'))
			cu_flag = c;
	}

	switch (cu_flag) {
	case 'c':

		// Create operation.
		debug_print(NOGET("In Create"));

		//
		// Check the SC delegated restarter state
		// and enable if it is disabled.
		//
		if ((return_code = check_and_enable_sc_restarter(
		    CHECK_ENABLE_SC_RESTARTER, method_time_out)) != 0) {
			goto finished;
		}

		debug_print(NOGET("--Validate sleeping--"));
		sleep(10);


		if (ReIM_clnt_connect(&reim_server_fd)) {
		// Prepare the container structure

			val_inp_args->method = METHOD_VALIDATE_IS_SVC_MANAGED;

			if (process_door_call(val_inp_args,
					    reim_server_fd) == 0) {
				fprintf(stderr,
				    gettext("Proxied service is"
				    " already managed by"
				    " another Proxy SMF resource"));
				return_code = PROXY_SVC_ALREADY_MANAGED;
				goto finished;
			}
		}

		// Process FMRI for validity.
		return_code = (process_fmri
		    (rs_val_args->boot_args->fmri, B_TRUE,
		    METHOD_VALIDATE, handle));
		debug_print(NOGET("After process FMRI %d"), return_code);
		break;


	case 'u':
		// Case for update.
		if ((return_code = check_and_enable_sc_restarter(
		    CHECK_SC_RESTARTER, method_time_out)) != 0) {
			goto finished;
		}

		// Validate FMRI
		// new service must not be managed by other restarter.
		rc = scha_resource_open(scds_get_resource_name(handle),
		    scds_get_resource_group_name(handle), &rs_handle);

		e = scha_resource_get(rs_handle, SCHA_RESOURCE_STATE, &state);
		if (e == SCHA_ERR_SEQID) {
			fprintf(stderr,
			    gettext("Error in getting Proxy SMF"
			    " Resource state"));
			(void) scha_resource_close(rs_handle);
			return_code = ERROR_GETTING_SCHA_RS_STATE;
			goto finished;
		}
		(void) scha_resource_close(rs_handle);

		if (ReIM_clnt_connect(&reim_server_fd)) {

			// Prepare the container structure
			val_inp_args->method = METHOD_VALIDATE_U;

			if (process_door_call(val_inp_args,
			    reim_server_fd) == 0)
				return_code = 0;

			//
			// Process the FMRI services and change
			// the default restarter to SC delegated
			// restarter for the FMRIs.
			//
			if (process_fmri
			    (rs_val_args->boot_args->fmri,
			    B_FALSE,
			    METHOD_VALIDATE_U,
			    handle) != 0) {
				return_code =
				    FAILURE_IN_PROCESSING_FMRI;
				goto finished;
			}
			debug_print(NOGET("After process FMRI %d"),
			    return_code);

		} else {
			fprintf(stderr,
			    gettext("Could not connect to door server\n"));
			return_code = REIM_DOOR_CONNECT_FAILED;
		}
		break;
	default:
		break;
	}

finished:
	if (handle)
		scds_close(&handle);
	if (rs_val_args) {
		if (rs_val_args->common_args) {
			if (rs_val_args->common_args->rs_name)
				free(rs_val_args->common_args->rs_name);
			if (rs_val_args->common_args->rg_name)
				free(rs_val_args->common_args->rg_name);
			free(rs_val_args->common_args);
		}
		if (rs_val_args->boot_args) {
			if (rs_val_args->boot_args->fmri)
				free(rs_val_args->boot_args->fmri);
			free(rs_val_args->boot_args);
		}
		free(rs_val_args);
	}
	if (val_inp_args)
		free(val_inp_args);
	return (return_code);
}
