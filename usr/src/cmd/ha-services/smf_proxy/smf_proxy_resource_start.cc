//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
#pragma ident	"@(#)smf_proxy_resource_start.cc	1.7	09/02/27 SMI"

// This file contains the start method implementation of SMF proxy resource

#include "smf_proxy_common.h"

// Main routine
int
main(int argc, char *argv[]) {

	scds_handle_t handle = NULL;
	int reim_server_fd = -1;
	// Return code.
	int return_code = -1;
	// Container struct for storing the input arguments
	// that are to be sent to the server via the door_call
	smf_proxy_rs_input_args *start_inp_args = {0};
	smf_proxy_rs_boot_start_args *rs_start_args = {0};
	int method_time_out = -1;
	// Init the SCDS handle.
	if (scds_initialize(&handle, argc, argv) != 0) {
		scds_syslog(LOG_ERR, "%s initialization failure", argv[0]);
		return_code = ERROR_GETTING_SCDS_HANDLE;
		goto finished;
	}

	if ((return_code = get_smf_proxy_method_timeout(METHOD_START,  handle,
	    &method_time_out)) != 0) {
		return_code = ERROR_GETTING_METH_TIMEOUT;
		goto finished;
	}

	// Check the SC delegated restarter state and enable if it is disabled.
	if ((return_code = check_and_enable_sc_restarter(
	    CHECK_ENABLE_SC_RESTARTER, method_time_out)) != 0) {
		goto finished;
	}

	// Check the RT type is of SMF_PROXY.
	if (!is_smf_proxy_resource_type(handle)) {
		scds_syslog(LOG_ERR, "Resource type is not of PROXY SMF type");
		return_code = RESOURCE_TYPE_NOT_PROXY_SMF_TYPE;
		goto finished;
	}

	// Allocate memory for common args.
	rs_start_args = (smf_proxy_rs_boot_start_args *)calloc(1,
	    sizeof (smf_proxy_rs_boot_start_args));


	if (rs_start_args == NULL) {
		proxysvc_errmsgnomem();
		return_code = MEMORY_ALLOCATION_FAILED;
		goto finished;
	}

	// Populate the common args R Name and RG Name
	if ((return_code = populate_proxy_common_args(
	    rs_start_args, handle)) != 0) {
		goto finished;
	}

	// Check if svc is already resgistered in Init / Boot method.
	if (get_resource_status_info(
	    reim_server_fd,
	    rs_start_args->common_args->rs_name,
	    rs_start_args->common_args->rg_name,
	    CHECK_PROXY_BOOT_METHOD_SUCCESSFUL)) {
		rs_start_args->boot_args = NULL;
		// Check if SVC is offline.
		if (!get_resource_status_info(
		    reim_server_fd,
		    rs_start_args->common_args->rs_name,
		    rs_start_args->common_args->rg_name,
		    CHECK_PROXIED_SVC_OFFLINE)) {
			return_code = PROXY_SVC_NOT_OFFLINE;
			goto finished;
		}

		start_inp_args = (smf_proxy_rs_input_args *)calloc
		    (1, sizeof (smf_proxy_rs_input_args));

		if (start_inp_args == NULL) {
			proxysvc_errmsgnomem();
			return_code = MEMORY_ALLOCATION_FAILED;
			goto finished;
		}

		// Prepare the container structure
		start_inp_args->method = METHOD_START;
		start_inp_args->data = rs_start_args;

		// open and attach to door server and call the door call
		if (ReIM_clnt_connect(&reim_server_fd)) {
			if (process_door_call(start_inp_args, reim_server_fd)
			    == 0)
				return_code = 0;
			else
				return_code = DOOR_CALL_RETURN_ERROR;
		} else {
			scds_syslog(LOG_ERR, "Could not connect to door "
				    "server");
			return_code = REIM_DOOR_CONNECT_FAILED;
		}

	} else {

		//
		// Boot / Init was not success.
		// Allocate memory for storing the arguments
		// to be passed
		//
		rs_start_args->boot_args=
		    (smf_proxy_rs_boot_extra_args *)calloc(1,
		    sizeof (smf_proxy_rs_boot_extra_args));

		if (rs_start_args->boot_args == NULL) {
			proxysvc_errmsgnomem();
			return_code = MEMORY_ALLOCATION_FAILED;
			goto finished;
		}

		//
		// Populate the Proxied Service instances,
		// retry count and retry interval.
		//
		if ((return_code = populate_proxy_boot_args(
		    rs_start_args, handle)) != 0) {
			goto finished;
		}

		start_inp_args = (smf_proxy_rs_input_args *)calloc
		    (1, sizeof (smf_proxy_rs_input_args));

		if (start_inp_args == NULL) {
			proxysvc_errmsgnomem();
			return_code = MEMORY_ALLOCATION_FAILED;
			goto finished;
		}

		// Prepare the container structure
		start_inp_args->method = METHOD_START;
		start_inp_args->data = rs_start_args;

		// open and attach to door server and call the door call
		if (ReIM_clnt_connect(&reim_server_fd)) {
			if (process_door_call(start_inp_args, reim_server_fd)
			    != 0) {
				return_code = DOOR_CALL_RETURN_ERROR;
				goto finished;
			}
		} else {
			scds_syslog(LOG_ERR, "Could not connect to door "
				    "server");
			return_code = REIM_DOOR_CONNECT_FAILED;
			goto finished;
		}

		// Parse the FMRI file.
		if (process_fmri(rs_start_args->boot_args->fmri,
		    B_FALSE,  METHOD_START, handle) != 0) {
			return_code = FAILURE_IN_PROCESSING_FMRI;
			goto finished;
		} else {
			return_code = 0;
		}
	}
finished :
	// Cleanup.
	if (handle)
		scds_close(&handle);
	if (rs_start_args) {
		if (rs_start_args->common_args) {
			if (rs_start_args->common_args->rs_name)
				free(rs_start_args->common_args->rs_name);
			if (rs_start_args->common_args->rg_name)
				free(rs_start_args->common_args->rg_name);
			free(rs_start_args->common_args);
		}
		if (rs_start_args->boot_args) {
			if (rs_start_args->boot_args->fmri)
				free(rs_start_args->boot_args->fmri);
			free(rs_start_args->boot_args);
		}
		free(rs_start_args);
	}
	if (start_inp_args)
		free(start_inp_args);
	return (return_code);
}
