#!/bin/ksh 
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

#pragma ident	"@(#)parse_file.ksh	1.6	08/09/22 SMI"

#
# This script takes in the smf service xml file and checks if the
# smf service has a restarter. If it has a restarter this default restarter
# is replaced by the sun cluster delegated restarter. This is what is being
# achieved by the "else" block in the script.
# In the "if" part the smf does not have a restarter in
# which case the sun cluster delegated restarter is made the restarter.
#

FILE=$1
#check if the <restarter> pattern exists
if [[ `grep -c "<restarter>" $FILE` -eq 0 ]]
then
	#check if the <dependency pattern exists
	if [[ `grep -c "<dependency" $FILE` -eq 0 ]]
	then
		# if the <dependency pattern does not exist insert the restarter
		# before the <exec_method
		sed -n '1,/<exec_method/p' $FILE  | grep -v "<exec_method" > /tmp/foo
		echo "	<restarter>
		<service_fmri value='svc:/system/cluster/sc_restarter:default' /> 
		</restarter>
		" >> /tmp/foo
	
		sed -n '/<exec_method/,$p' $FILE >> /tmp/foo 
	else
		# if the <dependency pattern exists insert the restarter
		# before the <dependency
		sed -n '1,/<dependency/p' $FILE  | grep -v "<dependency" > /tmp/foo
		echo "	<restarter>
		<service_fmri value='svc:/system/cluster/sc_restarter:default' /> 
		</restarter>
		" >> /tmp/foo
	
		sed -n '/<dependency/,$p' $FILE >> /tmp/foo 
	fi
else
	#if <restarter> pattern does not exist.
	sed -n '1,/<restarter>/p' $FILE >> /tmp/foo
	echo "		<service_fmri value='svc:/system/cluster/sc_restarter:default' />"  >> /tmp/foo
	sed -n '/<\/restarter>/,$p' $FILE >> /tmp/foo 
fi	

mv /tmp/foo $1
