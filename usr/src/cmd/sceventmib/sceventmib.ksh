#! /usr/xpg4/bin/sh -p
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#pragma ident	"@(#)sceventmib.ksh	1.17	08/12/03 SMI"
#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.

#
# This is the configuration script for the event mib module.
# All configuration of the module should be done through
# the use of this script.
#


####################
# Setup Constants  #
####################

typeset -r MODULE_DIR="/etc/cacao/instances/default/modules/"
typeset -r MODULE_PROP="${MODULE_DIR}/agent_snmp_events_mib.properties"
typeset -r SECURITY_DIR="/etc/cacao/instances/default/security"
typeset -r SNMP_DIR="${SECURITY_DIR}/snmp"
typeset -r ACL="${SNMP_DIR}/jdmk.acl"
typeset -r UACL="${SNMP_DIR}/jdmk.uacl"
typeset -r USER_FILE="${SNMP_DIR}/jdmk.security"
typeset -r PASSFILE="${SECURITY_DIR}/password"
typeset -r NSS_DIR="${SECURITY_DIR}/nss/wellknown"

typeset -r CACAOADM="/usr/lib/cacao/bin/cacaoadm"
typeset -r CACAOCSC="/usr/lib/cacao/lib/tools/cacaocsc"
typeset -r CACAO_CMDSTRM_PORT="\
           `${CACAOADM} get-param commandstream-adaptor-port | awk -F= '{print $2}'`"
typeset -r CACAOCSC_ARGS="-h localhost \
                          -p ${CACAO_CMDSTRM_PORT} \
                          -d ${NSS_DIR} \
                          -f ${PASSFILE}"
typeset -r JMX_SET_ATTRIBUTE="com.sun.cacao.jmx:setAttribute"
typeset -r JMX_INVOKE="com.sun.cacao.jmx:invoke"
typeset -r MODULE_MBEAN="com.sun.cluster.agent.event_mib:type=eventmib"
typeset -r RESTART_CACAO="${CACAOADM} restart"
typeset -r PRINT_LIST="/usr/cluster/lib/sc/print-list"

# The default security setting
typeset -r DEFAULT_SECURITY="authPriv"

# Program name and args list
typeset PROG=${0##*/}
typeset -r ARGS=$*

# Temp file
typeset -r TEMP=/tmp/sceventmib.tmp

typeset DO_MSGID=

# Hidden flag (-v flag)
typeset VERBOSE=

# Hidden flag (-x flag)
typeset CLCMD=

# Hidden flag (-X flag)
typeset PRINT_EXIT_STATUS=
typeset -r EXIT_MESSAGE="SCEVENTMIB_EXIT_STATUS: "

# Hidden variable (-z flag)
typeset OPERANDS=

# nss pkg's exist?
typeset NSS=
ls ${NSS_DIR} >/dev/null 2>&1
if [[ $? -eq 0 ]]; then
    NSS=true
fi

# Error return codes
typeset -r CL_NOERR=0
typeset -r CL_EINTERNAL=1
typeset -r CL_EEXISTS=2
typeset -r CL_ENOENT=3
typeset -r CL_EINVAL=4

# Set the path
PATH=/bin:/usr/bin:/sbin:/usr/sbin:${PATH}; export PATH

#####################################################
#
# I18N
#
#####################################################
typeset -x TEXTDOMAIN=SUNW_SC_CMD
typeset -x TEXTDOMAINDIR=/usr/cluster/lib/locale

/usr/cluster/lib/sc/sc_zonescheck
if [ $? -ne 0 ]; then
	exit 1
fi

####################################################
#
# print_usage
#
#   Print usage message to stderr
####################################################
print_usage()
{
    printf "$(gettext 'usage'):\n" >&2
    echo >&2
    printf "$(gettext '   %s -a -h <host> -c <community>')\n" ${PROG} >&2
    printf "$(gettext 'or %s -r -h <host> -c <community>')\n" ${PROG} >&2
    printf "$(gettext 'or %s -a -u <username> -t <auth_type> [-f <pass_file>]')\n" ${PROG} >&2
    printf "$(gettext 'or %s -m -u <username> -t <auth_type>')\n" ${PROG} >&2
    printf "$(gettext 'or %s -d -u <username> -s <security>')\n" ${PROG} >&2
    printf "$(gettext 'or %s -r -u <username>')\n" ${PROG} >&2
    printf "$(gettext 'or %s -l <protocol>')\n" ${PROG} >&2
    printf "$(gettext 'or %s -p %s')\n" ${PROG} "{all | hosts | users}" >&2
	# no I18n of keywords
    printf "$(gettext 'or %s {-e | -n}')\n" ${PROG} >&2
    echo >&2

    return 0
}


####################################################
#
# enable
#
#   Enable the module
####################################################
enable()
{
    ensureFile ${MODULE_PROP}

    #  Set state in file
    sed /^\#/\!s/eventmib.module.state=.*/eventmib.module.state=Enabled/ ${MODULE_PROP} > ${TEMP}
    cat ${TEMP} > ${MODULE_PROP}
    rm -f ${TEMP}

    # Set state in active module
    if [[ -z ${NSS} ]]; then
        ${RESTART_CACAO}
    else
        ${CACAOCSC} ${CACAOCSC_ARGS} -c "${JMX_SET_ATTRIBUTE} ${MODULE_MBEAN} ModuleEnabled true" 2> /dev/null
	status=$?
        if [[ status -ne 0 ]];  then
            printf "$(gettext 'Error while enabling Event MIB.')\n" | printerr
            exit ${CL_EINTERNAL}
        fi
    fi

    exit ${CL_NOERR}
}

####################################################
#
# disable
#
#   Disable the module
####################################################
disable()
{
    ensureFile ${MODULE_PROP}

    # Set state in file
    sed /^\#/\!s/eventmib.module.state=.*/eventmib.module.state=Disabled/ ${MODULE_PROP} > ${TEMP}
    cat ${TEMP} > ${MODULE_PROP}
    rm -f ${TEMP}

    # Set state in active module
    if [[ -z ${NSS} ]]; then
        ${RESTART_CACAO}
    else
        ${CACAOCSC} ${CACAOCSC_ARGS} -c "${JMX_SET_ATTRIBUTE} ${MODULE_MBEAN} ModuleEnabled false" 2> /dev/null
	status=$?
        if [[ status -ne 0 ]];  then
            printf "$(gettext 'Error while disabling Event MIB.')\n" | printerr
            exit ${CL_EINTERNAL}
        fi
    fi

    exit ${CL_NOERR}
}

####################################################
#
# set_protocol <version>
#
#   Set the protocol to version specified, 2 or 3
####################################################
set_protocol()
{
    typeset protocol=$1

    ensureFile ${MODULE_PROP}

    # Set protocol in file
    sed /^\#/\!s/snmp.version=.*/snmp.version=${protocol}/ ${MODULE_PROP} > ${TEMP}
    cat ${TEMP} > ${MODULE_PROP}
    rm -f ${TEMP}

    # Set protocol in active module
    if [[ -z ${NSS} ]]; then
        ${RESTART_CACAO}
    else
        ${CACAOCSC} ${CACAOCSC_ARGS} -c "${JMX_SET_ATTRIBUTE} ${MODULE_MBEAN} ModuleSNMPVersion ${protocol}" 2> /dev/null
	status=$?
        if [[ status -ne 0 ]];  then
            printf "$(gettext 'Error while changing Event MIB protocol.')\n" | printerr
            exit ${CL_EINTERNAL}
        fi
    fi

    exit ${CL_NOERR}
}

####################################################
#
# add_host <host> <community>
#
#   Add a host to the configuration with the 
#   given hostname and community
####################################################
add_host()
{
    typeset host=$1
    typeset community=$2

    ensureFile ${ACL}

    # First check for a trap definition in the jdmk.acl file
    sed -e '/^#/d' ${ACL} | grep "^trap" >/dev/null 2>&1
    exit_status=$?
    if [[ exit_status -eq 0 ]]; then

        # Trap section exists, look for community
        sed -e '/^#/d' ${ACL} | grep "trap-community" | grep " ${community}$" >/dev/null 2>&1
        exit_status=$?
        if [[ exit_status -ne 0 ]]; then
            # Community does not exist, create it and add the
            # host to the list
            sed -e "/^trap = {/a\\
 {\\
 trap-community = ${community}\\
 hosts = ${host}\\
 }" ${ACL} > ${TEMP}
            cat ${TEMP} > ${ACL}
            rm ${TEMP}
        else
            # Community does exist, now check for host
            # If host already listed, do nothing.
            sed -e '/^#/d' ${ACL} | sed -n -e "/^ trap-community.*${community}$/,/}/p" | grep -w "${host}" >/dev/null 2>&1
            exit_status=$?
            if [[ exit_status -ne 0 ]]; then
                # Host not specified, add it to the list
                sed -e "/^ trap-community.*${community}$/,/}/s/hosts.*/&, ${host}/" ${ACL} > ${TEMP}
                cat ${TEMP} > ${ACL}
                rm ${TEMP}
	    else
                printf "$(gettext 'Host \"%s\" already exists.')\n" ${host} | printerr
                exit ${CL_EEXISTS}
            fi
        fi
    else
        # Trap section does not exist, create it and add
        # this host to the list

        echo >> ${ACL}
        echo "# Following lines added by Sun Cluster" >> ${ACL}
        echo "trap = {" >> ${ACL}
        echo " {" >> ${ACL}
        echo " trap-community = ${community}" >> ${ACL}
        echo " hosts = ${host}" >> ${ACL}
        echo " }" >> ${ACL}
        echo "}" >> ${ACL}  
    fi

    # Tell the SNMPAdaptorServer to re-read its config
    SNMPAdaptorServer_sigHUP

    exit ${CL_NOERR}
}

####################################################
#
# remove_host <host> <community>
#
#   Remove a host from the configuration with the 
#   given hostname and community
####################################################
remove_host()
{
    typeset host=$1
    typeset community=$2

    ensureFile ${ACL}

    # First check for a trap definition in the jdmk.acl file
    sed -e '/^#/d' ${ACL} | grep "^trap" >/dev/null 2>&1
    exit_status=$?
    if [[ exit_status -eq 0 ]]; then
        # Trap section exists, look for community
        sed -e '/^#/d' ${ACL} | grep "trap-community" | grep " ${community}$" >/dev/null 2>&1
        exit_status=$?
        if [[ exit_status -eq 0 ]]; then
            # Community exists, now look for host
            sed -e '/^#/d' ${ACL} | sed -n -e "/^ trap-community.*${community}$/,/}/p" | grep -w "${host}" >/dev/null 2>&1
            exit_status=$?
            if [[ exit_status -eq 0 ]]; then
                # Host exists, now remove it
		let line=`grep -n "^ trap-community.*${community}$" ${ACL} | awk -F: '{ print $1 }'`
		let x=line+1
		sed -e "${x}s/, ${host}$//" ${ACL} > ${TEMP}
		sed -e "${x}s/, ${host},/,/" ${TEMP} > ${ACL}
		sed -e "${x}s/ hosts = ${host},/ hosts =/" ${ACL} > ${TEMP}
		sed -e "${x}s/ hosts = ${host}$/ hosts =/" ${TEMP} > ${ACL}
                rm ${TEMP}

                # Now, check if community is empty, if so, delete it
                sed -n -e "/^ trap-community.*${community}$/,/ }/p" ${ACL} | grep hosts | awk -F= '{ print $2 }' | grep '[a-z0-9A-Z]' >/dev/null 2>&1
                exit_status=$?
                if [[ exit_status -ne 0 ]]; then
                    # No more hosts left in community, so delete it
                    let x=line-1
                    let y=line+2
                    sed ${x},${y}d ${ACL} > ${TEMP}     
                    cat ${TEMP} > ${ACL}
                    rm ${TEMP}

                    # Now check for any remaining communities, if none,
                    # remove acl section altogether
                    grep '^ trap-community' ${ACL} >/dev/null 2>&1
                    exit_status=$?
                    if [[ exit_status -ne 0 ]]; then
                        # No more communities, remove acl def'n
                        sed -e '/^# Following lines added by Sun Cluster/,/&/d' ${ACL} > ${TEMP}
                        cat ${TEMP} > ${ACL}
                        rm ${TEMP}
                    fi
                fi
            else
		if [[ -z ${CLCMD} ]]; then
                	printf "$(gettext '%s: Host \"%s\" not found in community \"%s\"')\n" ${PROG} ${host} ${community}  >&2
		fi
                exit ${CL_ENOENT}
            fi
        else
	    if [[ -z ${CLCMD} ]]; then
               	printf "$(gettext '%s: Host \"%s\" not found in community \"%s\"')\n" ${PROG} ${host} ${community}  >&2
	    fi
            exit ${CL_ENOENT}
        fi
    else
	if [[ -z ${CLCMD} ]]; then
               	printf "$(gettext '%s: Host \"%s\" not found in community \"%s\"')\n" ${PROG} ${host} ${community}  >&2
	fi
	exit ${CL_ENOENT}
    fi

    # Tell the SNMPAdaptorServer to re-read its config
    SNMPAdaptorServer_sigHUP

    exit ${CL_NOERR}
}

####################################################
#
# add_user <username> <auth> <password>
#
#   Add a user to the configuration with the 
#   given username, authentication type, and password
#
#   All users will be added automatically to each 
#   security level
####################################################
add_user()
{
    typeset user=$1
    typeset auth=$2
    typeset passfile=$3
    typeset password=

    # format the auth
    if [[ ${auth} = "MD5" ]]; then 
        auth="usmHMACMD5AuthProtocol"
    elif [[ ${auth} = "SHA" ]]; then
        auth="usmHMACSHAAuthProtocol"
    fi

    # check for password
    if [[ -z ${passfile} ]]; then
        # Get password from command line
	printf "$(gettext 'Enter password for user %s: ')\c" ${user}
        stty -echo
	trap 'stty echo; exit 1' INT TERM
        read password
	trap - INT TERM
        stty echo
        echo ""
        validatePassword ${password}
    else
        # Get password from file
	if [[ ! -f ${passfile} ]]; then
		printf "$(gettext 'Unable to read password file \"%s\".')\n" ${passfile} | printerr
		exit ${CL_EINVAL}
	fi
        password=`grep -w ${user} ${passfile} | awk -F: '{ print $2 }'`
	if [[ -z ${password} ]]; then
	    printf "$(gettext 'Cannot find password for user \"%s\".')\n" ${user} | printerr
	    exit ${CL_EINVAL}
	fi
        validatePassword ${password}
    fi
    
    ensureFile ${UACL}
    ensureFile ${USER_FILE}

    # Check if user exists in jdmk.security file
    sed -n -e "/^userEntry=localEngineID,/p" ${USER_FILE} | grep ",${user}," >/dev/null 2>&1
    exit_status=$?
    if [[ exit_status -eq 0 ]]; then
	printf "$(gettext 'User \"%s\" already exists.')\n" ${user} | printerr
	exit ${CL_EEXISTS}
    else
        # User doesn't exist, add them to the conf files
        # First create the user in the security file
        echo "\nuserEntry=localEngineID,${user},null,${auth},${password},usmDESPrivProtocol,${password},3" >> ${USER_FILE}
    
        # Second add the user to the UACL file
        # Check for existing ACL section
        sed -e '/^#/d' ${UACL} | grep '^acl = {' >/dev/null 2>&1
        exit_status=$?
        if [[ exit_status -eq 0 ]]; then
            # acl section exists, add the user
            sed -e "s/users.*/&, ${user}/" ${UACL} > ${TEMP}
            cat ${TEMP} > ${UACL}
            rm ${TEMP}
        else
            # acl section does not exist, create it  
            echo "# The following lines added by Sun Cluster" >> ${UACL}
            echo "acl = {" >> ${UACL}
            echo " {" >> ${UACL}
            echo " context-names = null" >> ${UACL}
            echo " access = read-only" >> ${UACL}
            echo " security-level = authPriv" >> ${UACL}
            echo " users = ${user}" >> ${UACL}
            echo " }" >> ${UACL}
            echo " {" >> ${UACL}
            echo " context-names = null" >> ${UACL}
            echo " access = read-only" >> ${UACL}
            echo " security-level = authNoPriv" >> ${UACL}
            echo " users = ${user}" >> ${UACL}
            echo " }" >> ${UACL}
            echo " {" >> ${UACL}
            echo " context-names = null" >> ${UACL}
            echo " access = read-only" >> ${UACL}
            echo " security-level = noAuthNoPriv" >> ${UACL}
            echo " users = ${user}" >> ${UACL}
            echo " }" >> ${UACL}
            echo "}" >> ${UACL}
        fi
    fi

    # Assuming all conf is done through here, should be able to assume that if
    # there is not a default value set yet in the conf file then there are no
    # users and this one should be set as the default.
    typeset NO_RESTART=
    defaultUser=`sed -n -e 's/^eventmib.default.user=//p' ${MODULE_PROP}` 
    if [[ -z ${defaultUser} ]]; then

	security=${DEFAULT_SECURITY}

        sed -e "s/^eventmib.default.user=.*/eventmib.default.user=${user}/" -e "s/^eventmib.default.security=.*/eventmib.default.security=${security}/" ${MODULE_PROP} > ${TEMP}
        cat ${TEMP} > ${MODULE_PROP}
        rm ${TEMP}

        if [[ -z ${NSS} ]]; then
            ${RESTART_CACAO}
	    NO_RESTART=true
        else
            # Set the defaults in active module
            ${CACAOCSC} ${CACAOCSC_ARGS} -c "${JMX_SET_ATTRIBUTE} ${MODULE_MBEAN} DefaultUser ${user}"
            ${CACAOCSC} ${CACAOCSC_ARGS} -c "${JMX_SET_ATTRIBUTE} ${MODULE_MBEAN} DefaultSecurity ${security}"
        fi
    fi

    # Tell the SNMPAdaptorServer to re-read its config
    SNMPAdaptorServer_sigHUP ${NO_RESTART}

    exit ${CL_NOERR}
}

####################################################
#
# remove_user <username>
#
#   Removes a user with the given username
#   If this user was the default, then another user is
#   chosen to be the new default user.
####################################################
remove_user()
{
    typeset user=$1

    ensureFile ${UACL}
    ensureFile ${USER_FILE}

    # Check user file for user, if not there, then exit
    grep -v '^#' ${USER_FILE} | grep ",${user}," >/dev/null 2>&1
    exit_status=$?
    if [[ exit_status -ne 0 ]]; then
	if [[ -z ${CLCMD} ]]; then 
	    printf "$(gettext 'User \"%s\" does not exist.')\n" ${user} | printerr
	fi
	exit ${CL_ENOENT}
    fi

    # First check for an acl definition in the UACL file
    sed -e '/^#/d' ${UACL} | grep '^acl = {' >/dev/null 2>&1
    exit_status=$?
    if [[ exit_status -eq 0 ]]; then
        # acl section exists, check for user
        sed -n -e '/^#/d' -e '/^ users = /p' ${UACL} | grep -w "${user}" >/dev/null 2>&1
        exit_status=$?
        if [[ exit_status -eq 0 ]]; then
            # User exists, now remove them from each security section
	    for sec in authPriv authNoPriv noAuthNoPriv
	    do
	        let line=`grep -n "^ security-level.*${sec}$" ${UACL} | awk -F: '{ print $1 }'`
                let x=line+1
	        sed -e "${x}s/, ${user}$//" ${UACL} > ${TEMP}
	        sed -e "${x}s/, ${user},/,/" ${TEMP} > ${UACL}
	        sed -e "${x}s/ users = ${user},/ users =/" ${UACL} > ${TEMP}
	        sed -e "${x}s/ users = ${user}$/ users =/" ${TEMP} > ${UACL}
	    done
            rm ${TEMP}

            # Now check to see if other users exist
            # If none, then it is safe to remove entire acl
            users=`grep '^ users = ' ${UACL}  | awk -F= '{ print $2 }'`
            if [[ -z ${users} ]]; then
                   # No more users, remove entire acl def'n
                   sed -e '/^# The following lines added by Sun Cluster/,/&/d' ${UACL} > ${TEMP}
                   cat ${TEMP} > ${UACL}
                   rm ${TEMP}
            fi
            
            # User doesn't exist anymore, remove from USER_FILE
            sed "/^userEntry=localEngineID,${user},/d" ${USER_FILE} > ${TEMP}
            cat ${TEMP} > ${USER_FILE}
            rm ${TEMP}
        else
   	    if [[ -z ${CLCMD} ]]; then
	        printf "$(gettext 'User \"%s\" does not exist.')\n" ${user} | printerr
	    fi
            exit ${CL_ENOENT}
        fi
    else
        # acl section doesn't even exist, error out.
	if [[ -z ${CLCMD} ]]; then
	    printf "$(gettext 'User \"%s\" does not exist.')\n" ${user} | printerr
	fi
        exit ${CL_ENOENT}
    fi

    # Check if this user was the default, if so then set a different one.
    typeset NO_RESTART=
    grep "^eventmib.default.user=${user}$" ${MODULE_PROP} >/dev/null 2>&1
    exit_status=$?
    if [[ exit_status -eq 0 ]]; then
        # OK, the default user was removed, now set another as the default
        # It should be safe to assume that any user listed in the UACL file
        # is a valid user, so just use the first one we find.

        # copy the first security section to the temp file
        sed -e '/^#/d' ${UACL} | sed -n -e "/^ security-level/,/^ users/p" | sed -n -e "1,2p" > ${TEMP}

        # retrieve values
        defaultSecurity=${DEFAULT_SECURITY}
        defaultUser=`sed -n -e "2p" ${TEMP} | awk -F= '{print $2}' | awk -F, '{print $1}' | sed -e 's/ *//'`
            
        # Put these values in the props file
        sed -e "s/^eventmib.default.user=.*/eventmib.default.user=${defaultUser}/" -e "s/^eventmib.default.security=.*/eventmib.default.security=${defaultSecurity}/" ${MODULE_PROP} > ${TEMP}
        cat ${TEMP} > ${MODULE_PROP}
        rm ${TEMP}

        if [[ -z ${NSS} ]]; then
            ${RESTART_CACAO}
	    NO_RESTART=true
        else
            # Set the defaults in active module
            ${CACAOCSC} ${CACAOCSC_ARGS} -c "${JMX_SET_ATTRIBUTE} ${MODULE_MBEAN} DefaultUser \"${defaultUser}\""
            ${CACAOCSC} ${CACAOCSC_ARGS} -c "${JMX_SET_ATTRIBUTE} ${MODULE_MBEAN} DefaultSecurity \"${defaultSecurity}\""
        fi
    fi
    
    # Tell the SNMPAdaptorServer to re-read its config
    SNMPAdaptorServer_sigHUP ${NO_RESTART}

    exit ${CL_NOERR}
}

####################################################
#
# print_config 
#
#   Prints the configuration of the module
####################################################
print_config()
{
    printf "$(gettext 'SNMP Event MIB Module Configuration')\n"
    echo ""

    # Mode
    state=`sed -n -e '/^eventmib.module.state=/p' ${MODULE_PROP} | awk -F= '{ print $2 }'`
    printf "$(gettext 'Module State:  %s')\n" ${state}
    
    # SNMP Protocol
    protocol=`sed -n -e '/^snmp.version=/p' ${MODULE_PROP} | awk -F= '{ print $2 }'`
    printf "$(gettext 'Protocol:  SNMPv%s')\n" ${protocol}
    echo ""
    print_hosts
    print_users
}

####################################################
#
# print_mib_list 
#
#   Prints the MIB configuration.  Used by the
#   clsnmpmib list command to print verbose listing
####################################################
print_mib_list()
{
    # Mode
    state=`sed -n -e '/^eventmib.module.state=/p' ${MODULE_PROP} | awk -F= '{ print $2 }'`

    # Protocol
    protocol=`sed -n -e '/^snmp.version=/p' ${MODULE_PROP} | awk -F= '{ print $2 }'`

    name_heading=$(gettext 'MIB Name')
    state_heading=$(gettext 'State')
    proto_heading=$(gettext 'Protocol')
    
    # Echo values to temp file
    echo "${name_heading}\t${state_heading}\t${proto_heading}" > ${TEMP}
    echo "Event\t${state}\tSNMPv${protocol}" >> ${TEMP}
    echo "" >> ${TEMP}

    # Print a heading so we know what node it is on
    node=`uname -n`
    echo ""
    printf "$(gettext '--- SNMP MIBs on node %s ---')\n" ${node}
    echo ""

    ${PRINT_LIST} true < ${TEMP}
    error=$?

    rm -f ${TEMP}

    if [[ ! -z ${PRINT_EXIT_STATUS} ]]; then
	echo ${EXIT_MESSAGE}${error}
    fi
    exit ${error}
}

####################################################
#
# print_hosts
#
#   Prints all the hosts configured for this module
####################################################
print_hosts()
{
    printf "$(gettext 'Hosts set to receive SNMP traps')\n"
    echo ""
    printf "$(gettext '   Hostname                     Community')\n"
    echo "-------------------------------------------------------"

    # communities
    communities=`sed -n -e '/^ trap-community/p' ${ACL} | awk '{print $3}'`

    for community in ${communities}
    do
        # community subsection
        hosts=`sed -e '/^#/d' ${ACL} | sed -n -e "/^ trap-community.*${community}$/,/}/p" | sed -n -e '2p'`
        hostlist=`echo ${hosts} | awk -F= '{ print $2 }' | awk -F, ' { for (i=1; i <= NF; ++i) print $i }'`
        for host in ${hostlist}
        do
            let GAP=30
            SP=" "
            BREAK=""
            hlength=`echo ${host} | wc -m`
            difference=GAP-hlength
            while [[ difference -ne 0 ]]
            do
                BREAK=${BREAK}${SP}
                (( difference-= 1 ))
            done

            echo "   ${host}${BREAK}${community}"
        done
    done

    echo "-------------------------------------------------------"
    echo ""
}

####################################################
#
# print_host_list
#
#   Prints all the hosts configured in the system.
#   Only used by the clsnmphost command.
####################################################
print_host_list()
{
    name_heading=$(gettext 'Host Name')
    comm_heading=$(gettext 'Community')

    communities=`echo $1 | awk -F, '{ for (i = 1; i <= NF; i++) print $i }'`
    
    rm -f ${TEMP}
    rm -f ${TEMP}.$$
    # echo Headings to temp file, only if verbose
    if [[ ! -z ${VERBOSE} ]]; then
    	echo "${name_heading}\t${comm_heading}" > ${TEMP}
    fi

    # add the hosts
    print_hosts | sed -n -e "/^-.*-$/,/^-.*-$/p" | sed -e '1d' -e '$d' | awk '{ print $1"\t"$2 }' | sort >> ${TEMP}

    if [[ ! -z ${communities} ]]; then

	touch ${TEMP}.$$
	# Communities must be filtered
	if [[ ! -z ${VERBOSE} ]]; then
		sed -n -e 1p ${TEMP} > ${TEMP}.$$
	fi

	for comm in ${communities}
	do
		awk '{ print $2 }' ${TEMP} | grep -w ${comm} >/dev/null 2>&1
		if [[ $? -eq 0 ]]; then
			for line in `awk '{ print $2 }' ${TEMP} | grep -n -w ${comm} | awk -F: '{ print $1 }'`
			do
				sed -n -e ${line}p ${TEMP} >> ${TEMP}.$$
			done
		else
			printf "$(gettext 'Community \"%s\" not found.')\n" ${comm} | printerr
			error=${CL_EINVAL}
		fi
	done
	cat ${TEMP}.$$ > ${TEMP}
	rm -f ${TEMP}.$$
    fi

    if [[ ! -z ${OPERANDS} ]]; then

	touch ${TEMP}.$$
	# Operands must be filtered
	operands=`echo ${OPERANDS} | awk -F, '{ for (i = 1; i <= NF; i++) print $i }'`
	if [[ ! -z ${VERBOSE} ]]; then
		sed -n -e 1p ${TEMP} > ${TEMP}.$$
	fi
	for oper in ${operands}
	do
		awk '{ print $1 }' ${TEMP} | grep -w ${oper} >/dev/null 2>&1
		if [[ $? -eq 0 ]]; then
			for line in `awk '{ print $1 }' ${TEMP} | grep -n -w ${oper} | awk -F: '{ print $1 }'`
			do
				sed -n -e ${line}p ${TEMP} >> ${TEMP}.$$
			done
		else
			printf "$(gettext 'Host \"%s\" not found.')\n" ${oper} | printerr
			error=${CL_ENOENT}
		fi
	done
	cat ${TEMP}.$$ > ${TEMP}
	rm -f ${TEMP}.$$
    fi
    echo "" >> ${TEMP}

    # Print, verbose or standard
    verbose=false
    if [[ ! -z ${VERBOSE} ]]; then
        verbose=true

        # Print a heading so we know what node it is on
        node=`uname -n`
        echo ""
        printf "$(gettext '--- SNMP hosts on node %s ---')\n" ${node}
        echo ""
    fi
    ${PRINT_LIST} ${verbose} < ${TEMP}
    list_err=$?

    if [[ -z ${error} ]]; then
    	error=${list_err}
    fi

    rm -f ${TEMP}

    if [[ ! -z ${PRINT_EXIT_STATUS} ]]; then
	echo ${EXIT_MESSAGE}${error}
    fi
    exit ${error}
}

####################################################
#
# modify_user <user> <auth>
#
#   Modifies a user
####################################################
modify_user()
{
	typeset user=$1
	typeset auth=$2

	# First verify the user exists
	grep -w ${user} ${USER_FILE} >/dev/null 2>&1
	if [[ $? -ne 0 ]]; then
		printf "$(gettext 'User \"%s\" does not exist.')\n" ${user} | printerr
		exit ${CL_ENOENT}
	fi

	# Check the auth setting of the user
	if [[ ! -z ${auth} ]]; then
		# format the auth
   		if [[ ${auth} = "MD5" ]]; then
       			auth="usmHMACMD5AuthProtocol"
    		elif [[ ${auth} = "SHA" ]]; then
       			auth="usmHMACSHAAuthProtocol"
    		fi	    

		# First get the line number of the users entry
		let line=`grep -n -w ${user} ${USER_FILE} | awk -F: '{ print $1 }'`

		# replace either auth value with the new auth value
		sed -e "${line}s/usmHMACMD5AuthProtocol/${auth}/" -e "${line}s/usmHMACSHAAuthProtocol/${auth}/" ${USER_FILE} > ${TEMP}		
		cat ${TEMP} > ${USER_FILE}
		rm -f ${TEMP}
	fi

	# Tell the SNMPAdaptorServer to re-read its config
	SNMPAdaptorServer_sigHUP

	exit ${CL_NOERR}
}

####################################################
#
# set_default_user <user> <security>
#
#   Modifies a user
####################################################
set_default_user()
{
	typeset user=$1
	typeset security=$2
	
	ensureFile ${USER_FILE}

	# Check user file for SNMP user, if not there, then exit.
	grep -v '^#' ${USER_FILE} | grep ",${user}," >/dev/null 2>&1
	exit_status=$?
	if [[ exit_status -ne 0 ]]; then
		printf "$(gettext 'SNMP User \"%s\" does not exist.')\n" ${user} | printerr
		exit ${CL_ENOENT}
	fi
		
	# Set this user as the default
	sed -e "s/^eventmib.default.user=.*/eventmib.default.user=${user}/" -e "s/^eventmib.default.security=.*/eventmib.default.security=${security}/" ${MODULE_PROP} > ${TEMP}
        cat ${TEMP} > ${MODULE_PROP}
        rm -f ${TEMP}

	if [[ -z ${NSS} ]]; then
		${RESTART_CACAO}
		NO_RESTART=true
	else
		# Set the defaults in active module
		${CACAOCSC} ${CACAOCSC_ARGS} -c "${JMX_SET_ATTRIBUTE} ${MODULE_MBEAN} DefaultUser ${user}"
		${CACAOCSC} ${CACAOCSC_ARGS} -c "${JMX_SET_ATTRIBUTE} ${MODULE_MBEAN} DefaultSecurity ${security}" 
	fi

	# Tell the SNMPAdaptorServer to re-read its config
	SNMPAdaptorServer_sigHUP ${NO_RESTART}

	exit ${CL_NOERR}
}

####################################################
#
# print_users
#
#   Prints all the users configured for this module
####################################################
print_users()
{

    ensureFile ${UACL}
    ensureFile ${USER_FILE}

    printf "$(gettext 'Users authorized to access MIB (SNMPv3 only)')\n"
    echo ""
    printf "$(gettext '   Username                     Auth. Protocol')\n"
    echo "-------------------------------------------------------"

    # users
    users=`sed -n -e '/^userEntry=/p' ${USER_FILE} | awk -F= '{ print $2 }' | awk -F, '{ print $2 }'`

    # Defaults
    defaultUser=`sed -n -e '/^eventmib.default.user=/p' ${MODULE_PROP} | awk -F= '{print $2}'`
    defaultSecurity=`sed -n -e '/^eventmib.default.security=/p' ${MODULE_PROP} | awk -F= '{print $2}'`

    for user in ${users}
    do
	# First check for an acl definition in the UACL file
	sed -e '/^#/d' ${UACL} | grep '^acl = {' >/dev/null 2>&1
	exit_status=$?
	if [[ exit_status -eq 0 ]]; then
		# acl section exists, check for user
		sed -n -e '/^#/d' -e '/^ users = /p' ${UACL} | grep -w "${user}" >/dev/null 2>&1
		exit_status=$?
		if [[ exit_status -eq 0 ]]; then
        		auth=""
        		tempauth=`sed -n -e "/^userEntry=localEngineID,${user},/p" ${USER_FILE} | awk -F= '{ print $2 }' | awk -F, '{ print $4 }'`
        		if [[ ${tempauth} = "usmHMACMD5AuthProtocol" ]]; then
            			auth="MD5"
        		else
            			auth="SHA"
        		fi
			tempauth=""

			if [[ ${user} = ${defaultUser} ]]; then
	    			slength=`echo ${defaultSecurity} | wc -m`
	    			(( slength+= 3 ))
            			let GAP=30-slength
			else
	    			let GAP=30
			fi
        		SP=" "
        		BR=""
        		ulength=`echo ${user} | wc -m`
        		difference=GAP-ulength
        		while [[ difference -ne 0 ]]
        		do
            			BR=${BR}${SP}
            			(( difference-= 1 ))
        		done

		        # Check for default
			if [[ ${user} = ${defaultUser} ]]; then
	    			echo " * ${user} (${defaultSecurity})${BR} ${auth}"
			else
            			echo "   ${user}${BR}${auth}"
			fi
		fi
	fi
    done

    echo "-------------------------------------------------------"
    printf "$(gettext '* () Denotes default user and security level')\n"
}

####################################################
#
# print_user_list() seclevels auths default
#
#   Used by clsnmpuser list
####################################################
print_user_list()
{
    name_head=$(gettext 'User Name')
    auth_head=$(gettext 'Authentication Protocol')

    auths=`echo $1 | awk -F= '{ print $2 }' | awk -F, '{ for (i = 1; i <= NF; i++) print $i }'`
    default=`echo $2 | awk -F= '{ print $2 }'`

    rm -f ${TEMP}

    # Echo headings to temp file, only if verbose
    if [[ ! -z ${VERBOSE} ]]; then
	echo "${name_head}\t${auth_head}" > ${TEMP}
    fi

    error=0

    # If default, then only print that line
    if [[ ! -z ${default} ]]; then

        defaultUser=`sed -n -e '/^eventmib.default.user=/p' ${MODULE_PROP} | awk -F= '{print $2}'`
	auth=""
        tempauth=`sed -n -e "/^userEntry=localEngineID,${defaultUser},/p" ${USER_FILE} | awk -F= '{ print $2 }' | awk -F, '{ print $4 }'`
        if [[ ${tempauth} = "usmHMACMD5AuthProtocol" ]]; then
            auth="MD5"
        else
            auth="SHA"
	fi

	echo "${defaultUser}	${auth}"  >> ${TEMP}.$$
	echo "" >> ${TEMP}.$$
	cat ${TEMP}.$$ >> ${TEMP}

	verbose=false
	if [[ ! -z ${VERBOSE} ]]; then
		verbose=true

        	# Print a heading so we know what node it is on
        	node=`uname -n`
        	echo ""
        	printf "$(gettext '--- SNMP users on node %s ---')\n" ${node}
        	echo ""
	fi
	
	${PRINT_LIST} ${verbose} < ${TEMP}
	error=$?

	rm -f ${TEMP}
	rm -f ${TEMP}.$$

        if [[ ! -z ${PRINT_EXIT_STATUS} ]]; then
	    echo ${EXIT_MESSAGE}${error}
        fi
	exit ${error}
    fi

    # Get the default user
    print_users | sed -n -e "/^-.*$/,/^-.*-$/p" | sed -e '1d' -e '$d' | grep '*' | awk '{ print $2"\t"$4 }' >> ${TEMP}.$$
    # Get the other users
    print_users | sed -n -e "/^-.*$/,/^-.*-$/p" | sed -e '1d' -e '$d' | grep -v '*' | awk '{ print $1"\t"$2 }' >> ${TEMP}.$$

    if [[ ! -z ${auths} ]]; then
	# Auth levels must be filtered
	touch ${TEMP}.$$.2
	for auth in ${auths}
	do
		awk '{ print $2 }' ${TEMP}.$$ | grep -w ${auth} >/dev/null 2>&1
		if [[ $? -eq 0 ]]; then
			for line in `awk '{ print $2 }' ${TEMP}.$$ | grep -n -w ${auth} | awk -F: '{ print $1 }'`
                        do
                                sed -n -e ${line}p ${TEMP}.$$ >> ${TEMP}.$$.2
                        done
		fi
	done
	cat ${TEMP}.$$.2 > ${TEMP}.$$
	rm -f ${TEMP}.$$.2
    fi

    if [[ ! -z ${OPERANDS} ]]; then
	# Operands must be filtered
	touch ${TEMP}.$$.2
	operands=`echo ${OPERANDS} | awk -F, '{ for (i = 1; i <= NF; i++) print $i }'`
	sed_string="sed "
	for oper in ${operands}
	do
		awk '{ print $1 }' ${TEMP}.$$ | grep -w ${oper} >/dev/null 2>&1
		if [[ $? -eq 0 ]]; then
			for line in `awk '{ print $1 }' ${TEMP}.$$ | grep -n -w ${oper} | awk -F: '{ print $1 }'`
                        do
                                sed -n -e ${line}p ${TEMP}.$$ >> ${TEMP}.$$.2
                        done
		else
			printf "$(gettext 'User \"%s\" not found.')\n" ${oper} | printerr
			error=${CL_ENOENT}
		fi
	done
	cat ${TEMP}.$$.2 > ${TEMP}.$$
	rm -f ${TEMP}.$$.2
    fi

    cat ${TEMP}.$$ | sort >> ${TEMP}

    verbose=false
    if [[ ! -z ${VERBOSE} ]]; then
	verbose=true

       	# Print a heading so we know what node it is on
       	node=`uname -n`
       	echo ""
       	printf "$(gettext '--- SNMP users on node %s ---')\n" ${node}
       	echo ""
    fi
	
    ${PRINT_LIST} ${verbose} < ${TEMP}
    print_err=$?

    if [[ -z ${error} ]]; then
	error=${print_err}
    fi

    rm -f ${TEMP}
    rm -f ${TEMP}.$$

    if [[ ! -z ${PRINT_EXIT_STATUS} ]]; then
	echo ${EXIT_MESSAGE}${error}
    fi
    exit ${error}
}

####################################################
#
# validateWord() aWord type
#
#   Ensures that aWord does not contain invalid 
#   characters
####################################################
validateWord() 
{
    typeset aWord=$1
    typeset type=$2

    # If the word has any bad chars in it, then it is not valid
    # NOTE:  If changing the valid characters, be sure to make the 
    # corresponding SPM code changes as well so that they are in sync.
    echo ${aWord} | grep '[+{};:"\\=?~()<>&*|,$! ]' >/dev/null 2>&1
    exit_status=$?
    if [[ exit_status -eq 0 ]]; then
        # aWord has a bad char, exit
	printf "$(gettext 'Invalid %s: %s')\n" ${type} ${aWord} | printerr
        exit ${CL_EINVAL}
    fi
}


####################################################
#
# validateHost() host
#   Ensures that the given hostname is valid
#
####################################################
validateHost()
{

    typeset host=$1

    # There are 3 possibilities: regular IP address, IPv6 or hostname
    # The simplest is to determine if there are any characters that are
    # no good for any scenario
    echo ${host} | grep '[+{};"\\=?~()<>&*|$,! ]' >/dev/null 2>&1
    exit_status=$?
    if [[ exit_status -eq 0 ]]; then
        # This is not valid, no matter the intended format
	printf "$(gettext 'Invalid host \"%s\".')\n" ${host} | printerr
        exit ${CL_EINVAL}
    fi

    # From here, the easiest to determine is an IPv6 address
    echo ${host} | grep ':' >/dev/null 2>&1
    exit_status=$?
    if [[ exit_status -eq 0 ]]; then 
        # This should be an IPv6 address, there are several ways to write 
        # one, so just ensure that it doesn't have bad hex chars.
        echo ${host} | grep '[g-zG-Z]' >/dev/null 2>&1
    else
        # Not an IPv6 address, try IPv4 and hostname
        # Now if it has any alphas, it should be a hostname, not an IP
        echo ${host} | grep '[a-zA-Z]' >/dev/null 2>&1
        exit_status=$?
        if [[ exit_status -ne 0 ]]; then
            # It has no alphas, verify it is an IP
            # Should be of the form <num>.<num>.<num>.<num>
            # This is a basic check, not a check for weird numbers
            echo ${host} | grep '[0-9].[0-9].[0-9].[0-9]$' >/dev/null 2>&1
            exit_status=$?
            if [[ exit_status -ne 0 ]]; then
		printf "$(gettext 'Invalid host \"%s\".')\n" ${host} | printerr
                exit ${CL_EINVAL}
            fi
        fi
    fi
}

####################################################
#
# validatePassword() password
#
#   Ensures that password does not contain invalid 
#   characters
####################################################
validatePassword()
{
    typeset password=$1

    # If the password has any bad chars in it, then it is not valid
    # NOTE:  If changing the valid characters, be sure to make the 
    # corresponding SPM code changes as well so that they are in sync.
    echo ${password} | grep '[;:"\\ ]' >/dev/null 2>&1
    exit_status=$?
    if [[ exit_status -eq 0 ]]; then
        # password has bad char, exit
        printf "$(gettext 'Invalid password')\n" | printerr
        exit ${CL_EINVAL}
    fi
}

####################################################
#
# ensureFile() file
#
#   Ensures that file exists and is readable
####################################################
ensureFile()
{
    typeset file=$1

    ls ${file} >/dev/null 2>&1
    exit_status=$?
    if [[ exit_status -ne 0 ]]; then
        # file is not readable or not present
        printf "$(gettext 'Unable to read configuration files.')\n" | printerr
        exit ${CL_EINTERNAL}
    fi
}

####################################################
#
# SNMPAdaptorServer_sigHUP() [no_restart]
#
#   Signals the SNMPAdaptorServerModule to re-read 
#   its configuration information
####################################################
SNMPAdaptorServer_sigHUP()
{
    # optional no-restart flag, in case it was done already
    no_restart=$1

    # Tell the SNMPAdaptorServer to re-read its config
    if [[ -z ${no_restart} ]]; then
        if [[ -z ${NSS} ]]; then
            ${RESTART_CACAO}
        else
            ${CACAOCSC} ${CACAOCSC_ARGS} -c "${JMX_INVOKE} ${MODULE_MBEAN} SNMPAdaptorServerRestart"
        fi
    fi
}

###################################################
#
# printerr()
#
#   This is to add msgid's to error messages
#   so it looks like it is coming from one of the 
#   clsnmp* commands.  This should be removed once
#   this file gets it messages from the properties
#   file instead of the message catalogs
#
#   The function generates a 6 digit msgid from the
#   text from stdin and prints the message to stderr.
###################################################
printerr()
{
	# Save the message
	typeset msg=$(cat)

	if [[ -z ${msg} ]]; then
		return
	fi

	if [[ -z ${DO_MSGID} ]]; then
		echo ${msg} | sed "s/^/${PROG}:  /" >&2
		return
	fi

	# Create the message ID
	msgid=$(echo ${msg} | msgid | awk '{print $1}')
	msgid=$(printf "C%6.6d" ${msgid})

	# Print the message
	echo ${msg} | sed "s/^/${PROG}:  \(${msgid}\) /" >&2
}

# If no options are set, print usage and exit
if [[ $# -eq 0 ]]; then
    print_usage
    exit 1
fi

# local vars
typeset remove_flag=
typeset add_flag=
typeset modify_flag=
typeset default_flag=

typeset host=
typeset community=

typeset user=
typeset auth=
typeset pass_file=
typeset security=

typeset protocol=
typeset mode=
typeset print=


# Check options
OPTIND=1
while getopts adrh:c:u:s:t:l:f:mp:z:envxX c 2>/dev/null
do
    case ${c} in
    a)  # add
        add_flag=true
        ;;
    c)  # Community string
        community=$OPTARG
        ;;
    d)  # Set the user as the default
        default_flag=true
        ;;
    e)  # enable
    	if [[ ! -z ${mode} ]]; then
            printf "$(gettext '%s:  ERROR - invalid options')\n" ${PROG} >&2
	    print_usage
	    exit 1
        fi
        mode=enable
        ;;
    f)  # password file
        pass_file=$OPTARG
        ;;
    h)  # Set host access
        host=$OPTARG
        ;;
    l)  # Set SNMP version
        protocol=$OPTARG
        ;;
    m)  # Hidden option to modify a user
    	modify_flag=true
	;;
    n)  # disable
    	if [[ ! -z ${mode} ]]; then
            printf "$(gettext '%s:  ERROR - invalid options')\n" ${PROG} >&2
	    print_usage
	    exit 1
        fi
        mode=disable
        ;;
    p)  # print
        print=$OPTARG
        ;;
    r)  # remove 
        remove_flag=true
        ;;
    s)  # security
        security=$OPTARG
        ;;
    t)  # auth
        auth=$OPTARG
        ;;
    u)  # user
        user=$OPTARG
        ;;
    v)  # Verbose flag, only valid for use with -x
	VERBOSE=true
	;;
    x)  # Use the CL style of the command, vs old sc style
	CLCMD=true
	;;
    X)  # Print the exit status to stdout as the last line of output
	PRINT_EXIT_STATUS=true
	;;
    z)	# Hidden option indicating comma separated operands
	OPERANDS=${OPTARG}
	;;
    \?) # Bad option
        print_usage
        exit 1
        ;;
    *)  # we missed something!
        print_usage
        exit 1
        ;;
    esac
done
shift $((OPTIND - 1))

#  Ensure user root ( or noaccess in case of SPM )
thisuser=`id | awk '{ print $1 }'`
if [[ ${thisuser} != "uid=0(root)" && ${thisuser} != "uid=60002(noaccess)" ]]; then
    printf "$(gettext '%s:  ERROR - You must be root to use this command')\n" ${PROG}  >&2
    print_usage
    exit 1
fi

# Check for mutex flags
if [[ ! -z ${add_flag} && ( ! -z ${remove_flag} ||
			    ! -z ${modify_flag} ||
			    ! -z ${default_flag} ) ]]; then
    printf "$(gettext '%s:  ERROR - incompatible options')\n" ${PROG} >&2
    print_usage
    exit 1
elif [[ ! -z ${remove_flag} && ( ! -z ${modify_flag} ||
				 ! -z ${default_flag} ) ]]; then
    printf "$(gettext '%s:  ERROR - incompatible options')\n" ${PROG} >&2
    print_usage
    exit 1
elif [[ ! -z ${modify_flag} && ! -z ${default_flag} ]]; then
    printf "$(gettext '%s:  ERROR - incompatible options')\n" ${PROG} >&2
    print_usage
    exit 1
fi

# Check for host or user w/out add or remove flag
if [[ ! -z ${host} && ( -z ${add_flag} && -z ${remove_flag} ) ]]; then
    printf "$(gettext '%s:  ERROR - missing option')\n" ${PROG} >&2
    print_usage
    exit 1
elif [[ ! -z ${user} && ( -z ${add_flag} &&
			  -z ${remove_flag} &&
			  -z ${default_flag} &&
			  -z ${modify_flag} ) && -z ${CLCMD} ]]; then
    printf "$(gettext '%s:  ERROR - missing option')\n" ${PROG} >&2
    print_usage
    exit 1
fi

# Check for remove flag without host or user
if [[ ! -z ${remove_flag} && ( -z ${host} && -z ${user} ) ]]; then
    printf "$(gettext '%s:  ERROR - missing option')\n" ${PROG} >&2
    print_usage
    exit 1
fi

# Check for add flag without host or user
if [[ ! -z ${add_flag} && ( -z ${host} && -z ${user} ) ]]; then
    printf "$(gettext '%s:  ERROR - missing option')\n" ${PROG} >&2
    print_usage
    exit 1
fi

# Check for host without community
if [[ ! -z ${host} && -z ${community} ]]; then
    printf "$(gettext '%s:  ERROR - missing option')\n" ${PROG} >&2
    print_usage
    exit 1
fi

# Check for default without security (and without -x)
if [[ ! -z ${default_flag} && -z ${security} && -z ${CLCMD} ]]; then
    printf "$(gettext '%s:  ERROR - invalid options')\n" ${PROG} >&2
    print_usage
    exit 1
fi

# Check for modify without auth
if [[ ! -z ${modify_flag} && -z ${auth} ]]; then
    printf "$(gettext '%s:  ERROR - invalid options')\n" ${PROG} >&2
    print_usage
    exit 1
fi

# Check for pass_file without user
if [[ ! -z ${pass_file} && -z ${user} ]]; then
    printf "$(gettext '%s:  ERROR - invalid options')\n" ${PROG} >&2
    print_usage
    exit 1
fi

# Check for mutex flags
if [[ ! -z ${host} && ! -z ${user} ]]; then
    printf "$(gettext '%s:  ERROR - incompatible options')\n" ${PROG} >&2
    print_usage
    exit 1
elif [[ ! -z ${host} && ( ! -z ${security} || 
                          ! -z ${auth} ||
                          ! -z ${pass_file} ) ]]; then
    printf "$(gettext '%s:  ERROR - incompatible options')\n" ${PROG} >&2
    print_usage
    exit 1
elif [[ ! -z ${user} && ! -z ${community} ]]; then
    printf "$(gettext '%s:  ERROR - incompatible options')\n" ${PROG} >&2
    print_usage
    exit 1
elif [[ ! -z ${protocol} && ( ! -z ${host} ||
                              ! -z ${user} ||
                              ! -z ${print} ||
                              ! -z ${community} ||
                              ! -z ${security} ||
                              ! -z ${auth} ||
                              ! -z ${mode} ||
                              ! -z ${pass_file} ) ]]; then 
    printf "$(gettext '%s:  ERROR - incompatible options')\n" ${PROG} >&2
    print_usage
    exit 1
elif [[ ! -z ${mode} && ( ! -z ${host} ||
                          ! -z ${user} ||
                          ! -z ${print} ||
                          ! -z ${community} ||
                          ! -z ${security} ||
                          ! -z ${auth} ||
                          ! -z ${pass_file} ) ]]; then 
    printf "$(gettext '%s:  ERROR - incompatible options')\n" ${PROG} >&2
    print_usage
    exit 1
elif [[ ! -z ${print} && ( ! -z ${host} ||
                          ! -z ${user} ||
                          ! -z ${community} ||
                          ! -z ${security} ||
                          ! -z ${auth} ||
                          ! -z ${pass_file} ) && -z ${CLCMD} ]]; then 
    printf "$(gettext '%s:  ERROR - incompatible options')\n" ${PROG} >&2
    print_usage
    exit 1
fi

# check for remaining args
if [[ $# -ne 0 ]]; then
    printf "$(gettext '%s:  ERROR - invalid arguments')\n" ${PROG} >&2
    print_usage
    exit 1
fi

# check for auth if add user or modify
if [[ -z ${host} && ( ! -z ${add_flag} || ! -z ${modify_flag} ) &&
      ( -z ${user} || -z ${auth} ) ]]; then
    printf "$(gettext '%s:  ERROR - missing arguments')\n" ${PROG} >&2
    print_usage
    exit 1
fi

# check for absence of auth and pass if remove user or default
if [[ ( ! -z ${remove_flag} || ! -z ${default_flag} ) &&
      ( ! -z ${auth} || ! -z ${pass_file} ) ]]; then
    printf "$(gettext '%s:  ERROR - too many arguments')\n" ${PROG} >&2
    print_usage
    exit 1
fi

# check for valid security
if [[ ! -z ${security} ]]; then
    if [[ ${security} != "noAuthNoPriv" &&
          ${security} != "authNoPriv" &&
          ${security} != "authPriv" ]]; then
       	printf "$(gettext '%s:  ERROR - invalid security setting: %s')\n" ${PROG} ${security} >&2
       	print_usage
        exit 1
    fi
fi

# check for valid auth
if [[ ! -z ${auth} ]]; then
    if [[ ${auth} != "MD5" && ${auth} != "SHA" ]]; then
      	printf "$(gettext '%s:  ERROR - invalid authentication protocol: %s')\n" ${PROG} ${auth} >&2
       	print_usage
        exit 1
    fi
fi

# check for valid protocol
if [[ ! -z ${protocol} ]]; then
    if [[ ${protocol} != "2" && ${protocol} != "3" ]]; then
        if [[ ${protocol} = "SNMPv2" || ${protocol} = "snmpv2" ]]; then
            protocol=2
            break
        elif [[ ${protocol} = "SNMPv3" || ${protocol} = "snmpv3" ]]; then
            protocol=3
            break
	else
      	    printf "$(gettext '%s:  ERROR - invalid protocol value: %s')\n" ${PROG} ${protocol} >&2
       	    print_usage
            exit 1
        fi

    fi
fi

# Hidden flag without -x option
if [[ ! -z ${OPERANDS} && -z ${CLCMD} ]]; then
	print_usage
	exit 1
elif [[ ! -z ${VERBOSE} && -z ${CLCMD} ]]; then
	print_usage
	exit 1
fi


########################################
# Now forward to appropriate functions #
########################################
if [[ ! -z ${protocol} ]]; then
        set_protocol ${protocol}

elif [[ ! -z ${host} ]]; then
    if [[ -n ${CLCMD} ]]; then
	DO_MSGID=true
        PROG="clsnmphost"
    fi

    if [[ ! -z ${remove_flag} ]]; then
        remove_host ${host} ${community}
    else
        # ensure host and community are ok before adding
        validateHost ${host} 
        validateWord ${community} "community"
        add_host ${host} ${community}
    fi

elif [[ ! -z ${user} ]]; then
    if [[ -n ${CLCMD} ]]; then
        PROG="clsnmpuser"
	DO_MSGID=true
    fi

    if [[ ! -z ${remove_flag} ]]; then
        remove_user ${user}
    elif [[ ! -z ${modify_flag} ]]; then
	modify_user ${user} ${auth}
    elif [[ ! -z ${default_flag} ]]; then
	set_default_user ${user} ${security}
    else
        # ensure username is ok
        validateWord ${user} "username"
        add_user ${user} ${auth} ${pass_file}
    fi

elif [[ ! -z ${print} ]]; then
    if [[ ${print} = "hosts" ]]; then
        ensureFile ${ACL}
        print_hosts
	exit 0
    elif [[ ${print} = "users" ]]; then
        ensureFile ${UACL}
        ensureFile ${USER_FILE}
        print_users
	exit 0
    elif [[ ${print} = "all" ]]; then
        ensureFile ${MODULE_PROP}
        ensureFile ${ACL}
        ensureFile ${UACL}
        ensureFile ${USER_FILE}
        print_config
	exit 0

    # Hidden option for 'clsnmpmib list' command
    elif [[ ${print} = "mib-list"  && ! -z ${CLCMD} ]]; then
        if [[ -n ${CLCMD} ]]; then
            PROG="clsnmpmib"
	    DO_MSGID=true
        fi
	print_mib_list

    # Hidden option for 'clsnmphost list' command
    elif [[ ${print} = "host-list" && ! -z ${CLCMD} ]]; then
        if [[ -n ${CLCMD} ]]; then
            PROG="clsnmphost"
	    DO_MSGID=true
        fi
	print_host_list ${community}

    # Hidden option for 'clsnmpuser list' command
    elif [[ ${print} = "user-list" && ! -z ${CLCMD} ]]; then
        if [[ -n ${CLCMD} ]]; then
            PROG="clsnmpuser"
	    DO_MSGID=true
        fi
	print_user_list auth=${auth} default=${default_flag}
    fi

    printf "$(gettext '%s:  ERROR - invalid option: %s')\n" ${PROG} ${print} >&2
    print_usage
    exit 1

elif [[ ! -z ${mode} ]]; then
    if [[ -n ${CLCMD} ]]; then
	PROG="clsnmpmib"
	DO_MSGID=true
    fi
    ${mode}    

fi

exit 0
