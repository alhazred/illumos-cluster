#! /usr/xpg4/bin/sh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the "License").
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/OPENSOLARIS.LICENSE.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets "[]" replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
# 
# Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)cfgchk.ksh	1.8	09/04/18 SMI"
#

#
#
#	Startup wrapper for cfgchk, with or without cacao
#	may be called directly or by new cli C++ wrapper
#	as 'cluster check' or 'cluster list-checks'
#

# if PROG already set by calling command then honor it
# note that PROG is likely to be two words so uses of
# ${PROG} need to be quoted to protect the whitespace

typeset -r ORIG_PROG=${0##*/} # equiv to `basename $0`
typeset -r PROG=${PROG:-${ORIG_PROG}}; export PROG

typeset PROGCHECK="cluster check"
typeset PROGLIST="cluster list-checks"
typeset AWK=/usr/bin/awk

typeset VERSION=1.0
umask 0022

#
# load library definitions and functions
#	exit immediately if load fails
#

typeset -i status=0

#
# attempt to load default and/or user modified definitions
# either or both file locations may be overridden in environment
#

#
# must load lib before defaults file to get overrides in proper order
#

typeset CFGCHK_LIB=${CFGCHK_LIB:-/usr/cluster/lib/cfgchk/cfgchk_lib}
if [[ -e ${CFGCHK_LIB} ]]; then
    . ${CFGCHK_LIB} 2> /dev/null
    status=$?
else
    status=18 #$CL_EINTERNAL
fi

if [[ $status -ne 0 ]]; then  # $CL_NOERR
    # printerr not available
    printf "$(gettext '%s: Unable to load %s')\n" "${PROG}" "${CFGCHK_LIB}" >&2
    exit $status  # early exit
fi


typeset CFGCHK_DEFAULTS=${CFGCHK_DEFAULTS:-/etc/default/cfgchk}
if [[ -e ${CFGCHK_DEFAULTS} ]]; then
    . ${CFGCHK_DEFAULTS} 2> /dev/null
    status=$?
else
    status=$CL_EINTERNAL
fi

if [[ $status -ne $CL_NOERR ]]; then
    errmsg=`printf "$(gettext 'Unable to load %s')" "${CFGCHK_DEFAULTS}"`
    printerr "$errmsg"
    exit $status  # early exit
fi

# test for minimal version of java
typeset SC_LIB_SC=/usr/cluster/lib/scadmin/lib/sc_common
. ${SC_LIB_SC}
if [[ $? != $CL_NOERR ]]; then
    errmsg=`printf "$(gettext 'Unable to load %s')" "${SC_LIB_SC}"`
    printerr "$errmsg"
    exit $status  # early exit
fi

errmsg=`lib_test_java_version`
status=$?
if [[ $status -ne $CL_NOERR ]]; then
    printerr "$errmsg"
    exit $CL_EOP  # early exit
fi

#################################
#
#	Functions
#
#


#
#############
#

usage() {

    if [[ $# -lt 1 ]]; then
	exitval=$CL_EINVAL
    else
	exitval=$1
    fi

    if [[ $exitval -eq $CL_EINVAL ]]; then
	if [[ "$2" != "" ]]; then
	    printerr "$2"
	fi

	msg="Usage error."
	printerr "${msg}"
	echo ""
    fi

  if [[ "$PROG" == "$PROGCHECK" ]]; then
    # "cluster check"
    printf "$(gettext 'Usage:\t%s [<options>] [<clustername>]')\n\n" "${PROG}"
    printf "$(gettext 'Check cluster configuration')\n\n"
    printf "$(gettext 'OPTIONS:')\n\n"
    printf "$(gettext '  -?\tDisplay this help text.')\n\n"
    printf "$(gettext '  -C <checkID>,[...]')\n"
    printf "$(gettext '\tInclude only specified checks.')\n\n"
    printf "$(gettext '  -E <checkID>,[...]')\n"
    printf "$(gettext '\tExclude specified checks (overrides -C.)')\n\n"
    printf "$(gettext '  -e <explorer_archive>,[...]')\n"
    printf "$(gettext '\tPath to explorer archive to use as alternate input.')\n\n"
    printf "$(gettext '  -j <checks_jar>,[...]')\n"
    printf "$(gettext '\tPath to additional jars containing checks.')\n\n"
    printf "$(gettext '  -k <keyword>,[...]')\n"
    printf "$(gettext '\tInclude only checks containing specified keywords.')\n\n"
    printf "$(gettext '  -n <node>,[...]')\n"
    printf "$(gettext '\tRun checks only on specified nodes.')\n\n"
    printf "$(gettext '  -o <output directory>')\n"
    printf "$(gettext '\tOutput directory for reports.')\n\n"
    printf "$(gettext '  -s <severity_level>')\n"
    printf "$(gettext '\tInclude only checks with <severity_level> and above.')\n\n"
    printf "$(gettext '  -f\tForce: clear lock file')\n\n"
    printf "$(gettext '  -v\tVerbose output.')\n\n"

else # "cluster list-checks"

    printf "$(gettext 'Usage:\t%s [<options>] [<clustername>]')\n\n" "${PROG}"
    printf "$(gettext 'List cluster configuration checks')\n\n"
    printf "$(gettext 'OPTIONS:')\n\n"
    printf "$(gettext '  -?\tDisplay this help text.')\n\n"
    printf "$(gettext '  -C <checkID>,[...]')\n"
    printf "$(gettext '\tInclude only specified checks.')\n\n"
    printf "$(gettext '  -E <checkID>,[...]')\n"
    printf "$(gettext '\tExclude specified checks (overrides -C.)')\n\n"
    printf "$(gettext '  -j <jar>,[...]')\n"
    printf "$(gettext '\tPath to additional jars containing checks.')\n\n"
    printf "$(gettext '  -f\tForce: clear lock file')\n\n"
    printf "$(gettext '  -v\tVerbose output.')\n\n"

fi
    exit $exitval
}

#
#########
#

#
# example args: $checkIDFlag "CHECKIDS" $checkID_args
#
ARG_SEP="|"
concat_args() {
    if [[ $1 = ${ON} ]]; then
	ARGS="${ARGS}${ARG_SEP}$2${ARG_SEP}$3"
    fi
}


#
#	Functions
#
##################################





####################
#

integer -r ON=1
integer -r OFF=0

integer helpFlag=$OFF
integer checkIDFlag=$OFF
typeset checkID_args=
integer excludeCheckIDFlag=$OFF
typeset excludeCheckID_args=
integer explorerFlag=$OFF
typeset explorer_args=
integer jarsFlag=$OFF
typeset jar_args=
integer keywordFlag=$OFF
typeset keyword_args=
integer nodeFlag=$OFF
typeset node_args=
integer outputDirFlag=$ON # always on
typeset outputDir_args=
integer severityFlag=$OFF
typeset severity_args=
integer verboseFlag=$OFF
integer versionFlag=$OFF
integer listFlag=$OFF
integer forceFlag=$OFF


#
# Available options for the two commands
#
#	-t(testchecks) is undocumented; for testing use only
#	the name of the test jar specified in defaults file
#
#	-X(nocacao) is undocumented; for use by scinstall only
#

if [[ "$PROG" == "$PROGCHECK" ]]; then
OPTSTRING=":\
C:(checkID)\
E:(excludeCheckID)\
e:(explorer)\
f(force)\
j:(jar)\
k:(keyword)\
n:(node)\
o:(output)\
s:(severity)\
t(testchecks)\
v(verbose)\
V(version)\
X(nocacao)\
?(help)"

else   # "cluster list-checks"
OPTSTRING=":\
C:(checkID)\
E:(excludeCheckID)\
f(force)\
j:(jar)\
o:(output)\
t(testchecks)\
v(verbose)\
V(version)\
X(nocacao)\
?(help)"

listFlag=$ON
fi

# S9 can't deal with long option syntax
# strip out '(longname)' sequences
if [[ `/usr/bin/uname -r` == 5.9 ]]; then
    OPTSTRING=`echo $OPTSTRING | sed -e "s/([^)]*)//g"`
fi

#
# certain options must be handled first
#
#	process args Round One..."
    #
    # pick out output dir, version, and help options
    # check for mutually exclusive -e & -n so can exit before create logdir
    #
	OPTIND=1
	while getopts ${OPTSTRING} opt
	do
		case ${opt} in
		e)	explorerFlag=$ON
			;;
		n)	nodeFlag=$ON
			;;
		C|E|f|j|k|L) 	;; # handle in Round Two
		o)	# outputdir
			outputDirFlag=$ON
			outputDir_args=${OPTARG}
			OUTPUT_DIR=${OPTARG}
			msg=`printf "$(gettext '%s: Option -%s requires a value')\n" "${PROG}" ${opt}`
			test_args ${opt} ${OPTARG} || usage $CL_EINVAL "$msg"
			;;
		S|s|t|v)	;; # handle in Round Two
		V)	# version
			versionFlag=$ON
			;;
		X)	;; # handle in Round Two
		:)	# missing arg
			msg=`printf "$(gettext '%s: Option -%s requires a value')\n" "${PROG}" ${OPTARG}`
			usage $CL_EINVAL "$msg"
			;;
		?)	# help: must be penultimate in the case block
			if [[ "${OPTARG}" != "?" ]]; then
			    msg=`printf "$(gettext 'Unrecognized OPTION - %s')\n" ${OPTARG}`
			    usage $CL_EINVAL "$msg"
			else
			    helpFlag=$ON
			fi
			;;
		*)	# other
			msg=`printf "$(gettext 'Unrecognized option - %s')\n" "$opt"`
			usage $CL_EINVAL "$msg"
			;;
		esac
	done

	# test for clustername operand after round two

#
#####	mutually exclusive args
#
if [[ ${explorerFlag} = $ON && ${nodeFlag} = $ON ]]; then
    msg="$(gettext 'You cannot specify both -n and -e')"
    usage $CL_EINVAL "$msg"
fi

#
#####   handle help directly
#
if [[ ${helpFlag} = ${ON} ]]; then
    usage $CL_NOERR # early exit
fi

#
####    handle version option directly
#
if [[ ${versionFlag} = ${ON} ]]; then
    echo $VERSION
    exit $CL_NOERR  # early exit
fi


#
# setup output dir right now so we can log errors to it
#
	outputDir_args=${outputDir_args:-${DEFAULT_OUTPUT_DIR_BASE}/${TIMESTAMP}}
	LOGFILE=${outputDir_args}/${LOGFILENAME}
	EXITCODE_FILE=${outputDir_args}/${EXITCODE_FILENAME}

	#
	#  create output directory & logfile
	# (remove dir if already exists)
	#
	${RM} -rf ${outputDir_args} 2>/dev/null
	lib_mkdir ${outputDir_args}
	status=$?
	if [[ $status -eq 0 ]]; then
	    ${TOUCH} ${LOGFILE} # make sure we can write
	    status=$?
	fi

	if [[ $status -ne $CL_NOERR ]]; then
	    ${MKDIR} -p ${ERRORLOGDIR}
	    ${TOUCH} ${ERRORLOG}
	    echo "`date` $PROG: failed to create output directory: ${outputDir_args} or logfile: ${LOGFILE}." >> $ERRORLOG
	    errmsg=`printf "$(gettext 'Failed to create output directory (%s) or logfile (%s). Error output written to %s.')" ${outputDir_args} ${LOGFILE} ${ERRORLOG}`
	    printerr "$errmsg"
	    exit $CL_EINVAL  # early exit
	fi

date >> ${LOGFILE}
echo " "   >> ${LOGFILE}

#
# now process all other args
#
#	process args Round Two..."


# any unknown or bad options already handled in Round One
# as well as version, help, output dir
#
# any case using test_args() must keep keywords in synch between
# this usage, cfgchk_lib:test_args(), and Cfgchk.java:processArgs()
#
# any case that expects an arg must call test_args()
#

	OPTIND=1
	while getopts ${OPTSTRING} opt
	do
		msg=`printf "$(gettext '%s: Option -%s requires a value')\n" "${PROG}" ${opt}`
		case ${opt} in
		C)	# checkIDs
			checkIDFlag=$ON
			checkID_args=${OPTARG}
			test_args ${opt} ${OPTARG} || usage $CL_EINVAL "$msg"
			;;
		E)	# excludeCheckIDs
			excludeCheckIDFlag=$ON
			excludeCheckID_args=${OPTARG}
			test_args ${opt} ${OPTARG} || usage $CL_EINVAL "$msg"
			;;
		e)	# explorers
			explorerFlag=$ON
			explorer_args=${OPTARG}
			test_args ${opt} ${OPTARG} || usage $CL_EINVAL "$msg"
			USE_CACAO=${FALSE}
				# no need for cacao if using explorers
			;;
		f)	# force ignore existence of lock file
			forceFlag=$ON
			;;
		j)	# jars
			jarsFlag=$ON
			test_args ${opt} ${OPTARG} || usage $CL_EINVAL "$msg"
			if [[ "$jars_args" = "" ]]; then
			    jars_args=${OPTARG}
			else
			    jars_args=${jars_args},${OPTARG}
			fi
			;;
		k)	# keywords
			keywordFlag=$ON
			keyword_args=${OPTARG}
			test_args ${opt} ${OPTARG} || usage $CL_EINVAL "$msg"
			;;
		L)	# list-checks
			# this flag set only by 'cluster list-checks' not by user!
			listFlag=$ON
			;;
		n)	# nodes
			nodeFlag=$ON
			node_args=${OPTARG}
			test_args ${opt} ${OPTARG} || usage $CL_EINVAL "$msg"
			;;
		s)	# severity
			severityFlag=$ON
			severity_args=${OPTARG}
			test_args "s" $severity_args || usage $CL_EINVAL "Valid arguments for -s are \"critical\", \"high\", \"moderate\", \"low\", \"warning\", \"information\""  # no I18n
	;;
		t)	# special test_checks jar
			jarsFlag=$ON
			if [[ "$jars_args" = "" ]]; then
			    jars_args=${TESTCHECKSJAR}
			else
			    jars_args=${jars_args},${TESTCHECKSJAR}
			fi
			;;
		v)	# verbose
			verboseFlag=$ON
			;;
		o|V)	;;	# handled in Round One
		X)	USE_CACAO=${FALSE}
			;;
		:|?)	;;	# handled in Round One
		esac
	done

	# Single optional operand allowed: clustername to check
	# must be the same as current cluster
	if [[ $((OPTIND -1)) -ne $# ]]; then
	    shift $((OPTIND -1))
	    typeset clname=`/usr/cluster/bin/scha_cluster_get -O CLUSTERNAME 2> /dev/null`
	    # no need to test exit code
	    # if not in cluster mode or clustering not installed
	    # the clname comparison will fail
	    if [[ "$*" != "$clname" ]]; then
		msg=`printf "$(gettext '${NODENAME} is not a member of cluster %s.')\n" "$*"`
		usage $CL_EINVAL "$msg"
	    fi
	fi


#
######  end process args


#
# concatenation of user args for delivery to java as array of words
#	LOGFILE  *must* be first word!
#	NODENAME *must be second word!
#
# keyword must match Globals.java
#
ARGS="${LOGFILE}|${NODENAME}"

concat_args $checkIDFlag	INCLUDECHECKIDS	$checkID_args
concat_args $excludeCheckIDFlag EXCLUDECHECKIDS $excludeCheckID_args
concat_args $explorerFlag	EXPLORERS	$explorer_args
concat_args $jarsFlag		CHECKJARS	$jars_args
concat_args $keywordFlag	KEYWORDS	$keyword_args
concat_args $nodeFlag		NODES		$node_args
concat_args $outputDirFlag	OUTPUTDIR	$outputDir_args
concat_args $verboseFlag	VERBOSE		""
concat_args $severityFlag	SEVERITY	$severity_args
concat_args $listFlag		LISTCHECKS	""
concat_args $forceFlag		FORCECHECKS	""


##################################################################
#
#	invoke cfgchk via cacao or directly under java
#


    ####################      Using cacao

if [[ ${USE_CACAO} = ${TRUE} ]]; then
    echo "${PROG} using cacao..." >> ${LOGFILE}

    CSC=/usr/lib/cacao/lib/tools/cacaocsc
    TARGET_NODE=localhost

    CSC_PORT=`/usr/xpg4/bin/sh /usr/sbin/cacaoadm get-param commandstream-adaptor-port`
    CSC_PORT_NUM=`echo $CSC_PORT | $AWK -F= '{ print $2 }'`

    CERT_DIR=/etc/cacao/instances/default/security/nss/wellknown
    CACAO_PSWD=/etc/cacao/instances/default/security/password

    CMD="com.sun.cluster.cfgchk.agent.cfgchk:administrator" # matches CfgchkModule
    CMD="${CMD} ${ARGS}"

    echo "cmdline: $CSC -h $TARGET_NODE -p $CSC_PORT_NUM -d $CERT_DIR -f $CACAO_PSWD -c \'${CMD}\'" >> ${LOGFILE}

    $CSC -h $TARGET_NODE -p $CSC_PORT_NUM -d $CERT_DIR -f $CACAO_PSWD -c "${CMD}"
    status=$?

	# The java process will be spitting lines to stdout if verbose.
	# exitcode is highest severity of all violated checks or 100
	# for no violations at all. See ExitCode.java
	# These lines coming to stdout and stderr typically get logged
	# at their source.

    echo "   " >> ${LOGFILE}
    echo "${PROG} with cacao -- ending status: $status" >> ${LOGFILE}

else

    ###################  else not using cacao

    echo "${PROG} NOT USING CACAO..." >> ${LOGFILE}

    CLASSPATH=/usr/cluster/lib/cfgchk/agent_cfgchk.jar # Main
    CLASSPATH=${CLASSPATH}:/usr/cluster/lib/cfgchk/cfgchk_checks.jar  # factory checks
    CLASSPATH=${CLASSPATH}:/usr/cluster/lib/cfgchk/  # get resources
    CLASSPATH=${CLASSPATH}:/usr/lib/cacao/lib/cacao_cacao.jar
    CLASSPATH=${CLASSPATH}:/opt/SUNWjdmk/5.1/lib/jmx.jar
    CLASSPATH=${CLASSPATH}:/usr/jdk/latest/jre/lib/rt.jar

    echo $CLASSPATH >> ${LOGFILE}

    typeset -r CFGCHK_MAIN=com/sun/cluster/cfgchk/Cfgchk
    echo "${JAVA} -classpath $CLASSPATH $CFGCHK_MAIN $ARGS" >> ${LOGFILE}
    ${JAVA} -classpath $CLASSPATH $CFGCHK_MAIN $ARGS
    status=$?

	# The java process will be spitting lines to stdout if verbose.
	# exitcode is highest severity of all violated checks or 100
	# for no violations at all. See ExitCode.java
	# These lines to stdout and stderr typically get logged
	# at their source.

    echo "NO CACAO -- ending status: $status" >> ${LOGFILE}
    echo "   " >> ${LOGFILE}
fi

# Write the exit ("max severity encountered') return code into
# a file, $EXITCODE_FILENAME, in the
# report directory. This file can be used to check the return
# code from cluster check when it is called by other commands,
# or scripts, in which case the return code is
# mapped and different from this ${exitstatus}.
#

echo "${PROG}: =============== " >> ${LOGFILE}
echo "${PROG}: exit status $status " >> ${LOGFILE}

echo $status > ${EXITCODE_FILE}

#
# now determine appropriate final 'new cli' exit code
# 

errmsg=`lib_map_exit_code $status`
exitcode=$?

echo "${PROG}: exitcode: $exitcode " >> ${LOGFILE}
echo "${PROG}: errmsg: $errmsg" >> ${LOGFILE}
printerr "${errmsg}" # no op if zero length

date >> ${LOGFILE}
echo " -- END --" >> ${LOGFILE}

#
# don't leave logfiles around from successful list-checks runs
#
if [[ "$PROG" == "$PROGLIST" && $exitcode == 0 ]]; then
    ${RM} -rf ${outputDir_args}
fi

exit $exitcode
