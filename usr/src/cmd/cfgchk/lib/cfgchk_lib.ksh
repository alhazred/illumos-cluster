#! /bin/ksh

#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the "License").
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/OPENSOLARIS.LICENSE.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets "[]" replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
# 
# Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)cfgchk_lib.ksh	1.7	09/04/13 SMI"
#

##################################################################
#
#
#	definitions and functions for cfgchk with or without cacao
#
#
##################################################################

umask 0
export LC_COLLATE=C

typeset AWK=/usr/bin/awk
typeset -x TEXTDOMAIN=SUNW_SC_CMD
typeset -x TEXTDOMAINDIR=/usr/cluster/lib/locale

# constants
typeset -r TRUE="true"   # no I18N
typeset -r FALSE="false" # no I18N
typeset NODENAME=$(uname -n)

# new cli error codes (see cl_error.h)
# not all are used by cfgchk
#  (see lib_map_exit_code)
integer -r CL_NOERR=0	# No error
integer -r CL_ENOMEM=1	# Not! enough swap space
integer -r CL_EINVAL=3	# Invalid argument
integer -r CL_ERECONF=5	# Cluster is reconfiguring
integer -r CL_EACCESS=6	# Permission denied
integer -r CL_ESTATE=9	# Unexpected state
integer -r CL_EMETHOD=10	# Resource method failed
integer -r CL_EPROP=15		# Invalid property
integer -r CL_EINTERNAL=18	# Internal error
integer -r CL_EIO=35	# I/O error
integer -r CL_ENOENT=36	# No such object
integer -r CL_EOP=37	# Operation is not allowed
integer -r CL_EBUSY=38	# Object busy
integer -r CL_EEXIST=39	# Object already exists
integer -r CL_ETYPE=41	# Unknown type
integer -r CL_ECLMODE=50	# Node is in cluster mode
integer -r CL_ENOTCLMODE=51	# Node is not in cluster mode

# user can override via /etc/default/ config file
typeset JAVA_HOME=${JAVA_HOME:-/usr/java}
typeset JAVA=${JAVA:-${JAVA_HOME}/bin/java}
typeset	USE_CACAO=${TRUE}
typeset	TESTCHECKSJAR=/usr/cluster/lib/cfgchk/cfgchk_test_checks.jar

typeset -r TIMESTAMP="$(date +%Y-%m-%d.%T)"
typeset    OUTPUT_DIR=
typeset    LOGFILE=
typeset -r EXITCODE_FILENAME=cluster_check_exit_code.log

integer status=0

###################
# these values also may be overridden in /etc/default/cfgchk
#

RM=/usr/bin/rm
TOUCH=/usr/bin/touch
MKDIR=/usr/bin/mkdir
MV=/usr/bin/mv
TR=/usr/bin/tr

CFGCHK_BASE=/var/cluster/logs
LOGFILENAME=cluster_check.log
DEFAULT_OUTPUT_DIR_BASE=${CFGCHK_BASE}/cluster_check
STARTUP_LOG_DIR=${CFGCHK_BASE}/cluster_check
LOGGING=true

#
###################



##################################################################
#
#  functions
#


###################################################
#
#	called for additional input validation
#
###################################################
test_args() {
    case $1 in
	S) # sort (not offered in v1.0)
	    typeset -l keyw=$2 # force arg to lowercase
	    if [ "$keyw" != "severity" -a "$keyw" != "keyword" ]; then
	    return 1
	    fi
	    ;;
	s) # min severity to report
	    typeset -l keyw=$2 # force arg to lowercase
	    if [ "$keyw" != "critical" -a "$keyw" != "high" -a "$keyw" != "moderate" -a "$keyw" != "low" -a "$keyw" != "warning"  -a "$keyw" != "information" ]; then
	    return 1
	    fi
	    ;;

	# options that require an arg:
	#  assume that leading dash means forgotten arg and we're
	#  actually looking at next option: is usage error
	C|E|e|j|k|n|o)
	    echo $2 | /usr/bin/grep  "^-" > /dev/null
	    status=$?
	    if [[ $status -eq 0 ]]; then
		return 1
	    fi
	    ;;
    esac
    return 0
} # test_args

###################################################
#
# printerr()
#
#   Add msgid's to error messages like the other
#   'new cli' commands.
#
#   The function generates a 6 digit msgid from the
#   text from stdin and prints the message to stderr.
#
#	(copied verbatim from sceventmib.ksh,
#	wholly owned Sun Cluster original code)
###################################################
printerr()
{
	# Save the message
	typeset msg="$1"

	if [[ -z "${msg}" ]]; then
		return
	fi

	# Create the message ID
	msgid=$(echo "${msg}" | /usr/sbin/msgid | ${AWK} '{print $1}')
	msgid=$(printf "C%6.6d" ${msgid})

	# Print the message
	echo "${msg}" | sed "s/^/${PROG}:  \(${msgid}\) /" >&2
}


###############################################
#
# lib_map_exit_code
#
#	input:	exit code from configuration engine
#		or attempt to launch cacaocsc/java
#	output:	error message corresponding to mapped code
#	returns:	exit code to return to user
#
#	original code already stored in EXITCODE_FILENAME
#
#	5 if cfgchk module not registered or similar issue
#       127 if can't find cacaocsc
#	192 if cacao not running
#       196: cacao_launcher[23299]: SUNWcacaort launcher:
#		Common Agent Container exited abnormaly
#		got this case when cfgchk.jar changed under running cacao
#	From ExitCode.java:
#		OK             =   100;
#		ERR_ERR        =   120; // used as a non-err/err threshhold
#		ERR_BUSY       =   121; // run already in progress
#		ERR_ARG        =   124; // bad args fed in
#		ERR_INTERNAL   =   125; // otherwise uncaught
#		ERR_INTR       =   126; // interrupted
#
###############################################
lib_map_exit_code()
{
    incoming=$1
    mapped=

    case $1 in 
	5)   mapped=$CL_ESTATE
 	     errmsg="$(gettext 'not properly installed')"
		;;
	127) mapped=$CL_ENOENT
 	     errmsg="$(gettext 'unable to locate cacaocsc')"
		;;
	192) mapped=$CL_EOP
 	     errmsg="$(gettext 'unable to reach Common Agent Container')"
		;;
	196) mapped=$CL_ESTATE
 	     errmsg="$(gettext 'Common Agent Container exited abnormally')"
		;;

	# error messages for 110-126 already printed by engine
	120) mapped=$CL_EINTERNAL
 	     errmsg=
		;;
	121) mapped=$CL_EBUSY
 	     errmsg=
		;;
	124) mapped=$CL_EINVAL
 	     errmsg=
		;;
	125) mapped=$CL_EINTERNAL
 	     errmsg=
		;;
	126) mapped=$CL_EINTERNAL
 	     errmsg=
		;;
	*) if [[ $1 -gt 99 && $1 -lt 110 ]]; then
		mapped=$CL_NOERR;
	    else
		mapped=$1
	    fi
	    errmsg=
    esac

    if [[ "${errmsg}" != "" ]]; then
	echo $errmsg
    fi
    return $mapped
}

###############################################
#
# lib_mkdir()
#
#	input:		name of directory to create
#	output:		prints error message
#	returns:	0 success || 1 failure
#
#	action: 
#
#	Attempt to mkdir -p the target directory.
#
######################################################
lib_mkdir()
{
    typeset target=$1

    ${MKDIR} -p ${target} 2> /dev/null
    integer status=$?
    if [[ ${status} -ne 0 ]]; then
	return 1
    fi

    return 0
} # lib_mkdir
