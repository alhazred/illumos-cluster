/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)MsgID.java	1.2	08/05/29 SMI"
 */
package com.sun.cluster.cfgchk.utils;


/*
 * This class generates message id's according to the
 * algorithm of /usr/include/sys/strlog.h
 * Sun Cluster prepends a "C" to the integer, which we
 * do in ErrorMsg.java
 *
 *  See also end of file for original C source
 *	man msgid
 *	example# echo hello | msgid
 *	205790 hello
 *	137713 free:
 *		freeing free frag, dev:0x%lx, blk:%ld, cg:%d, ino:%lu, fs:%s
 *        567420 ialloccg: block not in mapfs = %s
 *        845546 alloc: %s: file system full
 */

public class MsgID  {

	public static String makeMsgID(String str) {

		long idu = 0;
		long msgidValL = 0L;
		int space = (int)' ';

		long longMaskAnd2 = 0L;
		longMaskAnd2 = 0x00000000ffffffffL; // keep bottom 32 bits

		int numChars = str.length();
		char[] chars = new char[numChars];
		str.getChars(0, numChars, chars, 0);

		for (int i = 0; i < numChars; i++) {
			int c = (int)chars[i];
			if (c >= space) {

				long idu1 = 0L;
				long idu2 = 0L;
				idu1 = (idu >> 5);
				idu2 = (idu << 27) & longMaskAnd2;
				idu = idu1 + idu2 + c;
				/*
				System.out.println("char["+i+"]: " + chars[i]);
				System.out.println("" +
				    " idu1: " + idu1 +
				    "\t idu2: " + idu2 +
				    "\t idu : " + idu +
				    "");
				System.out.println("" +
				    " idu1: 0x" + Long.toHexString(idu1) +
				    "\t idu2: 0x" + Long.toHexString(idu2) +
				    "\t idu : 0x" + Long.toHexString(idu) +
				    "");
				System.out.println("");
				*/
			}
		}

		msgidValL = (idu % 899981) + 100000;

		// return as a String in special Sun Clusterformat
		return "(C" + (int)msgidValL + ")";

	} // make Msgid

} // class

/*
	/usr/include/sys/strlog.h

	#define STRLOG_MAKE_MSGID(fmt, msgid)
	{
	        uchar_t *__cp = (uchar_t *)fmt;
	        uchar_t __c;
	        uint32_t __id = 0;
	        while ((__c = *__cp++) != '\0')
	                if (__c >= ' ')
	                        __id = (__id >> 5) + (__id << 27) + __c;
	        msgid = (__id % 899981) + 100000;
	}

*/
