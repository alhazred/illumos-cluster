/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)ClusterDataSrc.java	1.2	08/05/29 SMI"
 */
package com.sun.cluster.cfgchk.datasrc;

/**
 * SunCluster specific DataSrc interface that
 * has access to all the SolarisDataSrc data calls
 * as well as the top superclass, BasicDataSrc
 *
 * Implemented by LiveClusterDataSrc and
 * ExplorerClusterDataSrc.
 *
 * Used directly by cfgchk.
 */

public interface ClusterDataSrc extends SolarisDataSrc {



        public String  getNodeID() throws DataException;
	// nodeID is an integer under SC3.x but typically treated as String

        public String  getClustername() throws DataException;

	public String  getClusterVersion() throws DataException;

	public boolean isClusterMode(boolean usingCacao)
		throws DataException;

	public  String[] getRGNames() throws DataException;
	public  String[] getRGNodelist(String rgName) throws DataException;
	public  String[] getRSNames(String rgName) throws DataException;
	public  String   getRTName(String rsName, String rgName)
		throws DataException;
	public  String[] getRegisteredRTNames() throws DataException;


} // interface
