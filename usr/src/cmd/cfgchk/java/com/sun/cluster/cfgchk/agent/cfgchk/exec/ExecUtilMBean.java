/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)ExecUtilMBean.java	1.2	08/05/29 SMI"
 */
package com.sun.cluster.cfgchk.agent.cfgchk.exec;

import com.sun.cacao.invocation.InvocationException;
import com.sun.cacao.invocation.InvocationStatus;
import java.util.logging.Logger;

public interface ExecUtilMBean {

	/**
	 * Part of the contract with the ObjectNameFactory
	 *
	 */
	final public static String TYPE = "ExecUtil";

	/**
	 * getNodeName
	 *
	 * This method is called by the client to determine if the
	 * proxy is connected/mbean is alive. (See 6247850)
	 */
	public String getNodeName();

	/**
	 * execute
	 *
	 * This method is called to execute a command on a specified node
	 * in a specific locale under cacao.
	 *
	 * @param String[] : command to execute
	 *
	 * @return InvocationStatus  : OK
	 *         InvocationException : command failed
	 *
	 */
	public InvocationStatus cacaoExecuteCommand(String[] cmdArray)
		throws InvocationException;

	public InvocationStatus cacaoExecuteCommand(Logger logger,
	    String[] cmdArray)
		throws InvocationException;

} // interface
