/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)CheckSTest027.java	1.2	08/05/29 SMI"
 */
package com.sun.cluster.cfgchk.check.testchecks;

import java.io.FileNotFoundException;
import java.io.IOException;

import com.sun.cluster.cfgchk.BasicCheck;
import com.sun.cluster.cfgchk.Check;
import com.sun.cluster.cfgchk.CheckExecutionException;
import com.sun.cluster.cfgchk.datasrc.DataException;
import com.sun.cluster.cfgchk.utils.logger.LoggerUtils;
import com.sun.cluster.cfgchk.utils.StringUtils;
import com.sun.cluster.cfgchk.utils.Utils;


/**
 * Test Utils.runCommandStrArray()
 * Run command named in aux_data.
 * Insufficient data if command not found.
 * Always violated in order to show command output in report.
 *
 * Note: use command '/usr/bin/cat' with no args to test a command
 * that fails to return.
 *
 * Severity CRITICAL.
 */

// No I18n

public class CheckSTest027 extends BasicCheck {

	private StringBuffer cmdResultSB =
	    new StringBuffer("command unassigned");

    public CheckSTest027() {
        checkID = "STest027";
        severity = Check.INFORMATION;

	// sccs keyword: must be 1.2 when checked out under sccs
	version = "1.2";
	// sccs keyword: must be 08/05/29 when checked out under sccs
	revisionDate = "09/12/08";

        keywords.add("SunCluster3.x");
	keywords.add("whitebox test");
        keywords.add("test");

	/* BEGIN JSTYLED */
        problemStatement = "Test check: invoke command named in aux data. Always violated or InsufficientData. Shows command output in report.";
	/* END JSTYLED */

    } // ()

    /**
     * @return a string describing the logic of the applicability test
     */
    protected String assignApplicabilityLogic() {
        String aLogic = "Always applies to Sun Cluster 3.x node.";
        return aLogic;
    } // assignApplicabilityLogic


    /**
     * @return a string describing the logic of the check
     */
    protected String assignCheckLogic() {
	/* BEGIN JSTYLED */
        String cLogic = "Look up command in aux_data. Insufficient Data exit if command not found. Run command and show output in report. Always violated in order to get data to report.";
	/* END JSTYLED */
        return cLogic;
    } // assignCheckLogic


	/**
	 * @return a string for use in output report analyzing the problem
	 * may contain dynamic data
	 */
	public String getAnalysis() {
		return cmdResultSB.toString();
	} // getAnalysis

	public String getRecommendations() {
		return checkID;
	} // getRecommendations


    // always
    public boolean isApplicable() throws CheckExecutionException {
        logger.info("isApplicable() -- ENTER --");
        boolean applicable = true;

        return applicable;
    } // isApplicable

    public boolean doCheck() throws CheckExecutionException {

        logger.info("-- ENTER --");
        boolean passes = false;
	String cmdName = null;

	try {
		String nodename = Utils.getLocalHostname();
		logger.info("nodename: " + nodename);

		cmdName = getAuxDataValue(checkID + ".cmdName");
		logger.info("cmdName: " + cmdName);
		cmdResultSB = new StringBuffer("nodename: " + nodename +
		    "\tcommand: " + cmdName + "\n");

		String[] stdout = Utils.runCommandStrArray(getNodename(),
		    new String[] { cmdName }, /* usingCacao */ false);
		cmdResultSB.append("exitStatus: 0\n");
		cmdResultSB.append("stdout:\n");
		cmdResultSB.append(StringUtils.stringArrayToString(stdout));

	} catch (DataException ex) {

		// rare case where we're not treating dex as crashed check
		logger.severe("DataException: " + ex);

		// message includes exit code & stderr as a string
		cmdResultSB.append(ex.getMessage());

		// special case within special case...
		String s = "command execution thread timedout";
		if (ex.getMessage().indexOf(s) != -1) {
			throw new CheckExecutionException(ex);
		}

	} catch (Exception ex) {
		logger.severe("Exception: " + ex);
		LoggerUtils.exception("CheckSTest025.doCheck() Exception", ex,
		    logger);
		throw new CheckExecutionException(ex);
	}

        logger.info("passes: " + passes);
        logger.info("-- EXIT --");
        return passes;
    } // doCheck

} // class
