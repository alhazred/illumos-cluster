/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)MultiMsgException.java	1.3	08/09/22 SMI"
 */
package com.sun.cluster.cfgchk.common;

import java.util.ArrayList;
import java.util.List;
import java.util.Iterator;


/**
 * this flavor of Exception is capable of
 * holding more than one message
 */
public class MultiMsgException extends Exception {


	private static final long serialVersionUID = -6357426157032292335L;
	private ArrayList msgs = null;
	protected String exceptionType = "MultiMsgException";
	// each subclass must override with own type name

	public MultiMsgException() {
		super();
		msgs = new ArrayList();
    } // ()


	public MultiMsgException(String s) {
		super(s); // available via getMessage()
		msgs = new ArrayList();
		addMessage(s); // available via getMessages()
	} // ()

	public MultiMsgException(Exception e) {
		super(e);
		msgs = new ArrayList();
		addMessage(e.getMessage()); // available via getMessages()
	} // ()

	public MultiMsgException(Throwable t) {
		super(t);
		msgs = new ArrayList();
		addMessage(t.getMessage()); // available via getMessages()
	} // ()

	/**
	 * @param lMsg already localized message
	 */
	public void addMessage(String lMsg) {
		msgs.add(lMsg);
	}

	/**
	 * @param lMsg List of already localized messages
	 */
    	public void setMessages(ArrayList msgs) {
		this.msgs = msgs;
	}

	public String getMessage(int i) {
		return (String)msgs.get(i);
	}

	public List getMessages() {
		return msgs;
	}

	// overrides Exception.toString()
	// the messages contents may be localized
	public String toString() {
		StringBuffer sb = new StringBuffer(exceptionType + ": ");
		boolean firstMsg = true;
		List errMsgs = getMessages();
		Iterator it = errMsgs.iterator();
		while (it.hasNext()) {
			if (!firstMsg) sb.append(" | ");
			String msg = (String)it.next();
			sb.append(msg);
		}
		return sb.toString();
	} // toString

} // class
