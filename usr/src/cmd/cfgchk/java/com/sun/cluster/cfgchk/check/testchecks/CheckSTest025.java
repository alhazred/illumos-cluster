/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)CheckSTest025.java	1.2	08/05/29 SMI"
 */
package com.sun.cluster.cfgchk.check.testchecks;

import java.io.FileNotFoundException;
import java.io.IOException;

import com.sun.cluster.cfgchk.BasicCheck;
import com.sun.cluster.cfgchk.Check;
import com.sun.cluster.cfgchk.CheckExecutionException;
import com.sun.cluster.cfgchk.datasrc.DataException;
import com.sun.cluster.cfgchk.utils.logger.LoggerUtils;


/**
 * Passes if live data src.
 * Yields InsufficientData if explorer data src
 * Severity CRITICAL.
 */

// No I18n

public class CheckSTest025 extends BasicCheck {


    public CheckSTest025() {
        checkID = "STest025";
        severity = Check.CRITICAL;

	// sccs keyword: must be 1.2 when checked out under sccs
	version = "1.2";
	// sccs keyword: must be 08/05/29 when checked out under sccs
	revisionDate = "09/12/08";

        keywords.add("SunCluster3.x");
	keywords.add("whitebox test");
        keywords.add("test");

	/* BEGIN JSTYLED */
        problemStatement = "Test check: Live data src: passes. Explorer data src: insufficient data";
	/* END JSTYLED */


    } // ()

    /**
     * @return a string describing the logic of the applicability test
     */
    protected String assignApplicabilityLogic() {
        String aLogic = "Always applies to Sun Cluster 3.x node.";
        return aLogic;
    } // assignApplicabilityLogic


    /**
     * @return a string describing the logic of the check
     */
    protected String assignCheckLogic() {
        String cLogic = "exercise file not found in explorer archive";
        return cLogic;
    } // assignCheckLogic


	/**
	 * @return a string for use in output report analyzing the problem
	 * may contain dynamic data
	 */
	public String getAnalysis() {
		return checkID;
	} // getAnalysis

	public String getRecommendations() {
		return checkID;
	} // getRecommendations


    // always
    public boolean isApplicable() throws CheckExecutionException {
        logger.info("isApplicable() -- ENTER --");
        boolean applicable = true;

        return applicable;
    } // isApplicable

    public boolean doCheck() throws CheckExecutionException {

        logger.info("-- ENTER --");
        boolean passes = true; // defaut: live data src passes

	try {
		// do this work only if explorer data src
		if (! getDataSrc().isLiveDataSource()) {

			// mapped but not present in archive
			String filename = "/file/not/found";

            // TextFile not used; just get fnfex
			getDataSrc().getTextFile(filename);
		}
	} catch (DataException ex) {
		logger.severe("DataException: " + ex);
		LoggerUtils.exception("CheckSTest025.doCheck() DataException",
		    ex, logger);
		throw new CheckExecutionException(ex);
	} catch (FileNotFoundException ex) {
		logger.severe("FileNotFoundException: " + ex);
		logger.info("as expected... CheckSTest025.doCheck() " +
		    "FileNotFoundException" + ex);
		setInsufficientDataMsg(ex.getMessage());
		passes = false;
	} catch (IOException ex) {
		logger.severe("IOException: " + ex);
		LoggerUtils.exception("CheckSTest025.doCheck() IOException",
		    ex, logger);
		throw new CheckExecutionException(ex);
	} catch (Exception ex) {
		logger.severe("Exception: " + ex);
		LoggerUtils.exception("CheckSTest025.doCheck() Exception", ex,
		    logger);
		throw new CheckExecutionException(ex);
	}

        logger.info("passes: " + passes);
        logger.info("-- EXIT --");
        return passes;
    } // doCheck

} // class
