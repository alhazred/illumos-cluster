/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)VfstabEntry.java	1.2	08/05/29 SMI"
 */
package com.sun.cluster.cfgchk.datasrc.dataObject;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.StringTokenizer;
import java.util.logging.Logger;

import com.sun.cluster.cfgchk.datasrc.ParseException;
import com.sun.cluster.cfgchk.utils.StringUtils;

import com.sun.cluster.cfgchk.utils.logger.RemoteLogger;


/**
 * Stores a single (non-comment) vfstab line (entry)
 * offers parts of an entry on-demand
 *
 * populated by lines from TextFile
 */
public class VfstabEntry implements Serializable {
    static final long serialVersionUID = 2885185509823634153L;

    private String rawEntry = null;
    private String deviceToMount = null;
    private String deviceToFsck = null;
    private String mountPoint = null;
    private String fsType = null;
    private int fsckPass = -1;
    private boolean mountAtBoot = false;
    private ArrayList vfstabMountOptions = null;
    private boolean isGlobal = false;

    private static transient Logger logger = RemoteLogger.getRemoteLogger();


	// construct an entry from single vfstab non-comment line
    public VfstabEntry(String vfstabLine) throws ParseException {
        logger.finest("-- ENTER (vfstabLine): " + vfstabLine);
	// this.logger = lg;

	rawEntry = vfstabLine;
        StringTokenizer st = new StringTokenizer(vfstabLine);
        if (st.countTokens() != 7) {
            throw new ParseException("Insufficient fields in: " + vfstabLine);
        }

	// this order must be maintained
        deviceToMount = st.nextToken();
        deviceToFsck = st.nextToken();
        mountPoint = st.nextToken();
        fsType = st.nextToken();
	try {
		fsckPass = parseFsckPass(vfstabLine, st.nextToken());
		mountAtBoot = parseMountAtBoot(st.nextToken()); // no pex
		vfstabMountOptions = parseMountOptions(st.nextToken());
	    } catch (ParseException e) {
		    logger.warning("ParseException: " + e);
		    throw e;
	    }
    } // ()


	public String getRawEntry() {
		return rawEntry;
	}

    /**
     * @param str: raw mount options field from vfstab entry
     * @return options stored as elements in ArrayList
     */
    private ArrayList parseMountOptions(String str) {
        logger.finest(" -- ENTER: " + str);
        ArrayList mountOptions = new ArrayList();
        StringTokenizer st = new StringTokenizer(str, ",");
        String tok = null;
        while (st.hasMoreTokens()) {
            tok = st.nextToken();
            mountOptions.add(tok);
            logger.finest("added: " + tok);
            if (tok.equals("global")) {
                isGlobal = true;
                logger.finest("isGlobal: " + isGlobal);
            }
        } // while
        logger.finest(" -- EXIT; isGlobal: " + isGlobal);
        return mountOptions;
    } // parseMountOptions

    /**
     * @param str
     * @return true or false
     */
    private boolean parseMountAtBoot(String str) {
        boolean mab = false;
        if (str.equalsIgnoreCase("yes")) {
            mab = true;
        }
        return mab;
    } // parseMountAtBoot


    /**
     * @param str: expected to hold an int
     * @return int
     * @throws ParseException
     */
    private int parseFsckPass(String line, String str) throws ParseException {
        int fp = -1;
        try {
            fp = Integer.parseInt(str);
        } catch (NumberFormatException e) {
            if (!str.equals("-")) { // valid non-int value: leave as -1
                throw new ParseException("On line: '" +line +
		    "' expected integer; got >>" + str + "<<");
            }
        }
        return fp;
    } // parseFsckPass

    public String getDeviceToFsck() {
	    if (deviceToFsck != null)
		    return deviceToFsck;
	    else
		    return "";
    }
    public String getDeviceToMount() {
	    if (deviceToMount != null)
		    return deviceToMount;
	    else
		    return "";
    }

    public int getFsckPass() {
	    return fsckPass;
    }

    public String getFsType() {
	    if (fsType != null)
		    return fsType;
	    else
		    return "";
    }

    public boolean isMountAtBoot() {
        return mountAtBoot;
    }

    public String getMountPoint() {
	    if (mountPoint != null)
		    return mountPoint;
	    else
		    return "";
    }

    public ArrayList getVfstabMountOptions() {
	    if (vfstabMountOptions != null)
		    return vfstabMountOptions;
	    else
		    return new ArrayList();
    }

	public String getVfstabMountOptionsStr() {
		String[] sarray = StringUtils.stringArrayFromArrayList(logger,
		    getVfstabMountOptions());
		String s = StringUtils.stringArrayToString(sarray, "; ");
		return s;
    }

    /**
     * @return true if this entry is a global filesystem
     */
	public boolean isGlobalEntry() {
		logger.finest(getMountPoint() + "; global: " + isGlobal);
		return isGlobal;
	} // isGlobalEntry

    public boolean equals(VfstabEntry entry) {
        boolean equal = false;
        if (entry instanceof VfstabEntry) {
            if (
                    deviceToFsck.equals(entry.getDeviceToFsck()) &&
                    deviceToMount.equals(entry.getDeviceToMount()) &&
                    fsckPass == entry.getFsckPass() &&
                    fsType.equals(entry.getFsType()) &&
                    isGlobal == entry.isGlobalEntry() &&
                    mountAtBoot == entry.isMountAtBoot() &&
                    mountPoint.equals(entry.getMountPoint()) &&
                    isSameMountOptions(entry.getVfstabMountOptions())) {
                equal = true;
            }
        }
        return equal;
    } // equals

    public int hashCode() {
        return mountPoint.hashCode();
    } // hashCode

    /*
     * comparing two ArrayLists of Strings
     */
    private boolean isSameMountOptions(ArrayList otherMountOptions) {
        boolean same = false;
        if (
                vfstabMountOptions.containsAll(otherMountOptions) &&
                otherMountOptions.containsAll(vfstabMountOptions)) {
            same = true;
            logger.fine("same");
        } else {
            logger.fine("not same");
        }
        return same;
    } // isSameMountOptions

	public String toString() {
		String SEP = " | ";
		// SEP = "\n\t\t";
		StringBuffer sb = new StringBuffer("");
		if (sb.length() > 0) sb.append(SEP);
		sb.append("rawEntry: " + rawEntry + SEP);
		sb.append("mountPoint: " + mountPoint + SEP);
		sb.append("isGlobal: " + isGlobal + SEP);
		sb.append("deviceToMount: " +  deviceToMount + SEP);
		sb.append("deviceToFsck: " + deviceToFsck + SEP);
		sb.append("fsType: " + fsType + SEP);
		sb.append("fsckPass: " + fsckPass + SEP);
		sb.append("mountAtBoot: " + mountAtBoot + SEP);
		sb.append("vfstabMountOptions: " + getVfstabMountOptionsStr() +
		    SEP);
		return sb.toString();
	} // toString

} // class
