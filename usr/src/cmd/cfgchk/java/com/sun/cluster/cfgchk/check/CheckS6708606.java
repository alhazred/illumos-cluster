/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)CheckS6708606.java	1.3	08/08/26 SMI"
 */
package com.sun.cluster.cfgchk.check;

import java.util.ArrayList;
import java.util.Iterator;

import com.sun.cluster.cfgchk.BasicCheck;
import com.sun.cluster.cfgchk.Check;
import com.sun.cluster.cfgchk.CheckExecutionException;
import com.sun.cluster.cfgchk.datasrc.ParseException;
import com.sun.cluster.cfgchk.datasrc.dataObject.Ifconfig;
import com.sun.cluster.cfgchk.datasrc.dataObject.IfconfigEntry;


/* BEGIN JSTYLED */
/**
 *
 * 6708606 - 2161 (Medium)
 *
 * Single node check.
 * Multiple network interfaces on a single subnet have the same MAC address.
 *
 */
/* END JSTYLED */

/*
 * No I18n of any strings in this class.
 * Exceptions thrown from Checks are caught and handled by Engine
 * typically just by logging. Messages are never displayed to user.
 *
 * Lines with trailing 'TOC' comment are auto-gathered into
 * table of contents listing of all checks.
 *
 * Text lines intended for output to report must not contain "<" in order
 * not to mess up the xml.
 */

public class CheckS6708606 extends BasicCheck {

	private Ifconfig ifconfig = null;
	private IfconfigEntry[] entries = null;
	private ArrayList dupAdapters = null;

	public CheckS6708606() {
		checkID = "S6708606"; // TOC
		severity = Check.MODERATE; // TOC

		// sccs keyword: must be 1.3 when checked out under sccs
		version = "1.3";
		// sccs keyword: must be 08/08/26 when checked out under sccs
		revisionDate = "09/12/08";

		keywords.add("SunCluster3");
		keywords.add("single");
		keywords.add("network");
		keywords.add("adapter");
		keywords.add("ifconfig");
		keywords.add("installtime");  // can't depend on clustermode

		/* BEGIN JSTYLED */
		problemStatement = "Multiple network interfaces on a single subnet have the same MAC address."; // TOC
		/* END JSTYLED */
		// check-specific data
		dupAdapters = new ArrayList();

	} // ()

	/**
	 * @return a string describing the logic of the applicability test
	 */
	protected String assignApplicabilityLogic() {
		/* BEGIN JSTYLED */
		String aLogic = "Scan output of '/usr/sbin/ifconfig -a' for more than one interface with an 'ether' line. Check does not apply if zero or only one ether line.";
		/* END JSTYLED */
		return aLogic;
	} // assignApplicabilityLogic


	/**
	 * @return a string describing the logic of the check
	 */
	protected String assignCheckLogic() {
		/* BEGIN JSTYLED */
		String cLogic = "Scan output of '/usr/sbin/ifconfig -a' for more than one interface with the same netmask, broadcast and ether values. If more than one is found, the check is violated.";
		/* END JSTYLED */
		return cLogic;
	} // assignCheckLogic


	public String getAnalysis() {
		/* BEGIN JSTYLED */
		StringBuffer analysis = new StringBuffer("");
		analysis.append("Most switches work at the hardware layer to send packets to the correct destination. If two or more Network Interface Cards (NICs) are on the same subnet and assigned the same MAC address, packet forwarding problems and network outages might occur.");
		analysis.append("\n");
		analysis.append("The following network adapters have the same MAC address:\n");
		Iterator it = dupAdapters.iterator();
		while(it.hasNext()) {
			int[] dups = (int[])it.next();

			IfconfigEntry e1 = entries[dups[0]];
			String eth1 = e1.getProperty("ether");
			String n1 = e1.getProperty("netmask");
			String b1 = e1.getProperty("broadcast");

			IfconfigEntry e2 = entries[dups[1]];
			String eth2 = e2.getProperty("ether");
			String n2 = e2.getProperty("netmask");
			String b2 = e2.getProperty("broadcast");

			analysis.append("\n\t" + e1.getName() + "\t" + eth1 + "\t" + n1 + "\t" + b1);
			analysis.append("\n\t" + e2.getName() + "\t" + eth2 + "\t" + n2 + "\t" + b2);
			analysis.append("\n");
		}
		/* END JSTYLED */
		return analysis.toString();
	} // getAnalysis

	public String getRecommendations() {
		/* BEGIN JSTYLED */
		String recommend = "Older NICs (le, hme, be, qe) derive their MAC addresses from the PROM on the system (see Infodoc 16733). Newer NICs (qfe, PCI hme, ge, ce, eri, dmfe) contain their own MAC addresses, but the system must be configured with the \"local-mac-address?\" parameter to make use of this MAC address.\n\nFor newer NICs, set local-mac-address? to true. Two methods exist: 1) from the ok prompt or 2) as root with the \"eeprom\" command. NOTE: Changing the local-mac-address? setting to true is not supported with Sun Cluster 2.x or 3.0 nodes.\n\n1) At the ok prompt: \n\nok&amp;gt; setenv local-mac-address? true \n\n2) With the eeprom command: \n\n# eeprom local-mac-address?=true \n\nFor older NICs (without an onboard MAC) or Sun Cluster 2.x and 3.0 nodes, the MAC address must be changed manually with the ifconfig command: \n\n# ifconfig le0 ether 0a:0:20:77:dc:7b\n\nThis MAC address is an example. You could also select a unique address from Valid address ranges x00000000001 to x0007FFFFFFF where x can be 4, 5, 6, or 7. It can be anything as long as it does not conflict with an existing MAC address on the network. Sun Microsystems uses reserved ethernet ranges of 08:00:20:#:#:# or 00:03:ba:#:#:# and assigns unique numbers for each OBP and NIC that support local-mac-address.\n\nDo not use an odd number (such as \"09:\") for the first byte due to the fact that if you are implementing multicasting, the 1st bit transmitted (\"individual/group\" bit) of a 1 represents a multicast address.\n\nThis change can be permanently added in /etc/rcS.d/S30network.sh (Solaris[TM] 8 and above) or in /etc/rcS.d/S30rootusr.sh (Solaris 7 and below).";
		/* END JSTYLED */

		return recommend;
	} // getRecommendations


	// Applies only if at least 2 ethernet adapters
	public boolean isApplicable() throws CheckExecutionException {
		logger.info("isApplicable() -- ENTER --");
		boolean applicable = false;

		// Applies only if at least 2 ethernet adapters
		// pull ifconfig data here and store for doCheck() as well

		try {
			ifconfig = new Ifconfig(getDataSrc(), logger);
			entries = ifconfig.getEntries();
				// may include null entries at end

			int numEthers = 0;
			logger.info("num entries: " + entries.length);
			for (int i = 0; i < entries.length; i++) {
				IfconfigEntry entry = entries[i];
				logger.finest("entry["+i+"]: >" + entry + "<");
				if (entry == null) {
					break;
				}
				if (entry.getProperty("ether") != null) {
					if (++numEthers > 1) {
						logger.finest("numEthers > 1");
						applicable = true;
						break; // bail out of loop
					}
				} // test ether
			} // for each ifconfig line

		} catch (ParseException e) {
			logger.warning("ParseException(); throwing " +
			    "CheckExecutionException: " + e.getMessage());
			throw new CheckExecutionException(e);
		} catch (Exception e) {
			logger.warning("Exception(); throwing " +
			    "CheckExecutionException: " + e.getMessage());
			throw new CheckExecutionException(e);
		}

		logger.info("EXIT: " + applicable);
		return applicable;
	} // isApplicable


	/*
	 * for each IfconfigEntry (loaded up in isApplicable())
	 * test its etherValue against remaining IfconfigEntries
	 * Any duplicate violates the check
	 */
	public boolean doCheck() throws CheckExecutionException {

		logger.info("-- ENTER --");
		boolean passes = true;

		try {
			IfconfigEntry[] entries = ifconfig.getEntries();
				// may include null entries at end

			logger.info("num entries: " + entries.length);
			for (int i = 0; i < entries.length; i++) {
				IfconfigEntry entry = entries[i];
				if (entry == null) {
					logger.fine("TESTING AGAINST " +
					    "loop is done");
					break;
				}
				logger.fine("TESTING AGAINST entry["+i+"]: >" +
				    entry.getName() + "<");
				logger.fine("entry 1: ["+i+"]: " + entry);

				String etherValue = entry.getProperty("ether");
				if (etherValue != null) {
					String broadcast =
					    entry.getProperty("broadcast");
					/* BEGIN JSTYLED */
					for (int j = i + 1;j < entries.length; j++) {
						IfconfigEntry entry2 =
						    entries[j];
						logger.fine(" entry 2: ["+j+"]: " +
						    entry2);

						if (entry2 == null) {
							logger.fine("** TESTING AGAINST " +
							    "inner loop is done");
							break;
						}
						String etherValue2 = entry2.getProperty("ether");
						String netmask2 = entry2.getProperty("netmask");
						String broadcast2 = entry2.getProperty("broadcast");

						// the heart of the matter
						// OK for entry 2 values to be null
						if (etherValue.equals(etherValue2) &&
						    netmask2.equals(netmask2) &&
						    broadcast.equals(broadcast2)
						) {
							logger.info("duplicate");
							passes = false;
							int[] dups = { i, j };
								// store indices of dups
							dupAdapters.add(dups);
							// not breaking: get all possible
							//  errors not just first one
						} else {
							logger.fine("not a duplicate");
						}
					} // for entries
					/* END JSTYLED */
				} // got etherValue
				else {
					logger.info("no ether value; skipping");
				}
			} // for each ifconfig line

		} catch (Exception e) {
			logger.warning("Exception(): " + e.getMessage());
			throw new CheckExecutionException(e);
		}
		logger.info("-- EXIT passes: " + passes);
		return passes;
	} // doCheck

} // class

