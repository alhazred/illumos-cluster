/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)ExecUtil.java	1.2	08/05/29 SMI"
 */
package com.sun.cluster.cfgchk.agent.cfgchk.exec;

// JDK
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.PrivilegedAction;
import java.util.logging.Logger;

import javax.management.ObjectName;
import javax.security.auth.Subject;

import com.sun.cacao.ObjectNameFactory;
import com.sun.cacao.invocation.InvocationException;
import com.sun.cacao.invocation.InvocationStatus;
import com.sun.cacao.invocation.InvokeCommand;


/*
 * This mbean is always available on each node.
 *
 * Functionality in here is available to be executed on a
 * specified node of a cluster.
 *
 * Services available:
 *
 *      public String getNodeName()
 *      public InvocationStatus cacaoExecuteCommand(String[] cmdArray)
 *	public InvocationStatus directExecuteCommand(Logger logger,
 *		String[] cmdArray)
 */


/*
 * Inner classes:
 *
 * Need to execute the command in a separate
 * thread in order to be able to force a timeout.
 * Therefore, there is a private interface, RunnableCommand,
 * and two implementations which are run as Threads:
 *	InvokeCommandCacao
 *	InvokeCommandDirect
 *
 * Each can return the results of command execution:
 *	InvocationStatus
 *	InvocationException
 *
 */

public class ExecUtil implements ExecUtilMBean, PrivilegedAction {

	private String nodeName;
	private ObjectNameFactory onf;
	private javax.security.auth.Subject rootSubject;

	private static Logger mLogger =
		Logger.getLogger(
			"com.sun.cluster.cfgchk.agent.cfgchk.exec.ExecUtil");

	/*
	 * can't use RemoteLogger because we're used to init RemoteLogger
	 * at CfgchkModule start time
	 */

	// ===========================================

	private Logger logger = null;
	private String[] cmdArray = null;

	// constructor for use without cacao
	public ExecUtil() {
		nodeName = "localhost";
	} // ()

	// constructor for use with cacao
	public ExecUtil(ObjectNameFactory onf, String nodeName) {
		this.onf = onf;
		this.nodeName = nodeName;

		java.security.AccessControlContext acc =
			java.security.AccessController.getContext();
		rootSubject =
			javax.security.auth.Subject.getSubject(acc);
	} // ()


	// ===============  ExecUtilMBean interface


	/**
	 * getNodeName
	 */
	public String getNodeName() {
		mLogger.info("-- ENTER --");
		return nodeName;
	}

	/**
	 * directExecuteCommand
	 *
	 * @param String[]: cmdArray (already contains locale specification)
	 *
	 * @return InvocationStatus   : command was executed even if
	 * returns non-zero
	 *         InvocationException: command couldn't be executed
       	 */
	public InvocationStatus directExecuteCommand(Logger logger,
	    String[] cmdArray)
		throws InvocationException {
		if (logger == null) logger = mLogger;
		logger.info("-- ENTER -- ");

		/*
		 * cmdArray loaded by CommandInvoker.execute() and
		 * expected to hold:
		 * [0]: "/bin/sh";   // run under basic shell
		 * [1]: "-c";	     // following string contains commands
		 * [2]: target command
		 */
		if (cmdArray == null || cmdArray.length < 3) {
			throw new InvocationException(
				"cmdArray null or insufficient length");
		}
		this.cmdArray = cmdArray;

		logger.info("cmdArray[2]: " + cmdArray[2]);
		InvocationStatus status = null;

		try {
			status = runDirectInvokeCommand(logger);
		} catch (InvocationException iex) {
			logger.severe("InvocationException: " +
			    iex.getMessage());
			throw(iex);
		}
		logger.info("-- EXIT --");
		return status;
	} // directExecuteCommand


	/**
	 * cacaoExecuteCommand
	 *
	 * @param String[]: command (already contains locale specification)
	 *
	 * @return InvocationStatus  : OK
	 *         InvocationException : command failed
       	 */
	public InvocationStatus cacaoExecuteCommand(String[] cmdArray)
		throws InvocationException {
		mLogger.info("-- ENTER sig 1 --");
		return cacaoExecuteCommand(mLogger, cmdArray);
	}
	public InvocationStatus cacaoExecuteCommand(Logger logger,
	    String[] cmdArray)
		throws InvocationException {
		if (logger == null) logger = mLogger;
		this.logger = logger; // for use by this.run()
		logger.info("-- ENTER sig 2 -- ");

		/*
		 * cmdArray loaded by CommandInvoker.execute() and
		 *  expected to hold:
		 * [0]: "/bin/sh";   // run under basic shell
		 * [1]: "-c";	     // following string contains commands
		 * [2]: target command
		 */
		if (cmdArray == null || cmdArray.length < 3) {
			throw new InvocationException(
				"cmdArray null or insufficient length");
		}
		this.cmdArray = cmdArray;

		logger.info("cmdArray[2]: " + cmdArray[2]);

		try {
			// here's the action: Subject.doAs()
			Object ret = Subject.doAs(rootSubject, this);
				// control now passes to this.run()

			logger.info("back from doAs()");

			if (ret instanceof InvocationStatus) {
				logger.info("got InvocationStatus");
				return (InvocationStatus)ret; // early exit

			} else if (ret instanceof InvocationException) {
				logger.severe("got InvocationException: " +
				    ret);
				throw(InvocationException)ret;
			} else {
				logger.severe("unexpected return type from " +
				    "doAs(); throwing InvocationException");
				throw new InvocationException(
					"unexpected return type"); // early exit
			}

		} catch (SecurityException ex) {
			logger.warning("SecurityException on: " + cmdArray[2] +
			    "\n" + ex.toString());

			InvocationException ie = new InvocationException(
				"Security exception : " +
			    ex.toString());
			throw(ie);
		}
		// should never get here
	} // cacaoExecuteCommand

	/*
	 * mbean methods called by CfgchkModule
	 * (not Thread methods)
	 * no real work done by this mbean; just
	 * log the fact that we're through these transitions
	 */

	public void start(Logger logger) {
		if (logger == null) logger = mLogger;
		logger.info("starting ExecUtil");
	}

	public void stop(Logger logger) {
		if (logger == null) logger = mLogger;
		logger.info("stopping ExecUtil");
	}

	/**
	 * create the object name for the given operation
	 * object name (this method is called when the mbean for
	 * this object is registered with the MBean server).
	 *
	 * @return the objectName to use
	 */
	public ObjectName createON() {
		ObjectName on = onf.getObjectName(ExecUtilMBean.class,
		    null);
		return on;
	} // createON


	// ===================== end of mbean interface =====================


	// ===================== start interface PrivilegedAction  ==========

		/*
		 * entry point for Subject.doAs(): under cacao only
		 * this is not an implementation of Runnable
		 *
		 * returns either InvocationStatus
		 * or InvocationException
		 * caller must test to see what it has received
		 */
		public Object run() {
			logger.info("-- ENTER --");
			InvocationStatus es = null;
			try {
				// all args passed as globals
				es = runCacaoInvokeCommand();
			} catch (InvocationException ex) {
				logger.severe(
					"returning InvocationException: " + ex);
				return ex;
			}
			logger.info("returning InvocationStatus: " + es);
			return es;
		} // run

	// ===================== end interface PrivilegedAction  =============

		/*
		 * Two ways to invoke a command:
		 *
		 * runDirectInvokeCommand() executes a process directly on the
		 *	 OS on localnode and returns a created IS.
		 *
		 * runCacaoInvokeCommand() uses cacao's InvokeCommand class.
		 *
		 * runDirectInvokeCommand() is for use without cacao.
		 * It runs Process directly and throws an IE only if execution
		 * cannot be launched.
		 *
		 * runCacaoInvokeCommand() also returns only an IS by extracting
		 *	the IS from the IE that cacao returns if the invoked
		 *	cmd returns non-0.
		 *
		 * Both forms of execution are run in a thread with
		 * a monitoring timeout
		 * so we can abort if an executed command hangs
		 */

		private InvocationStatus runCacaoInvokeCommand()
			throws InvocationException  {
			logger.info("-- ENTER --");

			InvocationStatus es = null;
			InvokeCommandCacao icc = null;
			String cmd = cmdArray[2];

			icc = new InvokeCommandCacao(logger, cmdArray);

			try {
				es = runCommandThread(logger, cmd, icc);
			} catch (InvocationException iex) {
					// this is not timeout
				logger.severe("InvocationException: " + iex);
				throw(iex);
			}
			logger.info("result InvocationStatus: " + es);
			logger.info("-- EXIT --");
			return es;
		} // runCacaoInvokeCommand

		private InvocationStatus runDirectInvokeCommand(Logger logger)
			throws InvocationException {
			logger.info("-- ENTER --");

			InvocationStatus es = null;
			InvokeCommandDirect icd = null;
			String cmd = cmdArray[2];

			icd = new InvokeCommandDirect(logger, cmdArray);

			try {
				es = runCommandThread(logger, cmd, icd);
			} catch (InvocationException iex) {
					// this is not timeout
				logger.severe("InvocationException: " + iex);
				throw(iex);
			}

			logger.info("InvocationStatus: " + es);
			logger.info("-- EXIT --");
			return es;
		} // runDirectInvokeCommand

		/*
		 * run the actual command execution in a thread
		 * so we can force a timeout on command hang
		 * may throw InvocationException but will
		 * never return a null InvocationStatus
		 */
		public InvocationStatus runCommandThread(Logger logger,
		    String cmdName, RunnableCommand runnableCmd)
			throws InvocationException {
			logger.info("-- ENTER --");
			InvocationStatus es = null;
			InvocationException ie = null;
			InvokeCommandDirect idc = null;
			boolean interrupted = false;
			String cmd = cmdArray[2];


			for (int i = 0; i < cmdArray.length; i++) {
				logger.finest("[ExecUtil.ExecUtilCmd() " +
				    "cmdArray["+i+"]: " + cmdArray[i]);
			}

			logger.info(" ------------  STARTING WAIT FOR " +
			    "RunnableCommand THREAD: " + cmdName);
			Thread execThread = new Thread(runnableCmd);
			execThread.start();

			// wait for thread to complete
			// but not for more than timeout msec
			try {
				int timeout = 2 * 60 * 1000; // 2 minutes
				execThread.join(timeout);
				es = runnableCmd.getExitStatus();
				ie = runnableCmd.getInvocationException();
				if (es == null && ie == null) {
					/* BEGIN JSTYLED */
					logger.severe("both exitStatus & invocationException are null; forcing invocationException: " + cmdName);
					/* END JSTYLED */
					ie = new InvocationException(
						"command execution " +
							"returns null: " +
							cmdName);
				}

			} catch (InterruptedException intex) {
					// this is not timeout
				logger.severe("InterruptedException");
				interrupted = true;
				es = null;
				ie = new InvocationException("command " +
				    "execution thread interrupted: " + cmdName);
			}

			if (execThread.isAlive() && !interrupted) {
				logger.severe("timeout waiting for " +
				    "command execution thread");
				execThread.interrupt();
				es = null;
				ie = new InvocationException(
					"command execution thread timedout: " +
						cmdName);
			}

			logger.info(" ------------  DONE WAIT FOR " +
			    "RunnableCommand THREAD: " + cmdName);

			logger.info("result InvocationStatus: " + es);
			logger.info("result InvocationException: " + ie);

			if (ie != null) {
				throw ie;
			} // else
			logger.info("-- EXIT --");
			return es;
		} // runCommandThread


	// =====  private classes  =====


	private interface RunnableCommand extends Runnable {
		public InvocationStatus getExitStatus();
		public InvocationException getInvocationException();

	} // private interface RunnableCommand


	private class InvokeCommandCacao extends Thread
		implements RunnableCommand {

		// 'return' result
		private InvocationStatus exitStatus = null;

		// to be set
		private Logger logger = null;
		private String[] cmdArray;

		public InvokeCommandCacao(Logger logger, String[] cmdArray) {
			this.logger = logger;
			this.cmdArray = cmdArray;
			logger.fine("<init>");
		} // ()

		public InvocationStatus getExitStatus() {
			return exitStatus;
		}
		public InvocationException getInvocationException() {
			return null; // this implementation never returns InvEx
		}

		/*
		 * Always returns InvocationStatus.
		 *
		 * This implementation uses cacao's InvokeCommand
		 * which throws IE on non-zero return from cmd it's invoking.
		 * Extract the IS from the IE and return the IS.
		 *
		 * Do not return IE on non-zero return from target command.
		 */

		// Thread
		public void run() {
			logger.fine("-- ENTER --");

			this.exitStatus = null;


			try {
				// call cacao's method
				this.exitStatus =
				    InvokeCommand.execute(cmdArray, null);
				logger.finest("exitStatus: " +
				    exitStatus.toString());
				String cmdOutput = "\n";
				cmdOutput += exitStatus.getStdout();
				logger.fine("cmdOutput " + cmdOutput);

				int returnCode =
				    exitStatus.getExitValue().intValue();
				logger.fine("returnCode " + returnCode);

			} catch (InvocationException ex) {
				// typically non-0 exit of executed command
				// not a crash in the invocation mechanism
				logger.fine("got InvocationException: " + ex);

				// this array should always be size 1
				InvocationStatus[] exitStatusA =
				    ex.getInvocationStatusArray();
				// but just in case...
				for (int k = 0; k < exitStatusA.length; k++) {
					logger.fine("InvocationException " +
					    "exitStatusA["+k+"]: " +
					    exitStatusA[k].toString());
				}

				// force an InvocationStatus exit
				// with the InvocationStatus extracted from ex
				this.exitStatus = exitStatusA[0];
				logger.fine("setting exitStatus from " +
				    "InvocationException: " + this.exitStatus);
			}

			logger.fine("-- EXIT --");
		} // run

	} // private class InvokeCommandCacao


	private class InvokeCommandDirect extends Thread
		implements RunnableCommand {
		/*
		 * without cacao
		 * return InvocationStatus
		 * InvocationException only if framework throws exception
		 */

		// on exit one of these will be null and the other non-null
		private InvocationStatus exitStatus = null;
		private InvocationException invocationException = null;

		// to be set
		public String[] cmdArray;
		private Logger logger = null;

		public InvokeCommandDirect(Logger logger, String[] cmdArray) {
			this.logger = logger;
			this.cmdArray = cmdArray;
			logger.fine("<init>");
		} // ()

		public InvocationStatus getExitStatus() {
			return exitStatus;
		}
		public InvocationException getInvocationException() {
			return invocationException;
		}

		// Thread
		public void run() {
			logger.fine("-- ENTER --");


			int returnCode = 0;
			String line = null;
			StringBuffer cmdOutput = new StringBuffer("");
			StringBuffer errOutput = new StringBuffer("");

			try {
				logger.fine("launching exec");
				Process child =
				    Runtime.getRuntime().exec(cmdArray);
				logger.fine("launched exec");
				logger.fine("got return code " + returnCode);

				// stdOut of process is our input stream
				InputStream stdOut = child.getInputStream();
				InputStreamReader inR =
				    new InputStreamReader(stdOut);
				BufferedReader bR = new BufferedReader(inR);

				logger.finest("starting read stdout");
				while ((line = bR.readLine()) != null) {
					logger.finest(
						"output >>" + line +"<<");
					cmdOutput.append(line + "\n");
				}
				logger.finest("end reading stdout");

				// stdErr of process is our input stream
				InputStream stdErr = child.getErrorStream();
				inR = new InputStreamReader(stdErr);
				bR =  new BufferedReader(inR);

				logger.fine("starting read stderr");
				while ((line = bR.readLine()) != null) {
					logger.finest("err >>" + line + "<<");
					errOutput.append(line + "\n");
				}
				logger.finest("end reading stderr");
				logger.fine("waiting...");

				returnCode = child.waitFor();
				logger.fine("below wait: " + returnCode);


				// create InvocationStatus for caller to fetch
				exitStatus =
				    new InvocationStatus(cmdArray, null,
					returnCode, null, cmdOutput.toString(),
					errOutput.toString());
				exitStatus.setArgv(cmdArray);
					// workaround cacao init bug: 6410164
				logger.finest("exitStatus: " +
				    exitStatus.toString());
				logger.fine("exitValue(): " +
				    exitStatus.getExitValue());

			} catch (IOException ioex) { // getRuntime(); readLine()
				logger.severe("IOException: " +
				    ioex.getMessage());
				invocationException = new InvocationException(
					ioex.toString());

			} catch (Exception ex) {
					logger.severe("Exception: " +
					    ex.getMessage());
				invocationException = new InvocationException(
					ex.toString());
			} // try/catch


			if (exitStatus != null) {
				logger.fine("returning InvocationStatus");
			} else {
				logger.warning("throwing InvocationException");
			}
		} // run

	} // private class InvokeCommandDirect


	// =====   end inner classes   =====

} // class
