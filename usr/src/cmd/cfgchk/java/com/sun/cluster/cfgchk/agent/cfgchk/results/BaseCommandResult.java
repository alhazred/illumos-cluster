/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/* 
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)BaseCommandResult.java	1.2	08/05/29 SMI"
 */
package com.sun.cluster.cfgchk.agent.cfgchk.results;

// JDK Imports.
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.sun.cluster.cfgchk.Globals;

/**
 * Base implementation class for CommandResult object. Is subclassed to
 * define command specific result.
 *
 * Implements the following interfaces:
 * <ul>
 *  <li ><code>Serializable </code> in order to be send through JMX
 *  connections</li>
 *  <li ><code>CommandResult</code> the generic command result interface
 *  </li>
 *  <li><code>Globals</code> to use I18N framework.</li>
 * </ul>
 */
public class BaseCommandResult
	implements CommandResult, Globals, Serializable {

    private static final long serialVersionUID = 3626895951320759212L;
    
    private int returnCode = ExitCode.OK;
	private ArrayList messages = null;
	/**
	 * Default constructor
	 */
	public BaseCommandResult() {
		this(ExitCode.OK, null);
	}
	
	/**
	 * Constructor.
	 *
	 * @param returnCode command invocation return code.
	 */
	public BaseCommandResult(int returnCode) {
		this(returnCode, null);
	}

	/**
	 * Constructor.
	 *
	 * @param returnCode command invocation return code.
	 * @param msg first of possibly several messages to store
	 */
	public BaseCommandResult(int returnCode,
	    String msg) {

		this.returnCode = returnCode;
		messages = new ArrayList();
		if (msg != null) {
			setMessage(msg);
		}
	} // ()

	public String getMessage() {
		return getMessage(0);
	}

	public String getMessage(int i) {

		String msg = "no message";
		if (messages != null && messages.size() > i) {
			msg = (String)messages.get(i);
		}
		return msg;
	}

    /**
     * Message getter
     *
     * @return the list of messages of the current object
     */
	public List getMessages() {
		return messages;
	}

   	public void setMessages(ArrayList msgs) {
		messages = msgs;
	}

	public void setMessage(String msg) {
		messages.add(msg);
	}

	/**
	 * Exit code setter
	 *
	 * @param ec the new exit code value.
	 */
	public void setExitCode(int ec) {	
		returnCode = ec;
	}


	/**
	 * Exit code getter
	 *
	 * @return the exit code field of the current object.
	 */
	public int getExitCode() {
		return returnCode;
	}


	/**
	 * String serialization form.
	 *
	 * @return the string form of the current object.
	 */
	public String toString() {
		StringBuffer sb = new StringBuffer(getExitCode() + ": ");
		boolean firstMsg = true;
		List errMsgs = getMessages();
		Iterator it = errMsgs.iterator();
		while (it.hasNext()) {
			if (!firstMsg) sb.append(" | ");
			String msg = (String)it.next();
			sb.append(msg);
			firstMsg = false;
		}
		return sb.toString();
	} // toString

} // class
