/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)CfgchkProperties.java	1.2	08/05/29 SMI"
 */

package com.sun.cluster.cfgchk.utils;

import java.util.Properties;
import java.io.*;

/*
 * This Properties class enables a JVM-single Properties object
 * loaded from a named properties file.
 *
 * Convenience routines for returning typed values.
 *
 * SINGLETON
 *
 */
public class CfgchkProperties extends Properties {

    private static final long serialVersionUID = 3979181746329698072L;
    private static CfgchkProperties props = null;


	// -- create and fetch the Properties object

	public static CfgchkProperties getCfgchkProperties() {
		return props;
	} // getCfgchkProperties()

	public static CfgchkProperties getCfgchkProperties(String s)
		throws IOException {
		return getCfgchkProperties(s, false);
	}

	public static CfgchkProperties getCfgchkProperties(String s,
	    boolean forceReload)
		throws IOException {
		if (props == null || forceReload) {
			props = new CfgchkProperties(s);
		}
		return props;
	} // getCfgchkProperties()


	// Constructor: create properties from a String filename
	public CfgchkProperties(String s) throws IOException {
		super();
		File source = new File(s);
		InputStream in = new BufferedInputStream(
		    new FileInputStream(source.getPath()));
		load(in);
		in.close();
	} // ()

	// empty constructor used if filename fails
	public CfgchkProperties() { }


	// -- fetch typed values from Properties

	public static int intFromProps(String key, int defaultIntVal,
	    CfgchkProperties pr) {

	    if (pr == null) {
		    return defaultIntVal;
	    }
	    Integer x = new Integer(defaultIntVal);
	    try {
		    x = Integer.decode(
			pr.getProperty(key, new Integer(
			    defaultIntVal).toString()));
	    } catch (NumberFormatException e) {
		    return defaultIntVal;
	    }
	    return x.intValue();
    } // intFromProps


    public static long longFromProps(String key, long defaultLongVal,
	CfgchkProperties pr) {

	    if (pr == null) {
		    return defaultLongVal;
	    }
	    Long x = new Long(defaultLongVal);
	    try {
		    x = Long.valueOf(
			pr.getProperty(key, new Long(
			    defaultLongVal).toString()));
	    } catch (NumberFormatException e) {
		    return defaultLongVal;

	    }
	    return x.longValue();
    } // longFromProps


    public static String stringFromProps(String key,
	String defaultStringVal, CfgchkProperties pr) {

	    if (pr == null) {
		    return defaultStringVal;
	    }
	    String s = String.valueOf(pr.getProperty(key,
		new String(defaultStringVal)));
	    return s;
    } // stringFromProps


    public static boolean booleanFromProps(String key, boolean defaultBoolVal,
	CfgchkProperties pr) {
	    if (pr == null) {
		    return defaultBoolVal;
	    }
	    Boolean bool = Boolean.valueOf(
		pr.getProperty(key, new Boolean(defaultBoolVal).toString()));
	    return bool.booleanValue();
    } // booleanFromProps

} // CfgchkProperties
