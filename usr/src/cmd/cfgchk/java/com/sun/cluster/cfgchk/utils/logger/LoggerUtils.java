/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)LoggerUtils.java	1.3	08/09/22 SMI"
 */
package com.sun.cluster.cfgchk.utils.logger;

import java.io.File;
import java.io.IOException;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Formatter;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.sun.cluster.cfgchk.InvalidParameterException;
import com.sun.cluster.cfgchk.utils.ErrorMsg;

/*
 * Utilities for creating logs and providing
 * various logging services.
 *
 * The very few exceptions thrown from this class
 * are presented as ErrorMsg's and are bubbled
 * up to the user.
 *
 * The .lck files that are created will be removed
 * when the underlying files are closed or the Logger
 * that owns them expires.
 */
public class LoggerUtils {

	// custom presentation in the log of an exception
	public static void exception(String message, Throwable ex,
	    Logger logger) {
		if (logger == null) {
			return;
		}

		// don't access ex before testing for null
		logger.info(" *** exception() START ------------  ***");
		StringBuffer sb = new StringBuffer("");

		if (message != null && message.length() > 0) {
			logger.info(message);
		}

		if (ex == null) {
			logger.info("exception is null");
		} else {
			logger.info("message: " + ex.getMessage());
			StackTraceElement elements[] = ex.getStackTrace();
			int n = elements.length;
			for (int i = 0; i < n; i++) {
				sb.append("\t" + elements[i].toString() + "\n");
			}
			logger.info(sb.toString());
		}
		logger.info(" *** exception() END ------------  ***");
	} // exception

	public static FileHandler initTimestampFileHandler(String fname,
	    boolean rotatable) throws InvalidParameterException {
		Formatter f = new TimestampFormatter();
		return initFileHandler(fname, f, rotatable);
	}

	public static FileHandler initTextFileHandler(String fname,
	    boolean rotatable) throws InvalidParameterException {
		Formatter f = new TextFormatter();
		return initFileHandler(fname, f, rotatable);

	}

	private static FileHandler initFileHandler(
	    String fname, Formatter formatter, boolean rotatable)
		throws InvalidParameterException {

		FileHandler handler = null;

		try {
			// making File in order to call mkdirs()
			File f = new File(fname);
			File logdir = f.getParentFile();
			logdir.mkdirs();

			String pattern = "";
			if (rotatable) {
				pattern = ".%g"; // add "generation" numbers
			}
			String handlerName = fname + pattern;
			if (rotatable) {
				handler = new FileHandler(
					handlerName, 10000000, 5);
				// max size approx 10MB; 5 files rotating
			} else {
				boolean append = true;
				handler = new FileHandler(handlerName, append);
				// max size unlimited; 1 file not rotating
	}
			handler.setFormatter(formatter);

		} catch (SecurityException e) {
			String[] i18nArgs = { fname, e.getMessage() };
			String lMsg = ErrorMsg.makeErrorMsg(null,
			    "logger.insufficient.permissions", i18nArgs);
			throw new InvalidParameterException(lMsg);
		} catch (IOException e) {
			String[] i18nArgs = { fname, e.getMessage() };
			String lMsg = ErrorMsg.makeErrorMsg(null,
			    "logger.ioexception", i18nArgs);
			throw new InvalidParameterException(lMsg);
		}
		return handler;
	} // initFileHandler


	// these strings "session" and "remote" could be
	// "com.sun.cluster.cfgchk.classFoo" XXX
	// then all the classes that want loggers use the static call?
	// do I need to separate the hierarchy namespaces between session
	// and remote targets? XXX
	public static Logger initSessionLogger(FileHandler handler,
	    String loggingLevel) {
		return initLogger("com.sun.cluster.cfgchk.session", handler,
		    loggingLevel);
	} // initSessionLogger

	public static Logger initRemoteLogger(FileHandler handler,
	    String loggingLevel) {
		return initLogger("com.sun.cluster.cfgchk.remote", handler,
		    loggingLevel);
	} // initRemoteLogger

	private static Logger initLogger(String s, FileHandler handler,
	    String loggingLevel) {
		Logger logger = Logger.getLogger(s);
		logger.setUseParentHandlers(false);

		logger.setLevel(Level.parse(loggingLevel));
		logger.addHandler(handler);

		return logger;
	} // initLogger

	public static void setLoggingLevel(Logger logger, String newLevel) {
		logger.info("setting logging level to: " + newLevel);
		logger.setLevel(Level.parse(newLevel));
	} // setLoggingLevel

	// swap out the file underneath a given logger
	public static void swapFileHandlers(Logger logger, FileHandler fhOld,
	    FileHandler fhNew) {
		logger.removeHandler(fhOld);
		logger.addHandler(fhNew);
	} // swapFileHandlers

	public static void addConsoleHandler(Logger logger) {
		// echoes to originating windows
		logger.info("-- ENTER --");
		ConsoleHandler ch = new ConsoleHandler();
		System.out.println("ADDING CONSOLE LOGGER ************"); // XXX
		logger.addHandler(ch);
		logger.info("-- EXIT --");
	} // addConsoleHandler

} // class
