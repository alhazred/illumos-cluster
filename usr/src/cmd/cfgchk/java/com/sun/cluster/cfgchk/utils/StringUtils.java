/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)StringUtils.java	1.4	08/09/07 SMI"
 */
package com.sun.cluster.cfgchk.utils;

// JDK
import java.util.ArrayList;
import java.util.Iterator;
import java.util.StringTokenizer;
import java.util.logging.Logger;

import com.sun.cluster.cfgchk.Globals;
import com.sun.cluster.cfgchk.datasrc.DataException;

/*
 * Misc utils that act on Strings in various ways:
 *	getNumFields()
 *	getField()
 *	getFieldsXthroughY()
 *	fillDimensionString()
 *	padString()
 *	subtractStringArrays()
 *	indexOfStringInStringArray()
 *	strip()
 *	encodedStringFromStringArray()
 *	stringArrayFromEncodedString()
 *	intFromString()
 *	stringArrayToString()
 *	dumpStringArray()
 *	forceTrailingChar()
 */

public class StringUtils {

	public static String WHITESPACE = new String(" " + "\t" + "\n");
	public static int PAD_RIGHT = 0;
	public static int PAD_LEFT  = 1;


	/*
	 * field numbering in this interface is 1-based
	 */
	public static String getField(String line, int fieldNum)
		throws DataException {
		return StringUtils.getField(line, fieldNum,
		    StringUtils.WHITESPACE);
	} // getField

	public static String getField(String line, int fieldNum, String SEPs)
		throws DataException {

		if (fieldNum < 1) {
			throw new DataException(
				"field number must be 1 or greater");
		}
                StringTokenizer st = new StringTokenizer(line, SEPs);
		if (st.countTokens() < fieldNum) {
			throw new DataException("String contains less than " +
			    fieldNum + " fields: " + line);
		}
		for (int i = 1; i < fieldNum; i++) { // 1-based index
                        st.nextToken(); // ignore
		}
		return st.nextToken();
	} // getField

	public static String getFieldsXthroughY(String line, int startField,
	    int endField, String glueStr) throws DataException {
		return getFieldsXthroughY(line, startField, endField,
		    StringUtils.WHITESPACE, " ");
	} // getFieldsXthroughY

	public static String getFieldsXthroughY(String line, int startField,
	    int endField, String SEPs, String glueStr)
		throws DataException {
		StringBuffer result = new StringBuffer("");

		if (startField < 1) {
			throw new DataException(
				"start field number must be 1 or greater");
		}
                StringTokenizer st = new StringTokenizer(line, SEPs);
		if (st.countTokens() < endField) {
			throw new DataException("String contains less than " +
			    endField + " fields: " + line);
		}
		for (int i = 1; i < startField; i++) {
                        st.nextToken(); // ignore
		}
		for (int i = startField; i <= endField; i++) {
				// include endField
			if (result.length() > 0) {
				result.append(glueStr);
			}
                        result.append(st.nextToken()); // keep
		}
		return result.toString();
	} // getFieldsXthroughY

	public static int getNumFields(String line) throws DataException {
		return StringUtils.getNumFields(line, StringUtils.WHITESPACE);
	} // getNumFields

	public static int getNumFields(String line, String SEPs)
		throws DataException {
                StringTokenizer st = new StringTokenizer(line, SEPs);
		return st.countTokens();
	} // getNumFields

	public static String fillDimensionString(int size, char ch) {
		StringBuffer sb = new StringBuffer(size);
		for (int i = 0; i < size; i++) {
			sb.append(ch);
		}
		return sb.toString();
	} // fillDimensionString


	public static String padString(String str, int size) {
		return padString(str, size, ' ', PAD_RIGHT);
	} // padString

	public static String padString(String str, int size, int padSide) {
		return padString(str, size, ' ', padSide);
	} // padString

	public static String padString(String str, int size, char ch) {
		return padString(str, size, ch, PAD_RIGHT);
	} // padString


	public static String padString(String str, int size, char ch,
	    int padSide) {
		if (str == null) str = "";
		StringBuffer sb = new StringBuffer(str);
		for (int i = sb.length(); i < size; i++) {
			if (padSide == PAD_RIGHT) {
				sb.append(ch);
			} else {
				sb.insert(0, ch);
			}
		}
		return sb.toString();
	} // padString

	/*
	 * subtract a2 from a1
	 * may return size zero but never null
	 */
	public static String[] subtractStringArrays(String[] a1, String[] a2) {
		ArrayList aL1 = new ArrayList();
		ArrayList aL2 = new ArrayList();

		if (a1 == null) {
			return new String[0];
		}
		if (a2 == null) {
			return a1;
		}

		for (int i = 0; i < a1.length; i++) { aL1.add(a1[i]); }
		for (int i = 0; i < a2.length; i++) { aL2.add(a2[i]); }

		aL1.removeAll(aL2);
		String[] result = new String[aL1.size()];
		for (int i = 0; i < aL1.size(); i++) {
			result[i] = (String)aL1.get(i);
		}
		return result;
	} // subtractStringArrays


	// defaults to 'precise'
	public static int indexOfStringInStringArray(String target,
	    String[] strArry) {
		return indexOfStringInStringArray(target, strArry, true);
	}

	/**
	 * return index of string in array that matches
	 * target string
	 * if precise is true then test with 'equals'
	 * if precise is false then test with 'contains'
	 */
	public static int indexOfStringInStringArray(String target,
	    String[] strArry, boolean precise) {
		int index = -1;
		if (strArry != null) {
			for (int i = 0; i < strArry.length; i++) {
				if (precise == true) {
					if (target.equals(strArry[i])) {
						index = i;
						break;
					}
				} else {
					if (target.indexOf(strArry[i]) > -1) {
						index = i;
						break;
					}
				}
			}
		}
		return index;
	} // indexOfString

	/**
	 * @param src the string to remove something from
	 * @param remove the string to be removed from src
	 */
	public static String strip(String src, String remove) {
		return src.replaceAll(remove, "");
	} // strip


	// supplies comma as default separator
	public static String encodedStringFromStringArray(String[] strArry) {
		Logger logger = null;
		String SEP = Globals.LIST_ENCODING_SEP;
		return StringUtils.encodedStringFromStringArray(logger,
		    strArry, SEP);
	}

	public static String encodedStringFromStringArray(String[] strArry,
	    String SEP) {
		Logger logger = null;
		return StringUtils.encodedStringFromStringArray(logger,
		    strArry, SEP);
	}

	/*
	 * given a String[] return a single String of strArry
	 * elements separated by SEP
	 */
	public static String encodedStringFromStringArray(Logger logger,
	    String[] strArry, String SEP) {
		if (logger != null) logger.finest("-- ENTER --");
		StringBuffer sb = new StringBuffer("");
		for (int i = 0; i < strArry.length; i++) {
			if (sb.length() > 0) {
				sb.append(SEP);
			}
			sb.append(strArry[i]);
		}
		String str = sb.toString();
		if (logger != null) logger.finest("-- EXIT: "+ str);
		return str;
	} // encodedStringFromStringArray

	public static String[] stringArrayFromEncodedString(
			String encodedString) {
		Logger logger = null;
		String SEP = " ";
		return StringUtils.stringArrayFromEncodedString(logger,
		    encodedString, SEP);
	}
	public static String[] stringArrayFromEncodedString(
			String encodedString, String SEP) {
		Logger logger = null;
		return StringUtils.stringArrayFromEncodedString(logger,
		    encodedString, SEP);
	}

	// if input String is null, returns null
	public static String[] stringArrayFromEncodedString(Logger logger,
	    String encodedString, String SEP) {
		if (encodedString == null) {
			if (logger != null) {
				logger.finest("encodedString == null");
			}
			return null;
		}

                StringTokenizer st = new StringTokenizer(encodedString, SEP);
		int numTokens = st.countTokens();
		String[] strarry = new String[numTokens];
		int index = 0;
		while (st.hasMoreTokens()) {
			String tok = st.nextToken();

			if (logger != null) {
				logger.finest("tok: " + tok);
			}

			strarry[index++] = tok;
		}
		return strarry;

	} // stringArrayFromEncodedString

	public static String[] stringArrayFromArrayList(Logger logger,
	    ArrayList alist) {
		int numEntries = alist.size();
		String[] strArray = new String[numEntries];

		for (int i = 0; i < numEntries; i++) {
			strArray[i] = (String)alist.get(i);
		}
		return strArray;
	} // stringArrayFromArrayList

	public static int intFromString(String val, int defaultIntVal) {
		Integer x = new Integer(defaultIntVal);
		try {
			x = Integer.decode(val);
		} catch (NumberFormatException e) {
			return defaultIntVal;
		}
		return x.intValue();
	} // intFromString


	// default to space separator
	public static String stringArrayToString(String[] strArray) {
		return stringArrayToString(strArray, " ");
	} // stringArrayToString

	/*
	 * Given an array of Strings, return a String composed of SEP separated
	 * elements of array. A null or empty input string results in "" being
	 * inserted for that element. Given a null array, return empty
	 * String ("" not null). Very similar to
	 * encodedStringFromStringArray() but has
	 * additional content processing
	 */
	public static String stringArrayToString(String[] strArray,
	    String SEP) {
		StringBuffer sb = new StringBuffer("");
		if (strArray != null) {
			for (int i = 0; i < strArray.length; i++) {
				if (sb.length() > 0) {
					sb.append(SEP);
				}
				if (strArray[i] != null &&
				    strArray[i].trim().length() > 0) {
					sb.append(strArray[i]);
				} else {
					sb.append(" ");
				}
			}
		}
		return sb.toString();
	} // stringArrayToString

	public static void dumpStringArray(String comment, String[] strArray,
	    Logger logger) {
		if (strArray != null) {
			for (int i = 0; i < strArray.length; i++) {
				logger.info(comment + ": >" +
				    strArray[i] + "<");
			}
		}
	} // dumpStringArray


	public static String forceTrailingChar(String s, char ch) {
		if (!(s.charAt(s.length() - 1) == ch)) {
			s += ch;
		}
		return s;
	} // forceTrailingChar

} // class
