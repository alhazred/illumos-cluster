/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)CfgchkModule.java	1.2	08/05/29 SMI"
 */
package com.sun.cluster.cfgchk.agent.cfgchk;

// JDK
import java.net.InetAddress;
import java.net.UnknownHostException;

import java.util.Map;
import java.util.logging.Logger;

import javax.management.ObjectName;

import com.sun.cacao.DeploymentDescriptor;
import com.sun.cacao.Module;
import com.sun.cacao.ObjectNameFactory;
import com.sun.cacao.commandstream.CommandRegistry;
import com.sun.cacao.element.AdministrativeStateEnum;
import com.sun.cacao.element.AvailabilityStatusEnum;
import com.sun.cacao.element.OperationalStateEnum;
import com.sun.cacao.invocation.InvocationException;
import com.sun.cacao.invocation.InvocationStatus;

import com.sun.cluster.cfgchk.Globals;
import com.sun.cluster.cfgchk.agent.cfgchk.exec.ExecUtil;
import com.sun.cluster.cfgchk.common.CfgchkRuntimeException;
import com.sun.cluster.cfgchk.datasrc.DataException;
import com.sun.cluster.cfgchk.datasrc.LiveClusterDataSrc;

import com.sun.cluster.cfgchk.utils.logger.LoggerUtils;
import com.sun.cluster.cfgchk.utils.logger.RemoteLogger;


/**
 *
 **/
public class CfgchkModule extends Module implements Globals {

	// default logger: cacao.0
	// RemoteLogger initted later on for use by other classes
	private static Logger cLogger =
	Logger.getLogger("com.sun.cluster.cfgchk.agent.cfgchk");

	// parameters coming from the XML descriptor
	private Map parameters = null;

	private ObjectName adminName = null;
	private ObjectName execUtilName = null;
	private ObjectName liveClusterDataSrcName = null;

	private Administrator admin = null;
	private ExecUtil execUtil = null;
	private LiveClusterDataSrc liveClusterDataSrc = null;

	private String localNodename = "dynamically fetched at loadtime";
		// No I18n
	private String loggingLevel = "FINE";
		// no I18n

	// ===================================================================
	//   CfgchkModule constructor
	// ===================================================================
	/**
	 * Constructor called by the container.
	 *
	 * @param descriptor the deployment descriptor used to load the module
	 **/
	public CfgchkModule(DeploymentDescriptor descriptor) {
		super(descriptor);

		// load parameters from XML descriptor
		parameters = descriptor.getParameters();
		loggingLevel = (String) parameters.get("loggingLevel");
	} // ()

	// ==================================================================
	//   start
	// ===================================================================
	/**
	 * Invoked by the container when unlocking the module.
	 *
	 * @exception CfgchkRuntimeException If unable to start.
	 **/
	protected void start()
		throws CfgchkRuntimeException {
		cLogger.info("[>cfgchk start]");
		cLogger.info("**********************************************" +
		    "\n" + "      " +
		    "            Starting CfgchkModule" +
		    "\n" + "      " +
		    "*******************************************************");

		// create the ON factory for the whole module
		ObjectNameFactory onf = new ObjectNameFactory(DOMAIN_CFGCHK);

		try {

			// register the ExecUtil MBean
			cLogger.info("cfchk module : " +
			    "starting ExecUtil on localhost: " +
			    localNodename);
			execUtil = new ExecUtil(onf, localNodename);
			execUtilName = execUtil.createON();
			cLogger.info("cfgchk module: execUtilName: " +
			    execUtilName);
			getMbs().registerMBean(execUtil, execUtilName);

			cLogger.info("cfgchk module: ExecUtil");
			execUtil.start(cLogger);
			cLogger.info("ExecUtil started");


			localNodename = getNodename();

			// call idempotent init at very top of module lifetime
			// any class can now call RL.getLogger()
			// fetched from reg.xml as a module property
			cLogger.info("init remote logger on " + localNodename);
			RemoteLogger.initRemoteLogger(localNodename,
			    loggingLevel);

			cLogger.info("[<cfgchk start] on node " +
			    localNodename);

			// register Administrator as Command
			cLogger.info("cfgchk module : " +
			    "starting Administrator on localhost ");
			admin = new Administrator(onf);
			adminName = admin.createON();
			cLogger.info("cfgchk module: adminName: " + adminName);
			getMbs().registerMBean(admin, adminName);
			CommandRegistry.registerCommand(DOMAIN_CFGCHK,
			    "administrator",
			    admin);
			admin.start();
			cLogger.info("cfgchk module: reg'ed Administrator");

			// register the LiveClusterDataSrc MBean
			cLogger.info("cfchk module:  " +
			    "starting LiveClusterDataSrc on localhost: " +
			    localNodename);
			liveClusterDataSrc = new LiveClusterDataSrc(onf,
			    localNodename);
			liveClusterDataSrcName = liveClusterDataSrc.createON();
			cLogger.info("cfgchk module: " +
			    "liveClusterDataSrcName: " +
			    liveClusterDataSrcName);
			getMbs().registerMBean(liveClusterDataSrc,
			    liveClusterDataSrcName);
			cLogger.info("cfgchk module: registered " +
			    "LiveClusterDataSrc");
			liveClusterDataSrc.start();
			cLogger.info("LiveClusterDataSrc started");


			cLogger.info("**************************************" +
			    "\n" + "      " +
			    "cfgchk module SUCCESSFULLY started on node " +
			    localNodename + "\n" + "      " +
			    "***********************************************");

		} catch (Exception e) {
			// cleanup
			cLogger.severe("cfgchk module: Exception during " +
			    "startup msg: " + e.getMessage());
			cLogger.severe("cfgchk module: Exception during " +
			    "startup; see cacao log for stack trace");
			cLogger.throwing("cfgchkModule", "start()", e);
			StackTraceElement[] stackElts = e.getStackTrace();
			for (int i = 0; i < stackElts.length; i++) {
				cLogger.warning("cfgchk module stackElt: " +
				    stackElts[i]);
			}
			try {
				stop();
			} catch (CfgchkRuntimeException ex) {
				// XXXXXXXXXXXXXXXXXX
				// ignore it;
				// Bugfix 6383835:
				//  Disable the cfgchk module if
				// we fail to cleanup the objects during
				// partial startup. This will result in the
				// cfgchk module to be locked.
				// The module can be started again by
				//  cacaoadm restart)
				cLogger.warning("Failed to cleanup " +
				    "the cfgchk objects");
				setOperationalState(
					OperationalStateEnum.DISABLED);
			}

			availabilityStatusSetAdd(
				AvailabilityStatusEnum.FAILED);

			cLogger.warning("[!cfgchk start] " + e.toString());
			cLogger.severe(
				"WARNING: Failed to start the cfgchk agent");
			throw new CfgchkRuntimeException(e.toString());
		}

	} // start

	// =================================================================
	//   stop
	// ==================================================================
	/**
	 *
	 * Invoked by the container when stopping, must release all resources.
	 *
	 **/
	protected void stop() throws CfgchkRuntimeException {
		cLogger.info("[>cfgchk stop] on node " + localNodename);

		try {

			// shutdown & unregister mbeans
			liveClusterDataSrc.stop();
			execUtil.stop(cLogger);
			admin.stop();

			// Unregister our command from the registry
			CommandRegistry.unregisterCommand(DOMAIN_CFGCHK,
			    "administrator");

			getMbs().unregisterMBean(liveClusterDataSrcName);
			getMbs().unregisterMBean(execUtilName);
			getMbs().unregisterMBean(adminName);

			cLogger.info("[<cfgchk stop] SUCCESSFUL on node " +
			    localNodename);

		} catch (Exception e) {
			cLogger.warning("[!cfgchk stop] node " +
			    localNodename + " " + e.toString());
			throw new CfgchkRuntimeException(e.toString());
		}
	} // stop

	// ===================================================================
	//   isHealthy
	// ====================================================================
	/**
	 * Performs a 1-off health-check command
	 *
	 * @return  <code>TRUE</code>  means health-check success
	 *          <code>FALSE</code> means health-check failure
	 *
	 * If the health-check fails, the module will set its
	 * operational state to DISABLED
	 */
	public boolean isHealthy()
		{
			boolean res = true;

			cLogger.fine("Health check for the cfgchk module");

			if ((getAdministrativeState() !=
				AdministrativeStateEnum.UNLOCKED) ||
			    (getOperationalState() !=
				OperationalStateEnum.ENABLED)) {

				cLogger.fine("AdministrativeStateEnum = " +
				    getAdministrativeState() +
				    " AvailabilityStatusEnum=" +
				    getAvailabilityStatusSet() +
				    " OperationalState=" +
				    getOperationalState());

				cLogger.warning(
					"The module state indicates a failure");
				res = false;

			} else {
				cLogger.info("SUCCESS: Health check passed " +
				    "for cfgchk module");
			}
			return res;
		}

	private String getNodename() {
		cLogger.info("-- ENTER --");
		String nodename = "undetermined"; // No I18n

		try {
			nodename = getLocalHostname();
			cLogger.info("setting nodename to: >>" +
			    nodename + "<<");

		} catch (DataException dex) {
			cLogger.warning("DataException: " + dex);
			cLogger.warning("setting nodename to: " + nodename);
			LoggerUtils.exception(
				"cfgchkModule.getNodename() DataException",
					dex, cLogger);
		}

		cLogger.info("-- EXIT nodename: " + nodename);
		return nodename;
	} // getNodename

	// clone of Utils.getLocalHostname
	// calling method in that class now results in creation of
	// static RemoteLogger too soon so it gets name 'localhost'
	private String getLocalHostname() throws DataException {
		String localHostname = null;
		cLogger.fine("-- ENTER --");

		try {
			InetAddress ia = InetAddress.getLocalHost();
			cLogger.fine("InetAddress: " + ia);

			localHostname = ia.getCanonicalHostName();
			cLogger.fine("localHostname: >>" +
			    localHostname + "<<");

		} catch (UnknownHostException ex) {
			cLogger.severe("UnknownHostException: " + ex);
			throw new DataException(ex);
		}
		cLogger.fine("-- EXIT: " + localHostname);
		return localHostname;
	} // getLocalHostname

} // class

