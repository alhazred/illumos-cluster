/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)XMLResults.java	1.4	08/08/26 SMI"
 */
package com.sun.cluster.cfgchk.report;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.SimpleTimeZone;
import java.util.TimeZone;
import java.util.TreeSet;
import java.util.logging.Logger;

import com.sun.cluster.cfgchk.Check;
import com.sun.cluster.cfgchk.Globals;
import com.sun.cluster.cfgchk.utils.StringUtils;
import com.sun.cluster.cfgchk.utils.logger.LoggerUtils;


/**
 * The check run results in XML format.
 *
 * Tallies updated as checks accumulated with
 */
public class XMLResults {

	private Logger logger = null;
	private PrintWriter xmlWriter = null;
	private PrintWriter textWriter = null;
	private static String INDENT = "    ";
	private String xmlPathname = null;
	private String textPathname = null;
	private String logPathname = null;
	private String commandNodeName = null;

	private String[] nodenames = null;
	private String[] dsNames = null;
	private ArrayList[][] checks = null;

	private int multiNodeTallyIndex;
	private int[][] statusTally = null;
	private int[][] severityTally = null;
	private int[] totalChecksPerNode = null;
	private TreeSet uniqueChecks = null;
	private boolean multinodeRun = false;

	private  String reportDate = null;
	private  String reportDateGMT = null;
	private static String datePattern = "yyyy.MM.dd 'at' HH.mm.ss z";


	private static char D1 = '*';
	private static String DIVIDER1 = StringUtils.
	    fillDimensionString(75, D1);
	private static char D2 = '-';
	private static String DIVIDER2 = StringUtils.
	    fillDimensionString(75, D2);
	private static String DIVIDER2a = StringUtils.
	    fillDimensionString(26, D2);
	private static String DIVIDER2b = StringUtils.
	    fillDimensionString(17, D2);
	private static String DIVIDER2c = StringUtils.
	    fillDimensionString(22, D2);

	private static char D3 = '=';
	private static String DIVIDER3 = StringUtils.
	    fillDimensionString(75, D3);

	private static char D4 = '|';
	private static String MARKER1 = StringUtils.
	    fillDimensionString(1, D1);

	private String PROGNAME = "cluster check";

	public XMLResults(String commandNodeName, String outdir,
	    String[] hostnameList, String[] dsNameList, String logPathname,
	    Logger logger) throws IOException {
		this.logger = logger;
		this.logPathname = logPathname;
		this.commandNodeName = commandNodeName;

		try {
			// number of lists of checks to accumulate during
			// check run:
			//	number of nodes + 1 for the mult-list
			// (unless num nodes == 1)
			// arrays are 0-based so create an array that is
			// num-nodes + 1
			// but index to multi-node element is num-nodes
			logger.info("-- ENTER-- ");

			this.nodenames = hostnameList;
			this.dsNames = dsNameList;
			for (int i = 0; i < nodenames.length; i++) {
				logger.info("nodenames["+i+"]: " +
				    nodenames[i]);
			}
			for (int i = 0; i < dsNames.length; i++) {
				logger.info("dsNames["+i+"]: " + dsNames[i]);
			}

			// Single node run looks like nodename in [0]
			// and same nodename in [1] as multi-list
			//  which is not useful
			// Two nodes needs 3 elements in nodenames[]
			// then the multi-list element contains a comma
			// separated list of all nodenames and is useful
			if (nodenames.length > 2) {
				multinodeRun = true;
			}

			logger.info("multinode run: " + multinodeRun);

			multiNodeTallyIndex = nodenames.length - 1;
			// pointer to array elt holding multi-node results

			logger.info("outdir: " + outdir);

			// Check objects stored by status within
			// nodeIndex (or multiNodeTallyIndex)
			checks = new ArrayList
			    [multiNodeTallyIndex + 1]
			    [Check.NUM_STATUS_CATEGORIES];
				// 0-based addressing; multi at end
			for (int i = 0; i < multiNodeTallyIndex + 1; i++) {
				for (int j = 0;
				     j < Check.NUM_STATUS_CATEGORIES;
				     j++) {
					checks[i][j] = new ArrayList();
				}
			}

			totalChecksPerNode = new int
			    [multiNodeTallyIndex + 1];
				// 0-based addressing; multi at end
			for (int i = 0; i <= multiNodeTallyIndex; i++) {
				// guarantee init to 0
				totalChecksPerNode[i] = 0;
			}

			uniqueChecks = new TreeSet();

			// note the +1 array size: we're tracking
			//  multi-node in last elt
			statusTally = new int
			    [multiNodeTallyIndex + 1]
			    [Check.NUM_STATUS_CATEGORIES];
			severityTally = new int
			    [multiNodeTallyIndex + 1]
			    [Check.NUM_SEVERITIES];

			for (int i = 0; i <= multiNodeTallyIndex; i++) {
				for (int j = 0;
				     j < Check.NUM_STATUS_CATEGORIES;
				     j++) {
					statusTally[i][j] = 0;
					// guarantee init to 0
				}
				// note: severity 0 not used
				for (int j = 1; j < Check.NUM_SEVERITIES; j++) {
					// guarantee init to 0
					severityTally[i][j] = 0;
				}
			}

			String xmlFilename = Globals.xmlResultsName;
			xmlPathname = outdir + xmlFilename;
			logger.info("xmlPathname: " + xmlPathname);

			String textFilename = Globals.textResultsName;
			textPathname = outdir + textFilename;
			logger.info("textPathname: " + textPathname);

			xmlWriter = new PrintWriter(
				new FileWriter(xmlPathname, false));
				// overwrite
			textWriter = new PrintWriter(
				new FileWriter(textPathname, false));
				// overwrite

			// assemble report date strings
			TimeZone tz = new SimpleTimeZone(0, "GMT");
			Date d = new Date();
			DateFormat df = new SimpleDateFormat(datePattern);

			reportDate = df.format(d);
			df.setTimeZone(tz);
			reportDateGMT = df.format(d);

			startXMLReport();
			startTextReport();

		} catch (Exception e) {
			logger.info("Exception: " + e.getMessage());
			LoggerUtils.exception("Exception", e, logger);
		}

		logger.info("-- EXIT --");
	} // ()

	/**
	 * add Check to appropriate node tally or multi-node tally
	 * add <checkresult> to XML accumulation
	 */
	public void addCheck(Check check) {
		logger.fine("-- ENTER: " + check);
		int nodeIndex = multiNodeTallyIndex;

		try {
			if (!check.isMultiNodeCheck()) {
				logger.fine("NOT MultiNodeCheck");
				String nodelist = check.getCheckedNodenames();

				// the Check knows what node it ran against
				// get appropriate index into nodenames array
				nodeIndex =
				    StringUtils.indexOfStringInStringArray(
					    nodelist, nodenames);
				if (nodeIndex == -1) {
					// should never happen
					logger.warning(check.getID() +
					    ": getCheckedNodenames() not "
					    +"found in nodelist.");
				}
			} else {
				logger.fine("IS MultiNodeCheck");
			}

			logger.finest("nodeIndex: " + nodeIndex);
			tallyCheck(nodeIndex, check);
			addCheckXML(check);

		} catch (Exception e) {
			// exceptions here not fatal
			logger.warning("Exception: " + e.getMessage());
			LoggerUtils.exception("Exception", e, logger);
		}
		logger.info("added " + check.getID() + " nodeIndex: " +
		    nodeIndex);

	} // addCheck


	/**
	 *
	 */
	public void addCheckXML(Check check) {
		logger.info("-- ENTER --");

		String msg = null;
		println(xmlWriter, 3, "<checkresult status=\"" +
		    check.getStatusName() + "\" " +
		    "severity=\"" + check.getSeverityName() + "\">");
		print(xmlWriter, 4, "<check_id>");
		print(xmlWriter, check.getID());
		println(xmlWriter, "</check_id>");

		print(xmlWriter, 4, "<check_nodes>");
		print(xmlWriter, check.getCheckedNodenames());
		println(xmlWriter, "</check_nodes>");

		print(xmlWriter, 4, "<check_version>");
		print(xmlWriter, check.getVersion());
		println(xmlWriter, "</check_version>");

		print(xmlWriter, 4, "<check_revision_date>");
		print(xmlWriter, check.getRevisionDate());
		println(xmlWriter, "</check_revision_date>");

		print(xmlWriter, 4, "<keywords>");
		print(xmlWriter, check.getKeywords());
		println(xmlWriter, "</keywords>");

		print(xmlWriter, 4, "<problem_statement>");
		print(xmlWriter, check.getProblemStatement());
		println(xmlWriter, "</problem_statement>");

		print(xmlWriter, 4, "<analysis>");
		print(xmlWriter, check.getAnalysis());
		println(xmlWriter, "</analysis>");

		print(xmlWriter, 4, "<recommendations>");
		print(xmlWriter, check.getRecommendations());
		println(xmlWriter, "</recommendations>");

		print(xmlWriter, 4, "<extra_message>");
		msg = check.getInfoMessage();
		if (msg != null) {
			print(xmlWriter, msg);
		}
		println(xmlWriter, "</extra_message>");

		print(xmlWriter, 4, "<insufficient_data_message>");
		msg = check.getInsufficientDataMsg();
		if (msg != null) {
			print(xmlWriter, msg);
		}
		println(xmlWriter, "</insufficient_data_message>");

		print(xmlWriter, 4, "<applicability_logic>");
		print(xmlWriter, check.getApplicabilityLogic());
		println(xmlWriter, "</applicability_logic>");

		print(xmlWriter, 4, "<check_logic>");
		print(xmlWriter, check.getCheckLogic());
		println(xmlWriter, "</check_logic>");

		println(xmlWriter, 3, "</checkresult>");

		println(xmlWriter, "");
	} // addCheckXML


	private void tallyCheck(int nodeIndex, Check check) {
		logger.info("-- ENTER: " + check);
		logger.fine("nodeIndex: " + nodeIndex);
		if (nodeIndex < 0) {
			logger.warning(check.getID() +
			    ": can't add to tally. Ignoring.");
			return; // can't tally; ignore
		}

		int status = check.getStatus();
		String statusName = check.getStatusName();
		int severity = check.getSeverity();
		String severityName = check.getSeverityName();

		logger.fine("check.status(): " + status + ": " +
		    statusName); // finest
		logger.fine("current statusTally for this status on " +
		    "this node: " + statusTally[nodeIndex][status]);
		logger.fine("check.severity(): " + severity + ": " +
		    severityName); // finest
		logger.fine("current severityTally for this severity " +
		    "on this node: " + severityTally[nodeIndex][severity]);

		checks[nodeIndex][status].add(check);
		totalChecksPerNode[nodeIndex]++;
		uniqueChecks.add(check.getID());

		statusTally[nodeIndex][status]++;
		logger.finest("incremented statusTally for this status: " +
		    statusTally[nodeIndex][status]);

		if (status == Check.VIOLATED) {
			severityTally[nodeIndex][severity]++;
			logger.finest("incremented severityTally for " +
			    "this severity: " +
			    severityTally[nodeIndex][severity]);
		} else {
			logger.finest("not incrementing " +
			    "severityTally for this check");
			;
		}
		logger.info("-- EXIT --");
	} // tallyCheck

	public void startTextReport() {
		logger.finest("-- ENTER --");
		int indent = 0; // starting indent level

		// report header
		println(textWriter, indent, DIVIDER1);
		println(textWriter, indent, MARKER1);
		println(textWriter, indent, MARKER1 + "\t" + PROGNAME +
		    "\t\t(ver " + Globals.CHKCFG_VERSION + ")");
		println(textWriter, indent, MARKER1);
		println(textWriter, indent, DIVIDER1);
		println(textWriter, indent, "");

		println(textWriter, indent+1, "Report Date:\t" + reportDate);
		println(textWriter, indent+1, "\t\t\t"         + reportDateGMT);
		println(textWriter, indent+1, "Command run on host: ");
		println(textWriter, indent+6,  commandNodeName);
		println(textWriter, indent+1, "Checks run on nodes:");

		// don't print the extra "name" for multi
		for (int i = 0; i < multiNodeTallyIndex; i++) {
			println(textWriter, indent+6, dsNames[i]);
		}
		println(textWriter, indent, "");

		logger.finest("-- EXIT --");
	} // startTextReport

	public void startXMLReport() {
		logger.fine("-- ENTER --");

		println(xmlWriter, 0, "<?xml version=\"1.0\" " +
		    "encoding=\"UTF-8\"?>");
		/* BEGIN JSTYLED */
		println(xmlWriter, 0, "<!-- DOCTYPE cfgchkresult PUBLIC \"-//" +
		    /* END JSTYLED */
		    "Sun Microsystems, Inc.//DTD CfgChk Result 1.0//EN\" -->");
		println(xmlWriter, 0, "<cfgchk_result>");
		println(xmlWriter, 1, "<general_info>");
		println(xmlWriter, 2, "<report_host> " + commandNodeName +
		    " </report_host>");
		println(xmlWriter, 2, "<report_date> " + reportDateGMT +
		    " </report_date>");
		println(xmlWriter, 1, "</general_info>");
		println(xmlWriter, 1, "<run_result>");
		println(xmlWriter, 2, "<report_date> " + reportDateGMT +
		    " </report_date>");
		println(xmlWriter, 2, "<cfgchk_version> " +
		    Globals.CHKCFG_VERSION + " </cfgchk_version>");
		println(xmlWriter, 2, "<run_info>");
		println(xmlWriter, 3, "<input_data>");
		// don't include the extra "name" for multi
		for (int i = 0; i < multiNodeTallyIndex; i++) {
			println(xmlWriter, 4, "<data_src> " + dsNames[i] +
			    " </data_src>");
		}
		println(xmlWriter, 3, "</input_data>");
		/*
		println(xmlWriter, 3, "<system_info>");
		println(xmlWriter, 3, "</system_info>");
		*/
		println(xmlWriter, "");
		logger.fine("-- EXIT --");
	} // startXMLReport

	/*
	 * calc tallies
	 * write text report
	 * finish up xml output
	 */
	public void finish(long elapsedTime) {
		logger.fine("-- ENTER --");
		finishXML(elapsedTime);
		createTextReport(elapsedTime, 0); // start at indent level = 0

		for (int i = 0; i < multiNodeTallyIndex + 1; i++) {
			for (int j = 0; j < Check.NUM_STATUS_CATEGORIES; j++) {
				logger.fine("statusTally[" +i+", "+j+"]: " +
				    Check.STATUS_NAMES[j] + ": " +
				    statusTally[i][j]);
			}
		}
		logger.fine("-- EXIT --");
	} // finish

	/*
	 *
	 */
	public void createTextReport(long elapsedTime, int indent) {

		logger.fine("-- ENTER --");
		try {
			// dump tallies

			/*
			 * note on array addressing:
			 * 2 node cluster needs three tallies
			 * elt 0 & 1 are single node tallies
			 * elt 2 is multi-node tally
			 * end of loop condition must include
			 * multiNodeTallyIndex  each elt contains an
			 * array of ArrayLists
			 * these arrays are size NUM_STATUS_CATEGORIES
			 *
			 * if singlenode run then do not include
			 * multiNodeTallyIndex in output text report
			 */

			for (int i = 0; i <= multiNodeTallyIndex; i++) {
				for (int j = 0;
				     j < Check.NUM_STATUS_CATEGORIES;
				     j++) {
					logger.finest(
						"per node per status tally["+
							i+", "+j+"] =========");
					int numChecks = checks[i][j].size();
					Check ck = null;
					for (int k = 0; k < numChecks; k++) {
						ck = (Check)checks[i][j].get(k);
						/* BEGIN JSTYLED */
						logger.finest(
							"checks["+
								i+", "+j+", "+k+
								"]: " +
								ck.getID() +
								": " +
								ck.getCheckedNodenames());
						/* END JSTYLED */
					} //
				} // j
			} // i

			/*
			 * finish up header section
			 */

			// total checks
			println(textWriter, indent+1, "Unique Checks: " +
			    uniqueChecks.size());
			println(textWriter, indent, "");

			/*
			 * individual node & multi-node sections
			 *
			 * index i points to individual node to report on
			 * if i == multiNodeTallyIndex (and is multinode run)
			 * then report on multinode checks
			 */
			String title = "Single Node";
			for (int i = 0; i <= multiNodeTallyIndex; i++) {
				if (!multinodeRun &&
				    i == multiNodeTallyIndex) {
					logger.info(
						"skipping multi-node section");
					break;
					// don't do multi section
					// if isn't multi run
				}

				println(textWriter, indent, DIVIDER3);
				if (i == multiNodeTallyIndex) {
					title = "Multi Node";
				}
				println(textWriter, indent, MARKER1);
				println(textWriter, indent, MARKER1 +
				    "\t" + "Summary of " + title +
				    " Check Results for " + dsNames[i]);
				println(textWriter, indent, MARKER1);
				println(textWriter, indent, DIVIDER3);

				textSummaryPerNode(i, indent+2);

				int numCks = -1;
				for (int j = 0;
				     j < Check.NUM_STATUS_CATEGORIES;
				     j++) {
					numCks = checks[i][j].size();
					if (numCks < 1) {
						continue;
						// don't print an empty section
					}
					// pass indices into
					// tally and name arrays
					printStatusCategorySection(
						numCks, i, j, indent);
				}


			} // node sections

			// report closing
			println(textWriter, indent, "");
			println(textWriter, indent, DIVIDER3);
			println(textWriter, indent, MARKER1);
			println(textWriter, indent, MARKER1 +
			    "\tEnd of Report " + reportDate);
			println(textWriter, indent, MARKER1);
			println(textWriter, indent, DIVIDER3);

			textWriter.close();

		} catch (Exception e) {
			// non-fatal
			logger.warning("Exception: " + e.getMessage());
			LoggerUtils.exception("Exception", e, logger);
		}

		logger.fine("-- EXIT --");
	} // createTextReport

	private void printStatusCategorySection(int numCks, int i,
	    int j, int indent) {
		String ckStr = " Checks ";
		if (numCks == 1) { ckStr = " Check "; }
		Check ck = null;

		println(textWriter, indent, DIVIDER2);
		println(textWriter, indent, MARKER1);
		println(textWriter, indent, MARKER1 + "\tDetails for " +
		    numCks + " " + Check.STATUS_NAMES[j] + ckStr + "on " +
		    dsNames[i]);
		println(textWriter, indent, MARKER1);
		println(textWriter, indent, DIVIDER2);
		println(textWriter, indent, "");
		String msg = null;
		for (int k = 0; k < numCks; k++) {
			ck = (Check)checks[i][j].get(k);
			println(textWriter, indent+2, MARKER1 +
			    "  Check ID: " + ck.getID() + "  ***");
			println(textWriter, indent+2, DIVIDER2a);
			println(textWriter, indent+3, MARKER1 +
			    "  Severity: " + ck.getSeverityName());
			println(textWriter, indent, "");
			println(textWriter, indent+3, MARKER1 +
			    "  Problem Statement: " + ck.getProblemStatement());
			println(textWriter, indent, "");
			if (ck.getStatus() == Check.PASSED) {
				// empty body
			} else if (ck.getStatus() == Check.NOT_APPLICABLE) {
				println(textWriter, indent+3, MARKER1 +
				    "  Applicability: " +
				    ck.getApplicabilityLogic());
			} else if (ck.getStatus() == Check.VIOLATED ||
			    ck.getStatus() == Check.INFORMATION_ONLY ||
			    ck.getStatus() == Check.WARNING) {
				println(textWriter, indent+3, MARKER1 +
				    "  Analysis: " + ck.getAnalysis());
				println(textWriter, indent, "");
				println(textWriter, indent+3, MARKER1 +
				    "  Recommendations: " +
				    ck.getRecommendations());
			} else if (ck.getStatus() == Check.INSUFFICIENT_DATA) {
				println(textWriter, indent+3, MARKER1 +
				    "  Insufficient Data: " +
				    ck.getInsufficientDataMsg());
				println(textWriter, indent, "");
			} else if (ck.getStatus() == Check.EXECUTION_ERROR) {

				msg = "Send copies of " + xmlPathname +
				    " and " + logPathname +
				    " as well as all logs from " +
				    Globals.REMOTE_LOGS_DIR +
				    " on all nodes with this report to your " +
				    "Sun Microsystems (TM) representative " +
				    "for analysis."; // no I18n
				println(textWriter, indent+2, msg);
			} else if (ck.getStatus() == Check.UNKNOWN_STATUS) {
				println(textWriter, indent+2, msg);
			}
			println(textWriter, indent, "");
		} // k

	} // printStatusCategorySection

	private void textSummaryPerNode(int nodeIndex, int indent) {
		// logger.info("-- ENTER nodeIndex: " + nodeIndex);


		// total checks
		println(textWriter, indent, "");
		println(textWriter, indent, "Checks Considered: " +
		    totalChecksPerNode[nodeIndex]);
		println(textWriter, indent, "");
		println(textWriter, indent, "Results by Status");
		println(textWriter, indent, DIVIDER2b);

		// breakout by status
		for (int i = 0; i < Check.NUM_STATUS_CATEGORIES; i++) {
			String statusName = StringUtils.padString(
				Check.STATUS_NAMES[i], 18);
			String tally = StringUtils.padString("" +
			    statusTally[nodeIndex][i], 3, StringUtils.PAD_LEFT);
			println(textWriter, indent+1, statusName + ": " +
			    tally);
		}

		// violations breakout by severity
		println(textWriter, indent, "");
		println(textWriter, indent, "Violations by Severity");
		println(textWriter, indent, DIVIDER2c);
		for (int i = Check.CRITICAL; i < Check.WARNING; i++) {
			// there is no 0; WARNING is not a violation
			String statusName = StringUtils.padString(
				Check.SEVERITY_NAMES[i], 18);
			String tally = StringUtils.padString("" +
			    severityTally[nodeIndex][i], 3,
			    StringUtils.PAD_LEFT);
			println(textWriter, indent+1, statusName + ": " +
			    tally);
		}
		println(textWriter, indent, "");

		// logger.info("-- EXIT --");
	} // textSummaryPerNode

	public void finishXML(long elapsedTime) {
		logger.fine("-- ENTER --");

		println(xmlWriter, 3, "<run_summary>");

		print(xmlWriter, 4, "<elapsed_time>");
		print(xmlWriter, "" + elapsedTime);
		println(xmlWriter, "</elapsed_time>");

		String category = null;
		int count = 0;
		int nodeTotal = 0;

		// for each node + multi-node
		for (int i = 0; i < multiNodeTallyIndex + 1; i++) {
			// for each status catg per node
			count = 0;
			nodeTotal = 0;
			String nm = StringUtils.strip(nodenames[i], " ");
			println(xmlWriter, 4, "<node_summary name=\"" + nm + "\">");
			for (int j = 0; j < Check.NUM_STATUS_CATEGORIES; j++) {
				category = Check.STATUS_XML_NAMES[j];
				count = statusTally[i][j];
				println(xmlWriter, 5, "<" + category + "> " +
				    count + " </" + category + ">");
				nodeTotal += count;
			}
			println(xmlWriter, 5, "<node_total> " + nodeTotal +
			    " </node_total>");
			println(xmlWriter, 4, " </node_summary>");
		}

		println(xmlWriter, 3, "</run_summary>");
		println(xmlWriter, 2, "</run_info>");
		println(xmlWriter, 1, "</run_result>");
		println(xmlWriter, 0, "</cfgchk_result>");
		xmlWriter.close();
		logger.fine("-- EXIT --");
	} // finishXML


	private void println(PrintWriter writer, int indent, String str) {
		for (int i = 0; i < indent; i++) {
			writer.print(INDENT);
		}
		println(writer, str);
	} // println

	private void print(PrintWriter writer, int indent, int val) {
		for (int i = 0; i < indent; i++) {
			writer.print(INDENT);
		}
		print(writer, val);
	} // print
	private void print(PrintWriter writer, int indent, String str) {
		for (int i = 0; i < indent; i++) {
			writer.print(INDENT);
		}
		print(writer, str);
	} // print

	private void print(PrintWriter writer, int val) {
		writer.print(val);
	} // print
	private void print(PrintWriter writer, String str) {
		writer.print(str);
	} // print
	private void println(PrintWriter writer, String str) {
		writer.println(str);
	} // print

} // class
