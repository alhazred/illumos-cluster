/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)ExplorerSolarisDataSrc.java	1.3	08/06/02 SMI"
 */
package com.sun.cluster.cfgchk.datasrc;

import java.io.FileNotFoundException;
import java.io.IOException;

import java.util.ArrayList;
import java.util.StringTokenizer;
import java.util.logging.Logger;


import com.sun.cluster.cfgchk.datasrc.fileObject.TextFile;
import com.sun.cluster.cfgchk.datasrc.dataObject.VfstabEntry;
import com.sun.cluster.cfgchk.datasrc.fileObject.Vfstab;
import com.sun.cluster.cfgchk.i18n.I18n;


/**
 *
 * This is an implementation of interface SolarisDataSrc which knows
 * how to access to system files and commands from an
 * explorer archive.
 *
 * This code is always executing on the same node
 * that launched the command. Cacao is never used
 * so there is no mbean interface needed.
 *
 * Data cache in superclass.
 *
 * Explorer archives are all files: copies of files
 * and files of command output. This class and its
 * subclasses use the ExplorerMapping class to map
 * desired filenames and commands to archive filenames
 * which are then fetched and returned or data
 * extracted and returned as appropriate.
 *
 */
public class ExplorerSolarisDataSrc extends ExplorerBasicDataSrc
	implements SolarisDataSrc {


	public ExplorerSolarisDataSrc(String basedir, Logger logger) {
		super(basedir, logger);
		logger.info(dsName + ": -- ENTER --");
	}

	// ===  start interface SolarisDataSrc  ===


        public String getArchitecture() throws DataException {
		String arch = null;
		String cacheKey = "getArchitecture";
		logger.info(dsName + ": -- ENTER --" + cacheKey);

		try {
			arch = dataCache.getString(cacheKey);
			if (arch == null) {
				logger.info(dsName + ": not found in cache");
				TextFile tf = null;

				// get TextFile
				try {
					String cmdName = "/usr/bin/uname -a";
					String mappedName =
					    ExplorerMapping.get(cmdName);
					if (mappedName == null) {
						String[] i18nArgs = { cmdName };
						/* BEGIN JSTYLED */
						String lMsg =
						    I18n.getLocalized(logger,
							"internal.error.no.explorer.mapping",
							i18nArgs);
						/* END JSTYLED */
						throw new DataException(lMsg);
					}
					String fullPath = basedir +  mappedName;
					logger.info(dsName + ": fullPath: " +
					    fullPath);
					tf = new TextFile(fullPath);
						// FNFEx, IOEx

					// arch is 6th word in first line
					String line = tf.getLines()[0];
					logger.info(dsName + ": line: " + line);
					StringTokenizer st =
					    new StringTokenizer(line);
					if (st.countTokens() < 7) {
						String eMsg = "uname -a: " +
						    "insufficient tokens";
							// No I18n
						logger.info(dsName + ": " +
eMsg);
						String[] i18nArgs = { eMsg };
						/* BEGIN JSTYLED */
						String lMsg =
						    I18n.getLocalized(logger,
							"internal.error.cmd.unexpected.result",
							i18nArgs);
						/* END JSTYLED */
						throw new DataException(lMsg);
					}
					for (int i = 0; i < 5; i++) {
						st.nextToken(); // skip word
					}
					arch = st.nextToken();
					logger.info(dsName + ": arch: " + arch);
					dataCache.put(cacheKey, arch);
					logger.info(dsName +
					    ": String added to cache");

				} catch (IOException ioex) {
					String[] i18nArgs =
					    { ioex.getMessage() };
					/* BEGIN JSTYLED */
					String lMsg = I18n.getLocalized(logger,
					    "internal.error.cant.read.explorerfile",
					    i18nArgs);
					/* END JSTYLED */
					throw new DataException(lMsg);
				}
			} // == null
		} catch (DataException dex) { // already I18n; bubble it up
			logger.warning(dsName + ": DataException: " +
			    dex.getMessage());
			throw dex;
		}
		logger.info(dsName + ": arch: " + arch);
		return arch;
	} // getArchitecture

	public int getOSVersionMajor() throws DataException {

		Integer majorVersion = null;
		String cacheKey = "getOSVersionMajor";
		logger.info(dsName + ": -- ENTER --" + cacheKey);

		try {
			majorVersion = dataCache.getInteger(cacheKey);
			if (majorVersion == null) {
				logger.info(dsName + ": not found in cache");
				TextFile tf = null;

				// get TextFile
				try {
					String filename = "/etc/release";
					String mappedName =
					    ExplorerMapping.get(filename);
					if (mappedName == null) {
						String[] i18nArgs =
						    { filename };
						/* BEGIN JSTYLED */
						String lMsg =
						    I18n.getLocalized(logger,
							"internal.error.no.explorer.mapping",
							i18nArgs);
						/* END JSTYLED */
						throw new DataException(lMsg);
					}
					String fullPath = basedir +  mappedName;
					logger.info(dsName + ": fullPath: " +
					    fullPath);
					tf = new TextFile(fullPath);
						// FNFEx, IOEx
					majorVersion =
					    SolarisDataSrcCommon.
					    getOSVersionMajor(tf); // not null
					dataCache.put(cacheKey, majorVersion);
					logger.info(dsName +
					    ": Integer added to cache");

				} catch (IOException ioex) {
					String[] i18nArgs =
					    { ioex.getMessage() };
					/* BEGIN JSTYLED */
					String lMsg = I18n.getLocalized(logger,
					    "internal.error.cant.read.explorerfile",
					    i18nArgs);
					/* END JSTYLED */
					throw new DataException(lMsg);
				}

			} // == null
		} catch (DataException dex) { // already I18n; bubble it up
			logger.warning(dsName + ": DataException: " +
			    dex.getMessage());
			throw dex;
		}
		return majorVersion.intValue();
	} // getOSVersionMajor


	public Vfstab getVfstab(String nodename) throws DataException {
		Vfstab vfstab = null;
		String cacheKey = "getVfstab";
		logger.info(dsName + ": -- ENTER --" + cacheKey);

		try {
			vfstab = dataCache.getVfstab(cacheKey);
			if (vfstab == null) {
				logger.info(dsName + ": not found in cache");
				TextFile tf = null;

				// get TextFile
				try {
					String filename = "/etc/vfstab";
					String mappedName =
					    ExplorerMapping.get(filename);
					if (mappedName == null) {
						String[] i18nArgs =
						    { filename };
						/* BEGIN JSTYLED */
						String lMsg =
						    I18n.getLocalized(logger,
							"internal.error.no.explorer.mapping",
							i18nArgs);
						/* END JSTYLED */
						throw new DataException(lMsg);
					}
					String fullPath = basedir +  mappedName;
					logger.info(dsName + ": fullPath: " +
					    fullPath);
					tf = new TextFile(fullPath);
						// FNFEx, IOEx

					vfstab =
					    SolarisDataSrcCommon.loadVfstab(
						    nodename, tf);

					dataCache.put(cacheKey, vfstab);
					logger.info(dsName +
					    ": Vfstab added to cache");

				} catch (IOException ioex) {
					String[] i18nArgs =
					    { ioex.getMessage() };
					/* BEGIN JSTYLED */
					String lMsg = I18n.getLocalized(logger,
					    "internal.error.cant.read.explorerfile",
					    i18nArgs);
					/* END JSTYLED */
					throw new DataException(lMsg);
				}

			} // == null
		} catch (DataException dex) { // already I18n; bubble it up
			logger.warning(dsName + ": DataException: " +
			    dex.getMessage());
			throw dex;
		}
		return vfstab;

	} // getVfstab

	// ===  end interface SolarisDataSrc  ===


} // class
