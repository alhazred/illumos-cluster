/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)ClusterUtils.java	1.3	08/06/02 SMI"
 */
package com.sun.cluster.cfgchk.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Logger;

import com.sun.cacao.invocation.InvocationStatus;
import com.sun.cacao.invocation.InvocationException;

import com.sun.cluster.cfgchk.Globals;
import com.sun.cluster.cfgchk.common.CfgchkRemoteException;

import com.sun.cluster.cfgchk.datasrc.DataException;
import com.sun.cluster.cfgchk.datasrc.LiveClusterDataSrcMBean;
import com.sun.cluster.cfgchk.datasrc.fileObject.TextFile;

import com.sun.cluster.cfgchk.utils.logger.LoggerUtils;

import com.sun.cluster.cfgchk.agent.cfgchk.exec.CommandInvoker;

// JMX
import javax.management.MBeanServerConnection;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector; // JDK1.5

// CACAO
import com.sun.cacao.ObjectNameFactory;
import com.sun.cacao.agent.MBeanServerInvocationHandler;

/*
 * Misc utils for cluster-related service:
 *	getNodenameFromNodeID()
 *	getLocalNodename()
 *	getClusterNodenames()
 *	getClusterNodeIDs()
 *	getClusterName()
 *	isClusterNode()
 *	isInClusterMode()
 *	getLocalHostID()
 *	getValidate()
 *	invokeValidate()
 *	getRTStringParam()
 *	getRGStringParam()
 *	rtNametoCCRFilename
 *	rgNametoCCRFilename
 *	getRemoteLiveClusterDataSrc()
 */

public class ClusterUtils {

	public static String CL_BIN =
	    "/usr/cluster/bin";
	public static String SCHA_CL_GET =
	    CL_BIN + "/" + "scha_cluster_get";
	public static String SCHA_RT_GET =
	    CL_BIN + "/" + "scha_resourcetype_get";
	public static String SCHA_RG_GET =
	    CL_BIN + "/" + "scha_resourcegroup_get";
	public static String SCHA_RS_GET =
	    CL_BIN + "/" + "scha_resource_get";

	/*
	 * usingCacao: if true means ExecUtils will use cacao's invoker
	 * which allows us to exec on a different node but will throw
	 * an exception on non-0 return.
	 * if false means execute directly on local node
	 */



	// use scha cmd therefore localhost is OK
	// requires cluster mode
	public static String getNodenameFromNodeID(String nodeid,
	    Logger logger) throws DataException {
		// localhost must be cluster node
		// must be in cluster mode
		String[] cmd =
		    { SCHA_CL_GET, "-O", "NODENAME_NODEID", nodeid };
		String[] outstrs = null;
		try {
			outstrs = Utils.runCommandStrArray(cmd);
			for (int i = 0; i < outstrs.length; i++) {
				logger.info("outstrs[" + i+ "]: " + outstrs[i]);
			}
		} catch (DataException dex) {
			logger.info("DataException: " + dex);
			throw dex;
		}
		logger.info("-- EXIT: " + outstrs[0]);
		return outstrs[0];
	} // getNodenameFromNodeID

	/*
	 * alternative to use of scha commands
	 */
	public static String getLocalNodename(Logger logger)
		throws DataException {
		String nodename = null;
		try {
			nodename = Utils.getLocalHostname();
		} catch (DataException dex) {
			logger.info("DataException: " + dex);
			throw dex;
		}

		return nodename;
	} // getLocalNodename

	// use scha cmd therefore localhost is OK
	// requires clustermode
	public static String[] getClusterNodenames(Logger logger)
		throws DataException {
		// localhost must be cluster node
		// must be in cluster mode
		// ask local node for nodelist
		String[] cmd = { SCHA_CL_GET, "-O", "ALL_NODENAMES" };
		String[] nodes = null;
		try {
			nodes = Utils.runCommandStrArray(cmd);
			for (int i = 0; i < nodes.length; i++) {
				logger.info("node[" + i+ "]: " + nodes[i]);
			}
		} catch (DataException dex) {
			logger.info("DataException: " + dex);
			throw dex;
		}

		return nodes;
	} // getClusterNodenames

	// use scha cmd therefore localhost is OK
	// requires clustermode
	public static String[] getClusterNodeIDs(Logger logger)
		throws DataException {
		logger.info("-- ENTER --");
		// ask local node for nodelist
		String[] cmd = { SCHA_CL_GET, "-O", "ALL_NODEIDS" };
		String[] nodes = null;
		try {
			logger.info("will call Utils.runCommandStrArray() ");
			nodes = Utils.runCommandStrArray(cmd);
			logger.info("back from callUtils.runCommandStrArray()");
			for (int i = 0; i < nodes.length; i++) {
				logger.info("node[" + i+ "]: " + nodes[i]);
			}
		} catch (DataException dex) {
			logger.info("DataException: " + dex);
			throw dex;
		}

		logger.info("-- EXIT --");
		return nodes;
	} // getClusterNodeIDs

	// use scha cmd therefore localhost is OK
	// requires clustermode
	public static String getClusterName(Logger logger)
		throws DataException {
		logger.info("-- ENTER --");

		String[] cmd =
		    { "/usr/cluster/bin/scha_cluster_get",
		      "-O", "CLUSTERNAME" };
		String clName = null;
		String[] retVal = null;

		try {
			logger.info("will call Utils.runCommandStrArray() ");
			retVal = Utils.runCommandStrArray(cmd);
			logger.info("back from callUtils.runCommandStrArray()");
			clName = retVal[0]; // one word
		} catch (DataException dex) {
			logger.info("DataException: " + dex);
			throw dex;
		}

		logger.info("-- EXIT clName: " + clName);
		return clName;
	} // getClusterName


	// look on localnode
	// does not require cluster mode
	public static boolean isClusterNode(Logger logger) {
		logger.info("-- ENTER --");
		String indicatorFile = "/etc/cluster/nodeid";
		boolean isNode = false;
		try {
			new TextFile(indicatorFile);
			isNode = true;
		} catch (FileNotFoundException fnfex) {
			logger.info("FileNotFoundException: " +
			    "not a cluster node: " + fnfex);
		} catch (IOException ioex) {
			logger.info("IOException: not a cluster node: " + ioex);
		} catch (Exception ex) {
			logger.info("Exception: not a cluster node: " + ex);
		}

		logger.info("isClusterNode: " + isNode);
		return isNode;
	} // isClusterNode

	// look on localnode
	// does not require cluster mode (obviously)
	public static boolean isInClusterMode(Logger logger)
		throws DataException {
		logger.info("-- ENTER --");
		InvocationStatus exitStatus = null;
		String[] cmd = { "/usr/sbin/clinfo" };
		String nodename = "localhost";
		boolean mode = true;
		boolean usingCacao = false; // always go localhost
		try {
			exitStatus = CommandInvoker.execute(usingCacao, cmd,
			    nodename);
			if (exitStatus.getExitValue().intValue() != 0) {
				mode = false;
			} else {
				mode = true;
			}
		} catch (InvocationException iex) {
			logger.warning("InvocationException: " + iex);
			throw new DataException(iex);
		}
		logger.info("isInClusterMode: " + mode);
		return mode;
	} // isInClusterMode

	// look on localnode
	// does not require cluster mode
	// separate utility since its needed before data srcs are set up
	public static String getLocalHostID(Logger logger)
		throws DataException {
		logger.info("-- ENTER --");
		InvocationStatus exitStatus = null;
		String[] cmd = { "/usr/bin/hostid" };
		String nodename = "localhost";
		String hostid = "unknown"; // no I18b
		boolean usingCacao = false; // always go localhost
		try {
			exitStatus = CommandInvoker.execute(usingCacao, cmd,
			    nodename);
			logger.fine("exitStatus.getExitValue(): " +
			    exitStatus.getExitValue());
			if (exitStatus.getExitValue().intValue() == 0) {
				hostid = exitStatus.getStdout().trim();
					// trailing newline
			} else {
				// just report as unknown
				logger.warning("unable to determine hostid");
			}
		} catch (InvocationException iex) {
			logger.warning("InvocationException: " + iex);
			throw new DataException(iex);
		}
		logger.info("hostid: " + hostid);
		return hostid;
	} // getLocalHostID



	// get the executable name of the VALIDATE method
	// use scha cmd therefore localhost is OK
	// requires clustermode
	public static String getValidate(String rtName, Logger logger) {
		String validate = null;
		String rtBase = ClusterUtils.getRTStringParam(rtName,
		    "RT_BASEDIR", logger);
		String val = ClusterUtils.getRTStringParam(rtName,
		    "VALIDATE", logger);
		if (rtBase != null && val != null) {
			validate = rtBase + File.separator + val;
		}
		logger.finest("rtBase: " + rtBase);
		logger.finest("val: " + val);
		logger.finest("validate: " + validate);

		return validate;
	} // getValidate



	// must run on target node
	// requires clustermode
	public static InvocationStatus invokeValidate(
		String nodename, String rsName, String rgName, String rtName,
		String validate, boolean usingCacao, Logger logger)
			throws DataException {
		String[] cmd =
		    { validate, "-u", "-G", rgName,
		      "-R", rsName, "-T", rtName };
		InvocationStatus status = Utils.runCommand(nodename, cmd,
		    usingCacao);
		return status;
	} // invokeValidate

	// use scha cmd therefore localhost is OK
	// it is callers responsibility to send in a param that
	// will yield a simple String result
	// requires clustermode
	public static String getRTStringParam(String rtName, String param,
	    Logger logger) {
		String res = null;
		try {
			String[] cmd =
			    { SCHA_RT_GET, "-O", param, "-T", rtName };
			String[] result = Utils.runCommandStrArray(cmd);
			res = result[0];
		} catch (DataException e) {
			// no logging...
			// return null; caller must handle it
		}
		// ignoring return code; assuming single string return
		// s/b using InvocationStatus?  XXX
		return res;
	} // getRTStringParam


	// use scha cmd therefore localhost is OK
	// requires clustermode
	public static String[] getRGStringParam(String rgName, String param,
	    Logger logger) {
		String[] result = null;
		try {
			String[] cmd =
			    { SCHA_RG_GET, "-O", param, "-G", rgName };
			result = Utils.runCommandStrArray(cmd);
			for (int i = 0; i < result.length; i++) {
				logger.finest("result: " + result[i]);
			}
		} catch (DataException e) {
			//  no logging...
			// return null; caller must handle it
		}
		// ignoring return code; assuming single string return
		// s/b using InvocationStatus?  XXX
		return result;
	} // getRGStringParam

	public static String rgNametoCCRFilename(String rgName) {
		return "/etc/cluster/ccr/rgm_rg_" + rgName;
	}

	public static String rtNametoCCRFilename(String rtName) {
		return "/etc/cluster/ccr/rgm_rt_" + rtName;
	}


	/*
	 * Get LiveClusterDataSrc for given nodename.
	 *
	 * if nodename not available this method hangs for a few minutes
	 * but eventually breaks free with an acceptable error message.
	 * Any way to test in advance that there's
	 * an mbean server there? Maybe in processArgs()? XXX
	 *
	 * nodename may actually be commandNode but we're still going
	 * through mbean
	 */
	public static LiveClusterDataSrcMBean getRemoteLiveClusterDataSrc(
		String nodename, Logger logger) throws CfgchkRemoteException {
		logger.info("-- ENTER -- >" + nodename + "<");

		/*
		 * fetch LiveClusterDataSrc mbean proxy for specified node:
		 * get jmx connection, mbean server, mbean
		 */

		LiveClusterDataSrcMBean lcdsMBean = null;

		JMXConnector jmxc = JMXUtils.getJMXConnection(logger, nodename);
		if (jmxc == null) {
			logger.warning("No JMX connection for " + nodename);
			throw new CfgchkRemoteException(
				"Unable to connect to " + nodename);
		}

		MBeanServerConnection mbsc = null;
		try {
			mbsc = jmxc.getMBeanServerConnection();
		} catch (IOException ioex) {
			logger.warning("IOException getting mbeanserver for " +
			    nodename);
			JMXUtils.closeJMXConnection(logger, jmxc);
			throw new CfgchkRemoteException(
				"Unable to connect to server on " + nodename);
		}
		if (mbsc == null) {
			logger.warning("Null MBeanServerConnection; " +
			    "no mbeanserver for " + nodename);
			JMXUtils.closeJMXConnection(logger, jmxc);
			throw new CfgchkRemoteException(
				"Unable to connect to server on " + nodename);
		}

		logger.info("getting LiveClusterDataSrcMBean for " + nodename);
		lcdsMBean = getLiveClusterDataSrcMBean(mbsc, logger);
		logger.info("got LiveClusterDataSrcMBean for " + nodename);

		if (lcdsMBean == null) {
			logger.warning("Unable to get LiveClusterDataSrcMBean " +
			    "proxy for " + nodename);

			JMXUtils.closeJMXConnection(logger, jmxc);
			throw new CfgchkRemoteException(
				"Unable to connect to server on " + nodename);
		}

		logger.info("-- EXIT -- " + nodename);
		return lcdsMBean;
	} // getRemoteLiveClusterDataSrc


	// returns null on any error
	private static LiveClusterDataSrcMBean getLiveClusterDataSrcMBean(
			MBeanServerConnection mbsc, Logger logger) {
		LiveClusterDataSrcMBean lcdsMBean = null;
		logger.info(" -- ENTER -- ");

		try {
			ObjectNameFactory onf;
			onf = new ObjectNameFactory(Globals.DOMAIN_CFGCHK);

			ObjectName objName =
			    /* BEGIN JSTYLED */
			    onf.getObjectName(Class.forName("com.sun.cluster.cfgchk.datasrc.LiveClusterDataSrcMBean"), null);
			/* END JSTYLED */
			logger.info("Domain object handle OK");

			if (!mbsc.isRegistered(objName)) {
				logger.warning("LiveClusterDataSrcMBean not " +
				    "registered with the mbean server");
				return null;
			}

			logger.info("getting LiveClusterDataSrcMBean");
			/* BEGIN JSTYLED */
			lcdsMBean = (LiveClusterDataSrcMBean)
				MBeanServerInvocationHandler.newProxyInstance(
				    mbsc,
				    objName,
				    com.sun.cluster.cfgchk.datasrc.LiveClusterDataSrcMBean.class,
				    false);
			/* END JSTYLED */

			logger.info("got LiveClusterDataSrcMBean");

			// This call is added to check for
			// RuntimeMBeanException : see 6254948
 			String x = lcdsMBean.getDataSrcName();
			logger.info("getName OK: " + x + "<");
			x = lcdsMBean.getSystemName();
			logger.info("getSystemName OK: " + x + "<");

			logger.info("Got LiveClusterDataSrcMBean proxy OK");
		} catch (IOException ex) {
			// should not occur
			logger.warning("IOException: using mbean proxy for" +
			    " LiveClusterDataSrcMBean: " + ex.toString());
			LoggerUtils.exception(
				"ClusterUtils.getLiveClusterDataSrcMBean()",
				ex, logger);
			lcdsMBean = null;
		} catch (java.lang.reflect.UndeclaredThrowableException ex) {
			// should not occur
			logger.warning("UndeclaredThrowableException: Unable " +
			    "to get mbean proxy for LiveClusterDataSrcMBean: " +
			    ex.toString());
			Throwable t = ex.getCause();
			LoggerUtils.exception(
				"Cfgchk.getLiveClusterDataSrcMBean()" +
					" UndeclaredThrowableException: ", ex,
					logger);
            LoggerUtils.exception("Cfgchk.getLiveClusterDataSrcMBean() " +
		"contained Throwable: ", t, logger);
            lcdsMBean = null;
		} catch (Exception ex) {
			// should not occur
			logger.warning("Exception: Unable to get mbean proxy " +
			    "for LiveClusterDataSrcMBean: " + ex.toString());
			LoggerUtils.exception(
				"ClusterUtils.getLiveClusterDataSrcMBean()",
				 ex, logger);
			lcdsMBean = null;
		}

		logger.info("getLiveClusterDataSrcMBean() -- EXIT --");
		return lcdsMBean;
	} // getLiveClusterDataSrcMBean

} // class

