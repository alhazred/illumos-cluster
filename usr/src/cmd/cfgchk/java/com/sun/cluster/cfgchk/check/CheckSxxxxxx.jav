/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/* 
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)CheckSxxxxxx.jav	1.1	08/06/02 SMI"
 */
package com.sun.cluster.cfgchk.check;

import com.sun.cluster.cfgchk.Check;
import com.sun.cluster.cfgchk.BasicCheck;
import com.sun.cluster.cfgchk.CheckExecutionException;
import com.sun.cluster.cfgchk.utils.logger.LoggerUtils;

/**
 * Sxxxxxx -  (Severity)
 * 
 * Single node check.
 * Problem Statement: 
 */

/*
 * No I18n of any strings in this class.
 * Exceptions thrown from Checks are caught and handled by Engine
 * typically just by logging. Messages are never displayed to user.
 *
 * Lines with trailing 'TOC' comment are auto-gathered into
 * table of contents listing of all checks.
 */

public class CheckSxxxxxx extends BasicCheck {
    
    
    public CheckSxxxxxx() {
        checkID = "Sxxxxxx"; // TOC
        severity = Check.CRITICAL; // TOC

	// sccs keyword: must be 1.1 when checked out under sccs
        version = "1.1";
	// sccs keyword: must be 08/06/02 when checked out under sccs 
        revisionDate = "09/12/08";
        
        keywords.add("SunCluster");
        
	/* BEGIN JSTYLED */
        problemStatement = "Skeleton for single-node check: " + checkID; // TOC
	/* END JSTYLED */

    } // ()

    /**
     * @return a string describing the logic of the applicability test
     */
    protected String assignApplicabilityLogic() {
	/* BEGIN JSTYLED */
		String aLogic = "Always applies to Sun Cluster nodes.";
	/* END JSTYLED */
        return aLogic; 
    } // assignApplicabilityLogic
    

    /**
     * @return a string describing the logic of the check
     */
    protected String assignCheckLogic() {
	/* BEGIN JSTYLED */
        String cLogic = "" + problemStatement;
	/* END JSTYLED */
        return cLogic; 
    } // assignCheckLogic
    

     public String getAnalysis() { 
	/* BEGIN JSTYLED */
	/* END JSTYLED */
         return checkID;
    } // getAnalysis

	public String getRecommendations() {
	/* BEGIN JSTYLED */
	/* END JSTYLED */
		return "" + checkID;
	} // getRecommendations

 
    // applicable when...
    public boolean isApplicable() throws CheckExecutionException {
        logger.info("isApplicable() -- ENTER --");
        boolean applicable = true;

        return applicable;
    } // isApplicable
    
    public boolean doCheck() throws CheckExecutionException {

        logger.info("-- ENTER --");
        boolean passes = false;
	try {

		;
		;

	} catch (Exception e) {
		logger.warning("Exception: " + e.getMessage());
		LoggerUtils.exception("Exception", e, logger);
            throw new CheckExecutionException(e);
	}

        logger.info("passes: " + passes);
        logger.info("-- EXIT --");
        return passes;
    } // doCheck

    
} // class

