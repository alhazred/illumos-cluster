/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)CheckSTestXXX.java	1.1	08/05/29 SMI"
 */
package com.sun.cluster.cfgchk.check.testchecks;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;


import com.sun.cluster.cfgchk.Check;
import com.sun.cluster.cfgchk.BasicCheck;
import com.sun.cluster.cfgchk.CheckExecutionException;

import com.sun.cluster.cfgchk.datasrc.DataException;

import com.sun.cluster.cfgchk.datasrc.dataObject.VfstabEntry;
import com.sun.cluster.cfgchk.datasrc.fileObject.Vfstab;

import com.sun.cluster.cfgchk.utils.StringUtils;
import com.sun.cluster.cfgchk.utils.logger.LoggerUtils;


/**
 * Skeleton for one-off development testing
 */

// No I18n

public class CheckSTestXXX extends BasicCheck {

	private StringBuffer reportOutput = new StringBuffer(
		"Data gethering exercise:\n");

    public CheckSTestXXX() {
        checkID = "STestXXX";
        severity = Check.INFORMATION;

		// sccs keyword: must be 1.1 when checked out under sccs
		version = "1.1";
		// sccs keyword: must be 08/05/29 when checked out under sccs
		revisionDate = "09/12/08";

        keywords.add("SunCluster3.x");
	keywords.add("whitebox test");
        keywords.add("test");

	/* BEGIN JSTYLED */
        problemStatement = "Test check: development tool for data processing. Violated in order to get output into report. Severity INFORMATION.";
	/* END JSTYLED */

    } // ()

    /**
     * @return a string describing the logic of the applicability test
     */
    protected String assignApplicabilityLogic() {
        String aLogic = "Always applies to Sun Cluster 3.x node.";
        return aLogic;
    } // assignApplicabilityLogic


    /**
     * @return a string describing the logic of the check
     */
    protected String assignCheckLogic() {
        String cLogic = "Development aid.";
        return cLogic;
    } // assignCheckLogic


	/**
	 * @return a string for use in output report analyzing the problem
	 * may contain dynamic data
	 */
	public String getAnalysis() {
		return reportOutput.toString();
	} // getAnalysis

	public String getRecommendations() {
		return checkID;
	} // getRecommendations


    // always
    public boolean isApplicable() throws CheckExecutionException {
        logger.info("isApplicable() -- ENTER --");
        boolean applicable = true;

        return applicable;
    } // isApplicable

    public boolean doCheck() throws CheckExecutionException {

        logger.info("-- ENTER --");
        boolean passes = false; // in order to get output to report


	try {
		reportOutput.append("\n\n\t STestXXX output  START \n");

		Vfstab vfstab = null;
		String infoMsg = null;
		String nodename = getNodename();
		logger.info("loading Vfstab for " + nodename);
		vfstab = getDataSrc().getVfstab(nodename);
		VfstabEntry[] vfstabEntries = vfstab.getEntries();

		infoMsg = vfstab.getInfoMessage();
		reportOutput.append("\t infoMsg: >>" + infoMsg + "<<\n");

		if ((infoMsg != null) && !infoMsg.equals("")) {
			appendInfoMsg(infoMsg);
			logger.info("infoMessage from load vfstab: " + infoMsg);
		}

		for (int i = 0; i < vfstabEntries.length; i++) {
			VfstabEntry entry = vfstabEntries[i];
			if (entry == null) { continue; }
			// possible for global entries array to be oversized
			// and have null entries
			reportOutput.append("\t" + entry + "\n");
		} // for entries



		logger.info("\n STestXXX output  END ======================\n");
		reportOutput.append("\t STestXXX output  END ");

	} catch (DataException ex) {
		logger.severe("DataException: " + ex);
		LoggerUtils.exception("DataException", ex, logger);
		throw new CheckExecutionException(ex);
	} catch (Exception ex) {
		logger.severe("Exception: " + ex);
		LoggerUtils.exception("Exception", ex, logger);
		throw new CheckExecutionException(ex);
	}

        logger.info("passes: " + passes);
        logger.info("-- EXIT --");
        return passes;
    } // doCheck


} // class
