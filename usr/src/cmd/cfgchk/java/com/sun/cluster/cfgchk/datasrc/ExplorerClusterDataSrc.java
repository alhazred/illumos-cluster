/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)ExplorerClusterDataSrc.java	1.3	08/06/02 SMI"
 */
package com.sun.cluster.cfgchk.datasrc;

import java.io.IOException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import java.util.StringTokenizer;
import java.util.logging.Logger;

import com.sun.cluster.cfgchk.utils.StringUtils;

import com.sun.cluster.cfgchk.datasrc.fileObject.TextFile;
import com.sun.cluster.cfgchk.i18n.I18n;

/**
 *
 * This is an implementation of interface ClusterDataSrc which knows
 * how to access to cluster-realted  files and commands from an
 * explorer archive.
 *
 * This code is always executing on the same node
 * that launched the command. Cacao is never used
 * so there is no mbean interface needed.
 *
 * Data cache in superclass.
 *
 * Explorer archives are all files: copies of files
 * and files of command output. This class and its
 * subclasses use the ExplorerMapping class to map
 * desired filenames and commands to archive filenames
 * which are then fetched and returned or data
 * extracted and returned as appropriate.
 *
 */
public class ExplorerClusterDataSrc extends ExplorerSolarisDataSrc
	implements ClusterDataSrc {

	// RGM data support
	private HashMap rgNames = null;
	private HashMap rtNames =  null;
	private HashMap rsFromRG =  null;
	private HashMap rtFromRGandRS =  null;
	private HashMap rgNodelist =  null;


	public ExplorerClusterDataSrc(String basedir, Logger logger) {
		super(basedir, logger);
		logger.info(dsName + ":  -- ENTER --");
	}

	// ===  start interface ExplorerClusterDataSrc  ===


	public String getNodeID() throws DataException {
		String nodeid = null;
		String cacheKey = "getNodeID";
		logger.info(dsName + ": -- ENTER --" + cacheKey);

		try {
			nodeid = dataCache.getString(cacheKey);
			if (nodeid == null) {
				logger.info(dsName + ": not found in cache");
				TextFile tf = null;

				try { // get TextFile
					String filename = "/etc/cluster/nodeid";
					String mappedName =
					    ExplorerMapping.get(filename);
					if (mappedName == null) {
						String[] i18nArgs =
						    { filename };
						/* BEGIN JSTYLED */
						String lMsg = I18n.getLocalized(
							logger,
							"internal.error.no.explorer.mapping",
							i18nArgs);
						/* END JSTYLED */
						throw new DataException(lMsg);
					}
					String fullPath = basedir +  mappedName;
					logger.info(dsName + ": fullPath: " +
					    fullPath);
					tf = new TextFile(fullPath);
						// FNFEx, IOEx

					// assumed to be a single line
					nodeid = tf.getLines()[0];
						// just take whatever's there
					logger.info(dsName + ": nodeid: " +
					    nodeid);
					dataCache.put(cacheKey, nodeid);
					logger.info(dsName +
					    ": String added to cache");

				} catch (IOException ioex) {
					String[] i18nArgs =
					    { ioex.getMessage() };
					/* BEGIN JSTYLED */
					String lMsg = I18n.getLocalized(logger,
					    "internal.error.cant.read.explorerfile",
					    i18nArgs);
					/* END JSTYLED */
					throw new DataException(lMsg);
				}
			} // == null
		} catch (DataException dex) { // already I18n; bubble it up
			logger.warning(dsName + ": DataException: " +
			    dex.getMessage());
			throw dex;
		}
		logger.info(dsName + ": nodeid: " + nodeid);
		return nodeid;
	} // getNodeID

	public String getClustername() throws DataException {
		String clName = null;
		String cacheKey = "getClustername";
		logger.info(dsName + ": -- ENTER --" + cacheKey);

		try {
			clName = dataCache.getString(cacheKey);
			if (clName == null) {
				logger.info(dsName + ": not found in cache");
				TextFile tf = null;

				try { // get TextFile
					String cmdName =
					    "/usr/cluster/bin/scconf -pv";
					String mappedName =
					    ExplorerMapping.get(cmdName);
					if (mappedName == null) {
						String[] i18nArgs = { cmdName };
						/* BEGIN JSTYLED */
						String lMsg = I18n.getLocalized(
							logger,
							"internal.error.no.explorer.mapping",
							i18nArgs);
						/* END JSTYLED */
						throw new DataException(lMsg);
					}
					String fullPath = basedir + mappedName;
					logger.info(dsName + ": fullPath: " +
					    fullPath);
					tf = new TextFile(fullPath);
						// FNFEx, IOEx

					// clName is third word from first line
					String line = tf.getLines()[0];
					logger.info(dsName + ": scNameLine: " +
					    line);
					StringTokenizer st =
					    new StringTokenizer(line);
					if (st.countTokens() < 3) {
						String eMsg = "scNameLine: " +
						    "insufficient tokens";
							// No I18n
						logger.info(eMsg);
						String[] i18nArgs = { eMsg };
						/* BEGIN JSTYLED */
						String lMsg =
						    I18n.getLocalized(logger,
							"internal.error.cmd.unexpected.result",
							i18nArgs);
						/* END JSTYLED */
						throw new DataException(lMsg);
					}

					st.nextToken(); // skip first word
					st.nextToken(); // skip second word
					clName = st.nextToken();
						// third word is version
					dataCache.put(cacheKey, clName);
					logger.info(dsName +
					    ": String added to cache");

				} catch (IOException ioex) {
					String[] i18nArgs =
					    { ioex.getMessage() };
					/* BEGIN JSTYLED */
					String lMsg = I18n.getLocalized(logger,
					    "internal.error.cant.read.explorerfile",
					    i18nArgs);
					/* END JSTYLED */
					throw new DataException(lMsg);
				}
			} // == null
		} catch (DataException dex) { // already I18n; bubble it up
			logger.warning(dsName + ": DataException: " +
			    dex.getMessage());
			throw dex;
		}
		logger.info(dsName + ": clName: " + clName);
		return clName;
	} // getClustername

	public String getClusterVersion() throws DataException {
		String version = null;
		String cacheKey = "getClusterVersion";
		logger.info(dsName + ": -- ENTER --" + cacheKey);

		try {
			version = dataCache.getString(cacheKey);
			if (version == null) {
				logger.info(dsName + ": not found in cache");
				TextFile tf = null;

				try { // get TextFile
					String fileName =
					    "/etc/cluster/release";
					String mappedName =
					    ExplorerMapping.get(fileName);
					logger.info(dsName + ": mappedName: " +
					    mappedName);

					if (mappedName == null) {
						String[] i18nArgs =
						    { fileName };
						/* BEGIN JSTYLED */
						String lMsg =
						    I18n.getLocalized(logger,
							"internal.error.no.explorer.mapping",
							i18nArgs);
						/* END JSTYLED */
						throw new DataException(lMsg);
					}

					String fullPath = basedir + mappedName;
					logger.info(dsName + ": fullPath: " +
					    fullPath);
					tf = new TextFile(fullPath);

					// version is third word from first line
					String line = tf.getLines()[0];
					logger.info(dsName +
					    ": scVersionLine: " + line);
					StringTokenizer st =
					    new StringTokenizer(line);
					if (st.countTokens() < 3) {
						String eMsg =
						    "scVersionLine: " +
						    "insufficient tokens";
							// No I18n
						logger.info(eMsg);
						String[] i18nArgs = { eMsg };
						/* BEGIN JSTYLED */
						String lMsg =
						    I18n.getLocalized(logger,
							"internal.error.cmd.unexpected.result",
							i18nArgs);
						/* END JSTYLED */
						throw new DataException(lMsg);
					}
					st.nextToken(); // skip first word
					st.nextToken(); // skip second word
					version = st.nextToken();
						// third word is version
					dataCache.put(cacheKey, version);
					logger.info(dsName +
					    ": String added to cache");

				} catch (IOException ioex) {
					String[] i18nArgs =
					    { ioex.getMessage() };
					/* BEGIN JSTYLED */
					String lMsg = I18n.getLocalized(logger,
					    "internal.error.cant.read.explorerfile",
					    i18nArgs);
					/* END JSTYLED */
					throw new DataException(lMsg);
				}
			} // == null
		} catch (DataException dex) { // already I18n; bubble it up
			logger.warning(dsName + ": DataException: " +
			    dex.getMessage());
			throw dex;
		}

		logger.info(dsName + ": cluster version: " + version);
		return version;
	} // getClusterVersion


	public boolean isClusterMode(boolean usingCacao) throws DataException {
		return false;
	} // isClusterMode



	public  String[] getRGNames() throws DataException {
		String[] result = null;
		String cacheKey = "getRGNames";
		logger.info(dsName + ": -- ENTER --" + cacheKey);

		try {
			result = dataCache.getStringArray(cacheKey);
			if (result == null) {
				logger.info(dsName + ": not found in cache");

				try {
					result = getDataRGNames();
					dataCache.put(cacheKey, result);
					logger.info(dsName +
					    ": StringArray added to cache");

				} catch (DataException dex) {
					throw dex;
				}
			} // == null
		} catch (DataException dex) { // already I18n; bubble it up
			logger.warning(dsName + ": DataException: " +
			    dex.getMessage());
			throw dex;
		}
		logger.fine(dsName + ": getRGNames: " +
		    StringUtils.stringArrayToString(result, "; "));
		return result;
	} // getRGNames

	public  String[] getRGNodelist(String rgName) throws DataException {
		String[] result = null;
		String cacheKey = "getRGNodelist :-: " + rgName;
		logger.info(dsName + ": -- ENTER --" + cacheKey);

		try {
			result = dataCache.getStringArray(cacheKey);
			if (result == null) {
				logger.info(dsName + ": not found in cache");

				try {
					result = getDataRGNodelist(rgName);
					dataCache.put(cacheKey, result);
					logger.info(dsName +
					    ": StringArray added to cache");

				} catch (DataException dex) {
					throw dex;
				}
			} // == null
		} catch (DataException dex) { // already I18n; bubble it up
			logger.warning(dsName + ": DataException: " +
			    dex.getMessage());
			throw dex;
		}
		logger.fine(dsName + ": getRGNames: " +
		    StringUtils.stringArrayToString(result, "; "));
		return result;
	} // getRGNodelist

	public  String[] getRSNames(String rgName) throws DataException {
		String[] result = null;
		String cacheKey = "getRSNames :-: " + rgName;
		logger.info(dsName + ": -- ENTER --" + cacheKey);

		try {
			result = dataCache.getStringArray(cacheKey);
			if (result == null) {
				logger.info(dsName + ": not found in cache");

				try {
					result = getDataRSNames(rgName);
					dataCache.put(cacheKey, result);
					logger.info(dsName +
					    ": StringArray added to cache");

				} catch (DataException dex) {
					throw dex;
				}
			} // == null
		} catch (DataException dex) { // already I18n; bubble it up
			logger.warning(dsName + ": DataException: " +
			    dex.getMessage());
			throw dex;
		}
		logger.fine(dsName + ": getRGNames: " +
		    StringUtils.stringArrayToString(result, "; "));
		return result;
	} // getRSNames

	public  String getRTName(String rsName, String rgName)
		throws DataException {
		String result = null;
		String cacheKey = "getRTName :-: " + rsName + " " + rgName;
		logger.info(dsName + ": -- ENTER --" + cacheKey);

		try {
			result = dataCache.getString(cacheKey);
			if (result == null) {
				logger.info(dsName + ": not found in cache");

				try {
					result = getDataRTNameByRSRG(rsName,
					    rgName);
					dataCache.put(cacheKey, result);
					logger.info(dsName +
					    ": String added to cache");

				} catch (DataException dex) {
					throw dex;
				}
			} // == null
		} catch (DataException dex) { // already I18n; bubble it up
			logger.warning(dsName + ": DataException: " +
			    dex.getMessage());
			throw dex;
		}
		logger.fine(dsName + ": getRGNames: " + result);
		return result;
	} // getRTName

	public  String[] getRegisteredRTNames() throws DataException {

		String[] result = null;
		String cacheKey = "getRegisteredRTNames";
		logger.info(dsName + ": -- ENTER --" + cacheKey);

		try {
			result = dataCache.getStringArray(cacheKey);
			if (result == null) {
				logger.info(dsName + ": not found in cache");

				try {
					result = getDataRTNames();
					dataCache.put(cacheKey, result);
					logger.info(dsName +
					    ": StringArray added to cache");

				} catch (DataException dex) {
					throw dex;
				}
			} // == null
		} catch (DataException dex) { // already I18n; bubble it up
			logger.warning(dsName + ": DataException: " +
			    dex.getMessage());
			throw dex;
		}
		logger.fine(dsName + ": getRegisteredRTNames: " +
		    StringUtils.stringArrayToString(result, "; "));
		return result;
	} // getRegisteredRTNames



	// ===  end interface ExplorerClusterDataSrc  ===

	// ===  support for loading RGM data  ===

	// rgName as key & value
  	private String[] getDataRGNames() throws DataException {
		logger.finest("-- ENTER --");
		if (rgNames == null) {
			loadRGMData(); // dex
		}
		// rgNames may be empty but not null
		String[] result = new String[rgNames.size()];
		Set keySet = rgNames.keySet();
		Iterator it = keySet.iterator();
		int index = 0;
		while (it.hasNext()) {
			String key = (String)it.next();
			logger.finest("rgName: " + key);
			result[index++] = key;
		} // it
		logger.finest("-- EXIT --");
		return result;
	} // getDataRGNames

  	private String[] getDataRGNodelist(String rgName)
		throws DataException {
		logger.finest("-- ENTER --");
		if (rgNodelist == null) {
			loadRGMData(); // dex
		}
		// rgNodelist may be empty but not null
		String[] result = new String[0];

		String value = (String)rgNodelist.get(rgName);
		logger.finest("nodelist for " + rgName + ": " + value);
		if (value != null) {
			result =
			    StringUtils.stringArrayFromEncodedString(value);
		}

		logger.finest("-- EXIT --");
		return result;
	} // getDataRGNodelist

  	private String[] getDataRSNames(String rgName) throws DataException {
		logger.finest("-- ENTER --");
		if (rsFromRG == null) {
			loadRGMData(); // dex
		}
		// rsFromRG may be empty but not null
		String[] result = new String[0];

		HashMap value = (HashMap)rsFromRG.get(rgName);
		// value holds rs names for particular rgName
		logger.finest("rs names HashMap for " + rgName + ": " + value);
		if (value != null) {
			// value hashmap should always have at least one elt
			result = new String[value.size()];
			Set keySet = value.keySet();
			Iterator it = keySet.iterator();
			int index = 0;
			while (it.hasNext()) {
				String key = (String)it.next();
				logger.finest("rs name: " + key);
				result[index++] = key;
			} // it
		} // value != null
		logger.finest("-- EXIT --");
		return result;
	} // getDataRSNames

  	private String getDataRTNameByRSRG(String rsName, String rgName)
		throws DataException {
		logger.finest("-- ENTER --");
		if (rtFromRGandRS == null) {
			loadRGMData(); // dex
		}
		// rtFromRGandRS may be empty but not null

		String result = "unknown"; // no I18n
		String value = (String)rtFromRGandRS.get(rsName + " " + rgName);
		// key form must match loadRGMData()
		logger.finest("rt type  for " + rsName + " in " + rgName +
		    ": " + value);
		if (value != null) {
			result = value;
		}

		logger.finest("-- EXIT --");
		return result;
	} // getDataRTNameByRSRG

  	private String[] getDataRTNames() throws DataException {
		logger.finest("-- ENTER --");
		if (rtNames == null) {
			loadRGMData(); // dex
		}
		// rgNames may be empty but not null
		String[] result = new String[rtNames.size()];
		Set keySet = rtNames.keySet();
		Iterator it = keySet.iterator();
		int index = 0;
		while (it.hasNext()) {
			String key = (String)it.next();
			logger.finest("rtNames: " + key);
			result[index++] = key;
		} // it
		logger.finest("-- EXIT --");
		return result;
	} // getDataRTNames


	// read through scrgadm output
	// constructing useful collections
	private void loadRGMData() throws DataException {
		logger.finest("-- ENTER --");

		rgNames = new HashMap();
		// write rgName as key & value

		rtNames =  new HashMap();
		// write rtName as key & value

		rsFromRG =  new HashMap();
		// key is rgName
		// value is hashmap with rsname as key & value

		rtFromRGandRS =  new HashMap();
		// key is rsName + " "+ rgName
		// value is rtName

		rgNodelist =  new HashMap();
		// key is rgName
		// value is nodelist as String of <space> separated nodenames

		TextFile tf = null;
		try { // Exception
			try {
				tf = getScrgadmOutput(); // dex
				String[] lines = tf.getLines();

			/*
			 * process scrgadm-pv.out:
			 * section 1 is RT's
			 * section 2 is RG's with RS's embedded
			 * embedded RS tells us its RT
			 */
				String RTNAME     = "Res Type name:";
				String RGNAME     = "Res Group name:";
				String RGNODELIST = "Res Group Nodelist:";
				String RSNAME     = "Res name:";
				String RSTYPE     = "Res resource type:";

				String lastRGSeen = null;
				String lastRSSeen = null;

				for (int i = 0; i < lines.length; i++) {
					String line = lines[i];
					logger.finest("line: " + line);

					// registered RT name
					if (line.indexOf(RTNAME) != -1) {
						String rtName =
						    StringUtils.getField(line,
							4);
						rtNames.put(rtName, rtName);
						logger.finest("captured rt: " +
						    rtName);
					}

					// RG name
					if (line.indexOf(RGNAME) != -1) {
						String rgName =
						    StringUtils.getField(line,
							4);
						rgNames.put(rgName, rgName);
						lastRGSeen = rgName;
						logger.finest("captured rg: " +
						    rgName);
					}

					// RG nodelist
					if (line.indexOf(RGNODELIST) != -1) {
						int nf =
						    StringUtils.getNumFields(
							    line);
						String nodelist =
						    StringUtils.
						    getFieldsXthroughY(line,
							5, nf, " ");
						rgNodelist.put(lastRGSeen,
						    nodelist);
						logger.finest(
							"captured nodelist: " +
								nodelist);
					}

					// RS name
					if (line.indexOf(RSNAME) != -1) {
						String rsName =
						    StringUtils.getField(line,
							4);
						HashMap rss =
						    (HashMap)rsFromRG.get(
							    lastRGSeen);
						if (rss == null)
							{ rss = new HashMap(); }
						rss.put(rsName, rsName);
						rsFromRG.put(lastRGSeen, rss);
						lastRSSeen = rsName;
						logger.finest("captured rs: " +
						    rsName);
					}

					// RS type
					if (line.indexOf(RSTYPE) != -1) {
						String rsrt =
						    StringUtils.getField(line,
							5);
						rtFromRGandRS.put(lastRSSeen +
						    " " + lastRGSeen, rsrt);
						// key form must match
						// getDataRTNameByRSRG()
						logger.finest(
							"captured rs type: " +
								rsrt);
					}

				} // for lines
			} catch (DataException dex) {
				throw dex;
			}

		} catch (Exception ex) {
			logger.warning("Exception: " + ex);
			throw new DataException(ex);
		}

		logger.fine("-- EXIT --");
	} // loadRGMData

	private TextFile getScrgadmOutput() throws DataException {
		TextFile tf = null;
		try {
			String cmdName = "/usr/cluster/bin/scrgadm -pv";
			String mappedName = ExplorerMapping.get(cmdName);
			if (mappedName == null) {
				String[] i18nArgs = { cmdName };
				String lMsg = I18n.getLocalized(logger,
				    "internal.error.no.explorer.mapping",
				    i18nArgs);
				throw new DataException(lMsg);
			}
			String fullPath = basedir +  mappedName;
			logger.info("fullPath: " + fullPath);
			tf = new TextFile(fullPath); // FNFEx, IOEx

		} catch (IOException ioex) {
			String[] i18nArgs = { ioex.getMessage() };
			String lMsg = I18n.getLocalized(logger,
			    "internal.error.cant.read.explorerfile", i18nArgs);
			throw new DataException(lMsg);
		}
		return tf;
	} // getScrgadmOutput

} // class
