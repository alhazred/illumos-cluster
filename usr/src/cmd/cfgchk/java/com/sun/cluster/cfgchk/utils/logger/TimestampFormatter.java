/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)TimestampFormatter.java	1.2	08/05/29 SMI"
 */
package com.sun.cluster.cfgchk.utils.logger;

import java.util.Date;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;

import com.sun.cluster.cfgchk.utils.StringUtils;

/**
 * Custom logger record formatter
 */
public class TimestampFormatter extends Formatter {

	private static String SEP = " | ";

	    public String format(LogRecord record) {
		String level = StringUtils.padString(
		    record.getLevel().toString(), 7, ' ');
	        String s = "   " + level + SEP +
			Thread.currentThread().getName() + SEP +
			new Date().toString() + SEP +
			record.getSourceClassName() + "." +
			record.getSourceMethodName() + "():\n" +
			record.getMessage() + "\n\n";
	        return s;
	    }


	    public String format2(LogRecord record) {
	        String s = "   " + new Date().toString() + SEP +
			record.getSourceClassName() + "." +
			record.getSourceMethodName() + "():\n" +
			record.getMessage() + "\n\n";
	        return s;
	    }


	    public String format1(LogRecord record) {
	        String s = new Date().toString() + SEP + record.getMessage() +
		    "\n";
	        return s;
	    }

} // class
