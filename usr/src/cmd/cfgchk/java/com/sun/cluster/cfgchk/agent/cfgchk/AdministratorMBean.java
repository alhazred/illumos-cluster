/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)AdministratorMBean.java	1.2	08/05/29 SMI"
 */
package com.sun.cluster.cfgchk.agent.cfgchk;

// JDK imports
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;

import java.util.Map;

import com.sun.cluster.cfgchk.agent.cfgchk.results.CommandResult;

/**
 * Entry point for cacao based calls to the cfgchk system.
 */

public interface AdministratorMBean {

	// Part of the contract with the ObjectNameFactory
	final public static String TYPE = "Administrator";

	public int execute(String args, InputStream in, OutputStream out,
	    OutputStream err, Map env) throws Exception;

	public CommandResult runEngine(String encodedArgs, PrintWriter stdout,
	    PrintWriter stderr, boolean fromGui);

	/**
	 * Get the software version
	 *
	 * @return the version string.
	 */
	public String getVersion();

} // class
