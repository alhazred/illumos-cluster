/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)RemoteLogger.java	1.2	08/05/29 SMI"
 */
package com.sun.cluster.cfgchk.utils.logger;


import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.sun.cluster.cfgchk.Globals;
import com.sun.cluster.cfgchk.InvalidParameterException;

public class RemoteLogger {

	// Singleton per node

	/*
	 * Logger object : logs to Globals.REMOTE_LOG on local machine
	 * cacao or console logger as default fallback
	 *
	 * The 'remote' nature of this log is that it is used
	 * on the other side of an mbean call which means
	 * we don't know what host we're actually running on.
	 * Passing a logger from node1 to code executing
	 * on node2 doesn't get node2 stuff into that logger.
	 * Classes that can be called downstream of an mbean also
	 * must use the remote log.
	 */

	// cacao log: dumps to stdout if !cacao
	private static Logger cLogger =
		Logger.getLogger("com.sun.cluster.cfgchk.agent.cfgchk");

	private static Logger logger = null;
	private static FileHandler remoteFH = null;
	private static String nodename = null;

	public static Logger getRemoteLogger() {
		if (logger == null) {
			logger = initRemoteLogger("localhost",
			    Globals.DEFAULT_LOGGING_LEVEL);
		}
		return logger;
	} // getRemoteLogger


	// idempotent
	public static Logger initRemoteLogger(String nname,
	    String loggingLevel) {
		nodename = nname;
		if (logger == null) {
			try {
				startLogger(nodename, loggingLevel);
			} catch (InvalidParameterException iex) {
				logger = cLogger;
			}
		} // logger == null

		return logger;
	} // getRemoteLogger

	private static void startLogger(String nodename, String loggingLevel)
		throws InvalidParameterException {
		String fname = makeFHfname();
		boolean rotatable = true;
		remoteFH = LoggerUtils.initTimestampFileHandler(fname,
		    rotatable); // IPEx
		logger = LoggerUtils.initRemoteLogger(remoteFH, loggingLevel);
			// No Ex
		logger.info("log started on node: " + nodename + " as " +
		    fname + " at level " + loggingLevel);
	} // startLogger

	public static void setLoggingLevel(String newLevel) {
		logger.setLevel(Level.parse(newLevel));
		logger.info("set new logging level: " + newLevel);
	} // setLoggingLevel

	private static String makeFHfname() {
		return Globals.REMOTE_LOG + "." + nodename;
	}

} // class
