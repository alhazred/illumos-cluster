/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)CheckExecutionException.java	1.2	08/05/29 SMI"
 */
package com.sun.cluster.cfgchk;

/**
 * Exception thrown by Engine to wrap any Exception
 * or Error raised during Check evaluation.
 *
 * The original Throwable is available via
 * getPayloadThrowable(). It is of interest
 * primarily by LoggerUtils.exception()
 */
public class CheckExecutionException extends Exception {

	private static final long serialVersionUID = 4583005468178040300L;
	private Throwable t = null;

	public CheckExecutionException(String s) {
		super(s);
	} // ()

	public CheckExecutionException(Exception e) {
		super(e);
		t = e;
	} // ()


	public CheckExecutionException(Throwable t) {
		super(new Exception("CheckExecutionException: throwable"));
		this.t = t;
	} // ()

	public Throwable getPayloadThrowable() {
		return t;
	}


} // class
