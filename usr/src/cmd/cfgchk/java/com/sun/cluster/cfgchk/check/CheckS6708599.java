/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)CheckS6708599.java	1.2	08/06/02 SMI"
 */
package com.sun.cluster.cfgchk.check;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import com.sun.cluster.cfgchk.BasicCheck;
import com.sun.cluster.cfgchk.Check;
import com.sun.cluster.cfgchk.CheckExecutionException;

import com.sun.cluster.cfgchk.datasrc.DataException;
import com.sun.cluster.cfgchk.datasrc.BasicDataSrc;
import com.sun.cluster.cfgchk.datasrc.dataObject.MountPoint;
import com.sun.cluster.cfgchk.datasrc.dataObject.VfstabEntry;
import com.sun.cluster.cfgchk.datasrc.fileObject.Vfstab;

import com.sun.cluster.cfgchk.utils.logger.LoggerUtils;


/* BEGIN JSTYLED */
/**
 *
 * 6708599 - 2756 (variable: CRITICAL or INFO)
 *
 * Single node check.
 * "Sun Cluster node does not have a mount point for a global filesystem /etc/vfstab entry."
 *
 * Includes 'node@N' entries
 *
 * A missing nested mount point is treated as INFORMATION ONLY output.
 * Nested mount points may fall under a filesystem which is (correctly)
 * not mounted at this time. Improvement may be available here XXX.
 *
 * Not Applicable if not live data src: explorer
 *	 doesn't contain the data we need.
 *
 * See also Check6350172 - 3418 for global fs entries presence and consistency
 *
 */
/* END JSTYLED */

/*
 * No I18n of any strings in this class.
 * Exceptions thrown from Checks are caught and handled by Engine
 * typically just by logging. Messages are never displayed to user.
 *
 * Lines with trailing 'TOC' comment are auto-gathered into
 * table of contents listing of all checks.
 */

public class CheckS6708599 extends BasicCheck {

    private ArrayList missingMountPoints = null;

	public CheckS6708599() {
		checkID = "S6708599"; // TOC
		severity = Check.VARIABLE_SEV; // will be assigned CRITICAl or INFO // TOC

		// sccs keyword: must be 1.2 when checked out under sccs
		version = "1.2";
		// sccs keyword: must be 08/06/02 when checked out under sccs
		revisionDate = "09/12/08";

		keywords.add("SunCluster3");
		keywords.add("single");
		keywords.add("vfstab");
		keywords.add("global_filesystem");
		keywords.add("live_only");

		/* BEGIN JSTYLED */
		problemStatement = "Sun Cluster node does not have a mount point for a global filesystem /etc/vfstab entry."; // TOC
		/* END JSTYLED */

		// check-specific data
		missingMountPoints = new ArrayList();
    } // ()

    /**
     * @return a string describing the logic of the applicability test
     */
    protected String assignApplicabilityLogic() {

	    /* BEGIN JSTYLED */
        String aLogic = "This check is applicable only when running live on a Solaris node. This check is not applicable if taking data from an explorer archive.";
	/* END JSTYLED */
        return aLogic;
    } // assignApplicabilityLogic


    /**
     * @return a string describing the logic of the check
     */
    protected String assignCheckLogic() {
	    /* BEGIN JSTYLED */
        String cLogic = "For each global file system entry in /etc/vfstab, test to see if its mount point exists. If any mount points are missing, this check is violated. Issue INFO messages for nested mount points.";
	/* END JSTYLED */
        return cLogic;
    } // assignCheckLogic

	public String getAnalysis() {
		/* BEGIN JSTYLED */
		StringBuffer analysis = new StringBuffer("One or more global filesystem /etc/vfstab entries does not have a mount point on this node. Without this mount point, the node does not have access to its copy of the Global Namespace and/or other global filesystems.\n\n");

		if (missingMountPoints.size() > 0) {
			if (severity == Check.CRITICAL) {
				analysis.append("\tThe following mount points are present in /etc/vfstab but are missing or are not directories:\n");
			} else { // INFO
				analysis.append("\tThe following mount points are nested under other mount points:\n");
			}

			Iterator it = missingMountPoints.iterator();
			String data = null;
			while (it.hasNext()) {
				data = (String)it.next();
				analysis.append("\t\t" + data + "\n");
			}
			analysis.append("\n\t(Nested mount points are reported for informational purposes only.)");
			/* END JSTYLED */
			// XXX needs more work: research when nested is OK 
			// and when not: SAP & connectivity...
		}

		return analysis.toString();
	} // getAnalysis

    public String getRecommendations() {
	    /* BEGIN JSTYLED */
        String recommend = "" +
		"Edit /etc/vfstab to remove unneeded entries or create the mount point on all applicable nodes. Do not nest mount points unless all devices have connectivity to all nodes.";
	/* END JSTYLED */
	// clumsy phrasing XXX
        return recommend;
    } // getRecommendations


        // Applicable only with a live data source
    public boolean isApplicable() throws CheckExecutionException {
        logger.info("isApplicable() -- ENTER --");
        boolean applicable = true;

	try {
		BasicDataSrc ds = getDataSrc();
		if (!ds.isLiveDataSource()) {
			applicable = false;
			logger.info("Check is not applicable if " +
			    "not running on live system.");
		}
	} catch (Exception e) {
		logger.warning("Exception: " + e);
		LoggerUtils.exception("Exception", e, logger);
		throw new CheckExecutionException(e);
	}

        logger.info("isApplicable(): " + applicable);
        return applicable;
    } // isApplicable

	/*
	 * check for missing mount points
	 * by default this is a CRITICAl violation
	 * see if any missing mount points are nested (or under HASP ?? XXX)
	 * missing nested mount points get an INFO severity
	 *
	 */
    public boolean doCheck() throws CheckExecutionException {

        logger.info("-- ENTER --");
        boolean passes = true;

	try {
		Vfstab vfstab = null;
                String infoMsg = null;
		String nodename = getNodename();
		logger.info("loading Vfstab for " + nodename);
		vfstab = getDataSrc().getVfstab(nodename);
		infoMsg = vfstab.getInfoMessage();
		logger.info("infoMsg: >>" + infoMsg + "<<");
		if ((infoMsg != null) && !infoMsg.equals("")) {
                        appendInfoMsg(infoMsg);
                        logger.info("infoMessage from load vfstab: " + infoMsg);
		}
		VfstabEntry[] vfstabEntries = vfstab.getGlobalEntries(true);
			// do include node@ lines

		// mount point exists?
                for (int i = 0; i < vfstabEntries.length; i++) {
                    VfstabEntry entry = vfstabEntries[i];
                    if (entry == null) { continue; }
			// possible for global entries array to be
			// oversized and have null entries
                    String mountPoint = entry.getMountPoint();
		    logger.info("entry mountPoint: " + entry.getMountPoint());

		    File f = new File(mountPoint);
		    String msg = null;
		    if (!f.exists()) {
			    msg = "does not exist.";
			    int sev = Check.CRITICAL;
			    boolean nested = isNestedMountPoint(
				    mountPoint, vfstabEntries);
			    if (nested) {
				    sev = Check.INFORMATION;
				    msg = "is a nested mount point. (INFO)";
				    logger.info("set nested msg");
			    }
			    severity = maxSeverity(severity, sev);
		    } else if (!f.isDirectory()) {
			    msg = "is not a directory.";
		    }
		    if (msg != null) {
			missingMountPoints.add(mountPoint + " " + msg);
			passes = false;
			logger.info("adding " + mountPoint + " " + msg);
		    } else {
			logger.info("verified " + mountPoint);
		    }
		}

        } catch (DataException e) {
            logger.warning("DataException(): " + e);
            throw new CheckExecutionException(e);
        } catch (Exception e) {
            logger.warning("Exception(): " + e.getMessage());
            throw new CheckExecutionException(e);
        }
        logger.info("passes: " + passes);
        return passes;
    } // doCheck


	/**
	 * @return true if mountPoint
	 * is nested under some other mount ppoint
	 */
	private boolean isNestedMountPoint(String mountPoint,
	    VfstabEntry[] vfstabEntries) {
		boolean nested = false;

		logger.info("testing mountPoint: " + mountPoint);
		MountPoint mp = new MountPoint(mountPoint, logger);
		logger.info("testing mountPoint: " + mp);

		for (int i = 0; i < vfstabEntries.length; i++) {
			VfstabEntry entry = vfstabEntries[i];

			/*
			 * possible for global entries array to
			 * be oversized and have null entries
			 */
			if (entry == null) { continue; }
			MountPoint mp2 = new MountPoint(entry.getMountPoint(),
			    logger);
			logger.info("entry mp2: " + mp2);
			if (mp2.getName().equals(mp.getName())) {
				logger.info(mp + " skipping self: " + mp2);
				continue;
			}
			if (mp.isNested(mp2)) {
				nested = true;
				logger.info(mp + " is nested under: " + mp2);
				break;
			} else {
				logger.info(mp + " is not nested under: " +
				    mp2);
			}
		}
		return nested;
	} // isNestedMountPoint

} // class

