/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)SolarisDataSrcCommon.java	1.3	08/06/02 SMI"
 */
package com.sun.cluster.cfgchk.datasrc;

import java.io.FileNotFoundException;
import java.io.IOException;

import java.util.ArrayList;
import java.util.StringTokenizer;
import java.util.logging.Logger;

import com.sun.cluster.cfgchk.datasrc.fileObject.TextFile;
import com.sun.cluster.cfgchk.datasrc.fileObject.Vfstab;
import com.sun.cluster.cfgchk.datasrc.dataObject.VfstabEntry;
import com.sun.cluster.cfgchk.i18n.I18n;

import com.sun.cluster.cfgchk.utils.logger.RemoteLogger;

/*
 * Methods common to several implementations in the
 * SolarisDataSrc hierarchy
 */

public class SolarisDataSrcCommon {

	private static Logger logger = RemoteLogger.getRemoteLogger();

	/*
	 * accept /etc/release as TextFile and
	 * return MajorVersion as Integer
	 * will not return null
	 */
	public static Integer getOSVersionMajor(TextFile tf)
		throws DataException {
		logger.info("-- ENTER --");
		Integer majorVersion = null;

		// fetch & parse data
		try {
			// version is second word from first line
			// should be an int
			// no update release info
			String osVersionLine = tf.getLines()[0];
			logger.info("osVersionLine: " + osVersionLine);
			StringTokenizer st = new StringTokenizer(osVersionLine);
			String tok = null;
			if (st.countTokens() < 2) {
				String eMsg = "osVersionLine: " +
				    "insufficient tokens"; // No I18n
				logger.info(eMsg);
				String[] i18nArgs = { eMsg };
				String lMsg = I18n.getLocalized(logger,
				    "internal.error.cmd.unexpected.result",
				    i18nArgs);
				throw new DataException(lMsg);
			}
			tok = st.nextToken(); // skip first word
			tok = st.nextToken(); // second word is version

			// hope for an int
			try {
				int ver = Integer.parseInt(tok);
				logger.info("ver: " + ver);
				majorVersion = new Integer(ver);

			} catch (NumberFormatException e) {
				String eMsg = "osVersionLine: not an int";
					// No I18n
				logger.info(eMsg);
				String[] i18nArgs = { eMsg };
				String lMsg = I18n.getLocalized(logger,
				    "internal.error.cmd.unexpected.result",
				    i18nArgs);
                throw new DataException(lMsg);
            }
		} catch (Exception ex) {
			logger.warning("Exception: " + ex);
			throw new DataException(ex);
		}
		logger.info("majorVersion: " + majorVersion);
		return majorVersion;
	} // getOSVersionMajor


	/*
	 * Given /etc/vfstab as a TextFile, create, populate, return Vfstab
	 */
	public static Vfstab loadVfstab(String nodename, TextFile tf)
		throws DataException, FileNotFoundException, IOException {
		logger.info("Vfstab.load(): " + tf.getFilename());
		StringBuffer infoMsg = new StringBuffer("");

		Vfstab vfstab = new Vfstab(nodename);
		ArrayList lines = new ArrayList();

		String[] rawLines = tf.getLines();
		VfstabEntry ve = null;

		for (int i = 0; i < rawLines.length; i++) {
			logger.fine("rawlines[i]: " + rawLines[i]);
			String line = rawLines[i].trim();

			// ignore comment & blank lines
			if (line.startsWith("#") || line.length() < 1) {
				continue;
			}

			try {
				ve = new VfstabEntry(rawLines[i]);
				lines.add(ve);

			} catch (ParseException e) {
				logger.warning("ParseException on " +
				    nodename + ": >>" + e.getMessage() + "<<");
				// downgrade to info message
				//  ignore this vfstab line
				if (infoMsg.length() > 0) {
					infoMsg.append("\n");
				}
				logger.info("appending msg: " + e.getMessage());
				infoMsg.append(nodename + ": " +
				    e.getMessage());
			}
		} // for lines

		vfstab.setEntries(lines);

		if (infoMsg.length() > 0) {
			vfstab.appendInfoMessage(infoMsg.toString());
				// BasicObject
		}

		logger.info("-- EXIT --");
		return vfstab;
	} // loadVfstab


} // class
