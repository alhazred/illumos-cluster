/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)GenericCache.java	1.2	08/05/29 SMI"
 */
package com.sun.cluster.cfgchk.datasrc;

import java.io.File;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import java.util.logging.Logger;

import com.sun.cluster.cfgchk.datasrc.fileObject.DirectoryListing;
import com.sun.cluster.cfgchk.datasrc.fileObject.TextFile;
import com.sun.cluster.cfgchk.datasrc.fileObject.Vfstab;

import com.sun.cluster.cfgchk.utils.logger.RemoteLogger;


/*
 * GenericCache is used by DataSrc's.
 *
 * Convenience methods for returning stored data by type (no primitives)
 */

public class GenericCache {

	private HashMap map = null;
	private String myName = "uninitialized";
	private static Logger rLogger = RemoteLogger.getRemoteLogger();
	public static String CACHE_STATUS = "cache status";

	public GenericCache(String nm) {
		myName = nm;
		rLogger.info("-- ENTER -- "+ myName);
		init();
	} // ()

	private void init() {
		map = new HashMap();
		String dt = new Date().toString();
		put(CACHE_STATUS, "initialized at "+ dt);
		rLogger.info(myName + " initialized at "+ dt);
	} // init

	public void put(String key, Object value) {
		map.put(key, value);
	}

	public Object get(String key) {
		return map.get(key);
	}

	// not found: return null
	public Boolean getBoolean(String key) throws DataException {
		rLogger.info("-- ENTER, key: '" + key + "'");
		Boolean b = null;
		Object obj = this.get(key);
		if (obj != null) {
			rLogger.info("hit in cache: '" + key + "'");
			try {
				b = (Boolean)obj;
				rLogger.info("Boolean found in cache");
			} catch (ClassCastException ccex) {
				throw new DataException("ClassCastException: " +
				    ccex);
			}
		}
		rLogger.info("key: '" + key + " value: " + b);
		return b;
	} // getBoolean

	// not found: return null
	public Integer getInteger(String key) throws DataException {
		rLogger.info("-- ENTER, key: '" + key + "'");
		Integer integer = null;
		Object obj = this.get(key);
		if (obj != null) {
			rLogger.info("hit in cache: '" + key + "'");
			try {
				integer = (Integer)obj;
				rLogger.info("Integer found in cache");
			} catch (ClassCastException ccex) {
				throw new DataException("ClassCastException: " +
				    ccex);
			}
		}
		rLogger.info("key: '" + key + " value: " + integer);
		return integer;
	} // getInteger

	// not found: return null
	public String getString(String key) throws DataException {
		rLogger.info("-- ENTER, key: '" + key + "'");
		String str = null;
		Object obj = this.get(key);
		if (obj != null) {
			rLogger.info("hit in cache: '" + key + "'");
			try {
				str = (String)obj;
				rLogger.info("String found in cache");
			} catch (ClassCastException ccex) {
				throw new DataException("ClassCastException: " +
				    ccex);
			}
		}
		rLogger.info("key: '" + key + " value: " + str);
		return str;
	} // getString

	// not found: return null
	public String[] getStringArray(String key) throws DataException {
		rLogger.info("-- ENTER, key: '" + key + "'");
		String[] str = null;
		Object obj = this.get(key);
		if (obj != null) {
			rLogger.info("hit in cache: '" + key + "'");
			try {
				str = (String[])obj;
				rLogger.info("String[] found in cache");
			} catch (ClassCastException ccex) {
				throw new DataException("ClassCastException: " +
				    ccex);
			}
		}
		rLogger.info("key: '" + key + " value: " + str);
		return str;
	} // getStringArray

	// not found: return null
	public List getList(String key) throws DataException {
		rLogger.info("-- ENTER, key: '" + key + "'");
		List list = null;
		Object obj = this.get(key);
		if (obj != null) {
			rLogger.info("hit in cache: '" + key + "'");
			try {
				list = (List)obj;
				rLogger.info("List found in cache");
			} catch (ClassCastException ccex) {
				throw new DataException("ClassCastException: " +
				    ccex);
			}
		}
		rLogger.info("key: '" + key + " value: " + list);
		return list;
	} // getList

	// not found: return null
	public TextFile getTextFile(String key) throws DataException {
		rLogger.info("-- ENTER, key: '" + key + "'");
		TextFile tf = null;
		Object obj = this.get(key);
		if (obj != null) {
			rLogger.info("hit in cache: '" + key + "'");
			try {
				tf = (TextFile)obj;
				rLogger.info("TextFile found in cache");
			} catch (ClassCastException ccex) {
				throw new DataException("ClassCastException: " +
				    ccex);
			}
		}
		rLogger.info("key: '" + key + " value: " + tf);
		return tf;
	} // getTextFile

	// not found: return null
	public File getFile(String key) throws DataException {
		rLogger.info("-- ENTER, key: '" + key + "'");
		File f = null;
		Object obj = this.get(key);
		if (obj != null) {
			rLogger.info("hit in cache: '" + key + "'");
			try {
				f = (File)obj;
				rLogger.info("File found in cache");
			} catch (ClassCastException ccex) {
				throw new DataException("ClassCastException: " +
				    ccex);
			}
		}
		rLogger.info("key: '" + key + " value: " + f);
		return f;
	} // getFile

	// not found: return null
	public DirectoryListing getDirectoryListing(String key)
		throws DataException {
		rLogger.info("-- ENTER, key: '" + key + "'");
		DirectoryListing dl = null;

		Object obj = this.get(key);
		if (obj != null) {
			rLogger.info("hit in cache: '" + key + "'");
			try {
				dl = (DirectoryListing)obj;
				rLogger.info("DirectoryListing found in cache");
			} catch (ClassCastException ccex) {
				throw new DataException("ClassCastException: " +
				    ccex);
			}
		}
		if (dl != null) {
			rLogger.info("key: '" + key +
			    " num lines in DirectoryListing: " +
			    dl.getEntries().length);
		} else {
			rLogger.info("key: '" + key + " DirectoryListing: " +
			    dl);
		}
		return dl;
	} // getDirectoryListing

	// not found: return null
	public Vfstab getVfstab(String key) throws DataException {
		rLogger.info("-- ENTER, key: '" + key + "'");
		Vfstab vfstab = null;

		Object obj = this.get(key);
		if (obj != null) {
			rLogger.info("hit in cache: '" + key + "'");
			try {
				vfstab = (Vfstab)obj;
				rLogger.info("Vfstab found in cache");
			} catch (ClassCastException ccex) {
				throw new DataException("ClassCastException: " +
				    ccex);
			}
		}
		if (vfstab != null) {
			rLogger.info("key: '" + key + " num lines in Vfstab: " +
			    vfstab.getEntries().length);
		} else {
			rLogger.info("key: '" + key + " Vfstab: " + vfstab);
		}
		return vfstab;
	} // getVfstab

	public void clear() {
		rLogger.info("-- ENTER --");
		init();
	} // clear

	public String toString() {
		String SEP = " | ";
		Set set = map.keySet();
		Iterator it = set.iterator();

		StringBuffer sb = new StringBuffer(myName + ":");
		while (it.hasNext()) {
			String key = (String)it.next();
			Object value = map.get(key);
			sb.append(SEP + key + ": " + value.toString());

		} // while

		return sb.toString();
	} // toString

} // class
