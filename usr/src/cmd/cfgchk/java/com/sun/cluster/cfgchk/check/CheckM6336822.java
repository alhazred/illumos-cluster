/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)CheckM6336822.java	1.2	08/06/02 SMI"
 */
package com.sun.cluster.cfgchk.check;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import com.sun.cluster.cfgchk.Check;
import com.sun.cluster.cfgchk.BasicCheckMultiNode;
import com.sun.cluster.cfgchk.CheckExecutionException;

import com.sun.cluster.cfgchk.datasrc.DataException;
import com.sun.cluster.cfgchk.datasrc.dataObject.VfstabEntry;
import com.sun.cluster.cfgchk.datasrc.fileObject.Vfstab;

/* BEGIN JSTYLED */
/**
 *
 * 6336822 - 3065 (Critical)
 *
 * Multi node check.
 * Global filesystem /etc/vfstab entries are not consistent across all Sun Cluster nodes.
 *
 * Compares global fs entries for presence and consistency
 *
 * See CheckS6708652 - eRAS 2756 for mount points missing for global fs.
 *
 */
/* END JSTYLED */

/*
 * No I18n of any strings in this class.
 * Exceptions thrown from Checks are caught and handled by Engine
 * typically just by logging. Messages are never displayed to user.
 *
 * Lines with trailing 'TOC' comment are auto-gathered into
 * table of contents listing of all checks.
 */

public class CheckM6336822 extends BasicCheckMultiNode {

	private ArrayList mismatchedEntries = null;
	private ArrayList missingEntries = null;

	public CheckM6336822() {
		checkID = "M6336822"; // TOC
		severity = Check.CRITICAL; // TOC

		// sccs keyword: must be 1.2 when checked out under sccs
		version = "1.2";
		// sccs keyword: must be 08/06/02 when checked out under sccs
		revisionDate = "09/12/08";

		keywords.add("SunCluster3");
		keywords.add("multi");
		keywords.add("vfstab");
		keywords.add("global_filesystem");

		/* BEGIN JSTYLED */
		problemStatement = "Global filesystem /etc/vfstab entries are not consistent across all Sun Cluster nodes."; // TOC
		/* END JSTYLED */

		// check-specific data
		mismatchedEntries = new ArrayList();
		missingEntries = new ArrayList();
	} // ()

	/**
	 * @return a string describing the logic of the applicability test
	 */
	protected String assignApplicabilityLogic() {
		String aLogic = "Always applies to Sun Cluster nodes.";
		return aLogic;
	} // assignApplicabilityLogic


	/**
	 * @return a string describing the logic of the check
	 */
	protected String assignCheckLogic() {
		/* BEGIN JSTYLED */
		String cLogic = "Visit /etc/vfstab on each node. Check that every global filesystem line in each file is present in /etc/vfstab on all other nodes. If global entries are missing or mismatched then this check is violated. This check specifically excludes 'node@N' lines.";
		/* END JSTYLED */
		return cLogic;
	} // assignCheckLogic


	public String getAnalysis() {
		/*
		 * The global filesystem /etc/vfstab entries are not
		 * consistent across all nodes in this cluster. Filesystems:
		 * One or more fields in the vfstab entry for global
		 * filesystem <fs name> on node <nodename> differ from the
		 * corresponding entry on node(s) <nodename> [<nodename>...]
		 *
		 * Global filesystem <fs name> on node <nodename> is missing
		 * from vfstab on node <nodename> [<nodename...].
		 */

		StringBuffer analysis = new StringBuffer("");

		if (mismatchedEntries.size() > 0) {
			/* BEGIN JSTYLED */
			analysis.append(
			    "The following /etc/vfstab entries for global filesystems differ beween nodes:\n");
			/* END JSTYLED */

			Iterator it = mismatchedEntries.iterator();
			String data = null;
			while (it.hasNext()) {
				data = (String)it.next();
				analysis.append("\t\t** " + data + "\n");
			}
		}

		if (missingEntries.size() > 0) {
			/* BEGIN JSTYLED */
			analysis.append(
			    "The following entries are present on one node but missing from another node:\n");
			/* END JSTYLED */

			Iterator it = missingEntries.iterator();
			String data = null;
			while (it.hasNext()) {
				data = (String)it.next();
				analysis.append("\t\t* " + data + "\n");
			}
		}

		return analysis.toString();
	} // getAnalysis

	public String getRecommendations() {
		/* BEGIN JSTYLED */
		String recommend = "Edit /etc/vfstab on appropriate node(s) to make global entries consistent across nodes.";
		/* END JSTYLED */

		return recommend;
	} // getRecommendations

	// always applicable to Sun Cluster 3.x
	// requires at least two inputs
	public boolean isApplicable() throws CheckExecutionException {
		logger.info("isApplicable() -- ENTER -- ");
		boolean applicable = super.isApplicable(); // multi-inputs?
		logger.info("isApplicable() -- EXIT -- " + applicable);
		return applicable;
	} // isApplicable

	/*
	 * The present impl doubles the reports on differing entries: is OK
	 * Missing entries listed only once: is OK
	 */
	public boolean doCheck() throws CheckExecutionException {
		logger.info("-- ENTER --");
		boolean passes = true;

		/*
		 * create array of Vfstab
		 * for each Vfstab
		 * for each global entry (excluding 'node@' lines)
		 * seek corresponding entry in other vfstabs
		 * if mismatch or missing add message to appropriate list
		 * 		mismatchedEntries
		 * 			mntPt-name on node A differs from node B
		 * 		missingEntries
		 * 			mntPt-name on Node A missing from node B
		 *
		 * entries are identified by mount point name
		 */

		try {
			logger.info("getNumInputs(): " + getNumInputs());

			// load up vfstabs array
			Vfstab[] vfstabs = new Vfstab[getNumInputs()];
			for (int i = 0; i < getNumInputs(); i++) {
				String infoMsg = null;
				String nodename = getNodename(i);
				try {
					logger.info("loading Vfstab " + i);
					vfstabs[i] =
					    getDataSrc(i).getVfstab(nodename);
					infoMsg = vfstabs[i].getInfoMessage();
					logger.finest("infoMsg: >>" + infoMsg +
					    "<<");
					if ((infoMsg != null) &&
					    !infoMsg.equals("")) {
						appendInfoMsg(infoMsg);
						logger.finest("infoMessage " +
						    "from load vfstab[i]: " +
						    infoMsg);
					}
				} catch (DataException e) {
					logger.info("DataException(): " +
					    e.getMessage());
					throw new CheckExecutionException(e);
				}
			}

			logger.fine("  end doCheck() loops ------------------");

			VfstabEntry[] vfstabEntriesA = null;
			VfstabEntry[] vfstabEntriesB = null;
			VfstabEntry entryA = null;

			// for each vfstab
			for (int i = 0; i < vfstabs.length; i++) {
				logger.info("vfstab [" + i + "] ");

				// for each global fs entry in vfstab[i]
				//   ('source' entries)
				vfstabEntriesA =
				    vfstabs[i].getGlobalEntries(false);
					// do not include node@ lines

				/* BEGIN JSTYLED */
				// for each global entry
				for (int j = 0; j < vfstabEntriesA.length; j++) {

					// seek same entry in other vfstabs
					// skip self vfstab
					entryA = vfstabEntriesA[j];
					if (entryA == null) { continue; }
						// possible for global entries
						// array to be oversized
					logger.info("entryA: " + entryA.getMountPoint());
					String mountPointA = entryA.getMountPoint();
					logger.info("mountPointA: " + mountPointA);
					for (int k = 0; k < vfstabs.length; k++) {
						logger.info("vfstab k: " + k);
						if (k == i) { continue; }
							// with next vfstab, 'k'

						vfstabEntriesB =
						    vfstabs[k].getGlobalEntries(false);
							// do not include node@ lines
						String mountPointB = null;
						boolean found = false;
						for (int m = 0;
						     m < vfstabEntriesB.length;
						     m++) {
							if (vfstabEntriesB[m] == null) {
								continue;
							}
							logger.info("vfstab m: " + m);

							// seek by mount point name
							mountPointB =
							    vfstabEntriesB[m].getMountPoint();
							logger.info(
								"mountPointA: " +mountPointA +
								"; mountPointB: " + mountPointB);
							if (mountPointB.equals(mountPointA)) {
								found = true;
								logger.info("found!");

								// names match
								// how about complete entries?
								VfstabEntry entryB = vfstabEntriesB[m];
								if (!entryB.equals(entryA)) {
									// add entry to mismatched
									mismatchedEntries.add(
									    mountPointA + ": " +
										getNodename(i) + "; " +
										getNodename(k)
									);
									passes = false;
									logger.info("mismatched");
									break; // out of vfstabEntriesB loop 'm'
								} else {
									logger.info("(matched)");
									break;
								}
							} // found
						} // vfstabEntriesB, 'm'
						logger.info("end vfstabs m");

						if (!found) {
							// add entry to missing
							missingEntries.add(
							    mountPointA + ": " +
								getNodename(i) + "; " +
								getNodename(k)
							);
							passes = false;
							logger.info("missing");
						}
					} // for other vfstabs, 'k'
					logger.info("end vfstabs k");
				} // for vfstabEntriesA, 'j'
				logger.info("end vfstabs j");
				/* END JSTYLED */
			} // for vfstabs, 'i'
			logger.info("end vfstabs i");
			logger.info("   doCheck() loops ^^^^^^^^^^^^^^^^^^^^^");

		} catch (Exception e) {
			logger.warning("Exception: " + e.getMessage());
			throw new CheckExecutionException(e);
		}

		logger.info("passes: " + passes);
		return passes;
	} // doCheck

} // class

