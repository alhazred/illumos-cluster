/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)CheckS6708642.java	1.5	08/09/22 SMI"
 */
package com.sun.cluster.cfgchk.check;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.sun.cluster.cfgchk.BasicCheck;
import com.sun.cluster.cfgchk.Check;
import com.sun.cluster.cfgchk.CheckExecutionException;
import com.sun.cluster.cfgchk.datasrc.DataException;
import com.sun.cluster.cfgchk.datasrc.fileObject.TextFile;
import com.sun.cluster.cfgchk.i18n.I18n;
import com.sun.cluster.cfgchk.utils.logger.LoggerUtils;

/**
 * 6708642 - 1904 - /proc fails to mount periodically during reboots
 */

/*
 * No I18n of any strings in this class.
 * Exceptions thrown from Checks are caught and handled by Engine
 * typically just by logging. Messages are never displayed to user.
 *
 * Lines with trailing 'TOC' comment are auto-gathered into
 * table of contents listing of all checks.
 *
 * Text lines intended for output to report must not contain "<" in order
 * not to mess up the xml.
 */

public class CheckS6708642 extends BasicCheck {

	private ArrayList checkHits = null;


	public CheckS6708642() {
		checkID = "S6708642"; // TOC
		severity = Check.CRITICAL; // TOC

		// sccs keyword: must be 1.5 when checked out under sccs
		version = "1.5";
		// sccs keyword: must be 08/09/22 when checked out under sccs
		revisionDate = "10/24/08";

		keywords.add("SunCluster3");
		keywords.add("single");
		keywords.add("installtime");  // can't depend on clustermode

		/* BEGIN JSTYLED */
		problemStatement = "/proc fails to mount periodically during reboots."; // TOC
		/* END JSTYLED */

		// check-specific data
		checkHits = new ArrayList();

	} // ()

	/**
	 * @return a string describing the logic of the applicability test
	 */
	protected String assignApplicabilityLogic() {
		String aLogic = "Always applies to Sun Cluster nodes.";
		return aLogic;
	} // assignApplicabilityLogic


	/**
	 * @return a string describing the logic of the check
	 */
	protected String assignCheckLogic() {
		/*
		 * Weakness in this logic:
		 * We cannot restrict our check only to /var/adm/messages
		 * because it might have just been rolled. Similarly,
		 * the logs may have rolled a few times since the message
		 * was last emitted.
		 * This means that even if an error condition is observed 
		 * and fixed that this check will continue to be
		 * violated until the logs roll away or unless the admin
		 * decides to edit an old log to change the trigger line.
		 */
		/* BEGIN JSTYLED */
		String cLogic = "Examine /var/adm/messages[.n] for the following string: \"mount: /proc is already mounted, /proc is busy or allowable number of mount points exceeded\". If found, the check is violated.";
		/* END JSTYLED */
		return cLogic;
	} // assignCheckLogic


	public String getAnalysis() {
		StringBuffer analysis = new StringBuffer("");
		/* BEGIN JSTYLED */
		analysis.append("Something is trying to access /proc before it is normally mounted during the boot process.  This can cause /proc not to mount.  If /proc isn't mounted, some Sun Cluster daemons might fail on startup, which can cause the node to panic. The following lines were found:\n\n");
		/* END JSTYLED */

		Iterator it = checkHits.iterator();
		String data = null;
		while (it.hasNext()) {
			data = (String)it.next();
			analysis.append("\t\t" + data + "\n");
		}


		return analysis.toString();
	} // getAnalysis

	public String getRecommendations() {
		/* BEGIN JSTYLED */
		String[] recommendArray = {
			"Verify if any startup functionality has been changed or added in /etc/rcS.d and move that functionality to start after /proc has been mounted in the boot process.",
			"This can be achieved by using the pkgchk -l -p command to verify that the files in /etc/rcS.d are links, and then run the pkgchk -p against each file in /etc/init.d to verify that they have not been modified.",
			"",
			"For example:",
			"1)  First, use pkgchk -l -p the linked file:",
			"",
			"# pkgchk -l -p /etc/rcS.d/S40standardmounts.sh",
			"Pathname: /etc/rcS.d/S40standardmounts.sh",
			"Type: linked file",
			"Source of link: /etc/init.d/standardmounts",
			"Referenced by the following packages:",
			"        SUNWcsr        ",
			"Current status: installed",
			"",
			"2)  Then, use pkgchk -p to check the actual file:",
			"",
			"# pkgchk -p /etc/init.d/standardmounts ",
			"ERROR: /etc/init.d/standardmounts",
			"file size [2405] expected [2412] actual",
			"file cksum [793] expected [1332] actual"
		};
		/* END JSTYLED */
		StringBuffer sb = new StringBuffer("");
		for (int i = 0; i < recommendArray.length; i++) {
			if (i > 0) { sb.append("\t\t"); }
			sb.append(recommendArray[i] + "\n");
		}

		return sb.toString();
	} // getRecommendations

	// always applicable
	public boolean isApplicable() throws CheckExecutionException {
		logger.info("isApplicable() -- ENTER --");
		boolean applicable = true;
		return applicable;
	} // isApplicable

	public boolean doCheck() throws CheckExecutionException {
		logger.info("-- ENTER --");
		boolean passes = true;

		/* BEGIN JSTYLED */
		// do NOT break this string!
		String targetString = "mount: /proc is already mounted, /proc is busy or allowable number of mount points exceeded";
		/* END JSTYLED */

		try {
			/*
			 * for each file messages[.0-3] in /var/adm:
			 * examine for the target string
			 * match means check fails
			 *
			 * collect as many hits as possible
			 */

			String basename = "/var/adm/messages";
			ArrayList varAdmMesagesFiles = new ArrayList();
			varAdmMesagesFiles.add(basename);
			for (int i = 0; i < 4; i++) {
				varAdmMesagesFiles.add(basename + "."+ i);
			}
			Iterator it = varAdmMesagesFiles.iterator();
			String fname = null;

			while (it.hasNext()) {
				fname = (String)it.next();
				logger.info("fname: " + fname);

				// big files can take a while, so
				// give progress message
				String[] i18nArgs = { fname };
				String lMsg = I18n.getLocalized(logger, "msg.searching",
				    i18nArgs);
				engine.sendProgressMsg(lMsg);

				// returned Strings are formatted for
				// display by getAnalysis()
				// messages files may get huge so don't cache
				// them and fetch only search results,
				// not the entire file
				List fileHits =
					getDataSrc().getTextFileStrings(fname,
					    targetString);

				logger.info(fname + " contains " +
				    fileHits.size() + " matches");

				if (fileHits.size() > 0) {
					// setup for display later
					// prepend containing filename
					for (int i = 0;
					     i < fileHits.size();
					     i++) {
						String line =
						    (String)fileHits.get(i);
						checkHits.add(fname + ":\t" +
						    line);
					}
					passes = false;
				}
			} // while varAdmMesagesFiles

			/*
			 * several of these exceptions mean that the target
			 * file doesn't exist, which is OK
			 * isn't an error if old files don't exist
			 */

		} catch (DataException e) {  // I18n XXX
			logger.info("DataException: " + e.getMessage());
			// just fall through
		} catch (FileNotFoundException e) {
			logger.info("FileNotFoundException: " + e.getMessage());
			// just fall through
		} catch (IOException e) {
			logger.info("IOException: " + e.getMessage());
			throw new CheckExecutionException(e);
		} catch (Exception e) {
			logger.info("Exception: " + e.getMessage());
			LoggerUtils.exception("Exception", e, logger);
			throw new CheckExecutionException(e);
		}

		logger.info("-- EXIT passes: " + passes);
		return passes;
	} // doCheck

} // class

