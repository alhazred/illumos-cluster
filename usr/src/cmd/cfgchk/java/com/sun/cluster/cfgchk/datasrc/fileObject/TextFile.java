/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)TextFile.java	1.5	08/09/22 SMI"
 */
package com.sun.cluster.cfgchk.datasrc.fileObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Serializable;

import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.util.regex.PatternSyntaxException;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import com.sun.cluster.cfgchk.utils.logger.LoggerUtils;
import com.sun.cluster.cfgchk.utils.logger.RemoteLogger;

/**
 * Class for loading and working with text files.
 *
 * Stores lines in ArrayList for convenience
 * returns lines as String[].
 *
 * Various convenience methods, mostly for searching within the text.
 */
public class TextFile implements Serializable {

	private static final long serialVersionUID = 1L;
	private ArrayList lines = null;
	private String filename = "not assigned"; // no I18n
	private long fileSize = -1;

        // Logger object
	private static Logger logger = RemoteLogger.getRemoteLogger();

	// anonymous TextFile
	public TextFile(ArrayList lines) {
		this.lines = lines;
	} // ()

	// anonymous TextFile
	public TextFile(String[] saLines) {
		this.lines = new ArrayList();
		for (int i = 0; i < saLines.length; i++) {
			lines.add(saLines[i]);
			logger.finest("line: >" + saLines[i] + "<");
		}
	} // ()

	// named TextFile
    public TextFile(String filename) throws
    	FileNotFoundException, IOException {

	    logger.finest("=== TextFile(): " + filename);
	    this.filename = filename;
	    lines = new ArrayList();
	    load(filename);
	    logger.finest("-- EXIT --");
    } // ()

	public long getFileSize() {
		return fileSize;
	}

	public String getFilename() {
		return filename;
	}

	public int getNumLines() {
		return lines.size();
	}

    public String[] getLines() {
            return toStringArray(lines);
    } // getLines

    public String getLine(int i) {
            return (String)lines.get(i);
    } // getLines


    public String load(String filename) throws FileNotFoundException,
	    IOException {
	    logger.finest("=== TextFile.load(): " + filename);

        // basic reading never generates an extra info message
        String infoMsg = null;

	// File exists and readable errors obtained
	// implicitly by attempting to read
        try {
		BufferedReader br = new BufferedReader(
		    new FileReader(filename)); // ioex, fnfex

            String line = null;
            while ((line = br.readLine()) != null) {
                logger.finest(line);
                lines.add(line);
            }
            br.close();
        } catch (FileNotFoundException e) {

		/*
		 * not always a bad thing
		 * may be looking for a file
		 and it's good if file not present
		*/
            logger.warning("FileNotFoundException(): " + e.getMessage());
            throw e;
        } catch (IOException e) {
            logger.warning("IOException(): " + e.getMessage());
            throw e;
        }
        logger.finest("TextFile.load() -- EXIT--");
        return infoMsg;
    } // load

	/**
	 * @param target the string or substring to seek in this TextFile
	 * @return true if string found in TexfFile or false if not
	 */
	public boolean contains(String target) {
		return (containsString(target) != null);
	} // contains


	/**
	 * @param target the string or substring to seek in this TextFile
	 * @return line containing target String or null if no match
	 */
	public String containsString(String target) {
		logger.finest("-- ENTER--: " + target);

		String line = null;
		try {
			boolean found = false;

			for (int i = 0; i < lines.size(); i++) {
				line = (String)lines.get(i);
				if (line.indexOf(target) != -1) {
					logger.finest(
						"hit: " + line);
					found = true;
					break;
				} else {
					logger.finest(
						"no hit on: " + target);
				}
			}
			if (found == false) {
				line = null;
			}
		} catch (Exception e) {
			logger.warning("Exception: " + e);
			LoggerUtils.exception("Exception", e, logger);
		}
		logger.finest("returning: " + line);
		return line;
	} // containsString


	/**
	 * @param target the string or substring to seek in this TextFile
	 * @return List all lines containing target String or null if no match
	 * formatted as '(line num) line'
	 * May return size 0 nut never null.
	 */
	public List containsStrings(String target) {
		logger.info("-- ENTER: " + target);
		ArrayList list = new ArrayList();
		String line = null;

		try {
			for (int i = 0; i < lines.size(); i++) {
				line = (String)lines.get(i);
				if (line.indexOf(target) != -1) {
					logger.fine("hit:  " + line);
					int lineNum =  i + 1;
					list.add("(" + lineNum + ") " + line);
				}
			}
		} catch (Exception e) {
			logger.warning("Exception: " + e);
			LoggerUtils.exception("Exception", e, logger);
			// but don't throw anything
		}
		logger.fine("returning lines: " + list.size());
		return list;
	} // containsStrings

	/**
	 * Use only for one-time matches.
	 * If repeated uses of a Pattern are needed first obtain a compiled
	 * Pattern by calling TextFile.compilePattern() then sending it to
	 * TextFile.matches(String line, Pattern Pattern)
	 */
	public static Matcher matches(String line, String patternString)
		throws PatternSyntaxException {
		Pattern compiledPattern =
		    TextFile.compilePattern(patternString);
		Matcher regExMatcher = TextFile.matches(line, compiledPattern);
		return regExMatcher;
	}
	/**
	 * returns Matcher which can be queried via group etc methods
	 * or null if no match
	 */
	public static Matcher matches(String line, Pattern pattern) {
		Matcher regExMatcher = null;
		if (line != null) {
			regExMatcher = pattern.matcher(line);
			if (!regExMatcher.matches()) {
				regExMatcher = null;
			}
		}
		return regExMatcher;
	} // matches

	/**
	 * convenience routine
	 */
	public static Pattern compilePattern(String patternString)
		throws PatternSyntaxException {
		Pattern compiledPattern = null;
		try {
			compiledPattern = Pattern.compile(patternString);
		} catch (PatternSyntaxException psEx) {
			throw(psEx);
		}
		return compiledPattern;
	} // compilePattern

    /*
     * convert ArrayList to String typed array
     */
    private String[] toStringArray(ArrayList al) {
        if (al == null) {
            return new String[0];
        } else {
            String[] sa = new String[al.size()];
            for (int i = 0; i < al.size(); i++) {
                sa[i] = (String)al.get(i);
            }
            return sa;
        }
    } // toStringArray

	/*
	 * first test number of lines
	 * if same then roll lines looking for diff
	 */
	public boolean differsFrom(TextFile other) {
		logger.fine("-- ENTER --");
		boolean differs = false;
		if ((getFileSize() != other.getFileSize()) ||
		    (getNumLines() != other.getNumLines())) {
			differs = true;
		} else {
			for (int i = 0; i < getNumLines(); i++) {
				if (! getLine(i).equals(
					     (String)other.getLine(i))) {
					differs = true;
					break;
				}
			}
		}
		logger.fine(getFilename() + " differs from " +
		    other.getFilename() + ": " + differs);
		return differs;
	} // differsFrom


	public String toString() {
		String SEP = "\n";
		StringBuffer sb = new StringBuffer(filename + ": ");
		if (lines != null) {
			for (int i = 0; i < lines.size(); i++) {
				sb.append(SEP + (String)lines.get(i));
			}
		} else {
			sb.append("no lines"); // no I18b
		}
		return sb.toString();
	} // toString

} // class
