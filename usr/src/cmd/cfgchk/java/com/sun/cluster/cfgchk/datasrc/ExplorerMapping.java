/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)ExplorerMapping.java	1.2	08/05/29 SMI"
 */
package com.sun.cluster.cfgchk.datasrc;

import java.util.HashMap;

/*
 * Mapping table transforms "system" filenames and commands
 * into filename stored in an explorer arhive.
 */

public class ExplorerMapping {

	protected static HashMap map = new HashMap();
        protected static HashMap varSuffixMap = new HashMap();

	/* BEGIN JSTYLED */
	static {
		// Files
		map.put("/file/not/found",      "file/not/found"); // for test case
		map.put("/etc/vfstab",          "etc/vfstab");
		map.put("/etc/name_to_major",   "etc/name_to_major");
		map.put("/etc/nsswitch.conf",   "etc/nsswitch.conf");
		map.put("/etc/release",         "etc/release");
		map.put("/etc/system",          "etc/system");
		map.put("/etc/cluster/ccr/infrastructure",  "cluster/etc/cluster/ccr/infrastructure");
		map.put("/etc/cluster/nodeid",  "cluster/etc/cluster/nodeid");
		map.put("/etc/cluster/release", "cluster/etc/cluster/release");
		map.put("/kernel",               "");
		map.put("/var/adm/messages",     "messages/messages");
		map.put("/var/adm/messages.0",   "messages/messages.0");
		map.put("/var/adm/messages.1",   "messages/messages.1");
		map.put("/var/adm/messages.2",   "messages/messages.2");
		map.put("/var/adm/messages.3",   "messages/messages.3");


		// Directory listings
		map.put("/dev",			"disks/dev-lL.out");
		map.put("/kernel",		"sysconfig/kernel_ls-l.out");
		// actual directory, not file
		map.put("/etc/cluster/ccr",	"DIR:cluster/etc/cluster/ccr/");

		// Commands
		//	/usr/sbin
		map.put("/usr/sbin/ifconfig -a", "sysconfig/ifconfig-a.out");
		map.put("/usr/sbin/prtconf -v",  "sysconfig/prtconf-v.out");
		map.put("/usr/cluster/bin/scrgadm -pv",  "cluster/config/scrgadm-pv.out");

		//	/usr/bin
		map.put("/usr/bin/pkginfo -l",  "patch+pkg/pkginfo-l.out");
		map.put("/usr/bin/rpcinfo",     "netinfo/rpcinfo.out");
		map.put("/usr/bin/rpcinfo.err", "netinfo/rpcinfo.err");
		map.put("/usr/bin/ps -ef",      "sysconfig/ps-ef.out");
		map.put("/usr/bin/ps -acefl",   "sysconfig/ps-acefl.out");
		map.put("/usr/bin/showrev -p",  "patch+pkg/showrev-p.out");
		map.put("/usr/bin/uname -a",    "sysconfig/uname-a.out");
		map.put("/usr/bin/uname -X",    "sysconfig/uname-X.out");

		map.put("/usr/cluster/bin/scconf -pv", "cluster/config/scconf-pv.out");
		map.put("/usr/cluster/bin/scinstall -pv", "cluster/config/scinstall-pv.out");

	} // static initializer
	/* END JSTYLED */


	/**
	 * Given a filename or system command
	 * return the name of same data from within
	 * an explorer archive	 *
	 * @param name the name of a data file or command
	 * @return name of file from an explorer archive
	 */
	public static String get(String name) {
		return (String)map.get(name);
	} // get

} // class
