/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)CheckSTest006.java	1.2	08/05/29 SMI"
 */
package com.sun.cluster.cfgchk.check.testchecks;

import com.sun.cluster.cfgchk.Check;
import com.sun.cluster.cfgchk.BasicCheck;
import com.sun.cluster.cfgchk.CheckExecutionException;

/**
 * Test check: Always violated. Severity INFORMATION.
 */

// No I18n

public class CheckSTest006 extends BasicCheck {


    public CheckSTest006() {
        checkID = "STest006";
        severity = Check.INFORMATION;

	// sccs keyword: must be 1.2 when checked out under sccs
	version = "1.2";
	// sccs keyword: must be 08/05/29 when checked out under sccs
	revisionDate = "09/12/08";

        keywords.add("SunCluster3.x");
        keywords.add("keywordC:5-6");
        keywords.add("test");

	/* BEGIN JSTYLED */
        problemStatement = "Test check: Always violated. Severity INFORMATION. keywordC:5-6";
	/* END JSTYLED */

	// special test case directly set super.infoMsg StrigBuffer
	infoMsg = new StringBuffer(checkID + " informational message");
    } // ()

    /**
     * @return a string describing the logic of the applicability test
     */
    protected String assignApplicabilityLogic() {
        String aLogic = "Always applies to Sun Cluster 3.x node.";
        return aLogic;
    } // assignApplicabilityLogic


    /**
     * @return a string describing the logic of the check
     */
    protected String assignCheckLogic() {
        String cLogic = checkID;
        return cLogic;
    } // assignCheckLogic


	public String getAnalysis() {
		return checkID;
	} // getAnalysis

	public String getRecommendations() {
		return checkID;
	} // getRecommendations


    // always
    public boolean isApplicable() throws CheckExecutionException {
        logger.info("isApplicable() -- ENTER --");
        boolean applicable = true;

        return applicable;
    } // isApplicable

    public boolean doCheck() throws CheckExecutionException {

        logger.info("-- ENTER --");
        boolean passes = false;

        logger.info("passes: " + passes);
        logger.info("-- EXIT --");
        return passes;
    } // doCheck

} // class
