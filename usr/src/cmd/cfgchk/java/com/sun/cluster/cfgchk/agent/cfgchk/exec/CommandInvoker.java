/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)CommandInvoker.java	1.2	08/05/29 SMI"
 */
package com.sun.cluster.cfgchk.agent.cfgchk.exec;

// JDK
import java.io.IOException;
import java.lang.reflect.UndeclaredThrowableException;
import java.util.logging.Logger;

import javax.management.MBeanServerConnection;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;

import com.sun.cacao.ObjectNameFactory;
import com.sun.cacao.agent.MBeanServerInvocationHandler;
import com.sun.cacao.invocation.InvocationException;
import com.sun.cacao.invocation.InvocationStatus;
import com.sun.cluster.cfgchk.Globals;
import com.sun.cluster.cfgchk.agent.cfgchk.results.ExitCode;
import com.sun.cluster.cfgchk.datasrc.DataException;
import com.sun.cluster.cfgchk.utils.JMXUtils;
import com.sun.cluster.cfgchk.utils.StringUtils;
import com.sun.cluster.cfgchk.utils.logger.LoggerUtils;
import com.sun.cluster.cfgchk.utils.logger.RemoteLogger;


/*
 * CommandInvoker - execute a specified command under the OS
 * on a specified node in a specified locale
 *
 * Encapsulates all the complexity of mbean access
 *
 * Uses ExecUtil to actually perfom command, either via
 * mbean on target node or directly when non using cacao.
 */

public class CommandInvoker {

	private static Logger logger = RemoteLogger.getRemoteLogger();


	// ===============================================================

	/*
	 * timeout values given in seconds
	 * nodelist == null is interpreted as local node
	 * boolean arg in signatures indicates if cacao is to be used
	 *	default: true
	 */

	//
	//			Convenience signature stack
	//

	/*
	 * accepts one command in array to be executed in C locale
	 */
	public static InvocationStatus execute(String[] cmdArray,
	    String nodename)
		throws InvocationException, DataException {
		logger.info("signature 1");

		return execute(true, cmdArray, "C", nodename);
	} // execute

	public static InvocationStatus execute(boolean usingCacao,
	    String[] cmdArray, String nodename)
		throws InvocationException, DataException {
		logger.info("signature 2");

		return execute(usingCacao, cmdArray, "C", nodename);
	} // execute


	/*
	 * accepts two commands in arrays to be executed in C locale
	 */
	public static InvocationStatus execute(String[] cmdArray1,
	    String[] cmdArray2,
	    String nodename) throws InvocationException, DataException {

		String cmd1 = StringUtils.stringArrayToString(cmdArray1);
		String cmd2 = StringUtils.stringArrayToString(cmdArray2);
		logger.info("signature 3");

		return execute(true, cmd1, cmd2, "C", nodename);
	} // execute

	public static InvocationStatus execute(boolean usingCacao,
	    String[] cmdArray1, String[] cmdArray2,
	    String nodename) throws InvocationException, DataException {

		String cmd1 = StringUtils.stringArrayToString(cmdArray1);
		String cmd2 = StringUtils.stringArrayToString(cmdArray2);
		logger.info("signature 4");

		return execute(usingCacao, cmd1, cmd2, "C", nodename);
	} // execute

	/*
	 * accepts one command in array to be executed in specified locale
	 */
	public static InvocationStatus execute(String[] cmdArray,
	    String localeName,
	    String nodename) throws InvocationException, DataException {

		String cmd = StringUtils.stringArrayToString(cmdArray);
		logger.info("signature 5");

		return execute(true, (String)null, cmd, localeName, nodename);
	} // execute

	public static InvocationStatus execute(boolean usingCacao,
	    String[] cmdArray, String localeName, String nodename)
		throws InvocationException, DataException {

		String cmd = StringUtils.stringArrayToString(cmdArray);
		logger.info("signature 6");

		return execute(usingCacao, (String)null, cmd, localeName,
		    nodename);
	} // execute

	/*
	 * accepts two commands in arrays to be executed in specified locale
	 */
	public static InvocationStatus execute(String[] cmdArray1,
	    String[] cmdArray2,
	    String localeName,
	    String nodename) throws InvocationException, DataException {

		String cmd1 = StringUtils.stringArrayToString(cmdArray1);
		String cmd2 = StringUtils.stringArrayToString(cmdArray2);
		logger.info("signature 7");

		return execute(true, cmd1, cmd2, localeName, nodename);
	} // execute

	public static InvocationStatus execute(boolean usingCacao,
	    String[] cmdArray1, String[] cmdArray2, String localeName,
	    String nodename)
		throws InvocationException, DataException {

		String cmd1 = StringUtils.stringArrayToString(cmdArray1);
		String cmd2 = StringUtils.stringArrayToString(cmdArray2);
		logger.info("signature 8");

		return execute(usingCacao, cmd1, cmd2, localeName, nodename);
	} // execute


	/*
	 * accepts one command in String to be executed in specified locale
	 */
	public static InvocationStatus execute(String cmd,
	    String localeName,
	    String nodename) throws InvocationException, DataException {
		logger.info("signature 9");

		return execute(true, (String)null, cmd, localeName, nodename);
	} // execute

	public static InvocationStatus execute(boolean usingCacao, String cmd,
	    String localeName, String nodename)
		throws InvocationException, DataException {
		logger.info("signature 10");

		return execute(usingCacao, (String)null, cmd, localeName,
		    nodename);
	} // execute


	public static InvocationStatus execute(String cmd1, String cmd2,
	    String localeName, String nodename)
		throws InvocationException, DataException {
		logger.info("signature 11");

		return execute(true, cmd1, cmd2, localeName, nodename);
	} // execute


	/*
	 *			Implementation
	 *
	 * Accepts two separate commands (including arguments) as Strings
	 * First command is executed in default locale
	 * Second command is executed in the specified locale
	 * Example:
	 *	cmd1 = "ulimit -n 65536"
	 *	cmd2 = "/foo/bar/mycmd --flag1 arg1 --flag2 arg2 arg3"
	 * First command may be a Unix 'multiple' command
	 *	cmd1 = "/usr/bin/who -T; /usr/bin/uptime; date"
	 * Commands are executed on specified node via ExecUtilMBean unless
	 * not usingCacao in which case commands are executed  via ExecUtil
	 * on localhost
	 */

	// will not return null
	public static InvocationStatus execute(boolean usingCacao,
	    String cmd1, String cmd2, String localeName, String nodename)
		throws InvocationException, DataException {
		logger.info("-- ENTER --  signature ultimate");

		logger.info("cmd1; cmd2: " + cmd1 + "; "+ cmd2 +"\n" +
		    "nodename: "+ nodename + "; usingCacao: " + usingCacao +
		    "; localeName: " + localeName);

		logger.finest("usingCacao: " + usingCacao);

		InvocationStatus status = null;
		try {

			String[] command = new String[3];
			command[0] = "/bin/sh";	// run under basic shell
			command[1] = "-c";	// the following string
						// contains commands

			/*
			 * cmd1 may have been constructed in a prior signature
			 * and may be null
			 * cmd2 was provided by user; if null
			 * will produce InvocationException
			 */
			if (cmd1 == null || cmd1.trim().length() == 0) {
				cmd1 = "";
			} else {
				cmd1 = cmd1.trim().concat("; ");
			}

			StringBuffer commandString = new StringBuffer(cmd1);
			commandString.append("/usr/bin/env LC_ALL=");
			commandString.append(localeName);
			commandString.append(" ");
			commandString.append(cmd2);

			String cmd = commandString.toString();
			command[2] = cmd; // string to execute as commands

			logger.info("CommandInvoker executing " + cmd);

			try {
				if (usingCacao) {
					logger.info("calling cacao invoker");
					status = invokeCacao(command, nodename);
				} else {
					logger.info("calling invokeDirect()");
					status = invokeDirect(command);
				}

			} catch (InvocationException iex) {
				// rethrow
				logger.severe("InvocationException: " + iex);
				throw(iex);
			}
		} catch (Exception e) {
			logger.severe("Exception: " + e);
			LoggerUtils.exception("Exception", e, logger);
			throw new DataException(e);
		}

		if (status == null) {
			logger.severe("InvocationStatus is null");
			throw new InvocationException(
				"InvocationStatus is null");
		}

		logger.info("-- EXIT --");
		return status;
	} // execute


	/*
	 * Perform actual command launch under cacao:
	 *	fetch utility mbean proxy for specified node
	 *	pass command to ExecUtil mbean
	 * Error conditions: throw an InvocationException
	 * containing an InvocationStatus
	 * No I18n of InvocationException
	 */
	private static InvocationStatus invokeCacao(
	    String[] command, String nodename) throws InvocationException {

		logger.info("-- ENTER -- " + nodename);
		InvocationStatus status = null;

		// fetch utility mbean proxy for specified node:
		// get jmx connection, mbean server, mbean

		JMXConnector jmxc = JMXUtils.getJMXConnection(logger, nodename);
		if (jmxc == null) {
			logger.severe("No JMX connection for " + nodename);
			status = new InvocationStatus(command, null,
			    ExitCode.ERR_CACAO, null, null, "{0}=" + nodename);
			status.setArgv(command);
				// workaround cacao init bug: 6410164
			throw new InvocationException(
				"No JMX connection for " + nodename, status);
		}

		MBeanServerConnection mbsc = null;
		try {
			mbsc = jmxc.getMBeanServerConnection();
		} catch (IOException ioex) {
			logger.severe("IOException getting mbeanserver for " +
			    nodename);
			status = new InvocationStatus(command, null,
			    ExitCode.ERR_CACAO, null, null, "{0}=" + nodename);
			status.setArgv(command);
				// workaround cacao init bug: 6410164
			JMXUtils.closeJMXConnection(logger, jmxc);
			throw new InvocationException("No mbeanserver for " +
			    nodename, status);
		}
		if (mbsc == null) {
			logger.severe("No mbeanserver for " + nodename);
			status = new InvocationStatus(command, null,
			    ExitCode.ERR_CACAO, null, null, "{0}=" + nodename);
			status.setArgv(command);
				// workaround cacao init bug: 6410164
			JMXUtils.closeJMXConnection(logger, jmxc);
			throw new InvocationException("No mbeanserver for " +
			    nodename, status);
		}

		ExecUtilMBean execUtilMBean =
			getExecUtilMBean(mbsc);

		logger.finest("got execUtilMBean");
		if (execUtilMBean == null) {
			logger.severe("Unable to get ExecUtilMBean proxy for " +
			    nodename);
			status = new InvocationStatus(command, null,
			    ExitCode.ERR_CACAO, null, null, "{0}=" + nodename);
			status.setArgv(command);
				// workaround cacao init bug: 6410164
			JMXUtils.closeJMXConnection(logger, jmxc);
			throw new InvocationException("No mbeanserver for " +
			    nodename, status);
		}

		//
		// pass command to utility mbean for actual execution
		//
		try {
			logger.info("CommandInvoker calling execUtilMBean." +
			    "cacaoExecuteCommand(command)");
			status = execUtilMBean.cacaoExecuteCommand(command);

			JMXUtils.closeJMXConnection(logger, jmxc);

		} catch (InvocationException iex) {

			// log then rethrow the exception
			logger.severe("InvocationException: " + iex.toString());
			JMXUtils.closeJMXConnection(logger, jmxc);
			throw(iex);
		}

		logger.info("-- EXIT --");
		return status;
	} // invokeCacao


	/*
	 * Perform actual command launch without cacao:
	 *	command is run on localhost
	 * Error conditions throw an InvocationException
	 * containing an InvocationStatus
	 */
	private static InvocationStatus invokeDirect(String[] command)
		throws InvocationException {
		logger.info("-- ENTER --");
		InvocationStatus status = null;

		// fetch utility class to perform command execution locally
		ExecUtil execUtil = new ExecUtil();
		logger.info("got ExecUtil ");

		try {
			logger.finest("calling execUtil.directExecuteCommand(" +
			    "command)");
			status = execUtil.directExecuteCommand(logger, command);
			logger.info(
				"back from execUtil.directExecuteCommand() ");

		} catch (InvocationException iex) {
			// log then rethrow the exception
			logger.severe("InvocationException: " + iex.toString());
			throw(iex);
		}
		logger.info("-- EXIT --");
		return status;
	} // invokeDirect



	// returns null on any error
	private static ExecUtilMBean getExecUtilMBean(
		MBeanServerConnection mbsc) {
		ExecUtilMBean execUtilMBean = null;
		logger.info("-- ENTER --");

		try {
			ObjectNameFactory onf;
			onf = new ObjectNameFactory(Globals.DOMAIN_CFGCHK);

			/* BEGIN JSTYLED */
			ObjectName objName = onf.getObjectName(
				Class.forName(
					"com.sun.cluster.cfgchk.agent.cfgchk.exec.ExecUtilMBean"), null);
			/* END JSTYLED */
			logger.finest("Domain object handle OK");

			if (!mbsc.isRegistered(objName)) {
				logger.severe("ExecUtilMBean not registered " +
				    "with the mbean server");
				return null;
			}

			/* BEGIN JSTYLED */
			execUtilMBean = (ExecUtilMBean)
				MBeanServerInvocationHandler.newProxyInstance(mbsc,
				    objName,
				    com.sun.cluster.cfgchk.agent.cfgchk.exec.ExecUtilMBean.class,
				    false);
			/* END JSTYLED */

			// This call is added to check for
			// RuntimeMBeanException : see 6247850
			logger.fine("Got ExecUtilMBean proxy OK 1");
			String str = execUtilMBean.getNodeName();
			logger.fine("Got ExecUtilMBean proxy OK 2; " +
			    "nodename test returns: " + str);

		} catch (UndeclaredThrowableException utex) {
			// should not occur
			logger.severe("\nUndeclaredThrowableException: " +
			    "Unable to get mbean proxy for ExecUtilMBean: " +
			    utex + "\n");
			Throwable th = utex.getUndeclaredThrowable();
			logger.severe("\nThrowable: " + th + "\n");
			execUtilMBean = null;
		} catch (Exception ex) {
			logger.severe("\nException: Unable to get mbean " +
			    "proxy for ExecUtilMBean: " + ex + "\n");
			execUtilMBean = null;
		}

		logger.info("-- EXIT --");
		return execUtilMBean;
	} // getExecUtilMBean

} // class
