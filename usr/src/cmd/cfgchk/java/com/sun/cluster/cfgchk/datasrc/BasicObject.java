/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)BasicObject.java	1.2	08/05/29 SMI"
 */
package com.sun.cluster.cfgchk.datasrc;

import java.util.logging.Logger;

/**
 * Superclass of various data objects. Implements various
 * common methods including interface InfoMessageObject.
 */
public class BasicObject {

    protected Logger logger = null;
    private StringBuffer infoMsg = null;

    public void setLogger(Logger logger) {
        this.logger = logger;
    }


    // =====   start implementation for interface InfoMessageObject  =====

    /*
     * subclass should declare 'implements'
     * and use these implementations
     */

	/* BEGIN JSTYLED */
	/* (non-Javadoc)
	 * @see com.sun.cluster.cfgchk.datasrc.InfoMessageObject#appendInfoMessage(java.lang.String)
     */
	/* END JSTYLED */
    public void appendInfoMessage(String msg) {
        if (infoMsg == null) {
            infoMsg = new StringBuffer();
        } else {
            infoMsg.append("\n");
        }
        infoMsg.append(msg);
    } // appendInfoMessage


	/* BEGIN JSTYLED */
	/* (non-Javadoc)
	 * @see com.sun.cluster.cfgchk.datasrc.InfoMessageObject#getInfoMessage()
	 */
	/* END JSTYLED */
	public String getInfoMessage() {
		if (infoMsg != null) {
			return infoMsg.toString();
		} else {
			return "";
		}
	} // getInfoMessage

    // =====   end implementation for interface InfoMessageObject  =====


} // class
