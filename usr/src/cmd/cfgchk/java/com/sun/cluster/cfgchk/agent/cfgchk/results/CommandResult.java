/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)CommandResult.java	1.2	08/05/29 SMI"
 */
package com.sun.cluster.cfgchk.agent.cfgchk.results;

import java.util.ArrayList;
import java.util.List;

/**
 * Command result interface definition. Implemented by the
 * <code>BaseCommandResult</code> base implementation class and all derived
 * classes.
 */
public interface CommandResult {

	/**
	 * Exit code getter
	 *
	 * @return the exit code field of the current object
	 */
	public int getExitCode();

	/**
	 * Message getter
	 *
	 * @return the message field of the current object
	 */
	public String getMessage();
	public String getMessage(int i);

	/**
	 * Message getter
	 *
	 * @return the list of messages of the current object
	 */
	public List getMessages();


    	public void setMessages(ArrayList msgs);
	public void setMessage(String msg);

	public String toString();

} // CommandResult
