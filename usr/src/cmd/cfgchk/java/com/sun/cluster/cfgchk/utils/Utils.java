/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)Utils.java	1.3	08/06/02 SMI"
 */
package com.sun.cluster.cfgchk.utils;

import java.io.File;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Logger;

import com.sun.cacao.invocation.InvocationException;
import com.sun.cacao.invocation.InvocationStatus;
import com.sun.cluster.cfgchk.agent.cfgchk.exec.CommandInvoker;
import com.sun.cluster.cfgchk.datasrc.DataException;
import com.sun.cluster.cfgchk.i18n.I18n;
import com.sun.cluster.cfgchk.utils.logger.RemoteLogger;


/*
 * Utilities of a general nature:
 *
 *	convenience methods for running commands
 *		various return types:
 *			String[] of output
 *			cacao's InvocationStatus
 *				 (includes ret cide, stdout, stderr)
 *	getLocalHostname()
 *	createTimestamp()
 *	isValidExplorerArchive()
 */
public class Utils {

	private static Logger rLogger = RemoteLogger.getRemoteLogger();

	/*
	 * run command on localhost...
	 */

	public static String[] runCommandStrArray(String[] cmd)
		throws DataException {
		return runCommandStrArray(cmd, /* usingCacao */ false);
	}
	public static String[] runCommandStrArray(String[] cmd,
	    boolean usingCacao) throws DataException {
			String[] result =
			    Utils.runCommandStrArray("localhost", cmd,
				usingCacao);
			return result;
	} // runCommandStrArray


	/*
	 * run command on specified node and return std out as array of String
	 * if command returns non-0 throw DataException with message
	 * containing exit code and stderr as String
	 * if stdout from InvocationStatus is null return empty array
	 * do not return null
	 */
	public static String[] runCommandStrArray(String nodename,
	    String[] cmd, boolean usingCacao) throws DataException {
		rLogger.info("nodename: " + nodename);
		rLogger.info("cmd[0]: " + cmd[0]);

		InvocationStatus exitStatus = Utils.runCommand(nodename, cmd,
		    usingCacao);

		String[] stdoutArray = null;
		String stdout = null;
		String stderr = null;
		int exitCode = exitStatus.getExitValue().intValue();

		if (exitCode != 0) {
			stderr = exitStatus.getStderr();
			throw new DataException("" + exitCode + "; " +
			    StringUtils.stringArrayToString(cmd) + ": " +
			    stderr);
			// see LiveSolarisDataSrc.getArchitecture()
			// for example of handling this ex
		} else {
			stdout = exitStatus.getStdout();
			stdoutArray =
			    StringUtils.stringArrayFromEncodedString(stdout,
				"\n");
			if (stdoutArray == null) {
				stdoutArray = new String[0];
			}
		}

		rLogger.finest("stdout: " + stdout);
		rLogger.finest("stderr: " + stderr);

		return stdoutArray;
	} // runCommandStrArray

	/*
	 * run command on localhost...
	 */
	public static InvocationStatus runCommand(String[] cmd)
		throws DataException {
		InvocationStatus status = Utils.runCommand("localhost",
		    cmd, /* usingCacao */ false);
		return status;
	} // runCommand

	public static InvocationStatus runCommand(String[] cmd,
	    boolean usingCacao)
		throws DataException {
		InvocationStatus status = Utils.runCommand("localhost", cmd,
		    usingCacao);
		return status;
	} // runCommand

	public static InvocationStatus runCommand(String nodename, String[] cmd)
		throws DataException {
		InvocationStatus status = Utils.runCommand(nodename,
		    cmd, /* usingCacao */ false);
		return status;
	} // runCommand

	/*
	 * run command on specified node and return InvocationStatus
	 * if command returns non-0 throw DataException with message
	 * containing exit code and stderr as String
	 * In case of InvocationException return DataException with
	 * I18n message
	 */
	public static InvocationStatus runCommand(String nodename,
	    String[] cmd, boolean usingCacao)
		throws DataException {
		rLogger.info("Utils.runCommand() -- ENTER -- nodename: " +
		    nodename);
		rLogger.info("Utils.runCommand() -- ENTER -- cmd[0]: " +
		    cmd[0]);
		rLogger.info("Utils.runCommand() -- ENTER -- usingCacao: " +
		    usingCacao);

		InvocationStatus exitStatus = null;

		rLogger.fine("walk cmd elts:");
		for (int i = 0; i < cmd.length; i++) {
			rLogger.fine("cmd["+i+"]: " + cmd[i]);
		}

		try {
			exitStatus = CommandInvoker.execute(usingCacao, cmd,
			    nodename);

			rLogger.info("exitStatus.getExitValue(): " +
			    exitStatus.getExitValue());
			// rLogger.finest("exitStatus: " + exitStatus);

		} catch (InvocationException ie) {

			// I18n: assemble a generic user visible
			// 'internal error' message
			rLogger.warning("InvocationException: " +
			    ie.getMessage());
			String[] i18nArgs = { ie.getMessage() };
			String lMsg = I18n.getLocalized(rLogger,
			    "internal.error.cmd.exec.failed", i18nArgs);
			throw new DataException(lMsg);
		}

		rLogger.info("Utils.runCommand() -- EXIT: " + cmd[0]);
		return exitStatus;
	} // runCommand

	/*
	 * does not require cluster mode
	 * this is the node currently executing code
	 */
	public static String getLocalHostname() throws DataException {
		return getLocalHostname(rLogger);
	}
	public static String getLocalHostname(Logger logger)
		throws DataException {
		String localHostname = null;
		logger.info("-- ENTER --");

		try {
			InetAddress ia = InetAddress.getLocalHost();
			logger.info("InetAddress: " + ia);

			localHostname = ia.getCanonicalHostName();
			logger.info("localHostname: >>" + localHostname + "<<");

		} catch (UnknownHostException ex) {
			logger.warning("UnknownHostException: " + ex);
			throw new DataException(ex);
		}
		logger.info("-- EXIT: " + localHostname);
		return localHostname;
	} // getLocalHostname

	public static String createTimestamp() {
		String format = "yyyy-MM-dd.HH:mm:ss";
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		String timestamp = sdf.format(new Date());
		return timestamp;
	} // createTimestamp


	public static boolean isValidExplorerArchive(String basename,
	    Logger logger) {
		logger.info("-- ENTER: " + basename);
		boolean valid = true;

		// test for dirname/patch+pkg; minor fragility but s/b OK
		File f = new File(basename + "/patch+pkg");
		if (!f.exists() || !f.isDirectory()) {
			valid = false;
		}
		logger.info("-- EXIT valid: " + valid);
		return valid;
	} // isValidExplorerArchive

} // class
