/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)JarClassLoader.java	1.2	08/05/29 SMI"
 */
package com.sun.cluster.cfgchk.utils;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.IOException;
import java.security.ProtectionDomain;

import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.logging.Logger;

import com.sun.cluster.cfgchk.utils.ChecksCache;
import com.sun.cluster.cfgchk.utils.logger.RemoteLogger;


/*
 * This classloader allows Engine to find checks in user
 * (or test) provided jars. Under cacao all classpath
 * items must be specified in the registration xml file.
 *
 * Cfgchk instantiates this classloader and passes it to
 * the checkLoading routines in CheckUtils.
 *
 * This class is not MT safe.
 */

public class JarClassLoader extends ClassLoader {

    private ChecksCache checksCache = null;
    private ProtectionDomain protectionDomain = null;
    protected static Logger rLogger = RemoteLogger.getRemoteLogger();

    public JarClassLoader(ClassLoader parent,
	ProtectionDomain protectionDomain) {
	super(parent);
	this.protectionDomain = protectionDomain;
	rLogger.finest("instantiated");
    } // ()

    public void setChecksCache(ChecksCache checksCache) {
	rLogger.finest("-- ENTER --");
	this.checksCache = checksCache;
    } // setChecksCache

    // override
    protected Class findClass(String classname) throws ClassNotFoundException {
	rLogger.finest("-- ENTER: " + classname);
	try {
	    /*
	     * an earlier version of this classloader
	     * received an InputStream from the caller instead of the
	     * ChecksCache but findClass() is called again by super
	     * in the case of inner classes
	     * and the input stream is that of the main class.
	     * Therefore we now send in the ChecksCache to locate the
	     * InputStream on each call to this method.
	     */
	    InputStream is = getInputStream(classname);
	    if (is == null) {
		throw new ClassNotFoundException("Class " + classname);
	    }

	    ByteArrayOutputStream out = new ByteArrayOutputStream();
	    BufferedInputStream in = new BufferedInputStream(is);
	    byte [] buffer = new byte [8096];
	    int size;

	    while ((size = in.read(buffer)) != -1) { // read from ZipEntry
		out.write(buffer, 0, size); // stuff bytes into output stream
		rLogger.finest("in loop; bytes read: " + size);
	    }
	    in.close();
	    rLogger.finest("done reading; calling defineClass() on: " +
		classname);
	    Class cl = defineClass(classname, out.toByteArray(), 0, out.size(),
		protectionDomain);
	    rLogger.finest("back from defineClass() on: " + classname);
	    rLogger.finest("-- EXIT --");
	    return cl;
	} catch (IOException ex) {
	    rLogger.warning("IOException: " + ex);
	    throw new ClassNotFoundException("Class " + classname, ex);
	}
    } // findclass

    protected Class loadClass(String name, boolean resolve)
	throws ClassNotFoundException {
	rLogger.finest("-- ENTER: " + name + "; resolve: " + resolve);

	// First, check if the class has already been loaded
	Class loadedClass = findLoadedClass(name);
	rLogger.finest("class already loaded? (non-null means yes): " +
	    loadedClass);
	if (loadedClass == null) {
	    try {
		// let super try first
		rLogger.finest("calling super.loadClass()...");
		loadedClass = super.loadClass(name, resolve);
		rLogger.finest("loaded by super.loadClass()?: " + loadedClass);

		if (loadedClass == null) {
		    // Try JarClassLoader
		    rLogger.finest("calling local findClass()...");
		    loadedClass = findClass(name);
		    rLogger.finest(
			"loaded by local findClass()?: " + loadedClass);
		}
	    } catch (ClassNotFoundException ex) {
		// Let a chance to class to be loaded by default implementation
		rLogger.info(
		    "Ignoring ClassNotFoundException in default classloader: " +
		    ex);
	    }
	}
	if (resolve) {
	    rLogger.finest("calling resolveClass()()");
	    resolveClass(loadedClass);
	    rLogger.finest("back from resolveClass()()");
	}
	rLogger.finest("-- EXIT: " + name + " loaded?: " + loadedClass);
	return loadedClass;
    } // loadClass

    private InputStream getInputStream(String classname) throws IOException {
	rLogger.finest("-- ENTER: " + classname);
	InputStream is = null;
	JarEntry entry = CheckUtils.getJarEntry(classname, checksCache,
	    rLogger);

	if (entry != null) {
	    rLogger.finest("found check jar entry in cache");
	    try {
		is = checksCache.jar.getInputStream(entry);
		rLogger.finest("got input stream");
	    } catch (IOException ex) {
		rLogger.finest("rethrowing IOException: " + ex);
		throw ex;
	    }
	} else {
	    rLogger.info("unable to locate jar entry in cache");
	}
	rLogger.finest("-- EXIT --");

	return is;
    } // getInputStream

} // class
