/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)IfconfigEntry.java	1.2	08/05/29 SMI"
 */
package com.sun.cluster.cfgchk.datasrc.dataObject;

import java.util.Enumeration;
import java.util.Properties;
import java.util.logging.Logger;
import com.sun.cluster.cfgchk.utils.StringUtils;

/*
 * This class holds a single instance of an entry from ifconfig output.
 * Held in an array in Ifconfig.java to represent the present system.
 */
public class IfconfigEntry {

	private String name = null;
	private String type = null;
	private String number = null;
	private String[] flags = null;
	private String flagsString = null;
	private Properties properties = null;
	private Logger logger = null;

	public IfconfigEntry(Logger logger) {
		this.logger = logger;
		logger.finest("--ENTER--");
		properties = new Properties();
	} // ()

	public Properties getProperties() {
		return properties;
	}

	public String getProperty(String key) {
		return properties.getProperty(key);
	}
	public String getPropertiesString() {
		StringBuffer sb = new StringBuffer("");

		String SEP1 = " | ";
		String SEP2 = ": ";
		Enumeration e = properties.propertyNames();
		while (e.hasMoreElements()) {
			String key = (String)e.nextElement();
			String value = properties.getProperty(key);
			if (value == null) {
				value = "<null>";
			}
			if (sb.length() > 0) sb.append(SEP1);
			sb.append(key + SEP2 + value);
		}
		return sb.toString();
	} // getPropertiesString

	public void setProperty(String key, String value) {
		logger.finest("setting: >" + key + "< >" + value + "<");
		properties.setProperty(key, value);
	}

	public void setName(String value) {
		logger.finest("setting: >" + value + "<");
		name = value;
	}
	public String getName() {
		return name;
	}
	public void setType(String value) {
		logger.finest("setting: >" + value + "<");
		type = value;
	}
	public String getType() {
		return type;
	}
	public void setNumber(String value) {
		logger.finest("setting: >" + value + "<");
		number = value;
	}
	public String getNumber() {
		return number;
	}

	// assumes incoming comma separated string
	public void setFlags(String value) {
		logger.finest("setting: >" + value + "<");
		flags = StringUtils.stringArrayFromEncodedString(value, ",");
		flagsString = value;
	}
	public String[] getFlags() {
		return flags;
	}
	public String getFlagsString() {
		return flagsString;
	}
	public boolean containsFlag(String target) {
		return (StringUtils.indexOfStringInStringArray(target,
			flags) > -1);
	}

	public String toString() {
		String SEP = " | ";
		StringBuffer sb = new StringBuffer(name);
		sb.append(SEP + "<" + flagsString + ">");
		sb.append(SEP + getPropertiesString());
		return sb.toString();
	}

} // class
