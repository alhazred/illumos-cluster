/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)CheckS6708496.java	1.3	08/08/26 SMI"
 */
package com.sun.cluster.cfgchk.check;

import java.util.StringTokenizer;

import com.sun.cluster.cfgchk.BasicCheck;
import com.sun.cluster.cfgchk.Check;
import com.sun.cluster.cfgchk.CheckExecutionException;
import com.sun.cluster.cfgchk.datasrc.DataException;
import com.sun.cluster.cfgchk.utils.Utils;
import com.sun.cluster.cfgchk.utils.logger.LoggerUtils;

/* BEGIN JSTYLED */
/**
 *
 * 6708496 - 3067 (medium)
 *
 * Single node check.
 * "Cluster node (3.1 or later) OpenBoot Prom (OBP) has local-mac-address? variable set to 'false'."
 */
/* END JSTYLED */

/*
 * No I18n of any strings in this class.
 * Exceptions thrown from Checks are caught and handled by Engine
 * typically just by logging. Messages are never displayed to user.
 *
 * Lines with trailing 'TOC' comment are auto-gathered into
 * table of contents listing of all checks.
 *
 * Text lines intended for output to report must not contain "<" in order
 * not to mess up the xml.
 */

public class CheckS6708496 extends BasicCheck {

	public CheckS6708496() {
		checkID = "S6708496"; // TOC
		severity = Check.MODERATE; // TOC
		// sccs keyword: must be 1.3 when checked out under sccs
		version = "1.3";
		// sccs keyword: must be 08/08/26 when checked out under sccs
		revisionDate = "09/12/08";

		keywords.add("SunCluster3");
		keywords.add("single");
		keywords.add("eeprom");
		keywords.add("installtime");  // can't depend on clustermode

		/* BEGIN JSTYLED */
		problemStatement = "Cluster node (3.1 or later) OpenBoot Prom (OBP) has local-mac-address? variable set to 'false'."; // TOC
		/* END JSTYLED */
	} // ()

	/**
	 * applies only to sparc
	 */
	protected String assignApplicabilityLogic() {
		String aLogic = "Applicable to SPARC architecture only.";
		return aLogic;
	} // assignApplicabilityLogic


	/**
	 * @return a string describing the logic of the check
	 */
	protected String assignCheckLogic() {
		/* BEGIN JSTYLED */
		String cLogic = "Exec 'eeprom' command and test output for 'local-mac-address?=[word]'. If [word] is not equal to 'false,' the check fails.";
		/* END JSTYLED */
		return cLogic;
	} // assignCheckLogic


	public String getAnalysis() {
		/* BEGIN JSTYLED */
		String analysis = "Starting with version 3.1 of Sun Cluster local-mac-address? must be set to 'true.' Proper operation of the public networks depends on each interface having a different MAC address.";
		/* END JSTYLED */
		return analysis;
	} // getAnalysis

	public String getRecommendations() {
		/* BEGIN JSTYLED */
		String recommend = "" +
			"Change the local-mac-address? variable to true:" +
			"\n\t1) From the OBP (ok> prompt):"+
			"\n\t\tok> setenv local-mac-address? true" +
			"\n\t\tok> reset" +
			"\n\t2) Or as root:" +
			"\n\t\t# /usr/sbin/eeprom local-mac-address?=true" +
			"\n\t\t# init 0" +
			"\n\t\tok> reset";
		/* END JSTYLED */
		return recommend;
	} // getRecommendations

	// applicable to SPARC only
	public boolean isApplicable() throws CheckExecutionException {
		boolean applicable = false;
		logger.info("-- ENTER --");
		try {
			String arch = getDataSrc().getArchitecture();
				// DEx already I18n
			logger.info("arch: " + arch);

			applicable = (arch.equalsIgnoreCase("sparc"));
			logger.info("applicable: " + applicable);

		} catch (Exception e) {
			logger.warning("Exception: " + e);
			throw new CheckExecutionException(e);
		}
		return applicable;
	} // isApplicable

	public boolean doCheck() throws CheckExecutionException {
		logger.info("-- ENTER --");
		boolean passes = false;

		try {
			/*
			 * Get output of 'eeprom' command
			 * Parse for line starting with 'local-mac-address?='
			 * value must be 'true' else check fails
			 */
			String[] cmd = { "/usr/sbin/eeprom" };
			String[] eepromOut = null;
			try {
				eepromOut =
				    Utils.runCommandStrArray(getNodename(),
					cmd, /* usingCacao */ false);

			} catch (DataException dex) {
				logger.info("DataException: " + dex);
				throw new CheckExecutionException(dex);
			}

			boolean matched = false;
			String value = null;
			for (int i = 0; i < eepromOut.length; i++) {
				if (eepromOut[i].startsWith(
					    "local-mac-address?")) {
					String line = eepromOut[i];
					matched = true;
					logger.info("matched target key: " +
					    line);
					StringTokenizer st =
					    new StringTokenizer(line, "=");
					if (st.countTokens() != 2) {
						logger.info("Incorrect " +
						    "token count: " + line);
						matched = false;
						continue;
					}
					st.nextToken(); // local-mac-address?
					value = st.nextToken();
					logger.info("value: " + value);
					break;
				}
			}

			if (matched == false) {
				String msg = "Fatal: failed to find " +
				    "local-mac-address? value in " +
				    "eeprom output."; // no I18n
				logger.info(msg);
				throw new CheckExecutionException(msg);
			}

			if (value.equalsIgnoreCase("true")) {
				passes = true;
			}

		} catch (Exception e) {
			logger.warning("Exception: " + e.getMessage());
			LoggerUtils.exception("Exception", e, logger);
			throw new CheckExecutionException(e);
		}

		logger.info("-- EXIT passes: " + passes);
		return passes;
	} // doCheck

} // class

