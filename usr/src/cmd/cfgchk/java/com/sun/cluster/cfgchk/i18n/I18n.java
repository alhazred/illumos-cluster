/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)I18n.java	1.2	08/05/29 SMI"
 */
package com.sun.cluster.cfgchk.i18n;

import java.text.MessageFormat;
import java.util.Hashtable;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.logging.Logger;

import com.sun.cluster.cfgchk.Globals;
import com.sun.cluster.cfgchk.utils.logger.RemoteLogger;

public class I18n {

    /**
     *
     * I18n provides UI access to message strings from (localized) property
     * files.
     *
     * There is only one instance of this class per VM. Callers use one of the
     * static getLocalized() methods sending in their own logger if they wish.
     * If logger is null then messages will log to cacao.0
     *
     * Locale and textdomain may be specified or defaulted; an array of Objects
     * may be provided as relocatible arguments for messages using the {0} {1}
     * ... notation. See java.util.ResourceBundle for more details.
     *
     * Strings are delivered in property files or resource bundles and must be
     * found via CLASSPATH, either at java invocation or via cacao's module reg
     * xml file in section 'public-path'
     *
     * 'Textdomain' is the name of the resource file and defaults to
     * Globals.TEXTDOMAN.
     *
     * For example, if textdomain is "resources.cfgchk", then the resource file
     * must be installed as /usr/cluster/lib/cfgchk/resources/cfgchk.properties.
     * CLASSPATH must therefore include /usr/cluster/lib/cfgchk/.
     *
     * Localized versions of cfgchk.properties are installed in the very same
     * directory. A basic Japanese version will be called
     * 'cfgchk_ja.properties'. If there is special version for country "Foo" and
     * variant "Bar" it would be named 'cfgchk_ja_FOO_bar.properties' and
     * installed in the same directory.
     *
     * The I18n singleton provides a cache of all encountered resource files,
     * organized by locale.
     */

    /*
     * main table: one entry for each locale each entry is another hashtable
     * each entry of subtable is a resource bundl keyed by textdomain (String)
     */
    private static Hashtable locales = new Hashtable();

    // default Logger object
    // used if caller sends in null for logger
    private static Logger mLogger = RemoteLogger.getRemoteLogger();

    /*
     * singleton instance holds cache of resource bundles organized by locale
     */
    private static I18n privateI18n = I18n.getI18nInstance(mLogger);
	 // singleton

    /*
     * private constructor
     */
    private I18n() {
    }

    /*
     * get singleton instance
     */
    private static synchronized I18n getI18nInstance(Logger logger) {
        if (privateI18n == null) {
            privateI18n = new I18n();
            String lMsg = "initializing cache";
            if (logger != null)
                logger.info(lMsg);
            else
                mLogger.info(lMsg);
        }
        return privateI18n;
    } // getI18nInstance

    //
    // getLocalized() methods: a variety of signatures...
    //

    /**
     * getLocalized(Logger logger, String key)
     *
     * default textdomain: GLOBALS.TEXTDOMAIN default locale no relocatible args
     *
     * @param key
     *            Key to message in resource file
     */
    public static String getLocalized(Logger logger, String key) {
        String textdomain = Globals.TEXTDOMAIN;
        Locale locale = Locale.getDefault();
        return I18n.getLocalized(logger, locale, textdomain, key, null);
    } // getLocalized

    /**
     * getLocalized(Logger logger, String textdomain, String key)
     *
     * default locale no relocatible args
     *
     * @param textdomain
     *            Identifier of resource file. Must be in classpath. May be a
     *            dot separated hierarchical name as long as the root of the
     *            'path' is found under a CLASSFILE entry.
     * @param key
     *            Key to message in resource file
     */
    public static String getLocalized(Logger logger, String textdomain,
            String key) {
        Locale locale = Locale.getDefault();
        return I18n.getLocalized(logger, locale, textdomain, key, null);
    } // getLocalized

    /**
     * getLocalized(Logger logger, Locale locale, String key)
     *
     * default textdomain no relocatible args
     *
     * @param locale
     *            Localized version to search for.
     * @param key
     *            Key to message in resource file
     */
    public static String getLocalized(Logger logger, Locale locale,
	String key) {
        String textdomain = Globals.TEXTDOMAIN;
        return I18n.getLocalized(logger, locale, textdomain, key, null);

    } // getLocalized

    /**
     * getLocalized(Logger logger, Locale locale, String textdomain, String key)
     *
     * no relocatible args
     *
     * @param locale
     *            Localized version to search for.
     * @param textdomain
     *            Identifier of resource file. Must be in classpath. May be a
     *            dot separated hierarchical name as long as the root of the
     *            'path' is found under a CLASSFILE entry.
     * @param key
     *            Key to message in resource file
     */
    public static String getLocalized(Logger logger, Locale locale,
            String textdomain, String key) {
        return I18n.getLocalized(logger, locale, textdomain, key, null);

    } // getLocalized

    /**
     * getLocalized(Logger logger, String key, Object[] i18nArgs)
     *
     * default textdomain: GLOBALS.TEXTDOMAIN default locale
     *
     * @param key
     *            Key to message in resource file
     * @param i18nArgs
     *            Relocatible arguments
     */
    public static String getLocalized(Logger logger, String key,
            Object[] i18nArgs) {
        String textdomain = Globals.TEXTDOMAIN;
        Locale locale = Locale.getDefault();
        return I18n.getLocalized(logger, locale, textdomain, key, i18nArgs);
    } // getLocalized

    /**
     * getLocalized(Logger logger, String textdomain, String key, Object[]
     * i18nArgs)
     *
     * default locale
     *
     * @param textdomain
     *            Identifier of resource file. Must be in classpath. May be a
     *            dot separated hierarchical name as long as the root of the
     *            'path' is found under a CLASSFILE entry.
     * @param key
     *            Key to message in resource file
     * @param i18nArgs
     *            Relocatible arguments
     */
    public static String getLocalized(Logger logger, String textdomain,
            String key, Object[] i18nArgs) {
        Locale locale = Locale.getDefault();
        return I18n.getLocalized(logger, locale, textdomain, key, i18nArgs);
    } // getLocalized

    /**
     * getLocalized(Logger logger, Locale locale, String key, Object[] i18nArgs)
     *
     * default textdomain
     *
     * @param locale
     *            Localized version to search for.
     * @param key
     *            Key to message in resource file
     * @param i18nArgs
     *            Relocatible arguments
     */
    public static String getLocalized(Logger logger, Locale locale, String key,
            Object[] i18nArgs) {
        String textdomain = Globals.TEXTDOMAIN;
        return I18n.getLocalized(logger, locale, textdomain, key, i18nArgs);
    } // getLocalized

    /**
     * getLocalized(Logger logger, Locale locale, String textdomain, String key,
     * Object[] i18nArgs)
     *
     * specified locale; specified textdomain; relocatible args
     *
     * Wraps MessageFormat.format(String pattern, Object[] args);
     *
     * If texdomain not found for locale or key not found then key is returned.
     *
     * @param locale
     *            Localized version to search for.
     * @param textdomain
     *            Identifier of resource file. Must be in classpath. May be a
     *            dot separated hierarchical name as long as the root of the
     *            'path' is found under a CLASSFILE entry.
     * @param key
     *            Key to message in resource file
     * @param i18nArgs
     *            Relocatible arguments
     */
    public static String getLocalized(Logger logger, Locale locale,
            String textdomain, String key, Object[] i18nArgs) {

        String lMsg = "key: " + key + " ; locale: " + locale;
        if (logger != null)
            logger.finest(lMsg);
        else
            mLogger.finest(lMsg);

        ResourceBundle rb = getResourceBundle(logger, locale, textdomain);

        if (rb == null) {
            // 'failure' already logged
            return key;
        }

        String str = key;
        try {
            if (i18nArgs == null) {
                str = rb.getString(key);
            } else {
                String pattern = rb.getString(key);
                str = MessageFormat.format(pattern, i18nArgs);
            }
        } catch (java.util.MissingResourceException mrex) {
            lMsg = "key not found: " + key;
            if (logger != null)
                logger.warning(lMsg);
            else
                mLogger.warning(lMsg);
        }

        return str;
    } // getLocalized

    /*
     * Internal methods
     */

    /**
     * getResourceBundle(Locale locale, String textdomain)
     *
     * look for matching resource bundle in specified locale in cache; if not
     * found attempt to load from classpath if found, store in cache
     */
    private static ResourceBundle getResourceBundle(Logger logger,
            Locale locale, String textdomain) {

        String localeName = locale.toString();

        ResourceBundle rb = null;
        Hashtable localeTable = null;

        /*
         * fetch htab for locale if one call specifies a 'full' locale and
         * another specifies a 'partial' locale they will be two separate
         * entries in locales hashtab
         */

        synchronized (locales) {
            localeTable = (Hashtable) locales.get(localeName);
            if (localeTable == null) {

                // no table yet for this locale; make one
                localeTable = new Hashtable();
                locales.put(localeName, localeTable);
                String lMsg = "new locale table created for " + localeName;
                if (logger != null)
                    logger.finest(lMsg);
                else
                    mLogger.finest(lMsg);
            }
        } // synch locales

        String lMsg = null;
        rb = (ResourceBundle) localeTable.get(textdomain);
        if (rb == null) {
            lMsg = "resource bundle not found in cache for: " + textdomain
                    + " in locale: " + localeName;
            if (logger != null)
                logger.finest(lMsg);
            else
                mLogger.finest(lMsg);

            // not in cache: go find it in classpath
            rb = loadResourceBundle(locale, textdomain);

            if (rb != null) {
                /*
                 * found it: store in locale-specific table
                 */
                localeTable.put(textdomain, rb);
                String foundLocale = rb.getLocale().toString();
                if (foundLocale.length() == 0) {
                    foundLocale = "default";
                }
                lMsg = "rb located for " + textdomain + " in " + foundLocale
                        + " locale and added to cache.";
                if (logger != null)
                    logger.finest(lMsg);
                else
                    mLogger.finest(lMsg);

            } else {
                // just log a message
                lMsg = "resource file not found " + "for texdomain: "
                        + textdomain + " in locale: " + localeName;
                if (logger != null)
                    logger.warning(lMsg);
                else
                    mLogger.warning(lMsg);
            }
        } else {
            lMsg = "resource bundle retrieved from cache";
            if (logger != null)
                logger.finest(lMsg);
            else
                mLogger.finest(lMsg);

        }

        return rb;
    } // getResourceBundle

    /*
     * Call ResourceBundle to search for resource file under CLASSPATH. Default
     * fallback pattern used. See ResourceBundle for details.
     */
    private static ResourceBundle loadResourceBundle(Locale locale,
            String textdomain) {
        ResourceBundle rb = null;

        try {
            rb = ResourceBundle.getBundle(textdomain, locale);
        } catch (MissingResourceException mrex) {
            // ignore and just return null
            // caller will log a message
        }
        return rb;
    } // loadResourceBundle

    // built-in test routine; needs ksh wrapper. See SC Geo Edition for example.
    public static void main(String args[]) {
        System.out.println("I18n.main() -- ENTER --");
        Logger logger = null;

        // test allows an optional single relocatible arg
        // test allows optional locale-name (if arg is also given)

        if (args.length < 2) {
            System.out.println("usage: I18n <textdomain> <key> "
                    + "[<single relocatible arg> "
                    + "[<lang>] [<country>]] [<variant>]");
            System.exit(0);
        }

        String[] argArray = null;
        if (args.length > 2) {
            argArray = new String[1];
            argArray[0] = args[2];
        }

        String lang = "";
        if (args.length > 3)
            lang = args[3];
        System.out.println("I18n.main() lang: " + lang);

        String country = "";
        if (args.length > 4)
            country = args[4];
        System.out.println("I18n.main() country: " + country);

        String variant = "";
        if (args.length > 5)
            variant = args[5];
        System.out.println("I18n.main() variant: " + variant);

        Locale locale = null;

        String localeName = lang + country + variant;
        System.out.println("I18n.main() localeName: " + localeName);

        if (localeName.length() > 0) {
            locale = new Locale(lang, country, variant);
        } else {
            locale = Locale.getDefault();
        }
        System.out.println("I18n.main() locale: " + locale.toString());

        String incantation = "getLocalized(" + locale.toString() + ", "
                + args[0] + ", " + args[1] + ", " + args[2] + ")";
        System.out.println("I18n.main() FIRST  calling " + incantation);

        System.out.println(I18n.getLocalized(logger, locale, args[0], args[1],
                argArray));

        System.out.println("I18n.main() SECOND calling " + incantation);

        System.out.println(I18n.getLocalized(logger, locale, args[0], args[1],
                argArray));

        System.out.println("I18n.main() -- EXIT --");
    } // main

} // I18n

