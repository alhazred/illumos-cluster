/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)Ifconfig.java	1.4	08/08/26 SMI"
 */
package com.sun.cluster.cfgchk.datasrc.dataObject;

import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import com.sun.cluster.cfgchk.datasrc.BasicDataSrc;
import com.sun.cluster.cfgchk.datasrc.DataException;
import com.sun.cluster.cfgchk.datasrc.ParseException;

/*
 * Specialty class for handling ifconfig data
 * Holds an array of IfconfigEntry
 */
public class Ifconfig {

	private IfconfigEntry[] entries = null;
	private BasicDataSrc dataSrc = null;
	private Logger logger = null;

    // for parsing 'ifconfig -a' output
    final private static String interfaceHeaderPatternString =
	"^((\\S+?)(\\d+)):\\s+flags=\\w+<([^>]+)>(.*)$";
	// beginning of line plus non-whitespace chars plus digits plus ":"
	// plus whitespace plus 'flags=' plus word char plus '<' plus
	// anything up to next '>' plus anything to EOL
	// (CR 671248) switched to use of word char since
	// flag need not be decimal
    final private static String interfacePropertiesPatternString =
	"(?:\\s*(\\S+)\\s+(\\S+)(?:.*))";
	// "^(?:(\\S+?)\\s+?(\\S+?))+$";
	// "^\\s+(?:\\s*(\\S+)\\s+(\\S+))+$";

    // Prepare patterns
    private static Pattern interfaceHeaderPattern = null;
    private static Pattern interfacePropertiesPattern = null;
	private static ParseException pex = null;
	static {
	    try {
		    interfaceHeaderPattern =
			Pattern.compile(interfaceHeaderPatternString);
		    interfacePropertiesPattern =
			Pattern.compile(interfacePropertiesPatternString);
	    } catch (PatternSyntaxException e) {
		    // complaint deferred until session logger is passed in
		    pex =  new ParseException(e);
	    }
	}

	public Ifconfig(BasicDataSrc dataSrc, Logger lg) throws ParseException {
		this.logger = lg;
		logger.info("-- ENTER --");
		this.dataSrc = dataSrc;
		if (pex != null) {
			logger.warning("Exception compiling " +
			    "NetworkInterface pattern: " + pex);
			throw (pex);
		}
		parseIfconfigFile(); // throws PEx
	} // ()

	/**
	 * return array of IfconfigEntry
	 * may have null elements at end of array
	 */
	public IfconfigEntry[] getEntries() {
		return entries;
	} // getEntries

	public IfconfigEntry getEntry(String name) {
		IfconfigEntry entry = null;
		for (int i = 0; i < entries.length; i++) {
			if (entries[i].getName().equals(name)) {
				entry = entries[i];
				break;
			}
		}
		return entry;
	} // getEntry


	// fills global IfconfigEntry[] entries
	// the returned array may have nulls at teh end
	private void parseIfconfigFile() throws ParseException {
		logger.info("-- ENTER --");
		String[] rawData = getRawData();
		entries = new IfconfigEntry[rawData.length]; // throws PEx

		// this number is number of lines read
		// since each entry is composed of multiple lines
		// this number is > # entries. Unused entries
		// are null
		logger.info("entries[] size: " + rawData.length);
		int entriesIndex = 0;
		IfconfigEntry newEntry = null;
		for (int i = 0; i < rawData.length; i++) {
			String line = rawData[i].trim(); // trim() is required!
			logger.info("rawData["+i+"}: >>>" + line + "<<<");

			Matcher rem = null;
			if ((rem =
			    interfaceHeaderPattern.matcher(line)).matches()) {
				// matches header
				logger.info("line matches header line: " +
				    line);

				// save prior entry just built
				if (newEntry != null) {
					logger.info("gonna save mid entry #: " +
					    entriesIndex);
					entries[entriesIndex++] = newEntry;
				}
				newEntry = new IfconfigEntry(logger);
				applyHeaderLine(line, newEntry, rem);
			} else if (
				(rem = interfacePropertiesPattern.
				    matcher(line)).matches()) {
				// matches property only line
				logger.info("line matches properties line: " +
				    line);
				if (newEntry == null) { // never matched header
					logger.info("property match without " +
					    "header line; throwing " +
					    " ParseException on line: " + line);
					throw new ParseException(line);
				}
				applyPropertiesLine(line, newEntry, rem);
			} else {
				// no match: parse error
				logger.info("no header or property match; " +
				    "throwing ParseException: " + line);
				throw new ParseException(line);
			}


		}

		// store the entry just assembled
		if (newEntry != null) {
			logger.info("gonna save last entry #: " + entriesIndex);
			entries[entriesIndex++] = newEntry;
		}

		logger.info("-- EXIT --");
	} // parseIfconfigFile

	private void applyHeaderLine(String line, IfconfigEntry entry,
	    Matcher rem) throws ParseException {
		int numElems = rem.groupCount();
		entry.setName(rem.group(1));
		entry.setType(rem.group(2));
		entry.setNumber(rem.group(3));
		entry.setFlags(rem.group(4));
		if (numElems > 4) { // now treat properties
			String propsPortion = rem.group(5).trim();
				// trim() is required!
			rem = interfacePropertiesPattern.matcher(propsPortion);
                        applyPropertiesLine(propsPortion, entry, rem);
		}
	} // applyHeaderLine

	private void applyPropertiesLine(String line, IfconfigEntry entry,
	    Matcher rem) throws ParseException {
		 logger.info("line: >>" + line + "<<");
		if (rem.find(0) != false) {
			for (int i = 0; i <= rem.groupCount(); i++) {
				 logger.info("find(0) rem.group("+i+"): >>" +
				 rem.group(i) + "<<");
			}
			entry.setProperty(rem.group(1), rem.group(2));
		} // if

		int nextIndex = rem.end(2);
		while (rem.find(nextIndex) != false) {
			entry.setProperty(rem.group(1), rem.group(2));
			nextIndex = rem.end(2);
		}
	} // applyPropertiesLine


	private String[] getRawData() throws ParseException {
		String[] stdout = null;
		try {
			String[] cmd = { "/usr/sbin/ifconfig", "-a" };
			stdout = dataSrc.runCommandStrArray(cmd);
			// throws DataException if unable to execute cmd
			// message contains exit code and stderr as String

		} catch (DataException e) {
			logger.info("DataException(); throwing " +
			    "ParseException: " + e.getMessage());
			throw new ParseException(e);
		} catch (Exception e) {
			logger.info("Exception(); throwing ParseException: " +
			    e.getMessage());
			throw new ParseException(e);
		}

		return stdout;
	} // getRawData

} // class
