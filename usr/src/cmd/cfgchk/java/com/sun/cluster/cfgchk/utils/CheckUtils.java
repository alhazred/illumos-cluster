/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)CheckUtils.java	1.2	08/05/29 SMI"
 */
package com.sun.cluster.cfgchk.utils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.logging.Logger;

import com.sun.cluster.cfgchk.Check;
import com.sun.cluster.cfgchk.BasicCheck;
import com.sun.cluster.cfgchk.BasicCheckMultiNode;
import com.sun.cluster.cfgchk.CheckExecutionException;
import com.sun.cluster.cfgchk.Globals;
import com.sun.cluster.cfgchk.check.AnonymousCheck;
import com.sun.cluster.cfgchk.datasrc.ClusterDataSrc;
import com.sun.cluster.cfgchk.datasrc.DataException;
import com.sun.cluster.cfgchk.i18n.I18n;

import com.sun.cluster.cfgchk.utils.logger.LoggerUtils;

/**
 * Misc utility routines related to Checks
 *	getSeverityName()
 *	mostSevere()
 *	shouldSkipCheckForSeverity()
 *	shouldSkipCheckForKeyword()
 *	listAllChecks()
 *	listChecks()
 *	getAllCheckIDs()
 *	getSpecifiedChecks()
 *	getCheck()
 *	getAllCheckJarNames()
 *	getCheckJarEntries()
 *	isSameCluster()
 */
public class CheckUtils implements Globals {

	// control codes for listFactoryChecks
	public static final int ID_ONLY = 0;
	public static final int SYNOPSIS = 1;	// adds problem stmnt & severity
	public static final int LOGIC = 2;	// adds applicability &
						//  evaluation logic statements

	public static String getSeverityName(int severityIndex) {
		return Check.SEVERITY_NAMES[severityIndex];
	} // getSeverityName

	/*
	 * @return most severe of two input severities
	 */
	public static int mostSevere(int severity1, int severity2) {

		// smaller numbers are more severe
		// See defines in Check.java

		int max = severity2;

		if (severity1 == 0) {
			max = severity2;
		} else if (severity2 == 0) {
			max = severity1;
		} else if (severity1 < severity2) {
			max = severity1;
		}
		return max;
	} // mostSevere

	public static boolean shouldSkipCheckForSeverity(Check ck,
	    int minSeverity) {
		// smaller numbers are more severe
		// except for special case of value 0
		// See defines in Check.java
		boolean shouldSkip = false;
		int sev = ck.getSeverity();

		// low severity number is more severe
		// than higher number
		if (sev > minSeverity) {
			shouldSkip = true;
		}

		// never skip variable severity checks
		if (sev == Check.VARIABLE_SEV) {
			shouldSkip = false;
		}
		return shouldSkip;
	} // shouldSkipCheckForSeverity

	/*
	 * given an encoded String of keywords to accept (user input)
	 * examine keywords on the Check for a match
	 * if no match then return true: yes, shouldSkip
	 */
	public static boolean shouldSkipCheckForKeyword(Logger logger,
	    Check ck, String userKeywords) {
		//
		logger.info("-- ENTER --");
		boolean shouldSkip = true;
		List checkKeywords = ck.getKeywordsList();
		String[] targetKeywords =
		    StringUtils.stringArrayFromEncodedString(logger,
			userKeywords, LIST_ENCODING_SEP);

		Iterator it = checkKeywords.iterator();
		label: while (it.hasNext()) {
			String ckKeywd = (String)it.next();
			logger.info("consider: " + ckKeywd);
			for (int i = 0; i < targetKeywords.length; i++) {
				logger.info("   test against: " +
				    targetKeywords[i]);
				if (ckKeywd.equalsIgnoreCase(
					    targetKeywords[i])) {
					logger.info("   match!");
					shouldSkip = false;
					break label;
				}
			} // for targetKeywords
		} // while checkKeywords

		logger.info("-- EXIT --");
		return shouldSkip;
	} // shouldSkipCheckForSeverity

	/*
	 *  may be size 0 but never null
	 */
	public static String[] listAllChecks(ChecksCache[] checksCache,
	    Logger logger) {
		String[] checklist = null;
		checklist = getAllCheckIDs(logger, checksCache);
			// may be size 0 but never null
		return checklist;
	} // listAllChecks

	// may return size 0 but never null
	public static String[] listChecks(int verbosity,
	    JarClassLoader jarClassLoader, String[] checklist,
	    ChecksCache[] checksCache, Logger logger) {

		String[] displayValues = null;
		switch (verbosity) {
		case SYNOPSIS:
			displayValues = getCheckSynopses(logger, checklist,
			    checksCache, jarClassLoader);
			break;
		case LOGIC:
			displayValues = getCheckFullText(logger, checklist,
			    checksCache, jarClassLoader);
			break;
		default:
			break;
		} // switch
		return displayValues;
	} // listChecks

	// checkID + severity + synopsis
	// possibly size 0 but never null
	private static String[] getCheckSynopses(Logger logger,
	    String[] checklist, ChecksCache[] checksCache,
	    JarClassLoader jarClassLoader) {

		logger.info("-- ENTER --");
		Check[] checks = getSpecifiedChecks(logger, checklist,
		    checksCache, jarClassLoader);
		// possibly size 0 but never null

		String[] results = new String[checks.length];
		logger.info("-- CONTINUE --");

		for (int i = 0; i < checks.length; i++) {
			Check ck = checks[i];
			logger.fine("checkID: " + checklist[i]);
			String checkID = checklist[i];
				// will be overwritten in a moment
			if (ck != null) {
				logger.info("checks[" + i + "].getID(): " +
				    ck.getID());
				checkID = StringUtils.padString(
					ck.getID(), 10, ' ');
				String severity = StringUtils.padString(
					"(" + ck.getSeverityName() + ")",
						13, ' ');
				String synopsis = ck.getSynopsis();

				// save checkID + synopis for display
				results[i] = checkID + ":\t" + severity +
				    synopsis;
			} else { // was unable to locate check
				logger.info("got null check for " + checkID);
				checkID = StringUtils.padString(checklist[i],
				    10, ' ');
				String lMsg = I18n.getLocalized(logger,
				    "check.not.found");

				// save non-fatal error message for display
				results[i] =  checkID + ":\t" + lMsg;
			}
		}
		logger.info("returning results of size: " + results.length);
		return  results;
	} // getCheckSynopses


	// checkID + severity + synopsis + keywords + applic & check logic +
	// version and revision dates
	// will not return null; might return array of size 0
	private static String[] getCheckFullText(Logger logger,
	    String[] checklist, ChecksCache[] checksCache,
	    JarClassLoader jarClassLoader) {
		logger.info(" -- ENTER --");
		Check[] checks = getSpecifiedChecks(logger, checklist,
		    checksCache, jarClassLoader);
		// possibly size 0 but never null

		String[] results = new String[checks.length];
		logger.info("-- CONTINUE --");

		for (int i = 0; i < checks.length; i++) {
			Check ck = checks[i];
			String checkID = "------"; // no I18n
			if (ck != null) {
				checkID = ck.getID();
				String synopsis = ck.getSynopsis();
				String keywords = ck.getKeywords();
				String severity = ck.getSeverityName();
				String applLogic = ck.getApplicabilityLogic();
				String checkLogic = ck.getCheckLogic();
				String version = ck.getVersion();
				String revisionDate = ck.getRevisionDate();

				String[] i18nArgs = { keywords };
				String keywordsStr = "\n" +
					I18n.getLocalized(logger,
					    "list.checks.label.keyword",
					    i18nArgs);

				i18nArgs[0] = applLogic;
				String applLogicStr = "\n" +
					I18n.getLocalized(logger,
					    "list.checks.label.applicability",
					    i18nArgs);

				i18nArgs[0] = checkLogic;
				String checkLogicStr = "\n" +
					I18n.getLocalized(logger,
					    "list.checks.label.checklogic",
					    i18nArgs);

				i18nArgs[0] = version;
				String versionStr = "\n" +
					I18n.getLocalized(logger,
					    "list.checks.label.version",
					    i18nArgs);

				i18nArgs[0] = revisionDate;
				String revisionDateStr = "\n" +
					I18n.getLocalized(logger,
					    "list.checks.label.revisiondate",
					    i18nArgs);

				results[i] = checkID + ": (" + severity + ") " +
				    synopsis +
					keywordsStr +
					applLogicStr +
					checkLogicStr +
					versionStr +
					revisionDateStr +
					"\n";

			} else {
				String lMsg = I18n.getLocalized(logger,
				    "check.instantiation.failed");
				results[i] = checkID + ":" + lMsg;
			}
		}
		return  results;
	} // getCheckFullText


	// return String[] of all checkIDs in cache
	// will not return null; might return array of size 0
	public static String[] getAllCheckIDs(Logger logger,
	    ChecksCache[] checksCache) {
		logger.info("-- ENTER --");

		JarEntry[] entries = null;
		JarEntry entry = null;

		int numChecks = 0;
		for (int i = 0; i < checksCache.length; i++) {
			numChecks += checksCache[i].entries.length;
		}

		String[] checkids = new String[numChecks];

		int index = 0;
		for (int i = 0; i < checksCache.length; i++) {
			entries = checksCache[i].entries;
			for (int j = 0; j < entries.length; j++) {
				entry = entries[j];
				if (isCheckClass(entry, logger)) {
					String checkID =
					    getCheckNameFromClassName(
						    entry.getName(), logger);
					logger.info("checkID: " + checkID);
					checkids[index++] = checkID;
				}

			} // single cache
		} // for all caches
		logger.info(" -- EXIT --");

		return checkids;
	} // getAllCheckIDs

	// return Check[] of specified checkIDs
	// Check.init not called on Checks
	// may return array of size 0 but not null
	// may return null check as an element
	// creates an AnonymousCheck if unable to instantiate some Check
	public static Check[] getSpecifiedChecks(Logger logger,
	    String[] checklist, ChecksCache[] checksCache,
	    JarClassLoader jarClassLoader) {
		logger.info("-- ENTER --");
		Check check = null;

		int numChecks = checklist.length;
		Check[] checks = new Check[numChecks];

		int index = 0;
		for (int i = 0; i < checklist.length; i++) {
			logger.info("will get check: " + checklist[i]);

			try {
				check = getCheck(checklist[i], checksCache,
				    jarClassLoader, logger);
			} catch (ClassNotFoundException cnfex) {
				logger.warning("ClassNotFoundException: " +
				    cnfex);
				check = new AnonymousCheck(checklist[i]);
				// contains 'unable to instantiate'
				// problem statement
			}
			if (check == null) {
				// return null; caller will handle
				// must return in same array index
				// as input checklist
				logger.warning("checkID not found: " +
				    checklist[i] + "; returning null check");
			}
			logger.info("storing check for checkID: " +
			    checklist[i]);
			checks[index++] = check;
		} // checklist
		logger.info(" -- EXIT --");

		return checks;
	} // getSpecifiedChecks

	// instantiate check by checkID
	// returns null if unable to locate Check in cache
	// throws ClassNotFoundException if unable to instantiate Check
	public static Check getCheck(String checkID, ChecksCache[] checksCache,
	    JarClassLoader jarClassLoader, Logger logger)
		throws ClassNotFoundException {

		// locate jar entry and its jarfile in cache
		// then call getCheck() signature that uses jarClassLoader
		// to actually instantiate check
		// package unknown

		if (logger != null) logger.info("-- ENTER 'checkID': " +
		    checkID);

		String classname = "Check" + checkID + ".class";
			// not fully qualified
		if (logger != null) logger.info("target classname: " +
		    classname);
		JarFile jar = null;
		JarEntry[] entries = null;
		JarEntry entry = null;
		Check ck = null;
		ChecksCache cache = null;

		// figure out which cache contains the jar and entry
		for (int i = 0; i < checksCache.length; i++) {
			cache = checksCache[i];
			jar = cache.jar;
			logger.finest("iteration " + i +
			    ": calling getJarEntry() for " + classname);
			entry = getJarEntry(classname, cache, logger);
			if (entry != null) {
				logger.info("found entry");
				break;
			}
		} // for all caches

		if (entry != null) {
			// we found the entry in order to get the specific
			// ChecksCache. The classloader needs both in order
			// to handle inner classes
			logger.info("delegate to getCheck() " +
			    "via JarClassLoader");
			ck = getCheck(entry.getName(), jar, cache,
			    jarClassLoader, logger); // InstEx
		} else {
			logger.info("unable to locate check " +
			    "via JarClassLoader");
		}
		logger.info("-- EXIT --");
		return ck;
	} // getCheck

	// return JarEntry for classname or null if not found in this cache
	public static JarEntry getJarEntry(String classname,
	    ChecksCache checksCache, Logger logger) {
		logger.info("-- ENTER: " + classname);
		JarEntry entry = null;

		JarEntry[] entries = checksCache.entries;
		for (int j = 0; j < entries.length; j++) {
			entry = entries[j];
			String entryName = entry.getName();
			logger.fine("consider: " + entryName + " against " +
			    classname);

			/*
			 * entry always looks like
			 * 'com/sun/cluster/cfgchk/check/CheckM123456.class'
			 *
			 * classname may be a full dot separated packagename
			 * without '.class' suffix
			 * or simply a checkID transformed into a
			 * non-qualified Check classname including '.class'
			 */

			if (classname.endsWith(".class")) {

				// compare only unqualified classname
				// not complete package names
				// tiny little chance of collisions if
				// two packages deliver a same named check...
				if (entryName.endsWith(classname)) {
					logger.finest("located " + classname +
					    " in checksCache");
					break; // entry has good value
				}
			} else {
				// compare full package name
				// convert dots to slashes & append '.class'
				// (is regex--must quote the dot)
				String testName =
				    classname.replaceAll("\\.", "/") + ".class";
				logger.fine("testName: >>" + testName + "<<");
				if (entryName.equals(testName)) {
					logger.finest("located " + classname +
					    " in checksCache");
					break; // entry has good value
				}
			}
			entry = null;
			// don't send last considered if we're falling
			//  off the end
		} // for single cache

		logger.info("-- EXIT --");
		return entry;
	} // getJarEntry


	// instantiate check by jar & ChecksCache via custom classloader
	// throws ClassNotFoundException if unable to instantiate Check
	public static Check getCheck(String fullClassname, JarFile jar,
	    ChecksCache checksCache, JarClassLoader jarClassLoader,
	    Logger logger) throws ClassNotFoundException {

		if (logger != null) logger.info("-- ENTER 'jarfile': " +
		    fullClassname);
		String checkClassname =
		    CheckUtils.fullClassnameToClassname(fullClassname, logger);
		if (logger != null) logger.info("checkClassname: " +
		    checkClassname);
		Check ck = null;

		try {
			jarClassLoader.setChecksCache(checksCache);

			Class checkClass =
			    jarClassLoader.loadClass(checkClassname);
			if (logger != null)logger.info("  ** tag 1");
			if (checkClass == null) {
				throw new ClassNotFoundException(
					"CheckUtils.getCheck() unable to " +
						"load class: " +
						checkClassname);
			}

			Object obj = checkClass.newInstance();
			if (logger != null)logger.info("  ** tag 2");

			logger.info("  ===>>>  checkClass type: " +
			    checkClass.getClass().getName());
			logger.info("  ===>>>  obj type: " +
			    obj.getClass().getName());


			if (obj instanceof BasicCheckMultiNode) {
				logger.info("BasicCheckMultiNode: " +
				    checkClassname);
				ck = (BasicCheckMultiNode)obj;
			} else if (obj instanceof BasicCheck) {
				logger.info("BasicCheck: " + checkClassname);
				ck = (BasicCheck)obj;
			} else { // avoid ClassCastException
				logger.info("unable to cast: " +
				    checkClassname);
			}


			if (logger != null)logger.info("MADE CHECK CLASS");

		} catch (ClassNotFoundException cnfex) {
			if (logger != null) {
				logger.info("ClassNotFoundException: " + cnfex);
			}
			throw cnfex;
		} catch (Exception e) {
			logger.info("Exception: " + e);
			LoggerUtils.exception("Exception: " + checkClassname,
			    e, logger);
			throw new ClassNotFoundException(
				"CheckUtils.getCheck() caught Exception", e);
		}
		if (ck == null) {
			logger.warning("Check not created; " +
			    "throwing ClassNotFoundException");
			throw new ClassNotFoundException(
				"CheckUtils.getCheck() unable " +
					"to create class: " + checkClassname);
		}
		if (logger != null) { logger.info("-- EXIT -- "); }
		return ck;
	} // getCheck

	// can return null in rare case where checkName is too short
	private static String getCheckNameFromClassName(String classname,
	    Logger logger) {
		logger.fine("-- ENTER--");
		String checkName = null;

		String leadingPattern = "Check";
		int lastIndex = classname.lastIndexOf("/");
		logger.finest("lastIndex (/): " + lastIndex);
		if (lastIndex != -1) {
			String fname = classname.substring(lastIndex + 1);
			logger.finest("fname: >>" + fname + "<<");
			if (fname.startsWith(leadingPattern)) {
				// chop off leading "Check" and
				// trailing ".class"
				checkName = null;
				lastIndex = fname.lastIndexOf(".class");
				logger.finest("lastIndex (.class): " +
				    lastIndex);

				// len 'Check' + at least 2 chars of check id
				if (lastIndex > 6) {
					checkName = fname.substring(5,
					    lastIndex);
					logger.info("checkName: " + checkName);
				} // chop
			} // leadingPattern
		}
		logger.fine("-- EXIT: " + checkName);
		return checkName;
	} // getCheckNameFromClassName

	// return full dot separated name with ".class" stripped
	private static String fullClassnameToClassname(String fullClassname,
	    Logger logger) {
		String classname = null;
		// chop off trailing ".class"

		int lastIndex = fullClassname.lastIndexOf(".class");
		classname = fullClassname.substring(0, lastIndex);
		logger.info("classname: " + classname);
		classname = classname.replaceAll("/", ".");
		logger.info("classname: " + classname);

		return classname;
	} // fullClassnameToClassname

	private static boolean isCheckClass(JarEntry entry, Logger logger) {
		boolean isCheck = false;
		String checkName = null;

		String eName = entry.getName();
		logger.info("consider: " + eName);

		int subclassIndex = eName.indexOf("$");
		if (subclassIndex == -1) {  // not found == good
			checkName = getCheckNameFromClassName(eName, logger);
			if (checkName != null) {
				logger.info("checkName: " + checkName);
				isCheck = true;
			}
		}

		logger.info("isCheck: " + isCheck);

		return isCheck;
	} // isCheckClass


	public static String[] getAllCheckJarNames(Params params,
	    Logger logger) {
		logger.info("-- ENTER --");

		// get list of user specified jars, if any
		String[] userJars =
		StringUtils.stringArrayFromEncodedString(
		    logger, params.checkJars, LIST_ENCODING_SEP);

		int numUserJars = 0;
		if (userJars != null) {
			numUserJars = userJars.length;
		}

		// assemble array of all jars to dig in for check classes
		// (really just class names that match our pattern)
		String[] jarList = new String[numUserJars + 1]; // + factory jar
		jarList[0] = FACTORY_CHECKS_JAR;
		if (userJars != null) {
			for (int i = 1; i <= numUserJars; i++) {
				jarList[i] = userJars[i - 1];
			}
		}
		logger.info("-- EXIT --");
		return jarList;
	} // getAllCheckJarnames


	public static JarEntry[] getCheckJarEntries(JarFile jar,
	    Logger logger) {
		JarEntry[] entries = null;
		ArrayList alist = new ArrayList();
		entries = new JarEntry[jar.size()];
		Enumeration e = jar.entries();
		JarEntry entry = null;

		/*
		 * don't know how many classes we'll get so
		 * put them into ArrayList then convert to
		 * JarEntry[] at end
		 */
		while (e.hasMoreElements()) {
			entry = (JarEntry)e.nextElement();
			if (isCheckClass(entry, logger)) {
				logger.info("entry stored: " + entry.getName());
				alist.add(entry);
			}
		} // while

		// convert to JarEntry[]
		int numEntries = alist.size();
		entries = new JarEntry[numEntries];
		for (int i = 0; i < numEntries; i++) {
			entry = (JarEntry)alist.get(i);
			entries[i] = entry;
		}


		return entries;
	} // getCheckJarEntries

	// return true if all DataSrcs point to the same cluster
	public static boolean isSameCluster(ClusterDataSrc[] dataSrcs,
	    boolean usingCacao) throws DataException,
        FileNotFoundException, IOException {
		if (dataSrcs == null || dataSrcs.length < 2) {
			return false; // punt
		}

		String firstCluster = dataSrcs[0].getClustername();
		String nextCluster = null;
		for (int i = 1; i < dataSrcs.length; i++) {
			nextCluster = dataSrcs[i].getClustername();
			if (!firstCluster.equals(nextCluster)) {
				return false;
			}
		} // for
		return true;
	} // isSameCluster

} // class
