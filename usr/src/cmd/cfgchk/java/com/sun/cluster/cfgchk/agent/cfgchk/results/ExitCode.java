/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)ExitCode.java	1.2	08/05/29 SMI"
 */
package com.sun.cluster.cfgchk.agent.cfgchk.results;

/**
 * This interface contains the definition of exit codes.
 * These codes are bumped up to the 100 range
 * to avoid collisions with lower range codes potentially encountered
 * in the ksh wrapper. ksh will readjust the ExitCode codes to the new
 * cli values defined in cfgchk_lib.
 */
public interface ExitCode {

	/*
	 * must avoid cacaoadm return codes
	 * reference:
	 * /usr/lib/cacao/lib/tools/scripts/error_codes.cfg:
	 *
	 *
	 * CACAO_BAD_OWNER=200
	 * CACAO_BAD_FILE_RIGHTS=201
	 * CACAO_CANNOT_ACCESS_FILE=202
	 *
	 * #
	 * # cacaoadm exit codes macros
	 * #
	 *
	 * CACAO_CR_SUCCESS=0
	 * CACAO_CR_ERROR=1
	 *
	 * # invalid command or command line syntax error
	 * CACAO_CR_EINVALCOMMAND=2
	 *
	 * #if cacao is not started and the command fails
	 * CACAO_CR_ESRCH=3
	 *
	 * #Cacao is started, but cannot connect to cacao
	 * CACAO_CR_EAGAIN=11
	 *
	 * #user is not authorized to perform the command
	 * CACAO_CR_EACCES=13
	 * #value 1 is already used by CACAO_CR_ERROR
	 * #align EPERM on EACCESS
	 * CACAO_CR_EPERM=13
	 *
	 *
	 * # Cacao is already running, if for example you start 2
	 * # instances of the same cacao
	 * CACAO_CR_EEXIST=17
	 *
	 * # invalid argument
	 * CACAO_CR_EINVAL=22
	 *
	 *
	 * # in case of a command that has no meaning for an
	 * # embedded instances (start/stop/restart/disable/enable)
	 * CACAO_CR_EMBEDDED=1
	 */


	/*
	 * must avoid error codes returned when cacao can't run correctly:
	 * these values seen in ksh wrapper
	 *
	 * # exit 5 if cfgchk module not registered or similar issue
	 * #      127 if can't find cacaocsc
	 * #	  192 if cacao not running
	 * #      196: cacao_launcher[23299]: SUNWcacaort launcher:
	 *		Common Agent Container exited abnormaly
	 * #		got this case when cfgchk.jar changed
	 *		under running cacao
	 * #
	 */


	/*
	 * These next values are documented for user programs to
	 * test from EXITCODE_FILE created in output directory.
	 * See the ksh wrapper for more detail.
	 */

	// values 100 through 109 reserved for success
	// 101 thru 109 generated dynamically depending
	// on run results
	public static final int OK             =   100;

	/*
	 * values 120 and up to 126 (maximum) are internal error codes
	 */
	public static final int ERR_ERR        =   120;
		// used as a non-err/err threshhold
	public static final int ERR_BUSY       =   121;
		// run already in progress
	public static final int ERR_CACAO      =   122;
		// internal cacao cmx err
		// typically jmx problem or target mbean not avail

	public static final int ERR_ARG        =   124; // bad args fed in
	public static final int ERR_INTERNAL   =   125; // otherwise uncaught
	public static final int ERR_INTR       =   126; // interrupted

} // interface
