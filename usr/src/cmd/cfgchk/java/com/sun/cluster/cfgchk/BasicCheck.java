/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)BasicCheck.java	1.2	08/05/29 SMI"
 */
package com.sun.cluster.cfgchk;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.logging.Logger;

import com.sun.cluster.cfgchk.datasrc.ClusterDataSrc;
import com.sun.cluster.cfgchk.datasrc.DataException;
import com.sun.cluster.cfgchk.engine.Engine;
import com.sun.cluster.cfgchk.utils.CheckUtils;

/**
 *
 * Parent of all Checks; sufficient as parent of a single-node check
 *
 * See also BasicCheckMultiNode.java
 */

// No I18n

public abstract class BasicCheck implements Check {

	// "unassigned" members should be assigned in constructor of subclass

	protected String checkID = "unassigned";
	protected String problemStatement = "unassigned";
	protected int severity = Check.UNKNOWN_SEV;
	protected int status = Check.UNKNOWN_STATUS;
	protected ArrayList keywords = null;

	/*
	 * version and revisionDate are actually SCCS keywords
	 * (1.2 and 09/12/08) which must
	 * be defined in each Check instance since checking that file in
	 * and out will expand it
	 */

	protected String version = "unassigned";
	protected String revisionDate = "unassigned";

	protected String checkLogic = "unassigned";
	protected String applicabilityLogic = "unassigned";
	protected StringBuffer infoMsg = null;
	protected String extraMessage = null;
	protected String insufficientDataMsg = null;

	protected Logger logger = null;
	protected Engine engine = null;
	protected boolean usingCacao = true;
	protected Properties auxData;

	// convenience pointers; set in setEngine()
	private String nodename = null;

	private ClusterDataSrc dataSrc = null;
	protected int numInputs = 1;
		// always '1' for this class; may be different in subclasses

	/*
	 * logger, engine, and convenience methods based on engine
	 * are not available until CheckUtils.getCheck() has called
	 * the appropriate set methods on the instantiated Check
	 */
	public BasicCheck() {
		keywords = new ArrayList();
		checkLogic = assignCheckLogic();
		applicabilityLogic = assignApplicabilityLogic();
	} // ()


	protected abstract String assignApplicabilityLogic();
	protected abstract String assignCheckLogic();

	// =====     start interface Check     =====

	public boolean isMultiNodeCheck() {
		return false;
	}

	public String getID() {
		return checkID;
	} // getID

	public String getProblemStatement() {
		return problemStatement;
	} // getProblemStatement

	public String getSynopsis() {
		return problemStatement;
	} // getSynopsis

	public int getSeverity() {
		return severity;
	} // getSeverity

	public int getStatus() {
		return status;
	} // getStatus

	public void setStatus(int status) {
		this.status = status;
	} // setStatus

	public abstract String getAnalysis();
	public abstract String getRecommendations();

	public List getKeywordsList() {
		return keywords;
	} // getKeywordsList

	// String of comma separated keywords
	public String getKeywords() {
		StringBuffer sb = new StringBuffer("");
		Iterator it = keywords.iterator();
		while (it.hasNext()) {
			if (sb.length() > 0) sb.append(", ");
			sb.append((String)it.next());
		}
		return sb.toString();
	} // getKeywords

	public String getVersion() {
		return version;
	} // getVersion

	public String getRevisionDate() {
		return revisionDate;
	}

	public abstract boolean isApplicable() throws CheckExecutionException;


	public String getApplicabilityLogic() {
		return applicabilityLogic;
	} // getApplicabilityLogic

	public abstract boolean doCheck() throws CheckExecutionException;

	public String getCheckLogic() {
		return checkLogic;
	} // getCheckLogic

	public String getInfoMessage() {
		logger.info("infoMsg.toString(): >>" + infoMsg + "<<");
		if (infoMsg != null) {
			return infoMsg.toString();
		} else {
			logger.info("infoMsg: null");
			return null;
		}
	} // getInfoMessage

	/**
	 * Special case of extra message: indicates that parser
	 * was unable to obtain data from data source
	 */
	public String getInsufficientDataMsg() {
		return insufficientDataMsg;
	}

	public void setInsufficientDataMsg(String str) {
		logger.info("str: " + str);
		if (str == null || str.length() == 0) {
			str = " --empty string-- ";
		}
		insufficientDataMsg = str;
	} // setInsufficientDataMsg

	public String getCheckedNodenames() {
		return getNodename();
	}

	/**
	 * @return status of check result as printable string
	 */
	public String getStatusName() {
		return Check.STATUS_NAMES[getStatus()];
	} // getStatus

	/**
	 * @return severity of check result as printable string
	 */
	public String getSeverityName() {
		return Check.SEVERITY_NAMES[getSeverity()];
	} // getSeverityName


	/*
	 * @return most severe of two input severities
	 * see interface Check.java
	 */
	public int maxSeverity(int severity1, int severity2) {
		return CheckUtils.mostSevere(severity1, severity2);
	} // maxSeverity

	public String toString() {
		String SEP = " | ";
		StringBuffer sb = new StringBuffer(getID());
		sb.append(SEP + getProblemStatement());
		sb.append(SEP + getSeverityName());
		sb.append(SEP + getStatusName());
		sb.append(SEP + getKeywords());
		sb.append(SEP + getCheckedNodenames());
		return sb.toString();

	} // toString


	// =====     end interface Check     =====


	/**
	 * assign references to Engine and DataSrc[n]
	 * setup nodename via nth DataSrc
	 * assign convenience references via Engine
	 * @param engine The Engine instance that will run this Check
	 * @param logger The Logger in use
	 */
	public void init(Engine engine, int n, boolean usingCacao,
	    Logger logger)
		throws DataException, FileNotFoundException, IOException {

		this.logger = logger;
		logger.info("BasicCheck.init()  -- ENTER --");
		this.engine = engine;
		this.usingCacao = usingCacao;

		dataSrc = engine.getDataSrc(n);
		auxData = engine.getAuxData();

		nodename = dataSrc.getSystemName();
		logger.info("BasicCheck.init() got nodename: " + nodename);

	} // init


	// =====     convenience methods:     =====


	public String getNodename() {
		return nodename;
	}

	public ClusterDataSrc getDataSrc() {
		return dataSrc;
	}

	public int getNumInputs() {
		return numInputs;
	}

	public String getAuxDataValue(String key) {
		return auxData.getProperty(key);
	} // getAuxDataValue

	protected void appendInfoMsg(String str) {
		logger.info("appendInfoMsg: >>" + str + "<<");
		if (infoMsg == null) {
			infoMsg = new StringBuffer();
		} else {
			logger.info("appendInfoMsg: appending newline");
			infoMsg.append("\n");
		}
		infoMsg.append(str);
	} // appendInfoMsg


} // class

