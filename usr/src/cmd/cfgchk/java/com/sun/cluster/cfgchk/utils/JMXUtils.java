/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)JMXUtils.java	1.2	08/05/29 SMI"
 */
package com.sun.cluster.cfgchk.utils;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

//JMX
import javax.management.MBeanServerConnection;
// JMX-remote
import javax.management.remote.JMXConnector;

// cacao
import com.sun.cacao.agent.JmxClient;
import com.sun.cluster.cfgchk.common.CfgchkRuntimeException;

public class JMXUtils {

	// verify all logger stmnts: might be null: s/b a static in logger class that handles null logger arg case XXX

	// the timeouts are strings that will be put into the environment
	private static String jmxTimeout = new String("" + (120 * 1000));
	// 120 seconds (2 minutes) converted to ms
	private static String defaultCheckTimeout = "2000"; // ms
	private static int CONN_TIMEOUT_FACTOR = 2000;

	//
	// MBean related routines
	//

	public static void closeJMXConnection(Logger logger,
	    JMXConnector jmxc) {
		if (jmxc != null) {
			String connectionId = null;
			try {
				connectionId = jmxc.getConnectionId();
				jmxc.close();
				if (logger != null) {
					logger.fine("Connection " +
					    connectionId + " closed");
				}

				jmxc = null;

			} catch (Exception ex) {
				if (logger != null) {
					logger.warning(
						"Unable to close JMX " +
							"connection: " +
							ex.toString());
				}
				if (logger != null) {
					logger.warning("Unable to close jmx" +
					    " connection " + connectionId);
				}
				// best effort was good enough
			}
		}
	} // closeJMXConnection

	public static JMXConnector getJMXConnection(Logger logger,
	    String nodename) {
		JMXConnector jmxc = null;

		if (logger != null) logger.finest("nodename = " + nodename);

		try {
			jmxc = internalGetJMXConnection(logger, nodename);

		} catch (Exception ex) {
			if (logger != null) {
				logger.severe("Unable to get JMX connection: " +
				    ex.toString());
			}
			// return null
		}

		return jmxc;
	} // getJMXConnection


	public static MBeanServerConnection getMBeanServerConnection(
		Logger logger, JMXConnector jmxc) {
		MBeanServerConnection mbsc = null;
		try {
			mbsc = jmxc.getMBeanServerConnection();
		} catch (Exception e) {
			if (logger != null) {
				logger.severe("Unable to get mbeanserver: " +
				    e.toString()); }
			// return null
		}
		return mbsc;
	} // getMBeanServerConnection


    // ==========================
    // internalGetJMXConnection
    // ==========================
    /**
     * Helper function  : open a JMX connection
     *
     */
    public static JMXConnector internalGetJMXConnection(Logger logger,
	String ipAddress)
		throws CfgchkRuntimeException
    {
	CfgchkRuntimeException exc = null;
        String timeout = jmxTimeout;
        JMXConnector jmxConn = null;

	// Introducing this inner class as a workaround for the
	// JMX timeout problem. If a JMX connection is attempted when
	// the remote cluster is rebooting the connection is blocked till
	// the remote cluster comes back online.
	// Adding this workaround, where the connection is established on
	// a dedicated thread and the parent thread waits for the timeout
	// Refer bugid: 6184584, 6185609, 6185148, 6194770

	class timeoutConnection extends Thread {
	    Logger logger = null;
	    String ip_addr = null;
	    String connTimeout = null;
	    JMXConnector jmxconn = null;
	    String statusMsg = null;
	    String errorCode = null;
	    boolean connValid = true;

	    public timeoutConnection(Logger logger, String ipAddress,
		String timeout) {
		this.logger = logger;
		this.ip_addr = ipAddress;
		this.connTimeout = timeout;
	    }

	    public JMXConnector getJmxConn() {
		return this.jmxconn;
	    }

	    public String getStatus() {
		return statusMsg;
	    }

	    public String getErrorCode() {
		return errorCode;
	    }

	    public void setValidFlag(boolean flag) {
		connValid = flag;
	    }

	    public void run() {
		try {
		    HashMap env = new HashMap();

            	    env.put("jmx.remote.x.request.timeout",
			this.connTimeout);
            	    env.put("jmx.remote.x.client.connection.check.period",
                   	 defaultCheckTimeout);

		    // add current module class loader as the class to use
		    // for unpacking result of remote calls.
		    ClassLoader cl = this.getClass().getClassLoader();
		    env.put("jmx.remote.default.class.loader", cl);
		    // logger.info("got classloader: " + cl.toString());

		    // deprecated: supposed to use rmi-based
		    // instead of jmxmp protocol
		    // we obey or not?? XXX
            	    this.jmxconn = JmxClient.getJmxClientConnection(
			    this.ip_addr, (Map)env);

		    if ((isInterrupted()) || (connValid == false)) {
			this.jmxconn.close();
			this.jmxconn = null;
		    }
		    if (logger != null) {
			    logger.fine("Connection OK with " + this.ip_addr +
				" connectionID is " +
				jmxconn.getConnectionId() +
				" request timeout value is " +
				this.connTimeout);
		    }

		} catch (java.net.ConnectException ce) {
			if (logger != null) {
				logger.info("Impossible to connect to " +
				    this.ip_addr);
			}
            	    this.jmxconn = null;
		    this.statusMsg = new String(this.ip_addr);
		    this.errorCode = new String("ConnectException");

        	} catch (java.lang.IllegalArgumentException ei) {
		    this.jmxconn = null;
		    this.statusMsg = new String(ei.toString());
		    this.errorCode = new String("IllegalArgumentException");

		} catch (SecurityException excep) {
		    this.jmxconn = null;
		    this.statusMsg = new String(this.ip_addr);
		    this.errorCode = new String("SecurityException");

		} catch (Exception ei) {
		    this.jmxconn = null;
		    this.statusMsg = new String(this.ip_addr);
		    this.errorCode = new String("Exception");
        	}

	    }
	} // inner class

	timeoutConnection thread = new timeoutConnection(logger, ipAddress,
	    timeout);
	thread.start();
	int joinTimeout = Integer.parseInt(timeout);
	try {
	    // wait for more than timeout, to give the JMX connection
	    // a chance to complete
	    thread.join(joinTimeout + CONN_TIMEOUT_FACTOR);

	} catch (java.lang.InterruptedException ex) {
		if (logger != null) { logger.warning("Failed to connect - " +
			    "interrupted");
		}
	    thread.setValidFlag(false);
	    return null;
	}

	if (thread.isAlive()) {
	    if (logger != null) logger.warning("Failed to connect - timeout");
	    thread.setValidFlag(false);
	    thread.interrupt();
	    return null;
	}

	String error = thread.getErrorCode();
	String status = thread.getStatus();
	if (error != null) {
            exc = new CfgchkRuntimeException(error + ": " + status);
            if (logger != null) logger.info(status);
	    throw exc;

	}
	jmxConn = thread.getJmxConn();
	return (jmxConn);
    } // internalGetJMXConnection


} // class

