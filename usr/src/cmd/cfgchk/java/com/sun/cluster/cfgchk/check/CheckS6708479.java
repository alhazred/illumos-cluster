/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)CheckS6708479.java	1.2	08/06/02 SMI"
 */
package com.sun.cluster.cfgchk.check;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.StringTokenizer;

import com.sun.cluster.cfgchk.Check;
import com.sun.cluster.cfgchk.BasicCheck;
import com.sun.cluster.cfgchk.CheckExecutionException;


import com.sun.cluster.cfgchk.datasrc.fileObject.TextFile;

import com.sun.cluster.cfgchk.utils.logger.LoggerUtils;

/* BEGIN JSTYLED */
/**
 * CheckS6708479 - (critical)
 *
 * "The /etc/system rpcmod:svc_default_stksize parameter is missing or has an incorrect value for Sun Cluster"
 */
/* END JSTYLED */

/*
 * No I18n of any strings in this class.
 * Exceptions thrown from Checks are caught and handled by Engine
 * typically just by logging. Messages are never displayed to user.
 *
 * Lines with trailing 'TOC' comment are auto-gathered into
 * table of contents listing of all checks.
 */

public class CheckS6708479 extends BasicCheck {

    private boolean missing = false;
    private boolean misconfigured = false;

	public CheckS6708479() {
		checkID = "S6708479"; // TOC
		severity = Check.CRITICAL; // TOC
		// sccs keyword: must be 1.2 when checked out under sccs
		version = "1.2";
		// sccs keyword: must be 08/06/02 when checked out under sccs
		revisionDate = "09/12/08";

		keywords.add("SunCluster3");
		keywords.add("single");
		keywords.add("/etc/system");
		keywords.add("svc_default_stksize");

		/* BEGIN JSTYLED */
		problemStatement = "The /etc/system rpcmod:svc_default_stksize parameter is missing or has an incorrect value for Sun Cluster."; // TOC
		/* END JSTYLED */
    } // ()

    /**
     * @return a string describing the logic of the applicability test
     */
    protected String assignApplicabilityLogic() {
	    String aLogic = "Always applies to Sun Cluster nodes.";
	    return aLogic;
    } // assignApplicabilityLogic


    /**
     * @return a string describing the logic of the check
     */
	protected String assignCheckLogic() {
		/* BEGIN JSTYLED */
		String cLogic = "Search /etc/system for the \"rpcmod:svc_default_stksize\" entry. If the entry is missing or the value is less than 0x6000, this check is violated.";
		/* END JSTYLED */
		return cLogic;
	} // assignCheckLogic


	public String getAnalysis() {
		/* BEGIN JSTYLED */
		// default shows in complete run xml when check passes
		String keyphrase = "missing or misconfigured for Sun Cluster";

		// never both
		if (missing) { keyphrase = "is missing"; }
		if (misconfigured) { keyphrase = "has an incorrect value for Sun Cluster"; }

		String analysis = "The /etc/system rpcmod:svc_default_stksize parameter " + keyphrase + ". This affects the stack size used by many Sun Cluster kernel threads. If the kernel stack size is too small, stack overflows might occur which could result in system panics or drops to the OBP prompt.";
		/* END JSTYLED */

		return analysis;
	} // getAnalysis

	public String getRecommendations() {
		/* BEGIN JSTYLED */
		String recommend = "Set the rpcmod:svc_default_stksize parameter in /etc/system to 0x6000 ('set rpcmod:svc_default_stksize=0x6000').";
		/* END JSTYLED */

        return recommend;
    } // getRecommendations

	// always applicable
    public boolean isApplicable() throws CheckExecutionException {
        logger.info("isApplicable() -- ENTER --");
        boolean applicable = true;
        return applicable;
    } // isApplicable

	public boolean doCheck() throws CheckExecutionException {

		logger.info("-- ENTER --");
		boolean passes = false;
		missing = false;
		misconfigured = false;

		try {
			/* BEGIN JSTYLED */
			/*
			 * Determine the stksize value (set rpcmod:svc_default_stksize) in
			 * /etc/system.
			 * If "set rpcmod:svc_default_stksize" is missing from /etc/system,
			 * this check fails.
			 * OR
			 * If the svc_default_stksize value is < 0x6000, this check fails.
			 *
			 * Automation should list if the stksize parameter is missing or set
			 * incorrectly.
			 * Please note that when vxfs is used the requirement is higher!
			 * There will be another check created for that.
			 */
			/* END JSTYLED */
			String fname = "/etc/system";
			logger.info("-- ENTER 1 --");
			TextFile tf = getDataSrc().getTextFile(fname);
			logger.info("-- ENTER 2 --");
			if (tf == null) {
				logger.info("Got null tf: throwing FNFEx");
				throw(new FileNotFoundException(
					       "Null TextFile for " +
						fname));
			} else {
				logger.info("Got non-null tf for " + fname);
			}

			String line = tf.containsString(
				"rpcmod:svc_default_stksize");
			if (line != null) {
				logger.info("line found: " + line);

				// now check for proper contents
				// want two words:
				// "set rpcmod:svc_default_stksize=0x6000"
				// want to parse second word
				StringTokenizer st = new StringTokenizer(line);
				if (st.countTokens() < 2) {
					logger.info(
						"< 2 tokens in line: " + line);
					misconfigured = true;
					passes = false;
				} else {
					/* BEGIN JSTYLED */
					st.nextToken(); // ignore first word
					String tok = st.nextToken();
					logger.info("Considering word: " + tok);
					st = new StringTokenizer(tok, "=");
					if (st.countTokens() < 2) {
						logger.info(
							"< 2 tokens in word: " + tok);
						misconfigured = true;
						passes = false;
					} else {
						st.nextToken();
							// ignore first word
						String tok2 = st.nextToken();
						logger.info("Considering word 2: " + tok2);

						// is value < 0x6000?
						try {
							// an int in /etc/system can
							//  be decimal, octal, or hex
							Integer value = Integer.decode(tok2);
							if (value.intValue() < 0x6000) {
								logger.info(
									"Value too small: " + tok2);
								misconfigured = true;
								passes = false;
							} else {
								logger.info(
									"Value good: " + tok2);
								passes = true;
							}
						} catch (NumberFormatException e) {
							logger.info("Invalid value: " + tok2);
							misconfigured = true;
							passes = false;
						}
					}
					/* END JSTYLED */
				} // got 2+ tokens in word 2
			} else {
				logger.info("line not found; check fails");
				missing = true;
				passes = false;
			}

		} catch (FileNotFoundException e) {
			logger.warning("FileNotFoundException: " +
			    e.getMessage());
			setInsufficientDataMsg(e.getMessage());
			passes = false;
		} catch (IOException e) {
			logger.warning("IOException: " + e.getMessage());
			LoggerUtils.exception("IOException", e, logger);
			throw new CheckExecutionException(e);
		} catch (Exception e) {
			logger.warning("Exception: " + e.getMessage());
			LoggerUtils.exception("Exception", e, logger);
			throw new CheckExecutionException(e);
		}

		logger.info("-- EXIT passes: " + passes);
		return passes;
	} // doCheck


} // class

