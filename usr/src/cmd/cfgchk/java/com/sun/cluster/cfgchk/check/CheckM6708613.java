/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)CheckM6708613.java	1.2	08/06/02 SMI"
 */
package com.sun.cluster.cfgchk.check;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.sun.cluster.cfgchk.BasicCheckMultiNode;
import com.sun.cluster.cfgchk.Check;
import com.sun.cluster.cfgchk.CheckExecutionException;
import com.sun.cluster.cfgchk.datasrc.DataException;
import com.sun.cluster.cfgchk.datasrc.InsufficientDataException;
import com.sun.cluster.cfgchk.datasrc.fileObject.TextFile;

/**
 *
 * 6708613 - 1247 (Critical)
 *
 * Multi node check.
 *
 * vxio major numbers are not consistent across all Sun Cluster nodes.
 *
 */

/*
 * No I18n of any strings in this class.
 * Exceptions thrown from Checks are caught and handled by Engine
 * typically just by logging. Messages are never displayed to user.
 *
 * Lines with trailing 'TOC' comment are auto-gathered into
 * table of contents listing of all checks.
 */

public class CheckM6708613 extends BasicCheckMultiNode {

	private ArrayList mismatchedEntries = null;

	public CheckM6708613() {
		checkID = "M6708613"; // TOC
		severity = Check.CRITICAL; // TOC

		// sccs keyword: must be 1.2 when checked out under sccs
		version = "1.2";
		// sccs keyword: must be 08/06/02 when checked out under sccs
		revisionDate = "09/12/08";

		keywords.add("SunCluster3");
		keywords.add("multi");
		keywords.add("vxvm");

		/* BEGIN JSTYLED */
		problemStatement = "vxio major numbers are not consistent across all Sun Cluster nodes."; // TOC
		/* END JSTYLED */

		// check-specific data
		mismatchedEntries = new ArrayList();
	} // ()

	/**
	 * @return a string describing the logic of the applicability test
	 */
	protected String assignApplicabilityLogic() {
		String aLogic = "Applicable only if VxVm (VRTSvxvm) is installed.";
		return aLogic;
	} // assignApplicabilityLogic


	/**
	 * @return a string describing the logic of the check
	 */
	protected String assignCheckLogic() {
		/* BEGIN JSTYLED */
		String cLogic = "Read vxio major number from /etc/name_to_major on each node. If the number differs among nodes, the check is violated.";
		/* END JSTYLED */
		return cLogic;
	} // assignCheckLogic


	public String getAnalysis() {

		/*
		 * List nodes and vxio numbers
		 */

		StringBuffer analysis = new StringBuffer("");

		if (mismatchedEntries != null && mismatchedEntries.size() > 0) {
			/* BEGIN JSTYLED */
			analysis.append("All of the nodes in this cluster do not have the same vxio major number. If the major numbers for the vxio pseudo-device are not identical on all nodes in a cluster, problems ranging from the importation and deportation of disk groups to data corruption of volumes can result.\n");
			/* END JSTYLED */

			Iterator it = mismatchedEntries.iterator();
			String data = null;
			while (it.hasNext()) {
				data = (String)it.next();
				analysis.append("\t\t" + data + "\n");
			}
		}

		return analysis.toString();
	} // getAnalysis

	public String getRecommendations() {
		/* BEGIN JSTYLED */
		String recommend = "Make the vxio major number in /etc/name_to_major identical on each node.  When adjusting the entry, make sure that you are not using a major number assigned to another device.";
		/* END JSTYLED */

		return recommend;
	} // getRecommendations

	/*
	 * requires at least two inputs
	 * VxVm must be installed on at least one node
	 * VxVm not installed on all nodes will be evident
	 * in doCheck() where some nodes will not have
	 * vxio entries; will be reported as 'missing'
	 */
	public boolean isApplicable() throws CheckExecutionException {
		logger.info("-- ENTER -- ");
		boolean applicable = super.isApplicable(); // multi-inputs?
		logger.info("test multiple data sources: " + applicable);
		try {
			if (applicable) {
				try {
					for (int i = 0;
					     i < getNumInputs();
					     i++) {
						/* BEGIN JSTYLED */
						if (getDataSrc(i).isPackageInstalled("VRTSvxvm")) {
							applicable = true;
							break;
						}
						logger.info("package not found on any node: not applicable");
						/* END JSTYLED */
						applicable = false;
					} // for each node
				} catch (InsufficientDataException e) {
					logger.warning(
						"InsufficientDataException(): "
							+ e.getMessage());
					setInsufficientDataMsg(e.getMessage());
				} catch (DataException dex) {
					logger.warning("DataException: " + dex);
					throw new CheckExecutionException(dex);
				}
			}
		} catch (Exception e) {
			logger.warning("Exception: " + e);
			throw new CheckExecutionException(e);
		}
		logger.info("-- EXIT --: " + applicable);
		return applicable;
	} // isApplicable

	/*
	 * The present impl doubles the reports on differing entries: is OK
	 * Missing entries listed only once: is OK
	 */
	public boolean doCheck() throws CheckExecutionException {

		logger.info("-- ENTER --");
		boolean passes = true;
		try {

			/*
			 * create array of vxio major numbers
			 * if any differ the check fails
			 *
			 * if vxvm not installed on a node XXX
			 */

			logger.info("getNumInputs(): " + getNumInputs());

			// load up array of vxio numbers
			String[] vxioMajors = new String[getNumInputs()];
			for (int i = 0; i < getNumInputs(); i++) {
				String nodename = getNodename(i);
				try {
					logger.info("testing " + nodename +
					    " (datasrc " + i + ")");
					String fname = "/etc/name_to_major";
					TextFile tf =
					    getDataSrc(i).getTextFile(fname);
					if (tf == null) {
						/* BEGIN JSTYLED */
						logger.info("Got null tf: throwing FNFEx");
						throw (new FileNotFoundException("Null TextFile for " + fname));
						/* END JSTYLED */
					}
					logger.info("Got " + fname +
					    " as TextFile");
					/* BEGIN JSTYLED */
					/*
					 * Pattern:
					 * match a line that starts with (^) vxio'
					 * (not captured (starts with '?:'))
					 * followed by at least one whitespace char
					 * followed by one or more characters to EOL
					 * (saved as a capture group for reuse)
					 * We're looking for characters rather than digits
					 * in order to report *all* lines that start w/ 'vxio '
					 */
					/* END JSTYLED */
					Pattern pattern =
					    TextFile.compilePattern(
						    "(?:^vxio)(?:\\s+)(.+$)");

					// examine lines in /etc/name_to_major
					// for pattern
					// store every line in mismatchedEntries
					// mismatchedEntries will be used only
					// if a mismatch is found.
					String[] lines = tf.getLines();
					String vxioNum = "missing";
					for (int j = 0; j < lines.length; j++) {
						logger.finest("lines["+j+"]: >"
						    + lines[j] + "<");
						Matcher matcher =
						    TextFile.matches(lines[j],
							pattern);
						if (matcher != null) {
							logger.fine("match!!");
							vxioNum =
							    matcher.group(0);
							break;
						}
					} // for lines
					logger.info("vxioNum: " + vxioNum);
					vxioMajors[i] = vxioNum;

					// save in case we need it later
					mismatchedEntries.add(nodename +
					    ":\t" + vxioNum);

				} catch (FileNotFoundException e) {
					logger.info(
						"FileNotFoundException(): " +
							e.getMessage());
					throw new CheckExecutionException(e);
				} catch (IOException e) {
					logger.info("IOException(): " +
					    e.getMessage());
					throw new CheckExecutionException(e);
				}  catch (DataException e) {
					logger.info("DataException(): " +
					    e.getMessage());
					throw new CheckExecutionException(e);
				}
			} // for each node

			// evaluate for mismatches
			String target = vxioMajors[0];
			for (int i = 1; i < vxioMajors.length; i++) {
				if (!target.equals(vxioMajors[i])) {
					passes = false;
					logger.info("mismatch: >" + target +
					    "< vs >" + vxioMajors[i] + "<");
					break;
				}
				logger.info("no mismatches");
				mismatchedEntries = null;
			} // vxioMajors


		} catch (Exception e) {
			logger.info("Exception: " + e.getMessage());
			throw new CheckExecutionException(e);
		}

		logger.info("passes: " + passes);
		return passes;
	} // doCheck


} // class

