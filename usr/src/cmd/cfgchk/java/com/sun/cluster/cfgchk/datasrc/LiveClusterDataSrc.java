/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)LiveClusterDataSrc.java	1.3	08/06/02 SMI"
 */
package com.sun.cluster.cfgchk.datasrc;

import java.io.IOException;
import java.util.StringTokenizer;

import javax.management.ObjectName;
import com.sun.cacao.ObjectNameFactory;

import com.sun.cluster.cfgchk.datasrc.fileObject.TextFile;
import com.sun.cluster.cfgchk.i18n.I18n;
import com.sun.cluster.cfgchk.utils.ClusterUtils;
import com.sun.cluster.cfgchk.utils.Utils;
import com.sun.cluster.cfgchk.utils.StringUtils;


/**
 *
 * This is an implementation of interface ClusterDataSrc which knows
 * how to access to system files and commands on a live
 * Sun Cluster node..
 *
 * Code executed here is always executed on localhost but
 * is not always the originating node.
 * Data is made available transparently to checks via
 * mbean proxy.
 *
 * Data cache in superclass.
 */
public class LiveClusterDataSrc extends LiveSolarisDataSrc
	implements ClusterDataSrc, LiveClusterDataSrcMBean {

	private ObjectNameFactory onf = null;

	// Part of the contract with the ObjectNameFactory
	final public static String TYPE = "LiveClusterDataSrc";


	// access to scha commands for data query
	private static String CL_BIN = ClusterUtils.CL_BIN;
	private static String SCHA_CL_GET = ClusterUtils.SCHA_CL_GET;
	private static String SCHA_RT_GET = ClusterUtils.SCHA_RT_GET;
	private static String SCHA_RG_GET = ClusterUtils.SCHA_RG_GET;
	private static String SCHA_RS_GET = ClusterUtils.SCHA_RS_GET;




	// constructor for use as mbean
	public LiveClusterDataSrc(ObjectNameFactory onf, String systemName) {
		super(onf, systemName);
		this.onf = onf;
	} // ()

	// used by localhost & non-cacao
	public LiveClusterDataSrc(String systemName) {
		super(systemName);
		this.dsName = systemName;
		logger.info("dsName: " + dsName);
	} // ()


	// ===  start interface ClusterDataSrc  ===

	public String getNodeID() throws DataException {
		String nodeid = null;
		String cacheKey = "getNodeID";
		logger.info(dsName + ": -- ENTER -- " + cacheKey);

		try {
			nodeid = dataCache.getString(cacheKey);
			if (nodeid == null) {
				logger.info(dsName + ": not found in cache");
				try {
					String fullPath = "/etc/cluster/nodeid";
					logger.info(dsName + ": fullPath: " +
					    fullPath);
					TextFile tf = new TextFile(fullPath);
						// FNFEx, IOEx

					// assumed to be a single line
					nodeid = tf.getLines()[0];
					dataCache.put(cacheKey, nodeid);
					logger.info(dsName +
					    ": String added to cache");
				} catch (IOException ioex) {
					String[] i18nArgs =
					    { ioex.getMessage() };
					/* BEGIN JSTYLED */
					String lMsg = I18n.getLocalized(logger,
						"internal.error.cant.read.systemfile",
						i18nArgs);
					/* END JSTYLED */
					throw new DataException(lMsg);
				}
			} // == null
		} catch (DataException dex) { // already I18n; bubble it up
			logger.warning(dsName + ": DataException: " +
			    dex.getMessage());
			throw dex;
		}
		logger.info(dsName + ": nodeid: " + nodeid);
		return nodeid;
	} // getNodeID

	public String getClustername() throws DataException {
		String clusterName = null;
		String cacheKey = "getClustername";
		logger.info(dsName + ": -- ENTER -- " + cacheKey);

		try {
			clusterName = dataCache.getString(cacheKey);
			if (clusterName == null) {
				logger.info(dsName + ": not found in cache");
				try {
					clusterName =
					    ClusterUtils.getClusterName(logger);
					dataCache.put(cacheKey, clusterName);
					logger.info(dsName +
					    ": String added to cache");

				} catch (DataException dex) {
					logger.info(dsName +
					    ": DataException: " + dex);
					String[] i18nArgs =
					    { dex.getMessage() };
					String lMsg = I18n.getLocalized(logger,
					    "internal.error.cmd.returns.error",
					    i18nArgs);
					throw new DataException(lMsg);
				}
			} // == null
		} catch (DataException dex) { // already I18n; bubble it up
			logger.warning(dsName + ": DataException: " +
			    dex.getMessage());
			throw dex;
		}
		logger.info(dsName + ": cluster name: " + clusterName);
		return clusterName;
	} // getClustername

	public String getClusterVersion() throws DataException {
		String version = null;
		String cacheKey = "getClusterVersion";
		logger.info(dsName + ": -- ENTER -- " + cacheKey);

		try {
			version = dataCache.getString(cacheKey);
			if (version == null) {
				logger.info(dsName + ": not found in cache");
				try {
					String fullPath =
					    "/etc/cluster/release";
					logger.info(dsName +
					    ": SC version fullPath: " +
					    fullPath);
					TextFile tf = new TextFile(fullPath);
						// FNFEx, IOEx

					// version is third word from first line
					String line = tf.getLines()[0];
					logger.info(dsName +
					    ": scVersionLine: " + line);
					StringTokenizer st =
					    new StringTokenizer(line);
					if (st.countTokens() < 3) {
						String eMsg =
						    "scVersionLine: " +
						    "insufficient tokens";
							// No I18n
						logger.info(eMsg);
						String[] i18nArgs = { eMsg };
						/* BEGIN JSTYLED */
						String lMsg =
						    I18n.getLocalized(logger,
							"internal.error.cmd.unexpected.result",
							i18nArgs);
						/* END JSTYLED */
						throw new DataException(lMsg);
					}
					st.nextToken(); // skip first word
					st.nextToken(); // skip second word
					version = st.nextToken();
						// third word is version
					dataCache.put(cacheKey, version);
					logger.info(dsName +
					    ": String added to cache");

				} catch (IOException ioex) {
					String[] i18nArgs =
					    { ioex.getMessage() };
					/* BEGIN JSTYLED */
					String lMsg = I18n.getLocalized(logger,
					    "internal.error.cant.read.systemfile",
					    i18nArgs);
					/* END JSTYLED */
throw new DataException(lMsg);
				}

			} // == null
		} catch (DataException dex) { // already I18n; bubble it up
			logger.warning(dsName + ": DataException: " +
			    dex.getMessage());
			throw dex;
		}
		logger.info(dsName + ": cluster version: " + version);
		return version;
	} // getClusterVersion


	/*
	 * isInClusterMode() && isClusterNode() already calc'd in
	 * Params constructor but that covers only the node issuing
	 * the command.
	 *
	 * LiveClusterDataSrc will be instantiated on each node
	 * in nodelist or obtained from mbean server on each node
	 */
	public boolean isClusterMode(boolean usingCacao) throws DataException {
		boolean mode = false;
		String cacheKey = "isClusterMode";
		logger.info(dsName + ": -- ENTER -- " + cacheKey);

		try {
			Boolean b = dataCache.getBoolean(cacheKey);
			if (b == null) {
				logger.info(dsName + ": not found in cache");
				try {
					mode = ClusterUtils.isInClusterMode(
						logger);
					if (mode) {
						dataCache.put(cacheKey,
						    new Boolean(true));
					} else {
						dataCache.put(cacheKey,
						    new Boolean(false));
					}
				} catch (DataException dex) {
					logger.info(dsName +
					    ": DataException: " + dex);
					String[] i18nArgs =
					    { dex.getMessage() };
					String lMsg = I18n.getLocalized(logger,
					    "internal.error.cmd.returns.error",
					    i18nArgs);
					throw new DataException(lMsg);
				}
			} else { // got hit in cache: value can be true or false
				if (b.booleanValue() == true) {
					mode = true;
				}
			}
		} catch (DataException dex) { // already I18n; bubble it up
			logger.warning(dsName + ": DataException: " +
			    dex.getMessage());
			throw dex;
		}
		logger.info(dsName + ": isClusterMode: " + mode);
		return mode;
	} // isClusterMode


	/*
	 *  run scha cmds on localhost
	 */
	public String[] getRGNames() throws DataException {
		String cacheKey = "getRGNames";
		String[] result = null;
		logger.info(dsName + ": -- ENTER -- " + cacheKey);
		try {
			result = dataCache.getStringArray(cacheKey);
			if (result == null) {
				String[] cmd =
				    { SCHA_CL_GET, "-O", "ALL_RESOURCEGROUPS" };
				result = Utils.runCommandStrArray(cmd);
				dataCache.put(cacheKey, result);
				logger.info(dsName +
				    ": StringArray added to cache");
			} // == null
		} catch (DataException dex) { // already I18n; bubble it up
			logger.warning(dsName + ": DataException: " +
			    dex.getMessage());
			throw dex;
		}
		logger.fine(dsName + ": getRGNames: " +
		    StringUtils.stringArrayToString(result, "; "));
		return result;
	} // getRGNames


	public String[] getRGNodelist(String rgName) throws DataException {
		String cacheKey = "getRGNodelist :-: " + rgName;
		String[] result = null;
		logger.info(dsName + ": -- ENTER -- " + cacheKey);
		try {
			result = dataCache.getStringArray(cacheKey);
			if (result == null) {
				result = ClusterUtils.getRGStringParam(rgName,
				    "NODELIST", logger);
				dataCache.put(cacheKey, result);
				logger.info(dsName +
				    ": StringArray added to cache");
			} // == null
		} catch (DataException dex) { // already I18n; bubble it up
			logger.warning(dsName + ": DataException: " +
			    dex.getMessage());
			throw dex;
		}
		logger.fine(dsName + ": getRGNodelist for: " + rgName + ": " +
		    StringUtils.stringArrayToString(result, "; "));
		return result;
	} // getRGNodelist

	// use scha cmd therefore localhost is OK
	public String[] getRSNames(String rgName) throws DataException {
		String cacheKey = "getRSNames :-: " + rgName;
		String[] result = null;
		logger.info(dsName + ": -- ENTER -- " + cacheKey);
		try {
			result = dataCache.getStringArray(cacheKey);
			if (result == null) {
				String[] cmd =
				    { SCHA_RG_GET, "-O", "RESOURCE_LIST",
				      "-G", rgName };
				result = Utils.runCommandStrArray(cmd);
				dataCache.put(cacheKey, result);
				logger.info(dsName +
				    ": StringArray added to cache");
			} // == null
		} catch (DataException dex) { // already I18n; bubble it up
			logger.warning(dsName + ": DataException: " +
			    dex.getMessage());
			throw dex;
		}
		logger.fine(dsName + ": getRSNames for: " + rgName + ": " +
		    StringUtils.stringArrayToString(result, "; "));
		return result;
	} // getRSNames


	// use scha cmd therefore localhost is OK
	public String getRTName(String rsName, String rgName)
		throws DataException {
		String cacheKey = "getRTName :-: " + rgName + "::" + rsName;
		logger.info(dsName + ": -- ENTER -- " + cacheKey);
		String result = null;

		try {
			result = dataCache.getString(cacheKey);
			if (result == null) {
				String[] cmd =
				    { SCHA_RS_GET, "-O", "TYPE",
				      "-G", rgName, "-R", rsName };
				String[] cmdResult =
				    Utils.runCommandStrArray(cmd);

				// a RS has only one RT name
				result = cmdResult[0];
				dataCache.put(cacheKey, result);
				logger.info(dsName + ": String added to cache");
			} // == null
		} catch (DataException dex) { // already I18n; bubble it up
			logger.warning(dsName + ": DataException: " +
			    dex.getMessage());
			throw dex;
		}
		logger.fine(dsName + ": getRTName for: " + rsName +
		    " in: " + rgName + ": " + result);
		return result;
	} // getRTName

	// use scha cmd therefore localhost is OK
	public String[] getRegisteredRTNames() throws DataException {
		String cacheKey = "getRegisteredRTNames";
		String[] result = null;
		logger.info(dsName + ": -- ENTER -- " + cacheKey);
		try {
			result = dataCache.getStringArray(cacheKey);
			if (result == null) {
				String[] cmd =
				    { SCHA_CL_GET, "-O", "ALL_RESOURCETYPES" };
				result = Utils.runCommandStrArray(cmd);
				dataCache.put(cacheKey, result);
				logger.info(dsName +
				    ": StringArray added to cache");
			} // == null
		} catch (DataException dex) { // already I18n; bubble it up
			logger.warning(dsName + ": DataException: " +
			    dex.getMessage());
			throw dex;
		}
		logger.fine(dsName + ": getRegisteredRTNames: " +
		    StringUtils.stringArrayToString(result, "; "));
		return result;
	} // getRegisteredRTNames


	// ===  end interface ClusterDataSrc  ===



	// ===  mbean related methods  ===

	/**
	 * create the object name for the given operation
	 * object name (this method is called when the mbean for
	 * this object is registered with the MBean server).
	 *
	 * @return the objectName to use
	 */
	public ObjectName createON() {
		logger.info(dsName + ": [=LiveClusterDataSrc] createON");

		ObjectName on = onf.getObjectName(LiveClusterDataSrcMBean.class,
		    null);
		return on;
	} // createON

	public void start() {
		logger.fine(dsName + ": starting LiveClusterDataSrc");
	}

	public void stop() {
		logger.fine(dsName + ": stopping LiveClusterDataSrc");
	}

} // class
