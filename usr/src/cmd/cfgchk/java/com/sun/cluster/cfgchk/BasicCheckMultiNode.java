/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)BasicCheckMultiNode.java	1.2	08/05/29 SMI"
 */
package com.sun.cluster.cfgchk;

import java.io.FileNotFoundException;
import java.io.IOException;

import java.util.logging.Logger;

import com.sun.cluster.cfgchk.datasrc.ClusterDataSrc;
import com.sun.cluster.cfgchk.datasrc.DataException;
import com.sun.cluster.cfgchk.engine.Engine;

/**
 * Parent of all MultiNodeChecks
 * overrides and extends BasicCheck
 */

// No I18n

public abstract class BasicCheckMultiNode extends BasicCheck {

	// convenience pointers; set in setEngine()
	private ClusterDataSrc[] dataSrcs = null;
	private String[] nodenames = null;
	private String printableNodenameList = null;

	// logger not available until init() has been called

	public BasicCheckMultiNode() {
		numInputs = 1;
	} // ()

	public boolean isMultiNodeCheck() {
		return true;
	}

	public boolean isApplicable() throws CheckExecutionException {
		boolean applicable = true;
		if (getNumInputs() < 2) {
			applicable = false;
			logger.info("multi node check but only one data src");
		}
		logger.info(" -- EXIT: " + applicable);
		return applicable;
	} // isApplicable

	/**
	 * assign references to Engine and DataSrcs
	 * assign convenience references v
	 * setup nodenames array
	 * @param engine The Engine instance that will run this Check
	 * @param logger The Logger in use
	 */
	public void init(Engine engine, boolean usingCacao, Logger logger)
		throws DataException, FileNotFoundException, IOException {

		this.logger = logger;
		logger.info("-- ENTER; numInputs: " + numInputs);
		this.engine = engine;
		this.usingCacao = usingCacao;

		numInputs = engine.getNumInputs();
		dataSrcs = engine.getDataSrcs();

		nodenames = new String[numInputs];
		StringBuffer sb = new StringBuffer("");

		for (int i = 0; i < numInputs; i++) {
			logger.info("get nodenames["+ i + "]");
			nodenames[i] = dataSrcs[i].getSystemName();
			if (i > 0) {
				sb.append(", " + nodenames[i]);
			} else {
				sb.append(nodenames[i]);
			}
			logger.info("append: >" + nodenames[i] + "<");
		}

		printableNodenameList = sb.toString();
		logger.info("printableNodenameList: " + printableNodenameList);
	} // init

	public String getCheckedNodenames() {
		return printableNodenameList;
	}

	// =====     convenience methods:     =====

	public String getNodename(int i) {
		return nodenames[i];
	}

	public String[] getNodenames() {
		return nodenames;
	}


	// overrides BasicCheck
	public ClusterDataSrc getDataSrc() {
		return dataSrcs[0];
	}

	public ClusterDataSrc getDataSrc(int i) {
		return dataSrcs[i];
	}

	public ClusterDataSrc[] getDataSrcs() {
		return dataSrcs;
	}


} // class
