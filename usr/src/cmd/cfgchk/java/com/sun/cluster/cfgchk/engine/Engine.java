/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */


/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)Engine.java	1.5	08/09/22 SMI"
 */
package com.sun.cluster.cfgchk.engine;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;
import java.util.logging.Logger;

import com.sun.cluster.cfgchk.BasicCheck;
import com.sun.cluster.cfgchk.BasicCheckMultiNode;
import com.sun.cluster.cfgchk.Check;
import com.sun.cluster.cfgchk.CheckExecutionException;
import com.sun.cluster.cfgchk.Globals;
import com.sun.cluster.cfgchk.datasrc.ClusterDataSrc;
import com.sun.cluster.cfgchk.datasrc.DataException;
import com.sun.cluster.cfgchk.i18n.I18n;
import com.sun.cluster.cfgchk.report.XMLResults;
import com.sun.cluster.cfgchk.utils.CheckUtils;
import com.sun.cluster.cfgchk.utils.ChecksCache;
import com.sun.cluster.cfgchk.utils.JarClassLoader;
import com.sun.cluster.cfgchk.utils.Params;
import com.sun.cluster.cfgchk.utils.logger.LoggerUtils;

/**
 * The workhorse of cfgchk.
 * Engine accepts both sources of data and a
 * list of checkID's to process. Cfgchk may be run against live solaris
 * systems (usualy, but not always cluster nodes) or explorer
 * archives. On a live system the DataSrc knows how to fetch data from,
 * and run commands on, the live system. A DataSrc in the Explorer family
 * knows how to fetch info from the archive. Data is made available to
 * Checks uniformly.
 *
 * Engine is first setup then run() is called by the EngineListener.
 *
 * During the Engine run (as a Thread) it emites messages to an
 * EngineListener (Cfgchk) for optional display to the user. Also during
 * the run, results are tallied in preparation for reports to be
 * produced.
 */
public class Engine implements Runnable {
	private Logger logger = null; // user-visible
	private EngineListener engineListener = null;
	private ClusterDataSrc[] dataSrcs = null;
	private ChecksCache[] checksCache = null;
	private JarClassLoader jarClassLoader = null;
	private Params params = null;

	// auxillary data used by checks: not compiled in; not fetched 'live'
	private Properties auxData = null;

	// convenience for single node checks: first dataSrc from array
	private ClusterDataSrc singleDataSrc = null;

	private int numInputs = 0;
	private int maxViolatedSeverity = 0; // our "return code"
	private String[] checkIDs = null;
	private XMLResults results = null;

	private String engineName = null; // used only for toString()
	private String dsNameList = null;
	private String hostnameList = null;
	private String currentHostnameList = null;
    private boolean usingCacao = true;

	public Engine(Logger logger, EngineListener el, List dataSrcsList,
	    boolean usingCacao, String[] checkIDs, Params params,
	    ChecksCache[] checksCache, JarClassLoader jarClassLoader,
            String logPathname) throws DataException {
		engineName = "Engine created " + new Date().toString();
		logger.info("Engine() -- ENTER -- " + engineName);

		this.logger = logger;
		this.engineListener = el;

		this.usingCacao = usingCacao;
		this.checkIDs = checkIDs;
		this.params = params;

		this.checksCache = checksCache;
		this.jarClassLoader = jarClassLoader;

		String reportsDir = params.outputDir;
		logger.info("Engine() reportsDir: " + reportsDir);

		hostnameList = null; // can be one or more once filled in
		dsNameList   = null; // can be one or more once filled in
		StringBuffer hnlistsbuf = new StringBuffer("");
			// tmp for filling in nodelist
		StringBuffer dsnlistsbuf = new StringBuffer("");
		String lMsg = null; // for I18n fetch

		// convert data sources to typed array for later convenience
		numInputs = dataSrcsList.size();
		logger.info("num data srcs: " + numInputs);
		String[] nodenamesArry = new String[numInputs + 1];
			// incl multi
		String[] dsNamesArry   = new String[numInputs + 1];
			// incl multi

		logger.info("Engine() start load data sources array ");
		dataSrcs = new ClusterDataSrc[numInputs];
		try {
			for (int i = 0; i < numInputs; i++) {
				logger.info("load data src["+i+"]");
				dataSrcs[i] = (ClusterDataSrc)dataSrcsList.
				    get(i);
				logger.info("got dataSrcs["+i+"]: " +
				    dataSrcs[i].getDataSrcName());
				String nodename = dataSrcs[i].getSystemName();
					// ex I18n
				String dsName = dataSrcs[i].getDataSrcName();

				// build hostname list & array
				dsNamesArry[i] = dsName;
				nodenamesArry[i] = nodename;
				if (i > 0) {
					hnlistsbuf.append(", " + nodename);
				} else {
					hnlistsbuf.append(nodename);
				}
				dsnlistsbuf.append("\n\t\t\t" + dsName);
			}

			dsNameList = dsnlistsbuf.toString();
			hostnameList = hnlistsbuf.toString();
			dsNamesArry[nodenamesArry.length-1] = dsNameList;
				// last elt is multi
			nodenamesArry[nodenamesArry.length-1] = hostnameList;
				// last elt is multi

		} catch (Exception e) {
			logger.warning("Exception: " + e.getMessage());
			LoggerUtils.exception("Exception", e, logger);
			String[] i18nArgs = { e.getMessage() };
			lMsg = I18n.getLocalized(logger,
			    "internal.error.ds.exception", i18nArgs);
			throw new DataException(lMsg); // fatal
		}

		singleDataSrc = dataSrcs[0];
			// convenience access for single-node checks
		logger.info("end load data sources array ");
		logger.info("hostname list: " + hostnameList);

		/*
		 * single standalone data cache concept is unworkable
		 * implement a data cache INSIDE each data src
		 * this also solves the problem of nodename collisions
		 *  across clusters
		 */

		// init results object: will accumulate results during the run
		try {
			lMsg = I18n.getLocalized(logger,
			    "engine.initializing.xml");
			engineListener.progressMsg(lMsg);

			String reportHostname = params.commandHostID + "- " +
			    params.commandNode;
			results = new XMLResults(reportHostname, reportsDir,
			    nodenamesArry, dsNamesArry, logPathname, logger);

			logger.info("made XMLResults in " + reportsDir);
		} catch (IOException e) {
			logger.warning("IOException: " + e.getMessage());
			String[] i18nArgs = { e.getMessage() };
			lMsg = I18n.getLocalized(logger,
			    "internal.error.engine.report.create", i18nArgs);
			throw new DataException(lMsg); // fatal
		}

		try {
			lMsg = I18n.getLocalized(logger,
			    "engine.loading.auxdata");
			engineListener.progressMsg(lMsg);
			logger.info("loading auxillary data...");
			FileInputStream fis =
			    new FileInputStream(Globals.AUX_DATA_FILENAME);
			auxData = new Properties();
			// attempt to load aux data from file
			// if this fails auxData will be null
			auxData.load(fis);
			Enumeration e = auxData.propertyNames();
			while (e.hasMoreElements()) {
				String key = (String)e.nextElement();
				String value = auxData.getProperty(key);
				logger.info("aux data key: " + key +
				    ";  value: " + value);
			}
		} catch (FileNotFoundException e) {
			logger.info("unable to locate auxillary data: " +
			    Globals.AUX_DATA_FILENAME);
			auxData = new Properties(); // empty
		} catch (IOException e) {
			logger.info("unable to load auxillary data: " +
			    Globals.AUX_DATA_FILENAME);
			auxData = new Properties(); // empty
		}

		if (auxData == null)
			logger.info("auxillary data IS null");
		else
			logger.info("auxillary data NON null");

		if (params.minSeverity != Check.UNKNOWN_SEV) {

		    /*
		     * Don't run checks with severity less
		     * than this except variable_severity
		     * checks. A variable_severity check
		     * that is violated and is assigned
		     * severity less than minSeverity will
		     *not be reported in the text report.
		     */
            int severityIndex = params.minSeverity;
			String minSeverityName =
			    Check.SEVERITY_NAMES[severityIndex];
            logger.info("params.minSeverity: " + params.minSeverity);
			logger.info("severityIndex: " + severityIndex);
			logger.info("minSeverityName: " + minSeverityName);
			logger.info("filtering out checks with " +
			    "severity less than " + minSeverityName);

			String[] i18nArgs = { minSeverityName };
			lMsg = I18n.getLocalized(logger,
			    "engine.filtering.severity", i18nArgs);
			engineListener.progressMsg(lMsg);
		}

		if (params.keywords != null) {
			String[] i18nArgs = {  params.keywords };
			lMsg = I18n.getLocalized(logger,
			    "engine.filtering.keywords", i18nArgs);
			engineListener.progressMsg(lMsg);
			logger.info("filtering out checks not " +
			    "marked with one of keywords: " + params.keywords);
		}

		if (params.explorerNames != null) {
			lMsg = I18n.getLocalized(logger,
			    "engine.explorer.input");
			engineListener.progressMsg(lMsg);
		}

		logger.info("Engine() -- END--");
	} // ()


	/*
	 * The meat of the matter. Running as a Thread,
	 * instantiate and evaluate Checks.
	 * Tally results inXMLResults object
	 * Produce reports at end of run.
	 * Trap any exceptions thrown by any check so as not to allow
	 * a buggy check to kill the run.
	 */
	public void run() {
		logger.info(" ===  Starting Engine run thread  ==============");
		String lMsg = null; // for I18n fetch
		long startingTime = new Date().getTime();
		long elapsedTime = 0;

		engineListener.checklistStarting();
		String checkID = null;

		// for each checkID...
		for (int i = 0; i < checkIDs.length; i++) {
			checkID = checkIDs[i];
			logger.info("checkID: " + checkID);
				// hostname not available until check.init()
			if (checkID == null) { continue; }
				// entries dump from jar file may include null
			if (checkID.equals("")) { continue; }

			logger.info("instantiating " + checkID);
			Check ck = null;
			try {
				ck = CheckUtils.getCheck(checkID, checksCache,
				    jarClassLoader, logger); // no Ex thrown
			} catch (ClassNotFoundException ex) {
				// not a user entry error
				logger.warning("****  failed to instantiate " +
				    checkID + "*****");
				logger.warning("ClassNotFoundException: " + ex);
				String[] i18nArgs = { checkID };
				lMsg = I18n.getLocalized(logger,
				    "check.instantiation.failed", i18nArgs);
				engineListener.showErr(lMsg);
				continue;
			}
			if (ck == null) {
				// user entry error
				logger.warning(checkID + " ! found in cache: " +
				    "user error");
				String[] i18nArgs = { checkID };
				lMsg = I18n.getLocalized(logger,
				    "check.not.found", i18nArgs);
				engineListener.showErr(lMsg);
				continue;
			}
			// test against minSeverity filter
			// msg must start with "skipped: "
			if (CheckUtils.shouldSkipCheckForSeverity(ck,
				params.minSeverity)) {
				logSkippedCheck(checkID, hostnameList,
				    "skipped: severity too low");
					// I18n in EngineListener
				continue;
			}
			// test against keyword filter
			// msg must start with "skipped: "
			if (params.keywords != null &&
			    CheckUtils.shouldSkipCheckForKeyword(logger, ck,
				params.keywords)) {
				logSkippedCheck(checkID, hostnameList,
				    "skipped: not a keyword match");
					// I18n in EngineListener
				continue;
			}
			try {
				currentHostnameList =
				    dataSrcs[0].getSystemName();
			} catch (DataException dex) {
				logger.warning("DataException fetching " +
				    "systemName: " + dex);
				currentHostnameList = "unknown"; // no I18n
			}

			BasicCheckMultiNode bcmn = null;
			BasicCheck bc = null;

			try {
				logger.info("*****   created " + checkID +
				    ": \"" + ck.getProblemStatement() +
				    "\" for data src " + 0);

				// init & run multi-node check once
				// against all data srcs
				if (ck instanceof BasicCheckMultiNode) {
					logger.info(checkID +
					    ": init for multi-node");
					currentHostnameList = hostnameList;
					engineListener.checkStarting(checkID,
					    currentHostnameList,
					    ck.getProblemStatement());
					bcmn = (BasicCheckMultiNode)ck;
					bcmn.init(this, usingCacao, logger);
						// don't care about 'n'
						// for multi's
					evaluateCheck(checkID, bcmn);
						// ChExEx caught inside

				} else { // is BasicCheck: init & run
					 // once for each data src
					engineListener.checkStarting(checkID,
					    currentHostnameList,
					    ck.getProblemStatement());
					logger.info(checkID + ": init for " +
					    "(single) node data src(" + 0 +
					    "): " + currentHostnameList);

					bc = (BasicCheck)ck;
					bc.init(this, 0, usingCacao, logger);
						// init for first DataSrc
					evaluateCheck(checkID, bc);
						// run for first DataSrc
						// ChExEx caught inside
					/* BEGIN JSTYLED */
					for (int j = 1; j < numInputs; j++) {
						// recreate Check object
						// freshly for each subsequent DataSrc
						currentHostnameList = dataSrcs[j].getSystemName();
						engineListener.checkStarting(
							checkID,
							currentHostnameList,
							ck.getProblemStatement());
						logger.info(checkID +
						    ": init for (single) node data src(" + j + "): " +
						    currentHostnameList);
						ck = CheckUtils.getCheck(checkID,
						    checksCache, jarClassLoader,
						    logger);
						bc = (BasicCheck)ck;
						bc.init(this, j, usingCacao, logger);
							// for subsequent DataSrcs, one by one
						logger.info("*****   created " + checkID +
						    ": \"" + ck.getProblemStatement() +
						    "\" for data src " + j);
						evaluateCheck(checkID, bc);
							// ChExEx caught inside
					} // for numInputs
					/* END JSTYLED */
				} // evaluated check

			} catch (DataException e) {
				logger.warning("DataException " +
				    e.getMessage() + " in: " + checkID + " on "
				    + currentHostnameList);
				handleCrashedCheck(ck, "DataException", e,
				    checkID, currentHostnameList);
			} catch (FileNotFoundException e) {
				logger.warning("FileNotFoundException " +
				    e.getMessage() + " in: " + checkID + " on "
				    + currentHostnameList);
				handleCrashedCheck(ck, "FileNotFoundException",
				    e, checkID, currentHostnameList);
			} catch (IOException e) {
				logger.warning("IOException " + e.getMessage() +
				    " in: " + checkID + " on " +
				    currentHostnameList);
				handleCrashedCheck(ck, "IOException", e,
				    checkID, currentHostnameList);
			} catch (Exception e) {
				logger.warning("Exception " + e.getMessage() +
				    " in: " + checkID + " on " +
				    currentHostnameList);
				handleCrashedCheck(ck, "Exception", e, checkID,
				    currentHostnameList);
			}

		} // for each checkID

		elapsedTime = new Date().getTime() - startingTime;

		// finished with checks
		logger.info("checklistFinished ");
		engineListener.checklistFinished();
		logger.info("elapsed time for checklist: " + elapsedTime);

		// finish up xml & text reports
		lMsg = I18n.getLocalized(logger, "engine.finishing.xml");
		engineListener.progressMsg(lMsg);
		results.finish(elapsedTime);
		engineListener.checkRunFinished(maxViolatedSeverity);

		// dump the caches to session log
		logger.finest("dump caches start ======================");
		for (int i = 0; i < dataSrcs.length; i++) {
			logger.finest("dump cache from data src ["+ i + "]: " +
			    dataSrcs[i].getDataSrcName());
			logger.finest(dataSrcs[i].dumpCache());
		}
		logger.finest("dump caches end ======================");

		logger.info("Engine.run() -- EXIT -- ========================");
	} // run

	// accepts Throwable (needed for Error)
	private void handleCrashedCheck(Check ck, String exType, Throwable t,
	    String checkID, String currentHostnameList) {
		logger.warning("-- ENTER throwable: " + t);
		handleCrashedCheck(ck, exType, new CheckExecutionException(t),
		    checkID, currentHostnameList);
	} // handleCrashedCheck

	// accepts generic Exception
	private void handleCrashedCheck(Check ck, String exType, Exception e,
	    String checkID, String currentHostnameList) {
		logger.warning("-- ENTER generic: " + e);
		handleCrashedCheck(ck, exType, new CheckExecutionException(e),
		    checkID, currentHostnameList);
	} // handleCrashedCheck

	// accepts CheckExecutionException
	private void handleCrashedCheck(Check ck, String exType,
	    CheckExecutionException e, String checkID,
	    String currentHostnameList) {
		logger.warning("-- ENTER CheckExecutionException  --");
		logger.warning("*****  *****     *****    --  ENTER: " + ck);
		ck.setStatus(Check.EXECUTION_ERROR);
		logger.info(exType + " " + e.getMessage() + " in " + checkID +
		    " on " + currentHostnameList);
		LoggerUtils.exception("handleCrashedCheck(): " + checkID +
		    " on " + currentHostnameList, e.getPayloadThrowable(),
		    logger);

		engineListener.checkCrashed(checkID, e, currentHostnameList);

		// add to xml results
		results.addCheck(ck);
	} // handleCrashedCheck


	/*
	 * For a given Check, test to see if we should even evaluate it
	 * If so, run it's doCheck() method
	 * Tally the results
	 * Track the maximum severity of all violated checks.
	 * Don't allow any buggy check to crash the system.
	 */
	private void evaluateCheck(String checkID, Check ck) {
		logger.info("Engine.evaluateCheck() -- ENTER --.");
		Date date = new Date();
		long checkStartingTime = date.getTime();
		long checkElapsedTime = 0;
		logger.info("starting " + checkID + " on " +
		    currentHostnameList + " at " + date);
		try {
			boolean applicable = ck.isApplicable();
			logger.info("applicable: " + applicable + " for: " +
			    checkID + " on " + currentHostnameList);
			if (!applicable) {
				ck.setStatus(Check.NOT_APPLICABLE);
				engineListener.checkNA(checkID,
				    currentHostnameList);
			} else {

				boolean passed = ck.doCheck();
					// the action happens here

				String insufficientData =
				    ck.getInsufficientDataMsg();
				if (passed) {
					ck.setStatus(Check.PASSED);
					logger.info("check passed: " + checkID +
					    " on " + currentHostnameList);
					engineListener.checkPassed(checkID,
					    currentHostnameList);
				} else if (ck.getSeverity() ==
				    Check.INFORMATION) {
					ck.setStatus(Check.INFORMATION_ONLY);
					logger.info("check information only: " +
					    checkID + " on " +
					    currentHostnameList);
					engineListener.checkInfoOnly(checkID,
					    currentHostnameList);
				} else if (ck.getSeverity() == Check.WARNING) {
					ck.setStatus(Check.INFORMATION_ONLY);
					logger.info("check warning only: " +
					    checkID + " on " +
					    currentHostnameList);
					engineListener.checkWarning(checkID,
					    currentHostnameList);
				} else if (insufficientData != null &&
				    !insufficientData.equals("")) {
					ck.setStatus(Check.INSUFFICIENT_DATA);
					logger.info(
						"check insufficient data: " +
							checkID + " on " +
							currentHostnameList);
					engineListener.insufficentData(checkID,
					    insufficientData,
					    currentHostnameList);
				} else {
					ck.setStatus(Check.VIOLATED);
					logger.info("check violated: " +
					    checkID + " on " +
					    currentHostnameList);
					engineListener.checkViolated(checkID,
					    currentHostnameList);
					logger.info(checkID +
					    " getAnalysis(): " +
					    ck.getAnalysis());
					logger.info(
						"prior maxViolatedSeverity: " +
							maxViolatedSeverity);
					int sev = ck.getSeverity();
					logger.info("severity this " +
					    "violated check: " + sev);
					maxViolatedSeverity =
					    CheckUtils.mostSevere(
						    maxViolatedSeverity, sev);
					logger.info(
						"new maxViolatedSeverity: " +
							maxViolatedSeverity);
				}
				logger.info("testing for info message: " +
				    checkID + " on " + currentHostnameList);
				String infoMsg = ck.getInfoMessage();
				if (infoMsg != null && !infoMsg.equals("")) {
					logger.info("check has info message: " +
					    checkID + ": >>" + infoMsg + "<<");
					engineListener.infoMessage(checkID,
					    infoMsg, currentHostnameList);
				}
			} // is applicable

			// add to xml results (handleCrashedCheck()
			// calls add for its checks
			results.addCheck(ck);

		} catch (CheckExecutionException e) {
			logger.warning("CheckExecutionException " +
			    e.getMessage() + " in: " + checkID + " on " +
			    currentHostnameList);
			handleCrashedCheck(ck, "CheckExecutionException", e,
			    checkID, currentHostnameList);
		} catch (Exception e) {
			logger.warning("Exception " + e.getMessage() + " in: " +
			    checkID + " on " + currentHostnameList);
			handleCrashedCheck(ck, "Exception", e, checkID,
			    currentHostnameList);
			// no further throw
		} catch (ClassFormatError e) {
			logger.warning("ClassFormatError " + e.getMessage() +
			    " in: " + checkID + " on " + currentHostnameList);
			handleCrashedCheck(ck, "ClassFormatError", e, checkID,
			    currentHostnameList);
			// no further throw
		} catch (Error e) {
			logger.warning("Error " + e.getMessage() + " in: " +
			    checkID + " on " + currentHostnameList);
			handleCrashedCheck(ck, "Error", e, checkID,
			    currentHostnameList);
			// no further throw
		}
		checkElapsedTime = new Date().getTime() - checkStartingTime;
		logger.info("elapsed time for " + checkID + ": " +
		    checkElapsedTime + " on " + currentHostnameList);
		logger.info("Engine.evaluateCheck() -- END --.");
	} // evaluateCheck

	// I18n in EngineListener
	private void logSkippedCheck(String checkID, String hostnames,
	    String reason) {
		logger.info("skipping: " + checkID + " " + reason);
		engineListener.checkSkipped(checkID, hostnames, reason);
	} //     logSkippedCheck

	public ClusterDataSrc getSingleDataSrc() {
		return singleDataSrc;
	}
	public ClusterDataSrc getDataSrc(int i) {
		logger.finest("numInputs: " + numInputs);
		logger.finest("dataSrcs.length: " + dataSrcs.length);
		logger.info("dataSrcs[" + i+"] -- ENTER -- ");
		logger.info("dataSrcs[" + i+"]: " +
		    dataSrcs[i].getDataSrcName());
		return dataSrcs[i];
	}
	public ClusterDataSrc[] getDataSrcs() {
		return dataSrcs;
	}
	public int getNumInputs() {
		return numInputs;
	}

	public String toString() {
		return engineName;
	}
    	public String getEngineName() {
		logger.info("name: " + engineName);
		return engineName;
	}
	public int getNumDataSrcs() {
		logger.info("numInputs: " + numInputs);
		return numInputs;
	}

	public Properties getAuxData() {
		return auxData;
	}

	// msg must already be i18n
	public void sendProgressMsg(String lMsg) {
		engineListener.progressMsg(lMsg);
	}

} // class
