/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)AnonymousCheck.java	1.2	08/06/02 SMI"
 */
package com.sun.cluster.cfgchk.check;

import com.sun.cluster.cfgchk.BasicCheck;
import com.sun.cluster.cfgchk.Check;
import com.sun.cluster.cfgchk.CheckExecutionException;

/**
 *
 * AnonymousCheck - used automatically by list-checks
 * when the intended Check can't be instantiated.
 *
 * Never shown by name to users. Placeholder content.
 */

/*
 * No I18n of any strings in this class.
 *
 * Exceptions thrown from Checks are caught and handled by Engine
 * typically just by logging. Messages are never displayed to user.
 *
 * Lines with trailing 'TOC' comment are auto-gathered into
 * table of contents listing of all checks.
 */

public class AnonymousCheck extends BasicCheck {

	/*
	 * Used as a sort of info carrier when unable to instantiate a Check
	 */

	public AnonymousCheck(String checkID) {
		this.checkID = checkID; // whatever is sent in...  // TOC
		severity = Check.UNKNOWN_SEV; // TOC

		// sccs keyword: must be 1.2 when checked out under sccs
		version = "1.2";
		// sccs keyword: must be 08/06/02 when checked out under sccs
		revisionDate = "09/12/08";

		keywords.add("SunCluster3");
		keywords.add("single");
		problemStatement = "Unable to instantiate Check " + checkID; // TOC
	} // ()

	/**
	 * @return a string describing the logic of the applicability test
	 */
	protected String assignApplicabilityLogic() {
		String aLogic = "";
		return aLogic;
	} // assignApplicabilityLogic


	/**
	 * @return a string describing the logic of the check
	 */
	protected String assignCheckLogic() {
		String cLogic = "";
		return cLogic;
	} // assignCheckLogic


	/**
	 * @return a string for use in output report analyzing the problem
	 * may contain dynamic data
	 */
	public String getAnalysis() {
		StringBuffer analysis = new StringBuffer("");
		return analysis.toString();
	} // getAnalysis


	/**
	 * @return a string for use in output report giving recommended fix
	 */
	public String getRecommendations() {
		String recommend = "";
		return recommend;
	} // getRecommendations

	/*
	 * never applicable
	 */
	public boolean isApplicable() throws CheckExecutionException {
		boolean applicable = false;
		return applicable;
	} // isApplicable

	/*
	 * Perform the actual checking
	 * update message for use in getAnalysis() during check
	 * @return true if check passes, false if violated
	 */
	public boolean doCheck() throws CheckExecutionException {
		boolean passes = true;
		return passes;
	} // doCheck

} // class
