/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)Vfstab.java	1.2	08/05/29 SMI"
 */
package com.sun.cluster.cfgchk.datasrc.fileObject;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;

import java.util.ArrayList;
import java.util.logging.Logger;

import com.sun.cluster.cfgchk.datasrc.BasicDataSrc;
import com.sun.cluster.cfgchk.datasrc.BasicObject;
import com.sun.cluster.cfgchk.datasrc.DataException;
import com.sun.cluster.cfgchk.datasrc.InfoMessageObject;
import com.sun.cluster.cfgchk.datasrc.ParseException;
import com.sun.cluster.cfgchk.datasrc.dataObject.VfstabEntry;

import com.sun.cluster.cfgchk.utils.logger.RemoteLogger;

/**
 * Class representing /etc/vfstab<br>
 * Holds entries in VfstabEntry[]<br>
 * SolarisDataSrc creates and populates this object
 */
public class Vfstab extends BasicObject implements InfoMessageObject,
					Serializable {
        private static final long serialVersionUID = 8503207141497524473L;

    private ArrayList lines = null;
    private String nodename = null;
    private static transient Logger logger = RemoteLogger.getRemoteLogger();

    public Vfstab(String nodename) {
        logger.info("Vfstab() -- ENTER --");
	this.nodename = nodename;
        logger.info("Vfstab() -- EXIT --");
    } // ()


    public void setEntries(ArrayList entries) {
            this.lines = entries;
    } // setEntries


    /**
     * @return array of VfstabEntries
     */
    public VfstabEntry[] getEntries() {
            return toVfstabEntryArray(lines, false); // not globalOnly
    } // getEntries


	// private worker
    private VfstabEntry[] getEntries(boolean globalOnly) {
            return toVfstabEntryArray(lines, globalOnly);
    } // getEntries

    /**
     * @return VfstabEntry[] of global fs entries only
     */
    public VfstabEntry[] getGlobalEntries() {
        return getGlobalEntries(true); // incl 'node@' lines
    } // getGlobalEntries

    /**
     * @param includeNodeAtLines
     * @return VfstabEntry[] of global fs entries only
     */
    public VfstabEntry[] getGlobalEntries(boolean includeNodeAtLines) {
	logger.info("-- ENTER includeNodeAtLines: " + includeNodeAtLines);

        VfstabEntry[] globalEntries = getEntries(true); // global only

        if (includeNodeAtLines) {
            return globalEntries; // early exit
        }

        // else strip 'node@' lines
        VfstabEntry[] globalEntriesNoNodeAt =
	    new VfstabEntry[globalEntries.length];
        logger.fine("globalEntries.length: " + globalEntries.length);
        int index = 0;
        for (int i = 0; i < globalEntries.length; i++) {
            if (globalEntries[i] != null) {
                int nodeAtIndex =
		    globalEntries[i].getMountPoint().indexOf("node@");
                logger.fine("nodeAtIndex: " + nodeAtIndex);
                if (nodeAtIndex == -1) {
                    globalEntriesNoNodeAt[index++] = globalEntries[i];
                } else {
			logger.fine("exclude globalEntries[i]: " +
			    globalEntries[i].getMountPoint());
                }
            } // for globalEntries

        } // for globalEntries

        logger.info("-- EXIT --");
        return globalEntriesNoNodeAt;
    } // getGlobalEntries


    private VfstabEntry[] toVfstabEntryArray(ArrayList al) {
        return toVfstabEntryArray(al, false); // do not exclude non-globals
    }

    /*
     * convert ArrayList to VfstabEntry typed array
     * option to return global entries only
     * if global only then some entries in returned [] may be null
     */
    private VfstabEntry[] toVfstabEntryArray(ArrayList al, boolean globalOnly) {
	logger.info("-- ENTER -- globalOnly: " + globalOnly);

        VfstabEntry[] vea = null;
        VfstabEntry entry = null;
        if (al == null) {
            return new VfstabEntry[0];
        } else {
            vea = new VfstabEntry[al.size()];
            logger.finest("made new array, size " + al.size());
            int index = 0;
            for (int i = 0; i < al.size(); i++) {
                entry = (VfstabEntry)al.get(i);
                if (globalOnly && !entry.isGlobalEntry()) {
			logger.info("excluded : " + entry.getMountPoint());
			continue;
		}
		vea[index++] = entry;
		logger.fine("added : " + entry.getMountPoint());
            }
            return vea;
        }
    } // toVfstabEntryArray




} // class
