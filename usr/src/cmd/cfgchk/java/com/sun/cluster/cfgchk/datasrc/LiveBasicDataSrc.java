/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)LiveBasicDataSrc.java	1.4	08/09/22 SMI"
 */
package com.sun.cluster.cfgchk.datasrc;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import java.util.List;
import java.util.logging.Logger;

import javax.management.ObjectName;

import com.sun.cacao.ObjectNameFactory;
import com.sun.cacao.invocation.InvocationStatus;
import com.sun.cluster.cfgchk.datasrc.fileObject.DirectoryListing;
import com.sun.cluster.cfgchk.datasrc.fileObject.TextFile;
import com.sun.cluster.cfgchk.datasrc.fileObject.FileInfo;
import com.sun.cluster.cfgchk.utils.Utils;
import com.sun.cluster.cfgchk.utils.logger.RemoteLogger;


/**
 *
 * This is an implementation of interface BasicDataSrc which knows
 * how to access to system files and commands on a live
 * Solaris system.
 *
 * Code executed here is always executed on localhost but
 * is not always the originating node.
 *
 * Data is made available transparently to checks via
 * mbean proxy of either this class or a superclass.
 *
 * This implementation maintains a data cache.
 * Data is loaded lazily and cached when obtained.
 *
 */

public class LiveBasicDataSrc implements BasicDataSrc, LiveBasicDataSrcMBean {

	protected String systemName = null; // cheapie cache
	protected String dsName = null;
	private ObjectNameFactory onf = null;

	protected GenericCache dataCache = null;

	protected String dsVersion = "1.0";

        // Logger object
	protected static Logger logger = RemoteLogger.getRemoteLogger();

	// Part of the contract with the ObjectNameFactory
	final public static String TYPE = "LiveBasicDataSrc";

	// constructor for use as mbean
	public LiveBasicDataSrc(ObjectNameFactory onf, String systemName) {
		this.systemName = systemName;
		this.dsName = systemName;
		this.onf = onf;
		logger.info("dsName: " + dsName);
		dataCache = new GenericCache(dsName + ": LiveBasicDataSrc");
			// No I18n
	} // ()

	// used by localhost & non-cacao
	public LiveBasicDataSrc(String systemName) {
		this.dsName = systemName;
		this.systemName = systemName;
		dataCache = new GenericCache(dsName);
		logger.info(dsName + ": systemName: >>" + systemName + "<<");
	} // ()

	// ===  start interface BasicDataSrc  ===

	public String getDataSrcName() {
		logger.info(dsName + ": dsName: " + dsName);
		return dsName;
	}

	public String getDataSrcVersion() {
		return dsVersion;
	}

	public boolean isLiveDataSource() {
		logger.info(dsName + ": isLiveDataSource: " + true);
		return true;
	}

	public String getSystemName() {
		return systemName;
	}

	// may return empty, but never null, list
	public List getTextFileStrings(String filename, String target)
		throws DataException, FileNotFoundException, IOException {
		TextFile tf = getTextFile(filename, false);
		List list = tf.containsStrings(target);
		return list;
	} // getStreamingTextFile


	public TextFile getTextFile(String filename)
		throws DataException, FileNotFoundException, IOException {
		return getTextFile(filename, true);
	}

	// may specify not to cache the file
	// useful if expected to be large enough to run out of memory
	public TextFile getTextFile(String filename, boolean cache)
		throws DataException, FileNotFoundException, IOException {

		TextFile tf = null;
		String cacheKey = "getTextFile :-: " + filename;
		logger.info(dsName + ": -- ENTER -- " + cacheKey);

		try {
			tf = dataCache.getTextFile(cacheKey);
			if (tf == null) {
				logger.info(dsName + ": " + filename +
				    " not found in cache");
				try {
					tf = new TextFile(filename);
						// fnfex, ioex
					logger.info(dsName +
					    ": made TextFile: " + filename);
					/* BEGIN JSTYLED */
					if (cache == true) {
						dataCache.put(cacheKey, tf);
						logger.info(dsName +
						    ": TextFile added to cache");
					} else {
						logger.info(dsName +
						    ": TextFile NOT added to cache");
					}
					/* END JSTYLED */
				}  catch (FileNotFoundException fnfex) {
					logger.warning(dsName +
					    ": FileNotFoundException: " +
					    fnfex);
					throw fnfex;
				} catch (IOException ioex) {
					logger.warning(dsName +
					    ": IOException: " + ioex);
					throw ioex;
				}
			} // == null

		} catch (DataException dex) { // already I18n; bubble it up
			logger.warning(dsName + ": DataException: " +
			    dex.getMessage());
			throw dex;
		} catch (Error err) {
			logger.warning(dsName + ": caught java.lang.Error: " +
			    err.getMessage());
			throw new DataException(err.getCause());
		} catch (Exception ex) {
			logger.warning(dsName + ": caught java.lang.Exception: " +
			    ex.getMessage());
			throw new DataException(ex);
		}
		logger.info(dsName + ": -- EXIT-- TextFile: " + filename);
		return tf;
	} // getTextFile


	public File getFile(String filename) throws DataException,
		FileNotFoundException {

		File f = null;
		String cacheKey = "getFile :-: " + filename;
		logger.info(dsName + ": -- ENTER --" + cacheKey);

		try {
			f = dataCache.getFile(cacheKey);
			if (f == null) {
				logger.info(dsName + ": not found in cache");
				try {
					f = new File(filename);
					if (!f.exists()) {
						String msg =
						    "file does not exist: " +
						    filename; // no I18n
						logger.warning(msg);
						throw new
						    FileNotFoundException(msg);
					} else {
						logger.info(dsName +
						    ": got File: " + filename);

						dataCache.put(cacheKey, f);
						logger.info(dsName +
						    ": File added to cache");
					}

				} catch (Exception ex) {
					logger.warning(dsName +
					    ": Exception: " + ex); // no I18n
					throw new DataException(ex);
				}
			} // == null
		} catch (DataException dex) { // already I18n; bubble it up
			logger.warning(dsName + ": DataException: " +
			    dex.getMessage());
			throw dex;
		}
		logger.info(dsName + ": -- EXIT-- File: " + filename);
		return f;
	} // getFile


	public DirectoryListing getDirectoryListing(String dirName)
		throws DataException, FileNotFoundException {
		return getDirectoryListing(dirName, "");
	}

	/*
	 * flags arg never needed in this impl
	 * if dir doesn't exist or can't be read
	 * or is a file but not a directory then throw FNFEx
	 */
	public DirectoryListing getDirectoryListing(String dirName,
	    String flagsIgnored)
		throws DataException, FileNotFoundException {

		DirectoryListing dirListing = null;
		String cacheKey = "getDirectoryListing :-: " + dirName;
		logger.info(dsName + ": -- ENTER --" + cacheKey);

		try {
			dirListing = dataCache.getDirectoryListing(cacheKey);
			if (dirListing == null) {
				logger.info(dsName + ": not found in cache");
				try {
					dirListing = new DirectoryListing(
						dirName); // no Ex

					File dir = new File(dirName); // no Ex
					if (!dir.exists()) {
						logger.info(dsName +
						    ": dirName not exist: " +
						    dirName);
						throw new
						    FileNotFoundException(
							    dirName);
					}
					if (!dir.canRead()) {
						logger.info(dsName +
						    ": Unable to read: " +
						    dirName);
						throw new
						    FileNotFoundException(
							    "Unable to read " +
								    dirName);
					}
					if (!dir.isDirectory()) {
						logger.info(dsName +
						    ": Not a directory: " +
						    dirName);
						/* BEGIN JSTYLED */
						throw new
						    FileNotFoundException(
							    "Not a directory: " +
								    dirName);
						/* END JSTYLED */
					}

					File[] files = dir.listFiles();
					FileInfo[] fia =
					    new FileInfo[files.length];
					for (int i = 0; i < files.length; i++) {
						fia[i] = new FileInfo(files[i]);
						logger.finest(
							"made FileInfo: " +
								fia[i]);
					}
					dirListing.putEntries(fia);
					logger.info(dsName +
					    ": dirListing num lines: " +
					    files.length);

					dataCache.put(cacheKey, dirListing);
					logger.info(dsName +
					    ": DirectoryListing added 2 cache");

				}  catch (FileNotFoundException ex) {
					throw ex; // pass it on out
				} catch (Exception ex) {
					logger.warning(dsName +
					    ": Exception: " + ex);
					throw new DataException(ex);
				}
			} // == null
		} catch (FileNotFoundException ex) {
				// already I18n; bubble it up
			logger.warning(dsName + ": FileNotFoundException: " +
			    ex.getMessage());
			throw ex; // rethrow
		} catch (DataException ex) { // already I18n; bubble it up
			logger.warning(dsName + ": DataException: " +
			    ex.getMessage());
			throw ex; // rethrow
		} catch (Exception ex) {
			logger.warning(dsName + ": Exception: " +
			    ex.getMessage());
			throw new DataException(ex);
		}
		logger.info(dsName + ": -- EXIT: dirListing found: " +
		    dirListing);
		return dirListing;
	} // getDirectoryListing


	// DataEx not I18n; bubbled up to handleCrashedCheck(): I18n not needed
	public boolean isPackageInstalled(String packageName)
		throws DataException, InsufficientDataException {

		boolean isInstalled = false;
		String cacheKey = "isPackageInstalled :-: " + packageName;
		logger.info(dsName + ": -- ENTER --" + cacheKey);

		try {
			Boolean b = dataCache.getBoolean(cacheKey);
			if (b == null) { // not in cache
				logger.info(dsName + ": not found in cache");
				String[] cmd = { "/usr/bin/pkginfo", "-q",
						 packageName };
				int exitCode = runCommandInt(cmd); // DEx
				logger.info(dsName + ": packageName: " +
				    packageName + "; exitCode: " + exitCode);

				if (exitCode == 0) {
					isInstalled = true;
					dataCache.put(cacheKey,
					    new Boolean(true));
				} else {
					// not found is not an error case:
					// cache it
					dataCache.put(cacheKey,
					    new Boolean(false));
					String msg = packageName +
					    "not installed on " + systemName;
					logger.info(msg);
				}
				logger.info(dsName +
				    ": Boolean added to cache");

			} else { // got hit in cache: value can be true or false
				if (b.booleanValue() == true) {
					isInstalled = true;
				}
			}
		} catch (InsufficientDataException idex) {
			// never thrown by this impl of DataSrc
			// implemented to satisfy the interface
			logger.warning(dsName + ": InsufficientDataException: " +
			    idex.getMessage());
			throw idex;
		} catch (DataException dex) { // already I18n; bubble it up
			logger.warning(dsName + ": DataException: " +
			    dex.getMessage());
			throw dex;
		}
		logger.info(dsName + ": -- EXIT-- packageName: " +
		    packageName + "; isInstalled: " + isInstalled);
		return isInstalled;
	} // isPackageInstalled


	// should force runCommand to C locale? XXX
	// all these commands run on localhost so no need to use cacao

	/*
	 * run a command and return only the exit code
	 * command return code not cached
	 * InsufficientDataException never thrown in this implementation
	 */
	public int runCommandInt(String[] cmd)
		throws DataException, InsufficientDataException {
		logger.info(dsName + ": ENTER: cmd[0]: " + cmd[0]);

		InvocationStatus exitStatus = runCommand(cmd);
		int exitCode = exitStatus.getExitValue().intValue();
		logger.info(dsName + ": exitCode: " + exitCode);
		return exitCode;
	} // runCommand


	/*
	 * run command on localhost and return std out as array of String
	 * if command returns non-0 throw DataException with message
	 * containing exit code and stderr as String
	 *
	 * command output not cached
	 * InsufficientDataException never thrown in this implementation
	 */
	public String[] runCommandStrArray(String[] cmd)
		throws DataException, InsufficientDataException {
			logger.info(dsName + ": ENTER: cmd[0]: " + cmd[0]);
			String[] result = null;
			result = Utils.runCommandStrArray(cmd); // non-null
			logger.info(dsName +
			    " back from Utils.runCommandStrArray(cmd)");
			logger.info(dsName + ": EXIT:  num lines: " +
			    result.length);
			return result;
		} // runCommandStrArray

	/*
	 * run a command and return results in InvocationStatus
	 *
	 * command output not cached
	 * InsufficientDataException never thrown in this implementation
	 */
	public InvocationStatus runCommand(String[] cmd)
		throws DataException, InsufficientDataException {
		logger.info(dsName + ": -- ENTER --");
		return Utils.runCommand(cmd);
	} // runCommand

	public String dumpCache() {
		logger.fine(dsName + ": -- ENTER --");
		String dump = dataCache.toString();
		// caller will do actual logging of content
		logger.fine(dsName + ": -- EXIT --");
		return dump;
	} // dumpCache

	/*
	 * the mbean for this class is held open by CfgchkModule
	 * so we need to clear the cache at end of each run
	 */
	public void cleanup() {
		logger.info(dsName + ": -- ENTER --");
		dataCache.clear();
	} //

	public String toString() {
		return dsName + ": " + getDataSrcVersion();
	}

	// ===  end interface BasicDataSrc  ===


	// mbean related methods

	/**
	 * create the object name for the given operation
	 * object name (this method is called when the mbean for
	 * this object is registered with the MBean server).
	 *
	 * @return the objectName to use
	 */
	public ObjectName createON() {
		logger.info(dsName + ": [=LiveBasicDataSrc] createON");

		ObjectName on = onf.getObjectName(LiveBasicDataSrcMBean.class,
		    null);
		return on;
	} // createON

	public void start() {
		logger.fine(dsName + ": starting LiveBasicDataSrc");
	}

	public void stop() {
		logger.fine(dsName + ": stopping LiveBasicDataSrc");
	}

} // class
