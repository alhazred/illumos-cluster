/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)ErrorMsg.java	1.2	08/05/29 SMI"
 */
package com.sun.cluster.cfgchk.utils;

import java.util.logging.Logger;

import com.sun.cluster.cfgchk.Globals;
import com.sun.cluster.cfgchk.i18n.I18n;

public class ErrorMsg  {

	/*
	 * Given a message key (and optionally other params)
	 * calculate msgid from key, not from expaned English
	 * message because inserted args can change the msgid
	 * from node to node and run to run.
	 *
	 * Then look the key up in specified locale,
	 * prepend the msgid in proper format for new cli
	 * and return it for display.
	 *
	 */

	// these signatures match selected I18n.getLocalized() signatures

	public static String makeErrorMsg(Logger logger, String key) {
		if (logger != null) logger.info("Logger logger, String key: " +
		    key);
		String msgID = MsgID.makeMsgID(key);
		String msg = assemble(msgID, I18n.getLocalized(logger, key));
		if (logger != null) logger.info("-- EXIT --");
		return msg;
	} // makeErrorMsg



	public static String makeErrorMsg(Logger logger, String key,
	    Object[] i18nArgs) {

	if (logger != null) 	logger.info("Logger logger, String key, " +
		    "Object[] i18nArgs, Object[] i18nArgs: " + key);
		String msgID = MsgID.makeMsgID(key);
		String msg = assemble(msgID, I18n.getLocalized(logger, key,
			i18nArgs));
		if (logger != null) logger.info("-- EXIT --");
		return msg;
	} // makeErrorMsg


	private static String assemble(String msgID, String msg) {
		return Globals.PROGNAME + ":   " + msgID + " " + msg;
	} // assemble


} // class
