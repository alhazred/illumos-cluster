/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)FileInfo.java	1.2	08/05/29 SMI"
 */
package com.sun.cluster.cfgchk.datasrc.fileObject;

import java.io.File;
import java.io.Serializable;

import java.util.StringTokenizer;
import java.util.logging.Logger;

import com.sun.cluster.cfgchk.datasrc.DataException;
import com.sun.cluster.cfgchk.utils.logger.RemoteLogger;
import com.sun.cluster.cfgchk.utils.StringUtils;


/*
 * Class to unify representation of file information
 * regardless of  Data Src origin
 *
 * Used by DirectoryListing
 */

public class FileInfo implements Serializable {
	protected static Logger logger = RemoteLogger.getRemoteLogger();

    private static final long serialVersionUID = 52962521593L;

	private boolean isRegularFile = true;
	private boolean isDirectory = false;
	private boolean isLink = false;
	// expand into other types as needed in the future

	private String name = null;
	private String parentDir = null; // will be set with trailing slash
	private long size = -1;


	// load from File
	public FileInfo(File f) {
		loadFromFile(f);
	} // ()

	// load from `ls -l name`
	public FileInfo(String str, String parent) throws DataException {
		loadFromString(str, parent);
	} // ()

	public String getName() {
		return name;
	}

	public String getFullName() {
		return parentDir + name;
	}

	public long getSize() {
		return size;
	}

	public String getParentDir() {
		return parentDir;
	}

	public void setParentDir(String pd) {
		this.parentDir = StringUtils.forceTrailingChar(pd, '/');
	} // setParentDir

	public boolean isRegularFile() {
		return isRegularFile;
	}

	public boolean isDirectory() {
		return isDirectory;
	}

	private void loadFromFile(File f) {
		// the correct things happen with symlinks
		if (f.isDirectory()) {
			this.isDirectory = true;
			this.isRegularFile = false;
		} else {
			if (f.isFile()) {
			this.isDirectory = false;
			this.isRegularFile = true;
		}
		    }
		this.name = f.getName();
		setParentDir(f.getParent());
	} // loadFromFile

	private void loadFromString(String str, String parent)
		throws DataException {
		logger.finest("-- ENTER: "+ parent + "/" + str);
		StringTokenizer st = new StringTokenizer(str, " ,");
		int numTokens = st.countTokens();
		if (numTokens < 9) {
			throw new DataException(
				"insufficient tokens in line: " + str);
		}
		String[] tokens = new String[numTokens];
		for (int i = 0; i < numTokens; i++) {
			tokens[i] = st.nextToken();
			logger.finest("token: " + tokens[i]);
		}

		String ftype = tokens[0].substring(0, 1);
		logger.finest("perms: " + tokens[0]);
		logger.finest("ftype: " + ftype);

		if (ftype.equals("d")) {
			isRegularFile = false;
			isDirectory = true;
			isLink = false;
		} else if (ftype.equals("l")) { // ell, not one
			isRegularFile = false;
			isDirectory = false;
			isLink = true;
		} else { // expand into other types as needed in the future

			isRegularFile = true;
			isDirectory = false;
			isLink = false;
		}

		String sizeStr = tokens[4];
		// logger.finest("size: " + sizeStr);
		this.size = -1;
		try {
			this.size = Long.parseLong(sizeStr);
		} catch (NumberFormatException ex) {
			logger.severe("NumberFormatException: " + ex);
			// and continue
		}
		logger.finest("long size: " + this.size);

		this.name = tokens[8];
		if (numTokens == 10) {
			name = tokens[9];
		}
		logger.finest("name: " + name);

		setParentDir(parent);
		logger.finest("parent: " + parent);

	} // loadFromString



	public String toString() {
		String SEP = " | ";
		StringBuffer sb = new StringBuffer("name: " + name);
		sb.append(SEP + "parentDir: " + parentDir);
		sb.append(SEP + "isRegularFile: " + isRegularFile);
		sb.append(SEP + "isDirectory: " + isDirectory);
		sb.append(SEP + "size: " + size);


		return sb.toString();
	} // toString
} // class

