/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)Check.java	1.3	08/08/26 SMI"
 */
package com.sun.cluster.cfgchk;

import java.io.FileNotFoundException;
import java.util.List;


/**
 * A Check is an Object consumable by the cfgchk Engine
 *
 * Its key method is doCheck() which returns true or false
 * for passed or violated.
 */

public interface Check {


	// status categories in reporting order
    public static final int VIOLATED = 0;
    public static final int INSUFFICIENT_DATA = 1;
    public static final int EXECUTION_ERROR = 2;
    public static final int UNKNOWN_STATUS = 3;
    public static final int INFORMATION_ONLY = 4;
    public static final int NOT_APPLICABLE = 5;
    public static final int PASSED = 6;
    public static final int NUM_STATUS_CATEGORIES = 7;

        public static final String[] STATUS_NAMES = {
		"Violated", "Insufficient Data",
		"Execution Error", "Unknown Status",
		"Information Only", "Not Applicable",
		"Passed", "not-used"
	};
        public static final String[] STATUS_XML_NAMES = {
		"Violated", "Insufficient_Data",
		"Execution_Error", "Unknown_Status",
		"Information_Only", "Not_Applicable",
		"Passed", "not-used"
	};


	// severities
    public static final int CRITICAL = 1;
    public static final int HIGH = 2;
    public static final int MODERATE = 3;
    public static final int LOW = 4;
    public static final int WARNING = 5;
    public static final int INFORMATION = 6;
    public static final int VARIABLE_SEV = 7;
    public static final int UNKNOWN_SEV = 8; // must always be greatest value
    public static final int NUM_SEVERITIES = 9;

    public static final String[] SEVERITY_NAMES = {
        "No Violations", "Critical", "High", "Moderate", "Low",
        "Warning", "Information",
        "Variable", "Unknown", "not-used"
    };

    /**
     * Return the check's unique identifier. It is a String which
     * by convention is the latter part of the java file which implements it.
     * The filename must be of the form Check[S|M][..]
     * where "S" indicates single node checks and "M" indicates
     * a multi-node check. The [..] must be at least two characters long
     * and by convention is the bugster Change Request ID in which this
     * check is specified.
     * The checkID is the java filename excluding the leading "Check."
     *
     * See CheckUtils.getCheckNameFromClassName()
     *
     * @return the check's unique identifier
     */
    public String getID();

    /**
     * Return a short statement describing the violated condition.
     * This statement is also used as a check title and synopsis.
     * @return the check title: a short statement describing the
     * violated condition
     */
    public String getProblemStatement();
    public String getSynopsis();

    /**
     * Return an integer indicating severity of the check
     * XXX javadoc need to link to constants here XXX
     * @return integer indicating severity of the check
     */
    public int getSeverity();

    /**
     * Return an integer indicating the result status
     * XXX javadoc need to link to constants here XXX
     * @return integer indicating status of the check
     */
    public int getStatus();

    /**
     * XXX javadoc need to link to constants here XXX
     * This value is set by the Engine upon check completion,
     * then queried by report generation
     */
    public void setStatus(int status);

    /**
     * Return String describing the violation and its consequences.
     * Dynamic data may be included.
     * Not subject to localization.
     * @return String describing the violation and its consequences
     */
    public String getAnalysis();

    /**
     * Return String describing steps recommended to clear the violation.
     * Dynamic data may be included.
     * Not subject to localization.
     * @return String describing steps recommended to clear the violation
     */
    public String getRecommendations();

    /**
     * Returns a List of keywords attached to this Check.
     * The first keyword should be the name of the main product or
     * subsystem involved, e.g. SunCluster or Storage or SCGE.
     * Additional keywords may help identify other subject areas such as
     * RGM or HASP.
     * @return a List of keywords attached to this Check.
     */
    public List getKeywordsList(); // first is main product

    /**
     * Returns a String of comma separated keywords attached to this Check.
     * The first keyword should be the name of the main product or
     * subsystem involved, e.g. SunCluster or Storage or SCGE.
     * Additional keywords may help identify other subject areas such as
     * RGM or HASP or 'test' or installtime.
     * Keyword choice is up to the check developer.
     *
     * @return a comma separated String of keywords attached to this Check.
     */
	public String getKeywords();

    /**
     * Return the version of this Check.
     * @return the version of this Check.
     */
    public String getVersion();


    /**
     * @return the last revision date of this Check as a string
     *
     */
    public String getRevisionDate();

    /**
     * Return a boolean indicating if this Check should be run. This method
     * may use data input as context for making this decision. Typical reasons
     * that a Check might not be applicable include hardware architecture: some
     * Checks might not apply to x86 platforms; or that some particular software
     * is not installed; or that the condition being tested for is not relevant
     * under particular versions of the operating system.
     * @return a boolean indicating if this Check should be run.
     * @throws CheckExecutionException
     * @throws FileNotFoundException
     */
    public boolean isApplicable() throws CheckExecutionException;

    /**
     * Return a String for display under verbose listing which summarizes the
     * logic of the isApplicable() method.
     * @return a String summarizing the isApplicable() logic
     */
    public String getApplicabilityLogic();

    /**
     * Performs the actual checking operation. Returns true if the check passes
     * or false if the check is violated. If false, then getAnalysis() and
     * getRecommendations() will return appropriate content.
     * @return a boolean indicating check passing or violation
     * @throws CheckExecutionException
     * @throws FileNotFoundException
     */
    public boolean doCheck() throws CheckExecutionException;

    /**
     * Return a String for display under verbose listing which summarizes the
     * logic of the doCheck() method.
     * @return a String summarizing the doCheck() logic
     */
    public String getCheckLogic();

    /**
     * A data parser or check may return a non-fatal message (example:
     * unparsable line in vfstab) which may be fetched by this method.
     * Returns null if no message.
     * @return a String holding non-fatal info or warning message,
     * or null if none
     */
    public String getInfoMessage();

    /**
     * Special case of informational message:
     * indicates that parser was unable to obtain
     * data from data source
     */
    public String getInsufficientDataMsg();

    /**
     * Special case of informational message: indicates that data src was
     * unable to provide data for some non-broken reason, typically that
     * an explorer archive is simply missing the required files.
     */
    public void setInsufficientDataMsg(String msg);

    /**
     * Get name or names of nodes against which check was run
     */
    public String getCheckedNodenames();

    /**
     * @return status of check result as printable string
     */
    public String getStatusName();

    /**
     * @return severity of check result as printable string
     */
    public String getSeverityName();

    public boolean isMultiNodeCheck();

	/**
	 * @return the most severe of two severities
	 */
    public int maxSeverity(int severity1, int severity2);


    public String toString();

} // interface
