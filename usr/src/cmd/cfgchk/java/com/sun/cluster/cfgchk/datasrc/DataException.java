/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)DataException.java	1.3	08/09/22 SMI"
 */
package com.sun.cluster.cfgchk.datasrc;

import com.sun.cluster.cfgchk.common.MultiMsgException;

/**
 * Thrown when data cannot be returned. Used very generally.
 */
public class DataException extends MultiMsgException {


	private static final long serialVersionUID = -6357426157032292224L;

	protected String exceptionType = "DataException";
		// needed for toString()

	public DataException() {
		super();
	} // ()


	public DataException(String s) {
		super(s);
	} // ()

	public DataException(Exception ex) {
		super(ex);
	} // ()

	public DataException(Throwable thr) {
		super(thr);
	} // ()

} // class
