/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)DirectoryListing.java	1.2	08/05/29 SMI"
 */
package com.sun.cluster.cfgchk.datasrc.fileObject;

import java.util.ArrayList;
import java.util.logging.Logger;

import com.sun.cluster.cfgchk.datasrc.DataException;
import com.sun.cluster.cfgchk.datasrc.fileObject.TextFile;
import com.sun.cluster.cfgchk.utils.logger.RemoteLogger;

/**
 * holds FileInfo objects  XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXx
 *
 * Serializable since it may be returned thru MBean.
 */
public class DirectoryListing implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private String dirName = "unassigned"; // no I18n
	private FileInfo[] entries = null;

	private static Logger logger = RemoteLogger.getRemoteLogger();


	public DirectoryListing(String dn) {
		logger.info("-- ENTER (str): dirname: " + dn);
		this.dirName = dn;
	} // ()

	public void putEntries(FileInfo[] fia) {
		logger.info("-- ENTER--");
		entries = fia;
	}

	public void putEntries(ArrayList fial) {
		logger.info("-- ENTER--");
		entries = new FileInfo[fial.size()];
		for (int i = 0; i < fial.size(); i++) {
			entries[i] = (FileInfo)fial.get(i);
		}
		logger.info("-- EXIT--");
	}

	public FileInfo[] getEntries() {
		logger.info(" -- ENTER --");
		if (entries != null) {
			logger.info("entries.length: " + entries.length);
			return entries;
		} else {
			logger.info("entries is null");
			return new FileInfo[0];
		}
	} // getEntries

	public boolean hasEntry(String fname) {
		if (entries == null) {
			return false;
		}
		logger.info(" -- ENTER --");
		FileInfo fi = findFI(fname);
		return (fi != null);
	} // hasEntry

	// return FileIfo or null if not found
	public FileInfo getFileInfo(String fname) {
		logger.info(" -- ENTER --");
		if (entries == null) {
			return null;
		}
		FileInfo fi = findFI(fname);
		return fi;
	} // getFileInfo

	public FileInfo[] getFilesInfo(String pattern) {
		logger.info(" -- ENTER --  NYI XXXXXXXXXXXXXX");
		return null;
	} // getFilesInfo

	// return FI or null
	private FileInfo findFI(String fname) {
		if (entries == null) {
			return null;  // early exit
		}
		FileInfo fi = null;
		for (int i = 0; i < entries.length; i++) {
			if (entries[i].getName().equals(fname)) {
				fi = entries[i];
				break;
			}
		}
		return fi;
	} // findFI

	public String toString() {
		String SEP = " ;|; ";
		StringBuffer sb = new StringBuffer(dirName + ": ");
		int maxToShow = 5;

		if (entries != null) {
			if (entries.length < maxToShow) {
				maxToShow = entries.length;
			}
			for (int i = 0; i < maxToShow; i++) {
				sb.append(SEP + entries[i]);
			}
		} else {
			sb.append("no entries"); // no I18n
		}
		return sb.toString();
	} // toString

} // class
