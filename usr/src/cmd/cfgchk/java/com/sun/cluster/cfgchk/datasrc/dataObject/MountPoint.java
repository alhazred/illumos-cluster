/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)MountPoint.java	1.2	08/05/29 SMI"
 */
package com.sun.cluster.cfgchk.datasrc.dataObject;

import java.io.File;
import java.util.logging.Logger;

/*
 * Convenience class for dealing with vfstab mount points.
 */
public class MountPoint {

	private String name = null;
	private String parent = null;
	private Logger logger = null;

	public MountPoint(String mp, Logger logger) {
		this.logger = logger;

		// strip trailing slash, if any ? XXX

		name = mp;
		File f = new File(mp);
		parent = f.getParent();
		logger.fine("name: " + name);
		logger.fine(" parent: " + parent);
	} // ()

	/**
	 * @return true if this mp is nested within other
	 */
	public boolean isNested(MountPoint otherMP) {
		boolean nested = false;
		logger.fine("   ===>>  this name: " + name +
		    "  otherMP: " + otherMP);
		if (otherMP.getName().equals(this.parent)) {
			nested = true;
			logger.fine("otherMP is my parent");
		}
		return nested;
	} // isNested

	public String getName() {
		return name;
	} // getName

	public String toString() {
		return name;
	} // toString

} // class

