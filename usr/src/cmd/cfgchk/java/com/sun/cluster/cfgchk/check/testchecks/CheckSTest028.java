/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)CheckSTest028.java	1.2	08/05/29 SMI"
 */
package com.sun.cluster.cfgchk.check.testchecks;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;


import com.sun.cluster.cfgchk.Check;
import com.sun.cluster.cfgchk.BasicCheck;
import com.sun.cluster.cfgchk.CheckExecutionException;

import com.sun.cluster.cfgchk.datasrc.DataException;
import com.sun.cluster.cfgchk.datasrc.fileObject.DirectoryListing;
import com.sun.cluster.cfgchk.datasrc.fileObject.TextFile;

import com.sun.cluster.cfgchk.utils.StringUtils;
import com.sun.cluster.cfgchk.utils.logger.LoggerUtils;


/**
 * Test data fetching methods that use the data cache.
 * Passes by default.
 * Severity CRITICAL.
 */

// No I18n

public class CheckSTest028 extends BasicCheck {

	private StringBuffer reportOutput = new StringBuffer(
		"Data gethering exercise:\n");

    public CheckSTest028() {
        checkID = "STest028";
        severity = Check.INFORMATION;

	// sccs keyword: must be 1.2 when checked out under sccs
	version = "1.2";
	// sccs keyword: must be 08/05/29 when checked out under sccs
	revisionDate = "09/12/08";

        keywords.add("SunCluster3.x");
	keywords.add("whitebox test");
        keywords.add("test");

	/* BEGIN JSTYLED */
        problemStatement = "Test check: Test data fetching methods that use the data cache. Violated in order to get output into report. See logs for caching evidence. Severity INFORMATION.";
	/* END JSTYLED */

    } // ()

    /**
     * @return a string describing the logic of the applicability test
     */
    protected String assignApplicabilityLogic() {
        String aLogic = "Always applies to Sun Cluster 3.x node.";
        return aLogic;
    } // assignApplicabilityLogic


    /**
     * @return a string describing the logic of the check
     */
    protected String assignCheckLogic() {
        String cLogic = "Exercise all the DataSrc methods and internal cache";
        return cLogic;
    } // assignCheckLogic


	/**
	 * @return a string for use in output report analyzing the problem
	 * may contain dynamic data
	 */
	public String getAnalysis() {
		return reportOutput.toString();
	} // getAnalysis

	public String getRecommendations() {
		return checkID;
	} // getRecommendations


    // always
    public boolean isApplicable() throws CheckExecutionException {
        logger.info("isApplicable() -- ENTER --");
        boolean applicable = true;

        return applicable;
    } // isApplicable

    public boolean doCheck() throws CheckExecutionException {

        logger.info("-- ENTER --");
        boolean passes = false; // in order to get output to report

	// fetch once then fetch again to test cache hit
	// see main traces in output log
	// see cache and classloader traces in remote log
	try {
		/* BEGIN JSTYLED */
		// Basic
		logger.info(" STest028 output  START ========================");

		// non-cached stuff
		String dataSrcName = getDataSrc().getDataSrcName();
		logger.info("getDataSrcName: " + dataSrcName);
		reportOutput.append("\t getDataSrcName: " + dataSrcName + "\n");

		String dataSrcVersion = getDataSrc().getDataSrcVersion();
		logger.info("getDataSrcVersion: " + dataSrcVersion);
		reportOutput.append("\t getDataSrcVersion: " + dataSrcVersion + "\n");

		boolean isLiveDataSource =  getDataSrc().isLiveDataSource();
		logger.info("isLiveDataSource:" + isLiveDataSource);
		reportOutput.append("\t isLiveDataSource: " + isLiveDataSource + "\n");

		String systemName = getDataSrc().getSystemName();
		logger.info("getSystemName: " + systemName);
		reportOutput.append("\t getSystemName: " + systemName + "\n");

		// lazily cached stuff
		String filename = "/etc/system";
		TextFile tf = getDataSrc().getTextFile(filename);
		         tf = getDataSrc().getTextFile(filename);
		logger.info("TextFile: " + tf.toString());
		reportOutput.append("\t getTextFile: " + filename + "\n");


		File f = getDataSrc().getFile(filename);
		     f = getDataSrc().getFile(filename);
		logger.info(filename + ": f.exists: " + f.exists());
		reportOutput.append("\t getFile: " + filename + "\n");

		String dirName = "/kernel";
		String flags = "-l";
		DirectoryListing dl = getDataSrc().getDirectoryListing(dirName, flags);
                                 dl = getDataSrc().getDirectoryListing(dirName, flags);
		logger.info("DirectoryListing: " + dl.toString());
		reportOutput.append("\t getDirectoryListing: " + dirName + "\n");

		String packageName = "SUNWcacaort";
		boolean b = getDataSrc().isPackageInstalled(packageName);
		        b = getDataSrc().isPackageInstalled(packageName);
		logger.info(packageName + " is installed: " + b);
		reportOutput.append("\t isPackageInstalled SUNWcacaort: " + b + "\n");

		      packageName = "VRTSvxvm";
		        b = getDataSrc().isPackageInstalled(packageName);
		        b = getDataSrc().isPackageInstalled(packageName);
		logger.info(packageName + " is installed: " + b);
		reportOutput.append("\t isPackageInstalled VRTSvxvm: " + b + "\n");

		      packageName = "SUNWnoSuchPackage";
		        b = getDataSrc().isPackageInstalled(packageName);
		        b = getDataSrc().isPackageInstalled(packageName);
		logger.info(packageName + " is installed: " + b);
		reportOutput.append("\t isPackageInstalled SUNWnoSuchPackage: " + b + "\n");

		// Solaris
		String arch = getDataSrc().getArchitecture();
		       arch = getDataSrc().getArchitecture();
		logger.info("arch: " + arch);
		reportOutput.append("\t getArchitecture: " + arch + "\n");

		int osMajor = getDataSrc().getOSVersionMajor();
		    osMajor = getDataSrc().getOSVersionMajor();
		logger.info("osMajor: " + osMajor);
		reportOutput.append("\t getOSVersionMajor: " + osMajor + "\n");

		// Cluster
		String nodeid = getDataSrc().getNodeID();
                       nodeid = getDataSrc().getNodeID();
		logger.info("nodeid: " + nodeid);
		reportOutput.append("\t getNodeID: " + nodeid + "\n");

		String clName = getDataSrc().getClustername();
                       clName = getDataSrc().getClustername();
		logger.info("clName: " + clName);
		reportOutput.append("\t getClustername: " + clName + "\n");

		String clVersion = getDataSrc().getClusterVersion();
	               clVersion = getDataSrc().getClusterVersion();
		logger.info("clVersion: " + clVersion);
		reportOutput.append("\t getClusterVersion: " + clVersion + "\n");

		boolean mode = getDataSrc().isClusterMode(/*usingCacao*/false);
		        mode = getDataSrc().isClusterMode(/*usingCacao*/false);
		logger.info("mode: " + mode);
		reportOutput.append("\t isClusterMode: " + mode + "\n");

		// Cluster: RGM
		reportOutput.append("\n\t ========== RGM =========\n");

		// all RT's
		String[] rtNames = getDataSrc().getRegisteredRTNames();
		         rtNames = getDataSrc().getRegisteredRTNames();
		logger.info("rtNames: " + StringUtils.stringArrayToString(rtNames, ";"));
		reportOutput.append("\t getRegisteredRTNames:\n\t\t" + StringUtils.stringArrayToString(rtNames, "\n\t\t") + "\n");

		// all RG's
		String[] rgNames = getDataSrc().getRGNames();
		         rgNames = getDataSrc().getRGNames();
		logger.info("rgNames: " +
		    StringUtils.stringArrayToString(rgNames, ";"));
		reportOutput.append("\t getRGNames:\n\t\t" +
		    StringUtils.stringArrayToString(rgNames, "\n\t\t") + "\n");

		// for each RG:
		//	nodelist
		//	RS list
		//	Rt for each RS
		for (int i = 0; i < rgNames.length; i++) {
			reportOutput.append("\n\t\t =====  details for RG: " +
			    rgNames[i] + "\n");

			// nodelist
			String[] nodelist = getDataSrc().getRGNodelist(rgNames[i]);
			         nodelist = getDataSrc().getRGNodelist(rgNames[i]);
			logger.info("nodelist: " +
			    StringUtils.stringArrayToString(nodelist, ";"));
			reportOutput.append("\t\t nodelist for: " + rgNames[i] + ": " + StringUtils.stringArrayToString(nodelist, ";") + "\n");

			// RS's
			String[] rsNames = getDataSrc().getRSNames(rgNames[i]);

			logger.info("rsNames for: " + rgNames[i] + ": " +
			    StringUtils.stringArrayToString(rsNames, ";"));
			reportOutput.append("\t\t rsNames for: " + rgNames[i] + ":\n\t\t\t" + StringUtils.stringArrayToString(rsNames, "\n\t\t\t") + "\n");

			// RT for RS
			for (int j = 0; j < rsNames.length; j++) {
				String rtName = getDataSrc().getRTName(rsNames[j], rgNames[i]);
				       rtName = getDataSrc().getRTName(rsNames[j], rgNames[i]);
				logger.info("rtName for: " + rgNames[i] + "/" +
				    rsNames[j] + ": " + rtName);
				reportOutput.append("\t\t\t rtName for: " + rgNames[i] + "/" +  rsNames[j] + ": " + rtName + "\n");
			} // rtName from RsName/rgName
		} // rgNames


		logger.info(" STest028 output  END ==========================");
		reportOutput.append("\t STest028 output  END ");
		/* END JSTYLED */

	} catch (DataException ex) {
		logger.severe("DataException: " + ex);
		LoggerUtils.exception("CheckSTest028.doCheck() DataException",
		    ex, logger);
		throw new CheckExecutionException(ex);
	} catch (FileNotFoundException ex) {
		logger.severe("FileNotFoundException: " + ex);
		LoggerUtils.exception("CheckSTest028.doCheck() " +
		    "FileNotFoundException", ex, logger);
		throw new CheckExecutionException(ex);
	} catch (IOException ex) {
		logger.severe("IOException: " + ex);
		LoggerUtils.exception("CheckSTest028.doCheck() " +
		    "IOException", ex, logger);
		throw new CheckExecutionException(ex);
	} catch (Exception ex) {
		logger.severe("Exception: " + ex);
		LoggerUtils.exception("CheckSTest028.doCheck() Exception",
		    ex, logger);
		throw new CheckExecutionException(ex);
	}

        logger.info("passes: " + passes);
        logger.info("-- EXIT --");
        return passes;
    } // doCheck


} // class
