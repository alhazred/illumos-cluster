/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)ExplorerBasicDataSrc.java	1.4	08/09/22 SMI"
 */
package com.sun.cluster.cfgchk.datasrc;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.logging.Logger;

import com.sun.cacao.invocation.InvocationStatus;


import com.sun.cluster.cfgchk.datasrc.fileObject.DirectoryListing;
import com.sun.cluster.cfgchk.datasrc.fileObject.FileInfo;
import com.sun.cluster.cfgchk.datasrc.fileObject.TextFile;
import com.sun.cluster.cfgchk.i18n.I18n;

import com.sun.cluster.cfgchk.utils.StringUtils;
import com.sun.cluster.cfgchk.utils.logger.LoggerUtils;


/**
 *
 * This is an implementation of interface BasicDataSrc which knows
 * how to access to system files and commands from an
 * explorer archive.
 *
 * This code is always executing on the same node
 * that launched the command. Cacao is never used
 * so there is no mbean interface needed.
 *
 * This implementation maintains a data cache.
 * Data is loaded lazily and usually cached when obtained.
 *
 * Explorer archives are all files: copies of files
 * and files of command output. This class and its
 * subclasses use the ExplorerMapping class to map
 * desired filenames and commands to archive filenames
 * which are then fetched and returned or data
 * extracted and returned as appropriate.
 *
 */
public class ExplorerBasicDataSrc implements BasicDataSrc {

	protected String dsName = null;
	protected String basedir = null;
		// path to top of unpacked explorer archive
	protected String systemName = null;
		// cheapie cache; needed b4 cache is created

	protected GenericCache dataCache = null;
 	protected String dsVersion = "1.0";
	protected Logger logger = null;
	// not remote since all is running on local node



	public ExplorerBasicDataSrc(String basedir, Logger logger) {
		this.logger = logger;
		logger.info("ExplorerBasicDataSrc() -- ENTER --");
		this.basedir = basedir;
		try {
			systemName = getSystemName();
		} catch (DataException dex) {
		    systemName = "unknown"; // no I18n
		}
		this.dsName = basedir;
		dataCache = new GenericCache(dsName);
		logger.info(dsName + ": basedir: " + basedir);
	}

	// ===  start interface BasicDataSrc  ===

	public String getDataSrcName() {
		return dsName;
	}
	public String getDataSrcVersion() {
		return dsVersion;
	}

	// explorer is not 'live'
	public boolean isLiveDataSource() {
		return false;
	}

	/*
	 * this method is called before cache is created
	 * value is 'cached' in a protected global var
	 * treat missing data as execution error, not as insufficient data
	 */
	public String getSystemName() throws DataException {
		logger.info("-- ENTER --");

		if (systemName != null) { 
			return systemName;
		} // early exit

		// not set: initialize
		String sysName = null;
		TextFile tf = null;
		try { // Exception
			try {
				String cmdName = "/usr/bin/uname -X";
				String mappedName =
				    ExplorerMapping.get(cmdName);
				if (mappedName == null) {
					String[] i18nArgs = { cmdName };
					/* BEGIN JSTYLED */
					String lMsg = I18n.getLocalized(logger,
					    "internal.error.no.explorer.mapping",
						i18nArgs);
					/* END JSTYLED */
					throw new DataException(lMsg);
				}
				String fullPath = basedir +  mappedName;
				logger.info("fullPath: " + fullPath);
				tf = new TextFile(fullPath); // FNFEx, IOEx
			} catch (IOException ioex) {
				String[] i18nArgs = { ioex.getMessage() };
				String lMsg = I18n.getLocalized(logger,
				    "internal.error.cant.read.explorerfile",
				    i18nArgs);
				throw new DataException(lMsg);
			}

			// systemName is second word from second line;
			// field separator is '='
			String line = tf.getLines()[1];
			logger.info("sysNameLine: " + line);
			StringTokenizer st = new StringTokenizer(line, "=");
			if (st.countTokens() < 2) {
				String eMsg = "systemNameLine: insufficient " +
				    "tokens"; // No I18n
				logger.info(eMsg);
				String[] i18nArgs = { eMsg };
				String lMsg = I18n.getLocalized(logger,
				    "internal.error.cmd.unexpected.result",
				    i18nArgs);
				throw new DataException(lMsg);
			}
			st.nextToken(); // skip first word
			sysName = st.nextToken(); // second word is version

		} catch (Exception ex) {
			logger.warning("Exception: " + ex);
			throw new DataException(ex);
		}

		logger.info("sysName: " + sysName);
		return sysName;
	} // getSystemName


	// may return empty, but never null, list
	public List getTextFileStrings(String filename, String target)
		throws DataException, FileNotFoundException, IOException {
		TextFile tf = getTextFile(filename, false);
		List list = tf.containsStrings(target);
		return list;
	} // getStreamingTextFile


	// See below interface methods for getTextFileDirect()
	// caller can interpret FileNotFound as execution error
	// or insufficient data, as it suits them
	public TextFile getTextFile(String filename)
		throws DataException, FileNotFoundException, IOException {
		return getTextFile(filename, true);
	}
	// can specify not to cache the file
	public TextFile getTextFile(String filename, boolean cache)
		throws DataException, FileNotFoundException, IOException {

		TextFile tf = null;
		String cacheKey = "getTextFile :-: " + filename;
		logger.info(dsName + ": -- ENTER --" + cacheKey);

		try {
			tf = dataCache.getTextFile(cacheKey);
			if (tf == null) {
				try {
					String mappedName =
					    ExplorerMapping.get(filename);
					if (mappedName == null) {
						String[] i18nArgs =
						    { filename };
						/* BEGIN JSTYLED */
						String lMsg =
						    I18n.getLocalized(logger,
							"internal.error.no.explorer.mapping",
							i18nArgs);
						/* END JSTYLED */
						throw new DataException(lMsg);
					}
					String fullPath = basedir +  mappedName;
					tf = new TextFile(fullPath);
						// FNFEx, IOEx
					logger.info(dsName +
					    ": made TextFile: " + filename);
					/* BEGIN JSTYLED */
					if (cache == true) {
						dataCache.put(cacheKey, tf);
						logger.info(dsName +
						    ": TextFile added to cache");
					} else {
						logger.info(dsName +
						    ": TextFile NOT added to cache");
					}
					/* END JSTYLED */
				}  catch (FileNotFoundException fnfex) {
					logger.warning(dsName +
					    ": FileNotFoundException: " +
					    fnfex);
					throw fnfex;
				} catch (IOException ioex) {
					logger.warning(dsName +
					    ": IOException: " + ioex);
					throw ioex;
				}
			} // == null
		} catch (DataException dex) { // already I18n; bubble it up
			logger.warning(dsName + ": DataException: " +
			    dex.getMessage());
			throw dex;
		} catch (Error err) {
			logger.warning(dsName + ": caught java.lang.Error: " +
			    err.getMessage());
			throw new DataException(err.getCause());
		} catch (Exception ex) {
			logger.warning(dsName + ": caught java.lang.Exception: " +
			    ex.getMessage());
			throw new DataException(ex);
		}
		logger.info(dsName + ": -- EXIT-- TextFile: " + filename);
		return tf;
	} // getTextFile


	// caller can interpret FileNotFound as execution error
	// or insufficient data, as it suits them
	public File getFile(String filename) throws DataException,
		FileNotFoundException {

		File f = null;
		String cacheKey = "getFile :-: " + filename;
		logger.info(dsName + ": -- ENTER --" + cacheKey);

		try {
			f = dataCache.getFile(cacheKey);
			if (f == null) {
				logger.info(dsName + ": not found in cache");
				try {
					String mappedName =
					    ExplorerMapping.get(filename);
					if (mappedName == null) {
						String[] i18nArgs =
						    { filename };
						/* BEGIN JSTYLED */
						String lMsg =
						    I18n.getLocalized(logger,
							"internal.error.no.explorer.mapping",
							i18nArgs);
						/* END JSTYLED */
						throw new DataException(lMsg);
					}
					String fullPath = basedir +  mappedName;
					logger.info(dsName + ": fullPath: " +
					    fullPath);
					f = new File(fullPath);
					if (!f.exists()) {
						String msg =
						    "file does not exist: " +
						    filename; // no I18n
						logger.warning(dsName + ": " +
						    msg);
						throw new
						    FileNotFoundException(msg);
					} else {
						logger.info(dsName +
						    ": got File: " + filename);

						dataCache.put(cacheKey, f);
						logger.info(dsName +
						    ": File added to cache");
					}
				} catch (IOException ioex) {
					String[] i18nArgs =
					    { ioex.getMessage() };
					/* BEGIN JSTYLED */
					String lMsg = I18n.getLocalized(logger,
					    "internal.error.cant.read.explorerfile",
					    i18nArgs);
					/* END JSTYLED */
					throw new DataException(lMsg);
				}
			} // == null
		} catch (DataException dex) { // already I18n; bubble it up
			logger.warning(dsName + ": DataException: " +
			    dex.getMessage());
			throw dex;
		}
		logger.info(dsName + ": -- EXIT-- File: " + filename);
		return f;
	} // getFile

	/*
	 * all Ex I18n
	 * caller can interpret FileNotFound as execution error
	 * or insufficient data, as it suits them
	 *
	 * flags arg never needed in this impl
	 * if dir doesn't exist or can't be read
	 * or is a file but not a directory then throw FNFEx
	 */
	public DirectoryListing getDirectoryListing(String dirName)
		throws DataException, FileNotFoundException, IOException {
		return getDirectoryListing(dirName, "");
	}

	public DirectoryListing getDirectoryListing(String dirName,
	    String flags)
		throws DataException, FileNotFoundException, IOException {

		logger.info(dsName + ": dirName: " + dirName);
		DirectoryListing dirListing = null;
		String cacheKey = "getDirectoryListing :-: " + dirName + "::" +
		    flags;

		try {
			dirListing = dataCache.getDirectoryListing(cacheKey);
			if (dirListing == null) {
				logger.info(dsName + ": not found in cache");
				try {

					dirListing =
					    new DirectoryListing(dirName);
						// no Ex
					logger.info(dsName + ": dirName: " +
					    dirName);

					String mappedName =
					    ExplorerMapping.get(dirName);
					logger.fine(dsName + ": mappedName: " +
					    mappedName);
					if (mappedName == null) {
						String[] i18nArgs =
						    { mappedName };
						/* BEGIN JSTYLED */
						String lMsg =
						    I18n.getLocalized(logger,
						    "internal.error.no.explorer.mapping",
						    i18nArgs);
						/* END JSTYLED */
						throw new DataException(lMsg);
					}


					if (mappedName.startsWith("DIR:")) {
						String fullPath = basedir +
						    mappedName.substring(4);
						logger.fine(dsName +
						    ": fullPath: " + fullPath);
						dirListing =
						    new DirectoryListing(
							    basedir +
							    mappedName);
					} else {
						String fullPath = basedir +
						    mappedName;
						TextFile tf =
						    new TextFile(fullPath);
						logger.fine(dsName +
						    ": fullPath: " + fullPath);
						ArrayList fial =
						    loadDirectoryListing(tf,
							flags, dirName);
						dirListing.putEntries(fial);
						logger.info(dsName +
						    ": fial.size(): " +
						    fial.size());
					}

					logger.info(dsName +
					    ": dirListing num lines: " +
					    dirListing.getEntries().length);
					dataCache.put(cacheKey, dirListing);

					logger.info(dsName +
					    ": DirectoryListing added 2 cache");

				} catch (FileNotFoundException ex) {
					logger.warning(dsName +
					    ": FileNotFoundException: " + ex);
					throw ex;
					// caller should interpret as
					// insufficent data, not as a
					//  crash or a violation
					// do I18n for completeness
				} catch (IOException ex) {
					logger.warning(dsName +
					    ": IOException: " + ex);
						// not I18n
					String[] i18nArgs = { ex.getMessage() };
					/* BEGIN JSTYLED */
					String lMsg = I18n.getLocalized(logger,
					    "internal.error.cant.read.systemfile",
					    i18nArgs);
					/* END JSTYLED */
					throw new DataException(lMsg);
				} catch (DataException ex) {
					logger.warning(dsName +
					    ": DataException: " + ex);
						// not I18n
					String[] i18nArgs = { ex.getMessage() };
					/* BEGIN JSTYLED */
					String lMsg = I18n.getLocalized(logger,
					    "internal.error.cant.read.systemfile",
					    i18nArgs);
					/* END JSTYLED */
					throw new DataException(lMsg);
				} catch (Exception ex) {
					logger.warning(dsName + ": Exception: " +
					    ex);
					LoggerUtils.exception("Exception", ex,
					    logger);

					String[] i18nArgs = { ex.getMessage() };
					/* BEGIN JSTYLED */
					String lMsg = I18n.getLocalized(logger,
					    "internal.error.cant.read.systemfile",
					    i18nArgs);
					/* END JSTYLED */
					throw new DataException(lMsg);
				}
			} // == null
		} catch (FileNotFoundException ex) {
			// already I18n; bubble it up
			logger.warning(dsName + ": FileNotFoundException: " +
			    ex.getMessage());
			throw ex;
		} catch (DataException dex) { // already I18n; bubble it up
			logger.warning(dsName + ": DataException: " +
			    dex.getMessage());
			throw dex;
		} catch (Exception ex) {
			logger.warning(dsName + ": Exception: " +
			    ex.getMessage());
			throw new DataException(ex);
		}
		logger.info(dsName + ": -- EXIT: dirListing found: " +
		    dirListing);
		return dirListing;
	} // getDirectoryListing


	public boolean isPackageInstalled(String packageName)
		throws DataException, InsufficientDataException {
		logger.info(dsName + ":  -- ENTER: " + packageName);
		boolean isInstalled = false;
		String cacheKey = "isPackageInstalled :-: " + packageName;

		try {
			Boolean b = dataCache.getBoolean(cacheKey);
			if (b == null) { // not in cache
				logger.info(dsName + ": not found in cache");
				TextFile tf = null;
				try { // Exception
					try { // get file from explorer archive
						String cmdName =
						    "/usr/bin/pkginfo -l";
						String mappedName = 
						    ExplorerMapping.get(
							    cmdName);
						if (mappedName == null) {
							String[] i18nArgs =
							    { cmdName };
							/* BEGIN JSTYLED */
							String lMsg =
							    I18n.getLocalized(
								    logger,
								    "internal.error.no.explorer.mapping",
								    i18nArgs);
							throw new DataException(lMsg);
							/* END JSTYLED */
						}

						String fullPath = basedir +
						    mappedName;
						logger.info(dsName +
						    ": fullPath: " + fullPath);
						tf = new TextFile(fullPath);
							// FNFEx, IOEx

					} catch (FileNotFoundException fnfex) {
						logger.warning(dsName +
						    ": FNFException: " + fnfex);
						throw new
						    InsufficientDataException(
							    fnfex);
					} catch (IOException ioex) {
						logger.warning(dsName +
						    ": IOException: " + ioex);
						String[] i18nArgs =
						    { ioex.getMessage() };
						/* BEGIN JSTYLED */
						String lMsg = I18n.getLocalized(
							logger,
							"internal.error.cant.read.explorerfile",
							i18nArgs);
						/* END JSTYLED */
						throw new DataException(lMsg);
					}

					// now look for line with target pkgname
					boolean found = false;
					String[] lines = tf.getLines();
					logger.info(dsName +
					    ": pkginfo-l num lines: " +
					    lines.length);

					for (int i = 0; i < lines.length; i++) {
						String line = lines[i];
						logger.finest(dsName +
						    ": pkginfoLine: " + line);

						StringTokenizer st =
						    new StringTokenizer(line,
							":");
						if (st.countTokens() != 2) {
							continue;
						}
						String label =
						    st.nextToken().trim();
							// trim() is vital!!
						String pkg =
						    st.nextToken().trim();
						logger.finest(dsName +
						    ": label: >" + label +
						    "< pkg: >" + pkg + "<");
						// first word must be PKGINST
						// second word is pkgname
						if (!label.equals("PKGINST")) {
							logger.finest(dsName +
							    ": not PKGINST");
							continue;
						}
						if (pkg.equals(packageName)) {
							logger.finest(dsName +
							    ": found " +
							    packageName);
							found = true;
							break;
						}
					} // for lines

					// not found is not an error case:
					// cache it!
					String msg = null;
					if (found) {
						dataCache.put(cacheKey,
						    new Boolean(true));
						msg = packageName +
						    " is installed on " +
						    systemName;
						isInstalled = true;
					} else {
						dataCache.put(cacheKey,
						    new Boolean(false));
						msg = packageName +
						    " is not installed on " +
						    systemName;
					}
					logger.info(dsName + ": " + msg);
					logger.info(dsName +
					    ": Boolean added to cache");

				} catch (Exception ex) {
					logger.warning(dsName +
					    ": Exception: " + ex);
					throw new DataException(ex);
				}

			} else {
				// got hit in cache: value can be true or false
				if (b.booleanValue() == true) {
					isInstalled = true;
				}
			}
		} catch (DataException dex) { // already I18n; bubble it up
			logger.warning(dsName + ": DataException: " +
			    dex.getMessage());
			throw dex;
		}
		logger.info(dsName + ": -- EXIT-- packageName: " +
		    packageName + "; isInstalled: " + isInstalled);
		return isInstalled;
	} // isPackageInstalled

	public int runCommandInt(String cmd[])
		throws DataException, InsufficientDataException {

		InvocationStatus exitStatus = runCommand(cmd); // dex
		int exitVal = exitStatus.getExitValue().intValue();

		logger.info(dsName + "exitVal: " + exitVal);
		return exitVal;
	}

	// result not cached
	public String[] runCommandStrArray(String cmd[])
		throws DataException, InsufficientDataException {
			logger.info(dsName + ": -- ENTER --");
			StringUtils.dumpStringArray(
				"ExplorerBasicDataSrc.runCommandStrArray() ",
					cmd, logger);

			String[] lines = null;

			// mapping key is <space> separated concatenation
			// of elts
			String cmdName =
			    StringUtils.encodedStringFromStringArray(logger,
				cmd, " ");
			logger.info(dsName + ": cmdName: " + cmdName);
			TextFile tf = null;
			try {
				String mappedName =
				    ExplorerMapping.get(cmdName);
				if (mappedName == null) {
					String[] i18nArgs = { cmdName };
					/* BEGIN JSTYLED */
					String lMsg = I18n.getLocalized(logger,
					    "internal.error.no.explorer.mapping",
					    i18nArgs);
					/* END JSTYLED */
					throw new DataException(lMsg);
				}
				String fullPath = basedir +  mappedName;
				logger.info(dsName + ": fullPath: " + fullPath);
				tf = new TextFile(fullPath); // FNFEx, IOEx
				lines = tf.getLines();

			// I18n not really needed: will bubble up to
			// logging only in Engine.handleCrashedCheck()
			} catch (FileNotFoundException ex) {
				logger.warning("FileNotFoundException: " +
				    ex.getMessage());
				throw new InsufficientDataException(ex);
			} catch (IOException ex) {
				String[] i18nArgs = { ex.getMessage() };
				String lMsg = I18n.getLocalized(logger,
				    "internal.error.cant.read.explorerfile",
				    i18nArgs);
				throw new DataException(lMsg);
			}

			if (lines != null) {
				logger.info(dsName + ": " + cmdName +
				    " num lines: " + lines.length);
			    } else {
				    logger.info(dsName + ": " + cmdName +
					" null return");
			    }

			return lines;
		} // runCommandStrArray

	/*
	 * attempt to find cmd in explorer archive
	 * figure out if it is a success execution or exited in error
	 * assemble and return an appropriate InvocationStatus object
	 */
	public InvocationStatus runCommand(String[] cmd)
		throws DataException, InsufficientDataException {

		/*
		 * explorer command output is in the form of
		 *	cmdname-flags.out
		 * and if there is error output it will be in
		 *	cmdname-flags.err
		 * If error file is present the stdout file will also
		 * be present but of size zero
		 *
		 * we don't know what the erro exit code is, just that
		 * it's non-zero
		 */

		InvocationStatus exitStatus = new InvocationStatus(cmd, null,
		    0, null, null, null);
		// now fill in some fields

		exitStatus.setArgv(cmd); // workaround cacao init bug: 6410164

		// query explorer arch for foo.out & foo.err
		// if .out but not .err then cmd returned 0
		String cmdName = cmd[0];
		String errName = cmdName + ".err";
		int exitVal = -1;

		logger.info(dsName + ": cmdName: " + cmdName + " ; errName: " +
		    errName);

		String mappedName = ExplorerMapping.get(cmdName);
		if (mappedName == null) {
			String[] i18nArgs = { cmdName };
			String lMsg = I18n.getLocalized(logger,
			    "internal.error.no.explorer.mapping",
			    i18nArgs);
			throw new DataException(lMsg);
		}
		String mappedErrName = ExplorerMapping.get(errName);
		if (mappedErrName == null) {
			String[] i18nArgs = { errName };
			String lMsg = I18n.getLocalized(logger,
			    "internal.error.no.explorer.mapping",
			    i18nArgs);
			throw new DataException(lMsg);
		}

		String fullPathCmd = basedir +  mappedName;
		logger.info(dsName + ": fullPathCmd: " + fullPathCmd);

		String fullPathErr = basedir +  mappedErrName;
		logger.info(dsName + ": fullPathErr: " + fullPathErr);

		File fOut = new File(fullPathCmd);
		boolean bfOut = fOut.exists();

		File fErr = new File(fullPathErr);
		boolean bfErr = fErr.exists();

		logger.info(dsName + ": bfOut: " + bfOut + " ; bfErr: " +
		    bfErr);

		TextFile tf = null;
		String[] lines = null;
		String output = null;

		try {
			if (bfOut && !bfErr) {
				logger.info("try to load " + fullPathCmd);
				exitStatus.setExitValue(0);
				exitVal = 0; // for logger
				// cmd had succeeded at explorer run time
				tf = new TextFile(fullPathCmd); // FNFEx, IOEx
				lines = tf.getLines();
				output = StringUtils.stringArrayToString(lines,
				    "\n");
				exitStatus.setStdout(output);
			} else if (!bfOut && bfErr) {
				logger.info("try to load " + fullPathErr);
				exitStatus.setExitValue(1);
				exitVal = 1; // for logger
				// cmd had failed at explorer run time
				// don't know actual exit code so just
				// use non-zero
				tf = new TextFile(fullPathErr); // FNFEx, IOEx
				lines = tf.getLines();
				output = StringUtils.stringArrayToString(lines,
				    "\n");
				exitStatus.setStderr(output);
			} else {
				logger.info("neither file present");
				throw new InsufficientDataException(
					"File not found in explorer archive: " +
						fullPathCmd);
					// no I18n
			}

		// I18n not really needed: will bubble up to
		// logging only in Engine.handleCrashedCheck()
		} catch (FileNotFoundException ex) {
			logger.warning("FileNotFoundException: " +
			    ex.getMessage());
			throw new InsufficientDataException(ex);
		} catch (IOException ex) {
			logger.warning("IOException: " + ex.getMessage());
				throw new DataException(ex);
		}

		logger.info(dsName + ": cmdName: " + cmdName + " ; exitVal: " +
		    exitVal);
		logger.finest(dsName + ": exitStatus: " + exitStatus);

		return exitStatus;
	} // runCommand


	public String dumpCache() {
		logger.fine(dsName + ": -- ENTER --");
		String dump = dataCache.toString();
		// caller will do logging of actual content
		logger.fine(dsName + ": -- EXIT --");
		return dump;
	} // dumpCache

	// nothing for this particular data src to do
	public void cleanup() {
	}

	public String toString() {
		return dsName + ": " + getDataSrcVersion();
	}


	// ===  end interface BasicDataSrc  ===

	// not member of interface
	// bypass ExplorerMapping
	protected TextFile getTextFileDirect(String filename)
		throws DataException, FileNotFoundException, IOException {

		TextFile tf = null;
		String cacheKey = "getTextFileDirect :-: " + filename;
		logger.info(dsName + ": -- ENTER --" + cacheKey);

		try {
			tf = dataCache.getTextFile(cacheKey);
			if (tf == null) {
				try {
					tf = new TextFile(filename);
						// FNFEx, IOEx
					logger.info(dsName +
					    ": made TextFile: " + filename);

					dataCache.put(cacheKey, tf);
					logger.info(dsName +
					    ": TextFile added to cache");
				}  catch (FileNotFoundException fnfex) {
					logger.warning(dsName +
					    ": FileNotFoundException: " +
					    fnfex);
					throw fnfex;
				} catch (IOException ioex) {
					logger.warning(dsName +
					    ": IOException: " + ioex);
					throw ioex;
				}
			} // == null
		} catch (DataException dex) { // already I18n; bubble it up
			logger.warning(dsName + ": DataException: " +
			    dex.getMessage());
			throw dex;
		}
		logger.info(dsName + ": -- EXIT-- TextFile: " + filename);
		return tf;
	} // getTextFileDirect

	private ArrayList loadDirectoryListing(TextFile tf, String flags,
	    String parentDir) throws DataException {
		ArrayList fia = null;
		if (flags.equals("-lLR")) {
			logger.info("calling loadLongRecursive()");
			fia = loadLongRecursive(tf, parentDir);
		} else if (flags.equals("-l")) {
			logger.info("calling loadLong()");
			fia = loadLong(tf, parentDir);
		} else {
			throw new DataException("unrecognized flags");
		}
		logger.info("-- EXIT num items: " + fia.size());
		return fia;
	} // loadDirectoryListing

	// 'Recursive' from /usr/bin/ls -R flag
	private ArrayList loadLongRecursive(TextFile tf, String parentDir) {
		logger.info("-- ENTER: " + tf.getFilename());
		return doLoadLong(2, tf, parentDir);
	}

	private ArrayList loadLong(TextFile tf, String parentDir) {
		logger.info("-- ENTER: " + tf.getFilename());
		return doLoadLong(1, tf, parentDir);
	}

	private ArrayList doLoadLong(int leadingSkips, TextFile tf,
	    String parentDir) {
		logger.info("-- ENTER: " + tf.getFilename());

		ArrayList fia = new ArrayList();
		String[] lines = tf.getLines();

		/* BEGIN JSTYLED */
		/*
		 * Skip first 'leadingSkips' lines.
		 * Then read lines until hit one with
		 * less than 9 fields. This means we've
		 * recursed into a subdir: stop because
		 * we're reading only top dir.
		 *
		 * 9 fields: (files, dirs)
		 *   permissions; num links; owner; group; size; mod month; mod day; mod year or hour; name
		 * 10 fields: (char special)
		 *   permissions; num links; owner; group; major; minor; mod month; mod day; mod year or hour; name
		 * 11 fields: (symlink)
		 *   same as 9 plus arrow; target
		 */
		/* END JSTYLED */
		for (int i = 2; i < lines.length; i++) {
			String line = lines[i];
			int nf = 0;
			try {
				nf = StringUtils.getNumFields(line);

			} catch (DataException ex) {
				logger.warning(
					"DataException counting num fields: " +
						ex);
				continue; // ignore this bogus line

			}
			if (nf < 9) break;
			try {
				fia.add(new FileInfo(line, parentDir));
				logger.info("num fields: " + nf +
				    " for line: " + line);
			} catch (DataException ex) {
				logger.warning("DataException: " + ex);
				// skip and continue
			}

		}

		logger.info("-- EXIT. num FileInfo's: " + fia.size());
		return fia;
	} // doLoadLong

} // class
