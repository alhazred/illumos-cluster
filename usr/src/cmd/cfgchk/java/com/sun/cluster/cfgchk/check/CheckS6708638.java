/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)CheckS6708638.java	1.2	08/06/02 SMI"
 */
package com.sun.cluster.cfgchk.check;

import java.util.StringTokenizer;

import com.sun.cluster.cfgchk.BasicCheck;
import com.sun.cluster.cfgchk.Check;
import com.sun.cluster.cfgchk.CheckExecutionException;
import com.sun.cluster.cfgchk.Globals;
import com.sun.cluster.cfgchk.datasrc.DataException;
import com.sun.cluster.cfgchk.utils.logger.LoggerUtils;

/**
 *
 * 6708638 - 1393 (Moderate)
 *
 * Single node check.
 * Node does has insufficient physical memory.
 */

/*
 * No I18n of any strings in this class.
 * Exceptions thrown from Checks are caught and handled by Engine
 * typically just by logging. Messages are never displayed to user.
 *
 * Lines with trailing 'TOC' comment are auto-gathered into
 * table of contents listing of all checks.
 */

public class CheckS6708638 extends BasicCheck {

	private int installedMemory = 0;
	private int minMemory = 512; // MB

	public CheckS6708638() {
		checkID = "S6708638"; // TOC
		severity = Check.MODERATE; // TOC

		// sccs keyword: must be 1.2 when checked out under sccs
		version = "1.2";
		// sccs keyword: must be 08/06/02 when checked out under sccs
		revisionDate = "09/12/08";

		keywords.add("SunCluster3");
		keywords.add("single");
		keywords.add("installtime"); // can't depend on clustermode

		/* BEGIN JSTYLED */
		problemStatement = "Node does has insufficient physical memory."; // TOC
		/* END JSTYLED */
	} // ()

	/**
	 * @return a string describing the logic of the applicability test
	 */
	protected String assignApplicabilityLogic() {
		String aLogic = "Always applies to Sun Cluster nodes.";
		return aLogic;
	} // assignApplicabilityLogic


	/**
	 * @return a string describing the logic of the check
	 */
	protected String assignCheckLogic() {
		/* BEGIN JSTYLED */
		String cLogic = "Get installed memory from third word of second line of '/usr/bin/prtconf -v'. If less than minimum required, the check is violated. Miminum requirement obtained from the auxillary data file: " + Globals.AUX_DATA_FILENAME + ", keyed to this check ID.";
		/* END JSTYLED */
		return cLogic;
	} // assignCheckLogic


	public String getAnalysis() {
		/* BEGIN JSTYLED */
		String analysis = "" + installedMemory +" MB of memory is installed on this node. Starting with 3.0, Sun Cluster requires a minimum of " + minMemory + " MB of physical memory in each node. The minimum physical memory resource requirements per node include: 128 MB for the Solaris Operating Environment + 128 MB for Sun Cluster. Additional memory required for various Data Services.";
		/* END JSTYLED */

		return analysis;
	} // getAnalysis

	public String getRecommendations() {
		/* BEGIN JSTYLED */
		String recommend = "Add enough memory to this node to bring its physical memory up to the minimum required level.";
		/* END JSTYLED */

		return recommend;
	} // getRecommendations

	// always applicable to SC3.x
	public boolean isApplicable() throws CheckExecutionException {
		logger.info("isApplicable() -- ENTER --");
		boolean applicable = true;
		return applicable;
	} // isApplicable


	public boolean doCheck() throws CheckExecutionException {
		logger.info("-- ENTER --");
		boolean passes = false;
		int dfltMinMem = 512; // MB
		String dfltMinMemStr = "" + dfltMinMem;
		try {
			// read minimum physical memory requirement
			// from aux data file
			String minMemoryStr =
			    getAuxDataValue(checkID + ".minMem");
			logger.info("minMemoryStr:  " + minMemoryStr);
			if (minMemoryStr == null) {
				minMemoryStr = dfltMinMemStr;
			}
			minMemory = dfltMinMem;

			try {
				Integer mm = Integer.decode(minMemoryStr);
				minMemory = mm.intValue();
			} catch (NumberFormatException e) {
				logger.info("Invalid value: " + minMemoryStr);
				// default to initted value
			}
			logger.info("minMemory:  " + minMemory);

			String[] cmd = { "/usr/sbin/prtconf",  "-v" };
			String[] stdout = getDataSrc().runCommandStrArray(cmd);
			String memLine = stdout[1]; // 2nd line
			logger.info("memLine: " + memLine);

			StringTokenizer st = new StringTokenizer(memLine);
			if (st.countTokens() == 4) {
				st.nextToken(); // skip 1st word
				st.nextToken(); // skip 2nd word
				String strValue = st.nextToken();
				logger.info("strValue: " + strValue);

				// is value < 512?
				try {
					Integer value =
					    Integer.decode(strValue);
					installedMemory = value.intValue();
					if (installedMemory < minMemory) {
						logger.info(
							"Value too small: " +
								value);
						passes = false;
					} else {
						logger.info(
							"Value good: " + value);
						passes = true;
					}
				} catch (NumberFormatException e) {
					logger.info("Invalid value: " +
					    strValue);
					passes = false;
				}
			} else {
				logger.info("Incorrect token count: " +
				    memLine);
			}

		} catch (DataException e) {
			logger.warning("DataException: " + e); // I18n XXX
			LoggerUtils.exception("DataException", e, logger);
			throw new CheckExecutionException(e);
		} catch (Exception e) {
			logger.warning("Exception: " + e);
			LoggerUtils.exception("Exception", e, logger);
			throw new CheckExecutionException(e);
		}

		return passes;
	} // doCheck

    } // class

