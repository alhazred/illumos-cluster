/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)Params.java	1.2	08/05/29 SMI"
 */
package com.sun.cluster.cfgchk.utils;


/*
 * Params bundles up run-time config information needed by the Engine
 * Has various methods to obtain some info at creation time
 * when data sources are not yet available.
 *
 * Exceptions thrown will contain I18n messages
 */

import java.util.logging.Logger;

import com.sun.cluster.cfgchk.Check;
import com.sun.cluster.cfgchk.Globals;
import com.sun.cluster.cfgchk.datasrc.DataException;

public class Params {
	private Logger logger = null;

	// user providable values
	public String includeCheckIDs = null;
	public String explorerNames = null;
	public String excludeCheckIds = null;
	public String checkJars = null;
	public String keywords = null;
	public String checklistName = null; // pathname
	public String nodeNames = null;
	public String outputDir = null;
	public String sortKeys = null;

	// minSeverity filter value defaults to most minimal
	public int minSeverity = Check.UNKNOWN_SEV;

	public boolean verbose = false;
	public boolean list = false;
	// version is handled out in ksh

	// program provided values
	public String UNDEFINED = "undefined";
	public boolean isInClusterMode = false;
	public boolean isClusterNode = false;
	public String loggingLevel = Globals.DEFAULT_LOGGING_LEVEL;
	// default logging level
	// may be overridden in /etc/default/cfgchk fir session logs

	public boolean usingCacao = false;
	/*
	 * specifies if we're running under cacao or
	 * invoked java Cfgchk directly from cli
	 * (controlled by setting in /etc/default/cfgchk)
	 * can be false only from cli; true from cacaoadm & Admin
	 *
	 * has side-effect of specifying internal direct invocations
	 * of commands rather than using cacao's invoker
	 */

	// allClusterNodeIdentifiers supports Cfgchk.processNodeNames()
	public String[][] allClusterNodeIdentifiers = null;
	public int numClusterNodes = 0;
	public boolean fromGUI = false;
	public String commandNode = null; // cmd launched from this node
	public String commandHostID = null; // `/usr/bin/hostid`



	public Params(Logger logger, String localNodename, boolean usingCacao,
	    boolean fromGUI)  throws DataException {
		this.usingCacao = usingCacao;	// false only from cli
						// true from Admin
		this.logger = logger;
		logger.info("-- ENTER  --");
		this.fromGUI = fromGUI;
		isClusterNode = ClusterUtils.isClusterNode(logger);
		logger.info("isClusterNode: " + isClusterNode);

		if (isClusterNode) {
			logger.info("will call isInClusterMode()");
			isInClusterMode = isInClusterMode(usingCacao);
			logger.info("back from isInClusterMode()");
		}
		logger.info("isInClusterMode: " + isInClusterMode);

		if (isInClusterMode) {
			try {
				allClusterNodeIdentifiers =
				    fetchAllClusterNodeIdentifiers();
			} catch (DataException dex) {
				logger.warning("DataException" + dex);

				// prepare ErrorMsg for display to user
				String[] i18nArgs = { dex.getMessage() };
				String lMsg = ErrorMsg.makeErrorMsg(logger,
				    "internal.error.fail.get.nodeids",
				    i18nArgs);
				throw new DataException(lMsg);
			}
		}

		commandNode = localNodename;
		try {
			commandHostID = ClusterUtils.getLocalHostID(logger);
		} catch (DataException dex) {
			// don't treat this one as fatal...
			logger.warning("DataException" + dex);
		}
		logger.info("Params() -- EXIT --");
	} // ()

	public String nodeIDtoName(String nodeID) {
		logger.info("-- ENTER: " + nodeID);
		String nm = null;
		for (int i = 0; i < numClusterNodes; i++) {
			if (allClusterNodeIdentifiers[i][0].equals(nodeID)) {
				nm = allClusterNodeIdentifiers[i][1];
				break;
			}
		}
		logger.info("-- EXIT: " + nm);
		return nm;
	} // nodeIDtoName

	public boolean isClusterNodeName(String candidate) {
		logger.info("-- ENTER: " + candidate);
		boolean isNodeName = false;
		for (int i = 0; i < numClusterNodes; i++) {
			if (allClusterNodeIdentifiers[i][1].equals(candidate)) {
				isNodeName = true;
				break;
			}
		}
		logger.info("-- EXIT: " + isNodeName);
		return isNodeName;
	} // isClusterNodeName

	/*
	 * return String [nodeid][nodename]
	 * does not presume contigious nodeids
	 * requires clustermode or will throw DataException
	 * DataException carries I18n message
	 */
	private String[][] fetchAllClusterNodeIdentifiers()
		throws DataException {
		logger.info(" -- ENTER --");
		String[][] allIDs = null;
		String[] allNodeIDs = ClusterUtils.getClusterNodeIDs(logger);
		numClusterNodes = allNodeIDs.length; // set global
		allIDs = new String[numClusterNodes][2];
		for (int i = 0; i < numClusterNodes; i++) {
			String nodeID = allNodeIDs[i];
			String nodename =
			    ClusterUtils.getNodenameFromNodeID(nodeID, logger);
			allIDs[i][0] = nodeID;
			allIDs[i][1] = nodename;
			logger.info("slot: " + i + "; nodeID: " + nodeID +
			    "; nodename: " + nodename);
		}



		logger.info(" -- EXIT --");
		return allIDs;
	} // fetchAllClusterNodeNames


	// localnode, not DataSrc
	private boolean isInClusterMode(boolean usingCacao) {
		logger.info(" -- ENTER --");
		boolean mode = false;
		try {
			mode = ClusterUtils.isInClusterMode(logger);
		} catch (DataException dex) {
			logger.warning("defaulting to clustermode == false: " +
			    "DataException: " + dex);
		}
		logger.info(" -- EXIT: " + mode);
		return mode;
	} // isInClusterMode

	public String toString() {
		String SEP = " | ";
		StringBuffer sb = new StringBuffer("Params: ");
		sb.append(SEP + "isClusterNode: " + isClusterNode);
		sb.append(SEP + "isInClusterMode: " + isInClusterMode);
		if (includeCheckIDs != null) {
			sb.append(SEP + "includeCheckIDs: " + includeCheckIDs);
		}
		if (explorerNames != null) {
			sb.append(SEP + "explorerNames: " + explorerNames);
		}
		if (excludeCheckIds != null) {
			sb.append(SEP + "excludeCheckIds: " + excludeCheckIds);
		}
		if (checkJars != null) {
			sb.append(SEP + "checkJars: " + checkJars);
		}
		if (keywords != null) {
			sb.append(SEP + "keywords: " + keywords);
		}
		if (checklistName != null) {
			sb.append(SEP + "checklistName: " + checklistName);
		}
		if (nodeNames != null) {
			sb.append(SEP + "nodeNames: " + nodeNames);
		}
		if (outputDir != null) {
			sb.append(SEP + "outputDir: " + outputDir);
		}
		if (sortKeys != null) {
			sb.append(SEP + "sortKeys: " + sortKeys);
		}
		sb.append(SEP + "minSeverity: " + minSeverity);
		sb.append(SEP + "verbose: " + verbose);
		sb.append(SEP + "list: " + list);

		return sb.toString();
	} // toString

} // class
