/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)CheckS6708605.java	1.2	08/06/02 SMI"
 */
package com.sun.cluster.cfgchk.check;

import java.io.FileNotFoundException;
import java.io.IOException;

import com.sun.cluster.cfgchk.BasicCheck;
import com.sun.cluster.cfgchk.Check;
import com.sun.cluster.cfgchk.CheckExecutionException;
import com.sun.cluster.cfgchk.datasrc.DataException;
import com.sun.cluster.cfgchk.datasrc.fileObject.DirectoryListing;
import com.sun.cluster.cfgchk.datasrc.fileObject.FileInfo;

/* BEGIN JSTYLED */
/**
 *
 * 6708605 - 3418 (Critical)
 *
 * Single node check.
 * The /dev/rmt directory is missing.
 *
 */
/* END JSTYLED */

/*
 * No I18n of any strings in this class.
 * Exceptions thrown from Checks are caught and handled by Engine
 * typically just by logging. Messages are never displayed to user.
 *
 * Lines with trailing 'TOC' comment are auto-gathered into
 * table of contents listing of all checks.
 */

public class CheckS6708605 extends BasicCheck {

	public CheckS6708605() {
		checkID = "S6708605"; // TOC
		severity = Check.CRITICAL; // TOC

		// sccs keyword: must be 1.2 when checked out under sccs
		version = "1.2";
		// sccs keyword: must be 08/06/02 when checked out under sccs
		revisionDate = "09/12/08";

		keywords.add("SunCluster3");
		keywords.add("single");
		keywords.add("scdidadm");
		keywords.add("installtime");  // can't depend on clustermode

		/* BEGIN JSTYLED */
		problemStatement = "The /dev/rmt directory is missing."; // TOC
		/* END JSTYLED */
	} // ()

	/**
	 * @return a string describing the logic of the applicability test
	 */
	protected String assignApplicabilityLogic() {
		String aLogic = "Always applies to Sun Cluster nodes.";
		return aLogic;
	} // assignApplicabilityLogic


	/**
	 * @return a string describing the logic of the check
	 */
	protected String assignCheckLogic() {
		String cLogic = "Check directory /dev for existence of subdir 'rmt'.";
		return cLogic;
	} // assignCheckLogic


	public String getAnalysis() {
		/* BEGIN JSTYLED */
		String analysis = "The /dev/rmt directory is missing on this Sun Cluster node. The current implementation of scdidadm(1M) relies on the existence of /dev/rmt to successfully execute 'scdidadm -r'. The /dev/rmt directory is created by Solaris regardless of the existence of the actual nderlying devices. The expectation is that the user will never delete this directory. During a reconfiguration reboot to add new disk devices, if /dev/rmt is missing scdidadm will not create the new devices and will exit with the following error: \n" +
		    "\'ERR in discover_paths : Cannot walk /dev/rmt\' \n" +
		    "The absence of /dev/rmt might prevent a failover to this node and result in a cluster outage. See BugIDs 4368956 and 4783135 for more details.";
		/* END JSTYLED */
		return analysis;
	} // getAnalysis

	public String getRecommendations() {
		/* BEGIN JSTYLED */
		String recommend = "Create the /dev/rmt directory on all nodes where it is missing and then run \"cldevice populate\" from one node. Permissions and ownership should be as in this example:" +
		    "\n\t\t drwxr-xr-x   2 root     sys          512 Oct 17  2007 /dev/rmt";
		/* END JSTYLED */
		return recommend;
	} // getRecommendations

	// always applicable to SC3.x
	public boolean isApplicable() throws CheckExecutionException {
		logger.info("isApplicable() -- ENTER --");
		boolean applicable = true;
		return applicable;
	} // isApplicable


	/*
	 * If directory /dev can't be read
	 * (typically because mising from explorer archive)
	 * exit with setInsufficientDataMsg().
	 * If /dev/can be read but dir rmt isn't found then
	 * we're violated.
	 */
	public boolean doCheck() throws CheckExecutionException {
		logger.info("-- ENTER --");
		boolean passes = false;
		String targetDir = "/dev";
		String targetFile = "rmt";
		try {
			DirectoryListing dirListing =
			    getDataSrc().getDirectoryListing(targetDir, "-lLR");
			logger.info("got dir listing");

			FileInfo fi = dirListing.getFileInfo(targetFile);
			if (fi != null) {
				logger.info("did find match for '" +
				    targetFile + "': " + fi);
				if (fi.isDirectory()) {
					logger.info("is directory");
					passes = true;
				} else {
					logger.info(
						"target is not a directory");
				}
			} else {
				logger.info("did not find match for '" +
				    targetFile + "'");
			}

		} catch (FileNotFoundException e) {
			logger.info("FileNotFoundException(): " +
			    e.getMessage());
			setInsufficientDataMsg(
				"directory listing unavailable for " +
					targetDir); // no I18n
		} catch (IOException e) {
			logger.info("IOException(): " + e);
			throw new CheckExecutionException(e);
		} catch (DataException e) {
			logger.info("DataException(): " + e);
			throw new CheckExecutionException(e);
		}catch (Exception e) {
			logger.info("Exception(): " + e);
			throw new CheckExecutionException(e);
		}

		return passes;
	} // doCheck

} // class
 
