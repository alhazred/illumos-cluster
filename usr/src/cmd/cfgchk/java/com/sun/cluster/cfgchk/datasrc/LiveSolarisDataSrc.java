/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)LiveSolarisDataSrc.java	1.3	08/06/02 SMI"
 */
package com.sun.cluster.cfgchk.datasrc;

import java.io.IOException;
import java.util.ArrayList;

import javax.management.ObjectName;

import com.sun.cacao.ObjectNameFactory;

import com.sun.cluster.cfgchk.datasrc.fileObject.TextFile;
import com.sun.cluster.cfgchk.datasrc.dataObject.VfstabEntry;
import com.sun.cluster.cfgchk.datasrc.fileObject.Vfstab;
import com.sun.cluster.cfgchk.i18n.I18n;

import com.sun.cluster.cfgchk.utils.Utils;
import com.sun.cluster.cfgchk.utils.logger.LoggerUtils;

/**
 *
 * This is an implementation of interface SolarisDataSrc
 * which knows how to access to system files and
 * commands on a running Solaris system.
 *
 * Code executed here is always executed on localhost but
 * it is not always the originating node.
 *
 * Data is made available transparently to checks via
 * mbean proxy of either this class or a superclass.
 *
 * Data cache in superclass.
 */
public class LiveSolarisDataSrc extends LiveBasicDataSrc
	implements SolarisDataSrc, LiveSolarisDataSrcMBean {

	private ObjectNameFactory onf = null;

	// Part of the contract with the ObjectNameFactory
	final public static String TYPE = "LiveSolarisDataSrc";

	// constructor for use as mbean
	public LiveSolarisDataSrc(ObjectNameFactory onf, String systemName) {
		super(onf, systemName);
		this.onf = onf;
	} // ()

	// used by localhost & non-cacao
	public LiveSolarisDataSrc(String systemName) {
		super(systemName);
		this.dsName = systemName;
		logger.info("dsName: " + dsName);
	} // ()


	// ===  start interface SolarisDataSrc  ===

        public String getArchitecture() throws DataException {

		String arch = null;
		String cacheKey = "getArchitecture";
		logger.info(dsName + ": -- ENTER --" + cacheKey);

		try {
			arch = dataCache.getString(cacheKey);
			if (arch == null) {
				logger.info(dsName + ": not found in cache");
				try {
					String[] cmd = { "/usr/bin/uname -p" };
					String[] sarry =
					    Utils.runCommandStrArray(cmd);
						// dex
					arch = sarry[0];
					dataCache.put(cacheKey, arch);
					logger.info(dsName +
					    ": String added to cache");

				} catch (DataException dex) {
					logger.info(dsName +
					    ": DataException: " + dex);
					String[] i18nArgs =
					    { dex.getMessage() };
					/* BEGIN JSTYLED */
					String lMsg =
					    I18n.getLocalized(logger,
						"internal.error.cmd.returns.error",
						i18nArgs);
					/* END JSTYLED */
					throw new DataException(lMsg);
				}
			} // == null

		} catch (DataException dex) { // already I18n; bubble it up
			logger.warning(dsName + ": DataException: " +
			    dex.getMessage());
			throw dex;
		}
		logger.info(dsName + ": arch: " + arch);
		return arch;
	} // getArchitecture


	// Ex are I18n
	public int getOSVersionMajor() throws DataException {

		Integer majorVersion = null;
		String cacheKey = "getOSVersionMajor";
		logger.info(dsName + ": -- ENTER --" + cacheKey);

		try {
			majorVersion = dataCache.getInteger(cacheKey);
			if (majorVersion == null) {
				logger.info(dsName + ": not found in cache");
				TextFile tf = null;

				// get textFile
				try {
					tf = new TextFile("/etc/release");
				} catch (IOException ioex) {
					logger.warning(dsName +
					    ": IOException: " + ioex);
					String[] i18nArgs =
					    { ioex.getMessage() };
					/* BEGIN JSTYLED */
					String lMsg =
					    I18n.getLocalized(logger,
						"internal.error.cant.read.systemfile",
						i18nArgs);
					/* END JSTYLED */
					throw new DataException(lMsg);
				}

				// get data
				majorVersion =
				    SolarisDataSrcCommon.getOSVersionMajor(tf);
				if (majorVersion != null) {
					dataCache.put(cacheKey, majorVersion);
					logger.info(dsName +
					    ": Integer added to cache");

				}
			} // == null
		} catch (DataException dex) { // already I18n; bubble it up
			logger.warning(dsName + ": DataException: " +
			    dex.getMessage());
			throw dex;
		}
		logger.info(dsName + ": majorVersion: " +
		    majorVersion.intValue());
		return majorVersion.intValue();
	} // getOSVersionMajor


	public Vfstab getVfstab(String nodename) throws DataException {
		Vfstab vfstab = null;
		String cacheKey = "getVfstab";
		logger.info(dsName + ": -- ENTER --" + cacheKey);

		try {
			vfstab = dataCache.getVfstab(cacheKey);
			if (vfstab == null) {
				logger.info(dsName + ": not found in cache");

					String filename = "/etc/vfstab";
				TextFile tf = null;
				// get TextFile
				try {
					tf = new TextFile(filename);
						// FNFEx, IOEx

					vfstab = SolarisDataSrcCommon.
					    loadVfstab(nodename, tf);

					dataCache.put(cacheKey, vfstab);
					logger.info(dsName +
					    ": Vfstab added to cache");

				} catch (IOException ioex) {
					String[] i18nArgs =
					    { ioex.getMessage() };
					/* BEGIN JSTYLED */
					String lMsg =
					    I18n.getLocalized(logger,
						"internal.error.cant.read.systemfile",
						i18nArgs);
					/* END JSTYLED */
					throw new DataException(lMsg);
				}
			} // == null

		} catch (DataException dex) { // already I18n; bubble it up
			logger.warning(dsName + ": DataException: " +
			    dex.getMessage());
			LoggerUtils.exception(
				"LiveSolarisDataSrc.getVfstab() DataException",
					dex, logger);
			throw dex;
		} catch (Exception ex) { // already I18n; bubble it up
			logger.warning(dsName + ": Exception: " +
			    ex.getMessage());
			LoggerUtils.exception(
				"LiveSolarisDataSrc.getVfstab() Exception",
					ex, logger);
			throw new DataException(ex);
		}
		logger.info(dsName + ": returning vfstab: " + vfstab);
		return vfstab;

	} // getVfstab

	// ===  end interface SolarisDataSrc  ===


	// mbean related methods

	/**
	 * create the object name for the given operation
	 * object name (this method is called when the mbean for
	 * this object is registered with the MBean server).
	 *
	 * @return the objectName to use
	 */
	public ObjectName createON() {
		logger.info(dsName + ": [=LiveSolarisDataSrc] createON");

		ObjectName on = onf.getObjectName(LiveSolarisDataSrcMBean.class,
		    null);
		return on;
	} // createON

	public void start() {
		logger.fine(dsName + ": starting LiveSolarisDataSrc");
	}

	public void stop() {
		logger.fine(dsName + ": stopping LiveSolarisDataSrc");
	}

} // class
