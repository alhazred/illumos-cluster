/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)Administrator.java	1.5	08/09/22 SMI"
 */
package com.sun.cluster.cfgchk.agent.cfgchk;


// JDK
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.Map;
import java.util.logging.Logger;

import javax.management.ObjectName;

import com.sun.cacao.ObjectNameFactory;
import com.sun.cacao.commandstream.Command;
import com.sun.cacao.commandstream.builtin.SetEncoding;
import com.sun.cluster.cfgchk.Cfgchk;
import com.sun.cluster.cfgchk.Globals;
import com.sun.cluster.cfgchk.agent.cfgchk.results.CommandResult;
import com.sun.cluster.cfgchk.agent.cfgchk.results.ErrorResult;
import com.sun.cluster.cfgchk.agent.cfgchk.results.ExitCode;
import com.sun.cluster.cfgchk.utils.ErrorMsg;
import com.sun.cluster.cfgchk.utils.logger.LoggerUtils;
import com.sun.cluster.cfgchk.utils.logger.RemoteLogger;

/**
 * Service entry point implementation class.
 *
 * It is a singleton of such class that is registered as
 * <code>AdministratorMBean</code> MBean.
 */

public class Administrator implements AdministratorMBean, Command {

	/*
	 * MBean interface implementation
	 */

	// Logger object : logs to cacao logs

	private static Logger cLogger =
	Logger.getLogger(
	    "com.sun.cluster.cfgchk.agent.cfgchk.Administrator");

	private static Logger rLogger = RemoteLogger.getRemoteLogger();

	// Part of the contract with the ObjectNameFactory
	final public static String TYPE = "Administrator";

	private ObjectNameFactory onf = null;
	/**
	 * Constructor
	 *
	 * @param properties
	 */
	protected Administrator(ObjectNameFactory onf) {
		this.onf = onf;
		cLogger.info("Administrator created");
		rLogger.info("Administrator created");

		// say hello to the remote logger: demo what's being logged
		rLogger.severe("Admin () logging severe messages");
		rLogger.warning("Admin() logging warning messages");
		rLogger.info("Admin() logging info messages");
		rLogger.config("Admin() logging config messages");
		rLogger.fine("Admin() logging fine messages");
		rLogger.finest("Admin() logging finest messages");

	} // ()

	// CommandStream entry point called from cli
	public int execute(String encodedArgs, InputStream in,
	    OutputStream out, OutputStream err, Map env)
		throws Exception {

		String encoding = (String)env.get(
			SetEncoding.ENV_ENCODING_KEY);
		int exitValue = ExitCode.ERR_INTERNAL;

		// setup output streams; autoflush
		PrintWriter stdout = new PrintWriter(
		    new OutputStreamWriter(out, encoding), true);
		PrintWriter stderr =  new PrintWriter(
		    new OutputStreamWriter(err, encoding), true);

		CommandResult cr = null;

		try {
			rLogger.info("Admin.execute() encodedArgs: " +
			    encodedArgs);

			boolean fromGui = false;
			cr = runEngine(encodedArgs, stdout, stderr, fromGui);

			rLogger.info("cr: " + cr);

			// Exit code: Note that non-0 exitValue is not
			// necessarily an error
			exitValue = cr.getExitCode();
			String lMsg = cr.getMessage();
			rLogger.info("exitValue: " + exitValue);
			rLogger.info("cr.getMessage(): " + lMsg);

			if (exitValue >= ExitCode.ERR_ERR) {
				// already I18n as ErrorMsg
				stderr.println(lMsg); // show to user
			}

		} catch (Exception ex) {
			rLogger.warning("Exception: " + ex);
			LoggerUtils.exception("Administrator.execute()",
			    ex, rLogger);
			String[] i18nArgs = { ex.getMessage() };
			String lMsg = ErrorMsg.makeErrorMsg(
				rLogger, "internal.error", i18nArgs);
			stderr.println(lMsg);
			exitValue = ExitCode.ERR_INTERNAL;
		} finally {
			try {
				stdout.close();
				stderr.close();
			} catch (Exception ex) {
				rLogger.warning("Exception closing " +
				    "PrintWriters: " + ex);
			}
		}
		rLogger.info("EXIT exitValue: " + exitValue);
		cLogger.fine("EXIT exitValue: " + exitValue);

		cr = null;
		return exitValue;
	} // execute

	// called direct by gui: entry here means using cacao
	public CommandResult runEngine(String encodedArgs, PrintWriter stdout,
	    PrintWriter stderr, boolean fromGui) {
		rLogger.info("-- ENTER --");

		CommandResult cr = null;
		boolean usingCacao = true;
		String msg = null;
		try {
			rLogger.info("create Cfgchk");
			Cfgchk cfgchk  = new Cfgchk(encodedArgs, stdout,
			    stderr, usingCacao, fromGui);

			rLogger.info("fetch cr");
			cr = cfgchk.getCommandResult();
			cfgchk = null;

		} catch (Exception ex) {
			rLogger.warning("Exception: " + ex);
			LoggerUtils.exception("Administrator.runEngine(): ",
			    ex, rLogger);
			msg = ex.getMessage();
			cr = new ErrorResult(ExitCode.ERR_INTERNAL, msg);
		}

		rLogger.info("-- EXIT: " + cr);

		return cr;
	} // runEngine

	/*
	 * Other public methods and fields
	 */

	public String getVersion() {
		return Globals.CHKCFG_VERSION;
	} // getVersion


	public void start() {
		rLogger.info("starting Administrator");
	}

	public void stop() {
		rLogger.info("stopping Administrator");
	}

	/**
	 * create the object name for the given operation
	 * object name (this method is called when the mbean for
	 * this object is registered with the MBean server).
	 *
	 * @return the objectName to use
	 */
	public ObjectName createON() {
		rLogger.fine("[=Administrator] createON");

		ObjectName on = onf.getObjectName(AdministratorMBean.class,
		    null);
		return on;
	} // createON

} // class
