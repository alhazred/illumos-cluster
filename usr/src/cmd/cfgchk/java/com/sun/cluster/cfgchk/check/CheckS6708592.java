/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)CheckS6708592.java	1.2	08/06/02 SMI"
 */
package com.sun.cluster.cfgchk.check;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.regex.Matcher;

import com.sun.cluster.cfgchk.BasicCheck;
import com.sun.cluster.cfgchk.Check;
import com.sun.cluster.cfgchk.CheckExecutionException;
import com.sun.cluster.cfgchk.datasrc.fileObject.TextFile;
import com.sun.cluster.cfgchk.datasrc.DataException;
import com.sun.cluster.cfgchk.utils.logger.LoggerUtils;


/* BEGIN JSTYLED */

/**
 *
 * 6708592 - 1220 (Critical)
 *
 * Single node check.
 * The nsswitch.conf file "group" database entry does not have "files" first.
 */
/* END JSTYLED */

/*
 * No I18n of any strings in this class.
 * Exceptions thrown from Checks are caught and handled by Engine
 * typically just by logging. Messages are never displayed to user.
 *
 * Lines with trailing 'TOC' comment are auto-gathered into
 * table of contents listing of all checks.
 */

public class CheckS6708592 extends BasicCheck {

	public CheckS6708592() {
		checkID = "S6708592"; // TOC
		severity = Check.CRITICAL; // TOC

		// sccs keyword: must be 1.2 when checked out under sccs
		version = "1.2";
		// sccs keyword: must be 08/06/02 when checked out under sccs
		revisionDate = "09/12/08";

		keywords.add("SunCluster3"); // requirement imposed by SC3.x
		keywords.add("single");
		keywords.add("nsswitch.conf");

		/* BEGIN JSTYLED */
		problemStatement = "The nsswitch.conf file \"group\" database entry does not have \"files\" first."; // TOC
		/* END JSTYLED */
	} // ()

	/**
	 * @return a string describing the logic of the applicability test
	 */
	protected String assignApplicabilityLogic() {
		/* BEGIN JSTYLED */
		String aLogic = "Always applies to Sun Cluster nodes.";
		/* END JSTYLED */
		return aLogic;
	} // assignApplicabilityLogic


	/**
	 * @return a string describing the logic of the check
	 */
	protected String assignCheckLogic() {
		/* BEGIN JSTYLED */
		String cLogic = "Test /etc/nsswitch.conf for presence of 'group:' line with specified format. 'files' must be listed first, optionally followed by other specifiers.";
		/* END JSTYLED */
		return cLogic;
	} // assignCheckLogic

	public String getAnalysis() {
		/* BEGIN JSTYLED */
		StringBuffer analysis = new StringBuffer("");
		analysis.append("The \"group\" database entry in the nsswitch.conf file is missing or does not have \"files\" listed first. The nsswitch.conf file controls which data source the system will attempt to use to resolve a group name. Improper configuration of this file might prevent Sun Cluster daemons from communicating properly.");
		/* END JSTYLED */
		return analysis.toString();
	} // getAnalysis

	public String getRecommendations() {
		/* BEGIN JSTYLED */
		String recommend = "Edit the /etc/nsswitch.conf file and configure \"files\" as first entry for the \"group\" database entry.\n\t\tExample:\n\tgroup: files files nis\n\tNOTE: nis is not required.\n\tThis is the generic recommendation for the group entry. Some dataservices might require a different format and are included in separate checks.";
		/* END JSTYLED */
		return recommend;
	} // getRecommendations


	// always applicable
	public boolean isApplicable() throws CheckExecutionException {
		logger.info("isApplicable() -- ENTER --");
		boolean applicable = true;
		return applicable;
	} // isApplicable

	public boolean doCheck() throws CheckExecutionException {

		logger.info("-- ENTER --");
		boolean passes = false;

		try {
			/*
			 * load /etc/nsswitch.conf
			 * missing file is a violation
			 * seek "^group:" line
			 * missing line is a violation
			 * if group: line does not match the pattern
			 * specified below
			 * then check is violated
			 */
			String fname = "/etc/nsswitch.conf";
			/*
			 * Pattern:
			 * match a line that starts with (^) 'group:'
			 * followed by at least one whitespace char
			 * followed by the word 'files' (word boundary)
			 * followed by zero or more of any character
			 * to end of line
			 * no need to retain any capture groups so group
			 *	definitions start with '?:'
			 */
			String pattern = "(?:^group:)(?:\\s+)files\\b(?:.*$)";
			logger.info("-- ENTER 1 --");
			TextFile tf = getDataSrc().getTextFile(fname);
			logger.info("-- ENTER 2 --");
			if (tf == null) {
				logger.warning("create & throw " +
				    "FileNotFoundException");
				throw(new FileNotFoundException(
				    "Null TextFile for " + fname));
			} else {
				logger.finest("Got non-null tf for " + fname);

				logger.finest("lines dump:");
				String[] lines = tf.getLines();
				/* BEGIN JSTYLED */
				for (int i = 0; i < lines.length; i++) {
					logger.finest("lines["+i+"]: >" + lines[i] + "<");
					Matcher rem = TextFile.matches(lines[i],
					    pattern);
					if (rem != null) {
						logger.finest("match!!");
						passes = true;
						for (int j = 0;
						     j < rem.groupCount();
						     j++) {
							logger.info("group " + j +
							    ": >" + rem.group(j) + "<");
						}
					}

				}
				/* END JSTYLED */
			}
			logger.finest("lines dump done");

		} catch (FileNotFoundException e) {
			logger.warning("FileNotFoundException: " + 
			    e.getMessage());
			setInsufficientDataMsg(e.getMessage());
			// should be violation with info via analysis XXX
			passes = false;
		} catch (IOException e) {
			logger.warning("IOException: " + e.getMessage());
			LoggerUtils.exception("IOException", e, logger);
			throw new CheckExecutionException(e);
		} catch (DataException e) {
			logger.info("DataException(): " + e.getMessage());
			throw new CheckExecutionException(e);
		} catch (Exception e) {
			logger.warning("Exception: " + e.getMessage());
			LoggerUtils.exception("Exception", e, logger);
			throw new CheckExecutionException(e);
		}

		logger.info("passes: " + passes);
		return passes;
	} // doCheck


} // class

