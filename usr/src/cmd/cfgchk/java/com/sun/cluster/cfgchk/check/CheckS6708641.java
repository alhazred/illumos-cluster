/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)CheckS6708641.java	1.2	08/06/02 SMI"
 */
package com.sun.cluster.cfgchk.check;

import com.sun.cluster.cfgchk.BasicCheck;
import com.sun.cluster.cfgchk.Check;
import com.sun.cluster.cfgchk.CheckExecutionException;
import com.sun.cluster.cfgchk.datasrc.DataException;
import com.sun.cluster.cfgchk.datasrc.InsufficientDataException;
import com.sun.cluster.cfgchk.utils.logger.LoggerUtils;

/**
 *
 * 6708641 - 1709 - Cluster failover/switchover might fail because rpcbind is not running.
 *
 */

/*
 * No I18n of any strings in this class.
 * Exceptions thrown from Checks are caught and handled by Engine
 * typically just by logging. Messages are never displayed to user.
 *
 * Lines with trailing 'TOC' comment are auto-gathered into
 * table of contents listing of all checks.
 */

public class CheckS6708641 extends BasicCheck {

	public CheckS6708641() {
		checkID = "S6708641"; // TOC
		severity = Check.CRITICAL; // TOC

		// sccs keyword: must be 1.2 when checked out under sccs
		version = "1.2";
		// sccs keyword: must be 08/06/02 when checked out under sccs
		revisionDate = "09/12/08";

		keywords.add("SunCluster3");
		keywords.add("single");
		keywords.add("rpcbind");

		/* BEGIN JSTYLED */
		problemStatement = "Cluster failover/switchover might fail because rpcbind is not running."; // TOC
		/* END JSTYLED */


	} // ()

	/**
	 * @return a string describing the logic of the applicability test
	 */
	protected String assignApplicabilityLogic() {
		String aLogic = "Always applies to Sun Cluster nodes.";
		return aLogic;
	} // assignApplicabilityLogic


	/**
	 * @return a string describing the logic of the check
	 */
	protected String assignCheckLogic() {
		/* BEGIN JSTYLED */
		String cLogic = "Execute rpcinfo. If unable to contact rpcbind, the check fails.";
		/* END JSTYLED */
		return cLogic;
	} // assignCheckLogic


	public String getAnalysis() {
		StringBuffer analysis = new StringBuffer("");
		/* BEGIN JSTYLED */
		analysis.append("rpcbind is not running on this node.  Consequently, a failover/switchover to this node would not be possible as none of the services dependent on rpcbind will be started.  A number of programs are dependent on rpc services.  None of the resource groups can be brought online on this node without rpcbind.");
		analysis.append("\n\n");
		analysis.append("The following scenarios might cause rpcbind to not be running:\n");
		analysis.append("\t\t- rpcbind might have been killed.\n");
		analysis.append("\t\t- rpcbind would have exited due to some internal error.");
		/* END JSTYLED */

		return analysis.toString();
	} // getAnalysis

	public String getRecommendations() {
		/* BEGIN JSTYLED */
		String[] recommendArray = {
			"rpcbind must be restarted on this node before attempting a failover/switchover. However,  anything that was registered with rpcbind is lost when rpcbind is restarted, so it has no idea what was registered with it before it stopped running.",
			"",
			"Check to see if the following files are present:",
			"- /tmp/portmap.file",
			"- /tmp/rpcbind.file",
			"",
			"If so, try restarting rpcbind with -w option.",
			"- /usr/sbin/rpcbind -w",
			"",
			"If the files are not present, the safest way to restart rpcbind is to bring the node out of cluster and reboot it.",
			"",
			"NOTE:  Before rebooting a node, it is always best to move any resource/device groups on that node.  The exact scswitch options used will depend on the cluster configuration.",
			"",
			"a)  Move Resource Group(s) (RG) between nodes",
			"\tscswitch -z -g nfs-rg -h dest node",
			"b)  Move all RGs and Device Groups (DGs)",
			"\tscswitch -S -h source host",
			"",
			"scshutdown is recommended for shutting down an entire cluster.",
			"- init 0 or shutdown -y -g0 -i0",
			"- ok boot",
			"",
			"OR",
			"- init 6 or shutdown -y -g0 -i6",
			"",
			"The node should rejoin the cluster on reboot."
		};
		/* END JSTYLED */

		StringBuffer sb = new StringBuffer("");
		for (int i = 0; i < recommendArray.length; i++) {
			if (i > 0) { sb.append("\t\t"); }
			sb.append(recommendArray[i] + "\n");
		}

		return sb.toString();
	} // getRecommendations


	// always
	public boolean isApplicable() throws CheckExecutionException {
		logger.info("isApplicable() -- ENTER --");
		boolean applicable = true;

		return applicable;
	} // isApplicable

	public boolean doCheck() throws CheckExecutionException {
		/* BEGIN JSTYLED */
		/*
		 * Execute rpcinfo. If command returns 1 this check fails.
		 * In explorer, netinfo/rpcinfo.out will be 0-length
		 * and netinfo/rpcinfo.err will have contents:
		 * "rpcinfo: can't contact rpcbind: RPC: Rpcbind failure - RPC: Failed (unspecified error)"
		 */
		/* END JSTYLED */

		logger.info("-- ENTER --");
		boolean passes = true;

		try {
			String[] cmd = { "/usr/bin/rpcinfo" };
			int exitCode = getDataSrc().runCommandInt(cmd); // dex
			logger.info("exitCode: " + exitCode);

			/*
			 * note: some commands may have valid exit codes
			 * greater than 1, but rpcinfo does not
			 */
			if (exitCode > 1) {
				logger.info("failed to execute command: " +
				    cmd[0]);
				throw new CheckExecutionException(
					"Unable to execute " + cmd[0]);
			}
			if (exitCode != 0) {
				passes = false;
				logger.info("non-zero return code: " +
				    exitCode);
			}

		} catch (InsufficientDataException e) {
			logger.warning("InsufficientDataException: " +
			    e.getMessage());
			passes = false;
			setInsufficientDataMsg(e.getMessage());
		} catch (DataException e) {
			logger.warning("DataException: " + e.getMessage());
			LoggerUtils.exception("DataException", e, logger);
			throw new CheckExecutionException(e);
		} catch (Exception e) {
			logger.warning("Exception: " + e.getMessage());
			LoggerUtils.exception("Exception", e, logger);
			throw new CheckExecutionException(e);
		}

		logger.info("passes: " + passes);
		logger.info("-- EXIT --");
		return passes;
	} // doCheck

} // class

