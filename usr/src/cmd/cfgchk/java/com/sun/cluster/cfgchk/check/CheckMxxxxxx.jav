/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/* 
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)CheckMxxxxxx.jav	1.3	08/06/02 SMI"
 */
package com.sun.cluster.cfgchk.check;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;


import com.sun.cluster.cfgchk.Check;
import com.sun.cluster.cfgchk.BasicCheck;
import com.sun.cluster.cfgchk.BasicCheckMultiNode;
import com.sun.cluster.cfgchk.CheckExecutionException;
import com.sun.cluster.cfgchk.datasrc.fileObject.TextFile;


/**
 *
 * Mxxxxxx (Severity)            Demonstration check not supported by a CR.
 * 
 * Multi node check.
 * Problem Statement: One or more hostnames configured in a Logicalhostname
 * resource is not present in /etc/hosts on one or nmore nodes.
 * 
 */

/*
 * No I18n of any strings in this class.
 * Exceptions thrown from Checks are caught and handled by Engine
 * typically just by logging. Messages are never displayed to user.
 *
 * Lines with trailing 'TOC' comment are auto-gathered into
 * table of contents listing of all checks.
 */

public class CheckMxxxxxx extends BasicCheckMultiNode {
    
    private ArrayList missingEntries = null;

    
    public CheckMxxxxxx() {
        checkID = "Mxxxxxx"; // must match classname and original defining CR // TOC
        severity = Check.MODERATE; // TOC

	// sccs keyword: must be 1.3 when checked out under sccs
        version = "1.3";
	// sccs keyword: must be 08/06/02 when checked out under sccs
        revisionDate = "09/12/08";
        
        keywords.add("SunCluster3");
	keywords.add("multi");
        keywords.add("resource");
        keywords.add("logicalhostname");
        
	/* BEGIN JSTYLED */
        problemStatement = "Sample multi-node check Problem Statement"; // TOC
	/* END JSTYLED */

        // check-specific data
        missingEntries = new ArrayList();

    } // ()

    /**
     * @return a string describing the logic of the applicability test
     */
    protected String assignApplicabilityLogic() {
	String aLogic = "Always applies to Sun Cluster nodes.";
        return aLogic; 
    } // assignApplicabilityLogic
    

    /**
     * @return a string describing the logic of the check
     */
    protected String assignCheckLogic() {
	/* BEGIN JSTYLED */
        String cLogic = "For each hostname in each logicalhostname resource, check /etc/hosts on each node to see if the hostname is present. If any hostname is not present on any node, this check is violated.";
	/* END JSTYLED */
        return cLogic; 
    } // assignCheckLogic
    

    /**
     * @return a string for use in output report analyzing the problem
     * may contain dynamic data
     */
     public String getAnalysis() {
     
         StringBuffer analysis = new StringBuffer("");

	/* BEGIN JSTYLED */
         if (missingEntries.size() > 0) {
             analysis.append("The following hostnames are configured into Logicalhostname resources but are missing from /etc/hosts on one or more nodes:\n");
             
             Iterator it = missingEntries.iterator();
             String data = null;
             while (it.hasNext()) {
                 data = (String)it.next();
                 analysis.append("\t" + data + "\n");
             }
	/* END JSTYLED */
         }
          
        return analysis.toString();
    } // getAnalysis


    /**
     * @return a string for use in output report giving recommended fix
     */
    public String getRecommendations() {
	/* BEGIN JSTYLED */
        String recommend = "Enter the missing hostnames into /etc/hosts on the appropriate nodes.";
	/* END JSTYLED */

        return recommend;
    } // getRecommendations

    /*
     * always applicable to a 3.x cluster node
     * no other restrictions
     */
    public boolean isApplicable() throws CheckExecutionException {
        logger.info("isApplicable() -- ENTER --");
	boolean applicable = super.isApplicable(); // multi-inputs?
        try {
		// 
        } catch (Exception e) {
            logger.warning("Exception: " + e.getMessage());
            throw new CheckExecutionException(e);
	}
        logger.info("isApplicable() test SC version: " + applicable);
        
        return applicable;
    } // isApplicable
    
    /*
     * Perform the actual checking
     * update message for use in getAnalysis() during check
     * @return true if check passes, false if violated
     */
    public boolean doCheck() throws CheckExecutionException {
        
        logger.info("-- ENTER --");
        boolean passes = true;

        try {
            ;         
            /*
             * create array of /etc/hosts files
	     * get list of LHRS
	     * for each LHRS
	     *	get hostnames
	     *	for each hostname
	     *		check each /etc/hosts
	     *		if missing log it
             */
/*  XXXXX               
            logger.info("getNumInputs(): " + getNumInputs());

            // load up array of /etc/hosts files from all nodes
	    TextFile[] etcHosts = new TextFile[getNumInputs()];

            for (int i = 0; i < getNumInputs(); i++) {
                String infoMsg = null;
                String nodename = getNodename(i);
                try {
                    logger.info("loading /etc/hosts[" + i + "]");
                    etcHosts[i] = getDataSrc(i).getTextFile("/etc/hosts");   // XXX

                } catch (FileNotFoundException e) {
			logger.info("FileNotFoundException(): " + e.getMessage());
                    throw new CheckExecutionException(e);
                } catch (IOException e) {
                    logger.info("IOException(): " + e.getMessage());
                    throw new CheckExecutionException(e);
		}    
            }

	    // and matching array of nodenames
	    String[] nodenames = getNodenames();
            
	    // get List of LHRS. Clusterwide data from arbitray node.
	    SunClusterConfig sunClusterConfig = getSunClusterConfig(0);
	    List lhrsList = sunClusterConfig.getLHResources();
	    List hostnames = null;

            logger.info("   doCheck() loop top ====================");
	    Logicalhostnameresource lhrs = null;

	    // loop LHRS's
	    Iterator it1 = lhrsList.iterator();
	    Iterator it2 = null;
	    String lhname = null;
	    String host = null;
	    String nodesLackingEntry = null;

	    while (it1.hasNext()) {
		    lhrs = (Logicalhostnameresource)it1.next();
		    lhname = lhrs.getName();

		    // get hostnames
		    hostnames = lhrs.getHostnames();
		    it2 = hostnames.iterator();
		    while (it2.hasNext()) {
			    host = (String)it2.next();

			    // loop etcHosts; private utility method
			    nodesLackingEntry = testEtcHosts(etcHosts, nodenames, host);
			    if (nodesLackingEntry != null) {
				    missingEntries.add(nodesLackingEntry);
				    passes = false;
			    }
		    } // while hostnames
	    } // while lhrs
*/
	// catch any otherwise uncaught exception
        } catch (Exception e) {
            logger.warning("Exception: " + e.getMessage());
            throw new CheckExecutionException(e);
        }

        logger.info("-- EXIT passes: " + passes);
        return passes;
    } // doCheck


	/*
	 * Check each etcHosts for hostname
	 * Return comma separated list of nodenames missing hostname
	 * or return null for none missing.
	 */
	private String testEtcHosts(TextFile[] etcHosts, String[] nodenames,
	    String hostname, String lhrsName) {

		StringBuffer nodesLackingEntry = new StringBuffer("");
		for (int i = 0; i < etcHosts.length; i++) {
			/*
			 * FIX: this test does not account for hostname
			 * being present but commented out
			 */
			if (!etcHosts[i].contains(hostname)) {
				nodesLackingEntry.append(hostname + "\t");
				nodesLackingEntry.append(lhrsName + "\t");
				nodesLackingEntry.append(nodenames[i] + "\n");
			}
		} // for etcHosts

		if (nodesLackingEntry.length() > 0) {
			return nodesLackingEntry.toString();
		} else {
			return null;
		}
	} // testEtcHosts

} // class

