/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)CheckS6708502.java	1.4	08/10/13 SMI"
 */
package com.sun.cluster.cfgchk.check;

import java.util.ArrayList;

import com.sun.cluster.cfgchk.Check;
import com.sun.cluster.cfgchk.BasicCheck;
import com.sun.cluster.cfgchk.CheckExecutionException;
import com.sun.cluster.cfgchk.Globals;
import com.sun.cluster.cfgchk.datasrc.DataException;
import com.sun.cluster.cfgchk.utils.logger.LoggerUtils;
import com.sun.cluster.cfgchk.utils.StringUtils;

/**
 *
 * 6708502 - 1212 (Critical)
 *
 * Single node check.
 * "Unsupported Real Time processes are running on a Sun Cluster node"
 *
 * List of allowable processes obtained from cfgchk_aux.dat.
 *
 */

/*
 * No I18n of any strings in this class.
 * Exceptions thrown from Checks are caught and handled by Engine
 * typically just by logging. Messages are never displayed to user.
 *
 * Lines with trailing 'TOC' comment are auto-gathered into
 * table of contents listing of all checks.
 */

public class CheckS6708502 extends BasicCheck {

	private ArrayList illegalRTProcesses = null;

	public CheckS6708502() {
		checkID = "S6708502"; // TOC
		severity = Check.CRITICAL; // TOC

		// sccs keyword: must be 1.4 when checked out under sccs
		version = "1.4";
		// sccs keyword: must be 08/10/13 when checked out under sccs
		revisionDate = "10/24/08";

		keywords.add("SunCluster3");
		keywords.add("single");
		keywords.add("processes");

		/* BEGIN JSTYLED */
		problemStatement = "Unsupported Real Time processes are running on a Sun Cluster node."; // TOC
		/* END JSTYLED */

		// check-specific data
		illegalRTProcesses = new ArrayList();
	} // ()

	/**
	 * @return a string describing the logic of the applicability test
	 */
	protected String assignApplicabilityLogic() {
		String aLogic = "Always applies to Sun Cluster nodes.";
		return aLogic;
	} // assignApplicabilityLogic


	/**
	 * @return a string describing the logic of the check
	 */
	protected String assignCheckLogic() {
		/* BEGIN JSTYLED */
		String cLogic = "Examine process list for process running in real-time mode. Compare process names against list of allowable processes obtained from the auxillary data file: " + Globals.AUX_DATA_FILENAME + ", keyed to this check ID. If a running real-time process is not found in the list, this check is violated.";
		/* END JSTYLED */
		return cLogic;
	} // assignCheckLogic


	public String getAnalysis() {
		/* BEGIN JSTYLED */
		StringBuffer analysis = new StringBuffer("");
		analysis.append("A Real Time (RT) process other than a Sun Cluster daemon or an application daemon qualified to run RT with Sun Cluster was found running on this Sun Cluster node:\n\n");
		for (int i = 0; i < illegalRTProcesses.size(); i++) {
			analysis.append("\t" + (String)illegalRTProcesses.get(i) + "\n");
		}

		analysis.append("\n\tA Real Time process that has not been qualified to run with Sun Cluster might interfere with the proper operation of the cluster and result in unexpected cluster behavior.");
		/* END JSTYLED */

		return analysis.toString();
	} // getAnalysis

	public String getRecommendations() {
		/* BEGIN JSTYLED */
		String recommend = "" +
		    "Determine the use and/or cause of each non-Sun Cluster supported Real Time (RT) process running on this node.  Some third-party applications have RT processes that are qualified for use with Sun Cluster by the vendor of that application.  In those cases, check with the third-party vendor to verify that the process in question is supported to run RT with Sun Cluster.  If possible, remove or relocate any application that has an unsupported/unqualified RT process running on this Sun Cluster node.";
		/* END JSTYLED */

		return recommend;
	} // getRecommendations


	// always applicable to SC3.x
	public boolean isApplicable() throws CheckExecutionException {
		logger.info("isApplicable() -- ENTER --");
		boolean applicable = true;
		return applicable;
	} // isApplicable

	public boolean doCheck() throws CheckExecutionException {
		// explorer: sysconfig/ps-acefl.out
		// 6th field contains 'R'
		// note: ps -el would be sufficient but explorer runs the above
		// so we want to be consistent
		boolean passes = true;

		try {
			// read list of legal RT processes from aux data file
			String value = getAuxDataValue(checkID + ".rtproc");
			logger.info("legal Rt processes: " + value);
			String[] approvedProcesses =
			    StringUtils.stringArrayFromEncodedString(value);
			if (approvedProcesses == null) {
				approvedProcesses = new String[] {" "};
			}

			String[] cmd = { "/usr/bin/ps", "-acefl" };
			// could make this DataSrc.getPSListing() XXX

			String[] stdout = getDataSrc().runCommandStrArray(cmd);
			String schedClass = null;
			String uid = null;
			String process = null;
			String pid = null;
			int index = -2;
			logger.info("ps stdout: ");
			for (int i = 0; i < stdout.length; i++) {
				String line = stdout[i];
				logger.fine("stdout["+i+"]: " + stdout[i]);
				uid = line.substring(4,13).trim();
				schedClass = line.substring(28,30);
				logger.fine("stdout schedClass: >>" + schedClass + "<<");
				logger.fine("stdout uid: >>" + uid + "<<");
				if (schedClass.equals("RT")) {
					logger.info("Got a Real Time Process!");
					logger.info("stdout["+i+"]: >>" +
					    stdout[i]+"<<");
					process = line.substring(86);
					logger.info("process: >>" + process + "<<");

					pid =
					    line.substring(14, 19);
					logger.info("pid: >>" + pid + "<<");

					/*
					 * test for ownership that indicates
					 * oracle and allow all of these
					 */
					if (uid.equals("oracle") ||
					    uid.equals("crs")) {
						logger.info(
						    "uid IS approved: " +
							uid );
						continue;
					} // allow by uid
					 
					/*
					 * not a globally approved uid so
					 * test for inclusion in legal
					 * RT processes list
					 *
					 * some processes are launched under a
					 * shell meaning we can't just match
					 * on the first word. Instead, match
					 * 'non-precise' so that if our
					 * 'process' string contains (rather
					 * than exactly matching) a valid
					 * processs name, we call it legal
					 */
					index =
					    StringUtils.
					    indexOfStringInStringArray(
						    process, approvedProcesses,
							false);

					if (index < 0) {
						logger.warning(
							"process NOT legal: " +
								process);
						illegalRTProcesses.add(pid +
						    " " + process);
						passes = false;
					} else {
						logger.info(
							"process IS legal: " +
								process);
					}
				}
			}

		} catch (DataException e) {
			logger.warning("DataException: " + e.getMessage());
			LoggerUtils.exception("DataException", e, logger);
			throw new CheckExecutionException(e);
		} catch (Exception e) {
			logger.warning("Exception: " + e.getMessage());
			LoggerUtils.exception("Exception", e, logger);
			throw new CheckExecutionException(e);
		}

		logger.info("passes: " + passes);
		logger.info("-- EXIT --");
		return passes;
	} // doCheck

} // class

