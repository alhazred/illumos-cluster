/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)CheckS6708689.java	1.3	08/10/13 SMI"
 */
package com.sun.cluster.cfgchk.check;

import java.util.ArrayList;
import java.util.Hashtable;

import com.sun.cacao.invocation.InvocationStatus;
import com.sun.cluster.cfgchk.BasicCheck;
import com.sun.cluster.cfgchk.Check;
import com.sun.cluster.cfgchk.CheckExecutionException;
import com.sun.cluster.cfgchk.datasrc.ClusterDataSrc;
import com.sun.cluster.cfgchk.datasrc.DataException;
import com.sun.cluster.cfgchk.utils.ClusterUtils;
import com.sun.cluster.cfgchk.utils.logger.LoggerUtils;

/**
 *
 * 6708689 - (Critical, Warning)
 *
 * Single node check.
 * One or more resources cannot be validated
 *
 */

/*
 * No I18n of any strings in this class.
 * Exceptions thrown from Checks are caught and handled by Engine
 * typically just by logging. Messages are never displayed to user.
 *
 * Lines with trailing 'TOC' comment are auto-gathered into
 * table of contents listing of all checks.
 */

public class CheckS6708689 extends BasicCheck {

        private ArrayList validationResults = null;


	public CheckS6708689() {
		checkID = "S6708689"; // TOC
		severity = Check.VARIABLE_SEV; // CRITICAL or WARNING // TOC

		// sccs keyword: must be 1.3 when checked out under sccs
		version = "1.3";
		// sccs keyword: must be 08/10/13 when checked out under sccs
		revisionDate = "10/24/08";

		keywords.add("SunCluster3");
		keywords.add("single");
		keywords.add("rgm");
		keywords.add("resources");
		keywords.add("live_only");

		/* BEGIN JSTYLED */
		problemStatement = "One or more Sun Cluster resources cannot be validated"; // TOC
		/* END JSTYLED */

		// check-specific data
		validationResults = new ArrayList();

	} // ()

	/**
	 * @return a string describing the logic of the applicability test
	 */
	protected String assignApplicabilityLogic() {
		/* BEGIN JSTYLED */
		String aLogic = "This check is applicable only when running live on a Solaris node booted in cluster mode. This check is not applicable if the data source is an explorer archive.";
		/* END JSTYLED */
		return aLogic;
	} // assignApplicabilityLogic


	/**
	 * @return a string describing the logic of the check
	 */
	protected String assignCheckLogic() {
		/* BEGIN JSTYLED */
		String cLogic = "For each Resource Group with current node in nodelist, execute each resource's validate method, saving all output. A non-zero return value is a CRITICAL violation. A zero return value accompanied by warning output is a WARNING violation.";
		/* END JSTYLED */
		return cLogic;
	} // assignCheckLogic


	public String getAnalysis() {

		logger.info("getAnalysis() severity: " +
		    Check.SEVERITY_NAMES[severity]);
		logger.info("getAnalysis() number results: " +
		    validationResults.size());

		StringBuffer analysis = new StringBuffer("");
		/* BEGIN JSTYLED */
		analysis.append("The following resources could not be validated on node ");
		/* END JSTYLED */
		analysis.append(getNodename());
		analysis.append(":\n\n");
		for (int i = 0; i < validationResults.size(); i++) {
			logger.info("1 getAnalysis() vr["+i+"]");
			ValidationResult vr =
			    (ValidationResult)validationResults.get(i);
			logger.info("2 getAnalysis() vr["+i+"]" + vr);
			if (vr.severity == Check.UNKNOWN_SEV) {
				logger.info("resource " + vr.rsName +
				    " passes validation.");
				continue;
			}
			analysis.append("\t\t* resource \"" + vr.rsName);
			analysis.append("\" of type \"" +
			    vr.rtName);
			analysis.append("\" in resource group \"" +
			    vr.rgName + "\":");
			// analysis.append("\t" + vr.status);  // online?
			analysis.append("\n");
			logger.info("getAnalysis() number messages: " +
			    vr.messages.size());
			for (int j = 0; j <  vr.messages.size(); j++) {
				logger.info("3 getAnalysis() messages["+j+"]");
				analysis.append("\t\t\t> " +
				    (String)vr.messages.get(j) + "\n");
			}
		} // validationResults

		/* BEGIN JSTYLED */
		analysis.append("\t\tResources which cannot pass validation might not be able to perform as intended or might not be able to be brought online, possibly leading to loss of availability.");
		/* END JSTYLED */

		return analysis.toString();
	} // getAnalysis

	public String getRecommendations() {
		String recommend = "" +
			"Make adjustments as indicated in validation messages.";
		return recommend;
	} // getRecommendations


	public boolean isApplicable() throws CheckExecutionException {
		logger.info("isApplicable() -- ENTER --");
		boolean applicable = true;

		// Not applicable to explorer data source
		// Applies only to a cluster node in cluster mode
		try {
			ClusterDataSrc ds = getDataSrc();
			if (!ds.isLiveDataSource()) {
				applicable = false;
				logger.info("Check is not applicable " +
				    "if not running on live system.");
			}
			if (!ds.isClusterMode(usingCacao)) {
				applicable = false;
				logger.info("Check is not applicable " +
				    "in non-cluster mode.");
			}

		} catch (DataException e) {
			logger.info("DataException: " + e);
			throw new CheckExecutionException(e);
		} catch (Exception e) {
			logger.warning("Exception: " + e);
			LoggerUtils.exception("Exception", e, logger);
			throw new CheckExecutionException(e);
		}

		logger.info("isApplicable(): " + applicable);
		return applicable;
	} // isApplicable

	public boolean doCheck() throws CheckExecutionException {
		/*
		 * obtain & cache validate method for all registered RT's
		 * for each RG
		 *	if current node in nodelist
		 *		for each RS
		 *			fetch RT; invoke validate
		 *			non-0 return = critical
		 *			0 return + message     = warning
		 */

		logger.info("-- ENTER --");
		boolean passes = true;
		Hashtable validateTab = null;

		try {
			// obtain & cache validate method for all
			// registered RT's
			validateTab = loadValidateTab(); // DataEx

			// get RG's
			String[] rgs = getDataSrc().getRGNames();
			logger.info("rgs: ");
			for (int i = 0; i < rgs.length; i++) {
				String rgName = rgs[i];
				boolean inNodelist = false;
				logger.info("rgs["+i+"]: " + rgName);
				String[] rgNodelist =
				    getDataSrc().getRGNodelist(rgName);
				for (int j = 0; j < rgNodelist.length; j++) {
					logger.info("rgNodelist["+j+"]: " +
					    rgNodelist[j] +
					    "  top of rg nodelist loop");
					if (getNodename().equals(
						    rgNodelist[j])) {
						inNodelist = true;
					}
					if (!inNodelist) {
						logger.info(
							"! in nodelist: skip");
						continue; // next RG
					}
					logger.info("in nodelist: check rs's");

					// run VALIDATE on all RS in RG
					int result =
					    testRG(rgName, validateTab);
						// DataEx, ChExEx

					// save highest severity result
					// for check
					severity =
					    maxSeverity(result, severity);

					// no need to continue testing
					// rgNodelist
					logger.info("exiting rgNodelist " +
					    "loop after self match");
					break;
				}
			}


		} catch (DataException e) {
			logger.warning("DataException: " + e.getMessage());
			LoggerUtils.exception("DataException", e, logger);
			throw new CheckExecutionException(e);

		} catch (Exception e) {
			logger.warning("Exception: " + e);
			LoggerUtils.exception("Exception", e, logger);
			throw new CheckExecutionException(e);
		}

		if (severity != Check.VARIABLE_SEV) {
			passes = false;
		}

		logger.info("passes: " + passes);
		logger.info("computed severity: " + getSeverityName());
		logger.info("-- EXIT --");
		return passes;
	} // doCheck

	// run VALIDATE on each RS in RG
	// return computed severity value for check
	private int testRG(String rgName, Hashtable vTab)
		throws DataException, CheckExecutionException {
		logger.info(" -- ENTER --");
		int sev = Check.UNKNOWN_SEV;
		String[] rss = getDataSrc().getRSNames(rgName);
		for (int i = 0; i < rss.length; i++) {
			String rsName = rss[i];
			logger.info("rg: " + rgName + "; rss["+i+"]: " +
			    rsName);

			// get RT & validate method
			String rtName = getDataSrc().getRTName(rsName, rgName);
				// DataEx

			logger.info("rtName: " + rtName);
			String validate = (String)vTab.get(rtName);
			logger.info("validate: " + validate);

			// run VALIDATE on individual RS
			int result = testRS(rsName, rgName, rtName, validate);
			logger.info("result: " + result);

			// save maximum severity from this RG
			sev = maxSeverity(result, sev);
			if (result < sev) {
				sev = result;
			}
		}

		logger.info("sev: " + sev);
		logger.info(" -- EXIT --");
		return sev;
	} // testRG

	// run VALIDATE on individual RS
	// return computed severity value for check
	// store ValidationResult
	private int testRS(String rsName, String rgName, String rtName,
	    String validate) throws DataException, CheckExecutionException {

		int sev = Check.UNKNOWN_SEV;
		InvocationStatus status =
		    ClusterUtils.invokeValidate(
			    getNodename(), rsName, rgName, rtName,
				    validate, usingCacao, logger);
		logger.info("VALIDATE status: " + status +
		    " back from : " + getNodename());
		if (status == null || status.getExitValue() == null) {
			logger.info("failed to execute validation command: " +
			    validate);
			throw new CheckExecutionException(
				"Unable to execute " + validate);
		} else {
			ValidationResult vr =
			    new ValidationResult(rsName, rtName, rgName, status);
			validationResults.add(vr);
			logger.info("vr.severity: " + vr.severity);

			// consider severity result for check overall
			sev = maxSeverity(vr.severity, sev);
		}
		logger.info("sev: " + sev);
		return sev;
	} // testRS


	private Hashtable loadValidateTab() throws DataException {
		Hashtable vTab = new Hashtable();
		String[] rts = getDataSrc().getRegisteredRTNames(); // DataEx

		logger.fine("rts: ");
		for (int i = 0; i < rts.length; i++) {
			logger.fine("rts["+i+"]: " + rts[i]);
			String validate = ClusterUtils.getValidate(rts[i],
			    logger);
			logger.fine("validate: " + validate);
			vTab.put(rts[i], validate);
		}

		return vTab;
	} // loadValidateTab

	private class ValidationResult {
		private String rsName = null;
		private String rtName = null;
		private String rgName = null;
		private ArrayList messages = null;
		private int returnCode = 0;
		private int severity = Check.UNKNOWN_SEV;

		public ValidationResult(String rsName, String rtName, String rgName,
		    InvocationStatus status) {
			this.rsName = rsName;
			this.rtName = rtName;
			this.rgName = rgName;
			returnCode = status.getExitValue().intValue();
			messages = new ArrayList();
			populateMessages(status, messages);
			if (returnCode != 0) {
				severity = Check.CRITICAL;
				logger.info("setCheck.CRITICAL");
			}
		} // ()

		// stdOut & stdErr treated the same
		// treat both as WARNING
		// constructor will test return code and set
		// CRITICAL if appropriate
		private void populateMessages(InvocationStatus status,
		    ArrayList messages) {
			String msg = null;
			msg = status.getStdout();
			logger.info("stdOut: " + msg);
			if (msg != null && msg.length() > 0) {
				severity = Check.WARNING;
				messages.add(msg);
				logger.info("setCheck.WARNING");
			}

			msg = status.getStderr();
			logger.info("stdErr: " + msg);
			if (msg != null && msg.length() > 0) {
				severity = Check.WARNING;
				messages.add(msg);
				logger.info("setCheck.WARNING");
			}
		} // populateMessages

		public String toString() {
			return rsName+" "+rtName+" "+rgName+" "+messages.size();
		} // toString

	} // private class

} // class

