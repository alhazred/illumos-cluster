/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)Cfgchk.java	1.6	08/09/22 SMI"
 */
package com.sun.cluster.cfgchk;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.logging.FileHandler;
import java.util.logging.Logger;

import com.sun.cluster.cfgchk.agent.cfgchk.results.CommandResult;
import com.sun.cluster.cfgchk.agent.cfgchk.results.ErrorResult;
import com.sun.cluster.cfgchk.agent.cfgchk.results.ExitCode;
import com.sun.cluster.cfgchk.agent.cfgchk.results.SuccessResult;
import com.sun.cluster.cfgchk.common.CfgchkException;
import com.sun.cluster.cfgchk.common.CfgchkRemoteException;
import com.sun.cluster.cfgchk.datasrc.ClusterDataSrc;
import com.sun.cluster.cfgchk.datasrc.DataException;
import com.sun.cluster.cfgchk.datasrc.ExplorerClusterDataSrc;
import com.sun.cluster.cfgchk.datasrc.LiveClusterDataSrc;
import com.sun.cluster.cfgchk.datasrc.LiveClusterDataSrcMBean;
import com.sun.cluster.cfgchk.engine.Engine;
import com.sun.cluster.cfgchk.engine.EngineListener;
import com.sun.cluster.cfgchk.i18n.I18n;
import com.sun.cluster.cfgchk.utils.CfgchkProperties;
import com.sun.cluster.cfgchk.utils.CheckUtils;
import com.sun.cluster.cfgchk.utils.ChecksCache;
import com.sun.cluster.cfgchk.utils.ClusterUtils;
import com.sun.cluster.cfgchk.utils.ErrorMsg;
import com.sun.cluster.cfgchk.utils.JarClassLoader;
import com.sun.cluster.cfgchk.utils.Params;
import com.sun.cluster.cfgchk.utils.StringUtils;
import com.sun.cluster.cfgchk.utils.Utils;
import com.sun.cluster.cfgchk.utils.logger.LoggerUtils;
import com.sun.cluster.cfgchk.utils.logger.RemoteLogger;


/**
 * This is the main class for project 2006/1535, the Next-Gen
 * Configuration Checker. It knows itself as 'cfgchk' and is
 * made available to Sun Cluster starting in 3.2U2 as two
 * subcommands to the 'cluster' command:
 *	(1) 'check' - perfoms the actual check run
 *	(2) 'list-checks' dumps available checks to the screen. This
 * subcommand is implemented mostly in doListChecks()
 *
 * The 'version' option is handled directly in cfgchk.ksh.
 */

public class Cfgchk implements EngineListener, Globals {

	private FileHandler sessionLogFile = null;
	private Logger logger = null; // session
	private Engine engine = null;
	private CfgchkProperties props = null;
	private CommandResult commandResult = null;
	private int maxViolatedSeverity = 0;
	private boolean usingCacao = true;
	private String logFilename = null;
	private boolean verbose = false;
	private PrintWriter stdout = null;
	private PrintWriter stderr = null;
	private ChecksCache[] checksCache = null;
	// each elt holds jar file + jarEntries[]

	private Params params = null; // args & environment values bundle
	private ArrayList dataSrcs = null;

	private	JarClassLoader jarClassLoader = null;
	private File lockfile = null; // session lockfile
	private int displayMsgPad = 14;

	/*
	 * constructor
	 */
	public Cfgchk(String encodedArgs,
	    PrintWriter stdout, PrintWriter stderr,
	    boolean usingCacao, boolean fromGUI) {

		this.usingCacao = usingCacao;
		this.stdout = stdout;
		this.stderr = stderr;

		/*
		 * Session lock here captures cli with or without cacao
		 * as well as gui. Removed in cleanup()
		 */
		lockfile = new File(Globals.CFGCHK_LOCKFILE);
		boolean madeLock = false;
		boolean pd = false;
		try {
			// make sure directory exists
			File parentDir = lockfile.getParentFile();
			if (parentDir == null) {
				// insane: jump down to handler
				throw new IOException("");
			}
			pd = parentDir.mkdirs();
			if (!pd && !parentDir.exists()) {
				// couldn't create dirs
				// jump down to handler
				throw new IOException("");
			}
			madeLock = lockfile.createNewFile();
			// will be false if already exists

			if (madeLock == false) { // lockfile already exists
				/*
				 * check to see if we're forcing past an
				 * existing lockfile: encoded args not yet
				 * parsed so brute-force it here: safe
				 * assumption that keyword  will not
				 * otherwise appear in encodedArgs
				 */
				boolean forcing = (encodedArgs.indexOf(
				    Globals.CLI_FORCE_OPTION) != -1);
				if (!forcing) {
					String[] i18nArgs = { Globals.CFGCHK_LOCKFILE };
					String lMsg = ErrorMsg.makeErrorMsg(logger,
					    "cfgchk.locked", i18nArgs);
					commandResult = new ErrorResult(
					    ExitCode.ERR_BUSY, lMsg);
					return; // early exit. no cleanup()
				}
			}
		} catch (IOException ioex) {
			String[] i18nArgs = { Globals.CFGCHK_LOCKFILE };
			String lMsg = ErrorMsg.makeErrorMsg(logger,
			    "cfgchk.cant.make.lock", i18nArgs);
			commandResult = new ErrorResult(
			    ExitCode.ERR_INTERNAL, lMsg);
			return; // early exit. no cleanup()
		}

		/*
		 * Constructor performs necessary setups then calls runEngine()
		 *
		 * A CommandResult is created through these steps, available for
		 * pickup by the caller via getCommandResult()
		 *
		 * A SuccessResult will not contain any messages but
		 * an ErrorResult
		 * will contain one or more localized, arg substituted, messages
		 * ready for display on stderr
		 *
		 * Any exceptions that can appear are turned into I18n
		 * messages and packed into the ErrorResult.
		 *
		 * Messages, possibly multiple,  are displayed to user:
		 *	by Administrator.execute() if cli and using cacao
		 *		(see /etc/default/cfgchk)
		 *	by gui after extraction from ErrorResult
		 *	by Cfgchk.main() if cli and no cacao
		 *
		 * Administrator.execute(), Administer.runEngine()
		 * and Cfgchk.main()
		 * should not need  to do any exception catching and localized
		 * message creation	(but they may do it anyway as
		 * adding belt to suspenders)
		 *
		 * must call cleanup() before returning in order to
		 * shutdowm logging cleanly and roll the "remote" logs
		 */

		try {
			// no logger until we get back from setup()
			commandResult = setup(encodedArgs, fromGUI);

			logger.info("back from setup(): params: " + params);
			logger.info("back from setup(): commandResult: " +
			    commandResult);
			// params now loaded

			if (commandResult instanceof ErrorResult) {
				logger.info("back from setup(): ErrorResult ");

				/*
				 * Administrator will showErr
				 *
				 * possible error cases:
				 * from setup() inClusterMode but unable
				 * to fetchAllClusterNodeIdentifiers
				 * from log & output dir creation:
				 *   security or IOEx
				 * from processArgs(): validation issues
				 * from createChecksCache(): FNFEx, IOEx
				 */
				cleanup(); // will run before either return
				return; // early exit:
			}
			logger.info("back from setup(): SuccessResult ");
		} catch (Exception ex) { // unknown
			String msg = ex.getMessage();
			commandResult = new ErrorResult(ExitCode.ERR_INTERNAL,
			    msg);
			cleanup(); // will run before either return
			return; // early exit
		}

		try {
			logger.info("calling runEngine()");
			commandResult = runEngine();
			logger.info("back from runEngine(): " + commandResult);
			/*
			 * possible error cases:
			 */
		} catch (Exception ex) { // unknown
			String msg = ex.getMessage();
			commandResult = new ErrorResult(ExitCode.ERR_INTERNAL,
			    msg);
		}

		/*
		 * Exit both cacao & non-cacao runs by falling off the end here
		 * and returning to:
		 *
		 *	Administrator for cacao session.
		 * It will get cr by calling getCommandResult()
		 * and return exitCode through cacaocsc
		 *
		 *	Cfchk.main() for non-cacao session which
		 * will call System.exit() with the exitCode
		 * getting it back to ksh
		 */

		cleanup(); // close logs, etc

	} // ()

	/*
	 * handle various setup steps
	 * problems (bad input or internal errors) are returned in ErrorResult
	 * no problems returns SuccessResult
	 */
	private CommandResult setup(String encodedArgs, boolean fromGUI) {

		// no logger yet...
		CommandResult cr = null;
		String lMsg = null; // for I18n

		/*
		 * general pattern for most routines that can return
		 * both internal & user errors is for user errors
		 * to come back as returned Strings
		 * and internal errors come back as CfgchkExceptions
		 * containing I18n strings
		 */

		// parse args into array; want to get [0] right away
		// to setup logging
		if (encodedArgs == null) {
			lMsg = ErrorMsg.makeErrorMsg(/* logger */ null,
			    "internal.error.null.args");
			cr = new ErrorResult(ExitCode.ERR_INTERNAL, lMsg);
			return cr; // early exit
		}

		// decode user args into strarray
		String[] argv = StringUtils.stringArrayFromEncodedString(
			encodedArgs, ARG_ENCODING_SEP);

		// setup output & logs dir as side effect
		// first two elts setup in order by ksh
		logFilename = argv[0];
		String localNodename = argv[1];

		try {
			setupLoggers(logFilename, localNodename, fromGUI);
				// outputdir now exists

		} catch (InvalidParameterException ipex) {
			// no logger
			lMsg = ipex.getMessage(); // already I18n
			cr = new ErrorResult(ExitCode.ERR_ARG, lMsg);
			return cr; // early exit
		}


		/*
		 * logger is now available to use and pass around
		 */


		// dump both forms of user args to log
		logger.fine("== encodedArgs: " + encodedArgs);
		for (int i = 0; i < argv.length; i++) {
			logger.finest("decoded arg(" + i + "): " + argv[i]);
		}

		// create Params
		try {
			params = new Params(logger, localNodename, usingCacao,
			    fromGUI); // global params
			logger.info("params: " + params);

		} catch (DataException dex) {
			/*
			 * if inClusterMode but can't load all node identifiers
			 * or if fail to load local nodename
			 */
			logger.warning("DataException: " + dex);
			lMsg = dex.getMessage(); // already I18n in ErrorMsg
			cr = new ErrorResult(ExitCode.ERR_INTERNAL,
			    dex.getMessage());
			return cr; // early exit
		} catch (Exception ex) {
			LoggerUtils.exception("setup() new Params: ", ex,
			    logger);
		}


		// create global jarclassloader
		jarClassLoader = new JarClassLoader(
		    this.getClass().getClassLoader(),
			this.getClass().getProtectionDomain());


		// load global default props from file
		try {
			props = CfgchkProperties.getCfgchkProperties(PROPSFILE,
			    true); // force reload

		} catch (IOException ioex) {
			/*
			 * not fatal; queries to CfgchkProperties will return
			 * default value if props == null
			 */
			logger.warning("ignoring loading props IOException: " +
			    ioex);
			String[] i18nArgs = { ioex.getMessage() };
			lMsg = I18n.getLocalized(logger,
			    "cfgchk.warning.no.defaults", i18nArgs);
			stderr.println(lMsg); // display warning right now
		}

		loadDefaultParams(props); // no Ex

		// hold errMsg returns from next few routines
		List errMsgs = null;

		// process & validate user args
		try {
			errMsgs = processArgs(argv);

			// don't have verbose setting until control gets here...
			// first progress message
			lMsg = I18n.getLocalized(logger, "cfgchk.initializing");
			progressMsg(lMsg);

		} catch (CfgchkException cfgex) { // internal error
			/*
			 * I18n msg embedded
			 * possible error cases:
			 * uncaught exception in loadParams()
			 */
			cr = new ErrorResult(ExitCode.ERR_INTERNAL,
			    cfgex.getMessage());
			logger.warning("CfgchkException: " + cfgex);
			return cr; // early exit
		}

		// user-related errors
		if (errMsgs != null && errMsgs.size() != 0) {
				// List of I18n messages
			cr = errListToCR(ExitCode.ERR_ARG, errMsgs);
			return cr; // early exit
		}

		// load checksCache
		try {
			errMsgs = createChecksCache();
			// jarfile(s) not found

		} catch (CfgchkException cfgex) { // internal error
			/*
			 * I18n msg embedded
			 * possible error cases:
			 * IOException reading jars
			 * assume user-specified jar can't be found
			 */
			cr = new ErrorResult(ExitCode.ERR_ARG,
			    cfgex.getMessage());
			return cr; // early exit
		}
		if (errMsgs.size() != 0) { // List of I18n messages
			// user errors
			logger.info("createChecksCache number error msgs: " +
			    errMsgs.size());
			cr = errListToCR(ExitCode.ERR_ARG, errMsgs);
			return cr; // early exit
		}

		/*
		 * if we've gotten here we're OK!
		 */
		logger.info("-- EXIT--");
		cr = new SuccessResult();
		return cr;
	} // setup



	/*
	 * create and launch the Engine
	 */
	private CommandResult runEngine() {
		logger.info("-- ENTER -----  top of runEngine() -------------");
		CommandResult cr = null;

		// handle list-checks
		if (params.list) {
			// user cmd is 'cluster list-checks' not 'cluster check'
			doListChecks();
			cr = new SuccessResult(ExitCode.OK);
			return cr; // early exit
		} else {
			logger.info("not params.list");
		}

		// do real check run
		logger.info("will create engine");
		try {
			engine = createEngine(); // throws DEx

			logger.info("starting engine");
			Thread thread = new Thread(engine);
			thread.start();

			logger.info("back from starting engine");

			try {
				logger.info("--------------------- STARTING " +
				    "WAIT FOR Engine.run() ----------------");
				thread.join();
					// wait for engine.run() to finish
				logger.info("----------------------- DONE " +
				    "WAIT FOR Engine.run() ------------------");
				cr = new SuccessResult(maxViolatedSeverity);
				logger.info("after join: " + cr);

			} catch (InterruptedException ie) {
				logger.severe("InterruptedException");
				String lMsg = ErrorMsg.makeErrorMsg(logger,
				    "internal.error.interrupted");
				cr = new ErrorResult(ExitCode.ERR_ARG, lMsg);
			}
		} catch (DataException dex) {
			logger.info("DataException: " + dex.getMessage());
			String msg = dex.getMessage(); // I18n at the source
			cr = new ErrorResult(ExitCode.ERR_ARG, msg);
		}

		logger.info(" -- EXIT: " + cr);
		return cr;
	} // runEngine


	// exceptions carry I18n message
	private Engine createEngine() throws DataException {
		logger.info("-- ENTER -- ");

		try {
			String[] checklist = getChecklist(); // never null
			logger.info("got checklist; num items: " +
			    checklist.length);
			dataSrcs = createDataSrcs();
				// CfgEx; never null; global
			logger.info("got dataSrcs; num items: " +
			    dataSrcs.size());

			logger.info("start test dataSrcs");
			for (int i = 0; i < dataSrcs.size(); i++) {
				try {
					ClusterDataSrc ds =
					    (ClusterDataSrc)dataSrcs.get(i);
					logger.info("test ds.getNodename(): >" +
					    ds.getSystemName() + "<");
				} catch (ClassCastException ccex) {
					logger.info("ClassCastException: " +
					    ccex.getMessage());
					String[] i18nArgs = {
						ccex.getMessage()
					};
					String lMsg = I18n.getLocalized(logger,
					    "internal.error.ds.classcast",
					    i18nArgs);
					throw new DataException(lMsg);
				}
			}
			logger.info("done test dataSrcs");

			engine = new Engine(logger, this, dataSrcs, // DataEx
			    usingCacao, checklist, params,
			    checksCache, jarClassLoader,
			    logFilename);

		} catch (CfgchkException e) {
			logger.info("CfgchkException: " + e.getMessage());
			String[] i18nArgs = { e.getMessage() };
			String lMsg = I18n.getLocalized(logger,
			    "internal.error.ds.create", i18nArgs);
			throw new DataException(lMsg);
		} catch (DataException e) {
			logger.info("DataException: " + e.getMessage());
			String[] i18nArgs = { e.getMessage() };
			String lMsg = I18n.getLocalized(logger,
			    "internal.error.engine.create", i18nArgs);
			throw new DataException(lMsg);
		}
		logger.info("-- END --");
		return engine;
	} // createEngine

	/*
	 * return an ArrayList of ClusterDataSrc's
	 * may be size zero but never null
	 */
	private ArrayList createDataSrcs() throws CfgchkException {
		logger.info("-- ENTER -- " + new Date());
		ArrayList dtSrcs = null;

		if (params.explorerNames != null) {
			dtSrcs = createExplorerClusterDataSrcs(); // no Ex
		} else {
			dtSrcs = createLiveClusterDataSrcs(); // CfgRmtEx
		}
		logger.info("-- END -- " + new Date());
		return dtSrcs;
	} // createDataSrcs


	/*
	 * explorer comes in as directory name already validated
	 *
	 * returns ArrayList of ClusterDataSrcs (may be size 0 but never null)
	 * no Exceptions
	 */
	private ArrayList createExplorerClusterDataSrcs() {
		logger.info("-- ENTER -- " + new Date());
		ArrayList dtSrcs = new ArrayList();
		ClusterDataSrc cds = null;

		// explorer names already validated
		String explNameList = params.explorerNames;
		String[] explNames = StringUtils.stringArrayFromEncodedString(
			explNameList, LIST_ENCODING_SEP);

		logger.info("createExplorerClusterDataSrcs explNames.length: " +
		    explNames.length);
		for (int i = 0; i < explNames.length; i++) {
			logger.info("createExplorerClusterDataSrcs name[i]: " +
			    explNames[i]);
			// explorer basedir must end with '/'
			explNames[i] = StringUtils.forceTrailingChar(
				explNames[i], '/');
			cds = new ExplorerClusterDataSrc(explNames[i], logger);
				// no Ex; never null
			dtSrcs.add(cds);
			logger.info("createExplorerClusterDataSrcs " +
			    "name[i] being added: " + explNames[i]);
		} // for names

		logger.info("-- END -- " + new Date());
		return dtSrcs;
	} // createExplorerClusterDataSrcs


	/*
	 * if using cacao:
	 *	access local and remote nodes identically
	 * if not using cacao:
	 *	will be talking directly but must be only one node run
	 *	where node is localhost
	 *	(more typically not using cacao will mean we're using
	 *	explorer inputs rather than live node but that case is
	 *	handled in createExplorerClusterDataSrcs())
	 *
	 * returns ArrayList of ClusterDataSrcs (may be size 0 but never null)
	 * or throws CfgchkException containing localized message
	 */
	private ArrayList createLiveClusterDataSrcs() throws CfgchkException {
		logger.info("-- ENTER -- " + new Date());

		ArrayList dtSrcs = new ArrayList(); // output

		ClusterDataSrc cds = null;
		LiveClusterDataSrcMBean lcdsMBean = null;

		logger.info("createLiveClusterDataSrcs() nodeNames: " +
		    params.nodeNames);

		String[] nodeNames = StringUtils.stringArrayFromEncodedString(
			params.nodeNames, LIST_ENCODING_SEP);

		// create data src for each node either direct or remote
		String nodename = null;
		for (int i = 0; i < nodeNames.length; i++) {
			nodename = nodeNames[i];
			logger.info("createLiveClusterDataSrcs() creating " +
			    "datasrc for: >" + nodename + "<");
			if (!usingCacao) {
				// talk directly to localhost; already validated
				cds = new LiveClusterDataSrc(nodename); // No Ex
				dtSrcs.add(cds);
			} else {
				// fetch mbean for node, even if is localhost
				try {
					lcdsMBean = getRemoteLiveClusterDataSrc(
						nodename); // CfgREx
					dtSrcs.add(lcdsMBean);

				} catch (CfgchkRemoteException crex) { // fatal
					/* BEGIN JSTYLED */
					logger.warning(
						"unable to get LiveClusterDataSrcMBean for: " + nodename);
					String[] i18nArgs = { crex.getMessage() };
					String lMsg = I18n.getLocalized(logger,
					    "internal.error.systemdatasrc.cannot.create", i18nArgs);
					/* END JSTYLED */
					throw new CfgchkException(lMsg);
				}

			}
		}

		logger.info("-- END -- " + new Date());
		return dtSrcs;
	} // createLiveClusterDataSrcs


	/*
	 * Get LiveClusterDataSrc for given nodename.
	 *
	 * nodename may actually be commandNode but we're still going
	 * through mbean
	 */
	private LiveClusterDataSrcMBean getRemoteLiveClusterDataSrc(
		String nodename) throws CfgchkRemoteException {
		logger.info("-- ENTER -- >" + nodename + "<");

		LiveClusterDataSrcMBean lcdsMBean = null;

		try {
			lcdsMBean = ClusterUtils.getRemoteLiveClusterDataSrc(
				nodename, logger);
		} catch (CfgchkRemoteException crex) {
			logger.warning("CfgchkRemoteException" + crex);
			throw crex;
		}

		logger.info("-- EXIT -- " + nodename);
		return lcdsMBean;
	} // getRemoteLiveClusterDataSrc


	/*
	 * getChecklist()
	 *
	 * relevant params members:
	 *	checkJars, includeCheckIDs, excludeCheckIds
	 *
	 * may return size zero but never null
	 */
	private String[] getChecklist() {
		logger.info("-- ENTER --");

		String checkJars = params.checkJars;
		logger.info("checkJars: " + checkJars);

		/*
		 * if includeCheckID's is given then use only these
		 * else load all from checks jar as include list
		 * if excludeCheckID's is given then filter
		 * out from include list
		 */
		String[] inclChecks = null;

		String includeCheckIDs = params.includeCheckIDs;

		// default to all available checks?
		if (includeCheckIDs == null || includeCheckIDs.length() < 1) {
			// yes: populate from all check jar files
			// not instantiating Checks so classloader not needed
			inclChecks = CheckUtils.listAllChecks(checksCache,
			    logger);
				// may return arry size zero but never null
		} else {
			// no: convert "encoded" user arg to array
			inclChecks = StringUtils.stringArrayFromEncodedString(
				includeCheckIDs, LIST_ENCODING_SEP);
		}
		logger.fine("includeCheckIDs: " + includeCheckIDs);

		String excludeCheckIds = params.excludeCheckIds;
		logger.info("excludeCheckIds: " + excludeCheckIds);
		String[] exclChecks = StringUtils.stringArrayFromEncodedString(
			excludeCheckIds, LIST_ENCODING_SEP);

		// produce "net" checklist; exclude trumps include
		// may be zero but never null
		String[] checklist = StringUtils.subtractStringArrays(
			inclChecks, exclChecks);

		for (int i = 0; i < checklist.length; i++) {
			logger.info("checklist["+i+"]: " + checklist[i]);
		}
		logger.info("-- EXIT --");
		return checklist;
	} // getChecklist


	private void doListChecks() {
		logger.info("-- ENTER --");
		String[] checks = null;
		String[] checklist = getChecklist(); // never null

		// can use String[] checklist if ever want to
		// output checkID's only
		if (verbose) {
			checks = CheckUtils.listChecks(CheckUtils.LOGIC,
			    jarClassLoader, checklist, checksCache, logger);
		} else {
			checks = CheckUtils.listChecks(CheckUtils.SYNOPSIS,
			    jarClassLoader, checklist, checksCache, logger);
		}
		logger.info("loaded String[] checks");
		if (checks == null) {
			logger.info("no checks found to list!");
			String lMsg = ErrorMsg.makeErrorMsg(logger,
			    "check.list.no.checks");
			showErr(lMsg);
		} else {
			for (int i = 0; i < checks.length; i++) {
				if (checks[i] != null) {
					showMsg(checks[i]);
					// already I18n if needed
				} else {
					logger.warning("****  null check ****");
				}
			}
		}
		logger.info("-- EXIT --");
	} // doListChecks

	/*
	 * civilized shutdown of logging
	 * needed to wipe out the pesky lock files
	 * kill session lockfile
	 */
	private void cleanup() {
		try {
			String lMsg = I18n.getLocalized(logger,
			    "cfgchk.cleaning");
			progressMsg(lMsg);

			logger.info("calling for datasrc cleanup");
			// live data srcs may be persistant due to
			// mbean registered on each node

			if (dataSrcs != null) {
				for (int i = 0; i < dataSrcs.size(); i++) {
					ClusterDataSrc cds =
					    (ClusterDataSrc)dataSrcs.get(i);
					cds.cleanup();
				}
			}

			logger.info("-- session logging stopped -- " +
			    new Date());
			sessionLogFile.flush();
			sessionLogFile.close();

			lockfile.delete(); // best effort

		} catch (Exception e) {
			String[] i18nArgs = { e.getMessage() };
			String lMsg = I18n.getLocalized(logger,
			    "internal.error", i18nArgs);
			stderr.println("  " + lMsg);
				// can't call anything that uses
				// logs at this point
		}
	} // cleanup

	// =====================================================================
	//  Use of EngineListener interface is strictly for logging.
	//  Tallies are performed by the Engine using the report class.

	/* BEGIN JSTYLED */
	/* (non-Javadoc)
	 * @see com.sun.cluster.cfgchk.engine.EngineListener#checklistStarting(java.lang.String)
	 */
	/* END JSTYLED */
	public void checklistStarting() {
		// no extra indent
		showProgress(I18n.getLocalized(logger,
			"cfgchk.checklist.starting"));
	} // checklistStarting


	/* BEGIN JSTYLED */
	/* (non-Javadoc)
	 * @see com.sun.cluster.cfgchk.engine.EngineListener#checklistFinished(java.lang.String)
	 */
	/* END JSTYLED */
	public void checklistFinished() {
		// no extra indent
		showProgress(I18n.getLocalized(logger,
			"cfgchk.checklist.finished"));
	} // checklistFinished

	// these messages always shown
	public void checkRunFinished(int maxViolatedSeverity) {

		// msg #1
		// severty name not localized in order to
		// match reports which are not localized
		String[] i18nArgs = { CheckUtils.getSeverityName(
					      maxViolatedSeverity) };
		String lMsg = I18n.getLocalized(logger,
		    "cfgchk.check.run.finished.severity", i18nArgs);
		showMsg(lMsg);

		// bump our return code up into our special range
		// we'll pull it down again in ksh
		this.maxViolatedSeverity = maxViolatedSeverity + ExitCode.OK;

		// msg #2
		i18nArgs[0] = params.outputDir; // luckily same number of args
		lMsg = I18n.getLocalized(logger,
		    "cfgchk.check.run.finished.logs", i18nArgs);
		showMsg(lMsg);
	} // checkRunFinished

	/* BEGIN JSTYLED */
	/* (non-Javadoc)
	 * @see com.sun.cluster.cfgchk.engine.EngineListener#checkStarting(java.lang.String)
	 */
	/* END JSTYLED */
	public void checkStarting(String checkID, String hostnames,
	    String title) {
		int titleLen = 45;
		checkID = StringUtils.padString(checkID,
		    displayMsgPad - 2, '.');
		StringBuffer displayTitleSB = new StringBuffer("");
		displayTitleSB.append(
			StringUtils.padString(title, titleLen, ' ').
				substring(0, titleLen-1));
		if (title.length() > titleLen) { displayTitleSB.append("..."); }
		String[] i18nArgs = { hostnames, checkID };
		String lMsg =  I18n.getLocalized(logger,
		    "cfgchk.check.starting", i18nArgs);
		showProgressIndent(lMsg + "  " + displayTitleSB.toString());

	} // checkStarting


	/* BEGIN JSTYLED */
	/* (non-Javadoc)
	 * @see com.sun.cluster.cfgchk.engine.EngineListener#checkPassed(java.lang.String)
	 */
	/* END JSTYLED */
	public void checkPassed(String checkID, String hostnames) {
		checkID = StringUtils.padString(checkID, displayMsgPad, ' ');
		String[] i18nArgs = { hostnames, checkID };
		String lMsg =  I18n.getLocalized(logger, "cfgchk.check.passed",
		    i18nArgs);
		showProgressIndent(lMsg);
	} // checkPassed

	public void checkViolated(String checkID, String hostnames) {
		checkID = StringUtils.padString(checkID, displayMsgPad, ' ');
		String[] i18nArgs = { hostnames, checkID };
		String lMsg =  I18n.getLocalized(logger,
		    "cfgchk.check.violated", i18nArgs);
		showProgressIndent(lMsg);
	} // checkViolated

	public void checkWarning(String checkID, String hostnames) {
		checkID = StringUtils.padString(checkID, displayMsgPad, ' ');
		String[] i18nArgs = { hostnames, checkID };
		String lMsg =  I18n.getLocalized(logger,
		    "cfgchk.check.warning.only", i18nArgs);
		showProgressIndent(lMsg);
	} // checkViolated

	public void checkInfoOnly(String checkID, String hostnames) {
		checkID = StringUtils.padString(checkID, displayMsgPad, ' ');
		String[] i18nArgs = { hostnames, checkID };
		String lMsg =  I18n.getLocalized(logger,
		    "cfgchk.check.info.only", i18nArgs);
		showProgressIndent(lMsg);
	} // checkInfoOnly

	public void checkNA(String checkID, String hostnames) {
		checkID = StringUtils.padString(checkID, displayMsgPad, ' ');
		String[] i18nArgs = { hostnames, checkID };
		String lMsg =  I18n.getLocalized(logger, "cfgchk.check.NA",
		    i18nArgs);
		showProgressIndent(lMsg);
	} // checkNA


	public void checkSkipped(String checkID, String hostnames,
	    String reason) {
		checkID = StringUtils.padString(checkID, displayMsgPad, ' ');
		String[] i18nArgs = { hostnames, checkID, reason };
		String lMsg =  I18n.getLocalized(logger,
		    "cfgchk.check.skipped", i18nArgs);
		showProgressIndent(lMsg);
	} // checkSkipped

	public void infoMessage(String checkID, String msg, String hostnames) {
		checkID = StringUtils.padString(checkID, displayMsgPad, ' ');
		String[] i18nArgs = { hostnames, checkID, msg };
		String lMsg =  I18n.getLocalized(logger,
		    "cfgchk.check.info.message", i18nArgs);
		showProgressIndent(lMsg);
	} // extraMessage

	public void insufficentData(String checkID, String msg,
	    String hostnames) {
		checkID = StringUtils.padString(checkID, displayMsgPad, ' ');
		String[] i18nArgs = { hostnames, checkID, msg };
		String lMsg =  I18n.getLocalized(logger,
		    "cfgchk.check.insufficient.data", i18nArgs);
		showErrIndent(lMsg);
	} // insufficentData


	/* BEGIN JSTYLED */
	/* (non-Javadoc)
	 * @see com.sun.cluster.cfgchk.engine.EngineListener#checkCrashed(java.lang.String)
	 */
	/* END JSTYLED */
	public void checkCrashed(String checkID, CheckExecutionException e,
	    String hostnames) {
		checkID = StringUtils.padString(checkID, displayMsgPad, ' ');
		String[] i18nArgs = { hostnames, checkID };
		String lMsg = I18n.getLocalized(logger, "cfgchk.check.crashed",
		    i18nArgs);
		showErrIndent(lMsg);
		// exception is logged by Engine.handleCrashedCheck()
	} // checkCrashed

	// honors verbose flag; I18n done by caller
	public void progressMsg(String msg) {
		showProgress(msg);
	} // progressMsg

	// honors verbose flag; I18n done by caller
	// slight extra indent
	private void showProgressIndent(String msg) {
		logger.info(msg);
		if (verbose) {
			stdout.println("   " + "  " + msg);
		}
	} // showProgressIndent

	// honors verbose flag; I18n done by caller
	private void showProgress(String msg) {
		logger.info(msg);
		if (verbose) {
			stdout.println("  " + msg);
		}
	} // showProgress


	// ignores verbose flag; I18n done by caller
	public void showMsg(String msg) {
		logger.info(msg);
		stdout.println("  " + msg);
	} // showMsg

	// ignores verbose flag; I18n done by caller
	public void showErr(String msg) {
		logger.info(msg);
		stderr.println("  " + msg);
	} // showErr

	// ignores verbose flag; I18n done by caller
	// indent to match showProgressIndent()
	public void showErrIndent(String msg) {
		logger.info(msg);
			stdout.println("   " + "  " + msg);
	} // showErr

	// =============================================================

	/*
	 * load user args into params
	 * validate values
	 * accumulate as many error messages as possible
	 * and return as List of I18n messages
	 * exceptions and error msgs s/b returned as ErrorMsg's
	 */
	private List processArgs(String[] args) throws CfgchkException {
		ArrayList errMsgs = new ArrayList();
		String errMsg = null;
			// holds I18n error messages from subroutines

		// load user-supplied args into the Params object
		try {
			loadParams(args);

		} catch (Exception ex) { // safety
			String[] i18nArgs = { ex.getMessage() };
			String lMsg = ErrorMsg.makeErrorMsg(logger,
			    "internal.error.load.params", i18nArgs);
			throw new CfgchkException(lMsg);
		}

		/*
		 * validate & process user entries in Params
		 */

		// can't specify both nodes and explorer archives
		// s/b caught in ksh but...
		logger.info("test: -n vs -e: both not allowed");
		if (params.explorerNames != null &&
		    params.nodeNames != null) {
			String lMsg = ErrorMsg.makeErrorMsg(logger,
			    "cfgchk.error.node.and.explorer");
			errMsgs.add(lMsg);
		}


		// make sure explorer names, if given, are OK
		logger.info("test: if explorer then validate");
		if (params.explorerNames != null) {
			errMsg = validateExplorerArgs(params.explorerNames);
			if (errMsg != null) { errMsgs.add(errMsg); }
		}

		// convert nodeid's to nodenames (only if in clustermode)
		// not if explorers cause p.nodeNames will be null
		logger.info("test: nodenames");
		if (params.nodeNames != null && params.isInClusterMode) {
			// has side-effect of 'normalizing' params.nodeNames
			// may *add* multiple I18n messages to errMsgs
			processNodeNames(errMsgs); // no Ex
		}


		/*
		 * if not in cluster mode or not using cacao
		 * then require either explorers or
		 * or allow single node checks only on localnode
		 */
		logger.info("test: !clustermode or !cacao: explorer or " +
		    "localnode only required");
		if ((!params.isInClusterMode || !params.usingCacao)
		    && params.explorerNames == null) {
			logger.info("test: !clustermode or !cacao: ...");

			// if -n specified make sure it's local node only
			// if user specifies a domain name this match
			// against commandNode will fail XXX
			if (params.nodeNames != null) {
				logger.info("test: !clustermode or !cacao: " +
				    "validate given nodenames");
				String[] nodenames =
					StringUtils.
					stringArrayFromEncodedString(
					    params.nodeNames,
						    LIST_ENCODING_SEP);

				if ((nodenames.length > 1) ||
				    (!nodenames[0].equals(
					    params.commandNode))) {
					/* BEGIN JSTYLED */
					String lMsg = ErrorMsg.makeErrorMsg(logger,
					    "cfgchk.error.explorer.or.localnode");
					/* END JSTYLED */
					errMsgs.add(lMsg);
				}
			} else {
				logger.info("test: !clustermode or !cacao: " +
				    "forcing nodelist to commandNode only");
				// if -n not specified then force
				// nodelist to localhost only
				params.nodeNames = params.commandNode;
			} // node specified?
		} // !clusterMode && !explorers

		// if no nodnames and no explorers then load default nodelist
		logger.info("test: load defaultnodelist");
		if (params.explorerNames == null &&
		    params.nodeNames == null) {
			try {
				loadDefaultNodelist();
			} catch (DataException dex) {
				logger.info("DataException: " + dex);
				String[] i18nArgs = { dex.getMessage() };
				String lMsg = ErrorMsg.makeErrorMsg(logger,
				    "internal.error.load.default.nodelist",
				    i18nArgs);
				throw new CfgchkException(lMsg);
			}
		}


		logger.fine("-- EXIT with numErrors: " + errMsgs.size());
		return errMsgs;
	} // processArgs

	/*
	 * validate form of input args
	 * check just a little on validity of explorer archives themselves
	 * fatal error messages s/b returned as ErrorMsg's
	 */
	private String validateExplorerArgs(String explorerNames) {
		logger.finest("-- ENTER: " + explorerNames);
		String lMsg = null;

		String[] explNames =
		    StringUtils.stringArrayFromEncodedString(explorerNames,
			LIST_ENCODING_SEP);

		if (explorerNames == null ||
		    explNames == null ||
		    explNames.length == 0) {
			// should never happen
			logger.warning("explorer names not supplied");
			lMsg = ErrorMsg.makeErrorMsg(logger,
			    "cfgchk.error.validate.explorer.names.missing");
			return lMsg; // early exit
		} else {
			logger.info("explNames.length: " + explNames.length);
			for (int i = 0; i < explNames.length; i++) {
				logger.info("explNames[i]: " + explNames[i]);
				if (!Utils.isValidExplorerArchive(explNames[i],
					logger)) {
					String[] i18nArgs = { explNames[i] };
					lMsg = ErrorMsg.makeErrorMsg(logger,
					    "cfgchk.error.not.explorer",
					    i18nArgs);
					break;
				}

			} // for explNames
		} // else

		logger.finest("-- EXIT-- ");
		return lMsg;
	} // validateExplorerArgs

	private void loadDefaultParams(CfgchkProperties props) {

		// adjust logging level
		logger.info("default logging level: " + params.loggingLevel);
		// props.list(stderr);
		String loggingLevel = CfgchkProperties.stringFromProps(
			Globals.LOGGING_LEVEL, params.loggingLevel, props);
		params.loggingLevel = loggingLevel;
		logger.info("new logging level: " + params.loggingLevel);
		LoggerUtils.setLoggingLevel(logger, params.loggingLevel);
			// IPEx
		RemoteLogger.setLoggingLevel(params.loggingLevel); // IPEx
			// commandNode

	} // loadDefaultParams

	/*
	 * Flag and argument syntax correctness has already been verified
	 * in the ksh wrapper, therefore, if a flag requires an argument
	 * it may safely be assumed to be present in i+1.
	 * Content of that argument will be validated in processArgs()
	 *
	 * No errors will be identified; no exceptions expected
	 */
	private void loadParams(String[] args) {
		logger.info("-- ENTER --");
		// existing params object is modified

		for (int i = 0; i < args.length; i++) {
			logger.info("arg(" + i + "): " + args[i]);

			if (args[i].equals(CLI_INCLUDECHECKIDS_OPTION)) {
				logger.info("got option: " +
				    CLI_INCLUDECHECKIDS_OPTION);
				params.includeCheckIDs = args[++i];
				logger.info("got arg: " +
				    params.includeCheckIDs);
				continue;
			}

			if (args[i].equals(CLI_EXPLORERNAMES_OPTION)) {
				logger.info("got option: " +
				    CLI_EXPLORERNAMES_OPTION);
				params.explorerNames = args[++i];
				logger.info("got arg: " + params.explorerNames);
				continue;
			}

			if (args[i].equals(CLI_EXCLUDECHECKIDS_OPTION)) {
				logger.info("got option: " +
				    CLI_EXCLUDECHECKIDS_OPTION);
				params.excludeCheckIds = args[++i];
				logger.info("got arg: " +
				    params.excludeCheckIds);
				continue;
			}
			if (args[i].equals(CLI_CHECKJARS_OPTION)) {
				logger.info("got option: " +
				    CLI_CHECKJARS_OPTION);
				params.checkJars = args[++i];
				logger.info("got arg: " + params.checkJars);
				continue;
			}
			if (args[i].equals(CLI_KEYWORDS_OPTION)) {
				logger.info("got option: " +
				    CLI_KEYWORDS_OPTION);
				params.keywords = args[++i];
				logger.info("got arg: " + params.keywords);
				continue;
			}
			if (args[i].equals(CLI_NODENAMES_OPTION)) {
				logger.info("got option: " +
				    CLI_NODENAMES_OPTION);
				params.nodeNames = args[++i];
				logger.info("got arg: " + params.nodeNames);
				continue;
			}
			if (args[i].equals(CLI_OUTPUTDIR_OPTION)) {
				logger.info("got option: " +
				    CLI_OUTPUTDIR_OPTION);
				String tmp = args[++i];
				// XMLResults needs a trailing slash
				params.outputDir =
				    StringUtils.forceTrailingChar(tmp, '/');
				logger.info("got arg: " + params.outputDir);
				continue;
			}
			if (args[i].equals(CLI_SORT_OPTION)) {
				logger.info("got option: " + CLI_SORT_OPTION);
				params.sortKeys = args[++i];
				logger.info("got arg: " + params.sortKeys);
				continue;
			}
			if (args[i].equals(CLI_MINSEVERITY_OPTION)) {
				logger.info("got option: " +
				    CLI_MINSEVERITY_OPTION);
				params.minSeverity =
				    processSeverityArg(args[++i]);
				logger.info("got arg: " + params.minSeverity);
				continue;
			}
			if (args[i].equals(CLI_VERBOSE_OPTION)) {
				logger.info("got option: " +
				    CLI_VERBOSE_OPTION);
				params.verbose = true; // boolean
				verbose = true;
				continue;
			}
			if (args[i].equals(CLI_LIST_OPTION)) {
				logger.info("got option: " + CLI_LIST_OPTION);
				params.list = true; // boolean
				logger.info("got arg: " + params.list);
				continue;
			}

		} // for args

		logger.info("params: " + params);
	} // loadParams


	/*
	 * createChecksCache()
	 *
	 * Not in a utility class because of compound behavior:
	 *	create & populate ChecksCache array
	 *	return List of (fatal) ErrorMsgs
	 */
	private List createChecksCache() throws CfgchkException {
		logger.info("-- ENTER -- ");
		ArrayList errMsgs = new ArrayList();
		String[] jarNamesList = CheckUtils.getAllCheckJarNames(params,
		    logger);
		checksCache = new ChecksCache[jarNamesList.length];
		JarFile jar = null;

		// for each jar in list...
		for (int i = 0; i < jarNamesList.length; i++) {
			String jarName = jarNamesList[i];
			logger.info("jarName: " + jarName);
			try {
				jar = new JarFile(jarName);
				JarEntry[] entries =
				    CheckUtils.getCheckJarEntries(jar, logger);
				ChecksCache cache = new ChecksCache(jar,
				    entries);
				checksCache[i] = cache;

			} catch (FileNotFoundException fnfex) {
				logger.info("FileNotFoundException" +
				    fnfex.getMessage());
				String[] i18nArgs = { jarName,
						      fnfex.getMessage() };
				String lMsg = ErrorMsg.makeErrorMsg(logger,
				    "cfgchk.error.jar.not.found", i18nArgs);
				errMsgs.add(lMsg);

			} catch (IOException ioex) {
				logger.info("IOException" + ioex.getMessage());
				String[] i18nArgs = { jarName,
						      ioex.getMessage() };
				String lMsg = ErrorMsg.makeErrorMsg(logger,
				    "cfgchk.error.jar.cant.read", i18nArgs);
				throw new CfgchkException(lMsg);
			}
		} // for jars

		logger.info("-- EXIT -- ");
		return errMsgs;
	} // createChecksCache

	// figure out default nodelist and write it into Params object
	// exercised only if in cluster mode
	private void loadDefaultNodelist() throws DataException {
		logger.info("-- ENTER  --");
		String nodelist = "localhost";

		if (params.isClusterNode) {
			if (params.isInClusterMode) {
				// am I in cluster mode?
				// yes: nodelist = all current members
				try {
					String[] clusterNodes =
					    ClusterUtils.getClusterNodenames(
						    logger);
					nodelist =
					    StringUtils.
					    encodedStringFromStringArray(
						    logger, clusterNodes,
							    LIST_ENCODING_SEP);
				} catch (DataException dex) {
					logger.info("DataException: " + dex);
					throw dex;
				}
			} else {
				// no: nodelist = localhost only
				logger.info("not in cluster mode");
			}
		} else {
			// not cluster node: nodelist = localhost
			logger.info("not a cluster node");
		}

		params.nodeNames = nodelist;
		logger.info("-- EXIT: " + nodelist);
	} // loadDefaultNodelist

	/*
	 * processNodeNames()
	 *	replace any nodeids in params.nodeNames
	 *	with nodenames
	 * requires clustermode
	 * error messages returned as ErrorMsg via input arg: errMsgs
	 */
	private void processNodeNames(List errMsgs) {
		logger.info("-- ENTER: " + params.nodeNames);
		HashSet hset = new HashSet(); // tmp to elim dups
		String nodelist = params.nodeNames;
		// params.nodeNames guaranteed non-null


		String[] ids = StringUtils.stringArrayFromEncodedString(
			nodelist, LIST_ENCODING_SEP);
		for (int i = 0; i < ids.length; i++) {

			logger.info("test as name: " + ids[i]);
			if (! validNodeName(ids[i])) {

				logger.info("test as nodeid: " + ids[i]);
				String nm = nodeIDtoName(ids[i]);
				logger.info("nm: " + nm);
				if (nm != null) {
					logger.info("updating ids " +
					    "array from: " + ids[i] + " to: " +
					    nm);
					ids[i] = nm;
				} else {
					logger.warning("Unrecognized node " +
					    "identifier: " + ids[i]);
					String[] i18nArgs = { ids[i] };
					/* BEGIN JSTYLED */
					String lMsg = ErrorMsg.makeErrorMsg(logger,
							"cfgchk.error.node.identifier", i18nArgs);
					/* END JSTYLED */
					errMsgs.add(lMsg);
					// accumulate all possible msgs
				}
			}
			hset.add(ids[i]); // overwrites dups
		} // for ids

		// convert hashset to string[]: recreate ids[]
		ids = new String[hset.size()];
		int index = 0;
		Iterator it = hset.iterator();
		while (it.hasNext()) {
			String nm = (String)it.next();
			ids[index++] = nm;
			logger.fine("final nodelist member: " + nm);
		}

		// update params.nodeNames
		nodelist = StringUtils.encodedStringFromStringArray(logger,
		    ids, LIST_ENCODING_SEP);
		params.nodeNames = nodelist;

		logger.info("-- EXIT --");
	} // processNodeNames

	// convert name to int
	// case-insensitive name string was validated in ksh
	private int processSeverityArg(String severityName) {
		logger.info("-- ENTER: " + severityName);
		int sev = Check.NUM_SEVERITIES; // effectively a no-op

		for (int i = 1; i < Check.SEVERITY_NAMES.length; i++) {
			if (Check.SEVERITY_NAMES[i].equalsIgnoreCase(
				    severityName)) {
				logger.info("match: " + i);
				sev = i;
				break;
			}
		}
		logger.info("-- EXIT: " + sev);
		return sev;
	} // processSeverityArg

	private boolean validNodeName(String candidate) {
		logger.info("-- ENTER -- ");
		boolean isValid = params.isClusterNodeName(candidate);
		logger.info("-- EXIT: " + isValid);
		return isValid;
	} // validNodeName

	private String nodeIDtoName(String nodeid) {
		String nm = params.nodeIDtoName(nodeid);
		logger.info("found name: " + nm + " for nodeid: " + nodeid);
		return nm;
	} // nodeIDtoName





	public CommandResult getCommandResult() {
		return commandResult;
	} // getCommandResult


	/*
	 * side effect of setting application logger & sessionLogFile
	 * no need to trap exceptions inside because we can't log them
	 */
	private void setupLoggers(String logfileName, String localNodename,
	    boolean fromGui) throws InvalidParameterException {
		/*
		  potential for gui integration  XXX
		    if (fromGUI) {
			 try {
				 outputDir = createOutputDir();
			} catch (InvalidParameterException ipex) {
				// send rejection to gui
			 }
		    } // else if from cli will have already been created by ksh
		*/

		boolean rotatable = false;
		sessionLogFile = LoggerUtils.initTimestampFileHandler(
			logfileName, rotatable); // IPEx

		logger = LoggerUtils.initSessionLogger(sessionLogFile,
		    Globals.DEFAULT_LOGGING_LEVEL); // No Ex
		logger.info("Logger created: " + new Date() +
		    " at logging level: " + Globals.DEFAULT_LOGGING_LEVEL);

		// call idempotent init as early as possible
		// any class can now call RL.getRemoteLogger()
		// this call made in case we're invoked without cacao
		RemoteLogger.initRemoteLogger(
			localNodename, Globals.DEFAULT_LOGGING_LEVEL); // No Ex
	} // setupLogger


	private CommandResult errListToCR(int exitCode, List errMsgs) {
		CommandResult cr = new ErrorResult(exitCode);
		Iterator it = errMsgs.iterator();
		while (it.hasNext()) {
			String errMsg = (String)it.next(); // already I18n
			logger.warning("errMsg: " + errMsg);
			cr.setMessage(errMsg);
		}
		return cr;
	} // errListToCR

	// =========================================

	// display to user with msgid's on stderr
	// called only by Cfgchk.main() (which means cli but no cacao)
	private static void displayErrorResult(ErrorResult er,
	    PrintWriter stderr) {

		List errMsgs = er.getMessages();
		Iterator it = errMsgs.iterator();
		while (it.hasNext()) {
			String lMsg = (String)it.next();
			stderr.println(lMsg); // errMsgs already I18n
		}

	} // displayErrorResult

	// =========================================

	// this path used only from cli when not using cacao
	public static void main(String[] argv) {
		try {
			Cfgchk cfgchk = null;
			boolean usingCacao = false;
			boolean fromGUI = false;
			CommandResult cr = null;

			PrintWriter stdout = new PrintWriter(System.out, true);
			PrintWriter stderr = new PrintWriter(System.err, true);

			// to specify encoding use this form
			// PrintWriter stdout = new PrintWriter(
			//  new OutputStreamWriter(System.out, encoding), true);

			// arg[0] is an 'encoded' string of args from wrapper
			cfgchk = new Cfgchk(argv[0], stdout, stderr,
			    usingCacao, fromGUI);
			cr = cfgchk.getCommandResult();

			if (cr instanceof ErrorResult) {
				displayErrorResult((ErrorResult)cr, stderr);
			}

			stdout.close();
			stderr.close();
			System.exit(cr.getExitCode());

		} catch (Exception ex) {
			// should never happen
			String[] i18nArgs = { ex.getMessage() };
			String lMsg = ErrorMsg.makeErrorMsg(/* logger */ null,
			    "internal.error", i18nArgs);
			System.err.println(lMsg);
			System.exit(ExitCode.ERR_INTERNAL);
		}
	} // main

} // class
