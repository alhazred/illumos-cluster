/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)BasicDataSrc.java	1.3	08/09/22 SMI"
 */
package com.sun.cluster.cfgchk.datasrc;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import java.util.List;

import com.sun.cluster.cfgchk.datasrc.fileObject.DirectoryListing;
import com.sun.cluster.cfgchk.datasrc.fileObject.TextFile;

import com.sun.cacao.invocation.InvocationStatus;


/**
 *
 * Fundamental DataSrc interface. Extended by SolarisDataSrc
 * interface which in turn is extended by ClusterDataSrc.
 *
 * Implemented by LiveBasicDataSrc and ExplorerBasicDataSrc.
 *
 * Used indirectly by Cfgchk.
 */

public interface BasicDataSrc {

	public String getDataSrcName();
	public String getDataSrcVersion();
	public boolean isLiveDataSource();

	public String getSystemName() throws DataException;


	public List getTextFileStrings(String filename, String target)
		throws DataException, FileNotFoundException, IOException;
	public TextFile getTextFile(String filename)
		throws DataException, FileNotFoundException, IOException;
	public TextFile getTextFile(String filename, boolean cache)
		throws DataException, FileNotFoundException, IOException;

	public File getFile(String filename)
		throws DataException, FileNotFoundException;

	public DirectoryListing getDirectoryListing(String dir)
		throws DataException, FileNotFoundException, IOException;

	public DirectoryListing getDirectoryListing(String dir, String flags)
		throws DataException, FileNotFoundException, IOException;

	public boolean isPackageInstalled(String packageName)
		throws DataException, InsufficientDataException;

	public int runCommandInt(String cmd[])
		throws DataException, InsufficientDataException;

	public String[] runCommandStrArray(String cmd[])
		throws DataException, InsufficientDataException;

	public InvocationStatus runCommand(String[] cmd)
		throws DataException, InsufficientDataException;

	public String dumpCache();
	public void cleanup();

	public String toString();

} // interface
