/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)Globals.java	1.3	08/09/22 SMI"
 */
package com.sun.cluster.cfgchk;

/**
 * Constants used by the cfgchck project. No I18N.
 */

public interface Globals {
	/* BEGIN JSTYLED */

	public final static String PROGNAME = "cluster";
	// appears in new cli error messages

	public final static String DOMAIN_CFGCHK = "com.sun.cluster.cfgchk.agent.cfgchk";
	public static final String CHKCFG_VERSION = "1.0";
	// needed for XMLResult
	// new cli version cmd handled in ksh

	public static final String CHECKS_PKG = "com.sun.cluster.cfgchk.check.";
	public static final String BASE_CHECK_CLASSNAME = "Check";

	public static String xmlResultsName = "checkresults.xml";
        public static String textResultsName = "checkresults.txt";

	// dir & filename definitions: must be in synch w/ cfgcheck.ksh
 	public static final String ETCDIR = "/etc/default/";
 	public static final String LIBDIR = "/usr/cluster/lib/cfgchk/";
	public static final String PROPSFILE = ETCDIR + "cfgchk";
	public static final String AUX_DATA_FILENAME = LIBDIR + "cfgchk_aux.dat";
	public static final String FACTORY_CHECKS_JAR = LIBDIR + "cfgchk_checks.jar";

	public static final String LOGGING_LEVEL = "LOGGING_LEVEL";
	public static final String DEFAULT_LOGGING_LEVEL = "FINE";
	public static final String REMOTE_LOGS_DIR = "/var/cluster/logs/cluster_check";
	public static final String REMOTE_LOG = REMOTE_LOGS_DIR + "/remote.log";
	// nodename and a serial are tacked on at runtime (RemoteLogger.java)

	public static final String CFGCHK_LOCKFILE = REMOTE_LOGS_DIR + "/cfgchk.lck";


	// arguments passed from wrapper
	// OPTION names must agree with ksh
        public static final String ARG_ENCODING_SEP = "|"; // must agree with ksh wrapper
        public static final String LIST_ENCODING_SEP = ","; // must agree with docs
        public static final String EXPLORER_ARCH_SUFFIX = ".tar.gz"; // must agree with explorer

	public static final String CLI_INCLUDECHECKIDS_OPTION = "INCLUDECHECKIDS";
	public static final String CLI_EXPLORERNAMES_OPTION = "EXPLORERS";
	public static final String CLI_EXCLUDECHECKIDS_OPTION = "EXCLUDECHECKIDS";
	public static final String CLI_CHECKJARS_OPTION = "CHECKJARS";
	public static final String CLI_KEYWORDS_OPTION = "KEYWORDS";
	public static final String CLI_NODENAMES_OPTION = "NODES";
	public static final String CLI_OUTPUTDIR_OPTION = "OUTPUTDIR";
	public static final String CLI_SORT_OPTION = "SORT";
	public static final String CLI_MINSEVERITY_OPTION = "SEVERITY";

	public static final String CLI_VERBOSE_OPTION = "VERBOSE";
	public static final String CLI_LIST_OPTION = "LISTCHECKS";
	public static final String CLI_FORCE_OPTION = "FORCECHECKS";


	// property file (/etc/default) keys
	public static final String DEFAULT_OUTPUT_DIR_BASE = "DEFAULT_OUTPUT_DIR_BASE";
	public static final String CLUSTERCHECKLIST = "CLUSTERCHECKLIST";
	public static final String LOGFILENAME = "LOGFILENAME";


	/*
	 * I18n
	 *
	 * Location and names of I18n message files:
	 *
	 * Message files to be installed under /usr/cluster/lib/cfgchk/resources/.
	 * Run-time CLASSPATH to include /usr/cluster/lib/cfgchk/.
	 * 'Textdomain' is I18_BASE + base filename without the ".properties. extension.
	 */
	public static String I18_BASE = "resources";
	public static String I18_RESOURCES_FILE = Globals.I18_BASE + ".cfgchk";
	public static String TEXTDOMAIN = I18_RESOURCES_FILE;

	/* END JSTYLED */
} // class
