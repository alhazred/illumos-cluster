/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)CheckS6708644.java	1.3	08/09/09 SMI"
 */
package com.sun.cluster.cfgchk.check;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import com.sun.cluster.cfgchk.BasicCheck;
import com.sun.cluster.cfgchk.Check;
import com.sun.cluster.cfgchk.CheckExecutionException;

import com.sun.cluster.cfgchk.datasrc.DataException;
import com.sun.cluster.cfgchk.datasrc.InsufficientDataException;
import com.sun.cluster.cfgchk.datasrc.dataObject.VfstabEntry;
import com.sun.cluster.cfgchk.datasrc.fileObject.Vfstab;
import com.sun.cluster.cfgchk.utils.StringUtils;


/* BEGIN JSTYLED */
/**
 *
 * 6708644 - 2125
 * Global Veritas File Systems (VxFS) without logging enabled might lead to unpredictable results.
 *
 */
/* END JSTYLED */

/*
 * No I18n of any strings in this class.
 * Exceptions thrown from Checks are caught and handled by Engine
 *
 * Lines with trailing 'TOC' comment are auto-gathered into
 * table of contents listing of all checks.
 * typically just by logging. Messages are never displayed to user.
 */
public class CheckS6708644 extends BasicCheck {

	private ArrayList vxfsEntries = null;

	public CheckS6708644() {
		checkID = "S6708644"; // TOC
		severity = Check.CRITICAL; // TOC

		// sccs keyword: must be 1.3 when checked out under sccs
		version = "1.3";
		// sccs keyword: must be 08/09/09 when checked out under sccs
		revisionDate = "09/12/08";

		keywords.add("SunCluster3");
		keywords.add("single");
		keywords.add("vfstab");
		keywords.add("vxfs");
		keywords.add("global_filesystem");

		/* BEGIN JSTYLED */
		problemStatement = "Global Veritas File Systems (VxFS) without logging enabled might lead to unpredictable results."; // TOC
		/* END JSTYLED */

		// check-specific data
		vxfsEntries = new ArrayList();
	} // ()

	/**
	 * @return a string describing the logic of the applicability test
	 */
	protected String assignApplicabilityLogic() {
		/* BEGIN JSTYLED */
		String aLogic = "Applicable only if VxFS is installed.";
		/* END JSTYLED */
		return aLogic;
	} // assignApplicabilityLogic


	/**
	 * @return a string describing the logic of the check
	 */
	protected String assignCheckLogic() {
		/* BEGIN JSTYLED */
		String cLogic = "Examine each uncommented /etc/vfstab *global* entry for filesystem type 'vxfs'. For each vxfs entry, check to see if logging is specified as a mount option. If any global vxfs line does not specify logging, this check fails.";
		/* END JSTYLED */
		return cLogic;
	} // assignCheckLogic


	public String getAnalysis() {
		/* BEGIN JSTYLED */
		StringBuffer analysis = new StringBuffer("");

		if (vxfsEntries.size() > 0) {
			analysis.append("One or more global VxFS filesystems were found that do not have logging enabled:\n\n");
		}

		Iterator it = vxfsEntries.iterator();
		String data = null;
		while (it.hasNext()) {
			data = (String)it.next();
			analysis.append("\t\t" + data + "\n");
		}

		analysis.append("\n\tIt is necessary to enable logging for VxFS support on Sun Cluster");
		/* END JSTYLED */
		return analysis.toString();
	} // getAnalysis

	public String getRecommendations() {
		/* BEGIN JSTYLED */
		String recommend = "Manually add the \"log\" option to each global VxFS filesystem's \"mount options\" list in the /etc/vfstab file.  It might be necessary to umount and remount the affected filesystems or reboot the node to enable logging.";
		/* END JSTYLED */

		return recommend;
	} // getRecommendations


	// applies only if VxFS is installed
	public boolean isApplicable() throws CheckExecutionException {
		logger.info("isApplicable() -- ENTER --");
		boolean applicable = false;

		try {
			if (getDataSrc().isPackageInstalled("VRTSvxfs")) {
				applicable = true;
			}
		} catch (InsufficientDataException e) {
			logger.warning("InsufficientDataException: " +
			    e.getMessage());
			applicable = false;
			setInsufficientDataMsg(e.getMessage());
		} catch (DataException dex) {
			logger.warning("DataException: " + dex);
				// logged via handleCrashedCheck()
			throw new CheckExecutionException(dex);
		} catch (Exception ex) {
			logger.warning("Exception: " + ex);
				// logged via handleCrashedCheck()
			throw new CheckExecutionException(ex);
		}

		logger.info("isApplicable(): " + applicable);
		return applicable;
	} // isApplicable

	/*
	 * for each global entry:
	 * if is of type 'vxfs'
	 * then check for 'log' in options
	 */
	public boolean doCheck() throws CheckExecutionException {
		logger.info("-- ENTER --");
		boolean passes = true;

		try {

			Vfstab vfstab = null;
			String infoMsg = null;
			String nodename = getNodename();
			logger.info("loading Vfstab for " + nodename);
			vfstab = getDataSrc().getVfstab(nodename);
			infoMsg = vfstab.getInfoMessage();
			logger.info("infoMsg: >>" + infoMsg + "<<");
			if ((infoMsg != null) && !infoMsg.equals("")) {
				appendInfoMsg(infoMsg);
				logger.info("infoMessage from load vfstab: " +
				    infoMsg);
			}
			VfstabEntry[] vfstabEntries =
			    vfstab.getGlobalEntries(false);
				// do not include node@ lines

			// pick out vxfs lines
			for (int i = 0; i < vfstabEntries.length; i++) {
				VfstabEntry entry = vfstabEntries[i];
				if (entry == null) { continue; }
					// possible for global entries
					//  array to be oversized and
					//  have null entries
				logger.info("entry mountPoint: " +
				    entry.getMountPoint());
				String fsType = entry.getFsType();
				logger.info("entry fsType: " + fsType);

				if (!fsType.equals("vxfs")) { continue; }

				ArrayList options =
				    entry.getVfstabMountOptions();
				if (!options.contains("log")) {
					passes = false;
					vxfsEntries.add(entry.getRawEntry());
				}

			} // for vfstab global lines

		} catch (DataException e) {
			logger.warning("DataException(): " + e);
			throw new CheckExecutionException(e);
		} catch (Exception e) {
			logger.warning("Exception(): " + e.getMessage());
			throw new CheckExecutionException(e);
		}

		logger.info("passes: " + passes);
		return passes;
	} // doCheck

} // class

