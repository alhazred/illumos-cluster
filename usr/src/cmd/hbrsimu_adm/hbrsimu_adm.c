/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma	ident	"@(#)hbrsimu_adm.c 1.2     08/05/20 SMI"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <limits.h>
#include <sys/time.h>
#include <locale.h>

#include <fmm/hbrsimu.h>

int fifo_id = -1;


int
hbsimu_fifo_open(void)
{
	char fifo_name[PATH_MAX];
	int result = 0;
	mode_t oldMask;

	(void) sprintf(fifo_name, "%s/%s", HBSIMU_TMP_DIR, HBSIMU_FIFO);

	oldMask = umask(0);
	if ((fifo_id = open(fifo_name, O_WRONLY | O_NONBLOCK)) == -1) {
		result = -1;
		(void) umask(oldMask);
		(void) remove(fifo_name);
	}

	(void) umask(oldMask);
	return (result);
}


int
remove_leading_space(char *command)
{
	char *from = command;
	char *to = command;

	while (*from == ' ') {
		from++;
	}

	do {
		if (*from != '\n')
			*to++ = *from;
	} while (*from++ != '\0');

	return (to - command -1);
}


enum {
	UP,
	DOWN,
	EXIT,
	END_OF_CHOICES
};

struct {
	int value;
	char tag[20];
} menu[] = {
	{ UP, "up"},
	{ DOWN, "down"},
	{ EXIT, "exit"},
	{ END_OF_CHOICES, ""}
};

int
matchparametercommand(char *cmd)
{
	int c;

	for (c = 0; menu[c].value != END_OF_CHOICES; c++) {
		if (strcmp(cmd, menu[c].tag) == 0)
			return (c);
	}
	return (-1);
}


int
read_command(char *command)
{
	char *to = command;
	char *from = command;
	char *cmd = strdup(command);

	while ((*from != ' ') && (*from != '\0')) {
		from++;
	}

	do {
		*to++ = *from;
	} while (*from++ != '\0');
	cmd[from - to] = '\0';

	return (matchparametercommand(cmd));
}


void
read_node_range(char *command, long *min, long *max)
{
	*min = strtol(command, &command, 10);
	if (*command == '-') {
		command++;
		*max = strtol(command, &command, 10);
	} else
		*max = *min;
}


void
ask_for_choice(int *intChoice, long *min, long *max)
{
	char strChoice[32] = "";
	int length;

loop:
	(void) fflush(stdin);
	(void) printf(gettext("\n## Your choice: "));
	(void) fgets(strChoice, 32, stdin);
	length = remove_leading_space(strChoice);
	if (length == 0)
		goto loop;
	*intChoice = read_command(strChoice);
	read_node_range(strChoice, min, max);
}

int
hbsimu_fifo_write(int cmd, int nodeid)
{
	hbsimu_msg_t message;
	struct timeval tv;

	(void) gettimeofday(&tv, NULL);

	message.nodeid = (unsigned int) nodeid;
	message.incarnation = (unsigned int) tv.tv_sec;
	switch (cmd) {
	case UP:
		message.event = F_NODE_UP;
		break;
	case DOWN:
		message.event = F_NODE_DOWN;
		break;
	default:
		break;
	}

	if (write(fifo_id, &message, sizeof (hbsimu_msg_t))
	    != (ssize_t)sizeof (hbsimu_msg_t)) {
		(void) fprintf(stderr, gettext("write failed\n"));
		return (-1);
	}
	return (0);
}


void
main(void)
{
	boolean_t go_on = B_TRUE;
	int choice;
	long min_node, max_node;
	long ii;

	(void) setlocale(LC_ALL, "");
	(void) textdomain(TEXT_DOMAIN);
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	if (hbsimu_fifo_open() != 0) {
		(void) fprintf(stderr, gettext("Unable to connect to fmmd\n"));
		exit(1);
	}

	while (go_on) {
		ask_for_choice(&choice, &min_node, &max_node);
		switch (choice) {
		case EXIT:
			go_on = B_FALSE;
			break;
		case UP:
			if ((min_node > 0) && (max_node >= min_node)) {
				for (ii = min_node; ii <= max_node; ii++)
					(void) hbsimu_fifo_write(UP, ii);
			}
			break;
		case DOWN:
			if ((min_node > 0) && (max_node >= min_node)) {
				for (ii = min_node; ii <= max_node; ii++)
					(void) hbsimu_fifo_write(DOWN, ii);
			}
			break;
		default:
			(void) printf(gettext("Unknown choice\n"));
			break;
		}
	}


}
