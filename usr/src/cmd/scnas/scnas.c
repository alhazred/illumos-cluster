/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scnas.c	1.12	08/05/20 SMI"

/*
 * scnas(1M)
 */

#include <scnas.h>
#include <sys/cl_cmd_event.h>
#include <sys/stat.h>
#include <sys/cl_auth.h>
#include <rgm/sczones.h>

static char *progname;

static void usage(FILE *out);

static void print_help_add(FILE *out);
static void print_help_change(FILE *out);
static void print_help_remove(FILE *out);
static void print_help_print(FILE *out);

static scnas_errno_t
    scnascmd_add(int argc, char **argv, char **messages, uint_t uflag);
static scnas_errno_t
    scnascmd_remove(int argc, char **argv, char **messages, uint_t uflag);
static scnas_errno_t
    scnascmd_change(int argc, char **argv, char **messages, uint_t uflag);
static scnas_errno_t
    scnascmd_print(int argc, char **argv, char **messages, uint_t uflag);

/*
 * main
 */
int
main(int argc, char **argv)
{
	int c;
	uint_t i;
	uint_t aflg, cflg, rflg, Hflg, pflg, unflg;
	scnas_errno_t scnas_err;
	scnas_errno_t (*scnascmd)(int, char **, char **, uint_t uflag);
	char *messages = (char *)0;
	char *msg, *last;
	uint_t ismember;
	int exitstatus;
	FILE *out;
	scconf_errno_t scconf_rstatus;
	char errbuf[SCNAS_MAXSTRINGLEN];


	/* Initialize and demote to normal uid */
	cl_auth_init();
	cl_auth_demote();

	scnascmd = NULL;
	aflg = cflg = rflg = pflg = Hflg = unflg = 0;

	/* Set the program name */
	if ((progname = strrchr(argv[0], '/')) == NULL) {
		progname = argv[0];
	} else {
		++progname;
	}

	/* I18N housekeeping */
	(void) setlocale(LC_ALL, "");
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);
	(void) textdomain(TEXT_DOMAIN);

	if (sc_zonescheck() != 0)
		return (1);

	/* Process options */
	while ((c = getopt(argc, argv, "acrpnHh:t:f:o:"))
	    != EOF) {
		switch (c) {
		case 'a':
			cl_auth_check_command_opt_exit(*argv, "-a",
			    CL_AUTH_DEVICE_MODIFY, SCNAS_EPERM);
			aflg = 1;
			scnascmd = scnascmd_add;
			break;

		case 'c':
			cl_auth_check_command_opt_exit(*argv, "-c",
			    CL_AUTH_DEVICE_MODIFY, SCNAS_EPERM);
			cflg = 1;
			scnascmd = scnascmd_change;
			break;

		case 'r':
			cl_auth_check_command_opt_exit(*argv, "-r",
			    CL_AUTH_DEVICE_MODIFY, SCNAS_EPERM);
			rflg = 1;
			scnascmd = scnascmd_remove;
			break;

		case 'p':
			cl_auth_check_command_opt_exit(*argv, "-p",
			    CL_AUTH_DEVICE_READ, SCNAS_EPERM);
			pflg = 1;
			scnascmd = scnascmd_print;
			break;

		case 'H':
			++Hflg;
			break;

		case 'n':
			unflg = 1;
			break;

		case '?':
			usage(stderr);
			exit(SCNAS_EUSAGE);

		default:
			break;
		}
	}

	/* Make sure no additional args */
	if (optind != argc) {
		usage(stdout);
		exit(SCNAS_EUSAGE);
	}

	/* reset optind */
	optind = 1;

	/* Check the basic options */
	i = aflg + cflg + rflg + pflg;
	if (Hflg) {
		/* if no basic flags, print help for everything */
		if (i == 0) {
			aflg = cflg = rflg = pflg = 1;
		}
	} else {
		if ((i == 0) || (i > 1)) {
			usage(stdout);
			exit(SCNAS_EUSAGE);
		}
	}

	/* if Hflg is set, just print help, and exit */
	if (Hflg) {

		/* if help everything, include usage message */
		if (aflg && cflg && rflg && pflg) {
			(void) putc('\n', stdout);
			usage(stdout);
		}

		/* Print help */
		if (aflg) {
			print_help_add(stdout);
		}

		if (cflg) {
			print_help_change(stdout);
		}

		if (rflg) {
			print_help_remove(stdout);
		}

		if (pflg) {
			print_help_print(stdout);
		}

		exit(SCNAS_NOERR);
	}

	/* promote to euid=0 */
	cl_auth_promote();

	/* Make sure we are active in the cluster */
	ismember = 0;
	scconf_rstatus = scconf_ismember(0, &ismember);
	scnas_err = scnas_conv_scconf_err(scconf_rstatus);

	if (scnas_err != SCNAS_NOERR) {
		scnas_strerr(errbuf, scnas_err);
		(void) fprintf(stderr, "%s:  %s.\n",
		    progname, errbuf);
		exit(scnas_err);
	}

	if (!ismember) {
		(void) fprintf(stderr, gettext(
		    "%s:  This node is not currently "
		    "in the cluster.\n"), progname);
		exit(scconf_rstatus);
	}

	/* check for usage errors, then run the options */
	if (scnascmd == NULL) {
		usage(stdout);
		exit(SCNAS_EUSAGE);
	}

	/* Initialize cl_cmd_event library */
	if (!unflg && (aflg || cflg || rflg)) {
		cl_cmd_event_init(argc, argv, &exitstatus);
	}

	/* Run the command */
	exitstatus = scnascmd(argc, argv, &messages, unflg);

	/* Print msgbuffer */
	if ((messages != (char *)0) && (exitstatus)) {
		/* If non-zero exitstatus, use stderr, else use stdout */
		out = (exitstatus) ? stderr : stdout;

		msg = strtok_r(messages, "\n", &last);
		while (msg != NULL) {
			(void) fprintf(out, "%s:    %s\n", progname, msg);
			msg = strtok_r(NULL, "\n", &last);
		}
	}

	if (messages) {
		free(messages);
	}

	return (exitstatus);
}

/*
 * usage
 *
 *	Print a simple usage message to stderr.
 */
static void
usage(FILE *out)
{
	(void) fprintf(out, "%s:  %s -a|-c|-r|-p [-H] [-n] [options]\n",
	    gettext("usage"), progname);
	(void) fprintf(out, "\t%s [-H]\n",
	    progname);

	(void) putc('\n', out);
}

/*
 * print_help_add
 *
 *	Print a help message to "out" for the "add" form of the command.
 */
static void
print_help_add(FILE *out)
{
	(void) fprintf(out, "%s (%s -a):\n",
	    gettext("Add options"), progname);

	(void) fprintf(out,
	    "\t-h filer_name -t filer_type -o userid=userid\n");
	(void) fprintf(out,
	    "\t-h filer_name -t filer_type -o userid=userid -f input_file\n");
	(void) fprintf(out,
	    "\t-n -h filer_name -t filer_type -o userid=userid "
	    "-f input_file\n");
	(void) fprintf(out, "\t-H\n");

	(void) putc('\n', out);
}

/*
 * print_help_change
 *
 *	Print a help message to "out" for the "change" form of the command.
 */
static void
print_help_change(FILE *out)
{
	(void) fprintf(out, "%s (%s -c):\n",
	    gettext("Change options"), progname);

	(void) fprintf(out, "\t-h filer_name -o userid=userid\n");
	(void) fprintf(out, "\t-h filer_name -o userid=userid -f input_file\n");
	(void) fprintf(out, "\t-n -h filer_name -o userid=userid "
	    "-f input_file\n");
	(void) fprintf(out, "\t-H\n");

	(void) putc('\n', out);
}

/*
 * print_help_remove
 *
 *	Print a help message to "out" for the "remove" form of the command.
 */
static void
print_help_remove(FILE *out)
{
	(void) fprintf(out, "%s (%s -r):\n",
	    gettext("Remove options"), progname);

	(void) fprintf(out, "\t-h filer_name\n");
	(void) fprintf(out, "\t-H\n");

	(void) putc('\n', out);
}

/*
 * print_help_print
 *
 *	Print a help message to "out" for the "print" form of the command.
 */
static void
print_help_print(FILE *out)
{
	(void) fprintf(out, "%s (%s -p):\n",
	    gettext("Print options"), progname);

	(void) fprintf(out, "\t-h filer_name -t filer_type\n");
	(void) fprintf(out, "\t-h filer_name\n");
	(void) fprintf(out, "\t-t filer_type\n");
	(void) fprintf(out, "\t[ %s ]\n", gettext("no option"));

	(void) fprintf(out, "\t-H\n");
}

/*
 * scnascmd_add
 *
 *	The "add" form of scnas command.
 *
 *	If "uflag" is set, just check for usage errors.
 */
static scnas_errno_t
scnascmd_add(int argc, char **argv, char **messages, uint_t uflag)
{
	scnas_errno_t scnas_err = SCNAS_NOERR;
	char errbuff[SCNAS_MAXSTRINGLEN];
	char *filer_name = NULL;
	char *filer_type = NULL;
	char *input_file = NULL;
	char *specific_options  = NULL;
	scnas_filer_prop_t *proplist = (scnas_filer_prop_t *)0;
	scconf_errno_t scconf_err = SCCONF_NOERR;
	char input_buf[SCNAS_MAXSTRINGLEN];
	char *passwd = NULL;
	char *p_chk = NULL;
	int c;
	uint_t found_key;
	boolean_t no_passwd = B_FALSE;
	boolean_t prop_required = B_FALSE;

	/* Process the options */
	optind = 1;
	while ((c = getopt(argc, argv, "acrpnh:t:o:f:")) != EOF) {
		switch (c) {
		case 'a':
			break;

		case 'c':
		case 'r':
		case 'p':
			scnas_err = SCNAS_EUSAGE;
			goto cleanup;

		case 'n':
			/* Check usage */
			break;

		case 'h':
			/* Filer name */
			filer_name = optarg;
			break;

		case 't':
			/* Filer type */
			filer_type = optarg;
			break;

		case 'f':
			/* Input file */
			input_file = optarg;
			break;

		case 'o':
			/* Specific options */
			specific_options = optarg;
			break;

		default:
			scnas_err = SCNAS_EUSAGE;
			goto cleanup;
		}
	}

	/* Check for required arguments */
	if (!filer_name) {
		(void) fprintf(stderr, gettext(
		    "%s:  A suboption \"%s\" is required for "
		    "attaching a NAS filer. \n"),
		    progname, SUBOPT_FILERNAME);
		scnas_err =  SCNAS_EUSAGE;
		goto cleanup;
	}

	if (!filer_type) {
		(void) fprintf(stderr, gettext(
		    "%s:  A suboption \"%s\" is required for "
		    "attaching a NAS filer. \n"),
		    progname, SUBOPT_FILERTYPE);
		scnas_err =  SCNAS_EUSAGE;
		goto cleanup;
	} else if (!scnas_check_nas_devicetype(filer_type, &prop_required)) {
		/* Check the NAS type */
		scnas_err = SCNAS_EUNKNOWN;
		goto cleanup;
	}

	/* Get the properties */
	if (prop_required && !specific_options) {
		(void) fprintf(stderr, gettext(
		    "%s:  A suboption \"%s\" is required for "
		    "attaching a NAS filer. \n"),
		    progname, SUBOPT_SPEC_OPTIONS);
		scnas_err =  SCNAS_EUSAGE;
		goto cleanup;
	}
	if (specific_options) {
		if (!prop_required) {
			(void) fprintf(stderr, gettext(
			    "%s: Invalid special option "
			    "\"%s\" for NAS type \"%s\".\n"),
			    progname, specific_options, filer_type);
			scnas_err =  SCNAS_EUSAGE;
			goto cleanup;
		}

		scconf_err = scconf_propstr_to_proplist(
		    specific_options, (scconf_cfg_prop_t **)&proplist);
		if (scconf_err != SCCONF_NOERR) {
			scnas_err = scnas_conv_scconf_err(scconf_err);
			goto cleanup;
		}

		/* Check for required args */
		found_key = NAS_NOTFOUND_KEY;
		p_chk = scnas_get_prop_val(proplist,
		    NAS_USERID, &found_key);
		if (p_chk == NULL ||
		    found_key == NAS_NOTFOUND_KEY) {
			(void) fprintf(stderr, gettext(
			    "%s:  Invalid special option "
			    "\"%s\"\n"),
			    progname, specific_options);
			scnas_err =  SCNAS_EUSAGE;
			goto cleanup;
		}

		/* Password in the options? */
		found_key = NAS_NOTFOUND_KEY;
		no_passwd = B_FALSE;
		passwd = scnas_get_prop_val(proplist, NAS_PASSWD, &found_key);
		if (passwd && (found_key == NAS_FOUND_KEY)) {
			(void) fprintf(stderr, gettext(
			    "%s:  \"%s\" option cannot be specified in "
			    "command line. \n"), progname, NAS_PASSWD);
			scnas_err =  SCNAS_EUSAGE;
			goto cleanup;
		}

		if (found_key == NAS_FOUND_KEY) {
			no_passwd = B_TRUE;
		}
	}

	/* Process file */
	if (!no_passwd && prop_required) {
		/* Prompt for the passwd */
		if (!input_file && !uflag) {
			(void) sprintf(input_buf, "%s",
			    dgettext(TEXT_DOMAIN,
			    "Please enter password:  "));
			passwd = getpassphrase(input_buf);
			if (passwd && (strcmp(passwd, "") != 0)) {

				/* Use rot13 to obfuscate the password */
				scnas_rot13_alg(&passwd);

				scnas_err = scnas_add_one_prop(NAS_PASSWD,
				    passwd, &proplist);

				if (scnas_err != SCNAS_NOERR) {
					goto cleanup;
				}
			}
		} else if (input_file) {

			/* Process the input passwd file. */
			passwd = NULL;
			scnas_err = scnas_read_passwd_file(input_file, &passwd);
			if (scnas_err != SCNAS_NOERR) {
				goto cleanup;
			}

			/* Add password into property list */
			if (passwd != NULL && *passwd != '\0') {
				/* Use rot13 to obfuscate the password */
				scnas_rot13_alg(&passwd);

				scnas_err = scnas_add_one_prop(NAS_PASSWD,
				    passwd, &proplist);
				if (scnas_err != SCNAS_NOERR) {
					goto cleanup;
				}
			}
		}

		/* Check the input file */
		if (input_file && uflag) {
			if (!passwd) {
				(void) fprintf(stdout, gettext(
				    "%s:  No password found in input_file "
				    "\"%s\". \n"),
				    progname, input_file);
			} else {
				(void) fprintf(stdout, gettext(
				    "%s:  Found one password in input_file "
				    "\"%s\" to use.\n"),
				    progname, input_file);
			}
		}
	}

	/* Only check for usage? */
	if (uflag) {
		goto cleanup;
	}

	/* Generate event */
	(void) cl_cmd_event_gen_start_event();
	(void) atexit(cl_cmd_event_gen_end_event);

	/* Process filer attachment */
	scnas_err = scnas_attach_filer(filer_name, filer_type,
	    proplist, messages);

	if (scnas_err != SCNAS_NOERR) {
		goto cleanup;
	}

cleanup:
	/* Print any additional error messages */
	if (scnas_err != SCNAS_NOERR) {
		scnas_strerr(errbuff, scnas_err);
		if (uflag) {
			(void) fprintf(stderr, gettext(
			    "%s:  Attaching filer will fail - %s.\n"),
			    progname, errbuff);
		} else {
			(void) fprintf(stderr, gettext(
			    "%s:  Failed to attach the filer - %s.\n"),
			    progname, errbuff);
		}
	}

	/* Free memory */
	if (proplist != (scnas_filer_prop_t *)0) {
		scnas_free_proplist(proplist);
	}

	return (scnas_err);
}


/*
 * scnascmd_remove
 *
 *	The "remove" form of scnas command.
 *
 *	If "uflag" is set, just check for usage errors.
 */
static scnas_errno_t
scnascmd_remove(int argc, char **argv, char **messages, uint_t uflag)
{
	scnas_errno_t scnas_err = SCNAS_NOERR;
	char errbuff[BUFSIZ];
	char *filer_name = NULL;
	int c;

	/* Process the options */
	optind = 1;
	while ((c = getopt(argc, argv, "acrph:t:o:f:")) != EOF) {
		switch (c) {
		case 'r':
			break;

		case 'a':
		case 'c':
		case 'p':
			scnas_err = SCNAS_EUSAGE;
			goto cleanup;

		case 'h':
			/* Filer name */
			filer_name = optarg;
			break;

		default:
			scnas_err = SCNAS_EUSAGE;
			goto cleanup;
		}
	}

	/* Check required parameters */
	if (!filer_name) {
		(void) fprintf(stderr, gettext(
		    "%s:  A suboption \"%s\" is required for detaching "
		    "a NAS filer. \n"), progname, SUBOPT_FILERNAME);
		scnas_err =  SCNAS_EUSAGE;
		goto cleanup;
	}

	/* Just check usage? */
	if (uflag) {
		goto cleanup;
	}

	/* Generate event */
	(void) cl_cmd_event_gen_start_event();
	(void) atexit(cl_cmd_event_gen_end_event);

	/* Remove the filer or its properties */
	scnas_err = scnas_remove_filer(filer_name,
	    NULL, messages, 0);

cleanup:
	/* Print any additional error messages */
	if (scnas_err != SCNAS_NOERR) {
		scnas_strerr(errbuff, scnas_err);
		(void) fprintf(stderr, gettext(
		    "%s:  Failed to remove the filer - %s.\n"),
		    progname, errbuff);
	}

	return (scnas_err);
}


/*
 * scnascmd_change
 *
 *	The "change" form of scnas command.
 *
 *	If "uflag" is set, just check for usage errors.
 */
static scnas_errno_t
scnascmd_change(int argc, char **argv, char **messages, uint_t uflag)
{
	scnas_errno_t scnas_err = SCNAS_NOERR;
	char errbuff[BUFSIZ];
	char *filer_name = NULL;
	char *input_file = NULL;
	char *specific_options  = NULL;
	scnas_filer_prop_t *proplist = (scnas_filer_prop_t *)0;
	scconf_errno_t scconf_err = SCCONF_NOERR;
	char input_buf[SCNAS_MAXSTRINGLEN];
	char *passwd;
	char *p_chk = NULL;
	int c;
	uint_t found_key;
	boolean_t no_passwd = B_FALSE;
	boolean_t prop_required = B_FALSE;

	/* Process the options */
	optind = 1;
	while ((c = getopt(argc, argv, "acrpnh:t:o:f:")) != EOF) {
		switch (c) {
		case 'c':
			break;

		case 'a':
		case 'r':
		case 'p':
			scnas_err = SCNAS_EUSAGE;
			goto cleanup;

		case 'n':
			/* Check usage */
			break;

		case 'h':
			/* Filer name */
			filer_name = optarg;
			break;

		case 'f':
			/* Input file */
			input_file = optarg;
			break;

		case 'o':
			/* Specific options */
			specific_options = optarg;
			break;

		default:
			scnas_err = SCNAS_EUSAGE;
			goto cleanup;
		}
	}

	/* Check required parameters */
	if (!filer_name) {
		(void) fprintf(stderr, gettext(
		    "%s:  A suboption \"%s\" is required for "
		    "updating a NAS filer.\n"),
		    progname, SUBOPT_FILERNAME);
		scnas_err = SCNAS_EUSAGE;
		goto cleanup;
	} else if (!scnas_check_nas_proprequired(filer_name, &prop_required)) {
		scnas_err = SCNAS_ENOEXIST;
		goto cleanup;
	}

	/* No -c if no properties to change */
	if (!prop_required) {
		(void) fprintf(stderr, gettext(
		    "%s:  You cannot change any properties "
		    "for NAS device \"%s\".\n"),
		    progname, filer_name);
		scnas_err = SCNAS_EUSAGE;
		goto cleanup;
	}

	/* Get the properties */
	if (prop_required && !specific_options) {
		(void) fprintf(stderr, gettext(
		    "%s:  A suboption \"%s\" is required for "
		    "updating a NAS filer.\n"),
		    progname, SUBOPT_SPEC_OPTIONS);
		scnas_err = SCNAS_EUSAGE;
		goto cleanup;
	}
	if (specific_options) {
		scconf_err = scconf_propstr_to_proplist(
		    specific_options, (scconf_cfg_prop_t **)&proplist);
		if (scconf_err != SCCONF_NOERR) {
			scnas_err = scnas_conv_scconf_err(scconf_err);
			goto cleanup;
		}

		/* Check for userid */
		found_key = NAS_NOTFOUND_KEY;
		p_chk = scnas_get_prop_val(proplist,
		    NAS_USERID, &found_key);
		if (p_chk == NULL ||
		    found_key == NAS_NOTFOUND_KEY) {
			(void) fprintf(stderr, gettext(
			    "%s:  Invalid special suboption "
			    "\"%s\"\n"),
			    progname, specific_options);
			scnas_err =  SCNAS_EUSAGE;
			goto cleanup;
		}

		/* Password in the options? */
		found_key = NAS_NOTFOUND_KEY;
		no_passwd = B_FALSE;
		passwd = scnas_get_prop_val(proplist, NAS_PASSWD, &found_key);
		if (passwd && (found_key == NAS_FOUND_KEY)) {
			(void) fprintf(stderr, gettext(
			    "%s:  \"%s\" option cannot be specified in "
			    "command line. \n"), progname, NAS_PASSWD);
			scnas_err =  SCNAS_EUSAGE;
			goto cleanup;
		}

		if (found_key == NAS_FOUND_KEY) {
			no_passwd = B_TRUE;
		}
	}

	/* Process the input */
	if (!no_passwd) {
		/* Prompt for passwd */
		if (!input_file && !uflag) {
			(void) sprintf(input_buf, "%s",
			    dgettext(TEXT_DOMAIN,
			    "Please enter password:  "));
			passwd = getpassphrase(input_buf);
			if (passwd && (strcmp(passwd, "") != 0)) {

				/* Use rot13 to obfuscate the password */
				scnas_rot13_alg(&passwd);

				scnas_err = scnas_add_one_prop(NAS_PASSWD,
				    passwd, &proplist);

				if (scnas_err != SCNAS_NOERR) {
					goto cleanup;
				}
			}
		} else if (input_file) {
			/* Process the input passwd file. */
			passwd = NULL;
			scnas_err = scnas_read_passwd_file(input_file, &passwd);
			if (scnas_err != SCNAS_NOERR) {
				goto cleanup;
			}

			/* Add password into property list */
			if (passwd) {

				/* Use rot13 to obfuscate the password */
				scnas_rot13_alg(&passwd);

				scnas_err = scnas_add_one_prop(NAS_PASSWD,
				    passwd, &proplist);
				if (scnas_err != SCNAS_NOERR) {
					goto cleanup;
				}
			}
		}

		/* Check the input file */
		if (input_file && uflag) {
			if (!passwd) {
				(void) fprintf(stdout, gettext(
				    "%s:  No password found in input_file "
				    "\"%s\"\n"),
				    progname, input_file);
			} else {
				(void) fprintf(stdout, gettext(
				    "%s:  Found one password in input_file "
				    "\"%s\" to use.\n"),
				    progname, input_file);
			}
		}
	}

	/* Only check for usage? */
	if (uflag) {
		goto cleanup;
	}

	/* Generate event */
	(void) cl_cmd_event_gen_start_event();
	(void) atexit(cl_cmd_event_gen_end_event);

	/* Update the filer */
	scnas_err = scnas_change_filer(filer_name,
	    proplist, messages);

	if (scnas_err != SCNAS_NOERR) {
		goto cleanup;
	}

cleanup:
	/* Print any additional error messages */
	if (scnas_err != SCNAS_NOERR) {
		scnas_strerr(errbuff, scnas_err);
		if (uflag) {
			(void) fprintf(stderr, gettext(
			    "%s:  Updating the filer will fail - %s.\n"),
			    progname, errbuff);
		} else {
			(void) fprintf(stderr, gettext(
			    "%s:  Failed to update the filer - %s.\n"),
			    progname, errbuff);
		}
	}

	/* Free memory */
	if (proplist != (scnas_filer_prop_t *)0) {
		scnas_free_proplist(proplist);
	}

	return (scnas_err);
}

/*
 * scnascmd_print
 *
 *	The "print" format of scnas command.
 */
static scnas_errno_t
scnascmd_print(int argc, char **argv, char **messages, uint_t uflag)
{
	scnas_errno_t scnas_err = SCNAS_NOERR;
	char *filer_name = NULL;
	char *filer_type = NULL;
	char errbuff[BUFSIZ];
	int c;

	/*
	 * We need to have **messages as an arg to be in conosistent
	 * format as other cmd functions. This check is only to
	 * remove the lint info since messages is not referenced,
	 * but we need to have it here.
	 */
	if (messages != (char **)0) {
		scnas_err = SCNAS_NOERR;
	}

	/* Process the options */
	optind = 1;
	while ((c = getopt(argc, argv, "acrpnh:t:o:f:")) != EOF) {
		switch (c) {
		case 'p':
			break;

		case 'a':
		case 'c':
		case 'r':
			scnas_err = SCNAS_EUSAGE;
			break;

		case 'n':
			break;

		case 'h':
			/* Filer name */
			filer_name = optarg;
			break;

		case 't':
			/* Filer type */
			filer_type = optarg;
			break;

		default:
			scnas_err = SCNAS_EUSAGE;
			break;
		}

		if (scnas_err != SCNAS_NOERR) {
			return (scnas_err);
		}
	}

	/* just check usage? */
	if (uflag)
		return (scnas_err);

	/* Print the filer properties */
	if (filer_name) {
		scnas_err = scnas_print_one_filer(filer_name, SCNAS_CMD);
	} else if (filer_type) {
		scnas_err = scnas_print_filer_by_type(filer_type, SCNAS_CMD);
	} else {
		scnas_err = scnas_print_filer_all(SCNAS_CMD);
	}

	/* Print any additional error messages */
	if (scnas_err != SCNAS_NOERR) {
		scnas_strerr(errbuff, scnas_err);
		(void) fprintf(stderr, gettext(
		    "%s:  Failed to print the filer configuration  - %s.\n"),
		    progname, errbuff);
	}

	return (scnas_err);
}
