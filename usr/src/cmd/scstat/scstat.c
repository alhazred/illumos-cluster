/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */
#pragma ident	"@(#)scstat.c	1.53	08/12/11 SMI"

/*
 * scstat(1M)
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <locale.h>
#include <libintl.h>
#include <sys/types.h>
#include <unistd.h>

#include <scadmin/scstat.h>
#include <scadmin/scconf.h>
#include <sys/cl_auth.h>
#include <libdcs.h>
#include <rgm/sczones.h>

/*
 * _KERNEL_ORB is defined when compiled in UNODE environment
 */
#ifdef _KERNEL_ORB
#include <orbtest/trans_tests/unode_trtests.h>
#define	exit(x)	return (x)
#endif

#define	COLW8			8
#define	COLW10			10
#define	COLW15			15
#define	COLW20			20
#define	COLW23			23
#define	COLW25			25
#define	COLW30			30

#define	LINE	\
	"------------------------------------------------------------------"

static char *progname;
static boolean_t iseuropa;

/*lint -e793 */
/* local copy of cluster configuration - added to fix 4254961 */
static scconf_cfg_cluster_t *pcluster_cfg = NULL;

static void usage(void);
static void print_column(int, char *);
static void print_column_value(int, char *);
static char *dashes(char *, char *);

static scstat_errno_t print_clusternodes(int);
static scstat_errno_t print_servernodes(char *, int);
static scstat_errno_t print_farmnodes(char *, int);
static scstat_errno_t print_farmnodes_summary(void);
static scstat_errno_t print_node(char *, scstat_node_t *, int);
static scstat_errno_t print_farmnode(char *, scstat_farm_node_t *, int);

static scstat_errno_t print_transport(char *, int);

static scstat_errno_t print_paths(char *, scstat_path_t *);
static scstat_errno_t print_path(char *, scstat_path_t *);
static int is_node_in_endpoint(char *, char *);
static scstat_errno_t get_path_endpoints(char *, char *, char *);

static scstat_errno_t print_dgs(char *, int);
static int hide_dg(char *, scconf_cfg_ds_t *);
static scstat_errno_t print_dgs_servers(char *, int, scstat_ds_t *,
    scconf_cfg_ds_t *);
static scstat_errno_t print_dgs_spares(char *, int, scstat_ds_t *,
    scconf_cfg_ds_t *);
static scstat_errno_t print_dgs_inactives(char *, int, scstat_ds_t *,
    scconf_cfg_ds_t *);
static scstat_errno_t print_dgs_local(char *, int, scconf_cfg_ds_t *);
static scstat_errno_t print_dgs_intransit(char *, int, scstat_ds_t *,
    scconf_cfg_ds_t *);
static scstat_errno_t print_dg_ownerslist(int,  scstat_ds_t *, int);
static scstat_errno_t print_dg_onlinelist(int, scconf_cfg_ds_t *);
static int is_node_in_dg(char *node, scstat_ds_t *dgstat, int level);
static int is_node_in_nodelist(char *, scconf_cfg_ds_t *);
static scstat_errno_t print_dgs_status(char *, int, scstat_ds_t *,
    scconf_cfg_ds_t *);

static scstat_errno_t print_quorum(char *, int);
static void print_quorum_summary(scstat_quorum_t *qstat);
static void print_quorum_nodevotes(char *node, scstat_quorum_t *qstat);
static void print_quorum_devvotes(char *node, scstat_quorum_t *qstat);
static int is_node_enabledfor_qdev(char *, scstat_quorumdev_t *);

static scstat_errno_t print_rgs(char *, int);
static void print_rg_summary(char *, int, scstat_rg_t *);
static void print_rg_detail(char *, int, scstat_rg_t *);
static void print_rs_detail(char *, int, scstat_rg_t *);
static int is_node_in_rg(char *node, scstat_rg_t *lrgs);

static scstat_errno_t print_ipmp(char *, int);
static void print_ipmp_grps(scstat_ipmp_stat_list_t *node_grps);

static void print_error(scstat_errno_t);

/*
 * Parse and validate options.
 * Execute the command.
 * Print error if any.
 *
 * Possible return values:
 *	SCSTAT_ENOMEM           - not enough memory
 *	SCSTAT_ENOTCLUSTER	- not a cluster node
 *	SCSTAT_ESERVICENAME	- invalid device group name
 *	SCSTAT_EINVAL		- scconf: invalid argumen
 *	SCSTAT_EPERM		- not roo
 *	SCSTAT_ERECONFIG	- cluster is reconfiguring
 *	SCSTAT_EOBSOLETE	- Resource/RG has been updated
 *      SCSTAT_EUNEXPECTED      - internal or unexpected error
 */
int
#ifndef _KERNEL_ORB
main(int argc, char **argv)
#else
scstat(int argc, char **argv)
#endif
{
	scstat_errno_t error = SCSTAT_ENOERR;
	scconf_errno_t scconf_error = SCCONF_NOERR;
	int c;
	uint_t ismember = 0;
	int Cflg, Dflg, Fflg, Wflg, gflg, nflg, qflg, iflg, printall, verbose;
	int i, lines;
	char *node = NULL;
	char options[SCSTAT_MAX_STRING_LEN];
	uint_t isfarmnode = 0;
	boolean_t zc_flag = B_FALSE;

	/* Set the program name */
	progname = strrchr(argv[0], '/');
	if (progname == NULL) {
		progname = argv[0];
	} else {
		++progname;
	}

	/* Init flags */
	Dflg = Wflg = gflg = nflg = qflg = iflg = printall = verbose = 0;
	Cflg = Fflg = 0;

	/* I18N housekeeping */
	(void) setlocale(LC_ALL, "");
	(void) textdomain(TEXT_DOMAIN);
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	/* Initialize and promote to euid=0 */
	cl_auth_init();
	cl_auth_promote();

#if SOL_VERSION >= __s10
	/* Check for zone cluster here */
	scconf_error = scconf_zone_cluster_check(&zc_flag);
	error = scstat_convert_scconf_error_code(scconf_error);
	if (error != SCSTAT_ENOERR) {
		goto cleanup;
	}

	if (zc_flag) {
		error = SCSTAT_ECZONE;
		goto cleanup;
	}
#endif

	/* make sure that we are active in the cluster */
	scconf_error = scconf_ismember(0, &ismember);
	error = scstat_convert_scconf_error_code(scconf_error);
	if (error != SCSTAT_ENOERR) {
		goto cleanup;
	}

	if (!ismember) {
		error = SCSTAT_ENOTCLUSTER;
		goto cleanup;
	}

	/* Check if this an Europa cluster */
	scconf_error = scconf_iseuropa(&iseuropa);
	error = scstat_convert_scconf_error_code(scconf_error);
	if (error != SCSTAT_ENOERR) {
		goto cleanup;
	}

	/* set options for cluster/farm type */
	if (iseuropa) {
		strcpy(options, "CDFWgnpiqh:v");
	} else {
		strcpy(options, "DWgnpiqh:v");
	}

	/* process options */
	while ((c = getopt(argc, argv, options)) != EOF) {
		switch (c) {
		case 'D': /* HA device */
			++Dflg;
			cl_auth_check_command_opt_exit(*argv, "-D",
			    CL_AUTH_DEVICE_READ, SCSTAT_EPERM);
			break;

		case 'C': /* Cluster nodes */
			if (!iseuropa) {
				usage();
				error = SCSTAT_EUSAGE;
				goto cleanup;
			}
			++Cflg;
			break;

		case 'F': /* Farm nodes */
			if (!iseuropa) {
				usage();
				error = SCSTAT_EUSAGE;
				goto cleanup;
			}
			++Fflg;
			break;

		case 'W': /* transport */
			++Wflg;
			cl_auth_check_command_opt_exit(*argv, "-W",
			    CL_AUTH_TRANSPORT_READ, SCSTAT_EPERM);
			break;

		case 'g': /* RG */
			++gflg;
			cl_auth_check_command_opt_exit(*argv, "-g",
			    CL_AUTH_RESOURCE_READ, SCSTAT_EPERM);
			break;

		case 'n': /* node */
			++nflg;
			cl_auth_check_command_opt_exit(*argv, "-n",
			    CL_AUTH_NODE_READ, SCSTAT_EPERM);
			break;

		case 'q': /* quorum */
			++qflg;
			cl_auth_check_command_opt_exit(*argv, "-q",
			    CL_AUTH_QUORUM_READ, SCSTAT_EPERM);
			break;

		case 'i': /* ipmp */
			++iflg;
			break;

		case 'p': /* all */
		    ++printall;
			break;

		case 'h': /* specific node */
			node = optarg;
			cl_auth_check_command_opt_exit(*argv, "-h",
			    CL_AUTH_NODE_READ, SCSTAT_EPERM);
			break;

		case 'v': /* verbose */
			++verbose;
			break;

		case '?':
		default:
			usage();
			error = SCSTAT_EUSAGE;
			goto cleanup;
		}
	}

	/* Make sure there are no additional args */
	if (optind != argc) {
		usage();
		error = SCSTAT_EUSAGE;
		goto cleanup;
	}

	/* Disallow -C and -F options together. */
	if (Cflg && Fflg) {
		usage();
		error = SCSTAT_EUSAGE;
		goto cleanup;
	}

	/* If nflg is not set, do not allow -C, -F options */
	if (!nflg) {
		if (Cflg || Fflg) {
			usage();
			error = SCSTAT_EUSAGE;
			goto cleanup;
		}
	}

	/* If no opts (other than -C, -F, -h or -v), default is to print all */
	i = Dflg + Wflg + gflg + nflg + qflg + iflg;
	if (i == 0) {
		++printall;
	}

	if (printall > 0) {
			cl_auth_check_command_opt_exit(*argv, NULL,
			    CL_AUTH_DEVICE_READ, SCSTAT_EPERM);
			cl_auth_check_command_opt_exit(*argv, NULL,
			    CL_AUTH_TRANSPORT_READ, SCSTAT_EPERM);
			cl_auth_check_command_opt_exit(*argv, NULL,
			    CL_AUTH_NODE_READ, SCSTAT_EPERM);
			cl_auth_check_command_opt_exit(*argv, NULL,
			    CL_AUTH_QUORUM_READ, SCSTAT_EPERM);
			cl_auth_check_command_opt_exit(*argv, NULL,
			    CL_AUTH_RESOURCE_READ, SCSTAT_EPERM);
			cl_auth_check_command_opt_exit(*argv, NULL,
			    CL_AUTH_SYSTEM_READ, SCSTAT_EPERM);
	}


	/* Promote to euid=0 */
	cl_auth_promote();

	/* If Europa, dlopen libscxcfg, libfmm */
	if (iseuropa) {
		scconf_error = scconf_libfmm_open();
		if (scconf_error != SCCONF_NOERR) {
			fprintf(stderr, "Failed to open LIBFMM\n");
			error = SCSTAT_EUNEXPECTED;
			goto cleanup;
		}

		scconf_error = scconf_libscxcfg_open();
		if (scconf_error != SCCONF_NOERR) {
			fprintf(stderr, "Failed to open LIBSCXCFG\n");
			error = SCSTAT_EUNEXPECTED;
			goto cleanup;
		}
	}

	scconf_error = scconf_get_clusterconfig(&pcluster_cfg);
	error = scstat_convert_scconf_error_code(scconf_error);
	if (error != SCSTAT_ENOERR) {
		goto cleanup;
	}

	/* If more than one option, use lines to seperate blocks of status */
	lines = (i > 1 || printall) ? 1 : 0;

	if (lines)
		(void) puts(LINE);

	/* Nodes */
	if (nflg || printall) {
		if (iseuropa) {
			/* Determine if the node is server or farm type */
			if (node && !Cflg) {
				scconf_error = scconf_isfarmnode(node,
				    &isfarmnode);
				/*
				 * Skip error message for SCCONF_ENOEXIST,
				 * since the node may be a cluster node.
				 */
				if ((scconf_error != SCCONF_NOERR) &&
				    (scconf_error != SCCONF_ENOEXIST)) {
					error =
					    scstat_convert_scconf_error_code(
						scconf_error);
					if (error != SCSTAT_ENOERR) {
						goto cleanup;
					}
				}
			}

			if (node == NULL && !Cflg && !Fflg) {
				error = print_clusternodes(verbose);
			} else if (Fflg || isfarmnode) {
				error = print_farmnodes(node, verbose);
			} else {
				error = print_servernodes(node, verbose);
			}
		} else {
			error = print_servernodes(node, verbose);
		}
		if (error != SCSTAT_ENOERR)
			goto cleanup;

		if (lines)
			(void) puts(LINE);
	}

	/* Transport paths */
	if (Wflg || printall) {
		if ((error = print_transport(node, verbose)) != SCSTAT_ENOERR)
			goto cleanup;
		if (lines)
			(void) puts(LINE);
	}

	/* Quorum */
	if (qflg || printall) {
		if ((error = print_quorum(node, verbose)) != SCSTAT_ENOERR)
			goto cleanup;
		if (lines)
			(void) puts(LINE);
	}

	/* Device groups */
	if (Dflg || printall) {
		if ((error = print_dgs(node, verbose)) != SCSTAT_ENOERR)
			goto cleanup;
		if (lines)
			(void) puts(LINE);
	}

	/* Resource groups and resources */
	if (gflg || printall) {
		if ((error = print_rgs(node, verbose)) != SCSTAT_ENOERR)
			goto cleanup;
		if (lines)
			(void) puts(LINE);
	}

	/* IPMP Status */
	if (iflg || printall) {
		if (sc_nv_zonescheck()) {
			if (iflg) {
				error = SCSTAT_ENGZONE;
			}
			goto cleanup;
		}

		if ((error = print_ipmp(node, verbose)) != SCSTAT_ENOERR)
			goto cleanup;
		if (lines)
			(void) puts(LINE);
	}

cleanup:
	/* If Europa, dlclose libfmm, libscxcfg */
	if (iseuropa) {
		(void) scconf_libfmm_close();
		(void) scconf_libscxcfg_close();
	}

	if (pcluster_cfg != NULL)
		scconf_free_clusterconfig(pcluster_cfg);
	if (error != SCSTAT_ENOERR && error != SCSTAT_EUSAGE)
		print_error(error);
	return (error);
}

/*
 * usage
 *
 *      Print a simple usage message to stderr.
 */
static void
usage(void)
{
	if (iseuropa) {
		(void) fprintf(stderr,
		    "usage:  %s [-DWgpqi] [-v[v]] [-n[-C | -F]] [-h <host>]\n",
		    progname);
	} else {
		(void) fprintf(stderr,
		    "usage:  %s [-DWgnpqi] [-v[v]] [-h <host>]\n", progname);
	}

	(void) putc('\n', stderr);
}


/*
 * print_column
 *
 *	Print a column in a table using the given width.   If the
 *	"string" is NULL, print an empty column.
 */
static void
print_column(int width, char *string)
{
	int i;

	/* If string is NULL, just print spaces */
	if (string == NULL || *string == '\0') {
		for (i = 0;  i < width;  ++i)
			(void) putchar(' ');

	/* If width is zero, just print the string */
	} else if (width < 1) {
		(void) fputs(string, stdout);

	/* Print left justified column of given width */
	} else {
		(void) printf("%-*s ", width - 1, string);
	}
}

/*
 * print_column_value
 *
 *	Print a column in a table using the given width.   If the
 *	"value" is NULL, print a "-" for the value.
 */
static void
print_column_value(int width, char *value)
{
	if (value == NULL || *value == '\0')
		value = "-";

	print_column(width, value);
}

/*
 * dashes
 *
 *	Fill the buffer with a bunch of dashes the same length as the string
 */
static char *
dashes(char *buffer, char *string)
{
	size_t i, len;
	char *ptr;

	if (string == NULL || *string == '\0')
		return (buffer);

	ptr = buffer;
	len = (size_t)strlen(string);
	for (i = 0;  i < len;  ++i)
		*ptr++ = '-';

	*ptr = '\0';

	return (buffer);
}

/*
 * print_servernodes
 *
 *	Print status of server nodes in the cluster.
 */
/* ARGSUSED */
static scstat_errno_t
print_servernodes(char *node, int verbose)
{
	scstat_errno_t	error = SCSTAT_ENOERR;
	scstat_node_t *lnds = NULL;
	scstat_node_t *nd = NULL;
	char buffer[SCSTAT_MAX_STRING_LEN];

	/* get status for server nodes in the cluster */
	error = scstat_get_nodes_with_cached_config(pcluster_cfg, &lnds);
	if (error != SCSTAT_ENOERR)
		goto cleanup;
	if (lnds == NULL)
		goto cleanup;

	/* print header */
	(void) putchar('\n');
	if (iseuropa) {
		(void) puts(gettext("-- Server Nodes --"));
	} else {
		(void) puts(gettext("-- Cluster Nodes --"));
	}
	(void) putchar('\n');

	print_column(COLW20, NULL);
	print_column(COLW20, gettext("Node name"));
	print_column(0, gettext("Status"));
	(void) putchar('\n');

	print_column(COLW20, NULL);
	print_column(COLW20, dashes(buffer, gettext("Node name")));
	print_column(0, dashes(buffer, gettext("Status")));
	(void) putchar('\n');

	/* print status for each node */
	for (nd = lnds;  nd;  nd = nd->scstat_node_next) {
		/* skip this one? */
		if (node && nd->scstat_node_name &&
		    strcmp(node, nd->scstat_node_name) != 0)
			continue;

		/* print per-node status */
		error = print_node(node, nd, verbose);
		if (error != SCSTAT_ENOERR)
			goto cleanup;
	}

	(void) putchar('\n');

cleanup:
	/* free space */
	if (lnds)
		scstat_free_nodes(lnds);

	return (error);
}

/*
 * print_farmnodes_summary
 *
 *	Print summary of farm nodes in the cluster.
 */
static scstat_errno_t
print_farmnodes_summary(void)
{
	scstat_errno_t	error = SCSTAT_ENOERR;
	uint_t offline_count;
	uint_t online_count;
	scstat_farm_node_t *lnds = NULL;
	scstat_farm_node_t *nd = NULL;
	char buffer[SCSTAT_MAX_STRING_LEN];

	/* get status for farm nodes in the cluster */
	error = scstat_get_farmnodes(&lnds, &offline_count, &online_count);
	if (error != SCSTAT_ENOERR || lnds == NULL) {
		goto cleanup;
	}

	/* print farm nodes header */
	(void) putchar('\n');
	(void) puts(gettext("-- Farm Nodes Summary --"));
	(void) putchar('\n');

	/* print offline label */
	print_column(COLW30, gettext("  Total farm nodes offline:"));
	(void) sprintf(buffer, "%d", offline_count);
	print_column(0, buffer);
	(void) putchar('\n');

	/* print online label */
	print_column(COLW30, gettext("  Total farm nodes online:"));
	(void) sprintf(buffer, "%d", online_count);
	print_column(0, buffer);
	(void) putchar('\n');
	(void) putchar('\n');

cleanup:
	/* free space */
	if (lnds)
		scstat_free_farmnodes(lnds);

	return (error);
}

/*
 * print_farmnodes
 *
 *	Print status of farm nodes in the cluster.
 */
/* ARGSUSED */
static scstat_errno_t
print_farmnodes(char *node, int verbose)
{
	scstat_errno_t	error = SCSTAT_ENOERR;
	uint_t offline_count;
	uint_t online_count;
	scstat_farm_node_t *lnds = NULL;
	scstat_farm_node_t *nd = NULL;
	char buffer[SCSTAT_MAX_STRING_LEN];

	/* get status for farm nodes in the cluster */
	error = scstat_get_farmnodes(&lnds, &offline_count, &online_count);
	if (error != SCSTAT_ENOERR || lnds == NULL)
		goto cleanup;

	/* print header */
	(void) putchar('\n');
	(void) puts(gettext("-- Farm Nodes --"));
	(void) putchar('\n');

	print_column(COLW20, NULL);
	print_column(COLW20, gettext("Node name"));
	print_column(0, gettext("Status"));
	(void) putchar('\n');

	print_column(COLW20, NULL);
	print_column(COLW20, dashes(buffer, gettext("Node name")));
	print_column(0, dashes(buffer, gettext("Status")));
	(void) putchar('\n');

	/* print status for each node */
	for (nd = lnds;  nd;  nd = nd->scstat_node_next) {
		/* skip this one? */
		if (node && nd->scstat_node_name &&
		    strcmp(node, nd->scstat_node_name) != 0)
			continue;

		/* print per-node status */
		error = print_farmnode(node, nd, verbose);
		if (error != SCSTAT_ENOERR)
			goto cleanup;
	}

	(void) putchar('\n');

cleanup:
	/* free space */
	if (lnds)
		scstat_free_farmnodes(lnds);

	return (error);
}

/*
 * print_clusternodes
 *
 *	Print status of cluster (server) nodes in the cluster.
 *	Print status summary of cluster (farm) nodes in the cluster.
 */
/* ARGSUSED */
static scstat_errno_t
print_clusternodes(int verbose)
{
	scstat_errno_t	error = SCSTAT_ENOERR;

	error = print_servernodes(NULL, verbose);
	if (error != SCSTAT_ENOERR)
		return (error);

	error = print_farmnodes_summary();

	return (error);
}

/*
 * print_node
 *
 *	Print status of a node.
 */
/* ARGSUSED */
static scstat_errno_t
print_node(char *node, scstat_node_t *nd, int verbose)
{
	/* check argument */
	if (nd == NULL)
		return (SCSTAT_EUNEXPECTED);

	/* okay to print for this node? */
	if (node && strcmp(node, nd->scstat_node_name) != 0)
		return (SCSTAT_ENOERR);

	/* print label */
	if (iseuropa) {
		print_column(COLW20, gettext("  Server node:"));
	} else {
		print_column(COLW20, gettext("  Cluster node:"));
	}

	/* print node name */
	print_column_value(COLW20, nd->scstat_node_name);

	/* print status ONLINE/OFFLINE/FAULTED/DEGRADED/WAIT/UNKNOWN */
	print_column_value(0, nd->scstat_node_statstr);
	(void) putchar('\n');

	return (SCSTAT_ENOERR);
}

/*
 * print_farmnode
 *
 *	Print status of a farmnode.
 */
/* ARGSUSED */
static scstat_errno_t
print_farmnode(char *node, scstat_farm_node_t *nd, int verbose)
{
	/* check argument */
	if (nd == NULL)
		return (SCSTAT_EUNEXPECTED);

	/* okay to print for this node? */
	if (node && strcmp(node, nd->scstat_node_name) != 0)
		return (SCSTAT_ENOERR);

	/* print label */
	print_column(COLW20, gettext("  Farm node:"));

	/* print node name */
	print_column_value(COLW20, nd->scstat_node_name);

	/* print status ONLINE/OFFLINE */
	print_column_value(0, nd->scstat_node_statstr);
	(void) putchar('\n');

	return (SCSTAT_ENOERR);
}

/*
 * print_transport
 *
 *	Print status of transport objects in the cluster.
 */
/* ARGSUSED */
static scstat_errno_t
print_transport(char *node, int verbose)
{
	scstat_errno_t error = SCSTAT_ENOERR;
	scstat_transport_t *transport = NULL;

	/* get transport status */
	error = scstat_get_transport(&transport);
	if (error != SCSTAT_ENOERR)
		goto cleanup;
	if (transport == NULL)
		goto cleanup;

	/* print status for every path */
	error = print_paths(node, transport->scstat_path_list);
	if (error != SCSTAT_ENOERR)
		goto cleanup;

cleanup:
	/* free space */
	if (transport)
		scstat_free_transport(transport);

	return (error);
}

/*
 * print_paths
 *
 *	Print status of paths in the cluster.
 */
static 	scstat_errno_t
print_paths(char *node, scstat_path_t *lpaths)
{
	scstat_errno_t error = SCSTAT_ENOERR;
	scstat_path_t *path;
	char buffer[SCSTAT_MAX_STRING_LEN];

	if (lpaths == NULL)
		return (error);

	/* print header */
	(void) putchar('\n');
	(void) puts(gettext("-- Cluster Transport Paths --"));
	(void) putchar('\n');

	print_column(COLW20, NULL);
	print_column(COLW23, gettext("Endpoint"));
	print_column(COLW23, gettext("Endpoint"));
	print_column(0, gettext("Status"));
	(void) putchar('\n');

	print_column(COLW20, NULL);
	print_column(COLW23, dashes(buffer, gettext("Endpoint")));
	print_column(COLW23, dashes(buffer, gettext("Endpoint")));
	print_column(0, dashes(buffer, gettext("Status")));
	(void) putchar('\n');

	/* print status for each path */
	for (path = lpaths;  path;  path = path->scstat_path_next) {
		error = print_path(node, path);
		if (error != SCSTAT_ENOERR)
			return (error);
	}

	(void) putchar('\n');

	return (error);
}

/*
 * print_path
 *
 *	Print status of a path.
 */
static scstat_errno_t
print_path(char *node, scstat_path_t *path)
{
	scstat_errno_t error;
	char e1[SCSTAT_MAX_STRING_LEN];
	char e2[SCSTAT_MAX_STRING_LEN];

	if (path == NULL)
		return (SCSTAT_ENOERR);

	/* get transport path name */
	error = get_path_endpoints(path->scstat_path_name, e1, e2);
	if (error != SCSTAT_ENOERR)
		return (error);

	/* okay to print for this node? */
	if (node && !is_node_in_endpoint(node, e1) &&
	    !is_node_in_endpoint(node, e2))
		return (SCSTAT_ENOERR);

	/* print label */
	print_column(COLW20, gettext("  Transport path:"));

	/* print endpoint 1 */
	print_column_value(COLW23, e1);

	/* print endpoint 2 */
	print_column_value(COLW23, e2);

	/* print status ONLINE/OFFLINE/FAULTED/DEGRADED/WAIT/UNKNOWN */
	print_column_value(0, path->scstat_path_statstr);
	(void) putchar('\n');

	return (SCSTAT_ENOERR);
}

/*
 * is_node_in_endpoint
 *
 *	Return non-zero, if the node name is found in the endpoint.
 *	Otherwise, return zero.
 */
static int
is_node_in_endpoint(char *node, char *endpoint)
{
	char *ptr;
	int len;

	/* Check arguments */
	if (node == NULL || endpoint == NULL)
		return (0);

	/* Make sure the endpoint has a hostname */
	if ((ptr = strchr(endpoint, ':')) == NULL)
		return (0);
	len = ptr - endpoint;

	if (strlen(node) != (size_t)len ||
	    strncmp(node, endpoint, (size_t)len) != 0)
		return (0);

	return (1);
}

/*
 * get_path_endpoints
 *
 *	Get the two endpoints of path "iname".
 */
static scstat_errno_t
get_path_endpoints(char *iname, char *e1, char *e2)
{
	scstat_errno_t	error = SCSTAT_ENOERR;
	char *adapter_name1 = NULL;
	char *adapter_name2 = NULL;

	error = scstat_get_path_endpoints(iname, &adapter_name1,
	    &adapter_name2);
	if (error != SCSTAT_ENOERR)
		goto cleanup;

	error = scstat_get_cached_transport_adapter_name(pcluster_cfg,
	    adapter_name1, e1);
	if (error != SCSTAT_ENOERR)
		goto cleanup;

	error = scstat_get_cached_transport_adapter_name(pcluster_cfg,
	    adapter_name2, e2);
	if (error != SCSTAT_ENOERR)
		goto cleanup;

cleanup:
	if (adapter_name1 != NULL)
		free(adapter_name1);

	if (adapter_name2 != NULL)
		free(adapter_name2);

	return (error);
}

/*
 * print_dgs
 *
 *	Print status of device groups in the cluster.
 */
static scstat_errno_t
print_dgs(char *node, int verbose)
{
	scstat_errno_t error = SCSTAT_ENOERR;
	scconf_cfg_ds_t *dgconfig = NULL;
	scstat_ds_t *ldgstat = NULL;

	/* get status for device groups in the cluster */
	error = scstat_get_ds_status_with_cached_config(pcluster_cfg, NULL,
	    &ldgstat);
	if (error != SCSTAT_ENOERR)
		goto cleanup;
	if (ldgstat == NULL)
		goto cleanup;

	/* get a copy of the device group config */
	if (scconf_get_ds_config(NULL, SCCONF_BY_NAME_FLAG, &dgconfig)) {
		error = SCSTAT_EUNEXPECTED;
		goto cleanup;
	}

	/* Print the primary/secondary servers table */
	error = print_dgs_servers(node, verbose, ldgstat, dgconfig);
	if (error != SCSTAT_ENOERR)
		goto cleanup;

	/* if "verbose" mode, print the other "server" stuff */
	if (verbose >= 1) {

		/* Print the spares table */
		error = print_dgs_spares(node, verbose, ldgstat, dgconfig);
		if (error != SCSTAT_ENOERR)
			goto cleanup;

		/* Print the inactive table */
		error = print_dgs_inactives(node, verbose, ldgstat, dgconfig);
		if (error != SCSTAT_ENOERR)
			goto cleanup;

		/* Print the in transition table */
		error = print_dgs_intransit(node, verbose, ldgstat, dgconfig);
		if (error != SCSTAT_ENOERR)
			goto cleanup;
	}

	/* Unless this is a per-node listing, print the status table */
	if (node == NULL) {
		error = print_dgs_status(node, verbose, ldgstat, dgconfig);
		if (error != SCSTAT_ENOERR)
			goto cleanup;
	}

	/* Print multi-owner/local device table */
	error = print_dgs_local(node, verbose, dgconfig);
	if (error != SCSTAT_ENOERR)
		goto cleanup;

cleanup:
	/* free space */
	if (ldgstat)
		scstat_free_ds_status(ldgstat);
	if (dgconfig)
		scconf_free_ds_config(dgconfig);

	return (error);
}

/*
 * hide_dg
 *
 *	Return non-zero if the disk group should be hidden in
 *	non-vv (very verbose) mode.   A group is hidden if the
 *	SCCONF_DS_PROP_AUTOGENERATED property is set.
 *
 *	Otherwise, return zero.
 */
static int
hide_dg(char *dgname, scconf_cfg_ds_t *dgconfig)
{
	scconf_cfg_ds_t *dgp;
	scconf_cfg_prop_t *propp;

	/* Check arguemnts */
	if (dgname == NULL || dgconfig == NULL)
		return (0);

	/* Find the device group in the list */
	for (dgp = dgconfig;  dgp;  dgp = dgp->scconf_ds_next) {
		if (dgp->scconf_ds_name &&
		    strcmp(dgname, dgp->scconf_ds_name) == 0)
			break;
	}

	/* If we cannot find the group, just return zero */
	if (dgp == NULL)
		return (0);

	/* Check to see if the "autogenerated" property is set */
	for (propp = dgp->scconf_ds_propertylist;  propp;
	    propp = propp->scconf_prop_next) {
		if (propp->scconf_prop_key &&
		    strcmp(SCCONF_DS_PROP_AUTOGENERATED,
		    propp->scconf_prop_key) == 0)
			return (1);
	}

	return (0);
}

/*
 * print_dgs_servers
 *
 *	Print servers status of device groups in the cluster.
 */
static scstat_errno_t
print_dgs_servers(char *node, int verbose, scstat_ds_t *ldgstat,
    scconf_cfg_ds_t *dgconfig)
{
	scstat_errno_t error;
	scstat_ds_t *dgstat;
	char buffer[SCSTAT_MAX_STRING_LEN];

	/* print header for primaries and secondaries */
	(void) putchar('\n');
	(void) puts(gettext("-- Device Group Servers --"));
	(void) putchar('\n');

	print_column(COLW25, NULL);
	print_column(COLW20, gettext("Device Group"));
	print_column(COLW20, gettext("Primary"));
	print_column(0, gettext("Secondary"));
	(void) putchar('\n');

	print_column(COLW25, NULL);
	print_column(COLW20, dashes(buffer, gettext("Device Group")));
	print_column(COLW20, dashes(buffer, gettext("Primary")));
	print_column(0, dashes(buffer, gettext("Secondary")));
	(void) putchar('\n');

	/* print device group server status for each device group */
	for (dgstat = ldgstat;  dgstat;  dgstat = dgstat->scstat_ds_next) {

		/* make sure it has a name */
		if (dgstat->scstat_ds_name == NULL)
			return (SCSTAT_EUNEXPECTED);

		/* don't display multi-owner device groups */
		if (scconf_is_local_device_service(dgstat->scstat_ds_name)) {
			continue;
		}

		/* see if we want to supress it */
		if (verbose < 2 && hide_dg(dgstat->scstat_ds_name, dgconfig))
			continue;

		/* okay to print for this node at this level? */
		if (node && !is_node_in_dg(node, dgstat, SCSTAT_PRIMARY) &&
		    !is_node_in_dg(node, dgstat, SCSTAT_SECONDARY))
			continue;

		/* label */
		print_column(COLW25, gettext("  Device group servers:"));

		/* device group name */
		print_column_value(COLW20, dgstat->scstat_ds_name);

		/* print primary */
		error = print_dg_ownerslist(COLW20, dgstat, SCSTAT_PRIMARY);
		if (error != SCSTAT_ENOERR)
			return (error);

		/* print secondary */
		error = print_dg_ownerslist(0, dgstat, SCSTAT_SECONDARY);
		if (error != SCSTAT_ENOERR)
			return (error);

		/* end of line */
		(void) putchar('\n');
	}

	(void) putchar('\n');

	return (SCSTAT_ENOERR);
}

/*
 * print_dgs_spares
 *
 *	Print spare nodes status of device groups in the cluster.
 */
static scstat_errno_t
print_dgs_spares(char *node, int verbose, scstat_ds_t *ldgstat,
    scconf_cfg_ds_t *dgconfig)
{
	scstat_errno_t error;
	scstat_ds_t *dgstat;
	char buffer[SCSTAT_MAX_STRING_LEN];

	/* print header for spares */
	(void) putchar('\n');
	(void) puts(gettext("-- Device Group Spares --"));
	(void) putchar('\n');

	print_column(COLW30, NULL);
	print_column(COLW20, gettext("Device Group"));
	print_column(0, gettext("Spare Nodes"));
	(void) putchar('\n');

	print_column(COLW30, NULL);
	print_column(COLW20, dashes(buffer, gettext("Device Group")));
	print_column(0, dashes(buffer, gettext("Spare Nodes")));
	(void) putchar('\n');

	/* print device group spares status for each device group */
	for (dgstat = ldgstat;  dgstat;  dgstat = dgstat->scstat_ds_next) {

		/* make sure it has a name */
		if (dgstat->scstat_ds_name == NULL)
			return (SCSTAT_EUNEXPECTED);

		/* don't display multi-owner device groups */
		if (scconf_is_local_device_service(dgstat->scstat_ds_name)) {
			continue;
		}

		/* see if we want to supress it */
		if (verbose < 2 && hide_dg(dgstat->scstat_ds_name, dgconfig))
			continue;

		/* okay to print for this node at this level? */
		if (node && !is_node_in_dg(node, dgstat, SCSTAT_SPARE))
			continue;

		/* label */
		print_column(COLW30, gettext("  Device group spares:"));

		/* device group name */
		print_column_value(COLW20, dgstat->scstat_ds_name);

		/* print spares */
		error = print_dg_ownerslist(0, dgstat, SCSTAT_SPARE);
		if (error != SCSTAT_ENOERR)
			return (error);

		/* end of line */
		(void) putchar('\n');
	}

	(void) putchar('\n');

	return (SCSTAT_ENOERR);
}

/*
 * print_dgs_inactives
 *
 *	Print inactive nodes status of device groups in the cluster.
 */
static scstat_errno_t
print_dgs_inactives(char *node, int verbose, scstat_ds_t *ldgstat,
    scconf_cfg_ds_t *dgconfig)
{
	scstat_errno_t error;
	scstat_ds_t *dgstat;
	char buffer[SCSTAT_MAX_STRING_LEN];

	/* print header for inactive nodes */
	(void) putchar('\n');
	(void) puts(gettext("-- Device Group Inactives --"));
	(void) putchar('\n');

	print_column(COLW30, NULL);
	print_column(COLW20, gettext("Device Group"));
	print_column(0, gettext("Inactive Nodes"));
	(void) putchar('\n');

	print_column(COLW30, NULL);
	print_column(COLW20, dashes(buffer, gettext("Device Group")));
	print_column(0, dashes(buffer, gettext("Inactive Nodes")));
	(void) putchar('\n');

	/* print device group inactives status for each device group */
	for (dgstat = ldgstat;  dgstat;  dgstat = dgstat->scstat_ds_next) {

		/* make sure it has a name */
		if (dgstat->scstat_ds_name == NULL)
			return (SCSTAT_EUNEXPECTED);

		/* don't display multi-owner device groups */
		if (scconf_is_local_device_service(dgstat->scstat_ds_name)) {
			continue;
		}

		/* see if we want to supress it */
		if (verbose < 2 && hide_dg(dgstat->scstat_ds_name, dgconfig))
			continue;

		/* okay to print for this node at this level? */
		if (node && !is_node_in_dg(node, dgstat, SCSTAT_INACTIVE))
			continue;

		/* label */
		print_column(COLW30, gettext("  Device group inactives:"));

		/* device group name */
		print_column_value(COLW20, dgstat->scstat_ds_name);

		/* print inactive nodes */
		error = print_dg_ownerslist(0, dgstat, SCSTAT_INACTIVE);
		if (error != SCSTAT_ENOERR)
			return (error);

		/* end of line */
		(void) putchar('\n');
	}

	(void) putchar('\n');

	return (SCSTAT_ENOERR);
}

/*
 * print_dgs_local
 *
 *	Print multi-owner/local status of device groups in the cluster.
 */
static scstat_errno_t
print_dgs_local(char *node, int verbose, scconf_cfg_ds_t *dgconfig)
{
	scstat_errno_t error;
	scconf_cfg_ds_t *dgp;
	char buffer[SCSTAT_MAX_STRING_LEN];

	/* print header for inactive nodes */
	(void) putchar('\n');
	(void) puts(gettext("-- Multi-owner Device Groups --"));
	(void) putchar('\n');

	print_column(COLW30, NULL);
	print_column(COLW20, gettext("Device Group"));
	print_column(0, gettext("Online Status"));
	(void) putchar('\n');

	print_column(COLW30, NULL);
	print_column(COLW20, dashes(buffer, gettext("Device Group")));
	print_column(0, dashes(buffer, gettext("Online Status")));
	(void) putchar('\n');

	/* print multi-owner device group status for each device group */
	for (dgp = dgconfig;  dgp;  dgp = dgp->scconf_ds_next) {

		/* make sure it has a name */
		if (dgp->scconf_ds_name == NULL)
			return (SCSTAT_EUNEXPECTED);

		/* display only multi-owner device groups */
		if (!scconf_is_local_device_service(dgp->scconf_ds_name)) {
			continue;
		}

		/* see if we want to supress it */
		if (verbose < 2 && hide_dg(dgp->scconf_ds_name, dgconfig))
			continue;

		/* okay to print for this node */
		if (node && !is_node_in_nodelist(node, dgp))
			continue;

		/* label */
		print_column(COLW30, gettext("  Multi-owner device group:"));

		/* device group name */
		print_column_value(COLW20, dgp->scconf_ds_name);

		/* print online nodes */
		error = print_dg_onlinelist(0, dgp);

		/* end of line */
		(void) putchar('\n');

		if (error != SCSTAT_ENOERR)
			return (error);
	}

	(void) putchar('\n');

	return (SCSTAT_ENOERR);
}
/*
 * print_dgs_intransit
 *
 *	Print in transition nodes status of device groups in the cluster.
 */
static scstat_errno_t
print_dgs_intransit(char *node, int verbose, scstat_ds_t *ldgstat,
    scconf_cfg_ds_t *dgconfig)
{
	scstat_errno_t error;
	scstat_ds_t *dgstat;
	char buffer[SCSTAT_MAX_STRING_LEN];

	/* print header for in transitition nodes */
	(void) putchar('\n');
	(void) puts(gettext("-- Device Group Transitions --"));
	(void) putchar('\n');

	print_column(COLW30, NULL);
	print_column(COLW20, gettext("Device Group"));
	print_column(0, gettext("In Transition Nodes"));
	(void) putchar('\n');

	print_column(COLW30, NULL);
	print_column(COLW20, dashes(buffer, gettext("Device Group")));
	print_column(0, dashes(buffer, gettext("In Transition Nodes")));
	(void) putchar('\n');

	/* print device group in transition status for each device group */
	for (dgstat = ldgstat;  dgstat;  dgstat = dgstat->scstat_ds_next) {

		/* make sure it has a name */
		if (dgstat->scstat_ds_name == NULL)
			return (SCSTAT_EUNEXPECTED);

		/* don't display multi-owner device groups */
		if (scconf_is_local_device_service(dgstat->scstat_ds_name)) {
			continue;
		}

		/* see if we want to supress it */
		if (verbose < 2 && hide_dg(dgstat->scstat_ds_name, dgconfig))
			continue;

		/* okay to print for this node at this level? */
		if (node && !is_node_in_dg(node, dgstat, SCSTAT_TRANSITION))
			continue;

		/* label */
		print_column(COLW30, gettext("  Device group transitions:"));

		/* device group name */
		print_column_value(COLW20, dgstat->scstat_ds_name);

		/* print in transitition nodes */
		error = print_dg_ownerslist(0, dgstat, SCSTAT_TRANSITION);
		if (error != SCSTAT_ENOERR)
			return (error);

		/* end of line */
		(void) putchar('\n');
	}

	(void) putchar('\n');

	return (SCSTAT_ENOERR);
}

/*
 * print_dg_ownerslist
 *
 *	Print all owners of device group at given level, where
 *
 *		level 0 is	primary
 *		level 1 is	secondary
 *		level 2 is	spare
 *		level 3 is	inactive
 *		level 4 is	in transition
 */
static scstat_errno_t
print_dg_ownerslist(int width, scstat_ds_t *dgstat, int level)
{
	scstat_ds_node_state_t *node_state;
	char buffer[SCSTAT_MAX_STRING_LEN];
	size_t len, used;

	if (dgstat == NULL)
		return (SCSTAT_ENOERR);

	/* Clear the buffer */
	bzero(buffer, sizeof (buffer));
	used = 0;

	/* Add matching nodes to buffer */
	for (node_state = dgstat->scstat_node_state_list;  node_state;
	    node_state = node_state->scstat_node_next) {

		/* Matcing levels? */
		if (level == node_state->scstat_node_state) {

			/* check name */
			if (node_state->scstat_node_name == NULL ||
			    *node_state->scstat_node_name == '\0')
				return (SCSTAT_EUNEXPECTED);
			if ((len =
			    (size_t)strlen(node_state->scstat_node_name)) >
			    ((size_t)sizeof (buffer) - (used + 2)))
				return (SCSTAT_ENOMEM);

			/* append name to buffer */
			if (*buffer != '\0') {
				(void) strcat(buffer, ",");
				used += 1;
			}
			(void) strcat(buffer, node_state->scstat_node_name);
			used += len;
		}
	}

	/* Print */
	print_column_value(width, buffer);

	return (SCSTAT_ENOERR);
}

/*
 * print_dg_onlinelist
 *
 *	Print all nodes that are online and are associated with a
 *	multi-owner device group.
 *
 */
static scstat_errno_t
print_dg_onlinelist(int width, scconf_cfg_ds_t *dgp)
{
	char buffer[SCSTAT_MAX_STRING_LEN];
	size_t len, used;
	scconf_errno_t rval;
	scconf_nodeid_t *nodeidp;
	char *namep;
	uint_t member = 0;
	dc_error_t dc_err;

	if (dgp == NULL)
		return (SCSTAT_ENOERR);

	/* Clear the buffer */
	bzero(buffer, sizeof (buffer));
	used = 0;

	/* Add matching nodes to buffer */
	for (nodeidp = dgp->scconf_ds_nodelist; nodeidp && *nodeidp;
							nodeidp++) {

		dc_err = dcs_get_multiowner_status(*nodeidp, &member);

		if (dc_err != DCS_SUCCESS)
			return (SCSTAT_EUNEXPECTED);

		/* Not a member */
		if (!member)
			continue;

		rval = scconf_get_nodename(*nodeidp, &namep);
		if (rval != SCCONF_NOERR)
			return (scstat_convert_scconf_error_code(rval));

		if ((len =
		    (size_t)strlen(namep)) >
		    ((size_t)sizeof (buffer) - (used + 2)))
			return (SCSTAT_ENOMEM);

		/* append name to buffer */
		if (*buffer != '\0') {
			(void) strcat(buffer, ",");
			used += 1;
		}
		(void) strcat(buffer, namep);
		used += len;
		free(namep);
		member = 0;
	}

	/* Print */
	print_column_value(width, buffer);

	return (SCSTAT_ENOERR);
}
/*
 * is_node_in_rg
 *
 *	Return non-zero, if the node is found in the group at this "level".
 *	Otherwise, return zero.
 */
static int
is_node_in_dg(char *node, scstat_ds_t *dgstat, int level)
{
	scstat_ds_node_state_t *node_state;

	/* Check arguments */
	if (node == NULL || dgstat == NULL)
		return (0);

	/* Look for node */
	for (node_state = dgstat->scstat_node_state_list;  node_state;
	    node_state = node_state->scstat_node_next) {

		/* Matching levels? */
		if (level == node_state->scstat_node_state) {

			/* check name */
			if (node_state->scstat_node_name &&
			    strcmp(node, node_state->scstat_node_name) == 0)
				return (1);
		}
	}

	return (0);
}

/*
 * is_node_in_nodelist
 *
 *	Return non-zero, if the node is found in the nodelist
 *	Otherwise, return zero.
 */
static int
is_node_in_nodelist(char *node, scconf_cfg_ds_t *dgp)
{
	scconf_nodeid_t nid;
	scconf_nodeid_t *nodeidp;

	/* Check arguments */
	if (node == NULL || dgp == NULL)
		return (0);

	/* Convert the nodename to a nodeid */
	if (scconf_get_nodeid(node, &nid) != SCCONF_NOERR)
		return (0);

	/* Look for nodeid in the nodelist */
	for (nodeidp = dgp->scconf_ds_nodelist; nodeidp && *nodeidp;
							nodeidp++) {
		if (nodeidp && nid == *nodeidp)
			return (1);
	}
	return (0);
}

/*
 * print_dgs_status
 *
 *	Print status of device groups in the cluster.
 */
/* ARGSUSED */
static scstat_errno_t
print_dgs_status(char *node, int verbose, scstat_ds_t *ldgstat,
    scconf_cfg_ds_t *dgconfig)
{
	scstat_ds_t *dgstat;
	char buffer[SCSTAT_MAX_STRING_LEN];

	/* print header for inactive nodes */
	(void) putchar('\n');
	(void) puts(gettext("-- Device Group Status --"));
	(void) putchar('\n');

	print_column(COLW30, NULL);
	print_column(COLW20, gettext("Device Group"));
	print_column(COLW20, gettext("Status"));
	(void) putchar('\n');

	print_column(COLW30, NULL);
	print_column(COLW20, dashes(buffer, gettext("Device Group")));
	print_column(COLW20, dashes(buffer, gettext("Status")));
	(void) putchar('\n');

	/* print device group status for each device group */
	for (dgstat = ldgstat;  dgstat;  dgstat = dgstat->scstat_ds_next) {

		/* make sure it has a name */
		if (dgstat->scstat_ds_name == NULL)
			return (SCSTAT_EUNEXPECTED);

		/* don't display multi-owner device groups */
		if (scconf_is_local_device_service(dgstat->scstat_ds_name)) {
			continue;
		}

		/* see if we want to supress it */
		if (verbose < 2 && hide_dg(dgstat->scstat_ds_name, dgconfig))
			continue;

		/* label */
		print_column(COLW30, gettext("  Device group status:"));

		/* device group name */
		print_column_value(COLW20, dgstat->scstat_ds_name);

		/* print status */
		print_column_value(0, dgstat->scstat_ds_statstr);

		/* end of line */
		(void) putchar('\n');
	}

	(void) putchar('\n');

	return (SCSTAT_ENOERR);
}

/*
 * print_quorum
 *
 *	Print quorum status of the cluster.
 */
/* ARGSUSED */
static scstat_errno_t
print_quorum(char *node, int verbose)
{
	scstat_errno_t	error = SCSTAT_ENOERR;
	scstat_quorum_t *qstat = NULL;

	/* get quorum status for the cluster */
	error = scstat_get_quorum_with_cached_config(pcluster_cfg, &qstat);
	if (error != SCSTAT_ENOERR)
		goto cleanup;
	if (qstat == NULL)
		goto cleanup;

	/* Unless this is a per-node listing, print the quorum summary */
	if (node == NULL)
		print_quorum_summary(qstat);

	/* Print quorum votes by node */
	print_quorum_nodevotes(node, qstat);

	/* Print quorum votes by device */
	print_quorum_devvotes(node, qstat);

	(void) putchar('\n');

cleanup:
	/* free space */
	if (qstat)
		scstat_free_quorum(qstat);

	return (error);
}

/*
 * print_quorum_summary
 *
 *	Print quorum summary data.
 */
static void
print_quorum_summary(scstat_quorum_t *qstat)
{
	char buffer[SCSTAT_MAX_STRING_LEN];

	/* section heading */
	(void) putchar('\n');
	(void) puts(
	    gettext("-- Quorum Summary from latest node reconfiguration --"));
	(void) putchar('\n');

	/* print votes possible */
	print_column(COLW30, gettext("  Quorum votes possible:"));
	(void) sprintf(buffer, "%d", qstat->scstat_vote_configured);
	print_column(0, buffer);
	(void) putchar('\n');

	/* print votes needed */
	print_column(COLW30, gettext("  Quorum votes needed:"));
	(void) sprintf(buffer, "%d", qstat->scstat_vote_needed);
	print_column(0, buffer);
	(void) putchar('\n');

	/* print votes present */
	print_column(COLW30, gettext("  Quorum votes present:"));
	(void) sprintf(buffer, "%d", qstat->scstat_vote);
	print_column(0, buffer);
	(void) putchar('\n');

	(void) putchar('\n');
}

/*
 * print_quorum_nodevotes
 *
 *	Print quorum votes by node.
 */
static void
print_quorum_nodevotes(char *node, scstat_quorum_t *qstat)
{
	char buffer[SCSTAT_MAX_STRING_LEN];
	scstat_node_quorum_t *qstatn;

	/* section heading */
	(void) putchar('\n');
	(void) puts(gettext("-- Quorum Votes by Node (current status) --"));
	(void) putchar('\n');

	print_column(COLW20, NULL);
	print_column(COLW20, gettext("Node Name"));
	print_column(8, gettext("Present"));
	print_column(9, gettext("Possible"));
	print_column(0, gettext("Status"));
	(void) putchar('\n');

	print_column(COLW20, NULL);
	print_column(COLW20, dashes(buffer, gettext("Node Name")));
	print_column(8, dashes(buffer, gettext("Present")));
	print_column(9, dashes(buffer, gettext("Possible")));
	print_column(0, dashes(buffer, gettext("Status")));
	(void) putchar('\n');

	/* print quorum for every node */
	for (qstatn = qstat->scstat_node_quorum_list;  qstatn;
	    qstatn = qstatn->scstat_node_quorum_next) {

		/* okay to print for this node? */
		if (node && qstatn->scstat_node_name &&
		    strcmp(node, qstatn->scstat_node_name) != 0)
			continue;

		/* label */
		print_column(COLW20, gettext("  Node votes:"));

		/* node name */
		print_column_value(COLW20, qstatn->scstat_node_name);

		/* votes present */
		(void) sprintf(buffer, "%d", qstatn->scstat_vote_contributed);
		print_column_value(9, buffer);

		/* votes possible */
		(void) sprintf(buffer, "%d", qstatn->scstat_vote_configured);
		print_column_value(8, buffer);

		/* status */
		print_column_value(0, qstatn->scstat_node_quorum_statstr);

		/* end of line */
		(void) putchar('\n');
	}

	(void) putchar('\n');
}

/*
 * print_quorum_devvotes
 *
 *	Print quorum votes by device.
 */
static void
print_quorum_devvotes(char *node, scstat_quorum_t *qstat)
{
	char buffer[SCSTAT_MAX_STRING_LEN];
	scstat_quorumdev_t *qstatd;

	/* section heading */
	(void) putchar('\n');
	(void) puts(gettext("-- Quorum Votes by Device (current status) --"));
	(void) putchar('\n');

	print_column(COLW20, NULL);
	print_column(COLW20, gettext("Device Name"));
	print_column(8, gettext("Present"));
	print_column(9, gettext("Possible"));
	print_column(0, gettext("Status"));
	(void) putchar('\n');

	print_column(COLW20, NULL);
	print_column(COLW20, dashes(buffer, gettext("Device Name")));
	print_column(8, dashes(buffer, gettext("Present")));
	print_column(9, dashes(buffer, gettext("Possible")));
	print_column(0, dashes(buffer, gettext("Status")));
	(void) putchar('\n');

	/* print quorum for every device */
	for (qstatd = qstat->scstat_quorumdev_list;  qstatd;
	    qstatd = qstatd->scstat_quorumdev_next) {

		/*
		 * okay to print for this node?
		 *
		 * If "node" is given, we only print those devices
		 * with enabled paths to the node.
		 */
		if (node && !is_node_enabledfor_qdev(node, qstatd))
			continue;

		/* label */
		print_column(COLW20, gettext("  Device votes:"));

		/* device name */
		print_column_value(COLW20, qstatd->scstat_quorumdev_name);

		/* votes present */
		(void) sprintf(buffer, "%d", qstatd->scstat_vote_contributed);
		print_column_value(9, buffer);

		/* votes possible */
		(void) sprintf(buffer, "%d", qstatd->scstat_vote_configured);
		print_column_value(8, buffer);

		/* status */
		print_column_value(0, qstatd->scstat_quorumdev_statstr);

		/* end of line */
		(void) putchar('\n');
	}
}

/*
 * is_node_enabledfor_qdev
 *
 *	Return non-zero, if the node has an enabled path to the quorum device
 *	Otherwise, return zero.
 */
static int
is_node_enabledfor_qdev(char *node, scstat_quorumdev_t *qstatd)
{
	scstat_node_access_t *qaccess;

	/* Check arguments */
	if (node == NULL || qstatd == NULL)
		return (0);

	/* Look for an enabled path */
	for (qaccess = qstatd->scstat_node_access_list;
	    qaccess;  qaccess = qaccess->scstat_node_next) {
		if (qaccess->scstat_access_status != SCSTAT_ACCESS_ENABLED)
			continue;
		if (qaccess->scstat_node_name &&
		    strcmp(node, qaccess->scstat_node_name) == 0)
			return (1);
	}

	return (0);
}

/*
 * print_rgs
 *
 *	Print status of resource groups and their resources.
 */
static scstat_errno_t
print_rgs(char *node, int verbose)
{
	scstat_errno_t error;
	scstat_rg_t *lrgs = NULL;

	/* get rg status for the cluster */
	error = scstat_get_rgs(&lrgs);
	if (error != SCSTAT_ENOERR)
		goto cleanup;

	if (lrgs == NULL)
		goto cleanup;

	/* Print RGs summary */
	print_rg_summary(node, verbose, lrgs);

	/* Print RGs detail */
	print_rg_detail(node, verbose, lrgs);

	/* Print resources detail */
	print_rs_detail(node, verbose, lrgs);

cleanup:
	/* free space */
	if (lrgs != NULL)
		scstat_free_rgs(lrgs);

	return (error);
}

/*
 * print_rg_summary
 *
 *	Print summary config data of resource groups and their resources.
 */
/* ARGSUSED */
static void
print_rg_summary(char *node, int verbose, scstat_rg_t *lrgs)
{
	scstat_rg_t *rg;
	scstat_rs_t *rs;
	char buffer[SCSTAT_MAX_STRING_LEN];
	int found;

	/*
	 * Print Resource Group and Resources summary
	 */
	(void) putchar('\n');
	(void) puts(gettext("-- Resource Groups and Resources --"));
	(void) putchar('\n');

	print_column(12, NULL);
	print_column(COLW15, gettext("Group Name"));
	print_column(0, gettext("Resources"));
	(void) putchar('\n');

	print_column(12, NULL);
	print_column(COLW15, dashes(buffer, gettext("Group Name")));
	print_column(0, dashes(buffer, gettext("Resources")));
	(void) putchar('\n');

	/* print summary for each RG */
	for (rg = lrgs;  rg;  rg = rg->scstat_rg_next) {

		/* Skip the RG if "node" is not in RG's nodelist */
		if (node && !is_node_in_rg(node, rg))
			continue;

		/* label */
		print_column(12, gettext(" Resources:"));

		/* resource group name */
		print_column_value(COLW15, rg->scstat_rg_name);

		/* resource list */
		found = 0;
		for (rs = rg->scstat_rs_list;  rs;  rs = rs->scstat_rs_next) {
			if (rs->scstat_rs_name && *rs->scstat_rs_name) {
				if (found)
					(void) putchar(' ');
				++found;
				(void) fputs(rs->scstat_rs_name, stdout);
			}
		}
		if (!found)
			(void) putchar('-');

		/* new line */
		(void) putchar('\n');
	}

	/* blank line */
	(void) putchar('\n');
}

/*
 * print_rg_detail
 *
 *	Print detail config data for resource groups.
 */
/* ARGSUSED */
static void
print_rg_detail(char *node, int verbose, scstat_rg_t *lrgs)
{
	scstat_rg_t *rg;
	scstat_rg_status_t *rgstat;
	boolean_t suspended = B_FALSE;
	char buffer[SCSTAT_MAX_STRING_LEN];
	int found;

	/*
	 * Print Resource Group detail
	 */
	(void) putchar('\n');
	(void) puts(gettext("-- Resource Groups --"));
	(void) putchar('\n');

	print_column(12, NULL);
	print_column(COLW15, gettext("Group Name"));
	print_column(COLW25, gettext("Node Name"));
	print_column(COLW15, gettext("State"));
	print_column(0, gettext("Suspended"));
	(void) putchar('\n');

	print_column(12, NULL);
	print_column(COLW15, dashes(buffer, gettext("Group Name")));
	print_column(COLW25, dashes(buffer, gettext("Node Name")));
	print_column(COLW15, dashes(buffer, gettext("State")));
	print_column(0, dashes(buffer, gettext("Suspended")));
	(void) putchar('\n');

	/* print node and state for each RG */
	for (rg = lrgs;  rg;  rg = rg->scstat_rg_next) {

		/* init line print counter for this RG */
		found = 0;

		/* Skip the RG if "node" is not in RG's nodelist */
		if (node && !is_node_in_rg(node, rg))
			continue;

		/* Check the Suspend_automatic_recovery property */
		suspended = B_FALSE;
		(void) scstat_get_rg_boolprop(rg->scstat_rg_name,
		    SCHA_RG_SUSP_AUTO_RECOVERY, &suspended);

		/*
		 * Print per-node RG status for each node in the list.
		 * Lines are grouped by node.
		 */
		for (rgstat = rg->scstat_rg_status_list;  rgstat;
		    rgstat = rgstat->scstat_rg_status_by_node_next) {

			/* found one to print */
			++found;

			/* label */
			print_column(12, gettext("     Group:"));

			/* resource group name */
			print_column_value(COLW15, rg->scstat_rg_name);

			/* resource group node name */
			print_column_value(COLW25, rgstat->scstat_node_name);

			/* resource group state */
			print_column_value(COLW15, rgstat->scstat_rg_statstr);

			/* Suspended RG? */
			if (suspended) {
				print_column_value(0, gettext("Yes"));
			} else {
				print_column_value(0, gettext("No"));
			}

			/* new line */
			(void) putchar('\n');
		}

		/* blank line seperates each group of rgs */
		if (found)
			(void) putchar('\n');
	}
}

/*
 * print_rs_detail
 *
 *	Print detail config data for resources.
 */
/* ARGSUSED */
static void
print_rs_detail(char *node, int verbose, scstat_rg_t *lrgs)
{
	scstat_rg_t *rg;
	scstat_rs_t *rs;
	scstat_rs_status_t *rsstat;
	char buffer[SCSTAT_MAX_STRING_LEN];
	int found;

	/*
	 * Print Resource detail
	 */
	(void) putchar('\n');
	(void) puts(gettext("-- Resources --"));
	(void) putchar('\n');

	print_column(12, NULL);
	print_column(COLW15, gettext("Resource Name"));
	print_column(COLW25, gettext("Node Name"));
	print_column(COLW15, gettext("State"));
	print_column(0, gettext("Status Message"));
	(void) putchar('\n');

	print_column(12, NULL);
	print_column(COLW15, dashes(buffer, gettext("Resource Name")));
	print_column(COLW25, dashes(buffer, gettext("Node Name")));
	print_column(COLW15, dashes(buffer, gettext("State")));
	print_column(0, dashes(buffer, gettext("Status Message")));
	(void) putchar('\n');

	/* print node and state for each resource in each RG */
	for (rg = lrgs;  rg;  rg = rg->scstat_rg_next) {

		/* Skip the RG if "node" is not in RG's nodelist */
		if (node && !is_node_in_rg(node, rg))
			continue;

		/* Print per-node status for each resource */
		for (rs = rg->scstat_rs_list;  rs;  rs = rs->scstat_rs_next) {

			/* init line print counter for this RS */
			found = 0;

			/*
			 * Print per-node RS status for each node in the list.
			 * Lines are grouped by node.
			 */
			for (rsstat = rs->scstat_rs_status_list;  rsstat;
			    rsstat = rsstat->scstat_rs_status_by_node_next) {

				/* found one to print */
				++found;

				/* label */
				print_column(12, gettext("  Resource:"));

				/* resource name */
				print_column_value(COLW15, rs->scstat_rs_name);

				/* resource node name */
				print_column_value(COLW25,
				    rsstat->scstat_node_name);

				/* resource state */
				print_column_value(COLW15,
				    rsstat->scstat_rs_state_str);

				/* resource status message */
				print_column_value(0,
				    rsstat->scstat_rs_statstr);

				/* new line */
				(void) putchar('\n');
			}

			/* blank line seperates each group of resources */
			if (found)
				(void) putchar('\n');
		}
	}
}

/*
 * is_node_in_rg
 *
 *	Return non-zero, if the node is found in the resource group.
 *	Otherwise, return zero.
 */
static int
is_node_in_rg(char *node, scstat_rg_t *lrgs)
{
	scstat_rg_t *rg;
	scstat_rg_status_t *rgstat;

	/* Check argument */
	if (node == NULL)
		return (0);

	/* Search for the node */
	for (rg = lrgs;  rg;  rg = rg->scstat_rg_next) {
		for (rgstat = rg->scstat_rg_status_list;  rgstat;
		    rgstat = rgstat->scstat_rg_status_by_node_next) {
			if (rgstat->scstat_node_name &&
			    strcmp(node, rgstat->scstat_node_name) == 0)
				return (1);
		}
	}

	return (0);
}

/*
 * print_ipmp_grps
 *
 *	Print detail config data for an ipmp groups in a node.
 */
static void
print_ipmp_grps(scstat_ipmp_stat_list_t *node_grps)
{
	char *value;
	scstat_grp_stat_list_t	*gsp;
	unsigned int i = 0;

	/* Print per-grp status for this node */
	for (gsp = node_grps->pgslt;  gsp;  gsp = gsp->next) {

		/* Print per-adp status for this group */
		for (i = 0;  i < gsp->pgst.num_adps; i++) {

			/* label */
			print_column(14, gettext("  IPMP Group:"));

			/* node name */
			print_column_value(COLW20, node_grps->name);

			/* group name */
			print_column_value(COLW8, gsp->grp_name);

			/* group status */
			switch (gsp->status) {
			case PNM_STAT_OK:
				value = gettext("Online");
				break;

			case PNM_STAT_DOWN:
				value = gettext("Offline");
				break;

			case PNM_STAT_STANDBY:
			default:
				value = gettext("Unknown");
				break;
			}
			print_column_value(COLW15, value);

			/* adapter name */
			print_column_value(COLW10,
			    gsp->pgst.pnm_adp_stat[i].adp_name);

			/* adapter status */
			switch (gsp->pgst.pnm_adp_stat[i].status) {
			case PNM_STAT_OK:
				value = gettext("Online");
				break;

			case PNM_STAT_DOWN:
				value = gettext("Offline");
				break;

			case PNM_STAT_STANDBY:
				value = gettext("Standby");
				break;

			default:
				value = gettext("Unknown");
				break;
			}
			print_column_value(0, value);

			/* new line */
				(void) putchar('\n');
		} /* next adp */

	} /* next group */
}

/*
 * print_ipmp
 *
 *	Print detail config data for ipmp.
 */
/* ARGSUSED */
static scstat_errno_t
print_ipmp(char *node, int verbose)
{
	scstat_ipmp_stat_list_t	*isp = NULL, *ispp = NULL;
	scstat_errno_t error = 0;
	char buffer[SCSTAT_MAX_STRING_LEN];
	size_t nodelen = 0;

	/*
	 * Print IPMP detail
	 */
	(void) putchar('\n');
	(void) puts(gettext("-- IPMP Groups --"));
	(void) putchar('\n');

	print_column(14, NULL);
	print_column(COLW20, gettext("Node Name"));
	print_column(COLW8, gettext("Group"));
	print_column(COLW15, gettext("Status"));
	print_column(COLW10, gettext("Adapter"));
	print_column(0, gettext("Status"));
	(void) putchar('\n');

	print_column(14, NULL);
	print_column(COLW20, dashes(buffer, gettext("Node Name")));
	print_column(COLW8, dashes(buffer, gettext("Group")));
	print_column(COLW15, dashes(buffer, gettext("Status")));
	print_column(COLW10, dashes(buffer, gettext("Adapter")));
	print_column(0, dashes(buffer, gettext("Status")));
	(void) putchar('\n');

	/* Get ipmp state from libscstat */
	error =  scstat_get_ipmp_state(&isp);
	if (error)
		return (error);

	/* print state */
	for (ispp = isp;  ispp;  ispp = ispp->next) {
		/* Skip if "node" does not match or a zone is returned */
		if ((node && (strcmp(ispp->name, node) != 0)) ||
		    (strchr(ispp->name, ':')))
			continue;

		print_ipmp_grps(ispp);

		/* new line */
		(void) putchar('\n');
	}

	/*
	 * Go through the list once more for IPMP groups in exclusive IP
	 * zones
	 */

	(void) putchar('\n');
	(void) puts(gettext("-- IPMP Groups in Zones --"));
	(void) putchar('\n');

	print_column(14, NULL);
	print_column(COLW20, gettext("Zone Name"));
	print_column(COLW8, gettext("Group"));
	print_column(COLW15, gettext("Status"));
	print_column(COLW10, gettext("Adapter"));
	print_column(0, gettext("Status"));
	(void) putchar('\n');

	print_column(14, NULL);
	print_column(COLW20, dashes(buffer, gettext("Zone Name")));
	print_column(COLW8, dashes(buffer, gettext("Group")));
	print_column(COLW15, dashes(buffer, gettext("Status")));
	print_column(COLW10, dashes(buffer, gettext("Adapter")));
	print_column(0, dashes(buffer, gettext("Status")));
	(void) putchar('\n');

	/* print state */
	if (node)
		nodelen = strlen(node);
	for (ispp = isp;  ispp;  ispp = ispp->next) {
		/* Skip if it is not a zone */
		if (!strchr(ispp->name, ':'))
			continue;

		/* Skip if node part doesn't match if node is provided */
		if (node) {
			if (strlen(ispp->name) <= nodelen)
				continue;
			if (strncasecmp(node, ispp->name, strlen(node)) ||
			    (ispp->name[nodelen] != ':')) {
				continue;
			}
		}

		print_ipmp_grps(ispp);

		/* new line */
		(void) putchar('\n');
	}

	scstat_ipmp_stat_list_free(isp);
	return (0);

}

/*
 * print_error
 *
 *	Print error to stder.
 */
static void
print_error(scstat_errno_t error)
{
	char buffer[SCSTAT_MAX_STRING_LEN];

	buffer[0] = '\0';
	scstat_strerr(error, buffer);
	(void) fprintf(stderr, "%s:  %s.\n", progname, buffer);
}
