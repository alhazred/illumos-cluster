#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)Makefile.cmd	1.71	08/05/27 SMI"
#
# cmd/Makefile.cmd
#
# Definitions common to command source.
#

include $(SRC)/Makefile.master

ROOTBIN=	$(VROOT)/usr/bin
ROOTLIB=	$(VROOT)/usr/lib
ROOTSHLIB=	$(VROOT)/usr/share/lib
ROOTSBIN=	$(VROOT)/sbin
ROOTUSRSBIN=	$(VROOT)/usr/sbin
ROOTETC=	$(VROOT)/etc

LDLIBS =	$(LDLIBS.cmd)
LDFLAGS.cmd +=	$(STRIPFLAG) $(ENVLDFLAGS1) $(ENVLDFLAGS2) $(ENVLDFLAGS3)
LDFLAGS =	$(LDFLAGS.cmd)

LINTOUT=	lint.out

ROOTPROG=	$(PROG:%=$(ROOTBIN)/%)
ROOTSHFILES=	$(SHFILES:%=$(ROOTBIN)/%)
ROOTLIBPROG=	$(PROG:%=$(ROOTLIB)/%)
ROOTLIBSHFILES= $(SHFILES:%=$(ROOTLIB)/%)
ROOTSHLIBPROG=	$(PROG:%=$(ROOTSHLIB)/%)
ROOTSBINPROG=	$(PROG:%=$(ROOTSBIN)/%)
ROOTUSRSBINPROG=$(PROG:%=$(ROOTUSRSBIN)/%)
ROOTETCPROG=	$(PROG:%=$(ROOTETC)/%)

$(ROOTBIN)/%: %
	$(INS.file)

$(ROOTLIB)/%: %
	$(INS.file)

$(ROOTSHLIB)/%: %
	$(INS.file)

$(ROOTSBIN)/%: %
	$(INS.file)

$(ROOTUSRSBIN)/%: %
	$(INS.file)

$(ROOTETC)/%: %
	$(INS.file)

.KEEP_STATE:

CLASS = 32

ROOTCLUSTETC=		$(VROOT)/etc/cluster
ROOTCLUSTINC=		$(VROOT)/usr/cluster/include
ROOTCLUSTBIN=		$(VROOT)/usr/cluster/bin
ROOTCLUSTLIB=		$(VROOT)/usr/cluster/lib
ROOTCLUSTCLZONES=	$(ROOTCLUSTETC)/zone_cluster
ROOTBRANDLIB=		$(VROOT)/usr/lib/brand
ROOTCLUSTBRAND=		$(ROOTBRANDLIB)/cluster
ROOTCLUSTLIBSC=		$(ROOTCLUSTLIB)/sc
ROOTCLUSTLIBUCMM=	$(ROOTCLUSTLIB)/ucmm
ROOTCLUSTLIBSCADMIN=	$(ROOTCLUSTLIB)/scadmin
ROOTCLUSTLIBSCADMINQL=	$(ROOTCLUSTLIB)/scadmin/ql
ROOTCLUSTLIBCMD=	$(ROOTCLUSTLIB)/clcommands
ROOTCLUSTLIBMIB=	$(ROOTCLUSTLIB)/mib
ROOTOPT=		$(VROOT)/opt/$(PKGNAME)
ROOTOPTBIN=		$(VROOT)/opt/$(PKGNAME)/bin
ROOTOPTBIN64=		$(VROOT)/opt/$(PKGNAME)/bin/$(MACH64)
ROOTOPTETC=		$(VROOT)/opt/$(PKGNAME)/etc
ROOTOPTETC64=		$(VROOT)/opt/$(PKGNAME)/etc/$(MACH64)
ROOTOPTLIB=		$(VROOT)/opt/$(PKGNAME)/lib
ROOTOPTLIB64=		$(VROOT)/opt/$(PKGNAME)/lib/$(MACH64)
ROOTCLUSTLIBRGM=	$(ROOTCLUSTLIB)/rgm
ROOTCLUSTRT=		$(ROOTCLUSTLIBRGM)/rt/$(RTDIRNAME)
ROOTCLUSTRTREG=		$(ROOTCLUSTLIBRGM)/rtreg
ROOTCLUSTVARRUN=	$(VROOT)/var/cluster/run
ROOTCLUSTVARRUNSCINSTALL=	$(VROOT)/var/cluster/run/scinstall
ROOTCLUSTETCQL=		$(ROOTCLUSTETC)/ql

ROOTCLUSTPROG=		$(PROG:%=$(ROOTCLUSTBIN)/%)
ROOTCLUSTPRIVPROG=	$(PROG:%=$(ROOTCLUSTLIBSC)/%)
ROOTCLUSTNASPROG=	$(PROG:%=$(ROOTCLUSTLIBSC)/nas/%)
ROOTCLUSTQLPROG=	$(PROG:%=$(ROOTCLUSTLIBSCADMINQL)/%)
ROOTCLUSTETCQLPROG=	$(PROG:%=$(ROOTCLUSTETCQL)/%)
ROOTCLUSTDSPROG=	$(PROG:%=$(ROOTCLUSTLIB)/ds/%)
ROOTCLUSTPRIVUCMMPROG=	$(PROG:%=$(ROOTCLUSTLIBUCMM)/%)
ROOTCLUSTPRIVUCMMSCRIPT=	$(SCRIPT:%=$(ROOTCLUSTLIBUCMM)/%)
ROOTCLUSTPRIVUCMMDAEMON=	$(DAEMON:%=$(ROOTCLUSTLIBUCMM)/%)
ROOTCLUSTDAEMON=	$(DAEMON:%=$(ROOTCLUSTLIBSC)/%)
ROOTCLUSTDAEMON64=	$(DAEMON:%=$(ROOTCLUSTLIBSC)/$(MACH64)/%)
ROOTOPTBINPROG=		$(PROG:%=$(ROOTOPTBIN)/%)
ROOTOPTBINPROG64=	$(PROG:%=$(ROOTOPTBIN64)/%)
ROOTOPTETCPROG=		$(PROG:%=$(ROOTOPTETC)/%)
ROOTOPTETCSCRIPTS=	$(SCRIPTS:%=$(ROOTOPTETC)/%)
ROOTOPTETCPROG64=	$(PROG:%=$(ROOTOPTETC64)/%)
ROOTOPTETCCONF=		$(CONF:%=$(ROOTOPTETC)/%)
ROOTOPTLIBPROG=		$(PROG:%=$(ROOTOPTLIB)/%)
ROOTOPTLIBPROG64=	$(PROG:%=$(ROOTOPTLIB64)/%)
ROOTCLUSTRTPROG=	$(PROG:%=$(ROOTCLUSTRT)/%)
ROOTCLUSTRTREGRTRFILE=	$(RTRFILE:%=$(ROOTCLUSTRTREG)/%)

# scsymon related macros
ROOTSCSYMONBIN=		$(VROOT)/usr/cluster/lib/scsymon

#ccp, cconsole related macros
ROOTCCONSBIN=		$(VROOT)/opt/SUNWcluster/bin
ROOTCCP=		$(VROOT)/opt/SUNWcluster/etc/ccp
ROOTCCPCCONS=		$(ROOTCCP)/cconsole
ROOTCCPCRLOGIN=		$(ROOTCCP)/crlogin
ROOTCCPCTELNET=		$(ROOTCCP)/ctelnet
ROOTCCONSICONS=		$(VROOT)/opt/SUNWcluster/icons
ROOTCCONSXDEFAULTS=	$(VROOT)/opt/SUNWcluster/lib/locale/C/app-defaults
ROOTCCONSPROG=		$(PROG:%=$(ROOTCCONSBIN)/%)

ROOTRSMAPITEST=		$(VROOT)/usr/cluster/rsm_api_test
ROOTRSMAPIPROG=		$(PROG:%=$(ROOTRSMAPITEST)/%)

$(ROOTRSMAPITEST)/%: %
	$(INS.file)

# Define the majority text domain in this directory.
TEXT_DOMAIN=	SUNW_SC_CMD

TMPLDIR = .

CLOBBERFILES += $(LINTFILES) $(PIFILES) $(CHECK_FILES)

include $(SRC)/cmd/Makefile.cmd.clprops
