/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)rpc_scrcmd_closedown.c	1.3	08/05/20 SMI"

/*
 * rpc_scrcmd.c
 *
 *	This file is concatenated onto rpc_scrcmd.c,
 *	after it is has been created by rpcgen.
 */

/*
 * closedown thread
 */
/* ARGSUSED */
void *
closedown(void *arg)
{
	/* CONSTCOND */
	while (1) {
		(void) sleep(SCRCMD_RPCSVC_CLOSEDOWN/2);

		if (mutex_trylock(&_svcstate_lock) != 0)
			continue;

		if (mutex_trylock(&do_command_threads_lock) != 0) {
			(void) mutex_unlock(&_svcstate_lock);
			continue;
		}

		if (_rpcsvcstate == _IDLE && _rpcsvccount == 0 &&
		    do_command_threads == 0) {
			int size;
			int i, openfd = 0;

			size = svc_max_pollfd;
			for (i = 0; i < size && openfd < 2; i++)
				if (svc_pollfd[i].fd >= 0)
					openfd++;
			if (openfd <= 1)
				exit(0);
		} else {
			_rpcsvcstate = _IDLE;
		}

		(void) mutex_unlock(&do_command_threads_lock);
		(void) mutex_unlock(&_svcstate_lock);
	}
	/* NOTREACHED */
}
