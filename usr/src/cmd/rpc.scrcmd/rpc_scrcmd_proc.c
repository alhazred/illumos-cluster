/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)rpc_scrcmd_proc.c	1.5	08/05/20 SMI"

/*
 * rpc_scrcmd_proc.c
 *
 *	Server procedures for rpc.scrcmd(1M)
 */

/*lint -e793 */
/*lint -e702 */

#include "rpc_scrcmd.h"

#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <errno.h>
#include <unistd.h>
#include <libgen.h>
#include <errno.h>
#include <locale.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>

#include <syslog.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

#include <scha.h>


/*lint -e708 */

/*
 * This "scrcmd_cmds" list must be kept in sync with the client list.
 */
static scrcmd_cmd_t scrcmd_cmds[] = { SCRCMD_CMDS };

/*
 * argument for the "do_command()" thread
 */
typedef struct do_command_arg {
	in_port_t cmd_outport;
	in_port_t cmd_errport;
	uint_t cmd_protofmly;
	void *cmd_addr;
	uint_t cmd_addrlen;
	char *cmd_string;
	sc_envlist_t *cmd_envlist;
} do_command_arg_t;

/*
 * Count of running command threads, to avoid closedown.
 */
int do_command_threads = 0;
mutex_t do_command_threads_lock = DEFAULTMUTEX;

static void *do_command(void *do_command_arg);
static void free_cmd_arg(do_command_arg_t **cmd_argp);
static int get_connection(do_command_arg_t *arg, in_port_t port);
static int check_cmd_flags(uint_t flags, sc_errlist_t *errlist,
    sc_envlist_t *envlist);
static int verify_is_interconnect(struct svc_req *rqstp, char *name,
    sc_errlist_t *errlist, sc_envlist_t *envlist);
static void append_to_errlist(sc_errlist_t *errlist, char *errmsg);
static char *env_gettext(char *msgid, sc_envlist_t *envlist);
static sc_envlist_t *dup_envlist(sc_envlist_t *envlist);
static void free_envlist(sc_envlist_t *envlist);
static char *getenv_from_envlist(char *name, sc_envlist_t *envlist);
static void copy_buffer_to_file(char **filename, char *buffer);
static void setenv_from_envlist(sc_envlist_t *envlist);
static char *scrcmd_strerr(int err, sc_envlist_t *envlist);

/* ARGSUSED */
bool_t
scrcmdproc_do_cmd_1_svc(unsigned int outport, unsigned int errport,
    char *command_name, char *command_args, sc_envlist_t envlist,
    char *buffer, sc_result *result, struct svc_req *rqstp)
{
	int res;
	scrcmd_cmd_t *cmd;
	char *command_path;
	char *command_string = NULL;
	char *filename = NULL;
	sc_envlist_t *newlist = NULL;
	do_command_arg_t *do_command_arg = NULL;
	uint_t protofmly;
	char *rnetid;
	struct netbuf *rnetbuf;
	struct netconfig *rnetconfig = NULL;
	uint_t raddrlen;
	void *raddr;

	/* Initialize result code */
	bzero(result, sizeof (sc_result));

	/* Make sure that the command_name is set */
	if (command_name == NULL) {
		result->sc_res_errno = ENOEXEC;
		append_to_errlist(&result->sc_res_errlist,
		    env_gettext("No command name given", &envlist));
		append_to_errlist(&result->sc_res_errlist,
		    scrcmd_strerr(ENOEXEC, &envlist));
		goto cleanup;
	}

	/* Get the command to execute */
	command_path = NULL;
	for (cmd = scrcmd_cmds;  cmd->cmd_name != NULL;  ++cmd) {
		if (strcmp(cmd->cmd_name, command_name) == 0) {
			command_path = cmd->cmd_path;
			break;
		}
	}
	if (command_path == NULL) {
		result->sc_res_errno = ENOEXEC;
		append_to_errlist(&result->sc_res_errlist,
		    scrcmd_strerr(ENOEXEC, &envlist));
		goto cleanup;
	}

	/* Check the command flags */
	res = check_cmd_flags(cmd->cmd_flags, &result->sc_res_errlist,
	    &envlist);
	if (res != 0) {
		result->sc_res_errno = res;
		goto cleanup;
	}

	/*
	 * If SCRCMD_FLG_INTERCONNECT is set for this command, verify
	 * that the command request has arrived on the private interconnect.
	 */
	if (cmd->cmd_flags & SCRCMD_FLG_INTERCONNECT) {
		res = verify_is_interconnect(rqstp, cmd->cmd_name,
		    &result->sc_res_errlist, &envlist);
		if (res != 0) {
			result->sc_res_errno = res;
			goto cleanup;
		}
	}

	/* Make sure we can run the command */
	if (access(command_path, (R_OK | X_OK)) != 0) {
		res = ENOEXEC;
		result->sc_res_errno = res;
		append_to_errlist(&result->sc_res_errlist,
		    scrcmd_strerr(res, &envlist));
		goto cleanup;
	}

	/* Copy the buffer to a file, if it is valid */
	if (buffer != NULL && strcmp(buffer, "NOOP") != 0) {
		copy_buffer_to_file(&filename, buffer);
		if (filename == NULL) {
			res = EIO;
			result->sc_res_errno = res;
			append_to_errlist(&result->sc_res_errlist,
			    scrcmd_strerr(res, &envlist));
			goto cleanup;
		}
	} else {
		filename = strdup("");
	}

	/* Concat command args to the command */
	if (command_args != NULL && *command_args != '\0') {
		command_string = (char *)calloc(1, strlen(command_path) +
		    strlen(command_args) + strlen(filename) + 3);
		if (command_string != NULL) {
			(void) strcpy(command_string, command_path);
			(void) strcat(command_string, " ");
			(void) strcat(command_string, command_args);
			(void) strcat(command_string, " ");
			(void) strcat(command_string, filename);
		}
	} else {
		command_string = strdup(command_path);
	}
	if (command_string == NULL) {
		res = ENOMEM;
		result->sc_res_errno = res;
		append_to_errlist(&result->sc_res_errlist,
		    scrcmd_strerr(res, &envlist));
		goto cleanup;
	}

	/* Duplicate the "envlist" */
	if ((newlist = dup_envlist(&envlist)) == NULL) {
		res = ENOMEM;
		result->sc_res_errno = res;
		append_to_errlist(&result->sc_res_errlist,
		    scrcmd_strerr(res, &envlist));
		goto cleanup;
	}

	/* Get the network protocol family */
	rnetid = rqstp->rq_xprt->xp_netid;
	rnetconfig = getnetconfigent(rnetid);
	if (rnetconfig == NULL ||
	    rnetconfig->nc_protofmly == NULL) {
		res = ENOEXEC;
		result->sc_res_errno = res;
		append_to_errlist(&result->sc_res_errlist,
		    scrcmd_strerr(res, &envlist));
		goto cleanup;
	}
	if (strcmp(rnetconfig->nc_protofmly, "inet") == 0) {
		protofmly = AF_INET;
	} else if (strcmp(rnetconfig->nc_protofmly, "inet6") == 0) {
		protofmly = AF_INET6;
	} else {
		res = EPROTONOSUPPORT;
		freenetconfigent(rnetconfig);
		result->sc_res_errno = res;
		append_to_errlist(&result->sc_res_errlist, env_gettext(
		    "Unsupported network protocol family", &envlist));
		goto cleanup;
	}
	freenetconfigent(rnetconfig);
	rnetconfig = NULL;

	/* Get the remote address */
	rnetbuf = &rqstp->rq_xprt->xp_rtaddr;
	if (rnetbuf->buf == NULL) {
		res = ENOEXEC;
		result->sc_res_errno = res;
		append_to_errlist(&result->sc_res_errlist,
		    scrcmd_strerr(res, &envlist));
		goto cleanup;
	}
	raddrlen = rnetbuf->len;
	raddr = calloc(1, rnetbuf->len);
	if (raddr == NULL) {
		res = ENOMEM;
		result->sc_res_errno = res;
		append_to_errlist(&result->sc_res_errlist,
		    scrcmd_strerr(res, &envlist));
		goto cleanup;
	}
	bcopy(rnetbuf->buf, raddr, raddrlen);

	/* Initialize the argument structure for the do_command thread */
	do_command_arg = (do_command_arg_t *)calloc(1,
	    sizeof (do_command_arg_t));
	if (do_command_arg == NULL) {
		res = ENOMEM;
		result->sc_res_errno = res;
		append_to_errlist(&result->sc_res_errlist,
		    scrcmd_strerr(res, &envlist));
		goto cleanup;
	}
	do_command_arg->cmd_outport = (in_port_t)outport;
	do_command_arg->cmd_errport = (in_port_t)errport;
	do_command_arg->cmd_protofmly = protofmly;
	do_command_arg->cmd_addr = raddr;
	do_command_arg->cmd_addrlen = raddrlen;
	do_command_arg->cmd_string = command_string;
	do_command_arg->cmd_envlist = newlist;

	/* Kick off the thread to run the command */
	(void) mutex_lock(&do_command_threads_lock);
	++do_command_threads;
	(void) mutex_unlock(&do_command_threads_lock);
	if (thr_create(NULL, 0, do_command, do_command_arg, 0, NULL) != 0) {
		res = ENOEXEC;
		(void) mutex_lock(&do_command_threads_lock);
		--do_command_threads;
		(void) mutex_unlock(&do_command_threads_lock);
		result->sc_res_errno = res;
		append_to_errlist(&result->sc_res_errlist,
		    scrcmd_strerr(res, &envlist));
		goto cleanup;
	}

	/* Done - return to RPC client */
	return (TRUE);

	/*
	 * "cleanup" is only taken if the "do_command" thread is not started.
	 * Otherwise, the "do_command" thread is responsible for cleanup.
	 */

cleanup:
	if (command_string != NULL) {
		free(command_string);
		command_string = NULL;
		if (do_command_arg != NULL) {
			do_command_arg->cmd_string = NULL;
		}
	}
	if (newlist != NULL) {
		free_envlist(newlist);
		newlist = NULL;
		if (do_command_arg != NULL) {
			do_command_arg->cmd_envlist = NULL;
		}
	}
	if (do_command_arg != NULL) {
		free_cmd_arg(&do_command_arg);
	}
	if (rnetconfig != NULL) {
		freenetconfigent(rnetconfig);
		rnetconfig = NULL;
	}

	if (filename != NULL) {
		(void) remove(filename);
		free(filename);
	}

	/* Always return TRUE */
	return (TRUE);
}

/* ARGSUSED */
int
scrcmdprog_1_freeresult(SVCXPRT *transp, xdrproc_t xdr_result,
    caddr_t result)
{
	xdr_free(xdr_result, result);

	return (TRUE);
}

/*
 * do_command
 *
 * This is the do_command thread which actually runs the command
 * after the RPC client call has returned.
 *
 * This thread connects to the original caller on the outport and
 * errport ports.  The stdout and stderr from the command are copied
 * onto these connections.
 *
 * Before this thread exits, it is responsible for freeing all
 * of the memory associated with its command arguments.
 */
static void *
do_command(void *do_command_arg)
{
	int fd_out = -1;
	int fd_err = -1;
	pid_t pid = -1;
	int waitstat;
	do_command_arg_t *arg = (do_command_arg_t *)do_command_arg;

	/* Make sure that we have arguments */
	if (arg == NULL) {
		(void) mutex_lock(&do_command_threads_lock);
		--do_command_threads;
		(void) mutex_unlock(&do_command_threads_lock);
		thr_exit(0);
	}

	/* Establish connections */
	if ((fd_out = get_connection(arg, arg->cmd_outport)) < 0 ||
	    (fd_err = get_connection(arg, arg->cmd_errport)) < 0) {
		goto cleanup;
	}

	/* Fork a process to set the environment and run the command */
	if ((pid = fork()) == 0) {
		int status;

		/* fd_out becomes stdout */
		if (fd_out != 1) {
			if (dup2(fd_out, 1) < 0)
				exit(1);
			(void) close(fd_out);
		}

		/* fd_err becomes stderr */
		if (fd_err != 2) {
			if (dup2(fd_err, 2) < 0)
				exit(1);
			(void) close(fd_err);
		}

		/* set up the environment */
		setenv_from_envlist(arg->cmd_envlist);

		/* run the command */
		status = system(arg->cmd_string);

		/* if it exited normally, print command status */
		if (WIFEXITED(status)) {
			(void) printf("%s=%d\n", SCRCMD_CMD_STATUS_STRING,
			    WEXITSTATUS(status));
		}

		/* done */
		exit(0);
	}

	/* wait for command to complete */
	/*lint -e746 */
	while (waitpid(pid, &waitstat, 0) == -1 && errno == EINTR)
		;

	/*lint +e746 */

cleanup:
	free_cmd_arg(&arg);

	if (fd_out >= 0)
		(void) close(fd_out);
	if (fd_err >= 0)
		(void) close(fd_err);

	(void) mutex_lock(&do_command_threads_lock);
	--do_command_threads;
	(void) mutex_unlock(&do_command_threads_lock);

	thr_exit(0);

	return (NULL);
}

/*
 * free_cmd_arg
 *
 * Free all memory associated with the given "cmd_arg".
 */
static void
free_cmd_arg(do_command_arg_t **cmd_argp)
{
	do_command_arg_t *cmd_arg;

	/* Make sure there is something to free */
	if (cmd_argp == NULL || *cmd_argp == NULL)
		return;

	cmd_arg = *cmd_argp;

	if (cmd_arg->cmd_addr != NULL)
		free(cmd_arg->cmd_addr);
	if (cmd_arg->cmd_string != NULL)
		free(cmd_arg->cmd_string);
	if (cmd_arg->cmd_envlist != NULL)
		free_envlist(cmd_arg->cmd_envlist);

	free(cmd_arg);
}

/*
 * get_connection
 *
 * Create the socket and connect to it.
 *
 * Possible return values:
 *
 *	-1		- failure
 *	other           - the socket fd
 */
static int
get_connection(do_command_arg_t *arg, in_port_t port)
{
	int s;
	struct sockaddr_in sin;
	struct sockaddr_in *rsinp;
	struct sockaddr_in6 sin6;
	struct sockaddr_in6 *rsin6p;
	struct sockaddr *name;
	socklen_t namelen;

	/* Make sure that we have arguments */
	if (arg == NULL)
		return (-1);

	/* Create the socket */
	if ((s = socket((int)arg->cmd_protofmly, SOCK_STREAM, 0)) < 0)
		return (-1);

	/* Initialize sockaddr */
	switch (arg->cmd_protofmly) {
	case AF_INET:
		namelen = sizeof (sin);
		bzero(&sin, namelen);
		sin.sin_family = AF_INET;
		rsinp = (struct sockaddr_in *)arg->cmd_addr;
		bcopy(&rsinp->sin_addr.s_addr, &sin.sin_addr.s_addr,
		    sizeof (sin.sin_addr.s_addr));
		sin.sin_port = port;
		name = (struct sockaddr *)&sin;
		break;

	case AF_INET6:
		namelen = sizeof (sin6);
		bzero(&sin6, namelen);
		sin6.sin6_family = AF_INET6;
		rsin6p = (struct sockaddr_in6 *)arg->cmd_addr;
		bcopy(&rsin6p->sin6_addr, &sin6.sin6_addr,
		    sizeof (sin6.sin6_addr));
		sin6.sin6_port = port;
		name = (struct sockaddr *)&sin6;
		break;

	default:
		(void) close(s);
		return (-1);
	}

	/* Connect */
	if (connect(s, name, namelen) < 0) {
		(void) close(s);
		return (-1);
	}

	/* We have a good connection */
	return (s);
}

/*
 * check_cmd_flags
 *
 * Check the command flags to verify that it is okay to proceed with
 * local execution.
 *
 * If errors occur and the "errlist" is not NULL, error messages will be
 * added to this list.
 *
 * Possible return values:
 *
 *	0		- okay to proceed
 *	ENOEXEC		- both SCRCMD_FLG_NODEID and SCRCMD_FLG_NONODEID set
 *	ENOEXEC		- SCRCMD_FLG_NODEID test failed
 *	EEXIST		- already configured as an SC node (SCRCMD_FLG_NONODEID)
 *	ENOTACTIVE	- /etc/cluster/remoteconfiguration is not present
 */
static int
check_cmd_flags(uint_t flags, sc_errlist_t *errlist, sc_envlist_t *envlist)
{
	int res;
	uint_t mask;

	/* SCRCMD_FLG_NODEID and SCRCMD_FLG_NONODEID are mutually exclusive */
	mask = (SCRCMD_FLG_NODEID | SCRCMD_FLG_NONODEID);
	if ((flags & mask) == mask) {
		res = ENOEXEC;
		if (errlist != NULL) {
			append_to_errlist(errlist,
			    env_gettext("Internal error - "
			    "bad commands table", envlist));
			append_to_errlist(errlist, scrcmd_strerr(res, envlist));
		}
		return (res);
	}

	/* Node must be configured */
	if (flags & SCRCMD_FLG_NODEID) {
		if (access(SCRCMD_NODEID_FILE, F_OK) != 0) {
			res = ENOEXEC;
			if (errlist != NULL) {
				append_to_errlist(errlist, env_gettext(
				    "Cannot access nodeid file", envlist));
				append_to_errlist(errlist,
				    scrcmd_strerr(res, envlist));
			}
			return (res);
		}
	}

	/* Node must NOT be configured */
	if (flags & SCRCMD_FLG_NONODEID) {
		if (access(SCRCMD_NODEID_FILE, F_OK) != -1 ||
		    errno != ENOENT) {
			res = EEXIST;
			if (errlist != NULL) {
				append_to_errlist(errlist,
				    scrcmd_strerr(res, envlist));
			}
			return (res);
		}
	}

	/* /etc/cluster/remoteconfig must be present. */
	if (flags & SCRCMD_FLG_REMOTECONFIG) {
		if (access(SCRCMD_REMOTE_FILE, F_OK) != 0) {
			res = ENOTACTIVE;
			if (errlist != NULL) {
				append_to_errlist(errlist,
				    scrcmd_strerr(res, envlist));
			}
			return (res);
		}
	}

	return (0);
}

/*
 * verify_is_interconnect
 *
 * Verify that the command request has come in on the private
 * interconnect.
 *
 * If errors occur and the "errlist" is not NULL, error messages will be
 * added to this list.
 *
 * Possible return values:
 *
 *	0		- okay to proceed
 *	ENOEXEC		- failure
 */
static int
verify_is_interconnect(struct svc_req *rqstp, char *name,
    sc_errlist_t *errlist, sc_envlist_t *envlist)
{
	int res = 0;
	scha_err_t scha_res;
	char *lnetid;
	struct netconfig *lnetconfig = NULL;
	uint_t protofmly;
	struct netbuf *lnetbuf;
	void *laddr;
	scha_cluster_t scha_handle = (scha_cluster_t)0;
	scha_node_state_t scha_node_state;
	char *private_hostname;
	struct hostent *hp = (struct hostent *)0;
	struct sockaddr_in *lsinp;
	struct sockaddr_in6 *lsin6p;
	struct in6_addr in6;
	char laddr_string[INET6_ADDRSTRLEN + 1];
	char paddr_string[INET6_ADDRSTRLEN + 1];

	/* Set the local address netbuf */
	lnetbuf = &rqstp->rq_xprt->xp_ltaddr;

	/* Get the network protocol family */
	lnetid = rqstp->rq_xprt->xp_netid;
	lnetconfig = getnetconfigent(lnetid);
	if (lnetconfig == NULL ||
	    lnetconfig->nc_protofmly == NULL) {
		res = ENOEXEC;
		if (errlist != NULL) {
			append_to_errlist(errlist,
			    env_gettext("Unable to determine network "
			    "protocol family", envlist));
		}
		goto cleanup;
	}
	if (strcmp(lnetconfig->nc_protofmly, "inet") == 0) {
		protofmly = AF_INET;
		lsinp = (struct sockaddr_in *)lnetbuf->buf;
		laddr = (void *)&lsinp->sin_addr;
	} else if (strcmp(lnetconfig->nc_protofmly, "inet6") == 0) {
		protofmly = AF_INET6;
		lsin6p = (struct sockaddr_in6 *)lnetbuf->buf;
		laddr = (void *)&lsin6p->sin6_addr;
	} else {
		res = ENOEXEC;
		if (errlist != NULL) {
			append_to_errlist(errlist,
			    env_gettext("Unsupported network "
			    "protocol family", envlist));
		}
		goto cleanup;
	}
	freenetconfigent(lnetconfig);
	lnetconfig = NULL;

	/* Get the local address string */
	if (inet_ntop(protofmly, laddr, laddr_string,
	    sizeof (laddr_string)) == NULL) {
		res = ENOEXEC;
		if (errlist != NULL) {
			append_to_errlist(errlist,
			    env_gettext("Unsupported network "
			    "protocol family", envlist));
		}
		goto cleanup;
	}

	/* Use SCHA calls to get our private IP address */
	scha_res = scha_cluster_open(&scha_handle);
	if (scha_res != SCHA_ERR_NOERR) {
		res = ENOEXEC;
		if (errlist != NULL) {
			append_to_errlist(errlist,
			    env_gettext("Unable to determine if this node "
			    "is in the cluster", envlist));
		}
		goto cleanup;
	}

	/* Make sure we are in the cluser */
	scha_res = scha_cluster_get(scha_handle, SCHA_NODESTATE_LOCAL,
	    &scha_node_state);
	if (scha_res != SCHA_ERR_NOERR) {
		res = ENOEXEC;
		if (errlist != NULL) {
			append_to_errlist(errlist,
			    env_gettext("Unable to determine if the server "
			    "node is in the cluster", envlist));
		}
		goto cleanup;
	}
	if (scha_node_state != SCHA_NODE_UP) {
		res = ENOEXEC;
		if (errlist != NULL) {
			append_to_errlist(errlist,
			    env_gettext("The server node is not an active "
			    "cluster member", envlist));
		}
		goto cleanup;
	}

	/* Get our private hostname */
	scha_res = scha_cluster_get(scha_handle,
	    SCHA_PRIVATELINK_HOSTNAME_LOCAL, &private_hostname);
	if (scha_res != SCHA_ERR_NOERR) {
		res = ENOEXEC;
		if (errlist != NULL) {
			append_to_errlist(errlist,
			    env_gettext("Unable to determine the "
			    "private hostname of the server", envlist));
		}
		goto cleanup;
	}

	/* Get the IP address of the private hostname */
	hp = getipnodebyname(private_hostname, protofmly, 0, &res);
	if ((hp == NULL) || (*hp->h_addr_list == NULL)) {
		res = ENOEXEC;
		if (errlist != NULL) {
			append_to_errlist(errlist,
			    env_gettext("Unable to determine the "
			    "private IP address of the server", envlist));
		}
		goto cleanup;
	} else {
		res = 0;
	}
	bzero((caddr_t)&in6, sizeof (in6));
	bcopy(*hp->h_addr_list, (caddr_t)&in6, hp->h_length);
	if (inet_ntop(protofmly, (void *)&in6,
	    paddr_string, sizeof (paddr_string)) == NULL) {
		res = ENOEXEC;
		if (errlist != NULL) {
			append_to_errlist(errlist,
			    env_gettext("Unsupported network "
			    "protocol family", envlist));
		}
		goto cleanup;
	}

	/* Make sure that the two addresses are the same. */
	if (strcmp(laddr_string, paddr_string) != 0) {
		res = ENOEXEC;
		if (errlist != NULL) {
			append_to_errlist(errlist,
			    env_gettext("This command must use the "
			    "private network", envlist));
		}

		/*
		 * SCMSGS
		 * @explanation
		 * The rpc.scrcmd daemon received an illegal request to
		 * over the public network to execute a command.
		 * Command requests of this type are expected over
		 * the private network only.
		 * @user_action
		 * Report this problem to you Sun service representative.
		 * It could be that an internal error in the Sun Cluster
		 * code has neglected to send the command request over
		 * the correct network.
		 */
		(void) syslog(LOG_ERR, "Command request \"%s\" came in on "
		    "the public network (%s), rather than the private (%s)\n",
		    name, laddr_string, paddr_string);
		goto cleanup;
	}

cleanup:
	/* Free netconfig */
	if (lnetconfig != NULL)
		freenetconfigent(lnetconfig);

	/* Close the SCHA handle */
	if (scha_handle != (scha_cluster_t)0)
		(void) scha_cluster_close(scha_handle);

	/* Free the hostent */
	if (hp != NULL)
		freehostent(hp);

	return (res);
}

/*
 * append_to_errlist
 *
 * Append the given error message, "errmsg", to the "errlist".
 * No new memory is allocated for the given "errmsg".   And,
 * it will eventully be freed when the "errlist" is freed.
 */
static void
append_to_errlist(sc_errlist_t *errlist, char *errmsg)
{
	uint_t i;
	char **newlist;

	i = errlist->sc_namelist_t_len;
	newlist = (char **)realloc(errlist->sc_namelist_t_val,
	    ((i + 1) * sizeof (char *)));
	if (newlist == NULL)
		return;

	newlist[i] = errmsg;
	++errlist->sc_namelist_t_len;
	errlist->sc_namelist_t_val = newlist;
}

/*
 * env_gettext
 *
 * Attempt to return a localized target string based on "msgid" and
 * using the environment variables specified in the "envlist".
 *
 * The caller must free the returned message.
 */
static char *
env_gettext(char *msgid, sc_envlist_t *envlist)
{
	char *message;
	char *locale;
	char *saved_locale = NULL;
	static mutex_t gettext_lock = DEFAULTMUTEX;

	/* Lock out other threads until setlocale() can be restored */
	(void) mutex_lock(&gettext_lock);

	/* Try LC_ALL first */
	locale = getenv_from_envlist("LC_ALL", envlist);

	/* Then, LC_MESSAGES */
	if (locale == NULL) {
		locale = getenv_from_envlist("LC_MESSAGES", envlist);
	}

	/* Then, LANG */
	if (locale == NULL) {
		locale = getenv_from_envlist("LANG", envlist);
	}

	/* If there is a "locale" setting, set LC_MESSAGES to it */
	if (locale != NULL) {
		saved_locale = setlocale(LC_MESSAGES, NULL);
		(void) setlocale(LC_MESSAGES, locale);
	}

	/* Get a copy of the localized message */
	message = gettext(msgid);
	if (message) {
		message = strdup(message);
	}

	/* Restore the saved LC_MESSAGES locale */
	if (saved_locale != NULL) {
		(void) setlocale(LC_MESSAGES, locale);
	}

	/* Release the lock */
	(void) mutex_unlock(&gettext_lock);

	/* Return the message */
	return (message);
}

/*
 * dup_envlist
 *
 * Duplicate the given "envlist".
 */
static sc_envlist_t *
dup_envlist(sc_envlist_t *envlist)
{
	uint_t i;
	sc_envlist_t *newlist;

	/* Make sure there is something to dup */
	if (envlist == NULL)
		return (NULL);

	/* Allocate the new list */
	newlist = (sc_envlist_t *)calloc(1, sizeof (sc_envlist_t));
	if (newlist == NULL)
		return (NULL);

	/* Allocate the array */
	newlist->sc_envlist_t_val = (sc_env_t *)calloc(
	    envlist->sc_envlist_t_len, sizeof (char *));
	if (newlist->sc_envlist_t_val == NULL) {
		free_envlist(newlist);
		return (NULL);
	}
	newlist->sc_envlist_t_len = envlist->sc_envlist_t_len;

	/* Copy the strings */
	for (i = 0;  i < envlist->sc_envlist_t_len;  ++i) {
		if (envlist->sc_envlist_t_val[i] != NULL) {
			newlist->sc_envlist_t_val[i] = strdup(
			    envlist->sc_envlist_t_val[i]);
			if (newlist->sc_envlist_t_val[i] == NULL) {
				free_envlist(newlist);
				return (NULL);
			}
		}
	}

	/* Done */
	return (newlist);
}

/*
 * free_envlist
 *
 * Free all memory associated with the given "envlist".
 */
static void
free_envlist(sc_envlist_t *envlist)
{
	uint_t i;

	if (envlist == NULL)
		return;

	if (envlist->sc_envlist_t_val != NULL) {
		for (i = 0;  i < envlist->sc_envlist_t_len;  ++i) {
			if (envlist->sc_envlist_t_val[i] != NULL) {
				free(envlist->sc_envlist_t_val[i]);
			}
		}
		free(envlist->sc_envlist_t_val);
	}

	free(envlist);
}

/*
 * getenv_from_envlist
 *
 * Get an environment variable valu from the "envlist".
 */
static char *
getenv_from_envlist(char *name, sc_envlist_t *envlist)
{
	uint_t i;
	char *s;
	char *last;

	/* Make sure that there is something to get */
	if (envlist == NULL ||
	    envlist->sc_envlist_t_len == 0 ||
	    envlist->sc_envlist_t_val == (char **)0) {
		return (NULL);
	}

	/* Search for the variable name;  return the value, if found */
	for (i = 0;  i < envlist->sc_envlist_t_len;  ++i) {
		if (envlist->sc_envlist_t_val[i] != NULL) {
			s = strtok_r(envlist->sc_envlist_t_val[i], "=", &last);
			if (s != NULL && strcmp(name, s) == 0) {
				s = strtok_r(NULL, "=", &last);
				if (s != NULL) {
					return (s);
				}
			}
		}
	}

	return (NULL);
}

/*
 * setenv_from_envlist
 *
 * Set the environment from the given "envlist".
 */
static void
setenv_from_envlist(sc_envlist_t *envlist)
{
	uint_t i;

	/* Make sure that there is something to set */
	if (envlist == NULL ||
	    envlist->sc_envlist_t_len == 0 ||
	    envlist->sc_envlist_t_val == (char **)0) {
		return;
	}

	/* Set the environment */
	for (i = 0;  i < envlist->sc_envlist_t_len;  ++i) {
		if (envlist->sc_envlist_t_val[i] != NULL) {
			(void) putenv(envlist->sc_envlist_t_val[i]);
		}
	}
}

/*
 * copy_buffer_to_file
 *
 * Copies the contents of buffer into a temporary file.  The name
 * of the temp file is assigned to filename.
 */
static void
copy_buffer_to_file(char **filename, char *buffer)
{
	FILE *fp;
	*filename = tmpnam(NULL);

	fp = fopen(*filename, "w");
	if (fp == NULL)
		return;

	(void) fwrite(buffer, 1, strlen(buffer), fp);

	(void) fclose(fp);
}

/*
 * scrcmd_strerr
 *
 * Map the given "errno" to a string and return that string.
 *
 * The caller must free the returned message.
 */
static char *
scrcmd_strerr(int err, sc_envlist_t *envlist)
{
	char *errmsg;

	switch (err) {
	case 0:
		errmsg = env_gettext(SCRCMD_MSG_NOERR, envlist);
		break;

	case EINVAL:
		errmsg = env_gettext(SCRCMD_MSG_EINVAL, envlist);
		break;

	case ENOMEM:
		errmsg = env_gettext(SCRCMD_MSG_ENOMEM, envlist);
		break;

	case EEXIST:
		errmsg = env_gettext(SCRCMD_MSG_EEXIST, envlist);
		break;

	case EPROTONOSUPPORT:
		errmsg = env_gettext(SCRCMD_MSG_EPROTONOSUPPORT, envlist);
		break;

	case ENOTACTIVE:
		errmsg = env_gettext(SCRCMD_MSG_ENOTACTIVE, envlist);
		break;

	case ETIMEDOUT:
		errmsg = env_gettext(SCRCMD_MSG_ETIMEDOUT, envlist);
		break;

	case ECONNREFUSED:
		errmsg = env_gettext(SCRCMD_MSG_ECONNREFUSED, envlist);
		break;

	case ENOEXEC:
		errmsg = env_gettext(SCRCMD_MSG_ENOEXEC, envlist);
		break;

	case EIO:
		errmsg = env_gettext(SCRCMD_MSG_EIO, envlist);
		break;

	case ECONNABORTED:
		errmsg = env_gettext(SCRCMD_MSG_ECONNABORTED, envlist);
		break;

	default:
		errmsg = env_gettext("Error", envlist);
		break;
	}

	return (errmsg);
}
