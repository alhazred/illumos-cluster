/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

/* Copyright (c) 1984, 1986, 1987, 1988, 1989 AT&T */
/* All Rights Reserved */

#pragma ident	"@(#)rpc_scrcmd_main.c	1.5	08/05/20 SMI"

/*
 * rpc_scrcmd_main.c
 *
 *	main for rpc.scrcmd(1M)
 */
/*lint -e793 */
#include "rpc_scrcmd.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <strings.h>
#include <stropts.h>
#include <netconfig.h>
#include <syslog.h>
#include <locale.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/mutex.h>

#include <netinet/in.h>

#include <rpc/rpc.h>
#include <rpc/rpcent.h>
#include <rgm/sczones.h>

extern void *closedown(void *arg);

/* Globals */
char *progname;			/* program name */

/* ARGSUSED */
int
main(int argc, char **argv)
{
	struct rpcent rpc_ent;
	struct rpcent *rpcentp;
	char buffer[BUFSIZ];
	int scrcmdprog;

	if ((progname = strrchr(argv[0], '/')) != NULL) {
		++progname;
	} else {
		progname = argv[0];
	}

	/* I18N housekeeping */
	(void) setlocale(LC_ALL, "");
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);
	(void) textdomain(TEXT_DOMAIN);

	if (sc_zonescheck() != 0)
		return (1);

	openlog("rpc_scrcmd", LOG_PID, LOG_DAEMON);

	/* Must be root */
	if (getuid() != 0) {
		syslog(LOG_ERR, "%s:  Not root.", progname);
		exit(1);
	}


	/* Initialize the RPC program number */
	bzero(&rpc_ent, sizeof (rpc_ent));
	if ((rpcentp = getrpcbyname_r(SCRCMDPROGNAME, &rpc_ent,
	    buffer, sizeof (buffer))) == NULL) {
		scrcmdprog = SCRCMDPROG;
	} else {
		scrcmdprog = rpcentp->r_number;
	}

	(void) sigset(SIGPIPE, SIG_IGN);

	/*
	 * If stdin looks like a TLI endpoint, we assume
	 * that we were started by a port monitor. If
	 * t_getstate fails with TBADF, this is not a
	 * TLI endpoint.
	 */
	if (t_getstate(0) != -1 || t_errno != TBADF) {
		char *netid;
		struct netconfig *nconf = NULL;
		SVCXPRT *transp;
		int pmclose;

		if ((netid = getenv("NLSPROVIDER")) == NULL) {
			pmclose = 1;
		} else {
			if ((nconf = getnetconfigent(netid)) == NULL)
				syslog(LOG_ERR,
				    "%s:  Cannot get transport info", progname);
			pmclose = (t_getstate(0) != T_DATAXFER);
		}
		if ((transp = svc_tli_create(0, nconf, NULL, 0, 0)) == NULL) {
			syslog(LOG_ERR, "%s:  Cannot create server handle",
			    progname);
			exit(1);
		}
		if (nconf)
			freenetconfigent(nconf);
		if (!svc_reg(transp, (uint_t)scrcmdprog, SCRCMDVERS,
		    scrcmdprog_1, 0)) {
			syslog(LOG_ERR, "%s:  Unable to register.", progname);
			exit(1);
		}
		if (pmclose) {
			if (thr_create(NULL, 0, closedown, NULL,
			    0, NULL) != 0) {
				syslog(LOG_ERR,
				    "%s:  Cannot create closedown thread",
				    progname);
				exit(1);
			}
		}

	} else if (!svc_create(scrcmdprog_1, (uint_t)scrcmdprog,
	    SCRCMDVERS, "circuit_v")) {
		syslog(LOG_ERR,
		    "%s:  Create failed for netpath \"circuit_v\".", progname);
		exit(1);
	}

	svc_run();

	syslog(LOG_ERR, "%s:  svc_run returned", progname);
	return (1);
}
