#! /bin/ksh -p
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# ident "@(#)create_release.ksh	1.7	09/01/09 SMI"
#
# Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

#
# create_release -r <release> -v <version> -s <solaris> -m <mach> -n <productname>
# -u <urn> -p <parenturn> -k <mtkgvers> [-c]
#
#	-r <release>	replace <RELEASE> tokens with <release> string
#	-v <version>	replace <VERSION> tokens with <version> string
#	-s <solaris>	replace <SOLARIS> tokens with <solaris> string
#	-m <mach>	replace <MACH>    tokens with <mach>    string
#	-n <productname>	replace <PRODUCTNAME> tokens with <productname> string
#	-u <urn>	replace <URN>    tokens with <urn>    string
#	-p <parenturn>	replace <PARENTURN> tokens with <parenturn> string
#	-k <mtkgvers>	replace <MTKGVERS> tokens with <mtkgvers> string
#	-c		center each line of output
#
#        -u, -p, and -k values are used for service tag information.
#        Any changes to these values will require corresponding
#        modifications to the swoRDFish database.
#
#    Standard input is read and copied to standard output.  As input is
#	copied to stdout, recognized tokens are replaced with command line
#	argument strings.   If -c is given, each line is centered as it
#	is copied to stdout.
#	

PATH=${PATH}:/usr/bin

typeset release=
typeset version=
typeset solaris=
typeset mach=
typeset productname=
typeset urn=
typeset parenturn=
typeset mtkgvers=

typeset -r year=$(date +%Y)
typeset align=cat			# default alignment is to do nothing

#
# Parse command line
#
parse()
{
	typeset current=			# current option type

	while [[ $# -gt 0 ]]
	do
		case $1 in
		-r)	current=r ;;		# capture <release> string
		-v)	current=v ;;		# capture <version> string
		-s)	current=s ;;		# capture <solaris> string
		-m)	current=m ;;		# capture <mach>    string
		-n)	current=n ;;		# capture <productname> string
		-u)	current=u ;;		# capture <urn>    string
		-p)	current=p ;;		# capture <parenturn>    string
		-k)	current=k ;;		# capture <mtkgvers>    string

		-c)	current=c		# center each line of output
			align=center_text
			;;

		*)				# get option arguments
			case "${current}" in
			r)			# option args to -r
				release="${release} $1"
				;;

			v)			# option args to -v
				version="${version} $1"
				;;

			s)			# option args to -s
				solaris="${solaris} $1"
				;;

			m)			# option args to -m
				mach="${mach} $1"
				;;

			n)			# option args to -n
				productname="${productname} $1"
				;;

			u)			# option args to -u
				urn="${urn} $1"
				;;

			p)			# option args to -p
				parenturn="${parenturn} $1"
				;;

			k)			# option args to -k
				mtkgvers="${mtkgvers} $1"
				;;

			esac
		esac
		shift
	done

	# Remove extra spaces
	release="$(echo ${release})"
	version="$(echo ${version})"
	solaris="$(echo ${solaris})"
	mach="$(echo ${mach})"
	productname="$(echo ${productname})"
	urn="$(echo ${urn})"
	parenturn="$(echo ${parenturn})"
	mtkgvers="$(echo ${mtkgvers})"

	return 0
}

#
# Remove extra spaces
#
remove_extra_spaces()
{
	typeset line			# contents of a line

	while read line
	do
		echo ${line}
	done
}

#
# Center text
#
center_text()
{
	typeset -r standard_len=80	# standard line is 80 characters
	typeset line			# contents of a line
	integer line_len		# line length
	integer add_spaces		# number of left spaces to add

	# center each 80 character line
	while read line
	do
		let add_spaces=0
		line_len=${#line}
		if [[ ${line_len} -le ${standard_len} ]];  then
			add_spaces=$(((standard_len - line_len) / 2))
			while [[ ${add_spaces} -gt 0 ]]
			do
				echo ' \c'
				((add_spaces -= 1))
			done
		fi
		echo "${line}"
	done
}

#
# Parse
#
parse $*

#
# Substitue variables
#
sed \
	-e 's#<VERSION>#'"${version}"'#g' \
	-e 's#<RELEASE>#'"${release}"'#g' \
	-e 's#<SOLARIS>#'"${solaris}"'#g' \
	-e 's#<MACH>#'"${mach}"'#g' \
	-e 's#<PRODUCTNAME>#'"${productname}"'#g' \
	-e 's#<URN>#'"${urn}"'#g' \
	-e 's#<PARENTURN>#'"${parenturn}"'#g' \
	-e 's#<MTKGVERS>#'"${mtkgvers}"'#g' \
	-e 's#<YEAR>#'${year}'#g' \
| remove_extra_spaces | ${align}

# Done
exit
