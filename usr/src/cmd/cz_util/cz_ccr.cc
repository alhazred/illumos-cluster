//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)cz_ccr.cc	1.5	08/07/17 SMI"

#include <h/ccr.h>
#include <orb/infrastructure/orb_conf.h>
#include <orb/infrastructure/orb.h>
#include <sys/os.h>
#include <nslib/ns.h>
#include <orb/object/adapter.h>
#include <libintl.h>
#include <sys/clconf_int.h>
#include <sys/clconf.h>
#include <sys/sol_version.h>
#include <sys/cladm_int.h>

//
// Print the usage.
//
static void
print_usage()
{
	(void) printf("Usage: cz_ccr [-z cluster ] [-t table ] "
	    "[ create | destroy ]\n");
	(void) printf("Usage: cz_ccr [-z cluster ] [-t table ] "
	    "[ -a | -r | -m | -l | -R ] [key,data]\n");
}

class cz_ccr_op {

public:
	cz_ccr_op();
	void create_cluster(char *);
	void remove_cluster(char *);
	void create_table(char *, char *);
	void remove_table(char *, char *);
	void add_table_entry(char *, char *, char *, char *);
	void remove_table_entry(char *, char *, char *);
	void modify_table_entry(char *, char *, char *, char *);
	void read_table(char *, char *);
	void lookup_table(char *, char *);
private:
	ccr::directory_var		ccr_dir_v;
	CORBA::Environment		e;
	CORBA::Object_var		obj_v;
	naming::naming_context_var	ctx_v;
};

//
// Constructor
//
cz_ccr_op::cz_ccr_op()
{
	ctx_v = ns::local_nameserver();
	obj_v = ctx_v->resolve("ccr_directory", e);
	if (e.exception()) {
		os::printf("ccr_directory not found in name server\n");
		ASSERT(0);
	}
	ccr_dir_v = ccr::directory::_narrow(obj_v);
	ASSERT(!CORBA::is_nil(ccr_dir_v));
}

void
cz_ccr_op::create_cluster(char *cluster)
{
	ASSERT(cluster != NULL);
	ccr_dir_v->zc_create(cluster, e);
	if (e.exception()) {
		os::printf("Error creating cluster %s\n", cluster);
		e.exception()->print_exception("Create cluster");
		e.clear();
	}
}

void
cz_ccr_op::remove_cluster(char *cluster)
{
	ASSERT(cluster != NULL);
	ccr_dir_v->zc_remove(cluster, e);
	if (e.exception()) {
		os::printf("Error removing cluster %s\n", cluster);
		e.exception()->print_exception("Remove cluster");
		e.clear();
	}
}

void
cz_ccr_op::create_table(char *cluster, char *table)
{
	if (cluster != NULL) {
		ccr_dir_v->create_table_zc(cluster, table, e);
	} else {
		ccr_dir_v->create_table(table, e);
	}
	if (e.exception()) {
		if (cluster != NULL) {
			os::printf("Error creating table %s in cluster\n",
			    table, cluster);
		} else {
			os::printf("Error creating table %s\n", table);
		}
		e.exception()->print_exception("create_table");
		e.clear();
	}
}

void
cz_ccr_op::remove_table(char *cluster, char *table)
{
	if (cluster != NULL) {
		ccr_dir_v->remove_table_zc(cluster, table, e);
	} else {
		ccr_dir_v->remove_table(table, e);
	}
	if (e.exception()) {
		if (cluster != NULL) {
			os::printf("Error removing table %s in cluster %s\n",
			    table, cluster);
		} else {
			os::printf("Error removing table %s\n", table);
		}
		e.exception()->print_exception("remove_table");
		e.clear();
	}
}

void
cz_ccr_op::add_table_entry(char *cluster, char *table, char *key, char *data)
{
	ccr::updatable_table_ptr trans_p;

	if (cluster != NULL) {
		trans_p = ccr_dir_v->begin_transaction_zc(cluster,  table, e);
	} else {
		trans_p = ccr_dir_v->begin_transaction(table, e);
	}
	if (e.exception()) {
		os::printf("Begin transaction failed\n");
		e.exception()->print_exception("Add table entry:");
		e.clear();
		return;
	}

	trans_p->add_element(key, data, e);
	if (e.exception()) {
		os::printf("Add element failed\n");
		e.exception()->print_exception("Add entry:");
		e.clear();
		trans_p->abort_transaction(e);
		if (e.exception()) {
			os::printf("Abort transaction failed\n");
			e.exception()->print_exception("Abort entry");
			e.clear();
		}
		return;
	}
	trans_p->commit_transaction(e);
	if (e.exception()) {
		os::printf("Commit transaction failed\n");
		e.exception()->print_exception("Commit changes");
	}
	CORBA::release(trans_p);
}

void
cz_ccr_op::remove_table_entry(char *cluster, char *table, char *key)
{
	ccr::updatable_table_ptr trans_p;

	if (cluster != NULL) {
		trans_p = ccr_dir_v->begin_transaction_zc(cluster,  table, e);
	} else {
		trans_p = ccr_dir_v->begin_transaction(table, e);
	}
	if (e.exception()) {
		os::printf("Begin transaction failed\n");
		e.exception()->print_exception("Remove table entry:");
		e.clear();
		return;
	}

	trans_p->remove_element(key, e);
	if (e.exception()) {
		os::printf("Remove element failed\n");
		e.exception()->print_exception("Remove entry:");
		e.clear();
		trans_p->abort_transaction(e);
		if (e.exception()) {
			os::printf("Abort transaction failed\n");
			e.exception()->print_exception("Abort entry");
			e.clear();
		}
		return;
	}
	trans_p->commit_transaction(e);
	if (e.exception()) {
		os::printf("Commit transaction failed\n");
		e.exception()->print_exception("Commit changes");
	}
}

void
cz_ccr_op::modify_table_entry(char *cluster, char *table, char *key,
    char *data)
{
	ccr::updatable_table_ptr trans_p;

	if (cluster != NULL) {
		trans_p = ccr_dir_v->begin_transaction_zc(cluster,  table, e);
	} else {
		trans_p = ccr_dir_v->begin_transaction(table, e);
	}
	if (e.exception()) {
		os::printf("Begin transaction failed\n");
		e.exception()->print_exception("Modify table entry:");
		e.clear();
		return;
	}

	trans_p->update_element(key, data, e);
	if (e.exception()) {
		os::printf("Update element failed\n");
		e.exception()->print_exception("Update entry:");
		e.clear();
		trans_p->abort_transaction(e);
		if (e.exception()) {
			os::printf("Abort transaction failed\n");
			e.exception()->print_exception("Abort entry");
			e.clear();
		}
		return;
	}
	trans_p->commit_transaction(e);
	if (e.exception()) {
		os::printf("commit transaction failed\n");
		e.exception()->print_exception("Commit changes");
	}
}

void
cz_ccr_op::lookup_table(char *cluster, char *table)
{
	ccr::readonly_table_ptr		table_p;

	if (cluster != NULL) {
		table_p = ccr_dir_v->lookup_zc(cluster, table, e);
		if (e.exception()) {
			os::printf("lookup operation failed\n");
			e.exception()->print_exception("Lookup");
			e.clear();
		} else {
			os::printf("Lookup of table %s in cluster %s "
			    "succeeded\n", cluster, table);
		}
	} else {
		table_p = ccr_dir_v->lookup(table, e);
		if (e.exception()) {
			os::printf("lookup operation failed\n");
			e.exception()->print_exception("Lookup");
			e.clear();
		} else {
			os::printf("Lookup of table %s succeeded\n", table);
		}
	}
}

void
cz_ccr_op::read_table(char *cluster, char *table)
{
	ccr::readonly_table_ptr		table_p;
	ccr::table_element		*elementp = NULL;
	CORBA::Exception		*exp = NULL;

	if (cluster != NULL) {
		table_p = ccr_dir_v->lookup_zc(cluster, table, e);
		if (e.exception()) {
			os::printf("lookup operation failed\n");
			e.exception()->print_exception("Lookup");
			e.clear();
			return;
		}
	} else {
		table_p = ccr_dir_v->lookup(table, e);
		if (e.exception()) {
			os::printf("lookup operation failed\n");
			e.exception()->print_exception("Lookup");
			e.clear();
			return;
		}
	}
	table_p->atfirst(e);
	if (e.exception()) {
		os::printf("atfirst operation failed\n");
		e.exception()->print_exception("Atfirst");
		e.clear();
		return;
	}
	while (true) {
		table_p->next_element(elementp, e);
		if ((exp = e.exception()) != NULL) {
			if (ccr::NoMoreElements::_exnarrow(exp)) {
				// End of table.
				break;
			} else {
				os::printf("next_element operation failed\n");
				e.exception()->print_exception("next_element");
				e.clear();
				return;
			}
		}
		os::printf("%s\t%s\n", (char *)elementp->key,
		    (char *)elementp->data);
	}
}

//
// Main routine.
// Return value:
//	0 On success.
//	1 On Failure.
//
// lint -e818
int
main(int argc, char *const argv[])
{
	//
	// Initialize ORB.
	//
	if (ORB::initialize() != 0) {
		(void) printf(gettext("Could not initialize the ORB. "
		    "Exiting."));
		exit(1);
	}

	cz_ccr_op			vc_op;
	char				*cluster = NULL;
	char				*table = NULL;
	int				add_entry = 0;
	int				remove_entry = 0;
	int				modify_entry = 0;
	int				lookup_entry = 0;
	int 				read_entry = 0;
	int				arg;

	//
	// Get command line options
	//
	optind = 1;
	while ((arg = getopt(argc, argv, "z:t:a:r:m:lR?")) != EOF) {
		// switch on the option.
		switch (arg) {
		case 'z':
			if ((optarg == NULL) || (*optarg == NULL)) {
				print_usage();
				return (1);
			}
			cluster = os::strdup(optarg);
			break;
		case 't':
			if ((optarg == NULL) || (*optarg == NULL)) {
				print_usage();
				return (1);
			}
			table = os::strdup(optarg);
			break;
		case 'a':
			//
			// Add a new entry to the specified table.
			//
			add_entry = 1;
			if ((optarg == NULL) || (*optarg == NULL)) {
				print_usage();
				return (1);
			}
			break;
		case 'r':
			//
			// Remove an new entry in the specified table.
			//
			remove_entry = 1;
			if ((optarg == NULL) || (*optarg == NULL)) {
				print_usage();
				return (1);
			}
			break;
		case 'R':
			//
			// Read the contents of the CCR table.
			//
			read_entry = 1;
			break;
		case 'l':
			//
			// Look up the CCR table and check if it exists.
			//
			lookup_entry = 1;
			break;
		case 'm':
			//
			// Modify an entry in the specified table.
			//
			modify_entry = 1;
			if ((optarg == NULL) || (*optarg == NULL)) {
				print_usage();
				return (1);
			}
			break;
		case '?':
			print_usage();
			return (1);
		default:
			print_usage();
			return (1);
		} // switch(arg)
	}

	if (table == NULL) {
		// Cluster operation.
		if (add_entry || remove_entry || modify_entry ||
		    lookup_entry || read_entry) {
			print_usage();
			return (1);
		}
		optarg = argv[argc - 1];
		if (optarg == NULL) {
			os::printf("Error parsing command\n");
			print_usage();
			return (1);
		}
		// The next argument must be either "create" or "destroy"
		if (os::strcmp(optarg, "create") == 0) {
			os::printf("Create cluster %s\n", cluster);
			vc_op.create_cluster(cluster);
		} else if (os::strcmp(optarg, "destroy") == 0) {
			os::printf("Remove cluster %s\n", cluster);
			vc_op.remove_cluster(cluster);
		} else {
			os::printf("Error parsing command\n");
			print_usage();
			return (1);
		}
	} else {
		// Table operation
		int entry = add_entry + remove_entry + modify_entry +
		    lookup_entry + read_entry;
		if (entry == 0) {
			// This could be a request to create or remove table
			optarg = argv[argc - 1];
			if (optarg == NULL) {
				os::printf("Error parsing command\n");
				print_usage();
				return (1);
			}
			//
			// The next argument must be either "create" or
			// "destroy"
			//
			if (os::strcmp(optarg, "create") == 0) {
				if (cluster != NULL) {
					os::printf("Create table %s in "
					    "cluster %s\n", table, cluster);
				} else {
					os::printf("Create table %s\n", table);
				}
				vc_op.create_table(cluster, table);
			} else if (os::strcmp(optarg, "destroy") == 0) {
				if (cluster != NULL) {
					os::printf("Remove table %s in "
					    "cluster %s\n", table, cluster);
				} else {
					os::printf("Remove table %s\n", table);
				}
				vc_op.remove_table(cluster, table);
			} else {
				os::printf("Error parsing command\n");
				print_usage();
				return (1);
			}
		} else if (entry > 1) {
			os::printf("Error: More than one sub-command "
			    "specified\n");
			print_usage();
			return (1);
		}
		if (add_entry) {
			ASSERT(optarg != NULL);
			char *key = (char *)strtok(optarg, ",");
			char *data = strtok(NULL, ",");
			if (data) {
				if (cluster != NULL) {
					os::printf("Add <%s> <%s> entry to "
					    "table %s in cluster %s\n", key,
					    data, table, cluster);
				} else {
					os::printf("Add <%s> <%s> entry to "
					    "table %s\n", key, data, table);
				}
				vc_op.add_table_entry(cluster, table, key,
				    data);
			} else {
				if (cluster != NULL) {
					os::printf("Add <%s> entry to "
					    "table %s in cluster %s\n", key,
					    table, cluster);
				} else {
					os::printf("Add <%s> entry to "
					    "table %s\n", key, table);
				}
				vc_op.add_table_entry(cluster, table, key,
				    "");
			}
		} else if (remove_entry) {
			ASSERT(optarg != NULL);
			char *key = (char *)strtok(optarg, ",");
			char *data = strtok(NULL, ",");
			if (cluster != NULL) {
				os::printf("Remove <%s> key entry in table %s "
				    "in cluster %s\n", key, table, cluster);
			} else {
				os::printf("Remove <%s> key entry in table "
				    "%s\n", key, table);
			}
			vc_op.remove_table_entry(cluster, table, key);
		} else if (modify_entry) {
			ASSERT(optarg != NULL);
			char *key = (char *)strtok(optarg, ",");
			char *data = strtok(NULL, ",");
			if (data == NULL) {
				os::printf("Need data in <key> <data> pair "
				    " to modify element\n");
				print_usage();
				return (1);
			} else {
				if (cluster != NULL) {
					os::printf("Modify key <%s> with data "
					    "<%s> in table %s of cluster %s\n",
					    key, data, table, cluster);
				} else {
					os::printf("Modify key <%s> with data "
					    "<%s> in table %s\n", key, data,
					    table);
				}
			}
			vc_op.modify_table_entry(cluster, table, key, data);
		} else if (lookup_entry) {
			if (cluster != NULL) {
				os::printf("Lookup table %s in "
				    "cluster %s\n", table, cluster);
			} else {
				os::printf("Lookup table %s\n", table);
			}
			vc_op.lookup_table(cluster, table);
		} else if (read_entry) {
			if (cluster != NULL) {
				os::printf("Read table %s in "
				    "cluster %s\n", table, cluster);
			} else {
				os::printf("Read table %s\n", table);
			}
			vc_op.read_table(cluster, table);
		}
	}
	return (0);
}
