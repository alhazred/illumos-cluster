//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)zc_present.cc	1.3	08/07/25 SMI"

//
// This utility tells whether a zone cluster is present on the
// physical cluster in non cluster mode. This utility will not
// run in cluster mode. This program should run only in the
// global zone and with root privileges.
//
// Return Value:
// 	0:	Zone Cluster is present
// 	1:	Zone Cluster is not present
// 	-1:	Error occured
//

#include <locale.h>
#include <zone.h>
#include <sys/os.h>
#include <ccr_access.h>
#include <libintl.h>
#include <sys/cladm_int.h>

static int is_zc_present(void);

//
// This function will read the cluster_directory file
// and see if Zone Clusters are configured.
//
// Return Value:
// 	0: 	Success - Zone Cluster is pressent
// 	1:	Success - Zone Cluster is not present
// 	-1:	Error	- Some error occured
//
int
is_zc_present()
{
	int				retval = 0;
	int32_t				num_elements = 0;
	ccr_access_readonly_table	read_tab;

	retval = read_tab.initialize("cluster_directory");
	if (retval != 0) {
		(void) printf(
		    gettext("Error: Could not open the cluster_directory\n"));
		return (-1);

	}

	num_elements = read_tab.get_num_elements();
	if (num_elements == -1) {
		(void) printf(
		    gettext("Error: Could not read cluster_directory\n"));
		return (-1);
	}

	read_tab.close();

	if (num_elements == 1) {
		//
		// This means that there are no zone
		// clusters configured. The only entry
		// is for the physical cluster "global"
		//
		return (1);
	} else {
		// Zone cluster is present.
		return (0);
	}
}

// -------------------------- MAIN -------------------------
int
main()
{
	int			bootflags = 0;

	//
	// I18N housekeeping.
	//
	(void) setlocale(LC_ALL, "");
	(void) textdomain(TEXT_DOMAIN);
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	// User should run with root privileges
	if (geteuid() != 0) {
		(void) printf(
		    gettext("Error: Not running as root user\n"));
		return (-1);
	}

#if (SOL_VERSION >= __s10)
	//
	// Check if this is being run in the global zone
	// If not, then return error
	//
	zoneid_t		zid = getzoneid();
	if (zid != GLOBAL_ZONEID) {
		(void) printf(
		    gettext("Error: Not running in global zone\n"));
		return (-1);
	}
#endif

	if ((cladm(CL_INITIALIZE, CL_GET_BOOTFLAG, &bootflags) != 0) ||
	    !(bootflags & CLUSTER_BOOTED)) {
		// Node is not a cluster member
		return (is_zc_present());
	}

	(void) printf(gettext("Error: Node is in cluster mode\n"));
	return (-1);
}
