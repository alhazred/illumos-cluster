//
//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)vclinfo.cc	1.4	08/07/17 SMI"

//
// This utility checks if the zone in which it is running is a virtual
// cluster node.
//
#include <libintl.h>
#include <sys/clconf_int.h>
#include <sys/clconf.h>
#include <sys/cladm_int.h>
#include <sys/sol_version.h>
#include <zone.h>
#include <sys/vc_int.h>

//
// Main routine.
// Return value:
//	0 if the zone is a virtual cluster node
//	1 Otherwise.
//
int
main()
{
	char		zone_name[ZONENAME_MAX];
	zoneid_t	zid;
	uint32_t	clid;
	int		retval;
#if (SOL_VERSION >= __s10)
	//
	// Initialize ORB.
	//
	if (clconf_lib_init() != 0) {
		(void) fprintf(stderr, gettext("Could not initialize the ORB. "
		    "Exiting."));
		exit(1);
	}
	zid = getzoneid();
	if (zid == 0) {
		// global zone
		return (1);
	}
	if (getzonenamebyid(zid, zone_name, sizeof (zone_name)) < 0) {
		(void) fprintf(stderr, gettext("Failed to get zone name. "
		    "Exiting."));
		exit(1);
	}
	if ((retval = clconf_get_cluster_id(zone_name, &clid)) != 0) {
		(void) fprintf(stderr, gettext("Failed to get cluster id, "
		    "retval = %d. Exiting."), retval);
		exit(1);
	}
	if (clid >= MIN_CLUSTER_ID) {
		// virtual cluster
		return (0);
	} else {
		return (1);
	}
#else
	return (1);
#endif // SOL_VERSION >= __S10
}
