/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident   "@(#)cl_eventd.cc 1.16     08/05/20 SMI"

#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <limits.h>
#include <fcntl.h>
#include <thread.h>
#include <synch.h>
#include <sys/stat.h>
#include <sys/types.h>

#include <sys/cl_events_int.h>
#include <libsysevent.h>
#include <sys/sysevent_impl.h>
#include <libnvpair.h>

#include <sys/cladm.h>
#include <orb/infrastructure/orb.h>
#include <orb/invo/common.h>
#include <h/sol.h>
#include <h/naming.h>
#include <nslib/ns.h>

#include <sys/sc_syslog_msg.h>
#include <rgm/sczones.h>

#include "cl_event_priv.h"

// The event queue
cl_event_t	*event_q_first = NULL;	// Head
cl_event_t	*event_q_last = NULL;	// Tail

client_node_t	client_list[MAXNODES+1];	// Array of clients
cond_t		dispatch_cv;	// inform dispatch threads of event

mutex_t		global_mutex;		// Protects everything

nodeset		live_nodes;		// Nodes we have valid references to
int		num_ev_q = 0;		// Current Number of events queued
int		max_ev_q = 0;		// Max Number of events queued

int		RETRY_COUNT = 3;	// How many attempts to send message
int		RETRY_INTERVAL = 2;	// Sleep time between attempts

// verbosity level. Should be > 1 for any
// informational message to appear on
// a per-event basis. Level 1 only logs
// periodic aggregate information.
int		verbose = 0;
int		num_log_messages = 100;	// How often to log stats

//
// init_queues()
// Initialize queues and clients, create the mutexs and
// the condition variable.
// Called once from main in the begenning
//
static void
init_queues()
{
	event_q_first = NULL;
	event_q_last = NULL;
	(void) mutex_init(&global_mutex, USYNC_THREAD, NULL);

	(void) cond_init(&dispatch_cv, USYNC_THREAD, NULL);

	live_nodes.set(uint64_t(0));
	for (nodeid_t nid = 0; nid <= MAXNODES; nid++) {
		client_node_t	*c = &(client_list[nid]);

		c->c_nid = nid;
		c->c_tid = thread_t(-1);
		c->c_comm_p = clevent::clevent_comm::_nil();
	}
}

//
// queue_event()
// Queue a sysevent into the event queue
// First converts the sysevent into a cl_event_t, grabs
// the global_mutex then appends the event at the end of
// the queue (using the last pointer) and then wakes up any
// waiting threads with a cond_broadcast on the dispatch_cv.
// Returns EAGAIN if it is unable to allocate memory.
//
// Assumes that the global lock is held
//
static int
queue_event(sysevent_t *ev)
{
	sc_syslog_msg_handle_t handle;

	// The event must be packed
	ASSERT(SE_FLAG(ev) == SE_PACKED_BUF);

	cl_event_t	*cev = (cl_event_t *)malloc(sizeof (cl_event_t));
	if (cev == NULL) {
		(void) sc_syslog_msg_initialize(&handle,
			SC_SYSLOG_CLAPI_CLEVENTD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			"Out of memory.");
		sc_syslog_msg_done(&handle);
		return (EAGAIN);
	}
	cev->e_idl_ev = new clevent::idl_event(sysevent_get_size(ev),
		sysevent_get_size(ev));
	if (cev->e_idl_ev == NULL) {
		free(cev);
		(void) sc_syslog_msg_initialize(&handle,
			SC_SYSLOG_CLAPI_CLEVENTD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			"Out of memory.");
		sc_syslog_msg_done(&handle);
		return (EAGAIN);
	}
	cev->e_set.set(uint64_t(0));
	cev->e_next = NULL;
	cev->e_prev = NULL;
	cev->e_idl_ev->length(sysevent_get_size(ev));
	(void) memcpy(cev->e_idl_ev->buf(), ev, sysevent_get_size(ev));

	if (event_q_last == NULL) {
		ASSERT(event_q_first == NULL);
		event_q_last = cev;
		event_q_first = cev;
	} else {
		cev->e_prev = event_q_last;
		event_q_last->e_next = cev;
		event_q_last = cev;
	}
	//
	// Wake up ALL threads because all client threads can
	// start working on delivering this event to remote
	// nodes. That is why we broadcast here, instead of signal
	//
	(void) cond_broadcast(&dispatch_cv);

	return (0);
}

//
// send_to_node()
// Send the specifed event to the specified remote node.
// Two types of failures can happen
// 1. IDL call failures
//	1.1) IDL call fails with COMM_FAILURE or INV_OBJREF, that means
//	either the node is dead or the server on that node is dead. So in
//	this case we release our reference to it, mark the node dead in
//	live_nodes and return. The event is NOT delivered to the node
//	in question.
//
//	1.2) Some other IDL failure. We log the exception and return, the
//	event is not delivered to the node.
//  Update: Per ORB folks, Assert out, this should never happen.
//
// 2. Problems on remote nodes.
//    Are handled on the remote nodes themselves.
//	  Two probles can happen.
//	  2.1 Low memory: Sleep and retry
//	  2.2 Kernel Event queue is full: Sleep and retry
//	  See comments in idl_ev_send for details
//
//	Note: This function drops and reacquires the global lock.
//	Callers beware.
//
//
static void
send_to_node(client_node_t *clnt, clevent::idl_event *idl_ev)
{
	sol::nodeid_t nid = orb_conf::local_nodeid();
	sysevent_t	*sol_ev = (sysevent_t *)idl_ev->buf();
	sc_syslog_msg_handle_t handle;


	if (clnt->c_nid == nid) {
		return;		// Skip local node
	}

	if (CORBA::is_nil(clnt->c_comm_p)) {
		(void) sc_syslog_msg_initialize(&handle,
			SC_SYSLOG_CLAPI_CLEVENTD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The cl_eventd cannot forward the specified event to the
		// specified node because it has no reference to the remote
		// node.
		// @user_action
		// This message is informational only, and does not require
		// user action.
		//
		(void) sc_syslog_msg_log(handle, LOG_INFO, MESSAGE,
		    "No reference to remote node %d, not "
		    "forwarding event %lld", clnt->c_nid,
		    sysevent_get_seq(sol_ev));
		sc_syslog_msg_done(&handle);

		return;		// Skip nodes we don't have reference to
	}

	Environment e;
	e.clear();

	if (verbose > 0) {
		(void) sc_syslog_msg_initialize(&handle,
			SC_SYSLOG_CLAPI_CLEVENTD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The cl_eventd is forwarding an event to the specified node.
		// @user_action
		// This message is informational only, and does not require
		// user action.
		//
		(void) sc_syslog_msg_log(handle, LOG_INFO, MESSAGE,
		    "Sending to node %d.", clnt->c_nid);
		sc_syslog_msg_done(&handle);
	}
	// Make a duplicate reference for making the IDL call
	clevent::clevent_comm_var  dup_ptr =
		clevent::clevent_comm::_duplicate(clnt->c_comm_p);

	// Release lock before making remote call
	(void) mutex_unlock(&global_mutex);

	// make the IDL call
	sol::error_t err = dup_ptr->idl_ev_send(*idl_ev, e);

	// Reacquire lock
	(void) mutex_lock(&global_mutex);

	if (e.exception() != NULL) {
		if ((CORBA::COMM_FAILURE::_exnarrow(e.exception()) != NULL) ||
			(CORBA::INV_OBJREF::_exnarrow(e.exception()) != NULL)) {
			//
			// Remote node died or the server on the remote node
			// died. Remove our reference to this object.
			// We would recreate this reference when the node
			// sends us its reference by idl_send_ref()
			//
			// N.B. race condition between the server on the remote
			// node dieing and restarting quickly vs our IDL call
			// failing due to server death. After restarting, the
			// server could have made idl_send_ref() call and
			// updated our reference
			//
			// How do we tell if that has happened? We compare
			// the old reference with the new one. If both are
			// the same, it is an invalid stale reference, if
			// they are different, the remote server has
			// quickly restarted and updated our reference
			// while we had released the global_mutex while
			// making the IDL call.
			//

			if (dup_ptr->_equiv(clnt->c_comm_p)) {
				//
				// Referece is same, remote node has
				// not restarted.
				// Release its reference and remove it
				// from live_nodes.
				//
				CORBA::release(clnt->c_comm_p);
				clnt->c_comm_p = clevent::clevent_comm::_nil();
				live_nodes.remove_node(clnt->c_nid);
			}
		} else {
			//
			// Some other exception. This should not happen at
			// all. Best thing to do is to assert here.
			if (verbose)
				e.exception()->print_exception(
					"IDL Exception Sending event "
					"to remote node.");
			ASSERT(0);
		}
	}

	//
	// Log an error if we were unable to
	// deliver the event
	//
	if (err != 0) {
		(void) sc_syslog_msg_initialize(&handle,
			SC_SYSLOG_CLAPI_CLEVENTD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The cl_eventd was unable to deliver the specified event to
		// the specified node.
		// @user_action
		// Examine other syslog messages occurring at about the same
		// time to see if the problem can be identified. Save a copy
		// of the /var/adm/messages files on all nodes and contact
		// your authorized Sun service provider for assistance in
		// diagnosing and correcting the problem.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Failed to deliver event %lld to remote node %d:%s",
		    sysevent_get_seq(sol_ev), clnt->c_nid, strerror(err));
		sc_syslog_msg_done(&handle);
	} else if (verbose > 0) {
		(void) sc_syslog_msg_initialize(&handle,
			SC_SYSLOG_CLAPI_CLEVENTD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The cl_eventd was able to deliver the specified event to
		// the specified node.
		// @user_action
		// This message is informational only, and does not require
		// user action.
		//
		(void) sc_syslog_msg_log(handle, LOG_DEBUG, MESSAGE,
		    "Successfully delivered event %lld to remote node %d.",
		    sysevent_get_seq(sol_ev), clnt->c_nid);
		sc_syslog_msg_done(&handle);
	}
}

//
// ev_dispatch_thread()
// Waits for events to show up in the global
// event queue then delivers the event to
// a specifc remote node.
// Calls send_to_node() to deliver the event to
// the remote node.
//
static void
ev_dispatch_thread(void *arg)
{
	client_node_t	*myclnt = (client_node_t *)arg;
	nodeid_t	mynodeid = myclnt->c_nid;
	nodeset tmpset;

	(void) mutex_lock(&global_mutex);
	while (1) {
		//
		// Sleep while the event que is empty or we
		// don't have a reference to the remote node yet.
		// Or we are done with all messges on the que
		//
		// If event_q_first is NULL, it means there
		// are no messages on the que and hence we
		// must go back to cond_wait().
		//
		// If the last event on the queue indicates
		// that it has been sent to our client node,
		// it means that we have just traversed the
		// que and have handled all the messages.
		// Hence we must again go back to cond_wait
		// to wait for new events to arrive.
		//
		while ((event_q_first == NULL) ||
			(event_q_last->e_set.contains(mynodeid)) ||
			(CORBA::is_nil(myclnt->c_comm_p))) {

			(void) cond_wait(&dispatch_cv, &global_mutex);
		}

		//
		// Now scan the event queue looking for the first event which
		// has not yet been sent to our client node.
		//
		cl_event_t *cev = event_q_first;
		while (cev) {
			// Check if this event needs to be sent to our client
			if (!cev->e_set.contains(mynodeid)) {
				//
				// Need to send this message out.
				// send_to_node() drops the global_mutex,
				// but that is all right because the event
				// being sent will not be freed as it
				// contains our target node in its nodeset.
				//
				// Further justification for above statement:
				//
				// The event is free()d ONLY when a thread
				// finds that the set of nodes where this
				// event has been forwarded to contains
				// live_nodes. Now, is it possible that somehow,
				// live_nodes may get updated to NOT contain
				// our client node anymore? [That could
				// trigger removal of this event from the queue
				// as live_nodes does not contain our
				// client node anymore.]
				//
				// The answer is that it cannot happen. Nodes
				// are removed from live_nodes only under one
				// situation, When an IDL call to it fails.
				// But THIS thread is the only one making
				// those calls and hence other threads
				// cannot take that code path.
				//
				// XXX Can the reverse happen? That an event
				// on the queue will remain there forever?
				//
				send_to_node(myclnt, cev->e_idl_ev);

				//
				// Add mynodeid to the set of nodes this
				// event has been sent to
				//
				cev->e_set.add_node(mynodeid);
			}	// If event needs to goto my client
			//
			// See if all live nodes are done with this event
			// If so, dequeue and destroy this message
			//
			tmpset.set(live_nodes);
			tmpset.intersect(cev->e_set);
			if (tmpset.equal(live_nodes)) {
				// Dequeue the message
				if (cev->e_prev)
					cev->e_prev->e_next = cev->e_next;
				if (cev->e_next)
					cev->e_next->e_prev = cev->e_prev;

				if (cev == event_q_first)
					event_q_first = cev->e_next;
				if (cev == event_q_last)
					event_q_last = cev->e_prev;
				num_ev_q--;


				cl_event_t *nextev = cev->e_next;
				delete cev->e_idl_ev;
				cev->e_idl_ev = NULL;
				free(cev);

				//
				// Advance to the next event in the queue.
				// N.B. While sending the current event, we
				// might have realized that the remote node
				// is dead, shouldn't we check for that?
				// Why even bother with the remaining events
				// on the queue, we are not going to be
				// able to send them anyway.
				// Ans:
				// If we do that, and go back to sleep on
				// dispatch_cv, what happens to these
				// remaining events? It is possible that we
				// were the last thread to look
				// all these events (other threads were already
				// done with them). As a matter of fact,
				// since our client node just died, we were
				// just hung in the IDL call for 10 seconds
				// (CMM timeout) and it is LIKELY that
				// all the events in the queue are waiting
				// for US!
				// If we don't clean them up, there is no
				// gaurantee how long they will sit in
				// the queue. If another event were to
				// happen and other threads (if there is
				// at least one more active thread), were
				// to scan the queue, it will
				// clean up these events at that time, but
				// we have no idea when is that going to
				// happen, instead we clean up the
				// queue RIGHT now.
				//
				// XXX Can this semantics be improved to
				// allow for remote node to come back? e.g.
				// if we have a timer based
				// garbage cleaning thread which runs every
				// so often to clean old messages, it is
				// possible that the remote node
				// reboots and we can deliver the message to it.
				//
				cev = nextev;
				continue;
			}	// if this event has been delivered
			// Advance to next event in the queue
			cev = cev->e_next;
		} // While more events in the queue
	} // while(1)
}

//
// launch_thread()
// Starts the dispatcher thread for the given
// remote nodeid
//
static int
launch_thread(nodeid_t nid)
{
	sc_syslog_msg_handle_t handle;

	client_node_t	*clnt = &(client_list[nid]);
	if (clnt->c_tid == thread_t(-1)) {
		if (thr_create(NULL, NULL,
		    (void *(*)(void *))ev_dispatch_thread,
		    clnt, THR_BOUND, &clnt->c_tid) != 0) {

			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_CLAPI_CLEVENTD_TAG, "");
			//
			// SCMSGS
			// @explanation
			// The cl_eventd failed to create a thread. This error
			// may cause the daemon to operate in a mode of
			// reduced functionality.
			// @user_action
			// Examine other syslog messages occurring at about
			// the same time to see if the problem can be
			// identified. Save a copy of the /var/adm/messages
			// files on all nodes and contact your authorized Sun
			// service provider for assistance in diagnosing and
			// correcting the problem.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "Thread creation failed for node %d.", nid);
			sc_syslog_msg_done(&handle);
			return (-1);
		} else if (verbose > 0) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_CLAPI_CLEVENTD_TAG, "");
			//
			// SCMSGS
			// @explanation
			// The cl_eventd successfully created a necessary
			// thread.
			// @user_action
			// This message is informational only, and does not
			// require user action.
			//
			(void) sc_syslog_msg_log(handle, LOG_INFO, MESSAGE,
			    "Created thread %d for node %d.",
			    clnt->c_tid, nid);
			sc_syslog_msg_done(&handle);
		}
	} else {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_CLAPI_CLEVENTD_TAG, "");
			//
			// SCMSGS
			// @explanation
			// The cl_eventd does not need to create a thread
			// because it found one that it could use.
			// @user_action
			// This message is informational only, and does not
			// require user action.
			//
			(void) sc_syslog_msg_log(handle, LOG_INFO, MESSAGE,
			    "Thread already running for node %d.", nid);
			sc_syslog_msg_done(&handle);
	}
	return (0);
}

//
// start_threads()
// launches client handling threads as appropiate
// initially launches threads to service all nodes
// which are part of the cluster. Additional threads
// to service newly joining nodes are statrted from
// idl_send_ref()
//
static void
start_threads(void)
{
	cmm::control_var ctrlp;
	cmm::cluster_state_t cluster_state;
	sol::nodeid_t mynid = orb_conf::local_nodeid();
	Environment e;
	sc_syslog_msg_handle_t handle;

	//
	// Startup.... Query CMM to get the list of
	// nodes in the cluster and start a dispatch
	// thread for every node in the cluster.
	//
	ctrlp = cmm_ns::get_control();
	if (CORBA::is_nil(ctrlp)) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLEVENTD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The cl_eventd was unable to obtain a list of cluster nodes
		// from the CMM. It will exit.
		// @user_action
		// Examine other syslog messages occurring at about the same
		// time to see if the problem can be identified. Save a copy
		// of the /var/adm/messages files on all nodes and contact
		// your authorized Sun service provider for assistance in
		// diagnosing and correcting the problem.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Unable to get CMM control.");
		sc_syslog_msg_done(&handle);
		exit(1);
	}
	(void) mutex_lock(&global_mutex);
	live_nodes.set(uint64_t(0));
	e.clear();
	ctrlp->get_cluster_state(cluster_state, e);
	if (e.exception() != NULL) {
		e.clear();
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLEVENTD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The cl_eventd was unable to obtain a list of cluster nodes
		// from the CMM. It will exit.
		// @user_action
		// Examine other syslog messages occurring at about the same
		// time to see if the problem can be identified. Save a copy
		// of the /var/adm/messages files on all nodes and contact
		// your authorized Sun service provider for assistance in
		// diagnosing and correcting the problem.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Error getting cluster state from CMM.");
		sc_syslog_msg_done(&handle);
		exit(1);
	}
	for (nodeid_t nid = 1; nid <= MAXNODES; nid++) {
		// Skip over nodes not in cluster
		if (cluster_state.members.members[nid] == INCN_UNKNOWN) {
			continue;
		}
		// Don't need thread for local node
		if (nid == mynid) {
			continue;
		}
		client_node_t *clnt = &client_list[nid];
		//
		// Launch a new thread for this node
		//
		(void) launch_thread(nid);
		//
		// Update our view of which nodes are in cluster. This
		// could be a subset of the cluster membership if we
		// don't have a reference to a cluster node yet. We will
		// get a reference to it via the idl_send_ref() IDL call.
		// We add that node into live_nodes at that time.
		//
		// Update: It also must be true that we have a running
		// dispatch thread for this node (thr_create can fail),
		// if so, we should not be treating this node as a
		// live node, or else events would start piling up
		// for delivery to this node (which would never happen)
		//
		// XXX The issue of what happens eventually when thread
		// creation fails remains TBD.
		//
		if (!CORBA::is_nil(clnt->c_comm_p)) {
			if (clnt->c_tid != (thread_t)-1)
				live_nodes.add_node(nid);
		}
	}
	(void) mutex_unlock(&global_mutex);
}




//
// Some globals to add debugging
//
static pid_t mypid;
static FILE *cons;

//
// event_handler()
// Invoked via doors from syseventd to deliver an event
// to us. All we need to do here is to call queue_event()
// to queue the event into our queue.
//
// Drop the event if we don't have any remote node (e.g.
// we are the only node in a 2 node cluster).
//
// Drop the event if the publisher pid is the same as
// our pid, it must be an event which looped back to us.
//
// Some logging and statistics collection is also performed.
//
#ifdef	SYSEVENTD_CHAN
static void
#else
static int
#endif
event_handler(sysevent_t *ev)
{
	pid_t publisher;
	static hrtime_t maxdelay = 0, mindelay = (uint64_t)100e9, avgdelay = 0;
	static int num_events = 0;
	int dolog = 0;
	sc_syslog_msg_handle_t handle;

	num_events++;
	if (num_events % num_log_messages == 1)
		dolog = 1;
	sysevent_get_pid(ev, &publisher);
	if (verbose > 1) {
		char *pub = sysevent_get_pub_name(ev);
		if (pub) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_CLAPI_CLEVENTD_TAG, "");
			//
			// SCMSGS
			// @explanation
			// The cl_eventd received the specified event.
			// @user_action
			// This message is informational only, and does not
			// require user action.
			//
			(void) sc_syslog_msg_log(handle, LOG_DEBUG, MESSAGE,
			    "Class<%s> SubClass<%s> Pid<%d>"
			    " Pub<%s> Seq<%lld> Len<%d>",
			    sysevent_get_class_name(ev),
			    sysevent_get_subclass_name(ev),
			    publisher,
			    pub,
			    sysevent_get_seq(ev),
			    sysevent_get_size(ev));
			sc_syslog_msg_done(&handle);

			free(pub);
		}
	}
	if (mypid == publisher) {
		if (verbose > 1) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_CLAPI_CLEVENTD_TAG, "");
			//
			// SCMSGS
			// @explanation
			// the cl_eventd received an event that it generated
			// itself. This behavior is expected.
			// @user_action
			// This message is informational only, and does not
			// require user action.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "Got my own event. Ignoring...");
			sc_syslog_msg_done(&handle);
		}

		hrtime_t now = gethrtime()/(int64_t)1e6;
		hrtime_t send_time, thisdelay;
		sysevent_get_time(ev, &send_time);
		send_time /= (int64_t)1e6;
		thisdelay = now - send_time;

		avgdelay = (avgdelay*(num_events - 1) + thisdelay);
		avgdelay /= (num_events);
		if (thisdelay > maxdelay) {
			maxdelay = thisdelay;
		}
		if (thisdelay < mindelay) {
			mindelay = thisdelay;
		}
		if (dolog && verbose) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_CLAPI_CLEVENTD_TAG, "");
			//
			// SCMSGS
			// @explanation
			// The cl_eventd is receiving and delivering messages
			// with the specified delays, as calculated
			// empirically during the lifetime of the daemon.
			// @user_action
			// This message is informational only, and does not
			// require user action.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "Maxdelay = %lld Mindelay "
			    "= %lld Avgdelay = %lld NumEv = %d\n"
			    "MaxQlen = %d CurrQlen = %d\n",
			    maxdelay, mindelay, avgdelay, num_events,
			    max_ev_q, num_ev_q);
			sc_syslog_msg_done(&handle);
		}
#ifdef	SYSEVENTD_CHAN
		return;
#else
		return (0);
#endif
	}
	if (dolog && verbose) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLEVENTD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Maxdelay = %lld Mindelay = "
		    "%lld Avgdelay = %lld NumEv = %d\n"
		    "MaxQlen = %d CurrQlen = %d\n",
		    maxdelay, mindelay, avgdelay, num_events,
		    max_ev_q, num_ev_q);
		sc_syslog_msg_done(&handle);
	}

	if (verbose > 1) {
		if (SE_FLAG(ev) != SE_PACKED_BUF) {
			(void) fprintf(cons,
			    "Tx: Event size %d is not packed\n",
			    sysevent_get_size(ev));
		} else {
			(void) fprintf(cons,
			    "Tx: Event size %d is packed\n",
			    sysevent_get_size(ev));
		}
	}

	(void) mutex_lock(&global_mutex);

	//
	// If the set of remote nodes known to us is empty (we are
	// the only node in the cluster). Then we don't queue
	// this event, as we have no gaurantee on how long it would
	// be before the remote nodes are accessible again
	//
	if (live_nodes.is_empty()) {
		if (verbose > 0) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_CLAPI_CLEVENTD_TAG, "");
			//
			// SCMSGS
			// @explanation
			// The cl_eventd does not have references to any
			// remote nodes.
			// @user_action
			// This message is informational only, and does not
			// require user action.
			//
			(void) sc_syslog_msg_log(handle, LOG_INFO, MESSAGE,
			    "No reference to any remote nodes:"
			    " Not queueing event %lld.", sysevent_get_seq(ev));
			sc_syslog_msg_done(&handle);
		}
		(void) mutex_unlock(&global_mutex);
#ifdef	SYSEVENTD_CHAN
		return;
#else
		return (0);
#endif
	}
	num_ev_q++;
	if (num_ev_q > max_ev_q)
		max_ev_q = num_ev_q;
	//
	// lint does not identify the fact that 'rc' is being used
	// conditionally based on the preprocessor directive. so
	// suppressing lint warning.
	//
	/*lint -save -e529 */
	int rc = queue_event(ev);
	// Give up all locks
	(void) mutex_unlock(&global_mutex);
#ifdef	SYSEVENTD_CHAN
		return;
#else
		return (rc);
#endif
	/*lint -restore */
}

/*
 * Register with eventd for messages.
 */
static void
open_channel(void)
{
	sysevent_handle_t *sysevent_hp;
	sc_syslog_msg_handle_t handle;


#ifdef	SYSEVENTD_CHAN
	char	*subclass_list[] = {
			EC_SUB_ALL,
			NULL
			};
	sysevent_hp = sysevent_bind_handle(event_handler);
	if (sysevent_hp == NULL) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLEVENTD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "sysevent_bind_handle(): %s", strerror(errno));
		sc_syslog_msg_done(&handle);

		exit(1);
	}
	if (sysevent_subscribe_event(sysevent_hp, EC_CLUSTER,
	    (const char **)subclass_list, 1) != 0) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLEVENTD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "sysevent_subscribe_event(): %s", strerror(errno));
		sc_syslog_msg_done(&handle);

		exit(1);
	}
#else
	if ((sysevent_hp = cl_event_open_channel(CL_EVENT_DOOR)) == NULL) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLEVENTD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The cl_eventd was unable to create the channel by which it
		// receives sysevent messages. It will exit.
		// @user_action
		// Save a copy of the /var/adm/messages files on all nodes and
		// contact your authorized Sun service provider for assistance
		// in diagnosing and correcting the problem.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "cl_event_open_channel(): %s", strerror(errno));
		sc_syslog_msg_done(&handle);

		exit(1);
	}
	if (cl_event_bind_channel(sysevent_hp, event_handler) != 0) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLEVENTD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The cl_eventd was unable to create the channel by which it
		// receives sysevent messages. It will exit.
		// @user_action
		// Save a copy of the /var/adm/messages files on all nodes and
		// contact your authorized Sun service provider for assistance
		// in diagnosing and correcting the problem.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "cl_event_bind_channel(): %s", strerror(errno));
		sc_syslog_msg_done(&handle);

		exit(1);
	}
#endif
}

//
// register_self()
// Called once at startup.
// Register ourselves with the nameservice,
// the namtage used is "clevent_<nodeis>"
//
static void
register_self(void)
{
	char nametag[32];
	Environment e;
	sc_syslog_msg_handle_t handle;

	clevent::clevent_comm_ptr clevent_server =
		(new clevent_comm_impl())->get_objref();

	sol::nodeid_t mynid = orb_conf::local_nodeid();
	naming::naming_context_var ctxp = ns::root_nameserver();

	(void) sprintf(nametag, "clevent_%d", mynid);

	ctxp->rebind(nametag, clevent_server, e);
	(void) mutex_lock(&global_mutex);
	if (e.exception() != NULL) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLEVENTD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The cl_eventd was unable to register itself with the
		// cluster nameserver. It will exit.
		// @user_action
		// Examine other syslog messages occurring at about the same
		// time to see if the problem can be identified. Save a copy
		// of the /var/adm/messages files on all nodes and contact
		// your authorized Sun service provider for assistance in
		// diagnosing and correcting the problem.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Unable to bind to nameserver");
		sc_syslog_msg_done(&handle);
		exit(1);
	}

	client_list[mynid].c_comm_p =
		clevent::clevent_comm::_duplicate(clevent_server);
	CORBA::release(clevent_server);
	(void) mutex_unlock(&global_mutex);
}

//
// resolve_others()
// Called once at startup.
// Check all nodes which  have cl_eventd running on them
// and send our reference to them by invoking idl_send_ref()
// IDL call on them.
// At this point we are already registered in the nameservice,
// Thus the race condition between we checking for a node and
// the other node registering with the name service is
// avoided.
//
static void
resolve_others(void)
{
	char nametag[32];
	Environment e;
	CORBA::Object_ptr obj;
	sc_syslog_msg_handle_t handle;

	sol::nodeid_t mynid = orb_conf::local_nodeid();

	client_node_t	*clnt = &client_list[mynid];
	clevent::clevent_comm_ptr	me = clnt->c_comm_p;

	naming::naming_context_var ctxp = ns::root_nameserver();
	clevent::clevent_comm_ptr clev_ptr;

	(void) mutex_lock(&global_mutex);
	for (sol::nodeid_t i = 1; i <= MAXNODES; i++) {
		if (i == mynid)
			continue;

		clnt = &client_list[i];
		if (!CORBA::is_nil(clnt->c_comm_p)) {
			CORBA::release(clnt->c_comm_p);
			clnt->c_comm_p = clevent::clevent_comm::_nil();
		}

		(void) sprintf(nametag, "clevent_%d", i);
		e.clear();
		(void) mutex_unlock(&global_mutex);
		obj = ctxp->resolve(nametag, e);
		(void) mutex_lock(&global_mutex);
		if (e.exception() != NULL) {
			continue;
		}
		clev_ptr = clevent::clevent_comm::_narrow(obj);
		if (CORBA::is_nil(clev_ptr)) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_CLAPI_CLEVENTD_TAG, "");
			//
			// SCMSGS
			// @explanation
			// The cl_eventd found an unexpected object in the
			// nameserver. It may be unable to forward events to
			// one or more nodes in the cluster.
			// @user_action
			// Examine other syslog messages occurring at about
			// the same time to see if the problem can be
			// identified. Save a copy of the /var/adm/messages
			// files on all nodes and contact your authorized Sun
			// service provider for assistance in diagnosing and
			// correcting the problem.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "Some other object bound to <%s>.", nametag);
			sc_syslog_msg_done(&handle);

			continue;
		}
		// Send my own reference to the remote node
		(void) mutex_unlock(&global_mutex);
		sol::error_t err = clev_ptr->idl_send_ref(me, mynid, e);
		(void) mutex_lock(&global_mutex);
		if (e.exception() != NULL) {
			if ((CORBA::COMM_FAILURE::_exnarrow(e.exception())
				!= NULL) ||
				(CORBA::INV_OBJREF::_exnarrow(e.exception())
				!= NULL)) {
				// Remote node died or the server on the
				// remote node died. Don't bother with
				// this node, for now. When the
				// node reboots or the server restarts,
				// it would send its reference to us.
			} else {
				// Something else happend
				if (verbose)
					e.exception()->print_exception(
						"IDL Exception Sending self "
						" reference to remote node.");
				ASSERT(0);
			}
			CORBA::release(clev_ptr);
			continue;
		}

		// Update our reference to the remote node
		if (!CORBA::is_nil(clnt->c_comm_p)) {
			CORBA::release(clnt->c_comm_p);
			clnt->c_comm_p = clevent::clevent_comm::_nil();
		}
		clnt->c_comm_p =
			clevent::clevent_comm::_duplicate(clev_ptr);

		//
		// Return code from idl_send_ref() is non-zero IFF
		// we were unable to start a delivery thread on the
		// remote node for event delivery to us.
		//
		if (err) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_CLAPI_CLEVENTD_TAG, "");
			//
			// SCMSGS
			// @explanation
			// An attempt to deliver an event to the specified
			// node failed.
			// @user_action
			// Examine other syslog messages occurring at about
			// the same time on both this node and the remote node
			// to see if the problem can be identified. Save a
			// copy of the /var/adm/messages files on all nodes
			// and contact your authorized Sun service provider
			// for assistance in diagnosing and correcting the
			// problem.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "Unable to launch dispatch thread "
			    "on remote node %d", i);
			sc_syslog_msg_done(&handle);
		}

		CORBA::release(clev_ptr);
	}
	(void) mutex_unlock(&global_mutex);
}

//
// main()
// Do command line processing, daemonize,
// register with the name service, resolve other nodes,
// start dispatch threads, open doors channel to
// syseventd. Then the main thread exits.
//
int
main(int argc, char *argv[])
{
	int	c;
	sc_syslog_msg_handle_t handle;

	if (sc_zonescheck() != 0)
		return (1);

	// Must to root to start
	if (getuid() != 0) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLEVENTD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// A non-root user attempted to start the cl_eventd.
		// @user_action
		// Start the cl_event as root.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Must be root to start %s.", argv[0]);
		sc_syslog_msg_done(&handle);

		return (1);
	}
	while ((c = getopt(argc, argv, "vn:s:")) != EOF) {
		switch (c) {
		case 'v':
			verbose++;
			break;
		case 'n':
			num_log_messages = atoi(optarg);
			break;
		default:
			(void) fprintf(stderr,
				"Usage: %s [-v] [-n num_log_messages]\n",
				argv[0]);
			return (1);
		}
	}
	argc -= optind;
	argv += optind;

	if (argc > 0) {
		(void) fprintf(stderr,
			"Usage: %s [-v] [-n num_log_messages]\n",
			argv[0]);
		return (1);
	}
	if (!verbose) {
		// Become a daemon
		pid_t p = fork1();
		if (p < 0) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_CLAPI_CLEVENTD_TAG, "");
			//
			// SCMSGS
			// @explanation
			// The cl_eventd was unable to start because it could
			// not daemonize.
			// @user_action
			// Examine other syslog messages occurring at about
			// the same time to see if the problem can be
			// identified. Save a copy of the /var/adm/messages
			// files on all nodes and contact your authorized Sun
			// service provider for assistance in diagnosing and
			// correcting the problem.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "Cannot fork: %s", strerror(errno));
			sc_syslog_msg_done(&handle);

			exit(1);
		}
		if (p > 0)
			_exit(0);	// parent exits

		//
		// Child continues here.
		// Close all file descriptors to start with a clean fd space
		struct rlimit rl;
		(void) getrlimit(RLIMIT_NOFILE, &rl);
		for (uint_t fd = 0; fd < rl.rlim_max; fd++)
			(void) close((int)fd);

		errno = 0;	// Cleanup benign erros from close()

		//
		// Open /dev/null as stdin, stdout and stderr to
		// avoid any library calls accidently picking
		// them up.
		//
		(void) open("/dev/null", O_RDONLY);
		(void) open("/dev/null", O_WRONLY);
		(void) dup(1);

		(void) chdir("/");
		(void) umask(022);
		(void) setsid();

	}
	//
	// Store our pid in the global variable for comparison with
	// event publishers. This is how we determine whether or not
	// to forward events to other cluster nodes.
	//
	mypid = getpid();

	if (verbose)
		cons = stdout;
	else
		cons = fopen("/dev/null", "w");
	if (cons == NULL) {
		perror("fopen(cons)");
		return (1);
	}

	// We are now ready to initialize ORB
	if (ORB::initialize() != 0) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLEVENTD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Error: unable to initialize ORB.");
		sc_syslog_msg_done(&handle);

		exit(1);
	}

	// set up the syslog tag
	(void) sc_syslog_msg_set_syslog_tag(SC_SYSLOG_CLAPI_CLEVENTD_TAG);

	init_queues();

	// Register with the ORB nameserver
	register_self();

	//
	// Resolve object references to other nodes and send
	// them a reference to our server object.
	resolve_others();

	//
	// Launch dispatch threads for all nodes currently part of
	// the cluster
	//
	start_threads();

	// We are now ready to recieve events from the syseventd SLM
	open_channel();

	//
	// We are all set. All processing now happens via doors
	// and threads.
	// We have nothing to do. So make the main thread exit.
	//
	int rc = 0;
	thr_exit(&rc);
	return (0);
}


//
// Implementation of IDL methods
//

clevent_comm_impl::clevent_comm_impl()
{
}

//
// _unreferenced is called when the last reference to the
// server object is released. This would never happen because
// we keep a copy of the reference ourselves. We exit
// here just in case... We get restarted by PMF.
//
// Update: Per ORB folks, since we don't support
// mutiple 0->1 transitions, and this should never
// happen in the first place, best is to assert.
//
void
clevent_comm_impl::_unreferenced(unref_t)
{
	ASSERT(0);
}

//
// idl_ev_send()
// Invoked by remote nodes to deliver a cluster event to us.
// We essentially call sysevent_post_event() to post
// the event locally. In case of memory allocation failures,
// or EAGAIN from sysevent_post_event(), we sleep for
// RETRY_INTERVAL and try upto RETRY_COUNT times.
// It is better to retry here then to bubble back EAGAIN to
// the remote node and ask it to retry, saves on IDL calls.
//
sol::error_t
clevent_comm_impl::idl_ev_send(const clevent::idl_event &ev,
			Environment &)
{
	//
	// Post events recieved from remote nodes locally
	//
	sysevent_id_t	eid;
	sysevent_t	*sol_ev = NULL;

	sol_ev = (sysevent_t *)(ev.buf());
	errno = 0;

	//
	// get_vendor and get_pub return new memory which
	// needs to be free()ed. class and subcalls return
	// memory which points into the event and should
	// not be freed.
	//
	char *e_class = sysevent_get_class_name(sol_ev);
	char *e_subclass = sysevent_get_subclass_name(sol_ev);
	char *e_vendor = NULL;
	char *e_pub = NULL;
	nvlist_t	*nv = NULL;
	int			num_tries = 0;
	int			err = 0;
	sc_syslog_msg_handle_t handle;


	// Try upto RETRY_COUNT times to deliver the event
	while (num_tries++ < RETRY_COUNT) {

		if (err) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_CLAPI_CLEVENTD_TAG, "");
			//
			// SCMSGS
			// @explanation
			// The cl_eventd was unable to post an event to the
			// sysevent queue locally, but will retry.
			// @user_action
			// No action required yet.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "Unable to post event %lld: %s: Retrying..",
			    sysevent_get_seq(sol_ev), strerror(errno));
			sc_syslog_msg_done(&handle);

			(void) sleep((uint_t)RETRY_INTERVAL);
		}

		if (e_vendor == NULL)
			e_vendor = sysevent_get_vendor_name(sol_ev);
		if (e_pub == NULL)
			e_pub = sysevent_get_pub_name(sol_ev);
		if ((e_vendor == NULL) || (e_pub == NULL)) {
			err = EAGAIN;
			continue;	// Goto the top of loop
		}
		if (verbose > 1) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_CLAPI_CLEVENTD_TAG, "");
			//
			// SCMSGS
			// @explanation
			// The cl_eventd is posting an event locally with the
			// specified attributes.
			// @user_action
			// This message is informational only, and does not
			// require user action.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "Class <%s> SubClass <%s> Vendor <%s> Pub <%s>",
			    e_class, e_subclass, e_vendor, e_pub);
			sc_syslog_msg_done(&handle);
		}
		if (nv == NULL) {
			if (sysevent_get_attr_list(sol_ev, &nv) != 0) {
				err = errno;
				continue;	// Goto the top of loop
			}
		}
		if (sysevent_post_event(e_class, e_subclass,
			e_vendor, e_pub, nv, &eid) != 0) {
			err = errno;
			continue;
		} else {
			err = 0;
			break;
		}
	}

	//
	// We have already tried a couple of times, no point
	// returning EAGAIN to the remote node. We give up.
	//
	if (err == EAGAIN)
		err = -1;
	if (err != 0) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLEVENTD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The cl_eventd was unable to post an event to the sysevent
		// queue locally, and will not retry.
		// @user_action
		// Examine other syslog messages occurring at about the same
		// time to see if the problem can be identified. Save a copy
		// of the /var/adm/messages files on all nodes and contact
		// your authorized Sun service provider for assistance in
		// diagnosing and correcting the problem.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Failed to post event %lld: %s",
		    sysevent_get_seq(sol_ev), strerror(errno));
		sc_syslog_msg_done(&handle);
	}

	if (e_vendor)
		free(e_vendor);
	if (e_pub)
		free(e_pub);
	if (nv)
		nvlist_free(nv);

	return (err);
}

//
// idl_send_ref()
// Called from remote nodes joining the cluster to send
// their object references to us.
// We update our reference to this node and start a dispatch
// thread for it (if not already running)
//

sol::error_t
clevent_comm_impl::idl_send_ref(clevent::clevent_comm_ptr remote_ref,
		sol::nodeid_t	remote_nid, Environment&)
{
	sol::error_t	rc = 0;
	sc_syslog_msg_handle_t handle;

	if ((remote_nid < 1) || (remote_nid > MAXNODES)) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLEVENTD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The cl_eventd received an event delivery from an unknown
		// remote node.
		// @user_action
		// Examine other syslog messages occurring at about the same
		// time to see if the problem can be identified. Save a copy
		// of the /var/adm/messages files on all nodes and contact
		// your authorized Sun service provider for assistance in
		// diagnosing and correcting the problem.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Message from invalid nodeid <%d>.", remote_nid);
		sc_syslog_msg_done(&handle);

		return (-1);
	}

	(void) mutex_lock(&global_mutex);

	client_node_t *clnt = &client_list[remote_nid];

	//
	// Update our reference to this node, releasing
	// existing reference, if we do have one.
	//

	if (!CORBA::is_nil(clnt->c_comm_p)) {
		CORBA::release(clnt->c_comm_p);
		clnt->c_comm_p = clevent::clevent_comm::_nil();
	}
	clnt->c_comm_p = clevent::clevent_comm::_duplicate(remote_ref);


	//
	// Check if we already have a thread servicing this
	// newly joined node
	//
	if (clnt->c_tid == (thread_t)-1) {
		//
		// Don't have a dispatch thread,
		// So start one.
		//
		(void) launch_thread(remote_nid);
	}
	//
	// If we actually have a thread running for this node
	// (either running already, or just started successfully)
	// add this node to the live_nodes set.
	//

	if (clnt->c_tid != (thread_t)-1) {
		// Add this node to the set of live nodes
		live_nodes.add_node(remote_nid);
		rc = 0;
	} else {
		rc = -1;
	}
	if (verbose) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CLAPI_CLEVENTD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The cl_eventd has references to the specified nodes.
		// @user_action
		// This message is informational only, and does not require
		// user action.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Reachable nodes are %llx", live_nodes.bitmask());
		sc_syslog_msg_done(&handle);
	}

	(void) mutex_unlock(&global_mutex);
	return (rc);
}
