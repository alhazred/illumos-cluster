/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_CLEVENT_COMM_IMPL_H
#define	_CLEVENT_COMM_IMPL_H

#pragma ident	"@(#)clevent_comm_impl.h	1.5	08/05/20 SMI"

#include <orb/object/adapter.h>
#include <h/clevent.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif


/*CSTYLED*/
class clevent_comm_impl : public McServerof<clevent::clevent_comm> {
private:
	//
	// Public interface
	//
public:
	clevent_comm_impl();
	~clevent_comm_impl() {}
	void _unreferenced(unref_t cookie);

	//
	// Methods that implement the server side of the IDL interface
	//

	sol::error_t	idl_ev_send(
			const clevent::idl_event &event,
			Environment &e);

	sol::error_t idl_send_ref(
		clevent::clevent_comm* remote_ref_p,
		sol::nodeid_t	remote_nid, Environment &e);
};

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#endif /* _CLEVENT_COMM_IMPL_H */
