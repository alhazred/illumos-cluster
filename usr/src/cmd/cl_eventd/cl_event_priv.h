/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * clevent.h
 *
 * Defines classes used by the cluster event forwarder daemon
 * to queue sysevents and to manage delivery threads for delivery
 * to remote cluster nodes.
 */

#ifndef	_CLEVENT_PRIV_H
#define	_CLEVENT_PRIV_H

#pragma ident	"@(#)cl_event_priv.h	1.5	08/05/20 SMI"

#ifdef	__cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#include <orb/object/adapter.h>
#include <h/clevent.h>
#include <h/cmm.h>
#include <sys/nodeset.h>
#include <cmm/ucmm_api.h>
#include <cmm/cmm_ns.h>

#include "cl_event.h"

#include "clevent_comm_impl.h"


typedef struct _cl_event {
	_cl_event	*e_next;	// Pointer to next event on list
	_cl_event	*e_prev;	// Pointer to previous event on list
	nodeset		e_set;		// Set of nodes it has been delivered to
	clevent::idl_event	*e_idl_ev;	// Event converted to IDL
} cl_event_t;

typedef struct _client_node {
	nodeid_t			c_nid;	// Nodeid
	thread_t			c_tid;	// Thread id, -1 if no thread
	clevent::clevent_comm_ptr	c_comm_p;	// Remote ref
} client_node_t;

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#endif	/* _CLEVENT_PRIV_H */
