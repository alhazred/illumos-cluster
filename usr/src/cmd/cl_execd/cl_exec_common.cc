//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)cl_exec_common.cc	1.7	08/07/28 SMI"

#include <cmm/cmm_ns.h>
#include <cmm/ff_signal.h>
#include <signal.h>
#include <fcntl.h>
#include <sys/cladm_int.h>
#include <sys/cladm_debug.h>
#include <sys/priocntl.h>
#include <sys/sol_version.h>
#include <h/remote_exec.h>
#include <pthread.h>
#include <cl_execd.h>

#define	PROG_NAME	"cl_execd"

//
// Global declarations.
//

// Default scheduling class
extern remote_exec::sched_class_t default_cid;

static ff::failfast_var	death_ff_v;

//
// mutex to syncronize logging.
//
extern mutex_t dbg_mutex;

//
// Forward declarations.
//
void set_task_priority(remote_exec::sched_class_t, int);
void set_ts_prio(int);
void set_rt_prio(int);
void set_fss_prio(int);
void set_fx_prio(int);

//
// Zoneid of the zone in which the daemon is running.
//
#if (SOL_VERSION >= __s10)
extern zoneid_t zoneid;

//
// Default log file.
//
#define	CL_EXEC_LOG_FILE "/var/cluster/.cl_execd_debug.log"
static FILE *logfilep;
//
// Open the debug log file. This is used for logging if we are running in
// the non-global zone.
//
void
log_init()
{
	logfilep = fopen(CL_EXEC_LOG_FILE, "ab+");
}

//
// Log to the debug log file.
//
void
log_to_file(int level, const char *format, va_list v)
{
	char	buffer[MAXPATHLEN];
	log_init();
	if (logfilep != NULL) {
		(void) vsnprintf(buffer, MAXPATHLEN - 1, format, v);
		(void) fprintf(logfilep, "level %d: %s\n", level, buffer);
		(void) fflush(logfilep);
	}
	(void) fclose(logfilep);
}
#endif // __SOL_VERSION >= __s10

//
//
// Add a printf-style message to whichever debug logs we are currently using.
//
void
cl_exec_log(int level, const char *fmt, ...)
{
	va_list ap;
	cladmin_log_t *tolog;
	size_t len;

	(void) mutex_lock(&dbg_mutex);

	/*lint -e40 Undeclared identifier (__builtin_va_alist) */
	va_start(ap, fmt);
	/*lint +e40 */

#if (SOL_VERSION >= __s10)
	//
	// If running in a non-global zone we log to a file if possible.
	// If not, the logs go to the kernel debug buffer.
	//
	if (zoneid != GLOBAL_ZONEID && logfilep != NULL) {
		log_to_file(level, fmt, ap);
		va_end(ap);
		(void) mutex_unlock(&dbg_mutex);
		return;
	}
#endif // __SOL_VERSION >= __s10

	tolog = (cladmin_log_t *)malloc(sizeof (cladmin_log_t));

	if (tolog == NULL) {
		va_end(ap);
		(void) mutex_unlock(&dbg_mutex);
		return;
	}
	tolog->level = level;

	(void) snprintf(tolog->str, CLADMIN_LOG_SIZE - 1, "cl_exec%u,%u:",
	    getpid(), thr_self());

	len = strlen(tolog->str);
	(void) vsnprintf(tolog->str + len, (CLADMIN_LOG_SIZE - 1) - len, fmt,
	    ap);
	(void) os::cladm(CL_CONFIG, CL_ADMIN_LOG, (void *)tolog);
	free(tolog);
	va_end(ap);
	(void) mutex_unlock(&dbg_mutex);
}

//
// Helper function to log messages.
//
void
do_log(const char *format, va_list v)
{
	char work[MAXPATHLEN];

	(void) mutex_lock(&dbg_mutex);

	os::sc_syslog_msg msg("", PROG_NAME, NULL);
	(void) vsnprintf(work, MAXPATHLEN -1, format, v);
	(void) mutex_unlock(&dbg_mutex);

	cl_exec_log(CLADM_RED, "do_log %s\n", work);

	(void) mutex_lock(&dbg_mutex);
	(void) msg.log(SC_SYSLOG_ERROR, MESSAGE, NO_SC_SYSLOG_MESSAGE(work));
	(void) mutex_unlock(&dbg_mutex);
}

//
// Log errors to syslog as well as debug logs.
//
void
log_error(const char *format, ...)
{
	va_list v;
	va_start(v, format);	/*lint !e40 */
	do_log(format, v);
	va_end(v);
}
//
// Helper function to log errors and exit.
//
void
do_exit(int exit_code, const char *format, ...)
{
	va_list v;
	va_start(v, format);	/*lint !e40 */
	do_log(format, v);
	va_end(v);

	cl_exec_log(CLADM_RED, "do_exit %d\n", exit_code);
	exit(exit_code);
}

//
// Helper function to retrieve the scheduling class id for the
// current process.
//
idtype_t
get_proc_classid()
{
	pcparms_t	pc_parms;

	pc_parms.pc_cid = PC_CLNULL;

	if ((priocntl(P_PID, getpid(), PC_GETPARMS, (char *)&pc_parms))
	    == -1) {
		//
		// SCMSGS
		// @explanation
		// cl_execd program has encountered a failed priocntl(2)
		// system call. The error message indicates the error number
		// for the failure.
		// @user_action
		// Contact your authorized Sun service provider to
		// determine whether a workaround or patch is available.
		//
		do_exit(errno, SC_SYSLOG_MESSAGE("cl_execd: priocntl "
		    "returned %d.  Exiting."), errno);
		// NOTREACHABLE
	}
	return ((idtype_t)pc_parms.pc_cid);
}

//
// Helper function to retrieve the scheduling classid given the name.
//
idtype_t
get_classid(const char *classname)
{
	pcinfo_t pc_info;

	(void) strcpy(pc_info.pc_clname, classname);

	if ((priocntl(P_PID, P_MYID, PC_GETCID, (char *)&pc_info)) == -1) {
		//
		// scmsgs repeated above.
		//
		do_exit(errno, SC_SYSLOG_MESSAGE("cl_execd: priocntl "
		    "returned %d.  Exiting."), errno);
		// NOTREACHABLE
	}

	return ((idtype_t)pc_info.pc_cid);
}

//
// Set the priority to default, i.e. the priority that was recorded by
// the worker process just after starting up.
//
void
set_default_prio(int pri)
{
	set_task_priority(default_cid, pri);
}

//
// Helper function to set the priority and scheduling class of the task.
//
void
set_task_priority(remote_exec::sched_class_t cid, int pri)
{
	//
	// If a default priority is requested then the task is
	// assigned the priority that was recorded at startup of the.
	// worker process.
	//
	switch (cid) {

	case remote_exec::DEF:
		cl_exec_log(CLADM_GREEN, "Task: change to default class\n");
		set_default_prio(pri);
		break;
	case remote_exec::TS:
		cl_exec_log(CLADM_GREEN, "Task: change to Time shared "
		    "scheduling class\n");
		set_ts_prio(pri);
		break;
	case remote_exec::RT:
		cl_exec_log(CLADM_GREEN, "Task: change to real time scheduling"
		    " class\n");
		set_rt_prio(pri);
		break;
	case remote_exec::FSS:
		cl_exec_log(CLADM_GREEN, "Task: change to Fair share "
		    "scheduling class\n");
		set_fss_prio(pri);
		break;
	case remote_exec::FX:
		cl_exec_log(CLADM_GREEN, "Task: change to Fixed scheduling "
		    "class\n");
		set_fx_prio(pri);
		break;
	default:
		// This shoudn't happen.
		ASSERT(0);
		// NOT REACHED.
	}
}

//
// Set the priority to Real time scheduling class.
//
void
set_rt_prio(int pri)
{
	ff::failfast_admin_var  ff_admin_v;
	Environment 		e;
	int			error;

	ff_admin_v = cmm_ns::get_ff_admin();

	if (CORBA::is_nil(ff_admin_v)) {
		//
		// Given the lack of an error code to represent what happened
		// we choose ECOMM to best represent why we failed.
		//

		//
		// SCMSGS
		// @explanation
		// cl_execd program could not get a reference to the CORBA
		// failfast server object.
		// @user_action
		// Contact your authorized Sun service provider to
		// determine whether a workaround or patch is available.
		//
		do_exit(ECOMM, SC_SYSLOG_MESSAGE("set_rt_prio : "
		    "Unable to get ff_admin_v"));
	}

	//
	// Set the priority to real time using the failfast interface.
	//
	error = ff_admin_v->set_rt_priority((int)getpid(), pri, e);

	if (e.exception() != NULL || error != 0) {
		if (e.exception() != NULL) {
			e.clear();
		}
		//
		// SCMSGS
		// @explanation
		// cl_execd program failed to set the priority and the
		// scheduling class to real time for the task in execution.
		// @user_action
		// Contact your authorized Sun service provider to
		// determine whether a workaround or patch is available.
		//
		do_exit(errno, SC_SYSLOG_MESSAGE("cl_execd: Attempt to set "
		    "to real time scheduling class returned %d.  Exiting."),
		    error);
	}
}

//
// Set the priority to Fixed scheduling class.
//
void
set_fx_prio(int pri)
{
	ff::failfast_admin_var  ff_admin_v;
	Environment 		e;
	int			error;

	ff_admin_v = cmm_ns::get_ff_admin();

	if (CORBA::is_nil(ff_admin_v)) {
		//
		// Given the lack of an error code to represent what happened
		// we choose ECOMM to best represent why we failed.
		//
		// SCMSGS
		// @explanation
		// cl_execd program coudn't get a reference to the CORBA
		// failfast server object.
		// @user_action
		// Contact your authorized Sun service provider to
		// determine whether a workaround or patch is available.
		//
		do_exit(ECOMM, SC_SYSLOG_MESSAGE("set_fx_prio : "
		    "Unable to get ff_admin_v"));
	}

	//
	// Set the priority to real time using the failfast interface.
	//
	error = ff_admin_v->set_fx_priority((int)getpid(), pri, e);

	if (e.exception() != NULL || error != 0) {
		if (e.exception() != NULL) {
			e.clear();
		}
		//
		// SCMSGS
		// @explanation
		// cl_execd program failed to set the priority and the
		// scheduling class to Fixed for the task in execution.
		// @user_action
		// Contact your authorized Sun service provider to
		// determine whether a workaround or patch is available.
		//
		do_exit(errno, SC_SYSLOG_MESSAGE("cl_execd: Attempt to set "
		    "scheduling class to Fixed returned %d.  Exiting."), error);
	}
}

//
// Set the priority to Fair share scheduling class.
//
void
set_fss_prio(int pri)
{
	ff::failfast_admin_var  ff_admin_v;
	Environment 		e;
	int			error;

	ff_admin_v = cmm_ns::get_ff_admin();

	if (CORBA::is_nil(ff_admin_v)) {
		//
		// Given the lack of an error code to represent what happened
		// we choose ECOMM to best represent why we failed.
		//
		//
		// SCMSGS
		// @explanation
		// cl_execd program coudn't get a reference to the CORBA
		// failfast server object.
		// @user_action
		// Contact your authorized Sun service provider to
		// determine whether a workaround or patch is available.
		//
		do_exit(ECOMM, SC_SYSLOG_MESSAGE("set_fss_prio : "
		    "Unable to get ff_admin_v"));
	}

	//
	// Set the priority to real time using the failfast interface.
	//
	error = ff_admin_v->set_fss_priority((int)getpid(), pri, e);

	if (e.exception() != NULL || error != 0) {
		if (e.exception() != NULL) {
			e.clear();
		}
		//
		// SCMSGS
		// @explanation
		// cl_execd program failed to set the priority and the
		// scheduling class to Fair share for the task in execution.
		// @user_action
		// Contact your authorized Sun service provider to
		// determine whether a workaround or patch is available.
		//
		do_exit(errno, SC_SYSLOG_MESSAGE("cl_execd: Attempt to set "
		    "scheduling class to Fair share returned %d.  Exiting."),
		    error);
	}
}

//
// Set the priority to Time shared scheduling class.
//
void
set_ts_prio(int pri)
{
	ff::failfast_admin_var  ff_admin_v;
	Environment 		e;
	int			error;

	ff_admin_v = cmm_ns::get_ff_admin();

	if (CORBA::is_nil(ff_admin_v)) {
		//
		// Given the lack of an error code to represent what happened
		// we choose ECOMM to best represent why we failed.
		//
		//
		// SCMSGS
		// @explanation
		// cl_execd program coudn't get a reference to the CORBA
		// failfast server object.
		// @user_action
		// Contact your authorized Sun service provider to
		// determine whether a workaround or patch is available.
		//
		do_exit(ECOMM, SC_SYSLOG_MESSAGE("set_ts_prio : "
		    "Unable to get ff_admin_v"));
	}

	//
	// Set the priority in time shared scheduling class using the failfast
	// interface.
	//
	error = ff_admin_v->set_ts_priority((int)getpid(), pri, e);

	if (e.exception() != NULL || error != 0) {
		if (e.exception() != NULL) {
			e.clear();
		}
		//
		// SCMSGS
		// @explanation
		// cl_execd program failed to set the priority and the
		// scheduling class to Time shared for the task in execution.
		// @user_action
		// Contact your authorized Sun service provider to
		// determine whether a workaround or patch is available.
		//
		do_exit(errno, SC_SYSLOG_MESSAGE("cl_execd: Attempt to set "
		    "scheduling class to Time shared returned %d.  Exiting."),
		    error);
	}
}

//
// Read the output of the task execution on the streams. The output
// is truncated to a fixed size for simplicity of memory management
// for the door server. This should suffice as the generated output
// is used for informational purposes only.
//
void *
read_fd(void *argsp)
{
	ssize_t		len;
	char		rdbuf[MAXBUF + 1];
	size_t		buf_len			= MAXBUF;
	char		*data_buf		= NULL;
	char		*tmp_buf		= NULL;
	int		data_buf_len		= 0;
	read_fd_buf_t	*rfd_bufp		= (read_fd_buf_t *)argsp;
	int		fd			= rfd_bufp->fd;

	//
	// Read data up to MAXPATHLEN - 1 (save enough space to add
	// a NULL to the rdbuf) with each pass through the outer loop.
	// Add to the buffer with each pass.
	//
	do {
		do {
			len = read(fd, rdbuf, buf_len);
		} while ((len == -1) && (errno == EINTR));

		cl_exec_log(CLADM_GREEN, " Data length = %d\n", len);

		if (len == -1) {
			//
			// SCMSGS
			// @explanation
			// The cl_execd program encountered a failure while
			// executing the read(2) system call.
			// @user_action
			// Contact your authorized Sun service provider to
			// determine whether a workaround or patch is available.
			//
			log_error(SC_SYSLOG_MESSAGE("cl_execd: (thread %d): "
			    "Error %d from read"), pthread_self(), errno);
			if (data_buf != NULL) {
				delete [] data_buf;
				data_buf =  NULL;
			}
			break;
		} else if (len == 0) {
			//
			// The stream has no more data to read. We can
			// return here.
			//
			break;
		} else {
			//
			// We read some data from the stream (size <= buf_len)
			// We append this data read to the buffer.
			//
			rdbuf[len] = 0;

			if (data_buf == NULL) {
				//
				// If the buffer is unallocated we just read
				// from the stream for the first time.
				// Allocate memory for the buffer.
				//
				data_buf = new char[(uint_t)(len + 1)];
				data_buf_len = len + 1;

				if (data_buf == NULL) {
					//
					// SCMSGS
					// @explanation
					// Could not allocate memory. Node is
					// too low on memory.
					// @user_action
					// Contact your authorized Sun service
					// provider to determine whether a
					// workaround or patch is available.
					//
					do_exit(errno, SC_SYSLOG_MESSAGE(
					    "cl_execd: memory allocation "
					    " failed size = %d"), len + 1);
					break;
				}
				//
				// Copy data.
				//
				(void) strncpy(data_buf, rdbuf, len + 1);
			} else {
				//
				// The buffer was already allocated and has
				// some data from the stream. Reallocate the
				// buffer here.
				//
				tmp_buf =
				    new char[(uint_t)(len + data_buf_len)];

				if (tmp_buf == NULL) {
					//
					// scmsgs already explained before
					//
					do_exit(errno, SC_SYSLOG_MESSAGE(
					    "cl_execd: memory allocation "
					    " failed size = %d"), len +
					    data_buf_len + 1);
					break;
				}
				//
				// Copy data.
				//
				(void) strncpy(tmp_buf, data_buf,
				    data_buf_len);
				(void) strcat(tmp_buf, rdbuf);

				delete [] data_buf;
				data_buf = tmp_buf;
				//
				// Increment the data size read from the stream
				// so far.
				//
				data_buf_len += len;
			}
		}
	} while (1);
	if (data_buf != NULL) {
		if (rfd_bufp->op == STDOUT_OP) {
			(void) strncpy((char *)rfd_bufp->rbuf->output,
			    data_buf, MAXBUF);
		} else if (rfd_bufp->op == STDERR_OP) {
			(void) strncpy((char *)rfd_bufp->rbuf->erroutput,
			    data_buf, MAXBUF);
		}
		delete [] data_buf;
	}
	return ((void *)NULL);
}

//
// Create and arm a failfast object so that the node dies when cl_execd dies.
//
void
create_and_arm_failfast(const char *ff_name)
{
	ASSERT(ff_name != NULL);

	ff::failfast_admin_var  ff_admin_v = cmm_ns::get_ff_admin();
	Environment 		e;

	//
	// Use FFM_DEFERRED_PANIC to allow enough time for a core
	// dump of this process.
	//

	death_ff_v = ff_admin_v->ff_create(ff_name, ff::FFM_DEFERRED_PANIC, e);

	if (e.exception() || CORBA::is_nil(death_ff_v)) {

		//
		// Given the lack of an error code to represent what happened
		// we choose ECOMM to best represent why we failed.
		//

		//
		// SCMSGS
		// @explanation
		// The cl_execd program was not able to register with failfast.
		// @user_action
		// Contact your authorized Sun service provider to
		// determine whether a workaround or patch is available.
		//
		do_exit(ECOMM, SC_SYSLOG_MESSAGE("cl_execd: unable to create "
		    "failfast object."));
	} else {

		// Generate a failfast only when this process dies.

		death_ff_v->arm(ff::FF_INFINITE, e);
		if (e.exception()) {
			//
			// SCMSGS
			// @explanation
			// The cl_execd program was not able to arm the
			// failfast unit.
			// @user_action
			// Contact your authorized Sun service provider to
			// determine whether a workaround or patch is available.
			//
			do_exit(ECOMM, SC_SYSLOG_MESSAGE("cl_execd: unable to "
			    "arm failfast."));
		}
		// Trigger the failfast upon receipt of these signals
		ff_register_signal_handler(SIGILL, &death_ff_v);
		ff_register_signal_handler(SIGBUS, &death_ff_v);
		ff_register_signal_handler(SIGSEGV, &death_ff_v);
	}
}

//
// Utility function to mask all the maskable signals.
//
void
block_allsignals(void)
{
	sigset_t s_mask;

	//
	// Block the signals that can be masked.
	//

	//
	// SCMSGS
	// @explanation
	// cl_execd program has encountered a failed sigfillset(2)
	// system call. The error message indicates the error number
	// for the failure.
	// @user_action
	// Contact your authorized Sun service provider to
	// determine whether a workaround or patch is available.
	//
	if (sigfillset(&s_mask) == -1)
		do_exit(errno, SC_SYSLOG_MESSAGE("cl_execd: sigfillset "
		    "returned %d.  Exiting."), errno);

	if (thr_sigsetmask(SIG_BLOCK, &s_mask, NULL) != 0)
		//
		// SCMSGS
		// @explanation
		// cl_execd program has encountered a failed thr_sigsetmask(3C)
		// system call. The error message indicates the error number
		// for the failure.
		// @user_action
		// Contact your authorized Sun service provider to
		// determine whether a workaround or patch is available.
		//
		do_exit(errno, SC_SYSLOG_MESSAGE("cl_execd: thr_sigsetmask "
		    "returned %d.  Exiting."), errno);
}

//
// Wait for signals and log any signal received. The SIGHUP signal is
// ignored after logging a message. For any other signal the action is
// to exit after logging the information. This is run as a seperate
// thread.
//
void *
wait_for_signals(void *)
{
	sigset_t	s_mask;
	siginfo_t	sinfo;
	int		sig	= 0;

	cl_exec_log(CLADM_GREEN, "signal thread starting\n");

	//
	// Here we block all maskable signals.
	//
	block_allsignals();

	//
	// Fill the mask with all the signals in the system
	//
	if (sigfillset(&s_mask) == -1) {
		//
		// scmsgs already explained.
		//
		do_exit(errno, SC_SYSLOG_MESSAGE("cl_execd: sigfillset "
		    "returned %d.  Exiting."), errno);
	}

	//
	// sigwait can return with EINTR when a thread forks.
	// Also ignore SIGHUP. We could have simply added SIGHUP to the
	// list of signals to be masked but the check is here so that we
	// can add a syslog message if necessary for SIGHUP.
	//
	do {

		sig = sigwaitinfo(&s_mask, &sinfo);
		cl_exec_log(CLADM_GREEN, "catch signal %d "
			"%d si_code %x si_pid %ld si_uid %ld\n",
			sig,
			sinfo.si_signo,
			sinfo.si_code,
			sinfo.si_pid,
			sinfo.si_uid);
	} while (((sig < 0) && (errno == EINTR))
	      || (sig == SIGHUP) || (sig == SIGCHLD));

	switch (sig) {
	case -1:
		//
		// SCMSGS
		// @explanation
		// cl_execd program has encountered a failed sigwaitinfo(3RT)
		// system call. The error message indicates the error number
		// for the failure.
		// @user_action
		// Contact your authorized Sun service provider to
		// determine whether a workaround or patch is available.
		//
		do_exit(errno, SC_SYSLOG_MESSAGE("cl_execd: sigwait "
		    "returned %d.  Exiting."), errno);
		/* FALLTHROUGH */
		/* NOTREACHED */
	default:
		/* Log also the signal emitter */
		{
			char cmd[32];
			char data_buf[256];
			FILE *fp;
			(void) sprintf(cmd, "/usr/bin/ptree %u", sinfo.si_pid);
			//
			// Log the information about who sent the signal for
			// debug purposes. Here we execute the "ptree" command
			// on the process that sent the signal and log the
			// output to the buffer.
			//
			if ((fp = popen(cmd, "r")) != NULL) {
				while (fgets(data_buf, 256, fp) != NULL) {
					cl_exec_log(CLADM_GREEN,
					    "ptree %u:%s\n", sinfo.si_pid,
					    data_buf);
				}
				(void) pclose(fp);
			}
		}
		//
		// When we get SIGTERM log the fact so that we will know when
		// we terminate normally versus abnormally
		//
		if (sig == SIGTERM) {
			//
			// SCMSGS
			// @explanation
			// The cl_execd program got a SIGTERM signal and is
			// exiting.
			// @user_action
			// This is an informational message. No user action
			// is required.
			//
			do_exit(0, SC_SYSLOG_MESSAGE("cl_execd: Going down on "
			    "signal %d."), sig);
			/* NOTREACHED */
		}
		//
		// SCMSGS
		// @explanation
		// The cl_execd program got an unexpected signal and is
		// exiting.
		// @user_action
		// Contact your authorized Sun service provider to
		// determine whether a workaround or patch is available.
		//
		do_exit(1, SC_SYSLOG_MESSAGE("cl_execd: Got an unexpected"
		    " signal %d (pid=%d, ppid=%d)"), sig, getpid(),
		    getppid());
	}
	/* NOTREACHED */
	return ((void *)NULL);
}
