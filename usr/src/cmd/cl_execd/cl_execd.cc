//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)cl_execd.cc	1.6	08/07/28 SMI"

#include <nslib/ns.h>
#include <fcntl.h>
#include <sys/cladm_debug.h>
#include <cl_exec_impl.h>
#include <cl_execd.h>

//
// This file defines the remote execution service daemon. The daemon process
// hosts the CORBA server object that handles task execution requests from
// the clients. These requests are sent through a door call to the
// worker process that is the door server. The results of the task execution
// is returned back to us by the door server which is then passed on to the
// client.
//
#include <sys/sol_version.h>
#if (SOL_VERSION >= __s10)
#include <zone.h>
#endif

//
// Door descriptor.
//
extern	int	door_fd;

int	common_fd;

//
// The cl_exec daemon process. Here we create two threads, one to handle
// signals and the other to handle requests to execute tasks.
//
void
daemon_process()
{
	cl_exec_log(CLADM_GREEN, "Daemon: starting daemon process\n");

	//
	// Mask the signals.
	//
	block_allsignals();

	remote_exec::cl_exec_var	cl_exec_v;
	Environment			env;
	thread_t			sig_tid;
	char				*srv_name = get_srv_name();
	CORBA::Exception		*exp = NULL;
	naming::naming_context_var	ctx_v	= ns::root_nameserver();

	ASSERT(!CORBA::is_nil(ctx_v));

	//
	// Create and arm a failfast object so that if cl_execd dies, the
	// node will be forced to panic.
	//
	create_and_arm_failfast("cl_execd");

	//
	// Get an fd to '/dev/zero' to pass to all exec'ing processes.
	// We should do this before binding with the name server because as
	// soon as the bind is done, we may be executing programs.
	//
	if ((common_fd = open("/dev/zero", O_RDWR)) == -1) {
		//
		// SCMSGS
		// @explanation
		// cl_execd has encountered a failed open(2) system call.
		// The error message indicates the error number for the
		// failure.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		do_exit(errno, SC_SYSLOG_MESSAGE("cl_execd: Error %d from "
		    "open(/dev/zero).  Exiting."), errno);
	}

	//
	// Before we go ahead, use up stdin, stdout, stderr
	// This also has the effect of closing these descriptors.
	//
	(void) dup2(common_fd, 0);
	(void) dup2(common_fd, 1);
	(void) dup2(common_fd, 2);

	// Daemonize.
	(void) setsid();

	//
	// Create a thread that handles any signal and logs an
	// appropriate message.
	//
	cl_exec_log(CLADM_GREEN, "Daemon: create signals thread\n");

	if (thr_create(NULL, NULL, wait_for_signals, NULL, THR_DETACHED,
	    &sig_tid) != 0) {
		//
		// SCMSGS
		// @explanation
		// The cl_execd program encountered a failure while executing
		// thr_create(3C).
		// @user_action
		// Contact your authorized Sun service provider to
		// determine whether a workaround or patch is available.
		//
		do_exit(errno, SC_SYSLOG_MESSAGE("cl_execd: Unable to create "
		    "thread. Exiting.\n"));
	}

	//
	// Now we are ready to accept connection. Register the cl_exec
	// object with the name server and stay around to process requests.
	//
	cl_exec_v = (new cl_exec_impl)->get_objref();
	cl_exec_log(CLADM_GREEN, "Daemon: bind server object <%s>\n", srv_name);

	ctx_v->rebind(srv_name, cl_exec_v, env);

	exp = env.exception();
	if (exp != NULL) {
		//
		// Rebind failed. We will try to unbind and try binding again.
		//
		cl_exec_log(CLADM_GREEN, "Daemon: rebind of server object "
		    "failed\n");
		env.clear();
		ctx_v->unbind(srv_name, env);

		if ((exp = env.exception()) != NULL) {

			cl_exec_log(CLADM_RED, "Daemon: unbind of sever object"
			    " failed\n");
			//
			// SCMSGS
			// @explanation
			// The cl_execd program could not unbind the corba
			// object from the global name server.
			// @user_action
			// Contact your authorized Sun service provider to
			// determine whether a workaround or patch is
			// available.
			//
			do_exit(1, SC_SYSLOG_MESSAGE("Error while unbinding"
			    " '%s' in the name server.  Exiting."), srv_name);
		}
		ctx_v->bind(srv_name, cl_exec_v, env);

		if ((exp = env.exception()) != NULL) {
			cl_exec_log(CLADM_RED, "Daemon: bind of server object "
			    "failed\n");
			//
			// SCMSGS
			// @explanation
			// The cl_execd program coudn't bind the corba object
			// from the global name server.
			// @user_action
			// Contact your authorized Sun service provider to
			// determine whether a workaround or patch is
			// available.
			//
			do_exit(1, SC_SYSLOG_MESSAGE("Error while binding"
			    " '%s' in the name server.  Exiting."), srv_name);
		}
	}
	delete [] srv_name;
	thr_exit(0);
	// NOT REACHED
}

//
// This routine sends the program to be executed to the worker process, and
// waits for the result to be sent back. It uses a door call as the IPC
// mechanism to reach the server.
// Arguments:
//	command:	The task to be executed.
//	need_output:	If true, the stdout and stderr of the task are
//			are saved and returned back to the caller.
//	cid:		The scheduling class of the task to be executed.
//	pri:		priority of the task in the given scheduling class.
//	task_rtval:	Return value of the task execution.
// Return Value:
//	0 on Success
//	Failure:
//		E2BIG || errno
//
int
execute_cmd(const char *command, bool need_output,
    remote_exec::sched_class_t cid, int pri, int &task_rtval,
    char **stdout_str, char **stderr_str)
{
	door_arg_t		darg;
	int			ret_val;
	task_buf_t		cmd_msg;
	result_buf_t		cmd_result;

	ASSERT(command != NULL);
	(void) strncpy(cmd_msg.cmd, command, MAXBUF);
	cmd_msg.pri = pri;
	cmd_msg.cid = cid;
	cmd_msg.need_output = need_output;

	//
	// Prepare the door arguments before making the door call
	//
	darg.data_ptr = (char *)&cmd_msg;
	darg.data_size = sizeof (task_buf_t);
	darg.desc_num = 0;
	darg.desc_ptr = NULL;
	darg.rbuf = (char *)&cmd_result;
	darg.rsize = sizeof (cmd_result);

	//
	// The door call can fail with EINTR if interrupted by a signal
	// or if the door server dies unexpectedly. The signals are
	// already masked for this thread. If the server dies unexpecedly,
	// we will pass this error to the client to handle.
	//
	ret_val = door_call(door_fd, &darg);

	cl_exec_log(CLADM_GREEN, "Daemon: Door call returned = %d\n", ret_val);

	//
	// Door call succeeded. Retrieve the results of the command execution.
	// We further check if the command execution succeded before retrieving
	// the results.
	//
	if (ret_val == 0) {

		cl_exec_log(CLADM_GREEN, "Daemon: ret size = %d\n", darg.rsize);

		// Make lint happy.
		cmd_result = *((result_buf *)((void *)darg.rbuf));
		int cmd_retval = cmd_result.err_val;

		cl_exec_log(CLADM_GREEN, "cmd result = %d\n", cmd_retval);
		//
		// The exit value of the command is valid only if
		// there were no errors in executing the command by
		// the worker process (door server).
		//
		if (cmd_retval == 0) {
			task_rtval = cmd_result.ret_val;
			//
			// Retrieve the stdout and stderr of the command
			// execution if needed.
			//
			if (need_output) {
				ASSERT(stdout_str != NULL);
				ASSERT(stderr_str != NULL);
				*stdout_str = strdup(cmd_result.output);
				*stderr_str = strdup(cmd_result.erroutput);
			}
		}
	}
	return (ret_val);
}
