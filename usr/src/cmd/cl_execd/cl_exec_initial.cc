//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)cl_exec_initial.cc	1.6	08/07/28 SMI"

//
// This file defines the main process for the remote execution service.
// The main process defines the first generation of processes for this
// service. The main process forks off a second generation process and waits
// for the service to initialize. The main process exits after the service
// initialization is complete and marks the service to be available. The second
// generation process forks off the remote execution service daemon. The remote
// execution service daemon is the third generation of processes and hosts the
// CORBA server object to handle task execution requests. The second generation
// process functions as a worker process that hosts the door server and handles
// requests from the daemon (door client). The worker process forks and execs
// the tasks that form the fourth generation of processes in the process
// hierarchy.
//

#include <orb/infrastructure/orb.h>
#include <nslib/ns.h>
#include <sys/cladm_debug.h>
#include <cl_exec_impl.h>
#include <cl_execd.h>
#include <sys/sol_version.h>

#if (SOL_VERSION >= __s10)
#include <zone.h>
zoneid_t	zoneid;
#endif // SOL_VERSION

//
// mutex needed for synchronizing logging.
//
mutex_t dbg_mutex;

// Deafult scheduling class.
remote_exec::sched_class_t default_cid;

extern void create_process_pair(void);
extern idtype_t get_proc_classid(void);
extern idtype_t get_classid(const char *);
extern void set_rt_prio(int);

#define	MILLI_SEC	1000

#include <sys/sc_syslog_msg.h>
#include <alloca.h>

//
// Helper function to build the name of the cl_exec server object.
//
char *
get_srv_name(void)
{
	char	*name	= NULL;
#if (SOL_VERSION >= __s10)

	//
	// Build the namespace from "cl_exec.<nodeid>.<zone>"
	//
	char	zonename[ZONENAME_MAX];

	name = new char[ZONENAME_MAX + 12];

	if (name == NULL) {
		//
		// SCMSGS
		// @explanation
		// Could not allocate memory. Node is too low on memory.
		// @user_action
		// cl_execd program will exit and node will be halted or
		// rebooted to prevent data corruption. Contact your
		// authorized Sun service provider to determine whether
		// a workaround or patch is available.
		//
		do_exit(errno,  SC_SYSLOG_MESSAGE(
		    "cl_execd: Could not allocate memory size = %d"),
		    ZONENAME_MAX + 12);
	}

	//
	// If running in the global zone, the name will be
	// "cl_exec.<nodeid>". If running in the non-global
	// zone, the name will be "cl_exec.<nodeid>.<zonename>
	//
	if (zoneid == GLOBAL_ZONEID) {
		os::sprintf(name, "cl_exec.%d", orb_conf::node_number());
	} else {
		//
		// SCMSGS
		// @explanation
		// cl_execd program encountered a failure while executing
		// the getzonenamebyid(3C) command.  The error message
		// indicates the error number for the failure.
		// @user_action
		// cl_execd program will exit and the node will be halted or
		// rebooted to prevent data corruption. Contact your
		// authorized Sun service provider to determine whether
		// a workaround or patch is available.
		//
		if (getzonenamebyid(zoneid, zonename, sizeof (zonename)) < 0) {
			do_exit(1,
			    SC_SYSLOG_MESSAGE("Could not get the zone name."
			    " Exiting."));
		}
		os::sprintf(name, "cl_exec.%d.%s", orb_conf::node_number(),
		    zonename);
	}
#else
	name = new char[20];

	if (name == NULL) {
		//
		// scmsgs same as year before.
		//
		do_exit(errno,  SC_SYSLOG_MESSAGE(
		    "cl_execd: Could not allocate memory size = %d"), 20);
	}
	os::sprintf(name, "cl_exec.%d", orb_conf::node_number());
#endif
	return (name);
}

//
// Wait till the cl_execd daemon has started. We do this by resolving
// the the cl_exec object in the global name server. The cl_execd daemon
// will be ready to accept client requests after it registers this object
// in the name server.
//
static void
wait_for_daemon()
{
	remote_exec::cl_exec_var	cl_exec_v;
	CORBA::Object_var		obj_v;
	Environment			env;
	CORBA::Exception		*exp;
	naming::naming_context_var	ctx_v	= ns::root_nameserver();
	char				*srv_name = get_srv_name();

	ASSERT(srv_name != NULL);

	//
	// The parent process waits for the child to register an
	// IDL object with the name server and then exits so
	// the shell will think the command is finished.
	//
	cl_exec_log(CLADM_GREEN, "Main: cl_exec server object : <%s>\n",
	    srv_name);

	//
	// Wait till the server object is registered with the name server.
	//
	for (;;) {

		obj_v = ctx_v->resolve(srv_name, env);

		if ((exp = env.exception()) != NULL) {
			if (naming::not_found::_exnarrow(exp) == NULL) {
				do_exit(1, SC_SYSLOG_MESSAGE("Could not resolve"
				    " '%s' in the name server.  Exiting."),
				    srv_name);
			}
			// Wait and then try again.
			env.clear();
		} else {
			//
			// Test to see that the object we got from the
			// name server is active. If not, wait for
			// the child process to register the new
			// object.
			//
			cl_exec_v = remote_exec::cl_exec::_narrow(obj_v);

			ASSERT(!CORBA::is_nil(cl_exec_v));

			bool alive = cl_exec_v->is_alive(env);
			if (env.exception() == NULL && alive) {
				break;
			}
			env.clear();
		}
		//
		// Wait for 100 milliseconds.
		//
		os::usecsleep(MILLI_SEC*100);
		cl_exec_log(CLADM_GREEN, "Main: wait for cl_exec obj\n");
	}
	delete [] srv_name;
	cl_exec_log(CLADM_GREEN, "Main: cl_exec obj resolved in name server\n");
}

//
// Create the daemon process and wait for it to be ready to accept client
// requests in the parent. The parent then exists which indicates that the
// cl_execd service is up.
//
static void
daemonize()
{
	pid_t  proc_pid;

	//
	// Initialize ORB.
	//
	// We pass in a parameter to ORB::initialize() that causes it
	// not to increase the limit on open files to RLIM_INFINITY.
	// We need to do this because one of the programs cl_exec
	// interface exec's is the SDS metaset command. The SDS metaset
	// command tries to cleanup all open fds by closing fds from 3
	// to the maximum possible.  If ORB::initialize() sets the
	// file limit to RLIM_INFINITY, then this takes the better part
	// of the day to do.
	//
	if (ORB::initialize(false) != 0) {
		do_exit(1, SC_SYSLOG_MESSAGE("Could not initialize "
		    "the ORB. Exiting."));
	}

	remote_exec::cl_exec_var	cl_exec_v;
	CORBA::Object_var		obj_v;
	Environment			env;
	CORBA::Exception		*exp;
	naming::naming_context_var	ctx_v	= ns::root_nameserver();
	char				*srv_name = get_srv_name();

	if (CORBA::is_nil(ctx_v)) {
		cl_exec_log(CLADM_GREEN, "Failed to get context\n");
		//
		// TODO: Need to decide what to do for
		// release code. For debug code, assert
		//
		ASSERT(0);
	}

	//
	// The child process checks to see if there is already a process
	// running and doesn't start another.
	//
	obj_v = ctx_v->resolve(srv_name, env);

	if ((exp = env.exception()) != NULL) {
		if (naming::not_found::_exnarrow(exp) == NULL) {
			cl_exec_log(CLADM_RED,
			    "Daemon: Exception %s occured\n", exp->_name());
			//
			// SCMSGS
			// @explanation
			// The cl_execd program coudn't resolve the cl_exec
			// server object from the global name server.
			// @user_action
			// Contact your authorized Sun service provider to
			// determine whether a workaround or patch is
			// available.
			//
			do_exit(1, SC_SYSLOG_MESSAGE("cl_execd: Could not "
			    "resolve '%s' in the name server. Exiting."),
			    srv_name);
		}
		env.clear();
	} else {
		//
		// If the object is found and is active, flag an error
		// and exit.
		//
		cl_exec_v = remote_exec::cl_exec::_narrow(obj_v);
		ASSERT(!CORBA::is_nil(cl_exec_v));
		bool alive = cl_exec_v->is_alive(env);
		if (env.exception() == NULL && alive) {
			//
			// SCMSGS
			// @explanation
			// The cl_execd program found another active cl_execd
			// program already in execution.
			// @user_action
			// This is an information message. No user action
			// is required.
			//
			do_exit(0, SC_SYSLOG_MESSAGE("Found another active"
			    " instance of cl_execd. Exiting cl_execd."));
		}
	}

	//
	// We need a way to declare the cl_execd service as online
	// or on S9 let the rc scripts complete the bootup. Since cl_execd
	// service is a daemon that does not go away, we fork off a new
	// process that does the actual work of the cl_execd service.
	// The parent waits till the initialization is complete and the
	// service is ready to accept client requests and then goes away.
	// Based on the return status of the parent, SMF changes the
	// state of the service.
	//
	(void) mutex_lock(&dbg_mutex);
	proc_pid = fork1();
	(void) mutex_unlock(&dbg_mutex);

	if (proc_pid == (pid_t)-1) {
		//
		// SCMSGS
		// @explanation
		// The cl_execd program has encountered failure of fork1(2)
		// system call. The error message indicates the error number
		// for the failure.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		do_exit(errno, SC_SYSLOG_MESSAGE("cl_execd: fork1 returned %d."
		    "  Exiting."), errno);
	} else if (proc_pid == 0) {
		//
		// The daemon and worker processes will run in RT for a better
		// response time.
		//
		set_rt_prio(0);
		create_process_pair();
		/* NOTREACHED */
	} else {
		// Parent.
		// We exit after making sure the daemon is up and ready
		// to process its client requests.
		//
		cl_exec_log(CLADM_GREEN, "Main: wait for daemon to be ready\n");
		wait_for_daemon();
		cl_exec_log(CLADM_GREEN, "Main: daemon is ready\n");
	}
}

//
// Main function. Start the daemon service here.
//
int
main()
{
	//
	// Record the scheduling class as the default scheduling class
	// to execute the tasks.
	//
	idtype_t cid = get_proc_classid();

	if (cid == get_classid("RT")) {
		default_cid = remote_exec::RT;
	} else if (cid == get_classid("TS")) {
		default_cid = remote_exec::TS;
	} else if (cid == get_classid("FSS")) {
		default_cid = remote_exec::FSS;
	} else if (cid ==  get_classid("FX")) {
		default_cid = remote_exec::FX;
	} else {
		//
		// The class may be interactive (IA) or some class
		// we dont know about. We go ahead and set the
		// default scheduling class to be Time shared.
		//
		default_cid = remote_exec::TS;
	}

	cl_exec_log(CLADM_GREEN, "Main: Default sched class = %d\n",
	    default_cid);

#if (SOL_VERSION >= __s10)
	zoneid = getzoneid();
	//
	// If the daemon is running in a non-global zone initialize
	// logging. In this case, we log to a file instead of the
	// kernel debug buffer if the deamon is running in the global
	// zone.
	//
	if (zoneid != GLOBAL_ZONEID) {
		log_init();
	}
#endif
	(void) mutex_init(&dbg_mutex, USYNC_THREAD, 0);

	cl_exec_log(CLADM_GREEN, "Main: starting the cl_exec service\n");

	//
	// Create the daemon service.
	//
	daemonize();

	cl_exec_log(CLADM_GREEN, "Main: service is online\n");
	return (0);
}
