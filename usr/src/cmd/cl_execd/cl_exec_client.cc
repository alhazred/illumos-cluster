//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)cl_exec_client.cc	1.7	08/07/28 SMI"

//
// This program executes a command on all the nodes of a cluster or all zones
// in a Zone cluster. Optionally, the command can be run on a specific
// subset of nodes or zones. It then generates as output the exit status of
// the execution of the command on each node/zone and the standard output and
// standard error that the command generates on each node/zone.
// The following options are valid:
//	-z zonename		If a zonename is specified, the command is
//				run on the zone cluster represented by the
//				zonename.
//	-C { TS | RT | FSS | FX }
//				The scheduling class in which the command is
//				to be run. By default the command is executed
//				in the default scheduling class of the
//				remote execution service. The command can
//				be run in Time shared (TS), Real time (RT)
//				,Fair Share (FSS) or Fixed priority (FX).
//	-p pri			This specifies the priority of the command
//				in the given scheduling class.
//	-n id[,id..]		A comma seperated list of node ID's of a
//				zone cluster or a node to run the command.
//				Not specifying this option will result in the
//				command running on all nodes/zones.
//	-c cmd [Args]		The command to be run along with its arguments.
//

#include <orb/infrastructure/orb_conf.h>
#include <orb/infrastructure/orb.h>
#include <sys/os.h>
#include <nslib/ns.h>
#include <orb/object/adapter.h>
#include <libintl.h>
#include <h/remote_exec.h>
#include <sys/clconf_int.h>
#include <sys/clconf.h>
#include <sys/sol_version.h>
#include <cl_execd.h>

//
// Globals.
//
#if (SOL_VERSION >= __s10)
#include <zone.h>

static char		curr_zonename[ZONENAME_MAX];
static zoneid_t		zoneid;
#endif // SOL_VERSION

nodeid_t	node_list[NODEID_MAX];
uint_t		nodecnt = 0;

//
// Print the usage.
//
static void
print_usage()
{
	(void) printf("Usage: cl_exec_client [-z zonename] [-C { TS | RT "
	    " | FSS | FX } [ -p pri ] ] [-n nid,nid,..] -c cmd [Args]\n");
}

//
// Parse the comma seperated list of node ID's and populate the
// global node list.
//
static int
commalist_to_nodelist(char *nodelist)
{
	char		*nodeid;
	const char	*delimiter = ",";
	nodeid_t	nid;

	//
	// For each node id listed in the commalist, add it to
	// the list of nodes.
	//
	nodeid = (char *)strtok(nodelist, delimiter);

	while (nodeid != NULL) {
		nid = (nodeid_t)atoi(nodeid);
		if ((nid < 1) || (nid > NODEID_MAX)) {
			(void) printf(gettext("Invalid node ID encountered\n"));
			return (1);
		}
		node_list[nodecnt] = nid;
		nodecnt++;
		nodeid = (char *)strtok(NULL, delimiter);
	}
	return (0);
}

//
// Generate the name of the cl_exec server object given the node/zoneid.
//
static char *
get_clexec_name(char *trgt_zone, nodeid_t node)
{
	char	*clexec_name	= NULL;

#if (SOL_VERSION >= __s10)

	//
	// Build the namespace from "cl_exec.<nodeid>.<zone>"
	//
	clexec_name = new char[ZONENAME_MAX + 12];

	if (clexec_name == NULL) {
		(void) printf(gettext("Could not allocate memory size = %d"),
		    ZONENAME_MAX + 12);
		exit(1);
	}
	//
	// If running in the global zone, the name will be
	// "cl_exec.<nodeid>". If running in the non-global
	// zone, the name will be "cl_exec.<nodeid>.<zonename>
	//
	if (trgt_zone != NULL) {
		//
		// If we are running in a non-global zone, check if the
		// zonename on which the command is to be run is part of
		// the same virtual cluster.
		//
		if (zoneid != GLOBAL_ZONEID) {
			if (os::strcmp(curr_zonename, trgt_zone) != 0) {
				(void) printf(gettext("Cannot execute command "
				    " in another zone cluster\n"));
				exit(1);
			}
		}
		os::sprintf(clexec_name, "cl_exec.%d.%s", node, trgt_zone);
	} else {
		if (zoneid == GLOBAL_ZONEID) {
			os::sprintf(clexec_name, "cl_exec.%d", node);
		} else {
			os::sprintf(clexec_name, "cl_exec.%d.%s", node,
			    curr_zonename);
		}
	}
#else
	clexec_name = new char[20];

	if (clexec_name == NULL) {
		(void) printf(gettext("Could not allocate memory size = %d"),
		    20);
		exit(1);
	}
	os::sprintf(clexec_name, "cl_exec.%d", node);
#endif
	return (clexec_name);
}

//
// Execute the command remotely on another node/zone of a physical
// cluster/zone cluster and output the return value, stdout and
// stderr of the command executed.
// Arguments:
//	cmd:		Command to execute.
//	clexec_name:	Name of the CORBA server Object of the remote
//			execution infrastructure on the remote node/zone.
//
static int
execute_cmd(nodeid_t nid, const char *cmd, remote_exec::sched_class_t cid,
    int pri, const char *clexec_name)
{

	remote_exec::cl_exec_var	cl_exec_v;
	naming::naming_context_var	ctx_v = ns::root_nameserver();
	CORBA::Object_var		obj_v;
	Environment			env;
	CORBA::Exception		*exp;

	ASSERT(cmd != NULL);
	ASSERT(clexec_name != NULL);

	obj_v = ctx_v->resolve(clexec_name, env);

	if ((exp = env.exception()) != NULL) {
		if (naming::not_found::_exnarrow(exp) != NULL) {
			(void) printf(gettext("Nodeid : %d, Invalid "
			    " node ID or remote execution service not "
			    "up\n"), nid);
			return (1);
		}
	} else {
		//
		// Test to see that the object we got from the
		// name server is active.
		//
		cl_exec_v = remote_exec::cl_exec::_narrow(obj_v);
		ASSERT(!CORBA::is_nil(cl_exec_v));
		(void) cl_exec_v->is_alive(env);
		if (env.exception() != NULL) {
			(void) printf(gettext("The remote execution server "
			    "object is inactive.\n"));
			env.clear();
			exit(1);
		}
	}

	int	ret_val;
	char	*stdout_str = NULL;
	char	*stderr_str = NULL;
	int	optval;

	optval = cl_exec_v->exec_program_get_output(ret_val, stdout_str,
	    stderr_str, cmd, cid, pri, env);

	if ((exp = env.exception()) != NULL) {
		(void) printf(gettext("Exception occurred during remote "
		    "execution, %s"), exp->_name());
		env.clear();
		exit(1);
	}
	if (optval != 0) {
		(void) printf(gettext("Execution failed in "
		    "exec_program_get_output, retval = %d\n"), optval);
		return (1);
	}

	(void) printf(gettext("\nNode: %d\n"), nid);

	(void) printf(gettext("Task return value: %d\n"), ret_val);

	if (stdout_str != NULL) {
		(void) printf(gettext("Task stdout: %s\n"), stdout_str);
		delete [] stdout_str;
	}
	if (stderr_str != NULL) {
		(void) printf(gettext("Task stderr: %s\n"), stderr_str);
		delete [] stderr_str;
	}
	return (0);
}

//
// Main routine.
// Return value:
//	0 On success.
//	1 On Failure.
//
/*lint -e818 */
int
main(int argc, char *argv[])
{
	//
	// Initialize ORB.
	//
	if (ORB::initialize() != 0) {
		(void) printf(gettext("Could not initialize the ORB. "
		    "Exiting."));
		exit(1);
	}

#if (SOL_VERSION >= __s10)
	//
	// Get the zoneid and zonename of the zone where we are executing.
	//
	zoneid = getzoneid();

	if (getzonenamebyid(zoneid, curr_zonename,
	    sizeof (curr_zonename)) < 0) {
		(void) printf(gettext("Could not get the Zone name. "
		    "Exiting."));
		exit(1);
	}
#endif

	int	arg;
	//
	// Flag specifying the node/zone on which the command needs to
	// be executed.
	//
	uint_t	nflag		= 0;

	//
	// Flag specifying the command to be executed.
	//
	uint_t	cflag		= 0;
	uint_t	zflag		= 0;
	uint_t  cidflag		= 0;
	char	*clexec_name	= NULL;
	char	*trgt_zone	= NULL;
	char	cmd[MAXBUF];
	remote_exec::sched_class_t default_cid = remote_exec::DEF;
	int	pri		= 0;
	uint_t	indx;

	cmd[0] = 0;

	//
	// Parse the arguments.
	//
	optind = 1;
	while ((arg =  getopt(argc, argv, "n:z:C:p:c:?")) != EOF) {
		// switch on the option.
		switch (arg) {
		case 'n':
			nflag = 1;
			if ((optarg == NULL) || (*optarg == NULL)) {
				print_usage();
				return (1);
			}
			if (commalist_to_nodelist(optarg) != 0) {
				return (1);
			}
			break;
		case 'c':
			cflag = 1;
			if ((optarg == NULL) || (*optarg == NULL)) {
				print_usage();
				return (1);
			}
			(void) strncpy(cmd, optarg, MAXBUF - 1);
			break;
		case 'z':
			if (zflag) {
				//
				// If a target zone has already been specified
				// do not allow another.
				//
				print_usage();
				return (1);
			} else {
				zflag = 1;
			}
			if ((optarg == NULL) || (*optarg == NULL)) {
				print_usage();
				return (1);
			}
			trgt_zone = strdup(optarg);
			break;
		case 'C':
			cidflag = 1;
			if ((optarg == NULL) || (*optarg == NULL)) {
				print_usage();
				return (1);
			}
			if (strcmp(optarg, "TS") == 0) {
				default_cid = remote_exec::TS;
			} else if (strcmp(optarg, "FSS") == 0) {
				default_cid = remote_exec::FSS;
			} else if (strcmp(optarg, "RT") == 0) {
				default_cid = remote_exec::RT;
			} else if (strcmp(optarg, "FX") == 0) {
				default_cid = remote_exec::FX;
			} else {
				os::printf(gettext("Invalid or unknown "
				    "scheduling class\n"));
				return (1);
			}
			break;
		case 'p':
			if (!cidflag) {
				print_usage();
				return (1);
			}
			if ((optarg == NULL) || (*optarg == NULL)) {
				print_usage();
				return (1);
			}
			pri = atoi(optarg);
			break;
		case '?':
		default:
			print_usage();
			return (1);
		}
		//
		// If we have already seen a -c argument, then
		// we have seen the command and the rest of the arguments
		// are part of the command so we do not have to process them.
		//
		if (cflag) {
			break;
		}
	}

	//
	// The command option should be specified.
	//
	if (!cflag) {
		print_usage();
		return (1);
	}

	//
	// Build the command to be executed from the rest of the arguments.
	//
	argc -= optind;
	argv += optind;

	for (indx = 0; indx < (uint_t)argc; ++indx) {
		if ((strlen(cmd) + 1 + strlen(argv[indx])) > MAXBUF) {
			(void) printf(gettext("Command length too long\n"));
			return (1);
		}
		(void) strcat(cmd, " ");
		(void) strcat(cmd, argv[indx]);
	}

	if (nflag) {
		for (indx = 0; indx < nodecnt; ++indx) {
			clexec_name = get_clexec_name(trgt_zone,
			    node_list[indx]);
			//
			// If the node/zone corresponding to the node ID
			// is not up or does not exist we exit here.
			//
			if (execute_cmd(node_list[indx], cmd, default_cid,
			    pri, clexec_name) != 0) {
				return (1);
			}
			delete [] clexec_name;
		}
	} else {
		for (indx = 1; indx < NODEID_MAX; ++indx) {
			//
			// If the node is not configured as part of the
			// cluster, skip the node ID. If there are no
			// physical nodes with the node ID in the cluster
			// we cant have a virtual cluster with that node ID
			//
			if (!orb_conf::node_configured(indx)) {
				continue;
			}
			clexec_name = get_clexec_name(trgt_zone, indx);
			//
			// If the node/zone corresponding to the node ID
			// is not up or does not exist we try other
			// node ID's. This is because the intention was to
			// execute the command on all the nodes/zones of
			// the cluster/CZ and in the case of CZ there
			// is no way to check valid virtual node ID's.
			//
			(void) execute_cmd(indx, cmd, default_cid, pri,
			    clexec_name);
			delete [] clexec_name;
		}
	}
	return (0);
}
