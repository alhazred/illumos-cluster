//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)cl_exec_worker.cc	1.8	08/07/28 SMI"

#include <sys/cladm_debug.h>
#include <sys/wait.h>
#include <cl_execd.h>
#include <stropts.h>
#include <pthread.h>

//
// This file defines the worker process. This process starts off as a second
// generation process that forks and creates a daemon process and transorms
// itself as a door server to handle task execution requests from the daemon.
// The daemon is the only door client for the worker process.
//

//
// The worker process forks and creates a daemon process as a child.
//
extern void daemon_process(void);
extern void set_task_priority(remote_exec::sched_class_t, int);

//
// mutex needed for synchronizing logging.
//
extern mutex_t dbg_mutex;

//
// File descriptor of '/dev/zero'. If the task that is exec'ed expects some
// input from stdin, it reads from /dev/zero.
//
int common_fd;

//
// Door server descriptor.
//
int door_fd;

//
// Helper function to associate stdout and stderr with stream descriptors
// for the current process.
//
static void
graft_fds(int t_stdin, int t_stdout, int t_stderr,
    const task_buf_t *cmd_msg)
{
	//
	// Graft stdin' , 'stdout', and 'stderr' in place.
	//
	if ((dup2(t_stdin, 0)) < 0) {
		//
		// SCMSGS
		// @explanation
		// cl_execd program has encountered a failed dup2(2) system
		// call. The error message indicates the error number for the
		// failure.
		// @user_action
		// The cl_execd program will exit and the node will be halted
		// to prevent data corruption. Contact your authorized Sun
		// service provider to determine whether a workaround or
		// patch is available.
		//
		do_exit(errno, SC_SYSLOG_MESSAGE("cl_execd: dup2 of "
		    "stdin returned with errno %d while executing (%s)."
		    "  Exiting."), errno, cmd_msg->cmd);
	}
	(void) close(t_stdin);

	if ((dup2(t_stdout, 1)) < 0) {
		//
		// SCMSGS
		// @explanation
		// cl_execd program has encountered a failed dup2(2) system
		// call. The error message indicates the error number for the
		// failure.
		// @user_action
		// The cl_execd program will exit and the node will be halted
		// to prevent data corruption. Contact your authorized Sun
		// service provider to determine whether a workaround or
		// patch is available.
		//
		do_exit(errno, SC_SYSLOG_MESSAGE("cl_execd: dup2 of "
		    "stdout returned with errno %d while exec'ing (%s)."
		    "  Exiting."), errno, cmd_msg->cmd);
	}
	(void) close(t_stdout);

	if ((dup2(t_stderr, 2)) < 0) {
		//
		// SCMSGS
		// @explanation
		// cl_execd program has encountered a failed dup2(2) system
		// call. The error message indicates the error number for the
		// failure.
		// @user_action
		// The cl_execd program will exit and the node will be halted
		// to prevent data corruption. Contact your authorized Sun
		// service provider to determine whether a workaround or
		// patch is available.
		//
		do_exit(errno, SC_SYSLOG_MESSAGE("cl_execd: dup2 of "
		    "stderr returned with errno %d while exec'ing (%s)."
		    "  Exiting."), errno, cmd_msg->cmd);
	}
	(void) close(t_stderr);
}

//
// This is the function that does all the work of forking the task.
// If the request is for storing the output the task generates,
// (This information is embedded in the command buffer) the two file
// descriptors needed are retrieved from the command buffer and associated
// with stdout/stderr of the process. We then fork and exec the task.
// The result of this execution is stored back into the command buffer.
//
void
server_execute_cmd(task_buf_t *cmd_msg, result_buf_t *cmd_result)
{
	pid_t			proc_pid;
	pid_t			child_pid;
	int			err;
	int			ret_val		=  0;
	// Keep lint happy.
	int			stdout_fds[2]	= {-1, -1};
	int			stderr_fds[2]	= {-1, -1};
	int			t_stdout	= common_fd;
	int			t_stderr	= common_fd;
	char			*stdoutstr	= NULL;
	char			*stderrstr	= NULL;
	thread_t		stdout_tid;
	thread_t		stderr_tid;

	ASSERT(cmd_msg != NULL);

	//
	// Setup a stream for stdout and stderr of the task to be executed
	// if needed.
	//
	if (cmd_msg->need_output) {
		//
		// Setup stream for stdin of the task to be executed.
		//
		if (pipe(stdout_fds) != 0) {
			err = errno;
			//
			// SCMSGS
			// @explanation
			// cl_execd program has encountered a failed pipe(2)
			// system call. The error message indicates the error
			// number for the failure.
			// @user_action
			// Contact your authorized Sun service provider to
			// determine whether a workaround or patch is available.
			//
			log_error(SC_SYSLOG_MESSAGE("cl_execd: Error %d from "
			    "pipe"), err);
			cmd_result->err_val = err;
			return;
		}
		//
		// Setup stream for stdin of the task to be executed.
		//
		if (pipe(stderr_fds) != 0) {
			err = errno;
			//
			// Same scmsgs explanation as in the earlier instance.
			//
			log_error(SC_SYSLOG_MESSAGE("cl_execd: Error %d from "
			    "pipe"), err);
			cmd_result->err_val = err;
			return;
		}
		//
		// The standard output and standard error generated by the
		// command will be mapped to the write end of the stream
		// pipes.
		//
		t_stdout = stdout_fds[1];
		t_stderr = stderr_fds[1];
	}

	cl_exec_log(CLADM_GREEN, "Worker: task_thread fork1 <%s> \n",
	    cmd_msg->cmd);

	(void) mutex_lock(&dbg_mutex);

	//
	// Fork off the child process to execute the command.
	//
	proc_pid = fork1();
	err = errno;
	(void) mutex_unlock(&dbg_mutex);

	if (proc_pid == 0) {
		//
		// If we need the output of the command that we execute,
		// we associate the stream file descriptors with stdout and
		// stderr.
		//
		if (cmd_msg->need_output) {
			graft_fds(common_fd, t_stdout, t_stderr, cmd_msg);
		}

		//
		// Set the priority of the task as requested by the client.
		//
		set_task_priority(cmd_msg->cid, cmd_msg->pri);

		cl_exec_log(CLADM_GREEN, "Task: exec task <%s>\n",
		    cmd_msg->cmd);

		//
		// Execute the command here.
		//
		(void) execl("/bin/sh", "sh", "-c", cmd_msg->cmd, (char *)0);

		//
		// If exec fails, log the appropriate message and exit with
		// that error.
		//
		err = errno;

		//
		// SCMSGS
		// @explanation
		// The cl_execd program failed to execute the task. The error
		// message indicates the error number of failure.
		// @user_action
		// Contact your authorized Sun service
		// provider to determine whether a workaround or patch is
		// available.
		//
		log_error(SC_SYSLOG_MESSAGE("cl_execd: execl returned %d.  "),
		    err);

		cl_exec_log(CLADM_GREEN, "Task: exec task %s exit %d\n",
		    cmd_msg->cmd, err);

		exit(err);

	} else if (proc_pid != (pid_t)-1) {

		//
		// Parent process.
		//

		// The parent closes off unnecessary fd's.
		(void) close(t_stdout);
		(void) close(t_stderr);

		//
		// Here we create two threads, one for stdout and another
		// stderr that read the output generated by the task.
		// This is done only if we are interested in the output
		// of the task.
		//
		if (cmd_msg->need_output) {
			read_fd_buf stdout_fdbuf;
			stdout_fdbuf.fd = stdout_fds[0];
			stdout_fdbuf.op = STDOUT_OP;
			stdout_fdbuf.rbuf = cmd_result;

			if ((err = pthread_create(&stdout_tid, NULL,
			    read_fd, (void *)&stdout_fdbuf)) != 0) {
				cl_exec_log(CLADM_RED, "Worker: stdout_thread "
				    "creation failed  with err = %d.\n", err);
			} else {
				cl_exec_log(CLADM_GREEN, "Worker: "
				    "stdout_thread created, thread id = %d.\n",
				    stdout_tid);
			}
			read_fd_buf stderr_fdbuf;
			stderr_fdbuf.fd = stderr_fds[0];
			stderr_fdbuf.op = STDERR_OP;
			stderr_fdbuf.rbuf = cmd_result;

			if ((err = pthread_create(&stderr_tid, NULL, read_fd,
			    (void *)&stderr_fdbuf)) != 0) {
				cl_exec_log(CLADM_RED, "Worker: stderr_thread "
				    "creation failed  with err = %d.\n", err);
			} else {
				cl_exec_log(CLADM_GREEN, "Worker: "
				    "stderr_thread created, thread id = %d.\n",
				    stderr_tid);
			}
		}
		//
		//
		// Wait for the child to exit.
		//
		do {
			child_pid = waitpid(proc_pid, &ret_val, 0);
		} while ((child_pid < 0) && (errno == EINTR));

		err = errno;

		if (child_pid != proc_pid) {
			//
			// SCMSGS
			// @explanation
			// The cl_execd program has encountered a failed
			// waitpid(3C) system call. The error message indicates
			// the error number of failure.
			// @user_action
			// Contact your authorized Sun service provider to
			// determine whether a workaround or patch is
			// available.
			//
			log_error(SC_SYSLOG_MESSAGE("cl_execd: waitpid "
			    "returned %d.  "), err);
			cl_exec_log(CLADM_RED, "Worker: waitpid returned %d. ",
			    err);
			ret_val = err;
			cmd_result->err_val = ret_val;
		} else if (WIFSIGNALED(ret_val)) {
			//
			// Child received a signal. This is unexpected -
			// return an error to the caller.
			//
			ret_val = ECANCELED;
			cmd_result->err_val = ret_val;
		} else {
			ret_val = WEXITSTATUS(ret_val);	/*lint !e702 */
		}
	} else {
		// fork failed.
		(void) close(t_stdout);
		(void) close(t_stderr);
		//
		// SCMSGS
		// @explanation
		// The cl_execd program has encountered a failed
		// fork1(2) system call. The error message indicates
		// the error number of failure.
		// @user_action
		// Contact your authorized Sun service provider to
		// determine whether a workaround or patch is
		// available.
		//
		log_error(SC_SYSLOG_MESSAGE("cl_execd: fork1 returned %d. "
		    "Returning %d to cl_execd."), err, err);
		cl_exec_log(CLADM_RED, "Worker: fork1 returned %d. "
		    "Returning %d to cl_execd.", err, err);
		ret_val = err;
		cmd_result->err_val = ret_val;
	}

	//
	// Parent process.
	//
	// Record the return value of the command in the buffer.
	//
	cmd_result->ret_val = ret_val;

	//
	// If we are interested in getting the stdout and stdin of the
	// output we read it from the streams here.
	//
	if (cmd_msg->need_output) {
		if ((err = pthread_join(stdout_tid, NULL)) != 0) {
			cl_exec_log(CLADM_RED, "Thread join failed for "
			    "stdout_thread with error = %d.\n", err);
		}
		if ((err = pthread_join(stderr_tid, NULL)) != 0) {
			cl_exec_log(CLADM_RED, "Thread join failed for "
			    "stderr_thread with error = %d.\n", err);
		}
		(void) close(stdout_fds[0]);
		(void) close(stderr_fds[0]);
	}
	cl_exec_log(CLADM_GREEN, "Worker: return value of the task : %d\n",
	    ret_val);
}

//
// The Interface function exported by the door server.
//
void cl_execd_server(void *, char *dataptr, size_t data_size,
    door_desc_t *, size_t)
{
	task_buf_t		*cmd_msg = NULL;
	result_buf_t		cmd_result;

	(void) memset((void *)&cmd_result, 0, sizeof (result_buf_t));

	ASSERT(data_size != 0);

	// Make lint happy by casting to void * first.
	cmd_msg = (task_buf_t *)((void *)dataptr);

	ASSERT(strlen(cmd_msg->cmd) > 0);

	//
	// Execute the command.
	//
	server_execute_cmd(cmd_msg, &cmd_result);

	//
	// Ship across the result back to the door client. The results were
	// put on the same task buffer for simplicity.
	//
	(void) door_return((char *)&cmd_result, sizeof (result_buf_t), NULL, 0);
}

//
// The worker process. All that we do here is to create a thread to catch
// signals and register with failfast. The worker process hosts the door
// server to execute commands remotely.
//
void
worker_process(void)
{
	struct rlimit		fdlim;
	thread_t		sig_tid;

	cl_exec_log(CLADM_GREEN, "Worker: starting\n");

	//
	// Block all the maskable signals.
	//
	block_allsignals();

	//
	// Increase the max. number of fds we can open, we are going to consume
	// a lot of them.
	//
	if (getrlimit(RLIMIT_NOFILE, &fdlim) < 0) {
		cl_exec_log(CLADM_RED, "Worker: getrlimit returned %d\n",
		    errno);
		//
		// SCMSGS
		// @explanation
		// The cl_execd program has encountered a failed
		// getrlimit(2) system call. The error message indicates
		// the error number of failure.
		// @user_action
		// Contact your authorized Sun service provider to
		// determine whether a workaround or patch is
		// available.
		//
		do_exit(errno, SC_SYSLOG_MESSAGE("cl_execd: getrlimit returned"
		    " %d"), errno);
	}

	fdlim.rlim_cur = fdlim.rlim_max;

	if (setrlimit(RLIMIT_NOFILE, &fdlim) < 0) {
		cl_exec_log(CLADM_RED, "Worker: setrlimit returned %d\n",
		    errno);
		//
		// SCMSGS
		// @explanation
		// The cl_execd program has encountered a failed
		// setrlimit(2) system call. The error message indicates
		// the error number of failure.
		// @user_action
		// Contact your authorized Sun service provider to
		// determine whether a workaround or patch is
		// available.
		//
		do_exit(errno, SC_SYSLOG_MESSAGE("cl_execd: setrlimit returned"
		    " %d"), errno);
	}

	//
	// Create a thread that handles any signal and logs an
	// appropriate message.
	//
	cl_exec_log(CLADM_GREEN, "Worker: create signals thread\n");

	if (thr_create(NULL, NULL, wait_for_signals, NULL, THR_DETACHED,
	    &sig_tid) != 0) {
		//
		// SCMSGS
		// @explanation
		// cl_execd program has encountered a failed thr_create(2)
		// system call. The error message indicates the error number
		// for the failure.
		// @user_action
		// Contact your authorized Sun service provider to
		// determine whether a workaround or patch is
		// available.
		//
		do_exit(errno, SC_SYSLOG_MESSAGE("cl_execd: Unable to create "
		    "thread in the worker process. Exiting.\n"));
	}

	//
	// If the worker dies we bring down the node as the cl_execd service
	// will fail.
	//
	create_and_arm_failfast("cl_execd_worker");
	thr_exit(0);
	// NOT REACHED
}

//
// Here we fork to create a daemon process. The worker process (parent) acts
// as a door server and handles requests from the daemon process (child) which
// is the door client.
//
void
create_process_pair(void)
{
	pid_t	daemon_pid;

	int err;

	//
	// We make sure that all the signals are blocked for the door server
	// threads. We do it here so that the threads created by the doors
	// library inherit the signal mask from this thread.
	//
	block_allsignals();
	if ((door_fd = door_create(cl_execd_server, 0, 0)) < 0) {
		cl_exec_log(CLADM_RED, "Worker: Door creation failed\n");
		exit(1);
	}

	//
	// Fork child process. The child will be the daemon process and
	// the parent will go on to be the door server.
	//
	cl_exec_log(CLADM_GREEN, "Worker: create daemon process\n");

	(void) mutex_lock(&dbg_mutex);
	daemon_pid = fork1();
	err = errno;
	(void) mutex_unlock(&dbg_mutex);

	if (daemon_pid == ((pid_t)-1)) {
		//
		// SCMSGS
		// @explanation
		// The cl_execd program has encountered a failed
		// fork1(2) system call. The error message indicates
		// the error number of failure.
		// @user_action
		// Contact your authorized Sun service provider to
		// determine whether a workaround or patch is
		// available.
		//
		do_exit(err, SC_SYSLOG_MESSAGE("fork1 returned %d  "
		    "while creating daemon. Exiting."), err);
	}

	if (daemon_pid == 0) {
		// Child process.
		daemon_process();
	} else {
		// Parent process.
		worker_process();
	}
	/* NOTREACHED */
}
