//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// cl_exec_impl.h
//

#ifndef	_CL_EXEC_IMPL_H
#define	_CL_EXEC_IMPL_H

#pragma ident	"@(#)cl_exec_impl.h	1.5	08/07/17 SMI"

#include <orb/invo/corba.h>
#include <orb/object/adapter.h>
#include <orb/invo/common.h>
#include <h/remote_exec.h>

//
// The CORBA server class definition that defines the interfaces for the
// remote execution infrastructure environment.
// This interface is used by clients who need to execute commands remotely
// on other cluster nodes or clusterized zones.
//
class cl_exec_impl : public McServerof<remote_exec::cl_exec> {

	void _unreferenced(unref_t cookie);

	//
	// Interface to execute command.
	// Arguments:
	//	cmd : Command with its arguments to be executed.
	//	class: The scheduling class for the task to be executed.
	//	This can be any one of the following:
	//		DEF: The default scheduling class of the remote
	//		execution infrastructure service. This is usually
	//		the Time shared class or Fair share scheduling class
	//		if one is configured.
	//		RT:  Real time scheduling class
	//		TS:  Time shared class.
	//		FSS: Fair share scheduling class.
	//		FX:  Fixed scheduling class.
	//	pri:	The priority of the task in the given scheduling
	//		class.
	// Output:
	//	On success, returns the exit status of the command in
	//	retval and 0 as the return value. On failure, returns
	//	a non-zero value (errno).
	//
	int32_t exec_program(int32_t &retval, const char *cmd,
	    remote_exec::sched_class_t cid, int pri, Environment &env);

	//
	// Interface to execute command.
	// Arguments:
	//	stdout_str: Standard output that the command generates.
	//	stderr_str: Standard error that the command generates.
	//	cmd: Command with its arguments to be executed.
	//	class: The scheduling class for the task.
	//	pri: The priority of the task in the given scheduling class.
	// Output:
	//	On success, returns the exit status of the command in
	//	retval and 0 as the return value. On failure, returns a
	//	non-zero value (errno) .
	//
	int32_t exec_program_get_output(int32_t &retval, CORBA::String_out
	    stdout_str, CORBA::String_out stderr_str, const char *cmd,
	    remote_exec::sched_class_t cid, int pri, Environment &env);

	bool is_alive(Environment &);
};

#endif // _CL_EXEC_IMPL_H
