//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)cl_exec_impl.cc	1.6	08/07/17 SMI"

#include <cl_exec_impl.h>
#include <sys/cladm_debug.h>
#include <sys/sol_version.h>

//
// Definition of cl_exec interface functions used by the clients using the
// remote command execution infrastructure.
//
extern void cl_exec_log(int, const char *, ...);
extern int execute_cmd(const char *, bool, remote_exec::sched_class_t, int,
    int &, char **, char **);

//
// This routine returns true if the cl_exec interface is alive and working.
// It can be used to tell if the object reference obtained from the name server
// is valid or not.
//
bool
cl_exec_impl::is_alive(Environment &)
{
	return (true);
}

//
// Unreference notification.
//
void
cl_exec_impl::_unreferenced(unref_t cookie)
{
	(void) _last_unref(cookie);
	cl_exec_log(CLADM_GREEN, "cl_exec_impl: _unreferenced exit\n");
	exit(0);
}

//
// Execute the program specified by 'cmd' in user space.
//
// Arguments:
//	cmd: Command with its arguments to be executed.
//	rt_prio: Specifies if the command execution needs to
//	in real-time.
// Output:
//	On success, returns the exit status of the command in
//	retval and 0 as the function return value. On failure
//	returns a non-zero value (errno).
//
int32_t
cl_exec_impl::exec_program(int32_t &retval, const char *cmd,
    remote_exec::sched_class_t cid, int pri, Environment &)
{
	int32_t	exec_ret_val;

	cl_exec_log(CLADM_GREEN, "cl_exec: Execute task <%s>\n", cmd);

	//
	// Here we are not interested to get the stdout and stderr of the task.
	// We inform the infrastructure that we dont require this info to be
	// passed back to us.
	//
	exec_ret_val = execute_cmd(cmd, false, cid, pri, retval, NULL, NULL);

	if (exec_ret_val != 0) {
		cl_exec_log(CLADM_RED, "Executing task <%s> failed with error "
		    "%d\n", cmd, exec_ret_val);
	} else {
		cl_exec_log(CLADM_GREEN, "cl_exec: Task <%s> success, "
		    "retval = %d\n", cmd, retval);
	}
	return (exec_ret_val);
}

//
// Execute the program specified by 'cmd' in user space.
//
// Arguments:
//	cmd: Command with its arguments to be executed.
//	rt_prio: Specifies if the command execution needs to
//	in real-time.
// Output:
//	On failure, returns an non-zero value. On success returns
//	0.
//	retval: exit status of the command.
//	stdout_str: Standard output that the command generates.
//	stderr_str: Standard error that the command generates.
//
// The caller of this interface has to do the necessary cleanup of the
// memory allocated to these strings.
//
int32_t
cl_exec_impl::exec_program_get_output(int32_t &retval,
    CORBA::String_out stdout_str, CORBA::String_out stderr_str,
    const char *cmd, remote_exec::sched_class_t cid, int pri, Environment &e)
{
	int	exec_ret_val;
	char	*stdoutstr = NULL;
	char	*stderrstr = NULL;

#if (SOL_VERSION >= __s10)
	cl_exec_log(CLADM_GREEN, "cl_exec: cluster id = %d\n",
	    (uint_t)e.get_cluster_id());
#endif	// (SOL_VERSION >= __s10)

	cl_exec_log(CLADM_GREEN, "cl_exec: Execute task <%s>\n", cmd);

	//
	// Here we get the stdout and stderr of the task.
	// We inform the infrastructure that we do require this info to be
	// passed back to us.
	//
	exec_ret_val = execute_cmd(cmd, true, cid, pri, retval, &stdoutstr,
	    &stderrstr);

	if (exec_ret_val != 0) {
		cl_exec_log(CLADM_RED, "Executing task <%s> failed with error "
		    "%d\n", cmd, exec_ret_val);
	} else {
		cl_exec_log(CLADM_GREEN, "cl_exec: Task <%s> success, "
		    "retval = %d\n", cmd, retval);
		if (stdoutstr != NULL) {
			cl_exec_log(CLADM_GREEN, "cl_exec: stdout: %s\n",
			    stdoutstr);
		}
		if (stderrstr != NULL) {
			cl_exec_log(CLADM_GREEN, "cl_exec: stderr: %s\n",
			    stderrstr);
		}
	}
	stdout_str = stdoutstr;
	stderr_str = stderrstr;
	return (exec_ret_val);
}
