//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// cl_execd.h
//

#ifndef	_CL_EXECD_H
#define	_CL_EXECD_H

#pragma ident	"@(#)cl_execd.h	1.6	08/07/17 SMI"

#include <sys/os.h>
#include <door.h>
#include <sys/sc_syslog_msg.h>
#include <alloca.h>
#include <sys/procset.h>
#include <h/remote_exec.h>

#define	SC_SYSLOG_CL_EXECD_TAG	"Cluster.cl_execd"

#define	MAXBUF	8192

typedef struct task_buf {
	int cmd_len;
	char cmd[MAXBUF];
	bool need_output;
	remote_exec::sched_class_t cid;
	int pri;
} task_buf_t;

typedef struct result_buf {
	int err_val;
	int ret_val;
	char output[MAXBUF];
	char erroutput[MAXBUF];
} result_buf_t;

typedef enum { STDOUT_OP, STDERR_OP } op_type_t;

typedef struct read_fd_buf {
	int fd;
	op_type_t op;
	result_buf_t *rbuf;
} read_fd_buf_t;

//
// Common utility functions.
//
extern void log_error(const char *, ...);
extern void cl_exec_log(int, const char *, ...);
extern void do_exit(int, const char *, ...);
extern void * wait_for_signals(void *);
extern void create_and_arm_failfast(const char *);
extern void block_allsignals(void);
extern char *get_srv_name(void);
extern void set_ts_prio(void);
extern void set_rt_prio(void);
extern void * read_fd(void *);
extern void log_init(void);

#endif // _CL_EXECD_H
