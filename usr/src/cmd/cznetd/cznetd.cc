/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)cznetd.cc	1.13	08/11/17 SMI"

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <libintl.h>
#include <locale.h>
#include <fcntl.h>
#include <pthread.h>
#include <signal.h>
#include <errno.h>
#include <string.h>
#include <zone.h>
#include <stropts.h>
#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <net/if.h>
#include <inet/tcp.h>
#include <sys/sockio.h>
#include <pthread.h>
#include <sys/sc_syslog_msg.h>
#include <sys/rsrc_tag.h>
#include <h/ccr.h>
#include <h/naming.h>
#include <rgm/sczones.h>
#include <h/cmm.h>
#include <cmm/cmm_ns.h>
#include <h/membership.h>
#include <cmm/membership_client.h>
#include <cznet/cznet_internal.h>
#include <libzccfg/libzccfg.h>
#include <sys/list_def.h>

#define	CZNETD_LOCK "/var/cluster/run/cznetd.lock"
#define	CZNET_CCR_TABLE "cz_network_ccr"

/*lint -e708 */
static pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
static pthread_cond_t cond = PTHREAD_COND_INITIALIZER;
/*lint +e708 */
int listen_to_ccr_updates;

static void make_daemon(void);
static void make_unique(char *lock_filename);

static void init_signal_handlers(void);
void sig_handler(void *);

static int register_ccr_callbacks(void);
static int register_czmm_callbacks(char *czname);
static void register_zone_event_callbacks(void);
static void update_mappings(void);
static int ip_addr_exists(in_addr_t);
static int plumb_clprivnet(in_addr, const char *, uint_t, zoneid_t);
static int unplumb_clprivnet(in_addr, uint_t);
static int setup_zone_netconfig(in_addr_t ipaddr, in_addr_t netmask,
			char *zone);
static int remove_zone_netconfig(in_addr_t ipaddr, char *zone);
static int tcp_conn_abort(in_addr_t ipaddr);

char *progname;

void czmm_callback_func(const char *, const char *,
    const cmm::membership_t, const cmm::seqnum_t);

bool debug = false;

cznet_map cznetd_map;

class czmm_callback_client : public _SList::ListElem {
	public:
		czmm_callback_client();
		char zcname[ZONENAME_MAX + 1];
		membership::client_ptr client_p;
};

// Constructor
czmm_callback_client::czmm_callback_client() : _SList::ListElem(this)
{
	zcname[0] = '\0';
	client_p = membership::client::_nil();
}

typedef IntrList<czmm_callback_client, _SList> czmm_client_list_t;

// Global list of CZMM callback clients.
czmm_client_list_t client_list;

//lint -e708
static pthread_mutex_t cznetd_lock = PTHREAD_MUTEX_INITIALIZER;
//lint +e708

class cznetd_callback_impl : public McServerof<ccr::callback> {
public:
	void did_update(const char *, ccr::ccr_update_type,
	    Environment &);
	void did_update_zc(const char *, const char *, ccr::ccr_update_type,
	    Environment &);
	void did_recovery(const char *, ccr::ccr_update_type,
	    Environment &);
	void did_recovery_zc(const char *, const char *, ccr::ccr_update_type,
	    Environment &);
	void _unreferenced(unref_t);
};

/*
 * Create a daemon process and detach it from the controlling terminal.
 */
void
make_daemon()
{
	struct rlimit rl;
	int	fd;
	pid_t	pd;
	int	err;
	pid_t	rc;
	sc_syslog_msg_handle_t handle;
#if SOL_VERSION < __s9
	int i;
#endif

	pd = fork();
	if (pd < 0) {
		err = errno;
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CZNETD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "fork: %s", strerror(err));
		sc_syslog_msg_done(&handle);
		exit(1);
	}

	/*
	 * parent exits after fork
	 */
	if (pd > 0)
		exit(0);		/* parent */

	/*
	 * In the child, redirect stdin to /dev/null so that if
	 * the daemon reads from stdin, it doesn't block.
	 * Redirect stdout, stderr to /dev/console so that any
	 * output printed by the daemon goes somewhere.
	 */

	if ((fd = open("/dev/null", O_RDONLY)) == -1) {
		err = errno;
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_PRIVIPD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Unable to open /dev/null: %s", strerror(err));
		sc_syslog_msg_done(&handle);
		exit(1);
	}

	if (dup2(fd, 0) == -1)
		(void) close(fd);

	/*
	 * redirect stdout and stderr by opening /dev/console
	 * and duping to the appropriate descriptors.
	 */
	fd = open("/dev/console", O_WRONLY);
	if (fd == -1) {
		err = errno;
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CZNETD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Unable to open /dev/console: %s", strerror(err));
		sc_syslog_msg_done(&handle);
		exit(1);
	}
	if (dup2(fd, 1) == -1 || dup2(fd, 2) == -1)
		(void) close(fd);

	/*
	 * child closes rest of file descriptors
	 */
	rl.rlim_max = 0;
	(void) getrlimit(RLIMIT_NOFILE, &rl);
	if ((int)rl.rlim_max == 0)
		exit(1);

#if SOL_VERSION < __s9
	for (i = 3; i < (int)rl.rlim_max; i++) {
		(void) close(i);
	}
#else
	closefrom(3);
#endif

	/*
	 * child sets this process as group leader
	 */
	rc = setsid();
	if (rc == (pid_t)-1) {
		err = errno;
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CZNETD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "setsid: %s", strerror(err));
		sc_syslog_msg_done(&handle);
		exit(1);
	}
}

/*
 * This function opens a file and locks it.
 * This will prevent another rgmd daemon from starting while we are running
 * This must be called after the fork (used to daemonize)
 * as the lock is not inherited by the child.
 */
static void
make_unique(char *lock_filename)
{
	int file;
	int err;
	struct flock lock_attr;
	sc_syslog_msg_handle_t handle;

	file = open(lock_filename, O_RDWR|O_CREAT, S_IWUSR);
	if (-1 == file) {
		err = errno;
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_PRIVIPD_TAG, "");
		//
		// scmsgs already explained.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Unable to create the lock file (%s): %s\n",
		    lock_filename, strerror(err));
		sc_syslog_msg_done(&handle);
		exit(1);
	}

	/*
	 * Force the mode mask in case umask has been specified.
	 * Will not exit if chmod does not succeed.
	 */
	(void) chmod(lock_filename, S_IWUSR);

	lock_attr.l_type = F_WRLCK;
	lock_attr.l_whence = 0;
	lock_attr.l_start = 0;
	lock_attr.l_len = 0;

	err = fcntl(file, F_SETLK, &lock_attr);
	if (-1 == err) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_PRIVIPD_TAG, "");
		//
		// scmsgs already explained.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "There is already an instance of this daemon running\n");
		sc_syslog_msg_done(&handle);
		exit(1);
	}
}

int
main(int, char **argv)
{
	progname = argv[0];

	(void) setlocale(LC_ALL, "");
	(void) textdomain(TEXT_DOMAIN);
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	(void) sc_syslog_msg_set_syslog_tag(SC_SYSLOG_CZNETD_TAG);
	os::sc_syslog_msg logger(SC_SYSLOG_CZNETD_TAG, "", NULL);

	/*
	 * Check if the process was started by the superuser.
	 */
	if (getuid() != 0) {
		(void) logger.log(LOG_ERR, MESSAGE,
		    "fatal: must be superuser to start %s",
		    progname);
		exit(1);
	}

//	if (sc_zonescheck() != 0)
//		return (1);

	// daemonize
	make_daemon();

	// Make sure another daemon is not running
	make_unique(CZNETD_LOCK);

	// Check to see if IP range and the netmask assigned for the cluster
	// has been modified using the non-cluster mode flexible-IP interfaces.
	// If it has, then assign new subnets to the zone clusters from the
	// new IP range.
	if (zccfg_modify_subnet() != 0)
		return (-1);

	// Initialize the cznet_map
	if (cznetd_map.initialize() != 0)
		return (-1);

#ifndef _KERNEL
	if (clconf_lib_init() != 0) {
		return (-1);
	}
#endif

	// Register the CCR callbacks
	(void) register_ccr_callbacks();

	// set up the signal handlers
	init_signal_handlers();

	// Listen for the CCR callbacks. Exit when signalled.
	CL_PANIC(pthread_mutex_lock(&lock) == 0);
	listen_to_ccr_updates = 1;
	while (listen_to_ccr_updates == 1) {
		CL_PANIC(pthread_cond_wait(&cond, &lock) == 0);
	}
	CL_PANIC(pthread_mutex_unlock(&lock) == 0);

	// Shutdown the privipd_map
	cznetd_map.shutdown();

	// Exit after signalled
	return (0);
}

static int
register_ccr_callbacks()
{
	cznetd_callback_impl *cznet_ccr_cb;
	Environment e;
	CORBA::Exception *ex;
	ccr::callback_var cb_v;
	ccr::readonly_table_var tab_v;
	ccr::directory_var ccr_v;
	os::sc_syslog_msg logger(SC_SYSLOG_CZNETD_TAG, "", NULL);

	naming::naming_context_var ctx_v = ns::local_nameserver();
	if (CORBA::is_nil(ctx_v)) {
		//
		// SCMSGS
		// @explanation
		// The cznetd daemon was unable to register for
		// configuration callbacks.
		// @user_action
		// These callbacks are used only for enabling or disabling the
		// private IP communication for zone clusters. So, this feature
		// will be unavailable. To recover, it might be necessary to
		// reboot this node or the entire cluster. Contact your
		// authorized Sun service provider to determine whether a
		// workaround or patch is available.
		//
		(void) logger.log(LOG_ERR, MESSAGE,
		    "Failed to register configuration callbacks "
		    "for cznetd daemon");
		abort();
	}

	CORBA::Object_var obj_v = ctx_v->resolve("ccr_directory", e);
	if (e.exception()) {
		e.exception()->print_exception("get ccr");
		abort();
	}
	else
		ccr_v = ccr::directory::_narrow(obj_v);

	if (CORBA::is_nil(ccr_v))  {
		(void) logger.log(LOG_ERR, MESSAGE,
		    "Failed to register configuration callbacks");
		abort();
	}

	tab_v = ccr_v->lookup(CZNET_CCR_TABLE, e);
	if ((ex = e.exception()) != NULL) {
		if (ccr::no_such_table::_exnarrow(ex)) {
			e.clear();
			ccr_v->create_table(CZNET_CCR_TABLE, e);
			if ((ex = e.exception()) != NULL) {
				// Table exists exception is ok.
				if (ccr::table_exists::_exnarrow(ex)) {
					e.clear();
				} else {
					e.clear();
					(void) logger.log(LOG_ERR, MESSAGE,
					    "Failed to register configuration "
					    "callbacks");
					abort();
				}
			}
			tab_v = ccr_v->lookup(CZNET_CCR_TABLE, e);
			if ((ex = e.exception()) != NULL) {
				e.clear();
				(void) logger.log(LOG_ERR, MESSAGE,
					"Failed to register configuration "
					"callbacks");
				abort();
			}
		} else {
			e.clear();
			(void) logger.log(LOG_ERR, MESSAGE,
				    "Failed to register configuration "
				    "callbacks");
			abort();
		}
	} else {
		// If the table is already created, then read
		// existing mappings and register.
		update_mappings();
	}
	cznet_ccr_cb = new cznetd_callback_impl;
	cb_v = cznet_ccr_cb->get_objref();

	tab_v->register_callbacks(cb_v, e);
	if (e.exception()) {
		(void) logger.log(LOG_ERR, MESSAGE,
			"Failed to register configuration callbacks\n");
		abort();
	}

	return (0);
}

void cznetd_callback_impl::did_update_zc(
	const char *,
	const char *,
	ccr::ccr_update_type,
	Environment &)
{
	// Read the CCR mappings and update the cache with
	// with the current entries. If new virtual cluster
	// information is being added, then register for the
	// zone callbacks using the zone name. If virtual
	// cluster entries are being removed, unregister for
	// the zone callbacks.
	os::sc_syslog_msg logger(SC_SYSLOG_CZNETD_TAG, "", NULL);
	update_mappings();
}

void cznetd_callback_impl::did_update(
	const char *,
	ccr::ccr_update_type,
	Environment &)
{
	// For each entry in the privip_ccr table (if it exists and
	// has changed) belonging to the local node id, plumb or
	// unplumb the IP address in the zone specified if the zone is
	// in running state.
	os::sc_syslog_msg logger(SC_SYSLOG_CZNETD_TAG, "", NULL);
	update_mappings();
}

void cznetd_callback_impl::did_recovery(const char *, ccr::ccr_update_type,
    Environment &)
{
}

void cznetd_callback_impl::did_recovery_zc(const char *,
	const char *, ccr::ccr_update_type, Environment &)
{
}

void cznetd_callback_impl::_unreferenced(unref_t)
{
}

int
ip_addr_exists(in_addr ipaddr)
{
	int s;
	struct lifnum lifn;
	struct lifconf lifc;
	struct lifreq *lifrp;
	char *addrlist = NULL;
	struct sockaddr_in *sin;
	int i;

	s = socket(AF_INET, SOCK_DGRAM, 0);
	if (s < 0)
		return (0);

	lifn.lifn_family = AF_INET;
	lifn.lifn_flags = LIFC_ALLZONES;
	if (ioctl(s, SIOCGLIFNUM, &lifn) < 0) {
		(void) close(s);
		return (0);
	}

	addrlist = (char *)calloc((size_t)lifn.lifn_count,
		(size_t)sizeof (struct lifreq));
	if (addrlist == NULL) {
		(void) close(s);
		return (0);
	}

	lifc.lifc_family = AF_INET;
	lifc.lifc_len = lifn.lifn_count * (int)sizeof (struct lifreq);
	lifc.lifc_buf = (caddr_t)addrlist;
	lifc.lifc_flags = LIFC_ALLZONES;

	if (ioctl(s, SIOCGLIFCONF, (char *)&lifc) < 0) {
		free(addrlist);
		addrlist = NULL;
		(void) close(s);
		return (0);
	}

	lifrp = lifc.lifc_req;
	for (i = 0; i < (lifc.lifc_len / (int)sizeof (struct lifreq));
		i++, lifrp++) {
		sin = (struct sockaddr_in *)&lifrp->lifr_addr;
		if (sin->sin_addr.s_addr == ipaddr.s_addr) {
			free(addrlist);
			addrlist = NULL;
			(void) close(s);
			return (1);
		}
	}
	free(addrlist);
	addrlist = NULL;
	(void) close(s);
	return (0);
}

static int
plumb_clprivnet(in_addr ip_addr, const char *mask, uint_t vc_id,
    zoneid_t zoneid) {
	char ifname[LIFNAMSIZ];
	int s;
	struct lifreq lifr;
	unsigned short setflags = 0;
	unsigned short clearflags = 0;
	in_addr_t netmask;

	netmask = inet_addr(mask);

	// Check if IP address has already been plumbed.
	if (ip_addr_exists(ip_addr) == 1)
		return (0);

	s = socket(AF_INET, SOCK_DGRAM, 0);
	if (s < 0) {
		return (-1);
	}

	sprintf(ifname, "%s:%d", "clprivnet0", vc_id);

	// Add logical interface
	bzero(&lifr, sizeof (lifr));
	(void) strncpy(lifr.lifr_name, ifname, sizeof (lifr.lifr_name) - 1);
	if (ioctl(s, SIOCLIFADDIF, &lifr) < 0) {
		(void) close(s);
		return (-1);
	}
	(void) strncpy(ifname, lifr.lifr_name, sizeof (lifr.lifr_name) - 1);

	// Netmask for the logical interface
	lifr.lifr_addr.ss_family = AF_INET;
	((struct sockaddr_in *)&lifr.lifr_addr)->sin_addr.s_addr =
		netmask;

	if (ioctl(s, SIOCSLIFNETMASK, /*lint !e737 */
		&lifr) < 0) {
		(void) close(s);
		return (-1);
	}

	// IP address for the logical interface
	((struct sockaddr_in *)&lifr.lifr_addr)->sin_addr.s_addr =
		ip_addr.s_addr;

	if (ioctl(s, SIOCSLIFADDR, /*lint !e737 */
		&lifr) < 0) {
		(void) close(s);
		return (-1);
	}

	// Set flags to indicate its a private interface
	if (ioctl(s, SIOCGLIFFLAGS, &lifr) < 0) {
		(void) close(s);
		return (-1);
	}

	setflags = IFF_UP | IFF_PRIVATE;
	clearflags = IFF_NOARP;
	lifr.lifr_flags &= ~((uint64_t)clearflags);
	lifr.lifr_flags |= ((uint64_t)setflags);

	if (ioctl(s, SIOCSLIFFLAGS, /*lint !e737 */
		&lifr) < 0) {
		(void) close(s);
		return (-1);
	}

	// Set the zoneid for the interface
	lifr.lifr_zoneid = zoneid;
	if (ioctl(s, SIOCSLIFZONE, /*lint !e737 */
		(caddr_t)&lifr) < 0) {
		(void) close(s);
		return (-1);
	}

	(void) close(s);
	return (0);
}

static int
unplumb_clprivnet(in_addr ipaddr, uint_t vc_id)
{
	char ifname[LIFNAMSIZ];
	int s;
	struct lifreq lifr;

	// Check if IP address has already been unplumbed.
	if (ip_addr_exists(ipaddr) == 0)
		return (0);

	s = socket(AF_INET, SOCK_DGRAM, 0);
	if (s < 0)
		return (-1);

	sprintf(ifname, "%s:%d", "clprivnet0", vc_id);

	bzero(&lifr, sizeof (lifr));
	(void) strncpy(lifr.lifr_name, ifname, sizeof (lifr.lifr_name) - 1);
	((struct sockaddr_in *)&lifr.lifr_addr)->sin_addr.s_addr =
		ipaddr.s_addr;
	lifr.lifr_addr.ss_family = AF_INET;

	if (ioctl(s, SIOCLIFREMOVEIF, /*lint !e737 */
		&lifr) < 0) {
		(void) close(s);
		return (-1);
	}

	(void) close(s);
	return (0);
}


/*
 * init_signal_handlers
 * ---------------------
 * Blocks all signals in this thread, which will be the parent of the
 * other threads, so that they will inherit the signal mask.
 * This is especially important in those threads that will be handling
 * and making door calls.
 *
 * Creates a thread to handle all the signals.  It runs the sig_handler
 * method below.
 *
 * If we fail in any of this, exit the daemon.
 */
static void
init_signal_handlers(void)
{
	sc_syslog_msg_handle_t handle;

	// block all signals for all threads
	sigset_t sig_set;
	int err;

	if (sigfillset(&sig_set) != 0) {
		err = errno;
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CZNETD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "sigfillset: %s", strerror(err));
		sc_syslog_msg_done(&handle);
		exit(err);
	}

	if ((err = pthread_sigmask(SIG_BLOCK, &sig_set, NULL)) != 0) {
		char *err_str = strerror(err);
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CZNETD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "pthread_sigmask: %s", err_str ? err_str : "");
		sc_syslog_msg_done(&handle);
		exit(err);
	}

	// now spawn the thread that will actually handle the signals
	pthread_t sig_thr;
	if ((err = pthread_create(&sig_thr, NULL,
	    (void *(*)(void *))sig_handler, NULL)) != 0) {
		char *err_str = strerror(err);
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_PRIVIPD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "pthread_create: %s", err_str ? err_str : "");
		sc_syslog_msg_done(&handle);
		exit(err);
	}
}


/*
 * sig_handler
 * --------------
 * Signal handler
 *
 * This function runs in a dedicated signal handling thread.  The thread
 * should be created with a signal mask blocking all signals.
 *
 * Uses an infinite loop around sigwait, to wait for all signals.
 * Exits on SIGTERM, continues on all others.
 *
 * If anything goes awry, exits immediately.
 */
void
sig_handler(void *)
{
	sc_syslog_msg_handle_t handle;

	sigset_t sig_set;
	int err, sig;

	if (sigfillset(&sig_set) != 0) {
		err = errno;
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_CZNETD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "sigfillset: %s", strerror(err));
		sc_syslog_msg_done(&handle);
		exit(err);
	}

	while (true) {
		if ((sig = sigwait(&sig_set)) == -1) {
			err = errno;
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_CZNETD_TAG, "");
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "sigwait: %s", strerror(err));
			sc_syslog_msg_done(&handle);
			exit(err);
		}

		switch (sig) {
		case SIGTERM:
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_CZNETD_TAG, "");
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "fatal: received signal %d", sig);
			sc_syslog_msg_done(&handle);
			// Set listen_to_ccr_updates to 0 to have the
			// daemon exit
			CL_PANIC(pthread_mutex_lock(&lock) == 0);
			listen_to_ccr_updates = 0;
			CL_PANIC(pthread_cond_broadcast(&cond) == 0);
			CL_PANIC(pthread_mutex_unlock(&lock) == 0);
			break;
		default:
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_CZNETD_TAG, "");
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "received signal %d: continuing", sig);
			sc_syslog_msg_done(&handle);
		}
	}
}

static int
tcp_conn_abort(in_addr_t ipaddr)
{
	int s;
	struct strioctl iocb;
	tcp_ioc_abort_conn_t tcpac;
	struct sockaddr_in *sinp = (struct sockaddr_in *)&tcpac.ac_local;

	bzero(&iocb, sizeof (iocb));
	bzero(&tcpac, sizeof (tcpac));

	s = socket(AF_INET, SOCK_STREAM, 0);
	if (s < 0)
		return (-1);

	tcpac.ac_local.ss_family = AF_INET;
	sinp->sin_addr.s_addr = ipaddr;
	tcpac.ac_remote.ss_family = AF_INET;

	tcpac.ac_start = TCPS_SYN_SENT;
	tcpac.ac_end = TCPS_TIME_WAIT;

	iocb.ic_cmd = TCP_IOC_ABORT_CONN;
	iocb.ic_timout = 0;
	iocb.ic_len = (int)sizeof (tcp_ioc_abort_conn_t);
	iocb.ic_dp = (char *)&tcpac;

	if (ioctl(s, I_STR, &iocb) != 0) {
		(void) close(s);
		return (-1);
	}

	(void) close(s);
	return (0);
}

int
get_ip_address(char *sub_net, nodeid_t nid, in_addr *ip_addr) {
	ip_addr->_S_un._S_addr = inet_addr(sub_net) | ntohl(nid);
	if (ip_addr->_S_un._S_addr == (in_addr_t)(-1)) {
		ip_addr->_S_un._S_addr = 0;
		return (-1);
	}

	return (0);
}

void
czmm_callback_func(
    const char *cz_namep, const char *mem_obj_namep,
    const cmm::membership_t membership, const cmm::seqnum_t seqnum) {
	nodeid_t local_nodeid;
	uint32_t current_incn, new_incn;
	cznet_elem *cz_elem = NULL;
	char *czname;
	struct in_addr ip_addr;
	uint_t czid;
	zoneid_t zid;
	int ret = 0;
	const char *n_mask = NULL;
	os::sc_syslog_msg logger(SC_SYSLOG_CZNETD_TAG, "", NULL);

	//
	// SCMSGS
	// @explanation
	// Informational message from the cznetd daemon.
	// @user_action
	// No user action is required.
	//

	(void) logger.log(LOG_INFO, MESSAGE,
	    "%s: received callback from CZMM for cluster %s, "
	    "seqnum = %llu\n", progname, cz_namep, seqnum);

	/*
	for (nodeid_t nid = 1; nid <= NODEID_MAX; nid++) {
		if (membership.members[nid] != INCN_UNKNOWN) {
			os::printf("%s: node %d, incn %d\n",
			    progname, nid, membership.members[nid]);
		}
	}
	*/

	SList<cznet_elem>::ListIterator
	    iter(cznetd_map.get_map_list());

	while ((cz_elem = iter.get_current()) != NULL) {
		czname = cz_elem->get_czname();
		if (strcmp(czname, cz_namep) != 0) {
			iter.advance();
			continue;
		}

		// Check whether the local nodes' incarnation number
		// has changed.
		local_nodeid =  orb_conf::local_nodeid();
		current_incn = cz_elem->get_incn(local_nodeid);
		new_incn = membership.members[local_nodeid];

		// Get the ip address for plumb/unplumb

		// Create the ip Address using the subnet and nodeid
		(void) get_ip_address(cz_elem->get_subnet(),
		    local_nodeid, &ip_addr);

		// Get the clusterized zone id
		(void) clconf_lib_init();

		ret = clconf_get_cluster_id(czname, &czid);
		if (czid < 0) {
			//
			// SCMSGS
			// @explanation
			// Failed to get the zone cluster ID for
			// the given zone cluster name.
			// @user_action
			// Contact your authorized Sun service provider
			// to determine whether a workaround or patch is
			// available.
			//
			(void) logger.log(LOG_ERR, MESSAGE,
			    "Failed to configure the private IP "
			    "address for %s", czname);
				return;
		}

		// Check for the node join case
		if ((new_incn  > INCN_UNKNOWN) && (new_incn != current_incn)) {
			// Get the info needed to plumb the clprivnet

			// Get the zone id
			zid = getzoneidbyname(czname);
			if (zid == -1) {
				(void) logger.log(LOG_ERR, MESSAGE,
				    "Failed to configure "
				    "the private IP "
				    "address for a zone\n");
				return;
			}

			// Get the netmask
			clconf_cluster_t *cl = clconf_cluster_get_current();
			n_mask = clconf_obj_get_property((clconf_obj_t *)cl,
			    "private_subnet_netmask");

			(void) plumb_clprivnet(ip_addr, n_mask, czid, zid);
		} else if ((new_incn == INCN_UNKNOWN) &&
		    (current_incn > INCN_UNKNOWN)) {
			// node remove case
			unplumb_clprivnet(ip_addr, czid);
		}

		// Update the incn number
		cz_elem->set_incn(local_nodeid, new_incn);
		break;
	}
}

//
// Register membership callback.
//
static int
register_czmm_callbacks(char *czname)
{
	Environment e;
	CORBA::Exception *exp;

	os::sc_syslog_msg logger(SC_SYSLOG_CZNETD_TAG, "", NULL);

	//
	// Create the client and add it to the list of clients.
	//
	czmm_callback_client  *czmm_clientp = new czmm_callback_client;
	ASSERT(czmm_clientp != NULL);
	czmm_clientp->client_p =
	    (new membership_client_impl(progname, membership::ZONE_CLUSTER,
	    czname, NULL, czmm_callback_func))->get_objref();
	ASSERT(os::strlen(czname) <= ZONENAME_MAX);
	(void) os::strncpy(czmm_clientp->zcname, czname, ZONENAME_MAX);
	client_list.append(czmm_clientp);


	// Register for membership callbacks for a cluster

	membership::api_var membership_api_v = cmm_ns::get_membership_api_ref();
	ASSERT(!CORBA::is_nil(membership_api_v));
	membership_api_v->register_for_cluster_membership(
	    czmm_clientp->client_p, czname, e);
	if ((exp = e.exception()) != NULL) {
		//
		// SCMSGS
		// @explanation
		// The cznetd daemon was unable to register for
		// configuration callbacks.
		// @user_action
		// These callbacks are used for enabling or disabling the
		// private IP communication for zone cluster zones. So, this
		// feature // will be unavailable.
		// To recover, it may be necessary to
		// reboot this node or the entire cluster. Contact your
		// authorized Sun service provider to determine whether a
		// workaround or patch is available.
		//
		(void) logger.log(LOG_ERR, MESSAGE,
		    "Failed to register CZMM callbacks: Unknown Error");
		abort();
	}
	return (0);
}

//
// Unregister membership callbacks.
//
static int
unregister_czmm_callbacks(char *zcnamep)
{
	Environment e;
	CORBA::Exception *exp;
	czmm_callback_client *czmm_clientp;
	bool found = false;

	os::sc_syslog_msg logger(SC_SYSLOG_CZNETD_TAG, "", NULL);
	//
	// Walk through the list of clients and fetch the client
	// that matches the zone cluster name.
	//
	for (client_list.atfirst(); (czmm_clientp = client_list.get_current())
	    != NULL; client_list.advance()) {
		if (os::strcmp(czmm_clientp->zcname, zcnamep) == 0) {
			// Match
			(void) client_list.erase(czmm_clientp);
			found = true;
			break;
		}
	}
	if (!found) {
		//
		// SCMSGS
		// @explanation
		// The cznetd daemon was attempting to unregister
		// configuration callbacks when no such callback was registered.
		// @user_action
		// These callbacks are used for enabling or disabling the
		// private IP communication for zone cluster zones. So, this
		// feature will be unavailable.
		// To recover, it mght be necessary to
		// reboot this node or the entire cluster. Contact your
		// authorized Sun service provider to determine whether a
		// workaround or patch is available.
		//
		(void) logger.log(LOG_ERR, MESSAGE,
		    "No callbacks registered for cluster %s", zcnamep);
		abort();
	}
	//
	// Unregister for membership callbacks for a cluster
	//
	ASSERT(!CORBA::is_nil(czmm_clientp->client_p));
	membership::api_var membership_api_v = cmm_ns::get_membership_api_ref();
	ASSERT(!CORBA::is_nil(membership_api_v));
	membership_api_v->unregister_for_cluster_membership(
	    czmm_clientp->client_p, zcnamep, e);
	exp = e.exception();
	if (exp != NULL) {
		if (membership::no_such_membership::_exnarrow(exp)) {
			//
			// It is possible that zone cluster has been cleaned up
			// from the Membership subsystem before we could
			// unregister callback. This is ok, we just have
			// to cleanup the client and we are done.
			//
			e.clear();
		} else {
			//
			// SCMSGS
			// @explanation:
			// cznetd daemon has failed to unregister the
			// callbacks with zone cluster membership
			// infrastructure for unknown reason.
			// @user_action:
			// Search for messages from cznetd to determine the
			// source of the error. Save a copy of the
			// /var/adm/messages files on all nodes. If the
			// problem persists, contact your authorized Sun
			// service provider for  assistance in diagnosing
			// the problem.
			//
			(void) logger.log(LOG_ERR, MESSAGE,
			    "Failed to unregister CZMM callbacks: "
			    "Unknown Error");
			abort();

		}
	}
	CORBA::release(czmm_clientp->client_p);
	delete czmm_clientp;
	return (0);
}

static void
update_mappings()
{
	cznet_elem *cz_elem = NULL;
	char *czname;
	int err;
	os::sc_syslog_msg logger(SC_SYSLOG_CZNETD_TAG, "", NULL);

	CL_PANIC(pthread_mutex_lock(&cznetd_lock) == 0);

	// Obtain values from CCR infrastructure file.
	// Read the privip_ccr table to identify the update
	if (cznetd_map.read_ccr_mappings() != 0) {
		//
		// SCMSGS
		// @explanation
		// The private IP address assigned to a zone was not
		// configured correctly.
		// @user_action
		// The private IP address assigned to the zone is used for
		// private IP communication for the zone. So, this feature may
		// not work as expected as a result of this error. Contact
		// your authorized Sun service provider to determine whether a
		// workaround or patch is available.
		//
		(void) logger.log(LOG_ERR, MESSAGE,
		    "Failed to read the cz info \n");
		CL_PANIC(pthread_mutex_unlock(&cznetd_lock) == 0);
		return;
	}
	if (cznetd_map.is_changed()) {
		SList<cznet_elem>::ListIterator
		iter(cznetd_map.get_map_list());
		while ((cz_elem = iter.get_current()) != NULL) {
			err = 0;
			czname = cz_elem->get_czname();
			if (cz_elem->get_action() == cznet_elem::CZ_ADD) {
				(void) register_czmm_callbacks(czname);
				cz_elem->set_action(
					cznet_elem::CZ_EXISTS);
				iter.advance();
			} else if (cz_elem->get_action() ==
				cznet_elem::CZ_REMOVE) {
				(void) unregister_czmm_callbacks(czname);

				// Advance the iterator before
				// doing the erase.
				iter.advance();

				(void) cznetd_map.get_map_list().
				erase(cz_elem);
				delete cz_elem;
			} else {
				iter.advance();
			}
		}
	}
	CL_PANIC(pthread_mutex_unlock(&cznetd_lock) == 0);
}
