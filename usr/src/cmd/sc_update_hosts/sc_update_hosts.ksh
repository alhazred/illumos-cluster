#!/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# pragma ident	"@(#)sc_update_hosts.ksh	1.8	08/07/17 SMI"
#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.

#
# sc_update_hosts.ksh
#

# Program name and args list

typeset -r PROG=${0##*/}
typeset -r ARGS=$*

typeset -x TEXTDOMAIN=SUNW_SC_CMD
typeset -x TEXTDOMAINDIR=/usr/cluster/lib/locale
typeset    SC_BASEDIR=${SC_BASEDIR:-}
typeset -r SC_CONFIG=${SC_CONFIG:-${SC_BASEDIR}/etc/cluster/ccr/global/infrastructure}
typeset HOSTS=${SC_BASEDIR}/etc/inet/hosts
typeset SC_IP=${SC_BASEDIR}/usr/cluster/lib/scadmin/lib/sc_ip.pl

${SC_BASEDIR}/usr/cluster/lib/sc/sc_zonescheck
if [ $? -ne 0 ]; then
	exit 1
fi

#####################################################
#
# sc_addnodes_in_hosts
#
#      	Updates the local host file with the IP addresses and
#	hostnames of all nodes in the cluster .
#
#       Return:
#               zero            Success
#               non-zero        Failure
#
#####################################################

sc_addnodes_in_hosts()
{
	typeset cluster_nodes="$(sed -n 's/^cluster\.nodes\.[1-9][0-9]*\.name[ 	]\(.*\)/\1/p' ${SC_CONFIG})"

	if [[ -z "${cluster_nodes}" ]];then
		return 1
	fi
	# Iterate over all the nodes in the cluster
	for node in $cluster_nodes
	do
		node_ip=$(${SC_IP} get_ip_in_hosts ${node})
		if [[ -n ${node_ip} ]];then
			continue
		fi
		node_ip=$(${SC_IP} convert_to_ip ${node})
		if [[ -z ${node_ip} ]];then
			printf "%s:  $(gettext 'Can not resolve IP address for %s')\n" ${PROG} ${node} >&2
			continue
		fi
		node_again=$(${SC_IP} get_name_in_hosts ${node_ip})
		if [[ -n ${node_again} ]];then
			printf "%s:  $(gettext 'IP address conflict for node %s : %s')\n" "${PROG}" "${node}" "${node_ip}" >&2
			printf "%s:  $(gettext 'Failed to add node %s in %s')\n" "${PROG}" "${node}" "${HOSTS}" >&2
			continue
		fi
		# Write the IP address and the hostname
		echo "${node_ip}	${node} # Cluster Node" >> ${HOSTS}
		if [[ $? -ne 0 ]];then
			printf "%s:  $(gettext 'Failed to add node %s in %s')\n" "${PROG}" "${node}" "${HOSTS}" >&2
		fi
	done
	return 0
}

sc_addnodes_in_hosts || exit 1

exit 0
