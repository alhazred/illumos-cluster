#!/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# pragma ident	"@(#)sc_update_ntp.ksh	1.9	08/07/17 SMI"
#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.

#
# sc_update_ntp.ksh
#

# Program name and args list


typeset -r PROG=${0##*/}
typeset -r ARGS=$*

typeset -x TEXTDOMAIN=SUNW_SC_CMD
typeset -x TEXTDOMAINDIR=/usr/cluster/lib/locale
typeset    SC_BASEDIR=${SC_BASEDIR:-}
typeset -r SC_CONFIG=${SC_CONFIG:-${SC_BASEDIR}/etc/cluster/ccr/global/infrastructure}
typeset NTP_CLUSTER=${SC_BASEDIR}/etc/inet/ntp.conf.cluster
typeset NTP_CLUSTER_TEMP=${NTP_CLUSTER}.temp.$$

${SC_BASEDIR}/usr/cluster/lib/sc/sc_zonescheck
if [ $? -ne 0 ]; then
	exit 1
fi

#####################################################
#
# sc_addpeers_in_ntpconfcluster
#
#       Depending on the number of nodes in the cluster
#	adds those many entries in the /etc/inet/ntp.conf.cluster
#	file (i.e. for 4 nodes, 4 entries are added for cluster-priv.
#
#       Return:
#               zero            Success
#               non-zero        Failure
#
#####################################################

sc_addpeers_in_ntpconfcluster()
{
	integer lastentry=1
	integer line_no=0
	integer peerentry
	typeset peerentries
	typeset nodes="$(sed -n 's/^cluster\.nodes\.[1-9][0-9]*\.properties.private_hostname[ 	]\(.*\)/\1/p' ${SC_CONFIG})"

	peerentries=
	newnodes=
	for node in ${nodes}
	do
		peerentry=$(egrep -n "^(peer|server)[   ]*${node}.*" ${NTP_CLUSTER} | awk -F: '{print $1}')
		if [[ ${peerentry} -eq 0 ]];then
			newnodes="${newnodes} ${node}"
			continue
		fi
	done

	lastentry=$(grep -n "^peer" ${NTP_CLUSTER}|tail -1|awk -F: '{print $1}')

	if [[ ${lastentry} -eq 0 ]];then
		lastentry=$(cat ${NTP_CLUSTER}|wc -l)
	fi

	cp /dev/null ${NTP_CLUSTER_TEMP}

	if [[ ${lastentry} -eq 0 ]];then
		for node in ${newnodes}
		do
			echo "peer ${node}" >> ${NTP_CLUSTER_TEMP}
		done
	fi

	while read -rs line
	do
		((line_no += 1))
		echo "${line}"  >> ${NTP_CLUSTER_TEMP}
		if [[ ${line_no} -eq ${lastentry} ]];then
			for node in ${newnodes}
			do
				echo "peer ${node}" >> ${NTP_CLUSTER_TEMP}
			done
		fi
	done < ${NTP_CLUSTER}
	if [[ -n "$(diff ${NTP_CLUSTER} ${NTP_CLUSTER_TEMP})" ]];then
		cp ${NTP_CLUSTER_TEMP} ${NTP_CLUSTER} || return 1
	fi
	rm -rf ${NTP_CLUSTER_TEMP}
	return 0
}

#####################################################
#
# sc_setpeers_in_ntpconfcluster
#
#       Depending on the number of nodes in the cluster
#       adds those many entries in the /etc/inet/ntp.conf.cluster
#       file (i.e. for 4 nodes, 4 entries are added for cluster-priv.
#	One of the "peers" should be "preferred" over the others so,
#	the function adds prefer before the first "peers" entry.
#
#       Return:
#               zero            Success
#               non-zero        Failure
#
#####################################################

sc_setpeers_in_ntpconfcluster()
{
	integer firstentry=1
	integer line_no=0
	integer peerentry
	typeset peerentries
	typeset nodes="$(sed -n 's/^cluster\.nodes\.[1-9][0-9]*\.properties.private_hostname[ 	]\(.*\)/\1/p' ${SC_CONFIG})"

	peerentries=$(
		grep -n "^peer[     ]*clusternode.*-priv" ${NTP_CLUSTER} | \
		awk -F: '{print $1}'
	)

	cp /dev/null ${NTP_CLUSTER_TEMP}

	while read -rs line
	do
		((line_no += 1))
		for peerentry in ${peerentries}
		do
			if [[ ${line_no} -ne ${peerentry} ]];then
				continue
			fi
			if [[ ${firstentry} -ne 1  ]];then
				continue 2
			fi
			for node in ${nodes}
			do
				if [[ ${firstentry} -eq 1  ]];then
					echo "peer ${node} prefer" >> ${NTP_CLUSTER_TEMP}
					firstentry=0
				else
					echo "peer ${node}" >> ${NTP_CLUSTER_TEMP}
				fi
			done
			continue 2
		done
		echo "${line}"  >> ${NTP_CLUSTER_TEMP}
	done < ${NTP_CLUSTER}
	if [[ -n "$(diff ${NTP_CLUSTER} ${NTP_CLUSTER_TEMP})" ]];then
		cp ${NTP_CLUSTER_TEMP} ${NTP_CLUSTER} || return 1
	fi
	rm -rf ${NTP_CLUSTER_TEMP}
	return 0
}

##############################################################
#
# Main function
#
##############################################################

typeset method=$1

if [[ ! -f ${NTP_CLUSTER} ]]; then
	exit 0
fi

if [[ "${method}" != "" ]] &&
   [[ "${method}" != "initialize" ]];then
	printf "%s:  $(gettext 'Internal error - Unknown method \"%s\"')\n" ${PROG} "${method}" >&2
	exit 2
fi

if [[ "${method}" == "initialize" ]];then
	sc_setpeers_in_ntpconfcluster || exit 1
else
	sc_addpeers_in_ntpconfcluster || exit 1
fi

exit 0
