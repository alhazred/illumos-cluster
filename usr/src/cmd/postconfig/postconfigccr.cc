//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// ident	"@(#)postconfigccr.cc	1.7	08/05/20 SMI"
//
// postconfigccr.cc -
//	CCR manipulation program for post install configuration.
// 	This is meant to be used by scripts like scquorumconfig which
//	in turn is called by an rc script.
//

#include <locale.h>
#include <stdlib.h>
#include <stdio.h>

#include <h/ccr.h>
#include <h/naming.h>
#include <nslib/ns.h>
#include <scadmin/scconf.h>
#include <rgm/sczones.h>


#define	TASKPREFIX	"task"
#define	FLDSEP		"."

static char *progname;

void
usage()
{
	os::printf("Usage: %s [-s] -u task=<taskname>[,nodename=<nodename>],"
	    "state=<state>\n", progname);
}

//
// Interprete some of the common/possible errors in the current context.
// The user can do very little if anything fails at this level, the useful
// message will be provided by the caller of this program. Error messages
// in this program are mainly for debugging help.
//
void
interprete_ccr_error(CORBA::Exception *ex)
{
	if (ex == NULL)
		return;

	if (ccr::no_such_table::_exnarrow(ex) != NULL) {
		os::printf(gettext(
		    "%s: ccr table does not exist\n"), progname);
		return;
	}

	if (ccr::no_such_key::_exnarrow(ex) != NULL) {
		os::printf(gettext(
		    "%s: incorrect ccr key\n"), progname);
		return;
	}

	if (ccr::table_invalid::_exnarrow(ex) != NULL) {
		os::printf(gettext(
		    "%s: invalid ccr table\n"), progname);
		return;
	}
}

//
// update_ccr(arglist, sflg)
//	Update the postconfig ccr table for a given key. The key needs to be
//	deduced using the arglist passed to it. Only one property of a task
//	can be updated at a time.
//	quorum is the only task and state is the only property supported at
//	present.
//	If sflg is true, in addition to the updating, print the number of
//	entries in the ccr for the same task that has a property value
//	different than the one being updated with. Since we hold the ccr
//	table write-protected while we do this, this accurately indicates
//	if this instance (node) was the last to make the given state
//	transition.
//
int
update_ccr(char *arglist, int sflg)
{
	ccr::directory_ptr ccr_dir;
	ccr::updatable_table_ptr transptr;
	ccr::readonly_table_ptr tabptr;
	ccr::table_element *outelem = NULL;
	int rstatus = 0;
	Environment e;
	CORBA::Exception *ex = NULL;
	int initcount = 0;
	char *taskname = NULL;
	char *nodename = NULL;
	char *state = NULL;
	char *value;
	nodeid_t nodeid;
	char *nodenamep;
	char *ccrkey = NULL;
	char *property_to_update;
	char *elemkey, *elemdata;
	char *fld1, *fld2, *fld3, *fld4;

	char *knownopts[] = {
		"task",				// task name
		"nodename",			// node name
		"state",			// state name
		(char *)0
	};

	if (clconf_lib_init() != 0) {
		os::printf(gettext(
		    "%s: Failed to initialize orb\n"), progname);
		return (3);
	}

	// look up ccr directory in name server
	naming::naming_context_var ctxp = ns::local_nameserver();
	CORBA::Object_var obj = ctxp->resolve("ccr_directory", e);
	if (e.exception()) {
		os::printf(gettext(
		    "%s: ccr_directory not found in name server\n"),
		    progname);
		rstatus = 3;
		goto update_exit;
	}
	ccr_dir = ccr::directory::_narrow(obj);

	while (*arglist) {
		switch (getsubopt(&arglist, knownopts, &value)) {

		case 0:
			if (taskname != NULL) {
				os::printf(gettext(
				    "%s: task name specified multiple times\n"),
				    progname);
				usage();
				rstatus = 1;
				goto update_exit;
			}
			if (strcmp(value, "quorum") != 0) {
				os::printf(gettext(
				    "%s: Unsupported task name\n"), progname);
				usage();
				rstatus = 1;
				goto update_exit;
			}
			taskname = strdup(value);
			if (taskname == NULL) {
				os::printf(gettext(
				    "%s: Insufficient memory\n"), progname);
				rstatus = 2;
				goto update_exit;
			}
			break;
		case 1:
			if (nodename != NULL) {
				os::printf(gettext(
				    "%s: node name specified multiple times\n"),
				    progname);
				usage();
				rstatus = 1;
				goto update_exit;
			}

			if (value == NULL) {
				usage();
				rstatus = 1;
				goto update_exit;
			}
			nodename = strdup(value);
			if (nodename == NULL) {
				os::printf(gettext(
				    "%s: Insufficient memory\n"), progname);
				rstatus = 2;
				goto update_exit;
			}
			break;
		case 2:
			if (state != NULL) {
				os::printf(gettext(
				    "%s: same property specified multiple "
				    "times\n"), progname);
				usage();
				rstatus = 1;
				goto update_exit;
			}

			if (value == NULL) {
				usage();
				rstatus = 1;
				goto update_exit;
			}
			state = strdup(value);
			if (state == NULL) {
				os::printf(gettext(
				    "%s: Insufficient memory\n"), progname);
				rstatus = 2;
				goto update_exit;
			}
			break;
		case -1:
			os::printf(gettext(
			    "%s: Unknown suboption %s\n"), progname, value);
			/*FALLTHRU*/
		default:
			usage();
			rstatus = 1;
			goto update_exit;
		}
	}

	if (taskname == NULL) {
		os::printf(gettext(
		    "%s: A task name must be specified\n"), progname);
		usage();
		rstatus = 1;
		goto update_exit;
	}

	if (state == NULL) {
		// Atleast one property must be set, and state is the only
		// known propery at present.
		os::printf(gettext(
		    "%s: Update property not specified\n"), progname);
		usage();
		rstatus = 1;
		goto update_exit;
	} else {
		property_to_update = "state";
	}

	if (nodename == NULL) {
		if (scconf_get_nodeid(NULL, &nodeid) != SCCONF_NOERR) {
			os::printf(gettext(
			    "%s: Can not obtain current node id.\n"),
			    progname);
			rstatus = 1;
			goto update_exit;
		}
		if (scconf_get_nodename(nodeid, &nodenamep) != SCCONF_NOERR) {
			os::printf(gettext(
			    "%s: Can not obtain current node name.\n"),
			    progname);
			rstatus = 1;
			goto update_exit;
		}
		nodename = nodenamep;
	}

	ccrkey = (char *)malloc(strlen(taskname) + strlen(nodename) +
	    strlen(property_to_update) + strlen(TASKPREFIX) + 10);
	if (ccrkey == NULL) {
		os::printf(gettext(
		    "%s: Insufficient memory\n"), progname);
		rstatus = 2;
		goto update_exit;
	}
	(void) sprintf(ccrkey, "%s.%s.%s.%s", TASKPREFIX, taskname, nodename,
	    property_to_update);

	transptr = ccr_dir->begin_transaction("postconfig", e);
	if ((ex = e.exception()) != NULL) {
		interprete_ccr_error(ex);
		os::printf(gettext(
		    "%s: begin transaction operation failed on postconfig "
		    "ccr table\n"),
		    progname);
		rstatus = 3;
		goto update_exit;
	}

	tabptr = ccr_dir->lookup("postconfig", e);
	if ((ex = e.exception()) != NULL) {
		interprete_ccr_error(ex);
		os::printf(gettext(
		    "%s: Can not lookup postconfig ccr table\n"),
		    progname);
		rstatus = 3;
		goto update_exit;
	}

	tabptr->atfirst(e);
	if (e.exception()) {
		os::printf(gettext(
		    "%s: Error traversing postconfig table\n"),
		    progname);
		rstatus = 3;
		goto update_exit;
	}

	for (;;) {
		tabptr->next_element(outelem, e);
		if (e.exception()) {
			if (ccr::NoMoreElements::_exnarrow(
			    e.exception())) {
				e.clear();
				break;
			} else {
				break;
			}
		}

		//
		// If this is the same entry that we will update later,
		// check that is not already updated.
		// Also, count only the entries for the current task
		// and property that has a different property value.
		//

		elemkey = (char *)outelem->key;
		elemdata = (char *)outelem->data;

		if (strcmp(ccrkey, elemkey) == 0) {
			if (strcmp(state, elemdata) == 0) {
				delete outelem;
				os::printf(gettext(
				    "%s: ccr entry already updated\n"),
				    progname);
				CORBA::release(tabptr);
				rstatus = 0;
				goto update_exit;
			}
		}

		fld1 = strtok(elemkey, FLDSEP);
		if ((fld1 == NULL) || (strcmp(fld1, TASKPREFIX) != 0)) {
			delete outelem;
			continue;
		}

		fld2 = strtok(NULL, FLDSEP);
		if ((fld2 == NULL) || (strcmp(fld2, taskname) != 0)) {
			delete outelem;
			continue;
		}

		fld3 = strtok(NULL, FLDSEP);
		if (fld3 == NULL) {
			delete outelem;
			continue;
		}

		fld4 = strtok(NULL, FLDSEP);
		if ((fld4 == NULL) ||
		    (strcmp(fld4, property_to_update) != 0)) {
			delete outelem;
			continue;
		}

		if (strcmp(elemdata, state) != 0) {
			initcount++;
		}

		delete outelem;
	}

	if (e.exception()) {
		os::printf(gettext(
		    "%s: Error traversing postconfig table\n"),
		    progname);
		rstatus = 3;
		goto update_exit;
	}

	CORBA::release(tabptr);

	transptr->update_element(ccrkey, state, e);
	if ((ex = e.exception()) != NULL) {
		interprete_ccr_error(ex);
		os::printf(gettext(
		    "%s: Failed to update postconfig table for the given "
		    "key\n"), progname);
		e.clear();
		transptr->abort_transaction(e);
		rstatus = 3;
		goto update_exit;
	}

	transptr->commit_transaction(e);
	CORBA::release(transptr);
	if (e.exception()) {
		os::printf(gettext(
		    "%s: Failed to update postconfig table\n"), progname);
		rstatus = 3;
		goto update_exit;
	}

	if (sflg) {
		os::printf(gettext(
		    "%s: Number of entries with a different state: %d\n"),
		    progname, initcount - 1);
	}

	CORBA::release(ccr_dir);

update_exit:

	if (taskname)
		free(taskname);
	if (nodename)
		free(nodename);
	if (state)
		free(state);
	if (ccrkey)
		free(ccrkey);

	return (rstatus);
}

int
main(int argc, char **argv)
{
	int uflg = 0;
	int sflg = 0;
	int rstatus = 0;
	int c;

	/* Set the program name */
	if ((progname = strrchr(argv[0], '/')) == NULL) {
		progname = argv[0];
	} else {
		++progname;
	}

	/* I18N housekeeping */
	(void) setlocale(LC_ALL, "");
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);
	(void) textdomain(TEXT_DOMAIN);

	if (sc_zonescheck() != 0)
		return (1);

	while ((c = getopt(argc, argv, "su:")) != EOF) {
		switch (c) {
		case 'u':
			uflg++;
			break;
		case 's':
			sflg++;
			break;
		default:
			usage();
			goto cleanup;
		}
	}

	// Make sure that there are no additional arguments
	if (optind != argc) {
		usage();
		goto cleanup;
	}

	// -s can accompany only -u
	if (sflg && !uflg) {
		os::printf(gettext(
		    "%s: -s can not be specified alone\n"), progname);
		usage();
		rstatus = 1;
		goto cleanup;
	}

	// The only main option supported at present is -u
	if (!uflg) {
		os::printf(gettext(
		    "%s: No operation specified\n"), progname);
		usage();
		rstatus = 1;
		goto cleanup;
	}

	// Reset optind
	optind = 1;

	while ((c = getopt(argc, argv, "su:")) != EOF) {
		switch (c) {
		case 'u':
			rstatus = update_ccr(optarg, sflg);
			break;
		case 's':
			break;
		default:
			usage();
			goto cleanup;
		}
	}

cleanup:
	return (rstatus);
}
