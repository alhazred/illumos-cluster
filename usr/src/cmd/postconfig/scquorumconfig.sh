#!/bin/sh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)scquorumconfig.sh	1.7	08/05/20 SMI"
#
# usr/src/cmd/postconfig/scquorumconfig.sh
#
#	scquorumconfig [-i]
#
#	Perform the automatic quorum configuration task during the first 
#	reboot after install. With optional -i, skip the addition of quorum
#	disks and just perform cluster initialization steps.
#
#
# Returns 0 if the task was successfully performed,
#	  1 if there was an error
#	  2 if there was no need to perform the task
#

/usr/cluster/lib/sc/sc_zonescheck
if [ $? -ne 0 ]; then
	exit 1
fi

PROGNAME=scquorumconfig
POSTCONFIGCCR_CMD=/usr/cluster/lib/sc/postconfigccr
SCCONF_CMD=/usr/cluster/bin/scconf

iflg=0
while getopts i c 2>/dev/null
do
	case ${c} in
	i)	# Set iflg
		iflg=1
		;;

	*)	# Unknown option
		echo "${PROGNAME}: usage error, bad option(s) specified"
		return 1
		;;
	esac
done
		
nodename=`/sbin/uname -n`
cmd_out=`${POSTCONFIGCCR_CMD} -s -u task=quorum,state=BOOTED,nodename=${nodename} 2>&1`
exit_status=$?

if [ ${exit_status} -ne 0 ]; then
	echo ${cmd_out}
	echo "${PROGNAME}: ccr operation failed"
	exit 1
fi

num_printed=`echo ${cmd_out} | awk -F: '{print $3}'`

if [ -z "${num_printed}" ]; then
	exit 2
fi

if [ ${num_printed} -eq 0 ]; then
	exit_status=0
	if [ ${iflg} -eq 0 ]; then
		echo "Configuring Sun Cluster quorum..."
		${SCCONF_CMD} -v -a -q autoconfig
		exit_status=$?
	fi
	if [ ${exit_status} -eq 0 ]; then
		${SCCONF_CMD} -c -q reset
		exit_status_2=$?
		if [ ${exit_status_2} -eq 0 ]; then
			${SCCONF_CMD} -a -T node=.
			exit_status_3=$?
			if [ ${exit_status_3} -eq 0 ]; then
				exit 0
			else
				echo "${PROGNAME}: Failed to reset node authentication list"
				exit 1
			fi	
		else
			echo "${PROGNAME}: Failed to reset quorum devices"
			exit 1
		fi
	else
		echo "${PROGNAME}: Quorum autoconfig failed"
		exit 1
	fi
else
	exit 2
fi
