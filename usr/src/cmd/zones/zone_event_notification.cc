//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)zone_event_notification.cc	1.7	08/07/21 SMI"

//
// The zone_event_notification class provides part of the server-side code
// for the libsczones library (the other part is in libzone_server.cc file).
// This code makes the callbacks to the clients.
// It is compiled into the sc_zonesd server, which runs in each global
// zone (but not non-global zones)
//

// local includes
#include "zone_event_notification.h"
#include "zonesd_common.h"

// system includes
#include <pthread.h>
#include <door.h>

// SC includes
#include <rgm/rgm_msg.h>


using namespace std;

// The one zone_event_notification object
zone_event_notification* zone_event_notification::the_notifier = NULL;
pthread_mutex_t zone_event_notification::create_lock =
    PTHREAD_MUTEX_INITIALIZER;

zone_event_notification&
zone_event_notification::instance()
{
	if (the_notifier == NULL) {
		create();
	}

	return (*the_notifier);
}

void
zone_event_notification::create()
{
	// allow only one thread at a time to attempt to create
	// The notifier in order to avoid creating multiple notifiers
	// in a race condition.
	CL_PANIC(pthread_mutex_lock(&create_lock) == 0);
	if (the_notifier == NULL) {
		the_notifier = new zone_event_notification();
	}
	CL_PANIC(pthread_mutex_unlock(&create_lock) == 0);
}

zone_event_notification::zone_event_notification()
{
	char buf[128];

	// initialize the NUM_NOTIFICATION_THREADS thread pools
	for (int i = 0; i < NUM_NOTIFICATION_THREADS; i++) {
		// Construct the threadpool name
		(void) snprintf(buf, 128, "zone state notification %d", i);

		// Specify that the threadpool should not grow dynamically
		// and it should have one thread. Also specify the name.
		tps[i].modify(false, 1, buf);
	}
}


void
zone_event_notification::queue_notification(const string& zonename,
    sc_zone_state_t state_in, int fd_in)
{
	//
	// Create a new task and stick it on the queue
	// for the threadpool to process
	//
	// In order to obtain "sticky" threads, such
	// that the same thread processes all state notification for
	// a given clientt, we keep an array of single-thread
	// thread pools, and hash the client door fd to a
	// threadpool.
	//
	zone_state_task *zt = new zone_state_task(zonename, state_in, fd_in);
	debug_msg(NOGET("Queueing state task for zone %s, state %d\n"),
	    zonename.c_str(), state_in);
	tps[fd_in % NUM_NOTIFICATION_THREADS].defer_processing(zt);
}

// put this code here instead of the header, because we've included string
// here and are using namespace std and so know about string copy.
zone_state_task::zone_state_task(const std::string& zonename_in,
    sc_zone_state_t state_in, int fd_in) : zonename(zonename_in),
    state(state_in), fd(fd_in)
{
}

void
zone_state_task::execute()
{
	// The "task" here is to send a door call to the specified client

	sc_syslog_msg_handle_t handle;
	door_arg_t arg;
	int err;
	char *data;
	int data_size = zonename.size() + 1 + sizeof (sc_zone_state_t);
	data = (char *)malloc(data_size);
	if (data == NULL) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_ZONESD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    SYSTEXT("low memory"));
		sc_syslog_msg_done(&handle);
		delete this;
		_exit(1);
	}

	//
	// marshall the two arguments. The order is the state
	// (as four bytes) followed immediately by the zonename.
	// This won't work if client and server have different byte
	// orders!!
	//
	memcpy(data, (char *)&state, sizeof (sc_zone_state_t));
	strcpy(data + sizeof (sc_zone_state_t), zonename.c_str());

	arg.data_ptr = data;
	arg.data_size = data_size;
	arg.desc_ptr = NULL;
	arg.desc_num = 0;
	arg.rbuf = NULL;
	arg.rsize = 0;

	// No need to block signals here because in main we initialized
	// every thread to block all signals.
	//
	// We retry at most twice for EAGAIN or EINTR before giving up.
	//
	for (int i = 0; i < 3; i++) {
		if (door_call(fd, &arg) != 0) {
			err = errno;

			if (i < 2 && (err == EAGAIN || err == EINTR)) {
				os::sc_syslog_msg logger(
				    SC_SYSLOG_RGM_ZONESD_TAG, "", NULL);
				/*
				 * SCMSGS
				 * @explanation
				 * The door_call failed with the specified
				 * reason.
				 * @user_action
				 * No action necessary.
				 */
				logger.log(LOG_ERR, MESSAGE,
				    SYSTEXT("door_call: %s; will retry"),
				    strerror(err));

				// sleep, then retry
				sleep(i + 1);
			} else {
				// Assume this client is dead.
				// Assume all clients are imperative.
				// Thus, we abort and let failfast
				// take down the node.
				os::sc_syslog_msg logger(
				    SC_SYSLOG_RGM_ZONESD_TAG, "", NULL);
				/*
				 * SCMSGS
				 * @explanation
				 * The door_call failed with the specified
				 * reason. The program will be terminated and
				 * the node will be rebooted or halted.
				 * @user_action
				 * Save a copy of the /var/adm/messages files
				 * on all nodes. Contact your authorized Sun
				 * service provider for assistance in
				 * diagnosing the problem.
				 */
				logger.log(LOG_ERR, MESSAGE,
				    SYSTEXT("door_call: %s; (skip) aborting"),
				    strerror(err));
				break;
				// free(data);
				// delete this;
				// don't exit now, for testing
				// exit(1);
			}
		} else {
			// successful call
			break;
		}
	}
	free(data);
	delete this;
}

// the string frees itself
zone_state_task::~zone_state_task()
{
}
