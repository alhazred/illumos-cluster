//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef _ZONE_EVENT_NOTIFICATION_H
#define	_ZONE_EVENT_NOTIFICATION_H

#pragma ident	"@(#)zone_event_notification.h	1.3	08/05/20 SMI"

//
// The zone_event_notification class provides part of the server-side code
// for the libsczones library (the other part is in libzone_server.cc file).
// This code makes the callbacks to the clients.
// It is compiled into the sc_zonesd server, which runs in each global
// zone (but not non-global zones)
//

#include <sys/threadpool.h>
#include <rgm/sczones_common.h>

// cpp includes
#include <string>

// we don't expect a lot of clients
const int NUM_NOTIFICATION_THREADS = 4;

//
// zone_state_task
//
// This task is used to represent one state change that needs
// to be sent to one client
//

/* We don't want to define the default ctor */
/*lint -e1712 */
class zone_state_task : public defer_task {
public:
	zone_state_task(const std::string& zonename_in,
	    sc_zone_state_t state_in, int fd_in);
	~zone_state_task();

	void execute();
private:
	// Disallow assignment and pass by value
	zone_state_task(const zone_state_task &);

	// CSTYLED
	zone_state_task &operator=(const zone_state_task &);

	std::string zonename;
	sc_zone_state_t state;
	int fd;
};
/*lint +e1712 */

//
// The class follows the singleton pattern.
//
class zone_event_notification {
public:
	static void create();
	static zone_event_notification& instance();

	void queue_notification(const std::string& zonename,
	    sc_zone_state_t state_in, int fd_in);

protected:
	zone_event_notification();

	// Don't provide implementations for these
	zone_event_notification(const zone_event_notification& src);
	zone_event_notification& operator=(const zone_event_notification& rhs);

	// keep an array of single-threaded threadpools so we can control
	// the allocation of tasks to threads
	threadpool tps[NUM_NOTIFICATION_THREADS];

	static zone_event_notification* the_notifier;
	static pthread_mutex_t create_lock;
};

#endif /* _ZONE_EVENT_NOTIFICATION_H */
