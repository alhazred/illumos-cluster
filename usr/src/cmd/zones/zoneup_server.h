//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef	_ZONEUP_SERVER_H
#define	_ZONEUP_SERVER_H

#pragma ident	"@(#)zoneup_server.h	1.4	08/05/20 SMI"

//
// This file contains the definitions and prototypes used to implement
// the zoneup door server interface.
// The sc_zonesd daemon implements the server side and the sc_canary
// is the client.
//


//
// Creates a door for the zoneup_call.
// Called by the sc_zonesd daemon during its startup phase
// to create the door server.
//
void zoneup_register();

//
// Attach to the file with well-known location to create the file rendezvous.
// Called by the sc_zonesd daemon each time a new non-global zone is booting
// to attach the door server to a door file located in the non-global zone.
//
void zoneup_attach(const char *zone_name);

//
// Remove the door file rendezvous.
// Called by the sc_zonesd daemon each time a new non-global zone is halting
// to detach the door server from the door file located in the non-global zone.
//
void zoneup_detach(const char *zone_name);

#endif /* _ZONEUP_SERVER_H */
