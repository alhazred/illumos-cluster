//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)canary_main.cc	1.8	08/07/21 SMI"

//
// This file contains the source for the sc_canary process that
// runs in each non-global zone once at startup. It does not run
// in the global zone. Its responsibility is to notify the sc_zonesd
// process in the global zone that the non-global zone is up and ready
// to host services. It is a door client.
//

// standard includes
#include <string.h>
#include <stdarg.h>
#include <locale.h>
#include <pthread.h>
#include <zone.h>
#include <sys/stat.h>

#include <new>

// SC includes
#include<rgm/sczones.h>
#include<rgm/sczones_common.h>
#include <sys/rsrc_tag.h>
#include <rgm/rgm_msg.h>
#include <sys/os.h>
#include <rgm/security.h>


#define	CANARY_BUFSIZ 256

#define	RETRY_COUNT 60

bool debug = false;
pid_t pmf_pid = 0;

static void contact_global_zone(void);
static void debug_msg(char *fmt, ...);
static void parse_cmd_args(int argc, char **argv, const char *progname);
static void usage(const char *progname);

int
main(int argc, char **argv)
{
	//
	// set message path and file name
	//
	(void) setlocale(LC_ALL, "");
	(void) textdomain(TEXT_DOMAIN);
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	(void) sc_syslog_msg_set_syslog_tag(SC_SYSLOG_RGM_CANARY_TAG);

	char *progname;
	if ((progname = strrchr(argv[0], '/')) == NULL)
		progname = argv[0];
	else
		progname++;

	parse_cmd_args(argc, argv, progname);

	//
	// Check if the process was started by the superuser.
	//
	if (getuid() != 0) {
		os::sc_syslog_msg logger(SC_SYSLOG_RGM_CANARY_TAG, "", NULL);
		(void) logger.log(LOG_ERR, MESSAGE,
		    SYSTEXT("fatal: must be superuser to start %s"),
		    progname);
		_exit(1);
	}

	(void) std::set_new_handler(&abort);

	//
	// Tell the daemon in the global zone that this zone is
	// ready to host services.
	//
	contact_global_zone();

	// Now that we've completed our task, go ahead and exit
	return (0);
}

static void
contact_global_zone(void)
{
	int err;
	int serverfd;
	char zone_name[ZONENAME_MAX];
	door_arg_t door_arg;
	char *arg_buf;
	size_t arg_size;
	XDR xdrs;
	zoneup_args_t zoneup_args;
	pcparms_t parms;

	//
	// Retrieve the zonename
	//
	if (getzonenamebyid(getzoneid(), zone_name, sizeof (zone_name)) < 0) {
		int err = errno;
		os::sc_syslog_msg logger(SC_SYSLOG_RGM_CANARY_TAG, "",
		    NULL);
		/*
		 * SCMSGS
		 * @explanation
		 * getzonenamebyid failed for the specified reason. The
		 * specified program will be terminated. This zone will not be
		 * able to host applications.
		 * @user_action
		 * Save a copy of the /var/adm/messages files on all nodes.
		 * Contact your authorized Sun service provider for assistance
		 * in diagnosing the problem.
		 */
		(void) logger.log(LOG_ERR, MESSAGE,
		    SYSTEXT("getzonenamebyid: %s"),
		    strerror(err));
		abort();
	}
	if (sc_get_rtparameters(&parms) != 0) {
		os::sc_syslog_msg logger(SC_SYSLOG_RGM_CANARY_TAG, "",
		    NULL);
		/*
		 * SCMSGS
		 * @explanation
		 * Could not query Real Time parameters for the system. The
		 * process will be aborted and this zone will not be available
		 * to host applications.
		 * @user_action
		 * Save a copy of the /var/adm/messages files on all nodes.
		 * Contact your authorized Sun service provider for assistance
		 * in diagnosing the problem.
		 */
		(void) logger.log(LOG_ERR, MESSAGE,
		    SYSTEXT("Failed to get RT parameters"));
		abort();
	}

	//
	// we already have the pmf pid passed as a command-line arg
	//
	debug_msg("Calling %s for %s pid %d\n", ZONEUP_SERVICE,
	    zone_name, pmf_pid);

	// Open the server door
	if ((serverfd = get_door_syscall(RGM_ZONEUP_CODE, GLOBAL_ZONENAME))
		== -1) {
		err = errno;
		os::sc_syslog_msg logger(SC_SYSLOG_RGM_CANARY_TAG, "", NULL);
		/*
		 * SCMSGS
		 * @explanation
		 * open failed for the specified reason. The specified program
		 * will be terminated and this zone will not be able to host
		 * applications.
		 * @user_action
		 * Save a copy of the /var/adm/messages files on all nodes.
		 * Contact your authorized Sun service provider for assistance
		 * in diagnosing the problem.
		 */
		logger.log(LOG_ERR, MESSAGE,
		    SYSTEXT("open: %s"), strerror(err));
		abort();
	}

	zoneup_args.pid = -1;
	zoneup_args.parms = parms;
	zoneup_args.zonename = zone_name;
	arg_size = zoneup_xdr_sizeof(zoneup_args);
	arg_buf = (char *)calloc(1, arg_size);
	if (arg_buf == NULL) {
		os::sc_syslog_msg logger(SC_SYSLOG_RGM_CANARY_TAG, "", NULL);
		/*
		 * SCMSGS
		 * @explanation
		 * Unable to allocate memory. The program will be terminated
		 * and this zone will not be able to host applications.
		 * @user_action
		 * Save a copy of the /var/adm/messages files on all nodes.
		 * Contact your authorized Sun service provider for assistance
		 * in diagnosing the problem.
		 */
		logger.log(LOG_ERR, MESSAGE,
		    SYSTEXT("low memory"));
		abort();
	}

	// Initialize the XDR Stream for Encoding data
	xdrmem_create(&xdrs, arg_buf, (uint_t)arg_size, XDR_ENCODE);

	//
	// Marshall the three arguments. The order is the pmf_pid, followed by
	// the pcparms and then the zonename
	//
	if (!xdr_zoneup_args(&xdrs, &zoneup_args)) {
		os::sc_syslog_msg logger(SC_SYSLOG_RGM_CANARY_TAG, "", NULL);
		/*
		 * SCMSGS
		 * @explanation
		 * An internal error has occurred in the inter-process
		 * communication between Sun Cluster processes. Related error
		 * messages might be found near this one in the syslog output.
		 * This zone will not be able to host applications.
		 * @user_action
		 * Contact your authorized Sun service provider to determine
		 * whether a workaround or patch is available.
		 */
		logger.log(LOG_ERR, MESSAGE,
		    SYSTEXT("XDR Error while encoding arguments."));
		abort();
	}

	//
	// The data_ptr points to buffer storing the xdr-serialized input
	// arguments. data_size contains input size. The same buffer is also
	// used for storing the return output. Hence rbuf points to arg_buf.
	// rsize stores maximum size of buffer that can be used. On return from
	// the door_call, data_ptr and data_size conain information about result
	//
	door_arg.data_ptr =  arg_buf;
	door_arg.data_size = xdr_getpos(&xdrs);
	door_arg.desc_ptr = NULL;
	door_arg.desc_num = 0;
	door_arg.rbuf = arg_buf;
	door_arg.rsize = arg_size;

	// Call the door (zoneup_call) in sc_zonesd
	if ((err = make_door_call(serverfd, &door_arg)) != 0) {
		os::sc_syslog_msg logger(SC_SYSLOG_RGM_CANARY_TAG, "", NULL);
		/*
		 * SCMSGS
		 * @explanation
		 * Unable to contact cluster process. The program will be
		 * terminated and this zone will not be able to host
		 *  applications.
		 * @user_action
		 * Save a copy of the /var/adm/messages files on all nodes.
		 * Contact your authorized Sun service provider for assistance
		 * in diagnosing the problem.
		 */
		(void) logger.log(LOG_ERR, MESSAGE,
		    SYSTEXT("Unable to contact sc_zonesd."));
		abort();
	}
	if (door_arg.data_ptr == NULL || door_arg.data_size != sizeof (int) ||
	    *(int *)door_arg.data_ptr != 0) {
		os::sc_syslog_msg logger(SC_SYSLOG_RGM_CANARY_TAG, "", NULL);
		/*
		 * SCMSGS
		 * @explanation
		 * The sc_zonesd in the global zone was unable to service the
		 * request. This zone will not be able to host applications.
		 * @user_action
		 * Verify that the system is not running on low memory. Save a
		 * copy of the /var/adm/messages file on the node.
		 * Contact your authorized Sun service provider for assistance
		 * in diagnosing the problem.
		 */
		(void) logger.log(LOG_ERR, MESSAGE,
		    SYSTEXT("sc_zonesd couldn't service the request."));
		abort();
	}

	free(arg_buf);
	xdr_destroy(&xdrs);

	(void) close(serverfd);
}

static void
debug_msg(char *fmt, ...)
{
	uint_t  len = 0;
	char    str1[CANARY_BUFSIZ], fmt1[CANARY_BUFSIZ];
	pthread_t tid = pthread_self();
	va_list args;

	(void) snprintf(fmt1, CANARY_BUFSIZ, "[%u] %s", tid, fmt);

	//
	// Add newline if there isn't in 'fmt'.
	//
	len = (uint_t)strlen(fmt1);
	if (fmt1[len - 1] != '\n' && len < CANARY_BUFSIZ - 1) {
		fmt1[len] = '\n';
		fmt1[len + 1] = '\0';
	}

	va_start(args, fmt);    //lint !e40
	if (vsnprintf(str1, CANARY_BUFSIZ, fmt1, args) >= CANARY_BUFSIZ) {
		str1[CANARY_BUFSIZ - 1] = '\0';
	}
	va_end(args);

	if (debug) {
		//
		// syslog and write to stderr -- maybe overkill
		//
		os::sc_syslog_msg logger(SC_SYSLOG_RGM_CANARY_TAG, "", NULL);
		(void) logger.log(LOG_ERR, MESSAGE, str1);
		(void) fprintf(stderr, str1);
	}
}


//
// Parse the arguments from the command line.
// -d specifies whether verbose debugging is to be started or not.
// -p specifies the pid of the pmfd in this zone.
//
static void
parse_cmd_args(int argc, char **argv, const char *progname)
{
	int c;
	bool found_pid = false;

	while ((c = getopt(argc, argv, "dp:")) != -1) {
		switch (c) {
		case 'd':
			debug = true;
			break;
		case 'p':
			found_pid = true;
			pmf_pid = strtol(optarg, NULL, 0);
			if (pmf_pid == 0) {
				usage(progname);
			}
			break;
		case '?':
		default:
			usage(progname);
			// NOTREACHED
		}
	}
	if (!found_pid) {
		usage(progname);
	}
}

//
// Print the program usage.
//
static void
usage(const char *progname)
{
	(void) fprintf(stderr, gettext("Usage:\n"));
	(void) fprintf(stderr, "\t%s -p <pmf_pid> [-d]\n", progname);
	_exit(1);
}
