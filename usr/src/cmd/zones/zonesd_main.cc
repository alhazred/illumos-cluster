//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)zonesd_main.cc	1.7	08/05/20 SMI"

//
// This file provides main() and helper methods for sc_zonesd, which
// runs in every global zone (but not non-global zones).
//

// standard includes
#include <unistd.h>
#include <libintl.h>
#include <locale.h>
#include <pthread.h>
#include <signal.h>
#include <sys/stat.h>
#include <dlfcn.h>

// cpp includes
#include <new>

// SC includes
#include <rgm/rgm_msg.h>
#include <rgm/scutils.h>
#include <cmm/cmm_ns.h>

// local includes
#include "zonesd_common.h"
#include <rgm/sczones.h>
#include "event_proxy_server.h"

#define	SCZONESD_LOCK "/var/cluster/run/sczonesd.lock"
#define	SCZONES_BUFSIZ 256


//
// We need to distinguish between Server nodes (or sun cluster) nodes
// and farm nodes. The way to do that is to check if the Farm RGMD daemon
// is installed or not.
//
#define	FRGMD "/usr/cluster/lib/sc/frgmd"

//
// On the cluster nodes, the fail-fast library is different
// depending on the node type (server or farm node).
//
#ifdef __sparcv9
#define	LIBCLST		"/usr/cluster/lib/sparcv9/libclst.so.1"
#define	LIBFCLST	"/usr/cluster/lib/sparcv9/libfclst.so.1"
#else
#define	LIBCLST		"/usr/cluster/lib/libclst.so.1"
#define	LIBFCLST	"/usr/cluster/lib/libfclst.so.1"
#endif

#define	ST_FF_ARM	"st_ff_arm"
#define	ST_FF_DISARM	"st_ff_disarm"

//
// Initialize it in this file (with main()), so that we
// guarantee it's initialized before first use.
//
pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;

bool debug = false;

static bool isfarmnode;

static void parse_cmd_args(int argc, char **argv, const char *progname);
static void usage(const char *progname);
static void sig_handler(void *);
static void init_signal_handlers(void);

static void nodetype_init(void);
static int failfast_arm(char *pname);
static int failfast_disarm();
extern void zoneup_register();

int
main(int argc, char **argv)
{
	int rc;

	//
	// set message path and file name
	//
	(void) setlocale(LC_ALL, "");
	(void) textdomain(TEXT_DOMAIN);
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	if (sc_zonescheck() != 0)
		return (1);

	(void) sc_syslog_msg_set_syslog_tag(SC_SYSLOG_RGM_ZONESD_TAG);

	char *progname;
	if ((progname = strrchr(argv[0], '/')) == NULL)
		progname = argv[0];
	else
		progname++;

	parse_cmd_args(argc, argv, progname);

	//
	// Check if the process was started by the superuser.
	//
	if (getuid() != 0) {
		os::sc_syslog_msg logger(SC_SYSLOG_RGM_ZONESD_TAG, "", NULL);
		logger.log(LOG_ERR, MESSAGE,
		    SYSTEXT("fatal: must be superuser to start %s"),
		    progname);
		_exit(1);
	}

	(void) std::set_new_handler(&abort);

	//
	// Detach the process from the controlling terminal.
	//
	make_daemon();

	//
	// ensure that another sc_zonesd daemon is not running
	// This must be done after the fork!
	//
	make_unique(SCZONESD_LOCK);

	//
	// set up the signal handlers before we fork any threads
	//
	init_signal_handlers();

	//
	// Initialize the failfast driver.
	//  st_ff_arm returns error codes from the errno set, so use
	//  strerror to read them.
	if (failfast_arm("sc_zonesd") != 0) {
		os::sc_syslog_msg logger(SC_SYSLOG_RGM_ZONESD_TAG, "", NULL);
		logger.log(LOG_ERR, MESSAGE,
		    "st_ff_arm failed: %s", strerror(rc));
		abort();
	}

	// Create a door server to handle the zone up event coming from
	// non-global zones (sent by the short-lived sc_canary process).
	zoneup_register();

	// set up event listener
	zone_event_register();

	// Set up the door so clients can register for callbacks
	// This should be the last thing we do because it's what
	// the init script waits for to determine if we're up.
	libzone_server_register();

	// Create a door server to handle events published from non-global
	// zones.
	(void) sc_event_proxy_daemon_start();

	// thr_exit (instead of a pause loop) is ok here, because the orb
	// server initialization creates a thread pool, so there will always
	// be threads to maintain the process.
	thr_exit(NULL);

	// NOTREACHED
	return (0);
}

static void
nodetype_init(void)
{
	struct stat filestat;

	if (stat(FRGMD, &filestat) < 0)
		isfarmnode = false;
	else
		isfarmnode = true;
}

static int failfast_disarm() {
	void *dlhandle;
	int (*fn_ff_disarm)(void);
	char *ff_lib = LIBCLST;

	nodetype_init();
	if (isfarmnode) {
		ff_lib = LIBFCLST;
	}
	if ((dlhandle = dlopen(ff_lib, RTLD_LAZY)) == NULL) {
		return (-1);
	}
	if ((fn_ff_disarm =
		(int(*)())dlsym(dlhandle, ST_FF_DISARM)) /*lint !e611 */
		== NULL) {
		return (-1);
	}
	return ((*fn_ff_disarm)());
}

static int failfast_arm(char *pname) {
	void *dlhandle;
	int (*fn_ff_arm)(char *);
	char *ff_lib = LIBCLST;

	nodetype_init();
	if (isfarmnode) {
		ff_lib = LIBFCLST;
	}
	if ((dlhandle = dlopen(ff_lib, RTLD_LAZY)) == NULL) {
		return (-1);
	}
	if ((fn_ff_arm = (int(*)(char *))
		dlsym(dlhandle, ST_FF_ARM)) /*lint !e611 */
		== NULL) {
		return (-1);
	}
	return ((*fn_ff_arm)(pname));
}

//
// Parse the arguments from the command line.
// -d specifies whether verbose debugging is to be started or not.
//
static void
parse_cmd_args(int argc, char **argv, const char *progname)
{
	int c;

	while ((c = getopt(argc, argv, "d")) != -1) {
		switch (c) {
		case 'd':
			debug = true;
			break;
		case '?':
		default:
			usage(progname);
			// NOTREACHED
		}
	}
}

//
// Print the program usage.
//
static void
usage(const char *progname)
{
	(void) fprintf(stderr, gettext("Usage:\n"));
	(void) fprintf(stderr, "\t%s [-d]\n", progname);
	_exit(1);
}

void
debug_msg(char *fmt, ...)
{
	uint_t  len = 0;
	char    str1[SCZONES_BUFSIZ], fmt1[SCZONES_BUFSIZ];
	pthread_t tid = pthread_self();
	va_list args;

	(void) snprintf(fmt1, SCZONES_BUFSIZ, "[%u] %s", tid, fmt);

	// Add newline if there isn't in 'fmt'.
	len = (uint_t)strlen(fmt1);
	if (fmt1[len - 1] != '\n' && len < SCZONES_BUFSIZ - 1) {
		fmt1[len] = '\n';
		fmt1[len + 1] = '\0';
	}

	va_start(args, fmt);    //lint !e40
	if (vsnprintf(str1, SCZONES_BUFSIZ, fmt1, args) >= SCZONES_BUFSIZ) {
		str1[SCZONES_BUFSIZ - 1] = '\0';
	}
	va_end(args);

	if (debug) {
		// syslog and write to stderr -- maybe overkill
		os::sc_syslog_msg logger(SC_SYSLOG_RGM_ZONESD_TAG, "", NULL);
		logger.log(LOG_ERR, MESSAGE, str1);
		(void) fprintf(stderr, str1);
	}
}

//
// init_signal_handlers
// ---------------------
// Blocks all signals in this thread, which will be the parent of the
// other threads, so that they will inherit the signal mask.
// This is especially important in those threads that will be handling
// and making door calls.
//
// Creates a thread to handle all the signals.  It runs the sig_handler
// method below.
//
// If we fail in any of this, exit the daemon.
//
static void
init_signal_handlers(void)
{
	sc_syslog_msg_handle_t handle;

	// block all signals for all threads
	sigset_t sig_set;
	int err;

	if (sigfillset(&sig_set) != 0) {
		err = errno;
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_ZONESD_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * The daemon was unable to configure its signal handling
		 * functionality, so it is unable to run.
		 * @user_action
		 * Save a copy of the /var/adm/messages files on all nodes and
		 * contact your authorized Sun service provider for assistance
		 * in diagnosing and correcting the problem.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "sigfillset: %s", strerror(err));
		sc_syslog_msg_done(&handle);
		_exit(err);
	}

	if ((err = pthread_sigmask(SIG_BLOCK, &sig_set, NULL)) != 0) {
		char *err_str = strerror(err);
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_ZONESD_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * The daemon was unable to configure its signal handler, so
		 * it is unable to run.
		 * @user_action
		 * Save a copy of the /var/adm/messages files on all nodes and
		 * contact your authorized Sun service provider for assistance
		 * in diagnosing and correcting the problem.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "pthread_sigmask: %s", err_str ? err_str : "");
		sc_syslog_msg_done(&handle);
		_exit(err);
	}

	// now spawn the thread that will actually handle the signals
	pthread_t sig_thr;
	if ((err = pthread_create(&sig_thr, NULL,
	    (void *(*)(void *))sig_handler, NULL)) != 0) {
		char *err_str = strerror(err);
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_ZONESD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "pthread_create: %s", err_str ? err_str : "");
		sc_syslog_msg_done(&handle);
		_exit(err);
	}
}


//
// sig_handler
// --------------
// Signal handler
//
// This function runs in a dedicated signal handling thread.  The thread
// should be created with a signal mask blocking all signals.
//
// Uses an infinite loop around sigwait, to wait for all signals.
// Exits on SIGTERM, continues on all others.
//
// If anything goes awry, exits immediately.
//
static void
sig_handler(void *)
{
	sc_syslog_msg_handle_t handle;

	sigset_t sig_set;
	int err, sig;

	if (sigfillset(&sig_set) != 0) {
		err = errno;
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_ZONESD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "sigfillset: %s", strerror(err));
		sc_syslog_msg_done(&handle);
		_exit(err);
	}

	while (true) {
		if ((sig = sigwait(&sig_set)) == -1) {
			err = errno;
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_ZONESD_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * The daemon was unable to configure its signal
			 * handling functionality, so it is unable to run.
			 * @user_action
			 * Save a copy of the /var/adm/messages files on all
			 * nodes and contact your authorized Sun service
			 * provider for assistance in diagnosing and
			 * correcting the problem.
			 */
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "sigwait: %s", strerror(err));
			sc_syslog_msg_done(&handle);
			_exit(err);
		}

		switch (sig) {
		case SIGTERM:
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_ZONESD_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * The daemon indicated in the message tag has
			 * received a SIGTERM signal, possibly caused by an
			 * operator-initiated kill(1) command. The daemon will
			 * produce a core file and will force the node to halt
			 * or reboot to avoid the possibility of data
			 * corruption.
			 * @user_action
			 * The operator must use clnode and shutdown to take
			 * down a node, rather than directly killing the
			 * daemon.
			 */
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "fatal: received signal %d", sig);
			sc_syslog_msg_done(&handle);

			// disarm the failfast only for Farm nodes.
			if (isfarmnode) {
				(void) failfast_disarm();
				_exit(0);
			}

			// now exit
			_exit(1);
			break;
		default:
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_ZONESD_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * The daemon indicated in the message tag has
			 * received a signal, possibly caused by an
			 * operator-initiated kill(1) command. The signal is
			 * ignored.
			 * @user_action
			 * The operator must use clnode and shutdown to take
			 * down a node, rather than directly killing the
			 * daemon.
			 */
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "received signal %d: continuing", sig);
			sc_syslog_msg_done(&handle);
		}
	}
}
