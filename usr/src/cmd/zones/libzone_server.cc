//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)libzone_server.cc	1.4	08/05/20 SMI"

//
// This file contains part of the server-side code for libsczones.
// It is part of the sc_zonesd process that runs in each global zone.
// The other part is in the zone_event_notification class.
//

/* SC includes */
#include <rgm/sczones_common.h>
#include <sys/sc_syslog_msg.h>
#include <sys/rsrc_tag.h>
#include <rgm/rgm_msg.h>
#include <sys/cl_assert.h>
#include <sys/os.h>

/* system includes */
#include <door.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <stropts.h>
#include <unistd.h>
#include <pthread.h>

// local includes
#include "zonesd_common.h"
#include "zone_state_manager.h"
#include "zone_event_notification.h"

// cpp includes
#include <list>
#include <string>
using namespace std;

// This list stores the file descriptors of the doors
static std::list<int> cb_fds;

/*
 * The incoming registration should give us one descriptor for
 * callbacks
 *
 * It also passes one int in the data field, which is 0 or 1 to
 * specify a registration or unregistration.
 *
 * It returns (with door_return) one int of error code.
 */
static void
incoming_registration(void *cookie, char *dataptr, size_t datasize,
    door_desc_t *descptr, uint_t ndesc)
{
	int res = 0;

	/* Check the number of descriptor */
	if (ndesc != 1) {
		os::sc_syslog_msg logger(SC_SYSLOG_RGM_ZONESD_TAG, "", NULL);
		/*
		 * SCMSGS
		 * @explanation
		 * A libsczones registration for zone state callbacks was
		 * improperly formatted. It will be ignored.
		 * @user_action
		 * Look for messages from clients of libsczones such as rgmd
		 * and rpc.fed to determine why they sent an improperly
		 * formatted registration. Save a copy of the
		 * /var/adm/messages files on all nodes. Contact your
		 * authorized Sun service provider for assistance in
		 * diagnosing the problem.
		 */
		logger.log(LOG_ERR, MESSAGE,
		    SYSTEXT("incoming_registration: "
			"invalid number of descriptors"));
		res = 1;
		door_return((char *)&res, sizeof (int), NULL, 0);
	}

	// check the size of the dataptr
	if (datasize != sizeof (int)) {
		os::sc_syslog_msg logger(SC_SYSLOG_RGM_ZONESD_TAG, "", NULL);
		/*
		 * SCMSGS
		 * @explanation
		 * A libsczones registration for zone state callbacks was
		 * improperly formatted. It will be ignored.
		 * @user_action
		 * Look for messages from clients of libsczones such as rgmd
		 * and rpc.fed to determine why they sent an improperly
		 * formatted registration. Save a copy of the
		 * /var/adm/messages files on all nodes. Contact your
		 * authorized Sun service provider for assistance in
		 * diagnosing the problem.
		 */
		logger.log(LOG_ERR, MESSAGE,
		    SYSTEXT("incoming_registration: invalid dataptr size"));
		res = 1;
		door_return((char *)&res, sizeof (int), NULL, 0);
	}

	CL_PANIC(pthread_mutex_lock(&lock) == 0);

	// Check whether it's a registration or unregistration
	if (*(int *)dataptr == 1) {
		// registration -- add the fd to the list
		debug_msg("Incoming reg\n");

		// Check for duplicates??
		cb_fds.push_back(descptr->d_data.d_desc.d_descriptor);

		// Send current state of all known zones
		zone_state_manager::instance().send_all_state(
			descptr->d_data.d_desc.d_descriptor);
	} else {
		debug_msg("Incoming unreg\n");
		cb_fds.remove(descptr->d_data.d_desc.d_descriptor);
	}

	CL_PANIC(pthread_mutex_unlock(&lock) == 0);

	/* That's it! */
	door_return((char *)&res, sizeof (int), NULL, 0);
}

/*
 * Creates a door for the registrations and attach it to the file
 * with well-known location to create the file rendezvous.
 */
void
libzone_server_register()
{
	int myfd, tempfd;
	int err;

	/* create the door */
	if ((myfd = door_create(incoming_registration, NULL, 0)) == -1) {
		err = errno;
		os::sc_syslog_msg logger(SC_SYSLOG_RGM_ZONESD_TAG, "", NULL);
		/*
		 * SCMSGS
		 * @explanation
		 * door_create failed for the specified reason. The specified
		 * program will be terminated.
		 * @user_action
		 * Save a copy of the /var/adm/messages files on all nodes.
		 * Contact your authorized Sun service provider for assistance
		 * in diagnosing the problem.
		 */
		logger.log(LOG_ERR, MESSAGE,
		    SYSTEXT("door_create: %s"), strerror(err));

		_exit(1);
	}

	// remove any remnant that might exist at this location
	(void) unlink(SCZONES_LIB_SERVER_PATH);

	// Now create the file
	if ((tempfd = open(SCZONES_LIB_SERVER_PATH, O_CREAT | O_RDWR,
	    S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH)) <= 0) {
		err = errno;
		os::sc_syslog_msg logger(SC_SYSLOG_RGM_ZONESD_TAG, "", NULL);
		logger.log(LOG_ERR, MESSAGE,
		    SYSTEXT("open: %s"), strerror(err));

		_exit(1);
	}

	(void) close(tempfd);

	// attach the door to the file
	if (fattach(myfd, SCZONES_LIB_SERVER_PATH) != 0) {
		err = errno;
		os::sc_syslog_msg logger(SC_SYSLOG_RGM_ZONESD_TAG, "", NULL);
		/*
		 * SCMSGS
		 * @explanation
		 * fattach failed for the specified reason. The specified
		 * program will be terminated.
		 * @user_action
		 * Save a copy of the /var/adm/messages files on all nodes.
		 * Contact your authorized Sun service provider for assistance
		 * in diagnosing the problem.
		 */
		logger.log(LOG_ERR, MESSAGE,
		    SYSTEXT("fattach: %s"), strerror(err));

		_exit(1);
	}
}

// caller is expected to hold the lock already
void
trigger_callbacks(const std::string& zonename,
    zone_state_manager::zone_state state)
{
	//
	// Internaly in the sc_zonesd we keep track of a few
	// transitory zone states about which we don't inform clients.
	// We map them to the client visible state.
	//
	sc_zone_state_t visible_state;
	if (convert_state_int_to_ext(state, visible_state) != 0) {
		os::sc_syslog_msg logger(SC_SYSLOG_RGM_ZONESD_TAG, "", NULL);
		/*
		 * SCMSGS
		 * @explanation
		 * The sc_zonesd has encountered an internal logic error, but
		 * it will attempt to continue.
		 * @user_action
		 * Save a copy of the /var/adm/messages files on all nodes. If
		 * the problem persists, contact your authorized Sun service
		 * provider for assistance in diagnosing the problem.
		 */
		logger.log(LOG_ERR, MESSAGE,
		    SYSTEXT("Attempting to notify client of invalid "
		    "state for zone %s"), zonename.c_str());
		return;
	}

	// iterate through each door fd in the list, queueing an
	// event for each one
	zone_event_notification& notifier =
	    zone_event_notification::instance();

	// CSTYLED
	for (std::list<int>::const_iterator it = cb_fds.begin();
	    it != cb_fds.end(); ++it) {
		notifier.queue_notification(zonename, visible_state,
		    *it);
	}
}

int
convert_state_int_to_ext(zone_state_manager::zone_state state,
    sc_zone_state_t &ext_state)
{
	switch (state) {
	case zone_state_manager::DOWN:
		ext_state = ZONE_DOWN;
		break;
	case zone_state_manager::STARTING:
		ext_state = ZONE_STARTING;
		break;
	case zone_state_manager::UP:
		ext_state = ZONE_UP;
		break;
	case zone_state_manager::SHUTTING_DOWN:
		ext_state = ZONE_SHUTTING_DOWN;
		break;
	case zone_state_manager::STUCK:
		ext_state = ZONE_STUCK;
		break;
	case zone_state_manager::UNKNOWN: // fall through
	default:
		// error
		ext_state = ZONE_DOWN;
		return (1);
	}
	return (0);
}
