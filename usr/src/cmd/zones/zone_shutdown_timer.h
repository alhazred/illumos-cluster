//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef _ZONE_SHUTDOWN_TIMER_H
#define	_ZONE_SHUTDOWN_TIMER_H

#pragma ident	"@(#)zone_shutdown_timer.h	1.4	08/07/21 SMI"

//
// This class is part of the sc_zonesd daemon that runs in each global
// zone (but not non-global zones).
//

//
// General note: this code is basically copied from the pmf_throttle_wait
// module of the PMF
//

#include <list>
#include <string>

//
// timeout_entry is just a glorified struct, so all methods are public.
// It stores a zone name and the time at which the timeout should complete.
//
class timeout_entry {
public:
	timeout_entry(const char *zonename_in, time_t time_in);
	time_t timeout_time;
	std::string zonename;

	// CSTYLED
	friend bool operator==(const timeout_entry& lhs,
	    const timeout_entry& rhs);
};

class zone_shutdown_timer {
public:
	static zone_shutdown_timer& instance();
	static void create();

	//
	// wait() tells the zone_shutdown_timer
	// module to wait for SHUTDOWN_TIMEOUT seconds for the zone with
	// specified zonename to become stuck.
	//
	// cancel cancels the timeout of that zone.
	//
	void wait(const char *zonename);
	void cancel(const char *zonename);

	// timeout_waits is only public so that the timeout thread
	// can call it
	void timeout_waits();

protected:
	zone_shutdown_timer();

	// Don't provide implementations for these
	zone_shutdown_timer(const zone_shutdown_timer& src);
	zone_shutdown_timer& operator=(const zone_shutdown_timer& rhs);

	pthread_t wait_thread;

	//
	// Use a list instead of a queue so we can remove entries
	// from the middle
	//
	std::list<timeout_entry> to_queue;

	pthread_mutex_t lock;
	pthread_cond_t cond;

	static zone_shutdown_timer* the_shutdown_timer;
	static pthread_mutex_t create_lock;
};

#endif /* _ZONE_SHUTDOWN_TIMER_H */
