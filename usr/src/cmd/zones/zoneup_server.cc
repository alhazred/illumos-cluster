//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)zoneup_server.cc	1.8	08/07/21 SMI"

// system includes
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <zone.h>
#include <priv.h>

// SC includes
#include <rgm/sczones.h>
#include <rgm/sczones_common.h>
#include <rgm/rgm_msg.h>
#include <sys/os.h>
#include <rgm/security.h>

// local includes
#include "zonesd_common.h"

int zoneup_door_id;

static boolean_t security_svc_priv_verify();
//
// The zoneup_call is called to modify process's priority and/or to register a
// zone to the cluster framework. The door arg contains pid, scheduling params,
// and the zonename. The canary process uses the facility to notify a new zone
// is up and ready to host services. It also requests modifying pmf's priority
// to RT.
// If zonename is empty/null, the zone registration is skipped. This
// facility is currently used by scutils in svc_restore_priority()
//
static void
zoneup_call(void *cookie, char *arg_buf, size_t arg_size,
    door_desc_t *descptr, uint_t ndesc)
{
	XDR xdrs;
	zoneup_args_t zoneup_args;
	int retcode = 0;

	if (!security_svc_priv_verify()) {
		retcode = EACCES;
		door_return((char *)&retcode, sizeof (int), NULL, 0);
	}

	memset(&zoneup_args, 0, sizeof (zoneup_args));
	memset(&xdrs, 0, sizeof (xdrs));
	// Sanity check the validity of the buffer passed before
	// deferencing it. This prevent someone to bring down the server
	// by sending a malformed door call.
	if ((arg_buf == NULL) || (arg_size == (size_t)0)) {
		os::sc_syslog_msg logger(SC_SYSLOG_RGM_ZONESD_TAG, "", NULL);
		/*
		 * SCMSGS
		 * @explanation
		 * A zoneup message was improperly formatted. It will be
		 * ignored.
		 * @user_action
		 * Save a copy of the /var/adm/messages files on all nodes.
		 * Contact your authorized Sun service provider for assistance
		 * in diagnosing the problem.
		 */
		logger.log(LOG_ERR, MESSAGE,
		    SYSTEXT("Malformed door call discarded"));
		retcode = EINVAL;
		door_return((char *)&retcode, sizeof (int), NULL, 0);
	}

	// Initialize the XDR Stream for Decoding data
	xdrmem_create(&xdrs, arg_buf, (uint_t)arg_size, XDR_DECODE);

	if (!xdr_zoneup_args(&xdrs, &zoneup_args)) {
		os::sc_syslog_msg logger(SC_SYSLOG_RGM_ZONESD_TAG, "", NULL);
		/*
		 * SCMSGS
		 * @explanation
		 * An internal error has occurred in the inter-process
		 * communication between Sun Cluster processes. Related error
		 * messages might be found near this one in the syslog output.
		 * @user_action
		 * Contact your authorized Sun service provider to determine
		 * whether a workaround or patch is available.
		 */
		logger.log(LOG_ERR, MESSAGE,
		    SYSTEXT("XDR Error while decoding return arguments."));
		retcode = EINVAL;
		goto bailout;
	}
	if ((zoneup_args.pid != -1) &&
	    (retcode = sc_priocntl(zoneup_args.pid, &zoneup_args.parms)) != 0) {
		goto bailout;
	}
	if (zoneup_args.zonename == NULL || zoneup_args.zonename[0] == '\0') {
		// no zone registration requested. Return early.
		goto bailout;
	}
	debug_msg("zone up request from %s\n", zoneup_args.zonename);

	CL_PANIC(pthread_mutex_lock(&lock) == 0);
	//
	// set_zone_state takes care of queueing the notifications
	// to clients and sending an event
	//
	zone_state_manager::instance().set_zone_state(zoneup_args.zonename,
	    zone_state_manager::UP);

	CL_PANIC(pthread_mutex_unlock(&lock) == 0);

bailout:
	free(zoneup_args.zonename);
	xdr_destroy(&xdrs);
	door_return((char *)&retcode, sizeof (int), NULL, 0);
}

//
// Creates a door for the zoneup_call.
//
void
zoneup_register()
{
	int err;

	// Create the door
	if ((zoneup_door_id = security_svc_reg(zoneup_call,
		RGM_ZONEUP_CODE, 0, GLOBAL_ZONENAME))
	    == -1) {
		err = errno;
		os::sc_syslog_msg logger(SC_SYSLOG_RGM_ZONESD_TAG, "", NULL);
		logger.log(LOG_ERR, MESSAGE,
		    SYSTEXT("door_create: %s"), strerror(err));
		abort();
	}

	debug_msg("zoneup_register: zoneup_door_id is %d\n", zoneup_door_id);
}

// check if caller has sys_admin privilege (or has root's credentials)
static boolean_t
security_svc_priv_verify() {
	ucred_t *d_cred = NULL;
	const priv_set_t *set;

	if (door_ucred(&d_cred) != 0)
		return (B_FALSE);
	set = ucred_getprivset(d_cred, PRIV_EFFECTIVE);
	uid_t uid = ucred_geteuid(d_cred);
	ucred_free(d_cred);
	if (set != NULL) {
		return (priv_ismember(set, PRIV_SYS_ADMIN));
	} else {
		return ((boolean_t)(uid == 0));
	}
}
