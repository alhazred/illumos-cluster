//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)zone_state_manager.cc	1.8	08/05/20 SMI"

//
// This class is part of the sc_zonesd daemon that runs in each global
// zone (but not non-global zones).
//

// SC includes
#include <sys/cl_assert.h>
#include <rgm/rgm_msg.h>

// local includes
#include "zone_state_manager.h"
#include "zonesd_common.h"
#include "zone_event_notification.h"

using namespace std;

// The one zone_state_manager instance
zone_state_manager* zone_state_manager::the_manager = NULL;
pthread_mutex_t zone_state_manager::create_lock = PTHREAD_MUTEX_INITIALIZER;

zone_state_manager&
zone_state_manager::instance()
{
	if (the_manager == NULL) {
		create();
	}

	return (*the_manager);
}

void
zone_state_manager::create()
{
	// allow only one thread at a time to attempt to create
	// the table in order to avoid creating multiple tables
	// in a race condition.
	CL_PANIC(pthread_mutex_lock(&create_lock) == 0);
	if (the_manager == NULL) {
		the_manager = new zone_state_manager();
	}
	CL_PANIC(pthread_mutex_unlock(&create_lock) == 0);
}

// caller is expected to hold the lock already
//
// Returns true if the state was set.
//
bool
zone_state_manager::set_zone_state(const string& zone_name, zone_state state)
{
	bool ok_to_set = false;

	// will retrieve UNKNOWN if zone is not yet know in this mapping
	zone_state cur_state = zone_states[zone_name];
	if (cur_state == state) {
		debug_msg("Duplicate state: %d for %s\n", state,
		    zone_name.c_str());
		return (false);
	}

	if (cur_state == UNKNOWN) {
		ok_to_set = true;
	} else {

		/* BEGIN CSTYLED */

		/*
		 * state transition diagram:
		 *
		 * DOWN -> STARTING -> UP
		 * ^             |    |
		 * |---------     \   |
		 * |         \     v  v
		 * STUCK <---- SHUTTING_DOWN
		 *
		 * UNKNOWN can lead to any state,
		 * but we never return there.
		 *
		 * It's possible to skip from STARTING to
		 * SHUTTING_DOWN if the canary process fails to start on
		 * the zone and notify us before the zone shuts down.
		 */

		/* END CSTYLED */

		switch (state) {
		case UNKNOWN:
			ok_to_set = false;
			break;
		case DOWN:
			ok_to_set = (cur_state == STUCK ||
			    cur_state == SHUTTING_DOWN);
			break;
		case STUCK:
			ok_to_set = (cur_state == SHUTTING_DOWN);
			break;
		case SHUTTING_DOWN:
			ok_to_set = (cur_state == UP || cur_state == STARTING);
			break;
		case STARTING:
			ok_to_set = (cur_state == DOWN);
			break;
		case UP:
			ok_to_set = (cur_state == STARTING);
			break;
		default:
			//	should never happen
			CL_PANIC(0);
		}
	}

	if (ok_to_set) {
		// No need to handle exceptions -- if new fails, we'll abort
		// due to the set_new_handler call in main
		zone_states[zone_name] = state;

		debug_msg("Set zone %s state from %d to %d\n",
		    zone_name.c_str(), cur_state, state);

		// queue notifications
		trigger_callbacks(zone_name, state);

		// return true so caller knows we actually set the state
		return (true);
	} else {
		debug_msg("Not setting zone %s state from %d to %d\n",
		    zone_name.c_str(), cur_state, state);
	}
	// if we get here, we didn't actually set the state
	return (false);
}

bool
zone_state_manager::set_zone_state(const char *zone_name, zone_state state)
{
	return (set_zone_state(string(zone_name), state));
}

// iterate through all known zones and queue the state tasks
// on the given fd
void
zone_state_manager::send_all_state(int fd)
{
	zone_event_notification& notifier =
	    zone_event_notification::instance();
	sc_zone_state_t visible_state;

	// CSTYLED
	for (std::map<string, zone_state>::const_iterator it =
	    zone_states.begin(); it != zone_states.end(); ++it) {

		// convert the state
		if (convert_state_int_to_ext(it->second, visible_state) != 0) {
			os::sc_syslog_msg logger(SC_SYSLOG_RGM_ZONESD_TAG, "",
			    NULL);
			logger.log(LOG_ERR, MESSAGE,
			    SYSTEXT("Attempting to notify client of invalid "
				"state for zone %s"), it->first.c_str());
			// skip this one
			continue;
		}
		notifier.queue_notification(it->first, visible_state,
		    fd);
	}
}

zone_state_manager::zone_state_manager()
{
}
