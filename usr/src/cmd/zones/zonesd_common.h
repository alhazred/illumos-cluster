//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef	_ZONESD_COMMON_H
#define	_ZONESD_COMMON_H

#pragma ident	"@(#)zonesd_common.h	1.3	08/05/20 SMI"

//
// Common prototypes and defines for sc_zonesd.
//

// cpp includes
#include <string>

// system includes
#include <pthread.h>

// SC includes
#include <rgm/sczones_common.h>

// local includes
#include "zone_state_manager.h"

// One lock for all access to the stored zone state or the
// cb list. Using two locks leads to deadlock possibilities -- it's
// simpler to use just one.
extern pthread_mutex_t lock;

void debug_msg(char *fmt, ...);

// These functions will exit if any initialization fails
void zone_event_register();
void libzone_server_register();
void trigger_callbacks(const std::string& zonename,
    zone_state_manager::zone_state state);
int convert_state_int_to_ext(zone_state_manager::zone_state state,
    sc_zone_state_t &ext_state);

#endif /* _ZONESD_COMMON_H */
