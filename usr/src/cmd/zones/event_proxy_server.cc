//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//


#pragma ident	"@(#)event_proxy_server.cc	1.6	08/07/21 SMI"
//
// This file has the door_server for the daemon that is
// used to log sysevents from non-global zones.
// The programs in non-global zones that need to log sysevents
// communicate with the door server in the global zone and
// send their requests. Which are in turn logged by the door
// server.
//
#include <zone.h>
#include <sys/os.h>
#include <rgm/security.h>
#include <event_proxy_server.h>
#include <rgm/sczones.h>
#include <zonesd_common.h>
#include <sc_event_proxy/sc_event_proxy.h>
#include <sc_event_proxy_impl.h>
#include <libsysevent.h>
#include <sys/cl_eventdefs.h>

//
// size of return_code is sufficient for both the values it can have
// SUCCESS and FAILURE. The reason we dont use a char* instead is
// that we wont be able to free the char* before calling door_return
// as the data needs to be available at the time of calling door_return
// and there is no way we can get another chance to free it after
// door_return can be called. Thats why we have a variable on the stack
// which gets freed automatically.
//
#define	MAX_RETURN_CODE_SIZE 8

int sc_event_proxy_door_id;

/*lint -e715 -e818 */
static void
sc_event_proxy_daemon(void *cookie, char *arg_buf, size_t arg_size,
    door_desc_t *descptr, uint_t ndesc)
{
	char return_code[MAX_RETURN_CODE_SIZE] = "SUCCESS";
	nvlist *attr_list = NULL;
	sysevent_id_t eid;
	int err = 0;
	proxy_event_data event_data = {0};

	// Sanity check the validity of the buffer passed before
	// deferencing it. This prevent someone to bring down the server
	// by sending a malformed door call.

	if ((arg_buf == NULL) || (arg_size == (size_t)0)) {
		os::sc_syslog_msg logger(SC_SYSLOG_SC_EVENT_PROXY_TAG,
		    "", NULL);
		(void) logger.log(LOG_ERR, MESSAGE,
		    SYSTEXT("Malformed door call discarded"));
		(void) strcpy(return_code, "FAILURE");
		goto proxy_end; /*lint !e801 */
	}
	// Initialize the XDR Stream for Decoding data
	XDR xdrs;
	xdrs.x_ops = NULL;
	xdrmem_create(&xdrs, arg_buf, (uint_t)arg_size, XDR_DECODE);
	if (xdrs.x_ops == NULL) {
		os::sc_syslog_msg logger(SC_SYSLOG_SC_EVENT_PROXY_TAG,
		    "", NULL);
		//
		// SCMSGS
		// @explanation
		// An internal error has occurred in the inter-process
		// communication between Sun Cluster processes. Related error
		// messages might be found near this one in the syslog output.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) logger.log(LOG_ERR, MESSAGE,
		    SYSTEXT("Error while initializing XDR stream"));
		(void) strcpy(return_code, "FAILURE");
		goto proxy_end; /*lint !e801 */
	}

	//
	// Unmarshall the input arguments (XDR Decode the data)
	//
	if (!xdr_proxy_event_data(&xdrs, &event_data)) {
		os::sc_syslog_msg logger(SC_SYSLOG_SC_EVENT_PROXY_TAG,
		    "", NULL);
		(void) logger.log(LOG_ERR, MESSAGE,
		    SYSTEXT("XDR Error while decoding arguments."));
		(void) strcpy(return_code, "FAILURE");
		xdr_destroy(&xdrs);
		goto proxy_end; /*lint !e801 */
	}

	// Unpack the nvlist
	if (nvlist_unpack(arg_buf + xdr_getpos(&xdrs)
	    + EVENT_PROXY_PADDING_SPACE,
	    event_data.buf_size, &attr_list, 0) != 0) {
		err = errno;
		os::sc_syslog_msg logger(SC_SYSLOG_SC_EVENT_PROXY_TAG,
		    "", NULL);
		//
		// SCMSGS
		// @explanation
		// An internal error has occurred in the inter-process
		// communication between Sun Cluster processes. Related error
		// messages might be found near this one in the syslog output.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) logger.log(LOG_ERR, MESSAGE,
		    SYSTEXT("Error while unpacking data: %s"), strerror(err));
		(void) strcpy(return_code, "FAILURE");
		xdr_destroy(&xdrs);
		goto proxy_end; /*lint !e801 */
	}

	// log event here
	/*lint -e1776 */
	if (sysevent_post_event(EC_CLUSTER,
	    event_data.subclass,
	    SUNW_VENDOR, event_data.publisher,
	    attr_list, &eid) != 0) {
		err = errno;
		os::sc_syslog_msg logger(SC_SYSLOG_SC_EVENT_PROXY_TAG, "",
		    NULL);
		//
		// SCMSGS
		// @explanation
		// An internal error has occurred while posting a system event.
		// System events are used for keeping an audit trail of cluster
		// behavior. Related error messages might be found near this
		// one in the syslog output.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) logger.log(LOG_ERR, MESSAGE,
		    SYSTEXT("Error while posting sysevent from %s: %s"),
		    event_data.publisher, strerror(err));
		(void) strcpy(return_code, "FAILURE");
	}
	/*lint +e1776 */
	xdr_destroy(&xdrs);

proxy_end:
	if (attr_list) {
		nvlist_free(attr_list);
	}
	/* xdr_free((xdrproc_t)xdr_proxy_event_data, (char *)&event_data); */
	door_return(return_code, safe_strlen(return_code),
	    NULL, 0); /*lint !e534 */
	/*NOTREACHED*/
}
/*lint +e715 +e818 */

//
// This function created the event_proxy door server
//
int
sc_event_proxy_daemon_start()
{
	int err;

	// Create the door
	if ((sc_event_proxy_door_id = security_svc_reg(
		sc_event_proxy_daemon, RGM_SYSEVENT_PROXY_CODE,
		0, GLOBAL_ZONENAME)) == -1) {
		err = errno;
		os::sc_syslog_msg logger(SC_SYSLOG_SC_EVENT_PROXY_TAG,
		    "", NULL);
		//
		// SCMSGS
		// @explanation
		// The events proxy server could not be started due to some
		// internal error. Processes in non-global zones will be unable
		// to log systems events. System events is a mechanism to keep
		// an audit trail of the cluster actions. Although, this will
		// have no effect on the working of the cluster.
		// @user_action
		// Save a copy of the /var/adm/messages files on all nodes.
		// Contact your authorized Sun service provider for assistance
		// in diagnosing the problem.
		//
		(void) logger.log(LOG_ERR, MESSAGE,
		    SYSTEXT("Could not start the events proxy server: %s"),
		    strerror(err));
		return (-1);
	}
	return (0);
}
