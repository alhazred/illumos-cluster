//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)zone_events.cc	1.12	08/05/20 SMI"

//
// This file contains the code to listen for zone state change events
// from Solaris. It is part of the sc_zonesd daemon that runs in each
// global zone (but not non-global zones).
//

// system includes
#include <errno.h>
#include <pthread.h>
#include <unistd.h>
#include <stdio.h>
#include <dlfcn.h>
#include <zone.h>
#include <rgm/security.h>
#include "event_proxy_server.h"
#include <sc_event_proxy/sc_event_proxy.h>
#include <sc_event_proxy_impl.h>

extern int sc_event_proxy_door_id;
// redefine the macros in case we're compiling against an older Solaris version
#ifndef ZONE_EVENT_UNINITIALIZED
#define	ZONE_EVENT_UNINITIALIZED "uninitialized"
#endif

#ifndef ZONE_EVENT_READY
#define	ZONE_EVENT_READY "ready"
#endif

#ifndef ZONE_EVENT_RUNNING
#define	ZONE_EVENT_RUNNING "running"
#endif

#ifndef ZONE_EVENT_SHUTTING_DOWN
#define	ZONE_EVENT_SHUTTING_DOWN "shutting_down"
#endif

// SC includes
#include <sys/os.h>
#include <rgm/rgm_msg.h>
#include <sys/cl_assert.h>
#include <sys/sc_syslog_msg.h>
#include <sys/rsrc_tag.h>

// local includes
#include "zone_state_manager.h"
#include "zonesd_common.h"
#include "zone_shutdown_timer.h"

//
// This module handles the incoming Solaris events representing
// zone state changes.
//

//
// We convert the Solaris string states to internal states for use only in
// sc_zonesd. The state is converted again to an "sc external state" when
// sent to SC clients.
//
static zone_state_manager::zone_state
aggregate_event_state(const char *state)
{
	//
	// NOTE: "running" still counts as STARTING, because the
	// zone might not be at multi-user-server. Wait for the
	// canary to notify us.
	//
	if ((strcmp(state, ZONE_EVENT_READY) == 0) ||
	    (strcmp(state, ZONE_EVENT_RUNNING) == 0)) {
		return (zone_state_manager::STARTING);
	} else if (strcmp(state, ZONE_EVENT_UNINITIALIZED) == 0) {
		return (zone_state_manager::DOWN);
	} else if (strcmp(state, ZONE_EVENT_SHUTTING_DOWN) == 0) {
		return (zone_state_manager::SHUTTING_DOWN);
	}

	os::sc_syslog_msg logger(SC_SYSLOG_RGM_ZONESD_TAG, "", NULL);
	/*
	 * SCMSGS
	 * @explanation
	 * The sc_zonesd received an invalid zone state notification from
	 * Solaris.
	 * @user_action
	 * This message can be ignored and the user need not take any action
	 */
	logger.log(LOG_INFO, MESSAGE,
	    SYSTEXT("Invalid zone state: %s"), state);

	return (zone_state_manager::UNKNOWN);
}


// Need to grab lock here for the whole function in order to serialize the
// event callbacks
static int
zone_event_receive(const char *zonename, zoneid_t, const char *ns,
    const char *os, hrtime_t, void *)
{
	int err;

	CL_PANIC(pthread_mutex_lock(&lock) == 0);

	debug_msg("Received event: %s switched from %s to %s\n", zonename,
	    os, ns);

	zone_state_manager::zone_state agg_state;
	agg_state = aggregate_event_state(ns);

	// set_zone_state takes care of queueing the notifications
	// to clients. Returns true if it actually sets the state.
	bool set_state =
	    zone_state_manager::instance().set_zone_state(zonename, agg_state);

	CL_PANIC(pthread_mutex_unlock(&lock) == 0);

	//
	// If the zone starts shutting down, we set a timer.
	// If it takes too long, we declare it "stuck."
	//
	// We have to trigger or cancel the timeout thread here.
	// The set_zone_state() method can't do it because we need to
	// release the lock first, to avoid deadlock conditions.
	//
	if (set_state && agg_state == zone_state_manager::SHUTTING_DOWN) {
		debug_msg("triggering timeout for zone %s\n",
		    zonename);
		zone_shutdown_timer::instance().wait(zonename);
	} else if (set_state && agg_state == zone_state_manager::DOWN) {
		debug_msg("cancelling timeout for zone %s\n",
		    zonename);
		zone_shutdown_timer::instance().cancel(zonename);
	}
	return (0);
}

void
zone_event_register()
{
	void *(*real_znb)(int(*func)(const char *zonename, zoneid_t zid,
	    const char *newstate, const char *oldstate,
	    hrtime_t, void *p), void *p);

	real_znb =
	    (void *(*)(int(*func)(const char *zonename, zoneid_t zid,
	    const char *newstate, const char *oldstate, hrtime_t, void *p),
	    void *p))dlsym(RTLD_DEFAULT, "zonecfg_notify_bind");

	if (real_znb == NULL) {
		os::sc_syslog_msg logger(SC_SYSLOG_RGM_ZONESD_TAG, "", NULL);
		/*
		 * SCMSGS
		 * @explanation
		 * The sc_zonesd is unable to retrieve zone state change event
		 * registration symbol: zones functionality will not work
		 * correctly on this node.
		 * @user_action
		 * Save a copy of the /var/adm/messages files on all nodes.
		 * Contact your authorized Sun service provider for assistance
		 * in diagnosing the problem.
		 */
		logger.log(LOG_ERR, MESSAGE,
		    SYSTEXT("Error: unable to retrieve zone state change "
			"event registration symbol: zones functionality will "
			"not work correctly on this node"));
		//
		// Don't abort the node, so that we can continue to test
		// without the solaris functionality
		//
		return;
	}

	if (real_znb(zone_event_receive, NULL) == NULL) {
		os::sc_syslog_msg logger(SC_SYSLOG_RGM_ZONESD_TAG, "", NULL);
		/*
		 * SCMSGS
		 * @explanation
		 * The sc_zonesd is unable to register for zone state change
		 * events: zones functionality will not work correctly on this
		 * node.
		 * @user_action
		 * Save a copy of the /var/adm/messages files on all nodes.
		 * Contact your authorized Sun service provider for assistance
		 * in diagnosing the problem.
		 */
		logger.log(LOG_ERR, MESSAGE,
		    SYSTEXT("Error: unable to register for zone state change "
			"events: zones functionality will "
			"not work correctly on this node"));
		//
		// This means that the functionality should be there, but
		// isn't working correctly. Just return and allow everything
		// else to run.
		//
		return;
	}
}
