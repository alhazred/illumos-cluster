//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)zone_shutdown_timer.cc	1.4	08/05/20 SMI"

//
// This class is part of the sc_zonesd daemon that runs in each global
// zone (but not non-global zones).
//

#include "zone_shutdown_timer.h"

#include <sys/cl_assert.h>
#include <sys/sc_syslog_msg.h>
#include <sys/rsrc_tag.h>
#include <sys/os.h>
#include <rgm/rgm_msg.h>

#include <pthread.h>

#include "zone_state_manager.h"
#include "zonesd_common.h"

#define	SHUTDOWN_TIMEOUT 45

zone_shutdown_timer* zone_shutdown_timer::the_shutdown_timer = NULL;
pthread_mutex_t zone_shutdown_timer::create_lock = PTHREAD_MUTEX_INITIALIZER;

zone_shutdown_timer&
zone_shutdown_timer::instance()
{
	if (the_shutdown_timer == NULL) {
		create();
	}

	return (*the_shutdown_timer);
}

void
zone_shutdown_timer::create()
{
	// allow only one thread at a time to attempt to create
	// the shutdown timer in order to avoid creating multiple
	// shutdown timers in a race condition.
	CL_PANIC(pthread_mutex_lock(&create_lock) == 0);
	if (the_shutdown_timer == NULL) {
		the_shutdown_timer = new zone_shutdown_timer();
	}
	CL_PANIC(pthread_mutex_unlock(&create_lock) == 0);
}


//
// Wrapper function for thread creation.
//
// Defined with "C" linkage because it's called by a C function
// (pthread_create).
//
extern "C" void *shutdown_timer_thread_wrapper(void *)
{
	zone_shutdown_timer::instance().timeout_waits();
	return (NULL);
}

zone_shutdown_timer::zone_shutdown_timer()
{
	(void) pthread_mutex_init(&lock, NULL);
	(void) pthread_cond_init(&cond, NULL);

	//
	// Create the timeout thread
	//
	int ret;
	if ((ret = pthread_create(&wait_thread, NULL,
	    shutdown_timer_thread_wrapper, NULL)) != 0) {
		char *err_str = strerror(ret);
		os::sc_syslog_msg logger(SC_SYSLOG_RGM_ZONESD_TAG, "", NULL);
		/*
		 * SCMSGS
		 * @explanation
		 * The sc_zonesd daemon was not able to allocate a new thread.
		 * This problem can occur if the machine has low memory.
		 * @user_action
		 * Determine if the machine is running out of memory. If this
		 * is not the case, save the /var/adm/messages file. Contact
		 * your authorized Sun service provider to determine whether a
		 * workaround or patch is available.
		 */
		logger.log(LOG_ERR, MESSAGE,
		    "pthread_create: %s", err_str);
		_exit(ret);
	}
}

//
// timeout_waits()
//
// This method performs the timeout logic. It keeps a FIFO list
// of timeout_entry objects, with the next one to timeout first in the list.
// The thread sleeps on a condition variable until a new timeout entry is
// added to the queue, or until it's time to finish the timeout of the first
// entry on the queue. A timeout is completed by telling the zone state
// manager to mark the zone in question as STUCK.
//
void
zone_shutdown_timer::timeout_waits()
{
	CL_PANIC(pthread_mutex_lock(&lock) == 0);

	while (true) {
		timestruc_t timeout;
		int err;

		// If the queue is empty, do a normal cond_wait.
		if (to_queue.empty()) {
			err = pthread_cond_wait(&cond, &lock);
		} else {
			// The thread is not empty, so wait until it's time
			// to finish the timeout for the top entry.
			timeout.tv_sec = to_queue.front().timeout_time;
			timeout.tv_nsec = 0;
			err = pthread_cond_timedwait(&cond, &lock, &timeout);
		}

		debug_msg(NOGET("zone_shutdown_timer: waking up from cond_wait"
		    ". err=%d\n"), err);

		if (err != 0 && err != ETIMEDOUT) {
			os::sc_syslog_msg logger(SC_SYSLOG_PMF_PMFD_TAG, "",
			    NULL);
			/*
			 * SCMSGS
			 * @explanation
			 * The sc_zonesd daemon received an unexpected error
			 * from pthread_cond_wait. This error should not
			 * impact the sc_zonesd's operations.
			 * @user_action
			 * If the problem persists, look for other syslog
			 * error messages on the same node. Save a copy of the
			 * /var/adm/messages files on all nodes, and report
			 * the problem to your authorized Sun service
			 * provider.
			 */
			logger.log(LOG_ERR, MESSAGE, "pthread_cond_wait: %s",
			    strerror(err));
		}

		//
		// Here we're holding the lock again (after the return from
		// pthread_cond_wait or pthread_cond_timedwait)
		//

		//
		// Mark as "stuck" all processes that are done waiting
		//
		time_t cur_time = time(NULL);
		CL_PANIC(cur_time != -1);

		while (!to_queue.empty() && to_queue.front().timeout_time <=
		    cur_time) {
			//
			// tell the zone state manager to mark this zone
			// as STUCK. In case of a race condition, the
			// zone state manager will ignore this transition
			// if the current state is not SHUTTING_DOWN
			//
			debug_msg("Found stuck zone %s\n", to_queue.front().
			    zonename.c_str());
			zone_state_manager::instance().set_zone_state(
			    to_queue.front().zonename,
			    zone_state_manager::STUCK);

			// get rid of this one from the queue
			to_queue.pop_front();
		}
	}
}

// should NOT be holding the main minerd lock
void
zone_shutdown_timer::wait(const char *zonename)
{
	CL_PANIC(pthread_mutex_lock(&lock) == 0);

	//
	// Add this timeout entry to the end of the list. We know
	// it should be the last entry because everything waits for the
	// same amount of time.
	//
	// we don't have to worry about catching a bad_alloc because
	// our new handler just aborts
	//
	time_t cur_time = time(NULL);
	if (cur_time == -1) {
		int err = errno;
		os::sc_syslog_msg logger(SC_SYSLOG_PMF_PMFD_TAG, "", NULL);
		/*
		 * SCMSGS
		 * @explanation
		 * The time(2) function failed with the specified error. The
		 * program will exit and the node might halt or reboot.
		 * @user_action
		 * Search for other syslog error messages on the same node.
		 * Save a copy of the /var/adm/messages files on all nodes,
		 * and report the problem to your authorized Sun service
		 * provider.
		 */
		logger.log(LOG_ERR, MESSAGE, "time in throttle wait: "
		    "%s", strerror(err));
		_exit(1);
	}

	to_queue.push_back(timeout_entry(zonename, time(NULL) +
	    SHUTDOWN_TIMEOUT));

	// signal the timeout thread in case this is the only entry
	// on the queue
	(void) pthread_cond_broadcast(&cond);

	CL_PANIC(pthread_mutex_unlock(&lock) == 0);
}

void
zone_shutdown_timer::cancel(const char *zonename)
{
	CL_PANIC(pthread_mutex_lock(&lock) == 0);

	// construct a dummy timeout_entry struct to
	// which the queue can compare entries.
	to_queue.remove(timeout_entry(zonename, 0));

	CL_PANIC(pthread_mutex_unlock(&lock) == 0);
}

// CSTYLED
bool operator==(const timeout_entry& lhs, const timeout_entry& rhs)
{
	return (lhs.zonename == rhs.zonename);
}


timeout_entry::timeout_entry(const char *zonename_in, time_t time)
	: zonename(zonename_in), timeout_time(time)
{
}
