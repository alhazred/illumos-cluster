//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef	_ZONE_STATE_MANAGER_H
#define	_ZONE_STATE_MANAGER_H

#pragma ident	"@(#)zone_state_manager.h	1.3	08/05/20 SMI"

//
// This class is part of the sc_zonesd daemon that runs in each global
// zone (but not non-global zones).
//

#include <map>
#include <string>
using std::string;

#include <pthread.h>

// does NOT provide internal locking. Depend on global lock.

class zone_state_manager
{
public:
	typedef enum {UNKNOWN, DOWN, STUCK, SHUTTING_DOWN, STARTING, UP}
	zone_state;

	bool set_zone_state(const string& zone_name, zone_state state);
	bool set_zone_state(const char *zone_name, zone_state state);

	void send_all_state(int fd);

	static zone_state_manager &instance();
	static void create();

protected:
	zone_state_manager();

	// Don't provide implementations for these
	zone_state_manager(const zone_state_manager& src);
	zone_state_manager& operator=(const zone_state_manager& rhs);

	std::map<string, zone_state> zone_states;

	static zone_state_manager *the_manager;
	static pthread_mutex_t create_lock;
};

#endif /* _ZONE_STATE_MANAGER_H */
