/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scrcmd_main.c	1.11	08/07/24 SMI"

/*
 * scrcmd
 *
 *      The scrcmd command is the client interface to rpc.scrcmd.
 *	The purpose of scrcmd and rpc.scrcmd is to execute selected
 *	commands on a remote Sun Cluster machine.
 *
 *	The scrcmd interface runs only those commands found
 *	in the "scrcmd_cmds" list.   This list also includes a flag
 *	to indicate whether or not the command can be run if the
 *	node is already configured as a cluster node.
 *
 *	The rpc.scinstall daemon will not attempt to run commands
 *	if the machine is not configured to allow remote configuration.
 *
 *	The scrcmd/rpc.scrcmd protocol uses RPC to launch remote commands.
 *	But, it uses sockets to stream stdout/stderr from the daemon back
 *	to scrcmd.
 *
 *	scrcmd [-N <remotenode>] [-f <file>] [-S] <command>
 *	[<command_arguments>]
 *
 *	Possible commands:
 *
 *		command:		test
 *		  command arguments:	isfullyinstalled |
 *					isinstalling |
 *					isconfigured |
 *					hasbooted |
 *					isclustermember
 *		  command_exit_codes:	0  false
 *					1  true
 *					2  error
 *
 *		command:		reboot
 *		  command arguments:	<none>
 *		  command_exit_codes:	2  error
 *					<command does not normally return>
 *
 *		command:		install
 *		  command_arguments:	same as for "scinstall -i" PLUS
 *					-logfile <logfile>
 *		  command_exit_codes:	same as for "scinstall -i"
 *
 *		command:		sccheck
 *		  command_arguments:	<none>
 *		  command_exit_codes:	same as for sccheck
 *
 *		command:		autodiscovery
 *		  command_arguments:	<token> <adaptercount> <timeout>
 *		  command_exit_codes:	same as for "scrconf -n"
 *
 *		command:		chk_globaldev
 *		  command_arguments:	fs|device <mount_point>|<special_device>
 *
 *		  command_exit_codes when <mount_point> is <command_argument>:
 *
 *					0  Okay to use
 *					1  Does not begin with /
 * 					2  Is not a directory
 * 					3  Is not a mount point in /etc/vfstab
 * 					4  Error reading /etc/vfstab
 * 					5  Is not a mount point in /etc/mnttab
 * 					6  Error reading /etc/mnttab
 * 					7  Is not empty
 * 					8  Does not include lost+found
 * 					9  Is under /global
 * 					10 Other error
 *
 *		  command_exit_codes when <mount_point> is <command_argument>:
 *
 * 					0  Okay to use
 * 					1  Does not begin with /
 * 					2  Is not a character special device
 * 					3  Is already in use by /etc/vfstab
 * 					4  Error reading /etc/vfstab
 * 					10 Other error
 *
 *		command:		snmp
 *		  command_arguments:	see sceventmib script
 *		  command_exit_codes:	see sceventmib script
 *
 *		command:		scdidadm
 *		  command_arguments:	see scdidadm usage
 *		  command_exit_codes:	see scdidadm.c
 *
 *		command:		administerzone
 *		  command_arguments:	see clzonecluster usage
 *		  command_exit_codes:	see clzonecluster_common.c
 *
 *		command:		clzonehelper
 *		  command_arguments:	see clzone_helper usage
 *		  command_exit_codes:	see clzonecluster_common.c
 *
 *	Other possible exit codes from scrcmd:
 *
 *		99		- command exited with code > 99
 *		100		- usage error
 *		101		- not enough memory
 *		102		- already configured as a cluster node
 *		103		- cannot create socket or get port for socket
 *		104		- does not allow remote configuration
 *		105		- connection timed out
 *		106		- connection refused
 *		107		- command did not execute
 *		108		- unable to write command status to fd 3
 *		109		- connection aborted before command completed
 */

/*lint -e788 */
/*lint -e702 */

#include "rpc_scrcmd.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <strings.h>
#include <string.h>
#include <errno.h>
#include <netdb.h>
#include <libintl.h>
#include <locale.h>

#include <sys/types.h>
#include <sys/file.h>
#include <sys/wait.h>
#include <sys/socket.h>
#include <sys/uio.h>
#include <sys/time.h>

#include <rpc/rpc.h>
#include <rpc/rpcent.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <rgm/sczones.h>

#include <didadm.h>
#include <scha.h>

#define	SCINSTALL_CLNT_CREATE_TO (1 * 60)	/* Clnt create timeout */
#define	SCINSTALL_CLNT_CALL_TO   (5 * 60)	/* Clnt call timeout */

#define	SCINSTALL_ACCEPT_TO	 (1 * 60)	/* Accept timeout */

/*
 * This "scrcmd_cmds" list must be kept in sync with the server list.
 */
static scrcmd_cmd_t scrcmd_cmds[] = { SCRCMD_CMDS };

static char *progname;
static rpcprog_t scinstallprog = 0;

static void usage(void);
static int do_cmd(char *servername, char *filename, char *name,
    char *command_args, int *command_statusp);
static int do_cmd_local(char *filename, char *name, char *command_args,
    int *command_statusp);
static int do_cmd_remote(char *servername, char *filename,
    char *name, char *command_args, int *command_statusp);
static int check_cmd_flags(uint_t flags);
static int create_socket(int family, in_port_t port);
static int get_port(int family, int s);
static int noblocking(int fd);
static int accept_connections(int s1v6, int s2v6, int *fd1p, int *fd2p);
static int accept_connection(int s, int *fdp);
static int rw_cmd_output(int fd_out, int fd_err, int *command_statusp);
static sc_envlist_t *create_envlist(void);
static void free_envlist(sc_envlist_t *envlist);
static int scrcmd_clnt_create(char *servername, CLIENT **clntp);
static void print_remote_errlist(char *servername, sc_namelist_t *errlist);
static void free_errlist_messages(sc_namelist_t *errlist);
static int fill_buf_with_file(char **buffer, char *filename);
static char *scrcmd_strerr(int err);
static int scrcmd_err_to_exitcode(int err);

/*
 * main
 */
int
main(int argc, char **argv)
{
	int c;
	int i, j;
	uint_t len;
	scrcmd_cmd_t *cmd;
	char *servername = NULL;
	char *filename = NULL;
	char *name = NULL;
	char *command_args = NULL;
	int command_status = 0;
	int res = 0;
	int exitcode = 0;
	bool_t supress_warn_msg = B_FALSE;

	/* Set the program name */
	if ((progname = strrchr(argv[0], '/')) == NULL) {
		progname = argv[0];
	} else {
		++progname;
	}

	/* I18N housekeeping */
	(void) setlocale(LC_ALL, "");
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);
	(void) textdomain(TEXT_DOMAIN);
	if (sc_zonescheck() != 0)
		return (1);

	while ((c = getopt(argc, argv, "SN:f:")) != EOF) {
		switch (c) {
		case 'f':
			filename = optarg;
			break;

		case 'N':
			servername = optarg;
			break;

		case 'S':
			/*
			 * If this option is provided, set the
			 * warning message flag. This Flag is
			 * used to supress the warning message
			 * provided by scrcmd when server name
			 * is not provided.
			 */
			supress_warn_msg = B_TRUE;
			break;

		case '?':
			usage();
			exitcode = scrcmd_err_to_exitcode(EINVAL);
			goto cleanup;

		default:
			break;
		}
	}

	/* Make sure that there is a command name */
	i = optind;
	if (i == argc) {
		usage();
		exitcode = scrcmd_err_to_exitcode(EINVAL);
		goto cleanup;
	} else {
		name = argv[i++];
		if (name == NULL || *name == '\0') {
			usage();
			exitcode = scrcmd_err_to_exitcode(EINVAL);
			goto cleanup;
		}
	}

	/* Get the command arguments */
	j = i;
	len = 0;
	while (j < argc) {
		len += strlen(argv[j++]);
		++len;
	}
	len += 2;
	command_args = (char *)calloc(len, sizeof (char));
	if (command_args == NULL) {
		res = ENOMEM;
		(void) fprintf(stderr, "%s:  %s.\n", progname,
		    scrcmd_strerr(res));
		exitcode = scrcmd_err_to_exitcode(res);
		goto cleanup;
	}

	while (i < argc) {
		if (*command_args == '\0') {
			(void) strcpy(command_args, argv[i++]);
		} else {
			(void) strcat(command_args, argv[i++]);
		}
		(void) strcat(command_args, " ");
	}

	/* Do it */
	for (cmd = scrcmd_cmds;  cmd->cmd_name != NULL;  ++cmd) {
		if (strcmp(cmd->cmd_name, name) == 0) {
			res = do_cmd(servername, filename, name,
			    command_args, &command_status);
			if (res > 0) {
				exitcode = scrcmd_err_to_exitcode(res);
			} else if (command_status > 99) {
				exitcode = 99;
			} else {
				exitcode = command_status;
			}
			goto cleanup;
		}
	}

	usage();
	exitcode = scrcmd_err_to_exitcode(EINVAL);

cleanup:
	/*
	 * If there is no servername, print the exit code.  This
	 * is useful when called from rsh/ssh, since rsh/ssh do not
	 * return the exit code of the command.
	 */
	if ((servername == NULL) && (supress_warn_msg == B_FALSE))
		(void) printf("%s=%d\n", SCRCMD_CMD_STATUS_STRING, exitcode);

	return (exitcode);
}

/*
 * usage
 *
 * Print usage message to stderr.
 */
static void
usage(void)
{
	(void) fprintf(stderr, "%s:  %s %s\n", gettext("usage"), progname,
	    "[-N <remotenode>] [-f <file>] [-S] <command>"
	    "[<command_arguments>]");
}

/*
 * do_cmd
 *
 * Run the selected command "name" from the "scrcmd_cmds" list
 * with the given "command_args".
 *
 * Standard output from the command is printed on stdout;   error
 * output from the command is printed on stderr.
 *
 * If the "servername" is NULL, the command is run locally;  otherwise,
 * an attempt is made to run the command on the given "servername".
 *
 * If the command runs to completion, the exit status of the command
 * itself is left in "command_statusp".
 *
 * Possible return values:
 *
 *	0		- command ran to completion
 *	ENOMEM		- not enough memory
 *	EEXIST		- already configured as a cluster node
 *	EPROTONOSUPPORT	- cannot create socket or get port for socket
 *	ENOTACTIVE	- does not allow remote configuration
 *	ETIMEDOUT	- connection timed out
 *	ECONNREFUSED	- connection refused
 *	ENOEXEC		- command did not execute
 *	EIO		- IO error
 *	ECONNABORTED	- connection aborted before command completed
 */
static int
do_cmd(char *servername, char *filename, char *name, char *command_args,
    int *command_statusp)
{
	int res = 0;

	if (servername == NULL) {
		res = do_cmd_local(filename, name,
		    command_args, command_statusp);
	} else {
		res = do_cmd_remote(servername, filename, name, command_args,
		    command_statusp);
	}

	return (res);
}

/*
 * do_cmd_local
 *
 * Run the selected command "name" from the "scrcmd_cmds" list
 * with the given "command_args".
 *
 * Standard output from the command is printed on stdout;   error
 * output from the command is printed on stderr.   If the command
 * runs to completion, the exit status of the command itself is left in
 * "command_statusp".
 *
 * Possible return values:
 *
 *	0		- command ran to completion
 *	ENOMEM		- not enough memory
 *	ENOEXEC		- command did not execute
 *	EIO		- IO error
 */
static int
do_cmd_local(char *filename, char *name,
    char *command_args, int *command_statusp)
{
	int res = 0;
	int status;
	scrcmd_cmd_t *cmd;
	char *command_path = NULL;
	char *command_string;
	size_t filelen = 0;

	/* Get the command to execute */
	for (cmd = scrcmd_cmds;  cmd->cmd_name != NULL;  ++cmd) {
		if (strcmp(cmd->cmd_name, name) == 0) {
			command_path = cmd->cmd_path;
			break;
		}
	}
	if (command_path == NULL) {
		res = ENOEXEC;
		(void) fprintf(stderr, "%s:  %s.\n", progname,
		    scrcmd_strerr(res));
		return (res);
	}

	/* Check the command flags */
	res = check_cmd_flags(cmd->cmd_flags);
	if (res != 0)
		return (res);

	/* Make sure we can run the command */
	if (access(command_path, (R_OK | X_OK)) != 0) {
		res = ENOEXEC;
		(void) fprintf(stderr, "%s:  %s.\n", progname,
		    scrcmd_strerr(res));
		return (res);
	}

	/* Concat command args to the command */
	if (command_args != NULL && *command_args != '\0') {
		if (filename != NULL)
			filelen = strlen(filename);
		else
			filelen = 0;
		command_string = (char *)calloc(1, strlen(command_path) +
		    strlen(command_args) + filelen + 4);
		if (command_string != NULL) {
			(void) strcpy(command_string, command_path);
			(void) strcat(command_string, " ");
			(void) strcat(command_string, command_args);
			if (filename != NULL)
				(void) strcat(command_string, filename);
		}
	} else {
		command_string = strdup(command_path);
	}
	if (command_string == NULL) {
		res = ENOMEM;
		(void) fprintf(stderr, "%s:  %s.\n", progname,
		    scrcmd_strerr(res));
		return (res);
	}

	/* Run the command with its args */
	status = system(command_string);
	free(command_string);
	if (status == -1) {
		res = ENOEXEC;
		(void) fprintf(stderr, "%s:  %s.\n", progname,
		    scrcmd_strerr(res));
		return (res);
	}
	if (WIFEXITED(status)) {
		*command_statusp = WEXITSTATUS(status);

	} else if (WIFSIGNALED(status)) {
		res = ENOEXEC;
		(void) fprintf(stderr,
		    gettext("%s:  Command exited due to signal (%d).\n"),
		    progname, WTERMSIG(status));
		(void) fprintf(stderr, "%s:  %s.\n", progname,
		    scrcmd_strerr(res));
	}

	return (res);
}

/*
 * do_cmd_remote
 *
 * Run the selected command "name" from the "scrcmd_cmds" list
 * with the given "command_args" on the given "servername".
 *
 * Standard output from the command is printed on stdout;   error
 * output from the command is printed on stderr.   If the command
 * runs to completion, the exit status of the command itself is left in
 * "command_statusp".
 *
 * Possible return values:
 *
 *	0		- command ran to completion
 *	ENOMEM		- not enough memory
 *	EEXIST		- already configured as a cluster node
 *	EPROTONOSUPPORT	- cannot create socket or get port for socket
 *	ENOTACTIVE	- does not allow remote configuration
 *	ETIMEDOUT	- connection timed out
 *	ECONNREFUSED	- connection refused
 *	ENOEXEC		- command did not execute
 *	EIO		- IO error
 *	ECONNABORTED	- connection aborted before command completed
 */
static int
do_cmd_remote(char *servername, char *filename, char *name, char *command_args,
    int *command_statusp)
{
	int res = 0;
	scha_err_t scha_res;
	scha_cluster_t scha_handle = (scha_cluster_t)0;
	scha_node_state_t scha_node_state;
	char *private_hostname = (char *)0;
	scrcmd_cmd_t *cmd;
	char *command_path = NULL;
	int s_out = -1;
	int s_err = -1;
	int fd_out = -1;
	int fd_err = -1;
	int outport;
	int errport;
	char *buffer = NULL;
	sc_envlist_t *envlist = NULL;
	CLIENT *clnt = NULL;
	enum clnt_stat clnt_st;
	sc_result clnt_res;

	/* Get the command to execute */
	for (cmd = scrcmd_cmds;  cmd->cmd_name != NULL;  ++cmd) {
		if (strcmp(cmd->cmd_name, name) == 0) {
			command_path = cmd->cmd_path;
			break;
		}
	}
	if (command_path == NULL) {
		res = ENOEXEC;
		(void) fprintf(stderr, "%s:  %s.\n", progname,
		    scrcmd_strerr(res));
		return (res);
	}

	/* Initialize clnt_res */
	bzero(&clnt_res, sizeof (clnt_res));

	/* Get the IPv6 stdout socket and port */
	if ((s_out = create_socket(AF_INET6, 0)) < 0 ||
	    (outport = get_port(AF_INET6, s_out)) < 0) {
		res = EPROTONOSUPPORT;
		goto cleanup;
	}

	/* Get the IPv6 stderr socket and port */
	if ((s_err = create_socket(AF_INET6, 0)) < 0 ||
	    (errport = get_port(AF_INET6, s_err)) < 0) {
		res = EPROTONOSUPPORT;
		goto cleanup;
	}

	/* Listen for connection attempts */
	if (listen(s_out, 1) < 0 ||
	    listen(s_err, 1) < 0) {
		/*lint -e(746) */
		(void) fprintf(stderr, gettext("%s:  listen attempt failed "
		    "- %s.\n"), progname, strerror(errno));
		res = EPROTONOSUPPORT;
		goto cleanup;
	}

	/* Get the environment that we want to pass */
	if ((envlist = create_envlist()) == NULL) {
		res = ENOMEM;
		goto cleanup;
	}

	/*
	 * If SCRCMD_FLG_INTERCONNECT is set, then we must communicate
	 * over the private interconnect.  This means that we must
	 * be in the cluster.   And, if "servername" is a node
	 * name, then let's switch it to be the private hostname.
	 */
	if (cmd->cmd_flags & SCRCMD_FLG_INTERCONNECT) {

		/* Use SCHA calls */
		scha_res = scha_cluster_open(&scha_handle);
		if (scha_res != SCHA_ERR_NOERR) {

			/*
			 * Unfortunately, scha_cluster_open() throws
			 * an error of SCHA_ERR_INTERNAL when the node
			 * is not in the cluster.   A more appropriate
			 * error code would be nice.
			 *
			 * For our purposes, let's assume that
			 * SCHA_ERR_INTERNAL is an indicator that this
			 * node is not in the cluster.
			 */
			if (scha_res == SCHA_ERR_INTERNAL) {
				(void) fprintf(stderr, gettext("%s:  This "
				    "node is not an active cluster member.\n"),
				    progname);
			} else {
				(void) fprintf(stderr, gettext("%s:  Call to "
				    "%s failed (%d).\n"),
				    progname, "scha_cluster_open", scha_res);
				(void) fprintf(stderr, gettext("%s:  Unable "
				    "to determine if this node is in the "
				    "cluster.\n"), progname);
			}
			(void) fprintf(stderr, gettext("%s:  \"%s\" "
			    "can only be run over the private interconnect.\n"),
			    progname, name);
			res = ENOEXEC;
			goto cleanup;
		}

		/* Make sure we are in the cluster */
		scha_res = scha_cluster_get(scha_handle, SCHA_NODESTATE_LOCAL,
		    &scha_node_state);
		if (scha_res != SCHA_ERR_NOERR) {
			(void) fprintf(stderr, gettext("%s:  Call to "
			    "%s failed (%d).\n"),
			    progname, "scha_cluster_get", scha_res);
			(void) fprintf(stderr, gettext("%s:  Unable "
			    "to determine if this node is in the cluster.\n"),
			    progname);
			(void) fprintf(stderr, gettext("%s:  \"%s\" "
			    "can only be run over the private interconnect.\n"),
			    progname, name);
			res = ENOEXEC;
			goto cleanup;
		}
		if (scha_node_state != SCHA_NODE_UP) {
			(void) fprintf(stderr, gettext("%s:  This "
			    "node is not an active cluster member.\n"),
			    progname);
			(void) fprintf(stderr, gettext("%s:  \"%s\" "
			    "can only be run over the private interconnect.\n"),
			    progname, name);
			res = ENOEXEC;
			goto cleanup;
		}

		/* Make sure the other node is in the cluster */
		scha_res = scha_cluster_get(scha_handle, SCHA_NODESTATE_NODE,
		    servername, &scha_node_state);
		if (scha_res != SCHA_ERR_NOERR) {
			if (scha_res == SCHA_ERR_NODE) {
				(void) fprintf(stderr, gettext("%s:  \"%s\" "
				    "is not a cluster node name.\n"),
				    progname, servername);
			} else {
				(void) fprintf(stderr, gettext("%s:  Call to "
				    "%s failed (%d).\n"),
				    progname, "scha_cluster_get", scha_res);
				(void) fprintf(stderr, gettext("%s:  Unable "
				    "to determine if \"%s\" is in the "
				    "cluster.\n"), progname, servername);
				(void) fprintf(stderr, gettext("%s:  \"%s\" "
				    "can only be run over the private "
				    "interconnect.\n"), progname, name);
			}
			res = ENOEXEC;
			goto cleanup;
		}
		if (scha_node_state != SCHA_NODE_UP) {
			(void) fprintf(stderr, gettext("%s:  \"%s\" "
			    "is not an active cluster member.\n"),
			    progname, servername);
			(void) fprintf(stderr, gettext("%s:  \"%s\" "
			    "can only be run over the private interconnect.\n"),
			    progname, name);
			res = ENOEXEC;
			goto cleanup;
		}

		/*
		 * See if there is a private hostname.
		 *
		 * If so, reset the "servername" to the "private_hostname".
		 *
		 * Otherwise, if there is no private hostname,
		 * we assume that a private hostname has already
		 * been given on the command line, and we just continue on.
		 * If a private host name is not actually supplied,
		 * we will eventually fail on the server side.
		 */
		scha_res = scha_cluster_get(scha_handle,
		    SCHA_PRIVATELINK_HOSTNAME_NODE, servername,
		    &private_hostname);
		if ((scha_res == SCHA_ERR_NOERR) &&
		    (private_hostname != (char *)0))
			servername = private_hostname;
	}

	/* Get the CLIENT handle */
	res = scrcmd_clnt_create(servername, &clnt);
	if (res != 0) {
		goto cleanup;
	}

	/* Copy the file into the buffer */
	if (filename != NULL) {
		res = fill_buf_with_file(&buffer, filename);
		if (res != 0) {
			goto cleanup;
		}
	} else {
		buffer = strdup("NOOP");
	}

	/* Issue the command */
	clnt_st = scrcmdproc_do_cmd_1((uint_t)outport, (uint_t)errport,
	    name, command_args, *envlist, buffer, &clnt_res, clnt);

	/* Check for RPC failure */
	if (clnt_st != RPC_SUCCESS) {
		clnt_perror(clnt, progname);
		switch (clnt_st) {
		case RPC_TIMEDOUT:
			res = ETIMEDOUT;
			break;

		default:
			res = ECONNREFUSED;
			break;
		}

		goto cleanup;
	}

	/* Check for RPC procedure failure */
	res = (int)clnt_res.sc_res_errno;
	if (res != 0) {
		print_remote_errlist(servername, &clnt_res.sc_res_errlist);
		goto cleanup;
	}
	free_errlist_messages(&clnt_res.sc_res_errlist);

	/* Make sockets non-blocking */
	if (noblocking(s_out) < 0 ||
	    noblocking(s_err) < 0) {
		(void) fprintf(stderr, gettext("%s:  Cannot unblock socket "
		    "- %s.\n"), progname, strerror(errno));
		res = EPROTONOSUPPORT;
		goto cleanup;
	}

	/* Accept one fd_out and one fd_err connection */
	res = accept_connections(s_out, s_err, &fd_out, &fd_err);
	if (res != 0)
		goto cleanup;

	/* Make file descriptors non-blocking */
	if (noblocking(fd_out) < 0 ||
	    noblocking(fd_err) < 0) {
		(void) fprintf(stderr, gettext("%s:  Cannot unblock connection "
		    "- %s.\n"), progname, strerror(errno));
		res = EIO;
		goto cleanup;
	}

	/* Read remote stdout/stderr and print to our stdout/stderr */
	res = rw_cmd_output(fd_out, fd_err, command_statusp);
	if (res != 0)
		goto cleanup;

cleanup:
	/* Close the SCHA handle */
	if (scha_handle != (scha_cluster_t)0)
		(void) scha_cluster_close(scha_handle);

	/* Close sockets */
	if (s_out >= 0)
		(void) close(s_out);
	if (s_err >= 0)
		(void) close(s_err);
	if (fd_out >= 0)
		(void) close(fd_out);
	if (fd_err >= 0)
		(void) close(fd_err);

	/* Free the envlist */
	if (envlist != NULL)
		free_envlist(envlist);

	/* Destroy the RPC client handle */
	if (clnt != NULL)
		clnt_destroy(clnt);

	/* Free the buffer */
	if (buffer != NULL)
		free(buffer);

	return (res);
}

/*
 * check_cmd_flags
 *
 * Check the command flags to verify that it is okay to proceed with
 * local execution.
 *
 * Possible return values:
 *
 *	0		- okay to proceed
 *	ENOEXEC		- both SCRCMD_FLG_NODEID and SCRCMD_FLG_NONODEID set
 *	ENOEXEC		- SCRCMD_FLG_NODEID test failed
 *	EEXIST		- already configured as an SC node (SCRCMD_FLG_NONODEID)
 */
static int
check_cmd_flags(uint_t flags)
{
	int res;
	uint_t mask;

	/* SCRCMD_FLG_NODEID and SCRCMD_FLG_NONODEID are mutually exclusive */
	mask = (SCRCMD_FLG_NODEID | SCRCMD_FLG_NONODEID);
	if ((flags & mask) == mask) {
		res = ENOEXEC;
		(void) fprintf(stderr, gettext("%s:  Internal error - "
		    "bad commands table.\n"), progname);
		(void) fprintf(stderr, "%s:  %s.\n", progname,
		    scrcmd_strerr(res));
		return (res);
	}

	/* Node must be configured */
	if (flags & SCRCMD_FLG_NODEID) {
		if (access(SCRCMD_NODEID_FILE, F_OK) != 0) {
			res = ENOEXEC;
			(void) fprintf(stderr, gettext(
			    "%s:  Cannot access %s.\n"), progname,
			    SCRCMD_NODEID_FILE);
			(void) fprintf(stderr, "%s:  %s.\n", progname,
			    scrcmd_strerr(res));
			return (res);
		}
	}

	/* Node must NOT be configured */
	if (flags & SCRCMD_FLG_NONODEID) {
		if (access(SCRCMD_NODEID_FILE, F_OK) != -1 ||
		    errno != ENOENT) {
			res = EEXIST;
			(void) fprintf(stderr, "%s:  %s.\n", progname,
			    scrcmd_strerr(res));
			return (res);
		}
	}

	return (0);
}

/*
 * create_socket
 *
 * Create a bound server-side SOCK_STREAM socket for this RPC client.   The
 * protocol family must be either AF_INET or AF_INET6.   If a non-zero port
 * number is given, the socket is bound to the given port number;
 * otherwise, the system will pick a port number.
 *
 * Possible return values:
 *
 *	-1		- could not create/bind socket
 *	other		- the socket fd
 */
static int
create_socket(int family, in_port_t port)
{
	int s;
	int b;
	struct sockaddr_in sin;
	struct sockaddr_in6 sin6;
	struct sockaddr *name;
	socklen_t namelen;

	/* Make sure this is either AF_INET or AF_INET6 */
	if (family != AF_INET && family != AF_INET6) {
		(void) fprintf(stderr, gettext("%s:  Internal error - "
		    "unsupported network protocol family.\n"), progname);
		return (-1);
	}

	/* Create the socket */
	s = socket(family, SOCK_STREAM, 0);
	if (s < 0) {
		(void) fprintf(stderr, gettext("%s:  Cannot create socket "
		    "- %s.\n"), progname, strerror(errno));
		return (-1);
	}

	/* Bind the socket */
	switch (family) {
	case AF_INET:
		bzero(&sin, sizeof (sin));
		sin.sin_family = AF_INET;
		sin.sin_addr.s_addr = INADDR_ANY;
		sin.sin_port = port;
		name = (struct sockaddr *)&sin;
		namelen = sizeof (sin);
		break;

	case AF_INET6:
		bzero(&sin6, sizeof (sin6));
		sin6.sin6_family = AF_INET6;
		sin6.sin6_addr = in6addr_any;
		sin6.sin6_port = port;
		name = (struct sockaddr *)&sin6;
		namelen = sizeof (sin6);
		break;

	default:
		(void) close(s);
		return (-1);
	}
	b = bind(s, name, namelen);
	if (b < 0) {
		(void) fprintf(stderr, gettext("%s:  Cannot bind to socket "
		    "- %s.\n"), progname, strerror(errno));
		(void) close(s);
		return (-1);
	}

	/* return socket */
	return (s);
}

/*
 * get_port
 *
 * Get the port number associated with the given socket, "s".
 * The protocol family must be either AF_INET or AF_INET6.
 *
 * Possible return values:
 *
 *	-1		- could not determine port number
 *	other		- the port number
 */
static int
get_port(int family, int s)
{
	struct sockaddr_in sin;
	struct sockaddr_in6 sin6;
	struct sockaddr *name;
	socklen_t namelen;

	/* Make sure this is either AF_INET or AF_INET6 */
	if (family != AF_INET && family != AF_INET6) {
		(void) fprintf(stderr, gettext("%s:  Internal error - "
		    "unsupported network protocol family.\n"), progname);
		return (-1);
	}

	/* Initialize the sockaddr name */
	switch (family) {
	case AF_INET:
		bzero(&sin, sizeof (sin));
		name = (struct sockaddr *)&sin;
		namelen = sizeof (sin);
		break;

	case AF_INET6:
		bzero(&sin6, sizeof (sin6));
		name = (struct sockaddr *)&sin6;
		namelen = sizeof (sin6);
		break;

	default:
		return (-1);
	}

	/* Get the port number */
	if (getsockname(s, name, &namelen) < 0) {
		(void) fprintf(stderr, gettext("%s:  Cannot get port number "
		    "- %s.\n"), progname, strerror(errno));
		return (-1);
	}

	/* Return the port number */
	switch (family) {
	case AF_INET:
		/*lint -e(644) */
		return (sin.sin_port);

	case AF_INET6:
		/*lint -e(644) */
		return (sin6.sin6_port);

	default:
		return (-1);
	}
}

/*
 * noblocking
 *
 * Mark the file descriptor non-blocking.
 */
static int
noblocking(int fd)
{
	int flags;

	if ((flags = fcntl(fd, F_GETFL, 0)) < 0)
		return (-1);

	if (fcntl(fd, F_SETFL, (flags | FNDELAY)) < 0)
		return (-1);

	return (0);
}

/*
 * accept_connections
 *
 * Accept connections on "s1v6" and on "s2v6".
 * The file descriptors for the respective connections are returned
 * in "fd1p" and "fd2p".  "s1v6" and "s2v6" must be AF_INET6 sockets.
 *
 * Possible return values:
 *
 *	0		- success
 *	EPROTONOSUPPORT	- failed to accept connection
 *	ETIMEDOUT	- connection timed out
 */
static int
accept_connections(int s1v6, int s2v6, int *fd1p, int *fd2p)
{
	int res = 0;
	int fd1 = -1;
	int fd2 = -1;
	fd_set readfds;
	struct timeval accept_timeout   = { SCINSTALL_ACCEPT_TO, 0 };
	int num;

	/* Accept the two connections */
	while (fd1 == -1 || fd2 == -1) {

		/* Initialize readfds for stdout and stderr sockets */
		/*lint -e(534,573,737,713) */
		FD_ZERO(&readfds);
		if (fd1 == -1) {
			/*lint -e(573,737,713) */
			FD_SET(s1v6, &readfds);
		}
		if (fd2 == -1) {
			/*lint -e(573,737,713) */
			FD_SET(s2v6, &readfds);
		}

		/* Select on our sockets */
		num = select(FD_SETSIZE - 1, &readfds, NULL, NULL,
		    &accept_timeout);
		switch (num) {
		case 0:
			(void) fprintf(stderr, gettext("%s:  Failed to "
			    "accept connection - timed out.\n"), progname);
			return (ETIMEDOUT);

		case -1:
			(void) fprintf(stderr, gettext("%s:  select failed "
			    "- %s.\n"), progname, strerror(errno));
			return (EPROTONOSUPPORT);

		default:
			break;
		}

		/* If an s1 socket was selected, accept the connection */
		/*lint -e(573,737) */
		if (FD_ISSET(s1v6, &readfds)) {
			res = accept_connection(s1v6, &fd1);
			if (res != 0)
				return (res);
		}

		/* If an s2 socket was selected, accept the connection */
		/*lint -e(573,737) */
		if (FD_ISSET(s2v6, &readfds)) {
			res = accept_connection(s2v6, &fd2);
			if (res != 0)
				return (res);
		}
	}

	/* Set the fd pointers */
	*fd1p = fd1;
	*fd2p = fd2;

	/* Done */
	return (0);
}

/*
 * accept_connection
 *
 * Accept a connection on socket "s", returning the associated
 * file descriptor in "fdp". The protocol family must be either
 * AF_INET or AF_INET6.
 *
 * Possible return values:
 *
 *	0		- success
 *	EPROTONOSUPPORT	- failed to accept connection
 */
static int
accept_connection(int s, int *fdp)
{
	struct sockaddr_in6 sin6;
	struct sockaddr *name;
	socklen_t namelen;
	int fd;

	/* Initialize the sockaddr name */
	bzero(&sin6, sizeof (sin6));
	name = (struct sockaddr *)&sin6;
	namelen = sizeof (sin6);

	/* Accept connection */
	fd = accept(s, name, &namelen);
	if (fd < 0) {
		(void) fprintf(stderr, gettext("%s:  Failed to accept "
		    "connection - %s.\n"), progname, strerror(errno));
		return (EPROTONOSUPPORT);
	}

	/* Set the fd pointer */
	*fdp = fd;

	/* Done */
	return (0);
}

/*
 * rw_cmd_output
 *
 * Read stdout from the remote command on the "fd_out" connection and
 * print it to the stdout of this process.   Read stderr from the
 * remote command on the "fd_err" connection and print it to the
 * stderr of this process.  The exit status of the remote
 * command is left in "command_statusp".
 *
 * Possible return values:
 *
 *	0		- success
 *	EIO		- IO error
 *	ETIMEDOUT	- connection timed out
 *	ECONNABORTED	- connection aborted before command completed
 */
static int
rw_cmd_output(int fd_out, int fd_err, int *command_statusp)
{
	fd_set readfds;
	fd_set errorfds;
	int num;
	char buffer[BUFSIZ];
	char line[BUFSIZ];
	char *lptr = line;
	char *lend = &line[sizeof (line) - 1];
	int lcount = 0;
	int i;
	ssize_t count;
	int eof_out = 0;
	int eof_err = 0;
	int command_status = -1;

	/* Read-write stderr first */
	while (eof_err == 0) {

		/* Initialize readfds */
		/*lint -e(534,573,737,713) */
		FD_ZERO(&readfds);
		/*lint -e(573,737,713) */
		FD_SET(fd_err, &readfds);

		/* Initialize errorfds */
		/*lint -e(534,573,737,713) */
		FD_ZERO(&errorfds);
		/*lint -e(573,737,713) */
		FD_SET(fd_err, &errorfds);

		/*
		 * Wait for something to read or EOF
		 */
		num = select(FD_SETSIZE - 1, &readfds, NULL, &errorfds, NULL);
		if (num < 0) {
			(void) fprintf(stderr, gettext("%s:  select failed "
			    "- %s.\n"), progname, strerror(errno));
			return (EPROTONOSUPPORT);
		}

		/*
		 * Try to read data from our stderr connection, then
		 * write it to the stderr of this process.
		 */
		count = read(fd_err, buffer, sizeof (buffer));
		while (count > 0) {
			(void) write(2, buffer, (size_t)count);
			count = read(fd_err, buffer, sizeof (buffer));
		}
		if (count == 0) {
			++eof_err;

		} else if (errno != EWOULDBLOCK) {
			(void) fprintf(stderr,
			    gettext("%s:  read failed - %s.\n"),
			    progname, strerror(errno));
			return (EIO);
		}
	}

	while (eof_out == 0) {

		/* Initialize readfds */
		/*lint -e(534,573,737,713) */
		FD_ZERO(&readfds);
		/*lint -e(573,737,713) */
		FD_SET(fd_out, &readfds);

		/* Initialize errorfds */
		/*lint -e(534,573,737,713) */
		FD_ZERO(&errorfds);
		/*lint -e(573,737,713) */
		FD_SET(fd_out, &errorfds);

		/*
		 * Wait for something to read or EOF
		 */
		num = select(FD_SETSIZE - 1, &readfds, NULL, &errorfds, NULL);
		if (num < 0) {
			(void) fprintf(stderr, gettext("%s:  select failed "
			    "- %s.\n"), progname, strerror(errno));
			return (EPROTONOSUPPORT);
		}

		/*
		 * Try to read data from our stdout connection, then
		 * write it to the stdout of this process.
		 * We are also searching for a line which will
		 * identify the exit status of the remotely executed
		 * command.
		 */
		count = read(fd_out, buffer, sizeof (buffer));
		while (count > 0) {
			/*
			 * Try to assemble into a line and look for our
			 * status string.   In the event that multi-byte
			 * characters occur in the data stream, we
			 * could mistakenly identify a '\n' which is not
			 * meant to be a line feed.   However, this
			 * should not do any harm.
			 */
			for (i = 0;  i < count;  ++i) {
				if (lptr == lend) {
					(void) write(1, line, (size_t)lcount);
					lptr = line;
					lcount = 0;
					continue;
				}
				*lptr = buffer[i];
				if (*lptr == '\n') {
					char *string;
					char *lstring;
					char *sptr;
					int found_status = 0;

					*lptr = '\0';
					sptr = NULL;
					if ((string = strdup(line)) != NULL) {
						sptr = strtok_r(string,
						    "=", &lstring);
					}
					if (sptr != NULL &&
					    strcmp(sptr,
					    SCRCMD_CMD_STATUS_STRING) == 0) {
						sptr = strtok_r(NULL, "=",
						    &lstring);
						if (sptr != NULL) {
							command_status =
							    atoi(sptr);
							++found_status;
						}
					}
					free(string);
					*lptr = '\n';

					/* If not status line, write it out */
					if (!found_status) {
						(void) write(1, line,
						    (size_t)(lcount + 1));
					}
					lptr = line;
					lcount = 0;
				} else {
					++lptr;
					++lcount;
				}
			}

			/* Get the next buffer full */
			count = read(fd_out, buffer, sizeof (buffer));
		}
		if (count == 0) {
			++eof_out;

		} else if (errno != EWOULDBLOCK) {
			(void) fprintf(stderr,
			    gettext("%s:  read failed - %s.\n"),
			    progname, strerror(errno));
			return (EIO);
		}
	}

	/* Set the command status */
	if (command_status != -1)
		*command_statusp = command_status;

	/* Done */
	return (0);
}

/*
 * fill_buf_with_file
 *
 * Fills the buffer with the contents of the file referenced
 * by filename.
 */
static int
fill_buf_with_file(char **buffer, char *filename)
{
	long size;
	FILE *fp;

	if (filename == NULL)
		return (1);

	/* Open the file for reading */
	fp = fopen(filename, "r");
	if (fp == NULL) {
		(void) fprintf(stderr, "Error opening file - %s\n",
		    strerror(errno));
		return (EIO);
	}

	/* obtain file size */
	(void) fseek(fp, 0, SEEK_END);
	size = ftell(fp);
	rewind(fp);

	/* allocate memory to contain the whole file */
	*buffer = (char*) malloc((unsigned int) size);
	if (buffer == NULL)
		return (ENOMEM);

	/* copy the file into the buffer */
	(void) fread(*buffer, 1, (unsigned int) size, fp);

	/* Close the file */
	if (fclose(fp) != 0) {
		(void) fprintf(stderr,
		    "Warning:  Unable to close file %s\n", filename);
	}

	return (0);
}

/*
 * create_envlist
 *
 * Return a list of selected environment variable settings.
 *
 * This function returns NULL if there is not enough memory to allocate
 * the list.   An empty list is returned if none of the environment
 * variables of interest are set.
 */
static sc_envlist_t *
create_envlist(void)
{
	sc_envlist_t *envlist;
	int i;
	uint_t size;
	sc_env_t *ptr;
	char *value;
	char *envbuffer;
	char *env_names[] = {
		"LANG",
		"LC_ALL",
		"LANG",
		"LC_ALL",
		"LC_CTYPE",
		"LC_COLLATE",
		"LC_MESSAGES",
		"LC_MONETARY",
		"LC_NUMERIC",
		"LC_TIME",
		"SC_INSTALL_LOG",
		"SC_PATCH_INSTALL_LOG",
		CLCMD_ENV,
		(char *)0
	};

	/* Allocate the envlist */
	envlist = (sc_envlist_t *)calloc(1, sizeof (sc_envlist_t));
	if (envlist == NULL) {
		(void) fprintf(stderr, "%s:  %s.\n", progname,
		    scrcmd_strerr(ENOMEM));
		return (NULL);
	}

	/* How many members should be in the array? */
	for (i = 0;  env_names[i] != NULL;  ++i) {
		if (getenv(env_names[i]) != NULL) {
			++envlist->sc_envlist_t_len;
		}
	}

	/* Allocate the array */
	envlist->sc_envlist_t_val = (sc_env_t *)calloc(
	    envlist->sc_envlist_t_len, sizeof (char *));
	if (envlist->sc_envlist_t_val == NULL) {
		(void) fprintf(stderr, "%s:  %s.\n", progname,
		    scrcmd_strerr(ENOMEM));
		free_envlist(envlist);
		return (NULL);
	}

	/* Fill the array */
	ptr = envlist->sc_envlist_t_val;
	for (i = 0;  env_names[i] != NULL;  ++i) {
		if ((value = getenv(env_names[i])) == NULL) {
			continue;
		}
		size = strlen(env_names[i]) + strlen("=") + strlen(value) + 1;
		envbuffer = (char *)calloc(1, size);
		if (envbuffer == NULL) {
			(void) fprintf(stderr, "%s:  %s.\n", progname,
			    scrcmd_strerr(ENOMEM));
			free_envlist(envlist);
			return (NULL);
		}
		(void) sprintf(envbuffer, "%s=%s", env_names[i], value);
		*ptr++ = envbuffer;
	}

	return (envlist);
}

/*
 * free_envlist
 *
 * Free all memory associated with the given "envlist".
 */
static void
free_envlist(sc_envlist_t *envlist)
{
	uint_t i;

	if (envlist == NULL)
		return;

	if (envlist->sc_envlist_t_val != NULL) {
		for (i = 0;  i < envlist->sc_envlist_t_len;  ++i) {
			if (envlist->sc_envlist_t_val[i] != NULL) {
				free(envlist->sc_envlist_t_val[i]);
			}
		}
		free(envlist->sc_envlist_t_val);
	}

	free(envlist);
}

/*
 * scrcmd_clnt_create
 *
 * Create an RPC client handle to the given "servername".   Upon success,
 * the client handle is returned in "clntp".
 *
 * Possible return values:
 *
 *	0		- success
 *	ENOTACTIVE	- does not allow remote configuration
 *	ETIMEDOUT	- connection timed out
 *	ECONNREFUSED	- connection refused
 */
static int
scrcmd_clnt_create(char *servername, CLIENT **clntp)
{
	int res = 0;
	CLIENT *clnt = NULL;
	struct rpcent rpc_ent;
	struct rpcent *rpc_entp;
	char buffer[BUFSIZ];
	struct timeval clnt_create_timeout = { SCINSTALL_CLNT_CREATE_TO, 0 };
	struct timeval clnt_call_timeout   = { SCINSTALL_CLNT_CALL_TO, 0 };

	/* Check arguments */
	if (servername == NULL) {
		return (ENOTACTIVE);
	}

	/* Initialize the RPC program number, if necessary */
	if (scinstallprog == 0) {
		bzero(&rpc_ent, sizeof (rpc_ent));
		if ((rpc_entp = getrpcbyname_r(SCRCMDPROGNAME, &rpc_ent,
		    buffer, sizeof (buffer))) == NULL) {
			scinstallprog = SCRCMDPROG;
		} else {
			scinstallprog = (rpcprog_t)rpc_entp->r_number;
		}
	}

	/* Create the client handle */
	clnt = clnt_create_timed(servername, scinstallprog, SCRCMDVERS,
	    "circuit_v", &clnt_create_timeout);
	if (clnt == NULL) {
		clnt_pcreateerror(progname);
		/*lint -e(746) */
		switch (rpc_createerr.cf_stat) {
		case RPC_TIMEDOUT:
			res = ETIMEDOUT;
			break;

		default:
			res = ECONNREFUSED;
			break;
		}

		return (res);
	}

	/* Set the timeout value for clnt_call */
	(void) clnt_control(clnt, CLSET_TIMEOUT, (char *)&clnt_call_timeout);

	/* Set the client pointer */
	*clntp = clnt;

	/* Done */
	return (res);
}

/*
 * print_remote_errlist
 *
 * Print the "errlist" list of messages from the remote "servername"
 * onto our stderr.  Messages should already be localized.
 */
static void
print_remote_errlist(char *servername, sc_namelist_t *errlist)
{
	uint_t i;
	char *msg;

	/* If no errors to print, just return */
	if (errlist == NULL ||
	    errlist->sc_namelist_t_len == 0 ||
	    errlist->sc_namelist_t_val == (char **)0 ||
	    *errlist->sc_namelist_t_val == (char *)0 ||
	    **errlist->sc_namelist_t_val == '\0') {
		return;
	}

	/* Check for servername */
	if (servername == NULL) {
		servername = gettext("remote call failed");
	}

	/* Print the messages */
	for (i = 0;  i < errlist->sc_namelist_t_len;  ++i) {
		if (errlist->sc_namelist_t_val[i] == NULL ||
		    *(errlist->sc_namelist_t_val[i]) == '\0')
			continue;

		/* Error messages should already be localized */
		msg = errlist->sc_namelist_t_val[i];

		/* print the message */
		(void) fprintf(stderr, "%s:  %s - %s.\n", progname,
		    servername, msg);
	}
}

/*
 * free_errlist_messages
 *
 * Free the messages in the given errlist.
 */
static void
free_errlist_messages(sc_namelist_t *errlist)
{
	uint_t i;

	/* If nothing to free, just return */
	if (errlist == NULL ||
	    errlist->sc_namelist_t_len == 0 ||
	    errlist->sc_namelist_t_val == (char **)0) {
		return;
	}

	/* Free the memory */
	for (i = 0;  i < errlist->sc_namelist_t_len;  ++i) {
		if (errlist->sc_namelist_t_val[i] != NULL)
			free(errlist->sc_namelist_t_val[i]);
	}
	free(errlist->sc_namelist_t_val);

	/* Reset the errlist structure */
	bzero(errlist, sizeof (sc_namelist_t));
}

/*
 * scrcmd_strerr
 *
 * Map the given "errno" to a string and return that string.
 */
static char *
scrcmd_strerr(int err)
{
	char *errmsg;

	switch (err) {
	case 0:
		errmsg = gettext(SCRCMD_MSG_NOERR);
		break;

	case EINVAL:
		errmsg = gettext(SCRCMD_MSG_EINVAL);
		break;

	case ENOMEM:
		errmsg = gettext(SCRCMD_MSG_ENOMEM);
		break;

	case EEXIST:
		errmsg = gettext(SCRCMD_MSG_EEXIST);
		break;

	case EPROTONOSUPPORT:
		errmsg = gettext(SCRCMD_MSG_EPROTONOSUPPORT);
		break;

	case ENOTACTIVE:
		errmsg = gettext(SCRCMD_MSG_ENOTACTIVE);
		break;

	case ETIMEDOUT:
		errmsg = gettext(SCRCMD_MSG_ETIMEDOUT);
		break;

	case ECONNREFUSED:
		errmsg = gettext(SCRCMD_MSG_ECONNREFUSED);
		break;

	case ENOEXEC:
		errmsg = gettext(SCRCMD_MSG_ENOEXEC);
		break;

	case EIO:
		errmsg = gettext(SCRCMD_MSG_EIO);
		break;

	case ECONNABORTED:
		errmsg = gettext(SCRCMD_MSG_ECONNABORTED);
		break;

	default:
		errmsg = strerror(err);
		break;
	}

	return (errmsg);
}

/*
 * scrcmd_err_to_exitcode
 *
 * Map the given "errno" to an scrcmd exit code.
 */
static int scrcmd_err_to_exitcode(int err)
{
	int exitcode;

	switch (err) {
	case 0:
		exitcode = SCRCMD_NOERR;
		break;

	case EINVAL:
		exitcode = SCRCMD_EINVAL;
		break;

	case ENOMEM:
		exitcode = SCRCMD_ENOMEM;
		break;

	case EEXIST:
		exitcode = SCRCMD_EEXIST;
		break;

	case EPROTONOSUPPORT:
		exitcode = SCRCMD_EPROTONOSUPPORT;
		break;

	case ENOTACTIVE:
		exitcode = SCRCMD_ENOTACTIVE;
		break;

	case ETIMEDOUT:
		exitcode = SCRCMD_ETIMEDOUT;
		break;

	case ECONNREFUSED:
		exitcode = SCRCMD_ECONNREFUSED;
		break;

	case ENOEXEC:
		exitcode = SCRCMD_ENOEXEC;
		break;

	case EIO:
		exitcode = SCRCMD_EIO;
		break;

	case ECONNABORTED:
		exitcode = SCRCMD_ECONNABORTED;
		break;

	default:
		exitcode = SCRCMD_UNKNOWN;
		break;
	}

	return (exitcode);
}
