/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)rgmd.cc	1.24	08/07/21 SMI"

/*
 * rgmd.cc
 *
 * This file builds the rgmd daemon
 *
 */

#include <stdio.h>
#include <locale.h>
#include <stdlib.h>
#include <unistd.h>
#include <thread.h>
#include <sys/sc_syslog_msg.h>
#include <sys/rsrc_tag.h>
#include <signal.h>
#include <fcntl.h>
#include <syslog.h>
#include <rgm/rgm_malloc.h>
#include <sys/os.h>
#include <rgm/sczones.h>
#include <dlfcn.h>

// Pre-allocate a buffer of this size for use by syslog when out of swap space
const size_t RGM_SYSLOG_BUFFER_SIZE = 4 * 1024;

extern int	startrgmdstarter(int argc, char **argv);
static void	*syslog_buf = NULL;

#ifndef DEBUG

#define	SYSTEXT(s)	s

#ifndef MAXLINE
#define	MAXLINE 1024
#endif /* MAXLINE */

extern int	startrgmdstarter(int argc, char **argv);
static void print_msg_and_exit(char *msg);
static void final_msg_timeout_hdlr(int sig);
#endif // DEBUG




//
// malloc_nocheck
//
// This function simply calls malloc() defined outside rgmd.
// By default it will be libc's malloc(). Otherwise it will be
// the malloc() defined in any of the preloaded libraries.
//
// This function should be called if the caller can handle malloc returning
// NULL. For the rgmd process, the default call to malloc() will get a
// function which logs an error and kills rgmd and the node if memory
// cannot be allocated.
//
void *
malloc_nocheck(size_t n)
{
	static os::mutex_t	alloc_mutex;
	static void *		(*func)(size_t) = NULL;

	if (func == NULL) {
		alloc_mutex.lock();
		if (func == NULL) {
			func = (void *(*)(size_t))dlsym(RTLD_NEXT,
			    "malloc"); //lint !e611 function pointer cast is OK

			if (func == NULL) {
				//
				// No malloc() loaded. Severe problem! Exit.
				//
				(void) printf("malloc() is not loaded; "
				    "exiting.\n");
				exit(1);
			}
		}
		alloc_mutex.unlock();
	}
	return ((*func)(n));
}

//
// Use the overloaded malloc only for non-debug builds
//
#ifndef DEBUG
//
// Function that replaces malloc() from the C library for the RGMD process.
// This function calls malloc from the C library. If malloc() returns NULL
// (implying that the system is out of swap space), this function logs an
// error and exits rgmd (which causes the node to panic).
//
// If the caller (of malloc) does not want this default behavior, (s)he should
// call malloc_nocheck() (which simply calls malloc from the C library
// without checking for a NULL return value.
//
// Since syslog() calls malloc to allocate some memory, we need to use a hack
// to allow a syslog message to be printed when we are out of swap space. We
// pre-allocate some memory and free it when we first get a malloc failure.
// Syslog can then use it for printing the out-of-swap error message before
// the process exits.
//
// For debug builds build without the overloaded malloc so
// we can use tools like libumem and purify.
//
#ifndef linux
void *
malloc(size_t n)
#else
void *
malloc(size_t n) throw()
#endif
{
	static os::mutex_t	malloc_mutex;
	void			*bufptr = NULL;

	bufptr = malloc_nocheck(n);

	if (bufptr == NULL) {
		char		msgbuff[MAXLINE];

		//
		// Not enough swap space left. Log a message and exit.
		//

		malloc_mutex.lock();
		if (syslog_buf != NULL) {
			//
			// First failed memory allocation call. Free
			// syslog_buf for syslog to use for logging.
			//
			free(syslog_buf);
			syslog_buf = NULL;
			(void) sprintf(msgbuff,
			    SYSTEXT("RGM: Could not allocate %d bytes; "
			    "node is out of swap space; aborting node."), n);
			print_msg_and_exit(msgbuff);
		}
	}
	return (bufptr);
}
#endif // DEBUG


int
main(int argc, char *argv[])
{
	int			rc;

	//
	// Allocate the 'syslog_buffer' (used for 'last gasp' syslog msg.)
	//
	syslog_buf = malloc_nocheck(RGM_SYSLOG_BUFFER_SIZE);
	if (syslog_buf == NULL) {
		//
		// Cannot syslog this message. Print to the
		// console.
		//
		(void) fprintf(stderr, "RGM: no swap space; exiting\n");
		_exit(1);
	}

	/*
	 * set message path and file name
	 */
	(void) setlocale(LC_ALL, "");
	(void) textdomain(TEXT_DOMAIN);
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

#ifndef linux
	if (sc_zonescheck() != 0)
		return (1);
#endif

	(void) sc_syslog_msg_set_syslog_tag(SC_SYSLOG_RGM_RGMD_TAG);
	rc = startrgmdstarter(argc, argv);
	return (rc);
}


#ifndef DEBUG
//
// print_msg_and_exit() is called from rgmd interposed malloc in the event of
// error from "real" malloc (out of memory).
//
// We attempt to print a message to the console (in case of problems with
// syslog) and also syslog a final message. We exit (_exit1) when finished.
//
// An alarm is set to timeout in FINAL_MSG_TIMEOUT secs. in case the message
// operations hang. The exit code in the signal handler (final_msg_timeout_hdlr)
// should be identical to the exit code at the end of print_msg_and_exit().
//
static void
print_msg_and_exit(char *msg)
{
#define	FINAL_MSG_TIMEOUT	5
#define	ctty		"/dev/console"
#define	sysmsg		"/dev/sysmsg"

	sigset_t	sigs;
	unsigned int 	msglen;
	int		fd;

	// We set an alarm here in case either printing to the
	// console or syslog() should hang for some reason.
	//
	(void) signal(SIGALRM, final_msg_timeout_hdlr);
	(void) alarm(FINAL_MSG_TIMEOUT);
	(void) sigemptyset(&sigs);
	(void) sigaddset(&sigs, SIGALRM);
	(void) sigprocmask(SIG_UNBLOCK, &sigs, NULL);
	if (((fd = open(sysmsg, O_WRONLY)) >= 0) ||
	    (fd = open(ctty, O_WRONLY)) >= 0) {
		msglen = strlen(msg);
		msg[msglen] = '\n';
		(void) write(fd, msg, msglen);
		msg[msglen] = '\0';
		(void) close(fd);
	}
	openlog("RGMD", LOG_NOWAIT|LOG_PID, LOG_DAEMON);
	syslog(LOG_ERR, msg);
	closelog();

	//
	// Exit code here should be identical to that of
	// final_msg_timeout_hdlr()
	//
	_exit(1);
}

//
// Signal handler for SIGALRM used to timeout the printing of the
// final message from the rgmd in the event of malloc failure.
//
// Set by print_msg_and_exit()
//
static void
final_msg_timeout_hdlr(int)
{
	_exit(1);
}
#endif // DEBUG
