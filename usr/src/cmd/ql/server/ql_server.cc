/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)ql_server.cc	1.3	08/05/20 SMI"

#include <nslib/ns.h>
#include <libintl.h>
#include <signal.h>
#include <ql_err.h>
#include <ql_log.h>
#include <ql_ccr.h>
#include <ql_obj_impl.h>
#include <ql_rgm_obj_impl.h>
#include <ql_cl_ccr.h>
#include <rgm/sczones.h>

#define	SLEEP_INTERVAL	30

#define	QL_SERVER	"QL_SERVER"
#define	QL_SERVER_LOG	"/var/cluster/logs/ql_server_debug.log"

static ql_obj_impl	*ql_obj_implp		= NULL;
static ql_rgm_obj_impl	*ql_rgm_obj_implp	= NULL;
ql_log			*logp			= NULL;

const char *quantum_leap::ql_obj::QL_OBJ_KEY;
const char *quantum_leap::ql_rgm_obj::QL_RGM_OBJ_KEY;

//
// Check the type of exception and log the corresponding error
// message.
//
static void
check_register_exception(CORBA::Exception *exp)
{
	if (naming::not_found::_exnarrow(exp)) {
		logp->log_err(QL_SERVER, "not_found exception when "
		    "registering CORBA object\n");

	} else if (naming::cannot_proceed::_exnarrow(exp)) {
		logp->log_err(QL_SERVER, "cannot_proceed exception when "
		    "registering CORBA object\n");

	} else if (naming::invalid_name::_exnarrow(exp)) {
		logp->log_err(QL_SERVER, "invalid_name exception when "
		    "registering CORBA object\n");

	} else if (naming::already_bound::_exnarrow(exp)) {
		logp->log_err(QL_SERVER, "already_bound exception when "
		    "registering CORBA object\n");

	} else {
		logp->log_err(QL_SERVER, "Unknown exception when "
		    "registering CORBA object, id = %d\n",
		    exp->exception_enum());
	}
}

//
// Register the quantum-leap object in the global name server.
// Return value:
//	QL_NOERR	On success
//	Failure:
//		QL_EBIND
//		QL_ELOOKUP
//
//
static ql_errno_t
register_ql_object(quantum_leap::ql_obj_ptr ql_obj_p)
{
	Environment 			e;
	CORBA::Exception 		*exp;
	naming::naming_context_var 	root_ns_v;
	ql_errno_t			retval = QL_NOERR;

	logp->log_info(QL_SERVER, "Register QL object\n");

	//
	// Need to register the quantum_leap object in the global
	// name server as part of the quantum-leap upgrade.
	//
	root_ns_v =  ns::root_nameserver();

	if (CORBA::is_nil(root_ns_v)) {
		logp->log_err(QL_SERVER,
		    "Global name-server reference is Null\n");
		return (QL_ELOOKUP);
	}

	root_ns_v->bind(quantum_leap::ql_obj::QL_OBJ_KEY, ql_obj_p, e);

	if ((exp = e.exception()) != NULL) {
		check_register_exception(exp);
		e.clear();
		retval = QL_EBIND;
	}
	return (retval);
}

//
// Register the RGM object in the local name server.
// Return value:
//	QL_NOERR	On success
//	Failure:
//		QL_EBIND
//		QL_ELOOKUP
//
static ql_errno_t
register_rgm_object(quantum_leap::ql_rgm_obj_ptr ql_rgm_obj_p)
{
	Environment 			e;
	CORBA::Exception 		*exp;
	naming::naming_context_var 	root_ns_v;
	ql_errno_t			retval = QL_NOERR;

	logp->log_info(QL_SERVER, "Register RGM object\n");

	//
	// Need to register the "RGM object" in the local name-server
	// as part of the quantum-leap upgrade.
	//
	root_ns_v =  ns::local_nameserver();

	if (CORBA::is_nil(root_ns_v)) {
		logp->log_err(QL_SERVER,
		    "Global name-server reference is Null\n");
		return (QL_ELOOKUP);
	}

	root_ns_v->bind(quantum_leap::ql_rgm_obj::QL_RGM_OBJ_KEY,
	    ql_rgm_obj_p, e);

	if ((exp = e.exception()) != NULL) {
		check_register_exception(exp);
		e.clear();
		retval = QL_EBIND;
	}
	return (retval);
}

//
// Check if any invocations are in progress in the RGM server object.
// If so wait till the invocations complete.
//
static void
quiesce_rgm_invos()
{
	Environment			env;
	quantum_leap::ql_rgm_obj_ptr	ql_rgm_obj_p;
	CORBA::Exception		*ex		= NULL;

	ASSERT(ql_rgm_obj_implp != NULL);

	ql_rgm_obj_p = ql_rgm_obj_implp->get_objref();

	while (true) {
		if (!ql_rgm_obj_p->is_rgm_blocked(env)) {
			if ((ex = env.exception()) != NULL) {
				logp->log_info(QL_SERVER, "Encountered "
				    "exception, id = %d\n",
				    ex->exception_enum());
				env.clear();
				return;
			}
			break;
		}
		(void) sleep((uint_t)SLEEP_INTERVAL);
	}
	CORBA::release(ql_rgm_obj_p);
}

//
// Wait till we find that the RGM object has been unregistered from the
// name-server.
//	Return value:
//	QL_NOERR	On success.
//	QL_ELOOKUP	On Error.
//
static ql_errno_t
wait_for_unregister()
{
	Environment			env;
	naming::naming_context_var	ctx_v	= ns::local_nameserver();
	CORBA::Exception 		*ex	= NULL;
	CORBA::Object_var		obj_v;
	ql_errno_t			retval	= QL_NOERR;

	ASSERT(!CORBA::is_nil(ctx_v));

	while (true) {

		// Wait till the upgrade is done.
		(void) sleep((uint_t)SLEEP_INTERVAL);

		// Look for the "RGM object" in the name server
		obj_v =
		    ctx_v->resolve(quantum_leap::ql_rgm_obj::QL_RGM_OBJ_KEY,
		    env);

		if ((ex = env.exception()) != NULL) {
			if (naming::not_found::_exnarrow(ex) != NULL) {
				//
				// This is not an error condition. We are done.
				// return.
				//
				logp->log_info(QL_SERVER, "RGM object "
				    "unregistered\n");
			} else {
				logp->log_info(QL_SERVER, "Exception "
				    "encountered during RGM object lookup, "
				    "id = %d\n", ex->exception_enum());
				retval = QL_ELOOKUP;
			}
			env.clear();
			// Need to make sure no invocations exist.
			quiesce_rgm_invos();
			break;
		}
	}
	return (retval);
}

//
// This is executed when quantum-leap upgrade is in progress.
// It does the following:
// 1. If this is the root node of partition A
// 	Register the "quantum-leap" object in the global nameserver.
// 2. Wait till the "RGM-object" gets unregistered.
//	Return value:
//	QL_NOERR	On success.
//	Failure:
//		QL_ECLCONF
//		QL_ECCR
//		QL_ENOMEM
//		QL_EBIND
//		QL_ELOOKUP
//
static ql_errno_t
execute_ql_upgrade_server(ql_cl_ccr *ql_ccr_objp)
{
	nodeid_t			local_nodeid;
	char				local_nodename[CL_MAX_LEN+1];
	Environment			env;
	quantum_leap::ql_obj_ptr	ql_obj_p		= nil;
	CORBA::Exception		*ex			= NULL;
	char				*partition_a_root_node	= NULL;
	ql_errno_t			retval			= QL_NOERR;
	bool				is_root_node		= false;

	//
	// Check to see if quantum-leap upgrade is in state QL_UPGRADE_STATE_1
	// If not, we flag an error. We do proceed further here and not exit.
	//
	if (!ql_ccr_objp->ql_upgrade_check_state(QL_UPGRADE_STATE_1)) {
		logp->log_err(QL_SERVER, "QL upgrade in invalid state !\n");
	}

	//
	// Initialize clconf library.
	//
	if (clconf_lib_init() != 0) {
		logp->log_err(QL_SERVER,
		    "Failed to initialize clconf library\n");
		return (QL_ECLCONF);
	}

	//
	// Get the node id of this node.
	//
	local_nodeid = clconf_get_local_nodeid();

	//
	// Get the node name of this node.
	//
	clconf_get_nodename(local_nodeid, local_nodename);

	//
	// Fetch the root node of partition A from the CCR.
	//
	if (ql_ccr_objp->ql_read_root_a(&partition_a_root_node) != 0) {
		logp->log_err(QL_SERVER,
		    "Error reading root node of partition A\n");
		return (QL_ECCR);
	}

	if (strcmp(local_nodename, partition_a_root_node) == 0) {
		is_root_node = true;
	}

	//
	// Check if we are running on the root node.
	//
	if (is_root_node) {

		logp->log_info(QL_SERVER,
		    "Running on root node of partition A\n");

		//
		// We are running on the root node of partition A. We register
		// the ql_object in the global nameserver.
		//
		ql_obj_implp = new ql_obj_impl();

		if (ql_obj_implp == NULL) {
			logp->log_err(QL_SERVER,
			    "Failed to allocate quantum-leap object\n");
			return (QL_ENOMEM);
		}
		ql_obj_p = ql_obj_implp->get_objref();

		//
		// Register the quantum_leap object in the global name server.
		//
		logp->log_info(QL_SERVER,
		    "Registering QL object in the global name-server\n");

		if (register_ql_object(ql_obj_p) != QL_NOERR) {
			//
			// Failed to register the QL object, cant proceed
			// further.
			//
			return (QL_EBIND);
		}
	} else {
		//
		// We are not running on the root node of partition A.
		//
		logp->log_info(QL_SERVER,
		    "Not running on root node of partition A\n");
	}
	//
	// We wait here till the RGM_object is unregistered from
	// the nameserver.
	//
	retval = wait_for_unregister();

	//
	// Before we return make sure we dont have any invocations
	// in progress on the quantum-leap object.
	//
	if (is_root_node) {
		bool invos_in_progress = false;

		while (true) {
			invos_in_progress = ql_obj_p->is_ql_blocked(env);

			if ((ex = env.exception()) != NULL) {
				logp->log_info(QL_SERVER, "Encountered "
				    "exception, id = %d\n",
				    ex->exception_enum());
				env.clear();
				return (retval);
			}
			if (invos_in_progress) {
				(void) sleep((uint_t)SLEEP_INTERVAL);
			} else {
				break;
			}
		}
	}
	CORBA::release(ql_obj_p);
	return (retval);
}

//
// Main routine.
// Return values:
//	QL_NOERR	On success.
//	Failure:
//		QL_ENOLOG
//		QL_ENOMEM
//		QL_ECLCONF
//		QL_ECCR
//		QL_EBIND
//		QL_ELOOKUP
//
int
main()
{
	ql_cl_ccr			*ql_ccr_objp		= NULL;
	bool				is_upgrade_in_progress  = false;
	int				retval			= 0;
	quantum_leap::ql_rgm_obj_ptr	ql_rgm_obj_p;

	//
	// Return an error if run from a non-global zone.
	//
	if (sc_zonescheck() != 0) {
		return (1);
	}

	//
	// Mask all signals.
	//
	sigset_t	sigs;
	(void) sigfillset(&sigs);
	(void) sigprocmask(SIG_BLOCK, &sigs, NULL);

	logp = ql_log::get_instance(QL_SERVER_LOG);

	if (logp == NULL) {
		(void) printf(gettext("Log object is null\n"));
		return (QL_ENOLOG);
	}

	ql_ccr_objp = new ql_cl_ccr();

	if (ql_ccr_objp == NULL) {
		logp->log_err(QL_SERVER,
		    "Failed allocating memory to CCR object\n");
		return (QL_ENOMEM);
	}

	//
	// Register the RGM object in the local nameserver. The "RGM object"
	// is needed to enforce the dependency of RGM on availability of
	// global filesystems (PXFS). RGMD starts off early and does it's
	// initialization. It then blocks until the filesystems are mounted.
	// This happens after mountgfs runs.
	//
	ql_rgm_obj_implp = new ql_rgm_obj_impl();

	if (ql_rgm_obj_implp == NULL) {
		logp->log_err(QL_SERVER, "Failed to allocate RGM object\n");
		return (QL_ENOMEM);
	}
	ql_rgm_obj_p =  ql_rgm_obj_implp->get_objref();

	retval = register_rgm_object(ql_rgm_obj_p);

	if (retval != QL_NOERR) {
		logp->log_err(QL_SERVER, "Error while trying to register"
		    " RGM object\n");
		return (retval);
	}

	//
	// Check if quantum-leap upgrade is in progress. If upgrade is in
	// progress and upgrade state is QL_UPGRADE_STATE_1 we let the root
	// node of partition A host the "quantum-leap" object.
	//
	if (ql_ccr_objp->ql_upgrade_in_progress()) {
		logp->log_info(QL_SERVER, "QL upgrade in progress\n");
		is_upgrade_in_progress = true;
	}

	if (is_upgrade_in_progress &&
	    ql_ccr_objp->ql_upgrade_check_state(QL_UPGRADE_STATE_1)) {
		//
		// We wait in this function till we find the "RGM object"
		// unregistered from the local nameserver.
		//
		retval = execute_ql_upgrade_server(ql_ccr_objp);
		if (retval != QL_NOERR) {
			logp->log_err(QL_SERVER, "Error executing "
			    "ql_upgrade_server\n");
			return (retval);
		}
	} else {
		//
		// This is executed when quantum-leap upgrade is not in
		// progress or it is in progress and upgrade is in state
		// QL_UPGRADE_STATE_2.
		// We still go ahead and register the
		// "RGM object" in the local name-server. We need to wait
		// here till the "RGM object" is unregistered.
		//
		retval = wait_for_unregister();
	}
	CORBA::release(ql_rgm_obj_p);
	return (retval);
}
