/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)ql_upgrade.cc	1.14	08/05/20 SMI"

#include <nslib/ns.h>
#include <libintl.h>
#include <signal.h>
#include <pthread.h>
#include <fcntl.h>
#include <syslog.h>
#include <cmm/cmm_ns.h>
#include <sys/nodeset.h>
#include <ql_err.h>
#include <ql_log.h>
#include <ql_ccr.h>
#include <ql_obj_impl.h>
#include <ql_cl_ccr.h>
#include <rgm/sczones.h>
#include <ccr_access.h>

#define	BUFFER_SIZE		1024

// The alternate boot environment mount point
static char		mount_point[BUFFER_SIZE];


//
// Some commands are Quantum-leap specific scripts. If there is any
// change to the path where the scripts reside or change in name
// it should be reflected here.
//

#define	QL_UTIL_FILE		"/usr/cluster/lib/scadmin/ql/ql_util"

#define	QL_STOP_APP_CMD		"/usr/cluster/lib/scadmin/ql/ql_stopapp"

#define	QL_STATE_CLEANUP_CMD	"/usr/cluster/lib/scadmin/ql/ql_cleanup"

#define	QL_STATE_FILE_CLEANUP_CMD \
	"/usr/cluster/lib/scadmin/ql/ql_cleanup -f"


#define	QL_MOUNT_CMD		"/usr/cluster/lib/scadmin/ql/ql_mount"

#define	RSH_CMD			"/usr/bin/rsh -n -l root"
#define	RCP_CMD			"/usr/bin/rcp"

#define	SSH_CMD			"/usr/bin/ssh"
#define	SSH_OPTION_1		"-o \"BatchMode yes\""
#define	SSH_OPTION_2		"-o \"StrictHostKeyChecking yes\" -n"
#define	SCP_CMD			"/usr/bin/scp -Bq"

#define	ROOT_USERNAME		"root"

#define	QL_TMP_DIR		"/var/cluster/run"

#define	QL_STOP_APP_CMD_EXEC		\
	"/var/cluster/run/ql_stopapp | /usr/bin/grep 0"

#define	HALT_CMD		"/usr/sbin/halt"

#define	PING_CMD		"/usr/sbin/ping "

#define	ENABLE_FENCE_CMD	"rm -f /scnoreservedisks"

#define	NODE_FENCE_CMD	\
	"/usr/cluster/lib/sc/run_reserve -c fence_node_ng -f "

#define	NODE_JOIN_CMD	\
	"/usr/cluster/lib/sc/run_reserve -c  node_join"

#define	VXDCTL_CMD		"/usr/sbin/vxdctl"

#define	VXDCTL_ENABLE_CMD	"/usr/sbin/vxdctl  enable"

#define	LS_CMD			"/usr/bin/ls"

#define	INIT_CMD		"/usr/sbin/init 6"

// The timeout for the ping command is set to two seconds.
#define	PING_TIMEOUT		2

#define	REDIRECT_OUTPUT		">> " QL_LOG_FILE " 2>&1"

#define	SLEEP_INTERVAL		1

#define	QL_PROCESS "QL_UPGRADE"
#define	QL_CLEANUP "QL_CLEANUP"

#define	SC_SYSLOG_QL_TAG	"Cluster.upgrade"

#define	REPLACE_BC_CMD "/var/cluster/run/replace_bootcluster"

#define	QL_BOOTCLUSTER_FILE "/usr/cluster/lib/scadmin/ql/bootcluster"

#define	QL_REPLACE_BOOTCLUSTER_FILE \
	"/usr/cluster/lib/scadmin/ql/replace_bootcluster"

#define	QL_LU_BEGIN_FILE "/usr/cluster/lib/scadmin/ql/ql_lu_begin"

#define	QL_LU_BEGIN_CMD	"/var/cluster/run/ql_lu_begin -i -B -R"

extern ql_errno_t execute_cmd_with_popen(const char *, const char *, int *);
extern char *alloc_mem(const char *, size_t);

const char *quantum_leap::ql_obj::QL_OBJ_KEY;

ql_log *logp = NULL;

//
// Flag to indicate that QL is going
// on in Live Upgrade scenario
//
static bool lu_mode = false;

//
// If the flag if set, we use RSH for remote commands, otherwise we try
// SSH/SCP first.
//
static int use_rsh = 0;

//
// Ping timeout interval.
//
int timeout_interval = PING_TIMEOUT;


// Save the locale in this variable.
static char current_locale[PATH_MAX];
static int locale_saved = 0;

//
// Save the current locale.
//
static
void
save_env()
{
	char *lc_all = NULL;
	if (!locale_saved) {
		lc_all = getenv("LC_ALL");
		if (lc_all != NULL) {
			/* Save the LC_ALL environment value */
			(void) strncpy(current_locale, lc_all, PATH_MAX);
			locale_saved = 1;
			return;
		} else {
			/* LC_ALL not set in the environment */
			current_locale[0] = NULL;
			return;
		}
	} else {
		return;
	}
}

//
// set the LC_ALL Environment value to "C".
//
static
int
set_env_posix()
{
	save_env();
	return (putenv((char *)("LC_ALL=C")));
}

//
// set the  LC_ALL environment value to the value saved in 'current_locale'
//
static
int
reset_env()
{
	char env[PATH_MAX];

	if (!locale_saved) {
		/* Nothing has been saved to reset. */
		return (0);
	}
	if (current_locale[0] == NULL) {
		(void) snprintf(env, PATH_MAX, "LC_ALL=");
	} else {
		(void) snprintf(env, PATH_MAX, "LC_ALL=%s", current_locale);
	}
	return (putenv(env));
}

//
// This function will set the remote method (rsh/ssh) to be used
// for communicating between the node
//
// Parameters:
// 	node: The target node for communication
//
// Return value:
// 	0 : Success
// 	1 : Error
//
static
int
set_remote_method(char *node)
{
	char 	*command		= NULL;
	int	retval			= 0;
	int	ret			= 0;

	//
	// Check if SSH is configured
	//

	command = alloc_mem(QL_PROCESS,
	    strlen(SSH_CMD) + 1 +
	    strlen(ROOT_USERNAME) + 1 +
	    strlen((char *)node) + 1 +
	    strlen(SSH_OPTION_1) + 1 +
	    strlen(SSH_OPTION_2) + 1 +
	    strlen(LS_CMD) + 1 +
	    strlen(REDIRECT_OUTPUT) + 1);

	(void) sprintf(command,
	    "%s %s@%s %s %s %s %s",
	    SSH_CMD,
	    ROOT_USERNAME,
	    (char *)node,
	    SSH_OPTION_1,
	    SSH_OPTION_2,
	    LS_CMD,
	    REDIRECT_OUTPUT);

	logp->log_info(QL_PROCESS, "Executing command %s\n",
	    command);

	//
	// If we are able to execute the command, means that ssh
	// is configured
	//
	ret = execute_cmd_with_popen(QL_PROCESS, command, &retval);
	if (ret == 0 && retval == 0) {
		//
		// SSH is configured. No need to test for RSH
		//
		use_rsh = 0;
		delete [] command;

		logp->log_info(QL_PROCESS,
		    "Remote method set to ssh. Node = %s\n",
		    (char *)node);

		//
		// Success
		//
		return (0);
	}

	logp->log_err(QL_PROCESS,
	    "Encountered error executing command %s on node %s\n",
	    command, (char *)node);
	delete [] command;

	//
	// Check if RSH is configured
	//

	command = alloc_mem(QL_PROCESS,
	    strlen(RSH_CMD) + 1 +
	    strlen((char *)node) + 1 +
	    strlen(LS_CMD) + 1 +
	    strlen(REDIRECT_OUTPUT) + 1);

	(void) sprintf(command,
	    "%s %s %s %s",
	    RSH_CMD,
	    (char *)node,
	    LS_CMD,
	    REDIRECT_OUTPUT);

	//
	// If we are able to execute the command, means that rsh
	// is configured
	//
	ret = execute_cmd_with_popen(QL_PROCESS, command, &retval);
	if (ret == 0 && retval == 0) {
		//
		// RSH is configured.
		//
		use_rsh = 1;
		delete [] command;

		logp->log_info(QL_PROCESS,
		    "Remote method set to rsh. Node = %s\n",
		    (char *)node);

		//
		// Success
		//
		return (0);
	}

	logp->log_err(QL_PROCESS,
	    "Encountered error executing command %s on node %s\n",
	    command, (char *)node);
	delete [] command;


	//
	// Failed to set remote method.
	//
	logp->log_err(QL_PROCESS, "Failed to set the remote method\n");

	//
	// Failure
	//
	return (1);
}

//
// This function will be used to do a remote copy. It will use either
// scp or rcp.
//
// Parameters:
//	fromfile: The file to be copied
//	tonode	: The destination node
//
// Return value:
//	0 : Success
//	1 : Error
//
static
int
remote_copy(const char *fromfile, const char *tonode)
{

	char *cmd;
	int retval = 0;

	//
	// Build the remote command.
	//
	if (use_rsh) {
		//
		// Use rcp
		//
		cmd = alloc_mem(QL_PROCESS,
		    strlen(RCP_CMD) + 1 +
		    strlen(fromfile) + 1 +
		    strlen(tonode) + 1 +
		    strlen(QL_TMP_DIR) + 1 +
		    strlen(REDIRECT_OUTPUT) + 1);

		(void) sprintf(cmd,
		    "%s %s %s:%s %s",
		    RCP_CMD,
		    fromfile,
		    tonode,
		    QL_TMP_DIR,
		    REDIRECT_OUTPUT);

	} else {
		//
		// Use scp
		//
		cmd = alloc_mem(QL_PROCESS,
		    strlen(SCP_CMD) + 1 +
		    strlen(fromfile) + 1 +
		    strlen(ROOT_USERNAME) + 1 +
		    strlen(tonode) + 1 +
		    strlen(QL_TMP_DIR) + 1 +
		    strlen(REDIRECT_OUTPUT) + 1);

		(void) sprintf(cmd,
		    "%s %s %s@%s:%s %s",
		    SCP_CMD,
		    fromfile,
		    ROOT_USERNAME,
		    tonode,
		    QL_TMP_DIR,
		    REDIRECT_OUTPUT);
	}

	//
	// Execute the command.
	//
	logp->log_info(QL_PROCESS, "Executing command %s\n", cmd);

	//
	// If we are unable to execute the command, then we log a message.
	//
	if (execute_cmd_with_popen(QL_PROCESS, cmd, &retval) != 0) {
		logp->log_err(QL_PROCESS,
		    "Encountered error executing command %s for node %s\n",
		    cmd, tonode);
		delete [] cmd;
		return (1);
	}

	//
	// If we are unable to copy the file, log a message.
	//
	if (retval != 0) {
		logp->log_err(QL_PROCESS, "Error occurred while copying "
		    "%s on node %s. Return Value = %d\n",
		    fromfile, tonode, retval);
		delete [] cmd;
		return (1);
	}


	delete [] cmd;

	//
	// Success.
	//
	return (0);
}

//
// This function will read the mount point for the
// alternate boot environment from the QL_LU_CONTINUE file
//
// Return value:
//	0 : Success
//	1 : Error
//
static
int
read_mount_point()
{
	int fd;
	int retval 	= 0;
	size_t count	= 0;

	fd = open(QL_LU_CONTINUE, O_RDONLY);
	if (fd < 0) {
		logp->log_err(QL_PROCESS, "Failed to open file %s\n",
		    QL_LU_CONTINUE);
		return (1);
	}

	//
	// Read the first line from the file
	//
	retval = read(fd, mount_point, BUFFER_SIZE);
	if (retval == -1) {
		logp->log_err(QL_PROCESS, "Failed to read from file %s\n",
		    QL_LU_CONTINUE);
		(void) close(fd);
		return (1);
	}

	(void) close(fd);

	//
	// Extract the mount point, which is the first token
	//
	for (count = 0; count < strlen(mount_point); count++) {
		if (mount_point[count] == ' ' || mount_point[count] == '\t') {
			break;
		}
	}
	mount_point[count] = '\0';

	//
	// Check that the mount point is not empty
	//
	if (strlen(mount_point) == 0) {
		logp->log_err(QL_PROCESS, "Mount point cannot be empty\n");
		return (1);
	}

	logp->log_info(QL_PROCESS, "Read mount point %s\n", mount_point);

	//
	// Success
	//
	return (0);
}


//
// This is executed on each node of partition A during quantum-leap
// upgrade. The following tasks are executed.
//	1. Mount all shared QFS filesystems.
//	2. Enable Fencing.
//	3. Fence off all nodes in partition B.
// Return value:
//	None.
//
static
void
execute_common_upgrade_tasks(namelist_t *partition_b_list)
{
	char		*command	= NULL;
	namelist_t	*listp		= NULL;
	int		retval		= 0;

	//
	// We did not mount shared filesystems during boot up. We do that
	// now on this node.
	// We had earlier prevented fencing of nodes in partition B. It
	// is now safe to enable fencing here. If for some reason we fail
	// to do this, it has to be done manually later on.
	//
	logp->log_info(QL_PROCESS, "Executing %s\n", QL_MOUNT_CMD);

	//
	// Mount shared QFS filesystems.
	//
	command = alloc_mem(QL_PROCESS, strlen(QL_MOUNT_CMD) + 1 +
	    strlen(REDIRECT_OUTPUT) + 1);

	(void) sprintf(command, "%s %s", QL_MOUNT_CMD, REDIRECT_OUTPUT);

	if (execute_cmd_with_popen(QL_PROCESS, command, &retval) != 0) {
		logp->log_err(QL_PROCESS, "Could not execute the cmd %s\n",
		    command);
	} else if (retval != 0) {
		logp->log_err(QL_PROCESS, "Error while executing the cmd %s\n",
		    command);
	}
	delete [] command;

	//
	// Enable fencing by removing the "scnoreservedisks" file.
	//
	command = alloc_mem(QL_PROCESS, strlen(ENABLE_FENCE_CMD) + 1 +
	    strlen(REDIRECT_OUTPUT) + 1);

	(void) sprintf(command, "%s %s", ENABLE_FENCE_CMD, REDIRECT_OUTPUT);

	logp->log_info(QL_PROCESS, "Executing %s\n", command);

	if (execute_cmd_with_popen(QL_PROCESS, command, &retval) != 0) {
		logp->log_err(QL_PROCESS, "Could not execute the cmd %s\n",
		    command);
	}

	delete [] command;

	//
	// We have to execute run_reserve -c node_join on this node
	//
	command = alloc_mem(QL_PROCESS, strlen(NODE_JOIN_CMD) + 1 +
	    strlen(REDIRECT_OUTPUT) + 1);

	(void) sprintf(command, "%s %s", NODE_JOIN_CMD, REDIRECT_OUTPUT);

	logp->log_info(QL_PROCESS, "Executing %s\n", command);

	if (execute_cmd_with_popen(QL_PROCESS, command, &retval) != 0) {
		logp->log_err(QL_PROCESS, "Could not execute the cmd %s\n",
		    command);
	} else if (retval != 0) {
		logp->log_err(QL_PROCESS, "Error while executing the cmd %s\n",
		    command);
	}
	delete [] command;

	//
	// We have to fence off all the nodes in partition B. We invoke
	// run_reserve to do this for us.
	//
	listp = partition_b_list;

	while (listp) {
		command = alloc_mem(QL_PROCESS, strlen(NODE_FENCE_CMD) + 1 +
		    CL_MAX_LEN + 1 + strlen(REDIRECT_OUTPUT) + 1);

		(void) sprintf(command, "%s %s %s", NODE_FENCE_CMD, listp->name,
		    REDIRECT_OUTPUT);

		logp->log_info(QL_PROCESS, "Executing %s\n", command);

		if (execute_cmd_with_popen(QL_PROCESS, command, &retval) != 0) {
			logp->log_err(QL_PROCESS,
			    "Could not execute the cmd %s\n", command);
		} else if (retval != 0) {
			logp->log_err(QL_PROCESS,
			    "Error while executing the cmd %s\n", command);
		}
		delete [] command;
		listp = listp->next;
	}
	logp->log_info(QL_PROCESS, "Completed fencing activity\n");

	//
	// We had also skipped running "vxdctl enable" on the node.
	// We do this now.
	//
	if (access(VXDCTL_CMD, X_OK) == 0) {

		command = alloc_mem(QL_PROCESS, strlen(VXDCTL_ENABLE_CMD) + 1 +
		    strlen(REDIRECT_OUTPUT) + 1);

		(void) sprintf(command, "%s %s", VXDCTL_ENABLE_CMD,
		    REDIRECT_OUTPUT);

		logp->log_info(QL_PROCESS, "Executing %s\n", command);

		if (execute_cmd_with_popen(QL_PROCESS, command, &retval) != 0) {
			logp->log_err(QL_PROCESS,
			    "Could not execute the cmd %s\n", command);
		} else if (retval != 0) {
			logp->log_err(QL_PROCESS,
			    "Error while executing the cmd %s\n", command);
		}
		delete [] command;
	}

	logp->log_info(QL_PROCESS, "Completed execution of upgrade tasks "
	    "local to this node\n");
}

//
// This function will wait for all nodes of partition A nodes
// to come online.
//
// Return value:
// 	Success:
// 		QL_NOERR
//	Failure:
//		QL_ECLCONF

static
ql_errno_t
wait_for_partition_a_nodes(namelist_t *partition_a_listp)
{
	bool			online_flag		= false;
	int			retval			= 0;
	cmm::control_var	ctrl_v;
	cmm::cluster_state_t	new_state;
	Environment		env;
	CORBA::Exception	*ex;
	nodeset			partition_a_nodeset;
	namelist_t		*listp;
	nodeid_t		nodeid;
	unsigned int		node;

	listp = partition_a_listp;

	//
	// Initialize ORB. If we are not able to initialize the ORB here
	// we cant proceed further.
	//
	if ((retval = clconf_lib_init()) != 0) {
		logp->log_err(QL_PROCESS,
		    "Failed to initialize clconf library, Returned error "
		    "= %d\n", retval);
		return (QL_ECLCONF);
	}

	if (clconf_cluster_get_current() == NULL) {
		logp->log_err(QL_PROCESS,
		    "Error in clconf_cluster_get_current()\n");
		return (QL_ECLCONF);
	}

	ctrl_v = cmm_ns::get_control();

	if (CORBA::is_nil(ctrl_v)) {
		logp->log_err(QL_PROCESS, "Fatal error, get_control() "
		    "failure.\n");
		return (QL_ECLCONF);
	}

	//
	// Build the nodeset of partition B nodes
	//
	while (listp) {
		nodeid = clconf_cluster_get_nodeid_by_nodename(listp->name);
		if (nodeid == NODEID_UNKNOWN) {
			logp->log_err(QL_PROCESS, "Unable to map node name "
			    " to node ID for node %s\n", listp->name);
			return (QL_ECLCONF);
		}
		partition_a_nodeset.add_node(nodeid);
		listp = listp->next;
	}

	//
	// Check that all the partition A nodes are online
	// This function will wait forever for all the partition A
	// nodes to come online.
	//
	while (true) {

		online_flag = true;

		//
		// Get the Cluster membership.
		//
		ctrl_v->get_cluster_state(new_state, env);

		if ((ex = env.exception()) != NULL) {
			logp->log_err(QL_PROCESS, "Fatal error, "
			    "get_cluster_state exception, id = %d\n",
			    ex->exception_enum());
			return (QL_ECLCONF);
		}


		for (node = 1; node <= NODEID_MAX; node++) {
			if (!partition_a_nodeset.contains(node)) {
				continue;
			}

			if (new_state.members.members[node] == INCN_UNKNOWN) {
				//
				// This node is not online yet
				//
				online_flag = false;
				break;
			}
		}

		if (online_flag) {
			//
			// All partition A nodes are online
			//
			break;
		}
	}

	//
	// Success
	//
	return (QL_NOERR);
}

//
// This function is invoked from the ql_upgrade process running on a node
// that is not the root node of partition A. The following tasks are done:
// 1. Wait till lookup of the quantum-leap object succeeds.
// 2. Block on an invocation of the quantum-leap object.
// 3. Execute the common upgrade tasks on this node.
// Return value:
// 	QL_NOERR Success.
//	Failure:
//	QL_ELOOKUP
//	QL_ESPURIOUSOBJ
//	QL_EINVO
//	QL_ECCR
//
static
ql_errno_t
wait_for_ql_process(ql_cl_ccr* ql_ccr_objp)
{
	Environment			env;
	CORBA::Exception 		*ex;
	quantum_leap::ql_obj_var	qlobj_v;
	CORBA::Object_var		obj_v;
	namelist_t			*partition_b_list	= NULL;
	naming::naming_context_var	ctx_v = ns::root_nameserver();

	ASSERT(!CORBA::is_nil(ctx_v));

	while (true) {
		//
		// Lookup the QL object in the name server
		//
		logp->log_info(QL_PROCESS, "Check for QL Object, We block "
		    "here till ql_object is registered\n");

		obj_v = ctx_v->resolve(quantum_leap::ql_obj::QL_OBJ_KEY, env);

		if ((ex = env.exception()) != NULL) {
			if (naming::not_found::_exnarrow(ex) != NULL) {
				//
				// The QL object is not registered yet.
				//
				(void) sleep((uint_t)SLEEP_INTERVAL);
				env.clear();
			} else {
				logp->log_err(QL_PROCESS, "Exception while "
				    "resolving QL object, id = %d\n",
				    ex->exception_enum());
				env.clear();
				return (QL_ELOOKUP);
			}
		} else {
			break;
		}
	}

	//
	// Narrow the object to a quantum leap object
	//
	qlobj_v = quantum_leap::ql_obj::_narrow(obj_v);

	if (CORBA::is_nil(qlobj_v)) {
		//
		// The object is not of the type "ql"
		//
		logp->log_err(QL_PROCESS, "spurious ql object\n");
		env.clear();
		return (QL_ESPURIOUSOBJ);
	}

	logp->log_info(QL_PROCESS, "QL object found\n");

	logp->log_info(QL_PROCESS, "Invoking block_ql\n");

	qlobj_v->block_ql(env);

	if ((ex = env.exception()) != NULL) {

		logp->log_err(QL_PROCESS, "unblocked, encountered an exception "
		    ", id = \n", ex->exception_enum());
		env.clear();
		return (QL_EINVO);
	}

	logp->log_info(QL_PROCESS, "Returning from block_ql\n");

	if (ql_ccr_objp->ql_read_ccr_partition(QL_PARTITION_B_PREFIX,
	    &partition_b_list) != 0) {

		logp->log_err(QL_PROCESS,
		    "Could not read the nodes in second partition "
		    "from the CCR\n");

		return (QL_ECCR);
	}

	//
	// Execute the common tasks that needs to be done on each node
	// during quantum-leap upgrade.
	//
	execute_common_upgrade_tasks(partition_b_list);
	return (QL_NOERR);
}

//
// This function is executed by the child of the quantum-leap process.
// We wait here till a majority of nodes join the cluster and we then reset
// the quorum votes and cleanup the CCR of quantum-leap entries.
//
static
void
wait_and_cleanup()
{
	int			retval			= 0;
	int			node_count		= 0;
	int			online_nodes		= 1;
	ql_cl_ccr		*ql_ccr_objp		= NULL;
	char			*command		= NULL;
	clconf_cluster_t	*clconf			= NULL;
	clconf_iter_t		*currnodesi		= NULL;
	const char		*nodename		= NULL;
	char			*node_list		= NULL;
	nodeid_t		local_nodeid;
	CORBA::Exception	*ex;
	cmm::control_var	ctrl_v;
	cmm::cluster_state_t	new_state;
	cmm::cluster_state_t	old_state;
	Environment		env;
	nodeid_t		node;
	sc_syslog_msg_handle_t	handle;

	//
	// Initialize ORB. If we are not able to initialize the ORB here
	// we cant proceed further.
	//
	if ((retval = clconf_lib_init()) != 0) {
		logp->log_err(QL_CLEANUP,
		    "Failed to initialize clconf library, Returned error "
		    "= %d\n");
		return;
	}

	if ((clconf = clconf_cluster_get_current()) == NULL) {
		logp->log_err(QL_CLEANUP,
		    "Error in clconf_cluster_get_current()\n");
		return;
	}

	currnodesi = clconf_cluster_get_nodes(clconf);

	node_count = clconf_iter_get_count(currnodesi);

	for (node = 1; node < NODEID_MAX; node++) {
		old_state.members.members[node] = INCN_UNKNOWN;
	}

	logp->log_info(QL_CLEANUP, "Wait until a majority of nodes are up..");

	ql_ccr_objp = new ql_cl_ccr();

	if (ql_ccr_objp == NULL) {
		logp->log_err(QL_CLEANUP, "Failed to allocate CCR object, "
		    "cleanup did not succeed\n");
		return;
	}

	//
	// We need to make sure that the nodes in partition B are up before
	// proceeding further.
	//
	local_nodeid = orb_conf::node_number();

	ctrl_v = cmm_ns::get_control();

	if (CORBA::is_nil(ctrl_v)) {
		logp->log_info(QL_CLEANUP, "Fatal error, get_control() "
		    "failure.\n");
		return;
	}

	node_list = alloc_mem(QL_CLEANUP, NODEID_MAX * CL_MAX_LEN + 1);
	nodename = clconf_cluster_get_nodename_by_nodeid(clconf, local_nodeid);
	ASSERT(nodename != NULL);
	(void) strcpy(node_list, nodename);

	while (true) {

		//
		// Get the Cluster membership.
		//
		ctrl_v->get_cluster_state(new_state, env);

		if ((ex = env.exception()) != NULL) {
			logp->log_info(QL_CLEANUP, "Fatal error, "
			    "get_cluster_state exception, id = %d\n",
			    ex->exception_enum());
			return;
		}

		for (node = 1; node < NODEID_MAX; node++) {
			//
			// Skip the local node.
			//
			if (node == local_nodeid) {
				continue;
			}

			if ((nodename =
			    clconf_cluster_get_nodename_by_nodeid(clconf,
			    node)) == NULL) {
				// Not a valid node id.
				continue;
			}

			//
			// Node has come up and we did not know about it
			// earlier.
			//
			if ((old_state.members.members[node] ==
			    INCN_UNKNOWN) && (new_state.members.members[node] !=
			    INCN_UNKNOWN)) {
				old_state.members.members[node] =
				    new_state.members.members[node];
				//
				// Store the list of node names that have joined
				// the cluster.
				//
				(void) strcat(node_list, ",");
				(void) strcat(node_list,
				    clconf_cluster_get_nodename_by_nodeid(
				    clconf, node));
				online_nodes++;
				logp->log_info(QL_PROCESS,
				    "Node = %d, Incn = %d\n", node,
				    old_state.members.members[node]);
			}
		}
		//
		// Check if a majority of the nodes configured in the
		// cluster have joined the cluster. If so, we have to check
		// if the upgrade process on the root node of partition A
		// has proceeded to change the upgrade state to
		// QL_UPGRADE_STATE_2. Only then we go ahead and cleanup.
		//
		if (online_nodes >= ((node_count/2) + 1)) {

			while (!ql_ccr_objp->ql_upgrade_check_state(
			    QL_UPGRADE_STATE_2)) {
				logp->log_info(QL_CLEANUP, "Waiting for "
				    "QL_UPGRADE_STATE_2 state\n");
				(void) sleep((uint_t)SLEEP_INTERVAL * 3);
			}
			logp->log_info(QL_PROCESS, "Majority of nodes "
			    "have joined the cluster. Total number of "
			    "nodes in the cluster = %d, number "
			    "of nodes that became online = %d\n", node_count,
			    online_nodes);
			break;
		}
		// Sleep and retry.
		(void) sleep((uint_t)SLEEP_INTERVAL * 3);
	}
	//
	// If some nodes join later on, they need to cleanup state
	// files. We flag this in the CCR.
	//
	if (online_nodes != node_count) {


		for (node = 1; node < NODEID_MAX; node++) {

			if (node == local_nodeid) {
				continue;
			}

			if ((nodename =
			    clconf_cluster_get_nodename_by_nodeid(clconf,
			    node)) == NULL) {
				// Not a valid node id.
				continue;
			}

			//
			// If the node is configured as part of the cluster
			// but not yet joined the cluster, add the flag to
			// the CCR.
			//
			if (old_state.members.members[node] == INCN_UNKNOWN) {

				retval =
				    ql_ccr_objp->mark_need_cleanup(nodename);

				if (retval != QL_NOERR) {
					logp->log_info(QL_PROCESS,
					    "Failed to mark node %s for "
					    "cleanup\n", nodename);
				}
			}
		}
	}

	logp->log_info(QL_PROCESS, "List of online nodes = %s\n", node_list);

	//
	// Restore the quorum votes and cleanup the CCR of quantum-leap entries.
	// We do this by executing a script.
	//
	command = alloc_mem(QL_PROCESS, strlen(QL_STATE_CLEANUP_CMD) + 1 +
	    (unsigned int)(online_nodes * CL_MAX_LEN) +
	    strlen(REDIRECT_OUTPUT) + 1);

	(void) sprintf(command, "%s -n %s %s", QL_STATE_CLEANUP_CMD,
	    node_list, REDIRECT_OUTPUT);

	logp->log_info(QL_PROCESS, "Executing command %s\n", command);

	if (execute_cmd_with_popen(QL_PROCESS, command, &retval) != 0) {

		logp->log_err(QL_PROCESS,
		    "Encountered error initiating command %s\n", command);
	}

	(void) sc_syslog_msg_initialize(&handle, SC_SYSLOG_QL_TAG, "");

	//
	// If the cleanup could not complete successfully, log an error
	// message. This has to be done manually later on.
	//
	if (retval) {
		logp->log_err(QL_PROCESS, "Encountered error executing"
		    " command %s\n", command);

		//
		// SCMSGS
		// @explanation
		// Cluster upgrade has completed on all the nodes that have
		// joined the cluster. However cleanup of the upgrade state
		// has failed.
		// @user_action
		// Retry cleanup of the upgrade state by running
		// /usr/cluster/lib/scadmin/ql/ql_cleanup on any node of the
		// cluster.
		//
		(void) sc_syslog_msg_log(handle, LOG_NOTICE, MESSAGE,
		    "Cleanup after upgrade has failed on the nodes that "
		    "have the joined the cluster");
	} else {
		//
		// We are done. No need to release memory here since we will
		// exit soon.
		//
		logp->log_info(QL_PROCESS,
		    "Upgrade has completed on all the nodes that have joined "
		    "the cluster\n");

		//
		// SCMSGS
		// @explanation
		// Cluster upgrade has completed on all the nodes that have
		// joined the cluster. Software upgrade might not be complete
		// on any nodes that have not rejoined the cluster.
		// @user_action
		// This is an informational message, no user action is needed.
		//
		(void) sc_syslog_msg_log(handle, LOG_NOTICE, MESSAGE,
		    "Upgrade has completed on all the nodes that have joined "
		    "the cluster");
	}
	sc_syslog_msg_done(&handle);
	logp->close();
}

//
// Execute STOP_APP command on the root node of partition B. We
// try to use secure shell commands here.
// Return Value:
//	QL_NOERR on Success.
// Failure:
//	QL_EEXEC
//
static
int
execute_stopapp_cmd_use_ssh(char *node)
{
	char	*command	= NULL;
	int	retval		= 0;

	// SCP and SSH
	command = alloc_mem(QL_PROCESS, strlen("scp -Bq") + 1 +
	    strlen(QL_UTIL_FILE) + 1 + strlen("root@") + CL_MAX_LEN + 1 +
	    strlen(":") + strlen(QL_TMP_DIR) + 1 + strlen(REDIRECT_OUTPUT) + 1);

	(void) sprintf(command, "scp -Bq %s root@%s:%s %s", QL_UTIL_FILE,
	    node, QL_TMP_DIR, REDIRECT_OUTPUT);

	logp->log_info(QL_PROCESS, "Executing command %s\n", command);

	if (execute_cmd_with_popen(QL_PROCESS, command, &retval) != 0) {
		logp->log_err(QL_PROCESS, "Encountered error "
		    "executing command %s\n", command);
		delete [] command;
		return (QL_EEXEC);
	}

	delete [] command;

	command = alloc_mem(QL_PROCESS, strlen("scp -Bq") + 1 +
	    strlen(QL_STOP_APP_CMD) + 1 + strlen("root@") + CL_MAX_LEN + 1 +
	    strlen(":") + strlen(QL_TMP_DIR) + 1 + strlen(REDIRECT_OUTPUT) + 1);

	(void) sprintf(command, "scp -Bq %s root@%s:%s %s", QL_STOP_APP_CMD,
	    node, QL_TMP_DIR, REDIRECT_OUTPUT);

	logp->log_info(QL_PROCESS, "Executing command %s\n", command);

	if (execute_cmd_with_popen(QL_PROCESS, command, &retval) != 0) {
		logp->log_err(QL_PROCESS, "Encountered error "
		    "executing command %s\n", command);
		delete [] command;
		return (QL_EEXEC);
	}
	//
	// Return error if the command fails.
	//
	if (retval != 0) {
		logp->log_err(QL_PROCESS, "%s returned error =  %d\n",
		    command, retval);
		return (QL_EEXEC);
	}
	delete [] command;

	command = alloc_mem(QL_PROCESS, strlen("ssh root@") + CL_MAX_LEN + 1
	    + strlen("-o \"BatchMode yes\"") + 1 +
	    strlen("-o \"StrictHostKeyChecking yes\" -n") + 1 +
	    strlen(QL_STOP_APP_CMD_EXEC) + 1 + strlen(REDIRECT_OUTPUT) + 1);

	(void) sprintf(command, "ssh root@%s -o \"BatchMode yes\" -o "
	    "\"StrictHostKeyChecking yes\" -n %s %s", node,
	    QL_STOP_APP_CMD_EXEC, REDIRECT_OUTPUT);

	logp->log_info(QL_PROCESS, "Executing command %s\n", command);

	if (execute_cmd_with_popen(QL_PROCESS, command, &retval) != 0) {
		(void) reset_env();
		logp->log_err(QL_PROCESS, "Encountered error "
		    "executing command %s\n", command);
		delete [] command;
		return (QL_EEXEC);
	}

	//
	// Return error if the command fails.
	//
	if (retval != 0) {
		logp->log_err(QL_PROCESS, "%s returned %d\n", command, retval);
		return (QL_EEXEC);
	}
	delete [] command;
	return (QL_NOERR);
}

//
// Execute STOP_APP command on the root node of partition B. We
// try to use remote shell commands here.
// Return Value:
//	QL_NOERR on Success.
// Failure:
//	QL_EEXEC
//
static
int
execute_stopapp_cmd_use_rsh(char *node)
{
	char	*command	= NULL;
	int	retval		= 0;

	// RCP & RSH

	command = alloc_mem(QL_PROCESS, strlen("rcp") + 1 +
	    strlen(QL_UTIL_FILE) + 1 + CL_MAX_LEN + 1 +
	    strlen(QL_TMP_DIR) + 1 + strlen(REDIRECT_OUTPUT) + 1);

	//
	// The STOP_APP command wont exist on the remote node. The QL_UTIL
	// file is needed by the STOP_APP command.
	//
	(void) sprintf(command, "%s %s %s:%s %s", "rcp", QL_UTIL_FILE,
	    node, QL_TMP_DIR, REDIRECT_OUTPUT);

	logp->log_info(QL_PROCESS, "Executing command %s\n", command);

	if (execute_cmd_with_popen(QL_PROCESS, command, &retval) != 0) {
		(void) reset_env();
		logp->log_err(QL_PROCESS, "Encountered error "
		    "executing command %s\n", command);
		delete [] command;
		return (QL_EEXEC);
	}
	delete [] command;

	command = alloc_mem(QL_PROCESS, strlen("rcp") + 1 +
	    strlen(QL_STOP_APP_CMD) + 1 + CL_MAX_LEN + 1 +
	    strlen(QL_TMP_DIR) + 1 + strlen(REDIRECT_OUTPUT) + 1);

	//
	// The STOP_APP command wont exist on the remote node. This has to
	// be remotely copied to the node before remotely executing the
	// command.
	//
	(void) sprintf(command, "%s %s %s:%s %s", "rcp", QL_STOP_APP_CMD,
	    node, QL_TMP_DIR, REDIRECT_OUTPUT);

	logp->log_info(QL_PROCESS, "Executing command %s\n", command);

	if (execute_cmd_with_popen(QL_PROCESS, command, &retval) != 0) {
		(void) reset_env();
		logp->log_err(QL_PROCESS, "Encountered error "
		    "executing command %s\n", command);
		delete [] command;
		return (QL_EEXEC);
	}
	delete [] command;

	command = alloc_mem(QL_PROCESS, strlen(RSH_CMD) + 1 + CL_MAX_LEN + 1 +
	    strlen(QL_STOP_APP_CMD_EXEC) + 1 + strlen(REDIRECT_OUTPUT) + 1);

	(void) sprintf(command, "%s %s %s %s", RSH_CMD, node,
	    QL_STOP_APP_CMD_EXEC, REDIRECT_OUTPUT);

	logp->log_info(QL_PROCESS, "Executing command %s\n", command);

	if (set_env_posix() != 0) {
		// Could not set the environment.
		logp->log_err(QL_PROCESS, "Could not set the environment\n");
	}

	if (execute_cmd_with_popen(QL_PROCESS, command, &retval) != 0) {
		(void) reset_env();
		logp->log_err(QL_PROCESS, "Encountered error "
		    "executing command %s\n", command);
		delete [] command;
		return (QL_EEXEC);
	}
	delete [] command;
	(void) reset_env();
	//
	// Return error if the command fails.
	//
	if (retval != 0) {
		logp->log_err(QL_PROCESS, "%s returned %d\n",
		    QL_STOP_APP_CMD_EXEC);
		return (QL_EEXEC);
	}
	return (QL_NOERR);
}

//
// Executes the STOP_APP command on the root node in partition B.
// We try to execute the command using SCP and SSH, if SSH is not
// configured we try executing it using RCP and RSH.
// Return value:
//	QL_NOERR on Success.
//	Failure:
//		QL_EEXEC
//
static
int
execute_stopapp_cmd(char *node)
{
	int retval = QL_NOERR;

	if (use_rsh) {
		retval = execute_stopapp_cmd_use_rsh(node);
	} else {
		retval = execute_stopapp_cmd_use_ssh(node);
	}

	if (retval != QL_NOERR) {

		logp->log_info(QL_PROCESS, "Encountered error "
		    "executing command %s on node %s . use_rsh = %d\n",
		    QL_STOP_APP_CMD, node, use_rsh);
	}

	return (retval);
}

//
// Common function to execute a command in parallel on a list
// of nodes in the cluster.
//
static
ql_errno_t
execute_cmd(namelist_t *partition_listp, void * (func)(void *))
{
	namelist_t	*listp		=	NULL;
	nodeid_t	node_cnt	=	0;
	int		err;
	thread_t	tid[NODEID_MAX];
	ql_errno_t	status;

	listp = partition_listp;

	(void) memset(tid, 0, sizeof (tid));
	while (listp) {
		if ((err = pthread_create(&tid[node_cnt], NULL,
		    func, (void *)listp->name)) != 0) {
			logp->log_info(QL_PROCESS,
			    "pthread_create: Returned error %d\n", err);
			return (QL_ETHREAD);
		}
		node_cnt++;
		listp = listp->next;
	}

	//
	// Wait for all the threads to complete. Their succesfull completion
	// means that all the nodes have executed the command.
	//
	for (nodeid_t i = 0; i < node_cnt; ++i) {
		if ((err = pthread_join(tid[i], (void **) &status)) != 0) {
			logp->log_info(QL_PROCESS,
			    "pthread_join: Returned error %d\n", err);
			return (QL_ETHREAD);
		}
		if (status != QL_NOERR) {
			return (status);
		}
	}
	return (QL_NOERR);
}

//
// Check and wait till node is down.
//
static
ql_errno_t
check_node_down(nodename_t node)
{
	char	*command	= NULL;
	int	retval		= 0;

	command = alloc_mem(QL_PROCESS, strlen(PING_CMD) + 1 + CL_MAX_LEN +
	    4 + strlen(REDIRECT_OUTPUT) + 1);

	ASSERT(node != NULL);

	while (true) {
		(void) sprintf(command, "%s %s %d %s", PING_CMD,
		    (char *)node, timeout_interval, REDIRECT_OUTPUT);

		if (execute_cmd_with_popen(QL_PROCESS, command, &retval) != 0) {

			logp->log_err(QL_PROCESS,
			    "Encountered error executing command"
			    "  %s\n", PING_CMD);
			pthread_exit((void *)QL_EEXEC);
		}
		if (retval) {
			break;
		} else {
			(void) sleep((uint_t)SLEEP_INTERVAL);
		}
	}
	delete [] command;
	return (QL_NOERR);
}

//
// Execute init 6 on a node of partition B
// so that the node boots up in new boot environment.
//
// Return Value:
//	QL_NOERR on Success.
//	QL_EEXEC on Failure.
//
static
void *
execute_node_init(void *partition_b_node)
{
	char		*command = NULL;
	int		retval;

	if (use_rsh) {
		command = alloc_mem(QL_PROCESS,
		    strlen(RSH_CMD) + 1 +
		    CL_MAX_LEN + 1 +
		    strlen(INIT_CMD) + 1 +
		    strlen(REDIRECT_OUTPUT) + 1);

		(void) sprintf(command,
		    "%s %s %s %s",
		    RSH_CMD,
		    (char *)partition_b_node,
		    INIT_CMD,
		    REDIRECT_OUTPUT);
	} else {

		command = alloc_mem(QL_PROCESS,
		    strlen(SSH_CMD) + 1 +
		    strlen(ROOT_USERNAME) + 1 +
		    strlen((char *)partition_b_node) + 1 +
		    strlen(SSH_OPTION_1) + 1 +
		    strlen(SSH_OPTION_2) + 1 +
		    strlen(INIT_CMD) + 1 +
		    strlen(REDIRECT_OUTPUT) + 1);

		(void) sprintf(command,
		    "%s %s@%s %s %s %s %s",
		    SSH_CMD,
		    ROOT_USERNAME,
		    (char *)partition_b_node,
		    SSH_OPTION_1,
		    SSH_OPTION_2,
		    INIT_CMD,
		    REDIRECT_OUTPUT);
	}

	logp->log_info(QL_PROCESS, "Executing command %s\n", command);

	//
	// If we are not able to execute init, we log a message
	// and let the human admin reboot the nodes using init 6.
	//
	if (execute_cmd_with_popen(QL_PROCESS, command, &retval) != 0) {
		logp->log_err(QL_PROCESS,
		    "Encountered error executing command %s\n",
		    command);
		return ((void *)QL_EEXEC);
	}

	//
	// If init returns error for some strange reason and is not able
	// to restart the node, we block waiting for the nodes to go down.
	// This will now require manual intervention.
	//
	if (retval != QL_NOERR) {
		logp->log_err(QL_PROCESS, "init returned %d on node %s\n",
		    retval, (char *)partition_b_node);
		return ((void *)QL_EEXEC);
	}


	logp->log_info(QL_PROCESS, "Node %s is being rebooted in the "
	    "new boot environment\n",
	    (char *)partition_b_node);

	//
	// Ping the node to make sure that it was rebooted.
	// This approach has a limitation.
	// Since the second partition nodes are rebooted (init 6),
	// it could so happen that second partition nodes goes down
	// and comes up very very fast (within 1 sec). In this case
	// ping will always see the nodes up. Since its very unlikely
	// with the current h/w that the nodes will reboot in 1 sec
	// we are safe.
	//
	retval = check_node_down((nodename_t)partition_b_node);

	if (retval  != QL_NOERR) {
		logp->log_err(QL_PROCESS, "ping returned %d on node %s\n",
		    retval, (char *)partition_b_node);
		return ((void *)QL_EEXEC);
	}

	logp->log_info(QL_PROCESS, "Node %s has been rebooted\n",
	    (char *)partition_b_node);

	delete [] command;
	return ((void *)QL_NOERR);
}

//
// Reboot all the nodes of partition B in alternate root.
// We use secure shell (SSH) or RSH to do this.
//
static
ql_errno_t
execute_partition_init(namelist_t *partition_b_listp)
{

	ql_errno_t	retval;
	sc_syslog_msg_handle_t		handle;

	retval = execute_cmd(partition_b_listp, execute_node_init);

	if (retval != QL_NOERR) {
		//
		// Execution of halt failed on a node. log an error.
		//
		logp->log_info(QL_PROCESS,
		    "Executing init on a node returned error %d\n", retval);
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_QL_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The upgrade process will reboot the nodes of
		// second partition
		// in the alternate boot environment. Rebooting the
		// nodes failed.
		// @user_action
		// Cluster upgrade has failed. Reboot all the nodes out of
		// cluster mode and recover from upgrade. Finish the cluster
		// upgrade by using the standard upgrade method.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Failed to reboot the nodes of the partition");
		sc_syslog_msg_done(&handle);
	}
	logp->log_info(QL_PROCESS, "init 6 command has been executed "
	    "on all the nodes of second partition\n");
	return (retval);
}

//
// Execute shutdown on a node of partition B.
// Return Value:
//	QL_NOERR on Success.
//	QL_EEXEC on Failure.
//
static
void *
execute_node_shutdown(void *partition_b_node)
{
	char		*command = NULL;
	int		retval;

	if (use_rsh) {
		command = alloc_mem(QL_PROCESS, strlen(RSH_CMD) + 1 +
		    CL_MAX_LEN + 1 + strlen(HALT_CMD) + 1 +
		    strlen(REDIRECT_OUTPUT) + 1);

		(void) sprintf(command, "%s %s %s %s", RSH_CMD,
		    (char *)partition_b_node, HALT_CMD, REDIRECT_OUTPUT);
	} else {
		command = alloc_mem(QL_PROCESS, strlen("ssh root@") +
		    CL_MAX_LEN + 1 + strlen("-o \"BatchMode yes\"") + 1 +
		    strlen("-o \"StrictHostKeyChecking yes\" -n") + 1 +
		    strlen(HALT_CMD) + 1 + strlen(REDIRECT_OUTPUT) + 1);

		(void) sprintf(command, "ssh root@%s -o \"BatchMode yes\" -o "
		    " \"StrictHostKeyChecking yes\" -n %s %s",
		    (char *)partition_b_node, HALT_CMD, REDIRECT_OUTPUT);
	}

	logp->log_info(QL_PROCESS, "Executing command %s\n", command);

	//
	// If we are not able to execute halt, we log a message
	// and let the human admin bring down the nodes for us.
	//
	if (execute_cmd_with_popen(QL_PROCESS, command, &retval) != 0) {
		logp->log_err(QL_PROCESS,
		    "Encountered error executing command %s\n",
		    command);
		return ((void *)QL_EEXEC);
	}

	//
	// If halt returns error for some strange reason and is not able
	// to shutdown the node, we block waiting for the nodes to go down.
	// This will now require manual intervention.
	//
	if (retval != QL_NOERR) {
		logp->log_err(QL_PROCESS, "halt returned %d on node %s\n",
		    retval, (char *)partition_b_node);
	}

	//
	// We ping the node and make sure that it is down.
	//
	retval = check_node_down((nodename_t)partition_b_node);

	if (retval  != QL_NOERR) {
		logp->log_err(QL_PROCESS, "ping returned %d on node %s\n",
		    retval, (char *)partition_b_node);
		return ((void *)QL_EEXEC);
	}

	logp->log_info(QL_PROCESS, "Node %s is down\n",
	    (char *)partition_b_node);

	delete [] command;
	return ((void *)QL_NOERR);
}

//
// Halt all the nodes of partition B. We use secure shell (SSH) or
// RSH to do this.
//
static
ql_errno_t
execute_partition_shutdown(namelist_t *partition_b_listp)
{

	ql_errno_t	retval;
	sc_syslog_msg_handle_t		handle;

	retval = execute_cmd(partition_b_listp, execute_node_shutdown);

	if (retval != QL_NOERR) {
		//
		// Execution of halt failed on a node. log an error.
		//
		logp->log_info(QL_PROCESS,
		    "Executing shutdown on a node returned error %d\n", retval);
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_QL_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The upgrade process halts the nodes of the second
		// partition. It then waits till all the nodes of the
		// partition have halted. The upgrade process was unable to
		// ping all nodes of the partition.
		// @user_action
		// Cluster upgrade has failed. Reboot all the nodes out of
		// cluster mode and recover from upgrade. Finish the cluster
		// upgrade by using the standard upgrade method.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Failed to halt the nodes of the partition");
		sc_syslog_msg_done(&handle);
	}
	logp->log_info(QL_PROCESS, "halt command has been executed "
	    "on all the nodes of second partition\n");
	return (retval);
}

//
// Execute replace_bootcluster on a node of partition B.
// Return Value:
//	QL_NOERR on Success.
//	QL_EEXEC on Failure.
//
static
void *
replace_bootcluster(void *partition_b_node)
{

	char		*command_execute_replace_bootcluster = NULL;
	int		retval;

	//
	// Copy the ql_util script
	//
	if (remote_copy(QL_UTIL_FILE, (char *)partition_b_node) != 0) {
		return ((void *)QL_EEXEC);
	}

	//
	// Copy the bootcluster script
	//
	if (remote_copy(QL_BOOTCLUSTER_FILE, (char *)partition_b_node) != 0) {
		return ((void *)QL_EEXEC);
	}

	//
	// Copy the replace_bootcluster script
	//
	if (remote_copy(QL_REPLACE_BOOTCLUSTER_FILE,
	    (char *)partition_b_node) != 0) {
		return ((void *)QL_EEXEC);
	}

	if (use_rsh) {

		//
		// Build the command to execute replace_bootcluster
		//
		command_execute_replace_bootcluster = alloc_mem(QL_PROCESS,
		    strlen(RSH_CMD) + 1 +
		    strlen((char *)partition_b_node) + 1 +
		    strlen(REPLACE_BC_CMD) + 1 +
		    strlen(REDIRECT_OUTPUT) + 1);

		(void) sprintf(command_execute_replace_bootcluster,
		    "%s %s %s %s",
		    RSH_CMD,
		    (char *)partition_b_node,
		    REPLACE_BC_CMD,
		    REDIRECT_OUTPUT);

	} else {

		//
		// Build the command to execute replace_bootcluster
		//
		command_execute_replace_bootcluster = alloc_mem(QL_PROCESS,
		    strlen(SSH_CMD) + 1 +
		    strlen(ROOT_USERNAME) + 1 +
		    strlen((char *)partition_b_node) + 1 +
		    strlen(SSH_OPTION_1) + 1 +
		    strlen(SSH_OPTION_2) + 1 +
		    strlen(REPLACE_BC_CMD) + 1 +
		    strlen(REDIRECT_OUTPUT) + 1);


		(void) sprintf(command_execute_replace_bootcluster,
		    "%s %s@%s %s %s %s %s",
		    SSH_CMD,
		    ROOT_USERNAME,
		    (char *)partition_b_node,
		    SSH_OPTION_1,
		    SSH_OPTION_2,
		    REPLACE_BC_CMD,
		    REDIRECT_OUTPUT);

	}

	//
	// Execute the replace_bootcluster script.
	//
	logp->log_info(QL_PROCESS, "Executing command %s\n",
	    command_execute_replace_bootcluster);

	//
	// If we are unable to execute the command, then we log a message.
	//
	if (execute_cmd_with_popen(QL_PROCESS,
	    command_execute_replace_bootcluster, &retval) != 0) {
		logp->log_err(QL_PROCESS,
		    "Encountered error executing command %s on node %s\n",
		    command_execute_replace_bootcluster,
		    (char *)partition_b_node);
		delete [] command_execute_replace_bootcluster;
		return ((void *)QL_EEXEC);
	}

	//
	// If replace_bootcluster returns error and is not able to
	// replace the bootcluster file we log a message.
	// This is not a critical error.
	//
	if (retval != QL_NOERR) {
		logp->log_err(QL_PROCESS, "Error occurred while executing"
		    " replace_bootcluster on node %s Return Value = %d\n",
		    (char *)partition_b_node, retval);
		delete [] command_execute_replace_bootcluster;
		return ((void *)QL_EEXEC);
	}

	//
	// Free the memory for the variables.
	//
	delete [] command_execute_replace_bootcluster;

	//
	// Success
	//
	return ((void *)QL_NOERR);
}

//
// Replace bootcluster on all the nodes of partition B.
// We use secure shell (SSH) or RSH to do this.
//
static
ql_errno_t
execute_replace_bootcluster(namelist_t *partition_b_listp)
{

	ql_errno_t	retval;
	sc_syslog_msg_handle_t		handle;

	retval = execute_cmd(partition_b_listp, replace_bootcluster);

	if (retval != QL_NOERR) {
		//
		// Replacement of bootcluster failed on a node.
		// Log an error.
		//
		logp->log_err(QL_PROCESS,
		    "Replacement of bootcluster on a node returned error"
		    " %d\n", retval);
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_QL_TAG, "");
		//
		// SCMSGS
		// @explanation
		// During dual-partition upgrade, bootcluster component was
		// not replaced on the nodes of the second partition. The
		// dual-partition upgrade will continue to completion.
		// @user_action
		// Do not boot any nodes in second partition into cluster mode
		// until the dual-partition upgrade completes.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Failed to replace bootcluster on the nodes of "
		    "second partition");
		sc_syslog_msg_done(&handle);
	} else {
		logp->log_info(QL_PROCESS, "bootcluster has been replaced "
		    "on all the nodes of second partition.\n");
	}
	return (retval);
}

//
// Execute ql_lu_begin on a node of partition B.
// Return Value:
//	QL_NOERR on Success.
//	QL_EEXEC on Failure.
//
static
void *
ql_lu_begin_task(void *partition_b_node)
{

	char		*command_execute_ql_lu_begin = NULL;
	int		retval;

	//
	// Copy the ql_util script
	//
	if (remote_copy(QL_UTIL_FILE, (char *)partition_b_node) != 0) {
		return ((void *)QL_EEXEC);
	}

	//
	// Copy the ql_lu_begin script
	//
	if (remote_copy(QL_LU_BEGIN_FILE, (char *)partition_b_node) != 0) {
		return ((void *)QL_EEXEC);
	}

	if (use_rsh) {

		//
		// Build the command to execute ql_lu_begin script
		//
		command_execute_ql_lu_begin = alloc_mem(QL_PROCESS,
		    strlen(RSH_CMD) + 1 +
		    strlen((char *)partition_b_node) + 1 +
		    strlen(QL_LU_BEGIN_CMD) + 1 +
		    strlen(mount_point) + 1 +
		    strlen(REDIRECT_OUTPUT) + 1);

		(void) sprintf(command_execute_ql_lu_begin,
		    "%s %s %s %s %s",
		    RSH_CMD,
		    (char *)partition_b_node,
		    QL_LU_BEGIN_CMD,
		    mount_point,
		    REDIRECT_OUTPUT);

	} else {

		//
		// Build the command to execute ql_lu_begin script
		//
		command_execute_ql_lu_begin = alloc_mem(QL_PROCESS,
		    strlen(SSH_CMD) + 1 +
		    strlen(ROOT_USERNAME) + 1 +
		    strlen((char *)partition_b_node) + 1 +
		    strlen(SSH_OPTION_1) + 1 +
		    strlen(SSH_OPTION_2) + 1 +
		    strlen(QL_LU_BEGIN_CMD) + 1 +
		    strlen(mount_point) + 1 +
		    strlen(REDIRECT_OUTPUT) + 1);


		(void) sprintf(command_execute_ql_lu_begin,
		    "%s %s@%s %s %s %s %s %s",
		    SSH_CMD,
		    ROOT_USERNAME,
		    (char *)partition_b_node,
		    SSH_OPTION_1,
		    SSH_OPTION_2,
		    QL_LU_BEGIN_CMD,
		    mount_point,
		    REDIRECT_OUTPUT);

	}

	//
	// Execute the ql_lu_begin script.
	//
	logp->log_info(QL_PROCESS, "Executing command %s\n",
	    command_execute_ql_lu_begin);

	//
	// If we are unable to execute the command, then we log a message.
	//
	if (execute_cmd_with_popen(QL_PROCESS,
	    command_execute_ql_lu_begin, &retval) != 0) {
		logp->log_err(QL_PROCESS,
		    "Encountered error executing command %s on node %s\n",
		    command_execute_ql_lu_begin,
		    (char *)partition_b_node);
		delete [] command_execute_ql_lu_begin;
		return ((void *)QL_EEXEC);
	}

	//
	// If ql_lu_begin script returns error, we log the message and
	// abort the upgrade
	//
	if (retval != QL_NOERR) {
		logp->log_err(QL_PROCESS, "Error occurred while executing"
		    " ql_lu_begin on node %s"
		    " Return Value = %d\n",
		    (char *)partition_b_node, retval);
		delete [] command_execute_ql_lu_begin;
		return ((void *)QL_EEXEC);
	}

	//
	// Free the memory for the variables.
	//
	delete [] command_execute_ql_lu_begin;

	//
	// Success
	//
	return ((void *)QL_NOERR);
}

static
ql_errno_t
execute_ql_lu_begin_task(namelist_t *partition_b_listp)
{
	ql_errno_t	retval;
	sc_syslog_msg_handle_t		handle;

	//
	// Read the mount point
	//
	if (read_mount_point() != 0) {
		logp->log_err(QL_PROCESS,
		    "Failed to read the mount point\n");
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_QL_TAG, "");
		//
		// SCMSGS
		// @explanation
		// During dual-partition upgrade in a live upgrade scenario,
		// the mount point information is not present.
		// @user_action
		// Refer to documentation about recovering from a failed
		// dual-partition upgrade in a live upgrade scenario.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Failed to read mount point on local node");
		sc_syslog_msg_done(&handle);
		return (QL_ENOMNT);

	}

	retval = execute_cmd(partition_b_listp, ql_lu_begin_task);

	if (retval != QL_NOERR) {
		//
		// Execution of ql_lu_begin failed.
		// Log an error.
		//
		logp->log_err(QL_PROCESS,
		    "Execution of ql_lu_begin failed on a node. "
		    "Error %d\n", retval);
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_QL_TAG, "");
		//
		// SCMSGS
		// @explanation
		// During dual-partition upgrade in a live upgrade scenario,
		// some upgrade related tasks have failed.
		// @user_action
		// Refer to documentation about recovering from a failed
		// dual-partition upgrade in a live upgrade scenario.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Failed to perform dual-partition begin tasks "
		    "on the nodes of second partition");
		sc_syslog_msg_done(&handle);
	} else {
		logp->log_info(QL_PROCESS, "Execution of ql_lu_begin "
		    "successful on all the nodes of second partition.\n");
	}
	return (retval);

}

//
// Start the cleanup process. The cleanup process takes care of cleaning
// up upgrade state, enabling quorum devices and node votes.
// Return Value:
//	QL_NOERR On success
//	Failure:
//		QL_EFORK
//
static
ql_errno_t
start_cleanup_process()
{
	pid_t				process_id;
	sc_syslog_msg_handle_t		handle;

	//
	// We fork a new process and let this process wait for the nodes
	// in partition B to join the cluster. The child then resets the
	// quorum votes and cleans up the CCR of quantum-leap entries.
	//
	if ((process_id = fork()) != -1) {
		if (process_id == 0) {
			// child.
			wait_and_cleanup();
			closelog();
			// skip freeing memory, we will exit anyway.
			exit(0);
		} else {
			// parent.
			closelog();
			return (QL_NOERR);
		}
	} else {
		logp->log_err(QL_PROCESS,
		    "Could not create a new child for cleanup\n");

		(void) sc_syslog_msg_initialize(&handle, SC_SYSLOG_QL_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The upgrade process on the root node resumes the upgrade
		// process on each node after nodes in the second partition
		// are shut down. If this fails, the upgrade process on each
		// node might be hung or receive an exception on a blocked
		// invocation.
		// @user_action
		// Cluster upgrade has failed. Reboot all the nodes out of
		// cluster mode and recover from upgrade. Finish the cluster
		// upgrade by using the standard upgrade method.
		//
		(void) sc_syslog_msg_log(handle, LOG_WARNING, MESSAGE,
		    "Encountered an exception during unblock invocation");
		sc_syslog_msg_done(&handle);
		return (QL_EFORK);
	}
	// NOT REACHED
	return (QL_NOERR);
}

//
// This function encapsulates the actions of the quantum-leap process.
// It is executed on the root node of partition A. It does the following
// tasks:
//	1. Lookup the quantum-leap object from the global name-server.
//	2. Remote execute the ql_stopapp command on the root node of
//	   partition B.
//	3. Halt all the nodes of partition B.
//	4. Wait for all the nodes in partition B to go down.
//	5. Change the quantum-leap upgrade state in the CCR to
//	   QL_UPGRADE_STATE_2.
//	6. Unblock ql_process on the non-root nodes of partition A.
//	7. Execute the common upgrade tasks on the current node.
//	8. Spawn off a process that waits for all the nodes in partition B
//	   to join and does cleanup.
// Return Value:
//	QL_NOERR Success
//	Failure:
//		QL_ELOOKUP
//		QL_ESPURIOUSOBJ
//		QL_ECCR
//		QL_ETHREAD
//		QL_EINVO
//		QL_EFORK
//		QL_EEXEC
//
static
int
execute_ql_process(ql_cl_ccr *ql_ccr_objp)
{
	char		*partition_b_root	= NULL;
	namelist_t	*partition_b_list	= NULL;
	namelist_t	*partition_a_list	= NULL;
	int		retval			= 0;

	Environment			env;
	naming::naming_context_var	ctx_v = ns::root_nameserver();
	CORBA::Exception		*ex;
	quantum_leap::ql_obj_var	qlobj_v;
	CORBA::Object_var		obj_v;
	sc_syslog_msg_handle_t		handle;

	ASSERT(!CORBA::is_nil(ctx_v));

	openlog(SC_SYSLOG_QL_TAG,  LOG_CONS|LOG_PID, LOG_DAEMON);
	logp->log_info(QL_PROCESS, "Starting Quantum_leap process\n");

	//
	// Look for the QL object in the name server.
	//
	logp->log_info(QL_PROCESS, "Check for QL Object\n");

	obj_v = ctx_v->resolve(quantum_leap::ql_obj::QL_OBJ_KEY, env);

	if ((ex = env.exception()) != NULL) {

		if (naming::not_found::_exnarrow(ex) != NULL) {

			logp->log_err(QL_PROCESS, "QL object not found\n");

		} else {
			logp->log_info(QL_PROCESS, "Encountered exception "
			    "while resolving QL object, id = %d\n",
			    ex->exception_enum());
		}
		env.clear();

		(void) sc_syslog_msg_initialize(&handle, SC_SYSLOG_QL_TAG, "");

		//
		// SCMSGS
		// @explanation
		// Cluster upgrade is in progress but lookup of QL object
		// failed. This can happen if the node hosting the name-server
		// registry goes down or if an earlier attempt to register the
		// upgrade object fails.
		// @user_action
		// Cluster upgrade has failed. Reboot all the nodes out of
		// cluster mode and recover from upgrade. Finish the cluster
		// upgrade by using the standard upgrade method.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Lookup of QL object failed");

		sc_syslog_msg_done(&handle);
		return (QL_ELOOKUP);
	}

	//
	// Narrow the object to a quantum leap object.
	//
	qlobj_v = quantum_leap::ql_obj::_narrow(obj_v);

	CL_PANIC(!CORBA::is_nil(qlobj_v));

	//
	// Start the cleanup process here. We do this here to optimize
	// as much as possible on the downtime during QL upgrade.
	//
	retval = start_cleanup_process();

	if (retval != QL_NOERR) {
		return (retval);
	}

	//
	// Need to execute the QL_STOP_APP_CMD command on the root node in
	// partition B. We have the root node information in the CCR.
	// We will fetch the root node and remote execute the script.
	//
	if (ql_ccr_objp->ql_read_root_b(&partition_b_root) != QL_NOERR) {

		logp->log_err(QL_PROCESS,
		    "Could not read the root node in second partition from "
		    "the CCR\n");

		(void) sc_syslog_msg_initialize(&handle, SC_SYSLOG_QL_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The upgrade process was unable to find needed information
		// in the CCR. The CCR might be missing this information.
		// @user_action
		// Cluster upgrade has failed. Reboot all the nodes out of
		// cluster mode and recover from upgrade. Finish the cluster
		// upgrade by using the standard upgrade method.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Reading root node of second partition from CCR failed");
		sc_syslog_msg_done(&handle);
		return (QL_ECCR);
	}

	//
	// Read the list of nodes in partition B.
	//
	if (ql_ccr_objp->ql_read_ccr_partition(QL_PARTITION_B_PREFIX,
	    &partition_b_list) != 0) {

		logp->log_err(QL_PROCESS,
		    "Could not read the nodes in second partition from "
		    "the CCR\n");

		(void) sc_syslog_msg_initialize(&handle, SC_SYSLOG_QL_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The upgrade process was unable to find needed information
		// in the CCR. The CCR might be missing this information.
		// @user_action
		// Cluster upgrade has failed. Reboot all the nodes out of
		// cluster mode and recover from upgrade. Finish the cluster
		// upgrade by using the standard upgrade method.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Failed to read nodes of second partition from the CCR");
		sc_syslog_msg_done(&handle);
		return (QL_ECCR);
	}

	//
	// Read the list of nodes in partition A.
	//
	if (ql_ccr_objp->ql_read_ccr_partition(QL_PARTITION_A_PREFIX,
	    &partition_a_list) != 0) {

		logp->log_err(QL_PROCESS,
		    "Could not read the nodes in first partition "
		    "from the CCR\n");

		(void) sc_syslog_msg_initialize(&handle, SC_SYSLOG_QL_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The upgrade process was unable to find needed information
		// in the CCR. The CCR might be missing this information.
		// @user_action
		// Cluster upgrade has failed. Reboot all the nodes out of
		// cluster mode and recover from upgrade. Finish the cluster
		// upgrade by using the standard upgrade method.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Failed to read nodes of first partition from the CCR");
		sc_syslog_msg_done(&handle);
		return (QL_ECCR);
	}

	//
	// Set the remote method
	//
	retval = set_remote_method(partition_b_root);

	if (retval != 0) {
		return (QL_EREM);
	}

	//
	// We go ahead and execute the command to replace bootcluster
	// on the nodes of Partition B.
	// For some reason if the bootcluster file did not get replaced
	// on some nodes of partition B, we do not fail the upgrade
	// as this is not a critical failure. Hence we don't currently
	// check the return value.
	//
	// No need to replace bootcluster in Live Upgrade scnario
	// as this is a automated process and no uder intervention
	// is required.
	//
	if (!lu_mode) {
		//
		// lu_mode is false
		//
		(void) execute_replace_bootcluster(partition_b_list);
	} else {
		//
		// QL is going on in a Live Upgrade scenario, then we need
		// to do QL related stuff on the partition B nodes. Then we need
		// to unmount and activate the alternate boot environment and
		// reboot the nodes in the new boot environment.
		//
		retval = execute_ql_lu_begin_task(partition_b_list);
		if (retval != QL_NOERR) {
			logp->log_err(QL_PROCESS,
			    "ql_lu_begin command returned %d\n", retval);
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_QL_TAG, "");
			//
			// SCMSGS
			// @explanation
			// During a dual-partition upgrade in a live upgrade
			// scenario,
			// some dual-partition upgrade related tasks
			// have failed.
			// Upgrade process has stopped and cannot proceed.
			// @user_action
			// Cluster upgrade has failed.
			// Reboot all the nodes out of
			// cluster mode and recover from upgrade.
			// Finish the cluster
			// upgrade by using the standard upgrade method.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "Failed to do dual-partition upgrade tasks on "
			    "second partition");
			sc_syslog_msg_done(&handle);
			return (retval);
		}

		logp->log_info(QL_PROCESS,
		    "Waiting for the first partition nodes to come online\n");
		(void) sc_syslog_msg_initialize(&handle, SC_SYSLOG_QL_TAG, "");

		//
		// SCMSGS
		// @explanation
		// During a dual-partition upgrade in a live upgrade
		// scenario, the node is waiting for all the
		// nodes of the first partition to come online
		// in the new boot environment.
		// @user_action
		// This is a informational message. If the user sees that
		// any of the nodes of the first partition has panicked or
		// hung, then he should abort the upgrade and do a normal
		// upgrade after doing the recovery steps.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Waiting for the first partition nodes to come online");
		    sc_syslog_msg_done(&handle);

		//
		// Check that all the nodes in partition A have come up
		//
		retval = wait_for_partition_a_nodes(partition_a_list);

		if (retval != QL_NOERR) {
			logp->log_err(QL_PROCESS,
			    "Error occurred while waiting for "
			    "first partition nodes. Error = %d\n", retval);
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_QL_TAG, "");
			//
			// SCMSGS
			// @explanation
			// During a dual-partition upgrade in a live upgrade
			// scenario,
			// processing must wait for the first partition nodes
			// to come online.
			// Some error occurred while waiting for the
			// nodes to come online.
			// Upgrade process has stopped and cannot proceed.
			// @user_action
			// Cluster upgrade has failed.
			// Reboot all the nodes out of
			// cluster mode and recover from upgrade.
			// Finish the cluster
			// upgrade by using the standard upgrade method.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "Error occurred while waiting for "
			    "first partition nodes");
			sc_syslog_msg_done(&handle);
			return (retval);
		}

	}

	//
	// Execute the QL_STOP_APP_CMD command on the root node in
	// partition B
	//
	// We can start accounting for downtime after successfully executing
	// this step.
	//
	retval = execute_stopapp_cmd(partition_b_root);

	if (retval != 0) {
		logp->log_err(QL_PROCESS, "Stop_App command returned %d\n",
		    retval);
		(void) sc_syslog_msg_initialize(&handle, SC_SYSLOG_QL_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The upgrade process tries to stop all the services on the
		// second partition before it brings down the cluster. Not all
		// services on the partition could be stopped. Upgrade process
		// has stopped and cannot proceed.
		// @user_action
		// Cluster upgrade has failed. Reboot all the nodes out of
		// cluster mode and recover from upgrade. Finish the cluster
		// upgrade by using the standard upgrade method.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE, "Failed "
		    "to stop all the services on second partition");
		sc_syslog_msg_done(&handle);
		return (QL_EEXEC);
	}

	delete [] partition_b_root;

	if (lu_mode) {
		//
		// We go ahead and remote execute init 6 on
		// all the nodes of partition B.
		//
		retval = execute_partition_init(partition_b_list);
	} else {
		//
		// We go ahead and remote execute shutdown on all the nodes
		// of partition B. We also make sure that the nodes are down
		// before proceeding further.
		//
		retval = execute_partition_shutdown(partition_b_list);
	}

	//
	// Some nodes for some reason failed to halt or we were not
	// able to determine that all the nodes are down. We fail
	// the upgrade here.
	//
	if (retval != QL_NOERR) {
		return (retval);
	}

	logp->log_info(QL_PROCESS, "All nodes in second partition are down\n");

	//
	// Change CCR and record upgrade state.
	//
	if (ql_ccr_objp->ql_change_ccr_state(QL_UPGRADE_STATE_2) != 0) {

		logp->log_err(QL_PROCESS,
		    "Could not update the upgrade state\n");

		(void) sc_syslog_msg_initialize(&handle, SC_SYSLOG_QL_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The upgrade process on the root node has failed to update
		// the CCR.
		// @user_action
		// Cluster upgrade has failed. Reboot all the nodes out of
		// cluster mode and recover from upgrade. Finish the cluster
		// upgrade by using the standard upgrade method.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Failed to update the upgrade state in the CCR");
		sc_syslog_msg_done(&handle);
		return (QL_ECCR);
	}

	//
	// The QL-upgrade process on the other nodes in partition A
	// would have blocked on an invocation. Unblock them.
	//
	logp->log_info(QL_PROCESS, "Unblock blocked ql_process\n");

	qlobj_v->unblock_ql(env);

	if ((ex = env.exception()) != NULL) {

		logp->log_info(QL_PROCESS, "Encountered an exception during "
		    "unblock_ql invocation, id = %d\n", ex->exception_enum());

		env.clear();

		(void) sc_syslog_msg_initialize(&handle, SC_SYSLOG_QL_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Encountered an exception during unblock invocation");
		sc_syslog_msg_done(&handle);
		return (QL_EINVO);
	}

	logp->log_info(QL_PROCESS, "Unblocked blocked ql_process\n");

	//
	// Execute the common tasks that needs to be done locally on the node
	// during quantum-leap upgrade.
	//
	execute_common_upgrade_tasks(partition_b_list);

	return (QL_NOERR);
}

//
// Main routine.
//
// Check if quantum-leap upgrade is in progress. If not exit. If upgrade is in
// progress, we check the state of the upgrade. If the state is
// QL_UPGRADE_STATE_2 and this node belongs to partition B, we dont have to do
// any more work. If this node belongs to partition A it has to execute its
// local taks. If the state is QL_UPGRADE_STATE_1, check if current node is
// the root node of partition A. If so, executes the upgrade process,
// otherwise block until woken up by the upgrade process on the root node.
// We then execute the common tasks that need to be run during upgrade and exit.
//
// Return value:
//	QL_NOERR on Success.
//	Failure:
//		QL_ENOLOG
//		QL_EORB
//		QL_ENOMEM
//		QL_ECLCONF
//		QL_ECCR
//		QL_ELOOKUP
//		QL_ESPURIOUSOBJ
//		QL_ETHREAD
//		QL_EINVO
//		QL_EFORK
//		QL_EEXEC
//
int
main(int argc, char * const argv[])
{
	char			local_nodename[CL_MAX_LEN+1];
	nodeid_t		local_nodeid;
	int			retval;
	char 			*partition_a_root_node	= NULL;
	ql_cl_ccr		*ql_ccr_objp		= NULL;
	char			*command		= NULL;
	bool			is_root_a_node		= false;
	sigset_t		sigs;
	sc_syslog_msg_handle_t	handle;

	//
	// Return an error if run from a non-global zone.
	//
	if (sc_zonescheck() != 0) {
		return (1);
	}

	//
	// logs go to the default log file.
	//
	logp = ql_log::get_instance(NULL);

	if (logp == NULL) {
		(void) printf(gettext("Log object is null\n"));
		return (QL_ENOLOG);
	}

	//
	// Read the ping timeout interval from the command line.
	//
	if (argc > 1) {
		timeout_interval = atoi(argv[1]);
		if (timeout_interval == 0) {
			timeout_interval = PING_TIMEOUT;
		}
		logp->log_info(QL_PROCESS, "Ping time out interval = %d\n",
		    timeout_interval);
	}

	//
	// Mask all signals.
	//
	(void) sigfillset(&sigs);
	(void) sigprocmask(SIG_BLOCK, &sigs, NULL);

	//
	// Initialize clconf library. This also initializes ORB
	//
	if ((retval = clconf_lib_init()) != 0) {
		logp->log_err(QL_PROCESS,
		    "Failed to initialize clconf library, "
		    "returned error = %d\n", retval);
		return (QL_ECLCONF);
	}

	ql_ccr_objp = new ql_cl_ccr();

	if (ql_ccr_objp == NULL) {
		logp->log_err(QL_PROCESS,
		    "Failed allocating memory to CCR object\n");
		return (QL_ENOMEM);
	}

	//
	// get the node id of this node.
	//
	local_nodeid = clconf_get_local_nodeid();

	//
	// get the node name of this node.
	//
	clconf_get_nodename(local_nodeid, local_nodename);

	//
	// Check if we are running in a Live Upgrade scenario.
	// This is done by checking for the existence of the
	// QL_TRANSFORM_CCR_FILE
	//
	if (access(QL_TRANSFORM_CCR_FILE, F_OK) == 0) {
		lu_mode = true;
		logp->log_info(QL_PROCESS,
		    "Running in live upgrade mode\n");
	}

	//
	// Check to see if Quantum-leap upgrade is in progress. If not,
	// we check if cleanup has been done. Cleanup after upgrade
	// happens after a majority of nodes of partition B have joined
	// the cluster. This can be one of those nodes that are late
	// joinees. We still have to do the cleanup if this is so.
	//
	if (!ql_ccr_objp->ql_upgrade_in_progress()) {

		if (!ql_ccr_objp->cleanup_complete(local_nodename)) {

			//
			// Here we make sure that we remove the state files
			// on the local node.
			//
			command = alloc_mem(QL_PROCESS,
			    strlen(QL_STATE_FILE_CLEANUP_CMD) + 1);

			(void) sprintf(command, "%s",
			    QL_STATE_FILE_CLEANUP_CMD);

			logp->log_info(QL_PROCESS, "Executing command %s\n",
			    command);
			if (execute_cmd_with_popen(QL_PROCESS, command,
			    &retval) != 0) {
				logp->log_err(QL_PROCESS,
				    "Could not execute the cmd %s\n", command);
				// It is ok to still return success.
				return (QL_NOERR);
			} else if (retval != 0) {
				logp->log_err(QL_PROCESS,
				    "Error while executing the cmd %s\n",
				    command);
				// It is ok to still return success.
				return (QL_NOERR);
			}
			//
			// We have now succesfully done a cleanup.
			// We can mark the cleanup to be complete for this
			// node.
			//

			//
			// If we fail here, it is ok, it is just cleanup after
			// all!.
			//
			retval =
			    ql_ccr_objp->mark_cleanup_complete(local_nodename);

			if (retval != QL_NOERR) {
				logp->log_info(QL_PROCESS, "Failed to mark "
				    "cleanup as complete\n");
			}

			logp->log_info(QL_PROCESS,
			    "Upgrade has completed on this node\n");
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_QL_TAG, "");
			//
			// SCMSGS
			// @explanation
			// Cluster upgrade has been completed on this node. A
			// majority of nodes have joined the cluster after the
			// upgrade. This message does not mean that the
			// upgrade has completed on all the nodes of the
			// cluster or that the cluster has full membership.
			// @user_action
			// This is an informational message, no user action is
			// needed.
			//
			(void) sc_syslog_msg_log(handle,
			    LOG_NOTICE, MESSAGE, "Upgrade has completed on "
			    "this node");
			sc_syslog_msg_done(&handle);
		} else {
			logp->log_info(QL_PROCESS,
			    "Upgrade not in progress\n");
		}
		return (QL_NOERR);
	}

	logp->log_info(QL_PROCESS, "Upgrade in progress\n");

	//
	// Check the quantum-leap upgrade state. If the state is
	// QL_UPGRADE_STATE_2, then this is a node of partition B
	// joining the cluster after the upgrade or a non-root node of
	// partition A that is joining the cluster a bit later than the root
	// node itself. In case this node belongs to partition B we can let
	// the node join here and not do anything. If it is a node of
	// partition A it has to do its local tasks as part of the upgrade
	// procedure.
	//
	if (ql_ccr_objp->ql_upgrade_check_state(QL_UPGRADE_STATE_2)) {
		namelist_t *partition_b_listp = NULL;
		bool partition_b_node = false;
		if ((ql_ccr_objp->ql_read_ccr_partition(
		    QL_PARTITION_B_PREFIX, &partition_b_listp))
		    != QL_NOERR) {
			logp->log_err(QL_PROCESS,
			    "Error reading nodes of second partition\n");
			return (QL_ECCR);
		}
		while (partition_b_listp) {
			if (strcmp(local_nodename, partition_b_listp->name)
			    == 0) {
				// Match found.
				partition_b_node = true;
				break;
			}
			partition_b_listp = partition_b_listp->next;
		}
		if (partition_b_node) {
			logp->log_info(QL_PROCESS, "QL upgrade state is %d,"
			    " second partition node joining the cluster\n",
			    QL_UPGRADE_STATE_2);
			return (QL_NOERR);
		} else {
			//
			// This node belongs to partition A. This is a non-root
			// node of A that is joining the cluster a bit later
			// than the root node.
			// Execute the common tasks that needs to be done on
			// each node during quantum-leap upgrade.
			//
			logp->log_info(QL_PROCESS, "QL upgrade state is %d,"
			    " first partition node joining the cluster\n",
			    QL_UPGRADE_STATE_2);
			execute_common_upgrade_tasks(partition_b_listp);
			return (QL_NOERR);
		}
	}

	//
	// Fetch the root node of partition A from the CCR.
	//
	if (ql_ccr_objp->ql_read_root_a(&partition_a_root_node) != 0) {
		logp->log_err(QL_PROCESS,
		    "Error reading root node of first partition\n");
		return (QL_ECCR);
	}

	//
	// The behaviour of this process changes based on where it is
	// running.
	//
	if (strcmp(local_nodename, partition_a_root_node) == 0) {
		is_root_a_node = true;
		logp->log_info(QL_PROCESS,
		    "Running on root node of first partition\n");
	} else {
		logp->log_info(QL_PROCESS,
		    "Not running on root node of first partition\n");
	}

	if (is_root_a_node) {
		//
		// We are running on the root node of partition A. We
		// execute the upgrade process here.
		//
		retval = execute_ql_process(ql_ccr_objp);

		logp->close();

	} else {
		//
		// We are not running on the root node of partition A.
		// We wait till the ql_process on the root node
		// signals and unblocks us.
		//
		retval = wait_for_ql_process(ql_ccr_objp);
	}

	return (retval);
}
