/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)ql_rgm.cc	1.3	08/05/20 SMI"

#include <nslib/ns.h>
#include <libintl.h>
#include <signal.h>
#include <h/quantum_leap.h>
#include <ql_ccr.h>
#include <ql_err.h>
#include <ql_cl_ccr.h>
#include <rgm/sczones.h>

#define	QL_UNBLOCK_RGM "QL_RGM"
#define	QL_RGM_LOG "/var/cluster/logs/ql_server_debug.log"

const char *quantum_leap::ql_obj::QL_OBJ_KEY;
const char *quantum_leap::ql_rgm_obj::QL_RGM_OBJ_KEY;

ql_log *logp = NULL;

extern	ql_errno_t unregister_ql_object(const char *tag);
extern	ql_errno_t unregister_rgm_object(const char *tag);
extern 	ql_errno_t unblock_rgm(const char *tag);

//
// Unregister the QL object from the global nameserver if
// run from the root node of partition A.
// Return value:
//	QL_NOERR On success.
//	Failure:
//		QL_ECLCONF
//		QL_ECCR
//		QL_EUNBIND
//		QL_ELOOKUP
//		QL_ESTATE
//
static
ql_errno_t
execute_ql_upgrade_task(ql_cl_ccr* ql_ccr_objp)
{
	ql_errno_t	retval = QL_NOERR;
	nodeid_t	local_nodeid;
	char		local_nodename[CL_MAX_LEN+1];
	char 		*partition_a_root_node	= NULL;

	//
	// Validate state.
	//
	if (!ql_ccr_objp->ql_upgrade_check_state(QL_UPGRADE_STATE_2)) {
		logp->log_err(QL_UNBLOCK_RGM, "QL upgrade not in state %d!\n",
		    QL_UPGRADE_STATE_2);
		return (QL_ESTATE);
	}

	//
	// Initialize clconf library.
	//
	if (clconf_lib_init() != 0) {
		logp->log_err(QL_UNBLOCK_RGM,
		    "Failed to initialize clconf library\n");
		return (QL_ECLCONF);
	}

	//
	// Get the node id of this node.
	//
	local_nodeid = clconf_get_local_nodeid();

	//
	// Get the node name of this node.
	//
	clconf_get_nodename(local_nodeid, local_nodename);

	//
	// Fetch the root node of partition A from the CCR.
	//
	if (ql_ccr_objp->ql_read_root_a(&partition_a_root_node) != 0) {
		logp->log_err(QL_UNBLOCK_RGM,
		    "Error reading root node of partition A\n");
		return (QL_ECCR);
	}

	//
	// Check if root node of partition A.
	//
	if (strcmp(local_nodename, partition_a_root_node) == 0) {

		logp->log_info(QL_UNBLOCK_RGM,
		    "Running on root node of partition A\n");

		//
		// We are running on the root node of partition A. We
		// go ahead and unregister "quantum-leap" object.
		//
		retval = unregister_ql_object(QL_UNBLOCK_RGM);

	} else {
		//
		// We are not running on the root node of partition A.
		// We need not do any work here.
		//
		logp->log_info(QL_UNBLOCK_RGM,
		    "Not running on root node of partition A\n");
	}
	return (retval);
}

//
// Main routine.
// Return value:
//	QL_NOERR On success.
//	Failure:
//		QL_ENOLOG
//		QL_ENOMEM
//		QL_ELOOKUP
//		QL_EINVO
//		QL_ESPURIOUSOBJ
//		QL_EUNBIND
//		QL_ESTATE
//
int
main()
{
	ql_cl_ccr	*ql_ccr_objp		= NULL;
	ql_errno_t	retval			= QL_NOERR;
	sigset_t	sigs;

	//
	// If run within non-global zones, exit with an error.
	//
	if (sc_zonescheck() != 0) {
		return (1);
	}

	logp = ql_log::get_instance(QL_RGM_LOG);

	if (logp == NULL) {
		(void) printf(gettext("Log object is null\n"));
		return (QL_ENOLOG);
	}

	//
	// Mask all signals.
	//
	(void) sigfillset(&sigs);
	(void) sigprocmask(SIG_BLOCK, &sigs, NULL);

	ql_ccr_objp = new ql_cl_ccr();

	if (ql_ccr_objp == NULL) {
		logp->log_err(QL_UNBLOCK_RGM,
		    "Failed allocating memory to CCR object\n");
		return (QL_ENOMEM);
	}

	//
	// Check if quantum-leap upgrade is in progress. If so, we
	// execute the upgrade tasks for this process.
	// Even if we fail here, we still go ahead.
	//
	if (ql_ccr_objp->ql_upgrade_in_progress()) {
		retval = execute_ql_upgrade_task(ql_ccr_objp);
		if (retval != QL_NOERR) {
			logp->log_err(QL_UNBLOCK_RGM,
			    "Failed to execute Dual-partition upgrade task");
		}
	}

	//
	// unblock the blocked RGM invocation.
	//
	retval = unblock_rgm(QL_UNBLOCK_RGM);

	if (retval != QL_NOERR) {
		return (retval);
	}

	//
	// Remove the RGM object from the local nameserver.
	//
	retval = unregister_rgm_object(QL_UNBLOCK_RGM);

	logp->close();

	return (retval);
}
