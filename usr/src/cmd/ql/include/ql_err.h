/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_QL_ERR_H_
#define	_QL_ERR_H_

#pragma ident	"@(#)ql_err.h	1.4	08/05/20 SMI"

//
// List of errors possible during Upgrade.
//
typedef enum ql_errno {

	QL_NOERR	= 0,	// Normal return - no error.

	QL_ENOMEM	= 2,	// Memory Allocation failed.

	QL_EEXEC	= 3,	// Could not spawn and execute a QL task.

	QL_ELOOKUP	= 4,	// Encountered an exception while resolving
				// from the nameserver.

	QL_ESPURIOUSOBJ	= 5,	// The resolved object was not found to be of
				// expected type.

	QL_EINVO	= 6,	// Encountered an exception in a Blocking or
				// Unblocking invocation.

	QL_ECCR		= 7,	// Encountered an exception when reading from
				// the CCR.

	QL_ETHREAD	= 8,	// Failed during thread creation or thread
				// join.

	QL_EFORK	= 9,	// Failed to fork and execute a QL task.

	QL_ENOLOG	= 10,	// Failed to initialize logging.

	QL_EORB		= 11,	// ORB initialization failed.

	QL_ECLCONF	= 12,	// clconf library initialization failed or
				// some error occured while calling a function
				// of the clconf library.

	QL_EBIND	= 13,	// Failed to bind an object in the nameserver.

	QL_EUNBIND	= 14,	// Failed to unbind an object in the nameserver.

	QL_EBADARG	= 15,	// Argument list is incorrect for a QL command.

	QL_ESTATE	= 16,   // Bad or unexpected QL Upgrade state.

	QL_SCCONFERR	= 17,	// scconf returned an error.

	QL_ENOMNT	= 18,	// Did not find mount point for alternate BE

	QL_EREM		= 19	// Did not find remote method


} ql_errno_t;


#endif /* !_QL_ERR_H_ */
