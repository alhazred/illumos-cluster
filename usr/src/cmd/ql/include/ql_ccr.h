/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_QL_CCR_H_
#define	_QL_CCR_H_

#pragma ident	"@(#)ql_ccr.h	1.3	08/05/20 SMI"

#include <h/ccr.h>

#define	CCR_DIR			"ccr_directory"
#define	QL_TABLE		"infrastructure"

//
// Quantum-leap upgrade internal CCR states.
//
typedef enum ql_ccr_state {

	//
	// QL upgrade is in state 0 after the upgrade is complete.
	//
	QL_UPGRADE_COMPLETE_STATE	= 0,

	//
	// Upgrade is in state 1 after the nodes of partition A have been
	// upgraded and the QL_PARTA_CMD command is run.
	//
	QL_UPGRADE_STATE_1		= 1,

	//
	// Upgrade is in state 2 after the root node of partition A gets to
	// know that the nodes in partition B are down. This means that it
	// is safe to start the services on this partition.
	//
	QL_UPGRADE_STATE_2		= 2

} ql_ccr_state_t;

//
// CCR entry to record the upgrade state.
// QL_STATE_KEY	<ql_ccr_state_t>
//
#define	QL_STATE_KEY		"cluster.upgrade.ql.ql_state"

//
// CCR entries to record the root nodes of a partition.
// QL_PARTITION_[A|B]_ROOT_KEY	<node_name>
//
#define	QL_PARTITION_A_ROOT_KEY "cluster.upgrade.ql.partition_root_a"
#define	QL_PARTITION_B_ROOT_KEY "cluster.upgrade.ql.partition_root_b"

//
// CCR entries to record the list of nodes in each partition.
// QL_PARTITION_[A|B]_PREFIX.<node_name>	<node_name>
//
#define	QL_PARTITION_A_PREFIX	"cluster.upgrade.ql.partition_a"
#define	QL_PARTITION_B_PREFIX	"cluster.upgrade.ql.partition_b"

#define	QL_PREFIX		"cluster.upgrade.ql."
#define	QL_PARTITION_PREFIX	"cluster.upgrade.ql.partition_"

//
// CCR entry to indicate that the cleanup is not complete. We
// make sure that this has a different prefix other than QL_PREFIX
// so that it is not removed with the other QL entries. This
// key is removed automatically after the cleanup of the state
// files is complete on all the nodes configured in the cluster.
//
#define	QL_CLEANUP_KEY		"cluster.upgrade.cleanup.ql"

//
// If the key gets changed here, it also has to be changed in the
// transport code.
//
#define	SC_TRANSPORT_VERSION_KEY	"cluster.transport.sc_transport_version"

typedef char *nodename_t;

typedef struct namelist {
	nodename_t	name;
	struct namelist *next;
} namelist_t;

#endif /* !_QL_CCR_H_ */
