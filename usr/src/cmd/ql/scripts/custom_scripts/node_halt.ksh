#! /bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#pragma ident	"@(#)node_halt.ksh	1.3	08/05/20 SMI"
#
# node_halt.ksh
#
# node_halt
#
# When you execute the "scinstall -u begin" command, the system prepares
# the cluster for partitioning. Next the system halts the nodes that will
# form first partition to upgrade. This script is run on each node that will
# belong to just before the shutdown of that node.
#
# The system will evacuate from the node all applications that are under
# Sun Cluster control.
#
# If all applications are run under the control of Sun Cluster,
# you do not need to modify this script. The default action of this
# script does nothing.
#
# This script provides a place for you to take actions before
# the system shutdown of an individual node. This is usually
# required when you run applications on the node that are not
# under Sun Cluster control.
#
# To use this script, first create your own separate scripts. Then add to
# this script entries that call your scripts.
#
