#! /bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#pragma ident	"@(#)cluster_pre_halt_apps.ksh	1.3	08/05/20 SMI"
#
# cluster_pre_halt_apps.ksh
#
# cluster_pre_halt_apps
#
# This script provides a place for you to take actions before
# the system halts applications under Sun Cluster control.
# For example, early versions of Sun Cluster do not have the
# ability to halt Oracle RAC.
#
# This script is run on one node the second partition before the
# execution of the command that halts all applications under
# Sun Cluster control.
#
# Note that there is another script, cluster_post_halt_apps for
# taking actions after all the applications under Sun Cluster
# control have been halted.
#
# If all applications are run under the control of Sun Cluster,
# you do not need to modify this script. The default action of this
# script does nothing.
#
# To use this script, first create your own separate scripts. Then
# add to this script entries that call your scripts.
#
