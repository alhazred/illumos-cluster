#! /bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#pragma ident	"@(#)upgrade_partition_a_common.ksh	1.4	08/05/20 SMI"
#
# upgrade_partition_a_common.ksh
#
# upgrade_partition_a_common -a partition_a_root_node -b partition_b_root_node
# -n partition_a_commalist -v transport_version -R
#
# Options:
#
# -a root node of partition A
# -b root node of partition B
# -n list of comma seperated partition A nodes
# -v the transport version
# -R indicates that QL is going on in a LU scenario
#
# This script does the following changes to the CCR on the current
# node in partition A:
#
#	1. Add the root node of partititon A to the CCR.
#	2. Add nodes of partition A to the CCR.
#	3. Add the root node of partition B to the CCR.
#	3. Add nodes of partition B to the CCR.
#	4. Change the quorum vote of root node for partition A to one.
#	5. Change the quorum vote of all nodes in the cluster to zero.
#	6. Set the Dual-partition upgrade state to QL_UPGRADE_STATE_1
#	7. Add the transport version to the CCR.
#
# This script comments out in file /etc/vfstab any global file systems
# of type Shared QFS that request file system to be mounted at boot time.
# It disables fencing and runs a custom script to make any incompatible
# changes to the CCR to support the new software.
#

###############################################################
#
# Variable globals
#
###############################################################
integer -r SC_TRUE=1
integer -r SC_FALSE=0

typeset HOSTNAME_CMD=/usr/bin/hostname

typeset CLUSTER_LIB_PATH=/usr/cluster/lib

typeset QL_UTIL_PATH=/usr/cluster/lib/scadmin/ql/
typeset QL_UTIL_FILE=ql_util

#program name and args list. Used for log messages.
if [[ -z $PROG ]]; then
	typeset -r PROG=${0##*/}
fi

integer sc_transport_version=0

###############################################################
#
# print_usage
#
#	Print usage message to stderr
#
###############################################################
print_usage()
{
	echo "$(gettext 'usage'):${PROG} -a <node> -b <node> [-n {<node>,..}] [-R]"\
	    >&2
}

###############################################################
#
# loadlib() libname
#
#	Load the named include file.
#
#	Return:
#		zero		Success
#		non-zero	Failure
#
###############################################################
loadlib()
{
	typeset libname=$1

	#
	# Load the library
	#
	. ${libname}

	if [[ $? != 0 ]]; then

		printf "$(gettext '%s:  Unable to load \"%s\"')\n" \
		    "${PROG}" "${libname}" >&2

		return 1

	fi
}

################################################################
#
# ql_change_vfstab
#
# This function changes the /etc/vfstab file. It comments out
# entries of any QFS file system that mounts at boot time.
#
#	Return:
#		zero		Success
#		non-zero	Failure
################################################################
ql_change_vfstab()
{
	# data elements in a vfstab entry. 
	typeset special
	typeset fsckdev
	typeset mountp
	typeset type
	typeset pass
	typeset boot
	typeset options
	
	# local variable.
	typeset line
	
	/usr/bin/rm -f /tmp/vfstab.$$
	touch /tmp/vfstab.$$ 

	#
	# Read each entry of vfstab file and change it if required.
	#
	while read special fsckdev mountp type pass boot options
	do
		# Skip comments and blank lines

		case ${special} in

		'#'* | '')      # We add a "#" to the commented line to
				# distinguish this later on.

				printf \
				"$(gettext '%s\t%s\t%s\t%s\t%s\t%s\t%s')\n" \
				    "#$special" "$fsckdev" "$mountp" \
				    "$type" "$pass" "$boot" \
				    "$options" >> /tmp/vfstab.$$

				continue
				;;
		esac

		if [[ $boot = "yes" ]]; then

			if [[ $type = "samfs" || $type = "qfs" ]]; then
				#
				# Found a shared QFS filesystem entry.
				# Comment it out.
				#
				printf "$(gettext '%s: Commenting out %s')\n" \
				    "${PROG}" "{$special}" | logdbgmsg

				printf \
				"$(gettext '%s\t%s\t%s\t%s\t%s\t%s\t%s')\n" \
				    "#$special" "$fsckdev" "$mountp" \
				    "$type" "$pass" "$boot" \
				    "$options" >> /tmp/vfstab.$$

			else
				#
				# mount at boot option is set but this is
				# not a QFS filesystem. So we dont do
				# anything here except for putting the line
				# back to the file.
				#
				printf \
				"$(gettext '%s\t%s\t%s\t%s\t%s\t%s\t%s')\n" \
				    "$special" "$fsckdev" "$mountp" \
				    "$type" "$pass" "$boot" \
				    "$options" >> /tmp/vfstab.$$

			fi
		else
			#
			# mount at boot option is not set. So we dont have
			# to do anything here except for putting the line
			# back into the file.
			#
			printf \
			"$(gettext '%s\t%s\t%s\t%s\t%s\t%s\t%s')\n" \
			    "$special" "$fsckdev" "$mountp" \
			    "$type" "$pass" "$boot" \
			    "$options" >> /tmp/vfstab.$$
			
		fi

	done < /etc/vfstab

	#
	# If an error happened, log an error and return failure.
	#
	if [[ $? -ne 0 ]]; then

		printf "$(gettext '%s: Error changing vfstab')\n" \
		    "${PROG}" | logerr
		return 1

	fi

	#
	# Here we copy the changes back to the vfstab file.
	#
	cp /tmp/vfstab.$$ /etc/vfstab || return 1 

	#
	# return success.
	#
	return 0
}

################################################################
#
# verify_args partition_b_root_node partition_a_root_node partition_a_nodes
#
# Verify that the arguments passed are correct. This function reads data
# from the QL_DATA_FILE to verify the arguments.
#
#	Return:
#		zero		Success
#		non-zero	Failure
#
################################################################
verify_args()
{
	#
	# partition B
	#
	typeset partition_b_root_node
	typeset nodes_b_tmp

	#
	# partition A
	#
	typeset partition_a_root_node
	typeset partition_a_nodes
	typeset root_node_a_tmp
	typeset nodes_a_tmp

	# local variables.
	typeset node
	typeset nodeA

	partition_b_root_node=$1
	partition_a_root_node=$2

	shift 2
	
	partition_a_nodes="$*"

	#
	# Read partition A root node.
	#
	root_node_a_tmp=`read_partition_A_root`

	if [[ $? -ne 0 ]]; then
		printf "$(gettext '%s: Failed to read root node of partition A')\n" \
		    "${PROG}" | logerr
		return 1
	fi

	#
	# Read partition B root node.
	#
	root_node_b_tmp=`read_partition_B_root`

	if [[ $? -ne 0 ]]; then
		printf "$(gettext '%s: Failed to read root node of partition B')\n" \
		    "${PROG}" | logerr
		return 1
	fi

	#
	# Read partition A nodes.
	#
	nodes_a_tmp=`read_partition_A`

	if [[ $? -ne 0 ]]; then
		printf \
		    "$(gettext '%s: Failed to read nodes of partition A')\n" \
		    "${PROG}" | logerr
		return 1
	fi

	if [[ $partition_b_root_node != $root_node_b_tmp ]]; then
		printf "$(gettext '%s: Wrong root node')\n" "${PROG}" | logerr
		return 1
	fi

	if [[ $partition_a_root_node != $root_node_a_tmp ]]; then
		printf "$(gettext '%s: Wrong root node')\n" "${PROG}" | logerr
		return 1
	fi
	
	#	
	# Are all nodes in the file present in the argument list?
	#
	for node in $nodes_a_tmp
	do
		found=${SC_FALSE}
		for nodeA in $partition_a_nodes 
		do
			if [[ $nodeA == $node ]]; then
				found=${SC_TRUE}
				break
			fi
		done
		if [[ $found -ne ${SC_TRUE} ]]; then
			printf "$(gettext '%s: Node %s not in list')\n" \
			    "${PROG}" "${node}" | logerr
			return 1
		fi
	done

	#	
	# Are all nodes in the argument list present in the file?
	#
	for nodeA in $partition_a_nodes
	do
		found=${SC_FALSE}
		for node in $nodes_a_tmp 
		do
			if [[ $nodeA == $node ]]; then
				found=${SC_TRUE}
				break
			fi
		done
		if [[ $found -ne ${SC_TRUE} ]]; then
			printf "$(gettext '%s: Node %s not in list')\n" \
			    "${PROG}" "${nodeA}" | logerr
			return 1
		fi
	done

	#
	# Read and verify transport version.
	#
	sc_transport_version_tmp=`read_transport_version`
	
	if [[ $? -ne 0 ]]; then
		printf \
		    "$(gettext '%s: Failed to read transport version')\n" \
		    "${PROG}" | logerr
		return 1
	fi

	if [[ $sc_transport_version_tmp -ne $sc_transport_version ]]; then
		printf \
		    "$(gettext '%s: Invalid transport version')\n" \
		    "${PROG}" | logerr 
		return 1
	fi
	return 0
}

####################################################
#
# main
#
####################################################
main()
{
	# partition B.
	typeset partition_b_root_node
	typeset partition_b_nodes
	typeset root_node_b_tmp

	# partition A.
	typeset partition_a_root_node
	typeset partition_a_nodes
	typeset nodes_a_tmp
	typeset partition_a_commalist

	# flags.
	integer root_node_pb_opt=${SC_FALSE}
	integer root_node_pa_opt=${SC_FALSE}
	integer node_list_opt=${SC_FALSE}
	integer	version_opt=${SC_FALSE}
	integer found=${SC_FALSE}
	integer lu_mode=${SC_FALSE}

	# local variable.
	typeset hostname
	typeset node
	typeset retval

	# load common functions
	loadlib ${QL_UTIL_PATH}/${QL_UTIL_FILE} || return 1

	#
	# Global zone privilege check. If we are running in a non-global
        # we exit here.
        #
	sc_zones_check

	while getopts a:b:n:v:R c 2>/dev/null
        do
                case ${c} in
                a)      # Root node of partition A specified.
                        partition_a_root_node=${OPTARG}
                        root_node_pa_opt=${SC_TRUE}

			if [[ -z $partition_a_root_node ]]; then
				print_usage
				return 1
			fi
                        ;;
		b)	# Root node of partition B specified.
			partition_b_root_node=${OPTARG}
			root_node_pb_opt=${SC_TRUE}

			if [[ -z $partition_b_root_node ]]; then
				print_usage
				return 1
			fi
			;;
		n)	# Other nodes of partition A specified.
			partition_a_nodes="$(IFS=, ; echo ${OPTARG})"
			node_list_opt=${SC_TRUE}

			if [[ -z $partition_a_nodes ]]; then
				print_usage
				return 1
			fi
			;;
		v)	# Transport release version.
			sc_transport_version=${OPTARG}
			version_opt=${SC_TRUE}

			if [[ -z $sc_transport_version ]]; then
				print_usage
				return 1
			fi
			;;
		R)	# Live Upgrade Mode
			lu_mode=${SC_TRUE}
			;;
                \?)     print_usage
                        return 1
                        ;;
                *)      # bad option
                        print_usage
                        return 1
                        ;;
                esac
        done

	#
	# check if root node of partition A has been specified.
	#
	if [[ $root_node_pa_opt -ne ${SC_TRUE} ]]; then
		print_usage
		return 1
	fi
	
	#
	# check if root node of partition B has been specified.
	#
	if [[ $root_node_pb_opt -ne ${SC_TRUE} ]]; then
		print_usage
		return 1
	fi

	#
	# check if transport version has been specified.
	#
	if [[ $version_opt -ne ${SC_TRUE} ]]; then
		print_usage
		return 1
	fi

	#
	# In case the node list was not given, there was one node in the
	# partition that is also the root node.
	# 
	if [[ $node_list_opt -ne ${SC_TRUE} ]]; then
		partition_a_nodes=$partition_a_root_node
	fi

	#
	# check for duplicates.
	#
	duplicate "${partition_b_root_node} ${partition_a_nodes}"

	if [[ $? -ne 0 ]]; then
		printf "$(gettext '%s: Duplicate nodes present')\n" \
		"${PROG}" | logerr
		return 1
	fi

	#
	# Mask out SIGHUP and SIGINT
	#
	trap 'ign_sig' HUP INT

	#
	# check to see if we are root.
	#
	verify_isroot

	if [[ $? -ne 0 ]]; then
		return 1
	fi

	#
	# check if command exists
	# 
	if [[ ! -x /usr/sbin/clinfo ]]; then
		printf "$(gettext '%s: Could not find %s')\n" "${PROG}" \
		    "clinfo" | logerr
		return 1
	fi

	#
	# check for non-cluster mode.
	#
	/usr/sbin/clinfo > /dev/null 2>&1
	retval=$?

	if [[ ${retval} -eq 0 ]] && [[ ${lu_mode} -eq ${SC_FALSE} ]]; then
		#
		# In normal mode, when a plain QL
		# upgrade is going on, the node
		# should be in non cluster mode
		#
		printf "$(gettext '%s Node must be in noncluster mode')\n" \
		    "${PROG}" | logerr
		return 1
	fi

	if [[ ${retval} -ne 0 ]] && [[ ${lu_mode} -eq ${SC_TRUE} ]]; then
		#
		# In lu mode, when a QL upgrade is
		# going on along with a Live Upgrade
		# the node should be in cluster mode
		#
		printf "$(gettext '%s Node must be in cluster mode')\n" \
		    "${PROG}" | logerr
		return 1

	fi

	#
	# Before we change the CCR, we validate that the data given to this
	# command is consistent with the earlier command of Dual-partition
	# upgrade. We check if we have the file QL_DATA_FILE_PATH/QL_DATA_FILE.
	# This should have the root node of partition A, root node of
	# partition B  and all the nodes of partition A. We read this file and
	# validate the arguments of this command with this file.
	#
	verify_args $partition_b_root_node $partition_a_root_node \
	    $partition_a_nodes

	if [[ $? -ne 0 ]]; then
		return 1
	fi

	partition_a_commalist=$partition_a_root_node

	for node in $partition_a_nodes 
	do
		if [[ $node != $partition_a_root_node ]]; then
			partition_a_commalist="$partition_a_commalist,$node"
		fi
	done

	#
	# Add the following to the CCR:
	#	1. root node of partititon B.
	#	2. nodes of partition B.
	#	3. nodes of partition A.
	#	4. Change the quorum vote of root node for partition A to 1.
	#	5. Change the quorum vote of all nodes in the cluster to 0.
	#	6. Set the Dual-partition upgrade state to QL_UPGRADE_STATE_1
	#
	export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${CLUSTER_LIB_PATH}

	#
	# The first node in the comma seperated list of nodes is considered
	# to be the root node by the command.
	#
	if [[ ${lu_mode} -eq ${SC_TRUE} ]]; then
		#
		# Execute ql_change_ccr in live upgrade mode.
		#
		${QL_LIB_PATH}/ql_change_ccr -x -R -a $partition_a_commalist \
		    -b $partition_b_root_node > /dev/null 2>&1
	else
		#
		# Execute ql_change_ccr in normal mode
		#
		${QL_LIB_PATH}/ql_change_ccr -x -a $partition_a_commalist \
		    -b $partition_b_root_node > /dev/null 2>&1
	fi

	if [[ $? -ne 0 ]]; then

		printf "$(gettext '%s: ql_change_ccr returned error')\n" \
		    "${PROG}" | logerr

		return 1
	fi

	#
	# Add the transport version information to the CCR.
	#
	if [[ ${lu_mode} -eq ${SC_TRUE} ]]; then
		#
		# Execute ql_change_ccr in live upgrade mode.
		#
		${QL_LIB_PATH}/ql_change_ccr -x -R -v $sc_transport_version \
		    > /dev/null 2>&1
	else
		#
		# Execute ql_change_ccr in normal mode.
		#
		${QL_LIB_PATH}/ql_change_ccr -x -v $sc_transport_version \
		    > /dev/null 2>&1
	fi

	if [[ $? -ne 0 ]]; then

		printf "$(gettext '%s: ql_change_ccr returned error.')\n" \
		    "${PROG}" | logerr
		printf "$(gettext \
		    '%s: Failed to update the transport release version.')\n" \
		    "${PROG}" | logerr

		return 1
	fi

	#
	# Change the /etc/vfstab file to comment out entries that mount
	# shared QFS filesystems at boot time.
	#
	printf "$(gettext '%s: Changing /etc/vfstab')\n" ${PROG} | logdbgmsg \
	    > /dev/null 2>&1 

	ql_change_vfstab

	if [[ $? -ne 0 ]]; then
		return 1
	fi

	#
	# Touch "/scnoreserveddisks". This is to make sure that we dont
	# fence off disks in partition B when the nodes in partition
	# A are still up.  
	# 
	touch ${UPGRADE_FLAG_FILE} || return 1

	printf "$(gettext '%s: Executing %s')\n" ${PROG} \
	    ${QL_CCR_CUSTOM_SCRIPT_PATH}/${QL_CCR_CUSTOM_SCRIPT} | logdbgmsg \
	    > /dev/null 2>&1

	#
	# Execute the custom CCR script if one exists. This script
	# is needed to add changes to the CCR that happens during
	# upgrade.
	#
        if [[ -x ${QL_CCR_CUSTOM_SCRIPT_PATH}/${QL_CCR_CUSTOM_SCRIPT} ]]; then
                printf "$(gettext '%s: Executing %s')\n" "${PROG}" \
		    ${QL_CCR_CUSTOM_SCRIPT}
                ${QL_CCR_CUSTOM_SCRIPT_PATH}/$QL_CCR_CUSTOM_SCRIPT > /dev/null \
                    2>&1
                if [[ $? -ne 0 ]]; then
                        printf \
                            "$(gettext '%s: %s returned non-zero value')\n" \
                            "${PROG}" "${QL_CCR_CUSTOM_SCRIPT}"
                fi
        fi

	printf "$(gettext '%s: Completed execution of %s')\n" ${PROG} \
	    ${QL_CCR_CUSTOM_SCRIPT_PATH}/${QL_CCR_CUSTOM_SCRIPT} | logdbgmsg

	printf "$(gettext '%s: Completed execution')\n" ${PROG} | logdbgmsg

	return 0
}
main $*
return $?
