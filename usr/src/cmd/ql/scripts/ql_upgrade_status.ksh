#! /bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#pragma ident	"@(#)ql_upgrade_status.ksh	1.6	08/08/01 SMI"
#
# ql_upgrade_status.ksh
#
# ql_upgrade_state
#
# This script can be executed in both cluster and non-cluster modes.
# It displays the status of Dual-mode upgrade:
#		
#	UPGRADE_COMPLETE_STATE
#
#	QL_DATA_FILE does not exist and the upgrade state in the CCR is 0.
#
#	NO_UPGRADE_STATE
#
#	There is no upgrade in progress. We determine this by the
#	non-existance of QL_DATA_FILE.
#
# Apart from the above two states, if QL_DATA_FILE exists on the cluster
# node the following states are defined based on the partition to which
# the node belongs to and if they are in cluster/non-cluster mode. 
#	
# When run in cluster mode on a node in partition B:
#
#	UPGRADE_PENDING_COMPLETE_STATE
#
#	Definition: QL_DATA_FILE exists and the upgrade state in the CCR is
#	QL_UPGRADE_STATE_2. We will have this state until all the nodes of
#	partition B join the cluster.
#
# 	UPGRADE_BEGIN_STATE 
#
#	Definition: The BEGIN command has been run and has
#	created QL_DATA_FILE_PATH/QL_DATA_FILE on all the nodes of the
#	cluster. The upgrade_partition command has not yet been run on
#	nodes of partition A. We determine this state from the existance of 
#	QL_DATA_FILE and non existance of QL_STATUS_FILE.
#
#	UPGRADE_PARTA_STATE	
#
#	Definition: The upgrade_partition command has been run on parititon A.
#	We determine this state from the existance of QL_DATA_FILE and
#	QL_STATUS_FILE.
#
# When run in non-cluster mode on a node in partition B:
#
#	UPGRADE_PARTA_STATE
#	UPGRADE_ERR_STATE
#	
#	Definition: If either of QL_DATA_FILE and QL_STATUS_FILE does not exist
#	the node should not be in non-cluster mode. The upgrade state
#	is in error.
#
# When run in cluster mode on a node in partition A:
#
#	UPGRADE_PENDING_COMPLETE_STATE
#	UPGRADE_STATE_PRE_PARTB_SHUTDOWN
#
#	defn: partition A has been upgraded and has formed the cluster. The
#	services have not yet been started on partition A. cluster formed
#	by partition B nodes is still up.
#
#	UPGRADE_ERR_STATE
#
#	Definition: QL_DATA_FILE exists and CCR upgrade state = QL_UPGRADE_STATE_1
#
# When run in non-cluster mode on a node in partition A:
#
#	UPGRADE_BEGIN_STATE
#	UPGRADE_PARTA_STATE
#	UPGRADE_ERR_STATE
#

##################################################################
#
# Variable globals
#
#################################################################
integer -r SC_TRUE=1
integer -r SC_FALSE=0

typeset -r CMD_PWD=/usr/bin/pwd

typeset HOSTNAME_CMD=/usr/bin/hostname
typeset CLUSTER_LIB_PATH=/usr/cluster/bin
typeset QL_UTIL_PATH=/usr/cluster/lib/scadmin/ql
typeset QL_UTIL_FILE=ql_util

#program name and args list. Used for log messages.
if [[ -z $PROG ]]; then
	typeset -r PROG=${0##*/}
fi

#
# The QL_UPGRADE_DIR is the directory from which this script is run.
# Make sure that it is set with an absolute path and remove
# trailing dots.
#
typeset -r QL_CWD=$(${CMD_PWD})
typeset QL_UPGRADE_DIR=$(dirname $0)

if [[ "${QL_UPGRADE_DIR}" != /* ]]; then
	QL_UPGRADE_DIR=${QL_CWD}/${QL_UPGRADE_DIR}
fi

while [[ "${QL_UPGRADE_DIR##*/}" = "." ]]
do
	QL_UPGRADE_DIR=$(dirname ${QL_UPGRADE_DIR})
done

# Paths.
typeset qlupgradedir=${QL_UPGRADE_DIR}

integer PROG_ERR=1

#
# Define the states
#
integer NO_UPGRADE_STATE=2
integer UPGRADE_COMPLETE_STATE=3
integer UPGRADE_PENDING_COMPLETE_STATE=4
integer UPGRADE_BEGIN_STATE=5
integer UPGRADE_STATE_PRE_PARTB_SHUTDOWN=6
integer UPGRADE_PARTA_STATE=7
integer UPGRADE_ERR_STATE=8

#
# CCR infrastructure file.
#
typeset CCR_INF_FILE=/etc/cluster/ccr/global/infrastructure

if [[ ! -f ${CCR_INF_FILE} ]]; then
	CCR_INF_FILE=/etc/cluster/ccr/infrastructure
fi

#
# Upgrade state key in the infrastructure file.
# If this changes in /usr/src/cmd/ql/lib/ql_ccr.h
# it has to get reflected here.
#
typeset CCR_UPGRADE_STATE_KEY=cluster.upgrade.ql.ql_state

#
# Possible values for upgrade state key in the CCR.
# If this changes in /usr/src/cmd/ql/lib/ql_ccr.h
# it has to get reflected here.
#
typeset CCR_UPGRADE_STATE_VAL_0=0
typeset CCR_UPGRADE_STATE_VAL_1=1
typeset CCR_UPGRADE_STATE_VAL_2=2

#
# The status to report
#
integer state 

#####################################################
#
# loadlib() libname
#
#	Load the named include file.
#
#	Return:
#		zero		Success
#		non-zero	Failure
#
###############################################################
loadlib()
{
	typeset libname=$1

	# Load the library
	. ${libname}

	if [[ $? != 0 ]]; then
		printf "$(gettext '%s:  Unable to load \"%s\"')\n" "${PROG}" \
		    "${libname}" >&2
		return 1
	fi
}

##################################################################
#
# print_state state
#
#	prints the message based on the value of $state
#	
#	Return:
#		None
#
##################################################################
print_state()
{
	integer cstate

	cstate=$1

	if [[ $cstate -eq $NO_UPGRADE_STATE ]]; then

		printf "$(gettext '%s: Upgrade not in progress')\n" "${PROG}" \
		    | logmsg

	elif [[ $cstate -eq  $UPGRADE_COMPLETE_STATE ]]; then

		printf "$(gettext '%s: Upgrade has completed')\n" "${PROG}" \
		    | logmsg

	elif [[ $cstate -eq $UPGRADE_PENDING_COMPLETE_STATE ]]; then

		printf "$(gettext '%s: Upgrade is in the last phase.')\n" \
		    "${PROG}" | logmsg
		printf "$(gettext '%s: Waiting for some nodes to join')\n" \
		    "${PROG}" | logmsg

	elif [[ $cstate -eq $UPGRADE_BEGIN_STATE ]]; then

		printf "$(gettext '%s: Upgrade has begun.')\n" "${PROG}" \
		    | logmsg

	elif [[ $cstate -eq $UPGRADE_STATE_PRE_PARTB_SHUTDOWN ]]; then

		printf "$(gettext '%s: Upgrade is in progress.')\n" "${PROG}" \
		    | logmsg
		printf \
		    "$(gettext '%s: The second partition is not yet shut down')\n"\
		    "${PROG}" | logmsg

	elif [[ $cstate -eq $UPGRADE_PARTA_STATE ]]; then

		printf "$(gettext '%s: Upgrade is in progress.')\n" "${PROG}" \
		    | logmsg
		printf "$(gettext '%s: Upgrade preparation on the first partition has been run.')\n" "${PROG}" | logmsg

	elif [[ $cstate -eq $UPGRADE_ERR_STATE ]]; then

		printf \
		   "$(gettext '%s: Upgrade has encountered a fatal error.')\n" \
		   "${PROG}" | logerr
	fi  
}

#####################################################################
#
# get_state
#
#	Gets the current state of Dual-partition upgrade. 
#
#	Return:
#	Failure:
#		PROG_ERR
#	Success:
#		NO_UPGRADE_STATE
#		UPGRADE_COMPLETE_STATE
#		UPGRADE_PENDING_COMPLETE_STATE
#		UPGRADE_BEGIN_STATE
#		UPGRADE_STATE_PRE_PARTB_SHUTDOWN
#		UPGRADE_PARTA_STATE
#		UPGRADE_ERR_STATE
#
#####################################################################
get_state()
{
	# flags.
	integer found=${SC_FALSE}
	integer in_partition_a=${SC_FALSE}
	integer in_partition_b=${SC_FALSE}
	integer cluster_mode=${SC_TRUE}

	integer no_ccr_upgrade_state=${SC_FALSE}
	integer in_ccr_state_0=${SC_FALSE}
	integer in_ccr_state_1=${SC_FALSE}
	integer in_ccr_state_2=${SC_FALSE}

	integer data_file_exists=${SC_TRUE}

	# partition B.
	typeset partition_b_nodes

	# partition A.
	typeset partition_a_nodes

	# local variable.
	typeset hostname
	typeset line
	typeset node
	integer count=0

	#
	# This script should be executed from the cdrom image Tools
	# directory or from /usr/cluster/bin
	#
	if [[ "${qlupgradedir##*/}" == "lib" ]]; then
		qllibpath=$QL_TMP_DIR
		from_cdrom=${SC_TRUE}
	else
		qllibpath=${QL_UTIL_PATH}
	fi

	# Load common functions.
	if [[ $from_cdrom -eq ${SC_TRUE} ]]; then
		loadlib ${qlupgradedir}/${QL_UTIL_FILE} || return 1
	else
		loadlib ${QL_UTIL_PATH}/${QL_UTIL_FILE} || return 1
	fi


	if [[ $? -ne 0 ]]; then
		return $PROG_ERR
	fi 	

	#
	# Global zone privilege check. If we are running in a non-global
        # we exit here.
        #
	sc_zones_check

	#
	# check to see if we are root.
	#
	verify_isroot

	if [[ $? -ne 0 ]]; then
		return $PROG_ERR
	fi

	#
	# check if command exists
	#
	if [[ ! -x /usr/sbin/clinfo ]]; then
		printf "$(gettext '%s: Could not find %s')\n" "${PROG}" \
		    "clinfo" | logerr
		return $PROG_ERR
	fi

	#
	# check for cluster mode.
	#
	/usr/sbin/clinfo > /dev/null 2>&1

	if [[ $? -ne 0 ]]; then

		cluster_mode=${SC_FALSE}

		printf "$(gettext '%s: Node in non-cluster mode')\n" \
		    "${PROG}" | logdbgmsg
	else
		printf "$(gettext '%s: Node in cluster mode')\n" "${PROG}" \
		    | logdbgmsg
	fi
		
	#
	# check if $QL_DATA_FILE exists.
	#
	if [[ ! -f ${QL_DATA_FILE_PATH}/${QL_DATA_FILE} ]]; then

		printf "$(gettext '%s: File %s does not exist')\n" \
		    "${PROG}" "${QL_DATA_FILE_PATH}/${QL_DATA_FILE}" | logdbgmsg

		data_file_exists=${SC_FALSE}
	fi

	#
	# check the CCR for upgrade state.
	#
	if [[ ! -f ${CCR_INF_FILE} ]]; then

		printf "$(gettext '%s: File %s does not exist')\n" \
		    "${PROG}" "${CCR_INF_FILE}" | logdbgmsg

		return $PROG_ERR
	fi

	/usr/bin/grep ${CCR_UPGRADE_STATE_KEY} ${CCR_INF_FILE} > /dev/null 2>&1

	if [[ $? -ne 0 ]]; then
		no_ccr_upgrade_state=${SC_TRUE}
	fi

	/usr/bin/grep ${CCR_UPGRADE_STATE_KEY} ${CCR_INF_FILE} | \
	    /usr/bin/grep ${CCR_UPGRADE_STATE_VAL_0} > /dev/null 2>&1

	#
	# CCR_UPGRADE_STATE_KEY=CCR_UPGRADE_STATE_VAL_0
	#
	if [[ $? -eq 0 ]]; then
		in_ccr_state_0=${SC_TRUE}
	fi

	if [[ $data_file_exists -ne ${SC_TRUE} ]]; then
		
		if [[ $no_ccr_upgrade_state -eq ${SC_TRUE} ]]; then
			return $NO_UPGRADE_STATE
		elif [[ $in_ccr_state_0 -eq ${SC_TRUE} ]]; then
			return $UPGRADE_COMPLETE_STATE
		else
			return $UPGRADE_ERR_STATE
		fi
	fi 

	#
	# $QL_DATA_FILE exists. Upgrade is in progress.
	#
	/usr/bin/grep ${CCR_UPGRADE_STATE_KEY} ${CCR_INF_FILE} | \
	    /usr/bin/grep ${CCR_UPGRADE_STATE_VAL_1} > /dev/null 2>&1

	#
	# CCR_UPGRADE_STATE_KEY=CCR_UPGRADE_STATE_VAL_1
	#
	if [[ $? -eq 0 ]]; then

		if [[ $in_ccr_state_0 -eq ${SC_TRUE} ]]; then
			#
			# CCR is corrupt!
			#
			printf "$(gettext '%s: The Upgrade information in the CCR is corrupt')\n" "${PROG}"
			
			return $PROG_ERR
		fi
		in_ccr_state_1=${SC_TRUE}
	fi
			
	/usr/bin/grep ${CCR_UPGRADE_STATE_KEY} ${CCR_INF_FILE} | \
	    /usr/bin/grep ${CCR_UPGRADE_STATE_VAL_2} > /dev/null 2>&1

	#
	# CCR_UPGRADE_STATE_KEY=CCR_UPGRADE_STATE_VAL_2
	#
	if [[ $? -eq 0 ]]; then

		if [[ $in_ccr_state_0 -eq ${SC_TRUE} ]]; then
			#
			# CCR is corrupt!
			#
			printf "$(gettext '%s: CCR is corrupt')\n" "${PROG}"
			
			return $PROG_ERR
		fi

		if [[ $in_ccr_state_1 -eq ${SC_TRUE} ]]; then
			#
			# CCR is corrupt!
			#
			printf "$(gettext '%s: CCR is corrupt')\n" "${PROG}"
			
			return $PROG_ERR
		fi
		in_ccr_state_2=${SC_TRUE}
	fi

	#
	# Read the nodes of partition A from  $QL_DATA_FILE_PATH/$QL_DATA_FILE.
	#
	partition_a_nodes=`read_partition_A`

	if [[ $? -ne 0 ]]; then
		return 1
	fi

	#
	# Read the nodes of partition B from $QL_DATA_FILE_PATH/$QL_DATA_FILE.
	#
	partition_b_nodes=`read_partition_B`

	if [[ $? -ne 0 ]]; then
		return 1
	fi

	#
	# Read the root node of partition A.
	#
	partition_a_root_node=`read_partition_A_root`

	if [[ $? -ne 0 ]]; then
		return 1
	fi

	#
	# Read the root node of partition B.
	#
	partition_b_root_node=`read_partition_B_root`

	if [[ $? -ne 0 ]]; then
		return 1
	fi

	#
	# check for duplicates.
	#
	duplicate "${partition_a_nodes} ${partition_b_nodes}"

	if [[ $? -ne 0 ]]; then
		printf "$(gettext '%s: Bad -n option')\n" "${PROG}"
		return 1
	fi

	#
	# check if the command exists.
	#
	if [[ ! -x ${HOSTNAME_CMD} ]]; then
                printf "$(gettext '%s: Could not find %s')\n" "${PROG}" \
		    "${HOSTNAME_CMD}" | logerr
                return $PROG_ERR
        fi

	#
	# get the hostname for this node.
	#	
	hostname=`${HOSTNAME_CMD}`

	found=${SC_FALSE}
	#
	# Check to see if the current node is part of partition B.
	#
	for node in $partition_b_nodes
	do
		if [[ $hostname = $node ]]; then
			found=${SC_TRUE}
			break
		fi
	done

	if [[ $found -eq ${SC_TRUE} ]]; then
		
		if [[ $cluster_mode -eq ${SC_TRUE} ]]; then
			#
			# current host is in partition B and in cluster mode.
			#
			if [[ $in_ccr_state_2 -eq ${SC_TRUE} ]]; then
				return $UPGRADE_PENDING_COMPLETE_STATE
			fi

			if [[ ! -f ${QL_STATUS_FILE_PATH}/${QL_STATUS_FILE} ]]; then
				return $UPGRADE_BEGIN_STATE
			else
				return $UPGRADE_PARTA_STATE
			fi
		else
			#
			# current node is in partition B and in non-cluster
			# mode.
			#
			if [[ -f ${QL_STATUS_FILE_PATH}/${QL_STATUS_FILE} ]]; then
				return $UPGRADE_PARTA_STATE
			else
				return $UPGRADE_ERR_STATE
			fi
		fi
	fi

	found=${SC_FALSE}
	#
	# Check to see if the current node is part of partition A.
	#
	for node in $partition_a_nodes
	do
		if [[ $hostname = $node ]]; then
			found=${SC_TRUE}
			break
		fi
	done

	if [[ $found -eq ${SC_TRUE} ]]; then
		
		if [[ $cluster_mode -eq ${SC_TRUE} ]]; then
			#
			# Current host is in partition A and in cluster mode.
			#
			if [[ $in_ccr_state_2 -eq ${SC_TRUE} ]]; then
				return $UPGRADE_PENDING_COMPLETE_STATE
			fi

			if [[ $in_ccr_state_1 -eq ${SC_TRUE} ]]; then
				return $UPGRADE_STATE_PRE_PARTB_SHUTDOWN
			else
				return $UPGRADE_ERR_STATE
			fi
		else
			#
			# Current node is in partition A and in non-cluster
			# mode.
			#
			if [[ $in_ccr_state_2 -eq ${SC_TRUE} ]]; then
				return $UPGRADE_ERR_STATE
			fi
			if [[ $in_ccr_state_1 -eq ${SC_TRUE} ]]; then
				return $UPGRADE_PARTA_STATE
			else
				return $UPGRADE_BEGIN_STATE
			fi
		fi
	else
		#
		# The Current node was not found in either partition A or B.
		#
		printf \
		   "$(gettext '%s: Current node not part of any partition')\n" \
		   "${PROG}"
			
		return $PROG_ERR
	fi
	#
	# Should not reach here
	#
	printf "$(gettext '%s: Bad state')\n" "${PROG}"   
	return $PROG_ERR
}
get_state
state=$?
print_state $state
return $state
