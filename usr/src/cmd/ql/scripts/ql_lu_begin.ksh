#! /bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#pragma	ident	"@(#)ql_lu_begin.ksh	1.2	08/05/20 SMI"
#
# ql_lu_begin.ksh
#
# ql_lu_begin -R mount_point -A|-B [-i] [-c]
#
# Parameter:
# 	mount_point	The mount point of the alternate boot environment
#
# Options:
#
#	-R	Specifies the mount point. When only this option is specified
#		then the mount point is validated, then the boot environment
#		is unmounted and then activated. Finally rebooted.
#
#	-i	Interactive mode.Does all the same things as -R but does
#		not reboot the node.
#
#	-A	Indicates that the script is being run on partition A nodes.
#
#	-B	Indicates that the script is being run on partition B nodes.
#
#	-c	Check mode. Used to just validate the mount point. Will check
#		that the alternate boot environment is mounted on the mount
#		point.
#		If -c and -i both are used, then -i is ignored.
# 
# The script will map the mount point to the alternate boot environment.
# It expects the alternate boot environment to be mounted on the mount point
# specified. Then it will unmount the boot environment, activate it for next
# boot and then reboot the nodes.
#

###############################################################
#
# Variable globals
#
###############################################################
integer -r SC_TRUE=1
integer -r SC_FALSE=0

typeset QL_UTIL_PATH=/usr/cluster/lib/scadmin/ql/
typeset QL_UTIL_FILE=ql_util
typeset QL_TMP_DIR=/var/cluster/run

# Mount point for the alternate boot environment
typeset MOUNT_POINT

# Boot Environment mounted on the mount point
typeset NEW_BE

integer INTERACTIVE_MODE=${SC_FALSE}
integer CHECK_MOUNT_POINT_MODE=${SC_FALSE}
integer PART_A_MODE=${SC_FALSE}
integer PART_B_MODE=${SC_FALSE}

# Program name and args list
if [[ -z $PROG ]]; then
	typeset -r PROG=${0##*/}
fi

###############################################################
#
# print_usage
#
#       Print usage message to stderr
#
###############################################################
print_usage()
{
	echo "$(gettext 'usage'): ${PROG} -R <mount point> [-A] [-B] [-i] [-c] " \
	    >&2

}

################################################################
#
# loadlib() libname
#
#	Load the named include file.
#
#	Return:
#		zero		Success
#		non-zero	Failure
#
################################################################
loadlib()
{
	typeset libname=$1

	# Load the library

	. ${libname}

	if [[ $? != 0 ]]; then
		printf "$(gettext '%s:  Unable to load \"%s\"')\n" "${PROG}" \
		    "${libname}" >&2
		return 1
	fi

	return 0
}

################################################################
# validate_mount_point()
#
# This function will validate that the mount point exists 
# and that the alternate boot environment is mounted.
#
#	Return:
#		Zero:		Success
#		Non Zero:	Failure
#				Failure also indicates that
#				the mount point is not valid.
#
################################################################
validate_mount_point()
{
	#
	# Make sure that MOUNT_POINT is set before this
	# function is called.
	#
	if [[ -z ${MOUNT_POINT} ]]; then
		printf "$(gettext '%s: Mount point passed in NULL')\n" \
		    "${PROG}" | logerr
		return 1
	fi

	#
	# Check if the mount point is a valid mount point.
	# A valid mount point is one on which a boot environment
	# is mounted on and that boot environment has SC bits
	# installed.
	#
	if [[ ! -d "${MOUNT_POINT}/usr/cluster/bin" ]]; then
		printf "$(gettext '%s: %s is not a valid mount point')\n" \
		    "${PROG}" "${MOUNT_POINT}" | logdbgmsg
		return 1
	fi

	printf "$(gettext '%s: %s is a valid mount point')\n" \
	    "${PROG}" "${MOUNT_POINT}" | logdbgmsg

	#
	# Valid mount point
	#
	return 0
}

################################################################
#
# map_mount_point_to_be()
#
# This function will map the the mount point to the alternate
# boot environment. It expects that the boot environment is
# already mounted. The function will finally populate $NEW_BE
# with the name of the alternate boot environment.
#
#	Return:
#		Zero:		Success
#		Non Zero:	Failure
#
################################################################
map_mount_point_to_be()
{
	typeset lumount_op="/tmp/lumount_op.$$"
	typeset token_1=
	typeset token_3=
	typeset retval=0

	NEW_BE=

	#
	# Make sure that MOUNT_POINT is set before this
	# function is called.
	#
	if [[ -z ${MOUNT_POINT} ]]; then
		printf "$(gettext '%s: Mount point passed in NULL')\n" \
		    "${PROG}" | logerr
		return 1
	fi

	#
	# Check that the lumount command is present.
	#
	if [[ ! -x /usr/sbin/lumount ]]; then
		printf "$(gettext '%s: Could not find %s')\n" "${PROG}" \
		    "/usr/sbin/lumount" | logerr
		return 1
	fi

	#
	# Map the mount point to the boot environment
	#
	/usr/sbin/lumount > $lumount_op
	retval=$?
	if [[ "${retval}" != 0 ]]; then
		printf \
		    "$(gettext '%s: lumount failed with return value %d')\n" \
		    "${PROG}" "${retval}" | logerr
		rm -f ${lumount_op}
		return 1
	fi
	
	#
	# Read the lumount output and store the boot environment.
	# lumount output looks like
	# c0t0d0s0 on /
	# new_be  on /mnt/altroot
	#
	# hence the first token is the name of the boot environment and
	# the third token is the name of the mount point.
	#
	while read line
	do
        	token_1=`echo $line | awk '{print $1}'`
        	token_3=`echo $line | awk '{print $3}'`

        	if [[ "${token_3}" == "${MOUNT_POINT}" ]]; then
                	NEW_BE="${token_1}"
                	break
        	fi
	done < ${lumount_op}

	#
	# Delete the temporary file
	#
	rm -f ${lumount_op}

	#
	# Check that the boot environment was found
	#
	if [[ -z "${NEW_BE}" ]]; then
		printf "$(gettext \
		    '%s: %s could not be mapped to a boot environment')\n" \
		    "${PROG}" "${MOUNT_POINT}" | logerr
		return 1
	fi

	printf "$(gettext \
	    '%s: Mount Point = %s Boot Environment = %s')\n" \
	    "${PROG}" "${MOUNT_POINT}" "${NEW_BE}" | logdbgmsg

	#
	# Successfully mapped the mount point
	# to boot environment
	#
	return 0
}

################################################################
#
# store_mount_point_info()
#
#	The function will create the QL_LU_CONTINUE file in the
#	alternate boot environment. Then it will store the
#	mount point and the boot environment name.
#
# Note:
#	Its expected that MOUNT_POINT and NEW_BE will be
#	set before calling this function.
#
#	Return:
#		Zero:		Success
#		Non Zero:	Failure
#
################################################################
store_mount_point_info()
{
	typeset retval

	#
	# Check that the touch command is present.
	#
	if [[ ! -x /usr/bin/touch ]]; then
		printf "$(gettext '%s: Could not find %s')\n" "${PROG}" \
		    "/usr/bin/touch" | logerr
		return 1
	fi

	#
	# Create the QL_LU_CONTINUE file in the alternate
	# boot environment, so that at the next boot
	# QL will continue automatically
	#
	/usr/bin/touch "${MOUNT_POINT}/${QL_LU_CONTINUE}"
	retval=$?
	if [[ ${retval} != 0 ]]; then
		printf "$(gettext '%s: Failed to create file %s Retval=%d')\n" \
		    "${PROG}" "${MOUNT_POINT}/${QL_LU_CONTINUE}" \
		    "${retval}" | logerr
		return 1
	fi

	printf "$(gettext '%s: Created file %s')\n" \
	    "${PROG}" "${MOUNT_POINT}/${QL_LU_CONTINUE}" | logdbgmsg


	#
	# Store the mount point and the alternate boot environment
	# in the file so that in the next boot the mount point will
	# be available to QL utilities.
	#
	echo "${MOUNT_POINT}" "    " "${NEW_BE}" \
	    > "${MOUNT_POINT}/${QL_LU_CONTINUE}"
	retval=$?
	if [[ ${retval} != 0 ]]; then
		printf "$(gettext \
		    '%s: Failed to store mount point and boot environment. Retval %s')\n" \
		    "${PROG}" "${retval}" | logerr
		return ${retval}
	fi

	#
	# Success
	#
	return 0
}

################################################################
#
# copy_files_from_oldbe()
#
# Copy the ccr and QL related files from the old boot environment
# to the new boot environment.
#
#	Return:
#		Zero:		Success
#		Non Zero:	Failure
#
################################################################
copy_files_from_oldbe()
{
	typeset retval

	#
	# Delete old files if they are present on the alternate
	# boot environment.
	#

	if [[ -d  ${MOUNT_POINT}/etc/cluster/ccr ]]; then
		/usr/bin/rm -rf ${MOUNT_POINT}/etc/cluster/ccr \
		    > /dev/null 2>&1
	fi

	if [[ -d ${MOUNT_POINT}${QL_CCR_BACKUP} ]]; then
		/usr/bin/rm -rf ${MOUNT_POINT}${QL_CCR_BACKUP} \
		    > /dev/null 2>&1
	fi

	if [[ -f ${MOUNT_POINT}${QL_DATA_FILE_PATH}/${QL_DATA_FILE} ]]; then
		/usr/bin/rm -f \
		    ${MOUNT_POINT}${QL_DATA_FILE_PATH}/${QL_DATA_FILE} \
		    > /dev/null 2>&1
	fi

	#
	# Copy the /etc/cluster/ccr
	#
	/usr/bin/cp -r /etc/cluster/ccr ${MOUNT_POINT}/etc/cluster/ccr
	retval=$?
	if [[ ${retval} != 0 ]]; then
		printf "$(gettext \
		    '%s: Failed to copy %s with error code %s')\n"
		    "${PROG}" "/etc/cluster/ccr" "${retval}" | logerr
		return 1
	fi

	#
	# Copy the ${QL_CCR_BACKUP}
	#
	/usr/bin/cp -r ${QL_CCR_BACKUP} ${MOUNT_POINT}${QL_CCR_BACKUP}
	retval=$?
	if [[ ${retval} != 0 ]]; then
		printf "$(gettext \
		    '%s: Failed to copy %s with error code %s')\n"
		    "${PROG}" "${QL_CCR_BACKUP}" "${retval}" | logerr
		return 1
	fi

	#
	# Copy the ${QL_DATA_FILE_PATH}/${QL_DATA_FILE}
	#
	/usr/bin/cp ${QL_DATA_FILE_PATH}/${QL_DATA_FILE} \
	    ${MOUNT_POINT}${QL_DATA_FILE_PATH}/${QL_DATA_FILE}
	retval=$?
	if [[ ${retval} != 0 ]]; then
		printf "$(gettext \
		    '%s: Failed to copy %s with error code %s')\n"
		    "${PROG}" "${QL_DATA_FILE}" "${retval}" | logerr
		return 1
	fi

	
	#
	# All files have been successfully copied from old 
	# boot environment to new boot environment.
	#
	printf "$(gettext \
	'%s: File copying successful')\n" "${PROG}" | logdbgmsg

	#
	# Success
	#
	return 0
}

################################################################
# backup_vfstab_on_newbe()
#
# This function will backup the vfstab file present on the new
# boot environment. We do not copy the vfstab file of the old
# boot environment becuase the the two vfstab files may be 
# different as a result of upgrading software on the new boot
# environment.
#
#	Return:
#		Zero:		Success
#		Non Zero:	Failure
#
################################################################
backup_vfstab_on_newbe()
{
	typeset retval

	if [[ ! -f ${MOUNT_POINT}/etc/vfstab ]]; then
		printf "$(gettext '%s: Did not find %s')\n" \
		    "${PROG}" "${MOUNT_POINT}/etc/vfstab" | logerr
		return 1
	fi

	cp ${MOUNT_POINT}/etc/vfstab ${MOUNT_POINT}${QL_VFSTAB_BACKUP}
	retval=$?

	if [[ ${retval} != 0 ]]; then
		printf "$(gettext '%s: Failed to back up %s')\n" \
		    "${PROG}" "${MOUNT_POINT}/etc/vfstab" | logerr
		return 1
	fi

	printf "$(gettext '%s: Successfully copied %s to %s')\n" \
	    "${PROG}" "${MOUNT_POINT}/etc/vfstab" \
	    "${MOUNT_POINT}${QL_VFSTAB_BACKUP}" | logdbgmsg

	#
	# Success
	#
	return 0
}

################################################################
#
# handle_fencing
#
#	This function will create the ${UPGRADE_FLAG_FILE} in
#	the new boot environment so that partition B nodes do
#	not get fenced in next boot.
#
# Note:
#	Its expected that MOUNT_POINT is set before calling
#	this function.
#
#	Return:
#		Zero:		Success
#		Non Zero:	Failure
#
################################################################
handle_fencing()
{
	
	#
	# Check that the touch command is present.
	#
	if [[ ! -x /usr/bin/touch ]]; then
		printf "$(gettext '%s: Could not find %s')\n" "${PROG}" \
		    "/usr/bin/touch" | logerr
		return 1
	fi

	#
	# Touch "$MOUNT_POINT/scnoreserveddisks". 
	# This is to make sure that we dont
	# fence off disks in partition B when
	# the nodes in partition A are still up.  
	#	
	/usr/bin/touch ${MOUNT_POINT}${UPGRADE_FLAG_FILE} 
	if [[ $? != 0 ]]; then
		printf "$(gettext '%s: Failed to create %s')\n" \
		    "${PROG}" "${MOUNT_POINT}${UPGRADE_FLAG_FILE}" \
		    | logerr
		return 1
	fi

	printf "$(gettext '%s: Created %s')\n" "${PROG}" \
	    "${MOUNT_POINT}${UPGRADE_FLAG_FILE}" \
	    | logdbgmsg

	#
	# Success
	#
	return 0
}

################################################################
#
# umount_be()
#
# This funtion will unmount the alternate boot environment
# The function expects that the boot environment is already mounted.
# The function should be called after populating the $NEW_BE 
#
#	Return:
#		Zero:		Success
#		Non Zero:	Failure
#
################################################################
umount_be()
{

	typeset retval

	#
	# Check that boot environment is not null
	#
	if [[ -z ${NEW_BE} ]]; then
		printf "$(gettext '%s: Boot environment NULL')\n" \
		    "${PROG}" | logerr
		return 1
	fi

	#
	# Check that the luumount command is present.
	#
	if [[ ! -x /usr/sbin/luumount ]]; then
		printf "$(gettext '%s: Could not find %s')\n" "${PROG}" \
		    "/usr/sbin/luumount" | logerr
		return 1
	fi

	#
	# Unmount the new boot environment
	#
	/usr/sbin/luumount -f -n "${NEW_BE}"
	retval=$?
	if [[ "${retval}" != 0 ]]; then
		printf "$(gettext \
		    '%s: luumount failed with return value %d')\n" \
		    "${PROG}" "${retval}" | logerr
		printf "$(gettext \
		    '%s: Failed to unmount boot environment %s')\n" \
		    "${PROG}" "${NEW_BE}" | logerr
		return 1

	fi

	printf "$(gettext \
	    '%s: Successfully unmounted the boot environment %s')\n" \
	    "${PROG}" "${NEW_BE}" | logdbgmsg

	#
	# Success
	#
	return 0

}

################################################################
#
# activate_be()
#
# This function will activate the alternate boot environment
# so that in next boot, the machine boots with this alternate
# boot environment.
#
#	Return:
#		Zero:		Success
#		Non Zero:	Failure
#
################################################################
activate_be()
{

	typeset retval

	#
	# Check that boot environment is not null
	#
	if [[ -z ${NEW_BE} ]]; then
		printf "$(gettext '%s: Boot environment NULL')\n" \
		    "${PROG}" | logerr
		return 1
	fi

	#
	# Check that the luactivate command is present.
	#
	if [[ ! -x /usr/sbin/luactivate ]]; then
		printf "$(gettext '%s: Could not find %s')\n" "${PROG}" \
		    "/usr/sbin/luactivate" | logerr
		return 1
	fi

	#
	# Activate the new boot environment
	#
	/usr/sbin/luactivate "${NEW_BE}"
	retval=$?
	if [[ "${retval}" != 0 ]]; then
		printf "$(gettext \
		    '%s: luactivate failed with return value %d')\n" \
		    "${PROG}" "${retval}" | logerr
		printf "$(gettext \
		    '%s: Failed to activate boot environment %s')\n" \
		    "${PROG}" "${NEW_BE}" | logerr
		return 1

	fi

	printf "$(gettext \
	    '%s: Successfully activated the boot environment %s')\n" \
	    "${PROG}" "${NEW_BE}" | logdbgmsg

	#
	# Success
	#
	return 0
}

################################################################
# reboot_node_in_new_be
#
# The function will restart the node. Will execute a init 6
# so that the node boots in the new boot environment. This
# function should be called after activating the new boot
# environment.
#
#	Return:
#		Zero:		Success
#		Non Zero:	Failure
#
################################################################
reboot_node_in_new_be()
{
	#
	# Check that the luactivate command is present.
	#
	if [[ ! -x /usr/sbin/init ]]; then
		printf "$(gettext '%s: Could not find %s')\n" "${PROG}" \
		    "/usr/sbin/init" | logerr
		return 1
	fi

	printf "$(gettext \
	    '%s: Rebooting local node in new boot environment %s')\n" \
	    "${PROG}" "${NEW_BE}" | logmsg
	/usr/sbin/init 6

	#
	# May not reach here
	#
	return 0
}

################################################################
#
# Main
#
# This is the first function from where the execution of the script
# starts (similar to a c program main). More details about what the
# script does is documented at the begining of this file.
#
################################################################
main()
{
	typeset retval=0

	#
	# load common functions.
	#
	if [[ -f ${QL_TMP_DIR}/${QL_UTIL_FILE} ]]; then
		loadlib ${QL_TMP_DIR}/${QL_UTIL_FILE} || return 1
	else
		loadlib ${QL_UTIL_PATH}/${QL_UTIL_FILE} || return 1 
	fi

	#
	# Global zone privilege check. If we are running in a non-global
        # we exit here.
        #
	sc_zones_check

	#
	# create the log file if it does not already exist.
	#
	createlog

	while getopts icR:AB c 2>/dev/null
	do
		case ${c} in

		i)
			INTERACTIVE_MODE=${SC_TRUE}
			;;

		c)
			CHECK_MOUNT_POINT_MODE=${SC_TRUE}
			;;
		R)
			MOUNT_POINT=`echo ${OPTARG} | sed -e 's/\/$//g'`
			;;
		A)
			PART_A_MODE=${SC_TRUE}
			;;
		B)	
			PART_B_MODE=${SC_TRUE}
			;;

		\?)	print_usage
			return 1
			;;

		*)	# bad option
			print_usage
			return 1
			;;

		esac
	done

	#
	# Make sure that mount point is passed as a parameter
	# and that the mount point is not / (root)
	#
	if [[ -z ${MOUNT_POINT} ]]; then
		print_usage
		return 1
	fi


	#
	# Both part A and part B can not be specifed at the
	# same time
	#
	if [[ PART_A_MODE -eq ${SC_TRUE} ]] && \
	    [[ PART_B_MODE -eq ${SC_TRUE} ]]; then
		print_usage
		return 1
	fi

	#
	# check if there are any spurious arguments.
	#
	shift $(($OPTIND - 1))

	if [[ ! -z "$*" ]]; then
		# bad usage.
		print_usage
		return 1
	fi


	printf "$(gettext '%s: Executing %s with mount point %s')\n" \
	    "${PROG}" "ql_lu_begin" "${MOUNT_POINT}"| logdbgmsg

	#
	# Make sure we are root.
	#
	verify_isroot

	#
	# Mask out SIGHUP and SIGINT
	#
	trap 'ign_sig' HUP INT

	#
	# check if clinfo command exists
	# 
	if [[ ! -x /usr/sbin/clinfo ]]; then
		printf "$(gettext '%s: Could not find %s')\n" "${PROG}" \
		    "clinfo" | logerr
		return 1
	fi

	#
	# check for cluster mode.
	#
	/usr/sbin/clinfo > /dev/null 2>&1

	if [[ $? -ne 0 ]]; then
		printf "$(gettext '%s: Node must be in cluster mode')\n" \
		    "${PROG}"
		return 1
	fi

	#
	# All basic checks have been done.
	# Now we are ready to do Quantum Leap
	# related Live Upgrade work
	#

	#
	# First we validate the mount point 
	#
	validate_mount_point
	retval=$?

	#
	# If we just want to validate the mountpoint, then
	# return the status.
	#
	if [[ ${CHECK_MOUNT_POINT_MODE} -eq ${SC_TRUE} ]]; then
		printf "$(gettext '%s: Ran in checkmode. Retval = %d')\n" \
		    "${PROG}" "${retval}" | logdbgmsg
		return ${retval}
	fi

	#
	# If mount point is invalid, no need to proceed.
	# Return error
	#
	if [[ ${retval} != 0 ]]; then
		printf "$(gettext '%s: Invalid mount point %s Retval = %d')\n" \
		    "${PROG}" "${MOUNT_POINT}" "${retval}" | logerr
		return ${retval}
	fi


	#
	# Map the mount point to the new boot environment
	#
	map_mount_point_to_be
	retval=$?
	if [[ ${retval} != 0 ]]; then
		return ${retval}
	fi

	#
	# Store the mount point and the boot environment
	# in QL_LU_CONTINUE file.
	#
	store_mount_point_info
	retval=$?
	if [[ ${retval} != 0 ]]; then
		return ${retval}
	fi

	#
	# Copy ccr and other QL related files from 
	# old boot environment to new boot environment
	#
	copy_files_from_oldbe
	retval=$?
	if [[ ${retval} != 0 ]]; then
		return ${retval}
	fi

	#
	# Take backup of vfstab on new boot environment.
	# We will not take backup of the vfstab on the old
	# boot environment because the vfstab file may have
	# changed on the new be as a result of upgrading
	# software.
	#
	backup_vfstab_on_newbe
	retval=$?
	if [[ ${retval} != 0 ]]; then
		return ${retval}
	fi

	#
	# Do tasks specific to partition A nodes
	#
	if [[ ${PART_A_MODE} -eq ${SC_TRUE} ]]; then
		#
		# Change the vfstab file to comment out
		# QFS filesystems which get mounted
		# at boot time.
		#
		change_vfstab ${MOUNT_POINT}/etc/vfstab
		if [[ $? != 0 ]]; then
			return 1
		fi

		#
		# Do fencing related activities on
		# nodes of partition A.
		#
		handle_fencing
		if [[ $? != 0 ]]; then
			return 1
		fi
	fi

	#
	# Unmount the new boot environment
	#
	umount_be
	retval=$?
	if [[ ${retval} != 0 ]]; then
		return ${retval}
	fi

	#
	# Activate the new boot environment
	#
	activate_be
	retval=$?
	if [[ ${retval} != 0 ]]; then
		return ${retval}
	fi

	if [[ $INTERACTIVE_MODE -eq ${SC_FALSE} ]]; then
		#
		# Reboot the node in the new boot environment
		#
		reboot_node_in_new_be
		retval=$?
		if [[ ${retval} != 0 ]]; then
			return ${retval}
		fi
	fi

	#
	# Success
	# May not reach here
	#
	return 0
}

main $*
return $?
