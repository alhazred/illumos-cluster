#! /bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#pragma ident	"@(#)upgrade_partition.ksh	1.4	08/05/20 SMI"
#
# upgrade_partition.ksh
#
# upgrade_partition [-i] [-R <mount_point>]
#
# This script is a wrapper that executes either upgrade_partition_a or
# upgrade_partition_b based on which node it is running on, i.e.
# if this is running on a node in partition A, it will execute the
# QL_PARTA_CMD or if it is running on a node in partition
# B it will run the QL_PARTB_CMD. 
#
# The upgrade process has to happen in order i.e. upgrade_partition has
# to be run on a node in partition A first and then on partititon B.
# When QL_PARTA_CMD runs, before it completes, it creates a file
# on all the nodes in partition B ($QL_STATUS_FILE). The existance
# of this file is checked for before we start executing QL_PARTB_CMD.  
#

##################################################
#
# Variable globals
#
##################################################
integer -r SC_TRUE=1
integer -r SC_FALSE=0

typeset HOSTNAME_CMD=/usr/bin/hostname

typeset QL_UTIL_FILE=ql_util
typeset QL_UTIL_PATH=/usr/cluster/lib/scadmin/ql

# Mount Point
typeset MOUNT_POINT

#
# Program name and args list
#
if [[ -z $PROG ]]; then
	typeset -r PROG=${0##*/}
fi


#####################################################
#
# print_usage
#
#       Print usage message to stderr
#
######################################################
print_usage()
{
        echo "$(gettext 'usage'): ${PROG} [-i] [-R <mount_point>]" >&2
}

#####################################################
# loadlib() libname
#
#       Load the named include file.
#
#       Return:
#               zero            Success
#               non-zero        Failure
#
#####################################################
loadlib()
{
	typeset libname=$1

	#
	# Load the library
	#
	. ${libname}

	if [[ $? != 0 ]]; then

		printf "$(gettext '%s:  Unable to load \"%s\"')\n" \
		    "${PROG}" "${libname}" >&2

		return 1

	fi
}

####################################################
#
# main
#
####################################################
main()
{
	# partition B.
	typeset partition_b_nodes

	# partition A.
	typeset partition_a_nodes

	# local variable.
	typeset hostname
	typeset node
	integer interactive_mode=${SC_FALSE}
	integer lu_mode=${SC_FALSE}
	typeset retval

	# load common functions.
	loadlib ${QL_UTIL_PATH}/${QL_UTIL_FILE} || return 1

	#
	# Global zone privilege check. If we are running in a non-global
        # we exit here.
        #
	sc_zones_check

	while getopts iR: c 2>/dev/null
	do
		case ${c} in
		i)      # Interactive mode.
			interactive_mode=${SC_TRUE}
			;;

		R)	# Live Upgrade mode
			lu_mode=${SC_TRUE}

			MOUNT_POINT=`echo ${OPTARG} | sed -e 's/\/$//g'`
			#
			# Make sure that mount point is passed as a parameter
			# and that the mount point is not / (root)
			#
			if [[ -z ${MOUNT_POINT} ]]; then
				print_usage
				return 1
			fi
			;;

		\?)     print_usage
			return 1
			;;

		*)      # bad option
			print_usage
			return 1
			;;

		esac
	done

	#
	# If LU mode is set, then ignore -i
	#
	if [[ ${lu_mode} -eq ${SC_TRUE} ]]; then
		interactive_mode=${SC_FALSE}
	fi

	#
	# Create the debug log if it does not already exist.
	#
	createlog
	
	#
	# Read the nodes of partition A from  $QL_DATA_FILE_PATH/$QL_DATA_FILE.
	#
	partition_a_nodes=`read_partition_A`

	if [[ $? -ne 0 ]]; then
		return 1
	fi

	#
	# Read the nodes of partition B from $QL_DATA_FILE_PATH/$QL_DATA_FILE.
	#
	partition_b_nodes=`read_partition_B`

	if [[ $? -ne 0 ]]; then
		return 1
	fi

	#
	# check for duplicates.
	#
	duplicate "${partition_a_nodes} ${partition_b_nodes}"

	if [[ $? -ne 0 ]]; then
		printf "$(gettext '%s: Bad -n option')\n" "${PROG}"
		return 1
	fi

	#
	# Mask out SIGHUP and SIGINT.
	#
	trap 'ign_sig' HUP INT

	#
	# check to see if we are root.
	#
	verify_isroot

	if [[ $? -ne 0 ]]; then
		return 1
	fi

	#
	# check if command exists.
	#
	if [[ ! -x /usr/sbin/clinfo ]]; then
		printf "$(gettext '%s: Could not find %s')\n" "${PROG}" \
		    "clinfo" | logerr
		return 1
	fi

	#
	# check for non-cluster mode in non LU scenario and 
	# cluster mode for LU scenario.
	#
	/usr/sbin/clinfo > /dev/null 2>&1
	retval=$?
	if [[ ${retval} -eq 0 && ${lu_mode} -eq ${SC_FALSE} ]]; then
		#
		# Node should be in non cluster mode when this script
		# is executed in a non LU scenario.
		#
                printf "$(gettext '%s Node must be in noncluster mode')\n" \
                    "${PROG}" | logerr
                return 1
        fi

	if [[ ${retval} -ne 0 && ${lu_mode} -eq ${SC_TRUE} ]]; then
		#
		# Node should be in cluster mode when this script is
		# executed in a LU scenario.
		#
                printf "$(gettext '%s Node must be in cluster mode')\n" \
                    "${PROG}" | logerr
                return 1
	fi


	#
	# check if command exists
	#
	if [[ ! -x ${HOSTNAME_CMD} ]]; then
		printf "$(gettext '%s: Could not find %s')\n" "${PROG}" \
		    "${HOSTNAME_CMD}" | logerr
		return 1
	fi

	hostname=`${HOSTNAME_CMD}`

	found=${SC_FALSE}
	#
	# Check to see if the current node is part of partition A.
	#
	for node in $partition_a_nodes
	do
		if [[ $hostname = $node ]]; then
			found=${SC_TRUE}
			break
		fi
	done

	#
	# If we are running on a node in partition A we run ${QL_LIB_PATH}/
	# ${QL_PARTA_CMD}
	#
	if [[ $found -eq ${SC_TRUE} ]]; then

		printf "$(gettext '%s: Invoking %s')\n" "${PROG}" \
		    "${QL_LIB_PATH}/${QL_PARTA_CMD}" | logdbgmsg

		if [[ ! -x ${QL_LIB_PATH}/${QL_PARTA_CMD} ]]; then
			printf \
			    "$(gettext '%s: The command %s does not exist')\n" \
			    "${PROG}" "${QL_LIB_PATH}/${QL_PARTA_CMD}" | logerr
			return 1
		fi

		if [[ $lu_mode -eq ${SC_TRUE} ]]; then
			${QL_LIB_PATH}/${QL_PARTA_CMD} -R ${MOUNT_POINT}
		elif [[ $interactive_mode -eq ${SC_TRUE} ]]; then
			${QL_LIB_PATH}/${QL_PARTA_CMD} -i
		else
			${QL_LIB_PATH}/${QL_PARTA_CMD}
		fi

		if [[ $? -ne  0 ]]; then
			printf "$(gettext \
			    '%s: The command %s returned error')\n" \
			    "${PROG}" "${QL_LIB_PATH}/${QL_PARTA_CMD}" | logerr
			return 1
		fi
		return 0
	fi

	#
	# Check to see if the current node is part of partition B.
	#
	for node in $partition_b_nodes
	do
		if [[ $hostname = $node ]]; then
			found=${SC_TRUE}
			break
		fi
	done

	#
	# If we are running on a node in partition B we run ${QL_LIB_PATH}/
	# ${QL_PARTB_CMD}
	#
	if [[ $found -eq ${SC_TRUE} ]]; then

		printf "$(gettext '%s: Invoking %s')\n" "${PROG}" \
		    "${QL_LIB_PATH}/${QL_PARTB_CMD}" | logdbgmsg

		if [[ ! -x ${QL_LIB_PATH}/${QL_PARTB_CMD} ]]; then
			printf \
			    "$(gettext '%s: The command %s does not exist')\n" \
			    "${PROG}" "${QL_LIB_PATH}/${QL_PARTB_CMD}" | logerr
		    return 1
		fi

		#
		# Before we execute we check here if $QL_PARTA_CMD has been
		# run. We need to check it only in non LU scenarios.
		#
		if [[ ${lu_mode} -eq ${SC_FALSE} ]] &&
		    [[ ! -f ${QL_STATUS_FILE_PATH}/${QL_STATUS_FILE} ]]; then
			printf "$(gettext \
			    '%s: The command %s has not been run')\n" \
			    "${PROG}" "${QL_LIB_PATH}/${QL_PARTA_CMD}" | logerr
		   return 1
		fi

		if [[ $lu_mode -eq ${SC_TRUE} ]]; then
			${QL_LIB_PATH}/${QL_PARTB_CMD} -R ${MOUNT_POINT}
		elif [[ $interactive_mode -eq ${SC_TRUE} ]]; then
			${QL_LIB_PATH}/${QL_PARTB_CMD} -i
		else
			${QL_LIB_PATH}/${QL_PARTB_CMD}
		fi

		if [[ $? -ne  0 ]]; then
			printf "$(gettext \
			    '%s: The command %s returned error')\n" \
			    "${PROG}" "${QL_LIB_PATH}/${QL_PARTB_CMD}" | logerr
			return 1
		fi
	else
		#
		# This node is not in partition A or B. Bail out with an error
		# message.
		#
		printf "$(gettext \
		    '%s: The node does not belong to a partition.')\n"\
		    "${PROG}"
		return 1
	fi
	#
	# We dont return here.
        #
}
#
# Does not return in the absence of errors.
#
main $*
