#! /bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#pragma ident	"@(#)split_mode_upgrade_recovery.ksh	1.9	08/05/20 SMI"
#
# split_mode_upgrade_recovery.ksh
#
# split_mode_upgrade_recovery
#
# This script is executed in non-cluster mode. It restores the file
# /etc/vfstab and the CCR files from the backup.
# This script also removes the files created during upgrade.
#

#####################################################
#
# Variable globals
#
#####################################################

typeset HOSTNAME_CMD=/usr/bin/hostname

typeset QL_UTIL_PATH=/usr/cluster/lib/scadmin/ql/
typeset QL_UTIL_FILE=ql_util

# Location of bootcluster in non greenline systems
typeset ORG_BOOTCLUSTER_NON_GREENLINE=/etc/init.d/bootcluster

# Location of the bootcluster in greenline systems
typeset ORG_BOOTCLUSTER_GREENLINE=/usr/cluster/lib/svc/method/bootcluster

# Location of the backeup original bootcluster
typeset ORG_BOOTCLUSTER_BACKUP=/etc/cluster/bootcluster.org

# Location of the backup QL bootcluster
typeset QL_BOOTCLUSTER_BACKUP=/etc/cluster/bootcluster.ql

# variable to store the Solaris Version
typeset -i OS_VERSION

if [[ -z $PROG ]]; then
	typeset -r PROG=${0##*/}
fi

#
# The QL_UPGRADE_DIR is the directory from which this script is run.
# Make sure that it is set with an absolute path and remove
# trailing dots.
#
typeset -r CMD_PWD=/usr/bin/pwd
typeset -r QL_CWD=$(${CMD_PWD})

typeset QL_UPGRADE_DIR=$(dirname $0)
if [[ "${QL_UPGRADE_DIR}" != /* ]]; then
        QL_UPGRADE_DIR=${QL_CWD}/${QL_UPGRADE_DIR}
fi
while [[ "${QL_UPGRADE_DIR##*/}" = "." ]]
do
        QL_UPGRADE_DIR=$(dirname ${QL_UPGRADE_DIR})
done

typeset qlupgradedir=${QL_UPGRADE_DIR}

#####################################################
#
# loadlib() libname
#
#	Load the named include file.
#
#	Return:
#		zero		Success
#		non-zero	Failure
#
#####################################################
loadlib()
{
	typeset libname=$1

	# Load the library
	. ${libname}

	if [[ $? != 0 ]]; then
		printf "$(gettext '%s: Unable to load \"%s\"')\n" \
		    "${PROG}" "${libname}" >&2
		return 1
	fi
}

#####################################################
#
# get_os_version
#
# This function will get the Solaris version.
# e.g.
# If run on a Solaris 10 machine, the function
# will output 10
#
# Return Value:
#	Always returns 0
#
#####################################################
get_os_version()
{
	#
	# expected output from "uname -r" is something like: 5.10
	# this sed expr discards everything before and including
	# the ".".
	#
	LC_ALL=C uname -r | sed -e 's/.*\.//'
	return 0
}


####################################################
#
# main
#
####################################################
main()
{
	typeset hostname

	#
	# This script should be executed from the cdrom image Tools/lib
	# directory or from /usr/cluster/lib/scadmin/ql
	#

	#
	# Load common functions
	#
        if [[ "${qlupgradedir##*/}" == "lib" ]]; then
		loadlib ${qlupgradedir}/${QL_UTIL_FILE} || return 1
        else
		loadlib $QL_UTIL_PATH/$QL_UTIL_FILE > /dev/null 2>&1
        fi

	#
	# Global zone privilege check. If we are running in a non-global
        # we exit here.
        #
	sc_zones_check

	if [[ $? -ne 0 ]]; then
		return 1
	fi

	#
	# Check to see if we are root.
	#
	verify_isroot

	if [[ $? -ne 0 ]]; then
		return 1
	fi

	#
	# Check if command exists.
	#
	if [[ ! -x /usr/sbin/clinfo ]]; then
		printf "$(gettext '%s: Could not find %s')\n" "${PROG}" \
		    "clinfo" | logerr
		return 1
	fi

	#
	# Check for non-cluster mode.
        #
        /usr/sbin/clinfo > /dev/null 2>&1

	if [[ $? -eq 0 ]]; then
		printf "$(gettext '%s: Node must be in non-cluster mode')\n" \
		    "${PROG}" | logerr
		return 1
	fi

	#
	# Restore /etc/vfstab.
	# 
	if [[ ! -f ${QL_VFSTAB_BACKUP} ]]; then

		printf "$(gettext '%s: Backup of /etc/vfstab not found')\n" \
		    "$PROG" | logerr
		return 1
	fi

	/usr/bin/cp ${QL_VFSTAB_BACKUP} /etc/vfstab > /dev/null 2>&1

	if [[ $? -ne 0 ]]; then

		printf \
		    "$(gettext '%s: Failed to copy from backup of vfstab')\n"\
		    "$PROG" | logerr
		return 1
	fi

	#
        # Get the Solaris Version
        #
        OS_VERSION="$(get_os_version)"

	#
	# Restore bootcluster for partition B nodes.
	# If we don't find the backed up copy of bootcluster,
	# we assume that the bootcluster on that node was not
	# modified.
	#
	if [[ ! -f ${ORG_BOOTCLUSTER_BACKUP} ]];then
		printf "$(gettext '%s: Bootcluster does not need recovery')\n" \
		    "$PROG" | logdbgmsg
	else
		if [[ OS_VERSION -gt 9 ]];then
			#
			# Greenline
			#
			/usr/bin/cp ${ORG_BOOTCLUSTER_BACKUP} \
			    ${ORG_BOOTCLUSTER_GREENLINE}
		else
			#
			# RC scripts
			#
			/usr/bin/cp ${ORG_BOOTCLUSTER_BACKUP} \
			    ${ORG_BOOTCLUSTER_NON_GREENLINE}
		fi

		if [[ $? -ne 0 ]]; then
			printf \
			    "$(gettext '%s: Failed to recover bootcluster')\n"\
			    "$PROG" | logerr
			return 1
		fi


	fi

	
	#
	# Restore CCR.
	#
	if [[ ! -d ${QL_CCR_BACKUP} ]]; then

		printf "$(gettext '%s: Backup of CCR not found')\n" "$PROG" \
		    | logerr
		return 1

	fi

	/usr/bin/rm -rf /etc/cluster/ccr > /dev/null 2>&1

	/usr/bin/mv ${QL_CCR_BACKUP} /etc/cluster/ccr > /dev/null 2>&1

	if [[ $? -ne 0 ]]; then
		printf "$(gettext '%s: Failed to copy from backup CCR')\n" \
		    "$PROG" | logerr
		return 1
	fi   

	#
	# Remove the data file for Dual-partition upgrade.
	#
	rm -f  ${QL_DATA_FILE_PATH}/${QL_DATA_FILE}

	#
	# Remove the status file created by QL_PARTA_CMD if it exists.
	#
	rm -f ${QL_STATUS_FILE_PATH}/${QL_STATUS_FILE}

	#
	# We may have created /scnoreservedisks to turn off fencing. Here
	# we delete that file.
	# 
	rm -f ${UPGRADE_FLAG_FILE}

	#
	# Remove the begin file lock
	#
	rm -f ${QL_BEGIN_RUN_LOCK_FILE}

	#
	# Remove the apply file lock
	#
	rm -f ${QL_APPLY_RUN_LOCK_FILE}

        #
        # Remove the CCR transformation file
        #
        rm -f ${QL_TRANSFORM_CCR_FILE}

        #
        # Remove the QL LU file
        #
        rm -f ${QL_LU_CONTINUE}


	#
	# Delete the backed up copies of bootcluster
	#
	/usr/bin/rm -f ${QL_BOOTCLUSTER_BACKUP}
	/usr/bin/rm -f ${ORG_BOOTCLUSTER_BACKUP}

	/usr/bin/rm -f ${QL_VFSTAB_BACKUP}
	/usr/bin/rm -rf ${QL_CCR_BACKUP}

	return 0
}
main $*
