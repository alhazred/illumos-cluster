#! /bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#pragma ident	"@(#)replace_bootcluster.ksh	1.3	08/05/20 SMI"
#
# replace_bootcluster
#
# This script will take the backup of the bootcluster file and then replace
# the bootcluster file with the QL bootcluster file. This new
# bootcluster file will prevent the nodes from booting up in cluster mode.
#
# The debug logs generated are placed in QL_UPGRADE_LOG.
#

# variable to store the Solaris Version
typeset -i OS_VERSION

typeset QL_UTIL_PATH=/var/cluster/run
typeset QL_UTIL_FILE=ql_util

# Location of bootcluster in non greenline systems
typeset ORG_BOOTCLUSTER_NON_GREENLINE=/etc/init.d/bootcluster

# Location of the bootcluster in greenline systems
typeset ORG_BOOTCLUSTER_GREENLINE=/usr/cluster/lib/svc/method/bootcluster

# Location of the backeup original bootcluster
typeset ORG_BOOTCLUSTER_BACKUP=/etc/cluster/bootcluster.org

# Location of the backup QL bootcluster
typeset QL_BOOTCLUSTER_BACKUP=/etc/cluster/bootcluster.ql

# Location of temporary bootcluster
typeset TMP_BOOTCLUSTER=/var/cluster/run/bootcluster

# Location of the bootcluster file on the node
typeset ORG_BOOTCLUSTER=


if [[ -z $PROG ]]; then
	typeset -r PROG=${0##*/}
fi

###############################################################
# loadlib() libname
#
#	Load the named include file.
#
#	Return:
#		zero            Success
#		non-zero        Failure
#
###############################################################
loadlib()
{
	typeset libname=$1

	#
	# Load the library
	#
	. ${libname}

	if [[ $? != 0 ]]; then

		printf "$(gettext '%s:  Unable to load \"%s\"')\n" \
		    "${PROG}" "${libname}" >&2

		return 1

	fi
}

#####################################################
#
# get_os_version
#
# This function will get the Solaris version.
# e.g.
# If run on a Solaris 10 machine, the function
# will output 10
#
# Return Value:
#	Always returns 0
#
#####################################################
get_os_version()
{

	#
	# expected output from "uname -r" is something like: 5.10
	# this sed expr discards everything before and including
	# the ".".
	#
	LC_ALL=C uname -r | sed -e 's/.*\.//'
	return 0
}


#####################################################
# replace_bootcluster
#
# This function will first take backup of the
# existing bootcluster file, then replace the original
# bootcluster with a QL version of bootcluster.
#
# Return Values:
#	0: 		On Success
#	Other values: 	On Error
#
#####################################################
replace_bootcluster()
{

	printf \
	    "$(gettext '%s: Entering replace_bootcluster')\n" \
	    "$PROG" | logdbgmsg

	#
	# Check if the boot cluster file exists
	#
	if [[ ! -f ${ORG_BOOTCLUSTER} ]]; then
		printf "$(gettext '%s: bootcluster not found')\n" "$PROG" \
		    | logerr
		return 1
	fi

	#
	# Copy the boot cluster file
	#
	/usr/bin/cp ${ORG_BOOTCLUSTER} ${ORG_BOOTCLUSTER_BACKUP}

	if [[ $? -ne 0 ]]; then
		printf \
		    "$(gettext '%s: Failed to take backup of bootcluster')\n" \
		    "$PROG" | logerr
		return 1
	fi

	#
	# Take a backup copy of QL bootcluster.
	#
	/usr/bin/cp ${TMP_BOOTCLUSTER} ${QL_BOOTCLUSTER_BACKUP}

	if [[ $? -ne 0 ]]; then
		printf "$(gettext '%s: Failed to take backup of QL bootcluster')\n" \
		    "$PROG" | logerr
		return 1
	fi

	#
	# Replace the original boot clsuter with QL  bootcluster
	#
	/usr/bin/cp ${TMP_BOOTCLUSTER} ${ORG_BOOTCLUSTER}

	if [[ $? -ne 0 ]]; then
		printf "$(gettext '%s: Failed to replace bootcluster')\n" \
		    "$PROG" | logerr
		return 1
	fi

	#
	# Success
	#
	printf "$(gettext '%s: Successfully replaced bootcluster')\n" "$PROG" \
	    | logmsg

	return 0
}


#####################################################
#
# main
#
# Return Value:
#	0: 		On Success
#	Other values: 	On Error
#
#####################################################
main()
{
	typeset zone_name

	#
	# Check if executing from non-global zone. Executing from a non-global
	# zone is not allowed for this command.
	#
	if [[ -f /usr/bin/zonename ]]; then

		zone_name=`/usr/bin/zonename`

		if [[ "${zone_name}" != "global" ]]; then
			printf "$(gettext '%s: Command not supported from non-global zone.')\n" "${PROG}"
			exit 1
		fi
	fi

	#
	# load common functions.
	#
	loadlib ${QL_UTIL_PATH}/${QL_UTIL_FILE} || return 1

	#
	# Get Solaris version
	#
	OS_VERSION="$(get_os_version)"

	#
	# Identifythe location of the bootcluster file depending
	# on the Solaris version running on the system.
	#
	if [[ OS_VERSION -gt 9 ]];then
		ORG_BOOTCLUSTER=${ORG_BOOTCLUSTER_GREENLINE}
	else
		ORG_BOOTCLUSTER=${ORG_BOOTCLUSTER_NON_GREENLINE}
	fi

	#
	# Replace the bootcluster file
	#
	replace_bootcluster
	
	if [[ $? -ne 0 ]]; then
		#
		# Failure
		#
		return 1
	fi

	#
	# Success
	#
	return 0
}

main
