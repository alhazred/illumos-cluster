#! /bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#pragma ident	"@(#)restore_bootcluster.ksh	1.3	08/05/20 SMI"
#
# restore_bootcluster
#
# This script will restore the original bootcluster.
#
# Logic:
#
# Compare the bootcluster file with the QL bootcluster.
# If they are same then directly replace the bootcluster file with
# /etc/cluster/bootcluster.org (the original bootcluster). Retrun.
# If they are not same, it means that the user has done a scinstall -u update
# and hence the latest version of bootcluster has been installed. Hence, no
# need to do anything.
#
# Finally delete the backed up copies of bootcluster
# /etc/cluster/bootcluster.org (the original bootcluster)
# /etc/cluster/bootcluster.ql (the ql bootcluster)
#

# variable to store the Solaris Version
typeset -i OS_VERSION

typeset QL_UTIL_PATH=/usr/cluster/lib/scadmin/ql/
typeset QL_UTIL_FILE=ql_util

# Location of bootcluster in non greenline systems
typeset ORG_BOOTCLUSTER_NON_GREENLINE=/etc/init.d/bootcluster

# Location of the bootcluster in greenline systems
typeset ORG_BOOTCLUSTER_GREENLINE=/usr/cluster/lib/svc/method/bootcluster

# Location of the backeup original bootcluster
typeset ORG_BOOTCLUSTER_BACKUP=/etc/cluster/bootcluster.org

# Location of the backup QL bootcluster
typeset QL_BOOTCLUSTER_BACKUP=/etc/cluster/bootcluster.ql

# Location of temporary bootcluster
typeset TMP_BOOTCLUSTER=/var/cluster/run/bootcluster

# Location of the bootcluster file on the node
typeset ORG_BOOTCLUSTER=

# The Signature string
typeset SIGNATURE_STRING="Temporary Disable Bootcluster for Quantum Leap"

if [[ -z $PROG ]]; then
	typeset -r PROG=${0##*/}
fi


###############################################################
# loadlib() libname
#
#	Load the named include file.
#
#	Return:
#		zero            Success
#		non-zero        Failure
#
###############################################################
loadlib()
{
	typeset libname=$1

	#
	# Load the library
	#
	. ${libname}

	if [[ $? != 0 ]]; then

		printf "$(gettext '%s:  Unable to load \"%s\"')\n" \
		    "${PROG}" "${libname}" >&2

		return 1

	fi
}

#####################################################
#
# get_os_version
#
# This function will get the Solaris version.
# e.g.
# If run on a Solaris 10 machine, the function
# will output 10
#
# Return Value:
#	Always returns 0
#
#####################################################
get_os_version()
{
	#
	# expected output from "uname -r" is something like: 5.10
	# this sed expr discards everything before and including
	# the ".".
	#
	LC_ALL=C uname -r | sed -e 's/.*\.//'
	return 0
}

#####################################################
#
# restore_bootcluster
#
# This function will compare the bootcluster with
# the QL bootcluster. If they are the same then it
# will restore the original bootcluster. If they
# are not the same then it means that the latest
# version of bootcluster has been installed.
#
#
# Return Value:
#	1: Error
#	0: Success
#
#####################################################
restore_bootcluster()
{
	typeset -i line_count=0


	#
	# Check for the existence of the backed up
	# original bootcluster files. If it is not present,
	# we assume that the replace_bootcluster had failed.
	# Hence we return with success.
	#
	if [[ ! -f ${ORG_BOOTCLUSTER_BACKUP} ]];then
		printf "$(gettext '%s: Backup of original bootcluster not found.')\n" "$PROG" | logmsg
		return 0
	fi

	#
	# Check for the existence of the backed up
	# QL bootcluster files. If it is not present,
	# we assume that the replace_bootcluster had failed.
	# Hence we return with success.
	#
	if [[ ! -f ${QL_BOOTCLUSTER_BACKUP} ]];then
		printf "$(gettext '%s: Backup of QL bootcluster not found')\n" \
		    "$PROG" | logmsg
		return 0
	fi

	#
	# Check if the signature line exists in the bootcluster.
	# If it exists replace the file with the backedup original
	# bootcluster file.
	#
	line_count=`cat ${ORG_BOOTCLUSTER} | grep "${SIGNATURE_STRING}" | wc -l`

	if [[ $line_count -eq 1 ]];then
		/usr/bin/cp ${ORG_BOOTCLUSTER_BACKUP} \
		    ${ORG_BOOTCLUSTER}
		if [[ $? -ne 0 ]]; then
			#
			# Failed to restore bootcluster.
			# This is a fatal error. The QL upgrade
			# will be failed
			#
			printf "$(gettext '%s: Failed to restore bootcluster')\n" "$PROG" | logerr
			# Delete the temporary diff file
			/usr/bin/rm -f /tmp/bc_ql.diff
		
			return 1
		fi

		#
		# Success
		#
		printf "$(gettext '%s: Successfully restored bootcluster')\n" \
		    "$PROG" | logmsg

		# Delete the temporary diff file
		/usr/bin/rm -f /tmp/bc_ql.diff

		return 0

	fi

	#
	# The signature line was not found in bootcluster. There
	# could be a partially copied file. Check if the length of
	# the bootcluster file is less than 40 lines. If it is less
	# than 40 lines, then replace it with the backedup original
	# bootcluster file.
	#
	line_count=`cat ${ORG_BOOTCLUSTER} | wc -l`

	if [[ $line_count -lt 40 ]];then
		/usr/bin/cp ${ORG_BOOTCLUSTER_BACKUP} \
		    ${ORG_BOOTCLUSTER}
		if [[ $? -ne 0 ]]; then
			#
			# Failed to restore bootcluster.
			# This is a fatal error. The QL upgrade
			# will be failed
			#
			printf "$(gettext '%s: Failed to restore bootcluster')\n" "$PROG" | logerr

			# Delete the temporary diff file
			/usr/bin/rm -f /tmp/bc_ql.diff
			
			return 1
		fi

	fi

	#
	# Delete the temporary diff file
	#
	/usr/bin/rm -f /tmp/bc_ql.diff

	#
	# Success
	#
	printf "$(gettext '%s: Successfully restored bootcluster')\n" \
	    "$PROG" | logmsg

	return 0
}


#####################################################
#
# main
#
# Return Value:
#	0: 		On Success
#	Other values: 	On Error
#
#####################################################
main()
{
	#
	# load common functions.
	#
	loadlib ${QL_UTIL_PATH}/${QL_UTIL_FILE} || return 1

	#
	# Global zone privilege check. If we are running in a non-global
        # we exit here.
        #
	sc_zones_check

	#
	# Get the Solaris Version
	#
	OS_VERSION="$(get_os_version)"

	#
	# Identifythe location of the bootcluster file depending
	# on the Solaris version running on the system.
	#
	if [[ OS_VERSION -gt 9 ]];then
		ORG_BOOTCLUSTER=${ORG_BOOTCLUSTER_GREENLINE}
	else
		ORG_BOOTCLUSTER=${ORG_BOOTCLUSTER_NON_GREENLINE}
	fi

	#
	# Restore the bootcluster file.
	#
	restore_bootcluster

	if [[ $? -ne 0 ]]; then
		#
		# Failure
		#
		return 1
	fi

	#
	# Success
	#
	return 0
}

main
