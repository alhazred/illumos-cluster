#! /bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#pragma ident	"@(#)upgrade_partition_b.ksh	1.6	08/05/20 SMI"
#
# upgrade_partition_b.ksh
#
# upgrade_partition_b [-i] [-R <mount_point>]
#
# This script remotely executes the command $QL_PARTB_CMN_CMD on all
# nodes in partition B. On each node of partition B, this results in
# the following changes to the CCR:
#
#	1. Change the quorum vote of root node for partition A to one.
#
#	2. Change the quorum votes of all other nodes of the cluster to
#	   zero.
#	
#	3. Add the transport version information to the CCR.
#
# It then reboots all the nodes in partition B into cluster mode.
#
# When called with the -R option, it means that the script is running
# in a live upgrade scenario. It will call the $QL_PARTB_CMN_CMD only
# on the local node and will not try to reboot any node.
#

###############################################################
#
# Variable globals
#
###############################################################
integer -r SC_TRUE=1
integer -r SC_FALSE=0

# Modes of operation for this script
integer NORMAL_MODE=${SC_TRUE}
integer LU_MODE=${SC_FALSE}
integer INTERACTIVE_MODE=${SC_FALSE}

typeset HOSTNAME_CMD=/usr/bin/hostname

typeset CLUSTER_LIB_PATH=/usr/cluster/lib
typeset QL_UTIL_PATH=/usr/cluster/lib/scadmin/ql
typeset QL_UTIL_FILE=ql_util
typeset QL_RESTORE_BOOTCLUSTER_CMD=restore_bootcluster

# The mount point for the alternate boot environment.
typeset MOUNT_POINT

integer sc_transport_version=0

#
# Program name and args list
#
if [[ -z $PROG ]]; then
	typeset -r PROG=${0##*/}
fi
typeset hostname

###############################################################
#
# print_usage
#
#       Print usage message to stderr
#
###############################################################
print_usage()
{
        echo "$(gettext 'usage'): ${PROG} [-i] [-R <mount_point>]" >&2
}

###############################################################
# loadlib() libname
#
#       Load the named include file.
#
#       Return:
#               zero            Success
#               non-zero        Failure
#
###############################################################
loadlib()
{
	typeset libname=$1

	#
	# Load the library
	#
	. ${libname}

	if [[ $? != 0 ]]; then

		printf "$(gettext '%s:  Unable to load \"%s\"')\n" \
		    "${PROG}" "${libname}" >&2

		return 1

	fi
}


###############################################################
#
# restore_bootcluster partition_b_nodes
#
#      Restore the bootcluster on all nodes of partition B.
#
#       Return:
#               zero            Success
#               non-zero        Failure
#
###############################################################
restore_bootcluster()
{
	typeset node
	typeset partition_b_nodes


	partition_b_nodes="$*"

	for node in $partition_b_nodes
	do

		if [[ $hostname = $node ]]; then

			${QL_UTIL_PATH}/${QL_RESTORE_BOOTCLUSTER_CMD} \
			    > /dev/null 2>&1

			if [[ $? -ne 0 ]]; then

				printf "$(gettext '%s: %s returned error')\n" \
				    "${PROG}" "${QL_RESTORE_BOOTCLUSTER_CMD}" | logerr
				return 1
			fi

		else
			execute_cmd $node ${QL_UTIL_PATH}/${QL_RESTORE_BOOTCLUSTER_CMD} \
			    > /dev/null 2>&1

			if [[ $? -ne 0 ]]; then

				printf "$(gettext '%s: %s failed on %s')\n" \
				    "${PROG}" "${QL_RESTORE_BOOTCLUSTER_CMD}" \
				    "${node}" | logerr
				return 1
			fi
		fi
	done

	#
	# Success
	#
	return 0
}



###############################################################
#
# change_quorum_vote partition_a_root_node partition_b_nodes
#
#      Change the quorum vote in the CCR of all nodes in the
#      partition.
#
#       Return:
#               zero            Success
#               non-zero        Failure
#
###############################################################
change_quorum_vote()
{
	typeset node
	typeset retval
	typeset partition_b_nodes
	typeset partition_a_root_node

	partition_a_root_node=$1
	shift 1
	partition_b_nodes="$*"

	#
	# If QL is going on in a LU scenario, then
	# execute the command only in the local node
	# and return. No need to execute on remote nodes.
	#
	if [[ ${LU_MODE} -eq ${SC_TRUE} ]]; then

		${QL_LIB_PATH}/${QL_PARTB_CMN_CMD} \
		    -a $partition_a_root_node      \
		    -v $sc_transport_version       \
		    -R > /dev/null 2>&1
		
		retval=$?

		if [[ ${retval} -ne 0 ]]; then

			printf "$(gettext '%s: %s returned error')\n" \
			    "${PROG}" "${QL_PARTB_CMN_CMD}" | logerr

			return 1
		fi

		#
		# Success
		#
		return 0

	fi

	for node in $partition_b_nodes
	do

		if [[ $hostname = $node ]]; then

			${QL_LIB_PATH}/${QL_PARTB_CMN_CMD} -a \
			    $partition_a_root_node -v $sc_transport_version \
			    > /dev/null 2>&1

			if [[ $? -ne 0 ]]; then

				printf "$(gettext '%s: %s returned error')\n" \
				    "${PROG}" "${QL_PARTB_CMN_CMD}" | logerr

				return 1
			fi

		else
			
			execute_cmd \
			    $node ${QL_LIB_PATH}/${QL_PARTB_CMN_CMD} \
			    -a $partition_a_root_node \
			    -v $sc_transport_version \
			    > /dev/null 2>&1

			if [[ $? -ne 0 ]]; then

				printf "$(gettext \
				    '%s: %s failed on %s')\n" \
				    "${PROG}" "${QL_PARTB_CMN_CMD}" \
				    "${node}" | logerr
				return 1
			fi

		
		fi
	done

	#
	# Success
	#
	return 0
}

##############################################################
#
# reboot_partition_B partition_b_nodes
#
#	Reboot nodes of partition B.
#
#	Return:
#		zero            Success
#		non-zero        Failure
#
##############################################################
reboot_partition_B()
{
	typeset partition_b_nodes
	typeset node
	
	#
        # Reboot each node of partition B to cluster mode
        #
        if [[ ! -x /usr/sbin/reboot ]]; then
                printf "$(gettext '%s: reboot not found')\n" "${PROG}" | logerr 
                return 1
        fi

	partition_b_nodes="$*"

	for node in $partition_b_nodes
	do
		if [[ $hostname = $node ]]; then
			continue;
		fi

		printf "$(gettext '%s: Rebooting node %s')\n" "${PROG}" \
		    "${node}" | logdbgmsg > /dev/null 2>&1

		execute_cmd $node /usr/sbin/reboot > /dev/null 2>&1

		#
		# If reboot fails for some reason, nodes have to be
		# rebooted manually. We dont check for any return value
		# here.
		#
	done

        #
        # Reboot the current node and we are done.
        #
	printf "$(gettext '%s: Rebooting the node')\n" "${PROG}" | logdbgmsg \
	    > /dev/null 2>&1

	/usr/sbin/reboot
}

#############################################################
#
# main
#
############################################################
main()
{
	# partition B.
	typeset partition_b_nodes

	# partition A.
	typeset partition_a_root_node

	# partition B.
	typeset partition_b_root_node

	# flags.
	integer found=${SC_FALSE}
	

	# local variable.
	typeset node
	typeset retval

	# load common functions.
	loadlib ${QL_UTIL_PATH}/${QL_UTIL_FILE} || return 1

	#
	# Global zone privilege check. If we are running in a non-global
        # we exit here.
        #
	sc_zones_check

	while getopts iR: c 2>/dev/null
	do
		case ${c} in
		i)      # Interactive mode.
			INTERACTIVE_MODE=${SC_TRUE}
			;;
		R)	# Live Upgrade Mode
			LU_MODE=${SC_TRUE}
			INTERACTIVE_MODE=${SC_TRUE}
			NORMAL_MODE=${SC_FALSE}

			MOUNT_POINT=`echo ${OPTARG} | sed -e 's/\/$//g'`
			if [[ -z ${MOUNT_POINT} ]]; then
				print_usage
				return 1
			fi
			;;
		\?)     print_usage
			return 1
			;;
		*)      # bad option
			print_usage
			return 1
			;;
		esac
	done

	#
	# check to see if we are root.
	#
	verify_isroot

	if [[ $? -ne 0 ]]; then
		return 1
	fi

	#
	# check if command exists.
	#
	if [[ ! -x /usr/sbin/clinfo ]]; then
		printf "$(gettext '%s: Could not find %s')\n" "${PROG}" \
		    "clinfo" | logerr
		return 1
	fi

	#
	# check for non-cluster mode.
	#
	/usr/sbin/clinfo > /dev/null 2>&1
	retval=$?
	
	if [[ ${retval} -eq 0 ]] && [[ ${NORMAL_MODE} -eq ${SC_TRUE} ]]; then
		#
		# In normal mode, when a plain QL
		# upgrade is going on, the node
		# should be in non cluster mode
		#
                printf "$(gettext '%s Node must be in noncluster mode')\n" \
                    "${PROG}" | logerr
                return 1
        fi

	if [[ ${retval} -ne 0 ]] && [[ ${LU_MODE} -eq ${SC_TRUE} ]]; then
		#
		# In lu mode, when a QL upgrade is
		# going on along with a Live Upgrade
		# the node should be in cluster mode
		#
		printf "$(gettext '%s Node must be in cluster mode')\n" \
		    "${PROG}" | logerr
		return 1

	fi

	#
	# Read the nodes of partition B.
	#
	partition_b_nodes=`read_partition_B`

	if [[ $? -ne 0 ]]; then
		return 1
	fi

	#
	# Read the root node of partition A.
	#
	partition_a_root_node=`read_partition_A_root`

	if [[ $? -ne 0 ]]; then
		return 1
	fi

	#
	# Read the root node of partition B.
	#
	partition_b_root_node=`read_partition_B_root`

	if [[ $? -ne 0 ]]; then
		return 1
	fi

	#
	# Read the transport version.
	#
	sc_transport_version=`read_transport_version`
	
	if [[ $? -ne 0 ]]; then
		return 1
	fi

	#
	# Check for duplicates.
	#
	duplicate "${partition_b_nodes} ${partition_a_root_node}"

	if [[ $? -ne 0 ]]; then
		printf "$(gettext '%s: Duplicate nodes present')\n" \
		    "${PROG}" | logerr
		return 1
	fi

	#
	# Mask out SIGHUP and SIGINT.
	#
	trap 'ign_sig' HUP INT

	#
	# Find remote method of communication.
	# In Live Upgrade mode, we will not
	# do rsh/ssh calls. Hence find the
	# remote method only for non LU scenario
	#
	if [[ ${LU_MODE} -eq ${SC_FALSE} ]]; then
		find_remote_method $partition_b_nodes

		if [[ $? -ne 0 ]]; then
			printf "$(gettext \
			    '%s: Failed to find a remote method')\n" \
			    "${PROG}" | logerr
		fi
	fi

	#
	# check if command exists
	#
	if [[ ! -x ${HOSTNAME_CMD} ]]; then
		printf "$(gettext '%s: Could not find %s')\n" "${PROG}" \
		    "${HOSTNAME_CMD}" | logerr
		return 1
	fi

	hostname=`${HOSTNAME_CMD}`

	#
	# Check to see if the current node is in partition B.
	#
	for node in $partition_b_nodes
	do
		if [[ $hostname = $node ]]; then
			found=${SC_TRUE}
			continue;
		fi
	done

	#
	# Make sure we are being run on some node of partition B.
	#
	if [[ $found -ne ${SC_TRUE} ]]; then
		printf "$(gettext \
		    '%s: Current node not in the second partition')\n"
		    "${PROG}" | logerr
		return 1
	fi

	#
	# Grab the apply lock before executing. Should be done
	# when LU is not going on. In a LU scenario, the apply
	# command is executed on all nodes
	#
	if [[ ${LU_MODE} -eq ${SC_FALSE} ]]; then
		get_file_lock ${partition_b_root_node} ${QL_APPLY_RUN_LOCK_FILE}
		if [[ $? != 0 ]]; then
			printf "$(gettext \
			    '%s: The \"apply\" command is running or has been already run on the second partition')\n" \
		    	"${PROG}" | logerr
			return 1
		fi
	fi

	#
	# In normal mode or interactive mode do the following
	#
	# For each node in Partition B
	# Change the quorum vote of all the nodes except root node
	# of partition A to zero.
	# Change the quorum vote of the root node of partition A to one.
	# Change the Dual-partition upgrade state to QL_UPGRADE_STATE_2 (two).
	#
	# In LU mode, do the above tasks for only the local node
	#
	change_quorum_vote $partition_a_root_node $partition_b_nodes

	if [[ $? -ne 0 ]]; then
		return 1
	fi

	#
	# Restore the bootcluster file on each node of partition B
	# If there is an error, we will abort the upgrade.
	# This is done when LU is not happening
	#
	if [[ ${LU_MODE} -eq ${SC_FALSE} ]]; then
		restore_bootcluster $partition_b_nodes

		if [[ $? -ne 0 ]]; then
			return 1
		fi
	fi

	#
	# Reboot each node of partition B to cluster mode. We dont do this if
	# we are being run from an interactive mode or live upgrade mode.
	#
	if [[ ${INTERACTIVE_MODE} -ne ${SC_TRUE} ]]; then
		reboot_partition_B $partition_b_nodes

		#
		# We may not return here.
		#
		if [[ $? -ne 0 ]]; then
			printf "$(gettext \
			    '%s: Failed to reboot nodes in the second partition')\n" \
			    "${PROG}" | logerr
                fi
	fi

	#
	# We may not return here.
	#
	return 0
}
main $*
#
# Does not return in the absence of errors.
#
