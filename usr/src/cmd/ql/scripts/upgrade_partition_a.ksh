#! /bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#pragma ident	"@(#)upgrade_partition_a.ksh	1.5	08/05/20 SMI"
#
# upgrade_partition_a.ksh
#
# upgrade_partition_a [-i] [-R <mount_point>]
#
# This script should be executed from a node in partition A.
# This script remotely executes QL_PARTA_CMN_CMD on all
# nodes in partition A. On each node of partition A, this results
# in the following changes to the CCR:
#
#	1. Add the root node of partititon A to the CCR.
#	2. Add nodes of partition A to the CCR.
#	3. Add the root node of partition B to the CCR.
#	4. Add nodes of partition B to the CCR.
#	5. Change the quorum vote of root node for partition A to one.
#	6. Change the quorum vote of all nodes in the cluster to zero.
#	7. Set the Dual partition CCR upgrade state to QL_UPGRADE_STATE_1.
#	8. Add the transport version to the CCR.
#
# Also on each node, any entries of global QFS file systems that are 
# mounted at boot time are disbaled. Fencing is disabled on these
# nodes. This script then reboots all the nodes in partition A into cluster
# mode.
#

###############################################################
#
# Variable globals
#
###############################################################
integer -r SC_TRUE=1
integer -r SC_FALSE=0

typeset HOSTNAME_CMD=/usr/bin/hostname

typeset QL_UTIL_PATH=/usr/cluster/lib/scadmin/ql
typeset QL_UTIL_FILE=ql_util

# Mount point for the alternate boot environment
typeset MOUNT_POINT

#
# Flags indicating how the script will work
#
integer NORMAL_MODE=${SC_TRUE}
integer INTERACTIVE_MODE=${SC_FALSE}
integer LU_MODE=${SC_FALSE}

typeset hostname

#
# Program name and args list
#
if [[ -z $PROG ]]; then
	typeset -r PROG=${0##*/}
fi

typeset partition_a_nodes
typeset partition_b_nodes
integer sc_transport_version

################################################################
#
# print_usage
#
#       Print usage message to stderr
#
################################################################
print_usage()
{
        echo "$(gettext 'usage'):${PROG} [-i] [-R <mount_point>]" >&2
}

#####################################################
#
# loadlib() libname
#
#	Load the named include file.
#
#	Return:
#		zero		Success
#		non-zero	Failure
#
#####################################################
loadlib()
{
	typeset libname=$1

	#
	# Load the library
	#
	. ${libname}

	if [[ $? != 0 ]]; then

		printf "$(gettext '%s:  Unable to load \"%s\"')\n" \
		    "${PROG}" "${libname}" >&2

		return 1

	fi
}

##############################################################
#
# update_ccr partition_b_root_node partition_a_root_node
#
#	Add partition information to the CCR on all the nodes of
#	the cluster.
#
#	Return:
#		zero		Success
#		non-zero	Failure
#
##############################################################
update_ccr()
{
	typeset node

	# partition A.
	typeset partition_a_root_node
	typeset partition_a_commalist

	# partition B.
	typeset partition_b_root_node

	partition_b_root_node=$1
	partition_a_root_node=$2

	for node in $partition_a_nodes
	do
		if [[ -z $partition_a_commalist ]]; then
			partition_a_commalist=$node
		else
			partition_a_commalist=$partition_a_commalist,$node
		fi
	done

	#
	# If QL is going on in a LU scenario, execute the command
	# for the local node only. No need to execute command for
	# the remote nodes.
	#
	if [[ ${LU_MODE} -eq ${SC_TRUE} ]]; then

		${QL_LIB_PATH}/${QL_PARTA_CMN_CMD} \
		    -a $partition_a_root_node      \
		    -b $partition_b_root_node      \
		    -n $partition_a_commalist      \
		    -v $sc_transport_version       \
		    -R > /dev/null 2>&1

		if [[ $? -ne 0 ]]; then

			printf "$(gettext '%s: %s failed')\n" \
			    "${PROG}" "${QL_PARTA_CMN_CMD}" | logerr
			return 1

		fi

		#
		# Success
		#
		return 0
	fi
 
	for node in $partition_a_nodes
	do
		if [[ $hostname = $node ]]; then

			printf "$(gettext '%s: Executing %s ')\n" \
			    "${PROG}" "${QL_PARTA_CMN_CMD}" | logdbgmsg

			#
			# Check if this is the only node in the partition.
			#
			if [[ -z $partition_a_commalist ]]; then
				${QL_LIB_PATH}/${QL_PARTA_CMN_CMD} \
				    -a $partition_a_root_node \
			    	    -b $partition_b_root_node \
				    -v $sc_transport_version > /dev/null 2>&1
			else
				${QL_LIB_PATH}/${QL_PARTA_CMN_CMD} \
				    -a $partition_a_root_node \
				    -b $partition_b_root_node \
				    -n $partition_a_commalist \
				    -v $sc_transport_version > /dev/null 2>&1
			fi

			if [[ $? -ne 0 ]]; then

				printf "$(gettext '%s: %s failed')\n" \
				    "${PROG}" "${QL_PARTA_CMN_CMD}" | logerr
				return 1

			fi
		else
			printf "$(gettext '%s: Executing %s on node %s')\n" \
			    "${PROG}" "${QL_PARTA_CMN_CMD}" "${node}" \
			    | logdbgmsg 

			#
			# We cant have a null string here!.
			#
			if [[ -z $partition_a_commalist ]]; then
				printf \
				    "$(gettext '%s: Partition A is empty')\n" \
				    "${PROG}" | logerr
				return 1
			fi
			
			execute_cmd \
			    $node ${QL_LIB_PATH}/${QL_PARTA_CMN_CMD} \
			    -a $partition_a_root_node \
			    -b $partition_b_root_node \
			    -n $partition_a_commalist \
			    -v $sc_transport_version  \
			    > /dev/null 2>&1

			if [[ $? -ne 0 ]]; then

				printf "$(gettext \
				    '%s: %s failed on node %s')\n" \
				    "${PROG}" "${QL_PARTA_CMN_CMD}" \
				    "${node}" | logerr
				return 1
			fi

		fi
	done
	return 0
}

##############################################################
#
# reboot_partition_A
#
#	Reboot nodes of partition A.
#
#	Return:
#		zero		Success
#		non-zero	Failure
#
##############################################################
reboot_partition_A()
{
	typeset node

	#
	# check if command exists.
	#
	if [[ ! -x /usr/sbin/reboot ]]; then
		printf "$(gettext '%s: reboot not found')\n" "${PROG}" | logerr
		return 1
	fi

	for node in $partition_a_nodes
	do
		if [[ $hostname = $node ]]; then
			continue;
		fi

		printf "$(gettext '%s: Rebooting node %s')\n" "${PROG}" \
		    "${node}" | logdbgmsg

		execute_cmd $node /usr/sbin/reboot > /dev/null 2>&1

		#
		# If the reboot fails here for some reason, it has to
		# be done manually.
		#
	done

	#
	# Reboot the current node and we are done.
	#
	printf "$(gettext '%s: rebooting node')\n" "${PROG}" | logdbgmsg

	/usr/sbin/reboot

	#
	# we dont return here.
	#
}

###########################################################
#
# main
#
##########################################################
main()
{
	typeset retval

	# partition A.
	typeset partition_a_root_node

	# partition B.
	typeset partition_b_root_node

	# flags.
	integer found=${SC_FALSE}


	# load common functions
	loadlib ${QL_UTIL_PATH}/${QL_UTIL_FILE} || return 1

	#
	# Global zone privilege check. If we are running in a non-global
        # we exit here.
        #
	sc_zones_check

	#
	# Create the debug log if it does not already exist.
	#
	createlog

	while getopts iR: c 2>/dev/null
	do
		case ${c} in
		i)      # Interactive mode.
			INTERACTIVE_MODE=${SC_TRUE}
			;;

		R)
			# Live Upgrade Mode
			LU_MODE=${SC_TRUE}

			MOUNT_POINT=`echo ${OPTARG} | sed -e 's/\/$//g'`
			if [[ -z ${MOUNT_POINT} ]]; then
				print_usage
				return 1
			fi
			;;

		\?)     print_usage
			return 1
			;;

		*)      # bad option
			print_usage
			return 1
			;;

		esac
	done

	if [[ ${LU_MODE} -eq ${SC_TRUE} ]]; then

		# Disable Normal Mode
		NORMAL_MODE=${SC_FALSE}

		# Enable Interactive mode
		INTERACTIVE_MODE=${SC_TRUE}
	
	fi

	#
	# check to see if we are root.
	#
	verify_isroot

	if [[ $? -ne 0 ]]; then
		return 1
	fi

	#
	# check if command exists
	#
	if [[ ! -x ${HOSTNAME_CMD} ]]; then
		printf "$(gettext '%s: Could not find %s')\n" "${PROG}" \
		    "${HOSTNAME_CMD}" | logerr
		return 1
	fi

	hostname=`${HOSTNAME_CMD}`

	#
	# check if command exists.
	# 
	if [[ ! -x /usr/sbin/clinfo ]]; then
		printf "$(gettext '%s: Could not find %s')\n" "${PROG}" \
		    "clinfo" | logerr
		return 1
	fi

	#
	# check for non-cluster mode in a non LU scenario (normal mode) and
	# cluster mode in a LU scenario (lu mode).
	#
	/usr/sbin/clinfo > /dev/null 2>&1
	retval=$?

	if [[ ${retval} -eq 0 ]] && [[ ${NORMAL_MODE} -eq ${SC_TRUE} ]]; then
		#
		# In normal mode, when a plain QL
		# upgrade is going on, the node
		# should be in non cluster mode
		#
		printf "$(gettext '%s Node must be in noncluster mode')\n" \
		    "${PROG}" | logerr
		return 1
	fi

	if [[ ${retval} -ne 0 ]] && [[ ${LU_MODE} -eq ${SC_TRUE} ]]; then
		#
		# In lu mode, when a QL upgrade is
		# going on along with a Live Upgrade
		# the node should be in cluster mode
		#
		printf "$(gettext '%s Node must be in cluster mode')\n" \
		    "${PROG}" | logerr
		return 1

	fi

	#
	# Read the root node of partition A.
	#
	partition_a_root_node=`read_partition_A_root`

	if [[ $? -ne 0 ]]; then
		return 1
	fi

	#
	# Read the root node of partition B.
	#
	partition_b_root_node=`read_partition_B_root`

	if [[ $? -ne 0 ]]; then
		return 1
	fi

	#
	# Read nodes of partition A.
	#
	partition_a_nodes=`read_partition_A`

	if [[ $? -ne 0 ]]; then
		return 1
	fi

	#
	# Read nodes of partition B.
	#
	partition_b_nodes=`read_partition_B`

	if [[ $? -ne 0 ]]; then
		return 1
	fi

	#
	# Read the transport version.
	#
	sc_transport_version=`read_transport_version`
	
	if [[ $? -ne 0 ]]; then
		return 1
	fi

	#
	# check for duplicates.
	#
	duplicate "${partition_a_nodes} ${partition_b_nodes}"

	if [[ $? -ne 0 ]]; then
		printf "$(gettext '%s: Bad -n option')\n" "${PROG}" | logerr
		return 1
	fi

	if [[ $partition_a_nodes = $partition_a_root ]]; then
		# There is only one node in partition A.
		partition_a_commalist=
	fi

	#
	# Mask out SIGHUP and SIGINT
	#
	trap 'ign_sig' HUP INT



	#
	# Find remote method of communication.
	# In Live Upgrade mode, we will not
	# do rsh/ssh calls. Hence find the
	# remote method only for non LU scenario
	#
	if [[ ${LU_MODE} -eq ${SC_FALSE} ]]; then
		find_remote_method $partition_a_nodes $partition_b_nodes

		if [[ $? -ne 0 ]]; then
			printf "$(gettext \
			    '%s: Failed to find a remote method')\n" \
			    "${PROG}" | logerr
			return 1
		fi
	fi

	#
	# Check to see if the current node is in partition A.
	#
	for node in $partition_a_nodes
	do
		if [[ $hostname = $node ]]; then
			found=${SC_TRUE}
			continue;
		fi
	done

	#
	# Make sure we are being run from a node in partition A.
	#
	if [[ $found -ne ${SC_TRUE} ]]; then
		printf "$(gettext \
		    '%s: Current node not in the first partition')\n" \
		    "${PROG}" | logerr
		return 1
	fi

	#
	# Grab the apply lock before executing. Should be done
	# when LU is not going on. In a LU scenario, the apply
	# command is executed on all nodes
	#
	if [[ ${LU_MODE} -eq ${SC_FALSE} ]]; then
		get_file_lock ${partition_a_root_node} ${QL_APPLY_RUN_LOCK_FILE}
		if [[ $? != 0 ]]; then
			printf "$(gettext \
			    '%s: The \"apply\" command is running or has been already run on the first partition')\n" \
			    "${PROG}" | logerr
			return 1
		fi
	fi

	#
	# In normal mode or interactive mode do the following
	# For each node in Partition A, update the CCR.
	# Add to the CCR,
	#	1. Root node of partition B.
	#	2. Nodes of partition B.
	#	3. Root node of partition A.
	#	4. Nodes of partition A.
	#	5. Add the Dual-partition CCR upgrade state to
	#	   QL_UPGRADE_STATE_1.
	#
	# In LU Mode, do the following
	# Do the the above tasks only for local node.
	#
	update_ccr $partition_b_root_node $partition_a_root_node

	if [[ $? -ne 0 ]]; then
		printf "$(gettext \
		    '%s: CCR update failed')\n" "${PROG}" | logerr
		return 1
	fi

	#
	# If live upgrade is going on, then return
	#
	if [[ ${LU_MODE} -eq ${SC_TRUE} ]]; then
		printf "$(gettext \
		    '%s: Returning as running in Live Upgrade mode')\n" \
		    "${PROG}" | logdbgmsg
		#
		# Success
		#
		return 0
	fi
		 
	#
	# Before we reboot partition A we create a status file on the nodes
	# of partition B. This is to make sure that we dont have out of order
	# execution of the commands QL_PARTA_CMD and QL_PARTB_CMD
	#
	for node in $partition_b_nodes
	do
		printf "$(gettext '%s: creating file %s on node %s')\n" \
		    "${PROG}" "${QL_STATUS_FILE_PATH}/${QL_STATUS_FILE}" \
		    "$node" | logdbgmsg

		execute_cmd $node /usr/bin/touch \
		    ${QL_STATUS_FILE_PATH}/${QL_STATUS_FILE} > /dev/null 2>&1

		if [[ $? -ne 0 ]]; then
			printf "$(gettext '%s: Could not create %s on %s')\n" \
			    "${PROG}" \
			    "${QL_STATUS_FILE_PATH}/${QL_STATUS_FILE}" "$node"\
			    | logdbgmsg
			return 1
		fi
	done

	#
	# Reboot each node of partition A to cluster mode. We dont do this if
	# we are being run from an interactive mode or live upgrade mode.
	#
	if [[ ${INTERACTIVE_MODE} -ne ${SC_TRUE} ]]; then
		reboot_partition_A

		#
		# We may not return here. 
		#
		if [[ $? -ne 0 ]]; then 
			printf "$(gettext \
			    '%s: Failed to reboot nodes in the first partition')\n" \
			    "${PROG}" | logerr
		fi
	fi

	#
	# We may not return here.
	#
	return 0
}
main $*
