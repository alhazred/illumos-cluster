#! /bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#pragma ident	"@(#)ql_util.ksh	1.10	08/08/01 SMI"
#
# ql_util.ksh
#
# ql_util
#
# This script has the utility functions used during Dual-Partition upgrade.
#

#
# Define all the common variables in this script.
#
export CLUSTER_CMD_PATH=/usr/cluster/bin

# All the commands for upgrade reside here.
export QL_LIB_PATH=/usr/cluster/lib/scadmin/ql

#
# Command run by Dual-partition upgrade begin script on each node of
# partition B before shutdown of the node.
#
export PRE_SHUTDOWN_CMD=node_halt

#
# Directory in which PRE_SHUTDOWN_CMD script should reside.
#
export PRE_SHUTDOWN_CMD_PATH=/etc/cluster/ql

#
# This is the scratch-pad directory for the upgrade commands to put stuff
# that are needed later on. This is currently chosen to be /var/cluster/run.
# This directory is cleaned up automatically on each cluster reboot, so
# we dont have to worry about cleanup of anything that goes there.
#
export QL_TMP_DIR=/var/cluster/run

#
# The name of the upgrade status file that prevents out of order execution
# of the upgrade_partition commands.
#
export QL_STATUS_FILE=/upgrade_partition_a_run

#
# The directory where the upgrade status file resides.
#
export QL_STATUS_FILE_PATH=/etc/cluster

#
# This file has all the data that the upgrade needs. The upgrade_partition
# commands get the information they need from this file.
#
export QL_DATA_FILE=ql_data.txt

#
# The directory where the upgrade data file resides.
#
export QL_DATA_FILE_PATH=/etc/cluster

#
# The implementation file names for upgrade_partition commands. This is used
# by the upgrade_partition wrapper command.
#
export QL_PARTA_CMD=upgrade_partition_a
export QL_PARTB_CMD=upgrade_partition_b

#
# This script is invoked from the QL_PARTA_CMD command.
#
export QL_PARTA_CMN_CMD=upgrade_partition_a_common

#
# This file is invoked from the QL_PARTB_CMD command.
#
export QL_PARTB_CMN_CMD=upgrade_partition_b_common

#
# This file is invoked from QL_BEGIN_CMD command.
#
export QL_BEGIN_CMN_CMD=split_mode_upgrade_common

#
# This will be invoked for doing QL related
# work in a LU scenario. The script will be
# invoked from QL_BEGIN_CMD and from bootcluster
# 
export QL_LU_BEGIN_CMD=ql_lu_begin

#
# File to indicate that QL needs to continue on next boot
# This is used while doing QL in a Live Upgrade scenario
#
export QL_LU_CONTINUE=/etc/cluster/ql_lu_continue

#
# File to indicate that CCR transformation needs to be done
# and should be allowed even in cluster mode.
#
export QL_TRANSFORM_CCR_FILE=/etc/cluster/transform_ccr

#
# CCR infrastructure file
#
export QL_CCR_INFRASTRUCTURE=/etc/cluster/ccr/global/infrastructure

if [[ ! -f ${QL_CCR_INFRASTRUCTURE} ]]; then
	export QL_CCR_INFRASTRUCTURE=/etc/cluster/ccr/infrastructure
fi

#
# Location of the backedup ccr files
#
export QL_CCR_BACKUP=/etc/cluster/ccr_ql.bak

#
# Location of the backedup vfstab file
#
export QL_VFSTAB_BACKUP=/etc/vfstab_ql.bak

#
# These commands are invoked on the root node of partition B.
#
export QL_PRE_SHUTDOWN_APP_CMD=cluster_pre_halt_apps
export QL_POST_SHUTDOWN_APP_CMD=cluster_post_halt_apps

#
# The directory where the shutdown_application commands reside.
#
export QL_SHUTDOWN_APP_CMD_PATH=/etc/cluster/ql

#
# This command is run by QL_UPGRADE_PARTA_CMN_CMD to make any
# changes to the CCR as part of the upgrade.  
#
export QL_CCR_CUSTOM_SCRIPT=data_change

#
# The directory where the command to make upgrade changes to the CCR reside.
#
export QL_CCR_CUSTOM_SCRIPT_PATH=/usr/cluster/lib/scadmin/ql

#
# This file is used by run_reserve. If this file exists, run_reserve
# exits without doing any fencing activity. Any changes to this
# should get reflected here.
#
export UPGRADE_FLAG_FILE=/scnoreservedisks

#
# This file acts as a flag indicating that the begin
# command is running or has been already run on the
# cluster.
#
export QL_BEGIN_RUN_LOCK_FILE=/etc/cluster/begin_run.lock

#
# This file acts as a flag indicating that the apply
# command is running or has been already run on that
# partition
#
export QL_APPLY_RUN_LOCK_FILE=/etc/cluster/apply_run.lock

#
# The utility used to grab the file lock
#
export QL_GET_FILE_LOCK=/ql_get_file_lock

#
# The maximum number of nodes in the cluster
#
export QL_NODEID_MAX=64

#
# All the logs go here.
#
export QL_UPGRADE_LOG="ql_upgrade_debug.log"

#
# The logs are placed here.
#
export QL_UPGRADE_LOG_PATH="/var/cluster/logs/install"

#program name and args list. Used for log messages.
if [[ -z $PROG ]]; then
	typeset -r PROG=${0##*/}
fi

#
# The remote command used to execute remote tasks on other nodes. 
#
typeset SC_QL_REMOTE_METHOD

#################################################################
#
# createfile() "filename"
#
#	Create the given "filename", if it does
#	not exist.  Any needed directories are created.
#
#	The filename must begin with /.
#
#	Return values:
#		0	- success
#		1	- error
#
#################################################################
createfile()
{
	typeset filename=${1}

	typeset dir
	typeset dirs

	if [[ $# -ne 1 ]]; then
		printf \
		    "$(gettext '%s: Bad call to openfile()')\n" ${PROG} \
		    >&2 | logerr
		return 1
	fi

	# Make sure filename begins with /
	if [[ "${filename}" != /* ]]; then
		printf "$(gettext '%s: Bad filename passed to openfile()')\n" \
		    ${PROG} >&2 | logerr
		return 1
	fi

	# Get the list of dirs we must create
	dir=${filename}
	dirs=
	while [[ "${dir}" != "/" ]]
	do
		dir="$(/usr/bin/dirname ${dir})"
		if [[ -d "${dir}" ]]; then
			break
		else
			dirs="${dir} ${dirs}"
		fi
	done

	# create each directory with group sys
	for dir in ${dirs}
	do
		/usr/bin/mkdir -m 0755 ${dir} || return 1
		/usr/bin/chgrp sys ${dir} || return 1
	done

	# create the file
	/usr/bin/touch ${filename} || return 1

	return 0
}

#################################################################
#
# createlog()
#
#	Create the install log file.  If it does not
#	exist, "logmsg" and "logerr" will not create it.
#
#################################################################
createlog()
{
	createfile ${QL_UPGRADE_LOG_PATH}/${QL_UPGRADE_LOG}
	return $?
}

##################################################################
#
# logmsg()
#
#	Print message to the dual-partition upgrade debug log file
#	and stdout. If the log file does not exist, the message goes
#	just to stdout.
#
##################################################################
logmsg()
{
	typeset datestr
	typeset logstr
	typeset msg

	integer existslogfile=1

	datestr=`/usr/bin/date`

	if [[ ! -f ${QL_UPGRADE_LOG_PATH}/${QL_UPGRADE_LOG} ]]; then
		existslogfile=0
	fi

	msg=`/usr/bin/cat`
	logstr="$datestr $msg"

	if [[ $existslogfile -eq 1 ]]; then
		/usr/bin/echo $msg
		printf "$logstr\n\n" >> \
		    ${QL_UPGRADE_LOG_PATH}/${QL_UPGRADE_LOG}
		/usr/bin/cat
	else
		/usr/bin/echo $msg
	fi
}

###################################################################
#
# logerr()
#
#	Print message to the dual-partition upgrade debug log file and
#	stderr. If the debug log file is not created, the message goes
#	just to stderr.
#
###################################################################
logerr()
{
	typeset datestr
	typeset logstr
	typeset msg

	integer existslogfile=1

	datestr=`/usr/bin/date`

	if [[ ! -f ${QL_UPGRADE_LOG_PATH}/${QL_UPGRADE_LOG} ]]; then
		existslogfile=0
	fi

	msg=`/usr/bin/cat`
	logstr="$datestr $msg"

	if [[ $existslogfile -eq 1 ]]; then
		/usr/bin/echo $msg >&2
		printf "$logstr\n\n" >> \
		    ${QL_UPGRADE_LOG_PATH}/${QL_UPGRADE_LOG}
	else
		/usr/bin/echo $msg >&2
	fi
}

##################################################################
#
# logdbgmsg()
#
#	Print message to the dual-partition upgrade debug log file.
#	Only if the log file does not exist, it goes to stdout.
#	We use this for debug messages that need not go to stdout.
#
##################################################################
logdbgmsg()
{
	typeset datestr
	typeset logstr
	typeset msg

	integer existslogfile=1

	datestr=`/usr/bin/date`

	if [[ ! -f ${QL_UPGRADE_LOG_PATH}/${QL_UPGRADE_LOG} ]]; then
		existslogfile=0
	fi

	msg=`/usr/bin/cat`
	logstr="$datestr $msg"

	if [[ $existslogfile -eq 1 ]]; then
		printf "$logstr\n\n" >> ${QL_UPGRADE_LOG_PATH}/${QL_UPGRADE_LOG}
	else
		/usr/bin/echo $msg
	fi
}

###################################################################
#
# logdbgerr()
#
#	Print message to the dual-partition upgrade debug log file.
#	Only If the debug log file is not created, the message goes to
#	stderr. We use this for messages that report detailed errors
#	that need not go to stderr.
#
###################################################################
logdbgerr()
{
	typeset datestr
	typeset logstr
	typeset msg

	integer existslogfile=1

	msg=`/usr/bin/cat`
	datestr=`/usr/bin/date`

	if [[ ! -f ${QL_UPGRADE_LOG_PATH}/${QL_UPGRADE_LOG} ]]; then
		existslogfile=0
	fi

	logstr="$datestr $msg"

	if [[ $existslogfile -eq 1 ]]; then
		printf "$logstr\n\n" >> \
		    ${QL_UPGRADE_LOG_PATH}/${QL_UPGRADE_LOG}
	else
		/usr/bin/echo $msg >&2
	fi
}

###############################################################
#
# verify_isroot()
#
#	Print an error message and return non-zero
#	if the user is not root.
#
###############################################################
verify_isroot()
{
	typeset -r uid=$(expr "$(id)" : 'uid=\([0-9]*\)*')

	# make sure uid was set
	if [[ -z "${uid}" ]]; then
		printf "$(gettext '%s:  Cannot get uid')\n" ${PROG} | logerr
		return 1
	fi

	# check for root
	if [[ ${uid} -ne 0 ]]; then
		printf "$(gettext '%s:  Must be root')\n" "${PROG}" | logerr
		return 1
	fi

	return 0
}


#######################################################################
#
# find_remote_method node_list
#
# Finds a remote method to execute - either ssh or rsh in the order of
# preference. 
#	Return:
#		zero		Success
#		non-zero	Failure
######################################################################
find_remote_method()
{
	typeset nodelist
	typeset node
	typeset method
	typeset mynodename
	typeset tmpfile
	typeset tmperr
	typeset result

	integer sshok=1
	integer rshok=1
	integer pid=$$

	nodelist="$*"
	mynodename=`/usr/bin/hostname`

	tmpfile="ql_util.$pid.$pid"
	(( pid = pid + 1 ))
	tmperr="ql_util.$pid$pid"

	rm -f /tmp/$tmpfile
	rm -f /tmp/$tmperr

	#
	# Remote method is already set. Return.
	#
	if [[ ! -z $SC_REMOTE_METHOD && $SC_REMOTE_METHOD != "scrcmd" ]]; then
		return 0
	fi

	#
	# Remote method is already set by a previous call to this function.
	# Return.
	#
	if [[ ! -z $SC_QL_REMOTE_METHOD ]]; then
		return 0
	fi

	#
	# Look at each node.
	#
	for node in $nodelist
	do
		if [[ $node == $mynodename ]]; then
			continue
		fi

		# ssh
		if [[ -x /usr/bin/ssh ]]; then

		cmd="ssh root@${node} -o \"BatchMode yes\" -o \"StrictHostKeyChecking yes\" -n \"/bin/sh -c '/bin/true;  /bin/echo SC_COMMAND_STATUS=\\\$?'\""

		printf "$(gettext '%s: Execute cmd: %s')\n" "${PROG}" \
		    "${cmd}" | logdbgmsg

		eval ${cmd} >/tmp/${tmpfile} 2>/tmp/${tmperr}

		result=$(sed -n 's/^SC_COMMAND_STATUS=//p' /tmp/${tmpfile})

		printf "$(gettext '%s: result: %s')\n" "${PROG}" "${result}" \
		    | logdbgmsg

			if [[ -z $result ]]; then
				result=1
			fi
			if [[ "$result" != "0" ]]; then
				printf "$(gettext '%s: %s to %s failed.')\n" \
				    "${PROG}" "ssh" "${node}" | logdbgmsg
				sshok=0
				cat /tmp/${tmperr} | logdbgmsg
				break
			fi
		else
			sshok=0
			break
		fi
	done

	if [[ $sshok -ne 0 ]]; then
		SC_QL_REMOTE_METHOD="ssh"
		export $SC_QL_REMOTE_METHOD
		return 0
	fi

	rm -f /tmp/$tmpfile
	rm -f /tmp/$tmperr

	for node in $nodelist
	do
		if [[ $node == $mynodename ]]; then
			continue
		fi

		# rsh
		if [[ -x /usr/bin/rsh ]]; then
			cmd="rsh ${node} -n \"/bin/sh -c '/bin/true; /bin/echo SC_COMMAND_STATUS=\\\$?'\""

			printf "$(gettext '%s: Execute cmd: %s')\n" "${PROG}" \
			    "${cmd}" | logdbgmsg

			eval ${cmd} >/tmp/${tmpfile} 2>/tmp/${tmperr}

			result=$(sed -n 's/^SC_COMMAND_STATUS=//p' /tmp/${tmpfile})
			printf "$(gettext '%s: result: %s')\n" "${PROG}" \
			    "${result}" | logdbgmsg

			if [[ -z $result ]]; then
				result=1
			fi

			if [[ "$result" != "0" ]]; then
				printf "$(gettext '%s: %s to %s failed.')\n" \
				    "${PROG}" "rsh" "${node}" | logdbgmsg
				rshok=0
				cat /tmp/${tmperr} | logdbgmsg
				break
			fi
		else
			rshok=0
			break
		fi
	done

	if [[  $rshok -ne 0 ]]; then
		SC_QL_REMOTE_METHOD="rsh"
		export $SC_QL_REMOTE_METHOD
		return 0
	fi
	printf "$(gettext '%s:ssh and rsh failed.')\n" "${PROG}" | logdbgmsg
	return 1
}

#######################################################################
#
# execute_cmd nodename command [cmdArgs]
#
# Executes the command remotely on the node given by nodename. The
# method used will be ssh or rsh in the order of preference.
# If SC_REMOTE_METHOD is set, the method found in SC_REMOTE_METHOD
# is used. Otherwise we check and use SC_QL_REMOTE_METHOD.
# If SC_QL_REMOTE_METHOD is not set, we flag an error. 
#	Return:
#		zero		Success
#		non-zero	Failure
#
######################################################################
execute_cmd()
{
	typeset nodename
	typeset command
	typeset args
	typeset method
	typeset tmpfile
	typeset tmperr

	integer pid=$$

	nodename=$1
	command=$2
	shift 2
	args="$*"

	tmpfile="ql_util.$pid.$pid"
	tmperr="ql_util.$pid$pid"

	rm -f /tmp/$tmpfile
	rm -f /tmp/$tmperr

	if [[ -z $nodename ]]; then
		printf "$(gettext '%s: Remote node is Null')\n" \
		    "${PROG}" | logerr
		return 1
	fi

	if [[ -z $command ]]; then
		printf "$(gettext '%s: Remote command is Null')\n" \
		    "${PROG}" | logerr
		return 1
	fi

	#
	# Set SC_QL_REMOTE_METHOD so that the next time we need to execute 
	# a remote command we dont have to do more work.
	#
	if [[ ! -z $SC_REMOTE_METHOD && $SC_REMOTE_METHOD != "scrcmd" ]]; then
		method=$SC_REMOTE_METHOD
	elif [[ ! -z $SC_QL_REMOTE_METHOD ]]; then
		method=$SC_QL_REMOTE_METHOD
	else 
		printf "$(gettext '%s: Failed to find the remote method')\n" \
		    "${PROG}" | logerr
		return 1
	fi

	# Run the command.
	case ${method} in
	rsh)   # rsh
		cmd="rsh ${nodename} -n \"/bin/sh -c '${command} ${args}; /bin/echo SC_COMMAND_STATUS=\\\$?'\""
		;;

	ssh)   # ssh
		cmd="ssh root@${nodename} -o \"BatchMode yes\" -n \"/bin/sh -c '${command} ${args}; /bin/echo SC_COMMAND_STATUS=\\\$?'\""
		;;
	esac
	printf "$(gettext '%s: Executing remote command %s')\n" "${PROG}" \
	    "${cmd}" | logdbgmsg
	
	eval ${cmd} >/tmp/${tmpfile} 2>/tmp/${tmperr}

	result=$(sed -n 's/^SC_COMMAND_STATUS=//p' /tmp/${tmpfile})

	if [[ -z $result ]]; then
		result=1
	fi
	if [[ "$result" != "0" ]]; then
		return 1
	fi
	rm -f /tmp/$tmpfile
	rm -f /tmp/$tmperr
	return 0
}

###################################################################
#
# duplicate() [args ...]
#
#	args - a list of 0-n arguments
#
#	This function returns non-zero if any two
#	arguments in the list match. That is,
#	if duplicate args are found.
#
###################################################################
duplicate()
{
	typeset arglist;  set -A arglist $*

	integer i=0
	integer j

	while [[ -n "${arglist[i]}" ]]
	do
		((j = i + 1))
		while [[ -n "${arglist[j]}" ]]
		do
			if [[ "${arglist[i]}" == "${arglist[j]}" ]]; then
				return 1
			fi
			((j += 1))
		done
		((i += 1))
	done

	return 0
}

##################################################################
# read_partition_data $data
#
#	Retrieve data from the data file. 
#
#       Return:
#               zero            Success
#               non-zero        Failure
#
#################################################################
read_partition_data()
{
	# partition A.
	typeset partition_a_nodes
	typeset partition_a_root_node

	# partition B.
	typeset partition_b_nodes
	typeset partition_b_root_node

	# transport release version.
	typeset sc_transport_version

	# Node with leat nodeid
	typeset least_nodeid_node

	# local variables.
	typeset key
	typeset value
	typeset data=$1
	
	#
	# Read the data required for this command from 
	# $QL_DATA_FILE_PATH/$QL_DATA_FILE.
	# We need from this file the root nodes of both partitions
	# and the node lists of both partitions.
	# The file has the format of a key-value pair:
	# Key for the root node of partition A is PARTITION_A_ROOT.
	# Key for the root node of partition B is PARTITION_B_ROOT.
	# Key for a node of partition B is PARTITION_B_NODE.
	# Key for a node of partition A is PARTITION_A_NODE.
	#

	if [[ ! -f $QL_DATA_FILE_PATH/$QL_DATA_FILE ]]; then
		printf "$(gettext '%s: %s does not exist\n')" "${PROG}" \
		    "$QL_DATA_FILE_PATH/$QL_DATA_FILE" | logerr
		return 1
	fi
	#
	# Read each line of the file.
	#
        while read line
        do
               	# Skip comments and blank lines
               	case ${line} in

               	'#'* | '')	# skip.
				continue
				;;
                esac

		key=`/usr/bin/echo $line | awk '{ print $1 }'`

		if [[ $? -ne 0 ]]; then
			printf "$(gettext '%s: Error while reading %s\n')" \
			    "${PROG}" "$QL_DATA_FILE_PATH/$QL_DATA_FILE" \
			    | logerr
			return 1
		fi
			
		value=`/usr/bin/echo $line | awk '{ print $2 }'`

		if [[ $? -ne 0 ]]; then
			printf "$(gettext '%s: Error while reading %s\n')" \
			    "${PROG}" "$QL_DATA_FILE_PATH/$QL_DATA_FILE"  \
			    | logerr
			return 1
		fi

		case $key in

		'partition_b_root')	# root node of partition B.
					partition_b_root_node=$value
					;;

		'partition_a_root') 	# root node of partition A.
					partition_a_root_node=$value
					;;

		'partition_b_node')	# partition B node.
					if [[ -z $partition_b_nodes ]]; then
						partition_b_nodes=$value
					else
						partition_b_nodes="$partition_b_nodes $value"
					fi
					;;
		'partition_a_node')	# partition A node.
					if [[ -z $partition_a_nodes ]]; then
						partition_a_nodes=$value
					else
						partition_a_nodes="$partition_a_nodes $value"
					fi
					;;
		'sc_transport_version')	# Transport release version.
					sc_transport_version=$value
					;;
		'least_nodeid_node')	# Least node id node
					least_nodeid_node=$value
					;;
		*)			# Got a Key that we dont recognise. The
					# Data file is probably corrupted.
					# Return an error.
					#
					printf "$(gettext '%s: Incorrect key %s in %s.')" "${PROG}" $key "$QL_DATA_FILE_PATH/$QL_DATA_FILE" | logerr
					;;
		esac

	done < ${QL_DATA_FILE_PATH}/${QL_DATA_FILE}

	#
	# Based on what is required, return only that data.
	#
	case ${data} in

	1)
		/usr/bin/echo $partition_b_root_node
		;;
	2)
		/usr/bin/echo $partition_b_nodes
		;;
	3)
		/usr/bin/echo $partition_a_root_node
		;;
	4)
		/usr/bin/echo $partition_a_nodes
		;;
	5)
		/usr/bin/echo $sc_transport_version
		;;
	6)
		/usr/bin/echo $least_nodeid_node
		;;
	esac

	return 0
}

###############################################################
# read_partition_B
#
#	Read the nodes of partition B from the Data file.
#
#	Return:
#		zero            Success
#		non-zero        Failure
#
###############################################################
read_partition_B()
{
	typeset partition_b_nodes

	partition_b_nodes=`read_partition_data 2`

	if [[ $? -ne 0 ]]; then
		printf \
		    "$(gettext '%s: Failed to read from file \"%s\"')\n" \
		    "${PROG}" "$QL_DATA_FILE_PATH/$QL_DATA_FILE" | logerr
		return 1
	fi
	/usr/bin/echo $partition_b_nodes
	return 0
}

################################################################
# read_partition_A
#
#	Read the nodes of partition A from the Data file.
#
#	Return:
#		zero            Success
#		non-zero        Failure
#
#################################################################
read_partition_A()
{
	typeset partition_a_nodes

	partition_a_nodes=`read_partition_data 4`

	if [[ $? -ne 0 ]]; then
		printf "$(gettext '%s: Failed to read from file \"%s\"')\n" \
		    "${PROG}" "$QL_DATA_FILE_PATH/$QL_DATA_FILE" | logerr
		return 1
	fi
	/usr/bin/echo $partition_a_nodes
	return 0
}

##################################################################
# read_partition_B_root
#
#	Read the root node of partition B from the Data file.
#
#	Return:
#		zero            Success
#		non-zero        Failure
#
##################################################################
read_partition_B_root()
{
	typeset partition_b_root_node

	partition_b_root_node=`read_partition_data 1`

	if [[ $? -ne 0 ]]; then
		printf "$(gettext '%s: Failed to read from file \"%s\"')\n" \
		    "${PROG}" "$QL_DATA_FILE_PATH/$QL_DATA_FILE" | logerr
		return 1
	fi
	/usr/bin/echo $partition_b_root_node
	return 0
}

##################################################################
# read_partition_A_root
#
#	Read the root node of partition A from the Data file.
#
#	Return:
#		zero            Success
#		non-zero        Failure
#
#################################################################
read_partition_A_root()
{
	typeset partition_a_root_node

	partition_a_root_node=`read_partition_data 3`

	if [[ $? -ne 0 ]]; then
		printf "$(gettext '%s: Failed to read from file \"%s\"')\n" \
		    "${PROG}" "$QL_DATA_FILE_PATH/$QL_DATA_FILE" | logerr
		return 1
	fi
	/usr/bin/echo $partition_a_root_node
	return 0
}

##################################################################
# read_transport_version
#
#	Read the transport release version from the Data file.
#
#	Return:
#		zero            Success
#		non-zero        Failure
#
#################################################################
read_transport_version()
{
	typeset transport_version

	transport_version=`read_partition_data 5`

	if [[ $? -ne 0 ]]; then
		printf "$(gettext '%s: Failed to read from file \"%s\"')\n" \
		    "${PROG}" "$QL_DATA_FILE_PATH/$QL_DATA_FILE" | logerr
		return 1
	fi
	/usr/bin/echo $transport_version
	return 0
}


##################################################################
# read_least_nodeid_node
#
#	Read the least node id node from the Data file.
#
#	Return:
#		zero            Success
#		non-zero        Failure
#
#################################################################
read_least_nodeid_node()
{
	typeset least_nodeid_node

	least_nodeid_node=`read_partition_data 6`

	if [[ $? -ne 0 ]]; then
		printf "$(gettext '%s: Failed to read from file \"%s\"')\n" \
		    "${PROG}" "$QL_DATA_FILE_PATH/$QL_DATA_FILE" | logerr
		return 1
	fi
	/usr/bin/echo $least_nodeid_node
	return 0
}


#################################################################
#
# ign_sig
#
# signal handler that masks SIGINT and SIGHUP
#
#################################################################
ign_sig()
{
	printf "$(gettext '%s: Ignoring signal')\n" "${PROG}" | logdbgmsg
}

#################################################################
#
# sc_zones_check
#
# Check if we are running from a non global zone. If so, exit after
# logging the error message.
#
#	Return:
#		zero		Success 
#		non-zero	Failure
#
#################################################################
sc_zones_check()
{
	typeset zone_name

	if [[ -f /usr/bin/zonename ]]; then
		zone_name=`/usr/bin/zonename`
	else
		# Assume no zone support.
		return 0
	fi

	case "${zone_name}" in
	'global')
		return 0
		;;
	esac

	printf "$(gettext '%s: Command not supported from non-global zone')\n" \
	    "${PROG}"

	exit 1
}

################################################################
#
# change_vfstab $vfstab
#
# vfstab	The location of the vfstab file
#
# This function changes the $vfstab file. It comments out
# entries of any QFS file system that mounts at boot time.
#
#
#	Return:
#		zero		Success
#		non-zero	Failure
#
################################################################
change_vfstab()
{
	# data elements in a vfstab entry. 
	typeset special
	typeset fsckdev
	typeset mountp
	typeset type
	typeset pass
	typeset boot
	typeset options
	
	# local variable.
	typeset line
	typeset vfstab_file=$1

	/usr/bin/rm -f /tmp/vfstab.$$
	touch /tmp/vfstab.$$ 

	#
	# Read each entry of vfstab file and change it if required.
	#
	while read special fsckdev mountp type pass boot options
	do
		# Skip comments and blank lines

		case ${special} in

		'#'* | '')      # We add a "#" to the commented line to
				# distinguish this later on.

				printf \
				"$(gettext '%s\t%s\t%s\t%s\t%s\t%s\t%s')\n" \
				    "#$special" "$fsckdev" "$mountp" \
				    "$type" "$pass" "$boot" \
				    "$options" >> /tmp/vfstab.$$

				continue
				;;
		esac

		if [[ $boot = "yes" ]]; then

			if [[ $type = "samfs" || $type = "qfs" ]]; then
				#
				# Found a shared QFS filesystem entry.
				# Comment it out.
				#
				printf "$(gettext '%s: Commenting out %s')\n" \
				    "${PROG}" "{$special}" | logdbgmsg

				printf \
				"$(gettext '%s\t%s\t%s\t%s\t%s\t%s\t%s')\n" \
				    "#$special" "$fsckdev" "$mountp" \
				    "$type" "$pass" "$boot" \
				    "$options" >> /tmp/vfstab.$$

			else
				#
				# mount at boot option is set but this is
				# not a QFS filesystem. So we dont do
				# anything here except for putting the line
				# back to the file.
				#
				printf \
				"$(gettext '%s\t%s\t%s\t%s\t%s\t%s\t%s')\n" \
				    "$special" "$fsckdev" "$mountp" \
				    "$type" "$pass" "$boot" \
				    "$options" >> /tmp/vfstab.$$

			fi
		else
			#
			# mount at boot option is not set. So we dont have
			# to do anything here except for putting the line
			# back into the file.
			#
			printf \
			"$(gettext '%s\t%s\t%s\t%s\t%s\t%s\t%s')\n" \
			    "$special" "$fsckdev" "$mountp" \
			    "$type" "$pass" "$boot" \
			    "$options" >> /tmp/vfstab.$$
			
		fi

	done < ${vfstab_file}

	#
	# If an error happened, log an error and return failure.
	#
	if [[ $? -ne 0 ]]; then

		printf "$(gettext '%s: Error changing vfstab')\n" \
		    "${PROG}" | logerr
		return 1

	fi

	#
	# Here we copy the changes back to the vfstab file.
	#
	cp /tmp/vfstab.$$ ${vfstab_file} || return 1

	printf "$(gettext '%s: Successfully modified %s')\n" \
	    "${PROG}" "${vfstab_file}" | logdbgmsg

	#
	# return success.
	#
	return 0
}


################################################################
#
# get_file_lock $node $file_name
#
# vfstab	The location of the vfstab file
#
# The function will try to grab a file lock on the
# node specified in the first parameter.
#
#
#	Return:
#		zero		Success
#		non-zero	Failure
#
################################################################
get_file_lock()
{
	typeset hostname=`/usr/bin/hostname`
	typeset node=${1}
	typeset file_name=${2}
	typeset retval=0

	if [[ ${node} == ${hostname} ]]; then
		${QL_LIB_PATH}${QL_GET_FILE_LOCK} ${file_name}
	else
		
		execute_cmd ${node} \
		    ${QL_LIB_PATH}${QL_GET_FILE_LOCK} ${file_name}
	fi

	retval=$?

	return ${retval}
}


#################################################################
#
# umount_and_activate_new_be() $mount_point
#
# This function will unmount the new boot environment and then
# activate the new boot environment. It takes as parameter
# the mount point for the boot environment.
#
# For the function to work properly, the boot environment
# should be mounted.
#
#	Return:
#		zero		Success
#		non-zero	Failure
#################################################################
umount_and_activate_new_be()
{

	typeset mount_point=`echo $1 | sed -e 's/\/$//g'`
	typeset lumount_op="/tmp/lumount_op.$$"
	typeset new_be=
	typeset token_1=
	typeset token_3=
	typeset retval=0


	#
	# Check if mount point is supplied
	#
	if [[ $# != 1 ]]; then 	
		printf "$(gettext '%s: Need a mount point')\n" \
		    "${PROG}" | logerr
		return 1
	fi

	#
	# Check that the mount point supplied is not / (root)
	#
	if [[ "${mount_point}" == "/" ]]; then
		printf "$(gettext '%s: Mount point cannot be /')\n" \
		    "${PROG}" | logerr
		return 1
	fi

	#
	# Check if the mount point is a valid mount point
	#
	if [[ ! -d "${mount_point}/usr/cluster/bin" ]]; then
		printf "$(gettext '%s: %s is not a valid mount point')\n" \
		    "${mount_point}" "${PROG}" | logerr
		return 1
	fi

	#
	# Map the mount point to the boot environment
	#
	/usr/sbin/lumount > $lumount_op
	retval=$?
	if [[ "${retval}" != 0 ]]; then
		printf \
		    "$(gettext '%s: lumount failed with return value %d')\n" \
		    "${PROG}" "${retval}" | logerr
		rm -f ${lumount_op}
		return 1
	fi

	#
	# Read the lumount output and store the mount point.
	# lumount o/p looks like 
	# c0t0d0s0 on /
	# new_be  on /mnt/altroot
	#
	# hence the first token is the name of the boot environment and
	# the third token is the name of the mount point.
	#
	while read line
	do
        	token_1=`echo $line | awk '{print $1}'`
        	token_3=`echo $line | awk '{print $3}'`

        	if [[ "${token_3}" == "${mount_point}" ]]; then
                	new_be="${token_1}"
                	break
        	fi
	done < ${lumount_op}

	#
	# Delete the temporary file
	#
	rm -f ${lumount_op}

	#
	# Check that the boot environment was found
	#
	if [[ -z "${new_be}" ]]; then
		printf "$(gettext \
		    '%s: %s could not be mapped to a boot environment')\n" \
		    "${PROG}" "${mount_point}" | logerr
		return 1
	fi

	printf "$(gettext \
	    '%s: Mount Point = %s Boot Environment = %s')\n" \
	    "${PROG}" "${mount_point}" "${new_be}" | logdbgmsg

	#
	# Unmount the new boot environment
	#
	/usr/sbin/luumount -f -n "${new_be}"
	retval=$?
	if [[ "${retval}" != 0 ]]; then
		printf "$(gettext \
		    '%s: luumount failed with return value %d')\n" \
		    "${PROG}" "${retval}" | logerr
		printf "$(gettext \
		    '%s: Failed to unmount boot environment %s')\n" \
		    "${PROG}" "${new_be}" | logerr
		return 1

	fi

	printf "$(gettext \
	    '%s: Successfully unmounted the boot environment %s')\n" \
	    "${PROG}" "${new_be}" | logdbgmsg
	
	#
	# Activate the new boot environment
	#
	/usr/sbin/luactivate "${new_be}"
	retval=$?
	if [[ "${retval}" != 0 ]]; then
		printf "$(gettext \
		    '%s: luactivate failed with return value %d')\n" \
		    "${PROG}" "${retval}" | logerr
		printf "$(gettext \
		    '%s: Failed to activate boot environment %s')\n" \
		    "${PROG}" "${new_be}" | logerr
		return 1

	fi

	printf "$(gettext \
	    '%s: Successfully activated the boot environment %s')\n" \
	    "${PROG}" "${new_be}" | logdbgmsg

	#
	# Success
	#
	return 0
}
