#! /bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#pragma ident	"@(#)ql_cleanup.ksh	1.7	08/05/20 SMI"
#
# ql_cleanup.ksh
#
# ql_cleanup [-f] | [-n node1,node2...]
#
# This script is executed in cluster mode. This is executed on the
# root node of partition A when the Dual-partition upgrade is complete.
# It resets the quorum votes of all the nodes and in effect the
# quorum devices that were put into maintainance state while starting
# the upgrade. It also cleans up the files that have the upgrade state
# information on all the nodes.
# The option -f specifies that the cleanup need to happen only on
# the current node. The option -n specifies a list of nodes excluding
# the local node on which the cleanup happens. Executing this script
# without any options results in cleanup of the state on all the nodes
# currenly in the cluster.
#

###############################################################
#
# Variable globals
#
###############################################################
integer -r SC_TRUE=1
integer -r SC_FALSE=0
integer -r retry_attempts=3

typeset HOSTNAME_CMD=/usr/bin/hostname
typeset CLUSTER_LIB_PATH=/usr/cluster/bin
typeset QL_UTIL_PATH=/usr/cluster/lib/scadmin/ql
typeset QL_UTIL_FILE=ql_util

typeset node_list
typeset hostname

typeset data_file
typeset status_file

typeset partition_a_root_node
typeset partition_b_root_node
typeset least_nodeid_node

#
# Live Upgrade Mode is by default
# set to false
#
integer LU_MODE=${SC_FALSE}

# Program name and args list. Used for log messages.
if [[ -z $PROG ]]; then
	typeset -r PROG=${0##*/}
fi

#####################################################
#
# loadlib() libname
#
#	Load the named include file.
#
#	Return:
#		zero		Success
#		non-zero	Failure
#
#####################################################
loadlib()
{
	typeset libname=$1

	# Load the library
	. ${libname}

	if [[ $? != 0 ]]; then
		printf "$(gettext '%s:  Unable to load \"%s\"')\n" "${PROG}" \
		    "${libname}" >&2
		return 1
	fi
}

########################################################
#
# print_usage
#
#	Print usage message to stderr
#
########################################################
print_usage()
{
	echo "$(gettext 'usage'): ${PROG} [[-n node,node...]|[-f]]" >&2
}

##############################################################
#
# reset_quorum joined_nodes
#
#	Restores the node votes and enables quorum devices.
#
#	Return:
#		zero		Success
#		non-zero	Failure
#
##############################################################
reset_quorum()
{
	typeset node
	typeset tmpfile
	typeset tmperr
	typeset quorum_device
	typeset joined_nodes
	integer ret

	joined_nodes="$*"

	#
	# Reset the node vote for all the nodes to 1.
	#
	for node in $joined_nodes
	do
		#
		# The node vote count is reset to one.
		#
		printf "$(gettext '%s: Change node vote of %s to 1')\n" \
		    "${PROG}" "${node}" | logdbgmsg

		${CLUSTER_CMD_PATH}/scconf -c -q node=$node,defaultvote=1 \
		    > /dev/null 2>&1

		ret=$?
		#
		# Handle command failure. This is not a fatal error and we can
		# ignore it after logging a message.
		#
		if [[ $ret -ne 0 ]]; then
			printf \
			  "$(gettext '%s: scconf returned an error, errval = %d for %s')\n" "${PROG}" "${ret}" "${node}" | logerr
                fi
	done

	tmpfile="ql.$pid.$pid"
	rm -f /tmp/$tmpfile

	#
	# The Dual-partition upgrade procedure would have resulted in
	# all the configured  quorum devices to be put in maintainance
	# mode. Here we will try and bring all those quorum devices online.
	#

        #
        # check if the command file exists.
        #
        if [[ ! -x ${CLUSTER_CMD_PATH}/scstat ]]; then
                printf "$(gettext '%s: Could not find %s')\n" "${PROG}" \
                    "scstat" | logerr
                return 1
        fi
	#
	# Parse and redirect the list of DID devices to the 
	# temorary file.
	#
	${CLUSTER_CMD_PATH}/scstat -q  | \
	    /usr/bin/awk '/Device votes:/ {print $3}' > /tmp/$tmpfile

	ret=$?
	#
	# If the command fails, handle failure.
	#
	if [[ $ret -ne 0 ]]; then
		printf "$(gettext '%s: Failed to get quorum device list. scstat returned an error, errval = %d')\n" "${PROG}" "${ret}" | logerr
		return 1
	fi
		
        #
        # check if the command file exists.
        #
        if [[ ! -x ${CLUSTER_CMD_PATH}/scconf ]]; then
                printf "$(gettext '%s: Could not find %s')\n" "${PROG}" \
                    "scconf" | logerr
                return 1
        fi

	#
	# Iterate through each quorum device configured and currently
	# in maintainance mode to online state.
	# 
	for quorum_device in `/usr/bin/cat /tmp/$tmpfile`
	do
		printf "$(gettext '%s: Change %s to online state')\n" \
		    "${PROG}" "${quorum_device}" | logdbgmsg

		${CLUSTER_CMD_PATH}/scconf -c -q globaldev=$quorum_device,reset
		ret=$?
		#
		# This is not a fatal error. We can just ignore it here.
		#
		if [[ $ret -ne 0 ]]; then
			printf "$(gettext '%s:scconf returned an error, errval = %d')\n" "${PROG}" "${ret}" | logerr
		fi
	done

	#
	# cleanup
	#
	/usr/bin/rm -f /tmp/$tmpfile
}

##############################################################
#
# ql_change_ccr_with_retries
#
#	Removes the entries specific to upgrade.
#
#	Return:
#		zero		Success
#		non-zero	Failure
#
##############################################################
ql_change_ccr_with_retries()
{
	typeset num_attempts=${retry_attempts}

	export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${CLUSTER_LIB_PATH}

	while [[ $num_attempts -ne 0 ]];
	do
		${QL_LIB_PATH}/ql_change_ccr -c > /dev/null 2>&1

		#
		# Handle command failure.
		#
		if [[ $? -ne 0 ]]; then
			printf "$(gettext '%s: Failed to clean up the CCR')\n" \
			    "${PROG}"| logerr
			(( num_attempts = num_attempts - 1 ))
			sleep 10
		else
			break
		fi
	done

	#
	# If all retries failed, flag an error and return failure.
	#
	if [[ $num_attempts -eq 0 ]]; then
		printf
		    "$(gettext '%s: The retries to clean CCR failed')\n" \
		    "${PROG}" | logdbgmsg
		return 1 
	fi
	return 0
}

##############################################################
#
# find_remote_method_with_retries
#
#	Find whether to use RSH or SSH to remote copy files and
#	remote execute commands.
#
#	Return:
#		zero		Success
#		non-zero	Failure
#
##############################################################
find_remote_method_with_retries()
{
	typeset num_sttempts=${retry_attempts}
	typeset nodes="$*"

	while [[ $num_attempts -ne 0 ]];
	do
		find_remote_method $nodes

		if [[ $? -ne 0 ]]; then
			printf
			  "$(gettext '%s: Failed to find a remote method')\n" \
			  "${PROG}" | logerr
			(( num_attempts = num_attempts - 1 ))
			sleep 10
		else
			break
		fi
	done

	#
	# If all retries failed, flag an error and return failure.
	#
	if [[ $num_attempts -eq 0 ]]; then
		printf
		    "$(gettext '%s: The retries to find remote method failed')\n" \ "${PROG}" | logdbgmsg
		return 1 
	fi
	return 0
}

##############################################################
#
# remove_ql_files
#
#	Will delete the QL related data, status and lock files
#	on the local host. Will also delete QL LU related
#	state files.
#
#	The function should be called after setting
#	the hostname, partition A root node,
#	partition B root node and least nodeid node.
#
#	Return:
#		zero		Success
#		non-zero	Failure
#
##############################################################
remove_ql_files()
{
	#
	# Delete the QL data and status files
	#
	/usr/bin/rm -f $data_file $status_file

	if [[ $? -ne 0 ]]; then
		printf "$(gettext \
		    '%s: failed to remove status files on node %s')\n" \
		    "${PROG}" "${hostname}" | logerr
		return 1
	fi

	#
	# Delete the $QL_APPLY_RUN_LOCK_FILE file on the 
	# root node of the first partition
	#
	if [[ ${hostname} == ${partition_a_root_node} ]]; then

		/usr/bin/rm -f ${QL_APPLY_RUN_LOCK_FILE}
		
		if [[ $? -ne 0 ]]; then
			printf "$(gettext \
			    '%s: failed to remove %s file on node %s')\n" \
			    "${PROG}" "${QL_APPLY_RUN_LOCK_FILE}" \
			    "${hostname}" | logerr
			return 1
		fi

	fi

	#
	# Delete the $QL_APPLY_RUN_LOCK_FILE file on the 
	# root node of the second partition
	#
	if [[ ${hostname} == ${partition_b_root_node} ]]; then

		/usr/bin/rm -f ${QL_APPLY_RUN_LOCK_FILE}

		if [[ $? -ne 0 ]]; then
			printf "$(gettext \
			    '%s: failed to remove %s file on node %s')\n" \
			    "${PROG}" "${QL_APPLY_RUN_LOCK_FILE}" \
			    "${hostname}" | logerr
			return 1
		fi

	fi

	#
	# Delete the $QL_BEGIN_RUN_LOCK_FILE file on the
	# node with the least nodeid
	#
	if [[ ${hostname} == ${least_nodeid_node} ]]; then
		
		/usr/bin/rm -f ${QL_BEGIN_RUN_LOCK_FILE}

		if [[ $? -ne 0 ]]; then
			printf "$(gettext \
			    '%s: failed to remove %s file on node %s')\n" \
			    "${PROG}" "${QL_BEGIN_RUN_LOCK_FILE}" \
			    "${hostname}" | logerr
			return 1
		fi

	fi

	#
	# Delete the QL LU related files
	#
	if [[ ${LU_MODE} -eq ${SC_TRUE} ]]; then

		/usr/bin/rm -f ${QL_LU_CONTINUE} ${QL_TRANSFORM_CCR_FILE}

		if [[ $? -ne 0 ]]; then
			printf "$(gettext \
		    	'%s: failed to remove files %s and/or %s on node %s')\n" \
		    	"${PROG}" "${QL_LU_CONTINUE}" \
			"${QL_TRANSFORM_CCR_FILE}" "${hostname}" | logerr
			return 1
		fi

	fi

	#
	# Success
	#
	return 0
}

##############################################################
#
# remove_ql_files_remote $node_list
#
#	Will delete the QL related data, status and lock files
#	on the remote hosts which are online. Will also delete
#	QL LU related state files on the online nodes.
#
#	The function should be called after setting the
#	remote method, the hostname, partition A root node,
#	partition B root node and least nodeid node.
#
#	Parameters:
#
#	node_list: List of online nodes.
#
#	Return:
#		zero		Success
#		non-zero	Failure
#
##############################################################
remove_ql_files_remote()
{
	typeset node_list="$*"
	typeset node

	for node in $node_list
	do
		if [[ ${node} == ${hostname} ]]; then
			#
			# Do nothing for the local node
			#
			continue
		fi	

		#
		# Delete the QL data and status files
		#
		execute_cmd $node /usr/bin/rm -f $data_file \
		    $status_file

		if [[ $? -ne 0 ]]; then
			printf "$(gettext \
			    '%s: failed to remove status files on node %s')\n" \
			    "${PROG}" "${node}" | logerr
			return 1
		fi

		#
		# Delete the $QL_APPLY_RUN_LOCK_FILE file on the 
		# root node of the first partition
		#
		if [[ ${node} == ${partition_a_root_node} ]]; then

			execute_cmd ${node} \
			    /usr/bin/rm -f ${QL_APPLY_RUN_LOCK_FILE}
			
			if [[ $? -ne 0 ]]; then
				printf "$(gettext \
				'%s: failed to remove %s file on node %s')\n" \
				"${PROG}" "${QL_APPLY_RUN_LOCK_FILE}" \
				"${node}" | logerr
				return 1
			fi

		fi

		#
		# Delete the $QL_APPLY_RUN_LOCK_FILE file on the 
		# root node of the second partition
		#
		if [[ ${node} == ${partition_b_root_node} ]]; then

			execute_cmd ${node} \
			    /usr/bin/rm -f ${QL_APPLY_RUN_LOCK_FILE}

			if [[ $? -ne 0 ]]; then
				printf "$(gettext \
				'%s: failed to remove %s file on node %s')\n" \
				"${PROG}" "${QL_APPLY_RUN_LOCK_FILE}" \
				"${node}" | logerr
				return 1
			fi

		fi

		#
		# Delete the $QL_BEGIN_RUN_LOCK_FILE file on the
		# node with the least nodeid
		#
		if [[ ${node} == ${least_nodeid_node} ]]; then

			execute_cmd ${node} \
			    /usr/bin/rm -f ${QL_BEGIN_RUN_LOCK_FILE}

			if [[ $? -ne 0 ]]; then
				printf "$(gettext \
				'%s: failed to remove %s file on node %s')\n" \
				"${PROG}" "${QL_BEGIN_RUN_LOCK_FILE}" \
				"${node}" | logerr
				return 1
			fi

		fi

		#
		# Delete the QL LU related files
		#
		if [[ ${LU_MODE} -eq ${SC_TRUE} ]]; then

			execute_cmd ${node} \
			    /usr/bin/rm -f  \
			    ${QL_LU_CONTINUE} ${QL_TRANSFORM_CCR_FILE}

			if [[ $? -ne 0 ]]; then
				printf "$(gettext \
		    		    '%s: failed to remove files %s and/or %s on node %s')\n" \
		    		    "${PROG}" "${QL_LU_CONTINUE}" \
				    "${QL_TRANSFORM_CCR_FILE}" "${node}" \
				    | logerr
				return 1
			fi
		
		fi

	done

	#
	# Success
	#
	return 0
}


##############################################################
#
# main
#
#############################################################
main()
{
	typeset tmpfile
	typeset tmperr
	typeset online_nodes
	typeset curr_nodes

	integer file_mode=${SC_FALSE}
	integer nodelist_opt=${SC_FALSE}
	integer num_attempts=${retry_attempts}
	integer ret
	
	#
	# Load common functions
	#
	loadlib $QL_UTIL_PATH/$QL_UTIL_FILE > /dev/null 2>&1

	if [[ $? -ne 0 ]]; then
		return 1
	fi 	

	#
	# Global zone privilege check. If we are running in a non-global
	# we exit here.
	#
	sc_zones_check

	data_file=$QL_DATA_FILE_PATH/$QL_DATA_FILE 
	status_file=$QL_STATUS_FILE_PATH/$QL_STATUS_FILE

	while getopts fn: c 2>/dev/null
	do
		case ${c} in
                f)      # File mode.
                        file_mode=${SC_TRUE}
                        ;;
		n)	# List of nodes in the cluster.
			nodelist_opt=${SC_TRUE}
			online_nodes="$(IFS=, ; echo ${OPTARG})"
			;;
                \?)     print_usage
                        return 1
                        ;;
                *)      # bad option
                        print_usage
                        return 1
                        ;;
		esac
	done

	#
	# Check to see if we are root.
	#
	verify_isroot

	if [[ $? -ne 0 ]]; then
		return 1
	fi

	#
	# Check if command exists
	#
	if [[ ! -x /usr/sbin/clinfo ]]; then
		printf "$(gettext '%s: Could not find %s')\n" "${PROG}" \
		    "clinfo" | logerr
		return 1
	fi

	#
	# Check for cluster mode.
	#
	/usr/sbin/clinfo > /dev/null 2>&1

	if [[ $? -ne 0 ]]; then
		printf "$(gettext '%s: Node must be in cluster mode')\n" \
		    "${PROG}" | logerr
		return 1
	fi

	printf "$(gettext '%s: Starting execution')\n\n" "$PROG" | logdbgmsg

	#
	# check if the command exists.
	#
	if [[ ! -x ${HOSTNAME_CMD} ]]; then
                printf "$(gettext '%s: Could not find %s')\n" "${PROG}" \
		    "${HOSTNAME_CMD}" | logerr
                return 1
        fi

	#
	# Get the hostname for this node.
	#	
	hostname=`${HOSTNAME_CMD}`

	#
	# Read the root node of partition A.
	#
	partition_a_root_node=`read_partition_A_root`

	if [[ $? -ne 0 ]]; then
		return 1
	fi

	#
	# Read the root node of partition B.
	#
	partition_b_root_node=`read_partition_B_root`

	if [[ $? -ne 0 ]]; then
		return 1
	fi

	#
	# Read least nodeid node
	#
	least_nodeid_node=`read_least_nodeid_node`
	
	if [[ $? -ne 0 ]]; then
		return 1
	fi

	#
	# Check if QL LU is going on.
	# If the $QL_LU_CONTINUE file is
	# present then QL LU is going on.
	#
	if [[ -f ${QL_LU_CONTINUE} ]]; then
		LU_MODE=${SC_TRUE}
	fi

	if [[ $file_mode -eq ${SC_TRUE} ]]; then
		
		#
		# The node vote count is reset to one.
		#
		printf "$(gettext '%s: Change node vote of %s to 1')\n" \
		    "${PROG}" "${hostname}" | logdbgmsg

		${CLUSTER_CMD_PATH}/scconf -c -q \
		    node=$hostname,defaultvote=1 > /dev/null 2>&1

		ret=$?
		#
		# Handle command failure.
		#
		if [[ $ret -ne 0 ]]; then
			printf \
			  "$(gettext '%s: scconf returned an error, errval = %d for %s')\n" "${PROG}" "${ret}" "${hostname}" | logerr
			return 1
		fi

		#
		# Delete the QL status, data and lock files
		# from the local node
		#
		remove_ql_files

		if [[ $? -ne 0 ]]; then
			return 1
		fi

		printf "$(gettext '%s: Completed execution')\n\n" "$PROG" \
		    | logdbgmsg

		return 0
	fi
		
	#
	# Get the list of nodes configured in the cluster.
	#

	tmpfile="ql.$pid.$pid"
	rm -f /tmp/$tmpfile

	tmperr="ql.$pid$pid"
	rm -f /tmp/$tmpfile

	# save and reset the locale
	LC_ALL_BKUP=$LC_ALL
	LC_ALL="C"

	${CLUSTER_CMD_PATH}/scstat -n | grep Online | \
	    awk '/Cluster node:/ {print $3}' >/tmp/$tmpfile 2>/tmp/$tmperr

	ret=$?

	#restore the locale
	LC_ALL=$LC_ALL_BKUP

	#
	# If the command fails, handle failure.
	#
	if [[ $ret -ne 0 ]]; then
		printf "$(gettext '%s: Failed to get cluster nodes. scstat returned an error, errval = %d')\n" "${PROG}" "${ret}" | logerr
		return 1
	fi

	#
	# Restore the node votes and enable quorum devices.
	#
	if [[ $nodelist_opt -eq ${SC_TRUE} ]]; then

		node_list=$online_nodes

		printf "$(gettext '%s: Online nodes : %s')\n" "${PROG}" \
		    "${online_nodes}" | logdbgmsg
	else
		#
		# Generate the list of nodes that are Online.
		#
		for node in `cat /tmp/$tmpfile`
		do
			if [[ -z $curr_nodes ]]; then
				curr_nodes=$node
			else
				curr_nodes="$curr_nodes $node"
			fi
		done

		rm -rf /tmp/$tmpfile
		rm -rf /tmp/$tmperr

		printf "$(gettext '%s: Joined nodes : %s')\n" "${PROG}" \
		    "${curr_nodes}" | logdbgmsg

		node_list=$curr_nodes
	fi

	reset_quorum  $node_list

	if [[ $? -ne 0 ]]; then
		printf "$(gettext '%s: quorum reset failed')\n" "${PROG}" \
		    | logdbgmsg 
		return 1
	fi

	#
	# Run ql_change_ccr. This will cleanup the CCR infrastructure file
	# of Dual-partition upgrade entries. Retry in case of failure.
	#
	ql_change_ccr_with_retries

	if [[ $? -ne 0 ]]; then
		printf "$(gettext '%s: Failed to clean up the CCR')\n" \
		    "${PROG}"| logerr
		return 1
	fi

	#
	# Find remote method of communication. Retry in case of failure.
	#
	find_remote_method_with_retries $node_list

	#
	# If all retries failed, flag an error and return failure.
	#
	if [[ $? -ne 0 ]]; then
		printf "$(gettext '%s: Failed to find a remote method')\n" \
		    "${PROG}"| logerr
		return 1
	fi

	#
	# Delete the QL status, data and lock files
	# from the local node
	#
	remove_ql_files
	
	if [[ $? -ne 0 ]]; then
		return 1
	fi	

	#
	# Remove the upgrade status file on all the nodes that have joined
	# the cluster. Also remove the QL related lock files on all the
	# nodes that have joined the cluster.
	#
	remove_ql_files_remote $node_list
	
	if [[ $? -ne 0 ]]; then
		return 1
	fi

	printf "$(gettext '%s: Completed execution')\n\n" "$PROG" | logdbgmsg

	return 0
}
main $*
