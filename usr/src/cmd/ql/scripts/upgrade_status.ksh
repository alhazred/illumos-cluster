#! /bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#pragma ident	"@(#)upgrade_status.ksh	1.3	08/05/20 SMI"
#
# upgrade_status.ksh
#
# upgrade_status
#
# This script displays the Dual partition upgrade state that is report if the
# upgrade is in progress or not.
#

###############################################################
#
# Variable globals
#
###############################################################

typeset QL_DATA_FILE_PATH=/etc/cluster/
typeset QL_DATA_FILE=ql_data.txt

#program name and args list. Used for log messages.
if [[ -z $PROG ]]; then
	typeset -r PROG=${0##*/}
fi

########################################################
#
# print_usage
#
#       Print usage message to stderr
#
########################################################
print_usage()
{
	echo "$(gettext 'usage'): ${PROG}" >&2
}

#######################################################
#
# main
#
#######################################################
main()
{
	typeset zone_name

	#
	# Check if executing from non-global zone. Executing from a non-global
	# zone is not allowed for this command.
	#
	if [[ -f /usr/bin/zonename ]]; then
		zone_name=`/usr/bin/zonename`

		if [[ "${zone_name}" != "global" ]]; then
			printf "$(gettext '%s: Command not supported from non-global zone')\n" "${PROG}"
			exit 1
		fi
	fi
		
	if [[ -f ${QL_DATA_FILE_PATH}/${QL_DATA_FILE} ]]; then
		printf "$(gettext '%s: Upgrade is in progress.')\n" "${PROG}"
	else
		printf "$(gettext '%s: Upgrade not in progress.')\n" "${PROG}"
	fi

	return 0
}
main $*
