#!/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#pragma ident	"@(#)data_change.ksh	1.7	08/08/01 SMI"
#
# data_change.ksh
#
# data_change
#
# The purpose of this script is to support any required
# upgrade of persistent data. The most common kind of data
# to be upgraded is CCR data.
#
# Place in this script according to the desired order
# of execution the commands to execute other
# programs/scripts that update persistent data.
#
# Note that scripts should be called from this script
# and should not be placed in this file.
#
# The default action of this script does nothing.
#

#
# Program name and args list.
#
if [[ -z $PROG ]]; then
        typeset -r PROG=${0##*/}
fi

#
# rgm_dc
# rgm_datachange binary invoked in this function does the
# CCR data convertion for the changes made as part of FBC's 1334/1403.
# The following are the changes made to the persistent data.
#
# 1. Nodelist property in each of rgm_rg_* tables has be changed
# from a list of nodenames to nodeid's.
# 2. On_off_switch and Monitored_switch now contain a list of nodeids on
# which the resource is enabled, instead of a single value indicating
# "enabled" or "disabled".
# 3. Installed_nodes in the rgm_rt_* tables would be changed from
# list of nodenames to node'ids.
# 4. Existing rgm_vp tables would be deleted from the CCR.
#
# Return value of 3 would imply that the CCR is already updated.
# For idempotency, the return value of 3 from rgm_datachange
# is treated as SUCCESS.
#
rgm_dc()
{
	typeset rc
	/usr/cluster/lib/sc/rgm_datachange
	rc=$?

	if [ $rc -ne 0 ] && [ $rc -ne 3]; then
		return $rc
	fi
	return 0

}


typeset zone_name

#
# Check if executing from non-global zone. Executing from a non-global
# zone is not allowed for this command.
#
if [[ -f /usr/bin/zonename ]]; then

	zone_name=`/usr/bin/zonename`

	if [[ "${zone_name}" != "global" ]]; then
		printf \
		    "$(gettext '%s: Command not supported from non-global zone')\n" \
		    "${PROG}"
		exit 1
	fi
fi

rgm_dc "${@:-}"
