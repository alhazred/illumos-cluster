#! /bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#pragma ident	"@(#)ql_stopapp.ksh	1.4	08/05/20 SMI"
#
# ql_stopapp.ksh
#
# ql_stopapp
#
# This script is executed in cluster mode. This is executed on the
# nodes of partition B. It invokes scswitch -F to stop all
# services on the node. It also executes custom scripts to take care
# of applications running outside the control of RGM (ex. Oracle RAC).  
#

###############################################################
#
# Variable globals
#
###############################################################

typeset QL_UTIL_PATH=/var/cluster/run
typeset QL_UTIL_FILE=ql_util

typeset HOSTNAME_CMD=/usr/bin/hostname

# Program name and args list. Used for log messages.
if [[ -z $PROG ]]; then
	typeset -r PROG=${0##*/}
fi

#####################################################
#
# loadlib() libname
#
#	Load the named include file.
#
#	Return:
#		zero		Success
#		non-zero	Failure
#
#####################################################
loadlib()
{
	typeset libname=$1

	# Load the library
	. ${libname}

	if [[ $? != 0 ]]; then
		printf "$(gettext '%s:  Unable to load \"%s\"')\n" "${PROG}" \
		    "${libname}" >&2
		return 1
	fi
}

####################################################
#
# main
#
####################################################
main()
{
	typeset RESOURCE_LIST=
	typeset resource_commalist=
        typeset zone_name
	integer ret

	#
	# Check if executing from non-global zone. Executing from a non-global
	# zone is not allowed for this command.
	#
	if [[ -f /usr/bin/zonename ]]; then

		zone_name=`/usr/bin/zonename`

		if [[ "${zone_name}" != "global" ]]; then
			printf "$(gettext '%s: Command not supported from non-global zone.')\n" "${PROG}"
			exit 1
		fi
	fi
	
	#
	# Load common functions
	#
	loadlib $QL_UTIL_PATH/$QL_UTIL_FILE > /dev/null 2>&1

	if [[ $? -ne 0 ]]; then
		return 1
	fi 	

	#
	# check to see if we are root.
	#
	verify_isroot

	if [[ $? -ne 0 ]]; then
		return 1
	fi

	#
	# check if command exists
	#
	if [[ ! -x /usr/sbin/clinfo ]]; then
		printf "$(gettext '%s: Could not find %s')\n" "${PROG}" \
		    "clinfo" | logerr
		return 1
	fi

	#
	# check for cluster mode.
	#
	/usr/sbin/clinfo > /dev/null 2>&1

	if [[ $? -ne 0 ]]; then
		printf "$(gettext '%s: Node must be in cluster mode')\n" \
		    "${PROG}"
		return 1
	fi

	printf "$(gettext '%s: Starting execution')\n\n" "$PROG" | logmsg

	#
	# Execute the "pre-application-shutdown" script of one exists.
	#
	if [[ -x ${QL_SHUTDOWN_APP_CMD_PATH}/$QL_PRE_SHUTDOWN_APP_CMD ]]; then
		printf "$(gettext '%s: Executing pre-shutdown script')\n" \
		    "${PROG}"
		${QL_SHUTDOWN_APP_CMD_PATH}/$QL_PRE_SHUTDOWN_APP_CMD > \
		    /dev/null 2>&1
		if [[ $? -ne 0 ]]; then
			printf \
			    "$(gettext '%s: %s returned a non-zero value')\n" \
			    "${PROG}" "${QL_PRE_SHUTDOWN_APP_CMD}"
		fi
	fi
		
	#
	# check if command exists.
	#
	if [[ ! -x ${CLUSTER_CMD_PATH}/scha_cluster_get ]]; then
		printf "$(gettext '%s: Could not find %s')\n" "${PROG}" \
		    "scha_cluster_get" | logerr
		return 1
	fi

	if [[ ! -x ${CLUSTER_CMD_PATH}/scha_resourcegroup_get ]]; then
		printf "$(gettext '%s: Could not find %s')\n" "${PROG}" \
		    "scha_resourcegroup_get" | logerr
		return 1
	fi

	#
	# Get the list of resource groups configured in the cluster.
	#
	RESOURCE_LIST="$(${CLUSTER_CMD_PATH}/scha_cluster_get -O \
	    ALL_RESOURCEGROUPS)"

	ret=$?

	if [[ -z $RESOURCE_LIST ]]; then
		if [[  $ret -ne 0 ]]; then 
			printf "$(gettext '%s: Could not get the resource group list, errval = %d')\n" "${PROG}" "${ret}" | logerr
			return 1
		else
			printf "$(gettext '%s: Resource group list is empty')\n" "${PROG}" | logmsg
			return 0
		fi
	fi

	#
	# Build a comma separated list of resources from the resource list.
	#
	for resource in $RESOURCE_LIST
	do
		resource_status="$(${CLUSTER_CMD_PATH}/scha_resourcegroup_get \
		    -O RG_STATE -G ${resource})"

		ret=$?

		if [[ -z $resource_status ]]; then
			printf \
			 "$(gettext '%s: Could not get the status for the resource group %s, errval = %d')\n" "${PROG}" "${resource}" "${ret}" | logerr
			return 1
		fi

		#
		# If any RG is unmanaged we can't change it to offline state.
		#
		if [[ $resource_status = "UNMANAGED" ]]; then
			continue;
		fi
		
		if [[ -z $resource_commalist ]]; then
			resource_commalist=$resource
		else
			resource_commalist="$resource_commalist,$resource"
		fi
	done

	#
	# Take the resource groups offline on all the nodes
	#
	if [[ ! -z $resource_commalist ]]; then

		$CLUSTER_CMD_PATH/scswitch -F -g $resource_commalist \
		    > /dev/null 2>&1

		if [[ $? -ne 0 ]]; then
			printf "$(gettext '%s: scswitch -F -g %s failed')\n" \
			    "$PROG" "$resource_commalist" | logerr 
			return 1
		fi
	fi

	#
	# Execute the "post-application-shutdown" script if one exists.
	#
	if [[ -x ${QL_SHUTDOWN_APP_CMD_PATH}/$QL_POST_SHUTDOWN_APP_CMD ]]; then
		printf "$(gettext '%s: Executing post-shutdown script')\n" \
		    "${PROG}"
		${QL_SHUTDOWN_APP_CMD_PATH}/$QL_POST_SHUTDOWN_APP_CMD > \
		    /dev/null 2>&1
		if [[ $? -ne 0 ]]; then
			printf \
			    "$(gettext '%s: %s returned a non-zero value')\n" \
			    "${PROG}" "${QL_POST_SHUTDOWN_APP_CMD}"
		fi
	fi

	printf "$(gettext '%s: Completed execution')\n\n" "$PROG" | logmsg

	return 0
}
main $*
#
# The script that invokes this greps on the return value. This is
# required because we execute this script remotely from another node
# using rsh and rsh doesnt return the exit status of the command.
#
LC_ALL='C' echo $?
