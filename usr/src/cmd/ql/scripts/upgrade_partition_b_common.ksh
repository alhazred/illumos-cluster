#! /bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#pragma ident	"@(#)upgrade_partition_b_common.ksh	1.4	08/05/20 SMI"
#
# upgrade_partition_b_common.ksh
#
# upgrade_partition_b_common -a partition_a_root_node [-R]
#
# This script does the following changes to the CCR on the current
# node that belongs to partition B:
#
#	1. Change the Quorum vote of all the nodes except root node
#	   of partition A to 0.
#
#	2. Change the Quorum vote of root node of partition A to 1.
#
#	3. Add the transport version to the CCR.
#
# When the -R option is specified, it means that the script is run
# in a live upgrade scenario. It will invoke the ql_change_ccr in live
# upgrade mode.
#
# Before the node votes are changed in the CCR a script is run to
# incorporate # any in-compatible change to the CCR to support the
# new software.
#

###############################################################
#
# Variable globals
#
###############################################################
integer -r SC_TRUE=1
integer -r SC_FALSE=0

typeset CLUSTER_LIB_PATH=/usr/cluster/lib
typeset QL_UTIL_PATH=/usr/cluster/lib/scadmin/ql
typeset QL_UTIL_FILE=ql_util

#program name and args list. Used for log messages.
if [[ -z $PROG ]]; then
	typeset -r PROG=${0##*/}
fi

###############################################################
#
# print_usage
#
#       Print usage message to stderr
#
###############################################################
print_usage()
{
	echo "$(gettext 'usage'): ${PROG} -a <node> [-R]" >&2
}


###############################################################
#
# loadlib() libname
#
#	Load the named include file.
#
#	Return:
#		zero		Success
#		one		Failure
#
###############################################################
loadlib()
{
	typeset libname=$1

	# Load the library
	. ${libname}

	if [[ $? != 0 ]]; then

		printf "$(gettext '%s:  Unable to load \"%s\"')\n" "${PROG}" \
		    "${libname}" >&2
		return 1

	fi
}

###############################################################
#
# main
#
# 	Main routine.
#	
#	Return:
#		zero	Success
#		one	Failure

###############################################################
main()
{
	# partition A.
	typeset partition_a_root_node

	# flags
	integer root_node_a_opt=${SC_FALSE}
	integer lu_mode=${SC_FALSE}

	integer sc_transport_version_tmp=0

	typeset retval

	# load common functions
	loadlib ${QL_UTIL_PATH}/${QL_UTIL_FILE} || return 1

	#
	# Global zone privilege check. If we are running in a non-global
        # we exit here.
        #
	sc_zones_check

	while getopts a:v:R c 2>/dev/null
        do
                case ${c} in
                a)      # root node of partition A specified.
                        partition_a_root_node=${OPTARG}
                        root_node_a_opt=${SC_TRUE}

			if [[ -z $partition_a_root_node ]]; then
				print_usage
				return 1
			fi
                        ;;
		v)	# transport version.
			sc_transport_version=${OPTARG}
			version_opt=${SC_TRUE}

			if [[ -z $sc_transport_version ]]; then
				print_usage
				return 1
			fi
			;;
		R)	# Live Upgrade Mode
			lu_mode=${SC_TRUE}
			;;
                \?)     print_usage
                        return 1
                        ;;
                *)      # bad option
                        print_usage
                        return 1
                        ;;
                esac
        done

	#
	# check if the root node of partition A is specified.
	#
	if [[ $root_node_a_opt -ne ${SC_TRUE} ]]; then
		print_usage
		return 1
	fi

	#
	# check for spurious arguments.
	#
	shift $(($OPTIND - 1))

	if [[ ! -z "$*" ]]; then
		print_usage
		return 1
	fi

	#
	# check to see if we are root.
	#
	verify_isroot
	
	if [[ $? -ne 0 ]]; then
		return 1
	fi

	#
	# Check to see if the current node is in non-cluster mode.
	#

	#
	# check if command exists
	#
	if [[ ! -x /usr/sbin/clinfo ]]; then
		printf "$(gettext '%s: Could not find %s')\n" "${PROG}" \
		    "clinfo" | logerr
		return 1
	fi

	#
	# check for non-cluster mode.
	#
	/usr/sbin/clinfo > /dev/null 2>&1
	retval=$?

	if [[ ${retval} -eq 0 ]] && [[ ${lu_mode} -eq ${SC_FALSE} ]]; then
		#
		# In normal mode, when a plain QL
		# upgrade is going on, the node
		# should be in non cluster mode
		#
		printf "$(gettext '%s Node must be in non-cluster mode')\n" \
		    "${PROG}" | logerr
		return 1
	fi
	
	if [[ ${retval} -ne 0 ]] && [[ ${lu_mode} -eq ${SC_TRUE} ]]; then
		#
		# In LU mode, when a QL upgrade is
		# going on along with a Live Upgrade
		# the node should be in cluster mode
		#
		printf "$(gettext '%s Node must be in cluster mode')\n" \
		    "${PROG}" | logerr
		return 1
	fi

	
	#
	# Before we change the CCR, we validate that the data given to this
	# command is consistent with the earlier command of dual-partition
	# upgrade. We check if we have the file QL_DATA_FILE_PATH/QL_DATA_FILE.
	# This should have the root nodes of partition A and the transport
	# version number.
	#

	#
	# Read the root node of partition A.
	#
	root_node_a_tmp=`read_partition_A_root`

	if [[ $? -ne 0 ]]; then
		printf "$(gettext '%s Failed to read from the data file')\n" \
		    "${PROG}" | logerr
	fi
	
	if [[ $partition_a_root_node != $root_node_a_tmp ]]; then
		printf "$(gettext '%s: Wrong root node')\n" "${PROG}" | logerr
		return 1
	fi

	#
	# Read the transport version.
	#
	sc_transport_version_tmp=`read_transport_version`

	if [[ $? -ne 0 ]]; then
		printf "$(gettext \
		    '%s Failed to read transport version from data file')\n" \
		    "${PROG}" | logerr
		return 1
	fi

	if [[ $sc_transport_version_tmp -ne $sc_transport_version ]]; then
		printf "$(gettext '%s: Wrong transport version')\n" "${PROG}" \
		    | logerr
		return 1
	fi

	#
	# Execute the "Custom CCR" script if one exists. This script
	# is needed to add changes to the CCR that happens during
	# upgrade.
	#
        if [[ -x ${QL_CCR_CUSTOM_SCRIPT_PATH}/$QL_CCR_CUSTOM_SCRIPT ]]; then
                printf "$(gettext '%s: Executing %s')\n" "${PROG}" \
		    ${QL_CCR_CUSTOM_SCRIPT}
                ${QL_CCR_CUSTOM_SCRIPT_PATH}/$QL_CCR_CUSTOM_SCRIPT > /dev/null \
                    2>&1
                if [[ $? -ne 0 ]]; then
                        printf \
                            "$(gettext '%s: %s returned non-zero value')\n" \
                            "${PROG}" "${QL_CCR_CUSTOM_SCRIPT}"
                fi
        fi

	printf "$(gettext '%s: Completed execution of %s')\n" "${PROG}" \
	    ${QL_CCR_CUSTOM_SCRIPT_PATH}/$QL_CCR_CUSTOM_SCRIPT | logdbgmsg

	#
	# Change the quorum vote of the root node of partition A to one.
	# Change the quorum vote of other nodes to zero.
	#
	if [[ ${lu_mode} -eq ${SC_TRUE} ]]; then
		#
		# Execute ql_change_ccr in live upgrade mode.
		#
		${QL_LIB_PATH}/ql_change_ccr -x -R -w $partition_a_root_node \
		    > /dev/null 2>&1
	else
		${QL_LIB_PATH}/ql_change_ccr -x -w $partition_a_root_node \
		    > /dev/null 2>&1
	fi

        if [[ $? -ne 0 ]]; then
                printf "$(gettext '%s: Could not change quorum vote')\n" \
		    "${PROG}" | logerr
                return 1
        fi

	#
	# Add the  transport version field to the CCR.
	#
	if [[ ${lu_mode} -eq ${SC_TRUE} ]]; then
		#
		# Execute ql_change_ccr in live upgrade mode.
		#
		${QL_LIB_PATH}/ql_change_ccr -x -R -v $sc_transport_version > \
		    /dev/null 2>&1
	else
		${QL_LIB_PATH}/ql_change_ccr -x -v $sc_transport_version > \
		    /dev/null 2>&1
    	fi	

        if [[ $? -ne 0 ]]; then
                printf "$(gettext '%s: Could not add transport version')\n" \
		    "${PROG}" | logerr
                return 1
        fi

	printf "$(gettext '%s: Completed changing quorum vote')\n" "${PROG}"\
	    | logdbgmsg
 
	#
	# we are done!.
	#
	return 0
}
main $*
return $?
