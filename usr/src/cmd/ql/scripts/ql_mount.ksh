#! /bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#pragma ident	"@(#)ql_mount.ksh	1.3	08/05/20 SMI"
#
# ql_mount.ksh
#
# ql_mount
#
# This script is executed in cluster mode. It is executed on the nodes
# of partition A. It restores the the commented entries in /etc/vfstab
# that was done to prevent QFS filesystems to be mounted.
# These filesystems are now mounted.
#

###############################################################
#
# Variable globals
#
###############################################################

typeset QL_UTIL_PATH=/usr/cluster/lib/scadmin/ql
typeset QL_UTIL_FILE=ql_util

# Program name and args list. Used for log messages.
if [[ -z $PROG ]]; then
	typeset -r PROG=${0##*/}
fi

#####################################################
#
# loadlib() libname
#
#	Load the named include file.
#
#	Return:
#		zero		Success
#		non-zero	Failure
#
#####################################################
loadlib()
{
	typeset libname=$1

	# Load the library
	. ${libname}

	if [[ $? != 0 ]]; then
		printf "$(gettext '%s:  Unable to load \"%s\"')\n" "${PROG}" \
		    "${libname}" >&2
		return 1
	fi
}

################################################################
#
# ql_mnt_vfstab
#
# This function changes the /etc/vfstab file. It un-comments
# entries of any QFS file systems, that mount at boot time. It then 
# mounts these filesystems.
#
#	Return:
#		zero		Success
#		non-zero	Failure
################################################################
ql_mnt_vfstab()
{
	typeset line
	typeset special
	typeset fsckdev
	typeset mountp
	typeset type
	typeset pass
	typeset boot
	typeset options
	typeset spl

	/usr/bin/rm -f /tmp/vfstab.$$
	touch /tmp/vfstab.$$ 

	while read special fsckdev mountp typ pass boot options
	do
		# check commented lines

		case ${special} in

		"##"* ) #Already commented out lines. Ignore them.

			printf \
			    "$(gettext '%s\t%s\t%s\t%s\t%s\t%s\t%s')\n" \
			    "$special" "$fsckdev" "$mountp" \
			    "$typ" "$pass" "$boot" "$options" >> /tmp/vfstab.$$
			continue
			;;

		'#'* ) #check commented lines

			if [[ $boot = "yes" ]]; then

				if [[ $type = "samfs" || $type = "qfs" ]]; then

					#
					# Found a shared QFS filesystem entry.
					# un-comment it.
					#

					spl=`echo $special | /usr/bin/cut -c2-`

					printf \
					"$(gettext '%s\t%s\t%s\t%s\t%s\t%s\t%s')\n" \
					    "$spl" "$fsckdev" "$mountp" \
					    "$typ" "$pass" "$boot" "$options" >> /tmp/vfstab.$$

					/usr/sbin/mount $mountp > /dev/null 2>&1

					if [[ $? -ne 0 ]]; then
						 printf "$(gettext '%s: Could not mount %s')\n" "$PROG" "$mountp" | logerr
					fi
					
					printf "$(gettext '%s: mounted %s')\n" \
					    "$PROG" "$mountp" | logdbgmsg

				else 

					printf \
					"$(gettext '%s\t%s\t%s\t%s\t%s\t%s\t%s')\n" \
					    "$special" "$fsckdev" "$mountp" \
					    "$typ" "$pass" "$boot" "$options" >> /tmp/vfstab.$$
				fi
			else

					printf \
					"$(gettext '%s\t%s\t%s\t%s\t%s\t%s\t%s')\n" \
					    "$special" "$fsckdev" "$mountp" \
					    "$typ" "$pass" "$boot" "$options" >> /tmp/vfstab.$$

					
			fi

			continue
			;;
		esac

		printf "$(gettext '%s\t%s\t%s\t%s\t%s\t%s\t%s')\n" \
		    "$special" "$fsckdev" "$mountp" "$typ" "$pass" "$boot" \
		    "$options" >> /tmp/vfstab.$$
		
	done < /etc/vfstab

	if [[ $? -ne 0 ]]; then

		printf "$(gettext '%s: Error changing vfstab')\n" "${PROG}" | logerr
		return 1
	fi

	cp /tmp/vfstab.$$ /etc/vfstab || return 1 

	return 0
}

####################################################
#
# main
#
####################################################
main()
{
	typeset hostname

	#
	# Load common functions
	#
	loadlib $QL_UTIL_PATH/$QL_UTIL_FILE > /dev/null 2>&1

	if [[ $? -ne 0 ]]; then
		return 1
	fi 	
	
	#
	# Global zone privilege check. If we are running in a non-global
        # we exit here.
	#
	sc_zones_check

	#
	# check to see if we are root.
	#
	verify_isroot

	if [[ $? -ne 0 ]]; then
		return 1
	fi

	#
	# Check if command exists.
	#
	if [[ ! -x /usr/sbin/clinfo ]]; then
		printf "$(gettext '%s: Could not find %s')\n" "${PROG}" \
		    "clinfo" | logerr
		return 1
	fi

	#
	# Check for cluster mode.
	#
	/usr/sbin/clinfo > /dev/null 2>&1

	if [[ $? -ne 0 ]]; then
		printf "$(gettext '%s: Node must be cluster mode')\n" \
		    "${PROG}"
		return 1
	fi

	printf "$(gettext '%s: Starting execution')\n\n" "$PROG" | logdbgmsg

	ql_mnt_vfstab

	if [[ $? -ne 0 ]]; then
		printf "$(gettext '%s: ql_change_vfstab returned an error')\n" \
		    "$PROG" | logerr
		return 1
	fi

	printf "$(gettext '%s: Completed execution')\n\n" "$PROG" | logdbgmsg

	return 0
}
main
