#! /bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#pragma ident	"@(#)split_mode_upgrade_begin.ksh	1.9	08/05/20 SMI"
#
# split_mode_upgrade_begin.ksh
#
# split_mode_upgrade_begin -n partition_a_nodes [-i] [-R <mount_point>]
#
# This script does the following if executed on a node in partition B:
#
#	1. Take a backup copy of CCR and /etc/vfstab on each node of
#	   the cluster.
#
#	2. Change the Quorum vote of all the nodes in partititon A to 0.
#
#	3. Shutdown nodes of partition A.
#
# -R mount_point:
#	The script is executed in a live upgrade scenario.
# 	The mount point location where the alternate boot environment 
#	is located. In this mode, the additional tasks done are
#		- validation of the mount point
#		- execution of ql_lu_begin on nodes of partition A.
#		- Only partition A nodes are rebooted using init 6
#		
# The debug logs generated are placed in QL_UPGRADE_LOG.
#

###################################################
#
# Variable globals
#
####################################################
integer -r SC_TRUE=1
integer -r SC_FALSE=0

typeset -r CMD_PWD=/usr/bin/pwd
typeset HOSTNAME_CMD=/usr/bin/hostname
typeset SHUTDOWN_CMD="/usr/sbin/shutdown -y -g 0 -i 0"

typeset QL_UTIL_FILE=ql_util
typeset QL_UTIL_PATH=/usr/cluster/lib/scadmin/ql
typeset QL_LIBSC_PATH=/usr/cluster/lib/sc
typeset QL_TMP_DIR=/var/cluster/run

#program name and args list.
if [[ -z $PROG ]]; then
	typeset -r PROG=${0##*/}
fi

typeset NODELIST
typeset hostname
typeset least_nodeid_node

#
# The QL_UPGRADE_DIR is the directory from which this script is run.
# Make sure that it is set with an absolute path and remove
# trailing dots.
#
typeset -r QL_CWD=$(${CMD_PWD})

typeset QL_UPGRADE_DIR=$(dirname $0)
if [[ "${QL_UPGRADE_DIR}" != /* ]]; then
	QL_UPGRADE_DIR=${QL_CWD}/${QL_UPGRADE_DIR}
fi
while [[ "${QL_UPGRADE_DIR##*/}" = "." ]]
do
	QL_UPGRADE_DIR=$(dirname ${QL_UPGRADE_DIR})
done

# Partition B.
typeset partition_b_root_node
typeset partition_b_nodes
typeset partition_b_commalist

# Partition A.
typeset partition_a_root_node
typeset partition_a_nodes
typeset	partition_a_commalist

# Paths.
typeset qlupgradedir=${QL_UPGRADE_DIR}
typeset qllibpath

# Flags.
integer found=${SC_FALSE}
integer from_cdrom=${SC_FALSE}
integer interactive_mode=${SC_FALSE}
integer live_upgrade_mode=${SC_FALSE}

# Alternate boot environment mount point
typeset mount_point

########################################################
#
# print_usage
#
#	Print usage message to stderr
#
########################################################
print_usage()
{
	echo "$(gettext 'Usage'): ${PROG} -n <node>[,<node>,...] " \
	    "[-i] [-R <mount_point>]" >&2
}

#########################################################
#
# loadlib() libname
#
#	Load the named include file.
#
#	Return:
#		zero		Success
#		non-zero	Failure
#
#########################################################
loadlib()
{
	typeset libname=$1

	# Load the library
	. ${libname}

	if [[ $? != 0 ]]; then
		printf "$(gettext '%s: Unable to load \"%s\"')\n" \
		    "${PROG}" "${libname}" >&2
		return 1
	fi
}

##########################################################
#
# change_quorum() partition_b_root partition_b_nodelist
#
# 	We need to set the node votes for all the nodes in partition
#	A to zero. This is not a problem for two node clusters.
#	For clusters with more than two nodes scconf can crib and
#	fail with "quorum will be compromised" error. To avoid this
#	we do this:
#
# 	  1. Put all the quorum devices into maintainance mode i.e.
#	     take them offline.
#
# 	  2. Set the quorum vote for the root node of partition B
#	     to the number of nodes in the cluster.
#
#	  3. We change the votes for all the nodes, except for the
#	     root node of partition B, to zero.
#
#	  4. After this the vote count for the root node of
#	     partition B is set to 1.
#
#	  5. We change the votes for the other nodes in partition B
#	     to 1.
#
#	  6. Bring all the quorum devices that are in maintainance
#	     mode to online.
#
# 	This function will potentially lead to many CMM reconfigurations.
#	as we play around with quorum votes.
#
#	Return:
#		0 Success
#		1 Failure
#
####################################################################
change_quorum()
{
	typeset partition_b_root
	typeset partition_b_nodelist
	typeset node
	typeset tmpfile
	typeset tmperr
	typeset device
	
	integer node_count
	integer num_votes
	integer pid
	integer ret

	partition_b_root=$1

	shift 1

	partition_b_nodelist="$*"

	pid=$$
	tmpfile="ql.$pid$pid"
	(( pid = pid + 1 ))
	tmperr="ql.$pid$pid"
	
	rm -f /tmp/$tmpfile
	rm -f /tmp/$tmperr
	
	#
	# Count the number of nodes in the cluster.
	#
	for node in $NODELIST
	do
		(( node_count = node_count + 1 ))
	done

	#
	# check if the command file exists.
	#
	if [[ ! -x ${CLUSTER_CMD_PATH}/scconf ]]; then
		printf "$(gettext '%s: Could not find %s')\n" "${PROG}" \
		    "scconf" | logerr
		return 1
	fi

	if [[ $node_count -gt 2 ]]; then

		#
		# check if command exists.
		#
		if [[ ! -x /usr/bin/awk ]]; then
			printf "$(gettext '%s: /usr/bin/awk not found')\n" \
			    "${PROG}" | logerr 
			return 1
		fi

		#
		# Parse and redirect the list of Qourum devices to the 
		# temorary file.
		#

		# save and reset the locale
		LC_ALL_BKUP=$LC_ALL
		LC_ALL="C"

		${CLUSTER_CMD_PATH}/scstat -q  | \
		    /usr/bin/awk '/Device votes:/ {print $3}' \
		    >/tmp/$tmpfile 2>/tmp/$tmperr

		ret=$?

		#restore the locale
		LC_ALL=$LC_ALL_BKUP

		#
		# If the command fails, handle failure.
		#
		if [[ $ret -ne 0 ]]; then
			printf "$(gettext '%s: Failed to get quorum device list. scstat returned an error, errval = %d')\n" "${PROG}" "${ret}" | logerr
			return 1
		fi
		
		#
		# Iterate through each Quorum device configured and currently
		# online and set it state to maintainance mode.
		# 
		for device in `cat /tmp/$tmpfile`
		do
			printf \
			    "$(gettext '%s: Change %s to Offline state')\n" \
			    "${PROG}" "${device}" | logdbgmsg

			${CLUSTER_CMD_PATH}/scconf -c -q \
			    globaldev=$device,maintstate 
			
			ret=$?
			if [[ $ret -ne 0 ]]; then
				printf "$(gettext '%s: scconf returned error, errval = %d')\n" "${PROG}" "${ret}" | logerr
				return 1
			fi
		done
	
		#
		# Set the vote count of the root node to be equal to
		# number of nodes in the cluster.
		# 
		printf \
		    "$(gettext '%s: Change vote count of %s to %s')\n" \
		    "${PROG}" "${partition_b_root}" "${node_count}" | logdbgmsg

		${CLUSTER_CMD_PATH}/scconf -c -q \
		    node=$partition_b_root,defaultvote=$node_count > \
		    /dev/null 2>&1

		ret=$?
		#
		# Handle failure of the command.
		#
		if [[ $ret -ne 0 ]]; then
			printf "$(gettext '%s: scconf returned an error, errval = %d')\n" "${PROG}" "${ret}" | logerr
			return 1
		fi
	fi

	#
	# There is no need for Quorum vote manipulation for a cluster with two
	# nodes. We can directly proceed to reset all the votes except
	# the root.
	#  

	for node in $NODELIST
	do
		#
		# If this is the root node of partition B, skip this node.
		#
		if [[ $node = $partition_b_root ]]; then
			continue
		fi		  

		#
		# The node vote count is reset to zero.
		#
		${CLUSTER_CMD_PATH}/scconf -c -q node=$node,defaultvote=0 \
		    > /dev/null 2>&1

		ret=$?
		#
		# Handle command failure.
		#
		if [[ $ret -ne 0 ]]; then
			printf \
			   "$(gettext '%s: Failed to set the node vote to 0 for node %s. scconf returned an error, errval = %d')\n" "${PROG}" "${node}" "${ret}" | logerr
			return 1
		fi
	done

	#
	# Reset the vote count of the root node to 1. We cant directly
	# set the votecount to 1 as scconf can crib again. This will be
	# a step wise decrement of the votes.
	#
	num_votes=$node_count 
	(( num_votes = num_votes - 1 ))

	while  [[ $num_votes -ne 0 ]];
	do
		${CLUSTER_CMD_PATH}/scconf -c -q \
		    node=$partition_b_root,defaultvote=$num_votes \
		    > /dev/null 2>&1

		ret=$?
		#
		# Handle command failure. This is not a fatal failure
		# and we can ignore this here.
		#
		if [[ $ret -ne 0 ]]; then
			printf \
		    	  "$(gettext '%s: Failed to set node vote to %d for root node %s. scconf returned an error, errval = %d')\n" "${PROG}" "${num_votes}" "${partition_b_root}" "${ret}" | logdbgmsg
			break
		fi
		(( num_votes = num_votes - 1 ))
	done

	#
	# Reset the vote of each node in partition B to 1.
	#
	for node in $partition_b_nodelist
	do
		if [[ $node = $partition_b_root ]]; then
			continue
		fi
		#
		# reset the vote count of the node to 1.
		#
		${CLUSTER_CMD_PATH}/scconf -c -q \
		    node=$node,defaultvote=1 > /dev/null 2>&1

		ret=$?
		#
		# Handle command failure.
		#
		if [[ $ret -ne 0 ]]; then
			printf \
		    	"$(gettext '%s: Failed to reset node vote for node %s. scconf returned an error, errval = %d')\n" "${PROG}" "${node}" "${ret}" | logerr
			return 1
		fi
	done
		
	#
	# Reset the vote count of the root node of partition B to 1 in case
	# it is not 1. This may fail, in one particular scenario.
	# Three node cluster, with a QD between node1 and node3
	# and another QD between node2 and node3. Make node3 the second
	# partition. Hence we will not return error, if the command
	# fails. We will log an error.
	#
	${CLUSTER_CMD_PATH}/scconf -c -q node=$partition_b_root,defaultvote=1 \
	    > /dev/null 2>&1

	ret=$?
	#
	# Handle command failure. This is not a fatal error and we just
	# ignore it here.
	#
	if [[ $ret -ne 0 ]]; then
		printf \
	    	"$(gettext '%s: Failed to reset node vote for the root node %s. scconf returned an error, errval = %d')\n" "${PROG}" "${partition_b_root}" "${ret}" | logdbgerr
	fi

	#
	# If two node cluster, we are done.
	#
	if [[ $node_count -eq 2 ]]; then
		return 0
	fi

	# 
	#
	# We try and bring all the quorum devices online again.
	# 

	#
	# Iterate through each quorum device configured and currently
	# 'Offline' and change its state to 'Online'.
	# 
	for device in `cat /tmp/$tmpfile`
	do
		printf \
		    "$(gettext '%s: Change %s to Online state')\n" \
		    "${PROG}" "${device}" | logdbgmsg

		${CLUSTER_CMD_PATH}/scconf -c -q globaldev=$device,reset 
			
		ret=$?
		#
		# Handle command failure.
		#
		if [[ $ret -ne 0 ]]; then
			printf "$(gettext '%s:scconf returned an error, errval = %d')\n" "${PROG}" "${ret}" | logerr
		fi
	done
	#
	# We are done. Cleanup and return.
	#
	rm -f /tmp/$tmpfile
	return 0
}

##################################################################
#
# backup_files qllibpath
#
#	Backup /etc/vfstab and CCR on each node of the cluster.
#
#	Argument: 
#		$qllibpath gives the path where to find $QL_BEGIN_CMN_CMD
#
#	$QL_BEGIN_CMN_CMD is run on each node of the cluster. Apart from
#	taking a backup of the configuration files, the command also
#	creates a Data file on each node with information about the
#	partitions.
#
#	Return:
#		zero		Success
#		non-zero	Failure
#
##################################################################
backup_files()
{
	typeset qllibpath=$1
	typeset node

	for node in $NODELIST
	do

		if [[ $hostname = $node ]]; then

			${qllibpath}/${QL_BEGIN_CMN_CMD} -a \
			    $partition_a_root_node -b \
			    $partition_b_root_node -l \
			    $partition_b_commalist -n \
			    $partition_a_commalist -c \
			    $least_nodeid_node > /dev/null 2>&1

			if [[ $? -ne 0 ]]; then

				#restore the locale
				LC_ALL=$LC_ALL_BKUP

				printf "$(gettext '%s: %s returned error')\n" \
				    "${PROG}" "${QL_BEGIN_CMN_CMD}" | logerr 

				return 1
			fi

		else
			execute_cmd $node ${qllibpath}/${QL_BEGIN_CMN_CMD} -a \
			    $partition_a_root_node -b \
			    $partition_b_root_node -l \
			    $partition_b_commalist -n \
			    $partition_a_commalist -c \
			    $least_nodeid_node > /dev/null 2>&1

			if [[ $? -ne 0 ]]; then

			   printf \
			     "$(gettext '%s: %s returned error on node %s')\n"\
			     "${PROG}" "${QL_BEGIN_CMN_CMD}" "${node}" | logerr 

			   return 1
			fi
		fi
	done
}

##################################################################
#
# execute_pre_shutdown $partition_nodes
#
#	Remote execute a shutdown script to shutdown any
#	application running on a node of the partiton.
#
#	Argument: 
#		List of nodes in the partition.
#
#	Return:
#		zero		Success
#		non-zero	Failure
###################################################################
execute_pre_shutdown()
{
	typeset node
	typeset partition_nodes 
	integer in_partition=${SC_FALSE}

	partition_nodes="$*"

	for node in $partition_nodes
	do
		if [[ $hostname = $node ]]; then
			in_partition=${SC_TRUE}
			continue;
		fi
		#
		# If we have a pre-shutdown command script available we
		# copy that over to $QL_TMP_DIR and execute it on the
		# remote node.
		#
		if [[ -x ${PRE_SHUTDOWN_CMD_PATH}/${PRE_SHUTDOWN_CMD} ]]; then
			if [[ "$SC_QL_REMOTE_METHOD" == "ssh" ]]; then
				/usr/bin/scp -Bq \
				  ${PRE_SHUTDOWN_CMD_PATH}/${PRE_SHUTDOWN_CMD} \
				  root@${node}:${QL_TMP_DIR} > /dev/null 2>&1
			else
				/usr/bin/rcp \
				  ${PRE_SHUTDOWN_CMD_PATH}/${PRE_SHUTDOWN_CMD} \
				  ${node}:${QL_TMP_DIR} > /dev/null 2>&1
			fi

			if [[ $? -ne 0 ]]; then
				printf \
				 "$(gettext '%s: Failed to copy %s to %s')\n" \
				 "${PROG}" \
				 ${PRE_SHUTDOWN_CMD_PATH}/${PRE_SHUTDOWN_CMD} \
				 "${node}" | logerr
				return 1
			fi

			execute_cmd $node ${QL_TMP_DIR}/${PRE_SHUTDOWN_CMD} > \
			    /dev/null 2>&1

			if [[ $? -ne 0 ]]; then
				printf \
				"$(gettext '%s: %s returned error on %s')\n" \
				"${PROG}" \
				"${PRE_SHUTDOWN_CMD_PATH}/${PRE_SHUTDOWN_CMD}" \
				"${node}" | logerr
				return 1
			fi
		fi
	done

	#
	# If we have a pre-shutdown command script available we
	# execute it here.
	#
	if [[ $in_partition -eq ${SC_TRUE} ]]; then

		if [[ -x ${PRE_SHUTDOWN_CMD_PATH}/${PRE_SHUTDOWN_CMD} ]]; then

			${PRE_SHUTDOWN_CMD_PATH}/${PRE_SHUTDOWN_CMD}

			if [[ $? -ne 0 ]]; then
				printf \
				 "$(gettext '%s: %s returned error on %s')\n" \
				 "${PROG}" \
				 "${PRE_SHUTDOWN_CMD_PATH}/${PRE_SHUTDOWN_CMD}"\
				 "${hostname}" | logerr
				return 1
			fi
		fi
	fi
	return 0
}

##################################################################
#
# shutdown_partition $partition_nodes
#
#	Shutdown the list of nodes in the partiton.
#
#	Argument: 
#		List of nodes in the partition.
#
#	Return:
#		zero		Success
#		non-zero	Failure
###################################################################
shutdown_partition()
{
	typeset node
	typeset partition_nodes
	integer in_partition=${SC_FALSE}

	partition_nodes="$*"

	for node in $partition_nodes
	do
		if [[ $hostname = $node ]]; then
			in_partition=${SC_TRUE}
			continue;
		fi
		printf "$(gettext '%s: Attempting shutdown of node %s')\n" \
		    "${PROG}" "${node}" | logdbgmsg > /dev/null 2>&1


		execute_cmd $node $SHUTDOWN_CMD > /dev/null 2>&1

		#
		# If we fail to shutdown the node, this should be done
		# manually. Log an error and try to shutdown other nodes.
		# 
		if [[ $? -ne 0 ]]; then
			printf \
			    "$(gettext '%s: Shutdown failed for node %s')\n" \
			    "${PROG}" "${node}" | logerr
		fi
	done

	#
	# We are done!.
	#
	printf "$(gettext '%s: Completed execution')\n" "${PROG}" | logdbgmsg \
	    > /dev/null 2>&1
	
	#
	# We go down here since we are executing on a partititon A node.
	#
	if [[ $in_partition -eq ${SC_TRUE} ]]; then
		printf "$(gettext '%s: Attempting shutdown of node %s')\n" \
		    "${PROG}" "${hostname}" | logdbgmsg > /dev/null 2>&1
		
		$SHUTDOWN_CMD > /dev/null 2>&1

	fi

	#
	# We dont return here if we are executing this script on a node
	# in partition A.
	#
	return 0
}

##################################################################
#
# get_partition_b
#
#	Get the list of partition B nodes given the list of nodes 
#	in partition A.
#
#	Return:
#		zero		Success
#		non-zero	Failure
###################################################################
get_partition_b_list()
{
	typeset node
	typeset nodeA

	partition_b_nodes=
	partition_b_commalist=

	for node in $NODELIST
	do
		found=${SC_FALSE}

		for nodeA in $partition_a_nodes
		do
			if [[ $nodeA = $node ]]; then
				found=${SC_TRUE}
				break;
			fi
		done

		if [[ $found = ${SC_FALSE} ]]; then
			#
			# We found a node in partition B.
			#
			if [[ -z $partition_b_nodes ]]; then
			   partition_b_nodes=$node
			   #
			   # The first node found in the list of partition
			   # B nodes will be the root node of partition B.
			   #
			   partition_b_root_node=$node
			   partition_b_commalist=$node
			else
			   partition_b_nodes="$partition_b_nodes $node"
			   partition_b_commalist="$partition_b_commalist,$node"
			fi
		fi
	done
}
		
##################################################################
#
# copy_files
#
# Copy the executables on the remote nodes of the cluster. 
#
#	Return:
#		zero		Success
#		non-zero	Failure
###################################################################
copy_files()
{
	typeset node
	
        if [[ $from_cdrom -eq ${SC_TRUE} ]]; then
                for node in $NODELIST
                do
                        if [[ $node = $hostname ]]; then
                                /usr/bin/cp -p \
				    ${qlupgradedir}/${QL_BEGIN_CMN_CMD} \
				    ${qllibpath} || return 1
                                /usr/bin/cp -p ${qlupgradedir}/${QL_UTIL_FILE} \
				    ${qllibpath} || return 1
				if [[ ${live_upgrade_mode} == ${SC_TRUE} ]]; then
					/usr/bin/cp -p \
					    ${qlupgradedir}/${QL_LU_BEGIN_CMD} \
					    ${qllibpath} || return 1
				fi
                        else
				if [[ "$SC_QL_REMOTE_METHOD" == "ssh" ]]; then
					/usr/bin/scp -Bq \
					   ${qlupgradedir}/${QL_BEGIN_CMN_CMD} \
					   root@${node}:${qllibpath} || return 1
					/usr/bin/scp -Bq \
					   ${qlupgradedir}/${QL_UTIL_FILE} \
					   $node:${qllibpath} || return 1
					if [[ ${live_upgrade_mode} == ${SC_TRUE} ]]; then
						/usr/bin/scp -Bq \
						    ${qlupgradedir}/${QL_LU_BEGIN_CMD} \
						    $node:${qllibpath} || return 1
					fi
				else
                                	/usr/bin/rcp -p \
					    ${qlupgradedir}/${QL_BEGIN_CMN_CMD}\
					    $node:${qllibpath} || return 1
                                	/usr/bin/rcp -p \
					    ${qlupgradedir}/${QL_UTIL_FILE} \
					    $node:${qllibpath} || return 1
					if [[ ${live_upgrade_mode} == ${SC_TRUE} ]]; then
						/usr/bin/rcp -p \
						    ${qlupgradedir}/${QL_LU_BEGIN_CMD} \
						    $node:${qllibpath} || return 1
					fi
				fi
			fi
		done
	fi
	return 0
}

##################################################################
#
# verify_node_list
#
# Verify if the node list is part of the cluster configuration. 
#
#	Return:
#		zero		Success
#		non-zero	Failure
###################################################################
verify_node_list()
{
	typeset node
	typeset cluster_mode

	for node in $partition_a_nodes
	do
		found=${SC_FALSE}

		#
		# set the root node of partition A to the first node
		# in the partition A node list.
		#
		if [[ -z $partition_a_root_node ]]; then
			partition_a_root_node=$node
		fi

		for cluster_node in $NODELIST
		do
			if [[ $cluster_node = $node ]]; then
				found=${SC_TRUE}
				break;
			fi
		done

		if [[ $found = ${SC_FALSE} ]]; then
			printf "$(gettext '%s: Node %s not in cluster')\n" \
			    "${PROG}" "${node}"
			return 1
		fi
	done
}

#########################################################
#
# verify_rg_state $nodelist
#
# This will verify that no resource group on any node 
# is in ERROR_STOP_FAILED state. If a resource group
# is in ERROR_STOP_FAILED state, then returns 1 (failure)
# It also checks if RG_SYSTEM property is set for any resource
# group. If so, we return 1 (failure) 
#
#	Return:
#		zero		Success
#		non-zero	Failure
#########################################################
verify_rg_state()
{
	typeset rg
	typeset rg_list
	typeset rg_state
	typeset nodelist
	typeset node
	typeset sysproperty

	#
	# List of node names in the cluster
	#
	nodelist="$*"
	
	#
	# Get the list of resource groups configured in the cluster.
	#
	rg_list="$(${CLUSTER_CMD_PATH}/scha_cluster_get -O \
	    ALL_RESOURCEGROUPS)"

	if [[ -z $rg_list ]]; then
		printf \
		    "$(gettext '%s: No resource groups configured')\n" \
		    "${PROG}" | logdbgmsg

		# Success
		return 0
	fi

	for rg in $rg_list
	do
		#
		# Check if the RG is a system RG
		#
		sysproperty="$(${CLUSTER_CMD_PATH}/scha_resourcegroup_get \
		    -G $rg -O RG_SYSTEM)"
		ret=$?
		#
		# If RG_SYSTEM property is not valid on a SC version
		# it will return an error SCHA_ERR_TAG (4). Then
		# we can safely ignore this check here.
		#
		if [[ -z "${sysproperty}" ]] && [[ $ret -ne 4 ]]; then
			printf "$(gettext \
			    '%s: Could not get the RG_SYSTEM property for the resource group %s, errval = %d')\n" "${PROG}" "${rg}" "${ret}" | logerr
			#
			# Failure
			#
			return 1
		fi
		if [[ "$sysproperty" == "TRUE" ]]; then
			printf "$(gettext \
			    '%s: Resource group %s has RG_SYSTEM property set to TRUE. This property needs to be set to FALSE before the upgrade and can be set to TRUE after the upgrade completes.')\n" "${PROG}" "${rg}" | logerr
			#
			# Failure
			#
			return 1
		fi
		for node in $nodelist
		do
			rg_state="$(${CLUSTER_CMD_PATH}/scha_resourcegroup_get \
			    -O RG_STATE_node -G ${rg} ${node})"

			ret=$?
			if [[ -z $rg_state ]]; then
				printf "$(gettext \
				    '%s: Could not get the state for the resource group %s on node %s, errval = %d')\n" "${PROG}" "${rg}" "${node}" "${ret}" | logerr
				#
				# Failure
				#
				return 1
			fi

			#
			# Check if the resource group is 
			# in ERROR_STOP_FAILED state
			#
			if [[ "${rg_state}" == "ERROR_STOP_FAILED" ]]; then
				printf "$(gettext \
				     '%s: Resource group %s is in ERROR_STOP_FAILED state on node %s')\n" \
				     "${PROG}" "${rg}" "${node}" | logerr
				#
				# Failure
				#
				return 1
			fi
		done
	done

	#
	# Success
	#
	return 0
}

#########################################################
#
# validate_mount_point()
#
# This function will validate that the mount point is the
# same on all nodes of the cluster. It will also validate
# that the alternate boot environment is mounted.
#
# Note: This function expects $NODELIST and $mount_point
# to be set before being called.
#
#	Return:
#		zero		Success
#		non-zero	Failure
#
#########################################################
validate_mount_point()
{
	typeset node
	typeset result=0

	if [[ -z "${NODELIST}" ]]; then
		printf "$(gettext '%s: NODELIST is empty')\n" \
		    "${PROG}" | logdbgerr
		return 1
	fi

	if [[ -z "${mount_point}" ]]; then
		printf "$(gettext '%s: mount_point is empty')\n" \
		    "${PROG}" | logdbgerr
		return 1
	fi

	#
	# For every node, validate the mount point
	#
	for node in $NODELIST
	do
		#
		# Execute the QL_LU_BEGIN_CMD in check mode
		# for validating the mount points.
		# We will validate mount point for all the nodes
		# so that if there is an error, user can go and
		# and rectify the errors at one go.
		#
		execute_cmd $node ${qllibpath}/${QL_LU_BEGIN_CMD} \
		    -R ${mount_point} -c >  /dev/null 2>&1

		if [[ $? != 0 ]]; then
			printf "$(gettext \
			    '%s: Invalid mount point %s on node %s')\n" \
			    "${PROG}"  "${mount_point}" "${node}"| logerr
			result=1
		else
			printf "$(gettext \
			    '%s: Node %s has valid mount point %s')\n" \
			    "${PROG}" "${node}" "${mount_point}" | logdbgmsg
		fi		    

	done
	
	#
	# Check if any error has occurred
	#
	if [[ ${result} == 1 ]]; then
		#
		# Some nodes have an invalid mount point.
		#
		printf "$(gettext \
		    '%s: Some nodes have invalid mount point %s')\n" \
		    "${PROG}"  "${mount_point}" | logerr

		printf "$(gettext \
		    '%s: Refer to logs on those respective nodes')\n" \
		    "${PROG}" | logerr

		#
		# Failure
		#
		return 1
	fi

	printf "$(gettext '%s: All nodes have valid mount point %s')\n" \
	    "${PROG}" "${mount_point}" | logdbgmsg

	#
	# Success
	#
	return 0
}

#########################################################
#
# execute_lu_begin_task() $partition_nodes
#
#	Will execute the QL related Live Upgrade tasks
#	on the nodes of the partition.
#
#	Argument:
#		List of nodes in the partition.
#
#	Return:
#		zero		Success
#		non-zero	Failure
#
#########################################################
execute_lu_begin_task()
{
	typeset node
	typeset partition_nodes
	typeset local_node=`/usr/bin/hostname`
	integer in_partition=${SC_FALSE}

	partition_nodes="$*"

	for node in $partition_nodes
	do
		if [[ ${local_node} == ${node} ]]; then
			in_partition=${SC_TRUE}
			continue;
		fi
 
		#
		# Execute the ql_lu_begin command on nodes of partition A.
		# This command will do the following
		# 1. Map the mount point to the boot environment
		# 2. Store the name of the mount point and boot
		#    environment in $QL_LU_CONTINUE
		# 3. Copy /etc/cluster/ccr, ${QL_CCR_BACKUP} and
		#    ${QL_DATA_FILE_PATH}/${QL_DATA_FILE}
		#    from old boot environment to the new boot environment.
		# 4. Take backup of the vfstab file on the new
		#    boot environment. i.e. $MOUNT_POINT/etc/vfstab
		# 5. Change the vfstab file on the new boot environment
		#    to comment out all QFS file systems which get
		#    mounted at boot time.
		# 6. Create the ${MOUNTPOINT}${UPGRADE_FLAG_FILE} on new
		#    boot environment.
		# 7. Unmount the alternate boot environment
		# 8. Activate the boot environment
		# 9. Reboot the node in the boot environment.
		#    ( does a init 6)
		#
		execute_cmd $node ${qllibpath}/${QL_LU_BEGIN_CMD} \
		    -A -R ${mount_point} >  /dev/null 2>&1 
		if [[ $? -ne 0 ]]; then
			printf \
			    "$(gettext '%s: %s failed on node %s')\n" \
			    "${PROG}" "${QL_LU_BEGIN_CMD}" "${node}" | logerr
			printf "$(gettext \
			    '%s: Refer to logs on the node %s')\n" \
			    "${PROG}" "${node}" | logdbgerr
			return 1
		else
			printf "$(gettext \
			    '%s: Successfully completed dual-partition live upgrade tasks on node %s')\n" \
			    "${PROG}" "${node}" | logdbgmsg
		fi
	done

	
	if [[ $in_partition -eq ${SC_TRUE} ]]; then
		#
		# Execute the ql_lu_begin command on the
		# local node.
		#
		printf \
		    "$(gettext \
		    '%s: Executing dual-partition live upgrade begin task on node %s')\n" \
		    "${PROG}" "${local_node}" | logdbgmsg
		
		${qllibpath}/${QL_LU_BEGIN_CMD} -A -R ${mount_point}   \
		    >  /dev/null 2>&1
		
		if [[ $? != 0 ]]; then
			printf \
			"$(gettext '%s: %s failed on node %s')\n"  \
			    "${PROG}" "${QL_LU_BEGIN_CMD}" \
			    "${local_node}" | logerr
			printf "$(gettext \
			    '%s: Refer to logs on the node %s')\n" \
			    "${PROG}" "${local_node}" | logdbgerr
			return 1
		fi
	fi

	#
	# Success. May not reach here.
	#
	return 0
}

#########################################################
#
# get_least_nodeid_node()
#
#	Will parse the infrastructure file and find
#	out the node with the least node id.
#	The global variable $least_nodeid_node
#	will be set to the node name with the least node id.
#
# 	Logic:
#	The infrastructure file has a line which looks like
#		cluster.nodes.1.name    pocho1
#	The function will grep for the first column
#	and then extract the second column which is
#	the node name.
#
#	Return:
#		zero		Success
#		non-zero	Failure
#
#########################################################
get_least_nodeid_node()
{
	typeset nodeid=1
	typeset pattern_1=cluster.nodes.
	typeset pattern_2=.name

	while [[ ${nodeid} -le ${QL_NODEID_MAX} ]]
	do
		pattern=${pattern_1}${nodeid}${pattern_2}
		least_nodeid_node=`grep ${pattern} \
		    ${QL_CCR_INFRASTRUCTURE} | awk '{print $2}'`
		if [[ ! -z ${least_nodeid_node} ]]; then
			#
			# Found the node with the least node id
			#
			printf "$(gettext \
	    		    '%s: Node %s has least node ID %s')\n" \
			    "${PROG}" "${least_nodeid_node}" \
			    "${nodeid}" | logdbgmsg
			return 0
		fi
		nodeid=$(( $nodeid + 1))
		
	done

	#
	#  Error: Did not find the node with the least node id
	#
	printf "$(gettext \
	    '%s: Cluster Configuration Repositary is corrupted')\n" \
	    "${PROG}" | logerr

	#
	# Error
	#
	return 1


}

#########################################################
#
# get_begin_lock
#
#	Will grab the begin lock before executing the
#	begin command.
#
#	This function will also identify the node
#	with the least nodeid in the cluster.
#	
#	The function expects the remote method to be set.
#
#	Return:
#		zero		Success
#		non-zero	Failure
#
#########################################################
get_begin_lock()
{
	typeset hostname=`${HOSTNAME_CMD}`
	typeset node=
	typeset retval=0

	get_least_nodeid_node

	if [[ $? -ne 0 ]]; then
		return 1
	fi

	node=${least_nodeid_node}

	#
	# If the command is getting executed from the CDROM image
	# then copy the $QL_GET_FILE_LOCK utility from the Tools/lib
	# directory of the CDROM image on to ${qllibpath}
	#
	if [[ $from_cdrom -eq ${SC_TRUE} ]]; then
		if [[ ${node} == ${hostname} ]]; then
			/usr/bin/cp -p \
			    ${qlupgradedir}${QL_GET_FILE_LOCK} \
			    ${qllibpath} || return 1
		else
			if [[ "$SC_QL_REMOTE_METHOD" == "ssh" ]]; then
				/usr/bin/scp -Bq \
				${qlupgradedir}${QL_GET_FILE_LOCK} \
				root@${node}:${qllibpath} || return 1
			else
				/usr/bin/rcp -p \
				${qlupgradedir}${QL_GET_FILE_LOCK} \
				$node:${qllibpath} || return 1
			fi

		fi

		printf "$(gettext \
		    '%s: Copied the file %s%s successfully')\n" \
		    "${PROG}" "${qllibpath}" "${QL_GET_FILE_LOCK}" | logdbgmsg
	fi

	#
	# Grab the lock using the $QL_GET_FILE_LOCK utility.
	# If the target node is the local node then directly
	# execute the utility, else use RSH/SSH facility and
	# execute the utility and grab the lock.
	#
	if [[ ${node} == ${hostname} ]]; then
		${qllibpath}${QL_GET_FILE_LOCK} ${QL_BEGIN_RUN_LOCK_FILE}
	else
		execute_cmd ${node} \
		    ${qllibpath}${QL_GET_FILE_LOCK} ${QL_BEGIN_RUN_LOCK_FILE}
	fi
	
	retval=$?
	if [[ ${retval} == 0 ]]; then
		printf "$(gettext \
		    '%s: Grabbed begin lock.')\n" \
		    "${PROG}"  | logdbgmsg
	else
		printf "$(gettext \
		    '%s: Failed to grab begin lock.')\n" \
		    "${PROG}"  | logdbgerr
	
	fi

	return ${retval}

}

#########################################################
#
# Main
#
#########################################################
main()
{
	# Local variables.
	typeset node
	typeset cluster_node

	# Number of nodes in the cluster.
	integer node_count=0

	#
	# This script should be executed from the cdrom image Tools
	# directory or from /usr/cluster/bin
	#
	if [[ "${qlupgradedir##*/}" == "lib" ]]; then
		qllibpath=$QL_TMP_DIR
		from_cdrom=${SC_TRUE}
	else
		qllibpath=${QL_UTIL_PATH}
	fi

	# Load common functions.
	if [[ $from_cdrom -eq ${SC_TRUE} ]]; then
		loadlib ${qlupgradedir}/${QL_UTIL_FILE} || return 1
	else
		loadlib ${QL_UTIL_PATH}/${QL_UTIL_FILE} || return 1
	fi

	#
	# Global zone privilege check. If we are running in a non-global
        # we exit here.
        #
	sc_zones_check

	#
	# Create the debug log file if it does not already exist.
	#
	createlog

	while getopts in:R: c 2>/dev/null
	do
		case ${c} in
		n)	# Nodes of partition A.
			partition_a_nodes="$(IFS=, ; echo ${OPTARG})"
			partition_a_commalist=${OPTARG}
			node_list_opt=${SC_TRUE} 

			if [[ -z $partition_a_nodes ]]; then
				print_usage
				return 1
			fi
			;;
		
		i)	# Interactive mode.
			interactive_mode=${SC_TRUE}
			;;
		
		R)	# Live upgrade mode
			mount_point=`echo ${OPTARG} | sed -e 's/\/$//g'`
			if [[ -z ${mount_point} ]]; then
				print_usage
				return 1
			fi

			live_upgrade_mode=${SC_TRUE}
			interactive_mode=${SC_TRUE}
			;;
		
		\?)	print_usage
			return 1
			;;
		
		*)	# bad option
			print_usage
			return 1
			;;
		esac
	done

	if [[ $node_list_opt -ne ${SC_TRUE} ]]; then
		print_usage
		return 1
	fi

	#
	# check for duplicate entries.
	#
	duplicate ${partition_a_nodes}

	if [[ $? -ne 0 ]]; then
		printf "$(gettext '%s: Bad -n option')\n" "${PROG}"
		return 1
	fi

	#
	# check if there are any spurious arguments.
	#
	shift $(($OPTIND - 1))

	if [[ ! -z "$*" ]]; then
		# bad usage.
		print_usage
		return 1
	fi

	#
	# Make sure we are root.
	#
	verify_isroot

	if [[ $? -ne 0 ]]; then
		printf "$(gettext '%s Need to be root to execute command')\n" \
		    "${PROG}"
		return 1
	fi

	#
	# check if command exists
	# 
	if [[ ! -x /usr/sbin/clinfo ]]; then
		printf "$(gettext '%s: Could not find %s')\n" "${PROG}" \
		    "clinfo" | logerr
		return 1
	fi

	#
	# check for cluster mode.
	#
	/usr/sbin/clinfo > /dev/null 2>&1

	if [[ $? -ne 0 ]]; then
		printf "$(gettext '%s Node must be in cluster mode')\n" \
		    "${PROG}"
		return 1
	fi
	
	#
	# Make sure all nodes are in the cluster and booted in cluster mode.
	# If not we cant proceed further with Dual-Partition upgrade.
	#

	#
	# check if command exists.
	#
	if [[ ! -x ${CLUSTER_CMD_PATH}/scstat ]]; then
		printf "$(gettext '%s: Could not find %s')\n" "${PROG}" \ 
		    "scstat" | logerr
		return 1
	fi

	#
	# Check if any node in the cluster is Offline.
	#

	#
	# save and reset the locale
	#
	LC_ALL_BKUP=$LC_ALL
	LC_ALL="C"

	${CLUSTER_CMD_PATH}/scstat -n | /usr/bin/grep "Offline" > \
	    /dev/null 2>&1

	if [[ $? -eq 0 ]]; then
		printf "$(gettext '%s: Some nodes found offline')\n" \
		    "${PROG}" | logerr
		return 1
	fi

	#restore the locale
	LC_ALL=$LC_ALL_BKUP

	#
	# Make sure all the nodes in Partition A are part of the cluster.
	#

	#
	# check if command exists.
	#
	if [[ ! -x ${CLUSTER_CMD_PATH}/scha_cluster_get ]]; then
		printf "$(gettext '%s: Could not find %s')\n" "${PROG}" \
		    "scha_cluster_get" | logerr
		return 1
	fi

	#
	# check if the command exists.
	#
	if [[ ! -x ${HOSTNAME_CMD} ]]; then
                printf "$(gettext '%s: Could not find %s')\n" "${PROG}" \
		    "${HOSTNAME_CMD}" | logerr
                return 1
        fi

	#
	# get the hostname for this node.
	#	
	hostname=`${HOSTNAME_CMD}`

	#
	# Get the list of nodes in the cluster.
	#
	NODELIST=
	NODELIST="$(${CLUSTER_CMD_PATH}/scha_cluster_get -O ALL_NODENAMES)"

	if [[ -z $NODELIST ]]; then
		printf "$(gettext '%s: Could not get the node list')\n" \
		    "${PROG}" | logerr
		return 1
	fi

	#
	# Iterate through the argument list and check if the nodes
	# are really in the cluster.
	# The first node in the list of partition A nodes is considered
	# to be the root node of partition A.
	#
	verify_node_list

	if [[ $? -ne 0 ]]; then
		return 1
	fi

	#
	# Verify that no resource group is in ERROR_STOP_FAILED state
	# and no resource group has RG_SYSTEM property set.
	#
	verify_rg_state $NODELIST

	if [[ $? -ne 0 ]]; then
		return 1
	fi

	#
	# Find remote method of communication.
	#
	find_remote_method $NODELIST

	if [[ $? -ne 0 ]]; then
		printf "$(gettext '%s: Failed to find a remote method')\n" \
		    "${PROG}" | logerr
		return 1
	fi

	#
	# Get the lock before starting to execute the begin command.
	# This is not done earlier, because the user could potentially
	# fix the errors that could happen in the begining of this code
	# and start re-executing the begin command.
	#
	get_begin_lock
	if [[ $? != 0 ]]; then
		printf "$(gettext \
		    '%s: The \"begin\" command is running or has been already run on the cluster')\n" \
		    "${PROG}" | logerr
		    return 1
	fi

	#
	# Form the list of nodes in partition B given the list
	# the list of nodes of partition A.
	#
	get_partition_b_list

	#
	# Mask out SIGHUP and SIGINT
	#
	trap 'ign_sig' HUP INT

	#
	# If we are running off the cdrom image, copy the needed files
	# on all the nodes. The files are put in QL_TMP_DIR.
	#
	copy_files

	if [[ $? -ne 0 ]]; then
		printf "$(gettext \
		    '%s: Failed to copy files to remote nodes')\n" \
		    "${PROG}" | logerr
		return 1
	fi

	#
	# If we are doing QL in LU scenario, check that the
	# mountpoint is valid and the alternate boot environment
	# is mounted on all nodes.
	#
	if [[ $live_upgrade_mode -eq ${SC_TRUE} ]]; then
		validate_mount_point
		if [[ $? -ne 0 ]]; then
			printf "$(gettext \
			    '%s: Mount point %s validation failed')\n" \
			    "${PROG}" "${mount_point}" | logerr
			return 1
		fi
	fi

	printf "$(gettext '%s: Beginning dual-partition upgrade')\n" \
	    "$PROG" | logdbgmsg

	if [[ -x ${QL_LIBSC_PATH}/scds_syslog ]]; then 
		#
		# SCMSGS
		# @explanation
		# The upgrade procedure has begun.
		# @user_action
		# This is an informational message, no user action is needed.
		#
		${QL_LIBSC_PATH}/scds_syslog -p notice -t "Cluster.Upgrade" \
		    -m "Beginning cluster upgrade"
	fi
		
	#
	# Backup CCR and /etc/vfstab on all the nodes of the cluster.
	# Also count the number of nodes in the cluster.
	#
	backup_files $qllibpath

	if [[ $? -ne 0  ]]; then
		return 1
	fi

	#
	# Change the quorum votes of the cluster. The nodes of partition A
	# have a vote of 0 and the vote for the rest remains unchanged.
	#
	change_quorum $partition_b_root_node $partition_b_nodes

	if [[ $? -ne 0 ]]; then
		printf "$(gettext '%s: change_quorum returned error')\n" \
		    "${PROG}" | logerr 
		return 1
	fi
	
	#
	# Execute a pre-shutdown script if any on the nodes of partition A
	# before they are shutdown for clean shutdown of custom applications.
	#
	execute_pre_shutdown $partition_a_nodes

	#
	# If live upgrade is going on, then unmount the new boot
	# environment and activate it. Then reboot the nodes of
	# partition A ( first partition).
	#
	if [[ $live_upgrade_mode -eq ${SC_TRUE} ]]; then
		execute_lu_begin_task $partition_a_nodes
		if [[ $? != 0 ]]; then
			printf "$(gettext \
			    '%s Failed to execute live upgrade related tasks')\n" \
			    "${PROG}" | logerr
			return 1
		fi
		
		#
		# Enable interactive mode so that
		# we do not try to reboot the nodes
		# again.
		#
		interactive_mode=${SC_TRUE}	
	fi

	#
	# Shutdown all the nodes in Partition A. We dont do this if
	# we are being run from an interactive mode.
	#
	if [[ $interactive_mode -ne ${SC_TRUE} ]]; then

		shutdown_partition $partition_a_nodes
		#
		# We dont return here if the Current node belongs to
		# partition A. 
		#
		if [[ $? -ne 0 ]]; then
			printf \
			    "$(gettext '%s: Failed to halt partition A.')\n" \
			    "${PROG}" | logerr 
			return 1
		fi
	fi
	#
	# We may not return here.
	#
	return 0
}
main $*
