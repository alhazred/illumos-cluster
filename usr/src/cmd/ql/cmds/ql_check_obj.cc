/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)ql_check_obj.cc	1.5	08/05/20 SMI"

#include <orb/infrastructure/orb.h>
#include <nslib/ns.h>
#include <scadmin/scconf.h>
#include <h/quantum_leap.h>
#include <locale.h>
#include <ql_err.h>
#include <ql_log.h>
#include <rgm/sczones.h>

//
// ql_check_obj
//
// The following invocations are valid:
//
//	-o	Waits till the RGM object is found to be registered in
//		the local nameserver.
//
//	-r	Check if RGM object is registered. If so, unregister the
//		RGM object from the name-server. Before the unregister,
//		any invocations blocked on this object are unblocked.
//
//	-q	Check if QL object is registered. If so, unregister the
//		QL object from the global name-server. Before the unregister,
//		any invocations blocked on this object are unblocked.
//

extern  ql_errno_t unregister_ql_object(const char *);
extern  ql_errno_t unregister_rgm_object(const char *);
extern	ql_errno_t unblock_rgm(const char *);

const char *quantum_leap::ql_rgm_obj::QL_RGM_OBJ_KEY;

#define	QL_CHECK_OBJ "QL_CHECK_OBJ"

#define	SLEEP_INTERVAL		1

ql_log *logp = NULL;

//
// Print the usage.
//
static
void
print_usage()
{
	(void) printf("Usage:\n");
	(void) printf("ql_check_obj -o|-r|-q\n");
}

//
// If there are any invocations that are blocked on the QL object
// unblock the blocked invocations.
//
// Return Value:
//	QL_NOERR on success
//	Failure:
//		QL_ELOOKUP
//		QL_ESPURIOUSOBJ
//		QL_EINVO
static
ql_errno_t
unblock_ql()
{
	CORBA::Exception 		*exp	 = NULL;
	naming::naming_context_var	ctx_v	 = ns::root_nameserver();

	Environment			env;
	CORBA::Object_var		obj_v;
	quantum_leap::ql_obj_var	qlobj_v;

	ASSERT(!CORBA::is_nil(ctx_v));

	// Look for the QL object in the name server
	obj_v = ctx_v->resolve(quantum_leap::ql_obj::QL_OBJ_KEY, env);

	if ((exp = env.exception()) != NULL) {

		if (naming::not_found::_exnarrow(exp) != NULL) {
			logp->log_info(QL_CHECK_OBJ, "QL object not found\n");
		} else {
			logp->log_info(QL_CHECK_OBJ, "Unknown exception\n");
		}
		env.clear();
		return (QL_ELOOKUP);
	} else {
		// QL Object found!.
		logp->log_info(QL_CHECK_OBJ, "Found QL object\n");

		//
		// Narrow the object to a quantum leap object.
		//
		qlobj_v = quantum_leap::ql_obj::_narrow(obj_v);

		if (CORBA::is_nil(qlobj_v)) {
			//
			// The object is not of the correct type.
			//
			logp->log_err(QL_CHECK_OBJ, "Spurious RGM object\n");
			env.clear();
			return (QL_ESPURIOUSOBJ);
		}

		logp->log_info(QL_CHECK_OBJ, "Unblock blocked ql_process\n");

		qlobj_v->unblock_ql(env);

		if ((exp = env.exception()) != NULL) {

			logp->log_info(QL_CHECK_OBJ,
			    "Encountered an exception during unblock_ql "
			    "invocation\n");
			env.clear();
			return (QL_EINVO);
		}
		logp->log_info(QL_CHECK_OBJ, "Unblocked blocked ql_process\n");
	}
	return (QL_NOERR);
}

//
// Wait till we find that the RGM object has been registered in the name-server.
// Return Value:
//	QL_NOERR on success
//	Failure:
//		QL_ELOOKUP
//
static
ql_errno_t
wait_for_register()
{
	CORBA::Exception 		*exp	 =	NULL;
	naming::naming_context_var	ctx_v	 =	ns::local_nameserver();
	ql_errno_t			retval	 =	QL_NOERR;

	Environment			env;
	CORBA::Object_var		obj_v;

	ASSERT(!CORBA::is_nil(ctx_v));

	while (true) {
		//
		// Look for the RGM object in the name server.
		//
		obj_v = ctx_v->resolve(quantum_leap::ql_rgm_obj::QL_RGM_OBJ_KEY,
		    env);

		if ((exp = env.exception()) != NULL) {

			if (naming::not_found::_exnarrow(exp) != NULL) {
				logp->log_info(QL_CHECK_OBJ,
				    "RGM object not found...yet\n");
			} else {
				logp->log_info(QL_CHECK_OBJ,
				    "Unknown exception ...!!\n");
				retval = QL_ELOOKUP;
				break;
			}
			env.clear();
		} else {
			// RGM object found!.
			logp->log_info(QL_CHECK_OBJ, "Found RGM object\n");
			break;
		}
		(void) sleep(SLEEP_INTERVAL);
	}
	return (retval);
}

//
// Main routine.
// Return value:
//	QL_NOERR On success.
//	Failure:
//		QL_ENOMEM
//		QL_ENOLOG
//		QL_EEXEC
//		QL_EBADARG
//		QL_EORB
//		QL_EUNBIND
//		QL_ELOOKUP
//
int
main(int argc, char * const argv[])
{
	// Option flags.

	// Option specifies a wait for RGM object to be registered.
	int		oflag		= 0;

	//
	// Option specifies a check for registration of RGM object.
	// If registered any blocked invocations on the the RGM object
	// is unblocked and the RGM object is un-registered.
	//
	int		rflag		= 0;

	//
	// Option specifies a check for registration of Quantum-leap object.
	// If registered any blocked invoations on the Quantum-leap object
	// is unblocked and the Quantum-leap object is un-registered.
	//
	int		qflag		= 0;

	int 		retval		= 0;
	uint_t		ismember	= 0;
	scconf_errno_t  rstatus		= SCCONF_EUNEXPECTED;
	int		arg;

	//
	// Return an error if run from a non-global zone.
	//
	if (sc_zonescheck() != 0) {
		return (1);
	}

	//
	// I18N support.
	//
	(void) textdomain(TEXT_DOMAIN);
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);
	(void) setlocale(LC_ALL, "");

	//
	// The logs go to the default log file.
	//
	logp = ql_log::get_instance(NULL);

	if (logp == NULL) {
		(void) printf(gettext("log object is null\n"));
		return (QL_ENOLOG);
	}

	//
	// Get command line options.
	//
	optind = 1;
	while ((arg = getopt(argc, argv, "orq?")) != EOF) {
		// switch on the option.
		switch (arg) {
		case 'r':
			//
			// Check and unregister the RGM object from the local
			// name-server.
			//
			rflag = 1;
			break;
		case 'q':
			//
			// Check and unregister the QL object from the global
			// name-server.
			//
			qflag = 1;
			break;
		case 'o':
			//
			// Wait till "RGM object" is found in the local
			// name-server.
			//
			oflag = 1;
			break;
		case '?':
			print_usage();
			return (QL_EBADARG);
		default:
			logp->log_err(QL_CHECK_OBJ,
			    "Error parsing arguments\n");
			print_usage();
			return (QL_EBADARG);
		} // switch(arg)
	}

	//
	// Before we go ahead and execute the options, verify that
	// we are being executed in cluster mode.
	//
	rstatus = scconf_ismember(0, &ismember);

	if (rstatus != SCCONF_NOERR || !ismember) {
		logp->log_err(QL_CHECK_OBJ, "Should be in cluster mode\n");
		return (QL_EBADARG);
	}

	//
	// Initialize ORB.
	//
	if ((retval = ORB::initialize()) != 0) {
		logp->log_err(QL_CHECK_OBJ, "Failed to initialize ORB, "
		    "return value = %d\n", retval);
		return (QL_EORB);
	}

	//
	// Execute an option.
	//
	if (rflag) {

		retval = unblock_rgm(QL_CHECK_OBJ);

		if (retval != QL_ELOOKUP) {
			retval = unregister_rgm_object(QL_CHECK_OBJ);
		}

	} else if (qflag) {

		retval = unblock_ql();

		if (retval != QL_ELOOKUP) {
			retval = unregister_ql_object(QL_CHECK_OBJ);
		}

	} else if (oflag) {
		//
		// Wait for the RGM object to register in the global
		// nameserver.
		//
		retval = wait_for_register();
	} else {
		logp->log_err(QL_CHECK_OBJ, "Error parsing arguments\n");
		retval = QL_EBADARG;
	}
	logp->close();
	return (retval);
}
