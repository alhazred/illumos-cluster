//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)ql_get_file_lock.cc	1.3	08/05/20 SMI"

//
// This utility is supposed to be called with a filename as parameter.
// e.g. ql_get_file_lock /etc/cluster/apply_run
//
// This utility is meant to be used for grabbing a lock by creating the
// file passed as a parameter. If the file creation succeeds, that means
// the lock was grabbed. In that case the return value is 0.
//
// If the file creation fails for whatever reasosn, the lock is not
// grabbed and the return value is non zero.
//
// The caller has to take care to release the lock. This utility will
// not relase the lock. Releasing the lock means deleting the file.
//

// Note:
// This binary is executed in the "scinstall -u begin" command of QL and
// hence should not link with any sun cluster libraries. This is becase
// at that point the old software is running. Hence the zones check is
// not done.
//
// This binary is also executed from the "scinstall -u apply" command.
//

#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <locale.h>

//
// Print the usage.
//
static void
print_usage()
{
	(void) printf("Usage:\n"
	    "ql_get_file_lock <filename>\n");
}

int
main(int argc, char * const argv[])
{
	int fd = 0;

	//
	// I18N housekeeping.
	//
	(void) setlocale(LC_ALL, "");
	(void) textdomain(TEXT_DOMAIN);
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	if (argc != 2) {
		//
		// Improper usage.
		// Display the usage and return error.
		//
		print_usage();
		return (1);
	}

	//
	// Try to create the file.
	// If the file already exists
	// then file creation will fail.
	//
	fd = open(argv[1], O_CREAT | O_EXCL, S_IRUSR | S_IWUSR);

	if (fd < 0) {
		//
		// File already exists
		// Unable to get lock
		//
		return (1);
	}


	(void) close(fd);

	//
	// Successfully grabbed the lock
	// by creating the file
	//
	return (0);
}
