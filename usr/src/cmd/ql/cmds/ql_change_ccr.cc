/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)ql_change_ccr.cc	1.6	08/05/20 SMI"

#include <string.h>
#include <orb/infrastructure/orb.h>
#include <scadmin/scconf.h>
#include <locale.h>
#include <ql_ccr.h>
#include <ql_err.h>
#include <ql_log.h>
#include <ql_cl_ccr.h>
#include <ql_ncl_ccr.h>
#include <rgm/sczones.h>

#define	QL_CHANGE_CCR "CHANGE_CCR_CMD"

//
// ql_change_ccr
//
// The following invocations are valid:
// 	-x -r A		Print the root node of partition A followed by the
//			list of nodes in partition A in non-cluster mode.
//
//	-x -r B		Print the root node of partition B followed by the
//			list of nodes in partition B in non-cluster mode.
//
//	-x -a rootA,[nodeA1,nodeA2..] -b rootB
//
//			Updates the CCR, adds root node of partition A,
//			nodes of partition A, root node of partition B,
//			nodes of partition B and sets the quantum-leap state
//			to QL_UPGRADE_STATE_1
//
//	-x -w partition_a_root_node
//
//			Updates the CCR in non-cluster mode. Sets the root
//			node of partition A vote to 1 and the vote of the rest
//			of the nodes to 0.
//
//	-x -v sc_transport_version
//
//			Updates the CCR in non-cluster mode. Adds the transport
//			release version information to the CCR.
//
//	-x -c 		Cleans up the quantum-leap entries in non-cluster-mode.
//
//	-c		Cleans up the quantum-leap entries in cluster-mode.
//
//	-q		Returns 0 if quantum-leap upgrade is in progress.
//			Returns 1 otherwise.
//

//
// Log object.
//
ql_log	*logp = NULL;

//
// Defines actions to be taken when executed in non-cluster mode.
//
uint_t		read_entries		= 0;
// flag used to read QL entries.
uint_t		partition_a		= 0;
uint_t		add_entries		= 0;
uint_t		swap_vote		= 0;
uint_t		add_transport_version	= 0;

// Define actions to be taken when executed in cluster mode.
uint_t		query_status	= 0;

// Used in both cluster and non-cluster modes.
uint_t		remove_entries	= 0;

//
// Returns a linked list of nodenames.
//
static ql_errno_t
commalist_to_namelist(char *nodelist, namelist_t ** namelist_ptr)
{
	namelist_t *listelemp = NULL;
	namelist_t *headp = NULL;
	namelist_t *tailp = NULL;
	nodename_t name = NULL;
	const char *delimiter = ",";
	//
	// For each element in the comma list make new
	// namelist_t and add it to the linked list.
	//
	name = (char *)strtok(nodelist, delimiter);
	while (name != NULL) {
		listelemp = new namelist_t;
		if (listelemp) {
			listelemp->name = strdup(name);
			listelemp->next = NULL;
		}
		if (!listelemp) {
			logp->log_err(QL_CHANGE_CCR,
			    "Memory allocation failed, data size = %d\n",
			    sizeof (namelist_t));

			return (QL_ENOMEM);
		} else if (!listelemp->name) {
			logp->log_err(QL_CHANGE_CCR,
			    "Memory allocation failed, data size = %d\n",
			    strlen(name));
			return (QL_ENOMEM);
		}
		if (headp == NULL) {
			headp = listelemp;
		}
		if (tailp) {
			tailp->next = listelemp;
		}
		tailp = listelemp;
		listelemp = NULL;
		//
		// Get the next element from the comma list
		//
		name = (char *)strtok(NULL, delimiter);
	}
	*namelist_ptr = headp;
	return (QL_NOERR);
}

//
// Free linked list.
//
static void
cleanup(namelist_t *listp)
{
	namelist_t	*listelemp = NULL;

	while (listp) {
		listelemp = listp->next;
		delete (listp);
		listp = listelemp;
	}
}

//
// Print the usage.
// Arguments:
//	The name of this binary.
//
static void
print_usage()
{
	(void) printf("Usage:\n"
	    "ql_change_ccr -x -r {-A|-B}|-w rootA|-c|-v\n"
	    "ql_change_ccr -x -a rootA,[nodeA,..] -b rootB\n"
	    "ql_change_ccr -q|-c\n");
}

//
// Prints the root node of partition A followed by the list of nodes of
// partition A.
// Return Value:
// 	QL_NOERR on Success.
//	QL_ECCR	on Failure.
//
static ql_errno_t
get_partition_A(ql_ncl_ccr *qlobj)
{
	char		*partition_a_root	= NULL;
	namelist_t	*listp			= NULL;
	ql_errno_t	retval			= QL_NOERR;

	//
	// Print root node of partition A followed by all nodes of partition
	// A.
	//
	retval = qlobj->ql_read_root_a(&partition_a_root);

	if (retval != QL_NOERR) {
		logp->log_err(QL_CHANGE_CCR, "Error while reading root node of "
		    "partition A\n");
		return (QL_ECCR);
	}
	(void) printf("%s\n", partition_a_root);

	delete [] partition_a_root;
	retval = qlobj->ql_read_partition_a(&listp);

	if (retval != QL_NOERR) {
		logp->log_err(QL_CHANGE_CCR, "Error while reading "
		    "partition A\n");
		cleanup(listp);
		return (QL_ECCR);
	}
	while (listp) {
		(void) printf("%s\n", listp->name);
		listp = listp->next;
	}
	return (QL_NOERR);
}

//
// Prints the root node of partition B followed by the list of nodes of
// partition B.
// Return Value:
// 	QL_NOERR on Success.
//	QL_ECCR	on Failure.
//
static ql_errno_t
get_partition_B(ql_ncl_ccr *qlobj)
{
	char		*partition_b_root	= NULL;
	namelist_t	*listp			= NULL;
	ql_errno_t	retval			= QL_NOERR;

	//
	// Print root node of partition B followed by all nodes of partition B.
	//
	retval = qlobj->ql_read_root_b(&partition_b_root);

	if (retval != QL_NOERR) {
		logp->log_err(QL_CHANGE_CCR, "Error while reading root node of "
		    "partition B\n");
		return (QL_ECCR);
	}
	(void) printf("%s\n", partition_b_root);

	delete [] partition_b_root;
	retval = qlobj->ql_read_partition_b(&listp);

	if (retval != QL_NOERR) {
		logp->log_err(QL_CHANGE_CCR, "Error while reading "
		    "partition A\n");
		cleanup(listp);
		return (QL_ECCR);
	}
	while (listp) {
		(void) printf("%s\n", listp->name);
		listp = listp->next;
	}
	return (QL_NOERR);
}

//
// Execute the command in non-cluster mode.
// Return Value:
//	0, 1 on Success.
//	Failure:
//		QL_EBADARG
//		QL_ENOMEM
//		QL_ECCR
//
static int
execute_noncluster_mode(const char *const partition_a_root_node,
    const char *const partition_b_root_node,
    namelist_t *partition_a_listp, uint_t sc_transport_version)
{
	ql_ncl_ccr	*qlobj	= NULL;
	int		retval;

	qlobj = new ql_ncl_ccr();

	if (qlobj == NULL) {
		logp->log_err(QL_CHANGE_CCR, "Failed allocating memory "
		    "to CCR object\n");
		return (QL_ENOMEM);
	}
	if (read_entries) {

		if (partition_a) {
			//
			// Print root node of partition A followed
			// by all nodes of partition A.
			//
			retval = get_partition_A(qlobj);

			if (retval) {
				logp->log_err(QL_CHANGE_CCR, "Error while "
				    "reading root node of partition A\n");
				return (QL_ECCR);
			}
		} else {
			//
			// Print root node of partition B followed
			// by all nodes of partition B.
			//
			retval =  get_partition_B(qlobj);

			if (retval) {
				logp->log_err(QL_CHANGE_CCR, "Error while "
				    "reading root node of partition B\n");
				return (QL_ECCR);
			}
		}
	} else if (add_entries) {
		//
		// Add "quantum-leap" entries to the CCR.
		//
		logp->log_info(QL_CHANGE_CCR, "Start CCR edit\n");

		retval = qlobj->ql_write_ccr(partition_b_root_node,
		    partition_a_listp);

		if (retval != QL_NOERR) {
			logp->log_info(QL_CHANGE_CCR, "End CCR edit\n");
		}

	} else if (swap_vote) {
		//
		// Swap CCR vote of root nodes of the two
		// partitions.
		//
		retval = qlobj->ql_change_ccr_vote(partition_a_root_node);

	} else if (remove_entries) {
		//
		// Cleanup the CCR of "quantum-leap" entries.
		//
		retval = qlobj->ql_cleanup_ccr();

	} else if (add_transport_version) {
		//
		// Add transport version entry to the CCR.
		//
		retval = qlobj->ql_write_version(sc_transport_version);

	} else {
		print_usage();
		return (QL_EBADARG);
	}

	logp->close();
	return (retval);
}

//
// Execute the command in cluster mode.
// Return Value:
//	0, 1 on Success.
//	Failure:
//		QL_EORB
//		QL_ENOMEM
//		QL_EBADARG
//
int
execute_cluster_mode()
{
	int		retval		= QL_NOERR;
	ql_cl_ccr	*qlobj		= NULL;
	bool		ql_in_progress	= false;

	if (ORB::initialize() != 0) {
		logp->log_err(QL_CHANGE_CCR, "Failed to initialize ORB, "
		    "return value = %d\n", retval);
		return (QL_EORB);
	}
	qlobj = new ql_cl_ccr();

	if (qlobj ==  NULL) {
		logp->log_err(QL_CHANGE_CCR,
		    "Unable to allocate CCR object\n");
		return (QL_ENOMEM);
	}

	if (remove_entries) {
		//
		// cleanup the CCR of "quantum-leap" entries.
		//
		retval = qlobj->ql_cleanup_ccr();

	} else if (query_status) {
		//
		// check if quantum-leap upgrade is in progress.
		//
		ql_in_progress = qlobj->ql_upgrade_in_progress();
		if (ql_in_progress) {
			retval = 0;
		} else {
			retval = 1;
		}
	} else {
		print_usage();
		retval = QL_EBADARG;
	}
	return (retval);
}

//
// Main routine.
// Return Value:
//	Success:
//	0, 1
//	Failure:
//		QL_ENOLOG
//		QL_EBADARG
//		QL_EORB
//		QL_ECCR
//		QL_ENOMEM
//
int
main(int argc, char * const argv[])
{
	// Option flags.
	uint_t 		aflag			= 0;
	uint_t 		bflag			= 0;
	uint_t		cflag			= 0;
	uint_t		qflag			= 0;
	uint_t		wflag			= 0;
	uint_t		xflag			= 0;
	uint_t		rflag			= 0;
	uint_t		Aflag			= 0;
	uint_t		Bflag			= 0;
	uint_t		Rflag			= 0;
	uint_t		vflag			= 0;
	uint_t		chkflg			= 0;
	uint_t		ismember		= 0;
	uint_t		cluster_mode		= 0;
	char 		*partition_b_root_node	= NULL;
	char 		*partition_a_root_node	= NULL;
	namelist_t 	*partition_a_listp	= NULL;
	uint_t		sc_transport_version	= 0;
	scconf_errno_t	rstatus			= SCCONF_EUNEXPECTED;
	int		retval;
	int		arg;

	//
	// Return an error if run from a non-global zone.
	//
	if (sc_zonescheck() != 0) {
		return (1);
	}

	//
	// I18N housekeeping.
	//
	(void) setlocale(LC_ALL, "");
	(void) textdomain(TEXT_DOMAIN);
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	//
	// Set NULL as the argument. The logs go to the default
	// log file.
	//
	logp = ql_log::get_instance(NULL);

	if (logp == NULL) {
		(void) printf(gettext("log object is null!\n"));
		return (QL_ENOLOG);
	}

	//
	// Get command line options
	//
	optind = 1;
	while ((arg = getopt(argc, argv, "qxrRABw:ca:b:v:?")) != EOF) {
		// switch on the option.
		switch (arg) {
		case 'x':
			//
			// This option specifies that we are reading
			// or writing to CCR in non-cluster mode.
			//
			xflag = 1;
			break;
		case 'a':
			//
			// This option is followed by a comma seperated
			// list of nodes of partition A. We parse this
			// list and create a linked list of nodes.
			//
			aflag = 1;
			if ((optarg == NULL) || (*optarg == NULL)) {
				logp->log_err(QL_CHANGE_CCR,
				    "Invalid arguments \n");
				return (QL_EBADARG);
			}
			if (commalist_to_namelist(optarg,
			    &partition_a_listp) != 0) {
				logp->log_err(QL_CHANGE_CCR,
				    "Error parsing node list for "
				    "partition B\n");
				return (QL_EBADARG);
			}
			break;
		case 'b':
			//
			// This option is followed by the root node of
			// partition B.
			//
			bflag = 1;
			if ((optarg == NULL) || (*optarg == NULL)) {
				logp->log_err(QL_CHANGE_CCR,
				    "Invalid arguments \n");
				return (QL_EBADARG);
			}
			partition_b_root_node = strdup(optarg);
			break;
		case 'c':
			//
			// This option is used to clear the CCR of
			// quantum-leap entries in both cluster and
			// non-cluster modes.
			//
			cflag = 1;
			break;
		case 'r':
			//
			// This means we are reading the CCR in non-cluster
			// mode.
			//
			rflag = 1;
			break;
		case 'A':
			//
			// Read from the CCR and return the root node of
			// partition A and the rest of the nodes in partition
			// A.
			//
			Aflag = 1;
			partition_a = 1;
			break;
		case 'B':
			//
			// Read from the CCR and return the root node of
			// partition A and the rest of the nodes in partition
			// B.
			//
			Bflag = 1;
			break;
		case 'R':
			//
			// Running in Live Upgrade mode.
			//
			Rflag = 1;
			break;
		case 'w':
			//
			// This option means we are writing to CCR in
			// non-cluster mode.
			//
			wflag = 1;
			if ((optarg == NULL) || (*optarg == NULL)) {
				logp->log_err(QL_CHANGE_CCR,
				    "Invalid arguments \n");
				return (QL_EBADARG);
			}
			partition_a_root_node = strdup(optarg);
			break;
		case 'q':
			//
			// Check to see if QL upgrade is in progress.
			//
			qflag = 1;
			break;
		case 'v':
			//
			// Add the transport version to the CCR.
			vflag = 1;
			if ((optarg == NULL) || (*optarg == NULL)) {
				logp->log_err(QL_CHANGE_CCR,
				    "Invalid arguments \n");
				return (QL_EBADARG);
			}
			sc_transport_version = (uint_t)atoi(optarg);

			if (sc_transport_version == 0) {
				logp->log_err(QL_CHANGE_CCR,
				    "Invalid arguments \n");
				return (QL_EBADARG);
			}
			break;
		case '?':
			print_usage();
			return (QL_EBADARG);
		default:
			logp->log_err(QL_CHANGE_CCR,
			    "Error parsing arguments\n");
			return (QL_EBADARG);
		} // switch(arg)
	}

	if (xflag) {
		chkflg = aflag + bflag;

		if (chkflg == 2) {
			add_entries = 1;
		}

		if (wflag) {
			swap_vote = 1;
		}

		chkflg = rflag + Aflag + Bflag;

		if (chkflg == 3) {
			print_usage();
			return (QL_EBADARG);
		} else if (chkflg == 2) {
			read_entries = 1;
		}

		if (cflag) {
			remove_entries = 1;
		}

		if (vflag) {
			add_transport_version = 1;
		}
		//
		// We can specify only one task.
		//
		chkflg = add_entries + swap_vote + read_entries +
		    remove_entries + add_transport_version;

		if (chkflg != 1) {
			print_usage();
			return (QL_EBADARG);
		}
		//
		// Make sure there is no option that is expected in
		// cluster-mode.
		//
		if (qflag) {
			print_usage();
			return (QL_EBADARG);
		}
	} else {
		//
		// Make sure that there is no option that is expected in
		// noncluster-mode.
		//
		chkflg = aflag + bflag + rflag + Aflag + Bflag + wflag;

		if (chkflg != 0) {
			print_usage();
			return (QL_EBADARG);
		}
		if (qflag) {
			query_status  = 1;
		} else if (cflag) {
			remove_entries = 1;
		} else {
			print_usage();
			return (QL_EBADARG);
		}
		cluster_mode = 1;
	}

	//
	// Make sure we are in the cluster.
	// No need to do the check if we are in Live Upgrade mode.
	//
	if (Rflag == 0) {
		rstatus = scconf_ismember(0, &ismember);
		if (rstatus != SCCONF_NOERR) {
			logp->log_err(QL_CHANGE_CCR,
			    "scconf_ismember returned an error, error "
			    "returned = %d\n", rstatus);
			return (QL_SCCONFERR);
		}
		if ((ismember ^ cluster_mode) != 0) {
			if (!ismember) {
				logp->log_err(QL_CHANGE_CCR,
				    "Should be in cluster mode\n");
			} else {
				logp->log_err(QL_CHANGE_CCR,
				    "Should be in noncluster mode\n");
			}
			return (QL_EBADARG);
		}
	} else {
		logp->log_info(QL_CHANGE_CCR,
		    "Running in Live Upgrade mode\n");
	}

	if (cluster_mode) {
		//
		// Execute cluster mode option.
		//
		retval = execute_cluster_mode();
	} else {
		//
		// Execute a non-cluster mode option.
		//
		retval = execute_noncluster_mode(partition_a_root_node,
		    partition_b_root_node, partition_a_listp,
		    sc_transport_version);
	}
	logp->close();
	return (retval);
}
