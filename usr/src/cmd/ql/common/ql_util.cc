/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)ql_util.cc	1.3	08/05/20 SMI"

#include <sys/wait.h>
#include <nslib/ns.h>
#include <h/quantum_leap.h>
#include <ql_err.h>
#include <ql_log.h>

extern ql_log *logp;

//
// Utility function definitions.
//

char *
alloc_mem(const char *tag, size_t size)
{
	char *buffer = new char[size];
	if (buffer == NULL) {
		logp->log_err(tag, "Memory allocation failed, "
		    " data size = %d\n", size);
		exit(QL_ENOMEM);
	}
	return (buffer);
}

//
// Executes a command with popen. It returns the return value
// of the command through the status argument.
// Return value:
//	QL_NOERR on Success
//	QL_EEXEC on Failure
//
ql_errno_t
execute_cmd_with_popen(const char *tag, const char *cmd, int *status)
{
	FILE *stream	= NULL;
	int retval	= 0;

	if ((stream = popen(cmd, "r")) == NULL) {
		logp->log_err(tag, "Failed to execute the command %s\n", cmd);
		return (QL_EEXEC);
	}
	if ((retval = pclose(stream)) == -1) {
		logp->log_err(tag,
		    "Failed to close the pipe created for command %s\n", cmd);
		return (QL_EEXEC);
	}
	if (WIFEXITED(retval)) {

		//
		// Obtain the exit status as the command completed normally.
		//
		*status =  WEXITSTATUS((uint_t)retval);
	} else {

		//
		// Process was terminated by the signal.
		//
		logp->log_err(tag,
		    "The command %s was terminated by the signal\n", cmd);
		*status = -1;
		return (QL_EEXEC);
	}
	return (QL_NOERR);
}

//
// Check the type of exception and log the corresponding error
// message.
//
static void
check_unregister_exception(const char *tag, CORBA::Exception *exp)
{
	if (naming::not_found::_exnarrow(exp)) {
		logp->log_err(tag,
		    "not_found exception when unregistering CORBA object\n");

	} else if (naming::cannot_proceed::_exnarrow(exp)) {
		logp->log_err(tag, "cannot_proceed exception when "
		    "unregistering CORBA object\n");

	} else if (naming::invalid_name::_exnarrow(exp)) {
		logp->log_err(tag, "invalid_name exception when "
		    "unregistering CORBA object\n");

	} else {
		logp->log_err(tag, "Unknown exception when "
		    "unregistering CORBA object\n");
	}
}

//
// Unregisters the quantum-leap object from the global name server.
// Return value:
// 	QL_NOERR On success
//	Failure:
//		QL_EUNBIND
//		QL_ELOOKUP
//
ql_errno_t
unregister_ql_object(const char *tag)
{
	Environment 			e;
	CORBA::Exception 		*exp;
	naming::naming_context_var 	root_ns_v;

	root_ns_v =  ns::root_nameserver();

	if (CORBA::is_nil(root_ns_v)) {
		logp->log_err(tag, "Global name-server reference is Null\n");
		return (QL_ELOOKUP);
	}

	logp->log_info(tag, "Unregister QL object\n");

	root_ns_v->unbind(quantum_leap::ql_obj::QL_OBJ_KEY, e);

	if ((exp = e.exception()) != NULL) {
		check_unregister_exception(tag, exp);
		e.clear();
		return (QL_EUNBIND);
	}
	logp->log_info(tag, "QL object unregistered\n");
	return (QL_NOERR);
}

//
// Unblock RGM.
// Return value:
//	QL_NOERR On success.
//	Failure:
//		QL_ELOOKUP
//		QL_EINVO
//		QL_ESPURIOUSOBJ
//
ql_errno_t
unblock_rgm(const char *tag)
{
	Environment			env;
	naming::naming_context_var	ctx_v = ns::local_nameserver();
	CORBA::Exception 		*exp;
	quantum_leap::ql_rgm_obj_var	ql_rgm_obj_v;
	CORBA::Object_var		obj_v;

	ASSERT(!CORBA::is_nil(ctx_v));

	// Look for the RGM object in the name server

	logp->log_info(tag, "Check for RGM Object\n");
	obj_v = ctx_v->resolve(quantum_leap::ql_rgm_obj::QL_RGM_OBJ_KEY, env);

	if ((exp = env.exception()) != NULL) {

		if (naming::not_found::_exnarrow(exp) != NULL) {
			logp->log_err(tag, "RGM object not found\n");
		} else {
			logp->log_err(tag, "Encountered exception while "
			    "resolving RGM object\n");
		}
		env.clear();
		return (QL_ELOOKUP);
	}
	//
	// Narrow the object to the RGM object
	//
	ql_rgm_obj_v = quantum_leap::ql_rgm_obj::_narrow(obj_v);

	if (CORBA::is_nil(ql_rgm_obj_v)) {
		//
		// The object is not of the type "RGM"
		//
		logp->log_err(tag, "Spurious RGM object\n");
		env.clear();
		return (QL_ESPURIOUSOBJ);
	}
	//
	// RGM would have blocked on finding the RGM object. Now signal
	// it to unblock.
	//
	ql_rgm_obj_v->unblock_rgm(env);

	if ((exp = env.exception()) != NULL) {

		logp->log_info(tag, "Encountered an exception during "
		    "unblock_rgm invocation\n");
		env.clear();
		return (QL_EINVO);
	}
	return (QL_NOERR);
}

//
// Unregisters the RGM object from the local nameserver.
// Return value:
// 	QL_NOERR On success.
//	Failure:
//	QL_EUNBIND
//	QL_ELOOKUP
//
ql_errno_t
unregister_rgm_object(const char *tag)
{

	Environment 			e;
	CORBA::Exception 		*exp;
	naming::naming_context_var 	root_ns_v;

	root_ns_v =  ns::local_nameserver();

	if (CORBA::is_nil(root_ns_v)) {
		logp->log_err(tag, "Global name-server reference is Null\n");
		return (QL_ELOOKUP);
	}

	logp->log_info(tag, "Unregister RGM object\n");

	root_ns_v->unbind(quantum_leap::ql_rgm_obj::QL_RGM_OBJ_KEY, e);

	if ((exp = e.exception()) != NULL) {
		check_unregister_exception(tag, exp);
		e.clear();
		return (QL_EUNBIND);
	}
	logp->log_info(tag, "RGM object unregistered\n");
	return (QL_NOERR);
}
