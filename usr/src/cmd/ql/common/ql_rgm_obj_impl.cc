/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)ql_rgm_obj_impl.cc	1.3	08/05/20 SMI"

#include <ql_rgm_obj_impl.h>

#define	QL_RGM_OBJ_IMPL "RGM_OBJ"
#define	QL_RGM_OBJ_LOG	"/var/cluster/logs/ql_server_debug.log"

ql_rgm_obj_impl::ql_rgm_obj_impl() : rgm_block(true)
{
	//
	// Get the pointer to the singleton log object.
	//
	logp = ql_log::get_instance(QL_RGM_OBJ_LOG);
}

ql_rgm_obj_impl::~ql_rgm_obj_impl()
{
	logp = NULL;
}

//
// _unreferenced is called when the last reference to the server
// object is released.
//
void
ql_rgm_obj_impl::_unreferenced(unref_t cookie)
{
	logp->log_info(QL_RGM_OBJ_IMPL, "unreferenced called\n");

	if (_last_unref(cookie)) {
		delete (this);
	} else {
		ASSERT(0);
	}
}

//
// Interface definitions.
//

//
// RGM blocks in this invocation. It is later woken up by
// the ql_rgm process.
//
void
ql_rgm_obj_impl::block_rgm(CORBA::Environment&)
{
	logp->log_info(QL_RGM_OBJ_IMPL, "block_rgm: blocking rgm\n");
	rgm_lock.lock();

	logp->log_info(QL_RGM_OBJ_IMPL, "block_rgm: %d\n", rgm_block);
	while (rgm_block) {
		rgm_cv.wait(&rgm_lock);
	}

	rgm_lock.unlock();
	logp->log_info(QL_RGM_OBJ_IMPL, "block_rgm: rgm unblocked\n");
}

//
// Returns true if RGM can be blocked on an invocation to this object.
//
bool
ql_rgm_obj_impl::is_rgm_blocked(CORBA::Environment&)
{
	bool rgm_unblock_snapshot;

	logp->log_info(QL_RGM_OBJ_IMPL,
	    "RGM_object: check if rgm can block\n");

	rgm_lock.lock();
	rgm_unblock_snapshot = rgm_block;
	rgm_lock.unlock();
	return (rgm_unblock_snapshot);
}

//
// Signal RGM to unblock and complete its invocation.
//
void
ql_rgm_obj_impl::unblock_rgm(CORBA::Environment&)
{
	logp->log_info(QL_RGM_OBJ_IMPL, "unblock_rgm: unblock rgm\n");

	rgm_lock.lock();
	rgm_block = false;
	rgm_cv.broadcast();
	rgm_lock.unlock();

	logp->log_info(QL_RGM_OBJ_IMPL, "unblock_rgm: unblock rgm complete\n");
}
