/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_QL_NCL_CCR_H_
#define	_QL_NCL_CCR_H_

#pragma ident	"@(#)ql_ncl_ccr.h	1.4	08/05/20 SMI"

#include <ql_ccr.h>
#include <ql_log.h>

typedef namelist_t devnamelist_t;

//
// This class defines interfaces to Add/Remove/Read quantum-leap
// specific entries in the CCR when a node is booted out of cluster mode.
//
// This is used by the command that needs to add/remove/read the
// quantum-leap entries in the CCR during a quantum-leap upgrade.
//
// The methods in this class use the libqlccr library interfaces.
//
class ql_ncl_ccr {

public:
	ql_ncl_ccr();
	~ql_ncl_ccr();

	//
	// Functions to retrieve quantum-leap upgrade specific
	// information from the CCR.
	//

	// Returns the root node of partition A.
	ql_errno_t ql_read_root_a(char **);

	// Returns the root node of partition B.
	ql_errno_t ql_read_root_b(char **);

	// Returns the list of partition A nodes.
	ql_errno_t ql_read_partition_a(namelist **);

	// Returns the list of partition B nodes.
	ql_errno_t ql_read_partition_b(namelist **);

	//
	// Remove quantum-leap specific information from the CCR.
	//

	// Cleanup the CCR of quantum-leap entries.
	ql_errno_t ql_cleanup_ccr();

	//
	// Update the quantum-leap specific information in the CCR.
	//

	// Adds the quantum_leap entries in the CCR.
	ql_errno_t ql_write_ccr(const char *const, namelist_t *);

	// Adds the transport version entry to the CCR.
	ql_errno_t ql_write_version(uint_t);

	// Change the CCR vote of the root nodes.
	ql_errno_t ql_change_ccr_vote(const char *const);

private:

	// Allocate memory.
	char *alloc_mem(size_t);

	// Free linked list.
	void cleanup(namelist_t *);

	// Check if a node is part of a node list.
	bool check_node(namelist_t *, const char *const);

	// Add node to the list.
	ql_errno_t append_list(namelist_t **, const char *);

	// Get a list of all the nodes in the cluster.
	ql_errno_t get_nodelist(namelist_t **);

	// Read the root node of a partition.
	ql_errno_t ql_read_root_node(const char *, char**);

	// Returns the node list in a partition.
	ql_errno_t ql_read_partition(const char *, namelist_t **);

	// Read the elements in the CCR.
	ql_errno_t read_ccr_elements(const char *, namelist_t **, int);

	//
	// Disable all the quorum devices. The device names specified in the
	// argument list are not disabled.
	//
	ql_errno_t disable_quorum(devnamelist_t *);

	ql_log		*logp;  // log object that encapsulates information
				// needed for logging.
};

#endif /* !_QL_NCL_CCR_H_ */
