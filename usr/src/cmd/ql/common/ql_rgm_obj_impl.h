/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ql_obj_impl.h
 *
 */

#ifndef	_QL_RGM_OBJ_IMPL_H_
#define	_QL_RGM_OBJ_IMPL_H_

#pragma ident	"@(#)ql_rgm_obj_impl.h	1.3	08/05/20 SMI"

#include <orb/object/adapter.h>
#include <h/quantum_leap.h>
#include <ql_log.h>

//
// Quantum_leap RGM object definition. This is used to block RGM from
// starting services till a stage where it is safe to start the services.
//
class ql_rgm_obj_impl : public McServerof <quantum_leap::ql_rgm_obj> {

public:
	ql_rgm_obj_impl();
	~ql_rgm_obj_impl();

	void _unreferenced(unref_t);

	//
	// Interface definitions
	//

	// Interface function used by RGM.
	void block_rgm(CORBA::Environment&);

	//
	// Returns true if RGM can be blocked on an invocation to this
	// object.
	//
	bool is_rgm_blocked(CORBA::Environment&);

	//
	// Signals blocked RGM invocation to complete.
	//
	void unblock_rgm(CORBA::Environment&);

private:

	// flag if set means RGM needs to be unblocked.
	bool rgm_block;

	// pointer to the log object.
	ql_log *logp;

	os::mutex_t rgm_lock;
	os::condvar_t rgm_cv;
};

#endif /* !_QL_RGM_OBJ_IMPL_H_ */
