/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)ql_log.cc	1.5	08/05/20 SMI"

#include <ql_log.h>
#include <stdarg.h>


//
// Private constructor.
//
ql_log::ql_log()
{
}

//
// Private destructor.
//
ql_log::~ql_log()
{
	if (ql_log::logfilep != NULL) {
		(void) fflush(ql_log::logfilep);
		(void) fclose(ql_log::logfilep);
	}
}

//
// Initialize file pointer.
//
FILE *ql_log::logfilep = NULL;

ql_log ql_log::ql_log_obj;	 //lint !e1502

//
// Get the reference to the singleton object. The object
// is created if it does not exist.
//
ql_log*
ql_log::get_instance(const char *logfile) {

	if (ql_log::logfilep == NULL) {

		if (logfile == NULL) {

			if ((ql_log::logfilep = fopen(QL_LOG_FILE,  "ab+"))
			    == NULL) {
				//
				// Could not open the default log file.
				// This error has to be handled by the caller.
				//
				return (NULL);
			}

		} else {

			if ((ql_log::logfilep = fopen(logfile,  "ab+"))
			    == NULL) {
				//
				// Could not open the specified log file.
				// This error has to be handled by the caller.
				//
				return (NULL);
			}
		}
	}
	return (&ql_log::ql_log_obj);
}

//
// This function is used to log an error message. The first argument is the
// client which is logging the message. The second argument is the actual
// message.
//
void
ql_log::log_err(const char *prgname, const char *format, ...) const
{
	time_t		curr_time;
	char 		buffer[MAX_LOG_LEN];
	va_list		arg_list;
	char		temp_time[32];

	va_start(arg_list, format);	/*lint !e40 */
	(void) vsprintf(buffer, format, arg_list);
	(void) time(&curr_time);
	(void) snprintf(temp_time, 31, ctime(&curr_time));
	temp_time[strlen(temp_time) - 1] = '\0';

	(void) fprintf(ql_log::logfilep, "%s %s %s %s\n", temp_time, prgname,
	    "ERROR", buffer);

	(void) fflush(ql_log::logfilep);
	va_end(arg_list);
}

//
// This function should be used to log an informational message. The first
// argument is the client which is logging the message. The second argument
// is the actual message.
//
void
ql_log::log_info(const char *prgname, const char *format, ...) const
{
	time_t		curr_time;
	char		buffer[MAX_LOG_LEN];
	va_list		arg_list;
	char		temp_time[32];

	va_start(arg_list, format);	/*lint !e40 */
	(void) vsprintf(buffer, format, arg_list);
	(void) time(&curr_time);
	(void) snprintf(temp_time, 31, ctime(&curr_time));
	temp_time[strlen(temp_time) - 1] = '\0';

	(void) fprintf(ql_log::logfilep, "%s %s: %s %s\n", temp_time,
	    prgname, "INFO ", buffer);

	(void) fflush(ql_log::logfilep);
	va_end(arg_list);
}

//
// Flush data and close the file pointer for the log file.
//
void
ql_log::close() const
{
	if (ql_log::logfilep != NULL) {
		(void) fflush(ql_log::logfilep);
		(void) fclose(ql_log::logfilep);
	}
}
