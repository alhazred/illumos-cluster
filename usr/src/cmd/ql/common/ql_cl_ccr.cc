/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)ql_cl_ccr.cc	1.4	08/05/20 SMI"

#include <orb/infrastructure/orb.h>
#include <nslib/ns.h>
#include <ql_err.h>
#include <ql_cl_ccr.h>

#define	QL_CCR "CCR"

//
// Contructor.
//
ql_cl_ccr::ql_cl_ccr() : ccrdir_p(NULL)
{
	int	retval = QL_NOERR;

	//
	// Get the pointer to the singleton log object.  A NULL argument
	// here means that debug logs go to a default file.
	//
	logp = ql_log::get_instance(NULL);

	//
	// Initialize ORB. If we are not able to initialize the ORB here
	// all the subsequent operations on this object are bound to fail.
	// Failure is to be handled correctly by the entity that uses this
	// object.
	//
	if ((retval = ORB::initialize()) != 0) {
		logp->log_err(QL_CCR, "Failed to initialize ORB, ret value"
		    "  = %d\n", retval);
		return;
	}

	naming::naming_context_var 	ctxp_v;
	Environment			env;
	CORBA::Object_var		obj_v;
	CORBA::Exception		*exp;

	//
	// Lookup the CCR directory.
	//
	ctxp_v = ns::local_nameserver();

	obj_v = ctxp_v->resolve(CCR_DIR, env);

	if ((exp = env.exception()) != NULL) {

		if (naming::not_found::_exnarrow(exp)) {
			logp->log_err(QL_CCR, "CCR directory not found in name"
			    " server\n");

		} else if (naming::cannot_proceed::_exnarrow(exp)) {
			logp->log_err(QL_CCR, "cannot_proceed exception when"
			    " resolving CCR directory\n");

		} else if (naming::invalid_name::_exnarrow(exp)) {
			logp->log_err(QL_CCR, "invalid_name exception when"
			    " resolving CCR directory\n");

		} else {
			logp->log_err(QL_CCR, "Unknown exception when"
			    " resolving CCR directory, id = %d\n",
			    exp->exception_enum());
		}
		env.clear();
	}
	ccrdir_p = ccr::directory::_narrow(obj_v);
}

//
// Destructor
// Release the CCR directory reference.
//
// Lint gives 'function may throw exception warning'.
//lint -e1551
//
ql_cl_ccr::~ql_cl_ccr()
{
	CORBA::release(ccrdir_p);
	ccrdir_p = nil;
	logp = NULL;
}
//lint +e1551

//
// Check the type of CCR exception and log an error.
// Return value:
//	QL_NOERR if no exception occured.
//	QL_ECCR if there was some exception.
//
ql_errno_t
ql_cl_ccr::ql_check_ccr_exception(const char *func, Environment &e)
{
	char			nodename [CL_MAX_LEN + 1];
	ccr::system_error 	*errorp		= NULL;
	CORBA::Exception	*exp		= NULL;

	if ((exp = e.exception()) == NULL) {
		return (QL_NOERR);
	}

	//
	// There was an exception. Check the exception type and log
	// the information.
	//
	if (ccr::no_such_table::_exnarrow(exp)) {
		logp->log_err(QL_CCR, "%s: Table not found in CCR\n", func);

	} else if (ccr::table_modified::_exnarrow(exp)) {
		logp->log_err(QL_CCR, "%s: Table was modified\n", func);

	} else if ((errorp = ccr::system_error::_exnarrow(exp)) != NULL) {

		clconf_get_nodename((nodeid_t)errorp->nodeid, nodename);
		logp->log_err(QL_CCR, "%s: Can't access CCR metadata on node"
		    " %s errno=%d\n", func, nodename, errorp->errnum);

	} else if (ccr::table_invalid::_exnarrow(exp)) {
		logp->log_err(QL_CCR, "%s: Invalid table %s\n", func);

	} else if (ccr::database_invalid::_exnarrow(exp)) {
		logp->log_err(QL_CCR, "%s: The CCR repository is"
		    " corrupted or directory doesn't exist\n", func);

	} else if (ccr::out_of_service::_exnarrow(exp)) {
		logp->log_err(QL_CCR, "%s: Got an out of service exception"
		    " from the CCR\n", func);

	} else if (ccr::lost_quorum::_exnarrow(exp)) {
		logp->log_err(QL_CCR, "%s: Got a loss of quorum exception"
		    " from the CCR\n", func);

	} else if (ccr::readonly_access::_exnarrow(exp)) {
		logp->log_err(QL_CCR, "%s: Got a readonly_access exception"
		    " from the CCR\n", func);

	} else if (ccr::no_such_key::_exnarrow(exp)) {
		logp->log_err(QL_CCR, "%s: Got a no no_such_key exception\n",
		    func);

	} else {
		logp->log_err(QL_CCR, "%s: Got an unknown exception, id = %d\n",
		    func, exp->exception_enum());
	}
	e.clear();
	return (QL_ECCR);
}

//
// Allocate memory.
//
char*
ql_cl_ccr::alloc_mem(size_t size)
{
	char *buffer = new char[size];
	if (buffer == NULL) {
		logp->log_err(QL_CCR, "Memory allocation failed,"
		    " data size = %d\n", size);
		exit(QL_ENOMEM);
	}
	return (buffer);
}

//
// Free memory allocated to the list. Also releases references for
// readonly table and updatable table if they are not null.
//
void
ql_cl_ccr::cleanup(namelist_t *headp, ccr::readonly_table_ptr table_p,
    ccr::updatable_table_ptr trans_p)
{
	namelist_t 	*list_elemp;

	//
	// delete the linked list
	//
	while (headp) {
		list_elemp = headp->next;
		delete headp;
		headp = list_elemp;
	}
	//
	// Release references.
	//
	if (!CORBA::is_nil(table_p)) {
		CORBA::release(table_p);
	}
	if (!CORBA::is_nil(trans_p)) {
		CORBA::release(trans_p);
	}
}

//
// Walk through the CCR infrastructure file and generate a linked list
// of elements that have keys with a matching prefix.
// Return value:
//	if flag is 0 , The linked list of keys is returned in the argument
//	listp. If the flag is 1, the list of data corresponding to the keys
//	is returned.
//	QL_NOERR on success.
//	Failure:
//		QL_ENOMEM
//		QL_ECCR
//		QL_ELOOKUP
//
ql_errno_t
ql_cl_ccr::read_ccr_elements(const char *prefix, namelist_t **listp, int flag)
{
	Environment			e;
	ccr::readonly_table_ptr		table_p;
	ccr::table_element		*elementp = NULL;
	CORBA::Exception		*exp = NULL;
	namelist_t			*listelemp = NULL;
	namelist_t			*headp = NULL;
	namelist_t			*tailp = NULL;

	//
	// We had failed to resolve the directory. Return error.
	//
	if (CORBA::is_nil(ccrdir_p)) {
		return (QL_ECCR);
	}

	table_p = ccrdir_p->lookup(QL_TABLE, e);

	if (ql_check_ccr_exception("READ_CCR_ELEM", e) != QL_NOERR) {
		return (QL_ELOOKUP);
	}

	table_p->atfirst(e);

	if (ql_check_ccr_exception("READ_CCR_ELEM", e) != QL_NOERR) {
		return (QL_ECCR);
	}

	//
	// Here we walk through the CCR and generate a linked list of
	// the elements of the quantum-leap entries that have keys with
	// the expected prefix.
	//

	while (true) {

		table_p->next_element(elementp, e);

		if ((exp = e.exception()) != NULL) {
			if (ccr::NoMoreElements::_exnarrow(exp)) {
				// End of table
				break;
			} else {
				(void) ql_check_ccr_exception("READ_CCR_ELEM",
				    e);
				cleanup(headp, table_p, NULL);
				e.clear();
				return (QL_ECCR);
			}
		}

		//
		// Assumption: There wont be any new entries in the
		// CCR that are not quantum-leap entries with the same prefix!.
		// This is unlikely to happen.
		//
		if (strstr((char *)elementp->key, prefix) != NULL) {

			listelemp = new namelist_t;

			if (listelemp) {
				//
				// Add key or data to the list.
				//
				if (flag) {
					listelemp->name =
					    strdup(elementp->data);
				} else {
					listelemp->name = strdup(elementp->key);
				}
				listelemp->next = NULL;
			}
			if (!listelemp) {
				logp->log_err(QL_CCR, "Memory allocation"
				    " failed, data size = %d\n",
				    sizeof (namelist_t));
				cleanup(headp, table_p, NULL);
				return (QL_ENOMEM);

			} else if (!listelemp->name) {
				if (flag) {
					logp->log_err(QL_CCR,
					    "Memory allocation failed,"
					    " data size = %d\n",
					    strlen(elementp->data));
				} else {
					logp->log_err(QL_CCR,
					    "Memory allocation failed,"
					    " data size = %d\n",
					    strlen(elementp->key));
				}
				cleanup(headp, table_p, NULL);
				return (QL_ENOMEM);
			}
			if (headp == NULL) {
				headp = listelemp;
			}
			if (tailp) {
				tailp->next = listelemp;
			}
			tailp = listelemp;
			listelemp = NULL;
		}
		delete elementp;
	}
	cleanup(NULL, table_p, NULL);
	*listp = headp;
	return (QL_NOERR);
}

//
// Cleanup all CCR entries added as part of the Quantum-Leap upgrade.
// Return value:
//	QL_NOERR on successful cleanup.
//	Failure:
//		QL_ENOMEM
//		QL_ECCR
//		QL_ELOOKUP
//
ql_errno_t
ql_cl_ccr::ql_cleanup_ccr()
{

	ccr::updatable_table_ptr	trans_p;
	ccr::readonly_table_ptr		table_p;
	Environment			env;
	namelist_t			*headp		= NULL;
	namelist_t			*listp		= NULL;
	char				*state		= NULL;

	//
	// We had failed to resolve the directory. Return error.
	//
	if (CORBA::is_nil(ccrdir_p)) {
		return (QL_ECCR);
	}

	logp->log_info(QL_CCR, "cleanup Quantum_leap entries in the CCR\n");

	//
	// Generate a linked list with the keys of the quantum-leap entries
	// which we use to remove the elements from the CCR.
	//
	if (read_ccr_elements(QL_PARTITION_PREFIX, &headp, 0) != QL_NOERR) {
		cleanup(headp, NULL, NULL);
		return (QL_ECCR);
	}

	table_p = ccrdir_p->lookup(QL_TABLE, env);

	if (ql_check_ccr_exception("CCR_CLEANUP", env) != QL_NOERR) {
		cleanup(headp, NULL, NULL);
		return (QL_ELOOKUP);
	}

	state = table_p->query_element(QL_STATE_KEY, env);

	if (ql_check_ccr_exception("CCR_CLEANUP", env) != QL_NOERR) {
		cleanup(headp, table_p, NULL);
		return (QL_ECCR);
	}

	//
	// Begin transaction on table QL_TABLE
	//
	trans_p = ccrdir_p->begin_transaction(QL_TABLE, env);

	if (ql_check_ccr_exception("CCR_CLEANUP", env) != QL_NOERR) {
		cleanup(headp, table_p, NULL);
		CORBA::release(trans_p);
		return (QL_ECCR);
	}

	//
	// The state key is not removed from the CCR. It is reset to
	// QL_UPGRADE_COMPLETE_STATE.
	//
	if (state != NULL) {

		logp->log_info(QL_CCR, "Reset key %s from %s to %d\n",
		    QL_STATE_KEY, state, QL_UPGRADE_COMPLETE_STATE);

		char *newstate = alloc_mem(2);

		(void) sprintf(newstate, "%d", QL_UPGRADE_COMPLETE_STATE);

		trans_p->update_element(QL_STATE_KEY, newstate, env);

		if (ql_check_ccr_exception("CCR_CLEANUP", env) != QL_NOERR) {
			trans_p->abort_transaction(env);
			cleanup(headp, table_p, trans_p);
			delete [] state;
			delete [] newstate;
			return (QL_ECCR);
		}
	}

	//
	// Remove the quantum_leap keys from the CCR.
	//
	listp = headp;

	while (listp != NULL) {

		logp->log_info(QL_CCR, "Removing key %s \n", listp->name);

		trans_p->remove_element(listp->name, env);

		if (ql_check_ccr_exception("CCR_CLEANUP", env) != QL_NOERR) {
			trans_p->abort_transaction(env);
			cleanup(headp, table_p, trans_p);
			return (QL_ECCR);
		}
		listp = listp->next;
	}

	trans_p->commit_transaction(env);

	if (ql_check_ccr_exception("CCR_CLEANUP", env) != QL_NOERR) {
		cleanup(headp, table_p, trans_p);
		return (QL_ECCR);
	}

	logp->log_info(QL_CCR, "cleanup of Quantum_leap entries in the CCR"
	    "done\n");

	//
	// Free memory allocated to the linked list. This also releases
	// CORBA references.
	//
	cleanup(headp, table_p, trans_p);
	return (QL_NOERR);
}

//
// Read from the CCR the root node of partition B.
//
// Arguments:
//	partition_b_root: root node of partition B.
// Return value:
//	The root node is returned in partition_b_root.
//	QL_NOERR on success
//	Failure:
//		QL_ECCR
//		QL_ELOOKUP
//
ql_errno_t
ql_cl_ccr::ql_read_root_b(char **partition_b_root)
{
	ccr::readonly_table_ptr		table_p;
	Environment			e;
	char 				*rootb = NULL;

	//
	// We had failed to resolve the directory. Return error.
	//
	if (CORBA::is_nil(ccrdir_p)) {
		return (QL_ECCR);
	}

	logp->log_info(QL_CCR, "Lookup for partition B root node in the CCR\n");

	table_p = ccrdir_p->lookup(QL_TABLE, e);

	if (ql_check_ccr_exception("READ_ROOT_B", e) != QL_NOERR) {
		return (QL_ELOOKUP);
	}

	rootb = table_p->query_element(QL_PARTITION_B_ROOT_KEY, e);

	if (ql_check_ccr_exception("READ_ROOT_B", e) != QL_NOERR) {
		e.clear();
		return (QL_ECCR);
	}

	*partition_b_root = rootb;
	return (QL_NOERR);
}

//
// Read from the CCR the root node of partition A.
// Arguments:
//	partition_a_root: root node of partition A.
// Return value:
//	The root node is returned in partition_a_root.
//	QL_NOERR  on success
//	Failure:
//		QL_ECCR
//		QL_ELOOKUP
//
ql_errno_t
ql_cl_ccr::ql_read_root_a(char **partition_a_root)
{
	ccr::readonly_table_ptr		table_p;
	Environment			e;
	char 				*roota = NULL;

	//
	// We had failed to resolve the directory. Return error.
	//
	if (CORBA::is_nil(ccrdir_p)) {
		return (QL_ECCR);
	}

	logp->log_info(QL_CCR, "Lookup for partition A root node in the CCR\n");

	table_p = ccrdir_p->lookup(QL_TABLE, e);

	if (ql_check_ccr_exception("READ_ROOT_A", e) != QL_NOERR) {
		return (QL_ELOOKUP);
	}

	roota = table_p->query_element(QL_PARTITION_A_ROOT_KEY, e);

	if (ql_check_ccr_exception("READ_ROOT_A", e) != QL_NOERR) {
		e.clear();
		return (QL_ECCR);
	}

	*partition_a_root = roota;
	return (QL_NOERR);
}

//
// Change the state of the quantum-leap upgrade in the CCR.
// Arguments:
//	state: The current state of quantum-leap upgrade.
// Return value:
//	QL_NOERR on success
//	Failure:
//		QL_ECCR
//
ql_errno_t
ql_cl_ccr::ql_change_ccr_state(ql_ccr_state_t statenum)
{
	ccr::updatable_table_ptr	trans_p;
	Environment			e;
	char				*state	= NULL;

	//
	// We had failed to resolve the directory. Return error.
	//
	if (CORBA::is_nil(ccrdir_p)) {
		return (QL_ECCR);
	}

	logp->log_info(QL_CCR, "changing quantum_leap state in the CCR\n");

	//
	// Begin transaction on table "infrastructure"
	//
	trans_p = ccrdir_p->begin_transaction(QL_TABLE, e);

	if (ql_check_ccr_exception("CHANGE_QL_CCR_STATE", e) != QL_NOERR) {
		return (QL_ECCR);
	}

	state = alloc_mem(2);
	(void) sprintf(state, "%d", statenum);

	//
	// Add the quantum_leap state key to the CCR.
	//
	trans_p->update_element(QL_STATE_KEY, state, e);

	delete [] state;

	if (ql_check_ccr_exception("CHANGE_QL_CCR_STATE", e) != QL_NOERR) {
		trans_p->abort_transaction(e);
		e.clear();
		CORBA::release(trans_p);
		return (QL_ECCR);
	}

	trans_p->commit_transaction(e);

	if (ql_check_ccr_exception("CHANGE_QL_CCR_STATE", e) != QL_NOERR) {
		CORBA::release(trans_p);
		return (QL_ECCR);
	}
	logp->log_info(QL_CCR, "updated key=%s data=%d\n", QL_STATE_KEY,
	    statenum);

	return (QL_NOERR);
}

//
// Check if quantum-leap upgrade is in the same state as statenum.
// Return value:
//	true	if upgrade state matches with statenum.
//	false	if upgrade state does not match with statenum.
//
bool
ql_cl_ccr::ql_upgrade_check_state(ql_ccr_state_t statenum)
{
	ccr::readonly_table_ptr		table_p;
	Environment			e;
	CORBA::Exception		*exp;
	char 				*state	= NULL;
	char				*cstate	= NULL;

	//
	// We had failed to resolve the directory. Return error.
	//
	if (CORBA::is_nil(ccrdir_p)) {
		return (!QL_NOERR);
	}

	table_p = ccrdir_p->lookup(QL_TABLE, e);

	if (ql_check_ccr_exception("QL_CHECK_STATE", e) != QL_NOERR) {
		logp->log_err(QL_CCR, "CCR directory lookup failed\n");
		return (false);
	}

	state = table_p->query_element(QL_STATE_KEY, e);

	if ((exp = e.exception()) != NULL) {
		if (ccr::no_such_key::_exnarrow(exp)) {
			// Key not found. Upgrade is not in progress.
			logp->log_info(QL_CCR, "Upgrade state key not found\n");
		} else {
			logp->log_err(QL_CCR, "Exception occured during "
			    "upgrade state key lookup, id = \n",
			    exp->exception_enum());
		}
		CORBA::release(table_p);
		e.clear();
		return (false);
	}

	logp->log_info(QL_CCR, "Read upgrade state = %s\n", state);

	cstate = alloc_mem(2);
	(void) sprintf(cstate, "%d", statenum);

	//
	// check if quantum-leap upgrade state was a match.
	//
	if (strcmp(state, cstate) == 0) {
		return (true);
	}

	return (false);
}

//
// Check if quantum-leap upgrade is in progress.
// Return value:
//	true	if quantum-leap upgrade is in progress.
//	false	if quantum-leap upgrade is not in progress.
//
bool
ql_cl_ccr::ql_upgrade_in_progress()
{
	//
	// upgrade can be in one of any three states.
	//
	if (ql_upgrade_check_state(QL_UPGRADE_STATE_1) ||
	    ql_upgrade_check_state(QL_UPGRADE_STATE_2)) {
		return (true);
	}
	return (false);
}

//
// Read the CCR in cluster-mode. This function builds a linked list of
// nodes of a partition.
// Return value:
//	The linked list is returned in partition_listp.
//	QL_NOERR on Success
//	Failure:
//		QL_ECCR
//		QL_ENOMEM
//		QL_EBADARG
//
ql_errno_t
ql_cl_ccr::ql_read_ccr_partition(const char *partition_prefix,
    namelist_t **partition_listp)
{
	ql_errno_t retval = QL_NOERR;

	*partition_listp = NULL;

	if (strcmp(partition_prefix, QL_PARTITION_A_PREFIX) == 0) {

		logp->log_info(QL_CCR, "Reading elements of partition A in"
		    "the CCR\n");

	} else if (strcmp(partition_prefix, QL_PARTITION_B_PREFIX) == 0) {
		logp->log_info(QL_CCR, "Reading elements of partition B in"
		    "the CCR\n");
	} else {
		logp->log_err(QL_CCR, "Got an unknown partition prefix\n");
		return (QL_EBADARG);
	}

	//
	// Generate a linked list with the data corresponding to the
	// keys that specify the nodes of a partition.
	//
	retval = read_ccr_elements(partition_prefix, partition_listp, 1);
	return (retval);
}

//
// Add an entry to the CCR to mark the fact that cleanup is not complete
// on the cluster node given by nodename.
// Return Value:
//	QL_NOERR on Success.
//	QL_ECCR on Failure.
//
ql_errno_t
ql_cl_ccr::mark_need_cleanup(const char *nodename)
{
	ccr::updatable_table_ptr	trans_p;
	Environment			env;

	//
	// We had failed to resolve the directory. Return error.
	//
	if (CORBA::is_nil(ccrdir_p)) {
		return (QL_NOERR);
	}
	trans_p = ccrdir_p->begin_transaction(QL_TABLE, env);

	if (ql_check_ccr_exception("MARK_CLEANUP", env) != QL_NOERR) {
		return (QL_ECCR);
	}

	char *cleanup_key = alloc_mem(strlen(QL_CLEANUP_KEY) + 1 +
	    strlen(nodename) + 1);

	(void) sprintf(cleanup_key, "%s.%s", QL_CLEANUP_KEY, nodename);

	logp->log_info(QL_CCR, "Adding key = %s value = %s\n", cleanup_key,
	    "1");

	trans_p->add_element(cleanup_key, "1", env);

	delete [] cleanup_key;

	if (ql_check_ccr_exception("MARK_CLEANUP", env) != QL_NOERR) {
		CORBA::release(trans_p);
		return (QL_ECCR);
	}

	trans_p->commit_transaction(env);

	if (ql_check_ccr_exception("MARK_CLEANUP", env) != QL_NOERR) {
		CORBA::release(trans_p);
		return (QL_ECCR);
	}

	return (QL_NOERR);
}

//
// Remove the CCR entry showing that QL clean up is not complete.
// on the node given by nodename.
// Return Value:
//	QL_NOERR on Success.
//	Failure:
//
ql_errno_t
ql_cl_ccr::mark_cleanup_complete(const char *nodename)
{
	ccr::updatable_table_ptr	trans_p;
	Environment			env;
	char				*cleanup_key	=  NULL;

	//
	// We had failed to resolve the directory. Return error.
	//
	if (CORBA::is_nil(ccrdir_p)) {
		return (QL_NOERR);
	}
	trans_p = ccrdir_p->begin_transaction(QL_TABLE, env);

	if (ql_check_ccr_exception("CLEANUP_COMPLETE", env) != QL_NOERR) {
		return (QL_ECCR);
	}

	cleanup_key = alloc_mem(strlen(QL_CLEANUP_KEY) + 1 +
	    strlen(nodename) + 1);

	(void) sprintf(cleanup_key, "%s.%s", QL_CLEANUP_KEY, nodename);

	logp->log_info(QL_CCR, "Removing key = %s value = %s\n",
	    cleanup_key, "1");

	trans_p->remove_element(cleanup_key, env);

	delete [] cleanup_key;

	if (ql_check_ccr_exception("CLEANUP_COMPLETE", env) != QL_NOERR) {
		CORBA::release(trans_p);
		return (QL_ECCR);
	}

	trans_p->commit_transaction(env);

	if (ql_check_ccr_exception("CLEANUP_COMPLETE", env) != QL_NOERR) {
		CORBA::release(trans_p);
		return (QL_ECCR);
	}

	CORBA::release(trans_p);
	return (QL_NOERR);
}

//
// Check if QL cleanup is complete. This returns true if
// cleanup has been done on all the configured nodes, returns
// false otherwise.
//
bool
ql_cl_ccr::cleanup_complete(const char *nodename)
{
	Environment			env;
	ccr::readonly_table_ptr		table_p;
	CORBA::Exception		*exp	= NULL;
	char				*cleanup_key	=  NULL;

	//
	// We had failed to resolve the directory. Return error.
	//
	if (CORBA::is_nil(ccrdir_p)) {
		return (!QL_NOERR);
	}

	table_p = ccrdir_p->lookup(QL_TABLE, env);

	if (ql_check_ccr_exception("IS_CLEANUP_COMPLETE", env) != QL_NOERR) {
		return (false);
	}

	cleanup_key = alloc_mem(strlen(QL_CLEANUP_KEY) + 1 +
	    strlen(nodename) + 1);

	(void) sprintf(cleanup_key, "%s.%s", QL_CLEANUP_KEY, nodename);

	(void) table_p->query_element(cleanup_key, env);

	delete [] cleanup_key;

	if ((exp = env.exception()) != NULL) {
		if (ccr::no_such_key::_exnarrow(exp)) {
			// Key not found. Cleanup done.
			CORBA::release(table_p);
			env.clear();
			return (true);
		} else {
			logp->log_err(QL_CCR, "Exception occured during "
			    "lookup of %s, id = %d\n", QL_CLEANUP_KEY,
			    exp->exception_enum());
			env.clear();
			CORBA::release(table_p);
		}
	}
	return (false);
}
