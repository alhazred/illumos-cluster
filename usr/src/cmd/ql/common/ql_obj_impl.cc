/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)ql_obj_impl.cc	1.3	08/05/20 SMI"

#include <ql_obj_impl.h>

#define	QL_OBJ_IMPL "QL_OBJ"

//
// Constructor.
//
ql_obj_impl::ql_obj_impl() : ql_block(true)
{
	//
	// Get the pointer to the singleton log object.
	//
	logp = ql_log::get_instance(NULL);
}

//
// Destructor.
//
ql_obj_impl::~ql_obj_impl()
{
	logp = NULL;
}

//
// _unreferenced is called when the last reference to the server
// object is released.
//
void
ql_obj_impl::_unreferenced(unref_t cookie)
{
	logp->log_info(QL_OBJ_IMPL, "unreferenced called\n");
	if (_last_unref(cookie)) {
		delete this;
	} else {
		ASSERT(0);
	}
}

//
// Interface definitions.
//

//
// Returns true if there is a blocked invocation to this
// object.
//
bool
ql_obj_impl::is_ql_blocked(CORBA::Environment&)
{
	bool ql_unblock_snapshot;

	logp->log_info(QL_OBJ_IMPL,
	    "quantum_leap_object: check for ql block\n");

	ql_lock.lock();
	ql_unblock_snapshot = ql_block;
	ql_lock.unlock();
	return (ql_unblock_snapshot);
}

//
// Signal the blocked invocations to complete.
//
void
ql_obj_impl::unblock_ql(CORBA::Environment&)
{
	ql_lock.lock();
	ASSERT(ql_block);
	ql_block = false;
	ql_cv.broadcast();
	ql_lock.unlock();
}

//
// Interface method implementation for the blocking invocation.
//
void
ql_obj_impl::block_ql(CORBA::Environment&)
{
	logp->log_info(QL_OBJ_IMPL, "block_ql: blocking ql\n");
	ql_lock.lock();

	while (ql_block) {
		ql_cv.wait(&ql_lock);
	}

	ql_lock.unlock();
	logp->log_info(QL_OBJ_IMPL, "block_ql: ql unblocked\n");
}
