/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_QL_CL_CCR_H_
#define	_QL_CL_CCR_H_

#pragma ident	"@(#)ql_cl_ccr.h	1.3	08/05/20 SMI"

#include <ql_ccr.h>
#include <ql_log.h>

//
// This class defines interfaces to Add/Modify/Remove/Read
// quantum-leap specific information in the CCR in cluster mode.
//
// This is used by the CCR clients that need to do specific tasks
// related to quantum-leap upgrade, for example, check if upgrade
// is in progress, determine the nodes in a partition, change the
// CCR state of the upgrade etc.
//

class ql_cl_ccr {

public:
	ql_cl_ccr();
	~ql_cl_ccr();

	//
	// Functions to retrieve information from the CCR.
	//

	// Returns the node list in a partition.
	ql_errno_t ql_read_ccr_partition(const char *, namelist **);

	// Returns the root node of partition B.
	ql_errno_t ql_read_root_b(char **);

	// Returns the root node of partition A.
	ql_errno_t ql_read_root_a(char **);

	// Return true if quantum-leap upgrade is in progress.
	bool ql_upgrade_in_progress();

	//
	// Return true if the quantum-leap upgrade is in a state
	// specified by the argument.
	//
	bool ql_upgrade_check_state(ql_ccr_state_t);

	//
	// Removes quantum-leap upgrade entries from the CCR.
	//
	ql_errno_t ql_cleanup_ccr();

	//
	// Function that changes quantum-leap specific information
	// in the CCR.
	//

	// Update the quantum-leap upgrade state in the CCR.
	ql_errno_t ql_change_ccr_state(ql_ccr_state_t);

	//
	// Add the entry to identify that cleanup is needed.
	//
	ql_errno_t mark_need_cleanup(const char *);

	//
	// Remove the entry that identifies if cleanup is needed.
	//
	ql_errno_t mark_cleanup_complete(const char *);

	//
	// Check if cleanup is needed.
	//
	bool cleanup_complete(const char *);

private:

	// Check for exception and logs exception information if any.
	ql_errno_t ql_check_ccr_exception(const char *, Environment&);

	//
	// Read from the CCR and build a linked list of elements having keys
	// with a matching prefix.
	//
	ql_errno_t read_ccr_elements(const char *, namelist_t **, int);

	// Allocate memory.
	char *alloc_mem(size_t);

	// Free memory allocated for the linked list.
	void cleanup(namelist_t *, ccr::readonly_table_ptr,
	    ccr::updatable_table_ptr);

	ccr::directory_ptr	ccrdir_p; // CCR Directory pointer.

	ql_log			*logp;  // Log object that encapsulates
					// information needed for logging.
};

#endif /* !_QL_CL_CCR_H_ */
