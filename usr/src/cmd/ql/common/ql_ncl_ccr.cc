/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)ql_ncl_ccr.cc	1.4	08/05/20 SMI"

#include <ccr_access.h>
#include <ql_err.h>
#include <ql_ncl_ccr.h>

#define	QL_CCR "CCR"

ql_ncl_ccr::ql_ncl_ccr()
{
	logp = ql_log::get_instance(NULL);
}

ql_ncl_ccr::~ql_ncl_ccr()
{
	logp = NULL;
}

char *
ql_ncl_ccr::alloc_mem(size_t size)
{
	char *buffer = new char[size];
	if (buffer == NULL) {
		logp->log_err(QL_CCR, "Memory allocation failed,"
		    " data size = %d\n", size);
		exit(QL_ENOMEM);
	}
	return (buffer);
}

//
// Free the linked list.
//
void
ql_ncl_ccr::cleanup(namelist_t *headp)
{
	namelist_t *list_elemp = NULL;
	//
	// delete the linked list
	//
	while (headp) {
		list_elemp = headp->next;
		delete headp;
		headp = list_elemp;
	}
}

//
// Walk through the linked list to check if a node is part
// of the linked list.
//
bool
ql_ncl_ccr::check_node(namelist_t *node_listp, const char * const node_name)
{
	namelist_t	*list_elemp	= NULL;
	bool		found		= false;

	list_elemp = node_listp;

	while (list_elemp) {

		if (strcmp(list_elemp->name, node_name) == 0) {
			found = true;
			break;
		}
		list_elemp = list_elemp->next;
	}
	return (found);
}

//
// Common code to append an element to a linked list. If the list is empty
// a new list is created.
// Return value:
//	QL_NOERR on success.
//	Failure:
//		QL_ENOMEM
//
ql_errno_t
ql_ncl_ccr::append_list(namelist_t **listp, const char *data)
{
	namelist_t	*listelemp = NULL;
	namelist_t	*cur_listp = NULL;

	listelemp = new namelist_t;

	if (listelemp) {
		listelemp->name = strdup(data);
		listelemp->next = NULL;
	}
	if (!listelemp) {
		logp->log_err(QL_CCR, "Memory allocation failed, data size "
		    "= %d\n", sizeof (namelist_t));

		return (QL_ENOMEM);

	} else if (!listelemp->name) {
		logp->log_err(QL_CCR, "Memory allocation failed, data size "
		    "= %d\n", strlen(data));
		return (QL_ENOMEM);
	}
	if (*listp == NULL) {
		*listp = listelemp;
	} else {
		cur_listp = *listp;
		while (cur_listp->next != NULL) {
			cur_listp = cur_listp->next;
		}
		cur_listp->next = listelemp;
	}
	return (QL_NOERR);
}

//
// Read the CCR and return a linked list of all the nodes
// in the cluster.
// Return Value:
//	QL_NOERR on Success.
//	Returns the list of nodes in listp.
//	Failure:
//		QL_ENOMEM
//		QL_ECCR
//
ql_errno_t
ql_ncl_ccr::get_nodelist(namelist_t **listp)
{
	ccr_access_readonly_table	ql_table;
	char				*key		= NULL;
	char 				*ccr_data	= NULL;
	ql_errno_t			retval		= QL_NOERR;
	int				error		= 0;

	nodeid_t			node;

	// Linked list of all nodes.
	namelist_t			*headp	= NULL;

	error = ql_table.initialize(QL_TABLE);

	if (error != 0) {
		logp->log_info(QL_CCR, "Failed to initialize CCR table, error "
		    "= %d\n", error);
		return (QL_ECCR);
	}

	//
	// Find all entries of type:
	//	cluster.nodes.<nodeid>.name	<name>
	// Add <name> to the linked list of nodes in the cluster.
	//
	// Assumption: The CCR infrastructure table wont be changed
	// to not have this entry anymore!
	//

	key = alloc_mem(strlen("cluster.nodes.") + 2 + strlen(".name") + 1);

	for (node = 1; node < NODEID_MAX; ++node) {

		(void) sprintf(key, "cluster.nodes.%d.name", node);

		ccr_data = ql_table.query_element(key);

		if (ccr_data == NULL) {
			// We dont have any node with this node id.
			continue;
		}

		retval = append_list(&headp, ccr_data);

		if (retval != QL_NOERR) {
			cleanup(headp);
			ql_table.close();
			return (retval);
		}
	}
	*listp = headp;
	ql_table.close();
	return (retval);
}

//
// Walk through the CCR infrastructure file and generate a linked list
// of elements that have keys with a matching prefix.
// Return value:
//	QL_NOERR on success.
//	if flag is 0 , The linked list of keys is returned in the argument
//	listp. If the flag is 1, the data corresponding to the keys is
//	returned.
//	Failure:
//		QL_ENOMEM
//		QL_ECCR
//
ql_errno_t
ql_ncl_ccr::read_ccr_elements(const char *prefix, namelist_t **listp, int flag)
{
	ccr_access_readonly_table 	ql_table;
	table_element_t 		*elementp	= NULL;
	namelist_t			*headp		= NULL;
	ql_errno_t			retval		= QL_NOERR;
	int				error;

	error = ql_table.initialize(QL_TABLE);

	if (error != 0) {
		logp->log_info(QL_CCR, "Failed to initialize CCR table, error "
		    "= %d\n", error);
		return (QL_ECCR);
	}

	//
	// We walk through the CCR and generate a linked list of
	// the elements of the quantum-leap entries having keys with
	// the matching prefix.
	//
	ql_table.atfirst();

	while ((elementp = ql_table.next_element()) != NULL) {

		//
		// Assumption: There wont be any new entries in the
		// CCR that are not quantum-leap entries with the same prefix!.
		// This is unlikely to happen.
		//
		if (strstr((char *)elementp->key, prefix) != NULL) {

			logp->log_info(QL_CCR, "Read key = %s\n",
			    elementp->key);

			if (flag) {
				retval = append_list(&headp, elementp->data);
			} else {
				retval = append_list(&headp, elementp->key);
			}
			if (retval != QL_NOERR) {
				cleanup(headp);
				return (retval);
			}
		}
		delete elementp;
	}
	ql_table.close();
	*listp = headp;
	return (QL_NOERR);
}

//
// Reads the CCR from non-cluster mode. Reads the root node
// of either partition.
// Return value:
//	QL_NOERR On success.
//	QL_ECCR	on failure
//
ql_errno_t
ql_ncl_ccr::ql_read_root_node(const char *root_key, char **root_node)
{
	ccr_access_readonly_table	ql_table;
	int				error;

	error = ql_table.initialize(QL_TABLE);

	if (error != 0) {
		logp->log_info(QL_CCR, "Failed to initialize CCR table, error "
		    "= %d\n", error);
		return (QL_ECCR);
	}

	logp->log_info(QL_CCR, "Root key = %s\n", root_key);

	*root_node = ql_table.query_element(root_key);

	if (*root_node == NULL) {

		// Log an error message.
		logp->log_err(QL_CCR, "ql_read_root_node: Key %s not found\n",
		    root_key);

		ql_table.close();

		return (QL_ECCR);

	} else {
		logp->log_info(QL_CCR, "root_key = %s root node = %s\n",
		    root_key, root_node);
	}
	ql_table.close();
	return (QL_NOERR);
} 

//
// Read the CCR in non-cluster mode. This reads the root node of partition A.
// Return value:
//	QL_NOERR on Success
//	QL_ECCR on Failure
//
ql_errno_t
ql_ncl_ccr::ql_read_root_a(char **partition_a_root_node)
{
	ql_errno_t	retval;
	retval =  ql_read_root_node(QL_PARTITION_A_ROOT_KEY,
	    partition_a_root_node);
	return (retval);
}

//
// Read the CCR in non-cluster mode. This reads the root node of partition B.
// Return value:
//	QL_NOERR on Success
//	QL_ECCR on Failure
//
ql_errno_t
ql_ncl_ccr::ql_read_root_b(char **partition_b_root_node)
{
	ql_errno_t	retval;
	retval  =  ql_read_root_node(QL_PARTITION_B_ROOT_KEY,
	    partition_b_root_node);
	return (retval);
}


//
// Read the CCR in non-cluster mode. This function reads
// the list of nodes in a partition specified by the argument.
// Return value:
//	QL_NOERR on Success
//	Failure:
//		QL_ENOMEM
//		QL_ECCR
//
ql_errno_t
ql_ncl_ccr::ql_read_partition(const char *partition_prefix,
    namelist_t **partition_listp)
{
	ql_errno_t retval = QL_NOERR;

	//
	// We generate a linked list with the data of the quantum-leap
	// entries of a partition specified by keys with a prefix
	// that matches "partition_prefix".
	//
	retval = read_ccr_elements(partition_prefix, partition_listp, 1);
	return (retval);
}

//
// Read the CCR in non-cluster mode. This function reads
// the list of nodes in partition A.
// Return value:
//	QL_NOERR on Success
//	Failure:
//	QL_ECCR
//	QL_ENOMEM
//
ql_errno_t
ql_ncl_ccr::ql_read_partition_a(namelist_t **partition_listp)
{
	ql_errno_t	retval;
	retval = ql_read_partition(QL_PARTITION_A_PREFIX, partition_listp);
	return (retval);
}

//
// Read the CCR in non-cluster mode. This function reads
// the list of nodes in partition B.
// Return value:
//	QL_NOERR on Success
//	Failure:
//	QL_ECCR
//	QL_ENOMEM
//
ql_errno_t
ql_ncl_ccr::ql_read_partition_b(namelist_t **partition_listp)
{
	ql_errno_t	retval;
	retval  = ql_read_partition(QL_PARTITION_B_PREFIX, partition_listp);
	return (retval);
}

//
// Cleans up all the entries in the CCR in non-cluster mode.
// Return Value:
//	QL_NOERR on success.
//	Failure:
//		QL_ECCR
//		QL_ENOMEM
//
ql_errno_t
ql_ncl_ccr::ql_cleanup_ccr()
{
	ccr_access_updatable_table	ql_updatable_table;
	namelist_t			*headp		= NULL;
	namelist_t			*part_elemp	= NULL;
	ql_errno_t			retval		= QL_NOERR;
	int				error;

	//
	// We walk through the CCR and generate a linked list with
	// the keys of the quantum-leap entries.
	//
	if ((retval = read_ccr_elements(QL_PREFIX, &headp, 0)) != QL_NOERR) {
		return (retval);
	}

	error = ql_updatable_table.initialize(QL_TABLE);

	if (error != 0) {
		logp->log_info(QL_CCR, "Failed to initialize CCR updatable "
		    "table, error = %d\n", error);
		return (QL_ECCR);
	}

	part_elemp = headp;
	//
	// Remove the "Quantum-leap" related entries from the CCR.
	//
	while (part_elemp != NULL) {
		if (ql_updatable_table.remove_element(part_elemp->name) != 0) {
			// Remove failed.
			logp->log_err(QL_CCR, "Removing entry with key %s"
			    " failed\n", part_elemp->name);
			cleanup(headp);
			return (QL_ECCR);
		}
		logp->log_info(QL_CCR, "Removed entry with key %s\n",
		    part_elemp->name);
		part_elemp = part_elemp->next;
	}
	if (ql_updatable_table.commit() != 0) {
		logp->log_err(QL_CCR, "Commit failed\n");
		return (QL_ECCR);
	}
	cleanup(headp);
	return (QL_NOERR);
}

//
// Write to the CCR in Non-cluster mode. This function disables the
// the quorum entries of quorum devices shared with nodes of partition A.
//
// Argument:
//	None.
// Return Value:
//	QL_NOERR On success.
//	Failure:
//		QL_ENOMEM
//		QL_ECCR
//
ql_errno_t
ql_ncl_ccr::disable_quorum(devnamelist_t *gdevname_listp)
{
	int 				quorum_id;
	char				*quorum_key		= NULL;
	char				*quorum_vote_key	= NULL;
	char				*quorum_state_key	= NULL;
	char				*ccr_data		= NULL;
	devnamelist_t			*dev_elemp		= NULL;
	bool				found			= false;
	int				qd_cnt			= 0;
	int				qd_list[NODEID_MAX];
	int				retval;
	ccr_access_readonly_table 	ql_table;
	ccr_access_updatable_table 	ql_updatable_table;
	int				error;

	error = ql_table.initialize(QL_TABLE);

	if (error != 0) {
		logp->log_info(QL_CCR, "Failed to initialize CCR table, error "
		    "= %d\n", error);
		return (QL_ECCR);
	}

	//
	// Disable all quorum devices that are not local to
	// partition A. We definitely cant have more quorum devices than
	// maximum number of nodes here!.
	//
	quorum_key = alloc_mem(strlen("cluster.quorum_devices.") + 2 +
	    strlen(".properties.gdevname") + 1);

	(void) memset(qd_list, 0, sizeof (qd_list));
	for (quorum_id = 1; quorum_id < NODEID_MAX; quorum_id++) {

		(void) sprintf(quorum_key,
		    "cluster.quorum_devices.%d.properties.gdevname", quorum_id);

		ccr_data = ql_table.query_element(quorum_key);

		if (ccr_data == NULL) {
			continue;
		}

		//
		// If the quorum device is local to the nodes in partition A
		// we need not disable the device.
		//
		found = false;

		dev_elemp = gdevname_listp;

		while (dev_elemp) {
			if (strcmp(dev_elemp->name, ccr_data) == 0) {
				found = true;
				break;
			}
			dev_elemp = dev_elemp->next;
		}

		if (found) {
			delete [] ccr_data;
			continue;
		}

		delete [] ccr_data;

		//
		// Add this node-id to the list of nodes to disable the
		// corresponding quorum devices.
		//
		qd_list[qd_cnt++] = quorum_id;
	}
	ql_table.close();
	delete [] quorum_key;

	quorum_state_key = alloc_mem(strlen("cluster.quorum_devices.") + 2 +
	    strlen(".state") + 1);

	for (quorum_id = 0; quorum_id < qd_cnt; quorum_id++) {

		error = ql_updatable_table.initialize(QL_TABLE);

		if (error != 0) {
			logp->log_info(QL_CCR,
			    "Failed to initialize CCR updatable table, error "
			    "= %d\n", error);
			return (QL_ECCR);
		}

		//
		// Disable the quorum device.
		//
		(void) sprintf(quorum_state_key,
		    "cluster.quorum_devices.%d.state", qd_list[quorum_id]);

		if (ql_updatable_table.update_element(quorum_state_key,
		    "disabled") != 0) {

			logp->log_err(QL_CCR, "Failed to update the quorum"
			    " vote in the CCR\n");
			(void) printf("Failed to update the quorum"
			    " vote in the CCR\n");
			delete [] quorum_state_key;
			return (QL_ECCR);
		}
		if ((retval = ql_updatable_table.commit()) != 0) {
			logp->log_info(QL_CCR, "Commit failed, "
			    "return value=%d\n", retval);
			delete [] quorum_state_key;
			return (QL_ECCR);
		}

		logp->log_info(QL_CCR, "Updated key=%s data=%s\n",
		    quorum_state_key, "disabled");

		delete [] quorum_state_key;

		quorum_vote_key = alloc_mem(strlen("cluster.quorum_devices.")
		    + 2 + strlen(".properties.votecount") + 1);

		(void) sprintf(quorum_vote_key,
		    "cluster.quorum_devices.%d.properties.votecount",
		    qd_list[quorum_id]);

		error = ql_updatable_table.initialize(QL_TABLE);

		if (error != 0) {
			logp->log_info(QL_CCR, "Failed to initialize CCR "
			    "updatable table, error = %d\n", error);
			return (QL_ECCR);
		}

		if (ql_updatable_table.update_element(quorum_vote_key, "0")
		    != 0) {

			logp->log_err(QL_CCR, "Failed to update the quorum"
			    " vote in the CCR\n");
			delete [] quorum_vote_key;
			return (QL_ECCR);
		}

		logp->log_info(QL_CCR, "Updated key=%s data=%s\n",
		    quorum_vote_key, "disabled");

		if ((retval = ql_updatable_table.commit()) != 0) {
			logp->log_info(QL_CCR, "Commit failed, "
			    "return value=%d\n", retval);
			return (QL_ECCR);
		}

		delete [] quorum_vote_key;
	}
	return (QL_NOERR);
}

//
// Write to the CCR in non-cluster mode. This function resets the
// quorum vote of the root node of partition A to one and sets the quorum
// vote of the other nodes to zero. Here we also disable all the quorum
// devices.
//
// Argument:
//	root_node_a: root node of partition A.
//
// Return Value:
//	QL_NOERR On success.
//	Failure:
//		QL_ENOMEM
//		QL_ECCR
//
ql_errno_t
ql_ncl_ccr::ql_change_ccr_vote(const char * const root_node_a)
{
	ccr_access_readonly_table 	ql_table;
	ccr_access_updatable_table 	ql_updatable_table;
	nodeid_t			node;
	char				*ccr_data		= NULL;
	char				*key			= NULL;
	char				*quorum_key		= NULL;
	ql_errno_t			retval			= QL_NOERR;
	nodeid_t			root_node		= 0;
	int				ret_val			= 0;
	uint_t				node_count		= 0;
	int				error;
	nodeid_t			node_list[NODEID_MAX];

	key = alloc_mem(strlen("cluster.nodes.") + 2 + strlen(".name") + 1);

	error = ql_table.initialize(QL_TABLE);

	if (error != 0) {
		logp->log_info(QL_CCR, "Failed to initialize CCR table, error "
		    "= %d\n", error);
		return (QL_ECCR);
	}

	//
	// Get the node-id of all the nodes that are part of the cluster.
	// Also record the node-id of the root node of partition A.
	//
	(void) memset(node_list, 0, sizeof (node_list));
	for (node = 1; node < NODEID_MAX; ++node) {

		(void) sprintf(key, "cluster.nodes.%d.name", node);

		ccr_data = ql_table.query_element(key);

		if (ccr_data == NULL) {
			continue;
		}
		if (strcmp(ccr_data, root_node_a) == 0) {
			root_node = node;
		}

		node_list[node_count ++] = node;

		delete [] ccr_data;
	}

	//
	// If the root node is not found, return error.
	//
	if (root_node == 0) {
		logp->log_err(QL_CCR, "Could not find the root node in the "
		    "cluster\n");
		delete [] key;
		return (QL_ECCR);
	}

	ql_table.close();
	delete [] key;

	quorum_key = alloc_mem(strlen("cluster.nodes.") + 2 +
	    strlen(".properties.quorum_vote") + 1);

	error = ql_updatable_table.initialize(QL_TABLE);

	if (error != 0) {
		logp->log_info(QL_CCR, "Failed to initialize CCR updatable "
		    "table, error = %d\n", error);
		return (QL_ECCR);
	}

	//
	// For each node, reset the node vote except for the root node.
	//
	for (node = 0; node < node_count; node++) {

		(void) sprintf(quorum_key,
		    "cluster.nodes.%d.properties.quorum_vote", node_list[node]);

		if (node_list[node] == root_node) {

			if (ql_updatable_table.update_element(quorum_key, "1")
			    != 0) {
				logp->log_err(QL_CCR,
				    "Failed to update the quorum vote for root "
				    "node of partition A\n");
				delete [] quorum_key;
				return (QL_ECCR);
			}
			logp->log_info(QL_CCR, "Updated key=%s data=%s\n",
			    quorum_key, "1");
		} else {
			if (ql_updatable_table.update_element(quorum_key, "0")
			    != 0) {
				logp->log_err(QL_CCR, "Failed to update the"
				    " quorum vote for a cluster node\n");
				delete [] quorum_key;
				return (QL_ECCR);
			}
			logp->log_info(QL_CCR, "Updated key=%s data=%s\n",
			    quorum_key, "0");
		}
	}
	//
	// Commit the CCR changes.
	//
	if ((ret_val = ql_updatable_table.commit()) != 0) {
		logp->log_err(QL_CCR, "Commit failed, return value = %d",
		    ret_val);
		return (QL_ECCR);
	}

	//
	// Disable Quorum devices.
	//
	retval = disable_quorum(NULL);
	return (retval);
}

//
// Write to the CCR in non-cluster mode. This function adds the following
// to the CCR:
// 	1. Root node of partition B
// 	2. Nodes of partition B
// 	3. Root node of partition A
// 	4. Nodes of partition A
// 	5. Quantum-leap state = QL_UPGRADE_STATE_1
//
// This function changes the the vote of root node of partition A to one
// and the vote of the rest of the nodes to 0.
//
// Here we also disable all the quorum devices.
//
// Argument:
// 	root_node_b: Root node of partition B.
//	partition_a_listp: Nodes in partition A. The first node
//	in this list is taken to be as the root node of partition A.
//
// Return value:
//	QL_NOERR on Success
//	Failure:
//		QL_ECCR
//		QL_ENOMEM
//
ql_errno_t
ql_ncl_ccr::ql_write_ccr(const char *const root_node_b,
    namelist_t *partition_a_listp)
{
	ccr_access_readonly_table 	ql_table;
	ccr_access_updatable_table 	ql_updatable_table;
	char				*partition_a_key	= NULL;
	char				*partition_b_key	= NULL;
	char				*state			= NULL;

	// Linked list of all nodes.
	namelist_t			*headp			= NULL;

	namelist_t			*plist_elemp		= NULL;
	ql_errno_t			retval			= QL_NOERR;
	bool				found			= false;
	int				error;

	//
	// Build the nodes of partition B. Validate inputs
	// What we do in validate?
	// 	Check if all nodes of partition A are configured to be
	//	in the cluster.
	//	Check if partition B root node is configured to be
	//	in the cluster.
	//

	//
	// Get the list of all the nodes in the cluster.
	//
	if ((retval = get_nodelist(&headp)) != QL_NOERR) {
		return (QL_ECCR);
	}

	//
	// Is root node of partition B part of the cluster configuration.
	//
	if (!check_node(headp, root_node_b)) {
		logp->log_err(QL_CCR, "Node %s not configured in the"
		    " cluster\n", root_node_b);
		cleanup(headp);
		return (QL_EBADARG);
	}

	//
	// Check each node of partition A is part of cluster configuration.
	//
	plist_elemp = partition_a_listp;

	while (plist_elemp) {
		if (!check_node(headp, partition_a_listp->name)) {
			logp->log_err(QL_CCR, "Node %s not configured in the"
			    " cluster\n", plist_elemp->name);
			cleanup(headp);
			return (QL_EBADARG);
		}
		plist_elemp = plist_elemp->next;
	}

	//
	// Root node of partition B should not be in partition A.
	//
	found = check_node(partition_a_listp, root_node_b);

	if (found) {
		logp->log_err(QL_CCR, "Root node of parition B %s is part of "
		    "partition A\n", root_node_b);
		return (QL_ECCR);
	}

	error = ql_updatable_table.initialize(QL_TABLE);

	if (error != 0) {
		logp->log_info(QL_CCR, "Failed to initialize CCR updatable "
		    "table, error = %d\n", error);
		return (QL_ECCR);
	}

	//
	// Add all nodes of partition A to the CCR.
	//
	plist_elemp = partition_a_listp;

	partition_a_key = alloc_mem(strlen(QL_PARTITION_B_PREFIX) + 1 +
	    CL_MAX_LEN + 1);

	while (plist_elemp) {

		(void) sprintf(partition_a_key, "%s.%s", QL_PARTITION_A_PREFIX,
		    plist_elemp->name);

		if (ql_updatable_table.add_element(partition_a_key,
		    plist_elemp->name) != 0) {

			logp->log_err(QL_CCR, "Error while adding %s to"
			    " the CCR\n", partition_a_key);
			delete [] partition_a_key;
			cleanup(headp);
			return (QL_ECCR);
		}

		logp->log_info(QL_CCR, "Created entry key = %s data=%s\n",
		    partition_a_key, plist_elemp->name);

		plist_elemp = plist_elemp->next;
	}

	delete [] partition_a_key;

	//
	// We know the root node of partition B, but we dont know the
	// other nodes. We build it here.
	//
	plist_elemp = headp;

	partition_b_key = alloc_mem(strlen(QL_PARTITION_B_PREFIX) + 1
	    + CL_MAX_LEN + 1);

	while (plist_elemp) {

		found = check_node(partition_a_listp, plist_elemp->name);

		if (!found) {

			(void) sprintf(partition_b_key, "%s.%s",
			    QL_PARTITION_B_PREFIX, plist_elemp->name);

			if (ql_updatable_table.add_element(partition_b_key,
			    plist_elemp->name) != 0) {

				logp->log_err(QL_CCR,
				    "Error while adding %s to the CCR\n",
				    partition_b_key);
				cleanup(headp);
				return (QL_ECCR);
			}
			logp->log_info(QL_CCR,
			    "created entry key = %s data=%s\n",
			    partition_b_key, plist_elemp->name);
		}

		plist_elemp = plist_elemp->next;
	}

	delete [] partition_b_key;
	cleanup(headp);

	//
	// Add root node of partition A to the CCR.
	//
	if (ql_updatable_table.add_element(QL_PARTITION_A_ROOT_KEY,
	    partition_a_listp->name) != 0) {
		logp->log_err(QL_CCR, "Error while adding %s to the CCR\n",
		    QL_PARTITION_A_ROOT_KEY);
		return (QL_ECCR);
	}

	logp->log_info(QL_CCR, "Created entry key = %s data=%s\n",
	    QL_PARTITION_A_ROOT_KEY, partition_a_listp->name);

	//
	// Add root node of partition B to the CCR.
	//
	if (ql_updatable_table.add_element(QL_PARTITION_B_ROOT_KEY,
	    root_node_b) != 0) {
		logp->log_err(QL_CCR, "Error while adding %s to the CCR\n",
		    QL_PARTITION_B_ROOT_KEY);
		return (QL_ECCR);
	}

	logp->log_info(QL_CCR, "Created entry key = %s data=%s\n",
	    QL_PARTITION_B_ROOT_KEY, root_node_b);

	//
	// Add quantum-leap state to the CCR.
	//
	state = alloc_mem(2);
	(void) sprintf(state, "%d", QL_UPGRADE_STATE_1);

	if (ql_updatable_table.add_element(QL_STATE_KEY, state)
	    != 0) {
		//
		// Quantum-leap state was probably already there. Try to update.
		//
		if (ql_updatable_table.update_element(QL_STATE_KEY, state)
		    != 0) {
			// Even update failed!
			logp->log_err(QL_CCR, "Error while updating %s to the"
			    " CCR\n", QL_STATE_KEY);
			return (QL_ECCR);
		} else {
			logp->log_info(QL_CCR, "updated key = %s data=%d\n",
			    QL_STATE_KEY, QL_UPGRADE_STATE_1);
		}
	} else {
		logp->log_info(QL_CCR, "created entry key = %s data=%d\n",
		    QL_STATE_KEY, QL_UPGRADE_STATE_1);
	}
	//
	// Commit the changes.
	//
	if ((error = ql_updatable_table.commit()) != 0) {
		logp->log_err(QL_CCR, "Commit failed, return value = %d",
		    error);
		return (QL_ECCR);
	}

	//
	// All nodes of the cluster get 0 votes except the root node of
	// partition A.
	//
	retval = ql_change_ccr_vote(partition_a_listp->name);

	return (retval);
}

//
// Write to the CCR in non-cluster mode. This function adds the transport
// version information to the CCR.
//
// Argument:
//	sc_transport_version: transport version number.
//
// Return value:
//	QL_NOERR on Success
//	Failure:
//		QL_ECCR
//
ql_errno_t
ql_ncl_ccr::ql_write_version(uint_t sc_transport_version)
{
	ccr_access_updatable_table	ql_updatable_table;
	char				transport_version[5];
	int				error;

	error = ql_updatable_table.initialize(QL_TABLE);

	if (error != 0) {
		logp->log_info(QL_CCR, "Failed to initialize CCR updatable "
		    "table, error = %d\n", error);
		return (QL_ECCR);
	}

	//
	// Add the transport release version to the CCR.
	//
	(void) sprintf(transport_version, "%d", sc_transport_version);

	if (ql_updatable_table.update_element(SC_TRANSPORT_VERSION_KEY,
	    transport_version) != 0) {
		logp->log_err(QL_CCR, "Error while adding %s to the CCR ",
		    "data = %s\n", SC_TRANSPORT_VERSION_KEY, transport_version);
		return (QL_ECCR);
	}
	logp->log_info(QL_CCR, "Created entry key = %s data=%s\n",
	    SC_TRANSPORT_VERSION_KEY, transport_version);

	//
	// Commit the changes.
	//
	if ((error = ql_updatable_table.commit()) != 0) {
		logp->log_err(QL_CCR, "Commit failed, return value = %d",
		    error);
		return (QL_ECCR);
	}
	return (QL_NOERR);
}
