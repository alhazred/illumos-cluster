/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ql_obj_impl.h
 *
 */

#ifndef	_QL_OBJ_IMPL_H_
#define	_QL_OBJ_IMPL_H_

#pragma ident	"@(#)ql_obj_impl.h	1.3	08/05/20 SMI"

#include <orb/object/adapter.h>
#include <h/quantum_leap.h>
#include <ql_log.h>

//
// Quantum_leap server object definition. This is used
// during quantum-leap upgrade to co-ordinate the upgrade
// tasks amongst the nodes in the upgrade partition.
// There should be one instantiated object of this class
// in the cluster.
//
class ql_obj_impl : public McServerof <quantum_leap::ql_obj> {

public:
	ql_obj_impl();
	~ql_obj_impl();

	void _unreferenced(unref_t);

	//
	// Interface definitions
	//

	//
	// Returns true if there are invocations that are blocked.
	//
	bool is_ql_blocked(CORBA::Environment&);

	//
	// Signals any blocked invocations to complete.
	//
	void unblock_ql(CORBA::Environment&);

	// Invocation that blocks till the unblock invocation is called.
	void block_ql(CORBA::Environment&);

private:

	bool ql_block;	// flag to block invocation.
	ql_log *logp;	// pointer to the log object.

	os::mutex_t ql_lock;
	os::condvar_t ql_cv;
};

#endif /* !_QL_OBJ_IMPL_H_ */
