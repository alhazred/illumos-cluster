/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_QL_LOG_H_
#define	_QL_LOG_H_

#pragma ident	"@(#)ql_log.h	1.7	08/10/30 SMI"

#include <stdio.h>
#include <string.h>
#include <time.h>

//
// Default log file.
//
#define	QL_LOG_FILE "/var/cluster/logs/install/ql_upgrade_debug.log"
#define	MAX_LOG_LEN 65536

//
// Class definition that encapsulates the logging mechanism
// for the quantum_leap commands.
// This is a singleton class.
//
class ql_log {

public:
	// Returns a singleton instance of the ql_log object.
	static ql_log* get_instance(const char *);

	// log informational message to the log file.
	void log_info(const char *, const char *, ...) const;

	// log error message to the log file.
	void log_err(const char *, const char *, ...) const;

	// Close the log file pointer.
	void close() const;

private:
	// private constructor.
	ql_log();

	// private destructor.
	~ql_log();

	// static ql object.
	static ql_log ql_log_obj;

	// static file pointer
	static FILE *logfilep;
};

#endif /* !_QL_LOG_H_ */
