/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)send_ioctl.c	1.1	08/05/20 SMI"

#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <stdio.h>
#include <errno.h>
#include <limits.h>

#include <sys/lofi.h>
#include <sys/types.h>
#include <sys/stat.h>

/* Size of temporary buffer to prepare error messages */
#define	MESSAGE_BUF_SIZE	256

/* Supported devices */
#define	LOFI_DEVICE_NAME "/dev/lofictl"

/* Print send_ioctl usage message */
void
usage(char *app_name)
{
	printf("Usage:\n%s <device path> <ioctl number> [arguments]\n",
	    app_name);
}

int
lofi_dispatch(int fd, int ioctl_number, char **args)
{
	int error;
	struct lofi_ioctl ioctl_args;

	/* Clear ioctl argument structure */
	(void) memset(&ioctl_args, 0, sizeof (ioctl_args));

	/*
	 * Send the ioctl to device. As and when more ioctls are added, we
	 * will need a switch-case construct to read relevant parameters and
	 * populate the ioctl argument structure accordingly.
	 */
	error = ioctl(fd, ioctl_number, &ioctl_args);
	if (error == -1) {
		perror("ioctl to lofi device driver failed");
		return (5);
	}

	/* Process each supported ioctl as the case maybe */
	switch (ioctl_number) {
	case LOFI_GET_MAXMINOR:
		/* Print out the maximum number of devices possible. */
		printf("%d\n", ioctl_args.li_minor);
		error = 0;
		break;
	default:
		perror("unsupported ioctl for lofi device");
		error = 6;
	}

	return (error);
}	    

int
main(int argc, char **argv)
{
	char	*device_name;
	int	device_fd;
	long	ioctl_number;
	int	error;
	char	message_buf[MESSAGE_BUF_SIZE];

	if (argc < 3) {
		usage(argv[0]);
		return (1);
	}

	/* First parameter is the device to send the ioctl to */
	device_name = argv[1];

	/* second parameter is the ioctl request number */
	ioctl_number = strtol(argv[2], (char **)NULL, 10);

	/* Check if the parameter for ioctl number was valid  */
	if (ioctl_number == LONG_MAX || ioctl_number == LONG_MIN ||
	    (ioctl_number == 0 && errno != 0)) {
		printf("%s: ioctl number '%s' is invalid\n", argv[2]);
		return (2);
	}

	/* Try opening the device */
	device_fd = open(device_name, O_RDONLY | O_EXCL);
	if (device_fd == -1) {
		snprintf(message_buf, MESSAGE_BUF_SIZE,
		    "Open of %s failed", device_name);
		perror(message_buf);
		return (3);
	}

	/*
	 * Call device specific ioctl routine dispatcher with leftover
	 * arguments from command line. We support only lofi now.
	 */
	if (strncmp("/dev/lofictl", LOFI_DEVICE_NAME,
	    sizeof (LOFI_DEVICE_NAME)) == 0) {
		error = lofi_dispatch(device_fd, (int)ioctl_number, argv + 3);
	} else {
		printf("Device \"%s\" is not supported.\n", device_name);
		error = 4;
	}

	(void) close(device_fd);
	return (error);
}
