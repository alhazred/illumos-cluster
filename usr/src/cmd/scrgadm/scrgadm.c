/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scrgadm.c	1.135	08/09/10 SMI"

/* Tell lint that real ANSI C and not C++. Lint reports C++ errors */
/* lint -save -fcp -e1746 */

/*
 * scrgadm(1M)
 */

#include <ctype.h>
#include <unistd.h>
#include <stdio.h>
#include <netdb.h>		/* for hostname mappings */
#include <locale.h>
#include <signal.h>
#include <errno.h>

#include <sys/sol_version.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>		/* for AF_INET */
#include <arpa/inet.h>		/* for IP mappings */
#include <netinet/in.h>		/* for socket options */
#include <net/if.h>

#include <rgm/pnm.h>
#include <rgm/rgm_common.h>
#include <rgm/rgm_cnfg.h>
#include <rgm/sczones.h>
#include <sys/vc_int.h>

#if SOL_VERSION >= __s10
#include <zone.h>
#endif

#include <scadmin/scrgadm.h>
#include <scadmin/scconf.h>

#include <sys/cl_auth.h>
#include <sys/cl_cmd_event.h>

/* Printing */
#define	PRINTALL_ARG		"."	/* -tgj option arg to print all */

#define	LABELW1			30
#define	LABELW2			50

#define	LABEL_RT		gettext("Res Type")
#define	LABEL_RT_P		gettext("Res Type param")
#define	LABEL_RG		gettext("Res Group")
#define	LABEL_RS		gettext("Res")
#define	LABEL_RS_P		gettext("Res property")

#define	VALUE_STR_NULL		"<NULL>"
#define	VALUE_STR_UNSET		"<unset>"
#define	VALUE_STR_ALL		"<All>"
#define	VALUE_STR_UNKNOWN	"<Unknown>"
#define	VALUE_STR_TRUE		"True"
#define	VALUE_STR_FALSE		"False"

#define	ZONENAME_MAX 64

static int exit_code;

static boolean_t iseuropa;	/* B_TRUE if farm mgmt. enabled */
static boolean_t firsttime = B_TRUE;

typedef enum scrgadm_reversion_enum {
	SCRGADM_REVERSION_UPFROM = 0,		/* upgrade from */
	SCRGADM_REVERSION_DOWNTO		/* downgrade to */
} scrgadm_reversion_enum_t;

typedef enum scrgadm_action_enum {
	SCRGADM_ACTION_UNDEF = 0,	/* undefined action */
	SCRGADM_ACTION_ADD,		/* add action */
	SCRGADM_ACTION_CHANGE,		/* change action */
	SCRGADM_ACTION_REMOVE,		/* remove action */
	SCRGADM_ACTION_PRINT		/* print action */
} scrgadm_action_t;

typedef enum scrgadm_type_enum {	/* may be XOR'd together */
	SCRGADM_TYPE_UNDEF = 0,		/* undefined type */
	SCRGADM_TYPE_RG = 1,		/* RG type */
	SCRGADM_TYPE_RT = 2,		/* RT type */
	SCRGADM_TYPE_RS = 4,		/* Resource type */
	SCRGADM_TYPE_LHRS = 8,		/* Logical Host RS */
	SCRGADM_TYPE_SARS = 16		/* Shared Address RS */
} scrgadm_type_t;

typedef enum scrgadm_print_enum {
	SCRGADM_PRINT_LOW = 0,		/* verbosity levels */
	SCRGADM_PRINT_MEDIUM
} scrgadm_print_t;

typedef enum scrgadm_flag_enum {
	SCRGADM_FLAG_ADD = 0,	/* reflections of command line	*/
	SCRGADM_FLAG_CHANGE,
	SCRGADM_FLAG_REMOVE,
	SCRGADM_FLAG_GROUP,
	SCRGADM_FLAG_RESOURCE,
	SCRGADM_FLAG_TYPE,
	SCRGADM_FLAG_RTFILE,
	SCRGADM_FLAG_NODE,	/* -h group */
	SCRGADM_FLAG_PROP,
	SCRGADM_FLAG_XPROP,
	SCRGADM_FLAG_LH,
	SCRGADM_FLAG_HN,	/* -l group */
	SCRGADM_FLAG_IPMP,	/* -n group */
	SCRGADM_FLAG_SA,
	SCRGADM_FLAG_AUX,
	SCRGADM_FLAG_MAX	/* must be last in this list! */
} scrgadm_flag_t;

typedef enum scrgadm_main_option {
	P_OPTION = 0,		/* print */
	A_OPTION,		/* add */
	C_OPTION,		/* change */
	R_OPTION,		/* remove */
	NO_OPTION		/* none of the above */
} scrgadm_main_option_t;

typedef char *scrgadm_raw_value_t;

/*
 *   from rgm/rgm_common.h:
 *
 * typedef char *name_t;
 *
 * typedef struct namelist {
 *	name_t			nl_name;
 *	struct namelist		*nl_next;
 * } namelist_t;
 */

/*
 *   from rgm_scrgadm.h:
 *
 * typedef struct scha_property {
 *	char		*sp_name;
 *	namelist_t	*sp_value;
 * } scha_property_t;
 */

static scrgadm_name_t rt_file = NULL;

/*
 *		Prototypes
 */

	/* Miscellaneous */
static scha_errmsg_t scrgadm_init(void);

static scconf_errno_t set_type(const scrgadm_action_t,
    const scrgadm_flag_t [], scrgadm_type_t *);
static scconf_errno_t validate_node(const scrgadm_action_t,
    const scrgadm_type_t);
static scconf_errno_t validate_prop(const scrgadm_action_t,
    const scrgadm_type_t);
static scconf_errno_t validate_xprop(const scrgadm_action_t,
    const scrgadm_type_t);

	/* Print utilities */
static scrgadm_main_option_t get_main_option(int, char **);
static scconf_errno_t do_print(int, char **);
static void print_line(int, int, char *, char *, char *);
static char *namelist_to_string(namelist_t *);
static char *scrgadm_nodeidlist_to_spacestring(nodeidlist_t *list);
static char *rdep_list_to_string(rdeplist_t *);


static void create_nodename_prefix(const char *, const char *, char *);

	/* Resource Type (RT) */
static void print_rt_all(const int, const scrgadm_print_t);
static void print_rt(const int, const scrgadm_print_t, char *);
static void show_rt_upglist(const int, char *, rgm_rt_upgrade_t *,
    scrgadm_reversion_enum_t);
static void show_rt_methods(const int, char *, rgm_methods_t);
static void show_paramtable(const int, char *, rgm_param_t **);
static void show_paramtable_entry(const int, char *, rgm_param_t *);

static scha_errmsg_t convert_lprops_to_rtsys_prop_node_name(
    const scrgadm_name_valuel_list_t *lprops,
    namelist_t **lnodes_p);
static scha_errmsg_t manage_rt(
    const scrgadm_action_t,
    const scrgadm_name_t,
    const scrgadm_name_valuel_list_t *,
    namelist_t *,
    RtrFileResult *,
    boolean_t);
static scha_errmsg_t create_rt(RtrFileResult *rtr_fres,
    namelist_t *lnodes, boolean_t bypass_install_mode);
static scha_errmsg_t modify_rt(const scrgadm_name_t, const namelist_t *);
static scha_errmsg_t remove_rt(const scrgadm_name_t);

	/* Resource Group (RG) */
static void print_rg_all(const int, const scrgadm_print_t);
static void print_rg(const int, const scrgadm_print_t, char *rg_name);
static scha_errmsg_t manage_rg(
    const scrgadm_action_t,
    const scrgadm_name_t,
    scrgadm_name_valuel_list_t *,
    namelist_t *,
    boolean_t, boolean_t);
static scha_errmsg_t create_rg(const scrgadm_name_t,
    scrgadm_name_valuel_list_t *, boolean_t);
static scha_errmsg_t modify_rg(const scrgadm_name_t,
    const scrgadm_name_valuel_list_t *);
static scha_errmsg_t remove_rg(const scrgadm_name_t, boolean_t v_flag);


	/* Resource (RS) */
static void print_rs_all(const int, const scrgadm_print_t, char *);
static void print_rs(const int, const scrgadm_print_t, char *);
static void show_rs_property_list(const int, char *, uint_t,
    rgm_property_list_t *, rgm_resource_t *);
static void show_rs_property(const int, char *, uint_t,
    rgm_property_t *, rgm_resource_t *);
static void show_rs_pernode_property(const int, int, char *, char *, value_t);

static scha_errmsg_t manage_rs(
    const scrgadm_action_t,
    const scrgadm_name_t,
    const scrgadm_name_t,
    const scrgadm_name_t,
    const scrgadm_name_valuel_list_t *,
    const scrgadm_name_valuel_list_t *,
    boolean_t, boolean_t);
static scha_errmsg_t create_rs(const scrgadm_name_t, const scrgadm_name_t,
    const scrgadm_name_t, const scrgadm_name_valuel_list_t *,
    const scrgadm_name_valuel_list_t *, boolean_t);
static scha_errmsg_t modify_rs(const scrgadm_name_t,
    const scrgadm_name_valuel_list_t *, const scrgadm_name_valuel_list_t *);
static scha_errmsg_t remove_rs(const scrgadm_name_t, boolean_t verbose_flag);

	/* Logical Host (LH) */
static scha_errmsg_t
manage_lh(
    const scrgadm_action_t,
    const scrgadm_name_t,
    const scrgadm_name_t,
    scrgadm_hostname_list_t *,
    scrgadm_node_ipmp_list_t *,
    scrgadm_name_valuel_list_t *,
    boolean_t);
static scha_errmsg_t set_lh_xprops(scrgadm_name_valuel_list_t **,
    const scrgadm_hostname_list_t *,
    const scrgadm_node_ipmp_list_t *);
static scha_errmsg_t create_lhrs(const scrgadm_name_t,
    const scrgadm_name_t,
    scrgadm_hostname_list_t *,
    scrgadm_node_ipmp_list_t *,
    scrgadm_name_valuel_list_t *,
    boolean_t);

	/* Shared Address (SA) */
static scha_errmsg_t manage_sa(
    const scrgadm_action_t,
    const scrgadm_name_t,
    const scrgadm_name_t,
    scrgadm_hostname_list_t *,
    scrgadm_node_ipmp_list_t *,
    namelist_t *,
    scrgadm_name_valuel_list_t *,
    boolean_t);
static scha_errmsg_t set_sa_xprops(scrgadm_name_valuel_list_t **,
    const scrgadm_hostname_list_t *,
    const scrgadm_node_ipmp_list_t *,
    const namelist_t *);
static scha_errmsg_t create_sars(const scrgadm_name_t,
    const scrgadm_name_t,
    scrgadm_hostname_list_t *,
    scrgadm_node_ipmp_list_t *,
    namelist_t *,
    scrgadm_name_valuel_list_t *,
    boolean_t);

	/* Utility and shared */

static scha_errmsg_t commalist_to_hostnamelist(char *,
    scrgadm_hostname_list_t **);
static scha_errmsg_t commalist_to_namelist(char *,
    namelist_t **);
static scha_errmsg_t commalist_to_node_ipmp_list(char *,
    scrgadm_node_ipmp_list_t **);
static char **create_name_array(const namelist_t *);
static scha_property_t *create_property_array(
    const scrgadm_name_valuel_list_t *);

static scrgadm_name_valuel_list_t *find_list_prop(char *,
    scrgadm_name_valuel_list_t **);
static int get_name_value_count(const scrgadm_name_valuel_list_t *);
static boolean_t is_asterisk_specified(const namelist_t *);
static scha_errmsg_t namelist_to_nvl_list(char *, namelist_t *,
    scrgadm_name_valuel_list_t **);


static scha_errmsg_t scrgadm_is_per_node_prop(char *, boolean_t, char *,
    boolean_t *);
static scha_errmsg_t parse_prop_name(char *, scrgadm_node_list_t **);
static void append_node_to_node_list(scrgadm_node_list_t *,
    scrgadm_node_list_t *);

static void remove_node_from_node_list(scrgadm_node_list_t **,
    scrgadm_node_list_t **);
static void remove_prop_from_name_valuel_list(
    scrgadm_name_valuel_list_t **, scrgadm_name_valuel_list_t **);
static scha_errmsg_t find_extn_prop_pernode_entries(
    scrgadm_name_valuel_list_t **,
    char *, scrgadm_value_list_t *,  scrgadm_node_list_t *);
static scha_errmsg_t parse_node_list(char *, scrgadm_node_list_t **);

static scha_errmsg_t parse_prop(char *,
    scrgadm_name_valuel_list_t **, boolean_t,
    boolean_t, char *);

static scrgadm_value_list_t *parse_raw_value(scrgadm_raw_value_t,
    boolean_t not_check_comma);
static scrgadm_value_list_t *create_empty_valuelist(void);

static void print_all(const int, const scrgadm_print_t);
static void print_error(const scconf_errno_t, char **);
static void print_usage(void);

static scha_errmsg_t process_namelist_to_names(namelist_t *);

static scha_errmsg_t set_props(scrgadm_name_valuel_list_t **,
    char *[], char *[]);
static scha_errmsg_t append_props(scrgadm_name_valuel_list_t **,
    scrgadm_name_valuel_list_t **);
	/* Be Free! */
static void free_nvl_limited(scrgadm_name_valuel_list_t *);
static void free_property_array(scha_property_t **);
static void free_name_valuel_list(scrgadm_name_valuel_list_t **);
static void free_namelist(namelist_t *);
static void free_hostname_list(scrgadm_hostname_list_t *);
static void free_node_ipmp_list(scrgadm_node_ipmp_list_t *);

/* end prototypes */

/*
 * Main function for scrgadm.
 *
 *	main()
 */

/*
 * Parse and validate options.
 * Execute the command.
 * Print error if any.
 *
 * Possible exit codes:
 *	0  success
 *	1  error occurred
 */
int
main(int argc, char ** argv)
{
	scrgadm_action_t action =	SCRGADM_ACTION_UNDEF;
	scrgadm_type_t type =		SCRGADM_TYPE_UNDEF;
	scconf_errno_t scconf_status =	SCCONF_NOERR;
	scha_errmsg_t scha_status =	{SCHA_ERR_NOERR, NULL};
	boolean_t bypass_install_mode = B_FALSE; /* private for upgrade */
#if SOL_VERSION >= __s10
	zoneid_t	zoneid;
	char		zonename[ZONENAME_MAX];
	int		retval;
	uint32_t	clid;
#endif

	char *nodelist = NULL;
	uint_t ismix;

	int c;
	scrgadm_name_t rt_name = NULL;
	scrgadm_name_t rg_name = NULL;
	scrgadm_name_t rs_name = NULL;

	namelist_t *lnodes_p = NULL;

	scrgadm_name_valuel_list_t *lprops_p = NULL;
	scrgadm_name_valuel_list_t *lxprops_p = NULL;

	scrgadm_hostname_list_t *lhnames_p = NULL;
	scrgadm_node_ipmp_list_t *lipmp_p = NULL;
	namelist_t *laux_p = NULL;
	/* cluster/farm nodetype */
	scconf_nodetype_t rg_nodetype = SCCONF_NODE_CLUSTER;

	char *rt_name_or_rs_name = NULL;
	/* can hold resource or resource type name. */
	boolean_t is_change_mode;
	boolean_t verbose_flag = B_FALSE;
	/*
	 * rtr_results holds parse of RT reg file
	 * will be filled by regfile processing
	 * and freed by create_rt()
	 */
	RtrFileResult *rtr_results = NULL;
	/* carry command line switch info */
	scrgadm_flag_t flags[SCRGADM_FLAG_MAX];
	/* flag to check whether we are inside a CZ or not */
	boolean_t zc_flag = B_FALSE;

	scrgadm_main_option_t main_opt = NO_OPTION;

	/* Initialize and demote to normal uid */
	cl_auth_init();
	cl_auth_demote();

	/* Initialize cl_cmd_event lib */
	exit_code = scha_status.err_code;
	cl_cmd_event_init(argc, argv, &exit_code);

	/*
	 * housekeeping
	 */

	for (c = 0; c < SCRGADM_FLAG_MAX; c++) {
	    flags[c] = 0;
	}

	(void) setlocale(LC_ALL, "");
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);
	(void) textdomain(TEXT_DOMAIN);

	main_opt = get_main_option(argc, argv);

	switch (main_opt) {
	case A_OPTION:
		cl_auth_check_command_opt_exit(*argv, "-a",
		    CL_AUTH_RESOURCE_MODIFY, SCHA_ERR_ACCESS);
		break;
	case C_OPTION:
		cl_auth_check_command_opt_exit(*argv, "-c",
		    CL_AUTH_RESOURCE_MODIFY, SCHA_ERR_ACCESS);
		break;
	case R_OPTION:
		cl_auth_check_command_opt_exit(*argv, "-r",
		    CL_AUTH_RESOURCE_MODIFY, SCHA_ERR_ACCESS);
		break;
	case P_OPTION:
		cl_auth_check_command_opt_exit(*argv, "-p",
		    CL_AUTH_RESOURCE_READ, SCHA_ERR_ACCESS);
		break;
	case NO_OPTION:
	default:
		break;
	}

	/* Check membership and init orb */
	scha_status = scrgadm_init();
	if (scha_status.err_code != SCHA_ERR_NOERR)
		goto exit_main;

#if SOL_VERSION >= __s10
	/* Check whether we are inside a ZC */
	scconf_status = scconf_zone_cluster_check(&zc_flag);
	if (scconf_status != SCCONF_NOERR) {
		goto exit_main;
	}

	if (zc_flag) {
		/* We are inside a CZ. So, disallow */
		scconf_status = SCCONF_ECZONE;
		goto exit_main;
	}
#endif

	/* If this is print mode, just print, then exit */
	if (main_opt == P_OPTION) {

		/* Print it */
		scconf_status = do_print(argc, argv);

		/* Done */
		goto exit_main;
	}

	/* parse command options */


	/*
	 * Possible error values set by parsing command line:
	 *
	 *	SCCONF_NOERR
	 *	SCCONF_EUSAGE
	 *	SCCONF_EPERM
	 *
	 *	SCHA_ERR_NOERR
	 *	SCHA_ERR_NOMEM
	 *	SCHA_ERR_INVAL
	 */

	while ((scha_status.err_code == SCHA_ERR_NOERR) &&
	    (scconf_status == SCCONF_NOERR) &&
	    ((c = getopt(argc, argv, "acrg:j:t:f:h:y:x:Ll:n:SX:F")) != -1)) {
		switch (c) {
		case 'a': /* add */
			if ((flags[SCRGADM_FLAG_ADD] != 0) ||
			    (action != SCRGADM_ACTION_UNDEF)) {
				scconf_status = SCCONF_EUSAGE;
			}
			flags[SCRGADM_FLAG_ADD]++;
			action = SCRGADM_ACTION_ADD;
			break;
		case 'c': /* change */
			if ((flags[SCRGADM_FLAG_CHANGE] != 0) ||
			    (action != SCRGADM_ACTION_UNDEF)) {
				scconf_status = SCCONF_EUSAGE;
			}
			flags[SCRGADM_FLAG_CHANGE]++;
			action = SCRGADM_ACTION_CHANGE;
			break;
		case 'r': /* remove */
			if ((flags[SCRGADM_FLAG_REMOVE] != 0) ||
			    (action != SCRGADM_ACTION_UNDEF)) {
				scconf_status = SCCONF_EUSAGE;
			}
			flags[SCRGADM_FLAG_REMOVE]++;
			action = SCRGADM_ACTION_REMOVE;
			break;
		case 'g': /* RG */
			if (flags[SCRGADM_FLAG_GROUP] != 0) {
				scconf_status = SCCONF_EUSAGE;
			}
			flags[SCRGADM_FLAG_GROUP]++;
			if ((optarg == NULL) || (*optarg == NULL)) {
				scconf_status = SCCONF_EUSAGE;
				goto exit_main;
			}
			rg_name = optarg;
			if (strchr(rg_name, ',') != NULL) {
				scconf_status = SCCONF_EUSAGE;
				goto exit_main;
			}
			break;
		case 'j': /* Resource (RS) */
			if (flags[SCRGADM_FLAG_RESOURCE] != 0) {
				scconf_status = SCCONF_EUSAGE;
			}
			flags[SCRGADM_FLAG_RESOURCE]++;
			if ((optarg == NULL) || (*optarg == NULL)) {
				scconf_status = SCCONF_EUSAGE;
				goto exit_main;
			}
			rs_name = optarg;
			/* no commas allowed in rs name */
			if (strchr(rs_name, ',') != NULL) {
				scconf_status = SCCONF_EUSAGE;
				goto exit_main;
			}
			break;
		case 't': /* RT */
			if (flags[SCRGADM_FLAG_TYPE] != 0) {
				scconf_status = SCCONF_EUSAGE;
			}
			flags[SCRGADM_FLAG_TYPE]++;
			if ((optarg == NULL) || (*optarg == NULL)) {
				scconf_status = SCCONF_EUSAGE;
				goto exit_main;
			}
			rt_name = optarg;
			break;
		case 'f': /* registration file (RT) */
			if (flags[SCRGADM_FLAG_RTFILE] != 0) {
				scconf_status = SCCONF_EUSAGE;
			}
			flags[SCRGADM_FLAG_RTFILE]++;
			if ((optarg == NULL) || (*optarg == NULL)) {
				scconf_status = SCCONF_EUSAGE;
				goto exit_main;
			}
			rt_file = optarg;
			break;
		case 'h': /* nodelist (RT, RG) */
			if (flags[SCRGADM_FLAG_NODE] != 0) {
				scconf_status = SCCONF_EUSAGE;
			}
			flags[SCRGADM_FLAG_NODE]++;
			if ((optarg == NULL) || (*optarg == NULL)) {
				scconf_status = SCCONF_EUSAGE;
				goto exit_main;
			}
			nodelist = strdup(optarg);
			if (nodelist == NULL) {
				scconf_status = SCCONF_ENOMEM;
				goto exit_main;
			}
			scha_status = commalist_to_namelist(optarg,
			    &lnodes_p);
			break;
		case 'y': /* standard properties (RG, RS, RT_System) */
			flags[SCRGADM_FLAG_PROP]++;
				/* add to props list */
			if ((optarg == NULL) || (*optarg == NULL)) {
				scconf_status = SCCONF_EUSAGE;
				goto exit_main;
			}

			scha_status = parse_prop(optarg, &lprops_p,
			    B_FALSE, NULL, NULL);
			if (scha_status.err_code == SCHA_ERR_INVAL) {
				scconf_status = SCCONF_EINVAL;
			}
			break;
		case 'x': /* extended properties list (RS) */
			flags[SCRGADM_FLAG_XPROP]++;
			/* add to xprops list */
			if ((optarg == NULL) || (*optarg == NULL)) {
				scconf_status = SCCONF_EUSAGE;
				goto exit_main;
			}
			if (flags[SCRGADM_FLAG_CHANGE] != 0) {
				if (rs_name != NULL) {
					rt_name_or_rs_name = strdup(rs_name);
					if (rt_name_or_rs_name == NULL) {
						scconf_status = SCCONF_ENOMEM;
						goto exit_main;
					}
				}
				is_change_mode = B_TRUE;
			} else {
				if (rt_name != NULL) {
					rt_name_or_rs_name = strdup(rt_name);
					if (rt_name_or_rs_name == NULL) {
						scconf_status = SCCONF_ENOMEM;
						goto exit_main;
					}
				}
				is_change_mode = B_FALSE;
			}
			scha_status = parse_prop(optarg, &lxprops_p, B_TRUE,
			    is_change_mode, rt_name_or_rs_name);
			if (scha_status.err_code == SCHA_ERR_INVAL) {
				scconf_status = SCCONF_EINVAL;
			}
			if (rt_name_or_rs_name)
				free(rt_name_or_rs_name);
			break;
		case 'L': /* Logical Host */
			if ((flags[SCRGADM_FLAG_LH] != 0) ||
			    (flags[SCRGADM_FLAG_SA] != 0)) {
				scconf_status = SCCONF_EUSAGE;
			}
			flags[SCRGADM_FLAG_LH]++;
			break;
		case 'l': /* hostnames/addresses for LH/SA */
			if (flags[SCRGADM_FLAG_HN] != 0) {
				scconf_status = SCCONF_EUSAGE;
			}
			flags[SCRGADM_FLAG_HN]++;
			if ((optarg == NULL) || (*optarg == NULL)) {
				scconf_status = SCCONF_EUSAGE;
				goto exit_main;
			}
			scha_status = commalist_to_hostnamelist(
			    optarg, &lhnames_p);
			break;
		case 'n': /* ipmp groups */
			if (flags[SCRGADM_FLAG_IPMP] != 0) {
				scconf_status = SCCONF_EUSAGE;
			}
			flags[SCRGADM_FLAG_IPMP]++;
			if ((optarg == NULL) || (*optarg == NULL)) {
				scconf_status = SCCONF_EUSAGE;
				goto exit_main;
			}
			scha_status = commalist_to_node_ipmp_list(optarg,
			    &lipmp_p);
			if (scha_status.err_code == SCHA_ERR_INVAL) {
				scconf_status = SCCONF_EINVAL;
			}
			break;
		case 'S': /* Shared Address */
			if ((flags[SCRGADM_FLAG_SA] != 0) ||
			    (flags[SCRGADM_FLAG_LH] != 0)) {
				scconf_status = SCCONF_EUSAGE;
			}
			flags[SCRGADM_FLAG_SA]++;
			break;
		case 'X': /* Auxillary nodes for Shared Address */
			if (flags[SCRGADM_FLAG_AUX] != 0) {
				scconf_status = SCCONF_EUSAGE;
			}
			flags[SCRGADM_FLAG_AUX]++;
			if ((optarg == NULL) || (*optarg == NULL)) {
				scconf_status = SCCONF_EUSAGE;
				goto exit_main;
			}
			scha_status = commalist_to_namelist(optarg,
			    &laux_p);
			break;
		case 'F':
			/*
			 * Force bypass installmode checks in librgm
			 * This is a "private" option for use by upgrade
			 * scripts performing "add" actions only
			 */
			bypass_install_mode = B_TRUE;
			break;
		default:
			scconf_status = SCCONF_EUSAGE;
			/* msg = gettext("Unknown flag on command line."); */
			break;
		} /* switch */
	} /* getopt */

	if ((scha_status.err_code != SCHA_ERR_NOERR) ||
	    (scconf_status != SCCONF_NOERR)) {
	    goto exit_main;
	}

	/*
	 * Mask signals for all operations other than print.
	 *
	 * Mask SIGINT, SIGQUIT when run scrgadm to prevent customer
	 * using combined key (ctrl-c, ctrl-\) to interrupt the action.
	 * Since the command will be exit afterward, we don't need
	 * to bother to release signals.
	 *
	 * XXX This is only a temporary workaround for bug 4280582.
	 */
	(void) sigignore(SIGINT);
	(void) sigignore(SIGQUIT);

	/*
	 * Possible error values returned by
	 * the following validation calls:
	 *	SCCONF_NOERR
	 *	SCCONF_EUSAGE
	 *	SCCONF_EPERM
	 */


	/* make sure we ate the entire command line */
	if (optind != argc) {
	    scconf_status = SCCONF_EUSAGE;
	    goto exit_main;
	}

	/* check for an action flag */
	if (flags[SCRGADM_FLAG_ADD] +
	    flags[SCRGADM_FLAG_CHANGE] +
	    flags[SCRGADM_FLAG_REMOVE] == 0) {
		scconf_status = SCCONF_EUSAGE;
		goto exit_main;
	}

	/* check and set type */
	scconf_status = set_type(action, flags, &type);
	if (scconf_status != SCCONF_NOERR) {
	    goto exit_main;
	}

	/* disallow incorrect use of "force" flag */
	if (bypass_install_mode == B_TRUE &&
	    flags[SCRGADM_FLAG_ADD] != 1) {
		scconf_status = SCCONF_EUSAGE;
		goto exit_main;
	}

	/* validate & process reg file */
	if ((type == SCRGADM_TYPE_RT) && (action == SCRGADM_ACTION_ADD)) {
		scha_status = scrgadm_process_rtreg(rt_name, rt_file,
		    &rtr_results, NULL);
		if (scha_status.err_code != SCHA_ERR_NOERR) {
			goto exit_main;
		} else if (scha_status.err_msg != NULL) {
			/*
			 * print warning message if set.
			 * The change in NRU tunablity might have generated
			 * warning message.
			 */
			print_scha_error(scha_status);
		}
	}

	/* check nodelist */
	if (flags[SCRGADM_FLAG_NODE] > 0) {
		if (flags[SCRGADM_FLAG_NODE] == 1) {
			scconf_status = validate_node(action, type);
		} else {
			scconf_status = SCCONF_EUSAGE;
		}

		if (scconf_status != SCCONF_NOERR) {
			goto exit_main;
		}

		/* If europa, check for server/farm nodes mix */
		if (iseuropa && type == SCRGADM_TYPE_RG) {
			scconf_status = scconf_isserverfarm_mix(
			    nodelist, &ismix);
			if (scconf_status != SCCONF_NOERR) {
				goto exit_main;
			} else {
				if (ismix) {
					(void) fprintf(stderr, "%s\n",
					    gettext("You cannot specify "
					    "a mix of server and farm "
					    "nodes in the nodelist."));
					scconf_status = SCCONF_EINVAL;
					goto exit_main;
				}
			}
		}
	}

	/* check auxnodelist */
	if (flags[SCRGADM_FLAG_AUX] > 0) {
		if (flags[SCRGADM_FLAG_SA] != 1) {
			scconf_status = SCCONF_EUSAGE;
		}
	}

	/* check prop */
	if (flags[SCRGADM_FLAG_PROP] > 0) {
	    scconf_status = validate_prop(action, type);
	    if (scconf_status != SCCONF_NOERR) {
		goto exit_main;
	    }
	}

	/* check xprop */
	if (flags[SCRGADM_FLAG_XPROP] > 0) {
	    scconf_status = validate_xprop(action, type);
	    if (scconf_status != SCCONF_NOERR) {
		goto exit_main;
	    }
	}

	/* check for farm RG Shared Address */
	if (iseuropa && (flags[SCRGADM_FLAG_SA] > 0) &&
	    (flags[SCRGADM_FLAG_GROUP] > 0)) {
		scha_status = get_rg_nodetype(rg_name, &rg_nodetype,
						NULL);
		if (scha_status.err_code != SCHA_ERR_NOERR) {
			goto exit_main;
		}

		if (rg_nodetype == SCCONF_NODE_FARM) {
			(void) fprintf(stderr, "%s\n",
			    gettext("You cannot specify"
			    "a Shared Address resource for"
			    "a farm resource group."));
			scconf_status = SCCONF_EINVAL;
			goto exit_main;
		}
	}

	/*
	 *		D I S P A T C H
	 *
	 *
	 * Add, remove or change configuration of
	 *	RG, RT, RS, LH, or SA.
	 *
	 * Return values are of type scha_errmsg_t
	 *	generated by librgm(d)
	 *	(refer to /usr/cluster/include/scha_err.h)
	 */

	scha_status.err_code = SCHA_ERR_NOERR;
	exit_code = scha_status.err_code;

	/* Generate event */
	(void) cl_cmd_event_gen_start_event();
	(void) atexit(cl_cmd_event_gen_end_event);

	switch (type) {
	case SCRGADM_TYPE_RG:
		scha_status = manage_rg(action, rg_name, lprops_p,
		    lnodes_p, bypass_install_mode, verbose_flag);
		break;
	case SCRGADM_TYPE_RT:
		scha_status = manage_rt(action, rt_name, lprops_p,
		    lnodes_p, rtr_results, bypass_install_mode);
		break;
	case SCRGADM_TYPE_RS:
		scha_status = manage_rs(action, rs_name, rt_name,
		    rg_name, lprops_p, lxprops_p, bypass_install_mode,
		    verbose_flag);
		break;
	case SCRGADM_TYPE_LHRS:
		/*
		 * create_lhrs is not allowed in a non global native
		 * zone but is allowed in a zone cluster zone
		 */
#if SOL_VERSION >= __s10
		if ((zoneid = getzoneid()) < 0) {
			scconf_status = SCCONF_ENGZONE;
			goto exit_main;
		}
		if (getzonenamebyid(zoneid, zonename,
		    sizeof (zonename)) < 0) {
			scconf_status = SCCONF_ENGZONE;
			goto exit_main;
		}
		if ((retval = clconf_get_cluster_id(zonename, &clid)) != 0) {
			(void) fprintf(stderr, gettext("Failed to get cluster "
			    "id, retval = %d. Exiting."), retval);
			scconf_status = SCCONF_ENGZONE;
			goto exit_main;
		}
		if ((clid < MIN_CLUSTER_ID) && (clid > 0)) {
			/* Native Zone. */
			scconf_status = SCCONF_ENGZONE;
			goto exit_main;
		}
#endif
		scha_status = manage_lh(action, rg_name, rs_name,
		    lhnames_p, lipmp_p, lprops_p, bypass_install_mode);
		break;
	case SCRGADM_TYPE_SARS:
		/*
		 * create_lhrs is not allowed in a non global native
		 * zone but is allowed in a zone cluster zone
		 */
#if SOL_VERSION >= __s10
		if ((zoneid = getzoneid()) < 0) {
			scconf_status = SCCONF_ENGZONE;
			goto exit_main;
		}
		if (getzonenamebyid(zoneid, zonename,
		    sizeof (zonename)) < 0) {
			scconf_status = SCCONF_ENGZONE;
			goto exit_main;
		}
		if ((retval = clconf_get_cluster_id(zonename, &clid)) != 0) {
			(void) fprintf(stderr, gettext("Failed to get cluster "
			    "id, retval = %d. Exiting."), retval);
			scconf_status = SCCONF_ENGZONE;
			goto exit_main;
		}
		if ((clid < MIN_CLUSTER_ID) && (clid > 0)) {
			/* Native Zone. */
			scconf_status = SCCONF_ENGZONE;
			goto exit_main;
		}
#endif
		scha_status = manage_sa(action, rg_name, rs_name,
		    lhnames_p, lipmp_p, laux_p, lprops_p,
		    bypass_install_mode);
		break;
	default:
		scha_status.err_code = SCHA_ERR_INVAL;
		break;
	} /* lint !e788 */


exit_main:
	/* free nodelist */
	if (nodelist) {
		free(nodelist);
	}

	/* dlclose scxcfg. and libfmm */
	if (iseuropa) {
		(void) scconf_libscxcfg_close();
		(void) scconf_libfmm_close();
	}

	/*
	 * scconf error codes have to do with usage or validations
	 * performed inside scrgadm.
	 * If such an error was encountered then calls to librgm
	 * never took place.
	 *
	 * Print scrgadm's error message and map the scconf code
	 * to a scha_err code for final return value.
	 */
	if (scconf_status != SCCONF_NOERR) {
		print_error(scconf_status, &scha_status.err_msg);

		/* convert scconf error codes into scha error codes */
		scha_status.err_code =
				scconf_err_to_scha_err(scconf_status).err_code;
	} else {
		/*
		 * scha_err codes may be returned either from calls to librgm
		 * or scrgadm validation failures. Some of these
		 * validations use scconf codes internally (calls to libscconf,
		 * for example) but such instances are mapped over
		 * to scha_err codes before return. See manage_lh | sa.
		 *
		 * If not NOERR let librgm print its own message.
		 */
		print_scha_error(scha_status);
	}

	/* replace warning error code by noerr error code */
	if (scha_status.err_code >= SCHA_ERR_RS_VALIDATE)
		scha_status.err_code = SCHA_ERR_NOERR;

	exit_code = scha_status.err_code;
	return (scha_status.err_code);

} /* ******    main   ****** */

/*
 *
 *	***** Miscellaneous functions  *****
 *
 */


/*
 * scrgadm_init
 *
 *	Check membership and initialize the orb.
 */
static scha_errmsg_t
scrgadm_init(void)
{
	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};
	scconf_errno_t scconf_status;
	uint_t ismember = 0;

	/* Promote to euid=0 */
	cl_auth_promote();

	/* Check cluster membership */
	scconf_status = scconf_ismember(0, &ismember);
	if (scconf_status != SCCONF_NOERR)
		return (scconf_err_to_scha_err(scconf_status));
	if (!ismember) {
		scha_status.err_code = SCHA_ERR_MEMBER;
		return (scha_status);
	}

	/* Initialize the orb */
	if (rgm_orbinit().err_code != SCHA_ERR_NOERR) {
		scha_status.err_code = SCHA_ERR_CCR;
		return (scha_status);
	}

	/* Check if farm management is enabled */
	scconf_status = scconf_iseuropa(&iseuropa);
	if (scconf_status != SCCONF_NOERR) {
		scha_status.err_code = SCHA_ERR_CCR;
		return (scha_status);
	}

	/* dlopen libscxcfg */
	if (iseuropa && firsttime) {
		scconf_status = scconf_libscxcfg_open();
		if (scconf_status != SCCONF_NOERR) {
			scha_status.err_code = SCHA_ERR_INTERNAL;
			return (scha_status);
		}
		scconf_status = scconf_libfmm_open();
		if (scconf_status != SCCONF_NOERR) {
			scha_status.err_code = SCHA_ERR_INTERNAL;
			return (scha_status);
		}
		firsttime = B_FALSE;
	}

	/* Return */
	return (scha_status);
}

/*
 *
 *	***** Validation functions  *****
 *
 */

/*
 * validate type options and set the type
 *
 * set_type()
 *
 * Input: action, flags, ptr to 'type'
 *
 * Action:
 *	Set 'type' this action is operating upon based on
 *	the action and the type options.
 *
 * Output: sets type
 *
 * Possible return values:
 *	SCCONF_NOERR
 *	SCCONF_EUSAGE
 *
 */
static scconf_errno_t
set_type(const scrgadm_action_t action,
    const scrgadm_flag_t flags[], scrgadm_type_t *type)
{
	scconf_errno_t status = SCCONF_NOERR;
	*type = SCRGADM_TYPE_UNDEF;

	switch (action)	{
	case SCRGADM_ACTION_ADD:
		/*
		 * only one type per transaction
		 * operating on RT requires -t; -f is optional
		 * operating on RT requires RType
		 * operating on RG requires RG name
		 * operating on RS requires RType and RGroup as params
		 */

		/* pick out add RT vs RG vs RS: may have only one */

		if ((flags[SCRGADM_FLAG_TYPE] == 1) &&
		    (flags[SCRGADM_FLAG_GROUP] == 0) &&
		    (flags[SCRGADM_FLAG_RESOURCE] == 0)) {
			if (*type != SCRGADM_TYPE_UNDEF) {
				status = SCCONF_EUSAGE;
				break;
			} else {
				*type = SCRGADM_TYPE_RT;
			}
		}

		if ((flags[SCRGADM_FLAG_TYPE] == 0) &&
		    (flags[SCRGADM_FLAG_RTFILE] == 0) &&
		    (flags[SCRGADM_FLAG_GROUP] == 1) &&
		    (flags[SCRGADM_FLAG_RESOURCE] == 0) &&
		    (flags[SCRGADM_FLAG_LH] == 0) &&
		    (flags[SCRGADM_FLAG_SA] == 0)) {
			if (*type != SCRGADM_TYPE_UNDEF) {
				status = SCCONF_EUSAGE;
				break;
			} else {
				*type = SCRGADM_TYPE_RG;
			}
		}

		/* add RS requires -t & -g as well */
		if ((flags[SCRGADM_FLAG_TYPE] == 1) &&
		    (flags[SCRGADM_FLAG_RTFILE] == 0) &&
		    (flags[SCRGADM_FLAG_GROUP] == 1) &&
		    (flags[SCRGADM_FLAG_RESOURCE] == 1)) {
			if (*type != SCRGADM_TYPE_UNDEF) {
				status = SCCONF_EUSAGE;
				break;
			} else {
				*type = SCRGADM_TYPE_RS;
			}
		}

		/*
		 * add LHRS:
		 *	hostname list required
		 *	RG name required
		 */
		if (flags[SCRGADM_FLAG_LH] != 0) {
			/* -t -f -h -X -x are invalid with -L */
			if (flags[SCRGADM_FLAG_TYPE] +
			    flags[SCRGADM_FLAG_RTFILE] +
			    flags[SCRGADM_FLAG_NODE] +
			    flags[SCRGADM_FLAG_AUX] +
			    flags[SCRGADM_FLAG_XPROP] > 0) {
				status = SCCONF_EUSAGE;
				break;
			}
			if ((*type != SCRGADM_TYPE_UNDEF) ||
			    flags[SCRGADM_FLAG_HN] == 0 ||
			    (flags[SCRGADM_FLAG_GROUP] == 0)) {
				status = SCCONF_EUSAGE;
				break;
			} else {
				*type = SCRGADM_TYPE_LHRS;
			}
		}

		/*
		 * add SARS:
		 *	hostname list required
		 *	RG name required
		 */

		if (flags[SCRGADM_FLAG_SA] != 0) {
			/* -t -f -h -x are invalid with -S */
			if (flags[SCRGADM_FLAG_TYPE] +
			    flags[SCRGADM_FLAG_RTFILE] +
			    flags[SCRGADM_FLAG_NODE] +
			    flags[SCRGADM_FLAG_XPROP] > 0) {
				status = SCCONF_EUSAGE;
				break;
			}
			if ((*type != SCRGADM_TYPE_UNDEF) ||
				flags[SCRGADM_FLAG_HN] == 0 ||
			    (flags[SCRGADM_FLAG_GROUP] == 0)) {
				status = SCCONF_EUSAGE;
				break;
			} else {
				*type = SCRGADM_TYPE_SARS;
			}
		}

		if (*type == SCRGADM_TYPE_UNDEF) {
			status = SCCONF_EUSAGE;
			break;
		}

		break;

	case SCRGADM_ACTION_CHANGE:
		/*
		 * change operates on a single RT or RG or RS at a time
		 * RT requires -h or -y
		 * can't change RType of an RS
		 * can't have RT reg file under change mode
		 * no change for LH or SA
		 */
		if ((flags[SCRGADM_FLAG_RTFILE] > 1) ||
		    (flags[SCRGADM_FLAG_LH] != 0) ||
		    (flags[SCRGADM_FLAG_SA] != 0)) {
			status = SCCONF_EUSAGE;
			break;
		}
		/*
		 * '-h' and/or '-y' flag required with '-c -t'
		 */
		if ((flags[SCRGADM_FLAG_TYPE] > 0) &&
		    ((flags[SCRGADM_FLAG_NODE] != 1) &&
		    (flags[SCRGADM_FLAG_PROP] != 1))) {
			status = SCCONF_EUSAGE;
			break;
		}
		if ((flags[SCRGADM_FLAG_TYPE] == 1) &&
		    (flags[SCRGADM_FLAG_GROUP] == 0) &&
		    (flags[SCRGADM_FLAG_RESOURCE] == 0)) {
			if (*type != SCRGADM_TYPE_UNDEF) {
				status = SCCONF_EUSAGE;
				break;
			} else {
				*type = SCRGADM_TYPE_RT;
			}
		}

		if ((flags[SCRGADM_FLAG_TYPE] == 0) &&
		    (flags[SCRGADM_FLAG_GROUP] == 1) &&
		    (flags[SCRGADM_FLAG_RESOURCE] == 0)) {
			if (*type != SCRGADM_TYPE_UNDEF) {
				status = SCCONF_EUSAGE;
				break;
			} else {
				*type = SCRGADM_TYPE_RG;
			}
		}

		if ((flags[SCRGADM_FLAG_TYPE] == 0) &&
		    (flags[SCRGADM_FLAG_GROUP] == 0) &&
		    (flags[SCRGADM_FLAG_RESOURCE] == 1)) {
			if (*type != SCRGADM_TYPE_UNDEF) {
				status = SCCONF_EUSAGE;
				break;
			} else {
				*type = SCRGADM_TYPE_RS;
			}
		}

		if (*type == SCRGADM_TYPE_UNDEF) {
			status = SCCONF_EUSAGE;
			break;
		}

		break;

	case SCRGADM_ACTION_REMOVE:
		/*
		 * remove operates on a single RT or RG or RS
		 * No special remove for LH/SA
		 */
		if (flags[SCRGADM_FLAG_RTFILE] > 1) {
			status = SCCONF_EUSAGE;
			break;
		}
		if ((flags[SCRGADM_FLAG_TYPE] == 1) &&
		    (flags[SCRGADM_FLAG_GROUP] == 0) &&
		    (flags[SCRGADM_FLAG_RESOURCE] == 0)) {
			if (*type != SCRGADM_TYPE_UNDEF) {
				status = SCCONF_EUSAGE;
				break;
			} else {
				*type = SCRGADM_TYPE_RT;
			}
		}

		if ((flags[SCRGADM_FLAG_TYPE] == 0) &&
		    (flags[SCRGADM_FLAG_GROUP] == 1) &&
		    (flags[SCRGADM_FLAG_RESOURCE] == 0) &&
		    (flags[SCRGADM_FLAG_LH] == 0) &&
		    (flags[SCRGADM_FLAG_SA] == 0)) {
			if (*type != SCRGADM_TYPE_UNDEF) {
				status = SCCONF_EUSAGE;
				break;
			} else {
				*type = SCRGADM_TYPE_RG;
			}
		}

		if ((flags[SCRGADM_FLAG_TYPE] == 0) &&
		    (flags[SCRGADM_FLAG_GROUP] == 0) &&
		    (flags[SCRGADM_FLAG_RESOURCE] == 1)) {
			if (*type != SCRGADM_TYPE_UNDEF) {
				status = SCCONF_EUSAGE;
				break;
			} else {
				*type = SCRGADM_TYPE_RS;
			}
		}


		if (*type == SCRGADM_TYPE_UNDEF) {
			status = SCCONF_EUSAGE;
			break;
		}

		break;

	case SCRGADM_ACTION_PRINT:
		/*
		 * option to print a single "type" (RT or RG or RS)
		 * NYI !! (Not Yet Implemented)  XXXXX
		 */
		if ((flags[SCRGADM_FLAG_TYPE] +
		    flags[SCRGADM_FLAG_GROUP] +
		    flags[SCRGADM_FLAG_RESOURCE]) > 0) {
			status = SCCONF_EUSAGE;
		}
		break;

	default:
		/*
		 * impossible case
		 */
		status = SCCONF_EUNEXPECTED;
		break;
	}
	return (status);
} /* set_type */


/*
 * validate_node()
 *
 * Validate presence of NodeList.
 *
 * Possible return values:
 *	SCCONF_NOERR
 *	SCCONF_EUSAGE
 *
 */
static scconf_errno_t
validate_node(const scrgadm_action_t action,
    const scrgadm_type_t type)
{
	scconf_errno_t status = SCCONF_NOERR;

	/*
	 * node option is legal with -a or -c option for RT, RG.
	 */
	if (((action == SCRGADM_ACTION_ADD) ||
	    (action == SCRGADM_ACTION_CHANGE)) &&
	    ((type == SCRGADM_TYPE_RT) ||
	    (type == SCRGADM_TYPE_RG)))
		return (status);
	else
		status = SCCONF_EUSAGE;

	return (status);
} /* validate_node */


/*
 * validate_prop()
 *
 * Validate that is legal to have property list
 *
 * Possible return values:
 *	SCCONF_NOERR
 *	SCCONF_EUSAGE
 *
 */
static scconf_errno_t
validate_prop(const scrgadm_action_t action,
    const scrgadm_type_t type)
{
	scconf_errno_t status = SCCONF_NOERR;

	if (((action == SCRGADM_ACTION_ADD) ||
	    (action == SCRGADM_ACTION_CHANGE)) &&
	    ((type == SCRGADM_TYPE_RG) ||
	    (type == SCRGADM_TYPE_RT) ||
	    (type == SCRGADM_TYPE_RS)))
		return (status);
	else {
		if ((action == SCRGADM_ACTION_ADD) &&
		    ((type == SCRGADM_TYPE_LHRS) ||
		    (type == SCRGADM_TYPE_SARS)))
			return (status);
		status = SCCONF_EUSAGE;
	}

	return (status);
} /* validate_prop */


/*
 * validate_xprop()
 *
 * Extended property list is legal only for Resource.
 *
 * Possible return values:
 *	SCCONF_NOERR
 *	SCCONF_EUSAGE
 *
 */
static scconf_errno_t
validate_xprop(const scrgadm_action_t action,
    const scrgadm_type_t type)
{
	scconf_errno_t status = SCCONF_NOERR;

	if (((action == SCRGADM_ACTION_ADD) ||
	    (action == SCRGADM_ACTION_CHANGE)) &&
	    (type == SCRGADM_TYPE_RS))
		return (status);
	else
		status = SCCONF_EUSAGE;

	return (status);
} /* validate_xprop */

/*
 *
 *	*****  Print utility functions  *****
 *
 */

/*
 * get_main_option
 *
 *	Return P_OPTION if the print option is set.
 *	Return A_OPTION/C_OPTION/R_OPTION if -a/-c/-r is specified.
 *	Return NO_OPTION if -p/-a/-c/-r is not specified.
 */
static scrgadm_main_option_t
get_main_option(int argc, char **argv)
{
	int save_opterr = opterr;
	scrgadm_main_option_t main_opt = NO_OPTION;
	int c;

	/* Ignore errors */
	opterr = 0;

	/* Make sure optind is set */
	optind = 1;

	/* Just look for -p/-a/-c/-r, ignoring others */
	while ((c = getopt(argc, argv, "pacr")) != EOF) {
		switch (c) {
		case 'p':
			main_opt = P_OPTION;
			break;
		case 'a':
			main_opt = A_OPTION;
			break;
		case 'c':
			main_opt = C_OPTION;
			break;
		case 'r':
			main_opt = R_OPTION;
			break;

		default:
			break;
		}
	}

	/* Reset optind */
	optind = 1;

	/* Restore opterr */
	opterr = save_opterr;

	/* Return */
	return (main_opt);
}

/*
 * do_print
 *
 *	This function should be called if the -p option is known to be
 *	set.  It may return a usage error, if incompatible options are
 *	also set.
 *
 *	If -t, -g, or -j are given, only those resource types, resource
 *	groups, and resources will be printed.   Multiple options may
 *	be given (e.g., "-t <type1> -t <type2> -g <group1> -g <group2>").
 *	If the argument is PRINTALL_ARG, print all.
 */
static scconf_errno_t
do_print(int argc, char **argv)
{
	int save_opterr = opterr;
	scconf_errno_t rstatus = SCCONF_NOERR;
	scha_errmsg_t rgm_status = {SCHA_ERR_NOERR, NULL};
	char *rt_namebuff;
	int pflg, vflg, tflg, gflg, jflg;
	int c;

	/* Initialize flags */
	pflg = vflg = tflg = gflg = jflg = 0;

	/* Ignore errors */
	opterr = 0;

	/* Make sure optind is set */
	optind = 1;

	/* Just check usage and specific flags */
	while ((c = getopt(argc, argv, "pvt:g:j:")) != EOF) {
		switch (c) {
		case 'p':
			++pflg;
			break;

		case 'v':
			++vflg;
			break;

		case 't':
			++tflg;
			break;

		case 'g':
			++gflg;
			break;

		case 'j':
			++jflg;
			break;

		default:
			rstatus = SCCONF_EUSAGE;
			break;
		}
	}
	if (rstatus != SCCONF_NOERR)
		goto cleanup;

	/* We at expect -p to be set before this function is called */
	if (!pflg) {
		rstatus = SCCONF_EUNEXPECTED;
		goto cleanup;
	}

	/* If individual -t, -g, and -j flags are not set, print everything */
	if (tflg + gflg + jflg == 0) {
		print_all(0, vflg);
		goto cleanup;
	}

	/* Otherwise, print each as it occurs */
	optind = 1;
	while ((c = getopt(argc, argv, "pvt:g:j:")) != EOF) {
		switch (c) {
		case 't':
			if (optarg && strcmp(PRINTALL_ARG, optarg) == 0) {
				print_rt_all(0, vflg);
			} else {
				rgm_status = rgmcnfg_get_rtrealname(optarg,
				    &rt_namebuff, NULL);

				if (rgm_status.err_code != SCHA_ERR_NOERR) {
					print_scha_error(rgm_status);
					return (SCCONF_ENOEXIST);
				}

				print_rt(0, vflg, rt_namebuff);
				free(rt_namebuff);
			}
			break;

		case 'g':
			if (optarg && strcmp(PRINTALL_ARG, optarg) == 0) {
				print_rg_all(0, vflg);
			} else {
				print_rg(0, vflg, optarg);
			}
			break;

		case 'j':
			if (optarg && strcmp(PRINTALL_ARG, optarg) == 0) {
				print_rs_all(0, vflg, NULL);
			} else {
				print_rs(0, vflg, optarg);
			}
			break;

		case 'p':
		case 'v':
			break;

		default:
			rstatus = SCCONF_EUNEXPECTED;
			goto cleanup;
		}
	}

cleanup:
	/* Blank line */
	(void) putchar('\n');

	/* Restore optind */
	optind = 1;

	/* Restore opterr */
	opterr = save_opterr;

	return (rstatus);
}

/*
 * print_line
 *
 *      Print a line of configuration output.
 *
 *      If no prefix or prefix is NULL string, do not print prefix.
 *      If no value, do not print value or newline.
 */
static void
print_line(int margin, int labelw, char *prefix, char *label, char *value)
{
	int i;
	char labelbuff[BUFSIZ];

	/* check args */
	if (margin < 0 || labelw < margin || label == NULL || *label == '\0')
		return;

	/* Check for NULL strings */
	if (prefix && *prefix == '\0')
		prefix = NULL;
	if (value && *value == '\0')
		value = NULL;

	/* set up labelbuff, with or without prefix */
	if (prefix) {
		(void) sprintf(labelbuff, "%s %s", prefix, label);
	} else {
		(void) strcpy(labelbuff, label);
	}

	/* print margin */
	for (i = 0;  i < margin;  ++i)
		(void) putchar(' ');

	/* print label */
	(void) printf("%-*s ", labelw - margin, labelbuff);

	/* print value */
	if (value)
		(void) printf("%s\n", value);
	else
		(void) putchar('\n');
}


/*
 * get_fom_name_list
 *
 * return a single string with all the possible values for
 * Failover_mode
 * Since the introduction of the RESTART_ONLY and LOG_ONLY values
 * for Failover_mode, the enumlist is no longer used
 */
static char *
get_fom_name_list(void)
{
	int	n, i;
	size_t	len = 0;
	char	*str;
	char	*fom_values[] = { SCHA_NONE, SCHA_HARD, SCHA_SOFT,
	    SCHA_RESTART_ONLY, SCHA_LOG_ONLY };

	n = sizeof (fom_values) / sizeof (char *);

	for (i = 0; i < n; i++) {
		len += strlen(fom_values[i]) + 1;
	}

	/* allocate the buffer. calloc() zeroes everything */
	if ((str = (char *)calloc(1, len + 1)) == NULL) {
		return (NULL);
	}

	for (i = 0; i < n; i++) {
		/* add space sepator, except for the first iteration */
		if (*str != '\0') {
			(void) strcat(str, " ");
		}

		(void) strcat(str, fom_values[i]);
	}

	return (str);
}


/*
 * namelist_to_string
 *
 *	Return a single string for the list of names
 *	The caller is responsible for freeing the returned list.
 */
static char *
namelist_to_string(namelist_t *list)
{
	size_t len = 0;
	namelist_t *nl;
	char *string;

	/* Get the length required for the string buffer */
	for (nl = list;  nl;  nl = nl->nl_next) {
		if (nl->nl_name && *nl->nl_name) {
			len += strlen(nl->nl_name) + 1;
		} else {
			len += strlen(VALUE_STR_NULL) + 1;
		}
	}
	if (len == 0)
		return ((char *)0);

	/* Allocate the string buffer */
	if ((string = (char *)calloc(1, len + 1)) == NULL)
		return ((char *)0);

	/* Add values to the string buffer */
	for (nl = list;  nl;  nl = nl->nl_next) {

		/* trailing space */
		if (*string != '\0')
			(void) strcat(string, " ");

		/* name (or NULL) */
		if (nl->nl_name && *nl->nl_name) {
			(void) strcat(string, nl->nl_name);
		} else {
			(void) strcat(string, VALUE_STR_NULL);
		}
	}

	return (string);
}

static char *
scrgadm_nodeidzone_to_nodename(uint_t nodeid, char *zonename) {
	char *nodename = NULL;
	char *tmp = NULL;
	scconf_errno_t scconferr;

	scconferr = scconf_get_nodename(nodeid, &nodename);
	if (scconferr != SCCONF_NOERR) {
		if (scconferr == SCCONF_ENOEXIST) {
			scconferr = scconf_get_farmnodename(nodeid, &nodename);
		}
		if (scconferr != SCCONF_NOERR) {
			free(nodename);
			return (NULL);
		}
	}

	if (zonename == NULL) {
		return (nodename);
	}
	tmp = realloc(nodename, strlen(nodename) + strlen(zonename) + 2);
	if (tmp == NULL) {
		/* nodename is initialized to NULL so suppress lint warning */
		free(nodename); /* lint !e644 */
		return (NULL);
	}
	nodename = tmp;
	(void) strcat(nodename,  ":");
	(void) strcat(nodename, zonename);
	return (nodename);
}

/*
 * scrgadm_nodeidlist_to_spacestring
 *
 *	Return a single string for the list of nodeids
 *	The caller is responsible for freeing the returned list.
 */
static char *
scrgadm_nodeidlist_to_spacestring(nodeidlist_t *list)
{
	size_t size = 0;
	nodeidlist_t *nl;
	char *ptr = NULL;
	char *tmp; /* = NULL; */
	char *nodename = NULL;

	if (list == NULL) {
		return (strdup(""));
		/* check is this is the behaviour we want in clients */
	}

	for (nl = list;  nl;  nl = nl->nl_next) {

		nodename = scrgadm_nodeidzone_to_nodename(nl->nl_nodeid,
		    nl->nl_zonename);
		if (nodename == NULL) {
			/* should never occur */
			size += strlen(VALUE_STR_NULL) + 1;
		} else {
			size += strlen(nodename) + 1;
		}
		tmp = realloc(ptr, size);
		if (NULL == tmp) {
			/* ptr initialized to NULL so suppress lint warning */
			free(ptr); /* lint !e644 */
			return (NULL);
		}
		ptr = tmp;


		/* initilaize string to 0 for strcat. */
		if (nl == list) {
			/* so that strcat behaves like strcpy for first time */
			ptr[0] = '\0';
		} else {
			/* trailing space */
			(void) strcat(ptr, " ");
		}

		if (nodename == NULL) {
			(void) strcat(ptr, VALUE_STR_NULL);
		} else {
			(void) strcat(ptr, nodename);
		}


		free(nodename);
	}
	return (ptr);
}

/*
 * rdep_list_to_string
 *
 *      Return a single string for the list of resource dependency names.
 *      The caller is responsible for freeing the list of names.
 */
static char *
rdep_list_to_string(rdeplist_t *r_dependencies)
{
	rdeplist_t *r_dep;
	size_t len = 0;
	char *string;
	const char *local_node = gettext("{local_node}");
	const char *any_node = gettext("{any_node}");

	/* Get the length required for the string buffer. */
	for (r_dep = r_dependencies; r_dep; r_dep = r_dep->rl_next) {
		if (r_dep->rl_name && *r_dep->rl_name) {
			len += strlen(r_dep->rl_name) + 1;
		} else {
			len += sizeof (VALUE_STR_NULL) + 1;
		}

		switch (r_dep->locality_type) {
		case LOCAL_NODE:
			len += strlen(local_node);
			break;
		case ANY_NODE:
			len += strlen(any_node);
			break;
		case FROM_RG_AFFINITIES:
		default:
			break;
		}
	}

	/* Allocate the string buffer */
	if ((string = (char *)calloc(1, len)) == NULL) {
		return ((char *)0);
	}

	/*
	 * Add values to the string buffer. The string buffer contains the
	 * list of suffixed qualifier with the dependencies name.
	 */
	for (r_dep = r_dependencies; r_dep; r_dep = r_dep->rl_next) {
		/* trailing space */
		if (*string != '\0')
			(void) strcat(string, " ");

		/* name (or NULL) */
		if (r_dep->rl_name && *r_dep->rl_name) {
			(void) strcat(string, r_dep->rl_name);
		} else {
			(void) strcat(string, VALUE_STR_NULL);
		}

		switch (r_dep->locality_type) {
		case LOCAL_NODE:
			(void) strcat(string, local_node);
			break;
		case ANY_NODE:
			(void) strcat(string, any_node);
			break;
		case FROM_RG_AFFINITIES:
		default:
			break;
		}
	}

	return (string);
}

/*
 *
 *	*****  Resource Type (RT) functions  *****
 *
 */

/*
 * print_rt_all()
 *
 * Print current RT configuration.
 *
 * No error
 */
static void
print_rt_all(const int margin, const scrgadm_print_t print_level)
{
	scha_errmsg_t rgm_status = {SCHA_ERR_NOERR, NULL};
	char **rt_names = (char **)0;
	char **rt_namep;

	/* Get the list of all rt names */
	rgm_status = rgm_scrgadm_getrtlist(&rt_names, NULL);
	if (rgm_status.err_code != SCHA_ERR_NOERR) {
		print_scha_error(rgm_status);
		return;
	}
	if (rt_names == NULL)
		return;

	/* print config for each rt */
	for (rt_namep = rt_names;  rt_namep && *rt_namep;  ++rt_namep)
		print_rt(margin, print_level, *rt_namep);

	/* Free the list of rt names */
	rgm_free_strarray(rt_names);
}

/*
 * print_rt()
 *
 * Display RT
 *
 * No error
 *
 */
static void
print_rt(const int margin, const scrgadm_print_t print_level, char *rt_name)
{
	scha_errmsg_t rgm_status = {SCHA_ERR_NOERR, NULL};
	rgm_rt_t *rt = (rgm_rt_t *)0;
	char *ptr, *ptr2;
	char label[BUFSIZ];
	char prefix[BUFSIZ];
	char value[BUFSIZ];
	int labelw;

	/* Check arguments */
	if (rt_name == NULL)
		return;

	/* Get configuration for this rt name */
	rgm_status = rgm_scrgadm_getrtconf(rt_name, &rt, NULL);
	if (rgm_status.err_code != SCHA_ERR_NOERR) {
		print_scha_error(rgm_status);
		return;
	}
	if (rt == NULL)
		return;

	/* Set label width */
	labelw = (print_level > SCRGADM_PRINT_LOW) ? LABELW2 : LABELW1;

	/* Blank line */
	(void) putchar('\n');

	/* Resource type name */
	(void) sprintf(label, "%s %s:", LABEL_RT, gettext("name"));
	ptr = (rt->rt_name && *rt->rt_name) ? rt->rt_name : VALUE_STR_NULL;
	print_line(margin, labelw, NULL, label, ptr);

	/* Set prefix */
	*prefix = '\0';
	if (print_level > SCRGADM_PRINT_LOW) {
		if (rt->rt_name) {
			(void) sprintf(prefix, "(%s)", rt->rt_name);
		}
	}

	/* Resource type description */
	(void) sprintf(label, "%s %s:", LABEL_RT, gettext("description"));
	ptr = (rt->rt_description && *rt->rt_description)
	    ? rt->rt_description : VALUE_STR_NULL;
	print_line(margin + 2, labelw, prefix, label, ptr);

	/* Verbose */
	if (print_level > SCRGADM_PRINT_LOW) {

		/* Resource type base directory */
		(void) sprintf(label, "%s %s:", LABEL_RT,
		    gettext("base directory"));
		ptr = (rt->rt_basedir && rt->rt_basedir)
		    ? rt->rt_basedir : VALUE_STR_NULL;
		print_line(margin + 2, labelw, prefix, label, ptr);

		/* Resource type single instance */
		(void) sprintf(label, "%s %s:", LABEL_RT,
		    gettext("single instance"));
		ptr = (rt->rt_single_inst == B_FALSE)
		    ? VALUE_STR_FALSE : VALUE_STR_TRUE;
		print_line(margin + 2, labelw, prefix, label, ptr);

		/* Resource type init nodes */
		(void) sprintf(label, "%s %s:", LABEL_RT,
		    gettext("init nodes"));
		switch (rt->rt_init_nodes) {
		case SCHA_INFLAG_RG_PRIMARIES:
			ptr = "All potential masters";
			break;

		case SCHA_INFLAG_RT_INSTALLED_NODES:
			ptr = "All nodes";
			break;

		default:
			ptr = VALUE_STR_UNKNOWN;
			break;
		}
		print_line(margin + 2, labelw, prefix, label, ptr);

		/* Resource type failover */
		(void) sprintf(label, "%s %s:", LABEL_RT, gettext("failover"));
		ptr = (rt->rt_failover == B_FALSE)
		    ? VALUE_STR_FALSE : VALUE_STR_TRUE;
		print_line(margin + 2, labelw, prefix, label, ptr);

		/* Resource type proxy */
		(void) sprintf(label, "%s %s:", LABEL_RT, gettext("proxy"));
		ptr = (rt->rt_proxy == B_FALSE)
		    ? VALUE_STR_FALSE : VALUE_STR_TRUE;
		print_line(margin + 2, labelw, prefix, label, ptr);

		/* Resource type version */
		(void) sprintf(label, "%s %s:", LABEL_RT, gettext("version"));
		ptr = (rt->rt_version && *rt->rt_version)
		    ? rt->rt_version : VALUE_STR_NULL;
		print_line(margin + 2, labelw, prefix, label, ptr);

		/* Resource type API version */
		(void) sprintf(label, "%s %s:", LABEL_RT,
		    gettext("API version"));
		(void) sprintf(value, "%d", rt->rt_api_version);
		print_line(margin + 2, labelw, prefix, label, value);

		/* Resource type installed on nodes */
		(void) sprintf(label, "%s %s:", LABEL_RT,
		    gettext("installed on nodes"));
		ptr2 = NULL;
		if (rt->rt_instl_nodes.is_ALL_value) {
			ptr = VALUE_STR_ALL;
		} else {
			ptr2 = scrgadm_nodeidlist_to_spacestring(
			    rt->rt_instl_nodes.nodeids);
			ptr = (ptr2) ? ptr2 : VALUE_STR_NULL;
		}
		print_line(margin + 2, labelw, prefix, label, ptr);
		if (ptr2)
			free(ptr2);

		/* Resource type packages */
		(void) sprintf(label, "%s %s:", LABEL_RT, gettext("packages"));
		ptr2 = NULL;
		if (rt->rt_pkglist) {
			ptr2 = namelist_to_string(rt->rt_pkglist);
			ptr = (ptr2) ? ptr2 : VALUE_STR_NULL;
		} else {
			ptr = VALUE_STR_NULL;
		}
		print_line(margin + 2, labelw, prefix, label, ptr);
		if (ptr2)
			free(ptr2);

		/*
		 * RT_System
		 */
		(void) sprintf(label, "%s %s:", LABEL_RT, gettext("system"));
		ptr = (rt->rt_system == B_TRUE)
		    ? VALUE_STR_TRUE : VALUE_STR_FALSE;
		print_line(margin + 2, labelw, prefix, label, ptr);

		/* Very verbose */
		if (print_level > SCRGADM_PRINT_MEDIUM) {

			if (!rt->rt_sc30 &&
			    (rt->rt_upgrade_from || rt->rt_downgrade_to)) {

				/* Header for resource type upgrade list */
				(void) sprintf(label, "%s %s", LABEL_RT,
				    gettext("upgrade tunability"));
				print_line(margin + 2, labelw, prefix, label,
				    NULL);

				if (rt->rt_upgrade_from) {
					show_rt_upglist(margin + 2, rt->rt_name,
					    rt->rt_upgrade_from,
					    SCRGADM_REVERSION_UPFROM);
				}

				if (rt->rt_downgrade_to) {
					show_rt_upglist(margin + 2, rt->rt_name,
					    rt->rt_downgrade_to,
					    SCRGADM_REVERSION_DOWNTO);
				}
			}

			/* Resource type methods */
			show_rt_methods(margin + 2, rt->rt_name,
			    rt->rt_methods);

			/* Paramtable */
			if (rt->rt_paramtable && *rt->rt_paramtable) {
				show_paramtable(margin + 2, rt->rt_name,
				    rt->rt_paramtable);
			}
		} /* if (print_level > SCRGADM_PRINT_MEDIUM) */
	} /* if (print_level > SCRGADM_PRINT_LOW) */

	/* free rt */
	rgm_free_rt(rt);
}

/*
 * show_rt_upglist()
 *
 * Print the tunability for rt upgrade
 *
 * No return value.
 */
static void
show_rt_upglist(const int margin, char *rt_name, rgm_rt_upgrade_t *upglist,
    scrgadm_reversion_enum_t reversion)
{
	rgm_rt_upgrade_t *upgp;
	char label[BUFSIZ];
	char prefix[BUFSIZ];
	int labelw;
	char *ptr;
	char *version;

	/* Check arguments */
	if (rt_name == NULL || upglist == NULL)
		return;

	/* Set label width */
	labelw = LABELW2;

	/* Set prefix */
	(void) sprintf(prefix, "(%s)", rt_name);

	for (upgp = upglist; upgp; upgp = upgp->rtu_next) {
		if (strlen(upgp->rtu_version) == 0) {
			version = "<unset_version>";
		} else {
			version = upgp->rtu_version;
		}
		if (reversion == SCRGADM_REVERSION_UPFROM) {
			(void) sprintf(label, "%s %s %s:", LABEL_RT,
			    gettext("upgrade from"), version);
		} else {
			(void) sprintf(label, "%s %s %s:", LABEL_RT,
			    gettext("downgrade to"), version);
		}

		switch (upgp->rtu_tunability) {
		case TUNE_ANYTIME:
			ptr = "Anytime";
			break;

		case TUNE_AT_CREATION:
			ptr = "At creation";
			break;

		case TUNE_WHEN_DISABLED:
			ptr = "When disabled";
			break;

		case TUNE_WHEN_OFFLINE:
			ptr = "When offline";
			break;

		case TUNE_WHEN_UNMANAGED:
			ptr = "When unmanaged";
			break;

		case TUNE_WHEN_UNMONITORED:
			ptr = "When unmonitored";
			break;

		case TUNE_NONE:
		default:
			ptr = VALUE_STR_UNKNOWN;
			break;
		}
		print_line(margin + 2, labelw, prefix, label, ptr);
	}
}

/*
 * show_rt_methods()
 *
 * Print names of methods registered with RT.
 *
 * No return value.
 */
static void
show_rt_methods(const int margin, char *rt_name, rgm_methods_t m)
{
	char label[BUFSIZ];
	char prefix[BUFSIZ];
	int labelw;

	/* Check arguments */
	if (rt_name == NULL)
		return;

	/* Set label width */
	labelw = LABELW2;

	/* Set prefix */
	*prefix = '\0';
	if (rt_name)
		(void) sprintf(prefix, "(%s)", rt_name);

	/* Header for resource type methods */
	(void) sprintf(label, "%s %s", LABEL_RT, gettext("methods"));
	print_line(margin, labelw, prefix, label, NULL);

	/* Start method */
	if (m.m_start && *m.m_start) {
		(void) sprintf(label, "%s %s:", LABEL_RT, "START");
		print_line(margin + 2, labelw, prefix, label, m.m_start);
	}

	/* Stop method */
	if (m.m_stop && *m.m_stop) {
		(void) sprintf(label, "%s %s:", LABEL_RT, "STOP");
		print_line(margin + 2, labelw, prefix, label, m.m_stop);
	}

	/* Validate method */
	if (m.m_validate && *m.m_validate) {
		(void) sprintf(label, "%s %s:", LABEL_RT, "VALIDATE");
		print_line(margin + 2, labelw, prefix, label, m.m_validate);
	}

	/* Update method */
	if (m.m_update && *m.m_update) {
		(void) sprintf(label, "%s %s:", LABEL_RT, "UPDATE");
		print_line(margin + 2, labelw, prefix, label, m.m_update);
	}

	/* Init method */
	if (m.m_init && *m.m_init) {
		(void) sprintf(label, "%s %s:", LABEL_RT, "INIT");
		print_line(margin + 2, labelw, prefix, label, m.m_init);
	}

	/* Fini method */
	if (m.m_fini && *m.m_fini) {
		(void) sprintf(label, "%s %s:", LABEL_RT, "FINI");
		print_line(margin + 2, labelw, prefix, label, m.m_fini);
	}

	/* Boot method */
	if (m.m_boot && *m.m_boot) {
		(void) sprintf(label, "%s %s:", LABEL_RT, "BOOT");
		print_line(margin + 2, labelw, prefix, label, m.m_boot);
	}

	/* Monitor start method */
	if (m.m_monitor_start && *m.m_monitor_start) {
		(void) sprintf(label, "%s %s:", LABEL_RT, "MONITOR START");
		print_line(margin + 2, labelw, prefix, label,
		    m.m_monitor_start);
	}

	/* Monitor stop method */
	if (m.m_monitor_stop && *m.m_monitor_stop) {
		(void) sprintf(label, "%s %s:", LABEL_RT, "MONITOR STOP");
		print_line(margin + 2, labelw, prefix, label,
		    m.m_monitor_stop);
	}

	/* Monitor check method */
	if (m.m_monitor_check && *m.m_monitor_check) {
		(void) sprintf(label, "%s %s:", LABEL_RT, "MONITOR CHECK");
		print_line(margin + 2, labelw, prefix, label,
		    m.m_monitor_check);
	}

	/* Prenet start method */
	if (m.m_prenet_start && *m.m_prenet_start) {
		(void) sprintf(label, "%s %s:", LABEL_RT, "PRENET START");
		print_line(margin + 2, labelw, prefix, label,
		    m.m_prenet_start);
	}

	/* Postnet stop method */
	if (m.m_postnet_stop && *m.m_postnet_stop) {
		(void) sprintf(label, "%s %s:", LABEL_RT, "POSTNET STOP");
		print_line(margin + 2, labelw, prefix, label,
		    m.m_postnet_stop);
	}
}

/*
 * show_paramtable()
 *
 * Print entries in RT paramtable.
 *
 * No return value.
 */
static void
show_paramtable(const int margin, char *rt_name, rgm_param_t **table)
{
	rgm_param_t **tablep;
	char label[BUFSIZ];
	char prefix[BUFSIZ];
	int labelw;

	/* Check arguments */
	if (rt_name == NULL || table == NULL)
		return;

	/* Set label width */
	labelw = LABELW2;

	/* Set prefix */
	*prefix = '\0';
	if (rt_name)
		(void) sprintf(prefix, "(%s)", rt_name);

	/* Header for resource type paramtable */
	(void) sprintf(label, "%s %s", LABEL_RT, gettext("param table"));
	print_line(margin, labelw, prefix, label, NULL);

	/* For each param table entry, print entry */
	for (tablep = table;  *tablep && (*tablep)->p_name;  ++tablep)
	    show_paramtable_entry(margin + 2, rt_name, *tablep);
}


/*
 * show_paramtable_entry()
 *
 * Print fields of RT paramtable entry.
 *
 * No return value.
 */
static void
show_paramtable_entry(const int margin, char *rt_name, rgm_param_t *entry)
{
	char *ptr;
	boolean_t is_pn;
	char label[BUFSIZ];
	char prefix[BUFSIZ];
	char value[BUFSIZ];
	int labelw;

	/* Check arguments */
	if (rt_name == NULL || entry == NULL)
		return;

	/* Set label width */
	labelw = LABELW2;

	/* Set prefix */
	*prefix = '\0';
	if (rt_name)
		(void) sprintf(prefix, "(%s)", rt_name);

	/* Param table entry name */
	(void) sprintf(label, "%s %s:", LABEL_RT_P, gettext("name"));
	ptr = (entry->p_name && *entry->p_name)
	    ? entry->p_name : VALUE_STR_NULL;
	print_line(margin, labelw, prefix, label, ptr);

	/* Re-set prefix, if we can */
	if (*prefix && entry->p_name)
		(void) sprintf(prefix, "(%s:%s)", rt_name, entry->p_name);

	/* Param table extension */
	(void) sprintf(label, "%s %s:", LABEL_RT_P, gettext("extension"));
	ptr = (entry->p_extension == B_FALSE)
	    ? VALUE_STR_FALSE : VALUE_STR_TRUE;
	print_line(margin + 2, labelw, prefix, label, ptr);

	/* Param table per-node */
	(void) sprintf(label, "%s %s:", LABEL_RT_P, gettext("per-node"));
	ptr = (entry->p_per_node) ? VALUE_STR_TRUE : VALUE_STR_FALSE;
	print_line(margin + 2, labelw, prefix, label, ptr);

	/* Param table description */
	(void) sprintf(label, "%s %s:", LABEL_RT_P, gettext("description"));
	ptr = (entry->p_description && *entry->p_description)
	    ? entry->p_description : VALUE_STR_NULL;
	print_line(margin + 2, labelw, prefix, label, ptr);

	/* Param table tunability */
	(void) sprintf(label, "%s %s:", LABEL_RT_P, gettext("tunability"));
	switch (entry->p_tunable) {
	case TUNE_NONE:
		ptr = "Not tuneable";
		break;

	case TUNE_AT_CREATION:
		ptr = "At creation";
		break;

	case TUNE_ANYTIME:
		ptr = "Anytime";
		break;

	case TUNE_WHEN_DISABLED:
		ptr = "When disabled";
		break;

	case TUNE_WHEN_OFFLINE:
	case TUNE_WHEN_UNMANAGED:
	case TUNE_WHEN_UNMONITORED:
	default:
		ptr = VALUE_STR_UNKNOWN;
		break;

	}
	print_line(margin + 2, labelw, prefix, label, ptr);

	/* Param table type */
	switch (entry->p_type) {
	case SCHA_PTYPE_STRING:

		/* type string */
		(void) sprintf(label, "%s %s:", LABEL_RT_P, gettext("type"));
		print_line(margin + 2, labelw, prefix, label, "String");

		/* print min length */
		(void) sprintf(label, "%s %s:", LABEL_RT_P,
		    gettext("min length"));
		if (entry->p_min_isset) {
			(void) sprintf(value, "%d", entry->p_min);
			print_line(margin + 2, labelw, prefix, label, value);
		} else {
			print_line(margin + 2, labelw, prefix,
			    label, VALUE_STR_UNSET);
		}

		/* print max length */
		(void) sprintf(label, "%s %s:", LABEL_RT_P,
		    gettext("max length"));
		if (entry->p_max_isset && entry->p_max < MAXRGMVALUESLEN) {
			(void) sprintf(value, "%d", entry->p_max);
			print_line(margin + 2, labelw, prefix, label, value);
		} else {
			print_line(margin + 2, labelw, prefix,
			    label, VALUE_STR_UNSET);
		}

		/* print default */
		(void) sprintf(label, "%s %s:", LABEL_RT_P,
		    gettext("default"));
		if (entry->p_default_isset) {
			ptr = (entry->p_defaultstr && *entry->p_defaultstr)
			    ? entry->p_defaultstr : VALUE_STR_NULL;
		} else {
			ptr = VALUE_STR_UNSET;
		}
		print_line(margin + 2, labelw, prefix, label, ptr);

		break;

	case SCHA_PTYPE_INT:

		/* type int */
		(void) sprintf(label, "%s %s:", LABEL_RT_P, gettext("type"));
		print_line(margin + 2, labelw, prefix, label, "Int");

		/* print min int type */
		(void) sprintf(label, "%s %s:", LABEL_RT_P,
		    gettext("min int value"));
		if (entry->p_min_isset) {
			(void) sprintf(value, "%d", entry->p_min);
			print_line(margin + 2, labelw, prefix, label, value);
		} else {
			print_line(margin + 2, labelw, prefix,
			    label, VALUE_STR_UNSET);
		}

		/* print max int type */
		(void) sprintf(label, "%s %s:", LABEL_RT_P,
		    gettext("max int value"));
		if (entry->p_max_isset && entry->p_max < MAXRGMVALUESLEN) {
			(void) sprintf(value, "%d", entry->p_max);
			print_line(margin + 2, labelw, prefix, label, value);
		} else {
			print_line(margin + 2, labelw, prefix,
			    label, VALUE_STR_UNSET);
		}

		/* print default */
		(void) sprintf(label, "%s %s:", LABEL_RT_P,
		    gettext("default"));
		if (entry->p_default_isset) {
			(void) sprintf(value, "%d", entry->p_defaultint);
			print_line(margin + 2, labelw, prefix, label, value);
		} else {
			print_line(margin + 2, labelw, prefix,
				label, VALUE_STR_UNSET);
		}

		break;

	case SCHA_PTYPE_BOOLEAN:

		/* type boolean */
		(void) sprintf(label, "%s %s:", LABEL_RT_P, gettext("type"));
		print_line(margin + 2, labelw, prefix, label, "Boolean");

		/* print default */
		(void) sprintf(label, "%s %s:", LABEL_RT_P,
		    gettext("default"));
		if (entry->p_default_isset) {
			ptr = (entry->p_defaultbool == B_FALSE)
			    ? VALUE_STR_FALSE : VALUE_STR_TRUE;
		} else {
			ptr = VALUE_STR_UNSET;
		}
		print_line(margin + 2, labelw, prefix, label, ptr);

		break;

	case SCHA_PTYPE_ENUM:

		/* type enum */
		(void) sprintf(label, "%s %s:", LABEL_RT_P, gettext("type"));
		print_line(margin + 2, labelw, prefix, label, "Enum");

		/* print enum list */
		if (entry->p_name &&
		    (strcmp(SCHA_FAILOVER_MODE, entry->p_name) == 0)) {
			/*
			 * the failover_mode system property doesn't
			 * use the enumlist any more.
			 * we work around it.
			 */
			ptr = get_fom_name_list();
		} else {
			ptr = namelist_to_string(entry->p_enumlist);
		}
		if (ptr) {
			(void) sprintf(label, "%s %s:", LABEL_RT_P,
			    gettext("enum list"));
			print_line(margin + 2, labelw, prefix, label, ptr);
			free(ptr);
		}

		/* print default */
		(void) sprintf(label, "%s %s:", LABEL_RT_P,
		    gettext("default"));
		if (entry->p_default_isset) {
			ptr = entry->p_defaultstr;
			if (ptr) {
				(void) sprintf(label, "%s %s:", LABEL_RT_P,
				    gettext("default"));
				print_line(margin + 2, labelw, prefix,
				    label, entry->p_defaultstr);
			} else {
				print_line(margin + 2, labelw, prefix,
				    label, VALUE_STR_NULL);
			}
		} else {
			print_line(margin + 2, labelw, prefix,
			    label, VALUE_STR_UNSET);
		}

		break;

	case SCHA_PTYPE_STRINGARRAY:

		/* type stringarray */
		(void) sprintf(label, "%s %s:", LABEL_RT_P, gettext("type"));
		print_line(margin + 2, labelw, prefix, label, "Stringarray");

		/* print min length */
		(void) sprintf(label, "%s %s:", LABEL_RT_P,
		    gettext("min length"));
		if (entry->p_min_isset) {
			(void) sprintf(value, "%d", entry->p_min);
			print_line(margin + 2, labelw, prefix, label, value);
		} else {
			print_line(margin + 2, labelw, prefix,
			    label, VALUE_STR_UNSET);
		}

		/* print max length */
		(void) sprintf(label, "%s %s:", LABEL_RT_P,
		    gettext("max length"));
		if (entry->p_max_isset && entry->p_max < MAXRGMVALUESLEN) {
			(void) sprintf(value, "%d", entry->p_max);
			print_line(margin + 2, labelw, prefix, label, value);
		} else {
			print_line(margin + 2, labelw, prefix,
			    label, VALUE_STR_UNSET);
		}

		/* print min array length */
		(void) sprintf(label, "%s %s:", LABEL_RT_P,
		    gettext("min array length"));
		if (entry->p_arraymin_isset) {
			(void) sprintf(value, "%d", entry->p_arraymin);
			print_line(margin + 2, labelw, prefix, label, value);
		} else {
			print_line(margin + 2, labelw, prefix,
			    label, VALUE_STR_UNSET);
		}

		/* print max array length */
		(void) sprintf(label, "%s %s:", LABEL_RT_P,
		    gettext("max array length"));
		if (entry->p_arraymax_isset) {
			(void) sprintf(value, "%d", entry->p_arraymax);
			print_line(margin + 2, labelw, prefix, label, value);
		} else {
			print_line(margin + 2, labelw, prefix,
			    label, VALUE_STR_UNSET);
		}

		/* print default */
		(void) sprintf(label, "%s %s:", LABEL_RT_P,
		    gettext("default"));
		if (entry->p_default_isset) {
			ptr = namelist_to_string(entry->p_defaultarray);
			if (ptr) {
				print_line(margin + 2, labelw, prefix,
				    label, ptr);
				free(ptr);
			} else {
				print_line(margin + 2, labelw, prefix,
				    label, VALUE_STR_NULL);
			}
		} else {
			print_line(margin + 2, labelw, prefix,
			    label, VALUE_STR_UNSET);
		}
		break;

	default:

		/* type unknown */
		(void) sprintf(label, "%s %s:", LABEL_RT_P, gettext("type"));
		print_line(margin + 2, labelw, prefix, label,
		    VALUE_STR_UNKNOWN);

		break;
	}   /* lint !e788 */
}

/*
 * convert_lprops_to_rtsys_prop_node_name
 *
 * Called only by manage_rt.
 *
 * Extract RT_SYSTEM "synthetic" node name from property list "lprops_p"
 * and insert into namelist "lnodes_p".
 *
 * "-y" option for RT is valid only for RT_SYSTEM property.
 * This option should occur at most once in the scrgadm command.
 *
 * No other properties should be contained in the property list.
 *
 * If no errors, and RT_SYSTEM property is not found in proplist,
 * then lnodes_p is not modified. If RT_SYSTEM property is found,
 * then a new namelist elt. is created, and it's name is strdup'ed
 * from SCHA_RTSYS_TRUE or SCHA_RTSYS_FALSE, as appropriate.
 *
 * Possible return values:
 *	SCHA_ERR_NOERR
 *	SCHA_ERR_INVAL
 *	SCHA_ERR_NOMEM
 *
 */
static scha_errmsg_t
convert_lprops_to_rtsys_prop_node_name(const scrgadm_name_valuel_list_t *lprops,
    namelist_t **lnodes_p)
{
	scha_errmsg_t status = {SCHA_ERR_NOERR, NULL};
	char *prop_value = NULL;
	char *synth_node_name = NULL;
	char *new_name = NULL;
	namelist_t *new_nl = NULL;

	/*
	 * Empty props list is okay
	 */
	if (lprops == NULL)
		return (status);

	/*
	 * Check for valid prop. name
	 */
	if (strcasecmp(lprops->name, SCHA_RT_SYSTEM) != 0) {
		status.err_code = SCHA_ERR_INVAL;
		return (status);
	}

	/*
	 * There's only one valid prop, so list should contain
	 * no more than than one elt.
	 */
	if (lprops->next != NULL) {
		status.err_code = SCHA_ERR_INVAL;
		return (status);
	}

	/*
	 * should only have one elt. in prop. value namelist
	 */
	if (lprops->value_list->nl_next != NULL) {
		status.err_code = SCHA_ERR_INVAL;
		return (status);
	}

	/*
	 * Check value (string) of RT_SYSTEM prop.
	 */
	prop_value = lprops->value_list->nl_name;
	if (strcasecmp(prop_value, SCHA_TRUE) == 0) {
		synth_node_name = SCHA_RTSYS_TRUE;
	} else if (strcasecmp(prop_value, SCHA_FALSE) == 0) {
		synth_node_name = SCHA_RTSYS_FALSE;
	} else {
		status.err_code = SCHA_ERR_INVAL;
		return (status);
	}

	new_nl = (namelist_t *)calloc(1,
		    sizeof (namelist_t));

	if (new_nl == NULL) {
		status.err_code = SCHA_ERR_NOMEM;
		goto err_exit;
	}
	new_name = strdup(synth_node_name);
	if (new_name == NULL) {
		status.err_code = SCHA_ERR_NOMEM;
		goto err_exit;
	}
	new_nl->nl_name = new_name;

	/*
	 * Insert new elt. into the namelist
	 */
	new_nl->nl_next = *lnodes_p;
	*lnodes_p = new_nl;
	return (status);

err_exit:
	if (new_nl != NULL)
		free(new_nl);

	if (new_name != NULL)
		free(new_name);

	return (status);


}	/* convert_lprops_to_rtsys_prop_node_name */

/*
 * manage_rt()
 *
 * Manage Resource Type by specified action.
 *
 * Return values are of type scha_errmsg_t
 *	generated by librgm(d)
 *	(refer to /usr/cluster/include/scha_err.h)
 *
 * lnodes are expected to be "real" (not synthetic) node names.
 */
static scha_errmsg_t
manage_rt(
    const scrgadm_action_t action,
    const scrgadm_name_t rt_name,
    const scrgadm_name_valuel_list_t *lprops,
    namelist_t *lnodes,
    RtrFileResult *rtr_results,
    boolean_t bypass_install_mode)
{
	scha_errmsg_t status = {SCHA_ERR_NOERR, NULL};

	if (lnodes && !is_asterisk_specified(lnodes)) {
		status = process_namelist_to_names(
		    (namelist_t *)lnodes);
		if (status.err_code != SCHA_ERR_NOERR) {
			return (status);
		}
	}

	/*
	 * Extract RT_SYSTEM "synthetic" node name from lprops and insert
	 * into lnodes.
	 */
	status = convert_lprops_to_rtsys_prop_node_name(lprops, &lnodes);
	if (status.err_code != SCHA_ERR_NOERR) {
		return (status);
	}
	switch (action) {
	case SCRGADM_ACTION_ADD:
		status = create_rt(rtr_results, lnodes, bypass_install_mode);
		scrgadm_free_rtrresult(rtr_results);
		break;
	case SCRGADM_ACTION_CHANGE:
		status = modify_rt(rt_name, lnodes);
		break;
	case SCRGADM_ACTION_REMOVE:
		status = remove_rt(rt_name);
		break;
	default:
		status.err_code = SCHA_ERR_INVAL;
		break;
	}
	return (status);
} /* manage_rt */


/*
 * create_rt()
 *
 * Create Resource Type using librgm directly.
 * The RGM loads the new RT from CCR when it needs it.
 *
 * Possible return values:
 *	SCHA_ERR_NOERR
 *	SCHA_ERR_NOMEM
 *	SCHA_ERR_INVAL
 */
static scha_errmsg_t
create_rt(RtrFileResult *rtr_fres,
    namelist_t *lnodes,
    boolean_t bypass_install_mode) {

	namelist_t *tmp;
	boolean_t rt_system = B_FALSE;
	boolean_t rt_system_defined = B_FALSE;
	scha_errmsg_t status = {SCHA_ERR_NOERR, NULL};
	rgm_rt_t	*rt = NULL;
	nodeidlist_t *nodeidlist;

	status = rgm_scrgadm_parse2rt(rtr_fres, &rt, NULL);
	if (rt && status.err_code == SCHA_ERR_NOERR) {

		/*
		 * extract RT_SYSTEM prop value from the node list.
		 * the property, if present, is the first one in the list
		 */
		if (lnodes &&
		    (strcmp(lnodes->nl_name, SCHA_RTSYS_TRUE) == 0)) {
			rt_system_defined = B_TRUE;
			rt_system = B_TRUE;
		} else if (lnodes &&
		    (strcmp(lnodes->nl_name, SCHA_RTSYS_FALSE) == 0)) {
			rt_system_defined = B_TRUE;
			rt_system = B_FALSE;
		}
		if (rt_system_defined) {
			rt->rt_system = rt_system;
			tmp = lnodes;
			lnodes = lnodes->nl_next;
			tmp->nl_next = NULL;
			rgm_free_nlist(tmp);
		}

		rt->rt_instl_nodes.is_ALL_value =
			is_asterisk_specified(lnodes);
		if (!rt->rt_instl_nodes.is_ALL_value) {
			status = rgm_scrgadm_convert_namelist_to_nodeidlist(
			    lnodes, &nodeidlist);
			if (status.err_code != SCHA_ERR_NOERR) {
				rgm_free_rt(rt);
				return (status);
			}
			rt->rt_instl_nodes.nodeids = nodeidlist;
		}
		else
			rt->rt_instl_nodes.nodeids = NULL;
		status = rgm_scrgadm_register_rt(rt, bypass_install_mode, NULL);
	}
	rgm_free_rt(rt);

	return (status);
} /* create_rt */

/*
 * modify_rt()
 *
 * Modify Resource Type.
 *
 * Possible return values:
 *	SCHA_ERR_NOERR
 *	SCHA_ERR_NOMEM
 */
static scha_errmsg_t
modify_rt(const scrgadm_name_t rt_name, const namelist_t *lnodes) {
	char **inst_nodes = NULL;
	scha_errmsg_t status = {SCHA_ERR_NOERR, NULL};

	if (status.err_code == SCHA_ERR_NOERR) {
		/* create array of names from nameslist */
		inst_nodes = create_name_array(lnodes);
		if (inst_nodes) {
			/* modify rt configuration */
			status = rgm_scrgadm_update_rt_instnodes(rt_name,
			    inst_nodes, NULL);
			free(inst_nodes);
		} else { /* failed to create array */
			status.err_code = SCHA_ERR_NOMEM;
		}
	}

	return (status);
}	/* modify_rt */


/*
 *	remove_rt()
 *
 * Remove Resource Type.
 *
 * Possible return values:
 *	SCHA_ERR_NOERR
 */
static scha_errmsg_t
remove_rt(const scrgadm_name_t rt_name)
{
	scha_errmsg_t status = {SCHA_ERR_NOERR, NULL};

	status = rgm_scrgadm_unregister_rt(rt_name, NULL);

	return (status);
} /* remove_rt */


/*
 *
 *	*****  Resource Group (RG) functions  *****
 *
 */


/*
 * print_rg_all()
 *
 * Print current RG configuration.
 *
 * No return value.
 */
static void
print_rg_all(const int margin, const scrgadm_print_t print_level)
{
	scha_errmsg_t rgm_status = {SCHA_ERR_NOERR, NULL};
	char **managed_rg_names = 0;
	char **unmanaged_rg_names = 0;
	char **rg_namep;

	/* get all rgs */
	rgm_status = rgm_scrgadm_getrglist(&managed_rg_names,
	    &unmanaged_rg_names, B_FALSE, NULL);
	if (rgm_status.err_code != SCHA_ERR_NOERR) {
		print_scha_error(rgm_status);
		return;
	}

	/* Print managed rgs and their resources */
	for (rg_namep = managed_rg_names;  rg_namep && *rg_namep;
	    ++rg_namep)
		print_rg(margin, print_level, *rg_namep);

	/* Print unmanaged rgs and their resources */
	for (rg_namep = unmanaged_rg_names;  rg_namep && *rg_namep;
	    ++rg_namep)
		print_rg(margin, print_level, *rg_namep);

	/* Free lists */
	if (managed_rg_names)
		rgm_free_strarray(managed_rg_names);
	if (unmanaged_rg_names)
		rgm_free_strarray(unmanaged_rg_names);
}

/*
 * Display field values of RG and its resources
 *
 * print_rg()
 *
 * no return value
 */
static void
print_rg(const int margin, const scrgadm_print_t print_level, char *rg_name)
{
	scha_errmsg_t rgm_status = {SCHA_ERR_NOERR, NULL};
	rgm_rg_t *rg = (rgm_rg_t *)0;
	char *ptr, *ptr2;
	char label[BUFSIZ];
	char prefix[BUFSIZ];
	char value[BUFSIZ];
	int labelw;

	/* Check arguments */
	if (rg_name == NULL)
		return;

	/* Get the rg */
	rgm_status = rgm_scrgadm_getrgconf(rg_name, &rg, NULL);
	if (rgm_status.err_code != SCHA_ERR_NOERR) {
		print_scha_error(rgm_status);
		return;
	}
	if (rg == NULL)
		return;

	/* Set label width */
	labelw = (print_level > SCRGADM_PRINT_LOW) ? LABELW2 : LABELW1;

	/* Blank line */
	(void) putchar('\n');

	/* Resource group name */
	(void) sprintf(label, "%s %s:", LABEL_RG, gettext("name"));
	ptr = (rg->rg_name && *rg->rg_name) ? rg->rg_name : VALUE_STR_NULL;
	print_line(margin, labelw, NULL, label, ptr);

	/* Set prefix */
	*prefix = '\0';
	if (print_level > SCRGADM_PRINT_LOW) {
		if (rg->rg_name) {
			(void) sprintf(prefix, "(%s)", rg->rg_name);
		}
	}

	/* Resource group RG_description */
	(void) sprintf(label, "%s %s:", LABEL_RG, "RG_description");
	ptr = (rg->rg_description && *rg->rg_description)
	    ? rg->rg_description : VALUE_STR_NULL;
	print_line(margin + 2, labelw, prefix, label, ptr);


	/* Resource group mode */
	(void) sprintf(label, "%s %s:", LABEL_RG, gettext("mode"));
	ptr = (rg->rg_mode == RGMODE_FAILOVER)
		? "Failover" : "Scalable";
	print_line(margin + 2, labelw, prefix, label, ptr);

	/* Verbose */
	if (print_level > SCRGADM_PRINT_LOW) {

		/* Resource group management state */
		(void) sprintf(label, "%s %s:", LABEL_RG, "management state");
		ptr = (rg->rg_unmanaged == B_FALSE) ? "Managed" : "Unmanaged";
		print_line(margin + 2, labelw, prefix, label, ptr);

#ifdef SC_SRM
		/* Resource group project name */
		(void) sprintf(label, "%s %s:", LABEL_RG, "RG_project_name");
		ptr2 = NULL;
		if (rg->rg_project_name && *(rg->rg_project_name) != '\0') {
			ptr = rg->rg_project_name;
		} else {
			/*
			 * RG project name is not set; print the system default
			 * project name
			 */
			rgm_status =
			    rgm_scrgadm_get_default_proj(NULL, &ptr2, NULL);
			if (rgm_status.err_code != SCHA_ERR_NOERR) {
				print_scha_error(rgm_status);
				return;
			}
			ptr = ptr2;
		}
		print_line(margin + 2, labelw, prefix, label, ptr);
		if (ptr2)
			free(ptr2);
#endif

		/* SC SLM addon start */
		/*
		 * SLM type
		 */
		(void) sprintf(label, "%s %s:", LABEL_RG, SCHA_RG_SLM_TYPE);
		ptr2 = NULL;
		if (rg->rg_slm_type && *(rg->rg_slm_type) != '\0') {
			ptr = rg->rg_slm_type;
		} else {
			/*
			 * RG SLM type is not set; print the system
			 * default : "manual"
			 */
			rgm_status = rgm_scrgadm_get_rg_slm_type(
			    rg->rg_name, &ptr2, NULL);
			if (rgm_status.err_code != SCHA_ERR_NOERR) {
				print_scha_error(rgm_status);
				return;
			}
			ptr = ptr2;
		}
		print_line(margin + 2, labelw, prefix, label, ptr);
		if (ptr2)
			free(ptr2);

		if (strcmp(ptr, SCHA_SLM_TYPE_AUTOMATED) == 0) {

			/*
			 * SLM project name
			 */
			(void) sprintf(label, "%s %s:",
			    LABEL_RG, "RG_SLM_projectname");
			ptr2 = malloc(strlen(rg->rg_name) + strlen("SCSLM_"));
			if (ptr2 == NULL) {
				rgm_status.err_code = SCHA_ERR_NOMEM;
				print_scha_error(rgm_status);
				return;
			}
			(void) sprintf(ptr2, "SCSLM_%s", rg->rg_name);
			slm_replace_dash_in_str(ptr2);
			print_line(margin + 2, labelw, prefix, label, ptr2);
			free(ptr2);

			/*
			 * SLM pset type
			 */
			(void) sprintf(label, "%s %s:", LABEL_RG,
			    SCHA_RG_SLM_PSET_TYPE);
			ptr2 = NULL;
			if (rg->rg_slm_pset_type &&
			    *(rg->rg_slm_pset_type) != '\0') {
				ptr = rg->rg_slm_pset_type;
			} else {
				/*
				 * RG SLM pset type is not set; print the
				 * system default : "default"
				 */
				rgm_status = rgm_scrgadm_get_rg_slm_pset_type(
				    rg->rg_name, &ptr2, NULL);
				if (rgm_status.err_code != SCHA_ERR_NOERR) {
					print_scha_error(rgm_status);
					return;
				}
				ptr = ptr2;
			}
			print_line(margin + 2, labelw, prefix, label, ptr);
			if (ptr2)
				free(ptr2);

			/*
			 * RG_SLM_CPU_SHARES
			 */
			(void) sprintf(label, "%s %s:", LABEL_RG,
			    SCHA_RG_SLM_CPU_SHARES);
			(void) sprintf(value, "%d", rg->rg_slm_cpu);
			print_line(margin + 2, labelw, prefix, label, value);

			/*
			 * RG_SLM_PSET_MIN
			 */
			(void) sprintf(label, "%s %s:", LABEL_RG,
			    SCHA_RG_SLM_PSET_MIN);
			(void) sprintf(value, "%d", rg->rg_slm_cpu_min);
			print_line(margin + 2, labelw, prefix, label, value);
		}
		/* SC SLM addon end */

		/*
		 * Resource group affinities
		 */
		(void) sprintf(label, "%s %s:", LABEL_RG, "RG_affinities");
		ptr2 = NULL;
		if (rg->rg_affinities == NULL) {
			ptr = VALUE_STR_NULL;
		} else {
			ptr2 = namelist_to_string(rg->rg_affinities);
			ptr = (ptr2) ? ptr2 : VALUE_STR_NULL;
		}
		print_line(margin + 2, labelw, prefix, label, ptr);
		if (ptr2)
			free(ptr2);

		/* Resource group auto startup when all nodes are rebooting */
		(void) sprintf(label, "%s %s:", LABEL_RG,
		    "Auto_start_on_new_cluster");
		ptr = (rg->rg_auto_start == B_FALSE)
		    ? VALUE_STR_FALSE : VALUE_STR_TRUE;
		print_line(margin + 2, labelw, prefix, label, ptr);

		/* Resource group Failback */
		(void) sprintf(label, "%s %s:", LABEL_RG, "Failback");
		ptr = (rg->rg_failback == B_FALSE)
		    ? VALUE_STR_FALSE : VALUE_STR_TRUE;
		print_line(margin + 2, labelw, prefix, label, ptr);

		/* Resource group potential masters */
		(void) sprintf(label, "%s %s:", LABEL_RG, "Nodelist");
		ptr2 = NULL;
		if (rg->rg_nodelist == NULL) {
			ptr = VALUE_STR_ALL;
		} else {
			ptr2 = scrgadm_nodeidlist_to_spacestring(
			    rg->rg_nodelist);
			ptr = (ptr2) ? ptr2 : VALUE_STR_NULL;
		}
		print_line(margin + 2, labelw, prefix, label, ptr);
		if (ptr2)
			free(ptr2);

		/* Resource group Maximum_primaries */
		(void) sprintf(label, "%s %s:", LABEL_RG, "Maximum_primaries");
		(void) sprintf(value, "%d", rg->rg_max_primaries);
		print_line(margin + 2, labelw, prefix, label, value);

		/* Resource group Desired_primaries */
		(void) sprintf(label, "%s %s:", LABEL_RG, "Desired_primaries");
		(void) sprintf(value, "%d", rg->rg_desired_primaries);
		print_line(margin + 2, labelw, prefix, label, value);

		/* Resource group RG_dependencies */
		(void) sprintf(label, "%s %s:", LABEL_RG, "RG_dependencies");
		ptr2 = NULL;
		if (rg->rg_dependencies) {
			ptr2 = namelist_to_string(rg->rg_dependencies);
			ptr = (ptr2) ? ptr2 : VALUE_STR_NULL;
		} else {
			ptr = VALUE_STR_NULL;
		}
		print_line(margin + 2, labelw, prefix, label, ptr);
		if (ptr2)
			free(ptr2);

		/* Resource group network dependencies */
		(void) sprintf(label, "%s %s:", LABEL_RG,
		    gettext("network dependencies"));
		ptr = (rg->rg_impl_net_depend == B_FALSE)
		    ? VALUE_STR_FALSE : VALUE_STR_TRUE;
		print_line(margin + 2, labelw, prefix, label, ptr);

		/* Resource group Global_resources_used */
		(void) sprintf(label, "%s %s:", LABEL_RG,
		    "Global_resources_used");
		ptr2 = NULL;
		if (rg->rg_glb_rsrcused.is_ALL_value) {
			ptr = VALUE_STR_ALL;
		} else {
			ptr2 = namelist_to_string(rg->rg_glb_rsrcused.names);
			ptr = (ptr2) ? ptr2 : VALUE_STR_NULL;
		}
		print_line(margin + 2, labelw, prefix, label, ptr);
		if (ptr2)
			free(ptr2);

		/* Resource group pingpong interval */
		(void) sprintf(label, "%s %s:", LABEL_RG, "Pingpong_interval");
		(void) sprintf(value, "%d", rg->rg_ppinterval);
		print_line(margin + 2, labelw, prefix, label, value);

		/* Resource group Pathprefix */
		(void) sprintf(label, "%s %s:", LABEL_RG, "Pathprefix");
		ptr = (rg->rg_pathprefix && *rg->rg_pathprefix)
		    ? rg->rg_pathprefix : VALUE_STR_NULL;
		print_line(margin + 2, labelw, prefix, label, ptr);

		/*
		 * RG_System
		 */
		(void) sprintf(label, "%s %s:", LABEL_RG, gettext("system"));
		ptr = (rg->rg_system == B_FALSE)
		    ? VALUE_STR_FALSE : VALUE_STR_TRUE;
		print_line(margin + 2, labelw, prefix, label, ptr);

		/* Suspend_automatic_recovery */
		(void) sprintf(label, "%s %s:", LABEL_RG, gettext(
		    "Suspend_automatic_recovery"));
		ptr = (rg->rg_suspended == B_FALSE)
		    ? VALUE_STR_FALSE : VALUE_STR_TRUE;
		print_line(margin + 2, labelw, prefix, label, ptr);
	} /* if (print_level > SCRGADM_PRINT_LOW) */

	/* Print all the resources for this rg */
	print_rs_all(margin + 2, print_level, rg_name);
}

/*
 * manage_rg()
 *
 * Manage RG by specified action.
 *
 * Return values are of type scha_errmsg_t
 *	generated by librgm(d)
 *	(refer to /usr/cluster/include/scha_err.h)
 */
static scha_errmsg_t
manage_rg(const scrgadm_action_t action, const scrgadm_name_t rg_name,
    scrgadm_name_valuel_list_t *lprops,
    namelist_t *lnodes, boolean_t bypass_install_mode, boolean_t v_flag)
{
	scha_errmsg_t status = {SCHA_ERR_NOERR, NULL};
	scrgadm_name_valuel_list_t *nprops = NULL;


	if (lnodes) {
		status = process_namelist_to_names(
		    (namelist_t *)lnodes);
		if (status.err_code != SCHA_ERR_NOERR) {
			goto cleanup;
		}
		/* get nodelist into proper form for create_rg()/modify_rg() */
		status = namelist_to_nvl_list(SCHA_NODELIST,
		    lnodes, &nprops);
		if (status.err_code != SCHA_ERR_NOERR) {
			goto cleanup;
		}

		/* append the property to property list */
		status = append_props(&lprops, &nprops);
		if (status.err_code != SCHA_ERR_NOERR) {
			goto cleanup;
		}

	}

	switch (action) {
	case SCRGADM_ACTION_ADD:
		status = create_rg(rg_name, lprops, bypass_install_mode);
		break;
	case SCRGADM_ACTION_CHANGE:
		status = modify_rg(rg_name, lprops);
		break;
	case SCRGADM_ACTION_REMOVE:
		status = remove_rg(rg_name, v_flag);
		break;
	default:
		status.err_code = SCHA_ERR_INVAL;
		break;
	}

cleanup:
	free_nvl_limited(lprops);
	return (status);
} /* manage_rg */


/*
 *	create_rg()
 *
 * Create RG with specified properties.
 *
 * Possible return values:
 *	SCHA_ERR_NOERR
 *	SCHA_ERR_NOMEM
 */
static scha_errmsg_t
create_rg(const scrgadm_name_t rg_name,
    scrgadm_name_valuel_list_t *lprops,
    boolean_t bypass_install_mode)
{
	scha_property_t *pl = NULL;
	scha_errmsg_t status = {SCHA_ERR_NOERR, NULL};

	/* package up props list for librgm */
	pl = create_property_array(lprops);
	if (pl) {
		/* and create the rg */
		status = rgm_scrgadm_create_rg(rg_name, &pl,
		    bypass_install_mode, NULL);
		free_property_array(&pl);
	}
	return (status);
} /* create_rg */


/*
 *	modify_rg()
 *
 * Modify RG with specified properties.
 *
 * Possible return values:
 *	SCHA_ERR_NOERR
 *	SCHA_ERR_NOMEM
 */
static scha_errmsg_t
modify_rg(const scrgadm_name_t rg_name,
    const scrgadm_name_valuel_list_t *lprops) {
	scha_property_t *pl = NULL;
	scha_errmsg_t status = {SCHA_ERR_NOERR, NULL};

	if (status.err_code == SCHA_ERR_NOERR) {
		/* create array from props list */
		pl = create_property_array(lprops);
		if (pl) {
			/* modify rg configuration */
			status = rgm_scrgadm_update_rg_property(
			    rg_name, &pl, FROM_SCRGADM, NULL, NULL);
			free_property_array(&pl);
		}
	}
	return (status);
} /* modify_rg */


/*
 * remove_rg()
 *
 * Remove Resource Group
 *
 * Possible return values:
 *	SCHA_ERR_NOERR
 */
static scha_errmsg_t
remove_rg(const scrgadm_name_t rg_name, boolean_t v_flag)
{
	scha_errmsg_t status = {SCHA_ERR_NOERR, NULL};

	status = rgm_scrgadm_remove_rg(rg_name, v_flag, NULL);

	return (status);
} /* remove_rg */


/*
 *
 *	*****  Resource (RS) functions  *****
 *
 */



/*
 * print_rs_all()
 *
 * Print current Resource configuration.
 *
 * No return value.
 *
 */
static void
print_rs_all(const int margin, const scrgadm_print_t print_level,
    char *rg_name)
{
	scha_errmsg_t rgm_status = {SCHA_ERR_NOERR, NULL};
	char **rs_names = (char **)0;
	char **rs_namep;

	/* Print all resources? */
	if (rg_name == NULL) {
		char **managed_rg_names = (char **)0;
		char **unmanaged_rg_names = (char **)0;
		char **rg_namep;

		/* get all rgs */
		rgm_status = rgm_scrgadm_getrglist(&managed_rg_names,
		    &unmanaged_rg_names, B_FALSE, NULL);
		if (rgm_status.err_code != SCHA_ERR_NOERR) {
			print_scha_error(rgm_status);
			return;
		}

		/* Print managed rg resources */
		for (rg_namep = managed_rg_names;  rg_namep && *rg_namep;
		    ++rg_namep)
			print_rs_all(margin, print_level, *rg_namep);

		/* Print unmanaged rg resources */
		for (rg_namep = unmanaged_rg_names;  rg_namep && *rg_namep;
		    ++rg_namep)
			print_rs_all(margin, print_level, *rg_namep);

		/* Free lists */
		if (managed_rg_names)
			rgm_free_strarray(managed_rg_names);
		if (unmanaged_rg_names)
			rgm_free_strarray(unmanaged_rg_names);

		/* Done */
		return;
	}

	/* Get all resources for this RG */
	rgm_status = rgm_scrgadm_getrsrclist(rg_name, &rs_names, NULL);
	if (rgm_status.err_code != SCHA_ERR_NOERR) {
		print_scha_error(rgm_status);
		return;
	}
	if (rs_names == NULL)
		return;

	/* print each resource */
	for (rs_namep = rs_names;  *rs_namep;  ++rs_namep)
		print_rs(margin, print_level, *rs_namep);

	/* free list of resources */
	rgm_free_strarray(rs_names);
}

/*
 * print_rs()
 *
 * Display RS
 *
 * No return value.
 */
static void
print_rs(const int margin, const scrgadm_print_t print_level, char *rs_name)
{
	scha_errmsg_t rgm_status = {SCHA_ERR_NOERR, NULL};
	rgm_resource_t *rs = (rgm_resource_t *)0;
	char *ptr, *ptr2;
	char label[BUFSIZ];
	char prefix[BUFSIZ];
	int labelw;

	scconf_nodeid_t nodeid;
	rgm_rg_t *rg = (rgm_rg_t *)0;

	nodeidlist_t *nodelist, *nl;
	char *nodename = NULL;
	scha_switch_t swtch;
	char new_prefix[BUFSIZ];

	/* Check arguments */
	if (rs_name == NULL)
		return;

	/* get configuration for this resource */
	rgm_status = rgm_scrgadm_getrsrcconf(rs_name, &rs, NULL);
	if (rgm_status.err_code != SCHA_ERR_NOERR) {
		print_scha_error(rgm_status);
		return;
	}
	if (rs == NULL)
		return;

	/* Set label width */
	labelw = (print_level > SCRGADM_PRINT_LOW) ? LABELW2 : LABELW1;

	/* Blank line */
	(void) putchar('\n');

	/* Set prefix */
	*prefix = '\0';
	if (print_level > SCRGADM_PRINT_LOW && rs->r_rgname) {
		(void) sprintf(prefix, "(%s)", rs->r_rgname);
	}

	/* Resource name */
	(void) sprintf(label, "%s %s:", LABEL_RS, gettext("name"));
	ptr = rs->r_name ? rs->r_name : VALUE_STR_NULL;
	print_line(margin, labelw, prefix, label, ptr);

	/* RE-set prefix */
	if (print_level > SCRGADM_PRINT_LOW && rs->r_rgname && rs->r_name) {
		(void) sprintf(prefix, "(%s:%s)", rs->r_rgname, rs->r_name);
	}

	/* Resource R_description */
	(void) sprintf(label, "%s %s:", LABEL_RS, "R_description");
	ptr = rs->r_description ? rs->r_description : VALUE_STR_NULL;
	print_line(margin + 2, labelw, prefix, label, ptr);

	/* Resource type */
	(void) sprintf(label, "%s %s:", LABEL_RS, gettext("resource type"));
	ptr = rs->r_type ? rs->r_type : VALUE_STR_NULL;
	print_line(margin + 2, labelw, prefix, label, ptr);

	/* Resource type version */
	(void) sprintf(label, "%s %s:", LABEL_RS, gettext("type version"));
	ptr = rs->r_type_version ? rs->r_type_version : "";
	print_line(margin + 2, labelw, prefix, label, ptr);

	/* Resource group name */
	(void) sprintf(label, "%s %s:", LABEL_RS,
	    gettext("resource group name"));
	ptr = rs->r_rgname ? rs->r_rgname : VALUE_STR_NULL;
	print_line(margin + 2, labelw, prefix, label, ptr);

#ifdef SC_SRM
	/* Resource project name */
	(void) sprintf(label, "%s %s:", LABEL_RS,
	    gettext("resource project name"));
	ptr2 = NULL;
	if (rs->r_project_name && *(rs->r_project_name) != '\0')
		ptr = rs->r_project_name;
	else {
		/*
		 * Resource project name is not set; get the default project
		 * name of the containing RG
		 */
		rgm_status =
		    rgm_scrgadm_get_default_proj(rs->r_rgname, &ptr2, NULL);
		if (rgm_status.err_code != SCHA_ERR_NOERR) {
			print_scha_error(rgm_status);
			return;
		}
		ptr = ptr2;
	}
	print_line(margin + 2, labelw, prefix, label, ptr);
	if (ptr2)
		free(ptr2);
#endif

	/* Verbose */
	if (print_level > SCRGADM_PRINT_LOW) {

		/* Resource enabled/disabled (on/off switch) */
		(void) sprintf(label, "%s %s:", LABEL_RS,
		    gettext("enabled"));
		rgm_status = rgm_scrgadm_getrgconf(rs->r_rgname, &rg, NULL);
		if (rgm_status.err_code != SCHA_ERR_NOERR) {
			print_scha_error(rgm_status);
			goto exit_err;
		}
		nodelist = rg->rg_nodelist;

		for (nl = nodelist; nl; nl = nl->nl_next) {

			nodename = scrgadm_nodeidzone_to_nodename(nl->nl_nodeid,
			    nl->nl_zonename);
			rgm_status = get_swtch(rs->r_onoff_switch,
			    nodename, &swtch);
			if (rgm_status.err_code != SCHA_ERR_NOERR) {
				print_scha_error(rgm_status);
				goto exit_err;
			}
			switch (swtch) {
			case SCHA_SWITCH_DISABLED:
				ptr = VALUE_STR_FALSE;
				break;

			case SCHA_SWITCH_ENABLED:
				ptr = VALUE_STR_TRUE;
				break;

			default:
				ptr = VALUE_STR_UNKNOWN;
			}
			create_nodename_prefix(prefix, nodename, new_prefix);
			print_line(margin + 2, labelw, new_prefix, label, ptr);
		}

		/* Resource monitored switch */
		(void) sprintf(label, "%s %s:", LABEL_RS,
		    gettext("monitor enabled"));
		for (nl = nodelist; nl; nl = nl->nl_next) {

			nodename = scrgadm_nodeidzone_to_nodename(nl->nl_nodeid,
			    nl->nl_zonename);
			rgm_status = get_swtch(rs->r_monitored_switch,
			    nodename, &swtch);
			if (rgm_status.err_code != SCHA_ERR_NOERR) {
				print_scha_error(rgm_status);
				goto exit_err;
			}

			switch (swtch) {
			case SCHA_SWITCH_DISABLED:
				ptr = VALUE_STR_FALSE;
				break;

			case SCHA_SWITCH_ENABLED:
				ptr = VALUE_STR_TRUE;
				break;

			default:
				ptr = VALUE_STR_UNKNOWN;
			}

			create_nodename_prefix(prefix, nodename, new_prefix);
			print_line(margin + 2, labelw, new_prefix, label, ptr);
		}

		/* Very verbose */
		if (print_level > SCRGADM_PRINT_MEDIUM) {

			/* Resource strong dependencies */
			(void) sprintf(label, "%s %s:", LABEL_RS,
			    gettext("strong dependencies"));
			ptr2 = NULL;
			if (rs->r_dependencies.dp_strong) {
				ptr2 = rdep_list_to_string(
				    rs->r_dependencies.dp_strong);
				ptr = (ptr2) ? ptr2 : VALUE_STR_NULL;
			} else {
				ptr = VALUE_STR_NULL;
			}
			print_line(margin + 2, labelw, prefix, label, ptr);
			if (ptr2)
				free(ptr2);

			/* Resource weak dependencies */
			(void) sprintf(label, "%s %s:", LABEL_RS,
			    gettext("weak dependencies"));
			ptr2 = NULL;
			if (rs->r_dependencies.dp_weak) {
				ptr2 = rdep_list_to_string(
				    rs->r_dependencies.dp_weak);
				ptr = (ptr2) ? ptr2 : VALUE_STR_NULL;
			} else {
				ptr = VALUE_STR_NULL;
			}
			print_line(margin + 2, labelw, prefix, label, ptr);
			if (ptr2)
				free(ptr2);

			/*
			 * Resource restart dependencies
			 */
			(void) sprintf(label, "%s %s:", LABEL_RS,
			    gettext("restart dependencies"));
			ptr2 = NULL;
			if (rs->r_dependencies.dp_restart) {
				ptr2 = rdep_list_to_string(
				    rs->r_dependencies.dp_restart);
				ptr = (ptr2) ? ptr2 : VALUE_STR_NULL;
			} else {
				ptr = VALUE_STR_NULL;
			}
			print_line(margin + 2, labelw, prefix, label, ptr);
			if (ptr2)
				free(ptr2);

			/*
			 * Offline resource restart dependencies
			 */
			(void) sprintf(label, "%s %s:", LABEL_RS,
			    gettext("offline restart dependencies"));
			ptr2 = NULL;
			if (rs->r_dependencies.dp_offline_restart) {
				ptr2 = rdep_list_to_string(
				    rs->r_dependencies.dp_offline_restart);
				ptr = (ptr2) ? ptr2 : VALUE_STR_NULL;
			} else {
				ptr = VALUE_STR_NULL;
			}
			print_line(margin + 2, labelw, prefix, label, ptr);
			if (ptr2)
				free(ptr2);


			/* Resource property list */
			show_rs_property_list(margin + 2, prefix,
			    0, rs->r_properties, rs);

			/* Resource extension property list */
			show_rs_property_list(margin + 2, prefix,
			    1, rs->r_ext_properties, rs);
		} /* if (print_level > SCRGADM_PRINT_MEDIUM) */
	} /* if (print_level > SCRGADM_PRINT_LOW) */
exit_err:
	if (rg)
		rgm_free_rg(rg);
	if (nodename)
		free(nodename);
}


/*
 * create_nodename_prefix()
 *
 * Takes current prefix and converts it into another prefix.
 *  new_prefix would be of the form prefix{nodename}.
 *
 * Output: new prefix.
 *
 */
static void
create_nodename_prefix(const char *prefix, const char *nodename,
    char *new_prefix)
{
	size_t i;

	if (prefix && *prefix) {
		(void) strcpy(new_prefix, prefix);
		i = strlen(new_prefix) - 1;
		if (new_prefix[i] == ')') {
			new_prefix[i] = '\0';
			(void) sprintf(new_prefix, "%s{%s})", new_prefix,
			    nodename);
		}
	}
} 
/*
 * show_rs_property_list()
 *
 * Walk a resource property list and show each resource property
 * Parameter rgm_resource_t *rs is added to get the resource details
 * which is required for the computation
 * of NRU from the dependency lists.
 *
 * No return value.
 */
static void
show_rs_property_list(const int margin, char *prefix,
    uint_t extension, rgm_property_list_t *list, rgm_resource_t *rs)
{
	rgm_property_list_t *listp;

	/* Check arguments */
	if (list == NULL)
		return;

	/* for each property in the list */
	for (listp = list;  listp;  listp = listp->rpl_next) {

		/* show property data */
		show_rs_property(margin, prefix, extension,
		    listp->rpl_property, rs);
	}
}

/*
 * show_rs_property()
 *
 * show property attached to a resource
 * rgm_resource_t *rs is added to get the resource details
 * which is required for the computation
 * of NRU from the dependency lists.
 *
 * No return value.
 */
static void
show_rs_property(const int margin, char *old_prefix, uint_t extension,
    rgm_property_t *rp, rgm_resource_t *rs)
{
	char *ptr, *ptr2;
	char label[BUFSIZ];
	char prefix[BUFSIZ];
	int labelw;

	scha_errmsg_t rgm_status = { SCHA_ERR_NOERR, NULL};
	char *value = NULL;

	/* Check arguments */
	if (rp == NULL || rp->rp_key == NULL)
		return;

	/* Set label width */
	labelw = LABELW2;

	/* Resource property name */
	(void) sprintf(label, "%s %s:", LABEL_RS_P, gettext("name"));
	ptr = (rp->rp_key && *rp->rp_key) ? rp->rp_key : VALUE_STR_NULL;
	print_line(margin, labelw, old_prefix, label, ptr);

	/* RE-set prefix */
	*prefix = '\0';
	if (old_prefix && *old_prefix) {
		size_t i;
		(void) strcpy(prefix, old_prefix);
		i = strlen(prefix) - 1;
		if (prefix[i] == ')')
			prefix[i] = '\0';
		(void) sprintf(label, ":%s)", rp->rp_key);
		(void) strcat(prefix, label);
	}

	/* Resource property class */
	(void) sprintf(label, "%s %s:", LABEL_RS_P, gettext("class"));
	ptr = (extension) ? "extension" : "standard";
	print_line(margin + 2, labelw, prefix, label, ptr);

	/* Resource property description */
	if (rp->rp_description) {
		(void) sprintf(label, "%s %s:", LABEL_RS_P,
		    gettext("description"));
		print_line(margin + 2, labelw, prefix, label,
		    rp->rp_description);
	}

	/* Resource property pernode */
	if (extension) {
		(void) sprintf(label, "%s %s:", LABEL_RS_P, gettext("pernode"));
		ptr = (rp->is_per_node) ? VALUE_STR_TRUE : VALUE_STR_FALSE;
		print_line(margin, labelw, prefix, label, ptr);
	}

	/* Resource property type and value(s) */
	switch (rp->rp_type) {
	case SCHA_PTYPE_STRING:

		/* type string */
		(void) sprintf(label, "%s %s:", LABEL_RS_P, gettext("type"));
		print_line(margin + 2, labelw, prefix, label, "string");

		/* string value */
		if (rp->is_per_node) {
			(void) sprintf(label, "%s %s:",
			    LABEL_RS_P, gettext("value"));
			show_rs_pernode_property(margin, labelw,
			    prefix, label, rp->rp_value);
		} else {
			(void) sprintf(label, "%s %s:",
			    LABEL_RS_P, gettext("value"));
			rgm_status = get_value(rp->rp_value, NULL,
			    &value, B_FALSE);
			if (rgm_status.err_code != SCHA_ERR_NOERR) {
				print_scha_error(rgm_status);
				goto exit_err;
			}

			print_line(margin + 2, labelw,
			    prefix, label, value);
		}
		break;

	case SCHA_PTYPE_INT:

		/* type int */
		(void) sprintf(label, "%s %s:", LABEL_RS_P, gettext("type"));
		print_line(margin + 2, labelw, prefix, label, "int");

		/* int value */
		if (rp->is_per_node) {
			(void) sprintf(label, "%s %s:",
			    LABEL_RS_P, gettext("value"));
			show_rs_pernode_property(margin, labelw,
			    prefix, label, rp->rp_value);
		} else {
			(void) sprintf(label, "%s %s:",
			    LABEL_RS_P, gettext("value"));
			rgm_status = get_value(rp->rp_value, NULL,
			    &value, B_FALSE);
			if (rgm_status.err_code != SCHA_ERR_NOERR) {
				print_scha_error(rgm_status);
				goto exit_err;
			}
			print_line(margin + 2, labelw,
			    prefix, label, value);
		}
		break;

	case SCHA_PTYPE_BOOLEAN:

		/* type boolean */
		(void) sprintf(label, "%s %s:", LABEL_RS_P, gettext("type"));
		print_line(margin + 2, labelw, prefix, label, "boolean");

		/* boolean value */
		if (rp->is_per_node) {
			(void) sprintf(label, "%s %s:",
			    LABEL_RS_P, gettext("value"));
			show_rs_pernode_property(margin, labelw,
			    prefix, label, rp->rp_value);
		} else {
			(void) sprintf(label, "%s %s:",
			    LABEL_RS_P, gettext("value"));
			rgm_status = get_value(rp->rp_value, NULL,
			    &value, B_FALSE);
			if (rgm_status.err_code != SCHA_ERR_NOERR) {
				print_scha_error(rgm_status);
				goto exit_err;
			}
			print_line(margin + 2, labelw,
			    prefix, label, value);
		}
		break;

	case SCHA_PTYPE_ENUM:

		/* type enum */
		(void) sprintf(label, "%s %s:",
		    LABEL_RS_P, gettext("type"));
		print_line(margin + 2, labelw, prefix, label, "enum");

		/* enum value */
		if (rp->is_per_node) {
			(void) sprintf(label, "%s %s:",
			    LABEL_RS_P, gettext("value"));
			show_rs_pernode_property(margin, labelw,
			    prefix, label, rp->rp_value);
		} else {
			(void) sprintf(label, "%s %s:",
			    LABEL_RS_P, gettext("value"));
			rgm_status = get_value(rp->rp_value, NULL,
			    &value, B_FALSE);
			if (rgm_status.err_code != SCHA_ERR_NOERR) {
				print_scha_error(rgm_status);
				goto exit_err;
			}
			print_line(margin + 2, labelw, prefix,
			    label, value);
		}
		break;

	case SCHA_PTYPE_STRINGARRAY:

		/* type stringarray */
		(void) sprintf(label, "%s %s:", LABEL_RS_P, gettext("type"));
		print_line(margin + 2, labelw, prefix, label, "stringarray");

		/* stringarray values */
		(void) sprintf(label, "%s %s:", LABEL_RS_P, gettext("value"));
		ptr2 = NULL;
		/*
		 * If NRU is not set then compute the NRU from the
		 * dependency list and display it
		*/
		if ((strcasecmp(SCHA_NETWORK_RESOURCES_USED, rp->rp_key)
		    == 0) && (rp->rp_array_values == NULL)) {
			compute_NRUlist_frm_deplist(rs,
			    &(rp->rp_array_values), NULL);
		}
		if (rp->rp_array_values) {
			ptr2 = namelist_to_string(rp->rp_array_values);
			ptr = (ptr2) ? ptr2 : VALUE_STR_NULL;
		} else {
			ptr = VALUE_STR_NULL;
		}
		print_line(margin + 2, labelw, prefix, label, ptr);
		if (ptr2)
			free(ptr2);

		break;

	default:

		/* unknown */
		(void) sprintf(label, "%s %s:", LABEL_RS_P, gettext("type"));
		ptr = VALUE_STR_UNKNOWN;
		print_line(margin + 2, labelw, prefix, label, ptr);

		break;
	} /* lint !e788 */
exit_err:
	if (value)
		free(value);
}

/*
 * show_rs_pernode_property()
 *
 * show per node properties of a resource.
 *
 * No return value.
 */
static void
show_rs_pernode_property(const int margin, int labelw, char *prefix,
    char *label, value_t pernode_value) {

	scha_errmsg_t rgm_status = {SCHA_ERR_NOERR, NULL};
	char lasts[BUFSIZ];
	char *lasts_p = &lasts[0];
	char new_prefix[BUFSIZ];
	char *tmp_prefix = NULL;
	char *ptr = NULL;
	char *rg_name = NULL;
	rgm_rg_t *rg = (rgm_rg_t *)0;

	char *nodename = NULL;
	nodeidlist_t *nodelist, *nl;
	char *value = NULL;

	/* Get the RG */
	tmp_prefix = strdup(prefix);
	if (tmp_prefix == NULL) {
		rgm_status.err_code = SCHA_ERR_NOMEM;
		print_scha_error(rgm_status);
		goto exit_err;
	}
	rg_name = (char *)strtok_r(tmp_prefix + 1, ":",
	    (char **)&lasts_p);

	/* Get the nodelist for the RG */
	rgm_status = rgm_scrgadm_getrgconf(rg_name, &rg, NULL);
	if (rgm_status.err_code != SCHA_ERR_NOERR) {
		print_scha_error(rgm_status);
		goto exit_err;
	}

	nodelist = rg->rg_nodelist;
	for (nl = nodelist; nl; nl = nl->nl_next) {
		/*
		 * Change the current prefix to new-prefix
		 * of the form new-prefix = prefix{nodename}
		 */
		nodename = scrgadm_nodeidzone_to_nodename(nl->nl_nodeid,
		    nl->nl_zonename);
		rgm_status = get_value(pernode_value, nodename, &value, B_TRUE);
		if (rgm_status.err_code != SCHA_ERR_NOERR) {
			print_scha_error(rgm_status);
			goto exit_err;
		}
		create_nodename_prefix(prefix, nodename, new_prefix);
		ptr = (value) ?(value) : VALUE_STR_NULL;
		print_line(margin + 2, labelw, new_prefix,
		    label, ptr);

		/* Free the value. */
		if (value)
			free(value);
	}
exit_err:
	if (tmp_prefix)
		free(tmp_prefix);
	if (rg)
		rgm_free_rg(rg);
}


/*
 *
 * manage_rs()
 *
 * Manage Resource by specified action.
 *
 * Return values are of type scha_errmsg_t
 *	generated by librgm(d)
 *	(refer to /usr/cluster/include/scha_err.h)
 */
static scha_errmsg_t
manage_rs(const scrgadm_action_t action, const scrgadm_name_t rs_name,
    const scrgadm_name_t rt_name, const	scrgadm_name_t rg_name,
    const scrgadm_name_valuel_list_t *lprops,
    const scrgadm_name_valuel_list_t *lxprops,
    boolean_t bypass_install_mode, boolean_t verbose_flag)
{
	scha_errmsg_t status = {SCHA_ERR_NOERR, NULL};
	switch (action) {
	case SCRGADM_ACTION_ADD:
		status = create_rs(rs_name, rt_name, rg_name,
		    lprops, lxprops, bypass_install_mode);
		break;
	case SCRGADM_ACTION_CHANGE:
		status = modify_rs(rs_name, lprops, lxprops);
		break;
	case SCRGADM_ACTION_REMOVE:
		status = remove_rs(rs_name, verbose_flag);
		break;
	default:
		status.err_code = SCHA_ERR_INVAL;
		break;
	}
	return (status);
} /* manage_rs */


/*
 * create_rs()
 *
 * Create Resource by with specified properties.
 *
 * Possible return values:
 *	SCHA_ERR_NOERR
 *	SCHA_ERR_NOMEM
 */
static scha_errmsg_t
create_rs(const scrgadm_name_t rs_name,
    const scrgadm_name_t rt_name,
    const scrgadm_name_t rg_name,
    const scrgadm_name_valuel_list_t *lprops,
    const scrgadm_name_valuel_list_t *lxprops,
    boolean_t bypass_install_mode) {

	scha_errmsg_t status = {SCHA_ERR_NOERR, NULL};
	scha_property_t *plp = NULL;
	scha_property_t *plx = NULL;
	scha_resource_properties_t rpl;

	/* create array of resource properties from props list */
	plp = create_property_array(lprops);
	plx = create_property_array(lxprops);
	if (plp && plx) {
		rpl.srp_predefined = &plp;
		rpl.srp_extension = &plx;
		/* create resource */
		status = rgm_scrgadm_add_resource(
		    rs_name, rt_name, rg_name, &rpl, bypass_install_mode, NULL);
	} else { /* array create failed */
		status.err_code = SCHA_ERR_NOMEM;
	}
	if (plx)
		free_property_array(&plx);
	if (plp)
		free_property_array(&plp);

	return (status);
} /* create_rs */


/*
 * modify_rs()
 *
 * Modify Resource by with specified properties.
 *
 * Possible return values:
 *	SCHA_ERR_NOERR
 *	SCHA_ERR_NOMEM
 */
static scha_errmsg_t
modify_rs(const scrgadm_name_t rs_name,
    const scrgadm_name_valuel_list_t *lprops,
    const scrgadm_name_valuel_list_t *lxprops) {

	scha_errmsg_t status = {SCHA_ERR_NOERR, NULL};
	scha_resource_properties_t rpl;
	scha_property_t *plp = NULL;
	scha_property_t *plx = NULL;

	if (status.err_code == SCHA_ERR_NOERR) {
		/* create resource arrays from props lists */
		plp = create_property_array(lprops);
		plx = create_property_array(lxprops);
		if (plp && plx) {
			rpl.srp_predefined = &plp;
			rpl.srp_extension = &plx;
			status = rgm_scrgadm_update_resource_property(
			    rs_name, &rpl, NULL);
		} else { /* array create failed */
			status.err_code = SCHA_ERR_NOMEM;
		}
	}

	if (plp)
		free_property_array(&plp);
	if (plx)
		free_property_array(&plx);

	return (status);
} /* modify_rs */


/*
 * remove_rs()
 *
 * Remove Resource.
 *
 * Possible return values:
 *	SCHA_ERR_NOERR
 */
static scha_errmsg_t
remove_rs(const scrgadm_name_t rs_name, boolean_t verbose_flag)
{
	scha_errmsg_t status = {SCHA_ERR_NOERR, NULL};

	status = rgm_scrgadm_delete_resource(rs_name, verbose_flag, NULL);

	return (status);
} /* remove_rs */



/*
 *
 *	*****  Logical Host (LH) functions  *****
 *
 */

/*
 * manage_lh()
 *
 * Manage Logical Host by specified action.
 *
 * Return values are of type scha_errmsg_t
 *	generated by librgm(d)
 *	(refer to /usr/cluster/include/scha_err.h)
 */
static scha_errmsg_t
manage_lh(
    const scrgadm_action_t action,
    const scrgadm_name_t rg_name,
    const scrgadm_name_t rs_name,
    scrgadm_hostname_list_t *lhnames,
    scrgadm_node_ipmp_list_t *lipmp,
    scrgadm_name_valuel_list_t *lprops,
    boolean_t bypass_install_mode) {

	scha_errmsg_t status = {SCHA_ERR_NOERR, NULL};

	switch (action) {
	case SCRGADM_ACTION_ADD:
		status = create_lhrs(rg_name, rs_name, lhnames,
		    lipmp, lprops, bypass_install_mode);
		break;
	default:
		status.err_code = SCHA_ERR_INVAL;
		break;
	}
	return (status);
} /* manage_lh */


/*
 * create_lhrs()
 *
 * Inputs: name for LogicalHost RG
 *	list of hostnames
 *	list of netadapter@nodeidentifier's
 *
 * Action:
 *	validate the netadapter list entries
 *	validate the hostnames list
 *	create a RS with appropriate properties
 *	and add it to the named RG
 *
 * Possible return values:
 *	SCHA_ERR_NOERR
 *	SCHA_ERR_NOMEM
 */
static scha_errmsg_t
create_lhrs(const scrgadm_name_t rg_name,
    const scrgadm_name_t rs_name,
    scrgadm_hostname_list_t *lhnames,
    scrgadm_node_ipmp_list_t *lipmp,
    scrgadm_name_valuel_list_t *lprops,
    boolean_t bypass_install_mode) {

	scha_errmsg_t			status = {SCHA_ERR_NOERR, NULL};
	scrgadm_name_valuel_list_t	*lh_xprops = NULL;
	scrgadm_adapterinfo_list_t	*ailist = NULL;
	scrgadm_name_t lhrs_name;
	scconf_nodetype_t rg_nodetype = SCCONF_NODE_CLUSTER;
	scrgadm_name_valuel_list_t	*new_l = NULL;
	namelist_t			*new_nl = NULL;
	scrgadm_name_valuel_list_t	*tmp_l = NULL;
	rg_ip_type_t iptyp;
	boolean_t gz_ov_found;
	char errbuff[BUFSIZ];

	/* Check for Farm RGs. */
	if (iseuropa) {
		status = get_rg_nodetype(rg_name, &rg_nodetype, NULL);
		if (status.err_code != SCHA_ERR_NOERR)
			goto clean_up;
	}
	if (rg_nodetype == SCCONF_NODE_FARM) {
		status = validate_farmnodelist(rg_name);
	} else {
		status = validate_nodelist(rg_name, NULL);
	}

	if (status.err_code != SCHA_ERR_NOERR) {
		goto clean_up;
	}

	iptyp = get_ip_type(rg_name, NULL);
	if (iptyp == IP_TYPE_MIX) {
		status.err_code = SCHA_ERR_INVAL;
		(void) sprintf(errbuff, gettext(
		    "The resource group nodelist for the "
		    "logical hostname resource cannot "
		    "contain a mix of non-global zones of ip-type=exclusive "
		    "with either non-global zones of ip-type=shared or "
		    "global zones.\n"));
		status.err_msg = strdup(errbuff);
		goto clean_up;
	} else if (iptyp == IP_TYPE_UNKNOWN) {
		(void) sprintf(errbuff, gettext(
		    "Failed to verify that the resource "
		    "group nodelist for the logical hostname "
		    "resource does not contain a mix "
		    "of non-global zones of ip-type=exclusive with either "
		    "non-global zones of ip-type=shared or global zones.\n"));
		status.err_msg = strdup(errbuff);
	}

	/*
	 * If ip-type exclusive, make sure global_zone_override is
	 * set to false for LH resources.
	 */
	if (iptyp == IP_TYPE_EXCL) {
		gz_ov_found = B_FALSE;
		for (tmp_l = lprops; tmp_l; tmp_l = tmp_l->next) {
			if (strcasecmp(tmp_l->name, "Global_zone_override")
			    == 0) {
				gz_ov_found = B_TRUE;
				break;
			}
		}
		if (!gz_ov_found) {
			new_l = (scrgadm_name_valuel_list_t *)
			    calloc(1, sizeof (scrgadm_name_valuel_list_t));
			if (!new_l) {
				status.err_code = SCHA_ERR_NOMEM;
				goto clean_up;
			}
			new_l->name = strdup("Global_zone_override");
			if (!new_l->name) {
				status.err_code = SCHA_ERR_NOMEM;
				goto clean_up;
			}
			new_nl = (namelist_t *)calloc(1, sizeof (namelist_t));
			if (!new_nl) {
				status.err_code = SCHA_ERR_NOMEM;
				goto clean_up;
			}
			new_nl->nl_name = strdup("false");
			if (!new_nl->nl_name) {
				status.err_code = SCHA_ERR_NOMEM;
				goto clean_up;
			}
			new_nl->nl_next = NULL;
			new_l->value_list = new_nl;
			new_l->node_list = NULL;
			new_l->is_per_node = B_FALSE;
			new_l->next = lprops;
			lprops = new_l;
		}

	}

	status = process_netadapter_list(rg_name, lhnames, lipmp, &ailist,
					NULL);
	if (status.err_code != SCHA_ERR_NOERR) {
		goto clean_up;
	}

	status = convert_adapterinfo_to_ipmp(ailist, &lipmp, lhnames, NULL);
	if (status.err_code != SCHA_ERR_NOERR) {
		goto clean_up;
	}

	status = set_lh_xprops(&lh_xprops, lhnames, lipmp);
	if (status.err_code != SCHA_ERR_NOERR) {
		goto clean_up;
	}

	status = validate_unique_haip(lhnames);
	if (status.err_code != SCHA_ERR_NOERR) {
		goto clean_up;
	}

	lhrs_name = (rs_name) ? rs_name : lhnames->raw_name;
	status = create_rs(lhrs_name,
			get_latest_rtname(SCRGADM_RTLH_NAME, NULL),
	    rg_name, lprops, lh_xprops, bypass_install_mode);

clean_up:

	free_name_valuel_list(&lh_xprops);
	free_adapterinfo_list(ailist);
	return (status);
} /* create_lhrs */



/*
 * set_lh_xprops()
 *
 * Input: list of hostnames and list of ipmp information
 *
 * Action: Set required properties: NetIfList, HostnameList
 *
 * Output:
 *	lh_xprops_pp points to scrgadm_name_valuel_list_t
 *	which holds required properties with values set
 *
 * Caller must free lh_xprops_pp after use.
 *
 * Possible return values:
 *	SCHA_ERR_NOERR
 *	SCHA_ERR_INVAL
 *	SCHA_ERR_NOMEM
 *
 */
static scha_errmsg_t
set_lh_xprops(scrgadm_name_valuel_list_t **lh_xprops_pp,
    const scrgadm_hostname_list_t *hostnames,
    const scrgadm_node_ipmp_list_t *ipmps) {

	scha_errmsg_t status = {SCHA_ERR_NOERR, NULL};

	char *netif_list = NULL;
	char *hostname_list = NULL;

	char *rqd_props[3] = {
		SCRGADM_NETIFLIST,
		SCRGADM_HOSTNAMELIST,
		(char *)0	/* special end-of-list value */
	};
	char *values[3] = {
		"",
		"",
		(char *)0	/* special end-of-list value */
	};

	/* can't use non-constant as initializer so... */

	/* construct comma list of netif_list values */
	status = create_netiflist_val(ipmps, &netif_list);
	if (status.err_code != SCHA_ERR_NOERR) {
		goto clean_up;
	}
	values[0] = netif_list;

	/* construct comma list of hostname_list values */
	status = create_hnlist_val(hostnames, &hostname_list);
	if (status.err_code != SCHA_ERR_NOERR) {
		goto clean_up;
	}
	values[1] = hostname_list;

	status = set_props(lh_xprops_pp, &rqd_props[0], &values[0]);

clean_up:
	if (netif_list) {
		free(netif_list);
		netif_list = NULL;
	}
	if (hostname_list) {
		free(hostname_list);
		hostname_list = NULL;
	}	/* nothing special to do */
	return (status);
} /* set_lh_xprops */


/*
 *
 *	*****  Shared Address (SA) functions  *****
 *
 */


/*
 *
 * manage_sa()
 *
 * Manage Shared Address by specified action.
 *
 * Return values are of type scha_errmsg_t
 *	generated by librgm(d)
 *	(refer to /usr/cluster/include/scha_err.h)
 */
static scha_errmsg_t
manage_sa(
    const scrgadm_action_t action,
    const scrgadm_name_t rg_name,
    const scrgadm_name_t rs_name,
    scrgadm_hostname_list_t *lhnames,
    scrgadm_node_ipmp_list_t *lipmp,
    namelist_t *laux,
    scrgadm_name_valuel_list_t *lprops,
    boolean_t bypass_install_mode) {

	scha_errmsg_t status = {SCHA_ERR_NOERR, NULL};

	switch (action) {
	case SCRGADM_ACTION_ADD:
		status = create_sars(rg_name, rs_name, lhnames, lipmp,
		    laux, lprops, bypass_install_mode);
		break;
	default:
		status.err_code = SCHA_ERR_INVAL;
		break;
	}
	return (status);
} /* manage_sa */


/*
 * set_sa_xprops()
 *
 * Input: list of hostnames and list of ipmp information
 *
 * Action: Set required properties: NetIfList, HostnameList,
 *	optional AuxNodeList
 *
 * Output:
 *	lh_xprops_pp points to scrgadm_name_valuel_list_t
 *	which holds required properties with values set
 *
 * Caller must free lh_xprops_pp after use.
 *
 * Possible return values:
 *	SCHA_ERR_NOERR
 *	SCHA_ERR_INVAL
 *	SCHA_ERR_NOMEM
 */
static scha_errmsg_t
set_sa_xprops(scrgadm_name_valuel_list_t **lh_xprops_pp,
    const scrgadm_hostname_list_t *hostnames,
    const scrgadm_node_ipmp_list_t *ipmps,
    const namelist_t *auxnodes) {

	scha_errmsg_t status = {SCHA_ERR_NOERR, NULL};
	char *netif_list = NULL;
	char *hostname_list = NULL;
	char *auxnode_list = NULL;

	char *rqd_props[4] = {
		SCRGADM_NETIFLIST,
		SCRGADM_HOSTNAMELIST,
		(char *)0,	/* might overwrite for auxnode */
		(char *)0	/* special end-of-list value */
	};
	char *values[4] = {
		"", "",		/* will overwrite these in a moment */
		(char *)0,	/* might overwrite for auxnode */
		(char *)0	/* special end-of-list value */
	};

	/* can't use non-constant as initializer so... */

	/* construct comma list of netif_list values */
	status = create_netiflist_val(ipmps, &netif_list);
	if (status.err_code != SCHA_ERR_NOERR) {
		goto exit_err;
	}
	values[0] = netif_list;

	/* construct comma list of hostname_list values */
	status = create_hnlist_val(hostnames, &hostname_list);
	if (status.err_code != SCHA_ERR_NOERR) {
		goto exit_err;
	}
	values[1] = hostname_list;

	/* construct comma list of auxnode values */
	if (auxnodes) {
		status = create_auxnodelist_val(auxnodes, &auxnode_list);
		if (status.err_code != SCHA_ERR_NOERR) {
			goto exit_err;
		}
		rqd_props[2] = SCRGADM_AUXNODELIST;
		values[2] = auxnode_list;
	}

	status = set_props(lh_xprops_pp, &rqd_props[0], &values[0]);

	return (status);
exit_err:
	/* nothing special to do */
	return (status);
} /* set_sa_xprops */


/*
 * create_sars()
 *
 *
 * Inputs: name for SharedAddress RG
 *	list of hostnames
 *	list of netadapter@nodeidentifier's
 *
 * Action:
 *	validate the netadapter list entries
 *	validate the hostnames list
 *	create a RS with appropriate properties
 *	and add it to the named RG
 *
 * Possible return values:
 *	SCHA_ERR_NOERR
 *	SCHA_ERR_NOMEM
 */
static scha_errmsg_t
create_sars(const scrgadm_name_t rg_name,
    const scrgadm_name_t rs_name,
    scrgadm_hostname_list_t *lhnames,
    scrgadm_node_ipmp_list_t *lipmp,
    namelist_t *laux,
    scrgadm_name_valuel_list_t *lprops,
    boolean_t bypass_install_mode) {


	scha_errmsg_t status = {SCHA_ERR_NOERR, NULL};
	scrgadm_name_valuel_list_t *sa_xprops = NULL;
	scrgadm_adapterinfo_list_t *ailist = NULL;
	scrgadm_name_t lhrs_name;
	rg_ip_type_t iptyp;
	char errbuff[BUFSIZ];

	iptyp = get_ip_type(rg_name, NULL);
	if (iptyp == IP_TYPE_UNKNOWN) {
		(void) sprintf(errbuff, gettext(
		    "Failed to verify that the resource "
		    "group nodelist for the shared address "
		    "resource does not contain any "
		    "non-global zone of ip-type=exclusive.\n"));
		status.err_msg = strdup(errbuff);
	} else if (iptyp != IP_TYPE_SHARED) {
		status.err_code = SCHA_ERR_INVAL;
		(void) sprintf(errbuff, gettext(
		    "The resource group nodelist for the "
		    "shared address resource cannot "
		    "contain any non-global zone of ip-type=exclusive.\n"));
		status.err_msg = strdup(errbuff);
		goto clean_up;
	}

	status = validate_nodelist(rg_name, NULL);
	if (status.err_code != SCHA_ERR_NOERR) {
		goto clean_up;
	}

	status = process_netadapter_list(rg_name, lhnames, lipmp, &ailist,
					NULL);
	if (status.err_code != SCHA_ERR_NOERR) {
		goto clean_up;
	}

	status = convert_adapterinfo_to_ipmp(ailist, &lipmp, lhnames, NULL);
	if (status.err_code != SCHA_ERR_NOERR) {
		goto clean_up;
	}

	if (laux) {
		status = process_namelist_to_nodeids(laux);
		if (status.err_code != SCHA_ERR_NOERR) {
			goto clean_up;
		}
		status = validate_auxnodelist(ailist, laux);
		if (status.err_code != SCHA_ERR_NOERR) {
			goto clean_up;
		}
	}

	status = set_sa_xprops(&sa_xprops, lhnames, lipmp, laux);
	if (status.err_code != SCHA_ERR_NOERR) {
		goto clean_up;
	}

	status = validate_unique_haip(lhnames);
	if (status.err_code != SCHA_ERR_NOERR) {
		goto clean_up;
	}

	lhrs_name = (rs_name) ? rs_name : lhnames->raw_name;
	status = create_rs(lhrs_name,
		get_latest_rtname(SCRGADM_RTSA_NAME, NULL),
	    rg_name, lprops, sa_xprops, bypass_install_mode);

clean_up:

	free_name_valuel_list(&sa_xprops);
	free_adapterinfo_list(ailist);
	return (status);
} /* create_sars */



/*
 *
 *	*****  Utility and shared functions  *****
 *
 */



/*
 * commalist_to_hostnamelist()
 *
 * Input:
 *	char * that's expected to contain a comma-separated list of
 *	hostname or dotted IP entries.
 *
 * Action: Parse input list of names; create scrgadm_hostname_list
 *
 * Output: ptr to scrgadm_hostname_list
 *
 * Possible return values:
 *	SCHA_ERR_NOERR
 *	SCHA_ERR_NOMEM
 *
 */
static scha_errmsg_t
commalist_to_hostnamelist(char *optarg_str, scrgadm_hostname_list_t **lp)
{
	scha_errmsg_t status = {SCHA_ERR_NOERR, NULL};

	scrgadm_hostname_list_t *head = NULL;
	scrgadm_hostname_list_t *tail = NULL;
	scrgadm_hostname_list_t *elt = NULL;
	char *raw_input = NULL;

	char lasts[BUFSIZ];
	char *lasts_p = &lasts[0];

	/*
	 * for each elt in comma list
	 * make new scrgadm_hostname_list struct
	 * fill in raw_name and attach to list
	 */

	raw_input = (char *)strtok_r(optarg_str, ",",
	    (char **)&lasts_p);

	while (raw_input != NULL) {
		/*
		 * malloc a new scrgadm_hostname_list struct
		 */
		elt = (scrgadm_hostname_list_t *)calloc(1,
		    sizeof (scrgadm_hostname_list_t));

		if (elt) {
			elt->raw_name = strdup(raw_input);
			elt->next = NULL;
			elt->ip_addr = NULL;
			elt->ip6_addr = NULL;
		}

		if (!elt->raw_name) {
			status.err_code = SCHA_ERR_NOMEM;
			goto exit_err;
		}

		if (head == NULL) {
			head = elt;
		}
		if (tail) {
			tail->next = elt;
		}
		tail = elt;
		elt = NULL;

		/* get next elt from comma list */
		raw_input = (char *)strtok_r(NULL, ",", (char **)&lasts_p);
	} /* end while */

	/*
	 * now give new list to caller
	 */
	*lp = head;
	return (status);

exit_err:
	if ((elt != NULL) && (head == NULL)) {
		free_hostname_list(elt);
	}
	if (head) {
		free_hostname_list(head);
	}
	return (status);

} /* commalist_to_hostnamelist */


/*
 * commalist_to_namelist()
 *
 * Input: char * that's expected to contain a comma-separated list of names.
 *
 * Action: Parse input list of names and create namelist_t
 *
 * Output: ptr to namelist_t
 *
 * Possible return values:
 *	SCHA_ERR_NOERR
 *	SCHA_ERR_NOMEM
 *
 */
static scha_errmsg_t
commalist_to_namelist(char *optarg_str, namelist_t **lp)
{
	scha_errmsg_t status = {SCHA_ERR_NOERR, NULL};
	namelist_t *head = NULL;
	namelist_t *tail = NULL;
	namelist_t *elt = NULL;
	scrgadm_name_t name = NULL;

	char lasts[BUFSIZ];
	char *lasts_p = &lasts[0];

	/*
	 * for each elt in comma list
	 * make new namelist_t struct
	 * fill in name field and
	 * attach to list
	 */

	name = (char *)strtok_r(optarg_str, ",",
	    (char **)&lasts_p);
	while (name != NULL) {
		/*
		 * malloc a new namelist_t struct
		 * and space for name
		 */
		elt = (namelist_t *)calloc(1,
		    sizeof (namelist_t));

		if (elt) {
			/*
			 * Check for "synthetic" node names - not allowed
			 */
			if ((strcmp(name, SCHA_RTSYS_TRUE) == 0) ||
			    (strcmp(name, SCHA_RTSYS_FALSE) == 0)) {
				status.err_code = SCHA_ERR_INVAL;
				goto exit_err;
			}
			elt->nl_name = strdup(name);
			elt->nl_next = NULL;
		}

		if (!elt || !elt->nl_name) {
			status.err_code = SCHA_ERR_NOMEM;
			goto exit_err;
		}

		if (head == NULL) {
			head = elt;
		}
		if (tail) {
			tail->nl_next = elt;
		}
		tail = elt;
		elt = NULL;

		/* get next elt from comma list */
		name = (char *)strtok_r(NULL, ",", (char **)&lasts_p);
	} /* end while */

	/*
	 * now give new list to caller
	 */
	*lp = head;
	return (status);

exit_err:
	if ((elt != NULL) && ((head == NULL) ||
	    (elt->nl_name == NULL))) {
		/* 2 cases where elt not yet attached to list */
		free_namelist(elt);
	}
	if (head) {
		free_namelist(head);
	}
	return (status);

} /* commalist_to_namelist */


/*
 * commalist_to_node_ipmp_list()
 *
 * Input:
 *	char * that's expected to contain a comma-separated list of
 *	netif@nodeidentifier entries. 'netif' may be a true netif
 *	or 'ipmpN.'
 *
 * Action: Parse input list of names; create scrgadm_node_ipmp_list
 *
 * Output: ptr to scrgadm_node_ipmp_list_t
 *
 * Possible return values:
 *	SCHA_ERR_NOERR
 *	SCHA_ERR_NOMEM
 *	SCHA_ERR_INVAL
 *
 */
static scha_errmsg_t
commalist_to_node_ipmp_list(char *optarg_str,
    scrgadm_node_ipmp_list_t **lp)
{
	scha_errmsg_t status = {SCHA_ERR_NOERR, NULL};

	scrgadm_node_ipmp_list_t *head = NULL;
	scrgadm_node_ipmp_list_t *tail = NULL;
	scrgadm_node_ipmp_list_t *elt = NULL;
	char *raw_input = NULL;

	char lasts[BUFSIZ];
	char *lasts_p = &lasts[0];

	/*
	 * for each elt in comma list
	 * make new scrgadm_node_ipmp_list_t struct
	 * parse input into raw_node & raw_netid portions
	 * fill in elt and attach to list
	 */

	raw_input = (char *)strtok_r(optarg_str, ",",
	    (char **)&lasts_p);
	while (raw_input != NULL) {
		/*
		 * malloc a new scrgadm_node_ipmp_list_t struct
		 */
		elt = (scrgadm_node_ipmp_list_t *)calloc(1,
		    sizeof (scrgadm_node_ipmp_list_t));

		if (elt) {
			status = load_node_ipmp_elt(elt, raw_input);
			elt->next = NULL;
		}

		if (status.err_code != SCHA_ERR_NOERR) {
			goto exit_err;
		}

		if (head == NULL) {
			head = elt;
		}
		if (tail) {
			tail->next = elt;
		}
		tail = elt;
		elt = NULL;

		/* get next elt from comma list */
		raw_input = (char *)strtok_r(NULL, ",", (char **)&lasts_p);
	} /* end while */

	/*
	 * now give new list to caller
	 */
	*lp = head;
	return (status);

exit_err:
	if ((elt != NULL) && (head == NULL)) {
		free_node_ipmp_list(elt);
	}
	if (head) {
		free_node_ipmp_list(head);
	}
	return (status);

} /* commalist_to_node_ipmp_list */

/*
 * create_name_array()
 *
 * Create an array of names from namelist_t.
 *
 * Only caller are modify_rt() and create_rt()
 *
 * Caller must free memory.
 *
 * Return a pointer or NULL if malloc fails.
 *
 */
static char **
create_name_array(const namelist_t *list) {
	/*
	 * code adapted from librgm_utils.rgm_datatype.nstr2strarray()
	 */
	char **sa = 0;
	namelist_t *lp;
	size_t i = 0;

	if (list == NULL)
		return ((char **)NULL);

	for (lp = (namelist_t *)list; lp != NULL; lp = lp->nl_next) {
		char **ptr = (char **)realloc(sa, (i + 2) * (sizeof (char *)));
		if (ptr == (char **)NULL)
			return (ptr);
		ptr[i] = strdup(lp->nl_name);
		sa = ptr;
		i++;
	}
	if (sa != (char **)0)
		sa[i] = (char *)NULL;

	return (sa);
} /* create_name_array */


/*
 * create_property_array()
 *
 * Convert a name-value pair list to a pointer to a property array.
 *
 * Return an array of name_value_list nodes
 * each node field (name, value_list) simply points back into the
 * source list
 *
 * Return a pointer or NULL if either malloc fails.
 *
 */
static scha_property_t *
create_property_array(const scrgadm_name_valuel_list_t *list)
{
	scrgadm_name_valuel_list_t *ptr = (scrgadm_name_valuel_list_t *)list;
	int count = get_name_value_count(list);
	int i = 0;
	scha_property_t *property_array = NULL;

	/*
	 * XXX  this will be simplified to a list of scha_property_t structs
	 * instead of an array sometime "real soon"
	 */

	/* allocate storage for array */
	property_array = (scha_property_t *)calloc(
	    (size_t)(count + 1), sizeof (scha_property_t));
	if (property_array) {
		/* exit_err strategy not needed */
		for (i = 0; i < count; i++) {
			property_array[i].sp_name = (char *)ptr->name;
			property_array[i].sp_value = ptr->value_list;
			property_array[i].sp_nodes = ptr->node_list;
			property_array[i].is_per_node = ptr->is_per_node;
			ptr = ptr->next;
		}
		/* magic value that librgm looks for */
		property_array[count].sp_name = NULL;
	} /* if malloc'd */

	return (property_array);
}	/* create_property_array */


/*
 * find_list_prop()
 *
 * Search property list for given name.
 *
 * Return pointer to list node if
 * name is found
 *   else
 * return null.
 *
 */
static scrgadm_name_valuel_list_t *
find_list_prop(char *name, scrgadm_name_valuel_list_t **lprops_p) {
	scrgadm_name_valuel_list_t *ptr = *lprops_p;
	int matched = 0;

	while (ptr != NULL) {
		if (strcasecmp(ptr->name, name) == 0) {
			matched = 1;
			break;
		}
		ptr = ptr->next;
	} /* while */
	if (matched != 0) {
		return (ptr);
	} else {
		return ((scrgadm_name_valuel_list_t *)NULL);
	}
}	/* find_list_prop */


/*
 * get_name_value_count()
 *
 * Get count of name-value pairs in a name list.
 *
 * Return the count as an integer.
 *
 */
static int
get_name_value_count(const scrgadm_name_valuel_list_t *list)
{
	int i = 0;
	scrgadm_name_valuel_list_t *item = (scrgadm_name_valuel_list_t *)list;

	while (item != NULL) {
		item = item->next;
		i++;
	}
	return (i);
} /* get_name_value_count */

/*
 * namelist_to_nvl_list()
 *
 * Input: char *, namelist_t
 *
 * Action:
 *	construct a scrgadm_name_valuel_list_t
 *	  where name = passed-in char *
 *	  and value_list is the passed-in namelist_t
 *
 * Output: scrgadm_name_valuel_list_t
 *
 * Caller must call free_nvl_limited() after use;
 *	do NOT call free_name_valuel_list()!!
 *
 * Returns:
 *	SCHA_ERR_NOERR
 *	SCHA_ERR_NOMEM
 */
static scha_errmsg_t
namelist_to_nvl_list(char *name, namelist_t *name_list,
    scrgadm_name_valuel_list_t **nvl_p) {
	scha_errmsg_t status = {SCHA_ERR_NOERR, NULL};

	scrgadm_name_valuel_list_t *nvl = NULL;
	nvl = (scrgadm_name_valuel_list_t *)
		calloc(1, sizeof (scrgadm_name_valuel_list_t));
	/* and the name */
	nvl->name = strdup(name);
	nvl->value_list = name_list;
	nvl->node_list = NULL;
	nvl->is_per_node = B_FALSE;
	nvl->next = NULL;

	*nvl_p = nvl;

	return (status);

} /* namelist_to_nvl_list */


/*
 * parse_prop()
 *
 * Parse property or extension property for RG and RS.
 *
 * Add property to passed-in list.
 *
 * If property is already in list then
 *  its prior value is overwritten for properties
 *
 * If the property value is already in the list, its
 * prior value is overwritten.
 *
 * For per-node extension property, the property value is not overwritten
 * for property present in the list. Instead the property value is
 * appended to the list. These properties can take different values
 * on different nodes. These properties can be defined only for extension
 * properties which are defined as PER_NODE in the rtr file.
 *
 * This fnx yields the behavior that if the same property is defined
 * multiple times then the last-encountered value is used. Note that
 * by deleting the 'else' clause in this fnx that the first-encountered
 * value would be used and subsequent values ignored.
 *
 * Input is of the form: name=value or name{nodelist}=value
 *	where value may be a list of comma separated names and
 *	nodelist the list of comma separated node names.
 *
 * value is stored as scrgadm_value_list_t
 *
 * Possible return values:
 * 	SCHA_ERR_NOERR
 *	SCHA_ERR_INVAL
 *	SCHA_ERR_NOMEM
 *
 */
static scha_errmsg_t
parse_prop(char *optarg_str, scrgadm_name_valuel_list_t **list_p,
    boolean_t extension, boolean_t is_change_mode, char *rt_name_or_rs_name)
{
	scrgadm_value_list_t *old_value_list;
	scha_errmsg_t status = {SCHA_ERR_NOERR, NULL};
	char *equ_sign = NULL;
	char *name = NULL;
	char *raw_value = NULL;
	boolean_t is_per_node = B_FALSE;

	scrgadm_name_valuel_list_t *prop_p = NULL;
	scrgadm_value_list_t *value_list = NULL;
	scrgadm_node_list_t *node_list = NULL;

	/*
	 * Validate the contents of optarg:
	 * the minimally expected form is NAME=VALUE
	 */

	name = optarg_str;
	equ_sign = strchr(optarg_str, '=');
	if (equ_sign == NULL) {
		status.err_code = SCHA_ERR_INVAL;
		goto exit_err;
	} else {
		*equ_sign = '\0';
		raw_value = equ_sign + 1;
	}

	if (extension == B_TRUE) {
		status = parse_prop_name(name, &node_list);
		if (status.err_code != SCHA_ERR_NOERR)
			goto exit_err;

		/*
		 * Get the extension property type(per-node or not).
		 * Return error if the node_list is not empty for
		 * non per-node extension property.
		 */
		status = scrgadm_is_per_node_prop(name, is_change_mode,
		    rt_name_or_rs_name, &is_per_node);
		if (status.err_code != SCHA_ERR_NOERR)
			goto exit_err;
		if ((is_per_node == B_FALSE) && (node_list != NULL)) {
			status.err_code = SCHA_ERR_INVAL;
			goto exit_err;
		}
	}
	/*
	 * look for 'name' already in main list
	 * create value_list from raw_value
	 */
	if (is_per_node == B_FALSE)
		prop_p = find_list_prop(name, list_p);

	if (strlen(raw_value) == 0) {
		value_list = create_empty_valuelist();
	} else {
		/*
		 * rg_desciption and r_description property value can
		 * contain comma, so we should ignore checking for comma.
		 */
		if (strcasecmp(name, SCHA_RG_DESCRIPTION) == 0 ||
		    strcasecmp(name, SCHA_R_DESCRIPTION) == 0)
			value_list = parse_raw_value(raw_value, B_TRUE);
		else
			value_list = parse_raw_value(raw_value, B_FALSE);
	}

	if (!value_list) {
		status.err_code = SCHA_ERR_NOMEM;
		goto exit_err;
	} /* early exit */

	/*
	 * either create new list element or reuse existing one
	 */
	if (prop_p == NULL) {
		/*
		 * name not already in list
		 * so malloc a new list node
		 */
		if (is_per_node == B_TRUE) {
			status =
			    find_extn_prop_pernode_entries(
			    list_p, name, value_list, node_list);
			if (status.err_code != SCHA_ERR_NOERR) {
				goto exit_err;
			}
		}

		prop_p = (scrgadm_name_valuel_list_t *)
			calloc(1, sizeof (scrgadm_name_valuel_list_t));
		/* and the name */
		prop_p->name = strdup(name);
		if (!prop_p || !prop_p->name) {
			status.err_code = SCHA_ERR_NOMEM;
			goto exit_err;
		}
		/* insert new prop at head of list */
		prop_p->next = *list_p;
		*list_p = prop_p;
	} else {
		/*
		 * prop_p != NULL: found existing entry for 'name'
		 *
		 * reuse existing node
		 * free old valuelist & insert new one
		 */
		old_value_list = prop_p->value_list;
		free_namelist(old_value_list);
	} /* end else reuse */

	prop_p->is_per_node = is_per_node;
	prop_p->node_list = node_list;
	prop_p->value_list = value_list;

	return (status);

exit_err:
	if (prop_p && prop_p->name)
		free(prop_p->name);
	if (prop_p)
		free(prop_p);
	if (node_list)
		free(node_list);
	return (status);
} /* parse_prop */


/*
 * find_extn_prop_pernode_entries
 *
 * 	This function is used to validate the contents of
 * 	the name_valuel_list. This function is called only
 *      while parsing for  per-node extension properties.
 * 	It takes the base pointer of the scrgadm_name_valuel_list,
 *      the property name, value and the nodelist which is to be validated.
 *
 *	If the node(s) to be validated is present in the
 *	previously defined property, the node(s) are removed
 *      from the corresponding property.
 *
 *	If all the node(s) are removed from the same property,
 *	the property is removed fron the list for the
 *	property name which is equal to the property name
 *	which is to be validated.
 *
 *	If the nodelist to be validated is NULL, i.e for all nodes,
 *	then all the properties having the same property name
 *	as the property which is to be validated are removed.
 *
 *	For the property, if the property name and property value
 *	is equal to the property name and property value which
 *	is to be validated correspondingly, then the
 * 	nodelist of the property is appended to the nodelist
 *	of the input property.
 *
 *	Input:
 *		first_list_p		base pointer for the name value list.
 *		prop_name		name of the input property which
 *					is to be validated.
 *		value_list		the value-list of the input property.
 *		node_list		the node-list of the input property.
 *
 *	This function does not return any value.
 *
 */
static scha_errmsg_t
find_extn_prop_pernode_entries(scrgadm_name_valuel_list_t **first_list_p,
    char *prop_name, scrgadm_value_list_t *value_list,
    scrgadm_node_list_t *node_list)
{
	scha_errmsg_t status = {SCHA_ERR_NOERR, NULL};
	scrgadm_name_valuel_list_t *ptr;
	scrgadm_node_list_t *ptr1;
	scrgadm_node_list_t *ptr2;

	scrgadm_node_list_t *tmp_node = NULL;
	scrgadm_name_valuel_list_t *tmp_prop = NULL;

	char *prop_value;
	int i, num_nodes = 0, count = 0;
	char *flag;

	ptr = (*first_list_p);

	/*
	 * Only first element in the value-list is
	 * parsed since per-node extension
	 * properties is not supported for string
	 * array type.
	 */
	prop_value = strdup(value_list->nl_name);
	if (prop_value == NULL) {
		status.err_code = SCHA_ERR_NOMEM;
		goto validate_exit;
	}

	/* Calculate the number of nodes present in the input nodelist. */
	for (ptr1 = node_list; ptr1; ptr1 = ptr1->nl_next)
		num_nodes++;

	/*
	 * Allocate the memory for the flag.
	 * This variable is used to track whether the
	 * node present in the input nodelist is removed
	 * from the property's nodelist.
	 */
	flag = (char *)calloc(num_nodes, sizeof (char));
	if (flag == NULL) {
		status.err_code = SCHA_ERR_NOMEM;
		goto validate_exit;
	}

	while (ptr != NULL) {
		count = 0;
		/*
		 * Remove this property element from the
		 * name value list, if the node_list is empty,
		 * remove all the properties having the same
		 * property name present in the list.
		 */
		if (strcmp(ptr->name, prop_name) == 0) {
			if (node_list == NULL) {
				tmp_prop = ptr->next;
				remove_prop_from_name_valuel_list(
				    first_list_p, &ptr);
				ptr = tmp_prop;
				continue;
			}

			/*
			 * Remove the property element from the
			 * name value list and append it to the
			 * nodelist of the input property for the
			 * properties having the property name equal
			 * the input property name and value equal to
			 * the input value.
			 */

			if (strcmp(ptr->value_list->nl_name, prop_value) == 0) {
				append_node_to_node_list(
				    node_list, ptr->node_list);
				tmp_prop = ptr->next;
				remove_prop_from_name_valuel_list(
				    first_list_p, &ptr);
				ptr = tmp_prop;
				continue;
			}

			for (i = 0, ptr1 = node_list; ((i < num_nodes)&&(ptr1));
			    i++, ptr1 = ptr1->nl_next) {
				/*
				 * If the node is present in the input
				 * nodelist, remove node from the property's
				 * nodelist.
				 */
				if (flag[i] == '1')
					continue;

					for (ptr2 = ptr->node_list; ptr2;
					    ptr2 = ptr2->nl_next) {

					/*
					 * If the input node is present in
					 * the nodelist of the property
					 * list, remove the node from the
					 * property's nodelist.
					 */

					if (strcmp(ptr2->nl_name,
					    ptr1->nl_name) == 0) {
						tmp_node = ptr2->nl_next;
						remove_node_from_node_list(
						    &(ptr)->node_list, &ptr2);
						ptr2 = tmp_node;
						count++;

						flag[i] = '1';
						break;
					}
				}

				/*
				 * Exit from the loop, if all
				 * node(s) in the input nodelist
				 * are removed from the property
				 * nodelist.
				 */
				if (count >= num_nodes)
					break;
			}
			/*
			 * Remove the property from the list if all
			 * the node(s) are removed from the list.
			 */
			if (ptr->node_list == NULL) {
				tmp_prop = ptr->next;
				remove_prop_from_name_valuel_list(
				    first_list_p, &ptr);
				ptr = tmp_prop;
				continue;
			}
		}
		if (ptr)
			ptr = ptr->next;
	} /* End of while loop. */

validate_exit:
	if (flag)
		free(flag);
	if (prop_value)
		free(prop_value);
	return (status);
}


/*
 * append_node_to_node_list
 *
 * 	This function is used to append the node(s) to the end of another
 *	nodelist.
 *
 *	Input:
 *	node_list
 *	nodes	the list of nodes which should be appended to the node_list.
 *
 *	This function does not return any value.
 */
static void
append_node_to_node_list(scrgadm_node_list_t *node_list,
    scrgadm_node_list_t *nodes)
{
	scrgadm_node_list_t *nl = NULL;

	if (node_list == NULL)
		return;
	nl = node_list;
	while (nl && nl->nl_next)
		nl = nl->nl_next;

	nl->nl_next = nodes;
}

/*
 * remove_node_from_node_list
 *
 *	This function is used to remove a node from the nodelist.
 *
 *	Input:
 *		first	the first elemnt of the nodelist.
 *		node	the current element in the list which should be removed.
 */

static void
remove_node_from_node_list(scrgadm_node_list_t **first,
    scrgadm_node_list_t **node)
{
	scrgadm_node_list_t *prev = NULL;
	scrgadm_node_list_t *ptr = (*node);

	/*
	 * Return if the first element in the list or the node element
	 * which should be removed from the list is null.
	 */

	if (((*first) == NULL) || (ptr == NULL))
		return;

	if ((*first) != ptr) {
		for (prev = (*first); (prev && (prev->nl_next != ptr));
		    prev = prev->nl_next);
		prev->nl_next = ptr->nl_next;
	} else {
		(*first) = (*first)->nl_next;
	}

	ptr->nl_name = "";
	ptr->nl_next = NULL;
	free(ptr);
}

/*
 * remove_prop_from_name_valuel_list
 *
 *
 *	This function is used to remove a property from the name value list.
 *
 *	Input:
 *		first	the first elemnt of the name value list.
 *		node	the current element in the list which should be removed.
 */

static void
remove_prop_from_name_valuel_list(scrgadm_name_valuel_list_t **first,
    scrgadm_name_valuel_list_t **nvl)
{
	scrgadm_name_valuel_list_t *prev = NULL;
	scrgadm_name_valuel_list_t *ptr = (*nvl);

	/*
	 * Return if the first element in the list or the node element
	 * which should be removed from the list  is null.
	 */

	if (((*first) == NULL) || (ptr == NULL))
		return;

	if (ptr != (*first)) {
		for (prev = (*first); (prev && (prev->next != ptr));
		    prev = prev->next);
		prev->next = ptr->next;
	} else {
		(*first) = (*first)->next;
	}

	ptr->name = "";
	ptr->value_list = NULL;
	ptr->node_list = NULL;
	ptr->next = NULL;
	free(ptr);

}



/*
 * parse_prop_name()
 *
 * Input : name of the resource property, nodenames
 * in which the property should be set and property type
 * (standard or extension)
 *
 * Output:
 * nodelist containing the list of nodes.
 *
 * 	This function parses the property name and returns
 * nodelist if present.  Its the caller's responsibility
 * to free the nodelist.
 *
 * Possible return values are:
 *	SCHA_ERR_NO_ERR
 * 	SCHA_ERR_INVAL
 * 	SCHA_ERR_NOMEM
 */
static scha_errmsg_t
parse_prop_name(char *prop_name, scrgadm_node_list_t **nodelist)
{
	scha_errmsg_t status = {SCHA_ERR_NOERR, NULL};
	char *ptr = NULL;
	size_t len;
	if ((ptr = strchr(prop_name, '{')) != NULL) {
		*ptr++ = '\0';
		len = strlen(ptr);
		if (ptr[--len] != '}') {
			status.err_code = SCHA_ERR_INVAL;
			goto exit_err;
		}
		ptr[len] = '\0';
		if (ptr) {
			status = parse_node_list(ptr, nodelist);
			if (status.err_code != SCHA_ERR_NOERR) {
				status.err_code = SCHA_ERR_NOMEM;
				goto exit_err;
			}
			status = process_namelist_to_names(*nodelist);
			if (status.err_code != SCHA_ERR_NOERR)
				goto exit_err;
		}
	}

	return (status);

exit_err:
	if (*nodelist)
		free(*nodelist);
	return (status);

}

/*
 * parse_node_list()
 *
 * Input: A character array containing the comma separated nodes.
 *
 * Output : nodelist containing the list of nodes.nodelist is
 *          a scrgadm_node_list.
 *
 *   This function separates the comma-separated nodelist and
 *   returns the nodelist structure.
 *
 * Possible return values are:
 *	SCHA_ERR_NO_ERR
 * 	SCHA_ERR_NOMEM
 */
static scha_errmsg_t
parse_node_list(char *nodenames, scrgadm_node_list_t **nodelist)
{
	scha_errmsg_t status = {SCHA_ERR_NOERR, NULL};
	char lasts[BUFSIZ];
	char *lasts_p = &lasts[0];
	scrgadm_node_list_t *current_value = NULL;
	scrgadm_node_list_t *first_value = NULL;
	scrgadm_node_list_t *tmp_nodelist;
	char *ptr = NULL;

	ptr = strtok_r(nodenames, ",",  (char **)&lasts_p);
	if (ptr == NULL) {
		tmp_nodelist = (namelist_t *)calloc(1, sizeof (namelist_t));
		if (tmp_nodelist == NULL) {
			status.err_code = SCHA_ERR_NOMEM;
			goto exit_err;
		}
		tmp_nodelist->nl_name = "";
		tmp_nodelist->nl_next = NULL;
		*nodelist = tmp_nodelist;
		return (status);
	}

	while (ptr != NULL) {
		tmp_nodelist = (namelist_t *)calloc(1, sizeof (namelist_t));
		if (tmp_nodelist != NULL) {
			tmp_nodelist->nl_name = strdup(ptr);
			if (tmp_nodelist->nl_name == NULL) {
				status.err_code = SCHA_ERR_NOMEM;
				goto exit_err;
			}
			tmp_nodelist->nl_next = NULL;
			if (first_value == NULL) {
				first_value = tmp_nodelist;
			}

			if (current_value != NULL) {
				current_value-> nl_next = tmp_nodelist;
			}
			current_value = tmp_nodelist;
			ptr = (char *)strtok_r(NULL, ",", (char **)&lasts_p);
		} else {
			status.err_code = SCHA_ERR_NOMEM;
			goto exit_err;
		}
	}
	*nodelist = first_value;
	return (status);
exit_err:
	if (first_value) {
		free_namelist(first_value);
		first_value = NULL;
	}
	return (status);
} 

/*
 * scrgadm_is_per_node_prop()
 *
 * Input: a valid resource property name.
 *
 * boolean flag to indicate whether the given property
 * is to be set in rs creation or in changing the rs
 * property. B_TRUE for change and B_FALSE for creation.
 *
 * resource name or resource type name depending on
 * the flag.
 *
 * Output: boolean value which indicates the given property is
 *		per-node or not.
 *		TRUE 	- Given property is per node property.
 *		FALSE 	- Given property is not a per node property.
 *
 *	Checks whether the given property is per node property or not.
 *	Get the is_per_node entry from the resource type, if the resource
 *	is added.
 *	Get the is_per_node entry from the resource table, if the resource
 *	property is changed.
 */
static scha_errmsg_t
scrgadm_is_per_node_prop(char *prop_name, boolean_t is_change_mode,
    char *rt_name_or_rs_name, boolean_t *is_per_node)
{
	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};
	rgm_resource_t *rs = (rgm_resource_t *)0;
	rgm_property_list_t *listp, *list;
	rgm_rt_t *rt = (rgm_rt_t *)0;
	rgm_param_t **table, **tablep;
	char *rt_name = NULL;
	char *res_type = NULL;

	if (rt_name_or_rs_name == NULL)
		return (scha_status);

	/* Check membership and init orb */
	scha_status = scrgadm_init();
	if (scha_status.err_code != SCHA_ERR_NOERR)
		goto exit_err;
	if (is_change_mode) {
		scha_status =
		    rgm_scrgadm_getrsrcconf(rt_name_or_rs_name, &rs, NULL);
		if (scha_status.err_code != SCHA_ERR_NOERR)
			goto exit_err;
		if (rs->r_type)
			res_type = strdup(rs->r_type);
	} else {
		res_type = strdup(rt_name_or_rs_name);
	}
	scha_status = rgmcnfg_get_rtrealname(res_type,
	    &rt_name, NULL);
	if (scha_status.err_code != SCHA_ERR_NOERR)
		goto exit_err;
	scha_status = rgm_scrgadm_getrtconf(rt_name, &rt, NULL);
	if (scha_status.err_code != SCHA_ERR_NOERR)
		goto exit_err;
	table = rt->rt_paramtable;
	for (tablep = table; *tablep && (*tablep)->p_name; ++tablep) {
		if (strcasecmp((*tablep)->p_name, prop_name) == 0) {
			*is_per_node = (*tablep)->p_per_node;
			break;
		}
	}
exit_err:
	if (rs)
		rgm_free_resource(rs);
	if (rt)
		rgm_free_rt(rt);
	if (rt_name)
		free(rt_name);
	if (res_type)
		free(res_type);

	return (scha_status);
}

/*
 * parse_raw_value()
 *
 * input: comma separated values in a string, a boolean flag to
 *		ignore checking for the comma
 * 	Note: rg_description and r_description property are allowed to
 *		contain comma in the string value, so we should not
 *		check for comma.
 * output: scrgadm_value_list_t holding sane values
 * malloc storage for scrgadm_value_list_t name_t fields
 * return pointer to scrgadm_value_list_t or NULL if malloc fails
 */
static scrgadm_value_list_t *
parse_raw_value(scrgadm_raw_value_t raw_value, boolean_t not_check_comma) {

	char lasts[BUFSIZ];
	char *lasts_p = &lasts[0];
	char *value_storage = NULL;
	char *value = NULL;
	scrgadm_value_list_t *value_p = NULL;
	scrgadm_value_list_t *current_value = NULL;
	scrgadm_value_list_t *first_value = NULL;

	if (not_check_comma) {
		value_p = (scrgadm_value_list_t *)
		    calloc(1, sizeof (scrgadm_value_list_t));
		value_p->nl_name = strdup(raw_value);
		value_p->nl_next = NULL;
		return (value_p);
	}

	value = (char *)strtok_r(raw_value, ",", (char **)&lasts_p);

	/* special (insane) case where raw_value is comma(s) only */
	if (value == NULL) {
		value_p = (scrgadm_value_list_t *)
			calloc(1, sizeof (scrgadm_value_list_t));
		if (value_p) {
			value_p->nl_name = "";
			value_p->nl_next = NULL;
		}
		return (value_p);
	}

	while (value != NULL) {
		value_p = (scrgadm_value_list_t *)
			calloc(1, sizeof (scrgadm_value_list_t));
		value_storage = strdup(value);

		if (value_p && value_storage) {
			value_p->nl_name = value_storage;
			value_p->nl_next = NULL;

			if (first_value == NULL) /* first element */
				first_value = value_p;

			if (current_value != NULL) /* previous element */
				current_value->nl_next = value_p;

			current_value = value_p;

			value = (char *)strtok_r(NULL, ",", (char **)&lasts_p);
			value_p = NULL;
			value_storage = NULL;
		} else { /* a malloc failed */
			goto exit_err;
		}
	}	/* while */
	return (first_value);

exit_err:
	if (first_value) {
		free_namelist(first_value);
		first_value = NULL;
	}
	if (value_p)
		free(value_p);
	if (value_storage)
		free(value_storage);

	return (first_value);
}	/* parse_raw_value */


/*
 * create_empty_valuelist()
 *
 * input: none
 * output: scrgadm_value_list_t of one element holding empty string as value
 * malloc storage for scrgadm_value_list_t struct
 * return pointer to scrgadm_value_list_t or NULL if malloc fails
 */
static scrgadm_value_list_t *
create_empty_valuelist(void) {
	scrgadm_value_list_t *value_p = NULL;

	value_p = (scrgadm_value_list_t *)
		calloc(1, sizeof (scrgadm_value_list_t));

	if (value_p) {
		value_p->nl_name = "";
		value_p->nl_next = NULL;
	}

	return (value_p);
}	/* create_empty_valuelist */


/*
 * print_all()
 *
 * Print current configuration for RG, RT and Resource.
 *
 * No return value.
 *
 */
static void
print_all(const int margin, const scrgadm_print_t print_level)
{
	/* print all rts */
	print_rt_all(margin, print_level);

	/* print all rgs and their resources */
	print_rg_all(margin, print_level);
}

/*
 * print_error()
 *
 * print scrgadm status by type
 *
 */
static void
print_error(const scconf_errno_t status, char **errmsg)
{
	/* make announcement */
	switch (status)	{
	case SCCONF_NOERR:
	case SCCONF_ENOEXIST:
		break;
	case SCCONF_ENOMEM:
		(void) fprintf(stderr, "%s\n",
		    gettext("Out of memory."));
		break;
	case SCCONF_EUSAGE:
		(void) fprintf(stderr, "%s\n",
		    gettext("Usage error."));
		/* print_usage(); */
		/* now deferred until after errmsg print */
		break;
	case SCCONF_EPERM:
		(void) fprintf(stderr, "%s\n",
		    gettext("Must be root user."));
		break;
	case SCCONF_EINVAL:
		(void) fprintf(stderr, "%s\n",
		    gettext("Invalid entry."));
		break;
	case SCCONF_EUNEXPECTED:
		(void) fprintf(stderr, "%s\n",
		    gettext("Unexpected error."));
		break;
	case SCCONF_ENGZONE:
		(void) fprintf(stderr, "%s\n",
		    gettext("You cannot run this command "
		    "option from a non global zone."));
		break;
	case SCCONF_ECZONE:
		(void) fprintf(stderr, "%s\n",
		    gettext("You cannot run this command "
		    "from a zone cluster."));
		break;
	default:
		(void) fprintf(stderr, "%s\n",
		    gettext("Unknown error."));
		break;
	}

	/* print errmsg if present */
	if (errmsg && *errmsg) {
		(void) printf("%s\n", *errmsg);
		free (*errmsg);
		*errmsg = NULL;
	}

	/* print usage if appropriate */
	switch (status)	{
	case SCCONF_EUSAGE:
	case SCCONF_EINVAL:
		print_usage();
		break;
	default:
		break;
	}

} /* print_error */

/*
 * print usage
 *
 *	print_usage()
 */
static void
print_usage()
{
	char *msg;

	msg = gettext("usage:");
	(void) fprintf(stderr, "%s \n", msg);

	msg = "scrgadm -p[v[v]]";
	(void) fprintf(stderr, "\t%s ", msg);
	msg = "[-t <Resource_Type_name>] \\";
	(void) fprintf(stderr, " %s\n", msg);
	msg = "[-g <Resource_Group_name>] \\";
	(void) fprintf(stderr, "\t\t%s\n", msg);
	msg = "[-j <Resource_name>]";
	(void) fprintf(stderr, "\t\t%s\n", msg);

	msg = "scrgadm -a -t <Resource_Type_name>";
	(void) fprintf(stderr, "\t%s ", msg);
	msg = "[-f <RT_registration_file_path>] \\";
	(void) fprintf(stderr, " %s\n", msg);
	msg = "[-h RT_installed_node_list]";
	(void) fprintf(stderr, "\t\t%s", msg);
	msg = "[-y <property>]";
	(void) fprintf(stderr, " %s \n", msg);

	msg = "scrgadm -c -t <Resource_Type_name>";
	(void) fprintf(stderr, "\t%s ", msg);
	msg = "[-h RT_installed_node_list] \\";
	(void) fprintf(stderr, "%s \n", msg);
	msg = "[-y <property>]";
	(void) fprintf(stderr, "\t\t%s \n", msg);

	msg = "scrgadm -r -t <Resource_Type_name>";
	(void) fprintf(stderr, "\t%s \n", msg);

	msg = "scrgadm -a | -c -g <Resource_Group_name>";
	(void) fprintf(stderr, "\t%s ", msg);
	msg = "[-h RT_installed_node_list] \\";
	(void) fprintf(stderr, "%s \n", msg);
	msg = "[-y <property>]";
	(void) fprintf(stderr, "\t\t%s \n",	msg);

	msg = "scrgadm -r -g <RG_name>";
	(void) fprintf(stderr, "\t%s \n", msg);

	msg = "scrgadm -a -j <Resource_name>";
	(void) fprintf(stderr, "\t%s ", msg);
	msg = "-t <Resource_Type_name> \\";
	(void) fprintf(stderr, "%s \n", msg);
	msg = "-g <RG_name> [-y <property> [-y <property>]] \\";
	(void) fprintf(stderr, "\t\t%s\n", msg);
	msg = "[-x <property> [-x <property>]]";
	(void) fprintf(stderr, "\t\t%s \n", msg);

	msg = "scrgadm -c -j <Resource_name>";
	(void) fprintf(stderr, "\t%s ", msg);
	msg = "[-y <property> [-y <property>]] \\";
	(void) fprintf(stderr, "%s \n",	msg);
	msg = "[-x <property> -x <property>]]";
	(void) fprintf(stderr, "\t\t%s \n",	msg);

	msg = "scrgadm -r -j <Resource_name>";
	(void) fprintf(stderr, "\t%s \n", msg);

	msg = "scrgadm -a -L -g <LogicalHost_RG_Name>";
	(void) fprintf(stderr, "\t%s ", msg);
	msg = "[-j <Resource_name>] \\";
	(void) fprintf(stderr, "%s \n", msg);
	msg = "-l hostname[,hostname,...]";
	(void) fprintf(stderr, "\t\t%s ", msg);
	msg = "[-n ipmp@node[,ipmp@node,...]] \\";
	(void) fprintf(stderr, "%s \n", msg);
	msg = "[-y <property> [-y <property>]]";
	(void) fprintf(stderr, "\t\t%s\n", msg);

	msg = "scrgadm -a -S -g <SharedAddress_RG_Name>";
	(void) fprintf(stderr, "\t%s ", msg);
	msg = "[-j <Resource_name>] \\";
	(void) fprintf(stderr, "%s \n", msg);
	msg = "-l hostname[,hostname,...]";
	(void) fprintf(stderr, "\t\t %s ", msg);
	msg = "[-n ipmp@node[,ipmp@node,...]] \\";
	(void) fprintf(stderr, "%s \n", msg);
	msg = "[-X aux_node[,aux_node,...]]";
	(void) fprintf(stderr, "\t\t%s ", msg);
	msg = "[-y <property> [-y <property>]]";
	(void) fprintf(stderr, "%s\n", msg);

} /* print_usage */


/*
 * process_namelist_to_names()
 *
 * Input: namelist_t which may contain names or nodeid's
 *
 * Action:
 *	for each elt:
 *		convert entry from nodeid to name if needed
 *		make sure name is in cluster
 *
 * Output: same list, all elts guaranteed to hold nodenames, no nodeids
 *
 * Returns:
 *	scha_errmsg_t
 */
static scha_errmsg_t
process_namelist_to_names(namelist_t *list) {

	scconf_errno_t scconf_status = SCCONF_EINVAL; /* note unusual init! */
	scha_errmsg_t status = {SCHA_ERR_NOERR, NULL};
	scconf_nodeid_t nodeid;
	char *nodename = NULL;
#if SOL_VERSION >= __s10
	char *zonename = NULL;
#endif
	char errbuff[BUFSIZ];

	while (list) {

		/*
		 * call scconf_get_nodename() first to eliminate
		 * nodeid's because scconf_get_nodeid() will not
		 * return failure if we send in a nodeid
		 */

		/*
		 *  try treating nl_name as a nodeid
		 *   first:  try to convert char* raw input
		 *	to type scconf_nodeid_t
		 *   second: ask scconf if we're a legit nodeid
		 *	if yes, nodename will hold nodename
		 */
		if (str_to_nodeid(list->nl_name, &nodeid)) {
			scconf_status = scconf_get_nodename(nodeid, &nodename);
			/* If farm_mgmt enabled, check for farm nodename also */
			if (iseuropa && scconf_status == SCCONF_ENOEXIST) {
				scconf_status = scconf_get_farmnodename(nodeid,
				    &nodename);
			}
			if (scconf_status == SCCONF_NOERR) {
				/*
				 * input was a good nodeid
				 * dump original input and replace it
				 * with nodename
				 */
				free(list->nl_name);
				list->nl_name = nodename;
				if (!list->nl_name) {
					scconf_status = SCCONF_ENOMEM;
				}
				list = list->nl_next;
				continue;
			} else {
				/* free nodename */
				if (nodename) {
					free(nodename);
				}
			}
		}
		/*
		 * this next block is not an 'else' to str_to_node()
		 * because we want to execute it even if str_to_nodeid()
		 * succeeded but scconf_get_nodename() didn't
		 */

#if SOL_VERSION >= __s10
		/*
		 * Since the nodename passed in is a logical node name
		 * (nodename or nodename:zonename), isolate the nodename
		 * from the zonename.
		 */
		nodename = strdup(list->nl_name);
		zonename = strchr(nodename, ':');
		if (zonename) {
			*zonename++ = '\0';
		}
#endif

		/*
		 * our raw entry wasn't a nodeid so
		 * now try it as a nodename
		 * if successful, nodeid will hold value
		 * (we'll ignore it)
		 */
#if SOL_VERSION >= __s10
		scconf_status = scconf_get_nodeid(nodename, &nodeid);
		/* If farm_mgmt enabled, check for farm nodeid also */
		if (iseuropa && scconf_status == SCCONF_ENOEXIST) {
			scconf_status = scconf_get_farmnodeid(nodename,
			    &nodeid);
		}
#else
		scconf_status = scconf_get_nodeid(list->nl_name, &nodeid);
		if (iseuropa && scconf_status == SCCONF_ENOEXIST) {
			scconf_status = scconf_get_farmnodeid(list->nl_name,
			    &nodeid);
		}
#endif
		if (scconf_status != SCCONF_NOERR) {
			goto cleanup;
		}
		list = list->nl_next;

#if SOL_VERSION >= __s10
		if (nodename) {
			free(nodename);
			nodename = NULL;
		}
		/* do not free zonename -- it was not malloc'd */
#endif
	} /* while list */

cleanup:
	if (scconf_status != SCCONF_NOERR) {
		status = scconf_err_to_scha_err(scconf_status);
		if (nodename) {
			(void) sprintf(errbuff, "%s%s%s\n",
			    gettext("Unable to convert '"),
#if SOL_VERSION >= __s10
			    nodename,
#else
			    list->nl_name,
#endif
			    gettext("' to a cluster member nodename."));
			status.err_msg = strdup(errbuff);
		}
#if SOL_VERSION >= __s10
		if (nodename)
			free(nodename);
		/* do not free zonename -- it was not malloc'd */
#endif
	}
	return (status);
} /* process_namelist_to_names */

/*
 * set_props()
 *
 * Input:
 *	scrgadm_name_valuel_list,
 *	char array of prop names, and
 *	aligned char array of values
 *
 * Action:
 *	for each prop name:
 *		if not already on list
 *		add new name/valuel entry
 *
 * Output: new items on list
 *
 * Note: Incoming arrays have NULL in last cell.
 *
 * Possible return values:
 *	SCHA_ERR_NOERR
 *	SCHA_ERR_NOMEM
 */
scha_errmsg_t
set_props(scrgadm_name_valuel_list_t **sb_lprops_pp,
    char *props[], char *values[]) {

	scha_errmsg_t status = {SCHA_ERR_NOERR, NULL};

	char buff[BUFSIZ];
	boolean_t matched;
	int i = 0;
	scrgadm_name_valuel_list_t *ptr;

	while (props[i]) {
		matched = B_FALSE;
		ptr = *sb_lprops_pp;	/* pts to top node of of list */
		while (ptr != NULL) {
			if (strcasecmp(ptr->name, props[i]) == 0) {
				matched = B_TRUE;
				break;
			}
			ptr = ptr->next;
		} /* while in props list */
		if (matched == B_FALSE) {
			(void) strcpy(buff, props[i]);
			(void) strcat(buff, "=");
			(void) strcat(buff, values[i]);
			/*
			 * Parse the properties.
			 * 1 is passed since it is extension property.
			 * Two NULL values are passed since they are
			 * not used to set per-node property.
			 */
			status = parse_prop(buff, sb_lprops_pp,
			    B_TRUE, NULL, NULL);
		} /* if matched */
		i++;
	} /* while rqd_props */
	return (status);
} /* set_props */


/*
 * append_props()
 *
 * Input:
 *	scrgadm_name_valuel_list dest
 *	scrgadm_name_valuel_list src
 *
 * Action:
 *	for each elt in dest:
 *		if src holds no dup prop then append src list to dest list
 *
 * Output: original dest list with src list appended to it
 *
 * Possible return values:
 *	SCHA_ERR_NOERR
 *	SCHA_ERR_PROP
 */
scha_errmsg_t
append_props(scrgadm_name_valuel_list_t **dest,
    scrgadm_name_valuel_list_t **src) {

	scha_errmsg_t status = {SCHA_ERR_NOERR, NULL};

	scrgadm_name_valuel_list_t *p1;
	scrgadm_name_valuel_list_t *p1_prev;
	scrgadm_name_valuel_list_t *p2;

	if (*dest == NULL) {
		*dest = *src;
		goto cleanup;
	}
	if (*src == NULL) {
		goto cleanup;
	}
	/*
	 * for each elt in dest:
	 *   see if incoming elt list holds same name
	 *   if yes: error. This is a first-in wins scene.
	 *   If no: append entire src list to dest list.
	 */
	/* Initialise p1_prev otherwise lint report an error */
	p1_prev = *dest;
	for (p1 = *dest; p1 != NULL; p1 = p1->next) {
		p1_prev = p1;
		for (p2 = *src; p2 != NULL; p2 = p2->next) {
			/* Note here we need to use case insensitive compare */
			if (strcasecmp(p1->name, p2->name) == 0) {
				status.err_code = SCHA_ERR_PROP;
				goto cleanup;
			}
		}
	}
	p1_prev->next = *src;

cleanup:
	return (status);
} /* append_props */

/*
 * is_asterisk_specified()
 *
 * Input: namelist_t
 *
 * Action: check to see whether an asterisk(*) is specified
 *
 * Output:
 *	Return B_TRUE if the property value is an asterisk(*)
 *	Otherwise, return B_FALSE
 *
 * Possible return values:
 *	B_TRUE
 *	B_FALSE
 */
static boolean_t
is_asterisk_specified(const namelist_t *nlist)
{

	if (nlist != NULL) {
		if (strcmp(nlist->nl_name, SCHA_ALL_SPECIAL_VALUE) == 0)
			return (B_TRUE);
		else
			return (B_FALSE);
	}
	return (B_TRUE);
}


/*
 *	Supporting functions for freeing allocated memory
 */



static void
free_hostname_list(scrgadm_hostname_list_t *list_p) {
	scrgadm_hostname_list_t *ptr = list_p;
	scrgadm_hostname_list_t *next = NULL;

	while (ptr != NULL) {
		if (ptr->raw_name)
			free(ptr->raw_name);
		if (ptr->hostname)
			free(ptr->hostname);
		if (ptr->ip_addr)
			free(ptr->ip_addr);
		if (ptr->ip6_addr)
			free(ptr->ip6_addr);
		next = ptr->next;
		free(ptr);
		ptr = next;
	}
} /* free_hostname_list */


static void
free_namelist(namelist_t *list_p)
{
	namelist_t *ptr = list_p;
	namelist_t *next = NULL;

	while (ptr != NULL) {
		if (ptr->nl_name != NULL) {
			free(ptr->nl_name);
		}
		next = ptr->nl_next;
		free(ptr);
		ptr = next;
	}
} /* free_namelist */


static void
free_name_valuel_list(scrgadm_name_valuel_list_t **list_p)
{
	scrgadm_name_valuel_list_t *ptr = *list_p;
	scrgadm_name_valuel_list_t *next = NULL;

	while (ptr != NULL) {
		if (ptr->name != NULL) {
			free(ptr->name);
		}
		if (ptr->value_list != NULL) {
			free_namelist(ptr->value_list);
		}
		if (ptr->node_list != NULL) {
			free_namelist(ptr->node_list);
		}
		next = ptr->next;
		free(ptr);
		ptr = next;
	}
}


/*
 * This special 'free' handles nvl's created by namelist_to_nvl_list()
 * We must free ONLY the name field, but NOT free the value_list field
 *
 * There is only ONE element on this nvl list.
 */
static void
free_nvl_limited(scrgadm_name_valuel_list_t *list_p) {

	if (list_p != NULL) {
		if (list_p->name != NULL) {
			free(list_p->name);
		}
	}
}

static void
free_property_array(scha_property_t **array_p) {
	/*
	 * free only the scha_property_t objects
	 * do NOT free what the fields point to
	 * because this space is owned by property list
	 */
	free(*array_p);
}

static void
free_node_ipmp_list(scrgadm_node_ipmp_list_t *list_p) {

	scrgadm_node_ipmp_list_t *ptr = list_p;
	scrgadm_node_ipmp_list_t *next = NULL;

	while (ptr != NULL) {
		if (ptr->raw_netid)
			free(ptr->raw_netid);
		if (ptr->ipmp_group)
			free(ptr->ipmp_group);
		if (ptr->raw_node)
			free(ptr->raw_node);
		if (ptr->hostname)
			free(ptr->hostname);

		next = ptr->next;
		free(ptr); /* frees nodeid, too */
		ptr = next;
	}
} /* free_node_ipmp_list */

#if 0
see usr/cluster/include/scha.h and scha_tags.h
for various property definitions
#endif
