/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)fed_slm.c	1.16	08/05/20 SMI"

/*
 * fed_slm.c
 * fed routines for Service Level Management
 */


/*
 * SC_SRM: for Linux, there is no Solaris projects and no Solaris pools.
 * If this flag is not defined, simply by-pass this file.
 * RG_SLM_... properties always exists, but if SC_SRM is not not defined,
 * no SLM action is performed by the fed deamon.
 * See also SC_SRM use in fed_util.c and librgmserver.
 */
#ifdef SC_SRM


/*
 * Complete details in /shared/SC/dev-process/features/2004/1368/design-doc/
 * ganymede-design-part2.sxw or ganymede-design-part2.pdf.
 *
 * fed routines for Service Level Management handles the RG_SLM_TYPE,
 * RG_SLM_PSET_TYPE, RG_SLM_CPU_SHARES, RG_SLM_PSET_MIN RGM SLM properties, and
 * scconf per node SLM properties (globalzoneshares, defaultpsetmin).
 * See the man pages of scrgadm and scconf for details
 * of use (or the design doc listed above).
 *
 * Sun Cluster SLM requires the fed daemon to be aware of the resource
 * method type (there are 12 type of methods, METH_START, METH_VALIDATE, etc).
 * A RGM type has mandatory METH_START and/or METH_PRENET_START and METH_STOP
 * and/or a METH_POSTNET_STOP (otherwise, RGM type creation is refused).
 *
 * For all Solaris releases, if RG_SLM_TYPE is 'automated', fed SunCluster SLM:
 * - create Sun Cluster SLM generated projects, apply project.cpu-shares
 *   using RG_SLM_CPU_SHARES, each resource method is than exectuted in this
 *   project
 * - verify Sun Cluster SLM created projects have not be modified manualy
 *   before used in fed_util.c fed_setproject() function, for each method,
 *   when RG_SLM_TYPE is automated
 * - handles dynamic changes of RG_SLM_TYPE (manual to automated or the
 *   contrary)
 * - handles dynamic changes of RG_SLM_CPU_SHARES (project.cpu-shares value is
 *   recomputed and dynamically reasign
 *
 * For Solaris >= 10, when RG_SLM_TYPE is 'automated', these addional steps
 * are performed:
 * - create Sun Cluster pools (and optionaly Sun Cluster psets) for nodelist
 *   with non-global zones. A non-global zone is dynamically bind to a Sun
 *   Cluster generated pool (with Solaris pool_set_binding() API).
 * - a pset is created or not depending RG_SLM_PSET_TYPE value
 * - Sun Cluster generated psets (min,max) size are handled by the sum of
 *   RG_SLM_CPU_SHARES divided by 100 (for cpu.max) and the sum of
 *   RG_SLM_PSET_MIN (for cpu.min)
 * - zone.cpu-shares is applied to a non-global zone using the sum of
 *   RG_SLM_CPU_SHARES
 * - global zone zone.cpu-shares computation uses a SLM scconf per node
 *   property named globalzoneshares.
 *   (for global zone, it is the sum of RG_SLM_CPU_SHARES added scconf SLM
 *   globalzoneshares property value)
 * - When all resource group with RG_SLM_TYPE set to 'automated' leave a
 *   non-global zone:
 *   - destroy SunCluster generated pool and optionaly pset
 *   - the zone binding is set back to its original pool (pool_default)
 *   - the zone zone.cpu-shares value is set to the
 *     zone.cpu-shares value found in its zonecfg.
 * - When all resource group with RG_SLM_TYPE set to 'automated' leave global
 *   zone (bind to pool_default by default, global zone pool binding is
 *   never modified by fed SLM):
 *   - global zone.cpu-shares value is set to the scconf per node
 *     SLM globalzoneshares property value (Solaris RFE "4970603 should be able
 *     to persistently specify global zone's cpu shares" is not yet available)
 *
 * - handles changes of RG_SLM_PSET_TYPE (weak to default, etc)
 *
 * - Creation of pools/psets is done for METH_START or METH_PRENET_START,
 *   BEFORE the execution of these methods.
 * - Deletion of pools/psets is done for METH_STOP or METH_POSTNET_STOP,
 *   AFTER the execution of these methods.
 *
 *   Note that SunCluster SLM never modify a zone static configuration
 *   (zonecfg). SunCluster SLM actions on zones are dynamic (zone pool binding
 *   is done by fed SLM through Solaris
 *   pool_set_binding(poolname, P_ZONEID, zoneid) API and zone.cpu-shares are
 *   assigned through prctl command).
 *
 *
 *   EXAMPLE:
 *   ========
 *
 *   #scradm -a -g z1RG1 -y RG_SLM_TYPE=automated -y RG_SLM_CPU_SHARES=200
 *   -y RG_SLM_PSET_MIN=1 -y nodelist=civa:z1
 *   #scradm -a -g z1RG2 -y RG_SLM_TYPE=automated -y RG_SLM_CPU=300
 *   -y RG_SLM_PSET_MIN=3 -y nodelist=civa:z1
 *
 *   Note: methods of resource of z1RG1 and z1RG2 can only be launched if
 *   zone z1 zonecfg pool is pool_default, and zone z1 dynamic binding is also
 *   pool_default (these checks are done only for non-global zones, see
 *   /shared/SC/dev-process/features/2004/1368/design-doc/
 *   ganymede-design-part2.sxw or ganymede-design-part2.pdf).
 *
 *   Because z1RG1 has RG_SLM_TYPE set to 'automated':
 *   all methods of resources of z1RG1 will be executed in zone z1 project
 *   SCSLM_z1RG1, with a project.cpu-shares value of 200
 *
 *   Because z1RG2 has RG_SLM_TYPE set to 'automated':
 *   all methods of resources of z1RG2 will be executed in zone z1 project
 *   SCSLM_z1RG1, with a project.cpu-shares value of 300
 *
 *   Now:
 *
 *   1) a resource of z1RG1 executes a METH_START or a METH_PRENET_START method,
 *   BEFORE the method is executed by fed, fed SLM:
 *   - create a pool named SCSLM_pool_z1, bind the zone z1 to SCSLM_pool_z1.
 *   SCSLM_pool_z1 is associated to pset_default (because RG_SLM_PSET_TYPE is
 *   by default 'default')
 *   - zone z1 zone.cpu-shares value is set to 200 (with Solaris prctl command)
 *
 *   2) a resource of z1RG2 executes a METH_START or a METH_PRENET_START method,
 *   BEFORE the method is executed by fed, fed SLM:
 *   - zone z1 zone.cpu-shares value is set to 500 (200 + 300) (with prctl)
 *
 *   So zone z1 now competes for cpus of pset_default using a zone shares
 *   value of 500 with others zones bind to pools associated to pset_default
 *   (for example, the global zone).
 *   Within zone z1, z1RG1 competes for cpus using a project shares of 200 with
 *   z1RG2 that has a project.cpu-shares of 300.
 *
 *   3) a resource of z1RG1 executes a METH_STOP or a METH_POSTNET_STOP method,
 *   AFTER the method is executed by fed, fed SLM:
 *   - zone z1 zone.cpu-shares value is set to 300 (with prctl)
 *
 *   4) a resource of  z1RG2 executes a METH_STOP or a METH_POSTNET_STOP method,
 *   AFTER the method is executed by fed, fed SLM:
 *   - SCSLM_pool_z1 is destroyed, zone z1 binding goes to pool_default
 *   - zone z1 zone.cpu-shares value is set to value found in zone z1 zonecfg
 *     (1 by default) with prctl command
 *
 *   Example with psets
 *   ==================
 *
 *   If both z1RG1 and z1RG2 have RG_SLM_TYPE_SET=strong, the above
 *   scenario is now:
 *
 *   1) a resource of z1RG1 executes a METH_START or a METH_PRENET_START method,
 *   BEFORE the method is executed by fed, fed SLM:
 *   - create a pool named SCSLM_pool_z1, bind the zone z1 to SCSLM_pool_z1.
 *   - create a pset named SCSLM_pset_z1, pset size is cpu.min=1,cpu.max=2
 *   - SCSLM_pool_z1 is associated to SCSLM_pset_z1.
 *   - zone z1 zone.cpu-shares value is set to 200.
 *   zone z1 processes executes in a pset of size min=1,max=2
 *
 *   2) a resource if z1RG2 executes a METH_START or a METH_PRENET_START method,
 *   BEFORE the method is executed by fed, fed SLM:
 *   - pset SCSLM_pset_z1 size is changed to cpu.min=3 (1+2), cpu.max=5 (3 +2 )
 *   - zone z1 zone.cpu-shares value is set to 500 (200 + 300)
 *   zone z1 processes executes in a pset of size min=3,max=5
 *
 *   So zone z1 executes in a pset SCSLM_pset_z1 of size cpu.min=3,cpu,max=5,
 *   within this pset, z1RG1 competes for cpus using a project shares of 200
 *   with z1RG2 that has a project.cpu-shares of 300
 *
 *   3) a resource of z1RG1 executes a METH_STOP or a METH_POSTNET_STOP method,
 *   AFTER the method is executed by fed, fed SLM:
 *   - pset SCSLM_pset_z1 size is changed to cpu.min=2,cpu.max=3
 *   - zone z1 zone.cpu-shares value is set to 300
 *   zone z1 processes executes in a pset of size min=2,max=3
 *
 *   4) a resource z1RG2 executes a METH_STOP or a METH_POSTNET_STOP method,
 *   AFTER the method is executed by fed, fed SLM:
 *   - SCSLM_pset_z1 is destroyed
 *   - pool SCSLM_pool_z1 is destroyed
 *   - zone z1 binding goes to pool_default (by default associated to
 *     pset_default)
 *   - zone z1 zone.cpu-shares value is set to value found in zone z1 zonecfg
 *     (1 by default) with prctl command
 *
 */


#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/param.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <strings.h>
#include <sys/time.h>
#include <ctype.h>
#include <pthread.h>
#include <sys/mman.h>
#include <sys/sol_version.h>
#include <sys/wait.h>
#include <pool.h>
#include <assert.h>
#if SOL_VERSION >= __s10
#include <zone.h>
#else
#define	ZONENAME_MAX	64
#define	GLOBAL_ZONENAME	"global"
#endif
#include "fed.h"
#include <sys/sc_syslog_msg.h>
#include "rgm/rgm_cnfg.h"
#include <project.h>
#if DOOR_IMPL
#include <rgm/fe.h>
#else
#include <rgm/rpc/fe.h>
#endif
#include <rgm/rgm_methods.h>
#include "fed_slm_defs.h"


/*
 * Types definition
 */


/*
 * List of Resources, used in list of Resource Group
 */
typedef struct SCSLM_rlist {
	char *r_name;			/* resource name */
	unsigned int running;		/* running or about to be launched */
	struct SCSLM_rlist *next;	/* next resource of the RG */
	void *myaddr;			/* to check the list */
} t_SCSLM_rlist;

/*
 * running flag not zero for a resource <r_name> means:
 * if (running & METH_START), rgmd has asked fed to execute method METH_START of
 * resource <r_name>
 * if running & METH_PRENET_START, rgmd has asked fed to execute method
 * METH_PRENET_START of resource <r_name>
 * METH_START or METH_PRENET_START are mandatory methods. A RGM type cannot
 * be created if at least one of these two methods is not provided.
 * METH_STOP or METH_POSTNET_STOP are mandatory methods. A RGM type cannot
 * be created if at least one of these two methods is not provided.
 */


/*
 * List of Resource group
 */
typedef struct SCSLM_rglist {
	char *rg_name;				/* resource group name */
	char rg_slm_type[SCSLM_MAX_TYPE_LEN];	/* manual or automated */
	char rg_slm_pset_type[SCSLM_MAX_PSET_TYPE_LEN];
						/* default or dedicated */
	char rg_zone[ZONENAME_MAX + 1];		/* global or local zone name */
	t_SCSLM_rlist *rlist;			/* resource list */
	struct SCSLM_rglist *next;		/* chain of resource groups */
	unsigned int pset;			/* pset created ? */
	unsigned int pset_cpu;			/* size of pset, in CPUs */
	unsigned int pset_cpu_min;		/* min size of pset, in CPUs */
	unsigned int cpu;			/* RG_SLM_CPU_SHARES value */
	unsigned int cpu_min;			/* RG_SLM_PSET_MIN value */
	char *proj_name;			/* SCSLM project name */
	void *myaddr;				/* to check list */
} t_SCSLM_rglist;


/*
 * Prototypes
 */

/* Main fed routines for Service Level Management */
static int SCSLM_fe_run(fe_handle *);
static int SCSLM_stop(fe_handle *);
static int SCSLM_update(fe_slm_update_args *);
static int SCSLM_conf(fe_slm_conf_args *);
static int SCSLM_check(void);
static int SCSLM_hdl_meth(fe_handle *, char **);
static int SCSLM_hdl_meth_stop(fe_handle *, char **);

#if SOL_VERSION >= __s10
/* Main fed routines for Service Level Management for Solaris >= S10 */
static int SCSLM_hdl_meth_S10(t_SCSLM_rglist *, t_SCSLM_rlist *);
static int SCSLM_stop_S10(t_SCSLM_rglist *, t_SCSLM_rlist *);
static int SCSLM_hdl_strong(t_SCSLM_rglist *);
static int SCSLM_hdl_weak(t_SCSLM_rglist *);
static int SCSLM_destroy_weak_psets(void);
static void SCSLM_hdl_default_pset_min(void);
static int SCSLM_check_dedicated_pset(t_SCSLM_rglist *);
static int SCSLM_in_dedicated_pset(t_SCSLM_rglist *);
static int SCSLM_set_pset_in_zone(t_SCSLM_rglist *, unsigned int,
    unsigned int, unsigned int);
static void SCSLM_del_pset_in_zone(t_SCSLM_rglist *);
static int SCSLM_zone_find_shares(char *, unsigned int *, unsigned int *);
static int SCSLM_running_in_RG(char *, char *);
static int SCSLM_hdl_pool(t_SCSLM_rglist *);
static void SCSLM_thread(void);
#endif /* SOL_VERSION >= __s10 */

/*
 * fed routines for Service Level Management
 */
static int SCSLM_lock_file(void);
static int SCSLM_unlock_file(void);
static int SCSLM_R_running(t_SCSLM_rglist *);
static int SCSLM_update_per_zone(fe_slm_update_args *, t_SCSLM_rglist *);
static int SCSLM_update_delete_rg(char *);
static int SCSLM_update_delete_r(char *, char *);
#if SOL_VERSION >= __s10
static int SCSLM_zone_down(const char *);
static int SCSLM_update_pset(t_SCSLM_rglist *, unsigned int, unsigned int);
#endif /* SOL_VERSION >= __s10 */
static int SCSLM_exec(char *, int *, char **);
static void SCSLM_replace_dash_in_str(char *);
static void SCSLM_get_pset_min(int, unsigned int *);
static void SCSLM_get_cpu_shares(int, unsigned int *);
static void SCSLM_cpu_values(fe_handle *, t_SCSLM_rglist *);

/*
 * fed SLM data list handling
 */
static t_SCSLM_rglist *SCSLM_src_RG_fed(char *, char *);
static t_SCSLM_rlist *SCSLM_src_R_fed(char *, t_SCSLM_rglist *);
static int SCSLM_add_RG_fed(fe_handle *);
static t_SCSLM_rlist *SCSLM_append_R_fed(t_SCSLM_rglist *);
static int SCSLM_add_R_fed(t_SCSLM_rglist *, fe_handle *, t_SCSLM_rlist **);
static void SCSLM_prt_fed(void);

/*
 * Projects handling routines
 */
static int SCSLM_create_proj(char *, char *);
static int SCSLM_modify_proj_shares_file(char *, char *, unsigned int);
static int SCSLM_modify_proj_shares_mem(char *, char *, unsigned int);
static int SCSLM_fgetprojent(char *, char *, projid_t *, unsigned int *);
static int SCSLM_file_del_proj(char *, char *, int);
static int SCSLM_file_add_proj(char *, char *, projid_t, char *);
static int SCSLM_file_close_proj(void);
static int SCSLM_file_open_proj(void);
static int SCSLM_file_srch_proj(char *, char *, projid_t *, char **);

#if SOL_VERSION >= __s10
/*
 * Pools/Psets/Zones handling routines
 */
static int SCSLM_create_pool(char *);
static int SCSLM_delete_pool(char *);
static int SCSLM_create_pset(char *, uint64_t, uint64_t);
static int SCSLM_delete_pset(char *);
static int SCSLM_modify_pset(char *, uint64_t, uint64_t, int);
static int SCSLM_modify_zone_shares(char *, unsigned int *);
static int SCSLM_modify_pool_pset(char *, char *);
static int SCSLM_zone_pool(char *, char **);
static int SCSLM_get_poolid(char *, int64_t *);
static int SCSLM_pools_ok(void);
static int SCSLM_get_zone_dynamic_binding(char *, int64_t *);
#endif /* SOL_VERSION >= __s10 */


/*
 *  Global data
 */
extern pthread_mutex_t	fe_queue_lock;
static int		scslm_do_log = 0;
static t_SCSLM_rglist	*fedRGHead = NULL;
static int		slm_proj_file = -1;
static int		slm_lock_file = -1;
static char		*slm_proj_file_ptr;
static size_t		slm_proj_file_size;

static unsigned int	scslm_thread_period = SCSLM_THREAD_PERIOD;
static unsigned int	scslm_global_zone_shares = SCSLM_GLOBAL_ZONE_SHARES;
static unsigned int	scslm_default_pset_min =
			    SCSLM_DEFAULT_PSET_MIN;

#if SOL_VERSION >= __s10
static pthread_t	slm_thread;
#endif /* SOL_VERSION >= __s10 */



/*
 * -----------------------------------------------------------------------------
 *
 * Public functions, called by outside
 *
 * -----------------------------------------------------------------------------
 */

/*
 * Function scslm_init
 * SLM projects cleanup in global zone, as fed is started in global zone.
 * For Solaris >= 10, creates a thread that periodically try to set the
 * requested default pset minimum size (scconf SLM defaultminpset per node
 * property value).
 * Args In:
 *	None.
 * Args Out:
 *	None.
 * Return value:
 *	SCSLM_SUCCESS or SCSLM_ERROR
 * Called only by function main() in fed_main.c.
 */
int
scslm_init(void)
{
#if SOL_VERSION >= __s10
	sc_syslog_msg_handle_t	sys_handle;
	static int		slm_thread_created = 0;
	int			err;
#endif

#if SOL_VERSION >= __s10
	if (slm_thread_created == 0) {
		/*
		 * Create SCSLM S10 periodic thread
		 */
		if (pthread_create(&slm_thread, NULL,
		    (void *(*)(void *))SCSLM_thread, NULL) != 0) {
			err = errno; /*lint !e746 */
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * Should never occur.
			 * @user_action
			 * Contact your authorized Sun service provider to
			 * determine whether a workaround or patch is
			 * available.
			 */
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM scslm_init "
			    "pthread_create error <%s>", strerror(err));
			sc_syslog_msg_done(&sys_handle);
			return (SCSLM_ERROR);
		}
		slm_thread_created = 1;
	}
#endif /* SOL_VERSION >= __s10 */
	return (SCSLM_SUCCESS);
}


/*
 * Function scslm_fe_run
 * Called by function fe_run_process() function in fed_util.c BEFORE a
 * resource method is executed.
 * rgmd asks fed to execute a method with the FEPROC_RUN request (either a door
 * or RPC request), execution of the method is done by fe_run_process() function
 * in fed_util.c.
 * Args In:
 *	a fe_handle
 * Args Out:
 *	fe_handle->do_slm_stop and fe_handle->scslm_proj_name
 * Return value:
 *	SCSLM_SUCCESS or SCSLM errors
 * Called by function fe_run_process() BEFORE a resource method is executed.
 */
int
scslm_fe_run(fe_handle *hdl) {
	/* queue lock is already hold in caller */
	return (SCSLM_fe_run(hdl));
}


/*
 * Function scslm_stop
 * Called by function fe_run_process() function in fed_util.c AFTER the
 * method has been executed.
 * rgmd asks fed to execute a method with the FEPROC_RUN request (either a door
 * or RPC request), execution of the method is done by fe_run_process() function
 * in fed_util.c.
 * Args In:
 *	a fe_handle.
 * Args Out:
 *	None.
 * Return value:
 *	SCSLM_SUCCESS or SCSLM errors
 * Called by function fe_run_process() AFTER the method has been executed.
 */
int
scslm_stop(fe_handle *hdl)
{
	/* queue lock is already hold in caller */
	return (SCSLM_stop(hdl));
}


/*
 * Function scslm_update
 * This function is used to handle RG SLM properties changes.
 * (for example, scrgadm -c -g RG -y RG_SLM_CPU_SHARES=5).
 * It is also called when a resource group is deleted and
 * when a resource is deleted (scrgadm -r -g, scrgadm -r -j).
 * Args In:
 *	a fe_slm_update_args
 * Args Out:
 *	None.
 * Return value:
 *	SCSLM_SUCCESS or SCSLM errors
 * Called by librgmserver (or feadm unbundled debug tool) through
 * FEPROC_SLM_UPDATE request (door or RPC).
 */
int
scslm_update(fe_slm_update_args *args)
{
	int ret;

	LOCK_QUEUE();
	ret = SCSLM_update(args);
	UNLOCK_QUEUE();
	return (ret);
}


/*
 * Function scslm_conf
 * This function is used to handle per node scconf SLM properties changes
 * (for example, scconf -c -S node=clust1,globalzoneshares=10)
 * Args In:
 *	a fe_slm_conf_args
 * Args Out:
 *	None.
 * Return value:
 *	SCSLM_SUCCESS or SCSLM errors
 * Called by librgmserver (or feadm unbundled debug tool) through
 * FEPROC_SLM_CONF request (door or RPC).
 */
int
scslm_conf(fe_slm_conf_args *args)
{
	int ret;

	LOCK_QUEUE();
	ret = SCSLM_conf(args);
	UNLOCK_QUEUE();
	return (ret);
}


/*
 * Function scslm_debug
 * Toggles trace level in logs.
 * Args In:
 *	None.
 * Args Out:
 *	None.
 * Return value:
 *	SCSLM_SUCCESS or SCSLM errors
 * Called by feadm unbundled debug tool through FEPROC_SLM_DEBUG request
 * (door or RPC).
 */
void
scslm_debug(void)
{
	if (scslm_do_log == 0) {
		scslm_do_log = 1;
	} else {
		scslm_do_log = 0;
	}
}


/*
 * Function scslm_check
 * Displays in logs internal data and lists.
 * Args In:
 *	None.
 * Args Out:
 *	None.
 * Return value:
 *	SCSLM_SUCCESS or SCSLM errors
 * Called by feadm unbundled debug tool through FEPROC_SLM_CHECK request
 * (door or RPC).
 */
int
scslm_check(void)
{
	int ret;

	LOCK_QUEUE();
	ret = SCSLM_check();
	UNLOCK_QUEUE();
	return (ret);
}


#if SOL_VERSION >= __s10
/*
 * Function scslm_zone_down
 * Called by function down_callback() in fed_zone_state.c
 * Args In:
 *	a zone name
 * Args Out:
 *	None
 * Return value:
 *	SCSLM_SUCCESS or SCSLM errors
 */
int
scslm_zone_down(const char *zonename)
{
	int	ret;
	LOCK_QUEUE();
	ret = SCSLM_zone_down(zonename);
	UNLOCK_QUEUE();
	return (ret);
}


/*
 * Function scslm_zone_up
 * Called by function up_callback() in fed_zone_state.c
 * Args In:
 *	a zone name
 * Args Out:
 *	None
 * Return value:
 *	SCSLM_SUCCESS or SCSLM errors
 */
int
scslm_zone_up(const char *zonename)
{
	/*
	 * Do nothing to avoid fed slm
	 * from reentering rgmd code.
	 */
	return (SCSLM_SUCCESS);
}
#endif



/*
 * -----------------------------------------------------------------------------
 *
 * Main fed routines for Service Level Management
 *
 * -----------------------------------------------------------------------------
 */

/*
 * Function SCSLM_fe_run
 * See scslm_fe_run comments.
 * See comments at the begin of this file for detailled list of actions.
 * Args In:
 *	a fe_handle
 * Args Out:
 *	fe_handle->do_slm_stop and fe_handle->scslm_proj_name
 * Return value:
 *	SCSLM_SUCCESS or SCSLM errors
 * Called only by function scslm_fe_run().
 *
 */
static int
SCSLM_fe_run(fe_handle *handle)
{
	sc_syslog_msg_handle_t	sys_handle;
	int			ret;

	if (scslm_do_log != 0 && handle != NULL) {
		LOG_FED_SLM_INFO2(sys_handle,
		    "SCSLM fe_run cmd <%s>",
		    handle->cmd != NULL ? handle->cmd : "NULL");
		LOG_FED_SLM_INFO2(sys_handle,
		    "SCSLM fe_run r_name <%s>",
		    handle->r_name != NULL ? handle->r_name : "NULL");
		LOG_FED_SLM_INFO2(sys_handle,
		    "SCSLM fe_run rg_name <%s>",
		    handle->rg_name != NULL ? handle->rg_name : "NULL");
		LOG_FED_SLM_INFO2(sys_handle,
		    "SCSLM fe_run zonename <%s>",
		    handle->zonename != NULL ? handle->zonename : "NULL");
		LOG_FED_SLM_INFO2(sys_handle,
		    "SCSLM fe_run rg_slm_type <%s>",
		    handle->rg_slm_type != NULL ? \
			handle->rg_slm_type : "NULL");
		LOG_FED_SLM_INFO2(sys_handle,
		    "SCSLM fe_run rg_slm_pset_type <%s>",
		    handle->rg_slm_pset_type != NULL ? \
			handle->rg_slm_pset_type : "NULL");
		LOG_FED_SLM_INFO2(sys_handle,
		    "SCSLM fe_run slm_flags 0x%x",
		    handle->slm_flags);
	}

	/* Verify handle, mainly if called by feadm */
	if (handle == NULL) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Should never occur.
		 * @user_action
		 * Contact your authorized Sun service provider to determine
		 * whether a workaround or patch is available.
		 */
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM fe_run wrong args <%s>", "NULL handle");
		sc_syslog_msg_done(&sys_handle);
		return (SCSLM_ERROR);
	}
	if (handle->rg_name == NULL) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM fe_run wrong args <%s>", "NULL rg_name");
		sc_syslog_msg_done(&sys_handle);
		return (SCSLM_ERROR);
	}
	if (handle->r_name == NULL) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM fe_run wrong args <%s>", "NULL r_name");
		sc_syslog_msg_done(&sys_handle);
		return (SCSLM_ERROR);
	}
	if (handle->rg_slm_type == NULL) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM fe_run wrong args <%s>", "NULL rg_slm_type");
		sc_syslog_msg_done(&sys_handle);
		return (SCSLM_ERROR);
	}

	if (strcmp(handle->rg_name, "") == 0) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM fe_run wrong args <%s>", "empty rg_name");
		sc_syslog_msg_done(&sys_handle);
		return (SCSLM_ERROR);
	}

	if (strcmp(handle->r_name, "") == 0) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM fe_run wrong args <%s>", "empty r_name");
		sc_syslog_msg_done(&sys_handle);
		return (SCSLM_ERROR);
	}
	if (strcmp(handle->rg_slm_type, "") == 0) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM fe_run wrong args <%s>", "empty rg_slm_type");
		sc_syslog_msg_done(&sys_handle);
		return (SCSLM_ERROR);
	}
	if (strcmp(handle->rg_slm_type, RG_SLM_TYPE_MANUAL) != 0 &&
	    strcmp(handle->rg_slm_type, RG_SLM_TYPE_AUTO) != 0) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM fe_run wrong args <%s>", "unknown rg_slm_type");
		sc_syslog_msg_done(&sys_handle);
		return (SCSLM_ERROR);
	}

	if (strlen(handle->rg_name) > SCSLM_RG_NAME_MAX_LENGTH) {
		return (SCSLM_SUCCESS);
	}

	if (handle->zonename == NULL) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM fe_run wrong args <%s>", "NULL zonename");
		sc_syslog_msg_done(&sys_handle);
		return (SCSLM_ERROR);
	}
	if (handle->rg_slm_pset_type == NULL) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM fe_run wrong args <%s>", "NULL rg_slm_pset_type");
		sc_syslog_msg_done(&sys_handle);
		return (SCSLM_ERROR);
	}
#if SOL_VERSION < __s10
	/* There are no zones on S9, rg_zone must be "global" on S9 */
	if (strcmp(handle->zonename, GLOBAL_ZONENAME) != 0) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM fe_run wrong args <%s>", "S9, zone");
		sc_syslog_msg_done(&sys_handle);
		return (SCSLM_ERROR);
	}
#endif /* SOL_VERSION < __s10 */
	if (strcmp(handle->rg_slm_pset_type, RG_SLM_PSET_TYPE_DEFAULT) != 0 &&
	    strcmp(handle->rg_slm_pset_type,
		RG_SLM_PSET_TYPE_DEDICATED_WEAK) != 0 &&
	    strcmp(handle->rg_slm_pset_type,
		RG_SLM_PSET_TYPE_DEDICATED_STRONG) != 0) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM fe_run wrong args <%s>", "unknown rg_slm_pset_type");
		sc_syslog_msg_done(&sys_handle);
		return (SCSLM_ERROR);
	}
#if SOL_VERSION < __s10
	/* There is no dedicated support on S9 */
	if (strcmp(handle->rg_slm_pset_type,
		RG_SLM_PSET_TYPE_DEDICATED_WEAK) == 0 ||
	    strcmp(handle->rg_slm_pset_type,
		RG_SLM_PSET_TYPE_DEDICATED_STRONG) == 0) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM fe_run wrong args <%s>", "S9 no rg_slm_pset_type");
		sc_syslog_msg_done(&sys_handle);
		return (SCSLM_ERROR);
	}
#endif /* SOL_VERSION < __s10 */

	if (handle->slm_flags < METH_FIRST_METHOD ||
	    handle->slm_flags > METH_LAST_METHOD) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Should never occur.
		 * @user_action
		 * Contact your authorized Sun service provider to determine
		 * whether a workaround or patch is available.
		 */
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM unknown method 0x%x", handle->slm_flags);
		sc_syslog_msg_done(&sys_handle);
		return (SCSLM_ERROR);
	}

	/*
	 * If method is METH_STOP or METH_POSTNET_STOP, handling is done
	 * by SCSLM_hdl_meth_stop
	 * See comments at begin of this file for details.
	 */
	if (handle->slm_flags == METH_STOP ||
	    handle->slm_flags == METH_POSTNET_STOP) {
		ret = SCSLM_hdl_meth_stop(handle, &(handle->scslm_proj_name));
		return (ret);
	}

	/*
	 * For all methods except METH_STOP or METH_POSTNET_STOP,
	 * handling is done by SCSLM_hdl_meth
	 * See comments at begin of this file for details.
	 */
	ret = SCSLM_hdl_meth(handle, &(handle->scslm_proj_name));
	return (ret);
}


/*
 * Function SCSLM_stop
 * See scslm_stop comments.
 * See comments at the begin of this file for detailled list of actions.
 * Args In:
 *	A fe_handle
 * Args Out:
 *	None.
 * Return value:
 *	SCSLM_SUCCESS
 *	SCSLM_ERROR
 * Called only by function scslm_stop().
 */
static int
SCSLM_stop(fe_handle *handle)
{
	sc_syslog_msg_handle_t	sys_handle;
	t_SCSLM_rglist		*p;
	t_SCSLM_rlist		*r;
#if SOL_VERSION >= __s10
	int			ret;
#endif /* SOL_VERSION >= __s10 */

	/*
	 * handle->do_slm_stop is only set by SCSLM_hdl_meth_stop(),
	 * before METH_STOP or a METH_POSTNET_STOP has been executed.
	 */
	if (handle->do_slm_stop == 0) {
		/* There is nothing to do */
		return (SCSLM_SUCCESS);
	}

	/*
	 * A METH_STOP or a METH_POSTNET_STOP method has been executed.
	 * See comments at begin of this file for details.
	 */
	if (scslm_do_log != 0) {
		LOG_FED_SLM_INFO2(sys_handle,
		    "SCSLM stop r_name <%s>",
		    handle->r_name);
		LOG_FED_SLM_INFO2(sys_handle,
		    "SCSLM stop rg_name <%s>",
		    handle->rg_name);
		LOG_FED_SLM_INFO2(sys_handle,
		    "SCSLM stop zonename <%s>",
		    handle->zonename);
		LOG_FED_SLM_INFO2(sys_handle,
		    "SCSLM stop rg_slm_type <%s>",
		    handle->rg_slm_type);
		LOG_FED_SLM_INFO2(sys_handle,
		    "SCSLM stop rg_slm_pset_type <%s>",
		    handle->rg_slm_pset_type);
		LOG_FED_SLM_INFO2(sys_handle,
		    "SCSLM stop slm_flags 0x%x",
		    handle->slm_flags);
	}

	/* search the resource group in the list */
	p = SCSLM_src_RG_fed(handle->zonename, handle->rg_name);
	if (p == NULL) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Should never occur.
		 * @user_action
		 * Contact your authorized Sun service provider to determine
		 * whether a workaround or patch is available.
		 */
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM <%s> not found", handle->rg_name);
		sc_syslog_msg_done(&sys_handle);
		return (SCSLM_ERROR);
	}
	/* search the resource in resource group resource list */
	r = SCSLM_src_R_fed(handle->r_name, p);
	if (r == NULL) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM <%s> not found", handle->r_name);
		sc_syslog_msg_done(&sys_handle);
		return (SCSLM_ERROR);
	}
	if (r->running == 0) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Should never occur.
		 * @user_action
		 * Contact your authorized Sun service provider to determine
		 * whether a workaround or patch is available.
		 */
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM <%s> running is zero", r->r_name);
		sc_syslog_msg_done(&sys_handle);
		return (SCSLM_ERROR);
	}
	r->running = 0;

	if (scslm_do_log != 0) {
		LOG_FED_SLM_INFO3(sys_handle,
		    "SCSLM_stop <%s> running set to 0x%x",
		    r->r_name, r->running);
	}

	if (strcmp(p->rg_slm_type, RG_SLM_TYPE_UNKNOWN) == 0) {
		if (strcmp(handle->rg_slm_type, RG_SLM_TYPE_MANUAL) == 0) {
			handle->do_slm_stop = 0;
			return (SCSLM_SUCCESS);
		}
	}
	if (strcmp(p->rg_slm_type, RG_SLM_TYPE_MANUAL) == 0) {
		handle->do_slm_stop = 0;
		return (SCSLM_SUCCESS);
	}

#if SOL_VERSION >= __s10
	/*
	 * pools/psets/zone.cpu-shares handling
	 * See comments at begin of this file for details.
	 */
	ret = SCSLM_stop_S10(p, r);
	if (ret != SCSLM_SUCCESS) {
		return (SCSLM_ERROR);
	}
#endif /* SOL_VERSION >= __s10 */
	handle->do_slm_stop = 0;
	return (SCSLM_SUCCESS);
}


/*
 * Function SCSLM_update
 * This function is used to handle RG SLM properties changes.
 * (for example, scrgadm -c -g RG -y RG_SLM_CPU_SHARES=5).
 * It is also called when a resource group is deleted and
 * when a resource is deleted (scrgadm -r -g, scrgadm -r -j).
 * A resource group can have same name in different zones.
 * See scslm_update comments.
 * See comments at begin of this file for details.
 * Args In:
 *	None.
 * Args Out:
 *	None.
 * Return value:
 *	SCSLM_SUCCESS
 *	SCSLM_ERROR
 * Called only by function scslm_update().
 */
static int
SCSLM_update(fe_slm_update_args *args)
{
	t_SCSLM_rglist	*rg = fedRGHead;
	int		ret = SCSLM_SUCCESS;

	if (args->type == FE_SLM_UPDATE_DELETE_RG) {
		/*
		 * Remove a resource group from the fed SLM
		 * resource group list
		 */
		ret = SCSLM_update_delete_rg(args->rg_name);
		return (ret);
	}
	if (args->type == FE_SLM_UPDATE_DELETE_R) {
		/*
		 * Remove a resource from the fed SLM resource
		 * list of a fed SLM resource group
		 */
		ret = SCSLM_update_delete_r(args->rg_name, args->r_name);
		return (ret);
	}
	if (args->type != FE_SLM_UPDATE_RG_PROP) {
		return (SCSLM_ERROR);
	}
	/* loop the list of resource group */
	while (rg != NULL) {
		if (strcmp(rg->rg_name, args->rg_name) == 0) {
			/* resource group name found */
			ret += SCSLM_update_per_zone(args, rg);
		}
		rg = rg->next;
	}
	if (ret != 0) {
		return (SCSLM_ERROR);
	}
	return (SCSLM_SUCCESS);
}


/*
 * Function SCSLM_conf
 * This function is used to handle per node scconf SLM properties changes
 * (for example, scconf -c -S node=clust1,globalzoneshares=10)
 * See scslm_conf comments.
 * See comments at begin of this file for details.
 * Args In:
 *	None.
 * Args Out:
 *	None.
 * Return value:
 *	SCSLM_SUCCESS
 *	SCSLM_ERROR
 * Called only by function scslm_conf().
 */
int
SCSLM_conf(fe_slm_conf_args *args)
{
	sc_syslog_msg_handle_t	sys_handle;
	int			err = SCSLM_SUCCESS;
#if SOL_VERSION >= __s10
	unsigned int	tot_shares = 0;
	int		ret;
#endif

	if ((args->type == FE_SLM_CONF_GLOBAL_ZONE_SHARES) &&
	    (scslm_global_zone_shares != args->global_zone_shares)) {
		scslm_global_zone_shares = args->global_zone_shares;
		if (scslm_global_zone_shares < SCSLM_GLOBAL_ZONE_SHARES) {
			scslm_global_zone_shares = SCSLM_GLOBAL_ZONE_SHARES;
			LOG_FED_SLM_INFO3(sys_handle,
			    "SCSLM global zone shares lower than"
			    " %u, %u used",
			    SCSLM_GLOBAL_ZONE_SHARES,
			    scslm_global_zone_shares);
		}
		LOG_FED_SLM_INFO2(sys_handle,
		    "SCSLM global zone shares <%u>",
		    scslm_global_zone_shares);
#if SOL_VERSION >= __s10
		/* Apply the change */
		(void) SCSLM_zone_find_shares(GLOBAL_ZONENAME,
		    &tot_shares, NULL);
		ret = SCSLM_modify_zone_shares(GLOBAL_ZONENAME,
		    &tot_shares);
		if (ret == SCSLM_SUCCESS) {
			LOG_FED_SLM_INFO2(sys_handle,
			    "SCSLM set zone <global> shares to %u",
			    tot_shares);
		}
#endif
	}

	if ((args->type == FE_SLM_CONF_PSET_MIN) &&
	    (scslm_default_pset_min != args->pset_min)) {
		scslm_default_pset_min = args->pset_min;
		if (scslm_default_pset_min < SCSLM_DEFAULT_PSET_MIN) {
			scslm_default_pset_min =
			    SCSLM_DEFAULT_PSET_MIN;
			LOG_FED_SLM_INFO3(sys_handle,
			    "SCSLM default pset min lower than"
			    " %u, %u used",
			    SCSLM_DEFAULT_PSET_MIN,
			    scslm_default_pset_min);
		}
		LOG_FED_SLM_INFO2(sys_handle,
		    "SCSLM default pset min size <%u>",
		    scslm_default_pset_min);
#if SOL_VERSION >= __s10
		SCSLM_hdl_default_pset_min();
#endif
	}
	return (err);
}


/*
 * Function SCSLM_check
 * Displays in logs internal data and lists.
 * See scslm_check comments.
 * Args In:
 *	None.
 * Args Out:
 *	None.
 * Return value:
 *	SCSLM_SUCCESS
 */
static int
SCSLM_check(void)
{
	SCSLM_prt_fed();
	return (SCSLM_SUCCESS);
}


/*
 * Function SCSLM_hdl_meth
 * Called only by SCSLM_fe_run(), before method is executed, for all methods
 * except METH_STOP and METH_POSTNET_STOP (handled by SCSLM_hdl_meth_stop()).
 * See comments at the begin of this file for detailled list of actions.
 * Args In:
 *	a fe_handle
 * Args Out:
 *	SCSLM project name or NULL
 * Return value:
 *	SCSLM_SUCCESS or SCSLM errors
 * Called only by function SCSLM_fe_run()
 */
static int
SCSLM_hdl_meth(fe_handle *handle, char **projn)
{
	sc_syslog_msg_handle_t	sys_handle;
	t_SCSLM_rglist	*p;
	t_SCSLM_rlist	*r;
	int		ret;
	int		err;
	unsigned int	shares;		/* shares from CCR */
	unsigned int	fshares = 0;	/* shares in project file */
	unsigned int	found_fshares = 0;
	projid_t	projid;
	projid_t	projid_f;
	char		projname[PROJNAME_MAX + 1];

	*projn = NULL;

	/* Is RG on the fed list ? */
	p = SCSLM_src_RG_fed(handle->zonename, handle->rg_name);
	if (p == NULL) {
		/* No, create it */
		ret = SCSLM_add_RG_fed(handle);
		if (ret != SCSLM_SUCCESS) {
			return (SCSLM_ERROR);
		}
		p = SCSLM_src_RG_fed(handle->zonename, handle->rg_name);
		if (p == NULL) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * Should never occur.
			 * @user_action
			 * Contact your authorized Sun service provider to
			 * determine whether a workaround or patch is
			 * available.
			 */
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM zone <%s> rg_name <%s> "
			    "not found", handle->zonename, handle->rg_name);
			sc_syslog_msg_done(&sys_handle);
			return (SCSLM_ERROR);
		}
	}
	ret = SCSLM_add_R_fed(p, handle, &r);
	if (ret != SCSLM_SUCCESS) {
		return (SCSLM_ERROR);
	}
	/* r is not NULL, because success */

	/* Record project name in fed SLM internal list */
	if (p->proj_name == NULL) {
		ret = snprintf(projname, PROJNAME_MAX + 1, "%s%s",
		    SCSLM_PROJ, p->rg_name);
		err = errno; /*lint !e746 */
		if ((ret >= (PROJNAME_MAX + 1)) || ret < 0) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * Should never occur.
			 * @user_action
			 * Contact your authorized Sun service provider to
			 * determine whether a workaround or patch is
			 * available.
			 */
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM snprintf error <%s>", strerror(err));
			sc_syslog_msg_done(&sys_handle);
			return (SCSLM_ERROR);
		}
		SCSLM_replace_dash_in_str(projname);
		p->proj_name = strdup(projname);
		err = errno; /*lint !e746 */
		if (p->proj_name == NULL) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * Should never occur.
			 * @user_action
			 * Contact your authorized Sun service provider to
			 * determine whether a workaround or patch is
			 * available.
			 */
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM strdup error <%s>", strerror(err));
			sc_syslog_msg_done(&sys_handle);
			return (SCSLM_ERROR);
		}
	}

	/*
	 * A RGM type has mandatroy (METH_START and/or METH_PRENET_START)
	 * and (METH_STOP and/or a METH_POSTNET_STOP).
	 * Otherwise, type creation is refused.
	 */
	if (handle->slm_flags == METH_START ||
	    handle->slm_flags == METH_PRENET_START) {
		if (r->running == 0) {
			/* Save handle rg_slm_type value */
			(void) strcpy(p->rg_slm_type,
			    handle->rg_slm_type);
			/* Save handle rg_slm_pset_type value */
			(void) strcpy(p->rg_slm_pset_type,
			    handle->rg_slm_pset_type);
		}
		r->running |= (1 << (unsigned int)(handle->slm_flags));
		if (scslm_do_log != 0) {
			LOG_FED_SLM_INFO3(sys_handle,
			    "SCSLM_hdl_meth <%s> running set to 0x%x",
			    r->r_name, r->running);
		}
		if (strcmp(p->rg_slm_type, RG_SLM_TYPE_MANUAL) == 0) {
			return (SCSLM_SUCCESS);
		}
	} else {
		if (SCSLM_R_running(p) == 0) {
			(void) strcpy(p->rg_slm_type, RG_SLM_TYPE_UNKNOWN);
		}
		if (strcmp(p->rg_slm_type, RG_SLM_TYPE_UNKNOWN) == 0) {
			if (strcmp(handle->rg_slm_type,
			    RG_SLM_TYPE_MANUAL) == 0) {
				return (SCSLM_SUCCESS);
			}
		}
		if (strcmp(p->rg_slm_type, RG_SLM_TYPE_MANUAL) == 0) {
			return (SCSLM_SUCCESS);
		}
	}

	/* Under SCSLM control */
	ret = SCSLM_lock_file();
	if (ret != SCSLM_SUCCESS) {
		return (SCSLM_ERROR);
	}

	/* Is project already existing ? */
	ret = SCSLM_fgetprojent(p->proj_name, p->rg_zone, &projid, &fshares);
	if (ret == SCSLM_ERROR) {
		(void) SCSLM_unlock_file();
		return (SCSLM_ERROR);
	}
	if (ret == SCSLM_ERROR_CORRUPT_PROJECT) {
		(void) SCSLM_unlock_file();
		return (SCSLM_ERROR);
	}
	if (ret == SCSLM_ERROR_UNKNOWN_PROJECT) {
		/* Create the project */
		ret = SCSLM_create_proj(p->rg_zone, p->proj_name);
		if (ret != SCSLM_SUCCESS &&
		    ret != SCSLM_ERROR_EXISTING_PROJECT) {
			(void) SCSLM_unlock_file();
			return (SCSLM_ERROR);
		}
#if SOL_VERSION >= __s10
		LOG_FED_SLM_INFO3(sys_handle, ""
		    "SCSLM create project <%s> in zone <%s>",
		    p->proj_name, p->rg_zone);
#else
		LOG_FED_SLM_INFO2(sys_handle, ""
		    "SCSLM create project <%s>", p->proj_name);
#endif
#if SOL_VERSION >= __s10
		/*
		 * To be able to call SCSLM_fgetprojent with success,
		 * setup a temporary shares value of 1.
		 */
		ret = SCSLM_modify_proj_shares_file(p->rg_zone,
		    p->proj_name, 1);
		if (ret != SCSLM_SUCCESS) {
			(void) SCSLM_unlock_file();
			return (SCSLM_ERROR);
		}
		ret = SCSLM_fgetprojent(p->proj_name, p->rg_zone,
		    &projid, &fshares);
		if (ret != SCSLM_SUCCESS) {
			(void) SCSLM_unlock_file();
			return (SCSLM_ERROR);
		} else {
			found_fshares = 1;
		}
#else
		projid = getprojidbyname(p->proj_name);
		if (projid == -1) {
			err = errno; /*lint !e746 */
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * Should never occur.
			 * @user_action
			 * Contact your authorized Sun service provider to
			 * determine whether a workaround or patch is
			 * available.
			 */
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM "
			    "getprojidbyname <%s> error <%s>",
			    p->proj_name, strerror(err));
			sc_syslog_msg_done(&sys_handle);
			(void) SCSLM_unlock_file();
			return (SCSLM_ERROR);
		}
#endif /* SOL_VERSION >= __s10 */
		ret = SCSLM_file_del_proj(p->rg_zone, p->proj_name, 0);
		if (ret != SCSLM_ERROR) {
			ret = SCSLM_file_add_proj(
			    p->rg_zone, p->proj_name, projid, p->rg_name);
		}
	} else {
		/* SCSLM_SUCCESS */
		char		*rg;
		int		fd;

		found_fshares = 1;
		/*
		 * Handle the case SCSLM_PROJ_FILE has been deleted
		 * by error.
		 */
		fd = open(SCSLM_PROJ_FILE, O_RDWR | O_SYNC);
		err = errno;
		if (fd < 0 && err == ENOENT) {
			ret = SCSLM_file_add_proj(p->rg_zone,
			    p->proj_name, projid, p->rg_name);
			if (ret == SCSLM_SUCCESS) {
				LOG_FED_SLM_INFO5(sys_handle,
				    "SCSLM file <%s> not found, create entry "
				    "<%s> zone <%s> projid %u",
				    SCSLM_PROJ_FILE, p->proj_name,
				    p->rg_zone, (unsigned int)projid);
			}
		}
		if (fd > 0) {
			(void) close(fd);
		}

		if (SCSLM_file_open_proj() == SCSLM_SUCCESS) {
			ret = SCSLM_file_srch_proj(p->rg_zone,
			    p->proj_name, &projid_f, &rg);
			if (ret == SCSLM_SUCCESS) {
				if (projid != projid_f) {
					LOG_FED_SLM_INFO5(sys_handle,
					    "SCSLM WARNING: project "
					    "<%s> zone <%s> projid (%u) "
					    "is different (%u) "
					    "from initial setup",
					    p->proj_name,
					    p->rg_zone,
					    (unsigned int)projid,
					    (unsigned int)projid_f);
				}
				free(rg);
			} else {
				if (ret == SCSLM_ERROR_UNKNOWN_PROJECT) {
					ret = SCSLM_file_add_proj(p->rg_zone,
					    p->proj_name, projid, p->rg_name);
					if (ret == SCSLM_SUCCESS) {
						LOG_FED_SLM_INFO5(sys_handle,
						    "SCSLM create zone <%s> "
						    "proj <%s> "
						    "projid %u in file %s",
						    p->rg_zone,
						    p->proj_name,
						    (unsigned int)projid,
						    SCSLM_PROJ_FILE);
					}
				}
			}
			(void) SCSLM_file_close_proj();
		}
	}

	/* Apply project shares */
	shares = p->cpu;

	if ((found_fshares == 0) ||
	    ((found_fshares != 0) && (fshares != shares))) {
		ret = SCSLM_modify_proj_shares_file(
		    p->rg_zone, p->proj_name, shares);
		if (ret != SCSLM_SUCCESS) {
			(void) SCSLM_unlock_file();
			return (SCSLM_ERROR);
		}
#if SOL_VERSION >= __s10
		(void) SCSLM_modify_proj_shares_mem(
		    p->rg_zone, p->proj_name, shares);
#else
		(void) SCSLM_modify_proj_shares_mem(
		    p->rg_name, p->proj_name, shares);
#endif
#if SOL_VERSION >= __s10
		LOG_FED_SLM_INFO4(sys_handle,
		    "SCSLM set project <%s> shares in zone <%s> to %u",
		    p->proj_name, p->rg_zone, shares);
#else
		LOG_FED_SLM_INFO3(sys_handle,
		    "SCSLM set project <%s> shares to %u",
		    p->proj_name, shares);
#endif
	}
	(void) SCSLM_unlock_file();

#if SOL_VERSION >= __s10
	/*
	 * A RGM type has mandatroy (METH_START and/or METH_PRENET_START)
	 * and (METH_STOP and/or a METH_POSTNET_STOP).
	 * Otherwise, type creation is refused.
	 */
	if (((handle->slm_flags == METH_START) ||
	    (handle->slm_flags == METH_PRENET_START)) &&
	    ((r->running &
	    ((1 << METH_START) | (1 << METH_PRENET_START))) != 0)) {
		/*
		 * Create a dedicated pset only for a start and/or a
		 * prenet_start method, and only once.
		 */
		ret = SCSLM_hdl_meth_S10(p, r);
		if (ret != SCSLM_SUCCESS) {
			r->running &= (~((unsigned int)1 <<
			    (unsigned int)(handle->slm_flags)));
			if (scslm_do_log != 0) {
				LOG_FED_SLM_INFO3(sys_handle,
				    "SCSLM_hdl_meth <%s> running set to 0x%x",
				    r->r_name, r->running);
			}
			return (SCSLM_ERROR);
		}
	}
#endif /* SOL_VERSION >= __s10 */
	*projn = p->proj_name;
	return (SCSLM_SUCCESS);
}


/*
 * Function SCSLM_hdl_meth_stop
 * Called only by SCSLM_fe_run(), before method is executed, only for methods
 * METH_STOP and METH_POSTNET_STOP (all others methods are handled by
 * SCSLM_hdl_meth()).
 * See comments at the begin of this file for detailled list of actions.
 * Args In:
 *	a fe_handle
 * Args Out:
 *	fe_handle->do_slm_stop and project name
 * Return value:
 *	SCSLM_SUCCESS or SCSLM errors
 * Called only by function SCSLM_fe_run()
 */
static int
SCSLM_hdl_meth_stop(fe_handle *handle, char **projn)
{
	sc_syslog_msg_handle_t	sys_handle;
	t_SCSLM_rglist	*p;
	t_SCSLM_rlist	*r;
	projid_t	projid;
	projid_t	projid_f;
	unsigned int	shares;
	char		*rg;
	int		ret;

	*projn = NULL;

	p = SCSLM_src_RG_fed(handle->zonename, handle->rg_name);
	if (p == NULL) {
		/*
		 * Always accept METH_STOP or METH_POSTNET_STOP
		 * this occurs in the following case only:
		 * RGs under SCSLM control are online on this node,
		 * failfast is disabled, and someone kill rpc.fed and
		 * relaunch rpc.fed, and now RGs under SCSLM are switched.
		 * It is mainly a debug feature, you can relaunch rpc.fed
		 * even having RGs under SLM control online and still stop
		 * them.
		 */
		return (SCSLM_SUCCESS);
	}
	r = SCSLM_src_R_fed(handle->r_name, p);
	if (r == NULL) {
		/*
		 * Always accept METH_STOP or METH_POSTNET_STOP
		 * This occurs in the following case only:
		 * RGs under SCSLM control are online on this node,
		 * failfast is disabled, and someone kill rpc.fed and
		 * relaunch rpc.fed, and now RGs under SCSLM are switched.
		 * It is mainly a debug feature, you can relaunch rpc.fed
		 * even having RGs under SLM control online and still stop
		 * them.
		 */
		return (SCSLM_SUCCESS);
	}

	if (strcmp(p->rg_slm_type, RG_SLM_TYPE_UNKNOWN) == 0) {
		if (strcmp(handle->rg_slm_type, RG_SLM_TYPE_MANUAL) == 0) {
			if (r->running == 0) {
				return (SCSLM_SUCCESS);
			}
			handle->do_slm_stop = 1;
			return (SCSLM_SUCCESS);
		}
	}
	if (strcmp(p->rg_slm_type, RG_SLM_TYPE_MANUAL) == 0) {
		if (r->running == 0) {
			return (SCSLM_SUCCESS);
		}
		handle->do_slm_stop = 1;
		return (SCSLM_SUCCESS);
	}

	/* Verify project */
	ret = SCSLM_lock_file();
	if (ret != SCSLM_SUCCESS) {
		return (SCSLM_ERROR);
	}
	ret = SCSLM_fgetprojent(p->proj_name, p->rg_zone, &projid, &shares);
	if (ret == SCSLM_ERROR) {
		(void) SCSLM_unlock_file();
		return (SCSLM_ERROR);
	}
	if (ret == SCSLM_ERROR_CORRUPT_PROJECT) {
		(void) SCSLM_unlock_file();
		return (SCSLM_ERROR);
	}
	if (ret == SCSLM_ERROR_UNKNOWN_PROJECT) {
		(void) SCSLM_unlock_file();
		return (SCSLM_ERROR);
	}

	/* Verify project id, just a warning */
	if (SCSLM_file_open_proj() == SCSLM_SUCCESS) {
		ret = SCSLM_file_srch_proj(p->rg_zone,
		    p->proj_name, &projid_f, &rg);
		if (ret == SCSLM_SUCCESS) {
			if (projid != projid_f) {
				LOG_FED_SLM_INFO5(sys_handle,
				    "SCSLM WARNING: project "
				    "<%s> zone <%s> projid (%u) "
				    "is different (%u) "
				    "from initial setup",
				    p->proj_name,
				    p->rg_zone,
				    (unsigned int)projid,
				    (unsigned int)projid_f);
			}
			free(rg);
		} else {
			if (ret == SCSLM_ERROR_UNKNOWN_PROJECT) {
				ret = SCSLM_file_add_proj(p->rg_zone,
				    p->proj_name, projid, p->rg_name);
				if (ret == SCSLM_SUCCESS) {
					LOG_FED_SLM_INFO5(sys_handle,
					    "SCSLM create zone <%s> "
					    "proj <%s> "
					    "projid %u in file %s",
					    p->rg_zone,
					    p->proj_name,
					    (unsigned int)projid,
					    SCSLM_PROJ_FILE);
				}
			}
		}
		(void) SCSLM_file_close_proj();
	}
	(void) SCSLM_unlock_file();

	if (r->running == 0) {
		*projn = p->proj_name;
		return (SCSLM_SUCCESS);
	}

	/*
	 * A RGM type has mandatroy (METH_START and/or METH_PRENET_START)
	 * and (METH_STOP and/or a METH_POSTNET_STOP).
	 * Otherwise, type creation is refused.
	 */
	if ((r->running &
	    ((1 << METH_START) | (1 << METH_PRENET_START))) == 0) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Should never occur.
		 * @user_action
		 * Contact your authorized Sun service provider to determine
		 * whether a workaround or patch is available.
		 */
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM <%s> meth 0x%x with running 0x%x",
		    r->r_name, handle->slm_flags, r->running);
		sc_syslog_msg_done(&sys_handle);
		return (SCSLM_ERROR);
	}
	handle->do_slm_stop = 1;
	*projn = p->proj_name;
	return (SCSLM_SUCCESS);
}



#if SOL_VERSION >= __s10
/*
 * -----------------------------------------------------------------------------
 *
 * Main fed routines for Service Level Management for Solaris >= S10
 *
 * -----------------------------------------------------------------------------
 */

/*
 * Function SCSLM_hdl_meth_S10
 * Called only by SCSLM_hdl_meth(). Pools/Psets/zone.cpu-shares handling.
 * Called only for methods METH_START or METH_PRENET_START when Solaris >= S10.
 * See comments at the begin of this file for detailled list of actions.
 * Args In:
 *	a pointer to rg list
 *	a pointer to the resource list
 * Args Out:
 *	None
 * Return value:
 *	SCSLM_SUCCESS: done
 *	SCSLM_ERROR: error
 * Called only by function SCSLM_hdl_meth()
 */
static int
SCSLM_hdl_meth_S10(t_SCSLM_rglist *p, t_SCSLM_rlist *r)
{
	sc_syslog_msg_handle_t	sys_handle;
	unsigned int	tot_shares;
	int		ret;
	int		dedicated;

	ret = SCSLM_zone_find_shares(p->rg_zone, &tot_shares, NULL);
	if (ret != SCSLM_SUCCESS) {
		return (ret);
	}
	ret = SCSLM_modify_zone_shares(p->rg_zone, &tot_shares);
	if (ret != SCSLM_SUCCESS) {
		/*
		 * TODO:
		 * 6364293 remove Solaris bug 6201729 workarroud in fed_slm.c
		 *
		 * SunCluster uses currently S10 build74L2a, in which
		 * there is the following Solaris bug:
		 * "6201729 prctl fails to set resource controls on
		 * some processes: Not owner"
		 * This bug is targeted for solaris_10u1, when SunCluster
		 * moves to solaris_10u1 or above, and 6201729 has a fix,
		 * ifdef SOLARIS_BUG_6201729_FIXED can be removed.
		 * We curently simply log the message "prctl... Not owner"
		 * returned by the prctl command and ignore it. This
		 * concerns only dynamic project shares or zone shares
		 * changes, that may not be as appropriate due to this bug.
		 */
#ifdef SOLARIS_BUG_6201729_FIXED
		return (ret);
#endif
	}
	LOG_FED_SLM_INFO3(sys_handle,
	    "SCSLM set zone <%s> shares to %u",
	    p->rg_zone, tot_shares);
	if (strcmp(p->rg_zone, GLOBAL_ZONENAME) == 0) {
		return (SCSLM_SUCCESS);
	}

	/*
	 * Handle local zone case
	 */
	ret = SCSLM_pools_ok();
	if (ret == SCSLM_POOLS_DISABLED) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Should never occur.
		 * @user_action
		 * Contact your authorized Sun service provider to determine
		 * whether a workaround or patch is available.
		 */
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM pools facility is disabled");
		sc_syslog_msg_done(&sys_handle);
		return (SCSLM_ERROR);
	}
	if (ret == SCSLM_POOLS_NO_CONF) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Should never occur.
		 * @user_action
		 * Contact your authorized Sun service provider to determine
		 * whether a workaround or patch is available.
		 */
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM pools facility, no static config file");
		sc_syslog_msg_done(&sys_handle);
		return (SCSLM_ERROR);
	}
	if (ret != SCSLM_SUCCESS) {
		return (SCSLM_ERROR);
	}

	/*
	 * Temporary, should be done by scrgadm
	 * weak and strong cannot run at the same
	 * time in same zone
	 */
	dedicated = SCSLM_check_dedicated_pset(p);

	if ((dedicated == SCSLM_IS_DEDICATED_WEAK) &&
	    (strcmp(p->rg_slm_pset_type,
	    RG_SLM_PSET_TYPE_DEDICATED_STRONG) == 0)) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Should never occur.
		 * @user_action
		 * Contact your authorized Sun service provider to determine
		 * whether a workaround or patch is available.
		 */
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM "
		    "<%s> zone <%s> strong while weak running",
		    r->r_name, p->rg_zone);
		sc_syslog_msg_done(&sys_handle);
		return (SCSLM_ERROR);
	}
	if ((dedicated == SCSLM_IS_DEDICATED_STRONG) &&
	    (strcmp(p->rg_slm_pset_type,
	    RG_SLM_PSET_TYPE_DEDICATED_WEAK) == 0)) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Should never occur.
		 * @user_action
		 * Contact your authorized Sun service provider to determine
		 * whether a workaround or patch is available.
		 */
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM "
		    "<%s> zone <%s> weak while strong running",
		    r->r_name, p->rg_zone);
		sc_syslog_msg_done(&sys_handle);
		return (SCSLM_ERROR);
	}

	dedicated = SCSLM_in_dedicated_pset(p);

	/*
	 * Create the pool and set binding
	 */
	ret = SCSLM_hdl_pool(p);
	if (ret != SCSLM_SUCCESS) {
		return (ret);
	}

	switch (dedicated) {
		case SCSLM_IS_DEDICATED_STRONG:
			ret = SCSLM_hdl_strong(p);
			break;
		case SCSLM_IS_DEDICATED_WEAK:
			ret = SCSLM_hdl_weak(p);
			break;
		case SCSLM_IS_NOT_DEDICATED:
			ret = SCSLM_SUCCESS;
			break;
		default:
			ret = dedicated;
			break;
	}
	return (ret);
}


/*
 * Function SCSLM_stop_S10
 * Called only for methods METH_STOP or METH_POSTNET_STOP,
 * after method has been executed, for Solaris >= S10.
 * See comments at the begin of this file for detailled list of actions.
 * Args In:
 *	RG pointer, projname
 * Args Out:
 *	None
 * Return value:
 *	SCSLM_SUCCESS: done
 *	SCSLM_ERROR: error
 * Called only by function SCSLM_stop().
 */
static int
SCSLM_stop_S10(t_SCSLM_rglist *p, t_SCSLM_rlist *r)
{
	sc_syslog_msg_handle_t	sys_handle;
	unsigned int	shares;
	unsigned int	zshares;
	int		ret;
	unsigned int	num_cpu;
	unsigned int	min_cpu;

	SCSLM_del_pset_in_zone(p);

	ret = SCSLM_zone_find_shares(p->rg_zone, &shares, &min_cpu);
	if (ret != SCSLM_SUCCESS) {
		return (ret);
	}
	zshares = shares;
	ret = SCSLM_modify_zone_shares(p->rg_zone, &zshares);
	if (ret != SCSLM_SUCCESS) {
		/*
		 * TODO:
		 * 6364293 remove Solaris bug 6201729 workarroud in fed_slm.c
		 *
		 * SunCluster uses currently S10 build74L2a, in which
		 * there is the following Solaris bug:
		 * "6201729 prctl fails to set resource controls on
		 * some processes: Not owner"
		 * This bug is targeted for solaris_10u1, when SunCluster
		 * moves to solaris_10u1 or above, and 6201729 has a fix,
		 * ifdef SOLARIS_BUG_6201729_FIXED can be removed.
		 * We curently simply log the message "prctl... Not owner"
		 * returned by the prctl command and ignore it. This
		 * concerns only dynamic project shares or zone shares
		 * changes, that may not be as appropriate due to this bug.
		 */
#ifdef SOLARIS_BUG_6201729_FIXED
		return (ret);
#endif
	}
	LOG_FED_SLM_INFO3(sys_handle,
	    "SCSLM set zone <%s> shares to %u",
	    p->rg_zone, zshares);
	if (shares == 0) {
		/* Delete the pool */
		ret = SCSLM_delete_pool(p->rg_zone);
		if (ret == SCSLM_SUCCESS) {
			LOG_FED_SLM_INFO3(sys_handle,
			    "SCSLM delete pool <%s%s>", SCSLM_POOL, p->rg_zone);
		}
		/* error is not fatal */
	}
	if (shares == 0 && p->pset_cpu != 0) {
		/* Remove the pset */
		ret = SCSLM_delete_pset(p->rg_zone);
		if (ret != SCSLM_SUCCESS) {
			return (ret);
		}
		LOG_FED_SLM_INFO3(sys_handle,
		    "SCSLM delete pset <%s%s>",
		    SCSLM_PSET, p->rg_zone);
		/* There is no more pset dedicated */
		ret = SCSLM_set_pset_in_zone(p,
		    0, 0, SCSLM_PSET_NONE);
		if (ret != SCSLM_SUCCESS) {
			return (ret);
		}
		return (SCSLM_SUCCESS);
	}
	if (shares != 0 && p->pset_cpu != 0 && p->pset == SCSLM_PSET_NONE) {
		/* Remove the pset */
		ret = SCSLM_delete_pset(p->rg_zone);
		if (ret != SCSLM_SUCCESS) {
			return (ret);
		}
		LOG_FED_SLM_INFO3(sys_handle,
		    "SCSLM delete pset <%s%s>",
		    SCSLM_PSET, p->rg_zone);
		/* There is no more pset dedicated */
		ret = SCSLM_set_pset_in_zone(p,
		    0, 0, SCSLM_PSET_NONE);
		if (ret != SCSLM_SUCCESS) {
			return (ret);
		}
		return (SCSLM_SUCCESS);
	}
	if (shares != 0 && p->pset_cpu != 0) {
		/* In a dedicated pset */
		if (shares % 100 == 0) {
			num_cpu = (shares / 100);
		} else {
			num_cpu = (shares / 100) + 1;
		}
		if (num_cpu == 0) {
			num_cpu = 1;
		}
		if (num_cpu == p->pset_cpu && min_cpu == p->pset_cpu_min) {
			return (SCSLM_SUCCESS);
		}
		/* We must lower the pset size */
		ret = SCSLM_modify_pset(p->rg_zone,
		    (uint64_t)min_cpu, (uint64_t)num_cpu, 1);
		if (ret != SCSLM_SUCCESS) {
			return (ret);
		}
		LOG_FED_SLM_INFO5(sys_handle,
		    "SCSLM modify pset <%s%s> min=%u,max=%u",
		    SCSLM_PSET, p->rg_zone, min_cpu, num_cpu);
		ret = SCSLM_set_pset_in_zone(p,
		    min_cpu, num_cpu, SCSLM_PSET_UNMODIFIED);
		if (ret != SCSLM_SUCCESS) {
			return (ret);
		}
		return (SCSLM_SUCCESS);
	}
	if ((shares == 0) && (p->pset == SCSLM_PSET_WEAK_DELETED)) {
		ret = SCSLM_set_pset_in_zone(p,
		    0, 0, SCSLM_PSET_NONE);
		if (ret != SCSLM_SUCCESS) {
			return (ret);
		}
		return (SCSLM_SUCCESS);
	}
	if ((shares != 0) && (p->pset == SCSLM_PSET_WEAK_DELETED) &&
	    (p->pset == SCSLM_PSET_NONE)) {
		ret = SCSLM_set_pset_in_zone(p,
		    0, 0, SCSLM_PSET_NONE);
		if (ret != SCSLM_SUCCESS) {
			return (ret);
		}
		return (SCSLM_SUCCESS);
	}
	if ((shares == 0) && (p->pset != SCSLM_PSET_NONE)) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Should never occur.
		 * @user_action
		 * Contact your authorized Sun service provider to determine
		 * whether a workaround or patch is available.
		 */
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM "
		    "<%s> shares 0 pset %d",
		    r->r_name, p->pset);
		sc_syslog_msg_done(&sys_handle);
		return (SCSLM_ERROR);
	}
	return (SCSLM_SUCCESS);
}


/*
 * Function SCSLM_hdl_strong
 * Handle RG_SLM_PSET_TYPE strong, create or modify a strong pset
 * Args In:
 *	a pointer to rg list
 *	a pset type
 * Args Out:
 *	None
 * Return value:
 *	SCSLM_SUCCESS: done
 *	SCSLM_ERROR: unable to create or modify pset as asked
 */
static int
SCSLM_hdl_strong(t_SCSLM_rglist *rg)
{
	sc_syslog_msg_handle_t	sys_handle;
	int		ret;
	unsigned int	shares;
	unsigned int	num_cpu;
	unsigned int	count = 0;
	unsigned int	min_cpu;

	ret = SCSLM_zone_find_shares(rg->rg_zone, &shares, &min_cpu);
	if (ret != SCSLM_SUCCESS) {
		return (SCSLM_ERROR);
	}
	if (shares % 100 == 0) {
		num_cpu = (shares / 100);
	} else {
		num_cpu = (shares / 100) + 1;
	}
	if (num_cpu == 0) {
		num_cpu = 1;
	}
SCSLM_hdl_strong_again:
	if (rg->pset == SCSLM_PSET_NONE) {
		/* Create the pset */
		ret = SCSLM_create_pset(rg->rg_zone,
		    (uint64_t)min_cpu, (uint64_t)num_cpu);
		if (ret != SCSLM_SUCCESS) {
			if (count == 0) {
				count = 1;
				(void) SCSLM_destroy_weak_psets();
				SCSLM_hdl_default_pset_min();
				goto SCSLM_hdl_strong_again;
			}
			(void) sc_syslog_msg_initialize(
			    &sys_handle, SC_SYSLOG_RGM_FED_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * Requested number of cpus cannot be set for this
			 * resource group on this node.
			 * @user_action
			 * Switch the resource group on a node with enough
			 * cpus or lower RG_SLM_CPU_SHARES andor RG_SLM_PSET_MIN
			 * properties values or move RG_SLM_type to manual and
			 * restart the resource group.
			 */
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM cannot create pset <%s%s> "
			    "min=%u,max=%u",
			    SCSLM_PSET, rg->rg_zone, min_cpu, num_cpu);
			sc_syslog_msg_done(&sys_handle);
			return (SCSLM_ERROR);
		}
		LOG_FED_SLM_INFO5(sys_handle,
		    "SCSLM create strong pset <%s%s> min=%u,max=%u",
		    SCSLM_PSET, rg->rg_zone, min_cpu, num_cpu);
		ret = SCSLM_modify_pool_pset(rg->rg_zone, rg->rg_zone);
		if (ret != SCSLM_SUCCESS) {
			(void) SCSLM_delete_pset(rg->rg_zone);
			return (SCSLM_ERROR);
		}
		LOG_FED_SLM_INFO5(sys_handle,
		    "SCSLM associate pool <%s%s> to pset <%s%s>",
		    SCSLM_POOL, rg->rg_zone, SCSLM_PSET, rg->rg_zone);
		/* Set pset in RG */
		ret = SCSLM_set_pset_in_zone(rg,
		    min_cpu, num_cpu, SCSLM_PSET_STRONG);
		if (ret != SCSLM_SUCCESS) {
			return (SCSLM_ERROR);
		}
		return (SCSLM_SUCCESS);
	}
	if (num_cpu != rg->pset_cpu || min_cpu != rg->pset_cpu_min) {
		/* pset exists, adjust the pset size */
		ret = SCSLM_modify_pset(rg->rg_zone,
		    (uint64_t)min_cpu, (uint64_t)num_cpu, 1);
		if (ret != SCSLM_SUCCESS) {
			if (count == 0) {
				count = 1;
				(void) SCSLM_destroy_weak_psets();
				SCSLM_hdl_default_pset_min();
				goto SCSLM_hdl_strong_again;
			}
			(void) sc_syslog_msg_initialize(
			    &sys_handle, SC_SYSLOG_RGM_FED_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * Requested number of cpus cannot be set for this
			 * resource group on this node.
			 * @user_action
			 * Switch the resource group on a node with enough
			 * cpus or lower RG_SLM_CPU_SHARES andor RG_SLM_PSET_MIN
			 * properties values or move RG_SLM_type to manual and
			 * restart the resource group.
			 */
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM cannot modify pset <%s%s> to "
			    "min=%u,max=%u",
			    SCSLM_PSET, rg->rg_zone, min_cpu, num_cpu);
			sc_syslog_msg_done(&sys_handle);
			return (SCSLM_ERROR);
		}
		LOG_FED_SLM_INFO5(sys_handle,
		    "SCSLM modify strong pset <%s%s> min=%u,max=%u",
		    SCSLM_PSET, rg->rg_zone, min_cpu, num_cpu);
		ret = SCSLM_set_pset_in_zone(rg,
		    min_cpu, num_cpu, SCSLM_PSET_STRONG);
		if (ret != SCSLM_SUCCESS) {
			return (SCSLM_ERROR);
		}
	}
	return (SCSLM_SUCCESS);
}


/*
 * Function SCSLM_hdl_weak
 * Handle RG_SLM_PSET_TYPE weak, create/delete/modify a weak pset
 * Args In:
 *	a pointer to rg list
 *	pset type
 * Args Out:
 *	None
 * Return value:
 *	SCSLM_SUCCESS: done
 *	SCSLM_ERROR: unable to create or modify pset as asked
 */
static int
SCSLM_hdl_weak(t_SCSLM_rglist *rg)
{
	sc_syslog_msg_handle_t	sys_handle;
	int		ret;
	unsigned int	shares;
	unsigned int	num_cpu;
	unsigned int	min_cpu;

	if (rg->pset == SCSLM_PSET_WEAK_DELETED) {
		return (SCSLM_SUCCESS);
	}
	ret = SCSLM_zone_find_shares(rg->rg_zone, &shares, &min_cpu);
	if (ret != SCSLM_SUCCESS) {
		return (SCSLM_ERROR);
	}
	if (shares % 100 == 0) {
		num_cpu = (shares / 100);
	} else {
		num_cpu = (shares / 100) + 1;
	}
	if (num_cpu == 0) {
		num_cpu = 1;
	}
	if (rg->pset == SCSLM_PSET_NONE && num_cpu != 0) {
		/* Create the pset */
		ret = SCSLM_create_pset(rg->rg_zone,
		    (uint64_t)min_cpu, (uint64_t)num_cpu);
		if (ret != SCSLM_SUCCESS) {
			ret = SCSLM_set_pset_in_zone(rg,
			    0, 0, SCSLM_PSET_WEAK_DELETED);
			if (ret != SCSLM_SUCCESS) {
				return (SCSLM_ERROR);
			}
			LOG_FED_SLM_INFO3(sys_handle,
			    "SCSLM no weak pset for rg <%s> in zone <%s>",
			    rg->rg_name, rg->rg_zone);
			return (SCSLM_SUCCESS);
		}
		LOG_FED_SLM_INFO5(sys_handle,
		    "SCSLM create weak pset <%s%s> min=%u,max=%u",
		    SCSLM_PSET, rg->rg_zone, min_cpu, num_cpu);
		ret = SCSLM_modify_pool_pset(rg->rg_zone, rg->rg_zone);
		if (ret != SCSLM_SUCCESS) {
			(void) SCSLM_delete_pset(rg->rg_zone);
			return (SCSLM_ERROR);
		}
		LOG_FED_SLM_INFO5(sys_handle,
		    "SCSLM associate pool <%s%s> to pset <%s%s>",
		    SCSLM_POOL, rg->rg_zone, SCSLM_PSET, rg->rg_zone);
		/* Set pset in RG */
		ret = SCSLM_set_pset_in_zone(rg,
		    min_cpu, num_cpu, SCSLM_PSET_WEAK);
		if (ret != SCSLM_SUCCESS) {
			return (SCSLM_ERROR);
		}
	}
	if (rg->pset == SCSLM_PSET_WEAK &&
	    num_cpu != 0 && (num_cpu != rg->pset_cpu ||
	    min_cpu != rg->pset_cpu_min)) {
		/* pset exists, adjust the pset size */
		ret = SCSLM_modify_pset(rg->rg_zone,
		    (uint64_t)min_cpu, (uint64_t)num_cpu, 1);
		if (ret != SCSLM_SUCCESS) {
			ret = SCSLM_delete_pset(rg->rg_zone);
			if (ret != SCSLM_SUCCESS) {
				return (SCSLM_ERROR);
			}
			LOG_FED_SLM_INFO3(sys_handle,
			    "SCSLM delete weak pset <%s%s>",
			    SCSLM_PSET, rg->rg_zone);
			/* There is no more pset dedicated */
			ret = SCSLM_set_pset_in_zone(rg,
			    0, 0, SCSLM_PSET_WEAK_DELETED);
			if (ret != SCSLM_SUCCESS) {
				return (SCSLM_ERROR);
			}
			LOG_FED_SLM_INFO3(sys_handle,
			    "SCSLM no weak pset for rg <%s> in zone <%s>",
			    rg->rg_name, rg->rg_zone);
			return (SCSLM_SUCCESS);
		}
		LOG_FED_SLM_INFO5(sys_handle,
		    "SCSLM modify weak pset <%s%s> min=%u,max=%u",
		    SCSLM_PSET, rg->rg_zone, min_cpu, num_cpu);
		ret = SCSLM_set_pset_in_zone(rg,
		    min_cpu, num_cpu, SCSLM_PSET_WEAK);
		if (ret != SCSLM_SUCCESS) {
			return (SCSLM_ERROR);
		}
	}
	return (SCSLM_SUCCESS);
}


/*
 * Function SCSLM_destroy_weak_psets
 * Delete all weak psets
 * Args In:
 *	None
 * Args Out:
 *	None
 * Return value:
 *	SCSLM_SUCCESS: done
 *	SCSLM_ERROR: error
 */
static int
SCSLM_destroy_weak_psets(void)
{
	sc_syslog_msg_handle_t	sys_handle;
	t_SCSLM_rglist		*rg;
	int		ret;
	int		ret_value = SCSLM_SUCCESS;

	rg = fedRGHead;
	while (rg != NULL) {
		if (strcmp(rg->rg_zone, GLOBAL_ZONENAME) == 0) {
			rg = rg->next;
			continue;
		}
		if (strcmp(rg->rg_slm_type, RG_SLM_TYPE_AUTO) != 0) {
			rg = rg->next;
			continue;
		}
		if (SCSLM_running_in_RG(rg->rg_zone, rg->rg_name) !=
		    SCSLM_SUCCESS) {
			rg = rg->next;
			continue;
		}
		ret = SCSLM_in_dedicated_pset(rg);
		if (ret == SCSLM_IS_NOT_DEDICATED) {
			rg = rg->next;
			continue;
		}
		if (ret == SCSLM_IS_DEDICATED_STRONG) {
			rg = rg->next;
			continue;
		}
		if (ret != SCSLM_IS_DEDICATED_WEAK) {
			rg = rg->next;
			ret_value = SCSLM_ERROR;
			continue;
		}
		if (rg->pset != SCSLM_PSET_WEAK) {
			rg = rg->next;
			continue;
		}
		ret = SCSLM_delete_pset(rg->rg_zone);
		if (ret != SCSLM_SUCCESS) {
			ret_value = ret;
		} else {
			LOG_FED_SLM_INFO3(sys_handle,
			    "SCSLM delete weak pset <%s%s>",
			    SCSLM_PSET, rg->rg_zone);
			ret = SCSLM_set_pset_in_zone(rg,
			    0, 0, SCSLM_PSET_WEAK_DELETED);
			if (ret != SCSLM_SUCCESS) {
				ret_value = ret;
			}
		}
		rg = rg->next;
	}
	return (ret_value);
}


/*
 * Function SCSLM_hdl_default_pset_min
 * Handle the default pset min cpu size
 * Args In:
 * Args Out:
 *	None
 * Return value:
 *	None
 */
static void
SCSLM_hdl_default_pset_min(void)
{
	sc_syslog_msg_handle_t	sys_handle;
	unsigned int		min;
	int			ret = SCSLM_ERROR;
	int			removed_weak = 0;
	static unsigned int	last_asked = 0;

	/*
	 * Try to set pset_default pset.min to scslm_default_pset_min.
	 * Call SCSLM_modify_pset() with last parameter 'show_error' to 0,
	 * (no errors are logged), as it is not an error if we cannot put the
	 * desired value, it is a wished value.
	 *
	 * If seting scslm_default_pset_min fails, first, remove every SCSLM
	 * created weak pset, if any, this is only done once. This should
	 * always succed, if not, SCSLM_destroy_weak_psets() will itself
	 * log errors if there is a problem to remove SCSLM created weak psets.
	 * Trying SCSLM_destroy_weak_psets() several times is useless, it should
	 * always succed, if not, there is a major problem with pool
	 * configuration or poold.
	 *
	 * Then try to set scslm_default_pset_min - 1 (with no errors logging),
	 * if it fails, try scslm_default_pset_min - 2, etc, stoping at 2.
	 *
	 * Last, it should always be possible to set pset_min to 1,
	 * if not, there is a major problem with pool configuration or poold.
	 * Log the error in  this case (we call SCSLM_modify_pset with pset
	 * 1 and last parameter 'show_error' set to 1. This will log why can't
	 * we set pset.min to 1 (as pset_defaut cannot be deleted and a pset has
	 * necessary at least 1 cpu, there is a major problem with poold or pool
	 * configuration, this should always be possible.
	 *
	 * Note that SCSLM_modify_pset() handles the fact that pset.min and
	 * pset.max have already the expected values, it this case, the function
	 * does not commit any new poold configuration: it is already as asked.
	 */

	min = scslm_default_pset_min;
	if (min > (unsigned int)sysconf(_SC_NPROCESSORS_CONF)) {
		min = (unsigned int)sysconf(_SC_NPROCESSORS_CONF);
	}
	while (min > 1) {
		/*
		 * This can fail, but we are not interested about the error,
		 * last parameter is 0: do not log any error message.
		 */
		ret = SCSLM_modify_pset("pset_default",
		    (uint64_t)min, (uint64_t)-1, 0);
		if (ret != SCSLM_SUCCESS) {
			if (removed_weak == 0) {
				/*
				 * Remove all SCSLM created weak psets, if any.
				 * This function will itself log errors.
				 * If SCSLM weak psets exists, delete them will
				 * free cpus, that we try to give to
				 * pset_default as pset.min.
				 */
				(void) SCSLM_destroy_weak_psets();
				removed_weak = 1;
			} else {
				min = min - 1;
			}
		} else {
			break;
		}
	}
	if (ret == SCSLM_SUCCESS) {
		/* pset_min > 1 has been successfully set */
		goto SCSLM_hdl_default_pset_min_end;
	}
	/*
	 * Set pset_default pset.min to 1 should always
	 * be possibe, (see comments at begining of function), so log
	 * errors, last parameter of call 'show_error' is 1.
	 */
	ret = SCSLM_modify_pset("pset_default",
	    (uint64_t)1, (uint64_t)-1, 1);

SCSLM_hdl_default_pset_min_end:
	if (ret == SCSLM_SUCCESS &&
	    min != scslm_default_pset_min &&
	    last_asked != scslm_default_pset_min) {
		last_asked = scslm_default_pset_min;
		/*
		 * Signal that desired value cannot
		 * be set, it is less.
		 */
		LOG_FED_SLM_INFO3(sys_handle, "SCSLM WARNING "
		    "pset_default pset.min %u, asked %u",
		    min, scslm_default_pset_min);
	}
}


/*
 * Function SCSLM_check_dedicated_pset
 * Is a resource group in a dedicated pset ?
 * Args In:
 * 	a rg name
 * Args Out:
 *	None
 * Return value:
 *	SCSLM_IS_DEDICATED_WEAK: rg is in a weak dedicated pset
 *	SCSLM_IS_DEDICATED_STRONG: rg is in a strong dedicated pset
 *	SCSLM_IS_NOT_DEDICATED: rg is not in a dedicated pset
 */
static int
SCSLM_check_dedicated_pset(t_SCSLM_rglist *rg)
{
	t_SCSLM_rglist		*tmp;

	if (strcmp(rg->rg_zone, GLOBAL_ZONENAME) == 0) {
		return (SCSLM_IS_NOT_DEDICATED);
	}
	tmp = fedRGHead;
	while (tmp != NULL) {
		if (tmp == rg) {
			tmp = tmp->next;
			continue;
		}
		if ((strcmp(tmp->rg_zone, rg->rg_zone) == 0) &&
		    (strcmp(tmp->rg_slm_type, RG_SLM_TYPE_AUTO) == 0) &&
		    (SCSLM_R_running(tmp) != 0)) {
			if (strcmp(tmp->rg_slm_pset_type,
			    RG_SLM_PSET_TYPE_DEDICATED_WEAK) == 0) {
				return (SCSLM_IS_DEDICATED_WEAK);
			}
			if (strcmp(tmp->rg_slm_pset_type,
			    RG_SLM_PSET_TYPE_DEDICATED_STRONG) == 0) {
				return (SCSLM_IS_DEDICATED_STRONG);
			}
		}
		tmp = tmp->next;
	}
	return (SCSLM_IS_NOT_DEDICATED);
}


/*
 * Function SCSLM_in_dedicated_pset
 * Is a resource group in a dedicated pset ?
 * Args In:
 * 	a rg name
 * Args Out:
 *	pset type
 * Return value:
 *	SCSLM_IS_DEDICATED_WEAK: rg is in a weak dedicated pset
 *	SCSLM_IS_DEDICATED_STRONG: rg is in a strong dedicated pset
 *	SCSLM_IS_NOT_DEDICATED: rg is not in a dedicated pset
 */
static int
SCSLM_in_dedicated_pset(t_SCSLM_rglist *rg)
{
	t_SCSLM_rglist		*tmp;

	if (strcmp(rg->rg_zone, GLOBAL_ZONENAME) == 0) {
		return (SCSLM_IS_NOT_DEDICATED);
	}
	tmp = fedRGHead;
	while (tmp != NULL) {
		if ((tmp != rg) &&
		    (strcmp(tmp->rg_zone, rg->rg_zone) == 0) &&
		    (strcmp(tmp->rg_slm_type, RG_SLM_TYPE_AUTO) == 0) &&
		    (SCSLM_R_running(tmp) != 0)) {
			switch (tmp->pset) {
				case SCSLM_PSET_STRONG:
					(void) SCSLM_set_pset_in_zone(tmp,
					    tmp->pset_cpu_min, tmp->pset_cpu,
					    tmp->pset);
					return (SCSLM_IS_DEDICATED_STRONG);
				case SCSLM_PSET_WEAK:
				case SCSLM_PSET_WEAK_DELETED:
					(void) SCSLM_set_pset_in_zone(tmp,
					    tmp->pset_cpu_min, tmp->pset_cpu,
					    tmp->pset);
					return (SCSLM_IS_DEDICATED_WEAK);
				default:
					break;
			}
		}
		tmp = tmp->next;
	}
	if (SCSLM_R_running(rg) != 0) {
		if (strcmp(rg->rg_slm_pset_type,
		    RG_SLM_PSET_TYPE_DEDICATED_STRONG) == 0) {
			(void) SCSLM_set_pset_in_zone(rg,
			    rg->pset_cpu_min, rg->pset_cpu,
			    rg->pset);
			return (SCSLM_IS_DEDICATED_STRONG);
		}
		if (strcmp(rg->rg_slm_pset_type,
		    RG_SLM_PSET_TYPE_DEDICATED_WEAK) == 0) {
			(void) SCSLM_set_pset_in_zone(rg,
			    rg->pset_cpu_min, rg->pset_cpu,
			    rg->pset);
			return (SCSLM_IS_DEDICATED_WEAK);
		}

	}
	(void) SCSLM_set_pset_in_zone(rg,
	    rg->pset_cpu_min, rg->pset_cpu,
	    rg->pset);
	return (SCSLM_IS_NOT_DEDICATED);
}


/*
 * Function SCSLM_set_pset_in_zone
 * For all resource group in a zone, set pset data
 * Args In:
 * 	a rg name, a pset number
 * Args Out:
 *	none
 * Return value:
 *	SCSLM_SUCCESS: pset set in rg
 *	SCSLM_ERROR_UNKNOWN_RG: rg name not found
 *	SCSLM_ERROR: error
 */
static int
SCSLM_set_pset_in_zone(t_SCSLM_rglist *rg, unsigned int min_cpu,
	unsigned int num_cpu, unsigned int status)
{
	t_SCSLM_rglist	*tmp;

	if (strcmp(rg->rg_zone, GLOBAL_ZONENAME) == 0) {
		return (SCSLM_ERROR);
	}
	tmp = fedRGHead;
	while (tmp != NULL) {
		if (strcmp(tmp->rg_zone, rg->rg_zone) == 0) {
			if (status != SCSLM_PSET_UNMODIFIED) {
				tmp->pset = status;
			}
			tmp->pset_cpu = num_cpu;
			tmp->pset_cpu_min = min_cpu;
		}
		tmp = tmp->next;
	}
	return (SCSLM_SUCCESS);
}


/*
 * Function SCSLM_del_pset_in_zone
 * For all resource group in a zone, clear pset data
 * Args In:
 * 	a rg name
 * Args Out:
 *	None
 * Return value:
 *	None
 */
static void
SCSLM_del_pset_in_zone(t_SCSLM_rglist *rg)
{
	t_SCSLM_rglist		*tmp;
	t_SCSLM_rglist		*strong = NULL;
	t_SCSLM_rglist		*weak = NULL;
	t_SCSLM_rglist		*dft = NULL;

	if (strcmp(rg->rg_zone, GLOBAL_ZONENAME) == 0) {
		return;
	}
	tmp = fedRGHead;
	while (tmp != NULL) {
		if ((strcmp(tmp->rg_zone, rg->rg_zone) == 0) &&
		    (strcmp(tmp->rg_slm_type, RG_SLM_TYPE_AUTO) == 0) &&
		    (SCSLM_R_running(tmp) != 0)) {
			if (strcmp(tmp->rg_slm_pset_type,
			    RG_SLM_PSET_TYPE_DEDICATED_STRONG) == 0) {
				strong = tmp;
			}
			if (strcmp(tmp->rg_slm_pset_type,
			    RG_SLM_PSET_TYPE_DEDICATED_WEAK) == 0) {
				weak = tmp;
			}
			if (strcmp(tmp->rg_slm_pset_type,
			    RG_SLM_PSET_TYPE_DEFAULT) == 0) {
				dft = tmp;
			}
		}
		tmp = tmp->next;
	}
	if (strong != NULL) {
		/* Zone is still dedicated strong */
		return;
	}
	if (weak != NULL) {
		if ((weak->pset == SCSLM_PSET_STRONG) &&
		    (weak->pset_cpu != 0)) {
			/*
			 * Move zone from dedicated strong to dedicated weak.
			 * This never happens if dedicated weak & strong
			 * in same zone is refused in scrgadm.
			 */
			(void) SCSLM_set_pset_in_zone(rg,
			    rg->pset_cpu_min, rg->pset_cpu,
			    SCSLM_PSET_WEAK);
		}
		return;
	}
	if (dft != NULL) {
		/* Move to not dedicated */
		(void) SCSLM_set_pset_in_zone(rg,
		    rg->pset_cpu_min, rg->pset_cpu,
		    SCSLM_PSET_NONE);
	}
}


/*
 * Function SCSLM_zone_find_shares
 * Compute zone's zone.cpu-shares value
 * Args In:
 * 	a zone name
 * Args Out:
 *	total shares found in the zone
 *	sum of min cpu found in the zone
 * Return value:
 *	SCSLM_SUCCESS: zone was found, and shares computed
 *	SCSLM_ERROR_UNKNOWN_RG: zone not found
 */
static int
SCSLM_zone_find_shares(char *zone, unsigned int *shares, unsigned int *min_cpu)
{
	t_SCSLM_rglist	*rg;
	t_SCSLM_rlist	*r;
	int		ret = SCSLM_ERROR_UNKNOWN_RG;
	unsigned int	tmp;
	int		stop;

	*shares = 0;
	if (min_cpu != NULL) {
		*min_cpu = 0;
	}
	rg = fedRGHead;
	while (rg != NULL) {
		if ((strcmp(rg->rg_zone, zone) == 0) &&
		    (strcmp(rg->rg_slm_type, RG_SLM_TYPE_AUTO) == 0)) {
			ret = SCSLM_SUCCESS;
			for (r = rg->rlist; r != NULL; r = r->next) {
				stop = 0;
				if (r->running != 0) {
					(*shares) += rg->cpu;
					stop = 1;
				}
				if (min_cpu != NULL &&
				    r->running != 0) {
					tmp = (rg->cpu_min * 100);
					if (rg->cpu < tmp) {
						tmp = rg->cpu;
					}
					(*min_cpu) += tmp;
				}
				if (stop != 0) {
					break;
				}
			}
		}
		rg = rg->next;
	}
	if (min_cpu != NULL) {
		if (*min_cpu % 100 == 0) {
			*min_cpu = *min_cpu / 100;
		} else {
			*min_cpu = (*min_cpu / 100) + 1;
		}
		if (*min_cpu == 0) {
			*min_cpu = 1;
		}
	}
	return (ret);
}


/*
 * Function SCSLM_running_in_RG
 * Is a resource of a resource group named rgname in zone rgzone
 * have running flag set ?
 * Args In:
 * 	a zone name, a resource group name
 * Args Out:
 *	none
 * Return value:
 *	SCSLM_SUCCESS: a R of this RG has running flag set
 *	SCSLM_ERROR_UNKNOWN_RG: unknown RG name
 *	SCSLM_ERROR: no R of this RG has running flag set
 */
static int
SCSLM_running_in_RG(char *rgzone, char *rgname)
{
	t_SCSLM_rglist		*rg;
	t_SCSLM_rlist		*r;

	rg = SCSLM_src_RG_fed(rgzone, rgname);
	if (rg == NULL) {
		return (SCSLM_ERROR_UNKNOWN_RG);
	}
	for (r = rg->rlist; r != NULL; r = r->next) {
		if (r->running != 0) {
			return (SCSLM_SUCCESS);
		}
	}
	return (SCSLM_ERROR);
}


/*
 * Function SCSLM_hdl_pool
 * Create a pool for a non-global zone, bind the zone to the pool
 * Check zone pool configuration and binding
 * Args In:
 * Args Out:
 *	None
 * Return value:
 *	SCSLM_SUCCESS: pool created and bind
 *	SCSLM_ERROR: other errors
 */
static int
SCSLM_hdl_pool(t_SCSLM_rglist *p)
{
	sc_syslog_msg_handle_t	sys_handle;
	int			ret;
	int			err;
	int			has_pool = 0;
	zoneid_t		zoneid;
	int64_t			poolid;  /* poolid of SCSLM generated pool */
	int64_t			zpoolid; /* zone current pool binding */
	char			*pool;   /* zone zonecfg pool */
	char			poolname[MAX_COMMAND_SIZE];

	/*
	 * Verify zone zonecfg pool setup,
	 * either pool_default or empty string
	 */
	ret = SCSLM_zone_pool(p->rg_zone, &pool);
	if (ret != SCSLM_SUCCESS) {
		return (ret);
	}
	if (strcmp(pool, "") != 0 &&
	    strcmp(pool, "pool_default") != 0) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Should never occur.
		 * @user_action
		 * Contact your authorized Sun service provider to determine
		 * whether a workaround or patch is available.
		 */
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM zone <%s> pool configuration error,"
		    " pool <%s>", p->rg_zone, pool);
		sc_syslog_msg_done(&sys_handle);
		free(pool);
		return (SCSLM_ERROR);
	}
	free(pool);

	/*
	 * Create the pool if needed.
	 * Refuse to start the RG until the SCSLM pool can be
	 * created. Note that SCSLM created pools from a previous
	 * boot are deleted at init time before poold is started.
	 */
	ret = SCSLM_create_pool(p->rg_zone);
	if (ret != SCSLM_SUCCESS) {
		if (ret == SCSLM_ERROR_EXISTING_POOL) {
			has_pool = 1;
		} else {
			return (ret);
		}
	}

	/*
	 * Read the pool.sys_id of this SCSLM generated pool
	 */
	ret = SCSLM_get_poolid(p->rg_zone, &poolid);
	if (ret != SCSLM_SUCCESS) {
		/* Errors are logged from SCSLM_get_poolid itself */
		return (ret);
	}

	/*
	 * Get the zone current pool dynamic binding
	 */
	ret = SCSLM_get_zone_dynamic_binding(p->rg_zone, &zpoolid);
	if (ret != SCSLM_SUCCESS) {
		/*
		 * Errors are logged from
		 * SCSLM_get_zone_dynamic_binding() itself
		 */
		return (ret);
	}

	if (has_pool == 1) {
		/*
		 * The SCSLM generated pool already exists.
		 * Verify zone is still bind to this poolid
		 */
		if (zpoolid != poolid) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * A non-global zone used to run resource groups under
			 * SLM control is not dynamically bind to the expected
			 * SCSLM pool (poolbind).
			 * @user_action
			 * Set zone pool binding to the displayed SCSLM pool
			 * (with poolbind command) before launching a resource
			 * group under SLM control in this zone.
			 */
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM "
			    "zone <%s> bind to pool.sys_id %llu, "
			    "should be <%s%s> (pool.sys_id %llu)",
			    p->rg_zone, zpoolid, SCSLM_POOL,
			    p->rg_zone, poolid);
			sc_syslog_msg_done(&sys_handle);
			return (SCSLM_ERROR);
		} else {
			/* pool exists, zone is bind to pool */
			return (SCSLM_SUCCESS);
		}
	} else {
		/*
		 * The SCSLM generated pool has just been created.
		 */
		LOG_FED_SLM_INFO4(sys_handle,
		    "SCSLM create pool <%s%s> (pool.sys_id %llu)",
		    SCSLM_POOL, p->rg_zone, poolid);
		/*
		 * Verify that zone current pool binding is the default
		 * pool before bind the zone to the SCSLM generated pool.
		 * Another pool than the pool_default is not accepted as part
		 * of the spec. POOL_DEFAULT is defined in Solaris pool.h.
		 */
		if (zpoolid != POOL_DEFAULT) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * Non-global Zone configuration error. A non-global
			 * zone used to run resource groups under SLM control
			 * must use pool pool_default, (zonecfg) and
			 * dynamically (poolbind).
			 * @user_action
			 * Set zone pool to pool_default in zonecfg and set
			 * zone binding to pool_default (poolbind) before
			 * launching a resource group under SLM control in
			 * this zone.
			 */
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM "
			    "zone <%s> bind to pool.sys_id %llu,"
			    " which is not pool_default (pool.sys_id %llu)",
			    p->rg_zone, zpoolid, POOL_DEFAULT);
			sc_syslog_msg_done(&sys_handle);
			return (SCSLM_ERROR);
		}
	}

	/*
	 * Bind the zone to the SCSLM generated pool.
	 */
	zoneid = getzoneidbyname(p->rg_zone);
	if (zoneid == -1) {
		err = errno; /*lint !e746 */
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Should never occur.
		 * @user_action
		 * Contact your authorized Sun service provider to determine
		 * whether a workaround or patch is available.
		 */
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM "
		    "getzoneidbyname error <%s>", strerror(err));
		sc_syslog_msg_done(&sys_handle);
		return (ret);
	}
	ret = snprintf(poolname, MAX_COMMAND_SIZE,
	    "%s%s", SCSLM_POOL, p->rg_zone);
	err = errno; /*lint !e746 */
	if (ret >= MAX_COMMAND_SIZE || ret < 0) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM snprintf error <%s>", strerror(err));
		sc_syslog_msg_done(&sys_handle);
		return (SCSLM_ERROR);
	}
	if (pool_set_binding(poolname, P_ZONEID, zoneid) == PO_FAIL) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Should never occur.
		 * @user_action
		 * Contact your authorized Sun service provider to determine
		 * whether a workaround or patch is available.
		 */
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM "
		    "pool_set_binding <%s> zone <%d> error PO_FAIL",
		    poolname, zoneid);
		sc_syslog_msg_done(&sys_handle);
		return (SCSLM_ERROR);
	}
	LOG_FED_SLM_INFO5(sys_handle,
	    "SCSLM bind zone <%s> to pool <%s%s> (pool.sys_id %llu)",
	    p->rg_zone, SCSLM_POOL, p->rg_zone, poolid);
	return (SCSLM_SUCCESS);
}


/*
 * Function SCSLM_thread
 * Periodically try
 * - to apply default pset minimal size
 * - to create dedicated psets
 * Args In:
 *	None
 * Args Out:
 *	None
 * Return value:
 *	loops forever
 */
static void
SCSLM_thread(void)
{
	sc_syslog_msg_handle_t	sys_handle;
	int			period;
	int			ret;

	/*
	 * loop forever
	 */
	for (;;) {

		LOCK_QUEUE();

		/*
		 * thread is disabled (nothing is done)
		 * when period is -1 (4294967295)
		 */
		if (scslm_thread_period == (unsigned int)-1) {
			period = SCSLM_THREAD_PERIOD * 1000;
			UNLOCK_QUEUE();
			(void) poll(0, 0, period);
			continue;
		}

		period = ((int)scslm_thread_period) * 1000;

		ret = SCSLM_pools_ok();
		if (ret == SCSLM_POOLS_DISABLED) {
			UNLOCK_QUEUE();
			LOG_FED_SLM_INFO1(sys_handle,
			    "SCSLM thread "
			    "WARNING pools facility is disabled");
			(void) poll(0, 0, period);
			continue;
		}
		if (ret == SCSLM_POOLS_NO_CONF) {
			UNLOCK_QUEUE();
			LOG_FED_SLM_INFO1(sys_handle,
			    "SCSLM thread "
			    "WARNING no pools facility static config file");
			(void) poll(0, 0, period);
			continue;
		}
		if (ret != SCSLM_SUCCESS) {
			UNLOCK_QUEUE();
			(void) sc_syslog_msg_initialize(
			    &sys_handle, SC_SYSLOG_RGM_FED_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * Should never occur.
			 * @user_action
			 * Contact your authorized Sun service provider to
			 * determine whether a workaround or patch is
			 * available.
			 */
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM thread "
			    "pools facility error %d", ret);
			sc_syslog_msg_done(&sys_handle);
			(void) poll(0, 0, period);
			continue;
		}

		SCSLM_hdl_default_pset_min();

		UNLOCK_QUEUE();
		/*
		 * sleep interval
		 */
		(void) poll(0, 0, period);

	} /* end loop forever */
}
#endif /* SOL_VERSION >= __s10 */



/*
 * -----------------------------------------------------------------------------
 *
 * fed routines for Service Level Management
 *
 * -----------------------------------------------------------------------------
 */


/*
 * Function SCSLM_lock_file
 * To have mutual exclusion with scslm_cleanup tool
 * Args In:
 *	None
 * Args Out:
 *	None
 * Return value:
 *	SCSLM_SUCCESS or errors
 */
static int
SCSLM_lock_file(void)
{
	sc_syslog_msg_handle_t	sys_handle;
	int	err;

	assert(slm_lock_file < 0);
	slm_lock_file = open(SCSLM_PROJ_FILE_LOCK,
	    O_WRONLY | O_CREAT, SCSLM_FILE_LOCK_MODE);
	err = errno; /*lint !e746 */
	if (slm_lock_file < 0) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Should never occur.
		 * @user_action
		 * Contact your authorized Sun service provider to determine
		 * whether a workaround or patch is available.
		 */
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM "
		    "open <%s> error <%s>",
		    SCSLM_PROJ_FILE_LOCK, strerror(err));
		sc_syslog_msg_done(&sys_handle);
		slm_lock_file = -1;
		return (SCSLM_ERROR);
	}
	if (lockf(slm_lock_file, F_LOCK, 0) < 0) {
		err = errno; /*lint !e746 */
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Should never occur.
		 * @user_action
		 * Contact your authorized Sun service provider to determine
		 * whether a workaround or patch is available.
		 */
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM "
		    "lockf <%s> error <%s>",
		    SCSLM_PROJ_FILE_LOCK, strerror(err));
		sc_syslog_msg_done(&sys_handle);
		(void) close(slm_lock_file);
		slm_lock_file = -1;
		return (SCSLM_ERROR);
	}
	return (SCSLM_SUCCESS);
}


/*
 * Function SCSLM_unlock_file
 * To have mutual exclusion with scslm_cleanup tool
 * Args In:
 *	None
 * Args Out:
 *	None
 * Return value:
 *	SCSLM_SUCCESS or errors
 */
static int
SCSLM_unlock_file(void)
{
	assert(slm_lock_file > 0);
	/* close the file removes the lock */
	(void) close(slm_lock_file);
	slm_lock_file = -1;
	return (SCSLM_SUCCESS);
}


/*
 * Function SCSLM_R_running
 *
 * Is a resource of a resource group in the list have running flag set ?
 *
 * Args In:
 *	Resource group list pointer
 * Args Out:
 *	None
 * Return value:
 *	1 a resource has running flag set in this RG
 *	0
 */
static int
SCSLM_R_running(t_SCSLM_rglist *rg)
{
	t_SCSLM_rlist		*r = NULL;

	if (rg != NULL) {
		r = rg->rlist;
		while (r != NULL) {
			if (r->running != 0) {
				return (1);
			}
			r = r->next;
		}
	}
	return (0);
}


/*
 * Function SCSLM_update_per_zone
 * Handle SLM RG properties updates in a zone
 * (for example, scrgadm -c -g RG -y RG_SLM_CPU_SHARES=3)
 * or
 * Remove a resource group from the fed SLM resource group list
 * or
 * Remove a resource from the fed SLM resource list of a fed SLM
 * resource group.
 * A resource group can have same name in different zones.
 * Args In:
 *	None.
 * Args Out:
 *	None.
 * Return value:
 *	SCSLM_SUCCESS
 *	SCSLM_ERROR
 * Called only by function SCSLM_update()
 */
static int
SCSLM_update_per_zone(fe_slm_update_args *args, t_SCSLM_rglist *rg)
{
	sc_syslog_msg_handle_t	sys_handle;
	int			ret;
	unsigned int		shares = 0;
#if SOL_VERSION >= __s10
	unsigned int		saved;
	unsigned int		saved_min;
	unsigned int		zshares = 0;
	unsigned int		cpu_min;
#endif

	/*
	 * FE_SLM_UPDATE_RG_PROP handling
	 * Handle SLM RG properties changes
	 */
	if (strcmp(args->rg_slm_pset_type,
	    RG_SLM_PSET_TYPE_DEDICATED_WEAK) != 0 &&
	    strcmp(args->rg_slm_pset_type,
	    RG_SLM_PSET_TYPE_DEDICATED_STRONG) != 0 &&
	    strcmp(args->rg_slm_pset_type,
	    RG_SLM_PSET_TYPE_DEFAULT) != 0) {
		return (SCSLM_ERROR);
	}
	if (strcmp(args->rg_slm_type, RG_SLM_TYPE_MANUAL) != 0 &&
	    strcmp(args->rg_slm_type, RG_SLM_TYPE_AUTO) != 0) {
		return (SCSLM_ERROR);
	}
#if SOL_VERSION < __s10
	/* There is no dedicated support on S9 (currently)  */
	if (strcmp(args->rg_slm_pset_type,
	    RG_SLM_PSET_TYPE_DEDICATED_WEAK) == 0 ||
	    strcmp(args->rg_slm_pset_type,
	    RG_SLM_PSET_TYPE_DEDICATED_STRONG) == 0) {
		return (SCSLM_ERROR);
	}
#endif
	if (strcmp(rg->rg_slm_type, RG_SLM_TYPE_MANUAL) == 0) {
		return (SCSLM_SUCCESS);
	}
	if (SCSLM_R_running(rg) == 0) {
		return (SCSLM_SUCCESS);
	}
#if SOL_VERSION >= __s10
	saved = rg->cpu;
	saved_min = rg->cpu_min;
#endif
	SCSLM_get_cpu_shares(args->rg_slm_cpu, &rg->cpu);
	SCSLM_get_pset_min(args->rg_slm_cpu_min, &rg->cpu_min);
	shares = rg->cpu;
#if SOL_VERSION >= __s10
	ret = SCSLM_zone_find_shares(rg->rg_zone, &zshares, &cpu_min);
	if (ret != SCSLM_SUCCESS) {
		return (SCSLM_ERROR);
	}
	ret = SCSLM_update_pset(rg, zshares, cpu_min);
	if (ret == SCSLM_ERROR_UPDATE) {
		/*
		 * Aply saved values
		 */
		rg->cpu = saved;
		rg->cpu_min = saved_min;
		return (ret);
	}
	if (ret != SCSLM_SUCCESS) {
		return (SCSLM_ERROR);
	}
#endif
	ret = SCSLM_modify_proj_shares_file(rg->rg_zone, rg->proj_name, shares);
	if (ret != SCSLM_SUCCESS) {
		return (SCSLM_ERROR);
	}
#if SOL_VERSION >= __s10
	ret = SCSLM_modify_proj_shares_mem(
	    rg->rg_zone, rg->proj_name, shares);
#else
	ret = SCSLM_modify_proj_shares_mem(
	    rg->rg_name, rg->proj_name, shares);
#endif
	if (ret != SCSLM_SUCCESS) {
		/*
		 * TODO:
		 * 6364293 remove Solaris bug 6201729 workarroud in fed_slm.c
		 *
		 * SunCluster uses currently S10 build74L2a, in which
		 * there is the following Solaris bug:
		 * "6201729 prctl fails to set resource controls on
		 * some processes: Not owner"
		 * This bug is targeted for solaris_10u1, when SunCluster
		 * moves to solaris_10u1 or above, and 6201729 has a fix,
		 * ifdef SOLARIS_BUG_6201729_FIXED can be removed.
		 * We curently simply log the message "prctl... Not owner"
		 * returned by the prctl command and ignore it. This
		 * concerns only dynamic project shares or zone shares
		 * changes, that may not be as appropriate due to this bug.
		 */
#ifdef SOLARIS_BUG_6201729_FIXED
		return (SCSLM_ERROR);
#endif
	} else {
#if SOL_VERSION >= __s10
		LOG_FED_SLM_INFO4(sys_handle,
		    "SCSLM set project <%s> "
		    "shares in zone <%s> to %u",
		    rg->proj_name, rg->rg_zone, shares);
#else
		LOG_FED_SLM_INFO3(sys_handle,
		    "SCSLM set project <%s> "
		    "shares to %u",
		    rg->proj_name, shares);
#endif
	}
#if SOL_VERSION >= __s10
	ret = SCSLM_modify_zone_shares(rg->rg_zone, &zshares);
	if (ret == SCSLM_SUCCESS) {
		LOG_FED_SLM_INFO3(sys_handle,
		    "SCSLM set zone <%s> shares to %u",
		    rg->rg_zone, zshares);
	}
#endif
	return (SCSLM_SUCCESS);
}


/*
 * Function SCSLM_update_delete_rg
 * Delete a resource group in the resource group list
 * Args In:
 *	a pointer to fe_slm_update_args
 *	a pointer to t_SCSLM_rglist
 * Args Out:
 * 	None
 * Return value:
 *	SCSLM_SUCCESS
 *	SCSLM_ERROR
 * Called only by function SCSLM_update_per_zone()
 */
static int
SCSLM_update_delete_rg(char *rg_name)
{
	t_SCSLM_rglist		*rg = fedRGHead;
	t_SCSLM_rglist		*prev = NULL;
	t_SCSLM_rglist		*tofree;

	/* a resource group name can be in several zones */
	while (rg != NULL) {
		assert(rg->myaddr == rg);
		tofree = NULL;
		if (strcmp(rg->rg_name, rg_name) == 0) {
			tofree = rg;
		}
		if (tofree != NULL) {
			if (prev != NULL) {
				prev->next = rg->next;
			} else {
				fedRGHead = rg->next;
				prev = NULL;
			}
			rg = rg->next;
			if (tofree->rg_name != NULL) {
				free(tofree->rg_name);
			}
			if (tofree->proj_name != NULL) {
				free(tofree->proj_name);
			}
			free(tofree);
		} else {
			prev = rg;
			rg = rg->next;
		}
	}
	return (SCSLM_SUCCESS);
}


/*
 * Function SCSLM_update_delete_r
 * Delete a resource in a resource group
 * Args In:
 *	a pointer to fe_slm_update_args
 *	a pointer to t_SCSLM_rglist
 * Args Out:
 * 	None
 * Return value:
 *	SCSLM_SUCCESS
 *	SCSLM_ERROR
 * Called only by function SCSLM_update_per_zone()
 */
static int
SCSLM_update_delete_r(char *rg_name, char *r_name)
{
	t_SCSLM_rglist		*rg = fedRGHead;
	t_SCSLM_rlist		*r;
	t_SCSLM_rlist		*prev;
	t_SCSLM_rlist		*tofree;

	/* a resource group name can be in several zones */
	while (rg != NULL) {
		assert(rg->myaddr == rg);
		if (strcmp(rg->rg_name, rg_name) == 0) {
			prev = NULL;
			r = rg->rlist;
			while (r != NULL) {
				assert(r->myaddr == r);
				tofree = NULL;
				if (strcmp(r->r_name, r_name) == 0) {
					tofree = r;
				}
				if (tofree != NULL) {
					if (prev == NULL) {
						rg->rlist = tofree->next;
					} else {
						prev->next = tofree->next;
					}
					if (tofree->r_name != NULL) {
						free(tofree->r_name);
					}
					free(tofree);
					break;
				} else {
					prev = r;
					r = r->next;
				}
			}
		}
		rg = rg->next;
	}
	return (SCSLM_SUCCESS);
}


#if SOL_VERSION >= __s10
/*
 * Function SCSLM_zone_down
 * A non global zone is dead. Delete SCSLM generated pool and SCSLM
 * generated pset (if any). Also, set any running flag to zero of any
 * resource in this non-global zone.
 * Args In:
 *	A zone name
 * Args Out:
 * 	None
 * Return value:
 *	SCSLM_SUCCESS
 *	SCSLM errors
 * Called only by function scslm_zone_down()
 */
static int
SCSLM_zone_down(const char *zone)
{
	sc_syslog_msg_handle_t	sys_handle;
	int			ret;
	t_SCSLM_rglist		*rg = fedRGHead;
	t_SCSLM_rlist		*r;
	t_SCSLM_rglist		*rg_cleanup = NULL;

	LOG_FED_SLM_INFO2(sys_handle, "SCSLM zone <%s> down", zone);

	/* loop the list of resource group */
	while (rg != NULL) {
		if ((strcmp(rg->rg_zone, GLOBAL_ZONENAME) != 0) &&
		    (strcmp(rg->rg_zone, zone) == 0) &&
		    (SCSLM_running_in_RG(rg->rg_zone, rg->rg_name) ==
		    SCSLM_SUCCESS)) {
			/*
			 * A Resource Group was running in non global zone
			 * that is now dead
			 */
			if (rg_cleanup == NULL) {
				rg_cleanup = rg;
			}
			/*
			 * set resources running flag to zero
			 */
			r = rg->rlist;
			while (r != NULL) {
				r->running = 0;
				r = r->next;
			}
		}
		rg = rg->next;
	}
	if (rg_cleanup != NULL) {
		/* Delete the pool */
		ret = SCSLM_delete_pool(rg_cleanup->rg_zone);
		/* Error is not fatal */
		if (ret == SCSLM_SUCCESS) {
			LOG_FED_SLM_INFO3(sys_handle,
			    "SCSLM delete pool <%s%s>", SCSLM_POOL,
			    rg_cleanup->rg_zone);
		}
		if (rg_cleanup->pset_cpu != 0) {
			/* Remove the pset */
			ret = SCSLM_delete_pset(rg_cleanup->rg_zone);
			if (ret != SCSLM_SUCCESS) {
				return (ret);
			}
			LOG_FED_SLM_INFO3(sys_handle,
			    "SCSLM delete pset <%s%s>",
			    SCSLM_PSET, rg_cleanup->rg_zone);
			/* There is no more pset dedicated */
			ret = SCSLM_set_pset_in_zone(rg_cleanup,
			    0, 0, SCSLM_PSET_NONE);
			if (ret != SCSLM_SUCCESS) {
				return (ret);
			}
		}
	}
	return (SCSLM_SUCCESS);
}

/*
 * Function SCSLM_update_pset
 * Update a pset size (min,max cpus)
 * Args In:
 *	a resource group pointer, cpu shares, minimun cpu
 * Args Out:
 *	None
 * Return value:
 *	SCSLM_SUCCESS
 *	SCSLM_ERROR
 */
static int
/* ARGSUSED */
SCSLM_update_pset(t_SCSLM_rglist *rg,
    unsigned int shares, unsigned int min_cpu)
{
	sc_syslog_msg_handle_t	sys_handle;
	int			dedicated;
	unsigned int		count = 0;
	unsigned int		num_cpu;
	int			ret;

	/* Compute number of cpus */
	if (shares % 100 == 0) {
		num_cpu = (shares / 100);
	} else {
		num_cpu = (shares / 100) + 1;
	}
	if (num_cpu == 0) {
		num_cpu = 1;
	}

	dedicated = SCSLM_in_dedicated_pset(rg);
	if (dedicated == SCSLM_IS_NOT_DEDICATED) {
		return (SCSLM_SUCCESS);
	}
	if (dedicated == SCSLM_IS_DEDICATED_STRONG && rg->pset_cpu != 0 &&
	    (num_cpu != rg->pset_cpu || min_cpu != rg->pset_cpu_min)) {
		/* pset exists, adjust the pset size */
SCSLM_update_pset_again:
		ret = SCSLM_modify_pset(rg->rg_zone,
		    (uint64_t)min_cpu, (uint64_t)num_cpu, 1);
		if (ret != SCSLM_SUCCESS) {
			if (count == 0) {
				count = 1;
				(void) SCSLM_destroy_weak_psets();
				SCSLM_hdl_default_pset_min();
				goto SCSLM_update_pset_again;
			}
			LOG_FED_SLM_INFO5(sys_handle,
			    "SCSLM WARNING, cannot "
			    "modify strong pset <%s%s> to min=%u,max=%u",
			    SCSLM_PSET, rg->rg_zone, min_cpu, num_cpu);
			return (SCSLM_ERROR_UPDATE);
		} else {
			ret = SCSLM_set_pset_in_zone(rg,
			    min_cpu, num_cpu, SCSLM_PSET_UNMODIFIED);
			if (ret != SCSLM_SUCCESS) {
				return (ret);
			}
			LOG_FED_SLM_INFO5(sys_handle,
			    "SCSLM modify strong pset <%s%s> min=%u,max=%u",
			    SCSLM_PSET, rg->rg_zone, min_cpu, num_cpu);
		}
		return (SCSLM_SUCCESS);
	}
	if (dedicated == SCSLM_IS_DEDICATED_WEAK && rg->pset_cpu != 0 &&
	    (num_cpu != rg->pset_cpu || min_cpu != rg->pset_cpu_min)) {
		/* pset exists, adjust the pset size */
		ret = SCSLM_modify_pset(rg->rg_zone,
		    (uint64_t)min_cpu, (uint64_t)num_cpu, 1);
		if (ret != SCSLM_SUCCESS) {
			ret = SCSLM_delete_pset(rg->rg_zone);
			if (ret != SCSLM_SUCCESS) {
				return (SCSLM_ERROR);
			}
			LOG_FED_SLM_INFO3(sys_handle,
			    "SCSLM delete weak pset <%s%s>",
			    SCSLM_PSET, rg->rg_zone);
			/* There is no more pset dedicated */
			ret = SCSLM_set_pset_in_zone(rg,
			    0, 0, SCSLM_PSET_WEAK_DELETED);
			if (ret != SCSLM_SUCCESS) {
				return (ret);
			}
		} else {
			LOG_FED_SLM_INFO5(sys_handle,
			    "SCSLM modify weak pset <%s%s> min=%u,max=%u",
			    SCSLM_PSET, rg->rg_zone, min_cpu, num_cpu);
			ret = SCSLM_set_pset_in_zone(rg,
			    min_cpu, num_cpu, SCSLM_PSET_UNMODIFIED);
			if (ret != SCSLM_SUCCESS) {
				return (ret);
			}
		}
	}
	return (SCSLM_SUCCESS);
}
#endif /* SOL_VERSION >= __s10 */


/*
 * Function SCSLM_exec
 * a command to execute through popen(),it should include 2>&1 to
 * have also stderr output of command.
 * Args In:
 *	the command
 * Args Out:
 * 	the command return value
 *	the command outputs
 * Return value:
 *	SCSLM_SUCCESS: command launched, return_code set
 *	SCSLM_ERROR: failure in launching the command
 */
static int
SCSLM_exec(char *cmd, int *return_code, char **cmd_output)
{
	sc_syslog_msg_handle_t	sys_handle;
	FILE	*ptr;
	size_t	cnt = 0;
	size_t	ssize;
	int	err;
	char	*tmp;
	char	buf[SCSLM_FGETS_SIZE];

	*cmd_output = NULL;
	*return_code = 1;

	if ((ptr = popen(cmd, "r")) != NULL) {
		while (fgets(buf, SCSLM_FGETS_SIZE, ptr) == buf) {
			ssize = strlen(buf);
			if (ssize == 0) {
				(void) sc_syslog_msg_initialize(&sys_handle,
				    SC_SYSLOG_RGM_FED_TAG, "");
				/*
				 * SCMSGS
				 * @explanation
				 * Should never occur.
				 * @user_action
				 * Contact your authorized Sun service
				 * provider to determine whether a workaround
				 * or patch is available.
				 */
				(void) sc_syslog_msg_log(sys_handle,
				    LOG_ERR, MESSAGE,
				    "SCSLM exec "
				    "<%s> strlen is zero", cmd);
				sc_syslog_msg_done(&sys_handle);
				if (*cmd_output != NULL) {
					free(*cmd_output);
					*cmd_output = NULL;
				}
				return (SCSLM_ERROR);
			}
			/* realloc with NULL is like malloc */
			tmp = (char *)realloc(*cmd_output, cnt + ssize + 1);
			err = errno; /*lint !e746 */
			if (tmp == NULL) {
				(void) sc_syslog_msg_initialize(&sys_handle,
				    SC_SYSLOG_RGM_FED_TAG, "");
				/*
				 * SCMSGS
				 * @explanation
				 * Should never occur.
				 * @user_action
				 * Contact your authorized Sun service
				 * provider to determine whether a workaround
				 * or patch is available.
				 */
				(void) sc_syslog_msg_log(sys_handle,
				    LOG_ERR, MESSAGE,
				    "SCSLM realloc error <%s>", strerror(err));
				sc_syslog_msg_done(&sys_handle);
				return (SCSLM_ERROR);
			}
			(void) memcpy(tmp + cnt, buf, ssize);
			tmp[cnt + ssize] = '\0';
			*cmd_output = tmp;
			cnt += ssize;
		}
		*return_code = (int)((unsigned int)(pclose(ptr)) >> 8);
		return (SCSLM_SUCCESS);
	}
	err = errno; /*lint !e746 */
	(void) sc_syslog_msg_initialize(&sys_handle,
	    SC_SYSLOG_RGM_FED_TAG, "");
	/*
	 * SCMSGS
	 * @explanation
	 * Should never occur.
	 * @user_action
	 * Contact your authorized Sun service provider to determine whether a
	 * workaround or patch is available.
	 */
	(void) sc_syslog_msg_log(sys_handle,
	    LOG_ERR, MESSAGE,
	    "SCSLM <%s> popen error <%s>", cmd, strerror(err));
	sc_syslog_msg_done(&sys_handle);
	return (SCSLM_ERROR);
}


/*
 * Function SCSLM_replace_dash_in_str
 * Replace '-' by '_', if any, in a string
 * Args In:
 *	a string
 * Args Out:
 *     	the string
 * Return value:
 *	None
 */
static void
SCSLM_replace_dash_in_str(char *s)
{
	if (s == NULL) {
		return;
	}
	while (*s != '\0') {
		if (*s == '-') {
			*s = '_';
		}
		s++;
	}
}


/*
 * Function SCSLM_get_pset_min
 */
static void
SCSLM_get_pset_min(int s, unsigned int *x)
{
	*x = 0;
	if (s > SCSLM_MAX_CPUS) {
		return;
	}
	if (s < 0) {
		return;
	}
	*x = (unsigned int)s;
}


/*
 * Function SCSLM_get_cpu_shares
 */
static void
SCSLM_get_cpu_shares(int s, unsigned int *x)
{
	*x = 1;
	if (s > SCSLM_MAX_SHARES) {
		return;
	}
	if (s < 0) {
		return;
	}
	*x = (unsigned int)s;
}


/*
 * Function SCSLM_cpu_values
 * Extract CPU values from a fe_handle and assign
 * the CPU values to resource group list data
 * Args In:
 *	a fe_handle, a resource group list pointer
 * Args Out:
 *	None.
 * Return value:
 *	None.
 */
static void
SCSLM_cpu_values(fe_handle *hdl, t_SCSLM_rglist *p)
{
	SCSLM_get_cpu_shares(hdl->rg_slm_cpu, &p->cpu);
	SCSLM_get_pset_min(hdl->rg_slm_cpu_min, &p->cpu_min);
}



/*
 * -----------------------------------------------------------------------------
 *
 * fed SLM data list handling
 *
 * -----------------------------------------------------------------------------
 */

/*
 * Function SCSLM_src_RG_fed
 * Args In:
 *	a zone name, a resource group name
 * Args Out:
 *	a pointer to internal resource group list
 * Return value:
 *	None
 */
static t_SCSLM_rglist
*SCSLM_src_RG_fed(char *zone, char *name)
{
	t_SCSLM_rglist *p = fedRGHead;
	while (p != NULL) {
		if ((strcmp(p->rg_zone, zone) == 0) &&
		    (strcmp(p->rg_name, name) == 0)) {
			return (p);
		}
		p = p->next;
	}
	return (NULL);
}


/*
 * Function SCSLM_src_R_fed
 * Search a resource in the list of resource of a resource group
 * using the resource name
 * Args In:
 *	a resource name, a resource group list pointer
 * Args Out:
 *	a pointer to internal resource group list
 * Return value:
 *	a pointer to internal resource list
 */
static t_SCSLM_rlist *
SCSLM_src_R_fed(char *r_name, t_SCSLM_rglist *p)
{
	t_SCSLM_rlist *r;
	if (p != NULL) {
		for (r = p->rlist; r != NULL; r = r->next) {
			if (strcmp(r->r_name, r_name) == 0) {
				/* r_name found */
				return (r);
			}
		}
		p = p->next;
	}
	return (NULL);
}


/*
 * Function SCSLM_add_RG_fed
 * Add a resource group to the list of resource group.
 * Args In:
 *	a fe_handle
 * Args Out:
 *	None
 * Return value:
 *	SCSLM_SUCCESS
 *	SCSLM_ERROR
 */
static int
SCSLM_add_RG_fed(fe_handle *hdl)
{
	t_SCSLM_rglist		*p;
	t_SCSLM_rglist		*last;
	int			err;
	sc_syslog_msg_handle_t	sys_handle;

	p = (t_SCSLM_rglist *)malloc(sizeof (t_SCSLM_rglist));
	err = errno; /*lint !e746 */
	if (p == NULL) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Should never occur.
		 * @user_action
		 * Contact your authorized Sun service provider to determine
		 * whether a workaround or patch is available.
		 */
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM malloc error <%s>", strerror(err));
		sc_syslog_msg_done(&sys_handle);
		return (SCSLM_ERROR);
	}
	(void) memset((void *)p, 0, sizeof (t_SCSLM_rglist));
	p->myaddr = p;
	p->rg_name = strdup(hdl->rg_name);
	err = errno; /*lint !e746 */
	if (p->rg_name == NULL) {
		free(p);
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM strdup error <%s>", strerror(err));
		sc_syslog_msg_done(&sys_handle);
		return (SCSLM_ERROR);
	}
	(void) strcpy(p->rg_slm_type, RG_SLM_TYPE_UNKNOWN);
	(void) strcpy(p->rg_slm_pset_type, hdl->rg_slm_pset_type);
	(void) strcpy(p->rg_zone, hdl->zonename);

	p->next = NULL;
	if (fedRGHead == NULL) {
		fedRGHead = p;
	} else {
		last = fedRGHead;
		while (last->next != NULL) {
			last = last->next;
		}
		last->next = p;
	}
	return (SCSLM_SUCCESS);
}


/*
 * Function SCSLM_append_R_fed
 * Add at tail of the list of resources
 * of a resource group an empty element.
 * Args In:
 *	a pointer on a resource group from resource group list
 * Args Out:
 *	None
 * Return value:
 *	a resource list pointer
 */
static t_SCSLM_rlist *
SCSLM_append_R_fed(t_SCSLM_rglist *p)
{
	sc_syslog_msg_handle_t	sys_handle;
	t_SCSLM_rlist	*newr;
	t_SCSLM_rlist	*r;
	int		err;

	newr = (t_SCSLM_rlist *)malloc(sizeof (t_SCSLM_rlist));
	err = errno; /*lint !e746 */
	if (newr == NULL) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM malloc error <%s>", strerror(err));
		sc_syslog_msg_done(&sys_handle);
		return (NULL);
	}
	bzero(newr, sizeof (t_SCSLM_rlist));
	newr->myaddr = newr;
	if (p->rlist == NULL) {
		/* First element of the list */
		p->rlist = newr;
		return (newr);
	}
	/* Go to the last element of the list */
	for (r = p->rlist; r->next != NULL; r = r->next) {
	}
	/* Append new element */
	r->next = newr;
	return (newr);
}


/*
 * Function SCSLM_add_R_fed
 * Add a resource to the list of resources of a resource group, if not
 * already exists. If it already exists, use it.
 * Args In:
 *	a pointer on a resource group from resource group list, a fe_handle,
 * Args Out:
 *	next resource in the list
 * Return value:
 *	SCSLM_SUCCESS
 *	SCSLM_ERROR
 */
static int
SCSLM_add_R_fed(t_SCSLM_rglist *p, fe_handle *hdl, t_SCSLM_rlist **r)
{
	t_SCSLM_rlist	*nr;
	sc_syslog_msg_handle_t	sys_handle;

	*r = NULL;
	nr = SCSLM_src_R_fed(hdl->r_name, p);
	if (nr != NULL) {
		/* r_name already exists */
		if (nr->running == 0) {
			/*
			 * When resource is running
			 * (METH_START | METH_PRENET_START)
			 * this update is handled by SCSLM_update()
			 */
			SCSLM_cpu_values(hdl, p);
		}
		*r = nr;
		return (SCSLM_SUCCESS);
	}
	/* r_name does not exists, create it */
	nr = SCSLM_append_R_fed(p);
	if (nr == NULL) {
		return (SCSLM_ERROR);
	}
	nr->r_name = strdup(hdl->r_name);
	SCSLM_cpu_values(hdl, p);
	nr->running = 0;
	if (scslm_do_log != 0) {
		LOG_FED_SLM_INFO3(sys_handle,
		    "SCSLM_add_R_fed <%s> running set to 0x%x",
		    nr->r_name, nr->running);
	}
	*r = nr;
	return (SCSLM_SUCCESS);
}


/*
 * Function SCSLM_prt_fed
 * Logs fed SLM internal data and lists
 * Args In:
 *	None
 * Args Out:
 *	None
 * Return value:
 *	None
 */
static void
SCSLM_prt_fed(void)
{
	sc_syslog_msg_handle_t	sys_handle;
	t_SCSLM_rglist *p = fedRGHead;
	t_SCSLM_rlist *r;

	LOG_FED_SLM_INFO2(sys_handle,
	    "scslm_thread_period %u", scslm_thread_period);
	LOG_FED_SLM_INFO2(sys_handle,
	    "scslm_global_zone_shares %u", scslm_global_zone_shares);
	LOG_FED_SLM_INFO2(sys_handle,
	    "scslm_default_pset_min %u", scslm_default_pset_min);

	while (p != NULL) {
		LOG_FED_SLM_INFO2(sys_handle,
		    "==== rg_name <%s> =====", p->rg_name);
		LOG_FED_SLM_INFO2(sys_handle,
		    "rg_zone <%s>", p->rg_zone);
		LOG_FED_SLM_INFO2(sys_handle,
		    "rg_slm_type <%s>", p->rg_slm_type);
		LOG_FED_SLM_INFO2(sys_handle, "rg_slm_pset_type <%s>",
		    p->rg_slm_pset_type);
		LOG_FED_SLM_INFO2(sys_handle, "pset %u", p->pset);
		LOG_FED_SLM_INFO2(sys_handle, "pset_cpu %u",
		    p->pset_cpu);
		LOG_FED_SLM_INFO2(sys_handle, "pset_cpu_min %u",
		    p->pset_cpu_min);
		LOG_FED_SLM_INFO2(sys_handle,
		    "	SHARES %u", p->cpu)
		LOG_FED_SLM_INFO2(sys_handle,
		    "	MIN PSET %u", p->cpu_min)
		if (p->proj_name != NULL) {
			LOG_FED_SLM_INFO2(sys_handle,
			    "proj_name <%s>", p->proj_name);
		}
		for (r = p->rlist; r != NULL; r = r->next) {
			LOG_FED_SLM_INFO2(sys_handle,
			    "--- <%s> ----", r->r_name);
			LOG_FED_SLM_INFO2(sys_handle,
			    "running 0x%x", r->running);
		}
		p = p->next;
	}
}



/*
 * -----------------------------------------------------------------------------
 *
 * Projects handling routines
 *
 * -----------------------------------------------------------------------------
 */

/*
 * Function SCSLM_create_proj
 * Create a SCSLM project
 * Args In:
 * 	a zone name
 *	the zone path
 *	a project name
 * Args Out:
 *	none
 * Return value:
 *	SCSLM_SUCCESS: project created
 *	SCSLM_ERROR_EXISTING_PROJECT: project already exist
 * 	SCSLM_ERROR_UNKNOWN_ZONE:  zone is unknown
 *	SCSLM_ERROR: other errors
 */
static int
/* ARGSUSED */
SCSLM_create_proj(char *zone, char *proj)
{
	sc_syslog_msg_handle_t	sys_handle;
	int	err;
	int	ret;
	int	cmd_ret;
	char	*cmd_output;
	char	cmd[MAX_COMMAND_SIZE];
#if SOL_VERSION >= __s10
	char	serr[MAX_COMMAND_SIZE];
#endif /* SOL_VERSION >= __s10 */

	if (strcmp(zone, GLOBAL_ZONENAME) == 0) {
		ret = snprintf(cmd, MAX_COMMAND_SIZE,
		    "%s -U'*' -G'*' -c'%s' %s 2>&1",
		    PROJ_ADD, SCSLM_GENERATED, proj);
		err = errno; /*lint !e746 */
		if (ret >= MAX_COMMAND_SIZE || ret < 0) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM snprintf error <%s>", strerror(err));
			sc_syslog_msg_done(&sys_handle);
			return (SCSLM_ERROR);
		}
#if SOL_VERSION >= __s10
	} else {
		/* Create in local zone */
		ret = snprintf(cmd, MAX_COMMAND_SIZE,
		    "%s -S %s \"%s -U'*' -G'*' -c'%s' %s\" 2>&1",
		    ZONE_CMD, zone, PROJ_ADD, SCSLM_GENERATED, proj);
		err = errno; /*lint !e746 */
		if (ret >= MAX_COMMAND_SIZE || ret < 0) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM snprintf error <%s>",
			    strerror(err));
			sc_syslog_msg_done(&sys_handle);
			return (SCSLM_ERROR);
		}
	}
#else
	}
#endif /* SOL_VERSION >= __s10 */
	ret = SCSLM_exec(cmd, &cmd_ret, &cmd_output);
	if (ret != SCSLM_SUCCESS) {
		return (ret);
	}
	if (cmd_ret != 0) {
		if (cmd_output != NULL) {
			if (strstr(cmd_output,
			    "Duplicate project name") != NULL) {
				free(cmd_output);
				return (SCSLM_ERROR_EXISTING_PROJECT);
			}
#if SOL_VERSION >= __s10
			ret = snprintf(serr, MAX_COMMAND_SIZE,
			    "zlogin: zone \'%s\' unknown", zone);
			err = errno; /*lint !e746 */
			if (ret >= MAX_COMMAND_SIZE || ret < 0) {
				(void) sc_syslog_msg_initialize(&sys_handle,
				    SC_SYSLOG_RGM_FED_TAG, "");
				(void) sc_syslog_msg_log(sys_handle,
				    LOG_ERR, MESSAGE,
				    "SCSLM snprintf error <%s>", strerror(err));
				sc_syslog_msg_done(&sys_handle);
				free(cmd_output);
				return (SCSLM_ERROR);
			}
			if (strstr(cmd_output, serr) != NULL) {
				free(cmd_output);
				return (SCSLM_ERROR_UNKNOWN_ZONE);
			}
#endif /* SOL_VERSION >= __s10 */
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * Should never occur.
			 * @user_action
			 * Contact your authorized Sun service provider to
			 * determine whether a workaround or patch is
			 * available.
			 */
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM <%s> ret %d, outputs <%s>",
			    cmd, cmd_ret, cmd_output);
			sc_syslog_msg_done(&sys_handle);
			free(cmd_output);
			return (SCSLM_ERROR);
		} else {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * Should never occur.
			 * @user_action
			 * Contact your authorized Sun service provider to
			 * determine whether a workaround or patch is
			 * available.
			 */
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM <%s> ret %d", cmd, cmd_ret);
			sc_syslog_msg_done(&sys_handle);
			return (SCSLM_ERROR);
		}
	} else {
		if (cmd_output != NULL) {
			free(cmd_output);
		}
	}
	return (SCSLM_SUCCESS);
}


/*
 * Function SCSLM_modify_proj_shares_file
 * Modify project.cpu-shares property of SCSLM project, in the
 * project file.
 * Args In:
 * 	a project name
 *	a zone name
 *	a share number
 * Args Out:
 *	none
 * Return value:
 *	SCSLM_SUCCESS: project->pool binding modified
 *	SCSLM_ERROR_UNKNOWN_PROJECT: project not found
 *	SCSLM_ERROR_UNKNOWN_ZONE: zone not found
 *	SCSLM_ERROR: other errors
 */
static int
/* ARGSUSED */
SCSLM_modify_proj_shares_file(char *zone,
	char *project, unsigned int shares)
{
	sc_syslog_msg_handle_t	sys_handle;
	int	err;
	int	ret;
	int	cmd_ret;
	char	*cmd_output;
	char	cmd[MAX_COMMAND_SIZE];
	char	serr[MAX_COMMAND_SIZE];

#if SOL_VERSION >= __s10
	if (strcmp(zone, GLOBAL_ZONENAME) == 0) {
		ret = snprintf(cmd, MAX_COMMAND_SIZE,
		    "%s -s -K \'project.cpu-shares=(priv,%u,none)\' %s 2>&1",
		    PROJ_MOD, shares, project);
		err = errno; /*lint !e746 */
		if (ret >= MAX_COMMAND_SIZE || ret < 0) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM snprintf error <%s>", strerror(err));
			sc_syslog_msg_done(&sys_handle);
			return (SCSLM_ERROR);
		}
#else
	if (strcmp(zone, GLOBAL_ZONENAME) == 0) {
		ret = snprintf(cmd, MAX_COMMAND_SIZE,
		    "%s -p %s -a \'project.cpu-shares=(priv,%u,none)\' 2>&1",
		    PROJ_MODS9, project, shares);
		err = errno; /*lint !e746 */
		if (ret >= MAX_COMMAND_SIZE || ret < 0) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM snprintf error <%s>", strerror(err));
			sc_syslog_msg_done(&sys_handle);
			return (SCSLM_ERROR);
		}
#endif /* SOL_VERSION >= __s10 */
#if SOL_VERSION >= __s10
	} else {
		ret = snprintf(cmd, MAX_COMMAND_SIZE,
		    "%s -S %s %s -s -K"
		    "'project.cpu-shares=\\(priv,%u,none\\)' %s 2>&1",
		    ZONE_CMD, zone, PROJ_MOD, shares, project);
		err = errno; /*lint !e746 */
		if (ret >= MAX_COMMAND_SIZE || ret < 0) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM snprintf error <%s>", strerror(err));
			sc_syslog_msg_done(&sys_handle);
			return (SCSLM_ERROR);
		}
	}
#else
	}
#endif /* SOL_VERSION >= __s10 */
	ret = SCSLM_exec(cmd, &cmd_ret, &cmd_output);
	if (ret != SCSLM_SUCCESS) {
		return (ret);
	}
	if (cmd_ret != 0) {
		if (cmd_output != NULL) {
			ret = snprintf(serr, MAX_COMMAND_SIZE,
			    "Project \"%s\" does not exist", project);
			err = errno; /*lint !e746 */
			if (ret >= MAX_COMMAND_SIZE || ret < 0) {
				(void) sc_syslog_msg_initialize(&sys_handle,
				    SC_SYSLOG_RGM_FED_TAG, "");
				(void) sc_syslog_msg_log(sys_handle,
				    LOG_ERR, MESSAGE,
				    "SCSLM snprintf error <%s>", strerror(err));
				sc_syslog_msg_done(&sys_handle);
				free(cmd_output);
				return (SCSLM_ERROR);
			}
			if (strstr(cmd_output, serr) != NULL) {
				free(cmd_output);
				return (SCSLM_ERROR_UNKNOWN_PROJECT);
			}
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM <%s> ret %d, outputs <%s>",
			    cmd, cmd_ret, cmd_output);
			sc_syslog_msg_done(&sys_handle);
			free(cmd_output);
			return (SCSLM_ERROR);
		} else {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM <%s> ret %d", cmd, cmd_ret);
			sc_syslog_msg_done(&sys_handle);
			return (SCSLM_ERROR);
		}
	} else {
		if (cmd_output != NULL) {
			free(cmd_output);
		}
	}
	return (SCSLM_SUCCESS);
}


/*
 * Function SCSLM_modify_proj_shares_mem
 * Modify project.cpu-shares property of SCSLM project, in
 * memory (prctl command).
 * Args In:
 * 	a project name
 *	a zone name
 *	a share number
 * Args Out:
 *	none
 * Return value:
 *	SCSLM_SUCCESS: project->pool binding modified
 *	SCSLM_ERROR_UNKNOWN_PROJECT: project not found
 *	SCSLM_ERROR_UNKNOWN_ZONE: zone not found
 *	SCSLM_ERROR: other errors
 */
#if SOL_VERSION >= __s10
static int
/* ARGSUSED */
SCSLM_modify_proj_shares_mem(char *zone, char *project, unsigned int shares)
{
	sc_syslog_msg_handle_t	sys_handle;
	int			err;
	int			ret;
	int			cmd_ret;
	char			*cmd_output;
	char			cmd[MAX_COMMAND_SIZE];
	char			serr[MAX_COMMAND_SIZE];

	/* Change project share in memory */
	if (strcmp(zone, GLOBAL_ZONENAME) == 0) {
		ret = snprintf(cmd, MAX_COMMAND_SIZE,
		    "%s -n project.cpu-shares -v %u -r -i project %s 2>&1",
		    PRCTL_CMD, shares, project);
		err = errno; /*lint !e746 */
		if (ret >= MAX_COMMAND_SIZE || ret < 0) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM snprintf error <%s>", strerror(err));
			sc_syslog_msg_done(&sys_handle);
			return (SCSLM_ERROR);
		}
	} else {
		ret = snprintf(cmd, MAX_COMMAND_SIZE,
		    "%s -S %s %s -n project.cpu-shares -v %u -r -i "
		    "project %s 2>&1",
		    ZONE_CMD, zone, PRCTL_CMD, shares,
		    project);
		err = errno; /*lint !e746 */
		if (ret >= MAX_COMMAND_SIZE || ret < 0) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM snprintf error <%s>", strerror(err));
			sc_syslog_msg_done(&sys_handle);
			return (SCSLM_ERROR);
		}
	}
	ret = SCSLM_exec(cmd, &cmd_ret, &cmd_output);
	if (ret != SCSLM_SUCCESS) {
		return (ret);
	}
	if (cmd_ret != 0) {
		if (cmd_output != NULL) {
			ret = snprintf(serr, MAX_COMMAND_SIZE,
			    "No controllable process found");
			err = errno; /*lint !e746 */
			if (ret >= MAX_COMMAND_SIZE || ret < 0) {
				(void) sc_syslog_msg_initialize(&sys_handle,
				    SC_SYSLOG_RGM_FED_TAG, "");
				(void) sc_syslog_msg_log(sys_handle,
				    LOG_ERR, MESSAGE,
				    "SCSLM snprintf error <%s>", strerror(err));
				sc_syslog_msg_done(&sys_handle);
				free(cmd_output);
				return (SCSLM_ERROR);
			}
			if (strstr(cmd_output, serr) == NULL) {
				(void) sc_syslog_msg_initialize(&sys_handle,
				    SC_SYSLOG_RGM_FED_TAG, "");
				(void) sc_syslog_msg_log(sys_handle,
				    LOG_ERR, MESSAGE,
				    "SCSLM <%s> ret %d, outputs <%s>",
				    cmd, cmd_ret, cmd_output);
				sc_syslog_msg_done(&sys_handle);
				free(cmd_output);
				return (SCSLM_ERROR);
			}
			/* no process in the project, not an error */
			free(cmd_output);
		} else {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM <%s> ret %d", cmd, cmd_ret);
			sc_syslog_msg_done(&sys_handle);
			return (SCSLM_ERROR);
		}
	} else {
		if (cmd_output != NULL) {
			free(cmd_output);
		}
	}
	return (SCSLM_SUCCESS);
}
#else
static int
/* ARGSUSED */
SCSLM_modify_proj_shares_mem(char *rname, char *project, unsigned int shares)
{
	sc_syslog_msg_handle_t	sys_handle;
	int			err;
	int			ret;
	int			cmd_ret;
	char			*cmd_output;
	char			cmd[MAX_COMMAND_SIZE];
	pid_t			pid;
	unsigned int		wstat;
	extern int		fed_retrysetproject(char *, char *, char *);

	pid = fork1();
	err = errno; /*lint !e746 */
	if (pid == 0) {
		/* In the child */

		/* Add this process in the project, process is not TRACED */
		ret = fed_retrysetproject(rname, project, "root");
		if (ret != 0) {
			exit(1);
		}
		/*
		 * Now apply project shares to this process,
		 * other processes in this prject, including
		 * TRACED ones (under pmf control) will have
		 * the new incore project.cpu-shares value.
		 */
		ret = snprintf(cmd, MAX_COMMAND_SIZE,
		    "%s -n project.cpu-shares -v %u -r -i process %u 2>&1",
		    PRCTL_CMD, shares, getpid());
		err = errno; /*lint !e746 */
		if (ret >= MAX_COMMAND_SIZE || ret < 0) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM snprintf error <%s>", strerror(err));
			sc_syslog_msg_done(&sys_handle);
			exit(1);
		}
		ret = SCSLM_exec(cmd, &cmd_ret, &cmd_output);
		if (ret != SCSLM_SUCCESS) {
			exit(1);
		}
		if (cmd_ret != 0) {
			if (cmd_output != NULL) {
				(void) sc_syslog_msg_initialize(&sys_handle,
				    SC_SYSLOG_RGM_FED_TAG, "");
				(void) sc_syslog_msg_log(sys_handle,
				    LOG_ERR, MESSAGE,
				    "SCSLM <%s> ret %d, outputs <%s>",
				    cmd, cmd_ret, cmd_output);
				sc_syslog_msg_done(&sys_handle);
				free(cmd_output);
				exit(1);
			} else {
				(void) sc_syslog_msg_initialize(&sys_handle,
				    SC_SYSLOG_RGM_FED_TAG, "");
				(void) sc_syslog_msg_log(sys_handle,
				    LOG_ERR, MESSAGE,
				    "SCSLM <%s> ret %d", cmd, cmd_ret);
				sc_syslog_msg_done(&sys_handle);
				exit(1);
			}
		} else {
			if (cmd_output != NULL) {
				free(cmd_output);
			}
		}
		exit(0);
	}
	if (pid == (pid_t)-1) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Should never occur.
		 * @user_action
		 * Contact your authorized Sun service provider to determine
		 * whether a workaround or patch is available.
		 */
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM fork1 error <%s>", strerror(err));
		sc_syslog_msg_done(&sys_handle);
		return (SCSLM_ERROR);
	}
	while (1) {
		ret = waitpid(pid, (int *)&wstat, 0);
		err = errno; /*lint !e746 */
		if (ret  == -1 && err == EINTR) {
			continue;
		}
		break;
	}
	if (WIFEXITED(wstat) && WEXITSTATUS(wstat) == 0) {
		return (SCSLM_SUCCESS);
	}
	(void) sc_syslog_msg_initialize(&sys_handle,
	    SC_SYSLOG_RGM_FED_TAG, "");
	/*
	 * SCMSGS
	 * @explanation
	 * Should never occur.
	 * @user_action
	 * Contact your authorized Sun service provider to determine whether a
	 * workaround or patch is available.
	 */
	(void) sc_syslog_msg_log(sys_handle,
	    LOG_ERR, MESSAGE,
	    "SCSLM waitpid pid %u ret %d errno %d wstat 0x%x",
	    pid, ret, err, wstat);
	sc_syslog_msg_done(&sys_handle);
	return (SCSLM_ERROR);
}
#endif /* SOL_VERSION >= __s10 */


/*
 * Function SCSLM_fgetprojent
 * Read a SCSLM project from project file.
 * Args In:
 *	project name.
 *	zone name.
 * Args Out:
 *	None.
 * Return value:
 *	SCSLM_SUCCESS: project found in zone /etc/project file
 *	SCSLM_ERROR: failure or project not found
 */
static int
/* ARGSUSED */
SCSLM_fgetprojent(char *projname, char *zone,
    projid_t *projid, unsigned int *shares)
{
	sc_syslog_msg_handle_t	sys_handle;
	char			pbuf[PROJECT_BUFSZ];
	struct project		proj;
	int			err;
	char			*p;
	char			*ps;
	char			*pe;
	FILE			*fi = NULL;
	char			**cred;
	struct stat		statbuf;
#if SOL_VERSION >= __s10
	int			p_open = 0;
	int			ret;
	char			cmd[MAX_COMMAND_SIZE];

	*shares = 0;

	if (strcmp(zone, GLOBAL_ZONENAME) != 0) {
		ret = snprintf(cmd, MAX_COMMAND_SIZE,
		    "%s -S %s getent project %s",
		    ZONE_CMD, zone, projname);
		err = errno; /*lint !e746 */
		if (ret >= MAXPATHLEN || ret < 0) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM snprintf error <%s>", strerror(err));
			sc_syslog_msg_done(&sys_handle);
			return (SCSLM_ERROR);
		}
		fi = popen(cmd, "r");
		err = errno; /*lint !e746 */
		if (fi == NULL) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM <%s> popen error <%s>", cmd, strerror(err));
			sc_syslog_msg_done(&sys_handle);
			return (SCSLM_ERROR);
		}
		p_open = 1;
	}
#endif /* SOL_VERSION >= __s10 */
	if (strcmp(zone, GLOBAL_ZONENAME) == 0) {
		fi = fopen(SCSLM_ETC_PROJECT, "r");
		err = errno; /*lint !e746 */
		if (fi == NULL) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * Should never occur.
			 * @user_action
			 * Contact your authorized Sun service provider to
			 * determine whether a workaround or patch is
			 * available.
			 */
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM "
			    "fopen <%s> error <%s>",
			    SCSLM_ETC_PROJECT, strerror(err));
			sc_syslog_msg_done(&sys_handle);
			return (SCSLM_ERROR);
		}
		if (fstat(fileno(fi), &statbuf) < 0) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * Should never occur.
			 * @user_action
			 * Contact your authorized Sun service provider to
			 * determine whether a workaround or patch is
			 * available.
			 */
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM "
			    "fstat <%s> error <%s>",
			    SCSLM_ETC_PROJECT, strerror(err));
			sc_syslog_msg_done(&sys_handle);
			(void) fclose(fi);
			return (SCSLM_ERROR);
		}
		if (!S_ISREG(statbuf.st_mode)) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * Should never occur.
			 * @user_action
			 * Contact your authorized Sun service provider to
			 * determine whether a workaround or patch is
			 * available.
			 */
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM "
			    "<%s> is not a regular file",
			    SCSLM_ETC_PROJECT);
			sc_syslog_msg_done(&sys_handle);
			(void) fclose(fi);
			return (SCSLM_ERROR);
		}
#if SOL_VERSION < __s10
	} else {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Should never occur.
		 * @user_action
		 * Contact your authorized Sun service provider to determine
		 * whether a workaround or patch is available.
		 */
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM fgetprojent "
		    "S9 proj <%s> zone <%s>", projname, zone);
		sc_syslog_msg_done(&sys_handle);
		return (SCSLM_ERROR);
	}
#else
	}
#endif
	if (fi == NULL) {
		/* Just to make lint happy, never occur */
		return (SCSLM_ERROR);
	}
	while (fgetprojent(fi, &proj, pbuf, PROJECT_BUFSZ) != NULL) {
		if (strcmp(proj.pj_name, projname) != 0) {
			continue;
		}
		if (proj.pj_comment == NULL) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * Should never occur.
			 * @user_action
			 * Contact your authorized Sun service provider to
			 * determine whether a workaround or patch is
			 * available.
			 */
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM "
			    "<%s> zone <%s> corrupted project comment",
			    proj.pj_name, zone);
			sc_syslog_msg_done(&sys_handle);
#if SOL_VERSION >= __s10
			if (p_open != 0) {
				(void) pclose(fi);
			} else {
				(void) fclose(fi);
			}
#else
			(void) fclose(fi);
#endif
			return (SCSLM_ERROR_CORRUPT_PROJECT);
		}
		if (strcmp(proj.pj_comment, SCSLM_GENERATED) != 0) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM "
			    "<%s> zone <%s> corrupted project comment",
			    proj.pj_name, zone);
			sc_syslog_msg_done(&sys_handle);
#if SOL_VERSION >= __s10
			if (p_open != 0) {
				(void) pclose(fi);
			} else {
				(void) fclose(fi);
			}
#else
			(void) fclose(fi);
#endif
			return (SCSLM_ERROR_CORRUPT_PROJECT);
		}

		/* Parse project users list, expected '*' */
		cred = proj.pj_users;
		if (cred == NULL || *cred == NULL) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * Should never occur.
			 * @user_action
			 * Contact your authorized Sun service provider to
			 * determine whether a workaround or patch is
			 * available.
			 */
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM "
			    "<%s> zone <%s> no users",
			    proj.pj_name, zone);
			sc_syslog_msg_done(&sys_handle);
		} else {
			while (*cred != NULL) {
				if (strcmp(*cred, "*") != 0) {
					(void) sc_syslog_msg_initialize(
					    &sys_handle,
					    SC_SYSLOG_RGM_FED_TAG, "");
					/*
					 * SCMSGS
					 * @explanation
					 * Should never occur.
					 * @user_action
					 * Contact your authorized Sun service
					 * provider to determine whether a
					 * workaround or patch is available.
					 */
					(void) sc_syslog_msg_log(sys_handle,
					    LOG_ERR, MESSAGE,
					    "SCSLM "
					    "<%s> zone <%s> "
					    "unexpected user <%s>",
					    proj.pj_name, zone, *cred);
					sc_syslog_msg_done(&sys_handle);
				}
				cred++;
			}
		}

		/* Parse project groups list, expected '*' */
		cred = proj.pj_groups;
		if (cred == NULL || *cred == NULL) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * Should never occur.
			 * @user_action
			 * Contact your authorized Sun service provider to
			 * determine whether a workaround or patch is
			 * available.
			 */
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM "
			    "<%s> zone <%s> no groups",
			    proj.pj_name, zone);
			sc_syslog_msg_done(&sys_handle);
		} else {
			while (*cred != NULL) {
				if (strcmp(*cred, "*") != 0) {
					(void) sc_syslog_msg_initialize(
					    &sys_handle,
					    SC_SYSLOG_RGM_FED_TAG, "");
					/*
					 * SCMSGS
					 * @explanation
					 * Should never occur.
					 * @user_action
					 * Contact your authorized Sun service
					 * provider to determine whether a
					 * workaround or patch is available.
					 */
					(void) sc_syslog_msg_log(sys_handle,
					    LOG_ERR, MESSAGE,
					    "SCSLM "
					    "<%s> zone <%s> "
					    "unexpected group <%s>",
					    proj.pj_name, zone, *cred);
					sc_syslog_msg_done(&sys_handle);
				}
				cred++;
			}
		}

		if ((p = strstr(proj.pj_attr,
		    "project.cpu-shares=(priv,")) != NULL) {
			p = ps = p + strlen("project.cpu-shares=(priv,");
			while (isdigit(*p)) p++;
			pe = p;
			if (*p != ',') {
				(void) sc_syslog_msg_initialize(&sys_handle,
				    SC_SYSLOG_RGM_FED_TAG, "");
				/*
				 * SCMSGS
				 * @explanation
				 * Should never occur.
				 * @user_action
				 * Contact your authorized Sun service
				 * provider to determine whether a workaround
				 * or patch is available.
				 */
				(void) sc_syslog_msg_log(sys_handle,
				    LOG_ERR, MESSAGE,
				    "SCSLM "
				    "<%s> zone <%s> corrupted "
				    "project.cpu-shares",
				    proj.pj_name, zone);
				sc_syslog_msg_done(&sys_handle);
#if SOL_VERSION >= __s10
				if (p_open != 0) {
					(void) pclose(fi);
				} else {
					(void) fclose(fi);
				}
#else
				(void) fclose(fi);
#endif
				return (SCSLM_ERROR_CORRUPT_PROJECT);
			}
			if (strncmp(p, ",none)",
			    strlen(",none)")) == 0) {
				*pe = '\0';
				*shares = (unsigned int)atoi(ps);
				*pe = ',';
			} else {
				(void) sc_syslog_msg_initialize(&sys_handle,
				    SC_SYSLOG_RGM_FED_TAG, "");
				(void) sc_syslog_msg_log(sys_handle,
				    LOG_ERR, MESSAGE,
				    "SCSLM "
				    "<%s> zone <%s> corrupted "
				    "project.cpu-shares",
				    proj.pj_name, zone);
				sc_syslog_msg_done(&sys_handle);
#if SOL_VERSION >= __s10
				if (p_open != 0) {
					(void) pclose(fi);
				} else {
					(void) fclose(fi);
				}
#else
				(void) fclose(fi);
#endif
				return (SCSLM_ERROR_CORRUPT_PROJECT);
			}
			p += strlen(",none)");
			if (*p == '\0' || *p == ';') {
				if (*shares == 0) {
					(void) sc_syslog_msg_initialize(
					    &sys_handle,
					    SC_SYSLOG_RGM_FED_TAG, "");
					(void) sc_syslog_msg_log(sys_handle,
					    LOG_ERR, MESSAGE,
					    "SCSLM "
					    "<%s> zone <%s> corrupted "
					    "project.cpu-shares",
					    proj.pj_name, zone);
					sc_syslog_msg_done(&sys_handle);
#if SOL_VERSION >= __s10
					if (p_open != 0) {
						(void) pclose(fi);
					} else {
						(void) fclose(fi);
					}
#else
					(void) fclose(fi);
#endif
					return (SCSLM_ERROR_CORRUPT_PROJECT);
				}
				*projid = proj.pj_projid;
#if SOL_VERSION >= __s10
				if (p_open != 0) {
					(void) pclose(fi);
				} else {
					(void) fclose(fi);
				}
#else
				(void) fclose(fi);
#endif
				return (SCSLM_SUCCESS);
			} else {
				(void) sc_syslog_msg_initialize(&sys_handle,
				    SC_SYSLOG_RGM_FED_TAG, "");
				(void) sc_syslog_msg_log(sys_handle,
				    LOG_ERR, MESSAGE,
				    "SCSLM "
				    "<%s> zone <%s> corrupted "
				    "project.cpu-shares",
				    proj.pj_name, zone);
				sc_syslog_msg_done(&sys_handle);
#if SOL_VERSION >= __s10
				if (p_open != 0) {
					(void) pclose(fi);
				} else {
					(void) fclose(fi);
				}
#else
				(void) fclose(fi);
#endif
				return (SCSLM_ERROR_CORRUPT_PROJECT);
			}
		} else {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * Should never occur.
			 * @user_action
			 * Contact your authorized Sun service provider to
			 * determine whether a workaround or patch is
			 * available.
			 */
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM "
			    "<%s> zone <%s> missing project.cpu-shares",
			    proj.pj_name, zone);
			sc_syslog_msg_done(&sys_handle);
#if SOL_VERSION >= __s10
			if (p_open != 0) {
				(void) pclose(fi);
			} else {
				(void) fclose(fi);
			}
#else
			(void) fclose(fi);
#endif
			return (SCSLM_ERROR_CORRUPT_PROJECT);
		}
	}
#if SOL_VERSION >= __s10
	if (p_open != 0) {
		(void) pclose(fi);
	} else {
		(void) fclose(fi);
	}
#else
	(void) fclose(fi);
#endif
	LOG_FED_SLM_INFO3(sys_handle, "SCSLM "
	    "unknown project <%s> zone <%s>", projname, zone);
	return (SCSLM_ERROR_UNKNOWN_PROJECT);
}


/*
 * Functions used to store all projects created by SCSLM.
 * Is used for checks and for SCSLM project cleanup.
 */


/*
 * Function SCSLM_file_del_proj
 * Delete a SCSLM project from the file used
 * to store all SCSLM projects created.
 * Args In:
 *	project name.
 *	errors displayed or not
 * Args Out:
 *	None.
 * Return value:
 *	SCSLM_SUCCESS: project remove successfuly from file
 *	SCSLM_ERROR: failure
 */
/*lint -e528 */
static int
SCSLM_file_del_proj(char *zone, char *projname, int errors)
{
	sc_syslog_msg_handle_t	sys_handle;
	int	err;
	char	*ptr;
	size_t	size = 0;
	off_t	off;
	int	fd;
	char	*p_s;
	char	*p_e;
	char	*p;
	int	cnt_tab;
	int	deleted = 0;
	char	*fzone;
	char	proj[PROJNAME_MAX + 2];

	if (projname == NULL) {
		return (SCSLM_ERROR);
	}

	fd = open(SCSLM_PROJ_FILE, O_RDWR | O_SYNC);
	err = errno; /*lint !e746 */
	if (fd < 0) {
		if (errors != 0) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM "
			    "open <%s> error <%s>",
			    SCSLM_PROJ_FILE, strerror(err));
			sc_syslog_msg_done(&sys_handle);
			return (SCSLM_ERROR);
		} else {
			return (SCSLM_SUCCESS);
		}
	}
	off = lseek(fd, 0, SEEK_END);
	if (off == (off_t)-1) {
		err = errno; /*lint !e746 */
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Should never occur.
		 * @user_action
		 * Contact your authorized Sun service provider to determine
		 * whether a workaround or patch is available.
		 */
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM "
		    "lseek <%s> error <%s>",
		    SCSLM_PROJ_FILE, strerror(err));
		sc_syslog_msg_done(&sys_handle);
		(void) close(fd);
		return (SCSLM_ERROR);
	}
	if (off == 0) {
		/* file is emty */
		if (errors != 0) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * Should never occur.
			 * @user_action
			 * Contact your authorized Sun service provider to
			 * determine whether a workaround or patch is
			 * available.
			 */
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM "
			    "file <%s> is empty", SCSLM_PROJ_FILE);
			sc_syslog_msg_done(&sys_handle);
			(void) close(fd);
			return (SCSLM_ERROR);
		} else {
			(void) close(fd);
			return (SCSLM_SUCCESS);
		}
	}
	ptr = (char *)mmap((caddr_t)0, (size_t)off,
	    (PROT_READ | PROT_WRITE), MAP_SHARED, fd, 0);
	if (ptr == (char *)MAP_FAILED) {
		err = errno; /*lint !e746 */
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Should never occur.
		 * @user_action
		 * Contact your authorized Sun service provider to determine
		 * whether a workaround or patch is available.
		 */
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM "
		    "mmap <%s> error <%s>",
		    SCSLM_PROJ_FILE, strerror(err));
		sc_syslog_msg_done(&sys_handle);
		(void) close(fd);
		return (SCSLM_ERROR);
	}
	(void) sprintf(proj, "%s\t", projname);
	p_s = (char *)strstr(ptr, proj);
	while (p_s != NULL && deleted == 0) {
		p_e = p_s;
		while ((p_e < (ptr + (size_t)off)) && (*p_e != '\n')) {
			p_e++;
		}
		if (*p_e != '\n') {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * Should never occur.
			 * @user_action
			 * Contact your authorized Sun service provider to
			 * determine whether a workaround or patch is
			 * available.
			 */
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM "
			    "file <%s> corrupted", SCSLM_PROJ_FILE);
			sc_syslog_msg_done(&sys_handle);
			(void) close(fd);
			(void) munmap(ptr, (size_t)off);
			return (SCSLM_ERROR);
		}
		p_e++;
		/* Now verify entry starting at p_s and ending at p_e */
		p = p_s;
		cnt_tab = 0;
		fzone = NULL;
		while (p < p_e) {
			if (*p == '\t') {
				cnt_tab++;
				if (cnt_tab == 3) {
					fzone = p + 1;
				}
			}
			p++;
		}
		if (fzone == NULL) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM "
			    "file <%s> corrupted", SCSLM_PROJ_FILE);
			sc_syslog_msg_done(&sys_handle);
			(void) close(fd);
			(void) munmap(ptr, (size_t)off);
			return (SCSLM_ERROR);
		}
		*(p_e - 1) = '\0';
		if (strcmp(zone, fzone) != 0) {
			*(p_e - 1) = '\n';
			p_s = (char *)strstr(p_e, proj);
			continue;
		}
		*(p_e - 1) = '\n';
		size = (size_t)(ptr + (size_t)off - p_e);
		if (size != 0) {
			(void) memcpy(p_s, p_e, size);
		}
		deleted = 1;
	}

	if ((p_s != NULL) && (deleted != 0) &&
	    (ftruncate(fd, (off_t)(p_s - ptr + (char *)size)) < 0)) {
		err = errno; /*lint !e746 */
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Should never occur.
		 * @user_action
		 * Contact your authorized Sun service provider to determine
		 * whether a workaround or patch is available.
		 */
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM "
		    "ftruncate <%s> error <%s>",
		    SCSLM_PROJ_FILE, strerror(err));
		sc_syslog_msg_done(&sys_handle);
		(void) close(fd);
		(void) munmap(ptr, (size_t)off);
		return (SCSLM_ERROR);
	}
	(void) close(fd);
	if (munmap(ptr, (size_t)off) < 0) {
		err = errno; /*lint !e746 */
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Should never occur.
		 * @user_action
		 * Contact your authorized Sun service provider to determine
		 * whether a workaround or patch is available.
		 */
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM "
		    "munmap <%s> error <%s>",
		    SCSLM_PROJ_FILE, strerror(err));
		sc_syslog_msg_done(&sys_handle);
		return (SCSLM_ERROR);
	}
	if (deleted == 0) {
		return (SCSLM_ERROR_UNKNOWN_PROJECT);
	}
	return (SCSLM_SUCCESS);
}


/*
 * Function SCSLM_file_add_proj
 * Add a SCSLM project to the file used
 * to store all SCSLM projects created.
 * Args In:
 *	project name.
 *	zone name
 * Args Out:
 *	None.
 * Return value:
 *	SCSLM_SUCCESS: project remove successfuly from file
 *	SCSLM_ERROR: failure
 */
static int
SCSLM_file_add_proj(char *zone, char *projname, projid_t projid, char *rg)
{
	sc_syslog_msg_handle_t	sys_handle;
	int	err;
	int	fd;
	off_t	off;
	size_t	size;
	char	buf[MAXPATHLEN];

	fd = open(SCSLM_PROJ_FILE, O_RDWR | O_SYNC | O_CREAT,
	    S_IRUSR | S_IWUSR);
	err = errno; /*lint !e746 */
	if (fd < 0) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM "
		    "open <%s> error <%s>",
		    SCSLM_PROJ_FILE, strerror(err));
		sc_syslog_msg_done(&sys_handle);
		return (SCSLM_ERROR);
	}
	off = lseek(fd, 0, SEEK_END);
	if (off == (off_t)-1) {
		err = errno; /*lint !e746 */
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM "
		    "lseek <%s> error <%s>",
		    SCSLM_PROJ_FILE, strerror(err));
		sc_syslog_msg_done(&sys_handle);
		(void) close(fd);
		return (SCSLM_ERROR);
	}
	if (off == (off_t)0) {
		(void) snprintf(buf, MAXPATHLEN,
		    "#\n# %s, do not modify or delete this file\n"
		    "# Updated by SC fed daemon, "
		    "read by SC scslm_projcleanup binary\n#\n",
		    SCSLM_GENERATED);
		size = strlen(buf);
		if (write(fd, buf, size) != (int)size) {
			err = errno; /*lint !e746 */
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * Should never occur.
			 * @user_action
			 * Contact your authorized Sun service provider to
			 * determine whether a workaround or patch is
			 * available.
			 */
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM "
			    "write <%s> error <%s>",
			    SCSLM_PROJ_FILE, strerror(err));
			sc_syslog_msg_done(&sys_handle);
			(void) close(fd);
			return (SCSLM_ERROR);
		}
	}
	(void) snprintf(buf, MAXPATHLEN,
	    "%s\t%u\t%s\t%s\n", projname, (unsigned int)projid, rg, zone);
	size = strlen(buf);
	if (write(fd, buf, size) != (int)size) {
		err = errno; /*lint !e746 */
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM "
		    "write <%s> error <%s>",
		    SCSLM_PROJ_FILE, strerror(err));
		sc_syslog_msg_done(&sys_handle);
		(void) close(fd);
		return (SCSLM_ERROR);
	}
	(void) close(fd);
	return (SCSLM_SUCCESS);
}


/*
 * Function SCSLM_file_close_proj
 * Close the file used to check projects.
 * (this file stores all SCSLM projects created).
 * Args In:
 *	None.
 * Args Out:
 *	None.
 * Return value:
 *	SCSLM_SUCCESS: project remove successfuly from file
 *	SCSLM_ERROR: failure
 */
static int
SCSLM_file_close_proj(void)
{
	sc_syslog_msg_handle_t	sys_handle;
	int	err;

	if (slm_proj_file < 0) {
		slm_proj_file = -1;
		return (SCSLM_SUCCESS);
	}
	(void) close(slm_proj_file);
	slm_proj_file = -1;
	if (munmap((void *)slm_proj_file_ptr, slm_proj_file_size) < 0) {
		err = errno; /*lint !e746 */
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM "
		    "munmap <%s> error <%s>",
		    SCSLM_PROJ_FILE, strerror(err));
		sc_syslog_msg_done(&sys_handle);
		return (SCSLM_ERROR);
	}
	return (SCSLM_SUCCESS);
}


/*
 * Function SCSLM_file_open_proj
 * Open the file used to check projects.
 * (this file stores all SCSLM projects created).
 * Args In:
 *	None.
 * Args Out:
 *	None.
 * Return value:
 *	SCSLM_SUCCESS: SCSLM project file opened
 *	SCSLM_ERROR: failure
 */
static int
SCSLM_file_open_proj(void)
{
	sc_syslog_msg_handle_t	sys_handle;
	off_t	off;
	int	err;

	if (slm_proj_file > 0) {
		return (SCSLM_SUCCESS);
	}

	slm_proj_file = open(SCSLM_PROJ_FILE, O_RDWR | O_SYNC);
	if (slm_proj_file < 0) {
		err = errno; /*lint !e746 */
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM "
		    "open <%s> error <%s>",
		    SCSLM_PROJ_FILE, strerror(err));
		sc_syslog_msg_done(&sys_handle);
		slm_proj_file = -1;
		return (SCSLM_ERROR);
	}
	off = lseek(slm_proj_file, 0, SEEK_END);
	if (off == (off_t)-1) {
		err = errno; /*lint !e746 */
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM "
		    "lseek <%s> error <%s>",
		    SCSLM_PROJ_FILE, strerror(err));
		sc_syslog_msg_done(&sys_handle);
		(void) close(slm_proj_file);
		slm_proj_file = -1;
		return (SCSLM_ERROR);
	}
	if (off == 0) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Should never occur.
		 * @user_action
		 * Contact your authorized Sun service provider to determine
		 * whether a workaround or patch is available.
		 */
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM "
		    "lseek <%s> off is 0",
		    SCSLM_PROJ_FILE);
		sc_syslog_msg_done(&sys_handle);
		(void) close(slm_proj_file);
		slm_proj_file = -1;
		return (SCSLM_ERROR);
	}
	slm_proj_file_size = (size_t)off;
	slm_proj_file_ptr = (char *)mmap(
	    (caddr_t)0, slm_proj_file_size, (PROT_READ | PROT_WRITE),
	    MAP_PRIVATE, slm_proj_file, 0);
	if (slm_proj_file_ptr == (char *)MAP_FAILED) {
		err = errno; /*lint !e746 */
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM "
		    "mmap <%s> error <%s>",
		    SCSLM_PROJ_FILE, strerror(err));
		sc_syslog_msg_done(&sys_handle);
		(void) close(slm_proj_file);
		slm_proj_file = -1;
		return (SCSLM_ERROR);
	}
	/* Terminate with 0 in the private copy */
	slm_proj_file_ptr[slm_proj_file_size - 1] = '\0';
	return (SCSLM_SUCCESS);
}


/*
 * Function SCSLM_file_srch_proj
 * Search a project in the file used to check projects.
 * (this file stores all SCSLM projects created).
 * Args In:
 *	zone name
 *	project name
 * Args Out:
 *	a projid
 *	a rg name
 * Return value:
 *	SCSLM_SUCCESS: project remove successfuly from file
 *	SCSLM_ERROR: failure
 */
static int
SCSLM_file_srch_proj(char *zone, char *projname, projid_t *projid, char **rg)
{
	sc_syslog_msg_handle_t	sys_handle;
	char	*ptr;
	char	*ptr_s;
	char	*p_s;
	char	*p_e;
	int	count = 0;
	size_t	size;
	int	err;
	char	proj[PROJNAME_MAX + 2];
	projid_t	saved_projid = -1;
	char		**saved_rg = NULL;

	*projid = -1;
	*rg = NULL;
	if (*zone == NULL) {
		return (SCSLM_ERROR);
	}
	if (projname == NULL) {
		return (SCSLM_ERROR);
	}

	(void) sprintf(proj, "%s\t", projname);

	ptr = slm_proj_file_ptr;
	p_s = (char *)strstr(ptr, proj);
	while (p_s != NULL) {
		p_e = p_s;
		/* Search the end of the line */
		while (*p_e != '\n' && *p_e != '\0') {
			p_e++;
		}
		if (*p_e != '\n' && p_e >
		    (slm_proj_file_ptr + slm_proj_file_size - 1)) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM file <%s> corrupted", SCSLM_PROJ_FILE);
			sc_syslog_msg_done(&sys_handle);
			return (SCSLM_ERROR);
		}
		/* skip projname */
		ptr_s = ptr = p_s;
		while (*ptr != '\t' && ptr < p_e) {
			ptr++;
		}
		if (ptr >= p_e) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM file <%s> corrupted", SCSLM_PROJ_FILE);
			sc_syslog_msg_done(&sys_handle);
			return (SCSLM_ERROR);
		}
		ptr++;
		/* Extract projid */
		ptr_s = ptr;
		size = 0;
		while (*ptr != '\t' && ptr < p_e) {
			if (!isdigit(*ptr)) {
				(void) sc_syslog_msg_initialize(&sys_handle,
				    SC_SYSLOG_RGM_FED_TAG, "");
				(void) sc_syslog_msg_log(sys_handle,
				    LOG_ERR, MESSAGE,
				    "SCSLM file <%s> corrupted",
				    SCSLM_PROJ_FILE);
				sc_syslog_msg_done(&sys_handle);
				return (SCSLM_ERROR);
			}
			ptr++;
			size++;
		}
		if (ptr >= p_e) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM file <%s> corrupted", SCSLM_PROJ_FILE);
			sc_syslog_msg_done(&sys_handle);
			return (SCSLM_ERROR);
		}
		if (size == 0) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM file <%s> corrupted", SCSLM_PROJ_FILE);
			sc_syslog_msg_done(&sys_handle);
			return (SCSLM_ERROR);
		}
		*ptr = '\0';
		*projid = (projid_t)atoi(ptr_s);
		*ptr = '\t';
		ptr++;
		/* Extract rg name */
		ptr_s = ptr;
		size = 0;
		while (*ptr != '\t' && ptr < p_e) {
			ptr++;
			size++;
		}
		if (ptr >= p_e) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM file <%s> corrupted", SCSLM_PROJ_FILE);
			sc_syslog_msg_done(&sys_handle);
			return (SCSLM_ERROR);
		}
		if (size == 0) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM file <%s> corrupted", SCSLM_PROJ_FILE);
			sc_syslog_msg_done(&sys_handle);
			return (SCSLM_ERROR);
		}
		*ptr = '\0';
		if (*rg == NULL) {
			*rg = strdup(ptr_s);
		} else {
			free(*rg);
			*rg = strdup(ptr_s);
		}
		if (*rg == NULL) {
			err = errno; /*lint !e746 */
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM "
			    "strdup error <%s>", strerror(err));
			sc_syslog_msg_done(&sys_handle);
			return (SCSLM_ERROR);
		}
		*ptr = '\t';
		ptr++;
		/* Extract zone name */
		ptr_s = ptr;
		size = 0;
		while (*ptr != '\n' && ptr <= p_e) {
			ptr++;
			size++;
		}
		if (size == 0) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM file <%s> corrupted", SCSLM_PROJ_FILE);
			sc_syslog_msg_done(&sys_handle);
			return (SCSLM_ERROR);
		}
		*ptr = '\0';
		if (strcmp(zone, ptr_s) == 0) {
			saved_projid = *projid;
			saved_rg = rg;
			count++;
		}
		*ptr = '\n';
		*p_e = '\0';
		*p_e = '\n';
		p_s = (char *)strstr(p_e, proj);
	}

	if (count == 1) {
		*projid = saved_projid;
		rg = saved_rg;
		return (SCSLM_SUCCESS);
	}
	if (count > 1) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Should never occur.
		 * @user_action
		 * Contact your authorized Sun service provider to determine
		 * whether a workaround or patch is available.
		 */
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM file <%s> duplicate project name", SCSLM_PROJ_FILE);
		sc_syslog_msg_done(&sys_handle);
		return (SCSLM_ERROR);
	}
	LOG_FED_SLM_INFO4(sys_handle,
	    "SCSLM WARNING: "
	    "zone <%s> project <%s> not found in file %s",
	    zone, projname, SCSLM_PROJ_FILE);
	return (SCSLM_ERROR_UNKNOWN_PROJECT);
}



#if SOL_VERSION >= __s10
/*
 * -----------------------------------------------------------------------------
 *
 * Pools/Psets/Zones handling routines
 *
 * -----------------------------------------------------------------------------
 */

/*
 * Function SCSLM_create_pool
 * Create a Solaris DRP pool.
 * Args In:
 * 	a pool name
 * Args Out:
 *	none
 * Return value:
 *	SCSLM_SUCCESS: pool created
 *	SCSLM_ERROR_EXISTING_POOL: pool already exist
 *	SCSLM_ERROR: error
 */
static int
SCSLM_create_pool(char *pool)
{
	sc_syslog_msg_handle_t	sys_handle;
	int		err;
	int		ret;
	pool_conf_t	*pool_conf;
	pool_value_t	*pool_value;
	pool_elem_t	*pool_elem;
	pool_t		*mpool;
	const char	*static_conf;
	char		scslmpool[MAX_COMMAND_SIZE];

	ret = snprintf(scslmpool, MAX_COMMAND_SIZE, "%s%s", SCSLM_POOL, pool);
	err = errno; /*lint !e746 */
	if (ret >= MAX_COMMAND_SIZE || ret < 0) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM snprintf error <%s>", strerror(err));
		sc_syslog_msg_done(&sys_handle);
		return (SCSLM_ERROR);
	}
	pool_value = pool_value_alloc();
	if (pool_value == NULL) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Should never occur.
		 * @user_action
		 * Contact your authorized Sun service provider to determine
		 * whether a workaround or patch is available.
		 */
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM <%s> "
		    "pool_value_alloc error <%s>",
		    scslmpool, pool_strerror(pool_error()));
		sc_syslog_msg_done(&sys_handle);
		return (SCSLM_ERROR);
	}
	static_conf = pool_static_location();
	pool_conf = pool_conf_alloc();
	if (pool_conf == NULL) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Should never occur.
		 * @user_action
		 * Contact your authorized Sun service provider to determine
		 * whether a workaround or patch is available.
		 */
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM <%s> "
		    "pool_conf_alloc error <%s>",
		    scslmpool, pool_strerror(pool_error()));
		sc_syslog_msg_done(&sys_handle);
		pool_value_free(pool_value);
		return (SCSLM_ERROR);
	}
	if (pool_conf_open(pool_conf, static_conf, PO_RDWR) < 0) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Should never occur.
		 * @user_action
		 * Contact your authorized Sun service provider to determine
		 * whether a workaround or patch is available.
		 */
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM <%s> "
		    "pool_conf_open <%s> error <%s>",
		    scslmpool, static_conf, pool_strerror(pool_error()));
		sc_syslog_msg_done(&sys_handle);
		pool_conf_free(pool_conf);
		pool_value_free(pool_value);
		return (SCSLM_ERROR);
	}
	mpool = pool_get_pool(pool_conf, scslmpool);
	if (mpool != NULL) {
		if (pool_conf_close(pool_conf) < 0) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * Should never occur.
			 * @user_action
			 * Contact your authorized Sun service provider to
			 * determine whether a workaround or patch is
			 * available.
			 */
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM <%s> "
			    "pool_conf_close error <%s>",
			    scslmpool, pool_strerror(pool_error()));
			sc_syslog_msg_done(&sys_handle);
		}
		pool_conf_free(pool_conf);
		pool_value_free(pool_value);
		return (SCSLM_ERROR_EXISTING_POOL);
	}
	mpool = pool_create(pool_conf, scslmpool);
	if (mpool == NULL) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Should never occur.
		 * @user_action
		 * Contact your authorized Sun service provider to determine
		 * whether a workaround or patch is available.
		 */
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM <%s> "
		    "pool_create error <%s>",
		    scslmpool, pool_strerror(pool_error()));
		sc_syslog_msg_done(&sys_handle);
		if (pool_conf_close(pool_conf) < 0) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM <%s> "
			    "pool_conf_close error <%s>",
			    scslmpool, pool_strerror(pool_error()));
			sc_syslog_msg_done(&sys_handle);
		}
		pool_conf_free(pool_conf);
		pool_value_free(pool_value);
		return (SCSLM_ERROR);
	}
	if (pool_value_set_string(pool_value, SCSLM_GENERATED) < 0) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Should never occur.
		 * @user_action
		 * Contact your authorized Sun service provider to determine
		 * whether a workaround or patch is available.
		 */
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM <%s> "
		    "pool_value_set_string error <%s>",
		    scslmpool, pool_strerror(pool_error()));
		sc_syslog_msg_done(&sys_handle);
		if (pool_conf_close(pool_conf) < 0) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM <%s> "
			    "pool_conf_close error <%s>",
			    scslmpool, pool_strerror(pool_error()));
			sc_syslog_msg_done(&sys_handle);
		}
		pool_conf_free(pool_conf);
		pool_value_free(pool_value);
		return (SCSLM_ERROR);
	}
	pool_elem = pool_to_elem(pool_conf, mpool);
	if (pool_elem == NULL) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Should never occur.
		 * @user_action
		 * Contact your authorized Sun service provider to determine
		 * whether a workaround or patch is available.
		 */
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM <%s> "
		    "pool_to_elem error <%s>",
		    scslmpool, pool_strerror(pool_error()));
		sc_syslog_msg_done(&sys_handle);
		if (pool_conf_close(pool_conf) < 0) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM <%s> "
			    "pool_conf_close error <%s>",
			    scslmpool, pool_strerror(pool_error()));
			sc_syslog_msg_done(&sys_handle);
		}
		pool_conf_free(pool_conf);
		pool_value_free(pool_value);
		return (SCSLM_ERROR);
	}
	if (pool_put_property(pool_conf, pool_elem,
	    (const char *)"pool.comment", pool_value) < 0) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Should never occur.
		 * @user_action
		 * Contact your authorized Sun service provider to determine
		 * whether a workaround or patch is available.
		 */
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM <%s> "
		    "pool_put_property <%s> error <%s>",
		    scslmpool, "pool.comment", pool_strerror(pool_error()));
		sc_syslog_msg_done(&sys_handle);
		if (pool_conf_close(pool_conf) < 0) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM <%s> "
			    "pool_conf_close error <%s>",
			    scslmpool, pool_strerror(pool_error()));
			sc_syslog_msg_done(&sys_handle);
		}
		pool_conf_free(pool_conf);
		pool_value_free(pool_value);
		return (SCSLM_ERROR);
	}
	/* Commit in the dynamic configuration */
	if (pool_conf_commit(pool_conf, PO_TRUE) < 0) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Should never occur.
		 * @user_action
		 * Contact your authorized Sun service provider to determine
		 * whether a workaround or patch is available.
		 */
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM <%s> "
		    "pool_conf_commit dynamic error <%s>",
		    scslmpool, pool_strerror(pool_error()));
		sc_syslog_msg_done(&sys_handle);
		if (pool_conf_close(pool_conf) < 0) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM <%s> "
			    "pool_conf_close error <%s>",
			    scslmpool, pool_strerror(pool_error()));
			sc_syslog_msg_done(&sys_handle);
		}
		pool_conf_free(pool_conf);
		pool_value_free(pool_value);
		return (SCSLM_ERROR);
	} else {
		/* Commit in the static configuration */
		if (pool_conf_commit(pool_conf, PO_FALSE) < 0) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * Should never occur.
			 * @user_action
			 * Contact your authorized Sun service provider to
			 * determine whether a workaround or patch is
			 * available.
			 */
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM <%s> "
			    "pool_conf_commit static error <%s>",
			    scslmpool, pool_strerror(pool_error()));
			sc_syslog_msg_done(&sys_handle);
			if (pool_destroy(pool_conf, mpool) < 0) {
				(void) sc_syslog_msg_initialize(&sys_handle,
				    SC_SYSLOG_RGM_FED_TAG, "");
				/*
				 * SCMSGS
				 * @explanation
				 * Should never occur.
				 * @user_action
				 * Contact your authorized Sun service
				 * provider to determine whether a workaround
				 * or patch is available.
				 */
				(void) sc_syslog_msg_log(sys_handle,
				    LOG_ERR, MESSAGE,
				    "SCSLM <%s> "
				    "pool_destroy error <%s>",
				    scslmpool, pool_strerror(pool_error()));
				sc_syslog_msg_done(&sys_handle);
				if (pool_conf_close(pool_conf) < 0) {
					(void) sc_syslog_msg_initialize(
					    &sys_handle,
					    SC_SYSLOG_RGM_FED_TAG, "");
					(void) sc_syslog_msg_log(sys_handle,
					    LOG_ERR, MESSAGE,
					    "SCSLM <%s> "
					    "pool_conf_close error <%s>",
					    scslmpool,
					    pool_strerror(pool_error()));
					sc_syslog_msg_done(&sys_handle);
				}
				pool_conf_free(pool_conf);
				pool_value_free(pool_value);
				return (SCSLM_ERROR);
			}
			/* Commit in the dynamic configuration */
			if (pool_conf_commit(pool_conf, PO_TRUE) < 0) {
				(void) sc_syslog_msg_initialize(&sys_handle,
				    SC_SYSLOG_RGM_FED_TAG, "");
				(void) sc_syslog_msg_log(sys_handle,
				    LOG_ERR, MESSAGE,
				    "SCSLM <%s> "
				    "pool_conf_commit dynamic error <%s>",
				    scslmpool, pool_strerror(pool_error()));
				sc_syslog_msg_done(&sys_handle);
			}
			pool_conf_free(pool_conf);
			pool_value_free(pool_value);
			return (SCSLM_ERROR);
		}
	}
	if (pool_conf_close(pool_conf) < 0) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM <%s> "
		    "pool_conf_close error <%s>",
		    scslmpool, pool_strerror(pool_error()));
		sc_syslog_msg_done(&sys_handle);
	}
	pool_conf_free(pool_conf);
	pool_value_free(pool_value);
	return (SCSLM_SUCCESS);
}


/*
 * Function SCSLM_delete_pool
 * Delete a Solaris DRP pool.
 * Args In:
 * 	a pool name
 * Args Out:
 *	none
 * Return value:
 *	SCSLM_SUCCESS: pool deleted
 *	SCSLM_ERROR_UNKNOWN_POOL: pool does not exist
 *	SCSLM_ERROR: other errors
 */
static int
SCSLM_delete_pool(char *pool)
{
	sc_syslog_msg_handle_t	sys_handle;
	int		err;
	int		ret;
	pool_conf_t	*pool_conf;
	pool_t		*mpool;
	const char	*static_conf;
	char		scslmpool[MAX_COMMAND_SIZE];

	ret = snprintf(scslmpool, MAX_COMMAND_SIZE, "%s%s", SCSLM_POOL, pool);
	err = errno; /*lint !e746 */
	if (ret >= MAX_COMMAND_SIZE || ret < 0) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM snprintf error <%s>", strerror(err));
		sc_syslog_msg_done(&sys_handle);
		return (SCSLM_ERROR);
	}
	static_conf = pool_static_location();
	pool_conf = pool_conf_alloc();
	if (pool_conf == NULL) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM <%s> "
		    "pool_conf_alloc error <%s>",
		    scslmpool, pool_strerror(pool_error()));
		sc_syslog_msg_done(&sys_handle);
		return (SCSLM_ERROR);
	}
	if (pool_conf_open(pool_conf, static_conf, PO_RDWR) < 0) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM <%s> "
		    "pool_conf_open <%s> error <%s>",
		    scslmpool, static_conf, pool_strerror(pool_error()));
		sc_syslog_msg_done(&sys_handle);
		pool_conf_free(pool_conf);
		return (SCSLM_ERROR);
	}
	mpool = pool_get_pool(pool_conf, scslmpool);
	if (mpool == NULL) {
		if (pool_conf_close(pool_conf) < 0) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM <%s> "
			    "pool_conf_close error <%s>",
			    scslmpool, pool_strerror(pool_error()));
			sc_syslog_msg_done(&sys_handle);
		}
		pool_conf_free(pool_conf);
		return (SCSLM_ERROR_UNKNOWN_POOL);
	}
	if (pool_destroy(pool_conf, mpool) < 0) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM <%s> "
		    "pool_destroy error <%s>",
		    scslmpool, pool_strerror(pool_error()));
		sc_syslog_msg_done(&sys_handle);
		if (pool_conf_close(pool_conf) < 0) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM <%s> "
			    "pool_conf_close error <%s>",
			    scslmpool, pool_strerror(pool_error()));
			sc_syslog_msg_done(&sys_handle);
		}
		pool_conf_free(pool_conf);
		return (SCSLM_ERROR);
	}
	/* Commit in the dynamic configuration */
	if (pool_conf_commit(pool_conf, PO_TRUE) < 0) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM <%s> "
		    "pool_conf_commit dynamic error <%s>",
		    scslmpool, pool_strerror(pool_error()));
		sc_syslog_msg_done(&sys_handle);
		if (pool_conf_close(pool_conf) < 0) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM <%s> "
			    "pool_conf_close error <%s>",
			    scslmpool, pool_strerror(pool_error()));
			sc_syslog_msg_done(&sys_handle);
		}
		pool_conf_free(pool_conf);
		return (SCSLM_ERROR);
	} else {
		/* Commit in the static configuration */
		if (pool_conf_commit(pool_conf, PO_FALSE) < 0) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM <%s> "
			    "pool_conf_commit static error <%s>",
			    scslmpool, pool_strerror(pool_error()));
			sc_syslog_msg_done(&sys_handle);
			if (pool_conf_close(pool_conf) < 0) {
				(void) sc_syslog_msg_initialize(&sys_handle,
				    SC_SYSLOG_RGM_FED_TAG, "");
				(void) sc_syslog_msg_log(sys_handle,
				    LOG_ERR, MESSAGE,
				    "SCSLM <%s> "
				    "pool_conf_close error <%s>",
				    scslmpool, pool_strerror(pool_error()));
				sc_syslog_msg_done(&sys_handle);
			}
			pool_conf_free(pool_conf);
			return (SCSLM_ERROR);
		}
	}
	if (pool_conf_close(pool_conf) < 0) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM <%s> "
		    "pool_conf_close error <%s>",
		    scslmpool, pool_strerror(pool_error()));
		sc_syslog_msg_done(&sys_handle);
	}
	pool_conf_free(pool_conf);
	return (SCSLM_SUCCESS);
}


/*
 * Function SCSLM_create_pset
 * Create a Solaris DRP pset.
 * Args In:
 * 	a pset name
 * Args Out:
 *	none
 * Return value:
 *	SCSLM_SUCCESS: pset created
 *	SCSLM_ERROR_EXISTING_PSET: pset already exist
 *	SCSLM_ERROR: other errors
 */
static int
SCSLM_create_pset(char *pset, uint64_t pmin, uint64_t pmax)
{
	sc_syslog_msg_handle_t	sys_handle;
	int		err;
	int		ret;
	pool_conf_t	*pool_conf;
	pool_value_t	*pool_value;
	pool_elem_t	*pool_elem;
	pool_resource_t	*resource;
	const char	*static_conf;
	char		scslmpset[MAX_COMMAND_SIZE];

	ret = snprintf(scslmpset, MAX_COMMAND_SIZE, "%s%s", SCSLM_PSET, pset);
	err = errno; /*lint !e746 */
	if (ret >= MAX_COMMAND_SIZE || ret < 0) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM snprintf error <%s>", strerror(err));
		sc_syslog_msg_done(&sys_handle);
		return (SCSLM_ERROR);
	}
	pool_value = pool_value_alloc();
	if (pool_value == NULL) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM <%s> "
		    "pool_value_alloc error <%s>",
		    scslmpset, pool_strerror(pool_error()));
		sc_syslog_msg_done(&sys_handle);
		return (SCSLM_ERROR);
	}
	static_conf = pool_static_location();
	pool_conf = pool_conf_alloc();
	if (pool_conf == NULL) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM <%s> "
		    "pool_conf_alloc error <%s>",
		    scslmpset, pool_strerror(pool_error()));
		sc_syslog_msg_done(&sys_handle);
		pool_value_free(pool_value);
		return (SCSLM_ERROR);
	}
	if (pool_conf_open(pool_conf, static_conf, PO_RDWR) < 0) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM <%s> "
		    "pool_conf_open <%s> error <%s>",
		    scslmpset, static_conf, pool_strerror(pool_error()));
		sc_syslog_msg_done(&sys_handle);
		pool_conf_free(pool_conf);
		pool_value_free(pool_value);
		return (SCSLM_ERROR);
	}
	resource = pool_get_resource(pool_conf, "pset", scslmpset);
	if (resource != NULL) {
		if (pool_conf_close(pool_conf) < 0) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM <%s> "
			    "pool_conf_close error <%s>",
			    scslmpset, pool_strerror(pool_error()));
			sc_syslog_msg_done(&sys_handle);
		}
		pool_conf_free(pool_conf);
		pool_value_free(pool_value);
		return (SCSLM_ERROR_EXISTING_PSET);
	}
	resource = pool_resource_create(pool_conf, "pset", scslmpset);
	if (resource == NULL) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Should never occur.
		 * @user_action
		 * Contact your authorized Sun service provider to determine
		 * whether a workaround or patch is available.
		 */
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM <%s> "
		    "pool_resource_create error <%s>",
		    scslmpset, pool_strerror(pool_error()));
		sc_syslog_msg_done(&sys_handle);
		if (pool_conf_close(pool_conf) < 0) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM <%s> "
			    "pool_conf_close error <%s>",
			    scslmpset, pool_strerror(pool_error()));
			sc_syslog_msg_done(&sys_handle);
		}
		pool_conf_free(pool_conf);
		pool_value_free(pool_value);
		return (SCSLM_ERROR);
	}
	pool_elem = pool_resource_to_elem(pool_conf, resource);
	if (pool_elem == NULL) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Should never occur.
		 * @user_action
		 * Contact your authorized Sun service provider to determine
		 * whether a workaround or patch is available.
		 */
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM <%s> "
		    "pool_resource_to_elem error <%s>",
		    scslmpset, pool_strerror(pool_error()));
		sc_syslog_msg_done(&sys_handle);
		if (pool_conf_close(pool_conf) < 0) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM <%s> "
			    "pool_conf_close error <%s>",
			    scslmpset, pool_strerror(pool_error()));
			sc_syslog_msg_done(&sys_handle);
		}
		pool_conf_free(pool_conf);
		pool_value_free(pool_value);
		return (SCSLM_ERROR);
	}
	pool_value_set_uint64(pool_value, pmax);
	if (pool_put_property(pool_conf, pool_elem,
	    (const char *)"pset.max", pool_value) < 0) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM <%s> "
		    "pool_put_property <%s> error <%s>",
		    scslmpset, "pset.max", pool_strerror(pool_error()));
		sc_syslog_msg_done(&sys_handle);
		if (pool_conf_close(pool_conf) < 0) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM <%s> "
			    "pool_conf_close error <%s>",
			    scslmpset, pool_strerror(pool_error()));
			sc_syslog_msg_done(&sys_handle);
		}
		pool_conf_free(pool_conf);
		pool_value_free(pool_value);
		return (SCSLM_ERROR);
	}
	pool_value_set_uint64(pool_value, pmin);
	if (pool_put_property(pool_conf, pool_elem,
	    (const char *)"pset.min", pool_value) < 0) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM <%s> "
		    "pool_put_property <%s> error <%s>",
		    scslmpset, "pset.min", pool_strerror(pool_error()));
		sc_syslog_msg_done(&sys_handle);
		if (pool_conf_close(pool_conf) < 0) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM <%s> "
			    "pool_conf_close error <%s>",
			    scslmpset, pool_strerror(pool_error()));
			sc_syslog_msg_done(&sys_handle);
		}
		pool_conf_free(pool_conf);
		pool_value_free(pool_value);
		return (SCSLM_ERROR);
	}
	if (pool_value_set_string(pool_value, SCSLM_GENERATED) < 0) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM <%s> "
		    "pool_value_set_string error <%s>",
		    scslmpset, pool_strerror(pool_error()));
		sc_syslog_msg_done(&sys_handle);
		if (pool_conf_close(pool_conf) < 0) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM <%s> "
			    "pool_conf_close error <%s>",
			    scslmpset, pool_strerror(pool_error()));
			sc_syslog_msg_done(&sys_handle);
		}
		pool_conf_free(pool_conf);
		pool_value_free(pool_value);
		return (SCSLM_ERROR);
	}
	if (pool_put_property(pool_conf, pool_elem,
	    (const char *)"pset.comment", pool_value) < 0) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM <%s> "
		    "pool_put_property <%s> error <%s>",
		    scslmpset, "pset.comment", pool_strerror(pool_error()));
		sc_syslog_msg_done(&sys_handle);
		if (pool_conf_close(pool_conf) < 0) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM <%s> "
			    "pool_conf_close error <%s>",
			    scslmpset, pool_strerror(pool_error()));
			sc_syslog_msg_done(&sys_handle);
		}
		pool_conf_free(pool_conf);
		pool_value_free(pool_value);
		return (SCSLM_ERROR);
	}
	/* Commit in the dynamic configuration */
	if (pool_conf_commit(pool_conf, PO_TRUE) < 0) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM <%s> "
		    "pool_conf_commit dynamic error <%s>",
		    scslmpset, pool_strerror(pool_error()));
		sc_syslog_msg_done(&sys_handle);
		if (pool_conf_close(pool_conf) < 0) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM <%s> "
			    "pool_conf_close error <%s>",
			    scslmpset, pool_strerror(pool_error()));
			sc_syslog_msg_done(&sys_handle);
		}
		pool_conf_free(pool_conf);
		pool_value_free(pool_value);
		return (SCSLM_ERROR);
	} else {
		/* Commit in the static configuration */
		if (pool_conf_commit(pool_conf, PO_FALSE) < 0) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM <%s> "
			    "pool_conf_commit static error <%s>",
			    scslmpset, pool_strerror(pool_error()));
			sc_syslog_msg_done(&sys_handle);
			if (pool_resource_destroy(pool_conf, resource) < 0) {
				(void) sc_syslog_msg_initialize(&sys_handle,
				    SC_SYSLOG_RGM_FED_TAG, "");
				/*
				 * SCMSGS
				 * @explanation
				 * Should never occur.
				 * @user_action
				 * Contact your authorized Sun service
				 * provider to determine whether a workaround
				 * or patch is available.
				 */
				(void) sc_syslog_msg_log(sys_handle,
				    LOG_ERR, MESSAGE,
				    "SCSLM <%s> "
				    "pool_resource_destroy error <%s>",
				    scslmpset, pool_strerror(pool_error()));
				sc_syslog_msg_done(&sys_handle);
				if (pool_conf_close(pool_conf) < 0) {
					(void) sc_syslog_msg_initialize(
					    &sys_handle,
					    SC_SYSLOG_RGM_FED_TAG, "");
					(void) sc_syslog_msg_log(sys_handle,
					    LOG_ERR, MESSAGE,
					    "SCSLM <%s> "
					    "pool_conf_close error <%s>",
					    scslmpset,
					    pool_strerror(pool_error()));
					sc_syslog_msg_done(&sys_handle);
				}
				pool_conf_free(pool_conf);
				pool_value_free(pool_value);
				return (SCSLM_ERROR);
			}
			if (pool_conf_commit(pool_conf, PO_TRUE) < 0) {
				(void) sc_syslog_msg_initialize(
				    &sys_handle, SC_SYSLOG_RGM_FED_TAG, "");
				(void) sc_syslog_msg_log(sys_handle,
				    LOG_ERR, MESSAGE,
				    "SCSLM <%s> "
				    "pool_conf_commit dynamic error <%s>",
				    scslmpset, pool_strerror(pool_error()));
				sc_syslog_msg_done(&sys_handle);
				if (pool_conf_close(pool_conf) < 0) {
					(void) sc_syslog_msg_initialize(
					    &sys_handle,
					    SC_SYSLOG_RGM_FED_TAG, "");
					(void) sc_syslog_msg_log(sys_handle,
					    LOG_ERR, MESSAGE,
					    "SCSLM <%s> "
					    "pool_conf_close error <%s>",
					    scslmpset,
					    pool_strerror(pool_error()));
					sc_syslog_msg_done(&sys_handle);
				}
				pool_conf_free(pool_conf);
				pool_value_free(pool_value);
				return (SCSLM_ERROR);
			}
			pool_conf_free(pool_conf);
			pool_value_free(pool_value);
			return (SCSLM_ERROR);
		}
	}
	if (pool_conf_close(pool_conf) < 0) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM <%s> "
		    "pool_conf_close error <%s>",
		    scslmpset, pool_strerror(pool_error()));
		sc_syslog_msg_done(&sys_handle);
		return (SCSLM_ERROR);
	}
	pool_conf_free(pool_conf);
	pool_value_free(pool_value);
	return (SCSLM_SUCCESS);
}


/*
 * Function SCSLM_delete_pset
 * Delete a Solaris DRP pset.
 * Args In:
 * 	a pset name
 * Args Out:
 *	none
 * Return value:
 *	SCSLM_SUCCESS: pset deleted
 *	SCSLM_ERROR_UNKNOWN_PSET: pset does not exist
 *	SCSLM_ERROR: other errors
 */
static int
SCSLM_delete_pset(char *pset)
{
	sc_syslog_msg_handle_t	sys_handle;
	int		err;
	int		ret;
	pool_conf_t	*pool_conf;
	pool_resource_t	*resource;
	const char	*static_conf;
	char		scslmpset[MAX_COMMAND_SIZE];

	ret = snprintf(scslmpset, MAX_COMMAND_SIZE, "%s%s", SCSLM_PSET, pset);
	err = errno; /*lint !e746 */
	if (ret >= MAX_COMMAND_SIZE || ret < 0) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM snprintf error <%s>", strerror(err));
		sc_syslog_msg_done(&sys_handle);
		return (SCSLM_ERROR);
	}
	static_conf = pool_static_location();
	pool_conf = pool_conf_alloc();
	if (pool_conf == NULL) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM <%s> "
		    "pool_conf_alloc error <%s>",
		    scslmpset, pool_strerror(pool_error()));
		sc_syslog_msg_done(&sys_handle);
		return (SCSLM_ERROR);
	}
	if (pool_conf_open(pool_conf, static_conf, PO_RDWR) < 0) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM <%s> "
		    "pool_conf_open <%s> error <%s>",
		    scslmpset, static_conf, pool_strerror(pool_error()));
		sc_syslog_msg_done(&sys_handle);
		pool_conf_free(pool_conf);
		return (SCSLM_ERROR);
	}
	resource = pool_get_resource(pool_conf, "pset", scslmpset);
	if (resource == NULL) {
		if (pool_conf_close(pool_conf) < 0) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM <%s> "
			    "pool_conf_close error <%s>",
			    scslmpset, pool_strerror(pool_error()));
			sc_syslog_msg_done(&sys_handle);
		}
		pool_conf_free(pool_conf);
		return (SCSLM_ERROR_UNKNOWN_PSET);
	}
	if (pool_resource_destroy(pool_conf, resource) < 0) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM <%s> "
		    "pool_resource_destroy error <%s>",
		    scslmpset, pool_strerror(pool_error()));
		sc_syslog_msg_done(&sys_handle);
		if (pool_conf_close(pool_conf) < 0) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM <%s> "
			    "pool_conf_close error <%s>",
			    scslmpset, pool_strerror(pool_error()));
			sc_syslog_msg_done(&sys_handle);
		}
		pool_conf_free(pool_conf);
		return (SCSLM_ERROR);
	}
	/* Commit in the dynamic configuration */
	if (pool_conf_commit(pool_conf, PO_TRUE) < 0) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM <%s> "
		    "pool_conf_commit dynamic error <%s>",
		    scslmpset, pool_strerror(pool_error()));
		sc_syslog_msg_done(&sys_handle);
		if (pool_conf_close(pool_conf) < 0) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM <%s> "
			    "pool_conf_close error <%s>",
			    scslmpset, pool_strerror(pool_error()));
			sc_syslog_msg_done(&sys_handle);
		}
		pool_conf_free(pool_conf);
		return (SCSLM_ERROR);
	} else {
		/* Commit in the static configuration */
		if (pool_conf_commit(pool_conf, PO_FALSE) < 0) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM <%s> "
			    "pool_conf_commit static error <%s>",
			    scslmpset, pool_strerror(pool_error()));
			sc_syslog_msg_done(&sys_handle);
			if (pool_conf_close(pool_conf) < 0) {
				(void) sc_syslog_msg_initialize(&sys_handle,
				    SC_SYSLOG_RGM_FED_TAG, "");
				(void) sc_syslog_msg_log(sys_handle,
				    LOG_ERR, MESSAGE,
				    "SCSLM <%s> "
				    "pool_conf_close error <%s>",
				    scslmpset, pool_strerror(pool_error()));
				sc_syslog_msg_done(&sys_handle);
			}
			pool_conf_free(pool_conf);
			return (SCSLM_ERROR);
		}
	}
	if (pool_conf_close(pool_conf) < 0) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM <%s> "
		    "pool_conf_close error <%s>",
		    scslmpset, pool_strerror(pool_error()));
		sc_syslog_msg_done(&sys_handle);
		return (SCSLM_ERROR);
	}
	pool_conf_free(pool_conf);
	return (SCSLM_SUCCESS);
}


/*
 * Function SCSLM_modify_pset
 * Modify cpu min/max of Solaris DRP pset.
 * Args In:
 * 	a pset name
 *	min pset size
 *	max pset size
 *	display dynamic pool error or not
 * Args Out:
 *	none
 * Return value:
 *	SCSLM_SUCCESS: pset modified
 *	SCSLM_ERROR_UNKNOWN_PSET: pset not found
 *	SCSLM_ERROR: other errors
 */
static int
SCSLM_modify_pset(char *pset, uint64_t pmin, uint64_t pmax, int show_error)
{
	sc_syslog_msg_handle_t	sys_handle;
	int		err;
	int		ret;
	pool_conf_t	*pool_conf;
	pool_value_t	*pool_value;
	pool_elem_t	*pool_elem;
	pool_resource_t	*resource;
	const char	*static_conf;
	uint64_t	cur_pmin;
	uint64_t	cur_pmax;
	int		dontchangemax = 0;
	char		scslmpset[MAX_COMMAND_SIZE];

	if (strcmp(pset, "pset_default") == 0) {
		ret = snprintf(scslmpset, MAX_COMMAND_SIZE, "%s", pset);
		dontchangemax = 1;
	} else {
		ret = snprintf(scslmpset, MAX_COMMAND_SIZE, "%s%s",
		    SCSLM_PSET, pset);
	}
	err = errno; /*lint !e746 */
	if (ret >= MAX_COMMAND_SIZE || ret < 0) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM snprintf error <%s>", strerror(err));
		sc_syslog_msg_done(&sys_handle);
		return (SCSLM_ERROR);
	}
	if (pmin == 0) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Should never occur.
		 * @user_action
		 * Contact your authorized Sun service provider to determine
		 * whether a workaround or patch is available.
		 */
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM <%s> "
		    "error pmin zero", scslmpset);
		sc_syslog_msg_done(&sys_handle);
		return (SCSLM_ERROR);
	}
	if (dontchangemax == 0 && pmax == 0) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Should never occur.
		 * @user_action
		 * Contact your authorized Sun service provider to determine
		 * whether a workaround or patch is available.
		 */
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM <%s> "
		    "error pmax zero", scslmpset);
		sc_syslog_msg_done(&sys_handle);
		return (SCSLM_ERROR);
	}
	if (dontchangemax == 0 && (pmin > pmax)) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Should never occur.
		 * @user_action
		 * Contact your authorized Sun service provider to determine
		 * whether a workaround or patch is available.
		 */
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM <%s> "
		    "error pmin > pmax", scslmpset);
		sc_syslog_msg_done(&sys_handle);
		return (SCSLM_ERROR);
	}
	pool_value = pool_value_alloc();
	if (pool_value == NULL) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM <%s> "
		    "pool_value_alloc error <%s>",
		    scslmpset, pool_strerror(pool_error()));
		sc_syslog_msg_done(&sys_handle);
		return (SCSLM_ERROR);
	}
	static_conf = pool_static_location();
	pool_conf = pool_conf_alloc();
	if (pool_conf == NULL) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM <%s> "
		    "pool_conf_alloc error <%s>",
		    scslmpset, pool_strerror(pool_error()));
		sc_syslog_msg_done(&sys_handle);
		pool_value_free(pool_value);
		return (SCSLM_ERROR);
	}
	if (pool_conf_open(pool_conf, static_conf, PO_RDWR) < 0) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM <%s> "
		    "pool_conf_open <%s> error <%s>",
		    scslmpset, static_conf, pool_strerror(pool_error()));
		sc_syslog_msg_done(&sys_handle);
		pool_conf_free(pool_conf);
		pool_value_free(pool_value);
		return (SCSLM_ERROR);
	}
	resource = pool_get_resource(pool_conf, "pset", scslmpset);
	if (resource == NULL) {
		if (pool_conf_close(pool_conf) < 0) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM <%s> "
			    "pool_conf_close error <%s>",
			    scslmpset, pool_strerror(pool_error()));
			sc_syslog_msg_done(&sys_handle);
		}
		pool_conf_free(pool_conf);
		pool_value_free(pool_value);
		return (SCSLM_ERROR_UNKNOWN_PSET);
	}
	pool_elem = pool_resource_to_elem(pool_conf, resource);
	if (pool_elem == NULL) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM <%s> "
		    "pool_resource_to_elem error <%s>",
		    scslmpset, pool_strerror(pool_error()));
		sc_syslog_msg_done(&sys_handle);
		if (pool_conf_close(pool_conf) < 0) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM <%s> "
			    "pool_conf_close error <%s>",
			    scslmpset, pool_strerror(pool_error()));
			sc_syslog_msg_done(&sys_handle);
		}
		pool_conf_free(pool_conf);
		pool_value_free(pool_value);
		return (SCSLM_ERROR);
	}
	/* Read current pmin */
	if (pool_get_property(pool_conf, pool_elem,
	    (const char *)"pset.min", pool_value) < 0) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Should never occur.
		 * @user_action
		 * Contact your authorized Sun service provider to determine
		 * whether a workaround or patch is available.
		 */
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM <%s> "
		    "pool_get_property <%s> error <%s>",
		    scslmpset, "pset.min", pool_strerror(pool_error()));
		sc_syslog_msg_done(&sys_handle);
		if (pool_conf_close(pool_conf) < 0) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM <%s> "
			    "pool_conf_close error <%s>",
			    scslmpset, pool_strerror(pool_error()));
			sc_syslog_msg_done(&sys_handle);
		}
		pool_conf_free(pool_conf);
		pool_value_free(pool_value);
		return (SCSLM_ERROR);
	}
	if (pool_value_get_uint64(pool_value, &cur_pmin) < 0) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Should never occur.
		 * @user_action
		 * Contact your authorized Sun service provider to determine
		 * whether a workaround or patch is available.
		 */
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM <%s> "
		    "pool_value_get_uint64 error <%s>",
		    scslmpset, pool_strerror(pool_error()));
		sc_syslog_msg_done(&sys_handle);
		if (pool_conf_close(pool_conf) < 0) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM <%s> "
			    "pool_conf_close error <%s>",
			    scslmpset, pool_strerror(pool_error()));
			sc_syslog_msg_done(&sys_handle);
		}
		pool_conf_free(pool_conf);
		pool_value_free(pool_value);
		return (SCSLM_ERROR);
	}

	/* Read current pmax */
	if (pool_get_property(pool_conf, pool_elem,
	    (const char *)"pset.max", pool_value) < 0) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM <%s> "
		    "pool_get_property <%s> error <%s>",
		    scslmpset, "pset.max", pool_strerror(pool_error()));
		sc_syslog_msg_done(&sys_handle);
		if (pool_conf_close(pool_conf) < 0) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM <%s> "
			    "pool_conf_close error <%s>",
			    scslmpset, pool_strerror(pool_error()));
			sc_syslog_msg_done(&sys_handle);
		}
		pool_conf_free(pool_conf);
		pool_value_free(pool_value);
		return (SCSLM_ERROR);
	}
	if (pool_value_get_uint64(pool_value, &cur_pmax) < 0) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM <%s> "
		    "pool_value_get_uint64 error <%s>",
		    scslmpset, pool_strerror(pool_error()));
		sc_syslog_msg_done(&sys_handle);
		if (pool_conf_close(pool_conf) < 0) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM <%s> "
			    "pool_conf_close error <%s>",
			    scslmpset, pool_strerror(pool_error()));
			sc_syslog_msg_done(&sys_handle);
		}
		pool_conf_free(pool_conf);
		pool_value_free(pool_value);
		return (SCSLM_ERROR);
	}

	if (dontchangemax != 0) {
		pmax = cur_pmax;
	}

	if (pmin == cur_pmin && pmax == cur_pmax) {
		/* There is nothing to do in this case */
		if (pool_conf_close(pool_conf) < 0) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * Should never occur.
			 * @user_action
			 * Contact your authorized Sun service provider to
			 * determine whether a workaround or patch is
			 * available.
			 */
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM <%s> "
			    "pool_conf_close() error <%s>",
			    scslmpset, pool_strerror(pool_error()));
			sc_syslog_msg_done(&sys_handle);
		}
		pool_conf_free(pool_conf);
		pool_value_free(pool_value);
		return (SCSLM_SUCCESS);
	}

	if (pmin < cur_pmin) {
		/* pmin */
		pool_value_set_uint64(pool_value, pmin);
		if (pool_put_property(pool_conf, pool_elem,
		    (const char *)"pset.min", pool_value) < 0) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM <%s> "
			    "pool_put_property <%s> error <%s>",
			    scslmpset, "pset.min", pool_strerror(pool_error()));
			sc_syslog_msg_done(&sys_handle);
			if (pool_conf_close(pool_conf) < 0) {
				(void) sc_syslog_msg_initialize(&sys_handle,
				    SC_SYSLOG_RGM_FED_TAG, "");
				(void) sc_syslog_msg_log(sys_handle,
				    LOG_ERR, MESSAGE,
				    "SCSLM <%s> "
				    "pool_conf_close error <%s>",
				    scslmpset, pool_strerror(pool_error()));
				sc_syslog_msg_done(&sys_handle);
			}
			pool_conf_free(pool_conf);
			pool_value_free(pool_value);
			return (SCSLM_ERROR);
		}
		/* pmax */
		pool_value_set_uint64(pool_value, pmax);
		if (pool_put_property(pool_conf, pool_elem,
		    (const char *)"pset.max", pool_value) < 0) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM <%s> "
			    "pool_put_property <%s> error <%s>",
			    scslmpset, "pset.max", pool_strerror(pool_error()));
			sc_syslog_msg_done(&sys_handle);
			if (pool_conf_close(pool_conf) < 0) {
				(void) sc_syslog_msg_initialize(&sys_handle,
				    SC_SYSLOG_RGM_FED_TAG, "");
				(void) sc_syslog_msg_log(sys_handle,
				    LOG_ERR, MESSAGE,
				    "SCSLM <%s> "
				    "pool_conf_close error <%s>",
				    scslmpset, pool_strerror(pool_error()));
				sc_syslog_msg_done(&sys_handle);
			}
			pool_conf_free(pool_conf);
			pool_value_free(pool_value);
			return (SCSLM_ERROR);
		}
	} else {
		/* pmax */
		pool_value_set_uint64(pool_value, pmax);
		if (pool_put_property(pool_conf, pool_elem,
		    (const char *)"pset.max", pool_value) < 0) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM <%s> "
			    "pool_put_property <%s> error <%s>",
			    scslmpset, "pset.max", pool_strerror(pool_error()));
			sc_syslog_msg_done(&sys_handle);
			if (pool_conf_close(pool_conf) < 0) {
				(void) sc_syslog_msg_initialize(&sys_handle,
				    SC_SYSLOG_RGM_FED_TAG, "");
				(void) sc_syslog_msg_log(sys_handle,
				    LOG_ERR, MESSAGE,
				    "SCSLM <%s> "
				    "pool_conf_close error <%s>",
				    scslmpset, pool_strerror(pool_error()));
				sc_syslog_msg_done(&sys_handle);
			}
			pool_conf_free(pool_conf);
			pool_value_free(pool_value);
			return (SCSLM_ERROR);
		}
		/* pmin */
		pool_value_set_uint64(pool_value, pmin);
		if (pool_put_property(pool_conf, pool_elem,
		    (const char *)"pset.min", pool_value) < 0) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM <%s> "
			    "pool_put_property <%s> error <%s>",
			    scslmpset, "pset.min", pool_strerror(pool_error()));
			sc_syslog_msg_done(&sys_handle);
			if (pool_conf_close(pool_conf) < 0) {
				(void) sc_syslog_msg_initialize(&sys_handle,
				    SC_SYSLOG_RGM_FED_TAG, "");
				(void) sc_syslog_msg_log(sys_handle,
				    LOG_ERR, MESSAGE,
				    "SCSLM <%s> "
				    "pool_conf_close error <%s>",
				    scslmpset, pool_strerror(pool_error()));
				sc_syslog_msg_done(&sys_handle);
			}
			pool_conf_free(pool_conf);
			pool_value_free(pool_value);
			return (SCSLM_ERROR);
		}
	}
	/* Commit in the dynamic configuration */
	if (pool_conf_commit(pool_conf, PO_TRUE) < 0) {
		if (show_error != 0) {
			(void) sc_syslog_msg_initialize(
			    &sys_handle, SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM <%s> "
			    "pool_conf_commit dynamic error <%s>",
			    scslmpset, pool_strerror(pool_error()));
			sc_syslog_msg_done(&sys_handle);
		}
		if (pool_conf_close(pool_conf) < 0) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM <%s> "
			    "pool_conf_close error <%s>",
			    scslmpset, pool_strerror(pool_error()));
			sc_syslog_msg_done(&sys_handle);
		}
		pool_conf_free(pool_conf);
		pool_value_free(pool_value);
		return (SCSLM_ERROR);
	} else {
		/* Commit in the static configuration */
		if (pool_conf_commit(pool_conf, PO_FALSE) < 0) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM <%s> "
			    "pool_conf_commit static error <%s>",
			    scslmpset, pool_strerror(pool_error()));
			sc_syslog_msg_done(&sys_handle);
			if (pool_conf_close(pool_conf) < 0) {
				(void) sc_syslog_msg_initialize(&sys_handle,
				    SC_SYSLOG_RGM_FED_TAG, "");
				(void) sc_syslog_msg_log(sys_handle,
				    LOG_ERR, MESSAGE,
				    "SCSLM <%s> "
				    "pool_conf_close error <%s>",
				    scslmpset, pool_strerror(pool_error()));
				sc_syslog_msg_done(&sys_handle);
			}
			pool_conf_free(pool_conf);
			pool_value_free(pool_value);
			return (SCSLM_ERROR);
		}
	}
	if (pool_conf_close(pool_conf) < 0) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM <%s> "
		    "pool_conf_close error <%s>",
		    scslmpset, pool_strerror(pool_error()));
		sc_syslog_msg_done(&sys_handle);
		return (SCSLM_ERROR);
	}
	pool_conf_free(pool_conf);
	pool_value_free(pool_value);
	return (SCSLM_SUCCESS);
}


/*
 * Function SCSLM_modify_pool_pset
 * Associated a Solaris DRP pool to a DRP pset.
 * Args In:
 * 	a pool name
 *	a pset name
 * Args Out:
 *	none
 * Return value:
 *	SCSLM_SUCCESS: pool modified
 *	SCSLM_ERROR_UNKNOWN_PSET: pset not found
 *	SCSLM_ERROR_UNKNOWN_POOL: pool not found
 *	SCSLM_ERROR: other errors
 */
static int
SCSLM_modify_pool_pset(char *pool, char *pset)
{
	sc_syslog_msg_handle_t	sys_handle;
	int		err;
	int		ret;
	pool_conf_t	*pool_conf;
	pool_resource_t	*resource;
	pool_t		*mpool;
	const char	*static_conf;
	char		scslmpool[MAX_COMMAND_SIZE];
	char		scslmpset[MAX_COMMAND_SIZE];

	ret = snprintf(scslmpool, MAX_COMMAND_SIZE, "%s%s", SCSLM_POOL, pool);
	err = errno; /*lint !e746 */
	if (ret >= MAX_COMMAND_SIZE || ret < 0) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM snprintf error <%s>", strerror(err));
		sc_syslog_msg_done(&sys_handle);
		return (SCSLM_ERROR);
	}
	ret = snprintf(scslmpset, MAX_COMMAND_SIZE, "%s%s", SCSLM_PSET, pset);
	err = errno; /*lint !e746 */
	if (ret >= MAX_COMMAND_SIZE || ret < 0) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM snprintf error <%s>", strerror(err));
		sc_syslog_msg_done(&sys_handle);
		return (SCSLM_ERROR);
	}
	static_conf = pool_static_location();
	pool_conf = pool_conf_alloc();
	static_conf = pool_static_location();
	pool_conf = pool_conf_alloc();
	if (pool_conf == NULL) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM <%s> "
		    "pool_conf_alloc error <%s>",
		    scslmpool, pool_strerror(pool_error()));
		sc_syslog_msg_done(&sys_handle);
		return (SCSLM_ERROR);
	}
	if (pool_conf_open(pool_conf, static_conf, PO_RDWR) < 0) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM <%s> "
		    "pool_conf_open <%s> error <%s>",
		    scslmpool, static_conf, pool_strerror(pool_error()));
		sc_syslog_msg_done(&sys_handle);
		pool_conf_free(pool_conf);
		return (SCSLM_ERROR);
	}
	mpool = pool_get_pool(pool_conf,  scslmpool);
	if (mpool == NULL) {
		if (pool_conf_close(pool_conf) < 0) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM <%s> "
			    "pool_conf_close error <%s>",
			    scslmpool, pool_strerror(pool_error()));
			sc_syslog_msg_done(&sys_handle);
		}
		pool_conf_free(pool_conf);
		return (SCSLM_ERROR_UNKNOWN_POOL);
	}
	resource = pool_get_resource(pool_conf, "pset", scslmpset);
	if (resource == NULL) {
		if (pool_conf_close(pool_conf) < 0) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM <%s> "
			    "pool_conf_close error <%s>",
			    scslmpool, pool_strerror(pool_error()));
			sc_syslog_msg_done(&sys_handle);
		}
		pool_conf_free(pool_conf);
		return (SCSLM_ERROR_UNKNOWN_PSET);
	}
	if (pool_associate(pool_conf, mpool, resource) < 0) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Should never occur.
		 * @user_action
		 * Contact your authorized Sun service provider to determine
		 * whether a workaround or patch is available.
		 */
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM <%s> "
		    "pool_associate error <%s>",
		    scslmpool, pool_strerror(pool_error()));
		sc_syslog_msg_done(&sys_handle);
		if (pool_conf_close(pool_conf) < 0) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM <%s> "
			    "pool_conf_close error <%s>",
			    scslmpool, pool_strerror(pool_error()));
			sc_syslog_msg_done(&sys_handle);
		}
		pool_conf_free(pool_conf);
		return (SCSLM_ERROR);
	}
	/* Commit in the dynamic configuration */
	if (pool_conf_commit(pool_conf, PO_TRUE) < 0) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM <%s> "
		    "pool_conf_commit dynamic error <%s>",
		    scslmpool, pool_strerror(pool_error()));
		sc_syslog_msg_done(&sys_handle);
		if (pool_conf_close(pool_conf) < 0) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM <%s> "
			    "pool_conf_close error <%s>",
			    scslmpool, pool_strerror(pool_error()));
			sc_syslog_msg_done(&sys_handle);
		}
		pool_conf_free(pool_conf);
		return (SCSLM_ERROR);
	} else {
		/* Commit in the static configuration */
		if (pool_conf_commit(pool_conf, PO_FALSE) < 0) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM <%s> "
			    "pool_conf_commit static error <%s>",
			    scslmpool, pool_strerror(pool_error()));
			sc_syslog_msg_done(&sys_handle);
			if (pool_conf_close(pool_conf) < 0) {
				(void) sc_syslog_msg_initialize(&sys_handle,
				    SC_SYSLOG_RGM_FED_TAG, "");
				(void) sc_syslog_msg_log(sys_handle,
				    LOG_ERR, MESSAGE,
				    "SCSLM <%s> "
				    "pool_conf_close error <%s>",
				    scslmpool, pool_strerror(pool_error()));
				sc_syslog_msg_done(&sys_handle);
			}
			pool_conf_free(pool_conf);
			return (SCSLM_ERROR);
		}
	}
	if (pool_conf_close(pool_conf) < 0) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM <%s> "
		    "pool_conf_close error <%s>",
		    scslmpool, pool_strerror(pool_error()));
		sc_syslog_msg_done(&sys_handle);
		return (SCSLM_ERROR);
	}
	pool_conf_free(pool_conf);
	return (SCSLM_SUCCESS);
}


/*
 * Function SCSLM_zone_pool
 * Get a non-global zone zonecfg DRP pool.
 * Args In:
 * 	a zone name
 * Args Out:
 *	pool name bind to the zone, if any
 * Return value:
 *	SCSLM_SUCCESS: name of the pool returned
 * 	SCSLM_ERROR_UNKNOWN_ZONE: zone does not exist
 * 	SCSLM_ERROR_UNKNOWN_POOL: there is no pool binding
 *	SCSLM_ERROR: other errors
 */
static int
SCSLM_zone_pool(char *zone, char **pool)
{
	sc_syslog_msg_handle_t	sys_handle;
	int	err;
	int	ret;
	int	cmd_ret;
	char	*cmd_output;
	char	*tmp;
	char	cmd[MAX_COMMAND_SIZE];
	char	serr[MAX_COMMAND_SIZE];

	if (strcmp(zone, GLOBAL_ZONENAME) == 0) {
		return (SCSLM_ERROR);
	}

	ret = snprintf(cmd, MAX_COMMAND_SIZE,
	    "%s -z %s info pool 2>&1", ZONECFG_CMD, zone);
	err = errno; /*lint !e746 */
	if (ret >= MAX_COMMAND_SIZE || ret < 0) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM snprintf error <%s>", strerror(err));
		sc_syslog_msg_done(&sys_handle);
		return (SCSLM_ERROR);
	}
	ret = SCSLM_exec(cmd, &cmd_ret, &cmd_output);
	if (ret != SCSLM_SUCCESS) {
		return (ret);
	}
	if (cmd_ret != 0) {
		if (cmd_output != NULL) {
			ret = snprintf(serr, MAX_COMMAND_SIZE,
			    "%s: No such zone configured", zone);
			err = errno; /*lint !e746 */
			if (ret >= MAX_COMMAND_SIZE || ret < 0) {
				(void) sc_syslog_msg_initialize(&sys_handle,
				    SC_SYSLOG_RGM_FED_TAG, "");
				(void) sc_syslog_msg_log(sys_handle,
				    LOG_ERR, MESSAGE,
				    "SCSLM snprintf error <%s>", strerror(err));
				sc_syslog_msg_done(&sys_handle);
				free(cmd_output);
				return (SCSLM_ERROR);
			}
			if (strstr(cmd_output, serr) != NULL) {
				free(cmd_output);
				return (SCSLM_ERROR_UNKNOWN_ZONE);
			}
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM <%s> ret %d, outputs <%s>",
			    cmd, cmd_ret, cmd_output);
			sc_syslog_msg_done(&sys_handle);
			free(cmd_output);
			return (SCSLM_ERROR);
		} else {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM <%s> ret %d", cmd, cmd_ret);
			sc_syslog_msg_done(&sys_handle);
			return (SCSLM_ERROR);
		}
	} else {
		if (cmd_output != NULL) {
			ret = snprintf(serr, MAX_COMMAND_SIZE,
			    "pool: ");
			err = errno; /*lint !e746 */
			if (ret >= MAX_COMMAND_SIZE || ret < 0) {
				(void) sc_syslog_msg_initialize(&sys_handle,
				    SC_SYSLOG_RGM_FED_TAG, "");
				(void) sc_syslog_msg_log(sys_handle,
				    LOG_ERR, MESSAGE,
				    "SCSLM snprintf error <%s>", strerror(err));
				sc_syslog_msg_done(&sys_handle);
				free(cmd_output);
				return (SCSLM_ERROR);
			}
			tmp = strstr(cmd_output, serr);
			if (tmp != NULL) {
				*pool = tmp + strlen("pool: ");
				tmp = *pool;
				while (*tmp != '\n') {
					tmp++;
				}
				*tmp = '\0';
				*pool = strdup(*pool);
				err = errno; /*lint !e746 */
				if (*pool == NULL) {
					(void) sc_syslog_msg_initialize(
					    &sys_handle,
					    SC_SYSLOG_RGM_FED_TAG, "");
					(void) sc_syslog_msg_log(sys_handle,
					    LOG_ERR, MESSAGE,
					    "SCSLM strdup "
					    "error <%s>", strerror(err));
					sc_syslog_msg_done(&sys_handle);
					free(cmd_output);
					return (SCSLM_ERROR);
				}
			} else {
				free(cmd_output);
				return (SCSLM_ERROR);
			}
			free(cmd_output);
		} else {
			return (SCSLM_ERROR_UNKNOWN_POOL);
		}
	}
	return (SCSLM_SUCCESS);
}


/*
 * Function SCSLM_get_poolid
 *
 * Use libpool to find from a Solaris DRP pool name its DRP poolid.
 *
 * Args In:
 *      a pool name
 * Args Out:
 *	poolid
 * Return value:
 *      SCSLM_SUCCESS: pool_id is <> -1
 *      SCSLM_ERROR: error pool_id is -1
 */
static int
SCSLM_get_poolid(char *pool, int64_t *pool_id)
{
	sc_syslog_msg_handle_t	sys_handle;
	int		err;
	int		ret;
	pool_conf_t	*pool_conf;
	pool_value_t	*pool_value;
	pool_elem_t	*pool_elem;
	pool_t		*mpool;
	const char	*static_conf;
	char		scslmpool[MAX_COMMAND_SIZE];

	*pool_id = -1;
	if (strcmp(pool, "pool_default") == 0) {
		ret = snprintf(scslmpool,
		    MAX_COMMAND_SIZE, "%s", pool);
		err = errno; /*lint !e746 */
		if (ret >= MAX_COMMAND_SIZE || ret < 0) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM snprintf error <%s>", strerror(err));
			sc_syslog_msg_done(&sys_handle);
			return (SCSLM_ERROR);
		}
	} else {
		ret = snprintf(scslmpool,
		    MAX_COMMAND_SIZE, "%s%s", SCSLM_POOL, pool);
		err = errno; /*lint !e746 */
		if (ret >= MAX_COMMAND_SIZE || ret < 0) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM snprintf error <%s>", strerror(err));
			sc_syslog_msg_done(&sys_handle);
			return (SCSLM_ERROR);
		}
	}

	pool_value = pool_value_alloc();
	if (pool_value == NULL) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM <%s> pool_value_alloc error <%s>",
		    scslmpool, pool_strerror(pool_error()));
		sc_syslog_msg_done(&sys_handle);
		return (SCSLM_ERROR);
	}
	static_conf = pool_static_location();
	pool_conf = pool_conf_alloc();
	if (pool_conf == NULL) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM <%s> pool_conf_alloc error <%s>",
		    scslmpool, pool_strerror(pool_error()));
		sc_syslog_msg_done(&sys_handle);
		pool_value_free(pool_value);
		return (SCSLM_ERROR);
	}
	if (pool_conf_open(pool_conf, static_conf, PO_RDWR) < 0) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM <%s> "
		    "pool_conf_open <%s> error <%s>",
		    scslmpool, static_conf, pool_strerror(pool_error()));
		sc_syslog_msg_done(&sys_handle);
		pool_conf_free(pool_conf);
		pool_value_free(pool_value);
		return (SCSLM_ERROR);
	}
	mpool = pool_get_pool(pool_conf, scslmpool);
	if (mpool == NULL) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Should never occur.
		 * @user_action
		 * Contact your authorized Sun service provider to determine
		 * whether a workaround or patch is available.
		 */
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM <%s> "
		    "pool_get_pool error <%s>",
		    scslmpool, pool_strerror(pool_error()));
		sc_syslog_msg_done(&sys_handle);
		if (pool_conf_close(pool_conf) < 0) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM <%s> pool_conf_close error <%s>",
			    scslmpool, pool_strerror(pool_error()));
			sc_syslog_msg_done(&sys_handle);
		}
		pool_conf_free(pool_conf);
		pool_value_free(pool_value);
		return (SCSLM_ERROR);
	}
	pool_elem = pool_to_elem(pool_conf, mpool);
	if (pool_elem == NULL) {
		if (pool_conf_close(pool_conf) < 0) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM <%s> pool_conf_close error <%s>",
			    scslmpool, pool_strerror(pool_error()));
			sc_syslog_msg_done(&sys_handle);
		}
		pool_conf_free(pool_conf);
		pool_value_free(pool_value);
		return (SCSLM_ERROR);
	}

	if (pool_get_property(pool_conf, pool_elem, "pool.sys_id",
	    pool_value) < 0) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM <%s> pool_get_property <%s> error <%s>",
		    scslmpool, "pool.sys_id", pool_strerror(pool_error()));
		sc_syslog_msg_done(&sys_handle);
		if (pool_conf_close(pool_conf) < 0) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM <%s> pool_conf_close error <%s>",
			    scslmpool, pool_strerror(pool_error()));
			sc_syslog_msg_done(&sys_handle);
		}
		pool_conf_free(pool_conf);
		pool_value_free(pool_value);
		return (SCSLM_ERROR);
	}

	if (pool_value_get_int64(pool_value, pool_id) < 0) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Should never occur.
		 * @user_action
		 * Contact your authorized Sun service provider to determine
		 * whether a workaround or patch is available.
		 */
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM <%s> pool_value_get_int64 error <%s>",
		    scslmpool, pool_strerror(pool_error()));
		sc_syslog_msg_done(&sys_handle);
		if (pool_conf_close(pool_conf) < 0) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM <%s> pool_conf_close error <%s>",
			    scslmpool, pool_strerror(pool_error()));
			sc_syslog_msg_done(&sys_handle);
		}
		pool_conf_free(pool_conf);
		pool_value_free(pool_value);
		return (SCSLM_ERROR);
	}

	if (pool_conf_close(pool_conf) < 0) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM <%s> pool_conf_close error <%s>",
		    scslmpool, pool_strerror(pool_error()));
		sc_syslog_msg_done(&sys_handle);
	}
	pool_conf_free(pool_conf);
	pool_value_free(pool_value);

	return (SCSLM_SUCCESS);
}


/*
 * Function SCSLM_modify_zone_shares
 * Modify in memory a zone zone.cpu-shares value (prctl command).
 * Args In:
 * 	a zone name
 *	a share number
 * Args Out:
 *	none
 * Return value:
 *	SCSLM_SUCCESS: zone cpu-shares modified
 *	SCSLM_ERROR_UNKNOWN_ZONE: zone not found
 *	SCSLM_ERROR: other errors
 */
static int
SCSLM_modify_zone_shares(char *zone, unsigned int *shares)
{
	sc_syslog_msg_handle_t	sys_handle;
	int		err;
	int		ret;
	int		cmd_ret;
	char		*cmd_output;
	char		cmd[MAX_COMMAND_SIZE];
	char		serr[MAX_COMMAND_SIZE];

	if ((strcmp(zone, GLOBAL_ZONENAME) != 0) && (*shares == 0)) {
		/*
		 * no more RGs under SC SLM control running in this zone.
		 * Read Non-global zone prctl zone.cpu-shares value in zonecfg.
		 * If none, zone.cpu-shares is by default 1.
		 */
		*shares = 1;
		ret = snprintf(cmd, MAX_COMMAND_SIZE,
		    "%s -z %s info rctl name=zone.cpu-shares 2>&1",
		    ZONECFG_CMD, zone);
		err = errno; /*lint !e746 */
		if (ret >= MAX_COMMAND_SIZE || ret < 0) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM snprintf error <%s>", strerror(err));
			sc_syslog_msg_done(&sys_handle);
			return (SCSLM_ERROR);
		}
		ret = SCSLM_exec(cmd, &cmd_ret, &cmd_output);
		if ((ret == SCSLM_SUCCESS) && (cmd_ret != 0)) {
			/* Log the error, but continue */
			if (cmd_output != NULL) {
				(void) sc_syslog_msg_initialize(&sys_handle,
				    SC_SYSLOG_RGM_FED_TAG, "");
				(void) sc_syslog_msg_log(sys_handle,
				    LOG_ERR, MESSAGE,
				    "SCSLM <%s> ret %d, outputs <%s>",
				    cmd, cmd_ret, cmd_output);
				sc_syslog_msg_done(&sys_handle);
				free(cmd_output);
			} else {
				(void) sc_syslog_msg_initialize(&sys_handle,
				    SC_SYSLOG_RGM_FED_TAG, "");
				(void) sc_syslog_msg_log(sys_handle,
				    LOG_ERR, MESSAGE,
				    "SCSLM <%s> ret %d", cmd, cmd_ret);
				sc_syslog_msg_done(&sys_handle);
			}
		}
		if ((ret == SCSLM_SUCCESS) && (cmd_ret == 0) &&
		    (cmd_output != NULL)) { /*lint !e644 */
			/*
			 * Get zone.cpu-shares value from the output
			 * The output string, is for example, for a command
			 * "zonecfg -z z1 info rctl name=zone.cpu-shares"
			 * output:
			 * "rctl:\n"
			 * "name: zone.cpu-shares\n"
			 * "value: (priv=privileged,limit=10,action=none)\n"
			 */
			char *p, *p_end;
			if ((p = strstr(cmd_output, "limit=")) != NULL) {
				p += strlen("limit=");
				if ((p_end = strchr(p, ',')) != NULL) {
					 *p_end = '\0';
					 *shares = (unsigned int)atoi(p);
				}
			}
			free(cmd_output);
		}
	}
	if (strcmp(zone, GLOBAL_ZONENAME) == 0) {
		/*
		 * Global zone, scslm_global_zone_shares is at least 1
		 */
		*shares += scslm_global_zone_shares;
	}

	/* Update through prctl */
	ret = snprintf(cmd, MAX_COMMAND_SIZE,
	    "%s -n zone.cpu-shares -v %u -r -i zone %s 2>&1",
	    PRCTL_CMD, *shares, zone);
	err = errno; /*lint !e746 */
	if (ret >= MAX_COMMAND_SIZE || ret < 0) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM snprintf error <%s>", strerror(err));
		sc_syslog_msg_done(&sys_handle);
		return (SCSLM_ERROR);
	}
	ret = SCSLM_exec(cmd, &cmd_ret, &cmd_output);
	if (ret != SCSLM_SUCCESS) {
		return (ret);
	}
	if (cmd_ret != 0) {
		if (cmd_output != NULL) {
			ret = snprintf(serr,  MAX_COMMAND_SIZE,
			    "%s: unknown zone", zone);
			err = errno; /*lint !e746 */
			if (ret >= MAX_COMMAND_SIZE || ret < 0) {
				(void) sc_syslog_msg_initialize(&sys_handle,
				    SC_SYSLOG_RGM_FED_TAG, "");
				(void) sc_syslog_msg_log(sys_handle,
				    LOG_ERR, MESSAGE,
				    "SCSLM snprintf error <%s>", strerror(err));
				sc_syslog_msg_done(&sys_handle);
				free(cmd_output);
				return (SCSLM_ERROR);
			}
			if (strstr(cmd_output, serr) != NULL) {
				free(cmd_output);
				return (SCSLM_ERROR_UNKNOWN_ZONE);
			}
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM <%s> ret %d, outputs <%s>",
			    cmd, cmd_ret, cmd_output);
			sc_syslog_msg_done(&sys_handle);
			free(cmd_output);
			return (SCSLM_ERROR);
		} else {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM <%s> ret %d", cmd, cmd_ret);
			sc_syslog_msg_done(&sys_handle);
			return (SCSLM_ERROR);
		}
	} else {
		if (cmd_output != NULL) {
			free(cmd_output);
		}
	}
	return (SCSLM_SUCCESS);
}


/*
 * Function SCSLM_pools_ok
 * Verify everything is ok with Solaris DRP (pools enabled).
 * Args In:
 *	None
 * Args Out:
 *	None
 * Return value:
 *	SCSLM_SUCCESS: pools facility ok
 *	SCSLM_NO_POOLS: pools facility setup not done
 *	SCSLM_ERROR: other errors
 */
static int
SCSLM_pools_ok(void)
{
	sc_syslog_msg_handle_t	sys_handle;
	int			status;
	const char		*static_conf;
	pool_conf_t		*pool_conf;

	/* Verify pools facility is enabled */
	if (pool_get_status(&status) != PO_SUCCESS) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Should never occur.
		 * @user_action
		 * Contact your authorized Sun service provider to determine
		 * whether a workaround or patch is available.
		 */
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM <%s> "
		    "pool_get_status error <%s>",
		    SCSLM_POOL, pool_strerror(pool_error()));
		sc_syslog_msg_done(&sys_handle);
		return (SCSLM_ERROR);
	}
	if (status == POOL_DISABLED) {
		return (SCSLM_POOLS_DISABLED);
	}

	static_conf = pool_static_location();
	pool_conf = pool_conf_alloc();
	if (pool_conf == NULL) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM <%s> "
		    "pool_conf_alloc error <%s>",
		    SCSLM_POOL, pool_strerror(pool_error()));
		sc_syslog_msg_done(&sys_handle);
		return (SCSLM_ERROR);
	}
	if (pool_conf_open(pool_conf, static_conf, PO_RDWR) < 0) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM <%s> "
		    "pool_conf_open <%s> error <%s>",
		    "", static_conf, pool_strerror(pool_error()));
		sc_syslog_msg_done(&sys_handle);
		pool_conf_free(pool_conf);
		return (SCSLM_POOLS_NO_CONF);
	}
	if (pool_conf_close(pool_conf) < 0) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM <%s> "
		    "pool_conf_close error <%s>",
		    SCSLM_POOL, pool_strerror(pool_error()));
		sc_syslog_msg_done(&sys_handle);
		pool_conf_free(pool_conf);
		return (SCSLM_ERROR);
	}
	pool_conf_free(pool_conf);
	return (SCSLM_SUCCESS);
}








/*
 * Function SCSLM_get_zone_dynamic_binding
 * Find the zone current dynamic pool binding,
 * that is set with Solaris API pool_set_binding(poolname, P_ZONEID, zoneid)
 * or Solaris command line poolbind.
 * Args In:
 *	a zone name
 * Args Out:
 *	a poolid
 * Return value:
 *	SCSLM_SUCCESS
 *	SCSLM_ERROR
 */
static int
SCSLM_get_zone_dynamic_binding(char *zone, int64_t *poolid)
{
	sc_syslog_msg_handle_t	sys_handle;
	zoneid_t		*zids;
	uint_t			i, nzents;
	int			err;
	char			zonename[ZONENAME_MAX];
	poolid_t		lpoolid;

	*poolid = -1;

	if (zone_list(NULL, &nzents) != 0) {
		err = errno; /*lint !e746 */
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Should never occur.
		 * @user_action
		 * Contact your authorized Sun service provider to determine
		 * whether a workaround or patch is available.
		 */
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM zone_list error <%s>", strerror(err));
		sc_syslog_msg_done(&sys_handle);
		return (SCSLM_ERROR);
	}
	if (nzents == 0) {
		return (SCSLM_ERROR);
	}

	if ((zids = (zoneid_t *)malloc(nzents * sizeof (zoneid_t))) == NULL) {
		err = errno; /*lint !e746 */
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM malloc error <%s>", strerror(err));
		sc_syslog_msg_done(&sys_handle);
		return (SCSLM_ERROR);
	}
	if (zone_list(zids, &nzents) != 0) {
		err = errno; /*lint !e746 */
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "SCSLM zone_list error <%s>", strerror(err));
		sc_syslog_msg_done(&sys_handle);
		free(zids);
		return (SCSLM_ERROR);
	}

	/*
	 * Find the zone dynamic pool binding, using the zone list
	 */
	for (i = 0; i < nzents; i++) {
		if (zone_getattr(zids[i], ZONE_ATTR_NAME, zonename,
		    ZONENAME_MAX) < 0) {
			err = errno; /*lint !e746 */
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * Should never occur.
			 * @user_action
			 * Contact your authorized Sun service provider to
			 * determine whether a workaround or patch is
			 * available.
			 */
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "SCSLM "
			    "zone_getattr errno <%s>", strerror(err));
			sc_syslog_msg_done(&sys_handle);
			free(zids);
			return (SCSLM_ERROR);
		}
		if (strcmp(zonename, zone) == 0) {
			if (zone_getattr(zids[i], ZONE_ATTR_POOLID,
			    &lpoolid, sizeof (lpoolid)) < 0) {
				err = errno; /*lint !e746 */
				(void) sc_syslog_msg_initialize(&sys_handle,
				    SC_SYSLOG_RGM_FED_TAG, "");
				(void) sc_syslog_msg_log(sys_handle,
				    LOG_ERR, MESSAGE,
				    "SCSLM "
				    "zone_getattr errno <%s>", strerror(err));
				sc_syslog_msg_done(&sys_handle);
				free(zids);
				return (SCSLM_ERROR);
			}
			free(zids);
			*poolid = (int64_t)lpoolid;
			return (SCSLM_SUCCESS);
		}
	}
	/* zone not found */
	free(zids);
	return (SCSLM_ERROR);
}
#endif /* SOL_VERSION >= __s10 */
#endif /* SC_SRM */
