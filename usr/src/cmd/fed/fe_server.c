/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

/*
 * Linux FED only:
 *
 * Linux does not support MT RPC services.Hence, we have provided our
 * own MT RPC implementation (see libscutils).
 * There are three public interfaces that each individual rpc server needs to
 * call:
 * - Call rpc_init_rpc_mgr() at the startup to initialize the rpc manager.
 * - Call rpc_req_queue_append() when receives rpc client request.
 * - Call rpc_cleanup_rpc_mgr() to clean up the rpc manager at shut down.
 */

#pragma ident	"@(#)fe_server.c	1.5	09/01/06 SMI"

#include <stdio.h>
#include <stdlib.h>
#include <rpc/pmap_clnt.h>
#include <string.h>
#include <memory.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <rgm/rpc/fe.h>
#include <rgm/rpc_services.h>

#ifndef SIG_PF
#define	SIG_PF void(*)(int)
#endif

#define	FE_OPT_INITSUSPEND (1 << 0)
#define	FE_CAPTURE_OUTPUT (1 << 1)
#define	FE_SYSLOG_OUTPUT (1 << 2)

/* The following rpc.fed flags are not yet implemented in Linux FED: */
#define	FE_PRODUCE_CORE (1 << 3)
#define	FE_ASYNC (1 << 4)
#define	FE_VALIDATE_PROJ_NAME (1 << 5)

void
fe_program_2(struct svc_req *rqstp, register SVCXPRT *transp)
{
	union {
		fe_run_args feproc_run_2_arg;
		fe_args feproc_suspend_2_arg;
		fe_args feproc_resume_2_arg;
	} argument;
	union {
		fe_run_result feproc_run_2_res;
		fe_result feproc_suspend_2_res;
		fe_result feproc_resume_2_res;
	} result;
	bool_t retval;
	xdrproc_t _xdr_argument, _xdr_result;
	bool_t (*local)(char *, void *, struct svc_req *);

	switch (rqstp->rq_proc) {
	case FEPROC_NULL:
		_xdr_argument = (xdrproc_t)xdr_void;
		_xdr_result = (xdrproc_t)xdr_void;
		local = (bool_t (*) (char *, void *, struct svc_req *))
		    feproc_null_2_svc;
		break;

	case FEPROC_RUN:
		_xdr_argument = (xdrproc_t)xdr_fe_run_args;
		_xdr_result = (xdrproc_t)xdr_fe_run_result;
		local = (bool_t (*) (char *, void *, struct svc_req *))
		    feproc_run_2_svc;
		break;

	case FEPROC_SUSPEND:
		_xdr_argument = (xdrproc_t)xdr_fe_args;
		_xdr_result = (xdrproc_t)xdr_fe_result;
		local = (bool_t (*) (char *, void *, struct svc_req *))
		    feproc_suspend_2_svc;
		break;

	case FEPROC_RESUME:
		_xdr_argument = (xdrproc_t)xdr_fe_args;
		_xdr_result = (xdrproc_t)xdr_fe_result;
		local = (bool_t (*) (char *, void *, struct svc_req *))
		    feproc_resume_2_svc;
		break;

	case FEPROC_KILL:
		_xdr_argument = (xdrproc_t)xdr_fe_args;
		_xdr_result = (xdrproc_t)xdr_fe_result;
		local = (bool_t (*) (char *, void *, struct svc_req *))
		    feproc_kill_2_svc;
		break;

	default:
		svcerr_noproc(transp);
		return;
	}

	/*
	 * Append the request at the end of the rpc request queue
	 */
	(void) rpc_req_queue_append(transp, sizeof (argument),
	    _xdr_argument, _xdr_result, sizeof (result), local, rqstp);
}
