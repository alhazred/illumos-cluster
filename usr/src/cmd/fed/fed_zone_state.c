/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)fed_zone_state.c	1.6	08/05/20 SMI"

/* local includes */
#include "fed.h"
/* SC SLM addon start */
#include "fed_slm.h"
/* SC SLM addon end */

/* SC includes */
#include <rgm/sczones.h>
#include <sys/sc_syslog_msg.h>
#include <sys/rsrc_tag.h>

/* standard includes */
#include <stdlib.h>


void
up_callback(const char *zonename)
{
	if (debug) {
		dbg_msgout(NOGET("Received UP callback for zone %s\n"),
		    zonename);
	}
	/* SC SLM addon start */
	(void) scslm_zone_up(zonename);
	/* SC SLM addon end */
}

void
down_callback(const char *zonename)
{
	if (debug) {
		dbg_msgout(NOGET("Received DOWN callback for zone %s\n"),
		    zonename);
	}
	/* SC SLM addon start */
	(void) scslm_zone_down(zonename);
	/* SC SLM addon end */
}

/*
 * initialize_zone_callbacks
 *
 * Makes a call into libsczones to register with the sc_zonesd for
 * zone state change callbacks. The fed cares only about zone up and
 * zone down notifications, so only registers for two callbacks.
 */
void
initialize_zone_callbacks(void)
{
	sc_syslog_msg_handle_t sys_handle;
	zone_state_callback_t callbacks[3] = {NULL, up_callback,
	    down_callback};

	if (debug) {
		dbg_msgout(NOGET("Registering for zone callbacks\n"));
	}

	if (register_zone_state_callbacks(callbacks, 3) != 0) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * The rgmd or rpc.fed failed to register with sc_zonesd for
		 * zone state change notification. The node will be aborted.
		 * @user_action
		 * Examine other syslog messages occurring at about the same
		 * time to see if the problem can be identified which is
		 * preventing this registration. Look for door-related or
		 * sc_zonesd-related errors. Save a copy of the
		 * /var/adm/messages files on all nodes and contact your
		 * authorized Sun service provider for assistance in
		 * diagnosing and correcting the problem.
		 */
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE, SYSTEXT("fatal: Error registering "
			"for zone state change notifications"));
		sc_syslog_msg_done(&sys_handle);

		/* let failfast bring down the node */
		exit(1);
	}
}
