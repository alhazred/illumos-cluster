/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)feadm.c	1.24	08/09/30 SMI"


/*
 * feadm.c - command feadm(1)
 *
 *	RPC client interface to SC Fork Execution Daemon
 */

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <strings.h>
#include <unistd.h>
#include <errno.h>
#include <tzfile.h>
#include <rpc/rpc.h>
#include <pwd.h>
#include <locale.h>
#include <sys/wait.h>
/* SC SLM addon start */
#include <sys/sol_version.h>
#include "fed_slm_defs.h"
#include <rgm/scha_strings.h>
/* SC SLM addon end */

#include <sys/sol_version.h>
#if SOL_VERSION >= __s10
#include <zone.h>
#endif


/*
 * fe.h is included in fecl.h
 */
#include "rgm/fecl.h"

/*
 * Command line options.
 * See Usage below. All options have arguments except s, r, and B (no :)
 */
#if SOL_VERSION >= __s10
#define	FE_OPTARGS "e:m:t:h:g:s:r:k:z:niE:G:R:X:CUDFA:a:b:W:B"
#else
#define	FE_OPTARGS "e:m:t:h:g:s:r:k:niE:G:R:X:CUDFA:a:b:W:B"
#endif


struct fe_local_opts {
	int type;
	/* SC SLM addon start */
	int slm_type;
	/* SC SLM addon end */
	char *tag;
	char *cmd;
	int cmd_index;
};

typedef struct fe_local_opts fe_local_opts;

static void usage(char *progname);
static int parse_options(int argc, char **argv, fe_cmd *fed_opts,
			fe_local_opts *opts);



/*
 * Usage:
 *		e - execute
 *		m - method path
 * 		t - timeout
 * 		g - grace period
 * 		h - host
 *		      - env is going to be hardcoded so it's not an arg
 *		i - run with timeout initially suspended
 *
 *		n - no process core will be produced on timeout
 *
 *		s - suspend
 *
 *		r - resume
 *
 *		k - kill
 *
 *		z - zonename	<post SOL 9>
 *
 *		B - run in background (non-blocking)
 *
 *		execute method:
 *		   feadm -e <tag> -h <host> -t <timeout>
 *			-g <grace_period> -m <method> [args...]
 *		suspend timeout enforcement:
 *		   feadm -s <tag>
 *		resume timeout enforcement:
 *		   feadm -r <tag>
 *		kill method instantly:
 *		   feadm -k <tag>
 *		SLM addons:
 *		E - execute method under SLM control, to use with -G -R
 *		    [-X -W -A]
 *		U - update a RG SLM propertie, to use with -G -R and -X -W -A
 *		G - rg name, for -E or -U
 *		R - r name, for -E or -U
 *		X - RG_SLM_CPU_SHARES value for -E or -U
 *		W - RG_SLM_PSET_MIN value for -E or -U
 *		A - RG_SLM_pset_type value for -E or -U
 *		C - fed SLM check
 *		D - toggle fed SLM debug traces
 *		a - check scconf SLM propertie globalzoneshares update in fed
 *		b - check scconf SLM propertie defaultpsetmin update in fed
 */
static void
usage(char *progname)
{

	(void) fprintf(stderr, gettext(
	    "Usage: \n"
	    "to execute a method:\n"
	    "   %s [-inB] -e <tag> -t <timeout> -g <grace period >\n"
#if SOL_VERSION >= __s10
	    "		 -h <host> [-z <zonename>] -m <method> [args...]\n"
#else

	    "		 -h <host> -m <method> [args...]\n"
#endif
	    "Flags used with -e:\n"
	    "   -i timeout enforcement is initially suspended\n"
	    "   -n produce no process cores upon timeout\n"
	    "   -B execute command in background, non-blocking\n"
	    "to suspend timeout enforcement of a method:\n"
	    "   %s -s <tag>\n"
	    "to resume timeout enforcement of a method:\n"
	    "   %s -r <tag>\n"
	    "to kill method instantly:\n"
	    "   %s -k <tag>\n"),
	    progname, progname, progname, progname);
	(void) fprintf(stderr, gettext(
	    "options for fed SLM debug, use with care, no arg check:\n"
	    "-E execute method under SLM control, to use with -G -R "
		"[-X -W -A]\n"
	    "-U update a RG SLM propertie, to use with -G -R and -X -W -A\n"
	    "-G <rg name> for -E or -U\n"
	    "-R <r name> for -E or -U\n"
	    "-X <integer> RG_SLM_CPU_SHARES value for -E or -U\n"
	    "-W <integer> RG_SLM_PSET_MIN value for -E or -U\n"
	    "-A <slm_pset_type> for -E or -U\n"
	    "-C fed SLM check\n"
	    "-D toggle fed SLM debug traces\n"
	    "-a <shares> check scconf SLM propertie globalzoneshares "
		"update in fed\n"
	    "-b <cpu> check scconf SLM propertie defaultpsetmin "
		"update in fed\n"));
}

static char *progname;

int
main(int argc, char *argv[])
{
	fe_cmd fed_opts = {0};
	fe_local_opts local_opts = {0}; /* type, tag, cmd, cmd_index */
	fe_run_result result = {0};
	int err = 0;
	char *err_str = NULL;
	char	**env;
	/*
	 * get locale
	 */
	(void) setlocale(LC_ALL, "");
	(void) textdomain(TEXT_DOMAIN);
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	/*
	 * set environment for method execution
	 */
	env = fe_set_env_vars();

	if ((progname = strrchr(argv[0], '/')) == NULL)
		progname = argv[0];
	else
		progname++;

	if (parse_options(argc, argv, &fed_opts, &local_opts) == -1) {
		return (-1);
	}

	fed_opts.security = FE_SECURITY_DEF;

	switch (local_opts.type) {
	case FE_RUN:
		argv += (local_opts.cmd_index -1);
		err = fe_run(local_opts.tag, &fed_opts, local_opts.cmd,
		    argv, env, &result);

		free(fed_opts.corepath);
		free(fed_opts.zonename);	/* always null for pre s10 */
		break;
	case FE_SUSPEND:
		err = fe_suspend(local_opts.tag, &fed_opts, &result);
		break;
	case FE_RESUME:
		err = fe_resume(local_opts.tag, &fed_opts, &result);
		break;
	case FE_KILL:
		err = fe_kill(local_opts.tag, &fed_opts, &result);
		break;
	/* SC SLM addon start */
	case FE_SLM_UPDATE_ARGS:
		err = fe_slm_update(local_opts.tag, &fed_opts, &result);
		break;
	case FE_SLM_DEBUG:
		err = fe_slm_debug(local_opts.tag, &fed_opts, &result);
		break;
	case FE_SLM_CHECK:
		err = fe_slm_check(local_opts.tag, &fed_opts, &result);
		break;
	case FE_SLM_CONF_ARGS:
	{
		switch (local_opts.slm_type) {
		case FE_SLM_CONF_GLOBAL_ZONE_SHARES:
			err = fe_slm_conf_global_zone_shares(local_opts.tag,
			    &fed_opts, &result);
			break;
		case FE_SLM_CONF_PSET_MIN:
			err = fe_slm_conf_pset_min(local_opts.tag,
			    &fed_opts, &result);
			break;
		default:
			break;
		}
	}
	/* SC SLM addon end */
	default:
		return (-1);
	}

	if (err == -1)
		return (err);

	switch (result.type) {
	case FE_OKAY:
		(void) fprintf(stderr, "IT WORKED!\n");
		if (local_opts.type == FE_RUN) {
			(void) fprintf(stderr,
			"method returned code: %d %d\n",
			result.method_retcode,
			WEXITSTATUS(result.method_retcode));
		}
		return (0);
	case FE_SYSERRNO:
		if (strerror(result.sys_errno)) {
			/* no text, no need for gettext */
			(void) fprintf(stderr, "%s: \"%s\" %s\n",
			    progname, local_opts.tag,
			    strerror(result.sys_errno));
		} else {
			(void) fprintf(stderr, gettext(
				"%s: \"%s\" Errno: %d\n"),
			    progname, local_opts.tag,
			    result.sys_errno);
		}
		switch (result.sys_errno) {
		case ENOENT:
			return (1);
		case ETIME:
			return (2);
		default:
			return (-1);
		}
	case FE_NOTAG:
		(void) fprintf(stderr,
		    gettext("%s: Tag \"%s\" does not exist.\n"),
		    progname, local_opts.tag);
		return (1);
	case FE_DUP:
		(void) fprintf(stderr,
		    gettext("%s: Request \"%s\" already queued.\n"),
		    progname, local_opts.tag);
		return (1);
	case FE_UNKNC:
		(void) fprintf(stderr, gettext("%s: Unknown command type.\n"),
			progname);
		return (3);
	case FE_ACCESS:
		(void) fprintf(stderr, gettext("%s: ACCESS error code=%d.\n"),
			progname, result.sec_errno);
		err_str = security_get_errormsg(result.sec_errno);
		if (err_str) {
			(void) fprintf(stderr, "%s: %s\n",
				progname, err_str);
			free(err_str);
		}
		return (3);
	case FE_CONNECT:
		(void) fprintf(stderr, gettext("%s: Connection error.\n"),
			progname);
		if (result.sys_errno == RPC_PROGNOTREGISTERED)
			(void) fprintf(stderr, gettext(
			"Program not registered or server not started\n."));
		return (1);
	case FE_TIMEDOUT:
		(void) fprintf(stderr, gettext("%s: Method was timed out\n"),
			progname);
		return (3);
	case FE_QUIESCED:
		(void) fprintf(stderr, gettext("%s: Method was quiesced\n"),
			progname);
		return (3);
	case FE_UNKILLABLE:
		(void) fprintf(stderr, gettext("%s: Method was unkillable\n"),
			progname);
		return (3);
	/* SC SLM addon start */
	case FE_SLM_ERROR:
		(void) fprintf(stderr, gettext("%s: FE_SLM_ERROR\n"),
			progname);
		return (3);
	/* SC SLM addon end */
	default:
		(void) fprintf(stderr,
		    gettext("%s: Unknown error (%d/%d)\n"),
			progname, result.type,
			result.sys_errno);
		return (-1);
	}

}


/*
 * parse_options:
 *
 *	Parse the input options.  We need to keep track
 *	of the optind index so that we can retrieve the
 *	requested command from argv.  Rather than propagate
 *	the usage of optind (getopt is only used here), we
 *	save it in cmd_index in the fe_cmd structure.
 */
static int
parse_options(int argc, char *argv[], fe_cmd *fed_opts,
    fe_local_opts *local_opts)
{
	extern char *optarg;
	extern int optind;
	int c;
	boolean_t produce_core = B_TRUE;

	/*
	 * Set defaults for the following:
	 *
	 *	timeout == 0	-- don't wait
	 */

	fed_opts->timeout	= 0;
	fed_opts->flags	= 0;
	/* SC SLM addon start */
	fed_opts->slm_flags	= 0;
	/* SC SLM addon end */

	while ((c = getopt(argc, argv, FE_OPTARGS)) != EOF) {
		switch (c) {
			/* run tag */
			case 'e':
				if (local_opts->type != FE_NULL) {
					return (-1);
				}
				local_opts->type = FE_RUN;
				local_opts->tag = optarg;
				/* SC SLM addon start */
				fed_opts->rg_name = "x";
				fed_opts->r_name = "x";
				fed_opts->rg_slm_type = RG_SLM_TYPE_MANUAL;
				fed_opts->rg_slm_pset_type =
				    RG_SLM_PSET_TYPE_DEFAULT;
				fed_opts->rg_slm_cpu =
				    SCHA_SLM_RG_CPU_SHARES_DEFAULT;
				fed_opts->rg_slm_cpu_min =
				    SCHA_SLM_RG_PSET_MIN_DEFAULT;
				/* SC SLM addon end */
				break;
			/* host */
			case 'h':
				fed_opts->host = optarg;
				break;
			/* method path */
			case 'm':
				local_opts->cmd = optarg;
				break;
			/* timeout */
			case 't':
				fed_opts->timeout = atoi(optarg);
				break;
			/* initially suspend timeout */
			case 'i':
				fed_opts->flags |= FE_OPT_INITSUSPEND;
				break;
			/* execute command in background (non-blocking) */
			case 'B':
				fed_opts->flags |= FE_ASYNC;
				break;
#if SOL_VERSION >= __s10
			case 'z':
				fed_opts->zonename = optarg;
				break;
#endif
			/* produce no process core upon timeout */
			case 'n':
				produce_core = B_FALSE;
				break;
			/* grace period */
			case 'g':
				fed_opts->grace_period = atoi(optarg);
				break;
			/* suspend */
			case 's':
				if (local_opts->type != FE_NULL) {
					return (-1);
				}
				local_opts->type = FE_SUSPEND;
				local_opts->tag = optarg;
				break;
			/* resume */
			case 'r':
				if (local_opts->type != FE_NULL) {
					return (-1);
				}
				local_opts->type = FE_RESUME;
				local_opts->tag = optarg;
				break;
			case 'k':
				if (local_opts->type != FE_NULL) {
					return (-1);
				}
				local_opts->type = FE_KILL;
				local_opts->tag = optarg;
				break;

			/* SC SLM addon start */
			case 'E':
				/*
				 * A method run by fed under SLM control
				 * to be used with -G [-X] [-W] [-A]
				 */
				if (local_opts->type != FE_NULL) {
					return (-1);
				}
				local_opts->type = FE_RUN;
				local_opts->tag = optarg;
				fed_opts->rg_name = "x";
				fed_opts->r_name = "x";
				fed_opts->rg_slm_type = RG_SLM_TYPE_AUTO;
				fed_opts->rg_slm_pset_type =
				    RG_SLM_PSET_TYPE_DEFAULT;
				fed_opts->rg_slm_cpu =
				    SCHA_SLM_RG_CPU_SHARES_DEFAULT;
				fed_opts->rg_slm_cpu_min =
				    SCHA_SLM_RG_PSET_MIN_DEFAULT;
				break;
			case 'U':
				/*
				 * check RG SLM properties update in fed SLM
				 * to use with -G and -X,-W,-A
				 */
				if (local_opts->type != FE_NULL) {
					return (-1);
				}
				local_opts->type = FE_SLM_UPDATE_ARGS;
				fed_opts->slm_flags = FE_SLM_UPDATE_RG_PROP;
				fed_opts->rg_slm_type = RG_SLM_TYPE_AUTO;
				break;
			case 'G':
				/* RG name for -E or -U */
				fed_opts->rg_name = optarg;
				break;
			case 'R':
				/* R name for -E or -U */
				fed_opts->r_name = optarg;
				break;
			case 'X':
				/* RG_SLM_CPU_SHARES value for -E or -U */
				fed_opts->rg_slm_cpu = atoi(optarg);
				break;
			case 'W':
				/* RG_SLM_PSET_MIN value for -E or -U */
				fed_opts->rg_slm_cpu_min = atoi(optarg);
				break;
			case 'C':
				/* Check SLM in fed */
				if (local_opts->type != FE_NULL) {
					return (-1);
				}
				local_opts->type = FE_SLM_CHECK;
				break;
			case 'A':
				/* RG_SLM_pset_type value for -E or -U */
				fed_opts->rg_slm_pset_type = optarg;
				break;
			case 'D':
				/* Toggle SLM debug traces */
				if (local_opts->type != FE_NULL) {
					return (-1);
				}
				local_opts->type = FE_SLM_DEBUG;
				break;
			/* SLM conf global zone shares */
			case 'a':
				/*
				 * check scconf update of SLM
				 * propertie globalzoneshares in fed SLM
				 */
				if (local_opts->type != FE_NULL) {
					return (-1);
				}
				local_opts->type =
				    FE_SLM_CONF_ARGS;
				local_opts->slm_type =
				    FE_SLM_CONF_GLOBAL_ZONE_SHARES;
				fed_opts->global_zone_shares = (unsigned int)
				    atoi(optarg);
				break;
			case 'b':
				/*
				 * check scconf update of SLM
				 * propertie defaultpsetmin in fed SLM
				 */
				if (local_opts->type != FE_NULL) {
					return (-1);
				}
				local_opts->type = FE_SLM_CONF_ARGS;
				local_opts->slm_type = FE_SLM_CONF_PSET_MIN;
				fed_opts->pset_min = (unsigned int)
				    atoi(optarg);
				break;
			/* SC SLM addon end */
			default:
				usage(progname);
				return (-1);
		}
	}

	if (produce_core) {	/* default behaviour */
		fed_opts->flags 	|= FE_PRODUCE_CORE;
		fed_opts->corepath	= strdup("/var/cluster/core");
		if (fed_opts->corepath == NULL) {
			(void) fprintf(stderr, gettext("Out of swap"));
			return (-1);
		}
	}

	if (local_opts->type == FE_RUN) {

		local_opts->cmd_index = optind;
		(void) fprintf(stderr, "number of arguments is: %d\n",
		    local_opts->cmd_index);

		/*
		 * Print args read so the user can check
		 * if they entered correctly.
		 */
		(void) fprintf(stderr, gettext(
	"You entered the following arguments for the run command:\n"));
		if (local_opts->tag)
			(void) fprintf(stderr, gettext(
				"tag: %s\n"), local_opts->tag);
		else
			(void) fprintf(stderr, gettext(
				"tag is null\n"));

		if (fed_opts->host)
			(void) fprintf(stderr, gettext(
				"host: %s\n"), fed_opts->host);
		else
			(void) fprintf(stderr, gettext(
				"host is null\n"));

		if (local_opts->cmd)
			(void) fprintf(stderr, gettext(
				"method path: %s\n"), local_opts->cmd);
		else
			(void) fprintf(stderr, gettext(
				"method path is null\n"));

		if (fed_opts->timeout)
			(void) fprintf(stderr, gettext(
				"timeout: %d\n"), fed_opts->timeout);
		else
			(void) fprintf(stderr, gettext(
				"timeout is null\n"));
		if (fed_opts->flags & FE_OPT_INITSUSPEND)
			(void) fprintf(stderr, gettext(
				"initially suspend timeout\n"));
		if (fed_opts->flags & FE_PRODUCE_CORE)
			(void) fprintf(stderr, gettext(
				"produce core on method timeout\n"));
		if (fed_opts->grace_period)
			(void) fprintf(stderr, gettext(
				"grace_period: %d\n"), fed_opts->grace_period);
		else
			(void) fprintf(stderr, gettext(
				"grace_period is null\n"));
#if SOL_VERSION >= __s10
		if (fed_opts->zonename)
			(void) fprintf(stderr, gettext(
				"run in zone: %s\n"), fed_opts->zonename);
		else
			(void) fprintf(stderr, gettext(
				"zonename is NULL. run in global zone\n"));
#endif
	/* SC SLM addon start */
		if (fed_opts->r_name)
			(void) fprintf(stderr, gettext(
			    "r_name %s\n"), fed_opts->r_name);
		else
			(void) fprintf(stderr, gettext(
			    "r_name is null\n"));
		if (fed_opts->rg_name)
			(void) fprintf(stderr, gettext(
			    "rg_name %s\n"), fed_opts->rg_name);
		else
			(void) fprintf(stderr, gettext(
			    "rg_name is null\n"));
		if (fed_opts->rg_slm_type)
			(void) fprintf(stderr, gettext(
			    "rg_slm_type %s\n"), fed_opts->rg_slm_type);
		else
			(void) fprintf(stderr, gettext(
			    "rg_slm_type is null\n"));
		if (fed_opts->rg_slm_pset_type)
			(void) fprintf(stderr, gettext(
			    "rg_slm_pset_type %s\n"),
			    fed_opts->rg_slm_pset_type);
		else
			(void) fprintf(stderr, gettext(
			    "rg_slm_pset_type is null\n"));
		(void) fprintf(stderr, gettext(
		    "rg_slm_cpu %d\n"), fed_opts->rg_slm_cpu);
		(void) fprintf(stderr, gettext(
		    "rg_slm_cpu_min %d\n"), fed_opts->rg_slm_cpu_min);
		(void) fprintf(stderr, gettext(
		    "slm_flags %x\n"), fed_opts->slm_flags);

	} else if (local_opts->type == FE_SLM_DEBUG) {
		(void) fprintf(stderr, gettext(
			"SCSLM debug\n"));
	} else if (local_opts->type == FE_SLM_CHECK) {
		(void) fprintf(stderr, gettext(
			"SCSLM check\n"));
	} else if (local_opts->type == FE_SLM_UPDATE_ARGS) {
		(void) fprintf(stderr, gettext(
			"SCSLM update resource group\n"));
	} else if (local_opts->slm_type == FE_SLM_CONF_GLOBAL_ZONE_SHARES) {
		(void) fprintf(stderr, gettext(
			"SCSLM update global zone shares\n"));
	} else if (local_opts->slm_type == FE_SLM_CONF_PSET_MIN) {
		(void) fprintf(stderr, gettext(
			"SCSLM update default pset min size\n"));
	/* SC SLM addon end */

	} else if (local_opts->type == FE_NULL) {

		usage(progname);
		return (-1);

	} else if (local_opts->type == FE_SUSPEND ||
	    local_opts->type == FE_RESUME) {

		if (local_opts->tag)
			(void) fprintf(stderr, gettext(
				"tag: %s\n"), local_opts->tag);
		else
			(void) fprintf(stderr, gettext(
				"tag is null\n"));

	} else {
		if (optind < argc) {
			(void) fprintf(stderr,
			    gettext("%s: Too many command line arguments\n"),
			    progname);
			return (-1);
		}
	}

	return (0);
}
