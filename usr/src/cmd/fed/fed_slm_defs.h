/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _FED_SLM_DEFS_H
#define	_FED_SLM_DEFS_H

#pragma ident	"@(#)fed_slm_defs.h	1.9	08/05/20 SMI"

/*
 * fed_slm_defs.h - header file for fed Servive Level Management
 * Definitions.
 */

#ifdef __cplusplus
extern "C" {
#endif

#ifndef linux
#include <sys/fss.h>
#ifndef FSS_MAXSHARES
#define	BUG_6541238
#define	FSS_MAXSHARES   65535 /* XXX from sys/fss.h */
#endif
#endif


/* SCSLM configuration default values */
#define	SCSLM_THREAD_PERIOD		300
#define	SCSLM_GLOBAL_ZONE_SHARES	1
#define	SCSLM_DEFAULT_PSET_MIN		1

#define	SCSLM_MAX_SHARES		FSS_MAXSHARES
#define	SCSLM_MAX_CPUS			(FSS_MAXSHARES / 100)

#define	SCSLM_PROJ_FILE		"/etc/cluster/scslm_proj"
#define	SCSLM_PROJ_FILE_LOCK	"/var/cluster/run/scslm_proj.lock"
#define	SCSLM_FILE_LOCK_MODE	(S_IWUSR)
#define	SCSLM_GENERATED		"SunCluster generated"

#define	SCSLM_PROJ		"SCSLM_"
#define	SCSLM_POOL		"SCSLM_pool_"
#define	SCSLM_PSET		"SCSLM_pset_"

#define	PROJ_ADD		"/usr/sbin/projadd"
#define	PROJ_DEL		"/usr/sbin/projdel"
#define	PROJ_MOD		"/usr/sbin/projmod"
#if SOL_VERSION < __s10
#define	PROJ_MODS9		"/usr/cluster/lib/sc/scslm_projmod"
#endif
#define	SCSLM_PROJ_CLEANUP	"/usr/cluster/lib/sc/scslm_cleanup -j %s 2>&1 &"

#define	ZONE_CMD		"/usr/sbin/zlogin"
#define	ZONECFG_CMD		"/usr/sbin/zonecfg"
#define	PRCTL_CMD		"/usr/bin/prctl"

#define	SCSLM_ETC_PROJECT	"/etc/project"

#define	SCSLM_MAX_TYPE_LEN		16
#define	SCSLM_MAX_PSET_TYPE_LEN		24
#define	MAX_COMMAND_SIZE		256
#define	SCSLM_FGETS_SIZE		128

#define	FED_SET_PROJECT_RETRY		5

#define	RG_SLM_TYPE_UNKNOWN			"unknown"
#define	RG_SLM_TYPE_MANUAL			"manual"
#define	RG_SLM_TYPE_AUTO			"automated"
#define	RG_SLM_PSET_TYPE_DFT			"none"
#define	RG_SLM_PSET_TYPE_DEFAULT		"default"
#define	RG_SLM_PSET_TYPE_DEDICATED_WEAK		"weak"
#define	RG_SLM_PSET_TYPE_DEDICATED_STRONG	"strong"


/*
 * Errors
 */
#define	SCSLM_SUCCESS			0
#define	SCSLM_ERROR			1
#define	SCSLM_ERROR_UNKNOWN_ZONE	2
#define	SCSLM_ERROR_UNKNOWN_PROJECT	3
#define	SCSLM_ERROR_UNKNOWN_POOL	4
#define	SCSLM_ERROR_UNKNOWN_PSET	5
#define	SCSLM_ERROR_EXISTING_ZONE	6
#define	SCSLM_ERROR_EXISTING_PROJECT	7
#define	SCSLM_ERROR_EXISTING_POOL	8
#define	SCSLM_ERROR_EXISTING_PSET	9
#define	SCSLM_ERROR_MIN_PSET		10
#define	SCSLM_ERROR_MAX_PSET		11
#define	SCSLM_ERROR_UNKNOWN_RG		12
#define	SCSLM_ERROR_NOT_SLM_POOL	13
#define	SCSLM_IS_DEDICATED_WEAK		14
#define	SCSLM_IS_NOT_DEDICATED		15
#define	SCSLM_ERROR_POOLADM		16
#define	SCSLM_ERROR_CORRUPT_PROJECT	17
#define	SCSLM_PARSE_END			18
#define	SCSLM_POOLS_DISABLED		19
#define	SCSLM_POOLS_NO_CONF		20
#define	SCSLM_IS_DEDICATED_STRONG	21
#define	SCSLM_ERROR_UPDATE		22

#define	SCSLM_PSET_NONE			0
#define	SCSLM_PSET_STRONG		1
#define	SCSLM_PSET_WEAK			2
#define	SCSLM_PSET_WEAK_DELETED		3
#define	SCSLM_PSET_UNMODIFIED		4


#define	LOG_FED_SLM_INFO1(hdl, p1)	\
	(void) sc_syslog_msg_initialize(&hdl, SC_SYSLOG_RGM_FED_TAG, "");\
	(void) sc_syslog_msg_log(hdl, LOG_NOTICE, MESSAGE, p1);\
	sc_syslog_msg_done(&hdl);
#define	LOG_FED_SLM_INFO2(hdl, p1, p2)	\
	(void) sc_syslog_msg_initialize(&hdl, SC_SYSLOG_RGM_FED_TAG, "");\
	(void) sc_syslog_msg_log(hdl, LOG_NOTICE, MESSAGE, p1, p2);\
	sc_syslog_msg_done(&hdl);
#define	LOG_FED_SLM_INFO3(hdl, p1, p2, p3)	\
	(void) sc_syslog_msg_initialize(&hdl, SC_SYSLOG_RGM_FED_TAG, "");\
	(void) sc_syslog_msg_log(hdl, LOG_NOTICE, MESSAGE, p1, p2, p3);\
	sc_syslog_msg_done(&hdl);
#define	LOG_FED_SLM_INFO4(hdl, p1, p2, p3, p4)	\
	(void) sc_syslog_msg_initialize(&hdl, SC_SYSLOG_RGM_FED_TAG, "");\
	(void) sc_syslog_msg_log(hdl, LOG_NOTICE, MESSAGE, p1, p2, p3, p4);\
	sc_syslog_msg_done(&hdl);
#define	LOG_FED_SLM_INFO5(hdl, p1, p2, p3, p4, p5)	\
	(void) sc_syslog_msg_initialize(&hdl, SC_SYSLOG_RGM_FED_TAG, "");\
	(void) sc_syslog_msg_log(hdl, LOG_NOTICE, MESSAGE, p1, p2, p3, p4, p5);\
	sc_syslog_msg_done(&hdl);
#define	LOG_FED_SLM_INFO6(hdl, p1, p2, p3, p4, p5, p6)	\
	(void) sc_syslog_msg_initialize(&hdl, SC_SYSLOG_RGM_FED_TAG, "");\
	(void) sc_syslog_msg_log(hdl, \
	    LOG_NOTICE, MESSAGE, p1, p2, p3, p4, p5, p6);\
	sc_syslog_msg_done(&hdl);


#ifdef __cplusplus
}
#endif

#endif /* _FED_SLM_DEFS_H */
