/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)fed.c	1.22	08/05/20 SMI"

/*
 * fed.c
 * server routines for Fork Execution Daemon
 */
#include "fed.h"
#include <sys/sc_syslog_msg.h>

#if DOOR_IMPL
#include <rgm/fe.h>
#include <rgm/door/fe_door.h>
#include <alloca.h>
#else
#include <rgm/rpc/fe.h>
#endif
/* SC SLM addon start */
#include "fed_slm.h"
/* SC SLM addon end */

/*
 * compile flag debug is used to disable some options during testing
 */
#ifdef DEBUG
static int DBG = 1;
#else
static int DBG = 0;
#endif

#if DOOR_IMPL
/* ARGSUSED */
void fed_door_server(void *cookie,
    char *dataptr, size_t data_size,
    door_desc_t *desc_ptr, uint_t ndesc)
{
	fed_input_args_t *fed_arg = NULL;
	XDR xdrs, xdrs_result, xdrs_error;
	void *result = NULL;
	char *result_buf = NULL;
	char *error_buf = NULL;
	size_t result_size;
	int return_code = 0;
	int num_retries = XDR_NUM_RETRIES;
	sc_syslog_msg_handle_t handle;

	if (data_size == 0) {
		/*
		 * This should not happen but we check for it, just
		 * to be sure that the size of the arguments is not
		 * nil.
		 */
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * An internal error has occurred in the inter-process
		 * communication between rgmd and another clustering process.
		 * Other related error messages might precede or follow this
		 * one in the syslog output.
		 * @user_action
		 * Contact your authorized Sun service provider to determine
		 * whether a workaround or patch is available.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    SYSTEXT("Incoming argument size is zero."));
		sc_syslog_msg_done(&handle);
		return_code = DOOR_INCOMING_ARG_SIZE_NULL;
		goto error_return;
	}

	fed_arg = (fed_input_args_t *)calloc(1, sizeof (fed_input_args_t));
	if (fed_arg == NULL) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * The rgmd process has failed to allocate new memory, most
		 * likely because the system has run out of swap space.
		 * @user_action
		 * Investigate the cause of swap space depletion and correct
		 * the problem, if possible. Reboot your system. If the
		 * problem recurs, you might need to increase swap space by
		 * configuring additional swap devices. See swap(1M) for more
		 * information.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    SYSTEXT("Unable to allocate memory at door server"));
		sc_syslog_msg_done(&handle);
		return_code = UNABLE_TO_ALLOCATE_MEM;
		goto error_return;
	}

	xdrmem_create(&xdrs, dataptr, data_size, XDR_DECODE);

	if (!xdr_fed_input_args(&xdrs, fed_arg)) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * An internal error has occurred in the inter-process
		 * communication between Sun Cluster processes. Related error
		 * messages might be found near this one in the syslog output.
		 * @user_action
		 * Contact your authorized Sun service provider to determine
		 * whether a workaround or patch is available.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    SYSTEXT("XDR Error while decoding arguments."));
		sc_syslog_msg_done(&handle);
		return_code = XDR_ERROR_AT_SERVER;
		goto error_return;
	}
	switch (fed_arg->api_name) {
	case FEPROC_NULL:
		(void) feproc_null_2_svc(NULL, NULL);
		break;
	case FEPROC_RUN:
		result = (fe_run_result *)calloc(1, sizeof (fe_run_result));
		if (!feproc_run_2_svc((fe_run_args *)(fed_arg->data),
		    (fe_run_result *)(result))) {
			return_code = FALSE_RET_FROM_API_FN;
			goto error_return;
		}
		break;
	case FEPROC_SUSPEND:
		result = (fe_result *)calloc(1, sizeof (fe_result));
		if (!feproc_suspend_2_svc((fe_args *)(fed_arg->data),
		    (fe_result *)(result))) {
			return_code = FALSE_RET_FROM_API_FN;
			goto error_return;
		}
		break;
	case FEPROC_RESUME:
		result = (fe_result *)calloc(1, sizeof (fe_result));
		if (!feproc_resume_2_svc((fe_args *)(fed_arg->data),
		    (fe_result *)(result))) {
			return_code = FALSE_RET_FROM_API_FN;
			goto error_return;
		}
		break;
	case FEPROC_KILL:
		result = (fe_result *)calloc(1, sizeof (fe_result));
		if (!feproc_kill_2_svc((fe_args *)(fed_arg->data),
		    (fe_result *)(result))) {
			return_code = FALSE_RET_FROM_API_FN;
			goto error_return;
		}
		break;
	/* SC SLM addon start */
	case FEPROC_SLM_UPDATE:
		result = (int *)calloc(1, sizeof (int));
		if (!feproc_slm_update_2_svc((fe_slm_update_args *)
		    (fed_arg->data), (int *)(result))) {
			return_code = FALSE_RET_FROM_API_FN;
			goto error_return;
		}
		break;
	case FEPROC_SLM_CONF:
		result = (int *)calloc(1, sizeof (int));
		if (!feproc_slm_conf_2_svc((fe_slm_conf_args *)(fed_arg->data),
		    (int *)(result))) {
			return_code = FALSE_RET_FROM_API_FN;
			goto error_return;
		}
		break;
	case FEPROC_SLM_CHECK:
		result = (int *)calloc(1, sizeof (int));
		if (!feproc_slm_check_2_svc((void *)NULL, (int *)(result))) {
			return_code = FALSE_RET_FROM_API_FN;
			goto error_return;
		}
		break;
	case FEPROC_SLM_DEBUG:
		if (!feproc_slm_debug_2_svc((void *)NULL, (void *)NULL)) {
			return_code = FALSE_RET_FROM_API_FN;
			goto error_return;
		}
		break;
	/* SC SLM addon end */
	default:
		return_code = INVALID_API_NAME;
		goto error_return;
	}

	result_size = fed_xdr_sizeof_result(result, fed_arg->api_name);

	/*
	 * For encoding the arguments we will try XDR_NUM_RETRIES times
	 */
	for (num_retries = XDR_NUM_RETRIES; num_retries > 0; num_retries--) {
		result_buf = (char *)alloca(result_size);
		if (result_buf == NULL) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    SYSTEXT("Unable to allocate memory at "
			    "door server"));
			    sc_syslog_msg_done(&handle);
			return_code = UNABLE_TO_ALLOCATE_MEM;
			goto error_return;
		}
		(void) memset(result_buf, 0, (int)result_size);

		xdrmem_create(&xdrs_result, result_buf, result_size,
		    XDR_ENCODE);

		if (!xdr_int(&xdrs_result, &return_code)) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * An internal error has occurred in the inter-process
			 * communication between Sun Cluster processes.
			 * Related error messages might be found near this one
			 * in the syslog output.
			 * @user_action
			 * Contact your authorized Sun service provider to
			 * determine whether a workaround or patch is
			 * available.
			 */
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    SYSTEXT("XDR Error while encoding return "
			    "arguments."));
			sc_syslog_msg_done(&handle);
			return_code = XDR_ERROR_AT_SERVER;
			goto error_return;
		}

		if (!xdr_fed_result_args(&xdrs_result, result,
		    fed_arg->api_name)) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * A non-fatal error occurred while rpc.fed was
			 * marshalling arguments for a remote procedure call.
			 * The operation will be re-tried with a larger
			 * buffer.
			 * @user_action
			 * No user action is required. If the message recurs
			 * frequently, contact your authorized Sun service
			 * provider to determine whether a workaround or patch
			 * is available.
			 */
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    SYSTEXT("FED XDR Buffer Shortfall while "
			    "encoding return arguments API num = %d, "
			    "will retry."), fed_arg->api_name);
			sc_syslog_msg_done(&handle);
			/*
			 * Will retry to encode the result
			 * this time with a larger buffer
			 */
			if (num_retries) {
				result_size += EXTRA_BUFFER_SIZE;
				free(result_buf);
				result_buf = NULL;
				continue;
			}
			/*
			 * Unable to encode the arguments even after
			 * retry
			 */
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			SYSTEXT("XDR Error while encoding return arguments."));
			    sc_syslog_msg_done(&handle);
			return_code = XDR_ERROR_AT_SERVER;
			goto error_return;
		} else {
			break; /* Successfuly Encoded the return arguments. */
		}
	}
	result_size = xdr_getpos(&xdrs_result);

	if (result != NULL) {
		fed_result_args_xdr_free(fed_arg->api_name, result);
		free(result);
	}
	if (fed_arg->data != NULL) {
		fed_input_args_xdr_free(fed_arg);
		free(fed_arg);
	}

	xdr_destroy(&xdrs);
	xdr_destroy(&xdrs_result);

	(void) door_return(result_buf, result_size, NULL, 0);

error_return:
	error_buf = (char *)alloca(RNDUP(sizeof (int)));
	xdrmem_create(&xdrs_error, error_buf, RNDUP(sizeof (int)), XDR_ENCODE);
	if (!xdr_int(&xdrs_error, &return_code)) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * An internal error has occurred in the inter-process
		 * communication between rgmd and another clustering process.
		 * Other related error messages might precede or follow this
		 * one in the syslog output.
		 * @user_action
		 * Contact your authorized Sun service provider to determine
		 * whether a workaround or patch is available.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    SYSTEXT("Severe XDR Error, Cannot recover."));
		sc_syslog_msg_done(&handle);
			/*
			 * We will return with all parameters zero
			 * this has to be checked at the client end
			 * and the appropriate error has to be
			 * sent to the caller.
			 */
		(void) door_return(NULL, 0, NULL, 0);
	}
	xdr_destroy(&xdrs_error);
	(void) door_return(error_buf, RNDUP(sizeof (int)), NULL, 0);
}
#endif

/*
 * fork and start running a method
 */
#if DOOR_IMPL
bool_t
feproc_run_2_svc(fe_run_args *argp, fe_run_result *result)
#else
bool_t
feproc_run_2_svc(fe_run_args *argp, fe_run_result *result,
    struct svc_req *rqstp)
#endif
{
	security_cred_t	fed_cred;
#if DOOR_IMPL
	door_cred_t	door_creds;
#else
	struct authunix_parms rpc_cred;
#endif
	uint_t i;

	if (debug) {
		dbg_msgout(NOGET("entering feproc_run_2_svc %s\n"), argp->tag);
		dbg_msgout(NOGET("\ttimeout %d\n"), argp->timeout);
		dbg_msgout(NOGET("\tcmd %s\n"), argp->cmd);
		dbg_msgout(NOGET("\targ len %u\n"), argp->arg.arg_len);
		for (i = 0; i < argp->arg.arg_len; i++)
			dbg_msgout(NOGET(" %s"), argp->arg.arg_val[i]);
		dbg_msgout(NOGET("\n"));
		dbg_msgout(NOGET("\tenv len %u\n"), argp->env.env_len);
		for (i = 0; i < argp->env.env_len; i++)
			dbg_msgout(NOGET(" %s"), argp->env.env_val[i]);
		dbg_msgout(NOGET("\n"));
	}
	(void) memset(result, 0, sizeof (fe_run_result));

	/*
	 * check security and if check succeeds execute request
	 */
#if DOOR_IMPL
	if (security_svc_authenticate(&door_creds, (DBG) ? FALSE : TRUE)
	    != SEC_OK) {
#else
	if (security_svc_authenticate(rqstp, &rpc_cred,
	    SEC_UNIX_STRONG,
	    (DBG) ? FALSE : TRUE) != SEC_OK) {
#endif
		/*
		 * 1334 AI:it needs to be checkde, if setting SHCA_ERR_ACCESS
		 * as in rgm receptionist is useful or redundant.
		 */
		return (FALSE);
	}

#if DOOR_IMPL
	fed_cred.aup_uid = door_creds.dc_euid;
	fed_cred.aup_gid = door_creds.dc_egid;
#else
	/* for rpc, uid,gid contain euid,egid information */
	fed_cred.aup_uid = rpc_cred.aup_uid;
	fed_cred.aup_gid = rpc_cred.aup_gid;
#endif
	fe_run_svc(argp, result, &fed_cred);
	return (TRUE);
}


/*
 * suspend timeout monitoring and enforcing of
 * killing method when timeout expires
 */
#if DOOR_IMPL
bool_t
feproc_suspend_2_svc(fe_args *argp, fe_result *result)
#else
bool_t
feproc_suspend_2_svc(fe_args *argp, fe_result *result,
    struct svc_req *rqstp)
#endif
{
	security_cred_t	fed_cred;
#if DOOR_IMPL
	door_cred_t	door_creds;
#else
	struct authunix_parms rpc_cred;
#endif

	if (debug)
		dbg_msgout(NOGET("entering feproc_suspend_2_svc %s\n"),
		argp->tag);

	(void) memset(result, 0, sizeof (fe_result));

	/*
	 * check security and if check succeeds execute request
	 */
#if DOOR_IMPL
	if (security_svc_authenticate(&door_creds, (DBG) ? FALSE : TRUE)
	    != SEC_OK) {
#else
	if (security_svc_authenticate(rqstp, &rpc_cred,
	    SEC_UNIX_STRONG,
	    (DBG) ? FALSE : TRUE) != SEC_OK) {
#endif
		return (FALSE);
	}


#if DOOR_IMPL
	fed_cred.aup_uid = door_creds.dc_euid;
	fed_cred.aup_gid = door_creds.dc_egid;
#else
	/* for rpc, uid,gid contain euid,egid information */
	fed_cred.aup_uid = rpc_cred.aup_uid;
	fed_cred.aup_gid = rpc_cred.aup_gid;
#endif

	fe_suspend_svc(argp, result, &fed_cred);
	return (TRUE);
}



/*
 * resume timeout monitoring and enforcing of
 * killing method when timeout expires
 */
#if DOOR_IMPL
bool_t
feproc_resume_2_svc(fe_args *argp, fe_result *result)
#else
bool_t
feproc_resume_2_svc(fe_args *argp, fe_result *result,
    struct svc_req *rqstp)
#endif
{
#if DOOR_IMPL
	door_cred_t	door_creds;
#else
	struct authunix_parms rpc_cred;
#endif
	security_cred_t	fed_cred;

	if (debug)
		dbg_msgout(NOGET("entering feproc_resume_2_svc %s\n"),
		    argp->tag);

	(void) memset(result, 0, sizeof (fe_result));

	/*
	 * check security and if check succeeds execute request
	 */
#if DOOR_IMPL
	if (security_svc_authenticate(&door_creds, (DBG) ? FALSE : TRUE)
	    != SEC_OK) {
#else
	if (security_svc_authenticate(rqstp, &rpc_cred,
	    SEC_UNIX_STRONG,
	    (DBG) ? FALSE : TRUE) != SEC_OK) {
#endif
		return (FALSE);
	}

#if DOOR_IMPL
	fed_cred.aup_uid = door_creds.dc_euid;
	fed_cred.aup_gid = door_creds.dc_egid;
#else
	/* for rpc, uid,gid contain euid,egid information */
	fed_cred.aup_uid = rpc_cred.aup_uid;
	fed_cred.aup_gid = rpc_cred.aup_gid;
#endif
	fe_resume_svc(argp, result, &fed_cred);
	return (TRUE);
}

/*
 * kill method instantly. used by fast quiesce
 * to kill resource methods.
 */
#if DOOR_IMPL
bool_t
feproc_kill_2_svc(fe_args *argp, fe_result *result)
#else
bool_t
feproc_kill_2_svc(fe_args *argp, fe_result *result,
    struct svc_req *rqstp)
#endif
{
#if DOOR_IMPL
	door_cred_t	door_creds;
#else
	struct authunix_parms rpc_cred;
#endif
	security_cred_t	fed_cred;

	if (debug)
		dbg_msgout(NOGET("entering feproc_kill_2_svc %s\n"),
		    argp->tag);

	(void) memset(result, 0, sizeof (fe_result));

	/*
	 * check security and if check succeeds execute request
	 */
#if DOOR_IMPL
	if (security_svc_authenticate(&door_creds, (DBG) ? FALSE : TRUE)
	    != SEC_OK) {
#else
	if (security_svc_authenticate(rqstp, &rpc_cred,
	    SEC_UNIX_STRONG,
	    (DBG) ? FALSE : TRUE) != SEC_OK) {
#endif
		return (FALSE);
	}

#if DOOR_IMPL
	fed_cred.aup_uid = door_creds.dc_euid;
	fed_cred.aup_gid = door_creds.dc_egid;
#else
	/* for rpc, uid,gid contain euid,egid information */
	fed_cred.aup_uid = rpc_cred.aup_uid;
	fed_cred.aup_gid = rpc_cred.aup_gid;
#endif
	fe_kill_svc(argp, result, &fed_cred);
	return (TRUE);
}

#if !DOOR_IMPL
int
fe_program_2_freeresult(SVCXPRT *transp, xdrproc_t xdr_result,
	caddr_t result)
{

	if (debug)
		dbg_msgout(NOGET("entering fe_program_2_freeresult\n"));

	xdr_free(xdr_result, result);

	return (1);
/*
 * Suppress lint messages about unused function args.
 * This is an RPC service routine and must have the
 * signature specified for such routines.
 */
} /*lint !e715 */
#endif
/*
 * null procedure to test that the client can connect to the server
 */
#if DOOR_IMPL
bool_t
feproc_null_2_svc(void *argp, void *result)
#else
bool_t
feproc_null_2_svc(void *argp, void *result, struct svc_req *rqstp)
#endif
{
	if (debug)
		dbg_msgout(NOGET("entering feproc_null_2_svc\n"));
	return (TRUE);
/*
 * Suppress lint messages about unused function args.
 * This is an RPC service routine and must have the
 * signature specified for such routines.
 */
} /*lint !e715 */


/* SC SLM addon start */
#if DOOR_IMPL
bool_t
feproc_slm_update_2_svc(fe_slm_update_args *argp, int *result)
#else
bool_t
feproc_slm_update_2_svc(fe_slm_update_args *argp, int *result,
	struct svc_req *rqstp)
#endif
{
#if DOOR_IMPL
	door_cred_t	door_creds;
#else
	struct authunix_parms rpc_cred;
#endif

	if (debug)
		dbg_msgout(NOGET("entering feproc_slm_update_2_svc\n"));

	/*
	 * check security and if check succeeds execute request
	 */
#if DOOR_IMPL
	if (security_svc_authenticate(&door_creds, (DBG) ? FALSE : TRUE)
	    != SEC_OK) {
#else
	if (security_svc_authenticate(rqstp, &rpc_cred,
	    SEC_UNIX_STRONG,
	    (DBG) ? FALSE : TRUE) != SEC_OK) {
#endif
		return (FALSE);
	}

#ifdef SC_SRM
	*result = scslm_update(argp);
	return (TRUE);
#else
	return (FALSE);
#endif
}


#if DOOR_IMPL
/* ARGSUSED */
bool_t
feproc_slm_debug_2_svc(void *argp, void *result)
#else
/* ARGSUSED */
bool_t
feproc_slm_debug_2_svc(void *argp, void *result, struct svc_req *rqstp)
#endif
{
#if DOOR_IMPL
	door_cred_t	door_creds;
#else
	struct authunix_parms rpc_cred;
#endif

	if (debug)
		dbg_msgout(NOGET("entering feproc_slm_debug_2_svc\n"));

	/*
	 * check security and if check succeeds execute request
	 */
#if DOOR_IMPL
	if (security_svc_authenticate(&door_creds, (DBG) ? FALSE : TRUE)
	    != SEC_OK) {
#else
	if (security_svc_authenticate(rqstp, &rpc_cred,
	    SEC_UNIX_STRONG,
	    (DBG) ? FALSE : TRUE) != SEC_OK) {
#endif
		return (FALSE);
	}

#ifdef SC_SRM
	scslm_debug();
	return (TRUE);
#else
	return (FALSE);
#endif
}


#if DOOR_IMPL
/* ARGSUSED */
bool_t
feproc_slm_check_2_svc(void *argp, int *result)
#else
/* ARGSUSED */
bool_t
feproc_slm_check_2_svc(void *argp, int *result, struct svc_req *rqstp)
#endif
{
#if DOOR_IMPL
	door_cred_t	door_creds;
#else
	struct authunix_parms rpc_cred;
#endif

	if (debug)
		dbg_msgout(NOGET("entering feproc_slm_check_2_svc\n"));

	/*
	 * check security and if check succeeds execute request
	 */
#if DOOR_IMPL
	if (security_svc_authenticate(&door_creds, (DBG) ? FALSE : TRUE)
	    != SEC_OK) {
#else
	if (security_svc_authenticate(rqstp, &rpc_cred,
	    SEC_UNIX_STRONG,
	    (DBG) ? FALSE : TRUE) != SEC_OK) {
#endif
		return (FALSE);
	}

#ifdef SC_SRM
	*result = scslm_check();
	return (TRUE);
#else
	return (FALSE);
#endif
}


#if DOOR_IMPL
/* ARGSUSED */
bool_t
feproc_slm_conf_2_svc(fe_slm_conf_args *argp, int *result)
#else
/* ARGSUSED */
bool_t
feproc_slm_conf_2_svc(fe_slm_conf_args *argp, int *result,
	struct svc_req *rqstp)
#endif
{
#if DOOR_IMPL
	door_cred_t	door_creds;
#else
	struct authunix_parms rpc_cred;
#endif

	if (debug)
		dbg_msgout(NOGET("entering feproc_slm_conf_2_svc\n"));

	/*
	 * check security and if check succeeds execute request
	 */
#if DOOR_IMPL
	if (security_svc_authenticate(&door_creds, (DBG) ? FALSE : TRUE)
	    != SEC_OK) {
#else
	if (security_svc_authenticate(rqstp, &rpc_cred,
	    SEC_UNIX_STRONG,
	    (DBG) ? FALSE : TRUE) != SEC_OK) {
#endif
		return (FALSE);
	}

#ifdef SC_SRM
	*result = scslm_conf(argp);
	return (TRUE);
#else
	return (FALSE);
#endif
}
/* SC SLM addon end */
