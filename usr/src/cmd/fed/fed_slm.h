/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _FED_SLM_H
#define	_FED_SLM_H

#pragma ident	"@(#)fed_slm.h	1.4	08/05/20 SMI"


/*
 * fed_slm.h - header file for fed Servive Level Management
 */

#ifdef __cplusplus
extern "C" {
#endif

#include <sys/sol_version.h>

int scslm_fe_run(fe_handle *);
int scslm_update(fe_slm_update_args *);
int scslm_conf(fe_slm_conf_args *);
void scslm_debug(void);
int scslm_check(void);
int scslm_stop(fe_handle *);
int scslm_init(void);
#if SOL_VERSION >= __s10
int scslm_zone_down(const char *);
int scslm_zone_up(const char *);
#endif


#ifdef __cplusplus
}
#endif

#endif /* _FED_SLM_H */
