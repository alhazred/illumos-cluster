/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)fe_serv_xdr.c	1.6	08/05/20 SMI"


/*
 * File: fe_serv_xdr.c door xdr interface for use
 *	by Fork execution daemon
 */


#include <string.h>
#include <rgm/fe.h>
#include <rgm/door/fe_door.h>
#include <rgm/security.h>
#include <stdlib.h>
#include <sys/cl_assert.h>


void
fed_result_args_xdr_free(int api_name, void *result)
{
	switch (api_name) {
		case FEPROC_NULL:
			break;
		case FEPROC_RUN:
			xdr_free((xdrproc_t)xdr_fe_run_result,
			    (char *)result);
			break;
		case FEPROC_SUSPEND:
		case FEPROC_RESUME:
		case FEPROC_KILL:
			xdr_free((xdrproc_t)xdr_fe_result,
			    (char *)result);
			break;
		/* SC SLM addon start */
		case FEPROC_SLM_UPDATE:
		case FEPROC_SLM_CONF:
		case FEPROC_SLM_CHECK:
		case FEPROC_SLM_DEBUG:
			break;
		/* SC SLM addon end */
		default:
			CL_PANIC(0);
			break;
	}
}
void
fed_input_args_xdr_free(fed_input_args_t *fed_arg)
{
	switch (fed_arg->api_name) {
		case FEPROC_NULL:
			break;
		case FEPROC_RUN:
			xdr_free((xdrproc_t)xdr_fe_run_args,
			    (char *)fed_arg->data);
			break;
		case FEPROC_SUSPEND:
		case FEPROC_RESUME:
		case FEPROC_KILL:
			xdr_free((xdrproc_t)xdr_fe_args,
			    (char *)fed_arg->data);
			break;
		/* SC SLM addon start */
		case FEPROC_SLM_UPDATE:
			xdr_free((xdrproc_t)xdr_fe_slm_update_args,
			    (char *)fed_arg->data);
			break;
		case FEPROC_SLM_CONF:
			xdr_free((xdrproc_t)xdr_fe_slm_conf_args,
			    (char *)fed_arg->data);
			break;
		case FEPROC_SLM_DEBUG:
		case FEPROC_SLM_CHECK:
			break;
		/* SC SLM addon end */
		default:
			CL_PANIC(0);
			break;
	}
	free(fed_arg->data);
}
bool_t
xdr_fed_input_args(register XDR *xdrs, fed_input_args_t *objp)
{
	if (!xdr_int(xdrs, &objp->api_name)) {
		return (FALSE);
	}
	switch (objp->api_name) {
		case FEPROC_NULL:

			break;
		case FEPROC_RUN:
			objp->data = (fe_run_args *)
			    calloc(1, sizeof (fe_run_args));
			if (!xdr_fe_run_args(xdrs,
			    (fe_run_args *)objp->data)) {
				return (FALSE);
			}
			break;
		case FEPROC_SUSPEND:
		case FEPROC_RESUME:
		case FEPROC_KILL:
			objp->data = (fe_args *)
			    calloc(1, sizeof (fe_args));
			if (!xdr_fe_args(xdrs,
			    (fe_args *)objp->data)) {
				return (FALSE);
			}
			break;
		/* SC SLM addon start */
		case FEPROC_SLM_UPDATE:
			objp->data = (fe_slm_update_args *)
			    calloc(1, sizeof (fe_slm_update_args));
			if (!xdr_fe_slm_update_args(xdrs,
			    (fe_slm_update_args *)objp->data)) {
				return (FALSE);
			}
			break;
		case FEPROC_SLM_CONF:
			objp->data = (fe_slm_conf_args *)
			    calloc(1, sizeof (fe_slm_conf_args));
			if (!xdr_fe_slm_conf_args(xdrs,
			    (fe_slm_conf_args *)objp->data)) {
				return (FALSE);
			}
			break;
		case FEPROC_SLM_DEBUG:
		case FEPROC_SLM_CHECK:
			break;
		/* SC SLM addon end */
		default:
			/*
			 * XXX
			 * check what behaviour you have expected here
			 */
			return (FALSE);
	}
	return (TRUE);
}

bool_t
xdr_fed_result_args(XDR *xdrs, void *objp, int api_name)
{
	switch (api_name) {
		case FEPROC_NULL:
			break;

		case FEPROC_RUN:
			if (!xdr_fe_run_result(xdrs, (fe_run_result *)objp)) {
				return (FALSE);
			}
			break;

		case FEPROC_SUSPEND:
		case FEPROC_RESUME:
		case FEPROC_KILL:
			if (!xdr_fe_result(xdrs, (fe_result *)objp)) {
				return (FALSE);
			}
			break;

		/* SC SLM addon start */
		case FEPROC_SLM_UPDATE:
		case FEPROC_SLM_CONF:
		case FEPROC_SLM_CHECK:
		case FEPROC_SLM_DEBUG:
			break;
		/* SC SLM addon end */

		default:
			return (FALSE);
			/* 1334 AI: same comment as above */
	}
	return (TRUE);


}

/*
 * How we calculate size for xdr encoding:
 * ---------------------------------------
 * The Size calculation that we do here has to calculate
 * the sizeof the structure members and also the sizeof
 * the data that is pointed to be the pointers if any
 * int he structure.
 * For eg:
 * struct abc {
 *	int a;
 *	char *ch;
 * }
 * in this case we will find the size of the struct abc
 * which will give us the size needed for storing the
 * elements that is an int and a char pointer. And also
 * we will need to add the sizeof the array pointed to
 * by ch.
 * In addition to this, xdr encodes string in the following
 * way:
 * [integer(for size of the string) + string]
 * So, for any string say char *ch = {"example"}
 * The size needed will be :
 * sizeof (ch) + sizeof (int) + strlen(ch)
 * Here the sizeof (int) is taken for storing the sizeof
 * the string.
 */

size_t
fed_xdr_sizeof_result(void *data, int api)
{
	size_t size = 0;
	size += RNDUP(sizeof (int));
	switch (api) {
		case FEPROC_NULL:break;
		case FEPROC_RUN:
			size += RNDUP(sizeof (fe_run_result)) +
			    RNDUP(safe_strlen(((fe_run_result *)
			    (data))->output)) +
			    RNDUP(sizeof (int));
			break;
		case FEPROC_SUSPEND:
		case FEPROC_RESUME:
		case FEPROC_KILL:
			size += RNDUP(sizeof (fe_result));
			break;
		/* SC SLM addon start */
		case FEPROC_SLM_UPDATE:
		case FEPROC_SLM_CONF:
		case FEPROC_SLM_CHECK:
			size += RNDUP(sizeof (int));
			break;
		case FEPROC_SLM_DEBUG:
			break;
		/* SC SLM addon end */
		default:
			return (FALSE);
			/* XXX check what behaviour is expected here */
	}
	return (size);
}
