/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)fed_util.c	1.53	09/01/06 SMI"


/*
 * fed_util.c
 * Workhorse routines for the fe daemon
 */
#include <sys/stat.h>
#include <wait.h>
#include <dirent.h>
#include <libgen.h>
#include <syslog.h>
#include <errno.h>
#include <sys/utsname.h>

#include <sys/sol_version.h>
#if SOL_VERSION >= __s10
#include <zone.h>
#include <libcontract.h>
#include <sys/contract/process.h>
#include <sys/ctfs.h>
#else
#define	GLOBAL_ZONENAME	"global"
/* SC SLM addon start */
#include <rgm/scha_strings.h>
/* SC SLM addon end */
#endif

#include <sys/sc_syslog_msg.h>

#if DOOR_IMPL
#include <rgm/fe.h>
#else
#include "rgm/rpc/fe.h"
#endif
#include "fed.h"


#ifdef SC_SRM
#include <sys/task.h>
#include <project.h>
#include <pwd.h>
/* SC SLM addon start */
#include "fed_slm_defs.h"
#include "fed_slm.h"
#include <rgm/rgm_methods.h>
/* SC SLM addon end */

#define	PWD_BUF_SIZE	1024
/* error codes for project name validation */
#define	PROJECT_NAME_VALID	0
#define	PROJECT_NAME_INVALID	1

#endif

/* define some constants for capturing output */
#define	INITIAL_ALLOC_SIZE	128
#define	READ_BUF_SIZE	64

static fe_handle *fe_alloc_handle(fe_run_args *args,
	fe_run_result *result,
	security_cred_t *cred);
static fe_handle *id_to_handle(char *id);
static void fe_free_handle(fe_handle *handle);
static void fe_run_process(fe_handle *handle, fe_run_result *result);
static void print_history(fe_process_tag *h);
static bool_t is_unkillable_process(fe_handle *handle);
static void read_method_output(fe_run_result *result, boolean_t syslog_output,
    int pfd[2], fe_handle *handle);
static void prefix_line_tags(char *captured_output, char **final_output,
    boolean_t syslog_output, char *tag);
static void timeout_process_handle(fe_handle *handle);
static void abort_fed(void);
#ifndef linux
static void modify_coreadm_settings(fe_handle *handle);
static void create_method_core_location(fe_handle *handle);

#ifdef SC_SRM
static int fed_setproject(fe_handle *handle, boolean_t);
int fed_retrysetproject(char *, char *, char *);
static boolean_t validate_project(char *proj_name);
static enum FE_ERRCODE validate_project_on_zone(fe_handle *handle,
    boolean_t enter_local_zone, int *err_no, char **err_msg,
    boolean_t produce_core);
#endif

#endif
/*
 * fe_queue_lock protects fe_handle_base, and the ring
 * queue, so it can be updated/inspected by multiple threads.
 */
/*
 * Suppress lint warning about "union initialization"
 * PTHREAD_MUTEX_INITIALIZER is defined in /usr/include/pthread.h
 */
/* CSTYLED */
/*lint -e708 */
pthread_mutex_t fe_queue_lock = PTHREAD_MUTEX_INITIALIZER;
/* CSTYLED */
/*lint +e708 */

static fe_handle *fe_handle_base = NULL;


/*
 * Variables for debugging thread and memory leakage.
 * They are passed as args to functions svc_alloc, svc_free, svc_strdup
 * (in libscutils), where they are incremented/decremented accordingly.
 */
static int fe_alloc_cnt = 0;
static int fe_thread_cnt = 0;

/* hostname defined in fed_main.c */
extern char hostname[SYS_NMLN];

#if SOL_VERSION >= __s10
static int
init_template(int *error_value, char **error_message)
{
	int fd;

	*error_value = 0;
	*error_message = NULL;

	/*
	 * doesn't do anything with the contract.
	 * Deliver no events, don't inherit, and allow it to be orphaned.
	 */
	if ((fd = open64(CTFS_ROOT "/process/template", O_RDWR)) == -1) {
		*error_message = strdup("open64");
	} else if (ct_tmpl_set_critical(fd, 0)) {
		*error_message = strdup("ct_tmpl_set_critical");
	} else if (ct_tmpl_set_informative(fd, 0)) {
		*error_message = strdup("ct_tmpl_set_informative");
	} else if (ct_pr_tmpl_set_fatal(fd, CT_PR_EV_HWERR)) {
		*error_message = strdup("ct_pr_tmpl_set_fatal");
	} else if (ct_pr_tmpl_set_param(fd, CT_PR_PGRPONLY | CT_PR_REGENT)) {
		*error_message = strdup("ct_pr_tmpl_set_param");
	} else if (ct_tmpl_activate(fd)) {
		*error_message = strdup("ct_tmpl_activate");
	} else {
		return (fd);
	}

	/*
	 * We know errno is legitimate. Suppress the lint error.
	 */
	*error_value = errno; /*lint !e746 */
	(void) close(fd);
	return (-1);
}
#endif

static void
log_error(char *tag, int err, char *string) {

	sc_syslog_msg_handle_t sys_handle;
	(void) sc_syslog_msg_initialize(&sys_handle,
	    SC_SYSLOG_RGM_FED_TAG, "");
	/*
	 * SCMSGS
	 * @explanation
	 * The rpc.fed server was not able to run the method with the tag
	 * specified. The cause of the error is specified by the exact error
	 * message.
	 * @user_action
	 * Save the /var/adm/messages file. Contact your authorized Sun
	 * service provider to determine whether a workaround or patch is
	 * available.
	 */
	(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
	    "%s: Couldn't run method tag. Error in %s: %s.",
	    tag, string, strerror(err));
	sc_syslog_msg_done(&sys_handle);
}

#ifndef linux
/*
 * create core dump location before sending SIGABRT. If function fails, coredump
 * may not be produced at desired location w/o any other side effects.
 */
static void
create_method_core_location(fe_handle *handle) {

	sc_syslog_msg_handle_t sys_handle;
	DIR		*dirp;
	struct dirent	*dentp = NULL;
	char		filename[MAXPATHLEN+1];
	mode_t 		mode = S_IRWXU;
	char		*strerrorstring = NULL;
	char		*errorstring = NULL;
	char		coredir[MAXPATHLEN];

	dentp = (struct dirent *)svc_alloc(
	    sizeof (struct dirent) + 1 + MAXNAMELEN, &fe_alloc_cnt);

	if (dentp == NULL) {
		strerrorstring = strerror(ENOMEM);
		goto error_log;
	}
	coredir[0] = '\0';
#if SOL_VERSION >= __s10
	if (handle->zonepath) {
		(void) sprintf(coredir, "%s", handle->zonepath);
	}
#endif
	(void) sprintf(coredir + strlen(coredir), "%s/%s", handle->corepath,
	    handle->tag);

	if (access(coredir, F_OK) != 0) {
		/* core directory doesn't exist. Create one. */
		if (mkdirp(coredir, mode) != 0) {
			strerrorstring = strerror(errno); /*lint !e746 */
			errorstring = svc_strdup(
			    "Creation of core location failed.", &fe_alloc_cnt);
			if (errorstring == NULL)
				strerrorstring = strerror(ENOMEM);
			goto error_log;
		}
	} else {
		/* core directory already exixts. */
		if (access(coredir, W_OK) != 0) {
			/* writable core directory doesn't exist. */
			strerrorstring = strerror(errno); /*lint !e746 */
			errorstring = svc_strdup(
			    "Core Location not writable.", &fe_alloc_cnt);
			if (errorstring == NULL)
				strerrorstring = strerror(ENOMEM);
			goto error_log;
		} else {
			/*
			 * adequate permissions are there.
			 * delete files in directory. Only make a best effort
			 * attempt. don't produce error.
			 */
			if ((dirp = opendir(coredir)) != NULL) {
				do {
					if (readdir_r(dirp, dentp) == NULL)
						break;
					if (strcmp(dentp->d_name, ".") == 0 ||
					    strcmp(dentp->d_name, "..") == 0)
						continue;
					(void) sprintf(filename, "%s/%s",
					    coredir, dentp->d_name);
					(void) remove(filename);

				} while (B_TRUE);
				(void) closedir(dirp);
			}
		}
	}

	svc_free(dentp, &fe_alloc_cnt);
	return;

error_log:
	(void) sc_syslog_msg_initialize(&sys_handle,
	    SC_SYSLOG_RGM_FED_TAG, "");
	/*
	 * SCMSGS
	 * @explanation
	 * The rpc.fed couldn't ensure a writable directory in which to dump
	 * process cores. The exact message indicates the nature of the error.
	 * @user_action
	 * Low memory might be a transient error. If it persists, rebooting
	 * might help. If that fails to help, swap space might need to be
	 * increased. If it is a different error, it might be due to
	 * insufficient privileges for ensuring a writable directory to store
	 * the core. Check and correct the permissions for path indicated in
	 * the message.
	 */
	(void) sc_syslog_msg_log(sys_handle, LOG_WARNING, MESSAGE,
	    "<%s>:<%s>:Process core dump at <%s> for method <%s> may fail.",
	    errorstring == NULL ? "malloc failed" : errorstring, strerrorstring,
	    coredir, handle->tag);
	sc_syslog_msg_done(&sys_handle);

	svc_free(coredir, &fe_alloc_cnt);
	svc_free(errorstring, &fe_alloc_cnt);
	svc_free(dentp, &fe_alloc_cnt);
}

/*
 * This function is called from fe_run_process by the forked child before it
 * does an exec of the method to run. The function configures the core dump
 * location to an approproate value using coreadm. In case of method timeouts
 * SIGABRT dumps the core for the method and its children at this location.
 * coredump setting is inherited by all children unless they call setsid()
 */

static void
modify_coreadm_settings(fe_handle *handle)
{
	char 			*buf;
	char			*tempbuf;
	sc_syslog_msg_handle_t	sys_handle;
	int 			flag = 0;
	FILE			*p;
	char			*strerrorstring;

	buf = (char *)svc_alloc(sizeof (char) * (42 +
	    strlen(handle->corepath) + strlen(handle->tag)), &fe_alloc_cnt);
	tempbuf = (char *)svc_alloc(sizeof (char) * (12 +
	    strlen(handle->corepath) + strlen(handle->tag)), &fe_alloc_cnt);
	if (buf == NULL || tempbuf == NULL) {
		strerrorstring = strerror(ENOMEM);
		flag = ENOMEM;
		goto cleanup;
	}

	(void) strcpy(buf, "/usr/bin/coreadm -p ");		/* 20 */
	(void) sprintf(tempbuf, "%s/%s/", handle->corepath, handle->tag);
								/* 2+A+B */

	(void) strcat(buf, tempbuf);				/* 20+2+A+B */

	(void) strcat(buf, "%f.%n.%p");				/* 20+2+A+B+8 */
	/* handle->pid may not be set yet. Hence getpid() */
	(void) sprintf(tempbuf, " %d", getpid());		/* 1+10 */

	(void) strcat(buf, tempbuf);		/* 20+2+A+B+8+1+10 = 41+A+B */

	/*
	 * if later, we have a committed interface for core_set_process_path()
	 * or corectl(), which are more efficient, popen() can be replaced.
	 */
	if ((p = popen(buf, "w")) == NULL) {
		strerrorstring = strerror(errno); /*lint !e746 */
		flag = errno;
		goto cleanup;
	}
	(void) pclose(p);
	svc_free(buf, &fe_alloc_cnt);
	svc_free(tempbuf, &fe_alloc_cnt);
	return;

cleanup:
	(void) sc_syslog_msg_initialize(&sys_handle,
	    SC_SYSLOG_RGM_FED_TAG, "");
	/*
	 * SCMSGS
	 * @explanation
	 * The rpc.fed was unable to modify coredump settings for the
	 * indicated method.
	 * @user_action
	 * Low memory might be a transient error. If it persists, rebooting
	 * might help. If that fails to help, swap space might need to be
	 * increased. If it is a popen() failure, refer to
	 * coreadm(1M)/popen(3C), or contact your authorized Sun service
	 * proivider.
	 */
	(void) sc_syslog_msg_log(sys_handle, LOG_NOTICE, MESSAGE,
	    "<%s>:<%s>: Core location couldn't be configured for method <%s>.",
	    flag == ENOMEM ? "malloc() failed" : "popen() failed",
	    strerrorstring, handle->tag);

	sc_syslog_msg_done(&sys_handle);
	svc_free(buf, &fe_alloc_cnt);
	svc_free(tempbuf, &fe_alloc_cnt);
}
#endif
/*
 * Executes the fork and execve.
 * Comes in holding the queue lock.
 * Releases it when done copying the values of cmd, arg and env.
 * Then reacquires it after waitpid().
 * Exits holding the lock.
 */
static  void
fe_run_process(fe_handle *handle, fe_run_result *result)
{
	sc_syslog_msg_handle_t sys_handle;
	boolean_t enter_local_zone = B_FALSE;

	int	w = 0;
	int	err;
	char	*err_msg = NULL;
	int	tmpl_fd;

#if SOL_VERSION >= __s10
	zoneid_t zoneid;
#endif
	/* SC SLM addon start */
#ifdef SC_SRM
	int	ret;
	boolean_t	slm_ssm_wrapper = B_FALSE;
	char		*ssm_wrapper = NULL;
	char		**ssm_wrapper_project = NULL;
	enum FE_ERRCODE	proj_val_err;
#endif
	/* SC SLM addon end */

	/*
	 * temp vars to keep results until they can be copied back to handle
	 * after we reacquire the queue lock
	 */
	int	wstat = 0;
	pid_t	pid;
	int	i, err_res = 0;

	/*
	 * save pointers to args to have them when we release the lock
	 */
	char	*cmd = handle->cmd;
	char	**arg = handle->arg;
	char	**env = handle->env;

	/*
	 * Store flags in booleans for convenience
	 */
	boolean_t capture_output = (handle->flags & FE_CAPTURE_OUTPUT);
	boolean_t syslog_output = (handle->flags & FE_SYSLOG_OUTPUT);
	boolean_t produce_core = (handle->flags & FE_PRODUCE_CORE);
	boolean_t is_async = (handle->flags & FE_ASYNC);
	boolean_t val_proj_name = (handle->flags & FE_VALIDATE_PROJ_NAME);

	/* define the fds for the pipes */
	int pfd[2] = {0, 0};	/* static initialization to keep lint happy */

	/* FE_ASYNC overrides other flags */
	if (is_async) {
		capture_output = syslog_output = produce_core = B_FALSE;
	}

#if SOL_VERSION >= __s10
	if ((zoneid = getzoneidbyname(handle->zonename)) == -1) {
		err = errno;
		result->type = FE_SYSERRNO;
		result->sys_errno = errno;
		log_error(handle->tag, err, "getzoneidbyname:");
		return;
	}
	if (zoneid != GLOBAL_ZONEID)
		enter_local_zone = B_TRUE;
#endif

#ifdef SC_SRM
	/*
	 * If the client requested that fed should validate
	 * the project name, then check whether the project
	 * name is valid or not in this zone's context (i.e after
	 * zone_enter() has been called). zone_enter() will be called
	 * by validate_project_on_zone() to set the appropriate
	 * context. If the project name is not valid, then just
	 * return. There is no need to perform method execution
	 * in such a case.
	 */
	if (val_proj_name) {
		proj_val_err = validate_project_on_zone(handle,
		    enter_local_zone, &err, &err_msg, produce_core);

		if (proj_val_err != FE_OKAY) {
			/*
			 * There was an error validating the project name.
			 * We will return in such a case. If the project
			 * name was invalid, the value of proj_val_err
			 * will be FE_PROJECT_NAME_INVALID
			 */
			result->type = proj_val_err;
			result->sys_errno = err;
			if (err_msg != NULL) {
				log_error(handle->tag, err, err_msg);
				free(err_msg);
			}
			return;
		}
	}
#endif
#if SOL_VERSION >= __s10
	if (enter_local_zone &&
	    (tmpl_fd = init_template(&err, &err_msg)) == -1) {
		result->type = FE_SYSERRNO;
		result->sys_errno = err;
		log_error(handle->tag, err, err_msg);
		free(err_msg);
		return;
	}
#endif

	/* If we want to capture output, create a pipe */
	if (capture_output) {
		if (debug)
			dbg_msgout(NOGET("creating pipe\n"));
		if (pipe(pfd) != 0) {
			/*
			 * Suppress the lint info.
			 * lint doesn't understand errno.
			 */
			/* CSTYLED */
			err = errno; /*lint !e746 */
			log_error(handle->tag, err, "pipe");
			/*
			 * Turn off the capture mode and continue.
			 * We can go ahead and run the method normally,
			 * we just can't capture the output.
			 */
			capture_output = B_FALSE;
		}
	}

	/* SC SLM addon start */
#ifdef SC_SRM

#if SOL_VERSION < __s10
	if (handle->rg_slm_pset_type != NULL &&
	    strcmp(SCHA_SLM_PSET_TYPE_DEDICATED_WEAK,
	    handle->rg_slm_pset_type) == 0 ||
	    strcmp(SCHA_SLM_PSET_TYPE_DEDICATED_STRONG,
	    handle->rg_slm_pset_type) == 0) {
		/*
		 * This normaly does not occur, as rgm SLM properties
		 * will not allow this. It is here in case of use of
		 * feadm, that has no properties control, input parameters
		 * are fakes. feadm is not delivered, it is a debug tool.
		 */
		svc_free((void *)handle->rg_slm_pset_type, &fe_alloc_cnt);
		handle->rg_slm_pset_type = svc_strdup(
		    RG_SLM_PSET_TYPE_DEFAULT, &fe_alloc_cnt);
	}
#endif  /* SOL_VERSION < __s10 */

	/* queue lock is hold at this point */

	/*
	 * slm_flags == METH_FEDSLM_BYPASS is bypass fed SLM handling
	 */
	if (handle->slm_flags != METH_FEDSLM_BYPASS) {
		/*
		 * SLM handling of ssm_wrapper case
		 */
		if (handle->ssm_wrapper_zone != NULL &&
		    handle->ssm_wrapper_zone[0] != '\0' &&
		    strcmp(handle->ssm_wrapper_zone, GLOBAL_ZONENAME) != 0) {
			char **arg = handle->arg;
			/*
			 * Enter scslm_fe_run() with the
			 * zone asked by ssm_wrapper
			 */
			ssm_wrapper = handle->zonename;
			handle->zonename = handle->ssm_wrapper_zone;
			/*
			 * Find ssm_wrapper -P option argument (the project)
			 */
			while (*arg != NULL) {
				if (strcmp(*arg, "-P") == 0) {
					ssm_wrapper_project = arg + 1;
				}
				arg++;
			}
		}
		ret = scslm_fe_run(handle);
		if (ssm_wrapper != NULL) {
			/* reset zonename to original value */
			handle->zonename = ssm_wrapper;
		}
		if (ret != SCSLM_SUCCESS) {
			result->type = FE_SLM_ERROR;
			result->sys_errno = ret;
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * Resource group is under SC SLM control and an SLM
			 * error occurred. Check fed SLM errors for more
			 * details.
			 * @user_action
			 * Move RG_SLM_type to manual and restart the resource
			 * group.
			 */
			(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
			    "scslm_fe_run: FE_SLM_ERROR %d", ret);
			sc_syslog_msg_done(&sys_handle);
			/* exit holding the queue lock */
			return;
		}
		if (ssm_wrapper != NULL &&
		    *ssm_wrapper_project != NULL &&
		    handle->scslm_proj_name != NULL) {
			/*
			 * under SLM control,
			 * change -P project argument to SLM generated project
			 */
			svc_free(*ssm_wrapper_project, &fe_alloc_cnt);
			*ssm_wrapper_project = svc_strdup(
			    handle->scslm_proj_name, &fe_alloc_cnt);
			if (*ssm_wrapper_project == NULL) {
				result->type = FE_SYSERRNO;
				result->sys_errno = ENOMEM;
				/* exit holding the queue lock */
				return;
			}
			slm_ssm_wrapper = B_TRUE;
		}
	}
#endif	/* SC_SRM */
	/* SC SLM addon end */

	/*
	 * release queue lock acquired by calling function
	 */
	UNLOCK_QUEUE();

	/*
	 * Note that we use fork1 instead of fork to duplicate
	 * only the calling lwp.  Because we link with pthreads, this behavior
	 * would be the same with fork, but we use fork1 for explicitness.
	 */
#ifndef linux
/* SC SLM addon start */
#ifdef SC_SRM
	/*
	 * scslm_fe_run is called with queue lock already hold (creates if
	 * needed SLM project, pool and pset, etc). scslm_fe_stop() (SLM
	 * cleanup after a stop method executed) is called with queue lock
	 * already hold.
	 * scslm_update (RG SLM properties updates handling) uses internaly
	 * queue lock, hold at function entry and released at function return
	 * to protect a memory aera used for RGs under SLM control handling and
	 * also provide some nedded serialization.
	 * These functions may call popen() (vfork) to perform Solaris project
	 * creation and modification through projadd projmod binaries
	 * (there is no Solaris API to modify projects, also there is
	 * 6234796 "projadd, projmod, projdel: concurrency problems"). To avoid
	 * the classic thread/lock/fork deadlock, we hold queue lock before
	 * fork1 and release it just after. Not doing this shows a deadlock
	 * when for example 8 RGs are 'scwitch -Z' in parallel on a 8 processors
	 * node. queue lock hold time is negligible when there are no RG
	 * under SLM control. queue lock hold time is low when there are RG
	 * under SLM control (SLM projects for RG under SLM control are only
	 * created once, the first time a method of a SLM RG is run on a node).
	 * We don't use pthread_atfork as we only have one lock to handle.
	 */
	LOCK_QUEUE();
#endif
/* SC SLM addon end */
	pid = fork1();
/* SC SLM addon start */
#ifdef SC_SRM
	UNLOCK_QUEUE();
#endif
/* SC SLM addon end */
	if (pid != 0)
#else
	if ((pid = fork()) != 0)
#endif
	{
		if (debug)
			dbg_msgout(NOGET("after fork in parent\n"));

		/*
		 * Parent
		 */
		LOCK_QUEUE();
		handle->pid = pid;

		if (pid == -1) {
			/*
			 * Suppress the lint info.
			 * lint doesn't understand errno.
			 */
			/* CSTYLED */
			err = errno; /*lint !e746 */
			result->type = FE_SYSERRNO;
			result->sys_errno = errno;
			log_error(handle->tag, err, "fork");
#if SOL_VERSION >= __s10
			if (enter_local_zone) {
				/*
				 * suppress the lint error because
				 * we know tmpl_fd has been initialized
				 */
				(void) ct_tmpl_clear(tmpl_fd); /*lint !e644 */
				(void) close(tmpl_fd);
			}
#endif
			return;
		}

		UNLOCK_QUEUE();

	} else {
		/*
		 * Child
		 *
		 * Note that between the fork1 and exec, we call
		 * dbg_msgout_nosyslog, because the parent process may
		 * have had a thread in syslog at the time of the fork,
		 * which would keep the lock.
		 *
		 * This case is only likely to show up in debug mode, so
		 * it's ok to syslog genuine error messages.
		 */

		/*
		 * Set close on exec so open files are not inherited.
		 * Ignore case when file is not open.
		 */
#if SOL_VERSION >= __s10
		if (enter_local_zone)
			(void) ct_tmpl_clear(tmpl_fd);
#endif

		if (debug)
			dbg_msgout_nosyslog(NOGET("after fork in child\n"));

		for (i = getdtablesize() - 1; i >= 3; i--) {
			if (fcntl(i, F_SETFD, FD_CLOEXEC) == -1) {
				if (errno != EBADF) {
					err = errno;
					result->sys_errno = errno;
					(void) sc_syslog_msg_initialize(
					    &sys_handle,
					    SC_SYSLOG_RGM_FED_TAG, "");
					/*
					 * SCMSGS
					 * @explanation
					 * A server (rpc.pmfd or rpc.fed) was
					 * not able to execute the action
					 * shown, and the process associated
					 * with the tag is not started. The
					 * error message is shown.
					 * @user_action
					 * Save the /var/adm/messages file.
					 * Contact your authorized Sun service
					 * provider to determine whether a
					 * workaround or patch is available.
					 */
					(void) sc_syslog_msg_log(handle,
					    LOG_ERR, MESSAGE,
					    "fcntl: %s", strerror(err));
					sc_syslog_msg_done(&sys_handle);
				}
			}
		}

		/* set default behaviour in case fed modified it. */
		(void) sigset(SIGCLD, SIG_DFL);

		/*
		 * If we want to capture output, redirect stdout and
		 * stderr to the pipe and close the read side of the pipe.
		 */
		if (capture_output) {
			if (debug)
				dbg_msgout_nosyslog(NOGET(
				    "redirecting stderr,stdout\n"));
			if (dup2(pfd[1], STDOUT_FILENO) == -1 ||
			    dup2(pfd[1], STDERR_FILENO) == -1) {
				err = errno;
				(void) sc_syslog_msg_initialize(&sys_handle,
				    SC_SYSLOG_RGM_FED_TAG, "");
				/*
				 * SCMSGS
				 * @explanation
				 * Need explanation of this message!
				 * @user_action
				 * Need a user action for this message.
				 */
				(void) sc_syslog_msg_log(sys_handle, LOG_ERR,
				    MESSAGE, "dup2: %s", strerror(err));
				sc_syslog_msg_done(&sys_handle);

				/*
				 * error:
				 * don't capture output; close write pipe.
				 * It's too late to turn off the flag because
				 * the parent may already be waiting.
				 * Close the pipe so that the parent will get
				 * EOF out of the read call.
				 */
				(void) close(pfd[1]);
			}
			/*
			 * Close the read pipe.
			 */
			(void) close(pfd[0]);

			/*
			 * Ignore SIGPIPE so that we don't terminate
			 * the method if something goes wrong.
			 */
			(void) signal(SIGPIPE, SIG_IGN);
		}


		/*
		 * Make sure we're not running children as realtime
		 * processes.  We restore the scheduling parameters
		 * that rpc.fed was started with ... by default TS.
		 *
		 * In debug mode we skip this because we also skipped
		 * setting the priority to RT in svc_init() in fed_main.c.
		 * Both functions are in libscutils.
		 */
		if (!debug && svc_restore_priority() == 1) {
			err = errno;
			result->sys_errno = errno;
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * The rpc.pmfd or rpc.fed server was not able to run
			 * the application in the correct scheduling mode, and
			 * the system error is shown. An error message is
			 * output to syslog.
			 * @user_action
			 * Save the /var/adm/messages file. Contact your
			 * authorized Sun service provider to determine
			 * whether a workaround or patch is available.
			 */
			(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
			    "svc_restore_priority: %s", strerror(err));
			sc_syslog_msg_done(&sys_handle);
			exit(result->sys_errno);
		}
#if SOL_VERSION >= __s10
		if (enter_local_zone && zone_enter(zoneid) != 0) {
			err = errno;
			result->sys_errno = errno;
			log_error(handle->tag, err, "zone_enter");
			exit(result->sys_errno);
		}
#endif

		/*
		 * Set the uid and group id of the process.
		 */
		if ((handle->gid != 0) && (setgid(handle->gid) == -1)) {
			result->sys_errno = errno;
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * The rpc.pmfd server was not able to set the group
			 * id of a process. The message contains the system
			 * error. The server does not perform the action
			 * requested by the client, and an error message is
			 * output to syslog.
			 * @user_action
			 * Save the /var/adm/messages file. Contact your
			 * authorized Sun service provider to determine
			 * whether a workaround or patch is available.
			 */
			(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
			    "setgid: %s", strerror(result->sys_errno));
			sc_syslog_msg_done(&sys_handle);
			exit(result->sys_errno);
		}

		if ((handle->uid != 0) && (setuid(handle->uid) == -1)) {
			result->sys_errno = errno;
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * The rpc.pmfd server was not able to set the user id
			 * of a process. The message contains the system
			 * error. The server does not perform the action
			 * requested by the client, and an error message is
			 * output to syslog.
			 * @user_action
			 * Save the /var/adm/messages file. Contact your
			 * authorized Sun service provider to determine
			 * whether a workaround or patch is available.
			 */
			(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
			    "setuid: %s", strerror(result->sys_errno));
			sc_syslog_msg_done(&sys_handle);
			exit(result->sys_errno);
		}

		/*
		 * Make this process a session leader
		 * so when it gets a signal all its children
		 * are also killed.
		 */
		if ((handle->gpid = setsid()) == -1) {
			result->sys_errno = errno;
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
			    "setsid: %s", strerror(result->sys_errno));
			sc_syslog_msg_done(&sys_handle);
			exit(result->sys_errno);
		}
#ifndef linux
		/*
		 * Move into a new process group; the zone_enter will have
		 * placed us into zsched's session, and we want to be in
		 * a unique process group.
		 */
		(void) setpgid(getpid(), getpid());

		/*
		 * Change  coredump settings to be used in case of timeouts to
		 * dump the core of whole process tree.
		 */
		if (produce_core)
			modify_coreadm_settings(handle);
#endif
		if (debug)
			dbg_msgout_nosyslog(NOGET(
			    "after setpgrp gpid=%d uid=%d gid=%d\n"),
			    handle->gpid, handle->uid, handle->gid);
#ifdef SC_SRM
		/*
		 * Set project name before execute the method if project name is
		 * specified or resource is under SLM control.
		 */
		if ((err = fed_setproject(handle, slm_ssm_wrapper)) != 0) {
			exit(err);
		}

#endif
		/*
		 * this is the crux of the function - execute the method
		 */
		if (execve(cmd, arg, env) == -1) {
			result->sys_errno = errno;
			log_error(handle->tag, result->sys_errno, "execve");

			if (debug)
				dbg_msgout_nosyslog(NOGET(
				    "in child errno=%d\nresult->type=%d\n"
				    "result->sys_errno=%d\n"),
				    errno, result->type, result->sys_errno);

			_exit(result->sys_errno);
		}

		/* NOTREACHED if execve works */
	}

	/* parent again */
#if SOL_VERSION >= __s10
	if (enter_local_zone) {
		(void) ct_tmpl_clear(tmpl_fd);
		(void) close(tmpl_fd);
	}
#endif

	/* initialize the result error message */
	result->output = NULL;

	/*
	 * If the async flag is set, return here.  Of course, there will
	 * be no meaningful exit status returned.
	 */
	if (is_async) {
		if (debug)
			dbg_msgout(NOGET(
			    "in parent after fork, return early for async\n"));
		return;
	}

	/*
	 * If we're supposed to capture the output from the child,
	 * the child will have redirected its stderr and stdout to the
	 * pipe, so that we can read the output into our buffer.
	 * Call the helper function read_method_output to do the work.
	 */
	if (capture_output) {
		/* close the write pipe */
		(void) close(pfd[1]);

		/* Call the helper read function */
		read_method_output(result, syslog_output, pfd, handle);
	}

	/*
	 * Now we can simply wait to cleanup the zombie from our main process,
	 * or until the timeout thread tells us that the process we're
	 * waiting for is unkillable.
	 */
	while (!is_unkillable_process(handle)) {
		w = waitpid(pid, &wstat, 0);
		/* save errno for use below */
		err_res = errno;
		if (w == 0 || err_res != EINTR) {
			break;
		}
	}


	/*
	 * Now that the process has terminated, close the read pipe.
	 * We don't want to close it earlier in case the process
	 * tries to write after we've closed it, and thus generates a
	 * SIGPIPE for itself.
	 *
	 * If the process is unkillable, it won't yet have terminated,
	 * but since it's stuck in the kernel, it won't be trying to write
	 * to this pipe anymore.
	 */
	if (capture_output) {
		(void) close(pfd[0]);
	}

	if (debug)
		dbg_msgout(NOGET(
		    "in parent after waitpid errno=%d\nresult->type=%d\n"
		    "result->sys_errno=%d\n"),
		    errno, result->type, result->sys_errno);

	/*
	 * update handle values
	 */
	if (debug)
		dbg_msgout(NOGET("before lock_queue\n"));
	LOCK_QUEUE();
	if (debug)
		dbg_msgout(NOGET("after lock_queue\n"));

	/*
	 * First, check if the process was unkillable.
	 */
	if (handle->unkillable_process) {
		result->type = FE_UNKILLABLE;
		dbg_msgout(NOGET("Unkillable process: pid=%d\n"), pid);
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * The rpc.fed server is not able to kill the process with a
		 * SIGKILL. This means the process is stuck in the kernel.
		 * @user_action
		 * Save the syslog messages file. Examine other syslog
		 * messages occurring around the same time on the same node,
		 * to see if the cause of the problem can be identified.
		 */
		(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
		    "tag %s: unable to kill process with SIGKILL",
		    handle->tag);
		sc_syslog_msg_done(&sys_handle);

		/*
		 * Exit holding the queue lock
		 */
		return;
	}

	/* The process was not unkillable */
	handle->history.wstat = wstat;
	if (debug) {
		dbg_msgout(NOGET("after lock pid=%d wstat=%d\n"), pid, wstat);
		dbg_msgout(NOGET("WIFEXITED(wstat)=%d\n"), WIFEXITED(wstat));
		dbg_msgout(NOGET("WEXITSTATUS(wstat)=%d\n"),
			WEXITSTATUS((uint_t)wstat));
	}

	/*
	 * get the value returned by the child process executed
	 * don't interpret the resulting error code; the client will do that
	 */
	result->method_retcode = (uint_t)wstat;
	if (handle->end_flag) {
		if (debug)
			dbg_msgout(NOGET("%s: forced kill\n"), handle->tag);
		result->type = FE_QUIESCED;
	} else if (handle->history.sent_sig_stop ||
		    handle->history.sent_sig_kill) {
			if (debug)
				dbg_msgout(NOGET("tag %s: timeout expired\n"),
				    handle->tag);
			result->type = FE_TIMEDOUT;
	} else if (w == -1) {
		result->type = FE_SYSERRNO;
		result->sys_errno = err_res;
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * The rpc.pmfd or rpc.fed server was not able to wait for a
		 * process. The message contains the system error. The server
		 * does not perform the action requested by the client, and an
		 * error message is output to syslog.
		 * @user_action
		 * Save the /var/adm/messages file. Contact your authorized
		 * Sun service provider to determine whether a workaround or
		 * patch is available.
		 */
		(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
		    "%s:waitpid. %s", handle->tag, strerror(result->sys_errno));
		sc_syslog_msg_done(&sys_handle);
	}

	/* SC SLM addon start */
#ifdef SC_SRM
	/*
	 * slm_stop() is used to make some SLM cleanup, like, on Solaris 10,
	 * delete a pset if needed, etc.
	 */
	/* queue lock is hold at this point */
	ret = scslm_stop(handle);
	if (ret != SCSLM_SUCCESS) {
		result->type = FE_SLM_ERROR;
		result->sys_errno = ret;
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Resource group is under SC SLM control and an SLM error
		 * occurred. Check fed SLM errors for more details.
		 * @user_action
		 * Move RG_SLM_type to manual and restart the resource group.
		 */
		(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
		    "%s:scslm_stop. FE_SLM_ERROR %d", handle->tag, ret);
		sc_syslog_msg_done(&sys_handle);
	}
#endif
	/* SC SLM addon end */

	/* exit holding the queue lock */
}

/*
 * is_unkillable_process
 * ---------------------
 * Usage:
 *     if (is_unkillable_process(handle)) { break; }
 *
 * Helper function hiding the locking, so that the unkillable_process
 * flag can be checked in a single statement.
 */
static bool_t is_unkillable_process(fe_handle *handle)
{
	LOCK_QUEUE();
	if (handle->unkillable_process) {
		UNLOCK_QUEUE();
		return (TRUE);
	}
	UNLOCK_QUEUE();
	return (FALSE);
}

/*
 * read_method_output
 * -------------------
 * Usage: read_method_output(result_handle, syslog_output, pipe_arr, handle);
 *
 * Expects pfd to be a pipe, set up so that we can read from the read half.
 * tag should not be NULL.  result should not be NULL.  syslog_output is
 * a boolean value: B_TRUE or B_FALSE.
 *
 * This function reads from the read half of the pipe until EOF is read,
 * storing the text in a temporary buffer.  After EOF is read (read returns -1),
 * prefix_line_tags is called to copy the output to result->output.
 * Memory is properly allocated.  If something goes wrong, or if there
 * is no output, result->output will remain NULL.  Otherwise result->output
 * will contain the output, with each line prefixed with the line tag.
 *
 * If syslog_output is B_TRUE (non-zero), each line will be written to the
 * syslog at LOG_NOTICE level.
 *
 * When this function returns, it leaves the read half of the pipe open.
 * We don't want to close it until the writing process terminates, in case it
 * writes after we close it (in which case it would get a SIGPIPE).
 */
static void read_method_output(fe_run_result *result, boolean_t syslog_output,
    int pfd[2], fe_handle *handle)
{
	sc_syslog_msg_handle_t sys_handle;

	/* declare the buffer for reading from the pipe */
	char read_buf[READ_BUF_SIZE];
	int bytes_read = 0; /* the amt read in each call to read */

	/* declare a char * in which to store the captured output */
	char *output = NULL;
	char *temp_ptr = NULL;
	int total_allocated = 0; /* keep track of total allocation size */
	int total_written = 0; /* keep track of logical size of buffer */

	int err;

	/* allocate our initial output buffer */
	total_allocated = INITIAL_ALLOC_SIZE;
	output = (char *)malloc((size_t)total_allocated);
	if (output == NULL) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * The rpc.fed server was not able to allocate memory
		 * necessary to capture the output from methods it runs.
		 * @user_action
		 * Determine if the host is running out of memory. If not save
		 * the /var/adm/messages file. whether a workaround or patch
		 * is available.
		 */
		(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
		    "low memory: unable to capture output");
		sc_syslog_msg_done(&sys_handle);

		return;
	}

	if (debug)
		dbg_msgout(NOGET("About to start reading from pipe\n"));

	/* read from the read pipe */
	while ((bytes_read = read(pfd[0], read_buf, READ_BUF_SIZE)) != 0) {
		if (bytes_read == -1) {
		    /* error condition */
			err = errno;
			if (err == EINTR) {
				/*
				 * A signal interrupted us;
				 * check if we have encountered an
				 * unkillable process. If so, break.
				 * Otherwise, continue.
				 */
				if (is_unkillable_process(handle)) {
					if (debug)
						dbg_msgout(NOGET(
						    "EINTR: exiting"
						    " read loop due to "
						    "unkillable process\n"));
					break;
				}
				continue;
			} else {
				/* any other error: break out */
				(void) sc_syslog_msg_initialize(
				    &sys_handle, SC_SYSLOG_RGM_FED_TAG,
				    "");
				(void) sc_syslog_msg_log(
				    sys_handle, LOG_ERR, MESSAGE,
				    "read: %s", strerror(err));
				sc_syslog_msg_done(&sys_handle);

				/* break out of the loop */
				break;
			}
		}

		/*
		 * Check if we exceed our physical size, always
		 * leaving one extra byte for the NULL char.
		 */
		if (total_written + bytes_read >= total_allocated) {
			/* grow the buffer exponentially */
			total_allocated *= 2;
			temp_ptr = (char *)realloc((void *)output,
			    (size_t)total_allocated);
			/* Make sure we really reallocated */
			if (temp_ptr == NULL) {
				(void) sc_syslog_msg_initialize(
				    &sys_handle, SC_SYSLOG_RGM_FED_TAG,
				    "");
				(void) sc_syslog_msg_log(
				    sys_handle, LOG_ERR, MESSAGE,
				    "Error: low memory");
				sc_syslog_msg_done(&sys_handle);

				/* break out of the loop with what we've got */
				break;
			}
			/* If we reallocated properly, assign ptr to output */
			output = temp_ptr;
		}
		/*
		 * Now copy the bytes we just read.  Can't use str* functions
		 * because neither src or dest string has \0.
		 *
		 * Suppress the lint message about
		 * "Possible access beyond array for function memcpy"
		 * All the parameters to memcpy change in the dynamic execution
		 * of the program, so lint's static analyis is wrong here.
		 */
		(void) memcpy(output + total_written, read_buf, /* CSTYLED */
		    (size_t)bytes_read); /*lint !e670 */
		total_written += bytes_read;

		/*
		 * Check if the timeout thread has marked the process as
		 * unkillable.  We check here in case the timeout thread
		 * set the flag and sent the signal to us while we were
		 * not in the read system call.
		 */
		if (is_unkillable_process(handle)) {
			if (debug)
				dbg_msgout(NOGET("exiting read loop: "
				    "unkillable process\n"));
			break;
		}
	} /* read loop */

	if (debug)
		dbg_msgout(NOGET("done reading\n"));

	/*
	 * Append the NULL char so prefix_line_tags can use str* functions.
	 * Suppress the lint message about "output conceivably not
	 * initialized."
	 * Lint doesn't know what it's talking about here; output is
	 * initialized above the while loop from the call to malloc.
	 *
	 * Also suppress the message about accessing an array out of bounds.
	 * Lint is again confused.
	 */
	/* CSTYLED */
	/*lint -e771 -e661 */
	output[total_written] = '\0';
	/* CSTYLED */
	/*lint +e771 +e661 */

	/*
	 * Even if we had an error, we may have received some output, so
	 * go ahead and copy it over UNLESS we have zero length output.
	 */
	if (output[0] != '\0') {
		/* prefix the tags on each line */
		prefix_line_tags(output, &(result->output), syslog_output,
		    handle->tag);

		/*
		 * If result->output is NULL, it means we're short on memory.
		 * Just use the captured output without prefixing line tags.
		 * initialize output to NULL, so that we don't free the string
		 */
		if (result->output == NULL) {
			result->output = output;
			output = NULL;
		}
	}
	free(output);
}


/*
 * prefix_line_tags
 * -------------------
 * Usage:
 * prefix_line_tags(captured_output, &output_to_return, syslog_output,
 *     syslog_tag);
 *
 * Allocate memory in *final_output to hold all the text from captured_output,
 * plus a linetag at the beginning of each line consisting of:
 *	nodename -
 *
 * Copies the string over, prefixing the line tag on each line.
 * Does not free captured_output.
 *
 * Expects captured_output to be a NULL-terminated string.  *final_output
 * should be NULL (it should not point to memory, because the memory will
 * not be freed). syslog_output is a boolean_t value.
 */
static void prefix_line_tags(char *captured_output, char **final_output,
    boolean_t syslog_output, char *syslog_tag)
{
	/* line_tag format: 'hostname[:zonename] - ' */
#if SOL_VERSION >= __s10
	char		line_tag[SYS_NMLN + 1 + (ZONENAME_MAX - 1) + 3];
#else
	char		line_tag[SYS_NMLN + 3];
#endif

	sc_syslog_msg_handle_t sys_handle;
	char		*temp_ptr = NULL;
	uint_t		n_lines = 0;
	size_t		total_length;
	char		*lasts;
	int		zonenamelen;
	boolean_t	do_prefixing = B_TRUE;

	(void) sprintf(line_tag, "%s", hostname);
#if SOL_VERSION >= __s10
	/*
	 * verify if zonename component is present.
	 * assuming tag is of the form [zonename.]rgname.rname.methnum
	 */
	temp_ptr = strchr(syslog_tag, '.');
	if (temp_ptr != NULL) {
		temp_ptr = strchr(temp_ptr + 1, '.');
		if (temp_ptr != NULL) {
			temp_ptr = strchr(temp_ptr + 1, '.');
		}
	}
	if (temp_ptr != NULL) {
		/* zonename exists in the tag. append zonename */
		temp_ptr = strchr(syslog_tag, '.');

		zonenamelen = temp_ptr - syslog_tag + 1;
				/* + 1 for ":" */

		(void) snprintf(line_tag + strlen(line_tag), zonenamelen + 1,
		    ":%s", syslog_tag);
	}
#endif
	strcat(line_tag, " - ");

	/*
	 * Count the number of lines, which is the number of
	 * newlines plus one (except if the last char is a \n,
	 * in which case we allocate a little extra space)
	 */
	n_lines = 1;
	temp_ptr = strchr(captured_output, '\n');
	while (temp_ptr) {
		temp_ptr = strchr(temp_ptr + 1, '\n');
		n_lines++;
	}

	/*
	 * The total length is the length of all the output
	 * plus the length of n_lines line tags plus 1 for
	 * the null char, plus 1 for an extra newline at
	 * the very end.
	 */
	total_length = strlen(line_tag) * n_lines
		+ strlen(captured_output) + 2;

	/* allocate our final result buffer */
	*final_output = (char *)malloc(total_length);
	if (*final_output == NULL) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		(void) sc_syslog_msg_log(sys_handle, LOG_ERR,
		    MESSAGE, "low memory: unable to capture output");
		sc_syslog_msg_done(&sys_handle);

		do_prefixing = B_FALSE;
		/* don't return yet; we may still be able to do syslogging */
	} else {
		/* Make it a string so strcat will work */
		*final_output[0] = '\0';
	}
	if (!syslog_output && !do_prefixing) {
		return; /* nothing to do */
	}

	/*
	 * Use a strtok loop to extract each line, copy it over prefixing
	 * the line_tag.
	 * Prime with the initial line
	 */
	temp_ptr = strtok_r(captured_output, "\n", &lasts);
	while (temp_ptr) {
		/* First, write the line to syslog if we're supposed to */
		if (syslog_output) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * The tag specified that is being run under the
			 * rpc.fed produced the specified message.
			 * @user_action
			 * This message is for informational purposes only. No
			 * user action is necessary.
			 */
			(void) sc_syslog_msg_log(sys_handle, LOG_NOTICE,
			    MESSAGE, "tag %s: %s", syslog_tag, temp_ptr);
			sc_syslog_msg_done(&sys_handle);
		}
		if (do_prefixing) {
			/*
			 * concatenate the new line onto the
			 * end of the real output string,
			 * prefixed with the line_tag
			 */
			(void) strcat(*final_output, line_tag);
			(void) strcat(*final_output, temp_ptr);
			/* strtok_r replaces \n, so add it back */
			(void) strcat(*final_output, "\n");
		}
		/* get the next line */
		temp_ptr = strtok_r(NULL, "\n", &lasts);
	}
}

/*
 * Routines for allocating and freeing request handles.
 * The handles are organized in a circular queue, which is traversed
 * starting at the base, until we reach the base again.
 */
static fe_handle *
fe_alloc_handle(fe_run_args *args, fe_run_result *result,
	security_cred_t *cred)
{
	fe_handle *new_hdl = NULL;
	uint_t i;

	/*
	 * Check to make sure the identifier used is unique
	 */
	if (fe_handle_base != NULL) {
		for (new_hdl = fe_handle_base; ; new_hdl = new_hdl->next) {
			if (strcmp(new_hdl->tag, args->tag) == 0) {
				/*
				 * we have a duplicate request
				 */
				result->type = FE_DUP;

				return (NULL);
			}
			if (new_hdl->next == fe_handle_base)
				break;
		}
	}

	new_hdl = (fe_handle *) svc_alloc(sizeof (fe_handle), &fe_alloc_cnt);

	if (new_hdl == NULL) {
		result->type = FE_SYSERRNO;
		result->sys_errno = ENOMEM;
		return (NULL);
	}

	(void) memset(new_hdl, 0, sizeof (fe_handle));

	/*
	 * Create our ring queue - initial or otherwise
	 */
	if (fe_handle_base) {
		new_hdl->next = fe_handle_base;
		new_hdl->prev = fe_handle_base->prev;
		new_hdl->prev->next = new_hdl;
		fe_handle_base->prev = new_hdl;
		fe_handle_base = new_hdl;
	} else {
		fe_handle_base = new_hdl;
		new_hdl->next = new_hdl;
		new_hdl->prev = new_hdl;
	}

	/*
	 * Duplicate the argument list, making sure that the last element
	 * is NULL, not just empty, for execve().
	 */
	new_hdl->arg = svc_alloc(args->arg.arg_len * sizeof (char *),
		&fe_alloc_cnt);
	if (new_hdl->arg == NULL)
		goto cleanup_handle;
	(void) memset(new_hdl->arg, 0, sizeof (new_hdl->arg));
	for (i = 0; i < args->arg.arg_len; i++) {
		if (*args->arg.arg_val[i]) {
			new_hdl->arg[i] = svc_strdup(args->arg.arg_val[i],
				&fe_alloc_cnt);
			if (new_hdl->arg[i] == NULL)
				goto cleanup_handle;
		} else {
			new_hdl->arg[i] = NULL;
		}
	}

	/*
	 * Duplicate the env list, making sure that the last element
	 * is NULL, not just empty, for execve().
	 */
	new_hdl->env = svc_alloc(args->env.env_len * sizeof (char *),
		&fe_alloc_cnt);
	if (new_hdl->env == NULL)
		goto cleanup_handle;
	(void) memset(new_hdl->env, 0, sizeof (new_hdl->env));

	for (i = 0; i < args->env.env_len; i++) {
		if (*args->env.env_val[i]) {
			new_hdl->env[i] = svc_strdup(args->env.env_val[i],
				&fe_alloc_cnt);
			if (new_hdl->env[i] == NULL)
				goto cleanup_handle;
		} else {
			new_hdl->env[i] = NULL;
		}
	}

	/*
	 * Stuff the remaining arguments into our handle.
	 */
	new_hdl->tag		= svc_strdup(args->tag, &fe_alloc_cnt);
	if (new_hdl->tag == NULL)
		goto cleanup_handle;
	new_hdl->cmd		= svc_strdup(args->cmd, &fe_alloc_cnt);
	if (new_hdl->cmd == NULL)
		goto cleanup_handle;
	if (args->project_name != NULL) {
		new_hdl->project_name = svc_strdup(args->project_name,
		    &fe_alloc_cnt);
		if (new_hdl->project_name == NULL)
			goto cleanup_handle;
	} else
		new_hdl->project_name = NULL;

	/* SC SLM addon start */
#ifdef SC_SRM
	if (args->ssm_wrapper_zone != NULL) {
		new_hdl->ssm_wrapper_zone =
		    svc_strdup(args->ssm_wrapper_zone, &fe_alloc_cnt);
		if (new_hdl->ssm_wrapper_zone == NULL) {
			goto cleanup_handle;
		}
	} else {
		new_hdl->ssm_wrapper_zone = NULL;
	}

	if (args->rg_name != NULL) {
		new_hdl->rg_name = svc_strdup(args->rg_name, &fe_alloc_cnt);
		if (new_hdl->rg_name == NULL) {
			goto cleanup_handle;
		}
	} else {
		new_hdl->rg_name = NULL;
	}

	if (args->r_name != NULL) {
		new_hdl->r_name = svc_strdup(args->r_name, &fe_alloc_cnt);
		if (new_hdl->r_name == NULL) {
			goto cleanup_handle;
		}
	} else {
		new_hdl->r_name = NULL;
	}

	if (args->rg_slm_type != NULL) {
		new_hdl->rg_slm_type =
		    svc_strdup(args->rg_slm_type, &fe_alloc_cnt);
		if (new_hdl->rg_slm_type == NULL) {
			goto cleanup_handle;
		}
	} else {
		new_hdl->rg_slm_type = NULL;
	}

	if (args->rg_slm_pset_type != NULL) {
		new_hdl->rg_slm_pset_type =
		    svc_strdup(args->rg_slm_pset_type, &fe_alloc_cnt);
		if (new_hdl->rg_slm_pset_type == NULL) {
			goto cleanup_handle;
		}
	} else {
		new_hdl->rg_slm_pset_type = NULL;
	}

	new_hdl->rg_slm_cpu = args->rg_slm_cpu;
	new_hdl->rg_slm_cpu_min = args->rg_slm_cpu_min;
	new_hdl->slm_flags = args->slm_flags;
	new_hdl->do_slm_stop = 0;
	new_hdl->scslm_proj_name = NULL;
#endif
	/* SC SLM addon end */


	/*
	 * timeout is specified in seconds by the user, and
	 * is calculated using time_t (seconds).
	 */
	new_hdl->timeout = args->timeout;

	/*
	 * grace_period is specified in seconds by the user, and
	 * is calculated using time_t (seconds).
	 */
	new_hdl->grace_period = args->grace_period;
#if SOL_VERSION < __s10
	/* ensure that no code is modifying zonename in preS10 */
	ASSERT(!args->zonename || args->zonename[0] == '\0');
#endif

	if (args->zonename && args->zonename[0] != '\0') {
		new_hdl->zonename = svc_strdup(args->zonename, &fe_alloc_cnt);
	} else {
		new_hdl->zonename = svc_strdup(GLOBAL_ZONENAME, &fe_alloc_cnt);
		/* remains unused in pre s10 version. */
	}
	if (new_hdl->zonename == NULL)
		goto cleanup_handle;

	new_hdl->corepath = NULL;
	if (args->corepath != NULL) {
		new_hdl->corepath = svc_strdup(args->corepath, &fe_alloc_cnt);
		if (new_hdl->corepath == NULL)
			goto cleanup_handle;
	}

#if SOL_VERSION >= __s10
	new_hdl->zonepath = NULL;
	if (args->zonepath != NULL) {
		new_hdl->zonepath = svc_strdup(args->zonepath, &fe_alloc_cnt);
		if (new_hdl->zonepath == NULL)
			goto cleanup_handle;
	}
#endif


	/* copy flags */
	new_hdl->flags = args->flags;
	new_hdl->end_flag = FALSE;


	/*
	 * copy uid and gid from security credential
	 */
	new_hdl->uid = cred->aup_uid;
	new_hdl->gid = cred->aup_gid;

	/*
	 * fill in time history
	 */
	(void) memset(&new_hdl->history, 0, sizeof (fe_process_tag));
	new_hdl->history.wstat = 0;
	new_hdl->history.suspended = FALSE;
	new_hdl->history.sent_sig_stop = FALSE;
	new_hdl->history.sent_sig_kill = FALSE;
	new_hdl->history.last_resume_time = time(NULL);
	new_hdl->history.prev_exec_time = (time_t)0;
	new_hdl->history.stop_sig_time = (time_t)0;

	/*
	 * If flag is set, suspend timeout initially
	 */
	if (args->flags & FE_OPT_INITSUSPEND) {
		new_hdl->history.suspended = TRUE;
	}

	new_hdl->monitoring_thread = pthread_self();
	new_hdl->unkillable_process = FALSE;

	CL_PANIC(pthread_mutex_init(&handle_lock, NULL) == 0);

	return (new_hdl);

cleanup_handle:
	/*
	 * Something went wrong: free handle and return null.
	 * fe_alloc_handle is holding the lock, and that's OK because
	 * fe_free_handle assumes we're holding the lock.
	 */
	result->type = FE_SYSERRNO;
	result->sys_errno = ENOMEM;
	fe_free_handle(new_hdl);
	return (NULL);

}


/*
 * This has to be done while holding the lock.
 * We assume that the locking and unlocking is done in the
 * calling function (fe_run_process) before and after (respectively)
 * calling this function.
 */
static void
fe_free_handle(fe_handle *handle)
{
	char **ptr;

	/*
	 * Unlink us from the ring.
	 *
	 * Make sure we leave a valid handle in
	 * the base pointer.
	 */
	if (handle == fe_handle_base) {
		fe_handle_base = handle->next;
	}

	/*
	 * Take us out of the ring
	 */
	handle->next->prev = handle->prev;
	handle->prev->next = handle->next;

	/*
	 * Make sure nobody uses any left over garbage
	 */
	handle->prev = handle->next = NULL;

	/*
	 * If we were the only one left in the ring, then fe_handle_base
	 * is still pointing to us.  Null it out.
	 */
	if (handle == fe_handle_base) {
		fe_handle_base = NULL;
	}

	/*
	 * Now, free up any internal strings/structures
	 * that we previously malloc'd and free the
	 * handle.
	 */
	if (handle->arg) {
		ptr = handle->arg;
		while (*ptr)
			svc_free(*ptr++, &fe_alloc_cnt);
		svc_free(handle->arg, &fe_alloc_cnt);
	}
	if (handle->env) {
		ptr = handle->env;
		while (*ptr)
			svc_free(*ptr++, &fe_alloc_cnt);
		svc_free(handle->env, &fe_alloc_cnt);
	}
	if (handle->cmd)
		svc_free((void *)handle->cmd, &fe_alloc_cnt);
	if (handle->tag)
		svc_free((void *)handle->tag, &fe_alloc_cnt);
	if (handle->project_name)
		svc_free((void *)handle->project_name, &fe_alloc_cnt);
	if (handle->corepath)
		svc_free((void *)handle->corepath, &fe_alloc_cnt);
	if (handle->zonename)	/* always null in pre s10 */
		svc_free((void *)handle->zonename, &fe_alloc_cnt);
#if SOL_VERSION >= __s10
	if (handle->zonepath)
		svc_free((void *)handle->zonepath, &fe_alloc_cnt);
#endif

	/* SC SLM addon start */
#ifdef SC_SRM
	if (handle->ssm_wrapper_zone)
		svc_free((void *)handle->ssm_wrapper_zone, &fe_alloc_cnt);
	if (handle->rg_name)
		svc_free((void *)handle->rg_name, &fe_alloc_cnt);
	if (handle->r_name)
		svc_free((void *)handle->r_name, &fe_alloc_cnt);
	if (handle->rg_slm_type)
		svc_free((void *)handle->rg_slm_type, &fe_alloc_cnt);
	if (handle->rg_slm_pset_type)
		svc_free((void *)handle->rg_slm_pset_type, &fe_alloc_cnt);
	/*
	 * handle->scslm_proj_name must not be free, if not NULL, it is
	 * a string pointer to a data structure in the fed SLM.
	 */
#endif
	/* SC SLM addon end */

	/*
	 * destroy lock
	 */
	CL_PANIC(pthread_mutex_destroy(&handle_lock) == 0);

	/*
	 * Now free the handle
	 */
	svc_free((void *)handle, &fe_alloc_cnt);

	if (debug)
		dbg_msgout(NOGET(
		    "Number of mallocs for all clients is: %d\n"),
		    fe_alloc_cnt);

}

/*
 * What follows are our RPC entry points.
 *
 * Here's our "run" entry ... grab a new handle for the
 * request, and exec the process.
 */
void
fe_run_svc(fe_run_args *args, fe_run_result *result,
	security_cred_t *cred)
{
	fe_handle *handle;
	char **aptr;

	/*
	 * Make sure the queue doesn't change underneath us.
	 */
	LOCK_QUEUE(fe_alloc_handle);
	if (debug)
		dbg_msgout(NOGET("after lock_queue\n"));

	/*
	 * allocate handle for this process
	 */
	if ((handle = fe_alloc_handle(args, result, cred)) == NULL) {
		UNLOCK_QUEUE(fe_alloc_handle);
		return;
	}

	if (debug) {
		dbg_msgout(NOGET("starting\n"));
		for (aptr = handle->arg; aptr && *aptr; aptr++) {
			dbg_msgout(NOGET("\t%s \n"), *aptr);
		}
		dbg_msgout("\n");
	}

	/*
	 * update number of threads;
	 */
	fe_thread_cnt++;
	if (debug) {
		dbg_msgout(NOGET(
		    "Number of threads for all clients is: %d\n"),
		    fe_thread_cnt);
		dbg_msgout(NOGET(
		    "Number of mallocs for all clients is: %d\n"),
		    fe_alloc_cnt);
	}

	/*
	 * do actual work
	 * starts by holding the lock, releases it,
	 * reacquires it and returns holding it
	 */
	if (debug)
		dbg_msgout(NOGET("before fe_run_process\n"));
	fe_run_process(handle, result);
	if (debug)
		dbg_msgout(NOGET("after fe_run_process\n"));

	/*
	 * Do cleanup and return.
	 * Note, if FE_ASYNC flag was set, the child process is still
	 * running, but we will no longer monitor it.
	 */
	fe_free_handle(handle);
	if (debug)
		dbg_msgout(NOGET("after fe_free_handle\n"));
	UNLOCK_QUEUE(fe_alloc_handle);
	if (debug)
		dbg_msgout(NOGET("after unlock_queue\n"));

	/*
	 * decrease number of threads;
	 */
	fe_thread_cnt--;
	if (debug)
		dbg_msgout(NOGET(
		    "Number of threads for all clients is: %d\n"),
		    fe_thread_cnt);

}


void
fe_suspend_svc(fe_args *args, fe_result *result,
	security_cred_t *cred)
{

	fe_handle	*handle;
	sc_syslog_msg_handle_t sys_handle;

	if (debug)
		dbg_msgout(NOGET("in fe_suspend_svc\n"));

	/*
	 * get handle corresponding to tag name
	 */
	LOCK_QUEUE(fe_handle_base);
	if ((handle = id_to_handle(args->tag)) == NULL) {
		result->type = FE_NOTAG;
		UNLOCK_QUEUE(fe_handle_base);
		return;
	}

	/*
	 * check that tag belongs to user
	 * or if user is root match any tag
	 */
	if (!((cred->aup_uid == 0) ||
	    ((handle->uid == cred->aup_uid) &&
	    (handle->gid == cred->aup_gid)))) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * The user sent a suspend/resume command to the rpc.fed
		 * server for a tag that was started by a different user. An
		 * error message is output to syslog.
		 * @user_action
		 * Check the tag name.
		 */
		(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
			"tag %s: does not belong to caller", handle->tag);
		sc_syslog_msg_done(&sys_handle);
		result->type = FE_SYSERRNO;
		result->sys_errno = EACCES;
		UNLOCK_QUEUE(fe_handle_base);
		return;
	}

	/*
	 * check for redundancy
	 */
	if (handle->history.suspended) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * The user sent a suspend command to the rpc.fed server for a
		 * tag that is already suspended. An error message is output
		 * to syslog.
		 * @user_action
		 * Check the tag name.
		 */
		(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
		    "tag %s: already suspended", handle->tag);
		sc_syslog_msg_done(&sys_handle);
		result->type = FE_SYSERRNO;
		result->sys_errno = EBUSY;
		UNLOCK_QUEUE(fe_handle_base);
		return;
	}

	handle->history.prev_exec_time += time(NULL) -
	    handle->history.last_resume_time;
	handle->history.suspended = TRUE;

	if (debug)
		print_history(&handle->history);

	UNLOCK_QUEUE(fe_handle_base);

}

/*
 * sets the end_flag, so that the next time timeout thread runs
 * it kills the process irrespective of timeout value. Used to enforce
 * fast quiesce
 */
void
fe_kill_svc(fe_args *args, fe_result *result,
	security_cred_t *cred)
{
	fe_handle *handle;
	sc_syslog_msg_handle_t sys_handle;

	if (debug)
		dbg_msgout(NOGET("in fe_kill_svc\n"));
	/*
	 * get handle corresponding to tag name
	 */
	LOCK_QUEUE(fe_handle_base);
	if ((handle = id_to_handle(args->tag)) == NULL) {
		result->type = FE_NOTAG;
		UNLOCK_QUEUE(fe_handle_base);
		return;
	}

	/*
	 * check that tag belongs to user
	 * or if user is root match any tag
	 */
	if (!((cred->aup_uid == 0) ||
	    ((handle->uid == cred->aup_uid) &&
	    (handle->gid == cred->aup_gid)))) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
		    "tag %s: does not belong to caller", handle->tag);
		sc_syslog_msg_done(&sys_handle);
		result->type = FE_SYSERRNO;
		result->sys_errno = EACCES;
		UNLOCK_QUEUE(fe_handle_base);
		return;
	}

	if (debug)
		print_history(&handle->history);

	handle->end_flag = TRUE;

	if (debug)
		print_history(&handle->history);

	UNLOCK_QUEUE(fe_handle_base);

}

void
fe_resume_svc(fe_args *args, fe_result *result,
	security_cred_t *cred)
{
	fe_handle	*handle;
	sc_syslog_msg_handle_t sys_handle;

	if (debug)
		dbg_msgout(NOGET("in fe_resume_svc\n"));

	/*
	 * get handle corresponding to tag name
	 */
	LOCK_QUEUE(fe_handle_base);
	if ((handle = id_to_handle(args->tag)) == NULL) {
		result->type = FE_NOTAG;
		UNLOCK_QUEUE(fe_handle_base);
		return;
	}

	/*
	 * check that tag belongs to user
	 * or if user is root match any tag
	 */
	if (!((cred->aup_uid == 0) ||
	    ((handle->uid == cred->aup_uid) &&
	    (handle->gid == cred->aup_gid)))) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
		    "tag %s: does not belong to caller", handle->tag);
		sc_syslog_msg_done(&sys_handle);
		result->type = FE_SYSERRNO;
		result->sys_errno = EACCES;
		UNLOCK_QUEUE(fe_handle_base);
		return;
	}

	/*
	 * check for redundancy
	 */
	if (!handle->history.suspended) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * The user sent a resume command to the rpc.fed server for a
		 * tag that is not suspended. An error message is output to
		 * syslog.
		 * @user_action
		 * Check the tag name.
		 */
		(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
		    "tag %s: not suspended, cannot resume", handle->tag);
		sc_syslog_msg_done(&sys_handle);
		result->type = FE_SYSERRNO;
		result->sys_errno = EBUSY;
		UNLOCK_QUEUE(fe_handle_base);
		return;
	}

	if (debug)
		print_history(&handle->history);

	handle->history.last_resume_time = time(NULL);
	handle->history.suspended = FALSE;

	if (debug)
		print_history(&handle->history);

	UNLOCK_QUEUE(fe_handle_base);

}

/*
 * Time the timeout thread is sleeping between checks, in seconds.
 */
#define	TIMEOUT_SLEEP  5

/*
 * Periodically traverse ring of handles and kill timed-out methods.
 */
void
timeout_thread()
{

	fe_handle	*handle;
	int iter = 0;

	if (debug)
		dbg_msgout(NOGET("in timeout_thread\n"));

	/*
	 * loop forever
	 */
	for (;;) {
		++iter;

		/*
		 * sleep interval
		 */
		(void) poll(0, 0, TIMEOUT_SLEEP * 1000);

		if (debug)
			dbg_msgout(NOGET("in timer thread loop i=%d\n"), iter);

		/*
		 * traverse handles
		 */
		LOCK_QUEUE(fe_handle_base);

		if (fe_handle_base == NULL) {
			UNLOCK_QUEUE(fe_handle_base);
			continue;
		}

		/*
		 * Start checking handles at base of ring;
		 * stop when we've come full circle.
		 */
		handle = fe_handle_base;
		do {
			timeout_process_handle(handle);
			handle = handle->next;
		/* end traverse handle list */
		} while (handle != fe_handle_base);

		UNLOCK_QUEUE(fe_handle_base);

	}	/* end loop forever */
}

/*
 * timeout_process_handle
 * ------------------------
 * Expects the queue lock to be held.
 * "processes" one handle by checking if the method has been in flight for
 * too long. If so, and SIGTERM/ABRT has not yet been sent to the process,
 * SIGTERM/ABRT is sent. If SIGTERM/ABRT has already been sent, but SIGKILL has
 * not, SIGKILL is sent. If SIGKILL has been sent, and the process is still
 * around, then the process is marked unkillable in the handle, and the SIGUSR1
 * signal is sent to the monitoring thread to interrupt it from the waitpid or
 * read system call.
 */
static void timeout_process_handle(fe_handle *handle)
{
	int err;
	sc_syslog_msg_handle_t sys_handle;
	int sig = SIGTERM;

	/*
	 * get current time
	 */
	time_t curr_time = time(NULL);

	if (debug) {
		dbg_msgout(NOGET(
		    "handle->pid:%d timeout:%d grace_period:%d\n"),
		    handle->pid, handle->timeout, handle->grace_period);
		print_history(&handle->history);
		dbg_msgout(NOGET("\texec_time:\t%d\n"),
		    curr_time -  handle->history.last_resume_time +
		    handle->history.prev_exec_time);
		dbg_msgout(NOGET("if criterion=%d else criterion=%d\n"),
		    curr_time - handle->history.stop_sig_time +
		    handle->grace_period, curr_time -
		    handle->history.last_resume_time +
		    handle->history.prev_exec_time);
	}

	/*
	 * if suspended (flag to monitor is off) keep going;
	 * if timeout is 0 it means it's not set, keep going;
	 */
	if (handle->end_flag != TRUE &&
	    (handle->history.suspended || handle->timeout == 0)) {
		return;
	}

	/*
	 * Check if grace period given after timeout/end_flag
	 * setting, expired.
	 * If yes, and if we have not already sent a SIGKILL,
	 * send a SIGKILL signal to the method.
	 * The method will exit, the fe thread will catch the method
	 * exit (from its waitpid call), check if the timeout flag was set,
	 * and set error code to ETIMEDOUT(in fe_run_process).
	 *
	 * If we have already sent SIGKILL, we check if the
	 * process is really still alive, and, if so, mark
	 * the process as unkillable in the handle, and
	 * send a SIGUSR1 signal to the monitoring thread.
	 */
	if ((curr_time >= handle->history.stop_sig_time +
	    handle->grace_period) && handle->history.sent_sig_stop) {
		if (debug) {
			dbg_msgout(NOGET("tag %s: timeout expired\n"),
			    handle->tag);
		}

		if (!handle->history.sent_sig_kill) {

			if (debug) {
				dbg_msgout((const char *) NOGET(
				    "in 1st if killing tag %s, pid %d\n"),
				    handle->tag, handle->pid);
			}

			/*
			 * send the tag processes a SIGKILL
			 * signal to the method process group
			 */
			if (kill(-handle->pid, SIGKILL) == -1) {
				err = errno;
				if (err == ESRCH) {
					/*
					 * The process exited, but the
					 * monitoring thread hasn't cleaned
					 * up the handle yet.  That's fine;
					 * just skip this handle.
					 */
					if (debug) {
						dbg_msgout("ESRCH return from "
						    "kill for tag %s,pid %d\n",
						    handle->tag, handle->pid);
					}
					return;
				}

				/*
				 * If we get here, we have a serious error.
				 * Log a message and abort.
				 */
				err = errno;
				(void) sc_syslog_msg_initialize(
				    &sys_handle, SC_SYSLOG_RGM_FED_TAG, "");
				/*
				 * SCMSGS
				 * @explanation
				 * The rpc.fed server is not able to stop a
				 * tag that timed out, and the error message
				 * is shown. An error message is output to
				 * syslog.
				 * @user_action
				 * Save the /var/adm/messages file. Examine
				 * other syslog messages occurring around the
				 * same time on the same node, to see if the
				 * cause of the problem can be identified.
				 */
				(void) sc_syslog_msg_log(sys_handle,
				    LOG_ERR, MESSAGE,
				    "kill -KILL: %s", strerror(err));
				sc_syslog_msg_done(&sys_handle);

				abort_fed();

				/* not reached */
				return;
			}

			handle->history.sent_sig_kill = TRUE;

		} else {
			/*
			 * We have already sent sigkill, and the handle is
			 * still here.  Verify that the process is really
			 * still alive, and if so, mark it unkillable and
			 * send a USR1 signal to the monitoring thread.
			 */
			if (debug) {
				dbg_msgout((const char *) NOGET(
				    "Already sent KILL to tag %s, pid %d\n"),
				    handle->tag, handle->pid);
			}

			/*
			 * send the tag processes a 0
			 * signal to check if they're really still alive.
			 */
			if (kill(-handle->pid, 0) == -1) {
				err = errno;
				if (err == ESRCH) {
					/*
					 * The process exited, but the
					 * monitoring thread hasn't cleaned
					 * up the handle yet.  That's fine;
					 * just skip this handle.
					 */
					if (debug) {
						dbg_msgout("ESRCH return from "
						    "kill for tag %s,pid %d\n"
						    " with signal 0 returned"
						    " ESRCH (as expected)\n",
						    handle->tag, handle->pid);
					}
					return;
				}

				/*
				 * If we get here, we have a serious error.
				 * Log a message and abort.
				 */
				err = errno;
				(void) sc_syslog_msg_initialize(
				    &sys_handle, SC_SYSLOG_RGM_FED_TAG, "");
				/*
				 * SCMSGS
				 * @explanation
				 * The rpc.fed server is not able to send a
				 * signal to a tag that timed out, and the
				 * error message is shown. An error message is
				 * output to syslog.
				 * @user_action
				 * Save the syslog messages file. Examine
				 * other syslog messages occurring around the
				 * same time on the same node, to see if the
				 * cause of the problem can be identified.
				 */
				(void) sc_syslog_msg_log(sys_handle,
				    LOG_ERR, MESSAGE,
				    "kill -0: %s", strerror(err));
				sc_syslog_msg_done(&sys_handle);

				abort_fed();

				/* not reached */
				return;
			} else {
				/* the process is really still there */
				handle->unkillable_process = TRUE;
				err = pthread_kill(handle->monitoring_thread,
				    SIGUSR1);
				if (err != 0) {
					/*
					 * This shouldn't ever happen.  If
					 * it does, log a message and
					 * abort.
					 */
					(void) sc_syslog_msg_initialize(
					    &sys_handle, SC_SYSLOG_RGM_FED_TAG,
					    "");
					/*
					 * SCMSGS
					 * @explanation
					 * The rpc.fed server encountered an
					 * error with the pthread_kill
					 * function. The message contains the
					 * system error.
					 * @user_action
					 * Save the syslog messages file.
					 * Contact your authorized Sun service
					 * provider to determine whether a
					 * workaround or patch is available.
					 */
					(void) sc_syslog_msg_log(sys_handle,
					    LOG_ERR, MESSAGE,
					    "pthread_kill: %s", strerror(err));
					sc_syslog_msg_done(&sys_handle);

					abort_fed();

					/* not reached */
					return;
				}
			}
		}
	} else if (!handle->history.sent_sig_stop &&
	    (handle->end_flag == TRUE ||
	    (curr_time - handle->history.last_resume_time +
	    handle->history.prev_exec_time >= handle->timeout))) {

		/*
		 * if timeout expired or end_flag set;
		 * if yes send a stop signal to the method;
		 * The method will exit, and the fe thread will
		 * catch this and if timeout flag was set or end_flag
		 * was set, respectively set error codes to
		 * ETIMEDOUT/EQUIESCED (in fe_run_process).
		 */
		if (debug) {
			if (handle->end_flag == TRUE)
				dbg_msgout(NOGET("tag %s: quiesced\n"),
				    handle->tag);
			else
				dbg_msgout(NOGET("tag %s: timeout expired\n"),
				    handle->tag);

			dbg_msgout((const char *) NOGET("curr_time=%d\n"
			    "handle->history.prev_exec_time=%d\n"
			    "handle->history.last_resume_time=%d\n"
			    "handle->history.sent_sig_stop=%d\n"
			    "handle->timeout=%d\n"), curr_time,
			    handle->history.prev_exec_time,
			    handle->history.last_resume_time,
			    handle->history.sent_sig_stop, handle->timeout);

			dbg_msgout((const char *) NOGET(
			    "in else stopping tag %s, pid %d\n"),
			    handle->tag, handle->pid);
		}
#ifndef linux
		if (handle->flags & FE_PRODUCE_CORE) {
			sig = SIGABRT;
			create_method_core_location(handle);
		}
#endif

		/*
		 * send the tag processes a SIGTERM/ABRT signal
		 * to the method process group
		 */
		if (kill(-handle->pid, sig) == -1) {
			err = errno;
			if (err == ESRCH) {
				/*
				 * The process exited, but the monitoring
				 * thread hasn't cleaned up the handle
				 * yet.  That's fine; just skip this handle.
				 */
				if (debug) {
					dbg_msgout("ESRCH return from "
					    "kill for tag %s,pid %d\n",
					    handle->tag, handle->pid);
				}
				return;
			}

			/*
			 * If we get here, we have a serious error.
			 * Log a message and abort.
			 */
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * The rpc.fed server is not able to kill a tag that
			 * timed out, and the error message is shown. An error
			 * message is output to syslog.
			 * @user_action
			 * Save the /var/adm/messages file. Examine other
			 * syslog messages occurring around the same time on
			 * the same node, to see if the cause of the problem
			 * can be identified.
			 */
			(void) sc_syslog_msg_log(sys_handle, LOG_ERR,
			    MESSAGE, "kill -TERM: %s", strerror(err));
			sc_syslog_msg_done(&sys_handle);

			abort_fed();

			/* not reached */
			return;
		}

		/* Mark that we sent SIGTERM/ABRT */
		handle->history.sent_sig_stop = TRUE;
		handle->history.stop_sig_time = curr_time;
	}
}

/*
 * find handle corresponding to a certain tag.
 */
static fe_handle *
id_to_handle(char *id)
{
	fe_handle *ptr;
	bool_t found = FALSE;

	if (id == NULL)
		return (NULL);

	ptr = fe_handle_base;
	if (ptr == NULL)
		return (NULL);

	do {
		if (ptr->tag != NULL && strcmp(id, ptr->tag) == 0)  {
			found = TRUE;
			break;
		}

		ptr = ptr->next;
	} while (ptr != fe_handle_base);

	if (!found)
		ptr = NULL;

	return (ptr);
}


/*
 * print values of history struct in handle; used for debugging.
 */
void
print_history(fe_process_tag *h)
{

	dbg_msgout(NOGET("handle->history:\n"));
	dbg_msgout(NOGET("\tsuspended:\t%d\n"), h->suspended);
	dbg_msgout(NOGET("\tsent_sig_stop:\t%d\n"), h->sent_sig_stop);
	dbg_msgout(NOGET("\tsent_sig_kill:\t%d\n"), h->sent_sig_kill);
	dbg_msgout(NOGET("\tlast_resume_time:\t%d\n"), h->last_resume_time);
	dbg_msgout(NOGET("\tprev_exec_time:\t%d\n"), h->prev_exec_time);
	dbg_msgout(NOGET("\tstop_sig_time:\t%d\n"), h->stop_sig_time);

} /* end print_history */

/*
 * abort_fed()
 * ---------
 *
 * Function to log an error message and terminate the fed.
 * Should only be called in extreme, unrecoverable, circumstances.
 *
 * Note that we terminate the fed instead of the entire node, and let
 * the failfast mechanism bring down the node.
 */
void
abort_fed(void)
{
	sc_syslog_msg_handle_t sys_handle;

	(void) sc_syslog_msg_initialize(&sys_handle,
	    SC_SYSLOG_RGM_FED_TAG, "");
	/*
	 * SCMSGS
	 * @explanation
	 * The rpc.fed server experienced an unrecoverable error, and is
	 * aborting the node.
	 * @user_action
	 * Save the syslog messages file. Examine other syslog messages
	 * occurring around the same time on the same node, to see if the
	 * cause of the problem can be identified. Contact your authorized Sun
	 * service provider to determine whether a workaround or patch is
	 * available.
	 */
	(void) sc_syslog_msg_log(sys_handle, LOG_ERR,
	    MESSAGE, "Fatal error; aborting the rpc.fed daemon.");
	sc_syslog_msg_done(&sys_handle);

	abort();
}


#ifdef SC_SRM
/*
 * fed_set_project
 *
 * Function to set to a project before exec.
 */
static int
fed_setproject(fe_handle *handle, boolean_t slm_ssm_wrapper)
{
	sc_syslog_msg_handle_t	sys_handle;
	int			err;
	int			ret;
	char			*proj_name = NULL;
	char			*user_name;
	char			*resource = "";
	struct passwd		pwd;
	char			pwd_buff[PWD_BUF_SIZE];

	/* SC SLM addon start */
	if (handle->r_name != NULL) {
		resource = handle->r_name;
	} else {
		if (handle->rg_name != NULL) {
			resource = handle->rg_name;
		}
	}

	/*
	 * handle->scslm_proj_name is always NULL before scslm_fe_run() call,
	 * only scslm_fe_run() set handle->scslm_proj_name to not NULL, when
	 * a SCSLM generated project has to be used.
	 */
	if (slm_ssm_wrapper == B_TRUE) {
		/*
		 * ssm_wrapper -z zoneX -P SCLM_... is launched in global zone,
		 * use default project in global zone.
		 */
		proj_name = "default";
	} else {
		if (handle->scslm_proj_name != NULL) {
			/* SC SLM generated project name */
			proj_name = handle->scslm_proj_name;
		} else {
			/* Manual SC project name */
			if (handle->project_name != NULL &&
			    (*(handle->project_name) != '\0')) {
				/* manual project setup */
				proj_name = handle->project_name;
			}
		}
	}
	/* SC SLM addon end */

	/* User name */
	if (getpwuid_r(getuid(), &pwd, pwd_buff, PWD_BUF_SIZE) == NULL) {
		/*lint !e746 */
		err = errno;
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Should never occur.
		 * @user_action
		 * Contact your authorized Sun service provider to determine
		 * whether a workaround or patch is available.
		 */
		(void) sc_syslog_msg_log(
		    sys_handle, LOG_ERR, MESSAGE,
		    "<%s> getpwuid_r uid %d error %s",
		    resource, getuid(), strerror(err));
		sc_syslog_msg_done(&sys_handle);
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Should never occur.
		 * @user_action
		 * Verify project database. Contact your authorized Sun
		 * service provider to determine whether a workaround or patch
		 * is available.
		 */
		(void) sc_syslog_msg_log(
		    sys_handle, LOG_WARNING, MESSAGE,
		    "<%s> using fake user name for setproject",
		    resource);
		sc_syslog_msg_done(&sys_handle);
		user_name = "root";
	} else {
		user_name = pwd.pw_name;
	}

	if (proj_name != NULL) {
		ret = fed_retrysetproject(resource, proj_name, user_name);
		if (ret == 0) {
			if (debug) {
				dbg_msgout_nosyslog(NOGET(
				    "fed: Change"
				    " <%s> set to project <%s> user <%s>\n"),
				    resource, proj_name, user_name);
			}
			/* Done */
			return (0);
		}
		/* SC SLM addon start */
#if 0
		/*
		 * This is for futur use, if decided that
		 * unable to set a SC SLM project name is
		 * a failure. Current specs are run
		 * the resource anyway, with warnings messages
		 * emited in fed_slm.c.
		 */
		if (handle->scslm_proj_name != NULL) {
			/* SLM: mandatory to success */
			return (ret);
		}
#endif
		/* SC SLM addon start */
	}
	/*
	 * project name is not specified, or error, set to default
	 */
	ret = fed_retrysetproject(resource, "default", user_name);
	if (ret == 0) {
		if (debug) {
			dbg_msgout_nosyslog(NOGET(
			    "fed: Change"
			    " <%s> set to project <default> user <%s>\n"),
			    resource, user_name);
		}
		return (0);
	}
	return (0);
}


int
fed_retrysetproject(char *resource, char *projname, char *uname)
{
	int			cnt = 0;
	int			ret;
	int			err;
	projid_t		projid;
	char			ipbuf[PROJECT_BUFSZ];
	sc_syslog_msg_handle_t	sys_handle;

	/*
	 * Sometimes, setproject() fails, because to update
	 * /etc/project, the S9 scslm_projmod and S10 projmod
	 * tools rename /etc/project temporary.
	 */
	while (cnt++ < FED_SET_PROJECT_RETRY) {
		ret = setproject(projname, uname, TASK_NORMAL);
		err = errno;
		if (ret != 0) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * Should never occur.
			 * @user_action
			 * Verify project database. Contact your authorized
			 * Sun service provider to determine whether a
			 * workaround or patch is available.
			 */
			(void) sc_syslog_msg_log(
			    sys_handle, LOG_ERR, MESSAGE,
			    "pid %u <%s> project <%s> user <%s> "
			    "setproject() ret %d errno %d",
			    getpid(), resource, projname, uname, ret, err);
			sc_syslog_msg_done(&sys_handle);
		} else {
			if (cnt > 1) {
				(void) sc_syslog_msg_initialize(&sys_handle,
				    SC_SYSLOG_RGM_FED_TAG, "");
				/*
				 * SCMSGS
				 * @explanation
				 * A warning message.
				 * @user_action
				 * None.
				 */
				(void) sc_syslog_msg_log(
				    sys_handle, LOG_ERR, MESSAGE,
				    "pid %u <%s> project <%s> user <%s> "
				    "setproject() success after %d retry",
				    getpid(), resource, projname, uname, cnt);
				sc_syslog_msg_done(&sys_handle);
			}
			return (0);
		}
		ret = inproj(uname, projname,
		    (void *)ipbuf, sizeof (ipbuf));
		err = errno;
		if (!ret) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * Should never occur.
			 * @user_action
			 * Verify project database. Contact your authorized
			 * Sun service provider to determine whether a
			 * workaround or patch is available.
			 */
			(void) sc_syslog_msg_log(
			    sys_handle, LOG_ERR, MESSAGE,
			    "<%s> project <%s> user <%s> "
			    "inproj() ret %d errno %d",
			    resource, projname, uname, ret, err);
			sc_syslog_msg_done(&sys_handle);
		}
		projid = getprojidbyname(projname);
		err = errno;
		if (projid == (projid_t)-1) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * Should never occur.
			 * @user_action
			 * Verify project database. Contact your authorized
			 * Sun service provider to determine whether a
			 * workaround or patch is available.
			 */
			(void) sc_syslog_msg_log(
			    sys_handle, LOG_ERR, MESSAGE,
			    "<%s> project <%s> user <%s> "
			    "getprojidbyname() %d errno %d",
			    resource, projname, uname, projid, err);
			sc_syslog_msg_done(&sys_handle);
		}
		sync();
		/* Wait 500 ms */
		(void) poll(0, 0, 500);
	}
	return (1);
}

/*
 * validate_project -
 * Check if the specified is a valid project name.
 */
static boolean_t
validate_project(char *proj_name)
{

	if (proj_name == NULL || *proj_name == '\0')
		return (B_TRUE);

	if (getprojidbyname(proj_name) == -1) {
		return (B_FALSE);
	}

	return (B_TRUE);
}

/*
 * validate_project_on_zone() verifies the project name in
 * the context of the specific zone (specified in handle->zonename).
 * Since fed runs in the global zone, it is important that fed
 * sets the zone's context before verifying the project name. This
 * function uses zone_enter() to invoke getprojidbyname() in a zone's
 * context. If enter_local_zone is set to false, this function will
 * validate the project name in global zone context itself. The return
 * type of this function is FE_ERRCODE. If the project name is valid,
 * this function will return FE_OKAY. If the project name is invalid,
 * this function will return FE_PROJECT_NAME_INVALID. Project name
 * validation is mainly required for rgmd. However implementing this
 * code in rgmd would have made it very messy. Hence, this functionality
 * has been moved to fed.
 */
static enum FE_ERRCODE
validate_project_on_zone(fe_handle *handle, boolean_t enter_local_zone,
    int *err_no, char **err_msg, boolean_t produce_core) {

	sc_syslog_msg_handle_t sys_handle;
	int	tmpl_fd;
	int	pid;
	int 	ret_status;
#if SOL_VERSION >= __s10
	zoneid_t zoneid;
#endif
	/*
	 * If there is no need to enter the local zone
	 * then we can check the validity of the project name
	 * here itself.
	 */
	if (!enter_local_zone) {
		if (validate_project(handle->project_name)) {
			return (FE_OKAY);
		} else {
			return (FE_PROJECT_NAME_INVALID);
		}
	}

	/*
	 * If we are here, it means enter_local_zone flag was set to
	 * true. This flag is valid only in S10 and above
	 */
#if SOL_VERSION < __s10
	return (FE_UNKNC);
#else
	if ((zoneid = getzoneidbyname(handle->zonename)) == -1) {
		*err_no = errno;
		log_error(handle->tag, *err_no, "getzoneidbyname");
		return (FE_SYSERRNO);
	}

	if ((tmpl_fd = init_template(err_no, err_msg)) == -1) {
		return (FE_SYSERRNO);
	}

	pid = fork1();
	if (pid != 0) {
		if (pid == -1) {
			/*
			 * Suppress the lint info.
			 * lint doesn't understand errno.
			 */
			/* CSTYLED */
			*err_no = errno; /*lint !e746 */
			/*
			 * suppress the lint error because
			 * we know tmpl_fd has been initialized
			 */
			(void) ct_tmpl_clear(tmpl_fd); /*lint !e644 */
			(void) close(tmpl_fd);
			return (FE_SYSERRNO);
		}
	} else {
		/*
		 * Child
		 */

		(void) ct_tmpl_clear(tmpl_fd);

		/* set default behaviour in case fed modified it. */
		(void) sigset(SIGCLD, SIG_DFL);
		/* set the context of the zone */
		if (zone_enter(zoneid) != 0) {
			exit(errno);
		}

		/*
		 * Set the uid and group id of the process.
		 */
		if ((handle->gid != 0) && (setgid(handle->gid) == -1)) {
			*err_no = errno;
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
			    "setgid: %s", strerror(*err_no));
			sc_syslog_msg_done(&sys_handle);
			exit(*err_no);
		}

		if ((handle->uid != 0) && (setuid(handle->uid) == -1)) {
			*err_no = errno;
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
			    "setuid: %s", strerror(*err_no));
			sc_syslog_msg_done(&sys_handle);
			exit(*err_no);
		}

#ifndef linux
		/*
		 * Change  coredump settings to be used in case of timeouts to
		 * dump the core of whole process tree.
		 */
		if (produce_core)
			modify_coreadm_settings(handle);
#endif
		/*
		 * Validate the project name now.
		 */
		if (!validate_project(handle->project_name)) {
			/*
			 * Invalid project name was specified. Set the
			 * appropriate error.
			 */
			exit(PROJECT_NAME_INVALID);
		} else {
			/*
			 * Since the project name was valid exit with
			 * an exit status of zero.
			 */
			exit(PROJECT_NAME_VALID);
		}
	}

	/* parent again */
	(void) ct_tmpl_clear(tmpl_fd);
	(void) close(tmpl_fd);

	/* wait for the child process to exit */
	while (waitpid(pid, &ret_status, NULL) != pid)
		continue;

	if (WIFEXITED(ret_status)) {
		if (WEXITSTATUS(ret_status) != PROJECT_NAME_VALID) {
			/*
			 * If we are here, it means the child process
			 * which validates the project name exited
			 * with a non-zero exit status. This implies
			 * that the project name is invalid.
			 */
			return (FE_PROJECT_NAME_INVALID);
		} else {
			return (FE_OKAY);
		}
	} else {
		/*
		 * The child process did not exit normally.
		 */
		return (FE_SYSERRNO);
	}

#endif	/* Sol 10 */
}
#endif	/* SC_SRM */
