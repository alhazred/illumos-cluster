/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _FED_H_
#define	_FED_H_

#pragma ident	"@(#)fed.h	1.30	08/05/20 SMI"


/*
 * fed.h - header file for fed server
 * FE daemon data structures
 */

#ifdef __cplusplus
extern "C" {
#endif

#include <rgm/security.h>

#if DOOR_IMPL
#include <rgm/fe.h>
#else
#include <rgm/rpc/fe.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <strings.h>
#include <rpc/rpc.h>
#include <rpc/pmap_clnt.h> /* for pmap_unset */
#include <rpc/key_prot.h>
#include <netdb.h>
#include <pthread.h>
#include <synch.h>
#include <locale.h>
#include <errno.h>
#include <signal.h>
#include <rgm/scutils.h>
#include <sys/st_failfast.h>
#include <sys/cl_assert.h>
#include <sys/sol_version.h>

extern int debug;
extern int secure;

/* used with pre_alloc_buf() to pre-allocate swap */
#define	FED_MIN_SWAP_RESERVE_SIZE  4		/* 4 MB */
#define	FED_MAX_SWAP_RESERVE_SIZE  8		/* 8 MB */

/* The maximum filedescriptor limit fed will tolerate. */
#define	FED_MAX_FILEDESCRIPTORS	16384

#ifdef LOCK_DEBUG
#define	LOCK_QUEUE(name) \
	if (debug) dbg_msgout(NOGET("%s/%d LOCK QUEUE (0x%x)\n"), \
	NOGET("name"), __LINE__, &fe_queue_lock); \
	(CL_PANIC(pthread_mutex_lock(&fe_queue_lock) == 0));
#define	UNLOCK_QUEUE(name) \
	if (debug) dbg_msgout(NOGET("%s/%d UNLOCK QUEUE (0x%x)\n"), \
	NOGET("name"), __LINE__, &fe_queue_lock); \
	(CL_PANIC(pthread_mutex_unlock(&fe_queue_lock) == 0));
#else
#define	LOCK_QUEUE(h) (CL_PANIC(pthread_mutex_lock(&fe_queue_lock) == 0));
#define	UNLOCK_QUEUE(h) (CL_PANIC(pthread_mutex_unlock(&fe_queue_lock) == 0));
#endif

/*
 * Used to mark messages that are considered "normal" and would have
 * been targets for i18n using gettext(). SYSTEXT distinguishes this
 * category of messages from unusual or failure messages.
 */
#define	SYSTEXT(s)	s

pthread_mutex_t handle_lock;    /* lock around handle */

/*
 * We have a process tag for each process we start.
 * This keeps the time-related info and is used when a process is
 * timed-out, suspended or resumed.
 */
struct fe_process_tag {
	bool_t suspended;	/* is timeout monitoring suspended ? */
	bool_t sent_sig_stop;	/* was SIGTERM/SIGABRT signal sent? */
	bool_t sent_sig_kill;	/* was SIGKILL signal sent? */
	time_t last_resume_time;	/* last time timeout monitoring */
		/* was restarted - or start time, if never suspended */
	time_t prev_exec_time;	/* time spent executing before timeout */
				/* monitoring was suspended */
	time_t stop_sig_time;	/* time when sigterm/sigabrt signal was sent */
	int wstat;		/* return code from method execution */
};
typedef struct fe_process_tag fe_process_tag;

/*
 * Handle to hold all the configuration information
 * for requests, along with state that the thread needs
 * in order to make decisions about restarting them.
 */
struct fe_handle {
	char *cmd;  /* pathname passed by RPC call */
	char **arg;	/* arguments passed by RPC call */
	char **env;	/* environment passed by RPC call */
	char *tag;	/* process tag (identifier) */
	int timeout;	/* method timeout passed by RPC call in secs */
	int grace_period;
		/* method grace period passed by RPC call in secs */
	char *corepath;
		/* location for dumping method cores */
	uid_t uid;	/* user id */
	gid_t gid;	/* group id */
	pid_t pid;	/* process id of executing process */
	/*
	 * process id of session (group of processes,
	 * with the process as leader
	 */
	pid_t gpid;
	fe_process_tag 	history;
	struct fe_handle *next;
	struct fe_handle *prev;

	int flags;	/* bit flags */
	char *project_name;	/* project name */
	char *zonename;		/* zonename to launch method into */
#if SOL_VERSION >= __s10
	char *zonepath;
		/* zone root path for dumping core in ng zone */
#endif

	pthread_t monitoring_thread; /* the thread for this handle */

	/*
	 * flag set if process gets stuck in the kernel
	 */
	bool_t unkillable_process;

	/*
	 * if end_flag is set, timeout_thread kills the running method
	 * irrespective of timeout value.
	 */
	bool_t end_flag;

	/* SC SLM addon start */
	int slm_flags;		/* SLM flags */
	char *ssm_wrapper_zone;	/* ssm_wrapper zone */
	int do_slm_stop;	/* SLM created psets to remove ? */
	char *r_name;		/* resource name */
	char *rg_name;		/* resource group name */
	char *rg_slm_type;	/* SLM type */
	char *rg_slm_pset_type;	/* SLM pset */
	int rg_slm_cpu;		/* SLM RG_SLM_CPU_SHARES value */
	int rg_slm_cpu_min;	/* SLM RG_SLM_PSET_MIN value */
	char *scslm_proj_name;	/* SLM generated project name */
	/* SC SLM addon end */
};
typedef struct fe_handle fe_handle;

/*
 * Time measured in microseconds.
 */
typedef long long usectime_t;
#if DOOR_IMPL
void fed_door_server(void *cookie, char *dataptr, size_t data_size,
    door_desc_t *desc_ptr, uint_t ndesc);
#endif

extern void fe_run_svc(fe_run_args *args, fe_run_result *result,
	security_cred_t *cred);
extern void fe_suspend_svc(fe_args *args, fe_result *result,
	security_cred_t *cred);
extern void fe_resume_svc(fe_args *args, fe_result *result,
	security_cred_t *cred);
extern void fe_kill_svc(fe_args *args, fe_result *result,
	security_cred_t *cred);

extern void timeout_thread();

/*
 * S10 specific
 */
#if SOL_VERSION >= __s10
/*
 * Exits if initialization fails, so should be called after
 * failfast is initialized.
 */
void initialize_zone_callbacks(void);
#endif

#ifdef __cplusplus
}
#endif

#endif /* _FED_H_ */
