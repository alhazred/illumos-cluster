/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)fed_main.c	1.49	08/07/21 SMI"


/*
 * fed_main.c
 * Main for the fe daemon.
 * (default from fe_svc.c)
 */
#include "fed.h"
#include <sys/sol_version.h>
#if SOL_VERSION >= __s10
#include <zone.h>
#endif

#include <sys/resource.h>
#include <syslog.h>
#include <sys/sc_syslog_msg.h>
#include <sys/rsrc_tag.h>
#include <sys/utsname.h>
#include <sys/sol_version.h>

#ifndef linux
#include <rgm/sczones.h>
#endif

#if DOOR_IMPL
#include <rgm/fe.h>
#else
#include <rgm/rpc/fe.h>
#endif

#ifndef linux
/* SC SLM addon start */
#if SC_SRM
#include "fed_slm.h"
#endif
/* SC SLM addon end */
#endif
#ifdef linux
#include <rgm/rpc_services.h>
#endif

#include <dlfcn.h>
#include <sys/stat.h>

#define	FED_LOCK "/var/cluster/run/fed.lock"
/*
 * compile flag debug is used to disable some options during testing
 */
#ifdef DEBUG
static int DBG = 1;
int _rpcpmstart = 0;	/* Not started by a port monitor */
#else
static int DBG = 0;
#endif


/*
 * -d == debug (undocumented)
 */
#define	FE_OPTARGS NOGET("d")


int debug = 0;

/* global host name for prefix tag on captured stdout/stderr from methods */
char hostname[SYS_NMLN];

static char *progname;

#if DOOR_IMPL
int door_id;
#else
extern void fe_program_2(struct svc_req *rqstp, register SVCXPRT *transp);
#endif

static void usage(void);

/*
 * The signal handler must not use dbg_msgout() because it calls fprintf(3c)
 * which is not Async-Signal-Safe.
 */
/* ARGSUSED */
void
fed_sig_handler(int sig)
{
}

/*
 * For Europa, the fail-fast library is different depending on
 * the node type (server node or farm node).
 */
#define	FRGMD "/usr/cluster/lib/sc/frgmd"

#ifdef __sparcv9
#define	LIBCLST		"/usr/cluster/lib/sparcv9/libclst.so.1"
#define	LIBFCLST	"/usr/cluster/lib/sparcv9/libfclst.so.1"
#else
#define	LIBCLST		"/usr/cluster/lib/libclst.so.1"
#define	LIBFCLST	"/usr/cluster/lib/libfclst.so.1"
#endif

#define	ST_FF_ARM	"st_ff_arm"
#define	ST_FF_DISARM	"st_ff_disarm"

bool_t isfarmnode;

void
nodetype_init(void)
{
	struct stat filestat;

	if (stat(FRGMD, &filestat) < 0)
		isfarmnode = B_FALSE;
	else
		isfarmnode = B_TRUE;
}

int failfast_disarm() {
	void *dlhandle;
	int (*fn_ff_disarm)(void);
	char *ff_lib = LIBCLST;

	nodetype_init();
	if (isfarmnode) {
		ff_lib = LIBFCLST;
	}
	if ((dlhandle = dlopen(ff_lib, RTLD_LAZY)) == NULL) {
		return (-1);
	}
	if ((fn_ff_disarm =
		(int(*)())dlsym(dlhandle, ST_FF_DISARM)) /*lint !e611 */
		== NULL) {
		return (-1);
	}
	return ((*fn_ff_disarm)());
}

int failfast_arm(char *pname) {
	void *dlhandle;
	int (*fn_ff_arm)(char *);
	char *ff_lib = LIBCLST;

	nodetype_init();
	if (isfarmnode) {
		ff_lib = LIBFCLST;
	}
	if ((dlhandle = dlopen(ff_lib, RTLD_LAZY)) == NULL) {
		return (-1);
	}
	if ((fn_ff_arm = (int(*)())
		dlsym(dlhandle, ST_FF_ARM)) /*lint !e611 */
		== NULL) {
		return (-1);
	}
	return ((*fn_ff_arm)(pname));
}

void
#ifdef linux
term_sig_handler(int sig)
#else
term_sig_handler(void)
#endif
{
	if (isfarmnode) {
		(void) failfast_disarm();
		exit(0);
	}
}

static void
usage()
{
	(void) fprintf(stderr, (const char *) gettext(
		"Usage: %s [-d]\n"), progname);
}

int
main(int argc, char *argv[])
{
#if !DOOR_IMPL
#ifndef linux
	int mode = RPC_SVC_MT_AUTO;
	int no_threads = FE_RPC_THRMAX;
#endif
#endif
	struct rlimit rl = { 1024, 1024 };
	sc_syslog_msg_handle_t handle;
	pthread_t timeout_tid;
	int	err, i, rc;
	struct utsname name;
	struct sigaction saction;
	sigset_t sig;
#ifdef linux
	/*
	 * The default stack size on Solaris is about 1m while it is
	 * about 10m on Linux.  On Linux, with that much default stack
	 * size, we are not able to create couple hundred threads (e.g. 512).
	 * So, we need to change the default stack size.
	 */
	int stacksz = 70 * PTHREAD_STACK_MIN;
#endif

	if ((progname = strrchr(argv[0], '/')) == NULL)
		progname = argv[0];
	else
		progname++;

	(void) setlocale(LC_ALL, "");
	(void) textdomain(TEXT_DOMAIN);
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);
#ifndef linux
	if (sc_zonescheck() != 0)
		return (1);
#endif

	(void) sc_syslog_msg_set_syslog_tag(SC_SYSLOG_RGM_FED_TAG);

	while ((i = getopt(argc, argv, FE_OPTARGS)) != EOF) {
		switch (i) {
			case 'd':
				debug++;
				break;
			default:
				usage();
				exit(1);
		}
	}

	/*
	 * Setup up signal mask. SIGUSR1 is used by the timeout thread to
	 * notify a monitoring thread if its monitored process becomes
	 * unkillable.
	 *
	 * First make sure the signal is unblocked.
	 */
	if (sigemptyset(&sig) != 0) {
		/*
		 * Suppress lint message about "call to __errno() not
		 * made in the presence of a prototype."
		 * lint is unable to see the prototype for the __errno()
		 * function to which errno is macroed.
		 */
		/* CSTYLED */
		err = errno; /*lint !e746 */
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * The rpc.pmfd server was not able to initialize a signal
		 * set. The message contains the system error. This happens
		 * while the server is starting up, at boot time. The server
		 * does not come up, and an error message is output to syslog.
		 * @user_action
		 * Save the syslog messages file. Contact your authorized Sun
		 * service provider to determine whether a workaround or patch
		 * is available.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "sigemptyset: %s", strerror(err));
		sc_syslog_msg_done(&handle);
		exit(1);
	}

	if (sigaddset(&sig, SIGUSR1) != 0) {
		err = errno;
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * The rpc.pmfd server was not able to add a signal to a
		 * signal set. The message contains the system error. This
		 * happens while the server is starting up, at boot time. The
		 * server does not come up, and an error message is output to
		 * syslog.
		 * @explanation-2
		 * The rpc.fed server encountered an error with the
		 * sigemptyset function, and was not able to start. The
		 * message contains the system error.
		 * @user_action
		 * Save the syslog messages file. Contact your authorized Sun
		 * service provider to determine whether a workaround or patch
		 * is available.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "sigaddset: %s", strerror(err));
		sc_syslog_msg_done(&handle);
		exit(1);
	}

	if (sigprocmask(SIG_UNBLOCK, &sig, NULL) != 0) {
		err = errno;
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * The rpc.pmfd server was not able to set its signal mask.
		 * The message contains the system error. This happens while
		 * the server is starting up, at boot time. The server does
		 * not come up, and an error message is output to syslog.
		 * @user_action
		 * Save the syslog messages file. Contact your authorized Sun
		 * service provider to determine whether a workaround or patch
		 * is available.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "sigprocmask: %s", strerror(err));
		sc_syslog_msg_done(&handle);
		exit(1);
	}

	/*
	 * Setup signal handler to be used by threads.
	 */
	(void) memset(&saction, 0, sizeof (struct sigaction));
	saction.sa_handler = fed_sig_handler;
	saction.sa_flags = 0;
	if (sigaction(SIGUSR1, &saction, NULL)) {
		err = errno;
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * The rpc.pmfd server was not able to set its signal handler.
		 * The message contains the system error. This happens while
		 * the server is starting up, at boot time. The server does
		 * not come up, and an error message is output to syslog.
		 * @user_action
		 * Save the syslog messages file. Contact your authorized Sun
		 * service provider to determine whether a workaround or patch
		 * is available.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "sigaction: %s", strerror(err));
		sc_syslog_msg_done(&handle);
		exit(1);
	}

	/*
	 * Setup signal handler for SIGTERM
	 */
	(void) memset(&saction, 0, sizeof (struct sigaction));
	saction.sa_handler = term_sig_handler;
	saction.sa_flags = 0;
	if (sigaction(SIGTERM, &saction, NULL)) {
		err = errno;
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "sigaction: %s", strerror(err));
		sc_syslog_msg_done(&handle);
		exit(1);
	}

	/*
	 * check that user is root
	 */
	if (!DBG && getuid() != 0) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * The program or daemon has been started by someone not in
		 * superuser mode.
		 * @user_action
		 * Login as root and run the program. If it is a daemon, it
		 * may be incorrectly installed. Reinstall cluster packages or
		 * contact your service provider.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			"Must be root to start %s", progname);
		sc_syslog_msg_done(&handle);
		exit(1);
	}

	/*
	 * Get the system default number of descriptors. Max 0x4000
	 */

	if (getrlimit(RLIMIT_NOFILE, &rl) ==  -1) { /* CSTYLED */
		err = errno; /*lint !e746 */
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * The rpc.pmfd or rpc.fed server was not able to get the
		 * limit of files open. The message contains the system error.
		 * This happens while the server is starting up, at boot time.
		 * The server does not come up, and an error message is output
		 * to syslog.
		 * @user_action
		 * Save the /var/adm/messages file. Contact your authorized
		 * Sun service provider to determine whether a workaround or
		 * patch is available.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "getrlimit: %s", strerror(err));
		sc_syslog_msg_done(&handle);
		exit(1);
	}

	if (rl.rlim_cur > FED_MAX_FILEDESCRIPTORS) {
		rl.rlim_cur = FED_MAX_FILEDESCRIPTORS;
	}

	if (rl.rlim_max > FED_MAX_FILEDESCRIPTORS) {
		rl.rlim_max = FED_MAX_FILEDESCRIPTORS;
	}

	/*
	 * set limit on number of file descriptors that can be open
	 */
	if (setrlimit(RLIMIT_NOFILE, &rl) == -1) {
		/*
		 * Suppress the lint info.
		 * lint doesn't understand errno.
		 */
		/* CSTYLED */
		err = errno; /*lint !e746 */
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * The rpc.pmfd server was not able to set the limit of files
		 * open. The message contains the system error. This happens
		 * while the server is starting up, at boot time. The server
		 * does not come up, and an error message is output to syslog.
		 * @user_action
		 * Save the /var/adm/messages file. Contact your authorized
		 * Sun service provider to determine whether a workaround or
		 * patch is available.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "setrlimit: %s", strerror(err));
		sc_syslog_msg_done(&handle);
		exit(1);
	}

#ifndef linux
/* SC SLM addon start */
#if SOL_VERSION < __s10
	/*
	 * Set microstate accounting for FED.
	 * All of its childs will inherit from it.
	 * Microstate accounting is enabled by default since Solaris 10.
	 * A failure is not a fatal failure (should never happen).
	 * set_microstate_accounting(): in libscutils.
	 */
	if (set_microstate_accounting() != 0) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Should never occur.
		 * @user_action
		 * Contact your authorized Sun service provider to determine
		 * whether a workaround or patch is available.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Unable to set microstate accounting. Continue.");
		sc_syslog_msg_done(&handle);

		/*
		 * Continue.
		 * We do not want to fail
		 */
	}
#endif
/* SC SLM addon start */
#endif

#if !DOOR_IMPL
#ifndef linux
	/*
	 * set MT mode for rpc
	 */
	if (!rpc_control(RPC_SVC_MTMODE_SET, &mode)) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * The rpc.pmfd server was not able to set the multi-threaded
		 * operation mode. This happens while the server is starting
		 * up, at boot time. The server does not come up, and an error
		 * message is output to syslog.
		 * @user_action
		 * Save the /var/adm/messages file. Contact your authorized
		 * Sun service provider to determine whether a workaround or
		 * patch is available.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Unable to set automatic MT mode.");
		sc_syslog_msg_done(&handle);
		exit(1);
	}


	/*
	 * set the no. of threads for the rpc threadpool
	 *
	 * We need to increase the number of SunRPC server threads in the
	 * fed daemon. The number needs to be big enough for the sum of RGM's
	 * callback methods threadpool (which is max of 64), plus some
	 * threads to service the out-of-band calls that suspend and resume
	 * timeouts, plus some threads for the VALIDATE and MONITOR_CHECK
	 * methods (which do not use the methods threadpool), plus one thread
	 * for the OPS ucmmd.  Plus a cushion.
	 *
	 * Without this change, there is a chance that all of the feds
	 * RPC threads will be devoted to running methods, such that
	 * there are no threads to process the incoming out-of-band
	 * requests to suspend/resume timeouts.
	 *
	 */
	if (!rpc_control(RPC_SVC_THRMAX_SET, &no_threads)) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * The rpc.fed server was unable to set the number of threads
		 * for the RPC threadpool. This happens while the server is
		 * starting up.
		 * @user_action
		 * Save the /var/adm/messages file. Contact your authorized
		 * Sun service provider to determine whether a workaround or
		 * patch is available.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Unable to set the number of threads for the FED RPC "
		    "threadpool.");
		sc_syslog_msg_done(&handle);
		exit(1);
	}
#endif /* !linux */
#endif /* !DOOR_IMPL */

	(void) sigset(SIGPIPE, SIG_IGN);

	/*
	 * Daemonize if we're not in debug mode.
	 */
	if (!debug)
		make_daemon();

	/*
	 * ensure that another rgmd daemon is not running
	 * This must be done after the fork!
	 */
	make_unique(FED_LOCK);

	/*
	 * pre_alloc_heap() will reserve an additional amount of heap space
	 * by mallocing and freeing a large size of memory.
	 * This is done to check that there is enough memory available
	 * before starting the server.
	 */
	if (pre_alloc_heap(FED_MAX_SWAP_RESERVE_SIZE, FED_MIN_SWAP_RESERVE_SIZE)
	    != 0) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * The pmfd, fed, or other program was not able to allocate
		 * swap space. This means that the machine is low in swap
		 * space. The server does not come up, and an error message is
		 * output to syslog.
		 * @user_action
		 * Determine if the machine is running out of swap. If this is
		 * not the case, save the /var/adm/messages file. Contact your
		 * authorized Sun service provider to determine whether a
		 * workaround or patch is available.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Failed to pre-allocate swap space");
		sc_syslog_msg_done(&handle);
		exit(1);
	}

	/*
	 * cache the host name
	 * If this can't be done, initialise the string
	 * as an empty string.
	 * It's not worth aborting the daemon for this error.
	 */
	if (uname(&name) == -1) {
		err = errno;
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * The rpc.fed server encountered an error with the uname
		 * function. The message contains the system error.
		 * @user_action
		 * Save the /var/adm/messages file. Contact your authorized
		 * Sun service provider to determine whether a workaround or
		 * patch is available.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR,
		    MESSAGE, "uname: %s", strerror(err));
		sc_syslog_msg_done(&handle);
		hostname[0] = '\0';
	} else {
		if (sprintf(hostname, "%s", name.nodename) < 0)
			hostname[0] = '\0';
	}

	/*
	 * if debug is true this only sets the debug mode and returns;
	 * if not in debug mode, it also sets us in RT scheduling mode.
	 */
	if (svc_init(debug) == -1) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * The rpc.pmfd server was not able to initialize server
		 * operation. This happens while the server is starting up, at
		 * boot time. The server does not come up, and an error
		 * message is output to syslog.
		 * @user_action
		 * Save the /var/adm/messages file. Contact your authorized
		 * Sun service provider to determine whether a workaround or
		 * patch is available.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "svc_init failed.");
		sc_syslog_msg_done(&handle);
		exit(1);
	}

#ifdef linux
	if (rpc_init_rpc_mgr(
	    FE_RPC_THRMAX, debug, stacksz) != RPC_SERVICES_OK) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "rpc_init_rpc_mgr failed.");
		sc_syslog_msg_done(&handle);
		exit(1);
	}
#endif

	/*
	 * Initialize loopbackset array and rpc service.
	 */
#if DOOR_IMPL
	door_id = security_svc_reg(fed_door_server, RGM_FE_PROGRAM_CODE,
	    debug, "global");
	if (door_id < 0) {
#else
	if (security_svc_reg(fe_program_2, FE_PROGRAM_NAME,
	    FE_PROGRAM, FE_VERSION, debug) != 0) {
#endif
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * The rpc.pmfd server was not able to initialize
		 * authentication and rpc initialization. This happens while
		 * the server is starting up, at boot time. The server does
		 * not come up, and an error message is output to syslog.
		 * @user_action
		 * Save the /var/adm/messages file. Contact your authorized
		 * Sun service provider to determine whether a workaround or
		 * patch is available.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		"security_svc_reg failed.");
		sc_syslog_msg_done(&handle);
		exit(1);
	}

	/*
	 * Initialize the failfast driver.
	 * st_ff_arm returns error codes from the errno set, so use
	 * strerror to read them.
	 * If LOCAL is defined, skip it because we want to test
	 * on a standalone host so we don't link with the failfast libraries.
	 */
#ifndef LOCAL
	if ((rc = failfast_arm("fed")) != 0) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * The rpc.pmfd server was not able to initialize the failfast
		 * mechanism. This happens while the server is starting up, at
		 * boot time. The server does not come up, and an error
		 * message is output to syslog. The message contains the
		 * system error.
		 * @user_action
		 * Save the /var/adm/messages file. Contact your authorized
		 * Sun service provider to determine whether a workaround or
		 * patch is available.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "st_ff_arm failed: %s", strerror(rc));
		sc_syslog_msg_done(&handle);
		exit(1);
	}
#endif

	/*
	 * create thread to enforce method timeouts
	 */
	if (pthread_create(&timeout_tid, NULL,
	(void *(*)(void *))timeout_thread, NULL) != 0) {
		err = errno;
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "pthread_create: %s", strerror(err));
		sc_syslog_msg_done(&handle);
		exit(1);
	}

/* SC SLM addon start */
#ifdef SC_SRM
	if (scslm_init() != 0) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Should never occur.
		 * @user_action
		 * Contact your authorized Sun service provider to determine
		 * whether a workaround or patch is available.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "scslm_init error.");
		sc_syslog_msg_done(&handle);
		exit(1);
	}
#endif
/* SC SLM addon end */

#if SOL_VERSION >= __s10
	/* register for zone state change callbacks */
	initialize_zone_callbacks();
#endif

#if DOOR_IMPL
	while (1) {
		(void) pause();
	}
#else
	svc_run();
	(void) sc_syslog_msg_initialize(&handle, SC_SYSLOG_RGM_FED_TAG, "");
	/*
	 * SCMSGS
	 * @explanation
	 * The rpc.pmfd server was not able to run, due to an rpc error. This
	 * happens while the server is starting up, at boot time. The server
	 * does not come up, and an error message is output to syslog.
	 * @user_action
	 * Save the /var/adm/messages file. Contact your authorized Sun
	 * service provider to determine whether a workaround or patch is
	 * available.
	 */
	(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
	    "svc_run returned");
	sc_syslog_msg_done(&handle);

	return (1);
#endif
	/* NOTREACHED */

}
