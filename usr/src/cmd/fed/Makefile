#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#ident	"@(#)Makefile	1.48	08/05/20 SMI"
#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# cmd/fed/Makefile
#

CLIENT	= feadm
SERVER	= rpc.fed
PROG	= $(CLIENT)
DAEMON  = $(SERVER)

include ../Makefile.cmd
# include the macros for doors or rpc implementation
include $(SRC)/Makefile.ipc

RPCFILE = $(ROOTCLUSTINC)/rgm/door/fe_door.x
$(RPC_IMPL)RPCFILE = $(ROOTCLUSTINC)/rgm/rpc/fe.x

SRCS		= $(SVR_SRCS) $(CLNT_SRCS)
LINTFILES	= $(SRCS:%.c=%.ln)
PIFILES		= $(SRCS:%.c=%.pi)

# fed_sched.c functionality has moved to libscutils
SVR_SRCS	= fed.c fed_util.c fed_main.c fe_serv_xdr.c fed_slm.c
$(RPC_IMPL)SVR_SRCS	= fed.c fed_util.c fed_main.c
$(POST_S9_BUILD)SVR_SRCS += fed_zone_state.c

CLNT_SRCS	= feadm.c

RPCFILE_XDR     = fe_door_xdr.c
RPCFILE_SVC     =

$(RPC_IMPL)RPCFILE_XDR     = fe_xdr.c
$(RPC_IMPL)RPCFILE_SVC     = fe_svc.c

CLOBBERFILES	+= $(RPCFILE_XDR) $(RPCFILE_SVC) $(SERVER)
CLEANFILES	= $(RPCFILE_XDR) $(RPCFILE_SVC) $(SERVER) $(CLIENT)

CLNT_OBJS	+= $(CLNT_SRCS:%.c=objs/%.o)

SVR_OBJS	= $(RPCFILE_XDR:%.c=objs/%.o)
SVR_OBJS	+= $(RPCFILE_SVC:%.c=objs/%.o)
SVR_OBJS	+= $(SVR_SRCS:%.c=objs/%.o)

OBJDIRS = objs

#
# Compiler flags
#
MTFLAG = -mt

CPPFLAGS	+= $(MTFLAG)
CPPFLAGS	+= -I.
CPPFLAGS += -DSC_SRM

LDLIBS	+= -lc -lnsl -lscutils -lsecurity -lpthread -lclos
LDLIBS	+= -lgen  -lsczones $(DOOR_LDLIBS)

LDLIBS += -lproject
$(POST_S9_BUILD)LDLIBS += -lcontract -lpool

LINTFLAGS += -DFE_SECURITY_DEF=SEC_UNIX_STRONG

OBJECTS = $(SRCS:%.c=%.o) $(RPCFILE_XDR:%.c=%.o) $(RPCFILE_SVC:%.c=%.o)

CHECKHDRS= fed.h
CHECK_FILES = $(SVR_SRCS:%.c=%.c_check) $(CLNT_SRCS:%.c=%.c_check) \
	      $(CHECKHDRS:%.h=%.h_check)

RPCGENFLAGS	= $(RPCGENMT) -C -K -1

$(CLIENT):= LDLIBS += -lfe
$(CLIENT):= CPPFLAGS += -DFE_SECURITY_DEF=SEC_UNIX_STRONG
$(POST_S9_BUILD)$(SERVER) := LDLIBS += -lsczones

OTHER_LIBS = -lclos -ldl
$(SERVER):= LDLIBS += $(OTHER_LIBS)

# Since we're linking w/ C++ compilers...
#
CCYFLAG = -I 

.KEEP_STATE:

#
# Targets
#

ALL = $(OBJDIRS) $(RPCFILE_XDR) $(RPCFILE_SVC) $(PROG) $(SERVER)

all: $(ALL)

clean :
	$(RM) ./objs/*.o $(CLEANFILES)

install: $(ALL) $(ROOTCLUSTPROG) $(ROOTCLUSTDAEMON) 

$(SERVER): $(SVR_OBJS)
	$(LINK.c) $(SVR_OBJS) -o $@ $(LDLIBS)
	$(POST_PROCESS)

$(CLIENT): $(CLNT_OBJS)
	$(LINK.c) $(CLNT_OBJS) -o $@ $(LDLIBS)
	$(POST_PROCESS)

#
# build rules
#

$(RPCFILE_XDR): $(RPCFILE)
	$(RM) $@
	$(RPCGEN) $(RPCGENFLAGS) -c -o $@ $(RPCFILE)

$(RPCFILE_SVC): $(RPCFILE)
	$(RM) $@
	$(RPCGEN) $(RPCGENFLAGS) -m -o $@ $(RPCFILE)

include ../Makefile.targ

objs/%.o: %.c
	$(COMPILE.c) -o $@ $<

objs:
	-@$(MKDIR) -p $@
