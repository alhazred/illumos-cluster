/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scshutdown.c	1.63	09/02/08 SMI"

/*
 * scshutdown.c
 */

#include <sys/types.h>
#include <sys/param.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <string.h>
#include <sys/debug.h>
#include <sys/mnttab.h>
#include <sys/mount.h>
#include <sys/mntent.h>
#include <thread.h>
#include <errno.h>
#include <unistd.h>
#include <locale.h>
#include <libintl.h>

#include <dc/libdcs/libdcs.h>
#include <scadmin/scswitch.h>
#include <scadmin/scconf.h>
#include <scadmin/scstat.h>
#include <sys/cl_auth.h>
#include <sys/cl_cmd_event.h>
#include <sys/sc_syslog_msg.h>
#include <rgm/sczones.h>

#if SOL_VERSION >= __s10
#include <zone.h>
#include <sys/cladm_int.h>
#include <sys/quorum_int.h>
#include <sys/clconf_int.h>
#include <sys/vc_int.h>
#endif

#define	SC_SYSLOG_SCSHUTDOWN_TAG	"Cluster.scshutdown"

#define	MNTOPT_SEPARATOR	','
#define	DEFAULT_GRACE_PERIOD	60	/* default is 60 seconds */
#define	GDEV_PATH	"/global/.devices/node@"

/*
 * The number of times we retry scswitch_evacuate_devices(), and how long
 * (in seconds) we sleep in between each attempt. Refer to the comments below
 * in main() for scswitch_evacuate_devices()
 */
#define	EVACUATE_DEVICES_RETRY_LIMIT 5
#define	EVACUATE_DEVICES_RETRY_DELAY 5

/*
 * scshutdown_node_t holds the list of node id's
 * for all the nodes in the cluster.
 */
typedef struct scshutdown_node_struct {
	char				*scshutdown_nodename;
	nodeid_t			scshutdown_nodeid;
	int				online;
	struct scshutdown_node_struct	*next;
} scshutdown_node_t;

/*
 * scshutdown_cluster_t holds cluster name and
 * a list of all the node id's in the cluster.
 */
typedef struct scshutdown_cluster_struct {
	char			*scshutdown_cluster_name;
	scshutdown_node_t	*scshutdown_nodelist;
} scshutdown_cluster_t;

#define	SHUTDOWN_USER_CMD	"/usr/sbin/init 0"

#if SOL_VERSION >= __s10
#define	SHUTDOWN_ZC_CMD		"/usr/cluster/bin/clzonecluster halt +"
#endif /* SOL_VERSION >= __s10 */

static char *progname;
static int exit_code;

/*
 *	Prototypes
 */

static void usage(void);
static void print_error(scswitch_errno_t);
static void scshutdown_process_grace_period(scshutdown_cluster_t *, int,
    int, char *, boolean_t *);
static scswitch_errno_t scshutdown_get_node_info(scshutdown_node_t **);
static scswitch_errno_t scshutdown_get_cluster_info(scshutdown_cluster_t **);
static void scshutdown_free_nodeinfo(scshutdown_node_t *);
static void scshutdown_free_clusterinfo(scshutdown_cluster_t *);
static void scshutdown_send_startmsg_to_cluster(scshutdown_cluster_t *,
    char *, char *);
static void scshutdown_send_abortmsg_to_cluster(scshutdown_cluster_t *);
static int scshutdown_send_wall_msg(char *msgbuf, nodeid_t nodeid);
static int scshutdown_send_rwall_msg(char *msgbuf, nodeid_t nodeid);
static char *scshutdown_get_user_message_buf(int, int, char **);
static int scshutdown_set_cluster_shutdown(void);
static dc_error_t scshutdown_dsm(scshutdown_cluster_t *);
static dc_error_t scshutdown_nodes(scshutdown_node_t *, scshutdown_cluster_t *);
static int scshutdown_umountall(char *);
static int scshutdown_dotdevices_umountall(scshutdown_cluster_t *, nodeid_t,
	char *);
static int scshutdown_appendtolist(const char *mount_point);
static int do_force_unmount(char *progname, char *mntpt, int force_flag);

#if SOL_VERSION >= __s10
static clconf_errnum_t scshutdown_zc(nodeid_t);
#endif

/* end prototypes */

/*
 * Main function for scshutdown
 *
 *	main()
 */
int
main(int argc, char **argv)
{
	int c, count;
	int yflg = 0, gflg = 0;
	int grace = DEFAULT_GRACE_PERIOD;
	char *user_msg_buf;
	scconf_errno_t scconferr = SCCONF_NOERR;
	scswitch_errbuff_t scshutdownerr = { SCSWITCH_NOERR, NULL };
	scha_errmsg_t schaerror = { SCHA_ERR_NOERR, NULL };
	scshutdown_cluster_t *cluster_info = NULL;
	scshutdown_node_t *node_info = NULL;
	uint_t ismember;
	boolean_t shutdown = B_FALSE;
	int error;
	int i;
	int ret;
#if (SOL_VERSION >= __s10)
	nodeid_t nid;
	zoneid_t zoneid = getzoneid();
#endif

	/* Set the program name */
	if ((progname = strrchr(argv[0], '/')) == NULL) {
		progname = argv[0];
	} else {
		++progname;
	}

	/* I18N housekeeping */
	(void) setlocale(LC_ALL, "");
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);
	(void) textdomain(TEXT_DOMAIN);

#if (SOL_VERSION >= __s10)
	/*
	 * Check to see if we are running in a native non-global zone.
	 * If so, exit.
	 * scshutdown cannot run in a native non-global zone.
	 */
	if (zoneid != GLOBAL_ZONEID) {
		char cl_name[ZONENAME_MAX];
		cz_id_t czid;
		czid.clid = 0;
		if (getzonenamebyid(zoneid, cl_name,
		    ZONENAME_MAX) == -1) {
			(void) fprintf(stderr, "Failed to get cluster name\n");
			exit(ENOENT);
		}
		strcpy(czid.name, cl_name);
		if (ret = cladm(CL_CONFIG, CL_GET_ZC_ID, &czid) != 0) {
			(void) fprintf(stderr,
			    "Failed to get cluster ID, error %d\n", ret);
			exit(ret);
		}
		if (czid.clid == BASE_NONGLOBAL_ID) {
			/* Inside native non-global zone */
			(void) fprintf(stderr, "Cannot run scshutdown in "
			    "a native non-global zone\n");
			exit(ENOENT);
		}
	}
#endif

	/* Initialize and promote to euid=0 priviledge */
	cl_auth_init();
	cl_auth_promote();

	/* XXX this privileged code would be better after getopt() below */
	/* Make sure that we are active in the cluster */
	ismember = 0;
	scconferr = scconf_ismember(0, &ismember);
	scshutdownerr.err_code = scswitch_convert_scconf_error_code(
	    scconferr);
	if (scshutdownerr.err_code != SCSWITCH_NOERR) {
		print_error(scshutdownerr.err_code);
		exit(1);
	}

	if (ismember == 0) {
		(void) fprintf(stderr, gettext(
		    "%s:  This node is not currently in the cluster.\n"),
		    progname);
		exit(1);
	}

	/* no longer privileged */
	cl_auth_demote();

	/* Check for basic options */
	yflg = gflg = 0;
	while ((c = getopt(argc, argv, "yg:")) != EOF) {
		switch (c) {
		case 'y':		/* ask confirmation */
			yflg++;
			break;

		case 'g':		/* get grace period */
			for (i = 0; optarg[i]; i++) {
				if (!isdigit(optarg[i])) {
					(void) fprintf(stderr, gettext(
					    "%s: -g requires a numeric "
					    "option.\n"), progname);
					usage();
					exit(1);
				}
			}
			gflg++;
			grace = atoi(optarg);
			break;

		case '?':
		default:
			usage();
			exit(1);
		}
	}

	/* Check for required/excessive suboptions */
	if ((yflg > 1) || (gflg > 1)) {
		usage();
		exit(1);
	}

	/* Check authorization */
	cl_auth_check_command_opt_exit(*argv, NULL, CL_AUTH_SYSTEM_ADMIN, 1);

	/* Promote to euid=0 */
	cl_auth_promote();

	/*
	 * user_msg_buf holds the message string specified by the user
	 * with the shutdown message.
	 */
	user_msg_buf = scshutdown_get_user_message_buf(optind, argc, argv);
	if (user_msg_buf == NULL) {
		print_error(SCSWITCH_ENOMEM);
		exit((int)(SCSWITCH_ENOMEM));
	}

	/* Get node info */
	scshutdownerr.err_code = scshutdown_get_node_info(&node_info);
	if (scshutdownerr.err_code != SCSWITCH_NOERR) {
		print_error(scshutdownerr.err_code);
		exit((int)scshutdownerr.err_code);
	}

	/* Get cluster info */
	scshutdownerr.err_code = scshutdown_get_cluster_info(&cluster_info);
	if (scshutdownerr.err_code != SCSWITCH_NOERR) {
		print_error(scshutdownerr.err_code);
		exit((int)scshutdownerr.err_code);
	}

	/* Generate event */
	exit_code = 0;
	cl_cmd_event_init(argc, argv, &exit_code);
	(void) cl_cmd_event_gen_start_event();
	(void) atexit(cl_cmd_event_gen_end_event);

	/* Sends shutdown messages to cluster nodes */
	scshutdown_process_grace_period(cluster_info, grace, yflg,
	    user_msg_buf, &shutdown);

	/* See if the user answered "yes" to the shutdown question. */
	if (!shutdown) {
		exit_code = 1;
		exit(1);
	}

#if (SOL_VERSION >= __s10)
	/*
	 * If we are in the global zone, before we shut down the
	 * base cluster, we will gracefully shut down all the
	 * zone clusters if there are any.
	 */
	if (zoneid == GLOBAL_ZONEID) {
		nid = clconf_get_local_nodeid();
		error = scshutdown_zc(nid);
		if (error != 0) {
			(void) fprintf(stderr, gettext(
			    "%s:  Could not halt all the zone clusters.\n"),
			    progname);
			exit(error);
		}
	}
#endif /* SOL_VERSION >= __s10 */

	/*
	 * Shutdown  all the RGs in the cluster.
	 * Set the verbose_flag to B_FALSE.
	 */
	schaerror = scswitch_evacuate_rgs(NULL, 0, B_FALSE, NULL);
	if (schaerror.err_code != SCSWITCH_NOERR) {
		char *errormessage = schaerror.err_msg ? schaerror.err_msg :
			rgm_error_msg(schaerror.err_code);
		(void) fprintf(stderr, gettext(
		    "%s: Could not shut down all resource groups in "
		    "the cluster: %s\n"),
		    progname, errormessage);
		exit_code = (int)scshutdownerr.err_code;
		exit((int)schaerror.err_code);
	}

#if (SOL_VERSION >= __s10)
	if (zoneid != GLOBAL_ZONEID) {
		goto skip_global;
	}
#endif /* SOL_VERSION >= __s10 */

	/*
	 * Unmount all global filesystems in the cluster.
	 */
	error = scshutdown_umountall(progname);
	if (error != 0) {
		(void) fprintf(stderr, gettext(
		    "%s:  Could not unmount all PxFS filesystems.\n"),
		    progname);
		exit_code = error;
		exit(error);
	}

	/*
	 * Evacuate all the dcs device groups in the cluster.
	 *
	 * It can actually take some time (several seconds) for a device group
	 * to become idle even after all processes that were using it have
	 * exited. This is attributed to the deferred processing of inactive,
	 * checkpointing and unreference requests in the kernel. Therefore the
	 * scswitch_evacuate_devices() function can fail (SCSWITCH_EBUSY),
	 * even though nothing is using a device service anymore. We have no
	 * way of telling (from up here in userland) if a busy device service
	 * will soon be inactive, all we know is that it's currently busy. So
	 * we retry scswitch_evacuate_devices() in a delay loop. Note that the
	 * message printed when a device service is still busy is appended
	 * with a dot on each retry so that the admin knows the command is
	 * still processing (in case they're tempted to ctrl+C it).
	 */
	for (count = 1; count <= EVACUATE_DEVICES_RETRY_LIMIT; count++) {
		scshutdownerr = scswitch_evacuate_devices(NODEID_UNKNOWN);

		if (scshutdownerr.err_code != SCSWITCH_EBUSY) {
			break;
		} else {
			if (count == 1) {
				(void) fprintf(stderr, gettext("%s: Waiting "
				    "for devices services to become idle."),
				    progname);
			} else if (count == EVACUATE_DEVICES_RETRY_LIMIT) {
				(void) fprintf(stderr, gettext(".\n"));
				break;
			} else {
				(void) fprintf(stderr, gettext("."));
			}

			free(scshutdownerr.err_msg);
			(void) sleep((uint_t)EVACUATE_DEVICES_RETRY_DELAY);
		}
	}

	if (scshutdownerr.err_code != SCSWITCH_NOERR) {
		(void) fprintf(stderr, gettext("%s: Could not "
		    "shutdown device service in the cluster: %s\n"),
		    progname, scshutdownerr.err_msg);
		free(scshutdownerr.err_msg);
		exit_code = 1;
		exit(1);
	}

	error = scshutdown_dotdevices_umountall(cluster_info,
	    node_info->scshutdown_nodeid, progname);
	if (error != 0) {
		(void) fprintf(stderr, gettext(
		    "%s:  Could not unmount all PxFS filesystems.\n"),
		    progname);
		exit_code = error;
		exit(error);
	}

	/*
	 * Shutdown the DSM on all cluster nodes.
	 */
	error = scshutdown_dsm(cluster_info);
	if (error != 0) {
		(void) fprintf(stderr, gettext("%s: Could not "
		    "shut down all device services in the cluster.\n"),
		    progname);
		exit_code = error;
		exit(error);
	}

skip_global:
	/* Signal cluster shutdown on all nodes */
	error = scshutdown_set_cluster_shutdown();
	if (error != 0) {
		exit_code = error;
		exit(error);
	}

	/*
	 * Execute "init 0" on all cluster nodes.
	 */
	error = scshutdown_nodes(node_info, cluster_info);
	if (error != 0) {
		/*
		 * The error message has already been printed out by
		 * 'scshutdown_nodes'.  Just exit with an error.
		 */
		exit_code = error;
		exit(error);
	}

	free(user_msg_buf);
	scshutdown_free_nodeinfo(node_info);
	scshutdown_free_clusterinfo(cluster_info);
	return (0);
}

/*
 * Disable the DSMs on all cluster nodes.
 */
static dc_error_t
scshutdown_dsm(scshutdown_cluster_t *cluster_info)
{
	dc_error_t err = DCS_SUCCESS;
	scshutdown_node_t *node = NULL;
	scshutdown_node_t *node2 = NULL;
	nodeid_t tmpid;

	ASSERT(cluster_info);

	/* Walk the list of active nodes, disabling the DSM on each one. */
	node = cluster_info->scshutdown_nodelist;
	while (node != NULL) {
		if (node->online == 0) {
			node = node->next;
			continue;
		}
		tmpid = node->scshutdown_nodeid;
		err = dcs_disable_dsm(tmpid);
		if (err != DCS_SUCCESS) {
			/*
			 * Failure to disable one of the DSMs.  Enable all
			 * the ones we've disabled so far, and then return with
			 * the error.
			 */
			node2 = cluster_info->scshutdown_nodelist;
			while (node2 != node) {
				tmpid = node->scshutdown_nodeid;
				(void) dcs_enable_dsm(tmpid);
				node2 = node2->next;
			}
			break;
		}
		node = node->next;
	}

	return (err);
}

#if SOL_VERSION >= __s10
/*
 * Executes a command to gracefully shut down all the zone clusters.
 */
clconf_errnum_t
scshutdown_zc(nodeid_t my_node)
{
	clconf_errnum_t err;

	err = clconf_do_execution(SHUTDOWN_ZC_CMD, my_node,
	    NULL, B_FALSE, B_TRUE, B_TRUE);
	if (err != CL_NOERROR) {
		/*
		 * Error executing the zc halt command.
		 */
		if (err == CL_INVALID_OBJ) {
			(void) fprintf(stderr, gettext("Error halting zone "
			    "clusters. clexecd not found on node %d.\n"),
			    my_node);
		} else {
			(void) fprintf(stderr, gettext("Error halting zone "
			    "clusters. Unexpected internal error, errnum = "
			    "%d.\n"), err);
		}
	}
	return (err);
}
#endif /* SOL_VERSION >= __s10 */

/*
 * This call should be passed in a 'nodeid_t' casted as a 'void *'.
 */
static void *
scshutdown_node(void *arg)
{
	clconf_errnum_t err;
	scshutdown_node_t *node = (scshutdown_node_t *)arg;

	/*
	 * This call is made in a separate thread, so we need a different
	 * buffer for the error string.
	 */

	err = clconf_do_execution(SHUTDOWN_USER_CMD, node->scshutdown_nodeid,
	    NULL, B_FALSE, B_TRUE, B_TRUE);
	if (err != CL_NOERROR) {
		/*
		 * Error executing the init command on one of the nodes.
		 * It's too late to stop, so print out an error message
		 * and continue.
		 */
		if (err == CL_NO_CLUSTER)
			(void) fprintf(stderr, gettext("Error halting node %s."
			    "Node is not in the cluster\n"),
			    node->scshutdown_nodename);
		else if (err == CL_INVALID_OBJ)
			(void) fprintf(stderr, gettext("Error halting node %s."
			    "Not found clexecd on node.\n"),
			    node->scshutdown_nodename);
		else
			(void) fprintf(stderr, gettext("Error halting node %s."
			    "Unexpected internal error.\n"),
			    node->scshutdown_nodename);
	}

	return ((void *)err);
}

/*
 * Shutdown all nodes in the cluster.   "my_node" should be the node
 * from which this command is run;   it is always shutdown last.
 */
static dc_error_t
scshutdown_nodes(scshutdown_node_t *my_node,
    scshutdown_cluster_t *cluster_info)
{
	scshutdown_node_t *node = NULL;
	int err = 0;
	thread_t tid;

	ASSERT(my_node != NULL);
	ASSERT(cluster_info != NULL);

	/* Walk the list of active nodes, and call "init 0" on each one */
	node = cluster_info->scshutdown_nodelist;
	while (node != NULL) {
		if (node->online == 0) {
			node = node->next;
			continue;
		}
		/* Skip the node this command is running on - do that last. */
		if (node->scshutdown_nodeid == my_node->scshutdown_nodeid) {
			node = node->next;
			continue;
		}
		err = thr_create(NULL, NULL, scshutdown_node, (void *)node,
		    NULL, &tid);
		if (err != 0) {
			/*
			 * Error creating a thread to shutdown one of the
			 * remote nodes.  It's too late to stop, so print out
			 * an error message and continue.
			 */
			(void) fprintf(stderr, gettext("%s: Could not create"
			    " a thread to halt node %s. Halt node %s"
			    " manually. \n"), progname,
			    node->scshutdown_nodeid, node->scshutdown_nodeid);
		}
		node = node->next;
	}

	/* Now its time to do me. */
	err = (int)scshutdown_node((void *)my_node);

	return (err);
}

/* Variables that store the list of mountpoints from /etc/mnttab. */
static char *mp_buf = NULL;
static int *mount_points = NULL;
static size_t curr_mp_pos = 0;

/*
 * Helper function used to to build a list of mountpoints to unmount.
 * Returns 0 if successful, and 1 if out of memory.
 */
static int
scshutdown_appendtolist(const char *mount_point)
{
	static size_t mp_buf_increment = 1024;
	static size_t curr_mp_buf_pos = 0;
	static size_t max_mp_buf_pos = 0;
	static size_t mp_increment = 50;
	static size_t max_mp_pos = 0;

	size_t len;
	void *tmp;

	ASSERT(curr_mp_buf_pos <= max_mp_buf_pos);
	ASSERT(curr_mp_pos <= max_mp_pos);

	len = strlen(mount_point) + 1;
	/* Confirm that we have enough memory in 'mp_buf'. */
	if ((curr_mp_buf_pos + len) > max_mp_buf_pos) {
		if (len > mp_buf_increment) {
			max_mp_buf_pos += len;
		} else {
			max_mp_buf_pos += mp_buf_increment;
		}
		tmp = realloc((void *)mp_buf, max_mp_buf_pos * sizeof (char));
		if (tmp == NULL) {
			goto nomem;
		}
		mp_buf = (char *)tmp;
	}

	/* Confirm that we have enough memory in 'mount_points'. */
	if (curr_mp_pos >= max_mp_pos) {
		max_mp_pos += mp_increment;
		tmp = realloc((void *)mount_points, max_mp_pos * sizeof (int));
		if (tmp == NULL) {
			goto nomem;
		}
		mount_points = (int *)tmp;
	}

	/* Store the new mount point at the end */
	(void) strcpy(mp_buf + curr_mp_buf_pos, mount_point);
	mount_points[curr_mp_pos++] = (int)curr_mp_buf_pos;
	curr_mp_buf_pos += len;

	return (0);

nomem:
	/* Error case - clean up and return. */
	free(mp_buf);
	free(mount_points);
	mp_buf = NULL;
	mount_points = NULL;
	curr_mp_buf_pos = max_mp_buf_pos = curr_mp_pos = max_mp_pos = 0;
	return (1);
}

/*
 * Unmount all global filesystems.
 */
static int
scshutdown_umountall(char *prgname)
{
	FILE *fp;
	struct mnttab mnt;
	int ret;
	int err;
	int error_code;
	char *tmp;
	int i;
	size_t len = strlen(GDEV_PATH);
	sc_syslog_msg_handle_t sys_handle;

	/* Open /etc/mnttab in read-only mode. */
	fp = fopen("/etc/mnttab", "r");
	if (fp == NULL) {
		(void) fprintf(stderr, gettext("%s: Error opening /etc/mnttab."
		    " Fix errors and retry. \n"), prgname);
		return (1);
	}

	/* Iterate through entries in mnttab. */
	for (;;) {
		ret = getmntent(fp, &mnt);
		if (ret != 0) {
			break;
		}
		/*
		 * Look for the occurence of "global" in the mount
		 * option string.
		 */
		tmp = mnt.mnt_mntopts;
		while ((tmp = strstr(tmp, MNTOPT_GLOBAL)) != NULL) {
			/*
			 * Is this the "global" mount option or some
			 * other mount option that contains "global" as
			 * a substring?
			 */
			if ((tmp != mnt.mnt_mntopts) &&
			    (*(tmp - 1) != MNTOPT_SEPARATOR)) {
				tmp += (sizeof (MNTOPT_GLOBAL) - 1);
				continue;
			}

			tmp += (sizeof (MNTOPT_GLOBAL) - 1);
			if ((*tmp != MNTOPT_SEPARATOR) && (*tmp != 0)) {
				continue;
			}

			if (strncmp(GDEV_PATH, mnt.mnt_mountp, len) == 0) {
				/*
				 * Skip all the /global/.devices/node@ mounts
				 * so that retries of 'scshutdown' will succeed.
				 */
				break;
			}

			if (scshutdown_appendtolist(mnt.mnt_special) != 0) {
				print_error(SCSWITCH_ENOMEM);
				exit_code = (int)(SCSWITCH_ENOMEM);
				exit((int)(SCSWITCH_ENOMEM));
			};
		}
	}

	if (ret != -1) {
		(void) fprintf(stderr, gettext("%s: Error reading from"
		    " /etc/mnttab. Fix errors and retry. \n"), prgname);
		return (1);
	}

	/*
	 * Now that all the entries are in 'mount_points', we unmount them in
	 * reverse order - this way, we can handle nested mounts. If an
	 * unmount fails because the filesystem is busy (EBUSY), then the
	 * the unmount is retried using the force flag (MS_FORCE).
	 */
	for (i = (int)curr_mp_pos-1; i >= 0; i--) {
		err = umount(mp_buf + mount_points[i]);
		if (err == -1) {
			/*lint -e746 */
			error_code = errno;
			/*lint +e746 */
			switch (error_code) {
				case EINVAL:
					/*
					 * Invalid entry in /etc/mnttab.
					 * Go on with the next entry.
					 */
					continue;
				case EBUSY:
					err = do_force_unmount(prgname,
					    mp_buf + mount_points[i],
					    MS_FORCE);
					/* fall through */
				default:
					if (err < 0) {
						/*lint -e746 */
						error_code = errno;
						(void) fprintf(stderr,
						    gettext("%s: Unmount of %s "
						    "failed: %s. \n"), prgname,
						    mp_buf + mount_points[i],
						    strerror(error_code));
						return (error_code);
					}
					break;
			}
		}
	}

	free(mp_buf);
	free(mount_points);

	return (0);
}

static int
scshutdown_dotdevices_umountall(scshutdown_cluster_t *cluster_info, nodeid_t id,
    char *prgname)
{
	int err;
	int error_code;
	char gdevices_mnt[100];
	scshutdown_node_t *node = NULL;

	/*
	 * The other unmounts succeeded - try unmounting all the
	 * /global/.devices filesystems in the cluster.  We do this step as
	 * stage 2 because these filesystems need to exist for retries
	 * of 'scshutdown' from other nodes to work.
	 */
	node = cluster_info->scshutdown_nodelist;
	while (node != NULL) {
		/* Skip dead nodes, and the node we are on. */
		if ((node->online == 0) || (node->scshutdown_nodeid == id)) {
			node = node->next;
			continue;
		}
		(void) sprintf(gdevices_mnt, "%s%d", GDEV_PATH,
		    node->scshutdown_nodeid);
		err = umount(gdevices_mnt);
		if (err == -1) {
			/*lint -e746 */
			error_code = errno;
			/*lint +e746 */
			switch (error_code) {
				case EINVAL:
					/*
					 * Invalid entry in /etc/mnttab.
					 * Move to next entry.
					 */
					node = node->next;
					continue;
				case EBUSY:
					err = do_force_unmount(prgname,
					    gdevices_mnt, MS_FORCE);
					/* fall through */
				default:
					if (err < 0) {
						error_code = errno;
						(void) fprintf(stderr,
						    gettext(
						    "%s: Unmount of %s failed:"
						    " %s.\n"),
						    prgname, gdevices_mnt,
						    strerror(error_code));
						return (error_code);
					}
					break;
			}
		}
		node = node->next;
	}

	/*
	 * Unmount the local /global/.devices filesystem - this has to be last
	 * because we need the device nodes to exist for the other unmounts to
	 * succeed.
	 */
	(void) sprintf(gdevices_mnt, "%s%d", GDEV_PATH, id);
	err = umount(gdevices_mnt);
	if (err == -1) {
		error_code = errno;
		switch (error_code) {
			case EINVAL:
				/*
				 * Invalid entry in /etc/mnttab.
				 * Return normally.
				 */
				return (0);
			case EBUSY:
				err = do_force_unmount(prgname, gdevices_mnt,
				    MS_FORCE);
				/* fall through */
			default:
				if (err < 0) {
					error_code = errno;
					(void) fprintf(stderr,
					    gettext(
					    "%s: Unmount of %s failed: %s.\n"),
					    prgname, gdevices_mnt,
					    strerror(error_code));
					return (error_code);
				}
				break;
		}

	}
	return (0);
}

int
do_force_unmount(char *progname, char *mntpt, int force_flag)
{
	int err;
	sc_syslog_msg_handle_t sys_handle;

	/*
	 * SCMSGS
	 * @explanation
	 * An attempt to unmount a global filesystem using umount(2)
	 * was unsuccessful. The system will retry to unmount the global
	 * filesystem using umount2(2) with the force option (MS_FORCE).
	 * @user_action
	 * None.
	 */

	(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
	    "%s: Warning: The force option is being used to unmount"
	    " %s", progname, mntpt);

	(void) fprintf(stderr, gettext("%s: Warning: The force option"
	    " is being used to unmount %s\n"), progname, mntpt);

	err = umount2(mntpt, force_flag);
	if (err == 0) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_SCSHUTDOWN_TAG, "");

		/*
		 * SCMSGS
		 * @explanation
		 * The scshutdown command broadcasts a warning message of an
		 * impending cluster shutdown and provides a grace period
		 * for users to desist usage of filesystems. Subsequently,
		 * if scshutdown cannot unmount a global filesystem because
		 * of continued activity, scshutdown uses the force option
		 * for unmount, regardless of user activity.
		 * @user_action
		 * None.
		 */

		(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
		    "%s: Warning: The force option was needed to unmount"
		    " %s", progname, mntpt);

		(void) fprintf(stderr, gettext("%s: Warning: The force "
			"option was needed to unmount %s\n"), progname, mntpt);

	}
	(void) sc_syslog_msg_done(&sys_handle);
	return (err);
}

/*
 * usage
 *
 *	Print usage message.
 */
static void
usage()
{
	/* shutdown the cluster cluster */
	(void) fprintf(stderr,
	    "usage:  %s [ -y ] [ -g <grace-period> ] [message]\n", progname);
}

/*
 * scshutdown_get_user_message_buf
 *
 *	Get the message string entered with the shutdown command.
 */
static char *
scshutdown_get_user_message_buf(int optidx, int argc, char **argv)
{
	int i;
	char *buf = (char *)malloc(sizeof (char) * BUFSIZ);

	if (buf == NULL) {
		return (NULL);
	}

	bzero(buf, BUFSIZ);
	i = optidx;
	for (; i < argc; i++) {
		/* Get the message string */
		(void) strcat(buf, argv[i]);
		(void) strcat(buf, " ");
	}

	return (buf);
}

/*
 * print_error
 *
 *      Print error to stderr.
 */
static void
print_error(scswitch_errno_t error)
{
	char text[BUFSIZ];
	scswitch_strerr(NULL, text, error);

	(void) fprintf(stderr, "%s. \n", text);
}

/*
 * scshutdown_process_grace_period
 *
 * Sends shutdown related messages after a regular interval, and asks
 * for approval to continue with the shutdown.
 *
 * Possible return value:
 *	SCSWITCH_NOERR		- no error
 *	SCSWITCH_ENOMEM		- not enough memory
 */
static void
scshutdown_process_grace_period(scshutdown_cluster_t *cluster_info,
    int graceperiod, int askconfirmation, char *user_msg_buf,
    boolean_t *shutdown)
{
	int gracetable[] = {
		7200, 3600, 1800, 1200, 600, 300, 120, 60, 30, 10};
	char buftime[BUFSIZ];
	int hours, minutes, seconds, ti, i;
	char *rptr;
	char replybuf[BUFSIZ];
	size_t sz = 0;

	hours = minutes = seconds = ti = i = 0;
	/*lint -save -e661 */
	for (ti = gracetable[i]; i < 10; i++, ti = gracetable[i]) {
		if (graceperiod > ti) {
			hours = graceperiod / 3600;
			minutes = graceperiod % 3600 / 60;
			seconds = graceperiod % 60;
			buftime[0] = '\0';

			if (hours > 1)
				(void) sprintf(buftime, "%d hours", hours);
			else if (hours == 1)
				(void) sprintf(buftime, "%d hour", hours);

			if (minutes > 1)
				(void) sprintf(buftime, "%s %d minutes",
				    buftime, minutes);
			else if (minutes == 1)
				(void) sprintf(buftime, "%s %d minute",
				    buftime, minutes);

			if ((hours == 0) && (seconds > 0)) {
				if (seconds == 1)
					(void) sprintf(buftime, "%s %d second",
					    buftime, seconds);
				else
					(void) sprintf(buftime,
					    "%s %d seconds", buftime, seconds);
			}

			scshutdown_send_startmsg_to_cluster(
			    cluster_info, buftime, user_msg_buf);
			(void) sleep((uint_t)(graceperiod - ti));
			graceperiod = ti;
		}
	}
	/*lint -restore */

	if (!askconfirmation) {
		/* Mimic interface look of the Solaris shutdown(1m) command */
		if (strcmp(gettext("yes"), "yes") == 0 &&
		    strcmp(gettext("no"), "no") == 0) {
			(void) fprintf(stderr,
			    gettext("Do you want to continue? (y or n):   "));
			rptr = fgets(replybuf, sizeof (replybuf) - 1, stdin);
			if (rptr) {
				sz = strlen(replybuf) - 1;
				if (strncasecmp(replybuf, "y", sz) == 0 ||
				    strncasecmp(replybuf, "yes", sz) == 0) {
					*shutdown = B_TRUE;
					return;
				}
			}
		} else {
			(void) fprintf(stderr,
			    gettext("Do you want to continue? (%s or %s):   "),
			    gettext("yes"), gettext("no"));
			rptr = fgets(replybuf, sizeof (replybuf) - 1, stdin);
			if (rptr) {
				while (sz < BUFSIZ && replybuf[sz] != '\n')
					sz++;
				if (strncasecmp(replybuf, gettext("yes"), sz)
				    == 0) {
					*shutdown = B_TRUE;
					return;
				}
			}
		}
		scshutdown_send_abortmsg_to_cluster(cluster_info);
		*shutdown = B_FALSE;
		exit_code = 10;
		exit(10);
	} else {
		*shutdown = B_TRUE;
	}
}

/*
 * scshutdown_get_node_info
 *
 * Upon success, an object of the scshutdown_node_t is returned and it
 * is the caller's  responsibility to free it.
 *
 * Possible return value:
 *	SCSWITCH_NOERR		- no error
 *	SCSWITCH_ENOMEM		- not enough memory
 *	SCSWITCH_EPERM		- not root
 *      SCSWITCH_ENOCLUSTER	- there is no cluster
 *      SCCONF_EINVAL		- invalid argument
 *      SCCONF_EUNEXPECTED      - internal or unexpected error
 */
static scswitch_errno_t
scshutdown_get_node_info(scshutdown_node_t **ppnodeinfo)
{
	scswitch_errno_t error = SCSWITCH_NOERR;
	scconf_errno_t scconf_error = SCCONF_NOERR;
	scshutdown_node_t *pnodeinfo = NULL;
	scconf_nodeid_t nodeidp;
	char *nodenamep = NULL;

	if (ppnodeinfo == NULL) {
		error = SCSWITCH_EINVAL;
		goto cleanup;
	}
	pnodeinfo = (scshutdown_node_t *)calloc(1, sizeof (scshutdown_node_t));
	if (pnodeinfo == NULL) {
		error = SCSWITCH_ENOMEM;
		goto cleanup;
	}
	pnodeinfo->next = NULL;
	pnodeinfo->online = 1;

	/* get node id */
	scconf_error = scconf_get_nodeid(NULL, &nodeidp);
	error = scswitch_convert_scconf_error_code(scconf_error);
	if (error != SCSWITCH_NOERR)
		goto cleanup;
	pnodeinfo->scshutdown_nodeid = nodeidp;
	/* get node name */
	scconf_error = scconf_get_nodename(nodeidp, &nodenamep);
	error = scswitch_convert_scconf_error_code(
	    scconf_error);
	if (error != SCSWITCH_NOERR)
		goto cleanup;

	pnodeinfo->scshutdown_nodename = strdup(nodenamep);
	if (pnodeinfo->scshutdown_nodename == NULL) {
		error = SCSWITCH_ENOMEM;
		goto cleanup;
	}

	*ppnodeinfo = pnodeinfo;

cleanup:
	if (nodenamep)
		free(nodenamep);
	return (error);
}

/*
 * scshutdown_get_cluster_info
 *
 * Upon success, an object of scshutdown_cluster_t is returned.
 * The caller is responsible for freeing the space.
 *
 * Possible return values:
 *
 *      SCSWITCH_NOERR		- success
 *      SCSWITCH_ENOMEM		- not enough memory
 *	SCSWITCH_ENOCLUSTER	- not a cluster node
 *	SCSWITCH_EINVAL		- scconf: invalid argument
 *	SCSWITCH_EPERM		- not root
 *      SCCONF_EUNEXPECTED      - internal or unexpected error
 */
static scswitch_errno_t
scshutdown_get_cluster_info(scshutdown_cluster_t **ppcluster)
{
	scswitch_errno_t error = SCSWITCH_NOERR;
	scconf_errno_t scconf_error = SCCONF_NOERR;
	scshutdown_cluster_t *pcluster = NULL;
	scshutdown_node_t *pnode = NULL;
	scshutdown_node_t *pnode_current = NULL;
	scconf_cfg_cluster_t *pcluster_cfg = NULL;
	scconf_nodeid_t nodeidp;

	scstat_node_t *tmp;
	scstat_node_t *cl_nodes;
	scstat_errno_t scstat_err;
	uint32_t i;

#if (SOL_VERSION >= __s10)
	int ret_val;
	uint_t clid;
	char cl_name[ZONENAME_MAX];
	quorum_status_t *clust_memb = NULL;
	char *nodenamep = NULL;
	zoneid_t zoneid;
	clconf_cluster_t *clp = NULL;
#endif

	if (ppcluster == NULL) {
		error = SCSWITCH_EINVAL;
		goto cleanup;
	}

	pcluster = (scshutdown_cluster_t *)calloc(1,
	    sizeof (scshutdown_cluster_t));
	if (pcluster == NULL) {
		error = SCSWITCH_ENOMEM;
		goto cleanup;
	}

#if (SOL_VERSION >= __s10)
	zoneid = getzoneid();
	if (zoneid != GLOBAL_ZONEID) {
		/*
		 * If we are running in a native non-global zone,
		 * we would have exited much earlier.
		 * Reaching here means we are running in a zone cluster.
		 */

		if (clconf_lib_init() != 0) {
			(void) fprintf(stderr, "Failed to initialize clconf\n");
			error = SCSWITCH_EUNEXPECTED;
			goto cleanup;
		}

		if (getzonenamebyid(zoneid, cl_name, ZONENAME_MAX) == -1) {
			(void) fprintf(stderr, "Failed to get cluster name\n");
			error = SCSWITCH_EUNEXPECTED;
			goto cleanup;
		}

		ret_val = clconf_get_cluster_id(cl_name, &clid);
		if (ret_val == ENOENT) {
			/*
			 * We are querying the cluster id of
			 * the zone we are running in.
			 * ENOENT means the zone does not exist;
			 * we do not expect that.
			 */
			ASSERT(0);
			error = SCSWITCH_ENOCLUSTER;
			goto cleanup;
		} else if (ret_val == EACCES) {
			/*
			 * We should be able to get the current cluster id.
			 */
			ASSERT(0);
			error = SCSWITCH_EUNEXPECTED;
			goto cleanup;
		} else if (ret_val != 0) {
			/* Unexpected error */
			ASSERT(0);
			error = SCSWITCH_EUNEXPECTED;
			goto cleanup;
		}

		/* Sanity check that we are running in a zone cluster */
		ASSERT(clid >= MIN_CLUSTER_ID);

		/* get name of cluster */
		pcluster->scshutdown_cluster_name = strdup(cl_name);
		if (pcluster->scshutdown_cluster_name == NULL) {
			error = SCSWITCH_ENOMEM;
			goto cleanup;
		}

		/* get the clconf object */
		clp = clconf_cluster_get_current();
		if (clp == NULL) {
			error = SCSWITCH_EUNEXPECTED;
			goto cleanup;
		}

		if (cluster_get_quorum_status(&clust_memb) != 0) {
			error = SCSWITCH_EUNEXPECTED;
			goto cleanup;
		}

		for (i = 0; i < clust_memb->num_nodes; i++) {
			pnode = (scshutdown_node_t *)calloc(1,
			    sizeof (scshutdown_node_t));
			if (pnode == NULL) {
				error = SCSWITCH_ENOMEM;
				goto cleanup;
			}

			/* Get nodename */
			nodenamep =
			    (char *)clconf_cluster_get_nodename_by_nodeid(
			    clp, clust_memb->nodelist[i].nid);
			if (nodenamep == NULL) {
				error = SCSWITCH_EUNEXPECTED;
				goto cleanup;
			}

			pnode->scshutdown_nodename = strdup(nodenamep);
			if (pnode->scshutdown_nodename == NULL) {
				error = SCSWITCH_EUNEXPECTED;
				goto cleanup;
			}

			pnode->scshutdown_nodeid = clust_memb->nodelist[i].nid;
			if (clust_memb->nodelist[i].state
			    == QUORUM_STATE_ONLINE) {
				pnode->online = 1;
			} else {
				pnode->online = 0;
			}

			/* append it to the list */
			if (pnode_current != NULL) {
				pnode_current->next = pnode;
			} else { /* first element */
				pcluster->scshutdown_nodelist = pnode;
			}
			pnode_current = pnode;
			pnode = NULL; /* so we don't accidentally free it */
		}

		*ppcluster = pcluster;
		pcluster = NULL; /* don't free it if no error */
		cluster_release_quorum_status(clust_memb);
		goto cleanup;
	}
#endif

	/* get cluster config from scconf */
	scconf_error = scconf_get_clusterconfig(&pcluster_cfg);
	error = scswitch_convert_scconf_error_code(scconf_error);
	if (error != SCSWITCH_NOERR) {
		goto cleanup;
	}

	/* get name of cluster */
	pcluster->scshutdown_cluster_name = strdup(
	    pcluster_cfg->scconf_cluster_clustername);
	if (pcluster->scshutdown_cluster_name == NULL) {
		error = SCSWITCH_ENOMEM;
		goto cleanup;
	}

	/* get cluster config from scstat */
	scstat_err = scstat_get_nodes(&cl_nodes);
	if (scstat_err != SCSTAT_ENOERR) {
		error = SCSWITCH_EUNEXPECTED;
		goto cleanup;
	}

	tmp = cl_nodes;
	while (tmp) {
		pnode = (scshutdown_node_t *)calloc(1,
		    sizeof (scshutdown_node_t));
		if (pnode == NULL) {
			error = SCSWITCH_ENOMEM;
			goto cleanup;
		}

		/* Get node id */
		scconf_error = scconf_get_nodeid(
		    (char *)(tmp->scstat_node_name), &nodeidp);
		error = scswitch_convert_scconf_error_code(scconf_error);
		if (error != SCSWITCH_NOERR) {
			goto cleanup;
		}

		pnode->scshutdown_nodename = strdup(tmp->scstat_node_name);
		if (pnode->scshutdown_nodename == NULL) {
			goto cleanup;
		}

		pnode->scshutdown_nodeid = nodeidp;
		if (tmp->scstat_node_status == SCSTAT_ONLINE) {
			pnode->online = 1;
		} else {
			pnode->online = 0;
		}

		/* append it to the list */
		if (pnode_current != NULL) {
			pnode_current->next = pnode;
		} else { /* first element */
			pcluster->scshutdown_nodelist = pnode;
		}
		pnode_current = pnode;
		pnode = NULL; /* so we don't accidentally free it */

		tmp = tmp->scstat_node_next;
	}

	*ppcluster = pcluster;

	pcluster = NULL; /* don't free it if no error */

cleanup:
	/* free space */
#if (SOL_VERSION >= __s10)
	if (clp != NULL) {
		clconf_obj_release((clconf_obj_t *)clp);
	}
#endif

	if (pcluster_cfg != NULL) {
		scconf_free_clusterconfig(pcluster_cfg);
	}
	if (pnode != NULL) {
		free(pnode);
	}

	return (error);
}

/*
 * scshutdown_free_nodeinfo
 *
 * Free scshutdown_node_t structure.
 */
static void
scshutdown_free_nodeinfo(scshutdown_node_t *node_info)
{
	scshutdown_node_t *tmp = node_info;
	scshutdown_node_t *tmp2;

	if (tmp == NULL) {
		return;
	}

	while (tmp->next) {
		tmp2 = tmp->next;
		free(tmp2->scshutdown_nodename);
		tmp->next = tmp2->next;
		free(tmp2);
	}

	free(tmp->scshutdown_nodename);
	free(tmp);
}

/*
 * scshutdown_free_clusterinfo
 *
 * Free scshutdown_cluster_t structure.
 */
static void
scshutdown_free_clusterinfo(scshutdown_cluster_t *cluster_info)
{
	scshutdown_node_t *pnode = NULL;
	scshutdown_node_t *pnode_next = NULL;

	if (cluster_info != NULL) {
		free(cluster_info->scshutdown_cluster_name);
		pnode = cluster_info->scshutdown_nodelist;
		while (pnode != NULL) {
			pnode_next = pnode->next;
			free(pnode);
			pnode = pnode_next;
		}
		free(cluster_info);
	}
}

/*
 * scshutdown_send_startmsg_to_cluster
 *
 * Send shutdown start message at regular interval to all the nodes
 * in the cluster.
 *
 * gettext on message string sent to all the nodes on the cluster is not being
 * done here, because other machines outside the cluster can nfs mount and
 * if locale is set on the cluster node then the same message will be sent
 * out to all the machines.
 */
static void
scshutdown_send_startmsg_to_cluster(scshutdown_cluster_t *cluster_info,
    char *strtime, char *user_msg_buf)
{
	nodeid_t nodeid;
	scshutdown_node_t *pnode_list = NULL;
	char msgbuf[BUFSIZ];
	int msg_error = 0;

	(void) sprintf(msgbuf, "The cluster %s will be shutdown in %s\n %s\n",
	    cluster_info->scshutdown_cluster_name, strtime, user_msg_buf);

	pnode_list = cluster_info->scshutdown_nodelist;
	while (pnode_list != NULL) {
		if (pnode_list->online == 0) {
			pnode_list = pnode_list->next;
			continue;
		}
		nodeid = pnode_list->scshutdown_nodeid;
		msg_error = scshutdown_send_wall_msg(msgbuf, nodeid);
		if (msg_error != 0) {
			(void) fprintf(stderr, gettext(
			    "Could not execute %s on cluster %s.\n"),
			    "/usr/sbin/wall",
			    cluster_info->scshutdown_cluster_name);
		}
		pnode_list = pnode_list->next;
	}

	pnode_list = cluster_info->scshutdown_nodelist;
	while (pnode_list != NULL) {
		nodeid = pnode_list->scshutdown_nodeid;
		msg_error = scshutdown_send_rwall_msg(msgbuf, nodeid);
		pnode_list = pnode_list->next;
	}
}

/*
 * scshutdown_send_abortmsg_to_cluster
 *
 * Send shutdown abort message to all the nodes in the cluster.
 *
 * gettext() on message string sent to all the nodes on the cluster is not being
 * done here, because other machines outside the cluster can nfs mount and
 * if locale is set on the cluster node then the same message will be sent
 * out to all the machines.
 */
static void
scshutdown_send_abortmsg_to_cluster(scshutdown_cluster_t *cluster_info)
{
	nodeid_t nodeid;
	scshutdown_node_t *pnode_list = NULL;
	char msgbuf[BUFSIZ];
	int msg_error = 0;

	(void) sprintf(msgbuf, "Shutdown aborted\n");

	pnode_list = cluster_info->scshutdown_nodelist;
	while (pnode_list != NULL) {
		if (pnode_list->online == 0) {
			pnode_list = pnode_list->next;
			continue;
		}
		nodeid = pnode_list->scshutdown_nodeid;
		msg_error = scshutdown_send_wall_msg(msgbuf, nodeid);
		if (msg_error != 0) {
			(void) fprintf(stderr, gettext("Could not execute "
			    "/usr/sbin/wall on cluster %s. \n"),
			    cluster_info->scshutdown_cluster_name);
		}
		pnode_list = pnode_list->next;
	}

	(void) sprintf(msgbuf, "Shutdown aborted\n");

	pnode_list = cluster_info->scshutdown_nodelist;
	while (pnode_list != NULL) {
		nodeid = pnode_list->scshutdown_nodeid;
		msg_error = scshutdown_send_rwall_msg(msgbuf, nodeid);
		pnode_list = pnode_list->next;
	}
}

/*
 * scshutdown_send_wall_msg
 *
 * Build message to be sent to all the m/c's which are nfs mounted on the
 * cluster node and send the messages.
 */
static int
scshutdown_send_wall_msg(char *msgbuf, nodeid_t nodeid)
{
	char cmdbuf[BUFSIZ];
	int msg_error = 0;

	(void) sprintf(cmdbuf, "/usr/sbin/wall -a <<-!\n %s", msgbuf);

	/* Send message to the node */
	msg_error = clconf_do_execution(cmdbuf, nodeid, NULL, B_FALSE,
	    B_TRUE, B_TRUE);
	return (msg_error);
}

/*
 * scshutdown_send_rwall_msg
 *
 * Build message to be sent to all the m/c's which are nfs mounted on the
 * cluster node and send the messages.
 */
static int
scshutdown_send_rwall_msg(char *msgbuf, nodeid_t nodeid)
{
	char cmdbuf[BUFSIZ];
	int msg_error = 0;

	(void) sprintf(cmdbuf,
	    "if [ -x /usr/sbin/showmount -a -x /usr/sbin/rwall ]\n"
	    "then\n"
	    "	remotes=`/usr/sbin/showmount`\n"
	    "	if [ -n \"$remotes\" ]\n"
	    "	then\n"
	    "		/usr/sbin/rwall ${remotes} <<-!\n%s\n!\n"
	    "	fi\n"
	    "fi\n",
	    msgbuf);

	/* Send message to the node */
	msg_error = clconf_do_execution(cmdbuf, nodeid, NULL, B_FALSE,
	    B_TRUE, B_TRUE);

	return (msg_error);
}

/*
 * If running in global zone, set the cluster shutdown flag
 * on the CMM of every node.
 * If running in a zone cluster, tell the local node's ZCMM
 * to start the zone cluster's membership shutdown.
 */
int
scshutdown_set_cluster_shutdown(void)
{
	nodeid_t i;
	int error;

#if (SOL_VERSION >= __s10)
	if (getzoneid() != GLOBAL_ZONEID) {
		/*
		 * If we are running in a native non-global zone,
		 * then we would have exited much earlier.
		 * So we can assume we want to shut down
		 * the zone cluster we are in.
		 * We tell the local node's ZCMM to start
		 * the zone cluster's membership shutdown.
		 * If clconf_cluster_shutdown() is called in
		 * a zone cluster, it talks to the local node's ZCMM
		 * and ignores any nodeid argument passed to it.
		 * So we pass 0 as the nodeid argument.
		 */
		error = clconf_cluster_shutdown(0);
		if (error != 0) {
			switch (error) {
			case EEXIST:
				ASSERT(0);
				/*lint -e527 */
				(void) fprintf(stderr,\
				    "%s: A cluster shutdown is already"\
				    "in progress.\n", progname);
				break;
				/*lint +e527 */
			case EIO:
				(void) fprintf(stderr,\
				    "%s: Exception raised while communicating"\
				    " with the membership subsystem.\n",
				    progname);
				break;
			case ENOENT:
				(void) fprintf(stderr,\
				    "%s: The current zone is not part of "\
				    "any zone cluster. Cannot do "\
				    "scshutdown in such a zone", progname);
				break;
			case EACCES:
				ASSERT(0);
				break;
			default:
				break;
			}

			return (1);
		}

		return (0);
	}
#endif

	/* Signal cluster shutdown on all nodes */
	for (i = 1; i <= NODEID_MAX; i++) {
		error = clconf_cluster_shutdown(i);
		if ((error != 0) && (error != ENOENT)) {
			switch (error) {
			case EEXIST:
				(void) fprintf(stderr,\
				    "%s: A cluster shutdown is already"\
				    "in progress.\n", progname);
				break;
			case EIO:
				(void) fprintf(stderr,\
				    "%s: Exception raised while communicating"\
				    " with the node %d.\n", progname, i);
				break;
			case EACCES:
				(void) fprintf(stderr,\
				    "%s: Cannot shutdown the cluster. Rolling"\
				    " Upgrade in progress.\n", progname);
				break;
			default:
				break;
			}

			return (1);
		}
	}

	return (0);
}
