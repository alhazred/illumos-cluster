/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */
#pragma ident	"@(#)damadm.c	1.3	08/05/20 SMI"

#include <syslog.h>
#include <stdio.h>
#include <stdlib.h>
#include <netdb.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <netinet/in.h>
#include <sys/time.h>
#include <signal.h>
#include <errno.h>
#include <strings.h>
#include <unistd.h>
#include <stdarg.h>
#include <locale.h>
#include <dam.h>
/*
 * damadm -m <path>
 * damadm -u <path>
 *
 * DAM_OK
 * DAM_EXIT_USAGE
 * DAM_EXIT_FAIL
 * DAM_EXIT_NOTMONITORED
 */

void dam_log(int const pri, char const * const fmt, ...);

static char *progname;

#define	DAM_EXIT_OK		0
#define	DAM_EXIT_USAGE		1
#define	DAM_EXIT_FAIL		2
#define	DAM_EXIT_NOTMONITORED	3
#define	DAM_EXIT_NODAEMON	4

#define	DEFAULT_LOG_TAG	"damadm"
static char *logTag = DEFAULT_LOG_TAG;
static int logLevel = LOG_NOTICE;

#define	MAX_MSG_SIZE 1024
static char message[MAX_MSG_SIZE];


/*
 * dam_sendmsg(): Client side function to communicate with the dam daemon
 * Send a message to the dam daemon, which replies with a integer return
 * code. This function returns that code to client. -1 for any errors.
 */
int
dam_sendmsg(char *msg, int size, char *response, int rsize)
{
	int rc = DAM_ENODAEMON, sd;
	ssize_t n = 0;
	char *sockname, buf[32];
	struct sockaddr_un target;
	int result;

	dam_log(LOG_DEBUG, "dam_sendmsg: size %d", size);
	sockname = DAM_SOCKET_NAME;

	sd = socket(AF_UNIX, SOCK_STREAM, 0);
	if (sd < 0) {
		dam_log(LOG_ERR, "Failed to create socket: %s.",
		    strerror(errno));
		return (rc);
	}
	target.sun_family = AF_UNIX;
	(void) strcpy(target.sun_path, sockname);

	if (connect(sd, (struct sockaddr *)&target, (int)sizeof (target)) < 0) {
		dam_log(LOG_ERR,
		    "Failed to connect to socket: %s.", strerror(errno));
		goto finished;
	}
	if (send(sd, msg, (size_t)size, 0) != size) { /*lint !e571 */
		goto finished;
	}
	/*
	 * Wait for a return value from the DAM daemon. If the DAM
	 * is stopped due to an errorr, the recv() will return without
	 * reading anything.
	 */
	if ((n = recv(sd, buf, sizeof (buf), 0)) < 1) {
		rc = DAM_ESYSERR;
		dam_log(LOG_NOTICE, "dam_sendmsg: recv failed:"
		    " %s.", strerror(errno));
		goto finished;
	}

	buf[n] = 0;
	if (n < sizeof (dam_reply_msg)) {
		dam_log(LOG_NOTICE, "dam_sendmsg: recv response too small: %d",
		    n);
	}
	rc = ((dam_reply_msg *)(buf))->result;
	dam_log(LOG_DEBUG, "dam_sendmsg: size recvd %d,response result: %d",
	    n, rc);

	if (n > sizeof (dam_reply_msg)) {
		char *p = &buf[sizeof (dam_reply_msg)];
		int msg_size = n - sizeof (int);
		dam_log(LOG_DEBUG, "dam_sendmsg: response data size: %d",
		    msg_size);
		if (rsize <= msg_size) {
			rc =  (DAM_ETRUNC);
			goto finished;
		}
		strcpy(response, p);
	}
finished:
	(void) close(sd);
	return (rc);
}

void
dam_log_set_level(int level)
{
	logLevel = level;
}

void
dam_log_set_tag(char *tag)
{
	if (!tag)
		return;
	/* only first call initialize tag */
	if (strcmp(logTag, DEFAULT_LOG_TAG) == 0)
		logTag = strdup(tag);
}

void
dam_log(int const pri, char const * const fmt, ...)
{
	va_list ap;
	if (pri > logLevel)
		return;

	va_start(ap, fmt);
	openlog(logTag, LOG_CONS | LOG_PID, LOG_USER);
	vsyslog(pri, fmt, ap);
	closelog();
	va_end(ap);
}


int
dam_monitor(const char *path)
{
	int rc, size;
	dam_cmd_msg *msg;

	size = sizeof (*msg) + strlen(path) + 1;
	msg = (dam_cmd_msg *)malloc(size);
	if (!msg) {
		return (-1);
	}
	msg->size = size;
	msg->command = DAM_MONITOR;
	msg->path = (char *)(msg + 1);
	strcpy(msg->path, path);
	rc = dam_sendmsg((char *)msg, size, NULL, 0);
	return (rc);
}

int
dam_unmonitor(const char *path)
{
	int rc, size;
	dam_cmd_msg *msg;

	size = sizeof (*msg) + strlen(path) + 1;
	msg = (dam_cmd_msg *)malloc(size);
	if (!msg) {
		return (-1);
	}
	msg->size = size;
	msg->command = DAM_UNMONITOR;
	msg->path = (char *)(msg + 1);
	strcpy(msg->path, path);
	rc = dam_sendmsg((char *)msg, size, NULL, 0);
	return (rc);
}

static void
usage()
{
	(void) fprintf(stderr, "%s: %s -m | -u <path>\n",
	    gettext("usage"), progname);
}

int
main(int argc, char *argv[])
{
	int d_flg, m_flg, u_flg, c;
	char *path;
	int rstatus = DAM_EXIT_USAGE;
	int rc;
	if ((progname = strrchr(argv[0], '/')) == NULL) {
		progname = argv[0];
	} else {
		++progname;
	}
	dam_log_set_tag("Cluster.dam.cmd");
	d_flg = 0; m_flg = 0; u_flg = 0;

	while ((c = getopt(argc, argv, "m:u:d")) != EOF) {
		switch (c) {
		case 'd':
			d_flg = 1;
			dam_log_set_level(LOG_DEBUG);
			break;
		case 'm':
			m_flg = 1;
			path = optarg;
			break;
		case 'u':
			u_flg = 1;
			path = optarg;
			break;
		default:
			exit(DAM_EXIT_USAGE);
		}
	}

	c = u_flg +  m_flg;
	if (c != 1) {
		usage(argc, argv);
		exit(DAM_EXIT_USAGE);
	}
	if (u_flg) {
		rc = dam_unmonitor(path);
	} else if (m_flg) {
		rc = dam_monitor(path);
	}

	switch (rc) {
	case DAM_OK:
		rstatus = DAM_EXIT_OK;
		break;
	case DAM_ENODAEMON:
		dam_log(LOG_ERR, "Could not contact DAM daemon");
		fprintf(stderr, "Could not contact DAM daemon\n");
		rstatus = DAM_EXIT_NODAEMON;
		break;
	default:
		dam_log(LOG_ERR, "Error returned from damd daemon: %d", rc);
		fprintf(stderr, "Error returned from damd daemon: %d\n", rc);
		rstatus = DAM_EXIT_FAIL;
		break;
	}
	exit(rstatus);
}
