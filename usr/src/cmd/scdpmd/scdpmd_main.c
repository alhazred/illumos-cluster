/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scdpmd_main.c	1.12	08/05/20 SMI"

/*
 * scdpmd_main.c
 *
 *      main for scdpmd(1M)
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <strings.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include <libintl.h>
#include <locale.h>
#include <limits.h>
#include <fcntl.h>
#include <stdarg.h>
#include <devid.h>
#include <sys/didio.h>
#include <libdid.h>
#include <signal.h>
#include <pthread.h>
#include <netdb.h>
#include <sys/uadmin.h>
#include <sys/sc_syslog_msg.h>
#include <sys/clconf_int.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/dkio.h>
#include <sys/scsi/scsi.h>
#include <sys/cladm_int.h>
#include <sys/resource.h>
#include <sys/cl_events.h>
#include <sys/clconf.h>
#include <sys/cl_auth.h>
#include <rgm/sczones.h>

#include <libscdpm.h>

/*lint -e793 */

#define	DPM_ERR_BUFSIZ		1024	/* Used to format error messages */
#define	DPM_MIN_THREAD		4	/* Minium number of threads */
#define	DPM_MAX_THREAD		16	/* Max number of threads */

#define	DPM_SC_SYSLOG_TAG		"Cluster.scdpmd"
#define	DPMD_CONFIG_FILENAME		DPMD_DIR"/scdpmd.conf"

/*
 * This structure is used to store variables who are accessed from
 * every threads
 */
typedef struct DPM_GLOBAL_VARS {
	pthread_mutex_t _lock;
	int thflag;
	int interval;
	int timeout;
	int retry;
} DpmGlobalVars;

/* This node's node number */
nodeid_t my_nodeid;

/*
 * Tag to address the global variables
 */
#define	DPMD_INTERVAL	1
#define	DPMD_TIMEOUT	2
#define	DPMD_RETRY	3
#define	DPMD_FLAG	4

/*
 * messages the main can send to the threads
 */
#define	DPM_FLAG_INIT		0
#define	DPM_FLAG_SHUTDOWN	1
#define	DPM_FLAG_RECONFIG	2

/*
 * Disk ping defaults values.
 */
#define	MIN_PINGTIMEOUT		1
#define	MAX_PINGTIMEOUT		15
#define	MIN_PINGINTERVAL	60
#define	MAX_PINGINTERVAL	3600
#define	MIN_PINGRETRY		2
#define	MAX_PINGRETRY		10

#define	ONE_MINUTE		60
#define	MIN_SCDPM_THREAD_SLEEP	5
#define	MAX_SCDPM_THREAD_SLEEP	30

/*
 * tag names for config file variables.
 */
#define	PING_RETRY		"ping_retry"
#define	PING_TIMEOUT		"ping_timeout"
#define	PING_INTERVAL		"ping_interval"

/* The following values define disk path access status for auto reboot */
#define	MONITOR_DISKS_UNKNOWN			0	/* Unknown status */
#define	MONITOR_DISKS_HAS_ACCESS		1
#define	MONITOR_DISKS_NO_ACCESS			2
/* No access to any disk paths but no other nodes can take over either */
#define	MONITOR_DISKS_NO_ACCESS_NO_FAILOVER	3

#define	SCDPMD_DID_PATH		"/dev/did/rdsk/d"
#define	SCDPMD_DID_PATH_LEN	(sizeof ("/dev/did/rdsk/d") - 1)
#define	SCDPMD_CTD_PATH		"/dev/rdsk/c"
#define	SCDPMD_CTD_PATH_LEN	(sizeof ("/dev/rdsk/c") - 1)

#undef	MIN
#define	MIN(a, b) ((a) < (b) ? (a) : (b))
#undef	MAX
#define	MAX(a, b) ((a) > (b) ? (a) : (b))

/*
 * Global variables definitions.
 */
static char *progname;
static int debug_mode;
static DpmGlobalVars dpmvars;
static int wait_all_threads_created = 0;
static sc_syslog_msg_handle_t handle;	/* syslog message handle */
static int last_monitoring_status = MONITOR_DISKS_UNKNOWN;
static pthread_mutex_t dpmd_mutex;	/* global dpmd mutex lock */
static char hostname[MAXHOSTNAMELEN+1];

/*
 * Functions definitions.
 */
static char *delspaces(char *str);
static void *_dpm_monitor(void *);
static void daemon_init(void);
static int dpm_monitord(dpm_core_lst_t *);
static void err_quit(const char *, ...);
static void get_dpm_global(int, int *);
static void log_disk_status(const dpm_core_lst_t *);
static void log_disk_monitor(const dpm_core_lst_t *);
static void read_config(void);
static void set_dpm_global(int, int);
static void usage(void);
static void print_debug_status(dpm_core_lst_t *);
static dpm_core_lst_t *convert_to_dpm_core_lst(disk_status_node_t *);
static int is_disk_shared(const disk_status_node_t *, did_device_list_t *);
static int get_path_count_of_did_disk(const char *, did_device_list_t *);
static int get_path_count_of_ctd_disk(char *, did_device_list_t *);

/*
 * main
 */
int
main(int argc, char **argv)
{
	int c;
	dpm_core_lst_t *devlst;
	disk_status_node_t *ccr_disk_list;


	/* Set the program name */
	if ((progname = strrchr(argv[0], '/')) == NULL) {
		progname = argv[0];
	} else {
		++progname;
	}

	/* I18N housekeeping */
	(void) setlocale(LC_ALL, "");
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);
	(void) textdomain(TEXT_DOMAIN);

	if (sc_zonescheck() != 0)
		return (1);

	/* Initialize and promote to euid=0 priviledge */
	cl_auth_init();
	cl_auth_promote();

	/* Set defaults values and initializations. */
	set_dpm_global(DPMD_TIMEOUT, DPM_DEFAULT_TIMEOUT);
	set_dpm_global(DPMD_INTERVAL, DPM_DEFAULT_INTERVAL);
	set_dpm_global(DPMD_RETRY, DPM_DEFAULT_RETRY);
	set_dpm_global(DPMD_FLAG, DPM_FLAG_INIT);
	(void) pthread_mutex_init(&dpmvars._lock, NULL);

	/* no longer privileged */
	cl_auth_demote();

	while ((c = getopt(argc, argv, "?hd")) != -1) {
		switch (c) {
		case 'd':
			debug_mode = TRUE;
			break;
		case '?':
		case 'h':
		default:
			usage();
		}
	}

	/* Check authorization */
	cl_auth_check_command_opt_exit(*argv, NULL, CL_AUTH_SYSTEM_ADMIN, 1);

	/* Promote to euid=0 */
	cl_auth_promote();

	/*
	 * daemonize...
	 */
	if (!debug_mode)
		daemon_init();

	(void) sc_syslog_msg_initialize(&handle, DPM_SC_SYSLOG_TAG, "");
	(void) pthread_mutex_init(&dpmd_mutex, NULL);

	read_config();

	/*
	 * Initialize the ORB
	 */
	if (dpm_initialize() != 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * An attempt to start the scdpmd failed.
		 * @user_action
		 * Contact your authorized Sun service provider to determine
		 * whether a workaround or patch is available.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "ORB initialization failure");
		exit(1);
	}

	/*
	 * Initialize the dpmccr table
	 */
	if (dpmccr_initialize(&ccr_disk_list) != 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * An attempt to start the scdpmd failed.
		 * @user_action
		 * Contact your authorized Sun service provider to determine
		 * whether a workaround or patch is available.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "ccr_initialize failure");
		exit(1);
	}

	(void) sc_syslog_msg_set_syslog_tag(DPM_SC_SYSLOG_TAG);

	devlst = convert_to_dpm_core_lst(ccr_disk_list);
	if (devlst == NULL) {
		/*
		 * SCMSGS
		 * @explanation
		 * An attempt to start the scdpmd failed.
		 * @user_action
		 * The node should have a least a disk. Check the local disk
		 * list with the scdidadm -l command.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "There are no disks to monitor");
		exit(1);
	}
	dpm_free_status_list(ccr_disk_list);

	/*
	 * Fill the hostname value.
	 */
	(void) gethostname(hostname, sizeof (hostname));

	/*
	 * SCMSGS
	 * @explanation
	 * The scdpmd is started.
	 * @user_action
	 * No action required.
	 */
	(void) sc_syslog_msg_log(handle, LOG_INFO, MESSAGE, "daemon started.");

	return (dpm_monitord(devlst));
}

/*
 * usage
 *
 *	Print usage message.
 */
static void
usage()
{
	/* Print the usage */
	(void) fprintf(stderr,
	    "usage: %s [-dh?]\n", progname);
	exit(1);
}

/*
 * print_debug_status
 *
 *	Print debuging information.
 *
 *	Arg:
 *		dpm_core_lst_t *
 *
 */
void
print_debug_status(dpm_core_lst_t *devptr)
{
	char tstr[40];
	char *format = "Thread:0x%x Path:%s Status:0x%x - %s";
	time_t now = time(NULL);

	(void) fprintf(stderr, format, pthread_self(), devptr->path,
	    devptr->mon_status, ctime_r(&now, tstr));
}


/*
 * read_config(void)
 *
 * Read the daemon configuration from the file pointed by
 * DPMD_CONFIG_FILENAME.
 *
 * This file shuld contain a variable and a value by line separated
 * by an '=' character.  The possible variables are
 * ping_interval
 * ping_timeout
 * ping_retry
 *
 */

static void
read_config()
{
	FILE *fd;
	char buffer[BUFSIZ];
	char *tag, *strval, *cbrk;
	int line;
	int val;
	extern int errno;

	if (access(DPMD_CONFIG_FILENAME, R_OK) < 0) {
		if (errno == ENOENT) /*lint !e746 */
			return;

		/*
		 * SCMSGS
		 * @explanation
		 * No scdpmd config file (/etc/cluster/scdpm/scdpmd.conf) has
		 * been found. The scdpmd deamon uses default values.
		 * @user_action
		 * No action required.
		 */
		(void) sc_syslog_msg_log(handle, LOG_NOTICE, MESSAGE,
		    "%s access error: %s Continuing "
		    "with the scdpmd defaults values",
		    DPMD_CONFIG_FILENAME, strerror(errno));
		return;
	}

	fd = fopen(DPMD_CONFIG_FILENAME, "r");
	if (fd == NULL) {
		/*
		 * SCMSGS
		 * @explanation
		 * Open of scdpmd config file (/etc/cluster/scdpm/scdpmd.conf)
		 * has failed. The scdpmd deamon uses default values.
		 * @user_action
		 * Check the config file.
		 */
		(void) sc_syslog_msg_log(handle, LOG_NOTICE, MESSAGE,
		    "%s open error: %s Continuing "
		    "with the scdpmd defaults values",
		    DPMD_CONFIG_FILENAME, strerror(errno));
		return;
	}

	line = 0;
	while (fgets(buffer, BUFSIZ, fd) != NULL) {
		line++;
		if (buffer[0] == '#')
			continue;
		(void) delspaces(buffer);
		if (strlen(buffer) == 0)
			continue;

		tag = strtok_r(buffer, "=", &cbrk);
		strval = strtok_r(NULL, "=", &cbrk);

		if (tag == NULL || strval == NULL) {
			/*
			 * SCMSGS
			 * @explanation
			 * An error occurs in the scdpmd config file
			 * (/etc/cluster/scdpm/scdpmd.conf) has failed.
			 * @user_action
			 * Fix the config file.
			 */
			(void) sc_syslog_msg_log(handle, LOG_NOTICE, MESSAGE,
			    "Config file: %s error line %d",
			    DPMD_CONFIG_FILENAME, line);
			continue;
		}

		val = atoi(strval);
		if (strcasecmp(tag, PING_TIMEOUT) == 0) {
			if (val < MIN_PINGTIMEOUT || val > MAX_PINGTIMEOUT) {
				/*
				 * SCMSGS
				 * @explanation
				 * scdpmd config file
				 * (/etc/cluster/scdpm/scdpmd.conf) has a bad
				 * ping_timeout value. The default
				 * ping_timeout value is used.
				 * @user_action
				 * Fix the value of ping_timeout in the config
				 * file.
				 */
				(void) sc_syslog_msg_log(handle, LOG_INFO,
				    MESSAGE,
				    "ping_timeout out of bound. "
				    "The timeout must be between %d and %d: "
				    "using the default value",
				    MIN_PINGTIMEOUT, MAX_PINGTIMEOUT);
			} else {
				/*
				 * SCMSGS
				 * @explanation
				 * The ping_timeout value used by scdpmd.
				 * @user_action
				 * No action required.
				 */
				(void) sc_syslog_msg_log(handle, LOG_INFO,
				    MESSAGE,
				    "ping_timeout %d", val);
				set_dpm_global(DPMD_TIMEOUT, val);
			}
		} else if (strcasecmp(tag, PING_INTERVAL) == 0) {
			if (val < MIN_PINGINTERVAL || val > MAX_PINGINTERVAL) {
				/*
				 * SCMSGS
				 * @explanation
				 * scdpmd config file
				 * (/etc/cluster/scdpm/scdpmd.conf) has a bad
				 * ping_interval value. The default
				 * ping_interval value is used.
				 * @user_action
				 * Fix the value of ping_interval in the
				 * config file.
				 */
				(void) sc_syslog_msg_log(handle, LOG_INFO,
				    MESSAGE,
				    "ping_interval out of bound. "
				    "The interval must be between %d "
				    "and %d seconds: using the default "
				    "value",
				    MIN_PINGINTERVAL, MAX_PINGINTERVAL);
			} else {
				/*
				 * SCMSGS
				 * @explanation
				 * The ping_interval value used by scdpmd.
				 * @user_action
				 * No action required.
				 */
				(void) sc_syslog_msg_log(handle, LOG_INFO,
				    MESSAGE,
				    "ping_interval %d", val);
				set_dpm_global(DPMD_INTERVAL, val);
			}
		} else if (strcasecmp(tag, PING_RETRY) == 0) {
			if (val < MIN_PINGRETRY || val > MAX_PINGRETRY) {
				/*
				 * SCMSGS
				 * @explanation
				 * scdpmd config file
				 * (/etc/cluster/scdpm/scdpmd.conf) has a bad
				 * ping_retry value. The default ping_retry
				 * value is used.
				 * @user_action
				 * Fix the value of ping_retry in the config
				 * file.
				 */
				(void) sc_syslog_msg_log(handle, LOG_INFO,
				    MESSAGE,
				    "ping_retry out of "
				    "bound. The number of retry must be "
				    "between %d and %d: using the default "
				    "value",
				    MIN_PINGRETRY, MAX_PINGRETRY);
			} else {
				/*
				 * SCMSGS
				 * @explanation
				 * The ping_retry value used by scdpmd.
				 * @user_action
				 * No action required.
				 */
				(void) sc_syslog_msg_log(handle, LOG_INFO,
				    MESSAGE,
				    "ping_retry %d", val);
				set_dpm_global(DPMD_RETRY, val);
			}
		} else {
			/*
			 * SCMSGS
			 * @explanation
			 * Error in scdpmd config file
			 * (/etc/cluster/scdpm/scdpmd.conf).
			 * @user_action
			 * Fix the config file.
			 */
			(void) sc_syslog_msg_log(handle, LOG_NOTICE, MESSAGE,
			    "Config file: %s unknown variable "
			    "name line %d", DPMD_CONFIG_FILENAME, line);
		}
	}
	(void) fclose(fd);
}

/*
 * delspaces(char *str)
 *
 * Simply delete all the spaces into a string.
 *
 */
static char *
delspaces(char *str)
{
	char *src, *dst;

	src = dst = str;
	while (*src) {
		if (isspace(*src))
			src++;
		else
			*dst++ = *src++;
	}
	*dst = (char)0;
	return (str);
}

/*
 * err_quit(const char *msg, ...)
 *
 * print an error message and quit.
 *
 */
static void
err_quit(const char *msg, ...)
{
	char buf[DPM_ERR_BUFSIZ];
	va_list ap;

	va_start(ap, msg);	/*lint !e40 */
	(void) vsnprintf(buf, sizeof (buf), gettext(msg), ap);
	va_end(ap);

	(void) fprintf(stderr, "%s\n", buf);
	exit(1);
}

static dpm_core_lst_t *
convert_to_dpm_core_lst(disk_status_node_t *ccr_disk_list)
{
	disk_status_node_t *tmp_ccr_list;
	dpm_core_lst_t *devlst = NULL;
	char *curr;
	int nodeid, this_nodeid;
	char nodename[MAXHOSTNAMELEN];

	(void) gethostname(nodename, sizeof (nodename));
	if (strlen(nodename) == 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * Internal error.
		 * @user_action
		 * Contact your authorized Sun service provider to determine
		 * whether a workaround or patch is available.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Cannot determine the node name");
		return (NULL);
	}

	this_nodeid = get_nodeid_by_nodename(nodename);
	if (this_nodeid == -1) {
		/*
		 * SCMSGS
		 * @explanation
		 * Internal error.
		 * @user_action
		 * Contact your authorized Sun service provider to determine
		 * whether a workaround or patch is available.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Cannot determine the nodeid");
		return (NULL);
	}

	tmp_ccr_list = ccr_disk_list;
	while (tmp_ccr_list != NULL) {
		/* Strip out nodeid!!! */
		curr = strtok(tmp_ccr_list->name, ":");
		if (curr == NULL)
			return (NULL);
		nodeid = atoi(curr);
		if (nodeid == this_nodeid) {
		    curr = strtok(NULL, "\0");
		    if (curr == NULL)
			    return (NULL);
		    devlst = dpm_device_append_core(devlst, curr,
			(DPM_PATH_IS_MONITORED(tmp_ccr_list->status) ?
			    DPM_PATH_MONITORED : DPM_PATH_UNMONITORED)
			| DPM_PATH_UNKNOWN);
		}
		tmp_ccr_list = tmp_ccr_list->next;
	}
	return (devlst);
}

static void
set_dpm_global(int var, int value)
{
	(void) pthread_mutex_lock(&dpmvars._lock);
	switch (var) {
	case DPMD_INTERVAL:
		dpmvars.interval = value;
		break;
	case DPMD_TIMEOUT:
		dpmvars.timeout = value;
		break;
	case DPMD_RETRY:
		dpmvars.retry = value;
		break;
	case DPMD_FLAG:
		dpmvars.thflag = value;
		break;
	default:
		/*
		 * SCMSGS
		 * @explanation
		 * Internal error.
		 * @user_action
		 * Contact your authorized Sun service provider to determine
		 * whether a workaround or patch is available.
		 */
		(void) sc_syslog_msg_log(handle, LOG_INFO, MESSAGE,
		    "call set_dpm_global with an invalid argument");
		break;
	}
	(void) pthread_mutex_unlock(&dpmvars._lock);
}

static void
get_dpm_global(int var, int *val)
{
	(void) pthread_mutex_lock(&dpmvars._lock);
	switch (var) {
	case DPMD_INTERVAL:
		*val = dpmvars.interval;
		break;
	case DPMD_TIMEOUT:
		*val = dpmvars.timeout;
		break;
	case DPMD_RETRY:
		*val = dpmvars.retry;
		break;
	case DPMD_FLAG:
		*val = dpmvars.thflag;
		break;
	default:
		*val = -1;
		/*
		 * SCMSGS
		 * @explanation
		 * Internal error.
		 * @user_action
		 * Contact your authorized Sun service provider to determine
		 * whether a workaround or patch is available.
		 */
		(void) sc_syslog_msg_log(handle, LOG_INFO, MESSAGE,
		    "call get_dpm_global with an invalid argument");
		break;
	}
	(void) pthread_mutex_unlock(&dpmvars._lock);
}

void
daemon_init()
{
	rlim_t fd;
	struct rlimit rl;
	pid_t pid;
	extern int errno;

	pid = fork();
	if (pid < 0)
		err_quit("fork: %s", strerror(errno)); /*lint !e746 */
	if (pid > 0)
		exit(0); /* the parent exit after fork */

	/* child continues */
	/* set process group */
	pid = setsid();
	if (pid == (pid_t)-1)
		err_quit("setsid: %s", strerror(errno));


	/*
	 * Systeme V assign a control terminal to a process only is
	 * the process is a process group leader. With the call to
	 * setsid(2) we have made the daemon a process group
	 * leader. If the daemon stop being a process group leader, it
	 * can never reaquire a control terminal. To do this we have
	 * the daemon fork again and have the parent terminate.
	 */
	(void) signal(SIGCHLD, SIG_IGN);
	pid = fork();
	if (pid < 0)
		err_quit("fork: %s", strerror(errno));
	if (pid > 0)
		exit(0); /* the parent exit after fork */

	/*
	 * Now we are sure the control terminal is lost. The process
	 * is a real daemon!
	 */
	if (chdir("/") != 0)	/* change working directory */
		err_quit("chdir: %s", strerror(errno));

	(void) umask(0);	/* clear our file mode creation mask */

	/*
	 * close all the files descriptors
	 */
	if (getrlimit(RLIMIT_NOFILE, &rl) == 0) {
		for (fd = 0; fd < rl.rlim_cur; fd++)
			(void) close((int)fd);
	}
}

static void
log_disk_status(const dpm_core_lst_t *ptr)
{
	int st, error;
	char nodename[MAXHOSTNAMELEN];

	error = 0;
	(void) gethostname(nodename, sizeof (nodename));
	if (strlen(nodename) == 0) {
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Cannot determine the node name");
		(void) strcpy(nodename, "Unknown");
	}
	st = DPM_GET_STATUS(ptr->mon_status);
	switch (st) {
	case DPM_PATH_OK:
		/*
		 * SCMSGS
		 * @explanation
		 * A device is seen as OK.
		 * @user_action
		 * No action required.
		 */
		(void) sc_syslog_msg_log(handle, LOG_NOTICE, MESSAGE,
		    "The state of the path to device: %s has changed to OK",
		    ptr->path);
		error = sc_publish_event(
		    ESC_CLUSTER_DPM_DISK_PATH_OK,
		    CL_EVENT_PUB_DPM, CL_EVENT_SEV_INFO,
		    CL_EVENT_INIT_SYSTEM, 0, 0, 0,
		    CL_DISK_PATH, SE_DATA_TYPE_STRING, ptr->path,
		    CL_NODE_NAME, SE_DATA_TYPE_STRING, nodename,
		    NULL);
		break;
	case DPM_PATH_FAIL:
		/*
		 * SCMSGS
		 * @explanation
		 * A device is seen as FAILED.
		 * @user_action
		 * Check the device.
		 */
		(void) sc_syslog_msg_log(handle, LOG_NOTICE, MESSAGE,
		    "The state of the path to device: "
		    "%s has changed to FAILED", ptr->path);
		error = sc_publish_event(
		    ESC_CLUSTER_DPM_DISK_PATH_FAILED,
		    CL_EVENT_PUB_DPM, CL_EVENT_SEV_CRITICAL,
		    CL_EVENT_INIT_SYSTEM, 0, 0, 0,
		    CL_DISK_PATH, SE_DATA_TYPE_STRING, ptr->path,
		    CL_NODE_NAME, SE_DATA_TYPE_STRING, nodename,
		    NULL);
		break;
	default:
		break;
	}
	if (error != 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * Internal error.
		 * @user_action
		 * Contact your authorized Sun service provider to determine
		 * whether a workaround or patch is available.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Publish event error: %d", error);
	}
}

static void
log_disk_monitor(const dpm_core_lst_t *ptr)
{
	int mt, error;
	char nodename[MAXHOSTNAMELEN];

	error = 0;
	(void) gethostname(nodename, sizeof (nodename));
	if (strlen(nodename) == 0) {
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Cannot determine the node name");
		(void) strcpy(nodename, "Unknown");
	}
	mt = DPM_GET_MONITOR(ptr->mon_status);
	switch (mt) {
	case DPM_PATH_MONITORED:
		/*
		 * SCMSGS
		 * @explanation
		 * A device is monitored.
		 * @user_action
		 * No action required.
		 */
		(void) sc_syslog_msg_log(handle, LOG_NOTICE, MESSAGE,
		    "The status of device: %s "
		    "is set to MONITORED", ptr->path);
		error = sc_publish_event(
		    ESC_CLUSTER_DPM_DISK_PATH_MONITORED,
		    CL_EVENT_PUB_DPM, CL_EVENT_SEV_INFO,
		    CL_EVENT_INIT_SYSTEM, 0, 0, 0,
		    CL_DISK_PATH, SE_DATA_TYPE_STRING, ptr->path,
		    CL_NODE_NAME, SE_DATA_TYPE_STRING, nodename,
		    NULL);
		break;
	case DPM_PATH_UNMONITORED:
		/*
		 * SCMSGS
		 * @explanation
		 * A device is not monitored.
		 * @user_action
		 * No action required.
		 */
		(void) sc_syslog_msg_log(handle, LOG_NOTICE, MESSAGE,
		    "The status of device: %s "
		    "is set to UNMONITORED", ptr->path);
		error = sc_publish_event(
		    ESC_CLUSTER_DPM_DISK_PATH_UNMONITORED,
		    CL_EVENT_PUB_DPM, CL_EVENT_SEV_INFO,
		    CL_EVENT_INIT_SYSTEM, 0, 0, 0,
		    CL_DISK_PATH, SE_DATA_TYPE_STRING, ptr->path,
		    CL_NODE_NAME, SE_DATA_TYPE_STRING, nodename,
		    NULL);
		break;
	default:
		break;
	}
	if (error != 0) {
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Publish event error: %d", error);
	}
}

int
dpm_monitord(dpm_core_lst_t *devlst)
{
	pthread_t thlist[DPM_MAX_THREAD];
	sigset_t allsigs, sigs, sigsave;
	dpm_comm_t *dpmcom;
	dpm_core_lst_t *devptr;
	int i, sig;
	int nbthreads, nbdisks;
	int err = 0, retstatus = 1;

	/* block all signals from all threads initially */
	(void) sigfillset(&allsigs);
	(void) pthread_sigmask(SIG_BLOCK, &allsigs, NULL);

	/*
	 * compute the # of disks and the # of threads
	 */
	for (nbdisks = 0, devptr = devlst; devptr != NULL;
		devptr = devptr->next, nbdisks++)
		/* just to go to the end */
		;

	nbthreads = (int)(nbdisks / ONE_MINUTE);
	nbthreads = (nbthreads > DPM_MAX_THREAD) ? DPM_MAX_THREAD : nbthreads;
	nbthreads = (nbthreads < DPM_MIN_THREAD) ? DPM_MIN_THREAD : nbthreads;

	/*
	 * Initialize the structure used to communicate with the
	 * cluster infrastructure framework.
	 */
	dpmcom = dpmcom_init();
	if (dpmcom == NULL) {
		/*
		 * SCMSGS
		 * @explanation
		 * Internal error.
		 * @user_action
		 * Contact your authorized Sun service provider to determine
		 * whether a workaround or patch is available.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Communication module initialization error");
		goto cleanup;
	}
	dpmcom->devlst = devlst;

	/*
	 * this call create an object and register it into the Sun
	 * Cluster Name server infrastructure.
	 */
	if (register_dpm_daemon(dpmcom) != 0) {
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Communication module initialization error");
		goto cleanup;
	}

	/* Get our node ID */
	if (_cladm(CL_CONFIG, CL_NODEID, &my_nodeid) != 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Could not find node ID: %s", strerror(err));
		goto cleanup;
	}

	/* create the threads */
	(void) memset(thlist, 0, sizeof (thlist));
	for (i = 0; i < nbthreads; i++) {
		err = pthread_create(&thlist[i], NULL, _dpm_monitor, dpmcom);
		if (err != 0) {
			/*
			 * SCMSGS
			 * @explanation
			 * Internal error.
			 * @user_action
			 * Contact your authorized Sun service provider to
			 * determine whether a workaround or patch is
			 * available.
			 */
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "Thread creation error: %s", strerror(err));
			goto cleanup;
		}
	}

	/* now set up to catch signals we care about */
	(void) sigemptyset(&sigs);
	(void) sigaddset(&sigs, SIGHUP);	/* restart */
	(void) sigaddset(&sigs, SIGTERM);	/* exit */
	(void) sigaddset(&sigs, SIGINT);	/* exit if debugging */
	(void) sigaddset(&sigs, SIGQUIT);	/* exit if debugging */
	(void) sigaddset(&sigs, SIGPIPE);	/* catch & discard */
	(void) sigaddset(&sigs, SIGABRT);	/* catch */

	/*
	 * Now start monitoring in _dpm_monitor threads
	 */
	wait_all_threads_created = 1;

	/*
	 * We now turn the main thread into the signal handling module
	 */
	for (;;) {
		(void) sigwait(&sigs, &sig);
		switch (sig) {
		case SIGHUP:
			/*
			 * Read the new values and send a reconfig
			 * message to the threads
			 */
			(void) sigprocmask(SIG_BLOCK, &sigs, &sigsave);
			read_config();
			set_dpm_global(DPMD_FLAG, DPM_FLAG_RECONFIG);
			(void) sigprocmask(SIG_SETMASK, &sigsave, NULL);
			break;
		case SIGINT:
		case SIGQUIT:
			if (!debug_mode)
				break;
		case SIGTERM:			/*lint !e616 */
			(void) sigprocmask(SIG_BLOCK, &sigs, NULL);
			/*
			 * SCMSGS
			 * @explanation
			 * scdpmd has received a signal and is goind down.
			 * @user_action
			 * No action required.
			 */
			(void) sc_syslog_msg_log(handle, LOG_INFO, MESSAGE,
			    "going down on signal %d\n", sig);

			/* send a shutdown message to all the threads */
			set_dpm_global(DPMD_FLAG, DPM_FLAG_SHUTDOWN);

			/* wait for all the threads to terminate */
			for (i = 0; i < nbthreads; i++)
				(void) pthread_join(thlist[i], NULL);

			retstatus = 0;
			goto cleanup;
		case SIGABRT:
			abort();
			break;
		default:
			break;
		}
	}

cleanup:
	if (dpmcom != NULL && dpmcom->devlst != NULL)
		dpm_free(dpmcom, dpmcom->devlst);
	if (dpmcom != NULL)
		dpmcom_free(dpmcom);
	return (retstatus);
}

/*
 * Frees up the list of disks generated by another_node_has_disks() below.
 */
static void
free_disk_list(disk_status_node_t *disk_list)
{
	disk_status_node_t *free_disk;

	while (disk_list != NULL) {
		free_disk = disk_list;
		disk_list = disk_list->next;
		free((void *)free_disk);
	}
}

/*
 * Return whether any other node can talk to any of our monitored
 * disks.  Any failure to gather exact status will result in
 * a possible false negative, but that's ok.
 * If we are going to attempt to fail over to another node, we want to
 * know definitively that another node can talk to at least one of
 * our disks.
 */
static boolean_t
another_node_has_disks(disk_status_node_t *disk_list)
{
	disk_status_node_t *next_disk, *new_disk, *query_list = NULL;
	uint_t disk_count = 0;
	int ret;
	uint32_t node;
	quorum_status_t *quor_table = NULL;

	/* count the monitored disks and add them to query_list */
	for (next_disk = disk_list; next_disk != NULL;
	    next_disk = next_disk->next) {
		if (DPM_PATH_IS_MONITORED(next_disk->status)) {
			++disk_count;
			new_disk = malloc(sizeof (disk_status_node_t));
			if (new_disk == NULL) {
				free_disk_list(query_list);
				return (B_FALSE);
			}
			new_disk->name = next_disk->name;
			new_disk->next = query_list;
			query_list = new_disk;
		}
	}

	/* Get all the nodes in the cluster */
	if (cluster_get_quorum_status(&quor_table) != 0) {
		cluster_release_quorum_status(quor_table);
		return (B_FALSE);
	}

	/* Walk through the node list */
	for (node = 0; node < quor_table->num_nodes; ++node) {
		if (quor_table->nodelist[node].nid == my_nodeid ||
		    quor_table->nodelist[node].state != QUORUM_STATE_ONLINE) {
			/* Skip myself and offline nodes */
			continue;
		}
		/*
		 * Find out whether this remote node can talk to our disks.
		 */
		ret = libdpm_get_monitoring_status(
		    quor_table->nodelist[node].nid, query_list, disk_count,
		    B_TRUE);

		/* If we cannot talk to this node, try the others */
		if (ret != DPM_SUCCESS)
			continue;

		/* Can the other node definitely talk with at least one disk? */
		for (next_disk = query_list; next_disk != NULL;
		    next_disk = next_disk->next) {
			/* Only "OK" is considered as a yes or true */
			if (DPM_PATH_IS_OK(next_disk->status)) {
				free_disk_list(query_list);
				cluster_release_quorum_status(quor_table);
				return (B_TRUE);
			}
		}
	}
	free_disk_list(query_list);
	cluster_release_quorum_status(quor_table);
	return (B_FALSE);
}

/*
 * This function checks to see if all of our monitored shared disks have failed
 * and if so, does another node have contact with one or more of those
 * disks.  Basically, this function implements the auto reboot policy.
 */
void
check_our_disks(const dpm_comm_t *dpmcom)
{
	int ret;
	disk_status_node_t *my_disks = NULL;
	boolean_t all_shared_disks_down = B_TRUE;
	int number_disks_monitored = 0;
	boolean_t another_node_has_my_disks = B_FALSE;
	int perr = 0;
	char nodename[MAXHOSTNAMELEN];

	/* Get the list of all of this node's disks */
	ret = libdpm_get_monitoring_status_all(my_nodeid, &my_disks);
	if (ret == DPM_SUCCESS) {
		disk_status_node_t *cur_disk;
		did_device_list_t *ccr_disk_list = NULL;

		/* Get the list of all disks from CCR */
		ccr_disk_list = list_all_devices("disk");
		if (ccr_disk_list == NULL) {
			/*
			 * SCMSGS
			 * @explanation
			 * Internal error.
			 * @user_action
			 * Contact your authorized Sun service provider to
			 * determine whether a workaround or patch is
			 * available.
			 */
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
				"Failed to get the list of disks from CCR, "
				"line %d\n", __LINE__);
			exit(1);
		}

		perr = pthread_rwlock_rdlock(dpmcom->_lock);
		if (perr != 0) {
			/*
			 * SCMSGS
			 * @explanation
			 * Internal error.
			 * @user_action
			 * Contact your authorized Sun service provider to
			 * determine whether a workaround or patch is
			 * available.
			 */
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "pthread_rwlock_rdlock err %d line %d\n",
			    perr, __LINE__);
			exit(1);
		}

		for (cur_disk = my_disks; cur_disk != NULL;
		    cur_disk = cur_disk->next) {

			if (DPM_PATH_IS_MONITORED(cur_disk->status)) {
				++number_disks_monitored;

				/*
				 * Atleast one shared disk being online/up will
				 * avoid rebooting the node. So skip furthur
				 * checking of non-failed shared disks.
				 */
				if (all_shared_disks_down &&
				    !DPM_PATH_IS_FAIL(cur_disk->status) &&
				    is_disk_shared(cur_disk, ccr_disk_list))
					all_shared_disks_down = B_FALSE;
			}
		}

		/*
		 * We do not have contact to any of the monitored disks, need
		 * to check if any other nodes have contact to my disks.
		 */
		if (number_disks_monitored != 0 && all_shared_disks_down) {
			another_node_has_my_disks =
			    another_node_has_disks(my_disks);
		}

		perr = pthread_rwlock_unlock(dpmcom->_lock);
		if (perr != 0) {
			/*
			 * SCMSGS
			 * @explanation
			 * Internal error.
			 * @user_action
			 * Contact your authorized Sun service provider to
			 * determine whether a workaround or patch is
			 * available.
			 */
			(void) sc_syslog_msg_log(handle, LOG_ERR,
			    MESSAGE,
			    "pthread_rwlock_unlock "
			    "err %d line %d\n",
			    perr, __LINE__);
			exit(1);
		}

		dpm_free_status_list(my_disks);
		(void) did_freeinstlist(ccr_disk_list);

		/*
		 * Grab the global dpmd mutex lock to make sure only one
		 * thread does the check and possible reboot at a time.
		 */
		(void) pthread_mutex_lock(&dpmd_mutex);

		if (number_disks_monitored == 0 || !all_shared_disks_down) {
			if (last_monitoring_status >=
			    MONITOR_DISKS_NO_ACCESS) {
				/*
				 * SCMSGS
				 * @explanation
				 * Need explanation of this message!
				 * @user_action
				 * Need a user action for this message.
				 */
				(void) sc_syslog_msg_log(handle, LOG_INFO,
				    MESSAGE, gettext("Access to a monitored "
				    "disk regained. Automatic reboot "
				    "re-enabled."));
			}
			last_monitoring_status = MONITOR_DISKS_HAS_ACCESS;
			(void) pthread_mutex_unlock(&dpmd_mutex);
			return;
		}

		/* We do not have contact to monitored disks */
		switch (last_monitoring_status) {
		case MONITOR_DISKS_HAS_ACCESS:
			/*
			 * We have lost contact to monitored disks, need to
			 * reboot if any other nodes can take over our disks.
			 */
			/*
			 * SCMSGS
			 * @explanation
			 * Need explanation of this message!
			 * @user_action
			 * Need a user action for this message.
			 */
			(void) sc_syslog_msg_log(handle, LOG_CRIT,
			    MESSAGE, gettext("Access to all monitored disks "
			    "lost."));
			if (!another_node_has_my_disks) {
				/* No one can take over, will not reboot */
				last_monitoring_status =
				    MONITOR_DISKS_NO_ACCESS_NO_FAILOVER;
				(void) pthread_mutex_unlock(&dpmd_mutex);
				return;
			} else {
				/*
				 * There is another node having my disks, will
				 * reboot.
				 */
				break;
			}
		case MONITOR_DISKS_NO_ACCESS_NO_FAILOVER:
			if (another_node_has_my_disks) {
				/*
				 * Now one of the other nodes can take over
				 * Will reboot.
				 */
				break;
			} else {
				/* Do nothing */
				(void) pthread_mutex_unlock(&dpmd_mutex);
				return;
			}
		case MONITOR_DISKS_NO_ACCESS:
			/* Do nothing if there is no change to status */
			(void) pthread_mutex_unlock(&dpmd_mutex);
			return;
		case MONITOR_DISKS_UNKNOWN:
			/*
			 * SCMSGS
			 * @explanation
			 * Need explanation of this message!
			 * @user_action
			 * Need a user action for this message.
			 */
			(void) sc_syslog_msg_log(handle, LOG_CRIT,
			    MESSAGE, gettext("Still cannot access monitored "
			    "disk. Automatic reboot disabled."));

			/* Do nothing if this is the first time we check */
			last_monitoring_status = MONITOR_DISKS_NO_ACCESS;
			(void) pthread_mutex_unlock(&dpmd_mutex);
			return;
		default:
			/* Should never come here, simply return */
			(void) pthread_mutex_unlock(&dpmd_mutex);
			return;
		}

		/* Get the node name */
		(void) gethostname(nodename, sizeof (nodename));
		if (strlen(nodename) == 0) {
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "Cannot determine the node name");
			(void) strcpy(nodename, "Unknown");
		}
		/* Publish an event */
		ret = sc_publish_event(
		    ESC_CLUSTER_DPM_AUTO_REBOOT_NODE,
		    CL_EVENT_PUB_DPM, CL_EVENT_SEV_INFO,
		    CL_EVENT_INIT_SYSTEM, 0, 0, 0,
		    CL_NODE_NAME, SE_DATA_TYPE_STRING, nodename,
		    NULL);
		if (ret != 0) {
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "Publish event error: %d", ret);
		}
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		(void) sc_syslog_msg_log(handle, LOG_CRIT, MESSAGE,
		    gettext("Rebooting node...\n"));

		/* Wait a moment to allow events to propagate */
		(void) sleep(1);
		ret = uadmin(A_REBOOT, AD_BOOT, (uintptr_t)0);
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ALERT, MESSAGE,
		    "Failed to reboot: %s\n", strerror(ret));
		(void) pthread_mutex_unlock(&dpmd_mutex);
		return;
	} else {
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR,
		    MESSAGE, "Could not acquire the node's monitored disk "
		    " list, error %d.", perr);
	}
}

/*
 * Local helper function that checks if the given disk-path is shared.
 * It returns 1 if the disk is shared, otherwise it returns 0. It is
 * called by check_our_disks().
 */
static int
is_disk_shared(const disk_status_node_t *cur_disk,
	did_device_list_t *ccr_disk_list)
{
	int path_count = 0;
	char *disk = NULL;

	/*
	 * The device path we have is an absoulte path. It is either of the
	 * form /dev/did/rdsk/dXs0 or /dev/rdsk/cXtYdZs0.
	 */
	disk = cur_disk->name;

	if (strncmp(disk, SCDPMD_DID_PATH, SCDPMD_DID_PATH_LEN) == 0) {

		path_count = get_path_count_of_did_disk(disk, ccr_disk_list);

	} else if (strncmp(disk, SCDPMD_CTD_PATH, SCDPMD_CTD_PATH_LEN) == 0) {

		path_count = get_path_count_of_ctd_disk(disk, ccr_disk_list);

	} else {

		/* This shouldn't happen. */
	}
	if (path_count > 1) {
		/* Disk is shared */
		return (1);
	} else if (path_count == 1) {
		/* Disk is not shared */
		return (0);
	} else {
		/* Disk was not found in CCR. Something wrong happened. */
		/*
		 * SCMSGS
		 * @explanation
		 * Internal error.
		 * @user_action
		 * Contact your authorized Sun service provider to
		 * determine whether a workaround or patch is
		 * available.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			"Disk %s not found in CCR. Internal error. Line %d\n",
			cur_disk->name, __LINE__);
		exit(1);
	}
}

/*
 * Local helper function which returns number of possible paths for a given
 * DID disk.
 */
static int
get_path_count_of_did_disk(const char *disk, did_device_list_t *ccr_disk_list)
{
	char			*didname;
	int			didinst;
	did_device_list_t 	*curp;
	did_subpath_t 		*pathp;
	int			did_pathcount = 0;

	didname = (char *)strrchr(disk, '/');
	didname++;

	/* Now the didname contains dXs0 */
	didinst = atoi(didname + 1);

	for (curp = ccr_disk_list; curp != NULL; curp = curp->next) {

		if (curp->instance == didinst) {
			for (pathp = curp->subpath_listp; pathp != NULL;
						    pathp = pathp->next) {
				did_pathcount++;

			}
			/* Found the required DID disk */
			break;
		}
	}
	return (did_pathcount);
}
/*
 * Local helper function which returns number of possible paths to the given
 * CTD disk from multiple nodes.
 */
static int
get_path_count_of_ctd_disk(char *disk, did_device_list_t *ccr_disk_list)
{
	did_device_list_t 	*curp;
	did_subpath_t 		*pathp;
	int			ctd_pathcount = 0;
	did_device_list_t	*ctd_didp = NULL;
	char			ctd_libdid_path[MAXPATHLEN];

	/* paths from libdid do not have the slice 0 at the end */
	disk[strlen(disk) - ((sizeof ("s0")-1))] = '\0';

	/*
	 * The device path from libdid is of format "hostname:/dev/rdsk/cXtYdZ".
	 */
	(void) sprintf(ctd_libdid_path, "%s:%s", hostname, disk);

	for (curp = ccr_disk_list; (ctd_didp == NULL) && (curp != NULL);
	    curp = curp->next) {

		for (pathp = curp->subpath_listp; pathp != NULL;
					    pathp = pathp->next) {

			if (strcmp(ctd_libdid_path, pathp->device_path) == 0) {
				ctd_didp = curp;
				break;
			}
		}
	}
	if (ctd_didp != NULL) {
		for (pathp = ctd_didp->subpath_listp; pathp != NULL;
		    pathp = pathp->next) {
			ctd_pathcount++;
		}
	}

	/* restore slice info */
	disk[strlen(disk)] = 's';

	return (ctd_pathcount);
}

/*
 * _dpm_monitor()
 *
 * This function is the thread which does the device path
 * monitoring. The fucntion loops for ever and through the device path
 * incore list and ping the disk when the time to ping is
 * reached. Then we wait for few seconds before we start over. We use
 * the loop and the sleep insted of an alarm due the the probleme
 * catching SIGALRM in ptreaded programs. This made the function more
 * reliable.
 *
 */

void *
_dpm_monitor(void *arg)
{
	dpm_comm_t	*dpmcom;
	dpm_core_lst_t	*devptr;
	time_t		now;
	int		pitv, retry, timeout, msgflag;
	int		status;
	unsigned int	seed;
	boolean_t	auto_reboot_state;
	boolean_t	all_paths_known_ok = B_TRUE;
	time_t		devlist_query_start_timestamp;
	int		devlist_process_time;
	unsigned int	scdpm_thread_sleep_val;

	/*
	 * Wait that all threads have been created,
	 * because in case of a thread creation failure,
	 * data structures are freeed (see dpm_monitord)
	 * and process is exited.
	 */
	while (wait_all_threads_created == 0) {
		(void) sleep(1);
	}

	dpmcom = (dpm_comm_t *)arg;
	devptr = dpmcom->devlst;
	seed = (unsigned int)time(NULL);

	/*
	 * Force reading of the ping initial values.
	 */
	retry = DPM_DEFAULT_RETRY;
	timeout = DPM_DEFAULT_TIMEOUT;
	pitv = DPM_DEFAULT_INTERVAL;
	pitv += ((int)rand_r(&seed) % 20) - 10;
	set_dpm_global(DPMD_FLAG, DPM_FLAG_RECONFIG);
	devlist_query_start_timestamp = time(NULL);

	while (devptr) {
		int perr;

		/* check for message */
		get_dpm_global(DPMD_FLAG, &msgflag);

		/* got a shutdown message! */
		if (msgflag == DPM_FLAG_SHUTDOWN) {
			break;
		}

		/* got a reconfig message! */
		if (msgflag == DPM_FLAG_RECONFIG) {
			get_dpm_global(DPMD_RETRY, &retry);
			get_dpm_global(DPMD_TIMEOUT, &timeout);
			get_dpm_global(DPMD_INTERVAL, &pitv);
			pitv += ((int)rand_r(&seed) % 20) - 10;
		}

		/*
		 * Get the lock if the resources is locked this means
		 * an another thread is doing the test, We just go to
		 * the next disk.
		 */
		perr = pthread_rwlock_rdlock(dpmcom->_lock);
		if (perr != 0) {
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "pthread_rwlock_rdlock err %d line %d\n",
			    perr, __LINE__);
			exit(1);
		}
		perr = pthread_mutex_trylock(&(devptr->_lock));
		if (perr == EBUSY) {
			/*
			 * Another _dpm_monitor thread owns the mutex. This
			 * means that the work for this element is done by
			 * this other thread. Continue to follow the list or
			 * start again at the head of the list. We do not own
			 * the mutex at this point.
			 */
			goto next_2;
		}
		if (perr != 0) {
			/*
			 * SCMSGS
			 * @explanation
			 * Internal error.
			 * @user_action
			 * Contact your authorized Sun service provider to
			 * determine whether a workaround or patch is
			 * available.
			 */
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "pthread_mutex_trylock error %d line %d\n",
			    perr, __LINE__);
			exit(1);
		}
		/* We own the mutex at this point */

		/*
		 * No need to check the deleted paths
		 */
		if (DPM_PATH_IS_DELETED(devptr->mon_status)) {
			goto next_1;
		}

		/*
		 * First we check if the disk list entry has been
		 * changed
		 */
		if (devptr->timestamp == 0)
			log_disk_monitor(devptr);

		/*
		 * Ok! now we can query the device. Only if it is
		 * monitored!
		 */
		now = time(NULL);
		if ((devptr->timestamp + pitv +
		    (((int)rand_r(&seed) % 20) - 10)) > now) {
			goto next_1;
		}

		if (DPM_PATH_IS_MONITORED(devptr->mon_status)) {
			status = query_device(devptr->path, retry, timeout);
			/*
			 * check if the status have been changed since
			 * the last check. If The status have changed,
			 * we save the new status, log and send an
			 * event.
			 */
			if (status != DPM_GET_STATUS(devptr->mon_status)) {
				DPM_SET_STATUS(devptr->mon_status, status);
				log_disk_status(devptr);
			}
			/*
			 * Check whether any queried disk is in failed state
			 * and set the flag accordingly. We will treat the
			 * disk not in OK state as failed.
			 * This is used as indicater to perform autoreboot
			 * checks.
			 */
			if (all_paths_known_ok &&
			    !DPM_PATH_IS_OK(devptr->mon_status)) {
				all_paths_known_ok = B_FALSE;
			}
		} else {
			if (devptr->timestamp == 0) {
				DPM_SET_INIT(status);
				DPM_SET_STATUS(status, DPM_PATH_UNKNOWN);
				DPM_SET_MONITOR(status, DPM_PATH_UNMONITORED);
				devptr->mon_status = status;
				log_disk_status(devptr);
			}
		}
		devptr->timestamp = now;

		if (debug_mode)
			print_debug_status(devptr);
next_1:
		/*
		 * Release the mutex that we own
		 */
		(void) pthread_mutex_unlock(&(devptr->_lock));
next_2:
		if (devptr->next != NULL) {
			/*
			 * Get the next element of the list
			 */
			devptr = devptr->next;
			perr = pthread_rwlock_unlock(dpmcom->_lock);
			if (perr != 0) {
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE,
				    "pthread_rwlock_unlock "
				    "err %d line %d\n",
				    perr, __LINE__);
				exit(1);
			}
		} else {
			/*
			 * The entire list have been process. We wait
			 * few seconds and we startover.
			 */
			devptr = dpmcom->devlst;
			perr = pthread_rwlock_unlock(dpmcom->_lock);
			if (perr != 0) {
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE,
				    "pthread_rwlock_unlock "
				    "err %d line %d\n",
				    perr, __LINE__);
				exit(1);
			}

			/*
			 * If the policy engine doesn't exist, check disks and
			 * perform autoreboot if necessary.
			 * It is not required to perform autoreboot check if
			 * there is no disk failure.
			 */
			/*lint -e746 */
			if (!all_paths_known_ok &&
			    !libdpm_policy_engine_registered()) {
				/* Get auto reboot state */
				status =
				    libdpm_get_local_node_auto_reboot_state(
				    &auto_reboot_state);
				if (status == DPM_SUCCESS &&
				    auto_reboot_state) {
					check_our_disks(dpmcom);
				}
			}
			if (all_paths_known_ok) {
				last_monitoring_status =
				    MONITOR_DISKS_HAS_ACCESS;
			}

			/*
			 * Prevent unnecessary looping.
			 * XXX Still there is scope of improvement.
			 * It is unnecessary the probe the disk status
			 * before disk's ping_interval has elapsed.
			 * So, the sleep time is kept as average of
			 * remaining ping interval time and value in the range
			 * [MIN_SCDPM_THREAD_SLEEP, MAX_SCDPM_THREAD_SLEEP]
			 */

			devlist_process_time =
			    time(NULL) - devlist_query_start_timestamp;

			if (devlist_process_time <= pitv) {
				scdpm_thread_sleep_val =
				    (unsigned)((pitv - devlist_process_time)/2);
			} else {
				scdpm_thread_sleep_val = 0;
			}

			scdpm_thread_sleep_val = MAX(MIN_SCDPM_THREAD_SLEEP,
			    MIN(scdpm_thread_sleep_val,
			    MAX_SCDPM_THREAD_SLEEP));

			(void) sleep(scdpm_thread_sleep_val);

			all_paths_known_ok = B_TRUE;

			devlist_query_start_timestamp = time(NULL);
		}
	}
	pthread_exit(NULL);

	return (NULL);		/* Only to make lint happy */
}
