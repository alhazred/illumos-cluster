/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */


#pragma ident	"@(#)clquery_cluster.c	1.11	08/05/20 SMI"


#include <sys/cl_assert.h>
#include <stdlib.h>
#include <limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <strings.h>
#include <errno.h>
#include <rpc/rpc.h>
#include <rpc/xdr.h>
#include <rpc/svc.h>

#include <rgm/pnm.h>

#include <sys/clconf_int.h>
#include <sys/clconf_property.h>

#include <sys/cladm_int.h>

#include <libscdpm.h>


/*
 * for MHIOC_RESV_KEY_SIZE
 */
/*  #include <sys/mhd.h>  */

#include <cl_query/rpc/cl_query.h>
#include <cl_query/cl_query_types.h>


/*
 * file size max : 8 Ko
 */
#define	MAX_FILE_SIZE 8192
#define	CLUSTER_VERSION_FILE    "/usr/cluster/lib/scadmin/dot.release"
#define	PRODVERS_BUILD_STR "PRODVERS_BUILD"
#define	PRODVERS_STR "PRODVERS"
#define	SRC_STILL_VALID(A, B)    ((A != B) && (*A != '\n'))
#define	SRC_IS_WHITE_SPACE(A)   ((*A == ' ') || (*A == '\t'))

/*
 * lib pnm is not MT safe
 * as we have now several thread to treat request
 * we need a lock
 */


static
pthread_mutex_t pnm_lib_lock = PTHREAD_MUTEX_INITIALIZER; /*lint !e708 */

extern cl_query_error_t authenticate_request(void);
extern cl_query_error_t clquery_free_node_result(cl_query_node_output_t *);

extern cl_query_error_t clquery_free_ipmp_result(cl_query_ipmp_output_t *out);
static cl_query_error_t
clquery_ipmp_adp_dup(const char *node_name,
    cl_query_ipmp_prop_t *ipmp,
    char *ipmp_grp);

extern cl_query_error_t scconf_to_clquery(scconf_errno_t err);

extern cl_query_error_t
clquery_free_cluster_result(cl_query_cluster_output_t *);
extern cl_query_error_t
clquery_free_diskpath_result_list(cl_query_diskpath_prop_t **, int);
static void
    disk_path_info_fill_status(int *dest, int src);
static cl_query_error_t
disk_path_info_fill_info(disk_status_node_t *disk_list,
    cl_query_diskpath_prop_t **res,
    size_t *current_idx, size_t *current_size, const char *current_node_name);
static cl_query_error_t
node_info_get_by_name(char *name, cl_query_node_output_t *output);
static cl_query_error_t node_info_get_all(cl_query_node_output_t *output);

static cl_query_error_t farmnodes_info_get_all(cl_query_node_output_t *output);

static cl_query_error_t node_farm_info_get_all(cl_query_node_output_t *output);

static cl_query_error_t
clconf_to_clquery_node_dup(clconf_obj_t *src,
    quorum_node_status_t *node_qstatus, cl_query_node_prop_t dest);
static cl_query_error_t
clquery_get_joinlist(clconf_obj_t *src, strarr_list * jlist);

static cl_query_error_t clquery_get_nodeid(const char *node_name, int *id);

#ifdef NEEDED
static cl_query_error_t clquery_get_nodename(char **node_name, int id);
#endif
static
quorum_node_status_t *get_quorum_status_node(
	quorum_status_t *pquorum_status, int node_id);

static cl_query_error_t
clquery_get_monitoring_status(uint_t node_id, disk_status_node_t ** disk_list);

/*
 * look for token 'token' within the buffer src
 * the search will be perform up to 'end_of_buf'
 * on success CL_QUERY_OK is returned and result point on the begging of token
 * a valid token is : 'token'[white spaces]'='
 */

static cl_query_error_t
clquery_find_valid_token(char **result, char *src, char *token)
{
	char *tmp_result = NULL;
	int size;
	char *search_start = NULL;

	size = (int)strlen(token);
	search_start = src;

	while (1) {
		tmp_result = strstr(search_start, token);
		if (tmp_result == NULL)
			break;
		/*
		 * validate tmp_result : after token we should find  *
		 * white spaces or '='
		 */
		if (SRC_IS_WHITE_SPACE(((tmp_result + size))) ||
		    (*(tmp_result + size) == '=')) {
			/*
			 * we found a good one
			 */
			break;
		} else {
			search_start = tmp_result + 1;
		}
	}
	if (tmp_result == NULL)
		return (CL_QUERY_EUNEXPECTED);

	*result = tmp_result;
	return (CL_QUERY_OK);
}

/*
 * we receive a buffer src
 * it should be like :
 * 'pattern'<...>'='<...>'value''\n'
 *
 * move src to the begginning of value
 * replace '\n' by '\0'
 * RETURN : CL_QUERY_OK, CL_QUERY_UNEXPECTED otherwise
 * we asume that the file is a system file (owned by root), the user
 * may not modify it, so a minimum of check are performed
 *
 */
static cl_query_error_t
clquery_prepare_line(char **src, char *end_of_buf)
{
	char *src_ptr = *src;

	/*
	 * first : look for '='
	 */
	while ((*src_ptr != '=') && SRC_STILL_VALID(src_ptr, end_of_buf))
		src_ptr++;
	if (!SRC_STILL_VALID(src_ptr, end_of_buf))
		return (CL_QUERY_EUNEXPECTED);

	/*
	 * skip '='
	 */
	src_ptr++;
	if (!SRC_STILL_VALID(src_ptr, end_of_buf))
		return (CL_QUERY_EUNEXPECTED);

	/*
	 * we may found white spaces, skip them
	 */
	while (SRC_IS_WHITE_SPACE(src_ptr) &&
	    SRC_STILL_VALID(src_ptr, end_of_buf))
		src_ptr++;
	if (!SRC_STILL_VALID(src_ptr, end_of_buf))
		return (CL_QUERY_EUNEXPECTED);

	/*
	 * we should be now just before value, go at the end to set eol
	 */
	*src = src_ptr;
	while (!SRC_IS_WHITE_SPACE(src_ptr) &&
	    SRC_STILL_VALID(src_ptr, end_of_buf))
		src_ptr++;

	*src_ptr = '\0';

	return (CL_QUERY_OK);
}

/*
 * read the file 'path' into buffer buf
 * the buffer is allocated to contain all file's data
 * bufsize is set to the size of buf
 * RETRUN : CL_QEURY_OK , CL_QUERY_UNEXPECTED/CL_QUERY_ENOMEM otherwise
 * NOTICE : we assume that the file is small enough to be read in one shot
 * function will failed if size > 8Ko
 */



static cl_query_error_t
clquery_read_file(char *path, char **buf, int *bufsize)
{
	struct stat sbuf;
	ssize_t bytestoread;
	ssize_t diag_read;
	int fd;
	int tries = 3;
	char *tmpfilebufptr;
	char *tmpfilebuf;
	int diag;

	diag = stat(path, &sbuf);
	if (diag != 0)
		return (CL_QUERY_EUNEXPECTED);

	if (sbuf.st_size <= 0)
		return (CL_QUERY_EUNEXPECTED);

	if (sbuf.st_size > MAX_FILE_SIZE)
		return (CL_QUERY_EUNEXPECTED);

	fd = open(path, O_RDONLY);
	if (fd < 0)
		return (CL_QUERY_EUNEXPECTED);

	tmpfilebuf = (char *)malloc((size_t)(sbuf.st_size + 1));
	if (tmpfilebuf == NULL) {
		(void) close(fd);
		return (CL_QUERY_ENOMEM);
	}
	tmpfilebufptr = tmpfilebuf;
	bytestoread = sbuf.st_size;

	while ((tries > 0) && (bytestoread > 0)) {
		diag_read = read(fd, tmpfilebufptr, (size_t)bytestoread);
		if (diag_read > 0) {
			bytestoread -= (ssize_t)diag_read;
			tmpfilebufptr += diag_read;
		} else {
			/*lint -e746 */
			if ((diag_read <= 0) && (errno != EINTR)) {
				/*
				 * something wrong : give up
				 */
				tries = 0;
				break;
			} else {
				tries--;
			}
			/*lint +e746 */
		}

	}

	(void) close(fd);

	if (bytestoread > 0) {
		/*
		 * something went wrong , give up
		 */
		free(tmpfilebuf);
		return (CL_QUERY_EUNEXPECTED);
	}
	/*
	 * to be safe while strstr calls()
	 */
	tmpfilebuf[sbuf.st_size] = '\0';

	*buf = tmpfilebuf;
	*bufsize = (int)sbuf.st_size + 1;

	return (CL_QUERY_OK);

}

/*
 * get the cluster version
 * allocate and fill 'version' with the result
 * TODO : find another way than readding the
 * /usr/cluster/lib/scadmin/dot.release  file !@
 */
/*
 * DESCRIPTION:
 *    read the /usr/cluster/lib/scadmin/dot.release file
 *    to get the cluster version
 *    the version is compose by PRODVERS and PRODVERS_BUILD flag is the file
 *    inside the file we should have :
 *    ....
 *    PRODVERS=<sc vesion>
 *    PRODVERS_BUILD=<build version>
 *    ....
 *    we will return "[<sc vesion>][_<build version>]"
 *
 * INPUT:
 *    char **version : char to fill with the version
 *
 * WARNING:
 *    the code rely on /usr/cluster/lib/scadmin/dot.release syntax
 *    on must be updated if the syntax change
 *
 * OUTPUT:
 *
 *
 * RETURN:
 *    boolean:  TRUE for success
 *              FALSE on any error
 *
 * NOTES:
 *   we assume that the file si small enough to be
 *   read in one shot
 *
 * LOCKS:
 */

static cl_query_error_t
clquery_get_cluster_version(char **version)
{

	char *filebuf;
	int filebufsize;
	char *filebuf_end;
	char *prodvers_build = NULL;
	char *prodvers = NULL;
	int result_size = 0;
	cl_query_error_t cldiag;
	cl_query_error_t diag;
	char *version_ptr;



	cldiag = clquery_read_file(CLUSTER_VERSION_FILE,
	    &filebuf, &filebufsize);

	if (cldiag != CL_QUERY_OK)
		return (CL_QUERY_EUNEXPECTED);

	filebuf_end = filebuf + filebufsize;

	prodvers_build = NULL;
	(void) clquery_find_valid_token(&prodvers_build,
	    filebuf, PRODVERS_BUILD_STR);

	prodvers = NULL;
	(void) clquery_find_valid_token(&prodvers, filebuf, PRODVERS_STR);


	/*
	 * do some additional checks
	 */

	if (prodvers == prodvers_build) {
		free(filebuf);
		return (CL_QUERY_EUNEXPECTED);
	}
	if ((prodvers != NULL) &&
	    ((unsigned long)prodvers < (unsigned long)prodvers_build)) {
		if ((unsigned long)(prodvers + (int)strlen(PRODVERS_STR)) >=
		    (unsigned long)prodvers_build) {
			/*
			 * PRODVERS_STR was found first,  *but the end of
			 * the string is overlapping
			 */
			/*
			 * PRODVERS_BUILD_STR -> something wrong
			 */
			free(filebuf);
			return (CL_QUERY_EUNEXPECTED);
		}
	}
	if ((prodvers_build != NULL) &&
	    ((unsigned long)prodvers_build < (unsigned long)prodvers)) {
		if ((unsigned long)(prodvers_build +
			(int)strlen(PRODVERS_BUILD_STR)) >=
		    (unsigned long)prodvers) {
			/*
			 * PRODVERS_BUILD_STR was found first, but the end
			 * of the string is overlapping
			 */
			/*
			 * PRODVERS_STR -> something wrong
			 */
			free(filebuf);
			return (CL_QUERY_EUNEXPECTED);
		}
	}
	/*
	 * grab prodvers value
	 */
	/*
	 * string starting at prodvers position should be something like:
	 */
	/*
	 * "PRODVERS=3.1u2\n"
	 */

	result_size = 0;
	if (prodvers != NULL) {
		diag = clquery_prepare_line(&prodvers, filebuf_end);
		if (diag == CL_QUERY_OK) {
			/*
			 * everything went right
			 */
			result_size += (int)strlen(prodvers);
		} else {
			/*
			 * did not succed to extract the token
			 * give up for this one
			 */
			prodvers = NULL;
		}
	}
	if (prodvers_build != NULL) {
		diag = clquery_prepare_line(&prodvers_build, filebuf_end);
		if (diag == CL_QUERY_OK) {
			/*
			 * everything went right
			 */
			result_size += (int)strlen(prodvers_build);
		} else {
			/*
			 * did not succed to extract the token
			 * give up for this one
			 */
			prodvers_build = NULL;
		}
	}
	if (result_size == 0) {
		/*
		 * something wrong : give up
		 */
		free(filebuf);
		return (CL_QUERY_EUNEXPECTED);
	}


	/*
	 * + 2: '_' + '\0'
	 */

	*version = (char *)malloc((size_t)result_size + 2);
	if (*version == NULL) {
		free(filebuf);
		return (CL_QUERY_ENOMEM);
	}
	/*
	 * go for strings copies
	 */


	version_ptr = *version;

	if (prodvers != NULL) {
		while (*prodvers != '\0') {
			*version_ptr = *prodvers;
			prodvers++;
			version_ptr++;
		}

		if (prodvers_build != NULL) {
			*version_ptr = '_';
			/*
			 * if prodvers_build was not found, no need of '_'
			 */
			version_ptr++;
		}
	}
	if (prodvers_build != NULL) {
		while (*prodvers_build != '\0') {
			*version_ptr = *prodvers_build;
			prodvers_build++;
			version_ptr++;
		}
	}
	*version_ptr = '\0';

	free(filebuf);

	return (CL_QUERY_OK);
}

static strarr *
clquery_get_joinlist_val(const char *val, unsigned int *count)
{
	unsigned int tmp_count = 0;
	const char *tmp = NULL;
	const char *tmp_stop = NULL;
	strarr *tmp_res = NULL;
	char *tmp_res_str = NULL;
	int len = 0;
	short error = 0;
	unsigned int i = 0;

	/*
	 * value is a ',' separated token string
	 */

	*count = 0;

	/*
	 * first count number of tokens we have
	 */
	tmp = val;
	tmp_count = 0;
	while (*tmp != '\0') {
		if (*tmp == ',')
			tmp_count++;
		tmp++;
	}

	tmp_count++;

	tmp_res = (strarr *)calloc(tmp_count, sizeof (strarr));
	if (tmp_res == NULL)
		return ((strarr *)NULL);

	tmp = val;
	tmp_stop = val;

	for (i = 0; i < tmp_count; i++) {
		len = 0;
		/*
		 * what is the size of next token
		 */
		while ((*tmp_stop != ',') && (*tmp_stop != '\0')) {
			tmp_stop++;
			len++;
		}

		tmp_res[i] = (strarr)malloc((size_t)(len + 1));
		if (tmp_res[i] == NULL) {
			error = 1;
			break;
		}
		/*
		 * copy the token
		 */
		tmp_res_str = tmp_res[i];
		while (tmp != tmp_stop) {
			*tmp_res_str = *tmp;
			tmp_res_str++;
			tmp++;
		}
		*tmp_res_str = '\0';

		/*
		 * skip separator before next loop
		 */
		tmp++;
		tmp_stop++;
	}

	if (error == 1) {
		/*
		 * something went wrong
		 */
		for (i = 0; i < tmp_count; i++) {
			if (tmp_res[i] != NULL)
				free(tmp_res[i]);
		}
		free(tmp_res);
		tmp_res = NULL;
	} else {
		*count = tmp_count;
	}

	return (tmp_res);
}

static cl_query_error_t
clquery_get_joinlist(clconf_obj_t *src, strarr_list * jlist)
{
	const char *value = NULL;


	jlist->strarr_list_len = 0;
	jlist->strarr_list_val = NULL;

	value = clconf_obj_get_property((clconf_obj_t *)src,
	    "auth_joinlist_hostslist");


	/*
	 * empty list
	 */
	if (value == NULL) {
		return (CL_QUERY_OK);
	}

	jlist->strarr_list_val = clquery_get_joinlist_val(value,
	    &jlist->strarr_list_len);
	if (jlist->strarr_list_val == NULL)
		return (CL_QUERY_ENOMEM);

	return (CL_QUERY_OK);

}

/*
 * DESCRIPTION:
 *    This method is the server part of the CLUSTER_INFO_GET
 *     API query. All the cluster related information is retreived
 *     from the scconf library and returned to the user.
 *
 *
 * INPUT:
 *
 *    cl_query_input_t:  Name of the cluster or NULL
 *                       (default case) ignore for now
 *    struct svc_req  :  RPC request handler
 *
 * OUTPUT:
 *    cl_query_cluster_output_t: Cluster information
 *    error is returned in output.error
 *
 * RETURN:
 *    boolean:  TRUE for success
 *              FALSE on any error
 *
 * SIDE EFFECTS:
 *    None
 *
 * LOCKS:
 */

/*
 * ARGSUSED
 */
bool_t
cluster_info_get_1_svc(cl_query_input_t *input,
    cl_query_cluster_output_t *output, struct svc_req *rqstp /*lint !e715 */)
{
	clconf_cluster_t *clconf = NULL;
	const char *char_prop = NULL;
	cl_query_error_t error = CL_QUERY_OK;
	cl_query_cluster_prop_t tmp = NULL;



	if ((output == NULL) || (input == NULL)) {
		error = CL_QUERY_EINVAL;
	}
	/*
	 * Authenticate request
	 */
	if (error == CL_QUERY_OK) {
		error = authenticate_request();
	}
	if (error == CL_QUERY_OK) {
		clconf = clconf_cluster_get_current();
		if (clconf == NULL) {
			error = CL_QUERY_EUNEXPECTED;
		}
	}
	if (error == CL_QUERY_OK) {
		output->current_cluster = NULL;


		tmp = (cl_query_cluster_prop_t)calloc(1,
		    sizeof (cl_query_cluster_prop));
		if (tmp == NULL) {
			error = CL_QUERY_ENOMEM;
		}
	}
/*
 * get cluster name
 */
	if (error == CL_QUERY_OK) {
		char_prop = clconf_obj_get_name((clconf_obj_t *)clconf);
		if (char_prop != NULL) {
			tmp->cluster_name = (char *)strdup(char_prop);
			if (tmp->cluster_name == NULL) {
				error = CL_QUERY_ENOMEM;
			}
		} else {
			tmp->cluster_name = NULL;
		}
	}
	/*
	 * get cluster id
	 */
	if (error == CL_QUERY_OK) {
		char_prop = clconf_obj_get_property((clconf_obj_t *)
		    clconf, "cluster_id");
		if (char_prop != NULL) {
			tmp->cluster_id = (char *)strdup(char_prop);
			if (tmp->cluster_id == NULL) {
				error = CL_QUERY_ENOMEM;
			}
		} else {
			tmp->cluster_id = NULL;
		}
	}
	/*
	 * get cluster install mode
	 */
	if (error == CL_QUERY_OK) {
		char_prop = clconf_obj_get_property((clconf_obj_t *)
		    clconf, "installmode");
		if ((char_prop == NULL) ||
		    (strcasecmp(char_prop, "disabled") == 0)) {
			tmp->install_mode = (unsigned int)CL_QUERY_DISABLED;
		} else {
			tmp->install_mode = (unsigned int)CL_QUERY_ENABLED;
		}
	}
	/*
	 * get cluster version
	 */
	if (error == CL_QUERY_OK) {
		tmp->cluster_version = NULL;
		if (clquery_get_cluster_version(&(tmp->cluster_version))
		    != CL_QUERY_OK) {
			tmp->cluster_version = (char *)strdup("UNKNOWN");
			if (tmp->cluster_version == NULL) {
				error = CL_QUERY_ENOMEM;
			}
		}
	}
	/*
	 * get cluster auth type
	 */
	if (error == CL_QUERY_OK) {
		char_prop =
		    clconf_obj_get_property((clconf_obj_t *)clconf,
		    "auth_joinlist_type");
		if (char_prop == NULL)
			tmp->cluster_auth_type = CLQUERY_AUTH_UNIX;
		else if ((strcasecmp(char_prop, "sys") == 0) ||
		    (strcasecmp(char_prop, "unix") == 0))
			tmp->cluster_auth_type = CLQUERY_AUTH_UNIX;
		else if ((strcasecmp(char_prop, "des") == 0) ||
		    (strcasecmp(char_prop, "df") == 0))
			tmp->cluster_auth_type = CLQUERY_AUTH_DES;
		else {
			tmp->cluster_auth_type = CLQUERY_AUTH_UNKNOWN;
		}
	}
	/*
	 * get cluster auth list
	 */
	if (error == CL_QUERY_OK) {
		error = clquery_get_joinlist((clconf_obj_t *)clconf,
		    &(tmp->cluster_join_list));
	}

	/*
	 * get cluster private network
	 */
	if (error == CL_QUERY_OK) {
		char_prop = clconf_obj_get_property((clconf_obj_t *)
		    clconf, "private_net_number");
		if (char_prop != NULL) {
			tmp->cluster_netaddr = (char *)strdup(char_prop);
		} else {
			tmp->cluster_netaddr =
				(char *)strdup(CLQUERY_DEFAULT_NETADDR);
		}
		if (tmp->cluster_netaddr == NULL) {
			error = CL_QUERY_ENOMEM;
		}
	}

	/*
	 * get cluster netmask
	 */
	if (error == CL_QUERY_OK) {
		char_prop = clconf_obj_get_property((clconf_obj_t *)
		    clconf, "private_netmask");
		if (char_prop != NULL) {
			tmp->cluster_netmask = (char *)strdup(char_prop);
		} else {
			tmp->cluster_netmask =
				(char *)strdup(CLQUERY_DEFAULT_NETMASK);
		}
		if (tmp->cluster_netmask == NULL) {
			error = CL_QUERY_ENOMEM;
		}
	}

	/*
	 * get cluster max nodes
	 */
	if (error == CL_QUERY_OK) {
		char_prop = clconf_obj_get_property((clconf_obj_t *)
		    clconf, "private_maxnodes");
		if (char_prop != NULL) {
			tmp->cluster_max_nodes = (char *)strdup(char_prop);
		} else {
			tmp->cluster_max_nodes =
				(char *)strdup(CLQUERY_DEFAULT_MAXNODES);
		}
		if (tmp->cluster_max_nodes == NULL) {
			error = CL_QUERY_ENOMEM;
		}
	}

	/*
	 * get cluster max priv nets
	 */
	if (error == CL_QUERY_OK) {
		char_prop = clconf_obj_get_property((clconf_obj_t *)
		    clconf, "private_maxprivnets");
		if (char_prop != NULL) {
			tmp->cluster_max_priv_nets = (char *)strdup(char_prop);
		} else {
			tmp->cluster_max_priv_nets =
				(char *)strdup(CLQUERY_DEFAULT_MAXPRIVNETS);
		}
		if (tmp->cluster_max_priv_nets == NULL) {
			error = CL_QUERY_ENOMEM;
		}
	}

	if (error == CL_QUERY_OK) {
		if (clconf_is_farm_mgt_enabled()) {
			tmp->fmm_mode = 1;
		} else {
			tmp->fmm_mode = 0;
		}
	}

	output->current_cluster = tmp;

	if (clconf != NULL) {
		clconf_obj_release((clconf_obj_t *)clconf);
	}
	if (error != CL_QUERY_OK) {
		(void) clquery_free_cluster_result(output);
	}
	output->error = error;

	return (TRUE);
} /*lint !e715 */

void
get_votes_for_node(quorum_status_t *pquorum_status, uint_t *possible,
    uint_t *current, char *node_name)
{
	unsigned int i;
	quorum_node_status_t *node_ptr;
	int nodeidp;

	*possible = 0;
	*current = 0;


	if (node_name == NULL)
		return;

	if (clquery_get_nodeid(node_name, &nodeidp) != CL_QUERY_OK)
		return;

	for (i = 0; i < pquorum_status->num_nodes; i++) {
		node_ptr = &(pquorum_status->nodelist[i]);
		if (node_ptr->nid == (unsigned int)nodeidp) {
			*possible = node_ptr->votes_configured;
			if (node_ptr->state == QUORUM_STATE_ONLINE) {
				*current = *possible;
			}
		}
	}
}

static cl_query_error_t
clconf_to_clquery_node_dup(clconf_obj_t *src,
    quorum_node_status_t *node_qstatus, cl_query_node_prop_t dest)
{

	clconf_iter_t *adapter_iter;
	clconf_obj_t *adapter;
	const char *char_prop;
	uint_t adap_count;
	uint_t i;
	boolean_t  autorebootlist[NODEID_MAX];

	int scdpm_error = DPM_SUCCESS;
	boolean_t auto_reboot_status;
	char *just_for_lint = NULL;

/*
 * #define	_DEBUG
 */

#ifdef _DEBUG
	const char *r;
	clconf_iter_t *prop_iter;
	clconf_obj_t *obj_prop;

	prop_iter = clconf_obj_get_properties(src);
	if (prop_iter != NULL) {
		while ((obj_prop = clconf_iter_get_current(prop_iter))
		    != NULL) {
			r = clpl_get_name(obj_prop);
			if (r != NULL)
				printf("property nmae = %s\n", r);
			clconf_iter_advance(prop_iter);
		}
		clconf_iter_release(prop_iter);
	}
#endif

	/*
	 * get_quorum_status_node(int node_id) may return NULL
	 */
	/*
	 * we catch the error here
	 */
	if (node_qstatus == NULL)
		return (CL_QUERY_EINVAL);


/*
 * node name
 */
	char_prop = clconf_obj_get_name(src);

	if (char_prop != NULL) {
		dest->node_name = (char *)strdup(char_prop);
		if (dest->node_name == NULL) {
			return (CL_QUERY_ENOMEM);
		}
	} else {
		dest->node_name = NULL;
	}

	dest->node_category = strdup("1");
	/*
	 * node id
	 */
	dest->node_id = (unsigned int)clconf_obj_get_id(src);

	/*
	 * node enabled ?
	 */
	if (clconf_obj_enabled(src)) {
		/*
		 * printf("NODE %d IS UP\n",dest->node_id);
		 */
		dest->state = CL_QUERY_ENABLED;
	} else {
		/*
		 * printf("NODE %d IS DOWN\n",dest->node_id);
		 */
		dest->state = CL_QUERY_DISABLED;
	}



/*
 * node possible quorum votes
 */

	dest->possible_quorum_votes = node_qstatus->votes_configured;

/*
 * node current quorum votes
 */
	dest->cur_quorum_votes = 0;

	/*
	 * node state
	 */
	switch (node_qstatus->state) {
	case QUORUM_STATE_ONLINE:
		dest->status = CL_QUERY_ONLINE;
		dest->cur_quorum_votes = dest->possible_quorum_votes;
		break;
	case QUORUM_STATE_OFFLINE:
		dest->status = CL_QUERY_OFFLINE;
		break;
	default:
		dest->status = CL_QUERY_UNKNOWN;
	}

	/*
	 * node_reboot_flag_status
	 */

	if ((scdpm_error = libdpm_get_all_auto_reboot_state(autorebootlist)) !=
	    DPM_SUCCESS) {
		return (CL_QUERY_ECORRUPT);
	}

	auto_reboot_status = autorebootlist[dest->node_id - 1];

	if (auto_reboot_status) {
		dest->node_reboot_flag_status = CL_QUERY_REBOOT_FLAG_ENABLE;
	} else {
		dest->node_reboot_flag_status = CL_QUERY_REBOOT_FLAG_DISABLE;
	}

	/*
	 * node private hostname
	 */
	char_prop = clconf_obj_get_property(src, "private_hostname");
	if (char_prop != NULL) {
		dest->private_host_name = (char *)strdup(char_prop);
		if (dest->private_host_name == NULL) {
			return (CL_QUERY_ENOMEM);
		}
	} else {
		dest->private_host_name = NULL;
	}



	/*
	 * node reservation key
	 */
	char_prop = clconf_obj_get_property(src, "quorum_resv_key");
	if (char_prop != NULL) {
		dest->reservation_key = (char *)strdup(char_prop);
		if (dest->reservation_key == NULL) {
			return (CL_QUERY_ENOMEM);
		}
	} else {
		dest->reservation_key = NULL;
	}

	dest->public_adapters.strarr_list_len = 0;
	dest->public_adapters.strarr_list_val = NULL;
	dest->private_adapters.strarr_list_len = 0;
	dest->private_adapters.strarr_list_val = NULL;

/*
 * get node's adapters
 */
	adapter_iter = clconf_node_get_adapters((clconf_node_t *)src);
	if (adapter_iter != NULL) {
		adap_count = (uint_t)clconf_iter_get_count(adapter_iter);

		if (adap_count > 0) {
			dest->private_adapters.strarr_list_len = adap_count;
			dest->private_adapters.strarr_list_val =
			    (strarr *)calloc(adap_count, sizeof (strarr));
			if (dest->private_adapters.strarr_list_val == NULL) {
				clconf_iter_release(adapter_iter);
				return (CL_QUERY_ENOMEM);
			}
			i = 0;
			while (
				(adapter =
				    clconf_iter_get_current(adapter_iter))
				    != NULL) {
				char_prop = clconf_obj_get_name(adapter);
				if (char_prop != NULL) {
					dest->private_adapters.
					    strarr_list_val[i] =
					    (char *)strdup(char_prop);
					just_for_lint =
					    dest->private_adapters.
					    strarr_list_val[i];
					if (just_for_lint == NULL) {
						clconf_iter_release
						    (adapter_iter);
						return (CL_QUERY_ENOMEM);
					}
					i++;
				}
				clconf_iter_advance(adapter_iter);
			}
		}
		if (adapter_iter != NULL)
			clconf_iter_release(adapter_iter);
	}
	return (CL_QUERY_OK);

}

static quorum_node_status_t *
get_quorum_status_node(quorum_status_t *pquorum_status, int node_id)
{

	unsigned int i;


	for (i = 0; i < pquorum_status->num_nodes; i++) {
		if ((pquorum_status->nodelist[i]).nid ==
		    (unsigned int)node_id) {
			return (&(pquorum_status->nodelist[i]));
		}
	}

	return ((quorum_node_status_t *)NULL);

}



/*
 * DESCRIPTION:
 *    This method use clconf library to get node informations
 *
 * INPUT:
 *    name:  Name of the Node
 *
 * OUTPUT:
 *
 * RETURN:
 *    cl_query_error_t : error occured during catch of information
 *              FALSE on any error
 *
 * SIDE EFFECTS:
 *    None
 *
 * LOCKS:
 */

/*
 * ARGSUSED
 */
static cl_query_error_t
node_info_get_by_name(char *name, cl_query_node_output_t *output)
{


	cl_query_error_t error = CL_QUERY_OK;
	clconf_cluster_t *clconf = NULL;
	clconf_iter_t *node_iter = NULL;
	clconf_obj_t *node = NULL;
	const char *char_prop = NULL;
	quorum_status_t *pquorum_status = NULL;
	int id = 0;

	/*
	 * get quorum status, needed to get votes and states
	 */

	if (cluster_get_quorum_status(&pquorum_status) != 0) {
		return (CL_QUERY_EUNEXPECTED);
	}
	clconf = clconf_cluster_get_current();
	if (clconf == NULL) {
		return (CL_QUERY_EUNEXPECTED);
	}
	/*
	 * Get the number of nodes in the cluster
	 */


	node_iter = clconf_cluster_get_nodes(clconf);
	if (node_iter == NULL) {
		clconf_obj_release((clconf_obj_t *)clconf);
		return (CL_QUERY_EUNEXPECTED);
	}
	/*
	 * user wants info on one node check if we can find it
	 */
	while ((node = clconf_iter_get_current(node_iter)) != NULL) {
		char_prop = clconf_obj_get_name(node);
		if ((char_prop != NULL) && (strcmp(char_prop, name) == 0)) {
			id = clconf_obj_get_id(node);
			break;
		}
		clconf_iter_advance(node_iter);
	}

	if (node == NULL) {
		/*
		 * we did not find it
		 */
		if (node_iter != NULL)
			clconf_iter_release(node_iter);
		if (clconf != NULL)
			clconf_obj_release((clconf_obj_t *)clconf);
		return (CL_QUERY_ENOTCLUSTER);
	}
	/*
	 * Allocate memory for the node list
	 */

	/*
	 * Set the number of nodes in the cluster in the output param
	 */
	output->node_list.node_list_len = 0;
	output->node_list.node_list_val = NULL;

	/*
	 * Allocate memory for the node list
	 */
	output->node_list.node_list_val = (cl_query_node_prop_t *)
	    calloc(1, sizeof (cl_query_node_prop_t));

	if (output->node_list.node_list_val == NULL) {
		if (node_iter != NULL)
			clconf_iter_release(node_iter);
		if (clconf != NULL)
			clconf_obj_release((clconf_obj_t *)clconf);
		return (CL_QUERY_ENOMEM);
	}
	output->node_list.node_list_len = 1;


	/*
	 * Allocate memory for each of the node element
	 */

	output->node_list.node_list_val[0] =
	    (struct cl_query_node_prop *)calloc(1,
	    sizeof (struct cl_query_node_prop));

	if (output->node_list.node_list_val[0] == NULL) {
		if (node_iter != NULL)
			clconf_iter_release(node_iter);
		if (clconf != NULL)
			clconf_obj_release((clconf_obj_t *)clconf);
		return (CL_QUERY_ENOMEM);
	}
	/*
	 * go for copy
	 */


	error = clconf_to_clquery_node_dup(node,
	    get_quorum_status_node(pquorum_status, id),
	    output->node_list.node_list_val[0]);

	if (node_iter != NULL)
		clconf_iter_release(node_iter);
	if (clconf != NULL)
		clconf_obj_release((clconf_obj_t *)clconf);

	return (error);

}

/*
 * DESCRIPTION:
 *    This method use clconf library to get all nodes informations
 *
 * INPUT:
 * OUTPUT:
 *
 * RETURN:
 *    cl_query_error_t : error occured during catch of information
 *              FALSE on any error
 *
 * SIDE EFFECTS:
 *    None
 *
 * LOCKS:
 */


static cl_query_error_t
node_farm_info_get_all(cl_query_node_output_t *output) {
	cl_query_node_output_t node_output;
	cl_query_node_output_t farm_output;
	cl_query_error_t error = CL_QUERY_OK;
	uint_t i, j;

	node_output.node_list.node_list_val = NULL;
	node_output.node_list.node_list_len = 0;
	farm_output.node_list.node_list_val = NULL;
	farm_output.node_list.node_list_len = 0;

	if ((error = node_info_get_all(&node_output)) != CL_QUERY_OK) {
		(void) clquery_free_node_result(&node_output);
		return (error);
	}
	if ((error = farmnodes_info_get_all(&farm_output)) != CL_QUERY_OK) {
		(void) clquery_free_node_result(&node_output);
		(void) clquery_free_node_result(&farm_output);
		return (error);
	}
	/* now merge node and farm outputs */
	output->node_list.node_list_len =
	    node_output.node_list.node_list_len +
	    farm_output.node_list.node_list_len;

	output->node_list.node_list_val = (cl_query_node_prop_t *)
	    calloc((size_t)output->node_list.node_list_len,
	    sizeof (cl_query_node_prop_t));
	if (output->node_list.node_list_val == NULL) {
		(void) clquery_free_node_result(&node_output);
		(void) clquery_free_node_result(&farm_output);
		return (CL_QUERY_ENOMEM);
	}
	for (i = 0; i < node_output.node_list.node_list_len; i++) {
		output->node_list.node_list_val[i] =
		    node_output.node_list.node_list_val[i];
	}
	for (j = 0; j < farm_output.node_list.node_list_len; j++, i++) {
		output->node_list.node_list_val[i] =
		    farm_output.node_list.node_list_val[j];
	}
	/* no deep free required, since items referenced by output variable */
	/* so just free the container */
	free(node_output.node_list.node_list_val);
	free(farm_output.node_list.node_list_val);
	return (error);
}

static cl_query_error_t
node_info_get_all(cl_query_node_output_t *output)
{

	cl_query_error_t error = CL_QUERY_OK;
	clconf_cluster_t *clconf = NULL;
	clconf_iter_t *node_iter = NULL;
	clconf_obj_t *node = NULL;
	int node_count = 0;
	uint_t i = 0;
	quorum_status_t *pquorum_status = NULL;
	int id;

	/*
	 * get quorum status, needed to get votes and states
	 */

	if (cluster_get_quorum_status(&pquorum_status) != 0) {
		return (CL_QUERY_EUNEXPECTED);
	}
/*
 * Get the cluster properties handle from clconf lib
 */

	clconf = clconf_cluster_get_current();
	if (clconf == NULL) {
		return (CL_QUERY_EUNEXPECTED);
	}
	node_iter = clconf_cluster_get_nodes(clconf);
	if (node_iter == NULL) {
		if (clconf != NULL)
			clconf_obj_release((clconf_obj_t *)clconf);
		return (CL_QUERY_EUNEXPECTED);
	}
	/*
	 * Get the number of nodes in the cluster
	 */

	node_count = clconf_iter_get_count(node_iter);

	if (node_count <= 0) {
		if (node_iter != NULL)
			clconf_iter_release(node_iter);
		if (clconf != NULL)
			clconf_obj_release((clconf_obj_t *)clconf);
		return (CL_QUERY_ENOTCLUSTER);
	}
	/*
	 * Allocate memory for the node list
	 */

	/*
	 * Set the number of nodes in the cluster in the output param
	 */
	output->node_list.node_list_len = (unsigned int)node_count;
	output->node_list.node_list_val = NULL;

	/*
	 * Allocate memory for the node list
	 */
	output->node_list.node_list_val = (cl_query_node_prop_t *)
	    calloc((size_t)node_count, sizeof (cl_query_node_prop_t));

	if (output->node_list.node_list_val == NULL) {
		if (node_iter != NULL)
			clconf_iter_release(node_iter);
		if (clconf != NULL)
			clconf_obj_release((clconf_obj_t *)clconf);
		return (CL_QUERY_ENOMEM);
	}
	/*
	 * Allocate memory for each of the node element
	 */
	for (i = 0; i < (unsigned int)node_count; i++) {
		output->node_list.node_list_val[i] =
		    (struct cl_query_node_prop *)
		    calloc(1, sizeof (struct cl_query_node_prop));

		if (output->node_list.node_list_val[i] == NULL) {
			if (node_iter != NULL)
				clconf_iter_release(node_iter);
			if (clconf != NULL)
				clconf_obj_release((clconf_obj_t *)clconf);
			return (CL_QUERY_ENOMEM);
		}
	}



	/*
	 * go for copy
	 */



	i = 0;
	while (((node = clconf_iter_get_current(node_iter)) != NULL) &&
	    (i < (unsigned int)node_count)) {
		/*
		 * find the right index in quorum status nodes list
		 */
		id = clconf_obj_get_id(node);
		error = clconf_to_clquery_node_dup(node,
		    get_quorum_status_node(pquorum_status, id),
		    output->node_list.node_list_val[i]);
		if (error != CL_QUERY_OK)
			break;

		clconf_iter_advance(node_iter);
		i++;
	}

	if (node_iter != NULL)
		clconf_iter_release(node_iter);
	if (clconf != NULL)
		clconf_obj_release((clconf_obj_t *)clconf);
	if (pquorum_status != NULL)
		cluster_release_quorum_status(pquorum_status);

	return (error);

}

static cl_query_error_t
farmnode_to_clquery_node_dup(scconf_farm_nodelist_t *src,
	cl_query_node_prop_t dest)
{

	char *tmpptr = NULL;
	char *adapter_cpy = NULL;
	char *ptr =  NULL;
	uint_t adap_count, i;
	scconf_errno_t scconferr = SCCONF_NOERR;
	uint_t isfarmmember;

	dest->node_id = src->scconf_node_nodeid;
	dest->node_name = strdup(src->scconf_node_nodename);
	if (dest->node_name == NULL)
		return (CL_QUERY_ENOMEM);
	if ((dest->node_category = strdup("2")) == NULL)
		return (CL_QUERY_ENOMEM);

	for (adap_count = 0, ptr = src->scconf_node_adapters;
	    ptr != NULL; adap_count++, ptr = strchr(ptr, ',')) {
	}

	if (adap_count > 0) {
		dest->private_adapters.strarr_list_len = adap_count;
		dest->private_adapters.strarr_list_val =
		    (strarr *)calloc(adap_count, sizeof (strarr));
		if (dest->private_adapters.strarr_list_val == NULL) {
			return (CL_QUERY_ENOMEM);
		}
		adapter_cpy = strdup(src->scconf_node_adapters);
		if (adapter_cpy == NULL)
			return (CL_QUERY_ENOMEM);

		for (i = 0, ptr = adapter_cpy; ptr != NULL; i++) {

			tmpptr = strchr(ptr, ',');
			if (tmpptr != NULL)
				*tmpptr++ = '\0';
			dest->private_adapters.strarr_list_val[i] = strdup(ptr);
			if (dest->private_adapters.strarr_list_val[i] == NULL)
				return (CL_QUERY_ENOMEM);
			ptr = tmpptr;
		}
		ASSERT(i == adap_count);
		free(adapter_cpy);
	}

	dest->state = (src->scconf_node_monstate == SCCONF_STATE_ENABLED) ?
	    CL_QUERY_ENABLED : CL_QUERY_DISABLED;

	/* Get the farm node member status */
	scconferr = scconf_isfarmmember(dest->node_id, &isfarmmember);
	if (scconferr != SCCONF_NOERR) {
		return (CL_QUERY_EUNEXPECTED);
	}

	if (isfarmmember) {
		dest->status = CL_QUERY_ONLINE;
	} else {
		dest->status = CL_QUERY_OFFLINE;
	}

	return (CL_QUERY_OK);
}

/*
 * farmnodes_info_get_all
 *
 * Return the farmnodes names and their monitoring status.
 *
 * Possible return values:
 *
 *	CL_QUERY_OK		- success
 *	SCCONF_ENOCLUSTER	- node is not part of a cluster
 *	SCCONF_ENOEXIST		- the nodeid is not found
 *	SCCONF_EINVAL		- invalid arguments
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
static cl_query_error_t
farmnodes_info_get_all(cl_query_node_output_t *output)
{
	uint_t node_count;
	uint_t i, j;
	scconf_farm_nodelist_t *farm_nodelist = (scconf_farm_nodelist_t *)0;
	scconf_farm_nodelist_t *farmnodes;

	cl_query_error_t rstatus = CL_QUERY_OK;
	scconf_errno_t scconferr = SCCONF_NOERR;

	/* Get the farm nodes list */
	scconferr = scconf_get_farmnodes(&farm_nodelist);
	if (scconferr != SCCONF_NOERR) {
		rstatus = scconf_to_clquery(scconferr);
		goto cleanup;
	}

	if (farm_nodelist == NULL) {
		output->node_list.node_list_len = 0;
		output->node_list.node_list_val = NULL;
		goto cleanup;
	}

	/*
	 * Getting Total number of nodes
	 */

	for (node_count = 0, farmnodes = farm_nodelist; farmnodes;
	    farmnodes = farmnodes->scconf_node_next) {
		node_count++;
	}

	output->node_list.node_list_len = node_count;
	output->node_list.node_list_val = NULL;

	/*
	 * Allocate memory for the node list
	 */
	output->node_list.node_list_val = (cl_query_node_prop_t *)
	    calloc((size_t)node_count, sizeof (cl_query_node_prop_t));

	if (output->node_list.node_list_val == NULL) {
		rstatus = CL_QUERY_ENOMEM;
		goto cleanup;
	}

	/*
	 * Allocate memory for each of the node element
	 */
	for (i = 0; i < node_count; i++) {
		output->node_list.node_list_val[i] =
		    (cl_query_node_prop_t)
		    calloc(1, sizeof (struct cl_query_node_prop));

		if (output->node_list.node_list_val[i] == NULL) {
			rstatus = CL_QUERY_ENOMEM;
			goto cleanup;
		}
	}

	/* Fill the farm_nodelist with node names and their monitoring state */
	for (i = 0, farmnodes = farm_nodelist; farmnodes;
	    farmnodes = farmnodes->scconf_node_next) {
		/* Get the nodeid */
		rstatus = farmnode_to_clquery_node_dup(farmnodes,
				    output->node_list.node_list_val[i]);

		if (rstatus != CL_QUERY_OK)
			goto cleanup;

		i++;
	}
cleanup:

	/* free farm_nodelist linkde list */
	if (rstatus != CL_QUERY_OK) {
		(void) clquery_free_node_result(output);
	}

	return (rstatus);
}

/*
 * DESCRIPTION:
 *    This method is the server part of the NODE_INFO_GET API
 *     query. Information of all the nodes in the cluster is
 *     retreived from the scconf library and returned to the user.
 *
 *
 * INPUT:
 *    cl_query_input_t:  Name of the Node or NULL (default: all nodes )
 *    struct svc_req  :  RPC request handler
 *
 * OUTPUT:
 *    cl_query_node_output_t: Node information
 *    error is returned in output.error
 *
 * RETURN:
 *    boolean:  TRUE for success
 *              FALSE on any error
 *
 * SIDE EFFECTS:
 *    None
 *
 * LOCKS:
 */

/*
 * ARGSUSED
 */
bool_t
node_info_get_1_svc(cl_query_input_t *input,
    cl_query_node_output_t *output, struct svc_req *rqstp /*lint !e715 */)
{

	cl_query_error_t error = CL_QUERY_OK;
	static boolean_t iseuropa;
	scconf_errno_t scconferr = SCCONF_NOERR;

	/*
	 * Validate input parameters
	 */
	if ((output == NULL) || (input == NULL)) {
		output->error = CL_QUERY_EINVAL;
		return (TRUE);
	}

	/*
	 * Authenticate request
	 */
	output->error = authenticate_request();
	if (output->error != CL_QUERY_OK) {
		return (TRUE);
	}

	/* Check for Europa */
	scconferr = scconf_iseuropa(&iseuropa);
	if (scconferr != SCCONF_NOERR) {
		output->error = scconf_to_clquery(scconferr);
		return (TRUE);
	}

	/*
	 * Initialize output parameter
	 */
	output->error = CL_QUERY_OK;
	output->node_list.node_list_len = 0;
	output->node_list.node_list_val = NULL;


	if ((input->name == NULL) || (strlen(input->name) == 0)) {
		if (iseuropa)
			error = node_farm_info_get_all(output);
		else
			error = node_info_get_all(output);

	} else {
		/* we assume that name query won't work for farm nodes?? */
		error = node_info_get_by_name(input->name, output);
	}

	/*
	 * Check if we found at least one entry
	 */
	if (error != CL_QUERY_OK) {
		(void) clquery_free_node_result(output);
	}
	output->error = error;

	return (TRUE);
}/*lint !e715 */

#define	DEFAULT_GRP_COUNT_PER_NODE 5
#define	REALLOC_SIZE 20

static cl_query_error_t
clquery_ipmp_adp_dup(const char *node_name,
    cl_query_ipmp_prop **ipmp,
    char *ipmp_grp)
{

	pnm_status_t status;
	pnm_grp_status_t grp_status;
	cl_query_ipmp_prop_t tmp = NULL;

	uint_t adp_idx = 0;

	tmp = (cl_query_ipmp_prop_t)calloc(1,
	    sizeof (cl_query_ipmp_prop));
	if (tmp == NULL) {
		return (CL_QUERY_ENOMEM);
	}



	/*
	 * get the ipmp group name
	 */
	tmp->group_name = (char *)strdup(ipmp_grp);
	if (tmp->group_name == NULL) {
		return (CL_QUERY_ENOMEM);
	}
	tmp->node_name = (char *)strdup(node_name);
	if (tmp->node_name == NULL) {
		return (CL_QUERY_ENOMEM);
	}
	/*
	 * get the ipmp group status
	 */
	if (pnm_group_status(ipmp_grp, &status) != 0) {
		return (CL_QUERY_EUNEXPECTED);
	}
	switch (status.status) {
	case PNM_STAT_OK:
		tmp->status = CL_QUERY_ONLINE;
		break;
	case PNM_STAT_STANDBY:
	case PNM_STAT_DOWN:
		tmp->status = CL_QUERY_OFFLINE;
		break;
	default:
		tmp->status = CL_QUERY_UNKNOWN;
	}

	pnm_status_free(&status);

	/*
	 * get ipmp group adapter list
	 */
	if (pnm_grp_adp_status(ipmp_grp, &grp_status) != 0) {
		return (CL_QUERY_EUNEXPECTED);
	}

	tmp->adapter_list.strarr_list_val =
	    (strarr *)calloc(grp_status.num_adps, sizeof (strarr));
	if (tmp->adapter_list.strarr_list_val == NULL) {
		pnm_grp_status_free(&grp_status);
		return (CL_QUERY_EUNEXPECTED);
	}
	tmp->adapter_list.strarr_list_len = grp_status.num_adps;

	for (adp_idx = 0; adp_idx < grp_status.num_adps; adp_idx++) {
		tmp->adapter_list.strarr_list_val[adp_idx] =
		    (char *)strdup(grp_status.pnm_adp_stat[adp_idx].
			adp_name);
		if (tmp->adapter_list.strarr_list_val[adp_idx] ==
		    NULL) {
			pnm_grp_status_free(&grp_status);
			return (CL_QUERY_EUNEXPECTED);
		}
	}


	pnm_grp_status_free(&grp_status);

	*ipmp = tmp;

	return (CL_QUERY_OK);
}

/*
 * DESCRIPTION:
 *    This method is the server part of the IPMP_INFO_GET API query.
 *     Information of all the ipmp groups in the cluster is
 *
 *
 *
 * INPUT:
 *    cl_query_input_t:  Name of the IPMP group or NULL (default:all groups)
 *    struct svc_req  :  RPC request handler
 *
 * OUTPUT:
 *    cl_query_ipmp_output_t: IPMP group information
 *    error is returned in output.error
 *
 * RETURN:
 *    boolean:  TRUE for success
 *              FALSE on any error
 *
 * SIDE EFFECTS:
 *    None
 *
 * LOCKS:
 */

/*
 * ARGSUSED
 */

bool_t
ipmp_info_get_1_svc(cl_query_input_t *input,
    cl_query_ipmp_output_t *output, struct svc_req *rqstp /*lint !e715 */)
{

	clconf_cluster_t *clconf = NULL;
	clconf_iter_t *node_iter = NULL;
	clconf_obj_t *node = NULL;
	uint_t node_count = 0;
	uint_t current_grp_list_size = 0;
	uint_t current_grp_idx = 0;

	char *input_name_to_use = NULL;
	cl_query_error_t error = CL_QUERY_OK;
	const char *current_node_name = NULL;
	pnm_group_list_t listp = NULL;
	char *next_gr_str = NULL;
	char *gr_str = NULL;
	cl_query_ipmp_prop_t *tmp_realloc = NULL;

	quorum_node_status_t	*node_qstatus = NULL;
	quorum_status_t		*pquorum_status = NULL;

	if ((input == NULL) || (output == NULL)) {
		error = CL_QUERY_EINVAL;
	}
	output->ipmp_list.ipmp_list_len = 0;
	output->ipmp_list.ipmp_list_val = NULL;

	if (error == CL_QUERY_OK) {
		if (pthread_mutex_lock(&pnm_lib_lock) != 0) {
			error = CL_QUERY_EUNEXPECTED;
		}
	}
	if ((input->name == NULL) || (strlen(input->name) == 0)) {
		input_name_to_use = NULL;
	} else {
		input_name_to_use = input->name;
	}


	if (error == CL_QUERY_OK) {
		error = authenticate_request();
		if (error != CL_QUERY_OK) {
			output->error = error;
			(void) pthread_mutex_unlock(&pnm_lib_lock);
			return (TRUE);
		}
	}


	if (cluster_get_quorum_status(&pquorum_status) != 0) {
		output->error = CL_QUERY_EUNEXPECTED;
		(void) pthread_mutex_unlock(&pnm_lib_lock);
		return (TRUE);
	}

	/*
	 * no way to know how many groups we gonna retrieve
	 */
	/*
	 * alloc once and realloc if necesary
	 */
	/*
	 * (default alloc of DEFAULT_GRP_COUNT_PER_NODE groups per node)
	 */



	if (error == CL_QUERY_OK) {
		clconf = clconf_cluster_get_current();
		if (clconf == NULL) {
			error = CL_QUERY_EUNEXPECTED;

		}
	}
	if (error == CL_QUERY_OK) {
		node_iter = clconf_cluster_get_nodes(clconf);
		if (node_iter == NULL) {
			error = CL_QUERY_EUNEXPECTED;
		}
	}
	if (error == CL_QUERY_OK) {
		node_count = 0;
		while ((node = clconf_iter_get_current(node_iter)) != NULL) {
			/*
			 * count only node which are up (online)
			 */
			node_qstatus =
			    get_quorum_status_node(pquorum_status,
				(int)clconf_obj_get_id(node));
			if ((node_qstatus != NULL) &&
			    (node_qstatus->state == QUORUM_STATE_ONLINE)) {
				node_count++;
			}
			clconf_iter_advance(node_iter);
		}
		if (node_iter != NULL)
			clconf_iter_release(node_iter);
		node_iter = NULL;
	}
	if ((node_count == 0) && (error == CL_QUERY_OK)) {
		error = CL_QUERY_EUNEXPECTED;
	}
	current_grp_list_size = 0;

	if (error == CL_QUERY_OK) {

		/*
		 * go for allocation
		 */
		output->ipmp_list.ipmp_list_val =
		    (cl_query_ipmp_prop_t *)calloc(node_count *
		    DEFAULT_GRP_COUNT_PER_NODE, sizeof (cl_query_ipmp_prop_t));
		if (output->ipmp_list.ipmp_list_val == NULL) {
			error = CL_QUERY_ENOMEM;
		} else {
			current_grp_list_size =
			    node_count * DEFAULT_GRP_COUNT_PER_NODE;
		}
	}
	current_grp_idx = 0;


	/*
	 * reset iterator and go for information copy
	 */

	if (error == CL_QUERY_OK) {
		node_iter = clconf_cluster_get_nodes(clconf);
		if (node_iter == NULL) {
			error = CL_QUERY_EUNEXPECTED;
		}
	}
	if (error == CL_QUERY_OK) {
		while (((node = clconf_iter_get_current(node_iter)) != NULL) &&
		    (error == CL_QUERY_OK)) {

			node_qstatus =
			    get_quorum_status_node(pquorum_status,
				(int)clconf_obj_get_id(node));
			if ((node_qstatus == NULL) ||
			    (node_qstatus->state != QUORUM_STATE_ONLINE)) {
				clconf_iter_advance(node_iter);
				continue;
			}

			current_node_name = clconf_obj_get_name(node);
			if (current_node_name == NULL) {
				clconf_iter_advance(node_iter);
				continue;
			}
			/*
			 * Go to the pnmd of the specified node
			 */
			if (pnm_init(current_node_name) != 0) {
				clconf_iter_advance(node_iter);
				continue;
			}
			/*
			 * get ipmp group list for this node
			 */
			listp = NULL;
			if (pnm_group_list(&listp) != 0) {
				clconf_iter_advance(node_iter);
				continue;
			}

			next_gr_str = listp;
			while ((gr_str = strtok_r(next_gr_str, ":",
				    &next_gr_str)) != NULL) {

				if ((input_name_to_use != NULL) &&
				    (strcmp(input_name_to_use, gr_str) != 0)) {
					continue;
				}
				error = clquery_ipmp_adp_dup(current_node_name,
				    &(output->
					ipmp_list.
					ipmp_list_val[current_grp_idx]),
				    gr_str);
/*
 * Close the connection to the specified node's
 * pnmd
 */


				if (error != CL_QUERY_OK) break;

				current_grp_idx++;
				if (current_grp_idx >= current_grp_list_size) {
					tmp_realloc =
					    (cl_query_ipmp_prop_t*)realloc(
						    output->
							ipmp_list.
							ipmp_list_val,
						(current_grp_list_size +
							    REALLOC_SIZE) *
						sizeof (cl_query_ipmp_prop));

					if (tmp_realloc == NULL) {
						error = CL_QUERY_ENOMEM;
					} else {
						current_grp_list_size +=
						    REALLOC_SIZE;
						output->ipmp_list.ipmp_list_val
						    = tmp_realloc;
					}
				}
			}
			if (listp != NULL) {
				free(listp);
				listp = NULL;
			}

			pnm_fini();

			if (error != CL_QUERY_OK) {
				break;
			}

			clconf_iter_advance(node_iter);

		}
	}
	if (node_iter != NULL)
		clconf_iter_release(node_iter);
	if (clconf != NULL)
		clconf_obj_release((clconf_obj_t *)clconf);

	if (pquorum_status != NULL)
		cluster_release_quorum_status(pquorum_status);

	if (error == CL_QUERY_OK) {
		/*
		 * free useless allocated space
		 */
		/*
		 * we reduce the size -> no check
		 */
		output->ipmp_list.ipmp_list_val =
		    (cl_query_ipmp_prop_t *)realloc(output->ipmp_list.
		    ipmp_list_val,
		    (current_grp_idx + 1) * sizeof (cl_query_ipmp_prop_t));
		output->ipmp_list.ipmp_list_len = current_grp_idx;

	}
	if (error != CL_QUERY_OK) {
		output->ipmp_list.ipmp_list_len = current_grp_list_size;
		(void) clquery_free_ipmp_result(output);
		output->ipmp_list.ipmp_list_len = 0;
		output->ipmp_list.ipmp_list_val = NULL;
		if (current_grp_idx == 0) {
			error = CL_QUERY_ENOTCONFIGURED;
		}
	}
	output->error = error;
	(void) pthread_mutex_unlock(&pnm_lib_lock);

	return (TRUE);
}				/*lint !e715 */

static cl_query_error_t
clquery_get_nodeid(const char *node_name, int *id)
{
	clconf_cluster_t *clconf = NULL;
	clconf_iter_t *node_iter = NULL;
	clconf_obj_t *node = NULL;
	const char *n_name = NULL;
	cl_query_error_t res = CL_QUERY_OK;


	if (node_name == NULL) {
		/*
		 * wants local node id
		 */
		if (cladm(CL_CONFIG, CL_NODEID, &id) != 0) {
			res = CL_QUERY_ENOENT;
		}
	} else {
		clconf = clconf_cluster_get_current();
		if (clconf == NULL)
			return (CL_QUERY_EUNEXPECTED);

		node_iter = clconf_cluster_get_nodes(clconf);
		if (node_iter == NULL)
			return (CL_QUERY_EUNEXPECTED);

		while ((node = clconf_iter_get_current(node_iter)) != NULL) {
			n_name = clconf_obj_get_name(node);
			if ((n_name != NULL) &&
			    (strcmp(n_name, node_name) == 0))
				break;

			clconf_iter_advance(node_iter);
		}

		if (node != NULL) {
			*id = clconf_obj_get_id(node);
		} else {
			res = CL_QUERY_ENOENT;
		}
	}

	if (clconf != NULL)
		clconf_obj_release((clconf_obj_t *)clconf);
	if (node_iter != NULL)
		clconf_iter_release(node_iter);

	return (res);
}

#ifdef NEEDED
static cl_query_error_t
clquery_get_nodename(char **node_name, int id)
{
	clconf_cluster_t *clconf = NULL;
	clconf_iter_t *node_iter = NULL;
	clconf_obj_t *node = NULL;
	const char *n_name = NULL;

	cl_query_error_t res = CL_QUERY_OK;



	clconf = clconf_cluster_get_current();
	if (clconf == NULL)
		return (CL_QUERY_EUNEXPECTED);

	node_iter = clconf_cluster_get_nodes(clconf);
	if (node_iter == NULL)
		return (CL_QUERY_EUNEXPECTED);

	while ((node = clconf_iter_get_current(node_iter)) != NULL) {
		if (clconf_obj_get_id(node) == id)
			break;
		clconf_iter_advance(node_iter);
	}

	if (node != NULL) {
		n_name = clconf_obj_get_name(node);
		if (n_name != NULL)
			*node_name = (char *)strdup(n_name);
		if (*node_name == NULL)
			res = CL_QUERY_ENOMEM;
	} else {
		res = CL_QUERY_ENOENT;
	}


	if (node_iter != NULL)
		clconf_iter_release(node_iter);
	if (clconf != NULL)
		clconf_obj_release((clconf_obj_t *)clconf);

	return (res);
}
#endif
/*
 * DESCRIPTION
 *    map the libscdpm status flag on the clquery lib
 *    status flag
 *    if a change is made in libscdpm side
 *    this mapping must be updated (cf libscdpm.h)
 *
 * INPUT:
 *    dest : status flag to update
 *    src  : status flag to map
 *
 * OUTPUT:
 *
 * RETURN:
 *
 * SIDE EFFECTS:
 *    None
 *
 * LOCKS:
 */
static void
disk_path_info_fill_status(int *dest, int src)
{

	*dest = 0;

	if (DPM_PATH_IS_OK(src))
		*dest |= CLQUERY_DISKPATH_OK;
	if (DPM_PATH_IS_FAIL(src))
		*dest |= CLQUERY_DISKPATH_FAIL;
	if (DPM_PATH_IS_INVALID(src))
		*dest |= CLQUERY_DISKPATH_INVALID;
	if (DPM_PATH_IS_UNKNOWN(src))
		*dest |= CLQUERY_DISKPATH_UNKNOWN;
	if (DPM_PATH_IS_REGISTERED(src))
		*dest |= CLQUERY_DISKPATH_REGISTERED;
	if (DPM_PATH_IS_MONITORED(src))
		*dest |= CLQUERY_DISKPATH_MONITORED;
	if (DPM_PATH_IS_UNMONITORED(src))
		*dest |= CLQUERY_DISKPATH_UNMONITORED;
	if (DPM_PATH_IS_DELETED(src))
		*dest |= CLQUERY_DISKPATH_DELETE;


}


#ifdef __DEBUG__
void
print_disk_list(disk_status_node_t *head)
{
	while (head != NULL) {
		printf("disk name = %s\n", head->name);
		head = head->next;
	}
}

#endif

static void
clquery_cleanup_disk_list(disk_status_node_t ** headp, uint_t nodeid)
{
	disk_status_node_t *prev, *cur;
	char *sep;
	int res;


	prev = NULL;
	cur = *headp;


#ifdef __DEBUG__
	printf("node id %d\n", nodeid);
	print_disk_list(*headp);
#endif

	while (cur != NULL) {
		/*
		 * name patten is <id>:<path>
		 */
		/*
		 * check if id equal to 'nodeid'
		 */
		/*
		 * if not remove that from the list
		 */

		sep = strchr(cur->name, ':');
		if (sep != NULL)
			*sep = '\0';
		res = atoi(cur->name);
		if (sep != NULL)
			*sep = ':';

		if ((unsigned int)res != nodeid) {
			/*
			 * need to remove this one
			 */

			/*
			 * move head forward if we need
			 */
			if (cur == *headp) {
				*headp = cur->next;
			} else {
				prev->next = cur->next;
			}

			/*
			 * need to set 'next' to NULL
			 * otherwise 'dpm_disk_status_free' will free
			 * all the list
			 */
			cur->next = NULL;
			dpm_disk_status_free(cur);

			if (prev != NULL) {
				cur = prev->next;
			} else {
				/*
				 * we just removed the first element
				 */
				cur = *headp;
			}

		} else {
			prev = cur;
			cur = cur->next;
		}
	}
#ifdef __DEBUG__
	print_disk_list(*headp);
#endif
}


/*
 * DESCRIPTION
 *    call teh dpm library to get access the diskpath monitoring
 *    status. in case of failure use ccr access to have at least
 *    the list of disk path
 *
 * INPUT:
 *    node_id   : node id on which we want status
 *    disk_list : disk list filled by dpm lib.
 *  no check -> internal use
 * OUTPUT:
 *
 *
 * RETURN:
 *    cl_query_error_t :  CL_QUERY_OK for success
 *
 *
 */

/*
 * ARGSUSED
 */

static cl_query_error_t
clquery_get_monitoring_status(uint_t node_id, disk_status_node_t ** disk_list)
{

	int scdpm_error = DPM_SUCCESS;
	disk_status_node_t *tmp_disk_list = NULL;
	unsigned int dummy;

	*disk_list = NULL;

	scdpm_error = libdpm_get_monitoring_status_all(
	    (nodeid_t)node_id, &tmp_disk_list);

	if (scdpm_error == DPM_SUCCESS) {
		*disk_list = tmp_disk_list;
		return (CL_QUERY_OK);
	}
	if (scdpm_error == DPM_NO_DAEMON) {
		/*
		 * nodes down, use CCR table to know at least the list of
		 * diskpath
		 */

		scdpm_error = dpmccr_ready();
		if (scdpm_error != DPM_SUCCESS) {
			return (CL_QUERY_EUNEXPECTED);
		}
		scdpm_error = dpmccr_read_disks_info(&tmp_disk_list, &dummy);

		if (scdpm_error == DPM_SUCCESS) {

			/*
			 * why we need to clean ?? cf libdpm:monitor.c
			 */
			clquery_cleanup_disk_list(&tmp_disk_list, node_id);

			*disk_list = tmp_disk_list;

			return (CL_QUERY_OK);
		}
	}
	return (CL_QUERY_EUNEXPECTED);

}

static cl_query_error_t
disk_path_info_fill_info(disk_status_node_t *disk_list,
    cl_query_diskpath_prop_t **res,
    size_t *current_idx, size_t *current_size, const char *current_node_name)
{

	disk_status_node_t *disk_list_ptr = NULL;
	cl_query_diskpath_prop_t curprop = NULL;
	cl_query_diskpath_prop_t *tmp_list_realloc = NULL;


	disk_list_ptr = disk_list;

	while (disk_list_ptr) {
		(*res)[*current_idx] = (cl_query_diskpath_prop_t)calloc(1,
		    sizeof (cl_query_diskpath_prop));

		if ((*res)[*current_idx] == NULL) {
			return (CL_QUERY_ENOMEM);
		}
		curprop = (*res)[*current_idx];
		disk_path_info_fill_status(&(curprop->status),
		    disk_list_ptr->status);

		curprop->diskpath_name = (char *)strdup(disk_list_ptr->name);
		if (curprop->diskpath_name == NULL) {
			return (CL_QUERY_ENOMEM);

		}
		curprop->node_name = (char *)strdup(current_node_name);
		if (curprop->node_name == NULL) {
			return (CL_QUERY_ENOMEM);
		}

		(*current_idx)++;

		if ((size_t)*current_idx >= *current_size) {
			/*
			 * not enough size, add space for
			 * 5 nodes
			 */
			*current_size += 100;
			tmp_list_realloc = (cl_query_diskpath_prop_t *)
			    realloc(*res,
				(*current_size) *
				sizeof (cl_query_diskpath_prop_t));

			if (tmp_list_realloc == NULL) {
				*current_size -= 100;
				return (CL_QUERY_ENOMEM);
			}
			*res = tmp_list_realloc;
		}
		disk_list_ptr = disk_list_ptr->next;

	}


	return (CL_QUERY_OK);

}

/*
 * DESCRIPTION
 *    This method is the server part of the DISKPATH_INFO_GET
 *     API query. Information of all the diskpaths in the cluster is
 *     retreived from the scconf library and returned to the user.
 *
 *
 * INPUT:
 *    cl_query_input_t:  Name of the disk path or NULL (default)
 *    struct svc_req  :  RPC request handler
 *
 * OUTPUT:
 *    cl_query_diskpath_output_t: Diskpath information
 *    error is returned in output.error
 *
 * RETURN:
 *    boolean:  TRUE for success
 *              FALSE on any error
 *
 * SIDE EFFECTS:
 *    None
 *
 * LOCKS:
 */

/*
 * ARGSUSED
 */

bool_t
disk_path_info_get_1_svc(cl_query_input_t *input,
    cl_query_diskpath_output_t *output, struct svc_req *rqstp /*lint !e715 */)
{

	cl_query_error_t scdpm_diag = DPM_SUCCESS;
	disk_status_node_t *monitored_disk_list = NULL;
	size_t current = 0;
	size_t current_size = 0;
	cl_query_diskpath_prop_t *tmp_list = NULL;
	cl_query_diskpath_prop_t *tmp_list_realloc = NULL;
	cl_query_error_t error = CL_QUERY_OK;
	int node_id = -1;
	int id = -1;
	const char *current_node_name = NULL;
	char *input_name_to_use = NULL;

	clconf_cluster_t *clconf = NULL;
	clconf_iter_t *node_iter = NULL;
	clconf_obj_t *node = NULL;
	uint_t node_count = 0;


	if ((input == NULL) || (output == NULL))
		return (FALSE);

	if ((input->name == NULL) || (strlen(input->name) == 0)) {
		input_name_to_use = NULL;
	} else {
		input_name_to_use = input->name;
	}

	output->diskpath_list.diskpath_list_len = 0;
	output->diskpath_list.diskpath_list_val = NULL;
	output->error = CL_QUERY_OK;

	error = authenticate_request();
	if (error != CL_QUERY_OK) {
		output->error = error;
		return (TRUE);
	}
	if (error == CL_QUERY_OK) {
		if (input_name_to_use != NULL) {
			error = clquery_get_nodeid(input_name_to_use, &node_id);
			node_count = 1;
		} else {
			/*
			 * want all nodes
			 */
			node_id = -1;
		}
	}
	if (error == CL_QUERY_OK) {
		clconf = clconf_cluster_get_current();
		if (clconf == NULL) {
			error = CL_QUERY_EUNEXPECTED;

		}
	}
	if (error == CL_QUERY_OK) {
		node_iter = clconf_cluster_get_nodes(clconf);
		if (node_iter == NULL) {
			error = CL_QUERY_EUNEXPECTED;
		}
	}
	if ((error == CL_QUERY_OK) && (input_name_to_use == NULL)) {
		node_count = (uint_t)clconf_iter_get_count(node_iter);
	}
	/*
	 *  no simple/safe way to retrieve snapshot of the current config
	 *  in on shot. use the worst case and free/resize later
	 *  default size => 20 per node
	 */

	if (error == CL_QUERY_OK) {

		current_size = node_count * 20;

		tmp_list = (cl_query_diskpath_prop_t *)calloc(current_size,
		    sizeof (cl_query_diskpath_prop_t));

		if (tmp_list == NULL) {
			output->error = CL_QUERY_ENOMEM;
			if (node_iter != NULL)
				clconf_iter_release(node_iter);
			if (clconf != NULL)
				clconf_obj_release((clconf_obj_t *)clconf);
			return (TRUE);
		}
	}
	current = 0;

	if (error == CL_QUERY_OK) {

		while (((node = clconf_iter_get_current(node_iter)) != NULL) &&
		    (error == CL_QUERY_OK)) {



			id = clconf_obj_get_id(node);


			if ((node_id != -1) && (node_id != id)) {
				/*
				 * user specified a node and it is not
				 * this one
				 */
				clconf_iter_advance(node_iter);
				continue;
			}
			scdpm_diag = clquery_get_monitoring_status(
				(nodeid_t)id, &monitored_disk_list);

			if ((scdpm_diag != CL_QUERY_OK) ||
			    (monitored_disk_list == NULL)) {
				clconf_iter_advance(node_iter);
				continue;
			}

			current_node_name = clconf_obj_get_name(node);


			if (current_node_name == NULL) {
				error = CL_QUERY_EUNEXPECTED;
				break;
			}

			error = disk_path_info_fill_info(monitored_disk_list,
			    &tmp_list,
			    &current, &current_size, current_node_name);

			if (error != CL_QUERY_OK) {
				break;
			}


			clconf_iter_advance(node_iter);

			if ((scdpm_diag == CL_QUERY_OK) &&
			    (monitored_disk_list != NULL)) {
				dpm_free_status_list(monitored_disk_list);
				monitored_disk_list = NULL;
			}


		}

		if (node_iter != NULL)
			clconf_iter_release(node_iter);

		if (error != CL_QUERY_OK) {
			if ((scdpm_diag == CL_QUERY_OK) &&
			    (monitored_disk_list != NULL)) {
				/*
				 * we may exist from the loops abnormaly
				 */
				dpm_free_status_list(monitored_disk_list);
			}
			(void) clquery_free_diskpath_result_list(&tmp_list,
			    (int)current_size);
			tmp_list = NULL;
			current = 0;
		} else {
			if (current < (current_size - 1)) {
				/*
				 * space not used, free it
				 */
				/*lint -e807 */
				tmp_list_realloc = (cl_query_diskpath_prop_t *)
				    realloc(tmp_list,
					(size_t)((current + 1) * /*lint !e807 */
					sizeof (cl_query_diskpath_prop_t)));
				if (tmp_list_realloc == NULL) {
					error = CL_QUERY_ENOMEM;
				}
				/*lint +e807 */
				tmp_list = tmp_list_realloc;
			}
		}
	}
	if (clconf != NULL)
		clconf_obj_release((clconf_obj_t *)clconf);

	if ((scdpm_diag == CL_QUERY_OK) && (monitored_disk_list != NULL))
		dpm_free_status_list(monitored_disk_list);

	output->diskpath_list.diskpath_list_len = current;
	output->diskpath_list.diskpath_list_val = tmp_list;



	return (TRUE);
}				/*lint !e715 */
