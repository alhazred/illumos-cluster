/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 **************************************************************************
 *
 * Component = clquery deamon
 *
 * Synopsis  = CCR Access RPC server code
 *
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 **************************************************************************
 */

#pragma ident  "@(#)rpc_server.c 1.4 08/05/20 SMI"
/*lint -e322 -e7 -e793 generated file */
#include "cl_query.h"
/*lint +e322 +e7 +e793 */

#include <stdio.h>
#include <signal.h>
#include <rpc/pmap_clnt.h>
#include <unistd.h>
#include <sys/types.h>
#include <memory.h>
#include <stropts.h>
#include <sys/resource.h>


#include "rpc_queue.h"


/*
 * DESCRIPTION:
 *    Dispatch routine for the RPC request.
 *
 *
 */
void
clquery_server_1(struct svc_req *rqstp, register SVCXPRT * transp)
{
	xdrproc_t _xdr_argument, _xdr_result;
	bool_t(*rpc_func_ptr) (char *, void *, struct svc_req *);


	switch (rqstp->rq_proc) {
	case NULLPROC:
		(void) svc_sendreply(
			transp,
			    (xdrproc_t)xdr_void,
			    (char *)NULL);
		return;

	case ADAPTER_INFO_GET:
		_xdr_argument = (xdrproc_t)xdr_cl_query_input_t;
		_xdr_result = (xdrproc_t)xdr_cl_query_adapter_output_t;
		rpc_func_ptr = (bool_t(*)(char *, void *,
			struct svc_req *))adapter_info_get_1_svc;
		break;

	case NODE_INFO_GET:
		_xdr_argument = (xdrproc_t)xdr_cl_query_input_t;
		_xdr_result = (xdrproc_t)xdr_cl_query_node_output_t;
		rpc_func_ptr = (bool_t(*)(char *, void *,
			struct svc_req *))node_info_get_1_svc;
		break;

	case QUORUM_INFO_GET:
		_xdr_argument = (xdrproc_t)xdr_cl_query_input_t;
		_xdr_result = (xdrproc_t)xdr_cl_query_quorum_output_t;
		rpc_func_ptr = (bool_t(*)(char *, void *,
			struct svc_req *))quorum_info_get_1_svc;
		break;

	case CABLE_INFO_GET:
		_xdr_argument = (xdrproc_t)xdr_cl_query_input_t;
		_xdr_result = (xdrproc_t)xdr_cl_query_cable_output_t;
		rpc_func_ptr = (bool_t(*)(char *, void *,
			struct svc_req *))cable_info_get_1_svc;
		break;

	case JUNCTION_INFO_GET:
		_xdr_argument = (xdrproc_t)xdr_cl_query_input_t;
		_xdr_result = (xdrproc_t)xdr_cl_query_junction_output_t;
		rpc_func_ptr = (bool_t(*)(char *, void *,
			struct svc_req *))junction_info_get_1_svc;
		break;

	case DEVICE_INFO_GET:
		_xdr_argument = (xdrproc_t)xdr_cl_query_input_t;
		_xdr_result = (xdrproc_t)xdr_cl_query_device_output_t;
		rpc_func_ptr = (bool_t(*)(char *, void *,
			struct svc_req *))device_info_get_1_svc;
		break;

	case IPMP_INFO_GET:
		_xdr_argument = (xdrproc_t)xdr_cl_query_input_t;
		_xdr_result = (xdrproc_t)xdr_cl_query_ipmp_output_t;
		rpc_func_ptr = (bool_t(*)(char *, void *,
			struct svc_req *))ipmp_info_get_1_svc;
		break;

	case DISK_PATH_INFO_GET:
		_xdr_argument = (xdrproc_t)xdr_cl_query_input_t;
		_xdr_result = (xdrproc_t)xdr_cl_query_diskpath_output_t;
		rpc_func_ptr = (bool_t(*)(char *, void *,
			struct svc_req *))disk_path_info_get_1_svc;
		break;
	case CLUSTER_INFO_GET:
		_xdr_argument = (xdrproc_t)xdr_cl_query_input_t;
		_xdr_result = (xdrproc_t)xdr_cl_query_cluster_output_t;
		rpc_func_ptr = (bool_t(*)(char *, void *,
			struct svc_req *))cluster_info_get_1_svc;
		break;
	case TRANSPORT_PATH_INFO_GET:
		_xdr_argument = (xdrproc_t)xdr_cl_query_input_t;
		_xdr_result = (xdrproc_t)xdr_cl_query_transportpath_output_t;
		rpc_func_ptr = (bool_t(*)(char *, void *,
			    struct svc_req *))transport_path_info_get_1_svc;
		break;
	case TABLE_NAMES_GET:
		_xdr_argument = (xdrproc_t)xdr_cl_query_input_t;
		_xdr_result = (xdrproc_t)xdr_ccr_table_list_t;
		rpc_func_ptr = (bool_t(*)(char *, void *,
			struct svc_req *))table_names_get_1_svc;
		break;

	case TABLE_LEN_GET:
		_xdr_argument = (xdrproc_t)xdr_cl_query_input_t;
		_xdr_result = (xdrproc_t)xdr_ccr_table_size_t;
		rpc_func_ptr = (bool_t(*)(char *, void *,
			struct svc_req *))table_len_get_1_svc;
		break;

	case READ_TABLE:
		_xdr_argument = (xdrproc_t)xdr_cl_query_input_t;
		_xdr_result = (xdrproc_t)xdr_cl_query_ccr_param_t;
		rpc_func_ptr = (bool_t(*)(char *, void *,
			struct svc_req *))read_table_1_svc;
		break;

	case CREATE_TABLE:
		_xdr_argument = (xdrproc_t)xdr_cl_query_ccr_param_t;
		_xdr_result = (xdrproc_t)xdr_cl_query_output_t;
		rpc_func_ptr = (bool_t(*)(char *, void *,
			struct svc_req *))create_table_1_svc;
		break;

	case DELETE_TABLE:
		_xdr_argument = (xdrproc_t)xdr_cl_query_input_t;
		_xdr_result = (xdrproc_t)xdr_cl_query_output_t;
		rpc_func_ptr = (bool_t(*)(char *, void *,
			struct svc_req *))delete_table_1_svc;
		break;

	case DELETE_TABLE_ALL:
		_xdr_argument = (xdrproc_t)xdr_cl_query_input_t;
		_xdr_result = (xdrproc_t)xdr_cl_query_output_t;
		rpc_func_ptr = (bool_t(*)(char *, void *,
			struct svc_req *))delete_table_all_1_svc;
		break;

	case WRITE_TABLE_ALL:
		_xdr_argument = (xdrproc_t)xdr_cl_query_ccr_param_t;
		_xdr_result = (xdrproc_t)xdr_cl_query_output_t;
		rpc_func_ptr = (bool_t(*)(char *, void *,
			struct svc_req *))write_table_all_1_svc;
		break;

	default:
		svcerr_noproc(transp);
		return;
	}

	/*
	 * attach a destroy handler before executing client request
	 */
	if (!svc_control(transp,
		SVCSET_RECVERRHANDLER,
		(void *)clquery_close_handler)) { /*lint !e611 */
		/*
		 * cannot not attach handler, not safe -> reject that one
		 */
		(void) svc_sendreply(transp,
		    (xdrproc_t)xdr_void,
		    (char *)NULL);
		return;
	}
	/*
	 * Put the request in the rpc queue
	 */
	(void) rpcq_put(transp, _xdr_argument, _xdr_result, rpc_func_ptr);

	return;

}
