/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_CL_QUERY_CCR_ACCESS_H
#define	_CL_QUERY_CCR_ACCESS_H

#pragma ident	"@(#)cl_query_ccr_access.h	1.3	08/05/20 SMI"

#ifdef  __cplusplus
extern "C" {
#endif

#include <stdlib.h>
#include <limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <strings.h>

/*lint -e793 generated file */
#include "cl_query.h"
/*lint +e793 */

#include <cl_query/cl_query_types.h>


extern cl_query_error_t table_names_get(char *, uint_t *, char ***);
extern cl_query_error_t create_table(cl_query_ccr_param_t *);
extern cl_query_error_t read_table(char *, cl_query_ccr_param_t *);
extern cl_query_error_t table_len_get(char  *, uint_t  *);
extern cl_query_error_t delete_table(char  *);
extern cl_query_error_t write_table_all(cl_query_ccr_param_t  *);
extern cl_query_error_t delete_table_all(char *);


#ifdef  __cplusplus
}
#endif

#endif /* !_CL_QUERY_CCR_ACCESS_H */
