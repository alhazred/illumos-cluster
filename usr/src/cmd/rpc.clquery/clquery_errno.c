/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 **************************************************************************
 *
 * Component = clquery deamon
 *
 * Synopsis  = Error and Type Coversion routines
 *
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 **************************************************************************
 */

#include <stdio.h>
#include <libscdpm.h>
#include <scadmin/scconf.h>
#include <scadmin/scstat.h>
#include <cl_query/cl_query_types.h>

#pragma ident	"@(#)clquery_errno.c	1.3	08/05/20 SMI"


/*
 * DESCRIPTION:
 *    This method converts the errno returned from the pthread
 *    library to the clquery error code
 *
 * INPUT:
 *    errno: error code from pthread library
 *
 * OUTPUT:
 *    None
 *
 * RETURN:
 *    cl_query_error_t: clquery library error code
 *
 * SIDE EFFECTS:
 *    None
 *
 * LOCKS:
 */

cl_query_error_t
pthread_to_clquery_err(int err)
{
	switch (err) {
	case 0:
		return (CL_QUERY_OK);

	case EPERM:
		return (CL_QUERY_EPERM);

	case EINVAL:
		return (CL_QUERY_EINVAL);

	case ENOMEM:
		return (CL_QUERY_ENOMEM);

	case ETIMEDOUT:
		return (CL_QUERY_ETIMEDOUT);

	default:
		return (CL_QUERY_EUNEXPECTED);
	}
}

/*
 * DESCRIPTION:
 *    This method converts the errno returned from the scconf
 *    library to the clquery error code
 *
 * INPUT:
 *    scconf_errno_t: error code of scconf library
 *
 * OUTPUT:
 *    None
 *
 * RETURN:
 *    cl_query_error_t: clquery library error code
 *
 * SIDE EFFECTS:
 *    None
 *
 * LOCKS:
 */

cl_query_error_t
scconf_to_clquery(scconf_errno_t err)
{
	switch (err) {
	case SCCONF_NOERR:
		return (CL_QUERY_OK);

	case SCCONF_EPERM:
		return (CL_QUERY_EPERM);

	case SCCONF_ENOMEM:
		return (CL_QUERY_ENOMEM);

	case SCCONF_EINVAL:
		return (CL_QUERY_EINVAL);

	case SCCONF_ETIMEDOUT:
		return (CL_QUERY_ETIMEDOUT);

	case SCCONF_EUNEXPECTED:
		return (CL_QUERY_EUNEXPECTED);

	case SCCONF_EAUTH:
		return (CL_QUERY_EAUTH);

	case SCCONF_EIO:
		return (CL_QUERY_EIO);

	case SCCONF_ENOEXIST:
		return (CL_QUERY_ENOTCONFIGURED);

	case SCCONF_ESTALE:
		return (CL_QUERY_EOBSOLETE);

	case SCCONF_ENOCLUSTER:
	case SCCONF_EINSTALLMODE:
		return (CL_QUERY_ECLUSTERRECONFIG);

	case SCCONF_DS_ENODEINVAL:
		return (CL_QUERY_ENOTCLUSTER);

	default:
		return (CL_QUERY_EUNEXPECTED);
	}
}


/*
 * DESCRIPTION:
 *    This method converts the errno returned from the scstat
 *    library to the clquery error code
 *
 * INPUT:
 *    scstat_errno_t: error code of scstat library
 *
 * OUTPUT:
 *    None
 *
 * RETURN:
 *    cl_query_error_t: clquery library error code
 *
 * SIDE EFFECTS:
 *    None
 *
 * LOCKS:
 */

cl_query_error_t
scstat_to_clquery(scstat_errno_t err)
{
	switch (err) {
	case SCSTAT_ENOERR:
		return (CL_QUERY_OK);

	case SCSTAT_EPERM:
		return (CL_QUERY_EPERM);

	case SCSTAT_ENOMEM:
		return (CL_QUERY_ENOMEM);

	case SCSTAT_ENOTCLUSTER:
		return (CL_QUERY_ENOTCLUSTER);

	case SCSTAT_EINVAL:
		return (CL_QUERY_EINVAL);

	case SCSTAT_EUNEXPECTED:
		return (CL_QUERY_EUNEXPECTED);

	case SCSTAT_ENOTCONFIGURED:
		return (CL_QUERY_ENOTCONFIGURED);

	case SCSTAT_EOBSOLETE:
		return (CL_QUERY_EOBSOLETE);

	case SCSTAT_ERGRECONFIG:
		return (CL_QUERY_ERGRECONFIG);

	case SCSTAT_ECLUSTERRECONFIG:
		return (CL_QUERY_ECLUSTERRECONFIG);

	default:
		return (CL_QUERY_EUNEXPECTED);
	}
}

/*
 * DESCRIPTION:
 *    This method converts the state code returned from the
 *    scconf library to the clquery status code
 *
 * INPUT:
 *    scconf_state_t: state code of scconf library
 *
 * OUTPUT:
 *    None
 *
 * RETURN:
 *    cl_query_state_t: clquery library status code
 *
 * SIDE EFFECTS:
 *    None
 *
 * LOCKS:
 */

cl_query_state_t
convert_scconf_status(scconf_state_t state)
{
	switch (state) {
	case SCCONF_STATE_UNCHANGED:
		return (CL_QUERY_UNCHANGED);

	case SCCONF_STATE_ENABLED:
		return (CL_QUERY_ENABLED);

	case SCCONF_STATE_DISABLED:
		return (CL_QUERY_DISABLED);

	default:
		return (CL_QUERY_FAULTED);
	}
}

/*
 * DESCRIPTION:
 *    This method converts the node state code returned from
 *    the scstat library to the clquery status code
 *
 * INPUT:
 *    scstat_node_pref_t: node state code of scstat library
 *
 * OUTPUT:
 *    None
 *
 * RETURN:
 *    cl_query_state_t: clquery library status code
 *
 * SIDE EFFECTS:
 *    None
 *
 * LOCKS:
 */

cl_query_state_t
convert_scstat_node_status(scstat_node_pref_t state)
{
	switch (state) {
	case SCSTAT_PRIMARY:
		return (CL_QUERY_PRIMARY);

	case SCSTAT_SECONDARY:
		return (CL_QUERY_SECONDARY);

	case SCSTAT_SPARE:
		return (CL_QUERY_SPARE);

	case SCSTAT_INACTIVE:
		return (CL_QUERY_INACTIVE);

	case SCSTAT_TRANSITION:
		return (CL_QUERY_TRANSITION);

	default:
		return (CL_QUERY_INVALID);
	}
}

/*
 * DESCRIPTION:
 *    This method converts the state code returned from
 *    the scstat library to the clquery status code
 *
 * INPUT:
 *    scstat_state_code_t: state code of scstat library
 *
 * OUTPUT:
 *    None
 *
 * RETURN:
 *    cl_query_state_t: clquery library status code
 *
 * SIDE EFFECTS:
 *    None
 *
 * LOCKS:
 */

cl_query_state_t
convert_scstat_status(scstat_state_code_t state)
{
	switch (state) {
	case SCSTAT_ONLINE:
		return (CL_QUERY_ONLINE);

	case SCSTAT_OFFLINE:
		return (CL_QUERY_OFFLINE);

	case SCSTAT_FAULTED:
		return (CL_QUERY_FAULTED);

	case SCSTAT_DEGRADED:
		return (CL_QUERY_DEGRADED);

	case SCSTAT_WAIT:
		return (CL_QUERY_WAIT);

	case SCSTAT_UNKNOWN:
		return (CL_QUERY_UNKNOWN);

	case SCSTAT_NOTMONITORED:
		return (CL_QUERY_NOTMONITORED);

	default:
		return (CL_QUERY_INVALID);
	}
}

/*
 * DESCRIPTION:
 *    This method converts the controller endpoint state returned
 *    from the scconf library to the clquery endpoint state
 *
 * INPUT:
 *    scconf_cltr_epoint_type_t: endpoint code in scconf library
 *
 * OUTPUT:
 *    None
 *
 * RETURN:
 *    cl_query_epoint_type_t: endpoint type in clquery library
 *
 * SIDE EFFECTS:
 *    None
 *
 * LOCKS:
 */

cl_query_epoint_type_t
convert_scconf_epoint(scconf_cltr_epoint_type_t epoint)
{
	switch (epoint) {
	case SCCONF_CLTR_EPOINT_TYPE_ADAPTER:
		return (CL_QUERY_EPOINT_TYPE_ADAPTER);

	case SCCONF_CLTR_EPOINT_TYPE_CPOINT:
		return (CL_QUERY_EPOINT_TYPE_CPOINT);

	default:
		return (CL_QUERY_EPOINT_TYPE_INVALID);
	}
}

cl_query_state_t
convert_ipmp_group_status(int state)
{
	switch (state) {
	case PNM_STAT_OK:
		return (CL_QUERY_ONLINE);
	case PNM_STAT_DOWN:
		return (CL_QUERY_OFFLINE);
	default:
		return (CL_QUERY_UNKNOWN);
	}
}

cl_query_error_t
scdpm_to_clquery(int err)
{
	switch (err) {
	case DPM_SUCCESS:
		return (CL_QUERY_OK);
	case DPM_ENOMEM:
	case DPM_OUT_OF_MEMORY:
		return (CL_QUERY_ENOMEM);
	case DPM_NO_DAEMON:
	case DPM_DAEMON_NOT_FOUND:
	case DPM_ERR_COMM:
		return (CL_QUERY_ECOMM);
	case DPM_ENOENT:
		return (CL_QUERY_ENOTCONFIGURED);
	case DPM_UNKNOWN_ERROR:
	case DPM_EEXIST:
	default:
		return (CL_QUERY_EUNEXPECTED);
	}
}

cl_query_state_t
sccconf_port_status_to_clquery(scconf_cltr_portstate_t state)
{
	switch (state) {
	case SCCONF_STATE_UNCHANGED:
		return (CL_QUERY_UNCHANGED);
	case SCCONF_STATE_ENABLED:
		return (CL_QUERY_ENABLED);
	case SCCONF_STATE_DISABLED:
		return (CL_QUERY_DISABLED);
	default:
		return (CL_QUERY_UNKNOWN);
	}
}

cl_query_auth_type_t
sccconf_to_clquery_auth_type(scconf_authtype_t type)
{
	switch (type) {
	case SCCONF_AUTH_UNIX:
		return (CLQUERY_AUTH_UNIX);
	case SCCONF_AUTH_DES:
		return (CLQUERY_AUTH_DES);
	default:
		return (CLQUERY_AUTH_UNKNOWN);
	}
}
cl_query_state_t
scstate_to_clquery_state(sc_state_code_t state)
{

	switch (state) {
	case SC_STATE_ONLINE:
		return (CL_QUERY_ONLINE);
	case SC_STATE_OFFLINE:
		return (CL_QUERY_OFFLINE);
	case SC_STATE_FAULTED:
		return (CL_QUERY_FAULTED);
	case SC_STATE_DEGRADED:
		return (CL_QUERY_DEGRADED);
	case SC_STATE_WAIT:
		return (CL_QUERY_WAIT);
	case SC_STATE_NOT_MONITORED:
		return (CL_QUERY_NOTMONITORED);
	case SC_STATE_UNKNOWN:
	default:
		return (CL_QUERY_UNKNOWN);
	}

}
