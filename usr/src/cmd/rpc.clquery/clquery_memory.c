/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 **************************************************************************
 *
 * Component = clquery deamon
 *
 * Synopsis  = clquery server memory management routines
 *
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 ****************************************** *******************************
 */

#pragma ident "@(#)clquery_memory.c 1.6 08/05/20 SMI"

#include <stdlib.h>

#include <scadmin/scstat.h>
#include <scadmin/scconf.h>

#include <cl_query/rpc/cl_query.h>
#include <cl_query/cl_query_types.h>

static cl_query_error_t free_strarr(uint_t, strarr *);
static cl_query_error_t free_port_prop_list(cl_query_port_prop_list_t *);
static cl_query_error_t free_disk_path_list(cl_query_didpath_list_t *);


cl_query_error_t
clquery_free_cluster_result(cl_query_cluster_output_t *out)
{

	cl_query_cluster_prop_t tmp_out;


	if (out == NULL)
		return (CL_QUERY_EINVAL);

	tmp_out = out->current_cluster;

	if (tmp_out == NULL)
		return (CL_QUERY_EINVAL);

	if (tmp_out->cluster_name != NULL) {
		free(tmp_out->cluster_name);
		tmp_out->cluster_name = NULL;
	}
	if (tmp_out->cluster_id != NULL) {
		free(tmp_out->cluster_id);
		tmp_out->cluster_id = NULL;
	}

	if (tmp_out->cluster_version != NULL) {
		free(tmp_out->cluster_version);
		tmp_out->cluster_version = NULL;
	}

	if ((tmp_out->cluster_join_list.strarr_list_len > 0) &&
	    (tmp_out->cluster_join_list.strarr_list_val != NULL)) {
		(void) free_strarr(tmp_out->cluster_join_list.strarr_list_len,
		    tmp_out->cluster_join_list.strarr_list_val);
		tmp_out->cluster_join_list.strarr_list_val = NULL;
		tmp_out->cluster_join_list.strarr_list_len = 0;
	}


	/*
	 * in that case static variable : don;t free it
	 */
	/*
	 * free(out);
	 */


	return (CL_QUERY_OK);

}

/*
 * DESCRIPTION:
 *    This method is frees the memory allocated for the node
 *    query request.
 *
 *
 * INPUT:
 *    cl_query_node_output_t:   node query results.
 *
 * OUTPUT:
 *    None.
 *
 * RETURN:
 *    CL_QUERY_OK:  On success
 *    CL_QUERY_EXX: On failure
 *
 * SIDE EFFECTS:
 *    None
 *
 * LOCKS:
 */

cl_query_error_t
clquery_free_node_result(cl_query_node_output_t *node)
{
	cl_query_error_t error = CL_QUERY_OK;
	cl_query_node_prop_t node_list = NULL;
	uint_t i = 0, tmp_count = 0;
	strarr *tmp_ptr = NULL;


	if (node == NULL) {
		error = CL_QUERY_EINVAL;
	}

	/*
	 * If there are no entries return
	 */
	if (node->node_list.node_list_len == 0 ||
	    node->node_list.node_list_val == NULL) {
		return (CL_QUERY_OK);
	}

	/*
	 * Iterate throughthe list and free any allocated memory
	 */
	for (i = 0; i < node->node_list.node_list_len; i++) {
		node_list = node->node_list.node_list_val[i];

		/*
		 * Check for null pointer
		 */
		if (node_list == NULL) {
			continue;
		}

		/*
		 * Free node name pointer
		 */
		if (node_list->node_name != NULL) {
			free(node_list->node_name);
			node_list->node_name = NULL;
		}

		/*
		 * Free private host name
		 */
		if (node_list->private_host_name != NULL) {
			free(node_list->private_host_name);
			node_list->private_host_name = NULL;
		}

		if (node_list->reservation_key != NULL) {
			free(node_list->reservation_key);
			node_list->reservation_key = NULL;
		}

		/*
		 * Free the list of private adapters
		 */
		tmp_count = node_list->private_adapters.strarr_list_len;
		tmp_ptr = node_list->private_adapters.strarr_list_val;

		error = free_strarr(tmp_count, tmp_ptr);

		/*
		 * Free the list of public adapters
		 */
		tmp_count = node_list->public_adapters.strarr_list_len;
		tmp_ptr = node_list->public_adapters.strarr_list_val;

		error = free_strarr(tmp_count, tmp_ptr);

		free(node_list);
		node_list = NULL;
	}

	if (node->node_list.node_list_val != NULL) {
		free(node->node_list.node_list_val);
		node->node_list.node_list_val = NULL;
	}

	/*
	 * Initialialize to zero
	 */
	node->node_list.node_list_len = 0;
	node->node_list.node_list_val = NULL;

	return (error);
}


/*
 * DESCRIPTION:
 *    This method is frees the memory allocated for the adapter
 *    query request.
 *
 *
 * INPUT:
 *    cl_query_adapter_output_t:   adapter query results.
 *
 * OUTPUT:
 *    None.
 *
 * RETURN:
 *    CL_QUERY_OK:  On success
 *    CL_QUERY_EXX: On failure
 *
 * SIDE EFFECTS:
 *    None
 *
 * LOCKS:
 */

cl_query_error_t
clquery_free_adapter_result(cl_query_adapter_output_t *adapter)
{

	cl_query_error_t error = CL_QUERY_OK;
	cl_query_adapter_prop_t adapter_list = NULL;
	uint_t i = 0;


	if (adapter == NULL) {
		return (CL_QUERY_EINVAL);
	}

	/*
	 * If there are no entries return
	 */
	if (adapter->adapter_list.adapter_list_len == 0 ||
	    adapter->adapter_list.adapter_list_val == NULL) {
		return (error);
	}

	for (i = 0; i < adapter->adapter_list.adapter_list_len; i++) {
		adapter_list = adapter->adapter_list.adapter_list_val[i];

		if (adapter_list == NULL) {
			continue;
		}

		/*
		 * free adapter name pointer
		 */
		if (adapter_list->adapter_name != NULL) {
			free(adapter_list->adapter_name);
			adapter_list->adapter_name = NULL;
		}

		/*
		 * free device name pointer
		 */
		if (adapter_list->device_name != NULL) {
			free(adapter_list->device_name);
			adapter_list->device_name = NULL;
		}

		/*
		 * free ip addr pointer
		 */
		if (adapter_list->ip_addr != NULL) {
			free(adapter_list->ip_addr);
			adapter_list->ip_addr = NULL;
		}

		/*
		 * free netmask pointer
		 */
		if (adapter_list->netmask != NULL) {
			free(adapter_list->netmask);
			adapter_list->netmask = NULL;
		}

		/*
		 * free node name pointer
		 */
		if (adapter_list->node_name != NULL) {
			free(adapter_list->node_name);
			adapter_list->node_name = NULL;
		}

		/*
		 * free type pointer
		 */
		if (adapter_list->type != NULL) {
			free(adapter_list->type);
			adapter_list->type = NULL;
		}

		(void) free_port_prop_list(&(adapter_list->port_list));

		free(adapter_list);
		adapter_list = NULL;
	}


	adapter->adapter_list.adapter_list_len = 0;
	free(adapter->adapter_list.adapter_list_val);
	adapter->adapter_list.adapter_list_val = NULL;

	return (CL_QUERY_OK);
}

/*
 * DESCRIPTION:
 *    This method is frees the memory allocated for the junction
 *    query request.
 *
 *
 * INPUT:
 *    cl_query_junction_output_t:   junction query results.
 *
 * OUTPUT:
 *    None.
 *
 * RETURN:
 *    CL_QUERY_OK:  On success
 *    CL_QUERY_EXX: On failure
 *
 * SIDE EFFECTS:
 *    None
 *
 * LOCKS:
 */

cl_query_error_t
clquery_free_junction_result(cl_query_junction_output_t *junction)
{

	cl_query_error_t error = CL_QUERY_OK;
	cl_query_junction_prop_t junction_list = NULL;
	uint_t i = 0;


	if (junction == NULL) {
		return (CL_QUERY_EINVAL);
	}

	/*
	 * If there are no entries return
	 */
	if (junction->junction_list.junction_list_len == 0 ||
	    junction->junction_list.junction_list_val == NULL) {
		return (error);
	}

	for (i = 0; i < junction->junction_list.junction_list_len; i++) {
		junction_list = junction->junction_list.junction_list_val[i];

		if (junction_list == NULL) {
			continue;
		}

		/*
		 * free junction name pointer
		 */
		if (junction_list->junction_name != NULL) {
			free(junction_list->junction_name);
			junction_list->junction_name = NULL;
		}

		/*
		 * free junction type pointer
		 */
		if (junction_list->junction_type != NULL) {
			free(junction_list->junction_type);
			junction_list->junction_type = NULL;
		}

		/*
		 * free key name pointer
		 */
		if (junction_list->key_name != NULL) {
			free(junction_list->key_name);
			junction_list->key_name = NULL;
		}

		/*
		 * Free the port list
		 */
		(void) free_port_prop_list(&(junction_list->port_list));


		free(junction_list);
		junction_list = NULL;
	}

	junction->junction_list.junction_list_len = 0;
	free(junction->junction_list.junction_list_val);
	junction->junction_list.junction_list_val = NULL;

	return (CL_QUERY_OK);
}


/*
 * DESCRIPTION:
 *    This method is frees the memory allocated for the cable
 *    query request.
 *
 *
 * INPUT:
 *    cl_query_cable_output_t:   cable query results.
 *
 * OUTPUT:
 *    None.
 *
 * RETURN:
 *    CL_QUERY_OK:  On success
 *    CL_QUERY_EXX: On failure
 *
 * SIDE EFFECTS:
 *    None
 *
 * LOCKS:
 */

cl_query_error_t
clquery_free_cable_result(cl_query_cable_output_t *cable)
{

	cl_query_error_t error = CL_QUERY_OK;
	cl_query_cable_prop_t cable_list = NULL;
	uint_t i = 0;


	if (cable == NULL) {
		return (CL_QUERY_EINVAL);
	}

	/*
	 * If there are no entries return
	 */
	if ((cable->cable_list.cable_list_len == 0) ||
	    (cable->cable_list.cable_list_val == NULL)) {
		return (error);
	}

	for (i = 0; i < cable->cable_list.cable_list_len; i++) {

		cable_list = cable->cable_list.cable_list_val[i];

		if (cable_list != NULL) {
			/*
			 * free cable name pointer
			 */
			if (cable_list->cable_name != NULL)
				free(cable_list->cable_name);

			/*
			 * Free cable endpoint 1
			 */
			if (cable_list->cable_epoint1_name != NULL)
				free(cable_list->cable_epoint1_name);

			/*
			 * Free cable endpoint 2
			 */
			if (cable_list->cable_epoint2_name != NULL)
				free(cable_list->cable_epoint2_name);

			free(cable_list);

		}		/* if (cable_list != NULL) */
	}

	cable->cable_list.cable_list_len = 0;
	free(cable->cable_list.cable_list_val);
	cable->cable_list.cable_list_val = NULL;

	return (CL_QUERY_OK);
}


/*
 * DESCRIPTION:
 *    This method is frees the memory allocated for the quorum
 *    query request.
 *
 *
 * INPUT:
 *    cl_query_quorum_output_t:   quorum query results.
 *
 * OUTPUT:
 *    None.
 *
 * RETURN:
 *    CL_QUERY_OK:  On success
 *    CL_QUERY_EXX: On failure
 *
 * SIDE EFFECTS:
 *    None
 *
 * LOCKS:
 */

cl_query_error_t
clquery_free_quorum_result(cl_query_quorum_output_t *quorum)
{

	cl_query_error_t error = CL_QUERY_OK;
	cl_query_quorum_prop_t quorum_ptr = NULL;
	uint_t i = 0, tmp_count = 0;
	strarr *tmp_ptr = NULL;


	if (quorum == NULL) {
		return (CL_QUERY_EINVAL);
	}

	/*
	 * If there are no entries return
	 */
	if (quorum->quorum_list.quorum_list_len == 0 ||
	    quorum->quorum_list.quorum_list_val == NULL) {
		return (error);
	}

	for (i = 0; i < quorum->quorum_list.quorum_list_len; i++) {
		quorum_ptr = quorum->quorum_list.quorum_list_val[i];
		if (quorum_ptr == NULL) {
			continue;
		}

		/*
		 * free the quorum name pointer
		 */
		if (quorum_ptr->device_name != NULL) {
			free(quorum_ptr->device_name);
			quorum_ptr->device_name = NULL;
		}

		/*
		 * free the quorum key name pointer
		 */
		if (quorum_ptr->key_name != NULL) {
			free(quorum_ptr->key_name);
			quorum_ptr->key_name = NULL;
		}

		/*
		 * free the access mode pointer
		 */
		if (quorum_ptr->access_mode != NULL) {
			free(quorum_ptr->access_mode);
			quorum_ptr->access_mode = NULL;
		}

		/*
		 * free the device type pointer
		 */
		if (quorum_ptr->device_type != NULL) {
			free(quorum_ptr->device_type);
			quorum_ptr->device_type = NULL;
		}

		/*
		 * free the device filer name
		 */
		if (quorum_ptr->filer_name != NULL) {
			free(quorum_ptr->filer_name);
			quorum_ptr->filer_name = NULL;
		}

		/*
		 * free the device filer lun id
		 */
		if (quorum_ptr->lunid != NULL) {
			free(quorum_ptr->lunid);
			quorum_ptr->lunid = NULL;
		}

		/*
		 * free the device filer lun name
		 */
		if (quorum_ptr->lun_name != NULL) {
			free(quorum_ptr->lun_name);
			quorum_ptr->lun_name = NULL;
		}

		/*
		 * free the list of enabled nodes
		 */
		tmp_count = quorum_ptr->enabled_nodes.strarr_list_len;
		tmp_ptr = quorum_ptr->enabled_nodes.strarr_list_val;

		error = free_strarr(tmp_count, tmp_ptr);

		/*
		 * free the list of disabled nodes
		 */
		tmp_count = quorum_ptr->disabled_nodes.strarr_list_len;
		tmp_ptr = quorum_ptr->disabled_nodes.strarr_list_val;

		error = free_strarr(tmp_count, tmp_ptr);

		free(quorum_ptr);

	}


	quorum->quorum_list.quorum_list_len = 0;
	free(quorum->quorum_list.quorum_list_val);
	quorum->quorum_list.quorum_list_val = NULL;

	return (error);
}


/*
 * DESCRIPTION:
 *    This method is frees the memory allocated for the device
 *    query request.
 *
 *
 * INPUT:
 *    cl_query_device_output_t:   device query results.
 *
 * OUTPUT:
 *    None.
 *
 * RETURN:
 *    CL_QUERY_OK:  On success
 *    CL_QUERY_EXX: On failure
 *
 * SIDE EFFECTS:
 *    None
 *
 * LOCKS:
 */

cl_query_error_t
clquery_free_device_result(cl_query_device_output_t *device)
{

	cl_query_error_t error = CL_QUERY_OK;
	cl_query_device_prop_t dev = NULL;
	uint_t i = 0, tmp_count = 0;
	strarr *tmp_ptr = NULL;


	if (device == NULL) {
		return (CL_QUERY_EINVAL);
	}

	/*
	 * If there are no entries return
	 */
	if (device->device_list.device_list_len == 0 ||
	    device->device_list.device_list_val == NULL) {
		return (CL_QUERY_OK);
	}

	/*
	 * Free any allocated memory
	 */
	for (i = 0; i < device->device_list.device_list_len; i++) {
		dev = device->device_list.device_list_val[i];
		if (dev == NULL) {
			continue;
		}
		if (dev->logical_disk_path != NULL) {
			free(dev->logical_disk_path);
			dev->logical_disk_path = NULL;
		}

		if (((dev->file_system_path).
			cl_query_didpath_list_t_val != NULL) &&
		    ((dev->file_system_path).
			cl_query_didpath_list_t_len != 0)) {

			(void) free_disk_path_list(&(dev->file_system_path));

		}

		if (dev->device_name != NULL) {
			free(dev->device_name);
			dev->device_name = NULL;
		}

		if (dev->primary_node_name != NULL) {
			free(dev->primary_node_name);
			dev->primary_node_name = NULL;
		}

		if (dev->device_type != NULL) {
			free(dev->device_type);
			dev->device_type = NULL;
		}

		/*
		 * Free the list of spare nodes
		 */
		tmp_count = dev->spare_nodes.strarr_list_len;
		tmp_ptr = dev->spare_nodes.strarr_list_val;

		error = free_strarr(tmp_count, tmp_ptr);

		/*
		 * Free the list of alternate nodes
		 */
		tmp_count = dev->alternate_nodes.strarr_list_len;
		tmp_ptr = dev->alternate_nodes.strarr_list_val;

		error = free_strarr(tmp_count, tmp_ptr);

		free(dev);
		dev = NULL;

	}





	device->device_list.device_list_len = 0;
	free(device->device_list.device_list_val);
	device->device_list.device_list_val = NULL;

	return (error);
}


static cl_query_error_t
free_disk_path_list(cl_query_didpath_list_t *data)
{
	unsigned int i;


	for (i = 0; i < data->cl_query_didpath_list_t_len; i++) {
		if (data->cl_query_didpath_list_t_val[i] != NULL) {

			if (data->cl_query_didpath_list_t_val[i]->node_name !=
			    NULL) {
				free(data->cl_query_didpath_list_t_val[i]->
				    node_name);
			}
			if (data->cl_query_didpath_list_t_val[i]->
			    phys_path_name != NULL) {
				free(data->cl_query_didpath_list_t_val[i]->
				    phys_path_name);
			}
			if (data->cl_query_didpath_list_t_val[i]->mount_point !=
			    NULL) {
				free(data->cl_query_didpath_list_t_val[i]->
				    mount_point);
			}

			free(data->cl_query_didpath_list_t_val[i]);
		}
	}

	free(data->cl_query_didpath_list_t_val);


	data->cl_query_didpath_list_t_len = 0;
	data->cl_query_didpath_list_t_val = NULL;

	return (CL_QUERY_OK);
}


static cl_query_error_t
free_port_prop_list(cl_query_port_prop_list_t *data)
{
	unsigned int i;
	cl_query_port_prop_elem_t elem;

	if (data == NULL)
		return (CL_QUERY_ENOMEM);

	if (data->prop_list.prop_list_val == NULL)
		return (CL_QUERY_ENOMEM);

	for (i = 0; i < data->prop_list.prop_list_len; i++) {
		elem = data->prop_list.prop_list_val[i];
		if (elem != NULL) {
			if (elem->port_name != NULL) {
				free(elem->port_name);
			}
			free(elem);
		}
	}


	free(data->prop_list.prop_list_val);

	data->prop_list.prop_list_val = NULL;
	data->prop_list.prop_list_len = 0;

	return (CL_QUERY_OK);
}


cl_query_error_t
free_strarr(uint_t count, strarr *str_ptr)
{
	uint_t i = 0;

	if ((str_ptr == NULL) && (count == 0)) {
		return (CL_QUERY_OK);
	}

	for (i = 0; i < count; i++) {
		if (str_ptr[i] != NULL)
			free(str_ptr[i]);
	}

	if (str_ptr != NULL) {
		free(str_ptr);
		str_ptr = NULL;
	}

	return (CL_QUERY_OK);
}


/*
 * DESCRIPTION:
 *    This method is frees the memory allocated for the ipmp
 *    query request internal data.
 *
 *
 * INPUT:
 *    cl_query_ipmp_prop_t:   internal array.
 *
 * OUTPUT:
 *    None.
 *
 * RETURN:
 *    CL_QUERY_OK:  On success
 *    CL_QUERY_EXX: On failure
 *
 * SIDE EFFECTS:
 *    None
 *
 * LOCKS:
 */

cl_query_error_t
clquery_free_ipmp_result_list(cl_query_ipmp_prop_t **ptr, int size)
{
	int i;
	cl_query_ipmp_prop_t *tmp;

	if ((ptr == NULL) || (size <= 0))
		return (CL_QUERY_EINVAL);


	tmp = *ptr;


	for (i = 0; i < size; i++) {

		if (tmp[i] == NULL)
			continue;

		if (tmp[i]->group_name != NULL) {
			free(tmp[i]->group_name);
		}
		if (tmp[i]->node_name != NULL) {
			free(tmp[i]->node_name);
		}

		(void) free_strarr(tmp[i]->adapter_list.strarr_list_len,
		    tmp[i]->adapter_list.strarr_list_val);

		free(tmp[i]);
	}



	free(*ptr);
	*ptr = NULL;

	return (CL_QUERY_OK);

}

/*
 * DESCRIPTION:
 *    This method is frees the memory allocated for the ipmp
 *    query request internal data.
 *
 *
 * INPUT:
 *    l_query_ipmp_output_t : ipmp data structure
 *
 * OUTPUT:
 *    None.
 *
 * RETURN:
 *    CL_QUERY_OK:  On success
 *    CL_QUERY_EXX: On failure
 *
 * SIDE EFFECTS:
 *    None
 *
 * LOCKS:
 */

cl_query_error_t
clquery_free_ipmp_result(cl_query_ipmp_output_t *out)
{
	cl_query_error_t diag;
	/*
	 * out may be passed not completly filled
	 * but all will be initialize to 0
	 */

	if (out == NULL)
		return (CL_QUERY_EINVAL);

	if ((out->ipmp_list.ipmp_list_len == 0) ||
	    (out->ipmp_list.ipmp_list_val == NULL)) {
		return (CL_QUERY_OK);
	}

	diag = clquery_free_ipmp_result_list(&(out->ipmp_list.ipmp_list_val),
	    (int)out->ipmp_list.ipmp_list_len);


	out->ipmp_list.ipmp_list_len = 0;
	out->ipmp_list.ipmp_list_val = NULL;

	return (diag);
}



/*
 * DESCRIPTION:
 *    This method is frees the memory allocated for the diskpath
 *    query request internal data.
 *
 *
 * INPUT:
 *   cl_query_diskpath_prop_t  : internal data of
 *   diskapth request result
 *
 * OUTPUT:
 *    None.
 *
 * RETURN:
 *    CL_QUERY_OK:  On success
 *    CL_QUERY_EXX: On failure
 *
 * SIDE EFFECTS:
 *    None
 *
 * LOCKS:
 */

cl_query_error_t
clquery_free_diskpath_result_list(cl_query_diskpath_prop_t **ptr, int size)
{
	int i;
	cl_query_diskpath_prop_t *tmp;

	if ((ptr == NULL) || (size <= 0))
		return (CL_QUERY_EINVAL);

	tmp = *ptr;

	for (i = 0; i < size; i++) {
		if (tmp[i] == NULL)
			continue;

		if (tmp[i]->diskpath_name)
			free(tmp[i]->diskpath_name);
		if (tmp[i]->node_name)
			free(tmp[i]->node_name);
		free(tmp[i]);
	}

	free(*ptr);
	*ptr = NULL;

	return (CL_QUERY_OK);

}

/*
 * DESCRIPTION:
 *    This method is frees the memory allocated for the diskpath
 *    query request.
 *
 *
 * INPUT:
 *   cl_query_diskpath_output_t  : diskapth request result
 *
 * OUTPUT:
 *    None.
 *
 * RETURN:
 *    CL_QUERY_OK:  On success
 *    CL_QUERY_EXX: On failure
 *
 * SIDE EFFECTS:
 *    None
 *
 * LOCKS:
 */

cl_query_error_t
clquery_free_diskpath_result(cl_query_diskpath_output_t *out)
{
	cl_query_error_t diag;
	if (out == NULL)
		return (CL_QUERY_EINVAL);

	if ((out->diskpath_list.diskpath_list_len == 0) ||
	    (out->diskpath_list.diskpath_list_val == NULL)) {
		out->diskpath_list.diskpath_list_val = NULL;
		return (CL_QUERY_OK);
	}

	diag =
	    clquery_free_diskpath_result_list(&(out->diskpath_list.
		diskpath_list_val), (int)out->diskpath_list.diskpath_list_len);

	out->diskpath_list.diskpath_list_len = 0;
	out->diskpath_list.diskpath_list_val = NULL;

	return (diag);
}

cl_query_error_t
clquery_free_transportpath_result_list(
	cl_query_transportpath_prop_t **ptr,
	    int size)
{
	int i;
	cl_query_transportpath_prop_t *tmp;

	tmp = *ptr;

	for (i = 0; i < size; i++) {
		if (tmp[i] == NULL) continue;

		if (tmp[i]->transportpath_name != NULL)
			free(tmp[i]->transportpath_name);
		if (tmp[i]->transportpath_endpoint_1_name != NULL)
			free(tmp[i]->transportpath_endpoint_1_name);
		if (tmp[i]->transportpath_endpoint_1_strname != NULL)
			free(tmp[i]->transportpath_endpoint_1_strname);
		if (tmp[i]->transportpath_endpoint_2_name != NULL)
			free(tmp[i]->transportpath_endpoint_2_name);
		if (tmp[i]->transportpath_endpoint_2_strname != NULL)
			free(tmp[i]->transportpath_endpoint_2_strname);

		free(tmp[i]);
	}

	free(*ptr);
	*ptr = NULL;

	return (CL_QUERY_OK);
}

cl_query_error_t
clquery_free_transportpath_result(cl_query_transportpath_output_t *out)
{
	cl_query_error_t diag;
	if (out == NULL)
		return (CL_QUERY_EINVAL);

	if ((out->transportpath_list.transportpath_list_len == 0) ||
	    (out->transportpath_list.transportpath_list_val == NULL)) {
		out->transportpath_list.transportpath_list_val = NULL;
		return (CL_QUERY_OK);
	}

	diag =
	    clquery_free_transportpath_result_list(
		    &(out->transportpath_list.transportpath_list_val),
			(int)out->transportpath_list.transportpath_list_len);

	out->transportpath_list.transportpath_list_len = 0;
	out->transportpath_list.transportpath_list_val = NULL;

	return (diag);

}
