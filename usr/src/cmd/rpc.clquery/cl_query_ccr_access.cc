/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#pragma ident	"@(#)cl_query_ccr_access.cc	1.8	08/05/20 SMI"

#include <stdlib.h>
#include <limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <strings.h>

#include <h/naming.h>
#include <h/ccr.h>
#include <orb/invo/common.h>
#include <nslib/ns.h>

#include <cl_query/rpc/cl_query.h>
#include <cl_query/cl_query_types.h>

typedef char **  StringPtr;


extern "C" {
	cl_query_error_t table_names_get(char *, uint_t *, StringPtr *);
	cl_query_error_t create_table(cl_query_ccr_param_t *);
	cl_query_error_t read_table(char *, cl_query_ccr_param_t *);
	cl_query_error_t table_len_get(char  *, int  *);
	cl_query_error_t delete_table(char  *);
	cl_query_error_t write_table_all(cl_query_ccr_param_t  *);
	cl_query_error_t delete_table_all(char *);

}

/* Global variables */
static cl_query_error_t convert_ccr_error(CORBA::Exception *ex);
static naming::naming_context_var ctxp;
static CORBA::Object_var	obj;
static ccr::directory_ptr	ccr_dir;
static Environment	e;

/*
 * DESCRIPTION:
 *    This method iniitalizes the ORB to access the cluster
 *    repository.
 *
 * INPUT:
 *    None.
 *
 * OUTPUT:
 *    None.
 *
 * RETURN:
 *    cl_query_error_t:   Error codes
 */

cl_query_error_t
init_ccr()
{
	CORBA::Exception*excep = NULL;
	cl_query_error_t	res = CL_QUERY_OK;

	//
	// look up ccr directory in name server
	//

	/* FIX_ME: shud this initialization be done everytime? */
	ctxp = ns::local_nameserver();
	obj = ctxp->resolve("ccr_directory", e);
	if ((excep = e.exception()) != NULL) {
		res = convert_ccr_error(excep);
		e.clear();
		return (res);
	}
	ccr_dir = ccr::directory::_narrow(obj);
	return (CL_QUERY_OK);
}


/*
 * DESCRIPTION:
 *    This method returns the list of names in the cluster repository.
 *    repository.
 *
 * INPUT:
 *    None.
 *
 * OUTPUT:
 *    Number of tables.
 *    List of tables.
 *
 * RETURN:
 *    cl_query_error_t:   Error codes
 */

cl_query_error_t
table_names_get(char *pattern, uint_t *num_tables, StringPtr *table_name)
{
	CORBA::Exception	*excep = NULL;
	CORBA::StringSeq	*tnames = NULL;
	cl_query_error_t	res = CL_QUERY_OK;
	int	i = 0, tmp_count = 0, index = 0;
	char	*name_ptr = NULL;

	res = init_ccr();

	if (res != CL_QUERY_OK) {
		return (res);
	}

	//
	// get table names
	//

	ccr_dir->get_table_names(tnames, e);
	if (e.exception()) {
		e.clear();
	}

	*num_tables = tnames->length();

	/* Filter the names with the pattern */
	if (pattern != NULL) {
		for (i = 0; i < *num_tables; i++) {
			/* Check for NULL pointer */
			if ((*tnames)[i] == NULL) {
				continue;
			}

			name_ptr = NULL;
			/* Check if the table name matches the pattern */
			name_ptr = strstr((*tnames)[i], pattern);
			if (name_ptr != NULL) {
				tmp_count++;
			}
		}

		/* Allocate memory for the result structure */
		*table_name =
		    (char **)calloc((size_t)tmp_count, sizeof (char *));
		if (*table_name == NULL) {
			return (CL_QUERY_ENOMEM);
		}


		/* Store the final table names */
		index = 0;
		for (i = 0; i < *num_tables; i++) {
			name_ptr = NULL;
			/* Check if the table name matches the pattern */
			name_ptr = strstr((*tnames)[i], pattern);
			if (name_ptr != NULL) {
				(*table_name)[index] =
				    (char *)strdup((*tnames)[i]);
				if ((*table_name)[index] == NULL) {
					res = CL_QUERY_ENOMEM;
					break;
				}
				index++;
			}
		}

		/* Store the Table count */
		*num_tables = tmp_count;

	} else {
		/* Return the complete list with no filters */
		*table_name =
		    (char **)calloc((size_t)*num_tables, sizeof (char *));
		if (*table_name == NULL) {
			return (CL_QUERY_ENOMEM);
		}

		for (i = 0; i < *num_tables; i++) {
			(*table_name)[i] = (char *)strdup((*tnames)[i]);
			if ((*table_name)[i] == NULL) {
				res = CL_QUERY_ENOMEM;
				break;
			}
		}
	}
	return (res);
}



/*
 * DESCRIPTION:
 *    This method creates the CCR table in the cluster repository.
 *
 * INPUT:
 *    cl_query_ccr_param_t: table names and contents.
 *
 * OUTPUT:
 *    None.
 *
 * RETURN:
 *    cl_query_error_t:   Error codes
 */
cl_query_error_t
create_table(cl_query_ccr_param_t *input)
{
	CORBA::Exception	*excep = NULL;
	cl_query_error_t	res = CL_QUERY_OK;
	ccr::readonly_table_ptr	tablePtr;
	int	i = 0;
	char	*key = NULL, *value = NULL;
	ccr::updatable_table_var	transptr = NULL;

	if (input == NULL) {
		return (CL_QUERY_EINVAL);
	}

	res = init_ccr();
	if (res != CL_QUERY_OK) {
		return (res);
	}

	ccr_dir->create_table(input->table_name, e);
	if ((excep = e.exception()) != NULL) {
		res = convert_ccr_error(excep);
		e.clear();
		return (res);
	}

	/* If creating a empty table */
	if (input->table_rows.table_rows_len == 0 ||
	    input->table_rows.table_rows_val == NULL) {
		return (res);
	}


	res = write_table_all(input);

	return (res);
}



/*
 * DESCRIPTION:
 *    This method reads the CCR table contents
 *
 * INPUT:
 *    Name of the table name.
 *
 * OUTPUT:
 *    cl_query_ccr_param_t: table names and contents.
 *
 * RETURN:
 *    cl_query_error_t:   Error codes
 */

cl_query_error_t
read_table(char  *table_name, cl_query_ccr_param_t *result)
{
	CORBA::Exception	*excep = NULL;
	cl_query_error_t	res = CL_QUERY_OK;
	ccr::readonly_table_ptr	tablePtr;
	int	i = 0, index = 0;
	int	num_rows = 0, err = 0;
	ccr::table_element	*outelem = NULL;
	ccr_table_elem_t	rows = NULL;


	if (table_name == NULL) {
		return (CL_QUERY_EINVAL);
	}

	res = init_ccr();
	if (res != CL_QUERY_OK) {
		return (res);
	}

	// lookup table
	tablePtr = ccr_dir->lookup(table_name, e);
	if ((excep = e.exception()) != NULL) {
		res = convert_ccr_error(excep);
		e.clear();
		return (res);
	}

	// call get_num_elements
	num_rows = tablePtr->get_num_elements(e);
	result->table_rows.table_rows_len = num_rows;

	// iterate through table
	tablePtr->atfirst(e);
	if ((excep = e.exception()) != NULL) {
		res = convert_ccr_error(excep);
		e.clear();
		return (res);
	}

	// Allocate memory in the output param
	result->table_rows.table_rows_val = (ccr_table_elem_t *)
	    calloc(num_rows, sizeof (ccr_table_elem_t));
	if (result->table_rows.table_rows_val == NULL) {
		return (CL_QUERY_ENOMEM);
	}

	index = 0;
	// read elements from table file
	for (i = 0; i < num_rows; i++) {
		/* Read the row element */
		tablePtr->next_element(outelem, e);
		if ((excep = e.exception()) != NULL) {
			res = convert_ccr_error(excep);
			e.clear();
			break;
		}

		result->table_rows.table_rows_val[index] =
		    (struct ccr_table_elem *)
		    calloc(1, sizeof (struct ccr_table_elem));
		if (result->table_rows.table_rows_val[index] == NULL) {
			res = CL_QUERY_ENOMEM;
		}

		if (res == CL_QUERY_OK) {
			rows = result->table_rows.table_rows_val[index];

			rows->key = (char *)strdup(outelem->key);
			rows->value = (char *)strdup(outelem->data);

			if (rows->key == NULL || rows->value == NULL) {
				res = CL_QUERY_ENOMEM;
				// delete outelem;
				if (outelem != NULL) {
					delete(outelem);
				}
				break;
			}
		}

		// delete outelem;
		if (outelem != NULL) {
			delete(outelem);
		}

		// Increment the index
		index++;

	}

	if ((excep = e.exception()) != NULL) {
		res = convert_ccr_error(excep);
		e.clear();
	}
	return (res);
}


/*
 * DESCRIPTION:
 *    This method returns the size of the table specified.
 *
 * INPUT:
 *    Name of the Table.
 *
 * OUTPUT:
 *    Size of the Table.
 *
 * RETURN:
 *    cl_query_error_t:   Error codes
 */

cl_query_error_t
table_len_get(char  *table_name,  int  *count)
{
	CORBA::Exception	*excep = NULL;
	cl_query_error_t	res = CL_QUERY_OK;
	ccr::readonly_table_ptr	readPtr;


	if (table_name == NULL || count == NULL) {
		return (CL_QUERY_EINVAL);
	}

	res = init_ccr();
	if (res != CL_QUERY_OK) {
		return (res);
	}

	readPtr = ccr_dir->lookup(table_name, e);
	if ((excep = e.exception()) != NULL) {
		res = convert_ccr_error(excep);
		e.clear();
		return (res);
	}

	*count = readPtr->get_num_elements(e);
	if ((excep = e.exception()) != NULL) {
		res = convert_ccr_error(excep);
		e.clear();
		return (res);
	}

	return (res);
}


/*
 * DESCRIPTION:
 *    This method deletes the table in the repository.
 *
 * INPUT:
 *    Name of the Table.
 *
 * OUTPUT:
 *    None.
 *
 * RETURN:
 *    cl_query_error_t:   Error codes
 */

cl_query_error_t
delete_table(char *table_name)
{
	CORBA::Exception	*excep = NULL;
	cl_query_error_t	res = CL_QUERY_OK;
	ccr::readonly_table_ptr	tablePtr;
	CORBA::StringSeq	*tnames;
	int	i = 0;

	res = init_ccr();
	if (res != CL_QUERY_OK) {
		return (res);
	}

	ccr_dir->remove_table(table_name, e);
	if ((excep = e.exception()) != NULL) {
		e.clear();
		res = convert_ccr_error(excep);
	}

	return (res);
}


/*
 * DESCRIPTION:
 *    This method updates the contents of the Table
 *
 * INPUT:
 *    cl_query_ccr_param_t: table names and contents.
 *
 * OUTPUT:
 *    None.
 *
 * RETURN:
 *    cl_query_error_t:   Error codes
 */

cl_query_error_t
write_table_all(cl_query_ccr_param_t  *input)
{
	CORBA::Exception	*excep = NULL;
	cl_query_error_t	res = CL_QUERY_OK;
	ccr::updatable_table_ptr	update_ptr;
	ccr::element_seq *elems = NULL;
	char	*key = NULL, *value = NULL;

	elems = new ccr::element_seq(input->table_rows.table_rows_len);
	if (elems == NULL) {
		return (CL_QUERY_ENOMEM);
	}

	for (int i = 0; i < input->table_rows.table_rows_len; i++) {
		(*elems)[i].key = strdup(
		    input->table_rows.table_rows_val[i]->key);
		(*elems)[i].data = strdup(
		    input->table_rows.table_rows_val[i]->value);

		if ((*elems)[i].key == NULL ||
		    (*elems)[i].data == NULL) {
			res = CL_QUERY_ENOMEM;
			goto end;
		}
	}
	elems->length(input->table_rows.table_rows_len);

	res = init_ccr();
	if (res != CL_QUERY_OK) {
		goto end;
	}

	update_ptr = ccr_dir->begin_transaction(input->table_name, e);
	if ((excep = e.exception()) != NULL) {
		res = convert_ccr_error(excep);
		e.clear();
		goto end;
	}

	/* Delete the contents of the table before update */
	update_ptr->remove_all_elements(e);
	if ((excep = e.exception()) != NULL) {
		res = convert_ccr_error(excep);
		e.clear();
	}

	/* Return error if there is error in deleting the */
	/* previous content */
	if (res != CL_QUERY_OK) {
		update_ptr->abort_transaction(e);
		goto end;
	}

	/* add the elements to the table */
	update_ptr->add_elements(*elems, e);
	/* Check for any errors */
	if ((excep = e.exception()) != NULL) {
		res = convert_ccr_error(excep);
		e.clear();
	}

	if (res != CL_QUERY_OK) {
		update_ptr->abort_transaction(e);
	} else {
		update_ptr->commit_transaction(e);
	}

	if ((excep = e.exception()) != NULL) {
		res = convert_ccr_error(excep);
		e.clear();
	}

	end:

	// The line below will free all the strdup'd strings
	delete(elems);

	return (res);
}


/*
 * DESCRIPTION:
 *    This method deletes the contents of a table.
 *    repository.
 *
 * INPUT:
 *    Name of the table.
 *
 * OUTPUT:
 *    None.
 *
 * RETURN:
 *    cl_query_error_t:   Error codes
 */

cl_query_error_t
delete_table_all(char *table_name)
{
	CORBA::Exception	*excep = NULL;
	cl_query_error_t	res = CL_QUERY_OK;
	int	i = 0;
	CORBA::StringSeq	*tnames;
	ccr::updatable_table_ptr	delete_ptr;

	res = init_ccr();
	if (res != CL_QUERY_OK) {
		return (res);
	}

	delete_ptr = ccr_dir->begin_transaction(table_name, e);
	if ((excep = e.exception()) != NULL) {
		res = convert_ccr_error(excep);
		e.clear();
		return (res);
	}

	delete_ptr->remove_all_elements(e);
	if ((excep = e.exception()) != NULL) {
		res = convert_ccr_error(excep);
		e.clear();
	}

	if (res == CL_QUERY_OK) {
		delete_ptr->commit_transaction(e);
	} else {
		delete_ptr->abort_transaction(e);
	}

	return (res);
}


/*
 * DESCRIPTION:
 *    Utility function to convert the CCR errors into cl_query error
 *    codes.
 *
 * INPUT:
 *    Corba Exception
 *
 * OUTPUT:
 *    None.
 *
 * RETURN:
 *    cl_query_error_t:   Error codes
 */

static cl_query_error_t
convert_ccr_error(CORBA::Exception *ex)
{

	if (ex == NULL) {
		return (CL_QUERY_EINVAL);
	}

	if (ccr::no_such_table::_exnarrow(ex) != NULL) {
		return (CL_QUERY_ENOENT);
	}

	if (ccr::table_exists::_exnarrow(ex) != NULL) {
		return (CL_QUERY_EXISTS);
	}

	if (ccr::invalid_table_name::_exnarrow(ex) != NULL) {
		return (CL_QUERY_EINVALID);
	}

	if (ccr::no_such_key::_exnarrow(ex) != NULL) {
		return (CL_QUERY_ENOENT);
	}

	if (ccr::table_invalid::_exnarrow(ex) != NULL) {
		return (CL_QUERY_EINVALID);
	}

	if (ccr::table_modified::_exnarrow(ex) != NULL) {
		return (CL_QUERY_EMODIFIED);
	}

	if (ccr::readonly_access::_exnarrow(ex) != NULL) {
		return (CL_QUERY_EPERM);
	}

	if ((ccr::out_of_service::_exnarrow(ex) != NULL) ||
	    ccr::database_invalid::_exnarrow(ex) != NULL) {
		return (CL_QUERY_ECORRUPT);
	}

	if (ccr::lost_quorum::_exnarrow(ex) != NULL) {
		return (CL_QUERY_EIO);
	}

	return (CL_QUERY_EUNEXPECTED);
}
