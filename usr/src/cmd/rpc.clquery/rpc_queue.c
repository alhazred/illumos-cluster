/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 ****************************************************************************
 * Component = clquery deamon
 *
 * Synopsis  = clquery rpc request queue.
 *
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *
 ***************************************************************************
 */

#pragma ident "@(#)rpc_queue.c 1.3 08/05/20 SMI"

#include <stdlib.h>
#include <strings.h>
#include <rpc/rpc.h>
#include <rpc/xdr.h>
#include <cl_query/rpc/cl_query.h>
#include "rpc_queue.h"



static rpc_queue_t *rpcq_head;
static rpc_queue_t *rpcq_tail;
static uint_t rpcq_count;
static pthread_mutex_t rpcq_mutex;
static sem_t rpcq_sem;

static cl_query_error_t rpcq_remove_with_no_lock(rpc_queue_t *item);


/*
***********************************************************************
* RPC queue management:
* after each user request received, a new item is inserted in the queue.
* A closure handler is attach to each request.
* if a client closure is received, the 'faulty' flag is set by the callback.
* if the callback detects that the request is not assigned to a server thread
* it delete the request from the queue.
* When a thread is available to treat the request,
* it mark the request as used
* A item is only remove and freed by the responsible thread
* after the response
* have benn send to the client
***********************************************************************
*/

/*
 * DESCRIPTION:
 *    client closure handler
 *    this hamdler set the faulty flag on the pending request
 *    which just lost its client
 *    if the 'used' falg is not set , remove the request from the queue
 *
 *
 * INPUT:
 *    None
 *
 * OUTPUT:
 *    None
 *
 * RETURN:
 *
 *
 * SIDE EFFECTS:
 *    None
 *
 * LOCKS:
 * lock the request queue
 */


void
clquery_close_handler(const SVCXPRT * handle,
    const bool_t dummy /*lint !e715 */)
{
	cl_query_error_t error;
	rpc_queue_t *q_ptr;


	/*
	 * TODO : may be use trylock locking inside handler....not so good
	 */
	error = rpcq_lock();
	if (error != CL_QUERY_OK)
		return;

	if (rpcq_head == NULL) {
		/*
		 * how this can happen...
		 */
		(void) rpcq_unlock();
		return;
	}
	q_ptr = rpcq_head;
	while (q_ptr != NULL) {
		if (q_ptr->transp == handle) {
			/*
			 * we found the faulty one
			 */
			break;
		}
		q_ptr = q_ptr->next;
	}

	if (q_ptr == NULL) {
		/*
		 * we are late the consumer thread already used it ...not good
		 */
		(void) rpcq_unlock();
		return;
	}

	/*
	 * set the faulty flag to warn the thread that client is dead
	 */

	/*
	 * if 'used' not set, remove it dierctly from the queue
	 */
	if (q_ptr->used == 0) {
		(void) rpcq_remove_with_no_lock(q_ptr);
	} else {
		q_ptr->faulty = 1;
	}


	(void) rpcq_unlock();

}/*lint !e715 */



static cl_query_error_t
rpcq_wait()
{
	(void) sem_wait(&rpcq_sem);
	return (CL_QUERY_OK);
}

static cl_query_error_t
rpcq_post()
{
	(void) sem_post(&rpcq_sem);
	return (CL_QUERY_OK);
}

/*
 * DESCRIPTION:
 *    This method initializes the internal data structures
 *
 *
 * INPUT:
 *    None
 *
 * OUTPUT:
 *    None
 *
 * RETURN:
 *    CL_QUERY_OK   : On success
 *    CL_QUERY_EXXX : On error
 *
 * SIDE EFFECTS:
 *    None
 *
 * LOCKS:
 */

cl_query_error_t
rpc_queue_init()
{

	rpcq_head = NULL;
	rpcq_tail = NULL;
	rpcq_count = 0;

	if (pthread_mutex_init(&rpcq_mutex, NULL) != 0) {
		return (CL_QUERY_ENOMEM);
	}

	(void) sem_init(&rpcq_sem, 0, 0);

	return (CL_QUERY_OK);
}


/*
 * DESCRIPTION:
 *    This method locks the RPC queue
 *
 *
 * INPUT:
 *    None
 *
 * OUTPUT:
 *    None
 *
 * RETURN:
 *    CL_QUERY_OK   : On success
 *    CL_QUERY_EXXX : On error
 *
 * SIDE EFFECTS:
 *    No other thread will be able to access the RPC queue
 *     till the queue is unlocked.
 *
 * LOCKS:
 */

static cl_query_error_t
rpcq_lock()
{
	if (pthread_mutex_lock(&rpcq_mutex) != 0) {
		return (CL_QUERY_EUNEXPECTED);
	}
	return (CL_QUERY_OK);
}

/*
 * DESCRIPTION:
 *    This method unlocks the RPC queue
 *
 *
 * INPUT:
 *    None
 *
 * OUTPUT:
 *    None
 *
 * RETURN:
 *    CL_QUERY_OK   : On success
 *    CL_QUERY_EXXX : On error
 *
 * SIDE EFFECTS:
 *   None.
 *
 * LOCKS:
 */

static cl_query_error_t
rpcq_unlock()
{
	if (pthread_mutex_unlock(&rpcq_mutex) != 0) {
		return (CL_QUERY_EUNEXPECTED);
	}
	return (CL_QUERY_OK);
}

/*
 * DESCRIPTION:
 *    This method frees the internal data structures
 *
 *
 * INPUT:
 *    None
 *
 * OUTPUT:
 *    None
 *
 * RETURN:
 *    CL_QUERY_OK   : On success
 *    CL_QUERY_EXXX : On error
 *
 * SIDE EFFECTS:
 *    None
 *
 * LOCKS:
 */

/*lint -e528 not used for now  */
static cl_query_error_t
rpc_queue_cleanup /*lint !e528 */()
{
	cl_query_error_t res = CL_QUERY_OK;

	if (pthread_mutex_destroy(&rpcq_mutex) != 0) {
		res = CL_QUERY_ENOMEM;
	}

	(void) sem_destroy(&rpcq_sem);

	return (res);
}
/*lint +e528 not used for now  */
/*
 * DESCRIPTION:
 *    This methods returns the current count of entries in the Queue
 *
 *
 * INPUT:
 *    None
 *
 * OUTPUT:
 *    rpcq_count     : number of entries in the Queue
 *
 * RETURN:
 *    CL_QUERY_OK   : On success
 *    CL_QUERY_EXXX : On error
 *
 * SIDE EFFECTS:
 *    None
 *
 * LOCKS:
 */

static cl_query_error_t
/*lint +e528 not used for now */
rpc_queue_count_get /*lint !e528 */(uint32_t *count)
{
	*count = rpcq_count;
	return (CL_QUERY_OK);
}
/*lint -e528 not used for now */


/*
 * DESCRIPTION:
 *    This method inserts a RPC request in the queue
 *
 * INPUT:
 *    struct svc_req : RPC request handler
 *    SVCXPRT        : RPC service handler
 *
 * OUTPUT:
 *    None
 *
 * RETURN:
 *    CL_QUERY_OK   : On success
 *    CL_QUERY_EXXX : On error
 *
 * SIDE EFFECTS:
 *    Wakes up any threads waiting on the queue to be non-empty
 *    RPC request input argumnets are retrived from rqstp and stored
 *    in the queue element.
 *
 * LOCKS:
 */

cl_query_error_t
rpcq_put(register SVCXPRT * transp, xdrproc_t rpc_xdr_arg,
    xdrproc_t rpc_xdr_result, svc_call_t rpc_func_ptr)
{
	cl_query_error_t res = CL_QUERY_OK;
	rpc_queue_t *new = NULL;

	if (transp == NULL) {
		return (CL_QUERY_EINVAL);
	}

	new = (rpc_queue_t *)calloc(1, sizeof (rpc_queue_t));
	if (new == NULL) {
		return (CL_QUERY_ENOMEM);
	}

	/*
	 * Get the input arguments of the RPC request
	 */

	if (!svc_getargs(transp, rpc_xdr_arg, (char *)&(new->arguments))) {
		svcerr_decode(transp);
		return (CL_QUERY_EUNEXPECTED);
	}


	new->transp = transp;
	new->rpc_xdr_result = rpc_xdr_result;
	new->rpc_func_ptr = rpc_func_ptr;
	new->rpc_xdr_arg = rpc_xdr_arg;
	new->faulty = 0;
	new->used = 0;
	new->next = NULL;


	/*
	 * lock the queue
	 */
	res = rpcq_lock();
	if (res != CL_QUERY_OK) {
		free(new);
		return (res);
	}

	/*
	 * Insert the request at the end of the queue
	 */
	if (rpcq_head == NULL) {
		rpcq_head = new;
		rpcq_tail = rpcq_head;
	} else {
		rpcq_tail->next = new;
		rpcq_tail = new;
	}
	rpcq_count++;


	(void) rpcq_unlock();

	/*
	 * wake-up the threads in the thread pool to service the request
	 */
	(void) rpcq_post();

	return (CL_QUERY_OK);
}


/*
 * DESCRIPTION:
 *    This method set 'item' to the first unused pending
 *    request of the RPC Queue.
 *
 * INPUT:
 *    None
 *
 * OUTPUT:
 *    data         : First RPC request in the Queue
 *
 * RETURN:
 *    CL_QUERY_OK   : On success
 *    CL_QUERY_EXXX : On error
 *
 * SIDE EFFECTS:
 *    The thread will wait on a sem till data is available for removal.
 *
 * LOCKS:
 */

cl_query_error_t
rpcq_get(rpc_queue_t **item)
{

	cl_query_error_t res = CL_QUERY_OK;
	rpc_queue_t *seeker;

	(void) rpcq_wait();

	res = rpcq_lock();
	if (res != CL_QUERY_OK)
		return (CL_QUERY_EUNEXPECTED);
	*item = NULL;

	seeker = rpcq_head;
	while ((seeker != NULL) && (seeker->used != 0))
		seeker = seeker->next;

	if (seeker == NULL) {
		/*
		 * everything was already used, strange...
		 */
		(void) rpcq_unlock();
		return (CL_QUERY_EUNEXPECTED);
	}

	seeker->used = 1;
	*item = seeker;

	res = rpcq_unlock();

	return (res);
}


/*
 * DESCRIPTION:
 *    This method remove 'item' from RPC Queue.
 *    This method is called by one of the pool thread. once she is done
 *    with the request
 *
 * INPUT:
 *    None
 *
 * OUTPUT:
 *    data         : First RPC request in the Queue
 *
 * RETURN:
 *    CL_QUERY_OK   : On success
 *    CL_QUERY_EXXX : On error
 *
 * SIDE EFFECTS:
 *
 *
 * LOCKS: caller is already locking the queue
 */
static cl_query_error_t
rpcq_remove_with_no_lock(rpc_queue_t *item)
{
	rpc_queue_t *seeker;
	rpc_queue_t *seeker2;


	seeker = rpcq_head;
	while ((seeker != NULL) && (seeker != item))
		seeker = seeker->next;


	if (seeker == NULL)
		return (CL_QUERY_OK);


	if (seeker == rpcq_head) {
		/*
		 * just move the head forward
		 */
		rpcq_head = rpcq_head->next;
		if (rpcq_head == NULL) {
			/*
			 * this was the last one
			 */
			rpcq_tail = NULL;
		}
	} else {
		seeker2 = rpcq_head;
		while ((seeker2->next != seeker)) {
			seeker2 = seeker2->next;
		}
		if (rpcq_tail == seeker) {
			/*
			 * it was the last of the list
			 */
			rpcq_tail = seeker2;
		}
		seeker2->next = seeker->next;
	}

	/*
	 * Free the memory allocated by the RPC
	 */
	(void) svc_freeargs(seeker->transp, seeker->rpc_xdr_arg,
	    (caddr_t)&(seeker->arguments));

	svc_done(seeker->transp);

	free(seeker);

	rpcq_count--;

	return (CL_QUERY_OK);
}

cl_query_error_t
rpcq_remove(rpc_queue_t *item)
{
	cl_query_error_t res;

	(void) rpcq_lock();
	res = rpcq_remove_with_no_lock(item);
	(void) rpcq_unlock();

	return (res);

}
