/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 **************************************************************************
 *
 * Component = clquery deamon
 *
 * Synopsis  = clquery RPC manager to dispatch RPC requests from
 * different clients
 *
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 **************************************************************************
 */

#pragma ident "@(#)rpc_manager.c 1.5 08/05/20 SMI"

#include <stdlib.h>
#include <pthread.h>

#include "rpc_manager.h"


static pthread_t *thread_id_arr;
static int rpc_thread_count;

extern cl_query_error_t pthread_to_clquery_err(int);

/*
 * 5 threads to manage to request poll
 * shoul be enough server won't be highly stressed
 */

#define	CL_QUERY_THR_POOL_SIZE	5



/*
 * DESCRIPTION:
 *    RPC thread (thread pool)
 *    This method waits indefinetly on the rpc queue till a request
 *    is queued. It removes the request from the queue and dispathes it.
 *
 * INPUT:
 *    None.
 *
 * OUTPUT:
 *    None.
 *
 * RETURN:
 *    None.
 *
 * SIDE EFFECTS:
 *    RPC request is serviced
 *
 * LOCKS:
 */

/* ARGSUSED */
void *
rpcq_consume(void *dummy)
{

	cl_query_error_t res = CL_QUERY_OK;
	rpc_queue_t *item = NULL;
	int retval = 0;
	xdrproc_t _xdr_result;
	register SVCXPRT *transp = NULL;

	/*lint +e754 */
	union {
		cl_query_adapter_output_t adapter_info_get_1_res;
		cl_query_node_output_t node_info_get_1_res;
		cl_query_quorum_output_t quorum_info_get_1_res;
		cl_query_cable_output_t cable_info_get_1_res;
		cl_query_junction_output_t junction_info_get_1_res;
		cl_query_device_output_t device_info_get_1_res;
		cl_query_ipmp_output_t ipmp_info_get_1_res;
		cl_query_diskpath_output_t disk_path_info_get_1_res;
		cl_query_transportpath_output_t transportpath_info_get_1_res;

		ccr_table_list_t ccr_table_list_res;
		ccr_table_size_t ccr_table_size_res;
		cl_query_ccr_param_t ccr_table_cont_res;
		cl_query_output_t ccr_output_res;
	} result;
/*lint -e754 */

	(void) pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
	(void) pthread_setcanceltype(PTHREAD_CANCEL_DEFERRED, NULL);



	(void) memset(&result, 0, sizeof (result));
	while (1) {

		/*
		 * we don't want to be disturb while processing client request
		 */
		(void) pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
		pthread_testcancel();
		(void) pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL);

		res = rpcq_get(&item);
		if ((res != CL_QUERY_OK) || (item == NULL)) {
			continue;
		}

		transp = item->transp;
		_xdr_result = item->rpc_xdr_result;


		retval = (bool_t)(*item->rpc_func_ptr) ((void *)
		    &(item->arguments), (void *)&result, NULL);
		if (retval > 0) {
			/*
			 * the client may be already dead
			 */
			if (item->faulty == 0) {
				if (!svc_sendreply(transp, _xdr_result,
					(char *)&result)) {
					svcerr_systemerr(transp);
				}

			}
		}

		if (!clquery_server_1_freeresult(transp, _xdr_result,
			(caddr_t)&result)) {
		}



		(void) rpcq_remove(item);


	}
}


/*
 * DESCRIPTION:
 *    This method initializes the RPC dispatcher thread pool
 *
 * INPUT:
 *    thread_count:  Number of threads in the thread pool.
 *
 * OUTPUT:
 *    None
 *
 * RETURN:
 *    CL_QUERY_OK  :  On success
 *    CL_QUERY_EXXX: On error
 *
 * SIDE EFFECTS:
 *    Starts _consumers number of threads to execute consume.
 *
 * LOCKS:
 *
 */

cl_query_error_t
init_thread_pool(uint_t thread_count)
{
	pthread_attr_t attr;
	uint_t i = 0;
	int result = 0;
	cl_query_error_t res = CL_QUERY_OK;

	thread_id_arr = (pthread_t *)malloc(
		sizeof (pthread_t) * thread_count);
	if (thread_id_arr == NULL) {
		return (CL_QUERY_ENOMEM);
	}

	for (i = 0; i < thread_count; i++) {
		thread_id_arr[i] = 0;
	}

	(void) pthread_attr_init(&attr);

	/*
	 * Create a bound thread
	 */
	result = pthread_attr_setscope(&attr, PTHREAD_SCOPE_SYSTEM);
	res = pthread_to_clquery_err(result);
	if (res != CL_QUERY_OK) {
		return (res);
	}

	for (i = 0; i < thread_count; i++) {
		result =
		    pthread_create(&thread_id_arr[i], &attr, rpcq_consume,
		    NULL);
		res = pthread_to_clquery_err(result);
		if (res != CL_QUERY_OK) {
			return (res);
		}
	}

	(void) pthread_attr_destroy(&attr);

	return (CL_QUERY_OK);
}


/*
 * DESCRIPTION:
 *    This method cleans up thread pool
 *
 * INPUT:
 *    thread_count:  Number of threads in the thread pool.
 *
 * OUTPUT:
 *    None
 *
 * RETURN:
 *    CL_QUERY_OK  :  On success
 *    CL_QUERY_EXXX: On error
 *
 * SIDE EFFECTS:
 *
 * LOCKS:
 *
 */

cl_query_error_t
cleanup_thread_pool(int thread_count)
{
	int i;

	if (thread_id_arr != NULL) {
		for (i = 0; i < thread_count; i++) {
			if (thread_id_arr[i] != 0) {
				(void) pthread_cancel(thread_id_arr[i]);
			}
		}
		free(thread_id_arr);
		thread_id_arr = NULL;
	}
	return (CL_QUERY_OK);
}



/*
 * DESCRIPTION:
 *    This method initializes rpc manager
 *
 * INPUT:
 *
 * OUTPUT:
 *    None
 *
 * RETURN:
 *    CL_QUERY_OK        : if the operation is successful
 *    CL_QUERY_EINVAL    : if the input is invalid
 *
 * SIDE EFFECTS:
 *    Starts the thread pool
 *
 * LOCKS:
 *
 */

cl_query_error_t
init_rpc_manager(int consumers)
{
	cl_query_error_t rval = CL_QUERY_OK;

	rpc_thread_count = consumers;

	rval = rpc_queue_init();
	if (rval != CL_QUERY_OK) {
		return (rval);
	}

	if (rpc_thread_count > 0) {
		/*
		 * FIX_ME: remove hard coded value
		 */
		rval = init_thread_pool(CL_QUERY_THR_POOL_SIZE);
	}

	if (rval != CL_QUERY_OK) {
		(void) cleanup_rpc_manager();
		return (rval);
	}

	return (rval);
}


/*
 * DESCRIPTION:
 *    This method cleans up cghaReactor.
 *
 * INPUT:
 *    None
 *
 * OUTPUT:
 *    None
 *
 * RETURN:
 *    CL_QUERY_OK      : if the operation is successful
 *
 * SIDE EFFECTS:
 *
 * LOCKS:
 *
 */

cl_query_error_t
cleanup_rpc_manager()
{
	(void) cleanup_thread_pool(rpc_thread_count);
	return (CL_QUERY_OK);
}
