/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _RPC_MANAGER_H
#define	_RPC_MANAGER_H

#pragma ident	"@(#)rpc_manager.h	1.3	08/05/20 SMI"


#include <pthread.h>
#include <cl_query/cl_query_types.h>
#include "rpc_queue.h"



extern void * rpcq_consume(void *);
extern cl_query_error_t init_thread_pool(uint_t thread_count);
extern cl_query_error_t cleanup_thread_pool(int thread_count);
extern cl_query_error_t init_rpc_manager(int consumers);
extern cl_query_error_t cleanup_rpc_manager(void);

#endif /* _RPC_MANAGER_H */
