/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _RPC_QUEUE_H
#define	_RPC_QUEUE_H

#pragma ident	"@(#)rpc_queue.h	1.3	08/05/20 SMI"

#include <pthread.h>
#include <semaphore.h>
#include <rpc/rpc.h>
#include <rpc/xdr.h>
#include <rpc/svc.h>
#include <rpc/auth_unix.h>
#include <rpc/rpc_com.h>

/*
 * TODO : where is that include
 */
/*
 * #include <rpc/svc_auth.h>
 */
bool_t svc_control(SVCXPRT * svc, const uint_t req, void *info);

#include <cl_query/rpc/cl_query.h>
#include <cl_query/cl_query_types.h>



typedef bool_t(*svc_call_t) (char *, void *, struct svc_req *);

/*
 * RPC queue element
 */
typedef struct rpc_queue {
	/*
	 * Input arguments
	 *
	 */
	union {
		cl_query_input_t sc_get_arg;
		cl_query_ccr_param_t ccr_arg;
	} arguments;

	/*
	 * XDR routine for result
	 * structure
	 */
	xdrproc_t rpc_xdr_result;
/*
 * XDR routine for result structure
 *
 */
	xdrproc_t rpc_xdr_arg;
/*
 * func pointer for RPC call
 *
 */
	svc_call_t rpc_func_ptr;
/*
 * RPC transport handler
 *
 */
	SVCXPRT *transp;
/*
 * client closure callback was called on
 * this one
 */
	short faulty;
/*
 * 1 if already used by a thr
 */
	short used;
	struct rpc_queue *next;
} rpc_queue_t;



extern cl_query_error_t rpc_queue_init(void);
static cl_query_error_t rpcq_lock(void);
static cl_query_error_t rpcq_unlock(void);
/*lint -e715 -e528 */
static cl_query_error_t rpc_queue_cleanup(void);
static cl_query_error_t rpc_queue_count_get(uint32_t *);
/*lint +e715 +e528 */
extern cl_query_error_t rpcq_put(SVCXPRT *, xdrproc_t, xdrproc_t, svc_call_t);
extern cl_query_error_t rpcq_get(rpc_queue_t ** item);
extern void clquery_close_handler(const SVCXPRT * handle, const bool_t dummy);
extern cl_query_error_t rpcq_remove(rpc_queue_t *item);

static cl_query_error_t rpc_post(void);
static cl_query_error_t rpc_wait(void);

#endif /* _RPC_QUEUE_H */
