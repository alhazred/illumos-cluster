/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 **************************************************************************
 *
 * Component = clquery deamon
 *
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 **************************************************************************
 */

#pragma ident "@(#)clquery_network.c 1.8 08/05/20 SMI"

#include <stdlib.h>
#include <limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <strings.h>

#include <rpc/rpc.h>
#include <rpc/xdr.h>
#include <rpc/svc.h>

#include <scadmin/scstat.h>
#include <scadmin/scconf.h>

#include <cl_query/rpc/cl_query.h>
#include <cl_query/cl_query_types.h>

static cl_query_error_t
get_adapter_property(scconf_cfg_prop_t *, cl_query_adapter_prop_t);
static cl_query_error_t
get_port_list(scconf_cfg_port_t *, cl_query_port_prop_list_t *);
extern cl_query_state_t
    sccconf_port_status_to_clquery(scconf_cltr_portstate_t state);
extern cl_query_error_t scconf_to_clquery(scconf_errno_t err);
extern cl_query_state_t
    convert_scconf_status(scconf_state_t state);
extern cl_query_epoint_type_t convert_scconf_epoint(scconf_cltr_epoint_type_t);
extern cl_query_error_t authenticate_request(void);
extern cl_query_error_t
clquery_free_adapter_result(cl_query_adapter_output_t *);
extern cl_query_error_t clquery_free_cable_result(cl_query_cable_output_t *);
extern cl_query_error_t
clquery_free_junction_result(cl_query_junction_output_t *);
extern cl_query_state_t scstate_to_clquery_state(sc_state_code_t state);
extern cl_query_error_t
clquery_free_transportpath_result(cl_query_transportpath_output_t *out);


/*
 * DESCRIPTION:
 *    This method is the server part of the ADAPTER_INFO_GET Api
 *    query. Information related to all the adapeters are retrieved
 *    from the scconf library and returned to the user.
 *
 * INPUT:
 *    cl_query_input_t:  Name of the adapter or NULL (default:all adpaters)
 *    struct svc_req  :  RPC request handler
 *
 * OUTPUT:
 *    cl_query_adapter_output_t:  Adapter information
 *    error is returned in output.error
 *
 * RETURN:
 *    boolean:  TRUE for success
 *              FALSE on any error
 *
 * SIDE EFFECTS:
 *    None
 *
 * LOCKS:
 */

/* ARGSUSED */
bool_t
adapter_info_get_1_svc(cl_query_input_t *input,
    cl_query_adapter_output_t *output, struct svc_req *rqstp)
{
	scconf_cfg_cluster_t *pcluster_cfg = NULL;
	scconf_errno_t scconf_error = SCCONF_NOERR;
	scconf_cfg_node_t *node_list = NULL;
	scconf_cfg_cltr_adap_t *adapter_list = NULL;
	scconf_cfg_prop_t *prop_list = NULL;
	cl_query_error_t error = CL_QUERY_OK;
	int res = 0;
	uint_t i = 0, adap_count = 0;
	cl_query_adapter_prop_t res_list = NULL;
	scconf_cfg_port_t *port_list = NULL;
	char *input_name_to_use = NULL;

	/*
	 * Check for null input params
	 */
	if (output == NULL || input == NULL) {
		error = CL_QUERY_EINVAL;
	}


	if ((input->name == NULL) || (strlen(input->name) == 0)) {
		input_name_to_use = NULL;
	} else {
		input_name_to_use = input->name;
	}

	/*
	 * Initailize output param
	 */
	output->error = CL_QUERY_OK;
	output->adapter_list.adapter_list_len = 0;
	output->adapter_list.adapter_list_val = NULL;

	/*
	 * Authenticate the client request
	 */
	error = authenticate_request();

	/*
	 * Get the cluster configuration information
	 */
	if (error == CL_QUERY_OK) {
		scconf_error = scconf_get_clusterconfig(&pcluster_cfg);
		error = scconf_to_clquery(scconf_error);
	}

	/*
	 * Get the number of adapters in the cluster
	 */
	if (error == CL_QUERY_OK) {
		node_list = pcluster_cfg->scconf_cluster_nodelist;

		/*
		 * For each node in the cluster
		 */
		for (; node_list != NULL;
		    node_list = node_list->scconf_node_next) {
			adapter_list = node_list->scconf_node_adapterlist;
			for (; adapter_list; adapter_list = adapter_list->
			    scconf_adap_next) {

				/*
				 * If we are querying for a specific adapter
				 */
				if (input_name_to_use != NULL) {
					res = strcmp(input_name_to_use,
					    adapter_list->
					    scconf_adap_adaptername);
					if (res != 0) {
						continue;
					}
				}
				adap_count++;
			}
		}
	}

	/*
	 * If no entry if found return
	 */
	if (adap_count == 0) {
		error = CL_QUERY_ENOTCONFIGURED;
	}

	/*
	 * Allocate memory for the adapter list
	 */
	if (error == CL_QUERY_OK) {
		output->adapter_list.adapter_list_len = adap_count;
		node_list = pcluster_cfg->scconf_cluster_nodelist;

		output->adapter_list.adapter_list_val =
		    (cl_query_adapter_prop_t *)
		    calloc(adap_count, sizeof (cl_query_adapter_prop_t));

		if (output->adapter_list.adapter_list_val == NULL) {
			error = CL_QUERY_ENOMEM;
		}

		/*
		 * allocate memory for each list element
		 */
		for (i = 0; i < adap_count; i++) {
			output->adapter_list.adapter_list_val[i] =
			    (struct cl_query_adapter_prop *)
			    calloc(1, sizeof (struct cl_query_adapter_prop));
			if (output->adapter_list.adapter_list_val[i] == NULL) {
				error = CL_QUERY_ENOMEM;
				break;
			}
		}
	}
	if (error == CL_QUERY_OK) {
		/*
		 * Iterate through the list of nodes and retrieve info of each
		 * adapter
		 */
		for (
			i = 0;
			node_list;
			node_list = node_list->scconf_node_next) {
			/*
			 * Check for any previous erros
			 */
			if (error != CL_QUERY_OK) {
				break;
			}

			/*
			 * reset the adapter pointer
			 */
			adapter_list = node_list->scconf_node_adapterlist;

			for (; adapter_list;
			    adapter_list = adapter_list->scconf_adap_next) {
				/*
				 * Check if we querying for a specific adapter
				 */
				if (input_name_to_use != NULL) {
					res = strcmp(input_name_to_use,
					    adapter_list->
					    scconf_adap_adaptername);
					if (res != 0) {
						continue;
					}
				}

				/*
				 * Allocate memory for the result structure
				 */
				res_list =
				    output->adapter_list.adapter_list_val[i];

				/*
				 * Make sure all the char pointers are set to
				 * NULL, to avoid RPC errors
				 */
				res_list->ip_addr = NULL;
				res_list->netmask = NULL;
				res_list->node_name = NULL;
				res_list->adapter_name = NULL;
				res_list->device_name = NULL;
				res_list->type = NULL;

				/*
				 * Set the adapter ID
				 */
				res_list->adapter_id =
				    adapter_list->scconf_adap_adapterid;

				/*
				 * Get the transport type
				 */
				if (adapter_list->scconf_adap_cltrtype !=
				    NULL) {
					res_list->type =
					    strdup(adapter_list->
					    scconf_adap_cltrtype);
					if (res_list->type == NULL) {
						error = CL_QUERY_ENOMEM;
						break;
					}
				}

				/*
				 * Get the adapter name
				 */
				if (adapter_list->scconf_adap_adaptername !=
				    NULL) {
					res_list->adapter_name =
					    strdup(adapter_list->
					    scconf_adap_adaptername);
					if (res_list->adapter_name == NULL) {
						error = CL_QUERY_ENOMEM;
						break;
					}
				}


				/*
				 * Get the node name
				 */
				if (node_list->scconf_node_nodename != NULL) {
					res_list->node_name = strdup(node_list->
					    scconf_node_nodename);
					if (res_list->node_name == NULL) {
						error = CL_QUERY_ENOMEM;
						break;
					}
				}

				/*
				 * Get the adapter port list
				 */
				port_list = adapter_list->scconf_adap_portlist;
				error =
				    get_port_list(port_list,
				    &(res_list->port_list));

				/*
				 * Get the status of the adapter
				 */
				res_list->state =
				    convert_scconf_status(adapter_list->
				    scconf_adap_adapterstate);

				/*
				 * Get the adapter properties
				 */
				prop_list =
				    adapter_list->scconf_adap_propertylist;
				error =
				    get_adapter_property(prop_list, res_list);

				/*
				 * Increment the index
				 */
				i++;
			}	/* for loop for the adapter list */
		}		/* for loop for node list */
	}
	if (pcluster_cfg != NULL) {
		scconf_free_clusterconfig(pcluster_cfg);
		pcluster_cfg = NULL;
	}

	if (error != CL_QUERY_OK) {
		(void) clquery_free_adapter_result(output);
	}

	output->error = error;
	return (TRUE);
}


/*
 * DESCRIPTION:
 *    This method is the server part of the JUNCTION_INFO_GET Api
 *    query. Information of all the junctions in the cluster is
 *    retrieved from libscconf.
 *
 * INPUT:
 *    cl_query_input_t:  Name of the junction or NULL(default: all junctions)
 *    struct svc_req  :  RPC request handler
 *
 * OUTPUT:
 *    cl_query_junction_output_t: Junction information
 *    error is returned in output.error
 *
 * RETURN:
 *    boolean:  TRUE for success
 *              FALSE on any error
 *
 * SIDE EFFECTS:
 *    None
 *
 * LOCKS:
 */

/* ARGSUSED */
bool_t
junction_info_get_1_svc(cl_query_input_t *input,
    cl_query_junction_output_t *output, struct svc_req *rqstp)
{
	scconf_cfg_cluster_t *pcluster_cfg = NULL;
	scconf_errno_t scconf_rval = SCCONF_NOERR;
	cl_query_error_t error = CL_QUERY_OK;
	uint_t junction_count = 0;
	scconf_cfg_cpoint_t *junc = NULL;
	uint_t j_index = 0;
	uint_t i = 0;
	int res = 0;
	cl_query_junction_prop_t tmp_list = NULL;
	scconf_cfg_port_t *portlist = NULL;
	char *input_name_to_use = NULL;

	/*
	 * validate input parameters
	 */
	if (input == NULL || output == NULL) {
		return (CL_QUERY_EINVAL);
	}

	if ((input->name == NULL) || (strlen(input->name) == 0)) {
		input_name_to_use = NULL;
	} else {
		input_name_to_use = input->name;
	}

	/*
	 * Initialize output parameter
	 */
	output->error = CL_QUERY_OK;
	output->junction_list.junction_list_len = 0;
	output->junction_list.junction_list_val = 0;

	/*
	 * Validate the client request
	 */
	error = authenticate_request();

	/*
	 * Get the cluster information fro libscconf
	 */
	if (error == CL_QUERY_OK) {
		scconf_rval = scconf_get_clusterconfig(&pcluster_cfg);
		error = scconf_to_clquery(scconf_rval);
	}

	/*
	 * Get the number of cables in the cluster
	 */
	if (error == CL_QUERY_OK) {
		junc = pcluster_cfg->scconf_cluster_cpointlist;
		for (; junc; junc = junc->scconf_cpoint_next) {
			if (input_name_to_use != NULL &&
			    junc->scconf_cpoint_cpointname != NULL) {
				res =
				    strcmp(input_name_to_use,
				    junc->scconf_cpoint_cpointname);
				if (res != 0) {
					continue;
				}
			}
			junction_count++;
		}
	}

	if ((error == CL_QUERY_OK) && (junction_count == 0)) {
		error = CL_QUERY_ENOTCONFIGURED;
	}

	if (error == CL_QUERY_OK) {
		/*
		 * Set the junction count in output param
		 */
		output->junction_list.junction_list_len = junction_count;

		/*
		 * Allocate memory for the junction list
		 */
		output->junction_list.junction_list_val =
		    (cl_query_junction_prop_t *)
		    calloc(junction_count, sizeof (cl_query_junction_prop_t));
		if (output->junction_list.junction_list_val == NULL) {
			error = CL_QUERY_ENOMEM;
		}

		/*
		 * Allocate memory for each list element
		 */
		for (i = 0; i < junction_count; i++) {
			output->junction_list.junction_list_val[i] =
			    (struct cl_query_junction_prop *)
			    calloc(1, sizeof (struct cl_query_junction_prop));

			if (output->junction_list.junction_list_val[i] ==
			    NULL) {
				error = CL_QUERY_ENOMEM;
				break;
			}
		}
	}
	if (error == CL_QUERY_OK) {
		/*
		 * reset the junction pointer
		 */
		junc = pcluster_cfg->scconf_cluster_cpointlist;

		/*
		 * iterate through the list of junction pointers to get the
		 * information of each junction
		 */
		for (; junc; junc = junc->scconf_cpoint_next) {
			/*
			 * Check for previous errors
			 */

			if (error != CL_QUERY_OK) {
				break;
			}

			/*
			 * Check if we are querying for a specific cable
			 */
			if (input_name_to_use != NULL &&
			    junc->scconf_cpoint_cpointname != NULL) {
				res =
				    strcmp(input_name_to_use,
				    junc->scconf_cpoint_cpointname);
				if (res != 0) {
					continue;
				}
			}

			tmp_list =
			    output->junction_list.junction_list_val[j_index];

			tmp_list->junction_id = junc->scconf_cpoint_cpointid;

			/*
			 * get the junction name
			 */
			if (junc->scconf_cpoint_cpointname != NULL) {
				tmp_list->junction_name = strdup(junc->
				    scconf_cpoint_cpointname);
				if (tmp_list->junction_name == NULL) {
					error = CL_QUERY_ENOMEM;
					break;
				}
			} else {
				tmp_list->junction_name = NULL;
			}

			if (junc->scconf_cpoint_type != NULL) {
				tmp_list->junction_type =
				    strdup(junc->scconf_cpoint_type);
				if (tmp_list->junction_type == NULL) {
					error = CL_QUERY_ENOMEM;
					break;
				}
			} else {
				tmp_list->junction_type = NULL;
			}

			if (junc->scconf_cpoint_iname != NULL) {
				tmp_list->key_name =
				    strdup(junc->scconf_cpoint_iname);
				if (tmp_list->key_name == NULL) {
					error = CL_QUERY_ENOMEM;
					break;
				}
			} else {
				tmp_list->key_name = NULL;
			}

			/*
			 * Get the status of the junction
			 */
			tmp_list->status =
			    (uint_t)convert_scconf_status(junc->
			    scconf_cpoint_cpointstate);


			/*
			 * Get the port list
			 */
			if (junc->scconf_cpoint_portlist != NULL) {
				portlist = junc->scconf_cpoint_portlist;

				(void) get_port_list(portlist,
				    &(tmp_list->port_list));
			}

			j_index++;
		}
	}
	if (pcluster_cfg != NULL) {
		scconf_free_clusterconfig(pcluster_cfg);
		pcluster_cfg = NULL;
	}

	if (error != CL_QUERY_OK) {
		(void) clquery_free_junction_result(output);
	}

	output->error = error;
	return (TRUE);
}



/*
 * DESCRIPTION:
 *    This method is the server part of the CABLE_INFO_GET Api
 *    query. Information of all the cables in the cluster is
 *    retrieved from libscconf.
 *
 * INPUT:
 *    cl_query_input_t:  Name of the cable or NULL(default: all cables)
 *    struct svc_req  :  RPC request handler
 *
 * OUTPUT:
 *    cl_query_cable_output_t: Cable information
 *    error is returned in output.error
 *
 * RETURN:
 *    boolean:  TRUE for success
 *              FALSE on any error
 *
 * SIDE EFFECTS:
 *    None
 *
 * LOCKS:
 */

/* ARGSUSED */
bool_t
cable_info_get_1_svc(cl_query_input_t *input,
    cl_query_cable_output_t *output, struct svc_req *rqstp)
{

	scconf_cfg_cluster_t *pcluster_cfg = NULL;
	scconf_errno_t scconf_rval = SCCONF_NOERR;
	cl_query_error_t error = CL_QUERY_OK;
	uint_t cable_count = 0;
	scconf_cfg_cable_t *cable_list = NULL;
	uint_t c_index = 0, i = 0;
	int res = 0;
	cl_query_cable_prop_t tmp_list = NULL;
	char *input_name_to_use = NULL;

	/*
	 * check input params
	 */
	if (input == NULL || output == NULL) {
		return (CL_QUERY_EINVAL);
	}


	if ((input->name == NULL) || (strlen(input->name) == 0)) {
		input_name_to_use = NULL;
	} else {
		input_name_to_use = input->name;
	}


	/*
	 * Initialize output parameter
	 */
	output->error = CL_QUERY_OK;
	output->cable_list.cable_list_len = 0;
	output->cable_list.cable_list_val = NULL;

	/*
	 * Validate the client request
	 */
	error = authenticate_request();

	/*
	 * Get the cluster configuration from libscconf
	 */
	if (error == CL_QUERY_OK) {
		scconf_rval = scconf_get_clusterconfig(&pcluster_cfg);
		error = scconf_to_clquery(scconf_rval);
	}

	/*
	 * Get the number of cables in the cluster
	 */
	if (error == CL_QUERY_OK) {
		cable_list = pcluster_cfg->scconf_cluster_cablelist;
		for (; cable_list; cable_list = cable_list->scconf_cable_next) {
			if (input_name_to_use != NULL) {
				res =
				    strcmp(input_name_to_use,
				    cable_list->scconf_cable_iname);
				if (res != 0) {
					continue;
				}
			}
			cable_count++;
		}
	}

	/*
	 * Found no entry
	 */
	if (error == CL_QUERY_OK && cable_count == 0) {
		error = CL_QUERY_ENOTCONFIGURED;
	}

	if (error == CL_QUERY_OK) {
		output->cable_list.cable_list_len = cable_count;

		/*
		 * Allocate memory for the result structure
		 */
		output->cable_list.cable_list_val = (cl_query_cable_prop_t *)
		    calloc(cable_count, sizeof (cl_query_cable_prop_t));

		if (output->cable_list.cable_list_val == NULL) {
			error = CL_QUERY_ENOMEM;
		}

		/*
		 * Allocate memory for each list element
		 */
		for (i = 0; i < cable_count; i++) {
			output->cable_list.cable_list_val[i] =
			    (struct cl_query_cable_prop *)
			    calloc(1, sizeof (struct cl_query_cable_prop));

			if (output->cable_list.cable_list_val[i] == NULL) {
				error = CL_QUERY_ENOMEM;
				break;
			}
		}
	}

	/*
	 * reset the pointer
	 */
	cable_list = pcluster_cfg->scconf_cluster_cablelist;

	/*
	 * Iterate through the cable list and get the information
	 */
	for (; cable_list; cable_list = cable_list->scconf_cable_next) {
		/*
		 * Check for prvious error
		 */
		if (error != CL_QUERY_OK) {
			break;
		}

		/*
		 * Check if we are querying for a specific cable property
		 */
		if (input_name_to_use != NULL) {
			res =
			    strcmp(input_name_to_use,
			    cable_list->scconf_cable_iname);
			if (res != 0) {
				continue;
			}
		}

		tmp_list = output->cable_list.cable_list_val[c_index];

		/*
		 * Set all pointers to NULL
		 */
		tmp_list->cable_name = NULL;
		tmp_list->cable_epoint1_name = NULL;
		tmp_list->cable_epoint2_name = NULL;

		/*
		 * Get the name of the cable
		 */
		if (cable_list->scconf_cable_iname != NULL) {
			tmp_list->cable_name =
			    (char *)strdup(cable_list->scconf_cable_iname);
			if (tmp_list->cable_name == NULL) {
				error = CL_QUERY_ENOMEM;
				break;
			}
		} else {
			tmp_list->cable_name = NULL;
		}

		/*
		 * Get the cable id
		 */
		tmp_list->cable_id = cable_list->scconf_cable_cableid;

		/*
		 * Get the status of the cable
		 */
		tmp_list->state = (uint_t)convert_scconf_status(cable_list->
		    scconf_cable_cablestate);

		/*
		 * Get the type of the cable endpoints
		 */
		tmp_list->cable_epoint1_type =
		    (uint_t)convert_scconf_epoint(cable_list->
		    scconf_cable_epoint1.scconf_cltr_epoint_type);

		tmp_list->cable_epoint2_type =
		    (uint_t)convert_scconf_epoint(cable_list->
		    scconf_cable_epoint2.scconf_cltr_epoint_type);


		/*
		 * Get the name of the cable endpoints
		 */
		if (cable_list->scconf_cable_epoint1.
		    scconf_cltr_epoint_devicename != NULL) {

			tmp_list->cable_epoint1_name =
			    (char *)calloc(BUFSIZ, 1);
			if (tmp_list->cable_epoint1_name == NULL) {
				error = CL_QUERY_ENOMEM;
				break;
			}
			if ((cable_list->scconf_cable_epoint1.
				scconf_cltr_epoint_type ==
				SCCONF_CLTR_EPOINT_TYPE_ADAPTER) &&
			    (cable_list->scconf_cable_epoint1.
				scconf_cltr_epoint_nodename != NULL)) {
				(void) sprintf(tmp_list->cable_epoint1_name,
				    "%s:",
				    cable_list->scconf_cable_epoint1.
				    scconf_cltr_epoint_nodename);
			}

			if (cable_list->scconf_cable_epoint1.
			    scconf_cltr_epoint_devicename != NULL) {

				(void) strcat(tmp_list->cable_epoint1_name,
				    cable_list->scconf_cable_epoint1.
				    scconf_cltr_epoint_devicename);
			}
			/*
			 * add the port name
			 */
			if (cable_list->scconf_cable_epoint1.
			    scconf_cltr_epoint_portname != NULL) {
				(void) strcat(
					tmp_list->cable_epoint1_name,
					    "@");
				(void) strcat(tmp_list->cable_epoint1_name,
				    cable_list->scconf_cable_epoint1.
				    scconf_cltr_epoint_portname);
			}


		} else {
			tmp_list->cable_epoint1_name = NULL;
		}


		/*
		 * Get the name of the cable endpoints
		 */
		if (cable_list->scconf_cable_epoint2.
		    scconf_cltr_epoint_devicename != NULL) {

			tmp_list->cable_epoint2_name =
			    (char *)calloc(BUFSIZ, 1);
			if (tmp_list->cable_epoint2_name == NULL) {
				error = CL_QUERY_ENOMEM;
				break;
			}
			if ((cable_list->scconf_cable_epoint2.
				scconf_cltr_epoint_type ==
				SCCONF_CLTR_EPOINT_TYPE_ADAPTER) &&
			    (cable_list->scconf_cable_epoint2.
				scconf_cltr_epoint_nodename != NULL)) {
				(void) sprintf(tmp_list->cable_epoint2_name,
				    "%s:",
				    cable_list->scconf_cable_epoint2.
				    scconf_cltr_epoint_nodename);
			}

			if (cable_list->scconf_cable_epoint2.
			    scconf_cltr_epoint_devicename != NULL) {

				(void) strcat(tmp_list->cable_epoint2_name,
				    cable_list->scconf_cable_epoint2.
				    scconf_cltr_epoint_devicename);
			}
			/*
			 * add the port name
			 */
			if (cable_list->scconf_cable_epoint2.
			    scconf_cltr_epoint_portname != NULL) {
				(void) strcat(
					tmp_list->cable_epoint2_name,
					    "@");
				(void) strcat(tmp_list->cable_epoint2_name,
				    cable_list->scconf_cable_epoint2.
				    scconf_cltr_epoint_portname);
			}
		} else {
			tmp_list->cable_epoint2_name = NULL;
		}

		c_index++;
	}

	if (pcluster_cfg != NULL) {
		scconf_free_clusterconfig(pcluster_cfg);
		pcluster_cfg = NULL;
	}

	if (error != CL_QUERY_OK) {
		/*
		 * cleanup
		 */
		(void) clquery_free_cable_result(output);
	}

	output->error = error;
	return (TRUE);
}


cl_query_error_t
get_adapter_property(scconf_cfg_prop_t *prop_list,
    cl_query_adapter_prop_t res_list)
{
	cl_query_error_t error = CL_QUERY_OK;
	char key[1024], value[1024];

	if (prop_list == NULL || res_list == NULL) {
		return (CL_QUERY_EUNEXPECTED);
	}

	/*
	 * Get all the adapter properties
	 */
	for (; prop_list; prop_list = prop_list->scconf_prop_next) {

		/*
		 * Check if the property list is NULL
		 */
		if (prop_list->scconf_prop_key == NULL ||
		    prop_list->scconf_prop_value == NULL) {
			continue;
		}

		(void) strcpy(key, prop_list->scconf_prop_key);
		(void) strcpy(value, prop_list->scconf_prop_value);

		if (strcmp(key, "device_instance") == 0) {
			res_list->device_instance = (uint_t)atoi(value);

		} else if (strcmp(key, "device_name") == 0) {
			res_list->device_name = strdup(value);

		} else if (strcmp(key, "dlpi_heartbeat_timeout") == 0) {
			res_list->dlpi_heartbeat_timeout = (uint_t)atoi(value);

		} else if (strcmp(key, "dlpi_heartbeat_quantum") == 0) {
			res_list->dlpi_heartbeat_quantum = (uint_t)atoi(value);

		} else if (strcmp(key, "ip_address") == 0) {
			res_list->ip_addr = strdup(value);
			if (res_list->ip_addr == NULL) {
				error = CL_QUERY_ENOMEM;
				break;
			}
		} else if (strcmp(key, "netmask") == 0) {
			res_list->netmask = strdup(value);
			if (res_list->netmask == NULL) {
				error = CL_QUERY_ENOMEM;
				break;
			}
		} else if (strcmp(key, "nw_bandwidth") == 0) {
			res_list->network_bandwidth = (uint_t)atoi(value);

		} else if (strcmp(key, "lazy_free") == 0) {
			res_list->lazy_free = (uint_t)atoi(value);

		} else if (strcmp(key, "bandwidth") == 0) {
			res_list->bandwidth = (uint_t)atoi(value);

		} else if (strcmp(key, "vlan_id") == 0) {
			res_list->vlan_id = (uint_t)atoi(value);

		}
	}	/* end of while prop_list */

	return (error);
}


cl_query_error_t
get_port_list(scconf_cfg_port_t *portlist,
    cl_query_port_prop_list_t *dest_list)
{
	cl_query_error_t res = CL_QUERY_OK;
	scconf_cfg_port_t *tmp_ptr = NULL;
	uint_t i = 0, port_count = 0;
	cl_query_port_prop_elem_t *tmp_list;

	/*
	 * Store the pointer
	 */
	tmp_ptr = portlist;

	if ((portlist == NULL)) {
		return (CL_QUERY_EUNEXPECTED);
	}

	/*
	 * Get the ports count to allocate memory
	 */
	for (; portlist; portlist = portlist->scconf_port_next) {
		port_count++;
	}

	if (port_count > 0) {
		dest_list->prop_list.prop_list_len = port_count;
		/*
		 * Allocate memory for the portlist in the result
		 */
		(dest_list->prop_list).prop_list_val =
		    (cl_query_port_prop_elem_t *)calloc(port_count,
		    sizeof (cl_query_port_prop_elem_t));
		/*
		 * XXX TODO check allocation
		 */
		tmp_list = dest_list->prop_list.prop_list_val;
	} else {
		dest_list->prop_list.prop_list_len = 0;
		dest_list->prop_list.prop_list_val = NULL;
		return (CL_QUERY_OK);
	}

	/*
	 * Get the name of the ports
	 */
	portlist = tmp_ptr;

	for (i = 0; portlist; portlist = portlist->scconf_port_next) {
		if (portlist->scconf_port_iname != NULL) {
			tmp_list[i] =
			    (cl_query_port_prop_elem_t)calloc(1,
			    sizeof (cl_query_port_prop_elem));
			if (tmp_list[i] == NULL) {
				/*
				 * XXX TODO free already allocated element
				 */
				return (CL_QUERY_ENOMEM);
			}
			tmp_list[i]->port_id = portlist->scconf_port_portid;
			/*
			 * XXX TODO define clquery port state type to do a
			 * mapping with scconf port state
			 */
			tmp_list[i]->port_state =
			    (uint_t)sccconf_port_status_to_clquery(portlist->
			    scconf_port_portstate);
			tmp_list[i]->port_name =
			    (char *)strdup(portlist->scconf_port_portname);

			if (tmp_list[i]->port_name == NULL) {
				return (CL_QUERY_ENOMEM);
			}
		}

		/*
		 * increment the index
		 */
		i++;
	}

	return (res);
}

static cl_query_error_t
scstat_path_dup(cl_query_transportpath_prop_t dest, scstat_path_t *scpath_ptr)
{

	if (scpath_ptr->scstat_path_name != NULL) {
		dest->transportpath_name =
		    (char *)strdup(scpath_ptr->scstat_path_name);
		if (dest->transportpath_name == NULL) {
			return (CL_QUERY_ENOMEM);
		}
	}

	dest->status =
	    (uint_t)scstate_to_clquery_state(scpath_ptr->scstat_path_status);

	return (CL_QUERY_OK);
}

/*
 *
 */

static cl_query_error_t
clquery_get_adpname_byid(clconf_obj_t *node,
    const char *nodename, int adp_id, char **adpname)
{
	clconf_iter_t *adp_iter = NULL;
	clconf_obj_t *adapter = NULL;
	const char *char_prop = NULL;
	size_t slength;

	*adpname = NULL;

	adp_iter = clconf_node_get_adapters((clconf_node_t *)node);

	if (adp_iter == NULL) {
		return (CL_QUERY_EUNEXPECTED);
	}
	while ((adapter = clconf_iter_get_current(adp_iter)) != NULL) {
		if (clconf_obj_get_id(adapter) == adp_id) {
			char_prop = clconf_obj_get_name(adapter);
			break;
		}
		clconf_iter_advance(adp_iter);
	}

	if (adp_iter != NULL) {
		clconf_iter_release(adp_iter);
	}

	if (char_prop == NULL) {
		return (CL_QUERY_EUNEXPECTED);
	}

	slength = strlen(nodename) + strlen(char_prop) + 2;

	*adpname = (char *)malloc(slength);
	if (*adpname == NULL) {
		return (CL_QUERY_ENOMEM);
	}

	(void) sprintf(*adpname, "%s:%s", nodename, char_prop);

	return (CL_QUERY_OK);

}

static cl_query_error_t
clquery_fill_endpoints(cl_query_transportpath_prop_t tmp)
{

	char *tpath_name = NULL;
	char *start1 = NULL;
	char *start2 = NULL;
	nodeid_t nid1;
	int aid1;
	nodeid_t nid2;
	int aid2;
	cl_query_error_t error;
	clconf_cluster_t *clcluster = NULL;
	clconf_iter_t *node_iter = NULL;
	clconf_obj_t *node = NULL;
	char *bufcharjustforlint1 = NULL;
	char *bufcharjustforlint2 = NULL;
	const char *nodename = NULL;
	size_t _length;
	short nb_find;
	int cidx;

	clcluster = clconf_cluster_get_current();
	if (clcluster == NULL) {
		return (CL_QUERY_EUNEXPECTED);
	}

	tpath_name = tmp->transportpath_name;

	/*
	 * get local and remote node id
	 */

	start1 = strstr(tpath_name, ".nodes.");
	if (start1 == NULL) {
		return (CL_QUERY_EUNEXPECTED);
	}
	start1 += strlen(".nodes.");
	nid1 = (nodeid_t)atoi(start1);

	start1 = strstr(start1, ".nodes.");
	if (start1 == NULL) {
		return (CL_QUERY_EUNEXPECTED);
	}
	start1 += strlen(".nodes.");
	nid2 = (nodeid_t)atoi(start1);

	/*
	 * get local and remote adpater id
	 */

	start1 = strstr(tpath_name, ".adapters.");
	if (start1 == NULL) {
		return (CL_QUERY_EUNEXPECTED);
	}
	start1 += strlen(".adapters.");
	aid1 = (int)atoi(start1);

	start1 = strstr(start1, ".adapters.");
	if (start1 == NULL) {
		return (CL_QUERY_EUNEXPECTED);
	}

	start1 += strlen(".adapters.");
	aid2 = (int)atoi(start1);


	error = CL_QUERY_OK;

	node_iter = clconf_cluster_get_nodes(clcluster);
	if (node_iter == NULL) {
		error = CL_QUERY_EUNEXPECTED;
	}
	if (error == CL_QUERY_OK) {
		bufcharjustforlint1 = NULL;
		bufcharjustforlint2 = NULL;
		nb_find = 2;
		while ((node = clconf_iter_get_current(node_iter)) != NULL) {

			if (clconf_obj_get_id(node) == (int)nid1) {
				nodename = clconf_obj_get_name(node);
				error = clquery_get_adpname_byid(node,
				    nodename, aid1, &(bufcharjustforlint1));
				nb_find--;
				if (nb_find <= 0)
					break;
			}
			if (clconf_obj_get_id(node) == (int)nid2) {
				nodename = clconf_obj_get_name(node);
				error = clquery_get_adpname_byid(node,
				    nodename, aid2, &(bufcharjustforlint2));
				nb_find--;
				if (nb_find <= 0)
					break;
				break;
			}

			clconf_iter_advance(node_iter);
		}
	}


	/*
	 * don't check for 'error' value
	 */
	/*
	 * anyway we need a reference to free allocations
	 */
	if (bufcharjustforlint1 != NULL) {
		tmp->transportpath_endpoint_1_strname = bufcharjustforlint1;
	}

	if (bufcharjustforlint2 != NULL) {
		tmp->transportpath_endpoint_2_strname = bufcharjustforlint2;
	}

	if (node_iter != NULL) {
		clconf_iter_release(node_iter);
	}

	if (clcluster != NULL) {
		clconf_obj_release((clconf_obj_t *)clcluster);
	}

	/*
	 * get path real names
	 */
	if (error == CL_QUERY_OK) {
		/*
		 * don't check for erros
		 */
		/*
		 * if we are here tpath_name has a good pattern
		 */
		/*lint -e668 */
		start1 = strstr(tpath_name, "nodes.");
		start2 = strstr(start1, ".nodes.");
		_length = ((unsigned int)start2 - (unsigned int)start1 + 1);
		/*lint +e668 */
		tmp->transportpath_endpoint_1_name = (char *)malloc(_length);
		if (tmp->transportpath_endpoint_1_name == NULL) {
			error = CL_QUERY_ENOMEM;
		} else {
			cidx = 0;
			while (start1 != start2) {
				tmp->transportpath_endpoint_1_name[cidx++] =
				    *start1;
				start1++;
			}
			tmp->transportpath_endpoint_1_name[cidx] = '\0';
		}
	}

	if (error == CL_QUERY_OK) {
		/*
		 * skip '.' at the start
		 */
		start2++;
		tmp->transportpath_endpoint_2_name = (char *)strdup(start2);
		if (tmp->transportpath_endpoint_2_name == NULL) {
			error = CL_QUERY_ENOMEM;
		}
	}


	return (error);


}

/*lint -e715 */
bool_t
transport_path_info_get_1_svc(cl_query_input_t *input,
    cl_query_transportpath_output_t *output, struct svc_req *rqstp)
{

	char *input_name_to_use = NULL;

	scstat_transport_t *transport = NULL;
	scstat_path_t *scpath_ptr = NULL;
	scstat_errno_t scstat_cr = SCSTAT_ENOERR;

	cl_query_error_t error = CL_QUERY_OK;
	unsigned int t_count;
	unsigned int t_idx;
	cl_query_transportpath_prop_t *tmp = NULL;

	if ((input->name == NULL) || (strlen(input->name) == 0)) {
		input_name_to_use = NULL;
	} else {
		input_name_to_use = input->name;
	}

	output->error = CL_QUERY_OK;
	output->transportpath_list.transportpath_list_len = 0;
	output->transportpath_list.transportpath_list_val = NULL;

	/*
	 * Validate the client request
	 */
	error = authenticate_request();

	if (error == CL_QUERY_OK) {
		transport = NULL;
		scstat_cr = scstat_get_transport(&transport);
		if ((scstat_cr != SCSTAT_ENOERR) || (transport == NULL)) {
			error = CL_QUERY_EUNEXPECTED;
		}
	}

	t_count = 0;

	if (error == CL_QUERY_OK) {
		scpath_ptr = transport->scstat_path_list;
		while (scpath_ptr != NULL) {
			if ((input_name_to_use == NULL) ||
			    (strcmp(input_name_to_use,
				scpath_ptr->scstat_path_name) == 0)) {
				t_count++;
			}
			scpath_ptr = scpath_ptr->scstat_path_next;
		}
	}

	if ((error == CL_QUERY_OK) && (t_count == 0)) {
		error = CL_QUERY_ENOTCONFIGURED;
	}

	if (error == CL_QUERY_OK) {
		output->transportpath_list.transportpath_list_len = t_count;
		output->transportpath_list.transportpath_list_val =
		    (cl_query_transportpath_prop_t *)calloc(t_count,
		    sizeof (cl_query_transportpath_prop_t));

		if (output->transportpath_list.transportpath_list_val == NULL) {
			error = CL_QUERY_ENOMEM;
		}
		tmp = output->transportpath_list.transportpath_list_val;
		for (t_idx = 0; t_idx < t_count; t_idx++) {
			tmp[t_idx] =
			    (cl_query_transportpath_prop_t)calloc(1,
			    sizeof (cl_query_transportpath_prop));
			if (tmp[t_idx] == NULL) {
				error = CL_QUERY_ENOMEM;
				break;
			}
		}
	}

	if (error == CL_QUERY_OK) {
		t_idx = 0;
		scpath_ptr = transport->scstat_path_list;
		while (scpath_ptr != NULL) {
			if ((input_name_to_use == NULL) ||
			    (strcmp(input_name_to_use,
				    scpath_ptr->scstat_path_name) == 0)) {
				error = scstat_path_dup(tmp[t_idx], scpath_ptr);
				if (error != CL_QUERY_OK) {
					break;
				}
				t_idx++;
			}
			scpath_ptr = scpath_ptr->scstat_path_next;
		}
	}

	if (scstat_cr == SCSTAT_ENOERR) {
		scstat_free_transport(transport);
	}

	for (t_idx = 0; t_idx < t_count; t_idx++) {
		error = clquery_fill_endpoints(tmp[t_idx]);
		if (error != CL_QUERY_OK)
			break;
	}

	if (error != CL_QUERY_OK) {
		(void) clquery_free_transportpath_result(output);
	}


	output->error = error;
	return (TRUE);

} /*lint +e715 */
