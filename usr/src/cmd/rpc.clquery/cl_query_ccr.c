/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 **************************************************************************
 *
 * Component = clquery deamon
 *
 * Synopsis  = CCR Access API server code
 *
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 **************************************************************************
 */

#pragma ident	"@(#)cl_query_ccr.c	1.9	08/05/20 SMI"

#include <stdlib.h>
#include <limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <strings.h>

#include <sys/cl_events.h>

#include <rpc/rpc.h>
#include <rpc/xdr.h>
#include <rpc/svc.h>

#include <cl_query/rpc/cl_query.h>
#include <cl_query/cl_query_types.h>
#include "cl_query_ccr_access.h"

static cl_query_error_t free_rpc_buffer(cl_query_ccr_param_t *);

extern cl_query_error_t authenticate_request(void);


/*
 * DESCRIPTION:
 *    This method is the server part of the API TABLE_NAMES_GET.
 *    This returns a list of table names in cluster repository.
 *
 * INPUT:
 *    None.
 *
 * OUTPUT:
 *    ccr_table_list_t: List of CCR Table names
 *
 * RETURN:
 *    boolean:  TRUE for success
 *              FALSE on any error
 *
 * SIDE EFFECTS:
 *    None
 *
 * LOCKS:
 */

/* ARGSUSED */
bool_t
table_names_get_1_svc(cl_query_input_t *argp, ccr_table_list_t *result,
    struct svc_req *rqstp)
{
	cl_query_error_t res = CL_QUERY_OK;

	/*
	 * Check the input parameters
	 */
	if (result == NULL) {
		res = CL_QUERY_EINVAL;
	}

	if (res == CL_QUERY_OK) {
		/*
		 * Authenticate request
		 */
		res = authenticate_request();
	}

	if (res == CL_QUERY_OK) {
		/*
		 * Get the table names from ccr deamon
		 */
		res = table_names_get(argp->name,
		    &result->table_list.strarr_list_len,
		    &result->table_list.strarr_list_val);
	}

	/*
	 * Check if the list was returned
	 */
	if (res == CL_QUERY_OK) {
		if (result->table_list.strarr_list_len == 0 ||
		    result->table_list.strarr_list_val == NULL) {
			res = CL_QUERY_ENOENT;
		}
	}

	result->error = res;
	return (TRUE);
}


/*
 * DESCRIPTION:
 *    This method is the server part of the API TABLE_LEN_GET.
 *    This returns the size of the CCR table specified.
 *
 * INPUT:
 *    cl_query_input_t: Name of the CCR table.
 *
 * OUTPUT:
 *    ccr_table_size_t: Size of the table.
 *
 * RETURN:
 *    boolean:  TRUE for success
 *              FALSE on any error
 *
 * SIDE EFFECTS:
 *    None
 *
 * LOCKS:
 */

/* ARGSUSED */
bool_t
table_len_get_1_svc(cl_query_input_t *argp, ccr_table_size_t *result,
    struct svc_req *rqstp)
{
	cl_query_error_t res = CL_QUERY_OK;

	/*
	 * Check the input parameters
	 */
	if (argp == NULL || result == NULL) {
		res = CL_QUERY_EINVAL;
	}

	if (res == CL_QUERY_OK) {
		/*
		 * Authenticate request
		 */
		res = authenticate_request();
	}

	if (res == CL_QUERY_OK) {
		/*
		 * Get the table size from ccr deamon
		 */
		res = table_len_get(argp->name, &result->table_len);
		if (res != CL_QUERY_OK) {
			result->table_len = 0;
		}
	}

	result->error = res;
	return (TRUE);
}


/*
 * DESCRIPTION:
 *    This method is the server part of the API READ_TABLE.
 *    This returns the contents of the CCR table.
 *
 * INPUT:
 *    cl_query_input_t: name of the CCR Table.
 *
 * OUTPUT:
 *    cl_query_ccr_param_t: Contents of the table.
 *
 * RETURN:
 *    boolean:  TRUE for success
 *              FALSE on any error
 *
 * SIDE EFFECTS:
 *    None
 *
 * LOCKS:
 */

/* ARGSUSED */
bool_t
read_table_1_svc(cl_query_input_t *argp, cl_query_ccr_param_t *result,
    struct svc_req *rqstp)
{
	cl_query_error_t res = CL_QUERY_OK;

	/*
	 * Check the input parameters
	 */
	if (argp == NULL || result == NULL) {
		res = CL_QUERY_EINVAL;
	}

	if (res == CL_QUERY_OK) {
		/*
		 * Authenticate request
		 */
		res = authenticate_request();
	}

	/*
	 * FIX_ME: Shud a lock be used to prevent read/write from 2/3 threads
	 * in a same process
	 */

	if (res == CL_QUERY_OK) {
		/*
		 * Read the CCR table contents
		 */
		res = read_table(argp->name, result);
	}

	if (res != CL_QUERY_OK) {
		/*
		 * Free any allocated memory
		 */
		(void) free_rpc_buffer(result);

		/*
		 * Initialize RPC buffer
		 */
		result->table_rows.table_rows_len = 0;
		result->table_rows.table_rows_val = NULL;
	}


	/*
	 * Make sure all pointers are valid
	 */
	result->table_name = strdup(argp->name);

	if (result->table_name == NULL) {
		res = CL_QUERY_ENOMEM;
	}

	result->error = res;

	return (TRUE);
}



/*
 * DESCRIPTION:
 *    This method is the server part of the API WRITE_TABLE.
 *    This method updates the table with new contents. All the
 *    previous content will be erased.
 *
 * INPUT:
 *    cl_query_ccr_param_t: table name and contents
 *
 * OUTPUT:
 *    cl_query_output_t: Sucess/Failure error codes
 *
 * RETURN:
 *    boolean:  TRUE for success
 *              FALSE on any error
 *
 * SIDE EFFECTS:
 *    None
 *
 * LOCKS:
 */

/* ARGSUSED */
bool_t
write_table_all_1_svc(cl_query_ccr_param_t *argp, cl_query_output_t *result,
    struct svc_req *rqstp)
{
	cl_query_error_t	res = CL_QUERY_OK;

	/*
	 * Check the input parameters
	 */
	if (argp == NULL || result == NULL) {
		res = CL_QUERY_EINVAL;
	}

	if (res == CL_QUERY_OK) {
		/*
		 * Authenticate request
		 */
		res = authenticate_request();
	}

	if (res == CL_QUERY_OK) {
		res = write_table_all(argp);
	}

	if (res == CL_QUERY_OK) {
		(void) sc_publish_event(ESC_CLUSTER_CONFIG_CHANGE,
		    CL_CCRAD,
		    CL_EVENT_SEV_INFO,
		    CL_EVENT_INIT_SYSTEM, 0, 0, 0,
		    CL_CONFIG_ACTION,
		    SE_DATA_TYPE_UINT32, CL_EVENT_CONFIG_PROP_CHANGED,
		    CL_CCR_TABLE, SE_DATA_TYPE_STRING, argp->table_name, NULL);
	}
	result->error = res;

	return (TRUE);
}


/*
 * DESCRIPTION:
 *    This method is the server part of the API DELETE_TABLE_ALL.
 *    This method deletes the entire table contents.
 *
 * INPUT:
 *    cl_query_input_t: name of the CCR Table.
 *
 * OUTPUT:
 *    cl_query_output_t: Sucess/Failure error codes
 *
 * RETURN:
 *    boolean:  TRUE for success
 *              FALSE on any error
 *
 * SIDE EFFECTS:
 *    None
 *
 * LOCKS:
 */

/* ARGSUSED */
bool_t
delete_table_all_1_svc(cl_query_input_t *argp, cl_query_output_t *result,
    struct svc_req *rqstp)
{
	cl_query_error_t	res = CL_QUERY_OK;

	/*
	 * Check the input parameters
	 */
	if (argp == NULL || result == NULL) {
		res = CL_QUERY_EINVAL;
	}

	if (res == CL_QUERY_OK) {
		/*
		 * Authenticate request
		 */
		res = authenticate_request();
	}

	if (res == CL_QUERY_OK) {
		/*
		 * delete the table
		 */
		res = delete_table_all(argp->name);
	}

	/*
	 * Publish the event
	 */
	if (res == CL_QUERY_OK) {
		(void) sc_publish_event(ESC_CLUSTER_CONFIG_CHANGE,
		    CL_CCRAD,
		    CL_EVENT_SEV_INFO,
		    CL_EVENT_INIT_SYSTEM, 0, 0, 0,
		    CL_CONFIG_ACTION,
		    SE_DATA_TYPE_UINT32, CL_EVENT_CONFIG_PROP_CHANGED,
		    CL_CCR_TABLE, SE_DATA_TYPE_STRING, argp->name, NULL);
	}
	result->error = res;

	return (TRUE);
}


/*
 * DESCRIPTION:
 *    This method is the server part of the API DELETE_TABLE.
 *    This method deletes the table from the repository.
 *
 * INPUT:
 *    cl_query_input_t: name of the CCR Table.
 *
 * OUTPUT:
 *    cl_query_output_t: Sucess/Failure error codes
 *
 * RETURN:
 *    boolean:  TRUE for success
 *              FALSE on any error
 *
 * SIDE EFFECTS:
 *    None
 *
 * LOCKS:
 */

/* ARGSUSED */
bool_t
delete_table_1_svc(cl_query_input_t *argp, cl_query_output_t *result,
    struct svc_req *rqstp)
{
	cl_query_error_t res = CL_QUERY_OK;

	/*
	 * Check the input parameters
	 */
	if (argp == NULL || result == NULL) {
		res = CL_QUERY_EINVAL;
	}

	if (res == CL_QUERY_OK) {
		/*
		 * Authenticate request
		 */
		res = authenticate_request();
	}

	if (res == CL_QUERY_OK) {
		/*
		 * delete the table
		 */
		res = delete_table(argp->name);
	}

	if (res == CL_QUERY_OK) {
		(void) sc_publish_event(ESC_CLUSTER_CONFIG_CHANGE,
		    CL_CCRAD,
		    CL_EVENT_SEV_INFO,
		    CL_EVENT_INIT_SYSTEM, 0, 0, 0,
		    CL_CONFIG_ACTION,
		    SE_DATA_TYPE_UINT32, CL_EVENT_CONFIG_REMOVED,
		    CL_CCR_TABLE, SE_DATA_TYPE_STRING, argp->name, NULL);
	}
	result->error = res;

	return (TRUE);
}

/*
 * DESCRIPTION:
 *    This method is the server part of the API CREATE_TABLE.
 *    This method creates the table in the repository. If any
 *    table contents are provided, the new table will be populated
 *    with those rows.
 *
 * INPUT:
 *    cl_query_ccr_param_t: Table name and contents
 *
 * OUTPUT:
 *    cl_query_output_t: Sucess/Failure error codes
 *
 * RETURN:
 *    boolean:  TRUE for success
 *              FALSE on any error
 *
 * SIDE EFFECTS:
 *    None
 *
 * LOCKS:
 */

/* ARGSUSED */
bool_t
create_table_1_svc(cl_query_ccr_param_t *argp, cl_query_output_t *result,
struct svc_req *rqstp)
{
	cl_query_error_t res = CL_QUERY_OK;

	/*
	 * Check the input parameters
	 */
	if (argp == NULL || result == NULL) {
		res = CL_QUERY_EINVAL;
	}

	if (res == CL_QUERY_OK) {
		/*
		 * Authenticate request
		 */
		res = authenticate_request();
	}

	if (res == CL_QUERY_OK) {
		/*
		 * Create and populate the table
		 */
		res = create_table(argp);
	}

	if (res == CL_QUERY_OK) {
		(void) sc_publish_event(ESC_CLUSTER_CONFIG_CHANGE,
		    CL_CCRAD,
		    CL_EVENT_SEV_INFO,
		    CL_EVENT_INIT_SYSTEM, 0, 0, 0,
		    CL_CONFIG_ACTION,
		    SE_DATA_TYPE_UINT32, CL_EVENT_CONFIG_ADDED,
		    CL_CCR_TABLE, SE_DATA_TYPE_STRING, argp->table_name, NULL);
	}
	result->error = res;

	return (TRUE);
}


/*
 * DESCRIPTION:
 *    Utility routine to free the rpc buffer
 *
 * INPUT:
 *    cl_query_ccr_param_t: table contents
 *
 * OUTPUT:
 *    None.
 *
 * RETURN:
 *    cl_query_error_t: CL_QUERY_OK on success
 *                      CL_QUERY_EXX on error.
 *
 * SIDE EFFECTS:
 *    None
 *
 * LOCKS:
 */

cl_query_error_t
free_rpc_buffer(cl_query_ccr_param_t *result)
{
	uint_t i = 0;

	/*
	 * Nothing to free
	 */
	if (result == NULL) {
		return (CL_QUERY_OK);
	}

	/*
	 * Nothing to free
	 */
	if (result->table_rows.table_rows_val == NULL) {
		return (CL_QUERY_OK);
	}

	for (i = 0; i < result->table_rows.table_rows_len; i++) {
		if (result->table_rows.table_rows_val[i] != NULL) {
			/*
			 * Free the Key buffer
			 */
			if (result->table_rows.table_rows_val[i]->key != NULL) {
				free(result->table_rows.table_rows_val[i]->key);
				result->table_rows.table_rows_val[i]->key =
					NULL;
			}

			/*
			 * Free the Value buffer
			 */
			if (result->table_rows.table_rows_val[i]->value !=
			NULL) {
				free(result->table_rows.
					table_rows_val[i]->value);
				result->table_rows.
					table_rows_val[i]->value = NULL;
			}

			/*
			 * Free the table row buffer
			 */
			free(result->table_rows.table_rows_val[i]);
			result->table_rows.table_rows_val[i] = NULL;
		}
	}

	/*
	 * Free the table buffer
	 */
	if (result->table_rows.table_rows_val != NULL) {
		free(result->table_rows.table_rows_val);
		result->table_rows.table_rows_val = NULL;
	}
	return (CL_QUERY_OK);
}
