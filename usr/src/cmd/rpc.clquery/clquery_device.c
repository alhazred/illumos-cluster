/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)clquery_device.c	1.14	08/05/21 SMI"

/*
 * Component = clquery deamon
 *
 * Synopsis  = clquery server code
 */

#include <stdlib.h>
#include <limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <strings.h>
#include <stdio.h>
#include <sys/mnttab.h>

#ifndef MNTTAB
#define	MNTTAB "/etc/mnttab"
#endif

#include <rpc/rpc.h>
#include <rpc/xdr.h>
#include <rpc/svc.h>

#include <sys/clconf.h>
#include <sys/cladm_int.h>

#include <scadmin/scconf.h>
#include <libdcs.h>

/* needed by libdid.h */
#include <sys/dditypes.h>
#include <sys/didio.h>
#include <libdid.h>

#include <ctype.h>

#include <cl_query/rpc/cl_query.h>
#include <cl_query/cl_query_types.h>

#ifndef NODEID_UNKNOWN
#define	NODEID_UNKNOWN 0
#endif

static cl_query_error_t
get_device_node_names(clconf_cluster_t *clconf, cl_query_device_prop_t dev);
static cl_query_error_t
get_device_map(did_device_list_t *mapping,
    cl_query_didpath_list_t *dest, char *logical_path);


static cl_query_error_t
clquery_get_quorum_dev_all(clconf_cluster_t *clconf,
    clconf_iter_t *qref,
    quorum_status_t *qstat, cl_query_quorum_output_t *output);

static cl_query_error_t
clquery_get_quorum_dev_byname(clconf_cluster_t *clconf,
    clconf_obj_t *quorums,
    quorum_status_t *qstat, cl_query_quorum_output_t *output);

static cl_query_error_t
cl_query_qdev_dup(cl_query_quorum_prop_t dest, clconf_obj_t *qdev);
static char *clquery_get_nodename_byid(clconf_cluster_t *clconf, int id);
static int get_quorum_db_index(quorum_status_t *qstat, char *name);

extern cl_query_error_t scconf_to_clquery(scconf_errno_t err);
extern cl_query_state_t
    convert_scconf_status(scconf_state_t state);
extern cl_query_error_t authenticate_request(void);
extern cl_query_error_t clquery_free_quorum_result(cl_query_quorum_output_t *);
extern cl_query_error_t clquery_free_device_result(cl_query_device_output_t *);
extern cl_query_state_t
    scstate_to_clquery_state(sc_state_code_t state);
/*  extern void did_device_list_free(did_device_list_t *list); */


typedef struct mnt_ent {
	unsigned int node_id;
	char *dev;
	char *mountp;
	struct mnt_ent *next;
} mnt_ent_t;

static cl_query_error_t get_all_mount_point(mnt_ent_t ** head);
static cl_query_error_t get_all_local_mount_point(mnt_ent_t ** head);
static void free_all_mount_point(mnt_ent_t ** head);
static cl_query_error_t is_valid_fs(char *type);
static cl_query_error_t set_mount_points(mnt_ent_t *head,
    cl_query_didpath_list_t *dest, char *dev_path);


#ifdef OLD_VERSION
void
get_votes_for_dev(quorum_status_t *pquorum_status,
    uint_t *possible, uint_t *current, char *quorum_name)
{
	unsigned int i;
	quorum_device_status_t *pquorum_device_status = NULL;

	*possible = 0;
	*current = 0;

	if (quorum_name == NULL)
		return;

	for (i = 0; i < pquorum_status->num_quorum_devices; i++) {

		pquorum_device_status =
		    &(pquorum_status->quorum_device_list[i]);
		if (strcmp(
			    pquorum_device_status->gdevname,
				quorum_name) == 0) {
			*possible = pquorum_device_status->votes_configured;
			if (pquorum_device_status->state ==
			    QUORUM_STATE_ONLINE) {
				*current = *possible;
			}
			return;
		}
	}

}
#endif
/*
 * get all properties of quorum clconf obj
 * to retrieve attached nodes
 * attached nodes property pattern is path_<node id>
 */

static cl_query_error_t
get_quorum_nodes_list(clconf_cluster_t *clconf,
    clconf_obj_t *obj,
    strarr_list * enabled_ones,
    strarr_list * disabled_ones, int estimated_node_count)
{

	clconf_iter_t *it;
	clconf_obj_t *prop;
	const char *key;
	const char *value;

	int nodeid;
	char *current_node_name;
	int enode_idx = 0;
	int dnode_idx = 0;
	cl_query_error_t error = CL_QUERY_OK;
	strarr *tmp_realloc;

	/*
	 * we already know the number of attached nodes
	 * (estimated_node_count)
	 */
	/*
	 * first alloc, reduce later
	 */

	enabled_ones->strarr_list_len = 0;
	disabled_ones->strarr_list_len = 0;

#ifdef _DEBUG
	printf("estimated count = %d\n", estimated_node_count);
#endif

	enabled_ones->strarr_list_val =
	    (strarr *)calloc((size_t)estimated_node_count, sizeof (strarr));
	if (enabled_ones->strarr_list_val == NULL)
		return (CL_QUERY_ENOMEM);

	enabled_ones->strarr_list_len = (unsigned int)estimated_node_count;


	disabled_ones->strarr_list_val =
	    (strarr *)calloc((size_t)estimated_node_count, sizeof (strarr));
	if (disabled_ones->strarr_list_val == NULL)
		return (CL_QUERY_ENOMEM);

	disabled_ones->strarr_list_len = (unsigned int)estimated_node_count;


	it = clconf_obj_get_properties(obj);
	while ((prop = clconf_iter_get_current(it)) != NULL) {
		key = clpl_get_name(prop);
		if (key != NULL) {
			if (sscanf(key, "path_%d", &nodeid) != 1) {
				clconf_iter_advance(it);
				continue;
			}
			current_node_name = clquery_get_nodename_byid(clconf,
			    nodeid);
			if (current_node_name == NULL) {
				error = CL_QUERY_ENOMEM;
				break;
			}
			value = clconf_obj_get_property(obj, key);
			if (value != NULL) {
				if (strcmp(value, "enabled") == 0) {
					/*
					 * add this node inside the
					 * enabled node list
					 */
					enabled_ones->
					    strarr_list_val[enode_idx++] =
					    current_node_name;
				} else {
					disabled_ones->
					    strarr_list_val[dnode_idx++]
					    = current_node_name;
				}
			}
		}
		clconf_iter_advance(it);
	}

	if (it != NULL)
		clconf_iter_release(it);
#ifdef _DEBUG
	printf("enode_idx  = %d\n", enode_idx);
	printf("dnode_idx  = %d\n", dnode_idx);
#endif
	/*
	 * free useless space
	 */
	/*
	 * if realloc failed ignore it :
	 */
	/*
	 * not so bad, NULL pointer are tested before copy
	 */
	if (enode_idx < estimated_node_count) {
		if (enode_idx == 0) {
			free(enabled_ones->strarr_list_val);
			enabled_ones->strarr_list_val = NULL;
			enabled_ones->strarr_list_len = 0;
		} else {
#ifdef _DEBUG
			printf("enode_idx  realloc\n");
#endif
			tmp_realloc =
			    (strarr *)realloc(enabled_ones->strarr_list_val,
			    (size_t)enode_idx * sizeof (strarr));
			/*
			 * no check : we are downsizing
			 */
			enabled_ones->strarr_list_val = tmp_realloc;
			enabled_ones->strarr_list_len = (unsigned int)enode_idx;
		}
	}
	if (dnode_idx < estimated_node_count) {
		if (dnode_idx == 0) {
			free(disabled_ones->strarr_list_val);
			disabled_ones->strarr_list_val = NULL;
			disabled_ones->strarr_list_len = 0;
		} else {
#ifdef _DEBUG
			printf("dnode_idx  realloc\n");
#endif
			tmp_realloc =
			    (strarr *)realloc(disabled_ones->strarr_list_val,
			    (size_t)dnode_idx * sizeof (strarr));

			disabled_ones->strarr_list_val = tmp_realloc;
			disabled_ones->strarr_list_len =
			    (unsigned int)dnode_idx;
		}
	}

	return (error);
}



#ifdef _DEBUG
void
print_quorum_status(quorum_status_t *qstat)
{
	unsigned int i;
	printf("    **  quorum table is **\n");
	for (i = 0; i < qstat->num_nodes; i++) {
		printf("node id %d, state %d\n",
		    qstat->nodelist[i].nid, qstat->nodelist[i].state);
	}
	for (i = 0; i < qstat->num_quorum_devices; i++) {
		printf("device name %s\n",
		    qstat->quorum_device_list[i].gdevname);
	}
	printf("***************************\n");
}
#endif

static int
get_quorum_db_index(quorum_status_t *qstat, char *name)
{
	unsigned int i;

	for (i = 0; i < qstat->num_quorum_devices; i++) {
		if (strcmp(qstat->quorum_device_list[i].gdevname, name) == 0) {
			/*
			 * we have it
			 */
			return ((int)i);
		}
	}

#ifdef _DEBUG
	printf("get_quorum_db_index for name (%s) fail\n", name);
#endif

	return (-1);

}

static char *
clquery_get_nodename_byid(clconf_cluster_t *clconf, int id)
{
	clconf_iter_t *node_iter = NULL;
	clconf_obj_t *node = NULL;
	const char *n_name = NULL;



	node_iter = clconf_cluster_get_nodes(clconf);
	if (node_iter == NULL)
		return ((void *)NULL);
	while ((node = clconf_iter_get_current(node_iter)) != NULL) {
		if (clconf_obj_get_id(node) == id) {
			n_name = clconf_obj_get_name(node);
			break;
		}
		clconf_iter_advance(node_iter);
	}

	if (node_iter != NULL)
		clconf_iter_release(node_iter);

	if (n_name != NULL)
		return ((char *)strdup(n_name));

	return ((void *)NULL);

}

static cl_query_error_t
cl_query_qdev_dup(cl_query_quorum_prop_t dest, clconf_obj_t *qdev)
{

	const char *tmp_string;

	/*
	 * get quorum name
	 */
	tmp_string = clconf_obj_get_name(qdev);
	if (tmp_string == NULL)
		return (CL_QUERY_EUNEXPECTED);

	dest->device_name = (char *)strdup(tmp_string);
	if (dest->device_name == NULL)
		return (CL_QUERY_ENOMEM);

	/*
	 * get quorum key (device )name
	 */
	tmp_string = clconf_obj_get_property(qdev, "gdevname");
	if (tmp_string == NULL)
		return (CL_QUERY_EUNEXPECTED);

	dest->key_name = (char *)strdup(tmp_string);
	if (dest->key_name == NULL)
		return (CL_QUERY_ENOMEM);

	/*
	 * get quorum key (device )type
	 */
	tmp_string = clconf_obj_get_property(qdev, "type");

	if (tmp_string == NULL)
		dest->device_type = (char *)strdup(SD_TYPE);
	else
		dest->device_type = (char *)strdup(tmp_string);

	if (dest->device_type == NULL)
		return (CL_QUERY_ENOMEM);

	/* shared_disk is a new term to replace scsi. */
	if ((strncmp(dest->device_type, SCSI_TYPE,
	    strlen(SCSI_TYPE)) == 0) ||
	    (strcmp(dest->device_type, SD_TYPE) == 0)) {
		/*
		* get quorum access mode
		*/
		tmp_string = clconf_obj_get_property(qdev, "access_mode");
		if (tmp_string == NULL)
			return (CL_QUERY_EUNEXPECTED);

		dest->access_mode = (char *)strdup(tmp_string);
		if (dest->access_mode == NULL)
			return (CL_QUERY_ENOMEM);
	} else {
		dest->access_mode = NULL;
	}


	if (strcmp(dest->device_type, NETAPP_NAS) == 0) {
		/*
		* get nas quorum filer name
		*/
		tmp_string = clconf_obj_get_property(qdev, "filer");
		if (tmp_string == NULL)
			return (CL_QUERY_EUNEXPECTED);

		dest->filer_name = (char *)strdup(tmp_string);
		if (dest->filer_name == NULL)
			return (CL_QUERY_ENOMEM);
		/*
		* get nas quorum device lun ID
		*/
		tmp_string = clconf_obj_get_property(qdev, "lun_id");
		if (tmp_string == NULL)
			return (CL_QUERY_EUNEXPECTED);

		dest->lunid = (char *)strdup(tmp_string);
		if (dest->lunid == NULL)
			return (CL_QUERY_ENOMEM);
		/*
		* get nas quorum device lun name
		*/
		tmp_string = clconf_obj_get_property(qdev, "lun");
		if (tmp_string == NULL)
			return (CL_QUERY_EUNEXPECTED);

		dest->lun_name = (char *)strdup(tmp_string);
		if (dest->lun_name == NULL)
			return (CL_QUERY_ENOMEM);
	} else {
		dest->filer_name = NULL;
		dest->lunid = NULL;
		dest->lun_name = NULL;
	}

	if (strcmp(dest->device_type, QUORUM_SERVER) == 0) {
		/*
		 * Get quorum server IP
		 */
		tmp_string = clconf_obj_get_property(qdev, "qshost");
		if (tmp_string == NULL) {
			return (CL_QUERY_EUNEXPECTED);
		}
		dest->qs_host = (char *)strdup(tmp_string);
		if (dest->qs_host == NULL) {
			return (CL_QUERY_ENOMEM);
		}
		/*
		 * Get quorum server port number
		 */
		tmp_string = clconf_obj_get_property(qdev, "port");
		if (tmp_string == NULL) {
			return (CL_QUERY_EUNEXPECTED);
		}
		dest->qs_port = (char *)strdup(tmp_string);
		if (dest->qs_port == NULL) {
			return (CL_QUERY_ENOMEM);
		}
		/*
		 * Get quorum device hostname
		 */
		tmp_string = clconf_obj_get_property(qdev, "hostname");
		if (tmp_string == NULL) {
			return (CL_QUERY_EUNEXPECTED);
		}
		dest->qs_name = (char *)strdup(tmp_string);
		if (dest->qs_host == NULL) {
			return (CL_QUERY_ENOMEM);
		}
	} else {
		dest->qs_host = NULL;
		dest->qs_port = NULL;
		dest->qs_name = NULL;
	}
	/*
	 * quorum enabled ?
	 */
	if (clconf_obj_enabled(qdev)) {
		dest->state = CL_QUERY_ENABLED;
	} else {
		dest->state = CL_QUERY_DISABLED;
	}

	/*
	 * quorum possible (configured) votes
	 */
	tmp_string = clconf_obj_get_property(qdev, "votecount");
	if (tmp_string == NULL)
		return (CL_QUERY_EUNEXPECTED);

	dest->possible_votes = (unsigned int)atoi(tmp_string);
	dest->current_votes = 0;

	return (CL_QUERY_OK);

}

static cl_query_error_t
clquery_get_quorum_dev_all(clconf_cluster_t *clconf,
    clconf_iter_t *qref,
    quorum_status_t *qstat, cl_query_quorum_output_t *output)
{

	clconf_obj_t *qdev = NULL;
	int quorum_count = 0;
	cl_query_quorum_prop_t tmp;
	int qidx;
	cl_query_error_t error;
	/*
	 * everything il already allocated for output
	 */


	quorum_count = 0;

	while ((qdev = clconf_iter_get_current(qref)) != NULL) {

		tmp = output->quorum_list.quorum_list_val[quorum_count];

		error = cl_query_qdev_dup(tmp, qdev);
		if (error != CL_QUERY_OK) {
			(void) printf("cl_query_qdev_dup returned %d!!!\n",
			    error);
			return (error);
		}
		/*
		 * get quorum state
		 */
		qidx = get_quorum_db_index(qstat, tmp->key_name);

		if (qidx == -1) {
			tmp->status = CL_QUERY_OFFLINE;
			tmp->owner = NODEID_UNKNOWN;

		} else {
			/*
			 * we may not found the device on the current
			 * quorum snapshot
			 */
			switch (qstat->quorum_device_list[qidx].state) {
			case QUORUM_STATE_ONLINE:
				tmp->status = CL_QUERY_ONLINE;
				tmp->current_votes = tmp->possible_votes;
				break;
			case QUORUM_STATE_OFFLINE:
				tmp->status = CL_QUERY_OFFLINE;
				break;
			default:
				tmp->status = CL_QUERY_UNKNOWN;
			}


			tmp->owner =
			    qstat->quorum_device_list[qidx].reservation_owner;

		}

		/*
		 * XXX TODO move this info to another api call
		 */

		tmp->votes_needed = qstat->votes_needed_for_quorum;
		tmp->votes_configured = qstat->configured_votes;
		tmp->votes_present = qstat->current_votes;

		error = get_quorum_nodes_list(clconf,
		    qdev,
		    &(tmp->enabled_nodes),
		    &(tmp->disabled_nodes), (int)qstat->num_nodes);

		quorum_count++;
		clconf_iter_advance(qref);
	}



	return (CL_QUERY_OK);
}

static cl_query_error_t
clquery_get_quorum_dev_byname(clconf_cluster_t *clconf,
    clconf_obj_t *qdev,
    quorum_status_t *qstat, cl_query_quorum_output_t *output)
{
	cl_query_quorum_prop_t tmp;

	int qidx;
	cl_query_error_t error;

	/*
	 * in that case we have only one element
	 */
	tmp = output->quorum_list.quorum_list_val[0];


	error = cl_query_qdev_dup(tmp, qdev);
	if (error != CL_QUERY_OK)
		return (error);

	/*
	 * get quorum state
	 */

/*
 * we may not found the device on the current
 * quorum snapshot
 */

	qidx = get_quorum_db_index(qstat, tmp->key_name);
	if (qidx < 0) {
		tmp->owner = NODEID_UNKNOWN;
		tmp->status = CL_QUERY_OFFLINE;
	} else {

		switch (qstat->quorum_device_list[qidx].state) {
		case QUORUM_STATE_ONLINE:
			tmp->status = CL_QUERY_ONLINE;
			tmp->current_votes = tmp->possible_votes;
			break;
		case QUORUM_STATE_OFFLINE:
			tmp->status = CL_QUERY_OFFLINE;
			break;
		default:
			tmp->status = CL_QUERY_UNKNOWN;
		}


		tmp->owner = qstat->quorum_device_list[qidx].reservation_owner;
	}

	/*
	 * XXX TODO move this info to another api call
	 */
	tmp->votes_needed = qstat->votes_needed_for_quorum;
	tmp->votes_configured = qstat->configured_votes;
	tmp->votes_present = qstat->current_votes;

	error = get_quorum_nodes_list(clconf,
	    qdev,
	    &(tmp->enabled_nodes),
	    &(tmp->disabled_nodes), (int)qstat->num_nodes);



	return (error);

}



/*
 * DESCRIPTION:
 *    This method is the server part of the QUORUM_INFO_GET API
 *    query. Information related to all the quorums in the cluster
 *    is retrieved from the CCR infrastructure tables using the
 *    libscconf library and returned to the user.
 *
 *
 * INPUT:
 *    cl_query_input_t:  Name of the quorum or NULL (default: all quorums)
 *    struct svc_req  :  RPC request handler
 *
 * OUTPUT:
 *    cl_query_quorum_output_t: Quorum information
 *    error is returned in output.error
 *
 * RETURN:
 *    boolean:  TRUE for success
 *              FALSE on any error
 *
 * SIDE EFFECTS:
 *    None
 *
 * LOCKS:
 */

/*
 * ARGSUSED
 */
bool_t
quorum_info_get_1_svc(cl_query_input_t *input,
    cl_query_quorum_output_t *output, struct svc_req *rqstp /*lint !e715 */)
{
	cl_query_error_t error = CL_QUERY_OK;
	quorum_status_t *pquorum_status = NULL;
	char *input_name_to_use = NULL;
	clconf_cluster_t *clconf = NULL;
	clconf_iter_t *quorum_iterator = NULL;
	unsigned int quorum_count = 0;
	clconf_obj_t *qdev = NULL;
	const char *char_prop = NULL;
	unsigned int i = 0;

	/*
	 * Validate the input parameters
	 */
	if ((input == NULL) || (output == NULL)) {
		return (CL_QUERY_EINVAL);
	}
	if ((input->name == NULL) || (strlen(input->name) == 0)) {
		input_name_to_use = NULL;
	} else {
		input_name_to_use = input->name;
	}

	/*
	 * Initialize output parameter
	 */
	output->error = CL_QUERY_OK;
	output->quorum_list.quorum_list_len = 0;
	output->quorum_list.quorum_list_val = NULL;

	/*
	 * Check for security and initialize clconf library
	 */
	error = authenticate_request();



	/*
	 * Get the cluster information from libclconf
	 */
	if (error == CL_QUERY_OK) {
		if (cluster_get_quorum_status(&pquorum_status) != 0) {
			error = CL_QUERY_EUNEXPECTED;
		}
#ifdef _DEBUG
		if (pquorum_status != NULL)
			print_quorum_status(pquorum_status);
#endif
	}
	if (error == CL_QUERY_OK) {
		clconf = clconf_cluster_get_current();
		if (clconf == NULL) {
			error = CL_QUERY_EUNEXPECTED;
		}
	}
	if (error == CL_QUERY_OK) {
		quorum_iterator = clconf_cluster_get_quorum_devices(clconf);
		if (quorum_iterator == NULL) {
			error = CL_QUERY_EUNEXPECTED;
		}
	}
	/*
	 * get the number of configured quorum
	 */
	/*
	 * if user did not specify a name, we have to loop
	 */

	if (error == CL_QUERY_OK) {
		quorum_count = 0;
		if (input_name_to_use == NULL) {
			quorum_count =
			    (uint_t)clconf_iter_get_count(quorum_iterator);
		} else {
			while (
				(qdev = clconf_iter_get_current(
					quorum_iterator)) != NULL) {
				char_prop = clconf_obj_get_name(qdev);
				if ((char_prop != NULL) &&
				    (strcmp(char_prop,
					    input_name_to_use) == 0)) {
					quorum_count++;
					/*
					 * quorum name is unique, stop
					 * here
					 */
					break;
				}
				clconf_iter_advance(quorum_iterator);
			}

		}
	}
	if ((error == CL_QUERY_OK) && (quorum_count == 0)) {
		error = CL_QUERY_ENOTCONFIGURED;
	}
	if (error == CL_QUERY_OK) {
		/*
		 * allocated needed space
		 */

		output->quorum_list.quorum_list_val =
		    (cl_query_quorum_prop_t *)calloc(quorum_count,
		    sizeof (cl_query_quorum_prop_t));
		if (output->quorum_list.quorum_list_val == NULL) {
			error = CL_QUERY_ENOMEM;
		}
		for (i = 0; i < quorum_count; i++) {
			output->quorum_list.quorum_list_val[i] =
			    (cl_query_quorum_prop_t)calloc(1,
			    sizeof (cl_query_quorum_prop));
			if (output->quorum_list.quorum_list_val[i] == NULL) {
				error = CL_QUERY_ENOMEM;
				break;
			}
		}

		if (error == CL_QUERY_OK)
			output->quorum_list.quorum_list_len = quorum_count;
	}
	if (error == CL_QUERY_OK) {
		if (input_name_to_use == NULL) {
			error = clquery_get_quorum_dev_all(clconf,
			    quorum_iterator, pquorum_status, output);
		} else {
			error = clquery_get_quorum_dev_byname(clconf,
			    qdev, pquorum_status, output);
		}
	}
	if (clconf != NULL) {
		clconf_obj_release((clconf_obj_t *)clconf);
	}
	if (pquorum_status != NULL) {
		cluster_release_quorum_status(pquorum_status);
	}
	if (quorum_iterator != NULL) {
		clconf_iter_release(quorum_iterator);
	}
	if (error != CL_QUERY_OK) {
		/*
		 * cleanup
		 */
		(void) clquery_free_quorum_result(output);
	}
	output->error = error;

	return (TRUE);
}/*lint !e715 */



/*
 * DESCRIPTION:
 *    This method is the server part of the DEVICE_INFO_GET Api
 *    query. Global devices information from the cluster is
 *    retrieved using libscconf and libscstat libraries.
 *
 * INPUT:
 *    cl_query_input_t:  Name of the device or NULL(default: all devices)
 *
 * OUTPUT:
 *    cl_query_device_output_t: Global devices information
 *    error is returned in output.error
 *
 * RETURN:
 *    boolean:  TRUE for success
 *              FALSE on any error
 *
 * SIDE EFFECTS:
 *    None
 *
 * LOCKS:
 */

/*
 * ARGSUSED
 */
bool_t
device_info_get_1_svc(cl_query_input_t *input,
    cl_query_device_output_t *output, struct svc_req *rqstp /*lint !e715 */)
{
	scconf_errno_t scconf_rval = SCCONF_NOERR;
	scconf_nodeid_t *current_nodeid;
	scconf_cfg_ds_t *dsconfig = NULL, *res_list = NULL;
	uint_t device_count = 0;
	cl_query_error_t error = CL_QUERY_OK;
	cl_query_error_t diag = CL_QUERY_OK;
	uint_t node_count;
	cl_query_device_prop_t dev_list;
	uint_t d_index = 0, i = 0;
	did_device_list_t *didmap = NULL;
	int found = 0;
	char *input_name_to_use;
	mnt_ent_t *device_mnt_points = NULL;
	clconf_cluster_t *clconf = NULL;
	scconf_nodeid_t *nodeidp;
	char *namep = NULL;
	uint_t member = 0;
	dc_error_t dc_err = DCS_SUCCESS;
	char buffer[SCSTAT_MAX_STRING_LEN];
	size_t len, used;
	char *repl_prop = NULL;

	/*
	 * Validate the input parameters
	 */
	if ((input == NULL) || (output == NULL)) {
		return (CL_QUERY_EINVAL);
	}

	if ((input->name == NULL) || (strlen(input->name) == 0)) {
		input_name_to_use = NULL;
	} else {
		input_name_to_use = input->name;
	}

	/*
	 * Initialize output parameter
	 */
	output->error = CL_QUERY_OK;
	output->device_list.device_list_len = 0;
	output->device_list.device_list_val = NULL;


	/*
	 * Security verifications
	 */
	error = authenticate_request();

	if (error == CL_QUERY_OK) {
		/*
		 * need ref to get node names
		 */
		clconf = clconf_cluster_get_current();
		if (clconf == NULL) {
			error = CL_QUERY_EUNEXPECTED;
		}
	}
	if (error == CL_QUERY_OK) {
		scconf_rval = scconf_get_ds_config(NULL, SCCONF_BY_NAME_FLAG,
		    &dsconfig);

		error = scconf_to_clquery(scconf_rval);
	}
	/*
	 * Check if the results retured from scconf or sctsat library is
	 * not NULL
	 */
	if (error == CL_QUERY_OK) {
		if (dsconfig == NULL) {
			error = CL_QUERY_EUNEXPECTED;
		}
	}




	didmap = list_all_devices("disk");
	if (didmap == NULL) {
		error = CL_QUERY_EUNEXPECTED;
	}

	(void) get_all_mount_point(&device_mnt_points);


	if (error == CL_QUERY_OK) {

		/*
		 * Get the number of devices to allocate memory for
		 * device_list
		 */
		res_list = dsconfig;
		/*
		 * if the user specify a name , we will have to return
		 * only one element
		 */
		/*
		 * device groups are uniquely named
		 */
		found = 0;
		for (;
		    (res_list != NULL) && (found == 0);
		    res_list = res_list->scconf_ds_next) {
			if (input_name_to_use != NULL) {
				if (strcmp(input_name_to_use,
					res_list->scconf_ds_name) != 0) {
					continue;
				} else {
					/*
					 * we found it , no need to loop
					 * again
					 */
					found = 1;
				}
			}
			device_count++;
		}

	}
	/*
	 * If no entry is found
	 */
	if ((error == CL_QUERY_OK) && (device_count == 0)) {
		error = CL_QUERY_ENOTCONFIGURED;
	}
	if (error == CL_QUERY_OK) {
		/*
		 * Set the count in out param
		 */
		output->device_list.device_list_len = device_count;

		/*
		 * Allocate memory for the device list in out param
		 */
		output->device_list.device_list_val = (cl_query_device_prop_t *)
		    calloc(device_count, sizeof (cl_query_device_prop_t));
		if (output->device_list.device_list_val == NULL) {
			error = CL_QUERY_ENOMEM;
		}
	}
	if (error == CL_QUERY_OK) {
		for (i = 0; i < device_count; i++) {
			output->device_list.device_list_val[i] =
			    (cl_query_device_prop_t)calloc(1,
			    sizeof (struct cl_query_device_prop));
			if (output->device_list.device_list_val[i] == NULL) {
				error = CL_QUERY_ENOMEM;
				break;
			}
		}
	}
	/*
	 * XXX TODO : optimize the loop to avoid looping a second time
	 */

	if (error == CL_QUERY_OK) {
		/*
		 * Get all device groups info by iterating through the
		 * list
		 */
		res_list = dsconfig;
		d_index = 0;
		found = 0;
		for (; (dsconfig != NULL) && (found == 0);
		    dsconfig = dsconfig->scconf_ds_next) {

			/*
			 * Check for any previous errors
			 */
			if (error != CL_QUERY_OK) {
				break;
			}
			/*
			 * Check if we are looking for a specific device
			 */
			if (input_name_to_use != NULL) {
				if (strcmp(input_name_to_use,
					dsconfig->scconf_ds_name) != 0) {
					continue;
				} else {
					/*
					 * next time we gonna stop the
					 * loop
					 */
					found = 1;
				}
			}
			dev_list = output->device_list.device_list_val[d_index];

			/*
			 * Get the device name
			 */
			if (dsconfig->scconf_ds_name != NULL) {
				dev_list->device_name =
				    (char *)strdup(dsconfig->scconf_ds_name);
				if (dev_list->device_name == NULL) {
					error = CL_QUERY_ENOMEM;
					break;
				}
			} else {
				dev_list->device_name = NULL;
			}

			/*
			 * Get the device type
			 */
			if (dsconfig->scconf_ds_type != NULL) {
				dev_list->device_type =
				    (char *)strdup(dsconfig->scconf_ds_type);
				if (dev_list->device_type == NULL) {
					error = CL_QUERY_ENOMEM;
					break;
				}
			} else {
				dev_list->device_type = NULL;
			}


			/*
			 * Get the failback status
			 */
			dev_list->failback =
			    (uint_t)convert_scconf_status(dsconfig->
			    scconf_ds_failback);

			/* Get the replication property */
			dev_list->replication_type = NULL;
			repl_prop = scconf_get_repl_property(
			    dsconfig->scconf_ds_propertylist);
			if (repl_prop != NULL) {
				dev_list->replication_type = (char *)strdup(
				    repl_prop);
				if (dev_list->replication_type == NULL) {
					error = CL_QUERY_ENOMEM;
					break;
				}
			}

			/*
			 * Get the desired number of secondaries
			 */
			/*
			 * Desired number of secondaries
			 *
			 * zero means default is used for this device group.
			 * numsecondaries can't be set to zero otherwise.
			 * cf sccconf.c
			 */

			if (dsconfig->scconf_ds_num_secs == 0) {
				dev_list->desired_num_secondaries =
				    DCS_DEFAULT_DESIRED_NUM_SECONDARIES;
			} else {
				dev_list->desired_num_secondaries =
				    dsconfig->scconf_ds_num_secs;
			}


			/*
			 * Get the logical disk path
			 */
			if (dsconfig->
			    scconf_ds_devvalues->scconf_namelist_name != NULL) {
				dev_list->logical_disk_path = strdup(dsconfig->
				    scconf_ds_devvalues->scconf_namelist_name);
				if (dev_list->logical_disk_path == NULL) {
					error = CL_QUERY_ENOMEM;
					break;
				}
			} else {
				dev_list->logical_disk_path = NULL;
			}


			/*
			 * Get the ordered node list flag
			 */

			dev_list->OrderedNodesList =
			    (uint_t)convert_scconf_status(dsconfig->
			    scconf_ds_preference);


			/*
			 * get the node list
			 */

			dev_list->
			    file_system_path.cl_query_didpath_list_t_len = 0;
			dev_list->
			    file_system_path.cl_query_didpath_list_t_val = NULL;

			current_nodeid = dsconfig->scconf_ds_nodelist;
			if (current_nodeid != NULL) {
				/*
				 * first pass : count the space we need
				 */
				node_count = 0;
				while (*current_nodeid) {
					current_nodeid++;
					node_count++;
				}



				dev_list->
				    file_system_path.
				    cl_query_didpath_list_t_len = node_count;
				dev_list->
				    file_system_path.
				    cl_query_didpath_list_t_val =
				    (cl_query_didpath_list_elem_t *)
				    calloc(node_count,
				    sizeof (cl_query_didpath_list_elem_t));
				if (dev_list->file_system_path.
				    cl_query_didpath_list_t_val == NULL) {
					error = CL_QUERY_ENOMEM;
					break;
				}
				for (i = 0; i < node_count; i++) {

					dev_list->
					    file_system_path.
					    cl_query_didpath_list_t_val[i] =
					    (cl_query_didpath_list_elem_t)
					    calloc((size_t)1,
					sizeof (cl_query_didpath_list_elem));
					if (dev_list->file_system_path.
					    cl_query_didpath_list_t_val[i] ==
					    NULL) {
						error = CL_QUERY_ENOMEM;
						break;
					}
				}

				/*
				 * go for copy
				 */
				current_nodeid = dsconfig->scconf_ds_nodelist;
				i = 0;
				while (*current_nodeid) {
					dev_list->
					    file_system_path.
					    cl_query_didpath_list_t_val
					    [i]->node_id = *current_nodeid;
					(void)
					    scconf_get_nodename(*current_nodeid,
					    &(dev_list->file_system_path.
						cl_query_didpath_list_t_val[i]->
						node_name));
					if (dev_list->file_system_path.
					    cl_query_didpath_list_t_val[i]->
					    node_name == NULL) {
						error = CL_QUERY_ENOMEM;
						break;
					}
					current_nodeid++;
					i++;
				}
			}
			/*
			 * fetch now the mapping on physicals devices
			 */
			if ((strcasecmp(dev_list->device_type,
				    "Local_Disk") == 0) ||
			    (strcasecmp(dev_list->device_type, "Disk") == 0)) {

#ifdef _DEBUG
				for (i = 0; i < node_count; i++) {
					dev_list->
					    file_system_path.
					    cl_query_didpath_list_t_val[i]->
					    node_name = NULL;
					dev_list->
					    file_system_path.
					    cl_query_didpath_list_t_val[i]->
					    phys_path_name = NULL;
					dev_list->
					    file_system_path.
					    cl_query_didpath_list_t_val[i]->
					    mount_point = NULL;
					dev_list->
					    file_system_path.
					    cl_query_didpath_list_t_val[i]->
					    node_id = 0;

				}
				error = CL_QUERY_OK;

#else
				diag = get_device_map(didmap,
				    &(dev_list->file_system_path),
				    dev_list->device_name);

				if (diag != CL_QUERY_OK) {
					error = CL_QUERY_EUNEXPECTED;
				}
#endif
			} else {

				/*
				 * don't check errors, we may not find
				 * mount point
				 */
				(void) set_mount_points(device_mnt_points,
				    &(dev_list->file_system_path),
				    dev_list->device_name);
			}
			/*
			 * Get the primary node name  (cf
			 * get_device_node_names() )
			 */
			dev_list->primary_node_name = NULL;


			dev_list->alternate_nodes.strarr_list_len = 0;
			dev_list->alternate_nodes.strarr_list_val = NULL;

			dev_list->spare_nodes.strarr_list_len = 0;
			dev_list->spare_nodes.strarr_list_val = NULL;

			if (scconf_is_local_device_service(
				dev_list->device_name)) {
				/* Multi-owner device group */
				/* Clear the buffer */
				bzero(buffer, sizeof (buffer));
				used = 0;

				/* Add matching nodes to buffer */
				for (nodeidp = dsconfig->scconf_ds_nodelist;
					*nodeidp; nodeidp++) {
					/* Check name */
					if (nodeidp == NULL) {
						error = CL_QUERY_EUNEXPECTED;
					}
					if (error == CL_QUERY_OK) {
						dc_err =
					dcs_get_multiowner_status(*nodeidp,
						&member);
					}
					if (dc_err != DCS_SUCCESS) {
						error = CL_QUERY_EUNEXPECTED;
					}
					/* Not a member */
					if (!member) {
						continue;
					}
					if (error == CL_QUERY_OK) {
						scconf_rval =
						scconf_get_nodename(
							*nodeidp, &namep);
						if (scconf_rval !=
							SCCONF_NOERR) {
						error = scconf_to_clquery(
							scconf_rval);
						}
					}

					if (error == CL_QUERY_OK) {
						if ((len =
						(size_t)strlen(namep)) >
						((size_t)sizeof (buffer) -
							(used + 2)))
						error = CL_QUERY_ENOMEM;
					}

					if (error == CL_QUERY_OK) {
						/* append name to buffer */
						if (*buffer != '\0') {
						(void) strcat(buffer, ",");
						used += 1;
						}
						(void) strcat(buffer, namep);
						used += len;
					}
					free(namep);
					member = 0;
				}
				if (error == CL_QUERY_OK) {
					dev_list->primary_node_name =
						(char *)strdup(buffer);
					if (dev_list->primary_node_name ==
							NULL) {
						error = CL_QUERY_ENOMEM;
					}
				}
				if (error == CL_QUERY_OK) {
					dev_list->status == CL_QUERY_ONLINE;
				} else {
					dev_list->status == CL_QUERY_OFFLINE;
				}

			} else {
				/*
				 * Get the number of secondary and spare nodes
				 */
				if (error == CL_QUERY_OK) {
					error =
					get_device_node_names(clconf, dev_list);
				}
			}
			/*
			 * increment the index
			 */
			d_index++;
		}
	}
	if (didmap != NULL) {

		/*
		 * did_device_list_free(didmap);
		 */

		(void) did_freeinstlist(didmap);

		didmap = NULL;
	}
	if (clconf != NULL) {
		clconf_obj_release((clconf_obj_t *)clconf);
	}
	if (res_list != NULL) {
		scconf_free_ds_config(res_list);
		dsconfig = NULL;
	}
	if (error != CL_QUERY_OK) {
		/*
		 * cleanup
		 */
		(void) clquery_free_device_result(output);
	}

	free_all_mount_point(&device_mnt_points);



	output->error = error;
	return (TRUE);
}				/*lint !e715 */

static cl_query_error_t
get_device_node_names(clconf_cluster_t *clconf, cl_query_device_prop_t dev)
{
	int dcs_error = 0;
	dc_error_t dc_err = DCS_SUCCESS;
	dc_replica_status_seq_t *active_replicas = NULL;
	sc_state_code_t state = SC_STATE_UNKNOWN;
	int is_suspended = 0;
	unsigned int j = 0;
	int acount = 0;
	int scount = 0;
	char *current_node_name = NULL;



#ifdef CL_QUERY_CLEANUP
#undef CL_QUERY_CLEANUP
#endif

#define	CL_QUERY_CLEANUP()  {                    \
dcs_free_dc_replica_status_seq(active_replicas); \
}

	if ((dev == NULL) || (dev->device_name == NULL)) {
		return (CL_QUERY_EINVAL);
	}
	dcs_error = dcs_initialize();
	if (dcs_error != 0) {
		return (CL_QUERY_ENOTCLUSTER);
	}
	dc_err = dcs_get_service_status(dev->device_name,
	    &is_suspended, &active_replicas, &state, NULL);

	if (dc_err != DCS_SUCCESS) {
		return (CL_QUERY_EUNEXPECTED);
	}

	dev->status = (unsigned int)scstate_to_clquery_state(state);

	dev->alternate_nodes.strarr_list_len = 0;
	dev->alternate_nodes.strarr_list_val = NULL;

	dev->spare_nodes.strarr_list_len = 0;
	dev->spare_nodes.strarr_list_val = NULL;

	/*
	 * first shot : count nodes
	 */
	for (j = 0; j < active_replicas->count; j++) {
		if ((active_replicas->replicas)[j].state == SECONDARY) {
			dev->alternate_nodes.strarr_list_len++;
		}
		if ((active_replicas->replicas)[j].state == SPARE) {
			dev->spare_nodes.strarr_list_len++;
		}
		if ((active_replicas->replicas)[j].state == PRIMARY) {
			current_node_name = clquery_get_nodename_byid(clconf,
			    (int)(active_replicas->replicas)[j].id);
			if (current_node_name != NULL) {
				dev->primary_node_name =
				    (char *)strdup(current_node_name);
				if (dev->primary_node_name == NULL) {
					CL_QUERY_CLEANUP();
					dev->spare_nodes.strarr_list_len = 0;
					dev->alternate_nodes.strarr_list_len =
					    0;
					return (CL_QUERY_ENOMEM);
				}
			}
		}
	}

	if ((dev->alternate_nodes.strarr_list_len == 0) &&
	    (dev->spare_nodes.strarr_list_len == 0)) {
		CL_QUERY_CLEANUP();
		return (CL_QUERY_OK);
	}
	dev->alternate_nodes.strarr_list_val =
	    (strarr *)calloc
	    (dev->alternate_nodes.strarr_list_len, sizeof (strarr));
	if (dev->alternate_nodes.strarr_list_val == NULL) {
		dev->alternate_nodes.strarr_list_len = 0;
		CL_QUERY_CLEANUP();
		return (CL_QUERY_ENOMEM);
	}
	dev->spare_nodes.strarr_list_val =
	    (strarr *)calloc(dev->spare_nodes.strarr_list_len, sizeof (strarr));
	if (dev->spare_nodes.strarr_list_val == NULL) {
		dev->spare_nodes.strarr_list_len = 0;
		CL_QUERY_CLEANUP();
		return (CL_QUERY_ENOMEM);
	}

	acount = 0;
	scount = 0;

	for (j = 0; j < active_replicas->count; j++) {
		if ((active_replicas->replicas)[j].state == SECONDARY) {
			current_node_name = clquery_get_nodename_byid(clconf,
			    (int)(active_replicas->replicas)[j].id);
			if (current_node_name != NULL) {
				dev->alternate_nodes.strarr_list_val[acount] =
				    (char *)strdup(current_node_name);
				if (dev->
				    alternate_nodes.strarr_list_val[acount] ==
				    NULL) {
					CL_QUERY_CLEANUP();
					return (CL_QUERY_ENOMEM);
				}
				acount++;
			}
		}
		if ((active_replicas->replicas)[j].state == SPARE) {
			current_node_name = clquery_get_nodename_byid(clconf,
			    (int)(active_replicas->replicas)[j].id);
			if (current_node_name != NULL) {
				dev->spare_nodes.strarr_list_val[scount] =
				    (char *)strdup(current_node_name);
				if (dev->
				    spare_nodes.strarr_list_val[scount] ==
				    NULL) {
					CL_QUERY_CLEANUP();
					return (CL_QUERY_ENOMEM);
				}
				scount++;
			}
		}
	}

	CL_QUERY_CLEANUP();

	return (CL_QUERY_OK);

}

/*
 * use did library to find out the physical names on each
 * nodes for the given logical path
 * dest is already filled ,we grab here the physical path
 */

static cl_query_error_t
get_device_map(did_device_list_t *mapping,
    cl_query_didpath_list_t *dest, char *logical_path)
{

	extern int errno;
	char *name_to_use;
	int name_to_use_length;
	did_device_list_t *didmap = NULL;
	unsigned int i;
	char *pos;
	int number_of_path;
	char *name_ptr;
	did_subpath_t *subptr;
	int device_instance;


	if ((mapping == NULL) || (dest == NULL))
		return (CL_QUERY_EINVAL);


	if ((logical_path == NULL) || (strlen(logical_path) == 0)) {
		return (CL_QUERY_OK);
	}
	/*
	 * bypass all helper of libdid to go faster
	 */
	/*
	 * frist extract logical part of 'logical_path'
	 */
	/*
	 * logical_path should be something like dsk/d<num>
	 */
	/*
	 * need to extract num part
	 */

	/*
	 * putting ourself just before '\0'
	 */
	pos = logical_path + strlen(logical_path) - 1;
	while ((isdigit((*pos)) > 0) && (pos != logical_path))
		pos--;
	/*
	 * we are on the first non-digit, go further one char
	 */
	if (pos != logical_path)
		pos++;
	errno = 0;
	device_instance = atoi(pos);
	if (errno != 0) {
		/*
		 * something went wrong during conversion
		 */
		return (CL_QUERY_OK);
	}
	/*
	 * grab the rigth instlist inside 'mapping' for instance
	 * 'device_instance'
	 */
	didmap = mapping;
	while (didmap) {
		if (didmap->instance == device_instance)
			break;
		didmap = didmap->next;
	}

	if (didmap == NULL) {
		/*
		 * mapping not found
		 */
		return (CL_QUERY_OK);
	}
	/*
	 * first, fint out number of path
	 */
	number_of_path = 0;
	subptr = didmap->subpath_listp;
	while (subptr) {
		number_of_path++;
		subptr = subptr->next;
	}

	if (number_of_path <= 0) {
		return (CL_QUERY_OK);
	}
	for (i = 0; i < dest->cl_query_didpath_list_t_len; i++) {
		name_to_use = dest->cl_query_didpath_list_t_val[i]->node_name;
		name_to_use_length = (int)strlen(name_to_use);

		subptr = didmap->subpath_listp;
		while (subptr) {
			/*
			 * subptr->device_path entry looks like
			 */
			/*
			 * <node>:<physical path>
			 */
			/*
			 * check that node match
			 */
			if ((
				    strncmp(
					    name_to_use,
						subptr->device_path,
						(size_t)name_to_use_length)
					== 0) &&
			    (subptr->device_path[name_to_use_length] == ':')) {
				/*
				 * we have the right one, start copy after
				 * ':'
				 */
				name_ptr =
				    subptr->device_path +
				    (name_to_use_length + 1);
				dest->
				    cl_query_didpath_list_t_val[i]->
				    phys_path_name = (char *)strdup(name_ptr);
				if (dest->
				    cl_query_didpath_list_t_val[i]->
				    phys_path_name == NULL) {
					break;
				}
			}
			subptr = subptr->next;
		}
	}



	return (CL_QUERY_OK);
}


static cl_query_error_t
set_mount_points(mnt_ent_t *head,
    cl_query_didpath_list_t *dest, char *dev_path)
{
	mnt_ent_t *ptr = head;
	mnt_ent_t *mnt_res;
	unsigned int i;
	unsigned nodeId;
	cl_query_error_t res;
	int number_of_found = 0;

	if ((head == NULL) || (dest == NULL))
		return (CL_QUERY_EINVAL);

	res = CL_QUERY_ENOENT;

	for (i = 0; i < dest->cl_query_didpath_list_t_len; i++) {
		nodeId = dest->cl_query_didpath_list_t_val[i]->node_id;

		ptr = head;
		number_of_found = 0;
		mnt_res = NULL;
		while (ptr) {

			if ((ptr->node_id == nodeId) &&
			    (strstr(ptr->dev, dev_path) != NULL)) {

				number_of_found++;
				mnt_res = ptr;
			}
			ptr = ptr->next;
		}

		if (number_of_found == 1) {
			dest->cl_query_didpath_list_t_val[i]->mount_point =
			    (char *)strdup(mnt_res->mountp);
			if (dest->
			    cl_query_didpath_list_t_val[i]->
			    mount_point == NULL) {
				res = CL_QUERY_ENOMEM;
				break;
			}
		}
		if (number_of_found > 1) {
			dest->
			    cl_query_didpath_list_t_val[i]->mount_point =
			    (char *)strdup("NOT_UNIQUE");
			if (dest->
			    cl_query_didpath_list_t_val[i]->
			    mount_point == NULL) {
				res = CL_QUERY_ENOMEM;
				break;
			}
		}
#ifdef _DEBUG
		printf("NUMBER OF ITEM FOUND FOR %s :  %d\n",
		    dest->cl_query_didpath_list_t_val[i]->phys_path_name,
		    number_of_found);
#endif
	}
	return (res);

}

#ifdef _DEBUG
static void
print_mnt_tab(mnt_ent_t *head)
{
	mnt_ent_t *ptr = head;

	if (head == NULL) {
		printf("mnt head is NULL\n");
		exit(1);
	}
	ptr = head;
	while (ptr) {
		printf("dev -%s- mnt point -%s-\n", ptr->dev, ptr->mountp);
		ptr = ptr->next;
	}

}
#endif
static cl_query_error_t
get_all_mount_point(mnt_ent_t ** head)
{
	return (get_all_local_mount_point(head));
}

/*
 * inside mnt tab we only care about ufs,..
 * reject autofs nfs tmpfs and proc fs
 */
static cl_query_error_t
is_valid_fs(char *type)
{
	size_t size;

	if (type == NULL)
		return (CL_QUERY_EUNEXPECTED);

	size = strlen(type);
	if (size == 0)
		return (CL_QUERY_EUNEXPECTED);

	switch (size) {
	case 3:
		if (strcmp(type, "nfs") == 0)
			return (CL_QUERY_EUNEXPECTED);
		break;
	case 4:
		if (strcmp(type, "proc") == 0)
			return (CL_QUERY_EUNEXPECTED);
		break;
	case 5:
		if (strcmp(type, "tmpfs") == 0)
			return (CL_QUERY_EUNEXPECTED);
		break;
	case 6:
		if (strcmp(type, "autofs") == 0)
			return (CL_QUERY_EUNEXPECTED);
		break;
	default:
		return (CL_QUERY_OK);
	}

	return (CL_QUERY_OK);

}

static cl_query_error_t
get_all_local_mount_point(mnt_ent_t ** head)
{
	FILE *db = NULL;
	struct mnttab mp;
	mnt_ent_t *tmp = NULL;
	mnt_ent_t *tmpalloc = NULL;
	unsigned int myid;


	(void) cladm(CL_CONFIG, CL_NODEID, &myid);

	db = fopen(MNTTAB, "r");
	if (db == NULL)
		return (CL_QUERY_EUNEXPECTED);

	while (getmntent(db, &mp) == 0) {

		if (is_valid_fs(mp.mnt_fstype) != CL_QUERY_OK)
			continue;

		tmpalloc = (mnt_ent_t *)malloc(sizeof (mnt_ent_t));
		if (tmpalloc == NULL) {
/*
 * stop here but don't free,
 * we may already read what we need
 * will be freed at the end of device_info_get_1_svc()
 */
			break;
		}
		tmpalloc->dev = (char *)strdup(mp.mnt_special);
		tmpalloc->mountp = (char *)strdup(mp.mnt_mountp);
		tmpalloc->node_id = myid;
		tmpalloc->next = tmp;
		tmp = tmpalloc;
		if ((tmpalloc->dev == NULL) || (tmpalloc->mountp == NULL)) {
			/*
			 * memory problem
			 */
			/*
			 * stop here, leave the list like that with the
			 * last element attached
			 */
			/*
			 * everything will be freeed at the end of
			 * device_info_get_1_svc()
			 */
			break;
		}
	}

	(void) fclose(db);

	*head = tmp;

#ifdef _DEBUG
	print_mnt_tab(tmp);
#endif
	return (CL_QUERY_OK);
}


static void
free_all_mount_point(mnt_ent_t ** head)
{
	mnt_ent_t *tmp = *head;
	mnt_ent_t *tmp2 = *head;


	while (tmp) {
		tmp2 = tmp->next;
		if (tmp->dev)
			free(tmp->dev);
		if (tmp->mountp)
			free(tmp->mountp);
		free(tmp);
		tmp = tmp2;
	}

	*head = NULL;

}
