/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 **************************************************************************
 *
 * Component = clquery deamon
 *
 * Synopsis  = clquery server code
 *
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 **************************************************************************
 */

#pragma ident	"@(#)clquery_main.c	1.6	08/05/20 SMI"

#include <stdlib.h>
#include <limits.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <strings.h>


#include <unistd.h>


#include <rpc/rpc.h>
#include <rpc/xdr.h>
#include <rpc/svc.h>
#include <rpc/rpc_com.h>

#include <sys/clconf.h>
#include <sys/cladm_int.h>

#include <scadmin/scstat.h>
#include <scadmin/scconf.h>
#include <rgm/sczones.h>

#include <cl_query/rpc/cl_query.h>
#include <cl_query/cl_query_types.h>

#include "rpc_queue.h"
#include "rpc_manager.h"

#define	 RPC_THREAD_COUNT   1


extern void clquery_server_1(struct svc_req *, register SVCXPRT *);


/*
 * DESCRIPTION:
 *    This method frees all the memory allocated by the RPC
 *    for XDR conversions.
 *
 *
 * INPUT:
 *    SVCXPRT:  RPC transport handler
 *     xdr_result:  XDR result structure
 *
 * OUTPUT:
 *    None.
 *
 * RETURN:
 *    0: on success
 *    1: on Failure
 *
 * SIDE EFFECTS:
 *    None
 *
 * LOCKS:
 */

/* ARGSUSED */
int
clquery_server_1_freeresult(SVCXPRT * transp, xdrproc_t xdr_result,
    caddr_t result)
{
	xdr_free(xdr_result, result);

	return (TRUE);
}


/*
 * DESCRIPTION:
 *    This method initializes all the data structures of the server
 *    before registering the RPC service with rpcbind.
 *
 *
 * INPUT:
 *    None.
 *
 * OUTPUT:
 *    None.
 *
 * RETURN:
 *    None.
 *
 * SIDE EFFECTS:
 *    RPC queue is initialized and the thread pool is started.
 *
 * LOCKS:
 */

cl_query_error_t
start_clquery_server()
{
	cl_query_error_t res = CL_QUERY_OK;

	/*
	 * start the RPC dispatch thread pool
	 */
	res = init_rpc_manager(RPC_THREAD_COUNT);

	return (res);
}

/*
 * DESCRIPTION:
 *    This method registers the RPC service with rpcbind.
 *    After this point any rpc request to the clquery deamon
 *    will recievied by the server.
 *
 * INPUT:
 *    None.
 *
 * OUTPUT:
 *    None.
 *
 * RETURN:
 *    None.
 *
 * SIDE EFFECTS:
 *    RPC request will be serviced.
 *
 * LOCKS:
 */


void
start_rpc_service()
{

	int res = 0;

	res = svc_create(clquery_server_1, CLQUERY_SERVER, CLQUERY_VERS, "tcp");
	if (res == 0) {
		exit(1);
	}

	svc_run();
}

/*
 * DESCRIPTION:
 *    This initializes the libraries related to FMM.
 *
 *
 * INPUT:
 *    None.
 *
 * OUTPUT:
 *    None.
 *
 * RETURN:
 *    None.
 *
 * SIDE EFFECTS:
 *    None.
 *
 * LOCKS:
 */
void
init_farmmgmt_libs(void)
{
	/* Check for Europa */
	scconf_errno_t scconferr = SCCONF_NOERR;
	boolean_t is_farm;
	char scconf_errstr[BUFSIZ];

	scconferr = scconf_iseuropa(&is_farm);
	if (scconferr != SCCONF_NOERR) {
		return;
	}

	if (!is_farm) {
		return;
	}

	scconferr = scconf_libfmm_open();
	if (scconferr != SCCONF_NOERR) {
		scconf_strerr(scconf_errstr, scconferr);
		return;
	}

	scconferr = scconf_libscxcfg_open();
	if (scconferr != SCCONF_NOERR) {
		scconf_strerr(scconf_errstr, scconferr);
		scconf_libfmm_close();
		return;
	}

	scconferr = scconf_fmm_initialize();
	if (scconferr != SCCONF_NOERR) {
		scconf_strerr(scconf_errstr, scconferr);
		scconf_libfmm_close();
		scconf_libscxcfg_close();

		return;
	}
}

/*
 * DESCRIPTION:
 *
 *   check if we are member of a cluster
 *
 * INPUT:
 *    None.
 *
 * OUTPUT:
 *    None.
 *
 * RETURN:
 *    CL_QUERY_OK  : On success
 *    CL_QUERY_EXXX: On error
 *
 * SIDE EFFECTS:
 *    None.
 *
 * LOCKS:
 */

cl_query_error_t
clquery_ismember()
{
	cl_query_error_t res = CL_QUERY_OK;
	int bootflags;
	int mynodeid;
	quorum_status_t *quorum_status = NULL;
	unsigned int i;
	short found;
#ifdef MANU
	uint_t flagp;
#endif

#ifdef MANU
	scconf_ismember(0, &flagp);
	return (CL_QUERY_OK);

#else

	if ((cladm(CL_INITIALIZE, CL_GET_BOOTFLAG, &bootflags) != 0) ||
	    !(bootflags & CLUSTER_BOOTED)) {
		return (CL_QUERY_ENOTCLUSTER);
	}

	/*
	 * we are currently inside a cluster
	 */
	/*
	 * check quorum
	 */
	if (cladm(CL_CONFIG, CL_NODEID, &mynodeid) != 0) {
		return (CL_QUERY_ENOTCLUSTER);
	}
	/*
	 * Get quorum status
	 */
	if (cluster_get_quorum_status(&quorum_status) != 0)
		return (CL_QUERY_EINVAL);

	res = CL_QUERY_OK;

	/*
	 * Find the nodeid
	 */
	found = 0;
	for (i = 0; ((i < quorum_status->num_nodes) && (!found)); i++) {
		if ((int)quorum_status->nodelist[i].nid == mynodeid) {
			found = 1;
			break;
		}
	}

	if (!found) {
		cluster_release_quorum_status(quorum_status);
		return (CL_QUERY_ENOTCLUSTER);
	}

	/*
	 * Set the flag based on whether or not the node is online
	 */
	switch (quorum_status->nodelist[i].state) {
	case QUORUM_STATE_ONLINE:
		res = CL_QUERY_OK;
		break;

	case QUORUM_STATE_OFFLINE:
		res = CL_QUERY_EUNEXPECTED;
		break;

	default:
		res = CL_QUERY_EUNEXPECTED;
	}

	cluster_release_quorum_status(quorum_status);


	return (res);
#endif

}

/*
 * DESCRIPTION:
 *    This method quthenticates the RPC request.
 *    After this point any rpc request to the clquery deamon
 *    will recievied by the server.
 *
 * INPUT:
 *    None.
 *
 * OUTPUT:
 *    None.
 *
 * RETURN:
 *    CL_QUERY_OK  : On success
 *    CL_QUERY_EXXX: On error
 *
 * SIDE EFFECTS:
 *    None.
 *
 * LOCKS:
 */

cl_query_error_t
authenticate_request()
{
	cl_query_error_t res = CL_QUERY_OK;


	/*
	 * FIX_ME: Add other security checks
	 * we cannot reject call on  scconf_ismember check
	 * even if node is in install mode
	 * agent must be able to get info
	 */


	res = clquery_ismember();


	return (res);
}

/*
 * daemonize myself
 */
static void
daemonize(void)
{
	pid_t pid;
	extern int errno;

	pid = fork();
	if (pid < 0) {
		(void) fprintf(stderr, "fork: %s", strerror(errno));
		exit(1);
	}
	if (pid > 0) {
		exit(0);
	}


	/*
	 * set process group
	 */
	pid = setsid();
	if (pid == (pid_t)-1) {
		(void) fprintf(stderr, "setsid: %s", strerror(errno));
		exit(1);
	}


	(void) signal(SIGCHLD, SIG_IGN);
	(void) signal(SIGHUP, SIG_IGN);

	pid = fork();
	if (pid < 0) {
		(void) fprintf(stderr, "fork: %s", strerror(errno));
	}
	if (pid > 0) {
		exit(0);
	}

	(void) chdir("/");

	(void) close(STDIN_FILENO);
	(void) close(STDOUT_FILENO);
	(void) close(STDERR_FILENO);
}

/*
 * DESCRIPTION:
 *    This initializes the clconf library and ORB.
 *
 *
 * INPUT:
 *    None.
 *
 * OUTPUT:
 *    None.
 *
 * RETURN:
 *    CL_QUERY_OK  : On success
 *    CL_QUERY_EXXX: On error
 *
 * SIDE EFFECTS:
 *    None.
 *
 * LOCKS:
 */
cl_query_error_t
sc_com_init(void)
{


	if (clconf_lib_init() != 0)
		return (CL_QUERY_EUNEXPECTED);

	return (CL_QUERY_OK);

}

/*
 * DESCRIPTION:
 *    main() routine of the clquery deamon.
 *
 * INPUT:
 *    None.
 *
 * OUTPUT:
 *    None.
 *
 * RETURN:
 *
 * SIDE EFFECTS:
 *    None.
 *
 * LOCKS:
 */

int
main(int argc, char **argv)
{

	int mode = RPC_SVC_MT_USER;
	cl_query_error_t res = CL_QUERY_OK;
	int debug_mode = 0;
	int num = 1;

	if (sc_zonescheck() != 0)
		return (1);
	/*
	 * hidden option
	 */
	if (argc > 1) {
		if (strcmp(argv[1], "-d") == 0) {
			debug_mode = 1;
		}
	}


	if (!debug_mode) {
		daemonize();
	}

	/* Initialize FMM libs before initing orb */
	init_farmmgmt_libs();

	if (sc_com_init() != CL_QUERY_OK) {
		(void) fprintf(stderr,
		    "cannot initialise com -> abort\n");
		exit(1);
	}

	if (!rpc_control(RPC_SVC_MTMODE_SET, &mode)) {
		(void) fprintf(stderr,
		    "cannot set RPC_SVC_MTMODE_SET abort\n");
		exit(1);
	}

	if (!rpc_control(RPC_SVC_USE_POLLFD, &num)) {
		(void) fprintf(stderr,
		    "cannot set RPC_SVC_USE_POLLFD\n");
		exit(1);
	}

	/*
	 * Ignore the SIGPIPE signal
	 */
	(void) sigset(SIGPIPE, SIG_IGN);

	/*
	 * initialize the global data structures
	 */
	res = start_clquery_server();

	if (res == CL_QUERY_OK) {
		start_rpc_service();
	}

	exit(0);
}
