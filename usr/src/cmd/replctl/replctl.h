/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_RMCNTL_H
#define	_RMCNTL_H

#pragma ident	"@(#)replctl.h	1.12	08/05/20 SMI"

#include <h/replica.h>

#ifdef __cplusplus
extern "C" {
#endif

//
// Enum for the operation that rmcntl will perform
//
enum rmcntl_op {
	RMCNTL_OP_DISPLAY,
	RMCNTL_OP_STATE_CHANGE,
	RMCNTL_OP_SHUTDOWN,
	RMCNTL_OP_SET_NUMSECS,
	RMCNTL_OP_SET_PRI,
	RMCNTL_OP_UPGRADE_FREEZE,
	RMCNTL_OP_UPGRADE_UNFREEZE,
	RMCNTL_OP_DISP_UPGRADE_FROZEN
};

//
// opt_t
//
// This structure holds the values of the command line options specified
// by the user.
// This strcuture is filled in by the function get_options
//
typedef struct {
	int			hdrs;		// Print headers
	int			show_repl;	// Display replica info
	bool			force;		// Force state change
	replica::change_op	change_op;	// Change operation
	char			*service_desc;	// Service description
	char			*prov_desc;	// Repl prov description
	rmcntl_op		op;		// Operation that tool
						// will perform
	uint32_t		numsecs;
	uint32_t		priority;
} opt_t;

//
// External Function definition
//

// Print tool usage
void usage(const char *bin_name);

// Examine command line options
opt_t *get_options(int argc, char **argv);

// Take action
int do_rmcntl(opt_t *p_opt);

//
// Action functions
//
// Display
int do_display(opt_t *p_opt);
int do_numsecs_change(opt_t *p_opt);
int do_pri_change(opt_t *p_opt);
int do_state_change(opt_t *p_opt);
int do_shutdown(opt_t *p_opt);
int do_upgrade_freeze(opt_t *p_opt);
int do_upgrade_unfreeze(opt_t *p_opt);
int do_display_upgrade_frozen(void);

// Helper routines
void print_service(replica::service_info *svc_info);
void print_replica(replica::repl_prov_info *repl_info);

#ifdef __cplusplus
}
#endif

#endif /* _RMCNTL_H */
