/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _RMCNTL_LIB_H
#define	_RMCNTL_LIB_H

#pragma ident	"@(#)repl_lib.h	1.10	08/05/20 SMI"

#include <h/replica.h>

#ifdef __cplusplus
extern "C" {
#endif

//
// Functions to fetch and massage information from the replica
// idl interface.
//

//
// get_services
// Gets a sequence of replica::service_info from the rma
//
// Arguments:
//	Sequence in which to place services
//	A reference to the rma
// Returns
//	0 on success
int
get_services(
	replica::rm_service_list *&svc_list,
	const replica::rm_admin_var &rma_ref,
	Environment &e);

//
// get_control
// Gets the Service Admin interface for a specific service
//
// Arguments:
//	A string of the service name
//	A reference to the rma
// Returns:
//	A referernce to the service admin object
//	NULL in case of error
replica::service_admin_ptr
get_control(
	const char *service_desc,
	const replica::rm_admin_var &rma_ref,
	Environment &e);

//
// get_repl_provs
// Gets the list of repl providers
//
// Arguments
//	A sequence into which to place the prov info
//	A reference to the service control
// Returns
//	0 on success
int
get_repl_provs(
	replica::repl_prov_seq *&repl_list,
	const replica::service_admin_var &svc_admin_ref,
	Environment &e);

//
// get_service_info
// Gets teh lasters info on service
//
// Arguments
//	A reference to the service admin object
// Returns
//	A pointer to a service_info structure
replica::service_info *
get_service_info(replica::service_admin_var &svc_admin_ref, Environment &e);


//
// change_repl_prov_status
// Request a state change of a replica provider
//
// Arguments
//	A reference to the service admin object
//	The name of the repl_prov that we want to change
//	The state we want to change to
// Returns
//	0 on success
int
change_repl_prov_status(
	const replica::service_admin_var &svc_admin_ref,
	const char *repl_prov_str,
	const replica::change_op op,
	Environment &e);


//
// shutdown_service
// Shutdown the service from the rma
//
// Arguments
//	flag indicating whether this is a forced shutdown
//	String of service description to unregister
// Returns
//	0 on success
int
shutdown_service(
	const bool is_forced_shutdown,
	const replica::service_admin_var &svc_admin_ref,
	Environment &e);

#ifdef __cplusplus
}
#endif

#endif /* _RMCNTL_LIB_H */
