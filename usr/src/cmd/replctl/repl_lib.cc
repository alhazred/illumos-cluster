/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)repl_lib.cc	1.15	08/05/20 SMI"

#include <sys/os.h>
#include <h/replica.h>		// Replica FRMWK
#include <h/naming.h>		// Nameserver
#include <nslib/ns.h>		// Nameserver
#include <orb/invo/common.h>

#include "repl_lib.h"

//
// Functions to fetch and massage information from the replica
// idl interface.
//


// get_services
// Gets a sequence of replica::service_info from the rma
//
// Arguments:
//	a sequence of replica::service_info
//	A reference to the rma
// Returns:
//	0 on success
int
get_services(
	replica::rm_service_list *& svc_list,
	const replica::rm_admin_var &rma_ref,
	Environment &e)
{
	ASSERT(svc_list != NULL);
	ASSERT(!CORBA::is_nil(rma_ref));

	// Get the service list
	rma_ref->get_services(svc_list, e);
	if (e.exception())
		return (-1);

	return (0);
}


//
// get_control
// Gets the Service Admin interface for a specific service
//
// Arguments:
//	A string of the service name
//	A reference to the rma
// Returns:
//	A referernce to the service admin object
//	NULL in case of error
replica::service_admin_ptr
get_control(
	const char *service_desc,
	const replica::rm_admin_var &rma_ref,
	Environment &e)
{
	replica::service_admin_var	svc_admin_ref;

	ASSERT(service_desc != NULL);
	ASSERT(!CORBA::is_nil(rma_ref));

	// Get control
	svc_admin_ref = rma_ref->get_control(service_desc, e);
	if (e.exception())
		return (nil);

	if (CORBA::is_nil(svc_admin_ref))
		return (nil);

	return (replica::service_admin::_duplicate(svc_admin_ref));
}

//
// get_repl_provs
// Gets the list of repl providers
//
// Arguments:
//	A sequence into which to place the prov info
//	A reference to the service control
// Returns:
//	0 of success
int
get_repl_provs(
	replica::repl_prov_seq *& repl_list,
	const replica::service_admin_var &svc_admin_ref,
	Environment &e)
{
	ASSERT(repl_list != NULL);
	ASSERT(!CORBA::is_nil(svc_admin_ref));

	svc_admin_ref->get_repl_provs(repl_list, e);
	if (e.exception())
		return (-1);

	return (0);
}


//
// get_service_info
// Gets the latest info on service
//
// Arguments
//	A reference to the service admin object
// Returns
//	A pointer to a service_info structure
replica::service_info *
get_service_info(replica::service_admin_var &svc_admin_ref, Environment &e)
{
	replica::service_info	*svc_info	= NULL;

	ASSERT(!CORBA::is_nil(svc_admin_ref));

	svc_info = svc_admin_ref->get_service_info(e);
	if (e.exception())
		return (NULL);

	return (svc_info);
}


//
// change_repl_prov_status
// Request a state change of a replica provider
//
// Arguments
//	A reference to the service admin object
//	The name of the repl_prov that we want to change
//	The state we want to change to
// Returns:
//	0 on success
int
change_repl_prov_status(
	const replica::service_admin_var &svc_admin_ref,
	const char *repl_prov_str,
	const replica::change_op op,
	Environment &e)
{
	ASSERT(!CORBA::is_nil(svc_admin_ref));
	ASSERT(repl_prov_str != NULL);

	svc_admin_ref->change_repl_prov_status(repl_prov_str, op, false, e);
	if (e.exception())
		return (-1);

	return (0);
}


//
// shutdown_service
// Shutdown the service from the rma
//
// Arguments
//	flag indicating whether this is a forced shutdown
//	String of service description to unregister
//	A reference to the service admin object
// Returns:
//	0 on success
//	-1 if exception occured
//	non-zero error value, otherwise
//
int
shutdown_service(
	const bool is_forced_shutdown,
	const replica::service_admin_var &svc_admin_ref,
	Environment &e)
{
	int result = 0;
	ASSERT(!CORBA::is_nil(svc_admin_ref));

	result = (int)svc_admin_ref->shutdown_service(is_forced_shutdown, e);
	if (e.exception()) {
		return (-1);
	} else {
		return (result);
	}
}
