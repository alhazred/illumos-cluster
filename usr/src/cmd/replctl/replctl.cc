/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)replctl.cc	1.20	08/05/20 SMI"

// rmcntl
//
// Is an user-land admin tool to manage HA services and their replicas
//
// replctl supports functions to:
//
// 1 - Display services + state + priority + desired numsecondaries + replicas
// 2 - Request state changes of replicas
// 3 - Request priority changes of replicas
// 4 - Request setting of the desired number of secondaries for services
// 5 - Removal of replicas
// 6 - Shutdown of services
// 7 - Unregistering services
//


#include <sys/os.h>
#include <sys/rm_util.h>
#include <h/replica.h>
#include <h/naming.h>
#include <h/replica_int.h>

#include <stdio.h>

#include "replctl.h"
#include "repl_lib.h"

char message_prefix[] = "replctl:";

#define	PRINT_HEADER		os::printf("%-4s %-25s %-25s %s\n", "SID", \
				"Service Description", "Service State", \
				"Desired Numsecondaries");
#define	PRINT_REPL_HEADER	os::printf("%-4s %-25s %-25s %s\n", " ", \
				"Replica Description", "Replica State", \
				"Priority");

//
// Print tool usage
// Arguments:
//	The name of this binary
//
void
usage(const char *bin_name)
{
	ASSERT(bin_name != NULL);

	os::printf("Usage:\n");
	os::printf("[ Display HA Services ]\n");
	os::printf("\t%s <-d service name> [replica desc]\n", bin_name);
	os::printf("\n");
	os::printf("[ Request Replica State Change ]\n");
	os::printf("\t%s <-c service name> <replica desc> <state>\n",
		bin_name);
	os::printf("\tStates may be: \"primary\", \"secondary\", "
		"\"spare\", or \"remove\"\n");
	os::printf("\t%s <-c service name> <replica desc> <priority>\n",
		bin_name);
	os::printf("\tPriority may be: \"0\", \"1\" or etc.\n");
	os::printf("\n");
	os::printf("[ Change the desired number of secondaries of an HA "
		"Service ]\n");
	os::printf("\t%s <-n service name> <new desired number of secondaries>"
		"\n", bin_name);
	os::printf("\n");
	os::printf("[ Shutdown of an HA Service ]\n");
	os::printf("\t%s [-f] <-x service name>\n", bin_name);
	os::printf("\t-x\t- Shutdown service\n");
	os::printf("\t-f\t- Force shutdown\n");
	os::printf("\t-z <service_name> Freeze for upgrade\n");
	os::printf("\t-u <service_name> Unfreeze service frozen for upgrade\n");
	os::printf("\t-g Print list of services frozen for upgrade\n");
	os::printf("\n");
}

//
// Get command line options
//
// They are:
//	-d	- display service
//	-c	- change state
//	-x	- Shutdown a service
//
// Returns:
//	NULL on error
//
opt_t *
get_options(int argc, char **argv)
{
	int	arg;
	int	extended_args	= 0;
	opt_t	*p_opt		= NULL;
	// Allocate memory for optios
	p_opt = new opt_t;
	if (p_opt == NULL) {
		os::printf("Memory allocation error.\n");
		return (NULL);
	}

	// Write in default values
	p_opt->hdrs		= 1;		// Display headers
	p_opt->show_repl	= 1;		// Include repl info
	p_opt->force		= false;	// Dont force change
	p_opt->service_desc	= NULL;
	p_opt->prov_desc	= NULL;
	p_opt->op		= RMCNTL_OP_DISPLAY;
	p_opt->numsecs		= 0;

	// Examine the command line
	optind = 1;
	while ((arg = getopt(argc, argv, "fgd:c:n:u:x:z:?")) != EOF) {
		// Switch on the option
		switch (arg) {
		case 'd' :
			// Display service
			extended_args = 1;
			p_opt->op = RMCNTL_OP_DISPLAY;
			p_opt->service_desc = new
				char[os::strlen(optarg) + 1];
			os::sprintf(p_opt->service_desc, "%s", optarg);
			break;
		case 'c' :
			//
			// This is a request to change state or set priority
			// for a provider. We will parse the extended args
			// later. Turn on the flag extended_args for now.
			//
			extended_args = 1;
			p_opt->service_desc = new char[os::strlen(optarg) + 1];
			os::sprintf(p_opt->service_desc, "%s", optarg);
			break;
		case 'n' :
			if (optind >= argc) {
				//
				// We have reached the end of arguments.
				// This means the user didn't specify
				// the new value of the desired numsecs.
				//
				usage(argv[0]);
				delete [] (p_opt->service_desc);
				delete [] (p_opt->prov_desc);
				delete (p_opt);
				return (NULL);
			} else {
				// Change desired num secondaries
				extended_args = 0;
				p_opt->op = RMCNTL_OP_SET_NUMSECS;
				p_opt->service_desc = new
					char[os::strlen(optarg) + 1];
				os::sprintf(p_opt->service_desc, "%s", optarg);
				p_opt->numsecs = (uint32_t)atoi(argv[optind]);
			}
			break;
		case 'x' :
			// Unregister service
			extended_args = 0;
			p_opt->op = RMCNTL_OP_SHUTDOWN;
			p_opt->service_desc = new
				char[os::strlen(optarg) + 1];
			os::sprintf(p_opt->service_desc, "%s", optarg);
			break;
		case 'f' :
			// Force state change
			p_opt->force = 1;
			break;
		case 'g':
			// Get list of frozen services
			p_opt->op = RMCNTL_OP_DISP_UPGRADE_FROZEN;
			break;
		case 'z' :
			p_opt->op = RMCNTL_OP_UPGRADE_FREEZE;
			p_opt->service_desc = new
			    char[os::strlen(optarg) + 1];
			os::sprintf(p_opt->service_desc, "%s", optarg);
			break;
		case 'u':
			p_opt->op = RMCNTL_OP_UPGRADE_UNFREEZE;
			p_opt->service_desc = new
			    char[os::strlen(optarg) + 1];
			os::sprintf(p_opt->service_desc, "%s", optarg);
			break;
		case '?' :
			usage(argv[0]);
			delete [] (p_opt->service_desc);
			delete [] (p_opt->prov_desc);
			delete (p_opt);
			return (NULL);
			/* NOT REACHED */
		default :
			os::printf("Unknown option: -%c\n", arg);
			delete [] (p_opt->service_desc);
			delete [] (p_opt->prov_desc);
			delete (p_opt);
			return (NULL);
		} // switch(arg)
	}

	if (p_opt->force == 1) {
		if (p_opt->op != RMCNTL_OP_SHUTDOWN) {
			// Force option can only be used in shutdown
			usage(argv[0]);
			delete [] (p_opt->service_desc);
			delete [] (p_opt->prov_desc);
			delete (p_opt);
			return (NULL);
		}
	}

	// Do we need more arguments
	if (!extended_args)
		return (p_opt);

	// If optind is >= to argc then we've reached the end of the
	// command line options
	if (optind >= argc)
		return (p_opt);

	// First free form argument is the replica description
	p_opt->prov_desc = new char[strlen(argv[optind]) + 1];
	os::sprintf(p_opt->prov_desc, "%s", argv[optind]);
	optind++;

	// If optind is >= to argc then we've reached the end of the
	// command line options
	if (optind >= argc)
		return (p_opt);

	// Second free form argument is the state/priority change
	if (isdigit(*argv[optind])) {
		//
		// must be a request to set priority because the argument
		// is a number.
		//
		p_opt->op = RMCNTL_OP_SET_PRI;
		p_opt->priority = (uint32_t)atoi(argv[optind]);
	} else {
		//
		// This is a state change request since the argument is a
		// string.
		//
		p_opt->op = RMCNTL_OP_STATE_CHANGE;
		if (os::strcmp(argv[optind], "primary") == 0) {
			p_opt->change_op = replica::SC_SET_PRIMARY;

		} else if (os::strcmp(argv[optind], "secondary") == 0) {
			p_opt->change_op = replica::SC_SET_SECONDARY;

		} else if (os::strcmp(argv[optind], "spare") == 0) {
			p_opt->change_op = replica::SC_SET_SPARE;

		} else if (os::strcmp(argv[optind], "remove") == 0) {
			p_opt->change_op = replica::SC_REMOVE_REPL_PROV;

		} else {
			// Invalid argument
			os::printf("Invalid state change requested: "
			    "%s\n", argv[optind]);
			delete[] (p_opt->service_desc);
			delete[] (p_opt->prov_desc);
			delete (p_opt);
			return (NULL);
		}
	}

	return (p_opt);
}

//
// do_rmcntl
// Examine the p_opt->opt, and take action
//
// Arguments:
//	The pointer to the p_opt structure
// Returns:
//	0 on success
int
do_rmcntl(opt_t *p_opt)
{
	int rslt = 0;

	ASSERT(p_opt != NULL);

	//
	// Switch on the operation
	//
	switch (p_opt->op) {
	case RMCNTL_OP_DISPLAY:
		rslt = do_display(p_opt);
		break;
	case RMCNTL_OP_STATE_CHANGE:
		rslt = do_state_change(p_opt);
		break;
	case RMCNTL_OP_SHUTDOWN:
		rslt = do_shutdown(p_opt);
		break;
	case RMCNTL_OP_SET_NUMSECS:
		rslt = do_numsecs_change(p_opt);
		break;
	case RMCNTL_OP_SET_PRI:
		rslt = do_pri_change(p_opt);
		break;
	case RMCNTL_OP_UPGRADE_FREEZE:
		rslt = do_upgrade_freeze(p_opt);
		break;
	case RMCNTL_OP_UPGRADE_UNFREEZE:
		rslt = do_upgrade_unfreeze(p_opt);
		break;
	case RMCNTL_OP_DISP_UPGRADE_FROZEN:
		rslt = do_display_upgrade_frozen();
		break;
	default :
		ASSERT(false);
		break;
	}

	return (rslt);
}

//
// Take action
//
// do_display
// Display the service+provs+state
//
// Arguments:
//	pointer to the option structure
// Returns:
//	0 on success
int
do_display(opt_t *p_opt)
{
	Environment		e;
	replica::rm_admin_var	rm_ref;

	// Get the reference to the replica framework
	rm_ref = rm_util::get_rm();

	if (CORBA::is_nil(rm_ref)) {
		os::printf("%s Could not find local rm\n",
		    message_prefix);
		return (-1);
	}

	// Are we printing all the services?
	if ((p_opt->service_desc == NULL) && (p_opt->prov_desc == NULL)) {
		replica::rm_service_list	*svc_list;
		svc_list = new replica::rm_service_list;

		// Get the services
		if (get_services(svc_list, rm_ref, e) != 0) {
			if (e.exception()) {
				e.exception()->print_exception(message_prefix);
			} else {
				os::printf("%s Could not get list of "
				    "services\n", message_prefix);
			}
			delete (svc_list);
			return (-1);
		}

		// Check if list is empty
		if (svc_list->length() == 0) {
			os::printf("%s No HA services found on cluster\n",
			    message_prefix);
			delete (svc_list);
			return (0);
		}

		if (!p_opt->show_repl)
			PRINT_HEADER

		// Print each service
		for (unsigned int idx = 0; idx < svc_list->length();
		    idx++) {
			if (p_opt->show_repl)
				PRINT_HEADER

			replica::service_admin_var	svc_admin_ref;
			replica::service_info		*svc_info = NULL;

			// Get the service control
			svc_admin_ref = get_control(
			    (*svc_list)[idx].service_name, rm_ref, e);
			svc_info = get_service_info(svc_admin_ref, e);
			// Print this service info
			print_service(svc_info);
			delete(svc_info);

			// Do we need to get the repl
			if (p_opt->show_repl) {
				// Get service admin
				svc_admin_ref = get_control(
				    (*svc_list)[idx].service_name, rm_ref,
				    e);
				if (e.exception()) {
					if (replica::unknown_service::_exnarrow(
					    e.exception()) != NULL) {
						os::printf("%s service "
						    "(%s) is unknown\n",
						    message_prefix,
				(char *)(*svc_list)[idx].service_name);

					} else {
						e.exception()->print_exception(
						    message_prefix);
					}
					e.clear();
					os::printf("\n");
					continue;
				}

				if (CORBA::is_nil(svc_admin_ref)) {
					os::printf("%s Could not get "
					    "service admin object.\n",
					    message_prefix);
					os::printf("\n");
					continue;
				}

				replica::repl_prov_seq *repl_list;
				repl_list = new replica::repl_prov_seq;

				// Get a list of repl_provs
				if (get_repl_provs(repl_list, svc_admin_ref, e)
				    != 0) {
					if (e.exception()) {
						e.exception()->print_exception(
						    message_prefix);
						e.clear();
					} else {
						os::printf("%s Could not "
						    "get list of replicas.\n",
						    message_prefix);
					}
					delete (repl_list);
					continue;
				}

				PRINT_REPL_HEADER

				for (unsigned int sindex = 0;
				    sindex < repl_list->length();
				    sindex++) {
					print_replica(&((*repl_list)[sindex]));
				} // Replica list iteration

				delete(repl_list);
				os::printf("\n");
			}
		}

		delete(svc_list);
		return (0);
	} // print all

	// Are we printing just a specific service
	if ((p_opt->service_desc != NULL) && (p_opt->prov_desc == NULL)) {
		replica::service_admin_var	svc_admin_ref;
		replica::service_info		*svc_info	= NULL;

		// Get the service control
		svc_admin_ref = get_control(p_opt->service_desc, rm_ref, e);
		if (e.exception()) {
			if (replica::unknown_service::_exnarrow(e.exception())
			    != NULL) {
				os::printf("%s service (%s) is unknown\n",
				    message_prefix, p_opt->service_desc);
			} else {
				e.exception()->print_exception(message_prefix);
			}

			return (-1);
		}

		if (CORBA::is_nil(svc_admin_ref)) {
			os::printf("%s Could not get service admin "
			    "object\n", message_prefix);
			return (-1);
		}

		// Print the contents info for this service
		svc_info = get_service_info(svc_admin_ref, e);
		if (e.exception()) {
			e.exception()->print_exception(message_prefix);
			return (-1);
		}
		if (svc_info == NULL) {
			os::printf("%s Could not get service info.\n",
			    message_prefix);
			return (-1);
		}

		PRINT_HEADER
		print_service(svc_info);

		// Do we need to get the replicas
		if (p_opt->show_repl) {
			replica::repl_prov_seq *repl_list;
			repl_list = new replica::repl_prov_seq;

			// Get a list of repl_provs
			if (get_repl_provs(repl_list, svc_admin_ref, e) != 0) {
				if (e.exception()) {
					e.exception()->print_exception(
						message_prefix);
				} else {
					os::printf("%s Could not get "
					    "list of replicas\n",
					    message_prefix);
				}
				delete (repl_list);
				return (-1);
			}

			PRINT_REPL_HEADER

			for (unsigned int sindex = 0;
			    sindex < repl_list->length(); sindex++) {
				print_replica(&((*repl_list)[sindex]));
			} // Replica list iteration

			delete(repl_list);
			os::printf("\n");
		}

		delete(svc_info);

		return (0);
	} // print a service

	// We must be printing a specific replica
	replica::repl_prov_seq		*repl_list	= NULL;
	replica::repl_prov_info		*repl_info	= NULL;
	replica::service_admin_var	svc_admin_ref;

	// Get the service control
	svc_admin_ref = get_control(p_opt->service_desc, rm_ref, e);
	if (e.exception()) {
		if (replica::unknown_service::_exnarrow(e.exception())
		    != NULL) {
			os::printf("%s service %s is unknown\n",
			    message_prefix, p_opt->service_desc);
		} else {
			e.exception()->print_exception(message_prefix);
		}

		return (-1);
	}

	if (CORBA::is_nil(svc_admin_ref)) {
		os::printf("%s Could not get service admin object\n",
		    message_prefix);
		return (-1);
	}

	repl_list = new replica::repl_prov_seq;

	// Get a list of repl_provs
	if (get_repl_provs(repl_list, svc_admin_ref, e) != 0) {
		if (e.exception()) {
			e.exception()->print_exception(message_prefix);
		} else {
			os::printf("%s Could not get list of replicas.\n",
			    message_prefix);
		}
		delete (repl_list);
		return (-1);
	}

	for (unsigned int sindex = 0; sindex < repl_list->length(); sindex++) {
		if (strcmp(p_opt->prov_desc,
			(*repl_list)[sindex].repl_prov_desc) == 0) {

			// This is the repl we're looking for
			repl_info = &((*repl_list)[sindex]);
			break;
		}
	} // Replica list iteration

	PRINT_REPL_HEADER

	if (repl_info != NULL) {
		print_replica(repl_info);
	} else {
		os::printf("%s replica (%s) does not match a registered "
		    "replica for this service.\n", message_prefix,
		    p_opt->prov_desc);
	}

	delete(repl_list);
	return (0);
}

int
do_numsecs_change(opt_t *p_opt)
{
	Environment			e;
	replica::rm_admin_var		rm_ref;
	replica::service_admin_var	svc_admin_ref;

	ASSERT(p_opt->service_desc != NULL);

	// Get the reference to the replica framework
	rm_ref = rm_util::get_rm();

	if (CORBA::is_nil(rm_ref)) {
		os::printf("%s Could not get rm.\n",
		    message_prefix);
		return (-1);
	}

	// Get the service control
	svc_admin_ref = get_control(p_opt->service_desc, rm_ref, e);
	if (e.exception()) {
		if (replica::unknown_service::_exnarrow(e.exception())
		    != NULL) {
			os::printf("%s service (%s) is unknown\n",
			    message_prefix, p_opt->service_desc);
		} else {
			e.exception()->print_exception(message_prefix);
		}

		return (-1);
	}

	if (CORBA::is_nil(svc_admin_ref)) {
		os::printf("%s Could not get service admin object.\n",
		    message_prefix);
		return (-1);
	}

	svc_admin_ref->change_desired_numsecondaries(p_opt->numsecs, e);
	if (e.exception()) {
		e.exception()->print_exception(message_prefix);
		return (-1);
	}
	return (0);
}

int
do_pri_change(opt_t *p_opt)
{
	Environment			e;
	replica::rm_admin_var		rm_ref;
	replica::service_admin_var	svc_admin_ref;

	ASSERT(p_opt->service_desc != NULL);
	ASSERT(p_opt->prov_desc != NULL);

	// Get the reference to the replica framework
	rm_ref = rm_util::get_rm();

	if (CORBA::is_nil(rm_ref)) {
		os::printf("%s Could not get rm.\n",
		    message_prefix);
		return (-1);
	}

	// Get the service control
	svc_admin_ref = get_control(p_opt->service_desc, rm_ref, e);
	if (e.exception()) {
		if (replica::unknown_service::_exnarrow(e.exception())
		    != NULL) {
			os::printf("%s service (%s) is unknown\n",
			    message_prefix, p_opt->service_desc);
		} else {
			e.exception()->print_exception(message_prefix);
		}

		return (-1);
	}

	if (CORBA::is_nil(svc_admin_ref)) {
		os::printf("%s Could not get service admin object.\n",
		    message_prefix);
		return (-1);
	}

	replica::prov_priority_list prov_list(1, 1);
	prov_list[0].prov_desc = os::strdup(p_opt->prov_desc);
	prov_list[0].priority = p_opt->priority;
	svc_admin_ref->change_repl_prov_priority(prov_list, e);
	if (e.exception()) {
		if (replica::invalid_repl_prov::_exnarrow(e.exception())
		    != NULL) {
			// invalid_repl_prov
			os::printf("%s replica (%s) does not "
			    "match a registered replica for this "
			    "service\n", message_prefix,
			    p_opt->prov_desc);

		} else if (
		    replica::service_admin::service_changed::_exnarrow(
		    e.exception()) != NULL) {
			// service_changed
			os::printf("%s: service changed state "
			    "during operation, verify state before "
			    "retrying operation\n", message_prefix);

		} else {
			e.exception()->print_exception(message_prefix);
		}
		return (-1);

	} else {
		os::printf("%s priority change request was made.\n",
		    message_prefix);
	}
	return (0);
}


// do_state_change
// Change state of a replica
//
// Arguments:
//	pointer to the option structure
int
do_state_change(opt_t *p_opt)
{
	Environment			e;
	replica::rm_admin_var		rm_ref;
	replica::service_admin_var	svc_admin_ref;

	ASSERT(p_opt->service_desc != NULL);
	ASSERT(p_opt->prov_desc != NULL);

	// Get the reference to the replica framework
	rm_ref = rm_util::get_rm();

	if (CORBA::is_nil(rm_ref)) {
		os::printf("%s Could not get rm.\n",
		    message_prefix);
		return (-1);
	}

	// Get the service control
	svc_admin_ref = get_control(p_opt->service_desc, rm_ref, e);
	if (e.exception()) {
		if (replica::unknown_service::_exnarrow(e.exception())
		    != NULL) {
			os::printf("%s service (%s) is unknown\n",
			    message_prefix, p_opt->service_desc);
		} else {
			e.exception()->print_exception(message_prefix);
		}

		return (-1);
	}

	if (CORBA::is_nil(svc_admin_ref)) {
		os::printf("%s Could not get service admin object.\n",
		    message_prefix);
		return (-1);
	}

	if (change_repl_prov_status(svc_admin_ref, p_opt->prov_desc,
	    p_opt->change_op, e) != 0) {
		if (e.exception()) {
			if (replica::invalid_repl_prov::_exnarrow(e.exception())
			    != NULL) {
				// invalid_repl_prov
				os::printf("%s replica (%s) does not "
				    "match a registered replica for this "
				    "service\n", message_prefix,
				    p_opt->prov_desc);

			} else if (
			    replica::service_admin::service_changed::_exnarrow(
			    e.exception()) != NULL) {
				// service_changed
				os::printf("%s: service changed state "
				    "during operation, verify state before "
				    "retrying operation\n", message_prefix);

			} else {
				e.exception()->print_exception(message_prefix);
			}

		} else {
			os::printf("%s Failed to request "
			    "replica state change.\n", message_prefix);
		}

		return (-1);
	} else {
		os::printf("%s State change request was made.\n",
		    message_prefix);
	}

	return (0);
}

// do_shutdown
// Shutdown a service
//
// Arguments:
//	pointer to the option structure
// Returns
//	0 if success
int
do_shutdown(opt_t *p_opt)
{
	Environment			e;
	replica::rm_admin_var		rm_ref;
	replica::service_admin_var	svc_admin_ref;
	int				rslt;

	ASSERT(p_opt->service_desc != NULL);

	// Get the reference to the replica framework
	rm_ref = rm_util::get_rm();

	if (CORBA::is_nil(rm_ref)) {
		os::printf("%s Could not get rm.\n",
		    message_prefix);
		return (-1);
	}

	// Get control
	svc_admin_ref = get_control(p_opt->service_desc, rm_ref, e);
	if (e.exception()) {
		if (replica::unknown_service::_exnarrow(e.exception())
		    != NULL) {
			os::printf("%s service (%s) is unknown\n",
			    message_prefix, p_opt->service_desc);
		} else {
			e.exception()->print_exception(message_prefix);
		}

		return (-1);
	}

	if (CORBA::is_nil(svc_admin_ref)) {
		os::printf("%s Could not get service admin object.\n",
		    message_prefix);
		return (-1);
	}

	// Do the shutdown
	// Do unregister
	if (p_opt->force) {
		rslt = shutdown_service(true, svc_admin_ref, e);
	} else {
		rslt = shutdown_service(false, svc_admin_ref, e);
	}
	if (e.exception()) {
		if (replica::depends_on::_exnarrow(e.exception()) != NULL) {
			// depends_on exception
			os::printf("%s service (%s) has dependencies\n",
			    message_prefix, p_opt->service_desc);

		} else if (replica::service_admin::service_changed::_exnarrow(
		    e.exception()) != NULL) {
			// service_changed
			os::printf("%s: service changed state during "
			    "operation, verify state before retrying "
			    "operation\n", message_prefix);

		} else {
			e.exception()->print_exception(message_prefix);
		}

		return (-1);
	} else {
		os::printf("Service \"%s\" %s shutdown.\n",
		    p_opt->service_desc,
		    ((rslt == 0)? "was" : "was not"));
	}

	return (rslt);
}

//
// do_upgrade_freeze
//
// Freeze the list of given services for upgrade.
//
int
do_upgrade_freeze(opt_t *p_opt)
{
	Environment	e;

	replica::rm_admin_var rm_ref = rm_util::get_rm();

	// Get the reference to the replica framework
	rm_ref = rm_util::get_rm();

	if (CORBA::is_nil(rm_ref)) {
		os::printf("%s Could not get rm.\n", message_prefix);
		return (-1);
	}
	rm_ref->freeze_for_upgrade(p_opt->service_desc, e);
	if (e.exception()) {
		e.exception()->print_exception(message_prefix);
		return (-1);
	}
	return (0);
}

//
// do_upgrade_unfreeze
//
// Unfreeze the list of given.
//
int
do_upgrade_unfreeze(opt_t *p_opt)
{
	Environment	e;

	replica::rm_admin_var rm_ref = rm_util::get_rm();

	// Get the reference to the replica framework
	rm_ref = rm_util::get_rm();

	if (CORBA::is_nil(rm_ref)) {
		os::printf("%s Could not get rm.\n", message_prefix);
		return (-1);
	}
	rm_ref->unfreeze_after_upgrade(p_opt->service_desc, e);
	if (e.exception()) {
		e.exception()->print_exception(message_prefix);
		return (-1);
	}

	return (0);
}

//
// do_display_upgrade_frozen
//
int
do_display_upgrade_frozen()
{
	Environment	e;

	replica::rm_admin_var rm_ref = rm_util::get_rm();

	// Get the reference to the replica framework
	rm_ref = rm_util::get_rm();

	if (CORBA::is_nil(rm_ref)) {
		os::printf("%s Could not get rm.\n", message_prefix);
		return (-1);
	}

	replica::service_frozen_seq *get_svcs = NULL;

	rm_ref->get_services_frozen_for_upgrade(get_svcs, e);

	if (e.exception()) {
		os::printf("%s Failed to obtain list of services frozen"
		    " for upgrade\n", message_prefix);
		return (-1);
	}
	if ((*get_svcs).length() != 0) {
		os::printf("Services frozen for upgrade: %d\n",
		    (*get_svcs).length());
	}

	for (uint_t i = 0; i < (*get_svcs).length(); i++) {
		os::printf("\t%s\n", (char *)(*get_svcs)[i]);
	}
	delete get_svcs;

	return (0);
}
//
// Helper routines
//
// print_service
// Print the passed in service
// Arguments
//	Pointer to the service info structure
void
print_service(replica::service_info *svc_info)
{
	const char state_str[8][25] = {
		"UP", "DOWN", "UNINIT", "FRZ_FOR_UPGD", "FRZ_FOR_UPGD_BY_REQ",
		"DOWN_FRZ_FOR_UPGD", "DOWN_FRZ_FOR_UPGD_REQ", "UNSTABLE"
	};
	unsigned int idx;
	os::printf("%-4ld %-25s %-25s %ld\n", svc_info->sid,
	    (char *)svc_info->service_name, state_str[svc_info->s_state],
	    svc_info->desired_numsecondaries);

	for (idx = 0; idx < svc_info->this_depends_on.length(); idx++) {
		if ((char *)svc_info->this_depends_on[idx] != NULL) {
			os::printf("%-4s %-10s : %s\n", " ", "Depends on",
			    (char *)svc_info->this_depends_on[0]);
		}
	}

	for (idx = 0; idx < svc_info->depend_on_this.length(); idx++) {
		if ((char *)svc_info->depend_on_this[idx] != NULL) {
			os::printf("%-4s %-10s : %s\n", " ", "Dependents",
			    (char *)svc_info->depend_on_this[idx]);
		}
	}

}

//
// Helper routine
//
// print_replica
// Prints the passed in replica info
// Arguments
//	Pointer to the replica info
void
print_replica(replica::repl_prov_info *repl_info)
{
	const char state_str[4][10] = {
		"DOWN", "SPARE", "SECONDARY", "PRIMARY"
	};
	os::printf("     %-25s %-25s %d\n", (char *)repl_info->repl_prov_desc,
	    state_str[repl_info->curr_state], repl_info->priority);
}
