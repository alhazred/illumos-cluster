//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)replctl_main.cc	1.12	08/05/20 SMI"

#include <sys/os.h>
#include <orb/invo/common.h>
#ifndef _KERNEL_ORB
#include <unistd.h>
#include <orb/infrastructure/orb.h>
#endif

#include <sys/sol_version.h>
#if SOL_VERSION >= __s10
#include <zone.h>
#endif

#include "replctl.h"

//
// Contains main routine for rmcntl admin tool
//
#if !defined(_KERNEL) && defined(_KERNEL_ORB)
int
replctl(int argc, char **argv)
#else
int
main(int argc, char **argv)
#endif // !_KERNEL && _KERNEL_ORB
{
	opt_t	*p_opt = NULL;	// Command line options
	int	rslt;

#if SOL_VERSION >= __s10
	if (getzoneid() != 0) {
		(void) fprintf(stderr,
		    "You cannot run this command from a non-global "
		    "zone.\n");
		return (1);
	}
#endif

#if !defined(_KERNEL_ORB)
	if (ORB::initialize() != 0) {
		os::printf("%s: Can't Initialize ORB\n", argv[0]);
		return (1);
	}
#endif // !_KERNEL_ORB

	// Get command line options
	p_opt = get_options(argc, argv);
	if (p_opt == NULL) {
		return (1);
	}

	// Take action
	rslt = do_rmcntl(p_opt);

	// Delete the opt structure
	delete[] (p_opt->service_desc);
	delete[] (p_opt->prov_desc);
	delete (p_opt);

#ifdef _KERNEL_ORB
	return (rslt);
#else
	// XXX Workaround for the fact that ORB::shutdown() causes a failure
	// which makes the test think we didn't succeed.
	_exit(rslt);
#endif
} //lint !e533


#if !defined(_KERNEL) && defined(_KERNEL_ORB)

//
// unode_init(void)
//
int
unode_init(void)
{
	os::printf("replctl UNODE module loaded\n");
	return (0);
}

#endif // !_KERNEL && _KERNEL_ORB
