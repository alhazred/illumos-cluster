//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
#pragma ident	"@(#)get_oldest_nodeid.cc	1.7	08/05/20 SMI"


// Implements a command that prints out the node id for the ha name server.
// This is needed by the cluster time project to determine the timehost.
// Returns 0 on success, 1 on error.

#include <sys/os.h>
#include <orb/infrastructure/orb.h>
#include <sys/rm_util.h>
#include <h/replica.h>
#include <h/naming.h>
#include <rgm/sczones.h>

const char *name_service_name = "repl_name_server";

int
main(int, char **argv)
{
	if (sc_zonescheck() != 0)
		return (1);

	if (ORB::initialize() != 0) {
		os::printf("%s: Can't Initialize ORB\n", argv[0]);
		return (1);
	}

	// Get to the rm.
	replica::rm_admin_var rm_ref;
	rm_ref = rm_util::get_rm();

	if (CORBA::is_nil(rm_ref)) {
		os::printf("Could not find replica manager\n");
		return (1);
	}

	// Get the service admin.
	Environment e;
	replica::service_admin_var sa_ref;
	sa_ref = rm_ref->get_control(name_service_name, e);
	if (e.exception()) {
		if (replica::unknown_service::_exnarrow(e.exception())
		    != NULL) {
			os::printf("service (%s) is unknown\n",
			    name_service_name);
		} else {
			e.exception()->print_exception("Exception finding "
			    "name service");
		}

		return (1);
	}

	// Get the list of the repl providers.
	replica::repl_prov_seq *repl_list = NULL;
	sa_ref->get_repl_provs(repl_list, e);

	if (e.exception()) {
		e.exception()->print_exception("Get the list "
		    "of providers");
		return (1);
	}

	// From the list of providers get the primary.
	for (unsigned int i = 0; i < repl_list->length(); i++) {
		if ((*repl_list)[i].curr_state == replica::AS_PRIMARY) {
			// Found the primary.
			os::printf("%s\n",
			    (char *)(*repl_list)[i].repl_prov_desc);
			delete (repl_list);
			return (0);
		}
	}

	// Primary not found.
	os::printf("Can't find the primary for the name service");
	delete (repl_list);
	return (1);
}
