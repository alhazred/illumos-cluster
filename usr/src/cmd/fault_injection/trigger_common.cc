/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)trigger_common.cc	1.16	08/05/20 SMI"

#if defined(_FAULT_INJECTION)

//
// Common routines for the trigger_* utilities.
//

#include <sys/types.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <limits.h>
#include <string.h>
#include <errno.h>

#include "trigger_common.h"

//
// Global structure for storing options and arguments.
//

opts_args_t	opts_args;

static void	append_fault_arg(void *arg, size_t len);
static int	parse_fault_arg_char(char *arg);
static int	parse_fault_arg_int(char *arg);
static int	parse_fault_arg_uint(char *arg);
static int	parse_fault_arg_short(char *arg);
static int	parse_fault_arg_ushort(char *arg);
static int	parse_fault_arg_long(char *arg);
static int	parse_fault_arg_ulong(char *arg);
static int	parse_fault_arg_longlong(char *arg);
static int	parse_fault_arg_ulonglong(char *arg);
static int	parse_fault_arg_string(char *arg);

//
// parse_node_ids()
//
//	Parses the -n option argument which specifies one or more node ID's
//	separated by commas.
//
// Parameters:
//	arg	-- points to the option argument.
//
// Returns:
//	0 if successful, -1 otherwise.
//
// Side Effects:
//	Modifies the string pointed to by 'arg' (strtok() is used).
//	Stores the parsed node ID's in a dynamically-allocated array.
//	If successful, the global opts_args.node_ids structure member will
//	point to this array, and opts_args.num_node_ids will contain the
//	number of node ID's in the array.
//	Prints messages to stderr when an error occurs.
//

int
parse_node_ids(char *arg)
{
	int	*nids = NULL;			// array of node IDs
	int	arrsize = 0, num_nids = 0;
	int	failed = 0;
	char	*ptr;

	// One or more node IDs separated by commas.
	ptr = strtok(arg, ",");
	do {
		char	*endptr;
		long	value;

		// If array needs to be enlarged...
		if (num_nids >= arrsize) {
			arrsize += 64;
			nids = (int *)realloc(nids,
						arrsize * sizeof (nids[0]));
			if (nids == NULL) {
				perror("ERROR: realloc() failed");
				failed = 1;
				break;
			}
		}

		// If current node...
		if (strcasecmp(ptr, "this") == 0) {
			nids[num_nids++] = TRIGGER_THIS_NODE;
			continue;
		}

		// If all nodes...
		if (strcasecmp(ptr, "all") == 0) {
			// "all" takes precedence.
			nids[0] = TRIGGER_ALL_NODES;
			num_nids = 1;
			break;				// we're done
		}

		// Else convert to number.
		errno = 0;
		value = strtol(ptr, &endptr, 0);
		if (errno) {
			perror("ERROR: invalid node ID");
			failed = 1;
			break;
		}

		// If there are extra characters...
		if (*endptr != '\0') {
			fprintf(stderr, "ERROR: invalid node ID\n");
			failed = 1;
			break;
		}

		// Make sure node ID is a positive value.
		if (value <= 0) {
			fprintf(stderr, "ERROR: node ID must be > 0\n");
			failed = 1;
			break;
		}

		nids[num_nids++] = value;
	} while (ptr = strtok(NULL, ","));

	if (failed) {
		free(nids);
		return (-1);
	}

	opts_args.node_ids = nids;
	opts_args.num_node_ids = num_nids;
	return (0);
}

//
// parse_generic_op()
//
//	Parses the generic operation to take when fault triggers
//
// Parameters:
//	arg	-- points to the command line argument
//
// Returns:
//	0 if succesful, -1 otherwise
//
// Side effects:
//	Stores the result sin the global opts_args.generic_op structure member
//	Prints messages to stderr when an error occurs

int
parse_generic_op(char *arg)
{
	// Set the flag up
	opts_args.generic = 1;

	if (strcmp(arg, "panic") == 0) {
		opts_args.generic_op = FaultFunctions::PANIC;

	} else if (strcmp(arg, "reboot") == 0) {
		opts_args.generic_op = FaultFunctions::REBOOT;

	} else if (strcmp(arg, "reboot_once") == 0) {
		opts_args.generic_op = FaultFunctions::REBOOT_ONCE;

	} else if (strcmp(arg, "reboot_specified") == 0) {
		opts_args.generic_op = FaultFunctions::REBOOT_SPECIFIED;

	} else if (strcmp(arg, "cmm_reboot") == 0) {
		opts_args.generic_op = FaultFunctions::CMM_REBOOT;

	} else if (strcmp(arg, "sleep") == 0) {
		opts_args.generic_op = FaultFunctions::SLEEP;

	} else if (strcmp(arg, "pxfs_sleep") == 0) {
		opts_args.generic_op = FaultFunctions::PXFS_SLEEP;

	} else if (strcmp(arg, "pxfs_resolve") == 0) {
		opts_args.generic_op = FaultFunctions::PXFS_RESOLVE;

	} else if (strcmp(arg, "sleep_once") == 0) {
		opts_args.generic_op = FaultFunctions::SLEEP_ONCE;

	} else if (strcmp(arg, "sema_p") == 0) {
		opts_args.generic_op = FaultFunctions::SEMA_P;

	} else if (strcmp(arg, "sema_p_once") == 0) {
		opts_args.generic_op = FaultFunctions::SEMA_P_ONCE;

	} else if (strcmp(arg, "sema_p_specified") == 0) {
		opts_args.generic_op = FaultFunctions::SEMA_P_SPECIFIED;

	} else if (strcmp(arg, "sema_v") == 0) {
		opts_args.generic_op = FaultFunctions::SEMA_V;

	} else if (strcmp(arg, "sema_v_once") == 0) {
		opts_args.generic_op = FaultFunctions::SEMA_V_ONCE;

	} else if (strcmp(arg, "sema_v_specified") == 0) {
		opts_args.generic_op = FaultFunctions::SEMA_V_SPECIFIED;

	} else if (strcmp(arg, "switchover") == 0) {
		opts_args.generic_op = FaultFunctions::SWITCHOVER;

	} else {
		fprintf(stderr, "ERROR: invalid generic op type\n");
		return (-1);
	}

	return (0);
}

//
// parse_fault_num()
//
//	Parses the fault number argument.
//
// Parameters:
//	arg	-- points to the command line argument.
//
// Returns:
//	0 if successful, -1 otherwise.
//
// Side Effects:
//	Stores the result in the global opts_args.fault_num structure member.
//	Prints messages to stderr when an error occurs.
//

int
parse_fault_num(char *arg)
{
	char	*endptr;

	errno = 0;
	opts_args.fault_num = strtoul(arg, &endptr, 0);
	if (errno) {
		perror("ERROR: invalid fault number");
		return (-1);
	}

	// If there are extra characters...
	if (*endptr != '\0') {
		fprintf(stderr, "ERROR: invalid fault number\n");
		return (-1);
	}

	return (0);
}

//
// append_fault_arg()
//
//	Appends a fault argument to opts_args.fault_argp.
//
// Parameters:
//	arg	-- points to the fault argument.
//	len	-- length, in bytes, of arguments to append.
//
// Returns:
//	None.
//

static void
append_fault_arg(void *arg, size_t len)
{
	if (len == 0) {
		return;
	}

	opts_args.fault_argp = realloc(opts_args.fault_argp,
						opts_args.fault_argsize + len);
	memcpy((char *)opts_args.fault_argp + opts_args.fault_argsize, arg,
									len);
	opts_args.fault_argsize += len;			// update size
}

//
// parse_fault_arg()
//
//	Parses and converts the fault argument command-line argument.
//	The conversion performed is based on the argument's type specifier
//	(see usage[] for trigger_add for more info).
//
// Parameters:
//	arg	-- points to the command line argument.
//
// Returns:
//	0 if successful, -1 otherwise.
//
// Side Effects:
//	Modifies the string pointed to by 'arg' (changes 1st ':' to '\0').
//	Calls the following functions to do the actual conversion:
//
//		parse_fault_arg_char()		(type: char)
//		parse_fault_arg_int()		(type: int)
//		parse_fault_arg_uint()		(type: unsigned int)
//		parse_fault_arg_short()		(type: short)
//		parse_fault_arg_ushort()	(type: unsigned short)
//		parse_fault_arg_long()		(type: long)
//		parse_fault_arg_ulong()		(type: unsigned long)
//		parse_fault_arg_longlong()	(type: long long)
//		parse_fault_arg_ulonglong()	(type: unsigned long long)
//		parse_fault_arg_string()	(type: string)
//
//	Prints messages to stderr when an error occurs.
//

int
parse_fault_arg(char *arg)
{
	char	*colon, *after_colon;

	// If arg doesn't begin with '%', assume string fault arg (default).
	if (arg[0] != '%') {
		return (parse_fault_arg_string(arg));
	}

	// Find the ':' delimiter.
	colon = strchr(arg, ':');
	if (colon == NULL) {
		fprintf(stderr, "ERROR: unknown type specifier in fault "
			"argument\n");
		return (-1);
	}

	*colon = '\0';			// separate type spec
	after_colon = colon + 1;	// string following ':'

	//
	// Convert fault arg based on type specifier.
	//

	// char
	if (strcasecmp(arg, "%c") == 0) {
		return (parse_fault_arg_char(after_colon));
	}

	// int
	if (strcasecmp(arg, "%d") == 0 || strcasecmp(arg, "%i") == 0) {
		return (parse_fault_arg_int(after_colon));
	}

	// unsigned int
	if (strcasecmp(arg, "%u") == 0) {
		return (parse_fault_arg_uint(after_colon));
	}

	// short
	if (strcasecmp(arg, "%hd") == 0 || strcasecmp(arg, "%hi") == 0) {
		return (parse_fault_arg_short(after_colon));
	}

	// unsigned short
	if (strcasecmp(arg, "%hu") == 0) {
		return (parse_fault_arg_ushort(after_colon));
	}

	// long
	if (strcasecmp(arg, "%ld") == 0 || strcasecmp(arg, "%li") == 0) {
		return (parse_fault_arg_long(after_colon));
	}

	// unsigned long
	if (strcasecmp(arg, "%lu") == 0) {
		return (parse_fault_arg_ulong(after_colon));
	}

	// long long
	if (strcasecmp(arg, "%lld") == 0 || strcasecmp(arg, "%lli") == 0) {
		return (parse_fault_arg_longlong(after_colon));
	}

	// unsigned long long
	if (strcasecmp(arg, "%llu") == 0) {
		return (parse_fault_arg_ulonglong(after_colon));
	}

	// string
	if (strcasecmp(arg, "%s") == 0) {
		return (parse_fault_arg_string(after_colon));
	}

	// Otherwise it's an error.
	fprintf(stderr, "ERROR: unknown type specifier \"%s\" in "
		"fault argument\n", arg);
	return (-1);
}

static int
parse_fault_arg_char(char *arg)
{
	append_fault_arg(arg, sizeof (char));
	return (0);
}

static int
parse_fault_arg_int(char *arg)
{
	int	flt_arg;

	// Make sure there is fault argument.
	if (*arg == '\0') {
		fprintf(stderr, "ERROR: invalid int fault argument\n");
		return (-1);
	}

	// As a convenience allow user to specify "INT_MAX" or "INT_MIN".
	if (strcasecmp(arg, "INT_MAX") == 0) {
		flt_arg = INT_MAX;
	} else if (strcasecmp(arg, "INT_MIN") == 0) {
		flt_arg = INT_MIN;
	} else {
		long	value;
		char	*endptr;

		errno = 0;
		value = strtol(arg, &endptr, 0);
		if (errno) {
			perror("ERROR: invalid int fault argument");
			return (-1);
		}

		// If there are extra characters...
		if (*endptr != '\0') {
			fprintf(stderr, "ERROR: invalid int fault argument\n");
			return (-1);
		}

		// In case sizeof(long) > sizeof(int).
		if (value > INT_MAX || value < INT_MIN) {
			fprintf(stderr, "ERROR: invalid int fault argument: "
				"%s\n", strerror(ERANGE));
			return (-1);
		}

		flt_arg = (int)value;
	}

	append_fault_arg(&flt_arg, sizeof (flt_arg));
	return (0);
}

static int
parse_fault_arg_uint(char *arg)
{
	unsigned int	flt_arg;

	// Make sure there is fault argument.
	if (*arg == '\0') {
		fprintf(stderr, "ERROR: invalid unsigned int "
			"fault argument\n");
		return (-1);
	}

	// As a convenience allow user to specify "UINT_MAX" or "UINT_MIN".
	if (strcasecmp(arg, "UINT_MAX") == 0) {
		flt_arg = UINT_MAX;
	} else if (strcasecmp(arg, "UINT_MIN") == 0) {
		flt_arg = 0;			// UINT_MIN is 0
	} else {
		unsigned long	value;
		char		*endptr;

		errno = 0;
		value = strtoul(arg, &endptr, 0);
		if (errno) {
			perror("ERROR: invalid unsigned int fault argument");
			return (-1);
		}

		// If there are extra characters...
		if (*endptr != '\0') {
			fprintf(stderr, "ERROR: invalid unsigned int "
				"fault argument\n");
			return (-1);
		}

		// In case sizeof(ulong_t) > sizeof(uint_t).
		if (value > UINT_MAX) {
			fprintf(stderr, "ERROR: invalid unsigned int "
				"fault argument: %s\n", strerror(ERANGE));
			return (-1);
		}

		flt_arg = (unsigned int) value;
	}

	append_fault_arg(&flt_arg, sizeof (flt_arg));
	return (0);
}

static int
parse_fault_arg_short(char *arg)
{
	short	flt_arg;

	// Make sure there is fault argument.
	if (*arg == '\0') {
		fprintf(stderr, "ERROR: invalid short fault argument\n");
		return (-1);
	}

	// As a convenience allow user to specify "SHRT_MAX" or "SHRT_MIN".
	if (strcasecmp(arg, "SHRT_MAX") == 0) {
		flt_arg = SHRT_MAX;
	} else if (strcasecmp(arg, "SHRT_MIN") == 0) {
		flt_arg = SHRT_MIN;
	} else {
		long	value;
		char	*endptr;

		errno = 0;
		value = strtol(arg, &endptr, 0);
		if (errno) {
			perror("ERROR: invalid short fault argument");
			return (-1);
		}

		// If there are extra characters...
		if (*endptr != '\0') {
			fprintf(stderr, "ERROR: invalid short "
				"fault argument\n");
			return (-1);
		}

		// Verify range.
		if (value > SHRT_MAX || value < SHRT_MIN) {
			fprintf(stderr, "ERROR: invalid short "
				"fault argument: %s\n", strerror(ERANGE));
			return (-1);
		}

		flt_arg = (short)value;
	}

	append_fault_arg(&flt_arg, sizeof (flt_arg));
	return (0);
}

static int
parse_fault_arg_ushort(char *arg)
{
	unsigned short	flt_arg;

	// Make sure there is fault argument.
	if (*arg == '\0') {
		fprintf(stderr, "ERROR: invalid unsigned short "
			"fault argument\n");
		return (-1);
	}

	// As a convenience allow user to specify "USHRT_MAX" or "USHRT_MIN".
	if (strcasecmp(arg, "USHRT_MAX") == 0) {
		flt_arg = USHRT_MAX;
	} else if (strcasecmp(arg, "USHRT_MIN") == 0) {
		flt_arg = 0;			// USHRT_MIN is 0
	} else {
		unsigned long	value;
		char		*endptr;

		errno = 0;
		value = strtoul(arg, &endptr, 0);
		if (errno) {
			perror("ERROR: invalid unsigned short fault argument");
			return (-1);
		}

		// If there are extra characters...
		if (*endptr != '\0') {
			fprintf(stderr, "ERROR: invalid unsigned short "
				"fault argument\n");
			return (-1);
		}

		// Verify range.
		if (value > USHRT_MAX) {
			fprintf(stderr, "ERROR: invalid unsigned short "
				"fault argument: %s\n", strerror(ERANGE));
			return (-1);
		}

		flt_arg = (unsigned short) value;
	}

	append_fault_arg(&flt_arg, sizeof (flt_arg));
	return (0);
}

static int
parse_fault_arg_long(char *arg)
{
	long	flt_arg;

	// Make sure there is fault argument
	if (*arg == '\0') {
		fprintf(stderr, "ERROR: invalid long fault argument\n");
		return (-1);
	}

	// As a convenience allow user to specify "LONG_MAX" or "LONG_MIN".
	if (strcasecmp(arg, "LONG_MAX") == 0) {
		flt_arg = LONG_MAX;
	} else if (strcasecmp(arg, "LONG_MIN") == 0) {
		flt_arg = LONG_MIN;
	} else {
		char	*endptr;

		errno = 0;
		flt_arg = strtol(arg, &endptr, 0);
		if (errno) {
			perror("ERROR: invalid long fault argument");
			return (-1);
		}

		// If there are extra characters...
		if (*endptr != '\0') {
			fprintf(stderr, "ERROR: invalid long "
				"fault argument\n");
			return (-1);
		}
	}

	append_fault_arg(&flt_arg, sizeof (flt_arg));
	return (0);
}

static int
parse_fault_arg_ulong(char *arg)
{
	unsigned long	flt_arg;

	// Make sure there is fault argument.
	if (*arg == '\0') {
		fprintf(stderr, "ERROR: invalid unsigned long "
			"fault argument\n");
		return (-1);
	}

	// As a convenience allow user to specify "ULONG_MAX" or "ULONG_MIN".
	if (strcasecmp(arg, "ULONG_MAX") == 0) {
		flt_arg = ULONG_MAX;
	} else if (strcasecmp(arg, "ULONG_MIN") == 0) {
		flt_arg = 0;			// ULONG_MIN is 0
	} else {
		char	*endptr;

		errno = 0;
		flt_arg = strtoul(arg, &endptr, 0);
		if (errno) {
			perror("ERROR: invalid unsigned long fault argument");
			return (-1);
		}

		// If there are extra characters...
		if (*endptr != '\0') {
			fprintf(stderr, "ERROR: invalid unsigned long "
				"fault argument\n");
			return (-1);
		}
	}

	append_fault_arg(&flt_arg, sizeof (flt_arg));
	return (0);
}

static int
parse_fault_arg_longlong(char *arg)
{
	long long	flt_arg;

	// Make sure there is fault argument.
	if (*arg == '\0') {
		fprintf(stderr, "ERROR: invalid long long "
			"fault argument\n");
		return (-1);
	}

	// As a convenience allow user to specify "LLONG_MAX" or "LLONG_MIN".
	if (strcasecmp(arg, "LLONG_MAX") == 0) {
		flt_arg = LLONG_MAX;
	} else if (strcasecmp(arg, "LLONG_MIN") == 0) {
		flt_arg = LLONG_MIN;
	} else {
		char	*endptr;

		errno = 0;
		flt_arg = strtoll(arg, &endptr, 0);
		if (errno) {
			perror("ERROR: invalid long long fault argument");
			return (-1);
		}

		// If there are extra characters...
		if (*endptr != '\0') {
			fprintf(stderr, "ERROR: invalid long long "
				"fault argument\n");
			return (-1);
		}
	}

	append_fault_arg(&flt_arg, sizeof (flt_arg));
	return (0);
}

static int
parse_fault_arg_ulonglong(char *arg)
{
	unsigned long long	flt_arg;

	// Make sure there is fault argument.
	if (*arg == '\0') {
		fprintf(stderr, "ERROR: invalid unsigned long long "
			"fault argument\n");
		return (-1);
	}

	// As a convenience allow user to set "ULLONG_MAX" or "ULLONG_MIN".
	if (strcasecmp(arg, "ULLONG_MAX") == 0) {
		flt_arg = ULLONG_MAX;
	} else if (strcasecmp(arg, "ULLONG_MIN") == 0) {
		flt_arg = 0;			// ULLONG_MIN is 0
	} else {
		char	*endptr;

		errno = 0;
		flt_arg = strtoull(arg, &endptr, 0);
		if (errno) {
			perror("ERROR: invalid unsigned long long "
				"fault argument");
			return (-1);
		}

		// If there are extra characters...
		if (*endptr != '\0') {
			fprintf(stderr, "ERROR: invalid unsigned long long "
				"fault argument\n");
			return (-1);
		}
	}

	append_fault_arg(&flt_arg, sizeof (flt_arg));
	return (0);
}

static int
parse_fault_arg_string(char *arg)
{
	append_fault_arg(arg, strlen(arg) + 1);
	return (0);
}

#endif	// _FAULT_INJECTION
