/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)trigger_clear_all.cc	1.9	08/05/20 SMI"

#include <sys/types.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <limits.h>
#include <string.h>
#include <errno.h>

#if defined(_FAULT_INJECTION)

//
// Utility to allow users to clear all Node triggers from the command line.
//

#include "trigger_common.h"


/* BEGIN CSTYLED */
const char usage[] = "\
Usage:\n\
  trigger_clear_all [-n <node_ids>]\n\
\n\
Options and Arguments:\n\
  -n <node_ids>\n\
	ID(s) of the node(s) from which all Node triggers are to be cleared.\n\
	<node_ids> can be one of:\n\
\n\
		One or more positive numbers separated by commas.\n\
		\"this\" -- the current node.\n\
		\"all\"  -- all nodes in the cluster.\n\
\n\
	If no -n option is specified, the current node is assumed.\n\
";
/* END CSTYLED */

static int parse_args(int argc, char *argv[]);

//
// main()
//
main(int argc, char *argv[])
{
	if (sc_zonescheck() != 0)
		return (1);
	if (ORB::initialize() != 0) {
		fprintf(stderr, "ERROR: Can't initialize ORB\n");
		return (1);
	}

	if (parse_args(argc, argv) < 0) {
		return (1);
	}

	// Clear all Node triggers from each node specified.
	for (int i = 0; i < opts_args.num_node_ids; ++i) {
		NodeTriggers::clear_all(opts_args.node_ids[i]);
	}

	return (0);
}

//
// parse_args()
//
//	Parse command line arguments.
//
// Parameters:
//	argc	-- argument count from main().
//	argv	-- argument list from main().
//
// Returns:
//	0 if successful, -1 otherwise.
//
// Side Effects:
//	Stores the results in the global structure opts_args.
//	Prints message to stderr when an error occurs.
//

static int
parse_args(int argc, char *argv[])
{
	static int	this_node = TRIGGER_THIS_NODE;
	int		opt;

	// Initialize opts_args struct.
	opts_args.node_ids = &this_node;	// default: this node
	opts_args.num_node_ids = 1;

	// Parse options.
	while ((opt = getopt(argc, argv, "hn:")) != EOF) {
		switch (opt) {
		case 'n':
			if (parse_node_ids(optarg) < 0) {
				return (-1);
			}
			break;

		case 'h':
		case '?':
		default:
			fputs(usage, stderr);
			return (-1);
		}
	}

	return (0);
}

#else	// _FAULT_INJECTION

main()
{
	fprintf(stderr, "Fault injection not supported\n");
	return (1);
}

#endif	// _FAULT_INJECTION
