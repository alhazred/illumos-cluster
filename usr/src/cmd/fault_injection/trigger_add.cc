/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)trigger_add.cc	1.21	08/05/20 SMI"

#include <sys/types.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <limits.h>
#include <string.h>
#include <errno.h>

#if defined(_FAULT_INJECTION)

#include <nslib/ns.h>
#include <nslib/naming_context_impl.h>
#include <nslib/data_container_impl.h>

//
// Utility to allow users to add Node triggers from the command line.
//

#include "trigger_common.h"

/* BEGIN CSTYLED */
const char usage[] = "\
Usage:\n\
  trigger_add [-n <node_ids>] [-g <op>] <fault_num> [[<type_spec>:]<fault_arg>]...\n\
\n\
Options and Arguments:\n\
  -n <node_ids>\n\
	ID(s) of the node(s) to which the Node trigger is to be added.\n\
	<node_ids> can be one of:\n\
\n\
		One or more positive numbers separated by commas.\n\
		\"this\" -- the current node.\n\
		\"all\"  -- all nodes in the cluster.\n\
\n\
	If no -n option is specified, the current node is assumed.\n\
  -g <op>\n\
	Specifies the operand to the generic fault point.  (Generic\n\
	fault points are those that point to FaultFunctions::generic)\n\
\n\
		op can be one of:\n\
		\"panic\"\n\
		\"reboot\"\n\
		\"reboot_once\"\n\
		\"reboot_specified\"\n\
		\"cmm_reboot\"\n\
		\"sleep\"\n\
		\"bind_sleep\"\n\
		\"sleep_once\"\n\
		\"sema_p\"\n\
		\"sema_p_once\"\n\
		\"sema_p_specified\"\n\
		\"sema_v\"\n\
		\"sema_v_once\"\n\
		\"sema_v_specified\"\n\
		\"switchover\"\n\
\n\
	See the file src/orb/fault_function.h for more info.\n\
	NOTE: You must make sure that the fault arguments specifed on the\n\
	command line match the expected arguments of the fault function\n\
	specified by op\n\
  <fault_num>\n\
	Fault number associated with the Node trigger.\n\
  <fault_arg>\n\
	Fault argument associated with the Node trigger.\n\
  <type_spec>:\n\
	Specifies how to interpret <fault_arg>.  <type_spec> is similar to\n\
	printf()'s format directives and it can be one of the following:\n\
\n\
		%c          - char (only 1st character after ':' is used)\n\
		%d, %i      - int\n\
		%u          - unsigned int\n\
		%hd, %hi    - short\n\
		%hu         - unsigned short\n\
		%ld, %li    - long\n\
		%lu         - unsigned long\n\
		%lld, %lli  - long long\n\
		%llu        - unsigned long long\n\
		%s          - string\n\
\n\
	If no <type_spec>: is specified, the fault argument is interpreted\n\
	as a string.\n\
\n\
	NOTE: [<type_spec>:]<fault_arg> can be repeated several times for \n\
	multiple fault arguments\n\
";
/* END CSTYLED */

static int parse_args(int argc, char *argv[]);
static int resolve_name(uint32_t fault_number);

//
// main()
//

main(int argc, char *argv[])
{

	if (sc_zonescheck() != 0)
		return (1);

	if (ORB::initialize() != 0) {
		fprintf(stderr, "ERROR: Can't initialize ORB\n");
		return (1);
	}

	// Set the generic flag down
	opts_args.generic = 0;

	if (parse_args(argc, argv) < 0) {
		return (1);
	}

	// Add the Node trigger to each node specified.
	for (int i = 0; i < opts_args.num_node_ids; ++i) {
		if (!opts_args.generic) {
			NodeTriggers::add(opts_args.fault_num,
					opts_args.fault_argp,
					opts_args.fault_argsize,
					opts_args.node_ids[i]);
		} else {

			// It doesn't add any triggers, just finds
			// out if nameserver can resolve an object

			// This is used when PXFS FI tests use PXFS_SLEEP as
			// the fault function which binds an object to
			// the nameserver

			if (opts_args.generic_op !=
				FaultFunctions::PXFS_RESOLVE) {
				FaultFunctions::node_trigger_add(
					opts_args.generic_op,
					opts_args.fault_num,
					opts_args.fault_argp,
					opts_args.fault_argsize,
					opts_args.node_ids[i]);
			} else {
				if (resolve_name(opts_args.fault_num) < 0) {
					return (-1);
				}
			}
		}
	}

	return (0);
}

//
// parse_args()
//
//	Parse command line arguments.
//
// Parameters:
//	argc	-- argument count from main().
//	argv	-- argument list from main().
//
// Returns:
//	0 if successful, -1 otherwise.
//
// Side Effects:
//	Stores the results in the global structure opts_args.
//	Prints message to stderr when an error occurs.
//

static int
parse_args(int argc, char *argv[])
{
	static int	this_node = TRIGGER_THIS_NODE;
	int		opt;

	// Initialize opts_args struct.
	opts_args.node_ids = &this_node;	// default: this node
	opts_args.num_node_ids = 1;
	opts_args.fault_num = 0;
	opts_args.fault_argp = NULL;		// default: no fault arg
	opts_args.fault_argsize = 0;		// default: no fault arg

	// Make sure there is at least one argument (fault number).
	if (argc < 2) {
		fputs(usage, stderr);
		return (-1);
	}

	// Parse options.
	while ((opt = getopt(argc, argv, "g:hn:")) != EOF) {
		switch (opt) {
		case 'n':
			if (parse_node_ids(optarg) < 0) {
				return (-1);
			}
			break;

		case 'g':
			if (parse_generic_op(optarg) < 0) {
				return (-1);
			}
			break;

		case 'h':
		case '?':
		default:
			fputs(usage, stderr);
			return (-1);
		}
	}

	// Next arg must be fault number.
	if (optind >= argc) {
		fprintf(stderr, "ERROR: No fault number argument\n");
		return (-1);
	}
	if (parse_fault_num(argv[optind]) < 0) {
		return (-1);
	}
	++optind;

	// The next (optional) arg is fault argument.
	while (optind < argc) {
		if (parse_fault_arg(argv[optind])) {
			return (-1);
		}
		optind++;
	}

	return (0);
}

//
// resolve_name()
//
//	Resolve name from the nameserver
//
// Parameters:
//	name	-- name (which is fault number) to be resolved
//
//		   Assumption is that PXFS_SLEEP function was called
//		   prior to this which binds an object (name of the object
//		   bound is the fault number itself)
//
// Returns:
//	0 if successful, -1 otherwise.
//
// Side Effects:
//

static int
resolve_name(uint32_t fault_number)
{

	naming::naming_context_ptr	context = ns::root_nameserver();
	char				str[64];
	Environment			e;
	CORBA::Exception		*ex;
	CORBA::Object_var		tobj;

	// name of the object to be resolved is the fault number itself

	sprintf(str, "pxfs_sleep-%lu", fault_number);

	tobj = context->resolve(str, e);

	if ((ex = e.exception()) == NULL) {
		data_container::data_var dptr =
			data_container::data::_narrow(tobj);

		if (CORBA::is_nil(dptr)) {
			fprintf(stderr, "resolve_name %s failed while doing "
				"narrow\n", str);
			return (-1);
		}

		context->unbind(str, e);

		if ((ex = e.exception()) != NULL) {
			fprintf(stderr, "resolve_name %s failed while doing "
				"unbind\n", str);
			e.clear();
			return (-1);
		}
	} else {
		fprintf(stderr, "resolve_name %s failed \n", str);
		return (-1);
	}
	return (0);
}

#else	// _FAULT_INJECTION

main()
{
	fprintf(stderr, "Fault injection not supported\n");
	return (1);
}

#endif	// _FAULT_INJECTION
