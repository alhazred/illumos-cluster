/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_TRIGGER_COMMON_H
#define	_TRIGGER_COMMON_H

#pragma ident	"@(#)trigger_common.h	1.14	08/05/20 SMI"

#if defined(_FAULT_INJECTION)

//
// Common routines for the trigger_* utilities.
//

#include <orb/invo/common.h>
#include <sys/os.h>
#include <orb/fault/fault_injection.h>
#include <rgm/sczones.h>

// For storing options and arguments.
struct opts_args_t {
	int		*node_ids;		// array of node ID(s)
	int		num_node_ids;		// # of node ID(s) in array
	uint32_t	fault_num;
	void		*fault_argp;
	uint32_t	fault_argsize;

	// For generic fault points.
	int				generic;	// flag for generic fp
	FaultFunctions::generic_op_t	generic_op;	// op for generic fp
};
extern opts_args_t	opts_args;

int	parse_node_ids(char *arg);
int	parse_fault_num(char *arg);
int	parse_fault_arg(char *arg);
int	parse_generic_op(char *arg);

#endif	// _FAULT_INJECTION

#endif	// _TRIGGER_COMMON_H
