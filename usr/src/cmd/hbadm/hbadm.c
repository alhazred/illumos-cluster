/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma	ident	"@(#)hbadm.c 1.2     08/05/20 SMI"

#include <errno.h>
#include <libintl.h>
#include <locale.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <fmm/hblog.h>
#include <fmm/hbif.h>
#include <fmm/hbargs.h>
/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Print program usage
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
static void
hbadm_usage(void)
{
	printf("Usage: hbadm <command>\n"
	    "<command> = start <mode> | stop | reset | "
	    "set_key <pkey> | set_alt_key <pkey> | commit_key\n"
	    "<pkey> = private key value (up to %d bytes)\n"
	    "<mode> = -e | -r | -x\n",
	    HBX_PKEY_LEN);
}

/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Debug event-tracing callback
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
static void
hbadm_callback(hbx_event_t event)
{
	hblog_error(LOG_DEBUG,
	    "Receiving event %s for host \"%x\" in \"0x%x\" id \"%d\"",
	    hbx_events[event.type], event.src, event.in, event.node_id);
}

/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Execute commands
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * sends an indication to upper application
 *
 * Param IN:
 *   o cmd: Command to execute
 *
 * Returns:
 *   o HB_OK:      Success
 *   o HB_ENOTSUP: Failure
 *   o HB_ENINVAL: Failure
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
static hb_error_t
hbadm_exec_cmd(char *cmd, char *cmd_param)
{
	hb_error_t status = HB_OK;
	hbx_params_t hb_params;
	enum hbx_mode_t mode;

	/*
	 * Get the hbdrv parameters
	 */
	status = hbargs_get_heartbeat_params(&hb_params);
	if (status != HB_OK) {
		hblog_error(LOG_ERR,
		    "hbargs_get_heartbeat_params error %d",
		    status);
		goto terminate;
	}

	/*
	 * Initialize the hb module interface
	 */
#ifdef Solaris
	if (hb_params.debug_flag) {
		status = hbif_init(hbadm_callback);
	} else {
		status = hbif_init((hbif_callback*)NULL);
	}
#endif
#ifdef linux
	status = hbif_init((hbif_callback*)NULL);
#endif
	if (status != HB_OK) {
		hblog_error(LOG_ERR,
		    "Cannot initialize hbif %d",
		    status);
		goto terminate;
	}


	/*
	 * Execute the requested command
	 */
	if (strcmp(cmd, "stop") == 0) {
		/*
		 * Set the hbdrv parameters
		 */
		status = hbif_set_params(&hb_params);
		if (status != HB_OK) {
			hblog_error(LOG_ERR,
			    "hbif_set_params error %d",
			    status);
			goto terminate;
		}
		/* Stop the heartbeat message emission */
		status = hbif_stop();
		if (status != HB_OK) {
			hblog_error(LOG_ERR,
			    "hbif_stop error %d",
			    status);
		}
	} else if (strcmp(cmd, "start") == 0) {
		/*
		 * Set the requested hbdrv mode mode
		 */
		if ((strcmp(cmd_param, "-e") == 0)) {
			mode = HBX_EMT_MODE_ON;
#ifdef Solaris
		} else if ((strcmp(cmd_param, "-r") == 0)) {
			mode = HBX_RCV_MODE_ON;
		} else if ((strcmp(cmd_param, "-x") == 0)) {
			mode = HBX_EMT_MODE_ON | HBX_RCV_MODE_ON;
#endif
		} else {
			hblog_error(LOG_ERR,
			    "Wrong start mode \"%s\"",
			    cmd_param);
			status = HB_EINVAL;
			goto terminate;
		}
		/*
		 * Set the hbdrv parameters
		 */
		status = hbif_set_params(&hb_params);
		if (status != HB_OK) {
			hblog_error(LOG_ERR,
			    "hbif_set_params error %d",
			    status);
			goto terminate;
		}
		/* Start the heartbeat driver */
		status = hbif_start(mode);
		if (status != HB_OK) {
			hblog_error(LOG_ERR,
			    "hbif_start error %d",
			    status);
			goto terminate;
		}
	} else if (strcmp(cmd, "reset") == 0) {
		/*
		 * Set the hbdrv parameters
		 */
		status = hbif_set_params(&hb_params);
		if (status != HB_OK) {
			hblog_error(LOG_ERR,
			    "hbif_set_params error %d",
			    status);
			goto terminate;
		}
		/* Reset the heartbeat driver */
		status = hbif_reset();
		if (status != HB_OK) {
			hblog_error(LOG_ERR,
			    "hbif_reset error %d",
			    status);
			goto terminate;
		}
	} else if (strcmp(cmd, "set_alt_key") == 0) {
		/* Provide an alternate private key */
		status = hbif_set_alt_pkey((hbx_pkey_t*)cmd_param);
		if (status != HB_OK) {
			hblog_error(LOG_ERR,
			    "hbif_set_alt_pkey error %d",
			    status);
			goto terminate;
		}
	} else if (strcmp(cmd, "commit_key") == 0) {
		/* Commit the new private key */
		status = hbif_commit_pkey();
		if (status != HB_OK) {
			hblog_error(LOG_ERR,
			    "hbif_commit_pkey error %d",
			    status);
			goto terminate;
		}
	} else if (strcmp(cmd, "set_key") == 0) {
		/* set the private key */
		status = hbif_set_pkey((hbx_pkey_t*)cmd_param);
		if (status != HB_OK) {
			hblog_error(LOG_ERR,
			    "hbif_set_key error %d",
			    status);
			goto terminate;
		}
	} else {
		hblog_error(LOG_ERR,
		    "unknown command %s",
		    cmd);
		status = HB_ENOTSUP;
		goto terminate;
	}

terminate:
	return (status);
}

/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Execute commands
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * sends an indication to upper application
 *
 * Param IN:
 *   o cmd: Command to execute
 *
 * Returns:
 *   o HB_OK:      Success
 *   o HB_ENOTSUP: Failure
 *   o HB_ENINVAL: Failure
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
static hb_error_t
hbadm_init(void)
{
	hb_error_t status = HB_OK;

	/*
	 * Initialize hb module config. parameters
	 */
	if (hbargs_init() != HB_OK) {
		hblog_error(LOG_ERR,
		    "Cannot initialize hbargs %d",
		    status);
		goto terminate;
	}

terminate:
	return (status);
}

/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Main program
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
int
main(int argc, char **argv)
{
	int option;
	extern char *optarg;
	hb_error_t status;
	int exit_status = EXIT_SUCCESS;

	/*
	 * Initialize hb interface and parameters
	 */
	status = hbadm_init();
	if (status != HB_OK) {
		exit_status = EXIT_FAILURE;
		goto terminate;
	}

	/*
	 * Process the commands
	 */
	if ((argc == 1) || (argc > 3)) {
		(void) hbadm_usage();
		exit_status = EXIT_FAILURE;
		goto terminate;
	} else {
		if ((strcmp("reset", argv[1]) == 0) ||
		    (strcmp("stop", argv[1]) == 0) ||
		    (strcmp("commit_key", argv[1]) == 0)) {
			if (argc == 2) {
				/* Executing command w/o parameters */
				status = hbadm_exec_cmd(argv[1], NULL);
				if (status != HB_OK) {
					exit_status = EXIT_FAILURE;
					goto terminate;
				}
			} else {
				(void) hbadm_usage();
				exit_status = EXIT_FAILURE;
				goto terminate;
			}
		} else if ((strcmp("set_alt_key", argv[1]) == 0) ||
		    (strcmp("set_key", argv[1]) == 0) ||
		    (strcmp("start", argv[1]) == 0)) {
			if (argc == 3) {
				/* Executing command with parameters */
				status = hbadm_exec_cmd(argv[1], argv[2]);
				if (status != HB_OK) {
					exit_status = EXIT_FAILURE;
					goto terminate;
				}
			} else {
				(void) hbadm_usage();
				exit_status = EXIT_FAILURE;
				goto terminate;
			}
		} else {
			(void) hbadm_usage();
			exit_status = EXIT_FAILURE;
			goto terminate;
		}
	}

terminate:
	pthread_exit(NULL);
	return (exit_status);
}
