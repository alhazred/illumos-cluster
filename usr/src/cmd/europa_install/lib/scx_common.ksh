#! /bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident	"@(#)scx_common.ksh	1.3	08/05/20 SMI"

# Use is subject to license terms.
#

####################################################
#
# gettext() [domain] "msgid"
#
#	domain	- Optional argument specifying domain for internationalization.
#	msgid   - Text Message or Message id to be passed to "gettext"
#
#	This function masks the command "/usr/bin/gettext" to enable
#	proper concatenation of multiline text messages into a single
#	contiguous text string.
#
#	This function returns same results as from /usr/bin/gettext command.
#
####################################################
gettext()
{
	typeset -x domain
	typeset -x msgid

	if [[ $# -eq 2 ]];then
		domain=$1
		shift
	else
		if [[ $# -ne 1 ]];then
			/usr/bin/gettext $*
			return $?
		fi
	fi

	msgid=$1

	/usr/bin/gettext ${domain} "$(/bin/echo "${msgid}" | sed "s/		*/ /g" | tr -d "\n")"

	return $?
}

####################################################
#
# sc_print_title() "text"
#
#	text	- the title
#
#	Print the given "text" as the first title
#	on the page.  This title is both preceeded
#	and followed by a newline.
#
#	This function always returns zero.
#
####################################################
sc_print_title()
{
	set -f
	typeset text="$(echo $*)"
	set +f

	clear
	echo
	printf "  %s\n" "${text}"
	echo

	return 0
}

####################################################
#
# sc_print_prompt() text [space]
#
#	text	- the prompt
#	space	- preserve whitespace
#
#	Print the given "text" as a prompt.
#
#	This function always returns zero.
#
####################################################
sc_print_prompt()
{
	set -f
	typeset text="${1}"
	typeset space="${2}"

	if [[ -z "${space}" ]]; then
		text="$(echo ${text})"
	fi
	set +f

	printf "    %s  " "${text}"

	return 0
}

####################################################
#
# sc_print_line() text
#
#	text	- the line
#
#	Print the given "text" as a line with the same left margin
#	as a prompt.
#
#	This function always returns zero.
#
####################################################
sc_print_line()
{
	typeset text="${1}"

	printf "    %s" "${text}"

	return 0
}

####################################################
#
# sc_print_para() text
#
#	text	- the text of the paragraph
#
#	Print the given "text" as a formatted paragraph to stdout.
#
#	This function always returns zero.
#
####################################################
sc_print_para()
{
	set -f
	typeset text="$(echo $*)"

	echo "${text}" | fold -s -w 70 | sed 's/^/    /'
	echo

	return 0
}



####################################################
#
# sc_prompt_yesno() "prompt" [default]
#
#	prompt	- the prompt string
#	default	- the default value
#
#	Display the yes/no prompt and return the user's answer.
#
#	The user may y, yes, Y, YES, n, no, N, or NO.
#	Function will always print "yes" or "no" on stdout.
#
#	The prompt is printed on file descriptor 4, and the answer
#	is printed to stdout.  File descriptor 4 should be
#	dupped to the original stdout before this function is called.
#
#	Return values:
#		0	- proceed
#		1	- ^D was typed
#
####################################################
sc_prompt_yesno()
{
	typeset prompt="${1}"
	typeset default="${2}"

	typeset answer=

	integer i

	# if there is a terminating ? and ${YES} is yes and ${NO} is no,
	# add (yes/no) and shift the terminating "?"
	if [[ "${YES}" = "yes" ]] &&
	    [[ "${NO}" = "no" ]] &&
	    [[ "${prompt}" = *\? ]]; then
		prompt="${prompt%\?}"
		prompt="${prompt} (yes/no)?"
	fi

	let i=0
	while true
	do
		# If this is not the first time through, beep
		[[ ${i} -gt 0 ]] && echo "\a\c" >&4
		let i=1

		# Prompt and get response
		answer=$(sc_prompt "${prompt}" "${default}" "nonl")

		# Return 1 on EOF
		if [[ $? -eq 1 ]]; then
			echo >&4
			return 1
		fi

		# I18N sensative answer always returns "yes" or "no" string
		case ${answer} in
		${YES} | yes | y | YES | Y)
			answer="yes"
			break
			;;

		${NO} | no | n | NO | N)
			answer="no"
			break
			;;

		*)
			answer=
			;;
		esac
	done
	echo >&4

	echo "${answer}"

	return 0
}

####################################################
#
# sc_prompt_pause()
#
#	Print message asking user to type the ENTER key,
#	then wait for a response from the keyboard.
#
#	Return values:
#		0	- proceed
#		1	- ^D was typed
#
####################################################
sc_prompt_pause()
{
	# Pause until they hit ENTER
	sc_print_prompt "\n$(gettext SUNW_SC_INSTALL 'Press Enter to continue:')"
	read
	if [[ $? -ne 0 ]]; then
       		echo
		return 1
	fi
	echo

	return 0
}


#####################################################
#
# is_numeric() value
#
#	Return 0 if the given value is numeric.
#
#	Return values:
#		0	- the "value" is numeric
#		1	- the "value" is not numeric
#	
#####################################################
is_numeric()
{
	typeset value=${1}

	# If "value" is not given, it is not numeric
	if [[ -z "${value}" ]]; then
		return 1
	fi

	# If not numeric, return 1
	if [[ $(expr "${value}" : '[0-9]*') -ne ${#value} ]]; then
		return 1
	fi

	# Numeric
	return 0
}

#####################################################
#
# verify_isroot()
#
#	Print an error message and return non-zero
#	if the user is not root.
#
#####################################################
verify_isroot()
{

	typeset -r uid=`id | awk '{ print $1 }' | \
	    awk -F"(" '{ print $1 }' | awk -F"=" '{ print $2 }'`

	# make sure uid was set
	if [[ -z "${uid}" ]]; then
		printf "$(gettext SUNW_SC_INSTALL '%s:  Cannot get uid')\n" ${PROG} >&2
		return 1
	fi

	# check for root
	if [[ ${uid} -ne 0 ]]; then
		printf "$(gettext SUNW_SC_INSTALL '%s:  Must be root')\n" "${PROG}" >&2
		return 1
	fi

	return 0
}

#####################################################
#
# check_os()
#
#	Check is this node's OS is supported.
#
#####################################################
check_os()
{
	case "${SCX_OS}" in
	"Linux")
		;;
	"SunOS")
		;;
	*)
		printf "%s: Operating System %s not supported\n" \
			${PROG} ${SCX_OS} | logerr
		return 1
		;;
	esac

	return 0
}

#####################################################
#
# check_hostname()
#
#       Check that hostname is properly configured
#	Linux only
#
#####################################################
check_hostname()
{
	if [ "${SCX_OS}" != "Linux" ]
	then
		return 0
	fi

	ipaddr=`hostname -i`
	network=`echo ${ipaddr} | \
			awk -F"." '{ printf("%d.%d.%d.%d", $1, $2, $3, 0) }'`
	if [ "${network}" = "127.0.0.0" ] ; then
		printf "\nERROR: `hostname` is configured on loopback interface\n" | logerr
		printf "\nIt seems RedHat is newly installed on this machines.\n" | logerr
		printf "Please check /etc/hosts file.\n" | logerr
		return 1
	fi

	return 0
}

#####################################################
#
# openfile() "filename"
#
#	Create the given "filename", if it does
#	not exist.  Any needed directories are created.
#
#	The filename must begin with slash.
#
#	Return values:
#		0	- success
#		1	- error
#
#####################################################
openfile()
{
	typeset filename=${1}

	typeset dir
	typeset dirs

	if [[ $# -ne 1 ]]; then
		printf "%s:  Internal error - bad call to openfile()\n" ${PROG} >&2
		return 1
	fi

	# make sure filename begins with /
	if [[ "${filename}" != /* ]]; then
		printf "%s:  Internal error - bad filename passed to openfile()\n" ${PROG} >&2
		return 1
	fi

	# get the list of dirs we must create
	dir=${filename}
	dirs=
	while [[ "${dir}" != "/" ]]
	do
		dir="$(dirname ${dir})"
		if [[ -d "${dir}" ]]; then
			break
		else
			dirs="${dir} ${dirs}"
		fi
	done

	# create each directory with group sys
	for dir in ${dirs}
	do
		mkdir -m 0755 ${dir} || return 1
		chgrp sys ${dir} || return 1
	done

	# create the file
	touch ${filename} || return 1

	return 0
}

#####################################################
#
# setlock()
#
#	Check for the "lockfile".  If it already
#	exists, print an error, and return with non-zero.
#	Otherwise, create a lockfile with our pid inside.
#
#####################################################
setlock()
{
	# If we already set our lock, return
	if [[ ${SCXLOCK_ISSET} -eq 1 ]]; then
		return 0
	fi

	# Check for lockfile
	if [[ -f ${lockfile} ]]; then
		printf "%s:  Another instance of this program may already be running\n" ${PROG} >&2
		printf "%s:  If not, remove %s and try again\n" ${PROG} ${lockfile} >&2
		return 1
	fi

	# Create lockfile
	echo $$ >${lockfile} || return 1

	# Set the lock flag
	SCX_LOCK_ISSET=1

	return 0
}

#####################################################
#
# create_tmpdir()
#
#	Create the temp directory, if it does
#	not already exist.   The caller must be root.
#
#####################################################
create_tmpdir()
{
	openfile ${SCX_TMPDIR}/${PROG}.tmp.${pid} || return 1
	rm -f ${SCX_TMPDIR}/${PROG}.tmp.${pid}

	return 0
}

#####################################################
#
# openlog()
#
#	Create the install log file.  If it does not
#	exist, "logmsg" and "logerr" will not create it.
#
#####################################################
openlog()
{
	openfile ${install_log}

	return $?
}

#####################################################
#
# logmsg()
#
#	Print stdin to stdout and to the install log file.
#	If the install log has not been created, just
#	print to stdout.
#
#####################################################
logmsg()
{
	if [[ ! -f "${install_log}" ]]; then
		cat
	else
		tee -a ${install_log}
	fi
}

#####################################################
#
# logerr()
#
#	Print stdin to stderr and to the install log file.
#	If the install log has not been created, just
#	print to stderr.
#
#####################################################
logerr()
{
	if [[ ! -f "${install_log}" ]]; then
		cat >&2
	else
		tee -a ${install_log} >&2
	fi
}

#####################################################
#
# print_errorexit_msg [beep]
#
#	beep				beep
#
#	Print the error exit message
#
#	This function always returns 0.
#
#####################################################
print_errorexit_msg()
{

	printf "\n" >&2
	printf "%s:  %s did NOT complete successfully!\n\n" ${PROG} ${PROG} >&2
	if [[ -s "${install_log}" ]]; then
		printf "\n" >>${install_log}
		printf "%s:  %s did NOT complete successfully!\n\n" ${PROG} ${PROG} >>${install_log}
	fi

	return 0
}

#####################################################
#
# admin()
#
#	Creates "adminfile" for pkgadd and pkgrm.
#
#	Returns non-zero on error.
#
#####################################################
admin()
{
	# Create file
	cat >${adminfile} <<END
basedir=default
mail=
runlevel=quit
conflict=nocheck
setuid=nocheck
action=nocheck
partial=quit
instance=unique
idepend=quit
rdepend=nocheck
space=quit
END
	return $?
}

#####################################################
#
# reboot_node()
#
#	Reboot the europa node.
#
#####################################################
reboot_node()
{
	printf "\n" | logmsg
	printf "Rebooting ...\n\n" | logmsg

	case "${SCX_OS}" in
	"Linux")
		/sbin/reboot
		;;
	"SunOS")
		/usr/sbin/reboot
		;;
	esac
}

#####################################################
#
# cleanup() [exitstatus]
#
#	existatus		status to exit with
#
#	Cleanup and exit.
#	Remove lock file.
#	Print a message when exit status is not zero.
#
#	If "exitstatus" is not given, zero is assumed.
#
#	If "exitstatus" is -1, cleanup, but do not exit.
#
#####################################################
cleanup()
{
	integer exitstatus=$1

	# Cleanup lockfile and other files
	if [[ -f ${lockfile} ]] && [[ "$(cat ${lockfile})" = ${pid} ]]; then
		rm -f ${lockfile}
	fi
	rm -f ${adminfile} ${tmpfile} ${tmperrs}

	# Print exit message on error
	if [[ ${exitstatus} -gt 0 ]]; then
		print_errorexit_msg
	fi

	# Logfile
	if [[ -s "${install_log}" ]]; then
		printf "\n"
		printf "$(gettext 'Log file - %s')\n\n" "${install_log}"
	elif [[ -f "${install_log}" ]]; then
		rm -f ${install_log}
	fi


	# Return or exit
	if [[ ${exitstatus} -eq -1 ]]; then
		return 0
	fi

	exit ${exitstatus}
}

