#! /bin/ksh -p
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident	"@(#)egcconfig.ksh	1.8	08/07/17 SMI"
#
# Pre-requisites:
#	- A Sun Cluster is already installed on server nodes.
#
#
#############################################################


#
# Global constants	which may be set from the environment
#
typeset -r PROG=${0##*/}
typeset -r PROG_DIR=`dirname $0`

typeset	   SC_BASEDIR=${SC_BASEDIR:-}
typeset -r SC_DEBUG=${SC_DEBUG:-}

typeset -r CMD_PWD=pwd
typeset -r SCX_CWD=$(${CMD_PWD})

#
# The SCX_INSTALL_DIR is the directory from which this script is run.
# Make sure that it is set with an absolute path;  and, remove
# trailing dots.
#
typeset SCX_INSTALL_DIR=$(dirname $0)
if [[ "${SCX_INSTALL_DIR}" != /* ]]; then
	SCX_INSTALL_DIR=${SCX_CWD}/${SCX_INSTALL_DIR}
fi

while [[ "${SCX_INSTALL_DIR##*/}" = "." ]]
do
	SCX_INSTALL_DIR=$(dirname ${SCX_INSTALL_DIR})
done

typeset    SCX_SCADMINDIR=${SCX_SCADMINDIR:-${SCX_BASEDIR}/usr/cluster/lib/scadmin}
typeset -r SCX_SCADMINLIBDIR=${SCX_SCADMINDIR}/lib


# 
# shell library name.
#
typeset -r SCX_COMMON=scx_common

#####################################################
#
# Variable globals
#
#####################################################
#ypeset SCX_DEFAULTSDIR			# where to find defaults file
typeset SCX_LIBDIR			# where to find ksh includes and more

DEFAULT_NETWORK="192.168.0.0/16"
DEFAULT_LOW="192.168.0.1"
DEFAULT_HIGH="192.168.254.254"

#####################################################
#
# Constant globals
#
#####################################################


# XXX Linux dependent?
# Set the PATH
typeset SC_BINDIR=${SC_BASEDIR}/usr/cluster/bin
typeset SC_BINDIRS=${SC_BINDIR}:${SC_BASEDIR}/usr/cluster/lib/sc
PATH=${SC_BINDIRS}:${PWD}:/bin:/usr/bin:/sbin:/usr/sbin:${PATH}; export PATH


typeset -r SCX_HW_PLATFORM=$(uname -i)
typeset -r SCX_OS=$(uname)
typeset -r SCX_NODE_NAME=$(uname -n)
typeset -r SCX_DONE=done
typeset -r SCX_FAILED=failed

# Global variables
integer SCX_LOCK_ISSET=0		# set to 1 once we have set our lock

# Global constants which may be set from the environment
typeset -r SCX_BASEDIR=${SCX_BASEDIR:-}

# Constants
typeset -r SCX_TMPDIR=${SCX_BASEDIR}/var/cluster/run
typeset -r SCX_LOCKDIR=${SCX_TMPDIR}
typeset -r SCX_LOGDIR=${SCX_BASEDIR}/var/cluster/logs/install

typeset -r SCX_FARM_CACHE=${SCX_BASEDIR}/etc/cluster/cfg
typeset -r SC_CONFIG=${SCX_BASEDIR}/etc/cluster/ccr/global/infrastructure
#
# Commands
#
typeset -r SCXCFG_CMD=/usr/cluster/lib/sc/scxcfg
typeset -r SCX_CMD=/usr/cluster/lib/sc/scx
typeset -r SCCONF_CMD=/usr/cluster/bin/scconf
typeset -r SCNAS=/usr/cluster/bin/scnas
typeset -r SCNASDIR=/usr/cluster/bin/scnasdir
typeset -r SCHA_RGG=/usr/cluster/bin/scha_resourcegroup_get
typeset -r SCHA_RSG=/usr/cluster/bin/scha_resource_get

# Temp files and other stuff
typeset -r pid=$$
typeset lockfile=${SCX_LOCKDIR}/${PROG}.lock
typeset install_log=${SCX_LOGDIR}/${PROG}.log.${pid}
typeset -r tmperrs=${SCX_TMPDIR}/${PROG}.cmderr.${pid}
typeset tmplist=${SCX_TMPDIR}/${PROG}.list.${pid}

#
#  Global set by option parsing.
#
typeset SCX_NODE_TYPE
typeset SCX_GROUP="no"
typeset KERNEL

#####################################################
#
# loadlib()
#
#####################################################
#
loadlib()
{
	typeset libname=$1

	# Load the library

	. ${libname}
	if [[ $? != 0 ]]; then
		printf "$(gettext '%s:  Unable to load \"%s\"')\n" "${PROG}" "${libname}" >&2
		return 1
	fi
	return 0
}

#####################################################
#
# print_usage()
#
#	Print usage message to stderr
#
#####################################################
print_usage()
{
	echo "Usage:  ${PROG} -s -k authentication_key -A adapter_options" >&2
	echo "\t${PROG} -f -k authentication_key -A adapter_options  -C cluster_name [-G]" >&2
	echo "\t${PROG} -r" >&2
	echo >&2

	return 0
}

#####################################################
#
# config_uninstall_messages()
#
#####################################################
config_uninstall_messages()
{
	# /etc/hosts
	printf "\n" | logmsg

	# /var/cluster
	printf "\n" | logmsg
	printf "%s Node un-installation has finished.\n" $(SCX_NODE_TYPE)
	printf "The %s directory has not been removed.\n" "/var/cluster" \
					| logmsg
	printf "Among other things, this directory contains\n" | logmsg
	printf "uninstall logs and the uninstall archive.\n" |logmsg
	printf "You may remove this directory once you are satisfied\n" \
					| logmsg
	printf "that the logs and archive are no longer needed.\n" | logmsg

	# Done
	printf "\n" | logmsg

	return 0
}


#####################################################
#
# unconfig_linux_public_network()
#
#####################################################
unconfig_linux_public_network()
{
	netscriptdir="/etc/sysconfig/network-scripts"

	netconffile="/etc/modprobe.conf"


	adap_name=`/sbin/ip -o addr | grep SLAVE | cut -d: -f 2 | awk '{print $1}'`
	if [ -z "${adap_name}" ]; then
		return 0
	fi
	network=`/sbin/ip -o route | grep bond0 | head -1 | \
		awk '{ print $1}' | cut -d/ -f 1`

	printf "$(gettext 'Unconfiguring  bonding interface on NIC %s')\n" \
				${adap_name} | logmsg

	rm -f ${netscriptdir}/ifcfg-bond0

	rm -f ${netscriptdir}/ifcfg-${adap_name}

	if [ -f ${netscriptdir}/save.ifcfg-${adap_name} ] ; then
		mv ${netscriptdir}/save.ifcfg-${adap_name} \
			${netscriptdir}/ifcfg-${adap_name}
	fi

	let count=0
	count=$(grep -c bonding ${netconffile} 2>/dev/null)
	if [ $count != 0 ]
	then
		ed -s ${netconffile} <<EOF /dev/null
/bonding/d
/miimon\=100/d
w
q
EOF
	fi

	cat /etc/netmasks | sed -e "/${network}/d" > /etc/netmasks.${pid}
	mv /etc/netmasks.${pid} /etc/netmasks

	printf "$(gettext 'Restarting network')\n" | logmsg
	/sbin/ifconfig bond0 down
	/sbin/ifup ${adap_name}

	return 0
}

#####################################################
#
# config_linux_public_network()
#
#
#	Configure the public network.
#
#       Create a singleton bonfing group on the interface
#       configured with the hostname.
#	Needed for Logical hostname resource.
#
#	Return:
#		zero		Success
#		non-zero	Failure
#
#####################################################
config_linux_public_network()
{
	netscriptdir="/etc/sysconfig/network-scripts"
	netconffile="/etc/modprobe.conf"
	typeset STR1="alias bond0 bonding"
	typeset STR2="options bond0 mode=1 miimon=100 max_bonds=2"

	# Get ethernet public interface
	ipaddr=`hostname -i`
	adap_name=`/sbin/ip -o addr | grep ${ipaddr} | grep -vi bond | awk '{ print $2}'`

	printf "$(gettext 'Configuring bonding interface on NIC %s')\n" \
				${adap_name} | logmsg

	# Modify /etc/modprobe.conf
	grep "${STR1}" ${netconffile} > /dev/null 2>&1
	if [ $? -ne 0 ] ; then
		echo "${STR1}" >> ${netconffile}
	fi

	grep "${STR2}" ${netconffile} > /dev/null 2>&1
	if [ $? -ne 0 ] ; then
		echo "${STR2}" >> ${netconffile}
	fi

	#
	# Modify/add interface files under
	# /etc/sysconfig/network-scripts
	#
	if [ -f ${netscriptdir}/ifcfg-bond0 ] ; then
		return 0
	fi

	ipaddr=`hostname -i`
	network=`/sbin/ip -o route | grep ${adap_name} | head -1 | \
		awk '{ print $1}' | cut -d/ -f 1`
	netmask=`/bin/netstat -rn | grep ${network} | awk '{print $3}'`
	gateway=`/sbin/ip -o route | grep ${adap_name} | grep default | \
		awk '{print $3}'`
	broadcast=`/sbin/ip -o addr | grep ${ipaddr} | awk '{print $6}'`

	echo "DEVICE=bond0" > ${netscriptdir}/ifcfg-bond0
	echo "IPADDR=${ipaddr}" >> ${netscriptdir}/ifcfg-bond0
	echo "NETMASK=${netmask}" >> ${netscriptdir}/ifcfg-bond0
	echo "NETWORK=${network}" >> ${netscriptdir}/ifcfg-bond0
	echo "BROADCAST=${broadcast}" >> ${netscriptdir}/ifcfg-bond0
	echo "GATEWAY=${gateway}" >> ${netscriptdir}/ifcfg-bond0
	echo "ONBOOT=yes" >> ${netscriptdir}/ifcfg-bond0
	echo "BOOTPROTO=none" >> ${netscriptdir}/ifcfg-bond0
	echo "USERCTL=no" >> ${netscriptdir}/ifcfg-bond0

	# First save the original file
	if [ -f ${netscriptdir}/ifcfg-${adap_name} ] ; then
	    mv ${netscriptdir}/ifcfg-${adap_name} \
		${netscriptdir}/save.ifcfg-${adap_name}
	fi
	echo "DEVICE=${adap_name}" > ${netscriptdir}/ifcfg-${adap_name}
	echo "USERCTL=no" >> ${netscriptdir}/ifcfg-${adap_name}
	echo "ONBOOT=yes" >> ${netscriptdir}/ifcfg-${adap_name}
	echo "MASTER=bond0" >> ${netscriptdir}/ifcfg-${adap_name}
	echo "SLAVE=yes" >> ${netscriptdir}/ifcfg-${adap_name}
	echo "BOOTPROTO=none" >> ${netscriptdir}/ifcfg-${adap_name}

	# configure /etc/netmasks (needed by LogicalHostname)
	let count=0
	if [ -f "/etc/netmasks" ] ; then
		count=$(grep -c ${network} /etc/netmasks 2>/dev/null)
	fi
	if [ $count = 0 ]
	then
		echo "${network}\t${netmask}" >> /etc/netmasks
	fi

	printf "$(gettext 'Restarting network')\n" | logmsg
	/etc/rc.d/init.d/network restart

	return 0
}

#####################################################
#
# config_linux_trash()
#
#
#	Configure the linux node.
#
#	Return:
#		zero		Success
#		non-zero	Failure
#
#####################################################
config_linux_trash()
{
	# Modify /etc/hosts - Remove node name in the loopback line
	printf "\n*** Updating /etc/hosts ***\n\n"
	host=`hostname`
	ipaddr=`hostname -i`
	grep ${host} /etc/hosts > /dev/null 2>&1
	if [ $? -ne 0 ] ; then
	    ed -s /etc/hosts << EOF
/localhost/s/${host}//
.a
${ipaddr}	${host}
.
w
q
EOF
	fi
}


#####################################################
#
# unconfig_solaris_public_network()
#
#
#	Unconfigure the public network.
#
#	Return:
#		zero		Success
#		non-zero	Failure
#
#####################################################
unconfig_solaris_public_network()
{
	adap_name=`/usr/bin/netstat -i | grep \`hostname\` | awk '{print $1}'`

	printf "$(gettext 'Unconfiguring IPMP group NIC %s')\n" \
				${adap_name} | logmsg

	netfile="/etc/hostname.${adap_name}"
	/usr/bin/rm -rf ${netfile}

	echo "`hostname`" > ${netfile}

	/sbin/ifconfig ${adap_name} group ""

	return 0
}

#####################################################
#
# unconfig_server_private_network()
#
#####################################################
unconfig_server_private_network()
{
	adap_name=${1}
	netfile="/etc/hostname.${adap_name}"
	/usr/bin/rm -rf ${netfile}

	# /etc/netmasks XXX?
	return 0
}

#####################################################
#
# config_solaris_public_network()
#
#
#	Configure the public network.
#
#       Create a singleton IPMP group on the interface
#       configured with the hostname.
#	Needed for Logical hostname resource.
#
#	Return:
#		zero		Success
#		non-zero	Failure
#
#####################################################
config_solaris_public_network()
{
	adap_name=`/usr/bin/netstat -i | grep \`hostname\` | awk '{print $1}'`

	printf "$(gettext 'Configuring IPMP group NIC %s')\n" \
				${adap_name} | logmsg

	netfile="/etc/hostname.${adap_name}"
	/usr/bin/rm -rf ${netfile}

	echo "`hostname` netmask + broadcast + group sc_ipmp0 up" > ${netfile}

	/sbin/ifconfig ${adap_name} group sc_ipmp0

	return 0
}
#####################################################
#
# is_sc_shared_adapter()
#
#####################################################
is_sc_shared_adapter()
{
	node_id=$1
	adap_name=$2
	grep "cluster\.nodes\.${node_id}\.adapters\.[0-9]*\.name" ${SC_CONFIG} |
	egrep "${adap_name}$"  > /dev/null 2>&1
	if [ $? -eq 0 ] ; then
	    printf "WARNING: adapter ${adap_name} shared with SC\n" | logmsg
	    return 0
	fi
	return 1
}
#####################################################
#
# config_server_private_network()
#
#####################################################
config_server_private_network()
{
    printf "$(gettext 'Configuring Server private network')\n" | logmsg
    adap_name2=`echo ${1} | awk -F',' '{print $2}'`
    if [ -z "${adap_name2}" ]; then
	type="simple"
    else
	type="redundant"
    fi

    net=${DEFAULT_NETWORK}
    nid=${SCX_NODE_ID}

    # Set interface type
    ${SCXCFG_CMD} -s \
	"farm.servers.${nid}.network.interface.1.type=${type}"

    # Set main address and main adap name
    addr=`echo ${net} | \
	awk -F'.'  ' { printf "%s.%s.%s", $1, $2, $3 }'`.${nid}

    if [ "${type}" = "simple" ] ; then
	adap_name=${1}
	is_sc_shared_adapter ${nid} ${adap_name}
	if [ "$?" -eq 0 ]
	then
	    adap_name=${adap_name}:1
	fi
    elif [ "${type}" = "redundant" ] ; then
	adap_name="scxredund0"
    fi

    ${SCXCFG_CMD} -s \
	"farm.servers.${nid}.network.interface.1.address=${addr}"
    ${SCXCFG_CMD} -s \
	"farm.servers.${nid}.network.interface.1.name=${adap_name}"

    # If ipmp, set test addresses
    if [ "${type}" = "redundant" ] ; then
	adap1_name=`echo ${1} | awk -F, '{print $1}'`
	is_sc_shared_adapter ${nid} ${adap1_name}
	if [ "$?" -eq 0 ] ; then
	    adap1_name=${adap1_name}:1
	fi

	adap2_name=`echo ${1} | awk -F, '{print $2}'`
	is_sc_shared_adapter ${nid} ${adap2_name}
	if [ "$?" -eq 0 ] ; then
	    adap2_name=${adap2_name}:1
	fi

	addr1=`echo ${net} | awk -F'.' \
	    ' { printf "%s.%s.%d", $1, $2, $3 + 128 }'`.${nid}
	addr2=`echo ${net} | awk -F'.' \
	    ' { printf "%s.%s.%s", $1, $2, $3 + 192 }'`.${nid}

	${SCXCFG_CMD} -s "farm.servers.${nid}.network.interface.1.adapters.1.name=${adap1_name}"
	${SCXCFG_CMD} -s "farm.servers.${nid}.network.interface.1.adapters.1.address=${addr1}"

	${SCXCFG_CMD} -s "farm.servers.${nid}.network.interface.1.adapters.2.name=${adap2_name}"
	${SCXCFG_CMD} -s "farm.servers.${nid}.network.interface.1.adapters.2.address=${addr2}"
    fi
    return 0
}


#####################################################
#
# get_local_config()
#
#####################################################
get_local_config()
{
	nid=`${SCXCFG_CMD} -n`
	if [[ -z "${nid}" ]]
	then
	    	printf "%s: Failed to determine node_id\n" ${PROG}  | logerr
		return 1
	else
	    SCX_NODE_ID=${nid}
	fi
	return 0
}


####################################################
#
# get_config_from_server()
#
#####################################################
get_config_from_server()
{
	printf "$(gettext 'Getting config from Server(s)') \n" | logmsg
	#
    	cmdstring="${SCX_CMD} -R"
	printf "%s\n" "${cmdstring}" >> ${install_log}
	${cmdstring} > ${tmperrs} 2>&1

	if [[ $? -ne 0 ]]; then
		printf "$(gettext '%s: Failed to contact Server for coonfig update')\n" "${PROG}"  | logerr
	    return 1
	fi
	# XXX , need to differentiate/ bad cluster name, no server, bad key etc..
	printf "$(gettext 'Configuration retrieved from Server') \n" | logmsg
	return 0
}

####################################################
#
# setup_temp_interconnect
#
#####################################################
setup_temp_interconnect()
{
	adapt=`echo $1 | cut -d, -f1`
	rand=$RANDOM
	(( high= $rand / 256 ))
	(( low = $rand % 256 ))
	addr=192.168.$high.$low
	os=$(uname)
	printf "$(gettext 'Setting temporary interface') $adapt: $addr'\n" | logmsg
	if [ $os = "SunOS" ] ; then
		/sbin/ifconfig $adapt plumb > /dev/null 2>&1
		/sbin/ifconfig $adapt addif $addr/16 broadcast + up
	else
	    /sbin/ifconfig $adapt $addr netmask 255.255.0.0 \
	    broadcast 192.168.255.255 up
	fi
	SCX_TEMP_ADDR=$addr
	return $?
}
remove_temp_interconnect()
{
	adapt=`echo $1 | cut -d, -f1`
	os=$(uname)
	if [ $os = "SunOS" ] ; then
		/sbin/ifconfig $adapt removeif $SCX_TEMP_ADDR
	else
		/sbin/ifconfig $adapt down
	fi
}
####################################################
#
# get_install_parms_server()
#
#####################################################
get_install_parms_from_server()
{
	printf "$(gettext 'Sending Install Request on the network(s)') \n" | logmsg
	# "install" the farm node
    	cmdstring="${SCX_CMD} -i -A ${SCX_ADAP_NAME} -C ${SCX_CLUSTER_NAME} -k ${SCX_AUTH_KEY}"
	printf "%s\n" "${cmdstring}" >> ${install_log}
	${cmdstring} > ${tmperrs} 2>&1

	if [[ $? -ne 0 ]]; then
		printf "$(gettext '%s: Failed to contact Server')\n" "${PROG}"  | logerr
	    return 1
	fi
	# XXX , need to differentiate/ bad cluster name, no server, bad key etc..
	printf "$(gettext 'Install command acknowledged by Server') \n" | logmsg

	return 0
}

#####################################################
#
# config_first_server_node
#
#####################################################
config_first_server_node()
{
    printf "$(gettext 'Configuring first Server node')\n" | logmsg

    NETWORK=${DEFAULT_NETWORK}
    LOW=${DEFAULT_LOW}
    HIGH=${DEFAULT_HIGH}

    CLNAME=`grep cluster.name ${SC_CONFIG} | awk ' { print $2;} '`
    CLID=`grep cluster.properties.cluster_id ${SC_CONFIG} | 
	awk ' { print $2;} '`
    

    ${SCXCFG_CMD} -s "farm.name=${CLNAME}" || return 1
    ${SCXCFG_CMD} -s "farm.properties.cluster_id=${CLID}" || return 1
    ${SCXCFG_CMD} -s "farm.properties.authentication_key=${SCX_AUTH_KEY}" || return 1

    # heartbeat parameters
    ${SCXCFG_CMD} -s "farm.properties.heartbeat.quantum=5000" || return 1
    ${SCXCFG_CMD} -s "farm.properties.heartbeat.frequency=5" || return 1
    ${SCXCFG_CMD} -s "farm.properties.heartbeat.version=1.0" || return 1
    ${SCXCFG_CMD} -s "farm.properties.heartbeat.udp_dest_port=1515" || return 1
    ${SCXCFG_CMD} -s "farm.properties.heartbeat.ttl=1" || return 1
    ${SCXCFG_CMD} -s "farm.properties.heartbeat.syslog_period=3600" || return 1
    ${SCXCFG_CMD} -s "farm.properties.heartbeat.mca=234.0.123.0" || return 1

    # 1st europa interconnect
    ${SCXCFG_CMD} -s "farm.network.1.address=${NETWORK}" || return 1
    ${SCXCFG_CMD} -s "farm.network.1.address_low=${LOW}" || return 1
    ${SCXCFG_CMD} -s "farm.network.1.address_high=${HIGH}" || return 1

    return 0
}

#####################################################
#
# unconfig_last_server_node
#
#####################################################
unconfig_last_server_node()
{

    # delete Farm config
    ${SCX_CMD} -D || return 1
}

#####################################################
#
# config_server_node_common()
#
#####################################################
config_server_node_common()
{
	adap_name=${1}
	# config network interface(s)
	config_server_private_network ${adap_name} || return 1
    
	${SCXCFG_CMD} -s "farm.servers.${SCX_NODE_ID}.state=enabled" || return 1
	# Tell in the config that this server node is ok (installed/configured)
	${SCXCFG_CMD} -s "farm.servers.${SCX_NODE_ID}.server=ok" || return 1
	return 0
}


#####################################################
#
# test_first_europa_config()
#
#####################################################
test_first_europa_config()
{
	${SCXCFG_CMD} -g "farm.name" > /dev/null 2>&1 || return 0
	return 1
}

#####################################################
#
# test_last_server_config()
#
#####################################################
test_last_server_config()
{
	SCC="/usr/cluster/bin/scconf"
	scIds=`${SCC} -p | grep "Node ID:" | awk  '{print $3}'`
	serverIds=$(${SCXCFG_CMD} -l "farm.servers")
	if [ "$scIds" = "$serverIds" ] ; then
	    # All SC servers are configured with farm support
	    return 0 
	fi
	return 1
}

#####################################################
#
# config_last_server_node()
#
#####################################################
config_last_server_node()
{
	printf "Last Server node: setting Farm support toggle\n" | logmsg    
	# Set Europa toggle
	${SCXCFG_CMD} -G
	if [ $? = 0 ] ; then
	    printf "WARNING Farm support already enabled\n" | logmsg
	else
	    ${SCXCFG_CMD} -S  || return 1
	fi
}

#####################################################
#
# config_server_node()
#
#####################################################
config_server_node()
{
    # Get local_server_config
    get_local_config || return 1
    #
    # is it 1st europa config ?
    
    # Test if first europa confiug in SC cluster
    test_first_europa_config
    if [  "$?" -eq 0 ] ; then
	config_first_server_node || return 1
    fi

    config_server_node_common ${SCX_ADAP_NAME} || return 1

    test_last_server_config
    if [  "$?" -eq 0 ] ; then
	config_last_server_node || return 1
    fi
    return 0
}


#####################################################
#
# unconfig_server_node_common()
#
#####################################################
unconfig_server_node_common()
{
    echo "XXXX  unconfig_server_node_common()"
    return 0
}

#####################################################
#
# unconfig_server_node()
#
#####################################################
unconfig_server_node()
{
    unconfig_server_node_common || return 1
    # Test if last europa config in SC cluster
#    is_last_europa_config
    if [  "$?" -eq 0 ] ; then
	unconfig_last_server_node || return 1
    fi

    return 0
}
#####################################################
#
# config_farm_node_linux()
#
####################################################
config_farm_node_linux()
{
    if [ "${SCX_GROUP}" = "yes" ] ; then
	config_linux_public_network || return 1
    fi
}

#####################################################
#
# unconfig_farm_node_linux()
#
####################################################
unconfig_farm_node_linux()
{
    if [ "${SCX_GROUP}" = "yes" ] ; then
	unconfig_linux_public_network || return 1
    fi
}

#####################################################
#
# config_farm_node_solaris()
#
####################################################
config_farm_node_solaris()
{
    if [ "${SCX_GROUP}" = "yes" ] ; then
	config_solaris_public_network || return 1
    fi
}

#####################################################
#
# unconfig_farm_node_solaris()
#
####################################################
unconfig_farm_node_solaris()
{
    # Remove SLM generated pools/psets
    /usr/cluster/lib/sc/scslm_cleanup -p
    # Remove SLM generated projects
    /usr/cluster/lib/sc/scslm_cleanup -J
    if [ "${SCX_GROUP}" = "yes" ] ; then
	unconfig_solaris_public_network || return 1
    fi
}

#####################################################
#
# config_farm_node_common()
#
####################################################
config_farm_node_common()
{
	# XXX Temporary tweak; plumb a temporary interconnect
	# to reach the server
	setup_temp_interconnect ${SCX_ADAP_NAME} || return 1
	sleep 1
	# get install parameters
	get_install_parms_from_server || return 1
	# get the config from the Server
	get_config_from_server || return 1
	# XXX temporary
	remove_temp_interconnect  ${SCX_ADAP_NAME} 
	
	return 0
}


#####################################################
#
# config_farm_node_common()
#
####################################################
unconfig_farm_node_common()
{
    # remove the local farm config 
    /bin/rm -rf /etc/cluster || return 1
    return 0
}


#####################################################
#
# config_farm_node()
#
####################################################
config_farm_node()
{
    case ${SCX_OS} in

    "Linux")
	config_farm_node_linux || return 1
	;;
    "SunOS")
	config_farm_node_solaris || return 1
	;;
    esac

    config_farm_node_common || return 1	
}

#####################################################
#
# unconfig_farm_node()
#
####################################################
unconfig_farm_node()
{
    case ${SCX_OS} in

    "Linux")
	unconfig_farm_node_linux || return 1
	;;
    "SunOS")
	unconfig_farm_node_solaris || return 1
	;;
    esac

    unconfig_farm_node_common || return 1	
}

#####################################################
#
# unconfig_node()
#
#####################################################
unconfig_node()
{
	# Must be root
	verify_isroot || return 1

	# Check OS
	check_os || return 1

	# Create the temp directory, in case it is not there
	create_tmpdir || return 1

	# Set lockfile
	setlock ${lockfile} || return 1

	# Open the logfile
	openlog || return 1
    

	if [ -f /etc/cluster/nodeid ]
	then
	    SCX_NODE_TYPE="server"
	else
	    SCX_NODE_TYPE="farm"
	fi

	case "${SCX_NODE_TYPE}" in
	"server")
		unconfig_server_node || return 1
		;;
	"farm")
		unconfig_farm_node || return 1
		;;
	esac

	return 0
}

#####################################################
#
# config_node()
#	Configure the europa node.
#	Return:
#		zero		Success
#		non-zero	Failure
#####################################################
config_node()
{
	# Must be root
	verify_isroot || return 1

	# Check OS
	check_os || return 1

	# Check hostname properly configured
	check_hostname || return 1

	# Create the temp directory, in case it is not there
	create_tmpdir || return 1

	# Set lockfile
	setlock ${lockfile} || return 1

	# Open the logfile
	openlog || return 1

	# Add the command line to the logfile
	echo "\n${command_line}\n" >> ${install_log}

	case ${SCX_NODE_TYPE} in
	"server")
	    config_server_node|| return 1
	    ;;
	"farm")
	    # Get config from server
	    config_farm_node || return 1
	    ;;
	esac

	return 0
}

#####################################################
#
# config_nas_device()
#	Configure a nas fencing device
#	Return:
#		zero		Success
#		non-zero	Failure
#####################################################
config_nas_device()
{
    serv=`echo ${FENCE_DEVICE} | cut -d ":" -f 1`
    path=`echo ${FENCE_DEVICE} | cut -d ":" -f 2`
    if  [ -z "${serv}" ] || [ -z "${path}" ] ; then
	printf "%s: not a valid NFS server:path" "${FENCE_DEVICE}"
	return 1
    fi
    if  ${SCNAS} -p -h ${serv} > /dev/null 2>&1 ; then
	printf "%s is a configured NAS\n" "${serv}"
    else
	printf "%s is NOT a configured NAS\n" "${serv}"
	return 1
    fi
    if  ${SCNASDIR} -p -h ${serv} | grep "directories:" | 
	grep " ${path}" > /dev/null 2>&1 ; then
	printf "%s: is a configured path\n" "${path}"
    else
	printf "%s: is NOT a  configured path\n" "${path}"
	return 1
    fi

#echo "path: " $path 
#echo "serv: " $serv

    ${SCXCFG_CMD} -s farm.properties.fencing_fs.1.type=netapp || return 1
    ${SCXCFG_CMD} -s farm.properties.fencing_fs.1.server=${serv} || return 1
    ${SCXCFG_CMD} -s farm.properties.fencing_fs.1.path=${path} || return 1

    return 0
}

#####################################################
#
# config_hanfs_device()
#	Configure a ha-nfs fencing device
#	Return:
#		zero		Success
#		non-zero	Failure
#####################################################
config_hanfs_device()
{
    resources=`${SCHA_RGG} -O RESOURCE_LIST -G ${FENCE_DEVICE}`
    if [ $? != 0 ] ; then
	printf "%s: is NOT a resource group\n" "${FENCE_DEVICE}"
	return 1
    fi

    if [ -z "${resources}" ] ; then
	printf "No resources: in resource group\n" "${FENCE_DEVICE}"
	return 1
    fi

    typeset -i found_nfs=0
    typeset -i found_logicalhost=0
    
    for rs in ${resources} 
    do
	type=`${SCHA_RSG} -O TYPE -R ${rs}`
	case ${type} in 
	"SUNW.nfs:"*)
	    found_nfs=1
	    nfs_rs=${rs}
	    ;;
	 "SUNW.LogicalHostname:"*)
	    found_logicalhost=1
	    lh_rs=${rs}
	 esac
    done
    if [ ${found_nfs} -eq 0 ] ; then
	printf "No resources: of type SUNW.nfs in %s \n" "${FENCE_DEVICE}"
	return 1
    fi
    if [ ${found_logicalhost} -eq 0 ] ; then
	printf "No resources: of type SUNW.LogicalHostname in %s \n" \
	     "${FENCE_DEVICE}"
	return 1
    fi

    path=`${SCHA_RGG} -O PATHPREFIX -G ${FENCE_DEVICE}`

    if [ -z "${path}" ] ; then
	printf "No Path configued in resource group\n" "${FENCE_DEVICE}"
	return 1
    fi
    temp=`${SCHA_RSG}  -O EXTENSION  -R ${lh_rs} HostnameList`
    serv=`echo ${temp} | cut -d' ' -f 2 ` 

    ${SCXCFG_CMD} -s farm.properties.fencing_fs.1.type=hanfs || return 1
    ${SCXCFG_CMD} -s farm.properties.fencing_fs.1.server=${serv} || return 1
    ${SCXCFG_CMD} -s farm.properties.fencing_fs.1.path=${path} || return 1
    ${SCXCFG_CMD} -s farm.properties.fencing_fs.1.rg=${FENCE_DEVICE}:${nfs_rs}:${path} || return 1

}

#####################################################
#
# config_fence_device()
#	Configure a fencing device
#	Return:
#		zero		Success
#		non-zero	Failure
#####################################################
config_fence_device()
{
    prev=`${SCXCFG_CMD}  -l farm.properties.fencing_fs`
    if [ ! -z "${prev}" ] ; then
	printf "A fencing FS is already configured\n"
	return 1
    fi

    if echo ${FENCE_DEVICE} | grep ':' > /dev/null 2>&1 ; then
	dev_type="NAS"
    else
	dev_type="HANFS"
    fi

    ${SCXCFG_CMD} -sfarm.properties.fmtpnt_prefix="/global/fence" || return 1
    case ${dev_type} in
    "NAS")
	config_nas_device || return 1
    ;;
    "HANFS")
	config_hanfs_device || return 1
    ;;
    esac
    
    return 0
}

#####################################################
#
# Main
#
#####################################################
main()
{
	# command line
	typeset command_line="${PROG} $*"	# entire command line
	typeset scxinstalldir=${SCX_INSTALL_DIR}
	typeset cluster_name
	typeset auth_key

	if [ "${SCX_OS}" != "Linux" ]
	then
		sc_zonescheck
	fi
	if [ $? -eq 1 ]; then
		exit 1
	fi

	# determine location of support files
	if [[ -d ${scxinstalldir}/lib ]]; then
		SCX_LIBDIR=${scxinstalldir}/lib
	elif [[ -d ${SCX_SCADMINLIBDIR} ]]; then
		SCX_LIBDIR=${SCX_SCADMINLIBDIR}
	else
		printf "%s:  $(gettext 'Support files not found')\n" ${PROG} >&2
		return 1
	fi

	#
	# Get command line options.
	# We support only -i and -r for the moment
	#
	if [ $# -eq 0 ]; then
	    print_usage
	    exit 1
	fi

	typeset -i f_opt=0
	typeset -i s_opt=0
	typeset -i r_opt=0
	typeset -i d_opt=0
	typeset -i k_opt=0
	typeset -i C_opt=0
	typeset -i A_opt=0
	typeset -i F_opt=0
	typeset -i G_opt=0

	loadlib   ${SCX_LIBDIR}/${SCX_COMMON}

	# would we need -d ?? install of DS ?
	#
	while getopts sfrGk:A:C:F: c 2>/dev/null
	do
	    case ${c} in
	    s)             # server config
		if [ ${f_opt} -ne 0 ] ; then
		    print_usage
		    exit 1
		fi
		s_opt=`expr ${s_opt} + 1`
		;;
	    f)             # farm config
		if [ ${s_opt} -ne 0 ] ; then
		    print_usage
		    exit 1
		fi
		f_opt=`expr ${f_opt} + 1`
		;;
	    d)            # Package location
		if [ ${d_opt} -ne 0 ] || [ -z ${OPTARG} ] ; then
		    print_usage
		    exit 1
		fi
		d_opt=`expr ${d_opt} + 1`
		package_dir=${OPTARG}
		;;
	    r)             # Un-config  mode?
		if [ ${r_opt} -ne 0 ] ; then
		    print_usage
		    exit 1
		fi
		r_opt=`expr ${r_opt} + 1`
		;;
	    k)
		if [ ${k_opt} -ne 0 ] ; then
		    print_usage
		    exit 1
		fi
		k_opt=`expr ${k_opt} + 1`
		auth_key=${OPTARG}
		;;
	    C)
		if [ ${C_opt} -ne 0 ] ; then
		    print_usage
		    exit 1
		fi
		C_opt=`expr ${C_opt} + 1`
		cluster_name=${OPTARG}
		;;
	    A)
		if [ ${A_opt} -ne 0 ] ; then
		    print_usage
		    exit 1
		fi
		A_opt=`expr ${A_opt} + 1`
		adap_name=${OPTARG}
		;;
	    F)
		if [ ${F_opt} -ne 0 ] ; then
		    print_usage
		    exit 1
		fi
		F_opt=`expr ${F_opt} + 1`
		FENCE_DEVICE=${OPTARG}
		;;
	    G)
		if [ ${G_opt} -ne 0 ] ; then
		    print_usage
		    exit 1
		fi
		G_opt=`expr ${G_opt} + 1`
		SCX_GROUP="yes"
		;;
	    *)
		print_usage
		exit 1
		;;
	     esac
	done

	# Check that we have at least one mode 
	if [ ${s_opt} -eq 0 ] && [ ${f_opt} -eq 0 ] && [ ${r_opt} -eq 0 ] &&
	    [ ${F_opt} -eq 0 ]; then
	    print_usage
	    exit 1
	fi

	# If remove mode, not install
	if [ ${r_opt} -eq 1 ] ; then
	    if [ ${s_opt} -eq 1 ] || [ ${f_opt} -eq 1 ] ; then
		print_usage
		exit 1
	    fi
	fi

	# if config mode, need all parameters
	if [ ${f_opt} -eq 1 ] ; then
	    if [ ${C_opt} -eq 0 ]  || [ ${k_opt} -eq 0 ] || 
		[ ${A_opt} -eq 0 ] || [ ${F_opt} -eq 1 ] ; then
		print_usage
		exit 1
	    fi
	fi

	if [ ${s_opt} -eq 1 ] ; then
	    if [ ${k_opt} -eq 0 ] || [ ${A_opt} -eq 0 ] ; then
		print_usage
		exit 1
	    fi
	    if [ ${G_opt} -eq 1 ] ; then
		print_usage
		exit 1
	    fi
	fi
	
	if [ ${s_opt} -eq 1 ] ; then
		#
		# Install the server node
		#
		SCX_NODE_TYPE="server"
	fi

	if [ ${f_opt} -eq 1 ] ; then
		#
		# Install the farm node
		#
		SCX_NODE_TYPE="farm"
	fi

	SCX_AUTH_KEY=${auth_key}
	SCX_ADAP_NAME=${adap_name}
	SCX_CLUSTER_NAME=${cluster_name}


	if [ ${SCX_OS} = "Linux" ] ; then
	    vers=`uname -r`
	    if  echo ${vers} | egrep "^2.4" > /dev/null 2>&1
	    then
		KERNEL="2.4"
	    elif   echo  ${vers} | egrep "^2.6" > /dev/null 2>&1
	    then
		KERNEL="2.6"
	    else
		printf "%s: Linux version %s not recognized\n" \
			    "${PROG}" "${vers}" | logerr
		return 1
	    fi
	fi

	if [ ${f_opt} -eq 1 ] || [ ${s_opt} -eq 1 ] ; then
	    config_node || return 1
	fi

	if [ ${F_opt} -eq 1 ] ; then
	    config_fence_device || return 1
	fi
	   
	if [ ${r_opt} -eq 1 ] ; then
	    unconfig_node
	fi
}

# Calling main...
main $*
cleanup $?
