#! /bin/ksh -p
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident	"@(#)egcinstallpkg.ksh	1.14	08/05/20 SMI"

#
# Global constants	which may be set from the environment
#
typeset -r PROG=${0##*/}
typeset -r PROG_DIR=`dirname $0`

typeset	   SC_BASEDIR=${SC_BASEDIR:-}
typeset -r SC_DEBUG=${SC_DEBUG:-}

typeset -r CMD_PWD=pwd
typeset -r SCX_CWD=$(${CMD_PWD})

#
# The SCX_INSTALL_DIR is the directory from which this script is run.
# Make sure that it is set with an absolute path;  and, remove
# trailing dots.
#
typeset SCX_INSTALL_DIR=$(dirname $0)
if [[ "${SCX_INSTALL_DIR}" != /* ]]; then
	SCX_INSTALL_DIR=${SCX_CWD}/${SCX_INSTALL_DIR}
fi
while [[ "${SCX_INSTALL_DIR##*/}" = "." ]]
do
	SCX_INSTALL_DIR=$(dirname ${SCX_INSTALL_DIR})
done

typeset    SCX_SCADMINDIR=${SCX_SCADMINDIR:-${SCX_BASEDIR}/usr/cluster/lib/scadmin}
typeset -r SCX_SCADMINLIBDIR=${SCX_SCADMINDIR}/lib

# 
# tibrary name.
#
typeset -r SCX_COMMON=scx_common

#####################################################
#
# Variable globals
#
#####################################################
#ypeset SCX_DEFAULTSDIR			# where to find defaults file
typeset SCX_LIBDIR			# where to find ksh includes and more


#####################################################
#
# Constant globals
#
#####################################################


# XXX Linux dependent?
# Set the PATH
typeset SC_BINDIR=${SC_BASEDIR}/usr/cluster/bin
typeset SC_BINDIRS=${SC_BINDIR}:${SC_BASEDIR}/usr/cluster/lib/sc
PATH=${SC_BINDIRS}:${PWD}:/bin:/usr/bin:/sbin:/usr/sbin:${PATH}; export PATH


typeset -r SCX_SCADMINDIR=${SC_SCADMINDIR:-${SC_BASEDIR}/usr/cluster/lib/scadmin}

typeset -r SCX_OS=$(uname)
typeset -r SCX_OS_VERSION=$(uname -r)
typeset -r SCX_ARCH=$(uname -i)
typeset -r SCX_KERNEL_RELEASE=$(uname -r)
typeset -r SCX_KERN_MODULES="hbtdrv ffmod"
typeset -r SCX_NODE_NAME=$(uname -n)
typeset -r SCX_DONE=done
typeset -r SCX_FAILED=failed

# Package list
typeset -r SOLARIS9_FARM_PKG_LIST="\
    SUNWscdev \
    SUNWscrtlh \
    SUNWsccomu \
    SUNWsccomzu \
    SUNWegcff \
    SUNWegccfg \
    SUNWegchbr \
    SUNWegchbu \
    SUNWegcfr \
    SUNWegcfu \
    SUNWhasc"

typeset -r SOLARIS10_FARM_PKG_LIST="\
    SUNWscdev \
    SUNWscrtlh \
    SUNWsccomu \
    SUNWsccomzu \
    SUNWegcff \
    SUNWegccfg \
    SUNWegchbr \
    SUNWegchbu \
    SUNWegcfr \
    SUNWegcfzr \
    SUNWegcfu \
    SUNWhasc"

typeset -r SOLARIS_SERVER_PKG_LIST="\
    SUNWegccfg \
    SUNWegchbr \
    SUNWegchbu \
    SUNWegcsr \
    SUNWegcsu \
    SUNWhaglb \
    SUNWhasc"

typeset -r LINUX_FARM_PKG_LIST="\
    sun-egc-cfg \
    sun-egc-hbtr \
    sun-egc-hbtu \
    sun-egc-fu \
    sun-cluster-gds \
    sun-cluster-scrtlh \
    sun-cluster-sccomu \
    sun-cluster-scdev \
    sun-cluster-hasc"

# Global variables
integer SCX_LOCK_ISSET=0		# set to 1 once we have set our lock

# Global constants which may be set from the environment
typeset -r SCX_BASEDIR=${SCX_BASEDIR:-}

# Constants
typeset -r SCX_TMPDIR=${SCX_BASEDIR}/var/cluster/run
typeset -r SCX_LOCKDIR=${SCX_TMPDIR}
typeset -r SCX_LOGDIR=${SCX_BASEDIR}/var/cluster/logs/install



# Temp files and other stuff
typeset -r pid=$$
typeset lockfile=${SCX_LOCKDIR}/${PROG}.lock
typeset install_log=${SCX_LOGDIR}/${PROG}.log.${pid}
typeset -r tmperrs=${SCX_TMPDIR}/${PROG}.cmderr.${pid}
typeset tmplist=${SCX_TMPDIR}/${PROG}.list.${pid}
typeset -r adminfile=${SCX_TMPDIR}/${PROG}.admin.${pid}
typeset install_log=${SCX_LOGDIR}/${PROG}.log.${pid}


typeset SCX_NODE_TYPE
typeset SCX_PKG_LIST


#####################################################
#
# loadlib()
#
#####################################################
#
loadlib()
{
	typeset libname=$1
	# Load the library
	. ${libname}
	if [[ $? != 0 ]]; then
		printf "$(gettext '%s:  Unable to load \"%s\"')\n" "${PROG}" "${libname}" >&2
		return 1
	fi
	if [[ $? != 0 ]]; then
	    echo "FAILED"
		return 1
	fi
	return 0
}

#####################################################
#
# print_usage()
#
#	Print usage message to stderr
#
#####################################################
print_usage()
{
	echo "Usage:  ${PROG} -s | -f  -d <package dir>" >&2
	echo "" >&2
	echo "\t${PROG} -r" >&2
	echo >&2

	return 0
}

#####################################################
#
# build_farm_mod_pkg_list()
#
#	Set the farm node module package list.
#
#####################################################
build_farm_mod_pkg_list()
{
	# KERNEL_RELEASE
	k_rel=$(uname -r)
	# RPM version/release used for the cluster kernel modules
	rpm_ver=3.2
	rpm_rel=$(echo ${k_rel} | sed -e "s/-/_/g")
	arch=$(uname -i)

	LINUX_MODULE_PKG_LIST=
	for module in ${SCX_KERN_MODULES}
	do
	    PKG_NAME=sun-egc-${module}-${k_rel}-${rpm_ver}-${rpm_rel}
	    LINUX_MODULE_PKG_LIST=${LINUX_MODULE_PKG_LIST}" ${PKG_NAME}"
	done
}


#####################################################
#
# set_pkg_list_farm()
#
#	Set the package list.
#
#####################################################
set_pkg_list_farm()
{
	case "${SCX_OS}" in
	"Linux")
		SCX_PKG_LIST=${LINUX_FARM_PKG_LIST}
		build_farm_mod_pkg_list
		;;
	"SunOS")
		case "${SCX_OS_VERSION}" in
		"5.9")
			SCX_PKG_LIST=${SOLARIS9_FARM_PKG_LIST}
			;;
		"5.10")
			SCX_PKG_LIST=${SOLARIS10_FARM_PKG_LIST}
			;;
		esac
		;;
	esac
	return 0
}
#####################################################
#
# set_pkg_list_server()
#
#	Set the package list.
#
#####################################################
set_pkg_list_server()
{
	if [ "${SCX_OS}" = "Linux" ]
	then
		return 1
	fi
	SCX_PKG_LIST=${SOLARIS_SERVER_PKG_LIST}
	return 0
}



#####################################################
#
# uninstall_linux_package() <rpm package>
#
#
#	Un-install an rpm package.
#
#	Return:
#		zero		Success
#		non-zero	Failure
#
#####################################################
uninstall_linux_package()
{
    typeset PKG=$1

    /bin/rpm -q ${PKG} > /dev/null 2>&1

    if [ $? -eq 0 ] ; then
	cmdstring="/bin/rpm -e  ${PKG}"
	printf "%s" ${cmdstring} >> ${install_log}
	printf "\n" >> ${install_log}
	${cmdstring} > ${tmperrs} 2>&1

	if [ $? -ne 0 ] ; then
	    printf "\n" >> ${install_log}
	    cat ${tmperrs} >> ${install_log}
	    printf "%s:  Un-installation of \"%s\" failed\n" \
		${PROG} ${PKG} | logerr
	    return 1
	fi
    fi

    return 0
}

#####################################################
#
# remove_linux_packages()
#
#
#	Un-install all europa node linux packages.
#
#	Return:
#		zero		Success
#		non-zero	Failure
#
#####################################################
remove_linux_packages()
{
	for pkg in ${LINUX_FARM_PKG_LIST}
	do
		uninstall_linux_package ${pkg} || return 1
	done

	# Remove platform dependent packages
	build_farm_mod_pkg_list
	for pkg in ${LINUX_MODULE_PKG_LIST}
	do
		uninstall_linux_package ${pkg} || return 1
	done

	return 0
}

#####################################################
#
# install_linux_packages() <package location>
#
#
#	Install the europa node linux packages.
#
#	Already installed packages are removed, then re-installed.
#
#	Return:
#		zero		Success
#		non-zero	Failure
#
#####################################################
install_linux_packages()
{
    rpmbasedir=$1
    a_pkg=

    # Check if path is relative
    echo ${rpmbasedir} | grep "^\/" > /dev/null 2>&1
    if [ $? -ne 0 ] ; then
	echo ${rpmbasedir} | grep "^.." > /dev/null 2>&1
	if [ $? -eq 0 ] ; then
	    rpmbasedir=`pwd`/${rpmbasedir}
	else
	    echo ${rpmbasedir} | grep "^." > /dev/null 2>&1
	    if [ $? -eq 0 ] ; then
		rpmbasedir=`echo ${rpmbasedir} | sed 's/^\.//g'`
		rpmbasedir=`pwd`/${rpmbasedir}
	    fi
	fi
    fi
    
    # Un-install packages, if already installed
    printf "%s: Un-installing :\n" ${PROG} | logmsg

    for PKG_NAME in ${SCX_PKG_LIST}
    do
	uninstall_linux_package ${PKG_NAME} || return 1
    done
    for pkg in ${LINUX_MODULE_PKG_LIST}
    do
	uninstall_linux_package ${pkg} || return 1
    done

    printf "%s: Now installing : \n" ${PROG} | logmsg
    for PKG_NAME in ${SCX_PKG_LIST}
    do
	cat /dev/null > ${tmplist}

	# Make sure the package exists
	ls -1 ${rpmbasedir} | while read b
	do
	    echo ${b} | grep ${PKG_NAME} > /dev/null 2>&1
	    if [ $? -eq 0 ] ; then
		echo ${b} > ${tmplist}
		break
	    fi
	done

	if [ ! -s ${tmplist} ] ; then
	    printf "%s:  Unable to find \"%s\"\n" ${PROG} \
		${PKG_NAME} | logerr
	    return 1
	fi

	pkg=`cat ${tmplist}`
	pkg_file=${rpmbasedir}/${pkg}

	# Install the package
	cmdstring="/bin/rpm -ivh ${pkg_file}"
	printf "%s" "${cmdstring}" >> ${install_log}
	printf "\n" >> ${install_log}
	printf "\t%s ...\n" ${PKG_NAME}
	${cmdstring} > ${tmperrs} 2>&1

	if [ $? -ne 0 ] ; then
	    cat ${tmperrs} >> ${install_log}
	    printf "%s:  Installation of \"%s\" failed\n" \
		${PROG} ${pkg} | logerr
	    return 1
	fi
    done

    #
    # Complete the installation w/ the platform dependent packages
    #
    not_founds=0
    for PKG_NAME in ${LINUX_MODULE_PKG_LIST}
    do
	#
	# It is possible that the rpm does not exist.
	# if the current kernel release is not supported.
	#
	cat /dev/null > ${tmplist}

	# Make sure the package exists
	ls -1 ${rpmbasedir} | while read b
	do
	    echo ${b} | grep ${PKG_NAME} > /dev/null 2>&1
	    if [ $? -eq 0 ] ; then
		echo ${b} > ${tmplist}
		break
	    fi
	done

	if [ ! -s ${tmplist} ] ; then
	    not_founds=1
	    printf "\tNot Found %s ...\n" ${PKG_NAME} | logmsg
	    continue
	fi

	pkg=`cat ${tmplist}`
	pkg_file=${rpmbasedir}/${pkg}


	# Install the package
	cmdstring="/bin/rpm -ivh ${pkg_file}"
	printf "%s" "${cmdstring}" >> ${install_log}
	printf "\n" >> ${install_log}
	printf "\t%s ...\n" ${PKG_NAME}
	${cmdstring} > ${tmperrs} 2>&1

	if [ $? -ne 0 ] ; then
	    cat ${tmperrs} >> ${install_log}
	    printf "%s:  Installation of \"%s\" failed\n" \
		${PROG} ${pkg} | logerr
	    return 1
	fi
    done

    if [ ${not_founds} != 0 ] ; then
	printf "\n\n"
	printf "********************************************************\n" | logmsg
	printf "Some kernel modules for release ${SCX_KERNEL_RELEASE}\n"  | logmsg
	printf "were not found. Please consider installing source RPM \n" | logmsg
	printf "sun-egc-kernel-modules-src and build them.\n" | logmsg
	printf "********************************************************\n" | logmsg
    fi

    return 0
}


#####################################################
#
# remove_solaris_packages()
#
#
#	Un-install all europa node solaris packages.
#
#	Return:
#		zero		Success
#		non-zero	Failure
#
#####################################################
remove_solaris_packages()
{
	# create adminfile
	admin || return 1

	for pkg in ${SCX_PKG_LIST}
	do
	    if /usr/bin/pkginfo ${pkg} > /dev/null 2>&1 ; then
		cmdstring="/usr/sbin/pkgrm -n -a ${adminfile} ${pkg}"
		printf "%s" "${cmdstring}" >> ${install_log}
		printf "\n" >> ${install_log}
		printf "\t%s ...\n" ${pkg}
		${cmdstring} > ${tmperrs} 2>&1

		if [ $? -ne 0 ] ; then
			cat ${tmperrs} >> ${install_log}
			printf "%s:  Removal of \"%s\" failed\n" \
				${PROG} ${pkg} | logerr
			return 1
		fi
	    fi
	done

	return 0
}

#####################################################
#
# install_solaris_packages() <package location>
#
#
#	Install the europa node solaris packages.
#
#	Already installed packages are removed, then re-installed.
#
#	Return:
#		zero		Success
#		non-zero	Failure
#
#####################################################
install_solaris_packages()
{
	packagedir=$1

	# Check if path is relative
	echo ${packagedir} | grep "^\/" > /dev/null 2>&1
	if [ $? -ne 0 ] ; then
	    echo ${packagedir} | grep "^.." > /dev/null 2>&1
	    if [ $? -eq 0 ] ; then
		packagedir=`pwd`/${packagedir}
	    else
		echo ${packagedir} | grep "^." > /dev/null 2>&1
		if [ $? -eq 0 ] ; then
		    packagedir=`echo ${packagedir} | sed 's/^\.//g'`
		    packagedir=`pwd`/${packagedir}
		fi
	    fi
	fi

	# Remove previous version of europa node packages

	# create admin file
	admin || return 1

	printf "%s: Uninstalling : \n" \
				${PROG} | logmsg

	for pkg in ${SCX_PKG_LIST}
	do

		cmdstring="/usr/sbin/pkgrm -n -a ${adminfile} ${pkg}"
		printf "%s" "${cmdstring}" >> ${install_log}
		printf "\n" >> ${install_log}
		${cmdstring} > ${tmperrs} 2>&1
	done
	printf "%s: Now installing : \n" \
				${PROG} | logmsg
	# Install new version of europa node packages
	for pkg in ${SCX_PKG_LIST}
	do
		cmdstring="/usr/sbin/pkgadd -d ${packagedir} -a ${adminfile} ${pkg}"
		printf "%s" "${cmdstring}" >> ${install_log}
		printf "\n" >> ${install_log}
		printf "\t%s ...\n" ${pkg}
		${cmdstring} > ${tmperrs} 2>&1

		if [ $? -ne 0 ] ; then
			cat ${tmperrs} >> ${install_log}
			printf "%s:  Installation of \"%s\" failed\n" \
				${PROG} ${pkg} | logerr
			return 1
		fi
	done

	cmdstring="/usr/bin/pkginfo ${SCX_PKG_LIST}"
	printf "%s" "${cmdstring}" >> ${install_log}
	printf "\n" >> ${install_log}
	${cmdstring} | logmsg

	return 0
}



#####################################################
#
# install_packages() <package location>
#
#
#	Install the node packages.
#
#	Already installed packages are removed, then re-installed.
#
#	Return:
#		zero		Success
#		non-zero	Failure
#
#####################################################
install_packages()
{
	packagedir=$1

	printf "*** Installing %s Node packages ***\n\n" ${SCX_NODE_TYPE} \
		| logmsg

	case "${SCX_OS}" in
	"Linux")
		install_linux_packages ${packagedir} || return 1
		;;
	"SunOS")
		install_solaris_packages ${packagedir} || return 1
		;;
	esac

	printf "\n%s\n" ${SCX_DONE} | logmsg
	printf "Finished installation of %s Node packages\n\n" ${SCX_NODE_TYPE} \
		| logmsg

	return 0
}

#####################################################
#
# remove_packages()
#
#
#	Un-install all node packages.
#
#	Return:
#		zero		Success
#		non-zero	Failure
#
#####################################################
remove_packages()
{
	printf "*** Removing %s Node packages ***\n\n" ${SCX_NODE_TYPE} \
				| logmsg

	case "${SCX_OS}" in
	"Linux")
		remove_linux_packages || return 1
		;;
	"SunOS")
		remove_solaris_packages || return 1
		;;
	esac

	printf "\n%s\n" ${SCX_DONE} | logmsg
	printf "Finished removal of %s Node packages\n\n" ${SCX_NODE_TYPE} \
		| logmsg

	return 0
}

#####################################################
#
# set_node_type()
#
#	Set SCX_NODE_TYPE to server or farm.
#
#####################################################
set_node_type()
{
	case ${SCX_OS} in 
	SunOS)
	    if /usr/bin/pkginfo SUNWegcsu > /dev/null 2>&1 ; then
		SCX_NODE_TYPE="server"
		return
	    fi
	    if /usr/bin/pkginfo SUNWegcfu > /dev/null 2>&1 ; then
		SCX_NODE_TYPE="farm"
		return
	    fi
	;;
	Linux)
	    if /bin/rpm -q sun-egc-fu > /dev/null 2>&1 ; then
		SCX_NODE_TYPE="farm"
		return
	    fi
	;;
	esac
	# Unknown
	SCX_NODE_TYPE="unknown"
}

#####################################################
#
# uninstall_node()
#
#         This function uninstalls
#         an Europa (server/farm) node (solaris or linux)
#
#####################################################
uninstall_node() 
{
	printf "\n*** Uninstall Node %s ***\n" ${SCX_NODE_NAME} \
		| logmsg

	# Must be root
	verify_isroot || return 1

	# Set node type (server or farm)
	set_node_type

	# Check OS
	check_os || return 1

	# Create the temp directory, in case it is not there
	create_tmpdir || return 1

	# Set lockfile
	setlock ${lockfile} || return 1

	# Open the logfile
	openlog || return 1

	# Add the command line to the logfile
	echo "\n${command_line}\n" >> ${install_log}

	case ${SCX_NODE_TYPE} in 
	    "server")
		set_pkg_list_server || return 1
	    ;;
	    "farm")
		set_pkg_list_farm || return 1
	    ;;
	esac

	# Uninstall Europa Node packages
	remove_packages || return 1

	return 0
}

#####################################################
#
# install_node()
#
#         This function installs
#         an Europa (server/farm) node (solaris or linux)
#
#####################################################
install_node() 
{
	packagedir=$1
	SCX_NODE_TYPE=$2	

	# Must be root
	verify_isroot || return 1

	# Check OS
	check_os || return 1

	# Create the temp directory, in case it is not there
	create_tmpdir || return 1

	# Set lockfile
	setlock ${lockfile} || return 1

	# Open the logfile
	openlog || return 1

	# Add the command line to the logfile
	echo "\n${command_line}\n" >> ${install_log}

	printf "\n*** Installing Node %s ***\n" ${SCX_NODE_NAME} | logmsg

	case ${SCX_NODE_TYPE} in 
	    "server")
		set_pkg_list_server || return 1
	    ;;
	    "farm")
		set_pkg_list_farm || return 1
	    ;;
	esac
	
	install_packages ${packagedir} || return 1

	return 0
}

sc_zonescheck() {
	if [ -f /usr/bin/zonename ]; then
		zn=`/usr/bin/zonename`
	else
		return 0
	fi
	case "$zn" in
	'global')
		return 0
		;;
	esac
	return 1
}

#####################################################
#
# Main
#
#####################################################
main()
{
	# command line
	typeset command_line="${PROG} $*"	# entire command line
	typeset package_dir
	typeset scxinstalldir=${SCX_INSTALL_DIR}

	if [ "${SCX_OS}" != "Linux" ]
	then
		sc_zonescheck
	fi
	if [ $? -eq 1 ]; then
		echo `gettext "You cannot run this \
		    command from a non-global zone."` >&2
		exit 1
	fi

	# determine location of support files
	if [[ -d ${scxinstalldir}/lib ]]; then
		SCX_LIBDIR=${scxinstalldir}/lib
	elif [[ -d ${SCX_SCADMINLIBDIR} ]]; then
		SCX_LIBDIR=${SCX_SCADMINLIBDIR}
	else
		printf "%s:  $(gettext 'Support files not found')\n" ${PROG} >&2
		return 1
	fi

	#
	# Get command line options.
	# We support only -i and -r for the moment
	#
	if [ $# -eq 0 ]; then
	    print_usage
	    exit 1
	fi

	# First, get mode of operation
	typeset -i f_opt=0
	typeset -i s_opt=0
	typeset -i r_opt=0
	typeset -i d_opt=0

	loadlib   ${SCX_LIBDIR}/${SCX_COMMON}

	while getopts sfd:r c 2>/dev/null
	do
	    case ${c} in
	    s)             # server install
		if [ ${f_opt} -ne 0 ] ; then
		    print_usage
		    exit 1
		fi
		s_opt=`expr ${s_opt} + 1`
		;;
	    f)             # farm install
		if [ ${s_opt} -ne 0 ] ; then
		    print_usage
		    exit 1
		fi
		f_opt=`expr ${f_opt} + 1`
		;;
	    d)            # Package location
		if [ ${d_opt} -ne 0 ] || [ -z ${OPTARG} ] ; then
		    print_usage
		    exit 1
		fi
		d_opt=`expr ${d_opt} + 1`
		package_dir=${OPTARG}
		;;
	    r)             # Un-install mode
		if [ ${r_opt} -ne 0 ] ; then
		    print_usage
		    exit 1
		fi
		r_opt=`expr ${r_opt} + 1`
		;;
	    *)
		print_usage
		exit 1
		;;
	     esac
	done

	# Check that we have at least one mode 
	if [ ${s_opt} -eq 0 ] && [ ${f_opt} -eq 0 ]  && [ ${r_opt} -eq 0 ] ; then
	    print_usage
	    exit 1
	fi

	# If remove mode, not install
	if [ ${r_opt} -eq 1 ] ; then
	    if [ ${s_opt} -eq 1 ] || [ ${f_opt} -eq 1 ] ; then
		print_usage
		exit 1
	    fi
	fi

	if [ ${s_opt} -eq 1 ] || [ ${f_opt} -eq 1 ] ; then
	    if [ ${d_opt} -eq 0 ] ; then
		print_usage
		exit 1
	    fi
	fi
	if [ ${s_opt} -eq 1 ] && [ ${SCX_OS} != "SunOS" ]
	then
	    printf "%s:  $(gettext 'Can install server only on Solaris')\n" ${PROG} >&2
	    exit 1
	fi
	#
	# Switch on europa-install mode of operation
	#
	if [ ${s_opt} -eq 1 ] ; then
		#
		# Install the farm node
		#
		install_node ${package_dir} "server" || return 1
	fi

	if [ ${f_opt} -eq 1 ] ; then
		#
		# Install the farm node
		#
		install_node ${package_dir} "farm"  || return 1
	fi

	if [ ${r_opt} -eq 1 ] ; then
		uninstall_node || return 1
	fi
}

# Calling main...
main $*
cleanup $?
