/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scslm_cleanup.c	1.5	08/05/20 SMI"

/*
 * SC_SRM is set for Solaris 9 and Solaris 10, see Makefile.
 * Just in case someone remove the SC_SRM definition
 * from the Makefile.
 */
#ifndef	SC_SRM
int
main(int argc, char **argv)
{
}
#else

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <limits.h>
#include <libgen.h>
#include <libintl.h>
#include <ctype.h>
#include <errno.h>
#include <pool.h>
#include <project.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/sol_version.h>
#include <scha.h>
#include "../fed/fed_slm_defs.h"
#if SOL_VERSION >= __s10
#include <rgm/sczones.h>
#include <zone.h>
#else
#define	GLOBAL_ZONENAME	"global"
#endif


#define	RG_FOUND		1
#define	RG_NOT_FOUND		2
#define	RG_ERROR		3
#define	OPTARGS			"pj:J"

#define	SCHA_READY_DELAY	10
#define	SCHA_READY_RETRY	3


/*
 * Prototypes
 */
static int		search_rg(const char *);
static int		SCSLM_file_parse_proj(char **, char **, char **);
static int		SCSLM_file_open_proj(void);
static int		SCSLM_file_close_proj(void);
static int		SCSLM_exec(char *, int *, char **);
static int		SCSLM_delete_proj(char *, char *);
static int		SCSLM_file_del_proj(const char *, char *, int);
static int		proj_cleanup(const char *);
static int		proj_cleanup_all(void);
static			void usage(char *);
static int		get_scha_rgs(void);
#if SOL_VERSION >= __s10
static int		SCSLM_cleanup(void);
static int		SCSLM_delete_pset(char *);
static int		SCSLM_delete_pool(char *);
#endif


/*
 * Global data
 */

static char *cmdname;

typedef struct tprjlist {
	char		*proj;
	char		*zone;
	struct tprjlist	*next;
} tprjlist;

static	int	slm_proj_file = -1;
static	char	*slm_proj_file_ptr;
static	size_t	slm_proj_file_size;
static tprjlist	*prjhead = NULL;
static scha_str_array_t	*all_rgs = NULL;


/*
 * Functions
 */


/*lint -e801 */

int
main(int argc, char **argv)
{
	char	*cmd;
	int	err;
	int	c;
	char	*zone;

	/* Must be root */
	if (getuid() != 0) {
		(void) fprintf(stderr,
		    gettext("%s: You must be root.\n"), argv[0]);
		exit(2);
	}

#if SOL_VERSION >= __s10
	if (sc_zonescheck() != 0) {
		exit(3);
	}
#endif

	cmd = strdup(argv[0]);
	err = errno; /*lint !e746 */
	if (cmd == NULL) {
		(void) fprintf(stderr, gettext("%s: strdup error <%s>\n"),
		    argv[0], strerror(err));
		exit(2);
	}

	cmdname = basename(cmd);
	err = errno; /*lint !e746 */
	if (cmdname == NULL) {
		(void) fprintf(stderr, gettext("%s: basename error <%s>\n"),
		    argv[0], strerror(err));
		exit(2);
	}

	c = getopt(argc,  (char * const *)argv, OPTARGS);
	if (c != EOF) {
		switch (c) {
#if SOL_VERSION >= __s10
			case 'p':
				return (SCSLM_cleanup());
#else
			case 'p':
				return (0);
#endif
			case 'j':
				zone = optarg;
				return (proj_cleanup(zone));
			case 'J':
				return (proj_cleanup_all());
			default:
				usage(cmdname);
				return (-1);
		}
	}
	usage(cmdname);
	return (-1);
} /*lint !e818 */


static void
usage(char *progname)
{
	(void) fprintf(stderr, gettext("%s usage:\n"
	    "\t-j <zonename> (delete unused SC SLM projects)\n"
	    "\t-J (for SC uninstall)\n"
#if SOL_VERSION >= __s10
	    "\t-p (cleanup SC SLM created pools)\n"
#endif
	    ""), progname);
}


static int
proj_cleanup(const char *zone_to_clean)
{
	int		err;
	int		ret;
	int		retrg;
	char		*rg;
	char		*proj;
	char		*zone;
	tprjlist	*pjl;
	tprjlist	*pjt;
	int		fd;

	if (get_scha_rgs() != 0) {
		return (2);
	}

	/*
	 * Mutual exclusion with the fed daemon.
	 */
	fd = open(SCSLM_PROJ_FILE_LOCK,
	    O_WRONLY | O_CREAT, SCSLM_FILE_LOCK_MODE);
	err = errno; /*lint !e746 */
	if (fd < 0) {
		(void) fprintf(stderr,
		    gettext("%s: open <%s> error <%s>\n"),
		    cmdname, SCSLM_PROJ_FILE_LOCK, strerror(err));
		return (2);
	}
	if (lockf(fd, F_LOCK, 0) < 0) {
		err = errno; /*lint !e746 */
		(void) fprintf(stderr,
		    gettext("%s: lockf <%s> error <%s>\n"),
		    cmdname, SCSLM_PROJ_FILE_LOCK, strerror(err));
		(void) close(fd);
		return (2);
	}

	if (SCSLM_file_open_proj() != SCSLM_SUCCESS) {
		/* Release mutual exclusion with fed */
		(void) close(fd);
		return (1);
	}
	if (slm_proj_file == -1) {
		/* File does not exists */
		/* Release mutual exclusion with fed */
		(void) close(fd);
		return (0);
	}
	do {
		ret = SCSLM_file_parse_proj(&rg, &proj, &zone);
		if (ret == SCSLM_SUCCESS) {
			/*
			 * SCSLM_SUCCESS: rg, proj and zone are heap allocated
			 */
			if (strcmp(zone_to_clean, zone) != 0) {
				free(rg);
				free(zone);
				free(proj);
				continue;
			}
			retrg = search_rg(rg);
			if (retrg == RG_NOT_FOUND) {
				if (SCSLM_delete_proj(zone, proj) ==
				    SCSLM_SUCCESS) {
#if SOL_VERSION >= __s10
					(void) fprintf(stdout, gettext(
					    "%s: project <%s> zone <%s>"
					    " removed\n"),
					    cmdname, proj, zone);
#else
					(void) fprintf(stdout, gettext(
					    "%s: project "
					    "<%s> removed\n"),
					    cmdname, proj);
#endif
				} else {
#if SOL_VERSION >= __s10
					(void) fprintf(stderr, gettext(
					    "%s: project "
					    "<%s> zone <%s>, "
					    "unable to remove\n"),
					    cmdname, proj, zone);
#else
					(void) fprintf(stderr, gettext(
					    "%s: project "
					    "<%s>, unable to remove\n"),
					    cmdname, proj);
#endif
					continue;
				}
				/* Add to list */
				pjl = (tprjlist *)malloc(sizeof (tprjlist));
				err = errno; /*lint !e746 */
				if (pjl == NULL) {
					(void) fprintf(stderr, gettext(
					    "%s: malloc error <%s>\n"),
					    cmdname, strerror(err));
					/* Release mutual exclusion with fed */
					(void) close(fd);
					return (1);
				}
				pjl->proj = strdup(proj);
				pjl->zone = strdup(zone);
				pjl->next = NULL;
				if (prjhead == NULL) {
					prjhead = pjl;
				} else {
					pjt = prjhead;
					while (pjt->next != NULL) {
						pjt = pjt->next;
					}
					pjt->next = pjl;
				}
			}
			if (retrg == RG_ERROR) {
				(void) SCSLM_file_close_proj();
				/* Release mutual exclusion with fed */
				(void) close(fd);
				return (1);
			}
			free(rg);
			free(zone);
			free(proj);
		}
	} while (ret == SCSLM_SUCCESS);
	if (ret != SCSLM_PARSE_END) {
		(void) SCSLM_file_close_proj();
		/* Release mutual exclusion with fed */
		(void) close(fd);
		return (1);
	}
	(void) SCSLM_file_close_proj();
	pjl = prjhead;
	while (pjl != NULL) {
		ret = SCSLM_file_del_proj(pjl->zone, pjl->proj, 1);
		if (ret == SCSLM_SUCCESS) {
#if SOL_VERSION >= __s10
			(void) fprintf(stdout, gettext(
			    "%s: <%s> zone <%s> entry removed in %s\n"),
			    cmdname, pjl->proj, pjl->zone, SCSLM_PROJ_FILE);
#else
			(void) fprintf(stdout, gettext(
			    "%s: <%s> entry removed in %s\n"),
			    cmdname, pjl->proj, SCSLM_PROJ_FILE);
#endif
		}
		pjl = pjl->next;
	}
	/* Release mutual exclusion with fed */
	(void) close(fd);
	return (0);
}


static int
proj_cleanup_all(void)
{
	int		err;
	int		ret;
	char		*rg;
	char		*proj;
	char		*zone;
	tprjlist	*pjl;
	tprjlist	*pjt;
	int		fd;

	/*
	 * Mutual exclusion with the fed daemon.
	 * Lock is release on exit.
	 */
	fd = open(SCSLM_PROJ_FILE_LOCK,
	    O_WRONLY | O_CREAT, SCSLM_FILE_LOCK_MODE);
	err = errno; /*lint !e746 */
	if (fd < 0) {
		(void) fprintf(stderr,
		    gettext("%s: open <%s> error <%s>\n"),
		    cmdname, SCSLM_PROJ_FILE_LOCK, strerror(err));
		return (2);
	}
	if (lockf(fd, F_LOCK, 0) < 0) {
		err = errno; /*lint !e746 */
		(void) fprintf(stderr,
		    gettext("%s: lockf <%s> error <%s>\n"),
		    cmdname, SCSLM_PROJ_FILE_LOCK, strerror(err));
		(void) close(fd);
		return (2);
	}

	if (SCSLM_file_open_proj() != SCSLM_SUCCESS) {
		/* Release mutual exclusion with fed */
		(void) close(fd);
		return (1);
	}
	if (slm_proj_file == -1) {
		/* File does not exists */
		/* Release mutual exclusion with fed */
		(void) close(fd);
		return (0);
	}
	do {
		ret = SCSLM_file_parse_proj(&rg, &proj, &zone);
		if (rg == NULL || proj == NULL || zone == NULL) {
			break;
		}
		if (ret == SCSLM_SUCCESS) {
			/*
			 * SCSLM_SUCCESS: rg, proj and zone are heap allocated
			 */
			if (SCSLM_delete_proj(zone, proj) ==
			    SCSLM_SUCCESS) {
#if SOL_VERSION >= __s10
				(void) fprintf(stdout, gettext(
				    "%s: project <%s> zone <%s>"
				    " removed\n"),
				    cmdname, proj, zone);
#else
				(void) fprintf(stdout, gettext(
				    "%s: project "
				    "<%s> removed\n"),
				    cmdname, proj);
#endif
			} else {
#if SOL_VERSION >= __s10
				(void) fprintf(stderr, gettext(
				    "%s: project "
				    "<%s> zone <%s>, "
				    "unable to remove\n"),
				    cmdname, proj, zone);
#else
				(void) fprintf(stderr, gettext(
				    "%s: project "
				    "<%s>, unable to remove\n"),
				    cmdname, proj);
#endif
				continue;
			}
			/* Add to list */
			pjl = (tprjlist *)malloc(sizeof (tprjlist));
			err = errno; /*lint !e746 */
			if (pjl == NULL) {
				(void) fprintf(stderr, gettext(
				    "%s: malloc error <%s>\n"),
				    cmdname, strerror(err));
				/* Release mutual exclusion with fed */
				(void) close(fd);
				return (1);
			}
			pjl->proj = strdup(proj);
			pjl->zone = strdup(zone);
			pjl->next = NULL;
			if (prjhead == NULL) {
				prjhead = pjl;
			} else {
				pjt = prjhead;
				while (pjt->next != NULL) {
					pjt = pjt->next;
				}
				pjt->next = pjl;
			}
			free(rg);
			free(zone);
			free(proj);
		}
	} while (ret == SCSLM_SUCCESS);
	if (ret != SCSLM_PARSE_END) {
		(void) SCSLM_file_close_proj();
		/* Release mutual exclusion with fed */
		(void) close(fd);
		return (1);
	}
	(void) SCSLM_file_close_proj();
	pjl = prjhead;
	while (pjl != NULL) {
		ret = SCSLM_file_del_proj(pjl->zone, pjl->proj, 1);
		if (ret == SCSLM_SUCCESS) {
#if SOL_VERSION >= __s10
			(void) fprintf(stdout, gettext(
			    "%s: <%s> zone <%s> entry removed in %s\n"),
			    cmdname, pjl->proj, pjl->zone, SCSLM_PROJ_FILE);
#else
			(void) fprintf(stdout, gettext(
			    "%s: <%s> entry removed in %s\n"),
			    cmdname, pjl->proj, SCSLM_PROJ_FILE);
#endif
		}
		pjl = pjl->next;
	}
	/* Release mutual exclusion with fed */
	(void) close(fd);
	return (0);
}


/*
 * Function SCSLM_file_close_proj
 * Args In:
 *	None.
 * Args Out:
 *	None.
 * Return value:
 *	SCSLM_SUCCESS: project remove successfuly from file
 *	SCSLM_ERROR: failure
 */
static int
SCSLM_file_close_proj(void)
{
	int	err;

	if (slm_proj_file < 0) {
		slm_proj_file = -1;
		return (SCSLM_SUCCESS);
	}
	(void) close(slm_proj_file);
	slm_proj_file = -1;
	if (munmap((void *)slm_proj_file_ptr, slm_proj_file_size) < 0) {
		err = errno; /*lint !e746 */
		(void) fprintf(stderr, gettext("%s: "
		    "munmap <%s> error <%s>\n"),
		    cmdname, SCSLM_PROJ_FILE, strerror(err));
		return (SCSLM_ERROR);
	}
	return (SCSLM_SUCCESS);
}


/*
 * Function SCSLM_file_open_proj
 * Args In:
 *	None.
 * Args Out:
 *	None.
 * Return value:
 *	SCSLM_SUCCESS: SCSLM project file opened
 *	SCSLM_ERROR: failure
 */
static int
SCSLM_file_open_proj(void)
{
	off_t		off;
	int		err;

	if (slm_proj_file > 0) {
		return (SCSLM_SUCCESS);
	}

	slm_proj_file = open(SCSLM_PROJ_FILE, O_RDWR | O_SYNC);
	err = errno; /*lint !e746 */
	if (slm_proj_file < 0) {
		if (err == ENOENT) {
			slm_proj_file = -1;
			return (SCSLM_SUCCESS);
		}
		(void) fprintf(stderr, gettext("%s: "
		    "open <%s> error <%s>\n"),
		    cmdname, SCSLM_PROJ_FILE, strerror(err));
		slm_proj_file = -1;
		return (SCSLM_ERROR);
	}
	off = lseek(slm_proj_file, 0, SEEK_END);
	if (off == (off_t)-1) {
		err = errno; /*lint !e746 */
		(void) fprintf(stderr, gettext("%s: "
		    "lseek <%s> error <%s>\n"),
		    cmdname, SCSLM_PROJ_FILE, strerror(err));
		(void) close(slm_proj_file);
		slm_proj_file = -1;
		return (SCSLM_ERROR);
	}
	if (off == 0) {
		(void) close(slm_proj_file);
		slm_proj_file = -1;
		return (SCSLM_SUCCESS);
	}
	slm_proj_file_size = (size_t)off;
	slm_proj_file_ptr = (char *)mmap(
	    (caddr_t)0, slm_proj_file_size, (PROT_READ | PROT_WRITE),
	    MAP_PRIVATE, slm_proj_file, 0);
	if (slm_proj_file_ptr == (char *)MAP_FAILED) {
		err = errno; /*lint !e746 */
		(void) fprintf(stderr, gettext("%s: "
		    "mmap <%s> error <%s>\n"),
		    cmdname, SCSLM_PROJ_FILE, strerror(err));
		(void) close(slm_proj_file);
		slm_proj_file = -1;
		return (SCSLM_ERROR);
	}
	return (SCSLM_SUCCESS);
}


/*
 * Function SCSLM_file_parse_proj
 * Args In:
 *	project name.
 * Args Out:
 *	None.
 * Return value:
 *	SCSLM_SUCCESS: project remove successfuly from file
 *	SCSLM_ERROR: failure
 */
static int
SCSLM_file_parse_proj(char **rg, char **r, char **zone)
{
	char	*ptr;
	char	*ptr_s;
	static	char *p_s = NULL;
	char	*p_e;
	size_t	size;
	int	err;

	*rg = NULL;
	*r = NULL;
	*zone = NULL;

	if (p_s == NULL) {
		p_s =  slm_proj_file_ptr;
	}

parse_again:
	if (p_s >= (slm_proj_file_ptr + slm_proj_file_size)) {
		return (SCSLM_PARSE_END);
	}

	ptr = p_e = p_s;
	/* Search the end of the line */
	while (*p_e != '\n' &&
		p_e < (slm_proj_file_ptr + slm_proj_file_size)) {
		p_e++;
	}
	if (*p_e != '\n') {
		(void) fprintf(stderr, gettext("%s: "
		    "corrupted entry\n"), cmdname);
		return (SCSLM_ERROR);
	}
	/* skip comment line */
	if (*p_s == '#') {
		p_s = p_e + 1;
		goto parse_again;
	}
	/* read projname */
	size = 0;
	ptr_s = ptr = p_s;
	while (*ptr != '\t' && ptr < p_e) {
		ptr++;
		size++;
	}
	if (ptr >= p_e) {
		(void) fprintf(stderr, gettext("%s: "
		    "corrupted entry\n"), cmdname);
		return (SCSLM_ERROR);
	}
	*ptr = '\0';
	*r = strdup(ptr_s);
	if (*r == NULL) {
		err = errno; /*lint !e746 */
		(void) fprintf(stderr, ("%s: "
		    "strdup error <%s>\n"), cmdname, strerror(err));
		*ptr = '\t';
		return (SCSLM_ERROR);
	}
	*ptr = '\t';
	ptr++;
	/* skip projid */
	ptr_s = ptr;
	size = 0;
	while (*ptr != '\t' && ptr < p_e) {
		if (!isdigit(*ptr)) {
			(void) fprintf(stderr, gettext(
			    "%s: corrupted projid\n"), cmdname);
			return (SCSLM_ERROR);
		}
		ptr++;
		size++;
	}
	if (ptr >= p_e) {
		(void) fprintf(stderr, gettext(
		    "%s corrupted entry\n"), cmdname);
		return (SCSLM_ERROR);
	}
	if (size == 0) {
		(void) fprintf(stderr,
		    gettext("%s: corrupted projid\n"), cmdname);
		return (SCSLM_ERROR);
	}
	ptr++;
	/* Extract resource group name */
	ptr_s = ptr;
	size = 0;
	while (*ptr != '\t' && ptr < p_e) {
		ptr++;
		size++;
	}
	if (ptr >= p_e) {
		(void) fprintf(stderr,
		    gettext("%s: corrupted file\n"), cmdname);
		return (SCSLM_ERROR);
	}
	if (size == 0) {
		(void) fprintf(stderr, gettext(
		    "%s: corrupted resource group name\n"), cmdname);
		return (SCSLM_ERROR);
	}
	*ptr = '\0';
	*rg = strdup(ptr_s);
	if (*rg == NULL) {
		err = errno; /*lint !e746 */
		(void) fprintf(stderr, gettext("%s: "
		    "strdup error <%s>\n"), cmdname, strerror(err));
		*ptr = '\t';
		return (SCSLM_ERROR);
	}
	*ptr = '\t';
	ptr++;
	/* Extract zone name */
	ptr_s = ptr;
	size = 0;
	while (*ptr != '\n' && ptr <= p_e) {
		ptr++;
		size++;
	}
	if (size == 0) {
		(void) fprintf(stderr, gettext("%s: corrupted zone name\n"),
		    cmdname);
		return (SCSLM_ERROR);
	}
	*ptr = '\0';
	*zone = strdup(ptr_s);
	if (*zone == NULL) {
		err = errno; /*lint !e746 */
		(void) fprintf(stderr, gettext("%s: "
		    "strdup error <%s>\n"), cmdname, strerror(err));
		*ptr = '\n';
		return (SCSLM_ERROR);
	}
	*ptr = '\n';
	p_s = ptr + 1;
	return (SCSLM_SUCCESS);
}


/*
 * Function SCSLM_file_del_proj
 * Delete a SCSLM project from the file used
 * to store all SCSLM projects created.
 * Args In:
 *	project name.
 *	errors displayed or not
 * Args Out:
 *	None.
 * Return value:
 *	SCSLM_SUCCESS: project remove successfuly from file
 *	SCSLM_ERROR: failure
 */
/*lint -e528 */
static int
SCSLM_file_del_proj(const char *zone, char *projname, int errors)
{
	int	err;
	char	*ptr;
	size_t	size = 0;
	off_t	off;
	int	fd;
	char	*p_s;
	char	*p_e;
	char	*p;
	int	cnt_tab;
	int	deleted = 0;
	char	*fzone;
	char	proj[PROJNAME_MAX + 2];

	if (projname == NULL) {
		(void) fprintf(stderr, gettext("%s: projname NULL\n"), cmdname);
		return (SCSLM_ERROR);
	}

	fd = open(SCSLM_PROJ_FILE, O_RDWR | O_SYNC);
	err = errno; /*lint !e746 */
	if (fd < 0) {
		if (errors != 0) {
			(void) fprintf(stderr, gettext("%s: "
			    "open <%s> error <%s>\n"),
			    cmdname, SCSLM_PROJ_FILE, strerror(err));
			return (SCSLM_ERROR);
		} else {
			return (SCSLM_SUCCESS);
		}
	}
	off = lseek(fd, 0, SEEK_END);
	if (off == (off_t)-1) {
		err = errno; /*lint !e746 */
		(void) fprintf(stderr, gettext("%s: "
		    "lseek <%s> error <%s>\n"),
		    cmdname, SCSLM_PROJ_FILE, strerror(err));
		(void) close(fd);
		return (SCSLM_ERROR);
	}
	if (off == 0) {
		/* file is emty */
		if (errors != 0) {
			(void) fprintf(stderr, gettext("%s: "
			    "file <%s> is empty\n"),
			    cmdname, SCSLM_PROJ_FILE);
			(void) close(fd);
			return (SCSLM_ERROR);
		} else {
			(void) close(fd);
			return (SCSLM_SUCCESS);
		}
	}
	ptr = (char *)mmap((caddr_t)0, (size_t)off,
	    (PROT_READ | PROT_WRITE), MAP_SHARED, fd, 0);
	if (ptr == (char *)MAP_FAILED) {
		err = errno; /*lint !e746 */
		(void) fprintf(stderr, gettext("%s: "
		    "mmap <%s> error <%s>\n"),
		    cmdname, SCSLM_PROJ_FILE, strerror(err));
		(void) close(fd);
		return (SCSLM_ERROR);
	}
	(void) sprintf(proj, "%s\t", projname);
	p_s = (char *)strstr(ptr, proj);
	while (p_s != NULL && deleted == 0) {
		p_e = p_s;
		while ((p_e < (ptr + (size_t)off)) && (*p_e != '\n')) {
			p_e++;
		}
		if (*p_e != '\n') {
			(void) fprintf(stderr, gettext("%s: "
			    "file <%s> corrupted\n"),
			    cmdname, SCSLM_PROJ_FILE);
			(void) close(fd);
			(void) munmap(ptr, (size_t)off);
			return (SCSLM_ERROR);
		}
		p_e++;
		/* Now verify entry starting at p_s and ending at p_e */
		p = p_s;
		cnt_tab = 0;
		fzone = NULL;
		while (p < p_e) {
			if (*p == '\t') {
				cnt_tab++;
				if (cnt_tab == 3) {
					fzone = p + 1;
				}
			}
			p++;
		}
		if (fzone == NULL) {
			(void) fprintf(stderr, gettext("%s: "
			    "file <%s> corrupted\n"),
			    cmdname, SCSLM_PROJ_FILE);
			(void) close(fd);
			(void) munmap(ptr, (size_t)off);
			return (SCSLM_ERROR);
		}
		*(p_e - 1) = '\0';
		if (strcmp(zone, fzone) != 0) {
			*(p_e - 1) = '\n';
			p_s = (char *)strstr(p_e, proj);
			continue;
		}
		*(p_e - 1) = '\n';
		size = (size_t)(ptr + (size_t)off - p_e);
		if (size != 0) {
			(void) memcpy(p_s, p_e, size);
		}
		deleted = 1;
	}

	if ((p_s != NULL) && (deleted != 0) &&
	    (ftruncate(fd, (off_t)((p_s - ptr) + (char *)size)) < 0)) {
		err = errno; /*lint !e746 */
		(void) fprintf(stderr, gettext("%s: "
		    "ftruncate <%s> error <%s>\n"),
		    cmdname, SCSLM_PROJ_FILE, strerror(err));
		(void) close(fd);
		(void) munmap(ptr, (size_t)off);
		return (SCSLM_ERROR);
	}
	(void) close(fd);
	if (munmap(ptr, (size_t)off) < 0) {
		err = errno; /*lint !e746 */
		(void) fprintf(stderr, gettext("%s: "
		    "munmap <%s> error <%s>\n"),
		    cmdname, SCSLM_PROJ_FILE, strerror(err));
		return (SCSLM_ERROR);
	}
	if (deleted == 0) {
		return (SCSLM_ERROR_UNKNOWN_PROJECT);
	}
	return (SCSLM_SUCCESS);
}


static int
get_scha_rgs(void)
{
	int			retry = 0;
	scha_err_t		err;
	scha_cluster_t		handle;

	while (retry++ < SCHA_READY_RETRY) {
		err = scha_cluster_open(&handle);
		if (err != SCHA_ERR_NOERR) {
			(void) sleep(SCHA_READY_DELAY);
		} else {
			err = scha_cluster_get(handle,
			    SCHA_ALL_RESOURCEGROUPS, &all_rgs);
			if (err != SCHA_ERR_NOERR) {
				(void) fprintf(stderr,
				    gettext("%s: "
				    "FAILED: scha_cluster_get()"), cmdname);
				all_rgs = NULL;
				(void) scha_cluster_close(&handle);
				return (1);
			}
			(void) scha_cluster_close(&handle);
			return (0);
		}
	}
	(void) fprintf(stderr, gettext("%s: "
	    "scha_cluster_open() error %d\n"), cmdname, err);
	return (1);
}


/*
 *
 */
static int
search_rg(const char *rg_name)
{
	unsigned int		ix;

	if (all_rgs == NULL) {
		return (RG_NOT_FOUND);
	}
	for (ix = 0; ix < all_rgs->array_cnt; ix++) {
		if (strcmp(rg_name, all_rgs->str_array[ix]) == 0) {
			return (RG_FOUND);
		}
	}
	return (RG_NOT_FOUND);
}


/*
 * Function SCSLM_delete_proj
 * Args In:
 * 	a zone name
 *	a project name
 * Args Out:
 *	none
 * Return value:
 *	SCSLM_SUCCESS: project deleted
 *	SCSLM_ERROR_UNKNOWN_PROJECT: project not found
 *	SCSLM_ERROR_UNKNOWN_ZONE: zone not found
 *	SCSLM_ERROR: other errors
 */
static int
SCSLM_delete_proj(char *zone, char *proj)
{
	int	ret;
	int	cmd_ret;
	char 	*cmd_output;
	int	local_zone = 0;
	char	cmd[MAX_COMMAND_SIZE];
	char	serr[MAX_COMMAND_SIZE];

	if (strcmp(zone, GLOBAL_ZONENAME) == 0) {
		ret = snprintf(cmd,
		    MAX_COMMAND_SIZE, "%s %s 2>&1",
		    PROJ_DEL, proj);
		if (ret == MAX_COMMAND_SIZE || ret <= 0) {
			(void) fprintf(stderr, gettext("%s: "
			    "<%s> snprintf errno %d\n"),
			    cmdname, proj, errno);
			return (SCSLM_ERROR);
		}
#if SOL_VERSION >= __s10
	} else {
		local_zone = 1;
		ret = snprintf(cmd, MAX_COMMAND_SIZE,
		    "%s -S %s %s %s 2>&1",
		    ZONE_CMD, zone, PROJ_DEL, proj);
		if (ret == MAX_COMMAND_SIZE || ret <= 0) {
			(void) fprintf(stderr, gettext("%s: "
			    "<%s> zone <%s> snprintf errno %d\n"),
			    cmdname, proj, zone, errno);
			return (SCSLM_ERROR);
		}
	}
#else
	}
#endif
	ret = SCSLM_exec(cmd, &cmd_ret, &cmd_output);
	if (ret != SCSLM_SUCCESS) {
		return (ret);
	}
	if (cmd_ret != 0) {
		if (cmd_output != NULL) {
			if (local_zone == 1) {
				ret = snprintf(serr,  MAX_COMMAND_SIZE,
				    "Project \"%s\" does not exist",
				    proj);
			} else {
				ret = snprintf(serr,  MAX_COMMAND_SIZE,
				    "Project \"%s\" does not exist",
				    proj);
			}
			if (ret == MAX_COMMAND_SIZE || ret <= 0) {
				(void) fprintf(stderr, gettext("%s: "
				    "snprintf errno %d\n"), cmdname, errno);
				free(cmd_output);
				return (SCSLM_ERROR);
			}
			if (strstr(cmd_output, serr) != NULL) {
				free(cmd_output);
				return (SCSLM_ERROR_UNKNOWN_PROJECT);
			}
			ret = snprintf(serr, MAX_COMMAND_SIZE,
			    "zlogin: zone \'%s\' unknown", zone);
			if (ret == MAX_COMMAND_SIZE || ret <= 0) {
				(void) fprintf(stderr, gettext("%s: "
				    "snprintf errno %d\n"), cmdname, errno);
				free(cmd_output);
				return (SCSLM_ERROR);
			}
			if (strstr(cmd_output, serr) != NULL) {
				free(cmd_output);
				return (SCSLM_ERROR_UNKNOWN_ZONE);
			}
			(void) fprintf(stderr, gettext("%s: "
			    "<%s> ret %d, outputs <%s>\n"),
			    cmdname, cmd, cmd_ret, cmd_output);
			free(cmd_output);
			return (SCSLM_ERROR);
		} else {
			(void) fprintf(stderr, gettext("%s: "
			    "<%s> ret %d\n"), cmdname, cmd, cmd_ret);
			return (SCSLM_ERROR);
		}
	} else {
		if (cmd_output != NULL) {
			free(cmd_output);
		}
	}
	return (SCSLM_SUCCESS);
}


/*
 * Function SCSLM_exec
 * a command to execute through popen(),it should include 2>&1 to
 * have also stderr output of command.
 * Args In:
 *	the command
 * Args Out:
 * 	the command return value
 *	the command outputs
 * Return value:
 *	SCSLM_SUCCESS: command launched, return_code set
 *	SCSLM_ERROR: failure in launching the command
 */
static int
SCSLM_exec(char *cmd, int *return_code, char **cmd_output)
{
	FILE	*ptr;
	size_t	cnt = 0;
	size_t	ssize;
	int	err;
	char	*tmp;
	char	buf[SCSLM_FGETS_SIZE];

	*cmd_output = NULL;
	*return_code = 1;

	if ((ptr = popen(cmd, "r")) != NULL) {
		while (fgets(buf, SCSLM_FGETS_SIZE, ptr) == buf) {
			ssize = strlen(buf);
			if (ssize == 0) {
				(void) fprintf(stderr, gettext("%s: "
				    "<%s> strlen is zero\n"), cmdname, cmd);
				if (*cmd_output != NULL) {
					free(*cmd_output);
					*cmd_output = NULL;
				}
				return (SCSLM_ERROR);
			}
			/* realloc with NULL is like malloc */
			tmp = (char *)realloc(*cmd_output, cnt + ssize + 1);
			err = errno; /*lint !e746 */
			if (tmp == NULL) {
				(void) fprintf(stderr, gettext("%s: "
				    "realloc error <%s>\n"),
				    cmdname, strerror(err));
				return (SCSLM_ERROR);
			}
			(void) memcpy(tmp + cnt, buf, ssize);
			tmp[cnt + ssize] = '\0';
			*cmd_output = tmp;
			cnt += ssize;
		}
		*return_code = (int)((unsigned int)(pclose(ptr)) >> 8);
		return (SCSLM_SUCCESS);
	}
	err = errno; /*lint !e746 */
	(void) fprintf(stderr, gettext(
	    "%s: <%s> popen errno %d\n"), cmdname, cmd, err);
	return (SCSLM_ERROR);
}


#if SOL_VERSION >= __s10
/*
 * Function SCSLM_delete_pool
 * Args In:
 * 	a pool name
 * Args Out:
 *	none
 * Return value:
 *	SCSLM_SUCCESS: pool deleted
 *	SCSLM_ERROR_UNKNOWN_POOL: pool does not exist
 *	SCSLM_ERROR: other errors
 */
static int
SCSLM_delete_pool(char *pool)
{
	int		err;
	int		ret;
	pool_conf_t	*pool_conf;
	pool_t		*mpool;
	const char	*static_conf;
	char		scslmpool[MAX_COMMAND_SIZE];

	ret = snprintf(scslmpool, MAX_COMMAND_SIZE, "%s%s", SCSLM_POOL, pool);
	err = errno; /*lint !e746 */
	if (ret >= MAX_COMMAND_SIZE || ret < 0) {
		(void) fprintf(stderr, gettext("%s SCSLM_delete_pool "
		    "<%s%s> snprintf errno %d\n"),
		    cmdname, SCSLM_POOL, pool, err);
		return (SCSLM_ERROR);
	}
	static_conf = pool_static_location();
	pool_conf = pool_conf_alloc();
	if (pool_conf == NULL) {
		(void) fprintf(stderr, gettext("%s SCSLM_delete_pool <%s> "
		    "pool_conf_alloc error <%s>\n"),
		    cmdname, scslmpool, pool_strerror(pool_error()));
		return (SCSLM_ERROR);
	}
	if (pool_conf_open(pool_conf, static_conf, PO_RDWR) < 0) {
		(void) fprintf(stderr, gettext("%s SCSLM_delete_pool <%s> "
		    "pool_conf_open <%s> error <%s>\n"),
		    cmdname, scslmpool,
		    static_conf, pool_strerror(pool_error()));
		pool_conf_free(pool_conf);
		return (SCSLM_ERROR);
	}
	mpool = pool_get_pool(pool_conf, scslmpool);
	if (mpool == NULL) {
		if (pool_conf_close(pool_conf) < 0) {
			(void) fprintf(stderr, gettext(
			    "%s SCSLM_delete_pool <%s> "
			    "pool_conf_close error <%s>\n"),
			    cmdname, scslmpool, pool_strerror(pool_error()));
		}
		pool_conf_free(pool_conf);
		return (SCSLM_ERROR_UNKNOWN_POOL);
	}
	if (pool_destroy(pool_conf, mpool) < 0) {
		(void) fprintf(stderr, gettext("%s SCSLM_delete_pool <%s> "
		    "pool_resource_destroy error <%s>\n"),
		    cmdname, scslmpool, pool_strerror(pool_error()));
		if (pool_conf_close(pool_conf) < 0) {
			(void) fprintf(stderr, gettext(
			    "%s SCSLM_delete_pool <%s> "
			    "pool_conf_close error <%s>\n"),
			    cmdname, scslmpool, pool_strerror(pool_error()));
		}
		pool_conf_free(pool_conf);
		return (SCSLM_ERROR);
	}
	/* Commit in the dynamic configuration */
	if (pool_conf_commit(pool_conf, PO_TRUE) < 0) {
		(void) fprintf(stderr, gettext("%s SCSLM_delete_pool <%s> "
		    "pool_conf_commit dynamic error <%s>\n"),
		    cmdname, scslmpool, pool_strerror(pool_error()));
		if (pool_conf_close(pool_conf) < 0) {
			(void) fprintf(stderr, gettext(
			    "%s SCSLM_delete_pool <%s> "
			    "pool_conf_close error <%s>\n"),
			    cmdname, scslmpool, pool_strerror(pool_error()));
		}
		pool_conf_free(pool_conf);
		return (SCSLM_ERROR);
	} else {
		/* Commit in the static configuration */
		if (pool_conf_commit(pool_conf, PO_FALSE) < 0) {
			(void) fprintf(stderr, gettext(
			    "%s SCSLM_delete_pool <%s> "
			    "pool_conf_commit static error <%s>\n"),
			    cmdname, scslmpool, pool_strerror(pool_error()));
			if (pool_conf_close(pool_conf) < 0) {
				(void) fprintf(stderr, gettext(
				    "%s SCSLM_delete_pool <%s> "
				    "pool_conf_close error <%s>\n"),
				    cmdname, scslmpool,
				    pool_strerror(pool_error()));
			}
			pool_conf_free(pool_conf);
			return (SCSLM_ERROR);
		}
	}
	if (pool_conf_close(pool_conf) < 0) {
		(void) fprintf(stderr, gettext(
		    "SCSLM_delete_pool <%s> "
		    "%s pool_conf_close error <%s>\n"),
		    cmdname, scslmpool, pool_strerror(pool_error()));
	}
	pool_conf_free(pool_conf);
	return (SCSLM_SUCCESS);
}


/*
 * Function SCSLM_delete_pset
 * Args In:
 * 	a pset name
 * Args Out:
 *	none
 * Return value:
 *	SCSLM_SUCCESS: pset deleted
 *	SCSLM_ERROR_UNKNOWN_PSET: pset does not exist
 *	SCSLM_ERROR: other errors
 */
static int
SCSLM_delete_pset(char *pset)
{
	int		err;
	int		ret;
	pool_conf_t	*pool_conf;
	pool_resource_t	*resource;
	const char	*static_conf;
	char		scslmpset[MAX_COMMAND_SIZE];

	ret = snprintf(scslmpset, MAX_COMMAND_SIZE, "%s%s", SCSLM_PSET, pset);
	err = errno; /*lint !e746 */
	if (ret >= MAX_COMMAND_SIZE || ret < 0) {
		(void) fprintf(stderr, gettext("%s SCSLM_delete_pset "
		    "<%s%s> snprintf errno %d\n"),
		    cmdname, SCSLM_PSET, pset, err);
		return (SCSLM_ERROR);
	}
	static_conf = pool_static_location();
	pool_conf = pool_conf_alloc();
	if (pool_conf == NULL) {
		(void) fprintf(stderr, gettext("%s SCSLM_delete_pset <%s> "
		    "pool_conf_alloc error <%s>\n"),
		    cmdname, scslmpset, pool_strerror(pool_error()));
		return (SCSLM_ERROR);
	}
	if (pool_conf_open(pool_conf, static_conf, PO_RDWR) < 0) {
		(void) fprintf(stderr, gettext("%s SCSLM_delete_pset <%s> "
		    "pool_conf_open <%s> error <%s>\n"),
		    cmdname, scslmpset,
		    static_conf, pool_strerror(pool_error()));
		pool_conf_free(pool_conf);
		return (SCSLM_ERROR);
	}
	resource = pool_get_resource(pool_conf, "pset", scslmpset);
	if (resource == NULL) {
		if (pool_conf_close(pool_conf) < 0) {
			(void) fprintf(stderr, gettext(
			    "%s SCSLM_delete_pset <%s> "
			    "pool_conf_close error <%s>\n"),
			    cmdname, scslmpset, pool_strerror(pool_error()));
		}
		pool_conf_free(pool_conf);
		return (SCSLM_ERROR_UNKNOWN_PSET);
	}
	if (pool_resource_destroy(pool_conf, resource) < 0) {
		(void) fprintf(stderr, gettext("%s SCSLM_delete_pset <%s> "
		    "pool_resource_destroy error <%s>\n"),
		    cmdname, scslmpset, pool_strerror(pool_error()));
		if (pool_conf_close(pool_conf) < 0) {
			(void) fprintf(stderr, gettext(
			    "%s SCSLM_delete_pset <%s> "
			    "pool_conf_close error <%s>\n"),
			    cmdname, scslmpset, pool_strerror(pool_error()));
		}
		pool_conf_free(pool_conf);
		return (SCSLM_ERROR);
	}
	/* Commit in the dynamic configuration */
	if (pool_conf_commit(pool_conf, PO_TRUE) < 0) {
		(void) fprintf(stderr, gettext("%s SCSLM_delete_pset <%s> "
		    "pool_conf_commit dynamic error <%s>\n"),
		    cmdname, scslmpset, pool_strerror(pool_error()));
		if (pool_conf_close(pool_conf) < 0) {
			(void) fprintf(stderr, gettext(
			    "%s SCSLM_delete_pset <%s> "
			    "pool_conf_close error <%s>\n"),
			    cmdname, scslmpset, pool_strerror(pool_error()));
		}
		pool_conf_free(pool_conf);
		return (SCSLM_ERROR);
	} else {
		/* Commit in the static configuration */
		if (pool_conf_commit(pool_conf, PO_FALSE) < 0) {
			(void) fprintf(stderr, gettext(
			    "%s SCSLM_delete_pset <%s> "
			    "pool_conf_commit static error <%s>\n"),
			    cmdname, scslmpset, pool_strerror(pool_error()));
			if (pool_conf_close(pool_conf) < 0) {
				(void) fprintf(stderr, gettext(
				    "%s SCSLM_delete_pset <%s> "
				    "pool_conf_close error <%s>\n"),
				    cmdname, scslmpset,
				    pool_strerror(pool_error()));
			}
			pool_conf_free(pool_conf);
			return (SCSLM_ERROR);
		}
	}
	if (pool_conf_close(pool_conf) < 0) {
		(void) fprintf(stderr, gettext(
		    "%s SCSLM_delete_pset <%s> "
		    "pool_conf_close error <%s>\n"),
		    cmdname, scslmpset, pool_strerror(pool_error()));
	}
	pool_conf_free(pool_conf);
	return (SCSLM_SUCCESS);
}


/*
 * Function SCSLM_cleanup
 * Args In:
 *	None
 * Args Out:
 *	None
 * Return value:
 *	SCSLM_SUCCESS: cleanup done
 *	SCSLM_ERROR: other errors
 */
static int
SCSLM_cleanup(void)
{
	int		err;
	pool_conf_t	*pool_conf;
	const char	*static_conf;
	char		*info;
	char 		*tmp;
	char		*end;
	int		ret;
	char		serr[MAX_COMMAND_SIZE];

	pool_conf = pool_conf_alloc();
	if (pool_conf == NULL) {
		(void) fprintf(stderr, gettext("%s SCSLM_cleanup "
		    "pool_conf_alloc error <%s>\n"),
		    cmdname, pool_strerror(pool_error()));
		return (SCSLM_ERROR);
	}

	static_conf = pool_static_location();
	if (pool_conf_open(pool_conf, static_conf, PO_RDWR) < 0) {
		(void) fprintf(stderr, gettext("%s SCSLM_cleanup "
		    "pool_conf_open <%s> error <%s>\n"),
		    cmdname, static_conf, pool_strerror(pool_error()));
		return (SCSLM_ERROR);
	}

	info = pool_conf_info(pool_conf, PO_TRUE);
	if (info == NULL) {
		(void) fprintf(stderr, gettext("%s SCSLM_cleanup "
		    "pool_conf_info <%s>\n"),
		    cmdname, pool_strerror(pool_error()));
		return (SCSLM_ERROR);
	}

	tmp = info;
	/* Remove all the SCSLM pools */
	ret = snprintf(serr,  MAX_COMMAND_SIZE,
	    "pool %s", SCSLM_POOL);
	err = errno; /*lint !e746 */
	if (ret >= MAX_COMMAND_SIZE || ret < 0) {
		(void) fprintf(stderr, gettext("%s SCSLM_cleanup "
		    "snprintf errno %d\n"), cmdname, err);
		free(info);
		return (SCSLM_ERROR);
	}
	while (tmp != NULL) {
		tmp = strstr(tmp, serr);
		if (tmp != NULL) {
			tmp += (strlen("pool ") + strlen(SCSLM_POOL));
			end = tmp;
			while (*end != '\n') {
				end++;
			}
			*end = '\0';
			ret = SCSLM_delete_pool(tmp);
			if (ret != SCSLM_SUCCESS) {
				(void) fprintf(stderr, gettext(
				    "%s SCSLM_cleanup "
				    "SCSLM_delete_pool ret %d\n"),
				    cmdname, ret);
			}
			(void) fprintf(stdout, gettext(
			    "%s : delete pool %s\n"),
			    cmdname, tmp);
			*end = '\n';
		}
	}
	/* Remove all the SCSLM pset */
	tmp = info;
	ret = snprintf(serr,  MAX_COMMAND_SIZE,
	    "pset %s", SCSLM_PSET);
	err = errno; /*lint !e746 */
	if (ret >= MAX_COMMAND_SIZE || ret < 0) {
		(void) fprintf(stderr, gettext("%s SCSLM_cleanup "
		    "snprintf errno %d\n"), cmdname, err);
		free(info);
		return (SCSLM_ERROR);
	}
	while (tmp != NULL) {
		tmp = strstr(tmp, serr);
		if (tmp != NULL) {
			tmp += (strlen("pset ") + strlen(SCSLM_PSET));
			end = tmp;
			while (*end != '\n') {
				end++;
			}
			*end = '\0';
			ret = SCSLM_delete_pset(tmp);
			if (ret != SCSLM_SUCCESS) {
				(void) fprintf(stderr, gettext(
				    "%s SCSLM_cleanup "
				    "SCSLM_delete_pset <%s> ret %d\n"),
				    cmdname, tmp, ret);
			}
			(void) fprintf(stdout, gettext(
			    "%s : delete pset %s\n"),
			    cmdname, tmp);
			*end = '\n';
		}
	}
	free(info);
	return (SCSLM_SUCCESS);
}
#endif	/* SOL_VERSION >= __s10 */
#endif	/* !SC_SRM */
