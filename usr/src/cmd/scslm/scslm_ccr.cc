/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scslm_ccr.cc	1.3	08/05/20 SMI"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <libgen.h>
#include <string.h>
#include <libintl.h>		/* for gettext */
#include <locale.h>

/* cluster */
#include <rgm/sczones.h>

/* Command line utilities */
#include <sys/cl_auth.h>
#include <sys/cl_cmd_event.h>
#include <sys/cl_events.h>

/* Ganymede */
#include <scslm.h>
#include <scslm_sensor.h>
#include <scslm_threshold.h>
#include <scslm_ccr.h>
#include <scslm_cli.h>

#define	ACTION_REMOVE 0x01
#define	ACTION_CREATE_DEFAULT 0x02

#define	INPUT_SENSORS "sensors"
#define	INPUT_THRESHOLDS "thresholds"

#define	EXIT() { if (scslm_errno != SCSLM_NOERR)	\
			scslm_perror(NULL); exit(scslm_errno); }


typedef struct tabinfo {
	char *input;
	char *table;
	int action;
} tabinfo_t;


static tabinfo_t mappings[3] = {
	{INPUT_SENSORS, SCSLMADM_TABLE, (ACTION_CREATE_DEFAULT|ACTION_REMOVE)},
	{INPUT_THRESHOLDS, SCSLMTHRESH_TABLE, ACTION_REMOVE},
	{NULL, NULL, 0}};


static void
usage(char *name)
{
	(void) printf(
	    "%s:\n\t %s -c|-d %s|%s\n",
	    gettext("usage"), basename(name), INPUT_SENSORS, INPUT_THRESHOLDS);
	EXIT(); //lint !e527
}


char *
gettablename(const char *name, int action) {
	int i = 0;
	while (mappings[i].input != NULL) {
		if (strcmp(mappings[i].input, name) == 0 &&
		    (mappings[i].action & action)) {
			return (mappings[i].table);
		}
		i++;
	}
	return (NULL);
}

/*
 * MAIN
 */
int
main(int argc, char **argv)
{
	int c;
	char *tablename = NULL;
	char *inputstring = NULL;
	int action = 0;

	/* Initialize, demote to normal uid, check */
	/* authorization and then promote to euid=0 */
	cl_auth_init();
	cl_auth_demote();

	(void) setlocale(LC_ALL, "");
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);
	(void) textdomain(TEXT_DOMAIN);

	// parse options
	while ((c = getopt(argc, argv, "c:d:")) != EOF) {
		switch (c) {
		case 'c' :
			if (inputstring != NULL) {
				usage(argv[0]);
			}

			inputstring = strdup(optarg);
			if (inputstring == NULL) {
				(void) scslm_seterror(SCSLM_ENOMEM,
				    gettext("Unable to allocate memory\n"));
				EXIT();	/*lint !e527 */
			}

			action = ACTION_CREATE_DEFAULT;
			break;
		case 'd' :
			if (inputstring != NULL) {
				usage(argv[0]);
			}

			inputstring = strdup(optarg);
			if (inputstring == NULL) {
				(void) scslm_seterror(SCSLM_ENOMEM,
				    gettext("Unable to allocate memory\n"));
				EXIT();	/*lint !e527 */
			}

			action = ACTION_REMOVE;
			break;
		default:
			(void) scslm_seterror(SCSLM_EINVAL,
			    gettext("option \"%c\" is not supported.\n"),
			    optopt);
			usage(argv[0]);
			break;	/* UNREACHABLE usage is exiting */
		}
	}

	if (inputstring == NULL ||
	    (action != ACTION_REMOVE && action != ACTION_CREATE_DEFAULT)) {
		usage(argv[0]);
	}

	if (optind < argc) {
		(void) scslm_seterror(SCSLM_EINVAL,
		    gettext("unknown argument \"%s\".\n"),
		    argv[optind]);
		usage(argv[0]);
	}

	/* This command can only be called from global zone */
	if (sc_zonescheck() != 0) {
		(void) scslm_seterror(SCSLM_EACCESS,
		    gettext("Current user is not allowed to "
			"execute this command.\n"));
		EXIT();		/*lint !e527 */
	}

	tablename = gettablename(inputstring, action);
	if (tablename == NULL) {
		(void) scslm_seterror(SCSLM_ENOENT,
		    gettext("The keyword \"%s\" is not valid\n"), inputstring);
		EXIT();		/*lint !e527 */
	}


	/*
	 * basic checks
	 */
	cl_auth_check_command_opt_exit(*argv, NULL,
	    CL_AUTH_SYSTEM_MODIFY, SCSLM_EACCESS);

	cl_auth_promote();

	/* Generate event */
	cl_cmd_event_init(argc, argv, (int *)&scslm_errno);
	(void) cl_cmd_event_gen_start_event();
	(void) atexit(cl_cmd_event_gen_end_event);


	/*
	 * execute actions
	 */
	switch (action) {
	case ACTION_CREATE_DEFAULT:
		/* status is also in scslm_errno */
		(void) scslmadm_create_default_ccr(tablename);
		EXIT();		/*lint !e527 */
	case ACTION_REMOVE:
		/* status is also in scslm_errno */
		(void) scslm_delete_ccr(tablename);
		/* Do not want to fail if the table did not exist */
		if (scslm_errno == SCSLM_ENOENT) {
			scslm_errno = SCSLM_NOERR;
			goto success;
		}
		EXIT();		/*lint !e527 */
	default:
		/* add a default to make lint happy */
		(void) scslm_seterror(SCSLM_EINTERNAL,
		    gettext("Internal errror\n"));
		EXIT();		/*lint !e527 */
	}

	/*
	 * There is no need to send a config changed event
	 * as the deletion and creation of the CCR table with
	 * this command will hapeen only a installation and
	 * uninstallation time
	 */

success:

	return (SCSLM_NOERR);
}
