/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scslm_projmod.c	1.2	08/05/20 SMI"


#include <stdio.h>
#include <stdlib.h>
#include <project.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <limits.h>
#include <libgen.h>
#include <pwd.h>
#include <project.h>
#include <libintl.h>


#define	STRINGLEN		128
#define	BUFFSIZE		1024
#define	ETCPROJECT		(char *)"/etc/project"
#define	ETCPROJECTBACKUP	(char *)"/etc/project.bak"
#ifdef DEBUG
#define	TMPPROJECT		(char *)"/tmp/project.tmp"
#else
#define	TMPPROJECT		(char *)"/etc/project.tmp"
#endif




typedef enum {KEY = 0, EQUAL, VALUE, SEMICOLON} state_t;
typedef enum {UNKNOWN = 0, ADD, REMOVE} action_t;
typedef struct proj_attr {
	char key[STRINGLEN];
	char value[STRINGLEN];
	struct proj_attr *next;
} proj_attr_t;



char *attributes_result[128];
proj_attr_t *list = NULL;

char buffer[BUFFSIZE];

void
usage(char *name)
{
	(void) fprintf(stderr, gettext(
	    "usage :\n\t%s -p <projectname> -a <key>=<value>"
	    "[;<key>=<value>]*\n"), basename(name));
	(void) fprintf(stderr, gettext(
	    "usage :\n\t%s -p <projectname> -r <key>"
	    "[;<key>]*\n"), basename(name));
}

void
del_attribute_from_list(char *key)
{
	proj_attr_t *ptr = list;
	proj_attr_t *prev = NULL;
	while (ptr != NULL) {
		if (strcmp(ptr->key, key) == 0) {
			/* found */
			if (prev == NULL) {
				(void) fprintf(stdout, gettext(
				    "Delete first\n"));
				list = ptr->next;
				free(ptr);
				return;
			}
			(void) fprintf(stdout, gettext(
			    "Delete middle\n"));
			prev->next = ptr->next;
			free(ptr);
			ptr = prev->next;
			return;
		}
		prev = ptr;
		ptr = ptr->next;
	}
}

proj_attr_t *
get_attribute_index(char *key) {
	proj_attr_t *ptr = list;
	proj_attr_t *tmp = NULL;
	proj_attr_t *prev = list;
	while (ptr != NULL) {
		if (strcmp(ptr->key, key) == 0) {
			/* found */
			return (ptr);
		}

		prev = ptr;
		ptr = ptr->next;
	}


	tmp = (proj_attr_t *)calloc(1, sizeof (proj_attr_t));
	if (tmp == NULL) {
		(void) fprintf(stderr, gettext(
		    "Unable to allocate memory\n"));
		exit(1);
	}

	if (list == NULL) {
		list = tmp;
	} else {
		prev->next = tmp;
	}

	return (tmp);
}

void
remove_attributes(char *attr)
{

	char string[STRINGLEN];
	state_t state = KEY;
	int i;
	char tmp[2];

	(void) memset(string, 0, STRINGLEN);

	for (i = 0; i < ((int)strlen(attr)) + 1; i++) {
		switch (attr[i]) {
		case '\0':
			if (state == VALUE) {
				del_attribute_from_list(string);
				return;
			}
			if (state == SEMICOLON) {
				return;
			}
			(void) fprintf(stderr, gettext(
			    "WARNING : Last attribute doesn't seem "
			    "to be complete. Discarding it\n"));
			return;
		case ' ':
			/* remove spaces */
			break;
		case ';':
			state = SEMICOLON;
			del_attribute_from_list(string);
			(void) memset(string, 0, STRINGLEN);
			break;
		default:
			state = VALUE;
			tmp[0] = attr[i];
			tmp[1] = '\0';
			(void) strncat(string, tmp, STRINGLEN);
			break;
		}
	}
}

void
insert_attributes(char *attr)
{

	char string[STRINGLEN];
	state_t state = KEY;
	proj_attr_t *ptr = NULL;
	int i;
	char tmp[2];

	(void) memset(string, 0, STRINGLEN);

	for (i = 0; i < ((int)strlen(attr)) + 1; i++) {
		switch (attr[i]) {
		case '\0':
			if (state == VALUE) {
				(void) strncpy(ptr->value, string, STRINGLEN);
				(void) memset(string, 0, STRINGLEN);
				return;
			}
			if (state == SEMICOLON) {
				return;
			}
			(void) fprintf(stderr, gettext(
			    "WARNING : Last attribute doesn't seem "
			    "to be complete. Discarding it\n"));
			return;
		case ' ':
			/* remove spaces */
			break;
		case '=':
			if (state != KEY) {
				(void) fprintf(stderr, gettext(
				    "Parser error\n"));
				exit(1);
			}
			state = EQUAL;
			ptr = get_attribute_index(string);
			(void) strncpy(ptr->key, string, STRINGLEN);
			(void) memset(string, 0, STRINGLEN);
			break;
		case ';':
			if (state != VALUE) {
				(void) fprintf(stderr, gettext(
				    "Parser error\n"));
				exit(1);
			}
			state = SEMICOLON;
			(void) strncpy(ptr->value, string, STRINGLEN);
			(void) memset(string, 0, STRINGLEN);
			break;
		default:
			if (state == EQUAL) {
				state = VALUE;
			}
			if (state == SEMICOLON) {
				state = KEY;
			}
			tmp[0] = attr[i];
			tmp[1] = '\0';
			(void) strncat(string, tmp, STRINGLEN);
			break;
		}
	}
}






void
convert_list(char **attributes)
{
	int size = 1; /* 1 for '\0' */
	proj_attr_t *ptr = list;
	char *p;

	while (ptr != NULL) {
		size += (int)strlen(ptr->key) +
		    (int)strlen(ptr->value) + 2; /* 2 for '=' and ';' */
		ptr = ptr->next;
	}

	*attributes = (char *)calloc(1, (size_t)size);
	if (*attributes == NULL) {
		(void) fprintf(stderr, gettext(
		    "Unable to allocate memory\n"));
		exit(1);
	}

	/* rewind */
	ptr = list;
	while (ptr != NULL) {
		(void) strncat(*attributes, ptr->key, (size_t)size);
		(void) strncat(*attributes, "=", (size_t)size);
		(void) strncat(*attributes, ptr->value, (size_t)size);
		(void) strncat(*attributes, ";", (size_t)size);
		ptr = ptr->next;
	}
	/* The last attribute must not have a ';' */
	p = *attributes + strlen(*attributes) - 1;
	*p = '\0';
}

int
main(int argc, char *argv[])
{
	char outfile[PATH_MAX];
	int c;
	char *attributes = NULL;
	char *project_name = NULL;
	char *attributes_out = NULL;
	int rename_res;
	action_t action = UNKNOWN;
	struct project proj;
	FILE *in;
	FILE *out;
	int max;
	proj_attr_t *ptr;
	int i;
	int unlink_res;
	int err;

#ifndef DEBUG
	if (getuid() != 0 && geteuid() != 0) {
		(void) fprintf(stderr, gettext(
		    "Must be root to use %s\n"), basename(argv[0]));
		exit(1);
	}
#endif

	/* parse options */
	while ((c = getopt(argc, argv, "p:r:a:")) != EOF) {
		switch (c) {
		case 'r':
			if (action != UNKNOWN) {
				(void) fprintf(stderr, gettext(
				    "Option -a and -r are exclusive\n"));
				usage(argv[0]);
				exit(1);
			}
			action = REMOVE;
			attributes = optarg;
			break;
		case 'a':
			if (action != UNKNOWN) {
				(void) fprintf(stderr, gettext(
				    "Option -a and -r are exclusive\n"));
				usage(argv[0]);
				exit(1);
			}
			action = ADD;
			attributes = optarg;
			break;

		case 'p':
			project_name = optarg;
			break;
		default:
			usage(argv[0]);
			exit(1);
		}
	}

	if (attributes == NULL ||
	    project_name == NULL ||
	    action == UNKNOWN) {
		usage(argv[0]);
		exit(1);
	}

	(void) snprintf(outfile,
	    PATH_MAX, "%s.%d", TMPPROJECT, getpid());

	in = fopen(ETCPROJECT, "r");
	err = errno; /*lint !e746 */
	if (in == NULL) {
		(void) fprintf(stderr, gettext(
		    "Unable to open file '%s'. %s\n"),
		    ETCPROJECT, strerror(err));
		exit(1);
	}

	out = fopen(outfile, "w+");
	err = errno; /*lint !e746 */
	if (out == NULL) {
		(void) fprintf(stderr, gettext(
		    "Unable to open file '%s'. %s\n"),
		    outfile, strerror(err));
		exit(1);
	}

	/* browse /etc/project */
	while (fgetprojent(in, &proj, buffer, BUFFSIZE) != NULL) {
		int j;
		(void) fprintf(out, "%s:%d:%s:",
		    proj.pj_name,
		    proj.pj_projid,
		    proj.pj_comment);

		j = 0;
		while (proj.pj_users[j] != NULL) {
			(void) fprintf(out, "%s%s",
			    (j != 0)?(char *)",":"",
			    proj.pj_users[j]);
			j++;
		}

		(void) fprintf(out, ":");

		j = 0;
		while (proj.pj_groups[j] != NULL) {
			(void) fprintf(out, "%s%s", (j != 0)?(char *)",":"",
			    proj.pj_groups[j]);
			j++;
		}
		(void) fprintf(out, ":");
		max = (int)strlen(project_name);
		if ((int)strlen(proj.pj_name) > max)
			max = (int)strlen(proj.pj_name);

		if (strncmp(project_name, proj.pj_name, (size_t)max) != 0) {
			(void) fprintf(out, "%s", proj.pj_attr);
		} else {
			insert_attributes(proj.pj_attr);

			if (action == ADD) {
				insert_attributes(attributes);
			}
			if (action == REMOVE) {
				remove_attributes(attributes);
			}
			convert_list(&attributes_out);
			(void) fprintf(out, "%s", attributes_out);
			free(attributes_out);
		}
		(void) fprintf(out, "\n");
	}

	/* TODO : remove */
	ptr = list;
	i = 0;
	while (ptr != NULL)  {
		(void) fprintf(stdout,
		    "%3d) %s=%s\n", i+1, ptr->key, ptr->value);
		i++;
		ptr = ptr->next;

	}
	/* TODO : end of remove */

	(void) fclose(in);
	(void) fsync(fileno(out));
	(void) fclose(out);

#ifndef DEBUG
	/* create backup of /etc/project */
	rename_res = rename(ETCPROJECT, ETCPROJECTBACKUP);
	err = errno; /*lint !e746 */
	if (rename_res != 0) {
		(void) fprintf(stderr, gettext(
		    "Unable to create a backup of file '%s'"
		    ". %s\n"), ETCPROJECT, strerror(err));
		unlink_res = unlink(outfile);
		err = errno; /*lint !e746 */
		if (unlink_res != 0) {
			(void) fprintf(stderr, gettext(
			    "Unable to delete file '%s'. %s\n"),
			    outfile, strerror(err));
		}
		exit(1);
	}

	/* copy our temporary file in place of /etc/project */
	rename_res = rename(outfile, ETCPROJECT);
	err = errno; /*lint !e746 */
	if (rename_res != 0) {
		/* Undo */
		(void) rename(ETCPROJECTBACKUP, ETCPROJECT);

		(void) fprintf(stderr, gettext(
		    "Unable to update file '%s'. %s\n"),
		    ETCPROJECT, strerror(err));
		(void) unlink(outfile);
		exit(1);
	}
#endif
	sync();
	exit(0);
}
