/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scslm_getsched.c	1.2	08/05/20 SMI"

#include <stdio.h>
#include <procfs.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/param.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <locale.h>
#include <string.h>

void
print_scheduling_class(psinfo_t *psinfo)
{
	(void) fprintf(stdout, "%s\n", psinfo->pr_lwp.pr_clname);
}


int
read_psinfo(pid_t pid, psinfo_t *psinfo)
{
	char buf[MAXPATHLEN];
	int fd;

	(void) snprintf(buf, MAXPATHLEN, "/proc/%d/psinfo", (int)pid);
	if ((fd = open(buf, O_RDONLY)) == -1) {
		(void) fprintf(stderr, gettext("open failed : %s\n"),
		    strerror(errno));
		return (1);
	}

	if (read(fd, (void *)psinfo, sizeof (psinfo_t)) !=
	    (int)sizeof (psinfo_t)) {
		(void) fprintf(stderr, gettext("read failed : %s\n"),
		    strerror(errno));
		(void) close(fd);
		return (1);
	}

	(void) close(fd);

	return (0);
}


int
main()
{
	psinfo_t psinfo;
	pid_t pid;

	(void) setlocale(LC_ALL, "");
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);
	(void) textdomain(TEXT_DOMAIN);

	if ((pid = getpid()) == (pid_t)-1) {
		(void) fprintf(stderr, gettext("getpid failed : %s\n"),
		    strerror(errno));
		return (1);
	}

	if (read_psinfo(pid, &psinfo) != 0) {
		return (1);
	}

	print_scheduling_class(&psinfo);

	return (0);
}
