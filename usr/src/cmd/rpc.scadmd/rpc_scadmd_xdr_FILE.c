/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)rpc_scadmd_xdr_FILE.c	1.5	08/05/20 SMI"

/*
 * xdr_FILEP
 *
 *	On decode, read the wire and write to the FILE pointer.
 *	On encode, read from the FILE pointer and write to the wire.
 */

/*lint -e793 */
#include "rpc_scadmd.h"

#include <stdio.h>

#include <rpc/rpc.h>

bool_t
xdr_FILEP(register XDR *xdrs, FILE **fpp)
{
	char buffer[BUFSIZ];
	char *p;
	uint_t size = 0;

	/* Check arguments */
	if (xdrs == NULL)
		return (FALSE);

	/* if file pointer not set, just return TRUE */
	if (fpp == NULL || *fpp == NULL)
		return (TRUE);

	/* if XDR_FREE, close file */
	if (xdrs->x_op == XDR_FREE) {
		(void) fclose(*fpp);
		return (TRUE);
	}

	for (;;) {
		if (xdrs->x_op == XDR_ENCODE) {
			size = fread(buffer, 1, sizeof (buffer), *fpp);
			if (size == 0 && ferror(*fpp))
				return (FALSE);
		}
		p = buffer;
		if (!xdr_bytes(xdrs, &p, &size, sizeof (buffer)))
			return (FALSE);
		if (size == 0)
			break;
		if (xdrs->x_op == XDR_DECODE) {
			if (fwrite(buffer, 1, size, *fpp) != size)
				return (FALSE);
		}
	}

	return (TRUE);
}
