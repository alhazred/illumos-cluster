/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)rpc_scadmd_proc.c	1.50	08/05/27 SMI"

/*
 * rpc_scadmd_proc.c
 *
 *	Server procedures for rpc.scadmd(1M)
 */

/*lint -e793 */
#include "rpc_scadmd.h"
#include "rpc_scadmd_util.h"

#include <clcomm_cluster_vm.h>
#include <scadmin/scadmin.h>

#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <errno.h>
#include <libgen.h>

#include <sys/types.h>
#include <sys/stat.h>

/* new-cli headers */
#include <cl_errno.h>
#include <misc.h>

/* Permission-to-Boot token */
scconf_nodeid_t _boot_token = 0;		/* permission-to-boot token */
mutex_t _boot_token_lock;			/* lock for token */

/* ARGSUSED */
bool_t
scadmproc_add_node_1_svc(char *nodename, sc_result_addnode *result,
    struct svc_req *rqstp)
{
	scconf_cltr_handle_t handle = (scconf_cltr_handle_t)0;
	scconf_cfg_cluster_t *clconfig = (scconf_cfg_cluster_t *)0;
	scconf_cfg_node_t *cluster_node;
	scconf_cfg_cable_t *cltr_cable;
	scconf_errno_t rstatus = SCCONF_EUNEXPECTED;
	uint_t ismember;
	char *name;

	/* Make sure we are in the cluster */
	rstatus = scconf_ismember(0, &ismember);
	if (rstatus == SCCONF_NOERR && !ismember)
		rstatus = SCCONF_ENOCLUSTER;
	if (rstatus != SCCONF_NOERR)
		goto cleanup;

	/* Make sure busy node name is initialized to NULL */
	result->sc_res_busynode = NULL;

	/*
	 * xdr_string always sends NULL string, rather than NULL pointers.
	 * But, libscconf does not expect NULL strings.
	 */
	if (nodename && *nodename == '\0')
		nodename = NULL;
	if (nodename == NULL) {
		rstatus = SCCONF_EINVAL;
		goto cleanup;
	}

	/*
	 * First, check to see if installmode is enabled.  If it is,
	 * then we do not allow the addition of any more nodes until
	 * all configured nodes are also members of the cluster.  This
	 * is done to ensure proper CCR updating for new nodes joining
	 * the cluster for the first time.
	 *
	 * Then, if there is only one node in the cluster, we enable
	 * all existing cables at the time that this, the second node,
	 * is added.
	 */
	for (;;) {
		/* Free the old config, if necessary */
		if (clconfig != (scconf_cfg_cluster_t *)0)
			scconf_free_clusterconfig(clconfig);
		clconfig = (scconf_cfg_cluster_t *)0;

		/* Release the old handle, if necessary */
		if (handle != (scconf_cltr_handle_t)0)
			scconf_cltr_releasehandle(handle);
		handle = (scconf_cltr_handle_t)0;

		/* Get handle */
		rstatus = scconf_cltr_openhandle(&handle);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* Get a copy of the configuration */
		rstatus = scconf_get_clusterconfig(&clconfig);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* If installmode is enabled, make sure all nodes are members */
		if (clconfig->scconf_cluster_installmode != NULL &&
		    strcmp(clconfig->scconf_cluster_installmode,
		    "enabled") == 0) {
			for (cluster_node = clconfig->scconf_cluster_nodelist;
			    cluster_node != NULL;
			    cluster_node = cluster_node->scconf_node_next) {
				name = cluster_node->scconf_node_nodename;

				/* If this is us, just return EEXIST now */
				if (name && strcmp(nodename, name) == 0) {
					rstatus = SCCONF_EEXIST;
					goto cleanup;
				}

				/* See if it is a cluster member */
				rstatus = scconf_ismember(
				    cluster_node->scconf_node_nodeid,
				    &ismember);
				if (rstatus != SCCONF_NOERR)
					goto cleanup;

				/*
				 * If one of the nodes is found NOT
				 * to be a cluster member, return EBUSY,
				 * along with the name of the first node
				 * non-member node we find.
				 */
				if (!ismember) {
					rstatus = SCCONF_EBUSY;
					if (name) {
						result->sc_res_busynode =
						    strdup(name);
						if (result->sc_res_busynode ==
						    NULL)
							rstatus = SCCONF_ENOMEM;
						goto cleanup;
					}
				}
			}
		}

		/* If there is just one node, enable all of the cables */
		cluster_node = clconfig->scconf_cluster_nodelist;
		if (cluster_node && cluster_node->scconf_node_next == NULL) {
			for (cltr_cable = clconfig->scconf_cluster_cablelist;
			    cltr_cable;
			    cltr_cable = cltr_cable->scconf_cable_next) {
				rstatus = scconf_change_cltr_cable(handle,
				    &cltr_cable->scconf_cable_epoint1,
				    (scconf_cltr_epoint_t *)0,
				    SCCONF_STATE_ENABLED, NULL);
				if (rstatus != SCCONF_NOERR)
					goto cleanup;
			}
		}

		/* Add the node */
		rstatus = scconf_add_node(handle, nodename, NULL);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* update */
		rstatus = scconf_cltr_updatehandle(handle, NULL);

		/* if update return okay, we are done */
		if (rstatus == SCCONF_NOERR) {
			break;

		/* else if outdated handle, continue to retry */
		} else if (rstatus != SCCONF_ESTALE) {
			goto cleanup;
		}
	}

cleanup:
	/* Free the old config */
	if (clconfig != (scconf_cfg_cluster_t *)0)
		scconf_free_clusterconfig(clconfig);

	/* Release the old handle */
	if (handle != (scconf_cltr_handle_t)0)
		scconf_cltr_releasehandle(handle);

	/* Set the result code */
	result->sc_res_errno = (sc_errno_t)rstatus;

	return (TRUE);
}

/* ARGSUSED */
bool_t
scadmproc_add_cltr_adapter_1_svc(char *nodename, char *transport_type,
    char *adaptername, int vlanid, char *properties, sc_result *result,
    struct svc_req *rqstp)
{
	scconf_errno_t rstatus;
	uint_t ismember;
	uint_t noenable = 1;

	/* Make sure we are in the cluster */
	rstatus = scconf_ismember(0, &ismember);
	if (rstatus == SCCONF_NOERR && !ismember)
		rstatus = SCCONF_ENOCLUSTER;
	if (rstatus != SCCONF_NOERR) {
		result->sc_res_errno = (sc_errno_t)rstatus;
		return (TRUE);
	}

	/*
	 * xdr_string always sends NULL string, rather than NULL pointers.
	 * But, libscconf does not expect NULL strings.
	 */
	if (nodename && *nodename == '\0')
		nodename = NULL;
	if (transport_type && *transport_type == '\0')
		transport_type = NULL;
	if (adaptername && *adaptername == '\0')
		adaptername = NULL;
	if (properties && *properties == '\0')
		properties = NULL;

	/* Add cluster transport adapter */
	result->sc_res_errno = (sc_errno_t)scconf_add_cltr_adapter(NULL,
	    noenable, nodename, transport_type, adaptername, vlanid,
	    properties, NULL);

	return (TRUE);
}

/* ARGSUSED */
bool_t
scadmproc_add_cltr_cpoint_1_svc(char *cpoint_type, char *cpointname,
    char *properties, sc_result *result, struct svc_req *rqstp)
{
	scconf_errno_t rstatus;
	uint_t ismember;
	uint_t noenable = 1;

	/* Make sure we are in the cluster */
	rstatus = scconf_ismember(0, &ismember);
	if (rstatus == SCCONF_NOERR && !ismember)
		rstatus = SCCONF_ENOCLUSTER;
	if (rstatus != SCCONF_NOERR) {
		result->sc_res_errno = (sc_errno_t)rstatus;
		return (TRUE);
	}

	/*
	 * xdr_string always sends NULL string, rather than NULL pointers.
	 * But, libscconf does not expect NULL strings.
	 */
	if (cpoint_type && *cpoint_type == '\0')
		cpoint_type = NULL;
	if (cpointname && *cpointname == '\0')
		cpointname = NULL;
	if (properties && *properties == '\0')
		properties = NULL;

	/* Add off-node cluster transport connection point */
	result->sc_res_errno = (sc_errno_t)scconf_add_cltr_cpoint(NULL,
	    noenable, cpoint_type, cpointname, properties, NULL);

	return (TRUE);
}

/* ARGSUSED */
bool_t
scadmproc_add_cltr_cable_1_svc(sc_cltr_epoint_t *epoint1,
    sc_cltr_epoint_t *epoint2, sc_result *result, struct svc_req *rqstp)
{
	scconf_errno_t rstatus;
	uint_t noenable = 0;
	sc_cltr_epoint_t *sc_e;
	scconf_cltr_epoint_t e1, e2;
	scconf_cltr_epoint_t *e;
	uint_t ismember;
	int i;

	/* Check arguments */
	if (epoint1 == NULL || epoint2 == NULL) {
		result->sc_res_errno = (sc_errno_t)SCCONF_EUNEXPECTED;
		return (TRUE);
	}

	/* Make sure we are in the cluster */
	rstatus = scconf_ismember(0, &ismember);
	if (rstatus == SCCONF_NOERR && !ismember)
		rstatus = SCCONF_ENOCLUSTER;
	if (rstatus != SCCONF_NOERR) {
		result->sc_res_errno = (sc_errno_t)rstatus;
		return (TRUE);
	}

	/*
	 * xdr_string always sends NULL string, rather than NULL pointers.
	 * But, libscconf does not expect NULL strings.
	 */
	sc_e = epoint1;
	e = &e1;
	for (i = 0;  i < 2;  ++i) {

		/*
		 * copy values, changing pointers to NULL as needed
		 */

		/* type */
		e->scconf_cltr_epoint_type = sc_e->sc_epoint_type;

		/* nodename */
		if (sc_e->sc_epoint_nodename && *sc_e->sc_epoint_nodename)
			e->scconf_cltr_epoint_nodename =
			    sc_e->sc_epoint_nodename;
		else
			e->scconf_cltr_epoint_nodename = NULL;

		/* devicename */
		if (sc_e->sc_epoint_devicename && *sc_e->sc_epoint_devicename)
			e->scconf_cltr_epoint_devicename =
			    sc_e->sc_epoint_devicename;
		else
			e->scconf_cltr_epoint_devicename = NULL;

		/* portname */
		if (sc_e->sc_epoint_portname && *sc_e->sc_epoint_portname)
			e->scconf_cltr_epoint_portname =
			    sc_e->sc_epoint_portname;
		else
			e->scconf_cltr_epoint_portname = NULL;

		/* port id and state */
		e->scconf_cltr_epoint_portid =
			(scconf_cltr_portid_t)sc_e->sc_epoint_portid;
		e->scconf_cltr_epoint_portstate = sc_e->sc_epoint_portstate;

		/* Next */
		sc_e = epoint2;
		e = &e2;
	}

	/* Add cluster transport cable */
	result->sc_res_errno = (sc_errno_t)scconf_add_cltr_cable(NULL,
	    noenable, &e1, &e2, NULL);

	return (TRUE);
}

/* ARGSUSED */
bool_t
scadmproc_get_authtype_1_svc(sc_result_authtype *result, struct svc_req *rqstp)
{
	scconf_errno_t rstatus;
	uint_t ismember;

	/* Make sure we are in the cluster */
	rstatus = scconf_ismember(0, &ismember);
	if (rstatus == SCCONF_NOERR && !ismember)
		rstatus = SCCONF_ENOCLUSTER;
	if (rstatus != SCCONF_NOERR) {
		result->sc_res_errno = (sc_errno_t)rstatus;
		return (TRUE);
	}

	result->sc_res_errno = scconf_get_secure_authtype(
	    (scconf_authtype_t *)&result->sc_res_authtype);

	return (TRUE);
}

/* ARGSUSED */
bool_t
scadmproc_get_ismember_1_svc(sc_result_ismember *result, struct svc_req *rqstp)
{
	result->sc_res_ismember = 0;

	result->sc_res_errno = (sc_errno_t)scconf_ismember(0,
	    &result->sc_res_ismember);

	return (TRUE);
}

/* ARGSUSED */
bool_t
scadmproc_get_clustername_1_svc(sc_result_cname *result, struct svc_req *rqstp)
{
	scconf_errno_t rstatus;
	scconf_cfg_cluster_t *clconfig;
	uint_t ismember;

	bzero(result, sizeof (sc_result_cname));

	/* Make sure we are in the cluster */
	rstatus = scconf_ismember(0, &ismember);
	if (rstatus == SCCONF_NOERR && !ismember)
		rstatus = SCCONF_ENOCLUSTER;
	if (rstatus != SCCONF_NOERR) {
		result->sc_res_errno = (sc_errno_t)rstatus;
		return (TRUE);
	}

	/* Make sure name is initialized to NULL */
	result->sc_res_cname = NULL;

	/* Get a copy of the configuration */
	rstatus = scconf_get_clusterconfig(&clconfig);
	if (rstatus != SCCONF_NOERR) {
		result->sc_res_errno = (sc_errno_t)rstatus;
		return (TRUE);
	}

	/* If the cluster name is not set, return ENOEXIST */
	if (clconfig->scconf_cluster_clustername == NULL) {
		scconf_free_clusterconfig(clconfig);
		result->sc_res_errno = (sc_errno_t)SCCONF_ENOEXIST;
		return (TRUE);
	}

	/* Make a copy of the name */
	result->sc_res_cname = strdup(clconfig->scconf_cluster_clustername);
	if (result->sc_res_cname == NULL) {
		scconf_free_clusterconfig(clconfig);
		result->sc_res_errno = (sc_errno_t)SCCONF_ENOMEM;
		return (TRUE);
	}

	/* Done with the cluster config */
	scconf_free_clusterconfig(clconfig);

	return (TRUE);
}

/* ARGSUSED */
bool_t
scadmproc_get_cluster_vm_info_1_svc(sc_result_cluster_vm *result,
    struct svc_req *rqstp)
{
	scconf_errno_t rstatus;

	bzero(result, sizeof (sc_result_cluster_vm));

	/* get info from CMM and VM */
	rstatus = clcomm_get_cluster_vm_info(result);

	result->sc_res_errno = (sc_errno_t)rstatus;
	return (TRUE);
}

/* ARGSUSED */
bool_t
scadmproc_copy_from_1_svc(char *srcfile, sc_result_file *result,
    struct svc_req *rqstp)
{
	scconf_errno_t rstatus;
	struct stat sbuf;
	char *ptr, *buffer;
	uint_t ismember, i;
	boolean_t match_flag = B_FALSE;
	char *cpfilelist[] = {
	    "/etc/cluster/ccr/global/infrastructure",
	    "/etc/cluster/ccr/global/postconfig",
	    "/etc/cacao/instances/default/security/password",
	    "/etc/cacao/instances/default/security/jsse/agent.cert",
	    "/etc/cacao/instances/default/security/jsse/keystore",
	    "/etc/cacao/instances/default/security/jsse/truststore",
	    "/etc/cacao/instances/default/security/nss/localca/secmod.db",
	    "/etc/cacao/instances/default/security/nss/localca/cert8.db",
	    "/etc/cacao/instances/default/security/nss/localca/key3.db",
	    "/etc/cacao/instances/default/security/nss/localca/localca.cert",
	    "/etc/cacao/instances/default/security/nss/unknown/secmod.db",
	    "/etc/cacao/instances/default/security/nss/unknown/cert8.db",
	    "/etc/cacao/instances/default/security/nss/unknown/key3.db",
	    "/etc/cacao/instances/default/security/nss/wellknown/secmod.db",
	    "/etc/cacao/instances/default/security/nss/wellknown/cert8.db",
	    "/etc/cacao/instances/default/security/nss/wellknown/key3.db",
	    "/etc/cacao/instances/default/security/nss/wellknown/"
		"wellknown.cert",
	    "/opt/cluster/lib/ds/history/tmp_apachectl",
	    "/opt/cluster/lib/ds/history/tmp_docroot.tar",
	    "/opt/cluster/lib/ds/history/tmp_httpd.conf",
	    0 };


	bzero(result, sizeof (sc_result_file));

	/* Check arguments */
	if (srcfile == NULL || *srcfile == '\0') {
		result->sc_res_errno = (sc_errno_t)SCCONF_EINVAL;
		return (TRUE);
	}

	/* Make sure we are in the cluster */
	rstatus = scconf_ismember(0, &ismember);
	if (rstatus == SCCONF_NOERR && !ismember)
		rstatus = SCCONF_ENOCLUSTER;
	if (rstatus != SCCONF_NOERR) {
		result->sc_res_errno = (sc_errno_t)rstatus;
		return (TRUE);
	}

	/* Initialize the fp */
	result->sc_res_fp = (FILE *)0;

	/* Filename must begin w/ / */
	if (*srcfile != '/') {
		result->sc_res_errno = (sc_errno_t)SCCONF_EINVAL;
		return (TRUE);
	}

	/*
	 * Compare the filename given by user to
	 * the list of legal files to copy.
	 */
	for (i = 0; cpfilelist[i] != NULL; i++) {
		if (strcmp(srcfile, cpfilelist[i]) == 0) {
			match_flag = B_TRUE;
		}
	}
	if (match_flag == B_FALSE) {
		result->sc_res_errno = (sc_errno_t)SCCONF_EINVAL;
		return (TRUE);
	}

	/* Make sure the source file exists */
	if (stat(srcfile, &sbuf) < 0) {
		switch (errno) {	/*lint !e746 */
		case ENOENT:
		case ENOTDIR:
			result->sc_res_errno = (sc_errno_t)SCCONF_ENOEXIST;
			return (TRUE);

		default:
			result->sc_res_errno = (sc_errno_t)SCCONF_EINVAL;
			return (TRUE);
		}
	}

	/* regular file? */
	if (!S_ISREG(sbuf.st_mode)) {
		result->sc_res_errno = (sc_errno_t)SCCONF_EINVAL;
		return (TRUE);
	}

	/* check directory read/search permissions */
	if ((buffer = strdup(srcfile)) == NULL) {
		result->sc_res_errno = (sc_errno_t)SCCONF_ENOMEM;
		return (TRUE);
	}
	while ((ptr = strrchr(buffer, '/')) != NULL) {

		/* get the root directory, too */
		if (ptr == buffer)
			ptr[1] = '\0';
		else
			ptr[0] = '\0';

		/* stat the directory */
		if (stat(buffer, &sbuf) < 0) {
			result->sc_res_errno = (sc_errno_t)SCCONF_EUNEXPECTED;
			free(buffer);
			return (TRUE);
		}

		/* if at root, we are done */
		if (ptr == buffer)
			break;
	}
	free(buffer);

	/* open the file pointer - closed by xdr_FILEP on XDR_FREE opt */
	if ((result->sc_res_fp = fopen(srcfile, "r")) == NULL) {
		result->sc_res_errno = (sc_errno_t)SCCONF_EINVAL;
		return (TRUE);
	}

	return (TRUE);
}

/* ARGSUSED */
bool_t
scadmproc_remove_file_1_svc(char *filename, char *nodename,
    sc_result *result, struct svc_req *rqstp)
{
	scconf_errno_t rstatus;
	struct stat sbuf;
	uint_t ismember;
	uint_t i;
	boolean_t match_flag = B_FALSE;
	char *rmfilelist[] = { 0, 0 };

	/* Check arguments */
	if (filename == NULL || *filename == '\0') {
		result->sc_res_errno = (sc_errno_t)SCCONF_EINVAL;
		return (TRUE);
	}

	/* Make sure we are in the cluster */
	rstatus = scconf_ismember(0, &ismember);
	if (rstatus == SCCONF_NOERR && !ismember)
		rstatus = SCCONF_ENOCLUSTER;
	if (rstatus != SCCONF_NOERR) {
		result->sc_res_errno = (sc_errno_t)rstatus;
		return (TRUE);
	}

	/* Filename must begin w/ / */
	if (*filename != '/') {
		result->sc_res_errno = (sc_errno_t)SCCONF_EINVAL;
		return (TRUE);
	}

	/*
	 * Compare the filename given by user to
	 * the list of legal file to remove.
	 */
	for (i = 0; rmfilelist[i] != NULL; i++) {
		if (strcmp(filename, rmfilelist[i]) == 0) {
			match_flag = B_TRUE;
		}
	}
	if (match_flag == B_FALSE) {
		result->sc_res_errno = (sc_errno_t)SCCONF_EINVAL;
		return (TRUE);
	}

	/* Make sure the file exists */
	if (stat(filename, &sbuf) != 0) {
		result->sc_res_errno = (sc_errno_t)SCCONF_ENOEXIST;
		return (TRUE);
	}

	/* regular file? */
	if (!S_ISREG(sbuf.st_mode)) {
		result->sc_res_errno = (sc_errno_t)SCCONF_EINVAL;
		return (TRUE);
	}

	if (remove(filename) < 0) {
		switch (errno) {
		case EACCES:
			result->sc_res_errno = (sc_errno_t)SCCONF_EPERM;
			return (TRUE);

		case ENOENT:
		case ENOTDIR:
			result->sc_res_errno = (sc_errno_t)SCCONF_EEXIST;
			return (TRUE);

		default:
			result->sc_res_errno = (sc_errno_t)SCCONF_EINVAL;
			return (TRUE);
		}
	}

	result->sc_res_errno = (sc_errno_t)SCCONF_NOERR;
	return (TRUE);
}

/* ARGSUSED */
bool_t
scadmproc_add_ds_1_svc(char *dstype, char *dsname,
    sc_dsnodes_t dsnodes_xdrarray,
    sc_state_t dspreference, sc_gdev_range_t dsdevices_xdrarray,
    sc_state_t dsfailback, sc_cfg_prop_t dspropertylist_xdrarray,
    char *dsoptions, unsigned int dsnumsecondaries, sc_result_ds *result,
    struct svc_req *request)
{
	scconf_gdev_range_t *gdev_range_list = NULL;
	scconf_cfg_prop_t *properties_list = NULL;
	sc_errno_t rstatus;
	char **dsnode_array = NULL;
	char *msgbuffer = (char *)0;

	bzero(result, sizeof (sc_result_ds));
	result->sc_res_errmsg = NULL;

	/*  fix arrays */
	rstatus = scadm_gdevrange_array_to_llist(&dsdevices_xdrarray,
	    &gdev_range_list);
	if (rstatus != SCCONF_NOERR)
		goto cleanup;

	rstatus = scadm_props_array_to_llist(&dspropertylist_xdrarray,
	    &properties_list);
	if (rstatus != SCCONF_NOERR)
		goto cleanup;

	rstatus = scadmd_nodelist_xdr_array_to_c_array(dsnodes_xdrarray,
	    &dsnode_array);

	if (rstatus != SCCONF_NOERR)
		goto cleanup;

	/*  Now call the local function */
	rstatus = scconf_add_ds_local(dstype, dsname,
	    dsnode_array, dspreference, gdev_range_list,
	    dsfailback,	properties_list, dsoptions, dsnumsecondaries,
	    &msgbuffer);

cleanup:
	/*  free locally allocated memory */
	scadmd_free_gdevrange(gdev_range_list);
	scadmd_free_propertylist(properties_list);
	free(dsnode_array);

	/* populate the results structure */
	result->sc_res_errno = rstatus;
	if (msgbuffer) {
		result->sc_res_errmsg = strdup(msgbuffer);
		if (result->sc_res_errmsg == NULL) {
			result->sc_res_errno = SCCONF_ENOMEM;
		}
		free(msgbuffer);
	}

	/* return */
	return (TRUE);
}

/* ARGSUSED */
bool_t
scadmproc_change_ds_1_svc(char *dsname, sc_dsnodes_t dsnodes_xdrarray,
    sc_state_t dspreference, sc_gdev_range_t dsdevices_xdrarray,
    sc_state_t dsfailback, sc_cfg_prop_t dspropertylist_xdrarray,
    char *dsoptions, unsigned int dsnumsecondaries, sc_result_ds *result,
    struct svc_req *request)
{
	scconf_gdev_range_t *gdev_range_list = NULL;
	scconf_cfg_prop_t *properties_list = NULL;
	sc_errno_t rstatus;
	char **dsnode_array = NULL;
	char *msgbuffer = (char *)0;

	bzero(result, sizeof (sc_result_ds));
	result->sc_res_errmsg = NULL;

	/*  fix arrays */
	rstatus = scadm_gdevrange_array_to_llist(&dsdevices_xdrarray,
	    &gdev_range_list);
	if (rstatus != SCCONF_NOERR)
		goto cleanup;

	rstatus = scadm_props_array_to_llist(&dspropertylist_xdrarray,
	    &properties_list);
	if (rstatus != SCCONF_NOERR)
		goto cleanup;

	rstatus = scadmd_nodelist_xdr_array_to_c_array(dsnodes_xdrarray,
	    &dsnode_array);

	if (rstatus != SCCONF_NOERR)
		goto cleanup;

	/*  Now call the local function */
	rstatus = scconf_change_ds_local(dsname, dsnode_array,
	    dspreference, gdev_range_list, dsfailback, properties_list,
	    dsoptions, dsnumsecondaries, &msgbuffer);

cleanup:
	/*  free locally allocated memory */
	scadmd_free_gdevrange(gdev_range_list);
	scadmd_free_propertylist(properties_list);
	free(dsnode_array);

	/* populate the results structure */
	result->sc_res_errno = rstatus;
	if (msgbuffer) {
		result->sc_res_errmsg = strdup(msgbuffer);
		if (result->sc_res_errmsg == NULL) {
			result->sc_res_errno = SCCONF_ENOMEM;
		}
		free(msgbuffer);
	}

	/* return */
	return (TRUE);
}

/* ARGSUSED */
bool_t
scadmproc_get_boottoken_1_svc(sc_nodeid_t nodeid, sc_result *result,
    struct svc_req *rqstp)
{
	scconf_errno_t rstatus;
	uint_t ismember;
	uint_t isfullmembership;

	/* Initialize - deny permission to boot */
	result->sc_res_errno = (sc_errno_t)SCCONF_EUNEXPECTED;

	/* Make sure we are in the cluster */
	rstatus = scconf_ismember(0, &ismember);
	if (rstatus == SCCONF_NOERR && !ismember)
		rstatus = SCCONF_ENOCLUSTER;
	if (rstatus != SCCONF_NOERR) {
		result->sc_res_errno = (sc_errno_t)rstatus;
		return (TRUE);
	}

	/* Grab the lock on the boot token */
	if (mutex_lock(&_boot_token_lock) != 0) {
		result->sc_res_errno = (sc_errno_t)SCCONF_EUNEXPECTED;
		return (TRUE);
	}

	/* Check to see if the token is available */
	if (_boot_token != 0) {
		result->sc_res_errno = (sc_errno_t)SCCONF_EBUSY;
		(void) mutex_unlock(&_boot_token_lock);
		return (TRUE);
	}

	/* Make sure all configured nodes are cluster members */
	isfullmembership = 0;
	rstatus = scconf_isfullmembership(0, &isfullmembership);
	if (rstatus != SCCONF_NOERR) {
		result->sc_res_errno = (sc_errno_t)rstatus;
		(void) mutex_unlock(&_boot_token_lock);
		return (TRUE);
	}
	if (!isfullmembership) {
		result->sc_res_errno = (sc_errno_t)SCCONF_EBUSY;
		(void) mutex_unlock(&_boot_token_lock);
		return (TRUE);
	}

	/* Start the timeout thread on the lock */
	if (thr_create(NULL, 0, boot_token_timeout, NULL, 0, NULL) != 0) {
		result->sc_res_errno = (sc_errno_t)SCCONF_EUNEXPECTED;
		(void) mutex_unlock(&_boot_token_lock);
		return (TRUE);
	}

	/* Grab the boot token and give up the lock */
	_boot_token = nodeid;
	(void) mutex_unlock(&_boot_token_lock);

	/* Give the caller permission to boot */
	result->sc_res_errno = (sc_errno_t)SCCONF_NOERR;

	return (TRUE);
}

/* ARGSUSED */
bool_t
scadmproc_get_network_adapters_1_svc(uint_t flag, char *adaptypes,
    sc_result_adapters *result, struct svc_req *rqstp)
{
	scconf_errno_t rstatus;
	scadmin_namelist_t *namelist;
	uint_t ismember;
	uint_t count;
	char **array;

	/* Initialize result */
	result->sc_res_adapters.sc_namelist_t_len = 0;
	result->sc_res_adapters.sc_namelist_t_val = NULL;

	/* Make sure we are in the cluster */
	rstatus = (int)scconf_ismember(0, &ismember);
	if (rstatus || !ismember)
		return (TRUE);

	/* Get the adapters */
	namelist = scadmin_get_network_adapters(flag, adaptypes);
	if (namelist) {
		count = scadm_namelist_to_array(namelist, &array);
		scadmin_free_namelist(namelist);
		if (count) {
			result->sc_res_adapters.sc_namelist_t_len = count;
			result->sc_res_adapters.sc_namelist_t_val = array;
		}
	}

	return (TRUE);
}

/* ARGSUSED */
bool_t
scadmproc_transport_config_1_svc(sc_result_trconfig *result,
    struct svc_req *rqstp)
{
	scconf_errno_t rstatus;
	scconf_cfg_cluster_t *clconfig;
	scconf_cfg_node_t *cluster_nodes;
	scconf_cfg_node_t *cluster_node;
	scconf_cfg_cltr_adap_t *cltr_adapters;
	scconf_cfg_cltr_adap_t *cltr_adapter;
	scconf_cfg_cable_t *cluster_cables;
	scconf_cfg_cable_t *cluster_cable;
	scconf_cltr_epoint_t *e1, *e2;
	uint_t ismember;
	char *switchname;
	scadmin_namelist_t *namelist = (scadmin_namelist_t *)0;
	char buffer[MAXHOSTNAMELEN + 1024];
	uint_t count;
	char **array;
	scconf_cfg_prop_t *prop;
	int vlan_prop_found;
	int devname_prop_found;
	int devins_prop_found;
	char svlanid[VLAN_STRMAX];
	char devname[SCCONF_MAXSTRINGLEN];
	char devins[DEVINS_STRMAX];

	/* Initialize result */
	result->sc_res_errno = (sc_errno_t)SCCONF_NOERR;
	result->sc_res_trconfig.sc_namelist_t_len = 0;
	result->sc_res_trconfig.sc_namelist_t_val = NULL;

	/* Make sure we are in the cluster */
	rstatus = scconf_ismember(0, &ismember);
	if (rstatus == SCCONF_NOERR && !ismember)
		rstatus = SCCONF_ENOCLUSTER;
	if (rstatus != SCCONF_NOERR) {
		result->sc_res_errno = (sc_errno_t)rstatus;
		return (TRUE);
	}

	/* Get a copy of the configuration */
	rstatus = scconf_get_clusterconfig(&clconfig);
	if (rstatus != SCCONF_NOERR) {
		result->sc_res_errno = (sc_errno_t)rstatus;
		return (TRUE);
	}

	/*
	 * Each config line has the following format:
	 *
	 *	<nodename> <trtype> <adaptername> <vlanid> <switchname>|-
	 */
	cluster_nodes = clconfig->scconf_cluster_nodelist;
	for (cluster_node = cluster_nodes;  cluster_node;
	    cluster_node = cluster_node->scconf_node_next) {
		if (!cluster_node->scconf_node_nodename)
			continue;
		cltr_adapters = cluster_node->scconf_node_adapterlist;
		for (cltr_adapter = cltr_adapters;  cltr_adapter;
		    cltr_adapter = cltr_adapter->scconf_adap_next) {
			if (!cltr_adapter->scconf_adap_adaptername ||
			    !cltr_adapter->scconf_adap_cltrtype)
				continue;
			cluster_cables = clconfig->scconf_cluster_cablelist;
			switchname = NULL;
			for (cluster_cable = cluster_cables;  cluster_cable;
			    cluster_cable = cluster_cable->scconf_cable_next) {
				e1 = &cluster_cable->scconf_cable_epoint1;
				e2 = &cluster_cable->scconf_cable_epoint2;
				if (!e1 || !e2 ||
				    !e1->scconf_cltr_epoint_devicename ||
				    !e2->scconf_cltr_epoint_devicename)
					continue;
				if (e1->scconf_cltr_epoint_type ==
				    SCCONF_CLTR_EPOINT_TYPE_ADAPTER &&
				    e2->scconf_cltr_epoint_type ==
				    SCCONF_CLTR_EPOINT_TYPE_CPOINT &&
				    e1->scconf_cltr_epoint_nodename &&
				    strcmp(e1->scconf_cltr_epoint_nodename,
				    cluster_node->scconf_node_nodename) == 0 &&
				    strcmp(e1->scconf_cltr_epoint_devicename,
				    cltr_adapter->scconf_adap_adaptername)
				    == 0) {
					switchname =
					    e2->scconf_cltr_epoint_devicename;
					break;
				}
				if (e2->scconf_cltr_epoint_type ==
				    SCCONF_CLTR_EPOINT_TYPE_ADAPTER &&
				    e1->scconf_cltr_epoint_type ==
				    SCCONF_CLTR_EPOINT_TYPE_CPOINT &&
				    e2->scconf_cltr_epoint_nodename &&
				    strcmp(e2->scconf_cltr_epoint_nodename,
				    cluster_node->scconf_node_nodename) == 0 &&
				    strcmp(e2->scconf_cltr_epoint_devicename,
				    cltr_adapter->scconf_adap_adaptername)
				    == 0) {
					switchname =
					    e1->scconf_cltr_epoint_devicename;
					break;
				}
			}

			if (!switchname)
				switchname = "-";

			vlan_prop_found = 0;
			devname_prop_found = 0;
			devins_prop_found = 0;
			for (prop = cltr_adapter->scconf_adap_propertylist;
			    prop; prop = prop->scconf_prop_next) {
				if (strcmp(prop->scconf_prop_key, "vlan_id")
				    == 0) {
					vlan_prop_found = 1;
					(void) strcpy(svlanid,
					    prop->scconf_prop_value);
				}
				if (strcmp(prop->scconf_prop_key, "device_name")
				    == 0) {
					devname_prop_found = 1;
					(void) strcpy(devname,
					    prop->scconf_prop_value);
				}
				if (strcmp(prop->scconf_prop_key,
				    "device_instance") == 0) {
					devins_prop_found = 1;
					(void) strcpy(devins,
					    prop->scconf_prop_value);
				}
				if (vlan_prop_found && devname_prop_found &&
				    devins_prop_found)
					break;
			}

			if (!devname_prop_found || !devins_prop_found)
				continue;

			if (vlan_prop_found == 0)
				(void) sprintf(buffer, "%s %s %s%s %d %s",
				    cluster_node->scconf_node_nodename,
				    cltr_adapter->scconf_adap_cltrtype,
				    devname, devins, 0, switchname);
			else
				(void) sprintf(buffer, "%s %s %s%s %s %s",
				    cluster_node->scconf_node_nodename,
				    cltr_adapter->scconf_adap_cltrtype,
				    devname, devins, svlanid, switchname);

			/* Add the line to our namelist */
			if (scadmin_append_namelist(&namelist, buffer)) {
				scconf_free_clusterconfig(clconfig);
				scadmin_free_namelist(namelist);
				result->sc_res_errno =
				    (sc_errno_t)SCCONF_ENOMEM;
				return (TRUE);
			}
		}
	}

	/* Done with the cluster config */
	scconf_free_clusterconfig(clconfig);

	/* Convert namelist to array */
	if (namelist) {
		count = scadm_namelist_to_array(namelist, &array);
		scadmin_free_namelist(namelist);
		if (count) {
			result->sc_res_trconfig.sc_namelist_t_len = count;
			result->sc_res_trconfig.sc_namelist_t_val = array;
		}
	}

	return (TRUE);
}

/* ARGSUSED */
bool_t
scadmproc_broadcast_ping_1_svc(void *result, struct svc_req *rqstp)
{
	scconf_errno_t rstatus;
	uint_t ismember;

	/* Make sure we are in the cluster */
	rstatus = (int)scconf_ismember(0, &ismember);
	if (rstatus || !ismember)
		return (TRUE);

	/* Broadcast ping */
	scadmin_broadcast_ping();

	return (TRUE);
}

/* ARGSUSED */
bool_t
scadmproc_snoop_1_svc(char *adapters, uint_t discover_options, uint_t recvto,
    sc_result_snoop *result, struct svc_req *rqstp)
{
	int rstatus;
	uint_t ismember;
	scadmin_namelist_t *discovered = (scadmin_namelist_t *)0;
	scadmin_namelist_t *errs = (scadmin_namelist_t *)0;
	uint_t count;
	char **arrayd;
	char **arraye;

	/* Initialize result */
	result->sc_res_result = 0;
	result->sc_res_discovered.sc_namelist_t_len = 0;
	result->sc_res_discovered.sc_namelist_t_val = NULL;
	result->sc_res_errlist.sc_namelist_t_len = 0;
	result->sc_res_errlist.sc_namelist_t_val = NULL;

	/* Make sure we are in the cluster */
	rstatus = (int)scconf_ismember(0, &ismember);
	if (rstatus || !ismember) {
		result->sc_res_result = 1;
		return (TRUE);
	}

	/* Snoop */
	rstatus = (int)scadmin_snoop(adapters, &discovered, discover_options,
	    recvto, &errs);
	if (rstatus)
		result->sc_res_result = rstatus;

	/* Discovered namelist - convert to array */
	if (discovered) {
		count = scadm_namelist_to_array(discovered, &arrayd);
		scadmin_free_namelist(discovered);
		if (count) {
			result->sc_res_discovered.sc_namelist_t_len = count;
			result->sc_res_discovered.sc_namelist_t_val = arrayd;
		}
	}

	/* Error list - convert to array */
	if (errs) {
		count = scadm_namelist_to_array(errs, &arraye);
		scadmin_free_namelist(errs);
		if (count) {
			result->sc_res_errlist.sc_namelist_t_len = count;
			result->sc_res_errlist.sc_namelist_t_val = arraye;
		}
	}

	return (TRUE);
}

/* ARGSUSED */
bool_t
scadmproc_autodiscover_1_svc(char *adapters, char *vlans, char *filename,
    uint_t discover_options, uint_t sendcount, uint_t recvto,
    uint_t waitcount, char *token, sc_result_autodiscover *result,
    struct svc_req *rqstp)
{
	int rstatus;
	uint_t ismember;
	scadmin_namelist_t *discovered = (scadmin_namelist_t *)0;
	scadmin_namelist_t *errs = (scadmin_namelist_t *)0;
	uint_t count;
	char **arrayd;
	char **arraye;

	/* Initialize result */
	result->sc_res_result = 0;
	result->sc_res_discovered.sc_namelist_t_len = 0;
	result->sc_res_discovered.sc_namelist_t_val = NULL;
	result->sc_res_errlist.sc_namelist_t_len = 0;
	result->sc_res_errlist.sc_namelist_t_val = NULL;

	/* Initialize args */
	if (filename && *filename == '\0')
		filename = NULL;
	if (token && *token == '\0')
		token = NULL;

	/* Make sure we are in the cluster */
	rstatus = (int)scconf_ismember(0, &ismember);
	if (rstatus || !ismember) {
		result->sc_res_result = 1;
		return (TRUE);
	}

	/* Discover */
	rstatus = (int)scadmin_autodiscover(adapters, vlans, filename,
	    &discovered, discover_options, sendcount, recvto, waitcount,
	    token, &errs);
	if (rstatus)
		result->sc_res_result = rstatus;

	/* Discovered namelist - convert to array */
	if (discovered) {
		count = scadm_namelist_to_array(discovered, &arrayd);
		scadmin_free_namelist(discovered);
		if (count) {
			result->sc_res_discovered.sc_namelist_t_len = count;
			result->sc_res_discovered.sc_namelist_t_val = arrayd;
		}
	}

	/* Error list - convert to array */
	if (errs) {
		count = scadm_namelist_to_array(errs, &arraye);
		scadmin_free_namelist(errs);
		if (count) {
			result->sc_res_errlist.sc_namelist_t_len = count;
			result->sc_res_errlist.sc_namelist_t_val = arraye;
		}
	}

	return (TRUE);
}

/* ARGSUSED */
bool_t
scadmproc_remove_node_1_svc(char *node_to_be_removed, uint_t force_flag,
	sc_result_removenode *result, struct svc_req *rqstp)
{
	/*
	 * In order to remove a node successfully, all transport cables and
	 * endpoints must be removed first.  Then all autogenerated rawdisk
	 * device services (e.g. dsk/dX) need to have the node removed from
	 * its nodelist if the node is connected.  Any autogenerated rawdisk
	 * device service that has an empty nodelist will be automatically
	 * removed.  Finally, the node is removed from the cluster.
	 * If the force_flag is set (non-zero), then the node is also removed
	 * from the nodelist of all the RGs and the Installed_nodes property
	 * of all the RTs. If the force_flag is not set (zero) and if the
	 * node is present in the nodelist of any of the RGs or is present
	 * in the Installed_nodes property of any of the RTs then there
	 * will be an error and the node will not be removed.
	 */
	int rstatus = SCCONF_NOERR;
	uint_t ismember;
	char *arraye = (char *)0;
	char **errbuf = (char **)0;
	clerrno_t clerr = CL_NOERR;

	/*
	 * xdr_string always sends NULL string, rather than NULL pointers.
	 * But, libscconf does not expect NULL strings.
	 */
	if (node_to_be_removed && *node_to_be_removed == '\0') {
		node_to_be_removed = NULL;
		result->sc_res_errno = SCCONF_EINVAL;
		return (TRUE);
	}

	/* Initialize result */
	result->sc_res_errno = 0;
	result->sc_res_errlist.sc_namelist_t_len = 0;
	result->sc_res_errlist.sc_namelist_t_val = NULL;

	/* Make sure we are in the cluster */
	rstatus = (int)scconf_ismember(0, &ismember);
	if (rstatus || !ismember) {
		result->sc_res_errno = SCCONF_ENOCLUSTER;
		return (TRUE);
	}

	/* Remove the node from the cluster */
	clerr = clnode_clear_node(node_to_be_removed, force_flag, &arraye);
	/* Check for any errors and pass them back */
	if (arraye != NULL) {
		/* Allocate memory for the error buffer */
		errbuf = malloc(sizeof (char *));
		if (errbuf == NULL) {
			result->sc_res_errno = SCCONF_ENOMEM;
			return (TRUE);
		}
		*errbuf = arraye;
		result->sc_res_errlist.sc_namelist_t_len = 1;
		result->sc_res_errlist.sc_namelist_t_val = errbuf;
	}

	/* Set the result code */
	result->sc_res_errno = (sc_errno_t)rstatus;
	result->sc_res_errno = (sc_errno_t)clerr;

	return (TRUE);
}

/* ARGSUSED */
bool_t
scadmproc_get_major_number_1_svc(char *driver, sc_result_major_number *result,
	struct svc_req *rqstp)
{
	int rstatus = SCCONF_NOERR;
	uint_t ismember;
	scadmin_namelist_t *namelist = (scadmin_namelist_t *)0;
	char **array = (char **)0;
	uint_t count;

	/*
	 * xdr_string always sends NULL string, rather than NULL pointers.
	 * But, libscconf does not expect NULL strings.
	 */
	if (driver && *driver == '\0') {
		driver = NULL;
		result->sc_res_errno = SCCONF_EINVAL;
		return (TRUE);
	}

	/* Initialize result */
	result->sc_res_errno = 0;
	result->sc_res_major_numbers.sc_namelist_t_len = 0;
	result->sc_res_major_numbers.sc_namelist_t_val = NULL;

	/* Make sure we are in the cluster */
	rstatus = (int)scconf_ismember(0, &ismember);
	if (rstatus || !ismember) {
		result->sc_res_errno = SCCONF_ENOCLUSTER;
		return (TRUE);
	}

	/*
	 * Get the major number for this driver as well as the maximum
	 * entry in the /etc/name_to_major file.
	 */
	namelist = (scadmin_namelist_t *)scadmin_get_major_number(driver);
	if (namelist) {
		count = scadm_namelist_to_array(namelist, &array);
		scadmin_free_namelist(namelist);
		if (count) {
			result->sc_res_major_numbers.sc_namelist_t_len = count;
			result->sc_res_major_numbers.sc_namelist_t_val = array;
		}
	} else {
		/* Unexpected failure */
		rstatus = SCCONF_EUNEXPECTED;
	}

	/* Set the result code and return */
	result->sc_res_errno = (sc_errno_t)rstatus;
	return (TRUE);
}

/* ARGSUSED */
bool_t
scadmproc_get_zonelist_1_svc(sc_result_zonelist *result, struct svc_req *rqstp)
{
	int rstatus = SCCONF_NOERR;
	uint_t ismember;
	scadmin_namelist_t *namelist = (scadmin_namelist_t *)0;
	char **array = (char **)0;
	uint_t count;

	/* Initialize result */
	result->sc_res_errno = 0;
	result->sc_res_zonelist.sc_namelist_t_len = 0;
	result->sc_res_zonelist.sc_namelist_t_val = NULL;

	/* Make sure we are in the cluster */
	rstatus = (int)scconf_ismember(0, &ismember);
	if (rstatus || !ismember) {
		result->sc_res_errno = SCCONF_ENOCLUSTER;
		return (TRUE);
	}

	/*
	 * Get the list of all xip zones
	 */
	namelist = (scadmin_namelist_t *)scadmin_get_zonelist();
	if (namelist) {
		count = scadm_namelist_to_array(namelist, &array);
		scadmin_free_namelist(namelist);
		if (count) {
			result->sc_res_zonelist.sc_namelist_t_len = count;
			result->sc_res_zonelist.sc_namelist_t_val = array;
		}
	}

	/* Set the result code and return */
	result->sc_res_errno = (sc_errno_t)rstatus;
	return (TRUE);
}

bool_t
scadmproc_update_ntp_1_svc(sc_result *result, struct svc_req *rqstp)
{
	const char *cmd_string = "/usr/cluster/lib/sc/sc_update_ntp";
	scconf_errno_t rstatus;
	struct stat sbuf;
	uint_t ismember;
	clconf_errnum_t msg_error;
	scconf_nodeid_t nodeid;

	/* Make sure we are in the cluster */
	rstatus = scconf_ismember(0, &ismember);
	if (rstatus == SCCONF_NOERR && !ismember)
		rstatus = SCCONF_ENOCLUSTER;
	if (rstatus != SCCONF_NOERR) {
		result->sc_res_errno = (sc_errno_t)rstatus;
		return (TRUE);
	}

	/* Make sure the shell script exists */
	if (stat(cmd_string, &sbuf) != 0) {
		result->sc_res_errno = (sc_errno_t)SCCONF_ENOEXIST;
		return (TRUE);
	}

	/* regular file? */
	if (!S_ISREG(sbuf.st_mode)) {
		result->sc_res_errno = (sc_errno_t)SCCONF_EINVAL;
		return (TRUE);
	}

	/* check for execute */
	if (!(sbuf.st_mode & S_IXOTH)) {
		result->sc_res_errno = (sc_errno_t)SCCONF_EPERM;
		return (TRUE);
	}

	/* Get my nodeid */
	rstatus = scconf_get_nodeid(NULL, &nodeid);
	if (rstatus != SCCONF_NOERR) {
		result->sc_res_errno = (sc_errno_t)rstatus;
		return (TRUE);
	}

	msg_error = clconf_do_execution(cmd_string, (nodeid_t)nodeid,
			NULL, B_FALSE,
			B_TRUE, B_TRUE);
	if (msg_error != CL_NOERROR) {
		if (msg_error == CL_NO_CLUSTER) {
			result->sc_res_errno = (sc_errno_t)SCCONF_ENOCLUSTER;
		} else {
			result->sc_res_errno = (sc_errno_t)SCCONF_EUNEXPECTED;
		}
		return (TRUE);
	}

	result->sc_res_errno = (sc_errno_t)SCCONF_NOERR;
	return (TRUE);
}

bool_t
scadmproc_update_hosts_1_svc(sc_result *result, struct svc_req *rqstp)
{
	const char *cmd_string = "/usr/cluster/lib/sc/sc_update_hosts";
	scconf_errno_t rstatus;
	struct stat sbuf;
	uint_t ismember;
	clconf_errnum_t msg_error;
	scconf_nodeid_t nodeid;

	/* Make sure we are in the cluster */
	rstatus = scconf_ismember(0, &ismember);
	if (rstatus == SCCONF_NOERR && !ismember)
		rstatus = SCCONF_ENOCLUSTER;
	if (rstatus != SCCONF_NOERR) {
		result->sc_res_errno = (sc_errno_t)rstatus;
		return (TRUE);
	}

	/* Make sure the shell script exists */
	if (stat(cmd_string, &sbuf) != 0) {
		result->sc_res_errno = (sc_errno_t)SCCONF_ENOEXIST;
		return (TRUE);
	}

	/* regular file? */
	if (!S_ISREG(sbuf.st_mode)) {
		result->sc_res_errno = (sc_errno_t)SCCONF_EINVAL;
		return (TRUE);
	}

	/* check for execute */
	if (!(sbuf.st_mode & S_IXOTH)) {
		result->sc_res_errno = (sc_errno_t)SCCONF_EPERM;
		return (TRUE);
	}

	/* Get my nodeid */
	rstatus = scconf_get_nodeid(NULL, &nodeid);
	if (rstatus != SCCONF_NOERR) {
		result->sc_res_errno = (sc_errno_t)rstatus;
		return (TRUE);
	}

	if (scconf_get_nodeid(NULL, &nodeid) != 0) {
		result->sc_res_errno = SCCONF_ENOCLUSTER;
		return (TRUE);
	}

	msg_error = clconf_do_execution(cmd_string, (nodeid_t)nodeid,
			NULL, B_FALSE,
			B_TRUE, B_TRUE);
	if (msg_error != CL_NOERROR) {
		if (msg_error == CL_NO_CLUSTER) {
			result->sc_res_errno = (sc_errno_t)SCCONF_ENOCLUSTER;
		} else {
			result->sc_res_errno = (sc_errno_t)SCCONF_EUNEXPECTED;
		}
		return (TRUE);
	}

	result->sc_res_errno = (sc_errno_t)SCCONF_NOERR;
	return (TRUE);
}

/* ARGSUSED */
int
scadmprog_1_freeresult(SVCXPRT *transp, xdrproc_t xdr_result, caddr_t result)
{
	xdr_free(xdr_result, result);

	return (TRUE);
}
