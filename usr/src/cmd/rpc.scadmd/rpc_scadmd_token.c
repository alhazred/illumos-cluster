/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)rpc_scadmd_token.c	1.3	08/05/20 SMI"

/*
 * rpc_scadmd_token.c
 */

#include <unistd.h>

#include "rpc_scadmd.h"

/*
 * boot_token_timeout
 *
 * This is the boot token timeout thread.   After SCCONF_RPCSVC_TOKENTO
 * seconds, the token is release.
 */
/* ARGSUSED */
void *
boot_token_timeout(void *arg)
{
	(void) sleep(SCCONF_RPCSVC_TOKENTO);

	if (mutex_lock(&_boot_token_lock) != 0)
		return ((void *)0);
	_boot_token = 0;
	(void) mutex_unlock(&_boot_token_lock);
	return ((void *)0);
}
