/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

/* Copyright (c) 1984, 1986, 1987, 1988, 1989 AT&T */
/* All Rights Reserved */

#pragma ident	"@(#)rpc_scadmd_main.c	1.17	08/05/20 SMI"

/*
 * rpc_scadmd_main.c
 *
 *	main for rpc.scadmd(1M)
 */

/*lint -e793 */
#include "rpc_scadmd.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <strings.h>
#include <stropts.h>
#include <netconfig.h>
#include <syslog.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/mutex.h>

#include <netinet/in.h>

#include <rpc/rpc.h>
#include <rpc/rpcent.h>

#include "rpc_scadmd_util.h"
#include <rgm/sczones.h>

/* Globals */
char *progname;			/* program name */

/* ARGSUSED */
int
main(int argc, char **argv)
{
#ifdef MULTI_THREAD_MODE
	int mode = RPC_SVC_MT_AUTO;
#endif /* MULTI_THREAD_MODE */
	struct rpcent rpc_ent;
	struct rpcent *rpcentp;
	char buffer[BUFSIZ];
	int scadmprog;

	if ((progname = strrchr(argv[0], '/')) != NULL) {
		++progname;
	} else {
		progname = argv[0];
	}

	if (sc_zonescheck() != 0)
		return (1);



	openlog("rpc_scadmd", LOG_PID, LOG_DAEMON);

	/* Must be root */
	if (getuid() != 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * A user other than root attempted to run this program.
		 * @user_action
		 * If this error occurred during normal operation, save the
		 * contents of /var/adm/messages from all nodes and contact
		 * your Sun service representative for assistance in
		 * diagnosing and correcting the problem.
		 */
		syslog(LOG_ERR, "%s:  Not root.", progname);
		exit(1);
	}

	/* Initialize the RPC program number */
	bzero(&rpc_ent, sizeof (rpc_ent));
	if ((rpcentp = getrpcbyname_r(SCADMPROGNAME, &rpc_ent,
	    buffer, sizeof (buffer))) == NULL) {
		scadmprog = SCADMPROG;
	} else {
		scadmprog = rpcentp->r_number;
	}

#ifdef MULTI_THREAD_MODE
	/* Multi-thread mode */
	if (!rpc_control(RPC_SVC_MTMODE_SET, &mode)) {
		/*
		 * SCMSGS
		 * @explanation
		 * This program could not set automatic MT mode.
		 * @user_action
		 * Save the contents of /var/adm/messages from all nodes
		 * and contact your Sun service representative for assistance
		 * in diagnosing and correcting the problem.
		 */
		syslog(LOG_ERR, "%s:  Unable to set automatic MT mode.",
		    progname);
		exit(1);
	}
#endif /* MULTI_THREAD_MODE */
	(void) sigset(SIGPIPE, SIG_IGN);

	/*
	 * If stdin looks like a TLI endpoint, we assume
	 * that we were started by a port monitor. If
	 * t_getstate fails with TBADF, this is not a
	 * TLI endpoint.
	 */
	if (t_getstate(0) != -1 || t_errno != TBADF) {
		char *netid;
		struct netconfig *nconf = NULL;
		SVCXPRT *transp;
		int pmclose;

		if ((netid = getenv("NLSPROVIDER")) == NULL) {
			pmclose = 1;
		} else {
			if ((nconf = getnetconfigent(netid)) == NULL)
				/*
				 * SCMSGS
				 * @explanation
				 * This program could not get transport
				 * information for the specified NLSPROVIDER.
				 * @user_action
				 * Save the contents of /var/adm/messages from
				 * all nodes and contact your Sun service
				 * representative for assistance in diagnosing
				 * and correcting the problem.
				 */
				syslog(LOG_ERR,
				    "%s:  Cannot get transport info", progname);
			pmclose = (t_getstate(0) != T_DATAXFER);
		}
		if ((transp = svc_tli_create(0, nconf, NULL, 0, 0)) == NULL) {
			/*
			 * SCMSGS
			 * @explanation
			 * The daemon cannot provide RPC service because
			 * a call to get a service handle failed.
			 * @user_action
			 * Save the contents of /var/adm/messages from all
			 * nodes and contact your Sun service representative
			 * for assistance in diagnosing and correcting the
			 * problem.
			 */
			syslog(LOG_ERR, "%s:  Cannot create server handle",
			    progname);
			exit(1);
		}
		if (nconf)
			freenetconfigent(nconf);
		if (!svc_reg(transp, (uint_t)scadmprog, SCADMVERS,
		    scadmd_1, 0)) {
			/*
			 * SCMSGS
			 * @explanation
			 * This program could not register the RPC service.
			 * @user_action
			 * Save the contents of /var/adm/messages from all
			 * nodes and contact your Sun service representative
			 * for assistance in diagnosing and correcting the
			 * problem.
			 */
			syslog(LOG_ERR, "%s:  Unable to register.", progname);
			exit(1);
		}
		if (pmclose) {
			if (thr_create(NULL, 0, closedown, NULL,
			    0, NULL) != 0) {
				/*
				 * SCMSGS
				 * @explanation
				 * This program could not create the closedown
				 * thread.
				 * @user_action
				 * Save the contents of /var/adm/messages from
				 * all nodes and contact your Sun service
				 * representative for assistance in diagnosing
				 * and correcting the problem.
				 */
				syslog(LOG_ERR,
				    "%s:  Cannot create closedown thread",
				    progname);
				exit(1);
			}
		}

	} else if (!svc_create(scadmd_1, (uint_t)scadmprog, SCADMVERS,
	    "circuit_v")) {
		/*
		 * SCMSGS
		 * @explanation
		 * This program could not create the RPC server handle.
		 * @user_action
		 * Save the contents of /var/adm/messages from all nodes and
		 * contact your Sun service representative for assistance in
		 * diagnosing and correcting the problem.
		 */
		syslog(LOG_ERR,
		    "%s:  Create failed for netpath \"circuit_v\".", progname);
		exit(1);
	}

	svc_run();

	/*
	 * SCMSGS
	 * @explanation
	 * The RPC server exited unexpectedly.
	 * @user_action
	 * Save the contents of /var/adm/messages from all nodes and contact
	 * your Sun service representative for assistance in diagnosing and
	 * correcting the problem.
	 */
	syslog(LOG_ERR, "%s:  svc_run returned", progname);
	return (1);
}
