/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _RPC_SCCONF_UTIL_H
#define	_RPC_SCCONF_UTIL_H

#pragma ident	"@(#)rpc_scadmd_util.h	1.9	08/05/20 SMI"

/*
 * This header file contains internal procedures used by the SCCONF RPC
 * routines
 */

#ifdef __cplusplus
extern "C" {
#endif

#include <scadmin/scadmin.h>

/* Functions */
extern void *closedown(void *arg);
extern int scadmd_getprotaddr(int fd, struct t_bind *boundaddr,
    struct t_bind *peeraddr);
extern void scadmd_1(struct svc_req *rqstp, register SVCXPRT *transp);

/*
 *  scadm_gdevrange_array_to_llist
 *
 *  This function converts the gdev range array passed from RPC to linked
 *  list expected by the local functions called.
 */

extern sc_errno_t scadm_gdevrange_array_to_llist(sc_gdev_range_t *dsdevices,
    scconf_gdev_range_t **device_list);

/*
 *  scadm_props_array_to_llist
 *
 *  This function converts the properties arrays passed from RPC to linked
 *  list expected by the local functions called.
 */

extern sc_errno_t scadm_props_array_to_llist(sc_cfg_prop_t *prop_array,
    scconf_cfg_prop_t **prop_list);

/*
 *  scadm_nodelist_xdr_array_to_c_array
 *
 *  This function converts the nodename array passed from RPC to a null
 *  terminated array.
 *
 *  Upon success, the pointer pointed to by dsnodes_array
 *  will be set to point to a NULL terminated array of pointers to
 *  nodenames.
 */

extern sc_errno_t scadmd_nodelist_xdr_array_to_c_array(
    sc_dsnodes_t dsnodes_xdrarray,  char ***dsnodes_array);
/*
 *  scadmd_free_gdevrange
 *
 *  Frees the data associated with a gdevrange linked list
 */

extern void scadmd_free_gdevrange(scconf_gdev_range_t *device_list);

/*
 *  scadm_free_propertylist
 *
 *  Frees the data contained in a propties linked list
 */
extern void scadmd_free_propertylist(scconf_cfg_prop_t *prop_list);

/*
 * scadm_namelist_to_array
 *
 * This function converts an scadmin_namelist_t to an array.
 * The length of the array is returned.
 */
extern uint_t scadm_namelist_to_array(scadmin_namelist_t *namelist,
    char ***arrayp);

#ifdef __cplusplus
}
#endif

#endif /* _RPC_SCCONF_UTIL_H */
