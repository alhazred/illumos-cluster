/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)rpc_scadmd_prog.c	1.27	08/05/20 SMI"

/*
 * rpc_scadmd_prog.c
 *
 *	scadmd_1() performs important pre-processing before calling
 *	scadmprog_1(), the rpcgen(1) generated dispatch routine.
 */

/*lint -e793 */
#include "rpc_scadmd.h"
#include "rpc_scadmd_util.h"

#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <strings.h>
#include <syslog.h>

#include <sys/types.h>
#include <sys/types.h>
#include <sys/socket.h>

#include <netinet/in.h>
#include <arpa/inet.h>

extern char *progname;

/*
 * scadmd_1
 *
 * See if this request came in over the public or private wire.
 *
 * If it came in over the public wire, make sure that it is one of
 * the requests that we publicly honor.   Then, perform any necessary
 * authentication.   Requests coming in on the private line are not
 * authenticated.
 *
 * If the request is okay to pass on, call scadmprog_1, the rpcgen(1)
 * generated dispatch routine.
 */
void
scadmd_1(struct svc_req *rqstp, register SVCXPRT *transp)
{
	scconf_errno_t rstatus;
	scconf_namelist_t *joinlist = (scconf_namelist_t *)0;
	scconf_cfg_cluster_t *clconfig = (scconf_cfg_cluster_t *)0;
	struct t_bind *boundaddr;
	in_addr_t my_netaddr;
	uint_t isclustermember;
	in_addr_t priv_netaddr;
	in_addr_t priv_netmask;
	scconf_namelist_t *namelist;
	scconf_authtype_t authtype;
	int isprivate = 0;
	struct authsys_parms *sys_cred;
	struct authdes_cred *des_cred;
	char *authdes_name;
	char hostname[MAXHOSTNAMELEN + 1];
	uid_t uid;
	size_t len;

	/* Get the address on which this request came in */
	boundaddr = (struct t_bind *)t_alloc(rqstp->rq_xprt->xp_fd,
	    T_BIND, T_ALL);
	if (!boundaddr) {
		/*
		 * SCMSGS
		 * @explanation
		 * The cluster was unable to allocate space for network
		 * protocol information during a cluster installation or
		 * configuration change.
		 * @user_action
		 * Check memory utilization on each cluster node and install
		 * more memory or increase swap space, as necessary. If memory
		 * utilization appears to be otherwise normal, contact your
		 * authorized Sun service provider to determine whether a
		 * workaround or patch is available. Once the issue is
		 * addressed, retry the affected administrative operation.
		 */
		syslog(LOG_ERR, "%s: t_alloc() failed: %s.", progname,
		    t_strerror(t_errno));
		svcerr_systemerr(transp);
		goto cleanup;
	}
	if (scadmd_getprotaddr(rqstp->rq_xprt->xp_fd, boundaddr, 0) < 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		syslog(LOG_ERR, "%s:  t_getprotaddr() failed!",
		    progname);
		svcerr_systemerr(transp);
		goto cleanup;
	}

	/* Get the private netaddr and netmask */
	priv_netmask = 0;
	priv_netaddr = 0;
	isclustermember = 0;
	(void) scconf_ismember(0, &isclustermember);
	if (isclustermember) {
		if (scconf_get_clusterconfig(&clconfig) != SCCONF_NOERR ||
		    clconfig->scconf_cluster_privnetaddr == NULL ||
		    clconfig->scconf_cluster_privnetmask == NULL) {
			/*
			 * SCMSGS
			 * @explanation
			 * The daemon is unable to get private net address.
			 * Cluster is configured incorrectly on the machine
			 * where message is logged.
			 * @user_action
			 * Need a user action for this message.
			 */
			syslog(LOG_ERR, "%s:  Not able to get the private "
			    "network address.", progname);
			svcerr_systemerr(transp);
			goto cleanup;
		}
		priv_netaddr =
		    ntohl(inet_addr(clconfig->scconf_cluster_privnetaddr));
		priv_netmask =
		    ntohl(inet_addr(clconfig->scconf_cluster_privnetmask));
		if (priv_netmask)
			priv_netaddr &= priv_netmask;
		if (!priv_netaddr) {
			syslog(LOG_ERR, "%s:  Not able to get the private "
			    "network address.", progname);
			svcerr_systemerr(transp);
			goto cleanup;
		}
		scconf_free_clusterconfig(clconfig);
		clconfig = (scconf_cfg_cluster_t *)0;
	}

	/*
	 * If it came in on the private net, set "isprivate"
	 *
	 * The calculated priv_netaddr always results in an IPv4 address,
	 * since it's derived from the same in the CCR. But incoming
	 * connections may also arrive from IPv6 sources. If they do, then
	 * we can safely assume they're not coming from the private
	 * interconnect.
	 */

	if ((priv_netaddr) && (boundaddr->addr.maxlen ==
	    sizeof (struct sockaddr_in))) {
		my_netaddr = ntohl(
		    ((struct sockaddr_in *)boundaddr->addr.buf)->
		    sin_addr.s_addr);
		if (priv_netmask)
			my_netaddr &= priv_netmask;
		if (my_netaddr == priv_netaddr)
			isprivate = 1;
	}

	switch (rqstp->rq_proc) {

	/*
	 * Don't care which network or whether it is from a known host
	 */
	case NULLPROC:
	case SCADMPROC_GET_ISMEMBER:
	case SCADMPROC_GET_AUTHTYPE:
	case SCADMPROC_GET_CLUSTERNAME:
	case SCADMPROC_GET_CLUSTER_VM_INFO:
		break;

	/*
	 * Public network is okay, but we may need to authenticate caller
	 */
	case SCADMPROC_ADD_NODE:
	case SCADMPROC_ADD_CLTR_ADAPTER:
	case SCADMPROC_ADD_CLTR_CPOINT:
	case SCADMPROC_ADD_CLTR_CABLE:
	case SCADMPROC_COPY_FROM:
	case SCADMPROC_REMOVE_FILE:
	case SCADMPROC_GET_NETWORK_ADAPTERS:
	case SCADMPROC_TRANSPORT_CONFIG:
	case SCADMPROC_BROADCAST_PING:
	case SCADMPROC_SNOOP:
	case SCADMPROC_AUTODISCOVER:
	case SCADMPROC_REMOVE_NODE:
	case SCADMPROC_GET_MAJOR_NUMBER:
	case SCADMPROC_UPDATE_NTP:
	case SCADMPROC_UPDATE_HOSTS:
	case SCADMPROC_GET_ZONELIST:
		/* If this is on the private net, we are done */
		if (isprivate)
			break;

		/* Get the list of allowed hosts */
		rstatus = scconf_get_secure_joinlist(&joinlist);
		if (rstatus != SCCONF_NOERR) {
			svcerr_systemerr(transp);
			goto cleanup;
		}

		/* If the list is NULL, anyone is okay */
		if (joinlist == NULL)
			break;

		/* If the list contains a single ".", nobody is okay */
		if (joinlist->scconf_namelist_name &&
		    strcmp(joinlist->scconf_namelist_name, ".") == 0 &&
		    joinlist->scconf_namelist_next == NULL) {
			svcerr_weakauth(transp);
			goto cleanup;
		}

		/* Get the authentication type */
		rstatus = scconf_get_secure_authtype(&authtype);
		if (rstatus != SCCONF_NOERR) {
			svcerr_systemerr(transp);
			goto cleanup;
		}

		uid = -1;
		*hostname = '\0';
		switch (rqstp->rq_cred.oa_flavor) {
		case AUTH_SYS:
			/* Make sure cluster is configured to allow this */
			if (authtype != SCCONF_AUTH_SYS) {
				svcerr_weakauth(transp);
				goto cleanup;
			}

			/* Get the credentials */
			sys_cred = (struct authsys_parms *)rqstp->rq_clntcred;

			/* Get the uid and hostname */
			uid = sys_cred->aup_uid;
			(void) strncpy(hostname, sys_cred->aup_machname,
			    sizeof (hostname));

			break;

		case AUTH_DES:
			if (authtype != SCCONF_AUTH_DES &&
			    authtype != SCCONF_AUTH_SYS) {
				svcerr_weakauth(transp);
				goto cleanup;
			}
			/* Get the credentials */
			des_cred = (struct authdes_cred *)rqstp->rq_clntcred;
			authdes_name = des_cred->adc_fullname.name;

			/* Get the hostname, unqualified */
			if (netname2host(authdes_name, hostname,
			    sizeof (hostname)) != 1) {
				svcerr_weakauth(transp);
				goto cleanup;
			}

			/* If netname2host() succeeded, uid must be zero */
			uid = 0;

			break;

		default:
			svcerr_weakauth(transp);
			goto cleanup;
		}

		/* Make sure that the hostname is in our list */
		for (namelist = joinlist;  namelist;
		    namelist = namelist->scconf_namelist_next) {
			len = strlen(namelist->scconf_namelist_name);
			if (strncasecmp(namelist->scconf_namelist_name,
			    hostname, len) == 0 &&
			    (hostname[len] == '\0' || hostname[len] == '.')) {
				break;
			}
		}
		if (namelist == NULL || uid != 0) {
			svcerr_weakauth(transp);
			goto cleanup;
		}

		break;

	/*
	 * May only occur over the private network
	 */

	case SCADMPROC_ADD_DS:
	case SCADMPROC_CHANGE_DS:
	case SCADMPROC_GET_BOOTTOKEN:
		if (!isprivate) {
			svcerr_noproc(transp);
			goto cleanup;
		}
		break;

	/*
	 * Unknown procedure
	 */
	default:
		svcerr_noproc(transp);
		goto cleanup;
	}

	/* If we are here, it must be okay to call the dispatch function */
	scadmprog_1(rqstp, transp);

cleanup:
	if (clconfig)
		scconf_free_clusterconfig(clconfig);

	if (joinlist)
		scconf_free_namelist(joinlist);

	if (boundaddr)
		(void) t_free((char *)boundaddr, T_BIND);
}
