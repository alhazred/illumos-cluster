/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)rpc_scadmd_util.c	1.8	08/05/20 SMI"

/*
 * rpc_scadmd_util.c
 *
 *	This file contains internal utility routines used by the RPC routines
 */

/*lint -e793 */
#include "rpc_scadmd.h"
#include "rpc_scadmd_util.h"

#include <scadmin/scadmin.h>

#include <malloc.h>
#include <string.h>
#include <assert.h>

/*
 *  scadm_gdevrange_array_to_llist
 *
 *  This function converts the gdev range array passed from RPC to linked
 *  list expected by the local functions called.
 *
 *  Upon success, the pointer pointed to by device_list
 *  will be set to point to a NULL terminated array of pointers to
 *  gdev_range structures (scconf_gdev_range_t).
 */

sc_errno_t
scadm_gdevrange_array_to_llist(sc_gdev_range_t *dsdevices,
    scconf_gdev_range_t **device_list)
{
	sc_errno_t rstatus;
	int i;
	scconf_gdev_range_t *current_gdev, *prev_gdev = NULL;
	sc_gdev_range_entry_t *gdev_array_entry;

	*device_list = NULL;

	/*  loop through the device array */
	for (i = 0; i < (int)dsdevices->sc_gdev_range_t_len; ++i) {
		/*  Allocate a list entry */
		current_gdev = (scconf_gdev_range_t *)
		    calloc(1, sizeof (scconf_gdev_range_t));
		if (current_gdev == NULL) {
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}

		/*
		 * The first entry becomes the head of the list, so tell
		 * caller
		 */
		if (i == 0)
			*device_list = current_gdev;

		/*  Now populate the list entry */
		gdev_array_entry = &dsdevices->sc_gdev_range_t_val[i];
		current_gdev->major_no = gdev_array_entry->sc_range_major_no;
		current_gdev->start_minor
		    = gdev_array_entry->sc_range_start_minor;
		current_gdev->end_minor
		    = gdev_array_entry->sc_range_end_minor;
		current_gdev->scconf_gdev_next = NULL;

		/* Link this entry to the previous one */
		if (prev_gdev != NULL)
			prev_gdev->scconf_gdev_next = current_gdev;

		/* Save the pointer for the next entry */
		prev_gdev = current_gdev;
	}

	rstatus = SCCONF_NOERR;

cleanup:
	if (rstatus != SCCONF_NOERR) {
		scadmd_free_gdevrange(*device_list);

		*device_list = NULL;
	}

	return (rstatus);
}

/*
 *  scadm_props_array_to_llist
 *
 *  This function converts the properties arrays passed from RPC to linked
 *  list expected by the local functions called.
 *
 *  Upon success, the pointer pointed to by prop_list
 *  will be set to point to a NULL terminated array of pointers to
 *  properties.
 */

sc_errno_t
scadm_props_array_to_llist(sc_cfg_prop_t *prop_array,
    scconf_cfg_prop_t **prop_list)
{
	sc_errno_t rstatus;
	int i;
	scconf_cfg_prop_t *current_prop, *prev_prop = NULL;
	sc_cfg_prop_entry_t *prop_array_entry;

	prop_list = NULL;

	/*  loop through the properties array */
	for (i = 0; i < (int)prop_array->sc_cfg_prop_t_len; ++i) {
		/*  Allocate a list entry */
		current_prop
		    = (scconf_cfg_prop_t *)calloc(1,
			sizeof (scconf_cfg_prop_t));
		if (current_prop == NULL) {
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}

		/*
		 * The first entry becomes the head of the list, so tell
		 * caller
		 */
		if (i == 0)
			*prop_list = current_prop;

		/*  Now populate the list entry */
		prop_array_entry = &(prop_array->sc_cfg_prop_t_val[i]);
		current_prop->scconf_prop_key = prop_array_entry->sc_prop_key;
		current_prop->scconf_prop_value
		    = prop_array_entry->sc_prop_value;
		current_prop->scconf_prop_next = NULL;

		/* Link this entry to the previous one */
		if (prev_prop != NULL)
			prev_prop->scconf_prop_next = current_prop;

		/* Save the pointer for the next entry */
		prev_prop = current_prop;
	}

	rstatus = SCCONF_NOERR;

cleanup:
	if (rstatus != SCCONF_NOERR) {
		scadmd_free_propertylist(*prop_list);

		*prop_list = NULL;
	}

	return (rstatus);
}

/*
 *  scadmd_free_gdevrange
 *
 *  Frees the data associated with a gdevrange linked list
 */

void
scadmd_free_gdevrange(scconf_gdev_range_t *device_list)
{
	scconf_gdev_range_t *current_gdev, *prev_gdev = NULL;

	for (current_gdev = device_list; current_gdev != NULL;
	    current_gdev = prev_gdev) {
		prev_gdev = current_gdev->scconf_gdev_next;
		free(current_gdev);
	}
}

/*
 *  scadm_free_propertylist
 *
 *  Frees the data contained in a propties linked list
 */
void
scadmd_free_propertylist(scconf_cfg_prop_t *prop_list)
{
	scconf_cfg_prop_t *current_prop, *prev_prop = NULL;

	for (current_prop = prop_list; current_prop != NULL;
	    current_prop = prev_prop) {
		prev_prop = current_prop->scconf_prop_next;
		free(current_prop);
	}

}

/*
 *  scadm_nodelist_xdr_array_to_c_array
 *
 *  This function converts the nodename array passed from RPC to a null
 *  terminated array.
 *
 *  Upon success, the pointer pointed to by dsnodes_array
 *  will be set to point to a NULL terminated array of pointers to
 *  nodenames.
 */

/*lint -save -e1746 */
/*
 * Lint report a C++ informational messages
 */
sc_errno_t
scadmd_nodelist_xdr_array_to_c_array(sc_dsnodes_t dsnodes_xdrarray,
    char ***dsnodes_array)
{
	sc_errno_t rstatus;
	int i;

	*dsnodes_array = (char **)calloc(dsnodes_xdrarray.sc_dsnodes_t_len +
	    1, sizeof (char *));

	if (*dsnodes_array == NULL) {
		rstatus = SCCONF_ENOMEM;
		goto cleanup;
	}

	for (i = 0; i < (int)dsnodes_xdrarray.sc_dsnodes_t_len; ++i) {
		(*dsnodes_array)[i] = dsnodes_xdrarray.sc_dsnodes_t_val[i];
	}

	(*dsnodes_array)[i] = NULL;

	rstatus = SCCONF_NOERR;

cleanup:
	if (rstatus != SCCONF_NOERR) {
		free(*dsnodes_array);
		*dsnodes_array = NULL;
	}

	return (rstatus);
}
/*lint -restore */

/*
 * scadm_namelist_to_array
 *
 * This function converts an scadmin_namelist_t to an array.
 * The length of the array is returned.
 */
uint_t
scadm_namelist_to_array(scadmin_namelist_t *namelist, char ***arrayp)
{
	uint_t i, count;
	scadmin_namelist_t *nl;
	char **array = (char **)0;

	/* Check args */
	if (namelist == NULL || arrayp == NULL)
		return (0);

	count = 0;
	for (nl = namelist;  nl;  nl = nl->scadmin_nlist_next) {
		if (nl->scadmin_nlist_name)
			++count;
	}
	if (count == 0)
		return (0);

	array = (char **)calloc(count, sizeof (char *));
	if (array == NULL)
		return (0);

	i = 0;
	for (nl = namelist;  nl;  nl = nl->scadmin_nlist_next) {
		if (nl->scadmin_nlist_name) {
			array[i] = strdup(nl->scadmin_nlist_name);
			if (array[i] == NULL) {
				for (i = 0;  array[i];  i++)
					free(array[i]);
				free(array);
				return (0);
			}
			++i;
		}
	}
	assert(i == count);

	*arrayp = array;

	return (count);
}
