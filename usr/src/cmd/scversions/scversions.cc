/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scversions.cc	1.12	08/05/20 SMI"

/*
 * scversions(1M)
 *
 * command implementation.
 *
 */

#include <stdio.h>
#include <stdlib.h> /* getenv, exit */
#include <libintl.h>
#include <locale.h>
#include <sys/types.h>
#include <unistd.h>

#include <rgm/sczones.h>
#include <sys/vm_util.h>
#include <orb/infrastructure/orb.h>

#ifndef _KERNEL_ORB
#include <sys/cl_auth.h>
#endif

/* Forward declaration */
static int check_upgrade_needed(void);

static char *progname;

/*
 * purpose: print a usage message to stdout.
 */
static void
usage()
{
	(void) fprintf(stderr, gettext("usage:  %s [-c]\n"), progname);
}

/*
 * purpose: print list of nodes in the given bitmask.
 */
static void
print_mismatched_versions(version_manager::membership_bitmask_t downrev_nodes)
{
	char nodename[CL_MAX_LEN+1];

	// Initialize the clconf library to get node names from node ids.
	bool lib_inited = true;

	if (clconf_lib_init() != 0) {
		(void) fprintf(stderr, "%s\n",
		    gettext("Failed to initialize clconf library.\n"
		    "Cannot convert node ids to node names."));
		lib_inited = false;
	}
	(void) fprintf(stderr, "%s\n",
	    gettext("Can not commit upgrade as following nodes appear to\n"
	    "have older versions of Sun Cluster software: "));
	for (uint_t i = 1; i <= NODEID_MAX; i++) {
		if (downrev_nodes & (1ULL << (i-1))) {
			if (lib_inited) {
				clconf_get_nodename(i, nodename);
				(void) fprintf(stderr, "%s ", nodename);
			} else {
				(void) fprintf(stderr, "%d ", i);
			}

		}
	}
	(void) fprintf(stderr, "\n");
	(void) fprintf(stderr, "%s\n",
	    gettext("Please run scinstall(1M) on these nodes to identify"
	    " older versions."));
}

/*
 * purpose: to invoke commit callbacks.
 *
 * return: 0 if upgrade_commit() was invoked and will result in change
 *	     in versions.
 *	   1 If commit cannot be done because of mismatched versions on
 *	     different nodes.
 *	   2 if we could not invoke upgrade_commit().
 */
static int
do_commit()
{
	Environment		e;
	version_manager::vm_admin_var	vm_v;

	vm_v = vm_util::get_vm();
	if (CORBA::is_nil(vm_v)) {
		(void) fprintf(stderr, "%s:  %s\n", progname,
		    gettext("Could not find the version manager."));
		return (2);
	}
	version_manager::vp_state state = version_manager::ALL_VERSIONS_MATCH;
	version_manager::membership_bitmask_t downrev_nodes = 0;

	vm_v->upgrade_commit(state, downrev_nodes, e);
	CORBA::Exception *expectp = e.exception();

	if (expectp == NULL) {
		if (downrev_nodes != 0) {
			//
			// The RM supports upgrade_commit() but some other
			// nodes are running older versions
			//
			print_mismatched_versions(downrev_nodes);
			return (1);
		} else {
			return (0);
		}
	}

	if (CORBA::VERSION::_exnarrow(expectp)) {
		//
		// The RM does not support upgrade_commit() interface.
		//
		(void) fprintf(stderr, "%s:  %s\n", progname,
		    gettext("Upgrade commit cannot be performed until all "
			"cluster nodes are upgraded.\n"));
		(void) fprintf(stderr, "%s\n",
		    gettext("Please run scinstall(1M) on cluster nodes to "
			"identify older versions."));
		e.clear();
		return (2);
	} else if (expectp) {
		expectp->print_exception(
		    "upgrade_commit() returned exception.");
		(void) fprintf(stderr, "%s:  %s\n", progname,
		    gettext("Could not commit upgrade."));
		e.clear();
		return (2);
	}
	return (0);
}

/*
 * purpose: check if upgrade commit will cause change in running
 * versions.
 *
 * return: 0 if commit will change rvs or commit is not needed.
 *         2 if we could not determine due to exception or other errors.
 */
static int
check_upgrade_needed()
{
	Environment				e;
	version_manager::vm_admin_var		vm_v = nil;
	version_manager::vp_state		state;
	version_manager::membership_bitmask_t	downrev_nodes;

	vm_v = vm_util::get_vm();
	if (CORBA::is_nil(vm_v)) {
		(void) fprintf(stderr, "%s:  %s\n", progname,
		    gettext("Could not find the version manager."));
		return (2);
	}
	bool upgrade_needed = vm_v->is_upgrade_needed(state, downrev_nodes, e);
	CORBA::Exception *expectp = e.exception();

	if (CORBA::VERSION::_exnarrow(expectp)) {
		//
		// The VM primary does not support the is_upgrade_needed()
		// interface.
		//
		(void) fprintf(stderr, "%s:  %s\n", progname,
		    gettext("Check upgrade cannot be performed until all "
			"cluster nodes are upgraded.\n"));
		(void) fprintf(stderr, "%s\n",
		    gettext("Please run scinstall(1M) on cluster nodes to "
			"identify older versions."));
		e.clear();
		return (2);
	} else if (expectp != NULL) {
		expectp->print_exception("is_upgrade_needed() returned "
		    "unexpected exception.\n");
		(void) fprintf(stderr, "%s\n",
		    gettext("Can not check if upgrade commit is needed."));
		e.clear();
		return (2);
	}
	if (upgrade_needed) {
		(void) fprintf(stdout, "%s\n",
		    gettext("Upgrade commit is needed."));
		return (0);
	}
	if (!upgrade_needed && (state == version_manager::ALL_VERSIONS_MATCH)) {
		(void) fprintf(stdout, "%s\n",
		    gettext("Upgrade commit is NOT needed. All"
		    " versions match."));
		return (0);
	}
	if (!upgrade_needed &&
	    (state == version_manager::MISMATCHED_VERSIONS)) {
		print_mismatched_versions(downrev_nodes);
		return (0);
	}
	return (0);
}

/*
 * scversions()
 * Does all the work.
 * Called from main() or unode_load
 */
int
scversions(int argc, char **argv)
{
	int	c = 0;
	bool	commit = false;
	bool	check_upgrade = false;

	/* Set the program name  */
	char *np = strrchr(argv[0], '/');
	if (np) {
		progname = ++np;
	} else {
		progname = argv[0];
	}

#ifndef _KERNEL_ORB

	/* Initialize and demote to normal uid */
	cl_auth_init();
	cl_auth_demote();
#endif
	if (argc == 1) {
		check_upgrade = true;
	} else {

		while ((c = getopt(argc, argv, "c")) != EOF) {
			switch (c) {
			case 'c':
#ifndef _KERNEL_ORB
				cl_auth_check_command_opt_exit(*argv, "-c",
				    CL_AUTH_SYSTEM_ADMIN, 2);
#endif
				commit = true;
				break;
			case '?':
			default:
				usage();
				return (2);
			}
		}
	}

	if (ORB::initialize()) { //lint !e527
		(void) fprintf(stderr, "%s:  %s\n", progname,
		    gettext("Could not initialize ORB."));
		return (2);
	}
	if (check_upgrade) {
		return (check_upgrade_needed());
	}
	if (commit) {
		return (do_commit());
	}

	return (0);
}
#ifndef _KERNEL_ORB

/*
 * purpose: main program
 */
int
main(int argc, char *argv[])
{
	/* House keeping */
	(void) setlocale(LC_ALL, "");
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);
	(void) textdomain(TEXT_DOMAIN);

	if (sc_zonescheck() != 0)
		return (1);
	return (scversions(argc, argv));
}
#endif //  _KERNEL_ORB
