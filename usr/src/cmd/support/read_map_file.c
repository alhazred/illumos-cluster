/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)read_map_file.c	1.7	08/05/20 SMI"

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>

struct map_file_ent {
	int gminor;
	uint_t	lminor;
	uint_t type;
	uint_t nodeid;
};

int
main(int argc, char **argv)
{
	int fd;
	struct map_file_ent ent;

	if (argc < 2) {
		(void) fprintf(stderr, "Usage %s <filename> \n", argv[0]);
		exit(1);
	}

	if ((fd = open(argv[1], O_RDONLY)) < 0) {
		(void) fprintf(stderr, "file %s open failed \n", argv[1]);
		exit(1);
	}

	while (read(fd, &ent, sizeof (struct map_file_ent))) {
		if (lseek(fd, 0, SEEK_CUR) < 0) {
			(void) fprintf(stderr, "couldn't seek \n");
			exit(1);
		}
		if (ent.gminor != -1) {
			(void) fprintf(stdout,
			    "mapfileent's gminor %d lminor %d "
			    "nodeid %d type %d \n", ent.gminor, ent.lminor,
			    ent.nodeid, ent.type);
		}
	};
	(void) fprintf(stdout, "successful run! \n");
	exit(0);
}
