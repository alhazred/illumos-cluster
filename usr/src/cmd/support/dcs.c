/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)dcs.c	1.7	08/05/20 SMI"

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <stropts.h>
#include <sys/modctl.h>

/*
 * The following generate lint warnings due to being unreferenced in the code.
 * For historical reference - leave them enclosed in a comment.
 * #define	DCS_MAKE_BENIGN	1
 * #define	DCS_SERVER_INIT	2
 * #define	DCS_CLIENT_INIT	3
 * #define	DCS_DEVCONFIG_UNLOCK	5
 * #define	DCS_INIT_OPS	7
 */
#define	DCS_DEVCONFIG_LOCK	4

/*
 * System call prototype
 */
int modctl(int, ...);

int
main(int argc, char **argv)
{
	int fd;

	int command;

	if (argc < 2) {
		(void) fprintf(stderr, "Usage: %s command integer\n", argv[0]);
		exit(1);
	}

	command = atoi(argv[1]);

	if (command == 0) {
		int devcnt;
		if (modctl(MODRESERVED, NULL, &devcnt) < 0) {
			perror("modctl");
			exit(1);
		}
		(void) fprintf(stdout, "%d \n", devcnt);
		exit(0);
	}
	(void) fprintf(stdout, "command is %d \n", command);
	if ((fd = open("/dev/dcs", O_RDWR)) < 0)
		perror("open");

	switch (command) {
	default:
		if (ioctl(fd, command, 0) < 0)
			perror("ioctl");
		break;
	case DCS_DEVCONFIG_LOCK:
		{
		int sleep = 1;
		if (ioctl(fd, command, &sleep) < 0)
			perror("ioctl");
		}
		break;
	}
	exit(0);
}
