/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scxcfgcmd_admin.cc	1.4	08/05/20 SMI"

#include <fstream>
#include <iostream>

#include <stdlib.h>
#include <unistd.h>
#include <limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/utsname.h>
#include <strings.h>
#include <errno.h>

#include <scxcfg/scxcfg.h>
#include "scxcfg_int.h"

#include "scxcfg_data_remote.h"
#include "scxcfg_data_file.h"

#include "scxcfgcmd.h"

using namespace std;

//
// scxcfgcmd_admin
//
// Administrative functions performed on farm nodes.
// - refresh thile file configuration cache.
// - configure this system as a farm node for a given cluster.
//

//
// On farm node,  scxcfg command in admin mode access the
// repository through RPC via a scxcfg_data_remote object.
//
static scxcfg_data_remote *remoteDataAccess = NULL;

//
// On farm node,  scxcfg command in admin mode access the local
// config repository via a scxcfg_data_file object.
//
static scxcfg_data_file *localDataAccess = NULL;


//
// getLocalName()
//
// Utility function.
//
int
getLocalName(char *nodename)
{
	struct utsname u;
	int rc;

	rc = uname(&u);
	if (rc == -1) {
		scxcfg_log(LOG_ERR, "uname failed: errno %d", errno);
		return (SCXCFG_ESYSERR);
	} else {
		strcpy(nodename, u.nodename);
		return (SCXCFG_OK);
	}
}

//
// init_remote_access()
//
// Create a scxcfg_data_remote object. It will discover the
// server on the network and retrieve a snapshot of the configuration
// for this farm node.
//
int
init_remote_access()
{
	int rc;
	remoteDataAccess = new scxcfg_data_remote();
	if (!remoteDataAccess)
		return (SCXCFG_ENOMEM);
	//
	// Retrieves all the needed config information for this node
	//
	rc = remoteDataAccess->init(FARM_TABLE);
	if (rc != SCXCFG_OK) {
		delete remoteDataAccess;
		remoteDataAccess = NULL;
	}
	return (rc);
}

//
// end_remote_access()
//
// Close what has been opened by init_remote_access().
//
void
end_remote_access()
{
	if (remoteDataAccess) {
		remoteDataAccess->close();
		delete remoteDataAccess;
		remoteDataAccess = NULL;
	}
}


//
// init_local_access_for_update()
//
// Create a scxcfg_data_file with admin constructor. To access
// the local config cache for update.
//
int
init_local_access_for_update()
{
	int rc;
	if (!scxcfg_isfarm())
		return (SCXCFG_ENOTFARM);
	//
	// Call special constructor with 2 params for admin mode.
	//
	localDataAccess = new scxcfg_data_file(FARM_CONFIG_FILE, 1);
	if (!localDataAccess)
		return (SCXCFG_ENOMEM);

	// We do not init() this localDataAccess, since we
	// are not intersted in its contents.
	return (SCXCFG_OK);
}

//
// end_local_access()
//
// Close what has been opened by init_local_access_for_update().
//
void
end_local_access()
{
	if (localDataAccess) {
		localDataAccess->close();
		delete localDataAccess;
		localDataAccess = NULL;
	}
}

//
// refreshFileCache()
//
// Refresh the file cache on farm node.
//
// Rely on the implementation of the init method of scxcfg_data_remote
// object.
// Will retyrn MAX_TRY times.
//
int
refreshFileCache()
{
	scxcfg_log(LOG_DEBUG, "refreshFileCache");
	int rc;
	int count = 0;
	const int MAX_TRY = 3;

	if (!scxcfg_isfarm())
		return (SCXCFG_ENOTFARM);
retry:
	count++;
	//
	// Initing the remove access should get a copy of the
	// farm configuration for this node.
	//
	rc = init_remote_access();
	if (rc != SCXCFG_OK) {
		if (count > MAX_TRY) {
			fprintf(stderr, "Fail refresh file cache:"
			    "init_remote_access: %d\n", rc);
			scxcfg_log(LOG_ERR, "Fail refresh file cache:"
			    "init_remote_access: %d\n", rc);
			goto cleanup;
		} else {
			goto retry;
		}
	}

	//
	// Open the local file cache config for update.
	//
	rc = init_local_access_for_update();

	if (rc != SCXCFG_OK) {
		fprintf(stderr, "Fail refresh file cache:"
		    "init_local_access_for_update: %d\n", rc);
		scxcfg_log(LOG_ERR, "Fail refresh file cache:"
		    "init_local_access_for_update: %d\n", rc);
		goto cleanup;
	}
	//
	// Get the current contents of the config we just retrieved from server.
	//
	stringstream *ss;
	ss = remoteDataAccess->getStringCache();

	if (!ss) {
		fprintf(stderr, "Fail refresh file cache:"
		    "no data from remote access\n");
		scxcfg_log(LOG_ERR, "Fail refresh file cache:"
		    "no data from remote access\n");
		rc = SCXCFG_ESYSERR;
		goto cleanup;
	}
	//
	// Now write it to the local file cache.
	//
	rc = localDataAccess->writeFile(ss);

	if (rc != SCXCFG_OK) {
		fprintf(stderr, "Fail refresh file cache:"
		    "could not write to local file: %d\n", rc);
		scxcfg_log(LOG_ERR, "Fail refresh file cache:"
		    "could not write to local file: %d\n", rc);
		goto cleanup;
	}
	//
	// If everything goes well we can get the data now.
	//
	rc = localDataAccess->init(FARM_CONFIG_FILE);
	if (rc != SCXCFG_OK) {
		fprintf(stderr, "Config file cache has been updated:"
		    "but error in data: %d\n", rc);
		scxcfg_log(LOG_ERR, "Config file cache has been updated:"
		    "but error in data: %d\n", rc);
	}
cleanup:
	end_local_access();
	end_remote_access();
	return (rc);
}

//
// create_file()
//
// Utility function. Assure that dir exists and create empty
// file fileName with rw rights only for owner (root).
//
int
createFile(const char *dir, const char *fileName)
{
	int rc;
	int fd;
	struct stat stbuf;

	rc = mkdir(dir, 0755);
	if (rc != 0 && errno != EEXIST) {
		return (-1);
	}
	rc = stat(fileName, &stbuf);
	if (rc && (errno != ENOENT)) {
		return (-1);
	}
	if (errno == ENOENT) {
		fd = open(fileName, O_CREAT|O_WRONLY, 0600);
	} else {
		fd = open(fileName, O_WRONLY|O_TRUNC, 0600);
	}
	if (fd < 0) {
		return (-1);
	}
	close(fd);
	return (0);
}

//
// saveLocalNodeid()
//
int
saveLocalNodeid(int nodeid)
{
	FILE *fd;
	int rc;
	int rstatus = SCXCFG_OK;

	if (createFile(FARM_CONFIG_DIR, FARM_NODEID_FILE)) {
		rstatus = SCXCFG_ESYSERR;
		goto cleanup;
	}
	fd = fopen(FARM_NODEID_FILE, "w+");
	if (!fd) {
		rstatus = SCXCFG_ESYSERR;
		goto cleanup;
	}
	rc = fprintf(fd, "%d\n", nodeid);
	if (rc <= 0) {
		rstatus = SCXCFG_ESYSERR;
	}
	(void) fclose(fd);
cleanup:
	if (rstatus != SCXCFG_OK) {
		fprintf(stderr, "Cannot save nodeid: %s\n", strerror(errno));
		scxcfg_log(LOG_ERR, "Cannot save nodeid: %s", strerror(errno));
		return (rstatus);
	}
	return (SCXCFG_OK);
}

//
// saveLocalAuthKey()
//
int
saveLocalAuthKey(const char *authKey)
{
	FILE *fd;
	int rc;
	int rstatus = SCXCFG_OK;

	if (createFile(FARM_CONFIG_DIR, FARM_AUTH_KEY_FILE)) {
		rstatus = SCXCFG_ESYSERR;
		goto cleanup;
	}
	fd = fopen(FARM_AUTH_KEY_FILE, "w+");
	if (!fd) {
		rstatus = SCXCFG_ESYSERR;
		goto cleanup;
	}
	rc = fprintf(fd, "%s\n", authKey);
	if (rc <= 0) {
		rstatus = SCXCFG_ESYSERR;
	}
	(void) fclose(fd);
cleanup:
	if (rstatus != SCXCFG_OK) {
		fprintf(stderr, "Cannot save auth key: %s\n", strerror(errno));
		scxcfg_log(LOG_ERR, "Cannot save auth key: %s",
		    strerror(errno));
		return (rstatus);
	}
	return (SCXCFG_OK);
}

//
// saveLocalClusterId()
//
int
saveLocalClusterId(const char *clustId)
{
	FILE *fd;
	int rc;
	int rstatus = SCXCFG_OK;

	if (createFile(FARM_CONFIG_DIR, FARM_CLUSTERID_FILE)) {
		rstatus = SCXCFG_ESYSERR;
		goto cleanup;
	}
	fd = fopen(FARM_CLUSTERID_FILE, "w+");
	if (!fd) {
		rstatus = SCXCFG_ESYSERR;
		goto cleanup;
	}
	rc = fprintf(fd, "%s\n", clustId);
	if (rc <= 0) {
		rstatus = SCXCFG_ESYSERR;
	}
	(void) fclose(fd);
cleanup:
	if (rstatus != SCXCFG_OK) {
		fprintf(stderr, "Cannot save cluster id: %s\n",
		    strerror(errno));
		scxcfg_log(LOG_ERR, "Cannot save cluster id: %s",
		    strerror(errno));
		return (rstatus);
	}
	return (SCXCFG_OK);
}


//
// installFarmNode()
//
// Handle the -i opion of scx command. Install this node as a farm node
// for the given cluster: identified by cluster's name and auth key.
//
// Will retry MAX_TRY times.
//
int
installFarmNode(const  char *clusterName, const char *authKey,
	    char *adapter)
{
	scxcfg_log(LOG_DEBUG, "installFarmNode");
	scxcfg_install_input_t input;
	scxcfg_install_output_t output;
	scxcfg_data_remote *dataAccess;
	char nodeName[FCFG_MAX_LEN];
	int rc = SCXCFG_OK;
	int count = 0;
	const int MAX_TRY = 3;

	if (!scxcfg_isfarm())
		return (SCXCFG_ENOTFARM);

	dataAccess = new scxcfg_data_remote();
	if (!dataAccess) {
		return (SCXCFG_ENOMEM);
	}

	rc = getLocalName(nodeName);
	if (rc != SCXCFG_OK) {
		goto cleanup;
	}
retry:
	count++;
	//
	// The input and ouput are the RPC parameters
	// for scxcfg_install_farm_node routine.
	//
	bzero(&input, sizeof (input));
	bzero(&output, sizeof (output));

	input.node_name = strdup(nodeName);
	input.auth_key = strdup(authKey);
	input.cluster_name = strdup(clusterName);
	input.adap1 = strtok(adapter, ",");
	//	input.adap2 = NULL;
	if (input.adap1 != NULL)
		input.adap2 = strtok(NULL, ",");

	if (input.adap2 == NULL)
		input.adap2 = strdup("");

	output.node_id = 0;

	rc = dataAccess->installFarmNode(&input, &output);

	if (rc != SCXCFG_OK) {
		if (count > MAX_TRY) {
			scxcfg_log(LOG_ERR, "Failed to install farm node:"
			    "installfarm failed: %d\n", rc);
			goto cleanup;
		} else {
			goto retry;
		}
	}
	//
	// Save the minimal config to cluster config dir.
	// nodeid, auth key, clusterid.
	//
	rc = saveLocalNodeid(output.node_id);
	if (rc != SCXCFG_OK)
		goto cleanup;
	rc = saveLocalAuthKey(authKey);
	if (rc != SCXCFG_OK)
		goto cleanup;
	rc = saveLocalClusterId(output.cluster_id);
cleanup:
	if (input.node_name) free(input.node_name);
	if (input.auth_key) free(input.auth_key);
	if (input.cluster_name) free(input.cluster_name);
	if (output.cluster_id) free(output.cluster_id);
	delete dataAccess;
	return (rc);
}


//
// deleteConfig()
//
// Should be used for debug only. Delete the farm configuration in the
// server configuration repository.
//
int
deleteConfig(scxcfg *cfg)
{
	int rc = SCXCFG_OK;
	if (!cfg->handle)
		return (SCXCFG_EINVAL);
	scxcfg_data *dataAccessCCR = (scxcfg_data *)cfg->handle;
	rc = dataAccessCCR->deleteConfig(FARM_TABLE);
	return (rc);
}
