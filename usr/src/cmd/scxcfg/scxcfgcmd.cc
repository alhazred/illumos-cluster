/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scxcfgcmd.cc	1.4	08/05/20 SMI"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <strings.h>
#include <string.h>
#include <libintl.h>
#include <locale.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include <scxcfg/scxcfg.h>
#include "scxcfg_data.h"
#include "scxcfg_int.h"
#include "scxcfgcmd.h"

#include <rgm/sczones.h>

//
// scxcfg  / scx.
//
// Configuration CLI for FARM management support.
//
// The commands is delivered with two paths names.
//
// scxcfg form: supports configuration commands:
//   -g / -s : get / set of properties
//   -G retrieve the farm mangement toggle on server nodes.
//   -S set the farm mangement toggle on server nodes.
//   -n get the local nodeid (server or farm).
//
// scx form: support administrative functions
//   -R retrieve configuration cache for a farm node.
//   -i configure a new farm node. (Called on a farm node).
//
#define	LABELW			50
#define	COLW20			20

#define	STR_TRUE		"true"
#define	STR_FALSE		"false"
#define	STR_ENABLED		"enabled"
#define	STR_DISABLED		"disabled"

static char *progname;

static void usage(FILE *out);

static scxcfg_errno_t
    scxcfgcmd_get(scxcfg *cfg, int argc, char **argv, char **messages,
	int uflag);
static scxcfg_errno_t
    scxcfgcmd_getlist(scxcfg *cfg, int argc, char **argv, char **messages,
	int uflag);
static scxcfg_errno_t
    scxcfgcmd_set(scxcfg *cfg, int argc, char **argv, char **messages,
	int uflag);
static scxcfg_errno_t
    scxcfgcmd_del(scxcfg *cfg, int argc, char **argv, char **messages,
	int uflag);
static scxcfg_errno_t
    scxcfgcmd_install(scxcfg *cfg, int argc, char **argv, char **messages,
	int uflag);
static scxcfg_errno_t
    scxcfgcmd_refresh(scxcfg *cfg, int argc, char **argv, char **messages,
	int uflag);
static scxcfg_errno_t
    scxcfgcmd_delete_config(scxcfg *cfg, int argc, char **argv, char **messages,
	int uflag);
static scxcfg_errno_t
    scxcfgcmd_delete_node(scxcfg *cfg, int argc, char **argv, char **messages,
	int uflag);
static scxcfg_errno_t
    scxcfgcmd_set_europa(scxcfg *cfg, int argc, char **argv, char **messages,
	int uflag);
static scxcfg_errno_t
    scxcfgcmd_get_europa(scxcfg *cfg, int argc, char **argv, char **messages,
	int uflag);
static int get_nodeid();

scxcfg cfg_handle;
int adminMode = 0;

//
// main().
//
int
main(int argc, char **argv)
{
	int c;
	int i, gflg, lflg, sflg, dflg, verbose, otheroptions;
	int iflg, nflg, Dflg, rflg, Rflg, Gflg, Sflg;
	char *stropts;
	scxcfg_errno_t (*scxcfgcmd)(scxcfg *, int, char **, char **messages,
	    int uflag);
	char errbuff[BUFSIZ];
	int exitstatus;
	char *msgbuffer = (char *)0;
	char *msg, *last;
	FILE *out;
	scxcfg_nodeid_t nodeid;
	scxcfg_error err;
	int rc;
	int openLib = 0;

#ifndef linux
	if (sc_zonescheck() != 0)
		return (1);
#endif

	scxcfg_log_set_tag("Cluster.scxcfg.cmd");

	//
	// Ignore the SIGPIPE signal.
	//
	(void) sigset(SIGPIPE, SIG_IGN);

#define	SCXCFGCMD_OPTARGS "l:g:s:d:vnr:fSG"
#define	SCXCMD_OPTARGS "DiC:A:k:R"

	scxcfgcmd = NULL;
	if ((progname = strrchr(argv[0], '/')) == NULL) {
		progname = argv[0];
	} else {
		++progname;
	}

	if (strcmp(progname, "scx") == 0) {
		//
		// Admin command.
		//
		stropts = SCXCMD_OPTARGS;
		adminMode = 1;
	} else {
		//
		// Standard interface get, set.
		//
		stropts = SCXCFGCMD_OPTARGS;
		adminMode = 0;
	}

	(void) setlocale(LC_ALL, "");
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);
	(void) textdomain(TEXT_DOMAIN);

	//
	// Check for basic options and for help flag.
	//
	otheroptions = 0;
	gflg = sflg = dflg = verbose = 0;
	iflg = rflg = lflg = nflg = Dflg = Rflg = Sflg = Gflg =  0;

	openLib = 1;
	while ((c = getopt(argc, argv, stropts)) != EOF) {
		switch (c) {
		case 'g':
			gflg = 1;
			scxcfgcmd = scxcfgcmd_get;
			break;
		case 'l':
			lflg = 1;
			scxcfgcmd = scxcfgcmd_getlist;
			break;
		case 's':
			sflg = 1;
			scxcfgcmd = scxcfgcmd_set;
			break;
		case 'd':
			dflg = 1;
			scxcfgcmd = scxcfgcmd_del;
			break;
		case 'n':
			exit(get_nodeid());
			break;
		case 'f':
			exit(!scxcfg_isfarm());
			break;
		case 'G':
			Gflg = 1;
			scxcfgcmd = scxcfgcmd_get_europa;
			break;
		case 'S':
			Sflg = 1;
			scxcfgcmd = scxcfgcmd_set_europa;
			break;
		case 'i':
			iflg = 1;
			scxcfgcmd = scxcfgcmd_install;
			openLib = 0;
			break;
		case 'R':
			Rflg = 1;
			scxcfgcmd = scxcfgcmd_refresh;
			openLib = 0;
			break;
		case 'D':
			Dflg = 1;
			scxcfgcmd = scxcfgcmd_delete_config;
			openLib = 1;
			break;
		case 'r':
			rflg = 1;
			scxcfgcmd = scxcfgcmd_delete_node;
			openLib = 1;
			break;
		case 'v':
			verbose++;
			break;
		default:
			otheroptions++;
			break;
		}
	}

	//
	// Make sure there are no additional args.
	//
	if (optind != argc) {
		usage(stderr);
		exit(SCXCFG_EUSAGE);
	}

	//
	// Reset optind.
	//
	optind = 1;

	i = sflg + gflg + dflg + rflg + iflg + lflg + Dflg + Rflg + Gflg + Sflg;
	//
	// Only one form of the command allowed.
	//
	if ((i > 1) || (i == 0)) {
		usage(stderr);
		exit(SCXCFG_EUSAGE);
	}

	if (openLib) {
		//
		// Get local nodeid.
		//
		rc = scxcfg_get_local_nodeid(&nodeid, &err);
		if (rc != FCFG_OK) {
			switch (err.suberrno) {
			case SCXCFG_EPERM:
				errbuff[0] = 0;
				(void) fprintf(stderr, "%s:  %s.\n",
				    progname, errbuff);
				exit(SCXCFG_EPERM);
			default:
				(void) fprintf(stderr, gettext(
				    "%s:  This node currently "
				    "has not nodeid.\n"), progname);
				exit(SCXCFG_ENOCLUSTER);
			}
		}
		//
		// Open scxcfg API.
		//
		rc = scxcfg_open(&cfg_handle, &err);
		if (rc != FCFG_OK) {
			errbuff[0] = 0;
			(void) fprintf(stderr, gettext(
			    "%s:  Failed to get access to  configuration\n"),
			progname);
			return (SCXCFG_ESYSERR);
		}
	}
	//
	// Complete check for usage errors, then run the options.
	//
	if ((exitstatus = scxcfgcmd(&cfg_handle, argc, argv,
	    (char **)0, 1)) != 0) {
		if (exitstatus == SCXCFG_EUSAGE)
			usage(stderr);
		exit(exitstatus);
	}
	//
	// Run the command.
	//
	exitstatus = scxcfgcmd(&cfg_handle, argc, argv, &msgbuffer, 0);

	//
	// Dump the messages.
	//
	if (msgbuffer != (char *)0) {
		// If non-zero exitstatus, use stderr, else use stdout.
		out = (exitstatus) ? stderr : stdout;
		// Pre-pend program name to each line.
		msg = strtok_r(msgbuffer, "\n", &last);
		while (msg != NULL) {
			(void) fprintf(out, "%s:    %s\n", progname, msg);
			msg = strtok_r(NULL, "\n", &last);
		}
	}
	if (msgbuffer)
		free(msgbuffer);
	if (openLib) {
		scxcfg_close(&cfg_handle);
	}
	//
	// Done.
	//
	return (exitstatus);
}

//
// usage()
//
//  Print a simple usage message to FILE out.
//
static void
usage(FILE *out)
{
	//
	// Admin form usage.
	//
	if (strcmp(progname, "scx") == 0) {
		(void) fprintf(out, "%s: %s -R\n",
		    gettext("usage"), progname);
		(void) fprintf(out, "%s:  %s  -i -C cname -A adpt -k key\n",
		    gettext("usage"), progname);
		(void) putc('\n', out);
	} else {
		//
		// Standard form usage.
		//
		(void) fprintf(out, "%s:  %s -g|-s|-l\n",
		    gettext("usage"), progname);
		(void) putc('\n', out);
	}
}

//
// scxcfgcmd_get
//
//	The "print" form of the command.
//
//	If "uflag" is set, just check for usage errors.
//
static scxcfg_errno_t
scxcfgcmd_get(scxcfg *cfgh, int argc, char **argv, char **messages, int uflag)
{
	scxcfg_errno_t rstatus = SCXCFG_OK;
	scxcfg_error error;
	char value_buf[FCFG_MAX_LEN];
	char *prop;
	int rc;
	int c;

	while ((c = getopt(argc, argv, "g:")) != EOF) {
		switch (c) {
		case 'g':
			prop = optarg;
			break;
		default:
			rstatus = SCXCFG_EUSAGE;
		}
	}
	//
	// Just check usage?
	//
	if (uflag)
		goto cleanup;

	value_buf[0] = 0;
	rc = scxcfg_getproperty_value(cfgh, NULL, prop, value_buf, &error);
	if (!rc) {
		printf("%s\n", value_buf);
	} else {
		fprintf(stderr, "Error getting property: %d\n", rc);
		rstatus = (scxcfg_errno_t)error.scxcfg_errno;
	}
cleanup:
	if (rstatus == SCXCFG_EUSAGE) {
		(void) putc('\n', stderr);
		usage(stderr);
	}
	return (rstatus);
}

//
// scxcfgcmd_getlist()
//
// Get the immediate "children" of the passed subproperty in the config tree.
// eg: scxcfg -l farm.nodes if ther are two farm nodes 10 and 11.
// returns:
//	10
//	11
//
static scxcfg_errno_t
scxcfgcmd_getlist(scxcfg *cfgh, int argc, char **argv, char **messages,
    int uflag)
{
	scxcfg_errno_t rstatus = SCXCFG_OK;
	f_property_t *lvalues = NULL;
	f_property_t *v;
	scxcfg_error error;
	char *prop;
	int rc, num, i;
	int c;

	while ((c = getopt(argc, argv, "l:")) != EOF) {
		switch (c) {
		case 'l':
			prop = optarg;
			break;
		default:
			rstatus = SCXCFG_EUSAGE;
		}
	}
	//
	// Just check usage?
	//
	if (uflag)
		goto cleanup;

	rc = scxcfg_getlistproperty_value(cfgh, NULL,
	    prop, &lvalues, &num, &error);
	if (!rc) {
		v = lvalues;
		for (i = 0; i < num; i++, v = v->next) {
			if (!v) {
				fprintf(stderr, "Internal error \n");
				rstatus = SCXCFG_EUNEXPECTED;
			}
			printf("%s\n", v->value);
		}
	} else {
		fprintf(stderr, "Error getting property list: %d\n", rc);
		rstatus = (scxcfg_errno_t)error.scxcfg_errno;
	}
	if (lvalues)
		scxcfg_freelist(lvalues);
cleanup:
	if (rstatus == SCXCFG_EUSAGE) {
		(void) putc('\n', stderr);
		usage(stderr);
	}
	return (rstatus);
}

//
// scxcfgcmd_set()
//
//	If "uflag" is set, just check for usage errors.
//
static scxcfg_errno_t
scxcfgcmd_set(scxcfg *cfgh, int argc, char **argv, char **messages, int uflag)
{
	scxcfg_errno_t rstatus = SCXCFG_OK;
	scxcfg_error error;
	char *propVal = NULL, *val, *prop;
	int rc;
	int c;

	optind = 1;
	while ((c = getopt(argc, argv, "s:")) != EOF) {
		switch (c) {
		case 's':
			if (strlen(optarg) >= FCFG_MAX_LEN) {
				rstatus = SCXCFG_ERANGE;
			}

			if ((propVal = strdup(optarg)) == NULL) {
				rstatus = SCXCFG_ENOMEM;
				goto cleanup;
			}
			prop = strtok(propVal, "=");
			if ((val = strtok(NULL, "=")) == NULL) {
				rstatus = SCXCFG_EUSAGE;
			}
			break;
		default:
			rstatus = SCXCFG_EUSAGE;
		}
	}
	//
	// Just check usage?
	//
	if (uflag)
		goto cleanup;

	rc = scxcfg_setproperty_value(cfgh, NULL, prop, val, &error);
	if (!rc) {
		printf("%s: %s = %s  Ok\n", progname, prop, val);
	} else {
		rstatus = (scxcfg_errno_t)error.scxcfg_errno;
		fprintf(stderr, "Error setting property: %d\n", rc);
		fprintf(stderr, "Internal errno: %d\n", error.scxcfg_errno);
		fprintf(stderr, "Internal suberrno: %d\n", error.suberrno);
	}
cleanup:
	if (propVal)
		free(propVal);
	if (rstatus == SCXCFG_EUSAGE) {
		(void) putc('\n', stderr);
		usage(stderr);
	}
	return (rstatus);
}

//
// scxcfgcmd_del()
//
//
static scxcfg_errno_t
scxcfgcmd_del(scxcfg *cfgh, int argc, char **argv, char **messages, int uflag)
{
	scxcfg_errno_t rstatus = SCXCFG_OK;
	scxcfg_error error;
	char *prop;
	int rc;
	int c;

	optind = 1;
	while ((c = getopt(argc, argv, "d:")) != EOF) {
		switch (c) {
		case 'd':
			prop = optarg;
			break;
		default:
			rstatus = SCXCFG_EUSAGE;
		}
	}
	//
	// Just check usage?
	//
	if (uflag)
		goto cleanup;
	rc = scxcfg_delproperty(cfgh, NULL, prop, &error);
	if (rc) {
		fprintf(stderr, "Error deleting property: %d\n", rc);
		rstatus = (scxcfg_errno_t)error.scxcfg_errno;
	}
cleanup:
	if (rstatus == SCXCFG_EUSAGE) {
		(void) putc('\n', stderr);
		usage(stderr);
	}
	return (rstatus);
}

//
// get_nodeid()
//
// Utility function. Get local nodeid.
//
static int
get_nodeid()
{
	int rc;
	scxcfg_error error;
	scxcfg_nodeid_t nodeId;
	rc = scxcfg_get_local_nodeid(&nodeId, &error);
	if (rc == FCFG_OK) {
		printf("%d\n", nodeId);
		return (SCXCFG_OK);
	}
	return (SCXCFG_ENODEID);
}

//
// scxcfgcmd_install()
//
static scxcfg_errno_t
scxcfgcmd_install(scxcfg *cfg, int argc, char **argv, char **messages,
	int uflag)
{
	scxcfg_errno_t rstatus = SCXCFG_OK;
	scxcfg_error error;
	char *aName;
	char *cName;
	char *authKey;
	int C_flg, k_flg, A_flg;
	int c;

	C_flg = A_flg = k_flg = 0;

	optind = 1;
	while ((c = getopt(argc, argv, "iC:A:k:")) != EOF) {
		switch (c) {
		case 'C':
			cName = optarg;
			C_flg++;
			break;
		case 'A':
			aName = optarg;
			A_flg++;
			break;
		case 'k':
			authKey = optarg;
			k_flg++;
			break;
		case 'i':
			break;

		default:
			rstatus = SCXCFG_EUSAGE;
		}
	}
	//
	// All parameters required.
	//
	if ((C_flg != 1) || (k_flg != 1) || (A_flg != 1)) {
		rstatus = SCXCFG_EUSAGE;
	}
	//
	// Just check usage?
	//
	if (uflag)
		goto cleanup;

	rstatus = (scxcfg_errno_t)installFarmNode(cName, authKey, aName);

	if (rstatus != SCXCFG_OK) {
		fprintf(stderr, "Error installing farm node: %d\n", rstatus);
	}
cleanup:
	if (rstatus == SCXCFG_EUSAGE) {
		(void) putc('\n', stderr);
		usage(stderr);
	}
	return (rstatus);
}

//
// scxcfgcmd_delete_config()
//
static scxcfg_errno_t
scxcfgcmd_delete_config(scxcfg *cfg, int argc, char **argv, char **messages,
	int uflag)
{
	scxcfg_errno_t rstatus = SCXCFG_OK;
	char answer[1024];
	//
	// Just check usage?
	//
	if (uflag)
		goto cleanup;

	printf("You are about to delete the whole farm. Confirm\n");
	printf("Do you want to continue ?");
	gets(answer);
	if (answer[0] == 'y' || answer[0] == 'Y') {
		rstatus = (scxcfg_errno_t)deleteConfig(cfg);
	} else {
		return (SCXCFG_EUABORT);
	}
	if (rstatus != SCXCFG_OK) {
		fprintf(stderr, "Error when deleting config: %d\n", rstatus);
	}
cleanup:
	return (rstatus);
}

//
// scxcfgcmd_delete_node()
// (-r option)
//
static scxcfg_errno_t
scxcfgcmd_delete_node(scxcfg *cfg, int argc, char **argv, char **messages,
	int uflag)
{
	scxcfg_errno_t rstatus = SCXCFG_OK;
	scxcfg_error error;
	char *nodeName;
	scxcfg_nodeid_t i;
	int rc;
	int c;

	while ((c = getopt(argc, argv, "r:")) != EOF) {
		switch (c) {
		case 'r':
			nodeName = optarg;
			break;
		default:
			rstatus = SCXCFG_EUSAGE;
		}
	}
	//
	// Just check usage?
	//
	if (uflag)
		goto cleanup;

	//
	// The -r option can be used to:
	//  - remove a  farm node from the config
	//  - remove a sutree in the config tree
	// If the parameter is a valid nodename, then we assume
	// a node is to be removed, otherwise we use the param
	// as a node in the config tree.
	//
	if (scxcfg_get_nodeid(cfg, nodeName, &i, &error) == FCFG_OK) {
		rc = scxcfg_del_node(cfg, nodeName, &error);
	} else {
		rc = scxcfg_del_subtree(cfg, nodeName, &error);
	}
	if (rc != FCFG_OK) {
		fprintf(stderr, "Error deleting node: %d\n", rc);
		rstatus = (scxcfg_errno_t)error.scxcfg_errno;
	}
cleanup:
	if (rstatus == SCXCFG_EUSAGE) {
		(void) putc('\n', stderr);
		usage(stderr);
	}
	return (rstatus);
}

static scxcfg_errno_t
scxcfgcmd_refresh(scxcfg *cfg, int argc, char **argv, char **messages,
	int uflag)
{
	scxcfg_errno_t rstatus = SCXCFG_OK;
	//
	// Just check usage?
	//
	if (uflag)
		goto cleanup;

	rstatus = (scxcfg_errno_t)refreshFileCache();

	if (rstatus != SCXCFG_OK) {
		fprintf(stderr, "Error when refresing file cache: %d\n",
		    rstatus);
	}
cleanup:
	return (rstatus);
}

//
// scxcfgcmd_set_europa()
//
// Toggle of farm mangement support in sun cluster.
// This function set or unset the toggle in the Sun Cluster
// infrastructure table.
//
static scxcfg_errno_t
scxcfgcmd_set_europa(scxcfg *cfg, int argc, char **argv, char **messages,
	int uflag)
{
	scxcfg_data *dataAccess;
	char value[FCFG_MAX_LEN];
	int rc;
	if (uflag) {
		if (scxcfg_isfarm())
			return (SCXCFG_EUSAGE);
		else
			return (SCXCFG_OK);
	}
	dataAccess = (scxcfg_data *)cfg->handle;
	value[0] = 0;
	//
	// Get the value of "cluster.properties.type".
	//
	rc = dataAccess->getProperty(INFR_TABLE,
	    "cluster.properties.type", value);
	//
	// If the property exist, change it.
	//
	if (rc == SCXCFG_OK) {
		if (strcmp(value, "europa") == 0)
			strcpy(value, "sc");
		else
			strcpy(value, "europa");
	} else if (rc == SCXCFG_ENOEXIST) {
		//
		// Property does not exist, set farm
		// mangement, "europa".
		//
		strcpy(value, "europa");
	} else {
		//
		// Fatal error.
		//
		fprintf(stderr, "Error getting farm management support\n", rc);
		return ((scxcfg_errno_t)rc);
	}
	//
	// Set the property.
	//
	rc = dataAccess->setProperty(INFR_TABLE,
	    "cluster.properties.type", value);
	return ((scxcfg_errno_t)rc);
}

//
// scxcfgcmd_get_europa()
//
// Get the value of farm management support in sun cluster.
// This gets the toggle from  the Sun Cluster infrastructure table.
//
static scxcfg_errno_t
scxcfgcmd_get_europa(scxcfg *cfg, int argc, char **argv, char **messages,
	int uflag)
{
	scxcfg_data *dataAccess;
	char value[FCFG_MAX_LEN];
	int exit_val = 1;
	int rc;
	if (uflag) {
		if (scxcfg_isfarm())
			return (SCXCFG_EUSAGE);
		else
			return (SCXCFG_OK);
	}
	dataAccess = (scxcfg_data *)cfg->handle;
	value[0] = 0;
	rc = dataAccess->getProperty(INFR_TABLE,
	    "cluster.properties.type", value);
	if (rc == SCXCFG_OK)
		if (strcmp(value, "europa") == 0)
			exit_val = 0;
	scxcfg_close(cfg);
	exit(exit_val);
	return ((scxcfg_errno_t)rc);
}
