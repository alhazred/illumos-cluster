/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scprt.c	1.2	08/05/20 SMI"

/*
 * scprt
 *
 *	This private command is used to print the localized help text
 *	in scinstall, clsetup, and scsetup. It prints the text in a line
 *	which has a maximum length limit, and wrap it into the next line
 *	if it is longer than the limit. A word is a chunk of chars that
 *	is separated by spaces.
 *
 *	For single char text, it wraps the whole word into the next
 *	line when it is beyond the maximum length, unless the length of
 *	the word itself is longer than the maximum length in which case
 *	it is not wrapped and add "-" at the end of the line.
 *
 *	For wide char text, it wraps it to next line when it is beyond
 *	the limit, and don't add "-". If the text is mixed with single
 *	chars, it adds "-" if the char at the position of the limit is
 *	single char.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <strings.h>
#include <string.h>
#include <wchar.h>
#include <locale.h>

#include <sys/types.h>

static const char *progname;

static void usage(FILE *out);

/*
 * usage
 *
 *	Print a simple usage message to stderr.
 */
static void
usage(FILE *out)
{
	(void) fprintf(out, "%s:  %s <leading_spaces> <line_length> <text>\n",
	    gettext("Usage"),
	    progname);

	(void) putc('\n', out);
}

/*
 * main
 */
int
main(int argc, char **argv)
{
	int lead_length;
	int line_length;
	int i;
	uint_t word_len, thisline_len;
	uint_t tmp_len;
	int cut_len;
	wchar_t *wthis_line = NULL;
	wchar_t *wthis_word;
	wchar_t *word_p, *wline_ptr;
	boolean_t start_line = B_TRUE;
	boolean_t the_end = B_FALSE;
	boolean_t the_start = B_TRUE;
	boolean_t has_widechar = B_FALSE;
	uint_t *display_len = NULL;
	int w_index = 0;

	/* Set the program name */
	if ((progname = strrchr(argv[0], '/')) == NULL) {
		progname = argv[0];
	} else {
		++progname;
	}

	/* I18N housekeeping */
	(void) setlocale(LC_ALL, "");
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);
	(void) textdomain(TEXT_DOMAIN);

	/* Check for parameters */
	if (argc != 4) {
		usage(stderr);
		exit(1);
	}
	if (((lead_length = atoi(argv[1])) == 0) ||
	    ((line_length = atoi(argv[2])) == 0)) {
		usage(stderr);
		exit(1);
	}

	/* Go through the text */
	thisline_len = 0;

	/* Convert the multibyte string to wide char form */
	tmp_len = strlen(argv[3]) + 1;
	wthis_line = (wchar_t *)malloc(tmp_len * sizeof (wchar_t));
	if (!wthis_line) {
		exit(1);
	}
	if (mbstowcs(wthis_line, argv[3], tmp_len) == (size_t)-1) {
		exit(1);
	}

	/* Keep the pointer */
	wline_ptr = wthis_line;

	/* Allocate memory to save display length of each char */
	display_len = (uint_t *)malloc(tmp_len * sizeof (int));
	if (!display_len) {
		exit(1);
	}

	/* this_line = argv[3]; */
	while (*wthis_line) {
		/* Get the word */
		wthis_word = wthis_line;
		word_p = wthis_word;
		word_len = 0;
		i = 0;
		has_widechar = B_FALSE;
		w_index = 0;
		while (*wthis_line && *wthis_line != ' ' &&
		    *wthis_line != '\t') {
			tmp_len = (uint_t)wcwidth(*wthis_line++);
			word_len += tmp_len;
			display_len[i++] = tmp_len;

			/* Wide char? */
			if (!has_widechar && (tmp_len > 1)) {
				has_widechar = B_TRUE;
			}
		}

		if (!*wthis_line) {
			the_end = B_TRUE;
		} else {
			*wthis_line = '\0';
			wthis_line++;
		}

		/* Process this word */
		while (word_len) {
			/* Caculate the length */
			if (start_line) {
				thisline_len = 0;
				tmp_len = word_len;
				if (!the_start) {
					(void) printf("\n");
				}

				/* Print the leading spaces */
				for (i = 0; i < lead_length; i++) {
					(void) putchar(' ');
				}
			} else {
				tmp_len = thisline_len + word_len + 1;
				(void) putchar(' ');
			}

			/* Check the length */
			if ((int)tmp_len <= line_length) {
				/* Print the word */
				while (*wthis_word) {
					(void) putwchar(*wthis_word++);
				}

				if (start_line) {
					start_line = B_FALSE;
				}

				thisline_len = tmp_len;
				if (tmp_len == (uint_t)line_length) {
					start_line = B_TRUE;
				}

				/*
				 * Word is printed so its not the very
				 * beginning
				 */
				if (the_start) {
					the_start = B_FALSE;
				}
				break;
			}

			/*
			 * Process long word. For long single char word or
			 * single chars mixed with wide chars, we need to
			 * add "-" following the single char at the end of
			 * the line, so "line_length - 1". We don't add "-"
			 * if it is wide char at the end of the line.
			 */
			if (start_line) {
				cut_len = line_length;
			} else {
				cut_len = (int)((uint_t)line_length -
				    thisline_len)  - 1;
			}
			if (cut_len < 0) {
				/* Right on the edge, start a new line */
				start_line = B_TRUE;
				continue;
			}

			/* Check if it is long word */
			if (!has_widechar) {
				/*
				 * Continue to display if the word length
				 * is longer than the line limit; But if not
				 * we start a new line.
				 */
				if (word_len <= (uint_t)line_length) {
					start_line = B_TRUE;
					continue;
				}
			}

			/* Print the word */
			tmp_len = 0;
			for (i = 0; i < cut_len; i++) {
				/* Check if the display width is too long. */
				tmp_len += display_len[w_index++];
				if ((int)tmp_len < cut_len) {
					(void) putwchar(wthis_word[i]);
					continue;
				}

				/* Too long. Wrap to the next line. */
				if (display_len[w_index] == 1) {
					if (word_len > (uint_t)line_length) {
						(void) putchar('-');
					}
					w_index--;
					cut_len  = (int)(tmp_len -
					    display_len[w_index]);
				} else if ((int)tmp_len == cut_len) {
					(void) putwchar(wthis_word[i]);
				} else {
					w_index--;
					cut_len  = (int)(tmp_len -
					    display_len[w_index]);
				}
				start_line = B_TRUE;
				break;
			}

			/* Reset the word */
			word_len -= (uint_t)cut_len;
			wthis_word = word_p + w_index;
			thisline_len += (uint_t)cut_len + 1;

			/* Word is printed so it's not the very beginning */
			if (the_start) {
				the_start = B_FALSE;
			}
		}

		/* Done. */
		if (the_end) {
			break;
		}
	}

	(void) putchar('\n');

	/* Free memory */
	free(wline_ptr);
	free(display_len);

	return (0);
} /*lint !e818 */
