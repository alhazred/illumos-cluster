#!/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident	"@(#)zc_halt.ksh	1.6	08/10/13 SMI"

# Input arguments are:
# $1	Cluster brand zone name
# $2	Cluster brand zone root

#
# Exit if we are not booted in cluster mode.
#
/usr/sbin/clinfo > /dev/null 2>&1
if [ $? != 0 ] ; then
	exit 0
fi

# It executes list of required callbacks while zone cluster node is leaving
# the cluster.
error=0

# Callback for evacuation of resource groups for this zone of the zone cluster
/usr/cluster/lib/sc/zc_rgm_halt $1
if [ $? -ne 0 ]; then
	error=1
fi

# Callback to switchover the MDS server(s) that are hosted on this zone path.
# This should be invoked before ZCMM reconfiguration which marks the node
# as dead, to ensures that switchovers will be completed successfully.
/usr/cluster/lib/sc/zc_sqfs_halt $1 $2
if [ $? -ne 0 ]; then
	error=1
fi

exit $error
