//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)zc_helper.cc	1.17	09/03/31 SMI"

//
// This program serves two puposes: as a "helper" program for
// clzonecluster(1CL) and as the zoneadm(1M) handler program for "cluster"
// branded zones. clzonecluster(1CL) invokes the program to create, modify,
// and delete the Solaris zone components of a zone cluster on the physical
// cluster nodes. clzonecluster also uses the program to create the sysidcfg(4)
// file of a newly installed zone component.
//
// This program is also invoked by zoneadm(1M) to check that the intended
// zoneadm operation is allowed for the cluster branded zone; the program may
// perform any cluster brand specific actions for the zoneadm subcommand as
// well.
//
// This is a Sun Cluster private command, and as such must not be directly
// executed by the user.
//

#include <zone.h>
#include <libintl.h>
#include <locale.h>
#include <assert.h>
#include <signal.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <libzonecfg.h>

#include <libzccfg/libzccfg.h>
#include <nslib/ns.h>
#include <sys/clconf_int.h>
#include <rgm/rgm_scrgadm.h>
#include <rgm/rgm_util.h>
#include <scha.h>

#define	ZONECFG_CMD	"/usr/sbin/zonecfg"
#define	TMP_CMD_FILE	"/tmp/zonecfg_cmd_file.XXXXXX"

// Location of sysidcfg file relative to the zonepath.
#define	SYSID_FILE	"root/etc/sysidcfg"

char zone[ZONENAME_MAX];
char my_nodename[SYS_NMLN];
char *execname;
zc_dochandle_t handle; // zone cluster handle

#define	CMD_CREATE	0
#define	CMD_DELETE	1
#define	CMD_MODIFY	2
#define	CMD_SYSIDCFG	4
#define	CMD_SET		5
#define	CMD_END		6
#define	CMD_COMMIT	7
#define	CMD_INSTALL	8
#define	CMD_UNINSTALL	9
#define	CMD_BOOT	10
#define	CMD_REBOOT	11
#define	CMD_HALT	12
#define	CMD_MOVE	13
#define	CMD_CLONE	14
#define	CMD_MARK	15
#define	CMD_ATTACH	16
#define	CMD_DETACH	17
#define	CMD_LIST	18
#define	CMD_READY	19
#define	CMD_VERIFY	20
#define	CMD_STATUS	21
#define	CMD_CHECK_EXIST	22

//
// Undocumented commands in zoneadm(1M). Mount and unmount commands are
// used while a system is being upgraded. Apply command is used to apply global
// zone rctl settings to non-global zones.
//
#define	CMD_MOUNT	23
#define	CMD_UNMOUNT	24
#define	CMD_APPLY	25

#define	CMD_MIN		CMD_CREATE
#define	CMD_MAX		CMD_APPLY

struct cmd {
	uint_t		cmd_num;
	const char	*cmd_name;
};

// "Helper" functions for clzonecluster(1CL).
static int create_func(void);
static int delete_func(void);
static int modify_func(void);
static int sysidcfg_func(void);
static int status_func(void);

//
// Functions to handle zoneadm(1M) subcommands. Notice they have a different
// signature from the "helper" functions above.
//
static int install_func(int argc, char *argv[]);
static int uninstall_func(int argc, char *argv[]);
static int boot_func(int argc, char *argv[]);
static int reboot_func(int argc, char *argv[]);
static int halt_func(int argc, char *argv[]);
static int move_func(int argc, char *argv[]);
static int clone_func(int argc, char *argv[]);
static int mark_func(int argc, char *argv[]);
static int attach_func(int argc, char *argv[]);
static int detach_func(int argc, char *argv[]);
static int list_func(int argc, char *argv[]);
static int ready_func(int argc, char *argv[]);
static int verify_func(int argc, char *argv[]);
static int mount_func(int argc, char *argv[]);
static int unmount_func(int argc, char *argv[]);
static int apply_func(int argc, char *argv[]);
static int check_if_zone_exists(void);

static struct cmd cmdtab[] = {
	{ CMD_CREATE, 		"create" },
	{ CMD_DELETE,		"delete" },
	{ CMD_MODIFY, 		"modify" },
	{ CMD_SYSIDCFG,		"sysidcfg" },
	{ CMD_STATUS,		"status" },
	{ CMD_SET,		"set" },
	{ CMD_END,		"end" },
	{ CMD_COMMIT,		"commit" },
	{ CMD_INSTALL,		"install" },
	{ CMD_UNINSTALL,	"uninstall" },
	{ CMD_BOOT,		"boot" },
	{ CMD_REBOOT,		"reboot" },
	{ CMD_HALT,		"halt" },
	{ CMD_MOVE,		"move" },
	{ CMD_CLONE,		"clone" },
	{ CMD_MARK,		"mark" },
	{ CMD_ATTACH,		"attach" },
	{ CMD_DETACH,		"detach" },
	{ CMD_LIST,		"list" },
	{ CMD_READY,		"ready" },
	{ CMD_VERIFY,		"verify" },
	{ CMD_MOUNT,		"mount" },
	{ CMD_UNMOUNT,		"verify" },
	{ CMD_APPLY,		"apply" },
	{ CMD_CHECK_EXIST,	"exists"}
};

static char *
get_execbasename(char *execfullname)
{
	char *last_slash, *execbasename;

	// guard against '/' at end of command invocation
	for (;;) {
		last_slash = strrchr(execfullname, '/');
		if (last_slash == NULL) {
			execbasename = execfullname;
			break;
		} else {
			execbasename = last_slash + 1;
			if (*execbasename == '\0') {
				*last_slash = '\0';
				continue;
			}
			break;
		}
	}
	return (execbasename);
}

static void
usage()
{
	(void) printf(gettext("%s <zonename> <command>.\n"), execname);
}

//
// Returns: CMD_MIN thru CMD_MAX on success, -1 on error
//
static int
cmd_match(const char *cmdp)
{
	int i;

	for (i = CMD_MIN; i < CMD_MAX; i++) {
		// return only if there is an exact match
		if (strcmp(cmdp, cmdtab[i].cmd_name) == 0) {
			return (cmdtab[i].cmd_num);
		}
	}
	return (-1);
}

static int
initialize()
{
	int err;

	//
	// We must be a cluster node before we can proceed.
	//
	if (clconf_lib_init() != 0) {
		(void) printf(gettext("Failed to initialize the ORB.\n"));
		return (ZC_NO_CLUSTER);
	}

	if ((handle = zccfg_init_handle()) == NULL) {
		(void) printf(gettext("%s: failed to init handle.\n"),
		    execname);
		return (ZC_NOMEM);
	}

	if ((err = zccfg_get_handle(zone, handle)) != ZC_OK) {
		(void) printf(gettext("%s: failed to get handle for %s.\n"),
		    execname, zone);
		return (err);
	}

	return (ZC_OK);
}

static int
add_ipds(FILE *fp, zone_state_t state)
{
	int err;
	struct zc_fstab fstab;

	// Nothing to do if zone is already installed.
	if (state >= ZONE_STATE_INSTALLED) {
		return (ZC_OK);
	}

	//
	// Get inherited-pkg-dir's.
	//
	if ((err = zccfg_setipdent(handle)) != ZC_OK) {
		return (err);
	}

	while (zccfg_getipdent(handle, &fstab) == ZC_OK) {
		(void) fprintf(fp, "add inherit-pkg-dir\n");
		(void) fprintf(fp, "set dir=%s\n", fstab.zc_fs_dir);
		(void) fprintf(fp, "end\n");
	}
	(void) zccfg_endipdent(handle);
	return (ZC_OK);
}

//
// Add global-scope lofs file systems to the zone configuration.
//
static int
add_lofs_filesystems(FILE *fp)
{
	int err;
	struct zc_fstab fstab = {"", "", "", NULL, ""};

	if ((err = zccfg_setfsent(handle)) != ZC_OK) {
		return (err);
	}

	while (zccfg_getfsent(handle, &fstab) == ZC_OK) {
		zc_fsopt_t *optp;
		//
		// Nothing to do if fs type is not lofs.
		//
		if (strcmp(fstab.zc_fs_type, "lofs") != 0) {
			zccfg_free_fs_option_list(fstab.zc_fs_options);
			continue;
		}

		(void) fprintf(fp, "add fs\n");
		(void) fprintf(fp, "set dir=%s\n", fstab.zc_fs_dir);
		(void) fprintf(fp, "set special=%s\n", fstab.zc_fs_special);
		(void) fprintf(fp, "set type=%s\n", fstab.zc_fs_type);
		for (optp = fstab.zc_fs_options; optp != NULL;
		    optp = optp->zc_fsopt_next) {
			//
			// Take care of embedded equal signs.
			//
			if (strchr(optp->zc_fsopt_opt, '=')) {
				(void) fprintf(fp, "add options \"%s\"\n",
				    optp->zc_fsopt_opt);
			} else {
				(void) fprintf(fp, "add options %s\n",
				    optp->zc_fsopt_opt);
			}
		}
		(void) fprintf(fp, "end\n");
		zccfg_free_fs_option_list(fstab.zc_fs_options);
	}
	(void) zccfg_endfsent(handle);
	return (ZC_OK);
}

static int
add_rctls(FILE *fp)
{
	int err;
	struct zc_rctltab rctltab;
	struct zc_rctlvaltab *valtabp = NULL;

	//
	// Get rctl's.
	//
	if ((err = zccfg_setrctlent(handle)) != ZC_OK) {
		return (err);
	}

	while (zccfg_getrctlent(handle, &rctltab) == ZC_OK) {

		(void) fprintf(fp, "add rctl\n");
		(void) fprintf(fp, "set name=%s\n", rctltab.zc_rctl_name);

		valtabp = rctltab.zc_rctl_valp;
		while (valtabp) {
			(void) fprintf(fp, "add value (priv=%s,limit=%s,"
			    "action=%s)\n", valtabp->zc_rctlval_priv,
			    valtabp->zc_rctlval_limit,
			    valtabp->zc_rctlval_action);
			valtabp = valtabp->zc_rctlval_next;
		}
		(void) fprintf(fp, "end\n");
	}
	(void) zccfg_endrctlent(handle);
	return (ZC_OK);
}
//
// Add capped memory resource to the configuration. Due to the way
// capped-memory support is implemented in Solaris zones, we need to first
// check whether or not zone.max-locked-memory or zone.max-swap resource
// controls have been added by the call to add_rctls(). If either (or both) of
// these rctls have been specified, we use the "select" command to modify the
// capped-memory and set the physical memory cap. Otherwise we add a new
// capped-memory resource and set its physical memory cap property.
//
static int
add_mcap(FILE *fp)
{
	int err;
	struct zc_mcaptab mcaptab;
	struct zc_rctltab rctltab;
	bool found = false; // flags presence of mcap or related rctl's.

	// Get mcap
	err = zccfg_getmcapent(handle, &mcaptab);
	if (err != ZC_OK && err == ZC_NO_ENTRY) {
		return (ZC_OK);
	}

	//
	// Check for the presence of zone.max-locked-memory or
	// zone.max-swap rctls.
	//
	if ((err = zccfg_setrctlent(handle)) != ZC_OK) {
		return (err);
	}

	while (zccfg_getrctlent(handle, &rctltab) == ZC_OK) {
		if (strcmp(rctltab.zc_rctl_name, "zone.max-swap") == 0 ||
		    strcmp(rctltab.zc_rctl_name,
		    "zone.max-locked-memory") == 0) {
			found = true;
			break;
		}
	}
	(void) zccfg_endrctlent(handle);

	if (found) {
		(void) fprintf(fp, "select capped-memory\n");
	} else {
		(void) fprintf(fp, "add capped-memory\n");
	}
	(void) fprintf(fp, "set physical=%s\n", mcaptab.zc_physmem_cap);
	(void) fprintf(fp, "end\n");
	return (ZC_OK);
}

//
// Convenience function to get processor set resource from the CCR and add it
// to the zonecfg command file.
//
static int
add_pset(FILE *fp)
{
	int err;
	struct zc_psettab psettab;

	// Get pset
	err = zccfg_getpsetent(handle, &psettab);
	if (err != ZC_OK && err == ZC_NO_ENTRY) {
		return (ZC_OK);
	}

	(void) fprintf(fp, "add dedicated-cpu\n");
	(void) fprintf(fp, "set ncpus=%s-%s\n", psettab.zc_ncpu_min,
	    psettab.zc_ncpu_max);

	// 'importance' is optional.
	if (psettab.zc_importance[0] != '\0') {
		(void) fprintf(fp, "set importance=%s\n",
		    psettab.zc_importance);
	}
	(void) fprintf(fp, "end\n");
	return (ZC_OK);
}

static int
add_devices(FILE *fp)
{
	int err;
	struct zc_devtab devtab;

	if ((err = zccfg_setdevent(handle)) != ZC_OK) {
		(void) printf(gettext("%s: setdevent error %d\n"),
		    execname, err);
		return (err);
	}

	while (zccfg_getdevent(handle, &devtab) == ZC_OK) {
		(void) fprintf(fp, "add device\n");
		(void) fprintf(fp, "set match=%s\n", devtab.zc_dev_match);
		(void) fprintf(fp, "end\n");
	}

	return (ZC_OK);
}

static void
add_node_local_nets(FILE *fp, const struct zc_nodetab *tabp)
{
	zc_nwifelem_t *listp; // tmp pointer to walk the list
	//
	// Get node-local network resources.
	//
	// TODO handle exclusive IP type zones (no address property)
	//
	for (listp = tabp->zc_nwif_listp; listp; listp = listp->next) {
		(void) fprintf(fp, "add net\n");
		(void) fprintf(fp, "set address=%s\n",
		    listp->elem.zc_nwif_address);
		(void) fprintf(fp, "set physical=%s\n",
		    listp->elem.zc_nwif_physical);
		if (strlen(listp->elem.zc_nwif_defrouter) > 0) {
			(void) fprintf(fp, "set defrouter=%s\n",
			    listp->elem.zc_nwif_defrouter);
		}
		(void) fprintf(fp, "end\n");
	}
}

static int
add_node_local_res(FILE *fp)
{
	bool found = false; // this node is in the nodelist?
	int err;
	struct zc_nodetab nodetab;

	//
	// Get node-local resources.
	//
	if ((err = zccfg_setnodeent(handle)) != ZC_OK) {
		return (err);
	}
	while (zccfg_getnodeent(handle, &nodetab) == ZC_OK) {
		if (strcmp(my_nodename, nodetab.zc_nodename) == 0) {
			found = true;
			break;
		}
	}

	(void) zccfg_endnodeent(handle);
	assert(found);

	add_node_local_nets(fp, &nodetab);

	return (ZC_OK);
}

//
// Remove all existing resources in the zone. create_func() will add them back.
// We ignore return values from the delete function calls since it's possible
// that some of the resources we're deleting may not actually be in the
// configuration.
//
void
clear_existing_resources(zone_state_t state)
{
	zone_dochandle_t zone_handle;
	int err;

	if ((zone_handle = zonecfg_init_handle()) == NULL) {
		exit(ZC_NOMEM);
	}

	if (zonecfg_get_handle(zone, zone_handle) != Z_OK) {
		zonecfg_fini_handle(zone_handle);
		return; // nothing to remove
	}

	// Expand the list if/when new zone resources are added.
	if (state < ZONE_STATE_INSTALLED) {
		(void) zonecfg_del_all_resources(zone_handle,
		    "inherit-pkg-dir");
	}
	(void) zonecfg_del_all_resources(zone_handle, "fs");
	(void) zonecfg_del_all_resources(zone_handle, "net");
	(void) zonecfg_del_all_resources(zone_handle, "rctl");
	(void) zonecfg_del_all_resources(zone_handle, "device");
	(void) zonecfg_del_all_resources(zone_handle, "dataset");
	(void) zonecfg_rm_aliased_rctl(zone_handle, ALIAS_MAXLWPS);
	(void) zonecfg_rm_aliased_rctl(zone_handle, ALIAS_MAXSHMMEM);
	(void) zonecfg_rm_aliased_rctl(zone_handle, ALIAS_MAXSHMIDS);
	(void) zonecfg_rm_aliased_rctl(zone_handle, ALIAS_MAXMSGIDS);
	(void) zonecfg_rm_aliased_rctl(zone_handle, ALIAS_MAXSEMIDS);
	(void) zonecfg_rm_aliased_rctl(zone_handle, ALIAS_MAXLOCKEDMEM);
	(void) zonecfg_rm_aliased_rctl(zone_handle, ALIAS_MAXSWAP);
	(void) zonecfg_rm_aliased_rctl(zone_handle, ALIAS_SHARES);
	(void) zonecfg_rm_aliased_rctl(zone_handle, ALIAS_CPUCAP);
	(void) zonecfg_delete_pset(zone_handle);
	(void) zonecfg_delete_mcap(zone_handle);

	// Very bad. Exit the program.
	// TODO We should be syslog-ing this kind of error
	if ((err = zonecfg_save(zone_handle)) != Z_OK) {
		zonecfg_fini_handle(zone_handle);
		exit(err);
	}
	zonecfg_fini_handle(zone_handle);
}

//
// create_func() does two things:
//
// 1. Creates a new Solaris zone, or
// 2. Modifies the configuration of an existing Solaris zone.
//
// create_func() calls zonecfg(1M) to configure the zone. First a temp file is
// created and opened for writing. Then we parse the CZ handle,
// extract the zone properties and write zonecfg commands needed to
// configure the zone properties to the temp file. So the temp file is basically
// a zonecfg "command file". Once we extract all the zone properties relevant to
// the cluster node where this program is being executed, we invoke cl_exec
// to launch "zonecfg -z <zone> -f <temp file>" to configure the zone.
//
// If the zone is already configured on this node, we remove all the existing
// resources and add them back. We take this approach because it's very hard to
// implement a mechanism to modify existing resource - by the time sczonecfg
// invokes us after a commit, the CCR data contains the new configuration so we
// really don't know which resources have been modified.
//
// The temp file should always be unlink(2)'ed before returning from this
// function.
//
int
create_func(void)
{
	char cmdbuf[1024]; // "zonecfg -z <zone> -f tempfile"
	char tempfile[1024]; // The zonecfg "command file"
	char zonepath[MAXPATHLEN];
	char brand_type[MAXNAMELEN];
	char buff[MAXPATHLEN]; // for miscellaneous use
	bool autoboot;
	nodeid_t nodeid;
	zc_iptype_t iptype;
	char *priv_str = NULL;
	FILE *fp;
	int err = ZC_OK;
	zone_state_t state = ZONE_STATE_CONFIGURED;

	if ((err = initialize()) != ZC_OK) {
		return (err);
	}

	(void) strcpy(tempfile, TMP_CMD_FILE);

	if (mkstemp(tempfile) < 0) {
		return (ZC_TEMP_FILE);
	}

	if ((fp = fopen(tempfile, "w+")) == NULL) {
		(void) printf("%s: can't open %s.\n", execname, tempfile);
		err = ZC_TEMP_FILE;
		goto cleanup;
	}

	(void) snprintf(cmdbuf, sizeof (cmdbuf), "%s -z %s -f %s",
	    ZONECFG_CMD, zone, tempfile);

	//
	// Check if the zone is already configured on this node. If yes, skip
	// the "create" step of the configuration.
	//
	if ((err = zone_get_state(zone, &state)) != Z_OK && err == Z_NO_ZONE) {
		(void) fprintf(fp, "%s -b\n", cmdtab[CMD_CREATE].cmd_name);
	}

	if (err != Z_NO_ZONE) {
		clear_existing_resources(state);
	}

	if ((err = zccfg_get_zonepath(handle, zonepath,
	    sizeof (zonepath))) != ZC_OK) {
		(void) printf("get_zonepath error\n");
		(void) fclose(fp);
		goto cleanup;
	}

	if (state < ZONE_STATE_INSTALLED) {
		(void) fprintf(fp, "set zonepath=%s\n", zonepath);
	}

	//
	// Get autoboot flag.
	//
	if ((err = zccfg_get_autoboot(handle, &autoboot)) != ZC_OK) {
		(void) fclose(fp);
		goto cleanup;
	}
	(void) fprintf(fp, "set autoboot=%s\n",
	    autoboot ? "true" : "false");

	//
	// brand_type is always "cluster" for us, but we don't hard-code
	// that here.
	//
	if ((err = zccfg_get_brand(handle, brand_type,
	    sizeof (brand_type))) != ZC_OK) {
		(void) printf("get_brand error\n");
		(void) fclose(fp);
		goto cleanup;
	}

	// Can't set brand if zone is already installed.
	if (state < ZONE_STATE_INSTALLED) {
		(void) fprintf(fp, "set brand=%s\n", brand_type);
	}

	// Set ip-type.
	if ((err = zccfg_get_iptype(handle, &iptype)) != ZC_OK) {
		(void) printf("get_iptype error\n");
		(void) fclose(fp);
		goto cleanup;
	}
	(void) fprintf(fp, "set ip-type=%s\n",
	    iptype == CZS_SHARED ? "shared" : "exclusive");

	// Set pool if it is specified.
	if ((err = zccfg_get_pool(handle, buff, sizeof (buff))) == ZC_OK) {
		(void) fprintf(fp, "clear pool\n");
		if (buff[0] != '\0') {
			(void) fprintf(fp, "set pool=%s\n", buff);
		}
	} else {
		(void) printf("get_pool error\n");
		(void) fclose(fp);
		goto cleanup;
	}

	// Set bootargs if specified.
	if ((err = zccfg_get_bootargs(handle, buff, sizeof (buff)))
	    == ZC_OK) {
		(void) fprintf(fp, "clear bootargs\n");
		if (buff[0] != '\0') {
			(void) fprintf(fp, "set bootargs=%s\n", buff);
		}
	} else {
		(void) printf("get_bootargs error %s\n",
		    zccfg_strerror(err));
		(void) fclose(fp);
		goto cleanup;
	}

	// Set limitpriv if specified.
	if ((err = zccfg_get_limitpriv(handle, &priv_str)) == ZC_OK) {
		(void) fprintf(fp, "clear limitpriv\n");
		if (*priv_str != '\0') {
			(void) fprintf(fp, "set limitpriv=%s\n", priv_str);
		}
		free(priv_str);
	} else {
		(void) printf("get_limitpriv error\n");
		(void) fclose(fp);
		goto cleanup;
	}

	// Set scheduling-class if specified.
	if ((err = zccfg_get_sched_class(handle, buff, sizeof (buff)))
	    == ZC_OK) {
		(void) fprintf(fp, "clear scheduling-class\n");
		if (buff[0] != '\0') {
			(void) fprintf(fp, "set scheduling-class=%s\n", buff);
		}
	} else {
		(void) printf("get_sched_class error\n");
		(void) fclose(fp);
		goto cleanup;
	}

	//
	// Add inherit-pkg-dir's...
	//
	if ((err = add_ipds(fp, state)) != ZC_OK) {
		(void) fclose(fp);
		goto cleanup;
	}

	//
	// Add lofs file systems
	//
	if ((err = add_lofs_filesystems(fp)) != ZC_OK) {
		(void) fclose(fp);
		goto cleanup;
	}

	//
	// Add global scope rctl's.
	//
	if ((err = add_rctls(fp)) != ZC_OK) {
		(void) fclose(fp);
		goto cleanup;
	}

	//
	// Add capped memory, if any
	//
	if ((err = add_mcap(fp)) != ZC_OK) {
		(void) fclose(fp);
		goto cleanup;
	}

	//
	// Add processor set (dedicated CPU), if any
	//
	if ((err = add_pset(fp)) != ZC_OK) {
		(void) fclose(fp);
		goto cleanup;
	}

	//
	// Add global devices.
	//
	if ((err = add_devices(fp)) != ZC_OK) {
		(void) fclose(fp);
		goto cleanup;
	}

	//
	// TODO Get other zone resources here. Node-local reources must
	// always come last.
	//

	// Add node-local resources...
	if ((err = add_node_local_res(fp)) != ZC_OK) {
		(void) fclose(fp);
		goto cleanup;
	}
	(void) fprintf(fp, "commit");

	//
	// We need to close the file first before launching the zonecfg
	// command; this is to avoid file locking issues.
	//
	(void) fclose(fp);

	nodeid = clconf_cluster_get_nodeid_by_nodename(my_nodename);
	err = zccfg_execute_cmd(nodeid, cmdbuf);

	if (err != ZC_OK) {
		(void) printf("%s exited with %d\n", cmdbuf, err);
	}

cleanup:
	(void) unlink(tempfile);
	return (err);
}

//
// Calls "zonecfg -z <zone> delete -F" to delete the zone.
//
int
delete_func(void)
{
	int err;
	char cmdbuf[MAXPATHLEN];
	nodeid_t nodeid;

	if ((err = initialize()) != ZC_OK) {
		return (err);
	}

	(void) sprintf(cmdbuf, "%s -z %s delete -F", ZONECFG_CMD,
	    zone);
	nodeid = clconf_cluster_get_nodeid_by_nodename(my_nodename);
	err = zccfg_execute_cmd(nodeid, cmdbuf);
	if (err != ZC_OK) {
		// TODO Log error
		(void) printf("%s exited with %d\n", cmdbuf, err);
	}
	return (err);
}

int
modify_func(void)
{
	// TODO Implement this funtion.
	return (ZC_OK);
}

//
// Creates the sysidcfg(4) file for a freshly installed Solaris zone component
// of a zone cluster. It is automatically invoked by clzonecluster(1CL) after
// a "zoneadm -z <zonename> install" completes successfully on a node.
//
// Returns:
//	ZC_OK on success
//	ZC_* from libzccfg.h on failure
//
int
sysidcfg_func(void)
{
	char sysidcfg_file[MAXPATHLEN];
	char zonepath[MAXPATHLEN];
	FILE *fp;
	int err;
	struct zc_sysidtab sysidtab;
	struct zc_nodetab nodetab;

	if ((err = initialize()) != ZC_OK) {
		return (err);
	}

	if ((err = zccfg_get_zonepath(handle, zonepath,
	    sizeof (zonepath))) != ZC_OK) {
		return (err);
	}

	//
	// sysidcfg_file is <zonepath>/root/etc/sysidcfg relative to the
	// global zone's root.
	//
	(void) sprintf(sysidcfg_file, "%s/%s", zonepath, SYSID_FILE);

	if ((fp = fopen(sysidcfg_file, "w+")) == NULL) {
		return (ZC_MISC_FS);
	}

	if ((err = zccfg_setsysident(handle)) != ZC_OK) {
		(void) fclose(fp);
		return (err);
	}

	if ((err = zccfg_getsysident(handle, &sysidtab)) != ZC_OK) {
		(void) printf(gettext("couldn't get sysident for %s\n"),
		    zone);
		(void) fclose(fp);
		return (err);
	}
	(void) zccfg_endsysident(handle);

	if ((err = zccfg_setnodeent(handle)) != ZC_OK) {
		(void) fclose(fp);
		return (err);
	}

	//
	// TODO
	// We should handle the possibility that we don't find a nodename
	// matching my_nodename. That is, my_nodename is not in the nodelist
	// of zone. Normally this should never happen, but we should guard
	// against it all the same.
	//
	while (zccfg_getnodeent(handle, &nodetab) == ZC_OK) {
		if (strcmp(my_nodename, nodetab.zc_nodename) == 0) {
			break;
		}
	}
	(void) zccfg_endnodeent(handle);

	//
	// Now populate the sysidcfg file.
	//
	// If the root password is not specified, set it to the empty string.
	//
	if (strcmp(sysidtab.zc_root_password, "") == 0) {
		(void) fprintf(fp, "root_password=\"\"\n");
	} else {
		(void) fprintf(fp, "root_password=%s\n",
		    sysidtab.zc_root_password);
	}
	(void) fprintf(fp, "name_service=%s\n", sysidtab.zc_name_service);
	(void) fprintf(fp, "nfs4_domain=%s\n", sysidtab.zc_nfs4_domain);

	//
	// Add network_interface stuff here. The interface name must be
	// 'primary' for a shared-IP zone.
	//
	(void) fprintf(fp, "network_interface=primary{hostname=%s}\n",
	    nodetab.zc_hostname);
	(void) fprintf(fp, "security_policy=%s\n", sysidtab.zc_sec_policy);
	(void) fprintf(fp, "system_locale=%s\n", sysidtab.zc_sys_locale);
	(void) fprintf(fp, "terminal=%s\n", sysidtab.zc_terminal);
	(void) fprintf(fp, "timezone=%s\n", sysidtab.zc_timezone);

	(void) fclose(fp);
	return (ZC_OK);
}

//
// Get the status/state of the zone member on this node. The return value
// represents the state of the zone on this node.
//
int
status_func(void)
{
	int err;
	zone_state_t zstate = ZONE_STATE_CONFIGURED;
	zone_cluster_state_t state = ZC_STATE_UNKNOWN;

	if ((err = initialize()) != ZC_OK) {
		return (ZC_STATE_UNKNOWN);
	}

	err = zone_get_state(zone, &zstate);
	if (err != Z_OK) {
		return (ZC_STATE_UNKNOWN);
	} else {
		switch (zstate) {
		case ZONE_STATE_CONFIGURED:
			state = ZC_STATE_CONFIGURED;
			break;
		case ZONE_STATE_INCOMPLETE:
			state = ZC_STATE_INCOMPLETE;
			break;
		case ZONE_STATE_INSTALLED:
			state = ZC_STATE_INSTALLED;
			break;
		case ZONE_STATE_DOWN:
			state = ZC_STATE_DOWN;
			break;
		case ZONE_STATE_MOUNTED:
			state = ZC_STATE_MOUNTED;
			break;
		case ZONE_STATE_READY:
			state = ZC_STATE_READY;
			break;
		case ZONE_STATE_SHUTTING_DOWN:
			state = ZC_STATE_SHUTTING_DOWN;
			break;
		case ZONE_STATE_RUNNING:
			state = ZC_STATE_RUNNING;
			break;
		default:
			state = ZC_STATE_UNKNOWN;
			break;
		}
	}

	return (state);
}

int
install_func(int argc, char *argv[])
{
	int err;

	// TODO: Implement this funtion.
	if ((err = initialize()) != ZC_OK) {
		return (err);
	}

	return (ZC_OK);
}

//
// This helper function checks whether or not the zone-cluster member on
// this node is in some RG's nodelist.
//
// Returns TRUE if we're in some RG's nodelist, else returns FALSE.
//
// Could be enhanced to return a comma-separated list of RG names this zone
// is a potential primary of, if more verbose error message is desired.
//
static bool
zone_is_in_rg_nodelist(nodeid_t nodeid)
{
	scha_errmsg_t rgm_status = {SCHA_ERR_NOERR, NULL};
	char **managed_rg_names = 0;
	char **unmanaged_rg_names = 0;
	char **rg_namep;
	char *nodelist = NULL;
	char			nodeid_str[3]; // up to "64"

	// Get all RGs.
	rgm_status = rgm_scrgadm_getrglist(&managed_rg_names,
	    &unmanaged_rg_names, (boolean_t)false, zone);
	assert(rgm_status.err_code == SCHA_ERR_NOERR);

	(void) snprintf(nodeid_str, sizeof (nodeid_str), "%d", (int)nodeid);

	//
	// SCHA calls can't be relied upon here since the zone may
	// be down. Instead, we need to directly read the information from
	// the CCR. For this, we'll make use of the get_property_value()
	// function in librgm.
	//
	for (rg_namep = managed_rg_names; rg_namep && *rg_namep;
	    ++rg_namep) {
		rgm_status = get_property_value(*rg_namep, O_RG,
		    SCHA_NODELIST, &nodelist, zone);
		assert(rgm_status.err_code == SCHA_ERR_NOERR);
		// TODO Log an error and return from here if there was error.
		if (nodelist) {
			if (strstr(nodelist, nodeid_str)) {
				    return (true);
			}
		}
	}


	for (rg_namep = unmanaged_rg_names; rg_namep && *rg_namep;
	    ++rg_namep) {
		rgm_status = get_property_value(*rg_namep, O_RG,
		    SCHA_NODELIST, &nodelist, zone);
		assert(rgm_status.err_code == SCHA_ERR_NOERR);
		// TODO Log an error and return from here if there was error.
		if (nodelist) {
			if (strstr(nodelist, nodeid_str)) {
				return (true);
			}
		}
	}
	return (false);
}

int
uninstall_func(int argc, char *argv[])
{
	int			err;
	struct zc_nodetab	nodetab;
	nodeid_t		my_nodeid;

	if ((err = initialize()) != ZC_OK) {
		return (err);
	}

	(void) strcpy(nodetab.zc_nodename, my_nodename);
	(void) strcpy(nodetab.zc_hostname, "");
	if ((err = zccfg_lookup_node(handle, &nodetab)) != ZC_OK) {
		return (err);
	}
	my_nodeid = clconf_cluster_get_nodeid_by_nodename(
	    nodetab.zc_nodename);

	//
	// Make sure that there are no RG's configured in this
	// zone-cluster member.
	//
	if (zone_is_in_rg_nodelist(my_nodeid)) {
		(void) printf(gettext("%s on this node is a potential primary"
		    " of one or more resource groups.\n"), zone);
		(void) printf(gettext("Remove %s from the nodelist of all "
		    "resource groups before uninstalling %s on %s.\n"),
		    nodetab.zc_hostname, zone, my_nodename);
		return (ZC_BAD_ZONE_STATE);
	}

	return (ZC_OK);
}

//
// boot, reboot, halt, mount, unmount and apply functions do not call
// initialize() because these commands must be allowed to complete even if the
// physical node is not currently in the cluster.
//
int
boot_func(int argc, char *argv[])
{
	// TODO: Implement this funtion.
	return (ZC_OK);
}

int
reboot_func(int argc, char *argv[])
{
	// TODO: Implement this funtion.
	return (ZC_OK);
}

int
halt_func(int argc, char *argv[])
{
	// TODO: Implement this funtion.
	return (ZC_OK);
}

//
// Checks whether or not the propsed zonepath (argv[3]) matches the value
// stored in the CCR. Returns ZC_OK if it's a match, ZC_* otherwise.
//
int
move_func(int argc, char *argv[])
{
	char new_path[MAXPATHLEN];
	char zonepath[MAXPATHLEN];
	int err;

	if ((err = initialize()) != ZC_OK) {
		return (err);
	}

	(void) strlcpy(new_path, argv[3], sizeof (new_path));

	if (new_path[0] == '\0') {
		// Nothing to do.
		return (ZC_OK);
	}

	if ((err = zccfg_get_zonepath(handle, zonepath,
	    sizeof (zonepath))) != ZC_OK) {
		return (err);
	}

	if (strcmp(new_path, zonepath) != 0) {
		(void) printf(gettext("Proposed zonepath '%s' does not match "
		    "zonepath '%s' in the CCR.\nVerification failed.\n"),
		    new_path, zonepath);
		(void) printf(gettext("Use %s to move the zonepath of a zone "
		    "cluster.\n"), "clzonecluster(1CL)");
		return (ZC_INVAL);
	}
	return (ZC_OK);
}

//
// Alas, clone won't work on Solaris 10 until the fix for CR 6519634 is
// back-ported.
//
int
clone_func(int argc, char *argv[])
{
	int err;

	// TODO: Implement this funtion.
	if ((err = initialize()) != ZC_OK) {
		return (err);
	}

	return (ZC_OK);
}

int
list_func(int argc, char *argv[])
{
	// TODO: Implement this funtion.
	return (ZC_OK);
}

int
attach_func(int argc, char *argv[])
{
	int err;

	// TODO: Implement this funtion.
	if ((err = initialize()) != ZC_OK) {
		return (err);
	}

	return (ZC_OK);
}

int
detach_func(int argc, char *argv[])
{
	int err;

	// TODO: Implement this funtion.
	if ((err = initialize()) != ZC_OK) {
		return (err);
	}

	return (ZC_OK);
}

int
ready_func(int argc, char *argv[])
{
	// TODO: Implement this funtion.
	return (ZC_OK);
}

int
verify_func(int argc, char *argv[])
{
	int err;

	// TODO: Implement this funtion.
	if ((err = initialize()) != ZC_OK) {
		return (err);
	}

	return (ZC_OK);
}

int
mount_func(int argc, char *argv[])
{
	// TODO: Implement this funtion.
	return (ZC_OK);
}

int
unmount_func(int argc, char *argv[])
{
	// TODO: Implement this funtion.
	return (ZC_OK);
}

int
apply_func(int argc, char *argv[])
{
	// TODO: Implement this funtion.
	return (ZC_OK);
}

int
mark_func(int argc, char *argv[])
{
	// TODO: Implement this funtion.
	return (ZC_OK);
}

//
// Checks if a Solaris zone called "zone" (see global variable 'zone')
// exists on this node. Returns CZ_OK if zone exists, ZC_* otherwise.
//
// This function is called from by the "create" sub-command of sczonecfg.
//
int
check_if_zone_exists(void)
{
	int err;
	zone_dochandle_t tmp_handle;

	if ((tmp_handle = zonecfg_init_handle()) == NULL) {
		// TODO Log error.
		return (ZC_NOMEM);
	}

	err = zonecfg_get_handle(zone, tmp_handle);
	zonecfg_fini_handle(tmp_handle);

	if (err == Z_NO_ZONE) {
		// Zone does not exist.
		err = ZC_NO_ZONE;
	} else if (err == Z_OK) {
		// Zone exists.
		err = ZC_OK;
	}

	return (err);
}

int
main(int argc, char *argv[])
{
	int err = ZC_OK;
	execname = get_execbasename(argv[0]);

	(void) setlocale(LC_ALL, "");
	(void) textdomain(TEXT_DOMAIN);

	if (getzoneid() != GLOBAL_ZONEID) {
		(void) printf(gettext("%s can only be run from the global"
		    " zone.\n"), execname);
		exit(1);
	}

	if (argc < 3) {
		usage();
		exit(1);
	}

	(void) strlcpy(zone, argv[1], sizeof (zone));

	//
	// Get the hostname of this node.
	//
	if (gethostname(my_nodename, (int)(sizeof (my_nodename))) < 0) {
		exit(errno);
	}

	switch (cmd_match(argv[2])) {
	case CMD_CREATE:
		err = create_func();
		break;
	case CMD_DELETE:
		err = delete_func();
		break;
	case CMD_MODIFY:
		err = modify_func();
		break;
	case CMD_SYSIDCFG:
		err = sysidcfg_func();
		break;
	case CMD_STATUS:
		err = status_func();
		break;
	case CMD_INSTALL:
		err = install_func(argc, argv);
		break;
	case CMD_UNINSTALL:
		err = uninstall_func(argc, argv);
		break;
	case CMD_BOOT:
		err = boot_func(argc, argv);
		break;
	case CMD_REBOOT:
		err = reboot_func(argc, argv);
		break;
	case CMD_HALT:
		err = halt_func(argc, argv);
		break;
	case CMD_LIST:
		err = list_func(argc, argv);
		break;
	case CMD_READY:
		err = ready_func(argc, argv);
		break;
	case CMD_VERIFY:
		err = verify_func(argc, argv);
		break;
	case CMD_CLONE:
		err = clone_func(argc, argv);
		break;
	case CMD_MOVE:
		err = move_func(argc, argv);
		break;
	case CMD_MOUNT:
		err = mount_func(argc, argv);
		break;
	case CMD_UNMOUNT:
		err = unmount_func(argc, argv);
		break;
	case CMD_APPLY:
		err = apply_func(argc, argv);
		break;
	case CMD_ATTACH:
		err = attach_func(argc, argv);
		break;
	case CMD_DETACH:
		err = detach_func(argc, argv);
		break;
	case CMD_MARK:
		err = mark_func(argc, argv);
		break;
	case CMD_CHECK_EXIST:
		err = check_if_zone_exists();
		break;
	default:
		(void) printf(gettext("%s: unknown command '%s'.\n"),
		    execname, argv[2]);
		(void) printf(gettext("Ignoring it.\n"));
		break;
	}

	// TODO
	// do some cleanup here - e.g. removing tmp files. etc.
	//
	zccfg_fini_handle(handle);
	exit(err); //lint -e533
}
