#! /usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#
# ident	"@(#)zc_boot.ksh	1.11	09/01/20 SMI"
#
# Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
################################################################################
#
# boot callback script for the cluster brand zone
#
################################################################################

set -ufp
unset PATH
export PATH="/sbin:/bin:/usr/cluster/bin:/usr/cluster/lib/sc"


################################################################################
#
# initialize()
#
################################################################################
initialize()
{
	MYNAME=`/bin/basename $0`
	SYSLOG_TAG="Cluster.${MYNAME:-}"

	return 0
}

################################################################################
#
# read_arguments()
#
################################################################################
read_arguments()
{
	typeset rc=0
	typeset invalid_option=""
	typeset save_opt=""
	typeset save_optarg=""

	while getopts :z:r: opt
	do
		case ${opt} in
			z)	ZONENAME="${OPTARG}";;
			r)	ZONEPATH="${OPTARG}";
				ZONEROOT="${ZONEPATH}/root";
				;;
			*)	invalid_option="YES"; 
				save_opt="${opt}";
				save_optarg="${OPTARG}";
			;;
		esac
	done

	if [ -z "${ZONENAME}" \
		-o -z "${ZONEPATH}" \
		-o -n "${invalid_option}" ]; then

		# SCMSGS
		# @explanation
		# Unable to process parameters passed to the call back method.
		# This is an internal error.
		# @user_action
		# Please report this problem.
		scds_syslog -p error -t ${SYSLOG_TAG} -m \
			"Initialization failed. Invalid command line %s %s" \
			"${save_optarg}" "${save_opt}"
		rc=1;
	fi

	return ${rc}
}

################################################################################
#
# do_rac_mounts
# 
# Mounts any needed filesystems from currently hollow packages that are
# installed in the global zone.
#
################################################################################
do_rac_mounts()
{
	typeset	rc=0
	set -A	rac_mnts \
			"/opt/SUNWcvm" \
			"/opt/SUNWudlm" \
			"/usr/cluster/lib/ucmm" \
			"/usr/cluster/lib/libhaops.so.1" \
			"/usr/cluster/lib/libskgxn2.so" \
			"/usr/cluster/lib/libskgxn2.so.1" \
			"/usr/cluster/lib/amd64/libhaops.so.1" \
			"/usr/cluster/lib/amd64/libskgxn2.so" \
			"/usr/cluster/lib/amd64/libskgxn2.so.1" \
			"/usr/cluster/lib/sparcv9/libhaops.so.1" \
			"/usr/cluster/lib/sparcv9/libskgxn2.so" \
			"/usr/cluster/lib/sparcv9/libskgxn2.so.1"
	typeset	mnt_opts="-F lofs -r"

	for mnt in ${rac_mnts[*]} ; do
		# if mnt exists in the global zone, mount it in the local zone
		if [[ -e ${mnt} ]]; then
			# set mnt_dir to the directory of mnt
			if [[ -d ${mnt} ]]; then
				mnt_dir="${ZONEROOT}${mnt}"
			else
				mnt_dir=`dirname ${ZONEROOT}${mnt}`
			fi
			# ensure that the directory exists in the local zone
			if [[ ! -d ${mnt_dir} ]]; then
				# if the mount point does not exist in the
				# local zone, create it
				mkdir -p ${mnt_dir}
			fi

			# if mnt is a directory, mount the directory
			# in the local zone
			if [[ -d ${mnt} ]]; then
				mount ${mnt_opts} ${mnt} ${mnt_dir}
			else
				# if mnt is a symlink, create the same symlink
				# in the local zone
				if [[ -L ${mnt} ]]; then
					link=`ls -l $mnt | awk '{print $NF}'`
					mnt_bn=`basename $mnt`
					(cd ${mnt_dir};ln -s ${link} ${mnt_bn})
				else
					# if mnt is a regular file, mount the
					# file in the local zone
					if [[ ! -f ${ZONEROOT}${mnt} ]]; then
						touch ${ZONEROOT}${mnt}
					fi
					mount ${mnt_opts} ${mnt} \
						${ZONEROOT}${mnt}
				fi
			fi
		fi
	done

	return ${rc}
}

################################################################################
#
# do_system_rt_registration
# 
# Register the SUNW.LogicalHostname and SUNW.SharedAddress resource types when
# the zone cluster node boots
#
################################################################################
do_system_rt_registration()
{
	CLRT_CMD="/usr/cluster/bin/clresourcetype"
	#
	# Register the Logical and SharedAddress RTs using the clrt register command
	# We dont care about the exit status because this operation fails every time
	# except for the very first time when the zone cluster boots
	# This approach is better than running one clrt list command to check if
	# we have these two RTs already registered and not running the register command.
	#
	${CLRT_CMD} register -Z ${ZONENAME} SUNW.LogicalHostname SUNW.SharedAddress > /dev/null 2>&1
}

################################################################################
#
# main
#
################################################################################
main()
{
	typeset -i rc=0

	initialize

	read_arguments "${@:-}" || return $?

	#
	# Exit if we are not booted in cluster mode.
	#
	/usr/sbin/clinfo > /dev/null 2>&1
	if [[ $? != 0 ]] ; then
        	exit 0
	fi

	do_rac_mounts || return $?

	#
	# We ignore the return status of sqfs handler which makes configured
	# sQFS file systems available to the zone cluster node.
	# The failure to bring the sQFS file systems is not fatal to prevent
	# the node to boot.
	#
	zc_sqfs_boot ${ZONENAME} ${ZONEPATH}

	do_system_rt_registration

	return ${rc}
}

main "${@:-}"

exit $?

