//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)zc_rgm_halt.cc	1.3	08/08/26 SMI"

#include <sys/os.h>
#include <sys/clconf_int.h>
#include <cmm/cmm_ns.h>
#include <sys/cl_auth.h>
#include <scadmin/scswitch.h>
#include <rgm/scha_priv.h>
#include <rgm/rgm_common.h>
#include <orb/infrastructure/orb_conf.h>

#define	EVAC_TIMEOUT	900	// 900 seconds = 15 minutes

//
// This is a Sun Cluster private command, and as such must not be directly
// executed by the user.
//

//
// This program is called by the cluster brand handler for halt subcommand.
// When the zone is halted, this program is launched.
// This program evacuates the resource groups from this zone cluster node.
//

int
main(int, char *argv[])
{
	int bootflags = 0;
	uint_t ismember = 0;
	char *zonenamep = argv[1];
	scha_errmsg_t schaerror = { SCHA_ERR_NOERR, NULL };
	os::sc_syslog_msg msglog("", argv[0], NULL);

	//
	// We do not do anything when in non-cluster mode, but we exit
	// with success to allow zone to boot when in non-cluster mode.
	//
	if (os::cladm(CL_INITIALIZE, CL_GET_BOOTFLAG, &bootflags) != 0) {
		return (1);
	}
	if (!(bootflags & CLUSTER_BOOTED)) {
		// Not in cluster mode
		return (0);
	}

	// We are in cluster mode
	if (clconf_lib_init() != 0) {
		//
		// SCMSGS
		// @explanation
		// zc_rgm_halt failed to initialize ORB and will not be able
		// to evacuate resource groups for this zone which belongs
		// to a zone cluster.
		// Support for this zone cluster might not work properly.
		// @user_action
		// Contact your authorized Sun service provider to
		// determine whether a workaround or patch is available.
		//
		(void) msglog.log(SC_SYSLOG_PANIC, MESSAGE,
		    "zc_rgm_halt for zone %s: "
		    "Failed to initialize the ORB. Exiting.\n", zonenamep);
		return (1);
	}

	// Initialize and promote to euid=0 priviledge
	cl_auth_init();
	cl_auth_promote();

	// Make sure that we are active in the cluster
	membership::api_var membership_api_v = cmm_ns::get_membership_api_ref();
	if (CORBA::is_nil(membership_api_v)) {
		//
		// SCMSGS
		// @explanation
		// zc_rgm_halt failed to get a reference to the membership
		// API object, and thus failed to check whether the zone
		// is part of the zone cluster membership or not.
		// Hence zc_rgm_halt will not be able to evacuate resource
		// groups for this zone which belongs to a zone cluster.
		// Support for this zone cluster might not work properly.
		// @user_action
		// Contact your authorized Sun service provider to
		// determine whether a workaround or patch is available.
		//
		(void) msglog.log(SC_SYSLOG_PANIC, MESSAGE,
		    "zc_rgm_halt for zone %s: Could not get reference "
		    "to membership API object. Exiting.\n", zonenamep);
		return (2);
	}

	Environment e;
	CORBA::Exception *exp = NULL;
	cmm::membership_t membership;
	cmm::seqnum_t seqnum;
	membership_api_v->get_cluster_membership(
	    membership, seqnum, zonenamep, e);
	exp = e.exception();
	if (exp != NULL) {
		const char *exp_namep = (exp->_name())? (exp->_name()) : "NULL";
		//
		// SCMSGS
		// @explanation
		// An exception was thrown when zc_rgm_halt queried
		// for the membership of the zone cluster, and thus
		// zc_rgm_halt failed to check whether the zone
		// is part of the zone cluster membership or not.
		// Hence zc_rgm_halt will not be able to evacuate resource
		// groups for this zone which belongs to a zone cluster.
		// Support for this zone cluster might not work properly.
		// @user_action
		// Contact your authorized Sun service provider to
		// determine whether a workaround or patch is available.
		//
		(void) msglog.log(SC_SYSLOG_PANIC, MESSAGE,
		    "zc_rgm_halt for zone %s: Exception <%s> thrown "
		    "while querying membership. Exiting.\n",
		    zonenamep, exp_namep);
		e.clear();
		return (3);
	}

	nodeid_t local_nid = orb_conf::local_nodeid();
	if (membership.members[local_nid] == INCN_UNKNOWN) {
		//
		// This zone is not part of the zone cluster membership.
		// So we do not need to evacuate resource groups
		// as RGM would not have brought resource groups online
		// on this zone, as it is not part of the membership.
		// Simply return success.
		//
		return (0);
	}

	// no longer privileged
	cl_auth_demote();

	// Check authorization
	cl_auth_check_command_opt_exit(*argv, NULL, CL_AUTH_SYSTEM_ADMIN, 1);

	// Promote to euid=0
	cl_auth_promote();

	// Get the clconf tree for the zone cluster
	clconf_cluster_t *zc_clconfp =
	    clconf_cluster_get_vc_current(zonenamep);
	if (zc_clconfp == NULL) {
		//
		// SCMSGS
		// @explanation
		// zc_rgm_halt could not get the clconf tree for
		// the zone cluster and thus cannot determine
		// the hostname of the zone going down.
		// Hence zc_rgm_halt will not be able to evacuate resource
		// groups for this zone which belongs to a zone cluster.
		// Support for this zone cluster might not work properly.
		// @user_action
		// Contact your authorized Sun service provider to
		// determine whether a workaround or patch is available.
		//
		(void) msglog.log(SC_SYSLOG_PANIC, MESSAGE,
		    "zc_rgm_halt for zone %s: "
		    "Could not get clconf tree. Exiting.\n", zonenamep);
		return (4);
	}

	// Get the zone hostname for the zone going down
	const char *zone_hostnamep = clconf_cluster_get_nodename_by_nodeid(
	    zc_clconfp, clconf_get_local_nodeid());
	if (zone_hostnamep == NULL) {
		//
		// SCMSGS
		// @explanation
		// zc_rgm_halt could not get the hostname
		// of the zone going down.
		// Hence zc_rgm_halt will not be able to evacuate resource
		// groups for this zone which belongs to a zone cluster.
		// Support for this zone cluster might not work properly.
		// @user_action
		// Contact your authorized Sun service provider to
		// determine whether a workaround or patch is available.
		//
		(void) msglog.log(SC_SYSLOG_PANIC, MESSAGE,
		    "zc_rgm_halt for zone %s: Could not get hostname "
		    "of this zone. Exiting.\n", zonenamep);
		clconf_obj_release((clconf_obj_t *)zc_clconfp);
		return (5);
	}

	//
	// Evacuate all resource groups from this zone of a zone cluster.
	// Set the verbose_flag to B_FALSE.
	//
	schaerror = scswitch_evacuate_rgs(
		zone_hostnamep,	// zone from which evacuation is to be done
		EVAC_TIMEOUT,	// timeout of evacuation
		B_FALSE,	// verbose flag
		zonenamep);	// zone cluster name
	if (schaerror.err_code != SCHA_ERR_NOERR) {
		char *errormessagep = schaerror.err_msg ? schaerror.err_msg :
			rgm_error_msg(schaerror.err_code);
		//
		// SCMSGS
		// @explanation
		// zc_rgm_halt failed to evacuate all resource groups
		// from this zone which belongs to a zone cluster.
		// Support for this zone cluster might not work properly.
		// @user_action
		// Contact your authorized Sun service provider to
		// determine whether a workaround or patch is available.
		//
		(void) msglog.log(SC_SYSLOG_PANIC, MESSAGE,
		    "zc_rgm_halt for zone %s: "
		    "Could not shut down all resource groups: %s\n",
		    zonenamep, errormessagep);
		clconf_obj_release((clconf_obj_t *)zc_clconfp);

		//
		// Ignore the error. The zone is going down.
		// All processes inside the zone have already been
		// torn down by zoneadmd.
		//
		// We do this because in case of a resource which
		// has failover mode HARD and the stop method fails
		// then if we return an error, zoneadmd is unable to
		// halt the zone.
		//
		// TODO: RGM should have the capability to selectively
		// evacuate resources and resource groups. Once such
		// capability is implemented, we should only evacuate
		// Resource groups which have atleast one resource
		// whose methods run in the global zone.
		//

		return (0);
	}

	clconf_obj_release((clconf_obj_t *)zc_clconfp);

	return (0);
}
