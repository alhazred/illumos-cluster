//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)zccfg_verify.cc	1.9	08/07/17 SMI"

//
// zccfg_verify is the handler program for zonecfg(1M) for the "cluster"
// branded zone type, or zone cluster. It is invoked by the zonecfg command
// to verify the configuration of the zone being configured or modified. For
// the cluster branded zone, this program verifies that the configuration
// proposed by zonecfg(1M) does not conflict with the corresponding data stored
// in the CCR.
//
// This program also ensures that zonecfg(1M) cannot be used to create a new
// zone of type "cluster" because such a zone would not have an entry in the
// CCR. This rule requires the user to always use clzonecluster(1CL) to create
// a new zone cluster. Once the zone cluster is created and its component zones
// instantiated on the physical cluster nodes, the user could use zonecfg(1M)
// to edit the properties of the instance on any given physical node. So we
// must make sure that the changes the user makes to the zone instance do not
// conflict with those stored in the CCR, since allowing divergence could
// result in hard-to-diagnose configuration errors.
//
// Since the cluster code supporting zone clusters and Solaris zones code
// reside in different source code consolidations, it is possible that the
// Solaris code supports new zone properties and resources which are not yet
// ported to the cluster code. In such cases, zccfg_verify will ignore the
// unknown properties and resources since, by definition, such settings cannot
// conflict with the configuration data in the CCR.
//
// We will ensure that the global scope properties of the zone instance are the
// same as those in the CCR. This means that the following properties are
// checked:
//
// zonepath, pool, limitpriv, bootargs, autoboot, and ip-type.
//
// All zone cluster instances are required to have the same settings for the
// inherited-pkg-dir resource, and we ensure that this is the case here.
//
// Additionally, we make sure that the user does not modify/remove the per-node
// zone cluster resources of the zone cluster instance on this node using
// zonecfg. We currently check only the network resource in this category, but
// more zone resource types can be checked if needed.
//
// The above list must be expanded when there is a requirement to validate
// other zone properties.
//
// This program is linked with libzonecfg.so of Solaris.
//
// The input to the program are the name of the zone and a temporary XML file
// containing the proposed configuration of the zone. Note that this XML file
// must be parsed using the zonecfg_* interfaces in libzonecfg.h, and is not
// the same as the XML file referenced by zc_dochandle_t.
//
// On success, exit status must be ZC_OK (0).
// On failure, it must be ZC_ERR (1).
//

#include <stdio.h>
#include <stdlib.h>
#include <libintl.h>
#include <locale.h>
#include <zone.h>
#include <sys/clconf_int.h>

// For parsing Solaris zone XML file
#include <libzonecfg.h>

// For zone cluster interfaces
#include <libzccfg/libzccfg.h>

#define	ZC_ERR		1

static char target_zone[ZONENAME_MAX];
zone_dochandle_t	z_handle;	// Solaris zone handle
zc_dochandle_t		cl_handle;	// zone cluster handle

//
// Returns "true" if the zonepath in the XML file matches the zonepath
// in the CCR. Else returns "false:.
//
static bool
valid_zonepath(void)
{
	char zonepath[MAXPATHLEN]; // zonepath in XML file
	char clzonepath[MAXPATHLEN]; // zonepath in the CCR

	if (zonecfg_get_zonepath(z_handle, zonepath,
	    sizeof (zonepath)) != Z_OK) {
		(void) printf(gettext("Error getting zonepath from the XML"
		    " file.\n"));
		return (false);
	}
	if (zccfg_get_zonepath(cl_handle, clzonepath,
	    sizeof (clzonepath)) != ZC_OK) {
		(void) printf(gettext("Error getting zonepath from the"
		    " CCR.\n"));
		return (false);
	}

	if (strcmp(zonepath, clzonepath) != 0) {
		(void) printf(gettext("Proposed zonepath \'%s\' is different"
		    " from zonepath \'%s\' stored in the CCR.\nVerification "
		    " failed.\n"), zonepath, clzonepath);
		return (false);
	}

	return (true);
}

//
// Returns "true" if pool in the XML file matches the one in the CCR. Else
// returns "fasle".
//
static bool
valid_pool(void)
{
	char pool_name[MAXNAMELEN]; // pool stored in the XML file
	char clpool[MAXNAMELEN]; // pool stored in the CCR

	if (zonecfg_get_pool(z_handle, pool_name, sizeof (pool_name)) != Z_OK) {
		(void) printf(gettext("Error getting pool from the XML"
		    " file.\n"));
		return (false);
	}
	if (zccfg_get_pool(cl_handle, clpool, sizeof (clpool)) != ZC_OK) {
		(void) printf(gettext("Error getting pool from the"
		    " CCR.\n"));
		return (false);
	}

	if (strcmp(pool_name, clpool) != 0) {
		(void) printf(gettext("Proposed pool \'%s\' is different"
		    " from pool \'%s\' stored in the CCR.\nVerification "
		    " failed.\n"), pool_name, clpool);
		return (false);
	}

	return (true);
}

//
// Returns "true" if ip-type in the XML file matches the one in the CCR. Else
// returns "false".
//
static bool
valid_iptype(void)
{
	zone_iptype_t ziptype;
	zc_iptype_t cliptype;

	if (zonecfg_get_iptype(z_handle, &ziptype) != Z_OK) {
		(void) printf(gettext("Error getting ip-type from the XML"
		    " file.\n"));
		return (false);
	}
	if (zccfg_get_iptype(cl_handle, &cliptype) != ZC_OK) {
		(void) printf(gettext("Error getting ip-type from the"
		    " CCR.\n"));
		return (false);
	}

	if ((zc_iptype_t)ziptype != cliptype) {
		(void) printf(gettext("Proposed ip-type \'%s\' is different"
		    " from ip-type \'%s\' stored in the CCR.\nVerification "
		    " failed.\n"),
		    ziptype == ZS_SHARED ? "shared" : "exclusive",
		    cliptype == CZS_SHARED ? "shared" : "exclusive");
		return (false);
	}

	return (true);
}

//
// valid_net_config()
//
// We ensure that the set of network resources proposed by zonecfg is a
// superset of the per-node network resources of the zone cluster on this
// node. We perform this check by making sure that on this node, each network
// resource of the zone cluster (from the CCR) also exists in the configuration
// proposed by zonecfg.
//
// Returns: "true" if valid; "false" otherwise.
//
static bool
valid_net_config(void)
{
	struct zc_nodetab nodetab;
	struct zone_nwiftab nwiftab;
	zc_nwifelem_t	*listp;
	char my_nodename[SYS_NMLN];
	bool found = false;

	if (gethostname(my_nodename, (int)(sizeof (my_nodename))) < 0) {
		(void) printf(gettext("Failed to get my nodename. Cannot "
		    "verify network configuration.\n"));
		return (false);
	}

	if (zccfg_setnodeent(cl_handle) != ZC_OK) {
		(void) printf(gettext("Internal libzccfg.so failure."
		    " Cannot verify network configuration.\n"));
		return (false);
	}

	// Get per-node information for this node.
	while (zccfg_getnodeent(cl_handle, &nodetab) == ZC_OK) {
		if (strcmp(my_nodename, nodetab.zc_nodename) == 0) {
			found = true;
			break;
		}
	}
	(void) zccfg_endnodeent(cl_handle);

	// Should not happen...
	if (!found) {
		(void) printf(gettext("Error: %s is not a member of zone "
		    "cluster %s.\nVerification failed.\n"), my_nodename,
		    target_zone);
		return (false);
	}

	//
	// Now ensure each network resource of the zone cluster for this node
	// exists in the XML file.
	//
	// TODO handle exclusive IP type zones (no address property)
	found = true;
	listp = nodetab.zc_nwif_listp;
	while (listp) {
		(void) strcpy(nwiftab.zone_nwif_address,
		    listp->elem.zc_nwif_address);

		//
		// Due to CR 6676551 in Solaris, we have to make the physical
		// property empty for the lookup to go correctly. This means
		// that if a match is found, we need to double-check that the
		// physical property also matches.
		//
		(void) strcpy(nwiftab.zone_nwif_physical, "");

		if ((zonecfg_lookup_nwif(z_handle, &nwiftab) != Z_OK) ||
		    (strcmp(nwiftab.zone_nwif_physical,
		    listp->elem.zc_nwif_physical) != 0)) {
			(void) printf(gettext("Network resource with address "
			    "\'%s\' and physical \'%s\' is missing from the "
			    "proposed configuration.\nVerification failed.\n"),
			    listp->elem.zc_nwif_address,
			    listp->elem.zc_nwif_physical);
			found = false;
		}

		listp = listp->next;
	}

	return (found);
}

//
// Perform validation of the proposed configuration from zonecfg(1M)
// against the zone cluster configuration stored in the CCR. The properties
// we check are described in the block comment at the beginning of this file.
//
static int
validate_config(void)
{
	bool validation_failed = false;

	if (!valid_zonepath()) {
		validation_failed = true;
	}

	if (!valid_pool()) {
		validation_failed = true;
	}

	if (!valid_iptype()) {
		validation_failed = true;
	}

	if (!valid_net_config()) {
		validation_failed = true;
	}

	return (validation_failed ? ZC_INVAL : ZC_OK);
}

int
main(int argc, char *argv[])
{
	int err;

	(void) setlocale(LC_ALL, "");
	(void) textdomain(TEXT_DOMAIN);

	if (argc != 2) {
		exit(ZC_ERR);
	}

	if (clconf_lib_init() != 0) {
		(void) printf(gettext("%s: Node not in a cluster. Could not "
		"verify.\n"), argv[0]);
		exit(ZC_ERR);
	}

	// Initialize zone handle
	if ((z_handle = zonecfg_init_handle()) == NULL) {
		(void) printf(gettext("%s: libzonecfg.so.1 internal error.\n"),
		    argv[0]);
		exit(ZC_ERR);
	}

	// Populate z_handle from XML file (argv[1])
	if (zonecfg_get_xml_handle(argv[1], z_handle) != Z_OK) {
		zonecfg_fini_handle(z_handle);
		(void) printf(gettext("%s: Invalid XML file from zonecfg.\n"),
		    argv[0]);
		exit(ZC_ERR);
	}

	// Get the zone name
	if (zonecfg_get_name(z_handle, target_zone, sizeof (target_zone))
	    != Z_OK) {
		(void) printf(gettext("%s: Could not get zone name.\n"),
		    argv[0]);
		zonecfg_fini_handle(z_handle);
		exit(ZC_ERR);
	}

	// Initialize zone cluster handle
	if ((cl_handle = zccfg_init_handle()) == NULL) {
		(void) printf(gettext("%s: libzccfg.so.1 internal "
		    "error.\n"), argv[0]);
		exit(ZC_ERR);
	}

	// Populate cl_handle from the CCR
	err = zccfg_get_handle(target_zone, cl_handle);
	if (err != ZC_OK && err == ZC_NO_ZONE) {
		(void) printf(gettext("%s: No such zone cluster configured.\n"
		    "Use clzonecluster(1CL) to configure a zone cluster.\n"),
		    argv[0]);
		goto out;
	}

	if ((err = validate_config()) != ZC_OK) {
		goto out;
	}

out:
	zonecfg_fini_handle(z_handle);
	zccfg_fini_handle(cl_handle);

	exit(err ? ZC_ERR : ZC_OK); //lint -e533
}
