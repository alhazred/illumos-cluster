/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)zc_getProjectNames.cc	1.2	08/08/01 SMI"


#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <project.h>
#include <locale.h>
#include <string.h>

#define	PROJ_SIZE 200

void
print_ProjectNames() {

	struct project *proj, *projRecevied = NULL;
	void *buf;


	proj = (struct project *)calloc(1, sizeof (struct project));
	buf = (void *) calloc(PROJ_SIZE, sizeof (char));

	if (proj == NULL || buf == NULL) goto error;

	setprojent();
	projRecevied = getprojent(proj, buf, PROJ_SIZE);
	while (projRecevied != NULL) {
		(void) fprintf(stdout, "%s\n", projRecevied->pj_name);
		projRecevied = getprojent(proj, buf, PROJ_SIZE);
	}
	endprojent();

    error:

	free(buf);
	free(proj);
}



int
main()
{
	(void) setlocale(LC_ALL, "");
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);
	(void) textdomain(TEXT_DOMAIN);
	print_ProjectNames();
	return (0);
}
