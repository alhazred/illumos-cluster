//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)process_cmd_log.cc	1.5	08/07/30 SMI"

//
// process_cmd_log
//
// This program is launched on system start-up to process and replay, if
// necessary, command logs for all zone clusters configured on the cluster.
// It exits as soon as it has processed all pending commands.
//

#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <locale.h>
#include <errno.h>
#include <unistd.h>

#include <sys/clconf_int.h>
#include <sys/cladm_int.h>
#include <libzccfg/libzccfg.h>
#include <sys/os.h>
#include <sys/rsrc_tag.h>

#define	ZONECFG "/usr/sbin/zonecfg"

const char	*cmd_create = "create";
const char	*cmd_delete = "delete";

// For syslog
static os::sc_syslog_msg logger(SC_SYSLOG_ZC_CMD_LOG_TAG, "", NULL);

//
// Convenience function to check if ZC can be deleted from the system; goes
// ahead and deletes the ZC if the check succeeds.
//
void
destroy_zc_if_needed(const char *clustername, const char *nodename)
{
	zc_dochandle_t		handle;
	int			err;
	struct zc_nodetab	nodetab;
	bool			privateip = false;

	if ((handle = zccfg_init_handle()) == NULL) {
		//
		// SCMSGS
		// @explanation
		// process_cmd_log was unable to allocate memory.
		// @user_action
		// Ensure that the cluster node has sufficient memory.
		//
		(void) logger.log(LOG_ERR, MESSAGE, "Insufficient memory. "
		    "Exiting.\n");
		exit(ZC_NOMEM);
	}

	if ((err = zccfg_get_handle(clustername, handle)) != ZC_OK) {
		//
		// SCMSGS
		// @explanation
		// process_cmd_log was unable to read the zone cluster
		// configuration.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) logger.log(LOG_ERR, MESSAGE, "Could not read "
		    "configuration of zone cluster %s.\n", clustername);
		zccfg_fini_handle(handle);
		return;
	}

	//
	// First delete this node from the configuration.
	//
	(void) strcpy(nodetab.zc_nodename, nodename);
	if ((err = zccfg_delete_node(handle, &nodetab)) != ZC_OK) {
		//
		// SCMSGS
		// @explanation
		// process_cmd_log was unable to delete a node from the zone
		// cluster configuration.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) logger.log(LOG_ERR, MESSAGE, "Could not delete node %s "
		    "from zone cluster %s.\n", nodename, clustername);
		zccfg_fini_handle(handle);
		return;
	}

	// Commit the latest configuration
	if ((err = zccfg_save(handle)) != ZC_OK) {
		//
		// SCMSGS
		// @explanation
		// process_cmd_log was unable to commit the zone cluster
		// configuration.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) logger.log(LOG_ERR, MESSAGE, "Could not commit zone "
		    "cluster %s configuration.\n", clustername);
		zccfg_fini_handle(handle);
		return;
	}

	// Finish off old handle and re-init it for further use.
	zccfg_fini_handle(handle);

	if ((handle = zccfg_init_handle()) == NULL) {
		(void) logger.log(LOG_ERR, MESSAGE, "Insufficient memory. "
		    "Exiting.\n");
		exit(ZC_NOMEM);
	}
	if ((err = zccfg_get_handle(clustername, handle)) != ZC_OK) {
		(void) logger.log(LOG_ERR, MESSAGE, "Could not read "
		    "configuration of zone cluster %s.\n", clustername);
		zccfg_fini_handle(handle);
		return;
	}

	//
	// Can't destroy ZC if it still has non-empty nodelist.
	//
	if (zccfg_num_resources(handle, "node") > 0) {
		zccfg_fini_handle(handle);
		return;
	}

	//
	// Check if private IP is enabled. If yes, we need to delete the
	// private network information as well when destroying the ZC.
	//
	if ((err = zccfg_get_clprivnet(handle, &privateip)) != ZC_OK) {
		zccfg_fini_handle(handle);
		return;
	}

	// We don't need the handle any more.
	zccfg_fini_handle(handle);

	//
	// No nodes left in the ZC. It must be destroyed now.
	//
	err = zccfg_destroy(clustername, true);
	if (err != ZC_OK) {
		//
		// SCMSGS
		// @explanation
		// process_cmd_log was unable to destroy the zone cluster
		// configuration.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) logger.log(LOG_ERR, MESSAGE, "Could not destroy zone "
		    "cluster %s configuration. Error %d.\n", clustername, err);
		return;
	}
	if (privateip) {
		(void) zccfg_remove_netccr_entry((char *)clustername);
	}
}

//
// This is the main function that processes the command log of each ZC. The
// logic is very simple: Get the command to be executed for the specified
// ZC (first arg) on this node (second arg), and execute it using cl_execd.
// cl_execd needs the nodeid on which to run the command, so we pass that
// as an argument to this function as well (third arg).
//
void
process_cmd_log(const char *clustername, const char *nodename, nodeid_t nodeid)
{
	char			*cmdp = NULL;
	char			cmd_str[MAXPATHLEN];
	int			err;

	cmdp = zccfg_get_cmd_log_entry(clustername, (char *)nodename);

	if (cmdp == NULL) {
		// Nothing to process.
		return;
	} else if (strcmp(cmdp, cmd_create) == 0) {
		//
		// Create/update ZC on this node. We invoke ZC_HELPER to
		// do this for us.
		//
		(void) snprintf(cmd_str, sizeof (cmd_str), "%s %s %s",
		    ZC_HELPER, clustername, cmd_create);
		if ((err = zccfg_execute_cmd(nodeid, cmd_str)) != ZC_OK) {
			//
			// SCMSGS
			// @explanation
			// process_cmd_log was unable to replay the specified
			// zone cluster command log entry.
			// @user_action
			// Contact your authorized Sun service provider to
			// determine whether a workaround or patch is
			// available.
			//
			(void) logger.log(LOG_ERR, MESSAGE, "Could not process"
			    " %s command for zone cluster %s. Error %d.\n",
			    cmdp, clustername, err);
		} else {
			(void) zccfg_delete_cmd_log_entry(clustername,
			    (char *)nodename);
		}
	} else if (strcmp(cmdp, cmd_delete) == 0) {
		//
		// Delete ZC on this node. We invoke ZONECFG to do this for
		// us.
		//
		(void) snprintf(cmd_str, sizeof (cmd_str), "%s -z %s %s -F",
		    ZONECFG, clustername, cmd_delete);
		if ((err = zccfg_execute_cmd(nodeid, cmd_str)) != ZC_OK) {
			(void) logger.log(LOG_ERR, MESSAGE, "Could not process"
			    " %s command for zone cluster %s. Error %d.\n",
			    cmdp, clustername, err);
		} else {
			(void) zccfg_delete_cmd_log_entry(clustername,
			    (char *)nodename);

			//
			// We just deleted the Solaris zone member on this node.
			// Now the node must be deleted from the ZC config.
			//
			destroy_zc_if_needed(clustername, nodename);
		}
	} else {
		//
		// SCMSGS
		// @explanation
		// There was an unrecognized entry in the zone cluster command
		// log. The command log table may be corrupt.
		// @user_action
		// Contact your authorized Sun service provider to
		// determine whether a workaround or patch is
		// available.
		//
		(void) logger.log(LOG_ERR, MESSAGE, "Unknown command '%s' in "
		    "the command log of zone cluster %s.\n", cmdp,
		    clustername);
	}
}

//
// Get all configured zone clusters on the system and process the command log
// for each one in turn.
//
void
do_process(void)
{
	char 		**clusters = clconf_cluster_get_cluster_names();
	char		**cluster_name;
	char 		my_nodename[SYS_NMLN];
	nodeid_t 	nodeid;

	if (gethostname(my_nodename, (int)sizeof (my_nodename)) < 0) {
		//
		// SCMSGS
		// @explanation
		// process_cmd_log was unable to determine the hostname of
		// the node.
		// @user_action
		// Contact your authorized Sun service provider to
		// determine whether a workaround or patch is
		// available.
		//
		(void) logger.log(LOG_ERR, MESSAGE, "Unable to determine my "
		    "hostname. Errno %d. Exiting.\n", errno);
		exit(errno);
	}

	nodeid = clconf_cluster_get_nodeid_by_nodename(my_nodename);

	//
	// Process command log of each zone cluster.
	//
	for (cluster_name = clusters; cluster_name && *cluster_name;
	    ++cluster_name) {
		process_cmd_log(*cluster_name, my_nodename, nodeid);
	}
}

// ARGSUSED
int
main(int argc, char *argv[])
{
	int	bootflags = 0;
	int	err;

	(void) setlocale(LC_ALL, "");
	(void) textdomain(TEXT_DOMAIN);

	//
	// Exit quietly if in non-global zone.
	//
	if (getzoneid() != GLOBAL_ZONEID) {
		return (0);
	}

	if ((err = cladm(CL_INITIALIZE, CL_GET_BOOTFLAG, &bootflags)) != 0) {
		return (err);
	}
	if (!(bootflags & CLUSTER_BOOTED)) {
		// Not in cluster. Exit quietly.
		return (0);
	}

	// Init libclconf
	if ((err = clconf_lib_init()) != 0) {
		//
		// SCMSGS
		// @explanation
		// process_cmd_log was unable to initialize the ORB.
		// @user_action
		// Contact your authorized Sun service provider to
		// determine whether a workaround or patch is available.
		//
		(void) logger.log(LOG_ERR, MESSAGE, "Cannot initialize the ORB"
		    ". Error %d\n", err);
		return (err);
	}

	do_process();
	return (0);
}
