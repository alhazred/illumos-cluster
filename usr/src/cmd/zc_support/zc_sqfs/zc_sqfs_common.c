/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)zc_sqfs_common.c	1.2	08/12/18 SMI"

/*
 * Common functions implementation for zc_sqfs_halt and zc_sqfs_boot
 * handlers.
 */

/*lint -e801 */

#include "zc_sqfs.h"
#include <ctype.h>

#ifdef DEBUG
int			debug_mode = 1;
#else
int			debug_mode = 0;
#endif

static scha_err_t is_mds_in_zone(const char *, const char *, int *);
static scha_err_t add_mds_list(scha_str_array_t *, const char *, const char *);
static void parse_zc_halt_boot_file(char *);

/*
 *	Name:		add_mds_list
 *	Argument:
 *			1. Address of a variable for MDS Resource name list
 *			2. resource type name.
 *			3. zone root
 *	Return:
 *			SCHA_ERR_NOERR - Successfully checked the given
 *					SUNW.qfs RT
 *			SCHA_ERR_ENOMEM - system runs out of memory
 *			Any other	- Failure occurred while checking
 *					the given RT
 *
 *	This function returns a list of all QFS metadata server resource
 *	groups for a given version of SUNW.qfs RT. It assumes all metadata
 *	server resources must be part of the global zone configuration.
 */
static scha_err_t
add_mds_list(scha_str_array_t *mds_rs_list, const char *mds_rt,
    const char *zone_root)
{
	scha_err_t		err = SCHA_ERR_NOERR;
	scha_resourcetype_t	rt_handle;
	scha_str_array_t	*rs_list = NULL;
	int			result;
	int			i = 0;

	log_debug_message("Entered into add_mds_list...\n");

	if ((err = scha_resourcetype_open(mds_rt, &rt_handle))
			!= SCHA_ERR_NOERR) {
		/*
		 * SCMSGS
		 * @explanation
		 * A call to scha_resourcetype_open() failed.
		 * @user_action
		 * Contact your authorized Sun service provider to determine
		 * whether a workaround or patch is available.
		 */
		syslog(LOG_ERR, "scha_resourcetype_open() failed: %s\n",
		    scha_strerror(err));
		log_debug_message("Leaving add_mds_list...\n");
		return (err);
	}

	/* Get the list of resources of the given SUNW.qfs RT */
	if ((err = scha_resourcetype_get(rt_handle, SCHA_RESOURCE_LIST,
					&rs_list)) != SCHA_ERR_NOERR) {
		if (err == SCHA_ERR_RT) {
			/*
			 * SCMSGS
			 * @explanation
			 * The SUNW.qfs resource type is not registered in the
			 * cluster.
			 * @user_action
			 * Register the SUNW.qfs resource type, create the
			 * required instances of this resource type, and
			 * repeat the operation. For information about how to
			 * configure the shared QFS file system with Sun
			 * Cluster, see your Sun Cluster documentation and
			 * your Sun StorEdge QFS documentation.
			 */
			syslog(LOG_ERR, "Resource type %s does not exist.\n",
			    mds_rt);
		} else {
			/*
			 * SCMSGS
			 * @explanation
			 * A call to scha_resourcetype_get() failed.
			 * @user_action
			 * Contact your authorized Sun service provider to
			 * determine whether a workaround or patch is
			 * available.
			 */
			syslog(LOG_ERR, "scha_resourcetype_get() failed: %s\n",
			    scha_strerror(err));
		}

		(void) scha_resourcetype_close(rt_handle);
		log_debug_message("Leaving add_mds_list...\n");
		return (err);
	}

	/* There are not any resources in the given SUNW.qfs RT. */
	if (rs_list == NULL || rs_list->array_cnt == 0) {
		log_debug_message("No resource found in RT %s\n", mds_rt);
		(void) scha_resourcetype_close(rt_handle);
		log_debug_message("Leaving add_mds_list...\n");
		return (err);
	}

	/*
	 * There are resources of type SUNW.qfs RT. Now see if there is one
	 * that represents our ScalMountPoint resource.
	 */
	for (i = 0; i < (int)(rs_list->array_cnt); i++) {
		result = 0;
		if ((err = is_mds_in_zone(rs_list->str_array[i], zone_root,
						&result)) != SCHA_ERR_NOERR) {
			/* error logged by is_mds_in_zone */
			break;
		}

		if (result) {
			/* We have found a MDS Resource under our zone_root */
			log_debug_message("will evacuate MDS Resource : %s.\n",
			    rs_list->str_array[i]);

			if ((err = add_to_strarray(mds_rs_list,
			    rs_list->str_array[i])) != SCHA_ERR_NOERR) {
				/* error already logged */
				break;
			}
		}
	}

	(void) scha_resourcetype_close(rt_handle);
	log_debug_message("Leaving add_mds_list...\n");
	return (err);
}

/*
 *	Name:		find_zone_mds_list
 *	Argument:
 *			Address of a variable for MDS Resource Type name
 *			Path to the root for the zone that is shutting down.
 *	Return:
 *			SCHA_ERR_NOERR - found our MDS Resource
 *			SCHA_ERR_CHECKS - No resource of type SUNW.qfs found or
 *					  no SUNW.qfs resource type found.
 *			SCHA_ERR_ENOMEM - system runs out of memory
 *
 *	This function retrieves all of the Resource Types in the cluster
 *	and then for each of SUNW.qfs RT, calls add_mds_list() to see if
 *	a resource of type SUNW.qfs RT exists.
 */
scha_err_t
find_zone_mds_list(scha_str_array_t *mds_rs_list, const char *zone_root)
{
	scha_err_t		err = SCHA_ERR_NOERR;
	scha_cluster_t		clust_handle;
	scha_str_array_t	*rt_list = NULL;
	int			i = 0;
	uint_t			retries = 0;

	log_debug_message("Entered into find_zone_mds_list..\n");

retry_clstr_get:
	/* Find out all resource types that are registered */
	if ((err = scha_cluster_open(&clust_handle)) != SCHA_ERR_NOERR) {
		/*
		 * SCMSGS
		 * @explanation
		 * A call to scha_cluster_open() failed.
		 * @user_action
		 * Contact your authorized Sun service provider to determine
		 * whether a workaround or patch is available.
		 */
		syslog(LOG_ERR, "scha_cluster_open() failed: %s\n",
		    scha_strerror(err));
		log_debug_message("Leaving find_zone_mds_list...\n");
		return (err);
	}

	if ((err = scha_cluster_get(clust_handle, SCHA_ALL_RESOURCETYPES,
			&rt_list)) != SCHA_ERR_NOERR) {
		if (err == SCHA_ERR_SEQID && retries < zc_scha_max_retries) {
			log_debug_message("cluster configuration is "
			    "changed, retrying get resource types.");
			(void) scha_cluster_close(clust_handle);
			retries++;
			(void) sleep(zc_scha_retry_wait);
			goto retry_clstr_get;
		} else {
			/*
			 * SCMSGS
			 * @explanation
			 * A call to scha_cluster_get() failed.
			 * @user_action
			 * Contact your authorized Sun service provider
			 * to determine whether a workaround or patch is
			 * available.
			 */
			syslog(LOG_ERR, "scha_cluster_get() failed: %s\n",
			    scha_strerror(err));
			(void) scha_cluster_close(clust_handle);
			log_debug_message("Leaving find_our_mds_rs...\n");
			return (err);
		}
	}

	if (rt_list == NULL || (rt_list->str_array[0]) == NULL) {
		/*
		 * No registered resource types in the base cluster.
		 * While this is unusual, I do not think it to be an error.
		 */
		(void) scha_cluster_close(clust_handle);
		log_debug_message("Leaving find_zone_mds_list...\n");
		return (SCHA_ERR_NOERR);
	}

	/* For each SUNW.qfs RT, check if there's a resource */
	for (i = 0; i < (int)(rt_list->array_cnt); i++) {
		if (strncmp(rt_list->str_array[i], SUNW_QFS, strlen(SUNW_QFS))
		    == 0) {
			if ((err = add_mds_list(mds_rs_list,
			    rt_list->str_array[i],
			    zone_root)) != SCHA_ERR_NOERR) {
				(void) scha_cluster_close(clust_handle);
				log_debug_message("find_zone_mds_list...\n");
				return (err);
			}
		}
	}

	(void) scha_cluster_close(clust_handle);
	log_debug_message("Leaving find_zone_mds_list...\n");
	return (err);
}

/*
 *	Name:		is_mds_in_zone
 *	Argument:
 *			1. Name of an MDS Resource
 *			2. zone root
 *			3. Address of an integer variable
 *				(output argument)
 *			4. Address of a string pointer to return the group name
 *				(output argument)
 *	Return:
 *			SCHA_ERR_NOERR - successfully checked the extension
 *					 property of the MDS Resource
 *			SCHA_ERR_INTERNAL - Internal error occurred.
 *
 *	This is a helper function that is called by add_mds_list(). This
 *	function retrieves the "QFSFileSystem" extension property and checks
 *	if it is mounted under the zone_root in argument 2.
 */
scha_err_t
is_mds_in_zone(const char *rs_name, const char *zone_root, int *result)
{
	scha_err_t		err = SCHA_ERR_NOERR;
	scha_resource_t		rs_handle;
	scha_extprop_value_t	*prop_val = NULL;
	int			i = 0;
	uint_t			retries = 0;

	log_debug_message("Entered into check_mds_rs...\n");

retry_get_mp:
	if ((err = scha_resource_open(rs_name, NULL, &rs_handle))
				!= SCHA_ERR_NOERR) {
		/*
		 * SCMSGS
		 * @explanation
		 * An error occurred while the handle for the resource
		 * was being retrieved.
		 * @user_action
		 * Investigate possible RGM errors or DSDL errors.
		 * Contact your authorized Sun service provider for
		 * assistance in diagnosing the problem.
		 */
		syslog(LOG_WARNING,
		    "Failed to retrieve the resource handle of %s: %s.",
		    rs_name, scha_strerror(err));
		return (err);
	}

	if ((err = scha_resource_get(rs_handle, SCHA_EXTENSION,
	    QFSFILESYSTEM, &prop_val)) != SCHA_ERR_NOERR) {
		if (err == SCHA_ERR_SEQID && retries < zc_scha_max_retries) {
			log_debug_message("cluster configuration is "
			    "changed, retrying get QFSFileSystemypes "
			    "extension property");
			(void) scha_cluster_close(rs_handle);
			retries++;
			(void) sleep(zc_scha_retry_wait);
			goto retry_get_mp;
		} else {
			/*
			 * SCMSGS
			 * @explanation
			 * An error occurred while determining the resource
			 * property of specified resource.
			 * @user_action
			 * Investigate possible RGM errors or DSDL errors.
			 * Contact your authorized Sun service provider for
			 * assistance in diagnosing the problem with copy of
			 * the /var/adm/messages files on all nodes.
			 */
			syslog(LOG_WARNING,
			    "Failed to retrieve the resource property "
			    "%s of %s: %s.",
			    QFSFILESYSTEM, rs_name, scha_strerror(err));
			goto cleanup;
		}
	}
	if (prop_val->val.val_strarray->array_cnt == 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * The property 'QFSFileSystem' should contain at least one
		 * entry.
		 * @user_action
		 * Ensure that the metadata server resource was created
		 * properly. For information about how to configure the shared
		 * QFS file system with Sun Cluster, see your Sun Cluster
		 * documentation and your Sun StorEdge QFS documentation.
		 */
		syslog(LOG_WARNING,
		    "Extension property \"%s\" of MDS resource %s is "
		    "empty.\n", QFSFILESYSTEM, rs_name);
		err = SCHA_ERR_INTERNAL;
		goto cleanup;
	}

	for (i = 0; i < (int)(prop_val->val.val_strarray->array_cnt); i ++) {
		if (strncmp(prop_val->val.val_strarray->str_array[i],
		    zone_root, strlen(zone_root)) == 0) {
			*result = 1;
			break;
		}
	}

cleanup:
	(void) scha_resource_close(rs_handle);
	log_debug_message("Leaving is_mds_in_zone...\n");
	return (err);
}

/*
 * Adds string str to string array sa.
 */
scha_err_t
add_to_strarray(scha_str_array_t *sa, const char *str)
{
	char		**ptr;

	if (sa == NULL) {
		/*
		 * SCMSGS
		 * @explanation
		 * An internal programming error has been detected.
		 * @user_action
		 * Contact your authorized Sun service provider to
		 * determine whether a workaround or patch is available.
		 */
		syslog(LOG_WARNING, "Internal error, trying to add a string "
		    "to an invalid string array.");
		return (SCHA_ERR_INVAL);
	}

	/* Check whether the input string is NULL */
	if (str == NULL || str[0] == '\0') {
		/* If the input string is NULL, return success */
		log_debug_message("Null value specified.");
		return (SCHA_ERR_NOERR);
	}

	if (sa->array_cnt == 0) {
		ptr = (char **)malloc(2 * sizeof (char *));
	} else {
		ptr = (char **)realloc(sa->str_array,
		    (sa->array_cnt + 2) * (sizeof (char *)));
	}
	if (ptr == NULL) {
		syslog(LOG_ERR, "Out of memory.");
		return (SCHA_ERR_NOMEM);
	}
	sa->str_array = ptr;
	sa->str_array[sa->array_cnt] = strdup(str);
	if (sa->str_array[sa->array_cnt] == NULL) {
		syslog(LOG_ERR, "Out of memory.");
		return (SCHA_ERR_NOMEM);
	}
	sa->array_cnt++;
	sa->str_array[sa->array_cnt] = NULL;
	return (SCHA_ERR_NOERR);
}

/*
 * Checks the existence of given string in the string array.
 */
boolean_t
is_str_in_strarray(const scha_str_array_t *sa, const char *str)
{
	uint_t  i;
	for (i = 0; i < sa->array_cnt; i++) {
		if (strcmp(str, sa->str_array[i]) == 0) {
			return (B_TRUE);
		}
	}
	return (B_FALSE);
}

int
execute_cmd(const char *cmd, char *cmd_out, uint_t cmd_out_len,
    char *err_str, uint_t err_str_len)
{
	FILE	*stream;
	uint_t	cur_cmd_out_len = 0;
	char	buf[BUFSIZE];
	int	status;

	/*
	 * Initialize with null byte for command output and error strings.
	 */
	(void) snprintf(err_str, err_str_len, "\0");
	(void) snprintf(cmd_out, cmd_out_len, "\0");

	/*
	 * Execute the command.
	 */
	if ((stream = popen(cmd, "r")) == NULL) {
		int ret = errno;	/*lint !e746 */
		(void) snprintf(err_str, err_str_len,
		    "Failed to execute the command %s : %s.",
		    cmd, strerror(ret));
		return (1);
	}

	/*
	 * Drain the output and store in command output buffer if required.
	 */
	while (fgets(buf, BUFSIZE, stream)) {
		if ((cmd_out == NULL) || (cmd_out_len == 0)) {
			continue;
		}
		(void) snprintf(cmd_out + cur_cmd_out_len,
		    cmd_out_len - cur_cmd_out_len,
		    "%s", buf);
		cur_cmd_out_len += strlen(buf);
	}

	if ((status = pclose(stream)) == -1) {
		(void) snprintf(err_str, err_str_len,
		    "Failed to obatin the return status for command %s : %s.",
		    cmd, strerror(errno));
		return (1);
	}

	/*
	 * Check the status of command execution.
	 */
	if (WIFEXITED(status)) {
		/*
		 * Execution completed normally.
		 */
		return (WEXITSTATUS((uint_t)status));
	} else if (WIFSIGNALED(status)) {
		/*
		 * Execution terminated by signal.
		 */
		(void) snprintf(err_str, err_str_len,
		    "Command %s execution is terminated by signal %d.",
		    cmd,  WTERMSIG((uint_t)status));
		return (1);
	} else if (WIFSTOPPED(status)) {
		/*
		 * Execution is stopped by signal.
		 */
		(void) snprintf(err_str, err_str_len,
		    "Command %s execution is stopped by signal %d.",
		    cmd,  WSTOPSIG((uint_t)status));
		return (1);
	} else {
		(void) snprintf(err_str, err_str_len,
		    "Command %s execution failed.",
		    cmd);
		return (1);
	}
}

uint_t	zc_scha_retry_wait;
uint_t	zc_scha_max_retries;
uint_t	zc_mds_check_timeout;

/*
 * This functions reads tunables from the configuartion files.
 * The tunables will be effective in the following ascending order
 * 1. Default values
 * 2. Gloabl zone cluster configuartion file
 *    /etc/cluster/zc/zc_halt_boot.conf
 * 3. Specific zone cluster configuration file
 *    /etc/cluster/zc/<zc_name>/zc_halt_boot.conf
 * That means the values in specfic ZC file will have highest priority.
 */
void
read_zc_halt_boot_config(const char *zcname)
{
	char	conf_file[MAXPATHLEN];

	/*
	 * Initialize with the deafult values.
	 */
	zc_scha_retry_wait = SCHA_RETRY_WAIT;
	zc_scha_max_retries = SCHA_MAX_RETRIES;
	zc_mds_check_timeout = MDS_CHECK_TIMEOUT;

	/*
	 * Check for the global zc_halt_boot configuration file and overwrite
	 * the default values.
	 */
	(void) snprintf(conf_file, MAXPATHLEN, "%s/%s",
	    ZC_HALT_BOOT_CONF_DIR, ZC_HALT_BOOT_CONF_FILE);
	if (access(conf_file, R_OK) < 0) {
		int rc = errno;
		if (rc != ENOENT) {
			/*
			 * SCMSGS
			 * @explanation
			 * The zone handler failed to access the specified
			 * configuration file.
			 * @user_action
			 * Check for the error message and rectify the problem.
			 */
			syslog(LOG_WARNING, "Access error to %s : %s.",
			    conf_file, strerror(rc));
		}
	} else {
		parse_zc_halt_boot_file(conf_file);
	}

	/*
	 * Check for the specific zc_halt_boot configuration file and
	 * overwrite the above tunable values.
	 */
	(void) snprintf(conf_file, MAXPATHLEN, "%s/%s/%s",
	    ZC_HALT_BOOT_CONF_DIR, zcname, ZC_HALT_BOOT_CONF_FILE);
	if (access(conf_file, R_OK) < 0) {
		int rc = errno;
		if (rc != ENOENT) {
			syslog(LOG_WARNING, "Access error to %s : %s.",
			    conf_file, strerror(rc));
		}
	} else {
		parse_zc_halt_boot_file(conf_file);
	}

	/*
	 * SCMSGS
	 * @explanation
	 * This is a notification message about the tunables used by zone
	 * handler.
	 * @user_action
	 * This is an informational message; no user action is needed.
	 */
	syslog(LOG_NOTICE, "Using the tunable values %s=%d, %s=%d, %s=%d.",
	    ZC_SCHA_RETRY_WAIT, zc_scha_retry_wait,
	    ZC_SCHA_MAX_RETRIES, zc_scha_max_retries,
	    ZC_MDS_CHECK_TIMEOUT, zc_mds_check_timeout);
}

void
parse_zc_halt_boot_file(char *file_path)
{
	FILE		*fp;
	char		*key, *value, *lasts;
	char		buffer[BUFSIZE];
	uint_t		len, i;
	int		line = 0;
	boolean_t	empty_line;
	int		val;

	fp = fopen(file_path, "r");
	if (fp == NULL) {
		int rc = errno;
		/*
		 * SCMSGS
		 * @explanation
		 * The zone handler failed to open the specified
		 * configuration file.
		 * @user_action
		 * Check for the error message and rectify the problem.
		 */
		syslog(LOG_WARNING,
		    "Failed to open the configuration file %s : %s.",
		    file_path, strerror(rc));
		return;
	}

	while (fgets(buffer, BUFSIZE, fp)) {

		line++;
		/* Ignore the comments line */
		if (buffer[0] == '#') {
			continue;
		}

		len = strlen(buffer);
		if (buffer[len -1] != '\n') {
			/* Ignore this line as this too long */
			/*
			 * SCMSGS
			 * @explanation
			 * The number of characters in specified file file
			 * has exceeded and hence ignoring the line.
			 * @user_action
			 * Rectify the line if it contains tunable variable.
			 */
			syslog(LOG_INFO, "Ignoring the line %d of file %s as "
			    "it exceeds %s characters.",
			    line, file_path, BUFSIZE);
			continue;
		}
		buffer[len -1] = '\0';

		/* Ignore the whitespaces line. */
		empty_line = B_TRUE;
		for (i = 0; i < len -1; i++) {
			if (!isspace(buffer[i])) {
				empty_line = B_FALSE;
				break;
			}
		}
		if (empty_line) {
			continue;
		}

		/* Get the key, value pair. */
		key = strtok_r(buffer, "=", &lasts);
		value = strtok_r(NULL, "=", &lasts);
		if ((key == NULL) || (value == NULL)) {
			/*
			 * SCMSGS
			 * @explanation
			 * The zone handler detected that key=value pair
			 * is not specified in appropraite manner.
			 * @user_action
			 * Check the line number and ensure that tunables
			 * are kept in the format 'key'='value'.
			 */
			syslog(LOG_WARNING, "Incorrect specification of "
			    "tunable at line %d in file %s.",
			    line, file_path);
			continue;
		}

		/* Skip the leading whitespace of key and value. */
		while (isspace(*key)) {
			key++;
		}
		while (isspace(*value)) {
			value++;
		}

		/* Skip the trailing whitespace of key and value. */
		len = strlen(key);
		while (isspace(key[len-1])) {
			key[len-1] = '\0';
			len--;
		}
		len = strlen(value);
		while (isspace(value[len-1])) {
			value[len-1] = '\0';
			len--;
		}

		/* Check if the key matches to any tunable. */
		val = atoi(value);
		if (strcasecmp(key, ZC_SCHA_RETRY_WAIT) == 0) {
			if (val < MIN_SCHA_RETRY_WAIT) {
				/*
				 * SCMSGS
				 * @explanation
				 * The zone handler detected a bad value for
				 * the specified tunable in the specified
				 * configuration file.
				 * @user_action
				 * Fix the specified tunable value so that it
				 * meets the restrictions.
				 */
				syslog(LOG_NOTICE, "The %s value should be "
				    "greater than %d seconds: Hence ignoring "
				    "this value from %s file at line %d.",
				    ZC_SCHA_RETRY_WAIT, MIN_SCHA_RETRY_WAIT,
				    file_path, line);
				continue;
			}
			zc_scha_retry_wait = (uint_t)val;
		} else if (strcasecmp(key, ZC_SCHA_MAX_RETRIES) == 0) {
			if (val < MIN_SCHA_MAX_RETRIES) {
				/*
				 * SCMSGS
				 * @explanation
				 * The zone handler detected a bad value for
				 * the specified tunable in the specified
				 * configuration file.
				 * @user_action
				 * Fix the specified tunable value so that it
				 * meets the restrictions.
				 */
				syslog(LOG_NOTICE, "The %s value should be "
				    "greater than %d: Hence ignoring "
				    "this value from %s file at line %d.",
				    ZC_SCHA_MAX_RETRIES, MIN_SCHA_MAX_RETRIES,
				    file_path, line);
				continue;
			}
			zc_scha_max_retries = (uint_t)val;
		} else if (strcasecmp(key, ZC_MDS_CHECK_TIMEOUT) == 0) {
			if (val < MIN_MDS_CHECK_TIMEOUT) {
				syslog(LOG_NOTICE, "The %s value should be "
				    "greater than %d seconds: Hence ignoring "
				    "this value from %s file at line %d.",
				    ZC_MDS_CHECK_TIMEOUT, MIN_MDS_CHECK_TIMEOUT,
				    file_path, line);
				continue;
			}
			zc_mds_check_timeout = (uint_t)val;
		}
	}
	(void) fclose(fp);
}

void
log_debug_message(const char *msg, ...)
{
	va_list			ap;

	if (!debug_mode) {
		return;
	}
	va_start(ap, msg);	/*lint !e40 */
	vsyslog(LOG_DEBUG, msg, ap);
	va_end(ap);
}
