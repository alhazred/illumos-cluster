/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)zc_sqfs_halt.c	1.9	08/09/25 SMI"

/*
 * zc_sqfs_halt is the cluster brand handler for zone halt. Its purpose
 * is to make sure that no shared qfs file systems mounted under the
 * zone root of the halting zone is acting as the metadata server. If
 * it is, then we attempt to move the metadata server to another node
 * by swithcing over the HA agent that manages the metadata server.
 * This is needed because the Solaris zone halt code will unmount all
 * file systems under the zone root, and shared qfs cannot recover gracefully
 * from an unmount of a file system that is acting as the metadata server.
 * It is also acceptable if this zone is acting as metadata server and this
 * last client for the shared qfs file system.
 */

/*lint -e801 */

#include "zc_sqfs.h"

/*
 * usage: zc_sqfs_halt [-D] zone_name zone_root
 * exits 0 on success, 1 on error.
 */

void
usage(const char *progname)
{
	(void) fprintf(stderr,
	    "usage: %s [-D] zone_name zone_root\n", progname);
}

int
main(int argc, char * const argv[])
{
	int			c;
	char			*zone_root;
	char			*zone_name;
	char			*mds_group;
	unsigned		i;
	uint_t			retries;
	scha_err_t		err = SCHA_ERR_NOERR;
	scha_errmsg_t		scha_status;
	scha_str_array_t	mds_rs_list;
	int			exit_val = 0;
	scha_resource_t		rs_handle;
	scha_status_value_t	*statval = NULL;
	scha_str_array_t	mds_rg_list;
	char			cmd[CMDSIZE];
	char			msg_tag[MSG_TAG_LEN];

	/* initialize mds_rs_list */
	mds_rs_list.array_cnt = 0;
	mds_rs_list.is_ALL_value = 0;
	mds_rs_list.str_array = NULL;

	/* initialize mds_rg_list */
	mds_rg_list.array_cnt = 0;
	mds_rg_list.str_array = NULL;


	while ((c = getopt(argc, argv, "D")) != -1) {
		switch (c) {
		case 'D':
			debug_mode = 1;
			break;
		default:
			usage(argv[0]);
			exit(1);
		}
	}
	if ((argc-optind) != 2) {
		usage(argv[0]);
		exit(1);
	}
	zone_name = argv[optind];
	zone_root = argv[optind+1];

	/*
	 * Initialize the message log tag.
	 */
	(void) snprintf(msg_tag, MSG_TAG_LEN,
	    "Cluster.ZC.%s.zc_halt", zone_name);
	openlog(msg_tag, LOG_CONS|LOG_NDELAY, LOG_DAEMON);

	/*
	 * Read the configuration file and initialize the values.
	 */
	read_zc_halt_boot_config(zone_name);


	syslog(LOG_NOTICE, "Started obtaining the list of SUNW.qfs "
	    "resources that are configured for this zone cluster.");

	/*
	 * Get the list of shared QFS metadata resources with mount points
	 * within this zone.
	 */
	if (find_zone_mds_list(&mds_rs_list, zone_root) != SCHA_ERR_NOERR) {
		syslog(LOG_WARNING, "Failed to obtain the list of SUNW.qfs "
		    "resources that are configured for this zone cluster.");
		closelog();
		exit(1);
	}

	if (mds_rs_list.array_cnt == 0) {
		syslog(LOG_NOTICE, "No SUNW.qfs resources configured for this "
		    "zone cluster.");
		closelog();
		exit(0);
	}

	syslog(LOG_NOTICE, "Successfully obtained the list of SUNW.qfs "
	    "resources that are configured for this zone cluster.");

	/*
	 * SCMSGS
	 * @explanation
	 * This is a notification message that the checking of the status of
	 * SUNW.qfs resources is started.
	 * @user_action
	 * This is an informational message; no user action is needed.
	 */
	syslog(LOG_NOTICE, "Checking for the status of SUNW.qfs resources "
	    "that are configured for this zone cluster.");

	/*
	 * Now that we have a list of resources that manage shared QFS metadata
	 * servers, try to switch them to other nodes.
	 */
	for (i = 0; i < mds_rs_list.array_cnt; i++) {
		retries = 0;
retry_rs:
		if ((err = scha_resource_open(mds_rs_list.str_array[i], NULL,
					&rs_handle)) != SCHA_ERR_NOERR) {
			syslog(LOG_WARNING,
			    "Failed to retrieve the resource handle of %s: %s.",
			    mds_rs_list.str_array[i], scha_strerror(err));
			exit_val = 1;
			continue;
		}
		statval = NULL;
		/* Check to see if the resource is on-line on this node */
		if ((err = scha_resource_get(rs_handle, SCHA_STATUS,
		    &statval)) != SCHA_ERR_NOERR || statval == NULL) {
			if (err == SCHA_ERR_SEQID &&
			    retries < zc_scha_max_retries) {
				log_debug_message("cluster configuration is "
				    "changed, retrying get status for "
				    "resource %s", mds_rs_list.str_array[i]);
				(void) scha_resource_close(rs_handle);
				retries++;
				(void) sleep(zc_scha_retry_wait);
				goto retry_rs;
			}
			syslog(LOG_WARNING,
			    "Failed to retrieve the resource property "
			    "%s of %s: %s.",
			    SCHA_STATUS, mds_rs_list.str_array[i],
			    scha_strerror(err));
			(void) scha_resource_close(rs_handle);
			exit_val = 1;
			continue;
		}
		if (statval->status != SCHA_RSSTATUS_OK) {
			/*
			 * SCMSGS
			 * @explanation
			 * The specified SUNW.qfs resource is not online on this
			 * node. That means the MDS server(s) of shared qfs
			 * file systems of this resource will not be running
			 * on this node. Hence skipping the resource.
			 * @user_action
			 * This is an informational message; no user action is
			 * needed.
			 */
			syslog(LOG_NOTICE,
			    "The SUNW.qfs resource '%s' is not online on this "
			    "node. Skipping this resource ...",
			    mds_rs_list.str_array[i]);
			/* Metadata server agent not online on this node */
			(void) scha_resource_close(rs_handle);
			continue;
		}
		/* get the resource group name of the metadata server agent */
		if ((err = scha_resource_get(rs_handle, SCHA_GROUP,
		    &mds_group)) != SCHA_ERR_NOERR) {
			if (err == SCHA_ERR_SEQID &&
			    retries < zc_scha_max_retries) {
				log_debug_message("cluster configuration is "
				    "changed, retrying get status for "
				    "resource %s", mds_rs_list.str_array[i]);
				(void) scha_resource_close(rs_handle);
				retries++;
				(void) sleep(zc_scha_retry_wait);
				goto retry_rs;
			}
			syslog(LOG_WARNING,
			    "Failed to retrieve the resource property "
			    "%s of %s: %s.",
			    SCHA_GROUP, mds_rs_list.str_array[i],
			    scha_strerror(err));
			(void) scha_resource_close(rs_handle);
			exit_val = 1;
			continue;
		}

		/*
		 * SCMSGS
		 * @explanation
		 * The zone halt handler is trying to move the resource group
		 * form this node so that MDS server(s) of shared qfs file
		 * systems configured in this resource group will not be
		 * running on this node.
		 * @user_action
		 * This is an informational message; no user action is needed.
		 */
		syslog(LOG_NOTICE,
		    "Trying to switch over the resource group '%s' of "
		    "resource '%s' from this node.",
		    mds_group, mds_rs_list.str_array[i]);

		/* Move the resource group off this node */
		if ((err = scha_control(SCHA_GIVEOVER, mds_group,
		    mds_rs_list.str_array[i])) != SCHA_ERR_NOERR) {
			switch (err) {

			case SCHA_ERR_RECONF:
				/*
				 * Cluster is already reconfiguring. Who knows
				 * what will be the state afterward, so restart
				 * the entire givover process.
				 */
				(void) scha_resource_close(rs_handle);
				if (retries >= zc_scha_max_retries) {
					/*
					 * SCMSGS
					 * @explanation
					 * Repeated reconfiguration errors
					 * while attempting to switch over the
					 * QFS meta data resource group is
					 * likely an error in the cluster
					 * software.
					 * @user_action
					 * It is likely that the shared QFS
					 * file system meta data server has
					 * been forcibly unmounted. This can
					 * cause problems with proper operation
					 * of shared QFS on other nodes. Verify
					 * that shared QFS filesystem in use are
					 * working properly. If they are not,
					 * follow the QFS file system recovery
					 * procedures. If this problem persists
					 * contact your authorized Sun service
					 * provider to determine whether a
					 * workaround or patch is available.
					 */
					syslog(LOG_WARNING,
					    "Giveover attempt failed "
					    "for %s after exausting retries: "
					    "resource group is currently stuck"
					    "in reconfiguration.", mds_group);
					exit_val = 1;
					continue;
				}
				log_debug_message("Cluster undergoing "
				    "reconfiguration. Will retry moving "
				    "resource group %s.", mds_group);
				(void) scha_resource_close(rs_handle);
				retries++;
				(void) sleep(zc_scha_retry_wait);
				goto retry_rs;

			case SCHA_ERR_STATE:
				/*
				 * One or more resources are either starting or
				 * stopping Resource Group is not in a valid
				 * state for giveover. Retry the operation.
				 */
				(void) scha_resource_close(rs_handle);
				if (retries >= zc_scha_max_retries) {
					/*
					 * SCMSGS
					 * @explanation
					 * Got an error attempting to switch
					 * over the QFS meta data resource
					 * group. This error has persisted until
					 * all retries have been exhausted.
					 * @user_action
					 * Verify that the cluster is
					 * correctly configured. If it is,
					 * contact your authorized Sun service
					 * provider to determine whether a
					 * workaround or patch is available.
					 */
					syslog(LOG_WARNING,
					    "Giveover attempt failed "
					    "for %s: resource group is "
					    "currently starting or stopping. "
					    "retries exhausted.", mds_group);
					exit_val = 1;
					continue;
				}
				/*
				 * SCMSGS
				 * @explanation
				 * Got an error attempting to switch over the
				 * QFS meta data resource group. This is
				 * probably a transient error because the
				 * resource group is being modified.
				 * @user_action
				 * If this problem persists, verify that the
				 * the resource group is configured correctly.
				 */
				syslog(LOG_WARNING,
				    "Giveover attempt failed for %s: "
				    "resource group is currently starting or "
				    "stopping. Will retry.", mds_group);
				retries++;
				(void) sleep(zc_scha_retry_wait);
				goto retry_rs;

			case SCHA_ERR_METHOD:
				/*
				 * Failed to start the resource group on
				 * a node.
				 * This might happen when the potential node
				 * went down after checking that the node
				 * can be potential primary.
				 */
				(void) scha_resource_close(rs_handle);
				if (retries >= zc_scha_max_retries) {
					/*
					 * SCMSGS
					 * @explanation
					 * An error occurred while attempting
					 * to switch over the QFS meta data
					 * resource group.
					 * This error persisted until
					 * all retries were exhausted.
					 * @user_action
					 * Verify that the cluster is
					 * correctly configured. If it is,
					 * contact your authorized Sun service
					 * provider to determine whether a
					 * workaround or patch is available.
					 */
					syslog(LOG_WARNING,
					    "Giveover attempt failed "
					    "for %s: resource group is "
					    "unable to bring online on a "
					    "potential primary node. "
					    "retries exhausted.", mds_group);
					exit_val = 1;
					continue;
				}
				/*
				 * SCMSGS
				 * @explanation
				 * An error occurred while attempting to switch
				 * over the QFS meta data resource group.
				 * This is probably a transient error because
				 * the node might have gone down when resource
				 * group was about to be brought online.
				 * @user_action
				 * If this problem persists, verify that the
				 * the resource group is configured correctly.
				 */
				syslog(LOG_WARNING,
				    "Giveover attempt failed for %s: "
				    "resource group is unable to bring "
				    "online on a potential primary node. "
				    "Will retry.", mds_group);
				retries++;
				(void) sleep(zc_scha_retry_wait);
				goto retry_rs;
			case SCHA_ERR_CHECKS:
				/*
				 * SCMSGS
				 * @explanation
				 * No other node found that can host
				 * this RG. This is Ok, since this is
				 * the last node sharing the QFS if
				 * this is a properly configured
				 * cluster. Just let Solaris unmount
				 * the QFS file system.
				 * @user_action
				 * Informational message.
				 */
				syslog(LOG_INFO, "Giveover attempt failed: "
				    "no other node is currently available to "
				    "host the resource group. Should be OK as "
				    "no other node should be accessing the "
				    "QFS file system if this is the last MDS "
				    "node.");
				/*
				 * Add to the MDS RG list which we used to
				 * offline the RG later.
				 */
				if (!is_str_in_strarray(&mds_rg_list,
				    mds_group)) {
					if ((err = add_to_strarray(&mds_rg_list,
					    mds_group)) != SCHA_ERR_NOERR) {
						/*
						 * SCMSGS
						 * @explanation
						 * The zone halt handler failed
						 * to generate the SUNW.qfs
						 * resource group list.
						 * @user_action
						 * Investigate the zone halt log
						 * messages and try to rectify
						 * the problem. If problem
						 * persists after halting the
						 * zone, contact your authorized
						 * Sun service provider with
						 * copy of /var/adm/messages
						 * files on all nodes.
						 */
						syslog(LOG_WARNING,
						    "Failed to add '%s' to "
						    "MDS resource group list.",
						    mds_group);
						(void) scha_resource_close(
						    rs_handle);
						exit_val = 1;
						continue;
					}
				}
				break;

			default:
				/*
				 * SCMSGS
				 * @explanation
				 * Zone halt was unable to move the metadata
				 * server to a different node on zone shutdown.
				 * This is likely to cause operational problems
				 * with the shared QFS fileystems on nodes that
				 * remain up.
				 * @user_action
				 * Verify the operation of QFS file systems that
				 * were mounted on this zone cluster. If
				 * needed, correct by the appropriate QFS
				 * recovery procedure.
				 */
				syslog(LOG_WARNING,
				    "scha_control GIVEOVER of resource "
				    "group %s failed: %s", mds_group,
				    scha_strerror(err));
				exit_val = 1;
			}
		} else {
			/*
			 * SCMSGS
			 * @explanation
			 * The zone halt handler successfully switched over the
			 * specified SUNW.qfs resource group.
			 * @user_action
			 * This is an informational message; no user action is
			 * needed.
			 */
			syslog(LOG_NOTICE,
			    "Successfully switched over the resource group "
			    "'%s' of resource '%s' from this node.",
			    mds_group, mds_rs_list.str_array[i]);
		}
		(void) scha_resource_close(rs_handle);

		/*
		 * Delete the ping-pong file created by RGM for this resource.
		 * The existence of this file prevents to make this node
		 * as primary for this resource within ping-pong interval
		 * of the resource.
		 * XXX This approach is a workaround. The better one could be
		 * to have new API where RGM just switch over the given RG and
		 * does not impose any pingpong interval restriction for
		 * switching back.
		 */
		(void) snprintf(cmd, CMDSIZE, " %s %s/%s",
		    RM_CMD, GZ_PINPONG_DIR, mds_rs_list.str_array[i]);
		(void) execute_cmd(cmd, NULL, 0, NULL, 0);
	}

	/*
	 * SCMSGS
	 * @explanation
	 * This is a notification message that the checking for the
	 * SUNW.qfs resources is completed.
	 * @user_action
	 * This is an informational message; no user action is needed.
	 */
	syslog(LOG_NOTICE, "Completed checking the SUNW.qfs resources status "
	    "configured for this zone cluster.");

	/*
	 * Now we can explicitly bring offline the resource groups that didn't
	 * have other potential primary node as the QFS file systems are not
	 * mounted anywhere on the cluster after this node went done.
	 * The explicit resource group offline also avoids displaying the
	 * error messages by RGM about the failure of probes and failure of
	 * resource group bringing online on another nodes.
	 */
	if (mds_rg_list.array_cnt > 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * This is a notification message that zone halt handler is
		 * bringing the SUNW.qfs resource groups offline.
		 * @user_action
		 * This is an informational message; no user action is needed.
		 */
		syslog(LOG_NOTICE, "Bringing the resource groups "
		    "containing SUNW.qfs resources offline.");
		scha_status = rgm_scswitch_switch_rg(NULL,
		    (const char **)mds_rg_list.str_array, RGACTION_NONE, 0,
		    B_FALSE, GLOBAL_ZONENAME);
		if (scha_status.err_code != SCHA_ERR_NOERR) {
			/*
			 * SCMSGS
			 * @explanation
			 * The zone halt handler failed to bring SUNW.qfs
			 * resource groups offline successfully.
			 * @user_action
			 * Check for the system and zone halt logs to analyze
			 * the possible problem. After rectifying the problem
			 * try halting the zone cluster node. If the problem
			 * still persists contact your authorized Sun service
			 * provider with a copy of /var/adm/messages files on
			 * all nodes.
			 */
			syslog(LOG_WARNING, "Failed to bring resource groups "
			    "containing SUNW.qfs resources offline : %s.",
			    scha_strerror(scha_status.err_code));
			exit_val = 1;
		} else {
			/*
			 * SCMSGS
			 * @explanation
			 * This is a notification message that zone halt brought
			 * the SUNW.qfs resource groups offline successfully.
			 * @user_action
			 * This is an informational message; no user action is
			 * needed.
			 */
			syslog(LOG_NOTICE, "The resource groups "
			    "containing SUNW.qfs resources brought "
			    "offline successfully.");
		}
	}
	if (exit_val) {
		/*
		 * SCMSGS
		 * @explanation
		 * The zone halt handler failed to complete its action
		 * successfully.
		 * @user_action
		 * Check for the system and zone halt logs to analyze the
		 * problem. After rectifying the problem try halting the node.
		 * If the problem still persists contact your authorized Sun
		 * service provider with a copy of /var/adm/messages files on
		 * all nodes.
		 */
		syslog(LOG_ERR, "There might be still metadata server "
		    "of configured SUNW.qfs resources running on this node "
		    "which can prevent the sQFS file systems to be unmounted "
		    "by Solaris during zone halt. "
		    "Zone halt operation is not successful.");
	} else {
		/*
		 * SCMSGS
		 * @explanation
		 * The zone halt handler completed its operation successfully.
		 * @user_action
		 * This is an informational message; no user action is needed.
		 */
		syslog(LOG_NOTICE, "The metadata server of configured "
		    "SUNW.qfs resources either moved to other node or this "
		    "node is acting as metadata server being last client of "
		    "shared qfs file systems. "
		    "Zone halt operation completed successfully.");
	}
	closelog();
	exit(exit_val);
}
