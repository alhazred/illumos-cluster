/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)zc_sqfs_boot.c	1.2	08/10/20 SMI"

/*
 * zc_sqfs_boot is the cluster brand handler for zone boot. Its purpose
 * is to ensure that the shared qfs file systems configured under the booting
 * zone node path is mounted and also bring the resource groups of shared qfs
 * file system (SUNW.qfs) agent resources online. We need to bring the SUNW.qfs
 * agent resource groups online so that any dependent SUNW.ScalMountPoint
 * resources will be brought online transparently after zone boot.
 * The shared qfs file systems are mounted by invoking the INIT method of the
 * SUNW.qfs agent resources explicitly using an RGM API.
 */

/*lint -e801 */

#include "zc_sqfs.h"
#include <stdbool.h>
#include <libzccfg/libzccfg.h>

static char *switchover_mds_server(const char *, const char *, const char *,
    const scha_str_array_t *);
static char *get_mds_server_nodename(const char *);
static boolean_t is_rg_leader(const char *, const char *, const char *,
    const scha_str_array_t *);
static zc_node_status_t get_zc_node_status(char *, const char *);
static scha_err_t fill_rs_info(const char *, const char *, rs_info_t *);
static scha_err_t get_fsname_for_mntpt(char *, char **);

/*
 * usage: zc_sqfs_boot [-D] zone_name zone_root
 * exits 0 on success, 1 on error.
 */

void
usage(const char *progname)
{
	(void) fprintf(stderr,
	    "usage: %s [-D] zone_name zone_root\n", progname);
}

int
main(int argc, char * const argv[])
{
	int			c;
	char			*zone_name;
	char			*zone_root;
	uint_t			i, j;
	scha_err_t		err = SCHA_ERR_NOERR;
	scha_errmsg_t		scha_status = {SCHA_ERR_NOERR, NULL};
	int			exit_val = 0;
	scha_str_array_t	mds_rs_list;
	scha_str_array_t	run_init_r_list;
	scha_str_array_t	mds_rg_list;
	rs_info_t		mds_rs_info;
	scha_str_array_t	mds_server_node_list;
	char			*mds_server_name;
	char			msg_tag[MSG_TAG_LEN];

	/* initialize the lists */
	mds_rs_list.array_cnt = 0;
	mds_rs_list.str_array = NULL;

	run_init_r_list.array_cnt = 0;
	run_init_r_list.str_array = NULL;

	mds_rg_list.array_cnt = 0;
	mds_rg_list.str_array = NULL;

	mds_server_node_list.array_cnt = 0;
	mds_server_node_list.str_array = NULL;

	while ((c = getopt(argc, argv, "D")) != -1) {
		switch (c) {
		case 'D':
			debug_mode = 1;
			break;
		default:
			usage(argv[0]);
			exit(1);
		}
	}
	if ((argc-optind) != 2) {
		usage(argv[0]);
		exit(1);
	}
	zone_name = argv[optind];
	zone_root = argv[optind+1];

	/*
	 * Initialize the message log tag.
	 */
	(void) snprintf(msg_tag, MSG_TAG_LEN,
	    "Cluster.ZC.%s.zc_boot", zone_name);
	openlog(msg_tag, LOG_CONS|LOG_NDELAY, LOG_DAEMON);

	/*
	 * Read the configuration file and initialize the values.
	 */
	read_zc_halt_boot_config(zone_name);

	if (clconf_lib_init() != 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * Unable to initialize the libclconf library.
		 * @user_action
		 * Contact your authorized Sun service provider to
		 * determine whether a workaround or patch is available.
		 */
		syslog(LOG_ERR, "Internal Error: Failed to initialize "
		    "clconf library.");
		closelog();
		exit(1);
	}

	/*
	 * Get the list of shared QFS metadata resources with mount points
	 * under this zone path.
	 */

	/*
	 * SCMSGS
	 * @explanation
	 * This is a notification about retrieval of the configured SUNW.qfs
	 * resources for this zone cluster.
	 * @user_action
	 * This is an informational message; no user action is needed.
	 */
	syslog(LOG_NOTICE, "Started obtaining the list of SUNW.qfs "
	    "resources that are configured for this zone cluster.");

	if (find_zone_mds_list(&mds_rs_list, zone_root) != SCHA_ERR_NOERR) {
		/*
		 * SCMSGS
		 * @explanation
		 * Failed to get the list of SUNW.qfs
		 * resources that are configured for this zone cluster.
		 * @user_action
		 * Investigate using the log messages and rectify the
		 * problem. If problem persists after rebooting the zone
		 * contact your authorized Sun service provider with
		 * copy of /var/adm/messages files on all nodes.
		 */
		syslog(LOG_ERR, "Failed to obtain the list of SUNW.qfs "
		    "resources that are configured for this zone cluster.");
		closelog();
		exit(1);
	}

	if (mds_rs_list.array_cnt == 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * There are no SUNW.qfs resources configured for this
		 * zone cluster.
		 * @user_action
		 * This is an informational message; no user action is needed.
		 */
		syslog(LOG_NOTICE, "No SUNW.qfs resources configured for this "
		    "zone cluster.");
		closelog();
		exit(0);
	}

	/*
	 * SCMSGS
	 * @explanation
	 * This is a notification message about successful retrieval
	 * of configured SUNW.qfs resources for this zone cluster.
	 * @user_action
	 * This is an informational message; no user action is needed.
	 */
	syslog(LOG_NOTICE, "Successfully obtained the list of SUNW.qfs "
	    "resources that are configured for this zone cluster.");

	/*
	 * SCMSGS
	 * @explanation
	 * This is a notification message that the handler is going to
	 * verify the status of metadata servers of SUNW.qfs resources.
	 * @user_action
	 * This is an informational message; no user action is needed.
	 */
	syslog(LOG_NOTICE, "Verifying the SUNW.qfs resources metadata "
	    "server status.");
	/*
	 * Now we have a list of SUNW.qfs resources that manage shared
	 * QFS metadata servers configured for the zone cluster.
	 */
	for (i = 0; i < mds_rs_list.array_cnt; i++) {

		if (fill_rs_info(mds_rs_list.str_array[i], zone_name,
		    &mds_rs_info)) {

			/*
			 * Failed to get information about the resource.
			 * Skip this resource and try for the other resource
			 * as the intention is to bring the sQFS file systems
			 * to this node as best as we can.
			 */

			/*
			 * SCMSGS
			 * @explanation
			 * The zone boot handler failed to retrieve the
			 * specified resource information.
			 * @user_action
			 * Investigate the zone boot log messages and rectify
			 * the problem. If problem persists after rebooting
			 * the zone, contact your authorized Sun service
			 * provider with copy of /var/adm/messages files
			 * on all nodes.
			 */
			syslog(LOG_WARNING, "Failed to retrieve the resource "
			    "'%s' information. Skipping this resource ...",
			    mds_rs_list.str_array[i]);
			exit_val = 1;
			continue;
		}

		/*
		 * Skip the resource if this is in 'UNMANAGED' resource group.
		 */
		if (mds_rs_info.mds_rg_state == SCHA_RGSTATE_UNMANAGED) {

			/*
			 * SCMSGS
			 * @explanation
			 * The zone boot handler detected that specified
			 * resource is part of an unmanaged resource group and
			 * hence skipping processing the resource.
			 * @user_action
			 * This is an informational message; no user action
			 * is needed.
			 */
			syslog(LOG_NOTICE, "Resource '%s' is part of an "
			    "unmanaged resource group '%s'. "
			    "Skipping this resource ...",
			    mds_rs_list.str_array[i],
			    mds_rs_info.mds_rg_name);
			continue;
		}

		/*
		 * Ignore this resource, if the dependent SUNW.wait_zc_boot
		 * resource for this zone is not in online state.
		 * The intention of zone boot handler to invoke the INIT
		 * methods when the zone node is rebooted so that the sQFS file
		 * system will be mounted.
		 * If the dependent wait_zc_boot is not online state means the
		 * INIT method for this MDS resource is not yet invoked and
		 * INIT will be automatically called after this zone is up. This
		 * is case only during physical node boot.
		 * NOTE: The main reason for this check and to skip invoking
		 * INIT method during physical node boot is to avoid deadlock.
		 * During zc boot handler, the quiese step on the MDS resource
		 * will get blocked for dependent wait_zc_boot resource boot
		 * methods which this gets blocked for completion of
		 * zc boot handler.
		 */
		if (mds_rs_info.wait_zc_rs_state != SCHA_RSSTATE_ONLINE) {
			/*
			 * SCMSGS
			 * @explanation
			 * The dependent SUNW.wait_zc_boot resource of
			 * specified SUNW.qfs resource is not online. It
			 * means the shared qfs file systems are not mounted by
			 * INIT method of SUNW.qfs as they are blocked by
			 * INIT method of SUNW.wait_zc_boot resource.
			 * Hence skipping the resource to avoid invoking INIT
			 * method explicitly.
			 * @user_action
			 * This is an informational message; no user action
			 * is needed.
			 */
			syslog(LOG_NOTICE,
			    "The dependent SUNW.wait_zc_boot resource '%s' "
			    "of SUNW.qfs resource '%s' is not online. "
			    "Skipping this resource ...",
			    mds_rs_info.wait_zc_rs_name,
			    mds_rs_list.str_array[i]);
			continue;
		}

		/*
		 * Verify the MDS server status for each file system and
		 * do switch over if necessary so that the server is running
		 * on a node which are part of zone cluster.
		 */
		for (j = 0; j < mds_rs_info.mds_rs_qfs_list.array_cnt; j++) {

			mds_server_name = switchover_mds_server(
			    mds_rs_info.mds_rs_qfs_list.str_array[j],
			    zone_name, mds_rs_info.mds_rg_name,
			    mds_rs_info.mds_rg_nodelist);

			if (mds_server_name == NULL) {
				/*
				 * SCMSGS
				 * @explanation
				 * The zone boot handler is unable to determine
				 * the status of metadata server for the
				 * specified shared qfs file system.
				 * @user_action
				 * Investigate the zone boot log messages and
				 * try to rectify the problem. If problem
				 * persists after rebooting the zone,
				 * contact your authorized Sun service
				 * provider with copy of /var/adm/messages files
				 * on all nodes.
				 */
				syslog(LOG_WARNING,
				    "Failed to obatain the status of MDS for "
				    "'%s' file system of resource '%s'. "
				    "Skipping this file system ...",
				    mds_rs_info.mds_rs_qfs_list.str_array[j],
				    mds_rs_list.str_array[i]);
				exit_val = 1;
				continue;
			}

			/*
			 * SCMSGS
			 * @explanation
			 * This is a notification from the handler about the
			 * location of the node where metadata server is
			 * running.
			 * @user_action
			 * This is an informational message; no user action
			 * is needed.
			 */
			syslog(LOG_NOTICE, "The MDS for '%s' file system of "
			    "resource '%s' is running on node '%s'.",
			    mds_rs_info.mds_rs_qfs_list.str_array[j],
			    mds_rs_list.str_array[i],
			    mds_server_name);

			/*
			 * Add the MDS server node to the list.
			 */
			if (!is_str_in_strarray(&mds_server_node_list,
			    mds_server_name)) {
				if ((err = add_to_strarray(
				    &mds_server_node_list,
				    mds_server_name)) != SCHA_ERR_NOERR) {
					/*
					 * SCMSGS
					 * @explanation
					 * The zone boot handler failed to
					 * generate the list of MDS server
					 * nodes.
					 * @user_action
					 * Investigate the zone boot log
					 * messages and try to rectify the
					 * problem. If problem
					 * persists after rebooting the zone,
					 * contact your authorized Sun service
					 * provider with copy of
					 * /var/adm/messages files on all nodes.
					 */
					syslog(LOG_WARNING,
					    "Failed to add the MDS node '%s' "
					    "to the list of MDS nodes.",
					    mds_server_name);
					exit_val = 1;
					continue;
				}
			}
		}

		/*
		 * Add this resource to the list which requires to run INIT
		 * method.
		 */
		if ((err = add_to_strarray(&run_init_r_list,
		    mds_rs_list.str_array[i])) != SCHA_ERR_NOERR) {
			/*
			 * SCMSGS
			 * @explanation
			 * The zone boot handler failed to generate the
			 * list of SUNW.qfs resources.
			 * @user_action
			 * Investigate the zone boot log messages and
			 * try to rectify the problem. If problem
			 * persists after rebooting the zone, contact your
			 * authorized Sun service provider with copy of
			 * /var/adm/messages files on all nodes.
			 */
			syslog(LOG_WARNING,
			    "Failed to add resource '%s' to the list of "
			    "SUNW.qfs resource list.",
			    mds_rs_list.str_array[i]);
			exit_val = 1;
			continue;
		}

		/*
		 * Add the RG to a list.
		 */
		if (!is_str_in_strarray(&mds_rg_list,
		    mds_rs_info.mds_rg_name)) {
			if ((err = add_to_strarray(&mds_rg_list,
			    mds_rs_info.mds_rg_name)) != SCHA_ERR_NOERR) {
				/*
				 * SCMSGS
				 * @explanation
				 * The zone boot handler failed to generate the
				 * list resource groups containing SUNW.qfs
				 * resources.
				 * @user_action
				 * Investigate the zone boot log messages and
				 * try to rectify the problem. If problem
				 * persists after rebooting the zone,
				 * contact your authorized Sun service
				 * provider with copy of /var/adm/messages files
				 * on all nodes.
				 */
				syslog(LOG_WARNING,
				    "Failed to add resource group '%s' to the "
				    "MDS resource group list.",
				    mds_rs_info.mds_rg_name);
				exit_val = 1;
				continue;
			}
		}
	}

	/*
	 * SCMSGS
	 * @explanation
	 * This is a notification from the handler that it has completed
	 * verification of metadata servers status of SUNW.qfs resources.
	 * @user_action
	 * This is an informational message; no user action is needed.
	 */
	syslog(LOG_NOTICE, "Completed verification of SUNW.qfs resources "
	    "metadata server status.");

	if (run_init_r_list.array_cnt == 0) {
		/*
		 * We don't have any resources to run INIT method.
		 */
		/*
		 * SCMSGS
		 * @explanation
		 * There are no SUNW.qfs resources required to invoke INIT
		 * method.
		 * @user_action
		 * This is an informational message; no user action is needed.
		 */
		syslog(LOG_NOTICE, "No SUNW.qfs resource required to run "
		    "INIT method.");
		goto end;
	}

	/*
	 * Run the init methods for the QFS resources so that file systems will
	 * be mounted back which were earlier unmounted during a zone halt.
	 */

	/*
	 * SCMSGS
	 * @explanation
	 * This is a notification from the handler about requesting the RGM
	 * to mount the shared qfs file systems by invoking INIT method
	 * of SUNW.qfs resources.
	 * @user_action
	 * This is an informational message; no user action is needed.
	 */
	syslog(LOG_NOTICE, "Requesting RGM to mount QFS file systems.");

	err = scha_control_run_r_init(
	    (const char **)run_init_r_list.str_array);
	if (err != SCHA_ERR_NOERR) {
		/*
		 * SCMSGS
		 * @explanation
		 * The zone boot handler received failure status for the request
		 * to RGM to mount the shared qfs file systems.
		 * @user_action
		 * Investigate the zone boot log messages and rectify the
		 * problem. If the problem persists after rebooting the zone,
		 * contact your authorized Sun service provider with a
		 * copy of /var/adm/messages files on all nodes.
		 */
		syslog(LOG_WARNING, "Request to mount QFS file systems is "
		    "failed : %s.", scha_strerror(err));
		exit_val = 1;
	} else {
		/*
		 * SCMSGS
		 * @explanation
		 * This is a notification from the handler that the request to
		 * RGM to mount the shared qfs file systems returned without
		 * failure.
		 * @user_action
		 * This is an informational message; no user action is needed.
		 */
		syslog(LOG_NOTICE, "Request to mount QFS file systems "
		    "returned successfully.");
	}

	/*
	 * Wait for the above INIT methods execution to complete by making a
	 * quiese call on set of MDS resource groups.
	 */

	/*
	 * SCMSGS
	 * @explanation
	 * This is a notification from the handler that it is waiting for the
	 * completion of the request to RGM to mount shared qfs file systems.
	 * @user_action
	 * This is an informational message; no user action is needed.
	 */
	syslog(LOG_NOTICE, "Waiting for mount operation on QFS file systems "
	    "to complete.");

	(void) rgm_scswitch_quiesce_rgs((const char **)mds_rg_list.str_array,
	    NULL, 0, B_FALSE, GLOBAL_ZONENAME);

	/*
	 * Bring the MDS resource groups online.
	 */

	/*
	 * SCMSGS
	 * @explanation
	 * This is a notification from the handler that it is bringing the
	 * SUNW.qfs resource groups online.
	 * @user_action
	 * This is an informational message; no user action is needed.
	 */
	syslog(LOG_NOTICE, "Brining the resource groups containing "
	    "SUNW.qfs resources online.");

	if (mds_server_node_list.array_cnt == 1) {
		/*
		 * In case where there is one potential node to bring all
		 * resource groups we take the advantage to bring online
		 * by specifying the nodename.
		 */
		scha_status = rgm_scswitch_switch_rg(
		    (const char **)mds_server_node_list.str_array,
		    (const char **)mds_rg_list.str_array, RGACTION_ONLINE,
		    0, B_FALSE, GLOBAL_ZONENAME);
	} else {
		/*
		 * In case of multiple potential nodes, we are not sure which
		 * resource group can be online on which node. So we use
		 * RGACTION_REBALANCE which use general criteria of nodelist
		 * ordering and affinities to bring all the specified resource
		 * groups.
		 */
		scha_status = rgm_scswitch_switch_rg(
		    NULL, (const char **)mds_rg_list.str_array,
		    RGACTION_REBALANCE, 0, B_FALSE, GLOBAL_ZONENAME);
	}
	if (scha_status.err_code != SCHA_ERR_NOERR) {
		/*
		 * SCMSGS
		 * @explanation
		 * The zone boot handler failed to bring the SUNW.qfs resource
		 * groups online.
		 * @user_action
		 * Investigate the zone boot log messages and rectify the
		 * problem. If problem persists after rebooting the zone,
		 * contact your authorized Sun service provider with
		 * copy of /var/adm/messages files on all nodes.
		 */
		syslog(LOG_WARNING, "Failed to bring resource groups "
		    "containing SUNW.qfs resources online : %s.",
		    scha_strerror(scha_status.err_code));
		exit_val = 1;
	} else {

		/*
		 * SCMSGS
		 * @explanation
		 * This is a notification from the handler that it brought the
		 * SUNW.qfs resource groups online.
		 * @user_action
		 * This is an informational message; no user action is needed.
		 */
		syslog(LOG_NOTICE, "The resource groups containing "
		    "SUNW.qfs resources brought online successfully.");
	}

end:
	if (exit_val) {
		/*
		 * SCMSGS
		 * @explanation
		 * The zone boot handler failed to make the shared qfs file
		 * systems configured for this node available.
		 * @user_action
		 * Investigate the zone boot log messages and rectify the
		 * problem. If problem persists after rebooting the zone,
		 * contact your authorized Sun service provider with
		 * copy of /var/adm/messages files on all nodes.
		 */
		syslog(LOG_ERR, "All the sQFS file systems "
		    "configured to this node through SUNW.qfs resources "
		    "might not be available. "
		    "Zone boot operation is not successful.");
	} else {
		/*
		 * SCMSGS
		 * @explanation
		 * The zone boot handler successfully brought all the shared
		 * qfs file systems available to this node.
		 * @user_action
		 * This is an informational message; no user action is needed.
		 */
		syslog(LOG_NOTICE, "All the sQFS file systems configured to "
		    "this node through SUNW.qfs resources are available. "
		    "Zone boot operation completed successfully.");
	}
	closelog();
	return (exit_val);
}

/*
 * Switch over the MDS server if necessary.
 * The MDS server must be hosted on a machine where the zone cluster
 * is operational.
 * This function is returned only after confirming that the MDS is running on
 * a zone cluster node which is UP OR the timeout is completed.
 */
static char *
switchover_mds_server(const char *qfs_name, const char *zone_name,
    const char *rg_name, const scha_str_array_t *rg_nodelist)
{
	uint_t	i;
	char	*hostname = NULL;
	char	myhostname[MAXHOSTNAMELEN];
	uint_t	retries = 0;
	char	cmd[CMDSIZE];

	/*
	 * Fill the current hostname.
	 */
	(void) gethostname(myhostname, sizeof (myhostname));
restart:
	/*
	 * Obtain the current MDS nodeid for the file system.
	 */
	hostname = get_mds_server_nodename(qfs_name);
	if (hostname == NULL) {
		/* Unable to determine the MDS server */
		return (NULL);
	}

	/*
	 * Verify that nodename is part of RG node list.
	 */
	for (i = 0; i < rg_nodelist->array_cnt; i++) {
		if (strcmp(rg_nodelist->str_array[i], hostname) == 0) {
			break;
		}
	}
	if (i >= rg_nodelist->array_cnt) {
		/*
		 * MDS server nodename is not part of RG node list.
		 * This shouldn't happen. Prevent further processing.
		 */
		/*
		 * SCMSGS
		 * @explanation
		 * The zone boot handler found that the MDS is running on
		 * different node which is not part of configured
		 * resource-group node list.
		 * @user_action
		 * Check the location of the MDS and see whether it is running
		 * on the node which is in the resource-group node list.
		 * If that is the case contact your Sun service provider to
		 * determine whether a workaround or patch is available.
		 */
		syslog(LOG_WARNING,
		    "MDS server nodename '%s' of file system '%s' is not "
		    "in the nodelist of the resource group '%s'.",
		    hostname, qfs_name, rg_name);
		return (NULL);
	}

	if (get_zc_node_status(hostname, zone_name) == ZC_NODE_UP) {
		/* MDS Node is running on node which is part of cluster */
		return (hostname);
	} else {
		/*
		 * MDS server node is not part of the cluster.
		 * Try to switch over the server to this node if this is leader
		 * and then check for the status of MDS server again.
		 */
		if (is_rg_leader(myhostname, zone_name, rg_name, rg_nodelist)) {
			/*
			 * Form the command to switch over the MDS server.
			 * samsharefs -R -s <nodename> <qfs>
			 */
			/*
			 * SCMSGS
			 * @explanation
			 * This is a notification from the handler that it is
			 * trying to switch over the MDS for the specified file
			 * system.
			 * @user_action
			 * This is an informational message; no user action
			 * is needed.
			 */
			syslog(LOG_NOTICE, "Switch over the MDS for '%s' file "
			    "system from '%s' node to node '%s'.",
			    qfs_name, hostname, myhostname);

			(void) sprintf(cmd, "%s -R -s %s %s",
			    SAMSHAREFS_CMD, myhostname, qfs_name);
			(void) execute_cmd(cmd, NULL, 0, NULL, 0);
		}

		/*
		 * Check for the status of MDS server again after delay.
		 */
		(void) sleep(MDS_CHECK_RETRY_SLEEP);
		retries++;
		if (retries > (zc_mds_check_timeout/MDS_CHECK_RETRY_SLEEP)) {
			/*
			 * SCMSGS
			 * @explanation
			 * The zone boot handler found that the verification
			 * for the MDS of the specified file system has
			 * exceeded timeout.
			 * @user_action
			 * Check for the system logs and zone boot logs to
			 * investigate on possible problem. After rectifying the
			 * problem reboot the zone cluster node. If the problem
			 * still persists contact your authorized Sun service
			 * provider with copy of /var/adm/messages files on
			 * all nodes.
			 */
			syslog(LOG_WARNING, "Verification for '%s' file system "
			    "MDS node status exceeded timeout '%d' seconds.",
			    qfs_name, zc_mds_check_timeout);
			return (NULL);
		}
		goto restart;
	}
}

/*
 * Returns the node name where the MDS is running for a given file system.
 * It is caller responsibility fo free the buffer containing the nodename.
 */
static char *
get_mds_server_nodename(const char *qfs_name)
{
	char	cmd[CMDSIZE];
	char	out_buf[BUFSIZE];
	char	err_buf[BUFSIZE];
	int	rc;
	char	*tfile;
	char	*mds_nodename;

	/*
	 * Get temporary file to store command output.
	 */
	if ((tfile = tmpnam(NULL)) == NULL) {
		/*
		 * SCMSGS
		 * @explanation
		 * The zone boot handler failed to create a temporary file.
		 * @user_action
		 * Check the log messages to rectify the problem.
		 * If the problem persists, contact your authorized
		 * Sun service provider.
		 */
		int ret = errno;	/*lint !e746 */
		syslog(LOG_WARNING, "Failed to create a temporary file "
		    "during determining MDS node of '%s' file system : %s.",
		    qfs_name, strerror(ret));
		return (NULL);
	}
	/*
	 * Form the command to get the MDS server status.
	 * samsharefs -R <fs_name> > <file> 2>&1
	 */
	(void) snprintf(cmd, CMDSIZE, "%s -R %s > %s 2>&1",
	    SAMSHAREFS_CMD, qfs_name, tfile);

	rc = execute_cmd(cmd, out_buf, BUFSIZE, err_buf, BUFSIZE);
	if (rc != 0) {
		/*
		 * Execution didn't completed successfully.
		 * Log message by retrieving the error output which is stored
		 * in temporary file.
		 */
		FILE    *fp;
		uint_t	len = 0;
		if ((fp = fopen(tfile, "r")) == NULL) {
			/*
			 * SCMSGS
			 * @explanation
			 * The zone boot handler failed to open specific
			 * temporary file.
			 * @user_action
			 * Check the log messages to rectify the problem.
			 * If the problem persists, contact your authorized
			 * Sun service provider.
			 */
			int ret = errno;
			syslog(LOG_WARNING, "Failed to open the temporary file "
			    "'%s' while reporting the error status of failure "
			    "to find MDS node of '%s' file system : %s.",
			    tfile, qfs_name, strerror(ret));
			return (NULL);
		}
		while (fgets(out_buf, BUFSIZE, fp)) {
			(void) snprintf(err_buf+len, BUFSIZE-len,
			    "%s", out_buf);
			len += strlen(out_buf);
		}
		/*
		 * SCMSGS
		 * @explanation
		 * The zone boot handler failed to determine the MDS node of a
		 * file system.
		 * @user_action
		 * Try running the command manually and see the execution
		 * status. If the command execution succeeds contact your Sun
		 * service provider to determine whether a workaround or patch
		 * is available.
		 * In case the execution fails investigate the failure and
		 * reboot the zone after rectifying the problem.
		 */
		syslog(LOG_WARNING, "The command '%s' to find MDS of '%s' "
		    "file system failed : %s.",
		    cmd, qfs_name, err_buf);
		(void) remove(tfile);
		return (NULL);
	}

	/*
	 * Find the MDS nodename which is stored in temporary file.
	 * awk '$5~/server/ { printf $1 }' <file>
	 */
	(void) snprintf(cmd, CMDSIZE,
	    "awk '$5~/server/ { printf $1 }' %s",
	    tfile);

	rc = execute_cmd(cmd, out_buf, BUFSIZE, err_buf, BUFSIZE);
	if (rc != 0) {
		/*
		 * Execution didn't completed successfully.
		 */
		syslog(LOG_WARNING, "The command '%s' to find MDS of '%s' "
		    "file system failed : %s.",
		    cmd, qfs_name, err_buf);
		(void) remove(tfile);
		return (NULL);
	}
	(void) remove(tfile);

	/*
	 * The command ouput conatins the server nodename.
	 */
	mds_nodename = strdup(out_buf);
	if (mds_nodename == NULL) {
		syslog(LOG_WARNING, "Out of memory.");
		return (NULL);
	}
	return (mds_nodename);
}

/*
 * Determines whether given node is first node which is alive from the
 * given resource group node list.
 */
static boolean_t
is_rg_leader(const char *hostname, const char *zone_name, const char *rg_name,
    const scha_str_array_t *rg_nodelist)
{
	uint_t	i;

	for (i = 0; i < rg_nodelist->array_cnt; i++) {
		char *rg_node = rg_nodelist->str_array[i];
		if (get_zc_node_status(rg_node, zone_name) == ZC_NODE_UP) {
			if (strcmp(hostname, rg_node) == 0) {
				return (B_TRUE);
			} else {
				return (B_FALSE);
			}
		}
	}
	/*
	 * This code shouldn't hit as at least the current node is up and will
	 * be leader.
	 */
	assert(0);
	/*
	 * SCMSGS
	 * @explanation
	 * The zone boot handler failed to determine a leader for the specified
	 * resource group. This should not happen as the booting node at least
	 * will be leader.
	 * @user_action
	 * Contact your Sun service provider to determine whether a workaround
	 * or patch is available.
	 */
	syslog(LOG_WARNING,
	    "Failed to determine the resource group '%s' leader.", rg_name);
	return (B_TRUE);
}

/*
 * Returns the zone cluster node status on a given physical node.
 * NOTE: We can't use the libscha API to determine the status as the context
 * is in the middle of a zone cluster node boot and status of any node which
 * is booting will be returned as DOWN.
 * This status will create trouble when all the zone cluster nodes are booting
 * and every node will try to switch over the MDS server assuming others nodes
 * are DOWN.
 * So we use solaris zone status to determine zone cluster node status.
 */
static zc_node_status_t
get_zc_node_status(char *nodename, const char *zcname)
{
	zone_cluster_state_t	state;
	uint_t			nid;
	nid = clconf_cluster_get_nodeid_by_nodename(nodename);
	(void) get_zone_state_on_node(zcname, nid, &state);

	if (state < ZC_STATE_DOWN) {
		return (ZC_NODE_DOWN);
	} else {
		return (ZC_NODE_UP);
	}
}

/*
 * Fills the given MDS resource information.
 */
static scha_err_t
fill_rs_info(const char *rs_name, const char *zone_name, rs_info_t *rs_info)
{
	scha_extprop_value_t	*qfs_fs_val = NULL;
	scha_err_t		err = SCHA_ERR_NOERR;
	char			*qfs_name = NULL;
	uint_t			i;
	scha_resource_t		rs_handle;
	scha_str_array_t	*res_dep_list = NULL;
	scha_extprop_value_t	*zc_name_prop = NULL;
	scha_rsstate_t		rs_state;
	char			*rs_type;
	uint_t			retries = 0;

retry_rs:
	if ((err = scha_resource_open(rs_name, NULL,
	    &rs_info->mds_rs_handle)) != SCHA_ERR_NOERR) {
		syslog(LOG_WARNING,
		    "Failed to retrieve the resource handle of %s: %s.",
		    rs_name, scha_strerror(err));
		return (err);
	}

	/* Get the sQFS file systems configured for this resource */
	if ((err = scha_resource_get(rs_info->mds_rs_handle, SCHA_EXTENSION,
	    QFSFILESYSTEM, &qfs_fs_val)) != SCHA_ERR_NOERR) {
		if (err == SCHA_ERR_SEQID && retries < zc_scha_max_retries) {
			log_debug_message("cluster configuration is "
			    "changed, retrying get status for "
			    "resource %s", rs_name);
			(void) scha_resource_close(rs_info->mds_rs_handle);
			retries++;
			(void) sleep(zc_scha_retry_wait);
			goto retry_rs;
		}

		syslog(LOG_WARNING,
		    "Failed to retrieve the resource property %s of %s: %s.",
		    QFSFILESYSTEM, rs_name, scha_strerror(err));
		(void) scha_resource_close(rs_info->mds_rs_handle);
		return (err);
	}

	/*
	 * Get the state of the dependent SUNW.wait_zc_boot resource
	 * which has ZCName with current zone name.
	 */
	if ((err = scha_resource_get(rs_info->mds_rs_handle,
	    SCHA_RESOURCE_DEPENDENCIES, &res_dep_list)) != SCHA_ERR_NOERR) {
		if (err == SCHA_ERR_SEQID && retries < zc_scha_max_retries) {
			log_debug_message("cluster configuration is "
			    "changed, retrying get resource dependencies for "
			    "resource %s", rs_name);
			(void) scha_resource_close(rs_info->mds_rs_handle);
			retries++;
			(void) sleep(zc_scha_retry_wait);
			goto retry_rs;
		}

		syslog(LOG_WARNING,
		    "Failed to retrieve the resource property %s of %s: %s.",
		    SCHA_RESOURCE_DEPENDENCIES, rs_name, scha_strerror(err));
		(void) scha_resource_close(rs_info->mds_rs_handle);
		return (err);
	}

	rs_info->wait_zc_rs_state = SCHA_RSSTATE_ONLINE;
	for (i = 0; i < res_dep_list->array_cnt; i++) {
retry_dep_rs:
		if ((err = scha_resource_open(res_dep_list->str_array[i], NULL,
		    &rs_handle)) != SCHA_ERR_NOERR) {
			syslog(LOG_WARNING,
			    "Failed to retrieve the resource handle of %s: %s.",
			    res_dep_list->str_array[i], scha_strerror(err));
			return (err);
		}
		if ((err = scha_resource_get(rs_handle, SCHA_TYPE, &rs_type))
		    != SCHA_ERR_NOERR) {
			if (err == SCHA_ERR_SEQID &&
			    retries < zc_scha_max_retries) {
				log_debug_message("cluster configuration is "
				    "changed, retrying get resource type for "
				    "resource %s", res_dep_list->str_array[i]);
				(void) scha_resource_close(rs_handle);
				retries++;
				(void) sleep(zc_scha_retry_wait);
				goto retry_dep_rs;
			}
			syslog(LOG_WARNING, "Failed to retrieve the resource "
			    "property %s of %s: %s.",
			    SCHA_TYPE,
			    res_dep_list->str_array[i], scha_strerror(err));
			(void) scha_resource_close(rs_handle);
			return (err);
		}
		if (strncmp(rs_type, SUNW_WAIT_ZC, strlen(SUNW_WAIT_ZC)) != 0) {
			/*
			 * Skip this resource as this is not SUNW.wait_zc_boot
			 * type.
			 */
			(void) scha_resource_close(rs_handle);
			continue;
		}
		if ((err = scha_resource_get(rs_handle, SCHA_EXTENSION,
		    ZCNAME, &zc_name_prop))
		    != SCHA_ERR_NOERR) {
			if (err == SCHA_ERR_SEQID &&
			    retries < zc_scha_max_retries) {
				log_debug_message("cluster configuration is "
				    "changed, retrying to get extension "
				    "property %s for resource %s",
				    res_dep_list->str_array[i]);
				(void) scha_resource_close(rs_handle);
				retries++;
				(void) sleep(zc_scha_retry_wait);
				goto retry_dep_rs;
			}
			syslog(LOG_WARNING, "Failed to retrieve the resource "
			    "property %s of %s: %s.",
			    ZCNAME, res_dep_list->str_array[i],
			    scha_strerror(err));
			(void) scha_resource_close(rs_handle);
			return (err);
		}

		if (strcmp(zc_name_prop->val.val_str, zone_name) != 0) {
			/*
			 * Skip this resource as this resource is not meant
			 * for this zone.
			 * Issue a warning message as the MDS resource
			 * configured for this zone cluster has dependency
			 * on different zone cluster.
			 */
			/*
			 * SCMSGS
			 * @explanation
			 * The zone boot handler detected an invalid
			 * configuration.
			 * @user_action
			 * Correct the configuration by removing the specified
			 * dependency.
			 */
			syslog(LOG_WARNING, "The SUNW.qfs resource '%s' "
			    "that is configured for zone cluster '%s' has a "
			    "dependency on SUNW.wait_zc_boot resource '%s' "
			    "that is configured for zone cluster '%s'.",
			    rs_name, zone_name,
			    res_dep_list->str_array[i],
			    zc_name_prop->val.val_str);
			(void) scha_resource_close(rs_handle);
			continue;
		}
		if ((err = scha_resource_get(rs_handle, SCHA_RESOURCE_STATE,
		    &rs_state))
		    != SCHA_ERR_NOERR) {
			if (err == SCHA_ERR_SEQID &&
			    retries < zc_scha_max_retries) {
				log_debug_message("cluster configuration is "
				    "changed, retrying get resource type for "
				    "resource %s", res_dep_list->str_array[i]);
				(void) scha_resource_close(rs_handle);
				retries++;
				(void) sleep(zc_scha_retry_wait);
				goto retry_dep_rs;
			}
			syslog(LOG_WARNING, "Failed to retrieve the resource "
			    "property %s of %s: %s.",
			    SCHA_RESOURCE_STATE,
			    res_dep_list->str_array[i], scha_strerror(err));
			(void) scha_resource_close(rs_handle);
			return (err);
		}

		rs_info->wait_zc_rs_state = rs_state;
		rs_info->wait_zc_rs_name = res_dep_list->str_array[i];
		(void) scha_resource_close(rs_handle);

		/*
		 * Skip checking for the remaining dependencies as we got
		 * the result of SUNW.wait_zc_boot resource we are looking for.
		 */
		break;
	}

	/* Get the resource group name of the metadata server agent */
	if ((err = scha_resource_get(rs_info->mds_rs_handle, SCHA_GROUP,
	    &rs_info->mds_rg_name)) != SCHA_ERR_NOERR) {
		if (err == SCHA_ERR_SEQID && retries < zc_scha_max_retries) {
			log_debug_message("cluster configuration is "
			    "changed, retrying get status for "
			    "resource %s", rs_name);
			(void) scha_resource_close(rs_info->mds_rs_handle);
			retries++;
			(void) sleep(zc_scha_retry_wait);
			goto retry_rs;
		}
		syslog(LOG_WARNING,
		    "Failed to retrieve the resource property %s of %s: %s.",
		    SCHA_GROUP, rs_name, scha_strerror(err));
		(void) scha_resource_close(rs_info->mds_rs_handle);
		return (err);
	}

	/* Get the resource group status. */
	if ((err = scha_resourcegroup_open(rs_info->mds_rg_name,
	    &rs_info->mds_rg_handle)) != SCHA_ERR_NOERR) {
		/*
		 * SCMSGS
		 * @explanation
		 * An error occurred while the handle for the resource group
		 * was being retrieved.
		 * @user_action
		 * Investigate possible RGM errors.
		 * Contact your authorized Sun service provider for
		 * assistance in diagnosing the problem.
		 */
		syslog(LOG_WARNING,
		    "Failed to retrieve the resource group handle of %s: %s.",
		    rs_info->mds_rg_name, scha_strerror(err));
		(void) scha_resource_close(rs_info->mds_rs_handle);
		return (err);
	}
	if ((err = scha_resourcegroup_get(rs_info->mds_rg_handle, SCHA_RG_STATE,
	    &rs_info->mds_rg_state)) != SCHA_ERR_NOERR) {
		if (err == SCHA_ERR_SEQID && retries < zc_scha_max_retries) {
			log_debug_message("cluster configuration is "
			    "changed, retrying get status for "
			    "resource group %s", rs_info->mds_rg_name);
			(void) scha_resource_close(rs_info->mds_rs_handle);
			(void) scha_resourcegroup_close(rs_info->mds_rg_handle);
			retries++;
			(void) sleep(zc_scha_retry_wait);
			goto retry_rs;
		}
		/*
		 * SCMSGS
		 * @explanation
		 * An error occurred while retrieving the specified property
		 * of the resource group.
		 * @user_action
		 * Investigate possible RGM errors.
		 * Contact your authorized Sun service provider for
		 * assistance in diagnosing the problem with copy of
		 * the /var/adm/messages files on all nodes.
		 */
		syslog(LOG_WARNING,
		    "Failed to retrieve the resource group property %s "
		    "of %s: %s.",
		    SCHA_RG_STATE, rs_info->mds_rg_name, scha_strerror(err));
		(void) scha_resource_close(rs_info->mds_rs_handle);
		(void) scha_resourcegroup_close(rs_info->mds_rg_handle);
		return (err);
	}

	/* Get the node list for the resource group */
	if ((err = scha_resourcegroup_get(rs_info->mds_rg_handle, SCHA_NODELIST,
	    &rs_info->mds_rg_nodelist)) != SCHA_ERR_NOERR) {
		if (err == SCHA_ERR_SEQID && retries < zc_scha_max_retries) {
			log_debug_message("cluster configuration is "
			    "changed, retrying get status for "
			    "resource %s",
			    rs_info->mds_rg_name);
			(void) scha_resource_close(rs_info->mds_rs_handle);
			(void) scha_resourcegroup_close(rs_info->mds_rg_handle);
			retries++;
			(void) sleep(zc_scha_retry_wait);
			goto retry_rs;
		}
		syslog(LOG_WARNING,
		    "Failed to retrieve the resource group property %s "
		    "of %s: %s.",
		    SCHA_NODELIST, rs_info->mds_rg_name, scha_strerror(err));
		(void) scha_resourcegroup_close(rs_info->mds_rg_handle);
		return (err);
	}

	/*
	 * Initialize the sqfs name list in the resource structure.
	 */
	rs_info->mds_rs_qfs_list.array_cnt = 0;
	rs_info->mds_rs_qfs_list.str_array = NULL;

	/*
	 * Obtain the sQFS name for each mount point.
	 */
	for (i = 0; i < qfs_fs_val->val.val_strarray->array_cnt; i++) {

		if ((err = get_fsname_for_mntpt(
		    qfs_fs_val->val.val_strarray->str_array[i], &qfs_name))
		    != SCHA_ERR_NOERR) {
			/* message is already logged */
			return (err);
		}

		if ((err = add_to_strarray(&rs_info->mds_rs_qfs_list,
		    qfs_name)) != SCHA_ERR_NOERR) {
			/*
			 * SCMSGS
			 * @explanation
			 * The zone boot handler failed to generate the
			 * list of shared qfs file systems for a resource.
			 * @user_action
			 * Investigate using zone boot log messages and
			 * try to rectify the problem. If problem
			 * persists after rebooting the zone,
			 * contact your authorized Sun service
			 * provider with copy of /var/adm/messages files
			 * on all nodes.
			 */
			syslog(LOG_WARNING, "Failed to add qfs name '%s' to "
			    "qfs list for resource '%s'.",
			    qfs_name, rs_name);
			return (err);
		}
		free(qfs_name);
	}
	return (SCHA_ERR_NOERR);
}

static scha_err_t
get_fsname_for_mntpt(char *qfs_mntpt, char **qfs_fs)
{
	struct vfstab	vp, vref;
	FILE		*fp;

	(void) memset(&vref, 0, sizeof (struct vfstab));
	vref.vfs_mountp = qfs_mntpt;
	vref.vfs_fstype = SAMFS_TYPE;

	fp = fopen(VFSTAB_FILE, "r");
	if (fp == NULL) {
		/*
		 * SCMSGS
		 * @explanation
		 * The zone boot handler failed to open the specified file.
		 * @user_action
		 * Check for the possible reasons for open failure and reboot
		 * the node after rectify the problem.
		 */
		int ret = errno;	/*lint !e746 */
		syslog(LOG_WARNING, "Failed to open the file %s : %s",
		    VFSTAB, strerror(ret));
		return (SCHA_ERR_INTERNAL);
	}

	if (getvfsany(fp, &vp, &vref) == 0) {
		/*
		 * Found entry in the VFSTAB.
		 */
		*qfs_fs = strdup(vp.vfs_special);
		if (*qfs_fs == NULL) {
			syslog(LOG_WARNING, "Out of memory.");
			return (SCHA_ERR_NOMEM);
		}
		return (SCHA_ERR_NOERR);
	}
	(void) fclose(fp);
	/*
	 * SCMSGS
	 * @explanation
	 * The zone boot handler failed to find the mount point for the
	 * specified file system.
	 * @user_action
	 * Check the /etc/vfstab file for the entry of the specified file
	 * system.
	 * If the entry does not exists add the file system entry and reboot
	 * the zone cluster node. Contact your Sun service provider if the
	 * problem persists after you add the file system entry in /etc/vfstab.
	 */
	syslog(LOG_WARNING, "Entry for the mount point %s in not found in %s.",
	    qfs_mntpt, VFSTAB_FILE);
	return (SCHA_ERR_INTERNAL);
}
