/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_ZC_SQFS_H_
#define	_ZC_SQFS_H_

#pragma ident	"@(#)zc_sqfs.h	1.1	08/09/25 SMI"

/*lint -e801 */

#include <stdio.h>
#include <stdarg.h>
#include <unistd.h>
#include <scha.h>
#include <stdlib.h>
#include <strings.h>
#include <syslog.h>
#include <errno.h>
#include <assert.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/vfstab.h>
#include <sys/zone.h>
#include <sys/clconf_int.h>
#include <rgm/scha_priv.h>
#include <rgm/rgm_scswitch.h>

extern	int debug_mode;

#define	SUNW_QFS	"SUNW.qfs"
#define	QFSFILESYSTEM	"QFSFileSystem"

#define	SUNW_WAIT_ZC	"SUNW.wait_zc_boot"
#define	ZCNAME		"ZCName"

#define	SAMSHAREFS_CMD	"/opt/SUNWsamfs/sbin/samsharefs"

#define	RM_CMD		"/usr/bin/rm -f"

#define	GZ_PINPONG_DIR	"/var/cluster/rgm/global/pingpong_log"

#define	VFSTAB_FILE	"/etc/vfstab"
#define	SAMFS_TYPE	"samfs"

#define	CMDSIZE	1024
#define	BUFSIZE	256

#define	MSG_TAG_LEN	256

/*
 * various requests of the cluster can fail due to reconfiguraions
 * being in progress. We wait and retry if we get this kind of error.
 * This is the delay in seconds before retrying the operaion.
 */
#define	MIN_SCHA_RETRY_WAIT	1
#define	SCHA_RETRY_WAIT		10
#define	MIN_SCHA_MAX_RETRIES	30
#define	SCHA_MAX_RETRIES	30

/*
 * The timeout to ensure that MDS is available on a node.
 */
#define	MIN_MDS_CHECK_TIMEOUT	60
#define	MDS_CHECK_TIMEOUT	600
#define	MDS_CHECK_RETRY_SLEEP	5

/*
 * The following are tunables that will be read from configuration file(if
 * exists).
 * The default values of these can be modified by
 * -Having a specific zc_halt_boot configuartion file for a zone cluster.
 *  /etc/cluster/zc//<zc_name>/zc_halt_boot.conf
 * -Having a global zc_halt_boot configuration file.
 *  /etc/cluster/zc/zc_halt_boot.conf
 * When a specific zc_halt_boot configuartion file exists the values in that
 * file will be in effective.
 */
#define	ZC_SCHA_RETRY_WAIT	"zc_scha_retry_wait"
#define	ZC_SCHA_MAX_RETRIES	"zc_scha_max_retries"
#define	ZC_MDS_CHECK_TIMEOUT	"zc_mds_check_timeout"

extern	uint_t	zc_scha_retry_wait;
extern	uint_t	zc_scha_max_retries;
extern	uint_t	zc_mds_check_timeout;

#define	ZC_HALT_BOOT_CONF_DIR	"/etc/cluster/zc"
#define	ZC_HALT_BOOT_CONF_FILE	"zc_halt_boot.conf"

/*
 * structure to store the MDS resource information.
 */

typedef struct rs_info {
	char			*mds_rg_name;
	scha_rgstate_t		mds_rg_state;
	scha_str_array_t	*mds_rg_nodelist;
	scha_str_array_t	mds_rs_qfs_list;
	scha_resource_t		mds_rs_handle;
	scha_resourcegroup_t	mds_rg_handle;
	char			*wait_zc_rs_name;
	scha_rsstate_t		wait_zc_rs_state;
} rs_info_t;

/*
 * zone cluster node status.
 */
typedef enum {
	ZC_NODE_DOWN = 0,
	ZC_NODE_UP
} zc_node_status_t;

extern void log_debug_message(const char *, ...);
extern scha_err_t find_zone_mds_list(scha_str_array_t*, const char *);
extern scha_err_t add_to_strarray(scha_str_array_t *, const char *);
extern void free_strarry(scha_str_array_t *);
extern boolean_t is_str_in_strarray(const scha_str_array_t *, const char *);
extern int execute_cmd(const char *, char *, uint_t, char *, uint_t);
extern void read_zc_halt_boot_config(const char *);

#endif	/* _ZC_SQFS_H_ */
