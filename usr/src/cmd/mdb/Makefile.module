#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#ident	"@(#)Makefile.module	1.14	08/05/20 SMI"
#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# cmd/mdb/Makefile.module
#

.KEEP_STATE:
.SUFFIXES:

kvm_TGTFLAGS = -D_KERNEL
proc_TGTFLAGS = -D_USER

C_PICFLAGS = -K pic
CFLAGS += -v $(C_PICFLAGS) -G $(XREGSFLAG)
CFLAGS64 += -v $(C_PICFLAGS) -G $(XREGSFLAG64)
CPPFLAGS += $($(MDBTGT)_TGTFLAGS) -I../../../common
LDFLAGS += -z text -z combreloc
LDFLAGS64 += -z text -z combreloc
CLOBBERFILES += $(MDBLIB)

#
# Reset STRIPFLAG to the empty string.  MDB modules are intentionally installed
# with symbol tables in order to help module developers.
#
STRIPFLAG =

MODOBJS += $(MODSRCS:%.c=%.o)
MODNAME = $(MDBLIB:%.so=%)

INS.libmdblink=    -$(RM) $@; $(SYMLINK) $(MDBLIB) $@

# Use MODSRCS, not MODOBJS, as the basis for the lint files because
# the rgm module overrides MODOBJS
LINTFILES = $(MODSRCS:%.c=%.ln)
LINTS_DIR = .

all: $(MDBLIB)

lint: $(LINTFILES)

install: all $(ROOTMDBLIB) $(ROOTMDBLIBLINK)

dmods: install

.NO_PARALLEL:
.PARALLEL: $(MODOBJS) $(LINTFILES)

$(MDBLIB): $(MODOBJS)
	$(LINK.c) $(MODOBJS) -o $@ $(LDLIBS)
	$(POST_PROCESS_SO)

%.o: %.c
	$(COMPILE.c) $<

%.o: ../%.c
	$(COMPILE.c) $<

%.o: ../../../common/modules/$(MODNAME)/%.c
	$(COMPILE.c) $<

clean.lint:
	$(RM) $(LINTFILES)

clean:
	$(RM) $(MODOBJS)

%.ln: %.c
	$(LINT.c) -c $<

%.ln: ../%.c
	$(LINT.c) -c $<

%.ln: ../../../common/modules/$(MODNAME)/%.c
	$(LINT.c) -c $<

$(ROOTMDBLIBDIR)/%: % $(ROOTMDBLIBDIR)
	$(INS.file)

$(ROOTMDBLIBDIR)/$(MDBLIBLINK): $(ROOTMDBLIBDIR)/$(MDBLIB)
	$(INS.libmdblink)

$(ROOTMDBLIBDIR):
	$(INS.dir)
