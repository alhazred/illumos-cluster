#!/bin/sh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#pragma ident	"@(#)frgmd_debug.sh	1.3	08/05/20 SMI"
#
# This script is an undocumented utility which facilitates
# debugging of the (f)rgmd at customer sites. This script permits
# the verbose debugging mode of the (f)rgmd to be turned on or off
# on the local node.  To enable or disable verbose debugging mode
# on multiple nodes, the script has to be invoked on each node.
#
# It also allows the trace buffer in the (f)rgmd to be dumped out.
# The trace buffer is a ring buffer that has essentially the same
# information as the verbose debugging messages generated when
# debugging mode is turned on. When the trace buffer gets full,
# the oldest messages get dropped.
#
# Each trace buffer entry has the thread ID of the (f)rgmd thread that
# generated the message and the last 8 digits of a microsecond
# timestamp.
#
# It also provides functionality for printing the node number of the current
# president node.
#
# No references should be made in gate code to this script.
#
# Usage:  $0 { on | off | printbuf | pres | sysdump_on | sysdump_off | methdump_on | methdump_off }
#
# When verbose debugging mode is turned on, the (f)rgmd will syslog a
# large number of messages which will appear on the console and
# in /var/adm/messages.  These messages are intended for the use
# of product sustaining engineers within Sun; they are not intended
# to be used by customers.
#

#/usr/cluster/lib/sc/sc_zonescheck
#if [ $? -ne 0 ]; then
#	exit 1
#fi


PGREP=/usr/bin/pgrep
ID=/usr/bin/id
TEMPFILE=/tmp/`basename $0`.$$
rm -f $TEMPFILE

OS=`/bin/uname`


PROGRAM=frgmd
PROGRAM_FILE=/usr/cluster/lib/sc/${PROGRAM}


# codes for different functions. Only one can be invoked
DebugFlag_code=1
SysDumpFlag_code=2
MethDumpFlag_code=3
Print_buf_code=4
Pres_code=5

case "$1" in
on|ON|On|1)
	flag=1
	newstate=ON
	code=$DebugFlag_code
	;;

off|OFF|Off|0)
	flag=0
	newstate=OFF
	code=$DebugFlag_code
	;;

printbuf|PRINTBUF|Printbuf)
	code=$Print_buf_code
	;;

pres|PRES|Pres)
	code=$Pres_code
	;;

sysdump_on)
	flag=1
	newstate=ON
	code=$SysDumpFlag_code
	;;

sysdump_off)
	flag=0
	newstate=OFF
	code=$SysDumpFlag_code
	;;

methdump_on)
	flag=1
	newstate=ON
	code=$MethDumpFlag_code
	;;

methdump_off)
	flag=0
	newstate=OFF
	code=$MethDumpFlag_code
	;;
*)

	echo "Usage:	$0 { on | off | printbuf | pres | sysdump_on | sysdump_off | methdump_on | methdump_off }"
	exit 1
	;;
esac

res=`$ID`
if [ $? -ne 0 ]; then
	echo "Failed: cannot execute $ID command."
	exit 1
fi
set -- $res
if [ "$1" != "uid=0(root)" ]; then
	echo "Failed this program must be executed as root."
	exit 1
fi



procid=`$PGREP -x $PROGRAM`

numprocs=`echo $procid|wc -w`
if [ $numprocs -eq 0 ]; then
	echo "Failed: cannot obtain process id of ${PROGRAM}."
	exit 1
fi
if [ $numprocs -gt 1 ]; then
	echo "Failed: more than one ${PROGRAM} process is running."
	exit 1
fi

# Now start action depending on the option passed as argument

case $code in

$Pres_code)
# if we're supposed to print the president, do it first and then exit so
# we don't execute any of the other functionality

	case $OS in
	SunOS)
	    rgm_state_adr=`echo 0t${procid}':A\nRgm_state/K\n:R\n$q' | \
		adb ${PROGRAM_FILE} - | \
		awk -F':' '/Rgm_state:[a-z0-9]/ {print $2}'`

	    if [ $? -ne 0 ]; then
		echo "Command failed."
		exit 1
	    fi

	    #
	    # Rgm_president is the first field of Rgm_state
	    # (no offset).
	    #
	    node=`echo 0t${procid}':A\n'${rgm_state_adr}'/D\n:R\n$q' | \
		adb ${PROGRAM_FILE} - | \
		nawk -F: '/^[0-9a-fx]+:[1-9	 ]+/ {print $2}' | nawk \
		'{gsub(/	/,"")}{print}'`

	    if [ $? -ne 0 ]; then
		echo "Command failed."
		exit 1
	    fi
	    ;;

	Linux)
	    #
	    # The Rgm_president field is the first field
	    # of Rgm_state structure, so offset is 0x0.
	    #
	    cmdfile=/tmp/`basename $0`-gdbcmd-$$
	    echo 'printf "PRESIDENT:%d\n", *(int*)((char *)Rgm_state+0x0)' > ${cmdfile}
	    echo "quit" >> ${cmdfile}

	    node=`/usr/bin/gdb -q -p ${procid} ${PROGRAM_FILE} -x $cmdfile  > $TEMPFILE
	    cat $TEMPFILE |  /bin/awk  -F':'  ' /^PRESIDENT:/ {print $2}'`
	    
	    /bin/rm -f ${cmdfile}
	    /bin/rm -f ${TEMPFILE}
	    ;;
	esac
	    
	echo $node
	exit 0
	;;

#
# Enable/disable dump; enable/disable debugging; or printbuf
#

$DebugFlag_code)
	case $OS in
	SunOS)
	    echo 0t${procid}':A\nDebugflag/W '${flag}'\n:R\n$q' | \
		adb ${PROGRAM_FILE} - >$TEMPFILE 2>&1
	;;
	Linux)
	    cmdfile=/tmp/`basename $0`-gdbcmd-$$
	    echo "set write on" >  ${cmdfile}
	    echo "print Debugflag=${flag}" >> ${cmdfile}
	    echo "quit" >> ${cmdfile}

	    /usr/bin/gdb -q -p ${procid} ${PROGRAM_FILE} -x $cmdfile  > $TEMPFILE
	    /bin/rm -f ${cmdfile}
	;;
	esac

	res=$?
	printTEMPFILE=TRUE
	;;

#
# Print buf 
#
$Print_buf_code)
	case $OS in 
	SunOS)
	    echo 0t${procid}':A\n**rgm_dbg_buf/s\n:R\n$q' | \
		adb ${PROGRAM_FILE} -
	    ;;

	Linux)
	    cmdfile=/tmp/`basename $0`-gdbcmd-$$
	    echo 'printf "%s",rgm_dbg_buf->buf'  > ${cmdfile}
	    echo "quit" >> ${cmdfile}
	    /usr/bin/gdb -q -p ${procid} ${PROGRAM_FILE} -x $cmdfile  |  grep "^th "
	    /bin/rm -f ${cmdfile}
	    ;;

	esac

	res=$?
	printTEMPFILE=FALSE
	;;


$SysDumpFlag_code)
	case $OS in
	SunOS)
	    echo 0t${procid}':A\nSystem_dumpflag/W '${flag}'\n:R\n$q' | \
		adb ${PROGRAM_FILE} - >$TEMPFILE 2>&1
	;;

	Linux)
	    echo "Linux set Sysdump facility not implemented"
	;;

	esac

	res=$?
	printTEMPFILE=TRUE	esac
	;;

$MethDumpFlag_code)
	case $OS in
	SunOS)
	    echo 0t${procid}':A\nMethod_dumpflag/W '${flag}'\n:R\n$q' | \
		adb ${PROGRAM_FILE} - >$TEMPFILE 2>&1
	    ;;

	Linux)
	    echo "Linux set Methdump facility not implemented"
	    ;;
	esac

	res=$?
	printTEMPFILE=TRUE
	;;
esac

# print errors if any.
if [ $res -ne 0 ]; then
	echo "adb command execution failed."
	if [ $printTEMPFILE -eq TRUE ]; then
		echo "Output was:"
		cat $TEMPFILE 1>&2
	fi
	rm -f $TEMPFILE
	exit $res
fi

rm -f $TEMPFILE


# Now print relevant state information if change requested and successful.

case $code in

$DebugFlag_code)

	echo "${PROGRAM} verbose debugging is $newstate"
	;;

$Print_buf_code)
# Nothing to be done
	;;

$SysDumpFlag_code)
	echo "System dump facility is $newstate"
	;;

$MethDumpFlag_code)
	echo "Method dump facility is $newstate"
	;;

$Pres_code)
	echo "Internal error"
	exit -1
	;;
*)
	echo "Internal error"
	exit -1
	;;
esac

exit $res
