#!/usr/bin/sh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#pragma ident	"@(#)pmfd_debug.sh	1.6	08/05/20 SMI"
#
# This script is an undocumented utility which facilitates
# debugging of the pmfd at customer sites.  This script permits
# the verbose debugging mode of the pmfd to be turned on or off
# on the local node.  To enable or disable verbose debugging mode
# on multiple nodes, the script has to be invoked on each node.
#
# It also allows the trace buffer in the pmfd to be dumped out.
# The trace buffer is a ring buffer that has essentially the same
# information as the verbose debugging messages generated when
# debugging mode is turned on. When the trace buffer gets full,
# the oldest messages get dropped.
#
# Each trace buffer entry has the thread ID of the pmfd thread that
# generated the message.
#
# No references should be made in gate code to this script.
#
# Usage:	$0 { on | off | printbuf }
#
# When verbose debugging mode is turned on, the pmfd will syslog a
# large number of messages which will appear in /var/adm/messages.  These
# messages are intended for the use of product sustaining engineers within
# Sun; they are not intended to be used by customers.
#
PGREP=/usr/bin/pgrep
ID=/usr/bin/id
TEMPFILE=/tmp/`basename $0`.$$
rm -f $TEMPFILE

case "$1" in
on|ON|On|1)
	flag=1
	newstate=ON
	change_state=1
	;;

off|OFF|Off|0)
	flag=0
	newstate=OFF
	change_state=1
	;;

printbuf|PRINTBUF|Printbuf)
	change_state=0
	;;

*)
	echo "Usage:	$0 { on | off | printbuf }"
	exit 1
	;;
esac

res=`$ID`
if [ $? -ne 0 ]; then
	echo "Failed: cannot execute $ID command."
	exit 1
fi
set -- $res
if [ "$1" != "uid=0(root)" ]; then
	echo "Failed: this program must be executed as root."
	exit 1
fi

# following code assumes sol version is of the form 5.*,eg, 5.9, 5.10 and so on.
# we remove occurences of "5." for numeric comparison of versions
# so 5.9 -> 9; 5.10 -> 10; and so on.
VERSION=`uname -r | sed 's/.\.//g'`
if [ $VERSION -gt 9 ]; then
zname=`zonename`
procid=`$PGREP -x -z $zname rpc.pmfd`
else
procid=`$PGREP -x rpc.pmfd`
fi

numprocs=`echo $procid|wc -w`
if [ $numprocs -eq 0 ]; then
	echo 'Failed: cannot obtain process id of pmfd.'
	exit 1
fi
if [ $numprocs -gt 1 ]; then
	echo 'Failed: more than one pmfd process is running.'
	exit 1
fi

if [ $change_state -eq 1 ]; then
        echo "debug/W ${flag}" | mdb -w -p ${procid} >$TEMPFILE 2>&1
else
        echo '*pmf_dbg_buf/s' | mdb -p ${procid}
fi

res=$?
if [ $res -ne 0 -a $change_state -eq 1 ]; then
	echo "mdb command execution failed, output was:"
	cat $TEMPFILE 1>&2
fi

rm -f $TEMPFILE
if [ $res -eq 0 -a $change_state -eq 1 ]; then
	echo "rpc.pmfd verbose debugging is $newstate"
fi
exit $res
