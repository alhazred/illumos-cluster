#!/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# ident	"@(#)ipv6_check_ccr.sh	1.5	08/05/20 SMI"
#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# Determine if IPv6 resource groups exist on this cluster by looking into
# the ccr files of all RGs.
#
# Returns 0 if no such RGs exist; non-0 otherwise.
#
# Used by pre-backout script during patch removal to decide if the removal
# can proceed.

PATH=/usr/bin:/usr/sbin

/usr/cluster/lib/sc/sc_zonescheck
if [ $? -ne 0 ]; then
	exit 1
fi

# Check if an alternate root has been provided as an argument.
# If so, use the paths relative to the alternate root, else
# use the paths as they are.
if [[ -n $1 ]]; then
	CCRDIR=$1/etc/cluster/ccr
	SCINSTALL=$1/usr/cluster/bin/scinstall
	export TEXTDOMAINDIR=$1/usr/cluster/lib/locale
else
	CCRDIR=/etc/cluster/ccr
	SCINSTALL=/usr/cluster/bin/scinstall
	export TEXTDOMAINDIR=/usr/cluster/lib/locale
fi

BAD_PORT_LIST="udp6|tcp6"

# I18N
export TEXTDOMAIN=SUNW_SC_CMD
export LC_COLLATE=C


version=`$SCINSTALL -p | cut -f1 -d'_'`
if [[ "$version" != 3.1 && "$version" != 3.1u1 && "$version" != 3.1u2 ]]; then
	#
	# We must be trying to downgrade to a 3.1u3 or later
	# version. Since IPv6 support started in 3.1u3, no more checking
	# is required. Note that patch install does not update the version.
	#
	exit 0
fi

#
# Look through each resource group ccr file
#
for rg_file in `ls $CCRDIR | grep "^rgm_rg_"`; do

	# Skip rg that are not actually in use
	grep "^${rg_file}$" $CCRDIR/directory >/dev/null 2>&1
	if [[ "$?" != 0 ]]; then
		continue
	fi

	#
	# Each resouce has its own line starting with "RS_".
	# Go through all resouces in this group one by one.
	#
	egrep -e "^RS_.*Port_list=" $CCRDIR/$rg_file | while read line
	do
		# Extract resource name
		rs_name=`echo "$line" | cut -f1 -d'	' | cut -f2 -d'_'`

		# Truncate line down to just the Port_list value
		line=`echo "$line" | sed 's/^.*Port_list=//' | cut -f1 -d';'`

		# Skip if the value does not contain "udp6" or "tcp6"
		echo "$line" | egrep -e "${BAD_PORT_LIST}" >/dev/null
		if [[ "$?" != 0 ]]; then
			continue
		fi

		# We have a "bad" resource. Add it to the list
		if [[ "$bad_rs_list" == "" ]]; then
			bad_rs_list="$rs_name"
		else
			bad_rs_list="${rs_name}, ${bad_rs_list}"
		fi
	done
done

if [[ "$bad_rs_list" == "" ]]; then
	exit 0
else
	printf "$(gettext '\
The following resource(s) are configured to work with IPv6\
and must be removed before proceeding:') \c"
	echo "$bad_rs_list"
	echo
	exit 1
fi
