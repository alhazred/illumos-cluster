#!/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# ident	"@(#)tunhb_check_ccr.sh	1.10	08/07/17 SMI"
#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#


# Check if an alternate root has been provided as an argument.
# If so, use the paths relative to the alternate root, else
# use the paths as they are.
if [[ -n $1 ]]; then
	CCR=$1/etc/cluster/ccr/global/infrastructure
	CLPL_DIR=$1/etc/cluster/clpl
	SCINSTALL=$1/usr/cluster/bin/scinstall
	export TEXTDOMAINDIR=$1/usr/cluster/lib/locale
else
	CCR=/etc/cluster/ccr/global/infrastructure
	CLPL_DIR=/etc/cluster/clpl
	SCINSTALL=/usr/cluster/bin/scinstall
	export TEXTDOMAINDIR=/usr/cluster/lib/locale
fi

/usr/cluster/lib/sc/sc_zonescheck
if [ $? -ne 0 ]; then
	exit 1
fi

# Utilities to use
GREP=/usr/bin/grep
WC=/usr/bin/wc
NAWK=/usr/bin/nawk

# I18N
export TEXTDOMAIN=SUNW_SC_CMD
export LC_COLLATE=C

# Check Sun Cluster version. The following checks are not needed for
# Sun Cluster version > 3.1u2.

# scinstall -p returns the output in the form [1-9]\.[0-9]
# followed by [u][1-9]. This may optionally be followed by
# some more information.
SCVERSIONOUT=`$SCINSTALL -p`

# Get just the first part of the scinstall -p output since
# that is all we need. Debug versions may have some extended
# information, which we need to strip here.
SCRELEASEVERSION=`echo $SCVERSIONOUT | cut -f1 -d'_'`

# If SCRELEASEVERSION is not 3.1 and 3.1u1 and 3.1u2, it is
# effectively greater than or equal to 3.1u3. So, no need
# to check for heartbeat parameters. Exit successfully.
if [[ "${SCRELEASEVERSION}" != "3.1" && "${SCRELEASEVERSION}" != "3.1u1" && 
	"${SCRELEASEVERSION}" != "3.1u2" ]]; then
	# An SCRELEASEVERSION greater than or equal to 3.1u3.
	# No need to check for the heartbeat parameters.
	exit 0
fi

# SCRELEASEVERSION is either 3.1, 3.1u1 or 3.1u2 (less than 3.1u3). So,
# perform the heartbeat parameter checks.

# Check for the global heartbeat parameters, transport_heartbeat_timeout
# and transport_heartbeat_quantum. If either of these exist, it means
# that the heartbeat parameters have been changed using the new tunable
# heartbeats feature in u3. The changes made due to this need to be
# undone before the patch can be backed out. If these do not exist,
# the parameters have not been changed (or have been changed using
# some unsupported means) and can be left as they are.
hb_tout_pat="cluster.properties.transport_heartbeat_timeout"
hb_tout=`${NAWK} '/'"${hb_tout_pat}"'/ {printf $2}' \
	    ${CCR} 2>/dev/null`
hb_q_pat="cluster.properties.transport_heartbeat_quantum"
hb_q=`${NAWK} '/'"${hb_q_pat}"'/ {printf $2}' \
	    ${CCR} 2>/dev/null`
if [[ "${hb_tout}" == "" && "${hb_q}" == "" ]]; then
	exit 0
fi

# Get the nodes IDs in the cluster
NODES=`${GREP} '^cluster.nodes.[0-9]*.name	' ${CCR} \
	    2>/dev/null | ${NAWK} 'BEGIN { FS="." } {print $3}'`

good_setting="yes"

# Go through all nodes
for node_id in ${NODES}; do
	# Get the adapters of this node
	node_pat="'^cluster.nodes.${node_id}.adapters.[0-9]*.name	'"
	ADAPTERS=`eval "${GREP} ${node_pat} ${CCR} 2>/dev/null | \
	    ${NAWK} 'BEGIN { FS=\".\" } {print \\\$5}'"`

	# Go through each adapter of this node
	# Beginning of the adapter pattern
	pat_head="^cluster.nodes.${node_id}.adapters."
	for adapter_id in ${ADAPTERS}; do

		# Get the transport type of this adapter
		tr_pat="${pat_head}${adapter_id}.properties.transport_type	"
		trtype=`${NAWK} '/'"${tr_pat}"'/ {printf $2}' \
		    ${CCR} 2>/dev/null`

		# Set the adapter pattern tail
		pat_tail=".properties.${trtype}_heartbeat_"

		# Get the device name of this adapter
		dev_pat="${pat_head}${adapter_id}.properties.device_name	"
		dev_name=`${NAWK} '/'"${dev_pat}"'/ {printf $2}' \
		    ${CCR} 2>/dev/null`

		# Check the clpl files. Use the default clpl file
		# in case the adapter specific clpl file does not
		# exist.
		clpl_file="${CLPL_DIR}/SUNW.adapter.${dev_name}.clpl"
		if [[ -f ${clpl_file} ]]; then
			clpl_pat="^adapters.${dev_name}.${trtype}_heartbeat_"
		else
			clpl_file="${CLPL_DIR}/SUNW.adapter.default.clpl"
			clpl_pat="^adapters.default.${trtype}_heartbeat_"
		fi	

		# Get this adapter's default range
		t_def_clpl_pat="${clpl_pat}timeout_v1.default	"
		t_def_clpl=`${NAWK} '/'"${t_def_clpl_pat}"'/ {printf $2}' \
		    ${clpl_file} 2>/dev/null`

		# Get the timeout value for this adapter
		t_pat="${pat_head}${adapter_id}${pat_tail}timeout	"
		timeout=`${NAWK} '/'"${t_pat}"'/ {printf $2}' \
		    ${CCR} 2>/dev/null`

		#
		# Check if timeout is at the default value
		#
		if [[ ("${timeout}" -ne "${t_def_clpl}") ]]; then
			good_setting="no"
		fi


		# Get this adapter's quantum default
		q_def_clpl_pat="${clpl_pat}quantum_v1.default	"
		q_def_clpl=`${NAWK} '/'"${q_def_clpl_pat}"'/ {printf $2}' \
		    ${clpl_file} 2>/dev/null`

		# Get the quantum value for this adapter
		q_pat="${pat_head}${adapter_id}${pat_tail}quantum	"
		quantum=`${NAWK} '/'"${q_pat}"'/ {printf $2}' \
		    ${CCR} 2>/dev/null`

		#
		# Check if quantum is the same as the default
		#
		if [[ ("${quantum}" -ne "${q_def_clpl}") ]]; then
			good_setting="no"
		fi
	done
done

# If we are here, everything is ok
if [[ "${good_setting}" != "yes" ]]; then
	printf "$(gettext '\
You are downgrading to a patch level where heartbeat parameters must\
have the default values. You need to Boot into cluster mode and\
use cluster command to fix any heartbeat parameters that are not at the default\
values. Refer to related documents for details.')\n\n" 
	exit 1 
else 	
	exit 0
fi
