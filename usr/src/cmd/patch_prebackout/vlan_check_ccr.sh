#!/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# ident	"@(#)vlan_check_ccr.sh	1.7	08/07/17 SMI"
#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# Determines if Tagged VLAN (802.1Q) is being used on cluster interconnects.
# Returns 0 if no Tagged vlan; 1 otherwise.
#
# Used by pre-backout script during patch removal to decide if the removal
# can proceed.

PATH=/usr/bin:/usr/sbin



# Check if an alternate root has been provided as an argument.
# If so, use the paths relative to the alternate root, else
# use the paths as they are.
if [[ -n $1 ]]; then
	CCRDIR=$1/etc/cluster/ccr
	SCINSTALL=$1/usr/cluster/bin/scinstall
	export TEXTDOMAINDIR=$1/usr/cluster/lib/locale
else
	CCRDIR=/etc/cluster/ccr
	SCINSTALL=/usr/cluster/bin/scinstall
	export TEXTDOMAINDIR=/usr/cluster/lib/locale
fi

/usr/cluster/lib/sc/sc_zonescheck
if [ $? -ne 0 ]; then
	exit 1
fi

INFRASTRUCTURE=$CCRDIR/global/infrastructure

VLAN_ID_NAME="vlan_id"

# I18N
export TEXTDOMAIN=SUNW_SC_CMD
export LC_COLLATE=C

#
# Versions where feature is not supported
#
bad_versions="3.1 3.1u1 3.1u2 3.1u3"

version=`$SCINSTALL -p | cut -f1 -d'_'`

echo "$bad_versions" | grep -w $version >/dev/null
if [ $? != 0 ]; then
	#
	# Downgrading to a version that Tagged VLAN is supported,
	# i.e. 3.1u4 or later. Backout can proceed.
	#
	exit 0
fi

[ ! -f $INFRASTRUCTURE ] && exit 0

#
# Look for the vlan_id property. It does not exist if tagged vlan is not
# configured. Or if it's set to 0, its effect is the same as not having
# configured it and should be allowed.
#
grep "properties.${VLAN_ID_NAME}	" $INFRASTRUCTURE |
while read line; do
	set -- $line
	if [ "$2" != 0 ]; then
		found_vlan_id=true
	fi
done

if [[ "$found_vlan_id" == "" ]]; then
	exit 0
else
	printf "$(gettext '\
The transport adapter(s) are configured to use tagged VLANs. You must\
unconfigure these adapters and configure dedicated transport adapters before\
proceeding with the patch removal.\
')\n"
	echo
	exit 1
fi
