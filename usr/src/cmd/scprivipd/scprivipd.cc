/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scprivipd.cc	1.10	08/07/30 SMI"

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <libintl.h>
#include <locale.h>
#include <fcntl.h>
#include <pthread.h>
#include <signal.h>
#include <errno.h>
#include <string.h>
#include <zone.h>
#include <libzonecfg.h>
#include <stropts.h>
#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <net/if.h>
#include <inet/tcp.h>
#include <sys/sockio.h>
#include <pthread.h>
#include <sys/sc_syslog_msg.h>
#include <sys/rsrc_tag.h>
#include <h/ccr.h>
#include <h/naming.h>
#include <privip/privip_map.h>
#include <privip/privip_map_int.h>
#include <rgm/sczones.h>

#define	PRIVIPD_LOCK "/var/cluster/run/privipd.lock"

/*lint -e708 */
static pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
static pthread_cond_t cond = PTHREAD_COND_INITIALIZER;
/*lint +e708 */
int listen_to_ccr_updates;

static void make_daemon(void);
static void make_unique(char *lock_filename);
static void init_signal_handlers(void);
void sig_handler(void *);

static int register_ccr_callbacks(void);
static void register_zone_event_callbacks(void);
static void privip_setup(void);
static int ip_addr_exists(in_addr_t);
static int plumb_logical_clprivnet(in_addr_t, in_addr_t, zoneid_t);
static int unplumb_logical_clprivnet(in_addr_t);
static int setup_zone_netconfig(in_addr_t ipaddr, in_addr_t netmask,
			char *zone);
static int remove_zone_netconfig(in_addr_t ipaddr, char *zone);
static int tcp_conn_abort(in_addr_t ipaddr);

extern "C" void zone_starting_callback(const char *zonename);
extern "C" void zone_shutting_down_callback(const char *zonename);

bool debug = false;

privip_map privipd_map;
/*lint -e708 */
static pthread_mutex_t privipd_map_lock = PTHREAD_MUTEX_INITIALIZER;
/*lint +e708 */

class privipd_callback_impl : public McServerof<ccr::callback> {
public:
	void did_update(const char *, ccr::ccr_update_type,
	    Environment &);
	void did_update_zc(const char *, const char *, ccr::ccr_update_type,
	    Environment &);
	void did_recovery(const char *, ccr::ccr_update_type,
	    Environment &);
	void did_recovery_zc(const char *, const char *, ccr::ccr_update_type,
	    Environment &);
	void _unreferenced(unref_t);
};

/*
 * Create a daemon process and detach it from the controlling terminal.
 */
void
make_daemon()
{
	struct rlimit rl;
	int	fd;
	pid_t	pd;
	int	err;
	pid_t	rc;
	sc_syslog_msg_handle_t handle;
#if SOL_VERSION < __s9
	int i;
#endif

	pd = fork();
	if (pd < 0) {
		err = errno;
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_PRIVIPD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "fork: %s", strerror(err));
		sc_syslog_msg_done(&handle);
		exit(1);
	}

	/*
	 * parent exits after fork
	 */
	if (pd > 0)
		exit(0);		/* parent */

	/*
	 * In the child, redirect stdin to /dev/null so that if
	 * the daemon reads from stdin, it doesn't block.
	 * Redirect stdout, stderr to /dev/console so that any
	 * output printed by the daemon goes somewhere.
	 */

	if ((fd = open("/dev/null", O_RDONLY)) == -1) {
		err = errno;
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_PRIVIPD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Unable to open /dev/null: %s", strerror(err));
		sc_syslog_msg_done(&handle);
		exit(1);
	}

	if (dup2(fd, 0) == -1)
		(void) close(fd);

	/*
	 * redirect stdout and stderr by opening /dev/console
	 * and duping to the appropriate descriptors.
	 */
	fd = open("/dev/console", O_WRONLY);
	if (fd == -1) {
		err = errno;
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_PRIVIPD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Unable to open /dev/console: %s", strerror(err));
		sc_syslog_msg_done(&handle);
		exit(1);
	}
	if (dup2(fd, 1) == -1 || dup2(fd, 2) == -1)
		(void) close(fd);

	/*
	 * child closes rest of file descriptors
	 */
	rl.rlim_max = 0;
	(void) getrlimit(RLIMIT_NOFILE, &rl);
	if ((int)rl.rlim_max == 0)
		exit(1);

#if SOL_VERSION < __s9
	for (i = 3; i < (int)rl.rlim_max; i++) {
		(void) close(i);
	}
#else
	closefrom(3);
#endif

	/*
	 * child sets this process as group leader
	 */
	rc = setsid();
	if (rc == (pid_t)-1) {
		err = errno;
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_PRIVIPD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "setsid: %s", strerror(err));
		sc_syslog_msg_done(&handle);
		exit(1);
	}
}

/*
 * This function opens a file and locks it.
 * This will prevent another rgmd daemon from starting while we are running
 * This must be called after the fork (used to daemonize)
 * as the lock is not inherited by the child.
 */
static void
make_unique(char *lock_filename)
{
	int file;
	int err;
	struct flock lock_attr;
	sc_syslog_msg_handle_t handle;

	file = open(lock_filename, O_RDWR|O_CREAT, S_IWUSR);
	if (-1 == file) {
		err = errno;
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_PRIVIPD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The server was not able to start because it was not able to
		// create the lock file used to ensure that only one such
		// daemon is running at a time. An error message is output to
		// syslog.
		// @user_action
		// Save the /var/adm/messages file. Contact your authorized
		// Sun service provider to determine whether a workaround or
		// patch is available.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Unable to create the lock file (%s): %s\n",
		    lock_filename, strerror(err));
		sc_syslog_msg_done(&handle);
		exit(1);
	}

	/*
	 * Force the mode mask in case umask has been specified.
	 * Will not exit if chmod does not succeed.
	 */
	(void) chmod(lock_filename, S_IWUSR);

	lock_attr.l_type = F_WRLCK;
	lock_attr.l_whence = 0;
	lock_attr.l_start = 0;
	lock_attr.l_len = 0;

	err = fcntl(file, F_SETLK, &lock_attr);
	if (-1 == err) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_PRIVIPD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The server did not start because there is already another
		// identical server running
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "There is already an instance of this daemon running\n");
		sc_syslog_msg_done(&handle);
		exit(1);
	}
}

int
main(int, char **argv)
{
	char *progname = argv[0];

	(void) setlocale(LC_ALL, "");
	(void) textdomain(TEXT_DOMAIN);
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	(void) sc_syslog_msg_set_syslog_tag(SC_SYSLOG_PRIVIPD_TAG);
	os::sc_syslog_msg logger(SC_SYSLOG_PRIVIPD_TAG, "", NULL);

	/*
	 * Check if the process was started by the superuser.
	 */
	if (getuid() != 0) {
		(void) logger.log(LOG_ERR, MESSAGE,
		    "fatal: must be superuser to start %s",
		    progname);
		exit(1);
	}

	if (sc_zonescheck() != 0)
		return (1);

	// daemonize
	make_daemon();

	// Make sure another daemon is not running
	make_unique(PRIVIPD_LOCK);

	// Check to see if the pernode IP address range has been
	// changed. If it has changed, the existing private IP addresses
	// for the local zones will not belong to the pernode IP address
	// range and therefore must be reallocated.
	if (scprivip_modify_ip() != 0)
		return (-1);

	// Initialize the privipd_map
	if (privipd_map.initialize() != 0)
		return (-1);

	// The ORB initialize has happened as part of the privipd_map
	// initialization. Next step is to register the CCR callbacks
	(void) register_ccr_callbacks();

	// Register to listen for zone bootup/shutdown events
	register_zone_event_callbacks();

	// set up the signal handlers
	init_signal_handlers();

	// Listen for the CCR callbacks. Exit when signalled.
	CL_PANIC(pthread_mutex_lock(&lock) == 0);
	listen_to_ccr_updates = 1;
	while (listen_to_ccr_updates == 1) {
		CL_PANIC(pthread_cond_wait(&cond, &lock) == 0);
	}
	CL_PANIC(pthread_mutex_unlock(&lock) == 0);

	// Shutdown the privipd_map
	privipd_map.shutdown();

	// Exit after signalled
	return (0);
}

static int
register_ccr_callbacks()
{
	privipd_callback_impl *privip_ccr_cb;
	Environment e;
	CORBA::Exception *ex;
	ccr::callback_var cb_v;
	ccr::readonly_table_var tab_v;
	ccr::directory_var ccr_v;
	os::sc_syslog_msg logger(SC_SYSLOG_PRIVIPD_TAG, "", NULL);

	naming::naming_context_var ctx_v = ns::local_nameserver();
	if (CORBA::is_nil(ctx_v)) {
		//
		// SCMSGS
		// @explanation
		// The scprivipd daemon was unable to register for
		// configuration callbacks.
		// @user_action
		// These callbacks are used only for enabling or disabling the
		// private IP communication for local zones. So, this feature
		// will be unavailable. To recover, it may be necessary to
		// reboot this node or the entire cluster. Contact your
		// authorized Sun service provider to determine whether a
		// workaround or patch is available.
		//
		(void) logger.log(LOG_ERR, MESSAGE,
		    "Failed to register configuration callbacks\n");
		abort();
	}

	CORBA::Object_var obj_v = ctx_v->resolve("ccr_directory", e);
	if (e.exception()) {
		e.exception()->print_exception("get ccr");
		abort();
	}
	else
		ccr_v = ccr::directory::_narrow(obj_v);

	if (CORBA::is_nil(ccr_v))  {
		(void) logger.log(LOG_ERR, MESSAGE,
		    "Failed to register configuration callbacks\n");
		abort();
	}

	tab_v = ccr_v->lookup(PRIVIP_CCR_TABLE, e);
	if ((ex = e.exception()) != NULL) {
		if (ccr::no_such_table::_exnarrow(ex)) {
			e.clear();
			ccr_v->create_table(PRIVIP_CCR_TABLE, e);
			if ((ex = e.exception()) != NULL) {
				// Table exists exception is ok.
				if (ccr::table_exists::_exnarrow(ex)) {
					e.clear();
				} else {
					e.clear();
					(void) logger.log(LOG_ERR, MESSAGE,
					    "Failed to register configuration "
					    "callbacks\n");
					abort();
				}
			}
			tab_v = ccr_v->lookup(PRIVIP_CCR_TABLE, e);
			if ((ex = e.exception()) != NULL) {
				e.clear();
				(void) logger.log(LOG_ERR, MESSAGE,
					"Failed to register configuration "
					"callbacks\n");
				abort();
			}
		} else {
			e.clear();
			(void) logger.log(LOG_ERR, MESSAGE,
				    "Failed to register configuration "
				    "callbacks\n");
			abort();
		}
	}

	privip_ccr_cb = new privipd_callback_impl;
	cb_v = privip_ccr_cb->get_objref();

	tab_v->register_callbacks(cb_v, e);
	if (e.exception()) {
		(void) logger.log(LOG_ERR, MESSAGE,
			"Failed to register configuration callbacks\n");
		abort();
	}

	return (0);
}

static void
register_zone_event_callbacks()
{
	os::sc_syslog_msg logger(SC_SYSLOG_PRIVIPD_TAG, "", NULL);

	zone_state_callback_t callbacks[5] = {zone_starting_callback, NULL,
		NULL, zone_shutting_down_callback, NULL};

	if (register_zone_state_callbacks(callbacks, 5) != 0) {
		//
		// SCMSGS
		// @explanation
		// The scprivipd failed to register with sc_zonesd for zone
		// state change notification.
		// @user_action
		// The private IP communication for local zones depends on
		// scprivipd and the registration for zone state change
		// notification. So, this feature will not work as expected as
		// a result of this error. Contact your authorized Sun service
		// provider to determine whether a workaround or patch is
		// available.
		//
		(void) logger.log(LOG_ERR, MESSAGE,
			"Failed to register zone state change "
			"notification callbacks\n");
		/*
		 * An error means that the zone state change notification
		 * functionality isn't working correctly. Just return
		 * and allow everything else to run.
		 */
		return;
	}

}

void privipd_callback_impl::did_update_zc(
	const char *,
	const char *,
	ccr::ccr_update_type,
	Environment &)
{
	//
	// For each entry in the privip_ccr table (if it exists and
	// has changed) belonging to the local node id, plumb or
	// unplumb the IP address in the zone specified if the zone is
	// in running state.
	//
	privip_setup();
}

void privipd_callback_impl::did_update(
	const char *,
	ccr::ccr_update_type,
	Environment &)
{
	// For each entry in the privip_ccr table (if it exists and
	// has changed) belonging to the local node id, plumb or
	// unplumb the IP address in the zone specified if the zone is
	// in running state.
	privip_setup();
}

void privipd_callback_impl::did_recovery(const char *, ccr::ccr_update_type,
    Environment &)
{
}

void privipd_callback_impl::did_recovery_zc(const char *,
	const char *, ccr::ccr_update_type, Environment &)
{
}

void privipd_callback_impl::_unreferenced(unref_t)
{
}

int
ip_addr_exists(in_addr_t ipaddr)
{
	int s;
	struct lifnum lifn;
	struct lifconf lifc;
	struct lifreq *lifrp;
	char *addrlist = NULL;
	struct sockaddr_in *sin;
	int i;

	s = socket(AF_INET, SOCK_DGRAM, 0);
	if (s < 0)
		return (0);

	lifn.lifn_family = AF_INET;
	lifn.lifn_flags = LIFC_ALLZONES;
	if (ioctl(s, SIOCGLIFNUM, &lifn) < 0) {
		(void) close(s);
		return (0);
	}

	addrlist = (char *)calloc((size_t)lifn.lifn_count,
		(size_t)sizeof (struct lifreq));
	if (addrlist == NULL) {
		(void) close(s);
		return (0);
	}

	lifc.lifc_family = AF_INET;
	lifc.lifc_len = lifn.lifn_count * (int)sizeof (struct lifreq);
	lifc.lifc_buf = (caddr_t)addrlist;
	lifc.lifc_flags = LIFC_ALLZONES;

	if (ioctl(s, SIOCGLIFCONF, (char *)&lifc) < 0) {
		free(addrlist);
		addrlist = NULL;
		(void) close(s);
		return (0);
	}

	lifrp = lifc.lifc_req;
	for (i = 0; i < (lifc.lifc_len / (int)sizeof (struct lifreq));
		i++, lifrp++) {
		sin = (struct sockaddr_in *)&lifrp->lifr_addr;
		if (sin->sin_addr.s_addr == ipaddr) {
			free(addrlist);
			addrlist = NULL;
			(void) close(s);
			return (1);
		}
	}
	free(addrlist);
	addrlist = NULL;
	(void) close(s);
	return (0);
}

static void
privip_setup()
{
	privip_map_elem *privip_elem = NULL;
	char *zonename;
	in_addr_t ipaddr;
	const char *netmask;
	struct in_addr in;
	in_addr_t pernode_netmask;
	int err;
	os::sc_syslog_msg logger(SC_SYSLOG_PRIVIPD_TAG, "", NULL);

	// Get the pernode_netmask from the CCR. Note that
	// the clconf_lib_init has happened as part of the
	// privip_map initialization.
	(void) clconf_get_user_netmask(&in);
	netmask = inet_ntoa(in);
	pernode_netmask = inet_addr(netmask);

	CL_PANIC(pthread_mutex_lock(&privipd_map_lock) == 0);

	// Obtain values from CCR infrastructure file.
	// Read the privip_ccr table to identify the update
	if (privipd_map.read_ccr_mappings() != 0) {
		//
		// SCMSGS
		// @explanation
		// The private IP address assigned to a zone was not
		// configured correctly.
		// @user_action
		// The private IP address assigned to the zone is used for
		// private IP communication for the zone. So, this feature may
		// not work as expected as a result of this error. Contact
		// your authorized Sun service provider to determine whether a
		// workaround or patch is available.
		//
		(void) logger.log(LOG_ERR, MESSAGE,
			"Failed to configure the private IP address for "
			"a zone\n");
		CL_PANIC(pthread_mutex_unlock(&privipd_map_lock) == 0);
		return;
	}

	if (privipd_map.is_changed()) {
		SList<privip_map_elem>::ListIterator
			iter(privipd_map.get_map_list());
		while ((privip_elem = iter.get_current()) != NULL) {
			err = 0;
			if (privip_elem->get_nodeid() ==
				orb_conf::local_nodeid()) {
				if (privip_elem->get_action() ==
					privip_map_elem::PRIVIP_ADD) {
					ipaddr = privip_elem->get_addr();
					ipaddr = htonl(ipaddr);
					zonename = privip_elem->get_zonename();
					err = setup_zone_netconfig(ipaddr,
						pernode_netmask, zonename);
					if (err != 0) {
						(void) logger.log(LOG_ERR,
							MESSAGE,
							"Failed to configure "
							"the private IP "
							"address for a zone\n");
					}
					privip_elem->set_action(
						privip_map_elem::PRIVIP_EXISTS);
					iter.advance();
				} else if (privip_elem->get_action() ==
					privip_map_elem::PRIVIP_REMOVE) {
					ipaddr = privip_elem->get_addr();
					ipaddr = htonl(ipaddr);
					zonename = privip_elem->get_zonename();
					err = remove_zone_netconfig(ipaddr,
						zonename);
					if (err != 0) {
						//
						// SCMSGS
						// @explanation
						// The private IP address
						// assigned to a zone was not
						// unplumbed correctly.
						// @user_action
						// The private IP address
						// assigned to the zone is
						// used for private IP
						// communication for the zone.
						// So, this feature may not
						// work as expected as a
						// result of this error.
						// Contact your authorized Sun
						// service provider to
						// determine whether a
						// workaround or patch is
						// available.
						//
						(void) logger.log(LOG_ERR,
							MESSAGE,
							"Failed to remove "
							"the private IP "
							"address for a zone\n");
					}
					// Abort the connections using
					// this IP address so that the
					// clients are aware of it going
					// away
					(void) tcp_conn_abort(ipaddr);

					// Advance the iterator before
					// doing the erase.
					iter.advance();

					(void) privipd_map.get_map_list().
						erase(privip_elem);
					delete privip_elem;
				} else {
					iter.advance();

				}
			} else
				iter.advance();
		}
	}
	CL_PANIC(pthread_mutex_unlock(&privipd_map_lock) == 0);

}

static int
plumb_logical_clprivnet(in_addr_t ipaddr, in_addr_t netmask, zoneid_t zoneid)
{
	char ifname[LIFNAMSIZ];
	int s;
	struct lifreq lifr;
	unsigned short setflags = 0;
	unsigned short clearflags = 0;

	// Check if IP address has already been plumbed.
	if (ip_addr_exists(ipaddr) == 1)
		return (0);

	s = socket(AF_INET, SOCK_DGRAM, 0);
	if (s < 0) {
		return (-1);
	}

	(void) strcpy(ifname, "clprivnet0");

	// Add logical interface
	bzero(&lifr, sizeof (lifr));
	(void) strncpy(lifr.lifr_name, ifname, sizeof (lifr.lifr_name) - 1);
	if (ioctl(s, SIOCLIFADDIF, &lifr) < 0) {
		(void) close(s);
		return (-1);
	}
	(void) strncpy(ifname, lifr.lifr_name, sizeof (lifr.lifr_name) - 1);

	// Netmask for the logical interface
	lifr.lifr_addr.ss_family = AF_INET;
	((struct sockaddr_in *)&lifr.lifr_addr)->sin_addr.s_addr =
		netmask;

	if (ioctl(s, SIOCSLIFNETMASK, /*lint !e737 */
		&lifr) < 0) {
		(void) close(s);
		return (-1);
	}

	// IP address for the logical interface
	((struct sockaddr_in *)&lifr.lifr_addr)->sin_addr.s_addr =
		ipaddr;

	if (ioctl(s, SIOCSLIFADDR, /*lint !e737 */
		&lifr) < 0) {
		(void) close(s);
		return (-1);
	}

	// Set flags to indicate its a private interface
	if (ioctl(s, SIOCGLIFFLAGS, &lifr) < 0) {
		(void) close(s);
		return (-1);
	}

	setflags = IFF_UP | IFF_PRIVATE;
	clearflags = IFF_NOARP;
	lifr.lifr_flags &= ~((uint64_t)clearflags);
	lifr.lifr_flags |= ((uint64_t)setflags);

	if (ioctl(s, SIOCSLIFFLAGS, /*lint !e737 */
		&lifr) < 0) {
		(void) close(s);
		return (-1);
	}

	// Set the zoneid for the interface
	lifr.lifr_zoneid = zoneid;
	if (ioctl(s, SIOCSLIFZONE, /*lint !e737 */
		(caddr_t)&lifr) < 0) {
		(void) close(s);
		return (-1);
	}

	(void) close(s);
	return (0);
} 

static int
setup_zone_netconfig(in_addr_t ipaddr, in_addr_t netmask, char *zonename)
{
	int err;
	zone_state_t zstate;
	zoneid_t zoneid;
	os::sc_syslog_msg logger(SC_SYSLOG_PRIVIPD_TAG, "", NULL);

	/* Plumb only if zone is running */
	err = zone_get_state(zonename, &zstate);
	if ((err == 0) && (zstate == ZONE_STATE_RUNNING)) {
		zoneid = getzoneidbyname(zonename);
		if (zoneid == -1)
			return (-1);
		err = plumb_logical_clprivnet(ipaddr, netmask, zoneid);
		if (err != 0) {
			(void) logger.log(LOG_ERR, MESSAGE,
				"Failed to configure the private IP address "
				"for a zone\n");
			return (err);
		}
	}
	return (0);
}

static int
unplumb_logical_clprivnet(in_addr_t ipaddr)
{
	char ifname[LIFNAMSIZ];
	int s;
	struct lifreq lifr;

	// Check if IP address has already been unplumbed.
	if (ip_addr_exists(ipaddr) == 0)
		return (0);

	s = socket(AF_INET, SOCK_DGRAM, 0);
	if (s < 0)
		return (-1);

	(void) strcpy(ifname, "clprivnet0");

	bzero(&lifr, sizeof (lifr));
	(void) strncpy(lifr.lifr_name, ifname, sizeof (lifr.lifr_name) - 1);
	((struct sockaddr_in *)&lifr.lifr_addr)->sin_addr.s_addr =
		ipaddr;
	lifr.lifr_addr.ss_family = AF_INET;

	if (ioctl(s, SIOCLIFREMOVEIF, /*lint !e737 */
		&lifr) < 0) {
		(void) close(s);
		return (-1);
	}

	(void) close(s);
	return (0);
}

static int
remove_zone_netconfig(in_addr_t ipaddr, char *zonename)
{
	int err;
	zone_state_t zstate;
	os::sc_syslog_msg logger(SC_SYSLOG_PRIVIPD_TAG, "", NULL);

	/* Unplumb only if zone is running */
	err = zone_get_state(zonename, &zstate);
	if ((err == 0) && (zstate == ZONE_STATE_RUNNING)) {
		err = unplumb_logical_clprivnet(ipaddr);
		if (err != 0) {
			(void) logger.log(LOG_ERR, MESSAGE,
				"Failed to remove the private IP address "
				"for a zone\n");
			return (err);
		}
	}

	return (0);
}

void
zone_starting_callback(const char *zonename)
{
	privip_map_elem *privip_elem = NULL;
	char *zname;
	zoneid_t zoneid;
	in_addr_t ipaddr, pernode_netmask;
	const char *netmask;
	struct in_addr in;
	int err;
	os::sc_syslog_msg logger(SC_SYSLOG_PRIVIPD_TAG, "", NULL);

	// Get the pernode_netmask from the CCR. Note that
	// the clconf_lib_init has happened as part of the
	// privip_map initialization.
	(void) clconf_get_user_netmask(&in);
	netmask = inet_ntoa(in);
	pernode_netmask = inet_addr(netmask);

	CL_PANIC(pthread_mutex_lock(&privipd_map_lock) == 0);

	// Obtain values from CCR infrastructure file.
	// Read the privip_ccr table to identify the update
	if (privipd_map.read_ccr_mappings() != 0) {
		(void) logger.log(LOG_ERR, MESSAGE,
			"Failed to configure the private IP address "
			"for a zone\n");
		CL_PANIC(pthread_mutex_unlock(&privipd_map_lock) == 0);
		return;
	}

	SList<privip_map_elem>::ListIterator
		iter(privipd_map.get_map_list());
	while ((privip_elem = iter.get_current()) != NULL) {
		zname = privip_elem->get_zonename();
		err = 0;
		if ((privip_elem->get_nodeid() == orb_conf::local_nodeid()) &&
			(strcmp(zname, zonename) == 0)) {
				ipaddr = privip_elem->get_addr();
				ipaddr = htonl(ipaddr);
				zoneid = getzoneidbyname(zname);
				if (zoneid == -1) {
					CL_PANIC(pthread_mutex_unlock(
						&privipd_map_lock) == 0);
					return;
				}
				err = plumb_logical_clprivnet(ipaddr,
					pernode_netmask, zoneid);
				if (err != 0) {
					(void) logger.log(LOG_ERR, MESSAGE,
						"Failed to configure "
						"the private IP address for "
						"a zone\n");
				}
				/*
				 * There is only one private IP address
				 * per zone. So, we can return here.
				 */
				CL_PANIC(pthread_mutex_unlock(
					&privipd_map_lock) == 0);
				return;
		}
		iter.advance();
	}

	CL_PANIC(pthread_mutex_unlock(&privipd_map_lock) == 0);
}

void
zone_shutting_down_callback(const char *zonename)
{
	privip_map_elem *privip_elem = NULL;
	char *zname;
	in_addr_t ipaddr;
	int err;
	os::sc_syslog_msg logger(SC_SYSLOG_PRIVIPD_TAG, "", NULL);

	CL_PANIC(pthread_mutex_lock(&privipd_map_lock) == 0);

	// Obtain values from CCR infrastructure file.
	// Read the privip_ccr table to identify the update
	if (privipd_map.read_ccr_mappings() != 0) {
		(void) logger.log(LOG_ERR, MESSAGE,
			"Failed to configure the private IP address for "
			"a zone\n");
		CL_PANIC(pthread_mutex_unlock(&privipd_map_lock) == 0);
		return;
	}

	SList<privip_map_elem>::ListIterator
		iter(privipd_map.get_map_list());
	while ((privip_elem = iter.get_current()) != NULL) {
		zname = privip_elem->get_zonename();
		err = 0;
		if ((privip_elem->get_nodeid() == orb_conf::local_nodeid()) &&
			(strcmp(zname, zonename) == 0)) {
				ipaddr = privip_elem->get_addr();
				ipaddr = htonl(ipaddr);
				err = unplumb_logical_clprivnet(ipaddr);
				if (err != 0) {
					(void) logger.log(LOG_ERR, MESSAGE,
						"Failed to remove "
						"the private IP address "
						"for a zone\n");
				}
				/*
				 * There is only one private IP address
				 * per zone. So, we can return here.
				 */
				CL_PANIC(pthread_mutex_unlock(
					&privipd_map_lock) == 0);
				return;
		}
		iter.advance();
	}
	CL_PANIC(pthread_mutex_unlock(&privipd_map_lock) == 0);
}

/*
 * init_signal_handlers
 * ---------------------
 * Blocks all signals in this thread, which will be the parent of the
 * other threads, so that they will inherit the signal mask.
 * This is especially important in those threads that will be handling
 * and making door calls.
 *
 * Creates a thread to handle all the signals.  It runs the sig_handler
 * method below.
 *
 * If we fail in any of this, exit the daemon.
 */
static void
init_signal_handlers(void)
{
	sc_syslog_msg_handle_t handle;

	// block all signals for all threads
	sigset_t sig_set;
	int err;

	if (sigfillset(&sig_set) != 0) {
		err = errno;
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_PRIVIPD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "sigfillset: %s", strerror(err));
		sc_syslog_msg_done(&handle);
		exit(err);
	}

	if ((err = pthread_sigmask(SIG_BLOCK, &sig_set, NULL)) != 0) {
		char *err_str = strerror(err);
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_PRIVIPD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "pthread_sigmask: %s", err_str ? err_str : "");
		sc_syslog_msg_done(&handle);
		exit(err);
	}

	// now spawn the thread that will actually handle the signals
	pthread_t sig_thr;
	if ((err = pthread_create(&sig_thr, NULL,
	    (void *(*)(void *))sig_handler, NULL)) != 0) {
		char *err_str = strerror(err);
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_PRIVIPD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "pthread_create: %s", err_str ? err_str : "");
		sc_syslog_msg_done(&handle);
		exit(err);
	}
}


/*
 * sig_handler
 * --------------
 * Signal handler
 *
 * This function runs in a dedicated signal handling thread.  The thread
 * should be created with a signal mask blocking all signals.
 *
 * Uses an infinite loop around sigwait, to wait for all signals.
 * Exits on SIGTERM, continues on all others.
 *
 * If anything goes awry, exits immediately.
 */
void
sig_handler(void *)
{
	sc_syslog_msg_handle_t handle;

	sigset_t sig_set;
	int err, sig;

	if (sigfillset(&sig_set) != 0) {
		err = errno;
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_PRIVIPD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "sigfillset: %s", strerror(err));
		sc_syslog_msg_done(&handle);
		exit(err);
	}

	while (true) {
		if ((sig = sigwait(&sig_set)) == -1) {
			err = errno;
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_PRIVIPD_TAG, "");
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "sigwait: %s", strerror(err));
			sc_syslog_msg_done(&handle);
			exit(err);
		}

		switch (sig) {
		case SIGTERM:
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_PRIVIPD_TAG, "");
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "fatal: received signal %d", sig);
			sc_syslog_msg_done(&handle);
			// Set listen_to_ccr_updates to 0 to have the
			// daemon exit
			CL_PANIC(pthread_mutex_lock(&lock) == 0);
			listen_to_ccr_updates = 0;
			CL_PANIC(pthread_cond_broadcast(&cond) == 0);
			CL_PANIC(pthread_mutex_unlock(&lock) == 0);
			break;
		default:
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_PRIVIPD_TAG, "");
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "received signal %d: continuing", sig);
			sc_syslog_msg_done(&handle);
		}
	}
}

static int
tcp_conn_abort(in_addr_t ipaddr)
{
	int s;
	struct strioctl iocb;
	tcp_ioc_abort_conn_t tcpac;
	struct sockaddr_in *sinp = (struct sockaddr_in *)&tcpac.ac_local;

	bzero(&iocb, sizeof (iocb));
	bzero(&tcpac, sizeof (tcpac));

	s = socket(AF_INET, SOCK_STREAM, 0);
	if (s < 0)
		return (-1);

	tcpac.ac_local.ss_family = AF_INET;
	sinp->sin_addr.s_addr = ipaddr;
	tcpac.ac_remote.ss_family = AF_INET;

	tcpac.ac_start = TCPS_SYN_SENT;
	tcpac.ac_end = TCPS_TIME_WAIT;

	iocb.ic_cmd = TCP_IOC_ABORT_CONN;
	iocb.ic_timout = 0;
	iocb.ic_len = (int)sizeof (tcp_ioc_abort_conn_t);
	iocb.ic_dp = (char *)&tcpac;

	if (ioctl(s, I_STR, &iocb) != 0) {
		(void) close(s);
		return (-1);
	}

	(void) close(s);
	return (0);
}
