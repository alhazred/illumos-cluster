/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)chksci.cc	1.11	08/05/20 SMI"

#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <sys/os.h>
#include <sys/clconf_int.h>

// lint complains about destructor not being virtual for class stream_MT
//lint -e1512

void set_error(const char *, const char *);
int duplicate_entry(char *list[], const char *name);
extern char **adapter_ids;
/*
 * Checks for SCI adapter specific properties.
 * As per the SCI documentation, the adapter_id (called the nodeid in
 * SCI documentation) must follow these criteria:
 *
 * adapter ids should be within the listed range for that port:
 * switchport 	nodeid
 * 0		4-60
 * 1		68-124
 * 2		132-188
 *		:
 *		:
 * 7		452-508
 *
 * Also adapter_id should be an integer divisible by 4.
 *
 * So, in order to satisfy the above criteria, we have the various checks
 * in the following two functions.
 * adapter_id % 4 == 0
 * adapter_id / 64 == (0-7)
 * adapter_id > 64*port_number and adapter_id < 64*(port_number+1)
 *
 * Also, based on how the adapter_ids are calculated by the topology
 * manager, we need to have the following additional check
 * adapter_id % 64 >= 8
 *
 * These checks are done in check_sci_adapter and check_sci_cc functions.
 *
 */
void
check_sci_adapter(clconf_adapter_t *adapter, const char *name,
    const char *ttype)
{
	const char *prval;
	char *endp;
	int aid;

	if ((prval = clconf_obj_get_property((clconf_obj_t *)adapter,
	    "adapter_id")) != NULL) {
		if (duplicate_entry(adapter_ids, prval))
			set_error("Duplicate adapter id", prval);
		errno = 0;
		aid = (int)strtol(prval, &endp, 10);
		if ((errno != 0) || (*endp != '\0') || ((aid % 4) != 0) ||
		    ((aid % 64) < 8) || ((aid / 64) > 7) || (aid <= 0))
			set_error("Invalid adapter_id for adapter", name);
	}
	if ((ttype) && (strcmp(ttype, "dlpi") == 0)) {
		if ((prval = clconf_obj_get_property((clconf_obj_t *)adapter,
		    "dlpi_device_name")) == NULL)
			set_error("Missing dlpi_device_name prop. for adapter",
			    name);
		else if (strcmp(prval, "scid") != 0)
			set_error("Wrong dlpi_device_name value for adapter",
			    name);
	}
}

/*
 * SCI adapter/switch specific checking
 * This specifically checks for the following:
 * adapter_id > 64*port_number and adapter_id < 64*(port_number+1)
 */
void
check_sci_cc(clconf_port_t *port, int bb_port, const char *ids)
{
	clconf_obj_t *obj;
	const char *prval;
	char *endp;
	int adp_id;

	obj = clconf_obj_get_parent((clconf_obj_t *)port);
	if ((prval = clconf_obj_get_property(obj, "device_name")) == NULL)
		return;
	if (strcmp(prval, "sci") != 0)
		return;
	if ((prval = clconf_obj_get_property(obj, "adapter_id")) == NULL)
		return;
	errno = 0;
	adp_id = (int)strtol(prval, &endp, 10);
	if ((errno != 0) || (*endp != '\0') ||
	    (adp_id <= 64*bb_port) || (adp_id >= 64*(bb_port+1)))
		set_error("Bad SCI switch/adapter connections cable ", ids);
}
