/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)chkinfr.cc	1.19	08/07/17 SMI"

#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <netdb.h>
#include <sys/param.h>
#include <sys/os.h>
#include <arpa/inet.h>
#include <sys/clconf_int.h>
#include <clconf/clconf_file_io.h>
#include <sys/clconf_property.h>
#include <rgm/sczones.h>

// lint complains about destructor not being virtual for class stream_MT
//lint -e1512

extern const char *clconf_obj_get_state(clconf_obj_t *);
void check_sci_adapter(clconf_adapter_t *, const char *, const char *);
void check_sci_cc(clconf_port_t *, int, const char *);

static char **ip_addrs;
char **adapter_ids;
/*
 * Dynamically allocated memory for the following variables is never
 * explicitly freed. Since these are global variables and need to be there
 * for most part of the program execution, cleanup of this memory will
 * done by exit()
 */
static const char *supp_adap_devnames, *supp_bb_types;
static char *clpl_prefix = NULL;
static uint_t **nd_tab, **ndbb_tab;

static int quorum = 0;
static int strict = 0;
static int rcode = 0;
static int force = 0;
static uint_t nnodes = 0, nbbs = 0;

/*
 * prints the error message composed of both the arguments and sets the
 * return code to 1.
 */
void
set_error(const char *msg, const char *s)
{
	os::printf("Error : %s", msg);
	if (s != NULL)
		os::printf(" - %s", s);
	os::printf("\n");
	if (force == 0)
		rcode = 1;
}

void
print_warning(const char *msg)
{
	os::printf("Warning : %s\n", msg);
}


/*
 * Returns numbers of '.'s in the string passed as argument
 */
int
ndots(const char *s)
{
	int cnt = 0;

	while (*s)
		if (*s++ == '.')
			cnt++;
	return (cnt);
}

/*
 * Looks at the clpl tree to find the object with given component name
 * 'ctype', groupname 'gname' and property name 'pname'.
 * Gets the property description of the object.
 * Returns 1 if falied and 0 if successful.
 */
int
get_prop_from_clpltree(clconf_objtype_t ctype, const char *gname,
    const char *pname, clconf_propdesc_t *pr)
{
	clconf_iter_t *iter1;
	clconf_obj_t 	*obj, *obj1;

	obj = clpl_get_object(ctype, gname);
	if (obj == NULL)
		return (1);
	iter1 = clpl_obj_get_properties(obj);
	for (; (obj1 = clconf_iter_get_current(iter1)) != NULL;
	    clconf_iter_advance(iter1)) {
		if ((strcmp(clpl_get_name(obj1), pname) == 0)) {
		    *pr = clpl_prop_get_description(obj1);
		    clconf_iter_release(iter1);
		    return (0);
		}
	}
	clconf_iter_release(iter1);
	return (1);
}

/*
 * Reads the clplfile, whose name is passed as an argument, and constructs
 * the tree. If the tree is already constructed, then it adds the file
 * contents as a branch to it.
 */
void
read_clplfile(const char *fname)
{
	char filename[MAXPATHLEN];

	/*
	 * No need to do clpl_refresh() because all clpl files are under
	 * one root.
	 */

	if (filename != NULL) {
		if (os::snprintf(filename, MAXPATHLEN, "%s/%s", clpl_prefix,
		    fname) >= (MAXPATHLEN-1))
			filename[MAXPATHLEN-1] = '\0';
		clpl_read(filename);
	}
}

/*
 * Looks at the list passed as the first argument for the second argument
 * 'name' and if it already exists, then returns 1, otherwise adds name to
 * the list and returns 0.
 */
int
duplicate_entry(char *list[], const char *name)
{
	int i;

	for (i = 0; list[i] != NULL; i++)
		if (strcmp(list[i], name) == 0)
			return (1);	// Duplicate Entry
	list[i] = os::strdup(name);
	return (0);
}

/*
 * Returns 1, if the state is "enabled", otherwise returns 0.
 * If strict option is selected and the state is neither "enabled" nor
 * "disabled" then error message is printed.
 */
int
valid_state(const char *state)
{
	if ((strcmp(state, "enabled") != 0) &&
	    (strcmp(state, "disabled") != 0)) {
		if (strict)
			set_error("Invalid state", state);
		return (0);
	}
	if (strcmp(state, "disabled") == 0)
		return (0);
	return (1);
}

/*
 * Checks the property, with given groupname 'gname', property name 'pname'
 * and its value 'value', for its validity by looking at the corresponding
 * clpl file. It gets the property's minimun and maximum values from
 * the clpl file and compares them with the interger value of 'value'.
 * Prints error if it is out of range.
 */
void
check_property(const char *gname, const char *pname, const char *value)
{
	clconf_propdesc_t pr;
	char *endp;
	int val;
	int err = 1;

	errno = 0;
	val = (int)strtol(value, &endp, 10);
	if ((errno == 0) && (*endp == '\0')) {
		if (get_prop_from_clpltree(CL_ADAPTER, gname, pname, &pr) != 0)
			set_error("Wrong/Corrupted clpl files", "");
		else if ((val >= pr.min) && (val <= pr.max))
			err = 0;
	}
	if (err)
		set_error("Illegal value for the property", pname);
}

/*
 * Checks adapter object passed as the first argument for validity.
 * First, it checks if the state of the adapter is enabled, if not, then
 * the properties of the adapter are not validated.
 * The routine prints error in the following cases:
 *	- If the state is not available
 *	- If any of the transport_type, device_name, ip_address, netmask,
 *	  device_instance are missing
 *	- If the IP address/netmask is not in A.B.C.D format and A,B,C,D all are
 *	  with in the range of 0 to 255.
 *	- If the IP address is Duplicate.
 *	- If the device_name is not supported.
 *	- If the device name is sci and adapter_id property is missing.
 *	- If the device name is sci and transport type is dlpi and
 *	  dlpi_device_name property is missing
 *	- If heartbeat_timeout, heartbeat_quantum, nw_bandwidth, and bandwidth
 *	  properties are out of range.
 *	- If the adapter has no ports or more than one port.
 *	- If the port name of the adapter is missing.
 */
int
check_adapter(clconf_cluster_t *clconf, clconf_adapter_t *adapter,
		const char *name)
{
	int nports = 0;
	const char *state, *prval, *device_name, *ttype;
	char *hbt, *hbq, *trtypes = NULL;
	clconf_iter_t *iter1;
	clconf_port_t *port;
	clconf_propdesc_t prop;

	state = clconf_obj_get_state((clconf_obj_t *)adapter);
	if (state == NULL)
		set_error("Missing state for adapter", name);
	else
		if (! valid_state(state))
			return (0);

	if (!clconf_obj_enabled((clconf_obj_t *)adapter))
		return (0);
	if ((prval = clconf_obj_get_property((clconf_obj_t *)adapter,
	    "ip_address")) != NULL) {
		if ((inet_addr(prval) == (in_addr_t)-1) || (ndots(prval) != 3))
			set_error("Not a valid IP address", prval);
		else if (duplicate_entry(ip_addrs, prval))
			set_error("Duplicate IP address", prval);
	}

	if ((ttype = clconf_obj_get_property((clconf_obj_t *)adapter,
	    "transport_type")) == NULL)
		set_error("Missing transport type property for adapter", name);

	if ((device_name = clconf_obj_get_property((clconf_obj_t *)adapter,
	    "device_name")) != NULL) {
		if (strstr(supp_adap_devnames, device_name) == NULL)
			device_name = "default";

		if (get_prop_from_clpltree(CL_ADAPTER, device_name,
		    "transport_type", &prop) == 1)
			set_error("Missing transport_type in clplfiles",
			    device_name);

		trtypes = os::strdup(prop.enumlist);

		if (strcmp(device_name, "sci") == 0)
			check_sci_adapter(adapter, name, ttype);
	} else
		set_error("Missing device name property for adapter", name);

	if ((ttype) && ((trtypes == NULL) || (strstr(trtypes, ttype) == NULL)))
		set_error("Not a supported transport type", ttype);

	if (clconf_obj_get_property((clconf_obj_t *)adapter,
	    "device_instance") == NULL) {
		set_error("Missing device instance property for adapter", name);
	}
	if ((prval = clconf_obj_get_property((clconf_obj_t *)adapter,
	    "netmask")) != NULL) {
		if ((inet_addr(prval) == (in_addr_t)-1) || (ndots(prval) != 3))
			set_error("Not a valid netmask", prval);
	}

	if ((ttype != NULL) && (device_name != NULL)) {

		if ((prval = clconf_obj_get_property((clconf_obj_t *)adapter,
		    "nw_bandwidth")) != NULL)
			check_property(device_name, "nw_bandwidth", prval);
		if ((prval = clconf_obj_get_property((clconf_obj_t *)adapter,
		    "bandwidth")) != NULL)
			check_property(device_name, "bandwidth", prval);

		/*
		 * Check the cluster wide heartbeat timeout and quantum as
		 * well as the per adapter heartbeat timeout and quantums
		 * against the adapter min and max values
		 */
		hbt = new char[strlen(ttype)+strlen("_heartbeat_timeout")+1];
		ASSERT(hbt != NULL);
		os::sprintf(hbt, "%s_heartbeat_timeout", ttype);
		hbq = new char[strlen(ttype)+strlen("_heartbeat_quantum")+1];
		ASSERT(hbq != NULL);
		os::sprintf(hbq, "%s_heartbeat_quantum", ttype);

		/*
		 * Note that the cluster wide properties
		 * (transport_heartbeat_*) need to be checked against the
		 * respective per-adapter properties (ttype_heartbeat_*).
		 * So, we need to pass in hbt and hbq to check_property.
		 */
		if ((prval = clconf_obj_get_property((clconf_obj_t *)clconf,
		    "transport_heartbeat_timeout")) != NULL)
			check_property(device_name, hbt, prval);
		if ((prval = clconf_obj_get_property((clconf_obj_t *)clconf,
		    "transport_heartbeat_quantum")) != NULL)
			check_property(device_name, hbq, prval);

		if ((prval = clconf_obj_get_property((clconf_obj_t *)adapter,
		    hbt)) != NULL)
			check_property(device_name, hbt, prval);
		if ((prval = clconf_obj_get_property((clconf_obj_t *)adapter,
		    hbq)) != NULL)
			check_property(device_name, hbq, prval);

		delete [] hbt;
		delete [] hbq;
	}

	nports = 0;
	for (iter1 = clconf_adapter_get_ports(adapter);
	    (port = (clconf_port_t *)clconf_iter_get_current(iter1)) != NULL;
	    clconf_iter_advance(iter1)) {
		if (clconf_obj_get_name((clconf_obj_t *)port) == NULL)
			set_error("Missing port name in adapter", name);
		nports++;
	}
	clconf_iter_release(iter1);
	if (nports > 1)
	    set_error("More than one port per adapter is not supported", name);

	if (trtypes)
		delete [] trtypes;
	return (1);
}

/*
 * Checks quorum device object passed as the first argument for validity.
 * This routine prints error in the following cases:
 * 	- If the quorum device id is not in the supported range of ids
 *	- If the gdevname or votecount property is missing.
 */
void
check_qd(clconf_quorum_t *qd)
{
	const char *gdevname;
	int qid;

	qid = clconf_obj_get_id((clconf_obj_t *)qd);
	if ((qid < 0) || (qid > MAX_QUORUM_DEVICES))
		set_error("Unsupported quorum device id",
		    clconf_obj_get_idstring((clconf_obj_t *)qd));
	else if ((gdevname = clconf_obj_get_property((clconf_obj_t *)qd,
	    "gdevname")) == NULL)
		set_error("Missing gdevname for quorum device",
		    clconf_obj_get_idstring((clconf_obj_t *)qd));
	else if (clconf_obj_get_property((clconf_obj_t *)qd,
	    "votecount") == NULL)
		set_error("Missing vote count property for the quorum device",
		    gdevname);
}

#ifdef DEBUG
void
print_tables()
{
	uint_t i, j;

	os::printf("Node table :\n");
	for (i = 0; i < nnodes+1; i++) {
		for (j = 0; j < nnodes+1; j++)
			os::printf("%d ", nd_tab[i][j]);
		os::printf("\n");
	}
	os::printf("Node-BB table :\n");
	for (i = 0; i < nnodes+1; i++) {
		for (j = 0; j < nbbs+1; j++)
			os::printf("%d ", ndbb_tab[i][j]);
		os::printf("\n");
	}
}
#endif

/*
 * The routines get_nd_index, get_bb_index, update_tables, and
 * check_connectivity are used for checking the connectivity of the
 * nodes in the cluster by constructing a connectivity matrix.
 * nd_tab is a two dimensional table showing the node-node connection.
 * ndbb_tab is a two dimensional table showing the node-blackbox connection.
 * ndbb_tab is used to update nd_tab to show if a node-blackbox-node
 * connection exists. So, the final connectivity matrix is available in
 * the nd_tab table. A 1 in the table means that the node is either directly
 * or indirectly connected to the other node. The 0th column and 0th row are
 * used for storing the node ids.
 * Higher order 16 bits of the blackbox ids in the row 0 are used to store
 * black box port number.
 */
uint_t
get_nd_index(int ndid)
{
	uint_t i;

	for (i = 1; i < nnodes+1; i++)
		if (nd_tab[i][0] == (uint_t)ndid)
			return (i);
	return (0);
}

uint_t
get_bb_index(int bbid)
{
	uint_t i;

	for (i = 1; i < nbbs+1; i++)
		if ((ndbb_tab[0][i] & 0xffff) == (uint_t)bbid)
			return (i);
	return (0);
}

/*
 * Updates connectivity matrix and returns error if the same
 * switch port is used more than once.
 */
void
update_tables(int nd1, int nd2, int bb1, int bb2, int p1, int p2)
{
	uint_t ind1, ind2 = 0;
	char st[10];
	uint_t p = 0;

	if ((nd1) && (nd2)) {
		ind1 = get_nd_index(nd1);
		ind2 = get_nd_index(nd2);
		nd_tab[ind1][ind2] = 1;
		nd_tab[ind2][ind1] = 1;
	} else {
		if (nd1)
			ind1 = get_nd_index(nd1);
		else
			ind1 = get_nd_index(nd2);
		if (bb1) {
			ind2 = get_bb_index(bb1);
			ndbb_tab[ind1][ind2] = 1;
			p = 1 << (p1 + 15);	/* 16 + p1 -1 */
		}
		if (bb2) {
			ind2 = get_bb_index(bb2);
			ndbb_tab[ind1][ind2] = 1;
			p = 1 << (p2 + 15);	/* 16 + p2 -1 */
		}
		if (p & (ndbb_tab[0][ind2] & (uint_t)0xffff0000)) {
			os::sprintf(st, "%d", ind2);
			set_error("Duplicate port usage in blackbox", st);
		} else
			ndbb_tab[0][ind2] |= p;
	}
}

void
check_connectivity()
{
	uint_t i, j, k;
	char msg[100];


	/*
	 * Update the nd_tab with the indirect connections through the
	 * blackboxes.
	 */
	for (i = 1; i < nnodes+1; i++)
		for (j = 1; j < nbbs+1; j++) {
			if (ndbb_tab[i][j])
				for (k = 1; k < nnodes+1; k++)
					if (ndbb_tab[k][j]) {
						nd_tab[i][k] = 1;
						nd_tab[k][i] = 1;
					}
		}
	/*
	 * Check for connectivity
	 */
	for (i = 1; i < nnodes+1; i++)
		for (j = 1; j < nnodes+1; j++)
			if ((nd_tab[i][j] == 0) && (i != j)) {
			    os::sprintf(msg, "node %d cannot reach node %d"
				" - Check cables", nd_tab[i][0], nd_tab[j][0]);
			    print_warning(msg);
			    return;
			}
}

/*
 * Checks cable object passed as the first argument for validity.
 * This routine returns error in the following cases:
 *	- If the state of the cable is missing
 *	- If the endpoints are invalid or missing
 *	- If the endpoints are not either adapters or blackboxes
 *	- If the endpoint adapters are not compatible
 * If the cable passes all the above tests then the connectivity matrices
 * are updated.
 */
void
check_cable(clconf_cable_t *cable)
{
	clconf_port_t *port1, *port2, *port;
	clconf_obj_t *obj1, *obj2;
	clconf_objtype_t type1, type2;
	const char *state, *ids;
	int err = 0, enabled = 1;
	int nd1, nd2, bb1, bb2;
	int p1 = 0, p2 = 0, p;

	state = clconf_obj_get_state((clconf_obj_t *)cable);
	if (state == NULL) {
		set_error("Missing state for cable",
		    clconf_obj_get_idstring((clconf_obj_t *)cable));
		return;
	}
	if (! valid_state(state))
		return;

	ids = clconf_obj_get_idstring((clconf_obj_t *)cable);

	if ((port1 = clconf_cable_get_endpoint(cable, 1)) == NULL)
		set_error("Missing or invalid endpoint1 for cable", ids);
	else if (!clconf_obj_enabled((clconf_obj_t *)port1))
			enabled = 0;

	if ((port2 = clconf_cable_get_endpoint(cable, 2)) == NULL)
		set_error("Missing or invalid endpoint2 for cable", ids);
	else if (!clconf_obj_enabled((clconf_obj_t *)port2))
			enabled = 0;

	if (port1 && port2 && enabled) {

		obj1 = clconf_obj_get_parent((clconf_obj_t *)port1);
		obj2 = clconf_obj_get_parent((clconf_obj_t *)port2);
		if ((obj1 == NULL) || (obj2 == NULL)) {
			set_error("Invalid endpoint for cable", ids);
			return;
		}
		type1 = clconf_obj_get_objtype(obj1);
		type2 = clconf_obj_get_objtype(obj2);

		if ((type1 != CL_BLACKBOX) && (type1 != CL_ADAPTER)) {
			set_error("Cable endpoint1 is not adapter"
			    "or blackbox", ids);
			err = 1;
		}
		if ((type2 != CL_BLACKBOX) && (type2 != CL_ADAPTER)) {
			set_error("Cable endpoint2 is not adapter"
			    "or blackbox", ids);
			err = 1;
		}
		if ((type1 == CL_ADAPTER) && (type2 == CL_ADAPTER))
		    if (strcmp(clconf_adapter_get_transport_type(
			(clconf_adapter_t *)obj1),
			clconf_adapter_get_transport_type(
			(clconf_adapter_t *)obj2)) != 0) {
			    set_error("Incompatible adapter types for"
				"the cable endpoints", ids);
			err = 1;
		    }

		/*
		 * For connectivity matrix
		 */
		if (!err) {
			nd1 = nd2 = bb1 = bb2 = 0;
			port = port1;
			if (type1 == CL_ADAPTER) {
			    obj1 = clconf_obj_get_parent(obj1);
			    nd1 = clconf_obj_get_id(obj1);
			    if (!clconf_obj_enabled(obj1))
				return;
			    p = nd1 - 1;
			} else {
			    bb1 = clconf_obj_get_id(obj1);
			    p1 = clconf_obj_get_id((clconf_obj_t *)port1);
			    p = atoi(clconf_obj_get_name(
				(clconf_obj_t *)port1));
			}

			if (type2 == CL_ADAPTER) {
			    obj2 = clconf_obj_get_parent(obj2);
			    nd2 = clconf_obj_get_id(obj2);
			    if (!clconf_obj_enabled(obj2))
				return;
			} else {
			    bb2 = clconf_obj_get_id(obj2);
			    p2 = clconf_obj_get_id((clconf_obj_t *)port2);
			    port = port2;
			    p = atoi(clconf_obj_get_name(
				(clconf_obj_t *)port2));
			}
			update_tables(nd1, nd2, bb1, bb2, p1, p2);
			if ((type1 == CL_ADAPTER) && (type2 == CL_ADAPTER))
				check_sci_cc(port2, nd2-1, ids);
			if ((type1 == CL_ADAPTER) || (type2 == CL_ADAPTER))
				check_sci_cc(port, p, ids);
		}
	}
}

/*
 * Checks blackbox object passed as the first argument for validity.
 * This routine returns error in the following cases:
 *	- If the state is missing
 *	- If the blackbox type is not supported.
 *	- If strict option is selected, then if ports are missing or
 *	  if the ports do not have name.
 */
void
check_bb(clconf_bb_t *bb, const char *name)
{
	const char *type, *state;
	clconf_iter_t *iter1;
	clconf_port_t *port;
	int nports = 0;

	state = clconf_obj_get_state((clconf_obj_t *)bb);
	if (state == NULL)
		set_error("Missing state for the blackbox", name);
	else if (! valid_state(state))
			return;

	if ((type = clconf_obj_get_property((clconf_obj_t *)bb, "type"))
	    == NULL)
		set_error("type property missing for blackbox", name);
	else if (strstr(supp_bb_types, type) == NULL)
		set_error("Invalid type for blackbox", type);

	if (! strict)
		return;

	/*
	 * We get to this part only if user has strict option ON
	 * Since blackboxes are not necessary for the two nodes connected
	 * point to point.
	 * Anyway we check for connectivity, that should find the error
	 * the nodes are not connected correctly.
	 */

	if ((iter1 = clconf_bb_get_ports(bb)) == NULL)
		set_error("Missing ports in blackbox", name);

	for (; (port = (clconf_port_t *)clconf_iter_get_current(iter1)) != NULL;
	    clconf_iter_advance(iter1)) {
		if (clconf_obj_get_name((clconf_obj_t *)port) == NULL)
			set_error("Missing port name in blackbox", name);
		nports++;
	}
	clconf_iter_release(iter1);

	if (nports == 0)
		set_error("Missing ports in blackbox", name);
}

/*
 * Checks node object passed as the first argument for validity.
 * This routine returns error in the following cases:
 *	- If the state is missing.
 *	- If the adapter name is missing or duplicate
 *	- If quorum_vote or quorum_resv_key property is missing
 *	- If private_hostname property is missing
 */
uint_t
check_node(clconf_cluster_t *clconf, clconf_node_t *node,
		const char *name, uint_t numnodes)
{
	clconf_iter_t *iter1;
	clconf_adapter_t *adapter;
	char **adpnames;
	const char *adap_name, *state, *qv;
	int n = 0;
	uint_t i, nadps;

	state = clconf_obj_get_state((clconf_obj_t *)node);
	if (state == NULL)
		set_error("Missing state for node", name);
	else if (! valid_state(state))
			return (0);

	iter1 = clconf_node_get_adapters(node);
	nadps = (uint_t)clconf_iter_get_count(iter1);
	adpnames = new char *[nadps];
	for (i = 0; i < nadps; i++)
		adpnames[i] = NULL;

	for (; (adapter = (clconf_adapter_t *)
	    clconf_iter_get_current(iter1)) != NULL;
	    clconf_iter_advance(iter1)) {
		adap_name = clconf_obj_get_name((clconf_obj_t *)adapter);
		n += check_adapter(clconf, adapter, adap_name);
		if (adap_name == NULL)
			set_error("Missing adapter name",
			    clconf_obj_get_idstring((clconf_obj_t *)adapter));
		else if (duplicate_entry(adpnames, adap_name))
			set_error("Duplicate Adapter name", adap_name);
	}
	clconf_iter_release(iter1);

	/*
	 * msg should be large enough to accomodate a host name, which
	 * can be upto MAXHOSTNAMELEN, as well as the warning message
	 * below.
	 */
	char msg[MAXHOSTNAMELEN + 100];
	if ((n == 0) && (numnodes != 1)) {
		os::sprintf(msg, "No enabled adapters in the node - %s", name);
		print_warning(msg);
	}

	if ((qv = clconf_obj_get_property((clconf_obj_t *)node, "quorum_vote"))
	    == NULL)
		set_error("Missing quorum_vote for node", name);

	if (clconf_obj_get_property((clconf_obj_t *)node,
	    "quorum_resv_key") == NULL)
		set_error("Missing quorum_resv_key for node", name);

	if ((qv) && (os::atoi(qv) != 0))
		quorum = 1;

	if (clconf_obj_get_property((clconf_obj_t *)node,
	    "private_hostname") == NULL)
		set_error("Missing private hostname for the node",
		    clconf_obj_get_idstring((clconf_obj_t *)node));

	return (1);
}

/*
 * Creates clpl tree by reading the clpl generic file and then the adapter
 * specific clpl files.
 */
int
create_clpltree(const char *clplfn)
{
	clconf_propdesc_t	pr;
	char *values, *token, filename[50];
	int rc;
	uint_t pos = 0, len;

	read_clplfile(clplfn);
	rc = get_prop_from_clpltree(CL_ADAPTER, "generic", "device_name", &pr);
	if (rc == 1) {
		set_error("Missing device_name supported values in", clplfn);
		return (rc);
	} else
		supp_adap_devnames = os::strdup(pr.enumlist);

	values = os::strdup(pr.enumlist);
	len = strlen(values);
	while ((pos < len) && ((token = strtok(&values[pos], ":")) != NULL)) {
		os::sprintf(filename, "SUNW.adapter.%s.clpl", token);
		read_clplfile(filename);
		pos += (strlen(token)+1);
	}
	delete [] values;

	read_clplfile("SUNW.adapter.default.clpl");

	rc = get_prop_from_clpltree(CL_BLACKBOX, "generic", "type", &pr);
	if (rc == 1) {
		set_error("Missing blackbox type supported values in", clplfn);
		return (rc);
	} else
		supp_bb_types = os::strdup(pr.enumlist);
	values = os::strdup(pr.enumlist);
	pos = 0;
	len = strlen(values);
	while ((pos < len) && ((token = strtok(&values[pos], ":")) != NULL)) {
		os::sprintf(filename, "SUNW.blackboxtype.%s.clpl", token);
		read_clplfile(filename);
		pos += (strlen(token)+1);
	}
	delete [] values;
	return (0);
}

/*
 * gets the total number of adapters in the cluster by iterating
 * through the nodes and adapters list.
 */
int
get_adapter_count(clconf_cluster_t *clconf)
{
	clconf_iter_t *iter1, *iter2;
	clconf_node_t *node;
	int count = 0;

	iter1 = clconf_cluster_get_nodes(clconf);
	for (; (node = (clconf_node_t *)
	    clconf_iter_get_current(iter1)) != NULL;
	    clconf_iter_advance(iter1)) {
		iter2 = clconf_node_get_adapters(node);
		count += clconf_iter_get_count(iter2);
	}
	return (count);
}

/*
 * Checks the entire cluster for the validity.
 * This routine prints error in the following cases:
 *	- If clpl files are missing
 *	- If there are no nodes in the cluster
 *	- If node names are missing or duplicate.
 *	- If there is no quorum among the nodes.
 *	- If strict option is specified then duplicate blackboxes and missing
 *	  blackbox names
 */
void
check_cluster(clconf_cluster_t *clconf)
{
	clconf_iter_t  *iter1;
	clconf_node_t *node;
	clconf_bb_t *bb;
	clconf_cable_t *cable;
	clconf_quorum_t *qrm;
	int id;
	uint_t i, j, tnodes, tbbs, tadps;
	char **nodenames, **bbnames;
	const char *name, *ids;


	/*
	 * Reads all the necessary clpl files
	 */
	if (create_clpltree("SUNW.generic.clpl") != 0) {
		return;
	}

	if (((iter1 = clconf_cluster_get_nodes(clconf)) == NULL)) {
		set_error("No nodes in the cluster", "");
		return;
	}

	tnodes = (uint_t)clconf_iter_get_count(iter1);
	nodenames = new char *[tnodes];
	for (i = 0; i < tnodes; i++)
		nodenames[i] = NULL;

	tadps = (uint_t)get_adapter_count(clconf);
	ip_addrs = new char *[tadps];
	adapter_ids = new char *[tadps];
	for (i = 0; i < tadps; i++) {
		ip_addrs[i] = NULL;
		adapter_ids[i] = NULL;
	}

	for (; (node = (clconf_node_t *)
	    clconf_iter_get_current(iter1)) != NULL;
	    clconf_iter_advance(iter1)) {
		name = clconf_obj_get_name((clconf_obj_t *)node);
		ids = clconf_obj_get_idstring((clconf_obj_t *)node);
		if (name == NULL)
			set_error("Missing name for the node", ids);
		else if (duplicate_entry(nodenames, name))
			set_error("Duplicate node name", name);
		nnodes += check_node(clconf, node, name, tnodes);
	}
	clconf_iter_release(iter1);
	if (nnodes == 0)
		set_error("No enabled nodes in the cluster", "");
	if (quorum == 0)
		set_error("Missing quorum among the nodes",
		    "Check quorum_votes");

	iter1 = clconf_cluster_get_bbs(clconf);
	tbbs = (uint_t)clconf_iter_get_count(iter1);
	bbnames = new char *[tbbs];
	for (i = 0; i < tbbs; i++)
		bbnames[i] = NULL;

	for (; (bb = (clconf_bb_t *)
	    clconf_iter_get_current(iter1)) != NULL;
	    clconf_iter_advance(iter1)) {
		name = clconf_obj_get_name((clconf_obj_t *)bb);
		nbbs++;
		check_bb(bb, name);
		if (strict) {
			if (name == NULL)
			    set_error("Missing name for the blackbox",
				clconf_obj_get_idstring((clconf_obj_t *)bb));
			else if (duplicate_entry(bbnames, name))
				set_error("Duplicate blackbox name", name);
		}
	}
	clconf_iter_release(iter1);

	ndbb_tab = new uint_t *[nnodes+1];
	nd_tab = new uint_t *[nnodes+1];
	for (i = 0; i < nnodes+1; i++) {
		ndbb_tab[i] = new uint_t[nbbs+1];
		nd_tab[i] = new uint_t[nnodes+1];
	}
	iter1 = clconf_cluster_get_nodes(clconf);
	for (i = 1, j = 1; (node = (clconf_node_t *)
	    clconf_iter_get_current(iter1)) != NULL;
	    clconf_iter_advance(iter1)) {
		if (clconf_obj_enabled((clconf_obj_t *)node)) {
			id = clconf_obj_get_id((clconf_obj_t *)node);
			ndbb_tab[i++][0] = (uint_t)id;
			nd_tab[j][0] = (uint_t)id;
			nd_tab[0][j++] = (uint_t)id;
		}
	}
	clconf_iter_release(iter1);

	iter1 = clconf_cluster_get_bbs(clconf);
	for (i = 1; (bb = (clconf_bb_t *)
	    clconf_iter_get_current(iter1)) != NULL;
	    clconf_iter_advance(iter1)) {
		id = clconf_obj_get_id((clconf_obj_t *)bb);
		ndbb_tab[0][i++] = (uint_t)id;
	}
	clconf_iter_release(iter1);

	iter1 = clconf_cluster_get_cables(clconf);
	for (; (cable = (clconf_cable_t *)
	    clconf_iter_get_current(iter1)) != NULL;
	    clconf_iter_advance(iter1)) {
		check_cable(cable);
	}
	clconf_iter_release(iter1);

	iter1 = clconf_cluster_get_quorum_devices(clconf);
	for (; (qrm = (clconf_quorum_t *)
	    clconf_iter_get_current(iter1)) != NULL;
	    clconf_iter_advance(iter1)) {
		check_qd(qrm);
	}
	clconf_iter_release(iter1);

	check_connectivity();
}

void
usage(const char *bin_name)
{
	os::printf("Usage:\n");
	os::printf("[ Validate infrastructure file ]\n");
	os::printf("\t%s [-i <infrastructure file name>] [-s]\n\n", bin_name);
}

main(int argc, char **argv)
{
	clconf_cluster_t *clconf;
	clconf_file_io *inf;
	char *filename = NULL;
	int arg;

	if (sc_zonescheck() != 0)
		return (1);
	optind = 1;
	while ((arg = getopt(argc, argv, "Fsi:p:?")) != EOF) {
		switch (arg) {
			case 'i' :
				filename = os::strdup(optarg);
				break;
			/*
			 * 'p' option is Undocumented for now
			 * 'i' and 'p' options are useful for testing on a
			 * system without cluster software installed.
			 */
			case 'p' :
				clpl_prefix = os::strdup(optarg);
				break;
			case 's' :
				strict = 1;
				break;
			case 'F' :
				force = 1;
				break;
			case '?' :
				usage(argv[0]);
				return (1);
			default:
				os::printf("Unknown option: -%c\n", arg);
				usage(argv[0]);
				return (1);
		}
	}

	if (filename == NULL)
		filename = os::strdup("/etc/cluster/ccr/global/infrastructure");
	if (clpl_prefix == NULL)
		clpl_prefix = os::strdup("/etc/cluster/clpl/");

	// Check if the instrastructure file exists in the ccr
	if (access(filename, F_OK) != 0) {
		os::printf("Error: %s does not exist\n", filename);
		exit(1);
	}
	inf = new clconf_file_io(filename);
	if (! inf->initialize()) {
		os::printf("Unable to initialize clconf\n");
		exit(1);
	}

	clconf = clconf_cluster_get_current();
	if (clconf == NULL) {
		os::printf("Could not get the clconf\n");
		return (2);
	}

	check_cluster(clconf);
	return (rcode);
}
