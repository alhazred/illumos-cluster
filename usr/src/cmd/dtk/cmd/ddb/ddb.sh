#!/usr/bin/sh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident	"@(#)ddb.sh	1.5	08/05/20 SMI"

if [ $# -ne 2 ]; then
	echo usage: `basename $0`: kernel-symbols kernel-core 1>&2
	exit 1
fi

# We run adb twice because to only run it once we'd have to
# figure out the macro path adb is going to use, and that
# depends on the platform where the core comes from, and add
# the dtk macro path to it.  So we let adb do its thing for
# threadlist and then we use our macro path for our dump_all
# macro.

echo '$G;$<threadlist' | adb -k $1 $2
echo '$<dump_all' | adb -I /usr/cluster/dtk/lib/adb/K -k $1 $2
