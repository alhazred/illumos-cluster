.\"
.\" CDDL HEADER START
.\"
.\" The contents of this file are subject to the terms of the
.\" Common Development and Distribution License (the License).
.\" You may not use this file except in compliance with the License.
.\"
.\" You can obtain a copy of the license at usr/src/CDDL.txt
.\" or http://www.opensolaris.org/os/licensing.
.\" See the License for the specific language governing permissions
.\" and limitations under the License.
.\"
.\" When distributing Covered Code, include this CDDL HEADER in each
.\" file and include the License file at usr/src/CDDL.txt.
.\" If applicable, add the following below this CDDL HEADER, with the
.\" fields enclosed by brackets [] replaced with your own identifying
.\" information: Portions Copyright [yyyy] [name of copyright owner]
.\"
.\" CDDL HEADER END
.\"
.\" @(#)scdtk_enable.5 1.2 08/05/20 SMI
.\" Copyright (c) 2000 by Sun Microsystems, Inc.
.\" All rights reserved.
.TH scdtk_enable 5 "08/05/20" "SunCluster 3.0" "Miscellaneous Information"
.ds ]D Miscellaneous Information
.\"
.na
.SH NAME
.\"
scdtk_enable - information for enabling diagnostic information collection
.\"
.de ES
.LP
.RS 8
.nf
..
.de EE
.fi
.RE
.LP
..
.SH DESCRIPTION
.\"
This man page provides information for enabling the collection
of diagnostic information.  Diagnosis can be made easier in
several ways including:
.IP \(bu
logging of events and state transitions
.IP \(bu
saving of state information on failure
.IP \(bu
execution under control of debugging facilities
.LP
Following are sections describing various ways to enable
diagnostic information.
.LP
After enabling the collection of diagnostic information,
the information can be collected using the methods described in
.BR scdtk_collect (5).
.\"
.SH "Enabling syslog Messages for Data Services"
.\"
Various levels of debugging output are available for data
services through syslog.  To enable this, entries for local7.debug
need to be added to /etc/syslog.conf:
.ES
local7.debug		/dev/console
local7.debug		/var/adm/messages
.EE
The columns must be separated by tab(s).  The file can be edited
or appended with the commands:
.ES
# echo local7.debug\\\\t/dev/console >> /etc/syslog.conf
# echo local7.debug\\\\t/var/adm/messages >> /etc/syslog.conf
.EE
Restart syslogd with:
.ES
# pkill \-HUP syslogd
.EE
Test the addition to /etc/syslog.conf with:
.ES
# logger \-p local7.debug test
.EE
You should see output with "test" on the console and at the end of
/var/adm/messages.
.LP
The file that controls the debug level is called "loglevel" and is
stored in a directory specific to a resource type. We currently
use the Vendor ID and resource type name in the path for the debug file:
.ES
/var/cluster/rgm/rt/\fIrtname\fP/loglevel
.EE
To turn on debugging for a resource type, you would do the following:
.ES
# mkdir \-p /var/cluster/rgm/rt/\fIrtname\fP
# echo \fIdebuglevel\fP > /var/cluster/rgm/rt/\fIrtname\fP/loglevel
.EE
For example:
.ES
# mkdir \-p /var/cluster/rgm/rt/SUNW.test_svc
# echo 9 > /var/cluster/rgm/rt/SUNW.test_svc/loglevel
.EE
Debug levels:
.IP 0
is for no debug messages \- error and information messages only
.IP 1
is for the fewest debug messages, as well as all error and information messages
.IP 2-8
is for increasingly more debug messages
.IP 9
is for the most debug messages
.LP
Notes:
.LP
The debug level is read at the start of each method invocation. Changing the
debug level will take effect on the next call of a given method.
.\"
.SH "Enabling Resource Group Manager syslog Debug Messages"
.\"
If you see unexpected messages prefixed with "Cluster.RGM" it may
be helpful to enable additional Resource Group Manager (\s-1RGM\s0)
syslog messages.
If you have a reproducible problem it would make sense to
reboot and enable debug output to gather more information.
If you see some strange behavior while the system is running
you could enable debug output to try to diagnose the problem.
.LP
To enable RGM syslog debug messages run:
.ES
# /usr/cluster/lib/sc/rgmd_debug on
.EE
This will cause \s-1RGM\s0 debugging messages to be written to
the console and /var/adm/messages.
.LP
To disable RGM syslog debug messages run:
.ES
# /usr/cluster/lib/sc/rgmd_debug off
.EE
.\"
.SH "Component Debug Buffers"
.\"
Some of the clustering components use debug buffers to log
diagnostic information.  To enable all the debug buffers
on a running system use adb:
.ES
# adb -wk /dev/ksyms /dev/mem
haci_dbg_option/WFFFFFFFF
orb_trace_options/WFFFFFFFF
clnet_net_dbg_fwd/W1
$q
.EE
These settings will not persist across reboot, but that
can be done by adding these lines to /etc/system:
.ES
set cl_comm:haci_dbg_option = 0xffffffff
set cl_comm:orb_trace_options = 0xffffffff
set cl_net:clnet_net_dbg_fwd = 1
.EE
Specific bits in haci_dbg_option can be set using the OR form of set.  These
are cumulative so:
.ES
set cl_comm:haci_dbg_option | 0x40
set cl_comm:haci_dbg_option | 0x80
.EE
will set both bits.  orb_trace_options has bits that can be set similarly.
.SH "Operating System Crash Dumps"
.\"
When the operating system encounters a fatal error it will, by
default, produce a crash dump.  At times it is necessary to
force a crash dump.  The
.BR dumpadm (1M)
command tells you
the current dump settings, for example:
.ES
# dumpadm
      Dump content: kernel pages
       Dump device: /dev/dsk/c0t3d0s1 (swap)
Savecore directory: /var/crash/zoon
  Savecore enabled: yes
.EE
By default the dump device is the swap partition.
.BR savecore (1M),
which runs during boot to extract the crash data from the dump
device, stores the data in /var/crash/\`uname\ \-n\`.  You can
use dumpadm to specify a different dump device or savecore
directory.
.LP
Under certain circumstances it is beneficial to get a "live"
crash dump of the system.  This dumps state while the system
keeps running.  To be able to get a live crash dump it is
necessary to configure a dedicated dump device with dumpadm.
.LP
For maximum debugability we recommend:
.IP \(bu
if possible, configure a dedicated dump device that is as large as physical
memory
.IP \(bu
if the dump device is sufficiently large, set dump content to "all"
.LP
In practice, dump compression ratios are typically greater than 2.0 so it's
usually safe to have a dump device that is half the size of physical
memory.
.LP
As an example, if /dev/dsk/c7t3d0s2 is unused and available
as a dedicated dump device and we want to save all pages and store
it in /space/crash we would run dumpadm with these options and get
this output:
.ES
# dumpadm \-c all \-d /dev/dsk/c7t3d0s2 \-s /space/crash
      Dump content: all pages
       Dump device: /dev/dsk/c7t3d0s2 (dedicated)
Savecore directory: /space/crash
  Savecore enabled: yes
.EE
.\"
.SH "Process Core Files"
.\"
When a process terminates abnormally it may produce a core file
that can be helpful in determining what went wrong.  By default,
a file called "core" is created in the process's current directory.
To prevent core files from being overwritten by subsequent core
dumps and to make it easier to identify where core files came from,
.BR coreadm (1M)
is used to change the default.  We recommend:
.IP \(bu
enable global core file generation
.IP \(bu
enable global set-ID cores
.IP \(bu
use "%f.%p.%t" in the core file name pattern
.LP
Choose an appropriate directory for core dumps and make sure
the directory exists.  As an example, if we want core dumps
to go in /var/core we run these commands:
.ES
# mkdir /var/core
# coreadm \-e global \-e global-setid
# coreadm \-g \'/var/core/core.%f.%p.%t\'
# coreadm
     global core file pattern: /var/core/core.%f.%p.%t
       init core file pattern: core
            global core dumps: enabled
       per-process core dumps: enabled
      global setid core dumps: enabled
 per-process setid core dumps: disabled
     global core dump logging: disabled
.EE
To test the settings, abort a command with ctrl\-\\ (control backslash),
such as:
.ES
# sleep 13
^\\Quit \- core dumped
# ls \-l /var/core
total 134
-rw-------   1 root     other      67640 Feb 10 15:37 core.sleep.350.950225870
.EE
This will produce two core files, one named core in the current
directory and the other shown above.  If you only want core files to
be generated in the directory specified with -g, disable per-process
core files with:
.ES
# coreadm -d process
.EE
.\"
.SH "Booting With kadb"
.\"
During normal cluster operation it is not necessary to run kadb, the
kernel debugger.  However there may be times when it is necessary to
boot the kernel under the control of kadb in order to obtain diagnostic
information.
.LP
To configure a machine to boot kadb automatically, use the eeprom
command to set the boot file to kadb:
.ES
# eeprom boot-file=kadb
.EE
With this setting of boot-file, kadb will boot the appropriate kernel
for your system, either 32-bit or 64-bit.
.LP
To configure a machine to boot automatically a 32-bit kernel under kadb:
.ES
# eeprom boot-file="kadb \-D kernel/unix"
.EE
.LP
Setting boot-file establishes a default file (and optionally arguments)
that will be used when no arguments are given to the boot command at the
\s-1PROM\s0's ok prompt.  To unset boot-file:
.ES
# eeprom boot-file=""
.EE
This will restore the machine to its default behavior, which does
not run the kernel under control of kadb.
.LP
To boot one instance of the kernel under kadb, arguments are
given to the boot command.  For example to boot the default
kernel with kadb:
.ES
ok boot kadb
.EE
To boot the 32-bit kernel:
.ES
ok boot kadb \-D kernel/unix
.EE
The arguments to boot temporarily override the boot-file setting.
.SH SEE ALSO
.BR coreadm (1M),
.BR dumpadm (1M),
.BR eeprom (1M),
.BR kadb (1M),
.BR rgmd_debug (1M),
.BR savecore (1M),
.BR scdtk_collect (1M),
.BR scshutdown (1M),
.BR syslog.conf (4)
.SH SEA ALSO
.BR ocean (1)
