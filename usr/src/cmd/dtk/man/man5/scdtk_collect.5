.\"
.\" CDDL HEADER START
.\"
.\" The contents of this file are subject to the terms of the
.\" Common Development and Distribution License (the License).
.\" You may not use this file except in compliance with the License.
.\"
.\" You can obtain a copy of the license at usr/src/CDDL.txt
.\" or http://www.opensolaris.org/os/licensing.
.\" See the License for the specific language governing permissions
.\" and limitations under the License.
.\"
.\" When distributing Covered Code, include this CDDL HEADER in each
.\" file and include the License file at usr/src/CDDL.txt.
.\" If applicable, add the following below this CDDL HEADER, with the
.\" fields enclosed by brackets [] replaced with your own identifying
.\" information: Portions Copyright [yyyy] [name of copyright owner]
.\"
.\" CDDL HEADER END
.\"
.\" @(#)scdtk_collect.5 1.2 08/05/20 SMI
.\" Copyright (c) 2000 by Sun Microsystems, Inc.
.\" All rights reserved.
.TH scdtk_collect 5 "08/05/20" "SunCluster 3.0" "Miscellaneous Information"
.ds ]D Miscellaneous Information
.\"
.na
.SH NAME
.\"
scdtk_collect - information for diagnostic information collection
.\"
.de ES
.LP
.RS 8
.nf
..
.de EE
.fi
.RE
.LP
..
.SH DESCRIPTION
.\"
This man page provides information for the collection
of diagnostic information.
Before certain information can be collected the collection
needs to be enabled by the methods described in
.BR scdtk_enable (5).
.LP
Following are sections describing various information that
can be collected and how to collect it.
.\"
.SH "Collecting syslog Messages for Data Services"
.\"
If you have enabled syslog messages for data services (as described in
.BR scdtk_enable (5))
the messages will appear on the console and in /var/adm/messages.
Any method to collect /var/adm/messages, such as using Explorer,
will suffice for collecting the messages.
.\"
.SH "Collecting Resource Group Manager Debug Messages"
.\"
To display Resource Group Manager diagnostic information, run:
.ES
# /usr/cluster/lib/sc/rgmd_debug printbuf
.EE
This will dump the debugging buffers.  The debugging buffers
are always used regardless of whether syslog debugging has
been turned on with rgmd_debug.
.LP
See the gcore section below for more info about rgmd.
.\"
.SH "Collecting Component Debug Buffers"
.\"
To dump the contents of debugging buffers used by kernel-level
components of SC3.0, the ddb script is run with arguments
for a symbol table and memory image.  To run this on the currently
running system:
.ES
# /usr/cluster/dtk/bin/ddb /dev/ksyms /dev/mem
.EE
If you have a crash dump you can:
.ES
# /usr/cluster/dtk/bin/ddb unix.0 vmcore.0
.EE
ddb prints a threadlist for the kernel and then dumps all the
debugging buffers, so it can produce a lot of output.
.LP
If you would like to dump a single debugging buffer it can
be done with the dump_one adb macro.  For example, let's say
you want to dump the mount_dbg buffer on the running system:
.ES
# adb -I /usr/cluster/dtk/lib/adb -k /dev/ksyms /dev/mem
mount_dbg$<dump_one
$q
.EE
As with ddb, the unix and vmcore files from a crash dump can
be substituted for /dev/ksyms and /dev/mem.
.\"
.SH "Generating Operating System Crash Dumps"
.\"
If the kernel encounters a fatal error it will "panic" and produce a
crash dump.  In most situations the dump will happen automatically.  If
the kernel is booted under the control of kadb, however, the debugger
will get control and you tell the debugger to continue with ":c" to let
the dump happen.  For example:
.ES
panic[cpu0]/thread=f73c2b20: BAD TRAP: ...
...
panic: entering debugger (continue to save dump)
stopped at      0xfbd01028:     ta      0x7d
kadb[0]: :c
syncing file systems... 1 done
dumping to /dev/dsk/c0t3d0s1, offset 26935296
...
.EE
.LP
If the system has become unresponsive and you want to get a crash
dump you should first get to the \s-1PROM\s0's ok prompt.  Typing
L1-A or STOP-A at a Sun keyboard or sending a \s-1BREAK\s0 with
telnet or tip when using a serial connection will interrupt the
kernel.  From the ok prompt run sync:
.ES
ok sync
.EE
If the kernel was booted with kadb you need to exit kadb first
before running sync:
.ES
kadb[0]: $q
ok sync
.EE
(Alternatively you can use $<systemdump at the kadb prompt, which will
exit kadb and remind you to run sync to get a crash dump.)
.LP
After the kernel's data is dumped to the dump device the system
will reboot and during boot, savecore will copy the data from
the dump device into the directory specified with dumpadm.
.LP
If one node has crashed and you would like to get crash dumps
from the other nodes without bringing them down you can take
a "live" crash dump with savecore -L.  This requires a
dedicated dump device.  The kernel's data will be written to
the dump device and immediately read and put into the savecore
directory.  For example:
.ES
# savecore -L
dumping to /dev/dsk/c0t3d0s4, offset 65536
100% done: 10217 pages dumped, compression ratio 2.51, dump succeeded
System dump time: Wed Mar 22 16:15:49 2000
Constructing namelist /export/home/crash/unix.0
Constructing corefile /export/home/crash/vmcore.0
100% done: 10217 of 10217 pages saved
.EE
Because savecore -L dumps the kernel state while the kernel is running,
it will almost certainly contain inconsistent state.  If you collect
live crash dumps tell the person examining the dumps that they are
live crash dumps.
.\"
.SH "Generating Process Core Files"
.\"
If a user-level process appears to be stuck or is misbehaving, a core
file of the process state can be generated with the gcore utility.
By default gcore will create a file in the current directory named
"core.pid", where pid is the process-id of the target process.  gcore
does not use the core directory specified with coreadm so you may want
to use -o to specify where gcore will put the core file.  For example
to get a core of the RGM daemon:
.ES
# ps -ef | grep rgmd
    root 8159      1  0 13:55:51 ?        0:04 /usr/cluster/lib/sc/rgmd
# gcore -o /var/core/core.rgmd 8159
gcore: /var/core/core.rgmd.8159 dumped
.EE
Getting a process's core file with gcore does not disturb the process.
.LP
If you have a core file from rgmd you can use the dump_one macro
to dump the rgmd's debugging buffer:
.ES
# adb -I /usr/cluster/dtk/lib/adb /usr/cluster/lib/sc/rgmd core.rgmd.8159
rgm_dbg_buf$<dump_one
.EE
.\"
.SH "Other User-Level Tools"
.\"
Data from misbehaving processes can also be collected using
truss(1) and the proc(1) tools such as pstack and pfiles.
Some of the proc tools can also be used on core files.
.\"
.SH SEE ALSO
.BR adb (1),
.BR ddb (1M),
.BR dump_one (5),
.BR rgmd_debug (1M),
.BR scdtk_enable (5)
