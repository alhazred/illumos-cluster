'\" t
.\"
.\" CDDL HEADER START
.\"
.\" The contents of this file are subject to the terms of the
.\" Common Development and Distribution License (the License).
.\" You may not use this file except in compliance with the License.
.\"
.\" You can obtain a copy of the license at usr/src/CDDL.txt
.\" or http://www.opensolaris.org/os/licensing.
.\" See the License for the specific language governing permissions
.\" and limitations under the License.
.\"
.\" When distributing Covered Code, include this CDDL HEADER in each
.\" file and include the License file at usr/src/CDDL.txt.
.\" If applicable, add the following below this CDDL HEADER, with the
.\" fields enclosed by brackets [] replaced with your own identifying
.\" information: Portions Copyright [yyyy] [name of copyright owner]
.\"
.\" CDDL HEADER END
.\"
.\" @(#)ccradm.1m 1.3 08/05/20 SMI
.\" Copyright (c) 2000 by Sun Microsystems, Inc.
.\" All rights reserved.
.if n .tr \--
.TH ccradm 1M "08/05/20" "Sun Cluster 3.0"
.SH NAME
ccradm \- CCR table files administration command
.br
.SH SYNOPSIS
.B /usr/cluster/lib/sc/ccradm \-i \fIccr_table_file\fP
[
.B \-o
]
.br
.B /usr/cluster/lib/sc/ccradm \-r \fIccr_table_file\fP \-f \fIinput_file\fP
.SH DESCRIPTION
.B ccradm
is an emergency command line interface
designed only to be used when a
Cluster Configuration Repository
(CCR) table is corrupted.
CCR tables are represented in the file system by ASCII text files,
in the directory \f3/etc/cluster/ccr\f1.
.B ccradm
is used to make the data and checksum in the CCR table files consistent.
Each file starts with a generation number,
\fIccr_gennum\fP,
and a checksum,
\fIccr_checksum\fP.
\fIccr_gennum\fP
indicates the current generation number of the CCR table file,
and is used to keep track of the most current version
of the CCR table file among the different nodes in the cluster.
\fIccr_checksum\fP
indicates the checksum of the CCR table contents,
and provides a consistency check of the data in the table.
.LP
\fIccr_table_file\fP is the name of the file representing the CCR table
on the local node.
.LP
If a CCR table file is manually edited as part of some emergency repair
procedure,
then the checksum must be re-computed to make the data consistent.
.SH OPTIONS
.TP 10
.B \-i
This option can be used only in non-cluster mode.
It re-computes the checksum and updates
\fIccr_table_file\fP with the new checksum.
.sp
When used without the
.B \-o
option,
it sets the generation number to
.SM INIT_VERSION.
A generation number of
.SM INIT_VERSION
means that the CCR table file is valid only
while recovery is being performed
as part of the node boot process in cluster mode.
A prerequisite for successful recovery is one of the following:
either 1) one of the other nodes in the cluster must have the
override version set for the CCR table file,
or 2) at least one of the other nodes must have a valid copy of the
CCR table file.
A CCR table file is valid if it has a valid checksum
and its generation number is greater than or equal to zero.
.sp
If \fIccr_table_file\fP has a generation number of
.SM INIT_VERSION
on all nodes,
then the CCR table will remain invalid after recovery has completed.
Therefore, do not use the
.B \-i
option without the
.B \-o
option on a CCR table file on all nodes in the cluster.
.sp
The CCR table file is ignored when the node joins a cluster
where this CCR table has already been recovered.
.TP
.B \-o
The override option is used with the
.B \-i
option.
This option can be used only in non-cluster mode.
It sets the generation number to
.SM OVRD_VERSION.
This option is used to designate one CCR table file to be the master copy.
This version of the CCR table file will override other versions
of the file that are on the remaining nodes during recovery.
If a CCR table file has a generation number of
.SM OVRD_VERSION
on more than one node,
then only one of the files will be selected
and a warning message will be printed on the console of one of the nodes.
After recovery the table's generation number will be reset to 0.
.TP
\fB\-r\fP and \fB\-f\fP
These options can be used only in cluster mode.
Do not use these options unless explicitly directed
by a Sun Cluster development engineer.
These options must always be used together.
Contents of the file specified with the
.B \-f
option will replace the contents of the table file specified with the
.B \-r
option. The -r \fIccr_table_file\fP refers to the existing invalid table
in the CCR and the -f \fIinput_file\fP refers to the one it should be
replaced with. The existing CCR table file to be restored with the -r option
must exist under the directory /etc/cluster/ccr.
The checksum will be recomputed and the generation number will be reset to 0.
If a table is invalid even after recovery
then it can be restored by using these options.
.SH USAGE
.B ccradm
should be used only for emergency CCR table file repair.
.br
.B \-i
and
.B \-o
options can be used only in non-cluster mode.
.br
.B \-r
and
.B \-f
options can be used only in cluster mode.
.SH "REPAIR PROCEDURE"
Perform these steps to repair a corrupted RGM CCR table
only when directed as part of an emergency repair procedure.
.IP
Reboot all nodes in non-cluster mode.
.IP
Edit the file on all nodes to contain the correct data.
The file must be identical on all nodes.
.IP
Because the file is identical on all nodes,
it also can be designated as the override version on all nodes.
Recompute the checksum and designate this CCR table file to be the override
version by running the following command on all nodes.
\fIfile\fP is the name of the CCR table.
.LP
.RS
.ft 3
.in +0.4i
.nf
/usr/cluster/lib/sc/ccradm \-i /etc/cluster/ccr/\fIfile\fP \-o
.fi
.in -0.4i
.ft 1
.RE
.IP
Reboot all nodes in cluster mode.
.SH EXAMPLES
In non-cluster mode,
recompute the checksum and set the generation number to
.SM INIT_VERSION
for CCR table file \fIwww\fP on the local node:
.LP
.RS
.ft 3
.nf
# ccradm \-i /etc/cluster/ccr/www
.fi
.ft 1
.RE
.LP
In non-cluster mode,
recompute the checksum and set the generation number to
.SM OVRD_VERSION
for CCR table file \fIxxx\fP on the local node:
.LP
.RS
.ft 3
.nf
# ccradm \-i /etc/cluster/ccr/xxx \-o
.fi 
.ft 1 
.RE 
.LP
In cluster mode,
replace the CCR table \fIyyy\fP with the contents of its
backup version in the file
\f3/etc/cluster/ccr/yyy.bak\f1:
.LP 
.RS
.ft 3 
.nf
# ccradm \-r /etc/cluster/ccr/yyy \-f /etc/cluster/ccr/yyy.bak
.fi 
.ft 1  
.RE
.LP
Here is the generic procedure for fixing corrupted CCR tables.
In this example the table name is
.BR directory :
.IP
Reboot all nodes in non-cluster mode.
.IP
Edit the file
\f3/etc/cluster/ccr/directory\f1
on all nodes to contain the correct data.
The file must be identical on all nodes.
.IP
On all nodes,
recompute the checksum and designate this CCR table file to be the override
version.
.RS
.ft 3
.nf
phys-schost-1# /usr/cluster/lib/sc/ccradm -i /etc/cluster/ccr/directory -o
phys-schost-2# /usr/cluster/lib/sc/ccradm -i /etc/cluster/ccr/directory -o
phys-schost-3# /usr/cluster/lib/sc/ccradm -i /etc/cluster/ccr/directory -o
phys-schost-4# /usr/cluster/lib/sc/ccradm -i /etc/cluster/ccr/directory -o
.fi
.ft 1
.RE
.IP
Reboot all nodes in cluster mode.
.SH "EXIT STATUS"
The following exit values are returned:
.TP 10
0
No errors occurred. Successfully updated or restored the CCR table file
.TP
>0
Error occurred.
.SH WARNING
Do not run
.B ccradm \-i
without the
.B \-o
option on a CCR table file on all nodes.
.SH SEE ALSO
.BR chkinfr (1M)
.SH ATTRIBUTES
See
.BR attributes (5)
for descriptions of the following attributes:
.sp
.TS
box;
cbp-1 | cbp-1
l | l .
ATTRIBUTE TYPE	ATTRIBUTE VALUE
=
Availability	SUNWscu
_
Interface Stability	Evolving
.TE
