'\" t
.\"
.\" CDDL HEADER START
.\"
.\" The contents of this file are subject to the terms of the
.\" Common Development and Distribution License (the License).
.\" You may not use this file except in compliance with the License.
.\"
.\" You can obtain a copy of the license at usr/src/CDDL.txt
.\" or http://www.opensolaris.org/os/licensing.
.\" See the License for the specific language governing permissions
.\" and limitations under the License.
.\"
.\" When distributing Covered Code, include this CDDL HEADER in each
.\" file and include the License file at usr/src/CDDL.txt.
.\" If applicable, add the following below this CDDL HEADER, with the
.\" fields enclosed by brackets [] replaced with your own identifying
.\" information: Portions Copyright [yyyy] [name of copyright owner]
.\"
.\" CDDL HEADER END
.\"
.\" @(#)chkinfr.1m 1.2 08/05/20 SMI
.\" Copyright (c) 2000 by Sun Microsystems, Inc.
.\" All rights reserved.
.if n .tr \--
.TH chkinfr 1M "08/05/20" "Sun Cluster 3.0"
.SH NAME
chkinfr \- validate infrastructure file contents
.br
.SH SYNOPSIS
.B /usr/cluster/lib/sc/chkinfr
[
.B \-F
] [
.B \-i
.I filename
] [
.B \-p
.I directory
]
.SH DESCRIPTION
.B chkinfr
validates the contents of the
.B /etc/cluster/ccr/infrastructure
file, which contains information about the configuration of the cluster.
This file contains information about the nodes in the cluster,
adapters in each node,
switches/blackboxes used to connect the nodes (private interconnect),
cables that are connected between adapters or
between an adapter and a switch/blackbox,
and quorum devices.
It also contains other information such as name of the cluster and
the number of quorum votes given to each node.
See
.BR ccr_infrastructure (4)
for more details about the contents of the
infrastructure file.
.LP
Since the infrastructure information is stored in a file in the file system,
it is possible for it to get corrupted or truncated or removed
without the knowledge of cluster software.
Also the file can be edited manually by the cluster administrator
to correct or add information
as part of emergency repair procedures.
Thus,
.B chkinfr
is necessary to make sure that the
file contents are correct and the system will boot successfully
and function as expected.
.LP
.B chkinfr
is invoked at boot time before the cluster
software is loaded by the rc script
.BR /etc/rcS.d/S41bootcluster.sh .
Any errors detected are displayed on the console and also stored
in the
.B /etc/cluster/chkinfr.err
file.
The system is halted if any errors are detected.
If this happens,
you should boot the system in non-cluster mode and repair the file.
.LP
.B chkinfr
checks to see if the information about nodes, adapters, switches, cables,
and quorum devices is correct in the infrastructure file.
It uses the .clpl files in the
.B /etc/cluster/clpl
directory to validate the supported adapters
(e.g. hme,qfe), supported transport types (e.g. dlpi,rsm) for
each type of adapter, and supported blackbox types (e.g.
switch).
.SH OPTIONS
.TP 15
.B \-F
Force the command return value to 0.
Can be used to continue booting if
.B chkinfr
in the rc script reports errors.
.TP
.BI \-i " file"
validates
.I file
instead of the default
.BR /etc/cluster/ccr/infrastructure .
.TP
.BI \-p " directory"
searches for
the .clpl files in
.I directory 
instead of the default
.BR /etc/cluster/clpl .
.SH USAGE
.B chkinfr
can be used in cluster mode and non-cluster mode.
.SH EXIT STATUS
The following exit values are returned:
.TP 10
0
No errors were detected, or errors were detected but the
.B \-F
flag was used.
.TP
>0
Error(s) were detected.
.SH FILES
.TP 30
.B /etc/cluster/ccr/infrastructure
The clustering infrastructure file
.TP
.B /etc/cluster/clpl
The .clpl files directory
.TP
.B /etc/cluster/chkinfr.err
Error output from
.B chkinfr
.TP
.B /etc/rcS.d/S41bootcluster.sh
rc script that runs
.B chkinfr
.SH NOTES
Here is the list of possible cases where
.B chkinfr
reports errors.
.LP
.nf
The cluster wide check reports errors in the following cases:
	- it cannot open .clpl files or .clpl files are corrupted
	- there are no nodes in the cluster
	- the node name is missing or duplicated
	- there are no ENABLED nodes in the cluster
	- the blackbox name is missing or duplicated
	- the quorum_vote and quorum_resv_key are not in at least one node's properties
	- the connectivity matrix shows that one of the nodes is not reachable from another node

Each node check reports errors in the following cases:
	- node name or state is missing
	- missing or duplicate adapter name
	- there are no ENABLED adapters in the node
	- there is no private hostname property

Each blackbox/switch check can result in errors in the following cases:
	- blackbox name or state is missing
	- missing or invalid type
	- the same blackbox port is used more than once
	- blackbox is enabled and there are no ports
	- blackbox is enabled and port name is missing
	- blackbox is enabled and there are no ports

Each cable check can result in errors in the following cases:
	- cable state is missing
	- endpoint is not valid (For example, the endpoint does not exist in the infrastructure file)
	- cable endpoint is not an adapter or a blackbox
	- both the endpoints are adapters and they are of different types
	  (e.g. one is hme and the other is sci)

Each adapter check can result in errors in the following cases:
	- adapter name or state is missing
	- invalid IP address (e.g. 124.367.18.50)
	- duplicate IP address
	- invalid netmask
	- one or more of the ip_address, transport_type, device_name, device_instance,
	  or netmask properties are missing
	- device name is not supported (using .clpl files)
	- adapter_id property is missing when the device name is sci
	- device name is sci and trtype is dlpi and dlpi_device_name is missing
	- ports are missing and adapter is enabled
	- more than one port is mentioned for an adapter

Each quorum device check can result in errors in the following cases:
	- the quorum device id is out of range
	- the gdevname is missing
	- the vote count property is missing
.fi
.SH SEE ALSO
.BR ccr_infrastructure (4)
.SH ATTRIBUTES
See
.BR attributes (5)
for descriptions of the following attributes:
.sp
.TS
box;
cbp-1 | cbp-1
l | l .
ATTRIBUTE TYPE	ATTRIBUTE VALUE
=
Availability	SUNWscu
_
Interface Stability	Evolving
.TE
