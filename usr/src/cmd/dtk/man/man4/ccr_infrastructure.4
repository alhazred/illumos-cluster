'\" t
.\"
.\" CDDL HEADER START
.\"
.\" The contents of this file are subject to the terms of the
.\" Common Development and Distribution License (the License).
.\" You may not use this file except in compliance with the License.
.\"
.\" You can obtain a copy of the license at usr/src/CDDL.txt
.\" or http://www.opensolaris.org/os/licensing.
.\" See the License for the specific language governing permissions
.\" and limitations under the License.
.\"
.\" When distributing Covered Code, include this CDDL HEADER in each
.\" file and include the License file at usr/src/CDDL.txt.
.\" If applicable, add the following below this CDDL HEADER, with the
.\" fields enclosed by brackets [] replaced with your own identifying
.\" information: Portions Copyright [yyyy] [name of copyright owner]
.\"
.\" CDDL HEADER END
.\"
.\" @(#)ccr_infrastructure.4 1.2 08/05/20 SMI
.\" Copyright (c) 2000 by Sun Microsystems, Inc.
.\" All rights reserved.
.if n .tr \--
.TH ccr_infrastructure 4 "08/05/20" "Sun Cluster 3.0"
.SH NAME
ccr_infrastructure, infrastructure - CCR cluster infrastructure table
.SH SYNOPSIS
.B /etc/cluster/ccr/infrastructure
.SH DESCRIPTION
The CCR cluster infrastructure table is a CCR table which contains
most of the primary configuration data and properties for the
low level cluster infrastructure, transport, and quorum devices.
.LP
Each line in the table has an entry in the form of a key-value pair.
In some cases, the keyword itself includes variable data, such as
an identifier, or ID.   But, with the exception of node IDs, most IDs embedded
within keywords have little or no use outside of defining a branch
within the CCR infrastructure tree itself.
.LP
The infrastructure table hierarchy is that of a simple tree structure.  And,
each ID is used to group together branches of that tree.
.LP
This table, of course, follows all of the basic syntax rules required
of any CCR table.   It is stored under /etc/cluster/ccr as a regular ASCII
file, with a CCR directory entry.   And, it includes the standard
.B ccr_gennum
and
.B ccr_checksum
keywords.
.LP
As with all CCR table files, the manual editing of this file is
strongly discouraged, except under extreme emergency conditions and
under the direct instruction from service support.  Editing should only
occur when all nodes are booted in non-cluster mode and must be
performed using the approved procedures for repairing a CCR
table (see
.BR ccradm (1M)).
.SH FORMAT
This section describes the keywords found in the CCR cluster infrastructure
table.   If the cluster is in a healthy state, most of these data items
can be changed using either
.BR scconf (1M)
or
.BR scsetup (1M).
.\"
.\"
.\" *********************************************************
.\" * cluster.name
.\" *********************************************************
.\"
.TP
.BI cluster.name \0clustername
The
.I clustername
is the name of the cluster.  It may be any alphanumeric string of
up to 256 characters.
.RS
.LP
The
.I clustername
is set by
.BR scinstall (1M)
at the time that the first node is installed in the cluster.  And, it
can be used again by
.BR scinstall (1M)
to verify that subsequently installed nodes are added to the right
cluster.  Otherwise, the
.B Sun Cluster 3.0
software does not use the
.I clustername
for any purpose other than display.  However, additional dependencies may
evolve in the future.
.LP
If a
.I clustername
is not given at install time, the default is to set
.I clustername
to be the name of the first node used to establish the cluster.  Both
.BR scsetup (1M)
and
.BR scconf (1M)
can be used later to change the
.IR clustername .
.RE
.\"
.\"
.\" *********************************************************
.\" * cluster.state
.\" *********************************************************
.\"
.TP
.BI cluster.state \0state
The cluster
.I state
must always be set to
.BR enabled .
.\"
.\"
.\" *********************************************************
.\" * cluster.properties.cluster_id <clusterid>
.\" *********************************************************
.\"
.TP
.BI cluster.properties.cluster_id \0clusterid
The
.I clusterid
is a randomly generated 32-bit number represented in this
table in hexadecimal format.   It is preferred, but not required,
for each cluster within the enterprise to have a unique
.IR clusterid .
Currently,
.BR scinstall (1M)
uses the time-of-day clock to generate the
.I clusterid
at the time that the first node is installed in the cluster.
.RS
.LP
The
.I clusterid
is used in forming individual quorum reservation keys.
.LP
The
.I clusterid
cannot be changed by standard procedures or commands.
.RE
.\"
.\"
.\" *********************************************************
.\" * cluster.properties.installmode
.\" *********************************************************
.\"
.TP
.BI cluster.properties.installmode \0state
The cluster installmode
.I state
may be set to either
.B enabled
or
.BR disabled .
The installmode
.I state
is usually only
.B enabled
when the cluster is first installed.
.RS
.LP
At cluster install time, the first node to be installed is installed
with a quorum vote count of one, and all other nodes are installed with
a vote count of zero.  Normally, when a node boots in cluster mode, its
quorum vote count is always set to one.   However, if installmode is
.BR enabled ,
the vote count is left unchanged.
.LP
When
.BR scsetup (1M)
is run, it will detect whether or not installmode is
.BR enabled .
And, if it is set, it will allow the user to configure any required
shared quorum devices, reset the vote count of all the nodes to their
default of one, and, finally, disable installmode.
.RE
.\"
.\"
.\" *********************************************************
.\" * cluster.properties.private_net_number <netnumber>
.\" *********************************************************
.\"
.TP
.BI cluster.properties.private_net_number \0netnumber
The
.I netnumber
is the network address
.RB ( networks (1M))
assigned to the private network.  This, along with the
.B private_netmask
property, is used to generate individual transport adapter
.B netmask
and
.B ip_address
properties.
.RS
.LP
The
.I netnumber
is specified in standard dot notation.  And, the last two octets of the
.I netnumber
must always be zero.  The default number is 172.16.0.0.
.LP
This private
.I netnumber
is set by
.BR scinstall (1M)
at the time that the first node is installed into the cluster.
And, it cannot be changed later by standard procedures or commands.
.RE
.\"
.\"
.\" *********************************************************
.\" * cluster.properties.private_netmask <netmask>
.\" *********************************************************
.\"
.TP
.BI cluster.properties.private_netmask \0netmask
The
.I netmask
is used to define the network portion of the given
.BR private_net_number .
.RS
.LP
The
.I netmask
is specified in standard dot notation.  The last two octets of this
.I netmask
must always be zero, and there may not be any holes in the mask.  The
default mask is 255.255.0.0.
.LP
This private
.I netmask
is set by
.BR scinstall (1M)
at the time that the first node is installed into the cluster.
And, it cannot be changed later by standard procedures or commands.
.RE
.\"
.\"
.\" *********************************************************
.\" * cluster.nodes.<nodeid>.name <nodename>
.\" *********************************************************
.\"
.TP
.BI cluster.nodes. <nodeid> .name \0nodename
The cluster
.I nodename
for any given
.I nodeid
should always be the same as its Solaris nodename, as expressed by
uname -n
.RB ( uname (1M)).
.RS
.LP
Each
.I nodename
is set by
.BR scinstall (1M).
And, neither the
.I nodeid
nor the
.I nodename
can be changed later by standard procedures or commands.
.RE
.\"
.\"
.\"
.\"
.\" *********************************************************
.\" * cluster.nodes.<nodeid>.state <state>
.\" *********************************************************
.\"
.TP
.BI cluster.nodes. <nodeid> .state \0state
The
.I state
of a particular
.I nodeid
is either
.B enabled
or
.BR disabled .
.RS
.LP
A node will typically only be in the
.B disabled
state for a very brief period during its installation.  Once the first
.B cable
is established for a new cluster node, the
.I state
of the node is immediately
.BR enabled .
And, there is no command support for switching the
.I state
back to
.BR disabled .
.RE
.\"
.\"
.\" *********************************************************
.\" * cluster.nodes.<nodeid>.properties.private_hostname <phostname>
.\" *********************************************************
.\"
.TP
.BI cluster.nodes. <nodeid> .properties.private_hostname \0phostname
The private hostname of a node is the name by which a node can
be contacted over the private network.   Each private hostname should
be unique.
.RS
.LP
At install time, each node is assigned to a default private hostname of
.BI clusternode <nodeid> -priv.
And, private hostnames can be changed later using either
.BR scsetup (1M)
or
.BR scconf (1M).
.LP
Note that private hostnames should never be stored in
/etc/inet/hosts or other
.BR hosts (4)
databases.
.RE
.\"
.\"
.\" *********************************************************
.\" * cluster.nodes.<nodeid>.properties.quorum_vote <vcount>
.\" *********************************************************
.\"
.TP
.BI cluster.nodes. <nodeid> .properties.quorum_vote \0vcount
If a node is in quorum maintenance mode, it will have a quorum vote
count of zero.  Otherwise, its vote count should always be set to
one.  And, unless cluster installmode is set, the vote count of a node is
always reset to one when it boots into the cluster.
.RS
.LP
.BR scconf (1M)
can be used to set and clear the quorum maintenance state of a node.
However, there are no supported procedures for changing the vote count
of a node to anything other than zero or one.
.RE
.\"
.\"
.\" *********************************************************
.\" * cluster.nodes.<nodeid>.properties.quorum_resv_key <rkey>
.\" *********************************************************
.\"
.TP
.BI cluster.nodes. <nodeid> .properties.quorum_resv_key rkey
Each node in the cluster is automatically assigned to a quorum reservation
key at install time.  This is an eight byte value expressed in
.I rkey
as a single hexadecimal number.   The key is generated using the
.I clusterid
and
.IR nodeid .
.RS
.LP
The quorum reservation keys cannot be changed by standard procedures
or commands.
.RE
.\"
.\"
.\" *********************************************************
.\" * cluster.nodes.<nodeid>.adapters.<id>.name <adaptername>
.\" *********************************************************
.\"
.TP
.BI cluster.nodes. <nodeid>  .adapters. <id> .name \0adaptername
Each node in the cluster will typically be configured with two or more
cluster transport adapters.  Transport adapter names are given here
using the same style that physical
.I interface
names are given to
.BR ifconfig (1M).
.\"
.\"
.\" *********************************************************
.\" * cluster.nodes.<nodeid>.adapters.<id>.state <state>
.\" *********************************************************
.\"
.TP
.BI cluster.nodes. <nodeid> .adapters. <id> .state \0state
The state of an individual adapter is either
.B enabled
or
.BR disabled .
.RS
.LP
.BR scconf (1M)
can be used to change the state of an adapter.
.RE
.\"
.\"
.\" *********************************************************
.\" * cluster.nodes.<nodeid>.adapters.<id>.properties.device_name <dname>
.\" *********************************************************
.\"
.TP
.BI cluster.nodes. <nodeid> .adapters. <id> .properties.device_name \0dname
The
.B device_name
adapter property is always set to the device name portion of the
adapter name.   It is automatically set at the time that a new transport
adapter is added to the cluster configuration.
.RS
.LP
This adapter property cannot be changed with
.BR scconf (1M)
or other standard procedures or commands.
.RE
.\"
.\"
.\" *********************************************************
.\" * cluster.nodes.<nodeid>.adapters.<id>.properties.device_instance <dinst>
.\" *********************************************************
.\"
.TP
.BI cluster.nodes. <nodeid> .adapters. <id> .properties.device_instance \0dinst
The
.B device_instance
adapter property is always set to the device instance portion of the
adapter name.   It is automatically set at the time that a new transport
adapter is added to the cluster configuraion.
.RS
.LP
This adapter property cannot be changed with
.BR scconf (1M)
or other standard procedures or commands.
.RE
.\"
.\"
.\" *********************************************************
.\" * cluster.nodes.<nodeid>.adapters.<id>.properties.transport_type <trtype>
.\" *********************************************************
.\"
.TP
.BI cluster.nodes. <nodeid> .adapters. <id> .properties.transport_type \0trtype
The only currently supported transport type is
.BR dlpi .
However,
.B rsm
is expected to be supported in the near future for
.B sci
adapters.  Transport types should not be mixed on the same cluster.
.RS
.LP
The transport type is set at the time that a new transport
adapter is added to the cluster configuraion.  And, if not explicitly set,
it will be set to the default,
.BR dlpi .
Once it is set, this adapter property cannot be changed with
.BR scconf (1M)
or other standard procedures or commands.
.RE
.\"
.\"
.\" *********************************************************
.\" * cluster.nodes.<nodeid>.adapters.<id>.properties.netmask <netmask>
.\" *********************************************************
.\"
.TP
.BI cluster.nodes. <nodeid> .adapters. <id> .properties.netmask \0netmask
The per-adapter
.B netmask
property is internally generated by the transport software at the
time that a new adapter is added to the cluster configuration.
.RS
.LP
Users should not make any assumptions about how this property is
used by the clustering software and should have no reason to even look up
the setting of this property.
.LP
This adapter property cannot be changed with
.BR scconf (1M)
or other standard procedures or commands.
.RE
.\"
.\"
.\" *********************************************************
.\" * cluster.nodes.<nodeid>.adapters.<id>.properties.ip_address <ip_address>
.\" *********************************************************
.\"
.TP
.BI cluster.nodes. <nodeid> .adapters. <id> .properties.ip_address \0ip_address
The per-adapter
.B ip_address
property is internally generated by the transport software at the
time that a new adapter is added to the cluster configuration.
.RS
.LP
Users should not make any assumptions about how this property is
used by the clustering software and should have no reason to even look up
the setting of this property.
.LP
This adapter property cannot be changed with
.BR scconf (1M)
or other standard procedures or commands.
.RE
.\"
.\"
.\" *********************************************************
.\" * cluster.nodes.<nodeid>.adapters.<id>.properties.dlpi_heartbeat_quantum
.\" *    <quantum>
.\" *********************************************************
.\"
.TP
.BI cluster.nodes. <nodeid> .adapters. <id> .properties.dlpi_heartbeat_quantum \0quantum
The
.B dlpi_heartbeat_quantum
is an undocumented adapter property.   When a new
.B dlpi
cluster transport adapter is added to the cluster configuration, this
property is automatically set to a default value for that adapter.
Currently, all adapters use a default of 2000 milliseconds.
.RS
.LP
This adapter property can be changed using
.BR scconf (1M).
But, its use is not publicly documented.  It is currently considered
for internal use only and should only be changed under the explicit direction
of the Sun Cluster development team.
.RE
.\"
.\"
.\" *********************************************************
.\" * cluster.nodes.<nodeid>.adapters.<id>.properties.dlpi_heartbeat_timeout
.\" *    <tout>
.\" *********************************************************
.\"
.TP
.BI cluster.nodes. <nodeid> .adapters. <id> .properties.dlpi_heartbeat_timeout \0tout
The
.B dlpi_heartbeat_timeout
is an undocumented adapter property.   When a new
.B dlpi
cluster transport adapter is added to the cluster configuration, this
property is automatically set to a default value for that adapter.
Currently, all adapters use a default of 10000 milliseconds.
.RS
.LP
This adapter property can be changed using
.BR scconf (1M).
But, its use is not publicly documented.  It is currently considered
for internal use only and should only be changed under the explicit direction
of the Sun Cluster development team.
.RE
.\"
.\"
.\" *********************************************************
.\" * cluster.nodes.<nodeid>.adapters.<id>.properties.rsm_heartbeat_quantum
.\" *    <quantum>
.\" *********************************************************
.\"
.TP
.BI cluster.nodes. <nodeid> .adapters. <id> .properties.rsm_heartbeat_quantum \0quantum
The
.B rsm_heartbeat_quantum
is an undocumented adapter property.   When a new
.B rsm
cluster transport adapter is added to the cluster configuration, this
property is automatically set to a default value for that adapter.
Currently, all adapters use a default of 2000 milliseconds.
.RS
.LP
This adapter property can be changed using
.BR scconf (1M).
But, its use is not publicly documented.  It is currently considered
for internal use only and should only be changed under the explicit direction
of the Sun Cluster development team.
.RE
.\"
.\"
.\" *********************************************************
.\" * cluster.nodes.<nodeid>.adapters.<id>.properties.rsm_heartbeat_timeout
.\" *    <tout>
.\" *********************************************************
.\"
.TP
.BI cluster.nodes. <nodeid> .adapters. <id> .properties.rsm_heartbeat_timeout \0tout
The
.B rsm_heartbeat_timeout
is an undocumented adapter property.   When a new
.B rsm
cluster transport adapter is added to the cluster configuration, this
property is automatically set to a default value for that adapter.
Currently, all adapters use a default of 1000 milliseconds.
.RS
.LP
This adapter property can be changed using
.BR scconf (1M).
But, its use is not publicly documented.  It is currently considered
for internal use only and should only be changed under the explicit direction
of the Sun Cluster development team.
.RE
.\"
.\"
.\" *********************************************************
.\" * cluster.nodes.<nodeid>.adapters.<id>.properties.bandwidth <bwidth>
.\" *********************************************************
.\"
.TP
.BI cluster.nodes. <nodeid> .adapters. <id> .properties.bandwidth \0bwidth
The
.B bandwidth
is property an undocumented adapter property.   When a new
cluster transport adapter is added to the cluster configuration, this
property is automatically set to a default value for that adapter.
The current defaults are as follows:
.RS
.LP
.nf
		ge		70
		hme		10
		qfe		10
		sci		30
.fi
.LP
Values are in units of MB/sec.
.LP
This adapter property can be changed using
.BR scconf (1M).
But, its use is not publicly documented.  It is currently considered
for internal use only and should only be changed under the explicit direction
of the Sun Cluster development team.
.RE
.\"
.\"
.\" *********************************************************
.\" * cluster.nodes.<nodeid>.adapters.<id>.properties.nw_bandwidth <nw_bwidth>
.\" *********************************************************
.\"
.TP
.BI cluster.nodes. <nodeid> .adapters. <id> .properties.nw_bandwidth \0nw_bwidth
The
.B nw_bandwidth
is property an undocumented adapter property.   When a new
cluster transport adapter is added to the cluster configuration, this
property is automatically set to a default value for that adapter.
All adapters currently have the same default value of 80%.
.RS
.LP
This adapter property can be changed using
.BR scconf (1M).
But, its use is not publicly documented.  It is currently considered
for internal use only and should only be changed under the explicit direction
of the Sun Cluster development team.
.RE
.\"
.\"
.\" *********************************************************
.\" * cluster.nodes.<nodeid>.adapters.<id>.ports.<id>.name <portname>
.\" *********************************************************
.\"
.TP
.BI cluster.nodes. <nodeid> .adapters. <id> .ports. <id> .name \0portname
Whenever a cable is configured for a particular adapter, a new port
is added.  And, conversely, the port is automatically removed whenever
the cable is removed.  All currently supported adapters support only
a single port.  And, so, the adapter port
.I id
is always 1.
.RS
.LP
The default
.I portname
for an adapter is always 0.  And, while both
.BR scconf (1M)
and the non-interactive form of
.BR scinstall (1M)
allow the user to specify a different
.I portname
with the cable suboptions, the default is usually taken.  The adapter
.I portname
has no special meaning for any of the currently supported adapters.  However,
the command set disallows the inclusion of the characters '@', ':', and ','
in the name.
.RE
.\"
.\"
.\" *********************************************************
.\" * cluster.nodes.<nodeid>.adapters.<id>.ports.<id>.state <state>
.\" *********************************************************
.\"
.TP
.BI cluster.nodes. <nodeid> .adapters. <id> .ports <id> .state \0state
The
.I state
of an adapter port is either
.B enabled
or
.BR disabled .
.RS
.LP
Whenever a cable is enabled using either
.BR scinstall (1M),
.BR scsetup (1M),
or
.BR scconf (1M),
both of its ports, and their parent objects
(adapters or junctions/blackboxes) are also enabled.
However, when a cable is disabled, only the state of the cable itself
is changed.   The command set does not provide for the disabling of
an individual port once it has been enabled.
.RE
.\"
.\"
.\" *********************************************************
.\" * cluster.nodes.blackboxes.<id>.name <bbname>
.\" *********************************************************
.\"
.TP
.BI cluster.blackboxes. <id> .name \0bbname
Blackbox is an obsolete term for cluster transport junction.  However,
the old term is still used in the CCR infrastructure table.
.RS
.LP
Each junction name must be unique within the cluster.  But, none of the
currently support junction types have any special requirements of the
name.
.LP
A junction is named whenever it is added to the cluster configuration using
.BR scinstall (1M),
.BR scsetup (1M),
or
.BR scconf (1M).
The default name used by the command set is
.RI switch <N>.
The command set does not support changing the name once it has
been established.
.RE
.\"
.\"
.\" *********************************************************
.\" * cluster.nodes.blackboxes.<id>.state <state>
.\" *********************************************************
.\"
.TP
.BI cluster.blackboxes. <id> .state \0state
Again, blackbox is an obsolete term for cluster transport junction.  However,
the old term is still used in the CCR infrastructure table.
.RS
.LP
The
.I state
of a particular junction is either
.B enabled
or
.BR disabled .
.LP
Whenever a cable is enabled using either
.BR scinstall (1M),
.BR scsetup (1M),
or
.BR scconf (1M),
both of its ports, and their parent objects
(adapters or junctions), are also enabled.
The 
.B scconf (1M)
command also provides for expicit enabling
and disabling of an entire junction under certain conditions.
.RE
.\"
.\"
.\" *********************************************************
.\" * cluster.nodes.blackboxes.<id>.properties.type <bbtype>
.\" *********************************************************
.\"
.TP
.BI cluster.blackboxes. <id> .properties.type \0bbtype
Again, blackbox is an obsolete term for cluster transport junction.  However,
the old term is still used in the CCR infrastructure table.
.RS
.LP
The only junction type currently supported is
.I bbtype
.BR switch .
However the non-interactive form of
.BR scinstall (1M)
and the
.BR scconf (1M)
command do support options for expansion to other junction types.
But, the command set does not support changing the type once it has
been established.
.RE
.\"
.\"
.\" *********************************************************
.\" * cluster.nodes.blackboxes.<id>.ports.<id>.name <portname>
.\" *********************************************************
.\"
.TP
.BI cluster.blackboxes. <id> .ports. <id> .name \0portname
Again, blackbox is an obsolete term for cluster transport junction.  However,
the old term is still used in the CCR infrastructure table.
.RS
.LP
Whenever a cable is configured to a port on a particular junction, a new port
object is added.  And, conversely, the port is automatically removed whenever
the cable is removed.
.LP
.BR scinstall (1M),
.BR scsetup (1M),
and
.BR scconf (1M)
all provide for
.I portname
specification as part of adding cables.  The default junction
.I portname
used by the command set is the same as the
.I nodeid
number found at the other end of the cable.  However, some junction types,
such as the
.B sci
switch, do have special naming requirements.
But, the command set does not support changing the
.I portname
once it has been established at cable-add time.
.RE
.\"
.\"
.\" *********************************************************
.\" * cluster.nodes.blackboxes.<id>.ports.<id>.state <state>
.\" *********************************************************
.\"
.TP
.BI cluster.blackboxes. <id> .ports. <id> .state \0state
Again, blackbox is an obsolete term for cluster transport junction.  However,
the old term is still used in the CCR infrastructure table.
.RS
.LP
The
.I state
of an individual junction port is either
.B enabled
or
.BR disabled .
.LP
Whenever a cable is enabled using either
.BR scinstall (1M),
.BR scsetup (1M),
or
.BR scconf (1M),
both of its ports, and their parent objects
(adapters or junctions/blackboxes), are also enabled.
However, when a cable is disabled, only the state of the cable itself
is changed.   The command set does not provide for the disabling of
an individual port once it has been enabled.
.RE
.\"
.\"
.\" *********************************************************
.\" * cluster.cables.<id>.properties.end1 <endpoint1>
.\" * cluster.cables.<id>.properties.end1 <endpoint2>
.\" *********************************************************
.\"
.LP
.BI cluster.cables. <id> .properties.end1 \0endpoint1
.br
.BI cluster.cables. <id> .properties.end2 \0endpoint2
.RS
Each cluster transport cable has two endpoints,
.I endpoint1
and
.IR endpoint2 .
.LP
At least one of the two endpoints must be an adapter port endpoint.
And, the other endpoint is either that of a port on
another adapter or that of a port on a cluster transport junction.
.LP
An adapter port
.I endpoint
takes the following form:
.RS
.LP
.BI cluster.nodes. <nodeid> .adapters. <id> .ports. <id>
.RE
.LP
And, a junction endpoint looks like this:
.RS
.LP
.BI cluster.blackboxes. <id> .ports. <id>
.RE
.LP
.BR scinstall (1M),
.BR scsetup (1M),
and
.BR scconf (1M)
can all be used to add cables to the cluster.  But, the command set
does not support changing endpoint data for already established
cables.  Cable definitions may be added to or removed from the configuration,
but they cannot be changed in place.
.RE
.\"
.\"
.\" *********************************************************
.\" * cluster.cables.<id>.state <state>
.\" *********************************************************
.\"
.TP
.BI cluster.cables. <id> .state \0state
The state of a particular cable is either
.B enabled
or
.BR disabled .
.RS
.LP
And, whenever a cable is enabled using either
.BR scinstall (1M),
.BR scsetup (1M),
or
.BR scconf (1M),
both of its ports, and their parent objects
(adapters or junctions), are also enabled.
However, when a cable is disabled, only the state of the cable itself
is changed.  Both
.BR scsetup (1M)
and
.BR scconf (1M)
support changing the state of a cable to either one of the two states,
conditions permitting.
.RE
.\"
.\"
.\" *********************************************************
.\" * cluster.quorum_devices.<id>.name <name>
.\" *********************************************************
.\"
.TP
.BI cluster.quorum_devices. <id> .name \0name
In the current release, some number of shared quorum disk devices
may be added using either
.BR scsetup (1M)
or
.BR scconf (1M).
.RS
.LP
The minimum requirement recognized and enforced by the command set is that
all two-node clusters be configured with at least one dual-ported
shared quorum disk device.  However, the
.B Sun Cluster 3.0 AnswerBook
documentation should be carefully inspected for
additional requirements and recommendations which might need to be
met before a cluster can be considered supportable.
.LP
The quorum device
.I name
is always set to the relative name of a DID, or global, disk.  In other
words, the quorum device
.I name
will always have the format of
.BI d <N>.
Both an absolute path prefix and slice number suffix will always be
missing.
.LP
Both
.BR scsetup (1M)
and
.BR scconf (1M)
can be used to add and remove shared quorum devices to and from the
cluster configuration.
.RE
.\"
.\"
.\" *********************************************************
.\" * cluster.quorum_devices.<id>.state <name>
.\" *********************************************************
.\"
.TP
.BI cluster.quorum_devices. <id> .state \0state
When a shared quorum device is in quorum maintenance state, its
.I state
is set to
.BR disabled ,
and its quorum vote count is set to zero.
.RS
.LP
Note that the meaning of and operation against this field may
change in the
.B beta2
release of
.BR "Sun Cluster 3.0" .
.LP
.BR scconf (1M)
can be used to set or re-set the quorum maintenance state of a shared
quorum device.
.RE
.\"
.\"
.\" *********************************************************
.\" * cluster.quorum_devices.<id>.properties.votecount <vcount>
.\" *********************************************************
.\"
.TP
.BI cluster.quorum_devices. <id> .properties.votecount \0vcount
When a shared quorum device is in quorum maintenance state, its
.I state
is set to
.BR disabled ,
and its quorum vote count is set to zero.  But, if the device is
not in maintenance state, its
.I state
is set to
.BR enabled ,
and its vote count, or
.IR vcount ,
is set to zero or
.IR <N> -1,
whichever is greater.
.I <N>
is the number of enabled paths (see the
.BI path_ <nodeid>
property, below).
.RS
.LP
.BR scconf (1M)
can be used to set or re-set the quorum maintenance state of a shared
quorum device.
.RE
.\"
.\"
.\" *********************************************************
.\" * cluster.quorum_devices.<id>.properties.gdevname <gdevname>
.\" *********************************************************
.\"
.TP
.BI cluster.quorum_devices. <id> .properties.gdevname \0gdevname
The
.I gdevname
for a shared quorum device directly names the absolute path
of the DID disk name to use for accessing this device as a shared
quorum device.  For example, if the
.B quorum_devices
.I name
is
.BR d4 ,
the
.I gdevname
property for the device should likely be set to
.BR /dev/did/rdsk/d4s2 .
.RS
.LP
When either
.BR scsetup (1M)
or
.BR scconf (1M)
are used to add a new shared quorum disk device to the configuration,
only the relative device
.I name
is required.  The
.I gdevname
is automatically set.
.RE
.\"
.\"
.\" *********************************************************
.\" * cluster.quorum_devices.<id>.properties.path_<nodeid> <state>
.\" *********************************************************
.\"
.TP
.BI cluster.quorum_devices. <id> .properties.path_ <nodeid> " " state
A
.BI path_ <nodeid>
entry should exist for each configured node-to-shared-quorum-device.
If the node is in quorum maintenance state, the
.I state
value for this entry should be set to
.BR disabled .
Otherwise, it should be set to
.BR enabled .
.RS
.LP
Note that the meaning of and operation against this field may
change in the
.B beta2
release of
.BR "Sun Cluster 3.0" .
.RE
.\"
.\" EXAMPLE
.\"
.SH EXAMPLE
.LP
The following is an example of an infrastructure CCR table:
.LP
.nf
ccr_gennum	6
ccr_checksum	A43DCF0929777EF8FFC6FDC1B629CAD0
cluster.name	suncluster
cluster.state	enabled
cluster.properties.cluster_id	0x377274A2
cluster.properties.installmode	disabled
cluster.properties.private_net_number	172.16.0.0
cluster.properties.private_netmask	255.255.0.0
cluster.nodes.1.name	red
cluster.nodes.1.state	enabled
cluster.nodes.1.properties.private_hostname	clusternode1-priv
cluster.nodes.1.properties.quorum_vote	1
cluster.nodes.1.properties.quorum_resv_key	0x377274A200000001
cluster.nodes.1.adapters.1.name	hme1
cluster.nodes.1.adapters.1.state	enabled
cluster.nodes.1.adapters.1.properties.device_name	hme
cluster.nodes.1.adapters.1.properties.device_instance	1
cluster.nodes.1.adapters.1.properties.transport_type	dlpi
cluster.nodes.1.adapters.1.properties.netmask	255.255.255.128
cluster.nodes.1.adapters.1.properties.ip_address	172.16.0.129
cluster.nodes.1.adapters.1.properties.dlpi_heartbeat_quantum	2000
cluster.nodes.1.adapters.1.properties.dlpi_heartbeat_timeout	10000
cluster.nodes.1.adapters.1.ports.1.name	0
cluster.nodes.1.adapters.1.ports.1.state	enabled
cluster.nodes.1.adapters.2.name	hme2
cluster.nodes.1.adapters.2.state	enabled
cluster.nodes.1.adapters.2.properties.device_name	hme
cluster.nodes.1.adapters.2.properties.device_instance	2
cluster.nodes.1.adapters.2.properties.transport_type	dlpi
cluster.nodes.1.adapters.2.properties.netmask	255.255.255.128
cluster.nodes.1.adapters.2.properties.ip_address	172.16.1.1
cluster.nodes.1.adapters.2.properties.dlpi_heartbeat_quantum	2000
cluster.nodes.1.adapters.2.properties.dlpi_heartbeat_timeout	10000
cluster.nodes.1.adapters.2.ports.1.name	0
cluster.nodes.1.adapters.2.ports.1.state	enabled
cluster.nodes.2.name	green
cluster.nodes.2.state	enabled
cluster.nodes.2.properties.quorum_vote	1
cluster.nodes.2.properties.quorum_resv_key	0x377274A200000002
cluster.nodes.2.properties.private_hostname	clusternode2-priv
cluster.nodes.2.adapters.1.name	hme1
cluster.nodes.2.adapters.1.state	enabled
cluster.nodes.2.adapters.1.properties.device_name	hme
cluster.nodes.2.adapters.1.properties.device_instance	1
cluster.nodes.2.adapters.1.properties.transport_type	dlpi
cluster.nodes.2.adapters.1.properties.netmask	255.255.255.128
cluster.nodes.2.adapters.1.properties.ip_address	172.16.0.130
cluster.nodes.2.adapters.1.properties.dlpi_heartbeat_quantum	2000
cluster.nodes.2.adapters.1.properties.dlpi_heartbeat_timeout	10000
cluster.nodes.2.adapters.1.name	hme2
cluster.nodes.2.adapters.1.ports.1.name	0
cluster.nodes.2.adapters.1.ports.1.state	enabled
cluster.nodes.2.adapters.2.name	hme2
cluster.nodes.2.adapters.2.state	enabled
cluster.nodes.2.adapters.2.properties.device_name	hme
cluster.nodes.2.adapters.2.properties.device_instance	2
cluster.nodes.2.adapters.2.properties.transport_type	dlpi
cluster.nodes.2.adapters.2.properties.netmask	255.255.255.128
cluster.nodes.2.adapters.2.properties.ip_address	172.16.1.2
cluster.nodes.2.adapters.2.properties.dlpi_heartbeat_quantum	2000
cluster.nodes.2.adapters.2.properties.dlpi_heartbeat_timeout	10000
cluster.nodes.2.adapters.2.ports.1.name	0
cluster.nodes.2.adapters.2.ports.1.state	enabled
cluster.blackboxes.1.name	switch1
cluster.blackboxes.1.state	enabled
cluster.blackboxes.1.properties.type	switch
cluster.blackboxes.1.ports.1.name	1
cluster.blackboxes.1.ports.1.state	enabled
cluster.blackboxes.1.ports.2.name	2
cluster.blackboxes.1.ports.2.state	enabled
cluster.blackboxes.2.name	switch2
cluster.blackboxes.2.state	enabled
cluster.blackboxes.2.properties.type	switch
cluster.blackboxes.2.ports.1.name	1
cluster.blackboxes.2.ports.1.state	enabled
cluster.blackboxes.2.ports.2.name	2
cluster.blackboxes.2.ports.2.state	enabled
cluster.cables.1.properties.end1	cluster.nodes.1.adapters.1.ports.1
cluster.cables.1.properties.end2	cluster.blackboxes.1.ports.1
cluster.cables.1.state	enabled
cluster.cables.2.properties.end1	cluster.nodes.1.adapters.2.ports.1
cluster.cables.2.properties.end2	cluster.blackboxes.2.ports.1
cluster.cables.2.state	enabled
cluster.cables.3.properties.end1	cluster.nodes.2.adapters.1.ports.1
cluster.cables.3.properties.end2	cluster.blackboxes.1.ports.2
cluster.cables.3.state	enabled
cluster.cables.4.properties.end1	cluster.nodes.2.adapters.2.ports.1
cluster.cables.4.properties.end2	cluster.blackboxes.2.ports.2
cluster.cables.4.state	enabled
cluster.quorum_devices.1.name	d4
cluster.quorum_devices.1.state	enabled
cluster.quorum_devices.1.properties.votecount	1
cluster.quorum_devices.1.properties.gdevname	/dev/did/rdsk/d4s2
cluster.quorum_devices.1.properties.path_1	enabled
cluster.quorum_devices.1.properties.path_2	enabled
cluster.quorum_devices.2.name	d5
cluster.quorum_devices.2.state	enabled
cluster.quorum_devices.2.properties.votecount	1
cluster.quorum_devices.2.properties.gdevname	/dev/did/rdsk/d5s2
cluster.quorum_devices.2.properties.path_1	enabled
cluster.quorum_devices.2.properties.path_2	enabled
.fi
.LP
.SH SEE ALSO
.BR scconf (1M),
.BR ccradm (1M),
.BR chkinfr (1M)
.SH ATTRIBUTES
See
.BR attributes (5)
for descriptions of the following attributes:
.sp
.TS
box;
cbp-1 | cbp-1
l | l .
ATTRIBUTE TYPE	ATTRIBUTE VALUE
=
Availability	SUNWscr
_
Interface Stability	Evolving
.TE
