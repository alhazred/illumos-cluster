//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)qdmd.cc	1.1	08/06/03 SMI"

//
// Quorum Device Monitor Daemon
// Periodically pings quorum devices.
// provides early warning if any quorum device doesn't respond.
//
// This is a bare-bones daemon.  The parent process forks; waits for
// the child to report its initialization is complete; then exits.
// The child process periodically calls a routine that obtains the
// list of quorum devices and tries to ping each one in a non-intrusive way.
//

//lint -emacro(1776,CL_PANIC)

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <locale.h>

#include <map>
#include <string>
using namespace std;

#include <sys/os.h>
#include <rgm/sczones.h>
#include <sys/cladm_int.h>

#include "qd_prop_iter.h"
#include "qd_status_iter.h"
#include "qdmd_int.h"

//
// In the typedefs below, the std namespace is explicitly named
// because otherwise flexelint goes crazy.
//
typedef std::map<string, string> qd_list;
typedef std::pair<const string, string> qd_pair;
typedef std::map<string, bool> qd_offline;
typedef std::map<string, int> qd_status;

// This node's qd pathname
char pathname[BUFSIZ];

// The current status of each qd on this node
qd_status qd_stat;

// the program's base name, from the command line
const char *progname;

// program options
int slptime = 5*60;		// default sleep time is 5 minutes
bool opt_F = false;		// don't-fork flag
bool opt_T = false;		// testing mode
//
// opt_D is a debug flag set with "-D".  It enables DPRINTF messages
// and causes qdmd_error output to go to stderr instead of the syslog.
// Note that this option is not controlled by the DEBUG define constant.
// So opt_D debugging is available in both DEBUG and non-DEBUG builds.
//
bool opt_D = false;		// debug flag

// forward declarations
static void daemonize(void);
static void blockon(int ifd);
static void unblock(int ofd);
static qd_list *get_qd_list(void);
void ping_dev(const qd_pair& apair);
static void dev_failure(const qd_pair& apair);
static void act_deleted(const qd_pair& name);
static void act_added(const qd_pair& name);
static void scan_qds(void);
static void parent(int ifd);
static void child(int ofd);
static void usage(void);
static const char *basename(const char *pathnamep);

#ifdef _KERNEL
#error this is userland
#endif

//
// daemonize() --
// Run the monitor as a daemon by forking a child process
// and then exiting in the parent.
//
static void
daemonize(void)
{
	int pipe_fds[2];

	// open pipe for synchronization with child process
	errno = 0;
	int piperet = pipe(pipe_fds);
	CL_PANIC(piperet == 0);

	errno = 0;
	pid_t forkret = fork();
	CL_PANIC(forkret != (pid_t)-1);
	if (forkret == 0) {
		(void) close(pipe_fds[0]);
		child(pipe_fds[1]);
	} else {
		(void) close(pipe_fds[1]);
		parent(pipe_fds[0]);
	}
}

//
// blockon() --
// Wait (in parent process) for child to give the go-ahead thru the pipe.
// The actual data sent thru is presumed to be correct, and doesn't matter.
// The corresponding function in the child is unblock().
//
// parameters:
//	ifd - the input file descriptor for the shared pipe.
//
static void
blockon(int ifd)
{
	char char1 = 0;
	DPRINT((stderr, "waiting on child\n"));
	ssize_t readret = read(ifd, (void *)&char1, 1);
	DPRINT((stderr, "unblocked readret=%d\n", readret));
	if (readret != 1) {
		//
		// No syslog message needed here, since the child process
		// will have already logged its reason for the failure.
		//
		_exit(1);
		// NOTREACHED
	}
}

//
// unblock() --
// Give go-ahead to parent process by writing 1 byte to the shared pipe.
// The actual data sent doesn't matter.
//
// parameters:
//	ofd - the output file descriptor of the shared pipe.
//
static void
unblock(int ofd)
{
	char char1 = 0;

	ssize_t writeret = write(ofd, (void *)&char1, 1);
	CL_PANIC(writeret == 1);
	DPRINT((stderr, "unblock writeret=%d\n", writeret));
}

//
// get_qd_list() --
// Return the current list of names of quorum devices.
//
static qd_list *
get_qd_list(void)
{
	qd_list *retp = new qd_list;
	qd_prop_iter qpi;

	for (qpi.begin(); !qpi.atend(); qpi.advance()) {
		//
		// Check if this qd has a path from this node.
		// We do not monitor the devices which have
		// no path from this node.
		//
		const char *path = qpi.getprop(pathname);
		if (path == NULL) {
			continue;
		}
		const char *name = qpi.getprop("gdevname");
		//
		// First get the "access_mode" property. If it is
		// NULL, then fetch the "type" property. The mapping
		// is:
		//	Type		Access_mode
		//	-----		------------
		//	shared_disk	scsi2/scsi3/sq_disk
		//	netapp_nas	no access mode
		//	quorum_server	no access mode
		//
		const char *type = qpi.getprop("access_mode");
		if (type == NULL) {
			type = qpi.getprop("type");
		}
		DPRINT((stderr, "name=%s type=%s\n", name, type));
		(*retp)[string(name)] = string(type);
	}

	return (retp);
}

//
// dev_failure() --
// Called when qd_alive() returns an error.
// This routine does not have to issue a syslog message,
// because code in qd_alive() already did it.
//
// parameters:
//	the argument object, a devname/devtype pair.
//
static void
dev_failure(const qd_pair&)
{
	//
	// nothing yet. Can be expanded to have inbuilt
	// intelligence when qd failures occur
	//
}

//
// ping_dev() --
// Trivial check to see if dev is alive.
//
// parameters:
//	apair - the argument object, a devname/devtype pair.
//
// NOTE: logically, this function should be declared as static.
// unfortunately if this is done, the compiler chokes if tried to
// call the function within the xping class.
//
void
ping_dev(const qd_pair& apair)
{
	int old_status = 0;

	int status = qd_alive(apair.first.c_str(), apair.second.c_str());
	DPRINT((stderr,
	    "%s: pinging %s (type %s) => %d\n",
	    progname, apair.first.c_str(), apair.second.c_str(), status));

	//
	// Get the current status value. First check if the device
	// is in the list or not.
	//
	if ((qd_stat.find(apair.first.c_str()) != qd_stat.end())) {
		old_status = qd_stat[string(apair.first.c_str())];
	}

	//
	// If the device has become available after a failure, log
	// a message.
	//
	if (old_status == -1 && status == 0) {
		//
		// SCMSGS
		// @explanation
		// The quorum device monitoring daemon reports that
		// the indicated quorum device is now healthy after
		// a previous failure.
		// @user_action
		// No action required.
		//
		qdmd_error(SC_SYSLOG_MESSAGE(
		    "qdmd: "
		    "Quorum device %s is healthy now."), apair.first.c_str());
	}

	// Update the status value.
	qd_stat[string(apair.first.c_str())] = status;

	if (status < 0) {
		dev_failure(apair);
	}
}

//
// class xping is a function object class that calls ping with an extra
// argument that allows it to determine the online/offline status
// of any quorum device.
// its intended usage is within a for_each loop iterating over the
// list of quorum devices.
//
class xping
{
private:
	const qd_offline qdoff;
	// is the current quorum device offline?
	bool isoffline(const string& name) const
	{
		//lint -e{1702}	suppress info about operator!= multiple def
		return (qdoff.find(name) != qdoff.end());
	}
	xping() { }	// no public default constructor
public:
	// constructor remembers the set of offline quorum devices
	xping(const qd_offline& _qdoff) : qdoff(_qdoff) { }

	// ping the given device if it's not offline
	void operator() (const qd_pair& apair) const
	{
		if (!isoffline(apair.first)) {
			::ping_dev(apair);
		}
	}
};

//
// act_deleted() --
// Helper routine used by the code that prints names of deleted quorum devices.
// Also it deletes the removed quorum device entries from the
// qd_stat list.
//
// parameters:
//	apair - the argument object, a devname/devtype pair.
//
static void
act_deleted(const qd_pair& apair)
{
	qd_stat.erase(apair.first.c_str());
	DPRINT((stderr,
	    "%s: deleted %s\n",
	    progname, apair.first.c_str()));
}

//
// act_added() --
// Helper routine used by code that prints names of added quorum devices.
//
// parameters:
//	apair - the argument object, a devname/devtype pair.
//
static void
act_added(const qd_pair& apair)
{
	DPRINT((stderr,
	    "%s: added %s\n",
	    progname,
	    apair.first.c_str()));
}

//
// scan_qds() --
// Get the list of quorum devices and check that each one found is still alive.
//
static void
scan_qds(void)
{
	DPRINT((stderr, "scan_qds\n"));

	static qd_list *old_listp = new qd_list;
	qd_list *new_listp = get_qd_list();

	DPRINT((stderr, "get offline info\n"));
	// get the set of offline quorum devices
	qd_offline qdoff;

	DPRINT((stderr, "transform the offline info\n"));
	qd_status_iter qdsi;
	for (qdsi.begin(); !qdsi.atend(); qdsi.advance()) {
		const char *devname = qdsi.getname();
		bool isoffline = qdsi.isoffline();

		if (isoffline) {
			qdoff[string(devname)] = true;
		}

		DPRINT((stderr,
		    "%s: %s %s\n",
		    progname,
		    devname,
		    isoffline ? "offline" : "online"));
	}

	DPRINT((stderr, "ping all devs that are not offline\n"));

	// ping each device in the new list
	(void) for_each(new_listp->begin(), new_listp->end(), xping(qdoff));

	DPRINT((stderr, "checking for added devs\n"));
	// get list of devices added since last time
	qd_list added;
	(void) set_difference(new_listp->begin(), new_listp->end(),
		old_listp->begin(), old_listp->end(),
		inserter(added, added.end()));
	(void) for_each(added.begin(), added.end(), &act_added);

	DPRINT((stderr, "checking for deleted devs\n"));
	// get list of devices deleted since last time
	qd_list deleted;
	(void) set_difference(old_listp->begin(), old_listp->end(),
		new_listp->begin(), new_listp->end(),
		inserter(deleted, deleted.end()));
	(void) for_each(deleted.begin(), deleted.end(), &act_deleted);

	DPRINT((stderr, "deleting old list\n"));
	// dispose of old dev list, and save pointer to current dev list.
	delete old_listp;
	old_listp = new_listp;
	DPRINT((stderr, "scan_qds done\n"));
}

//
// parent() --
// Code executed in parent process after forking.
// Just wait for the child to notify us, thru the shared pipe,
// that it was able to start up OK.
//
// parameters:
//	ifd - the input file descriptor of the shared pipe.
//
static void
parent(int ifd)
{
	blockon(ifd);

	// everything seems hunky dory, so we can exit now.
	exit(0);
	// NOTREACHED
}

//
// child() --
// Code executed in child process after forking:
// do some setup, unblock the parent, then go into a loop
// scanning the list of quorum devices periodically.
//
// parameters:
//	ofd - the output file descriptor of the shared pipe.
//
static void
child(int ofd)
{
	nodeid_t nodeid;

	qdmd_openlog();

	// when we're testing, it's convenient to see the pid.
	if (opt_T) {
		qdmd_error("qdmd: pid=%d", getpid());
	}

	if (ORB::initialize() != 0) {
		//
		// SCMSGS
		// @explanation
		// The quorum device monitoring daemon received
		// an error during ORB initialization.
		// @user_action
		// Contact your authorized Sun service provider
		// to determine whether a workaround or patch is available.
		//
		qdmd_error(SC_SYSLOG_MESSAGE(
		    "qdmd: "
		    "ORB initialization failed. Exiting."));
		_exit(1);
	}

	// close unneeded file descriptors, unless debugging.
	if (!opt_D) {
		(void) close(0);
		(void) close(1);
	}

	// do session setup and sync for child process, unless didn't fork.
	if (!opt_F) {
		(void) setsid();
		unblock(ofd);
		(void) close(ofd);
	}

	// Initialize this node's quorum device pathname
	if (cladm(CL_CONFIG, CL_NODEID, &nodeid) != 0) {
		//
		// SCMSGS
		// @explanation
		// The quorum device monitoring daemon cannot
		// determine nodeid.
		// @user_action
		// Contact your authorized Sun service provider
		// to determine whether a workaround or patch is available.
		//
		qdmd_error(SC_SYSLOG_MESSAGE(
		    "qdmd: "
		    "Cannot determine nodeid of this node. Exiting."));
		_exit(1);
	}

	(void) sprintf(pathname, "path_%d", nodeid);

	// periodically scan the list of devices
	for (;;) {
		scan_qds();
		(void) sleep((unsigned)slptime);
	}
}

//
// usage() --
// Emit a usage message and exit with an error.
//
// flags:
//	-D		enable debugging printouts
//	-F		don't fork child
//	-T		testing only mode, enables extra printouts
//	-t SLPTIME	sleep for SLPTIME secs between iterations
//
static void
usage(void)
{
	(void) fprintf(stderr, "usage: %s [-DFT] [-t SLPTIME]\n", progname);
	_exit(1);
}

//
// basename() --
// Return the basename of the given pathname.
//
// parameters:
//	pathnamep - the pathname string.
//
static const char *
basename(const char *pathnamep)
{
	const char *slashp = strrchr(pathnamep, '/');
	return (slashp == 0 ? pathnamep : slashp+1);
}

int
main(int argc, char *const *argv)
{
	bool badarg = false;

	// get the name by which the program was invoked, for use in error msgs
	argc--;
	progname = basename(*argv++);

	(void) setlocale(LC_ALL, "");
	(void) textdomain(TEXT_DOMAIN);
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	if (sc_zonescheck() != 0)
		return (1);

	// do flag processing
	while (argc > 0 && **argv == '-') {
		argc--;
		const char *flagp = *argv++;
		if (strcmp(flagp, "--") == 0) {
			break;
		}
		flagp++;
		while (*flagp != '\0') {
			switch (*flagp++) {
			case 'D':
				opt_D = true;
				break;
			case 'F':
				opt_F = true;
				break;
			case 'T':
				opt_T = true;
				break;
			case 't':
				if (argc <= 0) {
					(void) fprintf(stderr,
					    "%s: missing -t SLEEPTIME\n",
					    progname);
					badarg = true;
					break;
				}
				argc--;
				slptime = atoi(*argv++);
				break;
			default:
				(void) fprintf(stderr,
				    "%s: illegal flag \"-%c\"\n",
				    progname,
				    flagp[-1]);
				badarg = true;
				break;
			}
		}
	}

	if (badarg) {
		usage();
	}
	DPRINT((stderr, "slptime=%d\n", slptime));

	if (!opt_F) {
		daemonize();
	} else {
		child(-1);
	}
	return (0);
}
