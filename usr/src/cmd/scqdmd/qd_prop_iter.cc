//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)qd_prop_iter.cc	1.1	08/06/03 SMI"

#include "qd_prop_iter.h"

// initialize the fields, and get a scconf handle
qd_prop_iter::qd_prop_iter()
	: clconf(0), openret(0), beginp(0), nextp(0), curr_objp(0)
{
	openret = scconf_cltr_openhandle(
	    (scconf_cltr_handle_t *)&clconf);
	beginp = clconf_cluster_get_quorum_devices(clconf);
}

// routine to be called at beginning of iteration
void
qd_prop_iter::begin()
{
	nextp = beginp;
	curr_objp = clconf_iter_get_current(nextp);
}

// return true if the last object has already been retrieved
bool
qd_prop_iter::atend() const
{
	return (curr_objp == 0);
}

// routine to advance to next item
void
qd_prop_iter::advance()
{
	clconf_iter_advance(nextp);
	curr_objp = clconf_iter_get_current(nextp);
}

// get a named property from the current item
const char *
qd_prop_iter::getprop(const char *prop) const
{
	return (clconf_obj_get_property(curr_objp, prop));
}

// do the cleanup
qd_prop_iter::~qd_prop_iter()		//lint -e1740
{
	if (beginp != 0) {
		clconf_iter_release(beginp);
	}
	if (clconf != 0) {
		clconf_obj_release((clconf_obj_t *)clconf);
	}
}
