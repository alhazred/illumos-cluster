//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef _QD_PROP_ITER_H
#define	_QD_PROP_ITER_H

#pragma ident	"@(#)qd_prop_iter.h	1.1	08/06/03 SMI"

#include <h/ccr.h>
#include <scadmin/scconf.h>

//
// class qd_prop_iter --
// provides a simplified iterator-like interface to the clconf
// routines used for reading the list of quorum devices.  it lacks
// the full generality of a true iterator.
//
// the intended usage is:
//
//	qd_prop_iter qpi; // instantiate the object
//	for (qpi.begin(); !qpi.atend(); qpi.advance()) { // loop over items
//		// fetch properties as desired from current item
//		const char *val1 = qpi.getprop(prop1);
//		const char *val2 = qpi.getprop(prop2);
//		...
//	}
//	// cleanup occurs automatically when object is destroyed
//
// it's intended that the application be able to create and destroy
// numerous instances of this class over the lifetime of the process.
//
// it's perfectly OK, and expected, that the administrator might
// change the list of configured devices while this is going on.
//
// not sure if this is MT-safe, but it doesn't matter because
// qdmd is a single-threaded program.
//
class qd_prop_iter
{
private:
	//
	// clconf is the handle passed back by scconf_cltr_openhandle.
	// openret is the returned value from scconf_cltr_openhande.
	//
	clconf_cluster_t *clconf;
	int openret;

	//
	// beginp is the iterator value of the first element in the sequence.
	// curr_objp is the object pointer for the current position.
	//
	clconf_iter_t *beginp;
	clconf_iter_t *nextp;
	clconf_obj_t *curr_objp;
public:
	qd_prop_iter();

	// routine to be called at beginning of iteration
	void begin();

	// return true if the last object has already been retrieved
	bool atend() const;

	// routine to advance to next item
	void advance();

	// get a named property from the current item
	const char *getprop(const char *prop) const;

	// do the cleanup
	~qd_prop_iter();
};

#endif // _QD_PROP_ITER_H
