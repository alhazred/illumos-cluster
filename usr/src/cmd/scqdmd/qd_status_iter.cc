//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)qd_status_iter.cc	1.2	08/10/01 SMI"

#include "qd_status_iter.h"

//
// The quorum monitor daemon tries to get the current quorum status
// from the quorum algorithm object in kernel.
// When the quorum monitor daemon queries this status, it is possible
// that a CMM reconfiguration is in progress. In that scenario,
// the query (IDL method) raises a CORBA exception.
// On seeing this exception raised, the quorum monitor daemon sleeps
// for a certain period and then retries the query operation.
//
// This global variable is the sleep period used in such a case.
// The default value of this sleep period is 2 seconds.
// It can be changed for a live running quorum monitor daemon using mdb.
// On the next query, the quorum monitor daemon will pick up the changed value.
//
unsigned int quorum_algo_status_query_sleep_period = 2; // seconds

qd_status_iter::qd_status_iter()
	: qstatp(0), qalg_v(cmm_ns::get_quorum_algorithm(0))
{
	//
	// After the call to quorum_get_immediate_status(),
	// qstatp points to an internal data object that has the info
	// we're interested in.
	//
	// If CMM reconfiguration is in progress,
	// we will see a retry_needed exception raised by the IDL method call.
	// In that case, retry getting the quorum status after
	// the globally specified sleep period -
	// quorum_algo_status_query_sleep_period (in seconds).
	//
	while (1) {
		qalg_v->quorum_get_immediate_status(qstatp, e);
		CORBA::Exception *exp = e.exception();
		if (exp == NULL) {
			// Success
			break;
		}
		if (quorum::retry_needed::_exnarrow(exp)) {
			//
			// Clear exception and retry after sleeping for
			// the number of seconds as specified by the global
			// variable for the sleep period.
			//
			e.clear();
			unsigned int sleep_period =
			    quorum_algo_status_query_sleep_period;
			if (sleep_period < 1) {
				// Sleep for at least 1 second
				sleep_period = 1;
			}
			// Sleep for 'sleep_period' seconds
			os::usecsleep((os::usec_t)sleep_period * MICROSEC);
		} else {
			// This IDL method does not throw any other exceptions
			e.clear();
			ASSERT(0);
		}
	}
	size_t listlen = qstatp->quorum_device_list.length();
	beginp = curr_objp = &qstatp->quorum_device_list[0];
	endp = &qstatp->quorum_device_list[listlen];
}

void
qd_status_iter::begin()
{
	curr_objp = beginp;
}

bool
qd_status_iter::atend()
{
	return (curr_objp == endp);
}

const char *
qd_status_iter::getname() const
{
	return (curr_objp->config.gdevname);
}

bool
qd_status_iter::isoffline() const
{
	return (curr_objp->state == quorum::QUORUM_STATE_OFFLINE);
}

void
qd_status_iter::advance()
{
	curr_objp++;
}

// do the cleanup
qd_status_iter::~qd_status_iter()
{
	// deallocate the status structure
	delete qstatp;
}
