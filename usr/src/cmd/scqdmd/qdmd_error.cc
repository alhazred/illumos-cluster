//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)qdmd_error.cc	1.1	08/06/03 SMI"

#include <sys/types.h>
#include <sys/syslog.h>
#include <stdio.h>
#include <stdarg.h>
#include "qdmd_int.h"

#include <syslog.h>

static const char QDMD_SYSLOG_TAG[] = "Cluster.qdmd";

//
// qdmd_openlog() --
// do the initial opening of the system log, unless the debug flag is on.
//
void
qdmd_openlog()
{
	if (opt_D) {
		return;
	}
	openlog(QDMD_SYSLOG_TAG, LOG_NDELAY, LOG_DAEMON);
}

//
// qdmd_error() --
// send an error message to syslog.  if the debug flag is on,
// send the message to stderr instead.
//
// parameters:
//	fmt - the format string, ala printf.
//	... - the rest of the printf arguments.
//
void
qdmd_error(const char *fmt, ...)
{
	static const int defpri = LOG_NOTICE;
	va_list vap;

	va_start(vap, fmt);
	if (opt_D) {
		(void) vfprintf(stderr, fmt, vap);
		(void) fprintf(stderr, "\n");
	} else {
		vsyslog(defpri, fmt, vap);
	}

	va_end(vap);
}
