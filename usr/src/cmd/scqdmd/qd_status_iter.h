//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef _QD_STATUS_ITER_H
#define	_QD_STATUS_ITER_H

#pragma ident	"@(#)qd_status_iter.h	1.1	08/06/03 SMI"

#include <cmm/cmm_ns.h>

//
// class qd_status_iter --
// provides an iterator-like interface for obtaining the online status
// of quorum devices.  it lacks the full generality of a true iterator.
//
// the intended usage is:
//
//	qd_status_iterator qsi; // instantiate the object
//	for (qsi.begin(); !qsi.atend(); qsi.advance()) { // loop over items
//		// fetch properties as desired from current item
//		const char *name = qsi.getname();
//		bool online = qsi.isoffline();
//		...
//	}
//
// it's intended that the application be able to create and destroy
// numerous instances of this class over the lifetime of the process.
//
// it's perfectly OK, and expected, that the administrator might
// change the list of configured devices while this is going on.
//
// not sure if this is MT-safe, but it doesn't matter because
// qdmd is a single-threaded program.
//
class qd_status_iter
{
private:
	Environment e;
	quorum::quorum_status_t *qstatp;
	quorum::quorum_algorithm_var qalg_v;
	quorum::quorum_device_status_t *beginp;
	quorum::quorum_device_status_t *endp;
	quorum::quorum_device_status_t *curr_objp;

public:
	qd_status_iter();

	// routine to be called at start of loop
	void begin();

	// routine that tells when loop should end
	bool atend();

	// return name of quorum device currently being examined
	const char *getname() const;

	// return true if the current quorum device is offline
	bool isoffline() const;

	// move on to the next quorum device
	void advance();

	// do the clenup
	~qd_status_iter();
};

#endif // _QD_STATUS_ITER_H
