//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef _QDMD_INT_H
#define	_QDMD_INT_H

#pragma ident	"@(#)qdmd_int.h	1.1	08/06/03 SMI"

//
// this header file contains miscellaneous declarations
// shared by various modules in the program.
//

extern bool opt_D;
extern const char *progname;
extern void qdmd_openlog(void);
extern void qdmd_error(const char *fmt, ...);
extern int qd_alive(const char *, const char *);
#define	DPRINT(x)	((void) (opt_D?(fprintf x):0))

#endif // _QDMD_INT_H
