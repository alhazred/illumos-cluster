//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
#pragma ident	"@(#)qd_alive.cc	1.2	08/09/02 SMI"

#include <sys/os.h>
#include <sys/cl_assert.h>
#include <sys/quorum_int.h>
#include <clconf/clnode.h>
#include <clconf/clconf_ccr.h>
#include <cmm/cmm_ns.h>
#include <cmm/cmm_debug.h>
#include <orb/fault/fault_injection.h>
#include "qdmd_int.h"

//
// corba_op_failed() --
// a CORBA operation returns two types of status: the return code
// and possibly an exception.  this function evaluates the success
// or failure of a CORBA operation.
//
// the recommended usage of Environment exception() and clear()
// is to always check for an exception before calling clear,
// rather than simply calling clear.  this function ensures that
// this restriction is uniformly obeyed.
//
// parameters:
//	e - an Environment object.
//	retval - the return value from the CORBA operation
//
// return value:
//	true  - an exception was returned, or the retval is not QD_SUCCESS.
//	false - otherwise
//
static bool
corba_op_failed(Environment& e, int retval)
{
	if (e.exception()) {
		e.clear();
		DPRINT((stderr, "there was an exception\n"));
		return (true);
	}
	DPRINT((stderr, "retval = %d\n", retval));
	return (retval != quorum::QD_SUCCESS);
}

//
// qd_alive() --
// return 0 if the quorum device can successfully respond to a
// "quorum read keys" command.
// return 0 on success, otherwise return -1.
//
// parameters:
//	devname - the name of the device
//	devtype - the type of the device (eg, scsi2, scsi3, netapp_nas)
//
int
qd_alive(const char *devname, const char *devtype)
{
	Environment e;
	nodeid_t mynodeid = orb_conf::local_nodeid();
	int retval = 0;
	quorum::quorum_error_t qerr;

	quorum::device_type_registry_var
	    dtreg_v = cmm_ns::get_device_type_registry(mynodeid);
	if (CORBA::is_nil(dtreg_v)) {
		//
		// SCMSGS
		// @explanation
		// The quorum device monitoring daemon encountered an error
		// while fetching information from the device type registry.
		// @user_action
		// Contact service support.
		//
		qdmd_error(SC_SYSLOG_MESSAGE(
		    "qdmd: "
		    "Quorum device type registry for node %d not found"),
		    mynodeid);
		return (-1);
	}

	quorum::quorum_device_type_var
	    qdt_v = dtreg_v->get_quorum_device_trace(devtype, CMM_AMBER, e);
	qerr = CORBA::is_nil(qdt_v) ? quorum::QD_EIO : quorum::QD_SUCCESS;
	if (corba_op_failed(e, qerr)) {
		//
		// SCMSGS
		// @explanation
		// The quorum device monitoring daemon encountered an error
		// while fetching device type information for the indicated
		// quorum device type.
		// @user_action
		// Contact service support.
		//
		qdmd_error(SC_SYSLOG_MESSAGE(
		    "qdmd: "
		    "Quorum device type variable for type %s not found"),
		    devtype);
		return (-1);
	}

	// an empty properties list is sufficient for the call to quorum_open.
	quorum::qd_property_seq_t qdprops;
	bool scrub_flag = false;

	qerr = qdt_v->quorum_open(devname, scrub_flag, qdprops, e);
	//
	// Check for open errors. EACCESS error is ok with quorum server type.
	// This is because Quorum Server returns EACCESS when a second
	// open is done.
	//
	if (e.exception() || (qerr != quorum::QD_SUCCESS &&
	    (strcmp(devtype, "quorum_server") ? qerr :
	    qerr != quorum::QD_EACCES))) {
		//
		// SCMSGS
		// @explanation
		// The quorum device monitoring daemon encountered an error
		// while opening the indicated quorum device.
		// @user_action
		// Unconfigure the quorum device and reconfigure it.
		// If that fails, replace the quorum device.
		//
		qdmd_error(SC_SYSLOG_MESSAGE(
		    "qdmd: "
		    "An error occurred while opening quorum device %s"),
		    devname);
		e.clear();
		(void) qdt_v->quorum_close(e);
		e.clear();
		return (-1);
	}

	quorum::reservation_list_t resvlist;
	resvlist.listsize = NODEID_MAX;
	resvlist.listlen = 0;
	resvlist.keylist.length(NODEID_MAX);
	ASSERT(resvlist.keylist.length() == NODEID_MAX);

	qerr = qdt_v->quorum_read_reservations(resvlist, e);
	if (corba_op_failed(e, qerr)) {
		//
		// SCMSGS
		// @explanation
		// The quorum device monitoring daemon encountered an error
		// while performing the quorum_read_reservations operation
		// on the indicated quorum device.
		// @user_action
		// Unconfigure the quorum device and reconfigure it.
		// If that fails, replace the quorum device.
		//
		qdmd_error(SC_SYSLOG_MESSAGE(
		    "qdmd: "
		    "An error occurred while reading "
		    "quorum reservation key from "
		    "quorum device %s"),
		    devname);
		retval = -1;
	}

	resvlist.listlen = 0;
	qerr = qdt_v->quorum_read_keys(resvlist, e);
	if (corba_op_failed(e, qerr)) {
		//
		// SCMSGS
		// @explanation
		// The quorum device monitoring daemon encountered an
		// error while performing the quorum_read_keys
		// operation on the indicated device.
		// @user_action
		// Unconfigure the quorum device and reconfigure it.
		// If that fails, replace the quorum device.
		//
		qdmd_error(SC_SYSLOG_MESSAGE(
		    "qdmd: "
		    "An error occurred while reading keys "
		    "on quorum device %s"),
		    devname);
		retval = -1;
	}

	qerr = qdt_v->quorum_close(e);
	(void) corba_op_failed(e, qerr);	// ignore any error here

#ifdef _FAULT_INJECTION
	if (fault_triggered(FAULTNUM_QD_BAD,
	    NULL, NULL)) {
		return (-1);
	}
#endif

	return (retval);
}
